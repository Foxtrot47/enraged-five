﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration
{
    /// <summary>
    /// Tools Configuration Format Exception class.
    /// </summary>
    public class ConfigurationFormatException : ConfigurationException
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public ConfigurationFormatException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public ConfigurationFormatException(String message, Exception inner)
            : base(message, inner)
        {
        }
        #endregion // Constructor(s)
    }
}
