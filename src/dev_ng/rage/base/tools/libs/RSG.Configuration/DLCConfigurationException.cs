﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration
{
    /// <summary>
    /// Tools DLC configuration exception class.
    /// </summary>
    public class DLCConfigurationException : ConfigurationException
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        public DLCConfigurationException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public DLCConfigurationException(String message, Exception inner)
            : base(message, inner)
        {
        }
        #endregion // Constructor(s)
    }
}
