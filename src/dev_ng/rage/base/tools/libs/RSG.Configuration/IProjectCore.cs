﻿//---------------------------------------------------------------------------------------------
// <copyright file="IProjectCore.cs" company="Rockstar">
//     Copyright © Rockstar Games 2011-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Core project interface; shared between all projects (including DLC).
    /// </summary>
    public interface IProjectCore :
        IProject
    {
        #region Properties
        /// <summary>
        /// Available DLC-projects.
        /// </summary>
        IEnumerable<IProjectDLC> DLCProjects { get; }
        
        /// <summary>
        /// Project DLC root directory.
        /// </summary>
        String RootDirectoryDLC { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Save all of the configuration data.
        /// </summary>
        /// <returns></returns>
        bool SaveLocal();
        #endregion // Methods
    }

} // RSG.Configuration namespace
