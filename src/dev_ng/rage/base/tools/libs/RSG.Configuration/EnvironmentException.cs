﻿//---------------------------------------------------------------------------------------------
// <copyright file="EnvironmentException.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Tools Environment Exception class.
    /// </summary>
    public class EnvironmentException : Exception
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected EnvironmentException()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        public EnvironmentException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public EnvironmentException(String message, Exception inner)
            : base(message, inner)
        {
        }
        #endregion // Constructor(s)
    }
} // RSG.Configuration namespace
