﻿using System;

namespace RSG.Configuration
{

    /// <summary>
    /// DLC Project encapsulation; representing a DLC project that is owned by
    /// a "core" project.  These also define a set of branches and targets.
    /// </summary>
    public interface IProjectDLC : 
        IProject
    {
        #region Properties
        /// <summary>
        /// Core-project.
        /// </summary>
        IProjectCore CoreProject { get; }
        #endregion // Properties
    }

} // RSG.Configuration
