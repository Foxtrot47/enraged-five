﻿using System;
using System.Reflection;
using RSG.Configuration.Attributes;

namespace RSG.Configuration
{

    /// <summary>
    /// Enumeration of common project labels.
    /// </summary>
    public enum Label
    {
        None,

        /// <summary>
        /// Current game release label.
        /// </summary>
        [ConfigurationConstant("current")]
        GameCurrent,

        /// <summary>
        /// Current network game release label.
        /// </summary>
        [ConfigurationConstant("network")]
        GameNetwork,

        [ConfigurationConstant("networkdemo")]
        GameNetworkDemo,

        /// <summary>
        /// Game milestone release label.
        /// </summary>
        [ConfigurationConstant("milestone")]
        GameMilestone,

        [ConfigurationConstant("prebuild")]
        GamePrebuild,
        
        /// <summary>
        /// Current tools release label.
        /// </summary>
        [ConfigurationConstant("tools_current")]
        ToolsCurrent,
        
        /// <summary>
        /// Testing tools label.
        /// </summary>
        [ConfigurationConstant("tools_testing")]
        ToolsTesting,

        /// <summary>
        /// Patch testing tools label.
        /// </summary>
        [ConfigurationConstant("tools_patch_testing")]
        ToolsPatchTesting,

        /// <summary>
        /// Incremental tools label.
        /// </summary>
        [ConfigurationConstant("tools_incremental")]
        ToolsIncremental,
    }


    /// <summary>
    /// Label enumeration helper methods.
    /// </summary>
    public static class LabelUtil
    {
        /// <summary>
        /// Returns a label value from a String representation.
        /// </summary>
        /// <param name="platform">Platform string.</param>
        /// <returns></returns>
        public static Label LabelFromConfigurationString(String label)
        {
            Label[] values = (Label[])Enum.GetValues(typeof(Label));
            foreach (Label value in values)
            {
                FieldInfo fi = typeof(Label).GetField(value.ToString());
                ConfigurationConstantAttribute[] attributes =
                    fi.GetCustomAttributes(typeof(ConfigurationConstantAttribute), false) as ConfigurationConstantAttribute[];
                if (0 == attributes.Length)
                    continue; // Skip enum value (no attribute)

                String attribute = (attributes.Length > 0 ? attributes[0].Constant : "");
                if (0 == String.Compare(attribute, label))
                    return (value);
            }
            return (Label.None);
        }
    }

} // RSG.Configuration namespace
