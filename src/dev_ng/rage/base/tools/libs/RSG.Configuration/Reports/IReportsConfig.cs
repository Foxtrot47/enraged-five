﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public interface IReportsConfig
    {
        #region Properties
        /// <summary>
        /// XML configuration filename.
        /// </summary>
        String Filename { get; }

        /// <summary>
        /// Reference to the associated project's branch.
        /// </summary>
        IBranch Branch { get; }

        /// <summary>
        /// Custom report groups
        /// </summary>
        IList<ICustomReportGroup> ReportGroups { get; }

        /// <summary>
        /// Automated report config
        /// </summary>
        IAutomatedReports AutomatedReports { get; }

        /// <summary>
        /// 
        /// </summary>
        IReportGeneratorConfig ReportGeneratorConfig { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        void Reload();
        #endregion // Methods
    } // IReportConfig
}
