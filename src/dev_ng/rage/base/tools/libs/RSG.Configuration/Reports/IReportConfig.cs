﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public interface IReportConfig
    {
        /// <summary>
        /// A name used to uniquely identify this report config.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The name of the report class this report config is for.
        /// </summary>
        string ClassName { get; }

        /// <summary>
        /// The name of the file that should be saved for this report (can be null).
        /// </summary>
        string OutputFilename { get; }

        /// <summary>
        /// List of parameters to set up for this report.
        /// </summary>
        IParameterList Parameters { get; }
    } // IReportConfig
}
