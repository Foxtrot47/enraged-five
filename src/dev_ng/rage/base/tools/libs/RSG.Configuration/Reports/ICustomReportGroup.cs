﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICustomReportGroup
    {
        #region Properties
        /// <summary>
        /// Name of the custom report group as displayed in the workbench.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// List of sub-groups.
        /// </summary>
        IList<ICustomReportGroup> SubGroups { get; }

        /// <summary>
        /// List of reports.
        /// </summary>
        IList<ICustomReport> Reports { get; }
        #endregion // Properties
    } // IReportGroup
}
