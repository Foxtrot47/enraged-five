﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Reports
{
    /// <summary>
    /// Contains all information regarding a single custom report.
    /// </summary>
    public interface ICustomReport
    {
        #region Properties
        /// <summary>
        /// Report's name as displayed in the workbench.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Executable to run when the report is invoked.
        /// </summary>
        string Executable { get; }

        /// <summary>
        /// List of arguments to provide to the executable.
        /// </summary>
        string Arguments { get; }

        /// <summary>
        /// Output file of the executable.
        /// </summary>
        string Output { get; }
        #endregion // Properties
    } // IReport
}
