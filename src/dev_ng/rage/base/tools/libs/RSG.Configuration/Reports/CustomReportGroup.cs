﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    internal class CustomReportGroup : ICustomReportGroup
    {
        #region Constants
        internal const string ELEM_GROUP = "group";
        internal const string ELEM_REPORT = "report";

        internal const string ATTR_NAME = "name";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Name of the custom report group as displayed in the workbench.
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// List of sub-groups.
        /// </summary>
        public IList<ICustomReportGroup> SubGroups
        {
            get;
            private set;
        }

        /// <summary>
        /// List of reports.
        /// </summary>
        public IList<ICustomReport> Reports
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        public CustomReportGroup(XElement xmlElem)
        {
            ParseConfigReportGroupAttributes(xmlElem);

            // Process the sub-groups
            SubGroups = new List<ICustomReportGroup>();
            foreach (XElement groupElem in xmlElem.Elements(ELEM_GROUP))
            {
                SubGroups.Add(new CustomReportGroup(groupElem));
            }

            // Process the reports in this group
            Reports = new List<ICustomReport>();
            foreach (XElement reportElem in xmlElem.Elements(ELEM_REPORT))
            {
                Reports.Add(new CustomReport(reportElem));
            }
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseConfigReportGroupAttributes(XElement xmlElem)
        {
            foreach (XAttribute xmlGroupAttr in xmlElem.Attributes())
            {
                if (0 == String.Compare(ATTR_NAME, xmlGroupAttr.Name.LocalName))
                {
                    Name = xmlGroupAttr.Value.Trim();
                }
                else
                {
                    ReportsConfig.Log.Warning("Unknown report group attribute: '{0}'.", xmlGroupAttr.Name.LocalName);
                }
            }
        }
        #endregion // Private Methods
    } // CustomReportGroup
}
