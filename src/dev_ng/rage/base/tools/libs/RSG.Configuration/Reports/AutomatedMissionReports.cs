﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using RSG.Configuration.Implementation;

namespace RSG.Configuration.Reports
{
    /// <summary>
    /// 
    /// </summary>
    internal class AutomatedMissionReports : ConfigBase, IAutomatedMissionReports
    {
        #region Constants
        internal const String ELEM_MISSIONS = "missions";
        internal const String ELEM_GROUP = "group";

        internal const String ATTR_MISSIONS_DIR = "missions_dir";
        internal const String ATTR_MISSIONS_REGEX = "stats_file_regex";
        internal const String ATTR_GROUP_NAME = "dir";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Reference to the owning automated reports config object.
        /// </summary>
        private IAutomatedReports AutomatedReports
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string MissionsDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string StatsFileRegexString
        {
            get;
            private set;
        }

        /// <summary>
        /// Path to the depot file that contains all the stats for per build tests
        /// </summary>
        public IList<string> PlaythroughGroups
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public AutomatedMissionReports(IAutomatedReports automatedReports)
        {
            AutomatedReports = automatedReports;
            PlaythroughGroups = new List<string>();
            Environment = ConfigFactory.CreateEnvironment();
        }

        /// <summary>
        /// 
        /// </summary>
        public AutomatedMissionReports(IAutomatedReports automatedReports, XElement xmlElem)
            : this(automatedReports)
        {
            ParseConfig(xmlElem);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseConfig(XElement xmlElem)
        {
            // Read in the attributes first
            string tempStr = ReadAttribute<String>(xmlElem, ATTR_MISSIONS_DIR);
            MissionsDirectory = Path.GetFullPath(AutomatedReports.Environment.Subst(tempStr));
            Environment.Add(ATTR_MISSIONS_DIR, MissionsDirectory);

            StatsFileRegexString = ReadAttribute<String>(xmlElem, ATTR_MISSIONS_REGEX);

            // Retrieve the playthrough names next
            PlaythroughGroups.Clear();

            foreach (XAttribute attr in ((IEnumerable<object>)xmlElem.XPathEvaluate(String.Format("{0}/@{1}", ELEM_GROUP, ATTR_GROUP_NAME))).OfType<XAttribute>())
            {
                PlaythroughGroups.Add(Path.GetFullPath(Environment.Subst(attr.Value)));
            }
        }
        #endregion // Private Methods
    } // AutomatedMissionReports
}
