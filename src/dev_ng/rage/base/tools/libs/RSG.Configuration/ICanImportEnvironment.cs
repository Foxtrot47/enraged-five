﻿//---------------------------------------------------------------------------------------------
// <copyright file="IHasEnvironment.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2011-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{

    /// <summary>
    /// 
    /// </summary>
    public interface ICanImportEnvironment
    {
        #region Methods
        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="env"></param>
        void Import(IEnvironment env);
        #endregion // Methods
    }

} // RSG.Configuration namespace
