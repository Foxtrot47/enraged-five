﻿//---------------------------------------------------------------------------------------------
// <copyright file="IRagConfig.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Net;

namespace RSG.Configuration.Rag
{
    /// <summary>
    /// Rag configuration interface.
    /// </summary>
    public interface IRagConfig
    {
        #region Properties
        /// <summary>
        /// Rag configuration XML file.
        /// </summary>
        string Filename { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a value indicating whether we should ignore connections from the
        /// specified IP address.
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        bool ShouldIgnoreConnectionsFrom(IPAddress address);
        #endregion
    }
}
