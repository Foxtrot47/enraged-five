﻿//---------------------------------------------------------------------------------------------
// <copyright file="RagConfig.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Configuration.Rag
{
    /// <summary>
    /// Rag configuration data.
    /// </summary>
    internal class RagConfig : IRagConfig
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filename"/> property.
        /// </summary>
        private readonly String _filename;

        /// <summary>
        /// List of ip addresses from which we should always ignore new game connections.
        /// </summary>
        private readonly IDictionary<string, IPAddress> _ignoredConnectionSources;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the <see cref="RagConfig"/> class using the specified
        /// log and project config objects.
        /// </summary>
        /// <param name="project"></param>
        public RagConfig(ILog log, IProject project)
        {
            _filename = Path.Combine(project.Config.ToolsConfigDirectory, "globals", "rag.meta");
            _ignoredConnectionSources = new Dictionary<string, IPAddress>();

            // Make sure the file exists.
            if (!File.Exists(_filename))
            {
                Debug.Fail(String.Format("Rag configuration file '{0}' does not exist.", _filename));
                throw new ConfigurationException("Rag configuration file does not exist.")
                {
                    Filename = _filename
                };
            }
            Parse(log, _filename);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Rag configuration XML file.
        /// </summary>
        public string Filename
        {
            get { return _filename; }
        }
        #endregion

        #region IRagConfig Methods
        /// <summary>
        /// Returns a value indicating whether we should ignore connections from the
        /// specified IP address.
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public bool ShouldIgnoreConnectionsFrom(IPAddress address)
        {
            return _ignoredConnectionSources.Values.Contains(address);
        }
        #endregion

        #region Serialisation Code
        /// <summary>
        /// Parses the passed in filename populating the list of available ports.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="filename">File to parse.</param>
        private void Parse(ILog log, String filename)
        {
            // Load the document and extract all ports.
            XDocument doc = XDocument.Load(filename);

            IEnumerable<XElement> ignoredConnectionElems =
                doc.XPathSelectElements("/RagConfiguration/IgnoredConnectionSources/Item");
            foreach (XElement connectionElem in ignoredConnectionElems)
            {
                XElement nameElem = connectionElem.Element("Name");
                if (nameElem == null)
                {
                    log.Warning("IgnoredConnectionSource item is missing the name sub-element and will be ignored.");
                    continue;
                }

                XElement ipAddressElem = connectionElem.Element("IPAddress");
                if (ipAddressElem == null)
                {
                    log.Warning("The '{0}' IgnoredConnectionSource item is missing the ip address sub-element and will be ignored.",
                        nameElem.Value);
                    continue;
                }

                IPAddress address;
                if (!IPAddress.TryParse(ipAddressElem.Value, out address))
                {
                    log.Warning("Unable to parse the ip address specified in the '{0}' IgnoredConnectionSource item.",
                        nameElem.Value);
                    continue;
                }

                _ignoredConnectionSources.Add(nameElem.Value, address);
            }
        }
        #endregion
    }
}
