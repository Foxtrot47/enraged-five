﻿//---------------------------------------------------------------------------------------------
// <copyright file="IHasEnvironment.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2011-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration
{
    using System;

    /// <summary>
    /// IHasEnvironment specifies that the object has an associated Environment
    /// object.
    /// </summary>
    /// <see cref="RSG.Configuration.ICanImportEnvironment"/>
    public interface IHasEnvironment
    {
        #region Properties
        /// <summary>
        /// Encapsulated Environment object.
        /// </summary>
        IEnvironment Environment { get; }
        #endregion // Properties
    }

} // RSG.Configuration namespace
