﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Installer
{

    /// <summary>
    /// Writable Project interface.  ONLY USED BY THE INSTALLER.
    /// </summary>
    public interface IWritableProject : IProject
    {
        #region User Local Data (Set by Installer)
        /// <summary>
        /// Locally configured default branch name for this project.
        /// </summary>
        String DefaultBranchName { get; set; }
        #endregion // User Local Data (Set by Installer)

        #region Methods
        /// <summary>
        /// Save local settings.
        /// </summary>
        /// <returns></returns>
        bool SaveLocal();
        #endregion // Methods
    }

} // RSG.Configuration.Installer namespace
