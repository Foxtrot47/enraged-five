﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Configuration.Installer
{

    /// <summary>
    /// Writable Configuration class for system-wide globals for current tools 
    /// branch.  ONLY USED BY THE INSTALLER.
    /// </summary>
    public interface IWritableConfig : IConfig
    {
        #region User Local Data (Set by Installer)
        /// <summary>
        /// Last install time.
        /// </summary>
        new DateTime InstallTime { get; set; }

        /// <summary>
        /// User's username.
        /// </summary>
        new String Username { get; set; }

        /// <summary>
        /// User's email address.
        /// </summary>
        new String EmailAddress { get; set; }
        
        /// <summary>
        /// Version control enabled flag.
        /// </summary>
        new bool VersionControlEnabled { get; set; }
        #endregion // User Local Data (Set by Installer)

        #region Methods
        /// <summary>
        /// Save local settings.
        /// </summary>
        /// <returns></returns>
        bool SaveLocal();
        #endregion // Methods
    }

} // RSG.Configuration.Installer namespace
