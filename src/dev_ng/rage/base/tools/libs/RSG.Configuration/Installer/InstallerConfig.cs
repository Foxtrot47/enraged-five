﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Configuration.Installer
{

    /// <summary>
    /// Installer configuration object.
    /// </summary>
    internal class InstallerConfig : IInstallerConfig
    {
        #region Properties
        /// <summary>
        /// Filename installer config has loaded.
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// Applications to install information.
        /// </summary>
        public IEnumerable<IApplicationInstall> Installs 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Installer runlines.
        /// </summary>
        public IEnumerable<IRunline> Runlines 
        { 
            get; 
            private set; 
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="project"></param>
        internal InstallerConfig(IProject project)
        {
            this.Filename = Path.Combine(project.Config.ToolsConfigDirectory, "installer.xml");
            if (File.Exists(this.Filename))
            {
                XDocument xmlDoc = XDocument.Load(this.Filename);
                Parse(xmlDoc);
            }
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        private void Parse(XDocument doc)
        {
            ICollection<IApplicationInstall> applications = new List<IApplicationInstall>();
            IEnumerable<XElement> xmlApplicationElems = doc.Root.XPathSelectElements(
                "/Installer/Applications/Application");
            foreach (XElement xmlApplicationElem in xmlApplicationElems)
            {
                applications.Add(new ApplicationInstall(xmlApplicationElem));
            }
            this.Installs = applications;

            // Installation Runlines
            ICollection<IRunline> runlines = new List<IRunline>();
            IEnumerable<XElement> xmlRunlineElems = doc.Root.XPathSelectElements(
                "/Installer/Runlines/Runline");
            foreach (XElement xmlRunlineElem in xmlRunlineElems)
            {
                runlines.Add(new Implementation.Runline(RunlineMode.Install, xmlRunlineElem));
            }
            this.Runlines = runlines;
        }
        #endregion // Private Methods
    }

} // RSG.Configuration.Installer namespace
