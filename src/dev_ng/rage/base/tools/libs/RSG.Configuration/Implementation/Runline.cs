﻿using System;
using System.Globalization;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Configuration.Implementation
{

	/// <summary>
	/// Runline implementation.
	/// </summary>
	internal class Runline : IRunline
	{
		#region Properties
		/// <summary>
		/// Runline installer mode.
		/// </summary>
		public RunlineMode Mode 
		{ 
			get;
			private set;
		}

		/// <summary>
		/// Command line to execute.
		/// </summary>
		public String Command
		{
			get;
			private set;
		}

		/// <summary>
		/// Whether to continue if the runline fails.
		/// </summary>
		public bool CanFail
		{
			get;
			private set;
		}

		/// <summary>
		/// Single run.
		/// </summary>
		public bool RunOnce
		{
			get;
			private set;
		}

		/// <summary>
		/// Always run in the installer (irrespective of version).
		/// </summary>
		public bool Always
		{
			get;
			private set;
		}

		/// <summary>
		/// Users mask for this runline.
		/// </summary>
		public uint Users
		{
			get;
			private set;
		}

		/// <summary>
		/// Version of config this runline was introduced.
		/// </summary>
		public int Version
		{
			get;
			private set;
		}
		#endregion // Properties

		#region Constructor(s)
		/// <summary>
		/// Constructor; from an XML element.
		/// </summary>
		/// <param name="xmlElem"></param>
		public Runline(RunlineMode mode, XElement xmlElem)
		{
            XElement xmlCommandElem = xmlElem.Element("Command");
            if (null != xmlCommandElem)
                this.Command = xmlCommandElem.Value;

            bool canFail = true;
            this.CanFail = true;
            XElement xmlCanFailElem = xmlElem.Element("CanFail");
            if (null != xmlCanFailElem && bool.TryParse(xmlCanFailElem.Value, out canFail))
                this.CanFail = canFail;

            bool always = true;
            this.Always = true;
            XElement xmlAlwaysElem = xmlElem.Element("Always");
            if (null != xmlAlwaysElem && bool.TryParse(xmlAlwaysElem.Value, out always))
                this.Always = always;

            bool once = false;
            this.RunOnce = false;
            XElement xmlRunOnceElem = xmlElem.Element("RunOnce");
            if (null != xmlRunOnceElem && bool.TryParse(xmlRunOnceElem.Value, out once))
                this.RunOnce = once;

            int version = 0;
            this.Version = 0;
            XElement xmlVersionElem = xmlElem.Element("Version");
            if (null != xmlVersionElem && int.TryParse(xmlVersionElem.Value, out version))
                this.Version = version;

            uint users = 0xFFFFFFFF;
            this.Users = 0xFFFFFFFF;
            XElement xmlUsersElem = xmlElem.Element("Users");
            if (null != xmlUsersElem && uint.TryParse(xmlUsersElem.Value, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out users))
                this.Users = users;
		}
		#endregion // Constructor(s)
	}

} // RSG.Configuration.Implementation namespace
