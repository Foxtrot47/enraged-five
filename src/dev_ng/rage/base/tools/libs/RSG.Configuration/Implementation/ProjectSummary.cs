﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectSummary.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    /// Project summary interface; for reading summary information about
    /// available projects when the configuration data may not be available.
    /// </summary>
    internal class ProjectSummary : 
        ConfigBase,
        IProjectSummary
    {
        #region Constants
        private const String xmlElementKey = "Key";
        private const String xmlElementFriendlyName = "FriendlyName";
        private const String xmlElementConfigurationFilename = "ConfigurationFilename";
        private const String xmlElementRootDirectory = "RootDirectory";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Project name.
        /// </summary>
        public String Key
        {
            get;
            private set;
        }

        /// <summary>
        /// Project friendly UI name.
        /// </summary>
        public String FriendlyName
        {
            get;
            private set;
        }

        /// <summary>
        /// Project root-directory (absolute path).
        /// </summary>
        public String RootDirectory
        {
            get { return (this.Environment.Subst(m_sRootDirectory)); }
            private set { m_sRootDirectory = value; }
        }
        private String m_sRootDirectory;

        /// <summary>
        /// Project configuration filename.
        /// </summary>
        public String ConfigurationFilename
        {
            get { return (this.Environment.Subst(m_sConfigurationFilename)); }
            private set { m_sConfigurationFilename = value; }
        }
        private String m_sConfigurationFilename;

        /// <summary>
        /// Project summary environment (not complete).
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        public ProjectSummary(XElement xmlElem)
        {
            XElement xmlNameElem = xmlElem.Element(xmlElementKey);
            if (null != xmlNameElem)
                this.Key = xmlNameElem.Value;
            XElement xmlFriendlyNameElem = xmlElem.Element(xmlElementFriendlyName);
            if (null != xmlFriendlyNameElem)
                this.FriendlyName = xmlFriendlyNameElem.Value;
            XElement xmlConfigurationFilenameElem = xmlElem.Element(xmlElementConfigurationFilename);
            if (null != xmlConfigurationFilenameElem)
                this.ConfigurationFilename = xmlConfigurationFilenameElem.Value;
            XElement xmlRootDirectoryElem = xmlElem.Element(xmlElementRootDirectory);
            if (null != xmlRootDirectoryElem)
                this.RootDirectory = xmlRootDirectoryElem.Value;

            this.Environment = new Environment();
            this.Environment.Add("name", this.Key);
            this.Environment.Add("root", this.RootDirectory);
        }
        #endregion // Constructor(s)
    }

} // RSG.Configuration.Implementation namespace
