﻿//---------------------------------------------------------------------------------------------
// <copyright file="Project.cs" company="Rockstar">
//     Copyright © Rockstar Games 2011-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using RSG.Base.Logging;

    /// <summary>
    /// Core project encapsulation; representing a project defining a set of
    /// branches and targets.
    /// </summary>
    internal class Project :
        ProjectBase,
        IProjectCore
    {
        #region Constants
        // ProjectSummaryCore XElement Name Constants
        internal static readonly String xmlElemRootDirectoryDlc = "RootDirectoryDLC";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Available Content Packs (DLC-projects).
        /// </summary>
        public IEnumerable<IProjectDLC> DLCProjects
        {
            get { return _dlcProjects; }
        }
        private ICollection<IProjectDLC> _dlcProjects;
        
        /// <summary>
        /// Project DLC root directory.
        /// </summary>
        public String RootDirectoryDLC
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sRootDirectoryDLC)); }
            private set { m_sRootDirectoryDLC = value; }
        }
        private String m_sRootDirectoryDLC;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config">Config object parent.</param>
        /// <param name="xmlProjectSummary"></param>
        public Project(ILog log, IConfig config, XElement xmlProjectSummary)
            : base(log, config, xmlProjectSummary)
        {
            if (null != xmlProjectSummary.Element(xmlElemRootDirectoryDlc))
                this.RootDirectoryDLC = xmlProjectSummary.Element(xmlElemRootDirectoryDlc).Value;
            this.Reload(log);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Reload all project configuration data.
        /// </summary>
        /// <param name="log"></param>
        public override void Reload(ILog log)
        {
            base.Reload(log);

            // Content Packs (DLC).
            this._dlcProjects = new List<IProjectDLC>();
            String contentPacksFilename = Path.Combine(Path.GetDirectoryName(this.Filename), "content_packs.meta");
            if (File.Exists(contentPacksFilename))
            {
                XDocument xmlDocContentPacks = XDocument.Load(contentPacksFilename,
                    LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
                ParseContentPacks(log, xmlDocContentPacks);
            }

            // Reconstruct environment.
            this.Import(this.Environment);
        }
        #endregion // Controller Methods

        #region Object Overrides
        /// <summary>
        /// Return simple string representation of this Project.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("{0}: {1}", this.Name, this.FriendlyName));
        }
        #endregion // Object Overrides

        #region Private Methods
        /// <summary>
        /// Parse content packs configuration data elements.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlDocContentPacks"></param>
        private void ParseContentPacks(ILog log, XDocument xmlDocContentPacks)
        {
            foreach (XElement xmlContentPack in 
                xmlDocContentPacks.XPathSelectElements("/ContentPacksFile/Packs/Item"))
            {
                IProjectDLC project = new ProjectDLC(log, this, xmlContentPack);
                this._dlcProjects.Add(project);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Configuration.Implementation namespace
