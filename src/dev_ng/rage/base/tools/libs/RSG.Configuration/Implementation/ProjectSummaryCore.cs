﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectSummary.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    /// Project summary interface; for reading summary information about
    /// available projects when the configuration data may not be available.
    /// </summary>
    internal class ProjectSummaryCore :
        ProjectSummary,
        IProjectSummaryCore
    {
        #region Constants
        private const String xmlElementRootDirectoryDlc = "RootDirectoryDLC";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Project root-directory DLC (absolute path).
        /// </summary>
        public String RootDirectoryDLC
        {
            get { return (this.Environment.Subst(m_sRootDirectoryDLC)); }
            private set { m_sRootDirectoryDLC = value; }
        }
        private String m_sRootDirectoryDLC;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        public ProjectSummaryCore(XElement xmlElem)
            : base(xmlElem)
        {
            XElement xmlRootDirectoryElem = xmlElem.Element(xmlElementRootDirectoryDlc);
            if (null != xmlRootDirectoryElem)
                this.RootDirectoryDLC = xmlRootDirectoryElem.Value;
        }
        #endregion // Constructor(s)
    }

} // RSG.Configuration.Implementation namespace
