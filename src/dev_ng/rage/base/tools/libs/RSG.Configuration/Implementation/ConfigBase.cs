﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConfigBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2011-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using RSG.Base.Logging;

    /// <summary>
    /// Base configuration class that contains common functions that all config base
    /// classes may need (such as attribute parsing methods).
    /// </summary>
    internal abstract class ConfigBase
    {
        #region Methods
        /// <summary>
        /// Read an attribute value from an XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <param name="attr"></param>
        /// <param name="value"></param>
        protected void GetAttrValue(XElement xmlElem, String attr, ref String value)
        {
            XAttribute xmlAttr = xmlElem.Attribute(attr);
            if (null != xmlAttr && !String.IsNullOrEmpty(xmlAttr.Value))
                value = xmlAttr.Value.Trim();
        }

        /// <summary>
        /// Read attribute of a particular type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="elem"></param>
        /// <param name="attr"></param>
        protected T ReadAttribute<T>(XElement elem, String attr)
        {
            if (elem.Attribute(attr) == null)
            {
                throw new ConfigurationException(String.Format("Unable to read the '{0}' attribute under the '{1}' element.", attr, elem.Name));
            }

            return (T)Convert.ChangeType(elem.Attribute(attr).Value, typeof(T));
        }
        #endregion // Methods
    } // ConfigBase
} // RSG.Configuration.Implementation namespace
