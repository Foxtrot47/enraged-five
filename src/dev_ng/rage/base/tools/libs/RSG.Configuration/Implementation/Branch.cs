﻿//---------------------------------------------------------------------------------------------
// <copyright file="Branch.cs" company="Rockstar">
//     Copyright © Rockstar Games 2011-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using RSG.Base.Logging;
    using RSG.Platform;

    /// <summary>
    /// Branch encapsulation; representing a project branch defining a set of
    /// paths and targets.
    /// </summary>
    internal class Branch :
        ConfigBase,
        IBranch
    {
        #region Constants
        private const String CORE_BRANCH_PREFIX = "core_";

        private const String ELEM_BRANCH = "branch";
        private const String ELEM_TARGETS = "targets";
        private const String ELEM_BUILDLABEL = "buildlabel";
        private const String ELEM_RAGE = "rage";
        private const String ELEM_RAGEBUILDERS = "ragebuilders";

        private const String ATTR_NAME = "name";
        private const String ATTR_ART = "art";
        private const String ATTR_ANIM = "anim";
        private const String ATTR_ASSETS = "assets";
        private const String ATTR_EXPORT = "export";
        private const String ATTR_PROCESSED = "processed";
        private const String ATTR_METADATA = "metadata";
        private const String ATTR_DEFINITIONS = "definitions";
        private const String ATTR_BUILD = "build";
        private const String ATTR_COMMON = "common";
        private const String ATTR_SHADERS = "shaders";
        private const String ATTR_CODE = "code";
        private const String ATTR_RAGECODE = "ragecode";
        private const String ATTR_PREVIEW = "preview";
        private const String ATTR_SCRIPT = "script";
        private const String ATTR_AUDIO = "audio";
        private const String ATTR_FRAGTUNE = "tune";
        private const String ATTR_TEXT = "text";

        private const string XPathTargets = "targets/target";
        private const string XPathCommonArgs = "Rage/Ragebuilders/CommonArgs";
        private const string XPathRagebuilder = "Rage/Ragebuilders/Ragebuilder";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Project container for this Branch object.
        /// </summary>
        public IProject Project
        {
            get;
            private set;
        }

        /// <summary>
        /// Branch name ("dev", "release" etc).
        /// </summary>
        public String Name
        {
            get { return m_sName; }
            private set { m_sName = value; }
        }
        private String m_sName;

        /// <summary>
        /// Branch locked state (e.g. flagged after branch is released or locked for future edits).
        /// </summary>
        public bool Locked
        {
            get;
            private set;
        }

        /// <summary>
        /// Branch environment; inheriting from Project and Config.
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }

        /// <summary>
        /// Art root directory path.
        /// </summary>
        public String Art
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sArt)); }
            private set { m_sArt = value; }
        }
        private String m_sArt;

        /// <summary>
        /// Anim root directory path.
        /// </summary>
        public String Anim
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sAnim)); }
            private set { m_sAnim = value; }
        }
        private String m_sAnim;

        /// <summary>
        /// Assets root directory path.
        /// </summary>
        public String Assets
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sAssets)); }
            private set { m_sAssets = value; }
        }
        private String m_sAssets;

        /// <summary>
        /// Root content-tree filename for this branch.
        /// </summary>
        public String Content
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sContent)); }
            private set { m_sContent = value; }
        }
        private String m_sContent;

        /// <summary>
        /// Export root directory path.
        /// </summary>
        public String Export
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sExport)); }
            private set { m_sExport = value; }
        }
        private String m_sExport;

        /// <summary>
        /// Processed root directory path.
        /// </summary>
        public String Processed
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sProcessed)); }
            private set { m_sProcessed = value; }
        }
        private String m_sProcessed;

        /// <summary>
        /// Metadata root directory path.
        /// </summary>
        public String Metadata
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sMetadata)); }
            private set { m_sMetadata = value; }
        }
        private String m_sMetadata;

        /// <summary>
        /// Definitions root directory path.
        /// </summary>
        public String Definitions
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sDefinitions)); }
            private set { m_sDefinitions = value; }
        }
        private String m_sDefinitions;

        /// <summary>
        /// Audio root directory path.
        /// </summary>
        public String Audio
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sAudio)); }
            private set { m_sAudio = value; }
        }
        private String m_sAudio;

        /// <summary>
        /// Build root directory path.
        /// </summary>
        public String Build
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sBuild)); }
            private set { m_sBuild = value; }
        }
        private String m_sBuild;

        /// <summary>
        /// Common root directory path.
        /// </summary>
        public String Common
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sCommon)); }
            private set { m_sCommon = value; }
        }
        private String m_sCommon;

        /// <summary>
        /// Shaders root directory path.
        /// </summary>
        public String Shaders
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sShaders)); }
            private set { m_sShaders = value; }
        }
        private String m_sShaders;

        /// <summary>
        /// Code root directory path.
        /// </summary>
        public String Code
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sCode)); }
            private set { m_sCode = value; }
        }
        private String m_sCode;

        /// <summary>
        /// RAGE Code root directory path.
        /// </summary>
        public String RageCode
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sRageCode)); }
            private set { m_sRageCode = value; }
        }
        private String m_sRageCode;

        /// <summary>
        /// Preview root directory path.
        /// </summary>
        public String Preview
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sPreview)); }
            private set { m_sPreview = value; }
        }
        private String m_sPreview;

        /// <summary>
        /// Script root directory path.
        /// </summary>
        public String Script
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sScript)); }
            private set { m_sScript = value; }
        }
        private String m_sScript;

        /// <summary>
        /// Fragment tuning root directory path.
        /// </summary>
        public String FragmentTune
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sFragmentTune)); }
            private set { m_sFragmentTune = value; }
        }
        private String m_sFragmentTune;

        /// <summary>
        /// Text export root directory path.
        /// </summary>
        public String Text
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sText)); }
            private set { m_sText = value; }
        }
        private String m_sText;

        /// <summary>
        /// Available Branch targets.
        /// </summary>
        public IDictionary<RSG.Platform.Platform, ITarget> Targets
        {
            get;
            private set;
        }

        /// <summary>
        /// Platform conversion tools per-platform (e.g. Ragebuilder).
        /// </summary>
        public IDictionary<RSG.Platform.Platform, IPlatformConversionTool> PlatformConversionTools
        {
            get;
            private set;
        }

        /// <summary>
        /// Branch build label (optional).
        /// </summary>
        public Label BuildLabel
        {
            get;
            private set;
        }

        /// <summary>
        /// Ragebuilder common arguments
        /// </summary>
        /// Common Arguments are regrouped under the "CommonArgs" block. Those are shared by all platforms.
        public String RageBuilderCommonArgs 
        { 
            get; 
            private set; 
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="project"></param>
        /// <param name="xmlElem"></param>
        internal Branch(ILog log, IProject project, XElement xmlElem)
        {
            this.Project = project;
            this.Environment = new Environment(this.Project);
            this.Targets = new Dictionary<RSG.Platform.Platform, ITarget>();
            this.PlatformConversionTools = new Dictionary<RSG.Platform.Platform, IPlatformConversionTool>();
            ParseBranch(log, xmlElem, false);

            // Reconstruct environment.
            this.Import(this.Environment);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return whether the passed in filename is from a particular branch's
        /// prefix directory.
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        /// E.g.
        ///   branch.IsRootedPath(branch.Export, @"x:\file\asset1.txt")
        ///   
        public bool IsRootedPath(String prefix, String filename)
        {
            String normalisedPrefix = Path.GetFullPath(prefix);
            String normalisedPath = Path.GetFullPath(filename);
            return (normalisedPath.StartsWith(normalisedPrefix, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="environment"></param>
        public void Import(IEnvironment environment)
        {
            this.Project.Import(environment);
            this.Import(environment, String.Empty);

            // If we have a core branch (we should) then import that too.  Need to
            // protect against importing environment during initial load.
            // DHM FIX ME: several core projects?  How to handle.
            IProject coreProject = this.Project.Config.Project;
            if (coreProject is Project)
            {
                if (!coreProject.Branches.ContainsKey(this.Name))
                {
                    Debug.Fail(String.Format("Core-project doesn't contain core-branch: {0}.", this.Name));
                    throw new ConfigurationException(String.Format("Core-project doesn't contain core-branch: {0}.", this.Name))
                        {
                            Filename = Project.Filename
                        };
                }

                Branch coreBranch = (Branch)coreProject.Branches[this.Name];
                coreBranch.Import(environment, CORE_BRANCH_PREFIX);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        public void ParseLocal(ILog log, XElement xmlElem)
        {
            foreach (XElement xmlTargetElem in xmlElem.XPathSelectElements("targets/target"))
            {
                ParseBranchTarget(log, xmlTargetElem, true);
            }
        }
        #endregion // Controller Methods
        
        #region Private Methods
        /// <summary>
        /// Parse branch element data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseBranch(ILog log, XElement xmlElem, bool local)
        {
            ParseBranchAttributes(log, xmlElem, local);
            // Import environment for target resolution; popped at end.
            this.Environment.Push();
            this.Import(this.Environment);
            
            XElement xmlLockedElem = xmlElem.Element("Locked");
            if (null != xmlLockedElem && null != xmlLockedElem.Attribute("value"))
            {
                XAttribute xmlLockedValueAttr = xmlLockedElem.Attribute("value");
                bool lockedState = false;
                if (null != xmlLockedElem.Value && bool.TryParse(xmlLockedValueAttr.Value, out lockedState))
                    this.Locked = lockedState;
                else
                    this.Locked = false;
            }

            XElement xmlBuildLabelElem = xmlElem.XPathSelectElement(ELEM_BUILDLABEL);
            if (null != xmlBuildLabelElem)
            {
                // DHM FIX ME: build label init.
            }

            foreach (XElement xmlTargetElem in xmlElem.XPathSelectElements(XPathTargets))
            {
                ParseBranchTarget(log, xmlTargetElem, local);
            }

            // populating branch's ragebuilder common option arguments
            XElement commonArgs = xmlElem.XPathSelectElement(XPathCommonArgs);
            if (commonArgs != null)
            {
                RageBuilderCommonArgs = commonArgs.Value;
            }

            foreach (XElement xmlRagebuilderElem in xmlElem.XPathSelectElements(XPathRagebuilder))
            {
                ParseConversionTool(log, xmlRagebuilderElem, local);
            }

            XElement xmlContentElem = xmlElem.Element("content");
            if (null != xmlContentElem)
            {
                this.m_sContent = xmlContentElem.Value.Trim();
            }

            this.Environment.Pop();
        }

        /// <summary>
        /// Parse branch element attribute data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseBranchAttributes(ILog log, XElement xmlElem, bool local)
        {
            base.GetAttrValue(xmlElem, ATTR_NAME, ref this.m_sName);
            base.GetAttrValue(xmlElem, ATTR_ART, ref this.m_sArt);
            base.GetAttrValue(xmlElem, ATTR_ANIM, ref this.m_sAnim);
            base.GetAttrValue(xmlElem, ATTR_ASSETS, ref this.m_sAssets);
            base.GetAttrValue(xmlElem, ATTR_EXPORT, ref this.m_sExport);
            base.GetAttrValue(xmlElem, ATTR_PROCESSED, ref this.m_sProcessed);
            base.GetAttrValue(xmlElem, ATTR_METADATA, ref this.m_sMetadata);
            base.GetAttrValue(xmlElem, ATTR_DEFINITIONS, ref this.m_sDefinitions);
            base.GetAttrValue(xmlElem, ATTR_BUILD, ref this.m_sBuild);
            base.GetAttrValue(xmlElem, ATTR_COMMON, ref this.m_sCommon);
            base.GetAttrValue(xmlElem, ATTR_SHADERS, ref this.m_sShaders);
            base.GetAttrValue(xmlElem, ATTR_CODE, ref this.m_sCode);
            base.GetAttrValue(xmlElem, ATTR_RAGECODE, ref this.m_sRageCode);
            base.GetAttrValue(xmlElem, ATTR_PREVIEW, ref this.m_sPreview);
            base.GetAttrValue(xmlElem, ATTR_SCRIPT, ref this.m_sScript);
            base.GetAttrValue(xmlElem, ATTR_AUDIO, ref this.m_sAudio);
            base.GetAttrValue(xmlElem, ATTR_FRAGTUNE, ref this.m_sFragmentTune);
            base.GetAttrValue(xmlElem, ATTR_TEXT, ref this.m_sText);
        }

        /// <summary>
        /// Parse branch target element data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseBranchTarget(ILog log, XElement xmlElem, bool local)
        {
            if (local)
            {
                // We don't define new Targets from the local configuration
                // data; only picking up the Enabled flag.
                XAttribute xmlPlatformAttr = xmlElem.Attributes().FirstOrDefault(attr => Target.ATTR_PLATFORM == attr.Name.LocalName);
                String platform = (xmlPlatformAttr != null) ? xmlPlatformAttr.Value : String.Empty;
                if (String.IsNullOrEmpty(platform))
                {
                    log.Warning(xmlElem, "Local target has no platform attribute.");
                }
                else
                {
                    RSG.Platform.Platform p = PlatformUtils.PlatformFromString(platform);
                    // Find target; for platform.
                    Debug.Assert(this.Targets.ContainsKey(p),
                        String.Format("Branch '{0}' does not define Target Platform '{1}'.", this.Name, platform));
                    if (!this.Targets.ContainsKey(p))
                    {
                        log.Warning("Branch '{0}' does not define Target Platform {1}.", 
                            this.Name, platform);
                    }
                    else
                    {
                        XAttribute xmlEnabledAttr = xmlElem.Attributes().FirstOrDefault(attr => Target.ATTR_ENABLED == attr.Name.LocalName);
                        bool enabled = xmlEnabledAttr != null && bool.Parse(xmlEnabledAttr.Value);
                        this.Targets[p].Enabled = enabled;
                    }
                }
            }
            else
            {
                // We only define new Targets from the non-local configuration
                // data.
                ITarget target = new Target(log, this, xmlElem);
                Debug.Assert(!this.Targets.ContainsKey(target.Platform),
                    String.Format("Duplicate Target name key: {0}.  Ignored", target.Platform));
                if (!this.Targets.ContainsKey(target.Platform))
                    this.Targets.Add(target.Platform, target);
                else
                    log.Warning(xmlElem, "Duplicate target name key: '{0}'.  Ignored.", 
                        target.Platform);
            }
        }

        /// <summary>
        /// Parse branch conversion tool element data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseConversionTool(ILog log, XElement xmlElem, bool local)
        {
            Debug.Assert(!local, "Platform conversion tools are not supported in local XML data.");
            if (local)
            {
                log.Warning(xmlElem,
                    "Platform conversion tools are not supported in local XML data.");
                return;
            }
            IPlatformConversionTool tool = new RageConversionTool(log, this, xmlElem);
            if (this.PlatformConversionTools.ContainsKey(tool.Platform))
            {
                Debug.Fail(String.Format("Already have a conversion tool for platform: {0}.  Ignoring.", tool.Platform));
                throw new ConfigurationException(String.Format("Already have a conversion tool for platform: {0}.  Ignoring.", tool.Platform))
                    {
                        Filename = Project.Filename
                    };
            }
            
            this.PlatformConversionTools.Add(tool.Platform, tool);
        }

        /// <summary>
        /// Import settings implementation; taking prefix to handle "core" settings
        /// and active variants.
        /// </summary>
        /// <param name="environment"></param>
        /// <param name="prefix"></param>
        private void Import(IEnvironment environment, String prefix)
        {
            String artDir = this.m_sArt.Replace("$(", String.Format("$({0}", prefix));
            String animDir = this.m_sAnim.Replace("$(", String.Format("$({0}", prefix));
            String assetsDir = this.m_sAssets.Replace("$(", String.Format("$({0}", prefix));
            String exportDir = this.m_sExport.Replace("$(", String.Format("$({0}", prefix));
            String processedDir = this.m_sProcessed.Replace("$(", String.Format("$({0}", prefix));
            String metadataDir = this.m_sMetadata.Replace("$(", String.Format("$({0}", prefix));
            String definitionsDir = this.m_sDefinitions.Replace("$(", String.Format("$({0}", prefix));
            String audioDir = this.m_sAudio.Replace("$(", String.Format("$({0}", prefix));
            String buildDir = this.m_sBuild.Replace("$(", String.Format("$({0}", prefix));
            String commonDir = this.m_sCommon.Replace("$(", String.Format("$({0}", prefix));
            String shadersDir = this.m_sShaders.Replace("$(", String.Format("$({0}", prefix));
            String codeDir = this.m_sCode.Replace("$(", String.Format("$({0}", prefix));
            String rageCodeDir = this.m_sRageCode.Replace("$(", String.Format("$({0}", prefix));
            String previewDir = this.m_sPreview.Replace("$(", String.Format("$({0}", prefix));
            String scriptDir = this.m_sScript.Replace("$(", String.Format("$({0}", prefix));
            String textDir = this.m_sText.Replace("$(", String.Format("$({0}", prefix));
            String fragTuneDir = this.m_sFragmentTune.Replace("$(", String.Format("$({0}", prefix));

            environment.Add(String.Format("{0}branch", prefix), this.m_sName);
            environment.Add(String.Format("{0}art", prefix), artDir);
            environment.Add(String.Format("{0}anim", prefix), animDir);
            environment.Add(String.Format("{0}assets", prefix), assetsDir);
#pragma warning disable 0618
            // For Texture Pipeline TCL/TCS files.
            environment.Add(String.Format("{0}RS_ASSETS", prefix), assetsDir, BracketType.AlternateCurly); 
#pragma warning restore 0618
            environment.Add(String.Format("{0}export", prefix), exportDir);
            environment.Add(String.Format("{0}processed", prefix), processedDir);
            environment.Add(String.Format("{0}metadata", prefix), metadataDir);
            environment.Add(String.Format("{0}definitions", prefix), definitionsDir);
            environment.Add(String.Format("{0}audio", prefix), audioDir);
            environment.Add(String.Format("{0}build", prefix), buildDir);
            environment.Add(String.Format("{0}common", prefix), commonDir);
            environment.Add(String.Format("{0}shaders", prefix), shadersDir);
            environment.Add(String.Format("{0}code", prefix), codeDir);
            environment.Add(String.Format("{0}ragecode", prefix), rageCodeDir);
            environment.Add(String.Format("{0}preview", prefix), previewDir);
            environment.Add(String.Format("{0}script", prefix), scriptDir);
            environment.Add(String.Format("{0}text", prefix), textDir);
            environment.Add(String.Format("{0}fragtune", prefix), fragTuneDir);
        }
        #endregion // Private Methods
    }

} // RSG.Configuration.Implementation namespace
