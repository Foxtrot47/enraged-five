﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Configuration.Implementation
{
    /// <summary>
    /// 
    /// </summary>
    internal class ParameterList : Dictionary<string, object>, IParameterList
    {
        #region Constants
        internal const string ELEM_PARAM = "param";

        internal const string ATTR_NAME  = "name";
        internal const string ATTR_TYPE  = "type";
        internal const string ATTR_VALUE = "value";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        public ParameterList(XElement xmlElem)
            : this(xmlElem, new Environment())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        public ParameterList(XElement xmlElem, IEnvironment environment)
        {
            foreach (XElement paramElem in xmlElem.Elements(ELEM_PARAM))
            {
                ParseParam(paramElem, environment);
            }
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="elem"></param>
        private void ParseParam(XElement elem, IEnvironment environment)
        {
            // Get the attributes we are after
            XAttribute nameAtt = elem.Attribute(ATTR_NAME);
            XAttribute typeAtt = elem.Attribute(ATTR_TYPE);
            XAttribute valueAtt = elem.Attribute(ATTR_VALUE);

            // Make sure we were provided with all the attributes.
            Debug.Assert(nameAtt != null, "Name attribute missing for parameter element.");
            Debug.Assert(typeAtt != null, "Type attribute missing for parameter element.");
            Debug.Assert(valueAtt != null, "Value attribute missing for parameter element.");
            if (nameAtt == null || typeAtt == null || valueAtt == null)
            {
                throw new ConfigurationException("Encountered a parameter that doesn't have name, type and/or value attributes set.");
            }

            String name = nameAtt.Value.Trim();

            // Make sure we don't have any duplicate 
            if (!ContainsKey(name))
            {
                String valueStr = environment.Subst(valueAtt.Value.Trim());
                object value = ParseParamValue(valueStr, typeAtt.Value.Trim());
                Add(name, value);
            }
            else
            {
                throw new ConfigurationException(String.Format("Parameter with name '{0}' already exists in the parameter list.", nameAtt.Value.Trim()));
            }
        }

        /// <summary>
        /// Converts the string representation of a parameter into a typed object.
        /// </summary>
        private object ParseParamValue(String value, String type)
        {
            switch (type)
            {
                case "bool":
                    return Boolean.Parse(value);

                case "int":
                    return Int32.Parse(value);

                case "uint":
                    return UInt32.Parse(value);

                case "long":
                    return Int64.Parse(value);

                case "ulong":
                    return UInt64.Parse(value);

                case "float":
                    return Single.Parse(value);

                case "double":
                    return Double.Parse(value);

                case "string":
                    return value;

                default:
                    throw new ConfigurationException(String.Format("Unsupported parameter type encountered.  Type: {0}, Value: {1}", type, value));
            }
        }
        #endregion // Private Methods
    } // ParameterList
}
