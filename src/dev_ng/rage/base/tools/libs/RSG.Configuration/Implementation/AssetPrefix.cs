﻿using System;
using System.Diagnostics;
using System.Xml.Linq;

namespace RSG.Configuration.Implementation
{

    /// <summary>
    /// Asset prefixes for the pipeline and all tools.
    /// </summary>
    internal class AssetPrefix : IAssetPrefix
    {
        #region Properties
        /// <summary>
        /// Project name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether project is DLC.
        /// </summary>
        public bool IsDLC
        {
            get; 
            private set;
        }

        /// <summary>
        /// Asset prefix string (default: empty for non-DLC, project-name for DLC).
        /// </summary>
        public String MapPrefix
        {
            get
            {
                if (null == _mapPrefix && this.IsDLC)
                    return (this.Name); // DLC-default
                else if (null == _mapPrefix && !this.IsDLC)
                    return (String.Empty); // !DLC-default
                return (_mapPrefix);
            }
        }
        private readonly String _mapPrefix;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor (for compatibility where XML data doesn't exist).
        /// </summary>
        /// <param name="project"></param>
        public AssetPrefix(IProject project)
        {
            this.Name = project.Name;
            this.IsDLC = (project is IProjectDLC);
            this._mapPrefix = null;
        }

        /// <summary>
        /// Constructor (overriding map prefix).
        /// </summary>
        /// <param name="project"></param>
        /// <param name="mapPrefix"></param>
        public AssetPrefix(IProject project, String mapPrefix)
            : this(project)
        {
            this._mapPrefix = mapPrefix;
        }

        /// <summary>
        /// Constructor (from XML data).
        /// </summary>
        /// <param name="project"></param>
        /// <param name="xmlElem"></param>
        public AssetPrefix(IProjectCore project, XElement xmlElem)
            : this(project)
        {
            XElement xmlMapPrefix = xmlElem.Element("MapPrefix");
            if (null != xmlMapPrefix)
                this._mapPrefix = xmlMapPrefix.Value;
        }
        #endregion // Constructor(s)
    }

} // RSG.Configuration.Implementation namespace
