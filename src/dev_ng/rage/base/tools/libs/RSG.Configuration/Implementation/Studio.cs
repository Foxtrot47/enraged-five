﻿//---------------------------------------------------------------------------------------------
// <copyright file="Studio.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using System.Diagnostics;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using RSG.Base.Logging;

    /// <summary>
    /// Studio object; representing a studio option for a user in our 
    /// configuration data.
    /// </summary>
    internal class Studio : IStudio
    {
        #region Constants
        private const String ELEM_NAME = "Key";
        private const String ELEM_FRIENDLY = "FriendlyName";
        private const String ELEM_SUBNET = "SubnetMask";
        private const String ELEM_DOMAIN = "Domain";
        private const String ELEM_PERFORCE = "PerforceServer";
        private const String ELEM_PERFORCEWEB = "PerforceWebServer";
        private const String ELEM_NETWORKDRIVE = "NetworkDrive";
        private const String ELEM_ACTIVEDIRECTORYDOMAIN = "ActiveDirectoryDomain";
        private const String ELEM_EXCHANGESERVER = "ExchangeServer";
        private const String ELEM_BUILDERUSER = "AutomatedUsername";
        private const String ELEM_BUILDERNAME = "AutomatedFriendlyName";
        private const String ELEM_TIMEZONEID = "TimeZoneId";
        private const String ELEM_PERFORCE_SWARM = "PerforceSwarmServer";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Studio key name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Studio friendly name (for UI display).
        /// </summary>
        public String FriendlyName
        {
            get;
            private set;
        }

        /// <summary>
        /// Network subnet range (e.g. "10.11.16.0/20").
        /// </summary>
        public String SubnetRange
        {
            get;
            private set;
        }

        /// <summary>
        /// Default Perforce server and port for the studio.
        /// </summary>
        public String DefaultPerforceServer 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Perforce web server.
        /// </summary>
        public String PerforceWebServer
        {
            get;
            private set;
        }

        /// <summary>
        /// Perforce swarm web address
        /// </summary>
        public String PerforceSwarmServer
        {
            get;
            private set;
        }

        /// <summary>
        /// Shared network drive resource.
        /// </summary>
        public String NetworkDrive
        {
            get;
            private set;
        }

        /// <summary>
        /// Exchange server URI.
        /// </summary>
        public String ExchangeServer
        {
            get;
            private set;
        }

        /// <summary>
        /// Studio domain URI.
        /// </summary>
        public String Domain
        {
            get;
            private set;
        }

        /// <summary>
        /// Studio Active Directory domain (for login authentication).
        /// </summary>
        public String ActiveDirectoryDomain 
        { 
            get;
            private set;
        }

        /// <summary>
        /// User name of automated builder user.
        /// </summary>
        public String BuilderUser
        {
            get;
            private set;
        }

        /// <summary>
        /// Friendly name of automated builder user.
        /// </summary>
        public String BuilderName
        {
            get;
            private set;
        }

        /// <summary>
        /// Time zone identifier (based on Studio location).
        /// </summary>
        public String TimeZoneId
        { 
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlStudioElem"></param>
        public Studio(ILog log, XElement xmlStudioElem)
        {
            ParseStudioElement(log, xmlStudioElem);
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Return TimeZoneInfo object for this studio.
        /// </summary>
        /// <returns></returns>
        public TimeZoneInfo GetTimeZoneInfo()
        {
            if (String.IsNullOrEmpty(this.TimeZoneId))
                throw (new ConfigurationException("TimeZoneId not set for studio."));

            try
            {
                TimeZoneInfo tmz = TimeZoneInfo.FindSystemTimeZoneById(this.TimeZoneId);
                return (tmz);
            }
            catch (InvalidTimeZoneException ex)
            {
                throw (new ConfigurationException(String.Format("Invalid studio TimeZoneId ('{0}').", this.TimeZoneId), ex));
            }
            catch (TimeZoneNotFoundException ex)
            {
                throw (new ConfigurationException(String.Format("Invalid studio TimeZoneId ('{0}').", this.TimeZoneId), ex));
            }
        }
        #endregion // Methods

        #region Object Overridden Methods
        /// <summary>
        /// Determine whether two Studio objects are equal (by key name).
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (!(obj is IStudio))
                return (false);

            IStudio other = (IStudio)obj;
            return (String.Equals(this.Name, other.Name));
        }

        /// <summary>
        /// Display string representation of Studio.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (this.FriendlyName);
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (this.Name.GetHashCode());
        }
        #endregion // Object Overridden Methods

        #region Private Methods
        /// <summary>
        /// Parse studio attributes from an XElement.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        private void ParseStudioElement(ILog log, XElement xmlStudioElem)
        {
            foreach (XElement xmlElem in xmlStudioElem.Elements())
            {
                if (String.Equals(ELEM_NAME, xmlElem.Name.LocalName))
                {
                    this.Name = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_FRIENDLY, xmlElem.Name.LocalName))
                {
                    this.FriendlyName = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_SUBNET, xmlElem.Name.LocalName))
                {
                    this.SubnetRange = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_PERFORCE, xmlElem.Name.LocalName))
                {
                    this.DefaultPerforceServer = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_PERFORCEWEB, xmlElem.Name.LocalName))
                {
                    this.PerforceWebServer = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_NETWORKDRIVE, xmlElem.Name.LocalName))
                {
                    this.NetworkDrive = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_EXCHANGESERVER, xmlElem.Name.LocalName))
                {
                    this.ExchangeServer = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_DOMAIN, xmlElem.Name.LocalName))
                {
                    this.Domain = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_ACTIVEDIRECTORYDOMAIN, xmlElem.Name.LocalName))
                {
                    this.ActiveDirectoryDomain = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_BUILDERUSER, xmlElem.Name.LocalName))
                {
                    this.BuilderUser = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_BUILDERNAME, xmlElem.Name.LocalName))
                {
                    this.BuilderName = xmlElem.Value.Trim();
                }
                else if (String.Equals(ELEM_TIMEZONEID, xmlElem.Name.LocalName))
                {
                    this.TimeZoneId = String.Intern(xmlElem.Value.Trim());
                }
                else if(String.Equals(ELEM_PERFORCE_SWARM, xmlElem.Name.LocalName))
                {
                    this.PerforceSwarmServer = xmlElem.Value.Trim();
                }
                else
                {
                    log.Debug(xmlStudioElem, "Unknown child Element on Studio element: '{0}'.",
                        xmlElem.Name.LocalName);
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Configuration.Implementation namespace
