﻿//---------------------------------------------------------------------------------------------
// <copyright file="Environment.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2010-2017. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using RSG.Base;
    using RSG.Base.Extensions;

    /// <summary>
    /// Environment class to allow variable string substitution.
    /// </summary>
    internal class Environment :
        IEnvironment,
        IEnumerable<KeyValuePair<String, String>>
    {
        #region Fields
        /// <summary>
        /// Local dictionary of Name -> Value pairs.
        /// </summary>
        /// This should not be public; use the Add/Remove method for validating
        /// entries.
        private Stack<Dictionary<String, String>> _variables;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="Environment"/> class.
        /// </summary>
        public Environment()
        {
            this._variables = new Stack<Dictionary<String, String>>();
            this._variables.Push(new Dictionary<String, String>());
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Environment"/> class
        /// from an existing <see cref="IEnvironment"/>.
        /// </summary>
        /// <param name="environment">
        /// Environment to clone.
        /// </param>
        public Environment(IEnvironment environment)
            : this()
        {
            foreach (KeyValuePair<String, String> kvp in environment)
            {
                String strippedKey = kvp.Key.TrimStart('$').TrimStart('(').TrimEnd(')');
                this.Add(strippedKey, kvp.Value);
            }
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Environment"/> class.
        /// </summary>
        /// <param name="configObject">
        /// Initial object to import.
        /// </param>
        public Environment(ICanImportEnvironment configObject)
            : this()
        {
            configObject.Import(this);
        }
        #endregion // Constructor(s)

        #region Properties
        /// <inheritdoc />
        public int Depth
        {
            get
            {
                Debug.Assert(
                    this._variables.Count >= 1,
                    "Internal error: variable stack is empty!!");
                return this._variables.Count;
            }
        }

        /// <inheritdoc />
        public String this[String key]
        {
            get
            {
                Dictionary<String, String> top = this._variables.Peek();
                return top[key.ToLower()];
            }
        }
        #endregion // Properties

        #region Methods
        /// <inheritdoc />
        public void Add(String variable, String value)
        {
            if (String.IsNullOrEmpty(variable))
                throw (new ArgumentNullException("variable"));
            if (String.IsNullOrEmpty(value))
                throw (new ArgumentNullException("value"));

            this.Add(variable, value, BracketType.Default);
        }

        /// <inheritdoc />
        public void Add(String variable, String value, BracketType bracket = BracketType.Default)
        {
            Debug.Assert(
                this._variables != null && this._variables.Count > 0,
                "Invalid internal state - cannot add.");
            Debug.Assert(
                !variable.StartsWith("$("),
                "Do not use the $() syntax for variables.  Automatically applied.");
            if (variable.StartsWith("$("))
            {
                throw new ArgumentException("Do not use the $() syntax for variables.  Automatically applied.");
            }

            Dictionary<String, String> top = this._variables.Peek();
            String var = String.Empty;
            switch (bracket)
            {
#pragma warning disable 618
                case BracketType.AlternateCurly:
#pragma warning restore 0618
                    var = String.Format("${{{0}}}", variable);
                    break;
                case BracketType.Default:
                default:
                    var = String.Format("$({0})", variable);
                    break;
            }

            if (!top.ContainsKey(var))
            {
                top.Add(var, value);
            }
            else
            {
                top[var] = value;
            }
        }

        /// <inheritdoc />
        public String Lookup(String variable)
        {
            return this[variable];
        }

        /// <inheritdoc />
        public String Subst(String value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return String.Empty;
            }

            String substValue = value;

            bool replaced = false;
            do
            {
                replaced = false;
                foreach (KeyValuePair<String, String> kvp in this._variables.Peek())
                {
                    if (!substValue.Contains(kvp.Key))
                    {
                        continue;
                    }

                    substValue = substValue.Replace(kvp.Key, kvp.Value);
                    replaced = true;
                }

                if (!replaced)
                {
                    break;
                }
            } while (replaced);

            return substValue;
        }

        /// <inheritdoc />
        public String ReverseSubst(String value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return String.Empty;
            }

            String pathValueToReverseSubst = value;

            try
            {
                // Build and sort subst dictionary values by length; we want to match the
                // longest string-value first.  We expand the values to ensure we handle
                // multiple environment variables and get the right length.
                List<KeyValuePair<String, String>> values = new List<KeyValuePair<String, String>>();
                foreach (KeyValuePair<String, String> kvp in this._variables.Peek())
                {
                    String substValue = this.Subst(kvp.Value);

                    // Make sure the path is rooted, so we don't include DLC uiname values etc which break with Path.GetFullPath
                    bool isRootedPath;
                    try
                    {
                        // check to ensure no invalid path chars.
                        isRootedPath = Path.IsPathRooted(substValue);
                    }
                    catch (ArgumentException)
                    {
                        // The variable contains characters the filesystem could not handle, so it's not a path. Ignore it.
                        // This is because Path.GetInvalidPathChars() is not reliable--see
                        // https://msdn.microsoft.com/en-us/library/system.io.path.getinvalidpathchars(v=vs.110).aspx
                        isRootedPath = false;
                    }

                    if (isRootedPath)
                    {
                        values.Add(new KeyValuePair<String, String>(kvp.Key, Path.GetFullPath(substValue)));
                    }
                }

                values.Sort(
                    delegate(KeyValuePair<String, String> first, KeyValuePair<String, String> second)
                    {
                        // Inverted compare to ensure longest first.
                        int comp = second.Value.Length.CompareTo(first.Value.Length);
                        if (comp != 0)
                        {
                            return comp;
                        }

                        // Alphabetic sort by path otherwise (needed for stripping out "core_" prefix, below)
                        return first.Value.CompareTo(second.Value);
                    });

                // We are checking if the string provided start with a non-expanded variable. For historical reason
                // reverse subst method expect the text to be a path and users are expecting it to be fully expanded
                // This was returning wrong path when the variables were known at later stage. see url:bugstar:2720748
                if (!(value[0] == '$' && (value[1] == '{' || value[1] == '(')))
                {
                    pathValueToReverseSubst = Path.GetFullPath(value);
                }

                bool replaced = false;
                do
                {
                    replaced = false;
                    for (int i = 0; i < values.Count; ++i)
                    {
                        KeyValuePair<String, String> kvp = values[i];

                        if (String.IsNullOrEmpty(kvp.Value))
                        {
                            continue; // Ignore empty values previously warned about!
                        }

                        if (!pathValueToReverseSubst.StartsWith(kvp.Value, StringComparison.OrdinalIgnoreCase))
                        {
                            continue;
                        }

                        // TCL files and TCS files are still being created with RS_ASSETS or core_RS_ASSETS in the name.
                        // Ignoring here so it doesn't appear in exported data anymore and then we can remove it from Branch.cs.  One day...
                        if (kvp.Key.Contains("RS_ASSETS"))
                        {
                            continue;
                        }

                        string key = kvp.Key;

                        // if we have a "core_" prefixed variable, let's check if we have the same variable name without the prefix for using it instead.
                        const string corePrefix = "$(core_";
                        if (kvp.Key.StartsWith(corePrefix))
                        {
                            int counter = 1;

                            // Strip out $(core_, and add $(, leaving $(key)
                            string keySearched = string.Format("$({0}", kvp.Key.Substring(corePrefix.Length));

                            // Iterate while the values have the same path
                            while (i + counter < values.Count && values[i + counter].Value.Equals(kvp.Value, StringComparison.OrdinalIgnoreCase))
                            {
                                if (values[i + counter].Key == keySearched)
                                {
                                    // we found the variable and are going to use it.
                                    key = keySearched;
                                }

                                ++counter;
                            }

                            // We can skip treating the values iterated in this loop as they were identical to the one already found
                            i += counter - 1;
                        }

                        pathValueToReverseSubst = pathValueToReverseSubst.Replace(kvp.Value, key, StringComparison.OrdinalIgnoreCase);
                        replaced = true;
                    }

                    if (!replaced)
                    {
                        break;
                    }
                } while (replaced);
            }
            catch (Exception ex)
            {
                throw new EnvironmentException("Exception running ReverseSubst.", ex);
            }

            return pathValueToReverseSubst;
        }

        /// <inheritdoc />
        public bool RequiresSubst(String value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return false;
            }

            return value.Contains("$(") || value.Contains("${");
        }

        /// <inheritdoc />
        public void Reset()
        {
            this._variables = new Stack<Dictionary<String, String>>();
            this._variables.Push(new Dictionary<String, String>());
        }

        /// <inheritdoc />
        public void Clear()
        {
            Dictionary<String, String> top = this._variables.Peek();
            top.Clear();
        }

        /// <inheritdoc />
        public void Push()
        {
            Debug.Assert(
                this._variables != null && this._variables.Count > 0,
                "Invalid internal state - cannot push.");
            Dictionary<String, String> top = this._variables.Peek();
            this._variables.Push(new Dictionary<String, String>(top));
        }

        /// <inheritdoc />
        public bool Pop()
        {
            Debug.Assert(
                this._variables != null && this._variables.Count > 1,
                "Attempting to pop the last environment on the stack.");
            if (1 == this._variables.Count)
            {
                return false;
            }

            this._variables.Pop();
            return true;
        }
        #endregion // Methods

        #region IEnumerable<KeyValuePair<String, String>> Methods
        /// <inheritdoc />
        public IEnumerator<KeyValuePair<String, String>> GetEnumerator()
        {
            foreach (KeyValuePair<String, String> kvp in this._variables.Peek())
            {
                yield return kvp;
            }
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        #endregion // IEnumerable<KeyValuePair<String, String>> Methods
    }
} // RSG.Configuration.Implementation namespace
