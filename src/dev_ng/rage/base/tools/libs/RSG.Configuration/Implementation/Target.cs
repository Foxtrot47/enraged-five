﻿//---------------------------------------------------------------------------------------------
// <copyright file="Target.cs" company="Rockstar">
//     Copyright © Rockstar Games 2011-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using SIO = System.IO;
    using System.Text;
    using System.Xml.Linq;
    using RSG.Base.Logging;
    using RSG.Platform;

    /// <summary>
    /// Target abstraction; representing a target platform defining the 
    /// architecture and build directory path.
    /// </summary>
    internal class Target : 
        ConfigBase,
        ITarget,
        IHasEnvironment
    {
        #region Constants
        internal const String ATTR_PLATFORM = "platform";
        internal const String ATTR_ENABLED = "enabled";

        private const String ATTR_PATH = "path";
        private const String ATTR_SHADERS = "shaders";
        private const String ATTR_SHADEREXT = "shader_ext";
        private const String ARG_CONVERT_SCRIPT_LOCAL = "$(toolslib)/util/ragebuilder/convert_file.rbs";
        private const String ARG_CONVERT_SCRIPT_XGE = "$(toolslib)/util/ragebuilder/convert_xge2.rbs";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Associated Branch object.
        /// </summary>
        public IBranch Branch
        {
            get;
            private set;
        }

        /// <summary>
        /// Associated platform key.
        /// </summary>
        public RSG.Platform.Platform Platform
        {
            get;
            private set;
        }

        /// <summary>
        /// Path to platform build files.
        /// </summary>
        public String ResourcePath
        {
            get { return SIO.Path.GetFullPath(this.Environment.Subst(m_sResourcePath)); }
            private set { m_sResourcePath = value; }
        }
        private String m_sResourcePath;

        /// <summary>
        /// Path for target's shaders.
        /// </summary>
        public String ShaderPath
        {
            get { return SIO.Path.GetFullPath(this.Environment.Subst(m_sShaderPath)); }
            private set { m_sShaderPath = value; }
        }
        private String m_sShaderPath;

        /// <summary>
        /// Shader file extension.
        /// </summary>
        public String ShaderExtension
        {
            get { return m_sShaderExtension; }
            private set { m_sShaderExtension = value; }
        }
        private String m_sShaderExtension;

        /// <summary>
        /// Enabled flag for this target (allowing local users to disable targets).
        /// </summary>
        public bool Enabled
        {
            get { return m_bIsEnabled; }
            set { m_bIsEnabled = value; }
        }
        private bool m_bIsEnabled;

        /// <summary>
        /// Target environment; inheriting from Branch, Project and Config.
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <param name="xmlElem"></param>
        internal Target(ILog log, IBranch branch, XElement xmlElem)
        {
            this.Branch = branch;
            this.Environment = new Environment(branch);
            ParseTarget(log, xmlElem);

            // Reconstruct environment.
            this.Import(this.Environment);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="environment"></param>
        public void Import(IEnvironment environment)
        {
            this.Branch.Import(environment);
            environment.Add("platform", this.Platform.ToString());
            environment.Add("target", this.m_sResourcePath);
        }

        /// <summary>
        /// Return Ragebuilder absolute executable path for a particular target.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public String GetRagebuilderExecutable()
        {
            IBranch branch = this.Branch;
            RSG.Platform.Platform platform = this.Platform;
            String conversionToolPath = branch.PlatformConversionTools[platform].ExecutablePath;
            conversionToolPath = branch.Environment.Subst(conversionToolPath);

            return (conversionToolPath);
        }

        /// <summary>
        /// Return Ragebuilder XGE conversion arguments for a particular target.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public String GetRagebuilderConvertXGEArguments()
        {
            return GetRagebuilderArguments();
        }

        /// <summary>
        /// Return Ragebuilder XGE conversion script argument.
        /// </summary>
        /// <returns></returns>
        public String GetRagebuilderConvertXGEScript()
        {
            return (ARG_CONVERT_SCRIPT_XGE);
        }

        /// <summary>
        /// Return Ragebuilder local conversion arguments for a particular target.
        /// </summary>
        /// <returns></returns>
        public String GetRagebuilderConvertLocalArguments()
        {
            return GetRagebuilderArguments(isLocal: true);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Parse a target data element.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        private void ParseTarget(ILog log, XElement xmlElem)
        {
            ParseTargetAttributes(log, xmlElem);
        }

        /// <summary>
        /// Parse a target attributes data.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        private void ParseTargetAttributes(ILog log, XElement xmlElem)
        {
            foreach (XAttribute xmlAttr in xmlElem.Attributes())
            {
                switch (xmlAttr.Name.LocalName)
                {
                    case ATTR_PLATFORM:
                        {
                            String platform = xmlAttr.Value.Trim();
                            this.Platform = PlatformUtils.PlatformFromString(platform);
                        }
                        break;
                    case ATTR_PATH:
                        this.ResourcePath = xmlAttr.Value.Trim();
                        break;
                    case ATTR_SHADERS:
                        this.ShaderPath = xmlAttr.Value.Trim();
                        break;
                    case ATTR_SHADEREXT:
                        this.ShaderExtension = xmlAttr.Value.Trim();
                        break;
                    default:
                        //log.Debug("Unknown Target attribute: '{0}'.", xmlAttr.Name.LocalName);
                        break;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isLocal"></param>
        /// <returns></returns>
        private String GetRagebuilderArguments(bool isLocal = false)
        {
            IBranch branch = this.Branch;
            Platform platform = this.Platform;

            StringBuilder parameters = new StringBuilder();
            if (isLocal)
                parameters.AppendFormat("{0} ", branch.Environment.Subst(ARG_CONVERT_SCRIPT_LOCAL));

            parameters.AppendFormat("-platform {0} ", platform.PlatformToRagebuilderPlatform());
            parameters.Append(branch.RageBuilderCommonArgs);

            // substitution for the current branch, before going through corebranch for $(core_)-based keys
            String branchArgs = branch.Environment.Subst(parameters.ToString());

            IBranch coreBranch;
            if (branch.Project is IProjectDLC)
            {
                IProjectDLC dlcProject = (IProjectDLC)branch.Project;
                dlcProject.CoreProject.Branches.TryGetValue(branch.Name, out coreBranch);
            }
            else
            {
                // Core-branch is the same as our branch.
                coreBranch = branch;
            }

            return (coreBranch.Environment.Subst(branchArgs));
        }
        #endregion // Private Methods
    }

} // RSG.Configuration.Implementation namespace
