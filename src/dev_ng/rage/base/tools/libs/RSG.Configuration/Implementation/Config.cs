﻿//---------------------------------------------------------------------------------------------
// <copyright file="Config.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using RSG.Configuration.Installer;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using System.Net.Sockets;
    using RSG.Base.Net;

    /// <summary>
    /// Entry Configuration class for system-wide globals for current tools 
    /// branch.  This includes the project context for the current tools branch.
    /// </summary>
    internal class Config : 
        ConfigBase,
        IConfig, 
        IConfigFile,
        IWritableConfig
    {
        #region Constants
        internal const int CONFIG_VERSION = 2;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Configuration system version.
        /// </summary>
        public int ConfigVersion
        {
            get { return (CONFIG_VERSION); }
        }

        /// <summary>
        /// Configuration filename.
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// Tools version information.
        /// </summary>
        public IToolsVersion Version
        {
            get;
            private set;
        }

        #region Core Tools Variables
        /// <summary>
        /// Tools root directory.
        /// </summary>
        public String ToolsRootDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Tools binary directory.
        /// </summary>
        public String ToolsBinDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Tools configuration data absolute path.
        /// </summary>
        public String ToolsConfigDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Tools temporary files absolute path.
        /// </summary>
        public String ToolsLogsDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Tools temporary files absolute path.
        /// </summary>
        public String ToolsTempDirectory
        {
            get;
            private set;
        }

        /// <summary>
        /// Absolute path to Tools Installer.
        /// </summary>
        public String ToolsInstaller
        {
            get;
            private set;
        }

        /// <summary>
        /// Absolute path to Tools Quick Installer.
        /// </summary>
        public String ToolsQuickInstaller
        {
            get;
            private set;
        }
        #endregion // Core Tools Variables

        /// <summary>
        /// Current core project.
        /// </summary>
        public IProjectCore Project 
        { 
            get;
            private set;
        }
        
        /// <summary>
        /// Current studio.
        /// </summary>
        public IStudio CurrentStudio
        {
            get;
            private set;
        }

        /// <summary>
        /// Available studio collection.
        /// </summary>
        public IEnumerable<IStudio> Studios
        {
            get { return (_studios); }
        }
        private List<IStudio> _studios;

        #region User Local Data (Set by Installer)
        /// <summary>
        /// Last install time.
        /// </summary>
        public DateTime InstallTime
        {
            get;
            set;
        }

        /// <summary>
        /// User's username.
        /// </summary>
        public String Username
        {
            get;
            set;
        }

        /// <summary>
        /// User's email address.
        /// </summary>
        public String EmailAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Version control enabled flag.
        /// </summary>
        public bool VersionControlEnabled 
        { 
            get;
            set;
        }
        #endregion // User Local Data (Set by Installer)

        /// <summary>
        /// Main configuration environment.
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }

        /// <summary>
        /// Property to determine whether internal project XML version check
        /// happens (its sometimes required to be switched off).
        /// </summary>
        private bool SkipVersionCheck
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="skip_version_check"></param>
        public Config(ILog log, bool skip_version_check = false)
        {
            this.SkipVersionCheck = skip_version_check;
            if (this.SkipVersionCheck)
                log.Message("Skipping configuration version check.");

            // Create new tools config object and initialise from that.
            ToolsConfig dynamicConfig = new ToolsConfig();

            this.ToolsRootDirectory = dynamicConfig.ToolsRootDirectory;
            Debug.Assert(Path.IsPathRooted(this.ToolsRootDirectory),
                String.Format("RS_TOOLSROOT ({0}) has no drive specified.  Misconfigured tools.", this.ToolsRootDirectory));
            if (!Path.IsPathRooted(this.ToolsRootDirectory))
                throw new ConfigurationException(String.Format("RS_TOOLSROOT ({0}) has no drive specified.  Misconfigured tools.", this.ToolsRootDirectory));

            // Setup basic properties.
            this.Filename = Path.Combine(this.ToolsRootDirectory, "Configuration.meta");
            this.ToolsBinDirectory = dynamicConfig.ToolsBinDirectory;
            this.ToolsConfigDirectory = dynamicConfig.ToolsConfigDirectory;
            this.ToolsTempDirectory = dynamicConfig.ToolsTempDirectory;
            this.ToolsLogsDirectory = dynamicConfig.ToolsLogsDirectory;
            this.ToolsInstaller = Path.Combine(this.ToolsRootDirectory, "install.bat");
            this.ToolsQuickInstaller = Path.Combine(this.ToolsRootDirectory, "quick_install.bat");
            this.Version = new ToolsVersion(log, this);
            this.Environment = new Environment(this);

            // Load file.
            this.Reload(log);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="environment"></param>
        public void Import(IEnvironment environment)
        {
            environment.Add("toolsroot", this.ToolsRootDirectory);
            environment.Add("toolsbin", this.ToolsBinDirectory);
            environment.Add("toolsconfig", this.ToolsConfigDirectory);
            environment.Add("toolstemp", this.ToolsTempDirectory);
        }

        /// <summary>
        /// Reload all the configuration data; and all projects.
        /// </summary>
        /// <param name="log"></param>
        public void Reload(ILog log)
        {
            Debug.Assert(File.Exists(this.Filename),
                String.Format("Config XML '{0}' does not exist.", this.Filename));
            if (!File.Exists(this.Filename))
                throw new ConfigurationException(String.Format("Config XML '{0}' does not exist.", this.Filename));
            XDocument xmlConfigDoc = XDocument.Load(this.Filename);
            ParseConfig(log, xmlConfigDoc, false);

            // Parse Studio.
            ParseStudioFile(log);
            SetCurrentStudio(log);

            // Parse local data.
            String localXml = Path.Combine(this.ToolsRootDirectory, "local.xml");
            if (File.Exists(localXml))
            {
                XDocument xmlDocLocal = XDocument.Load(localXml,
                    LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
                ParseConfig(log, xmlDocLocal, true);
            }

            // Needs to happen after the main config file is parsed.
            Version.Reload(log);

            // Reconstruct environment.
            this.Import(this.Environment);
        }

        /// <summary>
        /// Save local settings (used by WritableConfig and keeps serialisation and
        /// deserialisation code in one place).
        /// </summary>
        /// <returns></returns>
        public bool SaveLocal()
        {
            try
            {
                String filename = Path.Combine(this.ToolsRootDirectory, "local.xml");
                String filenameBackup = Path.ChangeExtension(filename, ".xml.bak");
                if (File.Exists(filename) && !File.Exists(filenameBackup))
                    File.Copy(filename, filenameBackup);
                
                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("local",
                        // New XElement-format.
                        new XComment("Tools Migration: new XElement-format."),
                        new XElement("ConfigFileVersion", CONFIG_VERSION),
                        new XElement("InstallTime", this.InstallTime.Ticks),
                        new XElement("LocalVersion", this.Version.LocalVersion.LabelName),
                        new XElement("InstalledVersion", this.Version.InstalledVersion.LabelName),
                        new XElement("VersionControlEnabled", new XAttribute("value", this.VersionControlEnabled)),
                        new XElement("User",
                            new XElement("Username", this.Username),
                            new XElement("EmailAddress", this.EmailAddress)
                        ), // User
                        new XElement("Applications",
                            new XElement("Application",
                                new XElement("Name", "Xoreax XGE"),
                                new XElement("Enabled", true)
                            )
                        ) // Applications
                    )
                );
                xmlDoc.Save(filename);

                bool result = true;
                result &= ((Installer.IWritableProject)this.Project).SaveLocal();
                return (true);
            }
            catch (Exception ex)
            {
                throw (new ConfigurationException("Failed to save local config.", ex));
            }
        }
        #endregion // Controller Methods
        
        #region Static Controller Methods
        /// <summary>
        /// Return global tools configuration filename.
        /// </summary>
        /// <param name="toolsRoot"></param>
        /// <returns></returns>
        public static String GetConfigFilename(String toolsRoot)
        {
            return (Path.Combine(toolsRoot, "Configuration.meta"));
        }

        /// <summary>
        /// Return content packs (DLC) configuration filename.
        /// </summary>
        /// <param name="toolsRoot"></param>
        /// <returns></returns>
        public static String GetContentPacksFilename(String toolsRoot)
        {
            return (Path.Combine(toolsRoot, "etc", "content_packs.meta"));
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Parse top-level configuration data elements.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlDoc"></param>
        /// <param name="local"></param>
        private void ParseConfig(ILog log, XDocument xmlDoc, bool local)
        {
            if (!local)
            {
                XElement xmlProjectElem = xmlDoc.XPathSelectElement("/ConfigurationFile/Project");
                if (null != xmlProjectElem)
                {
                    this.Project = new Project(log, this, xmlProjectElem);
                }
            }
            else
            {
                XElement xmlUserElem = xmlDoc.XPathSelectElement("/local/User");
                if (null != xmlUserElem)
                {
                    if (null != xmlUserElem.Element("Username"))
                        this.Username = xmlUserElem.Element("Username").Value;
                    if (null != xmlUserElem.Element("EmailAddress"))
                        this.EmailAddress = xmlUserElem.Element("EmailAddress").Value;
                }

                XElement xmlVersionControlEnabledElem = xmlDoc.XPathSelectElement("/local/VersionControlEnabled");
                if (null != xmlVersionControlEnabledElem)
                {
                    XAttribute valueAttr = xmlVersionControlEnabledElem.Attribute("value");
                    bool enabled = true;
                    if (null != valueAttr && bool.TryParse(valueAttr.Value, out enabled))
                        this.VersionControlEnabled = enabled;
                }
                else
                {
                    this.VersionControlEnabled = true;
                }

                XElement xmlInstallTimeElem = xmlDoc.XPathSelectElement("/local/InstallTime");
                if (null != xmlInstallTimeElem)
                {
                    long ticks = 0;
                    if (long.TryParse(xmlInstallTimeElem.Value, out ticks))
                    {
                        this.InstallTime = new DateTime(ticks);
                    }
                }
            }
        }

        /// <summary>
        /// Parse top-level Studio metadata file.
        /// </summary>
        /// <param name="log"></param>
        private void ParseStudioFile(ILog log)
        {
            // Studio data is now in a separate file.
            this._studios = new List<IStudio>();
            String studioConfigFilename = Path.Combine(this.ToolsConfigDirectory, "globals", "studios.meta");
            if (File.Exists(studioConfigFilename))
            {
                XDocument xmlStudioDoc = XDocument.Load(studioConfigFilename);
                foreach (XElement xmlStudiosElem in
                    xmlStudioDoc.XPathSelectElements("/StudioFile/Studios/Item"))
                {
                    this._studios.Add(new Studio(log, xmlStudiosElem));
                }
            }
        }

        /// <summary>
        /// Sets the current studio property based on subnet mask.
        /// </summary>
        /// <param name="log">
        /// Log object.
        /// </param>
        private void SetCurrentStudio(ILog log)
        {
            Debug.Assert(this.Studios.Any(), "No studio objects.");
            if (this.Studios.None())
                return;

            IReadOnlyCollection<IPAddress> domainAddressCandidates;
            try
            {
                // Get the mostly likely domain-connected IP addresses.
                List<IPAddress> prioritisedAddresses = IPAddressExtensions.GetAllDomainIPAddresses();

                // Get all other possible IP addresses that might be connected to the domain.
                // This will include addresses of VPN adapters.
                IEnumerable<IPAddress> extraAddresses =
                    IPAddressExtensions.GetAllCandidateDomainIPAddresses().Except(prioritisedAddresses);

                // Concatenate all candidates so that most likely addresses come first.
                domainAddressCandidates = prioritisedAddresses.Concat(extraAddresses).ToList();
            }
            catch (Exception e)
            {
                log.ToolException(e, "Exception encountered while trying to determine network interface addresses.");
                domainAddressCandidates = new IPAddress[0];
            }

            foreach (IPAddress ip in domainAddressCandidates)
            {
                // Loop through studio objects finding right studio.
                foreach (IStudio studio in this.Studios)
                {
                    KeyValuePair<IPAddress, IPAddress> subnetAndMask = SubnetMask.Create(studio.SubnetRange);

                    if (ip.IsInSameSubnet(subnetAndMask.Key, subnetAndMask.Value))
                    {
                        this.CurrentStudio = studio;
                        return;
                    }
                }

                log.MessageCtx("Finding Studio", "Given IP Address '{0}' has no matching studio subnet in the configuration", ip);
            }

            if (this.CurrentStudio == null)
            {
                throw new ConfigurationException("The Current Studio could not be found for the user. This is most likely caused by an out of date studios.meta config file, or the configuration is missing a studio.");
            }
        }
                
        /// <summary>
        /// Return environment variable value or a default value String.
        /// </summary>
        /// <param name="envvar"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        private String GetEnvVarOrDefault(String envvar, String defaultVal)
        {
            String envvarVal = System.Environment.GetEnvironmentVariable(envvar);
            if (String.IsNullOrEmpty(envvarVal))
                return (defaultVal);
            else
                return (envvarVal);
        }
        #endregion // Private Methods
    }

} // RSG.Configuration.Implementation namespace
