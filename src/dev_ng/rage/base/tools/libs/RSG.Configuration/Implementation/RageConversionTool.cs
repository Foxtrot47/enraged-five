﻿using System;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.Configuration.Implementation
{
    /// <summary>
    /// RAGE/Ragebuilder platform conversion tool.
    /// </summary>
    internal class RageConversionTool : IPlatformConversionTool
    {
        #region Constants
        private const String XmlAttrPlatform = "platform";
        private const String XmlAttrExecutable = "Executable";
        private const String XmlAttrArguments = "Arguments";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Associated platform for this conversion tool.
        /// </summary>
        public Platform.Platform Platform 
        { 
            get;
            private set;
        }

        /// <summary>
        /// Associated branch for this conversion tool.
        /// </summary>
        public IBranch Branch
        {
            get;
            private set;
        }

        /// <summary>
        /// Absolute path to conversion tool executable (or script).
        /// </summary>
        public String ExecutablePath
        {
            get { return Path.GetFullPath(this.Branch.Environment.Subst(m_sExecutablePath)); }
            private set { m_sExecutablePath = value; }
        }
        private String m_sExecutablePath;

        /// <summary>
        /// Platform and tool global options (independent of what is being 
        /// converted).
        /// </summary>
        public String Arguments
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="platform"></param>
        /// <param name="exe"></param>
        /// <param name="args"></param>
        public RageConversionTool(IBranch branch, RSG.Platform.Platform platform, String exe, String args)
        {
            this.Branch = branch;
            this.Platform = platform;
            this.ExecutablePath = exe;
            this.Arguments = args;
        }

        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="xmlElem"></param>
        internal RageConversionTool(ILog log, IBranch branch, XElement xmlElem)
        {
            this.Branch = branch;
            ParseRagebuilder(log, xmlElem);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        private void ParseRagebuilder(ILog log, XElement xmlElem)
        {
            XAttribute xmlPlatformAttr = xmlElem.Attribute(XmlAttrPlatform);
            if ((null == xmlPlatformAttr) || String.IsNullOrEmpty(xmlPlatformAttr.Value))
                throw (new ConfigurationFormatException("Ragebuilder Platform not found or not defined."));
            String platform = xmlPlatformAttr.Value;
            this.Platform = RSG.Platform.PlatformUtils.PlatformFromString(xmlPlatformAttr.Value);

            XElement xmlExecutableElem = xmlElem.Element(XmlAttrExecutable);
            if ((null == xmlExecutableElem) || String.IsNullOrEmpty(xmlExecutableElem.Value))
                throw (new ConfigurationException(string.Format("Ragebuilder Arguments: Executable path not found for platform {0}.", platform)));
            this.ExecutablePath = xmlExecutableElem.Value;
            
            XElement xmlArgumentsElem = xmlElem.Element(XmlAttrArguments);
            if ((null == xmlArgumentsElem) || String.IsNullOrEmpty(xmlArgumentsElem.Value))
                throw (new ConfigurationException(string.Format("Ragebuilder Arguments: Arguments not found for platform {0}.", platform)));
            this.Arguments = xmlArgumentsElem.Value;
        }
        #endregion // Private Methods
    }

} // RSG.Configuration.Implementation namespace
