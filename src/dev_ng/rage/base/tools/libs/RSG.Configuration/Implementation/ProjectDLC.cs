﻿//---------------------------------------------------------------------------------------------
// <copyright file="Project.cs" company="Rockstar">
//     Copyright © Rockstar Games 2011-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using RSG.Base.Logging;

    /// <summary>
    /// DLC project (content-pack) encapsulation; with a reference to the core project.
    /// </summary>
    internal class ProjectDLC :
        ProjectBase,
        IProjectDLC
    {
        #region Properties
        /// <summary>
        /// Core project associated with this DLC content-pack.
        /// </summary>
        public IProjectCore CoreProject
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="coreProject"></param>
        /// <param name="xmlProjectSummary"></param>
        public ProjectDLC(ILog log, IProjectCore coreProject, XElement xmlProjectSummary)
            : base(log, coreProject.Config, xmlProjectSummary)
        {
            if (null == coreProject)
                throw (new ArgumentNullException("coreProject"));

            this.CoreProject = coreProject;
        }
        #endregion // Constructor(s)

    }

} // RSG.Configuration.Implementation namespace
