﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProjectBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2011-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Configuration.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using RSG.Base.Logging;
    using RSG.Platform;

    /// <summary>
    /// Base project encapsulation.
    /// </summary>
    internal abstract class ProjectBase :
        IProject,
        Installer.IWritableProject
    {
        #region Constants
        private const String CORE_PROJECT_PREFIX = "core_";

        private const String ELEM_PROJECT = "project";
        private const String ELEM_LOCAL = "local";
        
        internal const String ATTR_BRANCH_DEFAULT = "default";

        internal const String ATTR_NAME = "name";

        internal const String xmlAttrCacheDirectory = "cache";
        internal const String ATTR_HASLEVELS = "has_levels";
        internal const String ATTR_LABEL_TYPE = "type";
        internal const String ATTR_LABEL_NAME = "name";
        internal const String ATTR_DEFAULT_BRANCH = "default";

        // ProjectSummary XElement Name Constants
        internal const String xmlElemKey = "Key";
        internal const String xmlElemFriendlyName = "FriendlyName";
        internal const String xmlElemConfigurationFilename = "ConfigurationFilename";
        internal const String xmlElemRootDirectory = "RootDirectory";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Project name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Project friendly UI name.
        /// </summary>
        public String FriendlyName
        {
            get;
            private set;
        }

        /// <summary>
        /// Project root directory $(root).
        /// </summary>
        public String RootDirectory
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sRootDirectory)); }
            private set { m_sRootDirectory = value; }
        }
        private String m_sRootDirectory;
        
        /// <summary>
        /// Project configuration filename.
        /// </summary>
        public String Filename
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sFilename)); }
        }
        private String m_sFilename;

        /// <summary>
        /// Project cache directory $(cache).
        /// </summary>
        public String CacheDirectory
        {
            get { return Path.GetFullPath(this.Environment.Subst(m_sCache)); }
            private set { m_sCache = value; }
        }
        private String m_sCache;

        /// <summary>
        /// Whether this project has levels.
        /// </summary>
        public bool HasLevels
        {
            get;
            private set;
        }

        /// <summary>
        /// Force flags collection.
        /// </summary>
        public IForceFlags ForceFlags
        {
            get;
            private set;
        }

        /// <summary>
        /// Asset prefix string (default: empty for non-DLC, project-name for DLC).
        /// </summary>
        public IAssetPrefix AssetPrefix
        {
            get;
            private set;
        }

        /// <summary>
        /// Config object parent.
        /// </summary>
        public IConfig Config
        {
            get;
            private set;
        }

        /// <summary>
        /// Enumerable of Tools-associated mail addresses (for sending logs and bug reports).
        /// </summary>
        public IEnumerable<System.Net.Mail.MailAddress> ToolsEmailAddresses
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of branches for this project.
        /// </summary>
        public IDictionary<String, IBranch> Branches
        {
            get;
            private set;
        }

        /// <summary>
        /// Dictionary of available labels for this project.
        /// </summary>
        public IDictionary<Label, String> Labels
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of default platforms, these can be used when the current default branch
        /// has no active targets in it.
        /// </summary>
        public IDictionary<Platform, bool> DefaultPlatforms
        {
            get;
            private set;
        }

        /// <summary>
        /// Application settings; enabled flags etc.
        /// </summary>
        public IDictionary<String, IApplicationSettings> ApplicationSettings
        {
            get;
            private set;
        }

        /// <summary>
        /// Project environment; inheriting from Config.
        /// </summary>
        public IEnvironment Environment
        {
            get;
            private set;
        }

        /// <summary>
        /// Local default branch name for this project.
        /// </summary>
        /// Note: user-applications use CommandOptions2 to get the branch context.
        /// 
        public String DefaultBranchName
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config">Config object parent.</param>
        /// <param name="xmlProjectSummary"></param>
        public ProjectBase(ILog log, IConfig config, XElement xmlProjectSummary)
        {
            this.Config = config;
            this.Environment = new Environment();
            this.Branches = new Dictionary<String, IBranch>();
            this.Labels = new Dictionary<Label, String>();
            this.DefaultPlatforms = new Dictionary<Platform, bool>();

            // Set specified configuration data.
            ParseProjectSummary(xmlProjectSummary);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Import settings into an IEnvironment object.
        /// </summary>
        /// <param name="environment"></param>
        public void Import(IEnvironment environment)
        {
            this.Config.Import(environment);
            this.Import(environment, String.Empty);
            this.Import(environment, CORE_PROJECT_PREFIX);
        }

        /// <summary>
        /// Reload all project configuration data.
        /// </summary>
        /// <param name="log"></param>
        public virtual void Reload(ILog log)
        {
            this.Branches.Clear();
            this.Environment.Clear();
            this.Config.Import(this.Environment);

            if (!File.Exists(this.Filename))
            {
                throw new ConfigurationException("Core project XML does not exist.")
                {
                    Filename = this.Filename
                };
            }

            XDocument xmlDoc = XDocument.Load(this.Filename,
                LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
            ParseProject(log, xmlDoc, false);

            String localConfigFilename = Path.Combine(Path.GetDirectoryName(this.Filename), "local.xml");
            if (File.Exists(localConfigFilename))
            {
                XDocument xmlDocLocal = XDocument.Load(localConfigFilename,
                    LoadOptions.SetLineInfo | LoadOptions.SetBaseUri);
                ParseProject(log, xmlDocLocal, true);
            }

            // Clear and re-import a fresh environment.
            this.Environment.Clear();
            this.Import(this.Environment);
        }

        /// <summary>
        /// Save all of the configuration data.
        /// </summary>
        public bool SaveLocal()
        {
            String filename = Path.Combine(this.Config.ToolsConfigDirectory, "local.xml");
            try
            {
                String filenameBackup = Path.ChangeExtension(filename, ".xml.bak");
                if (File.Exists(filename) && !File.Exists(filenameBackup))
                    File.Copy(filename, filenameBackup);

                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("local",
                        new XElement("ConfigFileVersion", this.Config.ConfigVersion),
                        new XElement("Branches",
                            new XElement("Default", this.DefaultBranchName),
                            this.Branches.Select(branch =>
                                new XElement("Branch",
                                    new XElement("Name", branch.Key),
                                    new XElement("Targets",
                                        branch.Value.Targets.Select(kvp =>
                                            new XElement("Target",
                                                new XElement("Platform", kvp.Key),
                                                new XElement("Enabled", kvp.Value.Enabled)
                                            )
                                        )
                                    )
                                )
                            ),
                            new XElement("DefaultPlatforms",
                                this.DefaultPlatforms.Select(kvp =>
                                    new XElement("DefaultPlatform",
                                        new XElement("Platform", kvp.Key),
                                        new XElement("Enabled", kvp.Value)
                                    )
                                )
                            )
                        )
                    )
                );
                xmlDoc.Save(filename);
                return (true);
            }
            catch (Exception ex)
            {
                throw new ConfigurationException("Failed to save local project config.", ex)
                {
                    Filename = filename
                };
            }
        }
        #endregion // Controller Methods

        #region Object Overrides
        /// <summary>
        /// Return simple string representation of this Project.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("{0}: {1}", this.Name, this.FriendlyName));
        }
        #endregion // Object Overrides

        #region Private Methods
        /// <summary>
        /// Parse top-level project elements.
        /// </summary>
        /// <param name="xmlProjectElem"></param>
        private void ParseProjectSummary(XElement xmlProjectElem)
        {
            if (null != xmlProjectElem.Element(xmlElemKey))
                this.Name = xmlProjectElem.Element(xmlElemKey).Value;
            if (null != xmlProjectElem.Element(xmlElemFriendlyName))
                this.FriendlyName = xmlProjectElem.Element(xmlElemFriendlyName).Value;
            if (null != xmlProjectElem.Element(xmlElemRootDirectory))
                this.RootDirectory = xmlProjectElem.Element(xmlElemRootDirectory).Value;
            if (null != xmlProjectElem.Element(xmlElemConfigurationFilename))
                m_sFilename = this.Environment.Subst(xmlProjectElem.Element(xmlElemConfigurationFilename).Value);
        }

        /// <summary>
        /// Parse top-level project configuration data elements.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlDoc"></param>
        /// <param name="local"></param>
        private void ParseProject(ILog log, XDocument xmlDoc, bool local)
        {
            ParseProjectAttributes(xmlDoc, local);
            String root = local ? ELEM_LOCAL : ELEM_PROJECT;

            // Only parse asset prefix data from the checked in configuration.
            // Do not allow local overrides.
            if (!local)
            {
                XAttribute xmlCacheDirectoryElem = xmlDoc.XPathSelectElement("/project").Attribute("cache");
                if (null != xmlCacheDirectoryElem)
                    this.CacheDirectory = xmlCacheDirectoryElem.Value;

                XElement xmlForceFlagsElem = xmlDoc.XPathSelectElement("/project/ForceFlags");
                if (null != xmlForceFlagsElem)
                    this.ForceFlags = new ForceFlags(this, xmlForceFlagsElem);
                else
                    this.ForceFlags = new ForceFlags(this);

                XElement xmlAssetPrefixElem = xmlDoc.XPathSelectElement("/project/AssetPrefix");
                if (null != xmlAssetPrefixElem)
                    this.AssetPrefix = new AssetPrefix(this, xmlAssetPrefixElem.Value);
                else
                    this.AssetPrefix = new AssetPrefix(this);

                List<System.Net.Mail.MailAddress> emailAddresses = new List<System.Net.Mail.MailAddress>();
                foreach (XElement xmlEmailAddress in
                    xmlDoc.XPathSelectElements("/project/ToolsEmailAddresses/EmailAddress"))
                {
                    String emailAddress = xmlEmailAddress.Element("Address").Value;
                    String displayName = xmlEmailAddress.Element("DisplayName").Value;
                    emailAddresses.Add(new System.Net.Mail.MailAddress(emailAddress, displayName));
                }
                this.ToolsEmailAddresses = emailAddresses.ToArray();
            }

            foreach (XElement xmlBranchElem in
                xmlDoc.XPathSelectElements(String.Format("{0}/branches/branch", root)))
            {
                ParseBranch(log, xmlBranchElem, local);
            }
            foreach (XElement xmlDefaultPlatformElem in
                xmlDoc.XPathSelectElements(String.Format("{0}/DefaultPlatforms/DefaultPlatform", root)))
            {
                // We currently have no default platforms inside the non-local files.
                if (local)
                {
                    ParseDefaultPlatform(log, xmlDefaultPlatformElem);
                }
            }
            foreach (XElement xmlLabelElem in
                xmlDoc.XPathSelectElements(String.Format("{0}/labels/label", root)))
            {
                ParseLabel(log, xmlLabelElem, local);
            }

            SetupDefaultBranch(log, xmlDoc, local);
        }

        /// <summary>
        /// Parse top-level project data attributes.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="local"></param>
        private void ParseProjectAttributes(XDocument xmlDoc, bool local)
        {
            foreach (XAttribute xmlProjectAttr in xmlDoc.Root.Attributes())
            {
                if (String.Equals(xmlAttrCacheDirectory, xmlProjectAttr.Name.LocalName))
                {
                    this.CacheDirectory = xmlProjectAttr.Value.Trim();
                }
                else if (String.Equals(ATTR_HASLEVELS, xmlProjectAttr.Name.LocalName))
                {
                    this.HasLevels = bool.Parse(xmlProjectAttr.Value);
                }
            }
        }

        /// <summary>
        /// Setup default branch properties.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlDoc"></param>
        /// <param name="local"></param>
        private void SetupDefaultBranch(ILog log, XDocument xmlDoc, bool local)
        {
            if (local)
            {
                XElement xmlBranchesElem = xmlDoc.XPathSelectElement(
                   String.Format("{0}/branches", ELEM_LOCAL));
                XAttribute xmlDefaultBranchAttr = xmlBranchesElem.Attributes().FirstOrDefault(
                    attr => (String.Equals(ATTR_BRANCH_DEFAULT, attr.Name.LocalName)));
                if (null != xmlDefaultBranchAttr)
                {
                    // Only set our default branch if its valid.  This allows
                    // us to remove branches that were previously default.
                    String defaultBranchName = xmlDefaultBranchAttr.Value.Trim();
                    if (this.Branches.ContainsKey(defaultBranchName))
                    {
                        this.DefaultBranchName = defaultBranchName;
                    }
                }
                else
                {
                    this.DefaultBranchName = String.Empty;
                    log.Warning(xmlDoc, "Default branch not set if config data.");
                }
            }
            else
            {
                XElement xmlBranchesElem = xmlDoc.XPathSelectElement(
                    String.Format("{0}/branches", ELEM_PROJECT));
                XAttribute xmlDefaultBranchAttr = xmlBranchesElem.Attributes().Where(
                    attr => (String.Equals(ATTR_BRANCH_DEFAULT, attr.Name.LocalName))).FirstOrDefault();
                if (null != xmlDefaultBranchAttr)
                {
                    // Only set our default branch if its valid.  This allows
                    // us to remove branches that were previously default.
                    String defaultBranchName = xmlDefaultBranchAttr.Value.Trim();
                    if (this.Branches.ContainsKey(defaultBranchName))
                    {
                        this.DefaultBranchName = defaultBranchName;
                    }
                }
                else
                {
                    this.DefaultBranchName = String.Empty;
                    log.Warning(xmlDoc, "Default branch not set if config data.");
                }
            }
        }

        /// <summary>
        /// Parse project branch element.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseBranch(ILog log, XElement xmlElem, bool local)
        {
            if (local)
            {
                XAttribute attrBranchName = xmlElem.Attributes().FirstOrDefault(
                    attr => String.Equals(ATTR_NAME, attr.Name.LocalName));
                if (null != attrBranchName)
                {
                    String branchName = attrBranchName.Value.Trim();
                    if (this.Branches.ContainsKey(branchName))
                    {
                        IBranch branch = this.Branches[branchName];
                        branch.ParseLocal(log, xmlElem);

                        foreach (ITarget target in branch.Targets.Values)
                        {
                            if (target.Enabled)
                            {
                                this.DefaultPlatforms[target.Platform] = true;
                            }
                        }
                    }
                    else
                    {
                        log.Warning("Local branch '{0}' does not exist in core configuration.", branchName);
                    }
                }
                else
                {
                    log.Error(xmlElem, "Local branch does not have a name assigned.");
                }
            }
            else
            {
                IBranch branch = new Branch(log, this, xmlElem);
                this.Branches.Add(branch.Name, branch);

                // Make sure all valid platforms are added to the default platform collection.
                foreach (ITarget target in branch.Targets.Values)
                {
                    if (!this.DefaultPlatforms.ContainsKey(target.Platform))
                    {
                        this.DefaultPlatforms.Add(target.Platform, false);
                    }
                }
            }
        }

        /// <summary>
        /// Parse project default platform element.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseDefaultPlatform(ILog log, XElement xmlElem)
        {
            // We don't define new Targets from the local configuration
            // data; only picking up the Enabled flag.
            XElement xmlPlatformElement = xmlElem.Element("Platform");
            String platform = (xmlPlatformElement != null) ? xmlPlatformElement.Value : String.Empty;
            if (String.IsNullOrEmpty(platform))
            {
                log.Warning(xmlElem, "Default target has no platform attribute.");
            }
            else
            {
                Platform p = PlatformUtils.PlatformFromString(platform);

                // Find default enabled value; for platform.
                Debug.Assert(this.DefaultPlatforms.ContainsKey(p),
                    String.Format("Project '{0}' does not define Platform '{1}'.", this.Name, platform));
                if (!this.DefaultPlatforms.ContainsKey(p))
                {
                    log.Warning("Project '{0}' does not define Platform {1}.", this.Name, platform);
                }
                else
                {
                    XElement xmlEnabledElement = xmlElem.Element("Enabled");
                    bool enabled = xmlEnabledElement != null && bool.Parse(xmlEnabledElement.Value);
                    this.DefaultPlatforms[p] = enabled;
                }
            }
        }

        /// <summary>
        /// Parse project label element.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="xmlElem"></param>
        /// <param name="local"></param>
        private void ParseLabel(ILog log, XElement xmlElem, bool local)
        {
            Debug.Assert(!local, "Label definitions are not supported in local XML data.");
            if (local)
            {
                log.Warning(xmlElem,
                    "Label definitions are not supported in local XML data.");
                return;
            }

            String labelName = String.Empty;
            String labelType = String.Empty;
            foreach (XAttribute xmlLabelAttr in xmlElem.Attributes())
            {
                if (String.Equals(ATTR_LABEL_NAME, xmlLabelAttr.Name.LocalName))
                {
                    labelName = xmlLabelAttr.Value.Trim();
                }
                else if (String.Equals(ATTR_LABEL_TYPE, xmlLabelAttr.Name.LocalName))
                {
                    labelType = xmlLabelAttr.Value.Trim();
                }
                else
                {
                    log.Warning(xmlElem, "Unknown label attribute: '{0}'.",
                         xmlLabelAttr.Name.LocalName);
                }
            }

            Label label = LabelUtil.LabelFromConfigurationString(labelType);
            if (!this.Labels.ContainsKey(label) && (Label.None != label))
                this.Labels.Add(label, labelName);
            else if (this.Labels.ContainsKey(label))
                log.Warning(xmlElem,
                    "Duplicate label key found: '{0}'.  Ignoring.", labelName);
            else
                log.Warning(xmlElem,
                    "Unknown label key: '{0}'.  Ignoring.", labelName);
        }

        /// <summary>
        /// Import settings implementation; taking prefix to handle "core" settings
        /// and active variants.
        /// </summary>
        /// <param name="environment"></param>
        /// <param name="prefix"></param>
        private void Import(IEnvironment environment, String prefix)
        {
            String rootDir = this.m_sRootDirectory.Replace("$(", String.Format("$({0}", prefix));
            String cacheDir = this.m_sCache.Replace("$(", String.Format("$({0}", prefix));

            environment.Add(String.Format("{0}name", prefix), this.Name);
            environment.Add(String.Format("{0}project", prefix), this.Name);
            environment.Add(String.Format("{0}uiname", prefix), this.FriendlyName);
            environment.Add(String.Format("{0}root", prefix), rootDir);
            environment.Add(String.Format("{0}cache", prefix), cacheDir);
        }
        #endregion // Private Methods
    }

} // RSG.Configuration.Implementation namespace
