﻿using System;
using System.Collections.Generic;
using System.Activities.Presentation.PropertyEditing;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Metadata.Parser
{
    #region 3x3-Component Matrix Member Classes

    #region Base

    /// <summary>
    /// 
    /// </summary>
    public class Matrix33MemberBase :
        MemberBase,
        IMemberInitialValue<float[]>,
        IMemberRange<float>,
        IMemberStep<float>,
        IMemberDecimalPlace
    {
        #region Fields

        private float[] m_init;
        private float m_minimum;
        private float m_maximum;
        private float m_step;
        private int m_decimalPlaces;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public float[] Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (float[])newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Minimum
        {
            get { return m_minimum; }
            set
            {
                SetPropertyValue(value, m_minimum, () => this.Minimum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minimum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Maximum
        {
            get { return m_maximum; }
            set
            {
                SetPropertyValue(value, m_maximum, () => this.Maximum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Step
        {
            get { return m_step; }
            set
            {
                SetPropertyValue(value, m_step, () => this.Step,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_step = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        public int DecimalPlaces
        {
            get { return m_decimalPlaces; }
            set
            {
                SetPropertyValue(value, m_decimalPlaces, () => this.DecimalPlaces,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_decimalPlaces = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Matrix33"; } }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Matrix33MemberBase(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = new float[9] { 0.0f, 0.0f, 0.0f,
                                       0.0f, 0.0f, 0.0f,
                                       0.0f, 0.0f, 0.0f };
            this.m_maximum = float.MaxValue / 1000.0f;
            this.m_minimum = float.MinValue / 1000.0f;
            this.m_step = 0.1f;
            this.m_decimalPlaces = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<float[]>.Parse(String s, ref float[] parseResult)
        {
            return Util.Parser.GetMulitpleFloatValuesFromString(s, this, out parseResult, 9, 0.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMin(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, -float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMax(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<float>.Parse(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, 0.1f);
        }

        /// <summary>
        /// 
        /// </summary>
        Boolean IMemberDecimalPlace.Parse(String s, ref int parseResult)
        {
            return Util.Parser.GetIntegerValue(s, this, out parseResult, -1);
        }

        #endregion // Controller Methods

        #region Private Methods
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<float[]>(reader);
            ParseMemberRangeData<float>(reader);
            ParseMemberStepData<float>(reader);
            ParseMemberDecimalPlaceData(reader);

            reader.ReadStartElement();
            if (reader.NodeType == XmlNodeType.Text)
                reader.ReadString();
            if (!isEmpty && reader.NodeType == XmlNodeType.EndElement)
                reader.ReadEndElement();
        }

        #endregion // Private Methods
    } // Matrix33MemberBase
    
    #endregion // Base

    #region Mat33V Member

    /// <summary>
    /// 
    /// </summary>
    public class Mat33VMember : Matrix33MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Mat33V"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Mat33VMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // Mat33VMember

    #endregion // Mat33V Member

    #endregion // 3x3-Component Matrix Member Classes

    #region 3x4-Component Matrix Member Classes

    #region Base

    /// <summary>
    /// 
    /// </summary>
    public class Matrix34MemberBase :
        MemberBase,
        IMemberInitialValue<float[]>,
        IMemberRange<float>,
        IMemberStep<float>,
        IMemberDecimalPlace
    {
        #region Fields

        private float[] m_init;
        private float m_minimum;
        private float m_maximum;
        private float m_step;
        private int m_decimalPlaces;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public float[] Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (float[])newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Minimum
        {
            get { return m_minimum; }
            set
            {
                SetPropertyValue(value, m_minimum, () => this.Minimum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minimum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Maximum
        {
            get { return m_maximum; }
            set
            {
                SetPropertyValue(value, m_maximum, () => this.Maximum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Step
        {
            get { return m_step; }
            set
            {
                SetPropertyValue(value, m_step, () => this.Step,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_step = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        public int DecimalPlaces
        {
            get { return m_decimalPlaces; }
            set
            {
                SetPropertyValue(value, m_decimalPlaces, () => this.DecimalPlaces,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_decimalPlaces = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Matrix34"; } }

        #endregion // Properties

        #region Constructors        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Matrix34MemberBase(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructors
        
        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = new float[12] { 0.0f, 0.0f, 0.0f, 0.0f,
                                        0.0f, 0.0f, 0.0f, 0.0f,
                                        0.0f, 0.0f, 0.0f, 0.0f };
            this.m_maximum = float.MaxValue / 1000.0f;
            this.m_minimum = float.MinValue / 1000.0f;
            this.m_step = 0.1f;
            this.m_decimalPlaces = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<float[]>.Parse(String s, ref float[] parseResult)
        {
            return Util.Parser.GetMulitpleFloatValuesFromString(s, this, out parseResult, 12, 0.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMin(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, -float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMax(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<float>.Parse(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, 0.1f);
        }

        /// <summary>
        /// 
        /// </summary>
        Boolean IMemberDecimalPlace.Parse(String s, ref int parseResult)
        {
            return Util.Parser.GetIntegerValue(s, this, out parseResult, -1);
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XPathNavigator nav)
        {
            ParseMemberData(nav);
            ParseMemberInitialValueData<float[]>(nav);
            ParseMemberRangeData<float>(nav);
            ParseMemberStepData<float>(nav);
            ParseMemberDecimalPlaceData(nav);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<float[]>(reader);
            ParseMemberRangeData<float>(reader);
            ParseMemberStepData<float>(reader);
            ParseMemberDecimalPlaceData(reader);

            reader.ReadStartElement();
            if (reader.NodeType == XmlNodeType.Text)
                reader.ReadString();
            if (!isEmpty && reader.NodeType == XmlNodeType.EndElement)
                reader.ReadEndElement();
        }

        #endregion // Private Methods
    }

    #endregion // Base

    #region Matrix34 Member

    /// <summary>
    /// 
    /// </summary>
    public class Matrix34Member : Matrix34MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Matrix34"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Matrix34Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // Matrix34Member

    #endregion // Matrix34 Member

    #region Mat34V Member

    /// <summary>
    /// 
    /// </summary>
    public class Mat34VMember : Matrix34MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Mat34V"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Mat34VMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // Mat34VMember

    #endregion // Mat34V Member

    #endregion // 3x4-Component Matrix Member Classes

    #region 4x4-Component Matrix Member Classes

    #region Base

    /// <summary>
    /// 
    /// </summary>
    public class Matrix44MemberBase :
        MemberBase,
        IMemberInitialValue<float[]>,
        IMemberRange<float>,
        IMemberStep<float>,
        IMemberDecimalPlace
    {
        #region Fields

        private float[] m_init;
        private float m_minimum;
        private float m_maximum;
        private float m_step;
        private int m_decimalPlaces;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public float[] Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (float[])newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Minimum
        {
            get { return m_minimum; }
            set
            {
                SetPropertyValue(value, m_minimum, () => this.Minimum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minimum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Maximum
        {
            get { return m_maximum; }
            set
            {
                SetPropertyValue(value, m_maximum, () => this.Maximum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Step
        {
            get { return m_step; }
            set
            {
                SetPropertyValue(value, m_step, () => this.Step,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_step = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        public int DecimalPlaces
        {
            get { return m_decimalPlaces; }
            set
            {
                SetPropertyValue(value, m_decimalPlaces, () => this.DecimalPlaces,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_decimalPlaces = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Matrix44"; } }

        #endregion // Properties

        #region Constructors        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Matrix44MemberBase(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructors

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = new float[16] { 0.0f, 0.0f, 0.0f, 0.0f,
                                        0.0f, 0.0f, 0.0f, 0.0f,
                                        0.0f, 0.0f, 0.0f, 0.0f,
                                        0.0f, 0.0f, 0.0f, 0.0f };
            this.m_maximum = float.MaxValue / 1000.0f;
            this.m_minimum = float.MinValue / 1000.0f;
            this.m_step = 0.1f;
            this.m_decimalPlaces = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<float[]>.Parse(String s, ref float[] parseResult)
        {
            return Util.Parser.GetMulitpleFloatValuesFromString(s, this, out parseResult, 16, 0.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMin(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, -float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMax(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<float>.Parse(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, 0.1f);
        }

        /// <summary>
        /// 
        /// </summary>
        Boolean IMemberDecimalPlace.Parse(String s, ref int parseResult)
        {
            return Util.Parser.GetIntegerValue(s, this, out parseResult, -1);
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XPathNavigator nav)
        {
            ParseMemberData(nav);
            ParseMemberInitialValueData<float[]>(nav);
            ParseMemberRangeData<float>(nav);
            ParseMemberStepData<float>(nav);
            ParseMemberDecimalPlaceData(nav);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<float[]>(reader);
            ParseMemberRangeData<float>(reader);
            ParseMemberStepData<float>(reader);
            ParseMemberDecimalPlaceData(reader);

            reader.ReadStartElement();
            if (reader.NodeType == XmlNodeType.Text)
                reader.ReadString();
            if (!isEmpty && reader.NodeType == XmlNodeType.EndElement)
                reader.ReadEndElement();
        }

        #endregion // Private Methods
    }

    #endregion // Base

    #region Matrix44 Member

    /// <summary>
    /// 
    /// </summary>
    public class Matrix44Member : Matrix44MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Matrix44"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Matrix44Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // Matrix44Member

    #endregion // Matrix44 Member

    #region Mat44V Member

    /// <summary>
    /// 
    /// </summary>
    public class Mat44VMember : Matrix44MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Mat44V"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Mat44VMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // Mat44VMember
    
    #endregion // Mat44V Member

    #endregion // 4x4-Component Matrix Member Classes
} // RSG.Metadata.Parser namespace
