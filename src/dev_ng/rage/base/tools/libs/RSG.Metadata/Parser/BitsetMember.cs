﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Model;
using RSG.Base.Logging;
using RSG.Metadata.Resources;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// 
    /// </summary>
    public class BitsetMember :
        MemberBase,
        IMemberEnumValues,
        IMemberInitialValue<String>,
        IMemberScalar
    {
        #region Enumerations

        /// <summary>
        /// 
        /// </summary>
        public enum BitsetType
        {
            Fixed8,
            Fixed16,
            Fixed32,
            Fixed,      // As 'Fixed32'
            atBitSet
        }

        #endregion // Enumerations

        #region Fields
        
        private String m_init;
        private BitsetType m_type;
        private UInt32 m_size;
        private String m_enumType;
        private Dictionary<String, Int64> m_values;
        private bool usesValues;

        #endregion // Fields

        #region Properties
        
        /// <summary>
        /// Initial value.
        /// </summary>
        [Category("Bitset"),
        Description("Initial value.")]
        public String Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Bitset"),
        Description("Bitset type; specifying number of bits.")]
        public BitsetType Type
        {
            get { return m_type; }
            set
            {
                SetPropertyValue(value, m_type, () => this.Type,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_type = (BitsetType)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Size (in bits) of the bitset.
        /// </summary>
        [Category("Bitset"),
        Description("Size (in bits) of the bitset.")]
        public UInt32 Size
        {
            get { return m_size; }
            set
            {
                SetPropertyValue(value, m_size, () => this.Size,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_size = (UInt32)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Enum to use for the bit names (optional).
        /// </summary>
        [Category("Bitset"),
        Description("Enum to use for the bit names (optional).")]
        public String EnumType
        {
            get { return m_enumType; }
            set
            {
                SetPropertyValue(value, m_enumType, () => this.EnumType,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_enumType = (String)newValue;
                        }
                ));
            }
        }

        public Enumeration EnumerationValues
        {
            get { return this.enumerationValues; }
        }

        private Enumeration enumerationValues;

        /// <summary>
        /// Enum values.
        /// </summary>
        [Category("Enum"),
        Description("Enum values.")]
        public Dictionary<String, Int64> Values
        {
            get { return m_values; }
            set
            {
                SetPropertyValue(value, m_values, () => this.Values,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_values = (Dictionary<String, Int64>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Bitset"; } }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Constructor from XML, XPathNavigator.
        /// </summary>
        /// <param name="nav"></param>
        public BitsetMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// Reset to defaults.
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_enumType = String.Empty;
            this.m_values = null;
            this.m_size = 32;
            this.m_type = BitsetType.Fixed32;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defDict"></param>
        protected override void PostprocessInternal(StructureDictionary defDict, ref Dictionary<int, Object> lookup)
        {
            if (!String.IsNullOrEmpty(this.EnumType))
            {
                Object type = null;
                lookup.TryGetValue(this.EnumType.GetHashCode(), out type);
                if (type is Enumeration)
                {
                    enumerationValues = type as Enumeration;
                    if (this.usesValues)
                    {
                        this.Size = (uint)enumerationValues.Values.Count;
                    }

                    this.m_values = new Dictionary<String, Int64>((type as Enumeration).Values);
                    if (!String.IsNullOrEmpty(this.Init))
                    {
                        String[] initparts = this.Init.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        List<String> validInitParts = new List<String>();
                        foreach (String initPart in initparts)
                        {
                            if (!(this.Values.ContainsKey(initPart)))
                            {
                                this.Dictionary.AddWarning(string.Format(SR.BitsetInvalidInitialValue, initPart, Filename, LineNumber));
                            }
                            else
                            {
                                validInitParts.Add(initPart);
                            }
                        }

                        this.m_init = String.Empty;
                        foreach (String validInitPart in validInitParts)
                        {
                            this.m_init += validInitPart + " ";
                        }
                        this.m_init = this.Init.Trim();
                    }
                }
                else
                {
                    this.Dictionary.AddError(string.Format(SR.BitsetUnresolvedRef, this.EnumType, Filename, LineNumber));
                }
            }
        }

        /// <summary>
        /// Validates this member and returns a value indicating whether this member is
        /// safe to instance inside a metadata document.
        /// </summary>
        /// <returns>
        /// True if this member is valid and can be instanced; otherwise, false.
        /// </returns>
        public override bool CanBeInstanced()
        {
            bool result = base.CanBeInstanced();
            if (string.IsNullOrWhiteSpace(this.EnumType))
            {
                return result;
            }
         
            if (this.Values == null)
            {
                Metadata.Log.Error(
                    "Unable to instance a bitset member whose enumeration of type '{0}' is unresolved.\nLocation: {1} Line {2} Position {3}",
                    this.EnumType,
                    this.Filename,
                    this.LineNumber,
                    this.LinePosition);
                result = false;
            }

            return result;
        }
        #endregion // Controller Methods

        #region Private Methods
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<String>(reader);
            ParseAttributes(reader);

            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            this.EnumType = reader.GetAttribute("values");
            this.EnumType = this.EnumType == null ? String.Empty : this.EnumType;

            String numBits = reader.GetAttribute("numBits");
            if (!String.IsNullOrEmpty(numBits) && !String.IsNullOrWhiteSpace(numBits))
            {
                if (numBits == "use_values")
                {
                    this.usesValues = true;
                }
                else
                {
                    int result = 0;
                    if (Util.Parser.GetIntegerValue(numBits, this, out result, 0))
                        this.Size = (uint)result;
                }
            }

            String type = reader.GetAttribute("type");
            switch (type)
            {
                case "fixed8":
                    this.Type = BitsetType.Fixed8;
                    this.Size = 8;
                    break;
                case "fixed16":
                    this.Type = BitsetType.Fixed16;
                    this.Size = 16;
                    break;
                case "fixed32":
                case "fixed":
                    this.Type = BitsetType.Fixed32;
                    this.Size = 32;
                    break;
                case "atBitSet":
                    this.Type = BitsetType.atBitSet;
                    break;
                default:
                    {
                        string msg;
                        if (type == null)
                        {
                            msg = string.Format(SR.BitsetTypeMissing, type, Filename, LineNumber);
                        }
                        else
                        {
                            msg = string.Format(SR.BitsetTypeInvalid, type, Filename, LineNumber);
                        }

                        this.Dictionary.AddError(msg);
                    }
                    break;
            }

            reader.ReadStartElement();
        }

        #endregion // Private Methods
       
        #region Public Functions

        /// <summary>
        /// 
        /// </summary>
        Boolean IMemberInitialValue<String>.Parse(String s, ref String parseResult)
        {
            parseResult = (s.Clone() as String);
            return true;
        }

        #endregion // Public Methods
    } // BitsetMember
} // RSG.Metadata.Parser namespace
