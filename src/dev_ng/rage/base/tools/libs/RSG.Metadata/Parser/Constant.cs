﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Logging;
using RSG.Metadata.Model;
using RSG.Metadata.Resources;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// parCodeGen Constant Definition data (const).
    /// </summary>
    public class Constant
    {
        #region Constants

        public const String CONST_ROOT_NAME = "const";
        public const String ATTR_NAME = "name";
        public const String ATTR_VALUE = "value";

        #endregion // Constants

        #region Properties and Associated Member Data

        /// <summary>
        /// Source filename for this enumeration definition; tool may serialise
        /// this enumeration elsewhere.
        /// </summary>
        [Category("Metadata"),
        Description("Source filename for this enumeration definition.")]
        public String Filename
        {
            get { return m_sFilename; }
            private set { m_sFilename = value; }
        }
        private String m_sFilename;

        /// <summary>
        /// Line number in the source filename for this structure definition.
        /// </summary>
        [Category("Metadata"),
        Description("Line number in source filename for this structure definition.")]
        public int LineNumber
        {
            get { return m_lineNumber; }
            private set { m_lineNumber = value; }
        }
        private int m_lineNumber;


        /// <summary>
        /// Name that this constant will be referred to as by the parser.
        /// </summary>
        /// Example: "MAX_PROPS_PER_PED".
        /// 
        [Category("Metadata"),
        Description("C++ type identifier that this XML metadata corresponds to.")]
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Value associated with this constant.
        /// </summary>
        /// Example: "3".
        /// 
        [Category("Metadata"),
        Description("C++ type identifier that this XML metadata corresponds to.")]
        public double Value
        {
            get;
            set;
        }

        #endregion // Properties and Associated Member Data
        
        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// This is deliberately non-public; please use the static factory
        /// methods or another constructor.
        Constant()
        {
            SetDefaults();
        }

        #endregion // Constructor(s)

        #region Static Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static Constant Create(String filename, XmlTextReader reader)
        {
            Constant c = new Constant();
            c.Filename = filename;
            c.Deserialise(reader);
            return c;
        }

        #endregion // Static Controller Methods

        #region Controller Methods
        /// <summary>
        /// Initialise object with default values.
        /// </summary>
        public void SetDefaults()
        {
            this.m_sFilename = String.Empty;
            this.Name = String.Empty;
            this.Value = 0.0;
        }

        #region Serialisation / Deserialisation Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <exception cref="ParserException" />
        public void Serialise(XmlDocument document)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        public void Deserialise(XmlTextReader reader)
        {
            try
            {
                LoadFrom(reader);
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception deserialising constant from XML document {0}.  See InnerException.",
                    this.Filename);
                throw new ParserException(message, ex);
            }
        }

        #endregion // Serialisation / Deserialisation Methods
        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        private void LoadFrom(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            this.LineNumber = reader.LineNumber;
            ParseAttributes(reader);

            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Deserialise enumdef node attributes.
        /// </summary>
        /// <param name="nav"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            int lineNumber = reader.LineNumber;

            this.Name = reader.GetAttribute(ATTR_NAME);
            if (this.Name == null)
            {
                string msg = string.Format(SR.ConstNameMissing, this.Filename, this.LineNumber);
            }
            else if (string.IsNullOrWhiteSpace(this.Name))
            {
                string msg = string.Format(SR.ConstNameInvalid, this.Name, this.Filename, this.LineNumber);
            }
            
            double value = 0;
            string valueStr = reader.GetAttribute(ATTR_VALUE);
            if (valueStr == null)
            {
                string msg = string.Format(SR.ConstValueMissing, this.Filename, this.LineNumber);
            }
            else
            {
                if (!Double.TryParse(reader.GetAttribute(ATTR_VALUE), out value))
                {
                    string msg = string.Format(SR.ConstValueInvalid, valueStr, this.Filename, this.LineNumber);
                }
            }

            this.Value = value;
            reader.ReadStartElement();
        }

        #endregion // Private Methods
    } // Constant
}
