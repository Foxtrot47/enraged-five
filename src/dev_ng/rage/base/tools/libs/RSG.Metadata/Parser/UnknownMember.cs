﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// Unknown member class.
    /// </summary>
    /// The Name property is automatically generated in the constructor as its
    /// used elsewhere as a IDictionary key.
    public class UnknownMember : MemberBase
    {
        #region Properties
        /// <summary>
        /// Gets the type that was unrecognised to create this member.
        /// </summary>
        public string UnrecognisedType { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Unknown"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public UnknownMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            this.UnrecognisedType = reader.Name;
            reader.Skip();
        }

        #endregion // Constructor(s)
    } // UnknownMember
} // RSG.Metadata.Parser namespace
