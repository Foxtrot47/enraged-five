﻿using System;
using System.Activities.Presentation.PropertyEditing;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Metadata.Model;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// String member class.
    /// </summary>
    public class StringMember : 
        MemberBase,
        IMemberInitialValue<String>
    {
        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        public enum StringType
        {
            Pointer,
            Member,
            STL_String,
            RAGE_ConstString,
            RAGE_atString,
            WidePointer,
            WideMember,
            RAGE_atWideString,
        }

        /// <summary>
        /// 
        /// </summary>
        public enum StringSourceType
        {
            None,
            EnumDefinition,
            Directory,
            ParentMap,
        }
        #endregion // Enumerations

        #region Fields
        private String m_init;
        private int m_size;
        private StringType m_charType;
        private string m_rawPossibleValues;
        private StringSourceType sourceType;
        #endregion // Fields

        #region Properties

        /// <summary>
        /// Initial value.
        /// </summary>
        [Category("String"),
        Description("Initial string value.")]
        public String Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Number of characters in string; depending on type.
        /// </summary>
        [Category("String"),
        Description("Number of characters in string; depending on type."),
        Editor(typeof(RSG.Base.Windows.Controls.TypeEditors.NumericUpDownEditor), typeof(PropertyValueEditor))]
        public int Size
        {
            get { return m_size; }
            set
            {
                SetPropertyValue(value, m_size, () => this.Size,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_size = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Type of string, see StringType enumeration.
        /// </summary>
        [Category("String"),
        Description("String type.")]
        public StringType CharType
        {
            get { return m_charType; }
            set
            {
                SetPropertyValue(value, m_charType, () => this.CharType,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_charType = (StringType)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName
        { 
            get { return "String"; }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<string> PossibleValues
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public StringSourceType SourceType
        {
            get { return sourceType; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public StringMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }
        #endregion // Constructor(s)

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
        }

        /// <summary>
        /// 
        /// </summary>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<String>(reader);

            m_rawPossibleValues = reader.GetAttribute("ui_values");
            sourceType = StringSourceType.None;
            if (m_rawPossibleValues != null)
            {
                if (m_rawPossibleValues.StartsWith("{enumdef/") && m_rawPossibleValues.EndsWith("}"))
                {
                    sourceType = StringSourceType.EnumDefinition;
                }
                else if (m_rawPossibleValues.StartsWith("{filenames/") && m_rawPossibleValues.EndsWith("}"))
                {
                    sourceType = StringSourceType.Directory;
                }
                else if (m_rawPossibleValues.StartsWith("{parentmapkey/") && m_rawPossibleValues.EndsWith("}"))
                {
                    sourceType = StringSourceType.ParentMap;
                }
            }
            if (sourceType != StringSourceType.None)
                this.PossibleValues = new ObservableCollection<string>();

            reader.ReadStartElement();
            if (reader.NodeType == XmlNodeType.Text)
                reader.ReadString();
            if (!isEmpty && reader.NodeType == XmlNodeType.EndElement)
                reader.ReadEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defDict"></param>
        protected override void PostprocessInternal(StructureDictionary defDict, ref Dictionary<int, Object> lookup)
        {
            if (this.PossibleValues == null || sourceType != StringSourceType.EnumDefinition)
                return;

            Object type = null;
            string source = m_rawPossibleValues.Replace("{enumdef/", "").Replace("}", "").Trim();
            lookup.TryGetValue(source.GetHashCode(), out type);
            if (type is Enumeration)
            {
                foreach (string constant in (type as Enumeration).Values.Keys)
                {
                    this.PossibleValues.Add(constant);
                }
            }
        }

        #endregion // Private Methods

        #region Public Functions

        /// <summary>
        /// 
        /// </summary>
        Boolean IMemberInitialValue<String>.Parse(String s, ref String parseResult)
        {
            parseResult = (s.Clone() as String);
            return true;
        }

        public void CreateDirectoryPossibleValues(IBranch branch)
        {
            if (sourceType != StringSourceType.Directory)
                return;

            string source = m_rawPossibleValues.Replace("{filenames/", "").Replace("}", "").Trim();
            string[] sourceParts = source.Split(new char[] {'\''}, StringSplitOptions.RemoveEmptyEntries);
            string sourceFilename = sourceParts[0];
            bool canBeEmpty = false;
            bool recursive = false;
            bool fullpath = false;
            if (sourceParts.Length > 1)
            {
                string[] parameters = sourceParts[1].Split(' ');
                foreach (string parameter in parameters)
                {
                    if (parameter.StartsWith("CanBeEmpty"))
                    {
                        string parameterValue = parameter.Replace("CanBeEmpty", "");
                        parameterValue = parameterValue.Replace("=", "");
                        parameterValue = parameterValue.Replace("\"", "");
                        parameterValue = parameterValue.Trim();

                        bool.TryParse(parameterValue, out canBeEmpty);
                    }
                    else if (parameter.StartsWith("Recursive"))
                    {
                        string parameterValue = parameter.Replace("Recursive", "");
                        parameterValue = parameterValue.Replace("=", "");
                        parameterValue = parameterValue.Replace("\"", "");
                        parameterValue = parameterValue.Trim();

                        bool.TryParse(parameterValue, out recursive);
                    }
                    else if (parameter.StartsWith("FullPath"))
                    {
                        string parameterValue = parameter.Replace("FullPath", "");
                        parameterValue = parameterValue.Replace("=", "");
                        parameterValue = parameterValue.Replace("\"", "");
                        parameterValue = parameterValue.Trim();

                        bool.TryParse(parameterValue, out fullpath);
                    }
                }
            }

            string directory = Path.GetDirectoryName(sourceFilename);
            string pattern = Path.GetFileName(sourceFilename);
            if (!pattern.Contains("."))
            {
                directory = sourceFilename;
                pattern = "*.*";
            }
            directory = branch.Environment.Subst(directory);

            if (Directory.Exists(directory))
            {
                List<string> filenames = null;
                SearchOption option = recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
                filenames = Directory.GetFiles(directory, pattern, option).ToList();

                List<string> names = new List<string>();
                foreach (var filename in filenames)
                {
                    string name = string.Empty;
                    if (fullpath)
                    {
                        name = Path.Combine(
                            Path.GetDirectoryName(filename),
                            Path.GetFileNameWithoutExtension(filename));

                        // TODO: This is a dirty hack required because RageBuilder doesn't support $() and requires this
                        // to be ${RS_ASSETS}.
                        name = name.Replace(branch.Assets, "$(core_assets)");
                        name = name.Replace("\\", "/");
                    }
                    else
                    {
                        name = Path.GetFileNameWithoutExtension(filename);
#warning DHM FIX ME: wtf?  Path.GetDirectoryName, combine with name?
                        int index = name.IndexOf('.');
                        if (index != -1)
                        {
                            name = name.Substring(0, index);
                        }
                    }

                    names.Add(name);
                    if (!this.PossibleValues.Contains(name))
                    {
                        this.PossibleValues.Add(name);
                    }
                }

                List<string> removeList = new List<string>();
                foreach (var value in this.PossibleValues)
                {
                    if (!names.Contains(value))
                        removeList.Add(value);
                }

                foreach (var value in removeList)
                {
                    this.PossibleValues.Remove(value);
                }

                if (canBeEmpty)
                {
                    if (!this.PossibleValues.Contains(string.Empty))
                    {
                        this.PossibleValues.Insert(0, string.Empty);
                    }
                }
            }
        }

        public string GetParentMapNameForValues()
        {
            if (sourceType != StringSourceType.ParentMap)
                return string.Empty;

            string source = m_rawPossibleValues.Replace("{parentmapkey/", "").Replace("}", "").Trim();
            string[] sourceParts = source.Split(new char[] { '\'' }, StringSplitOptions.RemoveEmptyEntries);
            return sourceParts[0];
        }

        public bool DoUIValuesIncludeEmpty()
        {
            string source;
            string[] sourceParts = null;
            if (m_rawPossibleValues.StartsWith("{filenames/") && m_rawPossibleValues.EndsWith("}"))
            {
                source = m_rawPossibleValues.Replace("{filenames/", "").Replace("}", "").Trim();
                sourceParts = source.Split(new char[] { '\'' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (m_rawPossibleValues.StartsWith("{parentmapkey/") && m_rawPossibleValues.EndsWith("}"))
            {
                source = m_rawPossibleValues.Replace("{parentmapkey/", "").Replace("}", "").Trim();
                sourceParts = source.Split(new char[] { '\'' }, StringSplitOptions.RemoveEmptyEntries);
            }

            bool canBeEmpty = false;
            if (sourceParts.Length > 1)
            {
                string[] parameters = sourceParts[1].Split(' ');
                foreach (string parameter in parameters)
                {
                    if (parameter.StartsWith("CanBeEmpty"))
                    {
                        string parameterValue = parameter.Replace("CanBeEmpty", "");
                        parameterValue = parameterValue.Replace("=", "");
                        parameterValue = parameterValue.Replace("\"", "");
                        parameterValue = parameterValue.Trim();

                        bool.TryParse(parameterValue, out canBeEmpty);
                    }
                }
            }

            return canBeEmpty;
        }
        #endregion // Public Methods
    } // StringMember
} // RSG.Metadata.Parser
