﻿using System;
using System.ComponentModel;
using System.Windows.Media;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Model;
using RSG.Base.Logging;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// Color32 member class.
    /// </summary>
    public class Color32Member :
        MemberBase,
        IMemberInitialValue<Color>,
        IMemberScalar
    {
        #region Fields

        private Color m_init;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Initial value.
        /// </summary>
        [Category("Colour"),
        Description("Initial colour value.")]
        public Color Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (Color)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Colour32"; } }

        #endregion // Properties

        #region Constructor(s)
        
        /// <summary>
        /// Constructor from XML, XPathNavigator.
        /// </summary>
        /// <param name="nav"></param>
        public Color32Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// Reset to defaults.
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = default(Color);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<Color>.Parse(String s, ref Color parseResult)
        {
            return Util.Parser.GetColourValue(s, this, out parseResult, Colors.Black);
        }

        #endregion // Controller Methods

        #region Private Methods
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<Color>(reader);

            reader.ReadStartElement();
            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        #endregion // Private Methods
    } // Color32Member
} // RSG.Metadata.Parser namespace
