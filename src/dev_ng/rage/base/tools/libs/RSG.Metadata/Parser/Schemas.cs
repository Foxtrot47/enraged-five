﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using RSG.Base.Configuration;

namespace RSG.Metadata.Parser
{

    /// <summary>
    /// parCodeGen Schema Validation.
    /// </summary>
    [Obsolete("Probably never used.")]
    internal static class Schemas
    {
        public static readonly XmlReaderSettings DefinitionSettings = null;
        public static readonly XmlReaderSettings MetadataSettings = null;

        /// <summary>
        /// Default (static) constructor.
        /// </summary>
        static Schemas()
        {
#warning DHM FIX ME: not branch aware!
            IConfig config = ConfigFactory.CreateConfig();
            IBranch branch = config.Project.DefaultBranch;

            DefinitionSettings = new XmlReaderSettings();
            DefinitionSettings.Schemas.Add(
                "http://www.rockstargames.com/RageParserSchema",
                Path.Combine(branch.Metadata, "metadata", "schema", "parsermetadata.xsd"));
            DefinitionSettings.ValidationType = ValidationType.Schema;

            MetadataSettings = new XmlReaderSettings();

#warning DHM FIX ME - add .meta file schema XSD
        }
    }

} // RSG.Metadata.Parser namespace
