﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Metadata.Model;
using System.IO;
using RSG.Metadata.Resources;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// parCodeGen Structure Definition data (structdef).
    /// </summary>
    public class Structure :
        IEnumerable<KeyValuePair<String, IMember>>, IXmlLineInfo
    {
        #region Fields
        public const String STRUCTDEF_ROOT_NAME = "structdef";
        private const String STRUCTDEF_TYPE = "type";
        private const String STRUCTDEF_NAME = "name";
        private const String STRUCTDEF_BASE = "base";
        private const String STRUCTDEF_VERSION = "version";
        private const String STRUCTDEF_CONSTRUCTABLE = "constructable";
        private const String STRUCTDEF_USESDATAINHERITANCE = "usesdi";
        private const String STRUCTDEF_HIDEELEMENT = "hide";
        private const String STRUCTDEF_AUTOREGISTER = "autoregister";
        private const String STRUCTDEF_TEMPLATE = "template";
        private const String STRUCTDEF_EVENT_PRELOAD = "onPreLoad";
        private const String STRUCTDEF_EVENT_POSTLOAD = "onPostLoad";
        private const String STRUCTDEF_EVENT_PRESAVE = "onPreSave";
        private const String STRUCTDEF_EVENT_POSTSAVE = "onPostSave";
        private const String STRUCTDEF_EVENT_PREADDWIDGETS = "onPreAddWidgets";
        private const String STRUCTDEF_EVENT_POSTADDWIDGETS = "onPostAddWidgets";
        private const String STRUCTDEF_EVENT_PRESET = "onPreSet";
        private const String STRUCTDEF_EVENT_POSTSET = "onPostSet";
        private const String ATTR_UI_LIVEEDIT = "ui_liveedit";

        /// <summary>
        /// Default major version number.
        /// </summary>
        private const int VERSION_DEFAULT = 0;

        /// <summary>
        /// Default Constructable value.
        /// </summary>
        private const bool CONSTRUCTABLE_DEFAULT = true;

        /// <summary>
        /// Default UsesDataInheritance value.
        /// </summary>
        private const bool USESDATAINHERITANCE_DEFAULT = false;

        /// <summary>
        /// Default HideElement value.
        /// </summary>
        private const bool HIDEELEMENT_DEFAULT = false;

        /// <summary>
        /// Default AutoRegister value.
        /// </summary>
        private const bool AUTOREGISTER_DEFAULT = true;

        /// <summary>
        /// Default IsTemplate value.
        /// </summary>
        private const bool TEMPLATE_DEFAULT = false;

        /// <summary>
        /// 
        /// </summary>
        public const String FILE_EXTENSION = "psc";

        private String m_sFilename;

        private int m_lineNumber;

        private int linePosition;

        private IBranch branch;

        private string cachedXml;

        private StructureDictionary dictionary;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public StructureDictionary Dictionary
        {
            get { return this.dictionary; }
        }

        /// <summary>
        /// Source filename for this structure definition; tool may serialise
        /// this structure elsewhere.
        /// </summary>
        [Category("Metadata"),
        Description("Source filename for this structure definition.")]
        public String Filename
        {
            get { return m_sFilename; }
            private set { m_sFilename = value; }
        }

        /// <summary>
        /// Line number in the source filename for this structure definition.
        /// </summary>
        [Category("Metadata"),
        Description("Line number in source filename for this structure definition.")]
        public int LineNumber
        {
            get { return m_lineNumber; }
            private set { m_lineNumber = value; }
        }

        /// <summary>
        /// Line position in the source filename for this structure definition.
        /// </summary>
        [Category("Metadata"),
        Description("Line position in source filename for this structure definition.")]
        public int LinePosition
        {
            get { return linePosition; }
            private set { linePosition = value; }
        }

        /// <summary>
        /// Full C++ type identifier that this XML metadata corresponds to.
        /// </summary>
        /// <seealso cref="ShortDataType" />
        /// Example: "::rage::evtInstancePrintf".
        /// 
        [Category("Metadata"),
        Description("Full C++ type identifier that this XML metadata corresponds to.")]
        public String DataType
        {
            get;
            private set;
        }

        /// <summary>
        /// Short C++ type identifier; excluding any namespaces.
        /// </summary>
        /// <seealso cref="DataType" />
        /// Example: "evtInstancePrintf".
        /// 
        [Category("Metadata"),
        Description("C++ type identifier that this XML metadata corresponds to.")]
        public String ShortDataType
        {
            get;
            private set;
        }

        /// <summary>
        /// Owning namespace object.
        /// </summary>
        [Browsable(false)]
        public Namespace Namespace
        {
            get;
            internal set;
        }

        /// <summary>
        /// Name that this type will be referred to as by the parser (default: Type).
        /// </summary>
        /// Example: "EventInstance".
        /// 
        [Category("Metadata"),
        Description("Name that this type will be referred to as by the parser (default: Type).")]
        public String Name
        {
            get;
            set;
        }
        
        /// <summary>
        /// (single) C++ classname that is a base class for the one being described here.
        /// </summary>
        /// Example: "::rage::evtInstance".
        /// 
        [Category("Metadata"),
        Description("(single) C++ class that is a base class for the one being described here.")]
        public String BaseDataType
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Metadata"),
        Description("Value indicating whether the structure can be edited live.")]
        public bool CanLiveEdit
        {
            get;
            protected set;
        }

        /// <summary>
        /// (single) Structure reference that is a base class for the one being described here.
        /// </summary>
        [Category("Metadata"),
        Description("(single) Structure reference that is a base class for the one being described here.")]
        public Structure Base
        {
            get
            {
                if (this.dictionary == null)
                {
                    return null;
                }

                return (from s in this.dictionary.Values
                        where s.DataType == this.BaseDataType
                        select s).FirstOrDefault();
            }
        }

        /// <summary>
        /// List of immediate descendant classes; useful for iteration.
        /// </summary>
        [Category("Metadata"),
        Description("List of immediate descendant classes.")]
        public IEnumerable<Structure> ImmediateDescendants
        {
            get
            {
                if (this.dictionary == null)
                {
                    return null;
                }

                return from s in this.dictionary.Values
                       where s.BaseDataType == this.DataType
                       select s;
            }
        }

        /// <summary>
        /// Major version number of the structure (default: 0).
        /// </summary>
        [Category("Metadata"),
        DefaultValue(VERSION_DEFAULT),
        Description("Major version number of the structure (default: 0).")]
        public int Version
        {
            get;
            set;
        }

        /// <summary>
        /// Prevents the parser from being able to construct one of these objects (default: true).
        /// </summary>
        [Category("Metadata"),
        DefaultValue(CONSTRUCTABLE_DEFAULT),
        Description("Prevents the parser from being able to construct one of these objects.")]
        public bool Constuctable
        {
            get;
            set;
        }

        /// <summary>
        /// Enables data inheritance features for this structure (default: false).
        /// </summary>
        [Category("Metadata"),
        DefaultValue(USESDATAINHERITANCE_DEFAULT),
        Description("Enables data inheritance features for this structure.")]
        public bool UsesDataInheritance
        {
            get;
            set;
        }

        /// <summary>
        /// Enables data inheritance features for this structure (default: false).
        /// </summary>
        [Category("Metadata"),
        DefaultValue(HIDEELEMENT_DEFAULT),
        Description("Enables hididng this element for this structure.")]
        public bool HideElement
        {
            get;
            set;
        }            
        

        /// <summary>
        /// Automatically register this structure definition with the parser (default: false).
        /// </summary>
        [Category("Metadata"),
        DefaultValue(AUTOREGISTER_DEFAULT),
        Description("Automatically register this structure definition with the parser.")]
        public bool AutoRegister
        {
            get;
            set;
        }

        /// <summary>
        /// Set if the structure definition describes a template specialisation (default: false).
        /// </summary>
        [Category("Metadata"),
        DefaultValue(TEMPLATE_DEFAULT),
        Description("Set if the structure definition describes a template specialisation.")]
        public bool IsTemplate
        {
            get;
            set;
        }
        
        /// <summary>
        /// Structure data members, indexed by member name.
        /// </summary>
        [Category("Metadata"),
        Description("Structure data members, indexed by member name.")]
        public Dictionary<String, IMember> Members
        {
            get
            {
                this.EnsureLoaded();
                return m_Members;
            }

            protected set
            {
                m_Members = value;
            }
        }
        private Dictionary<String, IMember> m_Members;

        /// <summary>
        /// Structure triggers; asserts and reports.
        /// </summary>
        [Category("Metadata"),
        Description("Structure triggers; asserts and reports.")]
        public List<ITrigger> Triggers
        {
            get { return m_Triggers; }
            protected set { m_Triggers = value; }
        }
        private List<ITrigger> m_Triggers;

        /// <summary>
        /// The IMember that can be though of as the key to the struct def
        /// </summary>
        [Browsable(false)]
        public IMember MemberKey
        {
            get { return m_memberKey; }
            protected set { m_memberKey = value; }
        }
        private IMember m_memberKey;

        #region Metadata Structure User-Defined Events

        /// <summary>
        /// Function that gets called before loading this structure.
        /// </summary>
        [Category("Metadata Events"),
        DefaultValue(""),
        Description("Function that gets called before loading this structure.")]
        public String PreLoadCallback 
        {
            get;
            set;
        }

        /// <summary>
        /// Function that gets called after loading this structure.
        /// </summary>
        [Category("Metadata Events"),
        DefaultValue(""),
        Description("Function that gets called after loading this structure.")]
        public String PostLoadCallback
        {
            get;
            set;
        }

        /// <summary>
        /// Function that gets called before saving this structure.
        /// </summary>
        [Category("Metadata Events"),
        DefaultValue(""),
        Description("Function that gets called before saving this structure.")]
        public String PreSaveCallback
        {
            get;
            set;
        }

        /// <summary>
        /// Function that gets called after saving this structure.
        /// </summary>
        [Category("Metadata Events"),
        DefaultValue(""),
        Description("Function that gets called after saving this structure.")]
        public String PostSaveCallback
        {
            get;
            set;
        }

        /// <summary>
        /// Function that gets called before adding widgets for this structure.
        /// </summary>
        [Category("Metadata Events"),
        DefaultValue(""),
        Description("Function that gets called before adding widgets for this structure.")]
        public String PreAddWidgetsCallback
        {
            get;
            set;
        }

        /// <summary>
        /// Function that gets called after adding widgets for this structure.
        /// </summary>
        [Category("Metadata Events"),
        DefaultValue(""),
        Description("Function that gets called after adding widgets for this structure.")]
        public String PostAddWidgetsCallback
        {
            get;
            set;
        }

        /// <summary>
        /// Function that gets called for each member of the structure, before that member is read.
        /// </summary>
        [Category("Metadata Events"),
        DefaultValue(""),
        Description("Function that gets called for each member of the structure, before that member is set.")]
        public String PreSetCallback
        {
            get;
            set;
        }

        /// <summary>
        /// Function that gets called for each member of the structure, after that member is read.
        /// </summary>
        [Category("Metadata Events"),
        DefaultValue(""),
        Description("Function that gets called for each member of the structure, after that member is set.")]
        public String PostSetCallback
        {
            get;
            set;
        }

        #endregion // Metadata Structure User-Defined Events

        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// This is deliberately non-public; please use the static factory 
        /// methods or another constructor.
        /// 
        Structure()
        {
            SetDefaults();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Structure(Namespace ns, String type)
        {
            SetDefaults();
            this.Namespace = ns;
            this.DataType = (type.Clone() as String);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        public Structure(Namespace ns, String type, String name)
        {
            SetDefaults();
            this.Namespace = ns;
            this.DataType = (type.Clone() as String);
            this.Name = (name.Clone() as String);
        }

        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="filename"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static Structure Create(StructureDictionary dictionary, IBranch branch, String filename, XmlTextReader reader)
        {
            Structure structure = new Structure();
            structure.dictionary = dictionary;
            structure.Filename = filename;
            structure.LineNumber = reader.LineNumber;
            structure.LinePosition = reader.LinePosition;
            structure.branch = branch;
            structure.ParseAttributes(reader);
            structure.cachedXml = reader.ReadOuterXml();
            return structure;
        }
        #endregion // Static Controller Methods

        #region Controller Methods

        #region Object Methods

        /// <summary>
        /// Determine if two Structure's are equal.
        /// </summary>
        public override bool Equals(Object obj)
        {
            if (!(obj is Structure))
                return (false);

            Structure o = (obj as Structure);
            return (o.DataType.Equals(this.DataType));
        }

        /// <summary>
        /// 
        /// </summary>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion // Object Methods

        #region IEnumerable<KeyValuePair<String, IMember>> Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<String, IMember>> GetEnumerator()
        {
            return (this.Members.GetEnumerator());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetEnumerator());
        }

        #endregion // IEnumerable<KeyValuePair<String, MemberBase>> Methods

        #region Serialisation / Deserialisation Methods

        /// <summary>
        /// 
        /// </summary>
        public void Serialise(XmlDocument document)
        {
            throw new NotImplementedException();            
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void Deserialise(IBranch branch, XmlTextReader reader)
        {
            try
            {
                LoadFrom(branch, reader);
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception deserialising structure from XML document {0}.  See InnerException.",
                    this.Filename);
                throw new ParserException(message, ex);
            }
        }

        #endregion // Serialisation / Deserialisation Methods

        /// <summary>
        /// Gets a value indicating whether the class can return line information.
        /// </summary>
        /// <returns>
        /// True if LineNumber and LinePosition can be provided; otherwise, false.
        /// </returns>
        public bool HasLineInfo()
        {
            return true;
        }

        /// <summary>
        /// Determine if this member is a subclass of the specified member;
        /// walking up the data inheritance tree.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public bool IsA(Structure s)
        {
            this.EnsureLoaded();
            // Base cases of Structure is self, or no data inheritance.
            if (this.Equals(s))
                return (true);
            if (null == this.Base)
                return (false);

            // Recurse up our class hierarchy.
            Structure parentClass = this.Base;
            while (null != parentClass)
            {
                if (parentClass.Equals(s))
                    return (true);

                parentClass = parentClass.Base;
            }
            return (false);
        }

        /// <summary>
        /// Validates this structure and returns a value indicating whether this structure is
        /// safe to instance inside a metadata document.
        /// </summary>
        /// <returns>
        /// True if this structure is valid and can be instanced; otherwise, false.
        /// </returns>
        public bool CanBeInstanced()
        {
            string location = string.Format(
                "Location: {0} Line {1} Position {2}",
                this.Filename,
                this.LineNumber,
                this.LinePosition);

            bool result = true;
            if (string.IsNullOrWhiteSpace(this.DataType))
            {
                Metadata.Log.Error("Unable to instance a structure that doesn't have a data type.\n{0}", location);
                result = false;
            }

            if (!string.IsNullOrEmpty(this.BaseDataType))
            {
                Structure baseStructure = this.Base;
                if (baseStructure == null)
                {
                    Metadata.Log.Error(
                        "Unable to instance a structure whose base structure of type '{0}' cannot be resolved.\n{1}",
                        this.BaseDataType,
                        location);
                    result = false;
                }
                else
                {
                    if (!baseStructure.CanBeInstanced())
                    {
                        result = false;
                    }
                }
            }

            foreach (KeyValuePair<string, IMember> member in this.Members)
            {
                UnknownMember unknownMember = member.Value as UnknownMember;
                if (unknownMember != null)
                {
                    Metadata.Log.Error(
                        "Unable to instance a structure that contains a member of a unknown type '{0}'.\n{1}",
                       unknownMember.UnrecognisedType,
                       location);

                    result = false;
                    continue;
                }

                if (!member.Value.CanBeInstanced())
                {
                    result = false;
                }
            }

            return result;
        }
        #endregion // Controller Methods

        #region Private Methods
        public void EnsureLoaded()
        {
            if (this.cachedXml != null)
            {
                string xml = this.cachedXml;
                this.cachedXml = null;
                using (StringReader stream = new StringReader(xml))
                {
                    using (XmlTextReader reader = new XmlTextReader(stream))
                    {
                        reader.MoveToContent();
                        if (!reader.IsEmptyElement)
                        {
                            reader.ReadStartElement();
                            this.ParseMembers(this.branch, reader);
                        }

                        Dictionary<int, object> lookupTable = new Dictionary<int, object>();
                        foreach (Structure structure in this.dictionary.Values)
                        {
                            int hashTag = structure.DataType.GetHashCode();
                            lookupTable[hashTag] = structure;
                        }

                        foreach (Enumeration enumeration in this.dictionary.Enumerations.Values)
                        {
                            int hashTag = enumeration.DataType.GetHashCode();
                            lookupTable[hashTag] = enumeration;
                        }

                        this.Postprocess(this.dictionary, ref lookupTable);
                    }
                }
            }
        }

        /// <summary>
        /// Postprocess the member.
        /// </summary>
        /// Ensures all definitions are loaded prior to attempting to complete
        /// the IMember structures.
        private void Postprocess(StructureDictionary defDict, ref Dictionary<int, Object> lookup)
        {
            // Find BaseDataType if applicable.
            if (String.Empty != this.BaseDataType)
            {
                Structure structureParent = this.Base;
                if (structureParent != null)
                {
                    Dictionary<String, IMember> allMembers = new Dictionary<String, IMember>();
                    foreach (KeyValuePair<String, IMember> entry in this.Members.Reverse())
                    {
                        if (string.IsNullOrEmpty(entry.Key) || entry.Value == null)
                        {
                            continue;
                        }

                        allMembers.Add(entry.Key, entry.Value);
                    }

                    while (structureParent != null)
                    {
                        structureParent.EnsureLoaded();
                        if (this.MemberKey == null)
                        {
                            this.MemberKey = structureParent.MemberKey;
                        }
                        foreach (KeyValuePair<String, IMember> entry in structureParent.Members.Reverse())
                        {
                            if (allMembers.ContainsKey(entry.Key))
                                continue;

                            allMembers.Add(entry.Key, entry.Value);
                        }
                        structureParent = structureParent.Base;
                    }

                    this.Members.Clear();
                    foreach (KeyValuePair<String, IMember> entry in allMembers.Reverse())
                    {
                        this.Members.Add(entry.Key, entry.Value);
                    }
                }
            }

            foreach (Structure structure in this.ImmediateDescendants)
            {
                structure.EnsureLoaded();
            }

            // Iterate through member definitions.
            foreach (IMember member in this.Members.Values)
            {
                member.Postprocess(defDict, ref lookup);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void ValidationHandler(Object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Warning:
                    Metadata.Log.WarningCtx(Consts.LOG_CONTEXT,
                        "XML validation warning: {0}.",
                        e.Message);
                    break;
                case XmlSeverityType.Error:
                    Metadata.Log.WarningCtx(Consts.LOG_CONTEXT,
                        "XML validation error: {0}.",
                        e.Message);
                    break;
            }
        }

        /// <summary>
        /// Initialise object with default values.
        /// </summary>
        private void SetDefaults()
        {
            this.Filename = String.Empty;
            this.LineNumber = -1;
            this.Name = String.Empty;
            this.BaseDataType = string.Empty;
            this.Version = VERSION_DEFAULT;
            this.Constuctable = CONSTRUCTABLE_DEFAULT;
            this.UsesDataInheritance = USESDATAINHERITANCE_DEFAULT;
            this.AutoRegister = AUTOREGISTER_DEFAULT;
            this.IsTemplate = TEMPLATE_DEFAULT;
            this.PreLoadCallback = String.Empty;
            this.PostLoadCallback = String.Empty;
            this.PreSaveCallback = String.Empty;
            this.PostSaveCallback = String.Empty;
            this.PreAddWidgetsCallback = String.Empty;
            this.PostAddWidgetsCallback = String.Empty;
            this.PreSetCallback = String.Empty;
            this.PostSetCallback = String.Empty;
            this.Members = new Dictionary<String, IMember>();
            this.Triggers = new List<ITrigger>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        private void SaveTo(out XmlDocument document)
        {
            document = new XmlDocument();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="reader"></param>
        private void LoadFrom(IBranch branch, XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseAttributes(reader);
            reader.ReadStartElement();

            if (!isEmpty)
            {
                ParseMembers(branch, reader);

                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }
        
        /// <summary>
        /// Deserialise structdef node attributes.
        /// </summary>
        /// <param name="nav"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            this.DataType = reader.GetAttribute(STRUCTDEF_TYPE);
            this.Name = this.GetConsistentName(ParseStringAttribute(STRUCTDEF_NAME, reader, String.Empty));

            int lineNumber = reader.LineNumber;
            if (String.IsNullOrEmpty(this.DataType) || String.IsNullOrWhiteSpace(this.DataType))
            {
                this.ShortDataType = "";
                this.dictionary.AddError(string.Format(SR.StructDefTypeMissing, this.Filename, this.LineNumber));
            }
            else
            {
                this.DataType = Namespace.ConvertDataTypeToFriendlyName(this.DataType);
                this.ShortDataType = Namespace.GetShortName(this.DataType);

                if (String.IsNullOrEmpty(this.Name) || String.IsNullOrWhiteSpace(this.Name))
                {
                    this.Name = this.GetConsistentName(this.DataType);
                }
            }

            string liveeditstring = reader.GetAttribute(ATTR_UI_LIVEEDIT);
            bool liveedit = true;
            if (!bool.TryParse(liveeditstring, out liveedit))
            {
                liveedit = true;
            }

            this.CanLiveEdit = liveedit;


            this.BaseDataType = Namespace.ConvertDataTypeToFriendlyName(ParseStringAttribute(STRUCTDEF_BASE, reader, string.Empty));
            this.Version = ParseIntAttribute(STRUCTDEF_VERSION, reader, VERSION_DEFAULT);
            this.Constuctable = ParseBoolAttribute(STRUCTDEF_CONSTRUCTABLE, reader, CONSTRUCTABLE_DEFAULT);
            this.UsesDataInheritance = ParseBoolAttribute(STRUCTDEF_USESDATAINHERITANCE, reader, USESDATAINHERITANCE_DEFAULT);
            this.HideElement = ParseBoolAttribute(STRUCTDEF_HIDEELEMENT, reader, HIDEELEMENT_DEFAULT);
            this.AutoRegister = ParseBoolAttribute(STRUCTDEF_AUTOREGISTER, reader, AUTOREGISTER_DEFAULT);
            this.IsTemplate = ParseBoolAttribute(STRUCTDEF_TEMPLATE, reader, TEMPLATE_DEFAULT);

            this.PreLoadCallback = ParseStringAttribute(STRUCTDEF_EVENT_PRELOAD, reader, String.Empty);
            this.PostLoadCallback = ParseStringAttribute(STRUCTDEF_EVENT_POSTLOAD, reader, String.Empty);
            this.PreSaveCallback = ParseStringAttribute(STRUCTDEF_EVENT_PRESAVE, reader, String.Empty);
            this.PostSaveCallback = ParseStringAttribute(STRUCTDEF_EVENT_POSTSAVE, reader, String.Empty);
            this.PreAddWidgetsCallback = ParseStringAttribute(STRUCTDEF_EVENT_PREADDWIDGETS, reader, String.Empty);
            this.PostAddWidgetsCallback = ParseStringAttribute(STRUCTDEF_EVENT_POSTADDWIDGETS, reader, String.Empty);
            this.PreSetCallback = ParseStringAttribute(STRUCTDEF_EVENT_PRESET, reader, String.Empty);
            this.PostSetCallback = ParseStringAttribute(STRUCTDEF_EVENT_POSTSET, reader, String.Empty);

            /*-----------------------------------------------------------------
             * Disclaimer:
             * Need to put in a hack for the texture structures to make sure that
             * they are using the usedi attribute. Once this is sorted the
             * following lines can be removed
             * ----------------------------------------------------------------*/
            if (this.ShortDataType == "CTextureConversionSpecification" ||
                this.ShortDataType == "CTextureConversionParams")
            {
                this.UsesDataInheritance = true;
            }
        }

        /// <summary>
        /// Parses a attribute as a string property and returns the result or
        /// the pased in default value if something goes wrong.
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="reader"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private String ParseStringAttribute(String attributeName, XmlTextReader reader, String defaultValue)
        {
            String stringValue = reader.GetAttribute(attributeName);
            if (String.IsNullOrEmpty(stringValue) || String.IsNullOrWhiteSpace(stringValue))
            {
                stringValue = defaultValue;
            }

            return stringValue;
        }

        /// <summary>
        /// Parses a attribute as a boolean property and returns the result or
        /// the pased in default value if something goes wrong.
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="reader"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private bool ParseBoolAttribute(String attributeName, XmlTextReader reader, bool defaultValue)
        {
            bool parsedValue = defaultValue;
            String stringValue = reader.GetAttribute(attributeName);
            if (!String.IsNullOrEmpty(stringValue) && !String.IsNullOrWhiteSpace(stringValue))
            {
                if (!Boolean.TryParse(stringValue, out parsedValue))
                {
                    this.dictionary.AddWarning(string.Format(SR.StructDefBoolParseWarning, stringValue, this.Filename, this.LineNumber));
                    parsedValue = defaultValue;
                }
            }
            return parsedValue;
        }

        /// <summary>
        /// Parses a attribute as a integer property and returns the result or
        /// the pased in default value if something goes wrong.
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="reader"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private int ParseIntAttribute(String attributeName, XmlTextReader reader, int defaultValue)
        {
            int parsedValue = defaultValue;
            String stringValue = reader.GetAttribute(attributeName);
            if (!String.IsNullOrEmpty(stringValue) && !String.IsNullOrWhiteSpace(stringValue))
            {
                if (!int.TryParse(stringValue, out parsedValue))
                {
                    this.dictionary.AddWarning(string.Format(SR.StructDefInParseWarning, stringValue, this.Filename, this.LineNumber));
                    parsedValue = defaultValue;
                }
            }
            return parsedValue;
        }

        /// <summary>
        /// Deserialise structdef child 'member' nodes.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="reader"></param>
        private void ParseMembers(IBranch branch, XmlTextReader reader)
        {
            int structLineNumber = this.LineNumber;
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                bool isKeyMember = false;
                if (this.MemberKey == null)
                {
                    String key = reader.GetAttribute("ui_key");
                    if (!String.IsNullOrEmpty(key) && !String.IsNullOrWhiteSpace(key))
                    {
                        if (!bool.TryParse(key, out isKeyMember))
                            isKeyMember = false;
                    }
                }

                int lineNumber = reader.LineNumber;
                String memberType = reader.Name;
                IMember member = MemberFactory.Create(branch, this, reader);
                if (member == null)
                {
                    reader.Skip();
                    continue;
                }

                if (isKeyMember == true)
                {
                    this.MemberKey = member;
                }

                if (!this.Members.ContainsKey(member.Name))
                {
                    this.Members.Add(member.Name, member);
                }
                else
                {
                    this.dictionary.AddError(string.Format(SR.StructDefDuplicate, member.Name, this.Filename, this.LineNumber));
                }
            }
        }

        /// <summary>
        /// Convert unfriendly type name into a more friendly name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private String GetConsistentName(String name)
        {
            String tempName = (name.Clone() as String);
            if (tempName.StartsWith("::"))
                tempName = tempName.TrimStart(new char[] { ':' });
            return (tempName.Replace("::", "__"));
        }
        #endregion // Private Methods
    } // Structure
} // RSG.Metadata.Parser
