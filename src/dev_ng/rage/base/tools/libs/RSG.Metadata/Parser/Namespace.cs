﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace RSG.Metadata.Parser
{

    /// <summary>
    /// 
    /// </summary>
    public class Namespace
    {
        #region Constants
        /// <summary>
        /// Namespace separator token for user-display.
        /// </summary>
        public static readonly String NAMESPACE_SEPARATOR_FRIENDLY = "::";

        /// <summary>
        /// Namespace separator token for serialisation (XML-friendly).
        /// </summary>
        public static readonly String NAMESPACE_SEPARATOR_SERIALISATION = "__";
        #endregion // Constants

        #region Properties and Associated Member Data

        /// <summary>
        /// Simple token name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String FullName
        {
            get
            {
                if (null == this.Parent)
                {
                    return (String.Empty);
                }
                else
                {
                    String typename = String.Empty;
                    if ((null != this.Parent) && (String.Empty != this.Parent.FullName))
                        typename = String.Format("{0}{1}{2}", this.Parent.FullName,
                            NAMESPACE_SEPARATOR_FRIENDLY, this.Name);
                    else
                        typename = this.Name;
                    return (typename);
                }
            }
        }

        /// <summary>
        /// Parent namespace object (null for root namespace).
        /// </summary>
        public Namespace Parent
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String SerialisationName
        {
            get
            {
                return (ConvertDataTypeToSerialisationName(this.FullName));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<Object> Children
        {
            get;
            private set;
        }

        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// Default constructor (root namespace).
        /// </summary>
        public Namespace()
        {
            this.Parent = null;
            this.Name = "Root";
            this.Children = new ObservableCollection<Object>();
        }

        /// <summary>
        /// Constructor, specifying name token and parent Namespace.
        /// </summary>
        /// <param name="token"></param>
        public Namespace(String token, Namespace parent)
        {
#if false
            Debug.Assert(!(token.Contains("__") || token.Contains(":")),
                "Namespace token contains invalid character(s).");
            if (token.Contains("__") || token.Contains(":"))
                throw new NotSupportedException("Namespace token contains invalid character(s).");
#endif
            this.Parent = parent;
            this.Name = (token.Clone() as String);
            this.Children = new ObservableCollection<Object>();
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="datatypeOrNamespace"></param>
        /// <returns></returns>
        public Namespace Find(String datatypeOrNamespace)
        {
            String ns = GetNamespaceName(datatypeOrNamespace);
            if (datatypeOrNamespace.Equals(this.FullName) || ns.Equals(this.FullName))
                return (this);
            
            foreach (Object o in this.Children)
            {
                if (!(o is Namespace))
                    continue;

                Namespace cn = (o as Namespace).Find(ns);
                if (null != cn)
                    return (cn);
            }
            return (null);
        }

        #endregion // Controller Methods

        #region Static Controller Methods

        /// <summary>
        /// Return new namespace chain for a String datatype identifier.
        /// </summary>
        /// <param name="datatype"></param>
        /// <returns></returns>
        public static Namespace Create(String datatype, Namespace parent)
        {
            Debug.Assert(!String.IsNullOrWhiteSpace(datatype));
            if (String.IsNullOrWhiteSpace(datatype))
                throw new ArgumentException();

            String friendlyDatatype = ConvertDataTypeToFriendlyName(datatype);
            String[] parts = friendlyDatatype.Split(new String[] { NAMESPACE_SEPARATOR_FRIENDLY },
                StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(parts.Length > 0);

            Namespace root = new Namespace(parts[0], parent);
            Namespace partParent = root;
            Namespace child = root;
            for (int n = 1; n < parts.Length; n++)
            {
                child = new Namespace(parts[n], partParent);
                partParent.Children.Add(child);
                partParent = child;
            }
            return (child);
        }

        /// <summary>
        /// Convert a String datatype identifier to a friendly-display String.
        /// </summary>
        /// <param name="datatype"></param>
        /// <returns></returns>
        public static String ConvertDataTypeToFriendlyName(String datatype)
        {
            String friendlyDataType = datatype.Replace(NAMESPACE_SEPARATOR_SERIALISATION, NAMESPACE_SEPARATOR_FRIENDLY);
            while (friendlyDataType.StartsWith(NAMESPACE_SEPARATOR_FRIENDLY))
                friendlyDataType = friendlyDataType.Remove(0, NAMESPACE_SEPARATOR_FRIENDLY.Length);
            return (friendlyDataType);
        }

        /// <summary>
        /// Convert a String datatype identifier to a serialisation String.
        /// </summary>
        /// <param name="datatype"></param>
        /// <returns></returns>
        public static String ConvertDataTypeToSerialisationName(String datatype)
        {
            return (datatype.Replace(NAMESPACE_SEPARATOR_FRIENDLY, NAMESPACE_SEPARATOR_SERIALISATION));
        }

        /// <summary>
        /// Return the shortened Datatype identifier (namespace stripped). 
        /// </summary>
        /// <param name="datatype"></param>
        /// <returns></returns>
        public static String GetShortName(String datatype)
        {
            String friendlyDatatype = ConvertDataTypeToFriendlyName(datatype);
            String[] parts = friendlyDatatype.Split(new String[] { NAMESPACE_SEPARATOR_FRIENDLY }, 
                StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(parts.Length > 0);           

            return (parts[parts.Length - 1]);
        }

        /// <summary>
        /// Return the namespace of the Datatype (shortname stripped).
        /// </summary>
        /// <param name="datatype"></param>
        /// <returns></returns>
        public static String GetNamespaceName(String datatype)
        {
            String friendlyDatatype = ConvertDataTypeToFriendlyName(datatype);
            String[] parts = friendlyDatatype.Split(new String[] { NAMESPACE_SEPARATOR_FRIENDLY },
                StringSplitOptions.RemoveEmptyEntries);
            Debug.Assert(parts.Length > 0);

            friendlyDatatype = String.Empty;
            for (int i = 0; i < parts.Length - 1; ++i)
            {
                if (i == parts.Length-2)
                {
                    friendlyDatatype += parts[i];
                }
                else
                {
                    friendlyDatatype += String.Format("{0}{1}",
                        parts[i], NAMESPACE_SEPARATOR_FRIENDLY);
                }
            }
            return (friendlyDatatype);
        }

        #endregion // Static Controller Methods
    }

} // RSG.Metadata.Parser namespace
