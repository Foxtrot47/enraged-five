﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Metadata.Model;
using RSG.Metadata.Resources;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// 
    /// </summary>
    public class StructMember :
        MemberBase,
        IMember
    {
        #region Constants
        #region Attribute Constants
        /// <summary>
        /// 
        /// </summary>
        private static readonly String ATTR_TYPE = "type";
        #endregion // Attribute Constants
        #endregion // Constants

        #region Fields

        private Structure m_definition;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Member definition field, the structure that
        /// this member belongs to
        /// </summary>
        [Category("Common"),
        Description("Member definition field.")]
        public Structure Definition
        {
            get { return m_definition; }
            set
            {
                SetPropertyValue(value, m_definition, () => this.Definition,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_definition = (Structure)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String TypeName 
        { 
            get { return Definition.Name; } 
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName 
        {
            get
            {
                Debug.Assert(null != Definition);
                return (Definition.DataType); 
            } 
        }

        #endregion // Properties

        #region Private Member Data
        /// <summary>
        /// Structure type string, stored for post-process.
        /// </summary>
        internal String m_sStructureType;
        #endregion // Private Member Data

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        public StructMember(Structure s, Structure parent)
            : base(parent)
        {
            Reset();
            this.Definition = s;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public StructMember(Structure s, XmlTextReader reader)
            : base(s, reader)
        {
            Reset();
            this.m_definition = s;
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        protected override void PostprocessInternal(StructureDictionary defDict, ref Dictionary<int, Object> lookup)
        {
            Object definition = null;
            lookup.TryGetValue(m_sStructureType.GetHashCode(), out definition);
            if (definition is Structure)
            {
                this.m_definition = definition as Structure;
            }
            else
            {
                this.Dictionary.AddError(string.Format(SR.StructUnresolvedRef, this.m_sStructureType, Filename, LineNumber));
            }
        }

        /// <summary>
        /// Validates this member and returns a value indicating whether this member is
        /// safe to instance inside a metadata document.
        /// </summary>
        /// <returns>
        /// True if this member is valid and can be instanced; otherwise, false.
        /// </returns>
        public override bool CanBeInstanced()
        {
            bool result = base.CanBeInstanced();
            if (string.IsNullOrWhiteSpace(this.m_sStructureType))
            {
                Metadata.Log.Error(
                    "Unable to instance a struct member that's missing its type attribute.\nLocation: {1} Line {2} Position {3}",
                    this.Filename,
                    this.LineNumber,
                    this.LinePosition);
                result = false;
            }
            else if (this.m_definition == null)
            {
                Metadata.Log.Error(
                    "Unable to instance a struct member whose structure of type '{0}' is unresolved.\nLocation: {1} Line {2} Position {3}",
                    m_sStructureType,
                    this.Filename,
                    this.LineNumber,
                    this.LinePosition);
                result = false;
            }

            return result;
        }
        #endregion // Controller Methods

        #region Private Methods
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseAttributes(reader);

            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            String type = reader.GetAttribute(ATTR_TYPE);
            if (!String.IsNullOrEmpty(type) && !String.IsNullOrWhiteSpace(type))
                m_sStructureType = Namespace.ConvertDataTypeToFriendlyName(type);

            reader.ReadStartElement();
        }

        #endregion // Private Methods
    } // StructMember
} // RSG.Metadata.Parser namespace
