﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// Bool member class.
    /// </summary>
    public class BoolMember :
        MemberBase,
        IMemberInitialValue<bool>
    {
        #region Fields

        private Boolean m_init;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Initial value.
        /// </summary>
        [Category("Boolean"),
        Description("Initial boolean value.")]
        public Boolean Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Boolean"; } }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public BoolMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<Boolean>.Parse(String s, ref Boolean parseResult)
        {
            return Util.Parser.GetBooleanValue(s, this, out parseResult, false);
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<bool>(reader);

            reader.ReadStartElement();
            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        #endregion // Private Methods
    } // BoolMember
} // RSG.Metadata.Parser namespace
