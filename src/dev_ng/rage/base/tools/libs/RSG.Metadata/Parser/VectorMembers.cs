﻿using System;
using System.Activities.Presentation.PropertyEditing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Metadata.Parser
{
    #region 2-Component Vector Member Classes

    #region Base Class

    public class Vector2MemberBase :
        MemberBase,
        IMemberInitialValue<float[]>,
        IMemberRange<float>,
        IMemberStep<float>,
        IMemberDecimalPlace,
        IMemberHighPrecision
    {
        #region Fields
        private float[] m_init;
        private float m_minimum;
        private float m_maximum;
        private float m_step;
        private int m_decimalPlaces;
        private bool m_highPrecision;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public float[] Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (float[])newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Minimum
        {
            get { return m_minimum; }
            set
            {
                SetPropertyValue(value, m_minimum, () => this.Minimum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minimum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Maximum
        {
            get { return m_maximum; }
            set
            {
                SetPropertyValue(value, m_maximum, () => this.Maximum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Step
        {
            get { return m_step; }
            set
            {
                SetPropertyValue(value, m_step, () => this.Step,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_step = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        public int DecimalPlaces
        {
            get { return m_decimalPlaces; }
            set
            {
                SetPropertyValue(value, m_decimalPlaces, () => this.DecimalPlaces,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_decimalPlaces = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Vector2"; } }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        [Category("Float"),
        Description("Whether or not this float needs to be serialised using 6 or 9 decimal places.")]
        public Boolean HighPrecision
        {
            get { return m_highPrecision; }
            set
            {
                SetPropertyValue(value, m_highPrecision, () => this.HighPrecision,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highPrecision = (Boolean)newValue;
                        }
                ));
            }
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Vector2MemberBase(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = new float[2] { 0.0f, 0.0f };
            this.m_maximum = float.MaxValue / 1000.0f;
            this.m_minimum = float.MinValue / 1000.0f;
            this.m_step = 0.1f;
            this.m_decimalPlaces = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<float[]>.Parse(String s, ref float[] parseResult)
        {
            return Util.Parser.GetMulitpleFloatValuesFromString(s, this, out parseResult, 2, 0.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMin(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, -float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMax(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<float>.Parse(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, 0.1f);
        }

        /// <summary>
        /// 
        /// </summary>
        Boolean IMemberDecimalPlace.Parse(String s, ref int parseResult)
        {
            return Util.Parser.GetIntegerValue(s, this, out parseResult, -1);
        }

        #endregion // Controller Methods

        #region Private Methods
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<float[]>(reader);
            ParseMemberRangeData<float>(reader);
            ParseMemberStepData<float>(reader);
            ParseMemberDecimalPlaceData(reader);
            ParseAttributes(reader); 
            
            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            bool highPrecision = false;
            String highPrecisionString = reader.GetAttribute("highPrecision");
            if (!String.IsNullOrEmpty(highPrecisionString) && !String.IsNullOrWhiteSpace(highPrecisionString))
            {
                if (!Util.Parser.GetBooleanValue(highPrecisionString, this, out highPrecision, false))
                    highPrecision = false;
            }
            this.HighPrecision = highPrecision;
            reader.ReadStartElement();
        }
        #endregion // Private Methods
    }

    #endregion // Base Class

    #region Vector2 Member

    /// <summary>
    /// 2-component floating-point vector member class.
    /// </summary>
    public class Vector2Member : Vector2MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Vector2"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Vector2Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // Vector2Member

    #endregion // Vector2 Member

    #region Vec2V Member

    /// <summary>
    /// 2-component floating-point vector member class.
    /// </summary>
    public class Vec2VMember : Vector2MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Vec2V"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Vec2VMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // Vec2VMember

    #endregion // Vec2 VMember

    #endregion // 2-Component Vector Member Classes

    #region 3-Component Vector Member Classes

    #region Base Class

    public class Vector3MemberBase :
        MemberBase,
        IMemberInitialValue<float[]>,
        IMemberRange<float>,
        IMemberStep<float>,
        IMemberDecimalPlace,
        IMemberHighPrecision
    {
        #region Enumerations

        /// <summary>
        /// 
        /// </summary>
        public enum Vector3Type
        {
            Normal, // Regular Vector
            Colour  // Vector represents a RGB colour.
        };

        #endregion // Enumerations

        #region Fields
        private float[] m_init;
        private float m_minimum;
        private float m_maximum;
        private float m_step;
        private int m_decimalPlaces;
        private Vector3Type m_vectorType;
        private bool m_highPrecision;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [Category("Vector")]
        public Vector3Type VectorType
        {
            get { return m_vectorType; }
            set
            {
                SetPropertyValue(value, m_vectorType, () => this.VectorType,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_vectorType = (Vector3Type)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float[] Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (float[])newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Minimum
        {
            get { return m_minimum; }
            set
            {
                SetPropertyValue(value, m_minimum, () => this.Minimum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minimum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Maximum
        {
            get { return m_maximum; }
            set
            {
                SetPropertyValue(value, m_maximum, () => this.Maximum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Step
        {
            get { return m_step; }
            set
            {
                SetPropertyValue(value, m_step, () => this.Step,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_step = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        public int DecimalPlaces
        {
            get { return m_decimalPlaces; }
            set
            {
                SetPropertyValue(value, m_decimalPlaces, () => this.DecimalPlaces,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_decimalPlaces = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public new String FriendlyTypeName { get; set; }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        [Category("Float"),
        Description("Whether or not this float needs to be serialised using 6 or 9 decimal places.")]
        public Boolean HighPrecision
        {
            get { return m_highPrecision; }
            set
            {
                SetPropertyValue(value, m_highPrecision, () => this.HighPrecision,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highPrecision = (Boolean)newValue;
                        }
                ));
            }
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Vector3MemberBase(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = new float[] { 0.0f, 0.0f, 0.0f };
            this.m_maximum = float.MaxValue / 1000.0f;
            this.m_minimum = float.MinValue / 1000.0f;
            this.m_step = 0.1f;
            this.m_decimalPlaces = -1;
            this.FriendlyTypeName = "Vector3";
            this.m_vectorType = Vector3Type.Normal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<float[]>.Parse(String s, ref float[] parseResult)
        {
            return Util.Parser.GetMulitpleFloatValuesFromString(s, this, out parseResult, 3, 0.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMin(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, -float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMax(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<float>.Parse(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, 0.1f);
        }

        /// <summary>
        /// 
        /// </summary>
        Boolean IMemberDecimalPlace.Parse(String s, ref int parseResult)
        {
            return Util.Parser.GetIntegerValue(s, this, out parseResult, -1);
        }

        #endregion // Controller Methods

        #region Private Methods       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<float[]>(reader);
            ParseMemberRangeData<float>(reader);
            ParseMemberStepData<float>(reader);
            ParseMemberDecimalPlaceData(reader);
            ParseAttributes(reader);

            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            bool highPrecision = false;
            String highPrecisionString = reader.GetAttribute("highPrecision");
            if (!String.IsNullOrEmpty(highPrecisionString) && !String.IsNullOrWhiteSpace(highPrecisionString))
            {
                if (!Util.Parser.GetBooleanValue(highPrecisionString, this, out highPrecision, false))
                    highPrecision = false;
            }
            this.HighPrecision = highPrecision;

            String type = reader.GetAttribute("type");
            if (type != null)
            {
                switch (type)
                {
                    case "color":
                        this.m_vectorType = Vector3Type.Colour;
                        break;
                    default:
                        this.m_vectorType = Vector3Type.Normal;
                        break;
                }
            }

            reader.ReadStartElement();
        }
        #endregion // Private Methods
    } // Vector3MemberBase

    #endregion // Base Class

    #region Vector3 Member

    /// <summary>
    /// 3-component floating-point vector member class.
    /// </summary>
    public class Vector3Member : Vector3MemberBase
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Vector3Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            if (this.VectorType == Vector3Type.Colour)
                this.FriendlyTypeName = "Vector3 (colour)";
        }

        #endregion // Constructor(s)
    } // Vector3Member

    #endregion // Vector3 Member

    #region Vec3V Member

    /// <summary>
    /// 3-component floating-point vector member class.
    /// </summary>
    public class Vec3VMember : Vector3MemberBase
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Vec3VMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            if (this.VectorType == Vector3Type.Colour)
                this.FriendlyTypeName = "Vec3V (colour)";
        }

        #endregion // Constructor(s)
    } // Vec3VMember

    #endregion // Vec3 VMember

    #endregion // 3-Component Vector Member Classes

    #region 4-Component Vector Member Classes

    #region Base Class

    public class Vector4MemberBase :
        MemberBase,
        IMemberInitialValue<float[]>,
        IMemberRange<float>,
        IMemberStep<float>,
        IMemberDecimalPlace,
        IMemberHighPrecision
    {
        #region Fields
        private float[] m_init;
        private float m_minimum;
        private float m_maximum;
        private float m_step;
        private int m_decimalPlaces;
        private bool m_highPrecision;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public float[] Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (float[])newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Minimum
        {
            get { return m_minimum; }
            set
            {
                SetPropertyValue(value, m_minimum, () => this.Minimum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minimum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Maximum
        {
            get { return m_maximum; }
            set
            {
                SetPropertyValue(value, m_maximum, () => this.Maximum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public float Step
        {
            get { return m_step; }
            set
            {
                SetPropertyValue(value, m_step, () => this.Step,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_step = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        public int DecimalPlaces
        {
            get { return m_decimalPlaces; }
            set
            {
                SetPropertyValue(value, m_decimalPlaces, () => this.DecimalPlaces,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_decimalPlaces = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Vector4"; } }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        [Category("Float"),
        Description("Whether or not this float needs to be serialised using 6 or 9 decimal places.")]
        public Boolean HighPrecision
        {
            get { return m_highPrecision; }
            set
            {
                SetPropertyValue(value, m_highPrecision, () => this.HighPrecision,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highPrecision = (Boolean)newValue;
                        }
                ));
            }
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Vector4MemberBase(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = new float[4] { 0.0f, 0.0f, 0.0f, 0.0f };
            this.m_maximum = float.MaxValue / 1000.0f;
            this.m_minimum = float.MinValue / 1000.0f;
            this.m_step = 0.1f;
            this.m_decimalPlaces = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<float[]>.Parse(String s, ref float[] parseResult)
        {
            return Util.Parser.GetMulitpleFloatValuesFromString(s, this, out parseResult, 4, 0.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMin(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, -float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMax(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<float>.Parse(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, 0.1f);
        }

        /// <summary>
        /// 
        /// </summary>
        Boolean IMemberDecimalPlace.Parse(String s, ref int parseResult)
        {
            return Util.Parser.GetIntegerValue(s, this, out parseResult, -1);
        }

        #endregion // Controller Methods

        #region Private Methods
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<float[]>(reader);
            ParseMemberRangeData<float>(reader);
            ParseMemberStepData<float>(reader);
            ParseMemberDecimalPlaceData(reader);
            ParseAttributes(reader);

            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            bool highPrecision = false;
            String highPrecisionString = reader.GetAttribute("highPrecision");
            if (!String.IsNullOrEmpty(highPrecisionString) && !String.IsNullOrWhiteSpace(highPrecisionString))
            {
                if (!Util.Parser.GetBooleanValue(highPrecisionString, this, out highPrecision, false))
                    highPrecision = false;
            }
            this.HighPrecision = highPrecision;
            reader.ReadStartElement();
        }
        #endregion // Private Methods
    }

    #endregion // Base Class

    #region Vector4 Member

    /// <summary>
    /// 4-component floating-point vector member class.
    /// </summary>
    public class Vector4Member : Vector4MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Vector4"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Vector4Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // Vector4Member

    #endregion // Vector4 Member

    #region Vec4V Member

    /// <summary>
    /// 4-component floating-point vector member class.
    /// </summary>
    public class Vec4VMember : Vector4MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Vec4V"; } }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Vec4VMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // Vec4VMember

    #endregion // Vec4 VMember

    #endregion // 4-Component Vector Member Classes

    #region VecBoolVMember

    /// <summary>
    /// 4-component boolean vector member class.
    /// </summary>
    public class VecBoolVMember :
        MemberBase,
        IMemberInitialValue<Boolean[]>
    {
        #region Fields

        private Boolean[] m_init;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Editor(typeof(RSG.Base.Windows.Controls.TypeEditors.NumericUpDownEditor),
            typeof(PropertyValueEditor))]
        public Boolean[] Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (Boolean[])newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "VecBoolV"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public VecBoolVMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = new Boolean[] { false, false, false, false };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<Boolean[]>.Parse(String s, ref Boolean[] parseResult)
        {
            List<Boolean> result = new List<Boolean>();
            if (Util.Parser.GetMulitpleBooleanValuesFromString(s, this, ref result, 4))
            {
                parseResult = result.ToArray();
                return true;
            }
            else
            {
                Metadata.Log.WarningCtx(this.ParentStructure.Filename,
                    "VecBoolV Member '{0}' Error: Failed to parse the string '{1}' on the structure '{2}' in file '{3}' as it is an unrecognized format.",
                    this.Name,
                    s,
                    this.ParentStructure.DataType,
                    this.ParentStructure.Filename);

                return false;
            }
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<bool[]>(reader);
            reader.ReadStartElement();
            if (reader.NodeType == XmlNodeType.Text)
                reader.ReadString();
            if (!isEmpty && reader.NodeType == XmlNodeType.EndElement)
                reader.ReadEndElement();
        }

        #endregion // Private Methods
    } // VecBoolVMember

    #endregion // VecBoolVMember
} // RSG.Metadata.Parser
