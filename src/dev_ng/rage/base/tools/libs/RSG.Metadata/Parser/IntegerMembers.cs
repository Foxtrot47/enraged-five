﻿using System;
using System.Activities.Presentation.PropertyEditing;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Metadata.Parser
{
    #region Integer Base Member

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class IntegerMemberBase<T> :
        MemberBase,
        IMemberScalar
    {
        #region Fields

        protected T m_init;
        protected T m_minimum;
        protected T m_maximum;
        protected T m_step;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Category("Integer"),
        Description("Initial integer value."),
        Editor(typeof(RSG.Base.Windows.Controls.TypeEditors.NumericUpDownEditor),
            typeof(PropertyValueEditor))]
        public T Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (T)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Integer"),
        Description("Minimum integer value."),
        Editor(typeof(RSG.Base.Windows.Controls.TypeEditors.NumericUpDownEditor),
            typeof(PropertyValueEditor))]
        public T Minimum
        {
            get { return m_minimum; }
            set
            {
                SetPropertyValue(value, m_minimum, () => this.Minimum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minimum = (T)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Integer"),
        Description("Maximum integer value."),
        Editor(typeof(RSG.Base.Windows.Controls.TypeEditors.NumericUpDownEditor),
            typeof(PropertyValueEditor))]
        public T Maximum
        {
            get { return m_maximum; }
            set
            {
                SetPropertyValue(value, m_maximum, () => this.Maximum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximum = (T)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Integer"),
        Description("UI widget increment/decrement value."),
        Editor(typeof(RSG.Base.Windows.Controls.TypeEditors.NumericUpDownEditor),
            typeof(PropertyValueEditor))]
        public T Step
        {
            get { return m_step; }
            set
            {
                SetPropertyValue(value, m_step, () => this.Step,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_step = (T)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Base Integer"; } }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public IntegerMemberBase(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
        }
        
        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<T>(reader);
            ParseMemberRangeData<T>(reader);
            ParseMemberStepData<T>(reader);
            ParseAttributes(reader);

            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        protected virtual void ParseAttributes(XmlTextReader reader)
        {
            reader.ReadStartElement();
        }

        #endregion // Private Methods
    } // IntegerMemberBase

    #endregion // Interger Base Member

    #region Signed Integer Member Classes

    #region 8 Bit Integers

    #region Base

    /// <summary>
    /// 
    /// </summary>
    public class Signed8MemberBase : 
        IntegerMemberBase<sbyte>,
        IMemberInitialValue<sbyte>,
        IMemberRange<sbyte>,
        IMemberStep<sbyte>
    {
        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Signed8MemberBase(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = 0;
            this.m_maximum = sbyte.MaxValue;
            this.m_minimum = sbyte.MinValue;
            this.m_step = 1;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<sbyte>.Parse(String s, ref sbyte parseResult)
        {
            return Util.Parser.GetSByteValue(s, this, out parseResult, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<sbyte>.ParseMin(String s, ref sbyte parseResult)
        {
            return Util.Parser.GetSByteValue(s, this, out parseResult, sbyte.MinValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<sbyte>.ParseMax(String s, ref sbyte parseResult)
        {
            return Util.Parser.GetSByteValue(s, this, out parseResult, sbyte.MaxValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<sbyte>.Parse(String s, ref sbyte parseResult)
        {
            return Util.Parser.GetSByteValue(s, this, out parseResult, 1);
        }

        #endregion // Controller Methods
    } // Signed8MemberBase

    #endregion Base

    #region S8Member

    /// <summary>
    /// 8-bit signed-integer member class.
    /// </summary>
    public class S8Member : Signed8MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "s8"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public S8Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // S8Member

    #endregion S8Member

    #region CharMember

    /// <summary>
    /// 8-bit signed-integer member class.
    /// </summary>
    public class CharMember : Signed8MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "char"; } }

        #endregion // Properties

        #region Constructor(s)                
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public CharMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // CharMember

    #endregion // CharMember

    #endregion // 8 Bit Integers

    #region 16 Bit Integers

    #region Base

    /// <summary>
    /// 
    /// </summary>
    public class Signed16MemberBase :
        IntegerMemberBase<Int16>,
        IMemberInitialValue<Int16>,
        IMemberRange<Int16>,
        IMemberStep<Int16>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Signed16MemberBase(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = 0;
            this.m_maximum = Int16.MaxValue;
            this.m_minimum = Int16.MinValue;
            this.m_step = 1;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<Int16>.Parse(String s, ref Int16 parseResult)
        {
            return Util.Parser.GetInt16Value(s, this, out parseResult, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<Int16>.ParseMin(String s, ref Int16 parseResult)
        {
            return Util.Parser.GetInt16Value(s, this, out parseResult, Int16.MinValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<Int16>.ParseMax(String s, ref Int16 parseResult)
        {
            return Util.Parser.GetInt16Value(s, this, out parseResult, Int16.MaxValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<Int16>.Parse(String s, ref Int16 parseResult)
        {
            return Util.Parser.GetInt16Value(s, this, out parseResult, 1);
        }

        #endregion // Controller Methods
    } // Signed16MemberBase

    #endregion Base

    #region S16 Member

    /// <summary>
    /// 16-bit signed-integer member class.
    /// </summary>
    public class S16Member : Signed16MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "s16"; } }

        #endregion // Properties

        #region Constructor(s)                
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public S16Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // S16Member

    #endregion // S16 Member

    #region Short Member

    /// <summary>
    /// 16-bit signed-integer member class.
    /// </summary>
    public class ShortMember : Signed16MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "short"; } }

        #endregion // Properties

        #region Constructor(s)                
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public ShortMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // ShortMember

    #endregion // Short Member

    #endregion // 16 Bit Integers

    #region 32 Bit Integers

    #region Base

    /// <summary>
    /// 
    /// </summary>
    public class Signed32MemberBase : IntegerMemberBase<Int32>,
        IMemberInitialValue<Int32>,
        IMemberRange<Int32>,
        IMemberStep<Int32>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Signed32MemberBase(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = 0;
            this.m_maximum = Int32.MaxValue;
            this.m_minimum = Int32.MinValue;
            this.m_step = 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<Int32>.Parse(String s, ref Int32 parseResult)
        {
            return Util.Parser.GetInt32Value(s, this, out parseResult, 0);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<Int32>.ParseMin(String s, ref Int32 parseResult)
        {
            return Util.Parser.GetInt32Value(s, this, out parseResult, Int32.MinValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<Int32>.ParseMax(String s, ref Int32 parseResult)
        {
            return Util.Parser.GetInt32Value(s, this, out parseResult, Int32.MaxValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<Int32>.Parse(String s, ref Int32 parseResult)
        {
            return Util.Parser.GetInt32Value(s, this, out parseResult, 1);
        }

        #endregion // Controller Methods
    } // Signed32MemberBase

    #endregion Base

    #region S32 Member

    /// <summary>
    /// 32-bit signed-integer member class.
    /// </summary>
    public class S32Member : Signed32MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "s32"; } }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public S32Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // S32Member

    #endregion // S32 Member

    #region Int Member

    /// <summary>
    /// 32-bit signed-integer member class.
    /// </summary>
    public class IntMember : Signed32MemberBase
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "int"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public IntMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)
    } // IntMember

    #endregion // Int Member

    #endregion // 32 Bit Integers

    #endregion // Signed Integer Member Classes

    #region Unsigned Integer Member Classes

    #region U8 Member

    /// <summary>
    /// 8-bit unsigned-integer member class.
    /// </summary>
    public class U8Member : IntegerMemberBase<byte>,
        IMemberInitialValue<byte>,
        IMemberRange<byte>,
        IMemberStep<byte>
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "u8"; } }

        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        public U8Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = 0;
            this.m_maximum = byte.MaxValue;
            this.m_minimum = byte.MinValue;
            this.m_step = 1;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<byte>.Parse(String s, ref byte parseResult)
        {
            return Util.Parser.GetByteValue(s, this, out parseResult, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<byte>.ParseMin(String s, ref byte parseResult)
        {
            return Util.Parser.GetByteValue(s, this, out parseResult, byte.MinValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<byte>.ParseMax(String s, ref byte parseResult)
        {
            return Util.Parser.GetByteValue(s, this, out parseResult, byte.MaxValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<byte>.Parse(String s, ref byte parseResult)
        {
            return Util.Parser.GetByteValue(s, this, out parseResult, 1);
        }

        #endregion // Controller Methods
    } // U8Member

    #endregion // U8 Member

    #region U16 Member

    /// <summary>
    /// 16-bit unsigned-integer member class.
    /// </summary>
    public class U16Member : IntegerMemberBase<UInt16>,
        IMemberInitialValue<UInt16>,
        IMemberRange<UInt16>,
        IMemberStep<UInt16>
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "u16"; } }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public U16Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = 0;
            this.m_maximum = UInt16.MaxValue;
            this.m_minimum = UInt16.MinValue;
            this.m_step = 1;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<UInt16>.Parse(String s, ref UInt16 parseResult)
        {
            return Util.Parser.GetUInt16Value(s, this, out parseResult, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<UInt16>.ParseMin(String s, ref UInt16 parseResult)
        {
            return Util.Parser.GetUInt16Value(s, this, out parseResult, UInt16.MinValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<UInt16>.ParseMax(String s, ref UInt16 parseResult)
        {
            return Util.Parser.GetUInt16Value(s, this, out parseResult, UInt16.MaxValue);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<UInt16>.Parse(String s, ref UInt16 parseResult)
        {
            return Util.Parser.GetUInt16Value(s, this, out parseResult, 1);
        }

        #endregion // Controller Methods
    }

    #endregion // U16 Member

    #region U32 Member

    /// <summary>
    /// 32-bit unsigned-integer member class.
    /// </summary>
    public class U32Member : IntegerMemberBase<UInt32>,
        IMemberInitialValue<UInt32>,
        IMemberRange<UInt32>,
        IMemberStep<UInt32>
    {
        #region Enumerations

        /// <summary>
        /// 
        /// </summary>
        public enum UInt32Type
        {
            Normal, // Regular 32 bit integer 0 -UINT_MAX
            Colour  // 32 bit integer represents ARGB colour.
        };

        #endregion // Enumerations

        #region Fields

        private UInt32Type m_integerType;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Category("Integer")]
        public UInt32Type IntegerType
        {
            get { return m_integerType; }
            set
            {
                SetPropertyValue(value, m_integerType, () => this.IntegerType,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_integerType = (UInt32Type)newValue;
                            OnPropertyChanged("FriendlyTypeName");
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName
        {
            get
            { 
                return IntegerType == UInt32Type.Normal ? 
                                "u32" : "u32 (colour)"; 
            }
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public U32Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = default(UInt32);
            this.m_maximum = UInt32.MaxValue;
            this.m_minimum = UInt32.MinValue;
            this.m_step = 1;
            this.m_integerType = UInt32Type.Normal;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<UInt32>.Parse(String s, ref UInt32 parseResult)
        {
            return Util.Parser.GetUInt32Value(s, this, out parseResult, 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<UInt32>.ParseMin(String s, ref UInt32 parseResult)
        {
            return Util.Parser.GetUInt32Value(s, this, out parseResult, UInt32.MinValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<UInt32>.ParseMax(String s, ref UInt32 parseResult)
        {
            return Util.Parser.GetUInt32Value(s, this, out parseResult, UInt32.MaxValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<UInt32>.Parse(String s, ref UInt32 parseResult)
        {
            return Util.Parser.GetUInt32Value(s, this, out parseResult, 1);
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        protected override void ParseAttributes(XmlTextReader reader)
        {
            String type = reader.GetAttribute("type");
            if (type != null)
            {
                switch (type)
                {
                    case "color":
                        this.m_integerType = UInt32Type.Colour;
                        break;
                    default:
                        this.m_integerType = UInt32Type.Normal;
                        break;
                }
            }

            base.ParseAttributes(reader);
        }

        #endregion // Private Methods
    } // U32Member
    
    #endregion // U32 Member

    #endregion // Unsigned Integer Member Classes
} // RSG.Metadata.Parser namespace
