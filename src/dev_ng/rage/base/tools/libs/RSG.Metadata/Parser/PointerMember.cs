﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Metadata.Model;
using RSG.Metadata.Resources;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// Pointer member class.
    /// </summary>
    public class PointerMember :
        MemberBase,
        IMember
    {
        #region Constants
        private static readonly String ATTR_POLICY = "policy";
        private static readonly String ATTR_TYPE = "type";
        private static readonly String ATTR_ONUNKNOWN_TYPE = "onUnknownType";
        private static readonly String ATTR_TOSTRING = "toString";
        private static readonly String ATTR_FROMSTRING = "fromString";
        private static readonly String ATTR_USERHANDLESNULL = "userHandlesNull";
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// Pointer policy enumeration; determine how the object is owned.
        /// </summary>
        public enum PointerPolicy
        {
            Owner,          // Data inline; owned by this (base type).
            SimpleOwner,    // Data inline; owned by this (one type).
            ExternalNamed,  // External data; string name 'ref' attribute.
            Link,           // Points to an object in a registered parDataSource
            LazyLink,       // "pointer" must actually be an instance of parLazyLink<>. 
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// C++ type of the pointed-to object.
        /// </summary>
        [Category("Pointer"),
        Description("C++ type of the pointed-to object.")]
        public String ObjectTypeName { get; set; }

        /// <summary>
        /// Structure of the pointed-to object.
        /// </summary>
        [Browsable(false)]
        public Structure ObjectType { get; private set; }

        /// <summary>
        /// Pointer type, see parser documentation.
        /// </summary>
        [Category("Pointer"),
        Description("Pointer type, see parser documentation.")]
        public PointerPolicy Policy { get; set; }

        /// <summary>
        /// Callback when Policy is Owner.
        /// </summary>
        [Category("Pointer"),
        Description("Callback when Policy is Owner.")]
        public String OnUnknownType { get; set; }

        /// <summary>
        /// For externally named pointers; convert from object to string.
        /// </summary>
        [Category("Pointer"),
        Description("For externally named pointers, method for serialisation.")]
        public String ToStringMethod { get; set; }

        /// <summary>
        /// For externally named pointers; convert from string to object.
        /// </summary>
        [Category("Pointer"),
        Description("For externally named pointers, method for deserialisation.")]
        public String FromStringMethod { get; set; }

        /// <summary>
        /// For externally named pointers; should NULL values be passed to the callback during load?
        /// </summary>
        [Category("Pointer"),
        Description("For externally named pointers, should NULL values be passed to the callback during load?")]
        public bool UserHandlesNull { get; set; }

        /// <summary>
        /// Gets a value indicating whether the pointer policy is either owner or simple
        /// owner.
        /// </summary>
        [Browsable(false)]
        public bool IsOwned
        {
            get
            {
                if (this.Policy == PointerPolicy.Owner)
                {
                    return true;
                }

                if (this.Policy == PointerPolicy.SimpleOwner)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Pointer"; } }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public Model.StructureDictionary DefinitionDictionary { get; private set; }
        #endregion // Properties

        #region Constructor(s)        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public PointerMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void PostprocessInternal(StructureDictionary defDict, ref Dictionary<int, Object> lookup)
        {
            if (this.Policy == PointerPolicy.ExternalNamed ||
                this.Policy == PointerPolicy.Link ||
                this.Policy == PointerPolicy.LazyLink)
                return;

            Debug.Assert(ObjectTypeName.Length > 0, "Invalid structure type.");

            this.DefinitionDictionary = defDict;
            Object type = null;
            lookup.TryGetValue(ObjectTypeName.GetHashCode(), out type);
            if (type is Structure)
            {
                this.ObjectType = type as Structure;
            }
            else
            {
                string msg = string.Format(SR.PointerUnresolvedRef, ObjectTypeName, Filename, LineNumber);
                this.Dictionary.AddError(msg);
            }
        }

        /// <summary>
        /// Validates this member and returns a value indicating whether this member is
        /// safe to instance inside a metadata document.
        /// </summary>
        /// <returns>
        /// True if this member is valid and can be instanced; otherwise, false.
        /// </returns>
        public override bool CanBeInstanced()
        {
            bool result = base.CanBeInstanced();
            if (this.Policy == PointerPolicy.ExternalNamed || this.Policy == PointerPolicy.Link || this.Policy == PointerPolicy.LazyLink)
            {
                return result;
            }

            if (string.IsNullOrWhiteSpace(this.ObjectTypeName))
            {
                Metadata.Log.Error(
                    "Unable to instance a pointer member that's missing its type attribute.\nLocation: {1} Line {2} Position {3}",
                    this.Filename,
                    this.LineNumber,
                    this.LinePosition);
                result = false;
            }
            else if (this.ObjectType == null)
            {
                Metadata.Log.Error(
                    "Unable to instance a pointer member whose structure of type '{0}' is unresolved.\nLocation: {1} Line {2} Position {3}",
                    this.ObjectTypeName,
                    this.Filename,
                    this.LineNumber,
                    this.LinePosition);
                result = false;
            }

            return result;
        }
        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// Deserialise pointer XML.
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParsePointerData(reader);

            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Deserialise pointer XML.
        /// </summary>
        /// <param name="nav"></param>
        private void ParsePointerData(XmlTextReader reader)
        {
            String policy = reader.GetAttribute(ATTR_POLICY);
            if (policy != null)
            {
                switch (policy)
                {
                    case "external_named":
                        this.Policy = PointerPolicy.ExternalNamed;
                        break;
                    case "owner":
                        this.Policy = PointerPolicy.Owner;
                        break;
                    case "simple_owner":
                        this.Policy = PointerPolicy.SimpleOwner;
                        break;
                    case "link":
                        this.Policy = PointerPolicy.Link;
                        break;
                    case "lazylink":
                        this.Policy = PointerPolicy.LazyLink;
                        break;
                    default:
                        {
                            string msg;
                            if (policy == null)
                            {
                                msg = string.Format(SR.PointerPolicyMissing, Filename, LineNumber);
                            }
                            else
                            {
                                msg = string.Format(SR.PointerPolicyInvalid, policy, Filename, LineNumber);
                            }

                            this.Dictionary.AddError(msg);
                        }
                        break;
                }
            }
            this.OnUnknownType = reader.GetAttribute(ATTR_ONUNKNOWN_TYPE);
            this.OnUnknownType = this.OnUnknownType == null ? String.Empty : this.OnUnknownType;
            this.ToStringMethod = reader.GetAttribute(ATTR_TOSTRING);
            this.ToStringMethod = this.ToStringMethod == null ? String.Empty : this.ToStringMethod;
            this.FromStringMethod = reader.GetAttribute(ATTR_FROMSTRING);
            this.FromStringMethod = this.ToStringMethod == null ? String.Empty : this.ToStringMethod;

            bool userHandlesNull = false;
            String userHandlesNullString = reader.GetAttribute(ATTR_USERHANDLESNULL);
            if (!String.IsNullOrEmpty(userHandlesNullString) && !String.IsNullOrWhiteSpace(userHandlesNullString))
            {
                if (!Boolean.TryParse(userHandlesNullString, out userHandlesNull))
                    userHandlesNull = false;
            }
            this.UserHandlesNull = userHandlesNull;

            this.ObjectTypeName = reader.GetAttribute(ATTR_TYPE);
            this.ObjectTypeName = this.ObjectTypeName == null ? String.Empty : Namespace.ConvertDataTypeToFriendlyName(this.ObjectTypeName);

            reader.ReadStartElement();
        }

        #endregion // Private Methods
    } // PointerMember
} // RSG.Metadata.Parser namespace
