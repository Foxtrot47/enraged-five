﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Metadata.Model;
using RSG.Metadata.Resources;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// Array definition.
    /// </summary>
    public class ArrayMember :  MemberBase
    {
        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="definition"></param>
        /// <param name="reader"></param>
        public ArrayMember(IBranch branch, Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(branch, reader);
        }
        #endregion

        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        public enum ArrayClass
        {
            RAGE_atArray,
            RAGE_atFixedArray,
            RAGE_atRangeArray,
            Pointer,
            Member,
            STL_vector
        }
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [Category("Array"),
        Description("Array element type.")]
        public IMember ElementType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Category("Array"),
        Description("Array element type.")]
        public String ElementStructureType { get; set; }

        /// <summary>
        /// Static size of array (required for some types).
        /// </summary>
        [Category("Array"),
        Description("Static size of array (required for some types).")]
        public int Size { get; set; }

        /// <summary>
        /// Type of the array.
        /// </summary>
        [Category("Array"),
        Description("Type of the array.")]
        public ArrayClass ArrayType { get; set; }

        /// <summary>
        /// Member variable name to use for the size.
        /// </summary>
        [Category("Array"),
        Description("Member variable name to use for the size.")]
        public string SizeVariable { get; set; }

        /// <summary>
        /// Number of bits to use for the index (16, 32)
        /// </summary>
        [Category("Array"),
        Description("Number of bits to use for the index (16, 32).")]
        public int IndexBits { get; set; }

        /// <summary>
        /// Friendly type name.
        /// </summary>
        [Browsable(false)]
        public override string FriendlyTypeName
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Array of {0}", this.ElementType.FriendlyTypeName);
                return (sb.ToString());
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="defDict"></param>
        protected override void PostprocessInternal(StructureDictionary defDict, ref Dictionary<int, Object> lookup)
        {
            if (null == this.ElementType)
            {
                this.Dictionary.AddError(string.Format(SR.ArrayElementMissing, Filename, LineNumber));
                return;
            }

            this.ElementType.Postprocess(defDict, ref lookup);
            if (this.ElementType is StructMember)
            {
                if ((this.ElementType as StructMember).Definition == null)
                {
                    this.ElementType = null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsFixedSize()
        {
            // Based on IsFixedSize() in
            // x:\gta5\src\dev\rage\base\src\parser\memberarray.h

            switch ( ArrayType )
            {
                case ArrayMember.ArrayClass.RAGE_atFixedArray:
                    return true;
                case ArrayMember.ArrayClass.RAGE_atRangeArray:
                    return true;
                case ArrayMember.ArrayClass.Pointer:
                    return true;
                case ArrayMember.ArrayClass.Member:
                    return true;
                default:
                    break;
            }

            return false;
        }

        /// <summary>
        /// Validates this member and returns a value indicating whether this member is
        /// safe to instance inside a metadata document.
        /// </summary>
        /// <returns>
        /// True if this member is valid and can be instanced; otherwise, false.
        /// </returns>
        public override bool CanBeInstanced()
        {
            bool result = base.CanBeInstanced();
            if (this.ElementType == null)
            {
                Metadata.Log.Error(
                    "Unable to instance a array member whose item type definition isn't defined.\nLocation: {0} Line {1} Position {2}",
                    this.Filename,
                    this.LineNumber,
                    this.LinePosition);
                result = false;
            }
            else
            {
                if (!this.ElementType.CanBeInstanced())
                {
                    result = false;
                }
            }

            return result;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(IBranch branch, XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;

            this.Size = -1;
            string sizeString = reader.GetAttribute("size");
            if (sizeString != null)
            {
                int size = 0;
                if (int.TryParse(sizeString, out size))
                {
                    this.Size = size;
                }
            }

            ParseMemberData(reader);
            ParseAttributes(reader);

            if (!isEmpty)
            {
                ParseArrayMemberData(branch, reader);

                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            string type = reader.GetAttribute("type");
            switch (type)
            {
                case "member":
                    this.ArrayType = ArrayClass.Member;
                    break;
                case "pointer":
                    this.ArrayType = ArrayClass.Pointer;
                    break;
                case "virtual":
                    this.ArrayType = ArrayClass.Pointer;
                    break;
                case "atArray":
                    this.ArrayType = ArrayClass.RAGE_atArray;
                    break;
                case "atFixedArray":
                    this.ArrayType = ArrayClass.RAGE_atFixedArray;
                    break;
                case "atRangeArray":
                    this.ArrayType = ArrayClass.RAGE_atRangeArray;
                    break;
                default:
                    {
                        string msg;
                        if (type == null)
                        {
                            msg = string.Format(SR.ArrayTypeMissing, Filename, LineNumber);
                        }
                        else
                        {
                            msg = string.Format(SR.ArrayTypeInvalid, type, Filename, LineNumber);
                        }

                        this.Dictionary.AddError(msg);
                    }
                    break;
            }

            reader.ReadStartElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void ParseArrayMemberData(IBranch branch, XmlTextReader reader)
        {
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                ElementType = MemberFactory.Create(branch, this.ParentStructure, reader);
                if (this.ElementType != null)
                {
                    this.ElementType.IsSubMember = true;
                    if (ElementType is StructMember)
                    {
                        ElementStructureType = (ElementType as StructMember).m_sStructureType;
                    }
                }
            }
        }
        #endregion
    } // RSG.Metadata.Parser.ArrayMember
} // RSG.Metadata.Parser
