﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Model;
using RSG.Base.Editor;
using RSG.Base.Logging;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// BaseDataType member class, for quickly implementing IMember classes.
    /// </summary>
    public class MemberBase : ModelBase, IMember
    {
        #region Constants
        #region Attribute Constants
        private static readonly String ATTR_NAME = "name";
        private static readonly String ATTR_UI_NAME = "ui_name";
        private static readonly String ATTR_UI_LIVEEDIT = "ui_liveedit";
        private static readonly String ATTR_DESCRIPTION = "description";
        private static readonly String ATTR_NOINIT = "noInit";
        private static readonly String ATTR_HIDEWIDGETS = "hideWidgets";
        private static readonly String ATTR_HIDEFROM = "hideFrom";
        private static readonly String ATTR_ONWIDGETCHANGED = "onWidgetChanged";
        private static readonly String ATTR_MINIMUM = "min";
        private static readonly String ATTR_MAXIMUM = "max";
        private static readonly String ATTR_STEP = "step";
        protected static readonly String ATTR_INIT = "init";
        private static readonly String ATTR_DECIMALPLACE = "ui_decimalplaces";
        #endregion // Attribute Constants

        #region Pre-Compiled XPath Expression Objects
        /// <summary>
        /// 
        /// </summary>
        protected static readonly XPathExpression XPATH_ATTR =
            XPathExpression.Compile("@*");
        #endregion // Pre-Compiled XPath Expression Objects
        #endregion // Constants

        #region Members
        private int m_lineNumber;
        private int m_linePosition;
        private Structure m_parentStructure;
        protected String m_name;

        protected String m_description;
        protected Boolean m_noInit;
        protected Boolean m_hideWidgets;
        protected String[] m_hideFrom;
        protected String m_widgetChangedCallback;
        private bool postProcessed;
        #endregion // Members

        #region Constructors
        public MemberBase(Structure parentStructure)
        {
            this.CanLiveEdit = true;
            this.m_parentStructure = parentStructure;
        }

        public MemberBase(Structure parentStructure, XmlTextReader reader)
        {
            this.CanLiveEdit = true;
            this.m_parentStructure = parentStructure;
            this.m_lineNumber = reader.LineNumber - 1;
            this.m_linePosition = reader.LinePosition;
        }
        #endregion

        #region Properties
        public StructureDictionary Dictionary
        {
            get
            {
                if (this.m_parentStructure == null)
                {
                    return null;
                }

                return this.m_parentStructure.Dictionary;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this member is a child element of another
        /// metadata member like a array etc.
        /// </summary>
        public bool IsSubMember
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Common"),
        Description("Value indicating whether the member can be edited live.")]
        public bool CanLiveEdit
        {
            get;
            protected set;
        }

        /// <summary>
        ///
        /// </summary>
        [Category("Common"),
        Description("Source filename for this structure definition.")]
        public String Filename
        {
            get
            {
                if (m_parentStructure == null)
                {
                    return "{Unknown File}";
                }

                return m_parentStructure.Filename;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Common"),
        Description("Line number in source filename for this structure definition.")]
        public int LineNumber
        {
            get
            {
                if (m_parentStructure == null)
                {
                    return m_lineNumber;
                }

                return m_lineNumber + m_parentStructure.LineNumber;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Common"),
        Description("Line position in source filename for this structure definition.")]
        public int LinePosition
        {
            get { return m_linePosition; }
        }

        /// <summary>
        /// Member definition field, the structure that
        /// this member belongs to
        /// </summary>
        [Category("Common"),
        Description("Member definition field."), Browsable(false)]
        public Structure ParentStructure
        {
            get { return m_parentStructure; }
            private set
            {
                SetPropertyValue(value, m_parentStructure, () => this.ParentStructure,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_parentStructure = (Structure)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Member name field.
        /// </summary>
        [Category("Common"),
        Description("Member name field.")]
        public String Name
        {
            get { return m_name; }
            set
            {
                SetPropertyValue(value, m_name, () => this.Name,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_name = (String)newValue;
                        }
                ));
            }
        }
        
        /// <summary>
        /// Member description field.
        /// </summary>
        [Category("Common"),
        Description("Member description field.")]
        public String Description
        {
            get { return m_description; }
            set
            {
                SetPropertyValue(value, m_description, () => this.Description,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_description = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// If true PARSER.InitObject() will not modify the member.
        /// </summary>
        [Category("Common"),
        Description("If true PARSER.InitObject() will not modify the member.")]
        public Boolean NoInit
        {
            get { return m_noInit; }
            set
            {
                SetPropertyValue(value, m_noInit, () => this.NoInit,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_noInit = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// If true does not create widgets for this member.
        /// </summary>
        [Category("Common"),
        Description("If true does not create widgets for this member.")]
        public Boolean HideWidgets
        {
            get { return m_hideWidgets; }
            set
            {
                SetPropertyValue(value, m_hideWidgets, () => this.HideWidgets,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_hideWidgets = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Which runtime-visitors will not be able to see this member.
        /// </summary>
        [Category("Common"),
        Description("Which runtime-visitors will not be able to see this member.")]
        public String[] HideFrom
        {
            get { return m_hideFrom; }
            set
            {
                SetPropertyValue(value, m_hideFrom, () => this.HideFrom,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_hideFrom = (String[])newValue;
                        }
                ));
            }
        }
        
        /// <summary>
        /// Callback function when widget is modified.
        /// </summary>
        [Category("Events"),
        Description("Callback function when widget is modified.")]
        public String WidgetChangedCallback
        {
            get { return m_widgetChangedCallback; }
            set
            {
                SetPropertyValue(value, m_widgetChangedCallback, () => this.WidgetChangedCallback,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_widgetChangedCallback = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public virtual String FriendlyTypeName { get { return "BaseDataType"; } }

        /// <summary>
        /// 
        /// </summary>
        public string FriendlyUIName { get; set; }
        #endregion // Properties
        
        #region Controller Methods

        /// <summary>
        /// Gets a value indicating whether the class can return line information.
        /// </summary>
        /// <returns>
        /// True if LineNumber and LinePosition can be provided; otherwise, false.
        /// </returns>
        public bool HasLineInfo()
        {
            return true;
        }

        /// <summary>
        /// Reset member to default values.
        /// </summary>
        public virtual void Reset()
        {
            this.m_name = String.Empty;
            this.m_description = null;

            this.m_widgetChangedCallback = String.Empty;
            this.m_hideWidgets = false;
            this.m_noInit = false;
            this.m_hideFrom = new String[] { };
        }

        public void Postprocess(StructureDictionary defDict, ref Dictionary<int, Object> lookup)
        {
            if (!this.postProcessed)
            {
                this.PostprocessInternal(defDict, ref lookup);
                this.postProcessed = true;
            }
        }

        /// <summary>
        /// Postprocess the member.
        /// </summary>
        /// Ensures all definitions are loaded prior to attempting to complete
        /// the IMember structures.
        protected virtual void PostprocessInternal(StructureDictionary defDict, ref Dictionary<int, Object> lookup)
        {
            // Default implementation does not do anything.
        }

        /// <summary>
        /// Validates this member and returns a value indicating whether this member is
        /// safe to instance inside a metadata document.
        /// </summary>
        /// <returns>
        /// True if this member is valid and can be instanced; otherwise, false.
        /// </returns>
        public virtual bool CanBeInstanced()
        {
            if (!string.IsNullOrWhiteSpace(this.Name) || m_parentStructure == null)
            {
                return true;
            }

            if (this.IsSubMember)
            {
                return true;
            }

            Metadata.Log.Error("Unable to instance a member that doesn't have a valid name.\nLocation: {0} Line {1} Position {2}", this.Filename, this.LineNumber, this.LinePosition);
            return false;
        }
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Deserialise common member data (IMember interface helper).
        /// </summary>
        /// <param name="nav"></param>
        protected void ParseMemberData(XPathNavigator nav)
        {
            XPathNodeIterator attrIter = nav.Select(XPATH_ATTR);
            while (attrIter.MoveNext())
            {
                if (typeof(String) != attrIter.Current.ValueType)
                    continue;
                String value = (attrIter.Current.TypedValue as String);

                if (ATTR_NAME.Equals(attrIter.Current.Name))
                {
#warning DHM -- HACK -- FIXME -- fix parCodeGen tool?
                    // There is an inconsistency between the way parCodeGen names
                    // members in the PSC files and in the data files; in PSC files
                    // we use 'm_' convention, in meta files we don't.  Lets detect
                    // and correct that.
                    this.Name = (value.Clone() as String);
                    if (this.Name.StartsWith("m_"))
                        this.Name = this.Name.Replace("m_", "");
                }
                else if (ATTR_DESCRIPTION.Equals(attrIter.Current.Name))
                    this.Description = (value.Clone() as String);
                else if (ATTR_HIDEWIDGETS.Equals(attrIter.Current.Name))
                    this.HideWidgets = String.Compare(value, "true", true) == 0 ? true : false;
                else if (ATTR_UI_NAME.Equals(attrIter.Current.Name))
                    this.FriendlyUIName = (value.Clone() as String);
            }
        }

        /// <summary>
        /// Deserialise common member data for IMemberInitialValue interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nav"></param>
        protected void ParseMemberInitialValueData<T>(XPathNavigator nav)
        {
            Debug.Assert(this is IMemberInitialValue<T>, "Invalid type." + this.GetType().ToString());

            XPathNodeIterator attrIter = nav.Select(XPATH_ATTR);
            while (attrIter.MoveNext())
            {
                if (typeof(String) != attrIter.Current.ValueType)
                    continue;
                String value = (attrIter.Current.TypedValue as String);

                if (ATTR_INIT.Equals(attrIter.Current.Name))
                {
                    IMemberInitialValue<T> t = (this as IMemberInitialValue<T>);
                    T parseResult = default(T);
                    if (t.Parse(value, ref parseResult))
                    {
                        t.Init = parseResult;
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Deserialise common member data for IMemberRange interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nav"></param>
        protected void ParseMemberRangeData<T>(XPathNavigator nav)
        {
            Debug.Assert(this is IMemberRange<T>, "Invalid type.");

            XPathNodeIterator attrIter = nav.Select(XPATH_ATTR);
            while (attrIter.MoveNext())
            {
                if (typeof(String) != attrIter.Current.ValueType)
                    continue;
                String value = (attrIter.Current.TypedValue as String);

                if (ATTR_MINIMUM.Equals(attrIter.Current.Name))
                {
                    IMemberRange<T> t = (this as IMemberRange<T>);
                    T parseResult = default(T);
                    if (t.ParseMin(value, ref parseResult))
                    {
                        t.Minimum = parseResult;
                    }
                } 
                else if (ATTR_MAXIMUM.Equals(attrIter.Current.Name))
                {
                    IMemberRange<T> t = (this as IMemberRange<T>);
                    T parseResult = default(T);
                    if (t.ParseMax(value, ref parseResult))
                    {
                        t.Maximum = parseResult;
                    }
                }
            }
        }

        /// <summary>
        /// Deserialise common member data for IMemberStep interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nav"></param>
        protected void ParseMemberStepData<T>(XPathNavigator nav)
        {
            Debug.Assert(this is IMemberStep<T>, "Invalid type.");

            XPathNodeIterator attrIter = nav.Select(XPATH_ATTR);
            while (attrIter.MoveNext())
            {
                if (typeof(String) != attrIter.Current.ValueType)
                    continue;
                String value = (attrIter.Current.TypedValue as String);

                if (ATTR_STEP.Equals(attrIter.Current.Name))
                {
                    IMemberStep<T> t = (this as IMemberStep<T>);
                    T parseResult = default(T);
                    if (t.Parse(value, ref parseResult))
                    {
                        t.Step = parseResult;
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Deserialise common member data for IMemberDecimalPlace interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nav"></param>
        protected void ParseMemberDecimalPlaceData(XPathNavigator nav)
        {
            Debug.Assert(this is IMemberDecimalPlace, "Invalid type.");

            XPathNodeIterator attrIter = nav.Select(XPATH_ATTR);
            while (attrIter.MoveNext())
            {
                if (typeof(String) != attrIter.Current.ValueType)
                    continue;
                String value = (attrIter.Current.TypedValue as String);

                if (ATTR_DECIMALPLACE.Equals(attrIter.Current.Name))
                {
                    IMemberDecimalPlace t = (this as IMemberDecimalPlace);
                    int parseResult = -1;
                    if (t.Parse(value, ref parseResult))
                    {
                        t.DecimalPlaces = parseResult;
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Deserialise common member data (IMember interface helper).
        /// </summary>
        /// <param name="nav"></param>
        protected void ParseMemberData(XmlTextReader reader)
        {
            this.m_name = reader.GetAttribute(ATTR_NAME);
            this.m_name = this.Name == null ? String.Empty : this.Name.Replace("m_", "");

            this.FriendlyUIName = reader.GetAttribute(ATTR_UI_NAME);
            string liveeditstring = reader.GetAttribute(ATTR_UI_LIVEEDIT);
            bool liveedit = true;
            if (liveeditstring == null)
            {
                if (this.ParentStructure != null)
                {
                    liveedit = this.ParentStructure.CanLiveEdit;
                }
            }
            else
            {
                if (!bool.TryParse(liveeditstring, out liveedit))
                {
                    liveedit = true;
                }
            }

            this.CanLiveEdit = liveedit;

            this.m_description = reader.GetAttribute(ATTR_DESCRIPTION);
            this.m_description = this.Description == null ? String.Empty : this.Description;

            bool noInit = false;
            String noInitString = reader.GetAttribute(ATTR_NOINIT);
            if (!String.IsNullOrEmpty(noInitString) && !String.IsNullOrWhiteSpace(noInitString))
            {
                if (!Boolean.TryParse(noInitString, out noInit))
                    noInit = false;
            }
            this.m_noInit = noInit;

            this.m_widgetChangedCallback = reader.GetAttribute(ATTR_ONWIDGETCHANGED);
            this.m_widgetChangedCallback = this.WidgetChangedCallback == null ? String.Empty : this.WidgetChangedCallback;

            bool hideWidgets = false;
            String hideWidgetsString = reader.GetAttribute(ATTR_HIDEWIDGETS);
            if (!String.IsNullOrEmpty(hideWidgetsString) && !String.IsNullOrWhiteSpace(hideWidgetsString))
            {
                if (!Boolean.TryParse(hideWidgetsString, out hideWidgets))
                    hideWidgets = false;
            }
            this.m_hideWidgets = hideWidgets;

            String hideFrom = reader.GetAttribute(ATTR_HIDEFROM);
            if (!String.IsNullOrEmpty(hideFrom) && !String.IsNullOrWhiteSpace(hideFrom))
            {
                this.m_hideFrom = hideFrom.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        /// <summary>
        /// Deserialise common member data for IMemberInitialValue interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nav"></param>
        protected void ParseMemberInitialValueData<T>(XmlTextReader reader)
        {
            if (!(this is IMemberInitialValue<T>))
                return;

            IMemberInitialValue<T> t = (this as IMemberInitialValue<T>);
            String value = reader.GetAttribute(ATTR_INIT);
            if (value != null)
            {
                T parseResult = default(T);
                if (t.Parse(value, ref parseResult))
                {
                    t.Init = parseResult;
                }
            }
        }

        /// <summary>
        /// Deserialise common member data for IMemberRange interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nav"></param>
        protected void ParseMemberRangeData<T>(XmlTextReader reader)
        {
            if (!(this is IMemberRange<T>))
                return;

            IMemberRange<T> t = (this as IMemberRange<T>);
            String min = reader.GetAttribute(ATTR_MINIMUM);
            if (min != null)
            {
                T parseResult = default(T);
                if (t.ParseMin(min, ref parseResult))
                {
                    t.Minimum = parseResult;
                }
            }
            String max = reader.GetAttribute(ATTR_MAXIMUM);
            if (max != null)
            {
                T parseResult = default(T);
                if (t.ParseMax(max, ref parseResult))
                {
                    t.Maximum = parseResult;
                }
            }
        }

        /// <summary>
        /// Deserialise common member data for IMemberStep interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nav"></param>
        protected void ParseMemberStepData<T>(XmlTextReader reader)
        {
            if (!(this is IMemberStep<T>))
                return;

            IMemberStep<T> t = (this as IMemberStep<T>);
            String step = reader.GetAttribute(ATTR_STEP);
            if (step != null)
            {
                T parseResult = default(T);
                if (t.Parse(step, ref parseResult))
                {
                    t.Step = parseResult;
                }
            }
        }

        /// <summary>
        /// Deserialise common member data for IMemberDecimalPlace interface.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nav"></param>
        protected void ParseMemberDecimalPlaceData(XmlTextReader reader)
        {
            if (!(this is IMemberDecimalPlace))
                return;

            IMemberDecimalPlace t = (this as IMemberDecimalPlace);
            String decimalplaces = reader.GetAttribute(ATTR_DECIMALPLACE);
            if (decimalplaces != null)
            {
                int parseResult = -1;
                if (t.Parse(decimalplaces, ref parseResult))
                {
                    t.DecimalPlaces = parseResult;
                }
            }
        }
        #endregion // Protected Methods
    } // MemberBase
} // RSG.Metadata.Parser namespace
