﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Metadata.Model;
using RSG.Metadata.Resources;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// parCodeGen Enumeration Definition data (enumdef).
    /// </summary>
    public class Enumeration : IXmlLineInfo
    {
        #region Fields
        public const string XmlNodeName = "enumdef";
        public const string NameAttr = "name";
        public const string ValueAttr = "value";
        public const string TypeAttr = "type";
        private string fileName;
        private int lineNumber;
        private int linePosition;
        private Dictionary<string, Int64> values;
        private List<string> _names;
        private string dataType;
        private string cachedXml;
        private bool numerical;

        private StructureDictionary dictionary;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public StructureDictionary Dictionary
        {
            get { return this.dictionary; }
        }

        /// <summary>
        /// Source filename for this enumeration definition; tool may serialise
        /// this enumeration elsewhere.
        /// </summary>
        [Category("Metadata"),
        Description("Source filename for this enumeration definition.")]
        public string Filename
        {
            get { return fileName; }
            private set { fileName = value; }
        }

        /// <summary>
        /// Line number in the source filename for this structure definition.
        /// </summary>
        [Category("Metadata"),
        Description("Line number in source filename for this structure definition.")]
        public int LineNumber
        {
            get { return lineNumber; }
            private set { lineNumber = value; }
        }

        /// <summary>
        /// Line position in the source filename for this structure definition.
        /// </summary>
        [Category("Metadata"),
        Description("Line position in source filename for this structure definition.")]
        public int LinePosition
        {
            get { return linePosition; }
            private set { linePosition = value; }
        }

        /// <summary>
        /// C++ type identifier that this XML metadata corresponds to.
        /// </summary>
        /// Example: "BlendMode".
        /// 
        [Category("Metadata"),
        Description("C++ type identifier that this XML metadata corresponds to.")]
        public string DataType
        {
            get { return this.dataType; }
            set { this.dataType = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, Int64> Values
        {
            get
            {
                this.EnsureLoaded();
                return values;
            }
        }

        public List<string> Names
        {
            get
            {
                this.EnsureLoaded();
                return this._names;
            }
        }

        public bool Numerical
        {
            get { return this.numerical; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// This is deliberately non-public; please use the static factory
        /// methods or another constructor.
        Enumeration()
        {
            SetDefaults();
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static Enumeration Create(StructureDictionary dictionary, string filename, XmlTextReader reader)
        {
            Enumeration e = new Enumeration();
            e.dictionary = dictionary;
            e.Filename = filename;
            e.lineNumber = reader.LineNumber;
            e.linePosition = reader.LinePosition;
            e.ParseAttributes(reader);
            e.cachedXml = reader.ReadOuterXml();
            return e;
        }

        /// <summary>
        /// Initialise object with default values.
        /// </summary>
        public void SetDefaults()
        {
            this.fileName = string.Empty;
            this.dataType = string.Empty;
            this.values = new Dictionary<string, Int64>();
            this._names = new List<string>();
        }

        /// <summary>
        /// Gets a value indicating whether the class can return line information.
        /// </summary>
        /// <returns>
        /// True if LineNumber and LinePosition can be provided; otherwise, false.
        /// </returns>
        public bool HasLineInfo()
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <exception cref="ParserException" />
        public void Serialise(XmlDocument document)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void Deserialise(XmlTextReader reader)
        {
            try
            {
                LoadFrom(reader);
            }
            catch (Exception ex)
            {
                string message = string.Format("Exception deserialising enumeration from XML document {0}.  See InnerException.",
                    this.Filename);
                throw new ParserException(message, ex);
            }
        }

        public void EnsureLoaded()
        {
            if (this.cachedXml != null)
            {
                string xml = this.cachedXml;
                this.cachedXml = null;
                using (StringReader stream = new StringReader(xml))
                {
                    using (XmlTextReader reader = new XmlTextReader(stream))
                    {
                        reader.MoveToContent();
                        if (!reader.IsEmptyElement)
                        {
                            reader.ReadStartElement();
                            this.ParseMembers(reader);
                        }
                    }
                }

                this.cachedXml = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadFrom(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseAttributes(reader);
            reader.ReadStartElement();

            if (!isEmpty)
            {
                ParseMembers(reader);

                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Deserialise enumdef node attributes.
        /// </summary>
        /// <param name="nav"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            this.DataType = reader.GetAttribute(TypeAttr);
            this.numerical = reader.GetAttribute("ui_numerical") != null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseMembers(XmlTextReader reader)
        {
            Int64 previousValue = -1;
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                previousValue = ParseMember(reader, previousValue);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private Int64 ParseMember(XmlTextReader reader, Int64 previousValue)
        {
            Int64 numericValue = 0;
            bool isEmpty = reader.IsEmptyElement;
            string name = reader.GetAttribute(NameAttr);
            string value = reader.GetAttribute(ValueAttr);
            if (!string.IsNullOrWhiteSpace(value) || !string.IsNullOrEmpty(value))
            {
                if (!Int64.TryParse(value, out numericValue))
                {
                    if (!RSG.Metadata.Util.Parser.TryToGetConstant(value, out numericValue))
                    {
                        string msg = string.Format(SR.EnumValueError, value, this.Filename, reader.LineNumber);
                        this.Dictionary.AddWarning(msg);
                        numericValue = previousValue + 1;
                    }
                }
            }
            else
            {
                numericValue = previousValue + 1;
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                string msg = string.Format(SR.EnumNameMissing, this.Filename, reader.LineNumber);
                this.Dictionary.AddError(msg);
            }
            else if (name.Contains(" "))
            {
                string msg = string.Format(SR.EnumNameInvalid, name, this.Filename, reader.LineNumber);
                this.Dictionary.AddError(msg);
            }

            if (values.ContainsKey(name))
            {
                string msg = string.Format(SR.EnumDuplicateValue, name, this.Filename, reader.LineNumber);
                this.Dictionary.AddError(msg);
            }
            else
            {
                values.Add(name, numericValue);
                this._names.Add(name);
            }

            reader.ReadStartElement();
            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }

            return numericValue;
        }
        #endregion
    } // Enumeration
} // RSG.Metadata.Parser namespace
