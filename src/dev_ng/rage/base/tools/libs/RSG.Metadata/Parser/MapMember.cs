﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Metadata.Model;
using RSG.Metadata.Resources;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// 
    /// </summary>
    public class MapMember : MemberBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [Category("Map"),
        Description("Map value type.")]
        public IMember ElementType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Category("Map"),
        Description("Array value type.")]
        public String ElementStructureType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Category("Map"),
        Description("Map key type.")]
        public String KeyType { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="nav"></param>
        public MapMember(IBranch branch, Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(branch, reader);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Resolves any external references to different structures,
        /// enumerations in the given structure dictionary.
        /// </summary>
        protected override void PostprocessInternal(StructureDictionary defDict, ref Dictionary<int, Object> lookup)
        {
            if (null == this.ElementType)
            {
                this.Dictionary.AddError(string.Format(SR.MapElementMissing, Filename, LineNumber));
                return;
            }

            this.ElementType.Postprocess(defDict, ref lookup);
            if (this.ElementType is StructMember)
            {
                if ((this.ElementType as StructMember).Definition == null)
                {
                    this.ElementType = null;
                }
            }
        }

        /// <summary>
        /// Validates this member and returns a value indicating whether this member is
        /// safe to instance inside a metadata document.
        /// </summary>
        /// <returns>
        /// True if this member is valid and can be instanced; otherwise, false.
        /// </returns>
        public override bool CanBeInstanced()
        {
            bool result = base.CanBeInstanced();
            switch (KeyType)
            {
                case "s8":
                case "u8":
                case "s16":
                case "u16":
                case "s32":
                case "u32":
                case "atHashString":
                case "atHashValue":
                    break;
                default:
                    {
                        Metadata.Log.Error(
                            "Unable to instance a map member whose key type isn't recognised as valid.\nLocation: {0} Line {1} Position {2}",
                            this.Filename,
                            this.LineNumber,
                            this.LinePosition);
                    }
                    break;
            }

            if (this.ElementType == null)
            {
                Metadata.Log.Error(
                    "Unable to instance a map member whose item type definition isn't defined.\nLocation: {0} Line {1} Position {2}",
                    this.Filename,
                    this.LineNumber,
                    this.LinePosition);
                result = false;
            }
            else
            {
                if (!this.ElementType.CanBeInstanced())
                {
                    result = false;
                }
            }

            return result;
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(IBranch branch, XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;

            ParseMemberData(reader);
            ParseAttributes(reader);

            if (!isEmpty)
            {
                ParseArrayMemberData(branch, reader);

                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            this.KeyType = reader.GetAttribute("key");
            switch (KeyType)
            {
                case "s8":
                case "u8":
                case "s16":
                case "u16":
                case "s32":
                case "u32":
                case "atHashString":
                case "atHashValue":
                    break;
                default:
                    {
                        string msg;
                        if (KeyType == null)
                        {
                            msg = string.Format(SR.MapTypeMissing, Filename, LineNumber);
                        }
                        else
                        {
                            msg = string.Format(SR.MapTypeInvalid, KeyType, Filename, LineNumber);
                        }

                        this.Dictionary.AddError(msg);
                    }
                    break;
            }

            reader.ReadStartElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void ParseArrayMemberData(IBranch branch, XmlTextReader reader)
        {
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                ElementType = MemberFactory.Create(branch, this.ParentStructure, reader);
                if (this.ElementType != null)
                {
                    this.ElementType.IsSubMember = true;
                    if (ElementType is StructMember)
                    {
                        ElementStructureType = (ElementType as StructMember).m_sStructureType;
                    }
                }
            }
        }
        #endregion // Private Methods
    } // MapMember
} // RSG.Metadata.Parser
