﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Metadata.Model;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// Factory class for constructing IMember objects from XmlNode objects.
    /// </summary>
    /// This is only used internally.
    internal static class MemberFactory
    {
        #region Constants

        #region Pre-Compiled XPath Expression Objects
        /// <summary>
        /// 
        /// </summary>
        public static readonly XPathExpression XPATH_MEMBER =
            XPathExpression.Compile("*");
        #endregion // Pre-Compiled XPath Expression Objects
        #endregion // Constants

        #region Static Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        internal static IMember Create(IBranch branch, Structure parent, XmlTextReader reader)
        {
            if (0 == String.Compare("float", reader.Name, true) || (0 == String.Compare("ScalarV", reader.Name, true)))
            {
                return (new FloatMember(parent, reader));
            }
            if (0 == String.Compare("Float16", reader.Name, true))
            {
                return (new Float16Member(parent, reader));
            }
            else if (0 == String.Compare("bool", reader.Name, true) || (0 == String.Compare("boolV", reader.Name, true)))
            {
                return (new BoolMember(parent, reader));
            }
            else if (0 == String.Compare("VecBoolV", reader.Name, true))
            {
                return (new VecBoolVMember(parent, reader));
            }
            else if (0 == String.Compare("char", reader.Name, true))
            {
                return (new CharMember(parent, reader));
            }
            else if (0 == String.Compare("s8", reader.Name, true))
            {
                return (new S8Member(parent, reader));
            }
            else if (0 == String.Compare("u8", reader.Name, true))
            {
                return (new U8Member(parent, reader));
            }
            else if (0 == String.Compare("short", reader.Name, true))
            {
                return (new ShortMember(parent, reader));
            }
            else if (0 == String.Compare("s16", reader.Name, true))
            {
                return (new S16Member(parent, reader));
            }
            else if (0 == String.Compare("u16", reader.Name, true))
            {
                return (new U16Member(parent, reader));
            }
            else if (0 == String.Compare("int", reader.Name, true))
            {
                return (new IntMember(parent, reader));
            }
            else if (0 == String.Compare("s32", reader.Name, true))
            {
                return (new S32Member(parent, reader));
            }
            else if (0 == String.Compare("u32", reader.Name, true))
            {
                return (new U32Member(parent, reader));
            }
            else if (0 == String.Compare("color32", reader.Name, true))
            {
                return (new Color32Member(parent, reader));
            }
            else if (0 == String.Compare("enum", reader.Name, true))
            {
                return (new EnumMember(parent, reader));
            }
            else if (0 == String.Compare("bitset", reader.Name, true))
            {
                return (new BitsetMember(parent, reader));
            }
            else if (0 == String.Compare("vector2", reader.Name, true))
            {
                return (new Vector2Member(parent, reader));
            }
            else if (0 == String.Compare("Vec2V", reader.Name, true))
            {
                return (new Vec2VMember(parent, reader));
            }
            else if (0 == String.Compare("vector3", reader.Name, true))
            {
                return (new Vector3Member(parent, reader));
            }
            else if (0 == String.Compare("Vec3V", reader.Name, true))
            {
                return (new Vec3VMember(parent, reader));
            }
            else if (0 == String.Compare("vector4", reader.Name, true))
            {
                return (new Vector4Member(parent, reader));
            }
            else if (0 == String.Compare("Vec4V", reader.Name, true))
            {
                return (new Vec4VMember(parent, reader));
            }
            else if (0 == String.Compare("Mat33V", reader.Name, true))
            {
                return (new Mat33VMember(parent, reader));
            }
            else if (0 == String.Compare("matrix34", reader.Name, true))
            {
                return (new Matrix34Member(parent, reader));
            }
            else if (0 == String.Compare("Mat34V", reader.Name, true))
            {
                return (new Mat34VMember(parent, reader));
            }
            else if (0 == String.Compare("matrix44", reader.Name, true))
            {
                return (new Matrix44Member(parent, reader));
            }
            else if (0 == String.Compare("Mat44V", reader.Name, true))
            {
                return (new Mat44VMember(parent, reader));
            }
            else if (0 == String.Compare("struct", reader.Name, true))
            {
                return (new StructMember(parent, reader));
            }
            else if (0 == String.Compare("pointer", reader.Name, true))
            {
                return (new PointerMember(parent, reader));
            }
            else if (0 == String.Compare("array", reader.Name, true))
            {
                return (new ArrayMember(branch, parent, reader));
            }
            else if (0 == String.Compare("string", reader.Name, true))
            {
                return (new StringMember(parent, reader));
            }
            else if (0 == String.Compare("map", reader.Name, true))
            {
                return (new MapMember(branch, parent, reader));
            }
            else if (0 == String.Compare("pad", reader.Name, true))
            {
                return null;
            }
            else
            {
                return (new UnknownMember(parent, reader));
            }
        }

        #endregion // Static Functions
    } // MemberFactory
} // RSG.Metadata.Parser namespace
