﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Xml;
using RSG.Metadata.Model;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// Base member interface.
    /// </summary>
    public interface IMember : IXmlLineInfo
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this member is a child element of another
        /// metadata member like a array etc.
        /// </summary>
        bool IsSubMember { get; set; }

        StructureDictionary Dictionary { get; }

        /// <summary>
        ///
        /// </summary>
        [Category("Common"),
        Description("Source filename for this structure definition.")]
        String Filename { get; }

        /// <summary>
        /// Member definition field, the structure that
        /// this member belongs to
        /// </summary>
        [Category("Common"),
        Description("Member definition field.")]
        Structure ParentStructure { get; }

        /// <summary>
        /// Member name field.
        /// </summary>
        [Category("Common"),
        Description("Member name field.")]
        String Name { get; set; }

        /// <summary>
        /// Member description field.
        /// </summary>
        [Category("Common"),
        Description("Member description field.")]
        String Description { get; set; }

        /// <summary>
        /// If true PARSER.InitObject() will not modify the member.
        /// </summary>
        [Category("Common"),
        Description("If true PARSER.InitObject() will not modify the member.")]
        bool NoInit { get; set; }

        /// <summary>
        /// If true does not create widgets for this member.
        /// </summary>
        [Category("Common"),
        Description("If true does not create widgets for this member.")]
        bool HideWidgets { get; set; }
        
        /// <summary>
        /// Which runtime-visitors will not be able to see this member.
        /// </summary>
        [Category("Common"),
        Description("Which runtime-visitors will not be able to see this member.")]
        String[] HideFrom { get; set; }
        
        /// <summary>
        /// Callback function when widget is modified.
        /// </summary>
        [Category("Events"),
        Description("Callback function when widget is modified.")]
        String WidgetChangedCallback { get; set; }
        
        /// <summary>
        /// Friendly type name.
        /// </summary>
        [Browsable(false)]
        String FriendlyTypeName { get; }

        /// <summary>
        /// Friendly type name.
        /// </summary>
        [Browsable(false)]
        String FriendlyUIName { get; }

        /// <summary>
        /// 
        /// </summary>
        [Category("Common"),
        Description("Value indicating whether the member can be edited live.")]
        bool CanLiveEdit { get; }
        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// Reset member to default values.
        /// </summary>
        void Reset();

        /// <summary>
        /// Postprocess the member.
        /// </summary>
        /// Ensures all definitions are loaded prior to attempting to complete
        /// the IMember structures.
        void Postprocess(StructureDictionary defDict, ref Dictionary<int, Object> lookup);
        
        /// <summary>
        /// Validates this member and returns a value indicating whether this member is
        /// safe to instance inside a metadata document.
        /// </summary>
        /// <returns>
        /// True if this member is valid and can be instanced; otherwise, false.
        /// </returns>
        bool CanBeInstanced();
        #endregion // Controller Methods
    }
    
    /// <summary>
    /// Scalar member interface (used for Array serialisation).
    /// </summary>
    public interface IMemberScalar : IMember
    {
    }

    /// <summary>
    /// Member interface for those members who have an initial value.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMemberInitialValue<T> : IMember
    {
        #region Properties
        /// <summary>
        /// Initial value.
        /// </summary>
        T Init { get; set; }
        #endregion // Properties

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean Parse(String s, ref T parseResult);

        #endregion // Controller Methods
    }

    /// <summary>
    /// Ranged member interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMemberRange<T> : IMember
    {
        #region Properties

        /// <summary>
        /// Minimum value.
        /// </summary>
        T Minimum { get; set; }

        /// <summary>
        /// Maximum value.
        /// </summary>
        T Maximum { get; set; }

        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean ParseMin(String s, ref T parseResult);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean ParseMax(String s, ref T parseResult);
        #endregion // Controller Methods
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMemberStep<T> : IMember
    {
        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        T Step { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean Parse(String s, ref T parseResult);
        #endregion // Controller Methods
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMemberDecimalPlace : IMember
    {
        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        int DecimalPlaces { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean Parse(String s, ref int parseResult);

        #endregion // Controller Methods
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMemberHighPrecision : IMember
    {
        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        bool HighPrecision { get; set; }

        #endregion // Controller Methods
    }

    public interface IMemberEnumValues : IMember
    {
        #region Properties
        /// <summary>
        /// Enum to use for the bit names (optional).
        /// </summary>
        String EnumType { get; }

        /// <summary>
        /// Enum values.
        /// </summary>
        Dictionary<String, Int64> Values { get; set; }
        #endregion // Properties
    }

    /// <summary>
    /// Member validation interface.
    /// </summary>
    /// Interface providing a Validate method to validate user-defined member
    /// definitions.
    /// 
    public interface ITunableValidatable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        bool IsValid { get; }
        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool Validate();
        #endregion // Controller Methods
    }
} // RSG.Metadata.Parser
