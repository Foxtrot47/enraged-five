﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace RSG.Metadata.Parser
{
    public class ParserError
    {
        public static readonly String Context = "PSC Parsing";

        public String Filename { get; set; }

        public int LineNumber { get; set; }

        public String Message { get; set; }

        public ParserError(String filename, int lineNumber, String message)
        {
            this.Filename = filename;
            this.LineNumber = lineNumber;
            this.Message = message;
        }
    };

    public class DuplicatedParserError : ParserError
    {
        public List<KeyValuePair<String, int>> Locations { get; set; }

        public String DataTypeName { get; set; }

        public DuplicatedParserError(List<KeyValuePair<String, int>> locations, String dataTypeName, String message)
            :base(String.Empty, 0, message)
        {
            this.Locations = locations;
            this.DataTypeName = dataTypeName;
        }
    };

    public class DuplicatedEnumParserError : DuplicatedParserError
    {
        public DuplicatedEnumParserError(List<KeyValuePair<String, int>> locations, String dataTypeName, String message)
            : base(locations, dataTypeName, message)
        {
        }
    };

    public class DuplicatedStructParserError : DuplicatedParserError
    {
        public DuplicatedStructParserError(List<KeyValuePair<String, int>> locations, String dataTypeName, String message)
            : base(locations, dataTypeName, message)
        {
        }
    };

    /// <summary>
    /// Exception class raised when there is an error in parsing.
    /// </summary>
    /// The root cause of the exception is specified in the message or the
    /// InnerException property (e.g. IOException, XmlException).
    ///
    public class ParserException : Exception
    {	
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ParserException()
        {
        }

        /// <summary>
        /// Constructor, specifying message.
        /// </summary>
        /// <param name="message"></param>
        public ParserException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="ctx"></param>
        public ParserException(SerializationInfo info, StreamingContext ctx)
            : base(info, ctx)
        {
        }

        /// <summary>
        /// Constructor, specifying message and inner exception.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public ParserException(String message, Exception inner)
            : base(message, inner)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Metadata.Parser namespace
