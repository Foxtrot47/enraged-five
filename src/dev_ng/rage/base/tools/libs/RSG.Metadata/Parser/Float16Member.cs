﻿using System;
using System.Activities.Presentation.PropertyEditing;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// 16-bit floating-point member class.
    /// </summary>
    public class Float16Member :
        MemberBase,
        IMemberInitialValue<float>,
        IMemberRange<float>,
        IMemberStep<float>,
        IMemberDecimalPlace,
        IMemberScalar,
        IMemberHighPrecision
    {
        #region Fields

        private const float Deg2Rad = (float)Math.PI / 180.0f;
        private const float Rad2Deg = 180.0f / (float)Math.PI;
        private float m_init;
        private float m_minimum;
        private float m_maximum;
        private float m_step;
        private int m_decimalPlaces;
        private Boolean m_highPrecision;
        private bool m_isDegreesToRadian;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Initial value.
        /// </summary>
        [Category("Float16"),
        Description("Initial float value.")]
        public float Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Minimum float value.
        /// </summary>
        [Category("Float16"),
        Description("Minimum float value.")]
        public float Minimum
        {
            get { return m_minimum; }
            set
            {
                SetPropertyValue(value, m_minimum, () => this.Minimum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_minimum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Maximum float value.
        /// </summary>
        [Category("Float16"),
        Description("Maximum float value.")]
        public float Maximum
        {
            get { return m_maximum; }
            set
            {
                SetPropertyValue(value, m_maximum, () => this.Maximum,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_maximum = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        [Category("Float16"),
        Description("UI widget increment/decrement value.")]
        public float Step
        {
            get { return m_step; }
            set
            {
                SetPropertyValue(value, m_step, () => this.Step,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_step = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        [Category("Float16"),
        Description("The number of decimal places to show and serialise this member with.")]
        public int DecimalPlaces
        {
            get { return m_decimalPlaces; }
            set
            {
                SetPropertyValue(value, m_decimalPlaces, () => this.DecimalPlaces,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_decimalPlaces = (int)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        [Category("Float16"),
        Description("Whether or not this float needs to be serialised using 6 or 9 decimal places.")]
        public Boolean HighPrecision
        {
            get { return m_highPrecision; }
            set
            {
                SetPropertyValue(value, m_highPrecision, () => this.HighPrecision,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_highPrecision = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// UI widget increment/decrement value.
        /// </summary>
        public Boolean IsDegreesToRadian
        {
            get { return m_isDegreesToRadian; }
            set
            {
                SetPropertyValue(value, m_isDegreesToRadian, () => this.IsDegreesToRadian,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isDegreesToRadian = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override String FriendlyTypeName { get { return "Float16"; } }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public Float16Member(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_init = 0.0f;
            this.m_maximum = float.MaxValue;
            this.m_minimum = float.MinValue;
            this.m_step = 0.1f;
            this.m_decimalPlaces = -1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<float>.Parse(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, 0.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMin(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, -float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberRange<float>.ParseMax(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, float.MaxValue / 1000.0f);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberStep<float>.Parse(String s, ref float parseResult)
        {
            return Util.Parser.GetFloatValue(s, this, out parseResult, 0.1f);
        }

        /// <summary>
        /// 
        /// </summary>
        Boolean IMemberDecimalPlace.Parse(String s, ref int parseResult)
        {
            return Util.Parser.GetIntegerValue(s, this, out parseResult, -1);
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            ParseMemberInitialValueData<float>(reader);
            ParseMemberRangeData<float>(reader);
            ParseMemberStepData<float>(reader);
            ParseMemberDecimalPlaceData(reader);
            ParseAttributes(reader);

            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }

            if (this.IsDegreesToRadian)
            {
                this.Init = this.Init * Rad2Deg;
                this.Maximum = this.Maximum * Rad2Deg;
                this.Minimum = this.Minimum * Rad2Deg;
                this.Step = 1.0f;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            bool highPrecision = false;
            String highPrecisionString = reader.GetAttribute("highPrecision");
            if (!String.IsNullOrEmpty(highPrecisionString) && !String.IsNullOrWhiteSpace(highPrecisionString))
            {
                if (!Util.Parser.GetBooleanValue(highPrecisionString, this, out highPrecision, false))
                    highPrecision = false;
            }
            this.HighPrecision = highPrecision;

            bool isDegrees = false;
            String isDegreesString = reader.GetAttribute("ui_degrees");
            if (!String.IsNullOrWhiteSpace(isDegreesString))
            {
                if (!Util.Parser.GetBooleanValue(isDegreesString, this, out isDegrees, false))
                {
                    isDegrees = false;
                }
            }
            this.IsDegreesToRadian = isDegrees;
            reader.ReadStartElement();
        }

        #endregion // Private Methods
    } //  FloatMember
} // RSG.Metadata.Parser namespace
