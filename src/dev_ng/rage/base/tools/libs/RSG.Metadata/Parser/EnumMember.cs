﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Model;
using RSG.Base.Logging;
using RSG.Metadata.Resources;

namespace RSG.Metadata.Parser
{
    /// <summary>
    /// 
    /// </summary>
    public class EnumMember :
        MemberBase,
        IMember,
        IMemberInitialValue<Int64>,
        IMemberEnumValues,
        IMemberScalar
    {
        #region Fields

        private Int64 m_init;
        private Dictionary<String, Int64> m_values;
        private String m_enumType;
        private string loadedInitValue;
        #endregion // Fields

        #region Properties

        /// <summary>
        /// Initial enum value.
        /// </summary>
        [Category("Enum"),
        Description("Initial enum value.")]
        public Int64 Init
        {
            get { return m_init; }
            set
            {
                SetPropertyValue(value, m_init, () => this.Init,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_init = (Int64)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Enum values.
        /// </summary>
        [Category("Enum"),
        Description("Enum values.")]
        public Dictionary<String, Int64> Values
        {
            get { return m_values; }
            set
            {
                SetPropertyValue(value, m_values, () => this.Values,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_values = (Dictionary<String, Int64>)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String EnumType
        {
            get { return m_enumType; }
            set
            {
                SetPropertyValue(value, m_enumType, () => this.EnumType,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_enumType = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public override string FriendlyTypeName { get { return "Enum"; } }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        public EnumMember(Structure definition, XmlTextReader reader)
            : base(definition, reader)
        {
            Reset();
            Parse(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.EnumType = String.Empty;
            this.Values = null;
            this.Init = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        Boolean IMemberInitialValue<Int64>.Parse(String s, ref Int64 parseResult)
        {
            if (this.Values != null)
            {
                if (this.Values.ContainsKey(s))
                {
                    Int64 value = this.Values[s];
                    if (value <= Int64.MaxValue && value >= Int64.MinValue)
                    {
                        parseResult = value;
                        return true;
                    }
                    else
                    {
                        Metadata.Log.WarningCtx(this.ParentStructure.Filename,
                       "Enum Member '{0}' Error: Failed to parse the string '{1}' on the structure '{2}' in file '{3}' as it was out of range",
                       this.Name,
                       s,
                       this.ParentStructure.DataType,
                       this.ParentStructure.Filename);
                    }
                }
                else
                {
                    Metadata.Log.WarningCtx(this.Filename,
                        "Enum Member '{0}' Error: Unable to set the init value to '{1}' as it doesn't belong to the given values definition '{2}'.",
                        this.Name,
                        s,
                        this.EnumType);

                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="defDict"></param>
        protected override void PostprocessInternal(StructureDictionary defDict, ref Dictionary<int, Object> lookup)
        {
            if (!String.IsNullOrEmpty(this.EnumType))
            {
                Object type = null;
                lookup.TryGetValue(this.EnumType.GetHashCode(), out type);
                if (type is Enumeration)
                {
                    this.m_values = new Dictionary<String, Int64>((type as Enumeration).Values);
                    if (loadedInitValue != null)
                    {
                        (this as IMemberInitialValue<Int64>).Parse(this.loadedInitValue, ref this.m_init);
                    }
                }
                else
                {
                    this.Dictionary.AddError(string.Format(SR.BitsetUnresolvedRef, this.EnumType, Filename, LineNumber));
                }
            }
        }

        /// <summary>
        /// Validates this member and returns a value indicating whether this member is
        /// safe to instance inside a metadata document.
        /// </summary>
        /// <returns>
        /// True if this member is valid and can be instanced; otherwise, false.
        /// </returns>
        public override bool CanBeInstanced()
        {
            bool result = base.CanBeInstanced();
            if (string.IsNullOrWhiteSpace(this.EnumType))
            {
                Metadata.Log.Error(
                    "Unable to instance a enum member that's missing its type attribute.\nLocation: {1} Line {2} Position {3}",
                    this.Filename,
                    this.LineNumber,
                    this.LinePosition);
                result = false;
            }
            else if (this.Values == null)
            {
                Metadata.Log.Error(
                    "Unable to instance a enum member whose enumeration of type '{0}' is unresolved.\nLocation: {1} Line {2} Position {3}",
                    this.EnumType,
                    this.Filename,
                    this.LineNumber,
                    this.LinePosition);
                result = false;
            }

            return result;
        }
        #endregion // Controller Methods

        #region Private Methods
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nav"></param>
        private void Parse(XmlTextReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            ParseMemberData(reader);
            this.loadedInitValue = reader.GetAttribute(ATTR_INIT);
            ParseAttributes(reader);

            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        private void ParseAttributes(XmlTextReader reader)
        {
            this.EnumType = reader.GetAttribute("type");
            this.EnumType = this.EnumType == null ? String.Empty : this.EnumType;
            reader.ReadStartElement();
        }

        #endregion // Private Methods
    } // EnumMember
} // RSG.Metadata.Parser namespace
