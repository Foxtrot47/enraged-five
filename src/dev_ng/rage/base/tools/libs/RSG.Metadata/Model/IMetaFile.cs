﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;

namespace RSG.Metadata.Model
{
    /// <summary>
    /// Interface for any metadata file model
    /// </summary>
    public interface IMetaFile : RSG.Base.Editor.IModel
    {
        #region Properties

        String Filename { get; }

        Structure Definition { get; }

        bool HasInternalFiles { get; }

        IEnumerable<string> GetInternalFilenames { get; }
        #endregion // Properties

        #region Methods

        void Serialise(String filename);

        #endregion // Methods
    } // IMetaFile
} // RSG.Metadata.Model
