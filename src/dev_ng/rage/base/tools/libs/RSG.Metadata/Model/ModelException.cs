﻿using System;
using System.Runtime.Serialization;

namespace RSG.Metadata.Model
{

    /// <summary>
    /// Exception class raised when there is an error in model construction.
    /// </summary>
    /// The root cause of the exception is specified in the message or the
    /// InnerException property (e.g. Parser.ParserException, IOException, XmlException).
    ///
    public class ModelException : Exception
    {	
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ModelException()
        {
        }

        /// <summary>
        /// Constructor, specifying message.
        /// </summary>
        /// <param name="message"></param>
        public ModelException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="ctx"></param>
        public ModelException(SerializationInfo info, StreamingContext ctx)
            : base(info, ctx)
        {
        }

        /// <summary>
        /// Constructor, specifying message and inner exception.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public ModelException(String message, Exception inner)
            : base(message, inner)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Metadata.Parser namespace
