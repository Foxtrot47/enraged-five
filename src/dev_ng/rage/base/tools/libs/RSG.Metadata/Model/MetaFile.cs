﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Editor;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using Mvp.Xml.XInclude;

namespace RSG.Metadata.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class MetaFile : HierarchicalModelBase, IMetaFile
    {
        #region Constants
        /// <summary>
        /// Default metadata file extension.
        /// </summary>
        public const String DEFAULT_FILE_EXTENSION = "meta";

        /// <summary>
        /// Default XML version.
        /// </summary>
        public const String DEFAULT_XML_VERSION = "1.0";

        /// <summary>
        /// Default XML character encoding.
        /// </summary>
        public const String DEFAULT_XML_ENCODING = "utf-8";
        #endregion // Constants

        #region Static Members
        private static IBranch Branch = null;
        #endregion
		
        #region Properties and Associated Member Data

        /// <summary>
        /// Filename containing loaded metadata.
        /// </summary>
        [Category("MetaFile"),
        Description("Filename containing loaded metadata.")]
        public String Filename
        {
            get { return m_sFilename; }
            set { m_sFilename = value; this.NotifyPropertyChanged("Filename"); }
        }
        private String m_sFilename;

        /// <summary>
        /// Associated StructureMember Definition object (optional, recommended for validation).
        /// </summary>
        [Category("MetaFile"),
        Description("StructureMember definition link.")]
        public Structure Definition
        {
            get { return m_Definition; }
            private set { m_Definition = value; }
        }
        private Structure m_Definition;

        /// <summary>
        /// Data.
        /// </summary>
        [Category("Data"),
        Description("Data")]
        public StructureTunable Members
        {
            get { return m_Members; }
            set { m_Members = value; }
        }
        private StructureTunable m_Members;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public IEnumerable<ITunable> AllMembers
        {
            get
            {
                foreach (ITunable tunable in this.GetAllMembers(true))
                {
                    yield return tunable;
                }
            }
        }

        public bool HasInternalFiles
        {
            get;
            private set;
        }

        public IEnumerable<string> GetInternalFilenames
        {
            get
            {
                if (this.HasInternalFiles == false)
                {
                    yield break;
                }

                List<XIncludeTunable> xincludes = new List<XIncludeTunable>();
                foreach (ITunable tunable in this.GetAllMembers(true))
                {
                    if (tunable is XIncludeTunable)
                    {
                        xincludes.Add(tunable as XIncludeTunable);
                    }
                }

                foreach (XIncludeTunable xtunable in xincludes)
                {
                    yield return xtunable.Filename;
                }
            }
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MetaFile()
            : base(null)
        {
            SetDefaults();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public MetaFile(StructureTunable rootTunable)
            : base(null)
        {
            SetDefaults();
            this.Definition = (rootTunable.Definition as StructMember).Definition;
            this.Members.Add((rootTunable.Definition as StructMember).FriendlyTypeName, rootTunable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public MetaFile(Parser.Structure def)
            : base(null)
        {
            SetDefaults();
            this.Definition = def;
            StructMember structMember = new StructMember(def, def);
            ITunable tunable = TunableFactory.Create(null, structMember);
            this.Members.Add(structMember.FriendlyTypeName, tunable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public MetaFile(String filename, Model.StructureDictionary defDict)
            : base(null)
        {
            SetDefaults();
            this.Filename = (filename.Clone() as String);
            Deserialise(filename, defDict, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public MetaFile(String filename, Model.StructureDictionary defDict, IConfig config)
            : base(null)
        {
            SetDefaults();
            Branch = config.Project.DefaultBranch; 
            this.Filename = (filename.Clone() as String);
            Deserialise(filename, defDict, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public MetaFile(String filename, Model.StructureDictionary defDict, IBranch branch)
            : base(null)
        {
            SetDefaults();
            Branch = branch;
            this.Filename = (filename.Clone() as String);
            Deserialise(filename, defDict, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="def"></param>
        public MetaFile(String filename, Parser.Structure def)
            : base(null)
        {
            SetDefaults();
            this.Filename = (filename.Clone() as String);
            this.Definition = def;
            Deserialise(filename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="def"></param>
        public MetaFile(String filename, Parser.Structure def, IConfig config)
            : base(null)
        {
            SetDefaults();
            Branch = config.Project.DefaultBranch; 
            this.Filename = (filename.Clone() as String);
            this.Definition = def;
            Deserialise(filename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public MetaFile(Stream source, Parser.Structure def)
            : base(null)
        {
            SetDefaults();
            this.Filename = "Unknown stream";
            this.Definition = def;
            Deserialise(source);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public MetaFile(Stream source, Parser.Structure def, IConfig config)
            : base(null)
        {
            SetDefaults();
            Branch = config.Project.DefaultBranch; 
            this.Filename = "Unknown stream";
            this.Definition = def;
            Deserialise(source);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public MetaFile(Stream source, Parser.Structure def, Boolean useXmlReader)
            : base(null)
        {
            SetDefaults();
            this.Filename = "Unknown stream";
            this.Definition = def;
            Deserialise(source);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public MetaFile(Stream source, Parser.Structure def, Boolean useXmlReader, IConfig config)
            : base(null)
        {
            SetDefaults();
            Branch = config.Project.DefaultBranch; 
            this.Filename = "Unknown stream";
            this.Definition = def;
            Deserialise(source);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public MetaFile(String filename, Model.StructureDictionary defDict, Boolean useXmlReader)
            : base(null)
        {
            SetDefaults();
            this.Filename = (filename.Clone() as String);
            Deserialise(filename, defDict, useXmlReader);
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void Serialise(String filename)
        {
            try
            {
                XmlDocument document = null;
                Serialise(out document);
                string folder = System.IO.Path.GetDirectoryName(filename);
                System.IO.Directory.CreateDirectory(folder);
                document.Save(filename);
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception serialising metadata to file {0}.  See InnerException.",
                    filename);
                throw new ParserException(message, ex);
            }
        }

        /// <summary>
        /// Alternative name for Serialise function since calling Serialise on a dotNetObject in max 
        /// doesn't work.  Who knew?
        /// </summary>
        /// <param name="filename"></param>
        public void SerialiseAlt(String filename)
        {
            Serialise(filename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        public void Serialise(out XmlDocument document)
        {
            try
            {
                SaveTo(out document);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void Deserialise(Stream source)
        {
            try
            {
                StreamReader sr = new StreamReader(source);
                sr.BaseStream.Position = 0;

                using (XIncludingReader reader = new XIncludingReader(sr))
                {
                    this.Deserialise(reader);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void Deserialise(String filename)
        {
            try
            {
                using (XIncludingReader reader = new XIncludingReader(filename))
                {
                    this.Deserialise(reader);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="defDict"></param>
        public void Deserialise(String filename, StructureDictionary defDict, Boolean useXmlReader)
        {
            using (XIncludingReader reader = new XIncludingReader(filename))
            {
                reader.MoveToContent();

                // Determine document type.
                String type = Namespace.ConvertDataTypeToFriendlyName(reader.Name);
                if (defDict.ContainsKey(type))
                {
                    this.Definition = defDict[type];
                    this.Deserialise(reader);
                }
                else
                {
                    Metadata.Log.Error("Unable to open metadata file due to the type '{0}' being unknown.", type);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        public void Deserialise(XIncludingReader reader)
        {
            if (Branch == null)
            {
                IConfig config = ConfigFactory.CreateConfig();
                Branch = config.Project.DefaultBranch;
            }

            XmlTunableFactory factory = new XmlTunableFactory();
            reader.XmlResolver = new XmlMetaResolver(Branch, factory, reader);

            this.Members.Clear();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                bool isEmpty = reader.IsEmptyElement;
                StructMember structMember = new StructMember(this.Definition, this.Definition);

                ITunable tunable = factory.Create(this, structMember, reader);
                this.Members.Add(structMember.FriendlyTypeName, tunable);

                if (!isEmpty)
                {
                    if (reader.NodeType == XmlNodeType.Text)
                        reader.ReadString();
                    if (reader.NodeType == XmlNodeType.EndElement)
                        reader.ReadEndElement();
                }
            }

            reader.XmlResolver = null;

            this.HasInternalFiles = false;
            foreach (ITunable tunable in this.GetAllMembers(true))
            {
                if (tunable is XIncludeTunable)
                {
                    this.HasInternalFiles = true;
                    break;
                }
            }
        }

        /// <summary>
        /// Initialise object with default values.
        /// </summary>
        private void SetDefaults()
        {
            this.Filename = String.Empty;
            this.Definition = null;
            this.Members = new Data.StructureTunable(this);
        }

        /// <summary>
        /// 
        /// </summary>
        private void SaveTo(out XmlDocument document)
        {
            bool containsIncludes = false;
            foreach (ITunable member in GetAllMembers(true))
            {
                if (member is XIncludeTunable)
                {
                    containsIncludes = true;
                    break;
                }
            }

            document = new XmlDocument();

            // XML Declaration
            XmlDeclaration xmlDecl = document.CreateXmlDeclaration(
                DEFAULT_XML_VERSION, "UTF-8", null);
            document.AppendChild(xmlDecl);

            // Document Root
            XmlElement xmlRoot = document.CreateElement(
                Util.XmlEscape.EscapeDataTypeString(this.Definition.DataType));

            if (containsIncludes)
            {
                xmlRoot.SetAttribute("xmlns:xi", "http://www.w3.org/2001/XInclude");
            }
            document.AppendChild(xmlRoot);

            // Loop through members
            foreach (Data.ITunable tune in this.Members.Values)
            {
                StructureTunable rootTune = (tune as StructureTunable);
                foreach (Data.ITunable childTune in rootTune.Values)
                {
                    try
                    {
                        if (childTune.InheritParent == true)
                            continue;

                        Debug.Assert(tune is Data.ITunableSerialisable,
                            "Unsupported ITunable, its not serialisable (internal error).");
                        if (!(childTune is Data.ITunableSerialisable))
                            continue;

                        XmlElement xmlElem =
                        (childTune as Data.ITunableSerialisable).Serialise(document, false);
                        if (childTune is PointerTunable)
                        {
                            PointerMember.PointerPolicy policy = ((childTune as PointerTunable).Definition as PointerMember).Policy;
                            if (policy == PointerMember.PointerPolicy.Owner || policy == PointerMember.PointerPolicy.SimpleOwner)
                            {
                                if (((childTune as PointerTunable).Value is ITunable))
                                {
                                    xmlElem.SetAttribute("type", ((childTune as PointerTunable).Value as ITunable).Definition.FriendlyTypeName);
                                }
                                else if (((childTune as PointerTunable).Value is String))
                                {
                                    xmlElem.SetAttribute("type", (childTune as PointerTunable).Value as String);
                                }
                            }
                        }
                        xmlRoot.AppendChild(xmlElem);
                    }
                    catch
                    {
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recursive"></param>
        public IEnumerable<ITunable> GetAllMembers(Boolean recursive)
        {
            List<ITunable> tunables = new List<ITunable>();
            foreach (ITunable tunable in this.Members.Values)
            {
                GetAllMembers(tunable, recursive, ref tunables);
            }
            foreach (ITunable tunable in tunables)
            {
                yield return tunable;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootTunable"></param>
        /// <param name="recursive"></param>
        /// <param name="tunables"></param>
        private void GetAllMembers(ITunable rootTunable, Boolean recursive, ref List<ITunable> tunables)
        {
            tunables.Add(rootTunable);
            if (recursive == false)
                return;

            if (rootTunable is StructureTunable)
            {
                foreach (ITunable tunable in (rootTunable as StructureTunable).Values)
                {
                    GetAllMembers(tunable, true, ref tunables);
                }
            }
            else if (rootTunable is PointerTunable)
            {
                if ((rootTunable as PointerTunable).Value is ITunable)
                    GetAllMembers((rootTunable as PointerTunable).Value as ITunable, true, ref tunables);
            }
            else if (rootTunable is ArrayTunable)
            {
                foreach (ITunable tunable in (rootTunable as ArrayTunable).Items)
                {
                    GetAllMembers(tunable, true, ref tunables);
                }
            }
            else if (rootTunable is MapTunable)
            {
                foreach (ITunable tunable in (rootTunable as MapTunable).Items)
                {
                    GetAllMembers(tunable, true, ref tunables);
                }
            }
            else if (rootTunable is MapItemTunable)
            {
                GetAllMembers((rootTunable as MapItemTunable).Value, true, ref tunables);
            }
        }
        #endregion

        #region Classes
        /// <summary>
        /// 
        /// </summary>
        private class XmlMetaResolver : XmlResolver
        {
            #region Fields
            private ICredentials credentials;
            private IBranch branch;
            XmlTunableFactory factory;
            XmlReader reader;
            #endregion

            #region Constructor(s)
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="branch"></param>
            /// <param name="factory"></param>
            /// <param name="reader"></param>
            public XmlMetaResolver(IBranch branch, XmlTunableFactory factory, XmlReader reader)
            {
                this.branch = branch;
                this.factory = factory;
                this.reader = reader;
            }
            #endregion // Constructor(s)

            #region Properties
            /// <summary>
            /// 
            /// </summary>
            public override ICredentials Credentials
            {
                set { this.credentials = value; }
            } 
            #endregion // Properties

            #region Methods

            /// <summary>
            /// 
            /// </summary>
            /// <param name="absoluteUri"></param>
            /// <param name="role"></param>
            /// <param name="ofObjectToReturn"></param>
            /// <returns></returns>
            public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
            {
                return new XIncludingReader(absoluteUri.AbsoluteUri);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="baseUri"></param>
            /// <param name="relativeUri"></param>
            /// <returns></returns>
            public override Uri ResolveUri(Uri baseUri, string relativeUri)
            {
                this.factory.HRefXiFile = relativeUri;
                this.factory.StartingNewXiFile = true;
                this.factory.XPointerXiFile = this.reader.GetAttribute("xpointer");
                string uri = relativeUri.ToLower();
                if (uri.StartsWith("common:"))
                {
                    uri = uri.Replace("common:", this.branch.Common);
                }
                else if (uri.StartsWith("platform:"))
                {
                    throw new NotSupportedException(
                        string.Format(
                        "Unable to resolve Uri path of '{0}' in the xi include node.",
                        baseUri));
                }

                return new Uri(uri);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="absoluteUri"></param>
            /// <param name="type"></param>
            /// <returns></returns>
            public override bool SupportsType(Uri absoluteUri, Type type)
            {
                return base.SupportsType(absoluteUri, type);
            }
            #endregion // Methods
        }
        #endregion
    } // RSG.Metadata.Model.MetaFile {Class}
} // RSG.Metadata.Model {Namespace}
