﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Editor;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using System.IO;

namespace RSG.Metadata.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class MultiMetaFiles : HierarchicalModelBase, IMetaFile, IWeakEventListener
    {
        #region Constants

        /// <summary>
        /// Default metadata file extension.
        /// </summary>
        public const String DEFAULT_FILE_EXTENSION = "meta";

        /// <summary>
        /// Default XML version.
        /// </summary>
        public const String DEFAULT_XML_VERSION = "1.0";

        /// <summary>
        /// Default XML character encoding.
        /// </summary>
        public const String DEFAULT_XML_ENCODING = "utf-8";

        #endregion // Constants

        #region Members

        private String m_filename;
        private Structure m_definition;
        private StructureTunable m_members;
        private Dictionary<String, MetaFile> m_metaFiles;
        private Dictionary<ITunable, List<ITunable>> m_resolvedTunables;

        #endregion // Members

        #region Properties and Associated Member Data

        /// <summary>
        /// Associated StructureMember Definition object (optional, recommended for validation).
        /// </summary>
        [Category("MetaFile"),
        Description("Metadata IO file path.")]
        public String Filename
        {
            get { return m_filename; }
            private set { m_filename = value; }
        }

        /// <summary>
        /// Associated StructureMember Definition object (optional, recommended for validation).
        /// </summary>
        [Category("MetaFile"),
        Description("StructureMember definition link.")]
        public Structure Definition
        {
            get { return m_definition; }
            private set { m_definition = value; }
        }

        /// <summary>
        /// Data.
        /// </summary>
        [Category("Data"),
        Description("Data")]
        public StructureTunable Members
        {
            get { return m_members; }
            private set { m_members = value; }
        }

        [Category("Children"),
        Description("Child metadata files.")]
        public Dictionary<String, MetaFile> MetaFiles
        {
            get { return m_metaFiles; }
            private set { m_metaFiles = value; }
        }

        public bool HasInternalFiles
        {
            get { return false; }
        }

        public IEnumerable<string> GetInternalFilenames
        {
            get
            {
                yield break;
            }
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public MultiMetaFiles(List<String> filenames, Model.StructureDictionary defDict)
            : base(null)
        {
            SetDefaults();
            if (filenames != null)
            {
                foreach (String filename in filenames)
                {
                    this.Filename += filename + ", ";
                }
                if (filenames.Count > 0)
                {
                    XmlDocument document = new XmlDocument();
                    document.Load(filenames[0]);

                    // Determine document type.
                    String type = Namespace.ConvertDataTypeToFriendlyName(document.DocumentElement.Name);
                    if (defDict.ContainsKey(type))
                    {
                        this.Definition = defDict[type];
                        StructMember structMember = new StructMember(this.Definition, this.Definition);
                        ITunable tunable = TunableFactory.Create(null, structMember);
                        this.Members.Add(structMember.FriendlyTypeName, tunable);

                        Deserialise(filenames, defDict);
                    }
                    else
                    {
                        String message = String.Format("Structure dictionary does not contain type {0}.", type);
                        Debug.Assert(defDict.ContainsKey(type), message);
                        throw new ParserException(message);
                    }
                }
            }
            if (this.Filename != null && this.Filename.Length >= 2)
                this.Filename = this.Filename.Substring(0, this.Filename.Length - 2);
        }

        /// <summary>
        /// 
        /// </summary>
        public MultiMetaFiles(List<MetaFile> files)
            : base(null)
        {
            SetDefaults();
            this.m_resolvedTunables = new Dictionary<ITunable, List<ITunable>>();
            if (files != null)
            {
                foreach (MetaFile file in files)
                {
                    this.Filename += file.Filename + ", ";
                    if (this.Definition == null)
                    {
                        this.Definition = file.Definition;
                        StructMember structMember = new StructMember(this.Definition, this.Definition);
                        ITunable tunable = TunableFactory.Create(null, structMember);
                        this.Members.Add(structMember.FriendlyTypeName, tunable);
                    }

                    Debug.Assert(file.Definition == this.Definition, "Trying to create a multi meta file with different definitions.");
                    if (file.Definition == this.Definition)
                        this.MetaFiles.Add(file.Filename, file);
                    else
                        Metadata.Log.Error("Trying to create a multi meta file with different definitions.");
                }
            }
            if (this.Filename != null && this.Filename.Length >= 2)
                this.Filename = this.Filename.Substring(0, this.Filename.Length - 2);



            // Resolve initial values, if all files that the same the value is taken else it is set to the default value 
            // (empty, null, 0, etc.)
            foreach (ITunable tunable in this.Members.Values)
            {
                this.ResolveMembers(tunable);
            }
            foreach (KeyValuePair<ITunable, List<ITunable>> resolvedTunable in this.m_resolvedTunables)
            {
                System.Diagnostics.Debug.Print(resolvedTunable.Value.Count.ToString());
                Debug.Assert(resolvedTunable.Value.Count == this.MetaFiles.Count, "Error while resolving tunables in a multi meta file, " + 
                                                                    "was unable to found certain tunables in the attached files");

                if (resolvedTunable.Value.Count > 0)
                {
                    ITunable previousTunable = null;
                    Boolean same = true;
                    foreach (ITunable tunable in resolvedTunable.Value)
                    {
                        if (previousTunable != null)
                        {
                            if (!previousTunable.HasSameValue(tunable))
                            {
                                same = false;
                                break;
                            }
                        }
                        previousTunable = tunable;
                    }
                    if (same == true)
                    {
                        resolvedTunable.Key.SetTunableValue(resolvedTunable.Value[0]);
                    }
                    else if (resolvedTunable.Key is BoolTunable)
                    {
                        (resolvedTunable.Key as BoolTunable).Value = null;
                    }
                }

                if (resolvedTunable.Key.CanInheritParent == true)
                {
                    Boolean? inheritParent = resolvedTunable.Value[0].InheritParent;
                    Boolean same = true;
                    foreach (ITunable tunable in resolvedTunable.Value)
                    {
                        if (tunable.InheritParent != inheritParent)
                            same = false;
                    }
                    if (same == true)
                        resolvedTunable.Key.InheritParent = inheritParent;
                    else
                        resolvedTunable.Key.InheritParent = null;
                }

                PropertyChangedEventManager.AddListener(resolvedTunable.Key, this, "");
            }
        }

        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void Serialise(String filename)
        {
            try
            {
                if (this.MetaFiles.ContainsKey(filename))
                {
                    this.MetaFiles[filename].Serialise(filename);
                }

                //XmlDocument document = null;
                //Serialise(out document);
                //string folder = System.IO.Path.GetDirectoryName(filename);
                //System.IO.Directory.CreateDirectory(folder);
                //document.Save(filename);
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception serialising metadata to file {0}.  See InnerException.",
                    filename);
                throw new ParserException(message, ex);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        public void Serialise(out XmlDocument document)
        {
            try
            {
                SaveTo(out document);
            }
            catch (Exception ex)
            {
                String message = String.Format("Exception serialising metadata to XML.  See InnerException.");
                throw new ParserException(message, ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void Deserialise(List<String> filenames, StructureDictionary defDict)
        {
            foreach (String filename in filenames)
            {
                MetaFile file = new MetaFile(filename, defDict);
                file.ParentModel = this;

                if (this.Definition == null)
                    this.Definition = file.Definition;

                Debug.Assert(file.Definition == this.Definition, "Trying to create a multi meta file with different definitions.");
                if (file.Definition == this.Definition)
                    this.MetaFiles.Add(filename, file);
                else
                    Metadata.Log.Error("Trying to create a multi meta file with different definitions.");
            }
        }

        /// <summary>
        /// Initialise object with default values.
        /// </summary>
        private void SetDefaults()
        {
            this.Definition = null;
            this.Members = new Data.StructureTunable(this);
            this.MetaFiles = new Dictionary<String, MetaFile>();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SaveTo(out XmlDocument document)
        {
            document = new XmlDocument();

            // XML Declaration
            XmlDeclaration xmlDecl = document.CreateXmlDeclaration(
                DEFAULT_XML_VERSION, "UTF-8", null);
            document.AppendChild(xmlDecl);

            // Document Root
            XmlElement xmlRoot = document.CreateElement(
                Util.XmlEscape.EscapeDataTypeString(this.Definition.DataType));
            document.AppendChild(xmlRoot);

            // Loop through members
            foreach (Data.ITunable tune in this.Members.Values)
            {
                StructureTunable rootTune = (tune as StructureTunable);
                foreach (Data.ITunable childTune in rootTune.Values)
                {
                    try
                    {
                        if (childTune.InheritParent == true)
                            continue;

                        Debug.Assert(tune is Data.ITunableSerialisable,
                            "Unsupported ITunable, its not serialisable (internal error).");
                        if (!(childTune is Data.ITunableSerialisable))
                            continue;

                        XmlElement xmlElem =
                        (childTune as Data.ITunableSerialisable).Serialise(document, false);
                        if (childTune is PointerTunable)
                        {
                            PointerMember.PointerPolicy policy = ((childTune as PointerTunable).Definition as PointerMember).Policy;
                            if (policy == PointerMember.PointerPolicy.Owner || policy == PointerMember.PointerPolicy.SimpleOwner)
                            {
                                if (((childTune as PointerTunable).Value is ITunable))
                                {
                                    xmlElem.SetAttribute("type", ((childTune as PointerTunable).Value as ITunable).Definition.FriendlyTypeName);
                                }
                                else if (((childTune as PointerTunable).Value is String))
                                {
                                    xmlElem.SetAttribute("type", (childTune as PointerTunable).Value as String);
                                }
                            }
                        }
                        xmlRoot.AppendChild(xmlElem);
                    }
                    catch
                    {
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ResolveMembers(ITunable tunableRoot)
        {
            this.m_resolvedTunables.Add(tunableRoot, GetTunablesFromFiles(tunableRoot));

            if (tunableRoot is StructureTunable)
            {
                foreach (ITunable child in (tunableRoot as StructureTunable).Values)
                {
                    this.ResolveMembers(child);
                }
            }
        }

        private List<ITunable> GetTunablesFromFiles(ITunable sourceTunable)
        {
            List<ITunable> tunables = new List<ITunable>();
            foreach (MetaFile file in this.MetaFiles.Values)
            {
                tunables.Add(GetTunableFromFile(file, sourceTunable));
            }

            return tunables;
        }

        private ITunable GetTunableFromFile(MetaFile file, ITunable sourceTunable)
        {
            String path = GetTunablePath(sourceTunable);
            ITunable foundTunable = GetTunableFromFile(file, path);

            return foundTunable;
        }

        private ITunable GetTunableFromFile(MetaFile file, String tunablePath)
        {
            String[] components = tunablePath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (components.Length > 0)
            {
                // Find the first tunable
                foreach (ITunable tunable in file.Members.Values)
                {
                    ITunable foundTunable = GetTunableFromFile(tunable, tunablePath);
                    if (foundTunable != null)
                        return foundTunable;
                }
            }

            return null;
        }

        private ITunable GetTunableFromFile(ITunable rootTunable, String tunablePath)
        {
            String[] components = tunablePath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            ITunable foundTunable = null;
            if (components.Length > 0)
            {
                // Find the first tunable
                if (rootTunable.Name == components[0])
                {
                    foundTunable = rootTunable;
                }
                else
                {
                    if (rootTunable is StructureTunable)
                    {
                        foreach (ITunable child in (rootTunable as StructureTunable).Values)
                        {
                            foundTunable = GetTunableFromFile(child, tunablePath);
                            if (foundTunable != null)
                                break;
                        }
                    }
                }

                if (foundTunable != null)
                {
                    if (components.Length > 1)
                    {
                        int index = tunablePath.IndexOf('/');
                        String newPath = tunablePath.Substring(index + 1);
                        foundTunable = GetTunableFromFile(foundTunable, newPath);
                    }
                }
            }
            else
            {
                foundTunable = rootTunable;
            }

            return foundTunable;
        }

        private String GetTunablePath(ITunable root)
        {
            ITunable parent = root.ParentModel as ITunable;
            List<String> pathComponents = new List<String>();
            pathComponents.Add(root.Name);
            while (parent != null)
            {
                pathComponents.Add(parent.Name);
                parent = parent.ParentModel as ITunable;
            }

            pathComponents.Reverse();
            String path = String.Empty;
            foreach (String component in pathComponents)
            {
                path += component + "/";
            }
            path = path.TrimEnd(new char[] { '/' });
            return path;
        }

        #endregion // Private Methods

        #region IWeakEventListener Implementation

        /// <summary>
        /// 
        /// </summary>
        public Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (sender is ITunable)
            {
                if (this.m_resolvedTunables.ContainsKey(sender as ITunable))
                {
                    List<ITunable> attachedTunables = this.m_resolvedTunables[(sender as ITunable)];
                    foreach (ITunable attachedTunable in attachedTunables)
                    {
                        attachedTunable.SetTunableValue(sender as ITunable);
                        if ((sender as ITunable).InheritParent != null)
                        {
                            attachedTunable.InheritParent = (sender as ITunable).InheritParent;
                        }
                    }
                }
            }

            return true;
        }

        #endregion 
    } // MultiMetaFiles
} // RSG.Metadata.Model
