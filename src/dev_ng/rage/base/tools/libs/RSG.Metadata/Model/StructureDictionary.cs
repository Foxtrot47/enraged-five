﻿namespace RSG.Metadata.Model
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using System.Xml;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using RSG.Metadata.Parser;

    /// <summary>
    /// Structure Dictionary class mapping Structure type names to Definition 
    /// View Model objects.
    /// </summary>
    public class StructureDictionary : Dictionary<String, Structure>
    {
        #region Fields
        /// <summary>
        /// The static reference to a structure that represents the null data type.
        /// </summary>
        public static Structure NullStructure;

        /// <summary>
        /// The name of the xml node that contains the data used to initialise a
        /// <see cref="RSG.Metadata.Definitions.IStructure"/> instance.
        /// </summary>
        private const string XmlStructureNodeName = "structdef";

        /// <summary>
        /// The name of the xml node that contains the data used to initialise a
        /// <see cref="RSG.Metadata.Definitions.IEnumeration"/> instance.
        /// </summary>
        private const string XmlEnumerationNodeName = "enumdef";

        /// <summary>
        /// The name of the xml node that contains the data used to initialise a
        /// <see cref="RSG.Metadata.Definitions.INumericalConstant"/> instance.
        /// </summary>
        private const string XmlConstantNodeName = "const";

        /// <summary>
        /// The file extension used on psc files.
        /// </summary>
        private const string PscFileExtension = "psc";

        /// <summary>
        /// Generic object used to control access to this instance from multiple threads.
        /// </summary>
        private object syncRoot;

        /// <summary>
        /// The private field used for the <see cref="Enumerations"/> property.
        /// </summary>
        private Dictionary<string, Enumeration> enumerations;

        /// <summary>
        /// The private field used for the <see cref="Warnings"/> property.
        /// </summary>
        private List<string> warnings;

        /// <summary>
        /// The private field used for the <see cref="Errors"/> property.
        /// </summary>
        private List<string> errors;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises the static members of the
        /// <see cref="RSG.Metadata.Model.StructureDictionary"/> class.
        /// </summary>
        static StructureDictionary()
        {
            NullStructure = new Structure(null, "NULL");
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.Metadata.Model.StructureDictionary"/> class.
        /// </summary>
        public StructureDictionary()
        {
            this.syncRoot = new object();
            this.enumerations = new Dictionary<String, Enumeration>();
            //this.duplicatedEnums = new Dictionary<string, List<KeyValuePair<String, int>>>();
            //this.duplicatedStructures = new Dictionary<string, List<KeyValuePair<String, int>>>();
            this.errors = new List<string>();
            this.warnings = new List<string>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the list containing all of the current warnings found during the last reload.
        /// </summary>
        public IEnumerable<string> Warnings
        {
            get { return this.warnings; }
        }

        /// <summary>
        /// Gets the list containing all of the current errors found during the last reload.
        /// </summary>
        public IEnumerable<string> Errors
        {
            get { return this.errors; }
        }

        /// <summary>
        /// Gets a value indicating whether this structure dictionary currently has errors
        /// attached to it.
        /// </summary>
        public bool HasErrors
        {
            get { return this.errors.Count > 0; }
        }
      
        /// <summary>
        /// Gets a value indicating whether this structure dictionary currently has warnings
        /// attached to it.
        /// </summary>
        public bool HasWarning
        {
            get { return this.warnings.Count > 0; }
        }

        /// <summary>
        /// Gets the number of errors currently present in the loaded data.
        /// </summary>
        public int ErrorCount
        {
            get { return this.errors.Count; }
        }

        /// <summary>
        /// Gets the number of warnings currently present in the loaded data.
        /// </summary>
        public int WarningCount
        {
            get { return this.warnings.Count; }
        }

        /// <summary>
        /// Gets the number of enumerations that are currently loaded.
        /// </summary>
        public int EnumerationCount
        {
            get { return this.enumerations.Count; }
        }

        /// <summary>
        /// Gets an iterator over the loaded enumerations.
        /// </summary>
        public Dictionary<string, Enumeration> Enumerations
        {
            get { return enumerations; }
            protected set { enumerations = value; }
        }

        /// <summary>
        /// Gets the number of structures that are currently loaded.
        /// </summary>
        public int StructureCount
        {
            get { return this.Count; }
        }

        /// <summary>
        /// Gets an iterator over the loaded structures.
        /// </summary>
        public IEnumerable<Structure> Structures
        {
            get
            {
                foreach (Structure structure in this.Values)
                {
                    yield return structure;
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a new warning to this structure dictionary.
        /// </summary>
        /// <param name="msg">
        /// The warning message to add.
        /// </param>
        internal void AddWarning(string msg)
        {
            lock (this.warnings)
            {
                this.warnings.Add(msg.Replace("\\r\\n", "\r\n"));
            }
        }
        
        /// <summary>
        /// Adds a new error to this structure dictionary.
        /// </summary>
        /// <param name="msg">
        /// The error message to add.
        /// </param>
        internal void AddError(string msg)
        {
            lock (this.errors)
            {
                this.errors.Add(msg.Replace("\\r\\n", "\r\n"));
            }
        }

        /// <summary>
        /// Loads all of the *.psc files that are found in and under the specified directory.
        /// </summary>
        /// <param name="branch">
        /// The <see cref="RSG.Base.Configuration.IBranch"/> object specifing which
        /// configuration branch the psc files are being loaded for.
        /// </param>
        /// <param name="directory">
        /// The directory path containing the psc files to load.
        /// </param>
        public void Load(IBranch branch, string directory)
        {
            this.Load(branch, directory, true);
        }

        /// <summary>
        /// Loads all of the *.psc files that are found in and under the specified directories.
        /// </summary>
        /// <param name="branch">
        /// The <see cref="RSG.Base.Configuration.IBranch"/> object specifing which
        /// configuration branch the psc files are being loaded for.
        /// </param>
        /// <param name="directories">
        /// A collection of directory paths containing the psc files to load.
        /// </param>
        public void Load(IBranch branch, IEnumerable<string> directories)
        {
            this.Load(branch, directories, true);
        }

        /// <summary>
        /// Loads all of the *.psc files that are found in and/or under the specified directory
        /// depending on the value of the <paramref name="recurse"/> parameter.
        /// </summary>
        /// <param name="branch">
        /// The <see cref="RSG.Base.Configuration.IBranch"/> object specifing which
        /// configuration branch the psc files are being loaded for.
        /// </param>
        /// <param name="directory">
        /// The directory path containing the psc files to load.
        /// </param>
        /// <param name="recurse">
        /// If true indicates that the files loaded are all the files underneath the specified
        /// directory; otherwise, just the files inside the specified directory are loaded.
        /// </param>
        public void Load(IBranch branch, string directory, bool recurse)
        {
            this.Load(branch, new string[] { directory }, recurse);
        }

        /// <summary>
        /// Loads all of the *.psc files that are found in and/or under the specified
        /// directories depending on the value of the <paramref name="recurse"/> parameter.
        /// </summary>
        /// <param name="branch">
        /// The <see cref="RSG.Base.Configuration.IBranch"/> object specifing which
        /// configuration branch the psc files are being loaded for.
        /// </param>
        /// <param name="directories">
        /// A collection of directory paths containing the psc files to load.
        /// </param>
        /// <param name="recurse">
        /// If true indicates that the files loaded are all the files underneath the specified
        /// directories; otherwise, just the files inside the specified directories are loaded.
        /// </param>
        public void Load(IBranch branch, IEnumerable<String> directories, bool recurse)
        {
            if (directories == null)
            {
                return;
            }

            this.errors.Clear();
            this.warnings.Clear();
            Metadata.Log.ProfileCtx(Metadata.LOG_CTX, "Loading PSC structures.");
            try
            {
                ISet<String> filenames = this.GetPscFiles(directories, recurse);
                if (filenames.Count == 0)
                {
                    return;
                }

                Parallel.ForEach(filenames, filename =>
                {
                    try
                    {
                        this.LoadFile(branch, filename);
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = "Unhandled exception occurred while parsing the '{0}' file.";
                        Metadata.Log.ExceptionCtx(ParserError.Context, ex, string.Format(errorMessage, filename));
                    }
                });

                if (!this.ContainsKey("NULL"))
                {
                    this.Add("NULL", StructureDictionary.NullStructure);
                }
            }
            finally
            {
                Metadata.Log.ProfileEnd();
            }
        }

        /// <summary>
        /// Loads the given file.
        /// </summary>
        /// <param name="branch">
        /// The <see cref="RSG.Base.Configuration.IBranch"/> object specifing which
        /// configuration branch the psc files are being loaded for.
        /// </param>
        /// <param name="filename">
        /// The path to the file to load.
        /// </param>
        private void LoadFile(IBranch branch, string filename)
        {
            using (XmlTextReader reader = new XmlTextReader(filename))
            {
                this.CreateDefinitions(branch, reader);
            }
        }

        /// <summary>
        /// Creates definitions using the data provided by the specified System.Xml.XmlReader.
        /// </summary>
        /// <param name="branch">
        /// The <see cref="RSG.Base.Configuration.IBranch"/> object specifing which
        /// configuration branch the psc files are being loaded for.
        /// </param>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to load a set
        /// of definitions.
        /// </param>
        private void CreateDefinitions(IBranch branch, XmlTextReader reader)
        {
            while (reader.MoveToContent() != XmlNodeType.None)
            {
                if (reader.IsStartElement())
                {
                    if (string.CompareOrdinal(reader.Name, XmlStructureNodeName) == 0)
                    {
                        Structure structure = Structure.Create(this, branch, reader.BaseURI, reader);
                        lock (this.syncRoot)
                        {
                            this[structure.DataType] = structure;
                        }

                        continue;
                    }
                    else if (string.CompareOrdinal(reader.Name, XmlEnumerationNodeName) == 0)
                    {
                        Enumeration enumeration = Enumeration.Create(this, reader.BaseURI, reader);
                        lock (this.syncRoot)
                        {
                            this.enumerations[enumeration.DataType] = enumeration;
                        }

                        continue;
                    }
                    else if (string.CompareOrdinal(reader.Name, XmlConstantNodeName) == 0)
                    {
                        Constant constant = Constant.Create(reader.BaseURI, reader);
                        lock (this.syncRoot)
                        {
                            Util.Parser.AddConstant(constant.Name, constant.Value);
                        }

                        continue;
                    }
                }

                reader.Read();
            }
        }

        /// <summary>
        /// Gets all of the psc files that should be loaded based on the specified directories
        /// and recurse parameter.
        /// </summary>
        /// <param name="directories">
        /// A collection of directory paths containing the psc files to load.
        /// </param>
        /// <param name="recurse">
        /// If true indicates that the files loaded are all the files underneath the specified
        /// directories; otherwise, just the files inside the specified directories are loaded.
        /// </param>
        /// <returns>
        /// A set containing the psc files that should be loaded.
        /// </returns>
        private ISet<string> GetPscFiles(IEnumerable<string> directories, bool recurse)
        {
            string searchPattern = string.Format("*.{0}", PscFileExtension);
            SearchOption options = SearchOption.TopDirectoryOnly;
            if (recurse)
            {
                options = SearchOption.AllDirectories;
            }

            ISet<String> filenames = new HashSet<String>();
            foreach (string directory in directories)
            {
                if (!Directory.Exists(directory))
                {
                    continue;
                }

                filenames.AddRange(Directory.GetFiles(directory, searchPattern, options));
            }

            return filenames;
        }
        #endregion
    } // RSG.Metadata.Model.StructureDictionary {Class}
} // RSG.Metadata.Model {Namespace}
