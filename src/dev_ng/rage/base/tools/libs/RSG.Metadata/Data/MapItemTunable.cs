﻿using System.Xml;
using System.Xml.XPath;
using System.ComponentModel;
using RSG.Base.Editor;
using RSG.Metadata.Parser;
using System;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class MapItemTunable : TunableBase,
        ITunableSerialisable,
        ITunableTyped<ITunable>
    {
        #region Constants
        #region XPath Compiled Expressions
        /// <summary>
        /// Commonly used 'key' attribute.
        /// </summary>
        protected static readonly XPathExpression XPATH_ATTR_KEY =
            XPathExpression.Compile("@key");
        #endregion // XPath Compiled Expressions
        #endregion

        #region Fields
        private ITunable m_value;
        private string m_key;
        private IMember m_valueDefinition;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Tunable string value.
        /// </summary>
        [Category("Tunable"),
        Description("Tunable value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public ITunable Value
        {
            get { return m_value; }
            set
            {
                SetPropertyValue(value, m_value, () => this.Value,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_value = (ITunable)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable string key value.
        /// </summary>
        [Category("Tunable"),
        Description("Tunable key."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public string Key
        {
            get { return m_key; }
            set
            {
                SetPropertyValue(value, m_key, () => this.Key,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_key = (string)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsIntegerKeyed
        {
            get
            {
                return (this.ParentModel as MapTunable).IsIntegerKey;
            }
        }
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="v"></param>
        public MapItemTunable(IMember def, IModel parent, string key)
            : base(def, parent)
        {
            this.Value = TunableFactory.Create(this, def);
            this.m_valueDefinition = def;
            this.Key = key;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public MapItemTunable(IMember def, IModel parent, XmlReader reader, XmlTunableFactory factory)
            : base(def, parent)
        {
            this.m_valueDefinition = def;
            Deserialise(reader, factory);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public MapItemTunable(ITunable value, IModel parent)
            : base(value.Definition, parent)
        {
            this.m_valueDefinition = value.Definition;
            this.Value = value;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader, XmlTunableFactory factory)
        {
            this.Key = reader.GetAttribute("key");
            this.Value = factory.Create(this, m_valueDefinition, reader);
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public new XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement("Item");
            if (this.Value is PointerTunable)
            {
                PointerTunable pointer = this.Value as PointerTunable;
                PointerMember member = pointer.Definition as PointerMember;
                PointerMember.PointerPolicy policy = member.Policy;
                if (policy == PointerMember.PointerPolicy.Owner || policy == PointerMember.PointerPolicy.SimpleOwner)
                {
                    if (pointer.Value is ITunable)
                    { 
                        IMember definition = (pointer.Value as ITunable).Definition;
                        if (definition is StructMember)
                        {
                            xmlElem.SetAttribute("type", (definition as StructMember).TypeName);
                        }
                        else
                        {
                            xmlElem.SetAttribute("type", definition.FriendlyTypeName);
                        }
                    }
                    XmlElement value = (this.Value as ITunableSerialisable).Serialise(document, false);
                    xmlElem.InnerXml = value.InnerXml;
                }
                else
                {
                    XmlElement value = (this.Value as ITunableSerialisable).Serialise(document, false);
                    foreach (XmlAttribute attribute in value.Attributes)
                        xmlElem.SetAttribute(attribute.Name, attribute.Value);

                    xmlElem.InnerXml = value.InnerXml;
                }
            }
            else
            {
                XmlElement value = (this.Value as ITunableSerialisable).Serialise(document, false);
                foreach (XmlAttribute attribute in value.Attributes)
                    xmlElem.SetAttribute(attribute.Name, attribute.Value);

                xmlElem.InnerXml = value.InnerXml;
            }

            xmlElem.SetAttribute("key", Key);
            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            if (this.Value is PointerTunable)
            {
                PointerTunable pointer = this.Value as PointerTunable;
                PointerMember member = pointer.Definition as PointerMember;
                PointerMember.PointerPolicy policy = member.Policy;
                if (policy == PointerMember.PointerPolicy.Owner || policy == PointerMember.PointerPolicy.SimpleOwner)
                {
                    if (pointer.Value is ITunable)
                    {
                        IMember definition = (pointer.Value as ITunable).Definition;
                        if (definition is StructMember)
                        {
                            writer.WriteAttributeString("type", (definition as StructMember).TypeName);
                        }
                        else
                        {
                            writer.WriteAttributeString("type", definition.FriendlyTypeName);
                        }
                    }
                }
                else
                {
                    XmlElement value = (this.Value as ITunableSerialisable).Serialise(new XmlDocument(), false);
                    foreach (XmlAttribute attribute in value.Attributes)
                    {
                        writer.WriteAttributeString(attribute.Name, attribute.Value);
                    }
                }
            }
            else
            {
                XmlElement value = (this.Value as ITunableSerialisable).Serialise(new XmlDocument(), false);
                foreach (XmlAttribute attribute in value.Attributes)
                {
                    writer.WriteAttributeString(attribute.Name, attribute.Value);
                }
            }

            writer.WriteAttributeString("key", Key);
        }
        #endregion // Controller Methods

        #region Override
        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is MapItemTunable)
            {
                this.Value = (sourceTunable as MapItemTunable).Value;
                this.Key = (sourceTunable as MapItemTunable).Key;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool HasSameValue(ITunable tunable)
        {
            if (tunable is MapItemTunable)
            {
                if (this.Key != (tunable as MapItemTunable).Key)
                    return false;

                if (this.Value.HasSameValue((tunable as MapItemTunable).Value))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            return "";
        }
        #endregion // Override
    } // MapItemTunable
} // RSG.Metadata.Data
