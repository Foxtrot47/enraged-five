﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Xml;
using RSG.Metadata.Parser;
using RSG.Base.Editor;
using Mvp.Xml.XInclude;

namespace RSG.Metadata.Data
{

    /// <summary>
    /// 
    /// </summary>
    public static class TunableFactory
    {
        /// <summary>
        /// Factory class for constructing ITunable objects from IMember objects.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static ITunable Create(ITunable parent, IMember m)
        {
            if (m is FloatMember)
            {
                return new FloatTunable(m as FloatMember, parent);
            }
            else if (m is Float16Member)
            {
                return new Float16Tunable(m as Float16Member, parent);
            }
            else if (m is BoolMember)
            {
                return new BoolTunable(m as BoolMember, parent);
            }
            else if (m is VecBoolVMember)
            {
                return new VecBoolVTunable(m as VecBoolVMember, parent);
            }
            else if (m is S8Member)
            {
                return new S8Tunable(m as S8Member, parent);
            }
            else if (m is CharMember)
            {
                return new CharTunable(m as CharMember, parent);
            }
            else if (m is U8Member)
            {
                return new U8Tunable(m as U8Member, parent);
            }
            else if (m is S16Member)
            {
                return new S16Tunable(m as S16Member, parent);
            }
            else if (m is ShortMember)
            {
                return new ShortTunable(m as ShortMember, parent);
            }
            else if (m is U16Member)
            {
                return new U16Tunable(m as U16Member, parent);
            }
            else if (m is S32Member)
            {
                return new S32Tunable(m as S32Member, parent);
            }
            else if (m is IntMember)
            {
                return new IntTunable(m as IntMember, parent);
            }
            else if (m is U32Member)
            {
                return new U32Tunable(m as U32Member, parent);
            }
            else if (m is Color32Member)
            {
                return new Color32Tunable(m as Color32Member, parent);
            }
            else if (m is EnumMember)
            {
                return new EnumTunable(m, parent);
            }
            else if (m is BitsetMember)
            {
                return new BitsetTunable(m as BitsetMember, parent);
            }
            else if (m is Vector2Member)
            {
                return new Vector2Tunable(m as Vector2Member, parent);
            }
            else if (m is Vec2VMember)
            {
                return new Vec2VTunable(m as Vec2VMember, parent);
            }
            else if (m is Vector3Member)
            {
                return new Vector3Tunable(m as Vector3Member, parent);
            }
            else if (m is Vec3VMember)
            {
                return new Vec3VTunable(m as Vec3VMember, parent);
            }
            else if (m is Vector4Member)
            {
                return new Vector4Tunable(m as Vector4Member, parent);
            }
            else if (m is Vec4VMember)
            {
                return new Vec4VTunable(m as Vec4VMember, parent);
            }
            else if (m is Mat33VMember)
            {
                return new Mat33VTunable(m as Mat33VMember, parent);
            }
            else if (m is Matrix34Member)
            {
                return new Matrix34Tunable(m as Matrix34Member, parent);
            }
            else if (m is Mat34VMember)
            {
                return new Mat34VTunable(m as Mat34VMember, parent);
            }
            else if (m is Matrix44Member)
            {
                return new Matrix44Tunable(m as Matrix44Member, parent);
            }
            else if (m is Mat44VMember)
            {
                return new Mat44VTunable(m as Mat44VMember, parent);
            }
            else if (m is StructMember)
            {
                return new StructureTunable(m as StructMember, parent);
            }
            else if (m is PointerMember)
            {
                return new PointerTunable(m as PointerMember, parent);
            }
            else if (m is ArrayMember)
            {
                return new ArrayTunable(m as ArrayMember, parent);
            }
            else if (m is StringMember)
            {
                return new StringTunable(m as StringMember, parent);
            }
            else if (m is MapMember)
            {
                return new MapTunable(m as MapMember, parent);
            }

            return (new UnknownTunable(m));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        internal static ITunable CreateScalarMember(ITunable parent, IMember m, String s)
        {
            if (m is FloatMember)
            {
                return new FloatTunable(m as FloatMember, parent, s);
            }
            else if (m is Float16Member)
            {
                return new Float16Tunable(m as Float16Member, parent, s);
            }
            else if (m is BoolMember)
            {
                return new BoolTunable(m as BoolMember, parent, s);
            }
            else if (m is VecBoolVMember)
            {
                return new VecBoolVTunable(m as VecBoolVMember, parent, s);
            }
            else if (m is S8Member)
            {
                return new S8Tunable(m as S8Member, parent, s);
            }
            else if (m is CharMember)
            {
                return new CharTunable(m as CharMember, parent, s);
            }
            else if (m is U8Member)
            {
                return new U8Tunable(m as U8Member, parent, s);
            }
            else if (m is S16Member)
            {
                return new S16Tunable(m as S16Member, parent, s);
            }
            else if (m is ShortMember)
            {
                return new ShortTunable(m as ShortMember, parent, s);
            }
            else if (m is U16Member)
            {
                return new U16Tunable(m as U16Member, parent, s);
            }
            else if (m is S32Member)
            {
                return new S32Tunable(m as S32Member, parent, s);
            }
            else if (m is IntMember)
            {
                return new IntTunable(m as IntMember, parent, s);
            }
            else if (m is U32Member)
            {
                return new U32Tunable(m as U32Member, parent, s);
            }
            else if (m is Color32Member)
            {
                return new Color32Tunable(m as Color32Member, parent, s);
            }
            else if (m is EnumMember)
            {
                return new EnumTunable(m, parent, s);
            }
            else if (m is BitsetMember)
            {
                return new BitsetTunable(m as BitsetMember, parent, s);
            }
            else if (m is Vector2Member)
            {
                return new Vector2Tunable(m as Vector2Member, parent, s);
            }
            else if (m is Vec2VMember)
            {
                return new Vec2VTunable(m as Vec2VMember, parent, s);
            }
            else if (m is Vector3Member)
            {
                return new Vector3Tunable(m as Vector3Member, parent, s);
            }
            else if (m is Vec3VMember)
            {
                return new Vec3VTunable(m as Vec3VMember, parent, s);
            }
            else if (m is Vector4Member)
            {
                return new Vector4Tunable(m as Vector4Member, parent, s);
            }
            else if (m is Vec4VMember)
            {
                return new Vec4VTunable(m as Vec4VMember, parent, s);
            }
            else if (m is Mat33VMember)
            {
                return new Mat33VTunable(m as Mat33VMember, parent, s);
            }
            else if (m is Matrix34Member)
            {
                return new Matrix34Tunable(m as Matrix34Member, parent, s);
            }
            else if (m is Mat34VMember)
            {
                return new Mat34VTunable(m as Mat34VMember, parent, s);
            }
            else if (m is Matrix44Member)
            {
                return new Matrix44Tunable(m as Matrix44Member, parent, s);
            }
            else if (m is Mat44VMember)
            {
                return new Mat44VTunable(m as Mat44VMember, parent, s);
            }
            else if (m is StringMember)
            {
                return new StringTunable(m as StringMember, parent, s);
            }

            return (new UnknownTunable(m));
        }
    }

    public class XmlTunableFactory
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool StartingNewXiFile
        {
            get;
            set;
        }

        public string HRefXiFile
        {
            get;
            set;
        }

        public string XPointerXiFile
        {
            get;
            set;
        }
        #endregion

        /// <summary>
        /// Factory class for constructing ITunable objects from IMember objects and XmlElement nodes.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="reader"></param>
        /// <returns></returns>
        public ITunable Create(IModel parent, IMember m, XmlReader reader)
        {
            Debug.Assert(null != reader, "NULL Reader.");
            bool startingNewFile = this.StartingNewXiFile;
            this.StartingNewXiFile = false;

            if (startingNewFile)
            {
                return new XIncludeTunable(m, parent, reader, this, this.HRefXiFile, this.XPointerXiFile);
            }

            if (m is FloatMember)
            {
                return new FloatTunable(m as FloatMember, parent, reader);
            }
            else if (m is Float16Member)
            {
                return new Float16Tunable(m as Float16Member, parent, reader);
            }
            else if (m is BoolMember)
            {
                return new BoolTunable(m as BoolMember, parent, reader);
            }
            else if (m is VecBoolVMember)
            {
                return new VecBoolVTunable(m as VecBoolVMember, parent, reader);
            }
            else if (m is S8Member)
            {
                return new S8Tunable(m as S8Member, parent as ITunable, reader);
            }
            else if (m is CharMember)
            {
                return new CharTunable(m as CharMember, parent as ITunable, reader);
            }
            else if (m is U8Member)
            {
                return new U8Tunable(m as U8Member, parent as ITunable, reader);
            }
            else if (m is S16Member)
            {
                return new S16Tunable(m as S16Member, parent as ITunable, reader);
            }
            else if (m is ShortMember)
            {
                return new ShortTunable(m as ShortMember, parent as ITunable, reader);
            }
            else if (m is U16Member)
            {
                return new U16Tunable(m as U16Member, parent as ITunable, reader);
            }
            else if (m is S32Member)
            {
                return new S32Tunable(m as S32Member, parent as ITunable, reader);
            }
            else if (m is IntMember)
            {
                return new IntTunable(m as IntMember, parent as ITunable, reader);
            }
            else if (m is U32Member)
            {
                return new U32Tunable(m as U32Member, parent as ITunable, reader);
            }
            else if (m is Color32Member)
            {
                return new Color32Tunable(m as Color32Member, parent, reader);
            }
            else if (m is EnumMember)
            {
                return new EnumTunable(m as EnumMember, parent, reader);
            }
            else if (m is BitsetMember)
            {
                return new BitsetTunable(m as BitsetMember, parent, reader);
            }
            else if (m is Vector2Member)
            {
                return new Vector2Tunable(m as Vector2Member, parent as ITunable, reader);
            }
            else if (m is Vec2VMember)
            {
                return new Vec2VTunable(m as Vec2VMember, parent as ITunable, reader);
            }
            else if (m is Vector3Member)
            {
                return new Vector3Tunable(m as Vector3Member, parent as ITunable, reader);
            }
            else if (m is Vec3VMember)
            {
                return new Vec3VTunable(m as Vec3VMember, parent as ITunable, reader);
            }
            else if (m is Vector4Member)
            {
                return new Vector4Tunable(m as Vector4Member, parent as ITunable, reader);
            }
            else if (m is Vec4VMember)
            {
                return new Vec4VTunable(m as Vec4VMember, parent as ITunable, reader);
            }
            else if (m is Mat33VMember)
            {
                return new Mat33VTunable(m as Mat33VMember, parent as ITunable, reader);
            }
            else if (m is Matrix34Member)
            {
                return new Matrix34Tunable(m as Matrix34Member, parent as ITunable, reader);
            }
            else if (m is Mat34VMember)
            {
                return new Mat34VTunable(m as Mat34VMember, parent as ITunable, reader);
            }
            else if (m is Matrix44Member)
            {
                return new Matrix44Tunable(m as Matrix44Member, parent as ITunable, reader);
            }
            else if (m is Mat44VMember)
            {
                return new Mat44VTunable(m as Mat44VMember, parent as ITunable, reader);
            }
            else if (m is StructMember)
            {
                return new StructureTunable(m as StructMember, parent, reader, this);
            }
            else if (m is PointerMember)
            {
                return new PointerTunable(m as PointerMember, parent, reader, this);
            }
            else if (m is ArrayMember)
            {
                return new ArrayTunable(m as ArrayMember, parent, reader, this);
            }
            else if (m is StringMember)
            {
                return new StringTunable(m as StringMember, parent, reader);
            }
            else if (m is MapMember)
            {
                return new MapTunable(m as MapMember, parent, reader, this);
            }

            return (new UnknownTunable(m, parent, reader));
        }
    }

} // RSG.Metadata.Data namespace
