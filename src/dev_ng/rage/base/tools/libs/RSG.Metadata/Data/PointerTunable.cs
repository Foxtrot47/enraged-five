﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Parser;
using RSG.Base.Editor;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class PointerTunable :
        TunableBase,
        ITunable,
        ITunableTyped<Object>,
        ITunableSerialisable
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private static readonly String ATTR_REF = "ref";
        private static readonly String ATTR_TYPE = "type";

        /// <summary>
        /// 
        /// </summary>
        private static readonly XPathExpression XPATH_ATTR_TYPE = 
            XPathExpression.Compile("@type");

        /// <summary>
        /// 
        /// </summary>
        private static readonly XPathExpression XPATH_NODE_ITEM =
            XPathExpression.Compile("/Item");
        #endregion // Constants

        #region Members

        private Object m_value;

        #endregion // Members

        #region Properties and Associated Member Data

        /// <summary>
        /// Tunable pointer reference value.
        /// </summary>
        [Category("Pointer"),
        Description("Tunable pointer reference value."), RSG.Base.Editor.Command.UndoableProperty(),
        PointerCollapse()]
        public Object Value
        {
            get { return m_value; }
            set
            {
                SetPropertyValue(value, m_value, () => this.Value,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_value = newValue;
                        }
                ));
            }
        }

        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="v"></param>
        public PointerTunable(PointerMember def, IModel parent)
            : base(def, parent)
        {
            if (def.Policy == PointerMember.PointerPolicy.ExternalNamed ||
                def.Policy == PointerMember.PointerPolicy.Link ||
                def.Policy == PointerMember.PointerPolicy.LazyLink)
            {
                this.Value = "NULL";
            }
            else
            {
                StructMember sm = new StructMember(Model.StructureDictionary.NullStructure, def.ParentStructure);
                this.Value = TunableFactory.Create(this, sm);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public PointerTunable(PointerMember def, IModel parent, XmlReader reader, XmlTunableFactory factory)
            : base(def, parent)
        {
            if (def.Policy == PointerMember.PointerPolicy.ExternalNamed ||
                def.Policy == PointerMember.PointerPolicy.Link ||
                def.Policy == PointerMember.PointerPolicy.LazyLink)
            {
                this.Value = "NULL";
            }
            else
            {
                StructMember sm = new StructMember(Model.StructureDictionary.NullStructure, def.ParentStructure);
                this.Value = TunableFactory.Create(this, sm);
            }
            Deserialise(reader, factory);
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader, XmlTunableFactory factory)
        {
            PointerMember m = (this.Definition as PointerMember);

            if ((PointerMember.PointerPolicy.Owner == m.Policy) ||
                (PointerMember.PointerPolicy.SimpleOwner == m.Policy))
            {
                String rawDatatype = reader.GetAttribute(ATTR_TYPE);
                if (!String.IsNullOrWhiteSpace(rawDatatype) && !String.IsNullOrEmpty(rawDatatype))
                {
                    String dataType = Namespace.ConvertDataTypeToFriendlyName(rawDatatype);
                    Debug.Assert(m.DefinitionDictionary.ContainsKey(dataType),
                        String.Format("Pointer data type '({0})' not found.", dataType));

                    if (!m.DefinitionDictionary.ContainsKey(dataType))
                    {
                        // Fatal Error unable to load the meta file without all the structures.
                        bool caseWrong = false;
                        string realType = null;
                        foreach (string key in m.DefinitionDictionary.Keys)
                        {
                            if (string.Compare(key, dataType, true) == 0)
                            {
                                caseWrong = true;
                                realType = key;
                                break;
                            }
                        }

                        if (caseWrong)
                        {
                            string msg = string.Format("The data type defined as '({0})' appears to not have the correct case, did you mean '{1}'?", dataType, realType);
                            Metadata.Log.Error(msg);
                            throw new NotSupportedException(msg);
                        }
                        else
                        {
                            string msg = string.Format("Pointer data type '({0})' not found unable to load metadata file. Please make sure the type exists in a psc file.", dataType);
                            Metadata.Log.Error(msg);
                            throw new NotSupportedException(msg);
                        }
                    }
                    else
                    {
                        Structure itemStruct = m.DefinitionDictionary[dataType];
                        Debug.Assert(itemStruct.IsA(m.ObjectType) || itemStruct == RSG.Metadata.Model.StructureDictionary.NullStructure,
                            String.Format("Invalid pointer member; not a valid '({0})' object.", m.ObjectTypeName));

                        StructMember sm = new StructMember(itemStruct, m.ParentStructure);
                        this.Value = factory.Create(this, sm, reader);
                    }
                }
                else if (m.ObjectType != null)
                {
                    StructMember sm = new StructMember(m.ObjectType, m.ParentStructure);
                    this.Value = factory.Create(this, sm, reader);
                }
                else
                {
                    // Fatal Error unable to load the meta file without all the structures.
                    Metadata.Log.Error("Unable to de-serialise pointer value as its missing a type.");
                    throw new NotSupportedException();
                }
            }
            else if (PointerMember.PointerPolicy.ExternalNamed == m.Policy ||
                     PointerMember.PointerPolicy.Link == m.Policy ||
                     PointerMember.PointerPolicy.LazyLink == m.Policy)
            {
                // Object is named using the 'ref' attribute.
                this.Value = reader.GetAttribute(ATTR_REF);
                bool isEmpty = reader.IsEmptyElement;
                reader.ReadStartElement();
                if (!isEmpty)
                {
                    if (reader.NodeType == XmlNodeType.Text)
                        reader.ReadString();
                    if (reader.NodeType == XmlNodeType.EndElement)
                        reader.ReadEndElement();
                }
            }
            else
            {
                Metadata.Log.Error("Unsupported Pointer Policy {0}.", m.Policy.ToString());
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public new XmlElement Serialise(XmlDocument document, bool root)
        {
            PointerMember.PointerPolicy policy = (this.Definition as PointerMember).Policy;
            if (policy == PointerMember.PointerPolicy.Owner || policy == PointerMember.PointerPolicy.SimpleOwner)
            {
                if (this.Value is ITunableSerialisable)
                {
                    XmlElement pointerChild = (this.Value as ITunableSerialisable).Serialise(document, root);
                    if (this.ParentModel is ArrayTunable)
                    {
                        return pointerChild;
                    }
                    string xmlElementName = this.Definition.Name;
                    if (string.IsNullOrWhiteSpace(xmlElementName))
                    {
                        xmlElementName = "Item";
                    }

                    XmlElement pointer = document.CreateElement(xmlElementName);
                    pointer.InnerXml = pointerChild.InnerXml;
                    return pointer;
                }
            }
            else if (policy == PointerMember.PointerPolicy.Link ||
                     policy == PointerMember.PointerPolicy.LazyLink ||
                     policy == PointerMember.PointerPolicy.ExternalNamed)
            {
                string xmlElementName = this.Definition.Name;
                if (string.IsNullOrWhiteSpace(xmlElementName))
                {
                    xmlElementName = "Item";
                }
                string value = "NULL";
                if (value != null)
                {
                    value = this.Value.ToString();
                    if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                        value = "NULL";
                }
                XmlElement xmlPointer = Util.Xml.CreateElementWithAttribute(document, null, xmlElementName, "ref", value);
                return xmlPointer;
            }
            
            return document.CreateElement("Item");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            PointerTunable pointer = this;
            PointerMember member = pointer.Definition as PointerMember;
            PointerMember.PointerPolicy policy = member.Policy;
            if (policy == PointerMember.PointerPolicy.Owner || policy == PointerMember.PointerPolicy.SimpleOwner)
            {
                if (pointer.Value is ITunable)
                {
                    IMember definition = (pointer.Value as ITunable).Definition;
                    string type = null;
                    if (definition is StructMember)
                    {
                        type = (definition as StructMember).TypeName.Replace(':', '_');
                    }
                    else
                    {
                        type = definition.FriendlyTypeName.Replace(':', '_');
                    }

                    writer.WriteAttributeString("type", type);
                }
            }
            else
            {
                string value = "NULL";
                if (value != null)
                {
                    value = this.Value.ToString();
                    if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                        value = "NULL";
                }

                writer.WriteAttributeString("ref", value);
            }
        }
        #endregion // Controller Methods

        #region Override
        
        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (!(sourceTunable is PointerTunable))
                return;

            this.ParentModel = sourceTunable.ParentModel;
            PointerTunable source = sourceTunable as PointerTunable;
            if (!(source.Definition == this.Definition))
                return;

            if (source.Value is ITunable)
            {
                this.Value = TunableFactory.Create(this, (source.Value as ITunable).Definition);
                (this.Value as ITunable).SetTunableValue(source.Value as ITunable);
            }
            else
            {
                this.Value = (sourceTunable as PointerTunable).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is PointerTunable)
            {
                if (this.Definition.Equals((tunable as PointerTunable).Definition))
                {
                    if (this.Value is ITunable && (tunable as PointerTunable).Value is ITunable)
                    {
                        if ((this.Value as ITunable).HasSameValue((tunable as PointerTunable).Value as ITunable))
                            return true;
                    }
                    else
                    {
                        if (this.Value.Equals((tunable as PointerTunable).Value))
                            return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            return "";
        }

        #endregion // Override

        private static Boolean PredictValueCollapse(Object obj1, Object obj2)
        {
            if (obj1 is String)
            {
                if (obj2 is String || obj2 == null)
                    return true;
            }
            if (obj2 is String)
            {
                if (obj1 is String || obj1 == null)
                    return true;
            }

            return false;
        }
    } // PointerTunable

    public class PointerCollapse : RSG.Base.Editor.Command.CollapsableProperty
    {
        public override Boolean Collapse(Object oldValue, Object newValue)
        {
            if (oldValue is String)
            {
                if (newValue is String || newValue == null)
                    return true;
            }
            if (newValue is String)
            {
                if (oldValue == null)
                    return true;
            }

            return false;
        }
    }

} // RSG.Metadata.Data namespace
