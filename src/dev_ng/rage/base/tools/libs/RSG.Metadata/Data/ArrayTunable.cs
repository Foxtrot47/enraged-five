﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using RSG.Metadata.Parser;
using System.Collections.Specialized;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Editor.Collections;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class ArrayTunable : HierarchicalModelBase,
        ITunable,
        ITunableSerialisable,
        ITunableArray,
        INotifyCollectionChanged
    {
        #region Events
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        #endregion // Events

        #region Members
        private IMember m_definition;
        private String m_name;
        private Boolean? m_inheritParent = false;
        private ModelObservableCollection<ITunable> m_Items;
        #endregion // Members

        #region Properties
        /// <summary>
        /// Definition for elements.
        /// </summary>
        [Browsable(false), DontRegister()]
        public IMember Definition
        {
            get { return m_definition; }
            set
            {
                SetPropertyValue(value, m_definition, () => this.Definition,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_definition = (IMember)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Number of items in this Array.
        /// </summary>
        public int Length
        {
            get { return (this.m_Items.Count); }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                SetPropertyValue(value, m_name, () => this.Name,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_name = (String)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean CanInheritParent
        {
            get
            {
                if (this.ParentModel is StructureTunable)
                {
                    if (((this.ParentModel as StructureTunable).Definition as StructMember).Definition.UsesDataInheritance == true)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean? InheritParent
        {
            get { return m_inheritParent; }
            set
            {
                SetPropertyValue(value, m_inheritParent, () => this.InheritParent,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_inheritParent = (Boolean?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Our list of elements.
        /// </summary>
        [UndoableProperty()]
        public ModelObservableCollection<ITunable> Items
        {
            get { return m_Items; }
            set
            {
                SetPropertyValue(value, m_Items, () => this.Items,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_Items = (ModelObservableCollection<ITunable>)newValue;
                        }
                ));
            }
        }

        #endregion // Properties and Associated Member Data
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public ArrayTunable(ArrayMember def, IModel parent)
            : base(parent)
        {
            this.ParentModel = parent;
            this.Definition = def;
            if (String.IsNullOrEmpty(def.Name))
            {
                if (!String.IsNullOrEmpty(def.FriendlyTypeName))
                {
                    this.Name = RSG.Metadata.Parser.Namespace.GetShortName(def.FriendlyTypeName);
                }
            }
            else
            {
                this.Name = def.Name;
            }
            this.Items = new ModelObservableCollection<ITunable>(this, "Items");
            int staticSize = def.Size;
            if (staticSize != -1 || staticSize != 0)
            {
                while (this.Length < staticSize)
                {
                    this.Items.Add(TunableFactory.Create(this, def.ElementType));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public ArrayTunable(ArrayMember def, IModel parent, XmlReader reader, XmlTunableFactory factory)
            : base(parent)
        {
            this.ParentModel = parent;
            this.Definition = def;
            if (String.IsNullOrEmpty(def.Name))
            {
                if (!String.IsNullOrEmpty(def.FriendlyTypeName))
                {
                    this.Name = RSG.Metadata.Parser.Namespace.GetShortName(def.FriendlyTypeName);
                }
            }
            else
            {
                this.Name = def.Name;
            }

            this.Items = new ModelObservableCollection<ITunable>(this, "Items");
            Deserialise(reader, factory);
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Indexer, set, get methods.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ITunable this[int index]
        {
            get { return this.m_Items[index]; }
            set { this.m_Items[index] = value; }
        }

        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader, XmlTunableFactory factory)
        {
            Debug.Assert(this.Definition is ArrayMember, "ArrayTunable requires ArrayMember definition.");
            ArrayMember m = (this.Definition as ArrayMember);
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();

            if (m.ElementType == null)
            {
                // Fatal Error unable to load the meta file without all the structures being dfeined.
                Metadata.Log.Error("Unable to load metadata file, the structure with type name '({0})' doesn't appear to exist. Please make sure the type exists in a psc file.",
                    m.ElementStructureType);
                throw new NotSupportedException();
            }

            if (!isEmpty)
            {
                if (m.ElementType is IMemberScalar)
                {
                    // Scalar elements are parsed in a single XML text node with
                    // whitespace between each element.
                    if (reader.NodeType == XmlNodeType.Text)
                    {
                        String[] elements = reader.ReadString().Split(new String[] { " ", "\t", "\r", "\r\n", "\n" },
                            StringSplitOptions.RemoveEmptyEntries);

                        foreach (String element in elements)
                        {
                            ITunable tunable = TunableFactory.CreateScalarMember(this, m.ElementType, element);
                            Add(tunable);
                        }
                    }
                    else if (m.ElementType is BitsetMember || m.ElementType is EnumMember)
                    {
                        // Bitsets and ebnums use <item></item> blocks.
                        while (reader.MoveToContent() == XmlNodeType.Element)
                        {
                            if (reader.Name == "Item")
                            {
                                reader.ReadStartElement();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    ITunable tunable = TunableFactory.CreateScalarMember(this, m.ElementType, reader.ReadString().Trim());
                                    Add(tunable);
                                }
                                if (reader.NodeType == XmlNodeType.EndElement)
                                    reader.ReadEndElement();
                            }
                            else
                            {
                                reader.Skip();
                            }
                        }
                    }
                }
                else
                {
                    // Non-scalar elements are parsed by having separate <item>
                    // XML elements.
                    while (reader.MoveToContent() == XmlNodeType.Element)
                    {
                        Add(factory.Create(this, m.ElementType, reader));
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Serialise the tunable to a new XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public XmlElement Serialise(XmlDocument document, bool root)
        {
            Debug.Assert(this.Definition is ArrayMember,
                "ArrayTunable requires ArrayMember definition.");
            ArrayMember m = (this.Definition as ArrayMember);

            string nodeName = this.Definition.Name;
            if (string.IsNullOrEmpty(nodeName))
            {
                nodeName = "Item";
            }
            XmlElement xmlElem = document.CreateElement(nodeName);

            IMember eType = m.ElementType;
            if (m.ElementType is IMemberScalar)
            {
                if (m.ElementType is BitsetMember)
                {
                    foreach (ITunable tunable in this.m_Items)
                    {
                        XmlElement element = (tunable as BitsetTunable).SerialiseToArray(document, false);
                        xmlElem.AppendChild(element);
                    }
                }
                else if (m.ElementType is EnumMember)
                {
                    foreach (ITunable tunable in this.m_Items)
                    {
                        XmlElement element = (tunable as EnumTunable).SerialiseToArray(document, false);
                        xmlElem.AppendChild(element);
                    }
                }
                else if (m.ElementType is StringMember)
                {
                    foreach (ITunable tunable in this.m_Items)
                    {
                        XmlElement element = (tunable as StringTunable).SerialiseToArray(document, false);
                        xmlElem.AppendChild(element);
                    }
                }
                else
                {
                    String text = String.Empty;
                    int index = 0;
                    foreach (ITunable tunable in this.m_Items)
                    {
                        if (index == 0)
                            text += String.Format("{0}{1}", tunable.ToXmlString(), Environment.NewLine);
                        else
                            text += String.Format(" {0}{1}", tunable.ToXmlString(), Environment.NewLine);
                        index++;
                    }
                    if (this.m_Items.Count > 0)
                    {
                        XmlText xmlText = document.CreateTextNode(text);
                        xmlElem.AppendChild(xmlText);
                    }
                    if ((this.Definition as ArrayMember).ElementType is U8Member)
                    {
                        xmlElem.SetAttribute("content", "char_array");
                    }
                    else if ((this.Definition as ArrayMember).ElementType is U16Member)
                    {
                        xmlElem.SetAttribute("content", "short_array");
                    }
                    else if ((this.Definition as ArrayMember).ElementType is U32Member)
                    {
                        xmlElem.SetAttribute("content", "short_array");
                    }
                    else if ((this.Definition as ArrayMember).ElementType is S8Member)
                    {
                        xmlElem.SetAttribute("content", "char_array");
                    }
                    else if ((this.Definition as ArrayMember).ElementType is S16Member)
                    {
                        xmlElem.SetAttribute("content", "short_array");
                    }
                    else if ((this.Definition as ArrayMember).ElementType is S16Member)
                    {
                        xmlElem.SetAttribute("content", "int_array");
                    }
                    else if ((this.Definition as ArrayMember).ElementType is FloatMember)
                    {
                        xmlElem.SetAttribute("content", "float_array");
                    }
                    else
                    {
                        xmlElem.SetAttribute("content", String.Format("{0}_array", (this.Definition as ArrayMember).ElementType.FriendlyTypeName));
                    }
                }
            }
            else if (eType is VecBoolVMember || eType is Vector2Member || eType is Vec2VMember || eType is Vector3Member || eType is Vec3VMember
                || m.ElementType is Vector4Member || m.ElementType is Vec4VMember)
            {
                String text = String.Empty;
                int index = 0;
                foreach (ITunable tunable in this.m_Items)
                {
                    if (index == 0)
                        text += String.Format("{0}", tunable.ToXmlString());
                    else
                        text += String.Format(" {0}", tunable.ToXmlString());
                    index++;
                }
                if (this.m_Items.Count > 0)
                {
                    XmlText xmlText = document.CreateTextNode(text);
                    xmlElem.AppendChild(xmlText);
                }
                xmlElem.SetAttribute("content", String.Format("array_{0}", (this.Definition as ArrayMember).ElementType.FriendlyTypeName));
            }
            else if (m.ElementType is StringMember)
            {
                foreach (ITunable tunable in this.m_Items)
                {
                    Debug.Assert(tunable is ITunableSerialisable);
                    if (!(tunable is ITunableSerialisable))
                        continue;

                    ITunableSerialisable ts = (tunable as ITunableSerialisable);
                    if (tunable is StringTunable)
                    {
                        XmlElement xmlStringElem = document.CreateElement("Item");
                        XmlText xmlText = document.CreateTextNode((tunable as StringTunable).Value);
                        xmlStringElem.AppendChild(xmlText);
                        xmlElem.AppendChild(xmlStringElem);
                    }
                    else
                    {
                        XmlElement xmlTunable = ts.Serialise(document, false);
                        XmlElement xmlItem = document.CreateElement("Item");
                        foreach (XmlNode child in xmlTunable.ChildNodes)
                            xmlItem.AppendChild(child.Clone());
                        xmlElem.AppendChild(xmlItem);
                    }
                }
            }
            else
            {
                foreach (ITunable tunable in this.m_Items)
                {
                    Debug.Assert(tunable is ITunableSerialisable);
                    if (!(tunable is ITunableSerialisable))
                        continue;

                    ITunableSerialisable ts = (tunable as ITunableSerialisable);
                    XmlElement xmlTunable = ts.Serialise(document, false);
                    XmlElement xmlItem = null;
                    if (tunable is PointerTunable)
                    {
                        PointerMember pointerDefinition = (tunable as PointerTunable).Definition as PointerMember;
                        if (pointerDefinition.Policy == PointerMember.PointerPolicy.ExternalNamed ||
                            pointerDefinition.Policy == PointerMember.PointerPolicy.Link ||
                            pointerDefinition.Policy == PointerMember.PointerPolicy.LazyLink)
                        {
                            string value = "NULL";
                            if (value != null)
                            {
                                value = (tunable as PointerTunable).Value.ToString();
                                if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                                    value = "NULL";
                            }

                            xmlItem = Util.Xml.CreateElementWithAttribute(document, null, "item", "ref", value);
                        }
                        else
                        {
                            xmlItem = document.CreateElement("Item");
                            foreach (XmlNode child in xmlTunable.ChildNodes)
                                xmlItem.AppendChild(child.Clone());

                            PointerMember.PointerPolicy policy = ((tunable as PointerTunable).Definition as PointerMember).Policy;
                            if (policy == PointerMember.PointerPolicy.Owner || policy == PointerMember.PointerPolicy.SimpleOwner)
                            {
                                if (((tunable as PointerTunable).Value is ITunable))
                                {
                                    IMember definition = ((tunable as PointerTunable).Value as ITunable).Definition;
                                    if (definition is StructMember)
                                    {
                                        xmlItem.SetAttribute("type", (definition as StructMember).TypeName);
                                    }
                                    else
                                    {
                                        xmlItem.SetAttribute("type", definition.FriendlyTypeName);
                                    }
                                }
                                else if (((tunable as PointerTunable).Value is String))
                                {
                                    xmlItem.SetAttribute("type", (tunable as PointerTunable).Value as String);
                                }
                            }
                        }
                    }
                    else
                    {
                        xmlItem = document.CreateElement("Item");
                        foreach (XmlNode child in xmlTunable.ChildNodes)
                            xmlItem.AppendChild(child.Clone());
                    }
                    xmlElem.AppendChild(xmlItem);
                }
            }

            return (xmlElem);    
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual String ToXmlString()
        {
            return ("");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public void SerialiseWithoutChildren(XmlWriter writer)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetInheritParent(Boolean? newValue)
        {
            this.InheritParent = newValue;
            this.OnPropertyChanged("InheritParent");
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void SetTunableValue(ITunable sourceTunable)
        {
            if (!(sourceTunable is ArrayTunable))
                return;

            ArrayTunable source = sourceTunable as ArrayTunable;
            if (!(source.Definition == this.Definition))
                return;

            this.Items.Clear();
            foreach (ITunable child in source.Items)
            {
                ITunable newTunable = TunableFactory.Create(this.ParentModel as ITunable, child.Definition);
                newTunable.SetTunableValue(child);
                this.Items.Add(newTunable);
            }
            this.InheritParent = source.InheritParent;
        }

        #endregion // Controller Methods

        #region Collection Methods
        /// <summary>
        /// Add a new element to the Array.
        /// </summary>
        /// <param name="node"></param>
        public void Add(ITunable item)
        {
            this.m_Items.Add(item);

            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        /// <summary>
        /// Remove an element from the Array.
        /// </summary>
        /// <returns></returns>
        /// <param name="item">Item to remove from Array</param>
        public bool Remove(ITunable item)
        {
            bool res = (this.m_Items.Remove(item));

            if (res && CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));

            return res;
        }

        /// <summary>
        /// Remove an element from the Array (by index).
        /// </summary>
        /// <param name="index">Index of item to remove from Array</param>
        /// <returns></returns>
        public void RemoveAt(int index)
        {
            ITunable removedItem = m_Items[index];
            this.m_Items.RemoveAt(index);

            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItem, index));
        }

        /// <summary>
        /// Return index of a particular tunable.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int IndexOf(ITunable item)
        {
            return (this.m_Items.IndexOf(item));
        }

        /// <summary>
        /// Insert an tunable item at a particular index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, ITunable item)
        {
            this.m_Items.Insert(index, item);

            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
        }

        /// <summary>
        /// Clear all items.
        /// </summary>
        public void Clear()
        {
            this.m_Items.Clear();
            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Move(int oldIndex, int newIndex)
        {
            using (var block = new BatchUndoRedoBlock(this))
            {
                ITunable item = this[oldIndex];
                this.Items.RemoveAt(oldIndex);
                this.Items.Insert(newIndex, item);
            }
        }

        #endregion // Collection Methods

        #region IEnumerable<ITunable> Interface
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ITunable> GetEnumerator()
        {
            return (this.m_Items.GetEnumerator());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetEnumerator());
        }
        #endregion // IEnumerable<ITunable> Interface

        #region Override
        /// <summary>
        /// 
        /// </summary>
        public Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is ArrayTunable)
            {
                if (this.Length != (tunable as ArrayTunable).Length)
                    return false;

                for (int i = 0; i < this.Length; i++)
                {
                    if (!this.m_Items[i].HasSameValue((tunable as ArrayTunable).m_Items[i]))
                        return false;
                }

                return true;
            }
            return false;
        }

        #endregion // Override
    }

} // RSG.Metadata.Data namespace
