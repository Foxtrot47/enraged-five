﻿using System;
using System.Windows.Media;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Math;
using RSG.Base.Editor;
using RSG.Metadata.Parser;

namespace RSG.Metadata.Data
{
    #region BaseDataType Vector Tunable Class

    /// <summary>
    /// 
    /// </summary>
    public abstract class VectorTunableBase<T> : 
        TunableBase,
        ITunableSerialisable
    {
        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        public enum VectorType
        {
            Normal, // Regular floating-point vector elements.
            Colour  // Floating-point vector represents RGB colour.
        };
        #endregion // Enumerations

        #region Fields

        private VectorType m_vectorKind;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Category("Vector")]
        public VectorType VectorKind
        {
            get { return m_vectorKind; }
            set
            {
                SetPropertyValue(value, m_vectorKind, () => this.VectorKind,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_vectorKind = (VectorType)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public VectorTunableBase(IMember def, ITunable parent)
            : base(def, parent)
        {
            Reset();
        }

        /// <summary>
        /// 
        /// </summary>
        public VectorTunableBase(IMember def, ITunable parent, XmlReader reader)
            : base(def, parent)
        {
            Reset();
            this.ParentModel = parent;
            Deserialise(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        public VectorTunableBase(IMember def, ITunable parent, String s)
            : base(def, parent)
        {
            Reset();
            Parse(s);
        }

        #endregion // Constructor(s)        
        
        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        protected virtual void Reset()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        public virtual void Parse(String s)
        {
        }

        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public virtual void Deserialise(XmlReader reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        protected void SerialiseAttribute(XmlDocument doc, XmlElement ele, string attribute, float value)
        {
            if (this.Definition is IMemberHighPrecision)
            {
                String stringValue = String.Empty;
                if ((this.Definition as IMemberHighPrecision).HighPrecision == true)
                {
                    stringValue = String.Format("{0:0.000000000}", value);
                }
                else
                {
                    stringValue = String.Format("{0:0.000000}", value);
                }

                SetXmlAttribute(doc, ele, attribute, stringValue);
            }
            else
            {
                SetXmlAttribute(doc, ele, attribute, value.ToString());
            }
        }
        #endregion // Controller Methods
    }

    #endregion // BaseDataType Vector Tunable Class

    #region 2-Component Vector Tunable Class

    #region Base Class

    /// <summary>
    /// 2-float component vector tunable class.
    /// </summary>
    public class Vector2TunableBase :
        VectorTunableBase<float>
    {
        #region Fields

        private float m_x;
        private float m_y;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Vector"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float X
        {
            get { return m_x; }
            set
            {
                SetPropertyValue(value, m_x, () => this.X,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_x = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Vector"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float Y
        {
            get { return m_y; }
            set
            {
                SetPropertyValue(value, m_y, () => this.Y,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_y = (float)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Vector2TunableBase(Vector2MemberBase def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2TunableBase(IMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2TunableBase(Vector2MemberBase def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            this.X = (this.Definition as Vector2MemberBase).Init[0];
            this.Y = (this.Definition as Vector2MemberBase).Init[1];
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Parse(String s)
        {
            String[] elements = s.Split(new String[] { " ", "\t", "\r", "\r\n", "\n" },
                    StringSplitOptions.RemoveEmptyEntries);

            Debug.Assert(elements.Length == 2, String.Format("Unable to parse Vector2TunableBase tunable with {0} parts for string {1}", elements.Length, s));
            this.X = (float)Util.Parser.ParseString(elements[0]);
            this.Y = (float)Util.Parser.ParseString(elements[1]);
        }

        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlReader reader)
        {
            string x = reader.GetAttribute("x");
            if (!String.IsNullOrWhiteSpace(x) && !String.IsNullOrEmpty(x))
            {
                float value = 0.0f;
                if (!float.TryParse(x, out value))
                {
                    this.X = 0.0f;
                }
                else
                {
                    this.X = value;
                }
            }

            string y = reader.GetAttribute("y");
            if (!String.IsNullOrWhiteSpace(y) && !String.IsNullOrEmpty(y))
            {
                float value = 0.0f;
                if (!float.TryParse(y, out value))
                {
                    this.Y = 0.0f;
                }
                else
                {
                    this.Y = value;
                }
            }

            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
            else
            {
                reader.ReadStartElement();
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public override XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement(this.Definition.Name);
            SerialiseAttribute(document, xmlElem, "x", this.X);
            SerialiseAttribute(document, xmlElem, "y", this.Y);
            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToXmlString()
        {
            return string.Format("{0} {1}", this.X, this.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            writer.WriteAttributeString("x", this.X.ToString());
            writer.WriteAttributeString("y", this.Y.ToString());
        }

        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is Vector2TunableBase)
            {
                this.X = (sourceTunable as Vector2TunableBase).X;
                this.Y = (sourceTunable as Vector2TunableBase).Y;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is Vector2TunableBase)
            {
                if (this.X.Equals((tunable as Vector2TunableBase).X) &&
                    this.Y.Equals((tunable as Vector2TunableBase).Y))
                    return true;
            }
            return false;
        }

        #endregion // Override
    } // Vector2TunableBase

    #endregion // BaseClass

    #region Vector2 Tunable

    /// <summary>
    /// 2-float component vector tunable class.
    /// </summary>
    public class Vector2Tunable : Vector2TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Vector2Tunable(Vector2Member def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2Tunable(Vector2Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2Tunable(Vector2Member def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    }

    #endregion // Vector2 Tunable

    #region Vec2V Tunable

    /// <summary>
    /// 2-float component vector tunable class.
    /// </summary>
    public class Vec2VTunable : Vector2TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Vec2VTunable(Vec2VMember def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vec2VTunable(Vec2VMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vec2VTunable(Vec2VMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // Vec2VTunable

    #endregion // Vec2V Tunable

    #endregion // 2-Component Vector Tunable Class

    #region 3-Component Vector Tunable Class

    #region Base Class

    /// <summary>
    /// 3-float component vector tunable class.
    /// </summary>
    public class Vector3TunableBase :
        VectorTunableBase<float>
    {
        #region Fields

        private float m_x;
        private float m_y;
        private float m_z;
        private Color m_colour;
        private Boolean m_currentlySyncing = false;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Vector"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float X
        {
            get { return m_x; }
            set
            {
                SetPropertyValue(value, m_x, () => this.X,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_x = (float)newValue;
                            SyncColourToValues();
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Vector"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float Y
        {
            get { return m_y; }
            set
            {
                SetPropertyValue(value, m_y, () => this.Y,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_y = (float)newValue;
                            SyncColourToValues();
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Vector"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float Z
        {
            get { return m_z; }
            set
            {
                SetPropertyValue(value, m_z, () => this.Z,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_z = (float)newValue;
                            SyncColourToValues();
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Vector"),
        Description("Tunable vector value as colour.")]
        public Color Colour
        {
            get { return m_colour; }
            set
            {
                SetPropertyValue(value, m_colour, () => this.Colour,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_colour = (Color)newValue;
                            SyncValuesToColour();
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Vector3TunableBase(Vector3MemberBase def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector3TunableBase(Vector3MemberBase def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector3TunableBase(Vector3MemberBase def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            this.X = (this.Definition as Vector3MemberBase).Init[0];
            this.Y = (this.Definition as Vector3MemberBase).Init[1];
            this.Z = (this.Definition as Vector3MemberBase).Init[2];
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Parse(String s)
        {
            String[] elements = s.Split(new String[] { " ", "\t", "\r", "\r\n", "\n" },
                    StringSplitOptions.RemoveEmptyEntries);

            Debug.Assert(elements.Length == 3, String.Format("Unable to parse Vector3TunableBase tunable with {0} parts for string {1}", elements.Length, s));
            this.X = (float)Util.Parser.ParseString(elements[0]);
            this.Y = (float)Util.Parser.ParseString(elements[1]);
            this.Z = (float)Util.Parser.ParseString(elements[2]);
        }

        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlReader reader)
        {
            String x = reader.GetAttribute("x");
            if (!String.IsNullOrWhiteSpace(x) && !String.IsNullOrEmpty(x))
            {
                float result = 0.0f;
                float.TryParse(x, out result);
                this.X = result;
            }

            String y = reader.GetAttribute("y");
            if (!String.IsNullOrWhiteSpace(y) && !String.IsNullOrEmpty(y))
            {
                float result = 0.0f;
                float.TryParse(y, out result);
                this.Y = result;
            }

            String z = reader.GetAttribute("z");
            if (!String.IsNullOrWhiteSpace(z) && !String.IsNullOrEmpty(z))
            {
                float result = 0.0f;
                float.TryParse(z, out result);
                this.Z = result;
            }

            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
            else
            {
                reader.ReadStartElement();
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public override XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement(this.Definition.Name);
            SerialiseAttribute(document, xmlElem, "x", this.X);
            SerialiseAttribute(document, xmlElem, "y", this.Y);
            SerialiseAttribute(document, xmlElem, "z", this.Z);
            return (xmlElem);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            return string.Format("{0} {1} {2}", this.X, this.Y, this.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            writer.WriteAttributeString("x", this.X.ToString());
            writer.WriteAttributeString("y", this.Y.ToString());
            writer.WriteAttributeString("z", this.Z.ToString());
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        private void SyncColourToValues()
        {
            if (m_currentlySyncing == true)
                return;

            m_currentlySyncing = true;

            byte r = System.Math.Max((byte)0, System.Math.Min((byte)255, (byte)(X * 255)));
            byte g = System.Math.Max((byte)0, System.Math.Min((byte)255, (byte)(Y * 255)));
            byte b = System.Math.Max((byte)0, System.Math.Min((byte)255, (byte)(Z * 255)));
            this.Colour = Color.FromArgb(255, r, g, b);

            m_currentlySyncing = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SyncValuesToColour()
        {
            if (m_currentlySyncing == true)
                return;

            m_currentlySyncing = true;

            float inverseScalar = 1.0f / 255.0f;
            this.X = this.Colour.R * inverseScalar;
            this.Y = this.Colour.G * inverseScalar;
            this.Z = this.Colour.B * inverseScalar;

            m_currentlySyncing = false;
        }

        #endregion // Private Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is Vector3TunableBase)
            {
                this.X = (sourceTunable as Vector3TunableBase).X;
                this.Y = (sourceTunable as Vector3TunableBase).Y;
                this.Z = (sourceTunable as Vector3TunableBase).Z;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is Vector3TunableBase)
            {
                if (this.X.Equals((tunable as Vector3TunableBase).X) &&
                    this.Y.Equals((tunable as Vector3TunableBase).Y) &&
                    this.Z.Equals((tunable as Vector3TunableBase).Z))
                    return true;
            }
            return false;
        }

        #endregion // Override
    } // Vector3TunableBase

    #endregion // BaseClass

    #region Vector3 Tunable

    /// <summary>
    /// 3-float component vector tunable class.
    /// </summary>
    public class Vector3Tunable : Vector3TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Vector3Tunable(Vector3Member def, ITunable parent)
            : base(def, parent)
        {
        }
   
        /// <summary>
        /// 
        /// </summary>
        public Vector3Tunable(Vector3Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }
     
        /// <summary>
        /// 
        /// </summary>
        public Vector3Tunable(Vector3Member def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // Vector3Tunable

    #endregion // Vector3 Tunable

    #region Vec3V Tunable

    /// <summary>
    /// 3-float component vector tunable class.
    /// </summary>
    public class Vec3VTunable : Vector3TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Vec3VTunable(Vec3VMember def, ITunable parent)
            : base(def, parent)
        {
        }
   
        /// <summary>
        /// 
        /// </summary>
        public Vec3VTunable(Vec3VMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }
     
        /// <summary>
        /// 
        /// </summary>
        public Vec3VTunable(Vec3VMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // Vec3VTunable

    #endregion // Vec3V Tunable

    #endregion // 3-Component Vector Tunable Class

    #region 4-Component Vector Tunable Class

    #region Base Class

    /// <summary>
    /// 4-float component vector tunable class.
    /// </summary>
    public class Vector4TunableBase :
        VectorTunableBase<float>
    {
        #region Fields

        private float m_x;
        private float m_y;
        private float m_z;
        private float m_w;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Vector"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float X
        {
            get { return m_x; }
            set
            {
                SetPropertyValue(value, m_x, () => this.X,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_x = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Vector"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float Y
        {
            get { return m_y; }
            set
            {
                SetPropertyValue(value, m_y, () => this.Y,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_y = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Vector"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float Z
        {
            get { return m_z; }
            set
            {
                SetPropertyValue(value, m_z, () => this.Z,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_z = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Vector"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float W
        {
            get { return m_w; }
            set
            {
                SetPropertyValue(value, m_w, () => this.W,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_w = (float)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Vector4TunableBase(Vector4MemberBase def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector4TunableBase(Vector4MemberBase def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector4TunableBase(Vector4MemberBase def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        protected override void Reset()
        {
            this.X = (this.Definition as Vector4MemberBase).Init[0];
            this.Y = (this.Definition as Vector4MemberBase).Init[1];
            this.Z = (this.Definition as Vector4MemberBase).Init[2];
            this.W = (this.Definition as Vector4MemberBase).Init[3];
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Parse(String s)
        {
            String[] elements = s.Split(new String[] { " ", "\t", "\r", "\r\n", "\n" },
                    StringSplitOptions.RemoveEmptyEntries);

            Debug.Assert(elements.Length == 4, String.Format("Unable to parse Vector4TunableBase tunable with {0} parts for string {1}", elements.Length, s));
            this.X = (float)Util.Parser.ParseString(elements[0]);
            this.Y = (float)Util.Parser.ParseString(elements[1]);
            this.Z = (float)Util.Parser.ParseString(elements[2]);
            this.W = (float)Util.Parser.ParseString(elements[3]);
        }

        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlReader reader)
        {
            String x = reader.GetAttribute("x");
            if (!String.IsNullOrWhiteSpace(x) && !String.IsNullOrEmpty(x))
                this.X = float.Parse(x);

            String y = reader.GetAttribute("y");
            if (!String.IsNullOrWhiteSpace(y) && !String.IsNullOrEmpty(y))
                this.Y = float.Parse(y);

            String z = reader.GetAttribute("z");
            if (!String.IsNullOrWhiteSpace(z) && !String.IsNullOrEmpty(z))
                this.Z = float.Parse(z);

            String w = reader.GetAttribute("w");
            if (!String.IsNullOrWhiteSpace(w) && !String.IsNullOrEmpty(w))
                this.W = float.Parse(w);

            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
            else
            {
                reader.ReadStartElement();
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public override XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement(this.Definition.Name);
            SerialiseAttribute(document, xmlElem, "x", this.X);
            SerialiseAttribute(document, xmlElem, "y", this.Y);
            SerialiseAttribute(document, xmlElem, "z", this.Z);
            SerialiseAttribute(document, xmlElem, "w", this.W);
            return (xmlElem);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToXmlString()
        {
            return string.Format("{0} {1} {2} {3}", this.X, this.Y,
                                                    this.Z, this.W);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            writer.WriteAttributeString("x", this.X.ToString());
            writer.WriteAttributeString("y", this.Y.ToString());
            writer.WriteAttributeString("z", this.Z.ToString());
            writer.WriteAttributeString("w", this.W.ToString());
        }

        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is Vector4TunableBase)
            {
                this.X = (sourceTunable as Vector4TunableBase).X;
                this.Y = (sourceTunable as Vector4TunableBase).Y;
                this.Z = (sourceTunable as Vector4TunableBase).Z;
                this.W = (sourceTunable as Vector4TunableBase).W;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is Vector4TunableBase)
            {
                if (this.X.Equals((tunable as Vector4TunableBase).X) &&
                    this.Y.Equals((tunable as Vector4TunableBase).Y) &&
                    this.Z.Equals((tunable as Vector4TunableBase).Z) &&
                    this.W.Equals((tunable as Vector4TunableBase).W))
                    return true;
            }
            return false;
        }

        #endregion // Override
    }

    #endregion // BaseClass

    #region Vector4 Tunable

    /// <summary>
    /// 4-float component vector tunable class.
    /// </summary>
    public class Vector4Tunable : Vector4TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Vector4Tunable(Vector4Member def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector4Tunable(Vector4Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector4Tunable(Vector4Member def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // Vector4Tunable

    #endregion // Vector4 Tunable

    #region Vec4V Tunable

    /// <summary>
    /// 4-float component vector tunable class.
    /// </summary>
    public class Vec4VTunable : Vector4TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Vec4VTunable(Vec4VMember def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vec4VTunable(Vec4VMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Vec4VTunable(Vec4VMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // Vec4VTunable

    #endregion // Vec4V Tunable

    #endregion // 4-Component Vector Tunable Class

    #region 4-Boolean-Component Vector Tunable Class

    /// <summary>
    /// 4-boolean component vector tunable class.
    /// </summary>
    public class VecBoolVTunable :
        TunableBase,
        ITunableSerialisable
    {
        #region Fields

        private Boolean m_x;
        private Boolean m_y;
        private Boolean m_z;
        private Boolean m_w;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("VecBoolV"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public Boolean X
        {
            get { return m_x; }
            set
            {
                SetPropertyValue(value, m_x, () => this.X,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_x = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("VecBoolV"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public Boolean Y
        {
            get { return m_y; }
            set
            {
                SetPropertyValue(value, m_y, () => this.Y,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_y = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("VecBoolV"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public Boolean Z
        {
            get { return m_z; }
            set
            {
                SetPropertyValue(value, m_z, () => this.Z,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_z = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("VecBoolV"),
        Description("Tunable vector value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public Boolean W
        {
            get { return m_w; }
            set
            {
                SetPropertyValue(value, m_w, () => this.W,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_w = (Boolean)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public VecBoolVTunable(VecBoolVMember def, ITunable parent)
            : base(def, parent)
        {
            this.SetValues(def.Init);
        }

        /// <summary>
        /// 
        /// </summary>
        public VecBoolVTunable(VecBoolVMember def, IModel parent, XmlReader reader)
            : base(def, parent)
        {
            this.SetValues(def.Init);
            this.ParentModel = parent;
            Deserialise(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        public VecBoolVTunable(VecBoolVMember def, ITunable parent, String s)
            : base(def, parent)
        {
            this.SetValues(def.Init);
            this.Parse(s);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public void Parse(String s)
        {
            String[] elements = s.Split(new String[] { " ", "\t", "\r", "\r\n", "\n" },
                    StringSplitOptions.RemoveEmptyEntries);

            Debug.Assert(elements.Length == 4, String.Format("Unable to parse VecBoolV tunable with {0} parts for string {1}", elements.Length, s));
            this.X = bool.Parse(elements[0]);
            this.Y = bool.Parse(elements[1]);
            this.Z = bool.Parse(elements[2]);
            this.W = bool.Parse(elements[3]);
        }

        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader)
        {
            this.X = DeserialiseAttribute(reader, "x");
            this.Y = DeserialiseAttribute(reader, "y");
            this.Z = DeserialiseAttribute(reader, "z");
            this.W = DeserialiseAttribute(reader, "w");

            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
            else
            {
                reader.ReadStartElement();
            }
        }

        private Boolean DeserialiseAttribute(XmlReader reader, String name)
        {
            String value = reader.GetAttribute(name);
            if (!String.IsNullOrWhiteSpace(value) && !String.IsNullOrEmpty(value))
            {
                bool boolean = false;
                if (bool.TryParse(value, out boolean))
                    return boolean;
            }
            return false;
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public new XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement(this.Definition.Name);
            SetXmlAttribute(document, xmlElem, "x", this.X == false ? "false" : "true");
            SetXmlAttribute(document, xmlElem, "y", this.Y == false ? "false" : "true");
            SetXmlAttribute(document, xmlElem, "z", this.Z == false ? "false" : "true");
            SetXmlAttribute(document, xmlElem, "w", this.W == false ? "false" : "true");

            return (xmlElem);
        }

        public override string ToXmlString()
        {
            return string.Format("{0} {1} {2} {3}", this.X == false ? "false" : "true",
                                                    this.Y == false ? "false" : "true", 
                                                    this.Z == false ? "false" : "true", 
                                                    this.W == false ? "false" : "true");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            writer.WriteAttributeString("x", this.X == false ? "false" : "true");
            writer.WriteAttributeString("y", this.Y == false ? "false" : "true");
            writer.WriteAttributeString("x", this.Z == false ? "false" : "true");
            writer.WriteAttributeString("w", this.W == false ? "false" : "true");
        }

        #endregion // Controller Methods

        #region Private Functions

        /// <summary>
        /// Sets this tunable value using an array of boolean values,
        /// used to set from the init value
        /// </summary>
        /// <param name="values"></param>
        private void SetValues(Boolean[] values)
        {
            Debug.Assert(values.Length == 4, String.Format("Unable to set a VecBoolV using an array of {0} varaibles", values.Length));
            this.X = values[0];
            this.Y = values[1];
            this.Z = values[2];
            this.W = values[3];
        }

        #endregion // Private Functions

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is VecBoolVTunable)
            {
                this.X = (sourceTunable as VecBoolVTunable).X;
                this.Y = (sourceTunable as VecBoolVTunable).Y;
                this.Z = (sourceTunable as VecBoolVTunable).Z;
                this.W = (sourceTunable as VecBoolVTunable).W;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is VecBoolVTunable)
            {
                if (this.X.Equals((tunable as VecBoolVTunable).X) &&
                    this.Y.Equals((tunable as VecBoolVTunable).Y) &&
                    this.Z.Equals((tunable as VecBoolVTunable).Z) &&
                    this.W.Equals((tunable as VecBoolVTunable).W))
                    return true;
            }
            return false;
        }


        #endregion // Override
    } // VecBoolVTunable

    #endregion // 4-Component Vector Tunable Class
} // RSG.Metadata.Data namespace
