﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using RSG.Metadata.Parser;
using RSG.Base.Editor;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class UnknownTunable :
        TunableBase,
        ITunable
    {
        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="member"></param>
        public UnknownTunable(IMember member)
            : base(member, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="member"></param>
        public UnknownTunable(IMember member, IModel parent, XmlReader reader)
            : base(member, parent)
        {
            reader.Skip();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Return true if the given object is the same tunable class with the
        /// same tunable value.
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is UnknownTunable)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
        }
        #endregion
    } // RSG.Metadata.Data.UnknownTunable {Class}
} // RSG.Metadata.Data {Namespace}
