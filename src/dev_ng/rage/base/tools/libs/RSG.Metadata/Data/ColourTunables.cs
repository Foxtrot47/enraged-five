﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Media;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Parser;
using RSG.Base.Editor;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// 32-bit colour tunable class.
    /// </summary>
    public class Color32Tunable :
        TunableBase,
        ITunable,
        ITunableTyped<Color>,
        ITunableSerialisable,
        ITunableScalar<Color>
    {
        #region Fields

        private Color m_value;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable colour value.
        /// </summary>
        [Category("Tunable"),
        Description("Tunable colour value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public Color Value
        {
            get { return m_value; }
            set
            {
                SetPropertyValue(value, m_value, () => this.Value,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_value = (Color)newValue;
                        }
                ));
            }
        }

        public uint ValueAsUint
        {
            get
            {
                if (Value == null)
                    return 0;

                return (uint)((Value.A << 24) | (Value.R << 16) |
                    (Value.G << 8) | (Value.B << 0));
            }
        }
        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public Color32Tunable(Color32Member def, IModel parent)
            : base(def, parent)
        {
            this.Value = def.Init;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public Color32Tunable(Color32Member def, IModel parent, XmlReader reader)
            : base(def, parent)
        {
            this.Value = def.Init;
            Deserialise(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public Color32Tunable(IMember def, IModel parent, String s)
            : base(def, parent)
        {
            this.Value = (def as Color32Member).Init;
            this.Value = Parse(s);
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader)
        {
            String value = reader.GetAttribute("value");
            if (!String.IsNullOrEmpty(value) && !String.IsNullOrWhiteSpace(value))
            {
                this.Value = Parse(value);
            }
            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
            else
            {
                reader.ReadStartElement();
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public override XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = base.Serialise(document, root);
            SetXmlAttribute(document, xmlElem, "value", ToXmlString());
            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public Color Parse(String s)
        {
            UInt32 v = default(UInt32);
            bool parseResult = false;
            if (s.StartsWith("0x"))
            {
                // Hexadecimal.
                s = s.Replace("0x", "");
                parseResult = UInt32.TryParse(s, NumberStyles.HexNumber,
                    CultureInfo.CurrentCulture, out v);
            }
            else
            {
                // Assume Decimal.
                parseResult = UInt32.TryParse(s, NumberStyles.Integer,
                    CultureInfo.CurrentCulture, out v);
            }
            Debug.Assert(parseResult, "Error parsing color32 value.  Adopting default value.");
            return Color.FromArgb(
                (byte)((v & 0xFF000000) >> 24),
                (byte)((v & 0x00FF0000) >> 16),
                (byte)((v & 0x0000FF00) >> 8),
                (byte)((v & 0x000000FF) >> 0));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            UInt32 argb = ((uint)Value.A << 24) | ((uint)Value.R << 16) |
                ((uint)Value.G << 8) | ((uint)Value.B);
            
            return (String.Format("0x{0:X8}", argb));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            writer.WriteAttributeString("value", ToXmlString());
        }

        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is Color32Tunable)
            {
                this.Value = (sourceTunable as Color32Tunable).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is Color32Tunable)
            {
                if (this.Value.Equals((tunable as Color32Tunable).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override
    } // Color32Tunable
} // RSG.Metadata.Data namespace
