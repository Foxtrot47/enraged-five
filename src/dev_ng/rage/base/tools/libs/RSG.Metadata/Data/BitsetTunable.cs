﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Media;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.Xml.XPath;
using RSG.Metadata.Parser;
using RSG.Base.Editor;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// Bitset tunable class.
    /// </summary>
    public class BitsetTunable : 
        TunableBase,
        ITunable,
        ITunableTyped<String>,
        ITunableSerialisable
    {
        #region Fields

        private String m_value;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable string value.
        /// </summary>
        [Category("Tunable"),
        Description("Tunable string value."), RSG.Base.Editor.Command.UndoableProperty()]
        public String Value
        {
            get { return m_value; }
            set
            {
                SetPropertyValue(value, m_value, () => this.Value,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_value = (String)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public BitsetTunable(BitsetMember def, IModel parent)
            : base(def, parent)
        {
            this.Value = def.Init;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public BitsetTunable(BitsetMember def, IModel parent, string s)
            : base(def, parent)
        {
            this.Value = def.Init;
            Parse(s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public BitsetTunable(BitsetMember def, IModel parent, XmlReader reader)
            : base(def, parent)
        {
            this.Value = def.Init;
            Deserialise(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader)
        {
            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                {
                    this.Value = reader.ReadString();
                    this.Value = this.Value.Replace("\r", "");
                    this.Value = this.Value.Replace("\n", "");
                    this.Value = this.Value.Replace("\t", "");
                    String[] parts = this.Value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    this.Value = String.Empty;
                    int index = 0;
                    foreach (String part in parts)
                    {
                        if (index != 0)
                        {
                            this.Value += " " + part;
                        }
                        else
                        {
                            this.Value += part;
                        }
                        index++;
                    }
                }
                else
                {
                    this.Value = string.Empty;
                }

                reader.MoveToContent();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
            else
            {
                this.Value = string.Empty;
                reader.ReadStartElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public override XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement(this.Definition.Name);
            Boolean addAttribute = false;
            Dictionary<string, long> nameValueMap = (this.Definition as BitsetMember).Values;
            if (nameValueMap == null || nameValueMap.Count == 0)
            {
                addAttribute = true;
            }

            string bitsetValue = this.Value;
            List<string> sorted = new List<string>();
            if (!String.IsNullOrEmpty(bitsetValue))
            {
                // Fix up string so that all instances of '|' are removed.
                bitsetValue = this.Value.Replace('|', ' ');
                if ((this.Definition as BitsetMember).Values != null)
                {
                    string[] parts = this.Value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string part in parts)
                    {
                        if (!(this.Definition as BitsetMember).Values.ContainsKey(part))
                        {
                            addAttribute = true;
                        }
                    }

                    List<string> names = nameValueMap.Keys.ToList();
                    sorted = parts.OrderBy(i => i, new BitsetValueComparer(names)).ToList();
                    bitsetValue = String.Join(" ", sorted);
                }


                if (addAttribute == true)
                {
                    BitsetMember.BitsetType type = (this.Definition as BitsetMember).Type;
                    switch (type)
                    {
                        case BitsetMember.BitsetType.atBitSet:
                        case BitsetMember.BitsetType.Fixed:
                        case BitsetMember.BitsetType.Fixed32:
                            xmlElem.SetAttribute("content", "int_array");
                            break;
                        case BitsetMember.BitsetType.Fixed16:
                            xmlElem.SetAttribute("content", "short_array");
                            break;
                        case BitsetMember.BitsetType.Fixed8:
                            xmlElem.SetAttribute("content", "char_array");
                            break;
                        default:
                            xmlElem.SetAttribute("content", "int_array");
                            break;
                    }
                }

                bitsetValue = Regex.Replace(bitsetValue, " {2,}", " ");
                base.SetXmlText(document, xmlElem, bitsetValue);
            }
            else
            {
            }
            return xmlElem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public XmlElement SerialiseToArray(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement("Item");

            if (!String.IsNullOrEmpty(this.Value))
            {
                base.SetXmlText(document, xmlElem, this.Value);
            }
            else
            {
            }
            return xmlElem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        public void Parse(string s)
        {
            this.Value = s;
            this.Value = this.Value.Replace("\r", "");
            this.Value = this.Value.Replace("\n", "");
            this.Value = this.Value.Replace("\t", "");
            String[] parts = this.Value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            this.Value = String.Empty;
            int index = 0;
            foreach (String part in parts)
            {
                if (index != 0)
                {
                    this.Value += " " + part;
                }
                else
                {
                    this.Value += part;
                }
                index++;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            return Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            Boolean addAttribute = false;
            Dictionary<string, long> nameValueMap = (this.Definition as BitsetMember).Values;
            if (nameValueMap == null || nameValueMap.Count == 0)
            {
                addAttribute = true;
            }

            string bitsetValue = this.Value;
            List<string> sorted = new List<string>();
            if (!String.IsNullOrEmpty(bitsetValue))
            {
                // Fix up string so that all instances of '|' are removed.
                bitsetValue = this.Value.Replace('|', ' ');
                if ((this.Definition as BitsetMember).Values != null)
                {
                    string[] parts = this.Value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string part in parts)
                    {
                        if (!(this.Definition as BitsetMember).Values.ContainsKey(part))
                        {
                            addAttribute = true;
                        }
                    }

                    List<string> names = nameValueMap.Keys.ToList();
                    sorted = parts.OrderBy(i => i, new BitsetValueComparer(names)).ToList();
                    bitsetValue = String.Join(" ", sorted);
                }


                if (addAttribute == true)
                {
                    BitsetMember.BitsetType type = (this.Definition as BitsetMember).Type;
                    switch (type)
                    {
                        case BitsetMember.BitsetType.atBitSet:
                        case BitsetMember.BitsetType.Fixed:
                        case BitsetMember.BitsetType.Fixed32:
                            writer.WriteAttributeString("content", "int_array");
                            break;
                        case BitsetMember.BitsetType.Fixed16:
                            writer.WriteAttributeString("content", "short_array");
                            break;
                        case BitsetMember.BitsetType.Fixed8:
                            writer.WriteAttributeString("content", "char_array");
                            break;
                        default:
                            writer.WriteAttributeString("content", "int_array");
                            break;
                    }
                }

                bitsetValue = Regex.Replace(bitsetValue, " {2,}", " ");
                writer.WriteString(bitsetValue);
            }
        }

        #endregion // Controller Methods

        #region Override
        
        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is BitsetTunable)
            {
                this.Value = (sourceTunable as BitsetTunable).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is BitsetTunable)
            {
                if (this.Value.Equals((tunable as BitsetTunable).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override

        #region Classes
        /// <summary>
        /// A comparer that compares two string where the numbers are treated numerical.
        /// </summary>
        private class BitsetValueComparer : IComparer<string>
        {
            #region Fields
            /// <summary>
            /// The private list of values that are used to determine the comparison value
            /// between two string values.
            /// </summary>
            private List<string> enumerationValues;
            #endregion Fields

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="BitsetValueComparer"/> class.
            /// </summary>
            /// <param name="enumerationValues">
            /// The list containing all the values from the associated enumeration to use as a
            /// comparison map.
            /// </param>
            public BitsetValueComparer(List<string> enumerationValues)
            {
                this.enumerationValues = enumerationValues;
            }
            #endregion

            #region Methods
            /// <summary>
            /// Compares two objects and returns a value indicating whether one is less than,
            /// equal to, or greater than the other.
            /// </summary>
            /// <param name="x">
            /// The first object to compare.
            /// </param>
            /// <param name="y">
            /// The second object to compare.
            /// </param>
            /// <returns>
            /// A signed integer that indicates the relative values of x and y. Less than zero:
            /// x is less than y. Zero: x equals y. Greater than zero: x is greater than y.
            /// </returns>
            public int Compare(string x, string y)
            {
                int xIndex = this.enumerationValues.IndexOf(x);
                int yIndex = this.enumerationValues.IndexOf(y);

                if (xIndex < yIndex)
                {
                    return -1;
                }
                else if (xIndex > yIndex)
                {
                    return 1;
                }

                return 0;
            }
            #endregion Methods
        }
        #endregion
    } // BitsetTunable
} // RSG.Metadata.Data namespace
