﻿using RSG.Base.Editor;
using System.Collections.Specialized;
using RSG.Metadata.Parser;
using RSG.Base.Editor.Collections;
using System.ComponentModel;
using RSG.Base.Editor.Command;
using System.Xml;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.Xml.XPath;
using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace RSG.Metadata.Data
{
    public class MapTunable : HierarchicalModelBase,
        ITunable,
        ITunableSerialisable,
        INotifyCollectionChanged
    {
        #region Events
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        #endregion // Events

        #region Constants
        /// <summary>
        /// Non-scalar Map elements are in 'item' tags.
        /// </summary>
        private static readonly XPathExpression XPATH_ITEM =
            XPathExpression.Compile("Item");

        /// <summary>
        /// Scalar Map elemnents are in the XML text separated by whitespace.
        /// </summary>
        private static readonly XPathExpression XPATH_TEXT =
            XPathExpression.Compile("text()");
        #endregion // Constants

        #region Members
        private IMember m_definition;
        private string m_name;
        private bool? m_inheritParent = false;
        private ModelObservableCollection<MapItemTunable> m_Items;
        #endregion // Members

        #region Properties

        /// <summary>
        /// Definition for elements.
        /// </summary>
        [Browsable(false), DontRegister()]
        public IMember Definition
        {
            get { return m_definition; }
            set
            {
                SetPropertyValue(value, m_definition, () => this.Definition,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_definition = (IMember)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Number of items in this Map.
        /// </summary>
        public int Length
        {
            get { return (this.m_Items.Count); }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return m_name; }
            set
            {
                SetPropertyValue(value, m_name, () => this.Name,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_name = (string)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool CanInheritParent
        {
            get
            {
                if (this.ParentModel is StructureTunable)
                {
                    if (((this.ParentModel as StructureTunable).Definition as StructMember).Definition.UsesDataInheritance == true)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool? InheritParent
        {
            get { return m_inheritParent; }
            set
            {
                SetPropertyValue(value, m_inheritParent, () => this.InheritParent,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_inheritParent = (bool?)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Our list of elements.
        /// </summary>
        [UndoableProperty()]
        public ModelObservableCollection<MapItemTunable> Items
        {
            get { return m_Items; }
            set
            {
                SetPropertyValue(value, m_Items, () => this.Items,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(object newValue)
                        {
                            m_Items = (ModelObservableCollection<MapItemTunable>)newValue;
                        }
                ));
            }
        }

        public bool IsIntegerKey
        {
            get
            {
                if ((this.Definition as MapMember).KeyType == "s8")
                {
                    return true;
                }
                else if ((this.Definition as MapMember).KeyType == "u8")
                {
                    return true;

                }
                else if ((this.Definition as MapMember).KeyType == "s16")
                {
                    return true;
                }
                else if ((this.Definition as MapMember).KeyType == "u16")
                {
                    return true;
                }
                else if ((this.Definition as MapMember).KeyType == "s32")
                {
                    return true;
                }
                else if ((this.Definition as MapMember).KeyType == "u32")
                {
                    return true;
                }
                return false;
            }
        }

        #endregion // Properties and Associated Member Data
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public MapTunable(MapMember def, ITunable parent)
            : base(parent)
        {
            this.m_definition = def;
            if (string.IsNullOrEmpty(def.Name))
            {
                if (!string.IsNullOrEmpty(def.FriendlyTypeName))
                {
                    this.m_name = RSG.Metadata.Parser.Namespace.GetShortName(def.FriendlyTypeName);
                }
            }
            else
            {
                this.m_name = def.Name;
            }
            this.m_Items = new ModelObservableCollection<MapItemTunable>(this, "Items");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public MapTunable(MapMember def, IModel parent, XmlReader reader, XmlTunableFactory factory)
            : base(parent)
        {
            this.m_definition = def;
            if (string.IsNullOrEmpty(def.Name))
            {
                if (!string.IsNullOrEmpty(def.FriendlyTypeName))
                {
                    this.m_name = RSG.Metadata.Parser.Namespace.GetShortName(def.FriendlyTypeName);
                }
            }
            else
            {
                this.m_name = def.Name;
            }

            this.m_Items = new ModelObservableCollection<MapItemTunable>(this, "Items");
            Deserialise(reader, factory);
        }
        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// Indexer, set, get methods.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public MapItemTunable this[int index]
        {
            get { return this.m_Items[index]; }
            set { this.m_Items[index] = value; }
        }

        public void Deserialise(XmlElement node)
        {}

        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader, XmlTunableFactory factory)
        {
            Debug.Assert(this.Definition is MapMember, "MapTunable requires MapMember definition.");
            MapMember m = (this.Definition as MapMember);
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();

            if (m.ElementType == null)
            {
                // Fatal Error unable to load the meta file without all the structures being dfeined.
                Metadata.Log.Error("Unable to load metadata file, the structure with type name '({0})' doesn't appear to exist. Please make sure the type exists in a psc file.",
                    m.ElementStructureType);
                throw new NotSupportedException();
            }

            if (!isEmpty)
            {
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    Add(new MapItemTunable(m.ElementType, this, reader, factory));
                }

                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Serialise the tunable to a new XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public XmlElement Serialise(XmlDocument document, bool root)
        {
            Debug.Assert(this.Definition is MapMember,
                "MapTunable requires MapMember definition.");
            MapMember m = (this.Definition as MapMember);
            XmlElement xmlElem = document.CreateElement(this.Definition.Name);

            List<MapItemTunable> sorted = this.m_Items.OrderBy(i => i.Key, new MapKeyComparer()).ToList();
            foreach (MapItemTunable item in sorted)
            {
                xmlElem.AppendChild(item.Serialise(document, false));
            }

            return xmlElem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual string ToXmlString()
        {
            return ("");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public void SerialiseWithoutChildren(XmlWriter writer)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetInheritParent(bool? newValue)
        {
            this.InheritParent = newValue;
            this.OnPropertyChanged("InheritParent");
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void SetTunableValue(ITunable sourceTunable)
        {
            if (!(sourceTunable is MapTunable))
                return;

            MapTunable source = sourceTunable as MapTunable;
            if (!(source.Definition == this.Definition))
                return;

            this.Items.Clear();
            int index = 0;
            IMember itemDefinition = (Definition as MapMember).ElementType;
            foreach (ITunable child in source.Items)
            {
                MapItemTunable newTunable = new MapItemTunable(itemDefinition, this, (index++).ToString());

                newTunable.SetTunableValue(child);
                this.Items.Add(newTunable);
            }
            this.InheritParent = source.InheritParent;
        }

        #endregion // Controller Methods

        #region Collection Methods

        /// <summary>
        /// Add a new element to the Map.
        /// </summary>
        /// <param name="node"></param>
        public void Add(MapItemTunable item)
        {
            this.m_Items.Add(item);

            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        }

        /// <summary>
        /// Remove an element from the Map.
        /// </summary>
        /// <returns></returns>
        /// <param name="item">Item to remove from Map</param>
        public bool Remove(MapItemTunable item)
        {
            bool res = (this.m_Items.Remove(item));

            if (res && CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));

            return res;
        }

        /// <summary>
        /// Remove an element from the Map (by index).
        /// </summary>
        /// <param name="index">Index of item to remove from Map</param>
        /// <returns></returns>
        public void RemoveAt(int index)
        {
            ITunable removedItem = m_Items[index];
            this.m_Items.RemoveAt(index);

            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItem, index));
        }

        /// <summary>
        /// Return index of a particular tunable.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int IndexOf(MapItemTunable item)
        {
            return (this.m_Items.IndexOf(item));
        }

        /// <summary>
        /// Insert an tunable item at a particular index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, MapItemTunable item)
        {
            this.m_Items.Insert(index, item);

            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
        }

        /// <summary>
        /// Clear all items.
        /// </summary>
        public void Clear()
        {
            this.m_Items.Clear();
            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Move(int oldIndex, int newIndex)
        {
            using (var block = new BatchUndoRedoBlock(this))
            {
                MapItemTunable item = this[oldIndex];
                this.Items.RemoveAt(oldIndex);
                this.Items.Insert(newIndex, item);
            }
        }

        #endregion // Collection Methods

        #region Override
        /// <summary>
        /// 
        /// </summary>
        public bool HasSameValue(ITunable tunable)
        {
            if (tunable is MapTunable)
            {
                if (this.Length != (tunable as MapTunable).Length)
                    return false;

                for (int i = 0; i < this.Length; i++)
                {
                    // Try and find the item with the same key in the other tunable
                    MapTunable mapTunable = tunable as MapTunable;
                    MapItemTunable mapItemTunable = mapTunable.Items.FirstOrDefault(item => item.Key == this.m_Items[i].Key);

                    if (mapItemTunable == null)
                        return false;

                    if (!this.m_Items[i].HasSameValue(mapItemTunable))
                        return false;
                }

                return true;
            }
            return false;
        }
        #endregion // Override

        #region Classes
        /// <summary>
        /// A comparer that compares two string where the numbers are treated numerical.
        /// </summary>
        public class MapKeyComparer : IComparer<string>
        {
            [DllImport("shlwapi.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
            public static extern int StrCmpLogicalW(string x, string y);

            public int Compare(string x, string y)
            {
                return StrCmpLogicalW(x, y);
            }
        }
        #endregion
    } // MapTunable
} // RSG.Metadata.Data
