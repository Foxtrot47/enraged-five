﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Parser;
using RSG.Base.Editor;

namespace RSG.Metadata.Data
{
    #region Base Integer Tunable Class

    public interface IntegerTunable : ITunable
    { }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class IntegerTunableBase<T> :
        TunableBase,
        ITunableTyped<T>,
        ITunableRange<T>,
        ITunableSerialisable,
        ITunableScalar<T>,
        IntegerTunable
    {
        #region Fields

        private T m_value;
        
        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable integer value.
        /// </summary>
        [Category("Integer"),
        Description("Tunable integer value."), RSG.Base.Editor.Command.UndoableProperty()]
        public T Value
        {
            get { return m_value; }
            set
            {
                SetPropertyValue(value, m_value, () => this.Value,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_value = (T)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable integer minimum.
        /// </summary>
        [Category("Range"),
        Description("Tunable integer minimum.")]
        public T Minimum
        {
            get
            {
                Debug.Assert(this.Definition is IMemberRange<T>,
                    "Invalid member range.");
                return (this.Definition as IMemberRange<T>).Minimum;
            }
        }

        /// <summary>
        /// Tunable integer maximum.
        /// </summary>
        [Category("Range"),
        Description("Tunable integer maximum.")]
        public T Maximum
        {
            get
            {
                Debug.Assert(this.Definition is IMemberRange<T>,
                    "Invalid member range.");
                return (this.Definition as IMemberRange<T>).Maximum;
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public IntegerTunableBase(IMember def, ITunable parent)
            : base(def, parent)
        {
            this.Reset();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public IntegerTunableBase(IMember def, ITunable parent, XmlReader reader)
            : base(def, parent)
        {
            this.Reset();
            this.ParentModel = parent;
            Deserialise(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public IntegerTunableBase(IMember def, ITunable parent, String s)
            : base(def, parent)
        {
            this.Reset();
            this.Value = Parse(s);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public void Reset()
        {
            this.Value = (this.Definition as IMemberInitialValue<T>).Init;
        }

        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader)
        {
            String value = reader.GetAttribute("value");
            if (!String.IsNullOrEmpty(value) && !String.IsNullOrWhiteSpace(value))
            {
                this.Value = Parse(value);
            }
            if (reader.IsEmptyElement)
            {
                reader.ReadStartElement();
            }
            else
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        public abstract bool Validate(T newValue);

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public new XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = base.Serialise(document, root);
            SetXmlAttribute(document, xmlElem, "value", Value.ToString());
            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public virtual T Parse(String s)
        {
            return default(T);;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            return (String.Format("{0}", this.Value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            writer.WriteAttributeString("value", Value.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            return false;
        }

        #endregion // Controller Methods
    } // IntegerTunableBase

    #endregion // Base Integer Tunable Class

    #region Signed Integer Tunable Classes

    #region 8 Bit Tunable Integers

    #region Base

    /// <summary>
    /// 
    /// </summary>
    public class Signed8TunableBase : IntegerTunableBase<sbyte>
    {       
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public Signed8TunableBase(IMember def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public Signed8TunableBase(IMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public Signed8TunableBase(IMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public override bool Validate(sbyte newValue)
        {
            return (ValidateTunableRange<sbyte>(newValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override sbyte Parse(String s)
        {
            sbyte value = (sbyte)Util.Parser.ParseString(s);
            return value;
        }

        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is Signed8TunableBase)
            {
                this.Value = (sourceTunable as Signed8TunableBase).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is Signed8TunableBase)
            {
                if (this.Value.Equals((tunable as Signed8TunableBase).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override
    }

    #endregion // Base

    #region S8Tunable

    /// <summary>
    /// 8-bit signed integer tunable.
    /// </summary>
    public class S8Tunable : Signed8TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public S8Tunable(S8Member def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public S8Tunable(S8Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public S8Tunable(S8Member def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // S8Tunable

    #endregion // S8Tunable

    #region CharTunable

    /// <summary>
    /// 8-bit signed integer tunable.
    /// </summary>
    public class CharTunable : Signed8TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public CharTunable(CharMember def, ITunable parent)
            : base(def, parent)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public CharTunable(CharMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public CharTunable(CharMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // CharTunable

    #endregion // S8Tunable

    #endregion // 8 Bit Tunable Integers

    #region 16 Bit Tunable Integers

    #region Base

    /// <summary>
    /// 
    /// </summary>
    public class Signed16TunableBase : IntegerTunableBase<Int16>
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public Signed16TunableBase(IMember def, ITunable parent)
            : base(def, parent)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public Signed16TunableBase(IMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public Signed16TunableBase(IMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public override bool Validate(Int16 newValue)
        {
            return (ValidateTunableRange<Int16>(newValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override Int16 Parse(String s)
        {
            Int16 value = (Int16)Util.Parser.ParseString(s);
            return value;
        }

        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is Signed16TunableBase)
            {
                this.Value = (sourceTunable as Signed16TunableBase).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is Signed16TunableBase)
            {
                if (this.Value.Equals((tunable as Signed16TunableBase).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override
    }

    #endregion // Base

    #region S16 Tunable

    /// <summary>
    /// 16-bit signed integer tunable.
    /// </summary>
    public class S16Tunable : Signed16TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public S16Tunable(S16Member def, ITunable parent)
            : base(def, parent)
        {
        }
            
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public S16Tunable(S16Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public S16Tunable(S16Member def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // S16Tunable

    #endregion // S16 Tunable

    #region Short Tunable

    /// <summary>
    /// 16-bit signed integer tunable.
    /// </summary>
    public class ShortTunable : Signed16TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public ShortTunable(ShortMember def, ITunable parent)
            : base(def, parent)
        {
        }
            
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public ShortTunable(ShortMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public ShortTunable(ShortMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // ShortTunable

    #endregion // Short Tunable

    #endregion // 16 Bit Tunable Integers

    #region 32 Bit Tunable Integers

    #region Base

    /// <summary>
    /// 
    /// </summary>
    public class Signed32TunableBase : IntegerTunableBase<Int32>
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public Signed32TunableBase(IMember def, ITunable parent)
            : base(def, parent)
        {
        }
                
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public Signed32TunableBase(IMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public Signed32TunableBase(IMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public override bool Validate(Int32 newValue)
        {
            return (ValidateTunableRange<Int32>(newValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override Int32 Parse(String s)
        {
            Int32 value = (Int32)Util.Parser.ParseString(s);
            return value;
        }

        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is Signed32TunableBase)
            {
                this.Value = (sourceTunable as Signed32TunableBase).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is Signed32TunableBase)
            {
                if (this.Value.Equals((tunable as Signed32TunableBase).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override
    }

    #endregion // Base

    #region S32 Tunable

    /// <summary>
    /// 32-bit signed integer tunable.
    /// </summary>
    public class S32Tunable : Signed32TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public S32Tunable(S32Member def, ITunable parent)
            : base(def, parent)
        {
        }
                    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public S32Tunable(S32Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public S32Tunable(S32Member def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // S32Tunable

    #endregion // S32 Tunable

    #region Int Tunable

    /// <summary>
    /// 32-bit signed integer tunable.
    /// </summary>
    public class IntTunable : Signed32TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public IntTunable(IntMember def, ITunable parent)
            : base(def, parent)
        {
        }
                    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public IntTunable(IntMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public IntTunable(IntMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    } // IntTunable

    #endregion // Int Tunable

    #endregion // 32 Bit Tunable Integers

    #endregion // Signed Integer Tunable Classes

    #region Unsigned Integer Tunable Classes

    #region U8 Tunable

    /// <summary>
    /// 8-bit unsigned integer tunable.
    /// </summary>
    public class U8Tunable : IntegerTunableBase<byte>
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public U8Tunable(U8Member def, ITunable parent)
            : base(def, parent)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public U8Tunable(U8Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public U8Tunable(U8Member def, ITunable parent, String s)
            : base(def, parent)
        {
            this.Value = Parse(s);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public override bool Validate(byte newValue)
        {
            return (ValidateTunableRange<byte>(newValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override byte Parse(String s)
        {
            byte value = (byte)Util.Parser.ParseString(s);
            return value;
        }

        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is U8Tunable)
            {
                this.Value = (sourceTunable as U8Tunable).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is U8Tunable)
            {
                if (this.Value.Equals((tunable as U8Tunable).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override
    } // U8Tunable

    #endregion // U8 Tunable

    #region U16 Tunable

    /// <summary>
    /// 16-bit unsigned integer tunable.
    /// </summary>
    public class U16Tunable : IntegerTunableBase<UInt16>
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public U16Tunable(U16Member def, ITunable parent)
            : base(def, parent)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public U16Tunable(U16Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public U16Tunable(U16Member def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public override bool Validate(UInt16 newValue)
        {
            return (ValidateTunableRange<UInt16>(newValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override UInt16 Parse(String s)
        {
            UInt16 value = (UInt16)Util.Parser.ParseString(s);
            return value;
        }

        #endregion // Controller Methods

        #region Override
        
        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is U16Tunable)
            {
                this.Value = (sourceTunable as U16Tunable).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is U16Tunable)
            {
                if (this.Value.Equals((tunable as U16Tunable).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override
    } // U16Tunable

    #endregion // U16 Tunable

    #region U32 Tunable

    /// <summary>
    /// 32-bit unsigned integer tunable.
    /// </summary>
    public class U32Tunable : IntegerTunableBase<UInt32>
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public U32Tunable(U32Member def, ITunable parent)
            : base(def, parent)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public U32Tunable(U32Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public U32Tunable(U32Member def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public override bool Validate(UInt32 newValue)
        {
            return (ValidateTunableRange<UInt32>(newValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public override UInt32 Parse(String s)
        {
            UInt32 value = (UInt32)Util.Parser.ParseString(s);
            return value;
        }

        #endregion // Controller Methods

        #region Override
        
        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is U32Tunable)
            {
                this.Value = (sourceTunable as U32Tunable).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is U32Tunable)
            {
                if (this.Value.Equals((tunable as U32Tunable).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override
    } // U32Tunable

    #endregion // U32 Tunable

    #endregion // Unsigned Integer Tunable Classes
} // RSG.Metadata.Data namespace
