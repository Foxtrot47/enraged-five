﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Parser;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using System.IO;
using Mvp.Xml.XInclude;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// Collection of ITunable values.
    /// </summary>
    public class StructureTunable :
        HierarchicalModelBaseDictionary<String, ITunable>, 
        ITunableStructure,
        ITunableSerialisable, INotifyPropertyChanged
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [DontRegister()]
        public IMember Definition { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        //[DontRegister()]
        //public IModel ParentModel
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// 
        /// </summary>
        public Boolean CanInheritParent
        {
            get
            {
                IModel parentModel = this.ParentModel;
                if (this.ParentModel is ArrayTunable)
                {
                    parentModel = (this.ParentModel as ArrayTunable).ParentModel;
                }

                if (parentModel is StructureTunable)
                {
                    if (((parentModel as StructureTunable).Definition as StructMember).Definition.UsesDataInheritance == true)
                    {
                        return true;
                    }
                }
                
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean? InheritParent
        {
            get;
            set;
        }

        /// <summary>
        /// The tunable that represents the key
        /// for this structure (can be null)
        /// </summary>
        [DontRegister()]
        public ITunable TunableKey
        {
            get;
            set;
        }

        private string _textData;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public StructureTunable(IModel parent)
            : base(parent)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public StructureTunable(StructMember def, IModel parent)
            : base( parent)
        {
            this.Definition = def;
            if (String.IsNullOrEmpty(def.Name))
            {
                if (!String.IsNullOrEmpty(def.FriendlyTypeName))
                {
                    this.Name = RSG.Metadata.Parser.Namespace.GetShortName(def.FriendlyTypeName);
                }
            }
            else
            {
                this.Name = def.Name;
            }
            foreach (KeyValuePair<string, IMember> kvp in def.Definition.Members)
                this.Add(kvp.Key, TunableFactory.Create(this, kvp.Value));

            if (def.Definition.MemberKey != null)
            {
                foreach (ITunable tunable in this.Values)
                {
                    if (tunable.Definition == def.Definition.MemberKey)
                    {
                        this.TunableKey = tunable;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public StructureTunable(StructMember def, IModel parent, XmlReader reader, XmlTunableFactory factory)
            : base(parent)
        {
            this.Definition = def;
            this.ParentModel = parent;

            if (String.IsNullOrEmpty(def.Name))
            {
                if (!String.IsNullOrEmpty(def.FriendlyTypeName))
                {
                    this.Name = RSG.Metadata.Parser.Namespace.GetShortName(def.FriendlyTypeName);
                }
            }
            else
            {
                this.Name = def.Name;
            }

            Deserialise(reader, factory);

            if (def.Definition.MemberKey != null)
            {
                foreach (ITunable tunable in this.Values)
                {
                    if (tunable.Definition == def.Definition.MemberKey)
                    {
                        this.TunableKey = tunable;
                        break;
                    }
                }
            }
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader, XmlTunableFactory factory)
        {
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();
            StructMember m = (this.Definition as StructMember);
            // Need to make sure that all the tunables are created in the
            // order that they appear in the structdef.

            // In the first pass just create a list of members and their tunables
            // that are present in the file (might not be all of them especially for data inheritance).
            Dictionary<String, ITunable> tunablesPresent = new Dictionary<String, ITunable>();
            if (!isEmpty)
            {
                if (reader.NodeType == XmlNodeType.Text)
                {
                    if (m.Definition.PreLoadCallback != null && m.Definition.PostSaveCallback != null)
                    {
                        this._textData = reader.Value;
                        reader.Skip();
                        if (reader.NodeType == XmlNodeType.EndElement)
                        {
                            reader.ReadEndElement();
                        }

                        return;
                    }

                    throw new XmlException(String.Format("Invalid string node type found expecting a '<' character. Should {0} be setup as a string member instead of a struct member in the psc?", m.Name), null, (reader as IXmlLineInfo).LineNumber, (reader as IXmlLineInfo).LinePosition);
                }

                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    String name = reader.Name;
                    if (!m.Definition.Members.ContainsKey(name))
                    {
                        reader.Skip();
                        continue;
                    }
                    IMember member = m.Definition.Members[name];
                    if (!tunablesPresent.ContainsKey(member.Name))
                    {
                        Data.ITunable tunable = factory.Create(this, member, reader);
                        tunablesPresent.Add(member.Name, tunable);
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }

            // Loop through all the members of this definition and either create new tunables
            // with default values or take them for the present tunables
            foreach (IMember childMember in m.Definition.Members.Values)
            {
                Data.ITunable tunable = null;
                if (tunablesPresent.ContainsKey(childMember.Name))
                {
                    tunable = tunablesPresent[childMember.Name];
                    tunable.InheritParent = false;
                }
                else
                {
                    tunable = Data.TunableFactory.Create(this, childMember);
                    if (tunable.CanInheritParent == true)
                    {
                        tunable.InheritParent = true;
                        if (tunable is ArrayTunable)
                        {
                            foreach (ITunable child in (tunable as ArrayTunable))
                            {
                                child.InheritParent = true;
                            }
                        }
                        else if (tunable is StructureTunable)
                        {
                            foreach (ITunable child in (tunable as StructureTunable).Values)
                            {
                                child.InheritParent = true;
                            }
                        }
                    }
                    else
                    {
                        tunable.InheritParent = false;
                        if (tunable is ArrayTunable)
                        {
                            foreach (ITunable child in (tunable as ArrayTunable))
                            {
                                child.InheritParent = false;
                            }
                        }
                        else if (tunable is StructureTunable)
                        {
                            foreach (ITunable child in (tunable as StructureTunable).Values)
                            {
                                child.InheritParent = false;
                            }
                        }
                    }
                }
                if (tunable != null)
                {
                    this.Add(childMember.Name, tunable);
                }
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public XmlElement Serialise(XmlDocument document, bool root)
        {
            Debug.Assert(this.Definition is StructMember,
                "StructureTunable requires StructMember definition.");
            StructMember m = (this.Definition as StructMember);

            if (m.Definition.HideElement)
                return null;

#warning DHM -- HACK -- FIXME -- handle names correctly.
            String xmlFriendlyDataType = (m.Definition.DataType.Clone() as String);
            if (root)
            {
                if (xmlFriendlyDataType.Contains(":"))
                    xmlFriendlyDataType = xmlFriendlyDataType.Replace(":", "_");
            }
            else
            {
                if (m.Name == String.Empty)
                    xmlFriendlyDataType = "Item";
                else
                xmlFriendlyDataType = (m.Name.Clone() as String);
            }

            XmlElement xmlElem = document.CreateElement(xmlFriendlyDataType);
            foreach (ITunable tunable in this.Values)
            {
                if (tunable.InheritParent == true)
                    continue;

                Debug.Assert(tunable is ITunableSerialisable);
                if (!(tunable is ITunableSerialisable))
                    continue;

                ITunableSerialisable ts = (tunable as ITunableSerialisable);
                XmlElement xmlChild = ts.Serialise(document, false);

                PointerTunable pointerTunable = tunable as PointerTunable;
                PointerMember pointerMember = tunable.Definition as PointerMember;
                if (pointerTunable != null && pointerMember != null)
                {
                    if (pointerMember.IsOwned)
                    {
                        ITunable tunableValue = pointerTunable.Value as ITunable;
                        if (tunableValue != null)
                        {
                            string typeName = tunableValue.Definition.FriendlyTypeName.Replace(':', '_');
                            xmlChild.SetAttribute("type", typeName);
                        }
                        else
                        {
                            string stringValue = pointerTunable.Value as string;
                            xmlChild.SetAttribute("type", stringValue);
                        }
                    }
                }

                xmlElem.AppendChild(xmlChild);
            }

            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String ToXmlString()
        {
            XmlDocument doc = new XmlDocument();
            XmlElement elem = Serialise(doc, true);
            return elem.OuterXml;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public void SerialiseWithoutChildren(XmlWriter writer)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetInheritParent(Boolean? newValue)
        {
            this.InheritParent = newValue;
            this.OnPropertyChanged("InheritParent");
        }
        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is StructureTunable)
            {
                Boolean same = true;
                if (this.Name == null)
                {
                    if ((tunable as StructureTunable).Name != null)
                        same = false;
                }
                else
                {
                    if ((tunable as StructureTunable).Name == null)
                        same = false;
                    else if (!this.Name.Equals((tunable as StructureTunable).Name))
                        same = false;
                }

                if (same == true)
                {
                    int index = 0;
                    foreach (KeyValuePair<String, ITunable> child in this)
                    {
                        int otherIndex = 0;
                        foreach (KeyValuePair<String, ITunable> otherChild in (tunable as StructureTunable))
                        {
                            if (otherIndex == index)
                            {
                                if (child.Key.Equals(otherChild.Key) && child.Value.HasSameValue(otherChild.Value))
                                {
                                    same = true;
                                    break;
                                }
                                else
                                {
                                    same = false;
                                }
                            }
                            otherIndex++;
                        }

                        if (same == false)
                            break;
                        index++;
                    }
                    return same;
                }
            }
            return false;
        }

        #endregion // Override

        /// <summary>
        /// 
        /// </summary>
        public virtual void SetTunableValue(ITunable sourceTunable)
        {
            if (!(sourceTunable is StructureTunable))
                return;

            StructureTunable source = sourceTunable as StructureTunable;
            if (!(source.Definition == this.Definition))
                return;

            this.Clear();
            foreach (ITunable child in source.Values)
            {
                ITunable newTunable = TunableFactory.Create(this.ParentModel as ITunable, child.Definition);
                newTunable.SetTunableValue(child);
                if (child == source.TunableKey)
                    this.TunableKey = newTunable;
                this.Add(child.Definition.Name, newTunable);
            }
            this.InheritParent = source.InheritParent;
        }

        public List<ITunable> GetAllTunablesRecursive()
        {
            List<ITunable> tunables = new List<ITunable>();
            tunables.Add(this);
            foreach (ITunable child in this.Values)
            {
                this.GetAllTunables(child, ref tunables);
            }

            return tunables;
        }

        private void GetAllTunables(ITunable root, ref List<ITunable> tunables)
        {
            if (!tunables.Contains(root))
            {
                tunables.Add(root);
            }

            if (root is StructureTunable)
            {
                foreach (ITunable child in (root as StructureTunable).Values)
                {
                    this.GetAllTunables(child, ref tunables);
                }
            }
            else if (root is ArrayTunable)
            {
                foreach (ITunable child in (root as ArrayTunable).Items)
                {
                    this.GetAllTunables(child, ref tunables);
                }
            }
            else if (root is MapTunable)
            {
                foreach (ITunable child in (root as MapTunable).Items)
                {
                    this.GetAllTunables(child, ref tunables);
                }
            }
            else if (root is PointerTunable)
            {
                if ((root as PointerTunable).Value is ITunable)
                {
                    ITunable child = (root as PointerTunable).Value as ITunable;
                    this.GetAllTunables(child, ref tunables);
                }
            }
            else if (root is MapItemTunable)
            {
                if ((root as MapItemTunable).Value is ITunable)
                {
                    ITunable child = (root as MapItemTunable).Value as ITunable;
                    this.GetAllTunables(child, ref tunables);
                }
            }
        }
    }

} // RSG.Metadata.Definition namespace
