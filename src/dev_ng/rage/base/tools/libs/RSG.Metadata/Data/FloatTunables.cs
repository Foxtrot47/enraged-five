﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Parser;
using RSG.Base.Editor;


namespace RSG.Metadata.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class FloatTunable :
        TunableBase,
        ITunableTyped<float>,
        ITunableRange<float>,
        ITunableSerialisable,
        ITunableScalar<float>
    {
        #region Fields
        private const double Deg2Rad = Math.PI / 180.0;
        private const double Rad2Deg = 180.0 / Math.PI;
        private float m_value;
        #endregion // Fields

        #region Properties and Associated Member Data

        /// <summary>
        /// Tunable float value.
        /// </summary>
        [Category("Tunable"),
        Description("Tunable float value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float Value
        {
            get { return m_value; }
            set
            {
                SetPropertyValue(value, m_value, () => this.Value,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_value = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable float minimum.
        /// </summary>
        [Category("Range"),
        Description("Tunable float minimum.")]
        public float Minimum
        {
            get
            {
                return (this.Definition as FloatMember).Minimum;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Range"),
        Description("Tunable float maximum.")]
        public float Maximum
        {
            get
            {
                return (this.Definition as FloatMember).Maximum;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public bool IsDegreesToRadian
        {
            get
            {
                return (this.Definition as FloatMember).IsDegreesToRadian;
            }
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public FloatTunable(FloatMember def, IModel parent)
            : base(def, parent)
        {
            this.Value = def.Init;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public FloatTunable(FloatMember def, IModel parent, XmlReader reader)
            : base(def, parent)
        {
            this.Value = def.Init;
            Deserialise(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public FloatTunable(FloatMember def, IModel parent, String s)
            : base(def, parent)
        {
            this.Value = def.Init;
            this.Value = Parse(s);
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader)
        {
            String stringValue = reader.GetAttribute("value");
            if (!String.IsNullOrEmpty(stringValue) && !String.IsNullOrWhiteSpace(stringValue))
            {
                if ((this.Definition as FloatMember).DecimalPlaces >= 0)
                {
                    float value = Parse(stringValue);
                    String formatString = "{0:N" + (this.Definition as FloatMember).DecimalPlaces + "}";
                    String valueString = String.Format(formatString, value);
                    this.Value = Parse(valueString);
                }
                else
                {
                    this.Value = Parse(stringValue);
                }
            }
            if (reader.IsEmptyElement)
            {
                reader.ReadStartElement();
            }
            else
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                    this.Value = Parse(reader.ReadString());
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }

            if (this.IsDegreesToRadian)
            {
                this.Value = (float)(this.Value * Rad2Deg);
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public new XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = base.Serialise(document, root);
            String stringValue = String.Empty;
            float actualValue = this.Value;
            if (this.IsDegreesToRadian)
            {
                actualValue = (float)(this.Value * Deg2Rad);
            }

            if ((this.Definition as FloatMember).HighPrecision == true)
            {
                stringValue = String.Format("{0:0.000000000}", actualValue);
            }
            else
            {
                stringValue = String.Format("{0:0.000000}", actualValue);
            }

            SetXmlAttribute(document, xmlElem, "value", stringValue);
            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public bool Validate(float newValue)
        {
            return (ValidateTunableRange<float>(newValue));
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public float Parse(String s)
        {
            double value = Util.Parser.ParseString(s);
            return (float)value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            // As recommended by Russ; we currently ignore the high/low
            // precision flag in FloatMember and serialise based on the
            // number of decimal places the .Net runtime reckons is right.
            return (String.Format("{0:r}", this.Value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            String stringValue = String.Empty;
            float actualValue = this.Value;
            if (this.IsDegreesToRadian)
            {
                actualValue = (float)(this.Value * Deg2Rad);
            }

            if ((this.Definition as FloatMember).HighPrecision == true)
            {
                stringValue = String.Format("{0:0.000000000}", actualValue);
            }
            else
            {
                stringValue = String.Format("{0:0.000000}", actualValue);
            }

            writer.WriteAttributeString("value", stringValue);
        }

        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is FloatTunable)
            {
                this.Value = (sourceTunable as FloatTunable).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is FloatTunable)
            {
                if (this.Value.Equals((tunable as FloatTunable).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override
    } // FloatTunable
} // RSG.Metadata.Data namespace
