﻿using System.Xml;
using RSG.Base.Editor;
using RSG.Metadata.Parser;
using System.IO;
using System.Collections.Generic;
using Mvp.Xml.XInclude;
using System;
using RSG.Metadata.Model;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// </summary>
    public class XIncludeTunable : TunableBase
    {
        #region Fields
        /// <summary>
        /// Represents the xml attribute name for the <see cref="filename"/> field.
        /// </summary>
        private const string XmlHref = "href";

        /// <summary>
        /// Represents the xml attribute name for the <see cref="xpointer"/> field.
        /// </summary>
        private const string XmlXPointer = "xpointer";

        /// <summary>
        /// The filename to the external file that the values for this come from.
        /// </summary>
        private string filename;

        /// <summary>
        /// The xpath to the member in the external file that starts the data.
        /// </summary>
        private string xpointer;

        /// <summary>
        /// The base uri that the x-include element is pointing to.
        /// </summary>
        private string href;

        /// <summary>
        /// The private field used for the <see cref="Data"/> property.
        /// </summary>
        private ITunable data;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="XIncludeTunable"/> class.
        /// </summary>
        /// <param name="member">
        /// The member that the external file and the xpath should be pointing at.
        /// </param>
        /// <param name="parent">
        /// The model parent for this tunable, used for undo/redo.
        /// </param>
        /// <param name="reader">
        /// The System.Xml.XmlTextReader used as a data provider.
        /// </param>
        public XIncludeTunable(IMember member, IModel parent, XmlReader reader, XmlTunableFactory factory, string href, string xpointer)
            : base(member, parent)
        {
            this.filename = new Uri(reader.BaseURI).AbsolutePath;
            this.Deserialise(reader, member, factory);
            this.href = href;
            this.xpointer = xpointer;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the tunable that represents the data defined in the external
        /// file that this xi include is pointing to.
        /// </summary>
        public ITunable Data
        {
            get { return this.data; }
            set { this.data = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Filename
        {
            get { return this.filename; }
        }

        /// <summary>
        /// 
        /// </summary>
        public new string Name
        {
            get { return Path.GetFileName(this.Filename); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public override XmlElement Serialise(XmlDocument document, bool root)
        {
            try
            {
                XmlDocument internalDocument = null;
                this.SaveInternalDocument(out internalDocument);
                string folder = System.IO.Path.GetDirectoryName(filename);
                System.IO.Directory.CreateDirectory(folder);
                internalDocument.Save(filename);
            }
            catch (Exception ex)
            {
                string message = string.Format("Exception serialising metadata to file '{0}'.  See InnerException.",
                    this.filename);
                throw new ParserException(message, ex);
            }


            XmlElement xmlElem = document.CreateElement("xi:include", "http://www.w3.org/2001/XInclude");
            SetXmlAttribute(document, xmlElem, XmlHref, this.href);
            SetXmlAttribute(document, xmlElem, XmlXPointer, this.xpointer);
            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            writer.WriteAttributeString(XmlHref, this.href);
            writer.WriteAttributeString(XmlXPointer, this.xpointer);
        }

        /// <summary>
        /// Uses the specified data provider to initialise this instance and altermately
        /// get the path to the external file to load that.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlTextReader used as a data provider.
        /// </param>
        /// <param name="parent">
        /// The model parent for this tunable, used for undo/redo.
        /// </param>
        /// <param name="member">
        /// The member that the external file and the xpath should be pointing at.
        /// </param>
        private void Deserialise(XmlReader reader, IMember member, XmlTunableFactory factory)
        {
            this.Data = factory.Create(this, member, reader);
        }

        private void SaveInternalDocument(out XmlDocument document)
        {
            bool containsIncludes = false;
            foreach (ITunable member in GetAllMembers(true))
            {
                if (member is XIncludeTunable)
                {
                    containsIncludes = true;
                    break;
                }
            }

            document = new XmlDocument();

            // XML Declaration
            XmlDeclaration xmlDecl = document.CreateXmlDeclaration(
                MetaFile.DEFAULT_XML_VERSION, "UTF-8", null);
            document.AppendChild(xmlDecl);

            // Document Root
            XmlElement xmlRoot = (this.data as ITunableSerialisable).Serialise(document, false); 
            if (containsIncludes)
            {
                xmlRoot.SetAttribute("xmlns:xi", "http://www.w3.org/2001/XInclude");
            }

            document.AppendChild(xmlRoot);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recursive"></param>
        private IEnumerable<ITunable> GetAllMembers(Boolean recursive)
        {
            List<ITunable> tunables = new List<ITunable>();
            GetAllMembers(this.data, recursive, ref tunables);
            foreach (ITunable tunable in tunables)
            {
                yield return tunable;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootTunable"></param>
        /// <param name="recursive"></param>
        /// <param name="tunables"></param>
        private void GetAllMembers(ITunable rootTunable, Boolean recursive, ref List<ITunable> tunables)
        {
            tunables.Add(rootTunable);
            if (recursive == false)
                return;

            if (rootTunable is StructureTunable)
            {
                foreach (ITunable tunable in (rootTunable as StructureTunable).Values)
                {
                    GetAllMembers(tunable, true, ref tunables);
                }
            }
            else if (rootTunable is PointerTunable)
            {
                if ((rootTunable as PointerTunable).Value is ITunable)
                    GetAllMembers((rootTunable as PointerTunable).Value as ITunable, true, ref tunables);
            }
            else if (rootTunable is ArrayTunable)
            {
                foreach (ITunable tunable in (rootTunable as ArrayTunable).Items)
                {
                    GetAllMembers(tunable, true, ref tunables);
                }
            }
            else if (rootTunable is MapTunable)
            {
                foreach (ITunable tunable in (rootTunable as MapTunable).Items)
                {
                    GetAllMembers(tunable, true, ref tunables);
                }
            }
            else if (rootTunable is MapItemTunable)
            {
                GetAllMembers((rootTunable as MapItemTunable).Value, true, ref tunables);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            return "";
        }
        #endregion
    } // XIncludeTunable
} // RSG.Metadata.Data namespace