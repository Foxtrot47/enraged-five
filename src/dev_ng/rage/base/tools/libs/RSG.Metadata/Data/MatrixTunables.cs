﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Math;
using RSG.Metadata.Parser;

namespace RSG.Metadata.Data
{
    #region BaseDataType Matrix Tunable Class

    /// <summary>
    /// 
    /// </summary>
    public abstract class MatrixTunableBase<T> :
        TunableBase,
        ITunableSerialisable
    {
        #region Fields

        private float m_xx;
        private float m_xy;
        private float m_xz;
        private float m_yx;
        private float m_yy;
        private float m_yz;
        private float m_zx;
        private float m_zy;
        private float m_zz;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component XX value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float XX
        {
            get { return m_xx; }
            set
            {
                SetPropertyValue(value, m_xx, () => this.XX,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_xx = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component XY value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float XY
        {
            get { return m_xy; }
            set
            {
                SetPropertyValue(value, m_xy, () => this.XY,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_xy = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component XZ value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float XZ
        {
            get { return m_xz; }
            set
            {
                SetPropertyValue(value, m_xz, () => this.XZ,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_xz = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component YX value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float YX
        {
            get { return m_yx; }
            set
            {
                SetPropertyValue(value, m_yx, () => this.YX,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_yx = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component YY value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float YY
        {
            get { return m_yy; }
            set
            {
                SetPropertyValue(value, m_yy, () => this.YY,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_yy = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component YZ value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float YZ
        {
            get { return m_yz; }
            set
            {
                SetPropertyValue(value, m_yz, () => this.YZ,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_yz = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component ZX value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float ZX
        {
            get { return m_zx; }
            set
            {
                SetPropertyValue(value, m_zx, () => this.ZX,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_zx = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component ZY value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float ZY
        {
            get { return m_zy; }
            set
            {
                SetPropertyValue(value, m_zy, () => this.ZY,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_zy = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component ZZ value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float ZZ
        {
            get { return m_zz; }
            set
            {
                SetPropertyValue(value, m_zz, () => this.ZZ,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_zz = (float)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public MatrixTunableBase(IMember def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public MatrixTunableBase(IMember def, ITunable parent, XmlReader reader)
            : base(def, parent)
        {
            this.ParentModel = parent;
            Deserialise(reader);
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Deserialise the tunable from the specified XmlReader.
        /// </summary>
        /// <param name="node"></param>
        public virtual void Deserialise(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public override XmlElement Serialise(XmlDocument document, bool root)
        {
            throw new NotImplementedException();
        }

        public override Boolean HasSameValue(ITunable tunable)
        {
            return false;
        }

        #endregion // Controller Methods
    }

    #endregion // BaseDataType Vector Tunable Class

    #region 3x3-Component Matrix Tunable Class

    #region Base

    /// <summary>
    /// 3x3-float component vector tunable class.
    /// </summary>
    public class Matrix33TunableBase :
        MatrixTunableBase<float>
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Matrix33TunableBase(Matrix33MemberBase def, ITunable parent)
            : base(def, parent)
        {
            this.Reset(def);
        }

        /// <summary>
        /// 
        /// </summary>
        public Matrix33TunableBase(Matrix33MemberBase def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Matrix33TunableBase(Matrix33MemberBase def, ITunable parent, String s)
            : base(def, parent)
        {
            this.Reset(def);
            Parse(s);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public void Parse(String s)
        {
            String[] elements = s.Split(new String[] { " ", "\t", "\r", "\r\n", "\n" },
                    StringSplitOptions.RemoveEmptyEntries);

            Debug.Assert(elements.Length == 9, String.Format("Unable to parse Matrix33TunableBase tunable with {0} parts for string {1}", elements.Length, s));
            this.XX = (float)Util.Parser.ParseString(elements[0]);
            this.XY = (float)Util.Parser.ParseString(elements[1]);
            this.XZ = (float)Util.Parser.ParseString(elements[2]);
            this.YX = (float)Util.Parser.ParseString(elements[3]);
            this.YY = (float)Util.Parser.ParseString(elements[4]);
            this.YZ = (float)Util.Parser.ParseString(elements[5]);
            this.ZX = (float)Util.Parser.ParseString(elements[6]);
            this.ZY = (float)Util.Parser.ParseString(elements[7]);
            this.ZZ = (float)Util.Parser.ParseString(elements[8]);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Reset(Matrix33MemberBase def)
        {
            this.XX = def.Init[0];
            this.XY = def.Init[1];
            this.XZ = def.Init[2];
            this.YX = def.Init[3];
            this.YY = def.Init[4];
            this.YZ = def.Init[5];
            this.ZX = def.Init[6];
            this.ZY = def.Init[7];
            this.ZZ = def.Init[8];
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public override XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement(this.Definition.Name);
            SetXmlAttribute(document, xmlElem, "content", "matrix34");
            String matrixString = String.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                this.XX, this.XY, this.XZ,
                this.YX, this.YY, this.YZ,
                this.ZX, this.ZY, this.ZZ);
            SetXmlText(document, xmlElem, matrixString);

            return (xmlElem);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToXmlString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8}", this.XX, this.XY, this.XZ,
                                                                        this.YX, this.YY, this.YZ,
                                                                        this.ZX, this.ZY, this.ZZ);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            writer.WriteAttributeString("content", "matrix34");
            String matrixString = String.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8}",
                this.XX, this.XY, this.XZ,
                this.YX, this.YY, this.YZ,
                this.ZX, this.ZY, this.ZZ);
            writer.WriteString(matrixString);
        }

        #endregion // Controller Methods

        #region Override
        
        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is Matrix33TunableBase)
            {
                this.XX = (sourceTunable as Matrix33TunableBase).XX;
                this.XY = (sourceTunable as Matrix33TunableBase).XY;
                this.XZ = (sourceTunable as Matrix33TunableBase).XZ;
                this.YX = (sourceTunable as Matrix33TunableBase).YX;
                this.YY = (sourceTunable as Matrix33TunableBase).YY;
                this.YZ = (sourceTunable as Matrix33TunableBase).YZ;
                this.ZX = (sourceTunable as Matrix33TunableBase).ZX;
                this.ZY = (sourceTunable as Matrix33TunableBase).ZY;
                this.ZZ = (sourceTunable as Matrix33TunableBase).ZZ;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is Matrix33TunableBase)
            {
                if (this.XX.Equals((tunable as Matrix33TunableBase).XX) &&
                    this.XY.Equals((tunable as Matrix33TunableBase).XY) &&
                    this.XZ.Equals((tunable as Matrix33TunableBase).XZ) &&
                    this.YX.Equals((tunable as Matrix33TunableBase).YX) &&
                    this.YY.Equals((tunable as Matrix33TunableBase).YY) &&
                    this.YZ.Equals((tunable as Matrix33TunableBase).YZ) &&
                    this.ZX.Equals((tunable as Matrix33TunableBase).ZX) &&
                    this.ZY.Equals((tunable as Matrix33TunableBase).ZY) &&
                    this.ZZ.Equals((tunable as Matrix33TunableBase).ZZ))
                    return true;
            }
            return false;
        }

        #endregion // Override
    }

    #endregion // Base

    #region Mat33V Tunable

    /// <summary>
    /// 
    /// </summary>
    public class Mat33VTunable : Matrix33TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Mat33VTunable(Mat33VMember def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Mat33VTunable(Mat33VMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Mat33VTunable(Mat33VMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    }

    #endregion // Mat34V Tunable

    #endregion // 3x3-Component Matrix Tunable Class

    #region 3x4-Component Matrix Tunable Class

    #region Base

    /// <summary>
    /// 3x4-float component vector tunable class.
    /// </summary>
    public class Matrix34TunableBase :
        MatrixTunableBase<float>
    {
        #region Fields

        private float m_xw;
        private float m_yw;
        private float m_zw;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component XW value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float XW
        {
            get { return m_xw; }
            set
            {
                SetPropertyValue(value, m_xw, () => this.XW,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_xw = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component YW value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float YW
        {
            get { return m_yw; }
            set
            {
                SetPropertyValue(value, m_yw, () => this.YW,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_yw = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component ZW value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float ZW
        {
            get { return m_zw; }
            set
            {
                SetPropertyValue(value, m_zw, () => this.ZW,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_zw = (float)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Matrix34TunableBase(Matrix34MemberBase def, ITunable parent)
            : base(def, parent)
        {
            Reset(def);
        }

        /// <summary>
        /// 
        /// </summary>
        public Matrix34TunableBase(Matrix34MemberBase def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        public Matrix34TunableBase(Matrix34MemberBase def, ITunable parent, String s)
            : base(def, parent)
        {
            this.Reset(def);
            Parse(s);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public void Parse(String s)
        {
            String[] elements = s.Split(new String[] { " ", "\t", "\r", "\r\n", "\n" },
                    StringSplitOptions.RemoveEmptyEntries);

            Debug.Assert(elements.Length == 12, String.Format("Unable to parse Matrix34TunableBase tunable with {0} parts for string {1}", elements.Length, s));
            this.XX = (float)Util.Parser.ParseString(elements[0]);
            this.XY = (float)Util.Parser.ParseString(elements[1]);
            this.XZ = (float)Util.Parser.ParseString(elements[2]);
            this.XW = (float)Util.Parser.ParseString(elements[3]);
            this.YX = (float)Util.Parser.ParseString(elements[4]);
            this.YY = (float)Util.Parser.ParseString(elements[5]);
            this.YZ = (float)Util.Parser.ParseString(elements[6]);
            this.YW = (float)Util.Parser.ParseString(elements[7]);
            this.ZX = (float)Util.Parser.ParseString(elements[8]);
            this.ZY = (float)Util.Parser.ParseString(elements[9]);
            this.ZZ = (float)Util.Parser.ParseString(elements[10]);
            this.ZW = (float)Util.Parser.ParseString(elements[11]);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Reset(Matrix34MemberBase def)
        {
            this.XX = def.Init[0];
            this.XY = def.Init[1];
            this.XZ = def.Init[2];
            this.XW = def.Init[3];
            this.YX = def.Init[4];
            this.YY = def.Init[5];
            this.YZ = def.Init[6];
            this.YW = def.Init[7];
            this.ZX = def.Init[8];
            this.ZY = def.Init[9];
            this.ZZ = def.Init[10];
            this.ZW = def.Init[11];
        }

        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                {
                    String value = reader.ReadString();
                    if (!String.IsNullOrWhiteSpace(value) && !String.IsNullOrEmpty(value))
                    {
                        String[] parts = value.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        Debug.Assert(parts.Length == 12, "Unable to parse matrix34 tunable as the number of parts doesn't equal 12");
                        if (parts.Length != 12)
                            return;

                        this.XX = (float)Util.Parser.ParseString(parts[0]);
                        this.XY = (float)Util.Parser.ParseString(parts[1]);
                        this.XZ = (float)Util.Parser.ParseString(parts[2]);
                        this.XW = (float)Util.Parser.ParseString(parts[3]);
                        this.YX = (float)Util.Parser.ParseString(parts[4]);
                        this.YY = (float)Util.Parser.ParseString(parts[5]);
                        this.YZ = (float)Util.Parser.ParseString(parts[6]);
                        this.YW = (float)Util.Parser.ParseString(parts[7]);
                        this.ZX = (float)Util.Parser.ParseString(parts[8]);
                        this.ZY = (float)Util.Parser.ParseString(parts[9]);
                        this.ZZ = (float)Util.Parser.ParseString(parts[10]);
                        this.ZW = (float)Util.Parser.ParseString(parts[11]);
                    }
                }
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public override XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement(this.Definition.Name);
            SetXmlAttribute(document, xmlElem, "content", "matrix34");
            String matrixString = String.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11}",
                this.XX, this.XY, this.XZ, this.XW,
                this.YX, this.YY, this.YZ, this.YW,
                this.ZX, this.ZY, this.ZZ, this.ZW);
            SetXmlText(document, xmlElem, matrixString);

            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11}", this.XX, this.XY, this.XZ, this.XW,
                                                                                      this.YX, this.YY, this.YZ, this.YW,
                                                                                      this.ZX, this.ZY, this.ZZ, this.ZW);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            writer.WriteAttributeString("content", "matrix34");
            String matrixString = String.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11}",
                this.XX, this.XY, this.XZ, this.XW,
                this.YX, this.YY, this.YZ, this.YW,
                this.ZX, this.ZY, this.ZZ, this.ZW);
            writer.WriteString(matrixString);
        }

        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is Matrix34TunableBase)
            {
                this.XX = (sourceTunable as Matrix34TunableBase).XX;
                this.XY = (sourceTunable as Matrix34TunableBase).XY;
                this.XZ = (sourceTunable as Matrix34TunableBase).XZ;
                this.XW = (sourceTunable as Matrix34TunableBase).XW;
                this.YX = (sourceTunable as Matrix34TunableBase).YX;
                this.YY = (sourceTunable as Matrix34TunableBase).YY;
                this.YZ = (sourceTunable as Matrix34TunableBase).YZ;
                this.YW = (sourceTunable as Matrix34TunableBase).YW;
                this.ZX = (sourceTunable as Matrix34TunableBase).ZX;
                this.ZY = (sourceTunable as Matrix34TunableBase).ZY;
                this.ZZ = (sourceTunable as Matrix34TunableBase).ZZ;
                this.ZW = (sourceTunable as Matrix34TunableBase).ZW;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is Matrix34TunableBase)
            {
                if (this.XX.Equals((tunable as Matrix34TunableBase).XX) &&
                    this.XY.Equals((tunable as Matrix34TunableBase).XY) &&
                    this.XZ.Equals((tunable as Matrix34TunableBase).XZ) &&
                    this.XW.Equals((tunable as Matrix34TunableBase).XW) &&
                    this.YX.Equals((tunable as Matrix34TunableBase).YX) &&
                    this.YY.Equals((tunable as Matrix34TunableBase).YY) &&
                    this.YZ.Equals((tunable as Matrix34TunableBase).YZ) &&
                    this.YW.Equals((tunable as Matrix34TunableBase).YW) &&
                    this.ZX.Equals((tunable as Matrix34TunableBase).ZX) &&
                    this.ZY.Equals((tunable as Matrix34TunableBase).ZY) &&
                    this.ZZ.Equals((tunable as Matrix34TunableBase).ZZ) &&
                    this.ZW.Equals((tunable as Matrix34TunableBase).ZW))
                    return true;
            }
            return false;
        }
        #endregion // Override
    }

    #endregion // Base

    #region Matrix34 Tunable

    /// <summary>
    /// 
    /// </summary>
    public class Matrix34Tunable : Matrix34TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Matrix34Tunable(Matrix34Member def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Matrix34Tunable(Matrix34Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Matrix34Tunable(Matrix34Member def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    }

    #endregion // Matrix34 Tunable

    #region Mat34V Tunable

    /// <summary>
    /// 
    /// </summary>
    public class Mat34VTunable : Matrix34TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Mat34VTunable(Mat34VMember def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Mat34VTunable(Mat34VMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Mat34VTunable(Mat34VMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    }

    #endregion // Mat34V Tunable

    #endregion // 3x4-Component Matrix Tunable Class

    #region 4x4-Component Matrix Tunable Class

    #region Base

    /// <summary>
    /// 4x4-float component vector tunable class.
    /// </summary>
    public class Matrix44TunableBase :
        MatrixTunableBase<float>
    {
        #region Fields

        private float m_xw;
        private float m_yw;
        private float m_zw;
        private float m_wx;
        private float m_wy;
        private float m_wz;
        private float m_ww;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component XW value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float XW
        {
            get { return m_xw; }
            set
            {
                SetPropertyValue(value, m_xw, () => this.XW,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_xw = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component YW value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float YW
        {
            get { return m_yw; }
            set
            {
                SetPropertyValue(value, m_yw, () => this.YW,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_yw = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component ZW value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float ZW
        {
            get { return m_zw; }
            set
            {
                SetPropertyValue(value, m_zw, () => this.ZW,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_zw = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component WX value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float WX
        {
            get { return m_wx; }
            set
            {
                SetPropertyValue(value, m_wx, () => this.WX,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_wx = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component WY value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float WY
        {
            get { return m_wy; }
            set
            {
                SetPropertyValue(value, m_wy, () => this.WY,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_wy = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component WZ value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float WZ
        {
            get { return m_wz; }
            set
            {
                SetPropertyValue(value, m_wz, () => this.WZ,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_wz = (float)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// Tunable vector value.
        /// </summary>
        [Category("Matrix"),
        Description("Tunable matrix component WW value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public float WW
        {
            get { return m_ww; }
            set
            {
                SetPropertyValue(value, m_ww, () => this.WW,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_ww = (float)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Matrix44TunableBase(Matrix44MemberBase def, ITunable parent)
            : base(def, parent)
        {
            Reset(def);
        }

        /// <summary>
        /// 
        /// </summary>
        public Matrix44TunableBase(Matrix44MemberBase def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public Matrix44TunableBase(Matrix44MemberBase def, ITunable parent, String s)
            : base(def, parent)
        {
            Reset(def);
            Parse(s);
        }

        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public void Parse(String s)
        {
            String[] elements = s.Split(new String[] { " ", "\t", "\r", "\r\n", "\n" },
                    StringSplitOptions.RemoveEmptyEntries);

            Debug.Assert(elements.Length == 16, String.Format("Unable to parse Matrix44TunableBase tunable with {0} parts for string {1}", elements.Length, s));
            this.XX = (float)Util.Parser.ParseString(elements[0]);
            this.XY = (float)Util.Parser.ParseString(elements[1]);
            this.XZ = (float)Util.Parser.ParseString(elements[2]);
            this.XW = (float)Util.Parser.ParseString(elements[3]);
            this.YX = (float)Util.Parser.ParseString(elements[4]);
            this.YY = (float)Util.Parser.ParseString(elements[5]);
            this.YZ = (float)Util.Parser.ParseString(elements[6]);
            this.YW = (float)Util.Parser.ParseString(elements[7]);
            this.ZX = (float)Util.Parser.ParseString(elements[8]);
            this.ZY = (float)Util.Parser.ParseString(elements[9]);
            this.ZZ = (float)Util.Parser.ParseString(elements[10]);
            this.ZW = (float)Util.Parser.ParseString(elements[11]);
            this.WX = (float)Util.Parser.ParseString(elements[12]);
            this.WY = (float)Util.Parser.ParseString(elements[13]);
            this.WZ = (float)Util.Parser.ParseString(elements[14]);
            this.WW = (float)Util.Parser.ParseString(elements[15]);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Reset(Matrix44MemberBase def)
        {
            this.XX = def.Init[0];
            this.XY = def.Init[1];
            this.XZ = def.Init[2];
            this.XW = def.Init[3];
            this.YX = def.Init[4];
            this.YY = def.Init[5];
            this.YZ = def.Init[6];
            this.YW = def.Init[7];
            this.ZX = def.Init[8];
            this.ZY = def.Init[9];
            this.ZZ = def.Init[10];
            this.ZW = def.Init[11];
            this.WX = def.Init[12];
            this.WY = def.Init[13];
            this.WZ = def.Init[14];
            this.WW = def.Init[15]; 
        }

        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public override void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                {
                    String value = reader.ReadString();
                    if (!String.IsNullOrWhiteSpace(value) && !String.IsNullOrEmpty(value))
                    {
                        this.Parse(value);
                    }
                }
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }
        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public override XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement(this.Definition.Name);
            SetXmlAttribute(document, xmlElem, "content", "matrix34");
            String matrixString = String.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14} {15}",
                this.XX, this.XY, this.XZ, this.XW,
                this.YX, this.YY, this.YZ, this.YW,
                this.ZX, this.ZY, this.ZZ, this.ZW,
                this.WX, this.WY, this.WZ, this.WW);
            SetXmlText(document, xmlElem, matrixString);

            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public bool Validate(float newValue)
        {
            return (ValidateTunableRange<float>(newValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToXmlString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14} {15}", this.XX, this.XY, this.XZ, this.XW,
                                                                                                          this.YX, this.YY, this.YZ, this.YW,
                                                                                                          this.ZX, this.ZY, this.ZZ, this.ZW,
                                                                                                          this.WX, this.WY, this.WZ, this.WW);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            writer.WriteAttributeString("content", "matrix34");
            String matrixString = String.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14} {15}",
                this.XX, this.XY, this.XZ, this.XW,
                this.YX, this.YY, this.YZ, this.YW,
                this.ZX, this.ZY, this.ZZ, this.ZW,
                this.WX, this.WY, this.WZ, this.WW);
            writer.WriteString(matrixString);
        }

        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is Matrix44TunableBase)
            {
                this.XX = (sourceTunable as Matrix44TunableBase).XX;
                this.XY = (sourceTunable as Matrix44TunableBase).XY;
                this.XZ = (sourceTunable as Matrix44TunableBase).XZ;
                this.XW = (sourceTunable as Matrix44TunableBase).XW;
                this.YX = (sourceTunable as Matrix44TunableBase).YX;
                this.YY = (sourceTunable as Matrix44TunableBase).YY;
                this.YZ = (sourceTunable as Matrix44TunableBase).YZ;
                this.YW = (sourceTunable as Matrix44TunableBase).YW;
                this.ZX = (sourceTunable as Matrix44TunableBase).ZX;
                this.ZY = (sourceTunable as Matrix44TunableBase).ZY;
                this.ZZ = (sourceTunable as Matrix44TunableBase).ZZ;
                this.ZW = (sourceTunable as Matrix44TunableBase).ZW;
                this.WX = (sourceTunable as Matrix44TunableBase).WX;
                this.WY = (sourceTunable as Matrix44TunableBase).WY;
                this.WZ = (sourceTunable as Matrix44TunableBase).WZ;
                this.WW = (sourceTunable as Matrix44TunableBase).WW;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is Matrix44TunableBase)
            {
                if (this.XX.Equals((tunable as Matrix44TunableBase).XX) &&
                    this.XY.Equals((tunable as Matrix44TunableBase).XY) &&
                    this.XZ.Equals((tunable as Matrix44TunableBase).XZ) &&
                    this.XW.Equals((tunable as Matrix44TunableBase).XW) &&
                    this.YX.Equals((tunable as Matrix44TunableBase).YX) &&
                    this.YY.Equals((tunable as Matrix44TunableBase).YY) &&
                    this.YZ.Equals((tunable as Matrix44TunableBase).YZ) &&
                    this.YW.Equals((tunable as Matrix44TunableBase).YW) &&
                    this.ZX.Equals((tunable as Matrix44TunableBase).ZX) &&
                    this.ZY.Equals((tunable as Matrix44TunableBase).ZY) &&
                    this.ZZ.Equals((tunable as Matrix44TunableBase).ZZ) &&
                    this.ZW.Equals((tunable as Matrix44TunableBase).ZW) &&
                    this.WX.Equals((tunable as Matrix44TunableBase).WX) &&
                    this.WY.Equals((tunable as Matrix44TunableBase).WY) &&
                    this.WZ.Equals((tunable as Matrix44TunableBase).WZ) &&
                    this.WW.Equals((tunable as Matrix44TunableBase).WW))
                    return true;
            }
            return false;
        }

        #endregion // Override
    }

    #endregion // Base

    #region Matrix44 Tunable

    /// <summary>
    /// 
    /// </summary>
    public class Matrix44Tunable : Matrix44TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Matrix44Tunable(Matrix44Member def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Matrix44Tunable(Matrix44Member def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Matrix44Tunable(Matrix44Member def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    }

    #endregion //Matrix44 Tunable

    #region Mat44V Tunable

    /// <summary>
    /// 
    /// </summary>
    public class Mat44VTunable : Matrix44TunableBase
    {
        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public Mat44VTunable(Mat44VMember def, ITunable parent)
            : base(def, parent)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Mat44VTunable(Mat44VMember def, ITunable parent, XmlReader reader)
            : base(def, parent, reader)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Mat44VTunable(Mat44VMember def, ITunable parent, String s)
            : base(def, parent, s)
        {
        }

        #endregion // Constructor(s)
    }
    
    #endregion //Mat44V Tunable

    #endregion // 3x4-Component Matrix Tunable Class
} // RSG.Metadata.Data
