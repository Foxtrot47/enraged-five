﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Parser;
using RSG.Base.Editor;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class EnumTunable :
        TunableBase,
        ITunable,
        ITunableTyped<Int64>,
        ITunableSerialisable,
        ITunableScalar<Int64>
    {
        #region Fields

        private Int64 m_value;
        private String m_valueAsString;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable enum value as string.
        /// </summary>
        [Category("Enum"),
        Description("Tunable enum value.")]
        public Int64 Value
        {
            get { return m_value; }
            set
            {
                if (m_value == value)
                    return;

                m_value = value;
                foreach (KeyValuePair<String, Int64> kvp in (this.Definition as EnumMember).Values)
                {
                    if (kvp.Value == value)
                    {
                        this.ValueAsString = kvp.Key;
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Tunable enum value.
        /// </summary>
        [Category("Enum"),
        Description("Tunable enum value as string."), RSG.Base.Editor.Command.UndoableProperty()]
        public String ValueAsString
        {
            get 
            {
                return m_valueAsString;
            }
            set
            {
                SetPropertyValue(value, m_valueAsString, () => this.ValueAsString,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_valueAsString = (String)newValue;
                            if ((this.Definition as EnumMember).Values.ContainsKey((String)newValue))
                            {
                                this.Value = (this.Definition as EnumMember).Values[(String)newValue];
                            }
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public EnumTunable(IMember def, IModel parent)
            : base(def, parent)
        {
            Debug.Assert(def is EnumMember);
            this.Value = (def as EnumMember).Init;
            if ((this.Definition as EnumMember).Values != null)
            {
                foreach (KeyValuePair<String, Int64> kvp in (this.Definition as EnumMember).Values)
                {
                    if (kvp.Value == this.Value)
                    {
                        this.ValueAsString = kvp.Key;
                        break;
                    }
                }
            }
            else
            {
                Metadata.Log.Error("Enum member type {0}. Please make sure the type exists in a psc file.", (this.Definition as EnumMember).EnumType);
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public EnumTunable(IMember def, IModel parent, XmlReader reader)
            : base(def, parent)
        {
            Debug.Assert(def is EnumMember);
            this.Value = (def as EnumMember).Init;
            if ((this.Definition as EnumMember).Values != null)
            {
                foreach (KeyValuePair<String, Int64> kvp in (this.Definition as EnumMember).Values)
                {
                    if (kvp.Value == this.Value)
                    {
                        this.ValueAsString = kvp.Key;
                        break;
                    }
                }
            }
            else
            {
                Metadata.Log.Error("Enum member type {0}. Please make sure the type exists in a psc file.", (this.Definition as EnumMember).EnumType);
                throw new NotSupportedException();
            }
            Deserialise(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public EnumTunable(IMember def, IModel parent, String s)
            : base(def, parent)
        {
            Debug.Assert(def is EnumMember);
            this.Value = (def as EnumMember).Init;
            if ((this.Definition as EnumMember).Values != null)
            {
                foreach (KeyValuePair<String, Int64> kvp in (this.Definition as EnumMember).Values)
                {
                    if (kvp.Value == this.Value)
                    {
                        this.ValueAsString = kvp.Key;
                        break;
                    }
                }
            }
            else
            {
                Metadata.Log.Error("Enum member type {0}. Please make sure the type exists in a psc file.", (this.Definition as EnumMember).EnumType);
                throw new NotSupportedException();
            }

            if ((this.Definition as EnumMember).Values.ContainsKey(s))
            {
                this.ValueAsString = s;
            }
            else
            {
                int numericValue;
                if (int.TryParse(s, out numericValue))
                {
                    foreach (KeyValuePair<string, long> value in (this.Definition as EnumMember).Values)
                    {
                        if (value.Value == numericValue)
                        {
                            this.Value = numericValue;
                        }
                    }
                }

                this.Value = 0;
            }
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader)
        {
            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                    this.Value = Parse(reader.ReadString().Trim());
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
            else
            {
                string integerValue = reader.GetAttribute("value");
                int value = 0;
                if (int.TryParse(integerValue, out value))
                {
                    foreach (KeyValuePair<String, Int64> kvp in (this.Definition as EnumMember).Values)
                    {
                        if (kvp.Value == value)
                        {
                            this.Value = value;
                            this.ValueAsString = kvp.Key;
                        }
                    }
                }

                reader.ReadStartElement();
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public new XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = base.Serialise(document, root);
            if (String.IsNullOrEmpty(ValueAsString))
            {
                SetXmlText(document, xmlElem, "0x00");
                xmlElem.SetAttribute("content", "char_array");
            }
            else
            {
                SetXmlText(document, xmlElem, ValueAsString);
            }
            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public XmlElement SerialiseToArray(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement("Item");

            if (!String.IsNullOrEmpty(this.ValueAsString))
            {
                base.SetXmlText(document, xmlElem, this.ValueAsString);
            }
            else
            {
            }
            return xmlElem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public Int64 Parse(String s)
        {
            if ((this.Definition as EnumMember).Values == null)
            {
                return 0;
            }
            else
            {
                if ((this.Definition as EnumMember).Values.ContainsKey(s))
                {
                    return (this.Definition as EnumMember).Values[s];
                }
                else
                {
                    int numericValue;
                    if (int.TryParse(s, out numericValue))
                    {
                        foreach (KeyValuePair<string, long> value in (this.Definition as EnumMember).Values)
                        {
                            if (value.Value == numericValue)
                            {
                                return numericValue;
                            }
                        }
                    }

                    return 0;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            return (String.Format("{0}", this.ValueAsString));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            if (String.IsNullOrEmpty(ValueAsString))
            {
                writer.WriteAttributeString("content", "char_array");
                writer.WriteString("0x00");
            }
            else
            {
                writer.WriteString(ValueAsString);
            }
        }

        #endregion // Controller Methods  
 
        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is EnumTunable)
            {
                this.Value = (sourceTunable as EnumTunable).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is EnumTunable)
            {
                if (this.Value.Equals((tunable as EnumTunable).Value) && this.ValueAsString.Equals((tunable as EnumTunable).ValueAsString))
                    return true;
            }
            return false;
        }

        #endregion // Override

        #region Private Functions

        private void SyncStringValueToValue()
        {
        }

        #endregion // Private Functions
    } // EnumTunable
} // RSG.Metadata.Parser namespace
