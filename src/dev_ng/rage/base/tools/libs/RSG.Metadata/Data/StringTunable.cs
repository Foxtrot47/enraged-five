﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Parser;
using RSG.Base.Editor;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// String tunable value.
    /// </summary>
    public class StringTunable :
        TunableBase,
        ITunableTyped<String>,
        ITunableSerialisable,
        ITunableScalar<String>
    {
        #region Fields

        private String m_value;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// Tunable string value.
        /// </summary>
        [Category("Tunable"),
        Description("Tunable string value."), RSG.Base.Editor.Command.UndoableProperty(),
        RSG.Base.Editor.Command.CollapsableProperty()]
        public String Value 
        {
            get { return m_value; }
            set
            {
                SetPropertyValue(value, m_value, () => this.Value,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_value = (String)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="v"></param>
        public StringTunable(StringMember def, IModel parent)
            : base(def, parent)
        {
            this.Value = def.Init;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public StringTunable(StringMember def, IModel parent, XmlReader reader)
            : base(def, parent)
        {
            this.Value = def.Init;
            Deserialise(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public StringTunable(StringMember def, IModel parent, String s)
            : base(def, parent)
        {
            this.Value = def.Init;
            if(null!=s)
                this.Value = Parse(s);
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Deserialise the tunable from the specified XmlElement.
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader)
        {
            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                    this.Value = Parse(reader.ReadString());
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();

                if (this.Definition is StringMember)
                {
                    StringMember stringMember = this.Definition as StringMember;
                    if (stringMember.SourceType == StringMember.StringSourceType.Directory)
                    {
                        this.Value = this.Value.Replace("${RS_ASSETS}", "$(core_assets)");
                    }
                }
            }
            else
            {
                reader.ReadStartElement();
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public new XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = base.Serialise(document, root);
            if (!String.IsNullOrEmpty(Value))
                SetXmlText(document, xmlElem, Value);
            return (xmlElem);
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public XmlElement SerialiseToArray(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement("Item");

            if (!String.IsNullOrEmpty(this.Value))
            {
                base.SetXmlText(document, xmlElem, this.Value);
            }
            else
            {
            }
            return xmlElem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public String Parse(String s)
        {
            return (s.Clone() as String);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            return (String.Format("\"{0}\"", this.Value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            if (!String.IsNullOrEmpty(this.Value))
            {
                writer.WriteAttributeString("value", this.Value);
            }
        }
        #endregion // Controller Methods

        #region Override

        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is StringTunable)
            {
                this.Value = (sourceTunable as StringTunable).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is StringTunable)
            {
                if (this.Value == null && (tunable as StringTunable).Value == null)
                    return true;

                if (this.Value.Equals((tunable as StringTunable).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override
    } // StringTunable
} // RSG.Metadata.Data namespace
