﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Parser;
using RSG.Base.Editor;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class BoolTunable :
        TunableBase,
        ITunable,
        ITunableTyped<bool?>,
        ITunableSerialisable,
        ITunableScalar<bool>
    {
        #region Fields

        private Boolean? m_value;

        #endregion // Fields

        #region Properties and Associated Member Data

        /// <summary>
        /// Tunable float value.
        /// </summary>
        [Category("Tunable"),
        Description("Tunable boolean value."), RSG.Base.Editor.Command.UndoableProperty()]
        public Boolean? Value
        {
            get { return m_value; }
            set
            {
                SetPropertyValue(value, m_value, () => this.Value,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_value = (Boolean?)newValue;
                        }
                ));
            }
        }

        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public BoolTunable(BoolMember def, IModel parent)
            : base(def, parent)
        {
            this.Value = false;
            this.Value = def.Init;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="node"></param>
        public BoolTunable(BoolMember def, IModel parent, XmlReader reader)
            : base(def, parent)
        {
            this.Value = false;
            this.Value = def.Init;
            Deserialise(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        /// <param name="s"></param>
        public BoolTunable(BoolMember def, IModel parent, String s)
            : base(def, parent)
        {
            this.Value = false;
            this.Value = def.Init;
            this.Value = Parse(s);
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        public void Deserialise(XmlReader reader)
        {
            String value = reader.GetAttribute("value");
            if (!String.IsNullOrEmpty(value) && !String.IsNullOrWhiteSpace(value))
            {
                this.Value = Parse(value);
            }
            if (!reader.IsEmptyElement)
            {
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
            else
            {
                reader.ReadStartElement();
            }
        }

        /// <summary>
        /// Serialise tunable to an XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        public new XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = base.Serialise(document, root);
            SetXmlAttribute(document, xmlElem, "value", ToXmlString());
            return (xmlElem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Parse(String s)
        {
            return (0 == String.Compare(s, "true", true) ? true : false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToXmlString()
        {
            if (this.Value.HasValue)
                return (this.Value.Value ? "true" : "false");
            else
                return "false";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void SerialiseWithoutChildren(XmlWriter writer)
        {
            if (this.Value.HasValue)
            {
                writer.WriteAttributeString("value", this.Value.Value ? "true" : "false");
            }
            else
            {
                writer.WriteAttributeString("value", "false");
            }
            
        }

        #endregion // Controller Methods

        #region Override
        
        /// <summary>
        /// 
        /// </summary>
        public override void SetTunableValue(ITunable sourceTunable)
        {
            if (sourceTunable is BoolTunable)
            {
                this.Value = (sourceTunable as BoolTunable).Value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override Boolean HasSameValue(ITunable tunable)
        {
            if (tunable is BoolTunable)
            {
                if (this.Value.Equals((tunable as BoolTunable).Value))
                    return true;
            }
            return false;
        }

        #endregion // Override
    } // BoolTunable
} // RSG.Metadata.Data namespace
