﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Metadata.Parser;
using RSG.Metadata.Model;
using RSG.Base.Editor.Command;
using RSG.Base.Editor;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// BaseDataType tunable class.
    /// </summary>
    public abstract class TunableBase : HierarchicalModelBase,
        ITunable,
        ITunableSerialisable
    {
        #region Constants
        #region XPath Compiled Expressions
        /// <summary>
        /// Commonly used 'value' attribute.
        /// </summary>
        protected static readonly XPathExpression XPATH_ATTR_VALUE =
            XPathExpression.Compile("@value");

        /// <summary>
        /// 
        /// </summary>
        protected static readonly XPathExpression XPATH_ATTR =
            XPathExpression.Compile("@*");
        #endregion // XPath Compiled Expressions

        #region Serialisation
        /// <summary>
        /// 
        /// </summary>
        protected static readonly String XML_ATTR_MIN = "min";

        /// <summary>
        /// 
        /// </summary>
        protected static readonly String XML_ATTR_MAX = "max";
        #endregion // Serialisation
        #endregion // Constants

        #region Fields

        private IMember m_definition;
        private Boolean m_canInheritParent;
        private Boolean? m_inheritParent = false;

        #endregion // Fields

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false), DontRegister()]
        public IMember Definition
        {
            get { return m_definition; }
            set
            {
                SetPropertyValue(value, m_definition, () => this.Definition,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_definition = (IMember)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get
            {
                return Definition.Name;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean CanInheritParent
        {
            get { return m_canInheritParent; }
            set
            {
                SetPropertyValue(value, m_canInheritParent, () => this.CanInheritParent,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_canInheritParent = (Boolean)newValue;
                        }
                ));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean? InheritParent
        {
            get { return m_inheritParent; }
            set
            {
                SetPropertyValue(value, m_inheritParent, () => this.InheritParent,
                    new Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_inheritParent = (Boolean?)newValue;
                        }
                ));
            }
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        protected TunableBase(IModel parent)
            : base(parent)
        {
            this.Definition = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="def"></param>
        public TunableBase(IMember def, IModel parent)
            : base(parent)
        {
            this.m_definition = def;
        }

        #endregion // Constructor(s)

        #region Property Changes

        protected override void OnParentModelChanged(IModel oldValue, IModel newValue)
        {
            if (newValue is StructureTunable)
            {
                StructMember structMember = (newValue as StructureTunable).Definition as StructMember;
                if (structMember != null)
                {
                    if (structMember.Definition.UsesDataInheritance == true)
                    {
                        this.CanInheritParent = true;
                    }
                }
            }
            else
            {
                this.CanInheritParent = false;
            }
        }
        
        #endregion // Property Changes

        #region ITunableSerialisable Interface
        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public virtual XmlElement Serialise(XmlDocument document, bool root)
        {
            XmlElement xmlElem = document.CreateElement(this.Definition.Name);
            return (xmlElem);            
        }
        #endregion // ITunableSerialisable Interface

        #region ITunableRange Interface Util Methods

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="newValue"></param>
        /// <returns></returns>
        protected bool ValidateTunableRange<T>(T newValue) 
            where T : IComparable
        {
            Debug.Assert(this.Definition is IMemberRange<T>,
                "Attempting to validate non-IMemberRange definition!");
            if (!(this.Definition is IMemberRange<T>))
                return (false);

            IMemberRange<T> def = (this.Definition as IMemberRange<T>);
            return ((newValue.CompareTo(def.Minimum) >= 0) &&
                (newValue.CompareTo(def.Maximum) <= 0));
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetInheritParent(Boolean? newValue)
        {
            this.InheritParent = newValue;
            this.OnPropertyChanged("InheritParent");
        }

        #endregion // ITunableRange Interface Util Methods

        #region XML Serialisation / Deserialisation Helper Methods
        /// <summary>
        /// Set XML attribute.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="elem"></param>
        /// <param name="attr"></param>
        /// <param name="val"></param>
        protected void SetXmlAttribute(XmlDocument xmlDoc, XmlElement xmlElem, String attr, String val)
        {
            XmlAttribute xmlAttr = xmlDoc.CreateAttribute(attr);
            xmlAttr.Value = val;
            xmlElem.Attributes.Append(xmlAttr);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlElem"></param>
        /// <param name="text"></param>
        protected void SetXmlText(XmlDocument xmlDoc, XmlElement xmlElem, String text)
        {
            XmlText xmlText = xmlDoc.CreateTextNode(text);
            xmlElem.AppendChild(xmlText);
        }
        #endregion // XML Serialisation / Deserialisation Helper Methods

        #region Virtual Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public abstract String ToXmlString();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public abstract void SerialiseWithoutChildren(XmlWriter writer);

        /// <summary>
        /// 
        /// </summary>
        public virtual void SetTunableValue(ITunable sourceTunable)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Boolean HasSameValue(ITunable tunable)
        {
            return false;
        }
           
        #endregion // Virtual Methods
    } // TunableBase
} // RSG.Metadata.Data namespace
