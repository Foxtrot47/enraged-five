﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Xml;
using RSG.Metadata.Parser;
using RSG.Metadata.Model;

namespace RSG.Metadata.Data
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITunableArray :
        IEnumerable<ITunable>,
        ITunable
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ITunableStructure :
        IDictionary<String, ITunable>,
        IEnumerable<KeyValuePair<String, ITunable>>,
        ITunable
    {
    }

    /// <summary>
    /// Tunable interface.
    /// </summary>
    public interface ITunable : INotifyPropertyChanged, RSG.Base.Editor.IModel
    {
        /// <summary>
        /// 
        /// </summary>
        IMember Definition { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        String ToXmlString();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        String Name { get; }

        /// <summary>
        /// 
        /// </summary>
        RSG.Base.Editor.IModel ParentModel { get; }

        /// <summary>
        /// 
        /// </summary>
        Boolean CanInheritParent { get; }

        /// <summary>
        /// 
        /// </summary>
        Boolean? InheritParent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        void SetInheritParent(Boolean? newValue);

        /// <summary>
        /// 
        /// </summary>
        void SetTunableValue(ITunable sourceTunable);

        /// <summary>
        /// 
        /// </summary>
        Boolean HasSameValue(ITunable tunable);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        void SerialiseWithoutChildren(XmlWriter writer);
    }

    /// <summary>
    /// 
    /// </summary>
    public interface ITunableScalar<T> : ITunable
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        T Parse(String s);
    }

    /// <summary>
    /// Tunable serialisable interface.
    /// </summary>
    /// This interface represents that the tunable is serialisable from/to
    /// an System.Xml.XmlElement.
    /// 
    public interface ITunableSerialisable
    {
        /// <summary>
        /// Serialise the tunable to a new XmlElement.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        XmlElement Serialise(XmlDocument document, bool root);
    }

    /// <summary>
    /// Tunable initial value interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITunableInitialValue<T>
    {
        /// <summary>
        /// Initial value.
        /// </summary>
        T Init { get; set; }
    }

    /// <summary>
    /// Typed tunable interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// This interface represents that the tunable has a typed value; with
    /// set/get modifiers.
    ///
    public interface ITunableTyped<T>
    {
        /// <summary>
        /// 
        /// </summary>
        T Value { get; set; }
    }

    /// <summary>
    /// Tunable validation interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// This was separated from the ITunableRange interface as some tunables
    /// may be able to validate themselves without having a user-defined range,
    /// e.g. Strings for length depending on sub-type or Enumerations as they
    /// must be a String value from a pre-defined list.
    /// 
    public interface ITunableValidatable<T> 
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        bool Validate(T newValue);
    }

    /// <summary>
    /// Ranged tunable interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// This interface represents that the tunable has a minimum and maximum.
    /// Its typically used in conjunction with the ITunableTyped interface
    /// although thats not necessary.
    /// 
    /// The interface also has a validation method that is used internally to
    /// validate any user-defined data for the tunable.
    /// 
    public interface ITunableRange<T> : ITunableValidatable<T> 
    {
        /// <summary>
        /// 
        /// </summary>
        T Minimum { get; }

        /// <summary>
        /// 
        /// </summary>
        T Maximum { get; }
    }

} // RSG.Metadata.Definition namespace
