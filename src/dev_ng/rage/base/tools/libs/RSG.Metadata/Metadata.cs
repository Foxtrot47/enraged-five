﻿using System;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Metadata
{

    /// <summary>
    /// 
    /// </summary>
    public static class Metadata
    {
        #region Constants
        internal readonly static String LOG_CTX = "Metadata";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Metadata library log object.
        /// </summary>
        public static IUniversalLog Log { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static Metadata()
        {
            LogFactory.Initialize();
            Metadata.Log = LogFactory.CreateUniversalLog("RSG.Metadata");
        }
        #endregion // Constructor(s)
    }

} // RSG.Metadata namespace
