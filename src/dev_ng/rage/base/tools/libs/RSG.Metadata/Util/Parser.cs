﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Metadata.Resources;

namespace RSG.Metadata.Util
{
    /// <summary>
    /// Member Utility Class; containing common utility methods to parse the different string types to certain
    /// value types
    /// </summary>
    public static class Parser
    {
        #region Static Members

        private static IDictionary<String, object> Constants = new Dictionary<String, object>()
        {
            { "PI".ToLower(), System.Math.PI },
            { "-PI".ToLower(), -System.Math.PI },
            { "INT_MAX".ToLower(), int.MaxValue },
        };

        private static Dictionary<String, Operator> Operators = new Dictionary<String, Operator>()
        {
            { "*".ToLower(), Operator.MULTIPLY },
            { "+".ToLower(), Operator.ADDITION },
            { "-".ToLower(), Operator.SUBTRACTION },
            { "/".ToLower(), Operator.DIVISION }
        };

        #endregion // Static Members
        
        #region Constructors
        /// <summary>
        /// Static constructor.
        /// </summary>
        static Parser()
        {
//#warning DHM FIX ME: not branch aware!
//            IConfig config = ConfigFactory.CreateConfig();
//            IBranch branch = config.Project.DefaultBranch;
//            String constantsPath = System.IO.Path.Combine(branch.Metadata, "definitions", "constants.xml");
//            if (!System.IO.File.Exists(constantsPath))
//            {
//                return;
//            }

//            RSG.Base.Xml.Parameters.Load(constantsPath, ref Constants, true);
        }
        #endregion
        
        #region Enums

        private enum Operator
        {
            MULTIPLY,
            ADDITION,
            SUBTRACTION,
            DIVISION,
        };

        #endregion // Enums

        public static void AddConstant(string name, double value)
        {
            lock (Constants)
            {
                Constants[name.ToLower()] = value;
            }
        }

        public static bool TryToGetConstant(string name, out long value)
        {
            object lookupValue;
            if (Constants.TryGetValue(name.ToLower(), out lookupValue))
            {
                try
                {
                    Nullable<long> theValue = lookupValue as Nullable<long>;
                    if (theValue != null)
                    {
                        value = (long)theValue;
                        return true;
                    }
                    else
                    {
                        value = 0;
                        return false;
                    }
                }
                catch
                {
                    Log.Log__Error("Failed to parse the constant {0}", name);
                }
            }

            value = 0;
            return false;
        }

        public static bool TryToGetConstant(string name, out int value)
        {
            object lookupValue;
            if (Constants.TryGetValue(name.ToLower(), out lookupValue))
            {
                value = (int)lookupValue;
                return true;
            }

            value = 0;
            return false;
        }

        public static bool TryToGetConstant(string name, out float value)
        {
            object lookupValue;
            if (Constants.TryGetValue(name.ToLower(), out lookupValue))
            {
                value = (float)lookupValue;
                return true;
            }

            value = 0;
            return false;
        }
        
        /// <summary>
        /// Parses the string into a dynamic type. Should be used for numeric types.
        /// </summary>
        /// <remarks>
        /// Needs to use a dynamic typed parameter as there is no base interface for numeric
        /// types so no casting would have been allowed.
        /// </remarks>
        public static Boolean ParseString(String s, IMember member, out dynamic result, dynamic defaultValue)
        {
            result = default(dynamic);
            s = s.Trim();
            String[] parts = s.Split(new String[1] { " " }, StringSplitOptions.RemoveEmptyEntries);

            // Make sure we have the correct number of parts
            if (parts.Length > 0 && (parts.Length == 1 || parts.Length % 3 == 0))
            {
                if (parts.Length == 1)
                {
                    dynamic parseResult = default(dynamic);
                    if (Parser.GetSingleValue(parts[0], out parseResult))
                    {
                        result = parseResult;
                        return true;
                    }
                }
                else // Use expression parsing
                {
                    try
                    {
                        List<double> values = new List<double>();
                        List<Operator> operators = new List<Operator>();
                        foreach (String part in parts)
                        {
                            if (Parser.Operators.ContainsKey(part.ToLower()))
                            {
                                operators.Add(Parser.Operators[part.ToLower()]);
                            }
                            else
                            {
                                values.Add(GetSingleValue(part));
                            }
                        }
                        if (values.Count == operators.Count + 1)
                        {
                            int valueIndex = 0;
                            double totalValue = values[0];
                            values.RemoveAt(0);
                            foreach (double value in values)
                            {
                                switch (operators[valueIndex])
                                {
                                    case Operator.MULTIPLY:
                                        totalValue *= value;
                                        break;
                                    case Operator.ADDITION:
                                        totalValue *= value;
                                        break;
                                    case Operator.SUBTRACTION:
                                        totalValue *= value;
                                        break;
                                    case Operator.DIVISION:
                                        totalValue *= value;
                                        break;
                                }
                                valueIndex++;
                            }

                            result = totalValue;
                            return true;
                        }
                    }
                    catch
                    {
                    }
                }
            }
       
            result = defaultValue;
            return false;
        }

        /// <summary>
        /// Takes a well defined string that represents a single value and
        /// parses it into a dynamic type. Should be used for numeric types.
        /// </summary>
        /// <remarks>
        /// Needs to use a dynamic typed parameter as there is no base interface for numeric
        /// types so no casting would have been allowed.
        /// </remarks>
        private static Boolean GetSingleValue(String s, out dynamic result)
        {
            try
            {
                // Check to see if we have multiple values separated with a comma
                // if so error and take the first value
                String[] commaSeparate = s.Split(new String[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
                if (commaSeparate.Length != 1)
                {
                    result = default(dynamic);
                    return false;
                }

                // Determine if the value is a hexidecimal a constant
                // or a safe string else return default
                if (s.Length >= 2 && String.Compare(s.Substring(0, 2), "0x", true) == 0)
                {
                    String hex = s.Substring(2);
                    long parseResult = default(long);
                    if (long.TryParse(hex, NumberStyles.HexNumber, new CultureInfo("en-US"), out parseResult))
                    {
                        result = (dynamic)parseResult;
                        return true;
                    }
                    else
                    {
                        result = default(dynamic);
                        return false;
                    }
                }
                else if (Parser.Constants.ContainsKey(s.ToLower()))
                {
                    result = (dynamic)Parser.Constants[s.ToLower()];
                    return true;
                }
                else if (s.Contains('f') || s.Contains("F"))
                {
                    System.Text.RegularExpressions.Regex isValidChar = new System.Text.RegularExpressions.Regex("[-.\\d]+", System.Text.RegularExpressions.RegexOptions.Compiled);
                    String numericString = String.Empty;
                    foreach (char c in s)
                    {
                        if (isValidChar.IsMatch(c.ToString()))
                            numericString += c.ToString();
                    }
                    double parseResult = default(double);
                    if (double.TryParse(numericString, NumberStyles.Number, new CultureInfo("en-US"), out parseResult))
                    {
                        result = (dynamic)parseResult;
                        return true;
                    }
                    else
                    {
                        result = default(dynamic);
                        return false;
                    }
                }
                else
                {
                    double parseResult = default(double);
                    if (double.TryParse(s, NumberStyles.Number, new CultureInfo("en-US"), out parseResult))
                    {
                        result = (dynamic)parseResult;
                        return true;
                    }
                    else
                    {
                        result = default(dynamic);
                        return false;
                    }
                }
            }
            catch
            {
                result = default(dynamic);
                return false;
            }
        }

        /// <summary>
        /// Parses a string into a boolean value
        /// </summary>
        public static Boolean GetBooleanValue(String s, IMember member, out Boolean result, bool defaultValue)
        {
            if (String.Compare("0", s, true) == 0)
            {
                result = false;
                return true;
            }
            else if (String.Compare("0.0", s, true) == 0)
            {
                result = false;
                return true;
            }
            else if (String.Compare("1", s, true) == 0)
            {
                result = true;
                return true;
            }
            else if (String.Compare("1.0", s, true) == 0)
            {
                result = true;
                return true;
            }
            else if (String.Compare("true", s, true) == 0)
            {
                result = true;
                return true;
            }
            else if (String.Compare("false", s, true) == 0)
            {
                result = false;
                return true;
            }
            else
            {
                dynamic parseResult = default(dynamic);
                if (Util.Parser.GetSingleValue(s, out parseResult))
                {
                    double value = (double)parseResult;
                    if (value == 0.0)
                    {
                        result = false;
                        return true;
                    }
                    else if (value == 1.0)
                    {
                        result = true;
                        return true;
                    }
                }
            }

            result = defaultValue;

            string msg = string.Format(SR.ParseBoolWarning, member.FriendlyTypeName, s, member.Filename, member.LineNumber);
            member.Dictionary.AddWarning(msg);
            return false;
        }

        /// <summary>
        /// Parses a string into a boolean value
        /// </summary>
        public static Boolean GetColourValue(String s, IMember member, out Color result, Color defaultValue)
        {
            dynamic parseResult = default(dynamic);
            if (Util.Parser.ParseString(s, member, out parseResult, defaultValue))
            {
                byte a = (byte)(((uint)parseResult & 0xFF000000) >> 24);
                byte r = (byte)(((uint)parseResult & 0x00FF0000) >> 16);
                byte g = (byte)(((uint)parseResult & 0x0000FF00) >> 8);
                byte b = (byte)(((uint)parseResult & 0x000000FF) >> 0);
                result = Color.FromArgb(a, r, g, b);
                return true;
            }

            result = defaultValue;
            string msg = string.Format(SR.ParseColourWarning, member.FriendlyTypeName, s, member.Filename, member.LineNumber);
            member.Dictionary.AddWarning(msg);
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="member"></param>
        /// <param name="result"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool GetFloatValue(String s, IMember member, out float result, float defaultValue)
        {
            dynamic parseResult = defaultValue;
            if (Util.Parser.ParseString(s, member, out parseResult, defaultValue))
            {
                result = (float)parseResult;
                return true;
            }

            result = defaultValue;
            string msg = string.Format(SR.ParseFloatWarning, member.FriendlyTypeName, s, member.Filename, member.LineNumber);
            member.Dictionary.AddWarning(msg);
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="member"></param>
        /// <param name="result"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool GetIntegerValue(String s, IMember member, out int result, int defaultValue)
        {
            dynamic parseResult = defaultValue;
            if (Util.Parser.ParseString(s, member, out parseResult, defaultValue))
            {
                result = (int)parseResult;
                return true;
            }

            result = defaultValue;
            string msg = string.Format(SR.ParseIntegerWarning, member.FriendlyTypeName, s, member.Filename, member.LineNumber);
            member.Dictionary.AddWarning(msg);
            return false;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="member"></param>
        /// <param name="result"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool GetByteValue(String s, IMember member, out byte result, byte defaultValue)
        {
            dynamic parseResult = defaultValue;
            if (Util.Parser.ParseString(s, member, out parseResult, defaultValue))
            {
                result = (byte)parseResult;
                return true;
            }

            result = defaultValue;
            string msg = string.Format(SR.ParseByteWarning, member.FriendlyTypeName, s, member.Filename, member.LineNumber);
            member.Dictionary.AddWarning(msg);
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="member"></param>
        /// <param name="result"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool GetSByteValue(String s, IMember member, out sbyte result, sbyte defaultValue)
        {
            dynamic parseResult = defaultValue;
            if (Util.Parser.ParseString(s, member, out parseResult, defaultValue))
            {
                result = (sbyte)parseResult;
                return true;
            }

            result = defaultValue;
            string msg = string.Format(SR.ParseSByteWarning, member.FriendlyTypeName, s, member.Filename, member.LineNumber);
            member.Dictionary.AddWarning(msg);
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="member"></param>
        /// <param name="result"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool GetUInt16Value(String s, IMember member, out UInt16 result, UInt16 defaultValue)
        {
            dynamic parseResult = defaultValue;
            if (Util.Parser.ParseString(s, member, out parseResult, defaultValue))
            {
                result = (UInt16)parseResult;
                return true;
            }

            result = defaultValue;
            string msg = string.Format(SR.ParseUInt16Warning, member.FriendlyTypeName, s, member.Filename, member.LineNumber);
            member.Dictionary.AddWarning(msg);
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="member"></param>
        /// <param name="result"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool GetInt16Value(String s, IMember member, out Int16 result, Int16 defaultValue)
        {
            dynamic parseResult = defaultValue;
            if (Util.Parser.ParseString(s, member, out parseResult, defaultValue))
            {
                result = (Int16)parseResult;
                return true;
            }

            result = defaultValue;
            string msg = string.Format(SR.ParseInt16Warning, member.FriendlyTypeName, s, member.Filename, member.LineNumber);
            member.Dictionary.AddWarning(msg);
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="member"></param>
        /// <param name="result"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool GetUInt32Value(String s, IMember member, out UInt32 result, UInt32 defaultValue)
        {
            dynamic parseResult = defaultValue;
            if (Util.Parser.ParseString(s, member, out parseResult, defaultValue))
            {
                result = (UInt32)parseResult;
                return true;
            }

            result = defaultValue;
            string msg = string.Format(SR.ParseUInt32Warning, member.FriendlyTypeName, s, member.Filename, member.LineNumber);
            member.Dictionary.AddWarning(msg);
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="member"></param>
        /// <param name="result"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool GetInt32Value(String s, IMember member, out Int32 result, Int32 defaultValue)
        {
            dynamic parseResult = defaultValue;
            if (Util.Parser.ParseString(s, member, out parseResult, defaultValue))
            {
                result = (Int32)parseResult;
                return true;
            }

            result = defaultValue;
            string msg = string.Format(SR.ParseInt32Warning, member.FriendlyTypeName, s, member.Filename, member.LineNumber);
            member.Dictionary.AddWarning(msg);
            return false;
        }

        /// <summary>
        /// Parses the string into multiple boolean values using comma separated
        /// parsing
        /// </summary>
        public static Boolean GetMulitpleBooleanValuesFromString(String s, IMember member, ref List<Boolean> result, int resultCount)
        {
            try
            {
                // Check to see if we have multiple values separated with a comma
                // if so error and take the first value
                String[] commaSeparate = s.Trim().Split(new String[1] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (String component in commaSeparate)
                {
                    Boolean parseResult = false;
                    if (!Parser.GetBooleanValue(component.Trim(), member, out parseResult, false))
                    {
                        return false;
                    }
                    else
                    {
                        result.Add(parseResult);
                    }
                }
                if (resultCount > 0)
                {
                    if (result.Count != resultCount)
                    {
                        if (result.Count < resultCount)
                        {
                            int missingCount = resultCount - result.Count;
                            for (int i = 0; i < missingCount; i++)
                            {
                                result.Add(default(Boolean));
                            }
                        }
                        else
                        {
                            while (result.Count > resultCount)
                            {
                                result.RemoveAt(result.Count - 1);
                            }
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Parses the string into multiple float values using comma separated
        /// parsing
        /// </summary>
        public static Boolean GetMulitpleFloatValuesFromString(String s, IMember member, out float[] result, int resultCount, float defaultValue)
        {
            List<float> parsedResult = new List<float>();

            try
            {
                // Check to see if we have multiple values separated with a comma
                // if so error and take the first value
                String[] commaSeparate = s.Trim().Split(new String[1] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (String component in commaSeparate)
                {
                    float parseResult = 0.0f;
                    if (!Parser.GetFloatValue(component.Trim(), member, out parseResult, 0.0f))
                    {
                        result = new float[resultCount];
                        for (int i = 0; i < resultCount; i++)
                            result[i] = defaultValue;
                        return false;
                    }
                    else
                    {
                        parsedResult.Add((float)parseResult);
                    }
                }
                if (resultCount > 0)
                {
                    if (parsedResult.Count != resultCount)
                    {
                        if (parsedResult.Count < resultCount)
                        {
                            int missingCount = resultCount - parsedResult.Count;
                            for (int i = 0; i < missingCount; i++)
                            {
                                parsedResult.Add(default(float));
                            }
                        }
                        else
                        {
                            while (parsedResult.Count > resultCount)
                            {
                                parsedResult.RemoveAt(parsedResult.Count - 1);
                            }
                        }
                    }
                }
                result = parsedResult.ToArray();
                return true;
            }
            catch
            {
                result = new float[resultCount];
                for (int i = 0; i < resultCount; i++)
                    result[i] = defaultValue;
                return false;
            }
        }

        /// <summary>
        /// Parses the string into a long integer.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static double ParseString(String s)
        {
            s = s.Trim();
            String[] parts = s.Split(new String[1] { " " }, StringSplitOptions.RemoveEmptyEntries);

            // Make sure we have the correct number of parts
            if (parts.Length > 0 && (parts.Length == 1 || parts.Length % 3 == 0))
            {
                if (parts.Length == 1)
                {
                    return Parser.GetSingleValue(parts[0]);
                }
                else // Use expression parsing
                {
                    try
                    {
                        List<double> values = new List<double>();
                        List<Operator> operators = new List<Operator>();
                        foreach (String part in parts)
                        {
                            if (Parser.Operators.ContainsKey(part.ToLower()))
                            {
                                operators.Add(Parser.Operators[part.ToLower()]);
                            }
                            else
                            {
                                values.Add(GetSingleValue(part));
                            }
                        }
                        if (values.Count == operators.Count + 1)
                        {
                            int valueIndex = 0;
                            double totalValue = values[0];
                            values.RemoveAt(0);
                            foreach (double value in values)
                            {
                                switch (operators[valueIndex])
                                {
                                    case Operator.MULTIPLY:
                                        totalValue *= value;
                                        break;
                                    case Operator.ADDITION:
                                        totalValue *= value;
                                        break;
                                    case Operator.SUBTRACTION:
                                        totalValue *= value;
                                        break;
                                    case Operator.DIVISION:
                                        totalValue *= value;
                                        break;
                                }
                                valueIndex++;
                            }

                            return totalValue;
                        }
                    }
                    catch
                    {
                        Log.Log__Warning("Unable to parse the string {0}", s);
                        return default(double);
                    }
                }

                Log.Log__Warning("Unable to parse the string {0}", s);
                return default(double);
            }
            else
            {
                Log.Log__Warning("Unable to parse the string {0}", s);
                return default(double);
            }
        }

        private static double GetSingleValue(String s)
        {
            try
            {
                // Check to see if we have multiple values separated with a comma
                // if so error and take the first value
                String[] commaSeparate = s.Split(new String[1] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (commaSeparate.Length > 1)
                {
                    RSG.Base.Logging.Log.Log__Warning("Syntax error on string property {0}", s);
                    s = commaSeparate[0];
                }

                // Determine if the value is a hexidecimal a constant
                // or a safe string else return default
                if (s.Length >= 2 && String.Compare(s.Substring(0, 2), "0x", true) == 0)
                {
                    String hex = s.Substring(2);
                    return (double)long.Parse(hex, System.Globalization.NumberStyles.HexNumber);
                }
                else if (Parser.Constants.ContainsKey(s.ToLower()))
                {
                    return (double)Parser.Constants[s.ToLower()];
                }
                else if (s.Contains('f') || s.Contains("F"))
                {
                    System.Text.RegularExpressions.Regex isValidChar = new System.Text.RegularExpressions.Regex("[-.\\d]+", System.Text.RegularExpressions.RegexOptions.Compiled);
                    String numericString = String.Empty;
                    foreach (char c in s)
                    {
                        if (isValidChar.IsMatch(c.ToString()))
                            numericString += c.ToString();
                    }
                    return double.Parse(numericString, System.Globalization.NumberStyles.Number);
                }
                else
                {
                    double parseResult = default(double);
                    if (!double.TryParse(s, out parseResult))
                    {
                        Log.Log__Warning("Unable to parse the string {0}", s);
                    }
                    return parseResult;
                }
            }
            catch
            {
                Log.Log__Warning("Unable to parse the string {0}", s);
                return default(double);
            }
        }

    } // Parser
} // RSG.Metadata.Util
