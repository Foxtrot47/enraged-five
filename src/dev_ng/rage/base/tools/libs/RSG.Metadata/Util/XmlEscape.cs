﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Metadata.Util
{

    /// <summary>
    /// 
    /// </summary>
    public static class XmlEscape
    {
        /// <summary>
        /// Escape a metadata DataType name.
        /// </summary>
        /// <param name="datatype"></param>
        /// <returns></returns>
        public static String EscapeDataTypeString(String datatype)
        {
            String copy = (datatype.Clone() as String);
            if (copy.StartsWith("::"))
                copy = copy.TrimStart(new char[] { ';' });
            copy = copy.Replace(":", "_");
            return (copy);
        }
    }

} // RSG.Metadata.Util namespace
