﻿using System;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Math;

namespace RSG.Metadata.Util
{

    /// <summary>
    /// Xml Utility Class; containing common utility methods to help XmlElement
    /// construction.
    /// </summary>
    public static class Xml
    {
        #region Common XmlDocument Methods
        /// <summary>
        /// Create an XML document for metadata.
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        public static XmlDocument CreateDocument(String rootName, out XmlElement xmlRoot)
        {
            XmlDocument xmlDoc = new XmlDocument();

            // Write down the XML declaration
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration(
                RSG.Metadata.Model.MetaFile.DEFAULT_XML_VERSION,
                RSG.Metadata.Model.MetaFile.DEFAULT_XML_ENCODING, null);
            xmlDoc.AppendChild(xmlDeclaration);

            // Create the root element
            xmlRoot = xmlDoc.CreateElement(rootName);
            xmlDoc.AppendChild(xmlRoot);

            return (xmlDoc);
        }
        #endregion // Common XmlDocument Methods

        #region Common XmlElement Methods
        /// <summary>
        /// Create, and return, new XmlElement with specified parameters.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="attrName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XmlElement CreateElementWithAttribute<T>(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, String attrName, T value)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(elemName);
            XmlAttribute xmlAttr = xmlDoc.CreateAttribute(attrName);
            xmlAttr.Value = value.ToString();
            xmlElem.Attributes.Append(xmlAttr);
            if (null != xmlParent)
                xmlParent.AppendChild(xmlElem);

            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement with specified parameters.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="attrName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XElement CreateElementWithAttribute<T>(XElement xmlParent, String elemName, String attrName, T value)
        {
            XElement xmlElem = new XElement(elemName);
            XAttribute xmlAttr = new XAttribute(attrName, value.ToString());
            xmlElem.Add(xmlAttr);
            if (null != xmlParent)
                xmlParent.Add(xmlElem);

            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement with specified parameters.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XmlElement CreateElementWithValueAttribute<T>(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, T value)
        {
            return (CreateElementWithAttribute(xmlDoc, xmlParent, elemName, "value", value));
        }

        /// <summary>
        /// Create, and return, new XmlElement with specified parameters.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XElement CreateElementWithValueAttribute<T>(XElement xmlParent, String elemName, T value)
        {
            return (CreateElementWithAttribute(xmlParent, elemName, "value", value));
        }


        /// <summary>
        /// Create, and return, new XmlElement for a Vector2f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XmlElement CreateElementWithVectorAttributes(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, Vector2f value)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(elemName);
            XmlAttribute xmlAttrX = xmlDoc.CreateAttribute("x");
            xmlAttrX.Value = String.Format("{0:r}", value.X);
            xmlElem.Attributes.Append(xmlAttrX);

            XmlAttribute xmlAttrY = xmlDoc.CreateAttribute("y");
            xmlAttrY.Value = String.Format("{0:r}", value.Y);
            xmlElem.Attributes.Append(xmlAttrY);

            xmlParent.AppendChild(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement for a Vector2f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XElement CreateElementWithVectorAttributes(XElement xmlParent, String elemName, Vector2f value)
        {
            XElement xmlElem = new XElement(elemName);
            XAttribute xmlAttrX = new XAttribute("x", String.Format("{0:r}", value.X));
            xmlElem.Add(xmlAttrX);

            XAttribute xmlAttrY = new XAttribute("y", String.Format("{0:r}", value.Y));
            xmlElem.Add(xmlAttrY);

            xmlParent.Add(xmlElem);
            return (xmlElem);
        }


        /// <summary>
        /// Create, and return, new XmlElement for a Vector3f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XmlElement CreateElementWithVectorAttributes(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, Vector3f value)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(elemName);
            XmlAttribute xmlAttrX = xmlDoc.CreateAttribute("x");
            xmlAttrX.Value = String.Format("{0:r}", value.X);
            xmlElem.Attributes.Append(xmlAttrX);

            XmlAttribute xmlAttrY = xmlDoc.CreateAttribute("y");
            xmlAttrY.Value = String.Format("{0:r}", value.Y);
            xmlElem.Attributes.Append(xmlAttrY);

            XmlAttribute xmlAttrZ = xmlDoc.CreateAttribute("z");
            xmlAttrZ.Value = String.Format("{0:r}", value.Z);
            xmlElem.Attributes.Append(xmlAttrZ);

            xmlParent.AppendChild(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement for a Vector3f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XElement CreateElementWithVectorAttributes(XElement xmlParent, String elemName, Vector3f value)
        {
            XElement xmlElem = new XElement(elemName);
            XAttribute xmlAttrX = new XAttribute("x", String.Format("{0:r}", value.X));
            xmlElem.Add(xmlAttrX);

            XAttribute xmlAttrY = new XAttribute("y", String.Format("{0:r}", value.Y));
            xmlElem.Add(xmlAttrY);

            XAttribute xmlAttrZ = new XAttribute("z", String.Format("{0:r}", value.Z));
            xmlElem.Add(xmlAttrZ);

            xmlParent.Add(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement for a Vector3f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XmlElement CreateElementWithVectorAttributes(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, Vector4f value)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(elemName);
            XmlAttribute xmlAttrX = xmlDoc.CreateAttribute("x");
            xmlAttrX.Value = String.Format("{0:r}", value.X);
            xmlElem.Attributes.Append(xmlAttrX);

            XmlAttribute xmlAttrY = xmlDoc.CreateAttribute("y");
            xmlAttrY.Value = String.Format("{0:r}", value.Y);
            xmlElem.Attributes.Append(xmlAttrY);

            XmlAttribute xmlAttrZ = xmlDoc.CreateAttribute("z");
            xmlAttrZ.Value = String.Format("{0:r}", value.Z);
            xmlElem.Attributes.Append(xmlAttrZ);

            XmlAttribute xmlAttrW = xmlDoc.CreateAttribute("w");
            xmlAttrW.Value = String.Format("{0:r}", value.W);
            xmlElem.Attributes.Append(xmlAttrW);

            xmlParent.AppendChild(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement for a Vector3f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XElement CreateElementWithVectorAttributes(XElement xmlParent, String elemName, Vector4f value)
        {
            XElement xmlElem = new XElement(elemName);
            XAttribute xmlAttrX = new XAttribute("x", String.Format("{0:r}", value.X));
            xmlElem.Add(xmlAttrX);

            XAttribute xmlAttrY = new XAttribute("y", String.Format("{0:r}", value.Y));
            xmlElem.Add(xmlAttrY);

            XAttribute xmlAttrZ = new XAttribute("z", String.Format("{0:r}", value.Z));
            xmlElem.Add(xmlAttrZ);

            XAttribute xmlAttrW = new XAttribute("w", String.Format("{0:r}", value.W));
            xmlElem.Add(xmlAttrW);

            xmlParent.Add(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new Vector3 XmlElement for a Vector3f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XmlElement CreateVector3ElementForColour(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, Vector3f value)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(elemName);
            XmlAttribute xmlAttrType = xmlDoc.CreateAttribute("type");
            xmlAttrType.Value = "color";
            xmlElem.Attributes.Append(xmlAttrType);

            XmlAttribute xmlAttrX = xmlDoc.CreateAttribute("x");
            xmlAttrX.Value = String.Format("{0:r}", value.X);
            xmlElem.Attributes.Append(xmlAttrX);

            XmlAttribute xmlAttrY = xmlDoc.CreateAttribute("y");
            xmlAttrY.Value = String.Format("{0:r}", value.Y);
            xmlElem.Attributes.Append(xmlAttrY);

            XmlAttribute xmlAttrZ = xmlDoc.CreateAttribute("z");
            xmlAttrZ.Value = String.Format("{0:r}", value.Z);
            xmlElem.Attributes.Append(xmlAttrZ);

            xmlParent.AppendChild(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement for a Vector3f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XElement CreateVector3ElementForColour(XElement xmlParent, String elemName, Vector3f value)
        {
            XElement xmlElem = new XElement(elemName);
            XAttribute xmlAttrType = new XAttribute("type", "color");
            xmlElem.Add(xmlAttrType);

            XAttribute xmlAttrX = new XAttribute("x", String.Format("{0:r}", value.X));
            xmlElem.Add(xmlAttrX);

            XAttribute xmlAttrY = new XAttribute("y", String.Format("{0:r}", value.Y));
            xmlElem.Add(xmlAttrY);

            XAttribute xmlAttrZ = new XAttribute("z", String.Format("{0:r}", value.Z));
            xmlElem.Add(xmlAttrZ);

            xmlParent.Add(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new Color32 XmlElement for a Vector3f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <param name="scale">true scales the Vector floats to 0-255, otherwise leave as-is</param>
        /// <returns></returns>
        public static XmlElement CreateColor32Element(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, Vector3f value, bool scale = true)
        {
            UInt32 color32 = 0x00000000;
            if (scale)
            {
                color32 = (uint)0xFF000000 |
                    (((uint)(value.X * 255.0f)) & 0xFF) << 16 |
                    (((uint)(value.Y * 255.0f)) & 0xFF) << 8 |
                    (((uint)(value.Z * 255.0f)) & 0xFF);
            }
            else
            {
                color32 = (uint)0xFF000000 |
                    (((uint)value.X) & 0xFF) << 16 |
                    (((uint)value.Y) & 0xFF) << 8 |
                    (((uint)value.Z) & 0xFF);
            }

            return (CreateElementWithValueAttribute(xmlDoc, xmlParent, elemName, (string)color32.ToString()));
        }

        /// <summary>
        /// Create, and return, new Color32 XmlElement for a Vector3f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <param name="scale">true scales the Vector floats to 0-255, otherwise leave as-is</param>
        /// <returns></returns>
        public static XElement CreateColor32Element(XElement xmlParent, String elemName, Vector3f value, bool scale = true)
        {
            UInt32 color32 = 0x00000000;
            if (scale)
            {
                color32 = (uint)0xFF000000 |
                    (((uint)(value.X * 255.0f)) & 0xFF) << 16 |
                    (((uint)(value.Y * 255.0f)) & 0xFF) << 8 |
                    (((uint)(value.Z * 255.0f)) & 0xFF);
            }
            else
            {
                color32 = (uint)0xFF000000 |
                    (((uint)value.X) & 0xFF) << 16 |
                    (((uint)value.Y) & 0xFF) << 8 |
                    (((uint)value.Z) & 0xFF);
            }

            return (CreateElementWithValueAttribute(xmlParent, elemName, (string)color32.ToString()));
        }
        
        /// <summary>
        /// Create, and return, new Color32 XmlElement for a Vector4f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <param name="scale">true scales the Vector floats to 0-255, otherwise leave as-is</param>
        /// <returns></returns>
        public static XmlElement CreateColor32Element(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, Vector4f value, bool scale = true)
        {
            UInt32 color32 = 0x00000000;
            if (scale)
            {
                color32 = (((uint)(value.W * 255.0f)) & 0xFF) << 24 |
                    (((uint)(value.X * 255.0f)) & 0xFF) << 16 |
                    (((uint)(value.Y * 255.0f)) & 0xFF) << 8 |
                    (((uint)(value.Z * 255.0f)) & 0xFF);
            }
            else
            {
                color32 = (((uint)value.W) & 0xFF) << 24 |
                    (((uint)value.X) & 0xFF) << 16 |
                    (((uint)value.Y) & 0xFF)<< 8 |
                    (((uint)value.Z) & 0xFF);
            }

            return (CreateElementWithValueAttribute(xmlDoc, xmlParent, elemName, (string)color32.ToString()));
        }


        /// <summary>
        /// Create, and return, new Color32 XmlElement for a Vector4f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <param name="scale">true scales the Vector floats to 0-255, otherwise leave as-is</param>
        /// <returns></returns>
        public static XElement CreateColor32Element(XElement xmlParent, String elemName, Vector4f value, bool scale = true)
        {
            UInt32 color32 = 0x00000000;
            if (scale)
            {
                color32 = (((uint)(value.W * 255.0f)) & 0xFF) << 24 |
                    (((uint)(value.X * 255.0f)) & 0xFF) << 16 |
                    (((uint)(value.Y * 255.0f)) & 0xFF) << 8 |
                    (((uint)(value.Z * 255.0f)) & 0xFF);
            }
            else
            {
                color32 = (((uint)value.W) & 0xFF) << 24 |
                    (((uint)value.X) & 0xFF) << 16 |
                    (((uint)value.Y) & 0xFF) << 8 |
                    (((uint)value.Z) & 0xFF);
            }

            return (CreateElementWithValueAttribute(xmlParent, elemName, (string)color32.ToString()));
        }


        /// <summary>
        /// Create, and return, new XmlElement for the BoundingBox3f type object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XmlElement CreateAABBElementForBoundingBox(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, BoundingBox3f value)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(elemName);

            CreateElementWithVectorAttributes(xmlDoc, xmlElem, "min", value.Min);
            CreateElementWithVectorAttributes(xmlDoc, xmlElem, "max", value.Max);

            xmlParent.AppendChild(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement for the BoundingBox3f type object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XElement CreateAABBElementForBoundingBox(XElement xmlParent, String elemName, BoundingBox3f value)
        {
            XElement xmlElem = new XElement(elemName);

            CreateElementWithVectorAttributes(xmlElem, "min", value.Min);
            CreateElementWithVectorAttributes(xmlElem, "max", value.Max);

            xmlParent.Add(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement for a Vector3f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XmlElement CreateElementWithVectorAttributes(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, Quaternionf value)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(elemName);
            XmlAttribute xmlAttrX = xmlDoc.CreateAttribute("x");
            xmlAttrX.Value = String.Format("{0:r}", value.X);
            xmlElem.Attributes.Append(xmlAttrX);

            XmlAttribute xmlAttrY = xmlDoc.CreateAttribute("y");
            xmlAttrY.Value = String.Format("{0:r}", value.Y);
            xmlElem.Attributes.Append(xmlAttrY);

            XmlAttribute xmlAttrZ = xmlDoc.CreateAttribute("z");
            xmlAttrZ.Value = String.Format("{0:r}", value.Z);
            xmlElem.Attributes.Append(xmlAttrZ);

            XmlAttribute xmlAttrW = xmlDoc.CreateAttribute("w");
            xmlAttrW.Value = String.Format("{0:r}", value.W);
            xmlElem.Attributes.Append(xmlAttrW);

            xmlParent.AppendChild(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement for a Vector3f object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XElement CreateElementWithVectorAttributes(XElement xmlParent, String elemName, Quaternionf value)
        {
            XElement xmlElem = new XElement(elemName);
            XAttribute xmlAttrX = new XAttribute("x", String.Format("{0:r}", value.X));
            xmlElem.Add(xmlAttrX);

            XAttribute xmlAttrY = new XAttribute("y", String.Format("{0:r}", value.Y));
            xmlElem.Add(xmlAttrY);

            XAttribute xmlAttrZ = new XAttribute("z", String.Format("{0:r}", value.Z));
            xmlElem.Add(xmlAttrZ);

            XAttribute xmlAttrW = new XAttribute("w",String.Format("{0:r}", value.W));
            xmlElem.Add(xmlAttrW);

            xmlParent.Add(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement for an Array object.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="xmlParent"></param>
        /// <param name="elemName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XmlElement CreateElementWithArrayNodes<T>(XmlDocument xmlDoc, XmlElement xmlParent, String elemName, T[] value, string delimiter="\n")
        {
            XmlElement xmlElem = xmlDoc.CreateElement(elemName);

            xmlParent.AppendChild(xmlElem);
            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement with specified parameters.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="parent"></param>
        /// <param name="elemName"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static XmlElement CreateElementWithText(XmlDocument xmlDoc, XmlElement parent, String elemName, String text)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(elemName);
            if (!String.IsNullOrEmpty(text))
            {
                XmlText xmlText = xmlDoc.CreateTextNode(text);
                xmlElem.AppendChild(xmlText);
            }
            if (null != parent)
                parent.AppendChild(xmlElem);

            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlElement with specified parameters.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="parent"></param>
        /// <param name="elemName"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static XElement CreateElementWithText(XElement parent, String elemName, String text)
        {
            XElement xmlElem = new XElement(elemName);
            if (!String.IsNullOrEmpty(text))
            {
                XText xmlText = new XText(text);
                xmlElem.Add(xmlText);
            }
            if (null != parent)
                parent.Add(xmlElem);

            return (xmlElem);
        }

        /// <summary>
        /// Create, and return, new XmlComment with specified parameters.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="parent"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public static XmlComment CreateComment(XmlDocument xmlDoc, XmlElement parent, String comment)
        {
            XmlComment xmlComment = xmlDoc.CreateComment(comment);
            if (null != parent)
                parent.AppendChild(xmlComment);
            return (xmlComment);
        }


        /// <summary>
        /// Create, and return, new XmlComment with specified parameters.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="parent"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        public static XComment CreateComment(XElement parent, String comment)
        {
            XComment xmlComment = new XComment(comment);
            if (null != parent)
                parent.Add(xmlComment);
            return (xmlComment);
        }

        public static XmlElement FindChildNodeWithAttr(XmlElement parent, String attr, String val)
        {
            if(parent.ChildNodes.Count<=0)
                return null;
            foreach (XmlNode cn in parent.ChildNodes)
            {
                if(null!=cn.Attributes[attr] && cn.Attributes[attr].Value == val)
                    return cn as XmlElement;
            }
            return null;
        }

        public static XElement FindChildNodeWithAttr(XElement parent, String attr, String val)
        {
            if (!parent.Elements().Any())
                return null;
            foreach (XElement cn in parent.Elements())
            {
                if (0 != cn.Attributes(attr).Count() && cn.Attributes(attr).First().Value == val)
                    return cn;
            }
            return null;
        }
        #endregion // Common XmlElement Methods
    }

} // RSG.Metadata.Util
