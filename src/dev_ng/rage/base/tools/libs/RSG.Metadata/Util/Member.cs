﻿using System;
using System.Collections.Generic;

using RSG.Metadata.Data;
using RSG.Metadata.Parser;

namespace RSG.Metadata.Util
{
    /// <summary>
    /// Member Utility Class; containing common utility methods to for accessing members in member hierarchies.
    /// </summary>
    public static class Member
    {
        #region Public Methods
        /// <summary>
        /// Find a StructMember given and StructArray name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="st"></param>
        public static StructMember FindStructMemberByName(IMember member, string name)
        {
            if (member is ArrayMember)
            {
                ArrayMember arrayMember = member as ArrayMember;
                if (arrayMember.ElementType is StructMember)
                {
                    if (String.Compare(arrayMember.ElementStructureType, name, true) == 0)
                    {
                        return (arrayMember.ElementType as StructMember);
                    }

                    Dictionary<String, IMember> members = (arrayMember.ElementType as StructMember).Definition.Members;

                    return FindStructMemberByName(members, name);
                }
                else if (arrayMember.ElementType is PointerMember)
                {
                    PointerMember pointerMember = (arrayMember.ElementType as PointerMember);
                    if (String.Compare(pointerMember.ObjectType.Name, name, true) == 0)
                    {
                        StructMember structure = new StructMember(pointerMember.ObjectType, pointerMember.ParentStructure);
                        return structure;
                    }

                    Dictionary<String, IMember> members = pointerMember.ObjectType.Members;
                    return FindStructMemberByName(members, name);
                }
            }
            else if (member is StructMember)
            {
                StructMember structMember = member as StructMember;
                return FindStructMemberByName(structMember.Definition.Members, name);
            }
            return null;
        }

        /// <summary>
        /// Find a StructMember given and StructArray name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="st"></param>
        public static StructMember FindStructMemberByName(Dictionary<String, IMember> members, string name)
        {
            StructMember memberTarget = null;
            if (true == members.ContainsKey(name))
            {
                if( members[name] is ArrayMember )
                    return ((members[name] as ArrayMember).ElementType as StructMember);
            }

            foreach (KeyValuePair<String, IMember> kvp in members)
            {
                memberTarget = FindStructMemberByName(kvp.Value as IMember, name);
                if( memberTarget != null )
                    break;
            }

            return memberTarget;
        }

        /// <summary>
        /// Find a PointerMember given and StructArray name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="st"></param>
        public static PointerMember FindPointerMemberByName(IMember member, string name)
        {
            if (member is ArrayMember)
            {
                ArrayMember arrayMember = member as ArrayMember;
                if (arrayMember.ElementType is PointerMember)
                {
                    PointerMember pointerMember = (arrayMember.ElementType as PointerMember);
                    if (String.Compare(pointerMember.ObjectType.Name, name, true) == 0)
                    {
                        return pointerMember;
                    }

                    Dictionary<String, IMember> members = pointerMember.ObjectType.Members;
                    return FindPointerMemberByName(members, name);
                }
            }
            else if (member is StructMember)
            {
                StructMember structMember = member as StructMember;
                return FindPointerMemberByName(structMember.Definition.Members, name);
            }
            return null;
        }

        /// <summary>
        /// Find a PointerMember given and StructArray name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="st"></param>
        public static PointerMember FindPointerMemberByName(Dictionary<String, IMember> members, string name)
        {
            PointerMember memberTarget = null;
            if (true == members.ContainsKey(name))
            {
                if (members[name] is ArrayMember)
                    return ((members[name] as ArrayMember).ElementType as PointerMember);
            }

            foreach (KeyValuePair<String, IMember> kvp in members)
            {
                memberTarget = FindPointerMemberByName(kvp.Value as IMember, name);
                if (memberTarget != null)
                    break;
            }

            return memberTarget;
        } 


#endregion // Public Methods
    }
}
