﻿using System;
using System.Collections.Generic;

using RSG.Metadata.Data;
using RSG.Metadata.Parser;

namespace RSG.Metadata.Util
{
    /// <summary>
    /// Tuenable Utility Class; containing common utility methods to for accessing tuneables in tuneable hierarchies.
    /// </summary>
    public static class Tunable
    {
        #region Public Methods
        /// <summary>
        /// Find the first array in the hierachy with a definition matching 'name'
        /// </summary>
        /// <param name="name"></param>
        /// <param name="tunableArray"></param>
        public static ITunable FindFirstArrayNamed(string name, ITunableArray tunableArray)
        {
            if ( tunableArray.Name == name )
                return tunableArray;

            foreach ( ITunable tunable in tunableArray )
            {
                if (tunable is ITunableArray)
                    return FindFirstArrayNamed(name, tunable as ITunableArray);
                if (tunable is ITunableStructure)
                    return FindFirstStuctureNamed(name, tunable as ITunableStructure);
            }
            return null;
        }

        /// <summary>
        /// Find the first structure in the hierachy with a key matching 'name'
        /// </summary>
        /// <param name="name"></param>
        /// <param name="st"></param>
        public static ITunable FindFirstStuctureNamed(string name, ITunableStructure tunableStructure)
        {
            ITunable targetTunable = null;
            if (true == tunableStructure.ContainsKey(name))
                return tunableStructure[name];

            foreach (KeyValuePair<string, ITunable> kvp in tunableStructure)
            {
                if (targetTunable == null)
                {
                    if (kvp.Value is ITunableStructure)
                        targetTunable = FindFirstStuctureNamed(name, kvp.Value as ITunableStructure);
                    else if (kvp.Value is ITunableArray)
                        targetTunable = FindFirstArrayNamed(name, kvp.Value as ITunableArray);
                }
               
            }
            return targetTunable;
        }

        /// <summary>
        /// Builds a list of structures in the hierachy with a key matching 'name'
        /// </summary>
        /// <param name="name"></param>
        /// <param name="st"></param>
        public static List<ITunableStructure> FindAllStucturesNamed(string name, ITunableStructure td)
        {
            List<ITunableStructure> target = new List<ITunableStructure>();
            foreach (KeyValuePair<string, ITunable> kvp in td)
            {
                if (kvp.Value is ITunableStructure)
                {
                    if (kvp.Key == name)
                        target.Add(kvp.Value as ITunableStructure);
                    else
                        target.AddRange(FindAllStucturesNamed(name, kvp.Value as ITunableStructure));
                }
                else if (kvp.Value is ITunableArray)
                {
                    foreach (ITunable tunable in (kvp.Value as ITunableArray))
                    {
                        if (kvp.Value is ITunableStructure)
                        {
                            if (kvp.Key == name)
                                target.Add(kvp.Value as ITunableStructure);
                            else
                                target.AddRange(FindAllStucturesNamed(name, kvp.Value as ITunableStructure));
                        }
                    }
                }
            }
            return target;
        }



        /// <summary>
        /// Returns the path to the object, including its name, in this format:
        /// <RootTunableName>/<ParentTunableName>/<ThisTunableName>
        /// </summary>
        /// <returns></returns>
        public static string GetPath(ITunable tunable)
        {
            string path = tunable.Name;
            ITunable parent = tunable.ParentModel as ITunable;
            while ( parent != null )
            {
                path = parent.Name + "/" + path;
                parent = parent.ParentModel as ITunable;
            }

            return path;
        }


        /// <summary>
        /// Returns the path to the object, including its name and any array indexes, in this format:
        /// <RootTunableName>/<ArrayTunableName>/<ArrayIndex>/<ThisTunableName>
        /// This is designed to be compatible with the URL format for live editing metadata.
        /// Handles arrays and pointers correctly, as far as I know.
        /// Example:
        /// A tunable named AmmoMax that has a StructureTunable as it's parent, which
        /// itself is pointed to by a PointerTunable, which itself is the first element of a
        /// ArrayTunable called Infos, which is pointed to by a PT which lies inside another AT
        /// also called Infos will get this path:
        /// Infos/0/Infos/0/AmmoMax
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetFullPath( ITunable tunable )
        {
            string path = tunable.Name;
            ITunable current = tunable;
            ITunable parent = tunable.ParentModel as ITunable;

            while ( parent != null )
            {
                if ( parent is ArrayTunable )
                {
                    ArrayTunable array = parent as ArrayTunable;
                    for ( int i = 0; i < array.Length; i++ )
                    {
                        if ( array[i] == current )
                        {
                            path = parent.Name + "/" + i.ToString() + "/" + path;
                            break;
                        }
                    }
                }
                else if (parent is MapTunable)
                {
                    MapTunable map = parent as MapTunable;
                    foreach (MapItemTunable mapItem in map.Items)
                    {
                        if (mapItem == current)
                        {
                            path = parent.Name + "/" + mapItem.Key + "/" + path;
                            break;
                        }
                    }
                }
                else if ( !(parent is PointerTunable) && !(parent is StructureTunable) && !(parent is MapItemTunable))
                {
                    path = parent.Name + "/" + path;
                }

                current = current.ParentModel as ITunable;
                parent = parent.ParentModel as ITunable;
            }

            return path;
        }


        #endregion // Public Methods
    }
}
