﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Metadata
{

    /// <summary>
    /// 
    /// </summary>
    public static class Consts
    {
        /// <summary>
        /// Universal log context.
        /// </summary>
        public static readonly String LOG_CONTEXT = "metadata";
    }

} // RSG.Metadata namespace
