﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

using RSG.Base.ConfigParser;

namespace AutomatedBuildShared
{
    public class CutsceneBuild : BuildTask
    {
        public bool MP3Hack;

        public string ScenePath;
        public int SectionMethod;
        public float SectionDuration;
        public bool IsConcatenated;

        public CutsceneBuild()
        {
            SectionMethod = 0;  //No sectioning.
            SectionDuration = 0;
        }

        public CutsceneBuild(bool section, float duration)
        {
            SectionMethod = section ? 1 : 0;
            SectionDuration = duration;
        }

        public CutsceneBuild(bool isConcatenatedScene)
        {
            SectionMethod = isConcatenatedScene ? 2 : 0;  //Sectioned by Camera Cuts
            SectionDuration = 0.0f;
        }

        public override bool Execute()
        {
            ConfigGameView coreView = new ConfigGameView();
            string cutsceneBuildScript = Path.Combine(coreView.ToolsLibDir, "util/motionbuilder/cutscene.rb");
            if (MP3Hack == true)
            {
                cutsceneBuildScript = "X:/payne/tools/lib/util/motionbuilder/cutscene.rb";
            }

            string cutsceneName = Path.GetFileName(ScenePath);
            Process pExe = new System.Diagnostics.Process();
            if (MP3Hack == true)
                pExe.StartInfo.FileName = "X:\\payne\\tools\\bin\\ruby\\bin\\ruby.exe";
            else
                pExe.StartInfo.FileName = "ruby";

            pExe.StartInfo.Arguments = cutsceneBuildScript + " --build --perforce --inputDir=" + ScenePath + " --sectionMethod=" + SectionMethod + " --sectionDuration=" + SectionDuration + " --sceneName=" + cutsceneName;

            if (IsConcatenated == true)
            {
                pExe.StartInfo.Arguments += " --concat";
            }

            pExe.StartInfo.CreateNoWindow = true;
            pExe.StartInfo.UseShellExecute = false;

            //HACK:  This is a hack to have RDR3's Automated Build run on the MP3 branch.
            //This is done because the MP3 installer does not have command line access to run
            //and more importantly, adding to it is undesirable.
            if (MP3Hack == true)
            {
                pExe.StartInfo.WorkingDirectory = "X:\\payne\\tools";

                pExe.StartInfo.EnvironmentVariables["RS_TOOLSROOT"] = "X:\\payne\\tools";
                pExe.StartInfo.EnvironmentVariables["RS_PROJROOT"] = "X:\\payne";
                pExe.StartInfo.EnvironmentVariables["RS_PROJECT"] = "payne";
                pExe.StartInfo.EnvironmentVariables["RS_BUILDBRANCH"] = "X:\\payne\\build\\dev";
                pExe.StartInfo.EnvironmentVariables["RS_CODEBRANCH"] = "X:\\payne\\src\\dev";
                pExe.StartInfo.EnvironmentVariables["RUBYLIB"] = "X:\\payne\\tools\\lib";

                string[] pathVars = pExe.StartInfo.EnvironmentVariables["PATH"].Split(';');
                pathVars[pathVars.Length - 2] = "X:\\payne\\tools\\bin";
                pathVars[pathVars.Length - 1] = "X:\\payne\\tools\\bin\\ruby\\bin";

                string fullPathString = "";
                for (int pathIndex = 0; pathIndex < pathVars.Length; ++pathIndex)
                {
                    fullPathString += pathVars[pathIndex];

                    if (pathIndex < pathVars.Length - 1)
                    {
                        fullPathString += ";";
                    }
                }
                pExe.StartInfo.EnvironmentVariables["PATH"] = fullPathString;
            }
            else
                pExe.StartInfo.WorkingDirectory = Environment.GetEnvironmentVariable("RS_TOOLSROOT");

            RegisterProcess(pExe);
            pExe.Start();

            int timeout = 2 * 60 * 60 * 1000; //2 hour timeout.
            pExe.WaitForExit(timeout);

            if (pExe.HasExited == false)
            {
                pExe.Kill();
                LogError("The cutscene building process has timed out.");
            }

            string cutsceneLog = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "logs\\cutscene\\" + cutsceneName + "_cutscene.log");
            if (MP3Hack == true)
            {
                cutsceneLog = @"X:\payne\tools\logs\cutscene\" + cutsceneName + "_cutscene.log";
            }

            if (File.Exists(cutsceneLog) == true)
            {
                string[] lines = File.ReadAllLines(cutsceneLog);
                foreach (string line in lines)
                {
                    Result.LogMessage(line);
                }
            }
            else
            {
                LogWarning("The log " + cutsceneLog + " could not be found.");
            }

            if (pExe.ExitCode != 0)
            {
                LogError("Unable to build cutscenes with command line: " + pExe.StartInfo.FileName + " " + pExe.StartInfo.Arguments);
                Result.Success = false;
                return Result.Success;
            }

            Result.Success = true;
            return Result.Success;
        }
    }
}
