﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Win32;
using RSG.Base.Command;

namespace AutomatedBuildShared
{
    public class ConfigurationTask : BuildTask
    {
        [XmlAttribute("File")]
        public string m_ConfigurationFile;

        [XmlAttribute("Branch")]
        public string m_Branch;
 
        public ConfigurationTask()
        {
            Name = "Configuration";
            m_ConfigurationFile = null;
        }

        public override bool Execute()
        {
            if (File.Exists(m_ConfigurationFile) == false)
            {
                Result.Success = false;
                Result.LogError("Unable to find configuration file: " + m_ConfigurationFile + ".");
                return Result.Success;
            }

            Process configProcess = new Process();
            configProcess.StartInfo.FileName = m_ConfigurationFile;
            configProcess.StartInfo.Arguments = "-quickinstall";

            if (m_Branch != null)
            {
                configProcess.StartInfo.Arguments += " -branch " + m_Branch;
            }

            RegisterProcess(configProcess);
            configProcess.Start();
            configProcess.WaitForExit();

            RegistryKey keys = Registry.CurrentUser.OpenSubKey("Environment");
            foreach (String envVar in keys.GetValueNames())
            {
                if (String.Compare(envVar, "PATH", true) != 0)  //HACK:  Don't change the PATH variable for the moment.  Need to figure out how to handle the change.
                {
                    if (keys.GetValueKind(envVar) == RegistryValueKind.String)
                    {
                        string registryValue = keys.GetValue(envVar) as string;
                        Environment.SetEnvironmentVariable(envVar, registryValue);
                    }
                }
            }

            if ( configProcess.ExitCode == 0 )
            {
                Result.Success = true;
            }
            else
            {
                Result.Success = false;
            }
            
            return Result.Success;
        }

        public override bool Localize()
        {
            m_ConfigurationFile = Environment.ExpandEnvironmentVariables(m_ConfigurationFile);
            return true;
        }

        public override string ToReportName()
        {
            return "Configure-Environment";
        }

        public override string ToShortString()
        {
            return "Configuring Environment";
        }

        public override string ToString()
        {
            return "Configuring environment using " + m_ConfigurationFile;
        }
    }
}
