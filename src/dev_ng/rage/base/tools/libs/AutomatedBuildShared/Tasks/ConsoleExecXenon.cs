using System;
using System.Diagnostics;
using System.Threading;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Serialization;
using XDevkit;

namespace AutomatedBuildShared
{
    //-------------------------------------------------------------------------
    // Name: DebugManager
    // Desc: The DebugManager class is the main application class for
    //       C# xbWatson. It connects to the default Xbox, waits
    //       for notifications, and then handles those notifications.
    //-------------------------------------------------------------------------
    public class ConsoleExecXenon : BuildTask
    {
        // m_xManager gives you access to one or more devkits, including letting7
        // you add to the list of available consoles.
        private static XboxManagerClass m_xManager = new XboxManagerClass();
        private XboxConsole m_xboxConsole;
        private IXboxDebugTarget m_xDebugTarget;
        private XboxEvents_OnStdNotifyEventHandler m_onStdNotify;

        private bool m_running;
        private bool m_exited;
        private bool m_crashed;

        [XmlAttribute("Name")]
        public string TestName;

        [XmlAttribute("Command")]
        public string Command;

        [XmlAttribute("Arguments")]
        public string Arguments;

        [XmlAttribute("Target")]
        public string Target;

        [XmlAttribute("FailOnError")]
        public bool FailOnError = false;

        [XmlAttribute("ColdReboot")]
        public bool ColdReboot = false;

        [XmlAttribute("SuccessMessage")]
        public string SuccessMessage;
      
        [XmlAttribute("Timeout")]
        public int m_timeout = 1000; //milliseconds 

        private int m_CurrentCount; 

        [XmlAttribute("Count")]
        public int m_Count;

        private string m_HungMessage = "appears to be hung";
        private int m_HungMessageCount;
        private int m_HungMessageCurrentCount;

        //---------------------------------------------------------------------
        // Name: Constructor
        //---------------------------------------------------------------------
        public ConsoleExecXenon()
        {
            TestName = "";
            m_exited = false;
            m_running = false;
            m_crashed = false;
            Target = null;
            Command = null;
            Arguments = null;
            m_CurrentCount = 0;
            m_Count = 1;
            m_HungMessageCount = 1000;
            m_HungMessageCurrentCount = 0;

            m_onStdNotify = new XboxEvents_OnStdNotifyEventHandler(xboxConsole_OnStdNotify);
        }

        private void OnAssertionFailed(IXboxEventInfo eventInfo)
        {
            // Title.
            string title = string.Format("Assertion Failed - {0}", m_xboxConsole.Name);

            // Overview.
            StringBuilder overview = new StringBuilder();
            overview.AppendFormat("An assertion failed on thread 0x{0:X8} \"{1}\".",
                eventInfo.Info.Thread.ThreadId,
                eventInfo.Info.Thread.ThreadInfo.Name);

            // Details.
            StringBuilder details = new StringBuilder();
            details.AppendFormat("{0}\n\n", overview);
            details.AppendFormat("{0}\n", eventInfo.Info.Message);

            Log(details.ToString());
        }

        /// <summary>
        /// Handle the Exception (DM_EXCEPTION) notification.
        /// </summary>
        private void OnException(IXboxEventInfo eventInfo)
        {
            // Title.
            string title = string.Format("Exception - {0}", m_xboxConsole.Name);

            // Overview.
            StringBuilder overview = new StringBuilder();
            overview.AppendFormat("An exception occurred on thread 0x{0:X8} \"{1}\".",
                eventInfo.Info.Thread.ThreadId,
                eventInfo.Info.Thread.ThreadInfo.Name);

            // Details.
            StringBuilder details = new StringBuilder();
            details.AppendFormat("{0}\n\n", overview);
            details.AppendFormat(" Exception: 0x{0:X8}\n", eventInfo.Info.Code);
            details.AppendFormat("   Address: 0x{0:X8}\n", eventInfo.Info.Address);

            StringBuilder parameters = new StringBuilder();
            for (uint index = 0; index < eventInfo.Info.ParameterCount; ++index)
                parameters.AppendFormat("0x{0:X8} ", eventInfo.Info.Parameters[index]);
            details.AppendFormat("Parameters: {0}\n", parameters.ToString());

            switch (eventInfo.Info.Code)
            {
                case 0xc0000005: // STATUS_ACCESS_VIOLATION
                    {
                        bool isReading = eventInfo.Info.Parameters[0] == 0;
                        uint address = eventInfo.Info.Parameters[1];

                        details.AppendFormat("\nAccess violation {0} memory at 0x{1:X8}.\n",
                            isReading ? "reading" : "writing", address);
                    }
                    break;
            }

            Log(details.ToString());
        }

        /// <summary>
        /// Handle the RIP (DM_RIP) notification.
        /// </summary>
        private void OnRIP(IXboxEventInfo eventInfo)
        {
            // Title.
            string title = string.Format("Fatal Error (RIP) - {0}", m_xboxConsole.Name);

            // Overview.
            StringBuilder overview = new StringBuilder();
            overview.AppendFormat("A fatal error (RIP) occurred on thread 0x{0:X8} \"{1}\".",
                eventInfo.Info.Thread.ThreadId,
                eventInfo.Info.Thread.ThreadInfo.Name);

            // Details.
            StringBuilder details = new StringBuilder();
            details.AppendFormat("{0}\n\n", overview);
            details.AppendFormat("RIP: {0}\n", eventInfo.Info.Message);

            Log(details.ToString());
        }

        /// <summary>
        /// Handle the ExecStateChange (DM_EXEC) notification.
        /// </summary>
        private void OnExecStateChange(IXboxEventInfo eventInfo)
        {
            // Update status and current running process.
            switch (eventInfo.Info.ExecState)
            {
                case XboxExecutionState.Pending:
                case XboxExecutionState.PendingTitle:
                    {
                        Log("State change: Pending.");
                        break;
                    }

                case XboxExecutionState.Rebooting:
                case XboxExecutionState.RebootingTitle:
                    {
                        Log("State change: Rebooting.");
                        break;
                    }

                case XboxExecutionState.Running:
                    {
                        Log("State change: Running.");
                        m_running = true;
                        break;
                    }

                case XboxExecutionState.Stopped:
                    {
                        Log("State change: Stopped.");
                        m_exited = true;
                        m_running = false;
                        break;
                    }
            }

            // Watch for reboots.
            if (eventInfo.Info.ExecState == XboxExecutionState.Rebooting ||
                eventInfo.Info.ExecState == XboxExecutionState.RebootingTitle)
            {
                Log("Console is rebooting...");
            }
        }

        //---------------------------------------------------------------------
        // Name: xboxConsole_OnStdNotify
        // Desc: Callback for DmNotify style notifications
        //---------------------------------------------------------------------
        private void xboxConsole_OnStdNotify(XboxDebugEventType eventCode, IXboxEventInfo eventInfo)
        {
            switch (eventCode)
            {
                case XboxDebugEventType.ExecStateChange: // DM_EXEC 
                    {
                        OnExecStateChange(eventInfo);
                        break;
                    }

                case XboxDebugEventType.DebugString: // DM_DEBUGSTR
                    {
                        // Log the message.
                        string debugMessage = eventInfo.Info.Message.TrimEnd("\n".ToCharArray());
                        if (!string.IsNullOrEmpty(debugMessage))
                        {
                            string[] messages = debugMessage.Split('\n');
                            foreach (string message in messages)
                            {
                                Log(message);

                                if (message.Contains(SuccessMessage) == true)
                                {
                                    m_CurrentCount++;

                                    Log("Success Message has been received! Count: " + m_CurrentCount);

                                    if (m_CurrentCount >= m_Count)
                                    {
                                        Log("Signaling an exit.");
                                        m_exited = true;
                                        Result.Success = true;
                                    }
                                }
                                else if ( message.Contains(m_HungMessage) == true )
                                {
                                    m_HungMessageCurrentCount++;

                                    if (m_HungMessageCurrentCount >= m_HungMessageCount)
                                    {
                                        Log("Signaling an exit.  The program is hanging.");
                                        m_exited = true;
                                        Result.Success = true;
                                    }
                                }
                            }
                        }

                        ContinueExecution(eventInfo);
                    }
                    break;

                case XboxDebugEventType.AssertionFailed: // DM_ASSERT
                    OnAssertionFailed(eventInfo);
                    break;

                case XboxDebugEventType.Exception: // DM_EXCEPTION
                    {
                        // Only handle first-chance exceptions.
                        if (eventInfo.Info.Flags != XboxExceptionFlags.FirstChance)
                            break;

                        // Ignore exception caused by SetThreadName.
                        if (eventInfo.Info.Code == 0x406D1388)
                            break;

                        OnException(eventInfo);

                        Log("Exception encountered.  The program has crashed.");
                        m_crashed = true;
                    }
                    break;

                case XboxDebugEventType.RIP: // DM_RIP
                    OnRIP(eventInfo);
                    break;
            }

            // NOTE: The .NET CLR garbage collector does not clean up
            // these COM objects as soon as we would like.

            // Clean up COM objects.
            XBOX_EVENT_INFO info = eventInfo.Info;

            FullyReleaseComObject(eventInfo);

            if (info.Module != null)
                FullyReleaseComObject(info.Module);

            if (info.Section != null)
                FullyReleaseComObject(info.Section);

            if (info.Thread != null)
                FullyReleaseComObject(info.Thread);
             
        }

        private bool Copy(string xbcpExecutable, string xboxTarget, string sourceFile)
        {
            string xboxDestFolder = "xe:\\game_automated\\";

            ProcessStartInfo startInfo = new ProcessStartInfo("\"" + xbcpExecutable + "\"", "/F /T /Y /X:" + xboxTarget + " " + sourceFile + " " + xboxDestFolder);

            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;

            Process p = Process.Start(startInfo);
            if (p != null)
            {
                p.WaitForExit();
            }

            return true;
        }

        private bool StartProcess(ProcessStartInfo psInfo, string xboxTarget)
        {
            Process p = Process.Start(psInfo);
            RegisterProcess(p);
            if (p != null)
            {
                while (!p.HasExited)
                {
                    // do nothing
                }
            }

            int retryCount = 0;
            int retryAttempts = 10;
            Exception lastException = null;
            while (retryCount < retryAttempts)
            {
                try
                {
                    // Hook up Notification Channel - tell the Xbox devkit to send
                    // notifications to us. The set of notifications is documented under
                    // DmNotify in the help file.
                    m_xboxConsole = m_xManager.OpenConsole(xboxTarget);
                    m_xboxConsole.OnStdNotify += m_onStdNotify;
                    m_xDebugTarget = m_xboxConsole.DebugTarget;
                    m_xDebugTarget.ConnectAsDebugger("BuildClient", XboxDebugConnectFlags.Force);
                    break;
                }
                catch (Exception e)
                {
                    // Display an error message if connecting fails. Translating
                    // error codes to text is not currently supported.
                    lastException = e;
                    retryCount++;
                }
            }

            if (retryCount == retryAttempts)
            {
                m_xboxConsole.OnStdNotify -= m_onStdNotify;
                LogError("Could not connect to Xbox\n " + lastException);
                Result.Success = false;
                return Result.Success;
            }

            return true;
        }

        public override bool Execute()
        {
            Result.Success = false;

            // Open the default Xbox. This will fail if there is no default
            // Xbox or if it is unreachable for any reason.
            if (Target != null && String.IsNullOrWhiteSpace(Target) == true)
            {
                Result.LogError("The target console specified is not valid.");
                return Result.Success;
            }

            if (Target == null)
            {
                try
                {
                    if (m_xManager.DefaultConsole == null)
                    {
                        Result.LogError("A default Xbox 360 console is not set.");
                        return Result.Success;
                    }
                }
                catch (System.Runtime.InteropServices.COMException e)
                {
                    Result.LogError("A default Xbox 360 console is not set.  " + e.Message);
                    return Result.Success; 
                }   
            }

            if ( Environment.GetEnvironmentVariable("XEDK") == null )
            {
                Result.LogError("The XEDK environment variable was not found on this build client.");
                return Result.Success;
            }

            //Copy the executable to the Xbox 360.
            string xbcpExecutable = Path.Combine(Environment.GetEnvironmentVariable("XEDK"), "bin\\win32\\xbcp.exe");
            if ( File.Exists(xbcpExecutable) == false )
            {
                Result.LogError(xbcpExecutable + " does not exist.  Unable to deploy executables to Xbox 360.");
                return Result.Success;
            }
            
            Target = Target == null ? m_xManager.DefaultConsole : Target;

            Copy(xbcpExecutable, Target, this.Command);
            string cmpFile = Path.Combine(Path.GetDirectoryName(this.Command), Path.GetFileNameWithoutExtension(this.Command) + ".cmp");
            Copy(xbcpExecutable, Target, cmpFile);

            string rfsDataFile = Path.Combine(Environment.GetEnvironmentVariable("TEMP"), "rfs.dat");
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());

            foreach (IPAddress localIP in localIPs)
            {
                if (localIP.IsIPv6LinkLocal == false)
                {
                    string ipAddress = localIP.ToString();

                    StreamWriter writer = new StreamWriter(rfsDataFile);
                    writer.WriteLine(ipAddress);
                    writer.Close();
                    break;
                }
            }

            Copy(xbcpExecutable, Target, rfsDataFile);

            //Execute the SysTrayRfs application if it is not online.
            Process[] sysTrayProcesses = Process.GetProcessesByName("SysTrayRfs");
            if (sysTrayProcesses.Length == 0)
            {
                string sysTrayExecutable = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "bin\\SysTrayRfs.exe");
                if ( File.Exists(sysTrayExecutable) == true )
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo(sysTrayExecutable, "-trusted");
                    Process.Start(startInfo);
                }
            }
            
            string xboxCommandFile = Path.Combine("xe:\\game_automated", Path.GetFileName(this.Command));
            
            try
            {
                // Get the Xbox we are talking to
                m_running = false;
                m_exited = false;

                ProcessStartInfo psInfo = new ProcessStartInfo( "\"" + Environment.GetEnvironmentVariable( "XEDK" ) + "\\bin\\win32\\xbreboot.exe\"",
                    (!String.IsNullOrEmpty(this.Target) ? "/X " + this.Target + " " : string.Empty) + "/w " + xboxCommandFile + " " + this.Arguments );

                string outputString = "Executing Xbox 360 task ";
                if ( String.IsNullOrEmpty(this.Target) == true )
                    outputString += " on " + this.Target;
                else
                    outputString += " on the default console ";

                outputString += " with " + psInfo.FileName + " " + psInfo.Arguments + ".";

                psInfo.CreateNoWindow = true;
                psInfo.UseShellExecute = false;

                StartProcess(psInfo, Target);

                Log("Console - " + m_xboxConsole.Name);
                Log("Starting Command: " + this.Command);

                while (!this.m_running)
                {
                    if (this.m_exited == true)
                    {
                        Log("Console exited before getting into a running state.");
                        this.m_exited = false;

                        psInfo = new ProcessStartInfo("\"" + Environment.GetEnvironmentVariable("XEDK") + "\\bin\\win32\\xbreboot.exe\"",
                            (!String.IsNullOrEmpty(this.Target) ? "/C /X " + this.Target + " " : string.Empty) + "/w " + xboxCommandFile + " " + this.Arguments);

                        StartProcess(psInfo, Target);
                    }

                    Thread.Sleep(100);
                }

                // could call script to reboot to dashboard
                while (!this.m_exited)
                {
                    Thread.Sleep(100);
                }
            }
            catch (COMException e)
            {
                LogCOMException(e);
            }
            catch (NullReferenceException)
            {
                Result.Success = false;
                return Result.Success;
            }

            RebootConsole();

            if (m_xDebugTarget != null)
            {
                m_xDebugTarget.DisconnectAsDebugger();
                FullyReleaseComObject(m_xDebugTarget);
                m_xDebugTarget = null;
            }

            if (m_xboxConsole != null)
            {
                m_xboxConsole.OnStdNotify -= m_onStdNotify;
                FullyReleaseComObject(m_xboxConsole);
                m_xboxConsole = null;
            }

            Thread.Sleep(m_timeout);

            Log( "Done " );

            if ( this.FailOnError )
            {
                return Result.ErrorCount == 0;
            }
            else
            {
                return Result.Success;
            }
        }

        /// <summary>
        /// Reboots the Xbox 360 console.
        /// </summary>
        public void RebootConsole()
        {
            try
            {
                Log("Rebooting...");
                if (m_crashed == true) 
                    ColdReboot = true;

                m_xboxConsole.Reboot(null, null, null, ColdReboot ? XboxRebootFlags.Cold : XboxRebootFlags.Title);

                if (ColdReboot == true)
                {
                    Thread.Sleep(20 * 1000);  //Sleep for 20 seconds so the Xbox 360 can refresh.
                }
            }
            catch (COMException e)
            {
                LogCOMException(e);
            }
            catch (NullReferenceException) { }
        }

        /// <summary>
        /// Force a COM object's reference count to zero.
        /// </summary>
        private static void FullyReleaseComObject(object o)
        {
            int refcount;
            do
            {
                // TODO: "RaceOnRCWCleanup was detected"
                refcount = Marshal.ReleaseComObject(o);
            } while (refcount > 0);
        }

        /// <summary>
        /// Continues execution of the Xbox 360 console.
        /// </summary>
        private void ContinueExecution(IXboxEventInfo eventInfo)
        {
            try
            {
                bool isStopped = (eventInfo.Info.IsThreadStopped != 0);

                if (isStopped)
                {
                    eventInfo.Info.Thread.Continue(true);

                    bool notStopped;
                    m_xDebugTarget.Go(out notStopped);
                }
            }
            catch (COMException e)
            {
                LogCOMException(e);
            }
        }

        /// <summary>
        /// Write the COM exception to the log window.
        /// </summary>
        private void LogCOMException(COMException e)
        {
            // Generate the log message.
            StringBuilder message = new StringBuilder();
            message.Append("Error: ");

            string translated = m_xManager.TranslateError(e.ErrorCode);
            if (!string.IsNullOrEmpty(translated))
                message.Append(translated);
            else
                message.Append("Unknown exception");

            message.AppendFormat(" (HRESULT = 0x{0:X8})", e.ErrorCode);

            // Update UI.
            LogError(message.ToString());
        }

        public override string ToReportName()
        {
            string safeTestName = TestName.Replace(" ", "_");
            return "Xbox_360_" + safeTestName;
        }

        public override string ToDisplayName()
        {
            return "360 " + TestName;
        }

        public override string ToShortString()
        {
            return ToDisplayName();
        }

        public override string ToString()
        {
            return "Xbox 360 Test " + TestName + ".";
            
        }
    }
}