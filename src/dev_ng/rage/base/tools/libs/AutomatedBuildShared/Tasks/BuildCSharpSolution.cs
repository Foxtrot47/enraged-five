﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomatedBuildShared
{
    [Serializable]
    public class CSharpSolution : BuildSolution
    {
        public CSharpSolution()
        {
            Name = "Build C# Solution";
            CompileWithDevenv = true;
        }
    }

    [Serializable]
    public class MakefileSolution : BuildSolution
    {
        public MakefileSolution()
        {
            Name = "Makefile Solution";
            CompileWithDevenv = true;
            NoVSI = true;
        }
    }
}
