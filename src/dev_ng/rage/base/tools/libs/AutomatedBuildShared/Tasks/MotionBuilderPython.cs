﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using Microsoft.Win32;

using RSG.Base.ConfigParser;
using P4API;


namespace AutomatedBuildShared
{
    public class MotionBuilderPython : BuildTask
    {
        [XmlAttribute("Version")]
        public MotionBuilderVersion Version;

        [XmlAttribute("FbxFile")]
        public string FbxFile;

        [XmlAttribute("MP3Hack")]
        public bool MP3Hack;

        [XmlAttribute("PythonScript")]
        public string PythonScript;

        private static string MotionBuilderProcessName = "motionbuilder";

        public MotionBuilderPython()
        {
            Version = MotionBuilderVersion.MotionBuilderNone;
            FbxFile = null;
            MP3Hack = false;
        }

        private static readonly Dictionary<MotionBuilderVersion, String> VERSION_NAMES = new Dictionary<MotionBuilderVersion, String>() 
	    {
            {MotionBuilderVersion.MotionBuilderNone, "None"},
		    {MotionBuilderVersion.MotionBuilder2010, "motionbuilder2010"},
		    {MotionBuilderVersion.MotionBuilder2012, "motionbuilder2012"}
	    };

        /// <summary>
        /// A failsafe function for writing the plug-in path for the current project or
        /// recovering the plugin path that may have been lost from a crash or early exit.  
        /// Prevents exporting from the wrong plug-ins and MotionBuilder hanging because 
        /// Python crashed trying to call into plug-ins that aren't loaded.
        /// </summary>
        /// <param name="configFile"></param>
        /// <param name="pluginsPath"></param>
        /// <returns></returns>
        public bool WriteSDKPluginsPath(string configFile, string pluginsPath)
        {
            //Find the MotionBuilder application configuration file, re-write the plug-ins path for the current project.
            string AdditionalPluginsIdentifier = "[AdditionnalPluginPath]";  //NOTE:  The typo in Additionnal is intentional -- this is what MotionBuilder uses...
            string CountIdentifier = "Count = 1";
            string PlugInPathIdentifier = "PlugInPath0 = ";
            string FileVersionIdentifier = "Config File Version";

            try
            {
                string[] configFileContents = File.ReadAllLines(configFile);
                for (int lineIndex = 0; lineIndex < configFileContents.Length; lineIndex++)
                {
                    if (configFileContents[lineIndex].StartsWith(AdditionalPluginsIdentifier) == true)
                    {
                        configFileContents[lineIndex] = null;
                        continue;
                    }

                    if (configFileContents[lineIndex].StartsWith(CountIdentifier) == true)
                    {
                        configFileContents[lineIndex] = null;
                        continue;
                    }

                    if (configFileContents[lineIndex].StartsWith(PlugInPathIdentifier) == true)
                    {
                        configFileContents[lineIndex] = null;
                        continue;
                    }
                }

                StreamWriter writer = new StreamWriter(configFile);

                for (int lineIndex = 0; lineIndex < configFileContents.Length; lineIndex++)
                {
                    writer.WriteLine(configFileContents[lineIndex]);

                    if (configFileContents[lineIndex] != null && configFileContents[lineIndex].StartsWith(FileVersionIdentifier) == true)
                    {
                        writer.WriteLine("");
                        writer.WriteLine(AdditionalPluginsIdentifier);
                        writer.WriteLine(CountIdentifier);
                        writer.WriteLine(PlugInPathIdentifier + pluginsPath);
                    }
                }

                writer.Close();

            }
            catch (Exception e)
            {
                Result.LogError("Unable to write plug-ins path to configuration file. " + e.Message);
                return false;
            }

            return true;
        }

        public bool CreateAndExecutePythonScripts()
        {
            string strMotionbuilderPath = "";

            ConfigCoreView cp = new ConfigCoreView();
            int amount = cp.GetDCCPackageCount("motionbuilder");
            /*if (amount == 0)
            {
                string installBatPath = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "install.bat");
                LogError("Unable to find an installed version of MotionBuilder. Unable to continue...");
                return false;
            }
            */

            //TODO:  Find out what happened to the motionbuilder configuration in the installer.

            string strInstallDir = @"C:\Program Files\Autodesk\MotionBuilder 2010\bin\x64\";
            if (Version == MotionBuilderVersion.MotionBuilder2012)
            {
                strInstallDir = @"C:\Program Files\Autodesk\MotionBuilder 2012 (64-bit)\bin\x64\";
            }

            for (int i = 0; i < amount; ++i)
            {
                string strVersion = cp.GetDCCPackageVersion("motionbuilder", i);

                if (String.Compare(strVersion, VERSION_NAMES[Version], true) == 0)
                {
                    strInstallDir = cp.GetDCCPackageInstallDir("motionbuilder", i);
                    break;
                }
            }

            if (String.IsNullOrWhiteSpace(strInstallDir) == true)
            {
                LogError("Unable to find MotionBuilder version " + Version.ToString() + ".");
                return false;
            }

            if (Directory.Exists(strInstallDir) == true)
            {
                string[] filePaths = Directory.GetFiles(strInstallDir, "*motionbuilder.exe", SearchOption.AllDirectories);
                if (filePaths.Length == 0)
                {
                    LogError("The MotionBuilder executable: " + strMotionbuilderPath + " does not exist.  Unable to continue.  Please re-execute the batch and select an installed version of MotionBuilder.");
                    return false;
                }

                strMotionbuilderPath = filePaths[0];

                if (!String.IsNullOrWhiteSpace(strMotionbuilderPath))
                {
                    try
                    {
                        //HACK:  MotionBuilder, 64-bit support only.  Need to convert this properly when
                        //we're no longer focused on Max Payne.
                        string configFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"Autodesk\MB2010\config", Environment.MachineName + ".Application.txt");
                        if (Version == MotionBuilderVersion.MotionBuilder2012)
                        {
                            configFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"MB\2012-x64\config", Environment.MachineName + ".Application.txt");
                        }

                        string pluginsPath = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
                        if (MP3Hack == true)
                            pluginsPath = "X:\\payne\\tools";

                        if (Version == MotionBuilderVersion.MotionBuilder2010)
                        {
                            pluginsPath = Path.Combine(pluginsPath, @"dcc\current\motionbuilder2010\plugins\x64");
                        }
                        else
                        {
                            pluginsPath = Path.Combine(pluginsPath, @"dcc\current\motionbuilder2012\plugins\x64");
                        }

                        WriteSDKPluginsPath(configFile, pluginsPath);

                        string logDirectory = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "logs");
                        if (MP3Hack == true)
                        {
                            logDirectory = Path.Combine("X:\\payne\\tools\\logs");
                        }

                        string[] logFiles = Directory.GetFiles(logDirectory, "*.log");
                        foreach (string logFile in logFiles)
                        {
                            try
                            {
                                File.Delete(logFile);
                            }
                            catch (Exception) { }
                        }

                        System.Diagnostics.Process pExe = new System.Diagnostics.Process();
                        pExe.StartInfo.FileName = strMotionbuilderPath;
                        pExe.StartInfo.Arguments = "-suspendMessages " + PythonScript;

                        if (MP3Hack == true)
                            pExe.StartInfo.WorkingDirectory = "X:\\payne\\tools";
                        else
                            pExe.StartInfo.WorkingDirectory = Environment.GetEnvironmentVariable("RS_TOOLSROOT");

                        pExe.StartInfo.CreateNoWindow = true;
                        pExe.StartInfo.UseShellExecute = false;

                        pExe.StartInfo.EnvironmentVariables["FBX_FILE"] = FbxFile;

                        //HACK:  This is a hack to have RDR3's Automated Build run on the MP3 branch.
                        //This is done because the MP3 installer does not have command line access to run
                        //and more importantly, adding to it is undesirable.
                        if (MP3Hack == true)
                        {
                            pExe.StartInfo.EnvironmentVariables["RS_TOOLSROOT"] = "X:\\payne\\tools";
                            pExe.StartInfo.EnvironmentVariables["RS_PROJROOT"] = "X:\\payne";
                            pExe.StartInfo.EnvironmentVariables["RS_PROJECT"] = "payne";
                            pExe.StartInfo.EnvironmentVariables["RS_BUILDBRANCH"] = "X:\\payne\\build\\dev";
                            pExe.StartInfo.EnvironmentVariables["RS_CODEBRANCH"] = "X:\\payne\\src\\dev";
                            pExe.StartInfo.EnvironmentVariables["RUBYLIB"] = "X:\\payne\\tools\\lib";

                            string[] pathVars = pExe.StartInfo.EnvironmentVariables["PATH"].Split(';');
                            pathVars[pathVars.Length - 2] = "X:\\payne\\tools\\bin";
                            pathVars[pathVars.Length - 1] = "X:\\payne\\tools\\bin\\ruby\\bin";

                            string fullPathString = "";
                            for (int pathIndex = 0; pathIndex < pathVars.Length; ++pathIndex)
                            {
                                fullPathString += pathVars[pathIndex];

                                if (pathIndex < pathVars.Length - 1)
                                {
                                    fullPathString += ";";
                                }
                            }
                            pExe.StartInfo.EnvironmentVariables["PATH"] = fullPathString;
                        }

                        RegisterProcess(pExe);
                        pExe.Start();

                        int timeout = 1000 * 60 * 60; //60 minutes.
                        pExe.WaitForExit(timeout);

                        if (pExe.HasExited == false)
                        {
                            LogError("The exported scene has timed out.");
                            Result.Success = false;
                            pExe.Kill();
                        }

                        //Process the outputted logs.
                        logFiles = Directory.GetFiles(logDirectory, "*.log");

                        foreach (string logFile in logFiles)
                        {
                            Result.LogMessage("Log Contents from " + logFile + ".");
                            Result.LogMessage("------------------------------------------------------------");
                            string[] logContents = File.ReadAllLines(logFile);

                            foreach (string logContent in logContents)
                            {
                                Result.LogMessage(logContent);
                            }

                            Result.LogMessage("\n");
                        }
                    }
                    catch (Exception e)
                    {
                        LogError(e.Message);
                        LogError("Unable to execute Python script " + PythonScript + ".");
                    }
                }
            }
            else
            {
                LogError("Unable to find MotionBuilder installation directory: " + strMotionbuilderPath + ".");
                return false;
            }

            return true;
        }

        public override bool Execute()
        {
            if (String.IsNullOrWhiteSpace(FbxFile) == true)
            {
                LogError("No FBX file was specified.  Unable to continue...");
                Result.Success = false;
                return false;
            }

            if (File.Exists(FbxFile) == false)
            {
                LogError("The FBX file " + FbxFile + " was not found.");
                Result.Success = false;
                return false;
            }

            if (File.Exists(PythonScript) == false)
            {
                LogError("The Python script " + PythonScript + " was not found.");
                Result.Success = false;
                return false;
            }

            if (CreateAndExecutePythonScripts() == false)
            {
                Result.Success = false;
                return false;
            }

            return true;
        }

        public override string ToReportName()
        {
            return "MotionBuilderPython_" + Path.GetFileName(Path.GetDirectoryName(this.PythonScript));
        }

        public override string ToShortString()
        {
            return Path.GetFileName(Path.GetDirectoryName(this.PythonScript));
        }

        public override string ToString()
        {
            return "Exporting from MotionBuilder with " + PythonScript + " on script " + FbxFile;
        }
    }

}
