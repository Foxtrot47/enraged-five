﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using P4API;
using RSG.SourceControl.Perforce;
using RSG.Base.Command;

namespace AutomatedBuildShared
{
    [Serializable]
    public class BuildTask
    {
        public static int InvalidID = -1;

        [XmlIgnoreAttribute] 
        public string Name { get; set; }

        [XmlArrayItem("ID")]
        [XmlArray("IDs")]
        public List<int> ID;

        public TaskResult Result { get; set; }
        public PerforceInfo PerforceInfo { get; set; }
        public DateTime StartTime { get; set; }
        public double TimeElapsed { get; set; }

        [XmlIgnoreAttribute]
        public bool Dispatched { get; set; }

        [XmlIgnoreAttribute]
        public BuildClient TargetClient { get; set; }

        [XmlAttribute("Filter")]
        public string FilterFile;

        [XmlArray("EnvironmentVariables")]
        [XmlArrayItem("Variable")]
        public List<EnvVar> EnvVars = null;
        private List<EnvVar> PreviousEnvVars = null;

        [XmlArrayItem("File")]
        public List<string> SyncFiles = null;

        [XmlArrayItem("File")]
        public List<string> CheckOutFiles = null;

        [XmlArrayItem("File")]
        public List<string> RemoveFiles = null;

        [XmlArrayItem("File")]
        public List<string> ReopenFiles = null;

        [XmlArrayItem("File")]
        public List<string> ReconcileFiles = null;

        [XmlArrayItem("File")]
        public List<string> DeleteFiles = null;

        [XmlArrayItem("File")]
        public List<string> SubmitFiles = null;

        [XmlIgnoreAttribute]
        private UInt32 m_HashCRC;
        public UInt32 HashCRC
        {
            get
            {
                if (m_HashCRC == UInt32.MaxValue)
                {
                    Crc32 crc = new Crc32();
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    byte[] bytes = encoding.GetBytes(this.ToString());
                    byte[] crcResult = crc.ComputeHash(bytes);
                    m_HashCRC = BitConverter.ToUInt32(crcResult, 0);
                }

                return m_HashCRC;
            }
        }

        [XmlIgnore]
        protected static List<Process> m_RegisteredProcesses = new List<Process>();

        [XmlIgnore]
        protected static List<rageExecuteCommand> m_RegisteredRageProcesses = new List<rageExecuteCommand>();

        public static List<Type> RegisteredTasks;
        public static List<Type> InitializationTasks;
        public static List<Type> NormalTasks;
        public static List<Type> ProcessTasks; //A list of tasks executed only by the Automated Build.
        
        public static void RegisterTasks()
        {
            InitializationTasks = new List<Type>();
            InitializationTasks.Add(typeof(PerforceInitializationTask));
            InitializationTasks.Add(typeof(PerforceSynchronization));
            InitializationTasks.Add(typeof(PerforceCleanupTask));
            InitializationTasks.Add(typeof(ConfigurationTask));

            NormalTasks = new List<Type>();
            NormalTasks.Add(typeof(BuildSolution));
            NormalTasks.Add(typeof(CSharpSolution));
            NormalTasks.Add(typeof(MakefileSolution));
            NormalTasks.Add(typeof(ConsoleExecPS3));
            NormalTasks.Add(typeof(ConsoleExecXenon));
            NormalTasks.Add(typeof(StudioMax));
            NormalTasks.Add(typeof(MotionBuilderCutscene));
            NormalTasks.Add(typeof(MotionBuilderPython));
            NormalTasks.Add(typeof(CutsceneConcatenation));
            NormalTasks.Add(typeof(CutsceneLightingExport));
            NormalTasks.Add(typeof(CutsceneMerge));
            NormalTasks.Add(typeof(Command));
            NormalTasks.Add(typeof(BuildAssets));

            ProcessTasks = new List<Type>();
            ProcessTasks.Add(typeof(ClientStatus));
            ProcessTasks.Add(typeof(DisconnectTask));
            ProcessTasks.Add(typeof(ShelveChangelistTask));
            ProcessTasks.Add(typeof(SubmitChangelistTask));

            RegisteredTasks = new List<Type>();
            RegisteredTasks.AddRange(InitializationTasks);
            RegisteredTasks.AddRange(NormalTasks);
            RegisteredTasks.AddRange(ProcessTasks);
        }

        public static int GetTaskMessageIndex(Type taskType)
        {
            int index = 0;
            foreach (Type type in RegisteredTasks)
            {
                if (type == taskType)
                {
                    return index;
                }

                index++;
            }

            return -1;
        }

        public static Type GetTypeFromMessageIndex( int index )
        {
            return RegisteredTasks[index];
        }

        /// <summary>
        /// Synchronous read from the specified tcp client.
        /// </summary>
        /// <param name="m_Client"></param>
        /// <returns></returns>
        public static BuildTask ReadTask(TcpClient m_Client)
        {
            BuildTask task = null;

            try
            {
                int numBytes = m_Client.Available;
                byte[] messageType = new byte[sizeof(Int32)];
                m_Client.GetStream().Read(messageType, 0, messageType.Length);
                numBytes -= messageType.Length;
                int messageIndex = BitConverter.ToInt32(messageType, 0);

                byte[] messageSizeBuffer = new byte[sizeof(long)];
                m_Client.GetStream().Read(messageSizeBuffer, 0, messageSizeBuffer.Length);
                numBytes -= messageType.Length;
                int messageSize = BitConverter.ToInt32(messageSizeBuffer, 0);

                MemoryStream stream = new MemoryStream(messageSize);
                int bytesRead = 0; 
                while ( bytesRead != messageSize )
                {
                    int partialMessageSize = m_Client.Available;
                    byte[] buffer = new byte[partialMessageSize];
                    m_Client.GetStream().Read(buffer, 0, partialMessageSize);
                    stream.Write(buffer, 0, buffer.Length);
                    bytesRead += buffer.Length;
                }

                Type type = BuildTask.GetTypeFromMessageIndex(messageIndex);
                XmlSerializer serializer = new XmlSerializer(type);

                stream.Seek(0, SeekOrigin.Begin);
                XmlTextReader reader = new XmlTextReader(stream);
                reader.Normalization = false;  //Turn normalization off so any illegal characters are properly parsed.
                task = (BuildTask)serializer.Deserialize(reader);
                task.StartTime = DateTime.MinValue;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                if (e.InnerException != null)
                    Console.WriteLine(e.InnerException);
            }

            return task;
        }

        /// <summary>
        /// Synchronous writing of a task to specified TcpClient's stream.
        /// </summary>
        /// <param name="m_Client"></param>
        /// <param name="task"></param>
        public static void WriteTask(TcpClient m_Client, BuildTask task)
        {
            try
            {
                MemoryStream stream = new MemoryStream();
                Int32 messageEnumeration = BuildTask.GetTaskMessageIndex(task.GetType());
                byte[] messageBytes = BitConverter.GetBytes(messageEnumeration);
                stream.Write(messageBytes, 0, sizeof(Int32));

                MemoryStream objectStream = new MemoryStream();
                XmlSerializer serializer = new XmlSerializer(task.GetType());
                serializer.Serialize(objectStream, task);

                long messageSize = objectStream.Length;
                byte[] messageSizeBytes = BitConverter.GetBytes(messageSize);
                stream.Write(messageSizeBytes, 0, messageSizeBytes.Length);

                objectStream.WriteTo(stream);

                stream.WriteTo(m_Client.GetStream());
            }
            catch(Exception e)
            {
                //Connection had been terminated.
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Helper function for BuildTask so any complex sub-objects' contents are copied as well.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static BuildTask DeepClone(BuildTask obj) 
        { 
            using (var ms = new MemoryStream()) 
            { 
                var formatter = new BinaryFormatter(); 
                formatter.Serialize(ms, obj); 
                ms.Position = 0; 
                return (BuildTask) formatter.Deserialize(ms); 
            } 
        } 

        public BuildTask()
        {
            Name = "BuildTask";
            Result = new TaskResult();
            ID = new List<int>();

            Dispatched = false;

            TargetClient = null;
            m_HashCRC = UInt32.MaxValue;
            FilterFile = null;
            SyncFiles = new List<string>();
            CheckOutFiles = new List<string>();
            RemoveFiles = new List<string>();
            ReopenFiles = new List<string>();
            ReconcileFiles = new List<string>();
            DeleteFiles = new List<string>();
            SubmitFiles = new List<string>();

            TimeElapsed = 0.0;

            EnvVars = new List<EnvVar>();
            PreviousEnvVars = new List<EnvVar>();
        }

        /// <summary>
        /// Helper function to handle any expansion of environment variables or 
        /// translation of XML-based data into full paths, complete data, etc.
        /// </summary>
        /// <returns></returns>
        public virtual bool Localize()
        {
            for (int index = 0; index < SyncFiles.Count; ++index)
            {
                string file = SyncFiles[index];
                SyncFiles[index] = Environment.ExpandEnvironmentVariables(file);
            }

            for (int index = 0; index < CheckOutFiles.Count; ++index)
            {
                string file = CheckOutFiles[index];
                CheckOutFiles[index] = Environment.ExpandEnvironmentVariables(file);
            }

            for (int index = 0; index < ReopenFiles.Count; ++index)
            {
                string file = ReopenFiles[index];
                ReopenFiles[index] = Environment.ExpandEnvironmentVariables(file);
            }

            for (int index = 0; index < RemoveFiles.Count; ++index)
            {
                string file = RemoveFiles[index];
                RemoveFiles[index] = Environment.ExpandEnvironmentVariables(file);
            }

            for (int index = 0; index < ReconcileFiles.Count; ++index)
            {
                string file = ReconcileFiles[index];
                ReconcileFiles[index] = Environment.ExpandEnvironmentVariables(file);
            }

            for (int index = 0; index < DeleteFiles.Count; ++index)
            {
                string file = DeleteFiles[index];
                DeleteFiles[index] = Environment.ExpandEnvironmentVariables(file);
            }

            for (int index = 0; index < EnvVars.Count; ++index)
            {
                string value = EnvVars[index].Value;
                EnvVars[index].Value = Environment.ExpandEnvironmentVariables(value);
            }

            return true;
        }

        /// <summary>
        /// Pre-processing steps.  Specific tasks can synchronize to sub-directories that aren't necessary to run 
        /// all build tasks.
        /// 
        /// </summary>
        /// <returns></returns>
        public bool PreExecute()
        {
            Result.Success = true;

            foreach(EnvVar variable in EnvVars)
            {
                string varValue = Environment.GetEnvironmentVariable(variable.Name);
                PreviousEnvVars.Add(new EnvVar(variable.Name, varValue));

                Environment.SetEnvironmentVariable(variable.Name, variable.Value);
            }

            foreach(string syncFile in SyncFiles)
            {
                try
                {
                    P4RecordSet revertSet = PerforceInfo.m_P4.Run("revert", syncFile);
                    if (revertSet.HasErrors() == true)
                    {
                        string errorString = "";
                        foreach (string error in revertSet.Errors)
                        {
                            errorString += error + "\n";
                        }

                        LogError("Unable to revert file " + syncFile + " prior to synchronizing: " + errorString);
                        Result.Success = false;
                    }

                    P4RecordSet recordSet = PerforceInfo.m_P4.Run("sync", syncFile);
                    if (recordSet.HasErrors() == true)
                    {
                        string errorString = "";
                        foreach (string error in recordSet.Errors)
                        {
                            errorString += error + "\n";
                        }

                        LogError("Unable to sync to file " + syncFile + ": " + errorString);
                        Result.Success = false;
                    }
                }
                catch (Exception e)
                {
                    LogError("An exception has occurred executing sync: " + e.Message);
                }
            }

            CheckOutFile checkOutFileTask = new CheckOutFile();
            checkOutFileTask.PerforceInfo = this.PerforceInfo;
            checkOutFileTask.Files = CheckOutFiles;
            if (checkOutFileTask.Execute() == false)
            {
                Result.Success = false;
                Result.Combine(checkOutFileTask.Result);
                LogError("Unable to check out files from Perforce.");
                return false;
            }

            return Result.Success;
        }


        private void RevertFileList(List<string> fileList)
        {
            foreach (string revertFile in fileList)
            {
                P4RecordSet recordSet = PerforceInfo.m_P4.Run("revert", revertFile);
                if (recordSet.HasErrors() == true)
                {
                    string errorString = "";
                    foreach (string error in recordSet.Errors)
                    {
                        errorString += error + "\n";
                    }

                    LogError("Unable to revert file " + revertFile + ": " + errorString);
                    Result.Success = false;
                }
            }
        }

        /// <summary>
        /// Post-processing steps after the task has been completed.
        /// This function will execute on either success or failure.
        /// </summary>
        /// <returns></returns>
        public bool PostExecute()
        {
            PerforceInfo.Activate();

            if (Result.Success == false)
            {
                RevertFileList(CheckOutFiles);
                RevertFileList(ReopenFiles);
                RevertFileList(ReconcileFiles);
                RevertFileList(DeleteFiles);
            }
            else
            {
                foreach (string reopenFile in ReopenFiles)
                {
                    P4RecordSet recordSet = PerforceInfo.m_P4.Run("reopen", "-c", PerforceInfo.m_Changelist.m_Number.ToString(), reopenFile);
                    if (recordSet.HasErrors() == true)
                    {
                        string errorString = "";
                        foreach (string error in recordSet.Errors)
                        {
                            errorString += error + "\n";
                        }

                        LogError("Unable to reopen file " + reopenFile + ": " + errorString);
                        Result.Success = false;
                    }
                }

                ProcessReconciledFiles();
            }

            foreach (string removeFile in RemoveFiles)
            {
                P4RecordSet recordSet = PerforceInfo.m_P4.Run("sync", removeFile + "#none");
                if (recordSet.HasErrors() == true)
                {
                    string errorString = "";
                    foreach (string error in recordSet.Errors)
                    {
                        errorString += error + "\n";
                    }

                    LogError("Unable to remove sync file " + removeFile + ": " + errorString);
                    Result.Success = false;
                }
            }

            foreach (string deleteFile in DeleteFiles)
            {
                if (Directory.Exists(deleteFile) == true)
                {
                    try
                    {
                        Directory.Delete(deleteFile, true);
                    }
                    catch (Exception e)
                    {
                        LogError("Unable to delete directory " + deleteFile + ": " + e.Message);
                    }
                }
                else if (File.Exists(deleteFile) == true)
                {
                    try
                    {
                        File.Delete(deleteFile);
                    }
                    catch (Exception e)
                    {
                        LogError("Unable to delete file " + deleteFile + ": " + e.Message);
                    }
                }

                //Only submit files on success.
                ProcessSubmittedFiles();
            }

            foreach(EnvVar variable in PreviousEnvVars)
            {
                Environment.SetEnvironmentVariable(variable.Name, variable.Value);
            }


            TimeElapsed = (DateTime.UtcNow - StartTime).TotalSeconds;

            return Result.Success;
        }

        /// <summary>
        /// Processes the files marked for reconciliation for this task.
        /// </summary>
        private void ProcessReconciledFiles()
        {
            const int MaxFilesPerCommand = 1000;

            foreach (string reconcileFile in ReconcileFiles)
            {
                string depotDirPath = null;
                string localDirPath = null;
                if (reconcileFile.EndsWith("...") == true || reconcileFile.EndsWith("*"))
                {
                    string dirName = Path.GetDirectoryName(reconcileFile);
                    P4RecordSet whereRecordSet = PerforceInfo.m_P4.Run("where", dirName);
                    if (whereRecordSet.HasErrors() == true)
                        continue;

                    depotDirPath = whereRecordSet[0]["depotFile"].ToLower();
                    localDirPath = whereRecordSet[0]["path"].ToLower();
                }
                else
                {
                    P4RecordSet fstatRecordSet = PerforceInfo.m_P4.Run("fstat", reconcileFile);
                    if (fstatRecordSet.HasErrors() == true)
                        continue;

                    depotDirPath = fstatRecordSet[0]["depotFile"].ToLower();
                    localDirPath = fstatRecordSet[0]["clientFile"].ToLower();
                }

                P4RecordSet filesRecordSet = PerforceInfo.m_P4.Run("files", reconcileFile);

                List<string> addedFiles = new List<string>();
                List<string> deletedFiles = new List<string>();
                List<string> processedFiles = new List<string>();
                foreach (P4Record fileRecord in filesRecordSet.Records)
                {
                    string depotFile = fileRecord["depotFile"].ToLower();
                    depotFile = depotFile.Replace(depotDirPath, localDirPath);
                    depotFile = depotFile.Replace("/", "\\");

                    //If the file has been deleted from Perforce but exists, add it.
                    if (fileRecord["action"] != "delete")
                    {
                        if (File.Exists(depotFile) == false)
                        {
                            //Delete this file from Perforce.
                            deletedFiles.Add(depotFile);
                            processedFiles.Add(depotFile);
                        }
                        else
                        {
                            //We'll check out this file in one fell swoop after processing the adds/deletes.
                            processedFiles.Add(depotFile);
                        }
                    }
                    else if (File.Exists(depotFile) == true)
                    {
                        //Re-add this file to Perforce.
                        addedFiles.Add(depotFile);
                        processedFiles.Add(depotFile);
                    }
                }

                PerforceInfo.m_P4.Run("edit", "-c", PerforceInfo.m_Changelist.m_Number.ToString(), reconcileFile);

                //Handle all files not in Perforce.
                string[] localFiles = null;
                string reconcileDir = Path.GetDirectoryName(reconcileFile);
                string reconcileFileName = Path.GetFileName(reconcileFile);
                if (reconcileFile.EndsWith("*") == true)
                {
                    localFiles = Directory.GetFiles(reconcileDir, "*", SearchOption.TopDirectoryOnly);
                }
                else if (reconcileFile.EndsWith("...") == true)
                {
                    localFiles = Directory.GetFiles(reconcileDir, "*", SearchOption.AllDirectories);
                }
                else if (File.Exists(localDirPath) == true)
                {
                    localFiles = new string[1];
                    localFiles[0] = localDirPath;
                }
                else
                {
                    localFiles = Directory.GetFiles(reconcileDir, reconcileFileName, SearchOption.AllDirectories);
                }

                foreach (string localFile in localFiles)
                {
                    bool processed = false;
                    foreach (string processedFile in processedFiles)
                    {
                        if (String.Compare(localFile, processedFile, true) == 0)
                        {
                            processed = true;
                            break;
                        }
                    }

                    //If we haven't processed this file, then it must be new.
                    if (processed == false)
                    {
                        addedFiles.Add(localFile);
                    }
                }

                //Batch the add commands together to reduce the number of commands to the server.
                int addedGroupsCount = (int) Math.Ceiling( ((decimal)addedFiles.Count) / ((decimal)MaxFilesPerCommand));
                for (int groupIndex = 0; groupIndex < addedGroupsCount; ++groupIndex)
                {
                    List<string> arguments = new List<string>();
                    arguments.Add("-c");
                    arguments.Add(PerforceInfo.m_Changelist.m_Number.ToString());

                    int groupOffset = groupIndex * MaxFilesPerCommand;
                    int limit = Math.Min(groupOffset + MaxFilesPerCommand, addedFiles.Count);
                    for(int fileIndex = 0; fileIndex < MaxFilesPerCommand; ++fileIndex)
                    {
                        arguments.Add(addedFiles[groupOffset + fileIndex]);
                    }

                    PerforceInfo.m_P4.Run("add", arguments.ToArray());
                }

                //Batch the delete commands in large groups.
                int deletedGroupsCount = (int)Math.Ceiling(((decimal)deletedFiles.Count) / ((decimal)MaxFilesPerCommand));
                for (int groupIndex = 0; groupIndex < deletedGroupsCount; ++groupIndex)
                {
                    List<string> arguments = new List<string>();
                    arguments.Add("-c");
                    arguments.Add(PerforceInfo.m_Changelist.m_Number.ToString());

                    int groupOffset = groupIndex * MaxFilesPerCommand;
                    for (int fileIndex = 0; fileIndex < MaxFilesPerCommand; ++fileIndex)
                    {
                        arguments.Add(deletedFiles[groupOffset + fileIndex]);
                    }

                    PerforceInfo.m_P4.Run("delete", arguments.ToArray());
                }

            }
        }

        /// <summary>
        /// Processes the files marked for submission in the build task.
        /// </summary>
        private void ProcessSubmittedFiles()
        {
            if (Result.Success == true)
            {
                //Acquire the changelist description.
                string changelistDescription = "Automated Build Client"; //TODO:  Acquire a proper changelist description from the database
                Changelist changelist = new Changelist(PerforceInfo.m_P4);
                if (changelist.Create(changelistDescription) == true)
                {
                    //Reopen all of the associated files into this changelist.
                    foreach (string submitFile in SubmitFiles)
                    {
                        P4RecordSet recordSet = PerforceInfo.m_P4.Run("reopen", "-c", changelist.m_Number.ToString(), submitFile);
                        if (recordSet.HasErrors() == true)
                        {
                            string errorString = "";
                            foreach (string error in recordSet.Errors)
                            {
                                errorString += error + "\n";
                            }

                            LogError("Unable to reopen file " + submitFile + " prior to submission: " + errorString);
                            Result.Success = false;
                        }
                    }

                    changelist.Submit();
                }
                else
                {
                    LogError("Unable to create changelist for client " + this.TargetClient.m_MachineName + " submission.");
                    Result.Success = false;
                }
            }
        }


        public virtual bool Execute()
        {
            Console.WriteLine("BuildTask Execute() function called!  Task is missing its Execute implementation");
            return false;
        }

        public static void RegisterProcess(Process command)
        {
            lock (m_RegisteredProcesses)
            {
                m_RegisteredProcesses.Add(command);
            }
        }

        public static void RegisterProcess(rageExecuteCommand command)
        {
            lock (m_RegisteredRageProcesses)
            {
                m_RegisteredRageProcesses.Add(command);
            }
        }

        public static void Abort()
        {
            foreach (Process process in m_RegisteredProcesses)
            {
                if (process.HasExited == false)
                    process.Kill(); 
            }

            foreach (rageExecuteCommand process in m_RegisteredRageProcesses)
            {
                process.Kill();
            }
        }

        public bool FilterReport()
        {
            if (FilterFile != null)
            {
                if (File.Exists(FilterFile) == true)
                {
                    //Filter files are meant to be Ruby script file and subscribe the following interface.
                    //<ruby file> --file=<input file> --output=<output file>
                    if (String.Compare(Path.GetExtension(FilterFile), ".rb", true) == 0)
                    {
                        string tmpFile = Path.Combine(Environment.GetEnvironmentVariable("TEMP"), ToReportName() + ".txt");
                        string outputFile = Path.Combine(Environment.GetEnvironmentVariable("TEMP"), "output.txt");
                        this.Result.WriteRawLog(tmpFile);

                        string command = "ruby.exe";
                        string arguments = FilterFile + " --file=" + tmpFile + " --output=" + outputFile;
                        ProcessStartInfo startInfo = new ProcessStartInfo(command, arguments);
                        startInfo.CreateNoWindow = true;
                        startInfo.UseShellExecute = false;

                        Process p = Process.Start(startInfo);
                        if (p != null)
                        {
                            p.WaitForExit();
                        }

                        if (p.ExitCode == 0)
                        {
                            TaskResult UpdatedResult = new TaskResult();
                            UpdatedResult.LogMessage("Filtered log with " + command + " " + arguments + "...");
                            UpdatedResult.ReadRawLog(outputFile);
                            UpdatedResult.Success = Result.Success;
                            Result = UpdatedResult;
                        }
                        else
                        {
                            LogError("Filtering file with command " + command + " " + arguments + " failed.");
                        }
                    }
                }
                else
                {
                    LogError("Filter file " + FilterFile + " to parse output could not be found.");
                }
            }

            return true;
        }

        public void Log(string output)
        {
            Console.WriteLine(output);
            Result.LogMessage(output);
        }

        public void LogWarning(string warning)
        {
            Result.LogWarning(warning);
        }

        public void LogError(string error)
        {
            Result.LogError(error);
        }

        public void LogPerforceRecords(P4RecordSet recordSet)
        {
            foreach (string message in recordSet.Messages)
            {
                Log(message);
            }

            foreach (string warning in recordSet.Warnings)
            {
                LogWarning(warning);
            }

            foreach (string error in recordSet.Errors)
            {
                LogError(error);
            }
        }

        //A human-readable string describing the task.
        public virtual string ToDisplayName()
        {
            return ToShortString();
        }

        //A file-friendly name describing the task.
        public virtual string ToReportName()
        {
            return ToShortString();
        }

        //A small-real estate name for the task.
        public virtual string ToShortString()
        {
            return "Unknown Build Task.";
        }

    }
}
