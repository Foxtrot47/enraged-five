﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using RSG.Base.ConfigParser;

namespace AutomatedBuildShared
{
    public class CutsceneLightingExport : StudioMax
    {
        [XmlArrayItem("ExportFolder")]
        public List<string> ExportFolders;

        private string ResultsFile;

        public CutsceneLightingExport()
        {
            ExportFolders = null;
        }

        public override bool Execute()
        {
            //There is an assumption that the MaxScript reads an input file for a list of scenes to export.
            ConfigGameView gameView = new ConfigGameView();
            ResultsFile = Path.Combine(gameView.ToolsRootDir, "tmp\\cutscenelighting_results.txt");
            if (MP3Hack == true)
            {
                ResultsFile = @"X:\payne\tools\tmp\cutscenelighting_results.txt";
            }

            //Create a list of files to export for 3D Studio Max to read in.

            string listFilePath = Path.Combine(gameView.ToolsRootDir, "tmp\\cutscenelighting.txt");
            if (MP3Hack == true)
            {
                listFilePath = @"X:\payne\tools\tmp\cutscenelighting.txt";
            }

            try
            {
                TextWriter outputFile = File.CreateText(listFilePath);
                foreach (string exportFolder in ExportFolders)
                {
                    outputFile.WriteLine(exportFolder);
                }
                outputFile.Close();
            }
            catch (Exception e)
            {
                LogError("Unable to create input file for 3D Studio Max.\n" + e.Message);
                Result.Success = false;
                return Result.Success;
            }

            base.Execute();

            //Read the results files.
            if (File.Exists(ResultsFile) == true)
            {
                string[] results = File.ReadAllLines(ResultsFile);
                foreach (string result in results)
                {
                    if (result.Contains("failed") == true)
                    {
                        LogError(result);
                        Result.Success = false;
                    }
                    else
                    {
                        Log(result);
                    }
                }
            }
            else
            {
                LogError("Unable to find results file " + ResultsFile + ".");
                Result.Success = false;
            }

            return Result.Success;

        }
    }
}
