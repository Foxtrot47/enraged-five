﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using RSG.SourceControl.Perforce;
using P4API;

namespace AutomatedBuildShared
{
    public class PerforceSynchronization : BuildTask
    {
        [XmlAttribute("SyncTime")]
        public DateTime m_SyncTime;

        [XmlAttribute("ChangelistNumber")]
        public int m_ChangelistNumber;

        [XmlAttribute("DepotPath")]
        public string m_DepotPath;

        [XmlAttribute("BuildLabel")]
        public string m_BuildLabel;

        [XmlAttribute("SourceMachine")]
        public string m_SourceMachine;

        public PerforceSynchronization()
        {
            Name = "Perforce Synchronization";
            m_ChangelistNumber = -1;
            m_DepotPath = null;
            m_BuildLabel = null;
            m_SourceMachine = null;
        }

        public PerforceSynchronization(int changelistNumber, string depotPath)
        {
            Name = "PerforceSynchronization";
            m_ChangelistNumber = changelistNumber;
            m_DepotPath = depotPath;
            m_BuildLabel = null;
            m_SourceMachine = null;
        }

        public override bool Execute()
        {
            if (m_ChangelistNumber == -1)
            {
                //HACK: Added an time zone offset.
                //DateTime shiftedNow = DateTime.Now;
                //shiftedNow = shiftedNow.AddHours(8);

                //string formatDate = shiftedNow.ToString("yyyy/MM/dd");
                //string formatTime = shiftedNow.ToString("HH:mm:ss");
                //string syncTime = "@" + formatDate + ":" + formatTime;

                //Assume that users want to operate on the head revision.
                P4RecordSet lastChangeRecord = PerforceInfo.m_P4.Run("changes", "-m", "1", "-s", "submitted");
                if ( lastChangeRecord.HasErrors() == false )
                {
                    if (lastChangeRecord.Records.Length == 0)
                    {
                        Result.Success = false;
                        Result.LogError("Unable to acquire last submitted changelist. Perforce provided no results.");
                    }
                    else
                    {
                        m_ChangelistNumber = Convert.ToInt32(lastChangeRecord.Records[0].Fields["change"]);
                    }
                }
                else
                {
                    Result.Success = false;
                    Result.LogError("Unable to acquire last submitted changelist.");
                    return Result.Success;
                }
            }

            string ChangelistDescription = "Automated Build";
            if (m_BuildLabel != null)
            {
                ChangelistDescription += " " + m_BuildLabel;
            }

            string depotPerforcePath = m_DepotPath;

            if (m_ChangelistNumber != -1)
            {
                depotPerforcePath += "@" + m_ChangelistNumber;
            }

            P4RecordSet syncRecord = PerforceInfo.m_P4.Run("sync", depotPerforcePath);

            Result.Success = !syncRecord.HasErrors();

            foreach (string message in syncRecord.Messages)
            {
                Result.LogMessage(message);
            }

            foreach (string warning in syncRecord.Warnings)
            {
                Result.LogWarning(warning);
            }

            foreach (string error in syncRecord.Errors)
            {
                Result.LogError(error);
            }

            return Result.Success;
        }

        public override string ToReportName()
        {
            return "Perforce_Sync-" + this.m_SourceMachine;
        }

        public override string ToShortString()
        {
            return "Syncing...";
        }

        public override string ToString()
        {
            if (m_ChangelistNumber == -1)
            {
                return Name + " to " + m_SyncTime.ToShortDateString() + " " + m_SyncTime.ToShortTimeString() + " on " + m_DepotPath;
            }
            else
            {
                return Name + " to Change " + m_ChangelistNumber + " on " + m_DepotPath;
            }
        }
    }
}
