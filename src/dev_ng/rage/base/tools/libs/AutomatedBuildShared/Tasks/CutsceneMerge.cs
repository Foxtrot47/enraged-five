﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace AutomatedBuildShared
{
    public class CutsceneMerge : BuildTask
    {
        [XmlAttribute("MergeList")]
        public string MergeList;

        [XmlAttribute("MP3Hack")]
        public bool MP3Hack;

        public CutsceneMerge()
        {
            MergeList = null;
        }

        /// <summary>
        /// Parses data in the mergelist such as the input and output scenes.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="folderList"></param>
        /// <param name="strOutputFolder"></param>
        /// <returns></returns>
        private bool ParseMergeList(string filename, out string[] folderList, out string strOutputFolder)
        {
            folderList = null;
            strOutputFolder = "";

            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Trim() == "#Scenes")
                    {
                        List<string> folders = new List<string>();
                        while ((line = sr.ReadLine()) != null && line != "" && line.Substring(0, 1) != "#")
                        {
                            folders.Add(line.Trim().Split(',').ElementAt(0));
                        }
                        folderList = new string[folders.Count()];
                        for (int i = 0; i < folders.Count(); i++)
                        {
                            folderList[i] = folders.ElementAt(i);
                        }
                    }
                    if (line.Trim() == "#Output")
                    {
                        strOutputFolder = sr.ReadLine();
                    }
                }
            }

            return true;
        }

        public bool MergeScene()
        {
            if (File.Exists(MergeList) == false)
            {
                Result.Success = false;
                Result.LogError("Merge list " + MergeList + " could not be found.");
                return Result.Success;
            }

            string[] folderList;
            string mergeOutputPath;
            if (ParseMergeList(MergeList, out folderList, out mergeOutputPath) == false)
            {
                Result.Success = false;
                Result.LogError("Merge list " + MergeList + " could not be parsed.");
                return Result.Success;
            }

            string mergeScript = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "lib\\util\\motionbuilder\\merge_cutscene.rb");
            string mergeLog = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "logs\\merge_cutscene.log");
            if (MP3Hack == true)
            {
                mergeScript = @"X:\payne\tools\lib\util\motionbuilder\merge_cutscene.rb";
                mergeLog = @"X:\payne\tools\logs\merge_cutscene.log";
            }

            try
            {
                if (File.Exists(mergeLog) == true)
                    File.Delete(mergeLog);
            }
            catch (Exception e)
            {
                Result.Success = false;
                Result.LogError("Unable to delete log file. " + e.Message);
                return Result.Success;
            }

            Process pExe = new System.Diagnostics.Process();
            if (MP3Hack == true)
                pExe.StartInfo.FileName = "X:\\payne\\tools\\bin\\ruby\\bin\\ruby.exe";
            else
                pExe.StartInfo.FileName = "ruby";
            pExe.StartInfo.Arguments = mergeScript + " --input=" + MergeList + " --verbose --nopack";
            pExe.StartInfo.CreateNoWindow = true;
            pExe.StartInfo.UseShellExecute = false;

            //HACK:  This is a hack to have RDR3's Automated Build run on the MP3 branch.
            //This is done because the MP3 installer does not have command line access to run
            //and more importantly, adding to it is undesirable.
            if (MP3Hack == true)
            {
                pExe.StartInfo.WorkingDirectory = "X:\\payne\\tools";

                pExe.StartInfo.EnvironmentVariables["RS_TOOLSROOT"] = "X:\\payne\\tools";
                pExe.StartInfo.EnvironmentVariables["RS_PROJROOT"] = "X:\\payne";
                pExe.StartInfo.EnvironmentVariables["RS_PROJECT"] = "payne";
                pExe.StartInfo.EnvironmentVariables["RS_BUILDBRANCH"] = "X:\\payne\\build\\dev";
                pExe.StartInfo.EnvironmentVariables["RS_CODEBRANCH"] = "X:\\payne\\src\\dev";
                pExe.StartInfo.EnvironmentVariables["RUBYLIB"] = "X:\\payne\\tools\\lib";

                string[] pathVars = pExe.StartInfo.EnvironmentVariables["PATH"].Split(';');
                pathVars[pathVars.Length - 2] = "X:\\payne\\tools\\bin";
                pathVars[pathVars.Length - 1] = "X:\\payne\\tools\\bin\\ruby\\bin";

                string fullPathString = "";
                for (int pathIndex = 0; pathIndex < pathVars.Length; ++pathIndex)
                {
                    fullPathString += pathVars[pathIndex];

                    if (pathIndex < pathVars.Length - 1)
                    {
                        fullPathString += ";";
                    }
                }
                pExe.StartInfo.EnvironmentVariables["PATH"] = fullPathString;
            }
            else
                pExe.StartInfo.WorkingDirectory = Environment.GetEnvironmentVariable("RS_TOOLSROOT");

            RegisterProcess(pExe);
            pExe.Start();

            pExe.WaitForExit();

            if (File.Exists(mergeLog) == true)
            {
                string[] lines = File.ReadAllLines(mergeLog);
                foreach (string line in lines)
                {
                    Result.LogMessage(line);
                }
            }
            else
            {
                Result.Success = false;
                LogError("The log " + mergeLog + " could not be found.");
                return Result.Success;
            }

            if (pExe.ExitCode != 0)
            {
                Result.Success = false;
            }

            return Result.Success;
        }

        public override bool Execute()
        {
            if (String.IsNullOrWhiteSpace(MergeList) == true)
            {
                LogError("No mergelist file was specified.  Unable to continue...");
                Result.Success = false;
                return false;
            }

            if (String.IsNullOrWhiteSpace(MergeList) == true)
            {
                LogError("No mergelist file was specified.  Unable to continue...");
                Result.Success = false;
                return false;
            }

            if (MergeScene() == false)
            {
                Result.Success = false;
                return false;
            }

            return true;
        }

        public override string ToReportName()
        {
            return "CutsceneMerge_" + Path.GetFileNameWithoutExtension(MergeList);
        }

        public override string ToShortString()
        {
            return Path.GetFileNameWithoutExtension(MergeList);
        }

        public override string ToString()
        {
            return "Merging Cutscene with " + MergeList;
        }
    }
}
