﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using RSG.SourceControl.Perforce;
using P4API;

namespace AutomatedBuildShared
{
    [Serializable]
    public class CheckOutFile : BuildTask
    {
        #region Variables
        public List<string> Files;
        public string Description;
        #endregion

        public override bool Execute()
        {
            if (Files.Count == 0)
            {
                LogError("No files were not specified for the Check Out File task.");
                return true;
            }

            bool success = true;
            foreach (string filename in Files)
            {
                Log("Checking Out " + filename + " From Perforce...");

                string perforcePath = filename; 
                if (filename.StartsWith("//") == false) 
                {
                    //The path is not a Perforce path.
                    P4RecordSet fstatRecord = PerforceInfo.m_P4.Run("fstat", filename);
                    if (fstatRecord.HasErrors() == false && fstatRecord.Records.Length != 0 && fstatRecord.Records.Length == 1)
                        perforcePath = fstatRecord[0].Fields["depotFile"];
                    else
                    {
                        //This is a directory.
                        string directory = filename;
                        if (directory.EndsWith("\\...") == true || directory.EndsWith("/...") == true)
                            directory = directory.Remove(directory.Length - 4);

                        P4RecordSet dirsRecord = PerforceInfo.m_P4.Run("dirs", directory);
                        if (dirsRecord.HasErrors() == true)
                        {
                            Log("Unable to convert " + filename + " into a Perforce path.");
                            return false;
                        }
                        else if (dirsRecord.Records.Length == 0)
                        {
                            LogWarning("Directory " + perforcePath + " does not exist in Perforce. Ignoring...");
                            continue;
                        }

                        perforcePath = dirsRecord[0]["dir"] + "/...";
                    }
                }

                if (PerforceInfo.m_Changelist.Edit(perforcePath) == false)
                {
                    LogError("Checking out " + perforcePath + " has failed.");
                    success = false;
                }
            }

            return success;
        }
    }
}