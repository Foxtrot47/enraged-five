﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using RSG.SourceControl.Perforce;
using P4API;

namespace AutomatedBuildShared
{
    public class BuildAssets : BuildTask
    {
        [XmlAttribute("GroupName")]
        public string m_GroupName;

        [XmlArray("Assets")]
        [XmlArrayItem("Asset")]
        public List<string> m_AssetsList;

        [XmlAttribute("Executable")]
        public string m_Executable;

        [XmlAttribute("ScriptFile")]
        public string m_ScriptFile;

        private int m_TimeoutSeconds;
        private List<Command> m_CommandList;

        public BuildAssets()
        {
            Name = "Build Assets";
            m_AssetsList = new List<string>();
            m_Executable = null;
            m_ScriptFile = null;

            m_TimeoutSeconds = 30 * 60;
            m_CommandList = new List<Command>();
        }

        private void BuildThread(object data)
        {
            string assetFile = (string) data;
            if ( String.IsNullOrWhiteSpace(assetFile) == true ) 
                return;

            Command buildCommand = new Command();
            buildCommand.Executable = m_Executable;
            if (String.IsNullOrWhiteSpace(m_ScriptFile) == true)
            {
                buildCommand.Arguments = assetFile;
            }
            else
            {
                buildCommand.Arguments = m_ScriptFile + " " + assetFile;
            }

            buildCommand.Execute();

            lock (m_CommandList)
            {
                m_CommandList.Add(buildCommand);
            }
        }

        public override bool Execute()
        {
            Result.Success = false;
            if ( String.IsNullOrWhiteSpace(m_Executable) == true)
            {
                Result.LogError("No executable was specified.");
            }

            if (String.IsNullOrWhiteSpace(m_ScriptFile) == true)
            {
                Result.LogWarning("No script file was specified.  Based on the executable specified this may cause assets to fail building.");
            }

            if (Result.ErrorCount > 0)
            {
                return Result.Success;
            }

            ParameterizedThreadStart threadDelegate = new ParameterizedThreadStart(BuildThread);
            int maxThreads = Environment.ProcessorCount;
            List<Thread> activeThreads = new List<Thread>();

            foreach (string asset in m_AssetsList)
            {
                while (activeThreads.Count >= maxThreads)
                {
                    for(int threadIndex = 0; threadIndex < activeThreads.Count; ++threadIndex)
                    {
                        if (activeThreads[threadIndex].IsAlive == false)
                        {
                            activeThreads.RemoveAt(threadIndex);
                            threadIndex--;
                        }
                    }

                    Thread.Sleep(100);
                }

                if (activeThreads.Count < maxThreads)
                {
                    Thread thread = new Thread(threadDelegate);
                    thread.Name = "Build Thread " + asset + " Processing Thread";
                    thread.Start(asset);

                    activeThreads.Add(thread);
                }

                Thread.Sleep(10);
            }

            DateTime startTime = DateTime.Now;
            
            while(true)
            {
                bool exit = true;
                foreach (Thread activeThread in activeThreads)
                {
                    if (activeThread.IsAlive == true)
                    {
                        exit = false;
                    }
                }

                if (exit == true)
                    break;

                DateTime endTime = DateTime.Now;
                TimeSpan timeSpan = endTime - startTime;
                if (timeSpan.TotalSeconds > m_TimeoutSeconds)
                {
                    Result.Success = false;
                    Result.LogError("The build has continued past its " + m_TimeoutSeconds + " second timeout.");
                }
                Thread.Sleep(100);
            }

            foreach (Command buildCommand in m_CommandList)
            {
                this.Result.Combine(buildCommand.Result);
            }

            if (this.Result.ErrorCount == 0)
            {
                Result.Success = true;
            }

            return Result.Success;
        }

        public override string ToReportName()
        {
            return "Build_Assets-" + m_GroupName;
        }

        public override string ToShortString()
        {
            return "Building Assets...";
        }

        public override string ToString()
        {
            return "Building Assets";
        }
    }
}
