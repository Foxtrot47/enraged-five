﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

using Microsoft.Win32;
using RSG.Base.Command;
using RSG.Base.Logging.Universal;

namespace AutomatedBuildShared
{
    public enum StudioMaxVersion
    {
        MaxNone,
        Max2009,
        Max2010,
        Max2011,
        Max2012
    }

    public class StudioMax : BuildTask
    {
        [XmlAttribute("Version")]
        public StudioMaxVersion Version;

        [XmlAttribute("MaxFile")]
        public string MaxFile;

        [XmlAttribute("MaxScript")]
        public string MaxScript;

        [XmlAttribute("MarkerFile")]
        public string MarkerFile;

        [XmlAttribute("UniversalLog")]
        public string UniversalLogFile;

        [XmlAttribute("MP3Hack")]
        public bool MP3Hack;

        private static readonly String AUTODESK_3DSMAX_KEY = "SOFTWARE\\Autodesk\\3dsMax";
        private static readonly String AUTODESK_3DSMAX_KEY_X86_ON_X64 = "SOFTWARE\\Wow6432Node\\Autodesk\\3dsMax";
        private static readonly String AUTODESK_3DSMAX_EXE = "3dsmax.exe";
        private static readonly Dictionary<StudioMaxVersion, String> VERSION_DIRS = new Dictionary<StudioMaxVersion, String>() 
		{
			{StudioMaxVersion.MaxNone, "0.0"},
			{StudioMaxVersion.Max2009, "11.0"},
			{StudioMaxVersion.Max2010, "12.0"},
			{StudioMaxVersion.Max2011, "13.0"},
			{StudioMaxVersion.Max2012, "14.0"}
		};

        public StudioMax()
        {
            Version = StudioMaxVersion.MaxNone;
            MaxFile = null;
            MaxScript = null;
            MarkerFile = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\tmp\\3dsmax_marker.txt";
        }

        public override bool Localize()
        {
            base.Localize();

            return true;
        }

        public override bool Execute()
        {
            string maxInstallDir = GetMaxInstallationDirectory(Version);

            if (maxInstallDir == null)
            {
                Result.Success = false;
                Result.LogError("Unable to find Max installation directory for " + Version.ToString() + ".");
                return Result.Success;
            }

            if (File.Exists(MaxScript) == false)
            {
                Result.Success = false;
                Result.LogError("The MaxScript " + MaxScript + " does not exist.");
                return Result.Success;
            }

            if (String.IsNullOrWhiteSpace(MaxFile) == false)
            {
                if (File.Exists(MaxFile) == false)
                {
                    Result.Success = false;
                    Result.LogError("The file " + MaxFile + " does not exist.");
                    return Result.Success;
                }

                //Delete any lingering lock files.
                string directory = Path.GetDirectoryName(MaxFile);
                string[] lockFiles = Directory.GetFiles(directory, "*.lock");
                foreach (string lockFile in lockFiles)
                {
                    try
                    {
                        File.Delete(lockFile);
                    }
                    catch (Exception) { }
                }
            }

            //Because 3D Studio Max will not start up because of a corrupted .mnu file,
            //consistently delete it.  
            string applicationDataDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "..");
            applicationDataDirectory = Path.GetFullPath(applicationDataDirectory);
            string mnuFile = Path.Combine(applicationDataDirectory, "Local\\Autodesk\\3dsMax\\2012 - 64bit\\enu\\UI\\MaxStartUI.mnu");
            if ( File.Exists(mnuFile) == true )
            {
                try { File.Delete(mnuFile); } 
                catch(Exception) {}
            }


            Process pExe = new System.Diagnostics.Process();
            pExe.StartInfo.FileName = Path.Combine(maxInstallDir, AUTODESK_3DSMAX_EXE);
            pExe.StartInfo.UseShellExecute = false;

            //Certain Max files, especially containers, want the Max file to be writable.
            //Since certain files are exclusively locked in Perforce, checking them out is
            //not necessarily an option.   Set this to be writable locally and then back to read-only
            //afterwards.
            File.SetAttributes(MaxFile, File.GetAttributes(MaxFile) & ~(FileAttributes.ReadOnly));

            if (String.Compare(Path.GetExtension(MaxFile), ".maxc", true) == 0)
            {
                //If users have specified a Max container, the Max script will need to know what to do with it.
                pExe.StartInfo.EnvironmentVariables.Add("MAX_CONTAINER_FILE", MaxFile);
                pExe.StartInfo.Arguments = "-silent -mip -U MAXScript " + MaxScript;
            }
            else
            {
                //string cmd = "3dsmax.exe -silent -mip -U MaxScript X:\rdr3\tools\dcc\current\max2011\scripts\rockstar\helpers\terrain_utilities_cli.ms X:\rdr3\art\models\terrain\q_06_.max"
                pExe.StartInfo.Arguments = "-silent -mip -U MAXScript " + MaxScript;
                if (String.IsNullOrEmpty(MaxFile) == false)
                {
                    pExe.StartInfo.Arguments += " " + MaxFile;
                }
            }
            
            if (MP3Hack == true)
            {
                MarkerFile = "X:\\payne\\tools\\tmp\\3dsmax_marker.txt";

                pExe.StartInfo.EnvironmentVariables["RS_TOOLSROOT"] = "X:\\payne\\tools";
                pExe.StartInfo.EnvironmentVariables["RS_PROJROOT"] = "X:\\payne";
                pExe.StartInfo.EnvironmentVariables["RS_PROJECT"] = "payne";
                pExe.StartInfo.EnvironmentVariables["RS_BUILDBRANCH"] = "X:\\payne\\build\\dev";
                pExe.StartInfo.EnvironmentVariables["RS_CODEBRANCH"] = "X:\\payne\\src\\dev";
                pExe.StartInfo.EnvironmentVariables["RUBYLIB"] = "X:\\payne\\tools\\lib";

                string[] pathVars = pExe.StartInfo.EnvironmentVariables["PATH"].Split(';');
                pathVars[pathVars.Length - 2] = "X:\\payne\\tools\\bin";
                pathVars[pathVars.Length - 1] = "X:\\payne\\tools\\bin\\ruby\\bin";

                string fullPathString = "";
                for (int pathIndex = 0; pathIndex < pathVars.Length; ++pathIndex)
                {
                    fullPathString += pathVars[pathIndex];

                    if (pathIndex < pathVars.Length - 1)
                    {
                        fullPathString += ";";
                    }
                }
                pExe.StartInfo.EnvironmentVariables["PATH"] = fullPathString;
            }

            //Delete the Marker file -- this is a file that determines that 3D Studio Max has exited.
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(MarkerFile));
                if (File.Exists(MarkerFile) == true)
                {
                    File.Delete(MarkerFile);
                }
            }
            catch (Exception e)
            {
                Result.Success = false;
                Result.LogError("Unable to set up 3D Studio Max task.");
                Result.LogError(e.Message);
                return Result.Success;
            }

            Thread statusThread = new Thread(ExecuteThread);
            statusThread.Name = "3D Studio Max Execute Thread";
            statusThread.Start(pExe);

            while (true)
            {
                Process[] processes = Process.GetProcessesByName("3dsmax");
                if (processes.Length > 0)
                {
                    break;
                }
            }

            //Because 3D Studio Max's command line uses  'fire and forget' paradigm, we have no way of knowing when the 
            //process has finished.  For the moment, the MaxScripts need to write a temporary file marking when it has completed.
            //
            Result.Success = true;
            while (File.Exists(MarkerFile) == false)
            {
                Process[] processes = Process.GetProcessesByName("3dsmax");
                if (processes.Length == 0)
                {
                    Result.Success = false;
                    Result.LogError("3D Studio Max closes before signaling success.");
                    break;
                }
                
                Thread.Sleep(1000);
            }

            if ( pExe.HasExited == false )
                pExe.Kill();
    
            statusThread.Abort();

            //Parse the universal logs.
            Result.ParseUniversalLog(this.UniversalLogFile);

            //Set the read-only attribute back on.
            File.SetAttributes(MaxFile, File.GetAttributes(MaxFile) & (FileAttributes.ReadOnly));

            return Result.Success;
        }

        private void ExecuteThread(object input)
        {
            Process command = (Process)input;
            RegisterProcess(command);
            command.Start();
            command.WaitForExit();
        }

        public string GetMaxInstallationDirectory(StudioMaxVersion version)
        {
            RegistryKey localKey = null;
            if (Environment.Is64BitOperatingSystem == true)
            {
                localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine,
                     RegistryView.Registry64);

            }
            else
            {
                localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine,
                    RegistryView.Registry32);
            }

            Object installDir = null;
            String key = String.Format("{0}\\{1:0.0}\\MAX-1:409", AUTODESK_3DSMAX_KEY, VERSION_DIRS[version]);
            localKey = localKey.OpenSubKey(key);

            if (null != localKey)
                installDir = localKey.GetValue("Installdir");

            return installDir.ToString();
        }

        private static string ProcessAutodesk3dsmaxKey(RegistryKey key, StudioMaxVersion version)
        {
            string MaxPrefix = "MAX-";
            string regKeyVersion = VERSION_DIRS[version];

            try
            {
                foreach (String s in key.GetSubKeyNames())
                 {
                    if (!s.StartsWith(regKeyVersion))
                        continue;

                    RegistryKey verKey = key.OpenSubKey(s);
                    foreach (String sub in verKey.GetSubKeyNames())
                    {
                        if (!sub.StartsWith(MaxPrefix))
                            continue;

                        RegistryKey verSubKey = verKey.OpenSubKey(sub);

                        List<String> names = new List<String>(verSubKey.GetValueNames());
                        if (!names.Contains("Installdir"))
                            continue;

                        if (verSubKey.GetValueKind("InstallDir") == RegistryValueKind.String)
                        {
                            string installDir = verSubKey.GetValue("Installdir") as string;
                            return installDir;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }

            return null;
        }

        public override string ToReportName()
        {
            return "Studio_Max_" + Path.GetFileNameWithoutExtension(MaxFile);
        }

        public override string ToShortString()
        {
            if (String.IsNullOrWhiteSpace(MaxFile) == true)
                return Path.GetFileNameWithoutExtension(MaxScript);

            return Path.GetFileNameWithoutExtension(MaxFile);
        }

        public override string ToString()
        {
            return "Exporting from 3D Studio Max with " + MaxScript + " on file " + MaxFile;
        }
    }
}
