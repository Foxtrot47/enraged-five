using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Timers;
using System.Xml.Serialization;

using ps3tmapiInterface;

namespace AutomatedBuildShared
{
    public class ConsoleExecPS3 : BuildTask
    {
        public ConsoleExecPS3()
        {
            m_target.ConnectError += new MessageEventHandler( target_ConnectError );
            m_target.ConnectInfo += new MessageEventHandler( target_ConnectInfo );
            m_target.ConnectStatus += new ConnectStatusEventHandler( target_ConnectStatus );
            m_target.DebugEvent += new DebugEventEventHandler( target_DebugEvent );
            m_target.OutputTTY = true;
            m_target.TTY += new MessageEventHandler( target_TTY );
            m_target.UnhandledCrash += new MessageEventHandler( target_UnhandledCrash );
            m_target.UnitStatus += new UnitStatusEventHandler( target_UnitStatus );

            m_CurrentCount = 0;
            m_Count = 1;
        }

        #region Variables
        [XmlAttribute("Name")]
        public string TestName;

        [XmlAttribute("Target")]
        public string Target;
        
        [XmlAttribute("Command")]
        public string Command;

        [XmlAttribute("Arguments")]
        public string Arguments;

        [XmlAttribute("BootArguments")]
        public string PS3BootArguments;

        [XmlAttribute("SuccessMessage")]
        public string SuccessMessage;

        [XmlAttribute("Count")]
        public int m_Count;

        private string m_cmdLine = string.Empty;
        private bool m_waitForConnection = true;
        private bool m_alreadyReset = false;
        private bool m_hasExited = false;
        private bool m_waitingForReset = false;

        private Process m_ps3runProcess = null;
        private PS3Target m_target = new PS3Target();
        private int m_CurrentCount;
        #endregion

        #region Task Overrides
        public override bool Execute()
        {
            Result.Success = true;

            if (String.IsNullOrWhiteSpace(this.Command) == true)
            {
                LogError("No command was specified for PS3 execution task.  Aborting...");
                return Result.Success;
            }

            if (StartPS3RunProcess() == false)
            {
                Result.Success = false;
            }
            else
            {
                Log( "Executing PS3 task on " + this.Target + " with the command " + m_ps3runProcess.StartInfo.FileName + " " + m_ps3runProcess.StartInfo.Arguments + "." );

                int retryCount = 20;
                m_waitForConnection = true;
                while ( m_waitForConnection )
                {
                    if ( !m_target.Connect( this.Target ) )
                    {
                        Thread.Sleep( 1000 );
                    }

                    --retryCount;
                    if ( retryCount == 0 )
                    {
                        LogError("Connection Timeout - Unable to connect.  " + m_ps3runProcess.StartInfo.FileName + " " + m_ps3runProcess.StartInfo.Arguments);
                        Result.Success = false;
                        return Result.Success;
                    }                    
                }

                m_target.Update();

                while ( m_target.IsConnected && !m_hasExited && !m_ps3runProcess.HasExited )
                {
                    m_target.Update();
                }

                Log( String.Format( "Done With Test (TargetConnected={0}, HasExited={1}, PS3RunExited={2}, WaitingForReset={3}",
                    m_target.IsConnected, m_hasExited, m_ps3runProcess.HasExited, m_waitingForReset ) );
                
                Log( "Waiting while rebooting..." );

                if ( m_ps3runProcess.HasExited )
                {
                    // just to make sure
                    ResetSystrayRfs();
                }

                // wait for reset, if any
                while ( m_target.IsConnected && (!m_hasExited || m_waitingForReset) )
                {
                    m_target.Update();
                }

                if ( !m_ps3runProcess.HasExited )
                {
                    Log( "Ps3run has not exited.  Killing it." );
                    m_ps3runProcess.Kill();
                }

                m_target.Disconnect();
                m_target.Dispose();
                m_target = null;
            }            

            return Result.Success;
        }
        #endregion

        #region Private Functions
        private bool StartPS3RunProcess()
        {
            ProcessStartInfo psInfo = new ProcessStartInfo( "ps3run", "-w -r -da");

            if ( String.IsNullOrWhiteSpace(this.Target) == false )
            {
                psInfo.Arguments += " -t " + this.Target;
            }

            if ( String.IsNullOrWhiteSpace(this.PS3BootArguments) == false )
            {
                psInfo.Arguments += this.PS3BootArguments;
            }

            psInfo.Arguments += " " + this.Command;

            if (String.IsNullOrWhiteSpace(this.Arguments) == false)
            {
                psInfo.Arguments += " " + this.Arguments;
            }
             
            psInfo.CreateNoWindow = true;
            psInfo.UseShellExecute = false;

            m_ps3runProcess = Process.Start( psInfo );
            RegisterProcess(m_ps3runProcess);

            if ( m_ps3runProcess == null )
            {
                LogError( "Could not start process ps3run " + psInfo.Arguments + ".");
                return false;
            }

            return true;
        }

        private void ResetSystrayRfs()
        {
            if ( m_alreadyReset == false )
            {
                m_alreadyReset = true;

                m_target.Reset();

                m_hasExited = true;
            }
        }
        #endregion

        #region PS3Target Event Handlers
        private void target_ConnectError( object sender, MessageEventArgs e )
        {
            LogError( e.Message );
        }

        private void target_ConnectInfo( object sender, MessageEventArgs e )
        {
            Log( e.Message );
        }

        private void target_ConnectStatus( object sender, ConnectStatusEventArgs e )
        {
            if ( e.Status == ConnectStatus.NOT_CONNECTED )
            {
                if ( !m_waitForConnection )
                {
                    if ( !m_alreadyReset )
                    {
                        LogError( e.AsString() );
                    }

                    if ( m_waitingForReset )
                    {
                        m_waitingForReset = false;
                        m_hasExited = true;
                    }
                }
                else
                {
                    Log( e.AsString() );
                }
            }
            else if ( e.Status == ConnectStatus.CONNECTING )
            {
                m_waitForConnection = true;
                Log( e.AsString() );
            }
            else if ( e.Status == ConnectStatus.CONNECTED )
            {
                m_waitForConnection = false;
                Log( e.AsString() );
            }
            else
            {
                Log( e.AsString() );
            }
        }

        private void target_DebugEvent( object sender, DebugEventEventArgs e )
        {
            switch ( e.Event )
            {
                case DebugEvent.PPU_THREAD_CREATE:
                case DebugEvent.PPU_THREAD_EXIT:
                    Log( e.AsString() );
                    break;
                case DebugEvent.PPU_EXP_ALIGNMENT:
                case DebugEvent.PPU_EXP_DABR_MATCH:
                case DebugEvent.PPU_EXP_DATA_HTAB_MISS:
                case DebugEvent.PPU_EXP_DATA_SLB_MISS:
                case DebugEvent.PPU_EXP_FLOAT:
                case DebugEvent.PPU_EXP_ILL_INST:
                case DebugEvent.PPU_EXP_PREV_INT:
                case DebugEvent.PPU_EXP_TEXT_HTAB_MISS:
                case DebugEvent.PPU_EXP_TEXT_SLB_MISS:
                case DebugEvent.PPU_EXP_TRAP:
                    LogError( e.AsString() );
                    break;
                default:
                    break;
            }
        }

        private void target_TTY( object sender, MessageEventArgs e )
        {
            bool bLogFatalError = false;
            bool bLogError = false;
            bool bLogWarning = false;

            string strUpper = e.Message.ToUpper();
            strUpper = strUpper.Trim();

            if ( strUpper.Contains( "): FATAL ERROR : " ) || strUpper.StartsWith( "FATAL ERROR: " ) )
            {
                bLogFatalError = true;
            }
            if ( strUpper.Contains( "): ERROR : " ) || strUpper.StartsWith( "ERROR: " ) )
            {
                bLogError = true;
            }
            if ( strUpper.Contains( "): WARNING : " ) || strUpper.StartsWith( "WARNING: " ) )
            {
                bLogWarning = true;
            }

            if ( bLogFatalError || bLogError )
            {
                Log(e.Message);     //General message since the task didn't fail.
            }
            else if ( bLogWarning )
            {
                Log( e.Message );   //Routed to the general message because this is a warning from the game; not from the task itself.
            }
            else
            {
                Log( e.Message );
            }

            if (SuccessMessage != null && e.Message.Contains(SuccessMessage) == true)
            {
                m_CurrentCount++;

                Log("Success Message has been received! Count: " + m_CurrentCount);

                if (m_CurrentCount >= m_Count)
                {
                    Log("Signaling an exit.");
                    Result.Success = true;
                    m_hasExited = true;
                }
            }

            if (bLogFatalError && !this.PS3BootArguments.Contains("-noquits"))
            {
                ResetSystrayRfs();
            }
        }

        private void target_UnhandledCrash( object sender, MessageEventArgs e )
        {
            LogError( e.Message );
            Result.Success = false;
            
            ResetSystrayRfs();
        }

        private void target_UnitStatus( object sender, UnitStatusEventArgs e )
        {
            if ( e.Status == UnitStatus.NOT_CONNECTED )
            {
                Log( e.AsString() );

                if ( !m_alreadyReset )
                {
                    LogError( "Lost Connection to Target.  Unable to complete task." );
                    Thread.Sleep( 1000 );
                    Result.Success = false;
                }                

                if ( m_waitingForReset )
                {
                    m_waitingForReset = false;
                    m_hasExited = true;
                }
            }
            else if ( e.Status == UnitStatus.RESETTING )
            {
                Log( e.AsString() );

                m_waitingForReset = true;
                m_alreadyReset = true;
            }
            else if ( e.Status == UnitStatus.RESET )
            {
                Log( e.AsString() );

                if ( m_waitingForReset )
                {
                    m_waitingForReset = false;
                    m_hasExited = true;
                }
            }
            else
            {
                Log( e.AsString() );
            }
        }
        #endregion

        public override string ToReportName()
        {
            string safeTestName = TestName.Replace(" ", "_");
            return "PS3_" + safeTestName;
        }

        public override string ToDisplayName()
        {
            return "PS3 " + TestName;
        }

        public override string ToShortString()
        {
            return ToDisplayName();
        }

        public override string ToString()
        {
            return "PS3 Test " + TestName + ".";

        }
    }
}
