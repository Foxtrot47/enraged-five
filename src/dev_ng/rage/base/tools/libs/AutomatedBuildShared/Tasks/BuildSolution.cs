﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

using RSG.Base;
using RSG.Base.Command;
using RSG.Base.IO;
using System.Reflection;
using Microsoft.Win32;

namespace AutomatedBuildShared
{
    /// <summary>
    /// An MSBuild Task to build a Visual Studio solution given a partial path and name of a solution
    /// </summary>
    [Serializable]
    public class BuildSolution : BuildTask
    {
        public enum SolutionVersion
        {
            kNoVersion,
            k2005Solution,
            k2008Solution,
            k2010Solution,
        };

        #region Variables
        /// <summary>
        /// The solution to build.  At minimum, the format must be that of the old tools.txt:  
        /// %RAGE_DIR%\suite\samples\tools\rageViewer\rageViewer.  If needed, "_2005.sln" or ".sln" will be appended.
        /// It's best if the path root is specified, but should also work if the path is relative to the current 
        /// directory (or the CruiseControl config's WorkingDirectory).
        /// </summary>
        [XmlAttribute("Solution")]
        public string Solution;

        /// <summary>
        /// The name of the project to specifically target in the solution build.
        /// </summary>
        [XmlAttribute]
        public string Project;

        /// <summary>
        /// The name of the configuration or configurations to build, comma-separated
        /// </summary>
        [XmlAttribute]
        public string Configuration;

        /// <summary>
        /// Disabled the use of any distributed build system.  This is used for C# solutions.
        /// </summary>
        protected bool CompileWithDevenv = false;
        protected bool NoVSI = false;
        
        [XmlAttribute]
        public bool Clean = false;

        [XmlAttribute]
        public bool Rebuild = false;

        [XmlAttribute]
        public bool Disabled = false;

        #endregion

        static string m_SolutionSuffix = ".sln";
        static string m_2005SolutionSuffix = "_2005" + m_SolutionSuffix;
        static string m_2008SolutionSuffix = "_2008" + m_SolutionSuffix;
        static string m_2010SolutionSuffix = "_2010" + m_SolutionSuffix;

        private rageExecuteCommand m_Command;

        public BuildSolution()
        {
            Name = "Build Solution";
            Solution = null;
            Project = null;
            Configuration = null;
        }

        public BuildSolution(string solution, string project, string configuration)
        {
            Solution = solution;
            Project = project;
            Configuration = configuration;
        }

        public override bool Localize()
        {
            Solution = Environment.ExpandEnvironmentVariables(Solution);

            base.Localize();
            return true;
        }

        public override string ToDisplayName()
        {
            if (String.IsNullOrWhiteSpace(this.Project) == true)
                return Path.GetFileNameWithoutExtension(this.Solution) + " " + this.Configuration;
            else
                return Path.GetFileNameWithoutExtension(this.Solution) + "," + this.Project + " " + this.Configuration;
        }

        public override string ToReportName()
        {
            string displayName = ToDisplayName();
            displayName = displayName.Replace(" ", "_");
            displayName = displayName.Replace("|", "_");
            return displayName;
        }

        public override string ToShortString()
        {
            return "Solution " + Path.GetFileName(this.Solution) + " " + this.Configuration;
        }

        public override string ToString()
        {
            string solutionName = this.Solution;
            string solutionFile = Path.GetFileName(this.Solution);
            SolutionVersion versionType;
            GetSolutionFilename(ref solutionName, out versionType);

            if (Project == null)
            {
                if (Clean == true)
                    return "Cleaning solution " + solutionFile + ", " + Configuration;
                else
                    return "Building solution " + solutionFile + ", " + Configuration;
            }
            else
            {
                if ( Clean == true )
                    return "Cleaning solution " + solutionFile + ", " + Project + ", " + Configuration;
                else
                    return "Building solution " + solutionFile + ", " + Project + ", " + Configuration;
            }
        }

        /// <summary>
        /// Given a partial path and solution name, determines the actual filename
        /// </summary>
        /// <param name="solution">The partial path and solution name.</param>
        /// <returns>true if the solution exists and is returned by reference, otherwise false</returns>
        protected bool GetSolutionFilename( ref string solution, out SolutionVersion versionType )
        {
            solution = Environment.ExpandEnvironmentVariables(solution);
            versionType = SolutionVersion.kNoVersion;

            //If the solution has an extension already
            if (String.Compare(Path.GetExtension(solution), m_SolutionSuffix, true) == 0)
            {
                if (solution.Contains(m_2005SolutionSuffix) == true)
                    versionType = SolutionVersion.k2005Solution;
                else if (solution.Contains(m_2008SolutionSuffix) == true)
                    versionType = SolutionVersion.k2008Solution;
                else if (solution.Contains(m_2010SolutionSuffix) == true)
                    versionType = SolutionVersion.k2010Solution;

                if (versionType != SolutionVersion.kNoVersion)
                    return true;
            }

            string rawSolutionName = Path.ChangeExtension(solution, null);

            //Find 2010 projects first,a then 2008 projects, then 2005, then any nondescript one.
            string solution2010 = rawSolutionName + m_2010SolutionSuffix;
            if (File.Exists(solution2010))
            {
                solution = solution2010;
                versionType = SolutionVersion.k2010Solution;
                return true;
            }

            string solution2008 = rawSolutionName + m_2008SolutionSuffix;
            if (File.Exists(solution2008))
            {
                solution = solution2008;
                versionType = SolutionVersion.k2008Solution;
                return true;
            }

            string solution2005 = rawSolutionName + m_2005SolutionSuffix;
            if (File.Exists(solution2005))
            {
                solution = solution2005;
                versionType = SolutionVersion.k2005Solution;
                return true;
            }

            string solutionNoVersion = rawSolutionName + m_SolutionSuffix;
            if (File.Exists(solutionNoVersion))
            {
                solution = solutionNoVersion;

                //Resort to reading the first line of the solution file.
                StreamReader sr = File.OpenText(solution);
                const int numLinesToCheck = 2;  //Some solutions have newlines at the top.  Check the first three lines for a signature.
                for (int lineIndex = 0; lineIndex < numLinesToCheck; ++lineIndex)
                {
                    if (sr.EndOfStream == false)
                    {
                        string line = sr.ReadLine();

                        if (String.IsNullOrEmpty(line) == false)
                        {
                            if (line == "Microsoft Visual Studio Solution File, Format Version 9.00")
                                versionType = SolutionVersion.k2005Solution;
                            else if (line == "Microsoft Visual Studio Solution File, Format Version 10.00")
                                versionType = SolutionVersion.k2008Solution;
                            else if (line == "Microsoft Visual Studio Solution File, Format Version 11.00")
                                versionType = SolutionVersion.k2010Solution;
                        }
                    }
                }

                sr.Close();

                if (versionType == SolutionVersion.kNoVersion)
                    return false;

                return true;
            }

            //Unable to find a correct solution!
            return false;
        }

        public override bool Execute()
        {
            if (this.Disabled == true)
            {
                Log("Solution " + Solution + "|" + Configuration + " is disabled.");
                return true;
            }

            if ( String.IsNullOrEmpty( this.Solution ) )
            {
                Result.Success = false;
                return false;
            }

            // Transform this.Solution into the actual file name, if needed
            string strSolution = this.Solution;
            SolutionVersion version;
            if ( !GetSolutionFilename( ref strSolution, out version ) )
            {
                if ( version == SolutionVersion.kNoVersion )
                    LogError( strSolution + " could not be found or its Visual Studio version could not be determined.");
                else
                    LogError( strSolution + " does not exist.");

                Result.Success = false;
                return true;
            }

            this.Solution = strSolution;

            if ( String.IsNullOrEmpty( this.Configuration ) )
            {
                LogError( "BuildRageSolution: No configuration was specified for " + this.Solution + ".");
                Result.Success = false;
                return true;
            }

            rageStatus obStatus;

            string[] configurationList = this.Configuration.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach(string configuration in configurationList)
            {
                DateTime startTime = DateTime.Now;

                // build the solution
                BuildProject(configuration, out obStatus);
                if (!obStatus.Success())
                {
                    //Set the IsSuccessful property in the MS Build as false.  Later on, MS Build will notify
                    //Cruise Control on success/failure using this variable.
                    //
                    Result.Success = false;

                    return true;
                }

                DateTime endTime = DateTime.Now;
                TimeSpan difference = endTime - startTime;

                Log("Time Taken: " + difference.ToString());
            }

            Result.Success = true;
            return true;
        }

        #region Private Functions
        /// <summary>
        /// Given the solution of a particular Visual Studio type, return the path to the Visual Studio executable.
        /// </summary>
        /// <param name="solution">The partial path and solution name.</param>
        /// <returns>string name of the Visual Studio executable; nil if it is of an indeterminable version.</returns>
        private string GetDevEnvExecutable(string solution)
        {
            SolutionVersion version;
            if (GetSolutionFilename(ref solution, out version) == false)
                return null;

            string devEnvPath = null;
            if (version == SolutionVersion.k2005Solution)
            {
                devEnvPath = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\ide\\devenv.com";
                if (File.Exists(devEnvPath) == false)
                    devEnvPath = "C:\\Program Files (x86)\\Microsoft Visual Studio 8\\Common7\\ide\\devenv.com";
            }
            else if (version == SolutionVersion.k2008Solution)
            {
                devEnvPath = "C:\\Program Files\\Microsoft Visual Studio 9.0\\Common7\\ide\\devenv.com";
                if (File.Exists(devEnvPath) == false)
                    devEnvPath = "C:\\Program Files (x86)\\Microsoft Visual Studio 9.0\\Common7\\ide\\devenv.com";
            }
            else if (version == SolutionVersion.k2010Solution
                    || version == SolutionVersion.kNoVersion)
            {
                devEnvPath = "C:\\Program Files\\Microsoft Visual Studio 10.0\\Common7\\ide\\devenv.com";
                if (File.Exists(devEnvPath) == false)
                    devEnvPath = "C:\\Program Files (x86)\\Microsoft Visual Studio 10.0\\Common7\\ide\\devenv.com";
            }

            return devEnvPath;
        }

        /// <summary>
        /// Acquires the command and arguments to call the devenv processes.
        /// </summary>
        private void GetDevEnvBuildCommand(string configuration, out string strCmd, out StringBuilder strArgs)
        {
             // Create command for the normal Visual Studio environment.
            strArgs = new StringBuilder();
            strCmd = GetDevEnvExecutable(this.Solution);

            strArgs.Append("\"");
            strArgs.Append(this.Solution);

            if ( this.Clean )
            {
                strArgs.Append( "\" /clean \"" );
            }
            else if (this.Rebuild == true)
            {
                strArgs.Append("\" /rebuild \"");
            }
            else
            {
                strArgs.Append("\" /build \"");
            }

            strArgs.Append(configuration);
            strArgs.Append("\"");

            if (!String.IsNullOrEmpty(this.Project))
            {
                strArgs.Append(" /Project \"");
                strArgs.Append(this.Project);
                strArgs.Append("\"");
            }
        }

        private bool GetProjectRelativePath(string solutionPath, string projectName, out string relativePath)
        {
            relativePath = null;

            if (File.Exists(solutionPath) == false)
            {
                return false;
            }

            string loweredProjectName = projectName.ToLower();
            StreamReader inStream = File.OpenText(solutionPath);
            string inString = inStream.ReadLine();
            while (inString != null)
            {
                if (inString.ToLower().Contains(loweredProjectName) == true)
                {
                    //The following line is in the format:
                    //Project("{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}") = "MetadataEditor", "..\MetadataEditor\MetadataEditor_2005.vcproj", "{4F18D2F2-925E-4788-ADDD-4B45404D0352}"
                    //
                    string[] splitByComma = inString.Split(',');
                    if (splitByComma.Length > 1)
                    {
                        relativePath = splitByComma[1];
                        relativePath = relativePath.Trim();
                        relativePath = relativePath.Trim('"');
                        return true;
                    }
                }
                inString = inStream.ReadLine();
            }

            return false;
        }

        /// <summary>
        /// Given a partial project path, determines the actual filename of the project.
        /// Use the version of the solution to assume which project is used, based on versions.
        /// </summary>
        private bool GetProjectFilename(string solutionPath, string projectName, out string outProjectPath)
        {
            outProjectPath = null;

            SolutionVersion version;
            if (GetSolutionFilename(ref solutionPath, out version) == false)
            {
                return false;
            }

            if (String.IsNullOrEmpty(projectName) == true)
            {
                return true;
            }

            string basePath = Path.GetDirectoryName(solutionPath);
            string relativePathToProject;
            if (GetProjectRelativePath(solutionPath, projectName, out relativePathToProject) == false)
            {
                return false;
            }

            string projectPath = Path.Combine(basePath, relativePathToProject);

            if (File.Exists(projectPath) == true)
            {
                outProjectPath = projectPath;
                return true;
            }

            return false;
        }
        
        /// <summary>
        /// Concatenates the given non-null and non-empty string parts into a log html filename, prefixed with a time-based filename.
        /// The resulting string is cleaned of illegal filename characters before returning.
        /// </summary>
        /// <param name="part1"></param>
        /// <param name="part2"></param>
        /// <param name="part3"></param>
        /// <param name="part4"></param>
        /// <param name="part5"></param>
        /// <returns>The log filename.</returns>
        private string BuildLogFileName(string part1, string part2, string part3, string part4, string part5)
        {
            StringBuilder strLogFileName = new StringBuilder(rageFileUtilities.GenerateTimeBasedFilename());

            if (!String.IsNullOrEmpty(part1))
            {
                strLogFileName.Append('_');
                strLogFileName.Append(part1.Replace(' ', '_'));
            }

            if (!String.IsNullOrEmpty(part2))
            {
                strLogFileName.Append('_');
                strLogFileName.Append(part2.Replace(' ', '_'));
            }

            if (!String.IsNullOrEmpty(part3))
            {
                strLogFileName.Append('_');
                strLogFileName.Append(part3.Replace(' ', '_'));
            }

            if (!String.IsNullOrEmpty(part4))
            {
                strLogFileName.Append('_');
                strLogFileName.Append(part4.Replace(' ', '_'));
            }

            if (!String.IsNullOrEmpty(part5))
            {
                strLogFileName.Append('_');
                strLogFileName.Append(part5.Replace(' ', '_'));
            }

            strLogFileName.Append(".html");

            return rageFileUtilities.CleanStringOfBadCharacters(strLogFileName.ToString());
        }
        /// <summary>
        /// Builds the project with the given parameters.
        /// </summary>
        /// <param name="strBuildLog">The verbose log that was created.  if !ShouldKeepBuildLog(), you must delete it yourself when done.</param>
        /// <param name="obStatus">The success of failure of the build.</param>
        private void BuildProject(string configuration, out rageStatus obStatus )
        {
            Environment.SetEnvironmentVariable( "XBECOPY_SUPPRESS_COPY", "1" );

            // Construct build command
            string strCmd = string.Empty;
            StringBuilder strArgs = new StringBuilder();
            bool bUseIncrediBuild = !this.CompileWithDevenv && !configuration.ToUpper().Contains( "PS3" );
            bool bUseVsi = !this.NoVSI && configuration.ToUpper().Contains("PS3");
            bool bVsiUseDbs = !this.CompileWithDevenv && bUseVsi;

            if ( bUseIncrediBuild )
            {
                string incrediBuildPath = "C:\\Program Files\\Xoreax\\IncrediBuild\\BuildConsole.exe";
                if ( File.Exists(incrediBuildPath) == false )
                    incrediBuildPath = "C:\\Program Files (x86)\\Xoreax\\IncrediBuild\\BuildConsole.exe";

                if ( File.Exists(incrediBuildPath) == false)
                {
                    LogError(String.Format("Unable to find IncrediBuild application.  Switching to Visual Studio."));
                    GetDevEnvBuildCommand(configuration, out strCmd, out strArgs);

                    if (File.Exists(strCmd) == false)
                    {
                        string errorMessage = String.Format("Unable to find " + strCmd + "!");
                        LogError(errorMessage);
                        obStatus = new rageStatus(errorMessage);
                        return;
                    }
                }
                else
                {
                    strCmd = "\"" + incrediBuildPath + "\"";

                    strArgs.Append("/SHOWAGENT ");

                    if (this.Rebuild == true)
                        strArgs.Append("/rebuild ");
                    else if (this.Clean == true)
                        strArgs.Append("/clean ");
                    else
                        strArgs.Append("/build ");

                    strArgs.Append("/cfg=\"");
                    strArgs.Append(configuration);
                    strArgs.Append("\"");

                    if (!String.IsNullOrEmpty(this.Project))
                    {
                        string projectName = null;
                        if (GetProjectFilename(this.Solution, this.Project, out projectName) == false)
                            projectName = this.Project;
                        else
                        {
                            //IncrediBuild is expecting only the name of the project as it is referenced
                            //in the solution.  By the standard, this requires the name to be like "projectName_2008".
                            projectName = Path.GetFileNameWithoutExtension(projectName);
                        }

                        strArgs.Append(" /prj=\"");
                        strArgs.Append(projectName);
                        strArgs.Append("\"");
                    }

                    strArgs.Append(" \"");
                    strArgs.Append(this.Solution);
                    strArgs.Append("\"");
                }
            }
            else if ( bUseVsi )
            {
                string vsiCommand = "C:\\Program Files\\SN Systems\\Common\\VSI\\bin\\vsibuild.exe";
                if (File.Exists(vsiCommand) == false)
                     vsiCommand = "C:\\Program Files (x86)\\SN Systems\\Common\\VSI\\bin\\vsibuild.exe";

                if ( File.Exists(vsiCommand) == false )
                    vsiCommand = @"C:\Program Files (x86)\SCE\Common\VSI\bin\vsibuild.exe";

                 if (File.Exists(vsiCommand) == false)
                {
                    string errorMessage = "Unable to find SN Systems vsibuild.exe application.  Unable to continue.";
                    LogError(errorMessage);
                    obStatus = new rageStatus(errorMessage);
                    return;
                }

                strCmd = "\"" + vsiCommand + "\"";

                strArgs.Append( "\"" );
                strArgs.Append( this.Solution );
                strArgs.Append( "\" \"" );
                strArgs.Append(configuration);
                strArgs.Append( "\" " );

                if ( !String.IsNullOrEmpty( this.Project ) )
                {
                    strArgs.Append( "/project \"" );
                    strArgs.Append( this.Project );
                    strArgs.Append( "\" " );
                }

                if (this.Rebuild == true)
                    strArgs.Append("/rebuild ");
                else if (this.Clean == true)
                    strArgs.Append("/clean ");
                else
                    strArgs.Append("/build ");

                if ( bVsiUseDbs )
                {
                    bool bVsiUseIncredibuild = true;

                    if ( bVsiUseIncredibuild )
                    {
                        strArgs.Append( " /incredi" );
                    }
                    else
                    {
                        strArgs.Append( " /sn-dbs" );
                    }
                }
            }
            else
            {
                GetDevEnvBuildCommand(configuration, out strCmd, out strArgs);

                if (File.Exists(strCmd) == false)
                {
                    string errorMessage = String.Format("Unable to find " + strCmd + "!");
                    LogError(errorMessage);
                    obStatus = new rageStatus(errorMessage);
                    return;
                }
            }

            if ( String.IsNullOrEmpty(strCmd) == true)
            {
                string errorMessage = "Resulting command for BuildSolution task is empty or null.  Unable to continue...";
                LogError(errorMessage);
                obStatus = new rageStatus(errorMessage);
                return;
            }

            m_Command = new rageExecuteCommand();
            m_Command.Command = strCmd;
            m_Command.Arguments = strArgs.ToString();
            m_Command.TimeOutInSeconds = 2.0f * 60.0f * 60.0f;		// Time out after two hours
            m_Command.UpdateLogFileInRealTime = false;

            m_Command.EchoToConsole = false;
            m_Command.RemoveLog = true;
            m_Command.UseBusySpinner = false;

            Log(m_Command.GetDosCommand());

            RegisterProcess(m_Command);
            m_Command.Execute(out obStatus);

            // Ok, build complete, did it work?
            obStatus = new rageStatus();

            // Look back through log to find conclusion
            List<string> astrBuildLogAsText = m_Command.GetLogAsArray();
            SolutionOutput slnOutput = SolutionOutput.Parse(this.Solution, configuration, astrBuildLogAsText);

            string errorString = "";
            string warningString = "";
            bool hasErrors = false;
            bool hasWarnings = false;
            const int numExtraLines = 1;
            for (int lineIndex = 0; lineIndex < slnOutput.OutputLines.Count; ++lineIndex)
            {
                OutputLine outLine = slnOutput.OutputLines[lineIndex];
                if (outLine.Logged == true)
                    continue;

                switch (outLine.Type)
                {
                    case OutputLine.OutputType.Error:
                    case OutputLine.OutputType.WarningTreatedAsError:
                    case OutputLine.OutputType.WarningsTreatedAsErrors:
                        {
                            if (outLine.Text.Contains("Failed to connect to Primary Coordinator") == true)
                                continue;

                            errorString += outLine.Text + "\n";
                            for (int nextLineIndex = 0; nextLineIndex < numExtraLines; ++nextLineIndex)
                            {
                                int arrayIndex = nextLineIndex + lineIndex + 1;
                                if (arrayIndex < slnOutput.OutputLines.Count)
                                {
                                    OutputLine nextLine = slnOutput.OutputLines[arrayIndex];
                                    errorString += nextLine.Text + "\n";
                                    nextLine.Logged = true;
                                }
                            }

                            hasErrors = true;
                            outLine.Logged = true;
                        }
                        break;
                    case OutputLine.OutputType.Warning:
                        {
                            warningString += outLine.Text + "\n";
                            for (int nextLineIndex = 0; nextLineIndex < numExtraLines; ++nextLineIndex)
                            {
                                int arrayIndex = nextLineIndex + lineIndex + 1;
                                if (arrayIndex < slnOutput.OutputLines.Count)
                                {
                                    OutputLine nextLine = slnOutput.OutputLines[arrayIndex];
                                    warningString += nextLine.Text + "\n";
                                    nextLine.Logged = true;
                                }
                            }

                            hasWarnings = true;
                            outLine.Logged = true;
                        }
                        break;
                    case OutputLine.OutputType.Normal:
                        {
                            Log(outLine.Text);
                            outLine.Logged = true;
                        }
                        break;
                }
            }

            string solutionHeader = null;
            if (String.IsNullOrEmpty(this.Project) == false)
            {
                solutionHeader = String.Format("Solution: {0}, Project: {1}, Configuration: {2}:", this.Solution, this.Project, configuration);
            }
            else
            {
                solutionHeader = String.Format("Solution: {0}, Configuration: {1}:", this.Solution, configuration);
            }

            if ( hasErrors == true )
            {
                LogError(solutionHeader + "\n" + errorString);
            }
            else if ( slnOutput.FailCount != 0 )
            {
                // Make sure we print out an error, if one occurred even though we may not have found an error string.
                LogError(obStatus.ErrorString);
            }

            if (hasWarnings == true)
            {
                LogWarning(solutionHeader + "\n" + warningString);
            }

            if (slnOutput.FailCount != 0 || hasErrors == true)
            {
                obStatus = new rageStatus(String.Format("{0}, {1}: Error occurred during build.", Path.GetFileName(this.Solution), configuration));
                if ( (slnOutput.SucceedCount == -1) && (slnOutput.FailCount == -1) && (slnOutput.SkipCount == -1) )
                {
                    obStatus = new rageStatus(
                        String.Format("{0}, {1}: The build ended unexpectedly.", Path.GetFileName(this.Solution), configuration));
                }
            }
 
            Log( slnOutput.SucceedCount + " Succeeded    " + slnOutput.FailCount + " Failed    " + slnOutput.SkipCount + " Skipped.");

            Environment.SetEnvironmentVariable( "XBECOPY_SUPPRESS_COPY", "0" );
        }
        #endregion
    }

    class OutputLine
    {
        public OutputLine()
        {

        }

        public OutputLine( OutputType outputType, string filename, int lineNumber, string text,
            bool processTimedOut, int projectID )
        {
            m_outputType = outputType;
            m_filename = filename;
            m_lineNumber = lineNumber;
            m_text = text;
            m_processTimedOut = processTimedOut;
            m_projectID = projectID;
            m_logged = false;
        }

        public enum OutputType
        {
            Normal,
            Warning,
            Error,
            WarningTreatedAsError,
            WarningsTreatedAsErrors
        }

        #region Variables
        private OutputType m_outputType = OutputType.Normal;
        private string m_filename = null;
        private int m_lineNumber = -1;
        private string m_text = null;
        private bool m_processTimedOut = false;
        private int m_projectID = -1;
        private bool m_logged;

        private static Regex sm_fileLineRegex = new Regex( "\\s*(?<File>[a-zA-Z_0-9\\x2F\\x2E\\x3A\\x5C\\x20]*)\\x28(?<lineNum>[0-9]+)\\x29\\s*:\\s*(?<Text>(.)*)", RegexOptions.Compiled );
        #endregion

        #region Properties
        public OutputType Type
        {
            get
            {
                return m_outputType;
            }
            set
            {
                m_outputType = value;
            }
        }

        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                m_filename = value;
            }
        }

        public int LineNumber
        {
            get
            {
                return m_lineNumber;
            }
            set
            {
                m_lineNumber = value;
            }
        }

        public string Text
        {
            get
            {
                return m_text;
            }
            set
            {
                m_text = value;
            }
        }

        public bool Logged
        {
            get
            {
                return m_logged;
            }
            set
            {
                m_logged = value;
            }
        }

        public bool ProcessTimedOut
        {
            get
            {
                return m_processTimedOut;
            }
            set
            {
                m_processTimedOut = value;
            }
        }

        public int ProjectID
        {
            get
            {
                return m_projectID;
            }
            set
            {
                m_projectID = value;
            }
        }
        #endregion

        #region Public Functions
        public static OutputLine Parse( string strLogLine )
        {
            string strUppercaseLogLine = strLogLine.ToUpper();

            OutputType outputType = OutputType.Normal;
            string filename = "";
            int lineNumber = -1;
            bool processTimedOut = false;
            int projID = -1;

            int indexOf = strLogLine.IndexOf( ">" );
            if ( (indexOf != -1) && (indexOf < 4) )
            {
                if (int.TryParse(strLogLine.Substring(0, indexOf), out projID) == true)
                    projID--;
                else //unsuccessful parsing
                    return null;

            }

            if ( !strUppercaseLogLine.Contains( " ERROR(S) " ) && !strUppercaseLogLine.Contains( "WARNING(S)" ) )
            {
                if ( strUppercaseLogLine.Contains( "WARNINGS BEING TREATED AS ERRORS" ) 
                    || strUppercaseLogLine.Contains( "WARNING TREATED AS ERROR" ) 
                    || strUppercaseLogLine.Contains( "WARNING AS ERROR" ) )
                {
                    outputType = OutputType.WarningsTreatedAsErrors;
                }
                else if ( (strUppercaseLogLine.IndexOf( " WARNING " ) != -1) || (strUppercaseLogLine.IndexOf( "): WARNING:" ) != -1)
                    || strUppercaseLogLine.StartsWith( "WARNING " ) || strUppercaseLogLine.StartsWith( "WARNING: " ) )
                {
                    if ( !strUppercaseLogLine.Contains( "XBECOPY_SUPPRESS_COPY" ) )
                    {
                        outputType = OutputType.Warning;
                    }
                }
                else if ( (strUppercaseLogLine.IndexOf( " ERROR " ) != -1) || (strUppercaseLogLine.IndexOf( "): ERROR:" ) != -1)
                    || strUppercaseLogLine.StartsWith( "ERROR " ) || strUppercaseLogLine.StartsWith( "ERROR: " )
                    || strUppercaseLogLine.StartsWith( "FAILED " )
                    || (strUppercaseLogLine.IndexOf( "FATAL ERROR: " ) != -1) )
                {
                    outputType = OutputType.Error;
                }
                else if ( strUppercaseLogLine.IndexOf( "PROCESS TIMED OUT." ) != -1 )
                {
                    outputType = OutputType.Error;
                    processTimedOut = true;
                }
                else if ( strUppercaseLogLine.IndexOf( "PROJECT CONFIGURATION NOT FOUND: " ) != -1 )
                {
                    outputType = OutputType.Error;
                }
                else if ( strUppercaseLogLine.Contains( "FAILED TO INITIATE BUILD" ) )
                {
                    outputType = OutputType.Error;
                }
                else if ( strUppercaseLogLine.Contains( "ERROR WAITING FOR DBSBUILD" ) )
                {
                    outputType = OutputType.Error;
                }

                Match match = sm_fileLineRegex.Match( strLogLine );
                if ( match.Success )
                {
                    filename = match.Result( "${File}" );

                    string lineNumStr = match.Result( "${lineNum}" );
                    try
                    {
                        lineNumber = Convert.ToInt32( lineNumStr ) - 1;
                    }
                    catch
                    {
                    	
                    }
                }
            }

            return new OutputLine( outputType, filename, lineNumber, strLogLine, processTimedOut, projID );
        }
        #endregion
    }

    class SolutionOutput
    {
        public SolutionOutput()
        {

        }

        public SolutionOutput( string name, string config )
        {
            m_name = name;
            m_config = config;
        }

        #region Variables
        private string m_name = null;
        private string m_config = null;
        private int m_numSucceeded = -1;
        private int m_numFailed = -1;
        private int m_numUpToDate = -1;
        private int m_numSkipped = -1;
        List<OutputLine> m_outputLines = new List<OutputLine>();
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        public string Config
        {
            get
            {
                return m_config;
            }
            set
            {
                m_config = value;
            }        
        }

        public int SucceedCount
        {
            get
            {
                return m_numSucceeded;
            }
            set
            {
                m_numSucceeded = value;
            }
        }

        public int FailCount
        {
            get
            {
                return m_numFailed;
            }
            set
            {
                m_numFailed = value;
            }
        }

        public int UpToDateCount
        {
            get
            {
                return m_numUpToDate;
            }
            set
            {
                m_numUpToDate = value;
            }
        }

        public int SkipCount
        {
            get
            {
                return m_numSkipped;
            }
            set
            {
                m_numSkipped = value;
            }
        }

        public List<OutputLine> OutputLines
        {
            get
            {
                return m_outputLines;
            }
            set
            {
                m_outputLines = value;
            }
        }

        #endregion

        #region Public Functions
        public static SolutionOutput Parse( string name, string config, List<string> strLogLines )
        {
            SolutionOutput slnOutput = new SolutionOutput( name, config );

            foreach ( string strLogLine in strLogLines )
            {
                OutputLine outLine = OutputLine.Parse( strLogLine );
                if ( outLine != null )
                {
                    string strUppercaseText = outLine.Text.ToUpper();

                    if ( (strUppercaseText.Contains( "SUCCEEDED" ) && strUppercaseText.Contains( "FAILED" )
                        && strUppercaseText.Contains( "SKIPPED" )) 
                        || strUppercaseText.Contains( "---------------------- DONE ----------------------" ) )
                    {
                        string[] parts = strUppercaseText.Split( new char[] { ' ' } );
                        for ( int i = 1; i < parts.Length; ++i )
                        {                            
                            string part = parts[i];

                            if ( part.StartsWith( "SUCCEEDED" ) )
                            {
                                if (int.TryParse(parts[i - 1], out slnOutput.m_numSucceeded) == false)
                                    slnOutput.SucceedCount = -1;
                            }
                            else if ( part.StartsWith( "FAILED" ) )
                            {
                                if (int.TryParse(parts[i - 1], out slnOutput.m_numFailed) == false)
                                    slnOutput.FailCount = -1;
                            }
                            else if ( part.StartsWith( "UP-TO-DATE" ) )
                            {
                                if ( parts[i - 1] == "OR" )
                                {
                                    slnOutput.UpToDateCount = slnOutput.SucceedCount;
                                }
                                else
                                {
                                    if (int.TryParse(parts[i - 1], out slnOutput.m_numUpToDate) == false)
                                        slnOutput.UpToDateCount = -1;
                                }
                            }
                            else if ( part.StartsWith( "SKIPPED" ) )
                            {
                                if (int.TryParse(parts[i - 1], out slnOutput.m_numSkipped) == false)
                                    slnOutput.SkipCount = -1;
                            }
                            else if ( part.StartsWith( "DONE" ) )
                            {
                                slnOutput.SucceedCount = 0;
                                slnOutput.FailCount = 0;
                                slnOutput.UpToDateCount = 0;
                                slnOutput.SkipCount = 0;
                            }
                        }
                    }

                    slnOutput.OutputLines.Add( outLine );
                }
            }

            return slnOutput;
        }

        #endregion
    }
}
