﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Xml.Serialization;

using P4API;

namespace AutomatedBuildShared
{
    [Serializable]
    public class ClientStatus : BuildTask
    {
        public string ActiveClientAddress; //This is the IP of the master server organizing tasks.
        public string ActiveClientPort;
        public string TargetMachine;  //The name of the build client's machine.
        public DateTime Timestamp; //The last time the status has been updated.
        public ClientState State;

        public ClientStatus()
        {
            ActiveClientAddress = null;
            ActiveClientPort = null;
            TargetMachine = null;
            State = ClientState.Unknown;
            Timestamp = DateTime.UtcNow;
        }

        public override bool Execute()
        {
            return false;
        }

        public override string ToString()
        {
            return ""; // "Querying client status for " + TargetMachine + ".";
        }
    }

    [Serializable]
    public class PerforceInitializationTask : BuildTask
    {
        [XmlAttribute("BuildLabel")]
        public string m_BuildLabel;

        [XmlAttribute("SourceMachine")]
        public string m_SourceMachine;

        [XmlArrayItem("ShelvedChangelist")]
        public List<int> m_ShelvedChangelists;

        public PerforceInitializationTask()
        {
            Name = "Perforce Initialization";
            m_BuildLabel = null;
            m_SourceMachine = null;
            m_ShelvedChangelists = null;
        }

        public PerforceInitializationTask(string buildLabel, string sourceMachine, List<int> shelvedChangelists)
        {
            Name = "Perforce Initialization";
            m_BuildLabel = buildLabel;
            m_SourceMachine = sourceMachine;
            m_ShelvedChangelists = shelvedChangelists;
        }

        public override bool Execute()
        {
            string ChangelistDescription = "Automated Build Client";
            if (m_BuildLabel != null)
            {
                ChangelistDescription += " - " + m_BuildLabel;
            }

            PerforceInfo.Activate();

            //Login if the password has been specified.
            if (PerforceInfo.m_Password != null)
            {
                try
                {
                    PerforceInfo.m_P4.Login(PerforceInfo.m_Password);
                }
                catch(Exception)
                {
                    Result.Success = false;
                    Result.LogError("Unable to login into Perforce.");
                    return Result.Success;
                }
            }

            if (m_ShelvedChangelists != null && m_ShelvedChangelists.Count > 0)
            {
                if (String.Compare(m_SourceMachine, Environment.MachineName) == 0)
                {
                    LogWarning("Changelists will not be unshelved on the source machine's build client.");
                    LogWarning("Any local changes not include in the shelved change list will be factored in this build client's results.");
                }
                else
                {
                    string UnshelvedDescription = ChangelistDescription + " - Unshelved Changes: ";
                    for (int changeIndex = 0; changeIndex < m_ShelvedChangelists.Count; changeIndex++)
                    {
                        UnshelvedDescription += m_ShelvedChangelists[changeIndex];

                        if (changeIndex != m_ShelvedChangelists.Count - 1)
                        {
                            UnshelvedDescription += ", ";
                        }
                    }

                    PerforceInfo.m_ShelvedChangelist = new Changelist(PerforceInfo.m_P4);
                    PerforceInfo.m_ShelvedChangelist.Create(UnshelvedDescription);
                    foreach (int shelvedChangelist in m_ShelvedChangelists)
                    {
                        //Unshelve each change into a single change managed by the Build Client.
                        PerforceInfo.m_P4.Run("unshelve", "-s", shelvedChangelist.ToString(), "-f", "-c", PerforceInfo.m_ShelvedChangelist.m_Number.ToString(), "//...");
                    }
                }
            }

            //Clean up any previous changes from this build.
            PerforceCleanupTask cleanupTask = new PerforceCleanupTask();
            cleanupTask.m_DescriptionIdentifier = ChangelistDescription;
            cleanupTask.PerforceInfo = this.PerforceInfo;
            if (cleanupTask.Execute() == false)
            {
                Result.LogWarning("Unable to clean up changelists with identifier \"" + ChangelistDescription + "\"");
            }

            Result.Combine(cleanupTask.Result);

            //Create a change that will hold the generated files from this build.
            PerforceInfo.m_Changelist = new Changelist(PerforceInfo.m_P4);
            if (PerforceInfo.m_Changelist.Create(ChangelistDescription) == false)
            {
                Result.Success = false;
                Result.LogError("Unable to create changelist.");
                return Result.Success;
            }

            Result.Success = true;
            return Result.Success;
        }

        public override string ToReportName()
        {
            return "Perforce_Initialization-" + this.m_SourceMachine;
        }

        public override string ToShortString()
        {
            return "Initializing...";
        }

        public override string ToString()
        {
            return "Client Initialization";
        }

    }

    //Clean up all changelists related to the Automated Build.  
    //This is essentially a catch-up task used while testing tasks in the Automated Build,
    //preventing any files from being locally changed and processed.  Occasionally if an abort
    //fails or there's a power outage, it's useful to ensure all the computers are working from 
    //a clean environment without manually checking in each system.
    [Serializable]
    public class PerforceCleanupTask : BuildTask
    {
        [XmlAttribute("DescriptionIdentifier")]
        public string m_DescriptionIdentifier;

        public PerforceCleanupTask()
        {
            m_DescriptionIdentifier = null;
        }

        public override bool Execute()
        {
            Result.Success = true;

            P4RecordSet changesRecord = PerforceInfo.m_P4.Run("changes", "-l", "-s", "pending", "-u", PerforceInfo.m_Client, "-c", PerforceInfo.m_Workspace);
            foreach(P4Record record in changesRecord)
            {
                if (record["desc"].ToLower().Contains(m_DescriptionIdentifier.ToLower()) == true)
                {
                    Result.LogMessage("Reverting changelist " + record["change"] + "...");
                    P4RecordSet revertRecord = PerforceInfo.m_P4.Run("revert", "-c", record["change"].ToString(), "//...");

                    string changelistNumber = record["change"].ToString();

                    if (revertRecord.HasErrors() == true)
                    {
                        LogError("Unable to revert files in changelist " + changelistNumber + ".");
                        Result.Success = false;
                    }

                    P4RecordSet deleteShelvedRecord = PerforceInfo.m_P4.Run("shelve", "-d", "-c", changelistNumber, "//...");
                    if (deleteShelvedRecord.HasErrors() == true)
                    {
                        //An error will occur if there is no files were shelved; ignore it for now.
                        //LogWarning("Unable to delete shelved files from changelist " + changelistNumber + ".");
                    }

                    P4RecordSet deleteRecord = PerforceInfo.m_P4.Run("change", "-d", changelistNumber);
                    if (deleteRecord.HasErrors() == true)
                    {
                        LogError("Unable to delete changelist " + changelistNumber + ".");
                        Result.Success = false;
                    }
                }
            }

            return Result.Success;
        }

        public override string ToReportName()
        {
            return "Perforce_Cleanup";
        }

        public override string ToShortString()
        {
            return "Cleaning up...";
        }

        public override string ToString()
        {
            return "Perforce Cleanup";
        }
    }

    [Serializable]
    public class ShelveChangelistTask : BuildTask
    {
        public int ChangelistNumber;

        [XmlAttribute("DestinationMachine")]
        public string m_DestinationMachine;

        [XmlAttribute("DestinationChangelist")]
        public int m_DestinationChangelist;

        public ShelveChangelistTask()
        {
            ChangelistNumber = -1;
            m_DestinationMachine = null;
            m_DestinationChangelist = -1;
        }

        public ShelveChangelistTask(string destinationMachineName, int destinationChangelist)
        {
            ChangelistNumber = -1;
            m_DestinationMachine = destinationMachineName;
            m_DestinationChangelist = destinationChangelist;
        }

        public override bool Execute()
        {
            if (PerforceInfo.m_Changelist == null)
            {
                LogError("No changelist to shelve exists.");
                Result.Success = false;
                return Result.Success;
            }

            if (Environment.MachineName == m_DestinationMachine)
            {
                //In the event that the build client and the coordinating machine are the same machine,
                //reopen these files instead of shelving them.
                PerforceInfo.m_Changelist.Reopen(m_DestinationChangelist);
            }
            else
            {
                bool shelveResult = PerforceInfo.m_Changelist.Shelve();
                LogPerforceRecords(PerforceInfo.m_Changelist.m_LastRecordSet);
                if (shelveResult == false)
                {
                    LogError("Unable to shelve changelist " + PerforceInfo.m_Changelist.m_Number + ".");
                    Result.Success = false;
                    return Result.Success;
                }

                bool revertResult = PerforceInfo.m_Changelist.Revert();
                LogPerforceRecords(PerforceInfo.m_Changelist.m_LastRecordSet);
                if (revertResult == false)
                {
                    LogError("Unable to revert changelist " + PerforceInfo.m_Changelist.m_Number + ".");
                    Result.Success = false;
                    return Result.Success;
                }
            }

            Result.Success = true;
            return Result.Success;
        }

        public override string ToDisplayName()
        {
            return "Shelving " + Environment.MachineName;
        }

        public override string ToReportName()
        {
            return "Perforce_Shelve-" + Environment.MachineName;
        }

        public override string ToShortString()
        {
            return "Shelving Change " + ChangelistNumber;
        }

        public override string ToString()
        {
            return "Shelving Change " + ChangelistNumber;
        }
    }

    [Serializable]
    public class SubmitChangelistTask : BuildTask
    {
        public int ChangelistNumber;

        public SubmitChangelistTask()
        {
            ChangelistNumber = -1;
        }

        public override bool Execute()
        {
            Result.Success = true;
            return Result.Success;
        }

        public override string ToDisplayName()
        {
            return "Submitting...";
        }
        public override string ToReportName()
        {
            return "Perforce_Submit";
        }

        public override string ToShortString()
        {
            return "Submitting Change " + ChangelistNumber;
        }

        public override string ToString()
        {
            return "Submit Changelist " + ChangelistNumber;
        }
    }

    [Serializable]
    public class DisconnectTask : BuildTask
    {
        public string BuildName;
        public bool RevertChangelist;  //Used to signal whether the client should preserve it's locally built files because of an error with shelving/unshelving.

        public DisconnectTask() 
        {
            BuildName = null;
            RevertChangelist = true;
        }

        public override bool Execute() 
        {
            if (PerforceInfo.m_Changelist != null && RevertChangelist == true)
            {
                if (PerforceInfo.m_Changelist.DeleteShelvedFiles() == false)
                {
                    LogError("Unable to delete shelved files from " + PerforceInfo.m_Changelist.m_Number + ".");
                    //Fall through.
                }

                if (PerforceInfo.m_Changelist.Revert() == false)
                {
                    LogError("Unable to revert changelist " + PerforceInfo.m_Changelist.m_Number + ".");
                    Result.Success = false;
                    return Result.Success;
                }

                if (PerforceInfo.m_Changelist.Delete() == false)
                {
                    LogError("Unable to delete changelist " + PerforceInfo.m_Changelist.m_Number + ".");
                    Result.Success = false;
                    return Result.Success;
                }

                PerforceInfo.m_Changelist = null;
            }

            if (PerforceInfo.m_ShelvedChangelist != null)
            {
                if (PerforceInfo.m_ShelvedChangelist.Revert() == false)
                {
                    LogError("Unable to revert shelved changelist " + PerforceInfo.m_ShelvedChangelist.m_Number + ".");
                    Result.Success = false;
                    return Result.Success;
                }

                if (PerforceInfo.m_ShelvedChangelist.Delete() == false)
                {
                    LogError("Unable to delete shelved changelist " + PerforceInfo.m_ShelvedChangelist.m_Number + ".");
                    Result.Success = false;
                    return Result.Success;
                }
            }

            Result.Success = true;
            return Result.Success; 
        }

        public override string ToShortString()
        {
            return "Disconnecting";
        }

        public override string ToString()
        {
            return "Disconnection notification from build " + BuildName + " has been received.";
        }
    }
}
