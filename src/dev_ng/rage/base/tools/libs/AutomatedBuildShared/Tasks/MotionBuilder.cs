﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using Microsoft.Win32;

using RSG.Base.ConfigParser;
using P4API;

namespace AutomatedBuildShared
{
    public enum MotionBuilderVersion
    {
        MotionBuilderNone,
        MotionBuilder2010,
        MotionBuilder2012
    }

    public class MotionBuilderCutscene : BuildTask
    {
        [XmlAttribute("Version")]
        public MotionBuilderVersion Version;

        [XmlAttribute("FbxFile")]
        public string FbxFile;

        [XmlAttribute("CutFile")]
        public string CutFile;

        [XmlAttribute("MP3Hack")]
        public bool MP3Hack;

        [XmlAttribute("ExistenceCheckOnly")]
        public bool ExistenceCheckOnly;

        private static string MotionBuilderProcessName = "motionbuilder";

        public MotionBuilderCutscene()
        {
            Version = MotionBuilderVersion.MotionBuilderNone;
            FbxFile = null;
            CutFile = null;
            MP3Hack = false;
            ExistenceCheckOnly = false;
        }

        private static readonly Dictionary<MotionBuilderVersion, String> VERSION_NAMES = new Dictionary<MotionBuilderVersion, String>() 
		{
            {MotionBuilderVersion.MotionBuilderNone, "None"},
			{MotionBuilderVersion.MotionBuilder2010, "motionbuilder2010"},
			{MotionBuilderVersion.MotionBuilder2012, "motionbuilder2012"}
		};

        private string CreateTempPythonFileName()
        {
            string filename = System.IO.Path.GetTempFileName();

            File.Delete(filename);

            return (System.IO.Path.GetTempPath() + "\\" + System.IO.Path.GetFileNameWithoutExtension(filename) + ".py");
        }


        /// <summary>
        /// A failsafe function for writing the plug-in path for the current project or
        /// recovering the plugin path that may have been lost from a crash or early exit.  
        /// Prevents exporting from the wrong plug-ins and MotionBuilder hanging because 
        /// Python crashed trying to call into plug-ins that aren't loaded.
        /// </summary>
        /// <param name="configFile"></param>
        /// <param name="pluginsPath"></param>
        /// <returns></returns>
        public bool WriteSDKPluginsPath(string configFile, string pluginsPath)
        {
            //Find the MotionBuilder application configuration file, re-write the plug-ins path for the current project.
            string AdditionalPluginsIdentifier = "[AdditionnalPluginPath]";  //NOTE:  The typo in Additionnal is intentional -- this is what MotionBuilder uses...
            string CountIdentifier = "Count = 1";
            string PlugInPathIdentifier = "PlugInPath0 = ";
            string FileVersionIdentifier = "Config File Version";

            try
            {
                string[] configFileContents = File.ReadAllLines(configFile);
                for(int lineIndex = 0; lineIndex < configFileContents.Length; lineIndex++)
                {
                    if (configFileContents[lineIndex].StartsWith(AdditionalPluginsIdentifier) == true )
                    {
                        configFileContents[lineIndex] = null;
                        continue;
                    }

                    if (configFileContents[lineIndex].StartsWith(CountIdentifier) == true)
                    {
                        configFileContents[lineIndex] = null;
                        continue;
                    }

                    if ( configFileContents[lineIndex].StartsWith(PlugInPathIdentifier) == true )
                    {
                        configFileContents[lineIndex] = null;
                        continue;
                    }
                }

                StreamWriter writer = new StreamWriter(configFile);

                for(int lineIndex = 0; lineIndex < configFileContents.Length; lineIndex++)
                {
                    writer.WriteLine(configFileContents[lineIndex]);

                    if (configFileContents[lineIndex] != null && configFileContents[lineIndex].StartsWith(FileVersionIdentifier) == true)
                    {
                        writer.WriteLine("");
                        writer.WriteLine(AdditionalPluginsIdentifier);
                        writer.WriteLine(CountIdentifier);
                        writer.WriteLine(PlugInPathIdentifier + pluginsPath);
                    }
                }

                writer.Close();

            }
            catch (Exception e)
            {
                Result.LogError("Unable to write plug-ins path to configuration file. " + e.Message);
                return false;
            }

            return true;
        }

        public bool CreateAndExecutePythonScripts()
        {
            string strMotionbuilderPath = "";

            ConfigCoreView cp = new ConfigCoreView();
            int amount = cp.GetDCCPackageCount("motionbuilder");
            /*if (amount == 0)
            {
                string installBatPath = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "install.bat");
                LogError("Unable to find an installed version of MotionBuilder. Unable to continue...");
                return false;
            }
            */

            //TODO:  Find out what happened to the motionbuilder configuration in the installer.

            string strInstallDir = @"C:\Program Files\Autodesk\MotionBuilder 2010\bin\x64\";
            if (Version == MotionBuilderVersion.MotionBuilder2012)
            {
                strInstallDir = @"C:\Program Files\Autodesk\MotionBuilder 2012 (64-bit)\bin\x64\";
            }

            for (int i = 0; i < amount; ++i)
            {
                string strVersion = cp.GetDCCPackageVersion("motionbuilder", i);

                if (String.Compare(strVersion, VERSION_NAMES[Version], true) == 0)
                {
                    strInstallDir = cp.GetDCCPackageInstallDir("motionbuilder", i);
                    break;
                }
            }

            if (String.IsNullOrWhiteSpace(strInstallDir) == true)
            {
                LogError("Unable to find MotionBuilder version " + Version.ToString() + ".");
                return false;
            }

            if (Directory.Exists(strInstallDir) == true)
            {
                string[] filePaths = Directory.GetFiles(strInstallDir, "*motionbuilder.exe", SearchOption.AllDirectories);
                if (filePaths.Length == 0)
                {
                    LogError("The MotionBuilder executable: " + strMotionbuilderPath + " does not exist.  Unable to continue.  Please re-execute the batch and select an installed version of MotionBuilder.");
                    return false;
                }

                strMotionbuilderPath = filePaths[0];

                if (!String.IsNullOrWhiteSpace(strMotionbuilderPath))
                {
                    string strLogPath = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\logs\\cutscene\\motionbuilder_task.log";
                    if (MP3Hack == true)
                    {
                        strLogPath = @"X:\payne\tools\logs\cutscene\motionbuilder_task.log";
                    }

                    try
                    {
                        //HACK:  MotionBuilder, 64-bit support only.  Need to convert this properly when
                        //we're no longer focused on Max Payne.
                        string configFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"Autodesk\MB2010\config", Environment.MachineName + ".Application.txt");
                        if (Version == MotionBuilderVersion.MotionBuilder2012)
                        {
                            configFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), @"MB\2012-x64\config", Environment.MachineName + ".Application.txt");
                        }

                        string pluginsPath = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
                        if (MP3Hack == true)
                            pluginsPath = "X:\\payne\\tools";

                        if (Version == MotionBuilderVersion.MotionBuilder2010)
                        {
                            pluginsPath = Path.Combine(pluginsPath, @"dcc\current\motionbuilder2010\plugins\x64");
                        }
                        else
                        {
                            pluginsPath = Path.Combine(pluginsPath, @"dcc\current\motionbuilder2012\plugins\x64");
                        }

                        WriteSDKPluginsPath(configFile, pluginsPath);

                        string strDirectoryPath = Path.GetDirectoryName(strLogPath);
                        if (Directory.Exists(strDirectoryPath) == false)
                        {
                            Directory.CreateDirectory(strDirectoryPath);
                        }

                        string[] logFiles = Directory.GetFiles(strDirectoryPath, "*.log");
                        foreach (string logFile in logFiles)
                        {
                            try
                            {
                                File.Delete(logFile);
                            }
                            catch (Exception) { }
                        }

                        string filename = CreateTempPythonFileName();

                        if (WritePythonFile(filename, FbxFile, CutFile) == true)
                        {
                            System.Diagnostics.Process pExe = new System.Diagnostics.Process();
                            pExe.StartInfo.FileName = strMotionbuilderPath;
                            pExe.StartInfo.Arguments = "-suspendMessages " + filename;

                            if (MP3Hack == true)
                                pExe.StartInfo.WorkingDirectory = "X:\\payne\\tools";
                            else
                                pExe.StartInfo.WorkingDirectory = Environment.GetEnvironmentVariable("RS_TOOLSROOT");

                            pExe.StartInfo.CreateNoWindow = true;
                            pExe.StartInfo.UseShellExecute = false;

                            //HACK:  This is a hack to have RDR3's Automated Build run on the MP3 branch.
                            //This is done because the MP3 installer does not have command line access to run
                            //and more importantly, adding to it is undesirable.
                            if (MP3Hack == true)
                            {
                                pExe.StartInfo.EnvironmentVariables["RS_TOOLSROOT"] = "X:\\payne\\tools";
                                pExe.StartInfo.EnvironmentVariables["RS_PROJROOT"] = "X:\\payne";
                                pExe.StartInfo.EnvironmentVariables["RS_PROJECT"] = "payne";
                                pExe.StartInfo.EnvironmentVariables["RS_BUILDBRANCH"] = "X:\\payne\\build\\dev";
                                pExe.StartInfo.EnvironmentVariables["RS_CODEBRANCH"] = "X:\\payne\\src\\dev";
                                pExe.StartInfo.EnvironmentVariables["RUBYLIB"] = "X:\\payne\\tools\\lib";

                                string[] pathVars = pExe.StartInfo.EnvironmentVariables["PATH"].Split(';');
                                pathVars[pathVars.Length - 2] = "X:\\payne\\tools\\bin";
                                pathVars[pathVars.Length - 1] = "X:\\payne\\tools\\bin\\ruby\\bin";

                                string fullPathString = "";
                                for(int pathIndex = 0; pathIndex < pathVars.Length; ++pathIndex)
                                {
                                    fullPathString += pathVars[pathIndex];

                                    if (pathIndex < pathVars.Length - 1)
                                    {
                                        fullPathString += ";";
                                    }
                                }
                                pExe.StartInfo.EnvironmentVariables["PATH"] = fullPathString;
                            }

                            Process[] processes = Process.GetProcessesByName(MotionBuilderProcessName);
                            int motionbuilderProcessCount = processes.Length;  //Handle the case where a previously opened MotionBuilder is idling on this machine harmlessly.

                            Thread statusThread = new Thread(ExecuteThread);
                            statusThread.Name = "3D Studio Max Execute Thread";
                            statusThread.Start(pExe);

                            while (true)
                            {
                                processes = Process.GetProcessesByName(MotionBuilderProcessName);
                                if (processes.Length > motionbuilderProcessCount)
                                {
                                    break;
                                }
                            }

                            bool exit = false;
                            while (exit == false)
                            {
                                processes = Process.GetProcessesByName(MotionBuilderProcessName);
                                if (processes.Length == 0)
                                {
                                    Result.Success = false;
                                    Result.LogError("MotionBuilder closes before signaling success.");
                                    break;
                                }

                                if (File.Exists(strLogPath) == true)
                                {
                                    try
                                    {
                                        string[] lines = File.ReadAllLines(strLogPath);
                                        if (lines.Length > 0 && lines[0].Contains("success"))
                                        {
                                            Result.Success = true;
                                        }
                                        else
                                        {
                                            LogError("The scene failed to export: " + lines[0] + ".");
                                            Result.Success = false;
                                        }

                                        exit = true;
                                    }
                                    catch (Exception) { }
                                }

                                try
                                {
                                    if (exit == false && pExe.HasExited == true)
                                    {
                                        //The process has been killed.
                                        LogError("The scene exporting has timed out.");
                                        Result.Success = false;
                                        exit = true;
                                    }
                                }
                                catch (Exception)
                                {
                                    //This process hasn't started yet. 
                                }

                                Thread.Sleep(1000);
                            }
                                                        
                            File.Delete(filename);

                            //Process the outputted logs.
                            string cutsceneName = Path.GetFileName(Path.GetDirectoryName(CutFile));
                            logFiles = null;
                            if (MP3Hack == true)
                            {
                                logFiles = Directory.GetFiles("X:\\payne\\tools\\logs\\cutscene\\", cutsceneName + "*.log", SearchOption.AllDirectories);
                            }
                            else
                            {
                                logFiles = Directory.GetFiles(Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "logs\\cutscene\\") + cutsceneName + "*.log");
                            }

                            //Initial information about what inputs were used.
                            P4RecordSet fstatRecord = PerforceInfo.m_P4.Run("fstat", this.FbxFile);
                            if (fstatRecord.HasErrors() == false)
                                Result.LogMessage(this.FbxFile + ": Revision " + fstatRecord[0]["haveRev"] + ".");
                            else
                                Result.LogMessage(this.FbxFile + ": Revision Unknown.");

                            fstatRecord = PerforceInfo.m_P4.Run("fstat", this.CutFile);
                            if (fstatRecord.HasErrors() == false)
                                Result.LogMessage(this.CutFile + ": Revision " + fstatRecord[0]["haveRev"] + ".");
                            else
                                Result.LogMessage(this.CutFile + ": Revision Unknown.");

                            foreach (string logFile in logFiles)
                            {
                                Result.LogMessage("Log Contents from " + logFile + ".");
                                Result.LogMessage("------------------------------------------------------------");
                                string[] logContents = File.ReadAllLines(logFile);

                                //TODO:  Once the cutscene system can legitimately
                                foreach (string logContent in logContents)
                                {
                                    Result.LogMessage(logContent);
                                }

                                Result.LogMessage("\n");
                            }
                        }
                        else
                        {
                            LogError("Unable to open stream to write to " + filename + ".");
                        }
                    }
                    catch (Exception e)
                    {
                        LogError(e.Message);
                        LogError("Unable to create log file for Python scripts at " + strLogPath + ".");
                    }
                }
            }
            else
            {
                LogError("Unable to find MotionBuilder installation directory: " + strMotionbuilderPath + ".");
                return false;
            }

            return true;
        }

        private void ExecuteThread(object input)
        {
            Process command = (Process)input;
            RegisterProcess(command);
            command.Start();
            int timeout = 1000 * 60 * 120; //120 minutes
            command.WaitForExit(timeout);

            if (command.HasExited == false)
            {
                command.Kill();
            }
        }

        public bool WritePythonFile(string filename, string fbxfile, string cutfile)
        {
            try
            {
                TextWriter tw = new StreamWriter(filename);

                tw.WriteLine("from ctypes import *");
                tw.WriteLine("from pyfbsdk import *");
                tw.WriteLine("from pyfbsdk_additions import *");
                tw.WriteLine("import os");
                tw.WriteLine("");
                tw.WriteLine("try:");
                tw.WriteLine("\tlogPath = os.getenv(\"RS_TOOLSROOT\") + \"\\logs\\cutscene\\motionbuilder_task.log\"");
                tw.WriteLine("\tLOGFILE = open(logPath,\"w\")");
                tw.WriteLine("");
                tw.WriteLine("\tcdll.rexMBRage.DisableBuildRpf_Py(True)");
                tw.WriteLine("");
                tw.WriteLine("\tFBApplication().FileOpen( \"" + fbxfile.Replace("\\", "/") + "\" )");
                tw.WriteLine("\tif cdll.rexMBRage.ExportBuildCutscene_Py( \"" + cutfile.Replace("\\", "/") + "\" ) == False:");
                tw.WriteLine("\t\tLOGFILE.write(\"" + fbxfile.Replace("\\", "/") + " - " + cutfile.Replace("\\", "/") + " failed\\n\")");
                tw.WriteLine("\telse:");
                tw.WriteLine("\t\tLOGFILE.write(\"" + fbxfile.Replace("\\", "/") + " - " + cutfile.Replace("\\", "/") + " success\\n\")");
                tw.WriteLine("");
                tw.WriteLine("\tLOGFILE.close()");
                tw.WriteLine("except IOError:");
                tw.WriteLine("  print \"An error has occurred within the Python script.\"");
                tw.WriteLine("FBApplication().FileExit()");

                tw.Close();
                return true;
            }
            catch (Exception e)
            {
                LogError(e.Message);
                return false;
            }
        }

        public override bool Execute()
        {
            if (String.IsNullOrWhiteSpace(FbxFile) == true)
            {
                LogError("No FBX file was specified.  Unable to continue...");
                Result.Success = false;
                return false;
            }

            if (String.IsNullOrWhiteSpace(FbxFile) == true)
            {
                LogError("No FBX file was specified.  Unable to continue...");
                Result.Success = false;
                return false;
            }

            if (File.Exists(FbxFile) == false)
            {
                LogError("The FBX file " + FbxFile + " was not found.");
                Result.Success = false;
                return false;
            }

            if (File.Exists(CutFile) == false)
            {
                LogError("The Cut file " + CutFile + " was not found.");
                Result.Success = false;
                return false;
            }

            if (ExistenceCheckOnly == false && CreateAndExecutePythonScripts() == false)
            {
                Result.Success = false;
                return false; 
            }

            return true;
        }

        public override string ToReportName()
        {
            return "MotionBuilderCutscene_" + Path.GetFileName(Path.GetDirectoryName(this.CutFile));
        }

        public override string ToShortString()
        {
            return Path.GetFileName(Path.GetDirectoryName(this.CutFile));
        }

        public override string ToString()
        {
            return "Exporting from MotionBuilder with " + FbxFile + " on file " + CutFile;
        }
    }
}
