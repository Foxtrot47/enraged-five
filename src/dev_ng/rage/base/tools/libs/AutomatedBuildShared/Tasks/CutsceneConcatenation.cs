﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace AutomatedBuildShared
{
    public class CutsceneConcatenation : BuildTask
    {
        [XmlAttribute("CutList")]
        public string CutList;

        [XmlAttribute("MP3Hack")]
        public bool MP3Hack;

        public CutsceneConcatenation()
        {
            CutList = null;
        }

        private bool ParseConcatList(string filename, out string[] folderList, out string strOutputFolder, out string strAudio)
        {
            folderList = null;
            strOutputFolder = "";
            strAudio = "";

            using (StreamReader sr = new StreamReader(filename))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.Trim() == "#Scenes")
                    {
                        List<string> folders = new List<string>();
                        while ((line = sr.ReadLine()) != null && line != "" && line.Substring(0, 1) != "#")
                        {
                            folders.Add(line.Trim().Split(',').ElementAt(0));
                        }
                        folderList = new string[folders.Count()];
                        for (int i = 0; i < folders.Count(); i++)
                        {
                            folderList[i] = folders.ElementAt(i);
                        }
                    }
                    if (line.Trim() == "#Output")
                    {
                        strOutputFolder = sr.ReadLine();
                    }
                    if (line.Trim() == "#Audio")
                    {
                        strAudio = sr.ReadLine();
                    }
                }
            }

            return true;
        }

        public bool ConcatenateScene()
        {
            if (File.Exists(CutList) == false)
            {
                Result.Success = false;
                Result.LogError("Cut list " + CutList + " could not be found.");
                return Result.Success;
            }

            string[] folderList;
            string concatOutputPath;
            string audioPath;
            if (ParseConcatList(CutList, out folderList, out concatOutputPath, out audioPath) == false)
            {
                Result.Success = false;
                Result.LogError("Concatenation list " + CutList + " could not be parsed.");
                return Result.Success;
            }

            string animConcatDriverExe = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "bin\\anim\\animconcatdriver.exe");
            string strSceneName = concatOutputPath.TrimEnd('\\', '/');
            strSceneName = Path.GetFileName(strSceneName);
            string animConcatLog = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "logs\\cutscene\\" + strSceneName + "_animconcatdriver.log");
            if (MP3Hack == true)
            {
                animConcatDriverExe = @"X:\payne\tools\bin\anim\animconcatdriver.exe";
                animConcatLog = @"X:\payne\tools\logs\cutscene\" + strSceneName + "_animconcatdriver.log";
            }

            try
            {
                if (File.Exists(animConcatLog) == true)
                    File.Delete(animConcatLog);
            }
            catch (Exception e)
            {
                Result.Success = false;
                Result.LogError("Unable to delete log file. " + e.Message);
                return Result.Success;
            }

            Process pExe = new System.Diagnostics.Process();
            pExe.StartInfo.FileName = animConcatDriverExe;
            pExe.StartInfo.Arguments = CutList;
            pExe.StartInfo.CreateNoWindow = true;
            pExe.StartInfo.UseShellExecute = false;

            //HACK:  This is a hack to have RDR3's Automated Build run on the MP3 branch.
            //This is done because the MP3 installer does not have command line access to run
            //and more importantly, adding to it is undesirable.
            if (MP3Hack == true)
            {
                pExe.StartInfo.WorkingDirectory = "X:\\payne\\tools";

                pExe.StartInfo.EnvironmentVariables["RS_TOOLSROOT"] = "X:\\payne\\tools";
                pExe.StartInfo.EnvironmentVariables["RS_PROJROOT"] = "X:\\payne";
                pExe.StartInfo.EnvironmentVariables["RS_PROJECT"] = "payne";
                pExe.StartInfo.EnvironmentVariables["RS_BUILDBRANCH"] = "X:\\payne\\build\\dev";
                pExe.StartInfo.EnvironmentVariables["RS_CODEBRANCH"] = "X:\\payne\\src\\dev";
                pExe.StartInfo.EnvironmentVariables["RUBYLIB"] = "X:\\payne\\tools\\lib";

                string[] pathVars = pExe.StartInfo.EnvironmentVariables["PATH"].Split(';');
                pathVars[pathVars.Length - 2] = "X:\\payne\\tools\\bin";
                pathVars[pathVars.Length - 1] = "X:\\payne\\tools\\bin\\ruby\\bin";

                string fullPathString = "";
                for (int pathIndex = 0; pathIndex < pathVars.Length; ++pathIndex)
                {
                    fullPathString += pathVars[pathIndex];

                    if (pathIndex < pathVars.Length - 1)
                    {
                        fullPathString += ";";
                    }
                }
                pExe.StartInfo.EnvironmentVariables["PATH"] = fullPathString;
            }
            else
                pExe.StartInfo.WorkingDirectory = Environment.GetEnvironmentVariable("RS_TOOLSROOT");

            RegisterProcess(pExe);
            pExe.Start();

            int timeout = 3 * 60 * 60 * 1000;  //3 hour timeout.
            pExe.WaitForExit(timeout);

            if (pExe.HasExited == false)
            {
                pExe.Kill();
                LogError("The concatenation process has timed out.");
            }

            if (File.Exists(animConcatLog) == true)
            {
                try
                {
                    string[] lines = File.ReadAllLines(animConcatLog);
                    foreach (string line in lines)
                    {
                        Result.LogMessage(line);
                    }
                }
                catch (Exception e)
                {
                    Log("Unable to read log " + animConcatLog + ". " + e.Message);
                }
            }
            else
            {
                Result.Success = false;
                LogError("The log " + animConcatLog + " could not be found.");
                return Result.Success;
            }

            if (pExe.ExitCode != 0)
            {
                Result.Success = false;
            }
            else
            {
                CutsceneBuild cutsceneBuildTask = new CutsceneBuild(true);  //HARDCODED
                cutsceneBuildTask.MP3Hack = MP3Hack;
                cutsceneBuildTask.ScenePath = concatOutputPath;
                cutsceneBuildTask.IsConcatenated = true;
                if (cutsceneBuildTask.Execute() == false)
                {
                    this.Result.Combine(cutsceneBuildTask.Result);  //Combines all result information in this task's TaskResult object.
                    LogError("Unable to build cutscene for " + CutList + ".");
                    return Result.Success;
                }

                this.Result.Combine(cutsceneBuildTask.Result);
            }

            return Result.Success;
        }

        public override bool Execute()
        {
            if (String.IsNullOrWhiteSpace(CutList) == true)
            {
                LogError("No CutList file was specified.  Unable to continue...");
                Result.Success = false;
                return false;
            }

            if (String.IsNullOrWhiteSpace(CutList) == true)
            {
                LogError("No CutList file was specified.  Unable to continue...");
                Result.Success = false;
                return false;
            }

            if (ConcatenateScene() == false)
            {
                Result.Success = false;
                return false;
            }

            return true;
        }

        public override string ToReportName()
        {
            return "CutsceneConcat_" + Path.GetFileNameWithoutExtension(this.CutList);
        }

        public override string ToShortString()
        {
            return Path.GetFileNameWithoutExtension(CutList);
        }

        public override string ToString()
        {
            return "Concatenating Cutscene with " + CutList;
        }
    }
}
