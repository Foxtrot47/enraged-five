﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Command;

namespace AutomatedBuildShared
{
    public class Command : BuildTask
    {
        [XmlAttribute("Name")]
        public string CommandName;

        [XmlAttribute]
        public string Executable;

        [XmlAttribute]
        public string Arguments;

        [XmlAttribute]
        public float TimeoutSeconds;

        [XmlAttribute]
        public string ExitCodes;

        public Command()
        {
            Executable = null;
            Arguments = null;
            ExitCodes = null;
            CommandName = "Unknown";
        }

        public override bool Execute()
        {
            rageExecuteCommand m_Command = new rageExecuteCommand();
            m_Command.Command = Executable;

            if ( m_Command.Arguments != null )
                m_Command.Arguments = Arguments;
    
            m_Command.TimeOutInSeconds = TimeoutSeconds;
            m_Command.UpdateLogFileInRealTime = false;
            m_Command.EchoToConsole = false;
            m_Command.RemoveLog = true;
            m_Command.UseBusySpinner = false;

            Log(m_Command.GetDosCommand());

            rageStatus status = new rageStatus();
            RegisterProcess(m_Command);
            m_Command.Execute(out status);

            //Process the output for basic error and warning identifiers.
            List<string> log = m_Command.GetLogAsArray();

            foreach(string logLine in log)
            {
                string upperLogLine = logLine.ToUpper();
                if ( upperLogLine.Contains("ERROR:") == true )
                {
                    Result.LogError(logLine);
                }
                else if ( upperLogLine.Contains("WARNING:") == true )
                {
                    Result.LogWarning(logLine);
                }

                Log(logLine);
            }

            if (status.Success() == false)
            {
                Result.Success = false;
                return Result.Success;
            }
            else
            {
                List<int> validExitCodes = GetValidExitCodes();
                if (validExitCodes.Count == 0)
                {
                    Result.Success = true;
                }
                else
                {
                    foreach (int validExitCode in validExitCodes)
                    {
                        if (m_Command.ExitCode == validExitCode)
                        {
                            Result.Success = true;
                        }
                    }
                }

                return Result.Success;
            }
        }

        private List<int> GetValidExitCodes()
        {

            List<int> validExitCodes = new List<int>();
            if (String.IsNullOrWhiteSpace(ExitCodes) == false)
            {
                string[] splitExitCodes = ExitCodes.Split(',');
                foreach (string exitCode in splitExitCodes)
                {
                    int exitCodeInt = -1;
                    if (Int32.TryParse(exitCode, out exitCodeInt) == true)
                    {
                        validExitCodes.Add(exitCodeInt);

                    }
                }
            }

            return validExitCodes;
        }

        public override string ToReportName()
        {
            string safeTestName = CommandName.Replace(" ", "_");
            return "Command_" + CommandName;
        }

        public override string ToDisplayName()
        {
            return "Command " + CommandName;
        }

        public override string ToShortString()
        {
            return ToDisplayName();
        }

        public override string ToString()
        {
            return "Command " + CommandName + ".";
            
        }
    }
}
