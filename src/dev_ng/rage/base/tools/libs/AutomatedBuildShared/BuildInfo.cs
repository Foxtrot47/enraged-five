﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace AutomatedBuildShared
{
    public enum ClientState
    {
        [XmlEnum(Name = "Unknown")]
        Unknown,
        [XmlEnum(Name = "Active")]
        Active,
        [XmlEnum(Name = "Queued")]
        Queued,
        [XmlEnum(Name = "Shutdown")]
        Shutdown
    }

    [Serializable]
    public class ClientInfo
    {
        public string ClientName;
        public ClientState State;
        public string TaskDescription;
        private DateTime TaskStartTime;

        [XmlIgnore]
        public TimeSpan TimeElapsed;

        [XmlElement("TimeElapsed")]
        public long TimeElapsedTicks
        {
            get { return TimeElapsed.Ticks; }
            set { TimeElapsed = new TimeSpan(value); }
        }

        public ClientInfo()
        {
            ClientName = null;
            State = ClientState.Unknown;
            TaskDescription = null;
            TaskStartTime = DateTime.UtcNow;
        }

        public ClientInfo(string clientName, ClientState state, string taskDescription, DateTime startTime)
        {
            ClientName = clientName;
            State = state;
            TaskDescription = taskDescription;
            TaskStartTime = startTime;
        }

        public void Update()
        {
            TimeElapsed = DateTime.UtcNow - TaskStartTime;
        }
    }

    public enum BuildStatus
    {
        [XmlEnum(Name = "Unknown")]
        Unknown,
        [XmlEnum(Name = "Initializing")]
        Initializing,
        [XmlEnum(Name = "Loading")]
        Loading,
        [XmlEnum(Name = "Running")]
        Running,
        [XmlEnum(Name = "Complete")]
        Complete,
    }

    /// <summary>
    /// A transitory structure to deliver information from the BuildEngine to an external structure
    /// such as the interface.
    /// </summary>
    [Serializable]
    public class BuildInfo
    {
        public string Name;
        public BuildStatus Status;
        public int UsedClients;
        public int ActiveClients;
        public int QueuedClients;
        public int TasksCompleted;
        public int TasksRemaining;
        public int TotalTasks;
        public int ErrorCount;
        public int WarningCount;
        public bool RetrieveChangelistSetting;
        public bool SubmitChangelistSetting;

        [XmlArray("ClientInfos")]
        [XmlArrayItem("ClientInfo")]
        public List<ClientInfo> ClientInfoList;

        public BuildInfo()
        {
            ClientInfoList = new List<ClientInfo>();
            Status = BuildStatus.Unknown;
        }
    }
}
