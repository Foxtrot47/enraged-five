﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

namespace AutomatedBuildShared
{
    public class SummaryTaskInfo
    {
        [XmlAttribute]
        public string Name;

        [XmlAttribute]
        public string HtmlUrl;

        [XmlAttribute]
        public bool Success;
    }

    public class SummaryReport
    {
        private List<ActiveTask> Tasks;
        private string OutputDirectory;
        private string XslDirectory;

        private static string SummaryReportXmlFile = "summary.xml";
        private static string SummaryReportXslFile = "summary.xsl";
        public static string SummaryReportHtmlFile = "summary.html";

        [XmlIgnore]
        public string OutputFile;

        [XmlAttribute("BuildName")]
        public string BuildName;

        [XmlAttribute("Version")]
        public int Version;

        [XmlAttribute]
        public string TotalDuration;

        [XmlAttribute]
        public int TasksFailed;

        [XmlAttribute]
        public int TotalTasks;

        public List<SummaryTaskInfo> TaskInfo;

        public SummaryReport()
        {
            BuildName = null;
            Version = -1;
            Tasks = null;
            XslDirectory = null;
            OutputDirectory = null;
            OutputFile = null;
            TaskInfo = null;
        }

        public SummaryReport(string buildFileName, int version, TimeSpan duration, List<ActiveTask> completedTasks, string xslDir, string outputDir)
        {
            BuildName = buildFileName;
            TotalDuration = String.Format("{0:hh\\:mm\\:ss}", duration);
            Tasks = completedTasks;
            XslDirectory = xslDir;
            OutputDirectory = outputDir;
            OutputFile = Path.Combine(OutputDirectory, SummaryReportHtmlFile);

            TaskInfo = new List<SummaryTaskInfo>();
        }

        public bool Generate()
        {
            TotalTasks = Tasks.Count;

            //Order the tasks based on their type.  Any "process tasks" are pushed down to the bottom.
            List<ActiveTask> ProcessTasks = new List<ActiveTask>();
            for (int taskIndex = 0; taskIndex < Tasks.Count; ++taskIndex)
            {
                if (BuildTask.ProcessTasks.Contains(Tasks[taskIndex].Task.GetType()) == true)
                {                   
                    ProcessTasks.Add(Tasks[taskIndex]);
                    Tasks.RemoveAt(taskIndex);
                    taskIndex--;
                }
            }

            Tasks.AddRange(ProcessTasks);

            foreach (ActiveTask activeTask in Tasks)
            {
                SummaryTaskInfo taskInfo = new SummaryTaskInfo();
                taskInfo.Name = activeTask.Task.ToDisplayName();
                taskInfo.HtmlUrl = activeTask.Task.ToReportName() + ".html";
                taskInfo.Success = activeTask.Task.Result.Success;

                if (taskInfo.Success == false)
                {
                    TasksFailed++;
                }

                TaskInfo.Add(taskInfo);
            }

            string summaryXslFile = Path.Combine(XslDirectory, SummaryReportXslFile);
            string summaryOutputFile = Path.Combine(OutputDirectory, SummaryReportXmlFile);
            string summaryOutputHtmlFile = OutputFile;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SummaryReport));
                StreamWriter writer = new StreamWriter(summaryOutputFile);
                serializer.Serialize(writer, this);
                writer.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to serialize summary report " + OutputFile + ".");
                Console.WriteLine(e.Message);
                return false;
            }

            try
            {
                XslCompiledTransform transform = new XslCompiledTransform();
                transform.Load(summaryXslFile);
                transform.Transform(summaryOutputFile, summaryOutputHtmlFile);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }

        public static bool Load(string file, out SummaryReport report)
        {
            report = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SummaryReport));
                StreamReader reader = new StreamReader(file);
                report = (SummaryReport) serializer.Deserialize(reader);
                reader.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to serialize summary report " + file + ".");
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }
    }
}
