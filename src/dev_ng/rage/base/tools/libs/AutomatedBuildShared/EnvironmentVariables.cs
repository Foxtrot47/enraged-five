﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace AutomatedBuildShared
{
    [Serializable]
    public class EnvVar
    {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string Value { get; set; }

        public EnvVar() { Name = null; Value = null; }
        public EnvVar(string name, string value) { Name = name; Value = value; }
    }

    [Serializable]
    public class EnvVarList : List<EnvVar>
    {
        public EnvVarList() { }

        public static bool Load(string filePath, out EnvVarList list)
        {
            list = null;

            if (File.Exists(filePath) == false)
            {
                return false;
            }

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(EnvVarList));
                StreamReader reader = new StreamReader(filePath);
                list = (EnvVarList) serializer.Deserialize(reader);
                reader.Close();

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public static bool Save(string filePath, EnvVarList list)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(EnvVarList));
                StreamWriter writer = new StreamWriter(filePath);
                serializer.Serialize(writer, list);
                writer.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void WriteSerializedXml()
        {
            EnvVarList list = new EnvVarList();

            EnvVar projectEnvVar = new EnvVar("RS_PROJECT", "X:\\rage");
            list.Add(projectEnvVar);

            EnvVar envVar = new EnvVar("RAGE_DIR", "%RS_PROJECT%\\src\\dev\\rage");
            list.Add(envVar);

            EnvVarList.Save("C:\\envvars.xml", list);
        }
    }
}
