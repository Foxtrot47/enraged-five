﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace AutomatedBuildShared
{
    public enum MessageType
    {
        Debug,
        Normal,
        TaskNotification,
        EngineNotification,
        Warning,
        Error,
        FatalError
    }

    [Serializable]
    public class TaskMessage
    {
        [XmlAttribute("Message")]
        public string Message;

        [XmlAttribute("Type")]
        public MessageType MsgType;

        public TaskMessage()
        {
            Message = null;
            MsgType = MessageType.Normal;
        }
        public TaskMessage(string message, MessageType type)
        {
            Message = message;
            MsgType = type;
        }
    }

    [Serializable]
    public class TaskLog
    {
        [XmlAttribute]
        public string Name;

        public List<TaskMessage> Messages;

        public TaskLog()
        {
            Name = null;
            Messages = new List<TaskMessage>();
        }

        public TaskLog(string name)
        {
            Name = name;
            Messages = new List<TaskMessage>();
        }

        public void Add(TaskMessage message)
        {
            Messages.Add(message);
        }
    }

    [Serializable]
    public class TaskResult
    {
        public bool Success;

        [XmlArrayItem("Log")]
        public List<TaskLog> Logs;

        public int ErrorCount;
        public int WarningCount;

        [XmlIgnore]
        private TaskLog m_CurrentLog;

        public TaskResult()
        {
            Success = false;
            Logs = new List<TaskLog>();
            m_CurrentLog = null;
        }

        public void LogError(string error)
        {
            if (m_CurrentLog == null)
            {
                m_CurrentLog = new TaskLog("Default");
                Logs.Add(m_CurrentLog);
            }

            error = RemoveInvalidCharacters(error);
            m_CurrentLog.Add(new TaskMessage(error, MessageType.Error));
            ErrorCount++;
        }

        public void LogWarning(string warning)
        {
            if (m_CurrentLog == null)
            {
                m_CurrentLog = new TaskLog("Default");
                Logs.Add(m_CurrentLog);
            }

            warning = RemoveInvalidCharacters(warning);
            m_CurrentLog.Add(new TaskMessage(warning, MessageType.Warning));
            WarningCount++;
        }

        public void LogMessage(string message)
        {
            if (m_CurrentLog == null)
            {
                m_CurrentLog = new TaskLog("Default");
                Logs.Add(m_CurrentLog);
            }

            message = RemoveInvalidCharacters(message);
            m_CurrentLog.Add(new TaskMessage(message, MessageType.Normal));
        }

        public void CreateLog(string logName)
        {
            m_CurrentLog = new TaskLog(logName);
            Logs.Add(m_CurrentLog);
        }

        /// <summary> 
        /// Removes control characters and other non-UTF-8 characters 
        /// </summary> 
        /// <param name="inString">The string to process</param> 
        /// <returns>A string with no control characters or entities above 0x00FD</returns> 
        public static string RemoveInvalidCharacters(string inString) 
        {
            if (inString == null) 
                return null;      
            StringBuilder newString = new StringBuilder();     
            char ch;      
            for (int i = 0; i < inString.Length; i++)    
            {          
                ch = inString[i];         
                // remove any characters outside the valid UTF-8 range as well as all control characters         
                // except tabs and new lines         
                if ((ch < 0x00FD && ch > 0x001F) || ch == '\t' || ch == '\n' || ch == '\r')         
                {             
                    newString.Append(ch);         
                }     
            }

            return newString.ToString();  
        }

        /// <summary>
        /// Combines the TaskResult object from another task (likely called as a sub-process) into this one.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public void Combine(TaskResult result)
        {
            if ( result.Success == false || this.Success == false ) 
            {
                this.Success = false;   
            }
            
            foreach(TaskLog combinedLog in result.Logs)
            {
                bool foundLog = false;
                foreach (TaskLog taskLog in Logs)
                {
                    if (String.Compare(taskLog.Name, combinedLog.Name, true) == 0)
                    {
                        foundLog = true;
                        foreach (TaskMessage message in combinedLog.Messages)
                        {
                            if (message.MsgType == MessageType.Error)
                            {
                                LogError(message.Message);
                            }
                            else if (message.MsgType == MessageType.Warning)
                            {
                                LogWarning(message.Message);
                            }
                            else
                            {
                                LogMessage(message.Message);
                            }
                        }
                    }
                }

                if (foundLog == false)
                {
                    Logs.Add(combinedLog);
                }
            }
        }
        
        /// <summary>
        /// Parses a universal log; inputs it into a TaskResult to be distributed through
        /// the build system.
        /// </summary>
        /// <param name="file"></param>
        /// <returns>Success if the universal log file could be parsed.</returns>
        public bool ParseUniversalLog(string file)
        {
            if ( String.IsNullOrWhiteSpace(file) == true ) 
                return false;

            if (File.Exists(file) == false)
                return false;

            UniversalLogFile uLog = new UniversalLogFile(file);
            foreach (UniversalLogMessage message in uLog.GetMessages())
            {
                if (String.Compare(message.MsgType, "Error", true) == 0)
                {
                    LogError(message.Msg);
                }
                else if (String.Compare(message.MsgType, "Warning", true) == 0)
                {
                    LogWarning(message.Msg);
                }
                else if (String.Compare(message.MsgType, "Message", true) == 0)
                {
                    LogMessage(message.Msg);
                }
            }

            if (uLog.HasErrors == true)
            {
                Success = false;
            }

            return true;
        }

        /// <summary>
        /// Routes all output from a file into the log.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool ReadRawLog(string file)
        {
            try
            {
                StreamReader reader = new StreamReader(file);

                string line = reader.ReadLine();
                while (line != null)
                {
                    line = reader.ReadLine();
                    m_CurrentLog.Messages.Add(new TaskMessage(line, MessageType.Normal));
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Generates a file containing all log output.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool WriteRawLog(string file)
        {
            try
            {
                StreamWriter writer = new StreamWriter(file);
                foreach (TaskMessage message in m_CurrentLog.Messages)
                {
                    writer.WriteLine(message.Message);
                }
                writer.Close();
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
