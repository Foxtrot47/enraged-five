﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

namespace AutomatedBuildShared
{
    [Serializable]
    public class StatusTaskInfo 
    {
        [XmlAttribute]
        public string Name;

        [XmlAttribute]
        public bool Dispatched;

        [XmlAttribute]
        public bool Success;
    }

    [Serializable]
    public class StatusReport
    {
        public static string StatusReportXmlFile = "statusreport.xml";
        private static string StatusReportLockFile = "statusreport.lock";
        
        private string OutputDirectory;

        [XmlAttribute]
        public string BuildName;

        [XmlAttribute]
        public int BuildVersion;
            
        public BuildInfo StatusInfo;
        public List<StatusTaskInfo> StatusTaskInfoList;

        public StatusReport()
        {
            BuildName = null;
            BuildVersion = -1;
            StatusInfo = null;
        }

        public StatusReport(string buildName, int buildVersion, BuildInfo buildInfo, string outputDir)
        {
            BuildName = buildName;
            BuildVersion = buildVersion;
            StatusInfo = buildInfo;
            OutputDirectory = outputDir;
        }

        public bool Generate(List<BuildTask> tasks)
        {
            StatusTaskInfoList = new List<StatusTaskInfo>();
            foreach (BuildTask task in tasks)
            {
                StatusTaskInfo info = new StatusTaskInfo();
                info.Name = task.ToDisplayName();
                info.Dispatched = task.Dispatched;
                info.Success = task.Result.Success;
                StatusTaskInfoList.Add(info);
            }

            string statusOutputFile = Path.Combine(OutputDirectory, StatusReportXmlFile);
            string statusOutputLockFile = Path.Combine(OutputDirectory, StatusReportLockFile);
            bool status = true;

            try
            {
                StreamWriter lockFile = new StreamWriter(statusOutputLockFile);
                lockFile.Close();

                XmlSerializer serializer = new XmlSerializer(typeof(StatusReport));
                StreamWriter writer = new StreamWriter(statusOutputFile);
                serializer.Serialize(writer, this);
                writer.Close();

                if (File.Exists(statusOutputLockFile) == true)
                    File.Delete(statusOutputLockFile);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to serialize status report " + statusOutputFile + ".");
                Console.WriteLine(e.Message);
                status = false;
            }

            return status;
        }

        public static bool Load(string reportfile, out StatusReport result)
        {
            try
            {
                StreamReader reader = new StreamReader(reportfile);
                XmlSerializer serializer = new XmlSerializer(typeof(StatusReport));
                result = (StatusReport)serializer.Deserialize(reader);
                reader.Close();
            }
            catch (Exception)
            {
                result = null;
                return false;
            }

            return true;
        }
    }
}
