﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutomatedBuildShared
{
    /// <summary>
    /// A task assigned to a specific client.  This pairing is used in the build engine to better
    /// maintain these sometimes-arbitrary client-task pairings.
    /// Used within the summary reports to better pinpoint any defective clients.
    /// </summary>
    public class ActiveTask
    {
        public BuildClient Client;
        public BuildTask Task;

        public ActiveTask(BuildClient client, BuildTask task)
        {
            Client = client;
            Task = task;
        }
    }
}
