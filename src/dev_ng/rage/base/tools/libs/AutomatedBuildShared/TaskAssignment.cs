﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace AutomatedBuildShared
{
    //Priorities in ascending order.
    public enum TaskPriority
    {
        TaskIgnored,
        TaskNormal,
        TaskHigh
    }

    [Serializable]
    public class TaskAssignments : List<TaskAssignment>
    {
        public TaskAssignments()
        {
        }

        public TaskPriority GetPriority(Type type)
        {
            foreach(TaskAssignment task in this)
            {
                if (String.Compare(task.TaskTypeName, type.Name, true) == 0)
                {
                    if (task.Ignore == true)
                    {
                        return TaskPriority.TaskIgnored;
                    }
                    else if (task.Exclusive == true)
                    {
                        return TaskPriority.TaskHigh;
                    }
                }
            }

            return TaskPriority.TaskNormal;
        }
    }

    [Serializable]
    public class TaskAssignment
    {
        [XmlAttribute("Name")]
        public string TaskTypeName;

        [XmlAttribute("Ignore")]
        public bool Ignore;

        [XmlAttribute("Exclusive")]
        public bool Exclusive;

        public TaskAssignment()
        {
            Ignore = false;
            Exclusive = false;
        }
    }
}
