﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;

namespace AutomatedBuildShared
{
    public static class ProcessUtility
    {
        public static int GetParentProcessId(this Process p) 
        {
            int parentId = 0; 
            try 
            { 
                ManagementObject mo = new ManagementObject("win32_process.handle='" + p.Id + "'"); 
                mo.Get(); 
                parentId = Convert.ToInt32(mo["ParentProcessId"]); 
            } 
            catch (Exception ex) 
            { 
                Console.WriteLine(ex.ToString()); 
                parentId = 0; 
            } 
            
            return parentId; 
        }
    }

    public class Utilities
    {
        /// <summary>     
        /// Get process and children     
        /// We use postorder (bottom up) traversal; good as any when you kill a process tree.
        /// </summary>     
        /// <param name="plist">Array of all processes</param>
        /// <param name="parent">Parent process</param>     
        /// <param name="output">Output list</param>     
        /// <param name="indent">Indent level</param>     
        private static void GetProcessAndChildren(Process[] plist, Process parent, List<Process> output, int indent)
        {
            foreach (Process p in plist)
            {
                if (p.GetParentProcessId() == parent.Id)
                {
                    GetProcessAndChildren(plist, p, output, indent + 1);
                }
            }

            output.Add(parent);
        } 
    }


}
