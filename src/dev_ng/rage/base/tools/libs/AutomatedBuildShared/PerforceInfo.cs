﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

using RSG.SourceControl.Perforce;
using P4API;

namespace AutomatedBuildShared
{
    public enum SubmissionResult
    {
        Success,
        Failure,
        NoFilesToSubmit,
        EmptyChangelist
    };

    public class Changelist
    {
        public List<string> m_Files;
        public string m_Description;
        private P4 m_P4;
        private P4PendingChangelist m_Changelist;
        public P4RecordSet m_LastRecordSet;
        public int m_Number { get { return m_Changelist.Number; } }
        public int m_SubmittedNumber;

        public Changelist(P4 p4)
        {
            m_P4 = p4;
            m_LastRecordSet = null;
            m_Changelist = null;
            m_SubmittedNumber = -1;
        }

        public bool Create(string description)
        {
            m_Description = description;
            try
            {
                lock (m_P4)
                {
                    //HACK:  Added a retry loop because Perforce will sometimes flake out.
                    int retryAttempt = 10;
                    while (m_Changelist == null)
                    {
                        m_Changelist = m_P4.CreatePendingChangelist(m_Description);
                        if (m_Changelist == null)
                        {
                            if (retryAttempt > 0)
                            {
                                retryAttempt--;
                                Thread.Sleep(10000);
                            }
                            else if (retryAttempt == 0)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //Perforce exception has occurred.
                return false;
            }
            

            m_Files = new List<string>();
            return true;
        }

        public bool Edit(string perforceFilePath)
        {
            P4RecordSet editRecord = m_P4.Run("edit", "-c", m_Changelist.Number.ToString(), perforceFilePath);

            m_LastRecordSet = editRecord;

            if ( editRecord.HasErrors() == true) 
                return false;

            foreach (string message in editRecord.Messages)
            {
                if (message.Contains("use 'reopen'") == true)
                {
                    lock (m_P4)
                    {
                        P4RecordSet reopenRecord = m_P4.Run("reopen", "-c", m_Changelist.Number.ToString(), perforceFilePath);
                        m_LastRecordSet = reopenRecord;
                        if (reopenRecord.HasErrors() == true)
                        {
                            return false;
                        }
                    }

                    break;
                }
            }

            return true;
        }

        /// <summary>
        /// Opens the contents of this changelist into the specified changelist parameter.
        /// 
        /// Perforce does not provide an API to reopen files from one changelist into another.
        /// As a result, if we're opening a changelist with hundreds of files, minimize
        /// the number of commands spammed to the server by grouping files together in the same command.
        /// </summary>
        /// <param name="destChangelist"></param>
        /// <returns></returns>
        public bool Reopen(int destChangelist)
        {
            lock (m_P4)
            {
                P4RecordSet openedRecordSet = m_P4.Run("opened", "-c", m_Changelist.Number.ToString());
                m_LastRecordSet = openedRecordSet;

                const int MaxFiles = 100;
                int numSections = (int) Math.Ceiling( ((decimal) openedRecordSet.Records.Length) / ((decimal) MaxFiles));
                for (int sectionIndex = 0; sectionIndex < numSections; sectionIndex ++)
                {
                    List<string> arguments = new List<string>();
                    arguments.Add("-c");
                    arguments.Add(destChangelist.ToString());

                    int initialOffset = (sectionIndex * MaxFiles);
                    int limit = Math.Min(initialOffset + MaxFiles, openedRecordSet.Records.Length);
                    for (int recordIndex = 0; recordIndex < limit; recordIndex++)
                    {
                        arguments.Add(openedRecordSet[initialOffset + recordIndex]["depotFile"]);
                    }

                    P4RecordSet reopenRecord = m_P4.Run("reopen", arguments.ToArray());

                    m_LastRecordSet = reopenRecord;

                    if (reopenRecord.HasErrors() == true)
                        return false;
                }
            }

            return true;
        }

        public bool Revert()
        {
            lock (m_P4)
            {
                P4RecordSet revertRecord = m_P4.Run("revert", "-c", m_Changelist.Number.ToString(), "//...");
                m_LastRecordSet = revertRecord;
                return !revertRecord.HasErrors();
            }
        }

        public bool Delete()
        {
            lock (m_P4)
            {
                P4RecordSet deleteRecord = m_P4.Run("change", "-d", m_Changelist.Number.ToString());
                m_LastRecordSet = deleteRecord;
                return !deleteRecord.HasErrors();
            }
        }

        public bool Shelve()
        {
            if (GetFileCount() > 0)
            {
                lock (m_P4)
                {
                    P4RecordSet shelveRecord = m_P4.Run("shelve", "-c", m_Changelist.Number.ToString(), "-f", "//...");
                    m_LastRecordSet = shelveRecord;
                    return !shelveRecord.HasErrors() && !shelveRecord.HasWarnings();
                }
            }
            else
            {
                return true;
            }
        }

        public bool DeleteShelvedFiles()
        {
            lock (m_P4)
            {
                P4RecordSet deleteShelvedRecord = m_P4.Run("shelve", "-d", "-c", m_Changelist.Number.ToString(), "//...");
                m_LastRecordSet = deleteShelvedRecord;
                return !deleteShelvedRecord.HasErrors();
            }
        }

        public SubmissionResult Submit()
        {
            if (GetFileCount() == 0)
            {
                return SubmissionResult.EmptyChangelist;
            }
            else
            {
                lock (m_P4)
                {
                    P4RecordSet submitRecords = m_P4.Run("submit", "-f", "revertunchanged", "-c", m_Changelist.Number.ToString());

                    m_LastRecordSet = submitRecords;

                    foreach (string error in submitRecords.Errors)
                    {
                        if (error.Contains("No files to submit.") == true)
                        {
                            return SubmissionResult.NoFilesToSubmit;
                        }
                    }

                    if ( submitRecords.HasErrors() == true )
                    {
                        return SubmissionResult.Failure;
                    }
                    else
                    {
                        //Update the changelist with the number given at submission.
                        //This number is cited in the last record in the set.
                        foreach (P4Record submitRecord in submitRecords.Records)
                        {
                            if ( submitRecord.Fields.ContainsKey("submittedChange") == true )
                            {
                                m_SubmittedNumber = Int32.Parse(submitRecord["submittedChange"]);
                            }
                        }
                        
                        return SubmissionResult.Success;
                    }
                }
            }
        }

        public int GetFileCount()
        {
            lock (m_P4)
            {
                P4RecordSet openedRecord = m_P4.Run("opened", "-c", m_Changelist.Number.ToString(), "//...");
                m_LastRecordSet = openedRecord;
                if (openedRecord.HasErrors() == false)
                    return openedRecord.Records.Length;
                else
                    return -1;
            }
        }
    }

    [Serializable]
    public class PerforceInfo
    {
        [XmlAttribute("Client")]
        public string m_Client;

        [XmlAttribute("Workspace")]
        public string m_Workspace;

        [XmlAttribute("Port")]
        public string m_Port;

        [XmlAttribute("Password")]
        public string m_Password;

        [XmlIgnore]
        public Changelist m_Changelist;

        [XmlIgnore]
        public Changelist m_ShelvedChangelist;  //This is the changelist that holds unshelved changes.

        [XmlIgnore]
        public P4 m_P4;

        public PerforceInfo()
        {
            m_Client = null;
            m_Workspace = null;            
            m_Port = null;
            m_Password = null;
            m_Changelist = null;
            m_ShelvedChangelist = null;
            m_P4 = new P4();
        }

        /// <summary>
        /// Activate this information as the current Perforce information to use for any subsequent commands.
        /// </summary>
        public void Activate()
        {
            lock (m_P4)
            {
                m_P4.Disconnect();

                m_P4.Client = m_Workspace;
                m_P4.Port = m_Port;
                m_P4.User = m_Client;
                m_P4.Connect(m_Password);
                m_P4.ExceptionLevel = P4ExceptionLevels.NoExceptionOnErrors;
            }
        }
    }
}