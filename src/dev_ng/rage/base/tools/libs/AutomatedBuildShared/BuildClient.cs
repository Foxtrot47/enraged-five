﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace AutomatedBuildShared
{
    public class BuildClients : List<BuildClient>
    {
        public static BuildClients GetClients(string inputFile)
        {
            if (File.Exists(inputFile) == false)
            {
                return null;
            }

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(BuildClients));
                StreamReader reader = new StreamReader(inputFile);
                BuildClients clients = (BuildClients)serializer.Deserialize(reader);
                return clients;
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to deserialize " + inputFile + ".");
                return null;
            }            
        }

        public static BuildClient GetBuildClient(string machineName, string inputFile)
        {
            BuildClients clients = GetClients(inputFile);
            if ( clients != null )
            {
                foreach (BuildClient client in clients)
                {
                    if (String.Compare(client.m_MachineName, machineName, true) == 0)
                    {
                        return client;
                    }
                }
            }

            Console.WriteLine("Unable to find machine information for " + machineName + "!");
            return null;
        }

        public BuildClient Find(string machineName)
        {
            foreach (BuildClient client in this)
            {
                if (String.Compare(client.m_MachineName, machineName, true) == 0)
                {
                    return client;
                }
            }

            return null;
        }
    }

    public class WaitingClient
    {
        public TcpClient m_Client;
        public TcpClient m_StatusClient;
        public Thread m_Thread;
        public bool Waiting { get; set; }

        public WaitingClient(TcpClient client, TcpClient statusClient,Thread thread)
        {
            m_Client = client;
            m_StatusClient = statusClient;
            m_Thread = thread;
            Waiting = true;
        }

        public void Close()
        {
            m_Client.Close();
            m_StatusClient.Close();
            m_Thread.Join();
        }
    }

    [Serializable]
    public class BuildClient
    {
        [XmlAttribute("Name")]
        public string m_Name;

        [XmlAttribute("MachineName")]
        public string m_MachineName;

        [XmlElement("PerforceInfo")]
        public PerforceInfo m_PerforceInfo;

        [XmlArray("TaskAssignments")]
        public TaskAssignments m_TaskAssignments;

        private static int m_ClientPort = 6075;
        private static int m_StatusClientPort = 6076;
        private TcpClient m_Client;
        private TcpClient m_StatusClient;
        private List<WaitingClient> m_WaitingClients;
        private TcpListener m_Listener;
        private TcpListener m_StatusListener;

        //A boolean to control whether the BuildClient should continue to process its connection.
        //If false, the BuildClient should begin to close its connection and stop processing.
        public bool Active { get; set; }
        public bool Busy { get; set; }
        public bool Closing { get; set; }
        public bool Shutdown { get; set; }

        [XmlIgnore]
        public BuildTask CurrentTask;

        [XmlIgnore]
        public ClientStatus CurrentStatus;

        private const int m_StatusUpdateTimeout = 300; // 5 minutes

        private Thread m_ConnectionThread;
        private Thread m_ProcessingThread;
        private Thread m_StatusThread;

        public delegate void OutputReceivedCallback(string output);
        public event OutputReceivedCallback OutputReceived;

        ManualResetEvent m_ConnectionResetEvent = new ManualResetEvent(false);
        ManualResetEvent m_StatusConnectionResetEvent = new ManualResetEvent(false);
        protected delegate TcpClient AcceptClientDel();

        public BuildClient() 
        {
            m_Name = null;
            m_MachineName = Environment.MachineName;

            m_Listener = null;
            m_StatusListener = null;
            m_WaitingClients = new List<WaitingClient>();
            m_TaskAssignments = new TaskAssignments();

            Active = false;
            Busy = false;
            Closing = false;

            CurrentTask = null;
            CurrentStatus = null;

            m_ConnectionThread = null;
            m_ProcessingThread = null;
            m_StatusThread = null;

            OutputReceived = null;
        } 

        public bool IsValid()
        {
            if (String.IsNullOrEmpty(m_PerforceInfo.m_Client)
                || String.IsNullOrEmpty(m_PerforceInfo.m_Port)
                || String.IsNullOrEmpty(m_PerforceInfo.m_Workspace))
            {
                return false;
            }

            return true;
        }

        public bool Connect()
        {
            try
            {
                m_Client = new TcpClient();
                m_Client.Connect(m_MachineName, m_ClientPort);

                m_StatusClient = new TcpClient();
                m_StatusClient.Connect(m_MachineName, m_StatusClientPort);
            }
            catch (SocketException)
            {
                return false;
            }

            return m_Client.Connected && m_StatusClient.Connected;
        }

        public bool IsConnected()
        {
            return IsConnected(m_Client);
        }

        public bool IsConnected(TcpClient tcpClient)
        {
            if (tcpClient == null)
                return false;

            lock (tcpClient)
            {
                if (tcpClient.Client.Poll(0, SelectMode.SelectRead))
                {
                    byte[] buff = new byte[1];
                    try
                    {
                        if (tcpClient.Client.Receive(buff, SocketFlags.Peek) == 0)
                        {
                            // Client disconnected
                            return false;
                        }
                    }
                    catch (System.Exception)
                    {
                        return false;
                    }
                }

                if (tcpClient.Client.Poll(0, SelectMode.SelectError))
                {
                    return false;
                }
            }

            return tcpClient.Connected;
        }

        public void Disconnect()
        {
            Active = false;
            Busy = false;

            if ( m_ConnectionThread != null )
                m_ConnectionThread.Join();

            if ( m_ProcessingThread != null )
                m_ProcessingThread.Join();

            if (m_StatusThread != null)
                m_StatusThread.Join();

            if (m_Client != null)
            {
                m_ConnectionResetEvent.Set();

                m_Client.Close();
                m_Client = null; 
            }

            if (m_StatusClient != null)
            {
                m_StatusConnectionResetEvent.Set();
                m_StatusClient.Close();
                m_StatusClient = null; 
            }
        }

        public void Close()
        {
            Closing = true;

            Disconnect();

            foreach (WaitingClient client in m_WaitingClients)
            {
                client.Close();
            }

            m_WaitingClients.Clear();
        }

        public bool Listen()
        {
            try
            {
                m_Listener = new TcpListener(IPAddress.Any, m_ClientPort);
                m_StatusListener = new TcpListener(IPAddress.Any, m_StatusClientPort);
                m_Listener.Start();
                m_StatusListener.Start();
            }
            catch (SocketException e)
            {
                Log("Unable to listen for incoming connections due to the exception:");
                Log(e.Message);
                return false;
            }

            while (Closing == false) //Should be some exit-testing bool
            {
                try
                {
                    AcceptClientDel del = new AcceptClientDel(m_Listener.AcceptTcpClient);
                    IAsyncResult result = del.BeginInvoke(null, null);
                    TcpClient newClient = null;

                    WaitHandle[] handles = new WaitHandle[] { m_ConnectionResetEvent, result.AsyncWaitHandle };
                    int handleIndex = WaitHandle.WaitAny(handles);
                    if (handleIndex == 0)
                    {
                        //Shutdown signal has occurred; gracefully exit the loop.
                        break;
                    }

                    newClient = del.EndInvoke(result);


                    del = new AcceptClientDel(m_StatusListener.AcceptTcpClient);
                    result = del.BeginInvoke(null, null);
                    TcpClient newStatusClient = null;

                    handles = new WaitHandle[] { m_StatusConnectionResetEvent, result.AsyncWaitHandle };
                    handleIndex = WaitHandle.WaitAny(handles);
                    if (handleIndex == 0)
                    {
                        //Shutdown signal has occurred; gracefully exit the loop.
                        break;
                    }

                    newStatusClient = del.EndInvoke(result);
                    
                    Log("Acquired a new connection from " + newClient.Client.RemoteEndPoint + ".");

                    if (Active == true)
                    {
                        //The client is already busy with another build.
                        Log("The client is already processing tasks from another build!  Queuing!");

                        //TODO:  Properly notify the build machine.
                        Thread waitingClientThread = new Thread(WaitingClientThread);
                        waitingClientThread.Name = "Build Client Waiting Client Thread from " + m_Listener.LocalEndpoint;
                        WaitingClient newQueuedClient = new WaitingClient(newClient, newStatusClient, waitingClientThread);
                        waitingClientThread.Start(newQueuedClient);
                        m_WaitingClients.Add(newQueuedClient);
                        continue;
                    }
                    else
                    {
                        //The client is available for use.
                        ActivateConnection(newClient, newStatusClient);
                    }
                }
                catch (SocketException e)
                {
                    Log("Socket exception has occurred.  " + e.Message);
                }
            }

            return true;
        }

        public void ActivateConnection(TcpClient newClient, TcpClient newStatusClient)
        {
            if ( m_ConnectionThread != null )
                m_ConnectionThread.Join();

            if ( m_ProcessingThread != null )
                m_ProcessingThread.Join();

            if (m_StatusThread != null)
                m_StatusThread.Join();

            //If we are not active, then accept this connection to the client and proceed.
            m_Client = newClient;
            if (m_Client.Available > 0)
            {
                byte[] buffer = new byte[m_Client.Available];
                m_Client.GetStream().Read(buffer, 0, m_Client.Available);
            }

            m_StatusClient = newStatusClient;

            Log("Activated connection from " + m_Client.Client.RemoteEndPoint + ".");

            Active = true;
            Busy = false;  //Allow the client to be able to accept tasks.

            m_ConnectionThread = new Thread(ConnectionThread);
            m_ConnectionThread.Name = "Build Client Connection Thread";
            m_ConnectionThread.Start();

            m_ProcessingThread = new Thread(ProcessingThread);
            m_ProcessingThread.Name = "Build Client Processing Thread";
            m_ProcessingThread.Start(m_Client);

            m_StatusThread = new Thread(ProcessingThread);
            m_StatusThread.Name = "Build Client Status Thread";
            m_StatusThread.Start(m_StatusClient);

        }

        //PURPOSE:  Intermittently pings the connection; deactivates if the connection is lost.
        public void ConnectionThread()
        {
            while (Active == true)
            {
                if (IsConnected(m_Client) == false)
                {
                    Log("Connection to " + m_Client.Client.RemoteEndPoint + " has been lost!");
                    Active = false;
                }

                Thread.Sleep(1000);
            }
        }

        public void WaitingClientThread(object client)
        {
            WaitingClient waitingClient = (WaitingClient) client;
            BuildTask buildTask = null;

            //Poll for connection.
            bool connected = waitingClient.m_StatusClient.Connected;
            while (connected == true && waitingClient.Waiting == true)
            {
                if (waitingClient.m_StatusClient.Client.Poll(0, SelectMode.SelectRead))
                {
                    byte[] buff = new byte[1];
                    try
                    {
                        if (waitingClient.m_StatusClient.Client.Receive(buff, SocketFlags.Peek) == 0)
                        {
                            // Client disconnected
                            connected = false;
                        }
                    }
                    catch (System.Exception)
                    {
                        connected = false;
                    }
                }

                lock (waitingClient.m_StatusClient)
                {
                    if (waitingClient.m_StatusClient.Connected == false)
                    {
                        Thread.Sleep(1000);
                        continue;
                    }

                    int numBytes = waitingClient.m_StatusClient.Available;
                    if (numBytes == 0)
                    {
                        Thread.Sleep(1);
                        continue;
                    }

                    try
                    {
                        buildTask = BuildTask.ReadTask(waitingClient.m_StatusClient);
                    }
                    catch (IOException)
                    {
                        //The connection has been interrupted.
                        buildTask = null;
                    }
                }

                if (buildTask != null)
                {
                    ProcessQueuedTask(waitingClient, buildTask);
                }
            }

            if (connected == false)
            {
                lock (m_WaitingClients)
                {
                    m_WaitingClients.Remove(waitingClient);
                }

                return;
            }

            //Wait for promotion.
            if (waitingClient.Waiting == false)
            {
                //We have been promoted.  Notify the connection.
                lock (m_WaitingClients)
                {
                    m_WaitingClients.Remove(waitingClient);

                    Log("Activating queued client at " + waitingClient.m_Client.Client.RemoteEndPoint + ".");
                    ActivateConnection(waitingClient.m_Client, waitingClient.m_StatusClient);
                }
            }
        }


        //PURPOSE:  Processes all incoming tasks on the connection.
        public void ProcessingThread(object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            BuildTask buildTask = null;
            Thread taskThread = null;

            while (Active == true)
            {
                lock (tcpClient)
                {
                    if (tcpClient.Connected == false)
                    {
                        Thread.Sleep(1000);
                        continue;
                    }

                    int numBytes = tcpClient.Available;
                    if (numBytes == 0)
                    {
                        Thread.Sleep(1);
                        continue;
                    }

                    try
                    {
                        buildTask = BuildTask.ReadTask(tcpClient);
                    }
                    catch (IOException)
                    {
                        //The connection has been interrupted.
                        buildTask = null;
                    }
                }

                if (buildTask != null)
                {
                    taskThread = new Thread(ProcessTask);
                    taskThread.Name = "Build Client Process Task Thread from " + m_Listener.LocalEndpoint;
                    taskThread.Start(buildTask);
                }
            }

            //Stop the current task.
            if (taskThread != null && taskThread.IsAlive == true)
            {
                BuildTask.Abort();
            }
        }

        //PURPOSE:  Processes a task locally.
        public void ProcessTask(object taskObject)
        {
            BuildTask task = taskObject as BuildTask;
            task.StartTime = DateTime.UtcNow;

            if (task.GetType() == typeof(DisconnectTask))
            {
                Log(task.ToString());

                //Clean up anything on this machine.
                DisconnectTask disconnectTask = (DisconnectTask)task;
                disconnectTask.PerforceInfo = this.m_PerforceInfo;
                disconnectTask.Localize();  //The DisconnectTask is a special task that is not normally initialized.  Be sure to localize this.
                disconnectTask.Execute();

                BuildTask.Abort();

                this.Active = false;

                if (m_WaitingClients.Count > 0)
                {
                    m_WaitingClients[0].Waiting = false;
                }
            }
            else if (task.GetType() == typeof(ClientStatus))
            {
                SendStatus(m_StatusClient, ClientState.Active); //Send the status with the note that this client has been active.
            }
            else if (task.GetType() == typeof(ShelveChangelistTask))
            {
                Log(task.ToString());

                ShelveChangelistTask changelistTask = (ShelveChangelistTask)task;
                changelistTask.PerforceInfo = this.m_PerforceInfo;
                changelistTask.ChangelistNumber = m_PerforceInfo.m_Changelist.m_Number;
                changelistTask.Execute();

                LogTaskNotification(changelistTask);
                DispatchTask(changelistTask);
            }
            else
            {
                Log(task.ToString());

                task.PerforceInfo = this.m_PerforceInfo;
                if (task.PreExecute() == true)
                {
                    task.Execute();
                }

                //Do want to fail the task if we couldn't remove the sync files from the workspace?
                task.PostExecute();

                LogTaskNotification(task);
                DispatchTask(task);
            }
        }

        public void ProcessQueuedTask(WaitingClient client, BuildTask taskObject)
        {
            BuildTask task = taskObject as BuildTask;

            if (task.GetType() == typeof(DisconnectTask))
            {
                //Clean up anything on this machine.
                DisconnectTask disconnectTask = (DisconnectTask)task;
                disconnectTask.PerforceInfo = this.m_PerforceInfo;
                disconnectTask.Localize();  //The DisconnectTask is a special task that is not normally initialized.  Be sure to localize this.
                disconnectTask.Execute();
            }
            else if (task.GetType() == typeof(ClientStatus))
            {
                SendStatus(client.m_StatusClient, ClientState.Queued); //Send the status with the note that this client has been active.
            }
            else
            {
                //An unsupported task while queued has been received! 
                Log("An unsupported task from a queued client has been received!");
            }
        }

        //PURPOSE:  Logs the status of a completed task.
        private void LogTaskNotification(BuildTask task)
        {
            if (task.Result.Success == true)
            {
                Log(task.ToString() + " has succeeded.");
            }
            else
            {
                Log(task.ToString() + " has failed.");
            }
        }

        //PURPOSE:  Dispatches a task to a client.
        public bool DispatchTask(BuildTask task)
        {
            //Serialize the task and then send it over.
            //
            if (Active == false)
                return false;

            if (m_Client.Connected == false)
                return false;

            lock (m_Client)
            {
                CurrentTask = task;
                BuildTask.WriteTask(m_Client, task);
            }

            Busy = true;

            return true;
        }

        public bool WaitForTaskResult(ref BuildTask task)
        {
            //Client spawns a thread to wait for a result back from the current task at hand.

            while (Busy == true)
            {
                if (IsConnected(m_Client) == false)
                    break;

                if (m_Client.Available != 0)
                {
                    lock (m_Client)
                    {
                        BuildTask taskReturn = BuildTask.ReadTask(m_Client);
                        if (taskReturn != null)
                        {
                            task = taskReturn;
                        }
                        else
                        {
                            task.Result = null;
                        }
                    }

                    Busy = false;
                    CurrentTask = null;
                    return true;
                }

                Thread.Sleep(100);
            }

            Busy = true;
            return false;  //Must reschedule the task. 
        }

        /// <summary>
        /// Queries the client's status; a type of heartbeat.  Uses a separate
        /// connection to transport this data asynchronously, away from the 
        /// task assignment connection.
        /// </summary>
        /// <returns></returns>
        public void UpdateStatus()
        {
            //Client spawns a thread to wait for a result back from the current task at hand.
            ClientStatus dummyTask = new ClientStatus();
            BuildTask.WriteTask(m_StatusClient, dummyTask);

            Thread statusThread = new Thread(GetStatus);
            statusThread.Name = m_MachineName + " Status Thread";
            statusThread.Start();

            while (Active == true)
            {
                if (statusThread.IsAlive == false)
                {
                    //Status has been updated.
                    break;
                }

                if (CurrentStatus != null)
                {
                    TimeSpan lastUpdateReceived = DateTime.UtcNow - CurrentStatus.Timestamp;
                    if (lastUpdateReceived.TotalSeconds >= m_StatusUpdateTimeout)
                    {
                        //After the length of this timeout without an update, mark this client
                        //as shutdown.
                        CurrentStatus.State = ClientState.Shutdown;
                        CurrentStatus.Timestamp = DateTime.UtcNow;
                        break;
                    }
                }

                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// This function is spawned on another thread; used to acquire the status from a connection
        /// via a connection call.
        /// </summary>
        private void GetStatus()
        {
            while (Active == true)
            {
                if (IsConnected(m_StatusClient) == false)
                    break;

                if (m_StatusClient.Available != 0)
                {
                    lock (m_StatusClient)
                    {
                        CurrentStatus = (ClientStatus)BuildTask.ReadTask(m_StatusClient);
                    }

                    break;
                }
            }
        }

        /// <summary>
        /// Sends the current state of this client across the connection.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="state"></param>
        public void SendStatus(TcpClient client, ClientState state)
        {
            //Log("Sending Status as " + state.ToString() + "...");
            string ipAddress = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString();
            string ipPort = ((IPEndPoint)client.Client.RemoteEndPoint).Port.ToString();
            ClientStatus clientStatus = new ClientStatus();
            clientStatus.ActiveClientAddress = ipAddress;
            clientStatus.ActiveClientPort = ipPort;
            clientStatus.TargetMachine = Environment.MachineName;
            clientStatus.State = state;
            clientStatus.Timestamp = DateTime.UtcNow;
          
            BuildTask.WriteTask(client, clientStatus);
        }

        public ShelveChangelistTask ShelveChanges(int destinationChangelist)
        {
            ShelveChangelistTask shelveChangelistTask = new ShelveChangelistTask(Environment.MachineName, destinationChangelist);
            BuildTask.WriteTask(m_Client, shelveChangelistTask);

            shelveChangelistTask = (ShelveChangelistTask)BuildTask.ReadTask(m_Client);
            return shelveChangelistTask;
        }

        public void SendDisconnect(string buildName, bool revertChangelist)
        {
            DisconnectTask disconnectTask = new DisconnectTask();
            disconnectTask.BuildName = buildName;
            disconnectTask.RevertChangelist = revertChangelist;
            BuildTask.WriteTask(m_Client, disconnectTask);
        }

        public void Log(string output)
        {
            if (output.EndsWith("\n") == false)
            {
                output += "\n";
            }

            if(OutputReceived != null)
            {
                OutputReceived(output);
            }
            else
            {
                Console.WriteLine(output);
            }
        }
    }
}
