﻿//---------------------------------------------------------------------------------------------
// <copyright file="TestChangelist.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SourceControl.Perforce.UnitTest
{
    using System;
    using System.IO;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Unit tests for the Changelist class.
    /// </summary>
    [TestClass]
    public class TestChangelist
    {
        #region Member Data
        /// <summary>
        /// Perforce API object.
        /// </summary>
        private P4 P4 { get; set; }
        #endregion // Member Data

        #region Unit Testcase Setup / Teardown
        /// <summary>
        /// 
        /// </summary>
        [TestInitialize]
        public void Setup()
        {
            String tools_root = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            Assert.IsTrue(Directory.Exists(tools_root));

            String cwd = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(tools_root);
            this.P4 = new RSG.SourceControl.Perforce.P4();
            this.P4.Connect();
            Directory.SetCurrentDirectory(cwd);
            Assert.IsTrue(this.P4.IsValidConnection(true, true));
        }

        /// <summary>
        /// 
        /// </summary>
        [TestCleanup]
        public void Teardown()
        {
            this.P4.Disconnect();
        }
        #endregion // Unit Testcase Setup / Teardown

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void TestChangelist1()
        {
            Changelist cl = new Changelist(this.P4, 2629184);
            Assert.AreEqual(2629184, cl.Number);
        }
    }

} // RSG.SourceControl.Perforce.UnitTest namespace
