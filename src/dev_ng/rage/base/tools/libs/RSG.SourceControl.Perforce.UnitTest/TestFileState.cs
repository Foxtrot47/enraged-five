﻿//---------------------------------------------------------------------------------------------
// <copyright file="TestFileMapping.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SourceControl.Perforce.UnitTest
{
    using System;
    using System.IO;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Unit tests for the FileState class.
    /// </summary>
    [TestClass]
    public class TestFileState
    {
        #region Member Data
        /// <summary>
        /// Perforce API object.
        /// </summary>
        private P4 P4 { get; set; }
        #endregion // Member Data

        #region Unit Testcase Setup / Teardown
        /// <summary>
        /// 
        /// </summary>
        [TestInitialize]
        public void Setup()
        {
            String tools_root = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            Assert.IsTrue(Directory.Exists(tools_root));

            String cwd = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(tools_root);
            this.P4 = new RSG.SourceControl.Perforce.P4();
            this.P4.Connect();
            Directory.SetCurrentDirectory(cwd);
            Assert.IsTrue(this.P4.IsValidConnection(true, true));
        }

        /// <summary>
        /// 
        /// </summary>
        [TestCleanup]
        public void Teardown()
        {
            this.P4.Disconnect();
        }
        #endregion // Unit Testcase Setup / Teardown

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void TestFileState1()
        {
            String tools_root = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
            String configXml = Path.Combine(tools_root, "config.xml");
            FileState fs = new FileState(this.P4, configXml);

            Assert.IsTrue(fs.IsMapped);
            Assert.IsTrue(File.Exists(fs.ClientFilename));
        }
    }

} // RSG.SourceControl.Perforce.UnitTest namespace
