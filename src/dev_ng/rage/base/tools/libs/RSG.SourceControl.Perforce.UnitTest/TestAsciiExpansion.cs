﻿//---------------------------------------------------------------------------------------------
// <copyright file="TestAsciiExpansion.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SourceControl.Perforce.UnitTest
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.SourceControl.Perforce.Util;

    /// <summary>
    /// Unit tests for the ASCII Expansion utility class.
    /// </summary>
    [TestClass]
    public class TestAsciiExpansion
    {
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test1()
        {
            String pathname1 = @"x:\gta5\test\example.txt";
            String pathname2 = @"x:\gta5\assets\export\anim\clip\veh@\veh@test.icd.zip";
            String pathname3 = @"x:\gta5\assets\export\anim\clip\veh%\veh%test.icd.zip";
            String pathname4 = @"x:\gta5\assets\export\anim\clip\veh#\veh#test.icd.zip";

            String epath1 = AsciiExpansion.Expand(pathname1);
            String epath2 = AsciiExpansion.Expand(pathname2);
            String epath3 = AsciiExpansion.Expand(pathname3);
            String epath4 = AsciiExpansion.Expand(pathname4);

            Assert.AreEqual(pathname1, epath1);
            Assert.AreEqual(@"x:\gta5\assets\export\anim\clip\veh%40\veh%40test.icd.zip", epath2);
            Assert.AreEqual(@"x:\gta5\assets\export\anim\clip\veh%25\veh%25test.icd.zip", epath3);
            Assert.AreEqual(@"x:\gta5\assets\export\anim\clip\veh%23\veh%23test.icd.zip", epath4);

            Assert.AreEqual(pathname1, AsciiExpansion.Collapse(epath1));
            Assert.AreEqual(pathname2, AsciiExpansion.Collapse(epath2));
            Assert.AreEqual(pathname3, AsciiExpansion.Collapse(epath3));
            Assert.AreEqual(pathname4, AsciiExpansion.Collapse(epath4));
        }
    }

} // RSG.SourceControl.Perforce.UnitTest namespace
