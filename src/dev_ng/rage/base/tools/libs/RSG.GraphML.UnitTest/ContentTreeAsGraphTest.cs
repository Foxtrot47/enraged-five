﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using RSG.GraphML;
using RSG.Base.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RSG.GraphML.UnitTest
{

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class ContentTreeAsGraphTest
    {
        /// <summary>
        /// Attempts to load the content-tree just using the GraphML library.
        /// This shows compatibility with GraphML.
        /// </summary>
        [TestMethod]
        public void TestContentTreeLoadAsGraph()
        {
            IConfig config = ConfigFactory.CreateConfig();

            String filename = Path.Combine(config.ToolsConfig, "content", "content.xml");
            Assert.IsTrue(File.Exists(filename));

            GraphCollection graphs = new GraphCollection(filename);
            Assert.IsTrue(graphs.Count > 0);
            Assert.IsTrue(graphs.Attributes.Count > 0); // From XInclude'd data
        }
    }

} // RSG.GraphML.UnitTest
