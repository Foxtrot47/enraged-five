﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RSG.GraphML.UnitTest
{

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class SimpleGraphTests
    {
        /// <summary>
        /// Simple GraphML test data.
        /// </summary>
        private static readonly String GRAPH_SIMPLE =
            @"<?xml version=""1.0"" encoding=""UTF-8""?>
            <graphml xmlns=""http://graphml.graphdrawing.org/xmlns""  
                xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                xsi:schemaLocation=""http://graphml.graphdrawing.org/xmlns
                 http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"">
              <graph id=""GRAPH_SIMPLES"" edgedefault=""undirected"">
                <node id=""n0""/>
                <node id=""n1""/>
                <node id=""n2""/>
                <node id=""n3""/>
                <node id=""n4""/>
                <node id=""n5""/>
                <node id=""n6""/>
                <node id=""n7""/>
                <node id=""n8""/>
                <node id=""n9""/>
                <node id=""n10""/>
                <edge source=""n0"" target=""n2""/>
                <edge source=""n1"" target=""n2""/>
                <edge source=""n2"" target=""n3""/>
                <edge source=""n3"" target=""n5""/>
                <edge source=""n3"" target=""n4""/>
                <edge source=""n4"" target=""n6""/>
                <edge source=""n6"" target=""n5""/>
                <edge source=""n5"" target=""n7""/>
                <edge source=""n6"" target=""n8""/>
                <edge source=""n8"" target=""n7""/>
                <edge source=""n8"" target=""n9""/>
                <edge source=""n8"" target=""n10""/>
              </graph>
            </graphml>";

        /// <summary>
        /// GraphML with Attributes.
        /// </summary>
        private static readonly String GRAPH_WITH_ATTRIBUTES =
            @"<?xml version=""1.0"" encoding=""UTF-8""?>
            <graphml xmlns=""http://graphml.graphdrawing.org/xmlns""  
                  xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                  xsi:schemaLocation=""http://graphml.graphdrawing.org/xmlns 
                    http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"">
              <key id=""stringAttr"" for=""node"" attr.name=""color"" attr.type=""string"">
                <default>yellow</default>
              </key>
              <key id=""doubleAttr"" for=""edge"" attr.name=""weight"" attr.type=""double""/>
              <graph id=""GRAPH_ATTRIBUTES"" edgedefault=""undirected"">
                <node id=""n0"">
                  <data key=""d0"">green</data>
                </node>
                <node id=""n1""/>
                <node id=""n2"">
                  <data key=""d0"">blue</data>
                </node>
                <node id=""n3"">
                  <data key=""d0"">red</data>
                </node>
                <node id=""n4""/>
                <node id=""n5"">
                  <data key=""d0"">turquoise</data>
                </node>
                <edge id=""e0"" source=""n0"" target=""n2"">
                  <data key=""d1"">1.0</data>
                </edge>
                <edge id=""e1"" source=""n0"" target=""n1"">
                  <data key=""d1"">1.0</data>
                </edge>
                <edge id=""e2"" source=""n1"" target=""n3"">
                  <data key=""d1"">2.0</data>
                </edge>
                <edge id=""e3"" source=""n3"" target=""n2""/>
                <edge id=""e4"" source=""n2"" target=""n4""/>
                <edge id=""e5"" source=""n3"" target=""n5""/>
                <edge id=""e6"" source=""n5"" target=""n4"">
                  <data key=""d1"">1.1</data>
                </edge>
              </graph>
            </graphml>";

        /// <summary>
        /// Graph constructor test case for simple node/edge graph.
        /// </summary>
        [TestMethod]
        public void TestLoadSimpleGraph()
        {
            GraphCollection graphs = new GraphCollection();
            using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(GRAPH_SIMPLE)))
            {
                graphs.LoadGraphs(stream);
                Assert.IsTrue(graphs.First().EdgeType == EdgeDefaultType.Undirected);
                Assert.IsTrue(graphs.First().Nodes().Count() > 0);
                Assert.IsTrue(graphs.First().Edges().Count() > 0);
            }
        }

        /// <summary>
        /// Graph constructor test case for graph with GraphML Attribute data.
        /// </summary>
        [TestMethod]
        public void TestLoadGraphWithAttributes()
        {
            GraphCollection graphs = new GraphCollection();
            using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(GRAPH_WITH_ATTRIBUTES)))
            {
                graphs.LoadGraphs(stream);
                Assert.IsTrue(graphs.First().EdgeType == EdgeDefaultType.Undirected);
                Assert.IsTrue(graphs.First().Nodes().Count() > 0);
                Assert.IsTrue(graphs.First().Edges().Count() > 0);
            }
        }

        /// <summary>
        /// Test case for the GraphCollection class.
        /// </summary>
        [TestMethod]
        public void TestGraphCollection()
        {
            GraphCollection graphs = new GraphCollection();
            Assert.IsTrue(graphs.Count == 0);
            Assert.IsTrue(graphs.Attributes.Count == 0);

            using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(GRAPH_SIMPLE)))
            {
                graphs.LoadGraphs(stream);
                Assert.AreEqual(1, graphs.Count());
                Assert.IsTrue(graphs.First().EdgeType == EdgeDefaultType.Undirected);
                Assert.IsTrue(graphs.First().Nodes().Count() > 0);
                Assert.IsTrue(graphs.First().Edges().Count() > 0);
            }
            using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(GRAPH_WITH_ATTRIBUTES)))
            {
                graphs.LoadGraphs(stream);
                Assert.AreEqual(2, graphs.Count);
            }
            Assert.IsTrue(2 == graphs.Count());
            Assert.IsTrue(graphs.ContainsGraph("GRAPH_SIMPLES"));
            Assert.IsInstanceOfType(graphs["GRAPH_SIMPLES"], typeof(Graph));
            Assert.IsTrue(graphs.ContainsGraph("GRAPH_ATTRIBUTES"));
            Assert.IsInstanceOfType(graphs["GRAPH_ATTRIBUTES"], typeof(Graph));
            Assert.IsFalse(graphs.ContainsGraph("GRAPH_DAVE"));
            Assert.AreEqual(null, graphs["GRAPH_DAVE"]);
        }
    }

} // RSG.GraphML.UnitTest namespace
