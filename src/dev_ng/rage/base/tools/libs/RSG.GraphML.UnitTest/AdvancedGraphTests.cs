﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using RSG.GraphML;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RSG.GraphML.UnitTest
{

    /// <summary>
    /// More advanced GraphML feature loading and validation tests.
    /// </summary>
    [TestClass]
    public class AdvancedGraphTests
    {

        /// <summary>
        /// Advanced GraphML with ports (note: hyperedge thats not supported).
        /// </summary>
        private static readonly String GRAPH_WITH_PORTS =
            @"<?xml version=""1.0"" encoding=""UTF-8""?>
            <graphml xmlns=""http://graphml.graphdrawing.org/xmlns""  xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
            xsi:schemaLocation=""http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"">
                <key id=""stringAttr"" for=""node"" attr.name=""color"" attr.type=""string"">
                    <default>yellow</default>
                </key>
                <key id=""doubleAttr"" for=""edge"" attr.name=""weight"" attr.type=""double""/>
                <graph id=""G"" edgedefault=""directed"">
                    <node id=""n0"">
                      <port name=""North""/>
                      <port name=""South""/>
                      <port name=""East""/>
                      <port name=""West""/>
                    </node>
                    <node id=""n1"">
                      <port name=""North""/>
                      <port name=""South""/>
                      <port name=""East""/>
                      <port name=""West""/>
                    </node>
                    <node id=""n2"">
                      <port name=""NorthWest""/>
                      <port name=""SouthEast""/>
                    </node>
                    <node id=""n3"">
                      <port name=""NorthEast""/>
                      <port name=""SouthWest""/>
                    </node>
                    <edge source=""n0"" target=""n3"" sourceport=""North"" targetport=""NorthEast""/>
                    <hyperedge>
                       <endpoint node=""n0"" port=""North""/>
                       <endpoint node=""n1"" port=""East""/>
                       <endpoint node=""n2"" port=""SouthEast""/>
                    </hyperedge>
                </graph>
            </graphml>";  

        /// <summary>
        /// Advanced graph with nested graphs.
        /// </summary>
        private static readonly String GRAPH_NESTED_GRAPHS =
            @"<?xml version=""1.0"" encoding=""UTF-8""?>
            <graphml xmlns=""http://graphml.graphdrawing.org/xmlns""  xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
            xsi:schemaLocation=""http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"">
                <key id=""stringAttr"" for=""node"" attr.name=""color"" attr.type=""string"">
                    <default>yellow</default>
                </key>
                <key id=""doubleAttr"" for=""edge"" attr.name=""weight"" attr.type=""double""/>
                <graph id=""G"" edgedefault=""undirected"">
                    <node id=""n0""/>
                    <node id=""n1""/>
                    <node id=""n2""/>
                    <node id=""n3""/>
                    <node id=""n4""/>

                    <node id=""n5"">
                        <graph id=""n5:"" edgedefault=""undirected"">
                            <node id=""n5::n0""/>
                            <node id=""n5::n1""/>
                            <node id=""n5::n2""/>
                            <edge id=""e0"" source=""n5::n0"" target=""n5::n2""/>
                            <edge id=""e1"" source=""n5::n1"" target=""n5::n2""/>
                        </graph>
                    </node>
                    <node id=""n6"">
                        <graph id=""n6:"" edgedefault=""undirected"">
                            <node id=""n6::n0"">
                                <graph id=""n6::n0:"" edgedefault=""undirected"">
                                    <node id=""n6::n0::n0""/>
                                </graph>
                            </node>
                            <node id=""n6::n1""/>
                            <node id=""n6::n2""/>
                            <edge id=""e10"" source=""n6::n1"" target=""n6::n0::n0""/>
                            <edge id=""e11"" source=""n6::n1"" target=""n6::n2""/>
                        </graph>
                    </node>
                    <edge id=""e2"" source=""n5::n2"" target=""n0""/>
                    <edge id=""e3"" source=""n0"" target=""n2""/>
                    <edge id=""e4"" source=""n0"" target=""n1""/>
                    <edge id=""e5"" source=""n1"" target=""n3""/>
                    <edge id=""e6"" source=""n3"" target=""n2""/>
                    <edge id=""e7"" source=""n2"" target=""n4""/>
                    <edge id=""e8"" source=""n3"" target=""n6::n1""/>
                    <edge id=""e9"" source=""n6::n1"" target=""n4""/>
                </graph>
            </graphml>";
        
        /// <summary>
        /// Test case for the port-based graph.
        /// </summary>
        [TestMethod]
        public void TestGraphPorts()
        {
            GraphCollection graphs = new GraphCollection();
            using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(GRAPH_WITH_PORTS)))
            {
                graphs.LoadGraphs(stream);
                Assert.AreEqual(EdgeDefaultType.Directed, graphs.First().EdgeType);
            }
        }


        /// <summary>
        /// Test case for nested graphs.
        /// </summary>
        [TestMethod]
        public void TestNestedGraphs()
        {
            GraphCollection graphs = new GraphCollection();
            using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(GRAPH_NESTED_GRAPHS)))
            {
                graphs.LoadGraphs(stream);
                Assert.AreEqual(EdgeDefaultType.Undirected, graphs.First().EdgeType);
            }
        }
    }

} // RSG.GraphML.UnitTest namespace
