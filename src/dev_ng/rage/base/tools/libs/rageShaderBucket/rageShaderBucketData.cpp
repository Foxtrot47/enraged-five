// 
// rageShaderMaterial/rageShaderBucketData.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "grcore/state.h"
#include "grmodel/shader.h"
#include "grmodel/model.h"
#include "vector/matrix34.h"

#include "rageShaderBucketData.h"

using namespace rage;

int		rageShaderBucketData::sm_UserData0 = -1;
int		rageShaderBucketData::sm_UserData1 = -1;
rageShaderBucketDataDrawCallbackClass *	rageShaderBucketData::sm_pClass = NULL;

void*	rageShaderBucketData::sm_VoidUserData0 = NULL;


rageShaderBucketData::rageShaderBucketData()
{	
	Reset();
}

rageShaderBucketData::~rageShaderBucketData()
{
	Reset();
}

void rageShaderBucketData::Reset()
{
	m_bReverse		= false;
	m_uRef			= 0xffffffff;
	m_nGeomIndex	= 0;
	m_pShader		= NULL;
	m_pModel		= NULL;
	m_pMatrix		= NULL;

	m_UserData0		= -1;
	m_UserData1		= -1;
	m_voidUserData0 = NULL;
}

void rageShaderBucketData::UpdateData(const u32 uRef, const grmShader * pShader, const grmModel * pModel, const Matrix34 * pMatrix, const int nGeomIndex)
{
	m_uRef			= uRef;
	m_nGeomIndex	= nGeomIndex;
	m_pShader		= pShader;
	m_pModel		= pModel;
	m_pMatrix		= pMatrix;

	m_bReverse = m_pMatrix->Determinant3x3() < 0.0f;

	// copy static into member data
	m_UserData0 = sm_UserData0;
	m_UserData1 = sm_UserData1;
	m_voidUserData0 = sm_VoidUserData0;
}

void rageShaderBucketData::Draw() const
{
	if (m_pShader && m_pModel)
	{
		bool doDraw = true;// default - must be set to true

		if( sm_pClass )
		{
			// give the predraw function, if it exists the capability to dictate whether an object gets rendered or not.
			doDraw = sm_pClass->PreDraw( m_voidUserData0, m_UserData0, m_UserData1, (void*) this );
		}

		// by default. matrix, cullmode and drawing is invoked, PostDraw still occurs even if PreDraw returns false
		if( doDraw )
		{		
			if (m_pMatrix)
				grcViewport::SetCurrentWorldMtx(*m_pMatrix);

			// Check for negative scale
			if (m_bReverse)
			{
				grcState::SetCullMode(grccmFront);
			}
			else
			{
				grcState::SetCullMode(grccmBack);
			}

			m_pShader->Draw(*m_pModel, m_nGeomIndex, 0, false);
		}

		if( sm_pClass )
			sm_pClass->PostDraw( m_voidUserData0, m_UserData0, m_UserData1, (void*) this );
	}
}

bool	rageShaderBucketDataDrawCallbackClass::PreDraw( void*, int , int, void*  ) const
{
	return true;
}

bool	rageShaderBucketDataDrawCallbackClass::PostDraw( void*, int , int, void*  ) const
{
	return true;
}
