// 
// rageShaderMaterial/rageShaderBucket.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_BUCKET_H
#define RAGE_SHADER_BUCKET_H

#include "atl/array.h"
#include "atl/bucket.h"
#include "parser/manager.h"
#include "grcore/statetypes.h"
#include "grcore/effect_typedefs.h"

#ifndef RAGE_SHADER_BUCKET_DATA_CLASS
#include "rageShaderBucketData.h"
#define RAGE_SHADER_BUCKET_DATA_CLASS rageShaderBucketData
#endif

namespace	rage
{

class	grmShader;
class	grmModel;
class	Matrix34;

class	rageShaderBucketState
{
public:

	rageShaderBucketState() { m_nState = -1; m_Value.u = 0;}
	~rageShaderBucketState() {}

	grcStateType				GetState()			const {return (grcStateType)m_nState;}
	grceSamplerState			GetSamplerState()	const {return (grceSamplerState)m_nState;}

	u32		GetInt()		const {return m_Value.u;}
	float	GetFloat()		const {return m_Value.f;}

	void Init(int state, u32 u)		{m_nState = state; m_Value.u = u;}
	void Init(int state, float f)	{m_nState = state; m_Value.f = f;}

private:

	int							m_nState;
	union { u32 u; float f; }	m_Value;

};

class	rageShaderBucket
{
public:
	rageShaderBucket();
	virtual ~rageShaderBucket();

	// Accessors

	// Initialization
	bool Validate();

	void UpdateData(const u32 uRef, const grmShader * pShader, const grmModel * pModel, const Matrix34 * pMatrix, const int nGeomIndex=0);
	void DeleteData(const u32 uRef);
	void DeleteAllData();

	void AddState(const rageShaderBucketState & state);
	void AddSamplerState(const rageShaderBucketState & state);

	// Reset
	void Reset();

	// Draw
	void SetStates() const;
	void Draw() const;

private:

	grceSamplerState GetSamplerStateCommand(const char * szCommand) const;
	grcStateType    GetStateCommand(const char * szCommand) const;
	bool			GetStateValue(const char * szCommand, u32 & value) const;
	bool			GetStateValue(const char * szCommand, float & value) const;

	// Written data
	atArray <ConstString> m_aStateCommands;
	atArray <ConstString> m_aSamplerStateCommands;

	// Non-written data
	atBucket <RAGE_SHADER_BUCKET_DATA_CLASS *>	m_apData;
	atBucket <rageShaderBucketState>			m_aStates;
	atBucket <rageShaderBucketState>			m_aSamplerStates;

	PAR_PARSABLE;
};

} // end using namespace rage

#endif
