<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef type="::rage::rageShaderBucket">
	<array name="m_aStateCommands" type="atArray">
		<string type="pointer"/>
	</array>
	<array name="m_aSamplerStateCommands" type="atArray">
		<string type="pointer"/>
	</array>
</structdef>

</ParserSchema>