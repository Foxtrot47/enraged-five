// 
// rageShaderBucket/rageShaderGlobalData.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef RAGE_SHADER_GLOBAL_DATA_H
#define RAGE_SHADER_GLOBAL_DATA_H

#include "parser/manager.h"
#include "atl/array.h"
#include "grcore/effect_typedefs.h"
#include "string/string.h"
#include "vector/vector4.h"
#include "vector/vector3.h"
#include "vector/vector2.h"

namespace	rage
{

class rageShaderGlobalDataValue
{
public:

	rageShaderGlobalDataValue() 
	{
		Reset();
	}

	virtual ~rageShaderGlobalDataValue()
	{
		Reset();
	}

	bool IsValid() const 
	{
		return (m_nType >= 0);
	}

	void Validate();

	void Reset()
	{
		m_sType = "None";
		m_nType = -1;
		m_vVector4.Zero();
		m_vVector3.Zero();
		m_vVector2.Zero();
		m_fFloat = 0.0f;
		m_nInt = 0;
	}

	void Set(const Vector4 & v)
	{
		m_sType = "Vector4";
		m_nType = 0;
		m_vVector4.Set(v);
	}

	void Set(const Vector3 & v)
	{
		m_sType = "Vector4";
		m_nType = 1;
		m_vVector3.Set(v);
	}

	void Set(const Vector2 & v)
	{
		m_sType = "Vector2";
		m_nType = 2;
		m_vVector2.Set(v);
	}

	void Set(const float f)
	{
		m_sType = "float";
		m_nType = 3;
		m_fFloat = f;
	}

	void Set(const int n)
	{
		m_sType = "int";
		m_nType = 4;
		m_nInt = n;
	}

	void SetGlobalVariable(const grcEffectGlobalVar & GlobalVariable);

private:

	// Written data
	ConstString m_sType;
	Vector4	m_vVector4;
	Vector3	m_vVector3;
	Vector2	m_vVector2;
	float	m_fFloat;
	int		m_nInt;

	// Not written data
	int			m_nType;

	PAR_PARSABLE;
};

class	rageShaderGlobalData
{
public:
	rageShaderGlobalData();
	virtual ~rageShaderGlobalData();

	// Reset
	void Reset();

	// Modifiers
	void Validate();
	void SetGlobalVariableName(const char * szGlobalVariableName);
	void SetGlobalVariableValue(const Vector4 & v);
	void SetGlobalVariableValue(const Vector3 & v);
	void SetGlobalVariableValue(const Vector2 & v);
	void SetGlobalVariableValue(const float f);
	void SetGlobalVariableValue(const int n);
	void SetGlobalVariable();

private:

	// Written data
	ConstString							m_sGlobalVariableName;
	rageShaderGlobalDataValue			m_GlobalVariableValue;

	// Non-written data
	bool				m_bIsGlobalVariableSet;

	PAR_PARSABLE;
};

} // end using namespace rage

#endif
