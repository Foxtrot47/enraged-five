<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef type="::rage::rageShaderGlobalDataValue">
	<string name="m_sType" noInit="true" type="pointer"/>	
	<Vector4 name="m_vVector4" type="Vector4"/>
	<Vector3 name="m_vVector3" type="Vector3"/>
	<Vector2 name="m_vVector2" type="Vector2"/>
	<float name="m_fFloat" type="float"/>
	<int name="m_nInt" type="int"/>
</structdef>

<structdef type="::rage::rageShaderGlobalData">
	<string name="m_sGlobalVariableName" noInit="true" type="pointer"/>	
	<struct name="m_GlobalVariableValue" type="::rage::rageShaderGlobalDataValue"/>
</structdef>

</ParserSchema>