<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef type="::rage::rageShaderBucketManager">
	<struct name="m_DefaultData" type="::rage::rageShaderBucket"/>
	<array name="m_aData" type="atArray">
		<struct type="::rage::rageShaderBucket"/>
	</array>
	<array name="m_aGlobalData" type="atArray">
		<struct type="::rage::rageShaderGlobalData"/>
	</array>
	<string name="m_sGlobalTimeVariable" type="pointer"/>
	<int name="m_nGlobalTimeIndex" noInit="true"/>
	<int name="m_nGlobalTimeOfDayIndex" noInit="true"/>
</structdef>

</ParserSchema>