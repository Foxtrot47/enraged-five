// 
// rageShaderMaterial/rageShaderBucketData.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_BUCKET_DATA_H
#define RAGE_SHADER_BUCKET_DATA_H

namespace	rage
{

class	grmShader;
class	grmModel;
class	Matrix34;



class	rageShaderBucketDataDrawCallbackClass;

class	rageShaderBucketData
{
public:
	rageShaderBucketData();
	virtual ~rageShaderBucketData();

	static	void	RegisterDrawCallbackClass( rageShaderBucketDataDrawCallbackClass *pDerivedClass ) { sm_pClass = pDerivedClass; }

	// Accessors
	u32					GetRef()		const {return m_uRef;}
	int					GetGeomIndex()	const {return m_nGeomIndex;}
	const grmShader *	GetShader()		const {return m_pShader;}
	const grmModel *	GetModel()		const {return m_pModel;}
	const Matrix34 *	GetMatrix()		const {return m_pMatrix;}
	
	// Initialization
	void UpdateData(const u32 uRef, const grmShader * pShader, const grmModel * pModel, const Matrix34 * pMatrix, const int nGeomIndex=0);

	// Reset
	void Reset();

	// Draw
	virtual void Draw() const;	

	static	rageShaderBucketDataDrawCallbackClass	*sm_pClass;
	static	int	sm_UserData0;
	static	int sm_UserData1;
	static	void* sm_VoidUserData0;


	int	m_UserData0;
	int m_UserData1;
	void*  m_voidUserData0;


private:

	bool				m_bReverse;
	u32					m_uRef;
	int					m_nGeomIndex;
	const grmShader *	m_pShader;
	const grmModel *	m_pModel;
	const Matrix34 *	m_pMatrix;
};


class rageShaderBucketDataDrawCallbackClass {
public:
	virtual ~rageShaderBucketDataDrawCallbackClass () { }
	friend class	rageShaderBucketData;

	virtual	bool	PreDraw		( void * voidUserData0, int userdata0, int userdata1, void* pShaderBucketDataInst ) const;
	virtual	bool	PostDraw	( void * voidUserData0, int userdata0, int userdata1, void* pShaderBucketDataInst ) const;
};




} // end using namespace rage

#endif
