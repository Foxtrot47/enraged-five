// 
// rageShaderBucketManager/rageShaderBucketManager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rageShaderBucketManager.h"
#include "rageShaderBucketManager_parser.h"

#include "rageShaderBucket.h"

#include "system/timemgr.h"
#include "grcore/state.h"
#include "grcore/effect.h"

using namespace rage;

bool		rageShaderBucketManager::sm_IsShaderBucketLibraryInitalized = false;

bool rageShaderBucketManager::InitShaderBucketLibrary()
{
	if (!sm_IsShaderBucketLibraryInitalized)
	{
		sm_IsShaderBucketLibraryInitalized = true;

		REGISTER_PARSABLE_CLASS(rageShaderGlobalDataValue);
		REGISTER_PARSABLE_CLASS(rageShaderGlobalData);
		REGISTER_PARSABLE_CLASS(rageShaderBucket);
		REGISTER_PARSABLE_CLASS(rageShaderBucketManager);

		return true;
	}

	return false;
}

bool rageShaderBucketManager::ShutdownShaderBucketLibrary()
{
	if (sm_IsShaderBucketLibraryInitalized)
	{
		UNREGISTER_PARSABLE_CLASS(rageShaderGlobalDataValue);
		UNREGISTER_PARSABLE_CLASS(rageShaderGlobalData);
		UNREGISTER_PARSABLE_CLASS(rageShaderBucket);
		UNREGISTER_PARSABLE_CLASS(rageShaderBucketManager);

		sm_IsShaderBucketLibraryInitalized = false;

		return true;
	}

	return false;
}

rageShaderBucketManager::rageShaderBucketManager()
{
	Reset();
}

rageShaderBucketManager::~rageShaderBucketManager()
{
	Reset();
}

void rageShaderBucketManager::Reset()
{
	m_bGlobalVariablesSet	= false;
	m_GlobalTimeVariable    = grcegvNONE;
	m_nGlobalTimeIndex		= 0;
	m_nGlobalTimeOfDayIndex = 1;
	m_fGlobalTimeOfDay		= 12.0f;
	m_DefaultData.Reset();
	m_ForceData.Reset();
	m_aGlobalData.Reset();
	m_aData.Reset();
}

void rageShaderBucketManager::UpdateData(const int nBucket, const u32 uRef, const grmShader * pShader, const grmModel * pModel, const Matrix34 * pMatrix, const int nGeomIndex)
{
	if (!m_bGlobalVariablesSet)
	{
		SetGlobalVariables();
		m_bGlobalVariablesSet = true;
	}

	m_aData[nBucket].UpdateData(uRef, pShader, pModel, pMatrix, nGeomIndex);
}

void rageShaderBucketManager::DeleteData(const u32 uRef)
{
	for (int i=0; i<m_aData.GetCount(); i++)
	{
		m_aData[i].DeleteData(uRef);
	}
}

void rageShaderBucketManager::DeleteData(const int nBucket, const u32 uRef)
{
	m_aData[nBucket].DeleteData(uRef);
}

void rageShaderBucketManager::DeleteAllData()
{
	for (int i=0; i<m_aData.GetCount(); i++)
	{
		m_aData[i].DeleteAllData();
	}
}

void rageShaderBucketManager::AddForceState(const rageShaderBucketState & state)
{
	m_ForceData.AddState(state);
}

void rageShaderBucketManager::AddForceSamplerState(const rageShaderBucketState & state)
{
	m_ForceData.AddSamplerState(state);
}

void rageShaderBucketManager::SetGlobalVariables()
{
	if ((const char *)m_sGlobalTimeVariable)
	{
		m_GlobalTimeVariable = grcEffect::LookupGlobalVar((const char *)m_sGlobalTimeVariable, false);
	}

	for (int i=0; i<m_aGlobalData.GetCount(); i++)
	{
		m_aGlobalData[i].SetGlobalVariable();
	}
}


bool rageShaderBucketManager::LoadFromXML(fiStream * s)
{
	Reset();
	if (!PARSER.LoadObject(s, *this))
		return false;
	return true;
}

bool rageShaderBucketManager::LoadFromXML(const char * szFileName)
{
	bool bResult = false;
	fiStream * streamBucket = fiStream::Open(szFileName);
	if (streamBucket)
	{
		bResult = LoadFromXML(streamBucket);
		streamBucket->Close();
	}
	return bResult;
}


bool rageShaderBucketManager::SaveToXML(fiStream * s) const
{
	return PARSER.SaveObject(s, this);
}

bool rageShaderBucketManager::SaveToXML(const char * szFileName) const
{
	bool bResult = false;
	fiStream * streamBucket = fiStream::Create(szFileName);
	if (streamBucket)
	{
		bResult = SaveToXML(streamBucket);
		streamBucket->Close();
	}
	return bResult;
}

bool rageShaderBucketManager::Validate()
{
	for (int i=m_aData.GetCount(); i<RAGE_SHADER_MAX_BUCKETS; i++)
	{
		m_aData.Grow();
	}

	if (!m_DefaultData.Validate())
		return false;

	for (int i=0; i<m_aData.GetCount(); i++)
	{
		if (!m_aData[i].Validate())
			return false;
	}

	for (int i=0; i<m_aGlobalData.GetCount(); i++)
	{
		m_aGlobalData[i].Validate();
	}

	return true;
}

void rageShaderBucketManager::Draw() const
{
	for (int i=0; i<m_aData.GetCount(); i++)
	{
		m_DefaultData.SetStates();

		m_aData[i].SetStates();

		m_ForceData.SetStates();

		m_aData[i].Draw();
	}
}
