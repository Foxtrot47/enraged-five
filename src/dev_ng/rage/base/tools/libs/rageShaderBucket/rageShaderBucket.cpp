// 
// rageShaderMaterial/rageShaderBucket.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "file/stream.h"
#include "grcore/state.h"
#include "grcore/effect_values.h"
#include "grmodel/shaderfx.h"

#include "rageShaderBucket.h"
#include "rageShaderBucket_parser.h"

using namespace rage;

rageShaderBucket::rageShaderBucket()
{	
	Reset();
}

rageShaderBucket::~rageShaderBucket()
{
	Reset();
}

void rageShaderBucket::Reset()
{
	m_aStateCommands.Reset();
	m_aSamplerStateCommands.Reset();

	for (int i=0; i<m_apData.GetCount(); i++)
	{
		delete m_apData[i];
		m_apData[i] = NULL;
	}
	m_apData.Reset();

	m_aStates.Reset();
	m_aSamplerStates.Reset();
}

void rageShaderBucket::UpdateData(const u32 uRef, const grmShader * pShader, const grmModel * pModel, const Matrix34 * pMatrix, const int nGeomIndex)
{
	int nTemplateIndex = pShader->GetTemplateIndex();

	for (int i=0; i<m_apData.GetCount(); i++)
	{
		if (m_apData[i]->GetRef() == uRef)
		{
			if (m_apData[i]->GetShader()->GetTemplateIndex() == nTemplateIndex)
			{
				m_apData[i]->UpdateData(uRef, pShader, pModel, pMatrix, nGeomIndex);
				return;
			}
			else
			{
				delete m_apData[i];
				m_apData[i] = NULL;
				m_apData.Delete(i);
				break;
			}			
		}
	}

	for (int i=0; i<m_apData.GetCount(); i++)
	{
		if (m_apData[i]->GetShader()->GetTemplateIndex() > nTemplateIndex)
		{
			RAGE_SHADER_BUCKET_DATA_CLASS * pData = new RAGE_SHADER_BUCKET_DATA_CLASS;
			pData->UpdateData(uRef, pShader, pModel, pMatrix, nGeomIndex);
			m_apData.Insert(i) = pData;
			return;
		}
	}

	RAGE_SHADER_BUCKET_DATA_CLASS * pData = new RAGE_SHADER_BUCKET_DATA_CLASS;
	pData->UpdateData(uRef, pShader, pModel, pMatrix, nGeomIndex);
	m_apData.Append() = pData;
}

void rageShaderBucket::DeleteData(const u32 uRef)
{
	for (int i=0; i<m_apData.GetCount(); i++)
	{
		if (m_apData[i]->GetRef() == uRef)
		{
			delete m_apData[i];
			m_apData[i] = NULL;
			m_apData.Delete(i);
			return;
		}
	}
}

void rageShaderBucket::DeleteAllData()
{
	for (int i=0; i<m_apData.GetCount(); i++)
	{
		delete m_apData[i];
		m_apData[i] = NULL;
	}
	m_apData.Reset();
}

void rageShaderBucket::AddState(const rageShaderBucketState & state)
{
	m_aStates.Grow() = state;
}

void rageShaderBucket::AddSamplerState(const rageShaderBucketState & state)
{
	m_aSamplerStates.Grow() = state;
}

bool rageShaderBucket::GetStateValue(const char * szCommand, u32 & value) const
{
	float fValue = 0.0f;
	if (!GetStateValue(szCommand, fValue))
		return false;

	value = (u32)(fValue+0.5f);
	return true;
}

bool rageShaderBucket::GetStateValue(const char * szCommand, float & value) const
{
	const char * temp = strstr(szCommand, "(");
	if (!temp)
		return false;

	bool bValid = false;
	char szValue[128];
	memset(szValue, 0, sizeof(szValue));

	int iCount = 0;
	for (u32 i=1; i<strlen(temp); i++)
	{
		if ((temp[i] >= '0' && temp[i] <= '9') || temp[i] == '.' || temp[i] == '-')
		{
			szValue[iCount] = temp[i];
			iCount++;
		}

		if (temp[i] == ')')
		{
			bValid = true;
			break;
		}
	}

	if (bValid)
	{
		value = (float)atof(szValue);

		return true;
	}

	Errorf("Could not find value for %s", szCommand);
	return false;	
}

grcStateType rageShaderBucket::GetStateCommand(const char * szCommand) const
{
	if (strstr(szCommand, "SetCullMode"))
	{
		return grcsCullMode;
	}
	else if (strstr(szCommand, "SetAlphaBlend"))
	{
		return grcsAlphaBlend;
	}
	else if (strstr(szCommand, "SetAlphaTest"))
	{
		return grcsAlphaTest;
	}
	else if (strstr(szCommand, "SetFillMode"))
	{
		return grcsFillMode;
	}
	else if (strstr(szCommand, "SetBlendSet"))
	{
		return grcsBlendSet;
	}
	else if (strstr(szCommand, "SetAlphaFunc"))
	{
		return grcsAlphaFunc;
	}
	else if (strstr(szCommand, "SetAlphaRef"))
	{
		return grcsAlphaRef;
	}
	else if (strstr(szCommand, "SetDepthFunc"))
	{
		return grcsDepthFunc;
	}
	else if (strstr(szCommand, "SetDepthTest"))
	{
		return grcsDepthTest;
	}
	else if (strstr(szCommand, "SetDepthWrite"))
	{
		return grcsDepthWrite;
	}
	else if (strstr(szCommand, "SetDepthBias"))
	{
		return grcsDepthBias;
	}
	else if (strstr(szCommand, "SetSlopeScaleDepthBias"))
	{
		return grcsSlopeScaleDepthBias;
	}
	else if (strstr(szCommand, "SetColorWrite"))
	{
		return grcsColorWrite;
	}
	else if (strstr(szCommand, "SetLightingMode"))
	{
		return grcsLighting;
	}
	else if (strstr(szCommand, "SetBestLightingMode"))
	{
		return grcsLighting;
	}
	else
	{
		Errorf("Could not find command %s", szCommand);
	}
		
	return grcsCount;
}

grceSamplerState rageShaderBucket::GetSamplerStateCommand(const char * szCommand) const
{
	if (strstr(szCommand, "ADDRESSU"))
	{
		return grcessADDRESSU;
	}
	else if (strstr(szCommand, "ADDRESSV"))
	{
		return grcessADDRESSV;
	}
	else if (strstr(szCommand, "ADDRESSW"))
	{
		return grcessADDRESSW;
	}
	else if (strstr(szCommand, "BORDERCOLOR"))
	{
		return grcessBORDERCOLOR;
	}
	else if (strstr(szCommand, "MAGFILTER"))
	{
		return grcessMAGFILTER;
	}
	else if (strstr(szCommand, "MINFILTER"))
	{
		return grcessMINFILTER;
	}
	else if (strstr(szCommand, "MIPFILTER"))
	{
		return grcessMIPFILTER;
	}
	else if (strstr(szCommand, "MIPMAPLODBIAS"))
	{
		return grcessMIPMAPLODBIAS;
	}
	else if (strstr(szCommand, "MAXMIPLEVEL"))
	{
		return grcessMAXMIPLEVEL;
	}
	else if (strstr(szCommand, "MAXANISOTROPY"))
	{
		return grcessMAXANISOTROPY;
	}
	else
	{
		Errorf("Could not find sampler state %s", szCommand);
	}

	return grcessCOUNT;
}

bool rageShaderBucket::Validate()
{
	for (int i=0; i<m_aStateCommands.GetCount(); i++)
	{
		switch (GetStateCommand((const char *)m_aStateCommands[i]))
		{
		case grcsCullMode:
			{
				if (strstr((const char *)m_aStateCommands[i], "grccmNone"))
				{
					m_aStates.Grow().Init(grcsCullMode, (u32)grccmNone);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccmFront"))
				{
					m_aStates.Grow().Init(grcsCullMode, (u32)grccmFront);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccmBack"))
				{
					m_aStates.Grow().Init(grcsCullMode, (u32)grccmBack);
				}
				else
				{
					Errorf("Could not find correct cull mode.");
				}
				break;
			}
		case grcsAlphaBlend:
			{
				if (strstr((const char *)m_aStateCommands[i], "true"))
				{
					m_aStates.Grow().Init(grcsAlphaBlend, (u32)1);
				}
				else if (strstr((const char *)m_aStateCommands[i], "false"))
				{
					m_aStates.Grow().Init(grcsAlphaBlend, (u32)0);
				}
				else
				{
					Errorf("Could not find true/false for set alpha blend.");
				}
				break;
			}
		case grcsAlphaTest:
			{
				if (strstr((const char *)m_aStateCommands[i], "true"))
				{
					m_aStates.Grow().Init(grcsAlphaTest, (u32)1);
				}
				else if (strstr((const char *)m_aStateCommands[i], "false"))
				{
					m_aStates.Grow().Init(grcsAlphaTest, (u32)0);
				}
				else
				{
					Errorf("Could not find true/false for set alpha test.");
				}
				break;
			}
		case grcsFillMode:
			{
				if (strstr((const char *)m_aStateCommands[i], "grcfmPoint"))
				{
					m_aStates.Grow().Init(grcsFillMode, (u32)grcfmPoint);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcfmWireframe"))
				{
					m_aStates.Grow().Init(grcsFillMode, (u32)grcfmWireframe);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcfmSolid"))
				{
					m_aStates.Grow().Init(grcsFillMode, (u32)grcfmSolid);
				}
				else
				{
					Errorf("Could not find grcFillMode for set fill mode.");
				}
				break;
			}
		case grcsBlendSet:
			{
				if (strstr((const char *)m_aStateCommands[i], "grcbsNormal"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsNormal);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsAdd"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsAdd);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsSubtract"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsSubtract);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsLightmap"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsLightmap);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsMatte"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsMatte);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsOverwrite"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsOverwrite);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsDest"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsDest);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsAlphaAdd"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsAlphaAdd);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsReverseSubtract"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsReverseSubtract);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsMin"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsMin);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsMax"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsMax);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsAlphaSubtract"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsAlphaSubtract);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcbsMultiplySrcDest"))
				{
					m_aStates.Grow().Init(grcsBlendSet, (u32)grcbsMultiplySrcDest);
				}
				else
				{
					Errorf("Could not find grcBlendSet for set blend set.");
				}
				break;
			}
		case grcsAlphaFunc:
			{
				if (strstr((const char *)m_aStateCommands[i], "grcafNever"))
				{
					m_aStates.Grow().Init(grcsAlphaFunc, (u32)grcafNever);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcafAlways"))
				{
					m_aStates.Grow().Init(grcsAlphaFunc, (u32)grcafAlways);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcafLess"))
				{
					m_aStates.Grow().Init(grcsAlphaFunc, (u32)grcafLess);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcafLessEqual"))
				{
					m_aStates.Grow().Init(grcsAlphaFunc, (u32)grcafLessEqual);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcafEqual"))
				{
					m_aStates.Grow().Init(grcsAlphaFunc, (u32)grcafEqual);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcafGreaterEqual"))
				{
					m_aStates.Grow().Init(grcsAlphaFunc, (u32)grcafGreaterEqual);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcafGreater"))
				{
					m_aStates.Grow().Init(grcsAlphaFunc, (u32)grcafGreater);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcafNotEqual"))
				{
					m_aStates.Grow().Init(grcsAlphaFunc, (u32)grcafNotEqual);
				}
				else
				{
					Errorf("Could not find grcAlphaFunc for set alpha function.");
				}
				break;
			}
		case grcsAlphaRef:
			{
				u32 value = 0;
				if (GetStateValue((const char *)m_aStateCommands[i], value))
				{
					m_aStates.Grow().Init(grcsAlphaRef, value);
				}
				else
				{
					Errorf("Could not find integer for set alpha ref.");
				}				
				break;
			}
		case grcsDepthFunc:
			{
				if (strstr((const char *)m_aStateCommands[i], "grcdfNever"))
				{
					m_aStates.Grow().Init(grcsDepthFunc, (u32)grcdfNever);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcdfAlways"))
				{
					m_aStates.Grow().Init(grcsDepthFunc, (u32)grcdfAlways);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcdfLess"))
				{
					m_aStates.Grow().Init(grcsDepthFunc, (u32)grcdfLess);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcdfLessEqual"))
				{
					m_aStates.Grow().Init(grcsDepthFunc, (u32)grcdfLessEqual);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcdfEqual"))
				{
					m_aStates.Grow().Init(grcsDepthFunc, (u32)grcdfEqual);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcdfGreaterEqual"))
				{
					m_aStates.Grow().Init(grcsDepthFunc, (u32)grcdfGreaterEqual);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcdfGreater"))
				{
					m_aStates.Grow().Init(grcsDepthFunc, (u32)grcdfGreater);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grcdfNotEqual"))
				{
					m_aStates.Grow().Init(grcsDepthFunc, (u32)grcdfNotEqual);
				}
				else
				{
					Errorf("Could not find grcDepthFunc for set depth function.");
				}
				break;
			}
		case grcsDepthTest:
			{
				if (strstr((const char *)m_aStateCommands[i], "true"))
				{
					m_aStates.Grow().Init(grcsDepthTest, (u32)1);
				}
				else if (strstr((const char *)m_aStateCommands[i], "false"))
				{
					m_aStates.Grow().Init(grcsDepthTest, (u32)0);
				}
				else
				{
					Errorf("Could not find true/false for set depth test.");
				}
				break;
			}
		case grcsDepthWrite:
			{
				if (strstr((const char *)m_aStateCommands[i], "true"))
				{
					m_aStates.Grow().Init(grcsDepthWrite, (u32)1);
				}
				else if (strstr((const char *)m_aStateCommands[i], "false"))
				{
					m_aStates.Grow().Init(grcsDepthWrite, (u32)0);
				}
				else
				{
					Errorf("Could not find true/false for set depth write.");
				}
				break;
			}
		case grcsDepthBias:
			{
				float value = 0.0f;
				if (GetStateValue((const char *)m_aStateCommands[i], value))
				{
					m_aStates.Grow().Init(grcsDepthBias, value);
				}
				else
				{
					Errorf("Could not find float for set depth bias.");
				}
				break;
			}			
		case grcsSlopeScaleDepthBias:
			{
				float value = 0.0f;
				if (GetStateValue((const char *)m_aStateCommands[i], value))
				{
					m_aStates.Grow().Init(grcsSlopeScaleDepthBias, value);
				}
				else
				{
					Errorf("Could not find float for set slope scale depth bias.");
				}
				break;
			}
		case grcsColorWrite:
			{
				if (strstr((const char *)m_aStateCommands[i], "grccwNone"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwNone);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwRed"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwRed);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwGreen"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwGreen);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwRedGreen"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwRedGreen);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwRedBlue"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwRedBlue);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwGreenBlue"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwGreenBlue);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwRGB"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwRGB);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwAlpha"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwAlpha);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwRedAlpha"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwRedAlpha);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwGreenAlpha"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwGreenAlpha);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwRedGreenAlpha"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwRedGreenAlpha);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwBlueAlpha"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwBlueAlpha);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwRedBlueAlpha"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwRedBlueAlpha);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwGreenBlueAlpha"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwGreenBlueAlpha);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grccwRGBA"))
				{
					m_aStates.Grow().Init(grcsColorWrite, (u32)grccwRGBA);
				}
				else
				{
					Errorf("Could not find grcColorWrite for set color write.");
				}
				break;
			}					
		case grcsLighting:
			{
				if (strstr((const char *)m_aStateCommands[i], "grclmNone"))
				{
					m_aStates.Grow().Init(grcsLighting, (u32)grclmNone);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grclmDirectional"))
				{
					m_aStates.Grow().Init(grcsLighting, (u32)grclmDirectional);
				}
				else if (strstr((const char *)m_aStateCommands[i], "grclmPoint"))
				{
					m_aStates.Grow().Init(grcsLighting, (u32)grclmPoint);
				}
				//else if (strstr((const char *)m_aStateCommands[i], "grclmSpot"))
				//{
				//	m_aStates.Grow().Init(grcsLighting, (u32)grclmSpot);
				//}
				else
				{
					Errorf("Could not find grcLightingMode for set lighting mode.");
				}
				break;
			}
		default:
			{
				Errorf("Could not find command %s", (const char *)(const char *)m_aStateCommands[i]);
			}			
		}
	}

	for (int i=0; i<m_aSamplerStateCommands.GetCount(); i++)
	{
		switch (GetSamplerStateCommand((const char *)m_aSamplerStateCommands[i]))
		{
		case grcessADDRESSU:
			{
				if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_WRAP"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSU, (u32)grcSSV::TADDRESS_WRAP);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRROR"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSU, (u32)grcSSV::TADDRESS_MIRROR);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_CLAMP"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSU, (u32)grcSSV::TADDRESS_CLAMP);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_CLAMP"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSU, (u32)grcSSV::TADDRESS_CLAMP);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_BORDER"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSU, (u32)grcSSV::TADDRESS_BORDER);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRRORONCE"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSU, (u32)grcSSV::TADDRESS_MIRRORONCE);
				}
#if __XENON
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_BORDER_HALF"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSU, (u32)grcSSV::TADDRESS_BORDER_HALF);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRRORONCE_BORDER_HALF"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSU, (u32)grcSSV::TADDRESS_MIRRORONCE_BORDER_HALF);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRRORONCE_BORDER"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSU, (u32)grcSSV::TADDRESS_MIRRORONCE_BORDER);
				}
#endif
				else
				{
					Errorf("Could not find correct grcessADDRESSU value.");
				}
				break;
			}
		case grcessADDRESSV:
			{
				if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_WRAP"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSV, (u32)grcSSV::TADDRESS_WRAP);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRROR"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSV, (u32)grcSSV::TADDRESS_MIRROR);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_CLAMP"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSV, (u32)grcSSV::TADDRESS_CLAMP);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_CLAMP"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSV, (u32)grcSSV::TADDRESS_CLAMP);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_BORDER"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSV, (u32)grcSSV::TADDRESS_BORDER);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRRORONCE"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSV, (u32)grcSSV::TADDRESS_MIRRORONCE);
				}
#if __XENON
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_BORDER_HALF"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSV, (u32)grcSSV::TADDRESS_BORDER_HALF);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRRORONCE_BORDER_HALF"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSV, (u32)grcSSV::TADDRESS_MIRRORONCE_BORDER_HALF);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRRORONCE_BORDER"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSV, (u32)grcSSV::TADDRESS_MIRRORONCE_BORDER);
				}
#endif
				else
				{
					Errorf("Could not find correct grcessADDRESSV value.");
				}
				break;
			}
		case grcessADDRESSW:
			{
				if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_WRAP"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSW, (u32)grcSSV::TADDRESS_WRAP);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRROR"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSW, (u32)grcSSV::TADDRESS_MIRROR);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_CLAMP"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSW, (u32)grcSSV::TADDRESS_CLAMP);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_CLAMP"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSW, (u32)grcSSV::TADDRESS_CLAMP);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_BORDER"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSW, (u32)grcSSV::TADDRESS_BORDER);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRRORONCE"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSW, (u32)grcSSV::TADDRESS_MIRRORONCE);
				}
#if __XENON
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_BORDER_HALF"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSW, (u32)grcSSV::TADDRESS_BORDER_HALF);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRRORONCE_BORDER_HALF"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSW, (u32)grcSSV::TADDRESS_MIRRORONCE_BORDER_HALF);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TADDRESS_MIRRORONCE_BORDER"))
				{
					m_aSamplerStates.Grow().Init(grcessADDRESSW, (u32)grcSSV::TADDRESS_MIRRORONCE_BORDER);
				}
#endif
				else
				{
					Errorf("Could not find correct grcessADDRESSW value.");
				}
				break;
			}
		case grcessBORDERCOLOR:
			{
				u32 value = 0;
				if (GetStateValue((const char *)m_aSamplerStateCommands[i], value))
				{
					m_aSamplerStates.Grow().Init(grcessBORDERCOLOR, value);
				}
				else
				{
					Errorf("Could not find value for border color.");
				}
				break;
			}
		case grcessMAGFILTER:
			{
				if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_NONE"))
				{
					m_aSamplerStates.Grow().Init(grcessMAGFILTER, (u32)grcSSV::TEXF_NONE);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_POINT"))
				{
					m_aSamplerStates.Grow().Init(grcessMAGFILTER, (u32)grcSSV::TEXF_POINT);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_LINEAR"))
				{
					m_aSamplerStates.Grow().Init(grcessMAGFILTER, (u32)grcSSV::TEXF_LINEAR);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_ANISOTROPIC"))
				{
					m_aSamplerStates.Grow().Init(grcessMAGFILTER, (u32)grcSSV::TEXF_ANISOTROPIC);
				}
#if !__XENON
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_PYRAMIDALQUAD"))
				{
					m_aSamplerStates.Grow().Init(grcessMAGFILTER, (u32)grcSSV::TEXF_PYRAMIDALQUAD);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_GAUSSIANQUAD"))
				{
					m_aSamplerStates.Grow().Init(grcessMAGFILTER, (u32)grcSSV::TEXF_GAUSSIANQUAD);
				}
#endif
				else
				{
					Errorf("Could not find correct grcessMAGFILTER value.");
				}
				break;
			}
		case grcessMINFILTER:
			{
				if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_NONE"))
				{
					m_aSamplerStates.Grow().Init(grcessMINFILTER, (u32)grcSSV::TEXF_NONE);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_POINT"))
				{
					m_aSamplerStates.Grow().Init(grcessMINFILTER, (u32)grcSSV::TEXF_POINT);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_LINEAR"))
				{
					m_aSamplerStates.Grow().Init(grcessMINFILTER, (u32)grcSSV::TEXF_LINEAR);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_ANISOTROPIC"))
				{
					m_aSamplerStates.Grow().Init(grcessMINFILTER, (u32)grcSSV::TEXF_ANISOTROPIC);
				}
#if !__XENON
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_PYRAMIDALQUAD"))
				{
					m_aSamplerStates.Grow().Init(grcessMINFILTER, (u32)grcSSV::TEXF_PYRAMIDALQUAD);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_GAUSSIANQUAD"))
				{
					m_aSamplerStates.Grow().Init(grcessMINFILTER, (u32)grcSSV::TEXF_GAUSSIANQUAD);
				}
#endif
				else
				{
					Errorf("Could not find correct grcessMINFILTER value.");
				}
				break;
			}
		case grcessMIPFILTER:
			{
				if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_NONE"))
				{
					m_aSamplerStates.Grow().Init(grcessMIPFILTER, (u32)grcSSV::TEXF_NONE);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_POINT"))
				{
					m_aSamplerStates.Grow().Init(grcessMIPFILTER, (u32)grcSSV::TEXF_POINT);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_LINEAR"))
				{
					m_aSamplerStates.Grow().Init(grcessMIPFILTER, (u32)grcSSV::TEXF_LINEAR);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_ANISOTROPIC"))
				{
					m_aSamplerStates.Grow().Init(grcessMIPFILTER, (u32)grcSSV::TEXF_ANISOTROPIC);
				}
#if !__XENON
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_PYRAMIDALQUAD"))
				{
					m_aSamplerStates.Grow().Init(grcessMIPFILTER, (u32)grcSSV::TEXF_PYRAMIDALQUAD);
				}
				else if (strstr((const char *)m_aSamplerStateCommands[i], "TEXF_GAUSSIANQUAD"))
				{
					m_aSamplerStates.Grow().Init(grcessMIPFILTER, (u32)grcSSV::TEXF_GAUSSIANQUAD);
				}
#endif
				else
				{
					Errorf("Could not find correct grcessMIPFILTER value.");
				}
				break;
			}
		case grcessMIPMAPLODBIAS:
			{
				float value = 0.0f;
				if (GetStateValue((const char *)m_aSamplerStateCommands[i], value))
				{
					m_aSamplerStates.Grow().Init(grcessMIPMAPLODBIAS, value);
				}
				else
				{
					Errorf("Could not find value for border color.");
				}
				break;
			}
		case grcessMAXMIPLEVEL:
			{
				u32 value = 0;
				if (GetStateValue((const char *)m_aSamplerStateCommands[i], value))
				{
					m_aSamplerStates.Grow().Init(grcessMAXMIPLEVEL, value);
				}
				else
				{
					Errorf("Could not find value for border color.");
				}
				break;
			}
		case grcessMAXANISOTROPY:
			{
				u32 value = 0;
				if (GetStateValue((const char *)m_aSamplerStateCommands[i], value))
				{
					m_aSamplerStates.Grow().Init(grcessMAXANISOTROPY, value);
				}
				else
				{
					Errorf("Could not find value for border color.");
				}
				break;
			}
		default:
			{
				Errorf("Could not find command %s", (const char *)(const char *)m_aSamplerStateCommands[i]);
			}			
		}
	}

	return true;
}

void rageShaderBucket::SetStates() const
{
	// Run through all the render states
	for (int iState=0; iState<m_aStates.GetCount(); iState++)
	{
		grcState::SetState(m_aStates[iState].GetState(), m_aStates[iState].GetInt());
	}

	// Run through all the sampler states
	for (int i=0; i<m_aSamplerStates.GetCount(); i++)
	{
		grcEffect::SetDefaultSamplerState(m_aSamplerStates[i].GetSamplerState(), m_aSamplerStates[i].GetInt());
	}
}

void rageShaderBucket::Draw() const
{
	for (int i=0; i<m_apData.GetCount(); i++)
	{
		m_apData[i]->Draw();
	}
}
