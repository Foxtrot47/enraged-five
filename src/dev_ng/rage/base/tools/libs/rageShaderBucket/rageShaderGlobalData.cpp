// 
// rageShaderBucket/rageShaderGlobalData.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include "rageShaderGlobalData.h"
#include "rageShaderGlobalData_parser.h"

#include "grcore/effect.h"

using namespace rage;

rageShaderGlobalData::rageShaderGlobalData()
{	
	Reset();
}

rageShaderGlobalData::~rageShaderGlobalData()
{
	Reset();
}

void rageShaderGlobalData::Reset()
{
	m_bIsGlobalVariableSet = false;
	m_sGlobalVariableName = "None";
	m_GlobalVariableValue.Reset();
}

void rageShaderGlobalData::Validate()
{
	m_GlobalVariableValue.Validate();
}

void rageShaderGlobalData::SetGlobalVariableName(const char * szGlobalVariableName)
{
	m_sGlobalVariableName = szGlobalVariableName;
}

void rageShaderGlobalData::SetGlobalVariableValue(const Vector4 & v)
{
	m_GlobalVariableValue.Reset();
	m_GlobalVariableValue.Set(v);
}

void rageShaderGlobalData::SetGlobalVariableValue(const Vector3 & v)
{
	m_GlobalVariableValue.Reset();
	m_GlobalVariableValue.Set(v);
}

void rageShaderGlobalData::SetGlobalVariableValue(const Vector2 & v)
{
	m_GlobalVariableValue.Reset();
	m_GlobalVariableValue.Set(v);
}

void rageShaderGlobalData::SetGlobalVariableValue(const float f)
{
	m_GlobalVariableValue.Reset();
	m_GlobalVariableValue.Set(f);
}

void rageShaderGlobalData::SetGlobalVariableValue(const int n)
{
	m_GlobalVariableValue.Reset();
	m_GlobalVariableValue.Set(n);
}

void rageShaderGlobalData::SetGlobalVariable()
{
	if (!m_bIsGlobalVariableSet && m_GlobalVariableValue.IsValid())
	{
		grcEffectGlobalVar GlobalVariable = grcEffect::LookupGlobalVar((const char *)m_sGlobalVariableName, false);
		if (GlobalVariable != grcegvNONE)
		{
			m_GlobalVariableValue.SetGlobalVariable(GlobalVariable);
			m_bIsGlobalVariableSet = true;
		}
	}
}
void rageShaderGlobalDataValue::Validate()
{
	if (strcmp(m_sType, "Vector4")==0)
	{
		m_nType = 0;
	}
	else if (strcmp(m_sType, "Vector3")==0)
	{
		m_nType = 1;
	}
	else if (strcmp(m_sType, "Vector2")==0)
	{
		m_nType = 2;
	}
	else if (strcmp(m_sType, "float")==0)
	{
		m_nType = 3;
	}
	else if (strcmp(m_sType, "int")==0)
	{
		m_nType = 4;
	}
}

void rageShaderGlobalDataValue::SetGlobalVariable(const grcEffectGlobalVar & GlobalVariable)
{
	if (m_nType == 0)
	{
		grcEffect::SetGlobalVar(GlobalVariable, m_vVector4);
	}
	else if (m_nType == 1)
	{
		grcEffect::SetGlobalVar(GlobalVariable, m_vVector3);
	}
	else if (m_nType == 2)
	{
		grcEffect::SetGlobalVar(GlobalVariable, m_vVector2);
	}
	else if (m_nType == 3)
	{
		grcEffect::SetGlobalVar(GlobalVariable, m_fFloat);
	}
	else if (m_nType == 4)
	{
		grcEffect::SetGlobalVar(GlobalVariable, (float) m_nInt);
	}	
}
