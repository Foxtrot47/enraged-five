// 
// rageShaderBucket/rageShaderBucketManager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// 
// rageShaderMaterial/rageShaderBucket.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_BUCKET_MANAGER_H
#define RAGE_SHADER_BUCKET_MANAGER_H

#include "atl/array.h"
#include "parser/manager.h"
#include "system/timer.h"

#include "rageShaderBucket.h"
#include "rageShaderGlobalData.h"

#define RAGE_SHADER_MAX_BUCKETS 16

namespace	rage
{

class   grmShader;
class	grmModel;
class	Matrix34;
class	fiStream;

class	rageShaderBucketManager
{
public:
	rageShaderBucketManager();
	virtual ~rageShaderBucketManager();

	// Initialization
	static bool InitShaderBucketLibrary();
	static bool ShutdownShaderBucketLibrary();

	bool LoadFromXML(fiStream * s);
	bool LoadFromXML(const char * szFileName);
	bool SaveToXML(fiStream * s) const;
	bool SaveToXML(const char * szFileName) const;

	bool Validate();

	// Reset
	void Reset();

	// Modifiers
	void UpdateData(const int nBucket, const u32 uRef, const grmShader * pShader, const grmModel * pModel, const Matrix34 * pMatrix, const int nGeomIndex=0);
	void DeleteData(const int nBucket, const u32 uRef);
	void DeleteData(const u32 uRef);
	void DeleteAllData();

	void AddForceState(const rageShaderBucketState & state);
	void AddForceSamplerState(const rageShaderBucketState & state);

	void SetGlobalVariables();

	void SetGlobalTimeOfDay(const float fTimeOfDay)		{m_fGlobalTimeOfDay = fTimeOfDay;}

	// Draw
	void Draw() const;

private:

	// Written data
	ConstString						m_sGlobalTimeVariable;
	int								m_nGlobalTimeIndex;
	int								m_nGlobalTimeOfDayIndex;
	rageShaderBucket				m_DefaultData;
	atArray <rageShaderBucket>		m_aData;
	atArray <rageShaderGlobalData>	m_aGlobalData;
	

	// Non-written data
	bool						m_bGlobalVariablesSet;
	rageShaderBucket			m_ForceData;
	grcEffectGlobalVar			m_GlobalTimeVariable;
	float						m_fGlobalTimeOfDay;
	static bool					sm_IsShaderBucketLibraryInitalized;

	PAR_PARSABLE;
	
};

} // end using namespace rage

#endif
