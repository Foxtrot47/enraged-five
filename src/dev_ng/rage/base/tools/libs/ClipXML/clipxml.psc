<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="rage::PropertyAttributeXML" autoregister="true">
    <string name="m_Name" type="atString"/>
    <u32 name="m_Type" init="0" />
  </structdef>

  <structdef type="rage::PropertyAttributeStringXML" base="rage::PropertyAttributeXML" autoregister="true">
    <string name="m_Value" type="atString"/>
  </structdef>

  <structdef type="rage::PropertyAttributeBoolXML" base="rage::PropertyAttributeXML" autoregister="true">
    <bool name="m_Value" init="false" />
  </structdef>

  <structdef type="rage::PropertyAttributeIntXML" base="rage::PropertyAttributeXML" autoregister="true">
    <s32 name="m_Value" init="0" />
  </structdef>

  <structdef type="rage::PropertyAttributeFloatXML" base="rage::PropertyAttributeXML" autoregister="true">
    <float name="m_Value" />
  </structdef>

  <structdef type="rage::PropertyAttributeQuaternionXML" base="rage::PropertyAttributeXML" autoregister="true">
    <Vec4V name="m_Value" />
  </structdef>

  <structdef type="rage::PropertyAttributeVector3XML" base="rage::PropertyAttributeXML" autoregister="true">
    <Vec3V name="m_Value" />
  </structdef>

  <structdef type="rage::PropertyXML" autoregister="true">
    <string name="m_Name" type="atString"/>
    <array name="m_Attributes" type="atArray">
      <pointer type="rage::PropertyAttributeXML" policy="owner"/>
    </array>
  </structdef>

  <structdef type="rage::TagXML" autoregister="true">
    <float name="m_StartTime" />
    <float name="m_EndTime" />
    <struct name="m_Property" type="rage::PropertyXML"/>
  </structdef>

  <structdef type="rage::ClipXML" autoregister="true">
	  <u32 name="m_ClipType" init="0" />
	  <string name="m_AnimationName" type="atString"/>
	  <bool name="m_Looped" init="false" />
	  <float name="m_StartTime" />
	  <float name="m_EndTime" />
	  <float name="m_Rate" />
    <map name="m_Properties"  type="atMap" key="u32">
      <pointer type="rage::PropertyXML" policy="owner"/>
    </map>
    <array name="m_Tags" type="atArray">
      <pointer type="rage::TagXML" policy="owner"/>
    </array>
  </structdef>

</ParserSchema>