#pragma once

#include "clipxml.h"

namespace ClipXMLUtil
{
	rage::ClipXML* ConcatClipXMLs(rage::atArray<rage::ClipXML*>& clipList);
}