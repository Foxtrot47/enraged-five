#include "ClipXML.h"

#include "clipxml_parser.h"
#include "vectormath/classes.h"

namespace rage
{

//////////////////////////////////////////////////////////////////////////
TagXML::TagXML() : m_EndTime(0),
					m_StartTime(0)
{}
//////////////////////////////////////////////////////////////////////////
TagXML* TagXML::Clone()
{
	return rage_new TagXML(*this);
}
//////////////////////////////////////////////////////////////////////////
TagXML::TagXML(TagXML& tag)
{
	m_EndTime = tag.GetEnd();
	m_StartTime = tag.GetStart();

	for(int i=0; i < tag.GetProperty().GetNumAttributes(); ++i)
	{
		m_Property.AddAttribute(tag.GetProperty().GetAttributeByIndex(i)->Clone());
	}
}
//////////////////////////////////////////////////////////////////////////
TagXML::~TagXML()
{
	for(int i=0; i < m_Property.GetNumAttributes(); ++i)
	{
		delete m_Property.GetAttributeByIndex(i);
	}
}
//////////////////////////////////////////////////////////////////////////
PropertyXML::PropertyXML() : m_Name("")
{}
//////////////////////////////////////////////////////////////////////////
PropertyXML::~PropertyXML()
{
	for(int i=0; i < m_Attributes.GetCount(); ++i)
	{
		delete m_Attributes[i];
	}
}
//////////////////////////////////////////////////////////////////////////
PropertyXML* PropertyXML::Clone()
{
	PropertyXML* prop = rage_new PropertyXML();
	prop->SetName(m_Name);

	for(int i=0; i < m_Attributes.GetCount(); ++i)
	{
		m_Attributes.PushAndGrow(m_Attributes[i]->Clone());
	}

	return prop;
}
//////////////////////////////////////////////////////////////////////////
void PropertyXML::AddAttribute(PropertyAttributeXML* attr)
{
	m_Attributes.PushAndGrow(attr);
}
//////////////////////////////////////////////////////////////////////////
bool PropertyXML::operator==(const PropertyXML& other) const
{
	if(GetKey() != other.GetKey())
	{
		return false;
	}

	if(GetNumAttributes() != other.GetNumAttributes())
	{
		return false;
	}

	for(int i=0; i<m_Attributes.GetCount(); ++i)
	{
		if(*m_Attributes[i] != *other.m_Attributes[i])
		{
			return false;
		}
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////
bool PropertyAttributeXML::operator==(const PropertyAttributeXML& other) const
{
	if(GetKey() != other.GetKey())
	{
		return false;
	}
	if(m_Type != other.m_Type)
	{
		return false;
	}

	return true;
}
//////////////////////////////////////////////////////////////////////////
inline bool PropertyAttributeXML::operator!=(const PropertyAttributeXML& other) const
{
	return !(*this == other);
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeXML* PropertyXML::GetAttributeByIndex(u32 idx)
{
	return m_Attributes[idx];
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeXML* PropertyXML::GetAttribute(const char* name)
{
	for(int i=0; i < m_Attributes.GetCount(); ++i)
	{
		if(m_Attributes[i]->GetKey() == atHashString(name))
		{
			return m_Attributes[i];
		}
	}

	return NULL;
}
//////////////////////////////////////////////////////////////////////////
void PropertyAttributeQuaternionXML::FromQuaternion(QuatV& quat)
{
	m_Value.SetX(quat.GetX());
	m_Value.SetY(quat.GetY());
	m_Value.SetZ(quat.GetZ());
	m_Value.SetW(quat.GetW());
}
//////////////////////////////////////////////////////////////////////////
void PropertyAttributeQuaternionXML::GetQuaternion(QuatV& quat)
{
	quat.SetXf(m_Value.GetX().Getf());
	quat.SetYf(m_Value.GetY().Getf());
	quat.SetZf(m_Value.GetZ().Getf());
	quat.SetWf(m_Value.GetW().Getf());
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeXML::PropertyAttributeXML() : m_Name("")
{
	m_Type = kTypeNone;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeStringXML::PropertyAttributeStringXML() : m_Value("")
{
	m_Type = kTypeString;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeIntXML::PropertyAttributeIntXML() : m_Value(0)													
{
	m_Type = kTypeInt;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeFloatXML::PropertyAttributeFloatXML() : m_Value(0)
{
	m_Type = kTypeFloat;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeBoolXML::PropertyAttributeBoolXML() : m_Value(0)
{
	m_Type = kTypeBool;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeVector3XML::PropertyAttributeVector3XML() : m_Value(0.0f,0.0f,0.0f)
{
	m_Type = kTypeVector3;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeQuaternionXML::PropertyAttributeQuaternionXML() : m_Value(0.0f,0.0f,0.0f,0.0f)
{
	m_Type = kTypeQuaternion;
}
//////////////////////////////////////////////////////////////////////////
bool PropertyAttributeFloatXML::operator==(const PropertyAttributeXML& other) const
{
	if(PropertyAttributeXML::operator==(other))
	{
		const PropertyAttributeFloatXML& otherFloat = static_cast<const PropertyAttributeFloatXML&>(other);
		return (m_Value == otherFloat.m_Value);
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
bool PropertyAttributeIntXML::operator==(const PropertyAttributeXML& other) const
{
	if(PropertyAttributeXML::operator==(other))
	{
		const PropertyAttributeIntXML& otherInt = static_cast<const PropertyAttributeIntXML&>(other);
		return (m_Value == otherInt.m_Value);
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
bool PropertyAttributeBoolXML::operator==(const PropertyAttributeXML& other) const
{
	if(PropertyAttributeXML::operator==(other))
	{
		const PropertyAttributeBoolXML& otherInt = static_cast<const PropertyAttributeBoolXML&>(other);
		return (m_Value == otherInt.m_Value);
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
bool PropertyAttributeVector3XML::operator==(const PropertyAttributeXML& other) const
{
	if(PropertyAttributeXML::operator==(other))
	{
		const PropertyAttributeVector3XML& otherInt = static_cast<const PropertyAttributeVector3XML&>(other);
		return IsTrueAll(m_Value == otherInt.m_Value);
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
bool PropertyAttributeQuaternionXML::operator==(const PropertyAttributeXML& other) const
{
	if(PropertyAttributeXML::operator==(other))
	{
		const PropertyAttributeQuaternionXML& otherInt = static_cast<const PropertyAttributeQuaternionXML&>(other);
		return IsTrueAll(m_Value == otherInt.m_Value);
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
bool PropertyAttributeStringXML::operator==(const PropertyAttributeXML& other) const
{
	if(PropertyAttributeXML::operator==(other))
	{
		const PropertyAttributeStringXML& otherInt = static_cast<const PropertyAttributeStringXML&>(other);
		return (m_Value == otherInt.m_Value);
	}
	return false;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeXML* PropertyAttributeXML::Clone()
{
	PropertyAttributeXML* attr = rage_new PropertyAttributeXML();
	attr->SetName(this->m_Name);
	return attr;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeXML* PropertyAttributeStringXML::Clone()
{
	PropertyAttributeStringXML* attr = rage_new PropertyAttributeStringXML();
	attr->SetName(this->m_Name);
	attr->GetString() = m_Value;
	return attr;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeXML* PropertyAttributeIntXML::Clone()
{
	PropertyAttributeIntXML* attr = rage_new PropertyAttributeIntXML();
	attr->SetName(this->m_Name);
	attr->GetInt() = m_Value;
	return attr;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeXML* PropertyAttributeBoolXML::Clone()
{
	PropertyAttributeBoolXML* attr = rage_new PropertyAttributeBoolXML();
	attr->SetName(this->m_Name);
	attr->GetBool() = m_Value;
	return attr;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeXML* PropertyAttributeFloatXML::Clone()
{
	PropertyAttributeFloatXML* attr = rage_new PropertyAttributeFloatXML();
	attr->SetName(this->m_Name);
	attr->GetFloat() = m_Value;
	return attr;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeXML* PropertyAttributeVector3XML::Clone()
{
	PropertyAttributeVector3XML* attr = rage_new PropertyAttributeVector3XML();
	attr->SetName(this->m_Name);
	attr->GetVector3() = m_Value;
	return attr;
}
//////////////////////////////////////////////////////////////////////////
PropertyAttributeXML* PropertyAttributeQuaternionXML::Clone()
{
	PropertyAttributeQuaternionXML* attr = rage_new PropertyAttributeQuaternionXML();
	attr->SetName(this->m_Name);
	attr->GetQuaternion() = m_Value;
	return attr;
}
//////////////////////////////////////////////////////////////////////////
ClipXML::ClipXML() : m_ClipType(0),
					m_Looped(false),
					m_Rate(1),
					m_StartTime(0),
					m_EndTime(0),
					m_AnimationName("")
{};
//////////////////////////////////////////////////////////////////////////
ClipXML::~ClipXML()
{
	atMap<u32, PropertyXML*>::Iterator iter = m_Properties.CreateIterator();
	for( iter.Start(); ! iter.AtEnd();  iter.Next() )
	{
		delete iter.GetData();
	}

	for(int i=0; i < m_Tags.GetCount(); ++i)
	{
		if(m_Tags[i] != NULL)
			delete m_Tags[i];
	}
}
//////////////////////////////////////////////////////////////////////////
void ClipXML::AddProperty(PropertyXML* prop)
{
	u32 key = prop->GetKey();

	RemoveProperty(key);

	m_Properties.Insert(key, prop);
}
//////////////////////////////////////////////////////////////////////////
bool ClipXML::HasProperty(u32 key)
{
	return FindProperty(key) ? true : false;
}
//////////////////////////////////////////////////////////////////////////
PropertyXML* ClipXML::FindProperty(u32 key)
{
	PropertyXML** prop = m_Properties.Access(key);
	return (prop)?*prop:NULL;
}
//////////////////////////////////////////////////////////////////////////
PropertyXML* ClipXML::FindProperty(const char* pName)
{
	return FindProperty(atHashString(pName));
}
//////////////////////////////////////////////////////////////////////////
void ClipXML::RemoveProperty(u32 key)
{
	PropertyXML* prop = FindProperty(key);
	if(prop)
	{
		delete prop;
		prop = NULL;
	}

	m_Properties.Delete(key);
}
//////////////////////////////////////////////////////////////////////////
void ClipXML::ClearProperties()
{
	atMap<u32, PropertyXML*>::Iterator iter = m_Properties.CreateIterator();
	for( iter.Start(); ! iter.AtEnd();  iter.Next() )
	{
		delete iter.GetData();
	}

	m_Properties.Reset();
}
//////////////////////////////////////////////////////////////////////////
bool ClipXML::HasProperties()
{
	return m_Properties.GetNumUsed() > 0;
}
//////////////////////////////////////////////////////////////////////////
u32 ClipXML::GetNumProperties()
{
	return u32(m_Properties.GetNumUsed());
}
//////////////////////////////////////////////////////////////////////////
u32 ClipXML::FindPropertyKeyByIndex(u32 idx)
{
	atMap<u32, PropertyXML*>::Iterator it = m_Properties.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
	return it.GetKey();
}
//////////////////////////////////////////////////////////////////////////
PropertyXML* ClipXML::FindPropertyByIndex(u32 idx)
{
	atMap<u32, PropertyXML*>::Iterator it = m_Properties.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
	return it.GetData();
}
//////////////////////////////////////////////////////////////////////////
float ClipXML::ConvertTimeToPhase(float time)
{
	if(m_EndTime > 0.f)
	{
		return time / m_EndTime;
	}
	return 0.f;
}
//////////////////////////////////////////////////////////////////////////
float ClipXML::ConvertPhaseToTime(float phase)
{
	return phase * m_EndTime;
}
//////////////////////////////////////////////////////////////////////////
void ClipXML::ClearTags()
{
	for(int i=0; i < m_Tags.GetCount(); ++i)
	{
		delete m_Tags[i];
	}

	m_Tags.Reset();
}
//////////////////////////////////////////////////////////////////////////
void ClipXML::AddUniqueTag(TagXML* tag)
{
	for(int i=0; i < m_Tags.GetCount(); ++i)
	{
		if(IsClose(m_Tags[i]->GetStart(), tag->GetStart()) && IsClose(m_Tags[i]->GetEnd(), tag->GetEnd()) && m_Tags[i]->GetProperty() == tag->GetProperty())
		{
			// found exact duplicate, so no need to add - just return
			return;
		}	
	}

	m_Tags.PushAndGrow(tag);
}
//////////////////////////////////////////////////////////////////////////
bool ClipXML::HasTags()
{
	return m_Tags.GetCount() > 0;
}
//////////////////////////////////////////////////////////////////////////
bool ClipXML::Load(const char* filename)
{
	if(ASSET.Exists(filename, ""))
	{
		if (PARSER.LoadObject(filename, "clipxml", *this))
		{
			return true;
		}
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////
bool ClipXML::Save(const char* filename)
{
	if (PARSER.SaveObject(filename, "clipxml", this))
	{
		return true;
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////
float ClipXML::GetDuration()
{
	return (m_EndTime-m_StartTime)/m_Rate;
}

};