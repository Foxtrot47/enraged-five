#include "util.h"

#define BLOCK_TAG_PAD 0.001f

rage::ClipXML* ClipXMLUtil::ConcatClipXMLs(rage::atArray<rage::ClipXML*>& clipList)
{
	rage::ClipXML* pOutputClip = rage_new rage::ClipXML();

	rage::atArray<float> timeList;
	timeList.Reserve( clipList.GetCount() );

	float totalDuration = 0;

	for ( int i = 0; i < clipList.GetCount(); ++i )
	{
		timeList.Append() = totalDuration;
		totalDuration += clipList[i]->GetEndTime();
	}

	pOutputClip->SetStart(0);
	pOutputClip->SetEnd(totalDuration);
	pOutputClip->SetAnimationName(clipList[0]->GetAnimationName());

	for ( int i = 0; i < clipList.GetCount(); ++i )
	{
		rage::ClipXML* srcClip = clipList[i];

		if( srcClip->HasProperties() )
		{
			for(int p=0; p<srcClip->GetNumProperties(); ++p)
			{
				bool match = false;
				for(int q=0; q<pOutputClip->GetNumProperties(); ++q)
				{
					if(srcClip->FindPropertyKeyByIndex(p) == pOutputClip->FindPropertyKeyByIndex(q))
					{
						match = true;
						break;
					}
				}

				if(!match)
				{
					pOutputClip->AddProperty(srcClip->FindPropertyByIndex(p));
				}
			}
		}
		
		if ( srcClip->HasTags() )
		{
			// transfer the tags, adjusting the phases for the new positions in the concatenated animation
			for ( int j = 0; j < srcClip->GetTags().GetCount(); ++j )
			{
				// clone the tag
				rage::TagXML* pTag =  srcClip->GetTags()[j]->Clone();

				// adjust the phase intervals
				float start = srcClip->ConvertPhaseToTime( pTag->GetStart() );
				start = pOutputClip->ConvertTimeToPhase( timeList[i] + start );

				float end = srcClip->ConvertPhaseToTime( pTag->GetEnd() );
				end = pOutputClip->ConvertTimeToPhase( timeList[i] + end );

				pTag->SetStart(start);
				pTag->SetEnd(end);

				// add the tag
				pOutputClip->GetTags().PushAndGrow(pTag);
			}
		}

		// add a block tag to cover the transition between concatenated anims
		if ( i > 0 )
		{
			float fHalfFrame = (0.5f * (1.0f / 30));
			float fTime = timeList[i] + fHalfFrame;

			rage::TagXML* pTag = rage_new rage::TagXML();
			pTag->GetProperty().SetName("Block");
			pTag->SetStart(pOutputClip->ConvertTimeToPhase( fTime - fHalfFrame - BLOCK_TAG_PAD ));
			pTag->SetEnd(pOutputClip->ConvertTimeToPhase( fTime + fHalfFrame + BLOCK_TAG_PAD ));

			rage::PropertyAttributeFloatXML* pAttr = rage_new rage::PropertyAttributeFloatXML();
			pAttr->SetName("division");
			pAttr->GetFloat() = 0.f;
			pTag->GetProperty().AddAttribute(pAttr);

			pOutputClip->GetTags().PushAndGrow(pTag);
		}
	}

	return pOutputClip;
}