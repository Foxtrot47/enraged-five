#ifndef CLIP_XML_H
#define CLIP_XML_H

#include "parser/macros.h"
#include "parser/manager.h"

#include "atl/array.h"
#include "atl/string.h"
#include "vectormath/vec4v.h"
#include "vector/quaternion.h"

//using namespace rage;
namespace rage
{

class PropertyAttributeXML
{
public:
	PropertyAttributeXML();
	virtual ~PropertyAttributeXML(){};
	void SetName(const char* name){ m_Name = name; };
	u32 GetKey() const { return atHashString(m_Name.c_str()); };
	virtual PropertyAttributeXML* Clone();
	atString& GetName(){ return m_Name; };
	s32 GetType() const { return m_Type; };

	virtual bool operator==(const PropertyAttributeXML&) const;

	// PURPOSE: Inequality comparison
	bool operator!=(const PropertyAttributeXML&) const;

	enum Types
	{
		kTypeNone,

		kTypeFloat,
		kTypeInt,
		kTypeBool,
		kTypeString,
		kTypeVector3,
		kTypeVector4,
		kTypeQuaternion,
		kTypeMatrix34,
		// RESERVED - for more Rage specific property attribute types only

		// this must follow the list of built in property attribute types
		kTypeNum,
	};

protected:
	atString m_Name;
	u32 m_Type;

	PAR_PARSABLE;
};
//////////////////////////////////////////////////////////////////////////
class PropertyAttributeStringXML : public PropertyAttributeXML
{
public:
	PropertyAttributeStringXML();
	virtual ~PropertyAttributeStringXML(){};
	atString& GetString(){ return m_Value; };
	PropertyAttributeXML* Clone();

	// PURPOSE: Equality comparison
	virtual bool operator==(const PropertyAttributeXML&) const;

private:
	atString m_Value;

	PAR_PARSABLE;
};
//////////////////////////////////////////////////////////////////////////
class PropertyAttributeBoolXML : public PropertyAttributeXML
{
public:
	PropertyAttributeBoolXML();
	virtual ~PropertyAttributeBoolXML(){};
	bool& GetBool(){ return m_Value; };
	PropertyAttributeXML* Clone();

	// PURPOSE: Equality comparison
	virtual bool operator==(const PropertyAttributeXML&) const;

private:
	bool m_Value;

	PAR_PARSABLE;
};
//////////////////////////////////////////////////////////////////////////
class PropertyAttributeIntXML : public PropertyAttributeXML
{
public:
	PropertyAttributeIntXML();
	virtual ~PropertyAttributeIntXML(){};
	int& GetInt(){ return m_Value; };
	PropertyAttributeXML* Clone();
	
	// PURPOSE: Equality comparison
	virtual bool operator==(const PropertyAttributeXML&) const;

private:
	int m_Value;

	PAR_PARSABLE;
};
//////////////////////////////////////////////////////////////////////////
class PropertyAttributeFloatXML : public PropertyAttributeXML
{
public:
	PropertyAttributeFloatXML();
	virtual ~PropertyAttributeFloatXML(){};
	float& GetFloat(){ return m_Value; };
	PropertyAttributeXML* Clone();

	// PURPOSE: Equality comparison
	virtual bool operator==(const PropertyAttributeXML&) const;

private:
	float m_Value;

	PAR_PARSABLE;
};
//////////////////////////////////////////////////////////////////////////
class PropertyAttributeQuaternionXML : public PropertyAttributeXML
{
public:
	PropertyAttributeQuaternionXML();
	virtual ~PropertyAttributeQuaternionXML(){};
	Vec4V_Ref GetQuaternion(){ return m_Value; };
	void GetQuaternion(QuatV& quat);
	void FromQuaternion(QuatV& quat);
	PropertyAttributeXML* Clone();

	// PURPOSE: Equality comparison
	virtual bool operator==(const PropertyAttributeXML&) const;

private:
	Vec4V m_Value;

	PAR_PARSABLE;
};
//////////////////////////////////////////////////////////////////////////
class PropertyAttributeVector3XML : public PropertyAttributeXML
{
public:
	PropertyAttributeVector3XML();
	virtual ~PropertyAttributeVector3XML(){};
	Vec3V_Ref GetVector3(){ return m_Value; };
	PropertyAttributeXML* Clone();

	// PURPOSE: Equality comparison
	virtual bool operator==(const PropertyAttributeXML&) const;

private:
	Vec3V m_Value;

	PAR_PARSABLE;
};
//////////////////////////////////////////////////////////////////////////
class PropertyXML
{
public:
	PropertyXML();
	virtual ~PropertyXML();
	void AddAttribute(PropertyAttributeXML* attr);
	void SetName(const char* name){ m_Name = name; };
	const char* GetName(){ return m_Name.c_str(); };
	u32 GetKey() const { return atStringHash(m_Name.c_str()); };
	s32 GetNumAttributes() const { return m_Attributes.GetCount(); };
	PropertyAttributeXML* GetAttributeByIndex(u32 idx);
	PropertyAttributeXML* GetAttribute(const char* name);

	PropertyXML* Clone();

	// PURPOSE: Equality comparison
	// NOTES: Compares keys, attributes (including their keys, types and values)
	bool operator==(const PropertyXML&) const;

private:
	atArray<PropertyAttributeXML*> m_Attributes;
	atString m_Name;

	PAR_PARSABLE;;
};
//////////////////////////////////////////////////////////////////////////
class TagXML
{
public:
	TagXML();
	TagXML(TagXML& tag);
	virtual ~TagXML();
	void SetStart(float start){ m_StartTime = start; };
	void SetEnd(float end){ m_EndTime = end; };
	PropertyXML& GetProperty(){ return m_Property; };

	float GetStart(){ return m_StartTime; };
	float GetEnd(){ return m_EndTime; };

	TagXML* Clone();

private:
	float m_StartTime;
	float m_EndTime;
	PropertyXML m_Property;

	PAR_PARSABLE;
};
//////////////////////////////////////////////////////////////////////////
class ClipXML
{
public:
	ClipXML();
	virtual ~ClipXML();

	bool Load(const char* filename);
	bool Save(const char* filename);

	void AddProperty(PropertyXML* prop);

	bool HasProperty(u32 key);
	PropertyXML* FindProperty(u32 key);
	PropertyXML* FindProperty(const char* pName);
	void RemoveProperty(u32 key);
	bool HasProperties();
	u32 GetNumProperties();
	u32 FindPropertyKeyByIndex(u32 idx);
	PropertyXML* FindPropertyByIndex(u32 idx);
	atMap<u32, PropertyXML*>& GetProperties(){ return m_Properties; };

	void SetStart(float startTime){ m_StartTime = startTime; };
	void SetEnd(float endTime){ m_EndTime = endTime; };
	void SetRate(float rate){ m_Rate = rate; };
	void SetLooped(bool looped){ m_Looped = looped; };
	void SetType(u32 clipType){ m_ClipType = clipType; };
	void SetAnimationName(const atString& name){ m_AnimationName = name; };

	float GetStartTime(){ return m_StartTime; };
	float GetEndTime(){ return m_EndTime; };
	float GetRate(){ return m_Rate; };
	float GetDuration();
	float IsLooped(){ return m_Looped; };
	u32 GetType(){ return m_ClipType; };
	atString GetAnimationName(){ return m_AnimationName; };

	float ConvertTimeToPhase(float time);
	float ConvertPhaseToTime(float phase);

	bool HasTags();
	atArray<TagXML*>& GetTags(){ return m_Tags; };
	void AddUniqueTag(TagXML* tag);

	void ClearTags();
	void ClearProperties();

private:
	
	// Variables
	u32 m_ClipType;
	bool m_Looped;
	atString m_AnimationName;
	float m_StartTime;
	float m_EndTime;
	float m_Rate;

	atMap<u32, PropertyXML*> m_Properties;
	atArray< TagXML* > m_Tags;

	PAR_PARSABLE;
};

}; // namespace rage

#endif