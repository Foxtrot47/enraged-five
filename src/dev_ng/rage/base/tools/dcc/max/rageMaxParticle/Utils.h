// RAGE headers
#include "parser/manager.h"
#include "parser/tree.h"
#include "EntityFx.h"
#include <vector>

using namespace std;
using namespace rage;

void ParseVectorData( parTreeNode* pElemDomain, float* pVectorData, const char *filename, float *firstValue=NULL);
void ParseVectorData( parTreeNode* pElemDomain, vector<float*>& vectorData, vector<float>& keyValues, const char *filename);
/**
 * @brief EntityFx Parser Exception Class
 *
 * This exception can be raised by most methods in the cEntityFx class to
 * indicate an error occurred (generally during parsing).
 */
class exEntityFxException : public std::exception
{
public:
	exEntityFxException( const std::string& sMessage );
	virtual ~exEntityFxException( );
};

// --- Globals ------------------------------------------------------------------


__forceinline void 
ThrowEntityFxException( const char* file, const char* sFormat, ... )
{
	va_list args;
	char sBuffer[1024];

	va_start( args, sFormat );
	vsprintf_s( sBuffer, 1024, sFormat, args );
	va_end( args );

	cEntityFx::ParserError(sBuffer);

	throw exEntityFxException( sBuffer );
}