//
// filename:	ParticleMouse.h
// description:	
//

#ifndef INC_PARTICLEMOUSE_H_
#define INC_PARTICLEMOUSE_H_

// --- Include Files ------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// Forward declaration
class cRageParticle;

class cRageParticleCreateCallBack : public CreateMouseCallBack
{
public:
	int proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat );
	void SetObj( cRageParticle *obj );

private:
	cRageParticle* ob;
	Point3 p0,p1;
	IPoint2 sp0, sp1;
	BOOL square;
};

#endif // !INC_PARTICLEMOUSE_H_
