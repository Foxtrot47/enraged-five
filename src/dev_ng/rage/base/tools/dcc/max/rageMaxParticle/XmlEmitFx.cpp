//
// filename:	XmlParticleEffect.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// Max headers
// #include <maxscript/maxscript.h>
// #include <maxscript/foundation/functions.h>
// #include <maxscript/macros/generic_class.h>
// #include <maxscript/maxwrapper/mxsobjects.h>
// 
// #include <maxscript\macros\local_class.h>
// #include <maxscript\macros\local_class_instantiations.h>
// #include "maxscript/util/IMXSDebugger.h"

// Local headers
#include "XmlEmitFx.h"
#include "EntityFx.h"
#include "Win32MsgBox.h"

// RAGE headers
#include "parser/manager.h"
#include "parser/tree.h"

#include "Utils.h"

#include <maxscript/foundation/3dmath.h>

// --- Constants ----------------------------------------------------------------


// --- Using Directives ---------------------------------------------------------

using namespace rage;

////namespace XmlEmitFx
////{
//
///* --------------------  local generics and names   --------------- */
//// #	include "listpro.h"
//// #	include "setpro.h"
//
////DEFINE_LOCAL_GENERIC_CLASS(cXmlEmitFx, cXmlEmitFxGeneric);
//
#include <maxscript\macros\local_class_instantiations.h>

def_name	( emName );
def_name	( emFilename );
def_name	( emType );
def_name	( emPos );
def_name	( emDir );
def_name	( emSize );
def_name	( emInnerSize );
def_name	( emZoom );
def_name	( emIsWorldSpace );
//
local_visible_class_instance ( cXmlEmitFx, "cXmlEmitFx" )

//// Value*
//// cXmlEmitFxClass::apply(Value** arg_list, int count,  CallContext* cc)
//// {
//// 	return_value(Value::apply(arg_list, count, cc));
//// }


// --- Code ---------------------------------------------------------------------


// cXmlEmitFx::~cXmlEmitFx( )
// {
// 
// }

// const std::string&	
// cXmlEmitFx::RetName( ) const
// {
// 	return ( m_sName );
// }
// 
// const std::string&	
// cXmlEmitFx::RetFilename( ) const
// {
// 	return ( m_sFilename );
// }
// 
// cXmlEmitFx::etDomainType		
// cXmlEmitFx::RetEmitterType( ) const
// {
// 	return ( m_eDomainType );
// }
// 
// const float*				
// cXmlEmitFx::RetPosition( ) const
// {
// 	return &m_vPosition[0];
// }
// 
// const float*				
// cXmlEmitFx::RetDirection( ) const
// {
// 	return &m_vDirection[0];
// }
// 
// const float*				
// cXmlEmitFx::RetSize( ) const
// {
// 	return &m_vSize[0];
// }

void				
cXmlEmitFx::ParseFile( const std::string& sFilename )
{
	m_sFilename = sFilename;

	parTree* pEmitXml = PARSER.LoadTree( m_sFilename.c_str(), "emitrule" );
	if ( pEmitXml == NULL )
		ThrowEntityFxException( m_sFilename.c_str(), "Error loading XML file %s.", m_sFilename.c_str() );

	parTreeNode* pElemRoot = pEmitXml->GetRoot();
	if ( 0 != strcmp( pElemRoot->GetElement().GetName(), sC_s_str_Emitter_Root ) )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) invalid root, not %s.", 
								m_sFilename.c_str(), sC_s_str_Emitter_Root );

	// Read <Name> Element
	parTreeNode* pElemName = pElemRoot->FindChildWithName( sC_s_str_Emitter_Name );
	//assert( pElemName );
	if ( !pElemName )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) looking for %s tag.", 
								m_sFilename.c_str(), sC_s_str_Emitter_Name );
	m_sName = std::string( pElemName->GetData() );

	// From <EmitInfo> Element read <EmitterDomain> node to find our emitter
	parTreeNode* pElemEmitterDomain = pElemRoot->FindChildWithName( sC_s_str_EmitterDomain );
	//assert( pElemEmitterDomain );
	if ( !pElemEmitterDomain )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) looking for %s tag.",
								m_sFilename.c_str(), sC_s_str_EmitterDomain );

	// From <EmitterDomain> Element read <Domain> and initialise our member data
	parTreeNode* pElemDomain = pElemEmitterDomain->FindChildWithName( sC_s_str_Domain );
	//assert( pElemDomain );
	if ( !pElemDomain )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) looking for %s tag.", 
								m_sFilename.c_str(), sC_s_str_Domain );
	
	parAttribute* pDomainTypeAttr = pElemDomain->GetElement().FindAttribute( sC_s_str_DomainType );
	//assert( pDomainTypeAttr );
	if ( !pDomainTypeAttr )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) domain type attribute not found.",
		m_sFilename.c_str() );
	const char* pAttrDomainType = pDomainTypeAttr->GetStringValue();
	//assert( pAttrDomainType );
	if ( !pAttrDomainType )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) domain type not specified.",
		m_sFilename.c_str() );

	// Set our type member data
	if ( 0 == strcmp( pAttrDomainType, sC_s_str_DomainTypeBox ) )
	{
		m_eDomainType = kDomainType_Box;
	}
	else if ( 0 == strcmp( pAttrDomainType, sC_s_str_DomainTypeCylinder ) )
	{
		m_eDomainType = kDomainType_Cylinder;
	}
	else if ( 0 == strcmp( pAttrDomainType, sC_s_str_DomainTypeSphere ) )
	{
		m_eDomainType = kDomainType_Sphere;
	}
	else
	{
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) unknown Emitter Domain Type (%s).", 
								m_sFilename.c_str(), pAttrDomainType );
	}

	// Position, Direction and Size can differ depending on Emitter Type
	if ( ( kDomainType_Box == RetEmitterType() ) || 
		 ( kDomainType_Cylinder == RetEmitterType() ) )
	{
		// Fetch our direction
		parTreeNode* pElemDirection = pElemDomain->FindChildWithName( sC_s_str_DomainDirection );
		//assert( pElemDirection );
		if ( !pElemDirection )
			ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) %s tag not found.", 
			m_sFilename.c_str(), sC_s_str_DomainDirection );
		parTreeNode* pElemKeyframe = pElemDirection->FindChildWithName( sC_s_str_DomainKeyFrame );
		if ( !pElemKeyframe )
			ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) %s tag not found.",
			m_sFilename.c_str(), sC_s_str_DomainKeyFrame );
		
		ParseVectorData( pElemKeyframe, &m_vDirection[0], m_sFilename.c_str() );
	}
	else if ( kDomainType_Sphere == RetEmitterType() )
	{
		// Nothing else required for Sphere's, Position and Size are
		// common to all emitters and handled below.
	}

	// Fetch our position
	parTreeNode* pElemPosition = pElemDomain->FindChildWithName( sC_s_str_DomainPosition );
	//assert( pElemPosition );
	if ( !pElemPosition )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) %s tag not found.", 
		m_sFilename.c_str(), sC_s_str_DomainPosition );
	parTreeNode* pElemKeyframe = pElemPosition->FindChildWithName( sC_s_str_DomainKeyFrame );
	//assert( pElemKeyframe );
	if ( !pElemKeyframe )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) %s tag not found.",
		m_sFilename.c_str(), sC_s_str_DomainKeyFrame );
	ParseVectorData( pElemKeyframe, &m_vPosition[0], m_sFilename.c_str() );

	// Fetch our position
	parTreeNode* pElemIsWorldSpace = pElemDomain->FindChildWithName( sC_s_str_DomainIsWorldSpace );
	//assert( pElemPosition );
	if ( !pElemIsWorldSpace )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) %s tag not found.", 
		m_sFilename.c_str(), sC_s_str_DomainIsWorldSpace );
	parAttribute *pWorldSpaceAttr = pElemIsWorldSpace->GetElement().FindAttribute("value");
	if ( !pElemKeyframe )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) %s tag attribute \"value\" not found.",
		m_sFilename.c_str(), sC_s_str_DomainIsWorldSpace );
	m_bIsWorldSpace = pWorldSpaceAttr->FindBoolValue();

	// Fetch our Size (same for all Emitter Types)
	parTreeNode* pElemSize = pElemDomain->FindChildWithName( sC_s_str_DomainSize );
	//assert( pElemSize );
	if ( !pElemSize )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) %s tag not found.", 
								m_sFilename.c_str(), sC_s_str_DomainSize );
	pElemKeyframe = pElemSize->FindChildWithName( sC_s_str_DomainKeyFrame );
	//assert( pElemKeyframe );
	if ( !pElemKeyframe )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) %s tag not found.",
		m_sFilename.c_str(), sC_s_str_DomainKeyFrame );
	ParseVectorData( pElemKeyframe, &m_vSize[0] , m_sFilename.c_str());

	parTreeNode* pInnerElemSize = pElemDomain->FindChildWithName( sC_s_str_InnerDomainSize );
	// Do not assert here since it doesn't have to be there
	if (pInnerElemSize )
	{
		pElemKeyframe = pInnerElemSize->FindChildWithName( sC_s_str_DomainKeyFrame );
		//assert( pElemKeyframe );
		if ( !pElemKeyframe )
			ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) %s tag not found.",
			m_sFilename.c_str(), sC_s_str_DomainKeyFrame );
		ParseVectorData( pElemKeyframe, &m_vInnerSize[0], m_sFilename.c_str());
	}

	// Cleanup
	delete pEmitXml;
}

//////////////////////////////////////////////////////////////////////////
// Max valule accessors
//////////////////////////////////////////////////////////////////////////
// 
// Value*
// cXmlEmitFx::set_name(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	m_sName = arg_list[0]->to_string();
// 	return &true_value;
// }
// Value*
// cXmlEmitFx::set_type(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	String t = arg_list[0]->to_string();
// 	switch(t)
// 	{
// 	case "Box":
// 		m_eDomainType = kDomainType_Box;
// 		break;
// 	case "Cylinder":
// 		m_eDomainType = kDomainType_Cylinder;
// 		break;
// 	case "Sphere":
// 		m_eDomainType = kDomainType_Sphere;
// 		break;
// 	default:
// 		m_eDomainType = kDomainType_Unknown;
// 		break;
// 	}
// 	return &true_value;
// }
// Value*
// cXmlEmitFx::set_pos(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	m_vPosition = arg_list[0]->to_point3();
// 	return &true_value;
// }
// Value*
// cXmlEmitFx::set_dir(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	m_vDirection = arg_list[0]->to_point3();
// 	return &true_value;
// }
// Value*
// cXmlEmitFx::set_size(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	m_vSize = arg_list[0]->to_point3();
// 	return &true_value;
// }
// //////////////////////////////////////////////////////////////////////////
// Value*
// cXmlEmitFx::get_name(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	return new String(m_sName.c_str());
// }
// Value*
// cXmlEmitFx::get_type(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	switch(m_eDomainType)
// 	{
// 	case kDomainType_Box:
// 		return new String("Box");
// 		break;
// 	case kDomainType_Cylinder:
// 		return new String("Cylinder");
// 		break;
// 	case kDomainType_Sphere:
// 		return new String("Sphere");
// 		break;
// 	case kDomainType_Unknown:
// 	default:
// 		return &undefined;
// 		break;
// 	}
// 	return &undefined;
// }
// Value*
// cXmlEmitFx::get_pos(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	return m_vPosition;
// }
// Value*
// cXmlEmitFx::get_dir(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	return m_vDirection;
// }
// Value*
// cXmlEmitFx::get_size(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	return m_vSize;
// }


void
cXmlEmitFx::sprin1( CharStream* s )
{
	s->printf( "cXmlEmitFx(\n" );
	s->printf( "\temName:	    \"%s\",\n", m_sName.c_str());
	const char* type = "undefined";
	switch(m_eDomainType)
	{
	case kDomainType_Box:
		type = "Box";
		break;
	case kDomainType_Cylinder:
		type = "Cylinder";
		break;
	case kDomainType_Sphere:
		type = "Sphere";
		break;
	default:
		break;
	}
	s->printf( "\tenType:	      \"%s\",\n", type);
	s->printf( "\temFilename:     \"%s\",\n", m_sFilename.c_str());
	s->printf( "\temIsWorldSpace: \"%s\",\n", m_bIsWorldSpace ? "true" : "false");
	s->printf( "\temPosition:      [%5.2f %5.2f %5.2f],\n", m_vPosition.x,m_vPosition.y,m_vPosition.z);
	s->printf( "\temDirection:     [%5.2f %5.2f %5.2f],\n", m_vDirection.x,m_vDirection.y,m_vDirection.z);
	s->printf( "\temSize:	       [%5.2f %5.2f %5.2f],\n", m_vSize.x,m_vSize.y,m_vSize.z);
	s->printf( "\temInnerSize:     [%5.2f %5.2f %5.2f],\n", m_vInnerSize.x,m_vInnerSize.y,m_vInnerSize.z);
	s->printf( "\temZoom(min/max): [%5.2f %5.2f],\n", m_vZoom.x,m_vZoom.y);
	s->printf( ")\n" );
}

Value*
cXmlEmitFx::get_property( Value** arg_list, int count )
{
	Value* prop = arg_list[0];

	if (prop == n_emName)
	{
		return_protected( new String( m_sName.c_str() ) );
	}
	else if (prop == n_emType)
	{
		switch(m_eDomainType)
		{
		case kDomainType_Box:
			return_protected( new String("Box"));
			break;
		case kDomainType_Cylinder:
			return_protected( new String("Cylinder"));
			break;
		case kDomainType_Sphere:
			return_protected( new String("Sphere") );
			break;
		case kDomainType_Unknown:
		default:
			return_protected( &undefined );
			break;
		}
		return &undefined;
	}
	else if (prop == n_emFilename)
	{
		return_protected( new String( m_sFilename.c_str() ) );
	}
	else if (prop == n_emPos)
	{
		return_protected( new Point3Value(m_vPosition) );
	}
	else if (prop == n_emDir)
	{
		return_protected( new Point3Value(m_vDirection) );
	}
	else if (prop == n_emSize)
	{
		return_protected( new Point3Value(m_vSize * 2) ); // SCALED UP BY 2!!!
	}
	else if (prop == n_emInnerSize)
	{
		return_protected( new Point3Value(m_vInnerSize * 2) ); // SCALED UP BY 2!!!
	}
	else if (prop == n_emZoom)
	{
		return( new Point3Value(m_vZoom) );
	}
	else if (prop == n_emIsWorldSpace)
	{
		return( m_bIsWorldSpace ? &true_value : &false_value );
	}
	return Value::get_property( arg_list, count );
}

Value*
cXmlEmitFx::set_property( Value** arg_list, int count )
{
	return Value::set_property(arg_list, count);
}
#include <maxscript/macros/local_implementations.h> // switch def

cXmlEmitFx::cXmlEmitFx( const char* sFilename )
: m_sFilename( sFilename )
, m_eDomainType( kDomainType_Unknown )
, m_vZoom( 1.0f, 1.0f, 1.0f )
, m_bIsWorldSpace(false)
{
	// 	memset( m_vPosition, 0, 3 * sizeof( float ) );
	// 	memset( m_vSize, 0, 3 * sizeof( float ) );
	// 	memset( m_vDirection, 0, 3 * sizeof( float ) );
	ParseFile( m_sFilename );

	def_name (emName);
	def_name (emFilename);
	def_name (emType);
	def_name (emPos);
	def_name (emDir);
	def_name (emSize);
	def_name (emInnerSize);
	def_name (emZoom);
	def_name (emIsWorldSpace);

	make_heap_permanent();
}

cXmlEmitFx::cXmlEmitFx( const std::string& sFilename )
: m_sFilename( sFilename )
, m_eDomainType( kDomainType_Unknown )
, m_vZoom( 1.0f, 1.0f, 1.0f )
, m_bIsWorldSpace(false)
{
	// 	memset( m_vPosition, 0, 3 * sizeof( float ) );
	// 	memset( m_vSize, 0, 3 * sizeof( float ) );
	// 	memset( m_vDirection, 0, 3 * sizeof( float ) );

	ParseFile( m_sFilename );

	def_name (emName);
	def_name (emFilename);
	def_name (emType);
	def_name (emPos);
	def_name (emDir);
	def_name (emSize);
	def_name (emInnerSize);
	def_name (emZoom);
	def_name (emIsWorldSpace);

	make_heap_permanent();
}


//} // End of XmlEmitFx namespace

// End of file