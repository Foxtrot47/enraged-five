#if 0
visible_class (cRsParticleInfo)

class cEffectInfo : public MAXWrapper
{
public:
	ScripterExport cRsParticleInfo();
	static ScripterExport Value* intern();

	BOOL		is_kind_of(ValueMetaClass* c) { return (c == class_tag(cRsParticleInfo)) ? 1 : MAXWrapper::is_kind_of(c); }
	void		collect() { delete this; }
	ScripterExport void		sprin1(CharStream* s);
	TCHAR*		class_name() { return _T("Filter"); }

	def_property	( name );
	def_property	( emitters );
};

class cRsParticleInfo : public MAXWrapper
{
public:
	ScripterExport cRsParticleInfo();
	static ScripterExport Value* intern();

	BOOL		is_kind_of(ValueMetaClass* c) { return (c == class_tag(cRsParticleInfo)) ? 1 : MAXWrapper::is_kind_of(c); }
	void		collect() { delete this; }
	ScripterExport void		sprin1(CharStream* s);
	TCHAR*		class_name() { return _T("Filter"); }

	def_property	( name );
	def_property	( type );
	def_property	( pos );
	def_property	( dir );
	def_property	( size );
};
#endif