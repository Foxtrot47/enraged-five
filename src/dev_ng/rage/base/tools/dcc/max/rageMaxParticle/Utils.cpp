#include "Utils.h"
#include "XmlEmitFx.h"

using namespace std;

void ParseVectorData( parTreeNode* pElemDomain, float* pVectorData, const char *filename, float *firstValue)
{
	parTreeNode* pNodeKFData = pElemDomain->FindChildWithName( sC_s_str_DomainData );
	//assert( pNodeKFData );
	if ( !pNodeKFData )
		ThrowEntityFxException( filename, "Error parsing XML file (%s) %s tag not found.",
		filename, sC_s_str_DomainData );

	parTreeNode* pNodeNumKeyEntries = pNodeKFData->FindChildWithName( sC_s_str_DomainNumKeyEntries );
	//assert( pNodeKFData );
	if ( !pNodeNumKeyEntries )
		ThrowEntityFxException( filename, "Error parsing XML file (%s) %s tag not found.", filename, sC_s_str_DomainNumKeyEntries );
	int numEntries = pNodeNumKeyEntries->GetElement().FindAttribute("value")->FindIntValue(); 

	if(numEntries<=0)
		return;

	parTreeNode* pNodeData = pNodeKFData->FindChildWithName( sC_s_str_DomainKeyData );
	//assert( pNodeData );
	if ( !pNodeData )
		ThrowEntityFxException( filename, "Error parsing XML file (%s) %s tag not found.",
		filename, sC_s_str_DomainKeyData );

	float* pData = (float*)pNodeData->GetData();
	//assert( 4 <= pNodeData->GetDataSize() );
	if ( 4 > pNodeData->GetDataSize() )
	{
		ThrowEntityFxException( filename, "Error parsing XML file (%s) vector data format error (%s: %s).", 
			filename, pElemDomain->GetElement().GetName(), pNodeData->GetElement().GetName() );
	}
	for ( int i = 1; i < 4; ++i )
		pVectorData[i-1] = pData[i];

	if(firstValue)
		*firstValue = pData[0];
}

void ParseVectorData( parTreeNode* pElemDomain, vector<float*>& vectorData, vector<float>& keyValues, const char *filename)
{
	parTreeNode* pNodeKFData = pElemDomain->FindChildWithName( sC_s_str_DomainData );
	//assert( pNodeKFData );
	if ( !pNodeKFData )
		ThrowEntityFxException( filename, "Error parsing XML file (%s) %s tag not found.",
		filename, sC_s_str_DomainData );

	parTreeNode* pNodeNumKeyEntries = pNodeKFData->FindChildWithName( sC_s_str_DomainNumKeyEntries );
	//assert( pNodeKFData );
	if ( !pNodeNumKeyEntries )
		ThrowEntityFxException( filename, "Error parsing XML file (%s) %s tag not found.", filename, sC_s_str_DomainNumKeyEntries );
	int numEntries = pNodeNumKeyEntries->GetElement().FindAttribute("value")->FindIntValue(); 

	if(numEntries<=0)
		return;

	parTreeNode* pNodeData = pNodeKFData->FindChildWithName( sC_s_str_DomainKeyData );
	//assert( pNodeData );
	if ( !pNodeData )
		ThrowEntityFxException( filename, "Error parsing XML file (%s) %s tag not found.",
		filename, sC_s_str_DomainKeyData );
	if ( 4 > pNodeData->GetDataSize() )
	{
		ThrowEntityFxException( filename, "Error parsing XML file (%s) vector data format error (%s: %s).", 
			filename, pElemDomain->GetElement().GetName(), pNodeData->GetElement().GetName() );
	}

	float* pData = (float*)pNodeData->GetData();
	for(int rowI=0;rowI<numEntries;rowI++) 
	{
		keyValues.push_back(*pData);
		pData++;
		vectorData.push_back(pData);
		pData+=4;
	}
}
