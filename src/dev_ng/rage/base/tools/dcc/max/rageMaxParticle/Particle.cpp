//
// filename:	Particle.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// Local headers
#include "Particle.h"
#include "ParticleMouse.h"
#include "EntityFx.h"
#include "XmlEffectFx.h"
#include "XmlEmitFx.h"
#include "Win32MsgBox.h"
#include "Win32Res.h"

// MaxDataStore headers
//#include "IMaxDataStore.h"
#include "IFpMaxDataStore.h"

// --- Globals ------------------------------------------------------------------

extern cEntityFx* g_pEntityFx; 
extern ClassDesc* GetMaxDataMgrDesc();

// --- Static Globals -----------------------------------------------------------

static cRageParticleCreateCallBack RageParticleCreateCB;

// --- Code ---------------------------------------------------------------------

namespace
{
	const int sC_s_cnt_MaxNumberOfEmitters = 10;
}

// ------------------------------------------------------------------------------
// --- Public Methods -----------------------------------------------------------
// ------------------------------------------------------------------------------

cRageParticle::cRageParticle( )
	: m_bDirty( true )
	, m_fScale( 0.0f )
{
	assert( g_pEntityFx );
	
	m_vpTriObjects.resize( sC_s_cnt_MaxNumberOfEmitters );
	ClearMeshes( );

	memset( m_sName, 0, 513 * sizeof(TCHAR) );
	strcpy( m_cClassName, GetStringResource( g_hInstance, IDS_RAGEPARTICLE_CLASSNAME) );
}

cRageParticle::~cRageParticle( )
{
}

void
cRageParticle::SetDirty( )
{
	m_bDirty = true;
}

ObjectState 
cRageParticle::Eval( TimeValue t )
{
	return ObjectState(this);
}

int				
cRageParticle::NumRefs( ) 
{ 
	return ( 1 + sC_s_cnt_MaxNumberOfEmitters ); 
}

RefTargetHandle 
cRageParticle::GetReference( int nIndex )
{
	assert( nIndex < NumRefs() );
	assert( m_vpTriObjects.size() == (NumRefs() - 1) );

	switch( nIndex )
	{
	case RAGEPARTICLE_PBLOCK_REF:
		return NULL;
	case RAGEPARTICLE_MESH0_REF:
	default:	
		return m_vpTriObjects[ nIndex-1 ];
	}

	return NULL;
}

void 
cRageParticle::SetReference( int nIndex, RefTargetHandle rtarg )
{
	assert( nIndex < NumRefs() );
	assert( m_vpTriObjects.size() == (NumRefs() - 1) );

	switch( nIndex )
	{
	case RAGEPARTICLE_PBLOCK_REF:
		break;
	case RAGEPARTICLE_MESH0_REF:
	default:
		m_vpTriObjects[ nIndex-1 ] = static_cast<TriObject*>( rtarg );
		break;
	}
}

RefResult 
cRageParticle::NotifyRefChanged( Interval changeInt, RefTargetHandle hTarget, 
							    PartID& partID, RefMessage message )
{
	return REF_SUCCEED;
}

void 
cRageParticle::BeginEditParams( IObjParam *ip, ULONG flags, Animatable *prev )
{
	HelperObject::BeginEditParams(ip,flags,prev);
}

void 
cRageParticle::EndEditParams( IObjParam *ip, ULONG flags, Animatable *next )
{
	HelperObject::EndEditParams(ip, flags, next);
}

TCHAR*
cRageParticle::GetObjectName( ) 
{ 
	return m_cClassName; 
}

void 
cRageParticle::GetWorldBoundBox(TimeValue t, INode* pNode, ViewExp* vp, Box3& box )
{
	Matrix3 local2world = pNode->GetObjectTM(t);
	Box3 localBox;

	GetLocalBoundBox( t, pNode, vp, localBox );
	box = localBox * local2world;
}

void 
cRageParticle::GetLocalBoundBox(TimeValue t, INode* pNode, ViewExp* vp, Box3& box )
{
	if ( sC_s_cnt_MaxNumberOfEmitters == m_vpTriObjects.size() )
	{
		for ( int nMesh = 0; nMesh < sC_s_cnt_MaxNumberOfEmitters; ++nMesh )
		{
			Box3 subbox;
			m_vpTriObjects.at( nMesh )->GetLocalBoundBox( t, pNode, vp, subbox );
			box += subbox;
		}
	}
	else
	{
		box.Init( );
	}
}

int 
cRageParticle::Display( TimeValue t, INode* pNode, ViewExp *vpt, int flags )
{
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 mat = pNode->GetObjectTM(t);
	mat.Orthogonalize();
	mat.NoScale();
	gw->setTransform(mat);

	DrawMeshes( gw, t, mat, pNode, vpt );

	return 0;
}

int 
cRageParticle::HitTest( TimeValue t, INode* pNode, int type, int crossing, 
					   int flags, IPoint2* p, ViewExp* vpt )
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 objMat = pNode->GetObjectTM(t);
	objMat.Orthogonalize();
	objMat.NoScale();
	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(objMat);
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();

	int nResult = 0;
	for ( int nTriObj = 0; nTriObj < sC_s_cnt_MaxNumberOfEmitters; ++nTriObj )
	{
		int nSubObjHitTest = m_vpTriObjects[ nTriObj ]->HitTest( t, pNode, type, crossing, flags, p, vpt );
		nResult |= nSubObjHitTest;	
	}

	nResult |= gw->checkHitCode();
	return nResult;
}

void 
GetMat(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm) 
{
	tm = inode->GetObjectTM(t);
	tm.NoScale();
	float scaleFactor = vpt->NonScalingObjectSize() * 
						vpt->GetVPWorldWidth(tm.GetTrans()) / (float)360.0;
	tm.Scale(Point3(scaleFactor,scaleFactor,scaleFactor));
}

CreateMouseCallBack* 
cRageParticle::GetCreateMouseCallBack( )
{
	RageParticleCreateCB.SetObj(this);
	return (&RageParticleCreateCB);
}

RefTargetHandle 
cRageParticle::Clone( RemapDir& remap )
{
	cRageParticle* newob = new cRageParticle( );
	BaseClone( this, newob, remap );

	return( newob );
}

// ------------------------------------------------------------------------------
// --- Private Methods ----------------------------------------------------------
// ------------------------------------------------------------------------------

void 
cRageParticle::DrawMeshes( GraphicsWindow *gw, TimeValue t, Matrix3& objMat, 
						  INode* pNode, ViewExp* vpt )
{
	BuildMeshes( pNode );
	Matrix3 m;

	DrawLineProc lineProc(gw);
	Point3 pt[4];
	Point3 normal;
	Matrix3 invCamMat;
	float camMat[4][4];
	int persp;
	float hither, yon;

	gw->getCameraMatrix(camMat, &invCamMat, &persp, &hither, &yon);

	invCamMat.NoTrans();
	invCamMat.NoScale();
	objMat.NoTrans();
	objMat.NoScale();

	invCamMat = objMat * Inverse(invCamMat);

	if ( pNode->Selected() )
	{
		lineProc.SetLineColor( 1.0f, 1.0f, 1.0f );
	}

	for ( int nMesh = 0; nMesh < sC_s_cnt_MaxNumberOfEmitters; ++nMesh )
	{
		Mesh& mesh = m_vpTriObjects[ nMesh ]->GetMesh( );

		// get face normal array built if not already built
		mesh.checkNormals(TRUE);

		for ( int i = 0; i < mesh.numFaces; ++i )
		{
			normal = mesh.getFaceNormal(i);
			if((normal * invCamMat)[2] < 0)
				continue;

			pt[0] = mesh.verts[mesh.faces[i].v[0]];
			pt[1] = mesh.verts[mesh.faces[i].v[1]];
			pt[2] = mesh.verts[mesh.faces[i].v[2]];
			pt[3] = mesh.verts[mesh.faces[i].v[0]];
			lineProc.proc(pt,4);
		}
	}
}

void
cRageParticle::ClearMeshes( )
{
	// Re-construct our TriObjects and initialize attributes
	for ( int nMesh = 0; nMesh < sC_s_cnt_MaxNumberOfEmitters; ++nMesh )
	{
		SetReference( RAGEPARTICLE_MESH0_REF + nMesh, NULL );
		TriObject* pNewTri = CreateNewTriObject( );
		ReplaceReference( RAGEPARTICLE_MESH0_REF + nMesh, pNewTri );
	}
}

void
cRageParticle::BuildMeshes( INode* pNode )
{
// 	using namespace XmlEffectFx;
// 	using namespace XmlEmitFx;
	assert( pNode );
	
	IFpMaxDataStore* pMaxDataStore = GetMaxDataStoreInterface();
	if ( pMaxDataStore == NULL || pMaxDataStore->GetNumAttr(pMaxDataStore->GetAttrClass(pNode)) <= 0 )
	{
		// If new object attributes might not be set yet
		m_vpTriObjects[ 0 ] = BuildDefault( );
		m_bDirty = false;
		NotifyDependents( FOREVER, PART_ALL, REFMSG_CHANGE );
		return;
	}

	TSTR attrName = pMaxDataStore->GetAttrStr( pNode, 0 );
	float fAttrScale = pMaxDataStore->GetAttrFloat( pNode, 4 );

	if ( ( ( ( ( fAttrScale - 0.0001f ) < m_fScale ) &&
		      ( ( fAttrScale + 0.0001f ) > m_fScale ) ) ) 
			  &&
		  ( 0 == strcmp( m_sName, attrName.data() ) ) &&
		  !m_bDirty )
		return;

	ClearMeshes( );
	m_fScale = fAttrScale;
	strncpy( m_sName, attrName.data(), 512 );

	cXmlEffectFx* pEffect = g_pEntityFx->FindEffect( m_sName );
	if ( !pEffect )
	{
		m_vpTriObjects[ 0 ] = BuildDefault( );
	}
	else
	{
		int nMesh = 0;

		for ( cXmlEffectFx::itContainer itEmitter = pEffect->EmitRuleBegin();
			itEmitter != pEffect->EmitRuleEnd();
			++itEmitter, ++nMesh )
		{
			cXmlEmitFx* pEmitter = (*itEmitter);
			const float* pPosition = pEmitter->RetPosition();
			const float* pDirection = pEmitter->RetDirection();
			const float* pSize = pEmitter->RetSize();
			Point3 ptPosition( pPosition[0], pPosition[1], pPosition[2] );
			Point3 ptDirection( pDirection[0], pDirection[1], pDirection[2] );
			Point3 ptSize( pSize[0], pSize[1], pSize[2] );

			// Scale size
			ptSize *= m_fScale;
			CheckMinSize( ptSize );

			switch ( (*itEmitter)->RetEmitterType() )
			{
			case cXmlEmitFx::kDomainType_Box:
				m_vpTriObjects[ nMesh ] = BuildBox( ptPosition, ptSize, ptDirection );
				break;
			case cXmlEmitFx::kDomainType_Cylinder:
				m_vpTriObjects[ nMesh ] = BuildCylinder( ptPosition, ptSize, ptDirection );
				break;
			case cXmlEmitFx::kDomainType_Sphere:
				m_vpTriObjects[ nMesh ] = BuildSphere( ptPosition, ptSize );
				break;
			case cXmlEmitFx::kDomainType_Unknown:
				m_vpTriObjects[ nMesh ] = BuildDefault( );
				break;
			}
		}
	}

	m_bDirty = false;

	// Notify system our mesh has changed
	//NotifyDependents( FOREVER, PART_ALL, REFMSG_CHANGE );
}

/**
* @brief Configure mesh as default Standard Helper mesh
* @param mesh
*/
TriObject*				
cRageParticle::BuildDefault( )
{
	return BuildBox( Point3( 0.0f, 0.0f, 0.0f ), 
					 Point3( 1.0f, 1.0f, 0.1f ), 
					 Point3( 0.0f, 0.0f, 0.0f ) );
}

/**
 * @brief Create box
 * @param ptPosition
 * @param ptSize
 * @param ptDirection
 * @param iVertOffset
 * @param iFaceOffset
 */
TriObject*
cRageParticle::BuildBox( Point3& ptPosition, Point3& ptSize, Point3& ptDirection )
{
	TriObject* pObject = static_cast<TriObject*>( 
		CreateInstance( GEOMOBJECT_CLASS_ID, Class_ID(BOXOBJ_CLASS_ID, 0) ) );
	assert( pObject );

	// Fix up procedural parameters
	IParamArray* pBoxParams = pObject->GetParamBlock();
	assert( pBoxParams );

	int nHeight = pObject->GetParamBlockIndex( BOXOBJ_HEIGHT );
	assert( nHeight >= 0 );
	pBoxParams->SetValue( nHeight, static_cast<TimeValue>( 0 ), ptSize.z );

	int nWidth = pObject->GetParamBlockIndex( BOXOBJ_WIDTH );
	assert( nWidth >= 0 );
	pBoxParams->SetValue( nWidth, static_cast<TimeValue>( 0 ), ptSize.x );

	int nLength = pObject->GetParamBlockIndex( BOXOBJ_LENGTH );
	assert( nLength >= 0 );
	pBoxParams->SetValue( nLength, static_cast<TimeValue>( 0 ), ptSize.y );

	// Convert to regular mesh so we can rotate/position it
	TriObject* pTriObject = 
		(TriObject*)pObject->ConvertToType( static_cast<TimeValue>( 0 ), Class_ID( TRIOBJ_CLASS_ID, 0 ) );

	// Transform verts (convert between Rage and 3ds Max coordinate systems)
	Matrix3 matTransform;
	matTransform.IdentityMatrix( );
	matTransform.RotateX( ptDirection.x * DEG_TO_RAD );
	matTransform.RotateY( ptDirection.y * DEG_TO_RAD );
	matTransform.RotateZ( ptDirection.z * DEG_TO_RAD );
	matTransform.Translate( Point3( 0.0f, 0.0f, -ptSize.z / 2.0f ) );
	matTransform.Translate( ptPosition );

	Mesh& mesh = pTriObject->GetMesh( );
	for ( int nVert = 0; nVert < mesh.getNumVerts(); ++nVert )
	{
		Point3& v = mesh.getVert( nVert );
		v = v * matTransform;
	}

	return pTriObject;
}

/**
 * @brief Configure cylinder
 * @param ptPosition
 * @param ptSize
 * @param ptDirection
 */
TriObject*		 
cRageParticle::BuildCylinder( Point3& ptPosition, Point3& ptSize, Point3& ptDirection )
{
	TriObject* pObject = static_cast<TriObject*>( 
		CreateInstance( GEOMOBJECT_CLASS_ID, Class_ID(CYLINDER_CLASS_ID, 0) ) );
	assert( pObject );

	// Fix up parameters
	IParamArray* pSphParams = pObject->GetParamBlock();
	assert( pSphParams );

	int nRadius = pObject->GetParamBlockIndex( CYLINDER_RADIUS );
	assert( nRadius >= 0 );

	// Determine scaling factor.  A cylinder can have x and y sizes to 'skew' 
	// the cylinder, making it an oval prism rather than a cylinder.
	float radius = 0.0f;
	Point3 scale(1.0f, 1.0f, 1.0f);
	if (ptSize.x > ptSize.y)
	{
		radius = ptSize.x;
		scale.y = ptSize.y / ptSize.x;
	}
	else
	{
		radius = ptSize.y;
		scale.x = ptSize.x / ptSize.y;
	}
	pSphParams->SetValue( nRadius, static_cast<TimeValue>( 0 ), radius );

	int nHeight = pObject->GetParamBlockIndex( CYLINDER_HEIGHT );
	assert( nHeight >= 0 );
	pSphParams->SetValue( nHeight, static_cast<TimeValue>( 0 ), ptSize.z );

	int nSides = pObject->GetParamBlockIndex( CYLINDER_SIDES );
	assert( nSides >= 0 );
	pSphParams->SetValue( nSides, static_cast<TimeValue>( 0 ), 16 );

	int nSegs = pObject->GetParamBlockIndex( CYLINDER_SEGMENTS );
	assert( nSegs >= 0 );
	pSphParams->SetValue( nSegs, static_cast<TimeValue>( 0 ), 1 );

	// Convert to regular mesh so we can rotate/position it
	TriObject* pTriObject = 
		(TriObject*)pObject->ConvertToType( static_cast<TimeValue>( 0 ), Class_ID( TRIOBJ_CLASS_ID, 0 ) );

	// Transform verts (convert between Rage and 3ds Max coordinate systems)
	Matrix3 matTransform;
	matTransform.IdentityMatrix( );
	matTransform.Scale( scale );
	matTransform.RotateX( -90.0f * DEG_TO_RAD ); // VFX constructs cylinder "lying down"
	matTransform.RotateX( ptDirection.x * DEG_TO_RAD );
	matTransform.RotateY( ptDirection.y * DEG_TO_RAD );
	matTransform.RotateZ( ptDirection.z * DEG_TO_RAD );
	matTransform.Translate( ptPosition );

	Mesh& mesh = pTriObject->GetMesh( );
	for ( int nVert = 0; nVert < mesh.getNumVerts(); ++nVert )
	{
		Point3& v = mesh.getVert( nVert );
		v = v * matTransform;
		mesh.setVert( nVert, v );
	}

	return pTriObject;
}

/**
 * @brief Configure mesh as a box
 * @param mesh 
 * @param ptPosition
 * @param ptSize
 * @param ptDirection
 * @param iVertOffset
 * @param iFaceOffset
 */
TriObject*		 
cRageParticle::BuildSphere( Point3& ptPosition, Point3& ptSize )
{
	TriObject* pObject = static_cast<TriObject*>( 
			CreateInstance( GEOMOBJECT_CLASS_ID, Class_ID(SPHERE_CLASS_ID, 0) ) );
	assert( pObject );

	// Fix up parameters
	IParamArray* pSphParams = pObject->GetParamBlock();
	assert( pSphParams );

	int nRadius = pObject->GetParamBlockIndex( SPHERE_RADIUS );
	assert( nRadius >= 0 );
	pSphParams->SetValue( nRadius, static_cast<TimeValue>( 0 ), ptSize.x );

	int nSegs = pObject->GetParamBlockIndex( SPHERE_SEGS );
	assert( nSegs >= 0 );
	pSphParams->SetValue( nSegs, static_cast<TimeValue>( 0 ), 16 );

	// Convert to regular mesh so we can rotate/position it
	TriObject* pTriObject = static_cast<TriObject*>( 
		pObject->ConvertToType( static_cast<TimeValue>( 0 ), Class_ID( TRIOBJ_CLASS_ID, 0 ) ) );

	// Transform verts (convert between Rage and 3ds Max coordinate systems)
	Matrix3 matTransform;
	matTransform.IdentityMatrix( );
	matTransform.Translate( ptPosition );

	Mesh& mesh = pTriObject->GetMesh( );
	for ( int nVert = 0; nVert < mesh.getNumVerts(); ++nVert )
	{
		Point3& v = mesh.getVert( nVert );
		v = v * matTransform;
	}

	return pTriObject;
}

/**
 * @brief Check minimum size
 *
 * For point effect emitters we want to ensure they are visible so lets make 
 * 1cm as the minimum size.
 */
void		
cRageParticle::CheckMinSize( Point3& ptSize )
{
	const float fEpsilon = 0.0001f;

	if ( ( -fEpsilon < ptSize.x ) && ( fEpsilon > ptSize.x ) )
		ptSize.x = 0.01f;
	if ( ( -fEpsilon < ptSize.y ) && ( fEpsilon > ptSize.y ) )
		ptSize.y = 0.01f;
	if ( ( -fEpsilon < ptSize.z ) && ( fEpsilon > ptSize.z ) )
		ptSize.z = 0.01f;
}

// End of file
