//
// filename:	ParticleMouse.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// Local headers
#include "ParticleMouse.h"
#include "Particle.h"

// --- Code ---------------------------------------------------------------------

int 
cRageParticleCreateCallBack::proc( ViewExp *vpt, int msg, int point, int flags, IPoint2 m, Matrix3& mat )
{
	Point3 d;
	if(msg == MOUSE_FREEMOVE)
	{
		vpt->SnapPreview(m,m,NULL, SNAP_IN_3D);
	}
	else if(msg==MOUSE_POINT||msg==MOUSE_MOVE)
	{
		switch(point)
		{
		case 0:
			sp0 = m;
			p0 = vpt->SnapPoint(m,m,NULL,SNAP_IN_3D);
			p1 = p0 + Point3(.01,.01,.01);
			mat.SetTrans(float(.5)*(p0+p1));

			{
				Point3 xyz = mat.GetTrans();
				xyz.z = p0.z;
				mat.SetTrans(xyz);
			}

			if(msg==MOUSE_POINT)
			{
				return CREATE_STOP;
			}

			break;
		}
	}
	else if (msg == MOUSE_ABORT)
	{
		return CREATE_ABORT;
	}

	return TRUE;
}

void 
cRageParticleCreateCallBack::SetObj( cRageParticle *obj ) 
{ 
	ob = obj; 
}

// End of file
