//
// filename:	DllEntry.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// RAGE headers
#include "system/param.h"
#include "parser/manager.h"

// Local headers
#include "Particle.h"
#include "EntityFx.h"
#include "Configuration.h"
#include "Win32MsgBox.h"
#include "Win32Res.h"

// --- Structure/Class Definitions ----------------------------------------------

/**
 *
 */
class cRageParticleClassDesc : public ClassDesc2
{
public:
	int 			IsPublic( );
	void*			Create(BOOL loading = FALSE);
	const TCHAR*	ClassName( );
	SClass_ID		SuperClassID( );
	Class_ID		ClassID( );
	const TCHAR* 	Category( );
	const TCHAR*	InternalName( );
	HINSTANCE		HInstance( );
};

// --- Globals ------------------------------------------------------------------

HINSTANCE	g_hInstance;
int			g_bControlsInit = FALSE;
cEntityFx*	g_pEntityFx = NULL;

// --- Static Globals -----------------------------------------------------------

static cRageParticleClassDesc rageParticleDesc;

// --- Code ---------------------------------------------------------------------

int 			
cRageParticleClassDesc::IsPublic( ) 
{ 
	return FALSE; 
}

void*			
cRageParticleClassDesc::Create( BOOL loading ) 
{ 
	return new cRageParticle; 
}

const TCHAR*	
cRageParticleClassDesc::ClassName( ) 
{ 
	return GetStringResource(g_hInstance, IDS_RAGEPARTICLE_CLASSNAME); 
}

SClass_ID		
cRageParticleClassDesc::SuperClassID( ) 
{ 
	return HELPER_CLASS_ID; 
}

Class_ID		
cRageParticleClassDesc::ClassID( ) 
{ 
	return RAGEPARTICLE_CLASS_ID; 
}

const TCHAR* 	
cRageParticleClassDesc::Category( ) 
{ 
	return GetStringResource(g_hInstance, IDS_RAGEPARTICLE_CATEGORY); 
}

const TCHAR*	
cRageParticleClassDesc::InternalName( ) 
{ 
	return GetStringResource(g_hInstance, IDS_RAGEPARTICLE_CLASSNAME); 
}

HINSTANCE		
cRageParticleClassDesc::HInstance( ) 
{ 
	return g_hInstance; 
}

// --- DLL Entry Point Functions ------------------------------------------------

BOOL WINAPI 
DllMain( HINSTANCE hinstDLL, ULONG fdwReason, LPVOID lpvReserved )
{
	g_hInstance = hinstDLL;

	if ( !g_bControlsInit ) 
	{
		g_bControlsInit = TRUE;
		
#if MAX_VERSION_MAJOR < 11 
		InitCustomControls(g_hInstance);	// Initialize MAX's custom controls
#endif //MAX_VERSION_MAJOR

		InitCommonControls( );

		// Initialize RAGE
		char* argv[1];
		argv[0] = "rageMaxParticle.dlo";
		rage::sysParam::Init( 1, argv );

		// Initialize the RAGE parser
		INIT_PARSER;
	}
			
	return ( TRUE );
}

RAGEPARTICLE_API int
LibInitialize( void )
{
	int nResult = 1; // Assume successful

	try
	{
		Config::Init();
		g_pEntityFx = new cEntityFx( Config::GetEntityFxFile(), true );
		assert( g_pEntityFx );
	}
	catch ( ... )
	{
		DisplayError( GetStringResource(g_hInstance, IDS_RAGEPARTICLE_LIBDESCRIPTION),
					  "Unknown exception loading plugin.  rageMaxParticle plugin disabled." );
		nResult = 0;
	}
	
	return nResult;
}

RAGEPARTICLE_API int
LibShutdown( void )
{	
	int nResult = 1; // Assume successful

	try
	{
		if ( g_pEntityFx )
		{
			delete ( g_pEntityFx );
			g_pEntityFx = NULL;
		}
	}
	catch (std::exception& )
	{
		MessageBox( 0, GetStringResource(g_hInstance, IDS_RAGEPARTICLE_LIBDESCRIPTION),
					"Error finalising rageMaxParticle plugin.", MB_OK );
		nResult = 1;
	}

	return nResult;
}

RAGEPARTICLE_API const TCHAR* 
LibDescription( void )
{
	return GetStringResource( g_hInstance, IDS_RAGEPARTICLE_LIBDESCRIPTION );
}

RAGEPARTICLE_API int 
LibNumberClasses( )
{
	return 1;
}

RAGEPARTICLE_API ClassDesc* 
LibClassDesc( int nIndex )
{
	switch( nIndex ) 
	{
		case 0: 
			return &rageParticleDesc;
		default: 
			return 0;
	}
}

RAGEPARTICLE_API ULONG 
LibVersion( )
{
	return ( VERSION_3DSMAX );
}

// End of file
