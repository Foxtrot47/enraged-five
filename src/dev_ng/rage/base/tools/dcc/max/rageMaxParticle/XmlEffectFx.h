//
// filename:	XmlEffectFx.h
// description:	Defines class representing a partial Rage Effect .effectrule file
//				Uses the .effectrule file to determine the emitrule files in 
//				order to read the Effect Domain.
//
//				As the .effectrule file is XML we use TinyXML.
//

#ifndef INC_XMLEFFECTFX_H_
#define INC_XMLEFFECTFX_H_

#define EFFECT_NAMEPSPACE_EFFECT

// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>
#include <vector>

#include <maxscript\maxscript.h>
#include <maxscript/foundation/functions.h>
#include <maxscript/macros/generic_class.h>
#include <maxscript/macros/local_class.h>
#include <maxscript/macros/define_implementations.h>

#ifdef ScripterExport
#undef ScripterExport
#endif
#define ScripterExport __declspec( dllexport )

// --- Forward Declarations -----------------------------------------------------
//namespace XmlEmitFx
//{
class cXmlEmitFx;
//} // End of XmlEmitFx namespace

#define EFFECT_NAMEPSPACE_EFFECT

//namespace XmlEffectFx
//{
 DECLARE_LOCAL_GENERIC_CLASS(cXmlEffectFx, cXmlEffectFxGeneric);
 local_visible_class (cXmlEffectFx);

	// --- Structure/Class Definitions ----------------------------------------------
class cXmlEffectFx : public Value
{
public:
// maxscript extension
	ValueMetaClass* local_base_class( ) { return ( class_tag( cXmlEffectFx ) ); }
				classof_methods( cXmlEffectFx, Value );
	void		collect() 
	{ 
		// should not be hit since it's managed by these functions
		//delete this;
	}
	void		sprin1(CharStream* s);
	TCHAR*		class_name() { return _T("cXmlEffectFx"); }

	// operations 
// #include <maxscript\macros\local_abstract_generic_functions.h>
// 	def_local_generic(isMember,		"isMember");
// 	def_local_generic(intersection,	"intersection");
// 	def_local_generic(union,		"union");
// #include <maxscript\macros\local_implementations.h>
// #	include "listpro.h"

	//properties
  	def_property	( fxName );
	def_property	( fxFilename );
	def_property	( fxAsset );
	def_property	( fxLoopCount );
  	def_property	( fxRules );
	def_property	( fxGlobalScaleKeyTimes );
	def_property	( fxGlobalMinScale );
	def_property	( fxGlobalMaxScale );

 	Value*	get_property(Value** arg_list, int count);
 	Value*	set_property(Value** arg_list, int count);

	// SDK functionality

	typedef std::vector<cXmlEmitFx*> tContainer;
	typedef tContainer::iterator				itContainer;
	typedef tContainer::const_iterator			itcContainer;

	cXmlEffectFx( const char* sName, const char* sAsset, const char* sFilename );
	cXmlEffectFx( const std::string& sName, const std::string& sAsset, const std::string& sFilename );
	~cXmlEffectFx( );

	const std::string&	RetName( ) const;
	const std::string&	RetFilename( ) const;
	const std::string&	RetAsset( ) const;

	itContainer			EmitRuleBegin( );
	itContainer			EmitRuleEnd( );
	itcContainer		EmitRuleBegin( ) const;
	itcContainer		EmitRuleEnd( ) const;

	size_t				RetNumEmitRules( ) const;

private:
	//! Hide default constructor
	cXmlEffectFx( );
	//! Hide copy constructor
	cXmlEffectFx( const cXmlEffectFx& );

	void				ParseFile( const std::string& sFilename );

	std::string					m_sFilename;	//!< .effectrule Filename
	std::string					m_sName;		//!< Effect Name
	std::string					m_sAsset;		//!< Asset Name
	int							m_loopCount;
	std::vector<cXmlEmitFx*>	m_vEmitRules;	//!< Emit Rule child objs
	std::vector<float>			m_vGlobalScaleKeyTimes;
	std::vector<float>			m_vGlobalMinScale;
	std::vector<float>			m_vGlobalMaxScale;
};

//} // End of XmlEffectFx namespace

#undef EFFECT_NAMEPSPACE_EFFECT

#endif // !INC_XMLPARTICLEEFFECT_H_

// End of file
