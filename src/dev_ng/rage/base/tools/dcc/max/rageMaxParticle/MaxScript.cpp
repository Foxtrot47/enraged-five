//
// filename:	MaxScript.cpp
// author:		David Muir
// date:		26 April 2007
// description:	MAXScript Functions for RAGE Particle Plugin
//

// --- Include Files ------------------------------------------------------------

#include "XmlEffectFx.h"
#include "XmlEmitFx.h"
#include "Particle.h"
#include "EntityFx.h"
#include "Configuration.h"
#include "maxscript/maxscript.h"
#include <maxscript/maxwrapper/mxsobjects.h>
// define the new primitives using macros from SDK
#include <maxscript\macros\define_instantiation_functions.h>
#include "maxscript/macros/local_implementations.h" 
#include "maxscript/foundation/strings.h"
// --- Defines ------------------------------------------------------------------
def_visible_primitive( rageparticle_fxrefresh,		"rageparticle_fxrefresh" );
def_visible_primitive( rageparticle_rebuild,		"rageparticle_rebuild" );
def_visible_primitive( rageparticle_listfx,			"rageparticle_listfx" );
def_visible_primitive( rageparticle_fxexists,		"rageparticle_fxexists" );
def_visible_primitive( rageparticle_fxnumemitters,	"rageparticle_fxnumemitters");
def_visible_primitive( rageparticle_fxemitters,		"rageparticle_fxemitters" );
def_visible_primitive( rageparticle_getentityfxdat,  "rageparticle_getentityfxdat" );
def_visible_primitive( rageparticle_setentityfxdat,  "rageparticle_setentityfxdat" );
def_visible_primitive( rageparticle_geteffectroot,   "rageparticle_geteffectroot" );
def_visible_primitive( rageparticle_seteffectroot,   "rageparticle_seteffectroot" );
def_visible_primitive( rageparticle_setValidEffectBlocks,   "rageparticle_setValidEffectBlocks" );
// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

extern cEntityFx* g_pEntityFx;

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

/**
 * @brief MAXScript function to refresh representation of VFX
 */
Value* 
rageparticle_fxrefresh_cf( Value** arg_list, int count )
{
	check_arg_count( rageparticle_fxrefresh, 1, count );
	type_check( arg_list[0], MAXNode, "rageparticle_fxrefresh <node>" );
	assert( g_pEntityFx );

	INode* pNode = arg_list[0]->to_node( );
	assert( pNode );
	if ( !pNode )
		return &false_value;

	Object* pObject = pNode->GetObjectRef();
	assert( pObject );
	if ( !pObject )
		return &false_value;

	if ( pObject->IsSubClassOf( RAGEPARTICLE_CLASS_ID ) )
	{
		mprintf( "Refreshing representation of %s\n", pObject->GetObjectName() );
		cRageParticle* pFX = static_cast<cRageParticle*>( pObject );
		pFX->SetDirty( );

		Interface* pInterface = GetCOREInterface();
		pInterface->RedrawViews( pInterface->GetTime() );

		return &true_value;
	}
	else
	{
		mprintf( "%s is not a RAGE Particle object\n", pObject->GetObjectName() );
		return &false_value;
	}
}

/**
 * @brief MAXScript function to rebuild loaded VFX (reparses XML)
 *
 * No arguments.
 */
Value* 
rageparticle_rebuild_cf( Value** arg_list, int count )
{
//	rageparticle_rebuild errors:<errorTab>
	check_arg_count_with_keys(rageparticle_rebuild, 0, count);

// 	bool loading = false;
// 	if(count>0 && arg_list[0]->to_bool())
// 		loading = true;
// 
	Value* def_name(loading);
	Value* loadVal;
	bool_key_arg(loading, loadVal, FALSE);
	BOOL loading = &unsupplied==loadVal ? FALSE : loadVal->to_bool();

	Value* def_name(errors);
	Value* errors = key_arg(errors);
	if(&unsupplied!=loadVal)
		type_check(errors, Array, "errors:");

	if(g_pEntityFx)
	{
		delete g_pEntityFx;
		g_pEntityFx = NULL;
	}
	g_pEntityFx = new cEntityFx( Config::GetEntityFxFile(), loading);
	assert( g_pEntityFx );

	if(&unsupplied!=errors)
	{
		Array *errorArray = (Array*)errors;
		Value** items;
		for(std::vector<std::string>::const_iterator it = cEntityFx::s_vParseErrors.begin();
			it!=cEntityFx::s_vParseErrors.end();
			it++)
		{
			errorArray->append(new String((*it).c_str()));
		}

//		int n = cEntityFx::s_vParseErrors.size();
// 		value_local_array(items, n);
// 		for (int i = 0; i < n; i++)
// 			items[i] = new String(cEntityFx::s_vParseErrors[i].c_str());
// 		&errors = (Array *)&items;

//// 		pop_value_local_array(items);
	}

	return &true_value;
}

/**
 * @brief MAXScript function to list all loaded VFX
 *
 * No arguments.
 */
Value* 
rageparticle_listfx_cf( Value** arg_list, int count )
{
	check_arg_count( rageparticle_listfx, 0, count );
	assert( g_pEntityFx );

	for ( cEntityFx::itContainer itEffect = g_pEntityFx->EffectBegin();
		  itEffect != g_pEntityFx->EffectEnd();
		  ++itEffect )
	{
		cXmlEffectFx* pEffect = (*itEffect);
		mprintf( "Effect: %s [file: %s]\n", pEffect->RetName().c_str(), 
			     pEffect->RetFilename().c_str() );
	}

	mprintf( "\n\n%d Effects Loaded.\n", g_pEntityFx->RetNumEffects() );

	return &true_value;
}

/**
 * @brief MAXScript function to check whether an Effect exists (by effect name)
 *
 * Single argument being a string for the effect name
 * Returns true iff effect exists, false otherwise.
 */
Value* 
rageparticle_fxexists_cf( Value** arg_list, int count )
{
	check_arg_count( rageparticle_fxexists, 1, count );
	assert( g_pEntityFx );
	type_check( arg_list[0], String, "rageparticle_fxexists <String_Effect_Name>" );

	cXmlEffectFx* pEffect = g_pEntityFx->FindEffect( arg_list[0]->to_string() );
	
	return ( pEffect ? &true_value : &false_value );
}

/**
 * @brief MAXScript function to return the number of emitters in an Effect(by effect name)
 */
Value*
rageparticle_fxnumemitters_cf( Value** arg_list, int count )
{
	check_arg_count( rageparticle_fxexists, 1, count );
	assert( g_pEntityFx );
	type_check( arg_list[0], String, "rageparticle_fxnumemitters <String_Effect_Name>" );

	cXmlEffectFx* pEffect = g_pEntityFx->FindEffect( arg_list[0]->to_string() );
	size_t nNumEmitters = pEffect->RetNumEmitRules( );

	return ( pEffect ? Integer::intern((int)nNumEmitters) : Integer::intern(0) );
}

/**
 * @brief MAXScript function to display Effect Emitter properties (by effect name)
 *
 * Single argument being a string for the effect name
 * Returns true iff effect exists, false otherwise.
 */

#include "cRsParticleInfo.h"

Value* 
rageparticle_fxemitters_cf( Value** arg_list, int count )
{
// 	using namespace XmlEffectFx;
// 	using namespace XmlEmitFx;

	check_arg_count( rageparticle_fxemitters, 1, count );
	assert( g_pEntityFx );
	type_check( arg_list[0], String, "rageparticle_fxemitters <String_Effect_Name>" );

	cXmlEffectFx* pEffect = g_pEntityFx->FindEffect( arg_list[0]->to_string() );
	if ( pEffect )
	{
// 		mprintf( "Effect %s Emitter Properties:\n", pEffect->RetName().c_str() );
// 		mprintf( "\t%d Emitters defined\n", pEffect->RetNumEmitRules() );
// 		
// 		for ( cXmlEffectFx::itContainer itEmitter = pEffect->EmitRuleBegin();
// 			  itEmitter != pEffect->EmitRuleEnd();
// 			  ++itEmitter )
// 		{
// 			cXmlEmitFx* pEmitter = (*itEmitter);
// 		
// 			mprintf( "\tName: %s\n", pEmitter->RetName().c_str() );
// 			
// 			switch ( pEmitter->RetEmitterType() )
// 			{
// 			case cXmlEmitFx::kDomainType_Box:
// 				mprintf( "\t\tEmitter Type: Box\n" );
// 				break;
// 			case cXmlEmitFx::kDomainType_Cylinder:
// 				mprintf( "\t\tEmitter Type: Cylinder\n" );
// 				break;
// 			case cXmlEmitFx::kDomainType_Sphere:
// 				mprintf( "\t\tEmitter Type: Sphere\n" );
// 				break;
// 			case cXmlEmitFx::kDomainType_Unknown:
// 			default:
// 				mprintf( "\t\tEmitter Type: Unknown\n" );
// 				break;
// 			}
// 
// 			const float* pPosition = pEmitter->RetPosition( );
// 			mprintf( "\t\tPosition (x, y, z): ( %f, %f, %f )\n", pPosition[0], pPosition[1], pPosition[2] );
// 			const float* pDirection = pEmitter->RetDirection( );
// 			mprintf( "\t\tDirection (x, y, z): ( %f, %f, %f )\n", pDirection[0], pDirection[1], pDirection[2] );
// 			const float* pSize = pEmitter->RetSize( );
// 			mprintf( "\t\tSize (x, y, z): ( %f, %f, %f )\n", pSize[0], pSize[1], pSize[2] );
// 		}

		return pEffect;
	}

	return ( &undefined );
}

/**
* @brief MAXScript function to set the current path of EntityFx.dat.
*
* Returns absolute filename of the entityfx.dat file.
*/
Value*
rageparticle_getentityfxdat_cf( Value** arg_list, int count )
{
	check_arg_count( rageparticle_getentityfxdat, 0, count );
	return_protected( new String( (char*)Config::GetEntityFxFile().c_str() ) );
}

/**
* @brief MAXScript function to set the path to the EntityFx.dat file.
*
* Returns true on success, false otherwise (e.g. if the file does not
* exist).
*/
Value*
rageparticle_setentityfxdat_cf( Value** arg_list, int count )
{
	check_arg_count( gtaparticle_setentityfxdat, 1, count );
	type_check( arg_list[0], String, "rageparticle_setentityfxdat <String_EntityFxDat_Filename>" );

	// Check the path is legal and that the file exists.
	MaxSDK::Util::Path fullpath( arg_list[0]->to_string() );
	if ( fullpath.IsLegal() && fullpath.IsAbsolute() ) 
	{
		FILE* fp = fopen( fullpath.GetCStr(), "r" );
		if ( fp )
		{
			fclose( fp );
			Config::SetEntityFxFile( fullpath.GetCStr() );
			return ( &true_value );
		}
		else
		{
			mprintf( "RageParticle: %s does not exist, or is not readable.\n", fullpath.GetCStr() );
		}
	}
	else
	{
		mprintf( "rageparticle_setentityfxdat requires a absolute path name to the entityfx.dat file.\n" );
	}

	return ( &false_value );
}

/**
* @brief MAXScript function to get the current root path to effects data.
*/
Value*
rageparticle_geteffectroot_cf( Value** arg_list, int count )
{
	check_arg_count( rageparticle_geteffectroot, 0, count );
	return_protected( new String( (char*)Config::GetEffectRoot().c_str() ) );
}

/**
* @brief MAXScript function to set the current root path to effects data.
*/
Value*
rageparticle_seteffectroot_cf( Value** arg_list, int count )
{
	check_arg_count( rageparticle_seteffectroot, 1, count );
	type_check( arg_list[0], String, "rageparticle_seteffectroot <String_EffectRoot_Directory>" );

	// Check the path is legal and that the file exists.
	MaxSDK::Util::Path fullpath( arg_list[0]->to_string() );
	if ( fullpath.IsLegal() && fullpath.IsAbsolute() ) 
	{
		Config::SetEffectRoot( fullpath.GetCStr() );
		Config::SetEffectRulesRoot( Config::GetEffectRoot() + "/effectrules" );
		Config::SetEmitRulesRoot( Config::GetEffectRoot() + "/emitrules" );
		
		return ( &true_value );
	}
	else
	{
		mprintf( "rageparticle_setentityfxdat requires a absolute path name to the entityfx.dat file.\n" );
	}

	return ( &false_value );
}

/**
/*
**/

Value* 
rageparticle_setValidEffectBlocks_cf(Value** arg_list, int count )
{
	check_arg_count( rageparticle_setValidEffectBlocks, 1, count );
	type_check( arg_list[0], Array, "rageparticle_setValidEffectBlocks <block name array>" );

	Array *pArray = dynamic_cast<Array*>(arg_list[0]);
//	int valueCount = pArray->get_count(NULL, 0)->to_int();
	cEntityFx::EffectBlocks().clear();
	for(int k=0;k<pArray->size;k++)
	{
		MCHAR *blockname = pArray->data[k]->to_string();
		cEntityFx::EffectBlocks().push_back(blockname);
	}
	return &true_value;
}

// End of file
