Project rageMaxParticle
Template max_configurations.xml

OutputFileName rageMaxParticle.dlo
ConfigurationType dll

Define RAGEPARTICLE_EXPORTS

EmbeddedLibAll core.lib geom.lib gfx.lib mesh.lib maxutil.lib maxscrpt.lib paramblk2.lib comctl32.lib

ForceInclude ForceInclude.h

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\tools\libs\rageDataStoreAttributes
IncludePath $(RAGE_DIR)\base\tools\dcc\max\rageMaxDataStore
IncludePath $(RAGE_DIR)\framework\tools\src

Files {
	Configuration.cpp
	Configuration.h
	DllEntry.cpp
	EntityFx.cpp
	EntityFx.h
	MaxScript.cpp
	Particle.cpp
	Particle.h
	Particle.rc
	ParticleMouse.cpp
	ParticleMouse.h
	Resource.h
	Win32MsgBox.cpp
	Win32MsgBox.h
	Win32Res.cpp
	Win32Res.h
	XmlEffectFx.cpp
	XmlEffectFx.h
	XmlEmitFx.cpp
	XmlEmitFx.h
}