//
// filename:	Particle.h
// description:	
//

#ifndef INC_PARTICLE_H_
#define INC_PARTICLE_H_

// --- Include Files ------------------------------------------------------------

// 3DS Max headers
#include "resource.h"

// STL headers
#include <vector>

// --- Defines ------------------------------------------------------------------

#ifdef RAGEPARTICLE_EXPORTS
#define RAGEPARTICLE_API __declspec(dllexport)
#else
#define RAGEPARTICLE_API __declspec(dllimport)
#endif

#define RAGEPARTICLE_CLASS_ID		Class_ID(0x110a6ecf, 0x561063cf)
#define PICKUP_CLASS_ID				Class_ID(0x608766b2, 0x20a03dbb)

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

extern HINSTANCE g_hInstance;

#define RAGEPARTICLE_PBLOCK_REF 0
#define RAGEPARTICLE_MESH0_REF 1

/**
 *
 */
class cRageParticle : public HelperObject
{
public:
	cRageParticle( );
	~cRageParticle( );

	void			SetDirty( );

	// HelperObject
	// -- None --

	// Object
	ObjectState		Eval( TimeValue t );
	void			InitNodeName( TSTR& s ) { s = m_cClassName; }

	// BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	void			BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev );
	void			EndEditParams( IObjParam *ip, ULONG flags,Animatable *next );
	TCHAR*			GetObjectName( );
	int				Display(TimeValue t, INode* pNode, ViewExp *vpt, int flags );
	void			GetWorldBoundBox( TimeValue t, INode* pNode, ViewExp* vp, Box3& box );
	void			GetLocalBoundBox( TimeValue t, INode* pNode, ViewExp* vp, Box3& box );
	int				HitTest( TimeValue t, INode* pNode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt );

	// ReferenceTarget
	ReferenceTarget* Clone( RemapDir &remap = DefaultRemapDir() );

	// ReferenceMaker
	int				NumRefs( );
	RefTargetHandle GetReference( int i );
	void			SetReference( int i, RefTargetHandle rtarg );

	// Animatable methods
	int				NumParamBlocks( ) { return 0; }
	IParamBlock2*	GetParamBlock( int i ) { return NULL; }
	IParamBlock2*	GetParamBlockByID( short id ) { return NULL; }
	void			DeleteThis( ) { delete this; }
	Class_ID		ClassID( ) { return RAGEPARTICLE_CLASS_ID; }  
	void			GetClassName( TSTR& s ) { s = m_cClassName; }

	RefResult		NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message);

private:
	void			DrawMeshes( GraphicsWindow* gw, TimeValue t, Matrix3& objMat, INode* pNode, ViewExp* vpt );

	void			ClearMeshes( );
	void			BuildMeshes( INode* pNode );
	TriObject*		BuildDefault( );
	TriObject*		BuildBox( Point3& ptPosition, Point3& ptSize, Point3& ptDirection );
	TriObject*		BuildCylinder( Point3& ptPosition, Point3& ptSize, Point3& ptDirection );
	TriObject*		BuildSphere( Point3& ptPosition, Point3& ptSize );

	static void		CheckMinSize( Point3& ptSize );

	float						m_fScale;			//!< Scaling factor
	TCHAR						m_sName[513];		//!< Attribute name
	bool						m_bDirty;
	TCHAR						m_cClassName[64];
	std::vector<TriObject*>		m_vpTriObjects;
};	

#endif // !INC_PARTICLE_H_

// End of file
