//
// filename:	Configuration.h
// description:	Configuration Data for Particle plugin
//

#ifndef INC_CONFIGURATION_H_
#define INC_CONFIGURATION_H_

// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>

/**
 * @brief Configuration settings class
 *
 * This object is responsible for getting the plug-in's configuration
 * information from the module settings.
 */
class Config
{
public:
	static void Init();
public:
	static const std::string	GetEntityFxFile()		{ return m_entityFxFile; }
	static const std::string	GetEffectRoot()			{ return m_effectRoot; }
	static const std::string	GetEffectRulesRoot()	{ return m_effectRulesRoot; }
	static const std::string	GetEmitRulesRoot()		{ return m_emitRulesRoot; }

	static void	SetEntityFxFile( std::string entityFxFile )	{ m_entityFxFile = entityFxFile; } 
	static void	SetEffectRoot( std::string effectRoot )	{ m_effectRoot = effectRoot; } 
	static void	SetEffectRulesRoot( std::string effectRulesRoot )	{ m_effectRulesRoot = effectRulesRoot; } 
	static void	SetEmitRulesRoot( std::string emitRulesRoot )	{ m_emitRulesRoot = emitRulesRoot; } 
private:
	static std::string m_entityFxFile;
	static std::string m_effectRoot;
	static std::string m_effectRulesRoot;
	static std::string m_emitRulesRoot;
};

#endif // !INC_CONFIGURATION_H_

// End of file
