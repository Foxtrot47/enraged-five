//
// filename:	XmlParticleEffect.h
// description:	Defines class representing a partial Rage Effect .emitrule file
//				Only reads enough information to read the Domain Size from the
//				.emitrule file.
//
//				As the .effectrule file is XML we use TinyXML.
//

#ifndef INC_XMLEMITFX_H_
#define INC_XMLEMITFX_H_

#include <maxscript/maxscript.h>
//#include <maxscrpt/Funcs.h>
#include <maxscript/macros/generic_class.h>
#include <maxscript/macros/local_class.h>
#include <maxscript/macros/define_implementations.h>


// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>

// --- Xml Tag Constants --------------------------------------------------------
const static char* sC_s_str_Emitter_Root				= "rage__ptxEmitterRule";
const static char* sC_s_str_Emitter_Name				= "name";
const static char* sC_s_str_EmitInfo					= "EmitInfo";
const static char* sC_s_str_EmitterDomain				= "creationDomainObj";
const static char* sC_s_str_Domain						= "pDomain";
const static char* sC_s_str_DomainPosition				= "positionKFP";
const static char* sC_s_str_DomainIsWorldSpace			= "isWorldSpace";
const static char* sC_s_str_DomainDirection				= "rotationKFP";
const static char* sC_s_str_DomainSize					= "sizeOuterKFP";
const static char* sC_s_str_InnerDomainSize				= "sizeInnerKFP";
const static char* sC_s_str_DomainKeyFrame				= "keyframe";
const static char* sC_s_str_DomainData					= "keyData";
const static char* sC_s_str_DomainNumKeyEntries			= "numKeyEntries";
const static char* sC_s_str_DomainKeyData				= "keyEntryData";

// --- Xml Attribute Constants --------------------------------------------------
const static char* sC_s_str_DomainType					= "type";

// --- Xml Data Constants -------------------------------------------------------
const static char* sC_s_str_DomainTypeBox				= "rage__ptxDomainBox";
const static char* sC_s_str_DomainTypeCylinder			= "rage__ptxDomainCylinder";
const static char* sC_s_str_DomainTypeSphere			= "rage__ptxDomainSphere";

// --- Forward Declarations -----------------------------------------------------
namespace rage
{
	class parTreeNode;
}

// --- Defines ------------------------------------------------------------------

//namespace XmlEmitFx
//{

// --- Structure/Class Definitions ----------------------------------------------

local_visible_class (cXmlEmitFx);

class cXmlEmitFx : public Value
{
public:
//maxscript extension
	ValueMetaClass* local_base_class( ) { return ( class_tag( cXmlEmitFx ) ); }
	classof_methods( cXmlEmitFx, Value );
	void		collect()
	{
		//should not be hit since it's managed by these functions
		//delete this; 
	}
	void		sprin1(CharStream* s);
	TCHAR*		class_name() { return _T("cXmlEmitFx"); }
	def_property	( emName );
	def_property	( emFilename );
	def_property	( emType );
	def_property	( emPos );
	def_property	( emDir );
	def_property	( emSize );
	def_property	( emInnerSize );
	def_property	( emZoom );
	def_property	( emIsWorldSpace );

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

// #include <maxscript\macros\local_abstract_generic_functions.h>
// #include <maxscript\macros\local_implementations.h>

	// SDK functionality
	cXmlEmitFx( const char* sFilename );
	cXmlEmitFx( const std::string& sFilename );
// 	~cXmlEmitFx( );

	enum etDomainType
	{
		kDomainType_Unknown,
		kDomainType_Box,
		kDomainType_Cylinder,
		kDomainType_Sphere
	};

	const std::string&	
		RetName( ) const
	{
		return ( m_sName );
	}

	const std::string&	
		RetFilename( ) const
	{
		return ( m_sFilename );
	}

	etDomainType		
		RetEmitterType( ) const
	{
		return ( m_eDomainType );
	}

	const float*				
		RetPosition( ) const
	{
		return &m_vPosition[0];
	}

	const float*				
		RetDirection( ) const
	{
		return &m_vDirection[0];
	}

	const float*				
		RetSize( ) const
	{
		return &m_vSize[0];
	}

private:
	//! Hide default constructor
	cXmlEmitFx( );
	//! Hide copy constructor
	cXmlEmitFx( const cXmlEmitFx& );

	void				ParseFile( const std::string& sFilename );

public:
	std::string			m_sName;			//!< Emitrule name
	std::string			m_sFilename;		//!< Filename of .emitrule

	etDomainType		m_eDomainType;		//!< Domain type
	Point3				m_vPosition;		//!< Relative position of domain volume
	Point3				m_vDirection;		//!< Direction of domain volume
	Point3				m_vSize;			//!< Size of domain volume
	Point3				m_vInnerSize;		//!< Size of domain volume
	Point3				m_vZoom;			// scale randomise factor
	bool				m_bIsWorldSpace;	// is the emitters domain in world space
};

//} // End of XmlEmitFx namespace

#undef EFFECT_NAMEPSPACE_EMITTER

#endif // !INC_XMLEMITFX_H_

// End of file
