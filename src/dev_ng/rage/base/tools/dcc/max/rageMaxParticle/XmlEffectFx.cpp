//
// filename:	XmlEffectFx.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// Max headers
#include <maxscript\macros\local_class.h>
#include <maxscript\macros\local_class_instantiations.h>
#include <maxscript/foundation/3dmath.h>

// Local headers
#include "XmlEffectFx.h"

// --- Using Directives ---------------------------------------------------------

using namespace rage;

//namespace XmlEffectFx
//{
/* --------------------  local generics and names   --------------- */
// #	include "listpro.h"
// #	include "setpro.h"

#ifdef ScripterExport
#undef ScripterExport
#endif
#define ScripterExport __declspec( dllexport )

DEFINE_LOCAL_GENERIC_CLASS(cXmlEffectFx, cXmlEffectFxGeneric);

#include <maxscript\macros\local_class_instantiations.h>
  	def_name (fxName);
   	def_name (fxRules);
	def_name (fxAsset);
	def_name (fxLoopCount);
	def_name (fxFilename);
	def_name (fxGlobalMinScale);
	def_name (fxGlobalMaxScale);
	def_name (fxGlobalScaleKeyTimes );
	
local_visible_class_instance ( cXmlEffectFx, "cXmlEffect" )

// Value*
// cXmlEffectFx::apply(Value** arg_list, int count,  CallContext* cc)
// {
// 	return_value(Value::apply(arg_list, count, cc));
// }

// Local headers
#include "XmlEmitFx.h"
#include "EntityFx.h"
#include "Configuration.h"
#include "Win32MsgBox.h"

// RAGE headers
#include "parser/manager.h"
#include "parser/tree.h"

#include "Utils.h"

// --- Constants ----------------------------------------------------------------

// --- Xml Tag Constants --------------------------------------------------------
const char* sC_s_str_Root						= "rage__ptxEffectRuleStd";
const char*	sC_s_str_Name						= "name";
const char* sC_s_str_TimeLine					= "timeline";
const char* sC_s_str_Events						= "events";
const char* sC_s_str_Item						= "Item";
const char* sC_s_str_EmitRuleName				= "emitterRuleName";
const char* sC_s_str_ParticleRuleName			= "particleRuleName";
const char* sC_s_str_ZoomMin					= "zoomScalarMin";
const char* sC_s_str_ZoomMax					= "zoomScalarMax";
const char* sC_s_str_LoopCount					= "numLoops";
const char* sC_s_str_FxDomainScaleKFP			= "zoomScalarKFP";

// --- Xml Attribute Constants --------------------------------------------------
const char* sC_s_str_ItemType					= "type";
const char* sC_s_str_ItemValue					= "value";

// --- Xml Data Constants -------------------------------------------------------
const char* sC_s_str_EventEmitter				= "rage__ptxEventEmitter";

const std::string&	
cXmlEffectFx::RetName( ) const
{
	return m_sName;
}

const std::string&	
cXmlEffectFx::RetFilename( ) const
{
	return m_sFilename;
}

cXmlEffectFx::itContainer			
cXmlEffectFx::EmitRuleBegin( )
{
	return ( m_vEmitRules.begin() );
}

cXmlEffectFx::itContainer			
cXmlEffectFx::EmitRuleEnd( )
{
	return ( m_vEmitRules.end() );
}

cXmlEffectFx::itcContainer		
cXmlEffectFx::EmitRuleBegin( ) const
{
	return ( m_vEmitRules.begin() );
}

cXmlEffectFx::itcContainer		
cXmlEffectFx::EmitRuleEnd( ) const
{
	return ( m_vEmitRules.end() );
}

size_t
cXmlEffectFx::RetNumEmitRules( ) const
{
	return ( m_vEmitRules.size() );
}

using namespace std;

void
cXmlEffectFx::ParseFile( const std::string& sFilename )
{
	m_sFilename = sFilename;

	parTree* pEffectXml = PARSER.LoadTree( m_sFilename.c_str(), "effectrule" );
	if ( pEffectXml == NULL )
		ThrowEntityFxException( m_sFilename.c_str(), "Error loading XML file %s.", m_sFilename.c_str() );

	// Get <Events> node
	parTreeNode* pNodeTimeline = pEffectXml->GetRoot()->FindChildWithName( sC_s_str_TimeLine );
	//assert( pNodeTimeline );
	if ( !pNodeTimeline )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) looking for <Timeline> tag.", m_sFilename.c_str());

	// Fetch our loop count
	parTreeNode* pElemLoopCount = pEffectXml->GetRoot()->FindChildWithName( sC_s_str_LoopCount );
	//assert( pElemLoopCount );
	if ( !pElemLoopCount )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) looking for <numLoops> tag.", m_sFilename.c_str());
	if ( pElemLoopCount )
	{
		atArray<parAttribute>& attributes = pElemLoopCount->GetElement().GetAttributes();
		if ( attributes.GetCount() > 0 )
		{
			m_loopCount = attributes[0].FindIntValue();
		}
	}

	parTreeNode* pNodeEvents = pNodeTimeline->FindChildWithName( sC_s_str_Events );
	//assert( pNodeEvents );
	if ( !pNodeEvents )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) looking for <Events> tag.", m_sFilename.c_str());

	// From <Events> node read through <Item> nodes to find all our emitters
	if ( pNodeEvents )
	{
		for ( parTreeNode::ChildNodeIterator it = pNodeEvents->BeginChildren(); it != pNodeEvents->EndChildren(); ++it )
		{
			parTreeNode* pElemItem = (*it);
			//assert( pElemItem );

			// If this child node is an <Item> node
			if ( 0 == strcmp( pElemItem->GetElement().GetName(), sC_s_str_Item ) )
			{
				atArray<parAttribute>& attributes = pElemItem->GetElement().GetAttributes();
				if ( attributes.GetCount() > 0 )
				{
					// If we are an emitter
					if ( 0 == strcmp( attributes[0].GetStringValue(), sC_s_str_EventEmitter ) )
					{
						parTreeNode* pElemEmitRule = pElemItem->FindChildWithName( sC_s_str_EmitRuleName );
						//assert( pElemEmitRule );
						if ( !pElemEmitRule )
							ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) looking for <EmitRuleName> tag.", m_sFilename.c_str());

						std::string sEmitPath = Config::GetEmitRulesRoot() + "\\" +
												pElemEmitRule->GetData() + ".emitrule";

// 						try
// 						{
							cXmlEmitFx* pEmitRule = new cXmlEmitFx( sEmitPath );
							//assert( pEmitRule );
							m_vEmitRules.push_back( pEmitRule );

							// Fetch our zoom min/max values.
							parTreeNode* pElemZoomMin = pElemItem->FindChildWithName( sC_s_str_ZoomMin );
							//assert( pElemZoomMin );
							if ( !pElemZoomMin )
								ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) looking for <zoomScalarMin> tag.", m_sFilename.c_str());
							else
							{
								atArray<parAttribute>& attributes = pElemZoomMin->GetElement().GetAttributes();
								if ( attributes.GetCount() > 0 )
								{
									pEmitRule->m_vZoom.x = attributes[0].FindFloatValue();
								}
							}
							parTreeNode* pElemZoomMax = pElemItem->FindChildWithName( sC_s_str_ZoomMax );
							//assert( pElemZoomMax );
							if ( !pElemZoomMax )
								ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) looking for <zoomScalarMax> tag.", m_sFilename.c_str());
							else
							{
								atArray<parAttribute>& attributes = pElemZoomMax->GetElement().GetAttributes();
								if ( attributes.GetCount() > 0 )
								{
									pEmitRule->m_vZoom.y = attributes[0].FindFloatValue();
								}
							}
//						}
// 						catch ( exEntityFxException& /*ex*/ )
// 						{
// 							// Prevent XML errors from being displayed.
// #if 0
// 							DisplayError( "RAGE Particle Error", ex.what() );
// #endif
// 						}
					}
				}
			}
		}
	}

	parTreeNode* pNodeScaleKFP = pEffectXml->GetRoot()->FindChildWithName( sC_s_str_FxDomainScaleKFP );
	//assert( pNodeEvents );
	if ( !pNodeScaleKFP )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) looking for <%s> tag.", m_sFilename.c_str(), sC_s_str_FxDomainScaleKFP);
	parTreeNode* pElemKeyframe = pNodeScaleKFP->FindChildWithName( sC_s_str_DomainKeyFrame );
	if ( !pElemKeyframe )
		ThrowEntityFxException( m_sFilename.c_str(), "Error parsing XML file (%s) %s tag not found.",
		m_sFilename.c_str(), sC_s_str_DomainKeyFrame );

	vector<float*> arrayData;
	vector<float> keyTimes;
	ParseVectorData(pElemKeyframe, arrayData, keyTimes, sFilename.c_str());
	for(int k=0;k<arrayData.size();k++)
	{
		m_vGlobalMinScale.push_back(arrayData[k][0]);
		m_vGlobalMaxScale.push_back(arrayData[k][1]);
		m_vGlobalScaleKeyTimes.push_back(keyTimes[k]);
	}

	// Cleanup
	delete pEffectXml;
}

//////////////////////////////////////////////////////////////////////////
// Max valule accessors
//////////////////////////////////////////////////////////////////////////

// Value*
// cXmlEffectFx::set_name(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	m_sName = arg_list[0]->to_string();
// 	return &true_value;
// }
// //////////////////////////////////////////////////////////////////////////
// Value*
// cXmlEffectFx::get_name(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	return new String(m_sName.c_str());
// }
// Value*
// cXmlEffectFx::get_emitterRules(Value** arg_list, int count)
// {
// 	check_for_deletion();
// 	Array *ret = new Array(m_vEmitRules.size);
// 	for(std::vector<cXmlEmitFx*>::iterator it = m_vEmitRules.begin();
// 		it < m_vEmitRules.end();
// 		it++)
// 		ret->push((*it));
// 	return ret;
// }

void
cXmlEffectFx::sprin1( CharStream* s )
{
	s->printf( "cXmlEffectFx(\n" );
	s->printf( "\tfxName:					\"%s\",\n",	m_sName.c_str());
	s->printf( "\tfxFilename:				\"%s\",\n",	m_sFilename.c_str() );
	s->printf( "\tfxAsset:					\"%s\",\n",	m_sAsset.c_str() );
	s->printf( "\tfxLoopCount:				 %d,\n",		m_loopCount );
	s->printf( "\tfxRules size:				 %d\n",		m_vEmitRules.size() );
	s->printf( "\tfxGlobalScaleKeyTimes size:%d\n",	m_vGlobalScaleKeyTimes.size() );
	s->printf( "\tfxGlobalMinScale size:	 %d\n",	m_vGlobalMinScale.size() );
	s->printf( "\tfxGlobalMaxScale size:	 %d\n",	m_vGlobalMaxScale.size() );
	s->printf( ")\n" );
}

Value*
cXmlEffectFx::get_property( Value** arg_list, int count )
{
	Value* prop = arg_list[0];
	if (prop == n_fxName)
	{
		return( new String( m_sName.c_str() ) );
	}
	else if (prop == n_fxFilename)
	{
		return( new String( m_sFilename.c_str() ) );
	}
	else if (prop == n_fxAsset)
	{
		return( new String( m_sAsset.c_str() ) );
	}
	else if (prop == n_fxLoopCount)
	{
		return( new Integer(m_loopCount) );
	}
	else if (prop == n_fxGlobalScaleKeyTimes)
	{
		Array *ret = new Array((int)m_vGlobalScaleKeyTimes.size());
		for(std::vector<float>::iterator it = m_vGlobalScaleKeyTimes.begin();
			it < m_vGlobalScaleKeyTimes.end();
			it++)
			ret->append(new Float(*it));
		return( ret );
	}
	else if (prop == n_fxGlobalMinScale)
	{
		Array *ret = new Array((int)m_vGlobalMinScale.size());
		for(std::vector<float>::iterator it = m_vGlobalMinScale.begin();
			it < m_vGlobalMinScale.end();
			it++)
			ret->append(new Float(*it));
 		return( ret );
	}
	else if (prop == n_fxGlobalMaxScale)
	{
		Array *ret = new Array((int)m_vGlobalMaxScale.size());
		for(std::vector<float>::iterator it = m_vGlobalMaxScale.begin();
			it < m_vGlobalMaxScale.end();
			it++)
			ret->append(new Float(*it));
		return( ret );
	}
	else if (prop == n_fxRules)
	{
		Array *ret = new Array((int)m_vEmitRules.size());
		for(std::vector<cXmlEmitFx*>::iterator it = m_vEmitRules.begin();
			it < m_vEmitRules.end();
			it++)
			ret->append((*it));
		return( ret );
	}
	return Value::get_property( arg_list, count );
}

Value*
cXmlEffectFx::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The tree is read-only.", this );
	return_protected(  &undefined );
}

//} // End of XmlEffectFx namespace

// End of file


#include <maxscript/macros/local_implementations.h> // switch def
// --- Code ---------------------------------------------------------------------
cXmlEffectFx::cXmlEffectFx( const char* sName, const char* sAsset, const char* sFilename )
	: m_sFilename( sFilename )
	, m_sName( sName )
	, m_sAsset( sAsset )
{
	tag = class_tag(cXmlEffectFx);
	ParseFile( m_sFilename );

	def_name (fxName);
	def_name (fxRules);
	def_name (fxAsset);
	def_name (fxLoopCount);
	def_name (fxFilename);
	def_name (fxGlobalMinScale);
	def_name (fxGlobalMaxScale);
	def_name (fxGlobalScaleKeyTimes );

	make_heap_permanent();
}

cXmlEffectFx::cXmlEffectFx( const std::string& sName, const std::string& sAsset, const std::string& sFilename )
: m_sFilename( sFilename )
, m_sName( sName )
, m_sAsset( sAsset )
{
	tag = class_tag(cXmlEffectFx);
	ParseFile( m_sFilename );

	def_name (fxName);
	def_name (fxRules);
	def_name (fxAsset);
	def_name (fxLoopCount);
	def_name (fxFilename);
	def_name (fxGlobalMinScale);
	def_name (fxGlobalMaxScale);
	def_name (fxGlobalScaleKeyTimes );

	make_heap_permanent();
}

cXmlEffectFx::~cXmlEffectFx( )
{
	// Free allocated cXmlEmitFx vector
	itContainer itEmitRule = EmitRuleBegin();
	while (itEmitRule != EmitRuleEnd())
	{
		cXmlEmitFx* pEmitRule = (*itEmitRule);
		delete pEmitRule;
		pEmitRule = NULL;
		itEmitRule = m_vEmitRules.erase(itEmitRule);
	}
// 	delete[] m_vGlobalMaxScale.;
// 	delete[] m_vGlobalMinScale;
// 	delete[] m_vGlobalScaleKeyTimes;
}
