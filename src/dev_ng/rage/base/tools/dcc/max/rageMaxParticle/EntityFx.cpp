//
// filename:	EntityFx.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// Local headers
#include "EntityFx.h"
#include "XmlEffectFx.h"
#include "Utils.h"

#include "Configuration.h"
#include "Particle.h"
#include "Win32MsgBox.h"
#include "Win32Res.h"
#include "resource.h"

// STL headers
#include <cassert>
#include <fstream>

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------
const int			sC_s_cnt_LineBufferSize			= 512;
const char			sC_s_ch_CommentPrefix			= '#';

// const char* m_vValidEffectBlocks[] = 
// {
// 	"ENTITYFX_AMBIENT_PTFX_START",
// 	"FRAGMENTFX_BREAK_PTFX_START",
// 	"ENTITYFX_COLLISION_PTFX_START",
// 	"FRAGMENTFX_DESTROY_PTFX_START",
// 	"ENTITYFX_SHOT_PTFX_START",
// 	"ENTITYFX_ANIM_PTFX_START",
// 	"ENTITYFX_RAYFIRE_PTFX_START",
// 	NULL
// };

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

std::vector<std::string>	cEntityFx::s_vParseErrors;
std::vector<const char *>	cEntityFx::s_vValidEffectBlocks;

void cEntityFx::ParserError(std::string msg)
{
	s_vParseErrors.push_back(msg);
}

// --- cEntityFx Code -----------------------------------------------------------

cEntityFx::cEntityFx( )
	: m_sFilename( "" )
{
	m_vParsedEffects.clear( );
}

void cEntityFx::CheckEffectsAreFilled()
{
// 	if(m_vParsedEffects.empty())
// 	{
// 		std::string msg = "No effects were found. Please get latest on:";
// 		msg.append("\n"+m_sFilename);
// 		for(std::vector<std::string>::iterator it = m_vEffectPaths.begin();
// 			it!=m_vEffectPaths.end();
// 			it++)
// 		{
// 			msg.append(",\n"+(*it));
// 		}
// 		MessageBox(GetCOREInterface()->GetMAXHWnd(), msg.c_str(), "Parsing Error", NULL);
// 	}
	if(!s_vParseErrors.empty() || m_vParsedEffects.empty())
	{
		std::string msg = "";
		bool firstmessage = true;
		if(m_vParsedEffects.empty())
		{
			std::string emptymsg = "";
			emptymsg.append("no effects were inited at all. make sure you've got latest on ");
			emptymsg.append(m_sFilename);
			emptymsg.append("and the effect definition files in the VFX folder in art.");
			s_vParseErrors.push_back(emptymsg);
		}
		for(std::vector<std::string>::iterator it = s_vParseErrors.begin();
			it!=s_vParseErrors.end();
			it++)
		{
			if(!firstmessage)
				msg.append(",\n");
			else
				firstmessage = false;
			msg.append(*it);
		}
//		MessageBox(GetCOREInterface()->GetMAXHWnd(), msg.c_str(), "Parsing Errors", NULL);
	}
}

cEntityFx::cEntityFx( const char* sFilename, bool loading)
	: m_sFilename( sFilename )
{
	m_vParsedEffects.clear( );
	m_vEffectPaths.clear();
	s_vParseErrors.clear();
	ParseFile( m_sFilename );
	if(!loading && strcmp(sFilename,""))
		CheckEffectsAreFilled();
}

cEntityFx::cEntityFx( const std::string& sFilename, bool loading)
	: m_sFilename( sFilename )
{
	m_vParsedEffects.clear( );
	m_vEffectPaths.clear();
	s_vParseErrors.clear();
	ParseFile( m_sFilename );
	if(!loading && strcmp(sFilename.c_str(),""))
		CheckEffectsAreFilled();
}

cEntityFx::~cEntityFx( )
{
	// No freeing of members since the memory gets killed on shutdown from max
}

cEntityFx::itContainer		
cEntityFx::EffectBegin( )
{
	return ( m_vParsedEffects.begin() );
}

cEntityFx::itContainer		
cEntityFx::EffectEnd( )
{
	return ( m_vParsedEffects.end() );
}

cEntityFx::itcContainer	
cEntityFx::EffectBegin( ) const
{
	return ( m_vParsedEffects.begin() );
}

cEntityFx::itcContainer	
cEntityFx::EffectEnd( ) const
{
	return ( m_vParsedEffects.end() );
}

size_t
cEntityFx::RetNumEffects( ) const
{
	return ( m_vParsedEffects.size() );
}

cXmlEffectFx* 
cEntityFx::FindEffect( const char* sEffectName ) const
{
	return FindEffect( std::string( sEffectName ) );
}

cXmlEffectFx* 
cEntityFx::FindEffect( const std::string& sEffectName ) const
{
	itcContainer itEffect = EffectBegin();
	while ( itEffect != EffectEnd() )
	{
		if ( (*itEffect)->RetName() == sEffectName )
			return (*itEffect);

		++itEffect;
	}

	return NULL;
}

void 
cEntityFx::ParseFile( const std::string& sFilename )
{
	m_sFilename = sFilename;

	std::ifstream fsInput;
	fsInput.open( m_sFilename.c_str(), std::ios::in );
	if ( !fsInput.is_open() )
		return;

	// Process each line of the input file constructing our effects
	// and pushing them onto our vector of effect objects.
	char sLineBuffer[sC_s_cnt_LineBufferSize];
	char sEffectName[sC_s_cnt_LineBufferSize];
	char sEffectAsset[sC_s_cnt_LineBufferSize];
	char sEffectFilename[sC_s_cnt_LineBufferSize];
	char sSlop[sC_s_cnt_LineBufferSize];

	// Read version identifier
	fsInput.getline( sLineBuffer, sC_s_cnt_LineBufferSize );

	// empty array and delete old data
	m_vParsedEffects.clear( );

	std::string sCurrBlock;

	while ( !fsInput.eof() )
	{
		// Clear string buffers
		memset( &sLineBuffer, 0, sC_s_cnt_LineBufferSize );
		memset( &sEffectName, 0, sC_s_cnt_LineBufferSize );
		memset( &sEffectAsset, 0, sC_s_cnt_LineBufferSize );
		memset( &sEffectFilename, 0, sC_s_cnt_LineBufferSize );
		memset( &sSlop, 0, sC_s_cnt_LineBufferSize );
		
		fsInput.getline( sLineBuffer, sC_s_cnt_LineBufferSize );

		// strip white spaces
		int spaces = 0;
		while(sLineBuffer[spaces]=='\t' || sLineBuffer[spaces]==' ')
			spaces++;
		strcpy(sLineBuffer, sLineBuffer+spaces);
		// Parse our line buffer

		// Ignore empty lines
		if ( 0 == strlen( sLineBuffer ) )
			continue;
		// Ignore comment lines
		else if ( sC_s_ch_CommentPrefix == sLineBuffer[0] )
			continue; 

		// Parse da meat
		int nParseResult = sscanf( sLineBuffer, "%s%*[ \t]%s%*[ \t]%s%*[ \t]%s", 
								  &sEffectName, &sEffectAsset, &sEffectFilename, &sSlop );
		if ( 2 > nParseResult )
		{
			sCurrBlock = sEffectName;
			continue; // Start or end of block
		}
		if(!IsValidParseBlock(sCurrBlock.c_str()))
			continue;

		std::string sEffectPath = Config::GetEffectRulesRoot() + "\\" + sEffectFilename + ".effectrule";
		try
		{
			// Construct and push our new effect (after creating full filename)
			m_vEffectPaths.push_back(std::string(sEffectPath));
			cXmlEffectFx* pEffect = new cXmlEffectFx( sEffectName, sEffectAsset, sEffectPath );
			//assert( pEffect );
			m_vParsedEffects.push_back( pEffect );
		}
		catch ( exEntityFxException& ex )
		{
			// Lets not bug the user for every missing .effectrule file...
#if 0
			DisplayError( "rageMaxParticle", 
						  "Exception loading: %s\n\r"
						  "This effect will not be available to the Particle Helper object.\n\r\n\r"
						  "Exception message: %s", 
						  sEffectPath.c_str(), ex.what() );
#endif
		}
	}

	// Close EntityFx.dat
	fsInput.close( );
}

// --- exEntityFxException Code -------------------------------------------------

exEntityFxException::exEntityFxException( const std::string& sMessage )
	: std::exception( sMessage.c_str() )
{
}

exEntityFxException::~exEntityFxException( )
{
}

// End of file
