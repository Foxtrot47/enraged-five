//
// filename:	EntityFx.h
// description:	EntityFx.dat Parser
//

#ifndef INC_ENTITYFX_H_
#define INC_ENTITYFX_H_

// --- Include Files ------------------------------------------------------------

// STL headers
#include <cstdarg>
#include <string>
#include <vector>
#include "IFPULog.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// Forward declarations
class cEntityFxEffect;
// namespace XmlEffectFx
// {
	class cXmlEffectFx;
//}

/**
* @brief EntityFx.dat Parser Class
*
* This object is responsible for parsing the entityFx.dat file
*/
class cEntityFx
{
public:
	typedef std::vector<cXmlEffectFx*>	tContainer;
	typedef tContainer::iterator					itContainer;
	typedef tContainer::const_iterator				itcContainer;

	cEntityFx( );
	cEntityFx( const char* sFilename, bool loading=false);
	cEntityFx( const std::string& sFilename, bool loading=false);
	~cEntityFx( );

	void CheckEffectsAreFilled();

	itContainer		EffectBegin( );
	itContainer		EffectEnd( );
	itcContainer	EffectBegin( ) const;
	itcContainer	EffectEnd( ) const;

	size_t			RetNumEffects( ) const;

	cXmlEffectFx* FindEffect( const char* sEffectName ) const;
	cXmlEffectFx* FindEffect( const std::string& sEffectName ) const;

	static void		ParserError(std::string msg);

	static std::vector<const char *>&	EffectBlocks()
	{
		return s_vValidEffectBlocks;
	}

private:
	void			ParseFile( const std::string& sFilename );

	std::string					m_sFilename;		//!< Filename parsed	
	tContainer					m_vParsedEffects;	//!< Vector of parsed effects
	std::vector<std::string>	m_vEffectPaths;

	bool IsValidParseBlock(const char *blockTag)
	{
		std::vector<const char *>::const_iterator it = s_vValidEffectBlocks.begin();
		for(;it!=s_vValidEffectBlocks.end();++it)
		{
			if(0==strcmp(blockTag, (*it)))
				return true;
		}
		return false;
	}
public:
	static std::vector<std::string>	s_vParseErrors;
	static std::vector<const char *> s_vValidEffectBlocks;
};

#endif // !INC_ENTITYFX_H_
