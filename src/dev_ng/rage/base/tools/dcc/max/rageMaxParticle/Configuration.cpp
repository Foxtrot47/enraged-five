//
// filename:	Configuration.cpp
// description:	Configuration Data for RAGE Particle plugin
//

// --- Include Files ------------------------------------------------------------

// Local headers
#include "Configuration.h"
#include "resource.h"
#include "Win32Res.h"


// --- Using Directives ---------------------------------------------------------

using namespace rage;

// --- Globals ------------------------------------------------------------------

extern HINSTANCE g_hInstance;

// --- Config Code -------------------------------------------------

std::string Config::m_entityFxFile		= "";
std::string Config::m_effectRoot		= "";
std::string Config::m_effectRulesRoot	= "";
std::string Config::m_emitRulesRoot		= "";

/**
 * @brief Gets all configuration settings from the module settings, throwing
 * an exception if any module setting isn't found.
 */
void Config::Init()
{

}

// End of file
