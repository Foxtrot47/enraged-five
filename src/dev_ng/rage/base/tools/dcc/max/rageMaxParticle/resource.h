//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Particle.rc
//
#define IDS_RAGEPARTICLE_CATEGORY       101
#define IDS_RAGEPARTICLE_LIBDESCRIPTION 102
#define IDS_RAGEPARTICLE_CLASSNAME      103
#define ERR_ENTITYFX_FILE_OPEN          104
#define ERR_ENTITYFX_FILE_READ          105
#define IDS_MODULESETTING_ENTITYFX      106
#define IDS_MODULESETTING_EFFECTROOT    107
#define IDS_MODULESETTING_EFFECTRULESROOT 108
#define IDS_MODULESETTING_EMITRULESROOT 109

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
