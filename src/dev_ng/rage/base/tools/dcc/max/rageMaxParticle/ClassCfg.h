/*	ClassCfg.h - local class & generic configuration file for MAXScript SDK plugins
 *
 *  John Wainwright, 1998
 */

#ifdef MS_LOCAL_ROOT_CLASS
#undef MS_LOCAL_ROOT_CLASS
#endif

#ifdef MS_LOCAL_GENERIC_CLASS
#undef MS_LOCAL_GENERIC_CLASS
#endif

//#ifdef EFFECT_NAMEPSPACE_EFFECT
	// local root Value class for this plug-in
	#define MS_LOCAL_ROOT_CLASS		cXmlEffectFx

	// local Generic function class
	#define MS_LOCAL_GENERIC_CLASS	cXmlEffectFxGeneric
// #else //if defined(EFFECT_NAMEPSPACE_EMITTER)
// 	// local root Value class for this plug-in
// 	#define MS_LOCAL_ROOT_CLASS		cXmlEmitFx
// 
// 	// local Generic function class
// 	#define MS_LOCAL_GENERIC_CLASS	cXmlEmitFxGeneric
// #endif