#include "converter.h"
#include "utility.h"

#include "skeleton/skeleton.h"

bool IsValidNode(atArray<atString> atFilters, atString atNodeName)
{
	for(int i=0; i < atFilters.GetCount(); ++i)
	{
		if(atFilters[i].StartsWith("*") && atFilters[i].EndsWith("*"))
		{
			atString atFilter = atFilters[i];
			atFilter.Replace("*", "");

			if(atNodeName.IndexOf(atFilter) != -1)
			{
				return false;
			}
		}
		else if(atFilters[i].StartsWith("*"))
		{
			atString atFilter = atFilters[i];
			atFilter.Replace("*", "");

			if(atNodeName.EndsWith(atFilter))
			{
				return false;
			}
		}
		else if(atFilters[i].EndsWith("*"))
		{
			atString atFilter = atFilters[i];
			atFilter.Replace("*", "");

			if(atNodeName.StartsWith(atFilter))
			{
				return false;
			}
		}
		else
		{
			atString atFilter = atFilters[i];
			atFilter.Replace("*", "");

			if(atNodeName.IndexOf(atFilter) != -1)
			{
				return false;
			}
		}
	}

	return true;
}

bool SkeletonConverter::Convert(INode* pNode, const char* pSkeleton, const char* pPath, const atArray<atString>& atFilters)
{
	ObjectSkeleton* newSkeleton = rage_new ObjectSkeleton;
	newSkeleton->SetName(pSkeleton);
	newSkeleton->m_SkeletonType = "skel";

	ObjectSkeleton::Bone* pBone = new ObjectSkeleton::Bone();

	Recurse(pNode, pBone);

	newSkeleton->m_RootBone = pBone;

	rage::Skeleton skel;
	skel.m_Name = pSkeleton;

	RecurseParcodegen(newSkeleton->m_RootBone, skel.GetRoot(), true, atFilters);

	skel.m_NumberOfBones = newSkeleton->GetBoneCount();
	return skel.Save(pPath);
}

bool SkeletonConverter::WriteBoneList(INode* pNode, const char* pPath)
{
	rage::BoneList boneList;

	ObjectSkeleton::Bone* pBone = new ObjectSkeleton::Bone();

	Recurse(pNode, pBone);

	AddToBoneList(pBone, boneList);

	return boneList.Save(pPath);
}

void SkeletonConverter::AddToBoneList(ObjectSkeleton::Bone* bone, BoneList& boneList)
{
	rage::Bone* pBone = rage_new rage::Bone();
	pBone->SetName(bone->m_Name.c_str());
	pBone->SetAlias(bone->m_BoneID.c_str());
	pBone->SetHash(atHash16U(bone->m_BoneID.c_str()));

	boneList.m_Bones.PushAndGrow(*pBone);

	for(int i=0; i < bone->m_Children.GetCount(); ++i)
	{
		AddToBoneList(bone->m_Children[i], boneList);
	}
}

void SkeletonConverter::RecurseParcodegen(ObjectSkeleton::Bone* pCurrentBone, rage::BoneInstance* pParent, bool bRoot, const atArray<atString>& atFilters)
{
	rage::BoneInstance* pBone = rage_new rage::BoneInstance();
	
	pBone->m_Bone.SetName(pCurrentBone->m_Name.c_str());
	pBone->m_Bone.SetAlias(pCurrentBone->m_BoneID.c_str());
	pBone->m_Bone.SetHash(atHash16U(pCurrentBone->m_BoneID.c_str()));
	pBone->SetScale(pCurrentBone->m_ScaleDof);
	pBone->SetTranslation(pCurrentBone->m_TranslationDof);
	pBone->SetRotation(pCurrentBone->m_RotationDof);
	pBone->SetTransform(pCurrentBone->GetBoneMatrix());

	for(int i=0; i < pCurrentBone->m_Children.GetCount(); ++i)
	{
		RecurseParcodegen(pCurrentBone->m_Children[i], pBone, false, atFilters);
	}

	//atArray<atString> nodes;
	//nodes.PushAndGrow(atString("*ROOT"));
	//nodes.PushAndGrow(atString("SKEL*"));

	//bool b1 = IsValidNode(nodes, atString("SKEL_ROOT"));
	//bool b2 = IsValidNode(nodes, atString("PELVIS_SKEL"));
	//bool b3 = IsValidNode(nodes, atString("tim"));
	//bool b4 = IsValidNode(nodes, atString("S_ROOT"));

	if(bRoot)
		*pParent = *pBone;
	else
		if(IsValidNode(atFilters, pBone->m_Bone.GetName()))
			pParent->AddChild(*pBone);
}

void SkeletonConverter::Recurse(INode* pNode, ObjectSkeleton::Bone* pBone)
{
	pBone->m_Name = pNode->GetName();
	pBone->m_IsJoint = true;

	pBone->m_BoneID = Utility::GetBoneID(pNode);

	Matrix3 nodeTM = pNode->GetNodeTM(0);
	INode* parent = pNode->GetParentNode();
	Matrix3 parentTM = parent->GetNodeTM(0);
	Matrix3 localTM = nodeTM*Inverse(parentTM);
	
	Matrix34 MatTemp;
	MatTemp.Identity();
	Utility::CopyMaxMatrixToRageMatrix(localTM,MatTemp);

	pBone->SetBoneMatrix(MatTemp);
	
	pBone->m_Offset = MatTemp.GetVector(3);

	pBone->m_RotationDof = (Utility::GetBoneRotation(pNode) == "true");
	pBone->m_TranslationDof = (Utility::GetBoneTranslation(pNode) == "true");
	pBone->m_ScaleDof = (Utility::GetBoneScale(pNode) == "true");

	//if(IsRoot /*&& m_RootAtOrigin*/)
	//{
	//	pBone->m_Offset = Vector3(0.0f,0.0f,0.0f);
	//}

	pBone->m_JointOrient = MatTemp;
	pBone->m_JointOrient.d.Zero();

	s32 i,ChildCount = pNode->NumberOfChildren();

	for(i=0;i<ChildCount;i++)
	{
		INode* p_ChildNode = pNode->GetChildNode(i);

		//if(rexConverterMaxSkeleton::IsValidSkelBone(p_ChildNode, &res))
		{
			ObjectSkeleton::Bone* pChildBone = pBone->AddChild(p_ChildNode->GetName(),M34_IDENTITY);
			Recurse(p_ChildNode,pChildBone);
		}
	}
}