#include "utility.h"
#include <string>
#include <algorithm>
#include <sstream>

static const s32 TicksPerSec = 4800;

namespace Utility
{

void CopyMaxMatrixToRageMatrix(const Matrix3& r_SrcMat,Matrix34& r_DstMat)
{
	Point4 SrcRow;

	SrcRow = r_SrcMat.GetRow(0);
	r_DstMat.a = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(1);
	r_DstMat.b = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(2);
	r_DstMat.c = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(3);
	r_DstMat.d = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
}

void CopyRageMatrixToMaxMatrix(const Matrix34& r_SrcMat,Matrix3& r_DstMat)
{
	Point3 SrcRow;

	SrcRow = Point3(r_SrcMat.a.x,r_SrcMat.a.y,r_SrcMat.a.z);
	r_DstMat.SetRow(0,SrcRow);
	SrcRow = Point3(r_SrcMat.b.x,r_SrcMat.b.y,r_SrcMat.b.z);
	r_DstMat.SetRow(1,SrcRow);
	SrcRow = Point3(r_SrcMat.c.x,r_SrcMat.c.y,r_SrcMat.c.z);
	r_DstMat.SetRow(2,SrcRow);
	SrcRow = Point3(r_SrcMat.d.x,r_SrcMat.d.y,r_SrcMat.d.z);
	r_DstMat.SetRow(3,SrcRow);
}

INode* GetParent(INode* p_Node)
{
	if(!p_Node || p_Node == GetCOREInterface()->GetRootNode())
		return NULL;

	INode* p_ParentNode = p_Node->GetParentNode();

	if(p_ParentNode == GetCOREInterface()->GetRootNode())
	{
		return NULL;
	}

	return p_ParentNode;
}

void GetLocalMatrix(INode* p_Node,f32 Time,Matrix34& r_Local,bool WithObjectTM)
{
	TimeValue TimeVal = (int)(Time * TicksPerSec);
	INode* p_ParentNode = GetParent(p_Node);

	Matrix34 MatObj;																																																																																																		
	Matrix34 MatParent;
		
	CopyMaxMatrixToRageMatrix(p_Node->GetObjectTM(TimeVal),MatObj);

	MatParent.Identity();

	if(p_ParentNode)
	{
		CopyMaxMatrixToRageMatrix(p_ParentNode->GetNodeTM(TimeVal),MatParent);
	}

	// Use this method to calculate the local, dottranspose does a subtract which fucks the scale
	MatParent.Inverse();
	r_Local.Dot(MatObj,MatParent);
	//r_Local.DotTranspose(MatObj,MatParent);
}

bool GetUserPropString(INode* pNode, const char* pEntry, TSTR& string)
{
	std::string entry(pEntry);
	std::transform(entry.begin(), entry.end(), entry.begin(), ::tolower);

	MSTR buffer;
	pNode->GetUserPropBuffer(buffer);

	std::string line;
	std::stringstream stream;
	stream.str(buffer.data());
	while( getline(stream, line) )
	{
		std::string lowercaseline = line;
		std::transform(lowercaseline.begin(), lowercaseline.end(), lowercaseline.begin(), ::tolower);

		// make sure the line starts with the entry we are looking for
		if(lowercaseline.find(entry) == 0)
		{
			int index = (int)line.find("=");
			if(index != -1)
			{
				std::string data = line.substr(index+1, line.length());
				data.erase(remove_if(data.begin(), data.end(), isspace), data.end());
				string = data.c_str();
				return true;
			}
		}
	}

	return false;
}

atString GetBoneID(INode* p_BoneNode)
{
	TSTR Prop;

	if(!GetUserPropString(p_BoneNode, "tag", Prop))
	{
		return atString("");
	}

	if(strcmp(Prop,"0") == 0)
	{
		return atString("#0");
	}

	return atString(Prop);
}

atString GetBoneTranslation(INode* p_BoneNode)
{
	TSTR Prop;

	if(!GetUserPropString(p_BoneNode, "exportTrans", Prop))
	{
		return atString("");
	}

	std::string entry(Prop);
	std::transform(entry.begin(), entry.end(), entry.begin(), ::tolower);

	return atString(entry.c_str());
}

atString GetBoneScale(INode* p_BoneNode)
{
	TSTR Prop;

	if(!GetUserPropString(p_BoneNode, "exportScale", Prop))
	{
		return atString("");
	}

	std::string entry(Prop);
	std::transform(entry.begin(), entry.end(), entry.begin(), ::tolower);

	return atString(entry.c_str());
}

atString GetBoneRotation(INode* p_BoneNode)
{
	TSTR Prop;

	if(!GetUserPropString(p_BoneNode, "exportRot", Prop))
	{
		return atString("");
	}

	std::string entry(Prop);
	std::transform(entry.begin(), entry.end(), entry.begin(), ::tolower);

	return atString(entry.c_str());
}

}