#include <maxscript/maxscript.h>
#include <maxscript/maxwrapper/mxsmaterial.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/foundation/Numbers.h>
#include <maxscript/foundation/structs.h>
#include <maxscript/foundation/hashtable.h>
#include <maxscript/foundation/3dmath.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
// GD actually this now last, to override the def_name macro to define not as external Value.

#include "converter.h"
#include "skeletonPlugin.h"
#include "utility.h"

def_visible_primitive(Skeleton_Serialise,"Skeleton_Serialise");
def_visible_primitive(Skeleton_Load,"Skeleton_Load");
def_visible_primitive(Skeleton_GetRootHash, "Skeleton_GetRootHash");
def_visible_primitive(Skeleton_GetChildHash, "Skeleton_GetChildHash");
def_visible_primitive(Skeleton_GetBoneNameByHash, "Skeleton_GetBoneNameByHash");
def_visible_primitive(Skeleton_GetNumberOfChildrenByHash, "Skeleton_GetNumberOfChildrenByHash");
def_visible_primitive(Skeleton_GetTransformByHash,"Skeleton_GetTransformByHash");
def_visible_primitive(Skeleton_GetDOFScaleByHash, "Skeleton_GetDOFScaleByHash");
def_visible_primitive(Skeleton_GetDOFTranslationByHash, "Skeleton_GetDOFTranslationByHash");
def_visible_primitive(Skeleton_GetDOFRotationByHash, "Skeleton_GetDOFRotationByHash");

def_visible_primitive(SkeletonBoneList_Serialise,"SkeletonBoneList_Serialise");
def_visible_primitive(SkeletonBoneList_Load,"SkeletonBoneList_Load");
def_visible_primitive(SkeletonBoneList_GetNumBones,"SkeletonBoneList_GetNumBones");
def_visible_primitive(SkeletonBoneList_GetBoneNameByIndex,"SkeletonBoneList_GetBoneNameByIndex");
def_visible_primitive(SkeletonBoneList_GetBoneHashByIndex,"SkeletonBoneList_GetBoneHashByIndex");

def_visible_primitive(SkeletonAttributes_Reset,"SkeletonAttributes_Reset");
def_visible_primitive(SkeletonAttributes_Load,"SkeletonAttributes_Load");
def_visible_primitive(SkeletonAttributes_Save,"SkeletonAttributes_Save");
def_visible_primitive(SkeletonAttributes_GetNumBones,"SkeletonAttributes_GetNumBones");
def_visible_primitive(SkeletonAttributes_GetNumAttributesOnBone,"SkeletonAttributes_GetNumAttributesOnBone");
def_visible_primitive(SkeletonAttributes_GetAttributeNameByIndex,"SkeletonAttributes_GetAttributeNameByIndex");
def_visible_primitive(SkeletonAttributes_GetAttributeValueByIndex,"SkeletonAttributes_GetAttributeValueByIndex");
def_visible_primitive(SkeletonAttributes_GetAttributeValueByName,"SkeletonAttributes_GetAttributeValueByName");
def_visible_primitive(SkeletonAttributes_AddAttribute,"SkeletonAttributes_AddAttribute");

def_visible_primitive(SkeletonAttributes_AddRootAttribute,"SkeletonAttributes_AddRootAttribute");
def_visible_primitive(SkeletonAttributes_GetNumRootAttributes,"SkeletonAttributes_GetNumRootAttributes");
def_visible_primitive(SkeletonAttributes_GetRootAttributeNameByIndex,"SkeletonAttributes_GetRootAttributeNameByIndex");
def_visible_primitive(SkeletonAttributes_GetRootAttributeValueByIndex,"SkeletonAttributes_GetRootAttributeValueByIndex");
def_visible_primitive(SkeletonAttributes_GetRootAttributeValueByName,"SkeletonAttributes_GetRootAttributeValueByName");

def_visible_primitive(SkeletonAttributes_AddBone,"SkeletonAttributes_AddBone");
def_visible_primitive(SkeletonAttributes_GetBoneNameByIndex,"SkeletonAttributes_GetBoneNameByIndex");
def_visible_primitive(SkeletonAttributes_GetBoneHashByIndex,"SkeletonAttributes_GetBoneHashByIndex");
def_visible_primitive(SkeletonAttributes_GetBoneIndexByName,"SkeletonAttributes_GetBoneIndexByName");
def_visible_primitive(SkeletonAttributes_GetBoneIndexByHash,"SkeletonAttributes_GetBoneIndexByHash");

void SplitString(const char* str, atArray<atString>& vec)
{
	char* pch = strtok ((char*)str,",");
	while (pch != NULL)
	{
		atString p(pch);
		vec.PushAndGrow(p);
		pch = strtok (NULL, ",");
	}
}

Value* Skeleton_Serialise_cf(Value** arg_list, int count)
{
	check_arg_count(Skeleton_Serialise, 3, count);
	type_check(arg_list[0],MAXNode,"rageMaxSkeleton [root node to traverse]");
	type_check(arg_list[1], String, "rageMaxSkeleton [skeleton path]");
	type_check(arg_list[2], String, "rageMaxSkeleton [to filter]");

	INode* p_Node = arg_list[0]->to_node();
	const char* p_Path = arg_list[1]->to_string();

	char cPath[RAGE_MAX_PATH];
	ASSET.RemoveExtensionFromPath(cPath, RAGE_MAX_PATH, p_Path);

	const char* p_Name = ASSET.FileName(cPath);

	atArray<atString> atFilters;
	SplitString(arg_list[2]->to_string(), atFilters);

	SkeletonConverter conv;
	return conv.Convert(p_Node, p_Name, cPath, atFilters) ? &true_value : &false_value;
}

Value* Skeleton_Load_cf(Value** arg_list, int count)
{
	check_arg_count(Skeleton_Load, 1, count);
	type_check(arg_list[0], String, "rageMaxSkeleton [filename]");

	if(!skeletonPlugin::GetInst().GetSkeleton().Load(arg_list[0]->to_string()))
	{
		return &false_value;
	}

	return &true_value;
};

Value* Skeleton_GetRootHash_cf(Value** arg_list, int count)
{
	check_arg_count(Skeleton_GetRootHash, 0, count);

	return_protected(new Integer64(skeletonPlugin::GetInst().GetSkeleton().GetRoot()->m_Bone.GetHash()));
};

Value* Skeleton_GetTransformByHash_cf(Value** arg_list, int count)
{
	check_arg_count(Skeleton_GetTransformByHash, 1, count);
	type_check(arg_list[0], Integer64, "rageMaxSkeleton [hash]");

	BoneInstance bone;
	if(!skeletonPlugin::GetInst().GetSkeleton().FindBone(arg_list[0]->to_int64(), bone))
	{
		return &undefined;
	}

	Matrix3 mat;
	Utility::CopyRageMatrixToMaxMatrix(bone.GetTransform(), mat);

	return_protected(new Matrix3Value(mat));
};

Value* Skeleton_GetNumberOfChildrenByHash_cf(Value** arg_list, int count)
{
	check_arg_count(Skeleton_GetNumberOfChildrenByHash, 1, count);
	type_check(arg_list[0], Integer64, "rageMaxSkeleton [hash]");

	BoneInstance bone;
	if(!skeletonPlugin::GetInst().GetSkeleton().FindBone(arg_list[0]->to_int64(), bone))
	{
		return &undefined;
	}

	return_protected(new Integer(bone.GetNumChildren()));
};

Value* Skeleton_GetChildHash_cf(Value** arg_list, int count)
{
	check_arg_count(Skeleton_GetNumberOfChildren, 2, count);
	type_check(arg_list[0], Integer64, "rageMaxSkeleton [hash]");
	type_check(arg_list[1], Integer, "rageMaxSkeleton [index bone]");

	BoneInstance bone;
	if(!skeletonPlugin::GetInst().GetSkeleton().FindBone(arg_list[0]->to_int64(), bone))
	{
		return &undefined;
	}

	if(arg_list[1]->to_int() >= bone.GetNumChildren()) return &undefined;

	BoneInstance child = bone.GetChild(arg_list[1]->to_int());

	return_protected(new Integer64(child.m_Bone.GetHash()));
};

Value* Skeleton_GetBoneNameByHash_cf(Value** arg_list, int count)
{
	check_arg_count(Skeleton_GetBoneNameByHash, 1, count);
	type_check(arg_list[0], Integer64, "rageMaxSkeleton [hash]");

	BoneInstance bone;
	if(!skeletonPlugin::GetInst().GetSkeleton().FindBone(arg_list[0]->to_int64(), bone))
	{
		return &undefined;
	}

	return_protected(new String(bone.m_Bone.GetName().c_str()));
};

Value* Skeleton_GetDOFScaleByHash_cf(Value** arg_list, int count)
{
	check_arg_count(Skeleton_GetDOFScaleByHash, 1, count);
	type_check(arg_list[0], Integer64, "rageMaxSkeleton [hash]");

	BoneInstance bone;
	if(!skeletonPlugin::GetInst().GetSkeleton().FindBone(arg_list[0]->to_int64(), bone))
	{
		return &undefined;
	}

	return (bone.GetScale() == true) ? &true_value : &false_value;
};

Value* Skeleton_GetDOFTranslationByHash_cf(Value** arg_list, int count)
{
	check_arg_count(Skeleton_GetDOFTranslationByHash, 1, count);
	type_check(arg_list[0], Integer64, "rageMaxSkeleton [hash]");

	BoneInstance bone;
	if(!skeletonPlugin::GetInst().GetSkeleton().FindBone(arg_list[0]->to_int64(), bone))
	{
		return &undefined;
	}

	return (bone.GetTranslation() == true) ? &true_value : &false_value;
};

Value* Skeleton_GetDOFRotationByHash_cf(Value** arg_list, int count)
{
	check_arg_count(Skeleton_GetDOFRotationByHash, 1, count);
	type_check(arg_list[0], Integer64, "rageMaxSkeleton [hash]");

	BoneInstance bone;
	if(!skeletonPlugin::GetInst().GetSkeleton().FindBone(arg_list[0]->to_int64(), bone))
	{
		return &undefined;
	}

	return (bone.GetRotation() == true) ? &true_value : &false_value;
};

Value* SkeletonBoneList_Serialise_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonBoneList_Serialise, 2, count);
	type_check(arg_list[0],MAXNode,"rageMaxSkeleton [root node to traverse]");
	type_check(arg_list[1], String, "rageMaxSkeleton [bone list path]");

	INode* p_Node = arg_list[0]->to_node();
	const char* p_Path = arg_list[1]->to_string();

	SkeletonConverter conv;
	return conv.WriteBoneList(p_Node, p_Path) ? &true_value : &false_value;
}

Value* SkeletonBoneList_Load_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonBoneList_Load, 1, count);
	type_check(arg_list[0], String, "rageMaxSkeleton [filename]");

	if(!skeletonPlugin::GetInst().GetBoneList().Load(arg_list[0]->to_string()))
	{
		return &false_value;
	}

	return &true_value;
};

Value* SkeletonBoneList_GetNumBones_cf(Value** arg_list, int count)
{
	int num = skeletonPlugin::GetInst().GetBoneList().m_Bones.GetCount();

	return_protected(new Integer(num));
};

Value* SkeletonBoneList_GetBoneNameByIndex_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonBoneList_GetBoneNameByIndex, 1, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index bone]");

	if(arg_list[0]->to_int() >= skeletonPlugin::GetInst().GetBoneList().m_Bones.GetCount())
		return &undefined;

	return_protected(new String(skeletonPlugin::GetInst().GetBoneList().m_Bones[arg_list[0]->to_int()].GetName().c_str()));
}

Value* SkeletonBoneList_GetBoneHashByIndex_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonBoneList_GetBoneHashByIndex, 1, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index bone]");

	if(arg_list[0]->to_int() >= skeletonPlugin::GetInst().GetBoneList().m_Bones.GetCount())
		return &undefined;

	return_protected(new Integer64(skeletonPlugin::GetInst().GetBoneList().m_Bones[arg_list[0]->to_int()].GetHash()));
}

Value* SkeletonAttributes_Reset_cf(Value** arg_list, int count)
{
	skeletonPlugin::GetInst().GetAttributeList().Reset();

	return &true_value;
};

Value* SkeletonAttributes_Load_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_Load, 1, count);
	type_check(arg_list[0], String, "rageMaxSkeleton [filename]");

	if(!skeletonPlugin::GetInst().GetAttributeList().Load(arg_list[0]->to_string()))
	{
		return &false_value;
	}

	return &true_value;
};

Value* SkeletonAttributes_Save_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_Save, 1, count);
	type_check(arg_list[0], String, "rageMaxSkeleton [filename]");

	skeletonPlugin::GetInst().GetAttributeList().Save(arg_list[0]->to_string());

	return &true_value;
};

Value* SkeletonAttributes_GetNumBones_cf(Value** arg_list, int count)
{
	int num = skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount();

	return_protected(new Integer(num));
};

Value* SkeletonAttributes_GetNumRootAttributes_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetNumRootAttributes, 0, count);

	int num = skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.GetNumUsed();

	return_protected(new Integer(num));
};

Value* SkeletonAttributes_GetRootAttributeNameByIndex_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetRootAttributeNameByIndex, 1, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index attr]");

	int i=0;
	atMap<atHashString, AttributeValue>::Iterator entry = skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		if(i == arg_list[0]->to_int())
		{
			return_protected(new String(entry.GetKey().GetCStr()));
		}
		i++;
	}

	return &undefined;
};

Value* SkeletonAttributes_GetRootAttributeValueByIndex_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetRootAttributeValueByIndex, 1, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index attr]");

	int i=0;
	atMap<atHashString, AttributeValue>::Iterator entry = skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		if(i == arg_list[0]->to_int())
		{
			if(entry.GetData().m_Type == FloatType)
			{
				return new Float(atof(entry.GetData().m_Value));
			}
			else if(entry.GetData().m_Type == IntegerType)
			{
				return new Integer(atoi(entry.GetData().m_Value));
			}
			else if(entry.GetData().m_Type == StringType)
			{
				return new String(entry.GetData().m_Value.c_str());
			}
			else if(entry.GetData().m_Type == BoolType)
			{
				if(stricmp(entry.GetData().m_Value.c_str(), "true") == 0)
				{
					return &true_value;
				}
				else if(stricmp(entry.GetData().m_Value.c_str(), "false") == 0) 
				{
					return &false_value;
				}
			}
			else if(entry.GetData().m_Type == UndefinedType)
			{
				return &undefined;
			}
			else if(entry.GetData().m_Type == Point3Type)
			{
				atString strBuf(entry.GetData().m_Value.c_str());
				atArray<atString> split;
				strBuf.Split( split, ',' );

				if(split.GetCount() != 3)
				{
					return &undefined;
				}

				return new Point3Value(atof(split[0].c_str()),atof(split[1].c_str()),atof(split[2].c_str()));
			}
			else if(entry.GetData().m_Type == Matrix3Type)
			{
				atString strBuf(entry.GetData().m_Value.c_str());
				atArray<atString> split;
				strBuf.Split( split, ',' );

				if(split.GetCount() != 12)
				{
					return &undefined;
				}

				return new Matrix3Value(Point3(atof(split[0].c_str()),atof(split[1].c_str()),atof(split[2].c_str())),
					Point3(atof(split[3].c_str()),atof(split[4].c_str()),atof(split[5].c_str())),
					Point3(atof(split[6].c_str()),atof(split[7].c_str()),atof(split[8].c_str())), 
					Point3(atof(split[9].c_str()),atof(split[10].c_str()),atof(split[11].c_str())));
			}
		}
		i++;
	}

	return &undefined;
};

Value* SkeletonAttributes_GetRootAttributeValueByName_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetRootAttributeValueByName, 1, count);
	type_check(arg_list[0], String, "rageMaxSkeleton [attr name]");

	AttributeValue* pAttrValue = skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.Access(arg_list[0]->to_string());
	if(pAttrValue)
	{
		if(pAttrValue->m_Type == FloatType)
		{
			return new Float(atof(pAttrValue->m_Value));
		}
		else if(pAttrValue->m_Type == IntegerType)
		{
			return new Integer(atoi(pAttrValue->m_Value));
		}
		else if(pAttrValue->m_Type == StringType)
		{
			return new String(pAttrValue->m_Value.c_str());
		}
		else if(pAttrValue->m_Type == BoolType)
		{
			if(stricmp(pAttrValue->m_Value.c_str(), "true") == 0)
			{
				return &true_value;
			}
			else if(stricmp(pAttrValue->m_Value.c_str(), "false") == 0) 
			{
				return &false_value;
			}
		}
		else if(pAttrValue->m_Type == UndefinedType)
		{
			return &undefined;
		}
		else if(pAttrValue->m_Type == Point3Type)
		{
			atString strBuf(pAttrValue->m_Value.c_str());
			atArray<atString> split;
			strBuf.Split( split, ',' );

			if(split.GetCount() != 3)
			{
				return &undefined;
			}

			return new Point3Value(atof(split[0].c_str()),atof(split[1].c_str()),atof(split[2].c_str()));
		}
		else if(pAttrValue->m_Type == Matrix3Type)
		{
			atString strBuf(pAttrValue->m_Value.c_str());
			atArray<atString> split;
			strBuf.Split( split, ',' );

			if(split.GetCount() != 12)
			{
				return &undefined;
			}

			return new Matrix3Value(Point3(atof(split[0].c_str()),atof(split[1].c_str()),atof(split[2].c_str())),
				Point3(atof(split[3].c_str()),atof(split[4].c_str()),atof(split[5].c_str())),
				Point3(atof(split[6].c_str()),atof(split[7].c_str()),atof(split[8].c_str())), 
				Point3(atof(split[9].c_str()),atof(split[10].c_str()),atof(split[11].c_str())));
		}
	}

	return &undefined;
};

Value* SkeletonAttributes_GetNumAttributesOnBone_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetNumAttributesOnBone, 1, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index]");

	int num = skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.GetNumUsed();
	
	return_protected(new Integer(num));
};

Value* SkeletonAttributes_GetAttributeNameByIndex_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetAttributeNameByIndex, 2, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index bone]");
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index attr]");

	if(arg_list[0]->to_int() >= skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount())
		return &undefined;

	int i=0;
	atMap<atHashString, AttributeValue>::Iterator entry = skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		if(i == arg_list[1]->to_int())
		{
			return_protected(new String(entry.GetKey().GetCStr()));
		}
		i++;
	}

	return &undefined;
};

Value* SkeletonAttributes_GetAttributeValueByIndex_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetAttributeValueByIndex, 2, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index bone]");
	type_check(arg_list[1], Integer, "rageMaxSkeleton [index attr]");

	if(arg_list[0]->to_int() >= skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount())
		return &undefined;

	int i=0;
	atMap<atHashString, AttributeValue>::Iterator entry = skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		if(i == arg_list[1]->to_int())
		{
			if(entry.GetData().m_Type == FloatType)
			{
				return new Float(atof(entry.GetData().m_Value));
			}
			else if(entry.GetData().m_Type == IntegerType)
			{
				return new Integer(atoi(entry.GetData().m_Value));
			}
			else if(entry.GetData().m_Type == StringType)
			{
				return new String(entry.GetData().m_Value.c_str());
			}
			else if(entry.GetData().m_Type == BoolType)
			{
				if(stricmp(entry.GetData().m_Value.c_str(), "true") == 0)
				{
					return &true_value;
				}
				else if(stricmp(entry.GetData().m_Value.c_str(), "false") == 0) 
				{
					return &false_value;
				}
			}
			else if(entry.GetData().m_Type == UndefinedType)
			{
				return &undefined;
			}
			else if(entry.GetData().m_Type == Point3Type)
			{
				atString strBuf(entry.GetData().m_Value.c_str());
				atArray<atString> split;
				strBuf.Split( split, ',' );

				if(split.GetCount() != 3)
				{
					return &undefined;
				}

				return new Point3Value(atof(split[0].c_str()),atof(split[1].c_str()),atof(split[2].c_str()));
			}
			else if(entry.GetData().m_Type == Matrix3Type)
			{
				atString strBuf(entry.GetData().m_Value.c_str());
				atArray<atString> split;
				strBuf.Split( split, ',' );

				if(split.GetCount() != 12)
				{
					return &undefined;
				}

				return new Matrix3Value(Point3(atof(split[0].c_str()),atof(split[1].c_str()),atof(split[2].c_str())),
					Point3(atof(split[3].c_str()),atof(split[4].c_str()),atof(split[5].c_str())),
					Point3(atof(split[6].c_str()),atof(split[7].c_str()),atof(split[8].c_str())), 
					Point3(atof(split[9].c_str()),atof(split[10].c_str()),atof(split[11].c_str())));
			}
		}
		i++;
	}

	return &undefined;
};

Value* SkeletonAttributes_GetAttributeValueByName_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetAttributeValueByName, 2, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index bone]");
	type_check(arg_list[1], String, "rageMaxSkeleton [attr name]");

	if(arg_list[0]->to_int() >= skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount())
		return &undefined;

	AttributeValue* pAttrValue = skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.Access(arg_list[1]->to_string());
	if(pAttrValue)
	{
		if(pAttrValue->m_Type == FloatType)
		{
			return new Float(atof(pAttrValue->m_Value));
		}
		else if(pAttrValue->m_Type == IntegerType)
		{
			return new Integer(atoi(pAttrValue->m_Value));
		}
		else if(pAttrValue->m_Type == StringType)
		{
			return new String(pAttrValue->m_Value.c_str());
		}
		else if(pAttrValue->m_Type == BoolType)
		{
			if(stricmp(pAttrValue->m_Value.c_str(), "true") == 0)
			{
				return &true_value;
			}
			else if(stricmp(pAttrValue->m_Value.c_str(), "false") == 0) 
			{
				return &false_value;
			}
		}
		else if(pAttrValue->m_Type == UndefinedType)
		{
			return &undefined;
		}
		else if(pAttrValue->m_Type == Point3Type)
		{
			atString strBuf(pAttrValue->m_Value.c_str());
			atArray<atString> split;
			strBuf.Split( split, ',' );

			if(split.GetCount() != 3)
			{
				return &undefined;
			}

			return new Point3Value(atof(split[0].c_str()),atof(split[1].c_str()),atof(split[2].c_str()));
		}
		else if(pAttrValue->m_Type == Matrix3Type)
		{
			atString strBuf(pAttrValue->m_Value.c_str());
			atArray<atString> split;
			strBuf.Split( split, ',' );

			if(split.GetCount() != 12)
			{
				return &undefined;
			}

			return new Matrix3Value(Point3(atof(split[0].c_str()),atof(split[1].c_str()),atof(split[2].c_str())),
				Point3(atof(split[3].c_str()),atof(split[4].c_str()),atof(split[5].c_str())),
				Point3(atof(split[6].c_str()),atof(split[7].c_str()),atof(split[8].c_str())), 
				Point3(atof(split[9].c_str()),atof(split[10].c_str()),atof(split[11].c_str())));
		}
	}

	return &undefined;
};

Value* SkeletonAttributes_AddBone_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_AddBone, 1, count);
	type_check(arg_list[0], MAXNode, "rageMaxSkeleton [node]");

	INode* p_Node = arg_list[0]->to_node();

	BoneAttribute attr;
	attr.m_Name = p_Node->GetName();
	atString strId = Utility::GetBoneID(p_Node);
	attr.m_Hash = atHash16U(strId.c_str());
	if(strId == "")
	{
		attr.m_Hash = atHash16U(attr.m_Name.c_str());
	}
	
	for(int i=0; i < skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount(); ++i)
	{
		if(stricmp(skeletonPlugin::GetInst().GetAttributeList().m_Entries[i].m_Name.c_str(), p_Node->GetName()) == 0)
		{
			return new Integer(i);
		}
	}
	
	skeletonPlugin::GetInst().GetAttributeList().m_Entries.PushAndGrow(attr);

	return new Integer(skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount()-1);
}

Value* SkeletonAttributes_GetBoneNameByIndex_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetBoneNameByIndex, 1, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index]");

	if(arg_list[0]->to_int() >= skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount())
		return &undefined;

	return new String(skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Name.c_str());
}

Value* SkeletonAttributes_GetBoneHashByIndex_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetBoneHashByIndex, 1, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index]");

	if(arg_list[0]->to_int() >= skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount())
		return &undefined;

	return new Integer64(skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Hash);
}

Value* SkeletonAttributes_GetBoneIndexByName_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetBoneIndexByName, 1, count);
	type_check(arg_list[0], String, "rageMaxSkeleton [name]");

	for(int i=0; i < skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount(); ++i)
	{
		if(stricmp(skeletonPlugin::GetInst().GetAttributeList().m_Entries[i].m_Name.c_str(), arg_list[0]->to_string()) == 0)
		{
			return new Integer(i);
		}
	}

	return &undefined;
}

Value* SkeletonAttributes_GetBoneIndexByHash_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_GetBoneIndexByName, 1, count);
	type_check(arg_list[0], Integer64, "rageMaxSkeleton [hash]");

	for(int i=0; i < skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount(); ++i)
	{
		if(skeletonPlugin::GetInst().GetAttributeList().m_Entries[i].m_Hash == arg_list[0]->to_int64())
		{
			return new Integer(i);
		}
	}

	return &undefined;
}

Value* SkeletonAttributes_AddAttribute_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_AddAttribute, 3, count);
	type_check(arg_list[0], Integer, "rageMaxSkeleton [index]");
	type_check(arg_list[1], String, "rageMaxSkeleton [name]");

	if(arg_list[0]->to_int() >= skeletonPlugin::GetInst().GetAttributeList().m_Entries.GetCount())
		return &undefined;

	char cValue[RAGE_MAX_PATH*3];

	if(arg_list[2]->tag == class_tag(Integer))
	{
		AttributeValue val;
		val.m_Type = IntegerType;

		sprintf(cValue, "%d", arg_list[2]->to_int());
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.Insert(arg_list[1]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[2]->tag == class_tag(Float))
	{
		AttributeValue val;
		val.m_Type = FloatType;

		sprintf(cValue, "%f", arg_list[2]->to_float());
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.Insert(arg_list[1]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[2]->tag == class_tag(String))
	{
		AttributeValue val;
		val.m_Type = StringType;

		sprintf(cValue, "%s", arg_list[2]->to_string());
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.Insert(arg_list[1]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[2]->tag == class_tag(Boolean))
	{
		AttributeValue val;
		val.m_Type = BoolType;

		sprintf(cValue, "%s", (arg_list[2]->to_bool() == true) ? "true" : "false");
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.Insert(arg_list[1]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[2]->tag == class_tag(Undefined))
	{
		AttributeValue val;
		val.m_Type = UndefinedType;

		val.m_Value = "";

		skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.Insert(arg_list[1]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[2]->tag == class_tag(Point3Value))
	{
		AttributeValue val;
		val.m_Type = Point3Type;

		sprintf(cValue, "%f,%f,%f", arg_list[2]->to_point3().x, arg_list[2]->to_point3().y, arg_list[2]->to_point3().z);
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.Insert(arg_list[1]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[2]->tag == class_tag(Matrix3Value))
	{
		AttributeValue val;
		val.m_Type = Matrix3Type;

		sprintf(cValue, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f", 
			arg_list[2]->to_matrix3().GetRow(0).x, arg_list[2]->to_matrix3().GetRow(0).y, arg_list[2]->to_matrix3().GetRow(0).z,
			arg_list[2]->to_matrix3().GetRow(1).x, arg_list[2]->to_matrix3().GetRow(1).y, arg_list[2]->to_matrix3().GetRow(1).z,
			arg_list[2]->to_matrix3().GetRow(2).x, arg_list[2]->to_matrix3().GetRow(2).y, arg_list[2]->to_matrix3().GetRow(2).z,
			arg_list[2]->to_matrix3().GetRow(3).x, arg_list[2]->to_matrix3().GetRow(3).y, arg_list[2]->to_matrix3().GetRow(3).z);
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_Entries[arg_list[0]->to_int()].m_Attributes.Insert(arg_list[1]->to_string(), val);
		return &true_value;
	}

	return &undefined;
}

Value* SkeletonAttributes_AddRootAttribute_cf(Value** arg_list, int count)
{
	check_arg_count(SkeletonAttributes_AddAttribute, 2, count);
	type_check(arg_list[0], String, "rageMaxSkeleton [name]");

	char cValue[RAGE_MAX_PATH*3];

	if(arg_list[1]->tag == class_tag(Integer))
	{
		AttributeValue val;
		val.m_Type = IntegerType;

		sprintf(cValue, "%d", arg_list[1]->to_int());
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.Insert(arg_list[0]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[1]->tag == class_tag(Float))
	{
		AttributeValue val;
		val.m_Type = FloatType;

		sprintf(cValue, "%f", arg_list[1]->to_float());
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.Insert(arg_list[0]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[1]->tag == class_tag(String))
	{
		AttributeValue val;
		val.m_Type = StringType;

		sprintf(cValue, "%s", arg_list[1]->to_string());
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.Insert(arg_list[0]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[1]->tag == class_tag(Boolean))
	{
		AttributeValue val;
		val.m_Type = BoolType;

		sprintf(cValue, "%s", (arg_list[1]->to_bool() == true) ? "true" : "false");
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.Insert(arg_list[0]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[1]->tag == class_tag(Undefined))
	{
		AttributeValue val;
		val.m_Type = UndefinedType;

		val.m_Value = "";

		skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.Insert(arg_list[0]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[1]->tag == class_tag(Point3Value))
	{
		AttributeValue val;
		val.m_Type = Point3Type;

		sprintf(cValue, "%f,%f,%f", arg_list[1]->to_point3().x, arg_list[1]->to_point3().y, arg_list[1]->to_point3().z);
		val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.Insert(arg_list[0]->to_string(), val);
		return &true_value;
	}
	else if(arg_list[1]->tag == class_tag(Matrix3Value))
	{
		AttributeValue val;
		val.m_Type = Matrix3Type;

		sprintf(cValue, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f", 
			arg_list[1]->to_matrix3().GetRow(0).x, arg_list[1]->to_matrix3().GetRow(0).y, arg_list[1]->to_matrix3().GetRow(0).z,
			arg_list[1]->to_matrix3().GetRow(1).x, arg_list[1]->to_matrix3().GetRow(1).y, arg_list[1]->to_matrix3().GetRow(1).z,
			arg_list[1]->to_matrix3().GetRow(2).x, arg_list[1]->to_matrix3().GetRow(2).y, arg_list[1]->to_matrix3().GetRow(2).z,
			arg_list[1]->to_matrix3().GetRow(3).x, arg_list[1]->to_matrix3().GetRow(3).y, arg_list[1]->to_matrix3().GetRow(3).z);
			val.m_Value = cValue;

		skeletonPlugin::GetInst().GetAttributeList().m_RootAttributes.Insert(arg_list[0]->to_string(), val);
		return &true_value;
	}

	return &undefined;
}

