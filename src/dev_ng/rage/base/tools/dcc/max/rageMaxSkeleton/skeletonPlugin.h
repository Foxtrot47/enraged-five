#ifndef __CLIP_EDITOR__
#define __CLIP_EDITOR__

#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
//SIMPLE TYPE

#include <guplib.h>

#include "skeleton/skeleton.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

using namespace rage;

class skeletonPlugin : public GUP {
public:

	static HWND hParams;

	// GUP Methods
	DWORD		Start			( );
	void		Stop			( );
	DWORD_PTR	Control			( DWORD parameter );

	// Loading/Saving
	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);

	// Singleton access
	static skeletonPlugin& GetInst();

	//Constructor/Destructor
	skeletonPlugin();
	~skeletonPlugin();

	static void InitClass();
	static void ShutdownClass();

	BoneAttributeList& GetAttributeList() { return m_attributeList; }
	BoneList& GetBoneList() { return m_boneList; }
	Skeleton& GetSkeleton() { return m_skeleton; }

	BoneAttributeList m_attributeList;
	BoneList m_boneList;
	Skeleton m_skeleton;

	static bool initialised;
};

extern skeletonPlugin g_skeletonPlugin;


#endif //__CLIP_EDITOR__
