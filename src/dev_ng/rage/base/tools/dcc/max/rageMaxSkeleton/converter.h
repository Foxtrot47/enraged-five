#ifndef __CONVERTER__
#define __CONVERTER__

#include "objectSkeleton.h"
#include "skeleton/skeleton.h"

class SkeletonConverter
{
public:
	bool Convert(INode* pNode, const char* pName, const char* pPath, const atArray<atString>& atFilters);
	void Recurse(INode* pNode, ObjectSkeleton::Bone* bone);
	void RecurseParcodegen(ObjectSkeleton::Bone* pCurrentBone, rage::BoneInstance* pParent, bool bRoot, const atArray<atString>& atFilters);
	bool WriteBoneList(INode* pNode, const char* pPath);

private:
	void AddToBoneList(ObjectSkeleton::Bone* bone, BoneList& boneList);
};

#endif