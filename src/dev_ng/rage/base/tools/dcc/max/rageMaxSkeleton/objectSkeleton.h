// 
// rexGeneric/objectSkeleton.h 
// 
#ifndef __SKELLY_H__
#define __SKELLY_H__

/////////////////////////////////////////////////////////////////////////////////////
#include "atl/array.h"
#include "atl/string.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"

using namespace rage;

/////////////////////////////////////////////////////////////////////////////////////
class ObjectSkeleton
{
public:
	ObjectSkeleton() : m_RootBone( NULL ), m_AuthoredOrientation(true){}
	~ObjectSkeleton()
	{
		if( m_RootBone )
		{
			delete m_RootBone;
			m_RootBone = NULL;
		}
	}

	ObjectSkeleton* CreateNew(){ return new ObjectSkeleton; }

	void SetName(const char* pName)
	{
		m_Name = pName;
	}

	struct Bone
	{		
		Bone()  
		{ 
			m_IsJoint = true; 
			m_Offset.x = m_Offset.y = m_Offset.z = 0.0f; 
			m_BoneMatrix.Identity(); 
			m_JointOrient.Identity(); 
			m_ScaleOrient.Identity(); 
			m_Parent = NULL; 
		}

		~Bone() 
		{
			while( m_Children.GetCount() ) 
				delete m_Children.Pop();
		}

		struct ChannelInfo
		{
			bool m_IsLocked;
			bool m_IsLimited;
			float m_Min, m_Max;
			atString m_Name;
		};

		bool				m_IsJoint;			// true if a joint, false otherwise, eg. transforms that don't animate
		atArray<ChannelInfo>m_Channels;			// Bones have degrees of freedom, these are called channels.  This contains names of channels, if locked and limits
		Vector3				m_Offset;			// Offset to child (this) from parent (m_Parent)
		Matrix34			m_JointOrient;		// Joint orient - static rotation added to a joint BEFORE the animation data
		Matrix34			m_ScaleOrient;		// Scale orient/Rotate axis - applied AFTER animated rotation and BEFORE child joints are considered
		atArray<Bone*>		m_Children;			// Child bones
		Bone*				m_Parent;			// Parent bone
		atString			m_Name;				// Name similar to the name in Maya but "cleaned" by magic
		atString			m_BoneID;			// Text that is hashed to produce the bone ID, usually the name of the bone but not always
		bool				m_ScaleDof;
		bool				m_TranslationDof;
		bool				m_RotationDof;

		Bone* AddChild( const char * name, const Matrix34& matrix ) 
		{ 
			Bone* bone = rage_new Bone; 
			bone->m_Name = name; 
			bone->SetBoneMatrix( matrix ); 
			bone->m_Parent = this; 
			m_Children.PushAndGrow( bone ); 
			return bone; 
		}

		int GetBoneCount()
		{
			int childCount = m_Children.GetCount();
			const int ogChildCount = childCount;
			for( int a = 0; a < ogChildCount; a++ )
			{
				childCount += m_Children[ a ]->GetBoneCount();
			}
			return childCount;
		}

		void SetBoneMatrix(const Matrix34& boneMatrix) 
		{
			m_BoneMatrix = boneMatrix;
		}
		const Matrix34& GetBoneMatrix() const { return m_BoneMatrix;}

	private:
		Matrix34			m_BoneMatrix;	// Default rotation of bone, defines the pose used if there isn't any animation
	};

	int GetBoneCount() const { return m_RootBone ? m_RootBone->GetBoneCount() + 1 : 0; }

	Bone*		m_RootBone;
	atString	m_SkeletonType;
	bool		m_AuthoredOrientation;
	atString	m_JointLimits;
	atString	m_Name;
};

/////////////////////////////////////////////////////////////////////////////////////

#endif
