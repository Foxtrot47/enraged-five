#include "skeletonPlugin.h"
#include "resource.h"

#define skeletonPlugin_CLASS_ID	Class_ID(0xa40fedfd, 0x63c0e1e6)

skeletonPlugin g_skeletonPlugin;

class skeletonPluginClassDesc : public ClassDesc2 
{
public:
	virtual int IsPublic() 							{ return TRUE; }
	virtual void* Create(BOOL /*loading = FALSE*/) 	{ return &skeletonPlugin::GetInst(); }
	virtual const TCHAR *	ClassName() 			{ return GetString(IDS_CLASS_NAME); }
	virtual SClass_ID SuperClassID() 				{ return GUP_CLASS_ID; }
	virtual Class_ID ClassID() 						{ return skeletonPlugin_CLASS_ID; }
	virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }

	virtual const TCHAR* InternalName() 			{ return _T("skeletonPlugin"); }	// returns fixed parsable name (scripter-visible name)
	virtual HINSTANCE HInstance() 					{ return hInstance; }					// returns owning module handle
};

bool skeletonPlugin::initialised = false;

static skeletonPluginClassDesc skeletonPluginDesc;

ClassDesc2* GetSkelPluginDesc() { return &skeletonPluginDesc; }

skeletonPlugin::skeletonPlugin()
{
	initialised = false;
}

skeletonPlugin::~skeletonPlugin()
{}

// Activate and Stay Resident
DWORD skeletonPlugin::Start( ) {

#pragma message(TODO("Do plugin initialization here"))

#pragma message(TODO("Return if you want remain loaded or not"))
	return GUPRESULT_KEEP;
}

void skeletonPlugin::Stop( ) {
#pragma message(TODO("Do plugin un-initialization here"))
}

DWORD_PTR skeletonPlugin::Control( DWORD parameter ) {
	return 0;
}

IOResult skeletonPlugin::Save(ISave *isave)
{
	return IO_OK;
}

IOResult skeletonPlugin::Load(ILoad *iload)
{
	return IO_OK;
}

skeletonPlugin& skeletonPlugin::GetInst()
{
	return g_skeletonPlugin;
}

void skeletonPlugin::InitClass()
{
	initialised = true;
}

void skeletonPlugin::ShutdownClass()
{
}

