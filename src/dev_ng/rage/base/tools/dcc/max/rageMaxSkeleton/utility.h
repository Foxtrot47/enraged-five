#include "vector\matrix34.h"
#include "atl/string.h"
using namespace rage;

namespace Utility
{
	void CopyMaxMatrixToRageMatrix(const Matrix3& r_SrcMat,Matrix34& r_DstMat);
	void CopyRageMatrixToMaxMatrix(const Matrix34& r_SrcMat,Matrix3& r_DstMat);
	INode* GetParent(INode* p_Node);
	void GetLocalMatrix(INode* p_Node,f32 Time,Matrix34& r_Local,bool WithObjectTM=false);

	atString GetBoneID(INode* p_BoneNode);
	atString GetBoneTranslation(INode* p_BoneNode);
	atString GetBoneScale(INode* p_BoneNode);
	atString GetBoneRotation(INode* p_BoneNode);
	bool GetUserPropString(INode* pNode, const char* pEntry, TSTR& string);
}

