#include "resource.h"

#include <sstream>
#include <string>
#include <plugapi.h>
#include "system/param.h"
#include "parser/macros.h"
#include "parser/manager.h"

extern ClassDesc2* GetSkelPluginDesc();

extern TCHAR *GetString(int id);

HINSTANCE hInstance;
int controlsInit = FALSE;

using namespace rage;

BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	hInstance = hinstDLL;				// Hang on to this DLL's instance handle.

	if (!controlsInit) {
		controlsInit = TRUE;

		// Initialize RAGE
		char* argv[1];
		argv[0] = "rageMaxSkeleton.dlu";
		::rage::sysParam::Init( 1, argv );

		// Initialize the RAGE parser
		INIT_PARSER;
	}
			
	return (TRUE);
}

__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(1);
}

//TODO: Must change this number when adding a new class
__declspec( dllexport ) int LibNumberClasses()
{
	return 1;
}

__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) {
		case 0: return GetSkelPluginDesc();
		default: return 0;
	}
}

__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

__declspec( dllexport ) int LibInitialize( void )
{
	int nResult = 1; // Assume successful

	return nResult;
}

__declspec( dllexport ) int LibShutdown( void )
{	
	int nResult = 1; // Assume successful

	return nResult;
}

TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	return NULL;
}
