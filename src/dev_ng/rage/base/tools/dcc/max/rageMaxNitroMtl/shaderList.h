#ifndef MAX_SHADERLIST_H
#define MAX_SHADERLIST_H

#include "atl/array.h"
#include "atl/map.h"
#include "file/asset.h"

namespace rage {

class grcEffect;
struct fiFindData;

class ShaderList
{
public:
	ShaderList();
	~ShaderList();

	void SetShaderPath(const char* path);
	void BuildShaderList();
	
	// Looks up a shader, if it doesn't exist, it will load it
	grcEffect *FindShader(const char *name);
protected:
	void ClearNameList(); 
	static void AddFileToList(const fiFindData &data,void *userArg);

private:
	const char*							m_ShaderPath;
	atArray<char *>						m_ShaderNames;
	atMap<ConstString, grcEffect *>		m_Shaders;
};

} // namespace rage

#endif // MAX_SHADERLIST_H