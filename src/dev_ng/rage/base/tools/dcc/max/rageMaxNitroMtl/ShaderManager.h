#ifndef MAX_SHADERMANAGER_H
#define MAX_SHADERMANAGER_H

#include "shaderdb.h"
#include "shaderlist.h"

#define RSTMTL_EXPORT __declspec( dllimport )

namespace rage {

class ShaderManager
{
public:
	virtual ShaderManager::~ShaderManager();

	static ShaderManager*	GetInstance();
	static bool				InitShaderLib();
	static ShaderDb*		GetShaderDb() { return sm_ShaderDb; }
	static ShaderList*		GetShaderList() { return sm_ShaderList; }

	static ShaderDbEntry* GetDbEntry(const atString& r_Name);
	static MSLShaderPreset* GetNitroDBEntry(const atString& r_Name);
	static grcEffect* FindShader(const atString& ShaderName);
	static bool IsAlphaShader(bool isPreset, atString shaderName);
	static const char*	GetPresetPath(){return sm_PresetPath;}
protected:
	static ShaderManager* sm_This;

private:
	static bool						sm_InitOK;
	static ShaderList*				sm_ShaderList;
	static ShaderDb*				sm_ShaderDb;
	static char						sm_ShaderPath[RAGE_MAX_PATH];
	static char						sm_PresetPath[RAGE_MAX_PATH];
};

} // namespace rage
#endif // MAX_SHADERMANAGER_H