#include "atl/string.h"
#include "file/stream.h"

using namespace rage;

class RageNitroMtlUtilities
{
public:
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	static atString GetTexMapName(Texmap* p_Texmap)
	{
		if(!p_Texmap)
		{
			return atString("none");
		}

		atString Ret(p_Texmap->GetName());

		if(p_Texmap->ClassID() == Class_ID(BMTEX_CLASS_ID,0))
		{
			Ret += " (";

			BitmapTex* p_BitmapTex = (BitmapTex*)p_Texmap;
			TSTR MapName = _T(p_BitmapTex->GetMapName());

			int CharIdx = MapName.last('\\');

			if(CharIdx != -1)
			{
				CharIdx++;
				int NumChars = MapName.length() - CharIdx;

				Ret += MapName.Substr(CharIdx,NumChars);
			}
			else
			{
				Ret += MapName;
			}

			Ret += ")";
		}

		return Ret;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	static atString GetTexMapFullPath(Texmap* p_Texmap)
	{
		atString Ret("");

		if(!p_Texmap)
		{
			return Ret;
		}

		Class_ID cid = p_Texmap->ClassID();

		if(cid == Class_ID(BMTEX_CLASS_ID,0))
		{
			BitmapTex* p_BitmapTex = (BitmapTex*)p_Texmap;
			Ret += _T(p_BitmapTex->GetMapName());
		}
		else if(cid == Class_ID(COMPOSITE_CLASS_ID,0))
		{
			s32 i,Count = p_Texmap->NumSubTexmaps();
			Texmap* p_subMap;

			for(i=0;i<Count;i++)
			{
				p_subMap = p_Texmap->GetSubTexmap(i);

				if(p_subMap && p_subMap->ClassID() == Class_ID(BMTEX_CLASS_ID,0))
				{
					BitmapTex* p_BitmapTex = (BitmapTex*)p_subMap;
					Ret += _T(p_BitmapTex->GetMapName());
					Ret += ">";
				}
			}
		}

		return Ret;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	static void WriteString(fiStream* p_Save,const atString& r_Write)
	{
		s32 WriteSize = r_Write.GetLength();
		p_Save->WriteInt(&WriteSize,1);
		p_Save->Write(r_Write,WriteSize);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	static void ReadString(fiStream* p_Load,atString& r_Read)
	{
		s32 ReadSize;
		p_Load->ReadInt(&ReadSize,1);
		char* p_Buffer = new char[ReadSize + 1];
		p_Buffer[ReadSize] = '\0';
		p_Load->Read(p_Buffer,ReadSize);
		r_Read = atString(p_Buffer);
		delete[] p_Buffer;
	}
};