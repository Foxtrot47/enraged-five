#ifndef DBEDITOR_H__
#define DBEDITOR_H__

#include "atl/string.h"

#include <commctrl.h>

#define ENABLE_PRESET_EDITING 0



namespace rage {

class ShaderDb;

#define IMAGE_GROUP 0 
#define IMAGE_GROUP_SEL 1
#define IMAGE_ITEM 2 
#define IMAGE_ITEM_SEL 3


	////////////////////////////////////////////////////////////////////////////////
	class DbEditor
	{
	public:

		static DbEditor& GetInst();

		bool Show();
		bool ChoosePreset(HWND m_ParentWnd,atString& r_PresetPath);
		void SetRootPath(const atString& r_RootPath);
		bool AddGroup(const char* r_FullPath);

	protected:
		DbEditor();
		~DbEditor() {}

		bool IsGroup(HTREEITEM Item);
		HTREEITEM AddTreeItem(HTREEITEM ParentItem,const char* r_Name,bool IsGroup);
		void GetPath(HTREEITEM Item,atString& r_PathOut);
		HTREEITEM GetItem(const char* r_PathIn);
		HTREEITEM GetCurrentGroup();
		void BuildEntries(HTREEITEM Parent,const atString& Level);
		bool SelectItem(const atString& r_PathIn);

		void OnInitDialog(HWND Wnd);
		void OnCommandOK();
		void OnCommandClose();

#if ENABLE_PRESET_EDITING
		void OnEntryEditPreset();
		void OnEntryAddPreset();
		void OnEntryAddGroup();
		void OnEntryRemove();
#endif //ENABLE_PRESET_EDITING

		void OnHelpAbout();
		bool OnNotifyTree(const NMTREEVIEW* p_NmTreeView);

		static INT_PTR CALLBACK DbChooserDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);

		ShaderDb* m_shaderDb;
		s32 m_NumDBEntries;
		atString m_PresetPath;
		HTREEITEM m_CurrentSel;
		HWND m_Wnd;
		HWND m_TreeWnd;
		bool m_IsChoosing;
	};
}

#endif // DBEDITOR_H__