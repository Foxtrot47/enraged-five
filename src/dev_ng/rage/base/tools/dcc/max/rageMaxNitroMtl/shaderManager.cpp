
#include "ShaderManager.h"

#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "system/exec.h"

using namespace rage;

bool						ShaderManager::sm_InitOK = false;
ShaderList*					ShaderManager::sm_ShaderList = 0;
ShaderDb*					ShaderManager::sm_ShaderDb = 0;
ShaderManager*				ShaderManager::sm_This = 0;
char						ShaderManager::sm_ShaderPath[RAGE_MAX_PATH] = {0};
char						ShaderManager::sm_PresetPath[RAGE_MAX_PATH] = {0};

bool ShaderManager::InitShaderLib()
{
	grcTextureFactory::CreateStringTextureFactory(true);
	grcTextureFactory::InitClass();
	
	sm_ShaderList = new ShaderList;
	sm_ShaderDb = new ShaderDb;

	char cEnv[RAGE_MAX_PATH];
	if(sysGetEnv("RS_BUILDBRANCH", cEnv, RAGE_MAX_PATH))
	{
		atString shaderDbPath(cEnv);
		shaderDbPath += "\\common\\shaders\\db\\";
		strcpy_s(sm_PresetPath, RAGE_MAX_PATH, shaderDbPath.c_str());

		atString shaderPath(cEnv);
		shaderPath += "\\common\\shaders\\";
		strcpy_s(sm_ShaderPath, RAGE_MAX_PATH, shaderPath.c_str());

	}
	else if(sysGetEnv("RS_OUTSOURCEBRANCH", cEnv, RAGE_MAX_PATH))
	{
		atString shaderDbPath(cEnv);
		shaderDbPath += "\\shaders\\db\\";
		strcpy_s(sm_PresetPath, RAGE_MAX_PATH, shaderDbPath.c_str());

		atString shaderPath(cEnv);
		shaderPath += "\\shaders\\";
		strcpy_s(sm_ShaderPath, RAGE_MAX_PATH, shaderPath.c_str());
	}
	else
	{
		strcpy_s(sm_PresetPath, RAGE_MAX_PATH, "X:/gta5/build/dev/common/shaders/db/");
		strcpy_s(sm_ShaderPath, RAGE_MAX_PATH, "X:/gta5/build/dev/common/shaders/");
	}

	sm_ShaderList->SetShaderPath(sm_ShaderPath);
	sm_ShaderList->BuildShaderList();

	grcEffect::Preload(sm_ShaderPath);
	
	sm_ShaderDb->SetShaderDbPath(sm_PresetPath);

	sm_InitOK = true;
	return sm_InitOK;
}

ShaderManager*	ShaderManager::GetInstance()
{
	if( !sm_This )
	{
		sm_This = new ShaderManager; 
	}
	return sm_This;
}

ShaderManager::~ShaderManager() 
{
	sm_This = 0;

	delete sm_ShaderDb;
	delete sm_ShaderList;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
ShaderDbEntry* ShaderManager::GetDbEntry(const atString& r_Name)
{
	return sm_ShaderDb->GetItem(r_Name);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
MSLShaderPreset* ShaderManager::GetNitroDBEntry(const atString& r_Name)
{
	return sm_ShaderDb->GetMSLItem(r_Name);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
grcEffect* ShaderManager::FindShader(const atString& ShaderName)
{
	if(ShaderName == "")
		return NULL;


	if(!sm_InitOK)
		return NULL;

	const char* p_Ext = strrchr(ShaderName,'.');
	grcEffect* p_Shader = NULL;

	if(p_Ext && (stricmp(p_Ext,".sps") == 0))
	{
		ShaderDbEntry* p_Entry = GetDbEntry(ShaderName);

		if(!p_Entry)
			return NULL;

		p_Shader = &p_Entry->m_Preset->GetBasis();
	}
	else
	{
		p_Shader = sm_ShaderList->FindShader(ShaderName);
	}

	return p_Shader;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool ShaderManager::IsAlphaShader(bool isPreset, atString shaderName)
{
	if(isPreset)
	{
		ShaderDbEntry* p_Entry = ShaderManager::GetDbEntry(shaderName);

		if(!p_Entry)
			return false;

		if(p_Entry->m_Preset)
		{
			if( p_Entry->m_Preset->GetDrawBucket() > 0 )
				return true;
		}
	}
	else
	{
		grcEffect* p_Shader = ShaderManager::FindShader(shaderName);

		if(p_Shader && p_Shader->GetDrawBucket() > 0)
			return true;
	}

	return false;
}