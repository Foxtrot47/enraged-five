#pragma warning( disable: 4265 )
#pragma warning( disable: 4263 )

#include <maxscript/maxscript.h>
#include <maxscript/foundation/3dmath.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/foundation/Numbers.h>
#include <maxscript/foundation/Name.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last

using namespace rage;

#include "rageMaxNitroMtl.h"
#include "utility.h"

def_visible_primitive(RNMSetShaderName,"RNMSetShaderName");
def_visible_primitive(RNMGetShaderName,"RNMGetShaderName");

def_visible_primitive(RNMGetVariable,"RNMGetVariable");
def_visible_primitive(RNMSetVariable,"RNMSetVariable");

def_visible_primitive(RNMGetVariableType,"RNMGetVariableType");

def_visible_primitive(RNMGetVariableCount,"RNMGetVariableCount");

def_visible_primitive(RNMGetIsTwoSided,"RNMGetIsTwoSided");
def_visible_primitive(RNMSetIsTwoSided,"RNMSetIsTwoSided");

def_visible_primitive(RNMGetIsAlphaShader,"RNMGetIsAlphaShader");

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RNMSetShaderName_cf(Value** arg_list, int count)
{
	check_arg_count( "RNMSetShaderName", 2, count);
	type_check( arg_list[1], String, "RNMSetShaderName <mtl> <string>" );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RAGE_MAX_NITRO_MTL_CLASS_ID)
	{
		return &false_value;
	}

	RageMaxNitroMtl* p_NitroMtl = (RageMaxNitroMtl*)p_MtlBase;	
	p_NitroMtl->SetShaderName(atString(arg_list[1]->to_string()));
	p_NitroMtl->SetNitroShader();

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RNMGetShaderName_cf(Value** arg_list, int count)
{
	check_arg_count( "RNMGetShaderName", 1, count );
	
	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	if(p_MtlBase->ClassID() != RAGE_MAX_NITRO_MTL_CLASS_ID)
	{
		return &undefined;
	}

	RageMaxNitroMtl* p_NitroMtl = (RageMaxNitroMtl*)p_MtlBase;

	char TempName[MAX_PATH];
	strcpy(TempName,p_NitroMtl->GetShaderName());		
	return_protected(new String(TempName));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RNMGetVariable_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetVariable", 2, count);
	type_check( arg_list[1], Integer, "RstGetVariable <mtl> <idx>" );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RAGE_MAX_NITRO_MTL_CLASS_ID)
		return &undefined;

	RageMaxNitroMtl* p_MtlMax = (RageMaxNitroMtl*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;

	
	// new
	grcEffect* p_Shader = ShaderManager::GetInstance()->FindShader(p_MtlMax->GetShaderName());

	if(!p_Shader)
	{
		return &undefined;
	}

	grmVariableInfo VarInfo;

	p_Shader->GetInstancedVariableInfo(index,VarInfo);
	ShaderInstanceData* p_ShaderInstanceData = p_MtlMax->GetShaderInstanceData(); 
	ShaderInstanceData::ShaderVar* p_OldVar = p_ShaderInstanceData->GetVar(VarInfo);

	if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
	{
		p_MtlMax->MapShaderToInstanceData();
	}

	grcEffect::VarType type = VarInfo.m_Type;

	switch(type)
	{
	case grcEffect::VT_FLOAT:
		{
			f32 val = p_ShaderInstanceData->GetFloat(p_OldVar->Idx);
			return_protected(Float::intern(val));
		}
	case grcEffect::VT_VECTOR2:
		{
			Vector2 val = p_ShaderInstanceData->GetVector2(p_OldVar->Idx);
			Point2 retval;

			retval.x = val.x;
			retval.y = val.y;

			return_protected(new Point2Value(retval));
		}
	case grcEffect::VT_VECTOR3:
		{
			Vector3 val = p_ShaderInstanceData->GetVector3(p_OldVar->Idx);
			Point3 retval;
			
			retval.x = val.x;
			retval.y = val.y;
			retval.z = val.z;

			return_protected(new Point3Value(retval));
		}
	case grcEffect::VT_TEXTURE:
		{
			atString val = RageNitroMtlUtilities::GetTexMapFullPath(p_ShaderInstanceData->GetTexture(p_OldVar->Idx));
			char Buffer[1024];
			strcpy(Buffer,val);
			return_protected(new String(Buffer));
		}
	case grcEffect::VT_VECTOR4: 
		{
			Vector4 val = p_ShaderInstanceData->GetVector4(p_OldVar->Idx);
			Point4 retval;
			
			retval.x = val.x;
			retval.y = val.y;
			retval.z = val.z;
			retval.w = val.w;

			return_protected(new Point4Value(retval));
		}
	case grcEffect::VT_MATRIX43:
	case grcEffect::VT_MATRIX44:
	case grcEffect::VT_STRING:
	case grcEffect::VT_NONE:
		//		case grcEffect::VT_COLOR32:
	case grcEffect::VT_BOOL_DONTUSE:
	case grcEffect::VT_INT_DONTUSE:
	case grcEffect::VT_COUNT:
	default:
		//		case grcEffect::VT_SIZE:
		break;
	}

	return &undefined;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RNMSetVariable_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetVariable", 3, count);
	type_check( arg_list[1], Integer, "RstSetVariable <mtl> <idx> <val>" );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RAGE_MAX_NITRO_MTL_CLASS_ID)
		return &undefined;

	RageMaxNitroMtl* p_MtlMax = (RageMaxNitroMtl*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;

	// new
	grcEffect* p_Shader = ShaderManager::GetInstance()->FindShader(p_MtlMax->GetShaderName());

	if(!p_Shader)
	{
		return &undefined;
	}

	grmVariableInfo VarInfo;

	p_Shader->GetInstancedVariableInfo(index,VarInfo);
	ShaderInstanceData* p_ShaderInstanceData = p_MtlMax->GetShaderInstanceData(); 
	ShaderInstanceData::ShaderVar* p_OldVar = p_ShaderInstanceData->GetVar(VarInfo);

	if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
	{
		p_MtlMax->MapShaderToInstanceData();
		p_OldVar = p_ShaderInstanceData->GetVar(VarInfo);
	}

	grcEffect::VarType type = VarInfo.m_Type;

	switch(type)
	{
	case grcEffect::VT_FLOAT:
		{
			f32 val = arg_list[2]->to_float();
			p_ShaderInstanceData->SetFloat(p_OldVar->Idx, val);
			return &true_value;
		}
	case grcEffect::VT_VECTOR2:
		{
			Vector2 val;
			Point2 inval = arg_list[2]->to_point2();

			val.x = inval.x;
			val.y = inval.y;

			p_ShaderInstanceData->SetVector2(p_OldVar->Idx, val);

			return &true_value;
		}
	case grcEffect::VT_VECTOR3:
		{
			Vector3 val;
			Point3 inval = arg_list[2]->to_point3();

			val.x = inval.x;
			val.y = inval.y;
			val.z = inval.z;

			p_ShaderInstanceData->SetVector3(p_OldVar->Idx, val);

			return &true_value;
		}
	case grcEffect::VT_TEXTURE:
		{
			char* p_inval = arg_list[2]->to_string();
			atString val(p_inval);

			BitmapTex* p_bitmapTex = NewDefaultBitmapTex();
			p_bitmapTex->SetMapName(val.c_str());
			
			p_MtlMax->SetSubTexmap(p_OldVar->Idx,p_bitmapTex);
			p_ShaderInstanceData->SetTexture(p_OldVar->Idx, p_bitmapTex);
			

			return &true_value;
		}
	case grcEffect::VT_VECTOR4:
		{
			Vector4 val;
			Point4 inval = arg_list[2]->to_point4();

			val.x = inval.x;
			val.y = inval.y;
			val.z = inval.z;
			val.w = inval.w;

			p_ShaderInstanceData->SetVector4(p_OldVar->Idx, val);

			return &true_value;
		}
	case grcEffect::VT_MATRIX43:
	case grcEffect::VT_MATRIX44:
	case grcEffect::VT_STRING:
	case grcEffect::VT_NONE:
		//		case grcEffect::VT_COLOR32:
	case grcEffect::VT_BOOL_DONTUSE:
	case grcEffect::VT_INT_DONTUSE:
	case grcEffect::VT_COUNT:
		default:
		//		case grcEffect::VT_SIZE:
		break;
	}
	p_MtlMax->NotifyChanged();
	return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RNMGetVariableType_cf(Value** arg_list, int count)
{
	check_arg_count( "RNMGetVariableType", 2, count);
	type_check( arg_list[1], Integer, "RNMGetVariableType <mtl> <idx>" );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RAGE_MAX_NITRO_MTL_CLASS_ID)
		return &undefined;

	RageMaxNitroMtl* p_MtlMax = (RageMaxNitroMtl*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;

	grcEffect* p_Shader = ShaderManager::GetInstance()->FindShader(p_MtlMax->GetShaderName());

	if(!p_Shader)
	{
		return &undefined;
	}

	grmVariableInfo VarInfo;

	p_Shader->GetInstancedVariableInfo(index,VarInfo);

	switch(VarInfo.m_Type)
	{
	case grcEffect::VT_FLOAT:
		return_protected(new String("float"));
		break;
	case grcEffect::VT_VECTOR2:
		return_protected(new String("vector2"));
		break;
	case grcEffect::VT_VECTOR3:
		return_protected(new String("vector3"));
		break;
	case grcEffect::VT_TEXTURE:
		return_protected(new String("texmap"));
		break;
	case grcEffect::VT_VECTOR4: 
		return_protected(new String("vector4"));
		break;
	case grcEffect::VT_MATRIX43:
	case grcEffect::VT_MATRIX44:
	case grcEffect::VT_STRING:
	case grcEffect::VT_NONE:
		//		case grcEffect::VT_COLOR32:
	case grcEffect::VT_BOOL_DONTUSE:
	case grcEffect::VT_INT_DONTUSE:
	case grcEffect::VT_COUNT:
		default:
		//		case grcEffect::VT_SIZE:
		break;
	}

	return &undefined;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RNMGetVariableCount_cf(Value** arg_list, int count)
{
	check_arg_count( "RNMGetVariableCount", 1, count);

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RAGE_MAX_NITRO_MTL_CLASS_ID)
	{
		return &undefined;
	}

	RageMaxNitroMtl* p_NitroMtl = (RageMaxNitroMtl*)p_MtlBase;

	return_protected(new Integer(p_NitroMtl->GetVariableCount()));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

Value* RNMGetIsTwoSided_cf( Value** arg_list, int count )
{
	check_arg_count( "RNMGetIsTwoSided", 1, count );

	MtlBase* pMtlBase = arg_list[0]->to_mtlbase();
	if ( RAGE_MAX_NITRO_MTL_CLASS_ID != pMtlBase->ClassID() )
		throw RuntimeError( "Invalid Rage Nitro Material." );

	RageMaxNitroMtl* pMtlMax = static_cast<RageMaxNitroMtl*>( pMtlBase );
	bool twoSided = pMtlMax->IsTwoSided();

	if ( twoSided )
		return ( &true_value );
	else
		return ( &false_value );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

Value* RNMSetIsTwoSided_cf( Value** arg_list, int count )
{
	check_arg_count( "RNMSetIsTwoSided", 2, count );
	type_check( arg_list[1], Boolean, "RNMSetIsTwoSided <mtl> <bool>" );

	MtlBase* pMtlBase = arg_list[0]->to_mtlbase();
	bool toggle = arg_list[1]->to_bool();
	if ( RAGE_MAX_NITRO_MTL_CLASS_ID != pMtlBase->ClassID() )
		throw RuntimeError( "Invalid Rage Material." );

	RageMaxNitroMtl* pMtlMax = static_cast<RageMaxNitroMtl*>( pMtlBase );
	pMtlMax->SetIsTwoSided(toggle);
	
	return ( &true_value );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RNMGetIsAlphaShader_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetIsAlphaShader", 1, count );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RAGE_MAX_NITRO_MTL_CLASS_ID)
	{
		return &false_value;
	}

	RageMaxNitroMtl* p_MtlMax = (RageMaxNitroMtl*)p_MtlBase;

	if( ShaderManager::IsAlphaShader( p_MtlMax->IsPreset(), p_MtlMax->GetShaderName() ) )
		return &true_value;

	return &false_value;
}