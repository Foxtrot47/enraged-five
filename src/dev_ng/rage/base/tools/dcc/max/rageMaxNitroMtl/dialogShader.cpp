#include "dialogShader.h"
#include "grcore/effect.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;
//////////////////////////////////////////////////////////////////////////////////////////////////////////
RsgMaxDlgShader::RsgMaxDlgShader(RageMaxNitroMtl* p_Mtl, IMtlParams* p_MtlParams, grcEffect* p_Shader):
m_pNitroMtl(p_Mtl),
m_Wnd(NULL),
m_pMtlParams(p_MtlParams),
m_ChangeFunctor(NULL),
m_Initialising(false)//,
{
	m_DadMgr.Init(this);
	m_ShaderControls = new ShaderControls( p_Shader, m_pNitroMtl->m_pShaderInstanceData, m_pNitroMtl->IsPreset(), m_pNitroMtl->GetShaderName() );
	Reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RsgMaxDlgShader::~RsgMaxDlgShader()
{
	delete m_ShaderControls;
	Reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgShader::SetupUI()
{
	// set up the spinner ui information

	s32 i,j,Count;

	Count = m_ShaderControls->GetFloatWidgetCount();
	for(i=0;i<Count;i++)
	{
		m_ShaderControls->GetFloatWidget(i).p_SpinCtrl = GetISpinner(GetDlgItem(m_Wnd,m_ShaderControls->GetFloatWidget(i).SpinnerID));
		m_ShaderControls->GetFloatWidget(i).p_EditCtrl = GetICustEdit(GetDlgItem(m_Wnd,m_ShaderControls->GetFloatWidget(i).EditID));

		f32 test = m_ShaderControls->GetFloatWidget(i).Step;
		m_ShaderControls->GetFloatWidget(i).p_SpinCtrl->SetScale(m_ShaderControls->GetFloatWidget(i).Step);
		m_ShaderControls->GetFloatWidget(i).p_SpinCtrl->SetLimits(m_ShaderControls->GetFloatWidget(i).Min,m_ShaderControls->GetFloatWidget(i).Max);

		m_ShaderControls->GetFloatWidget(i).p_SpinCtrl->SetResetValue(m_pNitroMtl->m_pShaderInstanceData->GetFloat(m_ShaderControls->GetFloatWidget(i).Idx));
		m_ShaderControls->GetFloatWidget(i).p_SpinCtrl->SetValue(m_pNitroMtl->m_pShaderInstanceData->GetFloat(m_ShaderControls->GetFloatWidget(i).Idx),FALSE);

		m_ShaderControls->GetFloatWidget(i).p_SpinCtrl->LinkToEdit(GetDlgItem(m_Wnd,m_ShaderControls->GetFloatWidget(i).EditID),EDITTYPE_FLOAT);
	}

	Count = m_ShaderControls->GetIntWidgetCount();
	for(i=0;i<Count;i++)
	{
		m_ShaderControls->GetIntWidget(i).p_SpinCtrl = GetISpinner(GetDlgItem(m_Wnd,m_ShaderControls->GetIntWidget(i).SpinnerID));
		m_ShaderControls->GetIntWidget(i).p_EditCtrl = GetICustEdit(GetDlgItem(m_Wnd,m_ShaderControls->GetIntWidget(i).EditID));

		m_ShaderControls->GetIntWidget(i).p_SpinCtrl->SetScale((float)m_ShaderControls->GetIntWidget(i).Step);
		m_ShaderControls->GetIntWidget(i).p_SpinCtrl->SetLimits(m_ShaderControls->GetIntWidget(i).Min,m_ShaderControls->GetIntWidget(i).Max);

		m_ShaderControls->GetIntWidget(i).p_SpinCtrl->SetResetValue(m_pNitroMtl->m_pShaderInstanceData->GetFloat(m_ShaderControls->GetIntWidget(i).Idx));
		m_ShaderControls->GetIntWidget(i).p_SpinCtrl->SetValue(m_pNitroMtl->m_pShaderInstanceData->GetFloat(m_ShaderControls->GetIntWidget(i).Idx),FALSE);

		m_ShaderControls->GetIntWidget(i).p_SpinCtrl->LinkToEdit(GetDlgItem(m_Wnd,m_ShaderControls->GetIntWidget(i).EditID),EDITTYPE_INT);
	}

	Count = m_ShaderControls->GetVector2WidgetCount();
	for(i=0;i<Count;i++)
	{
		for(j=0;j<2;j++)
		{
			m_ShaderControls->GetVector2Widget(i).Float[j].p_SpinCtrl = GetISpinner(GetDlgItem(m_Wnd,m_ShaderControls->GetVector2Widget(i).Float[j].SpinnerID));
			m_ShaderControls->GetVector2Widget(i).Float[j].p_EditCtrl = GetICustEdit(GetDlgItem(m_Wnd,m_ShaderControls->GetVector2Widget(i).Float[j].EditID));

			m_ShaderControls->GetVector2Widget(i).Float[j].p_SpinCtrl->SetScale(m_ShaderControls->GetVector2Widget(i).Float[j].Step);
			m_ShaderControls->GetVector2Widget(i).Float[j].p_SpinCtrl->SetLimits(m_ShaderControls->GetVector2Widget(i).Float[j].Min,m_ShaderControls->GetVector2Widget(i).Float[j].Max);

			m_ShaderControls->GetVector2Widget(i).Float[j].p_SpinCtrl->SetValue(m_pNitroMtl->m_pShaderInstanceData->GetVector2(m_ShaderControls->GetVector2Widget(i).Idx)[j],FALSE);
			m_ShaderControls->GetVector2Widget(i).Float[j].p_SpinCtrl->SetResetValue(m_pNitroMtl->m_pShaderInstanceData->GetVector2(m_ShaderControls->GetVector2Widget(i).Idx)[j]);

			m_ShaderControls->GetVector2Widget(i).Float[j].p_SpinCtrl->LinkToEdit(GetDlgItem(m_Wnd,m_ShaderControls->GetVector2Widget(i).Float[j].EditID),EDITTYPE_FLOAT);
		}
	}

	Count = m_ShaderControls->GetVector3WidgetCount();
	for(i=0;i<Count;i++)
	{
		for(j=0;j<3;j++)
		{
			m_ShaderControls->GetVector3Widget(i).Float[j].p_SpinCtrl = GetISpinner(GetDlgItem(m_Wnd,m_ShaderControls->GetVector3Widget(i).Float[j].SpinnerID));
			m_ShaderControls->GetVector3Widget(i).Float[j].p_EditCtrl = GetICustEdit(GetDlgItem(m_Wnd,m_ShaderControls->GetVector3Widget(i).Float[j].EditID));

			m_ShaderControls->GetVector3Widget(i).Float[j].p_SpinCtrl->SetScale(m_ShaderControls->GetVector3Widget(i).Float[j].Step);
			m_ShaderControls->GetVector3Widget(i).Float[j].p_SpinCtrl->SetLimits(m_ShaderControls->GetVector3Widget(i).Float[j].Min,m_ShaderControls->GetVector3Widget(i).Float[j].Max);

			m_ShaderControls->GetVector3Widget(i).Float[j].p_SpinCtrl->SetValue(m_pNitroMtl->m_pShaderInstanceData->GetVector3(m_ShaderControls->GetVector3Widget(i).Idx)[j],FALSE);
			m_ShaderControls->GetVector3Widget(i).Float[j].p_SpinCtrl->SetResetValue(m_pNitroMtl->m_pShaderInstanceData->GetVector3(m_ShaderControls->GetVector3Widget(i).Idx)[j]);

			m_ShaderControls->GetVector3Widget(i).Float[j].p_SpinCtrl->LinkToEdit(GetDlgItem(m_Wnd,m_ShaderControls->GetVector3Widget(i).Float[j].EditID),EDITTYPE_FLOAT);
		}
	}

	Count = m_ShaderControls->GetVector4WidgetCount();
	for(i=0;i<Count;i++)
	{
		for(j=0;j<4;j++)
		{
			m_ShaderControls->GetVector4Widget(i).Float[j].p_SpinCtrl = GetISpinner(GetDlgItem(m_Wnd,m_ShaderControls->GetVector4Widget(i).Float[j].SpinnerID));
			m_ShaderControls->GetVector4Widget(i).Float[j].p_EditCtrl = GetICustEdit(GetDlgItem(m_Wnd,m_ShaderControls->GetVector4Widget(i).Float[j].EditID));

			m_ShaderControls->GetVector4Widget(i).Float[j].p_SpinCtrl->SetScale(m_ShaderControls->GetVector4Widget(i).Float[j].Step);
			m_ShaderControls->GetVector4Widget(i).Float[j].p_SpinCtrl->SetLimits(m_ShaderControls->GetVector4Widget(i).Float[j].Min,m_ShaderControls->GetVector4Widget(i).Float[j].Max);

			m_ShaderControls->GetVector4Widget(i).Float[j].p_SpinCtrl->SetValue(m_pNitroMtl->m_pShaderInstanceData->GetVector4(m_ShaderControls->GetVector4Widget(i).Idx)[j],FALSE);
			m_ShaderControls->GetVector4Widget(i).Float[j].p_SpinCtrl->SetResetValue(m_pNitroMtl->m_pShaderInstanceData->GetVector4(m_ShaderControls->GetVector4Widget(i).Idx)[j]);

			m_ShaderControls->GetVector4Widget(i).Float[j].p_SpinCtrl->LinkToEdit(GetDlgItem(m_Wnd,m_ShaderControls->GetVector4Widget(i).Float[j].EditID),EDITTYPE_FLOAT);
		}
	}

	Count = m_ShaderControls->GetTextureWidgetCount();
	for(i=0;i<Count;i++)
	{
		HWND ButWnd = GetDlgItem(m_Wnd,m_ShaderControls->GetTextureWidget(i).ButtonID);
		m_ShaderControls->GetTextureWidget(i).p_ButCtrl = GetICustButton(ButWnd);
		m_ShaderControls->GetTextureWidget(i).p_ButCtrl->SetDADMgr(&m_DadMgr);

		ButWnd = GetDlgItem(m_Wnd,m_ShaderControls->GetTextureWidget(i).ButtonAlphaID);
		HWND SubButWnd = GetDlgItem(m_Wnd,m_ShaderControls->GetTextureWidget(i).ButtonSubAttrDialogID);

		if(ButWnd)
		{
			m_ShaderControls->GetTextureWidget(i).p_ButAlphaCtrl = GetICustButton(ButWnd);
			m_ShaderControls->GetTextureWidget(i).p_ButAlphaCtrl->SetDADMgr(&m_DadMgr);
		}
		else if(SubButWnd)
		{
			m_ShaderControls->GetTextureWidget(i).p_SutSubAttrCtrl = GetICustButton(SubButWnd);
		}
		else
		{
			m_ShaderControls->GetTextureWidget(i).p_ButAlphaCtrl = NULL;
		}
	}

	Count = m_ShaderControls->GetBoolWidgetCount();
	for(i=0;i<Count;i++)
	{
		CheckDlgButton(m_Wnd, m_ShaderControls->GetBoolWidget(i).ButtonID, m_pNitroMtl->m_pShaderInstanceData->GetBool(m_ShaderControls->GetBoolWidget(i).Idx) ? BST_CHECKED : BST_UNCHECKED);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgShader::UpdateMapName(int Index,const char* r_MapName)
{
	char Buffer[8192];
	strcpy(Buffer,r_MapName);

	s32 Count = m_ShaderControls->GetTextureWidgetCount();

	if(Index < 0)
		return;

	if(Index >= (Count << 1))
		return;

	if(Index < Count)
		m_ShaderControls->GetTextureWidget(Index).p_ButCtrl->SetText(Buffer);
	else
		m_ShaderControls->GetTextureWidget(Index - Count).p_ButAlphaCtrl->SetText(Buffer);

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgShader::Reset()
{
	if(m_Wnd)
	{
		m_pMtlParams->DeleteRollupPage(m_Wnd);
		DestroyWindow(m_Wnd);
	}

	m_Wnd = NULL;
	m_ChangeFunctor = NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ReferenceTarget* RsgMaxDlgShader::GetThing() 
{
	return m_pNitroMtl; 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgShader::DeleteThis() 
{
	//m_pShaderDlg = NULL;
	delete this; 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RsgMaxDlgShader::FindSubTexFromHWND(HWND hwnd)
{
	s32 i,Count = m_ShaderControls->GetTextureWidgetCount();

	for(i=0;i<Count;i++)
	{
		if(hwnd == GetDlgItem(m_Wnd,m_ShaderControls->GetTextureWidget(i).ButtonID))
		{
			return i;
		}

		if(hwnd == GetDlgItem(m_Wnd,m_ShaderControls->GetTextureWidget(i).ButtonAlphaID))
		{
			return i + Count;
		}
	}
	
	return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
HWND RsgMaxDlgShader::Create(TSTR title)
{
	m_ShaderControls->GetDialogResBuilder().SetSize(m_ShaderControls->GetWidth(),m_ShaderControls->GetHeight());

	m_Wnd = m_pMtlParams->AddRollupPage(g_hDllInstance,
		m_ShaderControls->GetDialogResBuilder().GetTemplate(),
		RstMaxMtlDlgProc,
		title,
		(LPARAM)this,0,1);

	return m_Wnd;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgShader::OnInitDialog(HWND DlgWnd)
{
	m_Initialising = true;
	m_Wnd = DlgWnd;
	SetWindowLongPtr(DlgWnd,GWLP_USERDATA,(LONG_PTR)this);
	SetupUI();
	m_Initialising = false;
}

// TODO: LPXO - Refactor into manager
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgShader::OnSpinnerChange(s32 SpinnerID)
{
	if(m_Initialising) return;

	s32 i,j,SpinnerCount = m_ShaderControls->GetFloatWidgetCount();

	m_pNitroMtl->NotifyDependents(FOREVER,PART_MTL,REFMSG_NODE_MATERIAL_CHANGED);

	for(i=0;i<SpinnerCount;i++)
	{
		if(m_ShaderControls->GetFloatWidget(i).SpinnerID == SpinnerID)
		{
			float Val = m_ShaderControls->GetFloatWidget(i).p_SpinCtrl->GetFVal();
			m_pNitroMtl->m_pShaderInstanceData->GetFloat(m_ShaderControls->GetFloatWidget(i).Idx) = Val;


			if ( m_ChangeFunctor != NULL )
			{
				m_ChangeFunctor(NULL, NULL);
			}

			return;
		}
	}

	SpinnerCount = m_ShaderControls->GetIntWidgetCount();

	for(i=0;i<SpinnerCount;i++)
	{
		if(m_ShaderControls->GetIntWidget(i).SpinnerID == SpinnerID)
		{
			m_pNitroMtl->m_pShaderInstanceData->GetInt(m_ShaderControls->GetIntWidget(i).Idx) = m_ShaderControls->GetIntWidget(i).p_SpinCtrl->GetIVal();

			if ( m_ChangeFunctor != NULL )
			{
				m_ChangeFunctor(NULL, NULL);
			}

			return;
		}
	}

	SpinnerCount = m_ShaderControls->GetVector2WidgetCount();

	for(i=0;i<SpinnerCount;i++)
	{
		for(j=0;j<2;j++)
		{
			if(m_ShaderControls->GetVector2Widget(i).Float[j].SpinnerID == SpinnerID)
			{
				float Val = m_ShaderControls->GetVector2Widget(i).Float[j].p_SpinCtrl->GetFVal();

				m_pNitroMtl->m_pShaderInstanceData->GetVector2(m_ShaderControls->GetVector2Widget(i).Idx)[j] = Val;

				if ( m_ChangeFunctor != NULL )
				{
					m_ChangeFunctor(NULL, NULL);
				}

				return;
			}
		}
	}

	SpinnerCount = m_ShaderControls->GetVector3WidgetCount();

	for(i=0;i<SpinnerCount;i++)
	{
		for(j=0;j<3;j++)
		{
			if(m_ShaderControls->GetVector3Widget(i).Float[j].SpinnerID == SpinnerID)
			{
				float Val = m_ShaderControls->GetVector3Widget(i).Float[j].p_SpinCtrl->GetFVal();
				
				m_pNitroMtl->m_pShaderInstanceData->GetVector3(m_ShaderControls->GetVector3Widget(i).Idx)[j] = Val;

				if ( m_ChangeFunctor != NULL )
				{
					m_ChangeFunctor(NULL, NULL);
				}

				return;
			}
		}
	}

	SpinnerCount = m_ShaderControls->GetVector4WidgetCount();

	for(i=0;i<SpinnerCount;i++)
	{
		for(j=0;j<4;j++)
		{
			if(m_ShaderControls->GetVector4Widget(i).Float[j].SpinnerID == SpinnerID)
			{
				float Val = m_ShaderControls->GetVector4Widget(i).Float[j].p_SpinCtrl->GetFVal();

				m_pNitroMtl->m_pShaderInstanceData->GetVector4(m_ShaderControls->GetVector4Widget(i).Idx)[j] = Val;

				if ( m_ChangeFunctor != NULL )
				{
					m_ChangeFunctor(NULL, NULL);
				}

				return;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgShader::OnButtonPressed(s32 ButtonID)
{
	s32 i,TextureCount = m_ShaderControls->GetTextureWidgetCount();

	for(i=0;i<TextureCount;i++)
	{
		if(m_ShaderControls->GetTextureWidget(i).ButtonID == ButtonID)
		{
			PostMessage(m_pNitroMtl->m_MtlEditWnd,WM_TEXMAP_BUTTON,m_ShaderControls->GetTextureWidget(i).Idx,(LPARAM)m_pNitroMtl);
			return;
		}

		if(m_ShaderControls->GetTextureWidget(i).ButtonAlphaID == ButtonID)
		{
			PostMessage(m_pNitroMtl->m_MtlEditWnd,WM_TEXMAP_BUTTON,m_ShaderControls->GetTextureWidget(i).Idx + TextureCount,(LPARAM)m_pNitroMtl);
			return;
		}

	}	

	s32 Count = m_ShaderControls->GetVector3WidgetCount();
	for(i=0;i<Count;i++)
	{
		if(m_ShaderControls->GetVector3Widget(i).ButtonID == ButtonID)
		{
			DWORD Get = RGB(	(char)(m_ShaderControls->GetVector3Widget(i).Float[0].p_SpinCtrl->GetFVal() * 255.0f),
				(char)(m_ShaderControls->GetVector3Widget(i).Float[1].p_SpinCtrl->GetFVal() * 255.0f),
				(char)(m_ShaderControls->GetVector3Widget(i).Float[2].p_SpinCtrl->GetFVal() * 255.0f));
			IPoint2 Pos(10,10);
			HSVDlg_Do(m_Wnd,&Get,&Pos,NULL,"Choose colour");

			m_ShaderControls->GetVector3Widget(i).Float[0].p_SpinCtrl->SetValue((float)GetRValue(Get) / 255.0f,FALSE);
			m_ShaderControls->GetVector3Widget(i).Float[1].p_SpinCtrl->SetValue((float)GetGValue(Get) / 255.0f,FALSE);
			m_ShaderControls->GetVector3Widget(i).Float[2].p_SpinCtrl->SetValue((float)GetBValue(Get) / 255.0f,FALSE);

			m_pNitroMtl->m_pShaderInstanceData->GetVector3(m_ShaderControls->GetVector3Widget(i).Idx)[0] = (float)GetRValue(Get) / 255.0f;
			m_pNitroMtl->m_pShaderInstanceData->GetVector3(m_ShaderControls->GetVector3Widget(i).Idx)[1] = (float)GetGValue(Get) / 255.0f;
			m_pNitroMtl->m_pShaderInstanceData->GetVector3(m_ShaderControls->GetVector3Widget(i).Idx)[2] = (float)GetBValue(Get) / 255.0f;
		}
	}

#define GetAValue(rgb)      ((BYTE)((rgb)>>24))

	Count = m_ShaderControls->GetVector4WidgetCount();
	for(i=0;i<Count;i++)
	{
		if(m_ShaderControls->GetVector4Widget(i).ButtonID == ButtonID)
		{
			DWORD Get = RGB(	(char)(m_ShaderControls->GetVector4Widget(i).Float[0].p_SpinCtrl->GetFVal() * 255.0f),
				(char)(m_ShaderControls->GetVector4Widget(i).Float[1].p_SpinCtrl->GetFVal() * 255.0f),
				(char)(m_ShaderControls->GetVector4Widget(i).Float[2].p_SpinCtrl->GetFVal() * 255.0f));

			Get |= (char)(m_ShaderControls->GetVector4Widget(i).Float[3].p_SpinCtrl->GetFVal() * 255.0f) << 24;

			IPoint2 Pos(10,10);
			HSVDlg_Do(m_Wnd,&Get,&Pos,NULL,"Choose colour");

			m_ShaderControls->GetVector4Widget(i).Float[0].p_SpinCtrl->SetValue((float)GetRValue(Get) / 255.0f,FALSE);
			m_ShaderControls->GetVector4Widget(i).Float[1].p_SpinCtrl->SetValue((float)GetGValue(Get) / 255.0f,FALSE);
			m_ShaderControls->GetVector4Widget(i).Float[2].p_SpinCtrl->SetValue((float)GetBValue(Get) / 255.0f,FALSE);
			m_ShaderControls->GetVector4Widget(i).Float[3].p_SpinCtrl->SetValue((float)GetAValue(Get) / 255.0f,FALSE);

			m_pNitroMtl->m_pShaderInstanceData->GetVector4(m_ShaderControls->GetVector4Widget(i).Idx)[0] = (float)GetRValue(Get) / 255.0f;
			m_pNitroMtl->m_pShaderInstanceData->GetVector4(m_ShaderControls->GetVector4Widget(i).Idx)[1] = (float)GetGValue(Get) / 255.0f;
			m_pNitroMtl->m_pShaderInstanceData->GetVector4(m_ShaderControls->GetVector4Widget(i).Idx)[2] = (float)GetBValue(Get) / 255.0f;
			m_pNitroMtl->m_pShaderInstanceData->GetVector4(m_ShaderControls->GetVector4Widget(i).Idx)[3] = (float)GetAValue(Get) / 255.0f;
		}
	}

	Count = m_ShaderControls->GetBoolWidgetCount();
	for(i=0;i<Count;i++)
	{
		if(m_ShaderControls->GetBoolWidget(i).ButtonID == ButtonID)
		{
			if(m_pNitroMtl->m_pShaderInstanceData->GetBool(m_ShaderControls->GetBoolWidget(i).Idx))
			{
				m_pNitroMtl->m_pShaderInstanceData->GetBool(m_ShaderControls->GetBoolWidget(i).Idx) = false;
				CheckDlgButton(m_Wnd, m_ShaderControls->GetBoolWidget(i).ButtonID, BST_UNCHECKED);
			}
			else
			{
				m_pNitroMtl->m_pShaderInstanceData->GetBool(m_ShaderControls->GetBoolWidget(i).Idx) = true;
				CheckDlgButton(m_Wnd,m_ShaderControls->GetBoolWidget(i).ButtonID, BST_CHECKED);
			}
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgShader::OnDestroy()
{
	m_ShaderControls->ReleaseAllControls();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK RsgMaxDlgShader::RstMaxMtlDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	RsgMaxDlgShader* p_ParamDlg = (RsgMaxDlgShader*)GetWindowLongPtr(DlgWnd,GWLP_USERDATA);
	switch( msg )
	{
	case WM_INITDIALOG:
		( ( RsgMaxDlgShader* )lParam )->OnInitDialog( DlgWnd );
		p_ParamDlg->m_pNitroMtl->NotifyChanged();
		return TRUE;
	case CC_SPINNER_CHANGE:
		p_ParamDlg->OnSpinnerChange(LOWORD(wParam));
		p_ParamDlg->m_pNitroMtl->NotifyChanged();
		return FALSE;
	case WM_COMMAND:
		p_ParamDlg->OnButtonPressed(LOWORD(wParam));
		return FALSE;			
	case WM_DESTROY:
		p_ParamDlg->OnDestroy();
		return FALSE;
	}
	return FALSE;
}