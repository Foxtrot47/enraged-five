#include "dialogHeader.h"
#include "dialogShader.h"
#include "atl/string.h"
#include "dbeditor.h"
#include "grmodel/shader.h"

extern HINSTANCE g_hDllInstance;

using namespace rage;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RsgMaxDlgHdr::RsgMaxDlgHdr(RageMaxNitroMtl* p_Mtl,IMtlParams* p_MtlParams):
m_pNitroMtl(p_Mtl),
m_pMtlParam(p_MtlParams),
m_pShaderDlg(NULL),
m_Wnd(NULL)
{
	m_Wnd = p_MtlParams->AddRollupPage(g_hDllInstance,
		MAKEINTRESOURCE(IDD_DLG_HEADER),
		RsgMaxParamDlgProc,
		_T("RSG Nitrous Material"),
		(LPARAM)this,0,0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RsgMaxDlgHdr::~RsgMaxDlgHdr()
{
	if(m_pNitroMtl)
	{
		m_pNitroMtl->m_pMtlDlg = NULL;
	}

	delete m_pShaderDlg;
	m_pShaderDlg = NULL;
		
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgHdr::SetThing(ReferenceTarget *m)
{
	if(m_pNitroMtl)
		m_pNitroMtl->m_pMtlDlg = NULL;

	m_pNitroMtl = (RageMaxNitroMtl*)m; 

	if(m_pNitroMtl)
		m_pNitroMtl->m_pMtlDlg = this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RsgMaxDlgHdr::FindSubTexFromHWND(HWND hwnd)
{
	return NULL; // Ever used?!
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgHdr::OnInitDialog(HWND DlgWnd)
{
	SetWindowLongPtr(DlgWnd,GWLP_USERDATA,(LONG_PTR)this);
	m_Wnd = DlgWnd;
	SetupShader(m_pNitroMtl->GetShaderName().c_str());
	
	/*
	int i,ShaderCount = RstMax::GetShaderCount();
	const char* p_ShaderName;

	HWND ListWnd = GetDlgItem(DlgWnd,IDC_CBO_SHADERNAME);
	EnableWindow(ListWnd,TRUE);

	for(i=0;i<ShaderCount;i++)
	{
		p_ShaderName = RstMax::GetShaderName(i);
		SendMessage(ListWnd,CB_ADDSTRING,0,(LPARAM)p_ShaderName);
	}

	ListWnd = GetDlgItem(DlgWnd,IDC_CBO_TEX_SCALE);

	float fMult = 1.0f;
	for(i=0;i<4;i++)
	{
		char cName[64];
		sprintf(cName, "%f", fMult);
		SendMessage(ListWnd,CB_ADDSTRING,0,(LPARAM)cName);
		fMult *= 0.5f;
	}
	*/
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgHdr::SetPresetName( const char *preset )
{
	char presetName[_MAX_FNAME + 1];
	const char *lastSep = strrchr( preset,'\\' );
	if ( lastSep )
	{
		strcpy( presetName,lastSep + 1 );
	}
	SetWindowText( GetDlgItem( m_Wnd,IDC_BTN_PRESET ),lastSep ? presetName:preset );
}

//////////////////////////////////////////////////////////////////////////
void RsgMaxDlgHdr::ConstructShaderParamDialog( grcEffect* p_Shader )
{
	bool Locked = false;
	RsgMaxDlgShader* pShaderDlg = NULL;

	if(m_pShaderDlg)
	{
		delete m_pShaderDlg;
	}
	m_pShaderDlg = new RsgMaxDlgShader( m_pNitroMtl, m_pMtlParam, p_Shader);
	m_pShaderDlg->Create();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgHdr::SetupShader( const char* preset )
{
	ShaderDbEntry* p_Entry = ShaderManager::GetDbEntry(atString(preset));
	grcEffect* p_Shader = &p_Entry->m_Preset->GetBasis();
	
	m_pNitroMtl->MapShaderToInstanceData();
	ConstructShaderParamDialog( p_Shader );
	SetPresetName( preset );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RsgMaxDlgHdr::SetShader( )
{
	m_pNitroMtl->SetNitroShader(  );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK RsgMaxDlgHdr::RsgMaxParamDlgProc( HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam ) 
{
	RsgMaxDlgHdr* p_ParamDlg = ( RsgMaxDlgHdr* )GetWindowLongPtr( DlgWnd,GWLP_USERDATA );
	switch( msg )
	{
	case WM_INITDIALOG:
		( ( RsgMaxDlgHdr* )lParam )->OnInitDialog( DlgWnd );
		return TRUE;
	case WM_COMMAND:
		if( lParam && ( LOWORD( wParam ) == IDC_BTN_PRESET ) )
		{
			
			atString preset = atString( p_ParamDlg->m_pNitroMtl->GetShaderName() );
			DbEditor::GetInst().ChoosePreset(DlgWnd,preset);
			if ((preset != p_ParamDlg->m_pNitroMtl->m_ShaderName) &&
				ShaderManager::GetDbEntry(preset))
			{
				p_ParamDlg->m_pNitroMtl->SetShaderName(preset);
				
				
				p_ParamDlg->SetupShader(preset);//,true);
				p_ParamDlg->SetPresetName(preset);

				MSLShaderPreset* nitroPreset = ShaderManager::GetNitroDBEntry(preset);

				if (nitroPreset)
				{
					p_ParamDlg->m_pNitroMtl->m_pShaderFile = nitroPreset->m_XMSLFilename.c_str();
					p_ParamDlg->m_pNitroMtl->m_pClassName = nitroPreset->m_MSLClassName.c_str();
				}

				p_ParamDlg->m_pNitroMtl->SetNitroShader();
			}
			return TRUE;
		}
	}
	return FALSE;
}