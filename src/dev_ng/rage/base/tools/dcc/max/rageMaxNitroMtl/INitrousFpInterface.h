#ifndef _INITROUSFPINTERFACE_H_
#define _INITROUSFPINTERFACE_H_

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
#include "iFnPub.h"

#define RSG_NITRO_INTERFACE Interface_ID(0x3e957f76, 0x29d6129d)



#pragma warning(push)
#pragma warning(disable:4265)
#pragma warning(disable:4100)

class INitrousFpInterface : public FPMixinInterface
{
public:
	enum { 
		RsgSetShader,
		RsgSetDiffuse
	};

	//BEGIN_FUNCTION_MAP
	//	FN_1( RsgSetShader, TYPE_BOOL, SetShaderPath, TYPE_TSTR_BV);
	//	FN_1( RsgSetDiffuse, TYPE_BOOL, SetDiffusePath, TYPE_TSTR_BV);
	//END_FUNCTION_MAP
		FPInterfaceDesc* GetDesc();

public:
	//virtual bool	SetShaderPath( const char* preset ) = 0;
	//virtual bool	SetDiffusePath( const char* diffusemap ) = 0;
};

#pragma warning(pop)

#endif //_INITROUSFPINTERFACE_H_