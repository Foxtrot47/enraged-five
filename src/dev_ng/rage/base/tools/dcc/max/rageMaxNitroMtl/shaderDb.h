#ifndef MAX_SHADERDB_H
#define MAX_SHADERDB_H

#include "parser/manager.h"
#include "atl/array.h"
#include "atl/map.h"

namespace rage {

class grcInstanceData;

struct ShaderDbEntry {
	grcInstanceData*	m_Preset;
	s32					m_TotalVariableCount;
	s32					m_DbOnlyCount;
};

class MSLShaderPreset
{
public:
	MSLShaderPreset() {}
	MSLShaderPreset(const char* presetName, const char* mslFilename, const char* xmslFilename, const char* mslClassName) :
		m_MSLFilename(mslFilename),
		m_XMSLFilename(xmslFilename),
		m_MSLPresetName(presetName),
		m_MSLClassName(mslClassName)
	{
	}

	virtual ~MSLShaderPreset() {}

public:
	ConstString m_MSLPresetName;
	ConstString m_MSLFilename;
	ConstString m_XMSLFilename;
	ConstString m_MSLClassName;

	PAR_SIMPLE_PARSABLE;
};

class MSLRageShaderPreset
{
public:
	MSLRageShaderPreset() {}
	MSLRageShaderPreset(const char* shaderName, ConstString mslPreset) :
		m_RageShaderPreset(shaderName),
		m_MSLShaderPreset(mslPreset)
	{
	}

	virtual ~MSLRageShaderPreset() {}
public:
	ConstString		m_RageShaderPreset;
	ConstString		m_MSLShaderPreset;

	PAR_SIMPLE_PARSABLE;
};

class MSLShaderPresetMapping
{
public:
	MSLShaderPresetMapping()
	{
		m_DefaultMSLPreset = "V_Diffuse";
	}

	virtual ~MSLShaderPresetMapping() {}

public:
	ConstString						m_DefaultMSLPreset;
	atArray<MSLShaderPreset>		m_MSLShaderPresets;
	atArray<MSLRageShaderPreset>	m_RageShaderPresets;

	PAR_SIMPLE_PARSABLE;
};

class ShaderDb
{
public:
	ShaderDb();
	~ShaderDb();
	
	// PURPOSE: Build up the top level info for the shader entries for the current level
	// PARAMS: level - the "relative path" to the current level of the database (from the database root)
	// RETURNS: The number of items in this level of the database
	int BuildEntryList( const char *level );
	
	// PURPOSE: Set the root path of the shader database
	// PARAMS: path - the explicit path to the root of the shader database
	bool SetShaderDbPath(const char *path);

	// PURPOSE: Create a new group of preset entries
	// PARAMS: folder - complete relative path from the db root to the folder to create
	bool CreateGroup( const char *folder );

	void LoadMSLShaderMapping();
	void PopulateShaderPresets();
	bool LoadMSLShaderMappingFile(const char* filePath);
	bool SaveMSLShaderMappingFile(const char* filePath);

	// RETURNS: The RAGE shader preset matching the specified name
	// PARAMS: entryName - the name of the preset's entry within the database 
	//	(including relative pathing from root)
	ShaderDbEntry *GetItem( const char *entryName );

	// RETURNS: The MSL shader preset matching the specified name
	// PARAMS: entryName - the name of the preset's entry within the database 
	MSLShaderPreset* GetMSLItem( const char* entryName );

	// RETURNS: The name of the specified shader db item
	inline const char *GetEntryName( int idx ) const;

	// RETURNS: True if specified shader entry actually refers to a new group of shaders
	inline bool IsGroup( int idx ) const;

	// RETURNS: True if specified entry is currently locked
	inline bool IsLocked( const ShaderDbEntry *entry ) const;

protected:
	ShaderDbEntry *CreateEntryForPreset( grcInstanceData *preset );
	struct ShaderDbItem {
		ShaderDbItem() : m_IsGroup(false) { /*EMPTY*/ }
		char m_Name[256];
		bool m_IsGroup;
	};

	atArray<ShaderDbItem>						m_CurrentItems;
	atMap<ConstString, ShaderDbEntry *>	m_Presets;
	ShaderDbEntry *								m_LockedEntry;

	bool										m_LoadedShaderPresets;
	atMap<ConstString, MSLShaderPreset*>		m_MSLPresets;
	MSLShaderPresetMapping*						m_MSLPresetMapping;
	MSLShaderPreset*							m_defaultPreset;
};

inline const char *ShaderDb::GetEntryName( int idx ) const {
	return m_CurrentItems[idx].m_Name;
}

inline bool ShaderDb::IsGroup( int idx ) const {
	return m_CurrentItems[idx].m_IsGroup;
}

inline bool ShaderDb::IsLocked( const ShaderDbEntry *entry ) const {
	return (entry == m_LockedEntry);
}

} // namesapce rage
#endif // MAX_SHADERDB_H