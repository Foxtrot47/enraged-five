#ifndef __MAX_SHADERCONTROLS_H__
#define __MAX_SHADERCONTROLS_H__

#include "grcore/effect.h"
#include "dialogResBuilder.h"
#include "rageMaxMaterial/shaderInstance.h"

namespace rage {

class ShaderControls
{
public:
	ShaderControls( grcEffect* shaderInstance, ShaderInstanceData* shaderInstanceData, bool isPreset, atString shaderName);
	~ShaderControls();

	struct FloatSpinnerWdgt
	{
		s32 Idx;
		f32 Min;
		f32 Max;
		f32 Step;

		u32 SpinnerID;
		u32 EditID;
		ISpinnerControl* p_SpinCtrl;
		ICustEdit* p_EditCtrl;
	};

	struct IntSpinnerWdgt
	{
		s32 Idx;
		s32 Min;
		s32 Max;
		s32 Step;

		u32 SpinnerID;
		u32 EditID;
		ISpinnerControl* p_SpinCtrl;
		ICustEdit* p_EditCtrl;
	};

	struct TextureWdgt
	{
		s32 Idx;
		s32 ButtonID;
		ICustButton* p_ButCtrl;
		s32 ButtonAlphaID;
		ICustButton* p_ButAlphaCtrl;

		ICustButton* p_SutSubAttrCtrl;
		s32 ButtonSubAttrDialogID;
	};

	struct Vector2Wdgt
	{
		s32 Idx;
		FloatSpinnerWdgt Float[2];
	};

	struct Vector3Wdgt
	{
		s32 Idx;
		FloatSpinnerWdgt Float[3];
		s32 ButtonID;
	};

	struct Vector4Wdgt
	{
		s32 Idx;
		FloatSpinnerWdgt Float[4];
		s32 ButtonID;
	};

	struct BoolWdgt
	{
		s32 Idx;
		s32 ButtonID;
	};

	FloatSpinnerWdgt&	GetFloatWidget( u32 idx ) { return m_FloatSpinners[idx]; }
	IntSpinnerWdgt&		GetIntWidget( u32 idx ) { return m_IntSpinners[idx]; }
	TextureWdgt&		GetTextureWidget( u32 idx ) { return m_Textures[idx]; }
	Vector2Wdgt&		GetVector2Widget( u32 idx ) { return m_Vector2s[idx]; }
	Vector3Wdgt&		GetVector3Widget( u32 idx ) { return m_Vector3s[idx]; }
	Vector4Wdgt&		GetVector4Widget( u32 idx ) { return m_Vector4s[idx]; }
	BoolWdgt&			GetBoolWidget( u32 idx )	{ return m_Bools[idx]; }

	u32	GetFloatWidgetCount( ) { return m_FloatSpinners.GetCount(); }
	u32	GetIntWidgetCount( ) { return m_IntSpinners.GetCount(); }
	u32	GetTextureWidgetCount( ) { return m_Textures.GetCount(); }
	u32	GetVector2WidgetCount( ) { return m_Vector2s.GetCount(); }
	u32	GetVector3WidgetCount( ) { return m_Vector3s.GetCount(); }
	u32	GetVector4WidgetCount( ) { return m_Vector4s.GetCount(); }
	u32	GetBoolWidgetCount( ) { return m_Bools.GetCount(); }
	
	s32	GetWidth( ) { return m_Width; }
	s32	GetHeight( ) { return m_Height; }


	DialogResBuilder&	GetDialogResBuilder() { return m_DialogRes; }
	void				ReleaseAllControls();

protected:
	void AssembleControls( grcEffect* shaderInstance, ShaderInstanceData* shaderInstanceData , bool isPreset, atString shaderName );

	void AddStatic(const atString& r_Name);
	void AddStaticString(const atString& r_Name,const atString& r_String);
	void AddFloatSpinner(const atString& r_Name,s32 idx,float min,float max,float step);
	void AddIntSpinner(const atString& r_Name,s32 idx,s32 min,s32 max,s32 step);
	void AddMapSelect(const atString& r_Name,s32 idx,const atString& r_MapName, atString& r_TemplateName);
	void AddMapSelect(const atString& r_Name,s32 idx,const atString& r_MapName,const atString& r_AlphaName, atString& r_TemplateName);
	void AddVector2Select(const atString& r_Name,s32 idx,float min, float max, float step);
	void AddVector3Select(const atString& r_Name,s32 idx,float min,float max,float step);
	void AddVector4Select(const atString& r_Name,s32 idx,float min,float max,float step);
	void AddBoolSelect(const atString& r_Name,s32 idx);

	void Reset();

	DialogResBuilder	m_DialogRes;
	s32					m_Width;
	s32					m_Height;
	s32					m_ControlHeight;
	s32					m_ControlGap;
	s32					m_BorderWidth;
	s32					m_LabelWidth;
	u32					m_ID;

	atArray<FloatSpinnerWdgt> m_FloatSpinners;
	atArray<IntSpinnerWdgt> m_IntSpinners;
	atArray<TextureWdgt> m_Textures;
	atArray<Vector2Wdgt> m_Vector2s;
	atArray<Vector3Wdgt> m_Vector3s;
	atArray<Vector4Wdgt> m_Vector4s;
	atArray<BoolWdgt> m_Bools;
};

} // namespace rage

#endif // __MAX_SHADERCONTROLS_H__