#ifndef __MAX_DIALOGSHADER_H__
#define __MAX_DIALOGSHADER_H__

#include "rageMaxNitroMtl.h"
#include "shaderControls.h"

class RageMaxNitroMtl;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
class RsgMaxDlgShader : public ParamDlg
{
public:
	friend class RsgMaxDlgHdr;
	
	RsgMaxDlgShader( RageMaxNitroMtl* p_Mtl, IMtlParams* p_MtlParams, rage::grcEffect* pShader);//rage::ShaderControls* p_ShaderControls );
	virtual ~RsgMaxDlgShader();

	// from ParamDlg
	Class_ID ClassID() { return RAGE_MAX_NITRO_MTL_CLASS_ID; }
	void SetThing(ReferenceTarget *m) {}
	ReferenceTarget* GetThing();
	void SetTime(TimeValue t) {}
	void ReloadDialog() {}
	void DeleteThis();
	void ActivateDlg(BOOL onOff) {}
	int FindSubTexFromHWND(HWND hwnd);

	// Custom
	void UpdateMapName(int Index,const char* r_MapName);
	void Reset();
	HWND Create(TSTR title="Shader Parameters");

	static INT_PTR CALLBACK RstMaxMtlDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);
	//void UpdateMapName(s32 Index,const atString& r_MapName);

	typedef void (*ControlValueChangeEventFunctor)(const char* shaderName, const char* attributeName);
	void SetControlValueChangeFunctor(ControlValueChangeEventFunctor func) { m_ChangeFunctor = func; }
protected:

	void OnInitDialog(HWND DlgWnd);
	void OnSpinnerChange(rage::s32 spinnerID);
	void OnButtonPressed(rage::s32 buttonID);
	void OnDestroy();
	//void SetupShader( const char* filename );
	//void SetDiffuseButton( char* diffuse );
	void SetupUI();

	ControlValueChangeEventFunctor m_ChangeFunctor;
private:
	TexDADMgr					m_DadMgr;
	RageMaxNitroMtl*			m_pNitroMtl;
	IMtlParams*					m_pMtlParams;
	HWND						m_Wnd;
	bool						m_Initialising;
	rage::ShaderControls*		m_ShaderControls;

	//DialogRes m_DialogRes;
};

#endif //__MAX_DIALOGSHADER_H__