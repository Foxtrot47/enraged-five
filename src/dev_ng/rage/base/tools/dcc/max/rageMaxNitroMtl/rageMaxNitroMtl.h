#pragma once

#define RAGE_MAX_NITRO_MTL_CLASS_ID	Class_ID(0x80fece75, 0x6c53a1ca)
#define NSUBMTL		1 // TODO: number of sub-materials supported by this plugin 

#ifdef RSTNITROMTL_DLL
#define RSTNITROMTL_EXPORT __declspec( dllexport )
#else
#ifdef RSTNITROMTL_NOIMPORT
#define RSTNITROMTL_EXPORT 
#else
#define RSTNITROMTL_EXPORT __declspec( dllimport )
#endif //RSTMAXMTL_NOIMPORT
#endif //RSTMAXMTL_DLL

#include "3dsmaxsdk_preinclude.h"
#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
//SIMPLE TYPE
#include <imtl.h>
#include <stdmat.h> 
#include <graphics/IShaderManager.h>
#include <graphics/IParameterTranslator.h>
#include "INitrousFpInterface.h"
#include "ShaderManager.h"
#include "shaderControls.h"
#include "rageMaxMaterial/shaderInstance.h"

#include "atl/string.h"

using namespace MaxSDK::Graphics;

extern TCHAR *GetString(int id);

extern HINSTANCE g_hDllInstance;

class RageMaxNitroMtl : public Mtl, public INitrousFpInterface, public IParameterTranslator {
public:
	friend class RsgMaxDlgHdr;
	friend class RsgMaxDlgShader;

	
	// Parameter block
	IParamBlock2	*pblock;	//ref 0


	Mtl				*submtl[NSUBMTL];  //array of sub-materials
	BOOL			mapOn[NSUBMTL];
	float			spin;
	Interval		ivalid;

	ParamDlg *CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp);
	void Update(TimeValue t, Interval& valid);
	Interval Validity(TimeValue t);
	void Reset();

	void NotifyChanged();

	// From MtlBase and Mtl
	void SetAmbient(Color c, TimeValue t);		
	void SetDiffuse(Color c, TimeValue t);		
	void SetSpecular(Color c, TimeValue t);
	void SetShininess(float v, TimeValue t);
	Color GetAmbient(int mtlNum=0, BOOL backFace=FALSE);
	Color GetDiffuse(int mtlNum=0, BOOL backFace=FALSE);
	Color GetSpecular(int mtlNum=0, BOOL backFace=FALSE);
	float GetXParency(int mtlNum=0, BOOL backFace=FALSE);
	float GetShininess(int mtlNum=0, BOOL backFace=FALSE);		
	float GetShinStr(int mtlNum=0, BOOL backFace=FALSE);
	float WireSize(int mtlNum=0, BOOL backFace=FALSE);


	// Shade and displacement calculation
	void Shade(ShadeContext& sc);
	float EvalDisplacement(ShadeContext& sc); 
	Interval DisplacementValidity(TimeValue t); 	

	// SubMaterial access methods
	/*int NumSubMtls() {return NSUBMTL;}
	Mtl* GetSubMtl(int i);
	void SetSubMtl(int i, Mtl *m);
	TSTR GetSubMtlSlotName(int i);
	TSTR GetSubMtlTVName(int i);
	*/
	// SubTexmap access methods
	void UpdateMapName(int Index,const char* r_MapName);
	int NumSubTexmaps();
	Texmap* GetSubTexmap(int i);
	void SetSubTexmap(int i, Texmap *m);
	TSTR GetSubTexmapSlotName(int i);
	TSTR GetSubTexmapTVName(int i);

	BOOL SetDlgThing(ParamDlg* dlg);
	RageMaxNitroMtl(BOOL loading);
	RSTNITROMTL_EXPORT RageMaxNitroMtl(rage::ShaderInstanceData* instanceData, rage::atString shaderName);
	~RageMaxNitroMtl();
	BaseInterface*	GetInterface(Interface_ID iid);

	// Loading/Saving
	IOResult		Load(ILoad *iload);
	IOResult		Save(ISave *isave);

	//From Animatable
	Class_ID		ClassID() {return RAGE_MAX_NITRO_MTL_CLASS_ID;}		
	SClass_ID		SuperClassID() { return MATERIAL_CLASS_ID; }
	void			GetClassName(TSTR& s) {s = GetString(IDS_CLASS_NAME);}

	RefTargetHandle Clone( RemapDir &remap );
	RefResult		NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message);


	int				NumSubs();// { return 1+NSUBMTL; }
	Animatable*		SubAnim(int i); 
	//TSTR			SubAnimName(int i);

	// TODO: Maintain the number or references here 
	int				NumRefs();// { return 1+NSUBMTL; }
	RefTargetHandle GetReference(int i);
	void			SetReference(int i, RefTargetHandle rtarg);



	int				NumParamBlocks() { return 1; }					// return number of ParamBlocks in this instance
	IParamBlock2*	GetParamBlock(int i) { return pblock; } // return i'th ParamBlock
	IParamBlock2*	GetParamBlockByID(BlockID id) { return (pblock->ID() == id) ? pblock : NULL; } // return id'd ParamBlock

	void			DeleteThis() { delete this; }

	// Nitrous
	IShaderManager* GetShaderManager(void);
	virtual bool	GetParameterValue(const TimeValue t, const MCHAR* shaderParamName,	void* value, IParameterTranslator::ShaderParameterType shaderParamType);
	virtual bool	GetShaderInputParameterName(IParameterTranslator::SubMtlBaseType shaderType, int subMtlBaseIndex, MSTR& shaderParamName);
	virtual bool	OnPreParameterTranslate();

	// Custom
	void			SetNitroShader( );
	
	IMtlParams*		GetMtlParams(){return m_pMtlParam;}

	rage::atString	GetShaderName() { return m_ShaderName; }
	void			SetShaderName( rage::atString shaderName ) { m_ShaderName = shaderName; }

	void			MapShaderToInstanceData();
	rage::ShaderInstanceData*	GetShaderInstanceData() { return m_pShaderInstanceData; }

	// LPXO: TODO: Either add an accessor or fucking sort it out
	RsgMaxDlgHdr*				m_pMtlDlg;

	// Var data
	int GetVariableCount();
	/*grcEffect::VarType GetVariableType(s32 i);
	void GetVariable(s32 i,atString& r_texmap);
	void GetVariable(s32 i,f32& r_float);
	void GetVariable(s32 i,s32& r_int);
	void GetVariable(s32 i,Vector2& r_vec);
	void GetVariable(s32 i,Vector3& r_vec);
	void GetVariable(s32 i,Vector4& r_vec);
	void GetVariable(s32 i,bool& r_bool);

	void SetVariable(s32 i,const atString& r_texmap);
	void SetVariable(s32 i,const f32& r_float);
	void SetVariable(s32 i,const s32& r_int);
	void SetVariable(s32 i,const Vector2& r_vec);
	void SetVariable(s32 i,const Vector3& r_vec);
	void SetVariable(s32 i,const Vector4& r_vec);
	void SetVariable(s32 i,const bool& r_bool);*/
	
	bool IsPreset() const { return m_IsPreset; }
	bool IsTwoSided() const { return m_IsTwoSided; }

	void SetIsPreset( bool isPreset ) { m_IsPreset = isPreset; }
	void SetIsTwoSided( bool isTwoSided ) { m_IsTwoSided = isTwoSided; }
	
private:
	enum
	{
		CURRENT_FILE_VERSION = 1
	};


	enum
	{
		FLAG_NONE = 0,
		FLAG_ISPRESET = 1,
		FLAG_ISTWOSIDED = 2,
		FLAG_OVERRIDETEXSCALE = 4,
		FLAG_HASLIGHTING = 8,
		FLAG_HASAMBLIGHTING = 16,
	};

	int GetDataSize();
	//ShaderVar* GetVar(const grmVariableInfo& r_VarInfo);

	HWND						m_MtlEditWnd;
	IMtlParams*					m_pMtlParam;
	IShaderManager*				m_pIShaderManager;
	
	rage::atString				m_pShaderFile;
	rage::atString				m_pClassName;
	rage::atString				m_ShaderName;
	
	rage::ShaderInstanceData*	m_pShaderInstanceData;

	bool						m_IsPreset;
	bool						m_IsTwoSided;
};
