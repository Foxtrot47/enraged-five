#include "rageMaxNitroMtl.h"
#include "dialogHeader.h"
#include "dialogShader.h"
#include "shaderInstance.h"
#include "ShaderInstancePreset.h"
#include "ShaderInstanceRageShader.h"

#include "grcore/effect.h"
#include "file/device.h"
#include "utility.h"

#define PBLOCK_REF	NSUBMTL
#define NITRO_DATA_CHUNK 1000 

using namespace rage;

class RageMaxNitroMtlClassDesc : public ClassDesc2 
{
public:
	virtual int IsPublic() 							{ return TRUE; }
	virtual void* Create(BOOL loading = FALSE) 		{ return new RageMaxNitroMtl(loading); }
	virtual const TCHAR *	ClassName() 			{ return GetString(IDS_CLASS_NAME); }
	virtual SClass_ID SuperClassID() 				{ return MATERIAL_CLASS_ID; }
	virtual Class_ID ClassID() 						{ return RAGE_MAX_NITRO_MTL_CLASS_ID; }
	virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }

	virtual const TCHAR* InternalName() 			{ return _T("Rage Nitrous"); }	// returns fixed parsable name (scripter-visible name)
	virtual HINSTANCE HInstance() 					{ return g_hDllInstance; }					// returns owning module handle
	

};

static RageMaxNitroMtlClassDesc MtlMetaSLDesc;

static FPInterfaceDesc rsg_nitro_mixininterface( RSG_NITRO_INTERFACE,
						_T("rsgNitroMaterial"), 0,
						&MtlMetaSLDesc, FP_MIXIN,

						INitrousFpInterface::RsgSetShader, 
						_T("RsgSetShader"), 0, TYPE_BOOL, 0, 1,
						_T("shaderPath"), 0, TYPE_TSTR_BV,
						INitrousFpInterface::RsgSetDiffuse, 
						_T("RsgSetDiffuse"), 0, TYPE_BOOL, 0, 1,
						_T("diffusePath"), 0, TYPE_TSTR_BV,

						end
						);

FPInterfaceDesc* INitrousFpInterface::GetDesc()
{
	return &rsg_nitro_mixininterface;
}

ClassDesc2* GetMtlMetaSLDesc() { 
	
	return &MtlMetaSLDesc; 
}

RageMaxNitroMtl::RageMaxNitroMtl(BOOL loading)
	: m_pMtlDlg(NULL)
	, m_pMtlParam(NULL)
	, m_pShaderInstanceData(NULL)
	, m_IsPreset(true)
	, m_IsTwoSided(false)
{
   // It's not recommended to create the IShaderManager in the plug-in's constructor 
   // since the shader manager needs a pointer to the plug-in, whichm hasn't been
   // fully constructed yet. Therefore calls the shader manager may make to 
   // virtual methods of the plug-in may lead to a crash.
	m_pIShaderManager = NULL;
	SetMtlFlag(MTL_HW_MAT_ENABLED);
	for (int i=0; i<NSUBMTL; i++) submtl[i] = NULL;
	pblock = NULL;

	if (!loading) 
		Reset();

	m_ShaderName = "";
	//m_pShaderFile = "X:/gta5/build/dev/common/shaders/metaSL/V_alpha.xmsl";
	m_pShaderFile = "X:/gta5/build/dev_ng/common/shaders/fx_max/terrain_cb_4lyr.fx";
	m_pClassName = "V_diffuse";
	m_pShaderInstanceData = new ShaderInstanceRageShader();
	
	MapShaderToInstanceData();
}

RageMaxNitroMtl::RageMaxNitroMtl(ShaderInstanceData* instanceData, atString shaderName)
	: m_pMtlDlg(NULL)
	, m_pMtlParam(NULL)
	, m_pShaderInstanceData(NULL)
	, m_IsPreset(true)
	, m_IsTwoSided(false)
{
	// It's not recommended to create the IShaderManager in the plug-in's constructor 
	// since the shader manager needs a pointer to the plug-in, whichm hasn't been
	// fully constructed yet. Therefore calls the shader manager may make to 
	// virtual methods of the plug-in may lead to a crash.
	m_pIShaderManager = NULL;
	SetMtlFlag(MTL_HW_MAT_ENABLED);
	for (int i=0; i<NSUBMTL; i++) submtl[i] = NULL;
	pblock = NULL;

	m_ShaderName = shaderName;

	MSLShaderPreset* nitroPreset = ShaderManager::GetNitroDBEntry(shaderName);
	if (nitroPreset)
	{
		m_pShaderFile = nitroPreset->m_XMSLFilename.c_str();
		m_pClassName = nitroPreset->m_MSLClassName.c_str();
	}

	m_pShaderInstanceData = new ShaderInstanceRageShader();

	if (instanceData)
		m_pShaderInstanceData->Copy(*instanceData);

	MapShaderToInstanceData();
}

RageMaxNitroMtl::~RageMaxNitroMtl() 
{
   // Do not call the delete operator in mShaderManager, ask IShaderManagerCreator 
   // to destroy it properly.
   IShaderManagerCreator::GetInstance()->DeleteShaderManager(m_pIShaderManager);
   m_pIShaderManager = NULL;
   
   //delete m_pShaderDlg;
   //m_pShaderDlg = NULL;

   //delete [] m_pShaderFile;
   //m_pShaderFile = NULL;
   
   if( m_pIShaderManager )
   {
	   IShaderManagerCreator::GetInstance()->DeleteShaderManager( m_pIShaderManager );
   }

   delete m_pShaderInstanceData;
   m_pShaderInstanceData = NULL;
}

IShaderManager *RageMaxNitroMtl::GetShaderManager() {
   return m_pIShaderManager;
}

BaseInterface *RageMaxNitroMtl::GetInterface(Interface_ID iid) {
	if(IPARAMETER_TRANSLATOR_INTERFACE_ID == iid){
		return static_cast<IParameterTranslator*>(this);
	} else
	if(ISHADER_MANAGER_INTERFACE_ID == iid)
		// In case the plug-in may have several shaders with one IShaderManager 
		// instance for each, it will need to make sure that the IShaderManager
		// interface pointer that is returned corresponds to the currently active 
		// shader.
		return GetShaderManager();
	else
		return Mtl::GetInterface(iid);
}

bool RageMaxNitroMtl::GetParameterValue(const TimeValue t, const MCHAR* shaderParamName,	void* value, IParameterTranslator::ShaderParameterType shaderParamType)
{
	Interval valid = FOREVER;		
	if (_tcscmp(_M("texture"), shaderParamName) == 0) {
		DbgAssert(IParameterTranslator::ShaderParameterTypeTexture == shaderParamType);
		//Texmap *l_pDiffuseTexture = new BitmapTex(;
		//pblock->GetValue("X:/gta5/art/Textures/ground/__CUNT.bmp", t, l_pDiffuseTexture, valid);
		//BitmapTex* p_bitmapTex = NewDefaultBitmapTex();
		//p_bitmapTex->SetMapName( GetDiffuseMap() );
		if(m_pShaderInstanceData && m_pShaderInstanceData->GetTextureCount())
		{
			if(m_pShaderInstanceData->GetTexture(0)->ClassID() == Class_ID(BMTEX_CLASS_ID,0))
			{
				*(MSTR*)value = ((BitmapTex*)m_pShaderInstanceData->GetTexture(0))->GetMapName();
			}
		}
		return true;
	}
	else if (_tcscmp(_M("alphaTexture"), shaderParamName) == 0) {
		DbgAssert(IParameterTranslator::ShaderParameterTypeTexture == shaderParamType);
		//Texmap *l_pDiffuseTexture = new BitmapTex(;
		//pblock->GetValue("X:/gta5/art/Textures/ground/__CUNT.bmp", t, l_pDiffuseTexture, valid);
		//BitmapTex* p_bitmapTex = NewDefaultBitmapTex();
		//p_bitmapTex->SetMapName( GetDiffuseMap() );
		if(m_pShaderInstanceData && m_pShaderInstanceData->GetTextureCount())
		{
			if(m_pShaderInstanceData->GetTexture(0)->ClassID() == Class_ID(BMTEX_CLASS_ID,0))
			{
				*(MSTR*)value = ((BitmapTex*)m_pShaderInstanceData->GetTextureAlpha(0))->GetMapName();
			}
		}
		return true;
	}
	/*if (_tcscmp(_M("diffuse_color"), shaderParamName) == 0) {
		DbgAssert(IParameterTranslator::ShaderParameterTypeColor == shaderParamType);
		Color l_DiffuseColor(1,1,1);
		pblock->GetValue(diffuse_color, t, l_DiffuseColor, valid);
		*(Color*)value = l_DiffuseColor;
		return true;
	} else
	if (_tcscmp(_M("diffuse_scalar"), shaderParamName) == 0) {
		DbgAssert(IParameterTranslator::ShaderParameterTypeFloat == shaderParamType);
		float l_DiffuseIntensity = 0.7f;
		pblock->GetValue(diffuse_intensity, t, l_DiffuseIntensity, valid);
		*(float*)value = l_DiffuseIntensity;
		return true;
	} else
	if (_tcscmp(_M("Bitmap"), shaderParamName) == 0) {
		DbgAssert(IParameterTranslator::ShaderParameterTypeTexture == shaderParamType);
		Texmap *l_pDiffuseTexture;
		pblock->GetValue(diffuse_texture, t, l_pDiffuseTexture, valid);
		*(MSTR*)value = ((BitmapTex *)l_pDiffuseTexture)->GetMapName();
		return true;
	} else
	if (_tcscmp(_M("texture_space"), shaderParamName) == 0) {
		//DbgAssert(IParameterTranslator::ShaderParameterTypeInt == shaderParamType);
		int l_TextureSpace = 0;
		pblock->GetValue(texture_space, t, l_TextureSpace, valid);
		*(int*)value = l_TextureSpace;
		return true;
	}
	// etc
	DbgAssert(false && _M("Unexpected shader parameter"));
	*/
	return false;
}

bool RageMaxNitroMtl::GetShaderInputParameterName(IParameterTranslator::SubMtlBaseType shaderType, int subMtlBaseIndex, MSTR& shaderParamName)
{
	/*if (shaderType == IParameterTranslator::SubTexmap && 0 == subMtlBaseIndex) {
		shaderParamName = _T("diffuse_texture");// name of input shader parameter corresponding to sub texture map slot 1
		return true;
	}*/
	return false;
}

bool RageMaxNitroMtl::OnPreParameterTranslate() 
{ 
	// Nothing to pre-process or initialize 
	return true; 
}

void RageMaxNitroMtl::Reset() 
{
	ivalid.SetEmpty();
	for (int i=0; i<NSUBMTL; i++) {
		if( submtl[i] ){ 
			DeleteReference(i);
			submtl[i] = NULL;
		}
	}
	//GetMtlMetaSLDesc()->MakeAutoParamBlocks(this);
}

ParamDlg* RageMaxNitroMtl::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp) 
{
	
	m_MtlEditWnd = hwMtlEdit;
	m_pMtlParam = imp;
	m_pMtlDlg = new RsgMaxDlgHdr(const_cast<RageMaxNitroMtl*>(this), imp);
	
	return m_pMtlDlg;
	
	/*
	IAutoMParamDlg* masterDlg = GetMtlMetaSLDesc()->CreateParamDlgs(hwMtlEdit, imp, this);
	masterDlg->AddDlg(m_pMtlDlg);
	// TODO: Set param block user dialog if necessary
	return masterDlg;
	*/
}

BOOL RageMaxNitroMtl::SetDlgThing(ParamDlg* dlg)
{
	return FALSE;
}

Interval RageMaxNitroMtl::Validity(TimeValue t)
{
	/*Color l_Color;
	float l_fValue;
	int l_iValue;
	Texmap *l_pTexmap;*/
	Interval valid = FOREVER;		

	for (int i=0; i<NSUBMTL; i++) 
	{
		if (submtl[i]) 
			valid &= submtl[i]->Validity(t);
	}
	/*	
	pblock->GetValue(diffuse_color, t, l_Color, valid);
	pblock->GetValue(diffuse_intensity, t, l_fValue, valid);
	pblock->GetValue(diffuse_texture, t, l_pTexmap, valid);
	pblock->GetValue(texture_space, t, l_iValue, valid);
	*/
   return valid;
}

/*===========================================================================*\
 |	Subanim & References support
\*===========================================================================*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RageMaxNitroMtl::NumSubs()
{
	return NumRefs();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
Animatable* RageMaxNitroMtl::SubAnim(int i)
{
	return (Animatable*)GetReference(i);
}

int RageMaxNitroMtl::NumRefs()
{
	if(!m_pShaderInstanceData)
		return 0;
	
	return (m_pShaderInstanceData->GetTextureCount() + m_pShaderInstanceData->GetTextureAlphaCount());
}

RefTargetHandle RageMaxNitroMtl::GetReference(int i) 
{
	if((i<0)||(i>=NumRefs()))//||!m_pShaderInstanceData)
	{
		//assert(false);
		return NULL;
	}

	s32 texCount = m_pShaderInstanceData->GetTextureCount();

	if(i < texCount)
	{
		return m_pShaderInstanceData->GetTexture(i);
	}

	return m_pShaderInstanceData->GetTextureAlpha(i - texCount);
}

void RageMaxNitroMtl::SetReference(int i, RefTargetHandle rtarg) 
{
	assert((rtarg == NULL) || (rtarg->SuperClassID() == TEXMAP_CLASS_ID));
	assert(i<NumRefs());
	if(i>=NumRefs())
	{
		return;
	}
	assert(i < (m_pShaderInstanceData->GetTextureCount() << 1));
	s32 texCount = m_pShaderInstanceData->GetTextureCount();

	if(i < texCount)
	{
		m_pShaderInstanceData->SetTexture(i, (Texmap*)rtarg);
	}
	else
	{
		m_pShaderInstanceData->SetTextureAlpha((i - texCount), (Texmap*)rtarg);
	}

	NotifyDependents(FOREVER,PART_ALL,REFMSG_CHANGE);
}

RefResult RageMaxNitroMtl::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message ) 
{
	switch (message) {
		case REFMSG_CHANGE:
			ivalid.SetEmpty();
			/*if (hTarget == pblock)
				{
				ParamID changing_param = pblock->LastNotifyParamID();
				mtlmetasl_param_blk.InvalidateUI(changing_param);
				}*/
			break;

		}
	return REF_SUCCEED;
}



/*===========================================================================*\
 |	SubMtl get and set
\*===========================================================================*/
/*
Mtl* RageMaxNitroMtl::GetSubMtl(int i)
{
	if (i < NSUBMTL )
		return submtl[i];
	return NULL;
}

void RageMaxNitroMtl::SetSubMtl(int i, Mtl *m)
{
	ReplaceReference(i,m);
	// TODO: Set the material and update the UI	
}

TSTR RageMaxNitroMtl::GetSubMtlSlotName(int i)
{
	// Return i'th sub-material name 
	return _T(""); 
}

TSTR RageMaxNitroMtl::GetSubMtlTVName(int i)
{
	return GetSubMtlSlotName(i);
}
*/
/*===========================================================================*\
 |	Texmap get and set
 |  By default, we support none
\*===========================================================================*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RageMaxNitroMtl::NumSubTexmaps()
{
	return NumRefs();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
Texmap* RageMaxNitroMtl::GetSubTexmap(int i)
{
	return (Texmap*)GetReference(i);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RageMaxNitroMtl::UpdateMapName(int Index,const char* r_MapName)
{
	if(m_pMtlDlg)
		m_pMtlDlg->m_pShaderDlg->UpdateMapName(Index,r_MapName);
}



void RageMaxNitroMtl::SetSubTexmap(int i, Texmap *m)
{
	MtlBase* p_Texmap = GetActiveTexmap();

	if(p_Texmap && (p_Texmap != m))
		GetCOREInterface()->DeActivateTexture(p_Texmap,this);

	if(m)
		ReplaceReference(i,m);
	else
		DeleteReference(i);

	if(m_pMtlDlg)
	{
		s32 totalCount = NumSubTexmaps();

		if(i >= totalCount)
			return;

		if(m)
			UpdateMapName(i,RageNitroMtlUtilities::GetTexMapName(m).c_str());
		else
			UpdateMapName(i,"none");
	}
}

TSTR RageMaxNitroMtl::GetSubTexmapSlotName(int i)
{
	return _T("CUNT");
}

TSTR RageMaxNitroMtl::GetSubTexmapTVName(int i)
{
	// Return i'th sub-texture name 
	return GetSubTexmapSlotName(i);
}



/*===========================================================================*\
 |	Standard IO
\*===========================================================================*/

#define MTL_HDR_CHUNK 0x4000

IOResult RageMaxNitroMtl::Save(ISave *isave) { 
	IOResult res;

	//standard material data
	isave->BeginChunk(MTL_HDR_CHUNK);
	res = MtlBase::Save(isave);
	if (res!=IO_OK) return res;
	isave->EndChunk();
	
	if(res != IO_OK)
	{
		return res;
	}

	//rstmtlmax data
	s32 i,writeSize;
	s32 dataCount,dataSize = GetDataSize();
	char* p_Data = new char[dataSize];

	isave->BeginChunk(NITRO_DATA_CHUNK);

	char Filename[1024];
	fiDeviceMemory::MakeMemoryFileName(Filename,1024,p_Data,dataSize,false,"nitromtl save buffer");
	fiDeviceMemory DevMemory;
	fiStream* p_MemStream = fiStream::Create(Filename,DevMemory);

	//version
	s32 version = CURRENT_FILE_VERSION;
	p_MemStream->WriteInt(&version,1);

	//flags
	
	s32 flags = IsPreset ()? FLAG_ISPRESET : 0;
	flags |= IsTwoSided() ? FLAG_ISTWOSIDED : 0;
	// Legacy from old material.  We wont need these unless
	// art want non-defaults when switching between Nitro and
	// RAGE DX
	/*
	flags |= m_OverrideTexScale ? FLAG_OVERRIDETEXSCALE : 0;
	flags |= HasLighting() ? FLAG_HASLIGHTING : 0;
	flags |= HasAmbLighting() ? FLAG_HASAMBLIGHTING : 0;
	*/

	p_MemStream->WriteInt(&flags,1);
	
	//num tex refs
	dataCount = m_pShaderInstanceData->GetTextureCount();
	
	p_MemStream->WriteInt(&dataCount,1);

	//shader name
	RageNitroMtlUtilities::WriteString(p_MemStream,m_ShaderName);

	//variables
	dataCount = m_pShaderInstanceData->GetVariableMap().GetNumUsed();
	p_MemStream->WriteInt(&dataCount,1);

	dataCount = m_pShaderInstanceData->GetVariableMap().GetNumSlots();
	for(i=0;i<dataCount;i++)
	{
		atMap<atString,ShaderInstanceData::ShaderVar>::Entry* p_Entry = m_pShaderInstanceData->GetVariableMap().GetEntry(i);

		while(p_Entry)
		{
			RageNitroMtlUtilities::WriteString(p_MemStream,p_Entry->key);
			p_MemStream->WriteInt((s32*)&p_Entry->data.VarType,1);
			p_MemStream->WriteInt(&p_Entry->data.Idx,1);

			switch(p_Entry->data.VarType)
			{
			case grcEffect::VT_FLOAT:
				writeSize = 4;
				p_MemStream->WriteInt(&writeSize,1);
				p_MemStream->WriteFloat(&m_pShaderInstanceData->GetFloat(p_Entry->data.Idx),1);
				break;
			case grcEffect::VT_INT_DONTUSE:
				writeSize = 4;
				p_MemStream->WriteInt(&writeSize,1);
				p_MemStream->WriteInt(&m_pShaderInstanceData->GetInt(p_Entry->data.Idx),1);
				break;
			case grcEffect::VT_VECTOR2:
				writeSize = 8;
				p_MemStream->WriteInt(&writeSize,1);
				p_MemStream->WriteFloat(&(m_pShaderInstanceData->GetVector2(p_Entry->data.Idx)[0]),1);
				p_MemStream->WriteFloat(&(m_pShaderInstanceData->GetVector2(p_Entry->data.Idx)[1]),1);
				break;
			case grcEffect::VT_VECTOR3:
				writeSize = 12;
				p_MemStream->WriteInt(&writeSize,1);
				p_MemStream->WriteFloat(&(m_pShaderInstanceData->GetVector3(p_Entry->data.Idx)[0]),1);
				p_MemStream->WriteFloat(&(m_pShaderInstanceData->GetVector3(p_Entry->data.Idx)[1]),1);
				p_MemStream->WriteFloat(&(m_pShaderInstanceData->GetVector3(p_Entry->data.Idx)[2]),1);
				break;
			case grcEffect::VT_VECTOR4:
				writeSize = 16;
				p_MemStream->WriteInt(&writeSize,1);
				p_MemStream->WriteFloat(&(m_pShaderInstanceData->GetVector4(p_Entry->data.Idx)[0]),1);
				p_MemStream->WriteFloat(&(m_pShaderInstanceData->GetVector4(p_Entry->data.Idx)[1]),1);
				p_MemStream->WriteFloat(&(m_pShaderInstanceData->GetVector4(p_Entry->data.Idx)[2]),1);
				p_MemStream->WriteFloat(&(m_pShaderInstanceData->GetVector4(p_Entry->data.Idx)[3]),1);
				break;
			case grcEffect::VT_BOOL_DONTUSE:
				{
					writeSize = 4;
					int iTmp = (int)m_pShaderInstanceData->GetBool(p_Entry->data.Idx);
					p_MemStream->WriteInt(&writeSize, 1);
					p_MemStream->WriteInt(&iTmp, 1);
					break;
				}
			case grcEffect::VT_TEXTURE:
			case grcEffect::VT_MATRIX43:
			case grcEffect::VT_MATRIX44:
			default:
				writeSize = 0;
				p_MemStream->WriteInt(&writeSize,1);
			}

			p_Entry = p_Entry->next;
		}
	}

	// Tex scale multiplier
	// Legacy from old material.  Id ratehr default to 1/8th 
	// so lets not store this value unless requested
	//p_MemStream->WriteFloat(&m_TexScaleMultiplier,1);

	// Render mode
	s32 hwEnabled = this->TestMtlFlag( MTL_HW_MAT_ENABLED );
	p_MemStream->WriteInt(&hwEnabled,1);

	//write it out
	p_MemStream->Close();

	s32 TellSize = p_MemStream->Tell();

	assert(TellSize == dataSize);

	ULONG Written;
	isave->Write(&dataSize,4,&Written);
	if(Written != 4)
	{
		delete[] p_Data;
		return IO_ERROR;
	}

	isave->Write(p_Data,dataSize,&Written);
	if(Written != dataSize)
		res = IO_ERROR;

	delete[] p_Data;

	isave->EndChunk();
	return res;

	}	

IOResult RageMaxNitroMtl::Load(ILoad *iload) 
{ 
	IOResult res;
	int id;
	ULONG nb; 
	while (IO_OK==(res=iload->OpenChunk())) {
			switch(id = iload->CurChunkID())  {
				case MTL_HDR_CHUNK:
					res = MtlBase::Load(iload);
					break;
				case NITRO_DATA_CHUNK:
					{
						char* p_Data;

						s32 dataCount,dataSize;
						ULONG read;
						iload->Read(&dataSize,4,&read);

						if(read != 4)
							return IO_ERROR;

						p_Data = new char[dataSize];
						iload->Read(p_Data,dataSize,&read);

						if(read != dataSize)
						{
							delete[] p_Data;
							return IO_ERROR;
						}

						bool TranslateType = false;
						char Filename[1024];
						fiDeviceMemory DevMemory;
						fiDeviceMemory::MakeMemoryFileName(Filename,1024,p_Data,dataSize,false,"nitromtl load buffer");
						fiStream* p_MemStream = fiStream::Create(Filename,DevMemory);

						//version
						s32 version;
						p_MemStream->ReadInt(&version,1);

						s32 flags;
						p_MemStream->ReadInt(&flags,1);

						if(flags & FLAG_ISPRESET)
							SetIsPreset(true);
						else
							SetIsPreset(false);

						if(flags & FLAG_ISTWOSIDED)
							SetIsTwoSided(true);
						else
							SetIsTwoSided(false);
							

						//num tex refs
						assert(m_pShaderInstanceData->GetTextureCount() == 0);
						m_pShaderInstanceData->Reset();

						p_MemStream->ReadInt(&dataCount,1);

						for(int i=0;i<dataCount;i++)
						{
							m_pShaderInstanceData->AddTexture(NULL);
							m_pShaderInstanceData->AddTextureAlpha(NULL);
						}
						//shader name
						RageNitroMtlUtilities::ReadString(p_MemStream,m_ShaderName);

						// Retire gta_ shaders...
						m_ShaderName.Replace("gta_", "");

#if HACK_GTA4
						//convert this to a sps file...

						char Temp[1024];
						strcpy(Temp,m_ShaderName);
						char* p_Dot = strrchr(Temp,'.');

						if(p_Dot)
						{
							p_Dot++;

							if(stricmp(p_Dot,"fx") == 0)
							{
								*p_Dot = '\0';
								strcat(Temp,"sps");
								m_ShaderName = Temp;
								SetIsPreset(true);
							}
						}
#endif //HACK_GTA4

						//variables
						p_MemStream->ReadInt(&dataCount,1);
						for(int i=0;i<dataCount;i++)
						{
							ShaderInstanceData::ShaderVar ReadShaderVar;
							atString VarName;

							RageNitroMtlUtilities::ReadString(p_MemStream,VarName);
							p_MemStream->ReadInt((s32*)&ReadShaderVar.VarType,1);
							p_MemStream->ReadInt(&ReadShaderVar.Idx,1);
							p_MemStream->ReadInt(&dataSize,1);	

							switch(ReadShaderVar.VarType)
							{
							case grcEffect::VT_FLOAT:

								if(dataSize != sizeof(f32))
									break;

								{	
									s32 add = ReadShaderVar.Idx - m_pShaderInstanceData->GetFloatCount() + 1;
									for(int j=0; j<add; j++)
									{
										m_pShaderInstanceData->AddFloat(NULL);
									}

									p_MemStream->ReadFloat(&m_pShaderInstanceData->GetFloat(ReadShaderVar.Idx),1);
									dataSize -= sizeof(f32);
								}

								break;

							case grcEffect::VT_INT_DONTUSE:

								if(dataSize != sizeof(s32))
									break;

								{	
									s32 add = ReadShaderVar.Idx - m_pShaderInstanceData->GetIntCount() + 1;
									for(int j=0; j<add; j++)
									{
										m_pShaderInstanceData->AddInt(NULL);
									}

									p_MemStream->ReadInt(&m_pShaderInstanceData->GetInt(ReadShaderVar.Idx),1);
									dataSize -= sizeof(s32);
								}

								break;

							case grcEffect::VT_VECTOR2:
								if(dataSize != (sizeof(f32) * 2))
									break;

								{
									s32 add = ReadShaderVar.Idx - m_pShaderInstanceData->GetVector2Count() + 1;
									for(int j=0; j<add; j++)
									{
										m_pShaderInstanceData->AddVector2(Vector2(0.0f,0.0f));
									}

									p_MemStream->ReadFloat(&(m_pShaderInstanceData->GetVector2(ReadShaderVar.Idx)[0]),1);
									p_MemStream->ReadFloat(&(m_pShaderInstanceData->GetVector2(ReadShaderVar.Idx)[1]),1);
									dataSize -= (sizeof(f32) * 2);
								}

								break;
							case grcEffect::VT_VECTOR3:

								if(dataSize != (sizeof(f32) * 3))
									break;

								{	
									s32 add = ReadShaderVar.Idx - m_pShaderInstanceData->GetVector3Count() + 1;
									for(int j=0; j<add; j++)
									{
										m_pShaderInstanceData->AddVector3(Vector3(0.0f,0.0f,0.0f));
									}

									p_MemStream->ReadFloat(&(m_pShaderInstanceData->GetVector3(ReadShaderVar.Idx)[0]),1);
									p_MemStream->ReadFloat(&(m_pShaderInstanceData->GetVector3(ReadShaderVar.Idx)[1]),1);
									p_MemStream->ReadFloat(&(m_pShaderInstanceData->GetVector3(ReadShaderVar.Idx)[2]),1);
									dataSize -= (sizeof(f32) * 3);
								}

								break;

							case grcEffect::VT_VECTOR4: 

								if(dataSize != (sizeof(f32) * 4))
									break;

								{	
									s32 add = ReadShaderVar.Idx - m_pShaderInstanceData->GetVector4Count() + 1;
									for(int j=0; j<add; j++)
									{
										m_pShaderInstanceData->AddVector4(Vector4(0.0f));
									}

									p_MemStream->ReadFloat(&(m_pShaderInstanceData->GetVector4(ReadShaderVar.Idx)[0]),1);
									p_MemStream->ReadFloat(&(m_pShaderInstanceData->GetVector4(ReadShaderVar.Idx)[1]),1);
									p_MemStream->ReadFloat(&(m_pShaderInstanceData->GetVector4(ReadShaderVar.Idx)[2]),1);
									p_MemStream->ReadFloat(&(m_pShaderInstanceData->GetVector4(ReadShaderVar.Idx)[3]),1);
									dataSize -= (sizeof(f32) * 4);
								}

								break;

							case grcEffect::VT_BOOL_DONTUSE:
								if(dataSize != (sizeof(int)))
									break;

								{
									s32 add = ReadShaderVar.Idx - m_pShaderInstanceData->GetBoolCount() + 1;
									for(int j=0; j<add; j++)
									{
										m_pShaderInstanceData->AddBool(NULL);
									}

									int iTmpVal;
									p_MemStream->ReadInt(&iTmpVal, 1);
									m_pShaderInstanceData->GetBool(ReadShaderVar.Idx) = (bool)iTmpVal;
									dataSize -= (sizeof(int));
								}

								break;

							case grcEffect::VT_TEXTURE:
							case grcEffect::VT_MATRIX43:
							case grcEffect::VT_MATRIX44:
							default:
								break;
							}
			
							m_pShaderInstanceData->GetVariableMap().Insert(VarName,ReadShaderVar);

							//seek across any extra data size left over
							s32 CurrentPos = p_MemStream->Tell();
							CurrentPos += dataSize;
							p_MemStream->Seek(CurrentPos);

						}

						// tex scale multiplier
						//m_TexScaleMultiplier = 0.125f;
						//p_MemStream->ReadFloat(&m_TexScaleMultiplier,1);
						//dataSize -= (sizeof(float));
						

						// Render mode
						s32 hwEnabled;
						p_MemStream->ReadInt(&hwEnabled,1);
						if( hwEnabled )
							this->SetMtlFlag( MTL_HW_MAT_ENABLED, TRUE );
						else
							this->SetMtlFlag( MTL_HW_MAT_ENABLED, FALSE );
						p_MemStream->Close();

						delete[] p_Data;
					}
					break;
			}
			iload->CloseChunk();
			if (res!=IO_OK) 
				return res;
		}
	SetNitroShader();
	return IO_OK;
}


/*===========================================================================*\
 |	Updating and cloning
\*===========================================================================*/

RefTargetHandle RageMaxNitroMtl::Clone(RemapDir &remap) 
{
	RageMaxNitroMtl* p_New = new RageMaxNitroMtl(FALSE);

	*(p_New->m_pShaderInstanceData) = *m_pShaderInstanceData;

	s32 count = m_pShaderInstanceData->GetTextureCount();

	for(s32 i=0;i<count;i++)
	{
		p_New->m_pShaderInstanceData->AddTexture(NULL);
		p_New->ReplaceReference(i,remap.CloneRef(m_pShaderInstanceData->GetTexture(i)));
	}

	s32 alphaCount = m_pShaderInstanceData->GetTextureAlphaCount();


	for(s32 i=0;i<alphaCount;i++)
	{
		p_New->m_pShaderInstanceData->AddTextureAlpha(NULL);
		p_New->ReplaceReference(i + count,remap.CloneRef(m_pShaderInstanceData->GetTextureAlpha(i)));
	}

	BaseClone(this,p_New,remap);
	p_New->SetNitroShader();

	return p_New;
}

void RageMaxNitroMtl::NotifyChanged() 
{
	NotifyDependents(FOREVER, PART_ALL, REFMSG_CHANGE);
}

void RageMaxNitroMtl::Update(TimeValue t, Interval& valid) 
{	
	if (!ivalid.InInterval(t)) {
		for (int i=0; i<NSUBMTL; i++) {
			if (submtl[i]) 
				submtl[i]->Update(t,ivalid);
		}
	}
	valid &= ivalid;
}

/*===========================================================================*\
 |	Determine the characteristics of the material
\*===========================================================================*/

void RageMaxNitroMtl::SetAmbient(Color c, TimeValue t) {}		
void RageMaxNitroMtl::SetDiffuse(Color c, TimeValue t) {}		
void RageMaxNitroMtl::SetSpecular(Color c, TimeValue t) {}
void RageMaxNitroMtl::SetShininess(float v, TimeValue t) {}
				
Color RageMaxNitroMtl::GetAmbient(int mtlNum, BOOL backFace)
{
	return submtl[0]?submtl[0]->GetAmbient(mtlNum,backFace):Color(0,0,0);
}

Color RageMaxNitroMtl::GetDiffuse(int mtlNum, BOOL backFace)
{
	return submtl[0]?submtl[0]->GetDiffuse(mtlNum,backFace):Color(0,0,0);
}

Color RageMaxNitroMtl::GetSpecular(int mtlNum, BOOL backFace)
{
	return submtl[0]?submtl[0]->GetSpecular(mtlNum,backFace):Color(0,0,0);
}

float RageMaxNitroMtl::GetXParency(int mtlNum, BOOL backFace)
{
	return submtl[0]?submtl[0]->GetXParency(mtlNum,backFace):0.0f;
}

float RageMaxNitroMtl::GetShininess(int mtlNum, BOOL backFace)
{
	return submtl[0]?submtl[0]->GetShininess(mtlNum,backFace):0.0f;
}

float RageMaxNitroMtl::GetShinStr(int mtlNum, BOOL backFace)
{
	return submtl[0]?submtl[0]->GetShinStr(mtlNum,backFace):0.0f;
}

float RageMaxNitroMtl::WireSize(int mtlNum, BOOL backFace)
{
	return submtl[0]?submtl[0]->WireSize(mtlNum,backFace):0.0f;
}

		
/*===========================================================================*\
 |	Actual shading takes place
\*===========================================================================*/

void RageMaxNitroMtl::Shade(ShadeContext& sc) 
{
	Mtl *sm1 = mapOn[0]?submtl[0]:NULL;
	if (gbufID) sc.SetGBufferID(gbufID);

	if(sm1) sm1->Shade(sc);
	
	// TODO: compute the color and transparency output returned in sc.out.
}

float RageMaxNitroMtl::EvalDisplacement(ShadeContext& sc)
{
	Mtl *sm1 = mapOn[0]?submtl[0]:NULL;
	return (sm1)?sm1->EvalDisplacement(sc):0.0f;
}

Interval RageMaxNitroMtl::DisplacementValidity(TimeValue t)
{
	Mtl *sm1 = mapOn[0]?submtl[0]:NULL;

	Interval iv; iv.SetInfinite();
	if(sm1) iv &= sm1->DisplacementValidity(t);

	return iv;	
}

void RageMaxNitroMtl::SetNitroShader(  )
{
	//ASSERT(m_pShaderFile);
	if( m_pIShaderManager )
	{
		IShaderManagerCreator::GetInstance()->DeleteShaderManager( m_pIShaderManager );
	}
	
	m_pIShaderManager = IShaderManagerCreator::GetInstance()->CreateShaderManager(
		IShaderManager::ShaderTypeMetaSL, 
		_M( m_pClassName.c_str() ), // same class name as found in the shader file
		_M( m_pShaderFile.c_str() ), // Full path to a xmsl shader file
		this );

	NotifyChanged();
}

void RageMaxNitroMtl::MapShaderToInstanceData()
{
	ShaderDbEntry* p_Entry = ShaderManager::GetDbEntry(GetShaderName());
	if( p_Entry )
	{
		grcEffect* p_Shader = &p_Entry->m_Preset->GetBasis();
		if( !m_pShaderInstanceData )
		{
			m_pShaderInstanceData = new ShaderInstanceRageShader();
		}
		
		// TL TODO - Sort this out, we don't have access to rageRstShaderDBEntry from this plugin.
		//m_pShaderInstanceData->MapInstanceData( this, p_Entry);
		
		NotifyChanged();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RageMaxNitroMtl::GetDataSize()
{
	s32 i,count;
	s32 size = 0;

	//version
	size += 4;

	//flags
	size += 4;

	//num tex refs
	size += 4;

	//shader name
	size += m_ShaderName.GetLength() + 4;

	//shader variables
	count = m_pShaderInstanceData->GetVariableMap().GetNumSlots();
	size += 4;


	for(i=0;i<count;i++)
	{
		atMap<atString, ShaderInstanceData::ShaderVar>::Entry* p_Entry = m_pShaderInstanceData->GetVariableMap().GetEntry(i);

		while(p_Entry)
		{
			size += p_Entry->key.GetLength() + 12; //key + type + index
			size += 4; // data size

			switch(p_Entry->data.VarType)
			{
			case grcEffect::VT_FLOAT:
			case grcEffect::VT_INT_DONTUSE:
				size += 4;
				break;
			case grcEffect::VT_VECTOR2:
				size += 8;
				break;
			case grcEffect::VT_VECTOR3:
				size += 12;
				break;
			case grcEffect::VT_VECTOR4: 
				size += 16;
				break;
			case grcEffect::VT_BOOL_DONTUSE:
				size += 4;
			case grcEffect::VT_TEXTURE:
			case grcEffect::VT_MATRIX43:
			case grcEffect::VT_MATRIX44:
			default:
				break;
			}

			p_Entry = p_Entry->next;
		}
	}

	// tex scale multiplier
	//Size += 4;

	// Render mode
	size +=4;


	return (int)size;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RageMaxNitroMtl::GetVariableCount()
{	
	grcEffect* p_Shader = ShaderManager::GetInstance()->FindShader(m_ShaderName);

	if(!p_Shader)
	{
		return -1;
	}

	return p_Shader->GetInstancedVariableCount();
}



	

