//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by rageMaxNitroMtl.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDS_diffuse_texture             5
#define IDS_diffuse_color               6
#define IDS_MTL1                        7
#define IDS_MTL1ON                      8
#define IDS_diffuse_intensity           9
#define IDS_glossy_color                10
#define IDS_glossy_intensity            11
#define IDS_glossy_shiness              12
#define IDS_glossy_shinness             12
#define IDS_texture_space               12
#define IDD_PANEL                       101
#define IDD_DLG_HEADER                  101
#define IDD_DLG_SHADER                  104
#define IDD_DLG_DBCHOOSER               105
#define IDB_BMP_ILCHOOSER               106
#define IDB_BITMAP2                     107
#define IDB_BMP_ROCKSTAR                107
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_DiffuseColor                1001
#define IDC_GlossyColor                 1002
#define IIDC_DiffuseTexture             1003
#define IDC_DiffuseTexture              1003
#define IDC_BTN_PRESET                  1005
#define IDC_BTN_DIFFUSE                 1006
#define IDC_TREE_DATA                   1007
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_DiffuseIntensity            1491
#define IDC_GlossyIntensity             1492
#define IDC_GlossyShinness              1493
#define IDC_TextureSpace                1493
#define IDC_SPIN                        1496
#define IDC_MTL1                        1497
#define IDC_MTLON1                      1498
#define IDC_DiffuseIntensitySpin        1499
#define IDC_GlossyIntensitySpin         1500
#define IDC_GlossyShinnessSpin          1501
#define IDC_TextureSpaceSpin            1501

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
