<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef type="::rage::MSLShaderPresetMapping">
	<string name="m_DefaultMSLPreset" type="ConstString" noInit="true"/>
	<array name="m_MSLShaderPresets" type="atArray">
		<struct type="::rage::MSLShaderPreset"/>
	</array>
	<array name="m_RageShaderPresets" type="atArray">
		<struct type="::rage::MSLRageShaderPreset"/>
	</array>
</structdef>

<structdef type="::rage::MSLRageShaderPreset">
	<string name="m_RageShaderPreset" type="ConstString" noInit="true"/>
	<string name="m_MSLShaderPreset" type="ConstString" noInit="true"/>
</structdef>

<structdef type="::rage::MSLShaderPreset">
	<string name="m_MSLFilename" type="ConstString" noInit="true"/>
	<string name="m_XMSLFilename" type="ConstString" noInit="true"/>
	<string name="m_MSLPresetName" type="ConstString" noInit="true"/>
	<string name="m_MSLClassName" type="ConstString" noInit="true"/>
</structdef>

</ParserSchema>