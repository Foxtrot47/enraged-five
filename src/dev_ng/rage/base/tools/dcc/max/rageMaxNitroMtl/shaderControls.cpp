#include "shaderControls.h"
#include "dialogResBuilder.h"
#include "ShaderManager.h"
#include "utility.h"

#include "grmodel/shader.h"
#include "atl/array.h"

using namespace rage;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ShaderControls::ShaderControls( grcEffect* shaderInstance, ShaderInstanceData* shaderInstanceData, bool isPreset, atString shaderName )
{
	AssembleControls( shaderInstance, shaderInstanceData, isPreset, shaderName );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ShaderControls::~ShaderControls()
{
	Reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::Reset()
{
	m_DialogRes.Reset();
	m_FloatSpinners.Reset();
	m_IntSpinners.Reset();
	m_Vector2s.Reset();
	m_Vector3s.Reset();
	m_Vector4s.Reset();
	m_Textures.Reset();

	m_LabelWidth = 60;
	m_Width = 218;	//this is a fixed size for a material rollup in max (left in this variable till Im
	//sure this is correct
	m_ControlHeight = 10;
	m_BorderWidth = 5;
	m_ControlGap = 3;
	m_Height = m_BorderWidth;
	m_ID = 1000;
}

void ShaderControls::AssembleControls( grcEffect* shaderInstance, ShaderInstanceData* shaderInstanceData, bool isPreset, atString shaderName )
{
	Reset();

	const char* p_Name = NULL;
	bool locked = false;
	s32 i,numVars;
	s32 numFloats = 0;
	s32 numInts = 0;
	s32 numTextures = 0;
	s32 numVector2s = 0;
	s32 numVector3s = 0;
	s32 numVector4s = 0;
	s32 numBools = 0;

	p_Name = shaderInstance->GetEffectName();
	numVars = shaderInstance->GetInstancedVariableCount();

	bool isAlpha = ShaderManager::IsAlphaShader( isPreset, shaderName );
	/*if(shaderInstance->GetDrawBucket() > 0)
	{
		isAlpha = true;
	}*/

	for(i=0;i<numVars;i++)
	{
		grmVariableInfo varInfo;

		shaderInstance->GetInstancedVariableInfo(i,varInfo);
		grcEffectVar handle = shaderInstance->LookupVar(varInfo.m_Name);

		atString Label(varInfo.m_UiName);
		Label += ":";
		switch(varInfo.m_Type)
		{
		case grcEffect::VT_FLOAT:
				AddFloatSpinner(	Label,
					numFloats++,
					varInfo.m_UiMin,
					varInfo.m_UiMax,
					varInfo.m_UiStep);
			break;
		case grcEffect::VT_TEXTURE:
			{
				atString TextureTemplate = atString(varInfo.m_TCPTemplate);
				if(isAlpha)
				{
					AddMapSelect(	Label,
						numTextures,
						RageNitroMtlUtilities::GetTexMapName(shaderInstanceData->GetTexture(numTextures)),
						RageNitroMtlUtilities::GetTexMapName(shaderInstanceData->GetTextureAlpha(numTextures)),
						TextureTemplate);
				}
				else
				{
					AddMapSelect(	Label,
						numTextures,
						RageNitroMtlUtilities::GetTexMapName(shaderInstanceData->GetTexture(numTextures)),
						TextureTemplate);
				}
				/*
				if(isAlpha)
				{
					AddMapSelect(	Label,
						numTextures,
						rstMaxUtility::GetTexMapName(m_Textures[numTextures]),
						rstMaxUtility::GetTexMapName(m_TextureAlphas[numTextures]),
						TextureTemplate);
				}
				else
				{
					AddMapSelect(	Label,
						numTextures,
						rstMaxUtility::GetTexMapName(m_Textures[numTextures]),
						TextureTemplate);
				}
				*/
				numTextures++;

				break;
			}
		case grcEffect::VT_VECTOR2:
				AddVector2Select(  Label,
					numVector2s++,
					varInfo.m_UiMin,
					varInfo.m_UiMax,
					varInfo.m_UiStep);
			break;
		case grcEffect::VT_VECTOR3:
				AddVector3Select(	Label,
					numVector3s++,
					varInfo.m_UiMin,
					varInfo.m_UiMax,
					varInfo.m_UiStep);
			break;

		case grcEffect::VT_VECTOR4: 
				AddVector4Select(	Label,
					numVector4s++,
					varInfo.m_UiMin,
					varInfo.m_UiMax,
					varInfo.m_UiStep);
			break;

		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		default:
			AddStatic(atString(varInfo.m_UiName));
			break;
		}

	}

}

void ShaderControls::ReleaseAllControls()
{
	s32 i,j,Count = GetFloatWidgetCount();
	for(i=0;i<Count;i++)
	{
		ReleaseISpinner(GetFloatWidget(i).p_SpinCtrl);
		ReleaseICustEdit(GetFloatWidget(i).p_EditCtrl);
	}

	Count = GetIntWidgetCount();
	for(i=0;i<Count;i++)
	{
		ReleaseISpinner(GetIntWidget(i).p_SpinCtrl);
		ReleaseICustEdit(GetIntWidget(i).p_EditCtrl);
	}

	Count = GetVector2WidgetCount();
	for(i=0;i<Count;i++)
	{
		for(j=0;j<2;j++)
		{
			ReleaseISpinner(GetVector2Widget(i).Float[j].p_SpinCtrl);
			ReleaseICustEdit(GetVector2Widget(i).Float[j].p_EditCtrl);
		}
	}

	Count = GetVector3WidgetCount();
	for(i=0;i<Count;i++)
	{
		for(j=0;j<3;j++)
		{
			ReleaseISpinner(GetVector3Widget(i).Float[j].p_SpinCtrl);
			ReleaseICustEdit(GetVector3Widget(i).Float[j].p_EditCtrl);
		}
	}

	Count = GetVector4WidgetCount();
	for(i=0;i<Count;i++)
	{
		for(j=0;j<4;j++)
		{
			ReleaseISpinner(GetVector4Widget(i).Float[j].p_SpinCtrl);
			ReleaseICustEdit(GetVector4Widget(i).Float[j].p_EditCtrl);
		}
	}

	Count = GetTextureWidgetCount();
	for(i=0;i<Count;i++)
	{
		if(GetTextureWidget(i).p_ButCtrl)
		{
			ReleaseICustButton(GetTextureWidget(i).p_ButCtrl);
		}
		if(GetTextureWidget(i).p_ButAlphaCtrl)
		{
			ReleaseICustButton(GetTextureWidget(i).p_ButAlphaCtrl);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::AddStatic( const atString& r_Name )
{
	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,(m_Width - (m_BorderWidth << 1)) >> 1,m_ControlHeight,m_ID++);

	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::AddStaticString( const atString& r_Name,const atString& r_String )
{
	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID++);
	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD,r_String,m_LabelWidth + m_BorderWidth + m_ControlGap,m_Height,m_Width - (m_LabelWidth + m_BorderWidth + 2 * m_ControlGap),m_ControlHeight,m_ID++);

	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::AddFloatSpinner( const atString& r_Name,s32 idx, float min, float max, float step )
{
	s32 m_SpinnerSize = 20;

	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,m_LabelWidth + m_BorderWidth + m_ControlGap,m_Height,m_Width - (m_LabelWidth + m_BorderWidth + 2 * m_ControlGap + m_SpinnerSize),m_ControlHeight,m_ID + 1);
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,m_Width - m_SpinnerSize - m_BorderWidth,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 2);

	FloatSpinnerWdgt FloatSpinner;
	FloatSpinner.Idx = idx;
	FloatSpinner.EditID = m_ID + 1;
	FloatSpinner.SpinnerID = m_ID + 2;
	FloatSpinner.Min = min;
	FloatSpinner.Max = max;
	FloatSpinner.Step = step;

	m_FloatSpinners.PushAndGrow(FloatSpinner);

	m_ID += 3;
	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::AddIntSpinner( const atString& r_Name,s32 idx,s32 min,s32 max,s32 step )
{
	s32 m_SpinnerSize = 20;

	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,m_LabelWidth + m_BorderWidth + m_ControlGap,m_Height,m_Width - (m_LabelWidth + m_BorderWidth + 2 * m_ControlGap + m_SpinnerSize),m_ControlHeight,m_ID + 1);
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,m_Width - m_SpinnerSize - m_BorderWidth,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 2);

	IntSpinnerWdgt IntSpinner;

	IntSpinner.Idx = idx;
	IntSpinner.EditID = m_ID + 1;
	IntSpinner.SpinnerID = m_ID + 2;
	IntSpinner.Min = min;
	IntSpinner.Max = max;
	IntSpinner.Step = step;

	m_IntSpinners.PushAndGrow(IntSpinner);

	m_ID += 3;
	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::AddMapSelect( const atString& r_Name, s32 idx, const atString& r_MapName, atString& r_TemplateName )
{
	s32 Pos1 = m_BorderWidth + m_LabelWidth + m_ControlGap;
	s32 Pos2 = ((m_Width - ((m_BorderWidth << 1) + m_LabelWidth + m_ControlGap))) / 2;
	s32 Pos3 = Pos1 + Pos2 + (m_BorderWidth << 1) + m_ControlGap;
	s32 width2 = 15;
	TextureWdgt Texture;

	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);
	s32 texButtId = m_ID+1;

	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,r_MapName,Pos1,m_Height,Pos2,m_ControlHeight,texButtId);
	s32 templateLabelId = m_ID+2;
	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_TemplateName,Pos3,m_Height,m_LabelWidth,m_ControlHeight,templateLabelId);

	Texture.Idx = idx;
	m_ID += 3;

	Texture.ButtonID = texButtId;
	Texture.ButtonAlphaID = -1;

	m_Textures.PushAndGrow(Texture);

	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::AddMapSelect( const atString& r_Name, s32 idx, const atString& r_MapName, const atString& r_AlphaName, atString& r_TemplateName )
{
	s32 Pos1 = m_BorderWidth + m_LabelWidth + m_ControlGap;
	s32 Pos2 = ((m_Width - ((m_BorderWidth << 1) + m_LabelWidth + m_ControlGap))) / 2;
	s32 Pos3 = Pos1 + Pos2;
	s32 Pos4 = Pos2;

	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);
	s32 texButtId1 = m_ID+1;
	s32 texButtId2 = m_ID+2;

	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,r_MapName,Pos1,m_Height,Pos2,m_ControlHeight,texButtId1);
	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,r_AlphaName,Pos3,m_Height,Pos4,m_ControlHeight,texButtId2);

	TextureWdgt Texture;

	Texture.Idx = idx;
	m_ID += 3;

	Texture.ButtonID = texButtId1;
	Texture.ButtonAlphaID = texButtId2;

	m_Textures.PushAndGrow(Texture);

	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::AddVector2Select( const atString& r_Name, s32 idx, float min, float max, float step )
{
	s32 m_SpinnerSize = 10;

	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);

	s32 Width = (m_Width - (m_LabelWidth + m_BorderWidth + m_ControlGap)) / 5;
	s32 ControlWidth = Width - (m_SpinnerSize + m_ControlGap);
	s32 Xpos = m_LabelWidth + m_BorderWidth + m_ControlGap;

	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 1);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 2);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 3);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 4);

	Vector2Wdgt Vec2;

	Vec2.Idx = idx;
	Vec2.Float[0].EditID = m_ID + 1;
	Vec2.Float[0].SpinnerID = m_ID + 2;
	Vec2.Float[0].Min = min;
	Vec2.Float[0].Max = max;
	Vec2.Float[0].Step = step;
	Vec2.Float[1].EditID = m_ID + 3;
	Vec2.Float[1].SpinnerID = m_ID + 4;
	Vec2.Float[1].Min = min;
	Vec2.Float[1].Max = max;
	Vec2.Float[1].Step = step;

	m_Vector2s.PushAndGrow(Vec2);

	m_ID += 5;
	m_Height += m_ControlHeight + m_ControlGap;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::AddVector3Select(const atString& r_Name, s32 idx, float min, float max, float step)
{
	s32 m_SpinnerSize = 10;

	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);

	s32 Width = (m_Width - (m_LabelWidth + m_BorderWidth + m_ControlGap)) / 5;
	s32 ControlWidth = Width - (m_SpinnerSize + m_ControlGap);
	s32 Xpos = m_LabelWidth + m_BorderWidth + m_ControlGap;

	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 1);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 2);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 3);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 4);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 5);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 6);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,atString("..."),Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 7);

	Vector3Wdgt Vec3;
	Vec3.Idx = idx;

	Vec3.Float[0].EditID = m_ID + 1;
	Vec3.Float[0].SpinnerID = m_ID + 2;
	Vec3.Float[0].Min = min;
	Vec3.Float[0].Max = max;
	Vec3.Float[0].Step = step;
	Vec3.Float[1].EditID = m_ID + 3;
	Vec3.Float[1].SpinnerID = m_ID + 4;
	Vec3.Float[1].Min = min;
	Vec3.Float[1].Max = max;
	Vec3.Float[1].Step = step;
	Vec3.Float[2].EditID = m_ID + 5;
	Vec3.Float[2].SpinnerID = m_ID + 6;
	Vec3.Float[2].Min = min;
	Vec3.Float[2].Max = max;
	Vec3.Float[2].Step = step;
	Vec3.ButtonID = m_ID + 7;

	m_Vector3s.PushAndGrow(Vec3);

	m_ID += 8;
	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::AddVector4Select( const atString& r_Name, s32 idx, float min, float max, float step )
{
	s32 m_SpinnerSize = 10;

	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);

	s32 Width = (m_Width - (m_LabelWidth + m_BorderWidth + m_ControlGap)) / 5;
	s32 ControlWidth = Width - (m_SpinnerSize + m_ControlGap);
	s32 Xpos = m_LabelWidth + m_BorderWidth + m_ControlGap;

	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 1);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 2);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 3);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 4);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 5);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 6);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 7);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 8);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,atString("..."),Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 9);

	Vector4Wdgt Vec4;
	Vec4.Idx = idx;

	Vec4.Float[0].EditID = m_ID + 1;
	Vec4.Float[0].SpinnerID = m_ID + 2;
	Vec4.Float[0].Min = min;
	Vec4.Float[0].Max = max;
	Vec4.Float[0].Step = step;
	Vec4.Float[1].EditID = m_ID + 3;
	Vec4.Float[1].SpinnerID = m_ID + 4;
	Vec4.Float[1].Min = min;
	Vec4.Float[1].Max = max;
	Vec4.Float[1].Step = step;
	Vec4.Float[2].EditID = m_ID + 5;
	Vec4.Float[2].SpinnerID = m_ID + 6;
	Vec4.Float[2].Min = min;
	Vec4.Float[2].Max = max;
	Vec4.Float[2].Step = step;
	Vec4.Float[3].EditID = m_ID + 7;
	Vec4.Float[3].SpinnerID = m_ID + 8;
	Vec4.Float[3].Min = min;
	Vec4.Float[3].Max = max;
	Vec4.Float[3].Step = step;
	Vec4.ButtonID = m_ID + 9;

	m_Vector4s.PushAndGrow(Vec4);

	m_ID += 10;
	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void ShaderControls::AddBoolSelect( const atString& r_Name, s32 idx )
{
	s32 m_ButtonSize = 20;

	m_DialogRes.AddControl(DialogResBuilder::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);

	s32 Width = (m_Width - (m_LabelWidth + m_BorderWidth + m_ControlGap)) / 5;
	s32 ControlWidth = Width - (m_ButtonSize + m_ControlGap);
	s32 Xpos = m_LabelWidth + m_BorderWidth + m_ControlGap;

	m_DialogRes.AddControl(atString("BUTTON"),WS_VISIBLE|WS_CHILD|BS_CHECKBOX,atString(""),Xpos,m_Height,m_ButtonSize,m_ControlHeight,m_ID + 1);

	BoolWdgt BoolButton;
	BoolButton.Idx = idx;
	BoolButton.ButtonID = m_ID + 1;

	m_Bools.PushAndGrow(BoolButton);

	m_ID += 2;
	m_Height += m_ControlHeight + m_ControlGap;
}
