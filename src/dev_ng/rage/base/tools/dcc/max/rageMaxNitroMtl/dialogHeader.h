#ifndef __RSG_MAX_DIALOGHDR_H__
#define __RSG_MAX_DIALOGHDR_H__

#include "rageMaxNitroMtl.h"

class RsgMaxDlgHdr : public ParamDlg
{
public:
	RsgMaxDlgHdr(RageMaxNitroMtl* p_Mtl,IMtlParams* p_MtlParams);
	virtual ~RsgMaxDlgHdr();

#pragma region ParamDlg
	// from ParamDlg
	virtual Class_ID ClassID() { return RAGE_MAX_NITRO_MTL_CLASS_ID; }
	virtual void SetThing(ReferenceTarget *m);
	virtual ReferenceTarget* GetThing() { return m_pNitroMtl; }
	virtual void SetTime(TimeValue t) {}
	virtual	void ReloadDialog() {}
	virtual void DeleteThis() { delete this; }
	virtual void ActivateDlg(BOOL onOff) {}
	virtual int FindSubTexFromHWND(HWND hwnd);

	static INT_PTR CALLBACK RsgMaxParamDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);
	static INT_PTR CALLBACK RsgMaxAboutDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);
#pragma endregion

	RsgMaxDlgShader* m_pShaderDlg;
protected:
	void Reset();
	void SetupUI();
	void SetupShader( const char* preset );
	void SetShader( );
	void SetPresetName( const char *preset );

	void ConstructShaderParamDialog( rage::grcEffect* p_Shader );

	//void SetupShader(const atString& r_ShaderName,bool IsPreset);

	void OnInitDialog(HWND DlgWnd);
	/*
	void OnShaderNameSelChange(HWND ListWnd);
	void OnPresetNameSelChange(HWND ListWnd);
	void OnRadioPreset();
	void OnRadioCustom();
	void OnButtonAbout();
	void OnButtonTwoSided();
	void OnButtonTexScale();
	void OnButtonLighting();
	void OnButtonAmbLighting();
	void OnButtonDiffuseAlpha();
	void OnTexScaleSelChange(HWND ListWnd);

	void SetPresetName(const char *preset);
	*/
	HWND m_Wnd;
	IMtlParams* m_pMtlParam;
	
	RageMaxNitroMtl* m_pNitroMtl;
};

#endif //__RSG_MAX_DIALOGHDR_H__
