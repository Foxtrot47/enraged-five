#include "dbeditor.h"
#include "shadermanager.h"
#include "resource.h"
#include "rst/shaderDb.h"
//#include "rstMaxMtl/inputname.h"
//#include "rstMaxMtl/dbeditpreset.h"
//#include "rstMaxMtl/dbaddpreset.h"
//#include "rstMaxMtl/rstMax.h"
//#include "rstplugin.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

////////////////////////////////////////////////////////////////////////////////
DbEditor& DbEditor::GetInst()
{
	static DbEditor Chooser;

	return Chooser;
}

////////////////////////////////////////////////////////////////////////////////
DbEditor::DbEditor():
m_NumDBEntries(0),
m_CurrentSel(TVI_ROOT),
m_Wnd(NULL),
m_IsChoosing(false)
{
	//rageRstMaxShaderPlugin* p_Plugin = rageRstMaxShaderPlugin::GetInstance();
	m_shaderDb = ShaderManager::GetInstance()->GetShaderDb();
}

////////////////////////////////////////////////////////////////////////////////
bool DbEditor::Show()
{
	if(m_Wnd)
		return true;

	m_Wnd = CreateDialogParam(	g_hDllInstance,
		MAKEINTRESOURCE(IDD_DLG_DBCHOOSER),
		GetCOREInterface()->GetMAXHWnd(),
		DbChooserDlgProc,
		(LPARAM)this);

	ShowWindow(m_Wnd,SW_SHOW);

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool DbEditor::ChoosePreset(HWND m_ParentWnd,atString& r_PresetPath)
{
	if(m_Wnd)
		return true;

	m_PresetPath = r_PresetPath;
	m_IsChoosing = true;

	DialogBoxParam(	g_hDllInstance,
		MAKEINTRESOURCE(IDD_DLG_DBCHOOSER),
		m_ParentWnd,
		DbChooserDlgProc,
		(LPARAM)this);

	r_PresetPath = m_PresetPath;
	m_IsChoosing = false;

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void DbEditor::SetRootPath(const atString& r_RootPath)
{
}

////////////////////////////////////////////////////////////////////////////////
HTREEITEM DbEditor::AddTreeItem(HTREEITEM ParentItem,const char* r_Name,bool IsGroup)
{
	TVINSERTSTRUCT InsertItem;
	InsertItem.hParent = ParentItem;
	InsertItem.hInsertAfter = TVI_LAST;
	InsertItem.itemex.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE;
	InsertItem.itemex.pszText = (char*)r_Name;
	InsertItem.itemex.iImage = IsGroup ? IMAGE_GROUP : IMAGE_ITEM;
	InsertItem.itemex.iSelectedImage = IsGroup ? IMAGE_GROUP_SEL : IMAGE_ITEM_SEL;

	return (HTREEITEM)SendMessage(m_TreeWnd,TVM_INSERTITEM,0,(LPARAM)&InsertItem);
}

////////////////////////////////////////////////////////////////////////////////
bool DbEditor::AddGroup(const char* r_FullPath)
{
	if(m_Wnd == NULL)
	{
		return m_shaderDb->CreateGroup(r_FullPath);
	}

	char p_FullPath[MAX_PATH];
	char p_GroupName[MAX_PATH];
	char p_ParentPath[MAX_PATH];
	char* p_LastSlash;

	strcpy(p_FullPath,r_FullPath);

	p_LastSlash = p_FullPath;

	while(*p_LastSlash)
	{
		if(*p_LastSlash == '\\')
		{
			*p_LastSlash = '/';
		}

		p_LastSlash++;
	}

	p_LastSlash = strrchr(p_FullPath,'/');

	if(p_LastSlash)
	{
		strcpy(p_GroupName,p_LastSlash + 1);
	}
	else
	{
		strcpy(p_GroupName,p_FullPath);
	}

	if(p_LastSlash)
	{
		*p_LastSlash = '\0';
		strcpy(p_ParentPath,p_FullPath);
	}
	else
	{
		p_ParentPath[0] = '\0';
	}

	HTREEITEM ParentItem = GetItem(p_ParentPath);

	if(!ParentItem)
	{
		return false;
	}

	if(!m_shaderDb->CreateGroup(r_FullPath))
	{
		return false;
	}

	AddTreeItem(ParentItem,p_GroupName,true);

	return true;
}

////////////////////////////////////////////////////////////////////////////////
HTREEITEM DbEditor::GetItem(const char* r_PathIn)
{
	char p_Path[MAX_PATH];
	char p_ItemBuffer[NAME_LENGTH];

	strcpy(p_Path,r_PathIn);

	char* p_Token = strtok(p_Path,"/");
	HTREEITEM ParentItem = TVI_ROOT;
	HTREEITEM CurrentItem = TVI_ROOT;
	CurrentItem = (HTREEITEM)SendMessage(m_TreeWnd,TVM_GETNEXTITEM,TVGN_CHILD,(LPARAM)CurrentItem);

	while(p_Token)
	{
		bool Found = false;

		while(CurrentItem)
		{
			TVITEMEX ItemInfo;
			ItemInfo.mask = TVIF_TEXT;
			ItemInfo.pszText = p_ItemBuffer;
			ItemInfo.cchTextMax = NAME_LENGTH;
			ItemInfo.hItem = CurrentItem;

			SendMessage(m_TreeWnd,TVM_GETITEM,0,(LPARAM)&ItemInfo);

			if(!stricmp(p_ItemBuffer,p_Token))
			{
				Found = true;
				ParentItem = CurrentItem;
				CurrentItem = (HTREEITEM)SendMessage(m_TreeWnd,TVM_GETNEXTITEM,TVGN_CHILD,(LPARAM)CurrentItem);
				break;
			}

			CurrentItem = (HTREEITEM)SendMessage(m_TreeWnd,TVM_GETNEXTITEM,TVGN_NEXT,(LPARAM)CurrentItem);
		}

		if(!Found)
		{
			return NULL;
		}

		p_Token = strtok(NULL,"/");
	}

	return ParentItem;
}

////////////////////////////////////////////////////////////////////////////////
HTREEITEM DbEditor::GetCurrentGroup()
{
	TVITEMEX ItemInfo;
	HTREEITEM ParentItem = TVI_ROOT;

	if(m_CurrentSel != ParentItem)
	{
		ItemInfo.hItem = m_CurrentSel;
		ItemInfo.mask = TVIF_IMAGE;

		SendMessage(m_TreeWnd,TVM_GETITEM,0,(LPARAM)&ItemInfo);

		if(IsGroup(m_CurrentSel))
		{
			//if its a group then we can use this as the parent

			ParentItem = m_CurrentSel;
		}
		else
		{
			//else its an actual shader so go up one in the tree to get the parent
			//(which might be the root of the structure)

			ParentItem = (HTREEITEM)SendMessage(m_TreeWnd,TVM_GETNEXTITEM,TVGN_PARENT,(LPARAM)m_CurrentSel);

			if(!ParentItem)
				ParentItem = TVI_ROOT;
		}
	}

	return ParentItem;
}

////////////////////////////////////////////////////////////////////////////////
void DbEditor::GetPath(HTREEITEM Item,atString& r_PathOut)
{
	r_PathOut = atString("");

	if(Item == TVI_ROOT)
		return;

	atString Buffer;
	char ItemBuffer[NAME_LENGTH];

	TVITEMEX ItemInfo;
	ItemInfo.mask = TVIF_IMAGE|TVIF_TEXT;
	ItemInfo.pszText = ItemBuffer;
	ItemInfo.cchTextMax = NAME_LENGTH;

	while(Item != TVI_ROOT)
	{
		ItemInfo.hItem = Item;

		SendMessage(m_TreeWnd,TVM_GETITEM,0,(LPARAM)&ItemInfo);

		Buffer = ItemInfo.pszText;

		// Retire gta_ shaders...
		Buffer.Replace("gta_", "");

		if(r_PathOut != atString(""))
			Buffer += atString("/");

		Buffer += r_PathOut;

		r_PathOut = Buffer;

		Item = (HTREEITEM)SendMessage(m_TreeWnd,TVM_GETNEXTITEM,TVGN_PARENT,(LPARAM)Item);

		if(!Item)
			Item = TVI_ROOT;
	}
}

////////////////////////////////////////////////////////////////////////////////
void DbEditor::BuildEntries(HTREEITEM Parent,const atString& Level)
{
	s32 i,Count = m_shaderDb->BuildEntryList(Level);

	for(i=0;i<Count;i++)
	{
		const char* p_Name = m_shaderDb->GetEntryName(i);
		atString nameString(p_Name);

		if((strcmp(p_Name,".") == 0) || (strcmp(p_Name,"..") == 0) || (nameString.IndexOf("gta_") != -1))
		{
			continue;
		}

		bool IsGroup = m_shaderDb->IsGroup(i);

		HTREEITEM NewItem = AddTreeItem(Parent,p_Name,IsGroup);

		if((NewItem != NULL) && IsGroup)
		{
			atString NewLevel = Level;
			NewLevel += "/";
			NewLevel += p_Name;

			BuildEntries(NewItem,NewLevel);
			m_shaderDb->BuildEntryList(Level);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
bool DbEditor::IsGroup(HTREEITEM Item)
{
	TVITEMEX ItemInfo;

	ItemInfo.hItem = Item;
	ItemInfo.mask = TVIF_IMAGE;

	SendMessage(m_TreeWnd,TVM_GETITEM,0,(LPARAM)&ItemInfo);

	if(ItemInfo.iImage == IMAGE_GROUP)
	{
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////
bool DbEditor::SelectItem(const atString& r_PathIn)
{
	HTREEITEM Item = GetItem(r_PathIn);

	if(Item == TVI_ROOT)
	{
	}
	else
	{
		BOOL Ret = SendMessage(m_TreeWnd,TVM_SELECTITEM,(WPARAM)(TVGN_CARET),(LPARAM)Item);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void DbEditor::OnInitDialog(HWND Wnd)
{
	m_Wnd = Wnd;

	//set up initial info for the tree

	//***PROBABLY NEED TO DELETE THIS ON EXIT***

	HIMAGELIST ImageList = ImageList_LoadBitmap(g_hDllInstance,
		MAKEINTRESOURCE(IDB_BMP_ILCHOOSER),
		16,
		1,
		CLR_NONE);


	m_TreeWnd = GetDlgItem(m_Wnd,IDC_TREE_DATA);
	SendMessage(m_TreeWnd,TVM_SETIMAGELIST,TVSIL_NORMAL,(LPARAM)ImageList);

#if !ENABLE_PRESET_EDITING
	RemoveMenu(GetMenu(m_Wnd), 1, MF_BYPOSITION);
#endif

	//get data from the shader plugin about the shader database 
	//and fill in the tree items

	BuildEntries(TVI_ROOT,atString(""));

	//select initial tree item

	SelectItem(m_PresetPath);

	if (m_IsChoosing)
	{
		SetWindowText(Wnd,"Choose preset (dbl-click)");
		LONG style = GetWindowLong(Wnd,GWL_STYLE);
		style &= ~WS_POPUP;
		SetWindowLong(Wnd,GWL_STYLE,style);

		MENUITEMINFO info;
		info.cbSize = sizeof(info);
		info.fMask = MIIM_STATE;
		info.fState = MFS_DISABLED;
		SetMenuItemInfo(GetMenu(Wnd),IDOK,FALSE,&info);
	}
}

////////////////////////////////////////////////////////////////////////////////
void DbEditor::OnCommandOK()
{
	GetPath(m_CurrentSel,m_PresetPath);

	EndDialog(m_Wnd,1);
	m_Wnd = NULL;
}

void DbEditor::OnCommandClose()
{
	EndDialog(m_Wnd,1);
	m_Wnd = NULL;
}

////////////////////////////////////////////////////////////////////////////////
#if ENABLE_PRESET_EDITING
void DbEditor::OnEntryEditPreset()
{
	atString Path;

	if(IsGroup(m_CurrentSel))
		return;

	GetPath(m_CurrentSel,Path);

	DbEditPreset EditPreset;

	rageRstShaderDbEntry* p_Entry = mp_DB->GetItem(Path);

	if(!p_Entry)
	{
		return;
	}

	mp_DB->LockEntry(Path);

	bool Ret = EditPreset.DoModal(m_Wnd,p_Entry);

	mp_DB->UnlockEntry(Path,Ret);
}

////////////////////////////////////////////////////////////////////////////////
void DbEditor::OnEntryAddPreset()
{
	atString FullPath;
	atString NewPresetName;
	HTREEITEM Group = DbEditor::GetCurrentGroup();
	GetPath(Group,FullPath);

	DbAddPreset AddPreset;

	if(!AddPreset.DoModal(m_Wnd,FullPath,NewPresetName))
	{
		return;
	}

	AddTreeItem(Group,NewPresetName,false);
}

////////////////////////////////////////////////////////////////////////////////
void DbEditor::OnEntryAddGroup()
{
	//get the name, and check that its unique...

	atString GroupName;
	InputName InputNameDlg;
	bool Ret = InputNameDlg.DoModal(m_Wnd,atString("enter the new group's name"),GroupName);

	if(!Ret)
		return;

	HTREEITEM ParentItem = GetCurrentGroup();

	//create the item in the tree

	AddTreeItem(ParentItem,GroupName,true);

	//create the database item

	atString FullPath;

	GetPath(ParentItem,FullPath);
	FullPath += "/";
	FullPath += GroupName;

	mp_DB->CreateGroup(FullPath);
}

////////////////////////////////////////////////////////////////////////////////
void DbEditor::OnEntryRemove()
{
	char Buffer[NAME_LENGTH];
	TVITEMEX ItemInfo;
	ItemInfo.hItem = m_CurrentSel;
	ItemInfo.mask = TVIF_IMAGE|TVIF_TEXT;
	ItemInfo.pszText = Buffer;
	ItemInfo.cchTextMax = NAME_LENGTH;

	SendMessage(m_TreeWnd,TVM_GETITEM,0,(LPARAM)&ItemInfo);

	atString FullPath;
	GetPath(m_CurrentSel,FullPath);

	if(ItemInfo.iImage == IMAGE_GROUP)
	{
		mp_DB->DeleteGroup(FullPath);

		SendMessage(m_TreeWnd,TVM_DELETEITEM,0,(LPARAM)m_CurrentSel);
	}
	else
	{
		mp_DB->DeleteEntry(FullPath);

		SendMessage(m_TreeWnd,TVM_DELETEITEM,0,(LPARAM)m_CurrentSel);
	}
}
#endif //ENABLE_PRESET_EDITING
////////////////////////////////////////////////////////////////////////////////
void DbEditor::OnHelpAbout()
{
}

////////////////////////////////////////////////////////////////////////////////
bool DbEditor::OnNotifyTree(const NMTREEVIEW* p_NmTreeView)
{
	switch (p_NmTreeView->hdr.code)
	{
	case TVN_SELCHANGED:
		{
			m_CurrentSel = p_NmTreeView->itemNew.hItem;

			// Gray out "Edit preset" item if group is selected

#if ENABLE_PRESET_EDITING
			HMENU menu = GetMenu(m_Wnd);
			MENUITEMINFO itemInfo;
			itemInfo.cbSize = sizeof(itemInfo);
			itemInfo.fMask = MIIM_STATE;
			if (IsGroup(m_CurrentSel))
			{
				itemInfo.fState = MFS_DISABLED;
			}
			else
			{
				itemInfo.fState = MFS_ENABLED;
			}
			SetMenuItemInfo(menu,
				ID_ENTRY_EDITPRESET,
				FALSE,
				&itemInfo);
#endif
		}
		break;

	case NM_DBLCLK:
		if (m_IsChoosing && !IsGroup(m_CurrentSel))
		{
			// Double-clicked on preset
			return true;
		}
		break;

#if ENABLE_PRESET_EDITING
	case NM_RCLICK:

		HWND treeWnd = p_NmTreeView->hdr.hwndFrom;
		TVHITTESTINFO hitInfo;
		POINT mouseScr;
		GetCursorPos(&mouseScr);
		hitInfo.pt = mouseScr;
		ScreenToClient(treeWnd,&hitInfo.pt);
		SendMessage(treeWnd,TVM_HITTEST,0,(LPARAM)&hitInfo);
		if (hitInfo.flags & TVHT_ONITEM)
		{
			// Select item that was right-clicked
			SendMessage(treeWnd,
				TVM_SELECTITEM,
				TVGN_CARET,
				(LPARAM)hitInfo.hItem);

			// Show popup menu
			HMENU popMenu = GetSubMenu(GetMenu(m_Wnd),1);
			TrackPopupMenu(popMenu,
				TPM_LEFTALIGN | TPM_TOPALIGN,
				mouseScr.x,mouseScr.y,
				0,
				m_Wnd,
				NULL);
		}
		break;
#endif 
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK DbEditor::DbChooserDlgProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam) 
{
	DbEditor* p_DbEditor = (DbEditor*)GetWindowLongPtr(DlgWnd,DWLP_USER);

	switch(Msg)
	{
	case WM_INITDIALOG:

		SetWindowLongPtr(DlgWnd,DWLP_USER,LParam);
		p_DbEditor = (DbEditor*)LParam;
		p_DbEditor->OnInitDialog(DlgWnd);
		return TRUE;

	case WM_NOTIFY:
		{
			NMHDR* p_NmHdr = (NMHDR*)LParam;

			switch(p_NmHdr->idFrom)
			{
			case IDC_TREE_DATA:
				if (p_DbEditor->OnNotifyTree((NMTREEVIEW*)LParam))
				{
					p_DbEditor->OnCommandOK();
				}
				break;
			}
		}
		return TRUE;

	case WM_SIZE:
		RECT rect;
		GetClientRect(DlgWnd,&rect);
		SetWindowPos(GetDlgItem(DlgWnd,IDC_TREE_DATA),
			NULL,
			rect.left,rect.top,
			rect.right,rect.bottom,
			SWP_NOZORDER | SWP_SHOWWINDOW);
		return 0;

	case WM_COMMAND:

		switch(LOWORD(WParam))
		{
		case IDOK:
			p_DbEditor->OnCommandOK();
			break;
#if ENABLE_PRESET_EDITING
		case ID_ENTRY_EDITPRESET:
			p_DbEditor->OnEntryEditPreset();
			break;
		case ID_ENTRY_ADD:
			p_DbEditor->OnEntryAddPreset();
			break;
		case ID_ENTRY_ADDGROUP:
			p_DbEditor->OnEntryAddGroup();
			break;
		case ID_ENTRY_REMOVE:
			p_DbEditor->OnEntryRemove();
			break;
#endif
		//case ID_HELP_ABOUT:
		//	p_DbEditor->OnHelpAbout();
		//	break;
		}

		return TRUE;

	case WM_CLOSE:
#if HACK_GTA4
		// Only dbl-click behavior in the 
		// tree view should update preset
		p_DbEditor->OnCommandClose();
#else
		p_DbEditor->OnCommandOK();
#endif // HACK_GTA4
		return TRUE;
	}

	return FALSE;
}
