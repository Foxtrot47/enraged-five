#include "shaderDb.h"
#include "shaderDb_parser.h"

#include "file/asset.h"
#include "file/device.h"
#include "grmodel/shader.h"

#define MATLIB		(*grcMaterialLibrary::GetCurrent())

using namespace rage;

ShaderDb::ShaderDb()
{
	// Init with a fairly large size
	//m_CurrentItems.Reserve(1024);
	//m_LockedEntry = 0;

	INIT_PARSER;

	REGISTER_PARSABLE_CLASS(MSLShaderPreset);
	REGISTER_PARSABLE_CLASS(MSLRageShaderPreset);
	REGISTER_PARSABLE_CLASS(MSLShaderPresetMapping);

	m_LoadedShaderPresets = false;
	m_MSLPresetMapping = rage_new MSLShaderPresetMapping();
}

ShaderDb::~ShaderDb()
{
	SHUTDOWN_PARSER;
}


int ShaderDb::BuildEntryList( const char* level ) {

	// Load the MSL shader mapping file if it's not already loaded.
	LoadMSLShaderMapping();

	// Clear existing list
	if ( m_CurrentItems.GetCount() )
		m_CurrentItems.Resize(0);

	char buff[256];
	formatf( buff, sizeof(buff), "%s/%s", MATLIB.GetRootPath() ? MATLIB.GetRootPath() : ".", level );

	fiFindData data;

	const fiDevice *d = fiDevice::GetDevice(buff,true);
	if ( d ) {
		fiHandle handle = d->FindFileBegin(buff,data);
		if (fiIsValidHandle(handle)) {
			do {
				const char *ext = strrchr(data.m_Name, '.');
				if ( (data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY || (ext && stricmp(ext, ".sps") == 0)) {
					ShaderDbItem &item = m_CurrentItems.Grow();
					formatf(item.m_Name, sizeof(item.m_Name), "%s", data.m_Name);
					item.m_IsGroup = (data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY;
				}
			} while (d->FindFileNext(handle,data));
			d->FindFileEnd(handle);
		}
		else {
			Warningf("ShaderDb -- Could not get directory for path %s", buff);
		}
	}
	else {
		Errorf("ShaderDb -- Device failure on item %s", buff);
	}

	return m_CurrentItems.GetCount();
}

void ShaderDb::LoadMSLShaderMapping()
{
	if (!m_LoadedShaderPresets)
	{
		ASSET.PushFolder( grmShaderFactory::GetShaderLibPath() );
		ASSET.PushFolder( "metaSL" );

		if (LoadMSLShaderMappingFile("MetaSLPresets.xml"))
		{
			// Find the default MSL shader preset that all rage presets will fall back on if they don't have one set.
			if (m_MSLPresetMapping->m_DefaultMSLPreset)
			{
				for (int i = 0; i < m_MSLPresetMapping->m_MSLShaderPresets.GetCount(); i++)
				{
					if (stricmp(m_MSLPresetMapping->m_MSLShaderPresets[i].m_MSLPresetName, m_MSLPresetMapping->m_DefaultMSLPreset) == 0)
					{
						m_defaultPreset = &m_MSLPresetMapping->m_MSLShaderPresets[i];
					}
				}
			}

			// Build up a map of shader presets to their corresponding MSL preset.
			PopulateShaderPresets();
		}

		ASSET.PopFolder();
		ASSET.PopFolder();

		m_LoadedShaderPresets = true;
	}
}

bool ShaderDb::SetShaderDbPath(const char *path) 
{
	grcMaterialLibrary* pMtlLib = grcMaterialLibrary::Preload(path);
	if(pMtlLib)
	{
		grcMaterialLibrary::SetCurrent(pMtlLib);
		return true;
	}
	else
	{
		return false;
	}
}

bool ShaderDb::CreateGroup( const char *folder ) {
	char buff[256];
	formatf( buff, sizeof(buff), "%s/%s", MATLIB.GetRootPath() ? MATLIB.GetRootPath() : ".", folder );
	const fiDevice *d = fiDevice::GetDevice(buff,false);
	bool retVal = false;
	if ( d ) {
		retVal = d->MakeDirectory(buff);
	}
	if ( retVal == false ) {
		Errorf("Could not create group -- %s", folder);
	}
	return retVal;
}

ShaderDbEntry *ShaderDb::GetItem( const char *entryName ) {
	char buff[2048];
	StringNormalize(buff, entryName, sizeof(buff));
	ShaderDbEntry **entryPtr = m_Presets.Access( buff );
	ShaderDbEntry *entry = 0;
	if ( entryPtr == 0 ) {
		grcInstanceData *preset = MATLIB.Lookup(entryName);
		if ( preset ) {
			entry = CreateEntryForPreset( preset );
		}
	}
	else {
		entry = *entryPtr;
	}
	return entry;	
}

ShaderDbEntry *ShaderDb::CreateEntryForPreset( grcInstanceData *preset ) {
	ShaderDbEntry *entry = new ShaderDbEntry;
	entry->m_Preset = preset;
	grcEffect *shader = &entry->m_Preset->GetBasis();
	if ( shader ) {
		entry->m_TotalVariableCount = shader->GetInstancedVariableCount();
		int dbOnlyCount = 0;
		for (int i=0; i < entry->m_TotalVariableCount; ++i) {
			grmVariableInfo info;
			shader->GetInstancedVariableInfo(i, info);
			if ( info.m_IsMaterial )
				dbOnlyCount++;
		}
		entry->m_DbOnlyCount = dbOnlyCount;
	}

	char buff[2048];
	StringNormalize(buff, preset->GetMaterialName(), sizeof(buff));
	m_Presets.Insert( ConstString(buff), entry );

	return entry;
}

MSLShaderPreset* ShaderDb::GetMSLItem( const char* entryName )
{
	// Load the MSL shader mapping file if we haven't already.
	LoadMSLShaderMapping();

	MSLShaderPreset** preset = m_MSLPresets.Access(entryName);

	if (preset && *preset)
		return *preset;
	else
		return m_defaultPreset;
}

void ShaderDb::PopulateShaderPresets()
{
	for (int i = 0; i < m_MSLPresetMapping->m_RageShaderPresets.GetCount(); i++)
	{
		ConstString MSLPreset = m_MSLPresetMapping->m_RageShaderPresets[i].m_MSLShaderPreset;
		ConstString RageShaderPreset = m_MSLPresetMapping->m_RageShaderPresets[i].m_RageShaderPreset;

		bool foundMSLPreset = false;

		// Find the matching preset based off the preset name.
		for (int j = 0; j < m_MSLPresetMapping->m_MSLShaderPresets.GetCount(); j++)
		{
			ConstString MSLPresetName = m_MSLPresetMapping->m_MSLShaderPresets[j].m_MSLPresetName;

			if (stricmp(MSLPresetName, MSLPreset) == 0)
			{
				foundMSLPreset = true;
				m_MSLPresets.Insert(RageShaderPreset, &m_MSLPresetMapping->m_MSLShaderPresets[j]);
			}
		}

		// If we didn't find a matching MSL preset, just use the default.
		if (!foundMSLPreset)
		{
			m_MSLPresets.Insert(RageShaderPreset, m_defaultPreset);
		}
	}
}

bool ShaderDb::LoadMSLShaderMappingFile(const char* filePath)
{
	bool bResult = true;
	fiStream * streamCtrl = ASSET.Open(filePath, "");
	if (streamCtrl)
	{
		if(!PARSER.LoadObject(streamCtrl, *m_MSLPresetMapping))
			bResult = false;

		streamCtrl->Close();
	}
	else
		bResult = false;

	return bResult;
}

bool ShaderDb::SaveMSLShaderMappingFile(const char* filePath)
{
	bool bResult = false;
	fiStream * streamCtrl = ASSET.Create(filePath, "");
	if (streamCtrl)
	{
		bResult = PARSER.SaveObject(streamCtrl, m_MSLPresetMapping);
		streamCtrl->Close();
	}
	return bResult;
}
