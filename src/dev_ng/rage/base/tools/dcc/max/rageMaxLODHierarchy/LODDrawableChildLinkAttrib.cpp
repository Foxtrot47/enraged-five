////
//// File::			LODDrawableChildLinkAttrib.cpp
//// Description::	RAGE Drawable Child LOD Custom Attribute Container
////
//// Author::			David Muir <david.muir@rockstarnorth.com>
//// Date::			12 March 2009
////
//
//// --- Include Files ------------------------------------------------------------
//
// Local headers
#include "LODDrawableChildLinkAttrib.h"
#include "Win32Res.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------
LodChildLinkAttribDataClassDesc theLodChildLinkAttribDataDesc;

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

ClassDesc* GetLodChildLinkAttribDataClassDesc( )
{
	return &theLodChildLinkAttribDataDesc;
}

LodDrawableChildLinkAttribData::LodDrawableChildLinkAttribData( ) 
: m_hWnd( NULL )
, m_invalid( false )
, refNode( NULL ) 
{
}

void 
LodDrawableChildLinkAttribData::SetInvalid( )
{
	m_invalid = true;
}

bool 
LodDrawableChildLinkAttribData::IsInvalid( ) 
{
	return m_invalid;
}

bool 
LodDrawableChildLinkAttribData::CheckCopyAttribTo(ICustAttribContainer *to)
{
	return true;
}

IOResult 
LodDrawableChildLinkAttribData::Load(ILoad *iload)
{
	return IO_OK;
}

IOResult 
LodDrawableChildLinkAttribData::Save(ISave *isave)
{
	return IO_OK;
}

RefResult 
LodDrawableChildLinkAttribData::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,PartID& partID,  RefMessage message)
{
	if((message == TARGETMSG_DELETING_NODE) && (hTarget == refNode))
	{
		refNode = NULL;
	}

	return REF_SUCCEED;
}

ReferenceTarget* 
LodDrawableChildLinkAttribData::Clone( RemapDir &remap )
{
	LodDrawableLinkAttribData* pCustAttrib = new LodDrawableLinkAttribData;
	return pCustAttrib;
}

int 
LodDrawableChildLinkAttribData::NumRefs()
{
	return (1);
}

RefTargetHandle 
LodDrawableChildLinkAttribData::GetReference(int i)
{
	return refNode;
}

void 
LodDrawableChildLinkAttribData::SetReference(int i, RefTargetHandle rtarg)
{
	refNode = (INode *)rtarg;
}

SvGraphNodeReference 
LodDrawableChildLinkAttribData::SvTraverseAnimGraph(IGraphObjectManager *gom, Animatable *owner, int id, DWORD flags)
{
	int i;

	SvGraphNodeReference nodeRef;
	SvGraphNodeReference childNodeRef;
	gom->PushLevel(this);

	nodeRef = gom->AddAnimatable(this, owner, id, flags);
	if (nodeRef.stat == SVT_PROCEED) 
	{
		for (i = 0; i < NumSubs(); i++) 
		{
			if (SubAnim(i)) 
			{
				childNodeRef = SubAnim(i)->SvTraverseAnimGraph(gom, this, i, flags);
				if (childNodeRef.stat != SVT_DO_NOT_PROCEED)
					gom->AddReference(nodeRef.gNode, childNodeRef.gNode, REFTYPE_SUBANIM);
			}
		}
	}

	gom->PopLevel();

	return nodeRef;
}