//
// File:: view.cpp
// Description:: 
//
// Author::
// Date::
//

// Max headers
#pragma warning ( push )
#pragma warning ( disable : 4100 4244 4238 4239 4245 4512 )
#include "Max.h"
#pragma warning ( pop )

//#include <list>

#ifdef MAX8
#include <xref/iXrefObj.h> 
#define XREFOBJ_CLASS_ID_PART 0x92aab38c
#else
#define XREFOBJ_CLASS_ID_PART XREFOBJ_CLASS_ID
#endif

#define MAXTEXTLEN 2048

// MaxUtil headers
#include "MaxUtil.h"
#include "IContainerObject.h"

// Local headers
#include "linkview.h"
#include "rageMaxHierarchy.h"
#include "linkattrib.h"
#include "resource.h"
#include "HierarchyInterface.h"

HWND g_hDialog = NULL;
eLinkType g_eType = eLinkType_LOD;
extern HINSTANCE g_hInstance;

INT_PTR CALLBACK DialogProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam);

void 
ShowDialog( eLinkType type )
{
	g_eType = type;
	if ( !g_hDialog )
	{
		g_hDialog = CreateDialog( g_hInstance,
									MAKEINTRESOURCE(IDD_DIALOG_LOD),
									GetCOREInterface()->GetMAXHWnd(),
									DialogProc );
	}

	IFpSceneHierarchy* ilnkinterface = GetSceneLinkInterface();
	TCHAR* winTitle = "LOD View";
	if(ilnkinterface)
		winTitle = ilnkinterface->GetChannelName(type);

	SetWindowText( g_hDialog, winTitle );
	ShowWindow(g_hDialog,SW_SHOW);

	RECT rectPos;
	GetClientRect(g_hDialog,&rectPos);

	LPARAM size = (rectPos.right - rectPos.left) + ((rectPos.bottom - rectPos.top) << 16);

	SendMessage(g_hDialog,WM_SIZE,SIZE_RESTORED,size);
}

HTREEITEM GetItemByName(   HWND hWnd, 
						HTREEITEM hItem, 
						LPCTSTR szItemName )
{
	try
	{
		// If hItem is NULL, start search from root item.
		if( hItem == NULL )
		{
			hItem = TreeView_GetRoot( hWnd );
		}//end if

		while( hItem != NULL )
		{
			TCHAR szBuffer[MAXTEXTLEN+1];
			TV_ITEM item;

			item.hItem = hItem;
			item.mask = TVIF_TEXT | TVIF_CHILDREN;
			item.pszText = szBuffer;
			item.cchTextMax = MAXTEXTLEN;

			TreeView_GetItem( hWnd, &item );

			// Did we find it?
			if( lstrcmp( item.pszText, szItemName ) == 0 )
			{
				return hItem;
			}//end if

// 			// Check whether we have child items.
// 			if( item.cChildren )
// 			{
// 				// Recursively traverse child items.
// 				HTREEITEM hItemFound, hItemChild;
// 
// 				hItemChild = TreeView_GetChild( hWnd, hItem );
// 
// 				hItemFound = GetItemByName( hWnd, hItemChild, szItemName );
// 
// 				// Did we find it?
// 				if( hItemFound != NULL )
// 				{
// 					return hItemFound;
// 				}//end if
// 			}//end if

			// Go to next sibling item.
			hItem = TreeView_GetNextSibling( hWnd, hItem );

		}//end while

		// Not found.
		return NULL;
	}//end try
	catch( ... )
	{
		// TODO: add some logging here, etc
		return NULL;
	}//end catch
}

void 
AddChildrenToTree( HWND hTree, HTREEITEM hTreeParent, INode* pRoot, eLinkType type )
{
	char cName[64];
	strcpy(cName,pRoot->GetName());

	if(NULL==GetItemByName(hTree, hTreeParent, cName))
	{
		Tab<INode*> llNodes;
		GetChildren( type, pRoot, llNodes );

		// drawable fucking hierarchies have fuckin wrong setup. Stupid fuck.
		if( type == eLinkType_LOD)
		{
			//bail out if we're in a link chain
			if(NULL == hTreeParent && 
				(NULL!=GetParent(type, pRoot) || llNodes.Count()<=0) )
				return;

			TVINSERTSTRUCT tvmInsertItem;

			tvmInsertItem.hParent = hTreeParent;
			tvmInsertItem.hInsertAfter = TVI_LAST;
			tvmInsertItem.item.mask = TVIF_TEXT;
			tvmInsertItem.item.pszText = cName;
			tvmInsertItem.item.cchTextMax = 64;

			HTREEITEM hCurrent = TreeView_InsertItem(hTree,&tvmInsertItem);

			int count = llNodes.Count();
			if ( count < 1 )
				return;

			for(int i=0; i<count; i++)
			{
				INodePtr begin = llNodes[i];

				AddChildrenToTree( hTree,hCurrent, begin, type );
			}
		}
		else if(type == eLinkType_DRAWLOD)
		{
			//bail out if we're in a link chain
			if(NULL == hTreeParent &&
				(llNodes.Count()!=0 || NULL==GetParent(type, pRoot)))
				return;

			TVINSERTSTRUCT tvmInsertItem;

			tvmInsertItem.hParent = hTreeParent;
			tvmInsertItem.hInsertAfter = TVI_LAST;
			tvmInsertItem.item.mask = TVIF_TEXT;
			tvmInsertItem.item.pszText = cName;
			tvmInsertItem.item.cchTextMax = 64;

			HTREEITEM hCurrent = TreeView_InsertItem(hTree,&tvmInsertItem);
			
			INodePtr begin = GetParent(type, pRoot);
			if(begin)
				AddChildrenToTree( hTree,hCurrent, begin, type );
		}
	}

}

void NodeScanRec(INode* pRoot, HWND hTree)
{
	for(int i=0; i<pRoot->NumberOfChildren(); i++)
	{
		INode *pNode = pRoot->GetChildNode(i);

		if(MaxUtil::IsContainer(pNode))
			NodeScanRec(pNode, hTree);

		if ( XREFOBJ_CLASS_ID_PART == pNode->GetObjectRef()->ClassID() )
			continue;

		Mesh* p_mesh = MaxUtil::GetMesh( pNode );
		if ( NULL == p_mesh )
			continue;

		AddChildrenToTree( hTree, NULL, pNode, g_eType );
	}
}

void RefreshTree()
{
	HWND hTree = GetDlgItem( g_hDialog, IDC_TREE_LOD );
	TreeView_DeleteAllItems(hTree);
	
	INode* pRoot = GetCOREInterface()->GetRootNode();
	NodeScanRec(pRoot, hTree);
}

bool SelectNodeInScene(INodePtr pNode, const char *name)
{
	Mesh* p_mesh = MaxUtil::GetMesh(pNode);
	if(	 pNode->GetObjectRef() &&
		( pNode->GetObjectRef()->ClassID() != XREFOBJ_CLASS_ID_PART) &&
		( p_mesh != NULL) &&
		(_stricmp(name,pNode->GetName())==0) )
	{
		GetCOREInterface()->SelectNode(pNode,0);
		return true;
	}

	for(int i=0; i<pNode->NumberOfChildren(); i++)
	{
		INode *pChild = pNode->GetChildNode(i);
		if(SelectNodeInScene(pChild, name))
			return true;
	}
	return false;
}

INT_PTR CALLBACK DialogProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_INITDIALOG:
		g_hDialog = hwndDlg;
		RefreshTree();
		return 0;

	case WM_NOTIFY:

		switch(wParam)
		{
		case IDC_TREE_LOD:

			{
				NMHDR* pHeader = (NMHDR*)lParam;

				if(pHeader->code == NM_DBLCLK)
				{
					HTREEITEM hTreeItem = TreeView_GetSelection(GetDlgItem(g_hDialog,IDC_TREE_LOD));

					TVITEM item;
					char cName[64];

					item.mask = TVIF_TEXT;
					item.hItem = hTreeItem;
					item.pszText = cName;
					item.cchTextMax = 64;

					TreeView_GetItem(GetDlgItem(g_hDialog,IDC_TREE_LOD),&item);

					GetCOREInterface()->ClearNodeSelection(TRUE);
					INode* pRoot = GetCOREInterface()->GetRootNode();

					SelectNodeInScene(pRoot, cName);
				}
			}

			return 0;	
		}

		return 0;

	case WM_SIZE:
		{
			int iWidth = LOWORD(lParam);
			int iHeight = HIWORD(lParam);

			SetWindowPos(GetDlgItem(g_hDialog,IDC_TREE_LOD),HWND_TOP,10,10,iWidth - 20,(iHeight - 60),SWP_SHOWWINDOW);

			SetWindowPos(GetDlgItem(g_hDialog,IDC_BTN_REFRESH),HWND_TOP,iWidth - 170,iHeight - 30,0,0,SWP_NOSIZE|SWP_SHOWWINDOW);
			SetWindowPos(GetDlgItem(g_hDialog,IDOK),HWND_TOP,iWidth - 90,iHeight - 30,0,0,SWP_NOSIZE|SWP_SHOWWINDOW);
		}

   	case WM_COMMAND:

		switch(wParam)
		{
		case IDC_BTN_REFRESH:
			g_hDialog = hwndDlg;
			RefreshTree();
			break;
		case IDOK:	
		case IDCANCEL:
			EndDialog(g_hDialog,1);
			g_hDialog = NULL;
			break;
		}

		return 0;

	case WM_DESTROY:
		g_hDialog = NULL;
		return 0;
	}

	return 0;
}

// view.cpp
