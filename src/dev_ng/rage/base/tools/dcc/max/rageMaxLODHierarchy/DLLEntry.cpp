//
// filename:	DLLEntry.cpp
// author:		David Muir
// date:		10 March 2009
// description:	3dsmax rageMaxHierarchy Plugin
//

// --- Include Files ------------------------------------------------------------

// 3D Studio Max SDK headers
#include "Max.h"

// Local headers
#include "rageMaxHierarchy.h"
#include "linkattrib.h"
#include "LODDrawableLinkAttrib.h"
#include "LODDrawableChildLinkAttrib.h"
#include "resource.h"
#include "Win32Res.h"

// --- Constants ----------------------------------------------------------------
const int	sC_cnt_NumberOfClasses = 3;

// --- Globals ------------------------------------------------------------------

HINSTANCE	g_hInstance = NULL;
int			g_ControlsInit = FALSE;

// --- Code ---------------------------------------------------------------------

//
// name:		DllMain
// description:	DLL Plugin Entry-Point
//
BOOL WINAPI 
DllMain( HINSTANCE hInstDLL, ULONG dwReason, LPVOID plReserved )
{
	// Store the DLLs instance handle
	g_hInstance = hInstDLL;

	if ( FALSE == g_ControlsInit )
	{
	#if MAX_VERSION_MAJOR < 11 
		InitCustomControls(g_hInstance);	// Initialize MAX's custom controls
	#endif //MAX_VERSION_MAJOR

		// Initialise platform common controls
		InitCommonControls( );
		g_ControlsInit = TRUE;
	}

	return TRUE;
}


RAGEMAXHIERARCHY_API int 
LibInitialize( void )
{
	return ( 1 );
}


RAGEMAXHIERARCHY_API int
LibShutdown( void )
{
	return ( 1 );
}


//
// name:		LibDescription
// description:	Return plugin description string (Max API)
//
RAGEMAXHIERARCHY_API const TCHAR* 
LibDescription()
{
	return GetStringResource( g_hInstance, IDS_DESCRIPTION );
}


//
// name:		LibNumberClasses
// description:	Return number of classes registered by plugin (Max API)
//
RAGEMAXHIERARCHY_API int 
LibNumberClasses( )
{
	return sC_cnt_NumberOfClasses;
}


//
// name:		LibClassDesc
// description:	Return indexed class description class
//
RAGEMAXHIERARCHY_API ClassDesc* 
LibClassDesc( int nClassIndex )
{
	assert( nClassIndex < sC_cnt_NumberOfClasses );
	switch ( nClassIndex )
	{
	case 0: 
		return ( GetLinkAttribDataClassDesc() );
 	case 1:
 		return ( GetLodDrawableLinkAttribDataClassDesc() );
 	case 2:
 		return ( GetLodChildLinkAttribDataClassDesc() );
	}
	return ( NULL );
}


//
// name:		LibVersion
// description:	Return version of Max SDK plugin was compiled with
//
RAGEMAXHIERARCHY_API ULONG 
LibVersion( )
{
	return ( VERSION_3DSMAX );
}


//
// name:		CanAutoDefer
// description:
//
RAGEMAXHIERARCHY_API ULONG 
CanAutoDefer( ) 
{ 
	return 0; 
}

// DLLEntry.cpp
