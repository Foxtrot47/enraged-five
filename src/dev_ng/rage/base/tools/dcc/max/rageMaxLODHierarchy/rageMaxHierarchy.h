//
// filename:	rageMaxHierarchy.h
// description:	 Hierarchy custom attribute container external interface.
//

#ifndef INC_rageMaxHierarchy_H_
#define INC_rageMaxHierarchy_H_

// --- Include Files ------------------------------------------------------------

// Local headers
#include "linkattrib.h"
#include "LODDrawableLinkAttrib.h"
#include "LODDrawableChildLinkAttrib.h"

// STL headers
#include <list>
#include <vector>

using namespace std;

typedef vector<LinkAttribData*> LinkAttribArray;
typedef LinkAttribArray::iterator LinkAttribIterator;
typedef vector<eLinkType> LinkTypeArray;
typedef LinkTypeArray::iterator LinkTypeIterator;

// --- Defines ------------------------------------------------------------------

#ifdef RAGEMAXLODHIERARCHY_EXPORTS
#define RAGEMAXHIERARCHY_API __declspec( dllexport )
#else
#define RAGEMAXHIERARCHY_API __declspec( dllimport )
#endif

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// Forward declare 3dsmax INode
class INode;


// --- Globals ------------------------------------------------------------------

/**
 * @brief Puts custom attribute container on node
 */
RAGEMAXHIERARCHY_API bool 
AddContainer( eLinkType type, INode* pNode );

/**
 * @brief removes custom attribute container
 */
RAGEMAXHIERARCHY_API bool 
RemoveContainer( eLinkType type, INode* pNode );

/**
 * @brief Removes a parent link
 */
RAGEMAXHIERARCHY_API bool 
RemoveParent( eLinkType type, INode* pNode, INode* pNodeParent );

/**
 * @brief Returns whether container exists
 */
RAGEMAXHIERARCHY_API bool 
HasContainer( eLinkType type, INode* pNode );

/**
 * @brief Return  Parent node (if exists)
 */
RAGEMAXHIERARCHY_API INode* 
GetParent( eLinkType type, INode* pNode );

/**
 * @brief Return  Parent node (if exists)
 */
RAGEMAXHIERARCHY_API bool 
SetParent( eLinkType type, INode* pNode, INode* pDrawableParentNode, bool append = false );

/**
 * @brief Return PARENT nodes (if exists)
 */
RAGEMAXHIERARCHY_API void 
GetParents( eLinkType type, INode* pNode, Tab<INode*>& llAdd);

/**
 * @brief Return  Child nodes (if exists)
 */
RAGEMAXHIERARCHY_API void 
GetChildren( eLinkType type, INode* pNode, Tab<INode*>& llAdd, LinkAttribArray* links = NULL);

/**
 * @brief Return owning container
 */
ICustAttribContainer* 
GetOwningContainer(CustAttrib* pLinkAttrib);

LinkAttribArray
GetCustAttrContainer( eLinkType type, INode* pNode );



#endif // !INC_rageMaxHierarchy_H_
