#ifndef LODDRAWABLELINKATTRIB_H
#define LODDRAWABLELINKATTRIB_H

//
// File::			LODDrawableLinkAttrib.cpp
// Description::	RAGE Drawable LOD Custom Attribute Container
//
// Author::			David Muir <david.muir@rockstarnorth.com>
// Date::			12 March 2009
//

// --- Include Files ------------------------------------------------------------

// Max headers
#include <Max.h>
#include <iparamm2.h>
#include <custattrib.h>
#include <icustattribcontainer.h>
#include "resource.h"

// Local headers
#include "Win32Res.h"
extern HINSTANCE g_hInstance;

// --- Defines ------------------------------------------------------------------
#define LODDRAWABLELINK_ATTR_NAME		( "LODDrawableAttributes" )
#define LODDRAWABLELINK_ATTR_CLASSID_PARTB 0x5f232f1
#define LODDRAWABLELINK_ATTR_CLASSID	( Class_ID(0x542a6bc1, LODDRAWABLELINK_ATTR_CLASSID_PARTB) )

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

/**
 * @brief RAGE Drawable LOD Attribute Container
 */
class LodDrawableLinkAttribData : public CustAttrib
{
public:
	LodDrawableLinkAttribData( );

	void SetInvalid( );
	bool IsInvalid( );

	// from CustAttrib
#if (MAX_RELEASE >= 12000)
	const MCHAR* GetName() {return LODDRAWABLELINK_ATTR_NAME;}
#else
	TCHAR* GetName() {return LODDRAWABLELINK_ATTR_NAME;}
#endif
	bool CheckCopyAttribTo(ICustAttribContainer *to);
	HWND GetRollupHWnd() {return m_hWnd;}

	// File operations
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);

	// from ReferenceMaker
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,PartID& partID,  RefMessage message);

	SClass_ID SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
	Class_ID ClassID() {return LODDRAWABLELINK_ATTR_CLASSID;}

#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

	void DeleteThis() { delete this;}
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);

	static IObjParam *iObjParams;
	bool m_invalid;
	HWND m_hWnd;

	SvGraphNodeReference SvTraverseAnimGraph(IGraphObjectManager *gom, Animatable *owner, int id, DWORD flags);

private:

	INode *refNode;
};

/**
* @brief
*/
class LodDrawableLinkAttribDataClassDesc : public ClassDesc2 
{
public:
	int 			IsPublic() {return 1;}
	void *			Create(BOOL loading) { return new LodDrawableLinkAttribData; }
	const TCHAR *	ClassName() { return GetStringResource(g_hInstance, IDS_LODDRAWABLEATTRIBS_NAME); }
	SClass_ID		SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
	Class_ID 		ClassID() {return LODDRAWABLELINK_ATTR_CLASSID;}
	const TCHAR* 	Category() {return _T("");}
	const TCHAR*	InternalName() { return _T(LODDRAWABLELINK_ATTR_NAME); }	// returns fixed parsable name (scripter-visible name)
};

// --- Globals ------------------------------------------------------------------
ClassDesc* GetLodDrawableLinkAttribDataClassDesc( );

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------


#endif // LODDRAWABLELINKATTRIB_H

// LODDrawableLinkAttrib.h
