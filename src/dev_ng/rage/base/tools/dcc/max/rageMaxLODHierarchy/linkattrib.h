#ifndef INC_LINK_DATA_H_
#define INC_LINK_DATA_H_

// Max headers
#include <Max.h>
#include <iparamm2.h>
#include <custattrib.h>
#include <icustattribcontainer.h>
#include "resource.h"
#include "LinkType.h"

// Local headers
#include "Win32Res.h"

// std lib
#include <vector>

extern HINSTANCE g_hInstance;
ClassDesc* GetLinkAttribDataClassDesc( );

#define LINK_ATTR_NAME "LinkAttributes"
#define LINK_ATTR_CLASSID_PARTB 0x1ac251c8
#define LINK_ATTR_CLASSID Class_ID(0x6af558bd, LINK_ATTR_CLASSID_PARTB)

//#define DRAWABLELINK_ATTR_NAME		( "DrawableLinkAttributes" )
// #define DRAWABLELINK_ATTR_CLASSID_PARTB 0x5f232f1
// #define DRAWABLELINK_ATTR_CLASSID	( Class_ID(0x542a6bc1, DRAWABLELINK_ATTR_CLASSID_PARTB) )

//#define DRAWABLECHILDLINK_ATTR_NAME			( "DrawablChilLinkdAttributes" )
// #define DRAWABLECHILDLINK_ATTR_CLASSID_PARTB 0xaec3fa5
// #define DRAWABLECHILDLINK_ATTR_CLASSID		( Class_ID(0x34bca368, DRAWABLECHILDLINK_ATTR_CLASSID_PARTB) )

/**
 * @brief
 */

class LinkAttribData : public CustAttrib
{
public:
	LinkAttribData() : m_hWnd(NULL), m_invalid(false), mLinkType(eLinkType_LOD)
	{}

	void SetInvalid() {m_invalid = true;}
	bool IsInvalid() {return m_invalid;}

	// from CustAttrib
#if (MAX_RELEASE >= 12000)
	const MCHAR* GetName() {return LINK_ATTR_NAME;}
#else
	TCHAR* GetName() {return LINK_ATTR_NAME;}
#endif
	bool CheckCopyAttribTo(ICustAttribContainer *to);
	HWND GetRollupHWnd() {return m_hWnd;}

	// File operations
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);

	// from ReferenceMaker
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,PartID& partID,  RefMessage message);

	SClass_ID SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
	Class_ID ClassID() {return LINK_ATTR_CLASSID;}

#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

	void DeleteThis() { delete this;}
	int NumRefs();
	int NumLinkRefNodes()
	{
		return (int)refNodes.size();
	}
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	bool HasReference(RefTargetHandle hTarget);
	void FindReferenceToDelete(RefTargetHandle hTarget);
	void AppendReference(INodePtr targ)
	{
		if(!HasReference(targ))
			ReplaceReference(NumLinkRefNodes(), targ);
	}

	static IObjParam *iObjParams;
	bool m_invalid;
	HWND m_hWnd;

	SvGraphNodeReference SvTraverseAnimGraph(IGraphObjectManager *gom, Animatable *owner, int id, DWORD flags);

	void SetLinkType(eLinkType type)
	{
		mLinkType = type;
	}
	eLinkType GetLinkType()
	{
		return mLinkType;
	}

private:

	eLinkType mLinkType;
	std::vector<INode *> refNodes;
};

/**
 * @brief
 */
class LinkAttribDataClassDesc : public ClassDesc2 
{
	public:
	int 			IsPublic() {return 1;}
	void *			Create(BOOL loading){return new LinkAttribData;}
	const TCHAR *	ClassName() { return GetStringResource(g_hInstance, IDS_LODATTRIBS_NAME); }
	SClass_ID		SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
	Class_ID 		ClassID() {return LINK_ATTR_CLASSID;}
	const TCHAR* 	Category() {return _T("");}
	const TCHAR*	InternalName() { return _T("Attributes"); }	// returns fixed parsable name (scripter-visible name)
};

#endif //INC_LINK_DATA_H_

// linkattrib.h
