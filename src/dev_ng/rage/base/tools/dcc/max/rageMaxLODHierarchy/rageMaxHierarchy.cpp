//
// File:: rageMaxHierarchy.cpp
// Description::
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 10 March 2009
//

// Local headers
#include "rageMaxHierarchy.h"
#include <algorithm>
#include <map>

// Max headers
#pragma warning ( push )
#pragma warning ( disable : 4100 4244 4238 4239 4245 4512 )
#include "Max.h"
#include "icustattribcontainer.h"
#include "custattrib.h"
#pragma warning ( pop )

typedef std::map<int, CustAttrib*> CustAttribMap;
typedef std::pair<int, CustAttrib*> CustAttribMapPair;
typedef CustAttribMap::iterator IntIterator;

static Class_ID sTypeCLassIDMap[] = {LINK_ATTR_CLASSID, LODDRAWABLELINK_ATTR_CLASSID, LODDRAWABLECHILDLINK_ATTR_CLASSID};
int FindClassID(Class_ID id)
{
	for(int k=0;k<3;k++)
	{
		if(sTypeCLassIDMap[k]==id)
			return k;
	}
	return -1;
}

BOOL 
IsRightTypeOfContainer(CustAttrib* pCustAttrib, eLinkType type)
{
	if(pCustAttrib->ClassID()!=LINK_ATTR_CLASSID)
		return FALSE;
	else if(	dynamic_cast<LinkAttribData*>(pCustAttrib)->GetLinkType() != type &&
				type != eLinkType_ANY	)
		return FALSE;

	return TRUE;
}


CustAttrib* ReplaceLegacyClass(CustAttrib* pCustAttrib,ICustAttribContainer* pContainer, int index, LinkTypeArray &containedTypes)
{
	INode *pRefNode = NULL;
	switch(pCustAttrib->ClassID().PartB())
	{
	case LINK_ATTR_CLASSID_PARTB:
		{
			//Just get rid of duplicates
			LinkAttribData* p_llaData = dynamic_cast<LinkAttribData*>(pCustAttrib);
			if(p_llaData->NumLinkRefNodes()<=0)
			{
				pContainer->RemoveCustAttrib(index);
				return NULL;
			}

			if(p_llaData && p_llaData->GetLinkType()==eLinkType_LOD)
			{
				LinkTypeIterator it = find(containedTypes.begin(), containedTypes.end(), eLinkType_LOD);
				if(!p_llaData->GetReference(0) || it!=containedTypes.end())
				{
					pContainer->RemoveCustAttrib(index);
					return NULL;
				}
				else
				{
					containedTypes.push_back(eLinkType_LOD);
					return pCustAttrib;
				}
			}
		}
		break;
	case LODDRAWABLELINK_ATTR_CLASSID_PARTB:
		{
			//wrong class!!! safe reference and deallocate!
			pRefNode = (INode*)pCustAttrib->GetReference(0);
			pCustAttrib->ReplaceReference(0,NULL);
			pContainer->RemoveCustAttrib(index);
			LinkTypeIterator it = find(containedTypes.begin(), containedTypes.end(), eLinkType_DRAWLOD);
			if(!pRefNode || it!=containedTypes.end())
			{
				pContainer->RemoveCustAttrib(index);
				return NULL;
			}
			pCustAttrib = new LinkAttribData();
			if(pCustAttrib)
			{
				pContainer->AppendCustAttrib(pCustAttrib);
				((LinkAttribData*)pCustAttrib)->SetLinkType(eLinkType_DRAWLOD);
				pCustAttrib->ReplaceReference(0, pRefNode);
				containedTypes.push_back(eLinkType_DRAWLOD);
				return pCustAttrib;
			}
		}
		break;
	case LODDRAWABLECHILDLINK_ATTR_CLASSID_PARTB:
		{
			//wrong class!!! safe reference and deallocate!
			pRefNode = (INode*)pCustAttrib->GetReference(0);
			pCustAttrib->ReplaceReference(0,NULL);
			pContainer->RemoveCustAttrib(index);
			LinkTypeIterator it = find(containedTypes.begin(), containedTypes.end(), eLinkType_RENDERSIM);
			if(!pRefNode || it!=containedTypes.end())
			{
				pContainer->RemoveCustAttrib(index);
				return NULL;
			}
			pCustAttrib = new LinkAttribData();
			if(pCustAttrib)
			{
				pContainer->AppendCustAttrib(pCustAttrib);
				((LinkAttribData*)pCustAttrib)->SetLinkType(eLinkType_RENDERSIM);
				pCustAttrib->ReplaceReference(0, pRefNode);
				containedTypes.push_back(eLinkType_RENDERSIM);
				return pCustAttrib;
			}
		}
		break;
	}
	return pCustAttrib;
}

void
ReplaceLegacyAttributes(INode* pNode, CustAttribMap *pIndexMap = NULL )
{
	Animatable* pActual = (Animatable*)pNode;
	CustAttrib* pCustAttrib = NULL;
	ICustAttribContainer* pContainer = pActual->GetCustAttribContainer();

	// Check that the node doesn't already have the Drawable  container.
	if ( pContainer )
	{
		/// first some legacy removing
		LinkTypeArray containedTypes;
		containedTypes.reserve(3);
		for(int i=pContainer->GetNumCustAttribs()-1; i>=0; i--)
		{
			pCustAttrib = pContainer->GetCustAttrib(i);
			bool replaced = false;
			// if it's ONE of the container classes
			if(pCustAttrib && -1!=FindClassID(pCustAttrib->ClassID()))
			{
				pCustAttrib = ReplaceLegacyClass(pCustAttrib, pContainer,i, containedTypes);
			}
			if(pIndexMap)
				pIndexMap->insert(CustAttribMapPair(i, pCustAttrib));
		}	
	}
}


LinkAttribArray GetCustAttrContainer( eLinkType type, INode* pNode )
{
	ICustAttribContainer* pContainer = pNode->GetCustAttribContainer();
	LinkAttribArray ret;
	if ( pContainer )
	{
		for ( int i=0; i<pContainer->GetNumCustAttribs(); i++ )
		{
			CustAttrib* pCustAttrib = pContainer->GetCustAttrib(i);
			if(pCustAttrib && IsRightTypeOfContainer(pCustAttrib, type))
			{
				LinkAttribData* pLinkAttrib = dynamic_cast<LinkAttribData*>(pCustAttrib);
				if(pLinkAttrib)
					ret.push_back(pLinkAttrib);
			}
		}
	}
	return ret;
}

INode* 
GetParent( eLinkType type, INode* pNode )
{

	ReplaceLegacyAttributes(pNode);

	LinkAttribArray links = GetCustAttrContainer(type, pNode);
	if(links.size()<=0)
		return NULL;

	INode* pParent = (INode*)links[0]->GetReference(0);

	if(pParent)
	{
		return pParent;
	}
	return NULL;
}

RAGEMAXHIERARCHY_API 
bool SetParent( eLinkType type, INode* pNode, INode* pDrawableParentNode, bool append )
{
	Animatable* p_actual = (Animatable*)pNode;

	if ( pNode->GetName() == pDrawableParentNode->GetName() )
		return ( false );

	ReplaceLegacyAttributes(pNode);

	if(!HasContainer(type, pNode))
		AddContainer(type, pNode);

	ICustAttribContainer* pContainer = p_actual->GetCustAttribContainer();
	if ( pContainer )
	{
		for(int i=0; i<pContainer->GetNumCustAttribs(); i++)
		{
			CustAttrib* pCustAttrib = pContainer->GetCustAttrib(i);
			// if it's one of our classids (legacy handling, only first one used from now on)
			if(pCustAttrib && IsRightTypeOfContainer(pCustAttrib, type))
			{
				LinkAttribArray links = GetCustAttrContainer(type, pNode);
				if(links.size()<=0)
					continue;
				LinkAttribData* p_llaData = links[0];
				p_llaData->SetLinkType(type);
				if(append)
					p_llaData->AppendReference(pDrawableParentNode);
				else
					p_llaData->ReplaceReference( 0, pDrawableParentNode );
				return ( true );
			}
		}
	}

	return ( false );

}

RAGEMAXHIERARCHY_API void 
GetParents(  eLinkType type, INode* pNode, Tab<INode*>& llAdd )
{

	ReplaceLegacyAttributes(pNode);

	LinkAttribArray links = GetCustAttrContainer(type, pNode);
	if(links.size()<=0)
		return;

	for (int linkIndex = 0;linkIndex<links.size();linkIndex++)
	{
		for(int i=0;i<links[linkIndex]->NumLinkRefNodes();i++)
		{
			INodePtr pNode = dynamic_cast<INodePtr>(links[linkIndex]->GetReference(i));
			llAdd.Append(1, &pNode);
		}
	}
}


int FindAttributeInContainer(CustAttrib* pLinkAttrib, ICustAttribContainer* pContainer)
{
	for(int k=0;k<pContainer->GetNumCustAttribs();k++)
	{
		if(pContainer->GetCustAttrib(k)==pLinkAttrib)
			return k;
	}
	return -1;
}

ICustAttribContainer* 
GetOwningContainer(CustAttrib* pLinkAttrib)
{
	DependentIterator refSubItem(pLinkAttrib);
	ReferenceMaker* submaker = NULL;
	ICustAttribContainer* pContainer = NULL;
	while(NULL != (submaker = refSubItem.Next()))
	{
		Class_ID id = submaker->ClassID();
		if ( CUSTATTRIB_CONTAINER_CLASS_ID == id )
		{
			return (ICustAttribContainer*)submaker;
		}
	}
	return NULL;
}

void 
GetChildren( eLinkType type, INode* pNode, Tab<INode*>& llAdd, LinkAttribArray* links )
{
	DependentIterator refItem((ReferenceTarget*)pNode);
	ReferenceMaker* maker = NULL;
	Interface *pI = GetCOREInterface();

	while(NULL != (maker = refItem.Next()))
	{
		TSTR strName;

		Class_ID id = maker->ClassID();

		// if it's one of our classids (legacy handling, only first one used from now on)
		if(-1 != FindClassID(id))
		{
			CustAttrib* pCustAttrib = dynamic_cast<CustAttrib*>(maker);
			if(!pCustAttrib)
				continue;

			ICustAttribContainer* pContainer = GetOwningContainer(pCustAttrib);
			if(!pContainer)
			{
				continue;
			}

			INode* pChild = dynamic_cast<INode*>(pContainer->GetOwner());
			if(!pChild)
				continue;
			INodePtr actNode = pChild->GetActualINode();
			MCHAR *childName = pChild->GetName();
			INodePtr nameResolveNode = pI->GetINodeByName(childName);
			if(!actNode || !nameResolveNode)
				continue;

			// Get former index
			int index = FindAttributeInContainer(pCustAttrib, pContainer);
			if( index<0 )
				continue;

			CustAttribMap replacedIndeces;
			ReplaceLegacyAttributes(pChild, &replacedIndeces);
			pCustAttrib = replacedIndeces[index];
			if(!pCustAttrib)
				continue;

			if( !IsRightTypeOfContainer(pCustAttrib, type))
				continue;

			llAdd.Append(1, &pChild);
			LinkAttribData* pLinkAttrib = dynamic_cast<LinkAttribData*>(pCustAttrib);
			if(links && pLinkAttrib)
				links->push_back(pLinkAttrib);
		}
	}
}


bool 
AddContainer( eLinkType type, INode* pNode )
{
	Animatable* pActual = (Animatable*)pNode;
	LinkAttribData* pCustAttrib = NULL;
	ICustAttribContainer* pContainer = pActual->GetCustAttribContainer();
	INode *pRefNode = NULL;

	ReplaceLegacyAttributes(pNode);

	// Check that the node doesn't already have the Drawable  container.
	if ( pContainer )
	{
		/// first some legacy removing
		for(int i=0; i<pContainer->GetNumCustAttribs(); i++)
		{
			pCustAttrib = dynamic_cast<LinkAttribData*>(pContainer->GetCustAttrib(i));
			// if it's ONE of the container classes
			if(pCustAttrib && IsRightTypeOfContainer(pCustAttrib, type))
			{
				// we're already attached, return
				return ( false );
			}
		}
	}

	if ( NULL == pContainer )
	{
		pActual->AllocCustAttribContainer();
		pContainer = pActual->GetCustAttribContainer();
	}

	pCustAttrib = new LinkAttribData();
	if(pCustAttrib)
	{
		pContainer->AppendCustAttrib(pCustAttrib);
		pCustAttrib->SetLinkType(type);
		pCustAttrib->ReplaceReference(0, pRefNode);
	}
	else
		return false;

	return ( true );
}

bool 
RemoveContainer( eLinkType type, INode* pNode )
{
	Animatable* p_actual = (Animatable*)pNode;
	ICustAttribContainer* pContainer = p_actual->GetCustAttribContainer();

	ReplaceLegacyAttributes(pNode);
	bool foundSomeMatchingData = false;
	if ( pContainer )
	{
		for(int i=pContainer->GetNumCustAttribs()-1; i>=0 ; i--)
		{
			LinkAttribData* pCustAttrib = dynamic_cast<LinkAttribData*>(pContainer->GetCustAttrib(i));
			if( pCustAttrib && 
				IsRightTypeOfContainer(pCustAttrib,type))
			{
				LinkAttribData* p_llaData = (LinkAttribData*)pCustAttrib;
				for(int refIndex = 0; refIndex<p_llaData->NumLinkRefNodes(); refIndex++)
					p_llaData->ReplaceReference(refIndex,NULL);
				pContainer->RemoveCustAttrib(i);

				foundSomeMatchingData = true;
			}
		}
	}

	return ( foundSomeMatchingData );
}

bool 
RemoveParent( eLinkType type, INode* pNode, INode* pNodeParent )
{
	Animatable* p_actual = (Animatable*)pNode;
	ICustAttribContainer* pContainer = p_actual->GetCustAttribContainer();

	ReplaceLegacyAttributes(pNode);

	if ( pContainer )
	{
		for(int i=0; i<pContainer->GetNumCustAttribs(); i++)
		{
			LinkAttribData* pCustAttrib = dynamic_cast<LinkAttribData*>(pContainer->GetCustAttrib(i));
			if( pCustAttrib && 
				IsRightTypeOfContainer(pCustAttrib,type))
			{
				LinkAttribData* p_llaData = (LinkAttribData*)pCustAttrib;
				p_llaData->FindReferenceToDelete(pNodeParent);

				return ( true );
			}
		}
	}

	return ( false );
}

bool 
HasContainer( eLinkType type, INode* pNode )
{
	Animatable* pActual = (Animatable*)pNode;
	ICustAttribContainer* pContainer = pActual->GetCustAttribContainer();

	ReplaceLegacyAttributes(pNode);

	if ( pContainer )
	{
		for(int i=0; i<pContainer->GetNumCustAttribs(); i++)
		{
			LinkAttribData* pCustAttrib = dynamic_cast<LinkAttribData*>(pContainer->GetCustAttrib(i));
			if(	pCustAttrib  && 
				IsRightTypeOfContainer(pCustAttrib,type))
			{
				return ( true );
			}
		}
	}

	return ( false );
}
