////
//// filename:	MaxScript.cpp
//// description:	rageMaxLODHierarchy MaxScript Functions
////
//
//// --- Include Files ------------------------------------------------------------
//
//// Max headers
//#include "max.h"
//#include "maxscrpt\MaxScrpt.h"
//#include "maxscrpt\MAXclses.h"
//#include "maxscrpt\Numbers.h"
//#include "maxscrpt\3dmath.h"
//#include "maxscrpt\Strings.h"
//#include "maxscrpt\definsfn.h"
//#include "maxscrpt\Listener.h"
//
//// Local headers
//#include "rageMaxLODHierarchy.h"
//#include "lodlinkattrib.h"
//#include "LODDrawableLinkAttrib.h"
//#include "LODDrawableChildLinkAttrib.h"
//#include "lodview.h"
//
//// --- Defines ------------------------------------------------------------------
//
//// --- Constants ----------------------------------------------------------------
//
//// --- Structure/Class Definitions ----------------------------------------------
//
//// --- Globals ------------------------------------------------------------------
//
//// --- Static Globals -----------------------------------------------------------
//
//// --- Static class members -----------------------------------------------------
//
//// --- Code ---------------------------------------------------------------------
//
//// ------------------------------------------------------------------------------
//// 	MaxScript Function Declarations
//// ------------------------------------------------------------------------------
//
//// Scene LOD Hierarchy MaxScript Functions
//def_visible_primitive( add_lodattr,			"AddLodAttr");
//def_visible_primitive( set_lodattrpar,		"SetLodAttrParent");
//def_visible_primitive( get_lodattrpar,		"GetLodAttrParent");
//def_visible_primitive( del_lodattr,			"DelLodAttr");
//def_visible_primitive( show_lodview,		"ShowLODView");
//
//// RAGE Drawable LOD Hierarchy MaxScript Functions
//def_visible_primitive( loddrawable_add,			"LodDrawable_Add" );
//def_visible_primitive( loddrawable_has_container, "LodDrawable_HasContainer" );
//def_visible_primitive( loddrawable_del,			"LodDrawable_Delete" );
//def_visible_primitive( loddrawable_set_parent,	"LodDrawable_SetParent" );
//def_visible_primitive( loddrawable_get_parent,	"LodDrawable_GetParent" );
//def_visible_primitive( loddrawable_show,		"LodDrawable_Show" );
//
//// RAGE Drawable Child
//def_visible_primitive( loddrawablechild_add,			"LodDrawableChild_Add" );
//def_visible_primitive( loddrawablechild_has_container, "LodDrawableChild_HasContainer" );
//def_visible_primitive( loddrawablechild_del,			"LodDrawableChild_Delete" );
//def_visible_primitive( loddrawablechild_set,			"LodDrawableChild_Set" );
//def_visible_primitive( loddrawablechild_get,			"LodDrawableChild_Get" );
//
//
//// ------------------------------------------------------------------------------
//// 	Private Functions
//// ------------------------------------------------------------------------------
//
//// /**
//// * @brief Determine whether LOD Drawable attribute container is present on node.
//// * @return true_value if container present, false_value otherwise.
//// */
//// Value*
//// has_container_customattribute( Value** arg_list, int count, const char* attributeName)
//// {
//// 	INode* pNode = arg_list[0]->to_node();
//// 	Animatable* pActual = (Animatable*)pNode;
//// 	ICustAttribContainer* pContainer = pActual->GetCustAttribContainer();
//// 	if ( pContainer )
//// 	{
//// 		for(int i=0; i<pContainer->GetNumCustAttribs(); i++)
//// 		{
//// 			CustAttrib* pCustAttrib = pContainer->GetCustAttrib(i);
//// 			if(pCustAttrib && ( 0 == strcmp(pCustAttrib->GetName(), attributeName)) )
//// 			{
//// 				return ( &true_value );
//// 			}
//// 		}
//// 	}
//// 
//// 	return ( &false_value );
//// }
//
//// ------------------------------------------------------------------------------
//// 	MaxScript Function Definitions
//// ------------------------------------------------------------------------------
//
////adds the lodlink custom attribute to an inode
//Value *add_lodattr_cf(Value** arg_list, int count)
//{
//	INode *pNode = arg_list[0]->to_node();
//
//	if(::AddContainer(LODLINK_ATTR_CLASSID, pNode))
//		return &true_value;
//	else
//		return &false_value;
//}
//
//////////////////////////////////////////////////////////////////////////////////////////////////
////sets the parent inode in a lodlink custom attribute
//Value *set_lodattrpar_cf(Value** arg_list, int count)
//{
//	INode *pNode = arg_list[0]->to_node();
//	INode *pNodeRef = arg_list[1]->to_node();
//	Animatable* p_actual = (Animatable*)pNode;
//
//	if(pNode->GetName() == pNodeRef->GetName())
//	{
//		return &false_value;
//	}
//
//	if(::SetLODParent(LODLINK_ATTR_CLASSID, pNode, pNodeRef))
//		return &true_value;
//	else
//		return &false_value;
//}
//
//////////////////////////////////////////////////////////////////////////////////////////////////
////retrieves the parent inode in a lodlink custom attribute
//Value *get_lodattrpar_cf(Value** arg_list, int count)
//{
//	INode *pNode = arg_list[0]->to_node();
//
//	INode* pRet = ::GetLODParent(LODLINK_ATTR_CLASSID, pNode );
//	if(pRet)
//	{
//		return_protected(new MAXNode(pRet));
//	}
//	else
//		return &undefined;
//}
//
//////////////////////////////////////////////////////////////////////////////////////////////////
////delete the lodlink custom attribute of an inode
//Value *del_lodattr_cf(Value** arg_list, int count)
//{
//	INode *pNode = arg_list[0]->to_node();
//	if(::RemoveContainer(LODLINK_ATTR_CLASSID, pNode))
//		return &true_value;
//	else
//		return &false_value;
//}
//
//
//Value *show_lodview_cf(Value** arg_list, int count)
//{
//	ShowLODDialog();
//
//	return &true_value;
//}
//
////---------------------------------------------------------------------------
//// RAGE Drawable LOD MaxScript Functions
////---------------------------------------------------------------------------
//
///**
// * @brief Add LOD Drawable custom attribute container to a node
// * @return true_value on success.
// */
//Value*
//loddrawable_add_cf( Value** arg_list, int count )
//{
//	INode *pNode = arg_list[0]->to_node();
//
//	if(::AddContainer(LODDRAWABLELINK_ATTR_CLASSID, pNode))
//		return &true_value;
//	else
//		return &false_value;
//}
//
///**
// * @brief Determine whether LOD Drawable attribute container is present on node.
// * @return true_value if container present, false_value otherwise.
// */
//Value*
//loddrawable_has_container_cf( Value** arg_list, int count )
//{
//	INode *pNode = arg_list[0]->to_node();
//	if(::HasContainer(LODDRAWABLELINK_ATTR_CLASSID, pNode))
//		return &true_value;
//	else
//		return &false_value;
//}
//
///**
// * @brief Set LOD Drawable parent of this node to another node.
// * @return true_value on success, otherwise false_value.
// */
//Value*
//loddrawable_set_parent_cf( Value** arg_list, int count )
//{
//	INode *pNode = arg_list[0]->to_node();
//	INode *pNodeRef = arg_list[1]->to_node();
//	if(::SetLODParent(LODDRAWABLELINK_ATTR_CLASSID, pNode, pNodeRef))
//		return &true_value;
//
//	mprintf( "Failed to set LOD parent as node %s does not have valid custom attribute container.", pNode->GetName() );
//	return &false_value;
//}
//
///**
// * @brief  Get LOD Drawable parent of this node.
// * @return Node if parent is set, undefined otherwise.
// */
//Value*
//loddrawable_get_parent_cf( Value** arg_list, int count )
//{
//	INode *pNode = arg_list[0]->to_node();
//	Animatable* p_actual = (Animatable*)pNode;
//
//	INode* pRet = ::GetLODParent(LODDRAWABLELINK_ATTR_CLASSID, pNode );
//	if(pRet)
//	{
//		return_protected(new MAXNode(pRet));
//	}
//	else
//		return &undefined;
//}
//
///**
// * @brief Remove LOD Drawable custom attribute container to a node
// * @return true_value on success, false_value otherwise.
// */
//Value*
//loddrawable_del_cf( Value** arg_list, int count )
//{
//	INode *pNode = arg_list[0]->to_node();
//	if(::RemoveContainer(LODDRAWABLELINK_ATTR_CLASSID, pNode))
//		return &true_value;
//	else
//		return &false_value;
//}
//
///**
// * @brief
// */
//Value* loddrawable_show_cf( Value** arg_list, int count )
//{
//	ShowLODDialog( Drawable );
//
//	return &true_value;
//}
//
///**
//* @brief Add LOD Sibling custom attribute container to a node
//* @return true_value on success.
//*/
//Value*
//loddrawablechild_add_cf( Value** arg_list, int count )
//{
//	INode *pNode = arg_list[0]->to_node();
//	if(::AddContainer(LODDRAWABLECHILDLINK_ATTR_CLASSID, pNode))
//		return &true_value;
//	else
//		return &false_value;
//}
//
///**
//* @brief Determine whether LOD Drawable attribute container is present on node.
//* @return true_value if container present, false_value otherwise.
//*/
//Value*
//loddrawablechild_has_container_cf( Value** arg_list, int count )
//{
//	INode *pNode = arg_list[0]->to_node();
//	if(::HasContainer(LODDRAWABLECHILDLINK_ATTR_CLASSID, pNode))
//		return &true_value;
//	else
//		return &false_value;
//}
//
///**
//* @brief Set LOD Drawable child of this node to another node.
//* @return true_value on success, otherwise false_value.
//*/
//Value*
//loddrawablechild_set_cf( Value** arg_list, int count )
//{
//	// the render mesh node that is attached
//	INode *pNode = arg_list[0]->to_node();
//	// The LOD node that is attached TO
//	INode *pNodeRef = arg_list[1]->to_node();
//	if(::SetLODParent(LODDRAWABLECHILDLINK_ATTR_CLASSID, pNode, pNodeRef))
//		return &true_value;
//	else
//		return &false_value;
//}
//
///**
//* @brief  Get LOD Sibling of this node.
//* @return Node if sibling is set, undefined otherwise.
//*/
//Value*
//loddrawablechild_get_cf( Value** arg_list, int count )
//{
//	INode *pNode = arg_list[0]->to_node();
//	Animatable* p_actual = (Animatable*)pNode;
//
//	INode* pRet = GetLODParent(LODDRAWABLECHILDLINK_ATTR_CLASSID, pNode );
//	if(pRet)
//	{
//		return_protected(new MAXNode(pRet));
//	}
//	else
//		return &undefined;
//}
//
///**
//* @brief Remove LOD Sibling custom attribute container to a node
//* @return true_value on success, false_value otherwise.
//*/
//Value*
//loddrawablechild_del_cf( Value** arg_list, int count )
//{
//	INode *pNode = arg_list[0]->to_node();
//	if(::RemoveContainer(LODDRAWABLECHILDLINK_ATTR_CLASSID, pNode))
//		return &true_value;
//	else
//		return &false_value;
//}
//
//
//// MaxScript.cpp
