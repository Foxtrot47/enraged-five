
#include "HierarchyInterface.h"
#include "rageMaxHierarchy.h"
#include "linkview.h"
#include <algorithm>
//---------------------------------------------------------------------------
// FpSceneHierarchy
//---------------------------------------------------------------------------

class FpSceneHierarchy : public IFpSceneHierarchy
{
public:
	INode* GetParent( eLinkType type, INode* pNode );
	void GetParents( eLinkType type, INode* pNode, Tab<INode*>& llParents );
	void GetChildren( eLinkType type, INode* pNode, Tab<INode*>& llChildren );

	bool HasParent( eLinkType type, INode* pNode );
	bool HasChildren( eLinkType type, INode* pNode );
	bool RemoveParent( eLinkType type, INode* pNode, INode* pNodeParent );

	bool AddContainer( eLinkType type, INode* pNode );
	bool RemoveContainer( eLinkType type, INode* pNode );
	bool HasContainer( eLinkType type, INode* pNode );
	bool SetParent( eLinkType type, INode* pNode, INode* pParent, bool append );

	TCHAR* GetLinkType( INode* pNode );

	void SetChannelName( eLinkType type, TCHAR *name );
	TCHAR* GetChannelName( eLinkType type );
	int GetChannel( TCHAR* channelName );

	Tab<int> GetNodeParentLinkTypes(INode* pNode);
	Tab<int> GetNodeChildLinkTypes(INode* pNode);

	void ShowDialog( eLinkType type );

	void Init()
	{
		m_LinkNames.clear();
		TCHAR *names[] = {_T("Scene LOD"), _T("Drawable LOD"), _T("Render-Sim"), _T("Cloth Collision")};
		for(int i=0;i<4;i++)
			SetChannelName(i, names[i]);
	}

	DECLARE_DESCRIPTOR(FpSceneHierarchy);
	BEGIN_FUNCTION_MAP;
	FN_2(IFpSceneHierarchy::em_getParent, TYPE_INODE, GetParent, TYPE_INT, TYPE_INODE);
	VFN_3(IFpSceneHierarchy::em_getParents, GetParents, TYPE_INT, TYPE_INODE, TYPE_INODE_TAB_BR);
	VFN_3(IFpSceneHierarchy::em_getChildren, GetChildren, TYPE_INT, TYPE_INODE, TYPE_INODE_TAB_BR);
	FN_2(IFpSceneHierarchy::em_addContainer, TYPE_BOOL, AddContainer, TYPE_INT, TYPE_INODE);
	FN_3(IFpSceneHierarchy::em_RemoveParent, TYPE_BOOL, RemoveParent, TYPE_INT, TYPE_INODE,TYPE_INODE);
	FN_2(IFpSceneHierarchy::em_removeContainer, TYPE_BOOL, RemoveContainer, TYPE_INT, TYPE_INODE);
	FN_2(IFpSceneHierarchy::em_hasContainer, TYPE_BOOL, HasContainer, TYPE_INT, TYPE_INODE);
	FN_4(IFpSceneHierarchy::em_setParent, TYPE_BOOL, SetParent, TYPE_INT, TYPE_INODE, TYPE_INODE, TYPE_BOOL);
	VFN_1(IFpSceneHierarchy::em_ShowDialog, ShowDialog, TYPE_INT);
	FN_1(IFpSceneHierarchy::em_GetLinkType, TYPE_STRING, GetLinkType, TYPE_INODE);
	VFN_2(IFpSceneHierarchy::em_SetChannelName, SetChannelName, TYPE_INT, TYPE_STRING);
	FN_1(IFpSceneHierarchy::em_GetChannelName, TYPE_STRING, GetChannelName, TYPE_INT);
	FN_1(IFpSceneHierarchy::em_GetChannel, TYPE_INT, GetChannel, TYPE_STRING);
	FN_1(IFpSceneHierarchy::em_GetNodeParentLinkTypes, TYPE_INT_TAB_BV, GetNodeParentLinkTypes, TYPE_INODE);
	FN_1(IFpSceneHierarchy::em_GetNodeChildLinkTypes, TYPE_INT_TAB_BV, GetNodeChildLinkTypes, TYPE_INODE);
	END_FUNCTION_MAP;

	std::vector<TCHAR *> m_LinkNames;
};

FpSceneHierarchy FpSceneHierarchyDesc(
	FP_SCENEHIERARCHY_INTERFACE,
	_T("RsSceneLink"),
	IDS_SCENELODHIERARCHY_NAME,
	NULL,
	FP_CORE,

	IFpSceneHierarchy::em_getParent, _T("GetParent"), 0, TYPE_INODE, 0, 2,
	_T("linkType"), 0, TYPE_INT,
	_T("node"), 0, TYPE_INODE,
	IFpSceneHierarchy::em_getParents, _T("GetParents"), 0 ,TYPE_VOID, 0, 3,
	_T("linkType"), 0, TYPE_INT,
	_T("node"), 0, TYPE_INODE,
	_T("parents"), 0, TYPE_INODE_TAB_BR,
	IFpSceneHierarchy::em_getChildren, _T("GetChildren"), 0 ,TYPE_VOID, 0, 3,
	_T("linkType"), 0, TYPE_INT,
	_T("node"), 0, TYPE_INODE,
	_T("children"), 0, TYPE_INODE_TAB_BR,
	IFpSceneHierarchy::em_addContainer, _T("AddContainer"), 0, TYPE_BOOL, 0, 2,
	_T("linkType"), 0, TYPE_INT,
	_T("node"), 0, TYPE_INODE,
	IFpSceneHierarchy::em_RemoveParent, _T("RemoveParent"), 0, TYPE_BOOL, 0, 3,
	_T("linkType"), 0, TYPE_INT,
	_T("node"), 0, TYPE_INODE,
	_T("parent"), 0, TYPE_INODE,
	IFpSceneHierarchy::em_removeContainer, _T("RemoveContainer"), 0, TYPE_BOOL, 0, 2,
	_T("linkType"), 0, TYPE_INT,
	_T("node"), 0, TYPE_INODE,
	IFpSceneHierarchy::em_hasContainer, _T("HasContainer"), 0, TYPE_BOOL, 0, 2,
	_T("linkType"), 0, TYPE_INT,
	_T("node"), 0, TYPE_INODE,
	IFpSceneHierarchy::em_setParent, _T("SetParent"), 0, TYPE_BOOL, 0, 4,
	_T("linkType"), 0, TYPE_INT,
	_T("node"), 0, TYPE_INODE,
	_T("parent"), 0, TYPE_INODE,
	_T("append"), 0, TYPE_BOOL, f_keyArgDefault, FALSE,
	IFpSceneHierarchy::em_ShowDialog, _T("ShowDialog"), 0, TYPE_VOID, 0, 1,
	_T("linkType"), 0, TYPE_INT,
	IFpSceneHierarchy::em_GetLinkType, _T("GetLinkType"), 0, TYPE_STRING, 0, 1,
	_T("node"), 0, TYPE_INODE,
	IFpSceneHierarchy::em_SetChannelName, _T("SetChannelName"), 0, TYPE_VOID, 0, 2,
	_T("linkType"), 0, TYPE_INT,
	_T("name"), 0, TYPE_STRING,
	IFpSceneHierarchy::em_GetChannelName, _T("GetChannelName"), 0, TYPE_STRING, 0, 1,
	_T("linkType"), 0, TYPE_INT,
	IFpSceneHierarchy::em_GetChannel, _T("GetChannel"), 0, TYPE_INT, 0, 1,
	_T("linkType"), 0, TYPE_STRING,
	IFpSceneHierarchy::em_GetNodeParentLinkTypes, _T("GetNodeParentLinkTypes"), 0, TYPE_TAB, 0, 1,
	_T("node"), 0, TYPE_INODE,
	IFpSceneHierarchy::em_GetNodeChildLinkTypes, _T("GetNodeChildLinkTypes"), 0, TYPE_TAB, 0, 1,
	_T("node"), 0, TYPE_INODE,

	end
);

INode* 
FpSceneHierarchy::GetParent( eLinkType type, INode* pNode )
{
	if(!pNode)
		return NULL;

	return ( ::GetParent(type, pNode ) );
}

void 
FpSceneHierarchy::GetChildren( eLinkType type, INode* pNode, Tab<INode*>& llChildren )
{
	::GetChildren(type, pNode, llChildren );
}

void 
FpSceneHierarchy::GetParents( eLinkType type, INode* pNode, Tab<INode*>& llParents )
{
	::GetParents(type, pNode, llParents );
}

bool 
FpSceneHierarchy::HasParent( eLinkType type, INode* pNode )
{
	if(!pNode)
		return false;

	Tab<INode*> llParents;
	::GetParents( type, pNode, llParents );
	return ( 0 != llParents.Count() );
}

bool 
FpSceneHierarchy::HasChildren( eLinkType type, INode* pNode )
{
	if(!pNode)
		return false;

	Tab<INode*> llChildren;
	::GetChildren( type, pNode, llChildren );
	return ( 0 != llChildren.Count() );
}

bool 
FpSceneHierarchy::AddContainer( eLinkType type, INode* pNode )
{
	if(!pNode)
		return false;

	return ::AddContainer(type, pNode);
}

bool 
FpSceneHierarchy::RemoveParent( eLinkType type, INode* pNode, INode* pNodeParent )
{
	if(!pNode)
		return false;

	return ::RemoveParent(type, pNode, pNodeParent);
}

bool 
FpSceneHierarchy::RemoveContainer( eLinkType type, INode* pNode )
{
	if(!pNode)
		return false;

	return ::RemoveContainer(type, pNode);
}

bool 
FpSceneHierarchy::HasContainer( eLinkType type, INode* pNode )
{
	if(!pNode)
		return false;

	return ::HasContainer(type, pNode);
}

bool 
FpSceneHierarchy::SetParent( eLinkType type, INode* pNode, INode* pParent, bool append )
{
	if(!pNode || !pParent)
		return false;

	return ::SetParent(type, pNode, pParent, append);
}

void 
FpSceneHierarchy::ShowDialog( eLinkType type )
{
	return ::ShowDialog(type);
}

// bool 
// FpSceneHierarchy::SetLinkType( eLinkType type, INode* pNode )
// {
// 	if(!pNode)
// 		return false;
// 
// 	if(HasContainer(eLinkType_ANY, pNode))
// 	{
// 		LinkAttribData *pData = dynamic_cast<LinkAttribData*>(GetCustAttrContainer(eLinkType_ANY, pNode));
// 		pData->SetLinkType(type);
// 		return true;
// 	}
// 	else if(HasChildren(eLinkType_ANY, pNode))
// 	{
// 		Tab<INode*> children;
// 		GetChildren(eLinkType_ANY, pNode, children);
// 		for(int k=0;k<children.Count(); k++)
// 		{
// 			LinkAttribData *pData = dynamic_cast<LinkAttribData*>(GetCustAttrContainer(eLinkType_ANY, children[k]));
// 			pData->SetLinkType(type);
// 		}
// 		return true;
// 	}
// 	return false;
// }

void FpSceneHierarchy::SetChannelName( eLinkType type, TCHAR *name )
{
	if(m_LinkNames.size()<=(int)type)
		m_LinkNames.resize((int)type+1, _T("undefined"));
	
	TCHAR *nameAlloc = new TCHAR[128];
	_tcscpy_s(nameAlloc,sizeof(TCHAR)*128, name);
	m_LinkNames[(int)type] = _T(nameAlloc);
}
TCHAR* FpSceneHierarchy::GetChannelName( eLinkType type )
{
	if(m_LinkNames.size()<=(int)type)
		return _T("undefined");
	return m_LinkNames[(int)type];
}

int FpSceneHierarchy::GetChannel( TCHAR* channelName )
{
	for(int channelindex=0;channelindex<m_LinkNames.size();channelindex++)
	{
		if(0==strcmp(m_LinkNames[channelindex], channelName))
		{
			return channelindex;
		}
	}
	return eLinkType_ANY;
}

Tab<int> FpSceneHierarchy::GetNodeParentLinkTypes(INode* pNode)
{
	Tab<int> types;
	LinkAttribArray links = GetCustAttrContainer(eLinkType_ANY, pNode);
	for(LinkAttribIterator it = links.begin(); it != links.end(); ++it)
	{
		int type = (*it)->GetLinkType();
		types.Append(1,&type);
	}
	return types;
}
// TCHAR* FpSceneHierarchy::GetNodeParentLinkTypes(INode* pNode)
// {
// 	TCHAR buffer[256] = {0};
// 	LinkAttribArray links = GetCustAttrContainer(eLinkType_ANY, pNode);
// 	bool first = true;
// 	for(LinkAttribIterator it = links.begin(); it != links.end(); ++it)
// 	{
// 		int type = (*it)->GetLinkType();
// 		char temp[64];
// 		if(first)
// 		{
// 			strcat_s(buffer, ",");
// 			first = false
// 		}
// 		strcat_s(buffer, itoa(type,temp, 10));
// 	}
// 	one_value_local(String *pLinkTypes);
// 	vl.pLinkTypes = new String();
// 	return vl.pLinkTypes;
// }

Tab<int> FpSceneHierarchy::GetNodeChildLinkTypes(INode* pNode)
{
	Tab<int> types;
	Tab<INode*> lcAdd;
	LinkAttribArray links;
	::GetChildren(eLinkType_ANY, pNode, lcAdd, &links);
	for(LinkAttribIterator it = links.begin(); it != links.end(); ++it)
	{
		int type = (*it)->GetLinkType();
		types.Append(1,&type);
	}
	return types;
}

TCHAR* 
FpSceneHierarchy::GetLinkType( INode* pNode )
{
	if(!pNode)
		return "None";

	TCHAR *retAlloc = new TCHAR[4096];
	TCHAR *ret = &retAlloc[0];
	_tcscpy(ret, "None");

	LinkAttribArray links = GetCustAttrContainer(eLinkType_ANY, pNode);
	for(LinkAttribIterator it = links.begin(); it != links.end(); ++it)
	{
		if(it==links.begin())
			_tcscpy(ret, GetChannelName((*it)->GetLinkType()));
		else
		{
			_tcscat(ret, ",");
			_tcscat(ret, GetChannelName((*it)->GetLinkType()));
		}
	}

	if(0==_tcscmp(ret, "None"))
		_tcscpy(ret, "|");
	else
		_tcscat(ret, "|");

	Tab<INode*> lcAdd;
	links.clear();
	::GetChildren(eLinkType_ANY, pNode, lcAdd, &links);
	for(LinkAttribIterator it = links.begin(); it != links.end(); ++it)
	{
		if(it!=links.begin())
			_tcscat(ret, ",");
		_tcscat(ret, GetChannelName((*it)->GetLinkType()));
	}

	if(0==_tcscmp(ret, "|"))
		_tcscpy(ret, "None");

	return retAlloc;
}

// HierarchyInterface.cpp
