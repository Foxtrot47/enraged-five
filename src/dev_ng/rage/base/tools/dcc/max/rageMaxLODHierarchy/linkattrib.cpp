#include <algorithm>
#include <iomanip>
#include <sstream>
#include <xlocale>

#include "linkattrib.h"
#include "rageMaxHierarchy.h"

#define LINKATTR_LINKTYPE_CHUNK 1000

LinkAttribDataClassDesc theLinkAttribDataDesc;
ClassDesc* GetLinkAttribDataClassDesc() {return &theLinkAttribDataDesc;}
IObjParam *LinkAttribData::iObjParams;

//////////////////////////////////////////////////////////////////////////////////////////////////////
int LinkAttribData::NumRefs()
{
	return 64;// limit statically to 64 references this can hold, although theorically dynamic
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle LinkAttribData::GetReference(int i)
{
	if(refNodes.size()<=i)
		return NULL;
	return (RefTargetHandle)refNodes[i];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void LinkAttribData::SetReference(int i, RefTargetHandle rtarg)
{
	if(refNodes.size()<=i)
		refNodes.resize(i+1, NULL);
	refNodes[i] = (INode *)rtarg;
}

bool LinkAttribData::HasReference(RefTargetHandle hTarget)
{
	std::vector<INode*>::iterator foundNodeIt = std::find(refNodes.begin(), refNodes.end(), hTarget);
	return (foundNodeIt!=refNodes.end());
}

void LinkAttribData::FindReferenceToDelete(RefTargetHandle hTarget)
{
	if(refNodes.size()<=0)
		return;

	for(int i=refNodes.size()-1;i>=0;i--)
	{
		INodePtr pNode = refNodes[i];
		if(pNode==hTarget)
		{
			DeleteReference(i);
			refNodes.erase(refNodes.begin() + i);
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
ReferenceTarget *LinkAttribData::Clone(RemapDir &remap)
{
	LinkAttribData* pCustAttrib = new LinkAttribData;
	LinkAttribData* oldData = dynamic_cast<LinkAttribData*>(&remap);
	if(oldData)
	{
		for(int i=0;i<oldData->NumRefs();i++)
			pCustAttrib->ReplaceReference(i, oldData->GetReference(i));
		pCustAttrib->SetLinkType(oldData->GetLinkType());
	}

	return pCustAttrib;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool LinkAttribData::CheckCopyAttribTo(ICustAttribContainer *to)
{
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
IOResult LinkAttribData::Load(ILoad *iload)
{
   ULONG nb;   
   IOResult res;   
   while (IO_OK==(res=iload->OpenChunk()))   
   {     
	   switch(iload->CurChunkID())     
	   {      
	   case LINKATTR_LINKTYPE_CHUNK:         
		   res=iload->Read(&mLinkType, sizeof(int), &nb);
		   break;      
	   }     
	   iload->CloseChunk();     
	   if (res!=IO_OK) 
		   return res;   
   }	
   return IO_OK;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
IOResult LinkAttribData::Save(ISave *isave)
{
   ULONG nb;   
   isave->BeginChunk(LINKATTR_LINKTYPE_CHUNK);   
   isave->Write(&mLinkType, sizeof(int), &nb);   
   isave->EndChunk();   
   return IO_OK;}

//////////////////////////////////////////////////////////////////////////////////////////////////////
RefResult LinkAttribData::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,PartID& partID,  RefMessage message)
{
	if((message == TARGETMSG_DELETING_NODE))
	{
		FindReferenceToDelete(hTarget);
	}

	return REF_SUCCEED;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
SvGraphNodeReference LinkAttribData::SvTraverseAnimGraph(IGraphObjectManager *gom, Animatable *owner, int id, DWORD flags)
{
	SvGraphNodeReference nodeRef = CustAttrib::SvTraverseAnimGraph(gom, owner, id, flags);
	if( nodeRef.stat == SVT_PROCEED ) {
		for( int i=0; i<NumLinkRefNodes(); i++ ) {
			gom->AddRelationship( nodeRef.gNode, GetReference(i), i, RELTYPE_CONSTRAINT );
		}
	}
	return nodeRef;
}