////
//// File::			LODDrawableLinkAttrib.cpp
//// Description::	RAGE Drawable LOD Custom Attribute Container
////
//// Author::			David Muir <david.muir@rockstarnorth.com>
//// Date::			12 March 2009
////
//
//// --- Include Files ------------------------------------------------------------
//
// Local headers
#include "LODDrawableLinkAttrib.h"
#include "Win32Res.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------
LodDrawableLinkAttribDataClassDesc theLodDrawableLinkAttribDataDesc;

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

ClassDesc* GetLodDrawableLinkAttribDataClassDesc( )
{
	return &theLodDrawableLinkAttribDataDesc;
}

LodDrawableLinkAttribData::LodDrawableLinkAttribData( ) 
	: m_hWnd( NULL )
	, m_invalid( false )
	, refNode( NULL ) 
{
}

void 
LodDrawableLinkAttribData::SetInvalid( )
{
	m_invalid = true;
}

bool 
LodDrawableLinkAttribData::IsInvalid( ) 
{
	return m_invalid;
}

bool 
LodDrawableLinkAttribData::CheckCopyAttribTo(ICustAttribContainer *to)
{
	return true;
}

IOResult 
LodDrawableLinkAttribData::Load(ILoad *iload)
{
	return IO_OK;
}

IOResult 
LodDrawableLinkAttribData::Save(ISave *isave)
{
	return IO_OK;
}

RefResult 
LodDrawableLinkAttribData::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,PartID& partID,  RefMessage message)
{
	if((message == TARGETMSG_DELETING_NODE) && (hTarget == refNode))
	{
		refNode = NULL;
	}

	return REF_SUCCEED;
}

ReferenceTarget* 
LodDrawableLinkAttribData::Clone( RemapDir &remap )
{
	LodDrawableLinkAttribData* pCustAttrib = new LodDrawableLinkAttribData;
	return pCustAttrib;
}

int 
LodDrawableLinkAttribData::NumRefs()
{
	return (1);
}

RefTargetHandle 
LodDrawableLinkAttribData::GetReference(int i)
{
	return refNode;
}

void 
LodDrawableLinkAttribData::SetReference(int i, RefTargetHandle rtarg)
{
	refNode = (INode *)rtarg;
}

SvGraphNodeReference 
LodDrawableLinkAttribData::SvTraverseAnimGraph(IGraphObjectManager *gom, Animatable *owner, int id, DWORD flags)
{
	int i;

	SvGraphNodeReference nodeRef;
	SvGraphNodeReference childNodeRef;
	gom->PushLevel(this);

	nodeRef = gom->AddAnimatable(this, owner, id, flags);
	if (nodeRef.stat == SVT_PROCEED) 
	{
		for (i = 0; i < NumSubs(); i++) 
		{
			if (SubAnim(i)) 
			{
				childNodeRef = SubAnim(i)->SvTraverseAnimGraph(gom, this, i, flags);
				if (childNodeRef.stat != SVT_DO_NOT_PROCEED)
					gom->AddReference(nodeRef.gNode, childNodeRef.gNode, REFTYPE_SUBANIM);
			}
		}
	}

	gom->PopLevel();

	return nodeRef;
}

// LODDrawableLinkAttrib.cpp

