#ifndef _LINKTYPE_H_
#define _LINKTYPE_H_

#include <vector>

enum /*eLinkType*/
{
	eLinkType_ANY = -1,
	eLinkType_LOD,
	eLinkType_DRAWLOD,
	eLinkType_RENDERSIM,
	eLinkType_CLOTHCOLLISION,
	eLinkType_ANIMBONE,
	eLinkType_TREELOD
};

typedef int eLinkType;

#endif _LINKTYPE_H_