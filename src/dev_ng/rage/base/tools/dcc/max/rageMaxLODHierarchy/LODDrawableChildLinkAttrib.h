#ifndef LODDRAWABLECHILDLINKATTRIB_H
#define LODDRAWABLECHILDLINKATTRIB_H

//
// File::			LODDrawableLinkAttrib.cpp
// Description::	RAGE Drawable LOD Custom Attribute Container
//
// Author::			David Muir <david.muir@rockstarnorth.com>
// Date::			12 March 2009
//

// --- Include Files ------------------------------------------------------------

// Max headers
 #include <Max.h>
 #include <iparamm2.h>
 #include <custattrib.h>
 #include <icustattribcontainer.h>
 #include "resource.h"
 
 // Local headers
 #include "Win32Res.h"
 #include "LODdrawableLinkAttrib.h"
 extern HINSTANCE g_hInstance;

// --- Defines ------------------------------------------------------------------
#define LODDRAWABLECHILDLINK_ATTR_NAME			( "LODDrawablChildAttributes" )
#define LODDRAWABLECHILDLINK_ATTR_CLASSID_PARTB 0xaec3fa5
#define LODDRAWABLECHILDLINK_ATTR_CLASSID		( Class_ID(0x34bca368, LODDRAWABLECHILDLINK_ATTR_CLASSID_PARTB) )

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

/**
* @brief RAGE Drawable Child LOD Attribute Container
*/
 class LodDrawableChildLinkAttribData : public CustAttrib
 {
 public:
 	LodDrawableChildLinkAttribData( );
 
 	void SetInvalid( );
 	bool IsInvalid( );
 
 	// from CustAttrib
 #if MAX_VERSION_MAJOR >= 12
 	const MCHAR* GetName() {return LODDRAWABLECHILDLINK_ATTR_NAME;}
 #else
 	TCHAR* GetName() {return LODDRAWABLECHILDLINK_ATTR_NAME;}
 #endif
 
 	bool CheckCopyAttribTo(ICustAttribContainer *to);
 	HWND GetRollupHWnd() {return m_hWnd;}
 
 	// File operations
 	IOResult Load(ILoad *iload);
 	IOResult Save(ISave *isave);
 
 	// from ReferenceMaker
 	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,PartID& partID,  RefMessage message);
 
 	SClass_ID SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
 	Class_ID ClassID() {return LODDRAWABLECHILDLINK_ATTR_CLASSID;}
 
 #if MAX_VERSION_MAJOR >= 9 
 	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
 #else
 	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
 #endif
 
 	void DeleteThis() { delete this;}
 	int NumRefs();
 	RefTargetHandle GetReference(int i);
 	void SetReference(int i, RefTargetHandle rtarg);
 
 	static IObjParam *iObjParams;
 	bool m_invalid;
 	HWND m_hWnd;
 
 	SvGraphNodeReference SvTraverseAnimGraph(IGraphObjectManager *gom, Animatable *owner, int id, DWORD flags);
 
 
 private:
 
 	INode *refNode;
 };
 
 /**
 * @brief
 */
 class LodChildLinkAttribDataClassDesc : public ClassDesc2 
 {
 public:
 	int 			IsPublic() {return 1;}
 	void *			Create(BOOL loading) { return new LodDrawableChildLinkAttribData; }
 	const TCHAR *	ClassName() { return GetStringResource(g_hInstance, IDS_LODDRAWABLECHILDATTRIBS_NAME); }
 	SClass_ID		SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
 	Class_ID 		ClassID() {return LODDRAWABLECHILDLINK_ATTR_CLASSID;}
 	const TCHAR* 	Category() {return _T("");}
 	const TCHAR*	InternalName() { return _T(LODDRAWABLECHILDLINK_ATTR_NAME); }	// returns fixed parsable name (scripter-visible name)
 };
 
 // --- Globals ------------------------------------------------------------------
 ClassDesc* GetLodChildLinkAttribDataClassDesc( );

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------


#endif // LODDRAWABLECHILDLINKATTRIB_H

// LODDrawableChildLinkAttrib.h
