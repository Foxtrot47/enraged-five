#ifndef INC_HIERARCHYINTERFACE_H_
#define INC_HIERARCHYINTERFACE_H_

// 3dsmax SDK headers
#include "max.h"
#include "ifnpub.h"
#include "LinkType.h"

#define FP_SCENEHIERARCHY_INTERFACE Interface_ID( 0x2fe142ae, 0x320b0edb )
#define FP_DRAWABLEHIERARCHY_INTERFACE Interface_ID( 0x3a1f14d6, 0x3a0e0802 )

#define GetSceneLinkInterface() \
	(IFpSceneHierarchy*)GetCOREInterface( FP_SCENEHIERARCHY_INTERFACE );
// #define GetDrawableHierarchyInterface() \
// 	(IFpDrawableHierarchy*)GetCOREInterface( FP_DRAWABLEHIERARCHY_INTERFACE );

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// Forward declare 3dsmax INode
class INode;

/**
 * Scene  hierarchy static interface.
 */
class IFpSceneHierarchy : public FPStaticInterface
{
public:
	enum 
	{ 
		em_getParent, 
		em_getParents, 
		em_getChildren,
		em_hasParent,
		em_hasChildren,
		em_RemoveParent,
		em_addContainer,
		em_removeContainer,
		em_hasContainer,
		em_setParent,
		em_ShowDialog,
		em_GetLinkType,
		em_SetChannelName,
		em_GetChannelName,
		em_GetChannel,
		em_GetNodeParentLinkTypes,
		em_GetNodeChildLinkTypes
	};

	virtual INode* GetParent( eLinkType type, INode* pNode ) = 0;
	virtual void GetParents( eLinkType type, INode* pNode, Tab<INode*>& llParents ) = 0;
	virtual void GetChildren( eLinkType type, INode* pNode, Tab<INode*>& llChildren ) = 0;
	
	virtual bool HasParent( eLinkType type, INode* pNode ) = 0;
	virtual bool HasChildren( eLinkType type, INode* pNode ) = 0;
	virtual bool RemoveParent( eLinkType type, INode* pNode, INode* pNodeParent ) = 0;

	virtual bool AddContainer( eLinkType type, INode* pNode ) = 0;
	virtual bool RemoveContainer( eLinkType type, INode* pNode ) = 0;
	virtual bool HasContainer( eLinkType type, INode* pNode ) = 0;

	virtual bool SetParent( eLinkType type, INode* pNode, INode* pParent, bool append ) = 0;
	virtual TCHAR* GetLinkType( INode* pNode ) = 0;

	virtual void SetChannelName( eLinkType type, TCHAR *name )=0;
	virtual TCHAR* GetChannelName( eLinkType type )=0;
	virtual int GetChannel( TCHAR* channelName )=0;

	virtual Tab<int> GetNodeParentLinkTypes(INode* pNode)=0;
	virtual Tab<int> GetNodeChildLinkTypes(INode* pNode)=0;

	virtual void ShowDialog( eLinkType type ) = 0;
};

#endif // INC_HIERARCHYINTERFACE_H_
