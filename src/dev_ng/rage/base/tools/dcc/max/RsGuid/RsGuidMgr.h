#ifndef INC_RSGUIDINTERFACE_H_
#define INC_RSGUIDINTERFACE_H_

// 3dsmax SDK headers
#include "max.h"
#include "ifnpub.h"
#include "RsGuid.h"

#define FP_RSGUID_INTERFACE Interface_ID( 0x1562e9ef, 0x5aa4a494 )

#define GetRsGuidInterface() \
	(IRsGuidMgrInterface*)GetCOREInterface( FP_RSGUID_INTERFACE );

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// Forward declare 3dsmax INode
class INode;

/**
 * Scene  hierarchy static interface.
 */
class IRsGuidMgrInterface : public FPStaticInterface
{
public:
	enum 
	{ 
		em_getGuid, 
		em_setGuid,
		em_hasGuid,
		em_resetGuid
	};

	virtual const TCHAR* GetGuid( RefTargetHandle rtarg ) = 0;
	virtual void SetGuid( RefTargetHandle rtarg, TCHAR* guid ) = 0;
	virtual BOOL HasGuid( RefTargetHandle rtarg ) = 0;
	virtual void ResetGuid( RefTargetHandle rtarg ) = 0;
};

#endif // INC_RSGUIDINTERFACE_H_
