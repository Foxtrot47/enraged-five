/**********************************************************************
 *<
	FILE: DllEntry.cpp

	DESCRIPTION:Contains the Dll Entry stuff

	CREATED BY: 

	HISTORY: 

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/
#include "resource.h"

// My includes
#include "max.h"
// STD includes
#include <sstream>

extern ClassDesc* GetRsGuidClassDesc();

extern TCHAR *GetString(int id);

HINSTANCE hInstance;
int controlsInit = FALSE;



BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	hInstance = hinstDLL;				// Hang on to this DLL's instance handle.

	if (!controlsInit) {
		controlsInit = TRUE;

#if MAX_VERSION_MAJOR < 11 
		InitCustomControls(hInstance);	// Initialize MAX's custom controls
#endif //MAX_VERSION_MAJOR

		InitCommonControls();			// Initialize Win95 controls

// 		// Initialize RAGE
// 		char* argv[1];
// 		argv[0] = "rageMaxDataStore.dlu";
// 		sysParam::Init( 1, argv );
// 
// 		// Initialize the RAGE parser
// 		INIT_PARSER;
	}
			
	return (TRUE);
}

__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_RSGUID_LIBDESCRIPTION);
}

//TODO: Must change this number when adding a new class
__declspec( dllexport ) int LibNumberClasses()
{
	return 1;
}

__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) {
		case 0: 
			return GetRsGuidClassDesc();
		default: 
			return 0;
	}
}

__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

__declspec( dllexport ) int LibInitialize( void )
{
	int nResult = 1; // Assume successful

	return nResult;
}

__declspec( dllexport ) int LibShutdown( void )
{	
	int nResult = 1; // Assume successful

	return nResult;
}

TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	return NULL;
}
