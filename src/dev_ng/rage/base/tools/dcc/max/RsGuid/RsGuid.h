//
//
//    Filename: RsGuid.h
//     Creator: Gunnar Droege
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#ifndef INC_RSGUID_H_
#define INC_RSGUID_H_

// Platform SDK headers
#pragma warning(push)
#pragma warning(disable:4668)
#include <Windows.h>
#pragma warning(pop)

// My headers
#include "resource.h"

// Max headers
#include <Max.h>
#include <iparamm2.h>
#include <custattrib.h>
#include <icustattribcontainer.h>
#include "resource.h"
#include <string>

using namespace rage;

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define RSGUID_NAME "RsGuid"
static const int sGuidStringLength = 40;

#define RSGUID_CLASSID		Class_ID(0x445d04a2, 0x3611277a)
class RsGuidAttribData : public CustAttrib
{

	friend class RsGuidMgr;

public:
	RsGuidAttribData() : m_hWnd(NULL), m_guid(GUID_NULL) {}

	// from CustAttrib
#if (MAX_RELEASE >= 12000)
	const MCHAR* GetName() {return &_T(RSGUID_NAME)[0];}
#else
	TCHAR* GetName() {return RSGUID_NAME;}
#endif
//	bool CheckCopyAttribTo(ICustAttribContainer *to);
	HWND GetRollupHWnd() {return m_hWnd;}
	static void RemoveRollup(HWND hWnd, Interface* ip);
	static void RemoveRollup(HWND hWnd, IMtlParams *iMtlPrms);

	// File operations
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);

	// from ReferenceMaker
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,	PartID& partID,  RefMessage message);

	void BeginEditParams(IObjParam *ip,ULONG flags,Animatable *prev);
	void EndEditParams(IObjParam *ip, ULONG flags, Animatable *next);
	void BeginEditParams(IMtlParams *iMtlPrms,ULONG flags,Animatable *prev);

	SClass_ID SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
	Class_ID ClassID() {return RSGUID_CLASSID;}

#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

	//	void BaseClone(ReferenceTarget* from,ReferenceTarget* to,RemapDir & remap);
	void DeleteThis() { delete this; }

	//////////////////////////////////////////////////////////////////////////
	// Custom code

	GUID GetGuid()
	{
		if(IsGuidInitialised())
			return m_guid;
		else
			return GUID_NULL;
	}
	void ResetGuid()
	{
		m_guid = GUID_NULL;
		CreateGuid();
	}

	BOOL GetGuidString(const TCHAR *guidString, int maxLength=sGuidStringLength)
	{
		OLECHAR guidWideString[sGuidStringLength]={0};
		int num = ::StringFromGUID2(m_guid, guidWideString, sGuidStringLength);
		int len;
		int slength = (int)wcslen(guidWideString) + 1;
		len = WideCharToMultiByte(CP_ACP, 0, guidWideString, slength, 0, 0, 0, 0);
		len = WideCharToMultiByte(CP_ACP, 0, guidWideString, slength, (LPSTR)guidString, len, 0, 0);
		if(len==0)
		{
			DWORD errorCode = GetLastError();
			mprintf("Couldn't resolve guid from wide string. Errorcode: %d\n", errorCode);
		}
		return len!=0;
	}

	void SetGuid(GUID g)
	{
		m_guid = g;
	}


	BOOL IsGuidInitialised()
	{
		return sizeof(m_guid)== sizeof(GUID);
	}
	void CreateGuid()
	{
		if(!IsGuidInitialised() || m_guid==GUID_NULL)
		{
			memset(&m_guid, 0, sizeof(GUID));
			CoCreateGuid(&m_guid);
		}
	}

private:
	GUID m_guid;
	HWND m_hWnd;
};


//
//
//
class RsGuidClassDesc : public ClassDesc2 {
public:
	int 			IsPublic() {return 1;}
	void *			Create(BOOL loading) {return new RsGuidAttribData;}
	const TCHAR *	ClassName() { return GetString(IDS_RSGUID_NAME); }
	SClass_ID		SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
	Class_ID 		ClassID() {return RSGUID_CLASSID;}
	const TCHAR* 	Category() {return _T("");}
	BOOL			NeedsToSave(){return TRUE;}
	const TCHAR*	InternalName() { return GetString(IDS_RSGUID_NAME); }	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance() { return hInstance; }			// returns owning module handle
};

#endif