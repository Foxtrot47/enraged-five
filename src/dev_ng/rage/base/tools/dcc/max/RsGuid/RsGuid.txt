Project RsGuid
Template max_configurations.xml

OutputFileName $(RS_TOOLSROOT)/dcc/current/max2012/plugins/x64/RsGuid.dlu
ConfigurationType dll

EmbeddedLibAll comctl32.lib core.lib geom.lib gfx.lib mesh.lib maxutil.lib maxscrpt.lib paramblk2.lib menus.lib gup.lib

ForceInclude ForceInclude.h

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\tools\libs\rageDataStoreAttributes
IncludePath $(RAGE_3RDPARTY)\dcc\Autodesk\3dsMax2012SDK\include

Files {
	Folder "Header Files" { 
		ForceInclude.h
		RsGuidMgr.h
	}
	Folder "Resource Files" {	
		RsGuid.rc
	}
	Folder "Source Files" {
		dllmain.cpp
		RsGuid.cpp
		RsGuidMgr.cpp
	}	
}