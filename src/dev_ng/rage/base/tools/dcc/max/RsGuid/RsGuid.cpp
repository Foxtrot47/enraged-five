// RsGuid.cpp : Defines the exported functions for the DLL application.
//

#include "RsGuid.h"

#define CUSTATTRIB_ATTR_CHUNK		0x4e52
#define CUSTATTRIB_ATTR_GUID_CHUNK	0x7829

RsGuidClassDesc theRsGuidAttribDataDesc;
ClassDesc* GetRsGuidClassDesc() {return &theRsGuidAttribDataDesc;}


//
//        name: AttributeDialogProc
// description: Dialog procedure for the attribute dialog
//
BOOL CALLBACK RollupDialogProc( HWND hwndDlg, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch(msg)
	{
	case WM_INITDIALOG:
// 		pDialog = (dmat::AttributeDialog *)lParam;
// 		pDialog->InitDialog(hwndDlg);
		SetWindowLongPtr(hwndDlg, GWLP_USERDATA, lParam);
		break;
	case WM_NOTIFY:
		break;

	case WM_COMMAND:
		// codes sent when focus is given to an edit box
		if(HIWORD(wParam) == 0x100)
			DisableAccelerators();
		// and when edit box loses focus
		else if(HIWORD(wParam) == 0x200)
			EnableAccelerators();
		/*pDialog = (dmat::AttributeDialog *)GetWindowLong(hwndDlg, GWL_USERDATA);
		if(pDialog)
			pDialog->SyncWithControls();*/
		break;

	case WM_CLOSE:
// 		pDialog = (dmat::AttributeDialog *)GetWindowLongPtr(hwndDlg, GWLP_USERDATA);
// 		if(pDialog)
// 			pDialog->SyncWithControls();
		break;

	}
	return FALSE;
}
 
RefResult RsGuidAttribData::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,	PartID& partID,  RefMessage message)
{
	

	return REF_SUCCEED;
}


//
//        name: RsGuidAttribData::Clone
// description: copy function
//
ReferenceTarget *RsGuidAttribData::Clone(RemapDir &remap)
{
	RsGuidAttribData* pCustAttrib = new RsGuidAttribData;

	pCustAttrib->SetGuid(m_guid);

	return pCustAttrib;
}

//
//        name: RsGuidAttribData::BeginEditParams
// description: create UI for editing
//
void RsGuidAttribData::BeginEditParams(IObjParam *ip,ULONG flags,Animatable *prev)
{
}

//
//        name: RsGuidAttribData::EndEditParams
// description: remove UI for editing
//
void RsGuidAttribData::EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)
{
	if(m_hWnd)
	{
		RemoveRollup(m_hWnd, ip);
		m_hWnd = NULL;
	}
}

//
//        name: RsGuidAttribData::RemoveRollup
// description: remove UI for editing
//
void RsGuidAttribData::RemoveRollup(HWND hWnd, Interface* ip)
{
}

//
//        name: RsGuidAttribData::BeginEditParams
// description: create UI for editing
//
void RsGuidAttribData::BeginEditParams(IMtlParams *iMtlPrms,ULONG flags,Animatable *prev)
{
}

//
//        name: RsGuidAttribData::RemoveRollup
// description: remove UI for editing
//
void RsGuidAttribData::RemoveRollup(HWND hWnd, IMtlParams *iMtlPrms)
{
}

//
//        name: RsGuidAttribData::Load
// description: load attribute data
//
IOResult RsGuidAttribData::Load(ILoad *iload)
{
	// load slot queue
	IOResult res;
	unsigned long length;

	while (IO_OK == (res = iload->OpenChunk())) {
		switch(iload->CurChunkID())  
		{
		case CUSTATTRIB_ATTR_GUID_CHUNK:
			{
				s32 size = (s32)iload->CurChunkLength();
				void *guidBuffer = new byte[size+1];
				res = iload->Read(guidBuffer, size, (ULONG*)&length);
				if(res!=IO_OK)
					mprintf(_T("Failed to read in GUID for Attribute Datainstance."));
				else
					SetGuid(*(GUID *)guidBuffer);
			}
			break;
		default:
			break;
		}
		iload->CloseChunk();
		if (res != IO_OK) 
			return res;
	}
	return IO_OK;
}

//
//        name: RsGuidAttribData::Save
// description: save attribute data
//
IOResult RsGuidAttribData::Save(ISave *isave)
{
	s32 size;
	unsigned long length;

	isave->BeginChunk(CUSTATTRIB_ATTR_GUID_CHUNK);
	isave->Write(&GetGuid(), sizeof(GUID), (ULONG*)&length);
	isave->EndChunk();
	
	return IO_OK;
}


