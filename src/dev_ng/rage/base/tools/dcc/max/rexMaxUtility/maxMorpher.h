//maxMorpher.h

//-----------------------------------------------------------------------------

//This is a stripped down copy of the code from "maxsdk\samples\modifiers\morpher\wm3.h"
//Autodesk doesn't include the interface to access the morph modifer as part of the code
//SDK headers, but rather in the sample code, so this was moved here locally and stripped down
//to eliminate the need to create another library for interfacing with the modifier.

//-----------------------------------------------------------------------------

#ifndef __MAX_MORPHER_H__
#define __MAX_MORPHER_H__

#define MR3_CLASS_ID		Class_ID(0x17bb6854, 0xa5cba2a3)
#define MR3_NUM_CHANNELS	100
#define MAX_TARGS			25

#define MR3_MORPHERVERSION	010

#define DLLImport	__declspec( dllimport )
#define MorphExport __declspec( dllexport )

class MorphR3;
class morphChannel;

static Point3 junkpoint(0,0,0);


//-----------------------------------------------------------------------------
// Unfortunatly we need to create a small shim class to use in place of the
// std::vector declarations that were in the original wm3.h sample header. This is needed
// because RexMAX is built using STLPort, and the MAX morpher plugin is built using
// a different STL version, so the vector memory layouts don't match up when accessing
// the public vector members of the classes implemented by the Morhpher
#pragma pack(push,_CRT_PACKING)

template<class T>
class MsftSTLVector
{
public:
	void*	allocPlaceHolder;
	T*		_First;
	T*		_Last;
	T*		_End;

public:
	int size()
	{
		return (_First == 0 ? 0 : _Last - _First);
	}

	T& operator[](int _Pos)
	{	
		return (*(_First + _Pos));
	}
};

#pragma pack(pop)

//-----------------------------------------------------------------------------

class TargetCache
{
public:
	long mNumPoints;
	INode *mTargetINode;
	MsftSTLVector<Point3> mTargetPoints;
	float mTargetPercent;

	TargetCache(){
		mNumPoints=0;
		mTargetINode=NULL;
		mTargetPercent = 0.0f;
	}

	TargetCache(const TargetCache &tcache);

	void operator=(const TargetCache &tcache); 

	void operator=(const morphChannel &mchan);
	void Init( INode *nd);
	void Reset(void);
	

	IOResult Save(ISave* isave);
	IOResult Load(ILoad* iload);
	INode *RefNode(void) { return mTargetINode; }
	
};

//-----------------------------------------------------------------------------

class morphChannel
{
public:

	// Construct/Destruct
	~morphChannel();
	morphChannel();
	morphChannel(const morphChannel & from);

	MorphR3		*mp;

	float mCurvature;

	// Number of points
	int			mNumPoints;

	int iTargetListSelection;

	// Actual morphable points
	MsftSTLVector<Point3>		mPoints;
	MsftSTLVector<Point3>		mDeltas;
	MsftSTLVector<double>		mWeights;
	
	MsftSTLVector<TargetCache>	mTargetCache;

    // BitArray to check against for point selection
	BitArray	mSel;
	
	// INode that we use to update and reload from, if possible
	INode*		mConnection;
	
	// Name for the morph channel
	TSTR		mName;
	//
	int mNumProgressiveTargs;
	float mTargetPercent;

	// Various, non-animatable stuff
	// mActive is TRUE if the channel has data in it in some form
	// mModded is TRUE if the channel has been changed in SOME form,
	//		ie, had its name changed or similar
	BOOL		mActive, mModded, mUseLimit, mUseSel;
	float		mSpinmin,mSpinmax;

	// TRUE if the channel has been marked as bad. It will not be 
	// considered when building the morph results.
	BOOL		mInvalid;

	// Channel enabled/disabled
	BOOL		mActiveOverride;

	void InitTargetCache(const int &targnum, INode *nd){ mTargetCache[targnum].Init( nd); }

	// paramblock for the morph channels values
	IParamBlock* cblock;
	
	// Delete and reset channel
	MorphExport void ResetMe();


	MorphExport void AllocBuffers( int sizeA, int sizeB );

	// Do some rough calculations about how much space this channel
	// takes up
	// This isn't meant to be fast or terribly accurate!
	MorphExport float getMemSize();


	// The rebuildChannel call will recalculate the optimization data
	// and refill the mSel selection array. This will be called each time a
	// targeted node is changed, or any of the 'Update Target' buttons is
	// pressed on the UI
	MorphExport void rebuildChannel();

	// Initialize a channel using a scene node
	MorphExport void buildFromNode( INode *node , BOOL resetTime=TRUE , TimeValue t=0, BOOL picked = FALSE );

	// Transfer data to another channel
	MorphExport void operator=(const morphChannel& from);
	void operator=(const TargetCache& tcache);

	// Load/Save channel to stream
	MorphExport IOResult Save(ISave* isave);
	MorphExport IOResult Load(ILoad* iload);
	void SetUpNewController();
	int  NumProgressiveTargets(void) { return mNumProgressiveTargs; }
	void ResetRefs(MorphR3 *, const int&);
	float GetTargetPercent(const int &which);
	void ReNormalize();
	void CopyTargetPercents(const morphChannel &chan);
};

//-----------------------------------------------------------------------------

class morphCache
{

public:

	BOOL CacheValid;

	Point3*		oPoints;
	double*		oWeights;
	BitArray	sel;

	int		Count;

	morphCache ();
	~morphCache () { NukeCache(); }

	MorphExport void MakeCache(Object *obj);
	MorphExport void NukeCache();

	MorphExport BOOL AreWeCached();
};

//-----------------------------------------------------------------------------

class MorphR3 : public Modifier, TimeChangeCallback {
	public:

		float mFileVersion;

		// Access to the interface
		static IObjParam *ip;
		
		// Pointer to the morph channels
		MsftSTLVector<morphChannel>	chanBank;

		// Pointer to the morph material bound to this morpher
		void *morphmaterial;
		
		// Currently selected channel (0-9)
		int					chanSel;
		
		// Currently viewable channel banks (0-99)
		int					chanNum;

		// Spinners from main page
		ISpinnerControl		*chanSpins[10];

		// Spinners from global settings page
		ISpinnerControl		*glSpinmin,*glSpinmax;

		// Spinners from the channel params dlg
		ISpinnerControl		*cSpinmin,*cSpinmax,*cCurvature,*cTargetPercent;

		// Global parameter block
		IParamBlock			*pblock;

		// The window handles for the 4 rollout pages
		HWND hwGlobalParams, hwChannelList,	hwChannelParams, hwAdvanced, hwLegend;
		static HWND hMaxWnd;

		// For the namer dialog
		ICustEdit			*newname;

		// Morph Cache
		morphCache MC_Local;

		BOOL tccI;
		char trimD[50];

		// 'Save as Current' support
		BOOL recordModifications;
		int recordTarget;


		// Marker support
		Tab<int>			markerIndex;
		NameTab				markerName;
		int					markerSel;


		// Channel operation flag for dialog use
		int					cOp;
		int					srcIdx;

		// storage variable for states between dialog procs
		bool hackUI;


		//Constructor/Destructor
		MorphR3();
		~MorphR3();
		
		// Handles the scroll bar on the channel list UI
		MorphExport void VScroll(int code, short int cpos );
		
		// Clamps channel number to valid range
		MorphExport void Clamp_chanNum();

		
		MorphExport void ChannelOp(int src, int targ, int flags);


		MorphExport void Update_globalParams();
		MorphExport void Update_advancedParams();	
		MorphExport void Update_channelParams();


		// evaluate the value increments setting
		MorphExport float GetIncrements();
		// SetScale on the channel list spinners
		MorphExport void Update_SpinnerIncrements();

		// Functions to update the channel list dialog box:
		MorphExport void Update_colorIndicators();
		MorphExport void Update_channelNames();
		MorphExport void Update_channelValues();
		MorphExport void Update_channelLimits();
		MorphExport void Update_channelInfo();
		MorphExport void Update_channelMarkers();
		// Seperated cause this function is pretty expensive
		// Lots done, complete update - calls all functions above
		MorphExport void Update_channelFULL();
		
		float GetCurrentTargetPercent(void); 
		void SetCurrentTargetPercent(const float &fval);
	
		void DeleteTarget(void);
		void Update_TargetListBoxNames(void);
		void SwapTargets(const int way);
		void SwapTargets(const int from, const int to, const bool isundo);

		int GetRefNumber(int chanNum, int targNum) { return (200 + targNum + (chanNum * MAX_TARGS)); }
		void DisplayMemoryUsage(void );

};

//-----------------------------------------------------------------------------

#endif //__MAX_MORPHER_H__

