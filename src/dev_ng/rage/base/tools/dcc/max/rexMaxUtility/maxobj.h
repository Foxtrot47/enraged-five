#ifndef __REX_MAX_MAXOBJ_H__
#define __REX_MAX_MAXOBJ_H__

///////////////////////////////////////////////////////////////////////////////////////
//contains the class ids for some user defined max objects. its done like this to
//reduce dependencies with the original dll's
///////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////

#define MR3_CLASS_ID Class_ID(0x17bb6854, 0xa5cba2a3)
#define MR3_FIRSTTARGET		101
#define MR3_LASTTARGET		200

///////////////////////////////////////////////////////////////////////////////////////

#define COLLISION_MESH_CLASS_ID		Class_ID(0x5d2707b2,0xb5c5d30)

#define COLMESH_REF_TRIOBJ			0

///////////////////////////////////////////////////////////////////////////////////////

#define COLLISION_BOX_CLASS_ID      Class_ID(0x7fca381e,0x47a93bdf)

//paramblock entries

enum
{
	rsobjpb_colbox_length,
	rsobjpb_colbox_width,
	rsobjpb_colbox_height
};

///////////////////////////////////////////////////////////////////////////////////////

#define COLLISION_SPHERE_CLASS_ID   Class_ID(0x9e12205,0x62dc4b42)

//paramblock entries

enum
{
	rsobjpb_colsphere_radius, 
	rsobjpb_colsphere_type 
};

///////////////////////////////////////////////////////////////////////////////////////

#define COLLISION_CAPSULE_CLASS_ID	Class_ID(0x5745733c, 0x7f435158)

enum
{
	rsobjpb_colcapsule_radius,
	rsobjpb_colcapsule_length
};

///////////////////////////////////////////////////////////////////////////////////////

#define COLLISION_CYLINDER_CLASS_ID	Class_ID(0x695d2eb9, 0x77fb6428)

enum
{
	rsobjpb_colcylinder_radius,
	rsobjpb_colcylinder_length,
	rsobjpb_colcylinder_capcurve,
	rsobjpb_colcylinder_sidecurve
};

//////////////////////////////////////////////////////////////////////////

#define COLLISION_DISC_CLASS_ID		Class_ID(0x1c9774c7, 0xa34be14)

// Paramblock2 parameter list
enum 
{ 
	rsobjpb_coldisc_radius, 
	rsobjpb_coldisc_length
};

/////////////////////////////////////////////////////////////////////////////////////////////////

#define CONSTRAINT_CLASS_ID	Class_ID(0x350739c4, 0x51515b0e)

enum 
{ 
	constraint_xmin, 
	constraint_xmax,
	constraint_ymin, 
	constraint_ymax,
	constraint_zmin, 
	constraint_zmax,
	constraint_pick,
};	

//---------------------------------------------------------------------------
// Simple Constraint Helpers
// These simple constraint helpers are defined in the startup MaxScript
// files RsRotationConstraint.ms and RsTranslationConstraint.ms.  They are
// both define single axis constraints.
//---------------------------------------------------------------------------

#define ROTATION_CONSTRAINT_CLASS_ID ( Class_ID(0x2ea24371, 0x65810cc5) )
enum
{
	rotation_constraint_axis,
	rotation_constraint_min,
	rotation_constraint_max
};

#define TRANSLATION_CONSTRAINT_CLASS_ID ( Class_ID(0x5a11807, 0x303613b6) )
enum
{
	translation_constraint_axis,
	translation_constraint_min,
	translation_constraint_max
};

// Simple Constraint Helper axis enumeration for axis parameter.
enum
{
	constraint_axis_x,
	constraint_axis_y,
	constraint_axis_z
};

#endif //__REX_MAX_MAXOBJ_H__
