#include "file/stream.h"
#include "rexBase/utility.h"
#include "rexMaxUtility/defshader.h"
#include "rexMaxUtility/utility.h"
#include "rstMaxMtl/rstMax.h"

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

using namespace rage;

static const char g_ConfigFileName[] = "rst_defaultshader.xml";

/*

rst_defaultshader.xml should be of this form and places in your 3dsmax/plugcfg directory

<?xml version = "1.0" encoding = "utf8"?>

<rst_defshaders>

	<shader alpha="0" normal="0" reflect="0" specular="0" shadername="gta_default"/>
	<shader alpha="1" normal="0" reflect="0" specular="0" shadername="gta_alpha"/>
	<shader alpha="0" normal="1" reflect="0" specular="0" shadername="gta_normal"/>
	<shader alpha="1" normal="1" reflect="0" specular="0" shadername="gta_normal_alpha"/>
	<shader alpha="0" normal="1" reflect="1" specular="0" shadername="gta_normal_reflect"/>
	<shader alpha="1" normal="1" reflect="1" specular="0" shadername="gta_normal_reflect_alpha"/>
	<shader alpha="0" normal="1" reflect="0" specular="1" shadername="gta_normal_spec"/>
	<shader alpha="1" normal="1" reflect="0" specular="1" shadername="gta_normal_spec_alpha"/>
	<shader alpha="0" normal="1" reflect="1" specular="1" shadername="gta_normal_spec_reflect"/>
	<shader alpha="1" normal="1" reflect="1" specular="1" shadername="gta_normal_spec_reflect_alpha"/>
	<shader alpha="0" normal="0" reflect="1" specular="0" shadername="gta_reflect"/>
	<shader alpha="1" normal="0" reflect="1" specular="0" shadername="gta_reflect_alpha"/>
	<shader alpha="0" normal="0" reflect="0" specular="1" shadername="gta_spec"/>
	<shader alpha="1" normal="0" reflect="0" specular="1" shadername="gta_spec_alpha"/>
	<shader alpha="0" normal="0" reflect="1" specular="1" shadername="gta_spec_reflect"/>
	<shader alpha="1" normal="0" reflect="1" specular="1" shadername="gta_spec_reflect_alpha"/>

</rst_defshaders>

*/

#define MAX_ATTR_LEN 512

/////////////////////////////////////////////////////////////////////////////////////////////////////
DefShader& DefShader::GetInst()
{
	static DefShader inst;

	return inst;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

DefShader::DefShader()
{
	//fill in generic shaders...

	//load in the configuration xml file

	atString ConfigFile;

	const TCHAR* p_PlugCfgDir = GetCOREInterface()->GetDir(APP_PLUGCFG_DIR);

	ConfigFile += p_PlugCfgDir;
	ConfigFile += "\\";
	ConfigFile += g_ConfigFileName;

	xmlDocPtr m_pXmlDoc = xmlParseFile( ConfigFile.c_str() );
	xmlXPathContextPtr m_pXPathCtx = xmlXPathNewContext( m_pXmlDoc );

	if ( m_pXmlDoc == NULL )
	{
		xmlFreeDoc( m_pXmlDoc );
		return;
	}
	if ( m_pXPathCtx == NULL )
	{
		xmlXPathFreeContext( m_pXPathCtx );
		return;
	}

	xmlXPathObjectPtr pXPathObj = xmlXPathEvalExpression( (xmlChar*) "//rst_defshaders/shader", m_pXPathCtx );
	#define XML_BUFFER_SIZE 512
	TCHAR xmlBuffer[XML_BUFFER_SIZE] = {0};
	if ( pXPathObj )
	{
		switch ( pXPathObj->type )
		{
		case XPATH_NODESET:
			{
				if ( !pXPathObj->nodesetval )
					return; // "User error: XPath expression invalid."

				size_t count = pXPathObj->nodesetval->nodeNr;
				for ( size_t n = 0; n < count; ++n )
				{
					//<shader alpha="0" normal="0" reflect="1" specular="1" dec_ambient="0" shadername="spec_reflect.sps"/>
					xmlNodePtr pNode = pXPathObj->nodesetval->nodeTab[n];
					const char* alphaAttribute = (const char*) xmlGetProp(pNode, (xmlChar*) "alpha");
					const char* normalAttribute = (const char*) xmlGetProp(pNode, (xmlChar*) "normal");
					const char* reflectAttribute = (const char*) xmlGetProp(pNode, (xmlChar*) "reflect");
					const char* specularAttribute = (const char*) xmlGetProp(pNode, (xmlChar*) "specular");
					const char* decAmbientAttribute = (const char*) xmlGetProp(pNode, (xmlChar*) "dec_ambient");
					const char* shaderNameAttribute = (const char*) xmlGetProp(pNode, (xmlChar*) "shadername");

					if ( alphaAttribute == NULL || normalAttribute == NULL || reflectAttribute == NULL 
						|| specularAttribute == NULL || decAmbientAttribute == NULL || shaderNameAttribute == NULL )
						continue;

					bool Alpha,Normal,Reflect,Specular,DecAmbient;

					Alpha = atoi(alphaAttribute) == 1 ? true : false;
					Normal = atoi(normalAttribute) == 1 ? true : false;
					Reflect = atoi(reflectAttribute) == 1 ? true : false;
					Specular = atoi(specularAttribute) == 1 ? true : false;
					DecAmbient = atoi(decAmbientAttribute) == 1 ? true : false;

					s32 Type = GetShaderType(Alpha,Normal,Reflect,Specular,DecAmbient);

					atString* p_Data = m_ShaderList.Access(Type);

					if(p_Data)
					{
						*p_Data = shaderNameAttribute;
					}
					else
					{
						m_ShaderList.Insert(Type,atString(shaderNameAttribute));
					}
				}
				
			}
			break;

		default:
			break; // "Internal error: XPath query return type not implemented." 
		}
	}
	xmlXPathFreeObject( pXPathObj );
	xmlXPathFreeContext( m_pXPathCtx );
	xmlFreeDoc( m_pXmlDoc );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
s32 DefShader::GetShaderType(bool Alpha,bool Normal,bool Reflect,bool Specular,bool DecAmbient)
{
	s32 Type = 0;

	if(Alpha)
	{
		Type += ALPHA;
	}

	if(Normal)
	{
		Type += NORMAL;
	}

	if(Reflect)
	{
		Type += REFLECT;
	}

	if(Specular)
	{
		Type += SPEC;
	}

	if(DecAmbient)
	{
		Type += DECAMBIENT;
	}

	return Type;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool DefShader::GetMaterial(StdMat* p_StdMtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo,bool& r_IsTwoSided,bool ForceAlphaShader)
{
	bool Alpha,Normal,Reflect,Specular,DecAmbient;

	r_IsTwoSided = p_StdMtl->GetTwoSided() == TRUE ? true : false;
	GetShaderFlags(p_StdMtl,Alpha,Normal,Reflect,Specular,DecAmbient);

	if(ForceAlphaShader && !Alpha)
		Alpha = true;

	s32 ShaderType = GetShaderType(Alpha,Normal,Reflect,Specular,DecAmbient);
	atString* p_ShaderName = m_ShaderList.Access(ShaderType);

	if(!p_ShaderName)
	{
		return GetMaterialStandard(p_StdMtl,r_MatInfo,r_IsTwoSided);
	}

	r_MatInfo.m_TypeName = *p_ShaderName;
	r_MatInfo.m_RageTypes.Reset();
	r_MatInfo.m_AttributeNames.Reset();
	r_MatInfo.m_AttributeValues.Reset();

	if(DecAmbient)
	{
		FillDecAmbient(p_StdMtl,r_MatInfo);
		return true;
	}
	else
	{
		FillDiffuse(p_StdMtl,r_MatInfo);
	}

	if(Normal)
	{
		FillNormal(p_StdMtl,r_MatInfo);
	}

	if(Reflect)
	{
		FillReflect(p_StdMtl,r_MatInfo);
	}

	if(Specular)
	{
		FillSpecular(p_StdMtl,r_MatInfo);
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool DefShader::GetMaterial( StdMat* p_Mtl, RstMtlMax& r_rstMat, bool bForceAlphaShader )
{
	bool Alpha,Normal,Reflect,Specular,DecAmbient;

	bool bIsTwoSided = p_Mtl->GetTwoSided() == TRUE ? true : false;
	GetShaderFlags( p_Mtl, Alpha, Normal, Reflect, Specular, DecAmbient );

	if( bForceAlphaShader && !Alpha )
		Alpha = true;

	s32 ShaderType = GetShaderType( Alpha, Normal, Reflect, Specular, DecAmbient );
	atString* p_ShaderName = m_ShaderList.Access( ShaderType );

	if( !p_ShaderName )
	{
		return GetMaterialStandard( p_Mtl, r_rstMat, bIsTwoSided );
	}

	r_rstMat.SetShaderName( *p_ShaderName );

	bool bHasValidChannel = false;
	if( DecAmbient )
	{
		bHasValidChannel = FillTextureChannel( p_Mtl, r_rstMat, ID_AM, 0 );
		return bHasValidChannel;
	}
	else
	{
		bHasValidChannel |= FillTextureChannel( p_Mtl, r_rstMat, ID_DI, 0 );
		bHasValidChannel |= FillTextureChannel( p_Mtl, r_rstMat, ID_OP, 1 );
	}

	if( Normal )
		bHasValidChannel |=FillTextureChannel( p_Mtl, r_rstMat, ID_BU, 1 );

	if( Reflect )
		bHasValidChannel |= FillTextureChannel( p_Mtl, r_rstMat, ID_RL, 1 );

	if( Specular )
		bHasValidChannel |= FillTextureChannel( p_Mtl, r_rstMat, ID_SP, 1 );

	return bHasValidChannel;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool DefShader::FillTextureChannel( StdMat* p_StdMtl, RstMtlMax& r_rstMat, int stdTexChannel, int rageTexChannel )
{
	BitmapTex* p_Tex = NULL;

	if(p_StdMtl->SubTexmapOn(stdTexChannel))
	{
		Texmap* p_TexMap = p_StdMtl->GetSubTexmap(stdTexChannel);

		p_Tex = GetIBitmapTextInterface(p_TexMap);
	}

	if(p_Tex)
	{
		r_rstMat.SetSubTexmap(rageTexChannel, p_Tex);
		return true;
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void DefShader::FillDecAmbient(StdMat* p_StdMtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo)
{
	BitmapTex* p_DiffuseTex = NULL;

	if(p_StdMtl->SubTexmapOn(ID_AM))
	{
		Texmap* p_TexMap = p_StdMtl->GetSubTexmap(ID_AM);

		p_DiffuseTex = GetIBitmapTextInterface(p_TexMap);

		atString DiffuseTex = rexMaxUtility::GetTexMapFullPath(p_DiffuseTex);

		if(DiffuseTex == "")
		{
			p_DiffuseTex = NULL;
		}
	}

	if(p_DiffuseTex)
	{
		r_MatInfo.m_InputTextureFileNames.PushAndGrow(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex),1);

		atString t(rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex))));
		r_MatInfo.m_AttributeValues.PushAndGrow(t,1);

		t += ".dds";
		r_MatInfo.m_OutputTextureFileNames.PushAndGrow(t,1);
		r_MatInfo.m_RageTypes.PushAndGrow(atString("grcTexture"),1);
		r_MatInfo.m_AttributeNames.PushAndGrow(atString("DiffuseTex"),1);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void DefShader::FillDiffuse(StdMat* p_StdMtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo)
{
	BitmapTex* p_DiffuseTex = NULL;
	BitmapTex* p_OpacityTex = NULL;

	if(p_StdMtl->SubTexmapOn(ID_DI))
	{
		Texmap* p_TexMap = p_StdMtl->GetSubTexmap(ID_DI);

		p_DiffuseTex = GetIBitmapTextInterface(p_TexMap);

		atString DiffuseTex = rexMaxUtility::GetTexMapFullPath(p_DiffuseTex);

		if(DiffuseTex == "")
		{
			p_DiffuseTex = NULL;
		}

		if(p_StdMtl->SubTexmapOn(ID_OP))
		{
			Texmap* p_TexMap = p_StdMtl->GetSubTexmap(ID_OP);

			p_OpacityTex = GetIBitmapTextInterface(p_TexMap);

			atString OpacityTex = rexMaxUtility::GetTexMapFullPath(p_OpacityTex);

			if(OpacityTex == "")
			{
				p_OpacityTex = NULL;
			}
		}
	}

	if(p_DiffuseTex)
	{
		if(p_OpacityTex)
		{
			atString combined(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex));
			combined += "+";  // indicate to lower-level code that we need to combine two textures
			combined += rexMaxUtility::GetTexMapFullPath(p_OpacityTex);
			r_MatInfo.m_InputTextureFileNames.PushAndGrow(combined,1);

			atString t(rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex))));
			t += (rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(rexMaxUtility::GetTexMapFullPath(p_OpacityTex))));
			r_MatInfo.m_AttributeValues.PushAndGrow(t,1);

			t += ".dds";
			r_MatInfo.m_OutputTextureFileNames.PushAndGrow(t,1);
		}
		else
		{
			r_MatInfo.m_InputTextureFileNames.PushAndGrow(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex),1);

			atString t(rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex))));
			r_MatInfo.m_AttributeValues.PushAndGrow(t,1);

			t += ".dds";
			r_MatInfo.m_OutputTextureFileNames.PushAndGrow(t,1);
		}

		r_MatInfo.m_RageTypes.PushAndGrow(atString("grcTexture"),1);
		r_MatInfo.m_AttributeNames.PushAndGrow(atString("DiffuseTex"),1);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void DefShader::FillNormal(StdMat* p_StdMtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo)
{
	BitmapTex* p_BumpTex = NULL;

	if(p_StdMtl->SubTexmapOn(ID_BU))
	{
		Texmap* p_TexMap = p_StdMtl->GetSubTexmap(ID_BU);

		p_BumpTex = GetIBitmapTextInterface(p_TexMap);

		atString BumpTex = rexMaxUtility::GetTexMapFullPath(p_BumpTex);

		if(BumpTex == "")
		{
			p_BumpTex = NULL;
		}
	}

	if(p_BumpTex)
	{
		atString t(rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(rexMaxUtility::GetTexMapFullPath(p_BumpTex))));

		r_MatInfo.m_InputTextureFileNames.PushAndGrow(rexMaxUtility::GetTexMapFullPath(p_BumpTex),1);

		r_MatInfo.m_RageTypes.PushAndGrow(atString("grcTexture"),1);
		r_MatInfo.m_AttributeNames.PushAndGrow(atString("BumpTex"),1);
		r_MatInfo.m_AttributeValues.PushAndGrow(t,1);

		t += ".dds";
		r_MatInfo.m_OutputTextureFileNames.PushAndGrow(t,1);
	}

	rexValue valf(p_StdMtl->GetTexmapAmt(ID_BU,0) * 2.0f);

	r_MatInfo.m_RageTypes.PushAndGrow(atString("float"),1);
	r_MatInfo.m_AttributeNames.PushAndGrow(atString("Bumpiness"),1);
	r_MatInfo.m_AttributeValues.PushAndGrow(valf,1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void DefShader::FillReflect(StdMat* p_StdMtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo)
{
	BitmapTex* p_ReflectionTex = NULL;

	if(p_StdMtl->SubTexmapOn(ID_RL))
	{
		Texmap* p_TexMap = p_StdMtl->GetSubTexmap(ID_RL);

		p_ReflectionTex = GetIBitmapTextInterface(p_TexMap);

		atString RefTex = rexMaxUtility::GetTexMapFullPath(p_ReflectionTex);

		if(RefTex == "")
		{
			p_ReflectionTex = NULL;
		}
	}

	if(p_ReflectionTex)
	{
		atString t(rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(rexMaxUtility::GetTexMapFullPath(p_ReflectionTex))));

		r_MatInfo.m_InputTextureFileNames.PushAndGrow(rexMaxUtility::GetTexMapFullPath(p_ReflectionTex),1);

		r_MatInfo.m_RageTypes.PushAndGrow(atString("grcTexture"),1);
		r_MatInfo.m_AttributeNames.PushAndGrow(atString("EnvironmentTex"),1);
		r_MatInfo.m_AttributeValues.PushAndGrow(t,1);

		t += ".dds";
		r_MatInfo.m_OutputTextureFileNames.PushAndGrow(t,1);
	}

	//"Reflectivity"

	rexValue valf(p_StdMtl->GetTexmapAmt(ID_RL,0));

	r_MatInfo.m_RageTypes.PushAndGrow(atString("float"),1);
	r_MatInfo.m_AttributeNames.PushAndGrow(atString("Reflectivity"),1);
	r_MatInfo.m_AttributeValues.PushAndGrow(valf,1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void DefShader::FillSpecular(StdMat* p_StdMtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo)
{
	BitmapTex* p_SpecularTex = NULL;

	if(p_StdMtl->SubTexmapOn(ID_SP))
	{
		Texmap* p_TexMap = p_StdMtl->GetSubTexmap(ID_SP);

		p_SpecularTex = GetIBitmapTextInterface(p_TexMap);

		atString SpecTex = rexMaxUtility::GetTexMapFullPath(p_SpecularTex);

		if(SpecTex == "")
		{
			p_SpecularTex = NULL;
		}
	}

	if(p_SpecularTex)
	{
		atString t(rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(rexMaxUtility::GetTexMapFullPath(p_SpecularTex))));

		r_MatInfo.m_InputTextureFileNames.PushAndGrow(rexMaxUtility::GetTexMapFullPath(p_SpecularTex),1);

		r_MatInfo.m_RageTypes.PushAndGrow(atString("grcTexture"),1);
		r_MatInfo.m_AttributeNames.PushAndGrow(atString("SpecularTex"),1);
		r_MatInfo.m_AttributeValues.PushAndGrow(t,1);

		t += ".dds";
		r_MatInfo.m_OutputTextureFileNames.PushAndGrow(t,1);
	}

	//"Specular Falloff" (taken from "glossiness" of standard material)

	float Glossiness = p_StdMtl->GetShininess(0); // 0 to 1

	if(Glossiness == 0.0f)
	{
		 Glossiness = 0.01f;
	}

	Glossiness = 1.0f / Glossiness;
	Glossiness *= 100.0f;

	rexValue vala(Glossiness);

	r_MatInfo.m_RageTypes.PushAndGrow(atString("float"),1);
	r_MatInfo.m_AttributeNames.PushAndGrow(atString("Specular"),1);
	r_MatInfo.m_AttributeValues.PushAndGrow(vala,1);

	//"Specular Intensity" (taken from "specular level" of standard material)

	float SpecularLevel = p_StdMtl->GetShinStr(0); // 0 to 10
	SpecularLevel *= 100.0f;

	rexValue valb(SpecularLevel);

	r_MatInfo.m_RageTypes.PushAndGrow(atString("float"),1);
	r_MatInfo.m_AttributeNames.PushAndGrow(atString("SpecularColor"),1);
	r_MatInfo.m_AttributeValues.PushAndGrow(valb,1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool DefShader::GetMaterialStandard(StdMat* p_StdMtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo,bool& r_IsTwoSided)
{
	BitmapTex* p_DiffuseTex = NULL;
	BitmapTex* p_OpacityTex = NULL;

	if(p_StdMtl->SubTexmapOn(ID_DI))
	{
		Texmap* p_TexMap = p_StdMtl->GetSubTexmap(ID_DI);

		p_DiffuseTex = GetIBitmapTextInterface(p_TexMap);
#if HACK_GTA4
		// Set the p_DiffuseTex to NULL if the tex map name cant be resolved 
		if(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex) == "")
			p_DiffuseTex = NULL;
#endif // HACK_GTA4
		if(p_StdMtl->SubTexmapOn(ID_OP))
		{
			Texmap* p_TexMap = p_StdMtl->GetSubTexmap(ID_OP);

			p_OpacityTex = GetIBitmapTextInterface(p_TexMap);
#if HACK_GTA4
		// Set the p_OpacityTex to NULL if the tex map name cant be resolved 
			if(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex) == "")
				p_OpacityTex = NULL;
#endif // HACK_GTA4
		}
	}

	if(p_OpacityTex)
	{
		r_MatInfo.m_TypeName = RstMax::GetDefaultAlphaShaderName();
	}
	else
	{
		r_MatInfo.m_TypeName = RstMax::GetDefaultShaderName();
	}

	if(p_DiffuseTex)
	{
		atString s(rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex))));
		atString t(rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex))));

		t += ".dds";

		if(p_OpacityTex)
		{
			atString combined(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex));
			combined += "+";  // indicate to lower-level code that we need to combine two textures
			combined += rexMaxUtility::GetTexMapFullPath(p_OpacityTex);
			r_MatInfo.m_InputTextureFileNames.PushAndGrow(combined,1);
		}
		else
			r_MatInfo.m_InputTextureFileNames.PushAndGrow(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex),1);

		r_MatInfo.m_OutputTextureFileNames.PushAndGrow(t,1);

		r_MatInfo.m_RageTypes.Reset();
		r_MatInfo.m_RageTypes.Resize(1);
		r_MatInfo.m_AttributeNames.Reset();
		r_MatInfo.m_AttributeNames.Resize(1);
		r_MatInfo.m_AttributeValues.Reset();
		r_MatInfo.m_AttributeValues.Resize(1);

		r_MatInfo.m_RageTypes[0] = "grcTexture";
		r_MatInfo.m_AttributeNames[0] = "diffuseTex";
		r_MatInfo.m_AttributeValues[0] = s;
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool DefShader::GetMaterialStandard( StdMat* p_StdMtl, RstMtlMax& r_RstMatInfo, bool& r_IsTwoSided )
{
	BitmapTex* p_DiffuseTex = NULL;
	BitmapTex* p_OpacityTex = NULL;

	if(p_StdMtl->SubTexmapOn(ID_DI))
	{
		Texmap* p_TexMap = p_StdMtl->GetSubTexmap(ID_DI);

		p_DiffuseTex = GetIBitmapTextInterface(p_TexMap);
		if(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex) == "")
			p_DiffuseTex = NULL;

		if(p_StdMtl->SubTexmapOn(ID_OP))
		{
			Texmap* p_TexMap = p_StdMtl->GetSubTexmap(ID_OP);

			p_OpacityTex = GetIBitmapTextInterface(p_TexMap);

			if(rexMaxUtility::GetTexMapFullPath(p_DiffuseTex) == "")
				p_OpacityTex = NULL;

		}
	}

	if(p_OpacityTex)
		r_RstMatInfo.SetShaderName( RstMax::GetDefaultAlphaShaderName() );
	else
		r_RstMatInfo.SetShaderName( RstMax::GetDefaultShaderName() );

	if(p_DiffuseTex)
	{
		if(p_OpacityTex)
			r_RstMatInfo.SetSubTexmap(1, p_OpacityTex);
			
		r_RstMatInfo.SetSubTexmap(0, p_DiffuseTex);
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void DefShader::GetShaderFlags(StdMat* p_StdMtl,bool& r_Alpha,bool& r_Normal,bool& r_Reflect,bool& r_Specular,bool& r_DecAmbient)
{
	r_Alpha = false;
	r_Normal = false;
	r_Reflect = false;
	r_Specular = false;
	r_DecAmbient = false;

	//only allow opacity if diffuse also on...

	if(p_StdMtl->SubTexmapOn(ID_DI) && p_StdMtl->GetSubTexmap(ID_DI))
	{
		if(p_StdMtl->SubTexmapOn(ID_OP) && p_StdMtl->GetSubTexmap(ID_OP))
		{
			r_Alpha = true;
		}
		else
		{
			Texmap* p_DiffuseMap = p_StdMtl->GetSubTexmap(ID_DI);
			BitmapTex* p_DiffuseTex = GetIBitmapTextInterface(p_DiffuseMap);

			if(p_DiffuseTex)
			{
				Bitmap* p_Bitmap = p_DiffuseTex->GetBitmap(0);
				if(p_Bitmap && p_Bitmap->HasAlpha())
				{
					r_Alpha = true;
				}
			}
		}
	}

	if(p_StdMtl->SubTexmapOn(ID_BU) && p_StdMtl->GetSubTexmap(ID_BU))
	{
		r_Normal = true;
	}

	if(p_StdMtl->SubTexmapOn(ID_RL) && p_StdMtl->GetSubTexmap(ID_RL))
	{
		r_Reflect = true;
	}

	if(p_StdMtl->SubTexmapOn(ID_SP) && p_StdMtl->GetSubTexmap(ID_SP))
	{
		r_Specular = true;
	}

	if(p_StdMtl->SubTexmapOn(ID_AM) && p_StdMtl->GetSubTexmap(ID_AM))
	{
		r_DecAmbient = true;
	}

}

bool DefShader::GetShaderName(StdMat* p_Mtl,atString& r_ShaderName)
{
	bool Alpha,Normal,Reflect,Specular,DecAmbient;

	GetShaderFlags(p_Mtl,Alpha,Normal,Reflect,Specular,DecAmbient);

	s32 ShaderType = GetShaderType(Alpha,Normal,Reflect,Specular,DecAmbient);

	r_ShaderName = *(m_ShaderList.Access(ShaderType));

	return true;
}

