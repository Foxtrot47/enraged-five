#include "rexMaxUtility/normals.h"

using namespace rage;

//////////////////////////////////////////////////////////////////////////////////////////////////////
class VNormal 
{
public:
	VNormal() {smooth=0;next=NULL;init=FALSE;norm=Vector3(0.0f,0.0f,0.0f);}
	VNormal(Vector3 &n,DWORD s) {next=NULL;init=TRUE;norm=n;smooth=s;}
	~VNormal() {delete next;}

	void AddNormal(Vector3 &n,DWORD s);
	Vector3& GetNormal(DWORD s);
	void Normalize();

protected:
	Vector3 norm;
	DWORD smooth;
	VNormal *next;
	BOOL init;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
void VNormal::AddNormal(Vector3 &n,DWORD s) 
{
	if (!(s&smooth) && init) 
	{
		if (next) 
			next->AddNormal(n,s);
		else 
			next = new VNormal(n,s);
	} 
	else 
	{
		norm += n;
		smooth |= s;
		init = TRUE;
	}
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////////
Vector3 &VNormal::GetNormal(DWORD s) 
{
	if (smooth&s || !next)
		return norm;
	else 
		return next->GetNormal(s); 
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////////
void VNormal::Normalize() 
{
	VNormal *ptr = next, *prev = this;

	while (ptr) 
	{
		if (ptr->smooth&smooth) 
		{
			norm += ptr->norm;
			prev->next = ptr->next;
			ptr->next = NULL;
			delete ptr;
			ptr = prev->next;
		} 
		else 
		{
			prev = ptr;
			ptr = ptr->next;
		}
	}

	norm.Normalize();

	if(next) 
		next->Normalize();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////
RexMaxNormals::RexMaxNormals(const Mesh& r_Mesh,const Point3* vertList, bool useWeightedNormals)
{
	//create the smoothing group vertex lists

	Point3 work,v0,v1,v2;
	s32 i,j,NumFaces = r_Mesh.getNumFaces();
	s32 NumVerts = r_Mesh.getNumVerts();

	atArray<VNormal, 0, unsigned int> VertNormals;

	VertNormals.Reset();
	VertNormals.Resize(NumVerts);

	for(i=0;i<NumFaces;i++) 
	{
		Face* face = &r_Mesh.faces[i];

		v0 = vertList[face->v[0]];
		v1 = vertList[face->v[1]];
		v2 = vertList[face->v[2]];
		//		v0 = r_Mesh.verts[face->v[0]];
		//		v1 = r_Mesh.verts[face->v[1]];
		//		v2 = r_Mesh.verts[face->v[2]];
		work = (v1-v0)^(v2-v1);

		if(useWeightedNormals)
			work = Normalize( work );

		for(j=0;j<3;j++) 
		{  
			VertNormals[face->v[j]].AddNormal(Vector3(work[0],work[1],work[2]),face->smGroup);
		}
	}

	for(i=0;i<NumVerts;i++)
	{
		VertNormals[i].Normalize();
	}

	//make our vertex normals and faces
	//currently doesnt take into account repeated normals
	//i.e. unoptimised

	m_VNFaces.Reset();
	m_VNFaces.Resize(NumFaces);

	m_VertNormals.Reset();
	m_VertNormals.Resize(NumFaces * 3);

	for(i=0;i<NumFaces;i++) 
	{
		Face* face = &r_Mesh.faces[i];

		m_VNFaces[i].v[0] = 3 * i;
		m_VNFaces[i].v[1] = 3 * i + 1;
		m_VNFaces[i].v[2] = 3 * i + 2;

		for(j=0;j<3;j++)
		{
			m_VertNormals[3 * i + j] = VertNormals[face->v[j]].GetNormal(face->smGroup);
			m_VertNormals[3 * i + j].Normalize();
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
s32 RexMaxNormals::GetNumVertNormals() const
{
	return m_VertNormals.GetCount();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxNormals::GetVertNormal(s32 Idx,Vector3& r_Normal) const
{
	r_Normal = m_VertNormals[Idx];
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
const RexMaxNormals::VNFace* RexMaxNormals::GetVertNormalFace(s32 Idx) const
{
	return &m_VNFaces[Idx];
}
