#ifndef __REX_MAX_NORMALS_H__
#define __REX_MAX_NORMALS_H__

#include "vector/vector3.h"
#include "atl/array.h"

namespace rage {

class Vector3;

/* PURPOSE:
	this class takes care of converting from max's smoothing groups to rage format vertex normals.
	the number of vertex normal faces will be the same as the number of vertex faces
*/
class RexMaxNormals
{
public:
	struct VNFace
	{
		s32 v[3];
	};

	/*	PURPOSE
		initialises normal data from passed in mesh
	PARAMS
		r_Mesh - source mesh
	*/
	RexMaxNormals(const Mesh& r_Mesh,const Point3* vertList, bool useWeightedNormals);

	/*	PURPOSE
			gets the number of unique normals that have been created
		RETURNS
			number of vertex normals
	*/
	s32 GetNumVertNormals() const;

	/*	PURPOSE
			gets a specific normals
		PARAMS
			Idx - index of the normal to get
			r_Normal - the output normals
	*/
	void GetVertNormal(s32 Idx,Vector3& r_Normal) const;

	/*	PURPOSE
			get the indices to the normals used by a specific face.
			the number of faces will be the same as in the mesh
		PARAMS
			Idx - face index to get the data for
		RETURNS
			const pointer the the face data
	*/
	const VNFace* GetVertNormalFace(s32 Idx) const;

protected:
	atArray<Vector3, 0, unsigned int> m_VertNormals;
	atArray<VNFace, 0, unsigned int> m_VNFaces;
};

} // namespace rage {

#endif //__REX_MAX_NORMALS_H__
