#ifndef __REX_MAX_LOG_H__
#define __REX_MAX_LOG_H__

#include "atl/string.h"

namespace rage {

/* PURPOSE:
	encapsulates funcionality for an output log window used during export
	to display warnings and errors
*/
class MaxExportLog
{
public:
	MaxExportLog();

	/*	PURPOSE
			show the window
		PARAMS
			ParentWnd - parent window handle
	*/
	void Open(HWND ParentWnd);

	/*	PURPOSE
			destroys the windows, all log contents are currently lost
	*/		
	void Close();

	/*	PURPOSE
			adds a new line to the log
		PARAMS
			r_Out - contents of new line to add
	*/
	void AddString(const char* r_Out);

protected:

	/*	PURPOSE
			internal windows update procedure
		PARAMS
			hWnd - handle to window that should handle this message
			uMsg - message to handle
			wParam - message data
			lParam - message data
		RETURNS
			return code from message
	*/
	static LRESULT WINAPI LogWindowProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);

	/*	PURPOSE
			called from LogWindowProc to handle a WM_CLOSE message
	*/
	void OnClose();

	/*	PURPOSE
			called from LogWindowProc to handle a WM_SIZE message
	*/
	void OnSize();

	static bool m_RunOnce;
	HWND m_WndMain;
	HWND m_WndList;
};

} //namespace rage {

#endif //__REX_MAX_LOG_H__
