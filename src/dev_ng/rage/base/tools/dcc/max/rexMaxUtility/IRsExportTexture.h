#include "atl/string.h"

class BitmapTex;

#define RSEXPORTTEXTURE_INTERFACE Interface_ID(0x790d35b1, 0x7d920e38)

class IRsExportTexture
{
public:
	virtual BitmapTex *GetResultMap()=0;
	virtual const char* GetMapName( )=0;
};