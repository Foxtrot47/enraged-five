Project rexMaxUtility
Template max_configurations.xml

ConfigurationType library

Define RSTMAXMTL_NOIMPORT

ForceInclude ForceInclude.h

IncludePath $(RAGE_3RDPARTY)\cli\libxml2-2.7.6\include
IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\tools\dcc\libs
IncludePath $(RAGE_DIR)\base\tools\dcc\max
IncludePath $(RAGE_DIR)\base\tools\libs
IncludePath $(RAGE_DIR)\framework\tools\src\GTA_tools\3dStudioMax\lib

Files {
	DefShader.cpp
	DefShader.h
	ForceInclude.h
	Log.cpp
	Log.h
	MaxMorpher.h
	MaxObj.h
	Normals.cpp
	Normals.h
	Utility.cpp
	Utility.h
}