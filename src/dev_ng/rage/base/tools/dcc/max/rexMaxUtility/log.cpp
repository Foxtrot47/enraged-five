
//////////////////////////////////////////////////////////////////////////////////////////////////////
#include "rexMaxUtility/log.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////
using namespace rage;

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool MaxExportLog::m_RunOnce(false);

//////////////////////////////////////////////////////////////////////////////////////////////////////
MaxExportLog::MaxExportLog():
	m_WndMain(NULL),
	m_WndList(NULL)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
LRESULT WINAPI MaxExportLog::LogWindowProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam)
{ 
	MaxExportLog* p_Log = (MaxExportLog*)GetWindowLongPtr(hWnd,0);

    switch(uMsg) 
	{
	case WM_SIZE:
		if(p_Log)
			p_Log->OnSize();
		return 0;
    case WM_CLOSE:
		if(p_Log)
			p_Log->OnClose();
    }

    return (LONG)DefWindowProc(hWnd, uMsg, wParam, lParam); 
} 

//////////////////////////////////////////////////////////////////////////////////////////////////////
void MaxExportLog::Open(HWND ParentWnd)
{
	if(m_WndMain != NULL)
	{
		return;
	}

	if(!m_RunOnce)
	{
		WNDCLASS wc;

		wc.style         = CS_OWNDC;
		wc.lpfnWndProc   = LogWindowProc;
		wc.cbClsExtra    = 0;
		wc.cbWndExtra    = 4;
		wc.hInstance     = GetModuleHandle(NULL);
		wc.hIcon         = LoadIcon(NULL,IDI_WINLOGO);
		wc.hCursor       = LoadCursor(NULL,IDC_ARROW);
		wc.hbrBackground = (HBRUSH)CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
		wc.lpszMenuName  = NULL;
		wc.lpszClassName = "rexlog";

		RegisterClass(&wc);

		m_RunOnce = true;
	}	

	m_WndMain = CreateWindowEx(	NULL,
								"rexlog",
								"rex output log",
								WS_VISIBLE|WS_CAPTION|WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN,
								200,200,
								500,250,
								ParentWnd,
								NULL,
								GetModuleHandle(NULL),
								NULL);

	SetWindowLongPtr(m_WndMain,0,(LONG_PTR)this);

	RECT RectDim;
 	GetClientRect(m_WndMain,&RectDim);

	m_WndList = CreateWindowEx(	WS_EX_CLIENTEDGE,
								"LISTBOX", 
								"", 
								WS_VISIBLE|WS_CHILD|WS_VSCROLL|ES_AUTOVSCROLL,
								RectDim.left,RectDim.top,
								RectDim.right,RectDim.bottom,
								m_WndMain,
								NULL,
								GetModuleHandle(NULL),
								NULL);

	SendMessage(m_WndList,WM_SETFONT,(LPARAM)GetStockObject(DEFAULT_GUI_FONT),(WPARAM)0);
}

void MaxExportLog::Close()
{
	DestroyWindow(m_WndMain);
	m_WndMain = NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void MaxExportLog::AddString(const char* r_Out)
{
	if(m_WndMain == NULL)
	{
		return;
	}

	const char* p_String = r_Out;
	s32 Index = SendMessage(m_WndList,LB_ADDSTRING,0,(LPARAM)p_String);
	SendMessage(m_WndList,LB_SETCURSEL,(WPARAM)Index,0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void MaxExportLog::OnClose()
{
	m_WndMain = NULL;
	m_WndList = NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void MaxExportLog::OnSize()
{
	RECT RectDim;
 	GetClientRect(m_WndMain,&RectDim);
	SetWindowPos(m_WndList,HWND_TOP,0,0,RectDim.right,RectDim.bottom,SWP_SHOWWINDOW);
	UpdateWindow(m_WndList);
}
