#ifndef __REX_MAX_UTILITY_H__
#define __REX_MAX_UTILITY_H__

#include "atl/array.h"
#include "atl/dlist.h"
#include "atl/string.h"

#include "rexBase/serializer.h"
#include "rexBase/utility.h"
#include "rexBase/shell.h"
#include "rexGeneric/objectMesh.h"
#include "rageMaxCollisionVolumes/SelectCMode.h"
#include "MaterialPresets/MaterialPreset.h"

#include "phcore/constants.h"

namespace rage {

class rexObjectGenericBound;

class ExportOptions;

#define NO_VERTEX_CHANNEL_CULLING -3

#if HACK_GTA4
#define GTAWHEEL_CLASS_ID Class_ID(0x575a5bdb, 0x4c09135f)
#endif // HACK_GTA4

/* PURPOSE:
	collection of utility functions for commomly used tasks in max
*/
namespace rexMaxUtility {

	/*	PURPOSE
		copy to RAGE type
	*/
	void GetVector3FromPoint3(Vector3 &outVec, Point3 &inVec);

	/*	PURPOSE
		get the scenes animation start time
	*/
	f32 GetAnimStartTime();

	/*	PURPOSE
		get the scenes animation end time
	*/
	f32 GetAnimEndTime();

	s32 GetAnimFrame();
	void SetAnimFrame(s32 Frame);

	/*	PURPOSE
		get the frames per second set in max
	*/
	f32 GetFPS();

	/*	PURPOSE
		add a time stamp to the log window
	*/
	void AddTimeStamp(const atString& Prepend);

	/*	PURPOSE
		add the passed in result's messages to the log window
	*/
	void AddResultsToLog(const rexResult& Result);

	/*	PURPOSE
		add a string to the log window
	*/
	void AddLog(const atString& Out);

	/*	PURPOSE
	find a node
	*/
	INode* FindNode(INode* p_Root,const atString& FindName);

	/*	PURPOSE
		add a string to the log window
	*/
	bool IsValidSkelNode(INode* p_MaxNode);

	/*	PURPOSE
		determine if the specified node is the root of a skeleton hierarchy
	*/
	bool IsRootSkelNode(INode* p_MaxNode);

	atString GetTexBaseNameFromPath(const atString& r_in);

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	/*	PURPOSE
		gets the local matrix of the passed in node at a specific time
	*/
#if HACK_GTA4
	void GetLocalMatrix(INode* p_Node,f32 Time,Matrix34& r_Local,bool WithObjectTM = false);
#else 
	void GetLocalMatrix(INode* p_Node,f32 Time,Matrix34& r_Local);
#endif // HACK_GTA4
	/*	PURPOSE
		get a max property from a node
	*/
	atString GetProperty(INode* p_BoneNode,const char* r_Property);

	/*	PURPOSE
	get a max property from a node and check whether is set to true
	*/
	BOOL GetBoolProperty(INode* p_BoneNode,const char* r_Property);

	/*	PURPOSE
		get the bone id assigned to a max node (tag=[num] in the user properties in max)
	*/
	atString GetBoneID(INode* p_BoneNode);

	/*	PURPOSE
		gets the bone tags assigned to the max node (bonetags=[tags] in the user properties in max)
	*/
	void GetBoneTags(INode* p_BoneNode, atArray< atString >& r_tagArray);

	/*	PURPOSE
		gets the parent of a node (returns null if the parent is the scene root)
	*/
	INode* GetParent(INode* p_Node);

	/*	PURPOSE
		gets the first bone parent of a node (returns null if not found)
	*/
	INode* GetParentBone(INode* p_Node);

	/*	PURPOSE
		gets the morph modifier applied to a mesh
	*/
#if HACK_GTA4
	// I Cant see anywhere we actually make a call requiring the mod context
	Modifier* GetMorph(INode* p_Node,ModContext** pp_ModContext = NULL);
	void GetMorphs(INode* p_Node, atArray<Modifier*>* pModifier, ModContext** pp_ModContext = NULL);
#else
	Modifier* GetMorph(INode* p_Node);
	void GetMorphs(INode* p_Node, atArray<Modifier*>* pModifiders);
#endif // HACK_GTA4

	/*	PURPOSE
		toggles the bind pose on/off for bipeds
	*/
	void SetSkinBindPose(INode* p_RootBone,bool On);

	/*	PURPOSE
		return if the passed in node's object is of a specific type
	*/
	bool IsType(INode* p_Node,Class_ID ClassID,bool Strict = false);

	/*	PURPOSE
		return material preset information, if valid.
	*/
	bool GetMaterialPresetInfo(Mtl* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo,MaterialPreset& materialPreset);

	/*	PURPOSE
		turns a max material into a generic rex material
	*/
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	bool GetMaterialInfoUnsafe(Mtl* p_Mtl,rexObjectGenericMesh::MaterialInfo& matInfo,bool& r_IsTwoSided,bool ForceAlphaShader,u8 vertColId);
#else
	bool GetMaterialInfoUnsafe(Mtl* p_Mtl,rexObjectGenericMesh::MaterialInfo& matInfo,bool& r_IsTwoSided,bool ForceAlphaShader);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS

	/*	PURPOSE
		turns a max material into a generic rex material
	*/
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	bool GetMaterialInfo(Mtl* p_Mtl,rexObjectGenericMesh::MaterialInfo& matInfo,bool& r_IsTwoSided,bool ForceAlphaShader,u8 vertColId);
#else
	bool GetMaterialInfo(Mtl* p_Mtl,rexObjectGenericMesh::MaterialInfo& matInfo,bool& r_IsTwoSided,bool ForceAlphaShader);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS

	bool GetMainTexMapInfo(Mtl* p_Mtl,Texmap** pp_texMap);

	/*	PURPOSE
		returns a pointer to a sub material if rootmtl is a multimaterial
		else just returns rootmtl
	*/
	Mtl* GetFaceMtl(Mtl* p_RootMtl,MtlID FaceMtlID);

	/*	PURPOSE
		gets the full bitmap path contained in a texmap
	*/
	atString GetTexMapFullPath(Texmap* p_Texmap);

	/*	PURPOSE
		sets the full bitmap path contained in a texmap
	*/
	void SetTexMapFullPath(Texmap* p_Texmap, atString& textureName);

	/*	PURPOSE
	return if a texture map has uv animation
	*/
	bool IsTexMapAnimated(Texmap* p_Texmap,s32& FirstFrame,s32& EndFrame);

	/*	PURPOSE
		get a list of the texmaps used by a geometry in the order they will be exported
	*/
	bool GetMainTexMapsFromMaxMeshNode(INode* p_Node,atArray<Texmap*>& r_TexMapList);
#if HACK_GTA4
	bool GetMainTexMapsFromMaxMeshNodeRec(INode* p_Node,atArray<Texmap*>& r_TexMapList);
#endif // HACK_GTA4

	/*	PURPOSE
		turns a max mesh into a generic rex mesh
	*/
	bool GetMaterialsFromMaxMeshNode(INode* p_Node,atArray<rexObjectGenericMesh::MaterialInfo>& r_MatList);

	/*	PURPOSE
		turns a max mesh into a generic rex mesh
	*/
	bool ConvertMaxMeshNodeIntoGenericMesh(	INode* p_Node,
											rexObjectGenericMesh* newObject,
											rexResult& r_res,
											s32 BoneIndex = 0,
											bool IsBound = false,
											bool WorldSpace = false,
											bool SkinOffset = true,
											bool KeepRotation = false,
											bool ObjectSpace = false,
											const atString& r_BoundMat = atString(""),
											s32 Time = 0,
											bool OptimiseMaterials = true,
											s32 RoomID = 0,
											const atString& r_CustomVertexColour = atString(""),
											bool ExportCollisionMeshTint = false,
											int lowerLimitVertexChannel = 8,
											int forcedAdditionalUVCount = 0,
											bool limitSkinToSpecFileBones = false,
											bool useWeightedNormals = true
											);

	/*	PURPOSE
		matrix copy functions from max to rage
	*/
	void CopyMaxMatrixToRageMatrix(const Matrix3& r_SrcMat,Matrix34& r_DstMat);

	/*	PURPOSE
	matrix copy functions from rage to max
	*/
	void CopyRageMatrixToMaxMatrix(const Matrix34& r_SrcMat,Matrix3& r_DstMat);

	/*	PURPOSE
		print up a modal 'ok' dialog box
	*/
	void MessageBoxOK(const char* info, const atString& title);

	/*	PURPOSE
		creates a progress window with numValues progress bars
	*/
	bool OpenProgressWindow(const char *title,int numValues);

	/*	PURPOSE
		closes the progress window
	*/
	bool CloseProgressWindow(void);

	/*	PURPOSE
		sets the passed in progress bars associated text
	*/
	bool UpdateProgressWindowInfo(int index,const char *info);

	/*	PURPOSE
		sets the passed in progress bars maximum value
	*/
	bool UpdateProgressWindowMax(int index,int max);

	/*	PURPOSE
		set the passed in progress bars current value
	*/
	bool UpdateProgressWindowValue(int index,int value);

	/*	PURPOSE
		replace whitespace chars with underscores
	*/
	atString ReplaceWhitespaceCharacters(const char* str);

	/*	PURPOSE
		fills in a list of all selected nodes from the passed in root down
	*/
	void GetSelectedNode(atArray<INode*>& r_NodeList,INode* p_RootNode = NULL);

	/*	PURPOSE
		saving to the registry
	*/
	void SetRegistry(const atString& r_Name,const atString& r_Value);
	void SetRegistry(const atString& r_Name,u32 Value);
	void SetRegistry(const atString& r_Name,bool Value);

	/*	PURPOSE
		loading from the registry
	*/
	bool GetRegistry(const atString& r_Name,atString& r_Value);
	bool GetRegistry(const atString& r_Name,u32& r_Value);
	bool GetRegistry(const atString& r_Name,bool& r_Value);

	rexResult::eResultType GetWarningLevel();

	/*	PURPOSE
		if the passed in node is selected then return it, else return the first
		child that is selected
	*/
	INode* GetNextSelected(INode* p_Node);

	/*	PURPOSE
		get a scene path for the node - needed because rex matches up nodes
		by their path in some parts of the export (such as the fragments).
		If a node has the name "Parent" and has a child in the scene called "Chil d",
		its path would be "Parent/Chil_d"
	*/
	void GetNodeFullPath(INode* p_Node,atString& r_Path);

	/*	PURPOSE
		Given an Object, find its point controller for vertex animation
	*/
	ReferenceMaker* GetMasterPointController(ReferenceMaker* p_Anim);

	/*	PURPOSE
		Given an Object, find a list of keyframe times for vertex animation
	*/
	bool GetVertexKeyTimes(Object* p_Object,atDList<s32>& r_KeyTimes);

	/*	PURPOSE
		Given an Node and a list of keyframe times, tag the materials that are affected
		by animation
	*/
	void TagBlendShapeMaterials(INode* p_Node,s32 NumTargets, rexObjectGenericMesh* newObject);

	/*	PURPOSE
		Give a Node that contains a collision box, fill in the bound structure with equivalent
		information for a collision mesh. Used the export collision boxes into the octree
	*/
	void ConvertCollisionBoxToRageMesh(INode* p_Node,rexObjectGenericBound* p_Bnd,const atString& r_BoundMat,bool WorldSpace);

	/*	PURPOSE
		returns true if the output file is out of date. i.e. should be reexported
	*/
	bool TextureExportNeeded(const atString& inputFileName,const atString& outputFileName);

	/*	PURPOSE
		Retrieves the bullet margin value from a node
	*/
	bool GetBoundMargin(INode* p_Node, float& r_Margin);

	/*	PURPOSE
		Define Collision types
	*/
	bool IsCollisionObject(INode* p_Node);
	/*	PURPOSE
		Retrieves the bullet margin value from a node
	*/
	bool CheckMaxMeshContinuinity(INode* p_Node, rexResult &result);

	void CentreWindow(HWND hWnd);

	s32 GetTicksPerSec();

	void BeginDevil();
	void EndDevil();

	rexResult LoadDevilImage(const char* fileName);
#if HACK_GTA4
	rexResult ConvertAndSaveDevilImage(const char* outputFileName);
	rexResult SaveDevilImage(const char* outputFileName );
	rexResult SaveDevilImageAsTiff(const char* outputFileName, int tiff_compression );
	unsigned char* DevILGetFlipped( );
#endif // 
	rexResult ConvertAndSaveBumpDevilImage(const char* outputFileName);

	rexResult ExportTexture(const char* inputFileName, const char* outputFileName, bool hasAlpha, bool isBumpMap, bool isRaw);

	rexResult ExportTextureAsTiff( const char* inputFileName, const char* outputFileName, int compression );

	void SetTextureSizeLimit(s32 SizeLimit);

	void SetTextureShiftDown(s32 ShiftDown);
	s32 GetTextureShiftDown( );

	bool HasTranslationAnimation(INode* p_Node,s32 StartFrame,s32 EndFrame);

	bool HasAnyAlphaMap(Mtl* p_Mtl);

	bool HasAlphaVertColour(INode* p_Node,Mtl* p_Mtl);

	bool IsMatch(const Vector3& r_A,const Vector3& r_B,float Tolerance = 0.0001f);

	void SetForceTextureExport(bool On);
	bool GetForceTextureExport( );

#if HACK_GTA4
	bool GenerateColourPalette( atArray<Mesh*>& meshes, atArray<int, 0, u32>& idxList, int maxPaletteSize, int minWidthPixels, const atString& outputFilename ); 
	bool GenerateColourList( atArray<Mesh*>& meshes, atArray<Vector4, 0, u32>& vertColList, int colourChannel );
#endif // HACK_GTA4

#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	static atArray<Vector4> sm_colourTemplate;
	atArray<Vector4>& GetColourTemplate(); 
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS

//	bool HasChildCollision(INode* p_Node);

} //namespace rexMaxUtility {

} //namespace rage {

#endif //__REX_MAX_UTILITY_H__
