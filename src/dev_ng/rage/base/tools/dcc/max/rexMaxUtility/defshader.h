#ifndef __REX_MAX_DEFSHADER_H__
#define __REX_MAX_DEFSHADER_H__

#include "rexGeneric/objectMesh.h"
#include "rstMaxMtl/mtl.h"
#include "atl/map.h"
#include "atl/string.h"

namespace rage {

//a system for converting from max materials to rage shaders
//it will choose the best shader for the job and fill in the
//shader variables as best it can.
//note that the variable names are pretty much hardcoded
//if you want to change the shaders that are used you will need to
//keep these variables the same

//on setup it will look for an xml config file called rst_defaultshader.xml
//in max's plugcfg directory

/////////////////////////////////////////////////////////////////////////////////////////////////////
class DefShader
{
public:
	static DefShader& GetInst();

	// Accessor to list of shader names based on shader flags
	bool GetShaderName(StdMat* p_Mtl,atString& r_ShaderName);

	bool GetMaterialStandard(StdMat* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo,bool& r_IsTwoSided);
	bool GetMaterialStandard( StdMat* p_StdMtl, RstMtlMax& r_RstMatInfo, bool& r_IsTwoSided );
	bool GetMaterial(StdMat* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo,bool& r_IsTwoSided,bool ForceAlphaShader);
	bool GetMaterial( StdMat* p_Mtl, RstMtlMax& r_rstMat, bool bForceAlphaShader );

protected:
	DefShader();

	// Add support for DecAmbient (decal shader that affects ambient occlusion only
	s32 GetShaderType(bool Alpha,bool Normal,bool Reflect,bool Specular,bool DecAmbient);
	void GetShaderFlags(StdMat* p_Mtl,bool& r_Alpha,bool& r_Normal,bool& r_Reflect,bool& r_Specular,bool& r_DecAmbient);

	bool FillTextureChannel( StdMat* p_StdMtl, RstMtlMax& r_rstMat, int stdTexChannel, int rageTexChannel );

	void FillDecAmbient(StdMat* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo);
	void FillDiffuse(StdMat* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo);
	void FillNormal(StdMat* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo);
	void FillReflect(StdMat* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo);
	void FillSpecular(StdMat* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo);
		
	enum
	{
		DEFAULT,
		ALPHA,
		REFLECT,
		REFLECT_ALPHA,
		SPEC,
		SPEC_ALPHA,
		SPEC_REFLECT,
		SPEC_REFLECT_ALPHA,

		NORMAL,
		NORMAL_ALPHA,
		NORMAL_REFLECT,
		NORMAL_REFLECT_ALPHA,
		NORMAL_SPEC,
		NORMAL_SPEC_ALPHA,
		NORMAL_SPEC_REFLECT,
		NORMAL_SPEC_REFLECT_ALPHA,

		// For decal ambient only shader
		DECAMBIENT,
		DECAMBIENT_ALPHA,
		DECAMBIENT_REFLECT,
		DECAMBIENT_REFLECT_ALPHA,
		DECAMBIENT_SPEC,
		DECAMBIENT_SPEC_ALPHA,
		DECAMBIENT_SPEC_REFLECT,
		DECAMBIENT_SPEC_REFLECT_ALPHA,

		DECAMBIENT_NORMAL,
		DECAMBIENT_NORMAL_ALPHA,
		DECAMBIENT_NORMAL_REFLECT,
		DECAMBIENT_NORMAL_REFLECT_ALPHA,
		DECAMBIENT_NORMAL_SPEC,
		DECAMBIENT_NORMAL_SPEC_ALPHA,
		DECAMBIENT_NORMAL_SPEC_REFLECT,
		DECAMBIENT_NORMAL_SPEC_REFLECT_ALPHA,

	};

	atMap<s32,atString> m_ShaderList;
};

} //namespace rage {

#endif //__REX_MAX_DEFSHADER_H__
