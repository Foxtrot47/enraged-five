#ifndef __MAX_FORCEINCLUDE_H__
#define __MAX_FORCEINCLUDE_H__

//this file is included through the forceinclude setting
//in the project so is included in every file

// This is required since we pulled out stlport.
// MS version of stl with 2008 sucks for debug.
#if defined(_DEBUG) && _MSC_VER < 1600
#define _HAS_ITERATOR_DEBUGGING 0
#endif

//we have to include max first because it defines some functions
//that are macros in the rage headers (Assert)

#include <max.h>
//#include <resource.h>
#include <bmmlib.h>
#include <guplib.h>
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <modstack.h>
#include <iskin.h>
#include <inode.h>
#include <actiontable.h>
#include <iskinpose.h>
#include <imenuman.h>
#include <cs/bipexp.h>
#include <cs/keytrack.h>
#include <stdmat.h>
#include <MeshNormalSpec.h> 
#include <xref/ixrefmaterial.h> 
#include <ipathconfigmgr.h> 

#if defined(_DEBUG) && defined(_M_X64)
#include "forceinclude/win64_tooldebug.h"
#elif defined(_DEBUG)
#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG) && defined(_M_X64)
#include "forceinclude/win64_toolrelease.h"
#elif defined(NDEBUG)
#include "forceinclude/win32_toolrelease.h"
#elif defined(_M_X64)
#include "forceinclude/win64_toolbeta.h"
#else
#include "forceinclude/win32_toolbeta.h"
#endif

#pragma warning( disable: 4800 )
#pragma warning( disable: 4265 )
#pragma warning( disable: 4263 )

enum
{
	NAME_LENGTH = 256
};

#define PLUGIN_VERSION 2 //1 = version 0.01

#endif //__MAX_FORCEINCLUDE_H__
