#include "rexMaxUtility/utility.h"
#include "rexMaxUtility/log.h"
#include "rexMaxUtility/normals.h"
#include "rexMaxUtility/maxobj.h"
#include "rexMaxUtility/IRsExportTexture.h"
//#include "rexMax/wrapper.h"
#include "rexMaxUtility/defShader.h"
#include "rexGeneric/objectMesh.h"
#include "rexGeneric/objectBound.h"
#include "rexMaxToGeneric/converterSkeleton.h"
#include "rexBase/colourquantizer.h"
#include "atl/map.h"
#include "atl/string.h"
#include "diag/output.h"
#include "file/stream.h"
#include "mesh/mesh.h"
#include "math/amath.h"
#include "rstMaxMtl/mtl.h"
#include "rstMaxMtl/rstMax.h"
#include "rexMaxBoundMtl/boundMtl.h"
#include "grmodel/shader.h"
#include "grcore/image.h"
#include "MNMATH.H"
#include "triobj.h"
#include "MaxUtil/MaxUtil.h"
#include "MaterialPresets/MaterialPresetManager.h"

// DEvIL headers
#include "devil/il.h"
#include "devil/ilu.h"

// libtiff headers
#include "libtiff/tiff.h"
#include "libtiff/tiffio.h"

#if HACK_GTA4
// Used for range check
#include "math/simplemath.h"
#endif // HACK_GTA4

#if (MAX_RELEASE >= 12000)	
#include "IFileResolutionManager.h"
#endif // (MAX_RELEASE >= 12000)

using namespace rage;

static const s32 TicksPerSec = 4800;
static MaxExportLog g_ExportLog;

atMap<u8,Vector3> sm_colourTemplate;

#if HACK_GTA4
// Geo mesh
#define MAP_COLOR1			9	// Terrain vertex painting channel
#define MAP_COLOR2			11	// CPV wind vertex channel
#define MESH_TINT_COLOR		13	// Mesh tinting colour channel
#define PED_ENV				14	// Environmental channel
#define PED_WEIGHT_GAIN		15	// Weight gain channel
#define MAP_COLOR3			16	// Wind and advanced terrain shader channel
#define MAP_COLOR3_ALPHA	17	// Wind and advanced terrain shader channel ALPHA

// Collision mesh (i.e. IsBound)
#define MAP_SECOND_SURFACE		10	// Bound second surface channel
#define MAP_BOUND_TINT_COLOUR	11	// Tint colour for bounds
#define MAP_BOUND_SCALE			12	// Procedural density value
#define MAP_BOUND_TINT_COLOUR_2	13	// New tint colour for bounds

/*
// Morph Target Regions
#define MAP_MORPH_EYES			20
#define MAP_MORPH_NOSE			21
#define MAP_MORPH_MOUTH			22
#define MAP_MORPH_EARS			23

#define MAP_MORPH_EYES_BIT		0x01
#define MAP_MORPH_NOSE_BIT		0x02
#define MAP_MORPH_MOUTH_BIT		0x04
#define MAP_MORPH_EARS_BIT		0x08
*/

#define NUM_UV_FLIPCHANNELS			10
#define BILLBOARD_SCALE_UVCHANNEL	2

#endif // HACK_GTA4

struct MapExportInfo
{
	s32 ID;
#if HACK_GTA4
	s32 AltID;
#endif
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS	
	int ColIdx;
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	bool IsTwoSided;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::GetVector3FromPoint3(Vector3 &outVec, Point3 &inVec)
{
	outVec.x = inVec.x;
	outVec.y = inVec.y;
	outVec.z = inVec.z;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
f32 rexMaxUtility::GetAnimStartTime()
{
	return GetCOREInterface()->GetAnimRange().Start() / (f32)TicksPerSec;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
f32 rexMaxUtility::GetAnimEndTime()
{
	return (GetCOREInterface()->GetAnimRange().End() / (f32)TicksPerSec);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
s32 rexMaxUtility::GetAnimFrame()
{
	s32 TicksPerFrame = (s32)(((float)TicksPerSec) * (1.0f / GetFPS()));

	return (s32)(GetCOREInterface()->GetTime() / (f32)TicksPerFrame);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::SetAnimFrame(s32 Frame)
{
	s32 TicksPerFrame = (s32)(((float)TicksPerSec) * (1.0f / GetFPS()));

	GetCOREInterface()->SetTime((int)((f32)Frame * (f32)TicksPerFrame),FALSE);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::IsTexMapAnimated(Texmap* p_Texmap,s32& FirstFrame,s32& EndFrame)
{
	s32 i;

	if(!p_Texmap)
		return false;

	UVGen* p_uvGen = p_Texmap->GetTheUVGen();

	if(!p_uvGen)
		return false;

	IParamBlock* pb_1 = (IParamBlock*)p_uvGen->SubAnim(0);
	s32 numKeys;
	Control* p_ctrl = NULL;
	s32 UVFirstFrame = -1;
	s32 UVEndFrame = -1;
	s32 TicksPerFrame = (s32)(((float)TicksPerSec) * (1.0f / GetFPS()));

	for(i=0;i<pb_1->NumParams();i++)
	{
		//0 is u offset
		//1 is v offset
		//6 is w rotation

		if(((i == 0) || (i == 1) || (i == 6)) == false)
			continue;

		p_ctrl = pb_1->GetController(i);

		if(!p_ctrl)
			continue;

		if(p_ctrl->ClassID() != Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0))
			continue;

		IKeyControl* p_ikeys = GetKeyControlInterface(p_ctrl);

		if(!p_ikeys)
			continue;

		IBezFloatKey key;
		s32 KeyTime;

		numKeys = p_ikeys->GetNumKeys();

		if(numKeys < 2)
			continue;

		p_ikeys->GetKey(0, &key);
		KeyTime = (s32)(key.time / (f32)TicksPerFrame);

		if(UVFirstFrame == -1)
			UVFirstFrame = KeyTime;
		else
		{
			KeyTime = (s32)(key.time / (f32)TicksPerFrame);

			if(KeyTime < UVFirstFrame)
				UVFirstFrame = KeyTime;
		}

		p_ikeys->GetKey((numKeys - 1), &key);
		KeyTime = (s32)(key.time / (f32)TicksPerFrame);

		if(UVFirstFrame == -1)
			UVFirstFrame = KeyTime;
		else
		{
			KeyTime = (s32)(key.time / (f32)TicksPerFrame);

			if(KeyTime > UVEndFrame)
				UVEndFrame = KeyTime;
		}
	}

	s32 CurrentFrame = rexMaxUtility::GetAnimFrame();
	Matrix3 matTrans,matTransCompare;
	bool RetVal = false;

	rexMaxUtility::SetAnimFrame(UVFirstFrame);
	p_uvGen->GetUVTransform(matTrans);

	for(i=(UVFirstFrame + 1);i<UVEndFrame;i++)
	{
		rexMaxUtility::SetAnimFrame(i);
		p_Texmap->Update((int)((f32)i * (f32)TicksPerFrame),FOREVER);
		p_uvGen->GetUVTransform(matTransCompare);

		if((matTransCompare == matTrans) == false)
		{
			RetVal = true;
			break;
		}
	}

	rexMaxUtility::SetAnimFrame(CurrentFrame);

	return RetVal;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
f32 rexMaxUtility::GetFPS()
{
	return ((f32)GetFrameRate());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::AddTimeStamp(const atString& Prepend)
{
	atString Out(Prepend);
	char Output[1024];
	SYSTEMTIME SystemTime;

	GetSystemTime(&SystemTime);

	sprintf(Output,"%d/%d/%d %d:%02d.%02d",SystemTime.wDay,SystemTime.wMonth,SystemTime.wYear,SystemTime.wHour,SystemTime.wMinute,SystemTime.wSecond);

	Out += atString(Output);

	AddLog(Out);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::AddResultsToLog(const rexResult& Result)
{
	s32 i,MessageCount = Result.GetMessageCount();

	for(i=0;i<MessageCount;i++)
	{
		AddLog(Result.GetMessageByIndex(i));
	}

	if(Result.Errors())
	{
		AddLog(atString("export had errors"));
	}
	else
	{
		AddLog(atString("exported ok"));
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::AddLog(const atString& info)
{
	g_ExportLog.AddString(info);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::IsValidSkelNode(INode* p_MaxNode)
{
	const char* p_Name = p_MaxNode->GetName();
	char Buffer[1024];
	strcpy(Buffer,p_Name);
	strlwr(Buffer);

	//dont allow the footsteps node through into the skeleton
	if(strstr(Buffer,"footsteps"))
	{
		return false;
	}

	// Toronto Specific - Oscar Valer
	if(strstr(Buffer,"hide"))
	{
		return false;
	}
	if(strstr(Buffer,"nub"))
	{
		return false;
	}
	// End of Toronto Specific - Oscar Valer

	if(	rexMaxUtility::IsType(p_MaxNode,Class_ID(TRIOBJ_CLASS_ID,0)) ||
		rexMaxUtility::IsType(p_MaxNode,Class_ID(DUMMY_CLASS_ID,0)) ||
		rexMaxUtility::IsType(p_MaxNode,Class_ID(POINTHELP_CLASS_ID,0)) ||
		rexMaxUtility::IsType(p_MaxNode,SKELOBJ_CLASS_ID) ||
		rexMaxUtility::IsType(p_MaxNode,BIPED_CLASS_ID))
	{
		return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::IsRootSkelNode(INode* p_Node)
{
	if(rexMaxUtility::IsValidSkelNode(p_Node))
	{
		INode* p_ParentNode = rexMaxUtility::GetParent(p_Node);

		while(p_ParentNode != NULL)
		{
			if(rexMaxUtility::IsValidSkelNode(p_ParentNode))
				return false;

			p_ParentNode = rexMaxUtility::GetParent(p_ParentNode);
		}

		return true;
	}

	return false;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
#if HACK_GTA4
void rexMaxUtility::GetLocalMatrix(INode* p_Node,f32 Time,Matrix34& r_Local,bool WithObjectTM)
#else
void rexMaxUtility::GetLocalMatrix(INode* p_Node,f32 Time,Matrix34& r_Local)
#endif // HACK_GTA4
{
	TimeValue TimeVal = (int)(Time * TicksPerSec);
	INode* p_ParentNode = rexMaxUtility::GetParent(p_Node);

	Matrix34 MatObj;																																																																																																		
	Matrix34 MatParent;
#if HACK_GTA4
	if(WithObjectTM)
		CopyMaxMatrixToRageMatrix(p_Node->GetObjectTM(TimeVal),MatObj);
	else
#endif // HACK_GTA4
		CopyMaxMatrixToRageMatrix(p_Node->GetNodeTM(TimeVal),MatObj);

	MatParent.Identity();

	if(p_ParentNode)
	{
		CopyMaxMatrixToRageMatrix(p_ParentNode->GetNodeTM(TimeVal),MatParent);
	}

	// Use this method to calculate the local, dottranspose does a subtract which fucks the scale
	MatParent.Inverse();
	r_Local.Dot(MatObj,MatParent);
	//r_Local.DotTranspose(MatObj,MatParent);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
atString rexMaxUtility::GetProperty(INode* p_BoneNode,const char* r_Property)
{
	TSTR Prop;

	if(!p_BoneNode->GetUserPropString(r_Property,Prop))
	{
		return atString("");
	}

	return atString(Prop);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL rexMaxUtility::GetBoolProperty(INode* p_BoneNode,const char* r_Property)
{
	TSTR Prop;

	if(!p_BoneNode->GetUserPropString(r_Property,Prop))
	{
		return FALSE;
	}

	return Prop == TSTR("true");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
atString rexMaxUtility::GetBoneID(INode* p_BoneNode)
{
	TSTR Prop;

	if(!MaxUtil::GetUserPropString(p_BoneNode, "tag", Prop))
	{
		return atString("");
	}

	if(strcmp(Prop,"0") == 0)
	{
		return atString("#0");
	}

	return atString(Prop);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::GetBoneTags(INode* p_BoneNode, atArray< atString >& r_tagArray)
{
	TSTR Prop;

	if(!p_BoneNode->GetUserPropString("bonetags", Prop))
	{
		return;
	}

	s32 cCount = Prop.Length();
	atString tagStr;
	for(s32 i=0; i<cCount; i++)
	{
		if(Prop[i] == ' ')
			continue;

		if(Prop[i] == ',' && tagStr.length())
		{
			r_tagArray.PushAndGrow(tagStr);
			tagStr.Clear();
		}
		else
		{
			tagStr += Prop[i];
		}
	}

	if(tagStr.length())
	{
		r_tagArray.PushAndGrow(tagStr);
		tagStr.Clear();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
INode* rexMaxUtility::GetParent(INode* p_Node)
{
	if(!p_Node || p_Node == GetCOREInterface()->GetRootNode())
		return NULL;

	INode* p_ParentNode = p_Node->GetParentNode();

	if(p_ParentNode == GetCOREInterface()->GetRootNode())
	{
		return NULL;
	}

	return p_ParentNode;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
INode* rexMaxUtility::GetParentBone(INode* p_Node)
{
	INode* p_ParentNode = rexMaxUtility::GetParent(p_Node);
	while(p_ParentNode != NULL)
	{
		if(rexMaxUtility::IsValidSkelNode(p_ParentNode))
			return p_ParentNode;

		p_ParentNode = rexMaxUtility::GetParent(p_Node);
	}

	return NULL;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
#if HACK_GTA4
Modifier* rexMaxUtility::GetMorph(INode* p_Node,ModContext** pp_ModContext)
#else
Modifier* rexMaxUtility::GetMorph(INode* p_Node)
#endif // HACK_GTA4
{
	Object* p_Obj = p_Node->GetObjectRef();

	if(!p_Obj)
		return NULL;

	while(p_Obj->SuperClassID() == GEN_DERIVOB_CLASS_ID)
	{
		IDerivedObject* p_DerObj = (IDerivedObject*)(p_Obj);

		s32 Idx = 0,Count = p_DerObj->NumModifiers();

		while(Idx < Count)
		{
			Modifier* p_Mod = p_DerObj->GetModifier(Idx);

			Class_ID classID = p_Mod->ClassID();

			if(classID == MR3_CLASS_ID)
			{
#if HACK_GTA4
				if(pp_ModContext)
					*pp_ModContext = p_DerObj->GetModContext(Idx);
#endif // HACK_GTA4
				return ((Modifier*)p_Mod);
			}

			Idx++;
		}

		p_Obj = p_DerObj->GetObjRef();
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
#if HACK_GTA4
void rexMaxUtility::GetMorphs(INode* p_Node, atArray<Modifier*> *pModifiers, ModContext** pp_ModContext)
#else
void rexMaxUtility::GetMorphs(INode* p_Node, atArray<Modifier*> *pModifiers)
#endif // HACK_GTA4
{
	Object* p_Obj = p_Node->GetObjectRef();

	if(!p_Obj || !pModifiers)
		return;

	while(p_Obj->SuperClassID() == GEN_DERIVOB_CLASS_ID)
	{
		IDerivedObject* p_DerObj = (IDerivedObject*)(p_Obj);

		s32 Idx = 0,Count = p_DerObj->NumModifiers();

		while(Idx < Count)
		{
			Modifier* p_Mod = p_DerObj->GetModifier(Idx);

			Class_ID classID = p_Mod->ClassID();

			if(classID == MR3_CLASS_ID)
			{
#if HACK_GTA4
				if(pp_ModContext)
					*pp_ModContext = p_DerObj->GetModContext(Idx);
#endif // HACK_GTA4

				pModifiers->PushAndGrow((Modifier*)p_Mod);
			}

			Idx++;
		}

		p_Obj = p_DerObj->GetObjRef();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::SetSkinBindPose(INode* p_RootBone,bool On)
{
	if(!p_RootBone)
		return;

	Control* p_C = NULL;

	while(!p_C)
	{
		p_C = p_RootBone->GetTMController();

		if(	(p_C->ClassID() == BIPSLAVE_CONTROL_CLASS_ID) ||
			(p_C->ClassID() == BIPBODY_CONTROL_CLASS_ID) ||
			(p_C->ClassID() == FOOTPRINT_CLASS_ID))
		{
			break;
		}

		p_C = NULL;
		INode* p_ParentBone = GetParent(p_RootBone);

		if(!p_ParentBone)
		{
			break;
		}

		p_RootBone = p_ParentBone;
	}

	const char* p_Name = p_RootBone->GetName();

	if(!p_C)
	{
		return;
	}

	IBipedExport* p_BipIface = (IBipedExport *)p_C->GetInterface(I_BIPINTERFACE);

	if(p_BipIface)
	{
		if(On)
		{
			p_BipIface->BeginFigureMode(0);
			p_BipIface->RemoveNonUniformScale(1);
		}
		else
			p_BipIface->EndFigureMode(0);
	}

	p_C->ReleaseInterface(I_BIPINTERFACE,p_BipIface);
}

#define PROCOBJ_CLASSID Class_ID(0x2e205dbb, 0x2d724d25)

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::IsType(INode* p_Node,Class_ID ClassID,bool Strict)
{
	if(!p_Node)
		return false;

	Object* p_Object = p_Node->EvalWorldState(0).obj;

	if(p_Object == NULL)
		return false;

	Class_ID ObjClassID = p_Object->ClassID();

	//if we get any more of these ill add a generic interface...
	if(ObjClassID == PROCOBJ_CLASSID)
		Strict = true;

	if(ObjClassID == ClassID)
		return true;

	if(Strict)
		return false;

	if(p_Object->CanConvertToType(ClassID))
		return true;

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
bool rexMaxUtility::GetMaterialInfo(Mtl* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo,bool& r_IsTwoSided,bool ForceAlphaShader,u8 vertColId)
#else
bool rexMaxUtility::GetMaterialInfo(Mtl* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo,bool& r_IsTwoSided,bool ForceAlphaShader)
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
{
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	if(!GetMaterialInfoUnsafe(p_Mtl,r_MatInfo,r_IsTwoSided,ForceAlphaShader,vertColId))
#else
	if(!GetMaterialInfoUnsafe(p_Mtl,r_MatInfo,r_IsTwoSided,ForceAlphaShader))
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	{
		r_MatInfo.m_Name = atString("dummy");
		r_MatInfo.m_TypeName = RstMax::GetDefaultShaderName();
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
atString rexMaxUtility::GetTexBaseNameFromPath(const atString& r_in)
{
	return atString("");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::GetMaterialPresetInfo(Mtl* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo,MaterialPreset& materialPreset)
{
	RstMtlMax* p_RstMtl = (RstMtlMax*)p_Mtl;
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(p_RstMtl->GetShaderName());
	if ( p_Entry == NULL || p_Entry->m_MaterialPreset == NULL )
		return false;

	const MaterialPreset& basePreset = *(p_Entry->m_MaterialPreset);

	materialPreset.SetParentName(r_MatInfo.m_TypeName);
	materialPreset.SetShaderName(basePreset.GetShaderName());
	materialPreset.SetFriendlyName(NULL);
	materialPreset.SetPresetLevel(kObject);

	const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
	s32 i,NumVars = variables.GetCount();
	for(i=0;i<NumVars;i++)
	{
		TypeBase* variable = variables[i];
		TypeBase::eVariableType presetVarType = variable->GetType();
		int varIndex = variable->GetIndex();
		atString varName(variable->GetName());  //Rename this...
		atString variableName(variable->GetVariableName());  //Actual variable name.
		switch(presetVarType)
		{
		case TypeBase::kFloat:
			{
				const FloatType* baseFloatType = basePreset.GetFloat(varIndex);

				f32 val;
				p_RstMtl->GetVariable(varIndex,val);

				if ( baseFloatType->GetValue() != val || r_MatInfo.m_CollapsedMaterialPreset)
				{
					FloatType newFloatType(varName, variableName, varIndex, false, val);  
					materialPreset.AddFloat(newFloatType);
				}
			}

			break;

		case TypeBase::kTextureMap:
			{
				const TexMapType* baseTexMapType = basePreset.GetTexMap(variable->GetIndex());

				atString val;
				p_RstMtl->GetVariable(varIndex,val);
				
				if ( stricmp(baseTexMapType->GetValue().c_str(), val.c_str()) != 0 || r_MatInfo.m_CollapsedMaterialPreset)
				{
					TexMapType newTexMap(varName, variableName, varIndex, false, val, baseTexMapType->GetTextureMapType());  
					materialPreset.AddTexMap(newTexMap);
				}
			}

			break;

		case TypeBase::kVector2:
			{
				const Vector2Type* baseVector2Type = basePreset.GetVector2(variable->GetIndex());

				Vector2 val;
				Point2 retval;
				p_RstMtl->GetVariable(varIndex, val);

				if ( baseVector2Type->GetValue().IsNotEqual(val) || r_MatInfo.m_CollapsedMaterialPreset)
				{
					Vector2Type newVector2Type(varName, variableName, varIndex, false, val);  
					materialPreset.AddVector2(newVector2Type);
				}
			}

			break;

		case TypeBase::kVector3:
			{
				const Vector3Type* baseVector3Type = basePreset.GetVector3(variable->GetIndex());

				Vector3 val;
				p_RstMtl->GetVariable(varIndex,val);

				if ( baseVector3Type->GetValue().IsNotEqual(val) || r_MatInfo.m_CollapsedMaterialPreset)
				{
					Vector3Type newVector3Type(varName, variableName, varIndex, false, val); 
					materialPreset.AddVector3(newVector3Type);
				}
			}

			break;

		case TypeBase::kVector4:
			{
				const Vector4Type* baseVector4Type = basePreset.GetVector4(variable->GetIndex());

				Vector4 val;
				p_RstMtl->GetVariable(varIndex,val);

				if ( baseVector4Type->GetValue().IsNotEqual(val) || r_MatInfo.m_CollapsedMaterialPreset)
				{
					Vector4Type newVector4Type(varName, variableName, varIndex, false, val); 
					materialPreset.AddVector4(newVector4Type);
				}
			}

			break;

		default:
			break;
		}
	}

	return true;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
bool rexMaxUtility::GetMaterialInfoUnsafe(Mtl* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo,bool& r_IsTwoSided,bool ForceAlphaShader,u8 vertColId)
#else
bool rexMaxUtility::GetMaterialInfoUnsafe(Mtl* p_Mtl,rexObjectGenericMesh::MaterialInfo& r_MatInfo,bool& r_IsTwoSided,bool ForceAlphaShader)
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
{
	if(p_Mtl == NULL)
	{
#if HACK_GTA4
		r_MatInfo.m_Name = atString("dummy");
		r_MatInfo.m_TypeName = RstMax::GetDefaultShaderName();
		r_MatInfo.m_RageTypes.Reset();
		r_MatInfo.m_AttributeNames.Reset();
		r_MatInfo.m_AttributeValues.Reset();
#else

		r_MatInfo.m_Name = atString("dummy");
		r_MatInfo.m_TypeName = RstMax::GetDefaultShaderName();
		r_MatInfo.m_RageTypes.Reset();
		r_MatInfo.m_RageTypes.Resize(1);
		r_MatInfo.m_AttributeNames.Reset();
		r_MatInfo.m_AttributeNames.Resize(1);
		r_MatInfo.m_AttributeValues.Reset();
		r_MatInfo.m_AttributeValues.Resize(1);
		r_MatInfo.m_AttributeNames[0] = "DiffuseTex";
		r_MatInfo.m_RageTypes[0] = "grcTexture";
		r_MatInfo.m_AttributeValues[0] = "givemechecker";
		r_MatInfo.m_InputTextureFileNames.PushAndGrow(atString("c:/givemechecker.dds"),1);
		r_MatInfo.m_OutputTextureFileNames.PushAndGrow(atString("givemechecker.dds"),1);
#endif // HACK_GTA4
		return true;
	}



	r_MatInfo.m_Name = ReplaceWhitespaceCharacters(atString(p_Mtl->GetName()));

	if(p_Mtl->IsSubClassOf(RSTMATERIAL_CLASS_ID))
	{
		RstMtlMax* p_RstMtl = (RstMtlMax*)p_Mtl;

		r_IsTwoSided = p_RstMtl->IsTwoSided();

		grcEffect* p_Shader = NULL;
		rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(p_RstMtl->GetShaderName());
		if ( p_Entry == NULL )
			return false;

		if ( p_Entry->IsMaterialPreset() )
		{
			r_MatInfo.m_TypeName = p_RstMtl->GetShaderName();
			r_MatInfo.m_TintShaderApplied = p_RstMtl->IsTintShader();
			r_MatInfo.m_TreeShaderApplied = p_RstMtl->IsTreeShader();
			r_MatInfo.m_FacingCollapse = p_RstMtl->IsCameraOrientedShader();

			p_RstMtl->GetShaderInstanceData()->MapInstanceData( p_RstMtl, p_Entry);

			//Populate the material preset data.
			GetMaterialPresetInfo(p_RstMtl, r_MatInfo, r_MatInfo.m_MaterialPreset);

			const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
			s32 i,NumVars = variables.GetCount();

			r_MatInfo.m_RageTypes.Reset();
			r_MatInfo.m_RageTypes.Resize(NumVars);
			r_MatInfo.m_AttributeNames.Reset();
			r_MatInfo.m_AttributeNames.Resize(NumVars);
			r_MatInfo.m_AttributeValues.Reset();
			r_MatInfo.m_AttributeValues.Resize(NumVars);

			rexValue Value(atString(""));

			for(i=0;i<NumVars;i++)
			{
				TypeBase* variable = variables[i];
				TypeBase::eVariableType presetVarType = variable->GetType();

				p_RstMtl->GetShaderValue(variable->GetVariableName(),Value);

				r_MatInfo.m_AttributeNames[i] = variable->GetName();
				
				switch(presetVarType)
				{
				case TypeBase::kFloat:

					r_MatInfo.m_RageTypes[i] = "float";

					r_MatInfo.m_AttributeValues[i] = Value;

					break;

				case TypeBase::kTextureMap:

					{
						if(strcmp(Value.toString(),"") == 0)
						{
							r_MatInfo.m_RageTypes[i] = "grcTexture";
							r_MatInfo.m_AttributeValues[i] = "givemechecker";
							r_MatInfo.m_InputTextureFileNames.PushAndGrow(atString("c:/givemechecker.dds"),1);
							r_MatInfo.m_OutputTextureFileNames.PushAndGrow(atString("givemechecker.dds"),1);
							//						r_MatInfo.m_AttributeNames[i] = "";
							break;
						}

						char seperators[] = "+>";
						char buffer[8192];
						atString s,t,out;
						bool first = true;
						bool alphaBlend = false;

						strcpy(buffer,Value.toString());
						if(strchr(buffer,'+'))
							alphaBlend = true;

						char* pToken = strtok(buffer,seperators);

						while(pToken)
						{
							if(first)
							{
								first = false;
							}
							else
							{
								if(alphaBlend)
									out += "+";
								else
									out += ">";
							}

#if (MAX_RELEASE >= 12000)			
							IFileResolutionManager* pFRM = IFileResolutionManager::GetInstance();
							MaxSDK::Util::Path resolvedPath(_M(pToken));
							pFRM->GetFullFilePath(resolvedPath, MaxSDK::AssetManagement::kBitmapAsset);
							out += resolvedPath.GetCStr();
#else
							out += (TSTR)(IPathConfigMgr::GetPathConfigMgr()->GetFullFilePath(pToken,false));
#endif // (MAX_RELEASE >= MAX_RELEASE_R12)

							s += rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(pToken));

							pToken = strtok(NULL,seperators);
						}

						t = s;

						if(t != "")
							t += ".dds";
						//	t += ".bmp";

						r_MatInfo.m_InputTextureFileNames.PushAndGrow(out,1);
						r_MatInfo.m_OutputTextureFileNames.PushAndGrow(t,1);
						r_MatInfo.m_RageTypes[i] = "grcTexture";
						r_MatInfo.m_AttributeValues[i] = s;
					}

					break;

				case TypeBase::kVector2:

					r_MatInfo.m_RageTypes[i] = "Vector2";

					r_MatInfo.m_AttributeValues[i] = Value;

					break;

				case TypeBase::kVector3:

					r_MatInfo.m_RageTypes[i] = "Vector3";

					r_MatInfo.m_AttributeValues[i] = Value;

					break;

				case TypeBase::kVector4:

					r_MatInfo.m_RageTypes[i] = "Vector4";

					r_MatInfo.m_AttributeValues[i] = Value;

					break;

				default:
					break;
				}
			}

			return true;
		}
		else if(p_RstMtl->IsPreset())
		{
			r_MatInfo.m_TypeName = p_RstMtl->GetShaderName();
			p_Shader = &p_Entry->m_Preset->GetBasis();
		}
		else
		{
			r_MatInfo.m_TypeName = p_RstMtl->GetShaderName();
			p_Shader = RstMax::FindShader(r_MatInfo.m_TypeName);

			const char* p_Start = r_MatInfo.m_TypeName;
			const char* p_End = strchr(p_Start,'.');

			if(p_End)
			{
				r_MatInfo.m_TypeName.Truncate(p_End - p_Start);
			}
		}

		r_MatInfo.m_TintShaderApplied = p_RstMtl->IsTintShader();
		r_MatInfo.m_TreeShaderApplied = p_RstMtl->IsTreeShader();
		r_MatInfo.m_FacingCollapse = p_RstMtl->IsCameraOrientedShader();

		p_RstMtl->GetShaderInstanceData()->MapInstanceData( p_RstMtl, p_Entry );

		if(p_Shader == NULL)
		{
			return false;
		}

		s32 i,NumVars = p_Shader->GetInstancedVariableCount();

		r_MatInfo.m_RageTypes.Reset();
		r_MatInfo.m_RageTypes.Resize(NumVars);
		r_MatInfo.m_AttributeNames.Reset();
		r_MatInfo.m_AttributeNames.Resize(NumVars);
		r_MatInfo.m_AttributeValues.Reset();
		r_MatInfo.m_AttributeValues.Resize(NumVars);

		rexValue Value(atString(""));

		for(i=0;i<NumVars;i++)
		{
			grmVariableInfo VarInfo;
			bool Found = false;

			p_Shader->GetInstancedVariableInfo(i,VarInfo);

			Found = p_RstMtl->GetShaderValue(VarInfo,Value);

			r_MatInfo.m_AttributeNames[i] = VarInfo.m_Name;

			switch(VarInfo.m_Type)
			{
			case grcEffect::VT_FLOAT:

				r_MatInfo.m_RageTypes[i] = "float";

				r_MatInfo.m_AttributeValues[i] = Value;

				break;

			case grcEffect::VT_TEXTURE:

				{
					if(strcmp(Value.toString(),"") == 0)
					{
						r_MatInfo.m_RageTypes[i] = "grcTexture";
						r_MatInfo.m_AttributeValues[i] = "givemechecker";
						r_MatInfo.m_InputTextureFileNames.PushAndGrow(atString("c:/givemechecker.dds"),1);
						r_MatInfo.m_OutputTextureFileNames.PushAndGrow(atString("givemechecker.dds"),1);
						//						r_MatInfo.m_AttributeNames[i] = "";
						break;
					}

					char seperators[] = "+>";
					char buffer[8192];
					atString s,t,out;
					bool first = true;
					bool alphaBlend = false;

					strcpy(buffer,Value.toString());
					if(strchr(buffer,'+'))
						alphaBlend = true;

					char* pToken = strtok(buffer,seperators);

					while(pToken)
					{
						if(first)
						{
							first = false;
						}
						else
						{
							if(alphaBlend)
								out += "+";
							else
								out += ">";
						}

#if (MAX_RELEASE >= 12000)			
						IFileResolutionManager* pFRM = IFileResolutionManager::GetInstance();
						MaxSDK::Util::Path resolvedPath(_M(pToken));
						pFRM->GetFullFilePath(resolvedPath, MaxSDK::AssetManagement::kBitmapAsset);
						out += resolvedPath.GetCStr();
#else
						out += (TSTR)(IPathConfigMgr::GetPathConfigMgr()->GetFullFilePath(pToken,false));
#endif // (MAX_RELEASE >= MAX_RELEASE_R12)

						s += rexUtility::RemoveWhitespaceCharacters(rexUtility::BaseFilenameFromPath(pToken));

						pToken = strtok(NULL,seperators);
					}

					t = s;

					if(t != "")
						t += ".dds";
					//	t += ".bmp";

					r_MatInfo.m_InputTextureFileNames.PushAndGrow(out,1);
					r_MatInfo.m_OutputTextureFileNames.PushAndGrow(t,1);
					r_MatInfo.m_RageTypes[i] = "grcTexture";
					r_MatInfo.m_AttributeValues[i] = s;
				}

				break;

			case grcEffect::VT_VECTOR2:

				r_MatInfo.m_RageTypes[i] = "Vector2";

				r_MatInfo.m_AttributeValues[i] = Value;

				break;

			case grcEffect::VT_VECTOR3:

				r_MatInfo.m_RageTypes[i] = "Vector3";

				r_MatInfo.m_AttributeValues[i] = Value;

				break;

			case grcEffect::VT_VECTOR4:

				r_MatInfo.m_RageTypes[i] = "Vector4";

				r_MatInfo.m_AttributeValues[i] = Value;

				break;

			case grcEffect::VT_MATRIX43:
			case grcEffect::VT_MATRIX44:
			default:
				break;
			}
		}

		return true;
	}
	else if(p_Mtl->IsSubClassOf(REXBND_MATERIAL_CLASS_ID))
	{
		RexBoundMtlMax* p_BndMtl = (RexBoundMtlMax*)p_Mtl;

		r_MatInfo.m_Name = ":";

		atString tempName = p_BndMtl->GetCollisionName();
		if(tempName == "")
			tempName = "default";

		r_MatInfo.m_Name += tempName;

		atString procName = p_BndMtl->GetProceduralName();
#if HACK_GTA4
		r_MatInfo.m_ProcCullImmune = p_BndMtl->GetProcCullImmune();
		s32 BndFlags = p_BndMtl->GetFlags(); // Material flags e.g. Stairs, non climable etc
		s32 PopDensity = p_BndMtl->GetPopDensity(); // Pop density 
		s32 RoomID = p_BndMtl->GetRoomID(); // Milo room id
#endif // HACK_GTA4
		if(procName != "")
		{
			r_MatInfo.m_Name += "|";
			r_MatInfo.m_Name += procName;
		}
#if HACK_GTA4
		else r_MatInfo.m_Name += "|0";

		char buffer[64];
		sprintf(buffer,"|%d|%d|%d",RoomID,PopDensity,BndFlags);
		r_MatInfo.m_Name += buffer;
#endif // HACK_GTA4

#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS

#endif //HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		r_MatInfo.m_TypeName = "rage_default";

		return true;
	}
	else if(p_Mtl->IsSubClassOf(Class_ID(DMTL_CLASS_ID,0)))
	{
		StdMat* p_StdMtl = (StdMat*)p_Mtl;

		return DefShader::GetInst().GetMaterial(p_StdMtl,r_MatInfo,r_IsTwoSided,ForceAlphaShader);
	}
	else
	{
		IXRefMaterial* p_xrefMat = (IXRefMaterial*)p_Mtl->GetInterface(IID_XREF_MATERIAL);

		if(!p_xrefMat)
			return false;
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		return GetMaterialInfoUnsafe(p_xrefMat->GetSourceMaterial(true),r_MatInfo,r_IsTwoSided,ForceAlphaShader,vertColId);
#else
		return GetMaterialInfoUnsafe(p_xrefMat->GetSourceMaterial(true),r_MatInfo,r_IsTwoSided,ForceAlphaShader);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::GetMainTexMapInfo(Mtl* p_Mtl,Texmap** pp_texMap)
{
	if(!p_Mtl)
	{
		*pp_texMap = NULL;
		return true;
	}

	if(p_Mtl->NumSubTexmaps() == 0)
	{
		*pp_texMap = NULL;
		return true;
	}

	if(p_Mtl->IsSubClassOf(Class_ID(DMTL_CLASS_ID,0)))
	{
		StdMat* p_StdMtl = (StdMat*)p_Mtl;

		*pp_texMap = p_StdMtl->GetSubTexmap(ID_DI);
	}
	else
	{
		*pp_texMap = p_Mtl->GetSubTexmap(0);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Mtl* rexMaxUtility::GetFaceMtl(Mtl* p_RootMtl,MtlID FaceMtlID)
{
	if(!p_RootMtl)
	{
		return NULL;
	}

	if(!p_RootMtl->IsMultiMtl())
	{
		return p_RootMtl;
	}

	s32 NumSubs = p_RootMtl->NumSubMtls();

	if(FaceMtlID >= NumSubs)
	{
		FaceMtlID = 0;
	}

	Mtl* p_ret = p_RootMtl->GetSubMtl(FaceMtlID);

	return p_ret;
}

template<class T>
int qsortCompareInts(T* p_A,T* p_B)
{
	return (*p_A - *p_B);
}

#if HACK_GTA4
bool rexMaxUtility::GetMainTexMapsFromMaxMeshNodeRec(INode* p_Node,atArray<Texmap*>& r_TexMapList)
{
	if(!rexMaxUtility::IsType(p_Node,Class_ID(TRIOBJ_CLASS_ID,0)))
		return false;

	Object* p_Object = p_Node->EvalWorldState(0).obj;
	TriObject* p_TriObject = NULL;
	Mesh* p_Mesh = NULL;
	Mtl* p_Mtl = p_Node->GetMtl();
	p_TriObject = (TriObject*)p_Object->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID,0));

	if(p_TriObject)
		p_Mesh = &(p_TriObject->GetMesh());

	if(!p_Mesh)
		return false;

	s32 i;
	s32 SrcNumVertices = p_Mesh->numVerts;
	s32 SrcNumFaces = p_Mesh->numFaces;
	Point3 SrcVrt;

	//faces

	atMap<Mtl*,MapExportInfo> MtlMap;

	atArray<s32> MatIDs;

	for(i=0;i<SrcNumFaces;i++)
	{
		Face* p_Face = &p_Mesh->faces[i];
		s32 MatID = p_Face->getMatID();

		if(MatIDs.Find(MatID) == -1)
		{
			MatIDs.PushAndGrow(MatID);
		}
	}

	s32 Count = MatIDs.GetCount();
	MatIDs.QSort(0,Count,qsortCompareInts);

	for(i=0;i<Count;i++)
	{
		Mtl* p_FaceMtl = GetFaceMtl(p_Mtl,MatIDs[i]);
		MapExportInfo* p_LocalMatID = MtlMap.Access(p_FaceMtl);
		bool IsTwoSided = false;

		if(!p_LocalMatID)
		{
			Texmap** pp_texmap = &r_TexMapList.Grow();

			if(GetMainTexMapInfo(p_FaceMtl,pp_texmap))
			{
				MapExportInfo Set;
				Set.ID = r_TexMapList.GetCount() - 1;
				Set.IsTwoSided = IsTwoSided;
				MtlMap.Insert(p_FaceMtl,Set);
			}
			else
			{
				r_TexMapList.Pop();
			}
		}
	}

	if(p_TriObject != p_Object)
		p_TriObject->DeleteMe();

	Count = p_Node->NumberOfChildren();

	for(i=0;i<Count;i++)
	{
		GetMainTexMapsFromMaxMeshNodeRec(p_Node->GetChildNode(i),r_TexMapList);
	}

	return true;
}
#endif // HACK_GTA4

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::GetMainTexMapsFromMaxMeshNode(INode* p_Node,atArray<Texmap*>& r_TexMapList)
{
#if HACK_GTA4
	// Use a recursive function so we can loop through a node hierarchy
	r_TexMapList.Reset();

	return rexMaxUtility::GetMainTexMapsFromMaxMeshNodeRec(p_Node,r_TexMapList);
#else
	Object* p_Object = p_Node->EvalWorldState(0).obj;
	TriObject* p_TriObject = NULL;
	Mesh* p_Mesh = NULL;
	Mtl* p_Mtl = p_Node->GetMtl();
	p_TriObject = (TriObject*)p_Object->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID,0));

	r_TexMapList.Reset();

	if(p_TriObject)
		p_Mesh = &(p_TriObject->GetMesh());

	if(!p_Mesh)
		return false;

	s32 i;
	s32 SrcNumVertices = p_Mesh->numVerts;
	s32 SrcNumFaces = p_Mesh->numFaces;
	Point3 SrcVrt;

	//faces

	atMap<Mtl*,MapExportInfo> MtlMap;

	atArray<s32> MatIDs;

	for(i=0;i<SrcNumFaces;i++)
	{
		Face* p_Face = &p_Mesh->faces[i];
		s32 MatID = p_Face->getMatID();

		if(MatIDs.Find(MatID) == -1)
		{
			MatIDs.PushAndGrow(MatID);
		}
	}

	s32 Count = MatIDs.GetCount();
	MatIDs.QSort(0,Count,qsortCompareInts);

	for(i=0;i<Count;i++)
	{
		Mtl* p_FaceMtl = GetFaceMtl(p_Mtl,MatIDs[i]);
		MapExportInfo* p_LocalMatID = MtlMap.Access(p_FaceMtl);
		bool IsTwoSided = false;

		if(!p_LocalMatID)
		{
			Texmap** pp_texmap = &r_TexMapList.Grow();

			if(GetMainTexMapInfo(p_FaceMtl,pp_texmap))
			{
				MapExportInfo Set;
				Set.ID = r_TexMapList.GetCount() - 1;
				Set.IsTwoSided = IsTwoSided;
				MtlMap.Insert(p_FaceMtl,Set);
			}
			else
			{
				r_TexMapList.Pop();
			}
		}
	}

	if(p_TriObject != p_Object)
		p_TriObject->DeleteMe();

	return true;
#endif // HACK_GTA4
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::GetMaterialsFromMaxMeshNode(INode* p_Node,atArray<rexObjectGenericMesh::MaterialInfo>& r_MatList)
{
	Object* p_Object = p_Node->EvalWorldState(0).obj;
	TriObject* p_TriObject = NULL;
	Mesh* p_Mesh = NULL;
	Mtl* p_Mtl = p_Node->GetMtl();
	p_TriObject = (TriObject*)p_Object->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID,0));

	r_MatList.Reset();

	if(p_TriObject)
		p_Mesh = &(p_TriObject->GetMesh());

	if(!p_Mesh)
		return false;

	s32 i;
	s32 SrcNumVertices = p_Mesh->numVerts;
	s32 SrcNumFaces = p_Mesh->numFaces;
	Point3 SrcVrt;

	//

	atMap<Mtl*,MapExportInfo> MtlMap;

	atArray<s32> MatIDs;

	for(i=0;i<SrcNumFaces;i++)
	{
		Face* p_Face = &p_Mesh->faces[i];
		s32 MatID = p_Face->getMatID();

		if(MatIDs.Find(MatID) == -1)
		{
			MatIDs.PushAndGrow(MatID);
		}
	}

	//faces

	s32 Count = MatIDs.GetCount();
	MatIDs.QSort(0,Count,qsortCompareInts);

	for(i=0;i<Count;i++)
	{
		Mtl* p_FaceMtl = GetFaceMtl(p_Mtl,MatIDs[i]);
		MapExportInfo* p_LocalMatID = MtlMap.Access(p_FaceMtl);
		bool IsTwoSided;

		if(!p_LocalMatID)
		{
			rexObjectGenericMesh::MaterialInfo& r_MatInfo = r_MatList.Grow();

			bool ForceAlphaShader = HasAlphaVertColour(p_Node,p_FaceMtl);
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			if(GetMaterialInfo(p_FaceMtl,r_MatInfo,IsTwoSided,ForceAlphaShader,0))
#else
			if(GetMaterialInfo(p_FaceMtl,r_MatInfo,IsTwoSided,ForceAlphaShader))
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			{
				MapExportInfo Set;
				Set.ID = r_MatList.GetCount() - 1;
				Set.IsTwoSided = IsTwoSided;
				MtlMap.Insert(p_FaceMtl,Set);
			}
			else
			{
				r_MatList.Pop();
			}
		}
	}

	if(p_TriObject != p_Object)
		p_TriObject->DeleteMe();

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
ReferenceMaker* rexMaxUtility::GetMasterPointController(ReferenceMaker* p_Ref)
{
	if(!p_Ref)
	{
		return NULL;
	}

	s32 m,Count = p_Ref->NumRefs();

	if(p_Ref->SuperClassID() == CTRL_MASTERPOINT_CLASS_ID)
	{
		return p_Ref;
	}

	for(m=0;m<Count;m++)
	{
		ReferenceMaker* p_SubRef = (ReferenceMaker*)p_Ref->GetReference(m);

		p_SubRef = GetMasterPointController(p_SubRef);

		if(p_SubRef)
		{
			return p_SubRef;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void AddTime(atDList<s32>& r_KeyTimes,s32 Time)
{
	if(Time == 0)
		return;

	atDNode<s32>* p_Node = r_KeyTimes.GetHead();
	atDNode<s32>* p_NodeLast = NULL;

	while(p_Node)
	{
		if(p_Node->Data == Time)
		{
			return;
		}

		if(p_Node->Data > Time)
		{
			if(p_NodeLast)
			{
				r_KeyTimes.InsertAfter(*p_NodeLast,*(new atDNode<s32>(Time)));
				return;
			}
			else
			{
				r_KeyTimes.Prepend(*(new atDNode<s32>(Time)));
				return;
			}
		}

		p_NodeLast = p_Node;
		p_Node = p_Node->GetNext();
	}

	r_KeyTimes.Append(*(new atDNode<s32>(Time)));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::TagBlendShapeMaterials(INode* p_Node,s32 NumTargets, rexObjectGenericMesh* newObject)
{
	s32 Zeros = 0;

	for(s32 Count = 0;Count<NumTargets;Count++)
	{
		s32 i,NumVertices = newObject->m_Vertices.GetCount();

		for(i=0;i<NumVertices;i++)
		{
			Vector3 delta = newObject->m_Vertices[i].m_Position - newObject->m_BlendTargets[Count].m_GenericMesh->m_Vertices[i].m_Position;

			if(delta.Mag() > 0.01f)
			{
				int mtlCount = newObject->m_Materials.GetCount();
				for(int mtlIdx = 0; mtlIdx < mtlCount; mtlIdx++)
				{
					rexObjectGenericMesh::MaterialInfo& mtl = newObject->m_Materials[mtlIdx];

					mtl.m_BlendTargetIndices.PushAndGrow(Count);
				}
			}
			else
			{
				delta = Vector3(0.0f,0.0f,0.0f);
				Zeros++;
			}

			newObject->m_Vertices[i].m_TargetDeltas.PushAndGrow(delta);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::GetVertexKeyTimes(Object* p_Object,atDList<s32>& r_KeyTimes)
{
	Control* p_Anim = (Control*)GetMasterPointController(p_Object);
	s32 i,j;
	s32 Count;

	if(!p_Anim)
	{
		return false;
	}

	Count = p_Anim->NumSubs();

	for(i=0;i<Count;i++)
	{
		Animatable* p_SubAnim = p_Anim->SubAnim(i);

		if(!p_SubAnim)
			continue;

		if(p_SubAnim->SuperClassID() == CTRL_POINT3_CLASS_ID)
		{
			IKeyControl* p_IKC = GetKeyControlInterface(((Control*)p_SubAnim));

			if(p_IKC)
			{
				s32 NumKeys = p_IKC->GetNumKeys();
				Class_ID CClassID = p_SubAnim->ClassID();

				if(p_SubAnim->ClassID() == Class_ID(TCBINTERP_POINT3_CLASS_ID,0))
				{
					for(j=0;j<NumKeys;j++)
					{
						ITCBPoint3Key key;
						p_IKC->GetKey(j,&key);

						AddTime(r_KeyTimes,key.time);
					}
				}
				else if(p_SubAnim->ClassID() == Class_ID(HYBRIDINTERP_POINT3_CLASS_ID,0))
				{
					for(j=0;j<NumKeys;j++)
					{
						IBezPoint3Key key;
						p_IKC->GetKey(j,&key);

						AddTime(r_KeyTimes,key.time);
					}
				}
			}
		}
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
s32 AddToNormalList(rexObjectGenericMesh::BigArray<Vector3>& r_NormalList,const Vector3& r_Normal)
{
	s32 i,Count = r_NormalList.GetCount();

	for(i=0;i<Count;i++)
	{
		if(rexMaxUtility::IsMatch(r_NormalList[i],r_Normal))
		{
			return i;
		}
	}

	r_NormalList.Grow(r_NormalList.GetCount() == 0 ? 1 : r_NormalList.GetCount()) = r_Normal;

	return Count;
}

void GrowBoundingBoxRec(INode *p_Node, TimeValue timeLinePos, Box3 &boundBox)
{
	Object* p_TimedObject = p_Node->EvalWorldState(timeLinePos).obj;
	Box3 timedBoundBox;
	Matrix3 transMat = p_Node->GetObjectTM(timeLinePos);
	p_TimedObject->GetDeformBBox(timeLinePos,timedBoundBox, &transMat);
	timedBoundBox.Translate(-transMat.GetTrans());
	boundBox += timedBoundBox;

	for(int ci = 0;ci < p_Node->NumChildren(); ci++)
	{
		GrowBoundingBoxRec(p_Node->GetChildNode(ci), timeLinePos, boundBox);
	}
}

void ParseMtlIdsToCollapse(atString idListString, atArray<int> &outArray)
{
	atArray<atString> idStrings;
	idListString.Split(idStrings,',');
	atArray<atString>::iterator it = idStrings.begin();
	for(;it!=idStrings.end();it++)
	{
		atString &idString = (*it);
		int maxMtlId = atoi(idString.c_str());
		// MAD MAX wants an ID subtracted by 1...
		outArray.PushAndGrow(maxMtlId - 1, 4);
	}
}

void ExpandBB(Vector3 &min, Vector3 &max, Vector3 newpos)
{
	if(newpos.x<min.x)
		min.x = newpos.x;
	if(newpos.x>max.x)
		max.x = newpos.x;

	if(newpos.y<min.y)
		min.y = newpos.y;
	if(newpos.y>max.y)
		max.y = newpos.y;

	if(newpos.z<min.z)
		min.z = newpos.z;
	if(newpos.z>max.z)
		max.z = newpos.z;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Additional parameters r_CustomVertexColour, RoomID and ObjectSpace
bool rexMaxUtility::ConvertMaxMeshNodeIntoGenericMesh(INode* p_Node,rexObjectGenericMesh* newObject,rexResult& r_res,s32 BoneIndex,bool IsBound,bool WorldSpace,bool SkinOffset,bool KeepRotation,bool ObjectSpace,const atString& r_BoundMat,s32 Time,bool OptimiseMaterials,s32 RoomID,const atString& r_CustomVertexColour, bool ExportCollisionMeshTint, int lowerLimitVertexChannel, int forcedAdditionalUVCount, bool limitSkinToSpecFileBones, bool useWeightedNormals)
{
	if(!newObject)
	{
		r_res.Combine(rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT));
		return false;
	}

	//create a copy of the object and collapse the modifier stack...

	s32 i = 0;
	s32 j = 0;
	s32 k = 0;
	TimeValue actualTimelinePos = 0;
	if(GetProperty(p_Node, "GetTimedBoundingBox")=="true")
		actualTimelinePos = GetCOREInterface()->GetTime();
	IDerivedObject *pSkinDer = NULL;
	Modifier* p_Mod=NULL;
	ISkin* p_Skin = MaxUtil::GetSkin(p_Node);
	ISkinContextData* p_SkinData = NULL;

	// Get mesh at actual timeline pos.
	// Subsequent EvalWorldState invalidates object pointer!
	Object* p_TimedObject = p_Node->EvalWorldState(actualTimelinePos).obj;
	Box3 timedBoundBox;
	Matrix3 transMat = p_Node->GetObjectTM(actualTimelinePos);
	p_TimedObject->GetDeformBBox(actualTimelinePos,timedBoundBox, &transMat);
	timedBoundBox.Translate(-transMat.GetTrans());
	GetVector3FromPoint3(newObject->m_BoundBoxMin, timedBoundBox.Min());
	GetVector3FromPoint3(newObject->m_BoundBoxMax, timedBoundBox.Max());
	DebugPrint("****************\n%s timed bound:\nmin %f %f %f \nmax %f %f %f\n", 
		p_Node->GetName(),
		newObject->m_BoundBoxMin.x,
		newObject->m_BoundBoxMin.y,
		newObject->m_BoundBoxMin.z,
		newObject->m_BoundBoxMax.x,
		newObject->m_BoundBoxMax.y,
		newObject->m_BoundBoxMax.z);

	const ObjectState *os = &p_Node->EvalWorldState(Time);
	Object* p_Object = os->obj;
	TriObject* p_TriObject = NULL;
	Mesh locaMeshInstance;
	Mesh* p_Mesh = NULL;
	MNMesh *p_MNMesh = NULL;
	Mtl* p_Mtl = p_Node->GetMtl();
	s32 TotalBones = -1;
	Matrix34 MatInitSkin;
	Matrix34 MatInitBoneRoot;
	bool UseMeshNorms = false;

	MatInitSkin.Identity();
	MatInitBoneRoot.Identity();
	Class_ID classID = p_Object->ClassID();

	if(classID == COLLISION_MESH_CLASS_ID)
	{
		//allows automatic conversion from collision mesh
		//to standard mesh data

		p_TriObject = static_cast<TriObject*>(p_Object->GetReference(COLMESH_REF_TRIOBJ));
		p_Object = p_TriObject;
		p_Skin = NULL;
	}
	else if(p_Object->ClassID() == (Class_ID(TRIOBJ_CLASS_ID,0)))
	{
		p_TriObject = (TriObject*)p_Object;
	}
// 	else if (os->obj->IsSubClassOf(polyObjectClassID))
// 	{
// 		PolyObject *pobj = (PolyObject*)os->obj;
// 		p_MNMesh = &pobj->GetMesh();
// 		p_MNMesh->OutToTri(locaMeshInstance);
// 	}
	else if(p_Object->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID,0)))
	{
		p_TriObject = (TriObject*)p_Object->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID,0));
	}

	if(p_TriObject)
	{
		locaMeshInstance = p_TriObject->GetMesh();
		p_Mesh = &locaMeshInstance;
	}

	MeshNormalSpec* p_MeshNormalSpec = NULL;

	if(!p_Mesh)
	{
		rexResult Result;
		Result.Combine(GetWarningLevel());
		Result.AddWarning("Cannot convert %s to a mesh. Will not be exported!",p_Node->GetName());
		r_res.Combine(Result);
		return false;
	}

	if (p_Mesh->getNumFaces() <= 0)
	{
		rexResult Result;
		Result.Combine(GetWarningLevel());
		Result.AddWarning("Cannot convert %s to a mesh. Too few faces, will not be exported!",p_Node->GetName());
		r_res.Combine(Result);
		return false;
	}

	UVVert* p_StoredTintVerts = NULL;
	TVFace* p_StoredTintFaces = NULL;
	int storedTintVertCount = 0;

	s32 SrcNumVertices = p_Mesh->numVerts;
	s32 SrcNumFaces = p_Mesh->numFaces;
	atArray<s32> MatIDs;
	for(i=0;i<SrcNumFaces;i++)
	{
		Face* p_Face = &p_Mesh->faces[i];
		s32 MatID = p_Face->getMatID();

		if(MatIDs.Find(MatID) == -1)
		{
			MatIDs.PushAndGrow(MatID);
		}
	}

	s32 MtlIDCount = MatIDs.GetCount();
	MatIDs.QSort(0,MtlIDCount,qsortCompareInts);

	bool optimseMeshForCloth = rexMaxUtility::GetBoolProperty(p_Node, "optimseMeshForCloth");
	if(	p_TriObject &&
		optimseMeshForCloth &&
		NO_VERTEX_CHANNEL_CULLING!=lowerLimitVertexChannel && 
		lowerLimitVertexChannel < p_Mesh->getNumMaps())
	{
		bool hasTintMtl = false;
		for(i=0;i<MtlIDCount && !hasTintMtl;i++)
		{
			Mtl* pFaceMtl = GetFaceMtl(p_Mtl,MatIDs[i]);
			if(pFaceMtl->IsSubClassOf(RSTMATERIAL_CLASS_ID) && ((RstMtlMax*)pFaceMtl)->IsTintShader())
				hasTintMtl = true;
		}

		r_res.AddMessageCtx(p_Node->GetName(), "Culling vertex channels %d to %d for object %s.", lowerLimitVertexChannel, p_Mesh->getNumMaps()-1, p_Node->GetName());
		for(int mapindex=p_Mesh->getNumMaps()-1;mapindex>lowerLimitVertexChannel;mapindex--)
		{
			if (p_Mesh->mapSupport(mapindex))
			{
				// We need to kill the extra map channels but cache the data first
				// so we can apply the tint colour without needing the map channel.
				if (mapindex == MESH_TINT_COLOR && hasTintMtl)
				{
					storedTintVertCount = p_Mesh->getNumMapVerts(mapindex);
					p_StoredTintVerts = new UVVert[storedTintVertCount];
					p_StoredTintFaces = new TVFace[p_Mesh->getNumFaces()];
					
					TVFace* mapFaces = p_Mesh->mapFaces(MESH_TINT_COLOR);
					UVVert* mapVerts = p_Mesh->mapVerts(MESH_TINT_COLOR);

					ZeroMemory(p_StoredTintVerts, sizeof(UVVert) * storedTintVertCount);
					ZeroMemory(p_StoredTintFaces, sizeof(TVFace) * p_Mesh->getNumFaces());

					memcpy(p_StoredTintVerts, mapVerts, sizeof(UVVert) * storedTintVertCount);
					memcpy(p_StoredTintFaces, mapFaces, sizeof(TVFace) * p_Mesh->getNumFaces());
				}
				
				p_Mesh->setNumMapVerts(mapindex, 0);
				p_Mesh->setMapSupport(mapindex, FALSE);
			}
		}

		p_Mesh->setNumMaps(lowerLimitVertexChannel-1,TRUE);	

		newObject->m_optimize = false;
	}

	if(IsBound == false)
	{
		p_MeshNormalSpec = p_Mesh->GetSpecifiedNormals();
		// Additional check on number of faces in the MeshNormalSpec
		if(p_MeshNormalSpec && p_MeshNormalSpec->GetNumNormals() && p_MeshNormalSpec->GetNumFaces())
		{
			p_MeshNormalSpec->BuildNormals();
			UseMeshNorms = true;
		}
	}

	if(p_Skin)
	{
		p_SkinData = p_Skin->GetContextInterface(p_Node);
		INode* p_SkinRootBone = MaxUtil::GetSkinRootBone(p_Skin);

		if(!p_SkinRootBone)
		{
			r_res.Combine(rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT));
			r_res.AddErrorCtx(p_Node->GetName(), "Skin modifier on node %s does not have a valid root bone!", p_Node->GetName());
			return false;
		}

		INode* p_Parent = rexMaxUtility::GetParent(p_SkinRootBone);

		if(p_Parent)
		{
			rexMaxUtility::CopyMaxMatrixToRageMatrix(rexMaxUtility::GetParent(p_SkinRootBone)->GetNodeTM(0),MatInitBoneRoot);
		}
		else
		{
			MatInitBoneRoot.Identity();
		}

		TSTR name = p_SkinRootBone->GetName();

		rexConverterMaxSkeleton::MaxSkeletonIterator ItBone(p_SkinRootBone, p_Skin, limitSkinToSpecFileBones);
		TotalBones = ItBone.GetObjectCount();

		// Unused.  Marked for deletion
		Matrix3 MaxMatInitBone;
		p_Skin->GetBoneInitTM(p_Node,MaxMatInitBone);
		MaxMatInitBone.Orthogonalize();

		Matrix3 MaxMatInitSkin;
		p_Skin->GetSkinInitTM(p_Node,MaxMatInitSkin);

		MaxMatInitSkin.Orthogonalize();

		rexMaxUtility::CopyMaxMatrixToRageMatrix(MaxMatInitSkin,MatInitSkin);
		MatInitSkin.Identity3x3();
	}

	newObject->SetMeshName(rexMaxUtility::ReplaceWhitespaceCharacters(p_Node->GetName()));

	// Get property to determine to write mesh to fragment section of .type file only
	// Used on instanced verhicle components such as wheels and chopper rotor blades
	// Physics have agreed to discuss whether we need this for the future
	TSTR Prop;

	if(p_Node->GetUserPropString("fragonly",Prop) && (stricmp(Prop,"true") == 0))
		//newObject->m_WriteToFragmentOnly = true;
		newObject->AddExclusiveTypeGroup(atString("fragments"));

	newObject->m_FullPath = p_Node->GetName();
	newObject->m_ParentBoneMatrix.Identity();
	newObject->m_IsSkinned = (p_Skin != NULL);
	newObject->m_Matrix.Identity();

	if(!p_Skin)
	{
		newObject->m_BoneIndex = BoneIndex;
	}

	Matrix34 MatTrans;
	Matrix34 MatLocal;

	CopyMaxMatrixToRageMatrix(p_Node->GetObjectTM(0),MatTrans);
	CopyMaxMatrixToRageMatrix(p_Node->GetNodeTM(0),MatLocal);

	// If we're working in object space set MatLocal to nodes ultimate
	// parent nodes TM
	if(ObjectSpace)
	{	
		INode* p_RootNode = GetCOREInterface()->GetRootNode();
		INode* p_OctreeRootParent = p_Node;

		while(p_OctreeRootParent->GetParentNode() != p_RootNode)
		{
			p_OctreeRootParent = p_OctreeRootParent->GetParentNode();
		}

		CopyMaxMatrixToRageMatrix(p_OctreeRootParent->GetNodeTM(0),MatLocal);
	}
	else
	{
		if(KeepRotation)
		{
			MatLocal.Identity3x3();
		}
	}

	Box3 SrcBoundBox;
	p_Object->GetDeformBBox(0,SrcBoundBox);

	Point3 SrcCenter;
	SrcCenter = SrcBoundBox.Center();

	newObject->m_Center = Vector3(SrcCenter[0],SrcCenter[1],SrcCenter[2]);
	newObject->m_PivotPoint = Vector3(0.0f,0.0f,0.0f);

	atString strLodGroup = rexMaxUtility::GetProperty(p_Node, "lodgroup");
	if(strcmp("",strLodGroup.c_str()))
		newObject->m_LODGroupIndex = atoi(strLodGroup);

	atString strSpecialFlags = rexMaxUtility::GetProperty(p_Node, "specialFlags");
	atArray<atString> flags;
	strSpecialFlags.Split(flags, "|", true);
	atArray<atString>::iterator it;
	for(it = flags.begin();it!=flags.end();it++)
		newObject->AddSpecialFlag((*it));


	atString strTypeGroups = rexMaxUtility::GetProperty(p_Node, "exclusiveTypeGroups");
	atArray<atString> groops;
	strTypeGroups.Split(groops, "|", true);
	atArray<atString>::iterator it2;
	for(it2 = groops.begin();it2!=groops.end();it2++)
		newObject->AddExclusiveTypeGroup((*it2));

	newObject->m_LODLevel = atoi(rexMaxUtility::GetProperty(p_Node,"lod"));

	atString strEmpty = atString("");
	atString strThreshold = rexMaxUtility::GetProperty(p_Node,"lodthreshold");
	if (strThreshold != strEmpty)
	{
		newObject->m_LODThreshold = (float)(atoi(strThreshold));
	}
	atString strOutThreshold = rexMaxUtility::GetProperty(p_Node,"lodoutthreshold");
	if (strcmp(strOutThreshold.c_str(),""))
	{
		newObject->m_LODOUTThreshold = (float)(atoi(strOutThreshold));
	}
	atString strOptimize = rexMaxUtility::GetProperty(p_Node,"optimize");
	if(strOptimize == "false")
		newObject->m_optimize = false;

	#define PROPERTY_ID_RENDERABLE_MESH "renderableMesh"
	atString renderableMesh = rexMaxUtility::GetProperty(p_Node, PROPERTY_ID_RENDERABLE_MESH);
	if(renderableMesh != strEmpty)
	{
		DebugPrint("%s\n", renderableMesh.c_str());
		//newObject->m_RenderableMesh = FindNode(p_Node, renderableMesh);
	}

	bool flipUVVerts[NUM_UV_FLIPCHANNELS] = {0};
	for(int i=0;i<NUM_UV_FLIPCHANNELS;i++)
	{
		char buffer[8];
		_itoa(i, buffer, 10);
		atString searchString(atString("rexFlipUV"), (const char *)buffer);
		flipUVVerts[i] = rexMaxUtility::GetBoolProperty(p_Node, searchString.c_str());
	}

	atString fragmentOffsetString = rexMaxUtility::GetProperty(p_Node, "fragmentOffset");
	if(fragmentOffsetString != strEmpty)
	{
		atArray<atString> parts;
		fragmentOffsetString.Split(parts, ",");
		if(parts.GetCount()!=3)
		{
			r_res.AddError("Mesh %s's UDP fragmentOffset could not be parsed into a three component vector!", p_Node->GetName());
		}
		else
		{
			newObject->m_FragmentRootOffset = Vector3(
					atof(parts[0]),
					atof(parts[1]),
					atof(parts[2])
				);
		}
	}

	s32 OutputFaces = 0;
	Point3 SrcVrt;

	if((SrcNumVertices == 0) || (SrcNumFaces == 0))
	{
		rexResult Result;
		Result.Combine(GetWarningLevel());
		Result.AddWarning("Mesh %s doesnt have any vertices and/or faces. Cannot Export!",(const char*)newObject->GetMeshName());
		r_res.Combine(Result);
		return false;
	}

	if(p_Skin && (p_SkinData->GetNumPoints() > SrcNumVertices))
	{
		rexResult Result;
		Result.Combine(GetWarningLevel());
		Result.AddWarning("Mesh %s's skin has %d points it influences, although the mesh only has %d verts.",(const char*)newObject->GetMeshName(), p_SkinData->GetNumPoints(), SrcNumVertices);
		r_res.Combine(Result);
		return false;
	}

	atString collapseBBIDString = rexMaxUtility::GetProperty(p_Node, "collapseBBVerts");
	atArray<int> mtldIdsToCollapse;
	if(strEmpty != collapseBBIDString)
	{
		ParseMtlIdsToCollapse(collapseBBIDString, mtldIdsToCollapse);
	}

	atString renderBucketMaskString = rexMaxUtility::GetProperty(p_Node, "renderBucketMask");
	if(strEmpty != renderBucketMaskString)
	{
		atArray<atString> bucketBits;
		renderBucketMaskString.Split(bucketBits, '|', true);
		atArray<atString>::iterator bitIt = bucketBits.begin();
		for(;bitIt!=bucketBits.end();++bitIt)
		{
			bool unsetBit = false;
			const char *bitString = (*bitIt).c_str();
			int flagbit = atoi(bitString);
			if(0!=flagbit)
				newObject->m_RenderBucketMask = flagbit;
			else
			{
				if(bitString[0]=='!')
				{
					unsetBit = true;
					bitString = &bitString[1];
				}
				int flagbit = GetRexMeshBucketBit(bitString);
				if(flagbit==-1 || flagbit>0xFF)
				{
					r_res.AddErrorCtx(p_Node->GetName(),"Invlid render flag \"%s\" given for mesh %s.", bitString, p_Node->GetName());
				}

				if(unsetBit)
					newObject->m_RenderBucketMask &= ~((u16)flagbit);
				else
					newObject->m_RenderBucketMask |= (u16)flagbit;
			}
		}
	}


	//vertices

	Point3* pntList = new Point3[SrcNumVertices];
	rexConverterMaxSkeleton::MaxSkeletonIterator::FlushCachedData();
	for(i=0;i<SrcNumVertices;i++)
	{
		rexObjectGenericMesh::VertexInfo& r_VertInfo = newObject->m_Vertices.Grow(SrcNumVertices);
		SrcVrt = p_Mesh->verts[i];
		r_VertInfo.m_Position.Set(SrcVrt[0],SrcVrt[1],SrcVrt[2]);
		r_VertInfo.m_SecondSurfaceDisplacement = 0.0f;

		MatTrans.Transform(r_VertInfo.m_Position);

		if(!WorldSpace)
		{
			MatLocal.UnTransform(r_VertInfo.m_Position);
		}

		//temporary storage for normal creation
		pntList[i] = Point3(r_VertInfo.m_Position.x,r_VertInfo.m_Position.y,r_VertInfo.m_Position.z);

		if(SkinOffset)
		{
			MatInitSkin.Transform(r_VertInfo.m_Position);
			MatInitBoneRoot.UnTransform(r_VertInfo.m_Position);
		}

		//skinning bone and weight info
		if(p_Skin)
		{
			s32 NumBones = p_SkinData->GetNumAssignedBones(i);
			INode* p_BoneNode = MaxUtil::GetSkinRootBone(p_Skin);
			rexConverterMaxSkeleton::MaxSkeletonIterator It(p_BoneNode, p_Skin, limitSkinToSpecFileBones);

			if(NumBones>mshMaxMatricesPerVertex)
			{
				r_res.AddMessage("vert %d has more than %d influences",i,mshMaxMatricesPerVertex);
			}

			int higherboneNum = Max(NumBones, TotalBones);
			r_VertInfo.m_BoneIndices.Reserve(higherboneNum);
			r_VertInfo.m_BoneIndices.Resize(higherboneNum);
			r_VertInfo.m_BoneWeights.Reserve(higherboneNum);
			r_VertInfo.m_BoneWeights.Resize(higherboneNum);

			float TotalWeight = 0.0f;

			for(j=0;j<TotalBones;j++)
			{
				r_VertInfo.m_BoneIndices[j] = j;
				r_VertInfo.m_BoneWeights[j] = 0.0f;
			}

			for(j=0;j<NumBones;j++)
			{
				TotalWeight += p_SkinData->GetBoneWeight(i,j);
			}

			for(j=0;j<NumBones;j++)
			{
				s32 BoneID = It.GetBoneOffset(p_Skin,p_Skin->GetBone(p_SkinData->GetAssignedBone(i,j)));
				f32 Weight = p_SkinData->GetBoneWeight(i,j);

				r_VertInfo.m_BoneIndices[j] = BoneID;
				r_VertInfo.m_BoneWeights[j] = Weight / TotalWeight;
			}
		}
	}

	//skinning matrices
	if(p_Skin)
	{
		INode* p_BoneNode = MaxUtil::GetSkinRootBone(p_Skin);
		MCHAR *boneName = p_BoneNode->GetName();
		r_res.AddMessageCtx(boneName, "resolved to root bone %s", boneName);
		rexConverterMaxSkeleton::MaxSkeletonIterator It(p_BoneNode, p_Skin, limitSkinToSpecFileBones);

		s32 NumBones = It.GetObjectCount();
		newObject->m_SkinBoneMatrices.Resize(NumBones);
		newObject->m_SkinBoneMatrices.Resize(NumBones);

		i=0;
		p_BoneNode = MaxUtil::GetSkinRootBone(p_Skin);
		It = rexConverterMaxSkeleton::MaxSkeletonIterator(p_BoneNode, p_Skin, limitSkinToSpecFileBones);
		p_BoneNode = It.GetNextObject();
		while(p_BoneNode)
		{
			rexMaxUtility::CopyMaxMatrixToRageMatrix(p_BoneNode->GetNodeTM(0),newObject->m_SkinBoneMatrices[i]);
			i++;
			p_BoneNode = It.GetNextObject();
		}
	}

	// I think this looks to scan through detecting nan's and setting them to the next valid point
	for(i=0;i<SrcNumVertices;i++)
	{
		if(pntList[i] != pntList[i])
		{
			bool Found = false;

			for(j=i;j<SrcNumVertices;j++)
			{
				if(pntList[j] == pntList[j])
				{
					Found = true;
					pntList[i] = pntList[j];
				}
			}

			if(Found == false)
			{
				for(j=0;j<i;j++)
				{
					if(pntList[j] == pntList[j])
					{
						Found = true;
						pntList[i] = pntList[j];
					}
				}				
			}

			if(Found == false)
			{
				rexResult Result;
				Result.Combine(GetWarningLevel());
				Result.AddWarning("Couldn't find valid vertex to set!",p_Node->GetName());
				r_res.Combine(Result);
				return false;
			}
		}
	}

	RexMaxNormals Normals(*p_Mesh,pntList,useWeightedNormals);
	delete[] pntList;

	//normals
	if(UseMeshNorms == false)
	{
		s32 SrcNmlCount = Normals.GetNumVertNormals();

		for(i=0;i<SrcNmlCount;i++)
		{
			Vector3 VecGet;
			Normals.GetVertNormal(i,VecGet);
			VecGet.Normalize();

			newObject->m_Normals.Grow(SrcNmlCount) = VecGet;
		}
	}

	int vertColCount = p_Mesh->getNumVertCol();
	bool HasColVerts = p_Mesh->getNumVertCol() == 0 ? false : true;

	// Vertex illumination, vertex alpha and a custom vertex colour
	bool HasIllumVerts = ((p_Mesh->mapSupport(MAP_SHADING) == 0) || (p_Mesh->mapVerts(MAP_SHADING) == NULL) || (p_Mesh->mapFaces(MAP_SHADING) == NULL)) ? false : true;
	bool HasAlphaVerts = ((p_Mesh->mapSupport(MAP_ALPHA) == 0) || (p_Mesh->mapVerts(MAP_ALPHA) == NULL) || (p_Mesh->mapFaces(MAP_ALPHA) == NULL)) ? false : true;
	bool HasColour1Verts = ((p_Mesh->mapSupport(MAP_COLOR1) == 0) || (p_Mesh->mapVerts(MAP_COLOR1) == NULL) || (p_Mesh->mapFaces(MAP_COLOR1) == NULL)) ? false : true;
	bool HasColour2Verts = ((p_Mesh->mapSupport(MAP_COLOR2) == 0) || (p_Mesh->mapVerts(MAP_COLOR2) == NULL) || (p_Mesh->mapFaces(MAP_COLOR2) == NULL)) ? false : true;
	bool HasColour3Verts = ((p_Mesh->mapSupport(MAP_COLOR3) == 0) || (p_Mesh->mapVerts(MAP_COLOR3) == NULL) || (p_Mesh->mapFaces(MAP_COLOR3) == NULL)) ? false : true;
	bool HasColour3AlphaVerts = ((p_Mesh->mapSupport(MAP_COLOR3_ALPHA) == 0) || (p_Mesh->mapVerts(MAP_COLOR3_ALPHA) == NULL) || (p_Mesh->mapFaces(MAP_COLOR3_ALPHA) == NULL)) ? false : true;
	
	bool HasMeshTintVerts = ((p_Mesh->mapSupport(MESH_TINT_COLOR) == 0) || (p_Mesh->mapVerts(MESH_TINT_COLOR) == NULL) || (p_Mesh->mapFaces(MESH_TINT_COLOR) == NULL)) ? false : true;
	bool HasEnvVerts = ((p_Mesh->mapSupport(PED_ENV) == 0) || (p_Mesh->mapVerts(PED_ENV) == NULL) || (p_Mesh->mapFaces(PED_ENV) == NULL)) ? false : true;
	bool HasWeightGainVerts = ((p_Mesh->mapSupport(PED_WEIGHT_GAIN) == 0) || (p_Mesh->mapVerts(PED_WEIGHT_GAIN) == NULL) || (p_Mesh->mapFaces(PED_WEIGHT_GAIN) == NULL)) ? false : true;

	/*
	bool HasMorphEyeVerts = ((p_Mesh->mapSupport(MAP_MORPH_EYES) == 0) || (p_Mesh->mapVerts(MAP_MORPH_EYES) == NULL) || (p_Mesh->mapFaces(MAP_MORPH_EYES) == NULL)) ? false : true;
	bool HasMorphNoseVerts = ((p_Mesh->mapSupport(MAP_MORPH_NOSE) == 0) || (p_Mesh->mapVerts(MAP_MORPH_NOSE) == NULL) || (p_Mesh->mapFaces(MAP_MORPH_NOSE) == NULL)) ? false : true;
	bool HasMorphMouthVerts = ((p_Mesh->mapSupport(MAP_MORPH_MOUTH) == 0) || (p_Mesh->mapVerts(MAP_MORPH_MOUTH) == NULL) || (p_Mesh->mapFaces(MAP_MORPH_MOUTH) == NULL)) ? false : true;
	bool HasMorphEarsVerts = ((p_Mesh->mapSupport(MAP_MORPH_EARS) == 0) || (p_Mesh->mapVerts(MAP_MORPH_EARS) == NULL) || (p_Mesh->mapFaces(MAP_MORPH_EARS) == NULL)) ? false : true;
	*/

	bool WarnOnce = true;


	UVVert* p_AlphaVerts = p_Mesh->mapVerts(MAP_ALPHA);
	TVFace* p_AlphaFaces = p_Mesh->mapFaces(MAP_ALPHA);
	int AlphaVertCount = p_Mesh->getNumMapVerts(MAP_ALPHA);

	UVVert* p_IllumVerts = p_Mesh->mapVerts(MAP_SHADING);
	TVFace* p_IllumFaces = p_Mesh->mapFaces(MAP_SHADING);
	int IllumVertCount = p_Mesh->getNumMapVerts(MAP_SHADING);

	UVVert* p_Color1Verts = p_Mesh->mapVerts(MAP_COLOR1);
	TVFace* p_Color1Faces = p_Mesh->mapFaces(MAP_COLOR1);
	int Color1VertCount = p_Mesh->getNumMapVerts(MAP_COLOR1);

	UVVert* p_Color2Verts = p_Mesh->mapVerts(MAP_COLOR2);
	TVFace* p_Color2Faces = p_Mesh->mapFaces(MAP_COLOR2);
	int Color2VertCount = p_Mesh->getNumMapVerts(MAP_COLOR2);

	UVVert* p_Color3Verts = p_Mesh->mapVerts(MAP_COLOR3);
	TVFace* p_Color3Faces = p_Mesh->mapFaces(MAP_COLOR3);
	int Color3VertCount = p_Mesh->getNumMapVerts(MAP_COLOR3);

	UVVert* p_Color3AlphaVerts = p_Mesh->mapVerts(MAP_COLOR3_ALPHA);
	TVFace* p_Color3AlphaFaces = p_Mesh->mapFaces(MAP_COLOR3_ALPHA);
	int Color3AlphaVertCount = p_Mesh->getNumMapVerts(MAP_COLOR3_ALPHA);

	UVVert* p_MeshTintVerts = p_Mesh->mapVerts(MESH_TINT_COLOR);
	TVFace* p_MeshTintFaces = p_Mesh->mapFaces(MESH_TINT_COLOR);
	int MeshTintVertCount = p_Mesh->getNumMapVerts(MESH_TINT_COLOR);

	UVVert* p_EnvVerts = p_Mesh->mapVerts(PED_ENV);
	TVFace* p_EnvFaces = p_Mesh->mapFaces(PED_ENV);
	int EnvVertCount = p_Mesh->getNumMapVerts(PED_ENV);

	UVVert* p_WeightGainVerts = p_Mesh->mapVerts(PED_WEIGHT_GAIN);
	TVFace* p_WeightGainFaces = p_Mesh->mapFaces(PED_WEIGHT_GAIN);
	int WeightGainVertCount = p_Mesh->getNumMapVerts(PED_WEIGHT_GAIN);

	/*
	UVVert* p_MorphEyesVerts = p_Mesh->mapVerts(MAP_MORPH_EYES);
	TVFace* p_MorphEyesFaces = p_Mesh->mapFaces(MAP_MORPH_EYES);
	int MorphEyesVertCount = p_Mesh->getNumMapVerts(MAP_MORPH_EYES);

	UVVert* p_MorphNoseVerts = p_Mesh->mapVerts(MAP_MORPH_NOSE);
	TVFace* p_MorphNoseFaces = p_Mesh->mapFaces(MAP_MORPH_NOSE);
	int MorphNoseVertCount = p_Mesh->getNumMapVerts(MAP_MORPH_NOSE);

	UVVert* p_MorphMouthVerts = p_Mesh->mapVerts(MAP_MORPH_MOUTH);
	TVFace* p_MorphMouthFaces = p_Mesh->mapFaces(MAP_MORPH_MOUTH);
	int MorphMouthVertCount = p_Mesh->getNumMapVerts(MAP_MORPH_MOUTH);

	UVVert* p_MorphEarsVerts = p_Mesh->mapVerts(MAP_MORPH_EARS);
	TVFace* p_MorphEarsFaces = p_Mesh->mapFaces(MAP_MORPH_EARS);
	int MorphEarsVertCount = p_Mesh->getNumMapVerts(MAP_MORPH_EARS);
	*/

	if(!p_Mesh->vcFace)
		HasColVerts = false;


	if(Color3VertCount != Color3AlphaVertCount)
	{
		HasColour3AlphaVerts = false;
		r_res.Combine(rexResult::WARNINGS);
		r_res.AddWarning("CPV3 data in channels 16 and 17 is not of same length. Not using alpha values.", p_Node->GetName());
	}

	//faces

	////////////////////////////////////////////////////////////////////////////
	//NEW CODE TO FORCE ORDER OF MESH TO BE THE SAME AS A MULTIMATERIALS USAGE//
	////////////////////////////////////////////////////////////////////////////
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	struct UniqueMtl
	{
	public:
		Mtl* p_FaceMtl;
		int ColIdx;
	};
	atMap<UniqueMtl*,MapExportInfo> MtlMap;
#else
	atMap<Mtl*,MapExportInfo> MtlMap;
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS


	if(!IsBound)
	{
		for(i=0;i<MtlIDCount;i++)
		{
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			UniqueMtl *pUniq = new UniqueMtl;
			
			pUniq->ColIdx = 0;
			pUniq->p_FaceMtl = GetFaceMtl(p_Mtl,MatIDs[i]);
#else
			Mtl* p_FaceMtl = GetFaceMtl(p_Mtl,MatIDs[i]);	
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			bool IsTwoSided = false;
			MapExportInfo LocalMatInfo;
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			MapExportInfo* p_LocalMatID = MtlMap.Access(pUniq);
#else
			MapExportInfo* p_LocalMatID = MtlMap.Access(p_FaceMtl);
#endif

			if(!p_LocalMatID)
			{
				rexObjectGenericMesh::MaterialInfo MatIn;
				MatIn.m_DCCMtlID = MatIDs[i];

				//look to see if any of the faces that use this material have alpha verts...
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
				bool ForceAlphaShader = HasAlphaVertColour(p_Node,pUniq->p_FaceMtl);
				if(GetMaterialInfo(pUniq->p_FaceMtl,MatIn,IsTwoSided,ForceAlphaShader,0))
#else
				bool ForceAlphaShader = HasAlphaVertColour(p_Node,p_FaceMtl);
				if(GetMaterialInfo(p_FaceMtl,MatIn,IsTwoSided,ForceAlphaShader))
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
				{
					s32 Count = newObject->m_Materials.GetCount();
					bool Found = false;

					if(OptimiseMaterials)
					{
						for(j=0;j<Count;j++)
						{
							if(newObject->m_Materials[j].IsSame(MatIn))
							{
								Found = true;
								break;
							}
						}
					}

					if(Found)
					{
						LocalMatInfo.ID = j;
						LocalMatInfo.IsTwoSided = IsTwoSided;
					}
					else
					{
						rexObjectGenericMesh::MaterialInfo& r_MatInfo = newObject->m_Materials.Grow();
						r_MatInfo = MatIn;

						LocalMatInfo.ID = newObject->m_Materials.GetCount() - 1;
						LocalMatInfo.IsTwoSided = IsTwoSided;

						int foundIndex = mtldIdsToCollapse.Find(MatIDs[i]);
						bool billboardsMeshWithSingleMtl = p_Node->GetMtl() && !p_Node->GetMtl()->IsMultiMtl() && mtldIdsToCollapse.size()>0;			
						if(foundIndex != -1 || billboardsMeshWithSingleMtl)
						{
							newObject->m_Materials[LocalMatInfo.ID].m_BillboardCollapse = true;
						}
					}
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
					MtlMap.Insert(pUniq,LocalMatInfo);
#else
					MtlMap.Insert(p_FaceMtl,LocalMatInfo);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
				}
				else
				{
					LocalMatInfo.ID = 0;
					LocalMatInfo.IsTwoSided = false;
				}
			}
		}
	}

	///////////////////
	//END OF NEW CODE//
	///////////////////
	// This is where things start to get messy with the head.  
	if(p_Mesh->faces != NULL)
	{
		Object* p_TempObject = p_Node->EvalWorldState(0).obj;
		Mesh* p_TempMesh = NULL;
		TriObject* p_TempTriObject = NULL;
		
		Matrix3 pivotTranform;
		Matrix34 pivotTranformRage;
		p_Node->GetObjOffsetRot().MakeMatrix(pivotTranform);
		rexMaxUtility::CopyMaxMatrixToRageMatrix(pivotTranform,pivotTranformRage);

		// LPXO: Push this processing into the post process modifier
		for(i=0;i<SrcNumFaces;i++)
		{
			Face* p_Face = &p_Mesh->faces[i];
			TVFace* p_VCFace = &p_Mesh->vcFace[i];
			const RexMaxNormals::VNFace* p_VNFace = Normals.GetVertNormalFace(i);
			bool IsTwoSided = false;

#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			UniqueMtl* uniqueMtl = new UniqueMtl();
			
			uniqueMtl->ColIdx = 0;
			uniqueMtl->p_FaceMtl = GetFaceMtl(p_Mtl,p_Face->getMatID());
			/* LPXO: Dont store the collision index here..
			if(IsBound && palIdxList.GetCount() == SrcNumFaces)
			{
				// Add one to the index since zero is default
				uniqueMtl->ColIdx = palIdxList[i] + 1;
			}
			*/
#else
			Mtl* p_FaceMtl = GetFaceMtl(p_Mtl,p_Face->getMatID());
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS

			// Use alternative collision material if any one side of the poly
			// is less than 1.5 in length
			bool AltCollisionMat = false;

			Vector3 VecA = newObject->m_Vertices[p_Face->v[0]].m_Position;
			Vector3 VecB = newObject->m_Vertices[p_Face->v[1]].m_Position;
			Vector3 VecC = newObject->m_Vertices[p_Face->v[2]].m_Position;
			Vector3 VecX = VecA - VecB;
			Vector3 VecY = VecB - VecC;

			if(IsBound)
			{
				Vector3 VecZ = VecA - VecC;
				float LengthX = VecX.Mag();
				float LengthY = VecY.Mag();
				float LengthZ = VecZ.Mag();
				// LPXO: Prevents procedural types being applied to small polys
				// by dynamically generating a new material with the procedural
				// type zero'd out. 
				if((LengthX < 1.5f) || (LengthY < 1.5f) || (LengthZ < 1.5f))
					AltCollisionMat = true;
			}


#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			if(!uniqueMtl->p_FaceMtl && p_Mtl && p_Mtl->IsMultiMtl() && WarnOnce)
#else
			if(!p_FaceMtl && p_Mtl && p_Mtl->IsMultiMtl() && WarnOnce)
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			{
				WarnOnce = false;
				r_res.AddMessageCtx(newObject->GetMeshName().c_str(), "Mesh %s uses submap %d but there are only %d submaps",(const char*)newObject->GetMeshName(),p_Face->getMatID() + 1,p_Mtl->NumSubMtls());
			}

			bool BadPoly = false;
			for(j=0;j<3;j++)
			{
				s32 VertIndex = p_Face->v[j];

				if(VertIndex >= newObject->m_Vertices.GetCount())
				{
					r_res.AddMessageCtx(newObject->GetMeshName(), "Mesh %s has bad face, discarding. Face %d, Vert %d",(const char*)newObject->GetMeshName(),i,VertIndex);
					BadPoly = true;
				}
				else if(newObject->m_Vertices[VertIndex].m_Position != newObject->m_Vertices[VertIndex].m_Position)
				{
					r_res.AddMessageCtx(newObject->GetMeshName(), "Mesh %s has bad face, discarding. Face %d, Vert %d",(const char*)newObject->GetMeshName(),i,VertIndex);
					BadPoly = true;
				}
			}

			if(BadPoly)
				continue;

			rexObjectGenericMesh::PrimitiveInfo& r_PrimInfo = newObject->m_Primitives.Grow(SrcNumFaces * 2);
			r_PrimInfo.m_FaceNormal = Vector3(0.0f,0.0f,0.0f);

			//materials
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			MapExportInfo* p_LocalMatID = NULL;

			atMap<UniqueMtl*,MapExportInfo>::Iterator mtlMapIterator = MtlMap.CreateIterator();
			mtlMapIterator.Start();


			while( !mtlMapIterator.AtEnd() )
			{

				UniqueMtl* thisUnique = mtlMapIterator.GetKey();
				MapExportInfo  thisLocalMat = mtlMapIterator.GetData();
				
				if(thisUnique->p_FaceMtl == uniqueMtl->p_FaceMtl && thisUnique->ColIdx == uniqueMtl->ColIdx)
				{
					p_LocalMatID = &thisLocalMat;
					break;
				}
				mtlMapIterator.Next();

			}
#else
			MapExportInfo* p_LocalMatID = MtlMap.Access(p_FaceMtl);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			MapExportInfo LocalMatInfo;
			
			LocalMatInfo.ID = -1;
			LocalMatInfo.AltID = -1;
			LocalMatInfo.IsTwoSided = false;

#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			LocalMatInfo.ColIdx = 0;
			if(IsBound && ((uniqueMtl->p_FaceMtl == NULL) || (uniqueMtl->p_FaceMtl->IsSubClassOf(REXBND_MATERIAL_CLASS_ID) == FALSE)))
#else
			if(IsBound && ((p_FaceMtl == NULL) || (p_FaceMtl->IsSubClassOf(REXBND_MATERIAL_CLASS_ID) == FALSE)))
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			{
				if(newObject->m_Materials.GetCount() < 1)
				{
					rexObjectGenericMesh::MaterialInfo& r_MatInfo = newObject->m_Materials.Grow();

					r_MatInfo.m_Name = ":";
					r_MatInfo.m_Name += r_BoundMat;
					r_MatInfo.m_TypeName = "rage_default";
				}

				LocalMatInfo.ID = 0;
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
				LocalMatInfo.ColIdx = uniqueMtl->ColIdx;
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS

				LocalMatInfo.AltID = 0;
				LocalMatInfo.IsTwoSided = false;
			}
			else
			{
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
				if(p_LocalMatID && LocalMatInfo.ColIdx == p_LocalMatID->ColIdx) 
#else
				if(p_LocalMatID) 
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
				{
					LocalMatInfo = *p_LocalMatID;
				}
				else
				{
					rexObjectGenericMesh::MaterialInfo MatIn;

					//look to see if any of the faces that use this material have alpha verts...
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
					bool ForceAlphaShader = HasAlphaVertColour(p_Node,uniqueMtl->p_FaceMtl);
#else
					bool ForceAlphaShader = HasAlphaVertColour(p_Node,p_FaceMtl);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS

#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
					if(GetMaterialInfo(uniqueMtl->p_FaceMtl,MatIn,IsTwoSided,ForceAlphaShader,uniqueMtl->ColIdx))
#else
					if(GetMaterialInfo(p_FaceMtl,MatIn,IsTwoSided,ForceAlphaShader))
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
					{
						s32 Count = newObject->m_Materials.GetCount();
						bool Found = false;

						if(OptimiseMaterials)
						{
							for(j=0;j<Count;j++)
							{
								if(newObject->m_Materials[j].IsSame(MatIn))
								{
									if(!IsBound || newObject->m_Materials[j].m_Name == MatIn.m_Name)
									{
										Found = true;
										break;
									}
								}
							}
						}

						if(Found)
						{
							LocalMatInfo.ID = j;
							LocalMatInfo.IsTwoSided = IsTwoSided;
						}
						else
						{
							rexObjectGenericMesh::MaterialInfo& r_MatInfo = newObject->m_Materials.Grow();
							r_MatInfo = MatIn;

							LocalMatInfo.ID = newObject->m_Materials.GetCount() - 1;
							LocalMatInfo.AltID = -1;
							LocalMatInfo.IsTwoSided = IsTwoSided;

							int foundIndex = mtldIdsToCollapse.Find(p_Face->getMatID());
							bool billboardsMeshWithSingleMtl = p_Node->GetMtl() && !p_Node->GetMtl()->IsMultiMtl() && mtldIdsToCollapse.size()>0;							
							if(foundIndex != -1 || billboardsMeshWithSingleMtl)
							{
								newObject->m_Materials[LocalMatInfo.ID].m_BillboardCollapse = true;
							}
						}
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
						MtlMap.Insert(uniqueMtl,LocalMatInfo);
#else
						MtlMap.Insert(p_FaceMtl,LocalMatInfo);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
					}
					else
					{
						LocalMatInfo.ID = 0;
						LocalMatInfo.AltID = 0;
						LocalMatInfo.IsTwoSided = false;
					}
				}
			}
			bool IsIllumExemptShader = false;

			if(IsBound && AltCollisionMat)
				//if(IsBound)
			{
				if(LocalMatInfo.AltID == -1)
				{
					rexObjectGenericMesh::MaterialInfo MatIn;
					MatIn = newObject->m_Materials[LocalMatInfo.ID];

					char buffer[1024];
					strcpy(buffer,MatIn.m_Name);
					const char* name = strtok(buffer,"|");
					const char* procname = strtok(NULL,"|");
					const char* roomid = strtok(NULL,"|");
					const char* pop = strtok(NULL,"|");
					const char* bndflags = strtok(NULL,"|");

					if((name || procname || roomid || pop || bndflags) && !MatIn.m_ProcCullImmune)
					{
						sprintf(buffer,"%s|0|%s|%s|%s",name,roomid,pop,bndflags);
						
						MatIn.m_Name = buffer;
						
						s32 Count = newObject->m_Materials.GetCount();

						for(j=0;j<Count;j++)
						{
							if(newObject->m_Materials[j].m_Name == MatIn.m_Name)
							{
								LocalMatInfo.AltID = j;
								break;
							}
						}

						if(LocalMatInfo.AltID == -1)
						{
							newObject->m_Materials.Grow() = MatIn;
							LocalMatInfo.AltID = newObject->m_Materials.GetCount() - 1;
						}
					}
					else
						LocalMatInfo.AltID = LocalMatInfo.ID;
				}

				r_PrimInfo.m_MaterialIndex = LocalMatInfo.AltID;
			}

			else
				r_PrimInfo.m_MaterialIndex = LocalMatInfo.ID;

			s32 AttrCount = newObject->m_Materials[LocalMatInfo.ID].m_AttributeNames.GetCount();

			for(j=0;j<AttrCount;j++)
			{
				if( stricmp( newObject->m_Materials[LocalMatInfo.ID].m_AttributeNames[j],"RiverFoamTex" ) == 0 )
					IsIllumExemptShader = true;
			}

			if(LocalMatInfo.ID >= 0)
			{
				rexObjectGenericMesh::MaterialInfo& matInfo = newObject->m_Materials[LocalMatInfo.ID];
				matInfo.m_PrimitiveIndices.PushAndGrow(OutputFaces);
				if(uniqueMtl->p_FaceMtl)
					matInfo.m_Material = GetRageShaderMtlInterface(uniqueMtl->p_FaceMtl);
				OutputFaces++;
			}

			s32 NumTexMaps = p_Mesh->getNumMaps();

			//vertices
			for(j=0;j<3;j++)
			{
				rexObjectGenericMesh::AdjunctInfo& r_AdjunctInfo = r_PrimInfo.m_Adjuncts.Grow(1);

				r_AdjunctInfo.m_VertexIndex = p_Face->v[j];

				if(UseMeshNorms)
				{
					Point3 CurrNormal = p_MeshNormalSpec->Normal(p_MeshNormalSpec->Face(i).GetNormalID(j)).Normalize();
					
					Vector3 normalIn = Vector3(CurrNormal.x,CurrNormal.y,CurrNormal.z);
					Vector3 normalOut;
					
					pivotTranformRage.Transform(normalIn, normalOut);

					if (normalOut.IsZero())
					{
						rexResult Result;
						Result.AddWarning("Mesh %s has a zero normal!",(const char*)newObject->GetMeshName());
						Result.Combine(GetWarningLevel());
						r_res.Combine(Result);
					}

					r_AdjunctInfo.m_NormalIndex = AddToNormalList(newObject->m_Normals,Vector3(normalOut.x,normalOut.y,normalOut.z));
				}
				else
				{
					r_AdjunctInfo.m_NormalIndex = p_VNFace->v[j];

					if (newObject->m_Normals[r_AdjunctInfo.m_NormalIndex].IsZero())
					{
						rexResult Result;
						Result.AddWarning("Mesh %s has a zero normal!",(const char*)newObject->GetMeshName());
						Result.Combine(GetWarningLevel());
						r_res.Combine(Result);
					}

					if(r_AdjunctInfo.m_NormalIndex > newObject->m_Normals.GetCount())
					{
						rexResult Result;
						Result.AddWarning("Mesh %s has adjunct whose normal index is too large for array!  Cannot Export!",(const char*)newObject->GetMeshName());
						Result.Combine(GetWarningLevel());
						r_res.Combine(Result);
						return false;
					}
				}

				if(r_AdjunctInfo.m_VertexIndex > SrcNumVertices)
				{
					rexResult Result;
					Result.AddWarning("Mesh %s has adjunct whose vert index is too large for array!  Cannot Export!",(const char*)newObject->GetMeshName());
					Result.Combine(GetWarningLevel());
					r_res.Combine(Result);
					return false;
				}

			
				// Our extra per vert bound data.  Mesh tint, scaling and density becomes part of a per face material when processed after mesh combining.
				if ( IsBound )
				{
					
					newObject->m_Vertices[p_Face->v[j]].m_SecondSurfaceDisplacement = 0.0f;
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE	
					s32 NumTexMaps = p_Mesh->getNumMaps();
					for(k=1;k<NumTexMaps;k++)
					{
						if ( k == MAP_SECOND_SURFACE )
						{
							TVFace* p_tvFace = p_Mesh->mapFaces(k);
							VertColor* p_VertCol = ( VertColor* )p_Mesh->mapVerts( k );
							if( p_VertCol )
							{
								VertColor vertCol = p_VertCol[p_tvFace[i].t[j]];

								float secondsurfaceDisp = 1.0f - (( vertCol.x + vertCol.y + vertCol.z ) / 3.0f); // Black is max height and white is minimum
								if( InRange(secondsurfaceDisp, 0.0001f, 1.0f) )//&& secondsurfaceDisp > SMALL_FLOAT )	
									newObject->m_Vertices[p_Face->v[j]].m_SecondSurfaceDisplacement = secondsurfaceDisp;
								else
									newObject->m_Vertices[p_Face->v[j]].m_SecondSurfaceDisplacement = 0.0f;
							}
						}
					}
#endif // HACK_GTA4_BOUND_GEOM_SECOND_SURFACE
					if(p_TempObject->ClassID() == COLLISION_MESH_CLASS_ID)
					{
						//allows automatic conversion from collision mesh
						//to standard mesh data

						p_TempTriObject = static_cast<TriObject*>(p_TempObject->GetReference(COLMESH_REF_TRIOBJ));
						p_TempObject = p_TempTriObject;
					}
					if(p_TempTriObject)
						p_TempMesh = &(p_TempTriObject->GetMesh());

					if(p_TempMesh)
					{
						bool HasScaleVerts = ((p_TempMesh->mapSupport(MAP_BOUND_SCALE) == 0) || (p_TempMesh->mapVerts(MAP_BOUND_SCALE) == NULL) || (p_TempMesh->mapFaces(MAP_BOUND_SCALE) == NULL)) ? false : true;
						bool HasCollisionMeshTint = ((p_TempMesh->mapSupport(MAP_BOUND_TINT_COLOUR) == 0) || (p_TempMesh->mapVerts(MAP_BOUND_TINT_COLOUR) == NULL) || (p_TempMesh->mapFaces(MAP_BOUND_TINT_COLOUR) == NULL)) ? false : true;


						if(ExportCollisionMeshTint && HasCollisionMeshTint)
						{

							VertColor* p_VertCol = (VertColor*)p_TempMesh->mapVerts(MAP_BOUND_TINT_COLOUR);
							TVFace* p_Face = p_TempMesh->mapFaces(MAP_BOUND_TINT_COLOUR);

							if( p_VertCol && p_Face )
							{
								VertColor vertCol = p_VertCol[p_Face[i].t[j]];
								r_AdjunctInfo.m_TintColour = Vector4(vertCol.x, vertCol.y, vertCol.z, 0.0f);
							}
						}
						else
						{
							r_AdjunctInfo.m_TintColour = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
						}
						
						if(HasScaleVerts)
						{
							
							VertColor* p_VertCol = (VertColor*)p_TempMesh->mapVerts(MAP_BOUND_SCALE);
							TVFace* p_Face = p_TempMesh->mapFaces(MAP_BOUND_SCALE);

							if( p_VertCol && p_Face )
							{
								VertColor vertCol = p_VertCol[p_Face[i].t[j]];							
								r_AdjunctInfo.m_ProceduralScaleZ = vertCol.x;
								r_AdjunctInfo.m_ProceduralScaleXYZ = vertCol.y;
								r_AdjunctInfo.m_ProceduralDensity = vertCol.z;
							}
						}
						else
						{
							// Since we later convert this to a 4 bit integer, our default needs to be correct
							r_AdjunctInfo.m_ProceduralScaleXYZ = 8.0f/15.0f;
							r_AdjunctInfo.m_ProceduralScaleZ = 1.0f;
							r_AdjunctInfo.m_ProceduralDensity = 1.0f;
						}
					}
				}


				if(IsBound == false)
				{
					atString& colourSetName = r_AdjunctInfo.m_ColorSetNames.Grow();
					colourSetName = "Default";
					Vector4 &color0 = r_AdjunctInfo.m_Colors.Grow();
					color0.Set(1.0f,1.0f,1.0f,1.0f);

					if(HasColVerts)
					{
						color0.x = p_Mesh->vertCol[p_VCFace->t[j]][0];
						color0.y = p_Mesh->vertCol[p_VCFace->t[j]][1];
						color0.z = p_Mesh->vertCol[p_VCFace->t[j]][2];
					}


					//bit of a hack, but the beginnings of a generic rule system
					//for custom vertex colours...

					if(HasIllumVerts && !IsIllumExemptShader && (r_CustomVertexColour == "illum.y>colour.y?shadvar[#all]=EmissiveMultiplier"))
					{
						s32 VertIndex = p_IllumFaces[i].t[j];

						if(VertIndex < IllumVertCount)
							color0.y = p_IllumVerts[VertIndex][1];
					}
					//else
					//	color0.y = 0.0f; // LPXO: We dont want to flood the emissive channel and fuck up lookig through milo portals so force to zero

					if(HasEnvVerts && p_EnvFaces && p_EnvVerts) // We really need to use a system like ChannelsToExport here
					{
						s32 VertIndex = p_EnvFaces[i].t[j];

						if(VertIndex < EnvVertCount)
							color0.z = p_EnvVerts[VertIndex][2];
					}

					if(color0.x != color0.x)
						color0.x = 1.0f;

					if(color0.y != color0.y)
						color0.y = 1.0f;

					if(color0.z != color0.z)
						color0.z = 1.0f;

					if(color0.w != color0.w)
						color0.w = 1.0f;

					if(HasAlphaVerts && p_AlphaFaces && p_AlphaVerts)
					{
						int Index = p_AlphaFaces[i].t[j];

						if((Index >= 0) && (Index < AlphaVertCount))
						{
							UVVert Alpha = p_AlphaVerts[Index];
							color0.w = Alpha[0];
						}
					}

					r_AdjunctInfo.m_ColorSetNames.PushAndGrow(atString("Default"));
					Vector4 &color1 = r_AdjunctInfo.m_Colors.Grow();
					color1.Set(1.0f,1.0f,1.0f,1.0f);

					// Color1 channel for shaders
					if(HasColour1Verts && p_Color1Faces && p_Color1Verts)
					{
						s32 VertIndex = p_Color1Faces[i].t[j];

						if(VertIndex < Color1VertCount)
						{
							color1.x = p_Color1Verts[VertIndex][0];
							color1.y = p_Color1Verts[VertIndex][1];
							color1.z = p_Color1Verts[VertIndex][2];
						}
					}
					else if(HasColour2Verts)
					{
						s32 VertIndex = p_Color2Faces[i].t[j];

						if(VertIndex < Color2VertCount)
						{
							color1.x = p_Color2Verts[VertIndex][0];
							color1.y = p_Color2Verts[VertIndex][1];
							color1.z = p_Color2Verts[VertIndex][2];
						}
					}
					
					/*
					// Morph region
					if (HasMorphEyeVerts || HasMorphNoseVerts || HasMorphMouthVerts || HasMorphEarsVerts)
					{
						u8 morphTargetRegionBits = 0x0;

						if (HasMorphEyeVerts)
						{
							s32 VertIndex = p_MorphEyesFaces[i].t[j];
							if (p_MorphEyesVerts[VertIndex][0] > 0 || p_MorphEyesVerts[VertIndex][1] > 0 || p_MorphEyesVerts[VertIndex][2] > 0)
							{
								morphTargetRegionBits |= MAP_MORPH_EYES_BIT;
							}
						}
						if (HasMorphNoseVerts)
						{
							s32 VertIndex = p_MorphNoseFaces[i].t[j];
							if (p_MorphNoseVerts[VertIndex][0] > 0 || p_MorphNoseVerts[VertIndex][1] > 0 || p_MorphNoseVerts[VertIndex][2] > 0)
							{
								morphTargetRegionBits |= MAP_MORPH_NOSE_BIT;
							}
						}
						if (HasMorphMouthVerts)
						{
							s32 VertIndex = p_MorphMouthFaces[i].t[j];
							if (p_MorphMouthVerts[VertIndex][0] > 0 || p_MorphMouthVerts[VertIndex][1] > 0 || p_MorphMouthVerts[VertIndex][2] > 0)
							{
								morphTargetRegionBits |= MAP_MORPH_MOUTH_BIT;
							}
						}
						if (HasMorphEarsVerts)
						{
							s32 VertIndex = p_MorphEarsFaces[i].t[j];
							if (p_MorphEarsVerts[VertIndex][0] > 0 || p_MorphEarsVerts[VertIndex][1] > 0 || p_MorphEarsVerts[VertIndex][2] > 0)
							{
								morphTargetRegionBits |= MAP_MORPH_EARS_BIT;
							}
						}

						color1.z = (float)(morphTargetRegionBits / 255.0f);
					}
					*/

					Vector4 &color2 = r_AdjunctInfo.m_Colors.Grow();
					color2.Set(1.0f,1.0f,1.0f,1.0f);
					if(HasColour3Verts && p_Color3Faces && p_Color3Verts)
					{
						s32 VertIndex = p_Color3Faces[i].t[j];

						if(VertIndex < Color3VertCount)
						{
							color2.x = p_Color3Verts[VertIndex][0];
							color2.y = p_Color3Verts[VertIndex][1];
							color2.z = p_Color3Verts[VertIndex][2];
							if(	HasColour3AlphaVerts && 
								p_Color3AlphaFaces && 
								p_Color3AlphaVerts && 
								VertIndex < Color3AlphaVertCount)
							{
								color2.w = p_Color3AlphaVerts[VertIndex][0];
							}
						}
					}

					if(HasWeightGainVerts && p_WeightGainVerts && p_WeightGainFaces) // We really need to use a system like ChannelsToExport here
					{
						s32 VertIndex = p_WeightGainFaces[i].t[j];

						if(VertIndex < WeightGainVertCount)
							color1.w = p_WeightGainVerts[VertIndex][2];
					}

					Vector4 &tintColour = r_AdjunctInfo.m_TintColour;
					tintColour.Set(1.0f, 1.0f, 1.0f, 1.0f);

					if(HasMeshTintVerts)
					{
						s32 VertIndex = p_MeshTintFaces[i].t[j];

						if(VertIndex < MeshTintVertCount)
						{
							if (p_MeshTintVerts[VertIndex][0] <= 1.0f && p_MeshTintVerts[VertIndex][0] >= 0.0f)
								tintColour.x = p_MeshTintVerts[VertIndex][0];
							else
								tintColour.x = 0.0f;
							if (p_MeshTintVerts[VertIndex][1] <= 1.0f && p_MeshTintVerts[VertIndex][1] >= 0.0f)
								tintColour.y = p_MeshTintVerts[VertIndex][1];
							else
								tintColour.y = 0.0f;
							if (p_MeshTintVerts[VertIndex][2] <= 1.0f && p_MeshTintVerts[VertIndex][2] >= 0.0f)
								tintColour.z = p_MeshTintVerts[VertIndex][2];
							else
								tintColour.z = 0.0f;
						}

					}
					else if (p_StoredTintFaces != NULL && p_StoredTintVerts != NULL && storedTintVertCount != 0)
					{
						s32 VertIndex = p_StoredTintFaces[i].t[j];

						if(VertIndex < storedTintVertCount)
						{
							if (p_StoredTintVerts[VertIndex][0] <= 1.0f && p_StoredTintVerts[VertIndex][0] >= 0.0f)
								tintColour.x = p_StoredTintVerts[VertIndex][0];
							else
								tintColour.x = 0.0f;
							if (p_StoredTintVerts[VertIndex][1] <= 1.0f && p_StoredTintVerts[VertIndex][1] >= 0.0f)
								tintColour.y = p_StoredTintVerts[VertIndex][1];
							else
								tintColour.y = 0.0f;
							if (p_StoredTintVerts[VertIndex][2] <= 1.0f && p_StoredTintVerts[VertIndex][2] >= 0.0f)
								tintColour.z = p_StoredTintVerts[VertIndex][2];
							else
								tintColour.z = 0.0f;
						}
					}

					if(color1.x != color1.x)
						color1.x = 1.0f;

					if(color1.y != color1.y)
						color1.y = 1.0f;

					if(color1.z != color1.z)
						color1.z = 1.0f;

					if(color1.w != color1.w)
						color1.w = 1.0f;
					
					color0.x = Clamp(color0.x, 0.0f, 1.0f);
					color0.y = Clamp(color0.y, 0.0f, 1.0f);
					color0.z = Clamp(color0.z, 0.0f, 1.0f);

					color1.x = Clamp(color1.x, 0.0f, 1.0f);
					color1.y = Clamp(color1.y, 0.0f, 1.0f);
					color1.z = Clamp(color1.z, 0.0f, 1.0f);

					rexObjectGenericMesh::MaterialInfo &primMtlInfo = newObject->m_Materials[LocalMatInfo.ID];
					s32 NumMaps = primMtlInfo.m_InputTextureFileNames.GetCount();
					if(NumMaps > 8)
					{
						NumMaps = 8;
					}

					for(k=1;k<NumTexMaps;k++)
					{
						//uv's should only be exported out when there are enough maps
						//being exported for the shader...

						if(k < (NumMaps + (forcedAdditionalUVCount+1)))
						{
							UVVert* p_UV = p_Mesh->mapVerts(k);
							TVFace* p_Face = p_Mesh->mapFaces(k);
							Point2 SrcTexCoord;

							if(!p_UV || !p_Face)
							{
								SrcTexCoord[0] = 1.0f;
								SrcTexCoord[1] = 1.0f;
							}
							else
							{
								SrcTexCoord[0] = p_UV[p_Face[i].t[j]].x;
								SrcTexCoord[1] = p_UV[p_Face[i].t[j]].y;
							}

							Vector2& r_VecUV = r_AdjunctInfo.m_TextureCoordinates.Grow(1);

							r_VecUV.x = SrcTexCoord[0];
							r_VecUV.y = SrcTexCoord[1];

							if(flipUVVerts[k])
								r_VecUV.y = 1.0f - r_VecUV.y;
						}
					}
				}
			}

			r_PrimInfo.m_FaceNormal.Cross(VecX,VecY);

			if(LocalMatInfo.IsTwoSided)
			{
				if(LocalMatInfo.ID >= 0)
				{
					rexObjectGenericMesh::MaterialInfo& matInfo = newObject->m_Materials[LocalMatInfo.ID];
					matInfo.m_PrimitiveIndices.PushAndGrow(OutputFaces,SrcNumFaces);
					OutputFaces++;
				}

				rexObjectGenericMesh::PrimitiveInfo& r_PrimInfoSecond = newObject->m_Primitives.Grow(SrcNumFaces);

				r_PrimInfoSecond.m_Adjuncts.Reset();
				r_PrimInfoSecond.m_Adjuncts.Resize(3);
				r_PrimInfoSecond.m_Adjuncts[0] = r_PrimInfo.m_Adjuncts[2];
				r_PrimInfoSecond.m_Adjuncts[1] = r_PrimInfo.m_Adjuncts[1];
				r_PrimInfoSecond.m_Adjuncts[2] = r_PrimInfo.m_Adjuncts[0];
				r_PrimInfoSecond.m_MaterialIndex = r_PrimInfo.m_MaterialIndex;
				r_PrimInfoSecond.m_FaceNormal = -r_PrimInfo.m_FaceNormal;

				r_PrimInfoSecond.m_Adjuncts[0].m_NormalIndex = newObject->m_Normals.GetCount();
				Vector3& AddNormalA = newObject->m_Normals.Grow(newObject->m_Normals.GetCount());
				AddNormalA = -newObject->m_Normals[r_PrimInfo.m_Adjuncts[2].m_NormalIndex];

				r_PrimInfoSecond.m_Adjuncts[1].m_NormalIndex = newObject->m_Normals.GetCount();
				Vector3& AddNormalB = newObject->m_Normals.Grow(newObject->m_Normals.GetCount());
				AddNormalB = -newObject->m_Normals[r_PrimInfo.m_Adjuncts[1].m_NormalIndex];

				r_PrimInfoSecond.m_Adjuncts[2].m_NormalIndex = newObject->m_Normals.GetCount();
				Vector3& AddNormalC = newObject->m_Normals.Grow(newObject->m_Normals.GetCount());
				AddNormalC = -newObject->m_Normals[r_PrimInfo.m_Adjuncts[0].m_NormalIndex];
			}
		}
	}

	if (p_StoredTintFaces)
	{
		delete [] p_StoredTintFaces;
		p_StoredTintFaces = NULL;
	}

	if (p_StoredTintVerts)
	{
		delete [] p_StoredTintVerts;
		p_StoredTintVerts = NULL;
	}

	// More scanning for nan's this time in th newObjects vertex positions
	for(i=0;i<SrcNumVertices;i++)
	{
		if(newObject->m_Vertices[i].m_Position != newObject->m_Vertices[i].m_Position)
		{
			bool Found = false;

			for(j=i;j<SrcNumVertices;j++)
			{
				if(newObject->m_Vertices[j].m_Position == newObject->m_Vertices[j].m_Position)
				{
					Found = true;
					newObject->m_Vertices[i].m_Position = newObject->m_Vertices[j].m_Position;
				}
			}

			if(Found == false)
			{
				for(j=0;j<i;j++)
				{
					if(newObject->m_Vertices[j].m_Position == newObject->m_Vertices[j].m_Position)
					{
						Found = true;
						newObject->m_Vertices[i].m_Position = newObject->m_Vertices[j].m_Position;
					}
				}				
			}

			if(Found == false)
			{
				rexResult Result;
				Result.Combine(GetWarningLevel());
				Result.AddWarning("Couldn't find valid vertex to set!",p_Node->GetName());
				r_res.Combine(Result);
				return false;
			}
		}
	}

	bool retval = true;

	if(!IsBound)
	{
		for(i=0;i<newObject->m_Materials.GetCount();i++)
		{
			// LPXO:  The old limit of 21,845 has been rmeoved because ragebuilder will take care
			//			of splitting meshes itself.  I've stuck a ceilng of 250k in which we can 
			//			change or data drive in the future.
			rexObjectGenericMesh::MaterialInfo &primMtlInfo = newObject->m_Materials[i];
			if(primMtlInfo.m_PrimitiveIndices.GetCount() >= 250000)
			{
				rexResult Result;
				Result.AddError("Mesh %s has a material ( %s ) that uses over 250,000 faces", 
					(const char*)newObject->GetMeshName(), (const char*)newObject->m_Materials[i].m_Name);
				Result.Combine(rexResult::ERRORS);
				r_res.Combine(Result);
				retval = false;			
			}

			// expanding the bounding box for billboards
			if(primMtlInfo.m_BillboardCollapse)
			{
				if((primMtlInfo.m_PrimitiveIndices.GetCount()%2) > 0)
				{
					r_res.Combine(rexResult::ERRORS);
					r_res.AddErrorCtx(p_Node->GetName(), "Billboard material %s on object %s has uneven count of primitives. Expecting quads!", primMtlInfo.m_Name, p_Node->GetName());
					continue;
				}
				Point4 billboardShaderScaleVar(1.0f,1.0f,1.0f,1.0f);
				IRageShaderMaterial *pShader = primMtlInfo.GetMaterial();
				if(pShader)
				{
					char *pShaderName = pShader->FpGetShaderName();
					for (int varIndex = 0; varIndex<pShader->FpGetVariableCount();varIndex++)
					{
						if(0==strcmp(pShader->FpGetVariableName(varIndex).data(), "BB Width/Height"))
						{
							billboardShaderScaleVar = pShader->FpGetVector4VariableValue(varIndex);
						}
					}
				}
				for(int primIt = 0; primIt < primMtlInfo.m_PrimitiveIndices.GetCount()-1; primIt += 2)
				{
					// calculating the center of this and the next face, which form the quad.
					Vector3 faceCenter(0.0f,0.0f,0.0f);
					int numVerts = 0;
					Vector2 scaleUVs(1.0f,1.0f);
					for(int quadFaceIndexOffset=0;quadFaceIndexOffset<2;quadFaceIndexOffset++)
					{
						int primIndex = primMtlInfo.m_PrimitiveIndices[primIt+quadFaceIndexOffset];
						rexObjectGenericMesh::PrimitiveInfo& r_PrimInfo = newObject->m_Primitives[primIndex];
						for(atArray<rexObjectGenericMesh::AdjunctInfo>::iterator adjIt= r_PrimInfo.m_Adjuncts.begin();
							adjIt != r_PrimInfo.m_Adjuncts.end();
							adjIt++)
						{
							faceCenter = faceCenter + newObject->m_Vertices[(*adjIt).m_VertexIndex].m_Position;
							if((*adjIt).m_TextureCoordinates.GetCount()<=BILLBOARD_SCALE_UVCHANNEL)
							{
								continue;
							}
							Vector2 vertUvScale = (*adjIt).m_TextureCoordinates[BILLBOARD_SCALE_UVCHANNEL];
							// meh, have been flipped. flip back - tex map channel count 1 based ffs.
							if(flipUVVerts[BILLBOARD_SCALE_UVCHANNEL+1])
								vertUvScale.y = (1.0 - vertUvScale.y);
							if(	(scaleUVs.x!=1.0f || scaleUVs.x!=1.0f) && 
								(scaleUVs.x!=vertUvScale.x && scaleUVs.y!=vertUvScale.y))
							{
								r_res.Combine(rexResult::WARNINGS);
								r_res.AddWarningCtx(p_Node->GetName(), "Vertices on billboard object %s's triangle number %d have varying scale information in UVs.", p_Node->GetName(), p_Node->GetName());
							}
							scaleUVs = vertUvScale;
							numVerts++;
						}
					}
					faceCenter = faceCenter/numVerts;
					// calculating the largest distance the billboard vertices can rotate to. That's the distance centre to edgevert on the billboard.
					Vector2 faceScale = Vector2(billboardShaderScaleVar.x, billboardShaderScaleVar.y);
					faceScale = faceScale * scaleUVs;
					float maxOffset = sqrt(faceScale.x*faceScale.x+faceScale.y*faceScale.y);
					Vector3 offsetvec(maxOffset,maxOffset,maxOffset);
					ExpandBB(newObject->m_BoundBoxMin, newObject->m_BoundBoxMax, faceCenter-offsetvec);
					ExpandBB(newObject->m_BoundBoxMin, newObject->m_BoundBoxMax, faceCenter+offsetvec);
				}
			}
		}
	}

	if(p_TriObject != p_Object)
	{
		delete p_TriObject;
	}

	return retval;
}

bool GetMapDataErrors(int channel, Mesh *p_Mesh, MCHAR *context, rexResult &result)
{
	Point3 reference;
	atArray<Point3> errorColors;
	bool returnValue=true;
	for(int i=0;i<p_Mesh->getNumMapVerts(channel);i++)
	{
		Point3 &currData = p_Mesh->mapVerts(channel)[i];
		if(0==i)
			reference = currData;
		// vert color differs to initial vert and is not in array yet.
		if (reference != currData &&
			-1==errorColors.Find(currData) )
		{
			errorColors.PushAndGrow(currData*255, 8);
		}
	}
	if(errorColors.GetCount()>0)
	{
		result.Combine( rexResult::WARNINGS );
		char vertString[2048] = "";
		for(int k=0;k<errorColors.GetCount();k++)
		{
			returnValue = false;
			if(k>0)
				strcat_s(vertString, 2048, ", ");
			char buffer[255];
			sprintf(buffer, "#%X%X%X", (int)errorColors[k].x,(int)errorColors[k].y,(int)errorColors[k].z);
			strcat_s(vertString, 2048, buffer);
		}
		result.AddWarningCtx(context, "CPV #%d: (https://devstar.rockstargames.com/wiki/index.php/Cloth_Export_Errors#Multiple_colours_on_object)\nError colours: %s", channel, vertString);
	}
	return returnValue;
}

// only return false on actual errorness processing
bool rexMaxUtility::CheckMaxMeshContinuinity(INode* p_Node, rexResult &result)

{
	if(!p_Node)
		return true;

	Object* p_Object = p_Node->GetObjectRef()->Eval(0).obj;// EvalWorldState(0).obj;

	if(!p_Object->CanConvertToType(triObjectClassID) )
		return true;

	TriObject* p_TriObject = (TriObject*)p_Object->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID,0));

	if(!p_TriObject)
		return true;

	Mesh* p_Mesh = &(p_TriObject->GetMesh());
	MCHAR *nodeName = p_Node->GetName();

	bool returnValue = true;

	// UV check
	if(p_Mesh->numVerts != p_Mesh->numTVerts)
	{
		result.Combine( rexResult::ERRORS );
		result.AddErrorCtx(nodeName, "UV vert count differs to mesh vert count. https://devstar.rockstargames.com/wiki/index.php/Cloth_Export_Errors#UV_vert_count_differs_to_mesh_vert_count");
		returnValue = false;
	}
	// Seek through verts for evilness
	atArray<int> errorVerts;
	p_Mesh->buildNormals();
	for(int i=0;i<p_Mesh->numVerts;i++)
	{
		if(p_Mesh->getRVert(i).ern!=NULL)
		{
			errorVerts.PushAndGrow(i, 8);
		}
	}
	// smoothing group check
	if(errorVerts.GetCount()>0)
	{
		result.Combine( rexResult::ERRORS );
		MSTR vertString("");
		for(int k=0;k<errorVerts.GetCount();k++)
		{
			if(k>0)
				vertString = vertString.Append(", ");
			char buffer[255];
			itoa(errorVerts[k],buffer,10);
			vertString = vertString.Append(buffer);
		}
		result.AddErrorCtx(nodeName, "Vertices are shared by multiple smoothing groups (https://devstar.rockstargames.com/wiki/index.php/Cloth_Export_Errors#Vertices_are_shared_by_multiple_smoothing_groups):  %s", vertString.data());
		returnValue = false;
	}

	// CPV check
	for(int checkChannel = -NUM_HIDDENMAPS;checkChannel<=0;checkChannel++)
	{
		if(!GetMapDataErrors(checkChannel, p_Mesh, nodeName, result))
			returnValue = false;
	}

	return returnValue;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::CopyMaxMatrixToRageMatrix(const Matrix3& r_SrcMat,Matrix34& r_DstMat)
{
	Point4 SrcRow;

	SrcRow = r_SrcMat.GetRow(0);
	r_DstMat.a = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(1);
	r_DstMat.b = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(2);
	r_DstMat.c = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
	SrcRow = r_SrcMat.GetRow(3);
	r_DstMat.d = Vector3(SrcRow[0],SrcRow[1],SrcRow[2]);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::CopyRageMatrixToMaxMatrix(const Matrix34& r_SrcMat,Matrix3& r_DstMat)
{
	Point3 SrcRow;

	SrcRow = Point3(r_SrcMat.a.x,r_SrcMat.a.y,r_SrcMat.a.z);
	r_DstMat.SetRow(0,SrcRow);
	SrcRow = Point3(r_SrcMat.b.x,r_SrcMat.b.y,r_SrcMat.b.z);
	r_DstMat.SetRow(1,SrcRow);
	SrcRow = Point3(r_SrcMat.c.x,r_SrcMat.c.y,r_SrcMat.c.z);
	r_DstMat.SetRow(2,SrcRow);
	SrcRow = Point3(r_SrcMat.d.x,r_SrcMat.d.y,r_SrcMat.d.z);
	r_DstMat.SetRow(3,SrcRow);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::MessageBoxOK(const char* info, const atString& title)
{
	MessageBox(GetCOREInterface()->GetMAXHWnd(),title,info,MB_OK);
}

static HWND m_hWnd = NULL;
static atArray<HWND> m_listProg;
static atArray<HWND> m_listInfo;

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::OpenProgressWindow(const char *title,int numValues)
{
	//const s32 CONTROLHEIGHT = 20;
	s32 CONTROLHEIGHT = 20;
	const s32 WINDOWWIDTH = 700;
	const s32 BORDERSIZE = 10;

	if(m_hWnd)
	{
		return true;
	}

	HINSTANCE hInstance = GetModuleHandle(NULL);

	static bool bRunOnce = false;

	if(!bRunOnce)
	{
		WNDCLASS wc;

		wc.style         = CS_OWNDC;
		wc.lpfnWndProc   = DefWindowProc;
		wc.cbClsExtra    = 0;
		wc.cbWndExtra    = 0;
		wc.hInstance     = hInstance;
		wc.hIcon         = LoadIcon(NULL,IDI_WINLOGO);
		wc.hCursor       = LoadCursor(NULL,IDC_ARROW);
		wc.hbrBackground = (HBRUSH)CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
		wc.lpszMenuName  = NULL;
		wc.lpszClassName = title;

		RegisterClass(&wc);

		bRunOnce = true;
	}

	HFONT hStdFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	m_listProg.Reset();
	m_listProg.Reserve(numValues);
	m_listProg.Resize(numValues);
	m_listInfo.Reset();
	m_listInfo.Reserve(numValues);
	m_listInfo.Resize(numValues);

	m_hWnd = CreateWindowEx(NULL,
		title,
		title,
		WS_CAPTION|WS_POPUPWINDOW|WS_CLIPCHILDREN,
		20,20,
		WINDOWWIDTH,numValues * CONTROLHEIGHT + 30,
		GetCOREInterface()->GetMAXHWnd(),
		NULL,
		hInstance,
		NULL);

	SendMessage(m_hWnd,WM_SETFONT,(LPARAM)hStdFont,(WPARAM)0);
	HDC hDC = GetDC(m_hWnd);
	TEXTMETRIC textMetric;
	GetTextMetrics(hDC,&textMetric);
	CONTROLHEIGHT = textMetric.tmHeight + 2;

	GetCOREInterface()->RegisterDlgWnd(m_hWnd);

	for(s32 i=0;i<numValues;i++)
	{
		HWND hProgress = CreateWindowEx(NULL,
			"STATIC",
			NULL,
			SS_SUNKEN|SS_LEFT|WS_VISIBLE|WS_CHILD,
			BORDERSIZE,i * CONTROLHEIGHT + 5,
			WINDOWWIDTH / 2 - BORDERSIZE,CONTROLHEIGHT,
			m_hWnd,
			NULL,
			hInstance,
			NULL);

		m_listInfo[i] = hProgress;

		SendMessage(hProgress,WM_SETFONT,(LPARAM)hStdFont,(WPARAM)0);
	}

	for(s32 i=0;i<numValues;i++)
	{
		HWND hProgress = CreateWindowEx(NULL,
			PROGRESS_CLASS,
			NULL,
			WS_VISIBLE|WS_CHILD,
			WINDOWWIDTH / 2,i * CONTROLHEIGHT + 5,
			WINDOWWIDTH / 2 - BORDERSIZE,CONTROLHEIGHT,
			m_hWnd,
			NULL,
			hInstance,
			NULL);
		m_listProg[i] = hProgress;
	}

	ShowWindow(m_hWnd,SW_SHOW);
	UpdateWindow(m_hWnd);

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::CloseProgressWindow()
{
	if(!m_hWnd)
	{
		return true;
	}

	GetCOREInterface()->UnRegisterDlgWnd(m_hWnd);
	DestroyWindow(m_hWnd);
	m_hWnd = NULL;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::UpdateProgressWindowInfo(int index,const char *info)
{
	if(!m_hWnd)
	{
		return true;
	}

	if(index >= m_listInfo.GetCount())
	{
		return false;
	}

	SetWindowText(m_listInfo[index],info);
	UpdateWindow(m_listInfo[index]);

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::UpdateProgressWindowMax(int index,int max)
{
	if(!m_hWnd)
	{
		return true;
	}

	if(index >= m_listProg.GetCount())
	{
		return false;
	}

	SendMessage(m_listProg[index],PBM_SETRANGE,0,MAKELPARAM(0,max));

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::UpdateProgressWindowValue(int index,int value)
{
	if(!m_hWnd)
	{
		return true;
	}

	if(index >= m_listProg.GetCount())
	{
		return false;
	}

	SendMessage(m_listProg[index],PBM_SETPOS,value,0);

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
atString rexMaxUtility::ReplaceWhitespaceCharacters(const char* str)
{
	int len = (int)strlen(str);
	atString retval;
	for( int a = 0; a < len; a++ )
	{
		char c = str[a];
		if( !isspace( c ) )
		{
			retval += c;
		}
		else
		{
			retval += '_';
		}
	}
	return retval;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::GetSelectedNode(atArray<INode*>& r_NodeList,INode* p_RootNode)
{
	if(!p_RootNode)
	{
		p_RootNode = GetCOREInterface()->GetRootNode();
	}

	if(p_RootNode->Selected() != 0)
	{
		r_NodeList.PushAndGrow(p_RootNode);
	}

	s32 i,Count = p_RootNode->NumberOfChildren();

	for(i=0;i<Count;i++)
	{
		GetSelectedNode(r_NodeList,p_RootNode->GetChildNode(i));
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
static HKEY CreateRegistryKey(HKEY ParentKey,const atString& r_KeyName)
{
	HKEY hNew;
	DWORD dwDisposition;

	if(RegCreateKeyEx(	ParentKey,
		r_KeyName,
		0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,
		NULL,
		&hNew,
		&dwDisposition) != ERROR_SUCCESS)
	{
		throw NULL;
	}

	return hNew;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::SetRegistry(const atString& r_Name,const atString& r_Value)
{
	HKEY hSoftware = CreateRegistryKey(HKEY_CURRENT_USER,atString("Software"));
	HKEY hRockstar = CreateRegistryKey(hSoftware,atString("Rockstar"));
	HKEY hApp = CreateRegistryKey(hRockstar,atString("rexMax"));

	const char* p_Val = r_Value;

	RegSetValueEx(  hApp,
		r_Name,
		NULL,
		REG_SZ,
		(const BYTE*)p_Val,
		r_Value.GetLength() + 1);

	RegCloseKey(hApp);
	RegCloseKey(hRockstar);
	RegCloseKey(hSoftware);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::SetRegistry(const atString& r_Name,u32 Value)
{
	HKEY hSoftware = CreateRegistryKey(HKEY_CURRENT_USER,atString("Software"));
	HKEY hRockstar = CreateRegistryKey(hSoftware,atString("Rockstar"));
	HKEY hApp = CreateRegistryKey(hRockstar,atString("rexMax"));

	RegSetValueEx(  hApp,
		r_Name,
		NULL,
		REG_DWORD,
		(const BYTE*)&Value,
		4);

	RegCloseKey(hApp);
	RegCloseKey(hRockstar);
	RegCloseKey(hSoftware);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::SetRegistry(const atString& r_Name,bool Value)
{
	SetRegistry(r_Name,(u32)(Value ? 1 : 0));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::GetRegistry(const atString& r_Name,atString& r_Value)
{
	DWORD uType;
	char cBuffer[1024];
	DWORD uSize = 1024;
	bool Ret;

	HKEY hSoftware = CreateRegistryKey(HKEY_CURRENT_USER,atString("Software"));
	HKEY hRockstar = CreateRegistryKey(hSoftware,atString("Rockstar"));
	HKEY hApp = CreateRegistryKey(hRockstar,atString("rexMax"));

	u64 uRet = RegQueryValueEx(hApp,r_Name,NULL,&uType,(BYTE*)cBuffer,&uSize);

	if(uRet == ERROR_SUCCESS)
	{
		r_Value = atString(cBuffer);
		Ret = true;
	}
	else
	{
		Ret = false;
	}

	RegCloseKey(hApp);
	RegCloseKey(hRockstar);
	RegCloseKey(hSoftware);

	return Ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::GetRegistry(const atString& r_Name,u32& r_Value)
{
	DWORD uSize;
	bool Ret;

	HKEY hSoftware = CreateRegistryKey(HKEY_CURRENT_USER,atString("Software"));
	HKEY hRockstar = CreateRegistryKey(hSoftware,atString("Rockstar"));
	HKEY hApp = CreateRegistryKey(hRockstar,atString("rexMax"));

	uSize = sizeof(u32);

	u64 uRet = RegQueryValueEx(hApp,r_Name,NULL,NULL,(BYTE*)&r_Value,&uSize);

	if(uRet == ERROR_SUCCESS)
		Ret = true;
	else
		Ret = false;

	RegCloseKey(hApp);
	RegCloseKey(hRockstar);
	RegCloseKey(hSoftware);

	return Ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::GetRegistry(const atString& r_Name,bool& r_Value)
{
	u32 TempVal;

	if(!GetRegistry(r_Name,TempVal))
	{
		return false;
	}

	r_Value = (TempVal == 1) ? true : false;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
atString rexMaxUtility::GetTexMapFullPath(Texmap* p_Texmap)
{
	atString Ret("");

	if(!p_Texmap)
	{
		return Ret;
	}

	Class_ID cid = p_Texmap->ClassID();

	IRsExportTexture *pExpTex = dynamic_cast<IRsExportTexture*>(p_Texmap);
	if(pExpTex)
	{
		Ret += _T(pExpTex->GetMapName());
	}
	else if(cid == Class_ID(BMTEX_CLASS_ID,0))
	{
		BitmapTex* p_BitmapTex = (BitmapTex*)p_Texmap;
		Ret += _T(p_BitmapTex->GetMapName());
	}
	else if(cid == Class_ID(COMPOSITE_CLASS_ID,0))
	{
		s32 i,Count = p_Texmap->NumSubTexmaps();
		Texmap* p_subMap;

		for(i=0;i<Count;i++)
		{
			p_subMap = p_Texmap->GetSubTexmap(i);

			if(p_subMap && p_subMap->ClassID() == Class_ID(BMTEX_CLASS_ID,0))
			{
				BitmapTex* p_BitmapTex = (BitmapTex*)p_subMap;
				Ret += _T(p_BitmapTex->GetMapName());
				Ret += ">";
			}
		}
	}

	return Ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::SetTexMapFullPath(Texmap* p_Texmap, atString& textureName)
{
	if(!p_Texmap)
	{
		return;
	}

	Class_ID cid = p_Texmap->ClassID();

	if(cid == Class_ID(BMTEX_CLASS_ID,0))
	{
		BitmapTex* p_BitmapTex = (BitmapTex*)p_Texmap;
		p_BitmapTex->SetMapName(textureName.c_str());
	}
	else if(cid == Class_ID(COMPOSITE_CLASS_ID,0))
	{
		s32 i,Count = p_Texmap->NumSubTexmaps();
		Texmap* p_subMap;

		for(i=0;i<Count;i++)
		{
			p_subMap = p_Texmap->GetSubTexmap(i);

			if(p_subMap && p_subMap->ClassID() == Class_ID(BMTEX_CLASS_ID,0))
			{
				BitmapTex* p_BitmapTex = (BitmapTex*)p_subMap;
				p_BitmapTex->SetMapName(textureName.c_str());
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult::eResultType rexMaxUtility::GetWarningLevel()
{
	return rexResult::WARNINGS;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
INode* rexMaxUtility::GetNextSelected(INode* p_Node)
{
	if(!p_Node)
		return NULL;

	if(p_Node->Selected())
		return p_Node;

	s32 i,ChildCount = p_Node->NumberOfChildren();

	for(i=0;i<ChildCount;i++)
	{
		INode* p_ChildNode = GetNextSelected(p_Node->GetChildNode(i));

		if(p_ChildNode)
		{
			return p_ChildNode;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::GetNodeFullPath(INode* p_Node,atString& r_Path)
{
	atString TempPath;

	TempPath = "";

	while(p_Node->GetParentNode())
	{
		atString NewPath("/");
		NewPath += p_Node->GetName();
		NewPath += TempPath;

		TempPath = NewPath;

		p_Node = p_Node->GetParentNode();
	}

	r_Path = "/root";
	r_Path += TempPath;
	r_Path += "/";

	rexMaxUtility::ReplaceWhitespaceCharacters(r_Path);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::ConvertCollisionBoxToRageMesh(INode* p_Node,rexObjectGenericBound* bnd,const atString& r_BoundMat,bool WorldSpace)
{
	Matrix34 MatLocal;

	if(WorldSpace)
		CopyMaxMatrixToRageMatrix(p_Node->GetObjectTM(0),MatLocal);
	else
#if HACK_GTA4
	{
		Matrix34 MatObj;																																																																																																		
		Matrix34 MatParent;

		CopyMaxMatrixToRageMatrix(p_Node->GetObjectTM(0),MatObj);
		CopyMaxMatrixToRageMatrix(p_Node->GetNodeTM(0),MatParent);

		MatParent.GetVector(0) = Vector3(1,0,0);
		MatParent.GetVector(1) = Vector3(0,1,0);
		MatParent.GetVector(2) = Vector3(0,0,1);

		MatLocal.DotTranspose(MatObj,MatParent);
	}
#else
		GetLocalMatrix(p_Node,0,MatLocal);
#endif // HACK_GTA4

	Object* p_Object = p_Node->GetObjectRef();
	IParamBlock2* p_ParamBlock = ((Animatable*)p_Object)->GetParamBlock(0);

	s32 i,j;
	float width,height,depth;

	p_ParamBlock->GetValue(rsobjpb_colbox_length,0,width,FOREVER);
	p_ParamBlock->GetValue(rsobjpb_colbox_width,0,height,FOREVER);
	p_ParamBlock->GetValue(rsobjpb_colbox_height,0,depth,FOREVER);

	bnd->m_Type = phBound::GEOMETRY;

	bnd->m_FullPath = p_Node->GetName();
	bnd->m_ParentBoneMatrix.Identity();
	bnd->m_IsSkinned = false;
	bnd->m_Matrix.Identity();

	rexObjectGenericMesh::MaterialInfo& r_MatInfo = bnd->m_Materials.Grow();

	r_MatInfo.m_Name = ":";
	r_MatInfo.m_Name += r_BoundMat;
	r_MatInfo.m_TypeName = "rage_default";

	bnd->m_Normals.Reset();
	bnd->m_Normals.Resize(1);
	bnd->m_Normals[0] = Vector3(0.0f,1.0f,0.0f);
	bnd->m_Center = MatLocal.GetVector(3);
	bnd->m_PivotPoint = Vector3(0.0f,0.0f,0.0f);

	bnd->m_Vertices.Reset();
	bnd->m_Vertices.Resize(8);

	bnd->m_Vertices[0].m_Position.Set(-width,-height,-depth);
	bnd->m_Vertices[1].m_Position.Set(width,-height,-depth);
	bnd->m_Vertices[2].m_Position.Set(-width,height,-depth);
	bnd->m_Vertices[3].m_Position.Set(width,height,-depth);
	bnd->m_Vertices[4].m_Position.Set(-width,-height,depth);
	bnd->m_Vertices[5].m_Position.Set(width,-height,depth);
	bnd->m_Vertices[6].m_Position.Set(-width,height,depth);
	bnd->m_Vertices[7].m_Position.Set(width,height,depth);

	for(i=0;i<8;i++)
	{
		MatLocal.Transform(bnd->m_Vertices[i].m_Position);
	}

	bnd->m_Primitives.Reset();
	bnd->m_Primitives.Resize(12);

	for(i=0;i<12;i++)
	{
		bnd->m_Primitives[i].m_Adjuncts.Reset();
		bnd->m_Primitives[i].m_Adjuncts.Resize(3);
		bnd->m_Primitives[i].m_MaterialIndex = 0;

		for(j=0;j<3;j++)
		{
			bnd->m_Primitives[i].m_Adjuncts[j].m_NormalIndex = 0;
		}
	}

	bnd->m_Primitives[0].m_Adjuncts[1].m_VertexIndex = 2; bnd->m_Primitives[0].m_Adjuncts[2].m_VertexIndex = 3; bnd->m_Primitives[0].m_Adjuncts[0].m_VertexIndex = 0;
	bnd->m_Primitives[1].m_Adjuncts[1].m_VertexIndex = 1; bnd->m_Primitives[1].m_Adjuncts[2].m_VertexIndex = 0; bnd->m_Primitives[1].m_Adjuncts[0].m_VertexIndex = 3;
	bnd->m_Primitives[2].m_Adjuncts[1].m_VertexIndex = 5; bnd->m_Primitives[2].m_Adjuncts[2].m_VertexIndex = 7; bnd->m_Primitives[2].m_Adjuncts[0].m_VertexIndex = 4;
	bnd->m_Primitives[3].m_Adjuncts[1].m_VertexIndex = 6; bnd->m_Primitives[3].m_Adjuncts[2].m_VertexIndex = 4; bnd->m_Primitives[3].m_Adjuncts[0].m_VertexIndex = 7;
	bnd->m_Primitives[4].m_Adjuncts[1].m_VertexIndex = 1; bnd->m_Primitives[4].m_Adjuncts[2].m_VertexIndex = 5; bnd->m_Primitives[4].m_Adjuncts[0].m_VertexIndex = 0;
	bnd->m_Primitives[5].m_Adjuncts[1].m_VertexIndex = 4; bnd->m_Primitives[5].m_Adjuncts[2].m_VertexIndex = 0; bnd->m_Primitives[5].m_Adjuncts[0].m_VertexIndex = 5;
	bnd->m_Primitives[6].m_Adjuncts[1].m_VertexIndex = 3; bnd->m_Primitives[6].m_Adjuncts[2].m_VertexIndex = 7; bnd->m_Primitives[6].m_Adjuncts[0].m_VertexIndex = 1;
	bnd->m_Primitives[7].m_Adjuncts[1].m_VertexIndex = 5; bnd->m_Primitives[7].m_Adjuncts[2].m_VertexIndex = 1; bnd->m_Primitives[7].m_Adjuncts[0].m_VertexIndex = 7;
	bnd->m_Primitives[8].m_Adjuncts[1].m_VertexIndex = 2; bnd->m_Primitives[8].m_Adjuncts[2].m_VertexIndex = 6; bnd->m_Primitives[8].m_Adjuncts[0].m_VertexIndex = 3;
	bnd->m_Primitives[9].m_Adjuncts[1].m_VertexIndex = 7; bnd->m_Primitives[9].m_Adjuncts[2].m_VertexIndex = 3; bnd->m_Primitives[9].m_Adjuncts[0].m_VertexIndex = 6;
	bnd->m_Primitives[10].m_Adjuncts[1].m_VertexIndex = 0; bnd->m_Primitives[10].m_Adjuncts[2].m_VertexIndex = 4; bnd->m_Primitives[10].m_Adjuncts[0].m_VertexIndex = 2;
	bnd->m_Primitives[11].m_Adjuncts[1].m_VertexIndex = 6; bnd->m_Primitives[11].m_Adjuncts[2].m_VertexIndex = 2; bnd->m_Primitives[11].m_Adjuncts[0].m_VertexIndex = 4;

	for(i=0;i<12;i++)
	{
		Vector3 VecA = bnd->m_Vertices[bnd->m_Primitives[i].m_Adjuncts[0].m_VertexIndex].m_Position;
		Vector3 VecB = bnd->m_Vertices[bnd->m_Primitives[i].m_Adjuncts[1].m_VertexIndex].m_Position;
		Vector3 VecC = bnd->m_Vertices[bnd->m_Primitives[i].m_Adjuncts[2].m_VertexIndex].m_Position;

		Vector3 VecX = VecA - VecB;
		Vector3 VecY = VecB - VecC;

		bnd->m_Primitives[i].m_FaceNormal.Cross(VecX,VecY);
	}
}

static bool ForceTextureExport(false);

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::SetForceTextureExport(bool On)
{
	ForceTextureExport = On;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::GetForceTextureExport( )
{
	return (ForceTextureExport);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::TextureExportNeeded(const atString& inputFileName,const atString& outputFileName)
{
#if HACK_GTA4
	if(ForceTextureExport)
		return true;
#endif // HACK_GTA4

	const fiDevice& device = fiDeviceLocal::GetInstance();
	u64 TimeOut = device.GetFileTime(outputFileName);

	if(TimeOut == 0)
		return true;

	char seperators[] = "+>";
	char buffer[8192];

	strcpy(buffer,inputFileName);
	char* pToken = strtok(buffer,seperators);

	while(pToken)
	{
		u64 TimeIn = device.GetFileTime(pToken);

		if(TimeIn == 0)
			return false;

		if(TimeIn > TimeOut)
			return true;

		pToken = strtok(NULL,seperators);
	}

	return false;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::GetBoundMargin(INode* p_Node, float& r_Margin)
{
	return p_Node->GetUserPropFloat("margin", r_Margin);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::CentreWindow(HWND hWnd)
{
	RECT rectSetup;
	RECT rect;
	GetClientRect(hWnd,&rectSetup);
	GetWindowRect(GetParent(hWnd),&rect);

	SetWindowPos(	hWnd,
		NULL,
		((rect.right - rect.left) / 2.0f) - (rectSetup.right / 2.0f),
		((rect.bottom - rect.top) / 2.0f) - (rectSetup.bottom / 2.0f),
		0,
		0,
		(SWP_NOOWNERZORDER|SWP_NOSIZE));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 rexMaxUtility::GetTicksPerSec()
{
	return TicksPerSec;
}

static inline int isPow2(int n)
{
	int m;
	n = (n < 0 ? -n : n);

	for(m = 1; m < n; m *= 2) { ; }

	return( (n < m) ? 0 : 1 );
}

static s32 TextureSizeLimit = -1;
#if HACK_GTA4
// For bit shifting texture size
static s32 TextureShiftDown = 0;
#endif // HACK_GTA4

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::SetTextureSizeLimit(s32 SizeLimit)
{
	TextureSizeLimit = SizeLimit;
}

#if HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void rexMaxUtility::SetTextureShiftDown(s32 ShiftDown)
{
	TextureShiftDown = ShiftDown;
}

s32 rexMaxUtility::GetTextureShiftDown( )
{
	return (TextureShiftDown);
}
#endif // HACK_GTA4

//////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexMaxUtility::ConvertAndSaveBumpDevilImage(const char* outputFileName)
{
	rexResult result(rexResult::PERFECT);

	ILboolean check = ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE);

	int format = IL_DXT5;
#if HACK_GTA4
	bool hasAlpha = false;
#endif // HACK_GTA4
	ILubyte *work = ilGetData();
	ILuint count = (ILuint) ilGetInteger(IL_IMAGE_WIDTH) * ilGetInteger(IL_IMAGE_HEIGHT);
	ILint val = ilGetInteger(IL_IMAGE_FORMAT);
	ILubyte *store = work;

	if ( val == IL_RGBA )
	{
#if HACK_GTA4
		// Determine alpha content
		for (ILuint i = 0; i < count; ++i) {
			if ( work[3] != 255 ) {
				hasAlpha = true;
				break;
			}
			work += 4;
		}
		work = ilGetData();
#endif
		for (ILuint i = 0; i < count; ++i)
		{
#if HACK_GTA4
			if( hasAlpha )
			{
				// Do alpha friendly twiddle
				work[2] = work[3];
				work[3] = work[0];
				work[0] = 0;
				work += 4;
			}
			else
			{
				work[3] = work[0];
				work[0] = 0;
				work[2] = 0;
				work += 4;
			}
#else
			work[3] = work[0];
			work[0] = 0;
			work[2] = 0;
			work += 4;
#endif // HACK_GTA4

		}

		ilSetData(store);
	}

	ilSetInteger(IL_DXTC_FORMAT, format);					// Set the dxt format
	ilEnable(IL_FILE_OVERWRITE);							// Set this to overwrite existing file
	iluImageParameter(ILU_FILTER, ILU_SCALE_LANCZOS3);		// filter is Lanczos, same as old image library
	iluBuildMipmaps();										// Generate mip-maps

	ILboolean bSaveRes = ilSaveImage((ILstring) (const char *)outputFileName);	// USE THIS VERSION OF SAVE!  other versions don't
	if(!bSaveRes)
	{
		result = rexResult::WARNINGS;
		result.AddWarning("Texture conversion warning. Failed to write converted texture '%s'", outputFileName);
		return result;
	}

	return result;
}

static ILuint s_DevilConvertImageNames[2];
static bool s_DevilBegun = false;

void rexMaxUtility::BeginDevil()
{
	Assert(s_DevilBegun==false);

	ilInit();
	iluInit();
	s_DevilBegun=true;
	// We need to bind images in order to clean up their memory later
	ilGenImages(2, s_DevilConvertImageNames);
	ilBindImage(s_DevilConvertImageNames[0]);
	ilEnable(IL_CONV_PAL);
}

void rexMaxUtility::EndDevil()
{
	Assert(s_DevilBegun==true);

	// Unbind this image & delete it
	ilBindImage(0);
	ilDeleteImages(2, s_DevilConvertImageNames) ;
	s_DevilBegun=false;
	ilShutDown();
}

#if HACK_GTA4
// Texture size bit shift function
void DoTextureShift(ILuint& width,ILuint& height)
{
	if(TextureShiftDown != 0)
	{
		s32 CurrShiftDown = TextureShiftDown;

		while((CurrShiftDown > 0) && (width > 32) && (height > 32))
		{
			width >>= 1;
			height >>= 1;
			CurrShiftDown--;
		}
	}

	if(TextureSizeLimit != -1)
	{
		//crop down textures by area

		s32 TexArea = TextureSizeLimit * TextureSizeLimit;

		while(((width * height) > TexArea) && (width > 32) && (height > 32))
		{
			width >>= 1;
			height >>= 1;
		}
	}
}
#endif // HACK_GTA4

rexResult rexMaxUtility::LoadDevilImage(const char* fileName)
{
	rexResult result(rexResult::PERFECT);

	const char *plus = strchr(fileName,'+');
	const char *bracket = strchr(fileName,'>');

	if (bracket)
	{
		char buffer[8192];
		strcpy(buffer,fileName);

		char* p_name = strtok(buffer,">");

		if(p_name)
		{
			ilBindImage(s_DevilConvertImageNames[0]);
			if (ilLoadImage((ILstring) p_name)==FALSE)
			{
				result.Combine(rexResult::ERRORS);
				result.AddError("Texture conversion warning. Failed to load diffuse map '%s'", p_name);
				return result;
			}

			if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
			{
				result.Combine(rexResult::ERRORS);
				result.AddError("Texture conversion warning. Unable to convert diffuse map '%s' to RGBA", p_name);
				return result;
			}
#if !HACK_GTA4
			ILubyte *diffuseData = ilGetData();
#endif // HACK_GTA4
			ILuint diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
			ILuint diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
#if HACK_GTA4
			DoTextureShift(diffuseWidth,diffuseHeight);
			iluImageParameter(ILU_FILTER,ILU_SCALE_LANCZOS3);
			iluScale(diffuseWidth,diffuseHeight,ilGetInteger(IL_IMAGE_DEPTH));

			ILubyte *diffuseData = ilGetData();
			diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
			diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
#endif // HACK_GTA4
			ILint diffuseFormat = ilGetInteger(IL_IMAGE_FORMAT);
#if HACK_GTA4 // HACK_GTA4
			ILuint diffuseDepth = (ILuint) ilGetInteger(IL_IMAGE_DEPTH);
#endif // HACK_GTA4
			ILuint count = diffuseWidth * diffuseHeight;
			while (count--)
			{
				diffuseData[0] = diffuseData[1];
				diffuseData[1] = 0;
				diffuseData[2] = 0;
				diffuseData[3] = 0xff;
				diffuseData += 4;
			}

			p_name = strtok(NULL,">");
			s32 MapCount = 1;

			while(p_name)
			{
#if HACK_GTA4
				bool changedMainSize = false;
#endif // HACK_GTA4

				ilBindImage(s_DevilConvertImageNames[1]);
				if (ilLoadImage((ILstring) p_name)==FALSE)
				{
					ilBindImage(s_DevilConvertImageNames[0]);
					result = rexResult::ERRORS;
					result.AddError("Texture conversion error.  Failed to load opacity map '%s'", p_name);
					return result;
				}
				if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
				{
					result.Combine(rexResult::ERRORS);
					result.AddError("Texture conversion warning. Unable to convert opacity map '%s' to RGBA", p_name);
					return result;
				}

				ILuint opacityWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
				ILuint opacityHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
				ILuint opacityDepth = (ILuint) ilGetInteger(IL_IMAGE_DEPTH);
				ILint opacityFormat = ilGetInteger(IL_IMAGE_FORMAT);
#if HACK_GTA4
				DoTextureShift(opacityWidth,opacityHeight);
				iluScale(opacityWidth,opacityHeight,ilGetInteger(IL_IMAGE_DEPTH));
#endif // HACK_GTA4
				if (opacityWidth != diffuseWidth || opacityHeight != diffuseHeight)
				{
#if HACK_GTA4
					if(opacityWidth > diffuseWidth)
					{
						diffuseWidth = opacityWidth;
						changedMainSize = true;
					}
					if(opacityHeight > diffuseHeight)
					{
						diffuseHeight = opacityHeight;
						changedMainSize = true;
					}

					iluScale(diffuseWidth,diffuseHeight,opacityDepth);
#else
					iluImageParameter(ILU_FILTER,ILU_SCALE_LANCZOS3);
					iluScale(diffuseWidth,diffuseHeight,opacityDepth);
#endif // HACK_GTA4
				}

				ILubyte *opacityData = ilGetData();
				ilBindImage(s_DevilConvertImageNames[0]);
#if HACK_GTA4
				// Check to make sure they're compatible
				if (changedMainSize)
				{
					iluImageParameter(ILU_FILTER,ILU_NEAREST);
					iluScale(diffuseWidth,diffuseHeight,diffuseDepth);
				}
#endif // HACK_GTA4
				diffuseData = ilGetData();

				if (opacityFormat != IL_RGBA || diffuseFormat != IL_RGBA)
				{
					result.Combine(rexResult::ERRORS);
					result.AddError("Texture conversion warning. Devil fn 'ilConvertImage' failed.");
					return result;
				}

				// Merge alpha channel from opacity map into alpha channel of diffuse map
				ILuint count = diffuseWidth * diffuseHeight;
				while (count--)
				{
					diffuseData[MapCount] = opacityData[1];
					diffuseData += 4;
					opacityData += 4;
				}

				p_name = strtok(NULL,">");
				MapCount++;

				if(MapCount == 4)
					break;
			}
		}
	}
	else if (plus)
	{
		char diffuse[512], opacity[512];
		safecpy(diffuse, fileName, plus - fileName + 1);
		safecpy(opacity, plus + 1, sizeof(opacity));

		// Bind diffuse map
		ilBindImage(s_DevilConvertImageNames[0]);
		if (ilLoadImage((ILstring) diffuse)==FALSE)
		{
			result.Combine(rexResult::ERRORS);
			result.AddError("Texture conversion warning. Failed to load diffuse map '%s'", diffuse);
			return result;
		}

		if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
		{
			result.Combine(rexResult::ERRORS);
			result.AddError("Texture conversion warning. Unable to convert diffuse map '%s' to RGBA", diffuse);
			return result;
		}
#if !HACK_GTA4
		ILubyte *diffuseData = ilGetData();
#endif // HACK_GTA4
		ILuint diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
		ILuint diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);

#if HACK_GTA4
		DoTextureShift(diffuseWidth,diffuseHeight);
		iluImageParameter(ILU_FILTER,ILU_SCALE_LANCZOS3);
		iluScale(diffuseWidth,diffuseHeight,ilGetInteger(IL_IMAGE_DEPTH));

		ILubyte *diffuseData = ilGetData();
		diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
		diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
#endif // HACK_GTA4
		ILint diffuseFormat = ilGetInteger(IL_IMAGE_FORMAT);

		// Bind opacity map
		ilBindImage(s_DevilConvertImageNames[1]);
		if (ilLoadImage((ILstring) opacity)==FALSE)
		{
			ilBindImage(s_DevilConvertImageNames[0]);
			result = rexResult::ERRORS;
			result.AddError("Texture conversion error.  Failed to load opacity map '%s'", opacity);
			return result;
		}
		if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
		{
			result.Combine(rexResult::ERRORS);
			result.AddError("Texture conversion warning. Unable to convert opacity map '%s' to RGBA", opacity);
			return result;
		}

		ILuint opacityWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
		ILuint opacityHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
		ILuint opacityDepth = (ILuint) ilGetInteger(IL_IMAGE_DEPTH);
		ILint opacityFormat = ilGetInteger(IL_IMAGE_FORMAT);

		// Check to make sure they're compatible
		if (opacityWidth != diffuseWidth || opacityHeight != diffuseHeight)
		{
			float diffuseRatio = (float)diffuseWidth / (float)diffuseHeight;
			float opacityRatio = (float)opacityWidth / (float)opacityHeight;

			// Check to make sure the texture ratios are at least the same otherwise a texture could look stretched when scaling to the same size.
			if (diffuseRatio != opacityRatio)
			{
				result.Combine(rexResult::ERRORS);
				result.AddError("Texture conversion error. Alpha and diffuse textures have mismatched texture ratios. Diffuse - '%s' Opacity - '%s'", diffuse, opacity);
				return result;
			}

#if HACK_GTA4
			iluImageParameter(ILU_FILTER,ILU_NEAREST);
#else
			iluImageParameter(ILU_FILTER,ILU_SCALE_LANCZOS3);
#endif // HACK_GTA4
			iluScale(diffuseWidth,diffuseHeight,opacityDepth);
		}

		ILubyte *opacityData = ilGetData();
		ilBindImage(s_DevilConvertImageNames[0]);

		if (opacityFormat != IL_RGBA || diffuseFormat != IL_RGBA)
		{
			result.Combine(rexResult::ERRORS);
			result.AddError("Texture conversion warning. Devil fn 'ilConvertImage' failed.");
			return result;
		}

		// Merge alpha channel from opacity map into alpha channel of diffuse map
		ILuint count = diffuseWidth * diffuseHeight;
		while (count--)
		{
			// TODO: Could trivially support premultiplied alpha here
			// diffuseData[0] = diffuseData[0];
			// diffuseData[1] = diffuseData[1];
			// diffuseData[2] = diffuseData[2];
			// TODO: This doesn't seem right; I'm taking green out of the opacity map
			// and feeding it into diffuse alpha because the opacity map itself doesn't
			// seem to have transparency.  Arguably I should be using 5/8 green, 2/8 red,
			// and 1/8 blue or some other YUV intensity approximation.
			diffuseData[3] = opacityData[1];
			diffuseData += 4;
			opacityData += 4;
		}

		return result;
	}
	else
	{
		if(ilLoadImage((ILstring) (const char *)fileName))
		{
#if HACK_GTA4
			if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
			{
				result.Combine(rexResult::ERRORS);
				result.AddError("Texture conversion warning. Unable to convert diffuse map '%s' to RGBA", fileName);
				return result;
			}

			ILuint diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
			ILuint diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);

			DoTextureShift(diffuseWidth,diffuseHeight);
			iluImageParameter(ILU_FILTER,ILU_SCALE_LANCZOS3);
			iluScale(diffuseWidth,diffuseHeight,ilGetInteger(IL_IMAGE_DEPTH));
#endif // HACK_GTA4
			return result;
		}
		else
		{
			result.Combine(rexResult::ERRORS);
			result.AddError("Texture conversion warning. Failed to load image '%s'", fileName);
			return result;
		}
	}

	return result;
}

#if HACK_GTA4
rexResult rexMaxUtility::ConvertAndSaveDevilImage(const char* outputFileName)
{
	rexResult result(rexResult::PERFECT);

	int format = IL_DXT1;
	bool hasWhite = false;
	bool hasBlack = false;

	// Auto switch format based on alpha content
	format = IL_DXT1;
	ILubyte *work = ilGetData();
	ILuint count = (ILuint) ilGetInteger(IL_IMAGE_WIDTH) * ilGetInteger(IL_IMAGE_HEIGHT);
	ILint val = ilGetInteger(IL_IMAGE_FORMAT);
	if ( val == IL_RGBA || val == IL_BGRA ) {
		for (ILuint i = 0; i < count; ++i) {
			if( work[3] == 255 )
				hasWhite = true;
			if( work[3] == 0 )
				hasBlack = true;
			if ( ( ( work[3] != 255 ) && ( work[3] != 0 ) ) || ( hasWhite && hasBlack ) ) {
				format = IL_DXT5;
				break;
			}
			work += 4;
		}
	}

	ilSetInteger(IL_DXTC_FORMAT, format);					// Set the dxt format
	ilEnable(IL_FILE_OVERWRITE);							// Set this to overwrite existing file
	iluImageParameter(ILU_FILTER, ILU_SCALE_LANCZOS3);		// filter is Lanczos, same as old image library
	iluBuildMipmaps();										// Generate mip-maps

	ILboolean bSaveRes = ilSaveImage((ILstring) (const char *)outputFileName);	// USE THIS VERSION OF SAVE!  other versions don't
	if(!bSaveRes)
	{
		result.Combine(rexResult::ERRORS);
		result.AddError("Texture conversion warning. Failed to write converted texture '%s'", outputFileName);
		return result;
	}

	return result;
}

rexResult rexMaxUtility::SaveDevilImage(const char* outputFileName)
{
	rexResult result(rexResult::PERFECT);

	ilSetInteger(IL_DXTC_FORMAT, IL_DXT_NO_COMP);					// Set the dxt format
	ilEnable(IL_FILE_OVERWRITE);							// Set this to overwrite existing file
	iluImageParameter(ILU_FILTER, ILU_NEAREST);	
	
	ILboolean bSaveRes = ilSaveImage((ILstring) (const char *)outputFileName);	// USE THIS VERSION OF SAVE!  other versions don't
	if(!bSaveRes)
	{
		result.Combine(rexResult::ERRORS);
		result.AddError("Texture conversion warning. Failed to write converted texture '%s'", outputFileName);
		return result;
	}

	return result;
}

//
// Use libtiff to write out a TIFF file; DevIL TIFF compression doesn't work.  *sigh*.
// This does not respect the rexTextureShift parameter for scaling; that parameter
// is needing to get removed anyway.
//
rexResult rexMaxUtility::SaveDevilImageAsTiff( const char* outputFileName, int tiff_compression )
{
	rexResult result(rexResult::PERFECT);
	if ( TextureShiftDown > 0 )
	{
		result.AddError("Cannot save TIFFs that are resized.  rexTextureShift not supported.  %s not saved.", 
			outputFileName);
		return (result);
	}

	// DevIL image is already in RGBA format; byte-per-channel from the 
	// LoadDevilImage call that loaded the source texture from disk.
	const int samplesPerPixel = 4;
	ILuint width = ilGetInteger( IL_IMAGE_WIDTH );
	ILuint height = ilGetInteger( IL_IMAGE_HEIGHT );
	ILubyte* pImageData = ilGetData( );
	if ( !pImageData )
	{
		result.AddError( "Failed to read DevIL image data; no data returned.  %s not saved.",
			outputFileName );
		return (result);
	}

	TIFF* pTif = TIFFOpen( outputFileName, "w" );
	if ( pTif )
	{
		TIFFSetField( pTif, TIFFTAG_COMPRESSION, tiff_compression );
		TIFFSetField( pTif, TIFFTAG_IMAGEWIDTH, width );
		TIFFSetField( pTif, TIFFTAG_IMAGELENGTH, height );
		TIFFSetField( pTif, TIFFTAG_SAMPLESPERPIXEL, samplesPerPixel );
		TIFFSetField( pTif, TIFFTAG_BITSPERSAMPLE, 8 );
		TIFFSetField( pTif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG );
		TIFFSetField( pTif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB );
		
		// http://old.nabble.com/TIFFWriteEncodedStrip-LZW-compression-failing-for-certain-input-td20703401.html
		//TIFFSetField( pTif, TIFFTAG_PREDICTOR, PREDICTOR_HORIZONTAL ); 

		ILinfo info;
		iluGetImageInfo( &info );
		switch ( info.Origin )
		{
		case IL_ORIGIN_LOWER_LEFT:
			TIFFSetField( pTif, TIFFTAG_ORIENTATION, ORIENTATION_BOTLEFT );
			break;
		case IL_ORIGIN_UPPER_LEFT:
			TIFFSetField( pTif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT );
			break;
		}
		if ( 0 == TIFFWriteEncodedStrip( pTif, 0, pImageData, width * height * samplesPerPixel ) )
			result.AddError( "Failed to write TIFF image data for: %s.", outputFileName );

		TIFFClose( pTif );
	}
	else
	{
		result.AddError( "Failed to create TIFF image for: %s.", outputFileName );
	}

	return result;
}

unsigned char* rexMaxUtility::DevILGetFlipped( )
{
	ILubyte	*StartPtr, *EndPtr;
	ILuint	/*x,*/ y, d;
	const int samplesPerPixel = 4;
	ILuint width = ilGetInteger( IL_IMAGE_WIDTH );
	ILuint height = ilGetInteger( IL_IMAGE_WIDTH );
	ILuint sizePerLine = width * samplesPerPixel;
	ILuint size = width * height * samplesPerPixel;
	unsigned char* pImageData = ilGetData( );
	unsigned char* pFlippedData = new unsigned char[size];
	if (NULL == pFlippedData)
		return (NULL);

	// Depth: 1 only
	for (d = 0; d < 1; d++) {
		StartPtr = pFlippedData + d * size;
		EndPtr = pImageData + d * size + size;

		for (y = 0; y < height; y++) {
			EndPtr -= sizePerLine;
			/*for (x = 0; x < Image->Bps; x++) {
				StartPtr[x] = EndPtr[x];
			}*/
			memcpy(StartPtr, EndPtr, sizePerLine);
			StartPtr += sizePerLine;
		}
	}

	return (pFlippedData);
}

#endif // HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexMaxUtility::ExportTexture(const char* inputFileName, const char* outputFileName, bool hasAlpha, bool isBumpMap, bool isRaw)
{
	rexResult result(rexResult::PERFECT);

	if(!TextureExportNeeded(atString(inputFileName),atString(outputFileName)))
		return result;

	//If there is an alpha texture in the inputFileName (e.g.
	//"x:/rdr3/art/textures/testbed/swamp/bay_duckweed_02.tif+x:/rdr3/art/textures/testbed/swamp/bay_duckweed_02_a.dds"
	//Then it needs to be properly processed through the pipeline.
	if(strstr(inputFileName,".dds") && !strstr(inputFileName, "+") && !isBumpMap)
	{
		BOOL Cancel = FALSE;
		BOOL copySuccess = CopyFileEx(inputFileName,outputFileName,NULL,NULL,&Cancel,0);
		if(!copySuccess)
		{
			result.Combine(rexResult::ERRORS);
			result.AddError("Copying of texture file %s to %s failed.", inputFileName,outputFileName);
		}
		return result;
	}

	BeginDevil();

	try
	{
		// Now convert
		result.Combine( LoadDevilImage(inputFileName) );
		if(!result.Errors())
		{
			//First perform some basic validation on the image size
			ILuint width = ilGetInteger(IL_IMAGE_WIDTH);
			ILuint height = ilGetInteger(IL_IMAGE_HEIGHT);

			//(1) Make sure the texture width and height are a power of 2
			if(!isPow2((int)width))
			{
				result.Combine(rexResult::WARNINGS);
				result.AddWarning("Texture conversion error. The image '%s' has a width of %d which is not a power of 2, texture will not be converted.",
					(const char*)inputFileName, width);
			}
			if(!isPow2((int)height))
			{
				result.Combine(rexResult::WARNINGS);
				result.AddWarning("Texture conversion error. The image '%s' has a height of %d which is not a power of 2, texture will not be converted.",
					(const char*)inputFileName, height);
			}

			//(2) Make sure the texture's height is not greater than its width
			if( height > width )
			{
				result.Combine(rexResult::WARNINGS);
				result.AddWarning("Texture conversion warning. The image '%s' has a height of < %d > which is greater than its width < %d >.",
					(const char*)inputFileName, height, width);
			}

#if !HACK_GTA4
			if(TextureSizeLimit != -1)
			{
				//crop down textures by area

				s32 TexArea = TextureSizeLimit * TextureSizeLimit;

				while((width * height) > TexArea)
				{
					width >>= 1;
					height >>= 1;
				}
			}
#endif // !HACK_GTA4

			if(width < 4 || height < 4)
			{
				result.AddError("Height or width of texture is less than 4 pixels.  This is not supported: %s.", inputFileName);
				return result;
			}


			iluScale(width,height,ilGetInteger(IL_IMAGE_DEPTH));

			if(!result.Errors())
			{
				rexResult saveResult;

				if(isBumpMap)
				{
					saveResult = ConvertAndSaveBumpDevilImage(outputFileName);
				}
				else if(isRaw)
				{
					saveResult = SaveDevilImage(outputFileName);
				}
				else
#if HACK_GTA4
					saveResult = ConvertAndSaveDevilImage(outputFileName);
#else
					saveResult = rexUtility::ConvertAndSaveDevilImage(outputFileName,hasAlpha);
#endif // HACK_GTA4

				result.Combine(saveResult);
			}
		}
	}
	catch(...)
	{
		//there are some exceptions being triggered by the devil library
		//this catches them so the export doesnt crash...
		//...obviously there should be some handling for this but I'd
		//rather the exporter didnt fall over because of it

		result.Combine(rexResult::WARNINGS);
		result.AddWarning("Texture conversion warning. The image '%s' has caused an exception in devil.",(const char*)inputFileName);
	}

	EndDevil();

	return result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult 
rexMaxUtility::ExportTextureAsTiff( const char* inputFileName, const char* outputFileName, int compression )
{
	rexResult result(rexResult::PERFECT);

	if(!TextureExportNeeded(atString(inputFileName),atString(outputFileName)))
		return result;

	BeginDevil();

	try
	{
		// Now convert
		result.Combine( LoadDevilImage(inputFileName) );
		if(!result.Errors())
		{
			//First perform some basic validation on the image size
			ILuint width = ilGetInteger(IL_IMAGE_WIDTH);
			ILuint height = ilGetInteger(IL_IMAGE_HEIGHT);

			//(1) Make sure the texture width and height are a power of 2
			if(!isPow2((int)width))
			{
				result.Combine(rexResult::WARNINGS);
				result.AddWarning("Texture conversion error. The image '%s' has a width of %d which is not a power of 2, texture will not be converted.",
					(const char*)inputFileName, width);
			}
			if(!isPow2((int)height))
			{
				result.Combine(rexResult::WARNINGS);
				result.AddWarning("Texture conversion error. The image '%s' has a height of %d which is not a power of 2, texture will not be converted.",
					(const char*)inputFileName, height);
			}

			//(2) Make sure the texture's height is not greater than its width
			if( height > width )
			{
				result.Combine(rexResult::WARNINGS);
				result.AddWarning("Texture conversion warning. The image '%s' has a height of < %d > which is greater than its width < %d >.",
					(const char*)inputFileName, height, width);
			}

			if(width < 4 || height < 4)
			{
				result.AddError("Height or width of texture is less than 4 pixels.  This is not supported: %s.", inputFileName);
				return result;
			}

			iluScale(width,height,ilGetInteger(IL_IMAGE_DEPTH));
			
			if(!result.Errors())
			{
				rexResult saveResult;
				saveResult = SaveDevilImageAsTiff(outputFileName, compression);
				result.Combine(saveResult);
			}
		}
	}
	catch(...)
	{
		//there are some exceptions being triggered by the devil library
		//this catches them so the export doesnt crash...
		//...obviously there should be some handling for this but I'd
		//rather the exporter didnt fall over because of it

		result.Combine(rexResult::WARNINGS);
		result.AddWarning("Texture conversion warning. The image '%s' has caused an exception in devil.",(const char*)inputFileName);
	}

	EndDevil();

	return result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::IsMatch(const Vector3& r_A,const Vector3& r_B,float Tolerance)
{
	for(s32 i=0;i<3;i++)
	{
		if(r_A[i] < (r_B[i] - Tolerance))
			return false;

		if(r_A[i] > (r_B[i] + Tolerance))
			return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::HasTranslationAnimation(INode* p_Node,s32 StartFrame,s32 EndFrame)
{
	Matrix34 Mat;
	Vector3 Init;

	for(s32 i=StartFrame;i<EndFrame;i++)
	{
		rexMaxUtility::GetLocalMatrix(p_Node,((f32)i) / rexMaxUtility::GetFPS(),Mat);

		if(i==StartFrame)
		{
			Init = Mat.d;
		}
		else
		{
			if(!IsMatch(Init,Mat.d))
			{
				return true;
			}
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::HasAnyAlphaMap(Mtl* p_Mtl)
{
	if(!p_Mtl)
		return false;

	if(p_Mtl->ClassID() == RSTMATERIAL_CLASS_ID)
	{
		s32 i,Count = p_Mtl->NumSubTexmaps();

		//if there are any in these slots then there's automatically alpha

		if(((RstMtlMax*)p_Mtl)->IsAlphaShader())
		{
			for(i=(Count >> 1);i<Count;i++)
			{
				Texmap* p_DiffuseMap = p_Mtl->GetSubTexmap(i);

				if(p_DiffuseMap)
				{
					return true;
				}
			}
		}

		Count >>= 1;

		for(i=0;i<Count;i++)
		{
			Texmap* p_DiffuseMap = p_Mtl->GetSubTexmap(i);

			if(p_DiffuseMap)
			{
				BitmapTex* p_DiffuseTex = GetIBitmapTextInterface(p_DiffuseMap);
				IRsExportTexture *pExpTex = dynamic_cast<IRsExportTexture*>(p_DiffuseTex);
				if(!p_DiffuseTex && pExpTex)
					p_DiffuseMap = pExpTex->GetResultMap();
				if(p_DiffuseTex)
				{
					Bitmap* p_Bitmap = p_DiffuseTex->GetBitmap(0);
					if(p_Bitmap && p_Bitmap->HasAlpha())
					{
						return true;
					}
				}
			}
		}
	}
	else
	{
		if(p_Mtl->SubTexmapOn(ID_DI) && p_Mtl->GetSubTexmap(ID_DI))
		{
			if(p_Mtl->SubTexmapOn(ID_OP) && p_Mtl->GetSubTexmap(ID_OP))
			{
				return true;
			}
			else
			{
				Texmap* p_DiffuseMap = p_Mtl->GetSubTexmap(ID_DI);
				BitmapTex* p_DiffuseTex = GetIBitmapTextInterface(p_DiffuseMap);

				IRsExportTexture *pExpTex = dynamic_cast<IRsExportTexture*>(p_DiffuseTex);
				if(!p_DiffuseTex && pExpTex)
					p_DiffuseMap = pExpTex->GetResultMap();
				if(p_DiffuseTex)
				{
					Bitmap* p_Bitmap = p_DiffuseTex->GetBitmap(0);
					if(p_Bitmap && p_Bitmap->HasAlpha())
					{
						return true;
					}
				}
			}
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
INode* rexMaxUtility::FindNode(INode* p_Root,const atString& FindName)
{
	s32 i,Count = p_Root->NumberOfChildren();

	for(i=0;i<Count;i++)
	{
		INode* p_FindNode = p_Root->GetChildNode(i);

		if(stricmp(p_FindNode->GetName(),FindName) == 0)
		{
			return p_FindNode;
		}

		INode* p_SearchedNode = FindNode(p_FindNode,FindName);

		if(p_SearchedNode)
			return p_SearchedNode;
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMaxUtility::HasAlphaVertColour(INode* p_Node,Mtl* p_CurrFaceMtl)
{
	if(!p_Node || !p_CurrFaceMtl)
		return false;

	if(HasAnyAlphaMap(p_CurrFaceMtl))
		return true;

	Object* p_Object = p_Node->EvalWorldState(0).obj;

	if(!p_Object->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID,0)))
		return false;

	TriObject* p_TriObject = (TriObject*)p_Object->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID,0));

	if(!p_TriObject)
		return false;

	Mesh* p_Mesh = &(p_TriObject->GetMesh());
#if HACK_GTA4
	if((p_Mesh->mapSupport(MAP_ALPHA) == 0) || (p_Mesh->mapVerts(MAP_ALPHA) == NULL) || (p_Mesh->mapFaces(MAP_ALPHA) == NULL))
#else
	if(!p_Mesh->mapSupport(MAP_ALPHA))
#endif // HACK_GTA4
	{
		if(p_TriObject != p_Object)
			p_TriObject->DeleteMe();

		return false;
	}

	Mtl* p_Mtl = p_Node->GetMtl();
	//atArray<s32,unsigned int,2147483647> vertList;
	atArray<s32,0,unsigned int> vertList;
	s32 i;

	for(i=0;i<p_Mesh->numFaces;i++)
	{
		Face* p_Face = &p_Mesh->faces[i];
		TVFace* p_AlphaFace = &(p_Mesh->mapFaces(MAP_ALPHA)[i]);

		if(GetFaceMtl(p_Mtl,p_Face->getMatID()) == p_CurrFaceMtl)
		{
			vertList.PushAndGrow(p_AlphaFace->t[0]);
			vertList.PushAndGrow(p_AlphaFace->t[1]);
			vertList.PushAndGrow(p_AlphaFace->t[2]);
		}
	}
#if HACK_GTA4
	int AlphaVertCount = p_Mesh->getNumMapVerts(MAP_ALPHA);
#endif // HACK_GTA4

	bool retval = false;
	UVVert* p_AlphaVert = p_Mesh->mapVerts(MAP_ALPHA);
	s32 VertCount = vertList.GetCount();

	for(i=0;i<VertCount;i++)
	{
		s32 vertIdx = vertList[i];
#if HACK_GTA4
		if((vertIdx >= 0) && (vertIdx < AlphaVertCount))
		{
#endif // HACK_GTA4
			//Max likes to be very exact in the SDK whereas the interface isn't. hurray.
			int precision = 10000;
			int roundedVal = round(p_AlphaVert[vertIdx][0]*precision);
			float fRoundedVal = (float)roundedVal/precision;
			if(fRoundedVal != 1.0f)
			{
				retval = true;
				break;
			}
#if HACK_GTA4
		}
#endif // HACK_GTA4 if((vertIdx >= 0) && (vertIdx < AlphaVertCount))
	}

	if(p_TriObject != p_Object)
		p_TriObject->DeleteMe();

	return retval;
}
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
atArray<Vector4>& rexMaxUtility::GetColourTemplate() 
{ 
	return sm_colourTemplate; 
}

#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
#if HACK_GTA4
bool rexMaxUtility::GenerateColourPalette( atArray<Mesh*>& meshes, atArray<int, 0, u32>& idxList, int maxPaletteSize, int minWidthPixels, const atString& outputFilename )
{
	atArray<ColourQuantizer::ColourRGB> colourPalette;
	atArray<Vector4, 0, u32> vertColList;
	
	GenerateColourList(meshes, vertColList, MESH_TINT_COLOR);
	rexUtility::GenerateColourPaletteFromArray( vertColList, colourPalette, idxList, maxPaletteSize );
	rexUtility::GenerateImageFromColourPalette( maxPaletteSize, minWidthPixels, outputFilename, colourPalette);

	return true;
}

bool rexMaxUtility::GenerateColourList( atArray<Mesh*>& meshes, atArray<Vector4, 0, u32>& vertColList, int colourChannel )
{
	for( int n=0; n < meshes.GetCount(); n++)
	{
		Mesh* p_Mesh = meshes[n];
		if(!p_Mesh)
			continue;

		bool HasMeshTintVerts = ((p_Mesh->mapSupport(colourChannel) == 0) || (p_Mesh->mapVerts(colourChannel) == NULL) || (p_Mesh->mapFaces(colourChannel) == NULL)) ? false : true;	

		s32 numFaces = p_Mesh->numFaces;	
		for( int i=0; i<numFaces; i++ )
		{
			for(int j=0;j<3;j++)
			{
				Vector4 meshTintVertCol( 0.0f, 0.0f, 0.0f, 0.0f );
				if(HasMeshTintVerts)
				{
					VertColor* p_VertCol = (VertColor*)p_Mesh->mapVerts(colourChannel);
					TVFace* p_Face = p_Mesh->mapFaces(colourChannel);

					if(p_VertCol && p_Face)
					{
						if(p_VertCol[p_Face[i].t[j]].x <= 1.0f && p_VertCol[p_Face[i].t[j]].x >= 0.0f)
							meshTintVertCol.x = p_VertCol[p_Face[i].t[j]].x;
						else 
							meshTintVertCol.x = 0.0f;
						if(p_VertCol[p_Face[i].t[j]].y <= 1.0f && p_VertCol[p_Face[i].t[j]].y >= 0.0f)
							meshTintVertCol.y = p_VertCol[p_Face[i].t[j]].y;
						else 
							meshTintVertCol.y = 0.0f;
						if(p_VertCol[p_Face[i].t[j]].z <= 1.0f && p_VertCol[p_Face[i].t[j]].z >= 0.0f)
							meshTintVertCol.z = p_VertCol[p_Face[i].t[j]].z;
						else
							meshTintVertCol.z = 0.0f;
						meshTintVertCol.w = 0.0f;
					}
				}
				else
				{
					meshTintVertCol = 1.0f;
				}
				vertColList.PushAndGrow(meshTintVertCol);
			}
		}
	}
	return true;
}
#endif // HACK_GTA4


bool rexMaxUtility::IsCollisionObject(INode* p_Node)
{
	Object* pObject = p_Node->GetObjectRef();
	if (
			pObject &&
			(
			(pObject->ClassID() == COLLISION_BOX_CLASS_ID) ||
			(pObject->ClassID() == COLLISION_SPHERE_CLASS_ID) ||
			(pObject->ClassID() == COLLISION_CAPSULE_CLASS_ID) ||
			(pObject->ClassID() == COLLISION_MESH_CLASS_ID) ||
			(pObject->ClassID() == COLLISION_CYLINDER_CLASS_ID) ||
			(pObject->ClassID() == COLLISION_DISC_CLASS_ID)
//		NULL!=dynamic_cast<CollisionHelperObject*>(p_Node)
#if HACK_GTA4 
		|| (pObject->ClassID() == GTAWHEEL_CLASS_ID)
#endif //HACK_GTA4
		))
		return true;
	return false;
}

