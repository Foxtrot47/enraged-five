#include "rexmax/options.h"
#include "rexmax/resource.h"
#include "rexmax/utility.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

#include <maxscrpt/maxscrpt.h>
#include <maxscrpt/strings.h>
#include <maxscrpt/definsfn.h> //this always has to be defined last out of the maxscript headers

//////////////////////////////////////////////////////////////////////////////////////////////////////
def_visible_primitive(rexSetExportOption,"rexSetExportOption");
def_visible_primitive(rexSaveExportOptions,"rexSaveExportOptions");
def_visible_primitive(rexRestoreExportOptions,"rexRestoreExportOptions");

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexSetExportOption_cf(Value** arg_list, int count)
{
	check_arg_count(rexSetExportOption,2,count);
	type_check(arg_list[0],String,"rexSetExportOption option value");
	type_check(arg_list[1],String,"rexSetExportOption option value");

	char* p_Option = arg_list[0]->to_string();
	char* p_Value = arg_list[1]->to_string();

	if(!strcmp(p_Option,"Texture Path"))
	{
		ExportOptions::GetInst().SetTexturePath(atString(p_Value));
		return &true_value;
	}
	else if(!strcmp(p_Option,"Export Textures"))
	{
		ExportOptions::GetInst().SetExportTextures(!stricmp("true",p_Value));
		return &true_value;
	}
	else if(!strcmp(p_Option,"Export Entity"))
	{
		ExportOptions::GetInst().SetExportEntity(!stricmp("true",p_Value));
		return &true_value;
	}
	else if(!strcmp(p_Option,"Export Mesh"))
	{
		ExportOptions::GetInst().SetExportMesh(!stricmp("true",p_Value));
		return &true_value;
	}
	else if(!strcmp(p_Option,"Export Bounds"))
	{
		ExportOptions::GetInst().SetExportBounds(!stricmp("true",p_Value));
		return &true_value;
	}
	else if(!strcmp(p_Option,"Export Ascii"))
	{
		ExportOptions::GetInst().SetExportMeshAscii(!stricmp("true",p_Value));
		return &true_value;
	}
	else if(!strcmp(p_Option,"Export Skeleton"))
	{
		ExportOptions::GetInst().SetExportSkeleton(!stricmp("true",p_Value));
		return &true_value;
	}
	else if(!strcmp(p_Option,"Export Animation"))
	{
		ExportOptions::GetInst().SetExportAnimation(!stricmp("true",p_Value));
		return &true_value;
	}
	else if(!strcmp(p_Option,"Show Log"))
	{
		ExportOptions::GetInst().SetShowLog(!stricmp("true",p_Value));
		return &true_value;
	}
	else if(!strcmp(p_Option,"Show Progress"))
	{
		ExportOptions::GetInst().SetShowProgress(!stricmp("true",p_Value));
		return &true_value;
	}
	else if(!strcmp(p_Option,"Warning As Error"))
	{
		ExportOptions::GetInst().SetWarningsAsErrors(!stricmp("true",p_Value));
		return &true_value;
	}

	mprintf("\"%s\" option doesn't exist\n",p_Option);
	return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexSaveExportOptions_cf(Value** arg_list, int count)
{
	ExportOptions::GetInst().SaveExportOptions();

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexRestoreExportOptions_cf(Value** arg_list, int count)
{
	ExportOptions::GetInst().RestoreExportOptions();

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
ExportOptions& ExportOptions::GetInst()
{
	static ExportOptions Inst;

	return Inst;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
ExportOptions::ExportOptions():
	m_DlgWnd(NULL),
	m_ShowLog(true),
	m_ShowProgress(true),
	m_ExportEntity(true),
	m_ExportMesh(true),
	m_ExportBounds(true),
	m_ExportAnimation(true),
	m_ExportSkeleton(true),
	m_ExportTextures(true),
	m_ExportMeshAscii(false),
	m_WarningsAsErrors(false)
{
	RestoreExportOptions();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
ExportOptions::~ExportOptions()
{
	SaveExportOptions();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool ExportOptions::ShowDialog(HWND ParentWnd)
{
	INT_PTR Ret = DialogBoxParam(	g_hDllInstance,
									MAKEINTRESOURCE(IDD_DLG_OPTIONS),
									ParentWnd,
									OptionsDialogProc,
									(LPARAM)this);

	if(Ret == 0)
	{
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void ExportOptions::OnInitDialog()
{
	SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_SHOWLOG),BM_SETCHECK,m_ShowLog ? BST_CHECKED : BST_UNCHECKED,0);
	SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTENTITY),BM_SETCHECK,m_ExportEntity ? BST_CHECKED : BST_UNCHECKED,0);
	SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTMESH),BM_SETCHECK,m_ExportMesh ? BST_CHECKED : BST_UNCHECKED,0);
	SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTBOUNDS),BM_SETCHECK,m_ExportBounds ? BST_CHECKED : BST_UNCHECKED,0);
	SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTSKEL),BM_SETCHECK,m_ExportSkeleton ? BST_CHECKED : BST_UNCHECKED,0);
	SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTMESHTEXTURES),BM_SETCHECK,m_ExportTextures ? BST_CHECKED : BST_UNCHECKED,0);
	SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTMESHASCII),BM_SETCHECK,m_ExportMeshAscii ? BST_CHECKED : BST_UNCHECKED,0);
	SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTANIM),BM_SETCHECK,m_ExportAnimation ? BST_CHECKED : BST_UNCHECKED,0);
	SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_WARNASERROR),BM_SETCHECK,m_WarningsAsErrors ? BST_CHECKED : BST_UNCHECKED,0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void ExportOptions::OnCommandOk()
{
	m_ShowLog = SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_SHOWLOG),BM_GETCHECK,0,0) == BST_CHECKED ? true : false;
	m_ExportEntity = SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTENTITY),BM_GETCHECK,0,0) == BST_CHECKED ? true : false;
	m_ExportMesh = SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTMESH),BM_GETCHECK,0,0) == BST_CHECKED ? true : false;
	m_ExportBounds = SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTBOUNDS),BM_GETCHECK,0,0) == BST_CHECKED ? true : false;
	m_ExportSkeleton = SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTSKEL),BM_GETCHECK,0,0) == BST_CHECKED ? true : false;
	m_ExportTextures = SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTMESHTEXTURES),BM_GETCHECK,0,0) == BST_CHECKED ? true : false;
	m_ExportMeshAscii = SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTMESHASCII),BM_GETCHECK,0,0) == BST_CHECKED ? true : false;
	m_ExportAnimation = SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_EXPORTANIM),BM_GETCHECK,0,0) == BST_CHECKED ? true : false;
	m_WarningsAsErrors = SendMessage(GetDlgItem(m_DlgWnd,IDC_CHK_WARNASERROR),BM_GETCHECK,0,0) == BST_CHECKED ? true : false;

	EndDialog(m_DlgWnd,1);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void ExportOptions::OnCommandCancel()
{
	EndDialog(m_DlgWnd,0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK ExportOptions::OptionsDialogProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam)
{
	ExportOptions* p_ExportOptions = (ExportOptions*)GetWindowLong(DlgWnd,DWL_USER);

	switch(Msg)
	{
	case WM_INITDIALOG:
		SetWindowLong(DlgWnd,DWL_USER,LParam);
		p_ExportOptions = (ExportOptions*)LParam;
		p_ExportOptions->m_DlgWnd = DlgWnd;
		p_ExportOptions->OnInitDialog();
		return TRUE;
	case WM_COMMAND:

		switch(LOWORD(WParam))
		{
		case IDOK:
			p_ExportOptions->OnCommandOk();
			break;
		case IDCANCEL:
			p_ExportOptions->OnCommandCancel();
			break;
		}

		return TRUE;
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void ExportOptions::SaveExportOptions()
{
	rexMaxUtility::SetRegistry(atString("ShowLog"),m_ShowLog);
	rexMaxUtility::SetRegistry(atString("ShowProgress"),m_ShowProgress);
	rexMaxUtility::SetRegistry(atString("ExportEntity"),m_ExportEntity);
	rexMaxUtility::SetRegistry(atString("ExportMesh"),m_ExportMesh);
	rexMaxUtility::SetRegistry(atString("ExportBounds"),m_ExportBounds);
	rexMaxUtility::SetRegistry(atString("ExportAnimation"),m_ExportAnimation);
	rexMaxUtility::SetRegistry(atString("ExportSkeleton"),m_ExportSkeleton);
	rexMaxUtility::SetRegistry(atString("ExportTextures"),m_ExportTextures);
	rexMaxUtility::SetRegistry(atString("ExportMeshAscii"),m_ExportMeshAscii);
	rexMaxUtility::SetRegistry(atString("WarningsAsErrors"),m_WarningsAsErrors);
	rexMaxUtility::SetRegistry(atString("TexturePath"),m_TexturePath);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void ExportOptions::RestoreExportOptions()
{
	rexMaxUtility::GetRegistry(atString("ShowLog"),m_ShowLog);
	rexMaxUtility::GetRegistry(atString("ShowProgress"),m_ShowProgress);
	rexMaxUtility::GetRegistry(atString("ExportEntity"),m_ExportEntity);
	rexMaxUtility::GetRegistry(atString("ExportMesh"),m_ExportMesh);
	rexMaxUtility::GetRegistry(atString("ExportBounds"),m_ExportBounds);
	rexMaxUtility::GetRegistry(atString("ExportAnimation"),m_ExportAnimation);
	rexMaxUtility::GetRegistry(atString("ExportSkeleton"),m_ExportSkeleton);
	rexMaxUtility::GetRegistry(atString("ExportTextures"),m_ExportTextures);
	rexMaxUtility::GetRegistry(atString("ExportMeshAscii"),m_ExportMeshAscii);
	rexMaxUtility::GetRegistry(atString("WarningsAsErrors"),m_WarningsAsErrors);
	rexMaxUtility::GetRegistry(atString("TexturePath"),m_TexturePath);
}