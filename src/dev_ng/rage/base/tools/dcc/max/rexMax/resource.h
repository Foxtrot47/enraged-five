//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by rexMax.rc
//
#define IDS_CMD_CAT                     102
#define IDS_CMD_EXPORT                  103
#define IDD_DLG_EXPORTSETUP             103
#define IDS_CMD_EXPORT_DESC             104
#define IDS_CMD_SETUP                   104
#define IDR_MNU_EXPORTSETUP             104
#define IDD_DLG_ITEMEDIT                105
#define IDD_DLG_PROPERTYEDIT            106
#define IDD_DLG_ABOUT                   107
#define IDB_BITMAP1                     108
#define IDC_CHK_EXPORTMESH              1001
#define IDC_CHK_EXPORTMESHTEXTURES      1002
#define IDC_CHK_EXPORTMESHASCII         1003
#define IDC_CHK_EXPORTANIM              1004
#define IDC_CHK_SHOWLOG                 1005
#define IDC_CHK_EXPORTSKEL              1006
#define IDC_CHK_WARNASERROR             1007
#define IDC_CHK_EXPORTBOUNDS            1008
#define IDC_CHECK1                      1009
#define IDC_CHK_EXPORTENTITY            1009
#define IDC_TREE1                       1010
#define IDC_TREE_SETUP                  1010
#define IDC_LST_NODES                   1011
#define IDC_CMD_ADDNODES                1012
#define IDC_CMD_REMOVENODE              1013
#define IDC_LST_PROPERTIES              1014
#define IDC_CBO_TYPE                    1015
#define IDC_EDT_NAME                    1016
#define IDC_CMD_ADDPROPERTY             1017
#define IDC_CMD_REMOVEPROPERTY          1018
#define IDC_CBO_PROPERTYLIST            1020
#define IDC_EDT_VALUE                   1021
#define IDC_CMD_EDITPROPERTY            1022
#define IDC_STC_VERSION                 1023
#define IDC_STC_RAGE                    1024
#define IDC_STC_BUILDTIME               1025
#define IDC_CMD_DEFAULTPROPERTIES       1026
#define ID_CMD_EXPORT                   40001
#define ID_CMD_SETUP                    40002
#define ID_FILE_CLOSEME                 40003
#define ID_ENTRY_ADD                    40004
#define ID_ENTRY_REMOVE                 40005
#define ID_ENTRY_EDIT                   40006
#define ID_Menu                         40007
#define ID_HELP_ABOUT                   40008
#define ID_TEMPLATE_OPEN                40009
#define ID_FILE_TEMPLATE                40010
#define ID_FILE_SAVEAS                  40012
#define ID_FILE_OPENG                   40013
#define ID_FILE_SAVEG                   40014
#define ID_FILE_SHOWLOG                 40015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40016
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
