#ifndef __REX_MAX_SETUP_H__
#define __REX_MAX_SETUP_H__

#include "atl/string.h"

#include "libxml/xmlwriter.h"
#include "libxml/parser.h"

namespace rage {

class RexMaxGraphItem;

/* PURPOSE:
	Main setup dialog for the max rex exporter. Consists of a tree on which
	export items can be created/edited. This is a singleton
*/
class SetupDlg
{
public:
	static SetupDlg& GetInst();

	/*	PURPOSE
			displays the setup dialog (this window is non modal)
	*/
	void Show();

	/*	PURPOSE
			forces an update of the ui
	*/
	void Update();

protected:
	SetupDlg();

	/*	PURPOSE
			main function for creating turning the export graph from
			RexMaxWrapper::GetInst().GetGraphRoot() into something the 
			user can edit
	*/
	HTREEITEM SetupTree();

	/*	PURPOSE
			turns an individual item in the export graph into an item in the ui
	*/
	HTREEITEM SetupTree(HTREEITEM ParentItem,const RexMaxGraphItem* p_GraphItem);

	/*	PURPOSE
			windows message handler for the dialog
	*/
	static INT_PTR CALLBACK RexMaxSetupDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);

	/*	PURPOSE
			windows message handler for the child about dialog
	*/
	static INT_PTR CALLBACK RexMaxAboutDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);

	/*	PURPOSE
			handles initialisation of the dialog
		PARAMS
			Wnd - window handle for the dialog
	*/
	void OnInitDialog(HWND Wnd);

	/*	PURPOSE
			handles destruction of the dialog
	*/
	void OnDestroy();

	void SaveItem(RexMaxGraphItem* p_Item, xmlTextWriterPtr writer, int indent);
	void LoadItem(RexMaxGraphItem* p_Item, atString& filename);
	void ReadItem(RexMaxGraphItem* p_Item, xmlNodePtr pNode);
	void UpdateTitle();

	/*	PURPOSE
			the following are buttons handlers for the dialog
	*/
	void OnCommandClose();
	void OnCommandAdd();
	void OnCommandEdit();
	void OnCommandRemove();
	void OnCommandAbout();
	void OnCommandOpen();
	void OnCommandSave();
	void OnCommandSaveAs();
	void OnShowLog();
	void OnSize(unsigned int newWidth, unsigned int newHeight);

	/*	PURPOSE
			handles tree notification such as the selection changing and drag and drop
	*/
	void OnNotifyTree(const NMTREEVIEW* p_NmTreeView);

	HWND m_Wnd;
	HWND m_TreeWnd;
	HTREEITEM m_CurrentSel;
	atString m_filename;
};

} //namespace rage {

#endif //__REX_MAX_SETUP_H__
