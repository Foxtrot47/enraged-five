#include "graphitem.h"
#include "file/device.h"
#include "wrapper.h"

using namespace rage;

/////////////////////////////////////////////////////////////////////////////////////////////////////
RexMaxGraphItem::RexMaxGraphItem(const atString& r_Name,bool IsLocked):
	mp_Next(NULL),
	mp_Previous(NULL),
	mp_Child(NULL),
	mp_Parent(NULL),
	m_Name(r_Name),
	m_IsLocked(IsLocked)
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
RexMaxGraphItem::~RexMaxGraphItem()
{
	VerifyReferences();
	DeleteAllRefs();
	m_NodeList.Reset();

	DeleteChildren();

	if(mp_Previous)
	{
		mp_Previous->mp_Next = mp_Next;

		if(mp_Next)
		{
			mp_Next->mp_Previous = mp_Previous;
		}
	}
	else
	{
		if(mp_Parent)
		{
			mp_Parent->mp_Child = mp_Next;
		}

		if(mp_Next)
		{
			mp_Next->mp_Previous = NULL;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
RexMaxGraphItem* RexMaxGraphItem::AddChild(const atString& r_Name)
{
#if 1

	RexMaxGraphItem* p_NewItem = new RexMaxGraphItem(r_Name);
	p_NewItem->mp_Parent = this;
	p_NewItem->mp_Next = NULL;

	if(mp_Child)
	{
		RexMaxGraphItem* p_Find = mp_Child;

		while(p_Find->mp_Next)
		{
			p_Find = p_Find->mp_Next;
		}

		p_Find->mp_Next = p_NewItem;
		p_NewItem->mp_Previous = p_Find;
	}
	else
	{
		mp_Child = p_NewItem;
	}

	return p_NewItem;

#else

	RexMaxGraphItem* p_NewItem = new RexMaxGraphItem(r_Name);
	p_NewItem->mp_Parent = this;
	p_NewItem->mp_Next = mp_Child;

	if(mp_Child)
	{
		mp_Child->mp_Previous = p_NewItem;	
	}

	mp_Child = p_NewItem;

	return p_NewItem;

#endif
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
RexMaxGraphItem* RexMaxGraphItem::AddSibling(const atString& r_Name)
{
	if(mp_Parent == NULL)
	{
		return NULL;
	}

	RexMaxGraphItem* p_NewItem = new RexMaxGraphItem(r_Name);
	p_NewItem->mp_Parent = mp_Parent;
	p_NewItem->mp_Next = NULL;

#if 1

	RexMaxGraphItem* p_Find = this;

	while(p_Find->mp_Next)
	{
		p_Find = p_Find->mp_Next;
	}

	p_Find->mp_Next = p_NewItem;
	p_NewItem->mp_Previous = p_Find;	

#else

	
	p_NewItem->mp_Next = mp_Next;
	p_NewItem->mp_Previous = this;
	mp_Next = p_NewItem;

#endif

	return p_NewItem;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool RexMaxGraphItem::IsValidItem(RexMaxGraphItem* p_GraphItem)
{
	if(this == p_GraphItem)
	{
		return true;
	}

	if(mp_Child)
	{
		if(mp_Child->IsValidItem(p_GraphItem))
		{
			return true;
		}
	}

	if(mp_Next)
	{
		if(mp_Next->IsValidItem(p_GraphItem))
		{
			return true;
		}
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
s32 RexMaxGraphItem::GetNumChildren()
{
	s32 Children = 0;
	RexMaxGraphItem* p_ChildGraphItem = GetChild();

	while(p_ChildGraphItem)
	{
		Children++;
		p_ChildGraphItem = p_ChildGraphItem->GetNext();
	}

	return Children;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxGraphItem::SetNodes(const atArray<INode*>& r_NodeList)
{
	DeleteAllRefs();

	m_NodeList.Reset();

	s32 i,Count = r_NodeList.GetCount();

	for(i=0;i<Count;i++)
	{
		if(IsNodeValid(r_NodeList[i]))
		{
			m_NodeList.PushAndGrow(r_NodeList[i],Count);

#if ( MAX_RELEASE < 9000 ) 
			MakeRefByID(FOREVER,i,r_NodeList[i]);
#else
			ReplaceReference(i,r_NodeList[i]);
#endif
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
RefResult RexMaxGraphItem::NotifyRefChanged(Interval ChangeInt,RefTargetHandle Target,PartID& PartID,RefMessage Message)
{
	if(Message != REFMSG_TARGET_DELETED)
	{
		return REF_SUCCEED;
	}

	s32 i,Count = m_NodeList.GetCount();

	for(i=0;i<Count;i++)
	{
		if(m_NodeList[i] == Target)
		{
			m_NodeList.Delete(i);
			return REF_SUCCEED;
		}
	}

	return REF_SUCCEED;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxGraphItem::VerifyReferences()
{
	return;

	atArray<INode*> NodeList;

	s32 i,Count = m_NodeList.GetCount();

	for(i=0;i<Count;i++)
	{
		if(IsNodeValid(m_NodeList[i]))
		{
			NodeList.PushAndGrow(m_NodeList[i],Count);
		}
	}

	m_NodeList.Reset();
	m_NodeList = NodeList;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxGraphItem::SetupReferences()
{
	s32 i,Count = m_NodeList.GetCount();

	for(i=0;i<Count;i++)
	{
		if(IsNodeValid(m_NodeList[i]))
		{
#if ( MAX_RELEASE < 9000 ) 
			MakeRefByID(FOREVER,i,m_NodeList[i]);			
#else
			ReplaceReference(i,m_NodeList[i]);
#endif
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
atArray<INode*>& RexMaxGraphItem::GetNodes() 
{ 
	VerifyReferences();

	return m_NodeList; 
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
int RexMaxGraphItem::NumRefs()
{
	VerifyReferences();

	s32 Count = m_NodeList.GetCount();

	return m_NodeList.GetCount();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle RexMaxGraphItem::GetReference(int i)
{
	return (RefTargetHandle)m_NodeList[i];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool RexMaxGraphItem::IsNodeValid(INode* p_Node,INode* p_Parent)
{
	if(p_Parent == NULL)
	{
		p_Parent = GetCOREInterface()->GetRootNode();
	}

	s32 i,Count = p_Parent->NumberOfChildren();

	for(i=0;i<Count;i++)
	{
		INode* p_Child = p_Parent->GetChildNode(i);

		if(p_Child == p_Node)
		{
			return true;
		}

		if(IsNodeValid(p_Node,p_Child))
		{
			return true;
		}
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxGraphItem::SetReference(int i, RefTargetHandle rtarg)
{
	if(i >= m_NodeList.GetCount())
	{
		return;
	}

	m_NodeList[i] = (INode*)rtarg;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
IOResult RexMaxGraphItem::FixupSave(ISave* p_Save,AppSave* p_AppSave)
{
	p_AppSave->BeginChunk(CHUNK_ITEM); //graph item block

	p_AppSave->BeginChunk(CHUNK_DATA); //data block

	//make sure that all inode pointers
	//that are being saved out are valid
	VerifyReferences();
/*
	atArray<INode*> NodeList;
	DataCount = m_NodeList.GetCount();

	for(i=0;i<DataCount;i++)
	{
		if(IsNodeValid(m_NodeList[i]))
		{
			NodeList.PushAndGrow(m_NodeList[i],DataCount);
		}
	}
*/
	//

	//create a memory version of the data we want to save

	s32 DataSize = GetDataSize();
	char* p_Data = new char[DataSize];

	char Filename[1024];
	fiDeviceMemory::MakeMemoryFileName(Filename,1024,p_Data,DataSize,false,"rexgraphitem save buffer");
	fiDeviceMemory DevMemory;
	fiStream* p_MemStream = fiStream::Create(Filename,DevMemory);

	//fill it with the graph items data

	s32 Version = 1;
	p_MemStream->WriteInt(&Version,1);
	WriteString(p_MemStream,m_Name);
	WriteString(p_MemStream,m_Type);

	s32 PropertyCount = 0;
	s32 i,DataCount = m_Properties.GetNumSlots();

	for(i=0;i<DataCount;i++)
	{
		atMap<atString,atString>::Entry* p_Entry = m_Properties.GetEntry(i);

		while(p_Entry)
		{
			PropertyCount++;
			p_Entry = p_Entry->next;
		}
	}

	p_MemStream->WriteInt(&PropertyCount,1);

	for(i=0;i<DataCount;i++)
	{
		atMap<atString,atString>::Entry* p_Entry = m_Properties.GetEntry(i);

		while(p_Entry)
		{
			WriteString(p_MemStream,p_Entry->key);
			WriteString(p_MemStream,p_Entry->data);

			p_Entry = p_Entry->next;
		}
	}

/*
	DataCount = NodeList.GetCount();
	p_MemStream->WriteInt(&DataCount,1);

	for(i=0;i<DataCount;i++)
	{
		s32 RefID = p_Save->GetRefID(NodeList[i]);
		p_MemStream->WriteInt(&RefID,1);
	}
*/
	
	DataCount = m_NodeList.GetCount();
	p_MemStream->WriteInt(&DataCount,1);

	for(i=0;i<DataCount;i++)
	{
		s32 RefID = p_Save->GetRefID(m_NodeList[i]);
		p_MemStream->WriteInt(&RefID,1);
	}

	//

	p_MemStream->Close();

	assert(p_MemStream->Tell() == DataSize);

	//and put it into the appsave

	ULONG Written;
	p_AppSave->Write(p_Data,DataSize,&Written);
	if(Written != DataSize)
	{
		delete[] p_Data;
		return IO_ERROR;
	}

	delete[] p_Data;

	//end data block

	p_AppSave->EndChunk(); 

	//next block

	if(mp_Next)
	{
		p_AppSave->BeginChunk(CHUNK_NEXT); 
		mp_Next->FixupSave(p_Save,p_AppSave);
		p_AppSave->EndChunk();
	}

	//child block

	if(mp_Child)
	{
		p_AppSave->BeginChunk(CHUNK_CHILD); 
		mp_Child->FixupSave(p_Save,p_AppSave);
		p_AppSave->EndChunk();
	}

	//end graph item block

	p_AppSave->EndChunk(); 

	return IO_OK;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
IOResult RexMaxGraphItem::FixupLoad(ILoad* p_Load,AppLoad* p_AppLoad)
{
	GraphCallback* p_Callback = new GraphCallback(this);

	p_Load->RegisterPostLoadCallback(p_Callback);

	p_AppLoad->OpenChunk();
	USHORT ChunkID = p_AppLoad->CurChunkID();

	if(ChunkID != CHUNK_ITEM)
	{
		return IO_ERROR;
	}

	//data block

	p_AppLoad->OpenChunk();
	ChunkID = p_AppLoad->CurChunkID();

	if(ChunkID != CHUNK_DATA)
	{
		return IO_ERROR;
	}

	s32 DataSize = p_AppLoad->CurChunkLength();
	char* p_Data = new char[DataSize];
	ULONG Read;

	p_AppLoad->Read(p_Data,DataSize,&Read);

	if(Read != DataSize)
	{
		delete[] p_Data;
		return IO_ERROR;
	}

	const int bufferSize = 4096;
	char Filename[bufferSize];
	fiDeviceMemory DevMemory;
	fiDeviceMemory::MakeMemoryFileName(Filename,bufferSize,p_Data,DataSize,false,"rexgraphitem load buffer");
	fiStream* p_MemStream = fiStream::Create(Filename,DevMemory);

	s32 Version;
	p_MemStream->ReadInt(&Version,1);
	ReadString(p_MemStream,m_Name);
	ReadString(p_MemStream,m_Type);	

	s32 i,Count;
	p_MemStream->ReadInt(&Count,1);

	for(i=0;i<Count;i++)
	{
		atString Name;
		atString Value;

		ReadString(p_MemStream,Name);
		ReadString(p_MemStream,Value);	

		m_Properties.Insert(Name,Value);
	}

	p_MemStream->Close();
	delete[] p_Data;

	s32 Added = 0;
	p_MemStream->ReadInt(&Count,1);

	for(i=0;i<Count;i++)
	{
		s32 RefID = -1;
		int intSize = sizeof(int);
		if(p_MemStream->Tell()+intSize>=bufferSize)
		{
			RexMaxWrapper::GetInst().GetShell().ExternalLogWarning("Couldn't load rex export tree completely.");
			break;
		}
		p_MemStream->ReadInt(&RefID,1);

		if(RefID != -1)
		{
			m_NodeList.PushAndGrow(NULL,Count);
			p_Load->RecordBackpatch(RefID,(void**)&(m_NodeList[Added]));
			Added++;
		}
	}

	//end data block

	p_AppLoad->CloseChunk();

	//next block

	if(p_AppLoad->PeekNextChunkID() == CHUNK_NEXT)
	{
		p_AppLoad->OpenChunk();
		RexMaxGraphItem* p_NewItem = AddSibling(atString("next"));
		p_NewItem->FixupLoad(p_Load,p_AppLoad);
		p_AppLoad->CloseChunk();
	}

	//child block

	if(p_AppLoad->PeekNextChunkID() == CHUNK_CHILD)
	{
		p_AppLoad->OpenChunk();
		RexMaxGraphItem* p_NewItem = AddChild(atString("child"));
		p_NewItem->FixupLoad(p_Load,p_AppLoad);
		p_AppLoad->CloseChunk();
	}

	//end graph item block

	p_AppLoad->CloseChunk();

	return IO_OK;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxGraphItem::Clear()
{
	//can only clear the root item
	if(mp_Parent)
	{
		return;
	}

	DeleteChildren();
	DeleteAllRefs();

	m_Properties.Kill();
	m_NodeList.Reset();

	mp_Next = NULL;
	mp_Previous = NULL;
	mp_Child = NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 RexMaxGraphItem::GetDataSize()
{
	s32 Size = 0;

	Size += 4; // Version
	Size += GetStringLength(m_Name);
	Size += GetStringLength(m_Type);
	Size += 4; // Property Count

	s32 i,DataCount = m_Properties.GetNumSlots();

	for(i=0;i<DataCount;i++)
	{
		atMap<atString,atString>::Entry* p_Entry = m_Properties.GetEntry(i);

		while(p_Entry)
		{
			Size += GetStringLength(p_Entry->key);
			Size += GetStringLength(p_Entry->data);

			p_Entry = p_Entry->next;
		}
	}

	Size += 4; //Node Count
	Size += m_NodeList.GetCount() * 4;

	return Size;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 RexMaxGraphItem::GetStringLength(const atString& r_Write)
{
	return r_Write.GetLength() + 4;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxGraphItem::WriteString(fiStream* p_Save,const atString& r_Write)
{
	s32 WriteSize = r_Write.GetLength();
	p_Save->WriteInt(&WriteSize,1);

	if(WriteSize > 0)
	{
		p_Save->Write(r_Write,WriteSize);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxGraphItem::ReadString(fiStream* p_Load,atString& r_Read)
{
	s32 ReadSize;
	p_Load->ReadInt(&ReadSize,1);

	if(ReadSize == 0)
	{
		r_Read = "";
		return;
	}

	char* p_Buffer = new char[ReadSize + 1];
	p_Buffer[ReadSize] = '\0';
	p_Load->Read(p_Buffer,ReadSize);
	r_Read = atString(p_Buffer);
	delete[] p_Buffer;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxGraphItem::DeleteChildren()
{
	if(!mp_Child)
	{
		return;
	}

	mp_Child->DeleteSiblings();
	delete mp_Child;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxGraphItem::DeleteSiblings()
{
	if(!mp_Next)
	{
		return;
	}

	mp_Next->DeleteSiblings();
	delete mp_Next;
}
