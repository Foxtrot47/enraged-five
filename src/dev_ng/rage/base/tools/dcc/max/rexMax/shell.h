#ifndef __REX_MAX_SHELL_H__
#define __REX_MAX_SHELL_H__

#include "atl/array.h"
#include "rexBase/shell.h"

class IFPULog;

namespace rage {

class RexMaxGraphItem;

/* PURPOSE:
	extension to the standard rexshell for querying the scene graph
*/
class rexShellMax : public rexShell
{
public:
	rexShellMax();
	virtual ~rexShellMax();

	/*	PURPOSE
		get a list of all the module type available for export
	*/
	void GetModuleList(atArray<atString>& r_ModuleList);

	/*	PURPOSE
		get a list of all the properties the can be set for graph items
	*/
	void GetPropertyList(atArray<atString>& r_PropertyList);

	BOOL SetULogFilePath(const char *filePath);
	void	ExternalLogMessage(const char* msg, const char* context=NULL, const char* filecontext=NULL);
	void	ExternalLogWarning(const char* msg, const char* context=NULL, const char* filecontext=NULL);
	void	ExternalLogError(const char* msg, const char* context=NULL, const char* filecontext=NULL);
	void	ExternalProfileStart(const char* /*context*/=NULL);
	void	ExternalProfileEnd(const char* /*context*/=NULL);

protected:

	//inherited from rexShell

	/*	PURPOSE
		currently does nothing
	*/
	virtual void Reset();

	/*	PURPOSE
		collects exportitem info for all objects in the scene recursively
	*/
	virtual rexResult GetExportItemInfo(const atArray<rexObject*>& inputObjects,atArray<ExportItemInfo>& exportItems);

	/*	PURPOSE
		returns a depth first iterator for all items in the scene
	*/
	virtual Iterator* GetIterator(ExportItemInfo& exportItem);

	/*	PURPOSE
		returns a hash string equivalent of the passed in object
	*/
	virtual atString GetInputObjectHashString(rexObject& obj);

	/*	PURPOSE
		returns false currently
	*/
	virtual bool IsObjectPartOfInput(const rexObject& obj,const atArray<rexObject*>& inputObjects,bool includeChildren) const;

	/*	PURPOSE
		currently does nothing
	*/
	virtual void CopyModuleInputObjects(rexModule& dest,const rexModule& source);

	/*	PURPOSE
		opens the progress window with numValues progress bars
	*/
	virtual bool OpenProgressWindow(const char *title,int numValues);

	/*	PURPOSE
		closes the progress window
	*/
	virtual bool CloseProgressWindow(void);

	/*	PURPOSE
		changes the info text associated with the requested progress bar
	*/
	virtual bool UpdateProgressWindowInfo(int index, const char *info);

	/*	PURPOSE
		sets the maximum value of the requested progress bar
	*/
	virtual bool UpdateProgressWindowMax(int index, int max);

	/*	PURPOSE
		changes the info text associated with the requested progress bar
	*/
	virtual bool UpdateProgressWindowValue(int index, int value);

	/*	PURPOSE
		does any initial option setup
	*/
	virtual rexResult PreExport();

	/*	PURPOSE
		currently does nothing
	*/
	virtual rexResult PostExport();

	/*	PURPOSE
		currently does nothing
	*/
	virtual void SetupExportItems(const atArray<ExportItemInfo>& items);

	/*	PURPOSE
		currently does nothing
	*/
	virtual void CleanupExportItems(const atArray<ExportItemInfo>& items);

	/*	PURPOSE
		if displaying dialogs returns a callback for setting the current value of a progress bar, else null
	*/
	virtual	rexProgressBarCallback GetProgressBarCurrValueCB();

	/*	PURPOSE
		if displaying dialogs returns a callback for setting the current max value of a progress bar, else null
	*/
	virtual	rexProgressBarCallback GetProgressBarItemCountCB();

	/*	PURPOSE
		if displaying dialogs returns a callback for setting the current info of a progress bar, else null
	*/
	virtual	rexProgressBarTextCallback GetProgressBarInfoCB();

	//end of inherited from rexShell

	/*	PURPOSE
		iterates over every root object of the scene
	*/
	class MaxListIterator : public rexShell::Iterator
	{
	public:
		MaxListIterator(const atArray<INode*>& r_RootNodes);
		virtual ~MaxListIterator() {}

		virtual rexObject* GetNextObject(int& depthDelta);
		virtual int GetObjectCount() const;

	protected:
		atArray<INode*> m_RootNodes;
		s32 m_CurrentRoot;
	};

	/*	PURPOSE
		for a selected export this only returns true for a selected node, else just always returns true
	*/
	bool ShouldExportItem(INode* p_MaxNode);

	/*	PURPOSE
		currently does nothing
	*/
	atString RemoveNamespaceCharacters(const atString& s);

	/*	PURPOSE
		removes any nasty characters from a string, for filenames etc
	*/
	atString RemoveNamespaceAndNonAlphabeticalCharacters(const atString& s);

	/*	PURPOSE
		currently does nothing
	*/
	virtual rexResult VerifyExportItems(rexShell::ExportItemInfo&) { return rexResult::PERFECT; }

	/*	PURPOSE
		adds a new property to an export item info and returns a reference to the connected objects
	*/
	atArray<rexObject*>& AddExportItemProperty(ExportItemInfo& r_ItemInfo,const atString& r_Name,const rexValue& r_Value,const bool Locked = false);

	/*	PURPOSE
		looks through the scene looking for valid export items
	*/
	void GetInputRecursively(ExportItemInfo& exportItem,atArray<INode*>& RootNodes);

	/*	PURPOSE
		looks through the scene looking for valid export items
	*/
	void GetInputFromGraph(RexMaxGraphItem& r_GraphItem,atArray<ExportItemInfo>& exportItems);
#if HACK_GTA4
#ifdef _DEBUG
	static atArray<rexObject*> m_ObjectList;
	static atMap<INode*,rexObject*> m_Created;
#endif // _DEBUG
#endif // HACK_GTA4

private:
	IFPULog *mp_Log;
};

} //namespace rage {

#endif //__REX_MAX_SHELL_H__
