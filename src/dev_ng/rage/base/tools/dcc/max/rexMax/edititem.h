#ifndef __REX_MAX_EDITITEM_H__
#define __REX_MAX_EDITITEM_H__

namespace rage {

class RexMaxGraphItem;
class atString;

/* PURPOSE:
	dialog for editing a single item in the rex exporter tree
*/
class EditItemDlg : public ReferenceMaker
{
public:
	EditItemDlg();
	~EditItemDlg();

	/*	PURPOSE
			creates the dialog ui for editing a graph item. doesnt return until the user closes the dialog
		PARAMS
			ParentWnd - parent window that this dialog is model to
			RexMaxGraphItem - graphitem that we are editing
			IsAdd - true if this dialog is adding a new item or false if we are editing
					an existing item
	*/
	bool DoModal(HWND ParentWnd,RexMaxGraphItem* p_GraphItem,bool IsAdd = false);

	//from ReferenceMaker
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message);
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	//

protected:

	/*	PURPOSE
			add a new property for the graphitem
		PARAMS
			r_Property - property name
			r_Value - property value
	*/
	void AddProperty(const atString& r_Property,const atString& r_Value);

	/*	PURPOSE
			returns true if the passed in node has already been added to the graphitem
		PARAMS
			p_Node - node to check
	*/
	bool IsNodePreset(INode* p_Node);

	/*	PURPOSE
			windows message handler for the dialog
		PARAMS
			DlgWnd - 
			msg - 
			wParam - 
			lParam - 
	*/
	static INT_PTR CALLBACK RexMaxEditItemDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);

	/*	PURPOSE
			handles initialisation of the dialog
		PARAMS
			Wnd - window handle for the dialog
	*/
	void OnInitDialog(HWND Wnd);

	/*	PURPOSE
			handles destruction of the dialog
	*/
	void OnDestroy();

	/*	PURPOSE
			the following are buttons handlers for the dialog
	*/
	void OnCommandOK();
	void OnCommandCancel();
	void OnCommandAddNodes();
	void OnCommandRemoveNodes();
	void OnCommandAddProperty();
	void OnCommandEditProperty();
	void OnCommandRemoveProperty();
	void OnCommandDefaultProperties();
	void OnChangeType();

	bool m_IsAdd;
	HWND m_Wnd;
	RexMaxGraphItem* mp_GraphItem;
	s32 CurType;
};

} //namespace rage {

#endif //__REX_MAX_EDITITEM_H__
