#include "shell.h"
#include "object.h"
#include "rexMaxUtility/utility.h"
#include "wrapper.h"
#include "IFPULog.h"
//#include "utility.h"

using namespace rage;

#if HACK_GTA4
// As far as I can tell Greg added this to help track down a memory leak but the changelist (81869) is unaccessible
// (my perforce client is claiming it doesnt exist).  In the interests of documenting this I've wrapped related code 
// in #ifdef _DEBUG but im not sure it is necessary to keep it
#ifdef _DEBUG
atArray<rexObject*> rexShellMax::m_ObjectList;
atMap<INode*,rexObject*> rexShellMax::m_Created;
#endif // _DEBUG
#endif // HACK_GTA4

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexShellMax::rexShellMax()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexShellMax::~rexShellMax()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexShellMax::Reset()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
atString rexShellMax::RemoveNamespaceCharacters(const atString& s)
{
	return s;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
atString rexShellMax::RemoveNamespaceAndNonAlphabeticalCharacters(const atString& s)
{
	return rexUtility::RemoveNonAlphabeticCharacters(RemoveNamespaceCharacters(s));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
atArray<rexObject*>& rexShellMax::AddExportItemProperty(ExportItemInfo& r_ItemInfo,const atString& r_Name,const rexValue& r_Value,const bool Locked)
{
	r_ItemInfo.m_PropertyIDs.PushAndGrow(rexUtility::RemoveNonAlphabeticCharacters(r_Name),1);
	r_ItemInfo.m_PropertyStrings.PushAndGrow(r_Name,1);
	r_ItemInfo.m_PropertyValues.PushAndGrow(r_Value,1);
	r_ItemInfo.m_PropertyLocked.PushAndGrow(rexValue(Locked != 0),1);

	return r_ItemInfo.m_PropertyConnectedObjects.Grow(1);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexShellMax::ShouldExportItem(INode* p_Node)
{
//	if(!ExportOptions::GetInst().GetExportSelected())
//		return true;

//	if(p_Node->Selected() == 0)
//		return false;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexShellMax::GetExportItemInfo(const atArray<rexObject*>& inputObjects,atArray<ExportItemInfo>& exportItems)
{
	rexResult retval(rexResult::PERFECT);

	RexMaxGraphItem& r_GraphRoot = RexMaxWrapper::GetInst().GetGraphRoot();
	RexMaxGraphItem* p_GraphItem = r_GraphRoot.GetChild();

	while(p_GraphItem)
	{
		GetInputFromGraph(*p_GraphItem,exportItems);

		p_GraphItem = p_GraphItem->GetNext();
	}

	return retval;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexShell::Iterator* rexShellMax::GetIterator(ExportItemInfo& exportItem)
{
	atArray<INode*> RootNodes;

	GetInputRecursively(exportItem,RootNodes);

	return new MaxListIterator(RootNodes);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexShellMax::GetInputRecursively(ExportItemInfo& exportItem,atArray<INode*>& RootNodes)
{
	if(!exportItem.m_ModuleInstance)
	{
		return;
	}

	int a,Count = exportItem.m_ModuleInstance->m_InputObjects.GetCount();

	for(a=0;a<Count;a++)
	{
		const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>(exportItem.m_ModuleInstance->m_InputObjects[a]);

		bool ExistsOrIsDescendantOfExisting = false;
		int b,RootCount = RootNodes.GetCount();

		if(!p_Obj)
			continue;

		for(b=0;b<RootCount;b++)
		{
			if(RootNodes[b] == p_Obj->GetMaxNode())
			{
				ExistsOrIsDescendantOfExisting = true;
				break;
			}
		}

		if(!ExistsOrIsDescendantOfExisting)
		{
			RootNodes.PushAndGrow(p_Obj->GetMaxNode(),(u16)Count);
		}
	}

	// scoop up the children's tagged nodes
	Count = exportItem.m_Children.GetCount();
	for(a=0;a<Count;a++)
	{
		GetInputRecursively(exportItem.m_Children[a],RootNodes);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
atString rexShellMax::GetInputObjectHashString(rexObject& obj)
{
	atString retval;

	const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>(&obj);

	if(p_Obj)
	{
		INode* p_Node = p_Obj->GetMaxNode();

		s32 NodeID = p_Node->GetHandle();
#if HACK_GTA4
		// Possible no brainer.  Fix for object names exceeding 64 chars
		char Temp[256];
#else
		char Temp[64];
#endif // HACK_GTA4
		char Module[64];

		Module[0] = '\0';

		if(rexMaxUtility::IsType(p_Node,Class_ID(TRIOBJ_CLASS_ID,0)))
		{
			strcpy(Module,"Mesh");
		}

		sprintf(Temp,"%s%s%d",Module,p_Node->GetName(),NodeID);

		retval = Temp;
	}

	return retval;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexShellMax::IsObjectPartOfInput(const rexObject& obj,const atArray<rexObject*>& inputObjects,bool includeChildren) const
{
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexShellMax::CopyModuleInputObjects(rexModule& dest, const rexModule& source)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexShellMax::OpenProgressWindow(const char *title, int numValues)
{
	if(!RexMaxWrapper::GetInst().GetShowProgress())
	{
		CloseProgressWindow();
		return true;
	}

	return rexMaxUtility::OpenProgressWindow(title,numValues);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexShellMax::CloseProgressWindow(void)
{
	return rexMaxUtility::CloseProgressWindow();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexShellMax::UpdateProgressWindowInfo(int index, const char *info)
{
	return rexMaxUtility::UpdateProgressWindowInfo(index,info);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexShellMax::UpdateProgressWindowMax(int index,int max)
{
	return rexMaxUtility::UpdateProgressWindowMax(index,max);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexShellMax::UpdateProgressWindowValue(int index,int value)
{
	return rexMaxUtility::UpdateProgressWindowValue(index,value);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexShellMax::PreExport()
{
	rexResult retval(rexResult::PERFECT);

	return retval;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexShellMax::PostExport()
{
	rexResult retval(rexResult::PERFECT);

#if HACK_GTA4
#ifdef _DEBUG
	// Cleanup of m_ObjectList and m_Created
	s32 i,Count = m_ObjectList.GetCount();

	for(i=0;i<Count;i++)
		delete m_ObjectList[i];

	m_ObjectList.Reset();
	m_Created.Kill();
#endif // _DEBUG
#endif //HACK_GTA4
	return retval;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexShellMax::SetupExportItems(const atArray<ExportItemInfo>& items)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexShellMax::CleanupExportItems(const atArray<ExportItemInfo>& items)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void ProgressBarCountCB(int count)
{
	rexMaxUtility::UpdateProgressWindowMax(2,count);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void ProgressBarCurrIndexCB(int value)
{
	rexMaxUtility::UpdateProgressWindowValue(2,value);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void ProgressBarCurrInfoCB(const char* s)
{
	rexMaxUtility::UpdateProgressWindowInfo( 2, s );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexProgressBarCallback rexShellMax::GetProgressBarCurrValueCB()
{
	return ProgressBarCurrIndexCB;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexProgressBarCallback rexShellMax::GetProgressBarItemCountCB()
{
	return ProgressBarCountCB;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexProgressBarTextCallback rexShellMax::GetProgressBarInfoCB()
{
	return ProgressBarCurrInfoCB;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexShellMax::MaxListIterator::MaxListIterator(const atArray<INode*>& r_RootNodes):
	m_RootNodes(r_RootNodes),
	m_CurrentRoot(0)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexObject* rexShellMax::MaxListIterator::GetNextObject(int& depthDelta)
{
	depthDelta = 0;

	if(m_CurrentRoot >= m_RootNodes.GetCount())
		return NULL;

#if HACK_GTA4
#ifdef _DEBUG
	// Check the object cache (which never seems to get greater than 1)
	INode* p_Current = m_RootNodes[m_CurrentRoot++];

	rexObject* p_Ret;
	rexObject** p_Find = m_Created.Access(p_Current);

	if(p_Find)
		p_Ret = *p_Find;
	else
	{
		p_Ret = new rexMaxObject(p_Current);
		m_ObjectList.PushAndGrow(p_Ret);
		m_Created.Insert(p_Current,p_Ret);
	}

	return p_Ret;
#endif // _DEBUG
#endif // HACK_GTA4
	return new rexMaxObject(m_RootNodes[m_CurrentRoot++]);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
int rexShellMax::MaxListIterator::GetObjectCount() const
{
	return m_RootNodes.GetCount();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexShellMax::GetModuleList(atArray<atString>& r_ModuleList)
{
	s32 i,DataCount = m_ModuleMap.GetNumSlots();

	for(i=0;i<DataCount;i++)
	{
		atMap<atString, rexModule*>::Entry* p_Entry = m_ModuleMap.GetEntry(i);

		while(p_Entry)
		{
			r_ModuleList.PushAndGrow(p_Entry->key);

			p_Entry = p_Entry->next;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexShellMax::GetPropertyList(atArray<atString>& r_PropertyList)
{
	s32 i,DataCount = m_PropertyMap.GetNumSlots();

	for(i=0;i<DataCount;i++)
	{
		atMap<atString, rexProperty*>::Entry* p_Entry = m_PropertyMap.GetEntry(i);

		while(p_Entry)
		{
			r_PropertyList.PushAndGrow(p_Entry->key);

			p_Entry = p_Entry->next;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexShellMax::GetInputFromGraph(RexMaxGraphItem& r_GraphItem,atArray<ExportItemInfo>& exportItems)
{
	s32 i,Count;
	ExportItemInfo* p_ExportItem;

	r_GraphItem.VerifyReferences();

	atString Name = r_GraphItem.GetName();

	p_ExportItem = &exportItems.Grow();

	p_ExportItem->m_ModuleID = r_GraphItem.GetType();
	p_ExportItem->m_ObjectNameDefault = RemoveNamespaceCharacters(Name);

	AddExportItemProperty(*p_ExportItem,atString("Name"),rexValue(Name));

	//properties

	const atMap<atString,atString>& r_Properties = r_GraphItem.GetProperties();

	Count = r_Properties.GetNumSlots();

	for(i=0;i<Count;i++)
	{
		const atMap<atString,atString>::Entry* p_Entry = r_Properties.GetEntry(i);

		while(p_Entry)
		{
			AddExportItemProperty(*p_ExportItem,p_Entry->key,p_Entry->data);

			p_Entry = p_Entry->next;
		}
	}

	//nodes

	const atArray<INode*>& r_Nodes = r_GraphItem.GetNodes();
	Count = r_Nodes.GetCount();

	if(Count > 0)
	{
		atArray<rexObject*>& ConnObjectArray = AddExportItemProperty(*p_ExportItem,atString("Nodes"),rexValue((const bool)true));

		for(i=0;i<Count;i++)
		{
			ConnObjectArray.PushAndGrow(new rexMaxObject(r_Nodes[i]),(u16)Count);
		}
	}

	RexMaxGraphItem* p_ChildGraphItem = r_GraphItem.GetChild();

	while(p_ChildGraphItem)
	{
		GetInputFromGraph(*p_ChildGraphItem,p_ExportItem->m_Children);

		p_ChildGraphItem = p_ChildGraphItem->GetNext();
	}
}
BOOL	rexShellMax::SetULogFilePath(const char *filePath)
{
	mp_Log = RsULogInst();
	if(!mp_Log)
		return FALSE;
	return mp_Log->InitPath(filePath, "Rex");
}
void	rexShellMax::ExternalLogMessage(const char* msg, const char* context, const char* filecontext)
{
	if(!mp_Log)
		return;

	mp_Log->LogMessage(msg, context, filecontext);
}
void	rexShellMax::ExternalLogWarning(const char* msg, const char* context, const char* filecontext)
{
	if(!mp_Log)
		return;

	mp_Log->LogWarning(msg, context, filecontext);
}
void	rexShellMax::ExternalLogError(const char* msg, const char* context, const char* filecontext)
{
	if(!mp_Log)
		return;

	mp_Log->LogError(msg, context, filecontext);
}

void	rexShellMax::ExternalProfileStart(const char* context)
{
	if(!mp_Log)
		return;
	mp_Log->ProfileStart(context);
}
void	rexShellMax::ExternalProfileEnd(const char* context)
{
	if(!mp_Log)
		return;
	mp_Log->ProfileEnd(context);
}
