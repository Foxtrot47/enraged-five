#include "setup.h"
#include "wrapper.h"
#include "edititem.h"
#include "resource.h"
#include "rexMaxUtility/utility.h"
#include "atl/string.h"
#include "file/device.h"

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"


using namespace rage;

extern HINSTANCE g_hDllInstance;

/////////////////////////////////////////////////////////////////////////////////////////////////////
SetupDlg::SetupDlg():
	m_Wnd(NULL)
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
SetupDlg& SetupDlg::GetInst()
{
	static SetupDlg Dlg;

	return Dlg;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::Show()
{
	if(m_Wnd)
	{
		return;
	}

	m_Wnd = CreateDialogParam(	g_hDllInstance,
								MAKEINTRESOURCE(IDD_DLG_EXPORTSETUP),
								GetCOREInterface()->GetMAXHWnd(),
								RexMaxSetupDlgProc,
								(LPARAM)this);

	ShowWindow(m_Wnd,SW_SHOW);

	UpdateTitle();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::Update()
{
	if(!m_Wnd)
	{
		return;
	}

	RexMaxGraphItem& r_GraphRoot = RexMaxWrapper::GetInst().GetGraphRoot();

	SendMessage(m_TreeWnd,TVM_DELETEITEM,0,(LPARAM)TVI_ROOT);

	HTREEITEM RootItem = SetupTree();

	TVITEM Item;
	Item.mask = TVIF_STATE;
	Item.state = TVIS_SELECTED;
	Item.stateMask = ~TVIS_OVERLAYMASK;
	Item.hItem = RootItem;

	SendMessage(m_TreeWnd,TVM_SETITEM,0,(LPARAM)&Item);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
HTREEITEM SetupDlg::SetupTree()
{
	RexMaxGraphItem& r_GraphRoot = RexMaxWrapper::GetInst().GetGraphRoot();

	SendMessage(m_TreeWnd,WM_SETREDRAW,FALSE,0);
	HTREEITEM RootItem = SetupTree(TVI_ROOT,&r_GraphRoot);
	SendMessage(m_TreeWnd,WM_SETREDRAW,TRUE,0);

	return RootItem;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
HTREEITEM SetupDlg::SetupTree(HTREEITEM ParentItem,const RexMaxGraphItem* p_GraphItem)
{
	char Buffer[NAME_LENGTH];

	strcpy(Buffer,p_GraphItem->GetName());

	TVINSERTSTRUCT InsertStruct;
	InsertStruct.hParent = ParentItem;
	InsertStruct.hInsertAfter = TVI_LAST;
	InsertStruct.item.mask = TVIF_TEXT|TVIF_PARAM;
	InsertStruct.item.pszText = Buffer;
	InsertStruct.item.lParam = (LPARAM)p_GraphItem;

	HTREEITEM NewItem = (HTREEITEM)SendMessage(m_TreeWnd,TVM_INSERTITEM,0,(LPARAM)&InsertStruct);

	const RexMaxGraphItem* p_Next = p_GraphItem->GetNext();

	if(p_Next)
	{
		SetupTree(ParentItem,p_Next);
	}

	const RexMaxGraphItem* p_Child = p_GraphItem->GetChild();

	if(p_Child)
	{
		SetupTree(NewItem,p_Child);
	}

	return NewItem;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnInitDialog(HWND Wnd)
{
	m_Wnd = Wnd;
	m_TreeWnd = GetDlgItem(m_Wnd,IDC_TREE_SETUP);
	m_CurrentSel = TVI_ROOT;

	//fill the tree from the items in the graph...

	HTREEITEM RootItem = SetupTree();

	TVITEM Item;
	Item.mask = TVIF_STATE;
	Item.state = TVIS_SELECTED;
	Item.stateMask = ~TVIS_OVERLAYMASK;
	Item.hItem = RootItem;

	SendMessage(m_TreeWnd,TVM_SETITEM,0,(LPARAM)&Item);

	GetCOREInterface()->RegisterDlgWnd(m_Wnd);

	rexMaxUtility::CentreWindow(m_Wnd);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnDestroy()
{
	GetCOREInterface()->UnRegisterDlgWnd(m_Wnd);
	m_Wnd = 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::UpdateTitle()
{
	const char* p_LastForward = strrchr(m_filename,'/');
	const char* p_LastBack = strrchr(m_filename,'\\');

	if(p_LastForward > p_LastBack)
		p_LastBack = p_LastForward;
	
	if(p_LastBack)
		p_LastBack++;

	atString out;
	out = "Rex Setup [";
	out += p_LastBack;
	out += "]";

	SetWindowText(m_Wnd,out);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::LoadItem(RexMaxGraphItem* p_Item, atString& filename)
{
	xmlDocPtr m_pXmlDoc = xmlParseFile( filename );
	xmlXPathContextPtr m_pXPathCtx = xmlXPathNewContext( m_pXmlDoc );

	if ( m_pXmlDoc == NULL )
	{
		xmlFreeDoc( m_pXmlDoc );
		return;
	}
	if ( m_pXPathCtx == NULL )
	{
		xmlXPathFreeContext( m_pXPathCtx );
		return;
	}

	xmlXPathObjectPtr pXPathObj = xmlXPathEvalExpression( (xmlChar*) "//item", m_pXPathCtx );
	#define XML_BUFFER_SIZE 512
	TCHAR xmlBuffer[XML_BUFFER_SIZE] = {0};
	if ( pXPathObj )
	{
		switch ( pXPathObj->type )
		{
			case XPATH_NODESET:
			{
				if ( !pXPathObj->nodesetval )
					return; // "User error: XPath expression invalid."

				size_t count = pXPathObj->nodesetval->nodeNr;
				for ( size_t n = 0; n < count; ++n )
				{
					xmlNodePtr pNode = pXPathObj->nodesetval->nodeTab[n];
					if ( stricmp( (const char*) pNode->name, "Item") == 0)
						ReadItem(p_Item, pNode);
				}
			}
			break;

			default:
				break; // "Internal error: XPath query return type not implemented." 
		}
	}
	xmlXPathFreeObject( pXPathObj );
	xmlXPathFreeContext( m_pXPathCtx );
	xmlFreeDoc( m_pXmlDoc );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::ReadItem(RexMaxGraphItem* p_Item, xmlNodePtr pNode)
{
	RexMaxGraphItem* p_NewItem = p_Item->AddChild( atString("new") );
	const char* nameAttribute = (const char*) xmlGetProp(pNode, (xmlChar*) "name");
	const char* typeAttribute = (const char*) xmlGetProp(pNode, (xmlChar*) "type");

	if ( nameAttribute == NULL || typeAttribute == NULL)
		return;

	p_NewItem->SetName( atString(nameAttribute) );
	p_NewItem->SetType( atString(typeAttribute) );

	//Acquire all the properties.
	for( xmlNodePtr child = pNode->children; child != NULL; child = child->next)
	{
		if ( stricmp((const char*) child->name, "Property") == 0)
		{
			const char* childNameAttribute = (const char*) xmlGetProp(child, (xmlChar*) "name");
			const char* valueAttribute = (const char*) xmlGetProp(child, (xmlChar*) "value");

			if ( childNameAttribute == NULL || valueAttribute == NULL)
				continue;

			p_NewItem->GetProperties().Insert( atString(childNameAttribute), atString(valueAttribute) );
		}
		else if (stricmp((const char*) child->name, "Item") == 0)
		{
			ReadItem(p_Item, child);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnCommandOpen()
{
	char nameBuffer[MAX_PATH];
	strcpy(nameBuffer,m_filename);

	OPENFILENAME openfilename;

	atString rootFolder(GetCOREInterface()->GetDir(APP_PLUGCFG_DIR));
	rootFolder += "\\rex";

	openfilename.lStructSize = sizeof(OPENFILENAME);
	openfilename.hwndOwner = m_Wnd;
	openfilename.hInstance = NULL;
	openfilename.lpstrFilter = "rex templates (*.rex)\0*.rex\0";
	openfilename.lpstrCustomFilter = NULL;
	openfilename.nFilterIndex = 0;
	openfilename.lpstrFile = nameBuffer;
	openfilename.nMaxFile = MAX_PATH;
	openfilename.lpstrFileTitle = NULL;
	openfilename.lpstrInitialDir = rootFolder;
	openfilename.lpstrTitle = "choose file to open";
	openfilename.Flags = 0;
	openfilename.lpstrDefExt = ".rex";
	openfilename.pvReserved = 0;
	openfilename.dwReserved = 0;
	openfilename.FlagsEx = 0;

	if(GetOpenFileName(&openfilename) == FALSE)
		return;

	m_filename = nameBuffer;

	RexMaxGraphItem& r_GraphRoot = RexMaxWrapper::GetInst().GetGraphRoot();
	r_GraphRoot.Clear();

	LoadItem(&r_GraphRoot,m_filename);
	
	Update();
	UpdateTitle();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnCommandSave()
{
	if(m_filename == "")
		return;

	RexMaxGraphItem& r_GraphRoot = RexMaxWrapper::GetInst().GetGraphRoot();

	fiDeviceLocal::GetInstance().Delete(m_filename);
	
	// Create a new XmlWriter for uri, with no compression. 
	xmlTextWriterPtr writer = xmlNewTextWriterFilename(m_filename.c_str(), 0);
	if (writer == NULL) 
	{
		printf("Error creating the xml writer\n");
		m_filename = "";
		UpdateTitle();
		return;
	}

	RexMaxGraphItem* p_Item = r_GraphRoot.GetChild();

	while(p_Item)
	{
		SaveItem(p_Item, writer, 0);
		
		p_Item = p_Item->GetNext();
	}

	int rc = xmlTextWriterEndDocument(writer);
	if (rc < 0)
	{
		printf("Error at xmlTextWriterEndDocument\n");
		return;
	}

	xmlFreeTextWriter(writer);

	UpdateTitle();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::SaveItem(RexMaxGraphItem* p_Item, xmlTextWriterPtr writer, int indent )
{
	xmlTextWriterSetIndent(writer, indent);

	//Create an item element.
	int rc = xmlTextWriterStartElement(writer, BAD_CAST "item");
	if (rc < 0) 
	{
		printf("Error at xmlTextWriterStartElement\n");
		return;
	}

	//Write the name attribute.
	rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "name", BAD_CAST p_Item->GetName().c_str());
	if (rc < 0) 
	{
		printf("Error at xmlTextWriterWriteAttribute\n");
		return;
	}

	//Write the type attribute.
	rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "type", BAD_CAST p_Item->GetType().c_str());
	if (rc < 0) 
	{
		printf("Error at xmlTextWriterWriteAttribute\n");
		return;
	}

	//Begin writing all the item's properties.
	atMap<atString,atString>& r_Properties = p_Item->GetProperties();

	xmlTextWriterSetIndent(writer, indent+1);

	s32 Count = r_Properties.GetNumSlots();
	for(s32 i = 0; i < Count; i++)
	{
		const atMap<atString,atString>::Entry* p_Entry = r_Properties.GetEntry(i);

		while(p_Entry)
		{

			//Create an item element.
			rc = xmlTextWriterStartElement(writer, BAD_CAST "property");
			if (rc < 0) 
			{
				printf("Error at xmlTextWriterStartElement\n");
				return;
			}

			//Write the name attribute.
			rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "name", BAD_CAST p_Entry->key.c_str());
			if (rc < 0) 
			{
				printf("Error at xmlTextWriterWriteAttribute\n");
				return;
			}

			//Write the value attribute.
			rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "value", BAD_CAST p_Entry->data.c_str());
			if (rc < 0) 
			{
				printf("Error at xmlTextWriterWriteAttribute\n");
				return;
			}

			//End the property element.
			rc = xmlTextWriterEndElement(writer);
			if (rc < 0)
			{
				printf("Error at xmlTextWriterEndElement\n");
				return;
			}

			p_Entry = p_Entry->next;
		}
	}

	RexMaxGraphItem* p_ChildItem = p_Item->GetChild();

	while(p_ChildItem)
	{
		SaveItem(p_ChildItem, writer, indent+1);

		p_ChildItem = p_ChildItem->GetNext();
	}

	//End the item element.
	rc = xmlTextWriterEndElement(writer);
	if (rc < 0)
	{
		printf("Error at xmlTextWriterEndElement\n");
		return;
	}

	xmlTextWriterSetIndent(writer, indent-1);
}



/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnCommandSaveAs()
{
	char nameBuffer[MAX_PATH];
	strcpy(nameBuffer,m_filename);

	OPENFILENAME openfilename;

	openfilename.lStructSize = sizeof(OPENFILENAME);
	openfilename.hwndOwner = m_Wnd;
	openfilename.hInstance = NULL;
	openfilename.lpstrFilter = "rex templates (*.rex)\0*.rex\0";
	openfilename.lpstrCustomFilter = NULL;
	openfilename.nFilterIndex = 0;
	openfilename.lpstrFile = nameBuffer;
	openfilename.nMaxFile = MAX_PATH;
	openfilename.lpstrFileTitle = NULL;
	openfilename.lpstrInitialDir = "c:/";
	openfilename.lpstrTitle = "choose file to save to";
	openfilename.Flags = 0;
	openfilename.lpstrDefExt = ".rex";
	openfilename.pvReserved = 0;
	openfilename.dwReserved = 0;
	openfilename.FlagsEx = 0;

	if(GetSaveFileName(&openfilename) == FALSE)
		return;

	m_filename = nameBuffer;

	OnCommandSave();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnCommandClose()
{
	m_filename = "";
	RexMaxWrapper::GetInst().GetGraphRoot().Clear();
	Update();
	UpdateTitle();

//	EndDialog(m_Wnd,0);
//	OnDestroy();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnCommandAdd()
{
	atString Name("new item");
	TVITEM Item;
	Item.mask = TVIF_PARAM;
	Item.hItem = m_CurrentSel;

	SendMessage(m_TreeWnd,TVM_GETITEM,0,(LPARAM)&Item);

	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)Item.lParam;
	RexMaxGraphItem* p_NewGraphItem = p_GraphItem->AddChild(Name);

	EditItemDlg Dlg;

	if(!Dlg.DoModal(GetCOREInterface()->GetMAXHWnd(),p_NewGraphItem,true))
	{
		delete p_NewGraphItem;
		return;
	}

	Name = p_NewGraphItem->GetName();

	TVINSERTSTRUCT InsertItem;
	InsertItem.itemex.mask = TVIF_TEXT|TVIF_PARAM;
	InsertItem.hParent = m_CurrentSel;
	InsertItem.hInsertAfter = TVI_LAST;
	InsertItem.itemex.pszText = (char*)((const char*)Name);
	InsertItem.itemex.lParam = (LPARAM)p_NewGraphItem;

	HTREEITEM NewItem = (HTREEITEM)SendMessage(m_TreeWnd,TVM_INSERTITEM,0,(LPARAM)&InsertItem);
	TreeView_Expand(m_TreeWnd, m_CurrentSel, TVE_EXPAND);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnCommandEdit()
{
	if(	(m_CurrentSel == NULL) ||
		(m_CurrentSel == TVI_ROOT))
	{
		return;
	}

	TVITEM Item;
	Item.mask = TVIF_PARAM;
	Item.hItem = m_CurrentSel;

	SendMessage(m_TreeWnd,TVM_GETITEM,0,(LPARAM)&Item);

	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)Item.lParam;

	if(p_GraphItem->IsLocked())
	{
		return;
	}

	EditItemDlg Dlg;

	if(!Dlg.DoModal(GetCOREInterface()->GetMAXHWnd(),p_GraphItem))
	{
		return;
	}

	char Buffer[NAME_LENGTH];
	strcpy(Buffer,p_GraphItem->GetName());

	Item.mask = TVIF_TEXT;
	Item.pszText = Buffer;

	SendMessage(m_TreeWnd,TVM_SETITEM,0,(LPARAM)&Item);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnCommandRemove()
{
	TVITEM Item;
	Item.mask = TVIF_PARAM;
	Item.hItem = m_CurrentSel;

	SendMessage(m_TreeWnd,TVM_GETITEM,0,(LPARAM)&Item);

	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)Item.lParam;

	if(p_GraphItem->IsLocked())
	{
		return;
	}

	delete p_GraphItem;

	SendMessage(m_TreeWnd,TVM_DELETEITEM,0,(LPARAM)m_CurrentSel);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnShowLog()
{
	RexMaxWrapper::GetInst().ShowLog();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnCommandAbout()
{
	DialogBoxParam(	g_hDllInstance,
					MAKEINTRESOURCE(IDD_DLG_ABOUT),
					GetCOREInterface()->GetMAXHWnd(),
					RexMaxAboutDlgProc,
					(LPARAM)this);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnNotifyTree(const NMTREEVIEW* p_NmTreeView)
{
	switch(p_NmTreeView->hdr.code)
	{
	case TVN_SELCHANGED:
		m_CurrentSel = p_NmTreeView->itemNew.hItem;
		break;
//	case TVN_BEGINDRAG:
//		HIMAGELIST TreeView_CreateDragImage(
//		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void SetupDlg::OnSize(unsigned int newWidth, unsigned int newHeight)
{
	//Resize the tree view control
	MoveWindow(m_TreeWnd, 0, 0, newWidth, newHeight, true);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK SetupDlg::RexMaxSetupDlgProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam)
{
	SetupDlg* p_SetupDlg = (SetupDlg*)GetWindowLongPtr(DlgWnd,DWLP_USER);

	switch(Msg)
	{
	case WM_INITDIALOG:

		SetWindowLongPtr(DlgWnd,DWLP_USER,LParam);
		p_SetupDlg = (SetupDlg*)LParam;
		p_SetupDlg->OnInitDialog(DlgWnd);
		return TRUE;
			
	case WM_COMMAND:

		switch(LOWORD(WParam))
		{
		case ID_FILE_OPENG:
			p_SetupDlg->OnCommandOpen();
			break;
		case ID_FILE_SAVEG:
			p_SetupDlg->OnCommandSave();
			break;
		case ID_FILE_SAVEAS:
			p_SetupDlg->OnCommandSaveAs();
			break;
		case ID_FILE_CLOSEME:
			p_SetupDlg->OnCommandClose();
			break;
		case ID_ENTRY_ADD:
			p_SetupDlg->OnCommandAdd();
			break;
		case ID_ENTRY_EDIT:
			p_SetupDlg->OnCommandEdit();
			break;
		case ID_ENTRY_REMOVE:
			p_SetupDlg->OnCommandRemove();
			break;
		case ID_HELP_ABOUT:
			p_SetupDlg->OnCommandAbout();
			break;
		case ID_FILE_SHOWLOG:
			p_SetupDlg->OnShowLog();
			break;
		}

		return TRUE;

	case WM_ACTIVATE:

		return TRUE;

	case WM_NOTIFY:
		
		{
			NMHDR* p_NmHdr = (NMHDR*)LParam;

			switch(p_NmHdr->idFrom)
			{
			case IDC_TREE_SETUP:
				p_SetupDlg->OnNotifyTree((NMTREEVIEW*)LParam);
				break;
			}
		}

		return TRUE;

	case WM_SIZE:
		{
			int newWidth = LOWORD(LParam);
			int newHeight = HIWORD(LParam);
			p_SetupDlg->OnSize(newWidth, newHeight);
			return 0;
		}

	case WM_CLOSE:
		//p_SetupDlg->OnCommandClose();
		EndDialog(DlgWnd,0);
		p_SetupDlg->OnDestroy();
		return TRUE;

	case WM_DESTROY:
		p_SetupDlg->OnDestroy();
		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK SetupDlg::RexMaxAboutDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	switch(msg)
	{
	case WM_INITDIALOG:
		{
			char Buffer[1024];

			sprintf(Buffer,"version %0.2f",0.01f * PLUGIN_VERSION);
			SetWindowText(GetDlgItem(DlgWnd,IDC_STC_VERSION),Buffer);
			sprintf(Buffer,"rage release %d",RAGE_RELEASE);
			SetWindowText(GetDlgItem(DlgWnd,IDC_STC_RAGE),Buffer);
			sprintf(Buffer,"built on %s at %s",__DATE__,__TIME__);
			SetWindowText(GetDlgItem(DlgWnd,IDC_STC_BUILDTIME),Buffer);
		}
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			EndDialog(DlgWnd,0);
		}
	}

	return FALSE;
}
