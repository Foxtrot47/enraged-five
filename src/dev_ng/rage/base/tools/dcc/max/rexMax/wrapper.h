#ifndef __REX_MAX_GRAPH_H__
#define __REX_MAX_GRAPH_H__

#include "shell.h"
#include "graphitem.h"
#include "rexMaxUtility/log.h"

namespace rage {

/* PURPOSE:
	Main entry for max into this plugin. Contains the root of the export
	graph; handles serialisation; triggers editing through the SetupDlg
	and triggers export
*/
class RexMaxWrapper : public GUP
{
public:
	/*	PURPOSE
		singleton access
	*/
	static RexMaxWrapper& GetInst();
	static ClassDesc2* Creator();
	static ActionTable* GetActions();

	//from GUP

	DWORD Start();
	void Stop();
	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);

	//

	virtual	void RegisterModules(rexShellMax& r_Shell) = 0;

	bool GetShowProgress() { return m_ShowProgress; }
	void SetShowProgress(bool ShowProgress) { m_ShowProgress = ShowProgress; }

	void ShowLog();

	/*	PURPOSE
		main trigger for bringing up the export setup dialog (SetupDlg)
	*/
	void DoSetup();

	/*	PURPOSE
		main trigger for triggering an export of the current graph
	*/
	// Return errors and warnings to be added to an array that
	// can be passed back to maxscript and displayed to artist
	rexResult DoExport(bool ShowProgress = true);

	/*	PURPOSE
		resets the entire export graph
	*/
	void Reset();

	/*	PURPOSE
		makes sure that the passed in pointer if valid and part of the export graph
	*/
	bool IsValidItem(RexMaxGraphItem* p_GraphItem);

	/*	PURPOSE
		return a reference to the main shell object
	*/
	rexShellMax& GetShell() { return m_Shell; }

	/*	PURPOSE
		get us a reference to the main export graph root. This is the main doosy
		thats edited by the SetupDlg and exported through the DoExport method of this
		class
	*/
	RexMaxGraphItem& GetGraphRoot() { return m_GraphRoot; }

protected:

	RexMaxWrapper();

	bool m_ShowProgress;
	rexShellMax m_Shell;
	RexMaxGraphItem m_GraphRoot;
	MaxExportLog m_ExportLog;
};

extern RexMaxWrapper& g_Wrapper;

} //namespace rage {

#endif //__REX_MAX_GRAPH_H__
