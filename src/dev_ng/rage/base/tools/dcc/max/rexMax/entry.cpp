//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// AGENT REX Max Entry Point
//
// This file hooks the Agent REX Wrapper up to Max
//
// The toolbuilder machine currently builds ToolDebugMax2012|x64 and ToolReleaseMax2012|x64.
// 
//////////////////////////////////////////////////////////////////////////////////////////////////////

#include "wrapper.h"
#include "rstMaxMtl/rstMax.h"
#include "rageMaxNitroMtl/rageMaxNitroMtl.h"

#include "grmodel/shader.h"
#include "system/param.h"
#include "diag/channel.h"
#include "cranimation/animchannel.h"

HINSTANCE g_hDllInstance;
int controlsInit = FALSE;
bool libInitialized = FALSE;

using namespace rage;

static char g_Description[1024];

//////////////////////////////////////////////////////////////////////////////////////////////////////
#if USE_RAGE_ALLOCATORS

void *operator_new(size_t size,size_t align) 
{
	void *result = ::rage::sysMemAllocator::GetCurrent().Allocate(size,align);

	RAGE_TRACKING_TALLY(result,::rage::MEMTYPE_GAME_VIRTUAL);

#if !__FINAL
	if (result == ::rage::sysMemAllocator::sm_BreakOnAddr)
		__debugbreak();
#endif

	return result;
}

void operator_delete(void *ptr) 
{
	if (ptr) 
	{
		RAGE_TRACKING_UNTALLY(ptr);
		::rage::sysMemAllocator::GetCurrent().Free(ptr);
	}
}

void* operator new(size_t size) {
	return operator_new(size,16);
}


void* operator new[](size_t size) {
	return operator_new(size,16);
}

void operator delete(void *ptr) {
	operator_delete(ptr);
}

void operator delete[](void *ptr) {
	operator_delete(ptr);
}
#endif // USE_RAGE_ALLOCATORS

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool listenerPrinter(const diagChannel& /*channel*/, diagSeverity /*mode*/,const char * /*file*/, int /*line*/, const char * /*msg*/, va_list)
{
	//mprintf("%s", msg);

	// Always attempt to continue (even if it's fatal)
	// Note that we'll never get assertion popups with this behavior either.
	// If we want to handle assertions here, check 'mode' for DIAG_SEVERITY_FATAL.
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	g_hDllInstance = hinstDLL;

#if ( MAX_RELEASE < 9000 ) 
	sprintf(g_Description,"Rage Export version %0.2f built with rage release %d on %s at %s",0.01f * PLUGIN_VERSION,RAGE_RELEASE,__DATE__,__TIME__);

	if (!controlsInit) 
	{
		controlsInit = TRUE;
		InitCustomControls(g_hDllInstance);
		InitCommonControls();
		
		char appName[] = "rexmax.dll";
		char* p_Argv[1];

		p_Argv[0] = appName;

		sysParam::Init(1,p_Argv);

		crAnimChannel::InitClass();

		diagLogCallback = listenerPrinter;
	}
#endif

	return (TRUE);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return g_Description;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) int LibNumberClasses()
{
	return 2;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) {
		case 1: return rage::RexMaxWrapper::Creator();
		default: return 0;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ULONG CanAutoDefer()
{
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////

#if ( MAX_RELEASE >= 9000 ) 

__declspec( dllexport ) int LibInitialize(void)
{
	//According to the SDK docs LibInitialize "should" only be called once but this isn't strictly enforced
	//so make sure we only initialize once
	if(!libInitialized)
	{
		char appName[] = "rexmax.dll";
		char* p_Argv[1];

		p_Argv[0] = appName;

		sysParam::Init(1,p_Argv);

		crAnimChannel::InitClass();
		pgBase::InitClass();

		diagLogCallback = listenerPrinter;

		libInitialized = TRUE;
	}
	return 1;
}


__declspec( dllexport ) int LibShutdown(void)
{
	return 1;
}

#endif //( MAX_RELEASE >= 9000 ) 

//////////////////////////////////////////////////////////////////////////////////////////////////////
TCHAR *GetString(int id)
{
	static TCHAR buf[NAME_LENGTH];

	if (g_hDllInstance)
		return LoadString(g_hDllInstance, id, buf, sizeof(buf)) ? buf : NULL;

	return NULL;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
