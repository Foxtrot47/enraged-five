#ifndef __REX_MAX_OBJECT_H__
#define __REX_MAX_OBJECT_H__

#include "rexbase/object.h"

namespace rage {

/* PURPOSE:
	max extension to the generic rexObject. contains a reference to a Max SDK scene graph
	object called an INode
*/
class rexMaxObject : public rexObject
{
public:

	/*	PURPOSE
			initialises the class with the passed in max node
		PARAMS
			p_Node - pointer to the max node
	*/
	rexMaxObject(INode* p_Node = NULL): mp_Node(p_Node) {}
	~rexMaxObject() {}

	/*	PURPOSE
			check that this isnt a null object
		RETURNS
			true if this is a valid object
	*/
	bool IsValid() { return (mp_Node == NULL) ? false : true; }

	/*	PURPOSE
			return a pointer to the max node
		RETURNS
			contained max node pointer
	*/
	INode* GetMaxNode() const { return mp_Node; }

protected:
	INode* mp_Node;
};

} //namespace rage {

#endif //__REX_MAX_OBJECT_H__