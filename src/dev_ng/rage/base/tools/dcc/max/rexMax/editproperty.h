#ifndef __REX_MAX_EDITPROPERTY_H__
#define __REX_MAX_EDITPROPERTY_H__

#include "atl/string.h"

namespace rage {

/* PURPOSE:
	dialog to edit a single property of an export item
*/
class EditPropertyDlg
{
public:
	/*	PURPOSE
			creates the dialog ui for editing a property of a graph item.
			doesnt return until the user closes the dialog
		PARAMS
			ParentWnd - parent window that this dialog is model to (will be the graph item edit dialog)
			r_Property - property name we are editing
			r_Value - property value we are editing
			IsAdd - true if this dialog is adding a new item or false if we are editing
					an existing item
	*/
	bool DoModal(HWND ParentWnd,atString& r_Property,atString& r_Value,bool IsAdd = false);

protected:

	/*	PURPOSE
			windows message handler for the dialog
		PARAMS
			DlgWnd - 
			msg - 
			wParam - 
			lParam - 
	*/
	static INT_PTR CALLBACK RexMaxEditPropertyDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);

	/*	PURPOSE
			handles initialisation of the dialog
		PARAMS
			Wnd - window handle for the dialog
	*/
	void OnInitDialog(HWND Wnd);

	/*	PURPOSE
			the following are buttons handlers for the dialog
	*/
	void OnCommandOK();
	void OnCommandCancel();

	bool m_IsAdd;
	HWND m_Wnd;
	atString m_Property;
	atString m_Value;
};

} //namespace rage {

#endif //__REX_MAX_EDITPROPERTY_H__
