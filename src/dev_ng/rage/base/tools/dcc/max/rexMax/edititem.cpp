#include "edititem.h"
#include "editproperty.h"
#include "resource.h"
#include "wrapper.h"
#include "rexMaxUtility/utility.h"
#include "atl/array.h"
#include "atl/string.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

/////////////////////////////////////////////////////////////////////////////////////////////////////
EditItemDlg::EditItemDlg()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
EditItemDlg::~EditItemDlg()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool EditItemDlg::DoModal(HWND ParentWnd,RexMaxGraphItem* p_GraphItem,bool IsAdd)
{
	if(!p_GraphItem)
	{
		return false;
	}

	m_IsAdd = IsAdd;
	mp_GraphItem = p_GraphItem;
	mp_GraphItem->VerifyReferences();

	s32 Ret = DialogBoxParam(	g_hDllInstance,
								MAKEINTRESOURCE(IDD_DLG_ITEMEDIT),
								ParentWnd,
								RexMaxEditItemDlgProc,
								(LPARAM)this);

	return (Ret == 1) ? true : false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
RefResult EditItemDlg::NotifyRefChanged(Interval ChangeInt,RefTargetHandle Target,PartID& PartID,RefMessage Message)
{
	if(Message != REFMSG_TARGET_DELETED)
	{
		return REF_SUCCEED;
	}

	HWND NodeWnd = GetDlgItem(m_Wnd,IDC_LST_NODES);
	s32 i,Count = SendMessage(NodeWnd,LB_GETCOUNT,0,0);

	for(i=0;i<Count;i++)
	{
		RefTargetHandle Compare = (RefTargetHandle)SendMessage(NodeWnd,LB_GETITEMDATA,(WPARAM)i,0);

		if(Compare == Target)
		{
			DeleteReference(i);
			SendMessage(NodeWnd,LB_DELETESTRING,i,0);
			return REF_SUCCEED;
		}
	}

	return REF_SUCCEED;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
int EditItemDlg::NumRefs()
{
	return SendMessage(GetDlgItem(m_Wnd,IDC_LST_NODES),LB_GETCOUNT,0,0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle EditItemDlg::GetReference(int i)
{
	return (RefTargetHandle)SendMessage(GetDlgItem(m_Wnd,IDC_LST_NODES),LB_GETITEMDATA,(WPARAM)i,0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::SetReference(int i, RefTargetHandle rtarg)
{
	SendMessage(GetDlgItem(m_Wnd,IDC_LST_NODES),LB_SETITEMDATA,i,(LPARAM)rtarg);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnInitDialog(HWND Wnd)
{
	m_Wnd = Wnd;

	rexMaxUtility::CentreWindow(m_Wnd);

	s32 i,Count;

	if(m_IsAdd)
	{
		SetWindowText(m_Wnd,"Add Item");
	}

	//item name

	SetWindowText(GetDlgItem(m_Wnd,IDC_EDT_NAME),mp_GraphItem->GetName());

	//modules

	atArray<atString> ModuleNameList;
	RexMaxWrapper::GetInst().GetShell().GetModuleList(ModuleNameList);

	Count = ModuleNameList.GetCount();

	HWND TypeWnd = GetDlgItem(m_Wnd,IDC_CBO_TYPE);

	for(i=0;i<Count;i++)
	{
		SendMessage(TypeWnd,CB_ADDSTRING,0,(LPARAM)((const char*)ModuleNameList[i]));
	}

	char Buffer[NAME_LENGTH];
	bool Found = false;

	for(i=0;i<Count;i++)
	{
		SendMessage(TypeWnd,CB_GETLBTEXT,i,(LPARAM)Buffer);

		if(mp_GraphItem->GetType() == Buffer)
		{
			CurType = i;
			SendMessage(TypeWnd,CB_SETCURSEL,i,0);
			Found = true;
			break;
		}
	}

	if(!Found)
	{
		SendMessage(TypeWnd,CB_SETCURSEL,0,0);
	}

	//node

	HWND NodeWnd = GetDlgItem(m_Wnd,IDC_LST_NODES);
	const atArray<INode*>& r_Nodes = mp_GraphItem->GetNodes();
	Count = r_Nodes.GetCount();

	for(i=0;i<Count;i++)
	{
		if(!IsNodePreset(r_Nodes[i]))
		{
			s32 NewIndex = SendMessage(NodeWnd,LB_ADDSTRING,0,(LPARAM)r_Nodes[i]->GetName());
#if ( MAX_RELEASE < 9000 ) 
			MakeRefByID(FOREVER,NewIndex,r_Nodes[i]);
#else
			ReplaceReference(NewIndex,r_Nodes[i]);
#endif
		}
	}

	//property grid

	HWND PropertyWnd = GetDlgItem(m_Wnd,IDC_LST_PROPERTIES);
	u32 Style = ListView_GetExtendedListViewStyle(PropertyWnd);

	Style |= LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT;

	ListView_SetExtendedListViewStyle(PropertyWnd,Style);

	LVCOLUMN Column;

	Column.mask = LVCF_TEXT|LVCF_WIDTH|LVCF_FMT|LVCF_SUBITEM|LVCF_ORDER;
	Column.fmt = LVCFMT_LEFT;
	Column.cx = 150;
	Column.pszText = "Name";
	Column.iSubItem = 0;
	Column.iOrder = 0;

	SendMessage(PropertyWnd,LVM_INSERTCOLUMN,0,(LPARAM)&Column);

	Column.cx = 70;
	Column.pszText = "Value";
	Column.iSubItem = 1;
	Column.iOrder = 1;

	SendMessage(PropertyWnd,LVM_INSERTCOLUMN,1,(LPARAM)&Column);

	//properties

	const atMap<atString,atString>& r_Properties = mp_GraphItem->GetProperties();

	Count = r_Properties.GetNumSlots();
	s32 Idx = 0;

	if(m_IsAdd)
	{
		OnCommandDefaultProperties();
	}
	else
	{
		for(i=0;i<Count;i++)
		{
			const atMap<atString,atString>::Entry* p_Entry = r_Properties.GetEntry(i);

			while(p_Entry)
			{
				LVITEM Item;
				Item.mask = LVIF_TEXT;
				Item.iItem = Idx++;
				Item.pszText = Buffer;

				strcpy(Buffer,p_Entry->key);
				Item.iSubItem = 0;
				SendMessage(PropertyWnd,LVM_INSERTITEM,0,(LPARAM)&Item);

				if(p_Entry->data.GetLength() < NAME_LENGTH)
				{
					strcpy(Buffer,p_Entry->data);
				}
				else
				{
					strcpy(Buffer,"too long");
				}

				Item.iSubItem = 1;
				SendMessage(PropertyWnd,LVM_SETITEM,0,(LPARAM)&Item);

				p_Entry = p_Entry->next;
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnDestroy()
{
	DeleteAllRefs();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnCommandOK()
{
	char Name[NAME_LENGTH];
	char Type[NAME_LENGTH];
	char Buffer[NAME_LENGTH];
	atArray<INode*> NodeList;
	atMap<atString,atString> Properties;
	s32 i,Count;

	HWND PropertyWnd = GetDlgItem(m_Wnd,IDC_LST_PROPERTIES);

	//name

	GetWindowText(GetDlgItem(m_Wnd,IDC_EDT_NAME),Name,NAME_LENGTH);

	if(Name[0] == '\0')
	{
		MessageBox(m_Wnd,"Please enter a name","Input Error",MB_OK);
		return;
	}

	//type

	s32 CurSel = SendMessage(GetDlgItem(m_Wnd,IDC_CBO_TYPE),CB_GETCURSEL,0,0);

	if(CurSel == -1)
	{
		MessageBox(m_Wnd,"Please select a type","Input Error",MB_OK);
		return;
	}

	SendMessage(GetDlgItem(m_Wnd,IDC_CBO_TYPE),CB_GETLBTEXT,CurSel,(LPARAM)Type);

	//nodes

	HWND NodeWnd = GetDlgItem(m_Wnd,IDC_LST_NODES);
	Count = SendMessage(NodeWnd,LB_GETCOUNT,0,0);

	for(i=0;i<Count;i++)
	{
		INode* p_Node = (INode*)SendMessage(NodeWnd,LB_GETITEMDATA,(WPARAM)i,0);
		NodeList.PushAndGrow(p_Node,Count);
	}

	//properties

	Count = SendMessage(PropertyWnd,LVM_GETITEMCOUNT,0,0);

	for(i=0;i<Count;i++)
	{
		LVITEM Item;

		Item.mask = LVIF_TEXT;
		Item.cchTextMax = NAME_LENGTH;
		Item.pszText = Buffer;
		Item.iItem = i;
		Item.iSubItem = 0;
		ListView_GetItem(PropertyWnd,&Item);

		atString Property(Buffer);

		Item.iSubItem = 1;
		ListView_GetItem(PropertyWnd,&Item);

		atString Value(Buffer);

		Properties.Insert(Property,Value);
	}

	//

	mp_GraphItem->SetName( atString(Name) );
	mp_GraphItem->SetType( atString(Type) );
	mp_GraphItem->SetNodes(NodeList);
	mp_GraphItem->SetProperties(Properties);

	EndDialog(m_Wnd,1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnCommandCancel()
{
	EndDialog(m_Wnd,0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnCommandAddNodes()
{
	atArray<INode*> SelectedNodes;

	rexMaxUtility::GetSelectedNode(SelectedNodes);

	s32 i,Count = SelectedNodes.GetCount();
	HWND NodeWnd = GetDlgItem(m_Wnd,IDC_LST_NODES);

	for(i=0;i<Count;i++)
	{
		if(!IsNodePreset(SelectedNodes[i]))
		{
			s32 NewIndex = SendMessage(NodeWnd,LB_ADDSTRING,0,(LPARAM)SelectedNodes[i]->GetName());
#if ( MAX_RELEASE < 9000 ) 
			MakeRefByID(FOREVER,NewIndex,SelectedNodes[i]);
#else
			ReplaceReference(NewIndex,SelectedNodes[i]);
#endif
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnCommandRemoveNodes()
{
	HWND NodeWnd = GetDlgItem(m_Wnd,IDC_LST_NODES);
	s32 CurSel = SendMessage(NodeWnd,LB_GETCURSEL,0,0);

	if(CurSel == -1)
	{
		return;
	}

	DeleteReference(CurSel);
	SendMessage(NodeWnd,LB_DELETESTRING,CurSel,0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnCommandAddProperty()
{
	EditPropertyDlg Dlg;
	atString Property,Value;

	HWND PropertyWnd = GetDlgItem(m_Wnd,IDC_LST_PROPERTIES);

	if(!Dlg.DoModal(GetCOREInterface()->GetMAXHWnd(),Property,Value,true))
	{
		return;
	}

	AddProperty(Property,Value);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::AddProperty(const atString& r_Property,const atString& r_Value)
{
	HWND PropertyWnd = GetDlgItem(m_Wnd,IDC_LST_PROPERTIES);

	char Buffer[NAME_LENGTH];
	s32 Count = SendMessage(PropertyWnd,LVM_GETITEMCOUNT,0,0);

	LVITEM Item;
	Item.mask = LVIF_TEXT;
	Item.iItem = Count;
	Item.pszText = Buffer;

	strcpy(Buffer,r_Property);
	Item.iSubItem = 0;
	SendMessage(PropertyWnd,LVM_INSERTITEM,0,(LPARAM)&Item);

	strcpy(Buffer,r_Value);
	Item.iSubItem = 1;
	SendMessage(PropertyWnd,LVM_SETITEM,0,(LPARAM)&Item);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnCommandEditProperty()
{
	EditPropertyDlg Dlg;

	HWND PropertyWnd = GetDlgItem(m_Wnd,IDC_LST_PROPERTIES);
	s32 SelMark = ListView_GetSelectionMark(PropertyWnd);

	if(SelMark == -1)
	{
		return;
	}

	LVITEM Item;
	char Buffer[NAME_LENGTH];

	Item.mask = LVIF_TEXT;
	Item.cchTextMax = NAME_LENGTH;
	Item.pszText = Buffer;
	Item.iItem = SelMark;
	Item.iSubItem = 0;
	ListView_GetItem(PropertyWnd,&Item);

	atString Property(Buffer);

	Item.iSubItem = 1;
	ListView_GetItem(PropertyWnd,&Item);

	atString Value(Buffer);

	if(!Dlg.DoModal(GetCOREInterface()->GetMAXHWnd(),Property,Value))
	{
		return;
	}

	strcpy(Buffer,Property);
	Item.iSubItem = 0;
	ListView_SetItem(PropertyWnd,&Item);
	strcpy(Buffer,Value);
	Item.iSubItem = 1;
	ListView_SetItem(PropertyWnd,&Item);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnCommandRemoveProperty()
{
	HWND PropertyWnd = GetDlgItem(m_Wnd,IDC_LST_PROPERTIES);
	s32 SelMark = ListView_GetSelectionMark(PropertyWnd);

	if(SelMark == -1)
	{
		return;
	}

	SendMessage(PropertyWnd,LVM_DELETEITEM,SelMark,0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnCommandDefaultProperties()
{
	HWND PropertyWnd = GetDlgItem(m_Wnd,IDC_LST_PROPERTIES);
	SendMessage(PropertyWnd,LVM_DELETEALLITEMS,0,0);

	//AddProperty

	s32 CurSel = SendMessage(GetDlgItem(m_Wnd,IDC_CBO_TYPE),CB_GETCURSEL,0,0);

	if(CurSel == -1)
		return;

	switch(CurSel)
	{
	case 0: //animation
		AddProperty(atString("OutputPath"),atString("c:/"));
		AddProperty(atString("ChannelsToExport"),atString("(transX;transY;transZ;)(rotX;rotY;rotZ;)"));
		AddProperty(atString("AnimExportSelected"),atString("false"));
		AddProperty(atString("AnimMoverTrack"),atString("true"));
		break;
	case 1: //bound
		AddProperty(atString("OutputPath"),atString("c:/"));
		AddProperty(atString("ExportAsComposite"),atString("false"));
		AddProperty(atString("ExportAsOctree"),atString("false"));
		AddProperty(atString("ForceExportAsComposite"),atString("false"));
		AddProperty(atString("BoundExportWorldSpace"),atString("false"));
		break;
	case 2: //char cloth
		break;
	case 3: //cloth piece
		break;
	case 4: //edge model
		AddProperty(atString("OutputPath"),atString("c:/"));
		break;
	case 5: //entity
		AddProperty(atString("OutputPath"),atString("c:/"));
		AddProperty(atString("SkipBoundSectionOfTypeFile"),atString("false"));
		break;
	case 6: //env cloth
		break;
	case 7: //fragment
		AddProperty(atString("OutputPath"),atString("c:/"));
		break;
	case 8: //locator
		break;
	case 9: //mesh
		AddProperty(atString("TextureOutputPath"),atString("c:/"));
		AddProperty(atString("OutputPath"),atString("c:/"));
		AddProperty(atString("ExportTextures"),atString("true"));
		AddProperty(atString("MeshForSkeleton"),atString("true"));
		AddProperty(atString("MeshAsAscii"),atString("false"));
		break;
	case 10: // particle
		AddProperty(atString("OutputPath"),atString("c:/"));
		break;
	case 11: //skeleton
		AddProperty(atString("OutputPath"),atString("c:/"));
		AddProperty(atString("ChannelsToExport"),atString("(transX;transY;transZ;)(rotX;rotY;rotZ;)"));
		AddProperty(atString("SkeletonRootAtOrigin"),atString("false"));
		AddProperty(atString("SkeletonWriteLimitAndLockInfo"),atString("false"));
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditItemDlg::OnChangeType()
{
	s32 NewType = SendMessage(GetDlgItem(m_Wnd,IDC_CBO_TYPE),CB_GETCURSEL,0,0);

	if(CurType == NewType)
	{
		return;
	}

	CurType = NewType;
	OnCommandDefaultProperties();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool EditItemDlg::IsNodePreset(INode* p_Node)
{
	HWND NodeWnd = GetDlgItem(m_Wnd,IDC_LST_NODES);
	s32 i,Count = SendMessage(NodeWnd,LB_GETCOUNT,0,0);

	for(i=0;i<Count;i++)
	{
		if(p_Node == (INode*)SendMessage(NodeWnd,LB_GETITEMDATA,i,0))
		{
			return true;
		}
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK EditItemDlg::RexMaxEditItemDlgProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam)
{
	EditItemDlg* p_EditItemDlg = (EditItemDlg*)GetWindowLongPtr(DlgWnd,DWLP_USER);

	switch(Msg)
	{
	case WM_INITDIALOG:

		SetWindowLongPtr(DlgWnd,DWLP_USER,LParam);
		p_EditItemDlg = (EditItemDlg*)LParam;
		p_EditItemDlg->OnInitDialog(DlgWnd);
		return TRUE;

	case WM_ACTIVATE:

		return TRUE;

	case WM_COMMAND:

		if(HIWORD(WParam) == CBN_SELCHANGE)
		{
			if((HWND)LParam == GetDlgItem(DlgWnd,IDC_CBO_TYPE))
			{
				p_EditItemDlg->OnChangeType();
			}
		}
		else
		{
			switch(LOWORD(WParam))
			{
			case IDOK:
				p_EditItemDlg->OnCommandOK();
				break;
			case IDCANCEL:
				p_EditItemDlg->OnCommandCancel();
				break;
			case IDC_CMD_ADDNODES:
				p_EditItemDlg->OnCommandAddNodes();
				break;
			case IDC_CMD_REMOVENODE:
				p_EditItemDlg->OnCommandRemoveNodes();
				break;
			case IDC_CMD_ADDPROPERTY:
				p_EditItemDlg->OnCommandAddProperty();
				break;
			case IDC_CMD_EDITPROPERTY:
				p_EditItemDlg->OnCommandEditProperty();
				break;
			case IDC_CMD_REMOVEPROPERTY:
				p_EditItemDlg->OnCommandRemoveProperty();
				break;
			case IDC_CMD_DEFAULTPROPERTIES:
				p_EditItemDlg->OnCommandDefaultProperties();
				break;
			}
		}

		return TRUE;

	case WM_CLOSE:
		p_EditItemDlg->OnCommandCancel();
		return TRUE;

	case WM_DESTROY:
		p_EditItemDlg->OnDestroy();
		return TRUE;
	}

	return FALSE;
}
