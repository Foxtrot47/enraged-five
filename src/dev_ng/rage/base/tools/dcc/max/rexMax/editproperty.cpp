#include "rexMax/editproperty.h"
#include "rexMax/resource.h"
#include "rexMax/wrapper.h"
#include "rexMaxUtility/utility.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool EditPropertyDlg::DoModal(HWND ParentWnd,atString& r_Property,atString& r_Value,bool IsAdd)
{
	m_IsAdd = IsAdd;
	m_Property = r_Property;
	m_Value = r_Value;

	s32 Ret = DialogBoxParam(	g_hDllInstance,
								MAKEINTRESOURCE(IDD_DLG_PROPERTYEDIT),
								ParentWnd,
								RexMaxEditPropertyDlgProc,
								(LPARAM)this);

	if(Ret == 0)
	{
		return false;
	}

	r_Property = m_Property;
	r_Value = m_Value;	

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditPropertyDlg::OnInitDialog(HWND Wnd)
{
	m_Wnd = Wnd;

	rexMaxUtility::CentreWindow(m_Wnd);

	if(m_IsAdd)
	{
		SetWindowText(m_Wnd,"Add Property");
	}

	atArray<atString> PropertyNameList;
	RexMaxWrapper::GetInst().GetShell().GetPropertyList(PropertyNameList);
	s32 i,Count = PropertyNameList.GetCount();

	HWND PropertyWnd = GetDlgItem(m_Wnd,IDC_CBO_PROPERTYLIST);

	for(i=0;i<Count;i++)
	{
		if((PropertyNameList[i] == "Name") || (PropertyNameList[i] == "Nodes"))
		{
			continue;
		}

		SendMessage(PropertyWnd,CB_ADDSTRING,0,(LPARAM)((const char*)PropertyNameList[i]));
	}

	char Buffer[NAME_LENGTH];

	for(i=0;i<Count;i++)
	{
		SendMessage(PropertyWnd,CB_GETLBTEXT,i,(LPARAM)Buffer);

		if(m_Property == Buffer)
		{
			SendMessage(PropertyWnd,CB_SETCURSEL,i,0);
			break;
		}
	}

	SetWindowText(GetDlgItem(m_Wnd,IDC_EDT_VALUE),m_Value);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditPropertyDlg::OnCommandOK()
{
	char Buffer[NAME_LENGTH];

	HWND PropertyWnd = GetDlgItem(m_Wnd,IDC_CBO_PROPERTYLIST);
	s32 CurSel = SendMessage(PropertyWnd,CB_GETCURSEL,0,0);

	if(CurSel == -1)
	{
		MessageBox(m_Wnd,"Please select a property","Input Error",MB_OK);
		return;
	}

	SendMessage(PropertyWnd,CB_GETLBTEXT,CurSel,(LPARAM)Buffer);
	m_Property = Buffer;

	GetWindowText(GetDlgItem(m_Wnd,IDC_EDT_VALUE),Buffer,NAME_LENGTH);
	m_Value = Buffer;

	if(m_Value == "")
	{
		MessageBox(m_Wnd,"Please enter a value","Input Error",MB_OK);
		return;
	}

	EndDialog(m_Wnd,1);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void EditPropertyDlg::OnCommandCancel()
{
	EndDialog(m_Wnd,0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK EditPropertyDlg::RexMaxEditPropertyDlgProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam)
{
	EditPropertyDlg* p_EditPropDlg = (EditPropertyDlg*)GetWindowLongPtr(DlgWnd,DWLP_USER);

	switch(Msg)
	{
	case WM_INITDIALOG:

		SetWindowLongPtr(DlgWnd,DWLP_USER,LParam);
		p_EditPropDlg = (EditPropertyDlg*)LParam;
		p_EditPropDlg->OnInitDialog(DlgWnd);
		return TRUE;

	case WM_ACTIVATE:

		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(WParam))
		{
		case IDOK:
			p_EditPropDlg->OnCommandOK();
			break;
		case IDCANCEL:
			p_EditPropDlg->OnCommandCancel();
			break;
		}

		return TRUE;

	case WM_CLOSE:
		p_EditPropDlg->OnCommandCancel();
		return TRUE;
	}

	return FALSE;
}
