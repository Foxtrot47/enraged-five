#include "wrapper.h"
#include "setup.h"
#include "resource.h"
#include "rexMaxUtility/utility.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

static const s32 MenuContext = 0x1bf80209;
static const s32 ActionContext = 0x58824075;
static const s32 ActionTableID = 0x4aa30b4a;
static TSTR MenuName = _T("Rex");
const atString RexMaxGraphItem::RootName("Root");

ActionTable* GetActions();

#define REXMAXUIEXPORT_CLASS_ID	Class_ID(0x296453f, 0x65674239)

/////////////////////////////////////////////////////////////////////////////////////////////////////
static ActionDescription g_Actions[] = {

	ID_CMD_SETUP,
	IDS_CMD_SETUP,
	IDS_CMD_SETUP,
	IDS_CMD_CAT,

	ID_CMD_EXPORT,
	IDS_CMD_EXPORT,
	IDS_CMD_EXPORT,
	IDS_CMD_CAT
};

/////////////////////////////////////////////////////////////////////////////////////////////////////
class RexMaxActionTable : public ActionTable
{
public:
	RexMaxActionTable(	ActionTableId id, 
					ActionContextId contextId, 
					TSTR& name, 
					HACCEL hDefaults, 
					int numIds, 
					ActionDescription* pOps, 
					HINSTANCE hInst):
		ActionTable(id, 
					contextId, 
					name, 
					hDefaults, 
					numIds, 
					pOps, 
					hInst) {}

	void DeleteThis() { delete this; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////
class RexMaxActionCB : public ActionCallback
{
public:
	BOOL ExecuteAction(int ID)
	{
		switch(ID) 
		{
		case ID_CMD_SETUP:
			RexMaxWrapper::GetInst().DoSetup();
			return TRUE;

		case ID_CMD_EXPORT:
			RexMaxWrapper::GetInst().DoExport();
			return TRUE;
		}

		return FALSE;
	}
};

RexMaxActionCB g_ActionCB;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
class RexMaxUIRageClassDesc : public ClassDesc2
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return &RexMaxWrapper::GetInst(); }
	const TCHAR *	ClassName() { return _T("rexmax ui"); }
	SClass_ID		SuperClassID() { return GUP_CLASS_ID; }
	Class_ID		ClassID() { return REXMAXUIEXPORT_CLASS_ID; }
	const TCHAR* 	Category() { return _T("export"); }
	HINSTANCE		HInstance() { return g_hDllInstance; }

	int				NumActionTables() { return 1; }
	ActionTable*	GetActionTable(int i) { return RexMaxWrapper::GetActions(); }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////
ClassDesc2* RexMaxWrapper::Creator()
{
	static RexMaxUIRageClassDesc ClassDesc;

	return &ClassDesc;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
RexMaxWrapper& RexMaxWrapper::GetInst()
{
	return g_Wrapper;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
RexMaxWrapper::RexMaxWrapper():
	m_GraphRoot(RexMaxGraphItem::RootName,true)
{
	rexResult::SetShell(&m_Shell);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
DWORD RexMaxWrapper::Start()
{
	RegisterModules(m_Shell);

	rexUtility::Startup();
	rexUtility::SetTextureConvertFlag(false);

	//

	IMenuManager* p_MenuMgr = GetCOREInterface()->GetMenuManager();
	IActionManager* p_ActionMgr = GetCOREInterface()->GetActionManager();

	p_ActionMgr->ActivateActionTable(&g_ActionCB,ActionTableID);

	if(!p_MenuMgr->RegisterMenuBarContext(MenuContext,MenuName))
	{
		return GUPRESULT_KEEP;
	}

	IMenuBarContext* p_MainContext = (IMenuBarContext*)p_MenuMgr->GetContext(kMenuContextMenuBar);
	IMenu* p_Menu = p_MenuMgr->FindMenu(MenuName);

	if(p_Menu)
	{
		p_MenuMgr->UnRegisterMenu(p_Menu);
	}

	p_Menu = GetIMenu();	
	p_Menu->SetTitle(MenuName);
	p_MenuMgr->RegisterMenu(p_Menu,0);

	ActionTable* p_ActionTable = GetCOREInterface()->GetActionManager()->FindTable(ActionTableID);
	s32 i,Count = sizeof(g_Actions) / sizeof(ActionDescription);

	for(i=0;i<Count;i++)
	{
		IMenuItem* p_MenuItem = GetIMenuItem();

		ActionItem* p_Action = p_ActionTable->GetAction(g_Actions[i].mCmdID);
		if(p_Action)
		{
			p_MenuItem->SetActionItem(p_Action);
			p_Menu->AddItem(p_MenuItem);
		}
	}

	IMenuBarContext* p_Context = (IMenuBarContext*) p_MenuMgr->GetContext(MenuContext);
	p_Context->SetMenu(p_Menu);
	p_Context->CreateWindowsMenu();

	IMenuItem* p_SubMenuItem = GetIMenuItem();
	p_SubMenuItem->SetSubMenu(p_Menu);

	IMenu* p_MainMenu = p_MenuMgr->GetMainMenuBar();
	p_MainMenu->AddItem(p_SubMenuItem);

	p_MenuMgr->UpdateMenuBar();

	return GUPRESULT_KEEP;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxWrapper::Stop()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
IOResult RexMaxWrapper::Save(ISave* p_ISave)
{
	AppSave* p_AppSave = NewAppSave(1024);
	
	p_AppSave->BeginChunk(RexMaxGraphItem::CHUNK); //graph block

	m_GraphRoot.FixupSave(p_ISave,p_AppSave);

	p_AppSave->EndChunk();

	ULONG Written;
	ULONG ToWrite;
	p_ISave->Write(p_AppSave->BufferPtr(),p_AppSave->NBytesWritten(),&Written);

	ToWrite = p_AppSave->NBytesWritten();

	p_AppSave->DeleteThis();

	if(Written != ToWrite)
	{
		return IO_ERROR;
	}

	return IO_OK;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
IOResult RexMaxWrapper::Load(ILoad* p_ILoad)
{
	p_ILoad->OpenChunk();

	USHORT ChunkID = p_ILoad->CurChunkID();

	if(ChunkID != RexMaxGraphItem::CHUNK)
	{
		return IO_ERROR;
	}

	ULONG ToRead = (ULONG)p_ILoad->CurChunkLength();

	unsigned char* p_Data = new unsigned char[ToRead];
	ULONG Read;

	p_ILoad->Read(p_Data,ToRead,&Read);

	if(Read != ToRead)
	{
		delete[] p_Data;
		return IO_ERROR;
	}

	AppLoad* p_AppLoad = NewAppLoad(p_Data,ToRead);

	m_GraphRoot.Clear();
	m_GraphRoot.FixupLoad(p_ILoad,p_AppLoad);

	SetupDlg::GetInst().Update();

	p_AppLoad->DeleteThis();
	delete[] p_Data;

	p_ILoad->CloseChunk();

	return IO_OK;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxWrapper::ShowLog()
{
	m_ExportLog.Open(GetCOREInterface()->GetMAXHWnd());
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxWrapper::DoSetup()
{
	SetupDlg::GetInst().Show();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult RexMaxWrapper::DoExport(bool ShowProgress)
{
	atArray<rexObject*> inputObjects;
	rexResult result(rexResult::PERFECT);

	//m_ExportLog.Open(GetCOREInterface()->GetMAXHWnd());
	m_ExportLog.AddString("Starting Export");

	m_ShowProgress = ShowProgress;

	result = m_Shell.Export(inputObjects,false,false,NULL);

	s32 i,Count = result.GetMessageCount();

	for(i=0;i<Count;i++)
	{
		const atString& rMsg = result.GetMessageByIndex(i);

		m_ExportLog.AddString(rMsg);
	}

	switch(result.GetResultType())
	{
	case rexResult::PERFECT:
		m_ExportLog.AddString("No Warnings or Errors");
		break;
	case rexResult::WARNINGS:
		m_ExportLog.AddString("Warnings");
		break;
	case rexResult::ERRORS:
		m_ExportLog.AddString("Errors");
		break;
	}

	m_ExportLog.AddString("Ending Export");

	return result;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxWrapper::Reset()
{
	m_GraphRoot.Clear();
	SetupDlg::GetInst().Update();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool RexMaxWrapper::IsValidItem(RexMaxGraphItem* p_GraphItem)
{
	return m_GraphRoot.IsValidItem(p_GraphItem);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
ActionTable* RexMaxWrapper::GetActions()
{
	static ActionTable* p_ActionTable = NULL;

	if(p_ActionTable != NULL)
	{
		return p_ActionTable;
	}

	p_ActionTable = new RexMaxActionTable(	ActionTableID,
											ActionContext,
											MenuName,
											NULL,
											sizeof(g_Actions) / sizeof(ActionDescription),
											g_Actions,
											g_hDllInstance);

	GetCOREInterface()->GetActionManager()->RegisterActionContext(ActionContext,MenuName);

	return p_ActionTable;
}
