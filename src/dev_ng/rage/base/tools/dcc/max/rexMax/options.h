#ifndef __REX_MAXROCKSTAR_OPTIONS_H__
#define __REX_MAXROCKSTAR_OPTIONS_H__

#include "atl/string.h"

namespace rage {

/* PURPOSE:
	singleton containing all major export options (i.e. which files to export).
	also contains functionality to bring up a dialog which can set these options
*/
class ExportOptions
{
public:
	static ExportOptions& GetInst();

	/*	PURPOSE
			brings up the dialog to edit export options
		PARAMS
			ParentWnd - handle to the parent of the options dialog
		RETURNS
			true on user pressing ok in dialog
	*/
	bool ShowDialog(HWND ParentWnd);

	/*	PURPOSE
			each of the following is a get function for an export option
		RETURNS
			export parameter
	*/
	bool GetShowLog() const { return m_ShowLog; }
	bool GetShowProgress() const { return m_ShowProgress; }
	bool GetWarningsAsErrors() const { return m_WarningsAsErrors; }
	bool GetExportEntity() const { return m_ExportEntity; }
	bool GetExportMesh() const { return m_ExportMesh; }
	bool GetExportBounds() const { return m_ExportBounds; }
	bool GetExportAnimation() const { return m_ExportAnimation; }
	bool GetExportSkeleton() const { return m_ExportSkeleton; }
	bool GetExportTextures() const { return m_ExportTextures; }
	bool GetExportMeshAscii() const { return m_ExportMeshAscii; }
	bool GetExportSelected() const { return m_ExportSelected; }
	const atString& GetOutputPath() const { return m_OutputPath; }
	const atString& GetTexturePath() const { return m_TexturePath; }

	/*	PURPOSE
			each of the following is a set function for an export option
		PARAMS
			options to set
	*/
	void SetShowLog(bool ShowLog) { m_ShowLog = ShowLog; }
	void SetShowProgress(bool ShowProgress) { m_ShowProgress = ShowProgress; }
	void SetWarningsAsErrors(bool WarningsAsErrors) { m_WarningsAsErrors = WarningsAsErrors; }
	void SetExportEntity(bool ExportEntity) { m_ExportEntity = ExportEntity; }
	void SetExportMesh(bool ExportMesh) { m_ExportMesh = ExportMesh; }
	void SetExportBounds(bool ExportBounds) { m_ExportBounds = ExportBounds; }
	void SetExportAnimation(bool ExportAnimation) { m_ExportAnimation = ExportAnimation; }
	void SetExportSkeleton(bool ExportSkeleton) { m_ExportSkeleton = ExportSkeleton; }
	void SetExportTextures(bool ExportTextures) { m_ExportTextures = ExportTextures; }
	void SetExportMeshAscii(bool ExportMeshAscii) { m_ExportMeshAscii = ExportMeshAscii; }
	void SetExportSelected(bool ExportSelected) { m_ExportSelected = ExportSelected; }
	void SetOutputPath(const atString& r_OutputPath) { m_OutputPath = r_OutputPath; }
	void SetTexturePath(const atString& r_TexturePath) { m_TexturePath = r_TexturePath; }

	/*	PURPOSE
			save current export options to the registry
	*/
	void SaveExportOptions();

	/*	PURPOSE
			restore current export options from the registry
	*/
	void RestoreExportOptions();

protected:
	ExportOptions();
	~ExportOptions();

	/*	PURPOSE
			internal windows update procedure
		PARAMS
			hWnd - handle to window that should handle this message
			uMsg - message to handle
			wParam - message data
			lParam - message data
		RETURNS
			return code from message
	*/
	static INT_PTR CALLBACK OptionsDialogProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam);

	/*	PURPOSE
			called from LogWindowProc to handle a WM_INITDIALOG message
	*/
	void OnInitDialog();

	/*	PURPOSE
			called from LogWindowProc to handle a WM_COMMAND message for OK button
	*/
	void OnCommandOk();

	/*	PURPOSE
			called from LogWindowProc to handle a WM_COMMAND message for Cancel button
	*/
	void OnCommandCancel();

	HWND m_DlgWnd;

	bool m_ShowLog;
	bool m_ShowProgress;
	bool m_WarningsAsErrors;
	bool m_ExportEntity;
	bool m_ExportMesh;
	bool m_ExportBounds;
	bool m_ExportAnimation;
	bool m_ExportSkeleton;
	bool m_ExportTextures;
	bool m_ExportMeshAscii;
	bool m_ExportSelected;

	atString m_OutputPath;
	atString m_TexturePath;
};

} //namespace rage {

#endif //__REX_MAXROCKSTAR_OPTIONS_H__