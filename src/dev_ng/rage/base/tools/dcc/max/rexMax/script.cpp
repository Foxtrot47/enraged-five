#include "MaxUtil/MaxUtil.h"
#include "file/device.h"
#include "file/token.h"
#include "file/stream.h"
#include "file/asset.h"
#include "atl/string.h"
#include "atl/array.h"
#include "cranimation/animation.h"
#include "cranimation/animtrack.h"
#include "rexMaxBoundMtl/boundmtl.h"

// Used for converting between Rage and Nitro material types
#include "rageMaxMaterial/shaderInstance.h"
#include "rageMaxNitroMtl/rageMaxNitroMtl.h"
#include "rstMaxMtl/mtl.h"

#include "rexMaxUtility/utility.h"
#if HACK_GTA4
	//Required for rexStdMatShaderName_cf
#include "rexMaxUtility/defshader.h"
#include "rexMaxToGeneric/converterSkeleton.h"
#endif // HACK_GTA4
#include "rexMax/wrapper.h"
#include <IPathConfigMgr.h>

#include <maxscript/maxscript.h>
#include <maxscript/maxwrapper/mxsmaterial.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/foundation/Numbers.h>
#include <maxscript/foundation/structs.h>
#include <maxscript/foundation/hashtable.h>
#include <maxscript/foundation/3dmath.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
// GD actually this now last, to override the def_name macro to define not as external Value.
#undef def_name
#define def_name(name)	Value *n_##name = Name::intern(_M(#name));

#include "vectormath/legacyconvert.h"

// libtiff headers
#include "libtiff/tiff.h" // for compression constants

using namespace rage;

def_visible_primitive(rexBindPose,"rexBindPose");
def_visible_primitive(rexDeleteDirectory,"rexDeleteDirectory");

def_visible_primitive(rexExport,"rexExport");
def_visible_primitive(rexReset,"rexReset");
def_visible_primitive(rexGetRoot,"rexGetRoot");
def_visible_primitive(rexGetChildren,"rexGetChildren");
def_visible_primitive(rexAddChild,"rexAddChild");
def_visible_primitive(rexRemove,"rexRemove");
def_visible_primitive(rexGetNodes,"rexGetNodes");
def_visible_primitive(rexSetNodes,"rexSetNodes");
def_visible_primitive(rexGetNodeType,"rexGetNodeType");
def_visible_primitive(rexGetNodeName,"rexGetNodeName");
def_visible_primitive(rexGetProperties,"rexGetProperties");
def_visible_primitive(rexGetPropertyValue,"rexGetPropertyValue");
def_visible_primitive(rexSetPropertyValue,"rexSetPropertyValue");
def_visible_primitive(rexGetSkinRootBone,"rexGetSkinRootBone");
def_visible_primitive(rexGetSkinBoneList,"rexGetSkinBoneList");
def_visible_primitive(rexSetTextureLimit,"rexSetTextureLimit"); // Max texture size
def_visible_primitive(rexSetTextureShift,"rexSetTextureShift"); // Bit shift the texture size
def_visible_primitive(rexGetTextureShift,"rexGetTextureShift");
def_visible_primitive(rexSetForceTextureExport,"rexSetForceTextureExport");
def_visible_primitive(rexGetForceTextureExport,"rexGetForceTextureExport");
def_visible_primitive(rexExportTexture,"rexExportTexture");
def_visible_primitive(rexExportRawTextureAsTiff, "rexExportRawTextureAsTiff");
def_visible_primitive(rexHasAlpha,"rexHasAlpha");
def_visible_primitive(rexIsTexMapAnimated,"rexIsTexMapAnimated");
def_visible_primitive(rexLoadCamera,"rexLoadCamera");
def_visible_primitive(rexGetBoneHash,"rexGetBoneHash");
def_visible_primitive(rexDisableSerialisation,"rexDisableSerialisation");
#if HACK_GTA4
def_visible_primitive(rexAreMatsEqual,"rexAreMatsEqual"); // Material comparison
def_visible_primitive(rexStdMatShaderName,"rexStdMatShaderName"); // Get shader to replace std material where one exists, see rexMaxUtility/defshader.cpp
def_visible_primitive(rexGenerateColourPalette, "rexGenerateColourPalette");
def_visible_primitive(rexSetULogFile, "rexSetULogFile");
#endif // HACK_GTA4
def_visible_primitive(rexFlushSkelCache, "rexFlushSkelCache");
def_visible_primitive(rexConvertRageMtlToNitro, "rexConvertRageMtlToNitro");
def_visible_primitive(rexConvertNitroMtlToRage, "rexConvertNitroMtlToRage");
def_visible_primitive(rexSetReportFile, "rexSetReportFile");
def_visible_primitive(rexReplaceWhitespaces, "rexReplaceWhitespaces");

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexBindPose_cf(Value** arg_list, int count)
{
	type_check(arg_list[0],MAXNode,"rexBindPose [node to set bind pose on/off] [on/off value for bind pose]");
	type_check(arg_list[1],Boolean,"rexBindPose [node to set bind pose on/off] [on/off value for bind pose]");

	INode* p_Node = arg_list[0]->to_node();
	bool BindPose = arg_list[1]->to_bool();

	rexMaxUtility::SetSkinBindPose(p_Node,BindPose);

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
#if ( MAX_RELEASE < 9000 )
int	default_eq_fn(void* key1, void* key2)
{
	return key1 == key2;
}

INT_PTR default_hash_fn(void* key)
{
	return (INT_PTR)key;
}
#endif
//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexDeleteDirectory_cf(Value** arg_list, int count)
{
	type_check(arg_list[0],String,"rexDeleteDirectory [directory to delete]");

	char* p_Directory = arg_list[0]->to_string();

	if(fiDeviceLocal::GetInstance().DeleteDirectory(p_Directory,true))
	{
		return &true_value;
	}

	return &false_value;
}

#ifdef _DEBUG
//////////////////////////////////////////////////////////////////////////////////////////////////////
int CustomAllocHook(int nAllocType,
					void *userData,
					size_t size,
					int nBlockType,
					long requestNumber,
					const unsigned char *filename,
					int lineNumber)
{
	return TRUE;
}
#endif // _DEBUG

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexExport_cf(Value** arg_list, int count)
{
	bool DisplayProgress = false;

	bool useUniLog = false;
	TSTR logFile = "";
	if(count > 1)
	{
		useUniLog = true;
		logFile = arg_list[1]->to_string();
		// Build up a new path to our Rex log file.
		// What comes through from Max is the global log file so strip out just the path and build a new log file in that path.
		char path[MAX_PATH];
		char processLogFile[MAX_PATH];
		ASSET.RemoveNameFromPath(path, MAX_PATH, logFile);

		DWORD processID = GetCurrentProcessId();
		
		formatf(processLogFile, MAX_PATH, "%s\\RexExport.%d.ulog", path, processID);
		RexMaxWrapper::GetInst().GetShell().SetULogFilePath(processLogFile);
	}

	if(count > 0)
		DisplayProgress = arg_list[0]->to_bool();
	rexResult res = RexMaxWrapper::GetInst().DoExport(DisplayProgress);

#ifdef _DEBUG

	_CrtMemState memstate1,memstate2,memstate3;
	HANDLE hLogFile = CreateFile("c:\\log.txt", GENERIC_WRITE,FILE_SHARE_WRITE, NULL, CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL, NULL);

	_CrtSetAllocHook(CustomAllocHook);
	_CrtSetReportMode(_CRT_WARN,_CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_WARN,hLogFile);
	_CrtMemCheckpoint(&memstate1);

#endif //#ifdef _DEBUG

	

#ifdef _DEBUG

	_CrtMemCheckpoint(&memstate2);

	if(_CrtMemDifference(&memstate3,&memstate1,&memstate2))
	{
		_CrtMemDumpAllObjectsSince(&memstate1);
	}

	CloseHandle(hLogFile);

#endif //#ifdef _DEBUG



	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexReset_cf(Value** arg_list, int count)
{
	RexMaxWrapper::GetInst().Reset();

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexSetReportFile_cf(Value** arg_list, int count)
{
	check_arg_count( "rexSetReportFile", 1, count );
	type_check( arg_list[0], String, "rexSetReportFile <filepath/name>" );
	rexReport &report = rexShell::Report();
	report.Open(arg_list[0]->to_string());
	return &ok;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexGetRoot_cf(Value** arg_list, int count)
{
	RexMaxGraphItem& r_GraphRoot = RexMaxWrapper::GetInst().GetGraphRoot();

	return_protected(IntegerPtr::intern((INT_PTR)&r_GraphRoot));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexGetChildren_cf(Value** arg_list, int count)
{
	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)arg_list[0]->to_intptr();

	if(!RexMaxWrapper::GetInst().IsValidItem(p_GraphItem))
	{
		return &undefined;
	}

	s32 NumChildren = p_GraphItem->GetNumChildren();

	RexMaxGraphItem* p_GraphChildItem = p_GraphItem->GetChild();
	Array* p_Ret = new Array(NumChildren);

	while(p_GraphChildItem)
	{
		p_Ret->append(new Integer((unsigned int)p_GraphChildItem));

		p_GraphChildItem = p_GraphChildItem->GetNext();
	}

	return p_Ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexAddChild_cf(Value** arg_list, int count)
{
	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)arg_list[0]->to_intptr();

	if(!RexMaxWrapper::GetInst().IsValidItem(p_GraphItem))
	{
		return &undefined;
	}

	const char* p_Name = arg_list[1]->to_string();
	const char* p_Module = arg_list[2]->to_string();

	RexMaxGraphItem* p_NewGraphItem = p_GraphItem->AddChild(atString(p_Name));
	p_NewGraphItem->SetType(atString(p_Module));
	
	return_protected(IntegerPtr::intern((INT_PTR)p_NewGraphItem));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexRemove_cf(Value** arg_list, int count)
{
	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)arg_list[0]->to_intptr();

	if(!RexMaxWrapper::GetInst().IsValidItem(p_GraphItem))
	{
		return &false_value;
	}

	if(p_GraphItem->IsLocked())
	{
		return &false_value;
	}

	delete p_GraphItem;

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexGetNodes_cf(Value** arg_list, int count)
{
	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)arg_list[0]->to_intptr();

	if(!RexMaxWrapper::GetInst().IsValidItem(p_GraphItem))
	{
		return &undefined;
	}

	atArray<INode*>& r_Nodes = p_GraphItem->GetNodes();
	s32 i,Count = r_Nodes.GetCount();
	Array* p_Ret = new Array(Count);

	for(i=0;i<Count;i++)
	{
		p_Ret->append(new MAXNode(r_Nodes[i]));
	}

	return p_Ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexSetNodes_cf(Value** arg_list, int count)
{
	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)arg_list[0]->to_intptr();

	if(!RexMaxWrapper::GetInst().IsValidItem(p_GraphItem))
	{
		return &false_value;
	}

	atArray<INode*> Nodes;

	def_name(exportNodes);
	Value* exportNodesVal = key_arg(exportNodes);
	if(exportNodesVal!=&unsupplied)
	{
		atString nodeHandleString = atString(exportNodesVal->to_string());
		atArray<atString> nodeHandleStrings;
		nodeHandleString.Split(nodeHandleStrings, "|");
		atArray<atString>::iterator it = nodeHandleStrings.begin();
		for (;it!=nodeHandleStrings.end();++it)
		{
			atString handleString = *it;
			int handle = atoi(handleString.c_str());
			Animatable *pAnim = Animatable::GetAnimByHandle( (AnimHandle)handle );
			INodePtr pNode = dynamic_cast<INodePtr>(pAnim);
			if(!pNode)
				RexMaxWrapper::GetInst().GetShell().ExternalLogError("Couldn't find node by anim handle!", handleString.c_str());
			else
				Nodes.PushAndGrow(pNode);
		}
	}
	else
		rexMaxUtility::GetSelectedNode(Nodes);

	if (Nodes.GetCount()<=0)
	{
		char msg[RAGE_MAX_PATH];
		sprintf_s(msg, RAGE_MAX_PATH, "No nodes were given to rex graph node %s.", p_GraphItem->GetName());
		RexMaxWrapper::GetInst().GetShell().ExternalLogError(msg, p_GraphItem->GetName());
	}

	p_GraphItem->SetNodes(Nodes);

	return &true_value;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexGetNodeType_cf(Value** arg_list, int count)
{
	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)arg_list[0]->to_intptr();

	if(!RexMaxWrapper::GetInst().IsValidItem(p_GraphItem))
	{
		return &undefined;
	}

	char Buffer[1024];
	const atString& type = p_GraphItem->GetType();
	strcpy(Buffer,type);
	return_protected(new String(Buffer));
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexGetNodeName_cf(Value** arg_list, int count)
{
	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)arg_list[0]->to_intptr();

	if(!RexMaxWrapper::GetInst().IsValidItem(p_GraphItem))
	{
		return &undefined;
	}

	char Buffer[1024];
	const atString& name = p_GraphItem->GetName();
	strcpy(Buffer,name);
	return_protected(new String(Buffer));
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexGetProperties_cf(Value** arg_list, int count)
{
	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)arg_list[0]->to_intptr();

	if(!RexMaxWrapper::GetInst().IsValidItem(p_GraphItem))
	{
		return &undefined;
	}

	const atMap<atString,atString>& r_Properties = p_GraphItem->GetProperties();

	s32 i,Count = r_Properties.GetNumSlots();
	s32 NumProperties = 0;

	for(i=0;i<Count;i++)
	{
		const atMap<atString,atString>::Entry* p_Entry = r_Properties.GetEntry(i);

		while(p_Entry)
		{
			NumProperties++;

			p_Entry = p_Entry->next;
		}
	}

	Array* p_Ret = new Array(NumProperties);

	for(i=0;i<Count;i++)
	{
		const atMap<atString,atString>::Entry* p_Entry = r_Properties.GetEntry(i);

		while(p_Entry)
		{
			p_Ret->append(new String((char*)((const char*)p_Entry->key)));

			p_Entry = p_Entry->next;
		}
	}

	return p_Ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexGetPropertyValue_cf(Value** arg_list, int count)
{
	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)arg_list[0]->to_intptr();

	if(!RexMaxWrapper::GetInst().IsValidItem(p_GraphItem))
	{
		return &undefined;
	}

	const char* p_PropertyName = arg_list[1]->to_string();

	const atString* p_Value = p_GraphItem->GetProperties().Access(p_PropertyName);

	if(!p_Value)
	{
		return &undefined;
	}

	return_protected(new String((char*)((const char*)*p_Value)));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexSetPropertyValue_cf(Value** arg_list, int count)
{
	RexMaxGraphItem* p_GraphItem = (RexMaxGraphItem*)arg_list[0]->to_intptr();

	if(!RexMaxWrapper::GetInst().IsValidItem(p_GraphItem))
	{
		return &false_value;
	}

	atString p_PropertyName(arg_list[1]->to_string());
	atString p_PropertyValue(arg_list[2]->to_string());

	atMap<atString,atString>& r_Properties = p_GraphItem->GetProperties();

	atString* p_Value = r_Properties.Access(p_PropertyName);

	if(!p_Value)
	{
		r_Properties.Insert(p_PropertyName,p_PropertyValue);
		return &true_value;
	}

	*p_Value = p_PropertyValue;

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexGetSkinRootBone_cf(Value** arg_list, int count)
{
	INode* p_Node = arg_list[0]->to_node();

	INode* p_BoneNode = MaxUtil::GetSkinRootBone(MaxUtil::GetSkin(p_Node));

	if(!p_BoneNode)
	{
		return &undefined;
	}

	return_protected(new MAXNode(p_BoneNode));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexGetSkinBoneList_cf(Value** arg_list, int count)
{
	INode* p_Node = arg_list[0]->to_node();

	ISkin* p_Skin = MaxUtil::GetSkin(p_Node);

	if(!p_Skin)
	{
		return_protected(new Array(0));
	}

	Array* p_BoneList = new Array(p_Skin->GetNumBones());
	for(int i=0; i<p_Skin->GetNumBones(); i++)
	{
		INode* p_Bone = p_Skin->GetBone(i);
		p_BoneList->append(new MAXNode(p_Bone));
	}
	return_protected(p_BoneList);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexSetTextureLimit_cf(Value** arg_list, int count)
{
	s32 SizeLimit = arg_list[0]->to_int();
	rexMaxUtility::SetTextureSizeLimit(SizeLimit);
	return &true_value;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexSetTextureShift_cf(Value** arg_list, int count)
{
	s32 SizeLimit = arg_list[0]->to_int();
#if HACK_GTA4
	rexMaxUtility::SetTextureShiftDown(SizeLimit);
#endif //HACK_GTA4
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value*
rexGetTextureShift_cf( Value** arg_list, int count )
{
#if HACK_GTA4
	s32 shift = rexMaxUtility::GetTextureShiftDown( );
#else
	s32 shift = 0;
#endif
	return ( (new Integer( (int)shift ) ) );
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexSetForceTextureExport_cf(Value** arg_list, int count)
{
	check_arg_count( "rexSetForceTextureExport", 1, count );
	type_check( arg_list[0], Boolean, "enable force-texture export" );
	rexMaxUtility::SetForceTextureExport(arg_list[0]->to_bool());

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexGetForceTextureExport_cf( Value** arg_list, int count )
{
	check_arg_count( "rexGetForceTextureExport", 0, count );
	
	bool on = rexMaxUtility::GetForceTextureExport( );
	return ( on ? &true_value : &false_value );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexExportTexture_cf(Value** arg_list, int count)
{
	// fileIn "filename" [quiet:true]
	check_arg_count_with_keys("rexExportTexture", 2, count);

	const char* p_Input = arg_list[0]->to_string();
	const char* p_Output = arg_list[1]->to_string();

//	Value* n_keyName = new String("sizeLimit");
	def_name(sizeLimit);
	Value* sizeLimitVal = key_arg(sizeLimit);
	if(sizeLimitVal!=&unsupplied)
	{
		s32 SizeLimit = sizeLimitVal->to_int();
		rexMaxUtility::SetTextureSizeLimit(SizeLimit);
	}

// 	n_keyName = new String("isBump");
	def_name(isBump);
	Value* isBumpVal = key_arg_or_default(isBump, &false_value);
	bool isBump = isBumpVal->to_bool();

//	n_keyName = new String("hasAlpha");
	def_name(hasAlpha);
	Value* alphaVal = key_arg_or_default(hasAlpha, &false_value);
	bool hasAlpha = alphaVal->to_bool();

//	n_keyName = new String("raw");
	def_name(raw);
	Value* rawVal = key_arg_or_default(raw, &false_value);
	bool isRaw = rawVal->to_bool();

//	n_keyName = new String("unilogFile");
	def_name(uniLogFile);
	Value* filenameVal = key_arg(uniLogFile);
	if(filenameVal!=&unsupplied)
	{
		const char *logFile = filenameVal->to_string();
		RexMaxWrapper::GetInst().GetShell().SetULogFilePath(logFile);
	}

	rexResult res = rexMaxUtility::ExportTexture(p_Input,p_Output,hasAlpha,isBump,isRaw);

	if ( res == false )
	{
		//One of the textures could not be found.
		return &false_value;
	}
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* 
rexExportRawTextureAsTiff_cf( Value** arg_list, int count )
{
	// Ideally the texture export functions would be rolled into one; taking
	// an output format argument and optional parameters (e.g. compression, sub-format).
	check_arg_count_with_keys( "rexExportRawTextureAsTiff", 2, count );
	type_check( arg_list[0], String, "input image filename" );
	type_check( arg_list[1], String, "output texture filename" );

	//Value* compression = key_arg_or_default(compression, Integer::intern(COMPRESSION_LZW));
	Value* compression = Integer::intern( COMPRESSION_LZW );
	type_check( compression, Integer, "compression: libtiff compression integer (default: COMPRESSION_LZW, 5)");

	const char* p_Input = arg_list[0]->to_string();
	const char* p_Output = arg_list[1]->to_string();

	def_name(uniLogFile);
	Value* filenameVal = key_arg(uniLogFile);
	if(filenameVal!=&unsupplied)
	{
		const char *logFile = filenameVal->to_string();
		RexMaxWrapper::GetInst().GetShell().SetULogFilePath(logFile);
	}

	// No parameter to limit size; thats what the texture processing pipeline is for.
	// We return false if the texture failed to write; true if it was written or
	// not because the modified timestamp check was ok.
	if ( !rexMaxUtility::ExportTextureAsTiff( p_Input, p_Output, compression->to_int() ) )
		return ( &false_value );

	return ( &true_value );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexHasAlpha_cf(Value** arg_list, int count)
{
	INode* p_Node = arg_list[0]->to_node();
	Mtl* p_Mtl = arg_list[1]->to_mtl();

	if(rexMaxUtility::HasAlphaVertColour(p_Node,p_Mtl))
		return &true_value;
	else
		return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexIsTexMapAnimated_cf(Value** arg_list, int count)
{
	Texmap* p_texmap = arg_list[0]->to_texmap();

	s32 FirstFrame = s32(rexMaxUtility::GetAnimStartTime() * rexMaxUtility::GetFPS());
	s32 EndFrame = s32(rexMaxUtility::GetAnimEndTime() * rexMaxUtility::GetFPS());

	if(rexMaxUtility::IsTexMapAnimated(p_texmap,FirstFrame,EndFrame))
		return &true_value;
	else
		return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* rexLoadCamera_cf(Value** arg_list, int count)
{
	atArray<Matrix34> CameraTransforms;
	crAnimation* pAnim = crAnimation::AllocateAndLoad(arg_list[0]->to_string());
#if HACK_GTA4
	// Looks like a fix to revert a difference in resulting anim files to
	// the orignal orientation in max.  Possible result of us being z-up runtime?
	Matrix34 rot;
	rot.Identity();
	rot.RotateFullX(PI / 2.0f);
#endif // HACK_GTA4
	if(pAnim)
	{
		s32 i,Count = pAnim->GetNumTracks();
		s32 j,Frames = (s32)pAnim->GetNum30Frames();
		Matrix34 mat;
		f32 timeIncrement = 1.0f;

		for(j=0;j<Frames;j++)
		{
			mat.Identity();
			QuatV quat(V_IDENTITY);
			Vec3V vec(V_ZERO);
			f32 currentTime = timeIncrement * j;

			for(i=0;i<Count;i++)
			{
				crAnimTrack* pAnimTrack = pAnim->GetTrack(i);
				u8 track = pAnimTrack->GetTrack();
				u8 type = pAnimTrack->GetType();

				switch(track)
				{
				case kTrackCameraTranslation:
					pAnimTrack->EvaluateVector3(currentTime,vec);
					break;
				case kTrackCameraRotation:
					pAnimTrack->EvaluateQuaternion(currentTime,quat);
					break;
				}
			}

			mat.FromQuaternion(RCC_QUATERNION(quat));
#if HACK_GTA4
			mat.Dot3x3FromLeft(rot);
#endif // HACK_GTA4
			mat.d = RCC_VECTOR3(vec);

			CameraTransforms.PushAndGrow(mat);
		}
	}

	Array* p_rootList = new Array(CameraTransforms.GetCount());

	for(s32 i=0;i<CameraTransforms.GetCount();i++)
	{
		Matrix3 MatTemp;
		rexMaxUtility::CopyRageMatrixToMaxMatrix(CameraTransforms[i],MatTemp);

		Matrix3Value* p_mat = new Matrix3Value(MatTemp);

		p_rootList->append(p_mat);
	}

	return p_rootList;
}

Value* rexGetBoneHash_cf(Value** arg_list, int count)
{
	return_protected(Integer::intern(crAnimTrack::ConvertBoneNameToId(arg_list[0]->to_string())));
}

#if HACK_GTA4
// New material comparison and std shader swap functions
Value* rexAreMatsEqual_cf(Value** arg_list, int count)
{
	Mtl* p_MtlA = arg_list[0]->to_mtl();
	Mtl* p_MtlB = arg_list[1]->to_mtl();
	rexObjectGenericMesh::MaterialInfo MatA,MatB;
	bool IsDoubleSided;

	if(p_MtlA->ClassID() == REXBND_MATERIAL_CLASS_ID &&
		p_MtlB->ClassID() == REXBND_MATERIAL_CLASS_ID)
	{
		RexBoundMtlMax* p_bndMtlA = (RexBoundMtlMax*)p_MtlA;
		RexBoundMtlMax* p_bndMtlB = (RexBoundMtlMax*)p_MtlB;

		if(p_bndMtlA->IsSame(*p_bndMtlB))
			return &true_value;

		return &false_value;
	}

	// We want the fully flattened preset to diff.
	MatA.m_CollapsedMaterialPreset = true;
	MatB.m_CollapsedMaterialPreset = true;

#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	rexMaxUtility::GetMaterialInfo(p_MtlA,MatA,IsDoubleSided,false,NULL);
	rexMaxUtility::GetMaterialInfo(p_MtlB,MatB,IsDoubleSided,false,NULL);
#else
	rexMaxUtility::GetMaterialInfo(p_MtlA,MatA,IsDoubleSided,false);
	rexMaxUtility::GetMaterialInfo(p_MtlB,MatB,IsDoubleSided,false);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS

	if(MatA.IsSame(MatB))
		return &true_value;

	return &false_value;
}

Value* rexStdMatShaderName_cf(Value** arg_list, int count)
{
	atString name;

	Mtl* p_Mtl = arg_list[0]->to_mtl();

	if(p_Mtl->IsSubClassOf(Class_ID(DMTL_CLASS_ID,0)))
	{
		StdMat* p_StdMtl = (StdMat*)p_Mtl;

		DefShader::GetInst().GetShaderName(p_StdMtl,name);
	}

	return_protected(new String((char*)((const char*)name)));
}

Value* rexGenerateColourPalette_cf(Value** arg_list, int count)
{
	type_check( arg_list[0], Array, "Meshes" );
	type_check( arg_list[1], Integer, "Max Palette Size" );
	type_check( arg_list[2], Integer, "Min Palette Width in pixels" );
	type_check( arg_list[3], String, "Palette filename" );
	
	Array* pMeshArray = static_cast<Array*>( arg_list[0] );
	if ( pMeshArray->size < 1 )
		return ( &false_value );

	int maxPaletteSize = arg_list[1]->to_int();
	int minPixelWidth = arg_list[2]->to_int();
	const char* p_OutputFilename = arg_list[3]->to_string();
	atArray<int, 0, u32> indexList;
	atArray<Mesh*> meshes;
	for( int i=1; i<=pMeshArray->size; i++ )
	{
		type_check( pMeshArray->get(i), MeshValue, "Mesh" );
		meshes.PushAndGrow( pMeshArray->get(i)->to_mesh() );
	}

	if( rexMaxUtility::GenerateColourPalette( meshes, indexList, maxPaletteSize, minPixelWidth, atString( p_OutputFilename ) ) )
	{
		Array* retIdxList = new Array(indexList.GetCount());
		for( int i=0; i< indexList.GetCount(); i++ )
		{
			retIdxList->append(new Integer(indexList[i]));
		}
		
		return retIdxList;
	}

	return &false_value;
}

Value* rexConvertRageMtlToNitro_cf(Value** arg_list, int count)
{
	check_arg_count( "rexConvertRageMtlToNitro", 1, count );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(RSTMATERIAL_CLASS_ID == p_MtlBase->ClassID())
	{
		RstMtlMax* pRageMtl = (RstMtlMax*)p_MtlBase;
		RageMaxNitroMtl* pNitroMtl = new RageMaxNitroMtl(pRageMtl->GetShaderInstanceData(), pRageMtl->GetCurrentShaderName());

		return MAXMaterial::intern( pNitroMtl );
	}
	return &undefined;
}

Value* rexConvertNitroMtlToRage_cf(Value** arg_list, int count)
{
	check_arg_count( "rexConvertNitroMtlToRage", 1, count );

	Mtl* p_MtlBase = arg_list[0]->to_mtl();

	if(RAGE_MAX_NITRO_MTL_CLASS_ID == p_MtlBase->ClassID())
	{
		RageMaxNitroMtl* pNitroMtl = (RageMaxNitroMtl*)p_MtlBase;
		RstMtlMax* pRageMtl = new RstMtlMax(pNitroMtl->GetShaderInstanceData(), pNitroMtl->GetShaderName());
		
		return MAXMaterial::intern( pRageMtl );
	}
	return &undefined;
}

Value *rexDisableSerialisation_cf(Value** arg_list, int count)
{
	check_arg_count( "rexDisableSerialisation", 1, count );
	type_check( arg_list[0], Boolean, "DontSerialise" );

	bool disableSerialisation = arg_list[0]->to_bool();
	rexShell::DontSerialise(disableSerialisation);

	if(disableSerialisation)
	{
// 		Value *n_tempPath;
// 		def_name(tempPath);
// 		Value *pPathVal = key_arg(tempPath);
// 		if(&unsupplied != pPathVal)
// 		{
// 			type_check(pPathVal, String, "tempPath:");
// 			char *path = pPathVal->to_string();
// 			rexShell::SetTempExportPath(path);
// 		}
// 		else
		{
			const MCHAR *tempDir = IPathConfigMgr::GetPathConfigMgr()->GetDir(APP_TEMP_DIR);
//			mprintf("Setting export directory to %s. \n", tempDir);
			rexShell::SetTempExportPath(tempDir);
			return new String(tempDir);
		}
	}
	return &ok;
}

Value *rexSetULogFile_cf(Value** arg_list, int count)
{
	check_arg_count( "rexSetULogFile", 1, count );
	type_check( arg_list[0], String, "rexSetULogFile <filepath>" );
	if(RexMaxWrapper::GetInst().GetShell().SetULogFilePath(arg_list[0]->to_string()))
		return &true_value;
	return &false_value;
}

#endif // HACK_GTA4

Value *rexFlushSkelCache_cf(Value** arg_list, int count)
{
	check_arg_count( "rexFlushSkelCache", 0, count );
	
	rexConverterMaxSkeleton::MaxSkeletonIterator::FlushCachedData();
	
	return &ok;
}

Value *rexReplaceWhitespaces_cf(Value** arg_list, int count)
{
	check_arg_count( "rexReplaceWhitespaces", 1, count );
	type_check( arg_list[0], String, "rexReplaceWhitespaces <spaced string>" );

	atString saveString = rexMaxUtility::ReplaceWhitespaceCharacters(arg_list[0]->to_string());

	return_protected(new String(saveString.c_str()));
}