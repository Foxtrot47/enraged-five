#ifndef __REX_MAX_GRAPHITEM_H__
#define __REX_MAX_GRAPHITEM_H__

#include "atl/string.h"
#include "atl/array.h"
#include "atl/map.h"
#include "file/stream.h"

namespace rage {

/* PURPOSE:
	Structure for the internal storage of the rex export item data.
	The root of this tree is stored and serialised in RexMaxWrapper
*/
class RexMaxGraphItem : public ReferenceMaker
{
public:
	friend class RexMaxWrapper;

	enum
	{
		CHUNK = 801,
		CHUNK_ITEM,
		CHUNK_DATA,
		CHUNK_NEXT,
		CHUNK_CHILD
	};

	/* PURPOSE:
		Used by FixupLoad
	*/
	class GraphCallback : public PostLoadCallback
	{
	public:
		GraphCallback(RexMaxGraphItem* p_Item): mp_Item(p_Item) {}

		void proc(ILoad *iload)
		{
			mp_Item->SetupReferences();
			delete this;
		}

		RexMaxGraphItem* mp_Item;
	};

	RexMaxGraphItem(const atString& r_Name,bool IsLocked = false);
	~RexMaxGraphItem();

	/*	PURPOSE
			adds a child of this node and returns a pointer to the new item
	*/
	RexMaxGraphItem* AddChild(const atString& r_Name);

	/*	PURPOSE
			adds a sibling of this node and returns a pointer to the new item
	*/
	RexMaxGraphItem* AddSibling(const atString& r_Name);

	/*	PURPOSE
			returns the first child of this node
	*/
	RexMaxGraphItem* GetChild() const { return mp_Child; }

	/*	PURPOSE
			returns the next sibling of this item
	*/
	RexMaxGraphItem* GetNext() const { return mp_Next; }

	/*	PURPOSE
			returns if this graphitem is locked from being deleted.
			used for the root graphitem which should not be deleted
	*/
	bool IsLocked() const { return m_IsLocked; }

	/*	PURPOSE
			returns true if the passed in pointer is part of the graph.
			used to do some validity checking on the pointers that are passed from maxscript
	*/
	bool IsValidItem(RexMaxGraphItem* p_GraphItem);

	/*	PURPOSE
			returns the number of direct children of this item
	*/
	s32 GetNumChildren();

	/*	PURPOSE
			the name of the graph item. in many cases this will become the name of the output file
	*/
	const atString& GetName() const { return m_Name; }

	/*	PURPOSE
			get the module type of this graph item
	*/
	const atString& GetType() const { return m_Type; }

	/*	PURPOSE
			get a reference to the list of contained node
	*/
	atArray<INode*>& GetNodes();

	/*	PURPOSE
			get a reference to the map of properties
	*/
	atMap<atString,atString>& GetProperties() { return m_Properties; }

	void SetName(const atString& r_Name) { m_Name = r_Name; }
	void SetType(const atString& r_Type) { m_Type = r_Type; }
	void SetNodes(const atArray<INode*>& r_NodeList);
	void SetProperties(const atMap<atString,atString>& r_Properties) { m_Properties = r_Properties; }

	/*	PURPOSE
			completely reset this graphitem, including deleting all children
	*/
	void Clear();

	/*	PURPOSE
			checks that all the inode pointers that this graph item references
			are valid (part of the max scene graph)
	*/
	void VerifyReferences();

	//from ReferenceMaker
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message);
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	//

protected:
	void SetupReferences();

	IOResult FixupSave(ISave* p_Save,AppSave* p_AppSave);
	IOResult FixupLoad(ILoad* p_Load,AppLoad* p_AppLoad);

	void DeleteSiblings();
	void DeleteChildren();

	s32 GetDataSize();
	static s32 GetStringLength(const atString& r_Write);
	static void WriteString(fiStream* p_Save,const atString& r_Write);
	static void ReadString(fiStream* p_Load,atString& r_Read);
	static bool IsNodeValid(INode* p_Node,INode* p_Parent = NULL);

	atString m_Name;
	atString m_Type;
	atArray<INode*> m_NodeList;
	atMap<atString,atString> m_Properties;

	RexMaxGraphItem* mp_Next;
	RexMaxGraphItem* mp_Previous;
	RexMaxGraphItem* mp_Child;
	RexMaxGraphItem* mp_Parent;
	bool m_IsLocked;

	static const atString RootName;
};

} //namespace rage {

#endif //__REX_MAX_GRAPHITEM_H__
