#ifndef __RST_MAX_DIALOGMTL_H__
#define __RST_MAX_DIALOGMTL_H__

#include "rstMaxMtl/dialogShader.h"
#include "rstMaxMtl/mtl.h"

namespace rage {

class RstMtlMax;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
class RstMaxMtlDlg : public ParamDlg
{
public:
	friend class RstMtlMax;
	friend class RstMaxShaderDlg;
	friend class RstMaxMtlPresetDlg;

	RstMaxMtlDlg(RstMtlMax* p_Mtl,IMtlParams* p_MtlParams);
	virtual ~RstMaxMtlDlg();

	// from ParamDlg
	virtual Class_ID ClassID() { return RSTMATERIAL_CLASS_ID; }
	virtual void SetThing(ReferenceTarget *m);
	virtual ReferenceTarget* GetThing() { return mp_Mtl; }
	virtual void SetTime(TimeValue t) {}
	virtual	void ReloadDialog() {}
	virtual void DeleteThis() { delete this; }
	virtual void ActivateDlg(BOOL onOff) {}
	virtual int FindSubTexFromHWND(HWND hwnd);

	static INT_PTR CALLBACK RstMaxParamDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);
	static INT_PTR CALLBACK RstMaxAboutDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);

	BOOL SetDialogShaderFlag(int cntrlID, BOOL check=TRUE)
	{
		if(!m_Wnd)
			return FALSE;
		return CheckDlgButton(m_Wnd, cntrlID, check);
	}

protected:
	void Reset();
	void SetupUI();
	void SetupShader(const atString& r_ShaderName,bool IsPreset);
		
	void OnInitDialog(HWND DlgWnd);
	void OnShaderNameSelChange(HWND ListWnd);
	void OnPresetNameSelChange(HWND ListWnd);
	void OnRadioPreset();
	void OnRadioCustom();
	void OnButtonAbout();
	void OnButtonTwoSided();
	void OnButtonTexScale();
	void OnButtonLighting();
	void OnButtonAmbLighting();
	void OnButtonDiffuseAlpha();
	void OnTexScaleSelChange(HWND ListWnd);

	void SetPresetName(const char *preset);

	HWND m_Wnd;
	RstMtlMax* mp_Mtl;
};

} // namespace rage {

#endif //__RST_MAX_DIALOGMTL_H__
