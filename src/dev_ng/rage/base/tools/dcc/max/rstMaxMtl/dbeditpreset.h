#ifndef __RST_DBEDITPRESET_H__
#define __RST_DBEDITPRESET_H__

#include "atl/string.h"
#include "rstMaxMtl/maxeditdlg.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

namespace rage {

struct rageRstShaderDbEntry;

////////////////////////////////////////////////////////////////////////////////
class DbEditPreset
{
public:
	DbEditPreset();

	bool DoModal(HWND ParentWnd,rageRstShaderDbEntry* p_Entry);

protected:
	static s32 g_DialogWidth;
	static s32 g_ControlWidth;

	rageRstShaderDbEntry* mp_Entry;
	MaxEditDialog m_Dialog;
	atString m_Comment;
	atArray<f32> m_Floats;
	atArray<s32> m_Ints;
	atArray<Vector2> m_V2s;
	atArray<Vector3> m_V3s;
	atArray<Vector4> m_V4s;
	atArray<atString> m_Strings;
	atArray<bool> m_Lockeds;
};

} //namespace rage {

#endif //__RST_DBEDITPRESET_H__