#ifndef __RSTMAX_MAXEDITDLG_H__
#define __RSTMAX_MAXEDITDLG_H__

#include "rstMaxMtl/dialog.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////
struct WidgetRect
{
	WidgetRect();
	WidgetRect(s32 IX,s32 IY,s32 IWidth,s32 IHeight):
		X(IX),Y(IY),Width(IWidth),Height(IHeight) {}

	s32 X,Y,Width,Height;
};

/////////////////////////////////////////////////////////////////////////////////////
class MaxEditDialog : public DialogRes
{
public:
	MaxEditDialog();

	void Reset();
	void AddTitles(const WidgetRect& r_Dim);
	void AddStatic(const atString& r_Label,const atString& r_Static,const WidgetRect& r_Dim);
	void AddFloatSpin(const atString& r_Label,f32& r_Value,f32 Min,f32 Max,f32 Step,const WidgetRect& r_Dim, bool &r_Locked);
	void AddIntSpin(const atString& r_Label,s32& r_Value,s32 Min,s32 Max,s32 Step,const WidgetRect& r_Dim);
	void AddStringEdit(const atString& r_Label,atString& r_Value,const WidgetRect& r_Dim, bool &r_Locked);
	void AddCheckBox(const atString& r_Label,bool& r_Value,const WidgetRect& r_Dim);

	void AddVector2Select(const atString& r_Name,float *r_Value,float Min, float Max, float Step,const WidgetRect& r_Dim, bool &r_Locked);
	void AddVector3Select(const atString& r_Name,float *r_Value,float Min,float Max,float Step,const WidgetRect& r_Dim, bool &r_Locked);
	void AddVector4Select(const atString& r_Name,float *r_Value,float Min,float Max,float Step,const WidgetRect& r_Dim, bool &r_Locked);
	void AddTexButton(const atString& r_Label,atString& r_Value,const WidgetRect& r_Dim, bool &r_Locked);

	void AddOKAndCancel(const WidgetRect& r_Dim);

	bool DoModal(HWND ParentWnd);

	static s32 g_CtrlBorder;
	static s32 g_StaticHeight;
	static s32 g_CtrlGapX;
	static s32 g_CtrlGapY;
	static s32 g_ButtonHeight;
	static s32 g_ButtonWidth;
	static s32 g_SpinnerSize;
	static s32 g_SpinButtSize;

protected:
	void SaveValues();
	void Finish(bool Save);

	void OnInitDialog(HWND Wnd);
	void OnCommand(s32 ID, HWND DlgWnd);

	static INT_PTR CALLBACK MaxEditDlgProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam);

	HWND m_Wnd;
	s32 m_ID;
	s32 m_BtnOk;
	s32 m_BtnCancel;
	
	struct FloatSpinWdgt
	{
		f32* p_Value;
		f32 Min;
		f32 Max;
		f32 Step;

		u32 SpinnerID;
		u32 EditID;
		ISpinnerControl* p_SpinCtrl;
		ICustEdit* p_EditCtrl;
	};

	struct IntSpinWdgt
	{
		s32* p_Value;
		s32 Min;
		s32 Max;
		s32 Step;

		u32 SpinnerID;
		u32 EditID;
		ISpinnerControl* p_SpinCtrl;
		ICustEdit* p_EditCtrl;
	};

	enum eStringEditType
	{
		STRINGEDIT_EDIT,
		STRINGEDIT_BUTTON
	};

	struct StringWdgt
	{
		atString* p_String;
		s32 EditID;
		ICustEdit* p_EditCtrl;
		ICustButton* p_ButtonCtrl;
		eStringEditType controlType;
	};

	struct BoolWdgt
	{
		bool* p_Bool;
		s32 ButtonID;
	};

	atArray<StringWdgt> m_Edits;
	atArray<FloatSpinWdgt> m_FloatSpins;
	atArray<IntSpinWdgt> m_IntSpins;
	atArray<BoolWdgt> m_Bools;
};

} //namespace rage {

#endif //__RSTMAX_MAXEDITDLG_H__