// 
// rageRst/shaderplugin.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "rstplugin.h"
#include "rstmax.h"

#include "file/asset.h"
#include "atl/string.h"

using namespace rage;

rageRstMaxShaderPlugin *rageRstMaxShaderPlugin::sm_This = 0;

rageRstMaxShaderPlugin::rageRstMaxShaderPlugin()
{
	Assert(sm_This == 0);
	sm_This = this;

	WNDCLASS c;
	c.lpszClassName = "RAGE Hidden Window Class";
	c.lpfnWndProc = DefWindowProc;
	c.hInstance = 0;//GetCurrentProcess();
	c.hCursor = 0;
	c.hIcon = 0;
	c.hbrBackground = 0;
	c.lpszMenuName = 0;
	c.style = 0;
	c.cbClsExtra = 0;
	c.cbWndExtra = 0;

	atString shaderPath = RstMax::GetShaderPath();

	grcEffect::SetDefaultPath(shaderPath);

	m_ShaderList = new rageRstShaderList;
	m_ShaderDb = new rageRstShaderDb;
}

rageRstMaxShaderPlugin::~rageRstMaxShaderPlugin() 
{
	sm_This = 0;
	
	delete m_ShaderDb;
	delete m_ShaderList;
}
