#include "rstMaxMtl/utility.h"
#include "rexBase/mgc.h"
#include "vector/geometry.h"
#include "mesh/mesh.h"
#include "file/device.h"
#include "file/token.h"
#include "file/stream.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "grmodel/model.h"
#include "grmodel/modelinfo.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "materialpresets/MaterialPresetManager.h"
#include "devil/il.h"
#include "devil/ilu.h"
#include "rexMaxUtility/utility.h"
#include "rstMax.h"

#include <sstream>
#include <fstream>
#include <strclass.h>

// LibXml2 headers
#include "libxml/parser.h"
#include "libxml/xpath.h"

using namespace rage;

enum
{
	DATA_SIZE = 4096 * 100
};

char Data[DATA_SIZE];
static bool NormalMapFlip(false);

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void rstMaxUtility::SetNormalMapFlip(bool onOff)
{
	NormalMapFlip = onOff;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rstMaxUtility::GetNormalMapFlip()
{
	return NormalMapFlip;
}


void rstMaxUtility::LoadGtaConfig(atArray<atArray<atString>>& GtaAttList)
{
	std::string cfgFilename = GetCOREInterface()->GetDir(APP_PLUGCFG_DIR) + std::string("\\RstMtlMaxConfig.xml");

	xmlDocPtr m_pXmlDoc = xmlParseFile( cfgFilename.c_str() );
	xmlXPathContextPtr m_pXPathCtx = xmlXPathNewContext( m_pXmlDoc );

	if ( !m_pXmlDoc )
	{
		xmlFreeDoc( m_pXmlDoc );
		return;
	}
	if ( !m_pXPathCtx )
	{
		xmlXPathFreeContext( m_pXPathCtx );
		return;
	}

	xmlXPathObjectPtr pXPathObj = xmlXPathEvalExpression( (xmlChar*) "//gtaconfig/rulelist/rule", m_pXPathCtx );
	
	#define XML_BUFFER_SIZE 512
	TCHAR xmlBuffer[XML_BUFFER_SIZE] = {0};
	if ( pXPathObj )
	{
		switch ( pXPathObj->type )
		{
		case XPATH_NODESET:
			{
				if ( !pXPathObj->nodesetval )
					return; // "User error: XPath expression invalid."

				size_t count = pXPathObj->nodesetval->nodeNr;
				for ( size_t n = 0; n < count; ++n )
				{
					xmlNodePtr pNode = pXPathObj->nodesetval->nodeTab[n];
					const char* typeAttribute = (const char*) xmlGetProp(pNode, (xmlChar*) "Type");
					const char* valueAttribute = (const char*) xmlGetProp(pNode, (xmlChar*) "Value");
					
					if ( typeAttribute == NULL || valueAttribute == NULL )
						continue;
						
					const char* nullChar = (const char*) "\0";

					atArray<atString> GtaRule;
					GtaRule.PushAndGrow(atString(typeAttribute));
					GtaRule.PushAndGrow(atString(valueAttribute));
					GtaRule.PushAndGrow(atString(nullChar));

					GtaAttList.PushAndGrow(GtaRule);
				}

			}
			break;

		default:
			break; // "Internal error: XPath query return type not implemented." 
		}
	}
	xmlXPathFreeObject( pXPathObj );
	xmlXPathFreeContext( m_pXPathCtx );
	xmlFreeDoc( m_pXmlDoc );
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
grmShaderGroup* rstMaxUtility::CreateMat(const atArray<rexObjectGenericMesh::MaterialInfo>& MatList)
{
	s32 i,j,k,Count = MatList.GetCount();

	grmShaderGroup*	p_ShaderGroup = grmShaderFactory::GetInstance().CreateGroup();
	p_ShaderGroup->InitCount(Count);

	for(i=0;i<Count;i++)
	{
		const rexObjectGenericMesh::MaterialInfo& r_Mat = MatList[i];
		char tempname[1024];
		char terminators = '\0';
		memset(Data,0,DATA_SIZE);

		fiDevice::MakeMemoryFileName(tempname,sizeof(tempname),Data,DATA_SIZE,false,"gshader");
		fiStream* p_Stream = fiStream::Create(tempname);
		fiTokenizer Tokens("var",p_Stream);
			
		Tokens.SetTerminators(&terminators);

		char Buffer[1024];
		s32 MaterialCount = r_Mat.m_AttributeNames.GetCount();

		for(j=0;j<MaterialCount;j++)
		{
			const char* p_Name = r_Mat.m_AttributeNames[j];
			const char* p_Value = r_Mat.m_AttributeValues[j];
			const char* p_Type = r_Mat.m_RageTypes[j];

			if(!p_Name || !p_Value || !p_Type || *p_Name == '\0' || *p_Value == '\0' || *p_Type == '\0')
				continue;

			sprintf_s(Buffer,1024,"%s ",p_Name);
			Tokens.Put(Buffer);
			Tokens.StartBlock();

			if(	(stricmp("grcTexture",p_Type) == 0) &&
				(r_Mat.m_InputTextureFileNames.GetCount() > 0))
			{
				atString OutputFile;
				OutputFile += p_Value;
				OutputFile += ".dds";

				bool IsNormalMap = false;

				if(stricmp(p_Name,"bumptex") == 0)
					IsNormalMap = true;

				for(k=0;k<r_Mat.m_OutputTextureFileNames.GetCount();k++)
				{
					if(stricmp(r_Mat.m_OutputTextureFileNames[k],OutputFile) == 0)
						break;
				}

				if(k >= r_Mat.m_InputTextureFileNames.GetCount())
					continue;

				sprintf_s(Buffer,1024,"grcTexture %s",p_Value);

				rexMaxUtility::BeginDevil();

				ilEnable(IL_ORIGIN_SET);
				ilOriginFunc(IL_ORIGIN_UPPER_LEFT);

				if(rexMaxUtility::LoadDevilImage(r_Mat.m_InputTextureFileNames[k]) == rexResult(rexResult::PERFECT))
				{
					if(ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE))
					{
						ILubyte* diffuseData = ilGetData();
						ILuint diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
						ILuint diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
						ILint diffuseFormat = ilGetInteger(IL_IMAGE_FORMAT);
						ILint originMode = ilGetInteger(IL_ORIGIN_MODE);

						grcImage* p_Image = grcImage::Create(diffuseWidth,diffuseHeight,1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);

						s32 x,y;

						for(x=0;x<(s32)diffuseWidth;x++)
							for(y=0;y<(s32)diffuseHeight;y++)	
							{
								ILuint pixelVal = 0;
								
								if(rstMaxUtility::GetNormalMapFlip() && IsNormalMap)
								{
									pixelVal += 0;													//b
									pixelVal += diffuseData[((y*diffuseWidth) + x) * 4 + 1] << 8;	//g
									pixelVal += 0;													//r
									pixelVal += diffuseData[((y*diffuseWidth) + x) * 4 + 0] << 24;	//a
								}
								else
								{
									pixelVal += diffuseData[((y*diffuseWidth) + x) * 4 + 2] << 0;
									pixelVal += diffuseData[((y*diffuseWidth) + x) * 4 + 1] << 8;
									pixelVal += diffuseData[((y*diffuseWidth) + x) * 4 + 0] << 16;
									pixelVal += diffuseData[((y*diffuseWidth) + x) * 4 + 3] << 24;
								}

								p_Image->SetPixel(x,y,pixelVal);
							}

						char NameBuffer[1024];
						StringNormalize(NameBuffer,p_Value,1024);

						grcTexture* p_Tex = grcTextureFactory::GetInstance().Create(p_Image);
						grcTextureFactory::GetInstance().DeleteTextureReference(NameBuffer);
						grcTextureFactory::GetInstance().RegisterTextureReference(NameBuffer,p_Tex);
						p_Image->Release();
					}
				}
				
				rexMaxUtility::EndDevil();

				char* p_Index = Buffer;

				while(*p_Index)
				{
					if(*p_Index == '\\')
						*p_Index = '/';

					p_Index++;
				}
			}
			else
			{
				sprintf_s(Buffer, 1024, "%s %s",p_Type,p_Value);
			}

			Tokens.Put(Buffer);
			Tokens.EndLine();
			Tokens.EndBlock();
		}

		p_Stream->Flush();
		s32 Size = p_Stream->Tell();
		Tokens.Reset();

		p_Stream->Close();

		fiDevice::MakeMemoryFileName(tempname,sizeof(tempname),Data,Size,false,"gshader");
		p_Stream = fiStream::Create(tempname);
		Tokens.Init("var",p_Stream);

		char shaderNameBuf[RAGE_MAX_PATH];
		const char *shaderName = MatList[i].m_TypeName;
		if (strstr(shaderName,".sps")) {
			shaderNameBuf[0] = '\0';
			strcat(shaderNameBuf,RstMax::GetPresetPath());
			strcat(shaderNameBuf,MatList[i].m_TypeName);

			fiStream *S = fiStream::Open(shaderNameBuf);
			if (!S)
				return NULL;

			fgetline(shaderNameBuf,sizeof(shaderNameBuf),S);
			S->Close();
			shaderName = strtok(shaderNameBuf," \t");
			shaderName = strtok(NULL, " \t");
		}

		grmShader* p_Shader = grmShaderFactory::GetInstance().Create();
		if(p_Shader->Load(MatList[i].m_TypeName,&Tokens))
		{
			p_ShaderGroup->Add(p_Shader);
		}

		p_Stream->Close();
	}

	return p_ShaderGroup;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
grmShaderGroup* rstMaxUtility::CreateRageShaderGroupFromMaxModel(INode* p_Node)
{
	atArray<rexObjectGenericMesh::MaterialInfo> MatList;

	rexMaxUtility::GetMaterialsFromMaxMeshNode(p_Node,MatList);

	return CreateMat(MatList);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
grmModelGeom* rstMaxUtility::CreateRageModelFromMaxModel(INode* p_Node,grmShaderGroup* p_ShaderGroup)
{
	rexObjectGenericMesh GenMesh;
	rexResult Res;

	Class_ID classID = p_Node->EvalWorldState(0).obj->ClassID();

	if(	(p_Node->EvalWorldState(0).obj->ClassID() != Class_ID(EDITTRIOBJ_CLASS_ID,0)) && 
		(p_Node->EvalWorldState(0).obj->ClassID() != Class_ID(POLYOBJ_CLASS_ID,0)) && 
		(p_Node->EvalWorldState(0).obj->ClassID() != EPOLYOBJ_CLASS_ID))
		return NULL;

	if(!rexMaxUtility::ConvertMaxMeshNodeIntoGenericMesh(p_Node,&GenMesh,Res))
		return NULL;
	
	mshMesh* Mesh = new mshMesh;
	ConvertGenericMeshToRageRenderMesh(&GenMesh,*Mesh);
//	Mesh->CleanModel();
//	Mesh->Tristrip();

	grmModelGeom* p_New = new grmModelGeom();
	grmModelInfo Info;
	p_New->Load(*Mesh,&Info,p_ShaderGroup);
	delete Mesh;

	return p_New; 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
atString rstMaxUtility::GetTexMapName(Texmap* p_Texmap)
{
	if(!p_Texmap)
	{
		return atString("none");
	}

	atString Ret(p_Texmap->GetName());

	if(p_Texmap->ClassID() == Class_ID(BMTEX_CLASS_ID,0))
	{
		Ret += " (";

		BitmapTex* p_BitmapTex = (BitmapTex*)p_Texmap;
		TSTR MapName = _T(p_BitmapTex->GetMapName());

		int CharIdx = MapName.last('\\');

		if(CharIdx != -1)
		{
			CharIdx++;
			int NumChars = MapName.length() - CharIdx;

			Ret += MapName.Substr(CharIdx,NumChars);
		}
		else
		{
			Ret += MapName;
		}

		Ret += ")";
	}

	return Ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rstMaxUtility::ConvertGenericMeshToRageRenderMesh( rexObjectGenericMesh *inputMesh,
															 mshMesh& outputMesh)
{
	enum  PolyMode
	{
		POLYGON,
		TRISTRIP,
		TRIANGLE,
	};

	PolyMode m_PolyMode = TRISTRIP;
	rexResult result(rexResult::PERFECT);

	float conversionFactor=1.0f;

	Matrix34 centeredTransformMatrix( M34_IDENTITY );
	Matrix34 pivotTransformMatrix( M34_IDENTITY );
	inputMesh->m_Matrix.UnTransform( inputMesh->m_Center, centeredTransformMatrix.d );
	inputMesh->m_Matrix.UnTransform( inputMesh->m_PivotPoint, pivotTransformMatrix.d );

	if( inputMesh->m_IsSkinned )
	{
		outputMesh.MakeSkinned();
		int offsetCount = inputMesh->m_SkinBoneMatrices.GetCount();
		for( int a = 0; a < offsetCount; a++ )
		{
			Vector3 offset=inputMesh->m_SkinBoneMatrices[ a ].d;
			offset.Scale(1.0f);
			outputMesh.AddOffset(offset);	
		}
	}

// If zero, no extrema are computed (kinda silly)
// If one, bound sphere and AABB are computed (negligible performance difference)
// If two, bound sphere and OOBB are computed (house.mb w/out tristripping went from 10.2 to 14.2 seconds serialization time)
#define COMPUTE_EXTREMA		1

	Vector3 mini(1E10,1E10,1E10), maxi(-1E10,-1E10,-1E10);
#if COMPUTE_EXTREMA
	Mgc::Vector3 *boundPoints = NULL;
	int boundPointCount = 0, boundPointAllocated = 0;
#endif

	int mtlCount = inputMesh->m_Materials.GetCount();
	for( int a = 0; a < mtlCount; a++ )
	{	
		const rexObjectGenericMesh::MaterialInfo& matInfo = inputMesh->m_Materials[ a ];

		char buffer[256]="#0";
		sprintf_s( buffer, 256, "#%d", a );

		int srcPrimCount = matInfo.m_PrimitiveIndices.GetCount();
		int srcPrimOffset = 0;
		const int maxBones = 16384;
		atRangeArray<float,maxBones> weightArray;
		//float* weightArray = new float[maxBones];

		// Introduce extra materials as necessary to avoid crossing matrix limits.
		do {
			mshMaterial &mtl = outputMesh.NewMtl();
			mtl.Name = buffer;

			for (int i=0; i<maxBones; i++)
				weightArray[i] = 0;

			int totalAdj = 0;
			int matrices = 0;
			int lastGoodPrimitive = -1;
			for( int b = srcPrimOffset; b < srcPrimCount; b++ ) {
				const rexObjectGenericMesh::PrimitiveInfo& primInfo = inputMesh->m_Primitives[ matInfo.m_PrimitiveIndices[ b ] ];
				int adjCount = primInfo.m_Adjuncts.GetCount();
				for( int c = 0; c < adjCount; c++ )
				{
					const rexObjectGenericMesh::AdjunctInfo& adjInfo = primInfo.m_Adjuncts[ c ];
					const rexObjectGenericMesh::VertexInfo& vertInfo = inputMesh->m_Vertices[ adjInfo.m_VertexIndex ];
					int boneCount = vertInfo.m_BoneWeights.GetCount();
					for (int w = 0; w < boneCount; w++) {
						if (vertInfo.m_BoneWeights[w] && !weightArray[vertInfo.m_BoneIndices[w]]) {
							weightArray[vertInfo.m_BoneIndices[w]] = vertInfo.m_BoneWeights[w];
							++matrices;
						}
					}
				}
				if (matrices > mshMaxMatricesPerMaterial)
				{
					Displayf("had to insert material break because of matrix limit");
					break;
				}
				totalAdj += adjCount;
				lastGoodPrimitive = b;
			}
			AssertMsg(lastGoodPrimitive != -1,"too many matrices in a single primitive!?!?!?");

			mtl.Prim.Reallocate( lastGoodPrimitive + 1 - srcPrimOffset );
			mtl.Verts.Reallocate( totalAdj );
			Vector2 emptyTexCoord(0,0);

#if COMPUTE_EXTREMA
			// Resize bounding region array so it's always big enough.
			if (boundPointAllocated < boundPointCount + totalAdj) {
				while (boundPointAllocated < boundPointCount + totalAdj) {
					if (boundPointAllocated)
						boundPointAllocated *= 2;
					else
						boundPointAllocated = 1024;
				}
				Mgc::Vector3 *newPoints = new Mgc::Vector3[boundPointAllocated];
				if (boundPointCount) {
					memcpy(newPoints,boundPoints,boundPointCount * sizeof(Mgc::Vector3));
					delete[] boundPoints;
				}
				boundPoints = newPoints;
			}
#endif
		
			for( int b = srcPrimOffset; b <= lastGoodPrimitive; b++ )
			{
				const rexObjectGenericMesh::PrimitiveInfo& primInfo = inputMesh->m_Primitives[ matInfo.m_PrimitiveIndices[ b ] ];
				int adjCount = primInfo.m_Adjuncts.GetCount();

				mshPrimitive& prim = mtl.Prim.Append();

				prim.Type = mshPOLYGON;
				prim.Priority = mtl.Priority;
				prim.Idx.Reallocate( adjCount );

				for( int c = 0; c < adjCount; c++ )
				{
					const rexObjectGenericMesh::AdjunctInfo& adjInfo = primInfo.m_Adjuncts[ c ];
					int numUVSets = adjInfo.m_TextureCoordinates.GetCount();
					const rexObjectGenericMesh::VertexInfo& vertInfo = inputMesh->m_Vertices[ adjInfo.m_VertexIndex ];

					Vector3 vertPos( vertInfo.m_Position );
					
//					if( m_ExportRootBoneMeshesRelativeToPivot && !inputMesh->m_BoneIndex )
//						pivotTransformMatrix.UnTransform( vertPos );							
//					else if( m_ExportRootBoneMeshesRelativeToMeshCenter && !inputMesh->m_BoneIndex )
//						centeredTransformMatrix.UnTransform( vertPos );
					//TL - Added check for skinned mesh before transforming verts.  For non-skinned, animated "level" objects this 
					//transformation was un-doing the transformation that occured in "rexMayaUtility::ConvertMayaMeshNodeIntoGenericMesh"
					//which essentially caused a double transform to be applied to those objects when rendered (due to the virtual bone 
					//creation?). 
					if( (inputMesh->m_IsSkinned) || (inputMesh->m_BoneIndex==0) )
					{
						inputMesh->m_Matrix.Transform( vertPos );
						inputMesh->m_ParentBoneMatrix.UnTransform( vertPos );
					}

					// const bool unique = false;
					// const float tol = 0.0001f;

					int boneCount = vertInfo.m_BoneWeights.GetCount();
					Assertf(boneCount <= maxBones, "%d bones is more than the upper limit of %d bones", boneCount, maxBones);
									
					for( int d = 0; d < boneCount; d++ )
					{
						weightArray[ d ] = 0.0f;
					}
					for( int d = 0; d < boneCount; d++ )
					{
						if (vertInfo.m_BoneIndices[ d ]!=-1)
							weightArray[vertInfo.m_BoneIndices[ d ]] = vertInfo.m_BoneWeights[ d ];
					}

					Vector3 pos, nrm = inputMesh->m_Normals[ adjInfo.m_NormalIndex ];
					nrm.Normalize();

					if (inputMesh->m_BoneIndex)
					{
						inputMesh->m_Matrix.Transform3x3( nrm );
						inputMesh->m_ParentBoneMatrix.UnTransform3x3( nrm );
						nrm.Normalize();
					}

					mshBinding binding;
					binding.Reset();
					if( inputMesh->m_IsSkinned )
						binding.Init( weightArray, boneCount, mshMaxMatricesPerVertex );
					int uvCount = numUVSets<mshMaxTexCoordSets? numUVSets : mshMaxTexCoordSets;
					vertPos.Scale(conversionFactor);
#if COMPUTE_EXTREMA
					Assert(boundPointCount < boundPointAllocated);
					Mgc::Vector3 &nbv = boundPoints[boundPointCount++];
					nbv.x = vertPos.x;
					nbv.y = vertPos.y;
					nbv.z = vertPos.z;
#endif
					vertPos.Min(mini,vertPos);
					vertPos.Max(maxi,vertPos);

					static Vector4 DefaultColor(1,1,1,1);

					int vertIdx = mtl.AddVertex(
						vertPos,
						binding,
						nrm,
						adjInfo.m_Colors[0],
						DefaultColor,
						DefaultColor,
						uvCount,
						uvCount? &adjInfo.m_TextureCoordinates[0] : &emptyTexCoord,
						0,
						NULL);
		
					prim.Idx.Append() = vertIdx;
				}
			}//End for( int b = srcPrimOffset; b <= lastGoodPrimitive; b++ )

			//Now that all of the vertices are added to the material go through and setup the mesh channels for any 
			//blind data attached to the generic mesh
			int blindChannelCount = inputMesh->m_BlindDataIds.GetCount();
			if(blindChannelCount)
			{
				atArray<mshChannel*> mshBlindChannels;

				//Add the channels
				for(int i=0; i<blindChannelCount; i++)
				{
					mtl.AddChannel(inputMesh->m_BlindDataIds[i], 1);
					mshBlindChannels.PushAndGrow((mshChannel*)(&(mtl.GetChannel(i))));
				}
			
				int blindVertIdx = 0;
				for( int b = srcPrimOffset; b <= lastGoodPrimitive; b++ )
				{
					const rexObjectGenericMesh::PrimitiveInfo& primInfo = inputMesh->m_Primitives[ matInfo.m_PrimitiveIndices[ b ] ];
					int adjCount = primInfo.m_Adjuncts.GetCount();

					for( int c = 0; c < adjCount; c++ )
					{
						const rexObjectGenericMesh::AdjunctInfo& adjInfo = primInfo.m_Adjuncts[ c ];
						const rexObjectGenericMesh::VertexInfo& vertInfo = inputMesh->m_Vertices[ adjInfo.m_VertexIndex ];

						for(int i=0; i<blindChannelCount; i++)
						{
							mshChannelData& chnData = mshBlindChannels[i]->Access(blindVertIdx,0);
							if(inputMesh->m_BlindDataTypes[i] == rexValue::valueTypes::FLOAT)
							{
								//Set the value as a float
								chnData.f = vertInfo.m_BlindData[i].toFloat();
							}
							else
							{
								//Set the value as a signed integer
								chnData.s = vertInfo.m_BlindData[i].toInt();
							}
						}

						blindVertIdx++;
					}
				}//End for( int b = srcPrimOffset; ...

				Assertf( (blindVertIdx == mtl.Verts.GetCount()), "Vertex count mismatch during mesh blind data serialization, %d blind verts doesn't match up with %d mesh verts",
					blindVertIdx, mtl.Verts.GetCount());
	
			}//End if(blindChannelCount)

			srcPrimOffset = lastGoodPrimitive + 1;
		} while (srcPrimOffset < srcPrimCount);
	}

#if COMPUTE_EXTREMA

	if (boundPointCount) {
		Mgc::Sphere sphere = Mgc::MinSphere(boundPointCount, boundPoints);
		outputMesh.SetBoundSphere(Vector4((float)sphere.Center().x, (float)sphere.Center().y, (float)sphere.Center().z,(float)sphere.Radius()));

		Matrix34 bbMatrix;
	#if COMPUTE_EXTREMA == 2
		Mgc::Box3 box = Mgc::MinBox(boundPointCount, boundPoints);
		bbMatrix.a.x = (float)box.Axis(0).x;
		bbMatrix.a.y = (float)box.Axis(0).y;
		bbMatrix.a.z = (float)box.Axis(0).z;
		bbMatrix.b.x = (float)box.Axis(1).x;
		bbMatrix.b.y = (float)box.Axis(1).y;
		bbMatrix.b.z = (float)box.Axis(1).z;
		bbMatrix.c.x = (float)box.Axis(2).x;
		bbMatrix.c.y = (float)box.Axis(2).y;
		bbMatrix.c.z = (float)box.Axis(2).z;
		bbMatrix.d.x = (float)box.Center().x;
		bbMatrix.d.y = (float)box.Center().y;
		bbMatrix.d.z = (float)box.Center().z;
		outputMesh.SetBoundBox(bbMatrix,Vector3((float)box.Extent(0),(float)box.Extent(1),(float)box.Extent(2)));
	#else	// COMPUTE_EXTREMA == 2
		bbMatrix.Identity();
		bbMatrix.d.Lerp(0.5f,mini,maxi);
		Vector3 size;
		size.Subtract(maxi,mini);
		size.Scale(0.5f);
		outputMesh.SetBoundBox(bbMatrix,size);
	#endif	// COMPUTE_EXTREMA == 2

		delete [] boundPoints;
	}
#endif	// COMPUTE_EXTREMA

#if HACK_GTA4
	outputMesh.Weld();
#endif // HACK_GTA4
	outputMesh.Triangulate();
	outputMesh.ComputeTanBi(0);
#if HACK_GTA4
	outputMesh.Tristrip();
#endif // HACK_GTA4

	// Do material merge after tristripping because smaller materials should tristrip faster.
	// The assumption here of course is that the duplicate materials were not good candidates
	// for tristripping anyway, so we should check that at some point!
	// We also cannot do this yet for skinned creatures because we would undo the material
	// breaks we just inserted earlier!
	if (!inputMesh->m_IsSkinned)	
		outputMesh.MergeSameMaterials();

	//Finally copy over the blind data ids
	/*
	for(int i=0; i<inputMesh->m_FloatBlindDataIds.GetCount(); i++)
		outputMesh.SetFloatBlindID(i, (mshVertexFloatBlindID)inputMesh->m_FloatBlindDataIds[i]);
	*/

	return result;
}

void rstMaxUtility::LogOut(const atString& r_Path)
{
	fiStream* p_Stream = fiStream::Open("c:/log.txt",false);
	p_Stream->Seek(p_Stream->Size());

	if(!p_Stream)
	{
		p_Stream = fiStream::Create("c:/log.txt");
	}

	if(p_Stream)
	{
		p_Stream->Write(r_Path,(int)strlen(r_Path));
		p_Stream->Close();
	}
}

void Helper_SkipTokenBlock(fiTokenizer &T)
{
	while(!T.CheckToken("}"))
		T.IgnoreToken();
}

void rstMaxUtility::GetSPSVars(const char *presetFileName, atArray<atString> &varNames)
{
	fiStream *S2 = ASSET.Open(presetFileName,"sps");
	if (!S2) 
		return;

	grcEffect *e = NULL;
	fiTokenizer T;
	char buf[RAGE_MAX_PATH];
	T.Init(buf,S2);
	T.MatchToken("shader");
	T.GetToken(buf,sizeof(buf));
	if (T.CheckToken("comment")) 
	{
		char buf[128];
		T.GetLine(buf,sizeof(buf));
	}

	// Note that to avoid getting the input stream way of of sync, we need to deal with e being NULL.
	e = grcEffect::LookupEffect(atStringHash(buf));

	// u32 *NameHash = (u32*)((char*)Data + (SpuSize));
	if (!e)
		grcErrorf("rstMaxUtility::GetSPSVars() cannot find parent effect '%s'",buf);

	if (T.CheckToken("__rage_drawbucket")) // Might not be present in old files.
	{
		Helper_SkipTokenBlock(T);
	}
	if (T.CheckToken("__rage_drawbucketmask")) // Might not be present in old files.
	{
		Helper_SkipTokenBlock(T);
	}
	if (T.CheckToken("__rage_physmtl")) // Might not be present in old files.
	{
		Helper_SkipTokenBlock(T);
	}

	// Stop parsing on closing brace or EOF.  Allows this to also handle inline sva data.
	while (!T.CheckToken("}",false) && T.GetToken(buf,sizeof(buf)))
	{
		varNames.PushAndGrow(atString(buf));
		Helper_SkipTokenBlock(T);
	}

	S2->Close();
}


bool rstMaxUtility::IsPreset(const char* fileName)
{
	return MaterialPresetManager::IsMaterialSurface(fileName) 
		|| MaterialPresetManager::IsMaterialTemplate(fileName)
		|| MaterialPresetManager::IsMaterialObject(fileName); 
}

bool rstMaxUtility::IsPresetExtension(const char* ext)
{
	return (stricmp(ext, MaterialPresetManager::m_TemplateFileExtension) == 0 
		|| stricmp(ext, MaterialPresetManager::m_SurfaceFileExtension) == 0
		|| stricmp(ext, MaterialPresetManager::m_ObjectFileExtension) == 0);
}

atString rstMaxUtility::ToLowerCase( const char * path )
{
	char buffer[4096];
	strcpy( buffer, path );
	strlwr( buffer );
	return atString( buffer );
}