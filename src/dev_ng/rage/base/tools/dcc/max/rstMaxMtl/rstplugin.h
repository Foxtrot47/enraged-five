// 
// rstMaxMtl/rstplugin.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __RST_MAX_SHADERPLUGIN_H
#define __RST_MAX_SHADERPLUGIN_H

#include "rst/shaderdb.h"
#include "rst/shaderlist.h"

namespace rage {

class rageRstMaxShaderPlugin
{
public:
	rageRstMaxShaderPlugin();
	~rageRstMaxShaderPlugin();

	// PURPOSE: Set the root path of the shader database
	bool SetShaderDbPath(const char *path)	{ return m_ShaderDb->SetShaderDbPath(path); }

	rageRstShaderList *GetShaderList()			{ return m_ShaderList; }
	rageRstShaderDb *GetShaderDb()				{ return m_ShaderDb; }
	
	static rageRstMaxShaderPlugin *GetInstance()	{ return sm_This; }

protected:
	rageRstShaderList		*m_ShaderList;
	rageRstShaderDb			*m_ShaderDb;

	static rageRstMaxShaderPlugin *sm_This;
};

}	// namespace rage


#endif // __RST_MAX_SHADERPLUGIN_H
