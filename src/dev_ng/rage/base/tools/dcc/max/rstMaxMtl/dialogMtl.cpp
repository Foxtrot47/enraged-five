
#include "MaterialPresets/MaterialPresetManager.h"

#include "rstMaxMtl/dialogMtl.h"
#include "rstMaxMtl/dialogMtlPreset.h"
#include "rstMaxMtl/resource.h"
#include "rstMaxMtl/utility.h"
#include "grmodel/shader.h"
#include "rstMaxMtl/rstMax.h"
#include "rstMaxMtl/dbeditor.h"
#include "rst/rageloop.h"
#include "rst/shaderlist.h"


#include "rstplugin.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RstMaxMtlDlg::RstMaxMtlDlg(RstMtlMax* p_Mtl,IMtlParams* p_MtlParams):
	mp_Mtl(p_Mtl),
	m_Wnd(NULL)
{
	m_Wnd = /*mp_Mtl->mp_MtlParam*/p_MtlParams->AddRollupPage(g_hDllInstance,
										MAKEINTRESOURCE(IDD_DLG_MATEDIT),
										RstMaxParamDlgProc,
										_T("Rage Shader"),
										(LPARAM)this);

	SetupUI();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RstMaxMtlDlg::~RstMaxMtlDlg()
{
	if(mp_Mtl)
	{
		mp_Mtl->mp_MtlDlg = NULL;
		if(mp_Mtl->mp_DlgExtra)
		{
			delete mp_Mtl->mp_DlgExtra;
			mp_Mtl->mp_DlgExtra = NULL;
		}
		if (mp_Mtl->mp_DlgMtlPreset)
		{
			//mp_Mtl->mp_DlgMtlPreset->~RstMaxMtlPresetDlg();
			delete mp_Mtl->mp_DlgMtlPreset;
			mp_Mtl->mp_DlgMtlPreset = NULL;
		}
	}
	//delete mp_ShaderPlugin;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::SetThing(ReferenceTarget *m)
{
	if(mp_Mtl)
		mp_Mtl->mp_MtlDlg = NULL;

	mp_Mtl = (RstMtlMax*)m; 

	if(mp_Mtl)
		mp_Mtl->mp_MtlDlg = this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RstMaxMtlDlg::FindSubTexFromHWND(HWND hwnd)
{
	return NULL;//mp_Mtl->mp_DlgExtra->FindSubTexFromHWND(hwnd); // Ever used?!
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::Reset()
{
	SendMessage(GetDlgItem(m_Wnd,IDC_CBO_SHADERNAME),CB_SETCURSEL,-1,0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::SetupUI()
{
	if(mp_Mtl->IsTwoSided())
		CheckDlgButton(m_Wnd,IDC_CHK_TWOSIDED,BST_CHECKED);
	else
		CheckDlgButton(m_Wnd,IDC_CHK_TWOSIDED,BST_UNCHECKED);

	if(mp_Mtl->IsPreset())
		CheckDlgButton(m_Wnd,IDC_RDO_PRESET,BST_CHECKED);
	else
		CheckDlgButton(m_Wnd,IDC_RDO_CUSTOM,BST_CHECKED);
#if RSTMTL_HW_SHADER
	if(mp_Mtl->GetLightingMode())
		CheckDlgButton(m_Wnd,IDC_CHK_LIGHTING,BST_CHECKED);
	else
		CheckDlgButton(m_Wnd,IDC_CHK_LIGHTING,BST_UNCHECKED);

	if(mp_Mtl->HasAmbLighting())
		CheckDlgButton(m_Wnd,IDC_CHK_AMB_LIGHTING, BST_CHECKED);
	else
		CheckDlgButton(m_Wnd,IDC_CHK_AMB_LIGHTING, BST_UNCHECKED);

	if(mp_Mtl->UsesDiffuseMapAlpha())
		CheckDlgButton(m_Wnd,IDC_CHK_DIFFUSEALPHA, BST_CHECKED);
	else
		CheckDlgButton(m_Wnd,IDC_CHK_DIFFUSEALPHA, BST_UNCHECKED);

	if(mp_Mtl->m_OverrideTexScale)
	{
		CheckDlgButton(m_Wnd,IDC_CHK_TEXSCALE,BST_CHECKED);
		EnableWindow(GetDlgItem(m_Wnd,IDC_CBO_TEX_SCALE),TRUE);
	}
	else
		CheckDlgButton(m_Wnd,IDC_CHK_TEXSCALE,BST_UNCHECKED);
#endif //RSTMTL_HW_SHADER
	if(mp_Mtl->IsPreset())
	{
		HWND btnWnd = GetDlgItem(m_Wnd, IDC_BTN_PRESET);

		if (mp_Mtl->m_ShaderName.length())
		{
			SetPresetName(mp_Mtl->m_ShaderName.c_str());
		}

		SetupShader(mp_Mtl->m_ShaderName,true);

		EnableWindow(GetDlgItem(m_Wnd,IDC_BTN_PRESET),TRUE);
		EnableWindow(GetDlgItem(m_Wnd,IDC_CBO_SHADERNAME),FALSE);
		CheckRadioButton(m_Wnd,IDC_RDO_PRESET,IDC_RDO_CUSTOM,IDC_RDO_PRESET);

#if HACK_GTA4
		EnableWindow(GetDlgItem(m_Wnd,IDC_RDO_CUSTOM),FALSE);
#endif //HACK_GTA4
#if RSTMTL_HW_SHADER
		char cCurrMult[64];
		sprintf_s(cCurrMult, 64, "%f", mp_Mtl->m_TexScaleMultiplier);
		SetWindowText(GetDlgItem(m_Wnd,IDC_CBO_TEX_SCALE), cCurrMult);
#endif //RSTMTL_HW_SHADER
		
	}
	else
	{
		HWND ListWnd = GetDlgItem(m_Wnd,IDC_CBO_SHADERNAME);

		s32 ShaderIdx = SendMessage(ListWnd,CB_FINDSTRING,-1,(LPARAM)((const char*)mp_Mtl->m_ShaderName));

		if(ShaderIdx == CB_ERR)
		{
			SendMessage(ListWnd,CB_SETCURSEL,-1,0);
		}
		else
		{
			SendMessage(ListWnd,CB_SETCURSEL,ShaderIdx,0);
		}

		SetupShader(mp_Mtl->m_ShaderName,false);

		EnableWindow(GetDlgItem(m_Wnd,IDC_BTN_PRESET),FALSE);
		EnableWindow(GetDlgItem(m_Wnd,IDC_CBO_SHADERNAME),TRUE);
		CheckRadioButton(m_Wnd,IDC_RDO_PRESET,IDC_RDO_CUSTOM,IDC_RDO_CUSTOM);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::SetupShader(const atString& presetName,bool IsPreset)
{
	//Cache the previous shader/preset so we can translate variables between them.
	rageRstShaderDbEntry* p_PreviousEntry = RstMax::GetDbEntry(mp_Mtl->m_ShaderName);
	rageRstShaderDbEntry* p_CurrentEntry = RstMax::GetDbEntry(presetName);

	MaterialPreset* p_PreviousMaterialPreset = NULL;
	grcEffect* p_PreviousShader = NULL;

	// We're switching from 1 type to another
	if (p_PreviousEntry && p_CurrentEntry != p_PreviousEntry)
	{
		// Switch type from material preset to rage shader
		if (p_PreviousEntry->IsMaterialPreset() && !p_CurrentEntry->IsMaterialPreset())
		{
			mp_Mtl->RecreateInstanceData(ShaderInstanceData::eInstanceDataType::INSTANCE_RAGESHADER);
		}
		// Switch type from rage shader to material preset.
		else if (!p_PreviousEntry->IsMaterialPreset() && p_CurrentEntry->IsMaterialPreset())
		{
			mp_Mtl->RecreateInstanceData(ShaderInstanceData::eInstanceDataType::INSTANCE_PRESET);
		}
	}
	// We're creating a new entry and it's not the same as a previous one.
	else if (p_CurrentEntry && p_PreviousEntry != p_CurrentEntry)
	{
		if (p_CurrentEntry->IsMaterialPreset())
		{
			mp_Mtl->RecreateInstanceData(ShaderInstanceData::eInstanceDataType::INSTANCE_PRESET);
		}
		else
		{
			mp_Mtl->RecreateInstanceData(ShaderInstanceData::eInstanceDataType::INSTANCE_RAGESHADER);
		}
	}

	if ( p_PreviousEntry )
	{
		if ( p_PreviousEntry->m_MaterialPreset == NULL )
		{
			if ( p_PreviousEntry->m_Preset )
			{
				p_PreviousShader = &p_PreviousEntry->m_Preset->GetBasis();
				const char* p_Name = p_PreviousEntry->m_Preset->GetShaderName();

				char buff[512];
				sprintf_s( buff, 512, "%s/fx_max/%s.fx", (RstMax::GetShaderPath()).c_str(), p_Name );

				mp_Mtl->LoadEffect(buff);
				if(IsPreset)
				{
					p_PreviousShader = &p_PreviousEntry->m_Preset->GetBasis();
				}
			}
			else
			{
				p_PreviousShader = RstMax::FindShader(mp_Mtl->m_ShaderName);
			}
		}
		else
		{
			p_PreviousMaterialPreset = p_PreviousEntry->m_MaterialPreset;
		}		
	}
	
	if((mp_Mtl->m_ShaderName != presetName) ||(mp_Mtl->IsPreset() != IsPreset))
	{
		mp_Mtl->m_ShaderName = presetName;
		mp_Mtl->SetIsPreset(IsPreset);

		mp_Mtl->NotifyDependents(FOREVER,PART_MTL,REFMSG_NODE_MATERIAL_CHANGED);
	}

#if RSTMTL_HW_SHADER
	grcEffect* p_Shader = NULL;
	if(p_CurrentEntry)
	{
		p_Shader = &p_CurrentEntry->m_Preset->GetBasis();
		const char* p_Name = p_CurrentEntry->m_Preset->GetShaderName();

		char buff[512];
		sprintf_s( buff, 512, "%s/fx_max/%s.fx", (RstMax::GetShaderPath()).c_str(), p_Name );

		mp_Mtl->LoadEffect(buff);

		HWND CommentWnd = GetDlgItem(m_Wnd,IDC_EDT_COMMENT);
		SetWindowText(CommentWnd, p_CurrentEntry->m_Preset->GetComment());
	}
#endif // RSTMTL_HW_SHADER

	if(p_CurrentEntry)
	{
		mp_Mtl->m_ShaderInstanceData->m_PresetOverrides.clear();
		if(IsPreset)
		{
			p_Shader = &p_CurrentEntry->m_Preset->GetBasis();

#if ENABLE_PRESET_EDITING
			atString fullFilePath = atString(RstMax::GetPresetPath(), presetName);
			rstMaxUtility::GetSPSVars( fullFilePath.c_str(), mp_Mtl->m_ShaderInstanceData->m_PresetOverrides);
#endif
		}
		else
		{
			p_Shader = RstMax::FindShader(mp_Mtl->m_ShaderName);
		}
	}

	if(!p_Shader)
		return;

	mp_Mtl->m_ShaderInstanceData->MapInstanceData( mp_Mtl, p_CurrentEntry, p_PreviousShader, p_PreviousMaterialPreset);

#if RSTMTL_HW_SHADER
	mp_Mtl->UpdateParameters( true );
#endif // RSTMTL_HW_SHADER

	if (p_CurrentEntry->IsMaterialPreset())
	{
		mp_Mtl->ConstructShaderParamDialog(p_CurrentEntry);
	}
	else
	{
		mp_Mtl->ConstructShaderParamDialog(p_Shader, p_CurrentEntry);
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::OnInitDialog(HWND DlgWnd)
{
	SetWindowLongPtr(DlgWnd,GWLP_USERDATA,(LONG_PTR)this);

	s32 i,ShaderCount = RstMax::GetShaderCount();
	const char* p_ShaderName;

	HWND ListWnd = GetDlgItem(DlgWnd,IDC_CBO_SHADERNAME);
	EnableWindow(ListWnd,TRUE);

	for(i=0;i<ShaderCount;i++)
	{
		p_ShaderName = RstMax::GetShaderName(i);
		SendMessage(ListWnd,CB_ADDSTRING,0,(LPARAM)p_ShaderName);
	}

	ListWnd = GetDlgItem(DlgWnd,IDC_CBO_TEX_SCALE);

	float fMult = 1.0f;
	for(i=0;i<4;i++)
	{
		char cName[64];
		sprintf_s(cName, 64, "%f", fMult);
		SendMessage(ListWnd,CB_ADDSTRING,0,(LPARAM)cName);
		fMult *= 0.5f;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::OnShaderNameSelChange(HWND ListWnd)
{
	s32 SelIdx = SendMessage(ListWnd,CB_GETCURSEL,0,0);

	const char* p_ShaderName = RstMax::GetShaderName(SelIdx);

	SetupShader(atString(p_ShaderName),false);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::OnRadioPreset()
{
	//remove current shader...

	SetupShader(atString(""),true);

	//disable custom combo box

	HWND m_ShaderWnd = GetDlgItem(m_Wnd,IDC_CBO_SHADERNAME);
	EnableWindow(m_ShaderWnd,FALSE);
	SendMessage(m_ShaderWnd,CB_SETCURSEL,(WPARAM)-1,0);

	m_ShaderWnd = GetDlgItem(m_Wnd,IDC_BTN_PRESET);
	EnableWindow(m_ShaderWnd,TRUE);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::OnRadioCustom()
{
	//remove current shader...

	SetupShader(atString(""),false);

	//disable preset button

	HWND m_ShaderWnd = GetDlgItem(m_Wnd,IDC_BTN_PRESET);

	EnableWindow(m_ShaderWnd,FALSE);
	SendMessage(m_ShaderWnd,CB_SETCURSEL,(WPARAM)-1,0);

	m_ShaderWnd = GetDlgItem(m_Wnd,IDC_CBO_SHADERNAME);
	EnableWindow(m_ShaderWnd,TRUE);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::OnButtonAbout()
{
	DialogBoxParam(	g_hDllInstance,
					MAKEINTRESOURCE(IDD_DLG_ABOUT),
					GetCOREInterface()->GetMAXHWnd(),
					RstMaxAboutDlgProc,
					(LPARAM)this);
}

void RstMaxMtlDlg::OnButtonTexScale()
{
#if RSTMTL_HW_SHADER
	UINT Ret = IsDlgButtonChecked(m_Wnd,IDC_CHK_TEXSCALE);
	if(Ret == BST_CHECKED)
	{
		mp_Mtl->m_OverrideTexScale = true;
		EnableWindow(GetDlgItem(m_Wnd,IDC_CBO_TEX_SCALE),TRUE);
	}
	else
	{
		mp_Mtl->m_OverrideTexScale = false;
		EnableWindow(GetDlgItem(m_Wnd,IDC_CBO_TEX_SCALE),FALSE);
	}
#endif //RSTMTL_HW_SHADER
}

void RstMaxMtlDlg::OnButtonLighting()
{
#if RSTMTL_HW_SHADER
	UINT Ret = IsDlgButtonChecked(m_Wnd,IDC_CHK_LIGHTING);

	if(Ret == BST_CHECKED)
	{
		mp_Mtl->SetLightingMode(true);
	}
	else
	{
		mp_Mtl->SetLightingMode(false);
	}
#endif // RSTMTL_HW_SHADER
}

void RstMaxMtlDlg::OnButtonAmbLighting()
{
#if RSTMTL_HW_SHADER
	UINT Ret = IsDlgButtonChecked(m_Wnd,IDC_CHK_AMB_LIGHTING);
	//Point4 ambLightUp;
	//Point4 ambLightDown;

	if(Ret == BST_CHECKED)
	{
		mp_Mtl->SetHasAmbLighting(true);
		//ambLightUp = Point4(144.0f*(0.7f/255.0f), 181.0f*(0.7f/255.0f), 232.0f*(0.7f/255.0f), 1.0f);
		//ambLightDown = Point4(230.0f*(0.4f/255.0f), 194.0f*(0.4f/255.0f), 147.0f*(0.4f/255.0f), 1.0f);
	}
	else
	{
		mp_Mtl->SetHasAmbLighting(false);
		//ambLightUp = Point4(0.0f, 0.0f, 0.0f, 0.0f);
		//ambLightDown = Point4(0.0f, 0.0f, 0.0f, 0.0f);
	}

	// It would be nice if we could pass through different time cycle values that the artists could choose.  For now, we will just settle for a toggle
	//mp_Mtl->m_ElemContainer.SetColorParam(0, mp_Mtl->m_EffectParser, ambLightUp);
	//mp_Mtl->m_ElemContainer.SetColorParam(1, mp_Mtl->m_EffectParser, ambLightDown);
#endif // RSTMTL_HW_SHADER
}

void RstMaxMtlDlg::OnButtonDiffuseAlpha()
{
#if RSTMTL_HW_SHADER
	UINT Ret = IsDlgButtonChecked(m_Wnd,IDC_CHK_DIFFUSEALPHA);
	if(Ret == BST_CHECKED)
	{
		mp_Mtl->SetUseDiffuseMapAlpha(true);
	}
	else
	{
		mp_Mtl->SetUseDiffuseMapAlpha(false);
	}
#endif // RSTMTL_HW_SHADER
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::OnButtonTwoSided()
{
	UINT Ret = IsDlgButtonChecked(m_Wnd,IDC_CHK_TWOSIDED);

	if(Ret == BST_CHECKED)
	{
		mp_Mtl->SetIsTwoSided(true);
	}
	else
	{
		mp_Mtl->SetIsTwoSided(false);
	}
}

void RstMaxMtlDlg::OnTexScaleSelChange(HWND ListWnd)
{
#if RSTMTL_HW_SHADER
	HWND DlgItem = GetDlgItem(m_Wnd,IDC_CBO_TEX_SCALE);
	s32 Index = SendMessage(DlgItem,CB_GETCURSEL,0,0);

	if(Index != CB_ERR)
	{
		char buffer[64];
		SendMessage(DlgItem,CB_GETLBTEXT,Index,(LPARAM)buffer);
		mp_Mtl->m_TexScaleMultiplier = atof(buffer);
	}

	mp_Mtl->UpdateParameters( true );
#endif //RSTMTL_HW_SHADER
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlDlg::SetPresetName(const char *preset)
{
	char presetName[_MAX_FNAME + 1];
	const char *lastSep = strrchr(preset,'/');
	if (lastSep)
	{
		strcpy(presetName,lastSep + 1);
	}
	SetWindowText(GetDlgItem(m_Wnd,IDC_BTN_PRESET),lastSep ? presetName:preset);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK RstMaxMtlDlg::RstMaxParamDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam) 
{
	RstMaxMtlDlg* p_ParamDlg = (RstMaxMtlDlg*)GetWindowLongPtr(DlgWnd,GWLP_USERDATA);

	switch(msg)
	{
	case WM_INITDIALOG:

		((RstMaxMtlDlg*)lParam)->OnInitDialog(DlgWnd);
		return TRUE;

	case WM_COMMAND:

		if(lParam && (LOWORD(wParam) == IDC_CBO_SHADERNAME))
		{
			switch(HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				p_ParamDlg->OnShaderNameSelChange((HWND)lParam);
				break;
			}
		}
		if(lParam && (LOWORD(wParam) == IDC_CBO_TEX_SCALE))
		{
			switch(HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				p_ParamDlg->OnTexScaleSelChange((HWND)lParam);
				break;
			}
		}
		if(lParam && (LOWORD(wParam) == IDC_BUTT_RELOAD_PRESET))
		{
//			char preset[MAX_PATH];
//			GetWindowText(GetDlgItem(m_Wnd,IDC_BTN_PRESET),preset, MAX_PATH);
			rage::atString preset = p_ParamDlg->mp_Mtl->m_ShaderName;
			if(RstMax::GetDbEntry(preset))
			{
#if RSTMTL_HW_SHADER
				char buff[512];
				rageRstMaxShaderPlugin::GetInstance()->GetShaderDb()->ReloadEntry(preset);
				rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(preset);
				if ( p_Entry->IsMaterialPreset() )
				{
					const char* p_Name = p_Entry->m_MaterialPreset->GetShaderName();
					sprintf_s( buff, 512, "%s/fx_max/%s.fx", (RstMax::GetShaderPath()).c_str(), p_Name );
					MaterialPresetManager::GetCurrent().ReloadPreset(preset);
					p_ParamDlg->mp_Mtl->LoadEffect(buff);
				}
				else
				{
					const char* p_Name = p_Entry->m_Preset->GetShaderName();
					sprintf_s( buff, 512, "%s/fx_max/%s.fx", (RstMax::GetShaderPath()).c_str(), p_Name );
					p_ParamDlg->mp_Mtl->LoadEffect(buff);
				}
				
#endif // RSTMTL_HW_SHADER

				p_ParamDlg->SetupShader(preset,true);
				p_ParamDlg->SetPresetName(preset);
			}
		}
		if(lParam && (LOWORD(wParam) == IDC_BTN_PRESET))
		{
			rage::atString preset = p_ParamDlg->mp_Mtl->m_ShaderName;
			DbEditor::GetInst().ChoosePreset(DlgWnd,preset);
			if (RstMax::GetDbEntry(preset))
			{
				p_ParamDlg->SetupShader(preset,true);
				p_ParamDlg->SetPresetName(preset);
			}
		}
		else
		{
			switch(LOWORD(wParam))
			{
			case IDC_RDO_PRESET:
				p_ParamDlg->OnRadioPreset();
				break;
			case IDC_RDO_CUSTOM:
				p_ParamDlg->OnRadioCustom();
				break;
			case IDC_BTN_ABOUT:
				p_ParamDlg->OnButtonAbout();
				break;
			case IDC_CHK_TWOSIDED:
				p_ParamDlg->OnButtonTwoSided();
				break;
			case IDC_CHK_TEXSCALE:
				p_ParamDlg->OnButtonTexScale();
				break;
			case IDC_CHK_LIGHTING:
				p_ParamDlg->OnButtonLighting();
				break;
			case IDC_CHK_AMB_LIGHTING:
				p_ParamDlg->OnButtonAmbLighting();
				break;
			case IDC_CHK_DIFFUSEALPHA:
				p_ParamDlg->OnButtonDiffuseAlpha();
				break;
			}
		}

		return TRUE;
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK RstMaxMtlDlg::RstMaxAboutDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	switch(msg)
	{
	case WM_INITDIALOG:
		{
			char Buffer[1024];

			sprintf_s(Buffer, 1024, "version %0.2f",0.01f * PLUGIN_VERSION);
			SetWindowText(GetDlgItem(DlgWnd,IDC_STC_VERSION),Buffer);
			sprintf_s(Buffer, 1024, "rage release %d",RAGE_RELEASE);
			SetWindowText(GetDlgItem(DlgWnd,IDC_STC_RAGE),Buffer);
			sprintf_s(Buffer, 1024, "built on %s at %s",__DATE__,__TIME__);
			SetWindowText(GetDlgItem(DlgWnd,IDC_STC_BUILDTIME),Buffer);
		}
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			EndDialog(DlgWnd,0);
		}
	}

	return FALSE;
}
