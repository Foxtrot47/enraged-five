#include "rstMaxMtl/dbeditpreset.h"
#include "grmodel/shader.h"
#include "grcore/texture.h"
#include "grcore/texturestring.h"
#include "grcore/texturereference.h"
#include "rst/shaderdb.h"
#include "rst/shaderplugin.h"
#include "rst/rageloop.h"
#include "rst/shaderlist.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

////////////////////////////////////////////////////////////////////////////////
s32 DbEditPreset::g_DialogWidth(200);
s32 DbEditPreset::g_ControlWidth(200);

////////////////////////////////////////////////////////////////////////////////
DbEditPreset::DbEditPreset()
{
}

////////////////////////////////////////////////////////////////////////////////
bool DbEditPreset::DoModal(HWND ParentWnd,rageRstShaderDbEntry* p_Entry)
{
	mp_Entry = p_Entry;

	if(!mp_Entry)
	{
		return false;
	}

	m_Dialog.Reset();
		
	grcInstanceData *pPreset = p_Entry->m_Preset;
	grcEffect* p_Shader = &pPreset->GetBasis();
	const char* p_ShaderName = p_Shader->GetEffectName();
	const char* p_Name = mp_Entry->m_Preset->GetMaterialName();
	// m_Comment = mp_Entry->m_Preset->GetComment();

	//fill in the dialog

	WidgetRect CtrlDim = WidgetRect(MaxEditDialog::g_CtrlBorder,MaxEditDialog::g_CtrlBorder,g_ControlWidth - (MaxEditDialog::g_CtrlBorder << 1),MaxEditDialog::g_StaticHeight);
	m_Dialog.AddStatic(atString("Shader Name:"),atString(p_ShaderName),CtrlDim);
	CtrlDim.Y += MaxEditDialog::g_StaticHeight + MaxEditDialog::g_CtrlGapY;
	// m_Dialog.AddStringEdit(atString("Comment:"),m_Comment,CtrlDim);
	// CtrlDim.Y += MaxEditDialog::g_StaticHeight + MaxEditDialog::g_CtrlGapY;
	m_Dialog.AddTitles(CtrlDim);
	CtrlDim.Y += MaxEditDialog::g_StaticHeight + MaxEditDialog::g_CtrlGapY;

	s32 i,Count = p_Shader->GetInstancedVariableCount();

	float TempFloat = 0.0f;
	s32 TempInt = 0;
	atString TempString;

	s32 NumFloats = 0;
	s32 NumInts = 0;
	s32 NumStrings = 0;
	s32 NumV2s = 0;
	s32 NumV3s = 0;
	s32 NumV4s = 0;

	//we need to presize the variable arrays

	for(i=0;i<Count;i++)
	{
		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);

		switch(VarInfo.m_Type)
		{
		case grcEffect::VT_FLOAT:
			NumFloats++;
			break;
		case grcEffect::VT_TEXTURE:
			NumStrings++;
			break;
		case grcEffect::VT_VECTOR2:
			NumV2s++;
			break;
		case grcEffect::VT_VECTOR3:
			NumV3s++;
			break;
		case grcEffect::VT_VECTOR4: 
			NumV4s++;
			break;
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
//		case grcEffect::VT_COLOR32:
//		case grcEffect::VT_COUNT:
		default:
			break;
		}
	}

	m_Floats.Resize(NumFloats);
	m_Ints.Resize(NumInts);
	m_Strings.Resize(NumStrings);
	m_V2s.Resize(NumV2s);
	m_V3s.Resize(NumV3s);
	m_V4s.Resize(NumV4s);
	m_Lockeds.Resize(Count);
	atArray<bool> changeCompareLockeds;
	atArray<f32> changeCompareFloats;
	atArray<s32> changeCompareInts;
	atArray<atString> changeCompareStrings;
	atArray<Vector2> changeCompareV2s;
	atArray<Vector3> changeCompareV3s;
	atArray<Vector4> changeCompareV4s;
	changeCompareFloats.Resize(NumFloats);
	changeCompareInts.Resize(NumInts);
	changeCompareStrings.Resize(NumStrings);
	changeCompareV2s.Resize(NumV2s);
	changeCompareV3s.Resize(NumV3s);
	changeCompareV4s.Resize(NumV4s);
	changeCompareLockeds.Resize(Count);

	NumFloats = 0;
	NumV2s = 0;
	NumV3s = 0;
	NumV4s = 0;
	NumInts = 0;
	NumStrings = 0;

	//set up the ui

	for(i=0;i<Count;i++)
	{
		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);

		grcEffectVar handle = p_Shader->LookupVar(VarInfo.m_Name);

		bool Set = changeCompareLockeds[i] = m_Lockeds[i] = pPreset->GetPresetFlag(handle);

		atString VarName(VarInfo.m_UiName);
		VarName += ":";

		switch(VarInfo.m_Type)
		{
		case grcEffect::VT_FLOAT:
			p_Shader->GetVar(*p_Entry->m_Preset, handle,m_Floats[NumFloats]);
			changeCompareFloats[NumFloats] = m_Floats[NumFloats];
			m_Dialog.AddFloatSpin(VarName,m_Floats[NumFloats],VarInfo.m_UiMin,VarInfo.m_UiMax,VarInfo.m_UiStep,CtrlDim, m_Lockeds[i]);
			NumFloats++;
			break;
		case grcEffect::VT_TEXTURE:
			{
				atString texName("<none>");
				grcTexture *pTmpVal;
				p_Shader->GetVar(*pPreset,handle,pTmpVal);
				grcTextureString *pTexString = dynamic_cast<grcTextureString *>(pTmpVal);
				if(pTexString)
					texName = pTmpVal->GetName();

				changeCompareStrings[NumStrings] = m_Strings[NumStrings] = texName;
				m_Dialog.AddTexButton(VarName,m_Strings[NumStrings],CtrlDim, m_Lockeds[i]);
				NumStrings++;
			}
			break;
		case grcEffect::VT_VECTOR2:
			p_Shader->GetVar(*p_Entry->m_Preset, handle,m_V2s[NumV2s]);
			changeCompareV2s[NumV2s] = m_V2s[NumV2s];
			m_Dialog.AddVector2Select(VarName,&m_V2s[NumV2s].x, VarInfo.m_UiMin,VarInfo.m_UiMax,VarInfo.m_UiStep,CtrlDim, m_Lockeds[i]);
			NumV2s++;
			break;
		case grcEffect::VT_VECTOR3:
			p_Shader->GetVar(*p_Entry->m_Preset, handle,m_V3s[NumV3s]);
			changeCompareV3s[NumV3s] = m_V3s[NumV3s];
			m_Dialog.AddVector3Select(VarName,&m_V3s[NumV3s].x, VarInfo.m_UiMin,VarInfo.m_UiMax,VarInfo.m_UiStep,CtrlDim, m_Lockeds[i]);
			NumV3s++;
			break;
		case grcEffect::VT_VECTOR4: 
			p_Shader->GetVar(*p_Entry->m_Preset, handle,m_V4s[NumV4s]);
			changeCompareV4s[NumV4s] = m_V4s[NumV4s];
			m_Dialog.AddVector4Select(VarName,&m_V4s[NumV4s].x, VarInfo.m_UiMin,VarInfo.m_UiMax,VarInfo.m_UiStep,CtrlDim, m_Lockeds[i]);
			NumV4s++;
			break;
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		default:
			break;
		}
		
//		m_Dialog.AddCheckBox(	atString("Locked"),
//								m_Lockeds[i],
//								WidgetRect(g_ControlWidth,CtrlDim.Y,g_DialogWidth - g_ControlWidth,MaxEditDialog::g_StaticHeight));
			
		CtrlDim.Y += MaxEditDialog::g_StaticHeight + MaxEditDialog::g_CtrlGapY;
	}

	CtrlDim.Y += MaxEditDialog::g_CtrlBorder;
	CtrlDim.X = (g_DialogWidth >> 1) - MaxEditDialog::g_ButtonWidth - MaxEditDialog::g_CtrlGapX;
	CtrlDim.Width = (MaxEditDialog::g_ButtonWidth << 1) + (MaxEditDialog::g_CtrlGapX << 1);
	CtrlDim.Height = MaxEditDialog::g_ButtonHeight;

	m_Dialog.AddOKAndCancel(CtrlDim);
	m_Dialog.SetTitle(p_Name);
	m_Dialog.SetSize(g_DialogWidth,CtrlDim.Y + (MaxEditDialog::g_CtrlBorder << 1));

	//create and show it

	if(!m_Dialog.DoModal(ParentWnd))
	{
		return false;
	}

	//got here so set the variables back into the shader preset

	// mp_Entry->m_Preset->SetComment(m_Comment);

	NumFloats = 0;
	NumInts = 0;
	NumStrings = 0;
	NumV2s = 0;
	NumV3s = 0;
	NumV4s = 0;

	for(i=0;i<Count;i++)
	{
		grmVariableInfo *pVarInfo;
		p_Shader->GetInstancedVariableInfo(i,pVarInfo);
		grmVariableInfo &VarInfo = *pVarInfo;

		grcEffectVar handle = p_Shader->LookupVar(VarInfo.m_Name);

//		if(m_Lockeds[i])
//			VarInfo.m_Flags.Set(grmVariableInfo::FLG_PRESET_ONLY);
//		else
//			VarInfo.m_Flags.Clear(grmVariableInfo::FLG_PRESET_ONLY);
//		p_Shader->SetInstancedVariableInfo(i,VarInfo);

		bool changed = false;

		switch(VarInfo.m_Type)
		{
		case grcEffect::VT_FLOAT:
			//only change if different
			if(m_Floats[NumFloats] != changeCompareFloats[NumFloats])
			{
				p_Shader->SetVar(*p_Entry->m_Preset, handle, m_Floats[NumFloats]);
				changed = true;
			}
			NumFloats++;
			break;
		case grcEffect::VT_TEXTURE:
			if(m_Strings[NumStrings] != changeCompareStrings[NumStrings])
			{
				if(m_Strings[NumStrings]=="<none>")
				{
					// prevent serialising
					changeCompareLockeds[i] = m_Lockeds[i] = false;
				}
				else
				{
					grcTexture *pTex = grcTextureFactory::GetInstance().Create(m_Strings[NumStrings].c_str(),NULL);
					if(pPreset)
						p_Shader->SetVar(*pPreset,handle,pTex);
					else
						p_Shader->SetVar(handle, pTex);
					changed = true;
				}
			}
			NumStrings++;
			break;
		case grcEffect::VT_VECTOR2:
			if(m_V2s[NumV2s] != changeCompareV2s[NumV2s])
			{
				p_Shader->SetVar(*p_Entry->m_Preset, handle, m_V2s[NumV2s]);
				changed = true;
			}
			NumV2s++;
			break;
		case grcEffect::VT_VECTOR3:
			if(m_V3s[NumV3s] != changeCompareV3s[NumV3s])
			{
				p_Shader->SetVar(*p_Entry->m_Preset, handle, m_V3s[NumV3s]);
				changed = true;
			}
			NumV3s++;
			break;
		case grcEffect::VT_VECTOR4: 
			if(m_V4s[NumV4s] != changeCompareV4s[NumV4s])
			{
				p_Shader->SetVar(*p_Entry->m_Preset, handle, m_V4s[NumV4s]);
				changed = true;
			}
			NumV4s++;
			break;
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		default:
			pPreset->SetPresetFlag(handle, false);
			break;
		}

		if(changeCompareLockeds[i] != m_Lockeds[i] || changed)
			pPreset->SetPresetFlag(handle, changed?true:m_Lockeds[i]);
	}

	return true;
}
