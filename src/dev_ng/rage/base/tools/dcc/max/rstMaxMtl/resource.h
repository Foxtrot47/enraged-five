//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by rstMaxMtl.rc
//
#define IDD_DLG_MATEDIT                 101
#define IDD_DLG_DBCHOOSER               102
#define IDB_BMP_ROCKSTAR                102
#define IDR_MNU_DBCHOOSER               103
#define IDD_DLG_ABOUT                   103
#define IDB_BMP_ILCHOOSER               104
#define IDD_DLG_INPUTTEXT               105
#define IDD_DLG_OPTIONS                 106
#define IDD_DLG_MTLPRESETS              107
#define IDC_CBO_SHADERNAME              1001
#define IDC_EDT_DIR                     1001
#define IDC_RDO_PRESET                  1004
#define IDC_BTN_DIR                     1005
#define IDC_RDO_CUSTOM                  1005
#define IDC_TREE_DATA                   1007
#define IDC_BTN_PRESET                  1007
#define IDC_EDT_INPUTTEXT               1010
#define IDC_EDT_SHADERNAME              1010
#define IDC_STC_INPUTMESSAGE            1011
#define IDC_EDT_ALPHA_NAME              1011
#define IDC_BTN_ABOUT                   1012
#define IDC_CHK_TWOSIDED                1013
#define IDC_BUTTON1                     1014
#define IDC_CHK_TWOSIDED2               1014
#define IDC_CHK_LIGHTING                1014
#define IDC_BTN_SAVE_MATPRESET          1014
#define IDC_CBO_TEX_SCALE               1015
#define IDC_BTN_SAVE_MATPRESET2         1015
#define IDC_BTN_UNLOCK_MATPRESET        1015
#define IDC_CHK_LIGHTING2               1016
#define IDC_CHK_AMB_LIGHTING            1016
#define IDC_CHK_TEXSCALE                1017
#define IDC_CHK_DIFFUSEALPHA            1018
#define IDC_BUTT_RELOAD_PRESET          1019
#define IDC_BUTTON3                     1020
#define IDC_BUTT_RELOAD_CUSTOM          1020
#define IDC_EDIT1                       1020
#define IDC_EDT_COMMENT                 1020
#define IDC_STC_VERSION                 1023
#define IDC_STC_RAGE                    1024
#define IDC_STC_BUILDTIME               1025
#define ID_ENTRY_ADD                    40001
#define ID_ENTRY_ADDGROUP               40002
#define ID_ENTRY_REMOVE                 40003
#define ID_HELP_ABOUT                   40005
#define ID_ENTRY_EDITPRESET             40006
#define ID_ENTRY_EDITPRESETTYPE         40007
#define ID_ENTRY_EDITPRESETVARS         40008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40009
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
