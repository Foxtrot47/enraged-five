#pragma warning( disable: 4265 )
#pragma warning( disable: 4062 )
#include <max.h>
#include <ID3D9GraphicsWindow.h>
#include <iparamb2.h>
#include <pbbitmap.h>
#include <IPathConfigMgr.h>
#include "EffectElements.h"

#include "devil/ilu.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "devil/il_internal.h"
#ifdef __cplusplus
}
#endif

#include "file/asset.h"
#include "file/device.h"

class FloatElement;
class ColorElement;
class TransformElement;
class TextureElement;
class LightElement;


//////////////////////////////////////////////////////////////////////////
// EffectElements
//	This class is the base for the classes that contain the data set and read
//	from ElementContainer through the IParameterManager and IEffectManager interfaces
#pragma region EffectElements
//////////////////////////////////////////////////////////////////////////

#define NOT_SET -1
EffectElements::EffectElements()
{

	semantic = kSemanticUnknown;
	valid = false;
	renderNode = NULL;
	elementStyle = NOT_SET;
	hParameter = NULL;

}

void EffectElements::SetGenericData(D3DXHANDLE handle, int uiID, int DlgInt, TCHAR * UIName, TCHAR * paramName,LPDIRECT3DDEVICE9 dev)
{
	this->hParameter = handle;
	this->UIName = TSTR(UIName);
	this->ParameterName = TSTR(paramName);
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
// FloatElement : EffectElements
//	The simplest of parameters.
#pragma region FloatElement
//////////////////////////////////////////////////////////////////////////
class FloatElement : public EffectElements
{
	float defaultVal;
	//float setVal;
public:
	FloatElement(float def);
	bool GetValue(void * data);
	bool SetValue(void * val);
	void GetParamValidity(IParamBlock2 * pblock, TimeValue t, Interval & valid){};

};

FloatElement::FloatElement(float def)
{
	defaultVal = def;
	SetStyle(EffectElements::kEleFloat);
}

bool FloatElement::SetValue(void * val)
{
	defaultVal = *((float*)val);
	return true;
}

bool FloatElement::GetValue(LPVOID data)
{
	*(float*)data = defaultVal;
	return true;
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
// Point4Element : EffectElements
//	Guess what this is.
#pragma region Point4Element
//////////////////////////////////////////////////////////////////////////
class Point3Element : public EffectElements
{
	Point3 defaultVal;
public:
	Point3Element(Point3 def);
	bool GetValue(void * data);
	bool SetValue(void * val);
	void GetParamValidity(IParamBlock2 * pblock, TimeValue t, Interval & valid){};

};

Point3Element::Point3Element(Point3 def)
{
	defaultVal = def;
	SetStyle(EffectElements::kElePoint3);
}

bool Point3Element::SetValue(void * val)
{
	defaultVal = *((Point3*)val);
	return true;
}

bool Point3Element::GetValue(LPVOID data)
{
	*(Point3*)data = defaultVal;
	return true;
}

#pragma endregion // Point3Element

//////////////////////////////////////////////////////////////////////////
// Point4Element : EffectElements
//	Guess what this is.
#pragma region Point4Element
//////////////////////////////////////////////////////////////////////////
class Point4Element : public EffectElements
{
	Point4 defaultVal;
	//float setVal;
public:
	Point4Element(Point4 def);
	bool GetValue(void * data);
	bool SetValue(void * val);
	void GetParamValidity(IParamBlock2 * pblock, TimeValue t, Interval & valid){};

};

Point4Element::Point4Element(Point4 def)
{
	defaultVal = def;
	SetStyle(EffectElements::kEleFloat);
}

bool Point4Element::SetValue(void * val)
{
	defaultVal = *((Point4*)val);
	return true;
}

bool Point4Element::GetValue(LPVOID data)
{
	*(Point4*)data = defaultVal;
	return true;
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
// IntElement : EffectElements
//	The simplest of parameters.
#pragma region IntElement
//////////////////////////////////////////////////////////////////////////
class IntElement : public EffectElements
{
	int defaultVal;
	//float setVal;
public:
	IntElement(int def);
	bool GetValue(void * data);
	bool SetValue(void * val);
	void GetParamValidity(IParamBlock2 * pblock, TimeValue t, Interval & valid){};

};

IntElement::IntElement(int def)
{
	defaultVal = def;
	SetStyle(EffectElements::kEleInt);
}

bool IntElement::SetValue(void * val)
{
	defaultVal = *((int*)val);
	return true;
}

bool IntElement::GetValue(LPVOID data)
{
	*(int*)data = defaultVal;
	return true;
}

#pragma endregion


//////////////////////////////////////////////////////////////////////////
// BoolElement : EffectElements
//	The simplest of parameters.
#pragma region BoolElement
//////////////////////////////////////////////////////////////////////////
class BoolElement : public EffectElements
{
	bool defaultVal;
	//float setVal;
public:
	BoolElement(bool def);
	bool GetValue(void * data);
	bool SetValue(void * val);
	void GetParamValidity(IParamBlock2 * pblock, TimeValue t, Interval & valid){};

};

BoolElement::BoolElement(bool def)
{
	defaultVal = def;
	SetStyle(EffectElements::kEleBool);
}

bool BoolElement::SetValue(void * val)
{
	defaultVal = *((bool*)val);
	return true;
}

bool BoolElement::GetValue(LPVOID data)
{
	*(bool*)data = defaultVal;
	return true;
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
// Class ColorElement : EffectElements
//	Represents a simple float4 value
//	Currently non-animatable
#pragma region ColorElement
//////////////////////////////////////////////////////////////////////////

class ColorElement : public EffectElements
{
	AColor defaultVal;

public:
	ColorElement(AColor def);
	bool GetValue(void * data);
	bool SetValue(void * val);// {return false;}
	void GetParamValidity(IParamBlock2 * pblock, TimeValue t, Interval & valid){};

};

bool ColorElement::SetValue(void * val)
{
	defaultVal = *((AColor*)val);
	return true;
}

/*
bool ColorElement::GetValue(LPVOID data)
{
	*(AColor*)data = defaultVal;
	return true;
}
*/
D3DCOLORVALUE GetD3DColor (AColor c)
{
	D3DCOLORVALUE dc;
	dc.r = c.r;
	dc.g = c.g;
	dc.b = c.b;
	dc.a = c.a;//1.0f;
	return dc;
}

ColorElement::ColorElement(AColor def)
{
	defaultVal = def;
	SetStyle(EffectElements::kEleSwatch);
}

bool ColorElement::GetValue(LPVOID data)
{
	MaxSemantics semantic = GetSemantic();
	TimeValue t = GetCOREInterface()->GetTime();
	*(D3DCOLORVALUE*)data = GetD3DColor(defaultVal);
	//*(D3DCOLORVALUE*)data = effectColor;
	return true;
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
// TransformElement : EffectElements
//	This transform element can be used to represent
//	any of the standard transform parameters eg
//	world, worldInv, projection, etc
#pragma region TransformElement
//////////////////////////////////////////////////////////////////////////
class TransformElement : public EffectElements
{
private:
	ID3D9GraphicsWindow * m_gw;
public:
	TransformElement(){};
	void SetGraphicsWindow(ID3D9GraphicsWindow*pGW){m_gw = pGW;}
	bool GetValue(void * data);
	bool SetValue(void * val) {return false;}
	int GetStyle();

};

int TransformElement::GetStyle()
{
	if(GetSemantic()==kSemanticWorldCamPos || GetSemantic()==kSemanticViewportDimensions)
		return kElePoint4;
	else 
		return kEleTM;
}

bool TransformElement::GetValue(void * data)
{
	D3DXMATRIX	MatWorld;
	D3DXMATRIX	MatView;
	D3DXMATRIX	MatProj;
	D3DXMATRIX  WorldView;
	D3DXMATRIX  WorldViewProj;
	D3DXMATRIX MatTemp,MatTemp2,MatTemp3;
	IDxSceneManager * sceneMgr = IDxSceneManager::GetIDxSceneManager();
	IDxSceneTransformManager *transMgr = sceneMgr->GetTransformManager();

	//! Always the active World..
	MatView  = transMgr->GetD3DMatrix(IDxSceneTransformManager::kViewXform);
	MatProj  = transMgr->GetD3DMatrix(IDxSceneTransformManager::kProjectionXform);

	if(GetStyle()==kEleTM)
	{
		MatWorld = m_gw->GetWorldXform();
		D3DXMatrixMultiply(&MatTemp,&MatWorld,&MatView);
		D3DXMatrixMultiply(&WorldViewProj,&MatTemp,&MatProj);
		D3DXMatrixMultiply(&WorldView,&MatWorld,&MatView);
	}

	switch(GetSemantic())
	{
	case kSemanticWorldView:
		{
			*(D3DXMATRIX*)data  = WorldView;
		}
		break;
	case kSemanticProjection:
		{
			*(D3DXMATRIX*)data =  MatProj;
		}
		break;
	case kSemanticProjectionI:
		{
			D3DXMATRIX MatInvProj;
			D3DXMatrixInverse(&MatInvProj,NULL,&MatProj);
			*(D3DXMATRIX*)data = MatInvProj;
		}
		break;
	case kSemanticProjectionIT:
		{
			D3DXMATRIX MatInvProj;
			D3DXMatrixInverse(&MatInvProj,NULL,&MatProj);
			*(D3DXMATRIX*)data = MatInvProj;
		}
		break;
	case kSemanticWorld:
		{
			*(D3DXMATRIX*)data = MatWorld;
		}
		break;
	case kSemanticView:
		{
			*(D3DXMATRIX*)data = MatView;
		}
		break;
	case kSemanticWorldViewProj:
		{
			*(D3DXMATRIX*)data = WorldViewProj;
		}
		break;
	case kSemanticWorldI:
		{
			D3DXMATRIX MatInvWorld;
			D3DXMatrixInverse(&MatInvWorld,NULL,&MatWorld);
			*(D3DXMATRIX*)data = MatInvWorld;
		}
		break;
	case kSemanticViewI:
		{
			D3DXMATRIX MatInvView;
			D3DXMatrixInverse(&MatInvView,NULL,&MatView);
			*(D3DXMATRIX*)data = MatInvView;
		}
		break;
	case kSemanticWorldViewI:
		{
			D3DXMATRIX MatInvWorldView;
			D3DXMatrixInverse(&MatInvWorldView,NULL,&WorldView);
			*(D3DXMATRIX*)data = MatInvWorldView;
		}
		break;
	case kSemanticWorldViewIT:
		{
			D3DXMATRIX MatWorldViewIT;
			D3DXMatrixInverse(&MatWorldViewIT,NULL,&WorldView);
			D3DXMatrixTranspose(&MatWorldViewIT, &MatWorldViewIT);
			*(D3DXMATRIX*)data = MatWorldViewIT;
		}
		break;

	case kSemanticViewIT:
		{
			D3DXMATRIX MatViewIT;
			D3DXMatrixInverse(&MatViewIT,NULL,&MatView);
			D3DXMatrixTranspose(&MatViewIT, & MatViewIT);
			*(D3DXMATRIX*)data = MatViewIT;
		}
		break;
	case kSemanticWorldIT:
		{
			D3DXMATRIX MatWorldIT;
			D3DXMatrixInverse(&MatWorldIT,NULL,&MatWorld);
			D3DXMatrixTranspose(&MatWorldIT, &MatWorldIT);
			*(D3DXMATRIX*)data = MatWorldIT;
		}
		break;
	case kSemanticWorldViewT:
		{
			D3DXMatrixTranspose(&WorldView,&WorldView);
			*(D3DXMATRIX*)data = WorldView;
		}
		break;
	case kSemanticWorldT:
		{
			D3DXMatrixTranspose(&MatWorld,&MatWorld);
			*(D3DXMATRIX*)data = MatWorld;
		}
		break;
	case kSemanticProjectionT:
		{
			D3DXMatrixTranspose(&MatProj, &MatProj);
			*(D3DXMATRIX*)data = MatProj;
		}
		break;
	case kSemanticWorldCamPos:
		{
			D3DXVECTOR3 camPos,camWPos;
			camPos.x = camPos.y = camPos.z  = 0.0f;

			// Inverse of View Transform to Transform Camera from Direct3D Camera Space
			// to 3ds max World Space

			D3DXMatrixInverse(&MatTemp,NULL,&MatView);

			// Find Camera position in Direct3D World Space
			D3DXVec3TransformCoord(&camWPos, &camPos, &MatTemp);
			D3DXVECTOR4 temp = D3DXVECTOR4(camWPos.x,camWPos.y,camWPos.z,0.0f);
			*(D3DXVECTOR4*)data = temp;

		}
		break;
	case kSemanticViewportDimensions:
		{
			int w =0, h =0;
			m_gw->GetWindowDimension(w,h);
			D3DXVECTOR4 temp(float(w),float(h),0.5,0.5);
			*(D3DXVECTOR4*)data = temp;
		}
		break;

	}
	return true;

}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
// TextureElement : EffectElements
//	Holds a DX (Not Max) Texture element.
#pragma region TextureElement
//////////////////////////////////////////////////////////////////////////
class TextureElement : public EffectElements
{
	TSTR mapName;
	TSTR fileName;
	//	LPDIRECT3DTEXTURE9 pTex;
	//	LPDIRECT3DTEXTURE9 pBumpTex;
	D3DXHANDLE texHandle;

public:

	LPDIRECT3DTEXTURE9 pTex;
	LPDIRECT3DTEXTURE9 pBumpTex;


public:
	enum{

		kTexture2D,
		kTextureCube,
		kTextureVolume,
		kTextureRenderTarget,
	};
	TextureElement(D3DXHANDLE handle,TCHAR * mapName, TCHAR * filename, TCHAR * uiName);
	~TextureElement();
	bool GetValue(LPVOID data) {return false;}
	bool SetValue(void * val) {return false;}
	inline TSTR GetMapName(){return mapName;}
	inline TSTR GetFileName(){return fileName;}
	inline LPDIRECT3DTEXTURE9 GetD3DTexture(){return pTex;}
	inline LPDIRECT3DTEXTURE9 GetD3DBumpTexture(){return pBumpTex;}

};

TextureElement::TextureElement(D3DXHANDLE handle, TCHAR * mapName,TCHAR * filename,TCHAR * uiName)
{
	this->texHandle = handle;
	this->mapName = mapName;
	this->fileName = filename;
	this->SetUIName(CStr(uiName));
	pTex = NULL;
	pBumpTex = NULL;
	SetStyle(EffectElements::kEleTex);
}

TextureElement::~TextureElement()
{
	SAFE_RELEASE(pTex);
	SAFE_RELEASE(pBumpTex);
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
// LightElement : EffectElements
//	Interfaces a light element from Max to DX.
#pragma region LightElement
//////////////////////////////////////////////////////////////////////////
class LightElement : public EffectElements ,public ILightingData
{
	D3DXVECTOR4 defaultVec;
	INode * lightNode;
	int lightType;
	bool target;
	LightElement * parentElement;
	bool useLPRT;
	float lightIntens;
	LPDIRECT3DDEVICE9 pDevice;

public:

	enum{
		kUnassigned,
		kPoint,
		kTarget,
	};

	LightElement(bool target);

	void AddLight(INode *light);
	INode * GetLight(); 
		
	void DeleteLight(int index);
	void DeleteLight(ULONG handle);
	bool GetValue(void * data){return false;}
	bool SetValue(void * val){return false;}
	int GetLightType();
	inline INode * GetActiveLight(){return lightNode;}
	void SetDevice(LPDIRECT3DDEVICE9 dev){pDevice = dev;}


	//from LightingData
	D3DXVECTOR4 GetPosition();
	D3DXVECTOR4 GetDirection();
	D3DXVECTOR4 GetColor();
	float GetHotSpot();
	float GetFallOff();
	D3DXVECTOR4 GetAttenuation();


};
void LightElement::AddLight(INode *light)
{
	char* test = light->GetName();
	lightNode = light;
}
INode * LightElement::GetLight()
{
	char* test = lightNode->GetName();
	return lightNode;
}
LightElement::LightElement( bool target):EffectElements()
{
	SetStyle(EffectElements::kEleLight);
	this->target = target;
	lightNode = NULL;
	lightIntens = 1.0f;

	TCHAR filename[MAX_PATH];
	TCHAR str[MAX_PATH];
	_tcscpy(filename,GetCOREInterface()->GetDir(APP_PLUGCFG_DIR));
	_tcscat(filename,"\\DXDisplay.ini");
	useLPRT = GetPrivateProfileInt(_T("Settings"),_T("LPRTEnabled"),0,filename) ? true : false;

	char* saved_lc = NULL;
	char* lc = setlocale(LC_NUMERIC, NULL); // query  
	if (lc)  
		saved_lc = strdup(lc);  
	lc = setlocale(LC_NUMERIC, "C");  

	int res = GetPrivateProfileString(_T("Settings"),_T("LightMultiplier"),_T("1.0"),str,MAX_PATH,filename);
	if (res && useLPRT) 
		sscanf(str,"%f",&lightIntens);

	if (saved_lc)  
	{  
		lc = setlocale(LC_NUMERIC, saved_lc);  
		free(saved_lc);  
		saved_lc = NULL;  
	} 

}

int LightElement::GetLightType()
{
	if(target)
		return kTarget;
	else
		return kPoint;
}


D3DXVECTOR4 LightElement::GetColor()
{
	LightObject* pLight = NULL;
	TimeValue t = GetCOREInterface()->GetTime();
	D3DXVECTOR4 Color4;
	BOOL inHold = theHold.RestoreOrRedoing();


	if(lightNode && !inHold){
		ObjectState Os  = lightNode->EvalWorldState(t);
		if(Os.obj && Os.obj->SuperClassID() == LIGHT_CLASS_ID) 
		{
			pLight = (LightObject *)Os.obj;
			LightState Ls;
			Interval valid;
			pLight->EvalLightState(t,valid,&Ls);
			Color c = Ls.color * (Ls.intens * lightIntens);	// fudge factor for LPRTS
			Color4 = D3DXVECTOR4(c.r, c.g, c.b, 1.0f);
		}
	}
	else{
		D3DLIGHT9 DLight;
		pDevice->GetLight(0,&DLight);
		Color4 = D3DXVECTOR4(DLight.Diffuse.r * lightIntens,DLight.Diffuse.g*lightIntens,DLight.Diffuse.b*lightIntens,1.0f);
	}
	return Color4;
}

D3DXVECTOR4 LightElement::GetDirection()
{
	Point3 Dir;
	TimeValue t = GetCOREInterface()->GetTime();
	BOOL inHold = theHold.RestoreOrRedoing();

	if(lightNode && !inHold){
		Matrix3 Mat	= lightNode->GetNodeTM(t);			
		return D3DXVECTOR4(Mat.GetRow(2));
	}
	else{
		D3DLIGHT9 DLight;
		pDevice->GetLight(0,&DLight);
		return D3DXVECTOR4(-DLight.Direction.x,-DLight.Direction.y,-DLight.Direction.z,0.0f);
	}
}

D3DXVECTOR4 LightElement::GetPosition()
{
	Point3 Pos;
	TimeValue t = GetCOREInterface()->GetTime();
	BOOL inHold = theHold.RestoreOrRedoing();

	if(lightNode && !inHold){
		Matrix3 Mat = lightNode->GetObjTMAfterWSM(t); 
		Pos	= Mat.GetTrans();
		D3DXVECTOR4 pos = D3DXVECTOR4(Pos.x,Pos.y,Pos.z,1.0f);
		return pos;
	}
	else{
		D3DLIGHT9 DLight;
		pDevice->GetLight(0,&DLight);
		return D3DXVECTOR4(DLight.Position.x,DLight.Position.y,DLight.Position.z,1.0f);
	}


}

float LightElement::GetHotSpot()
{
	LightObject* pLight = NULL;
	TimeValue t = GetCOREInterface()->GetTime();
	float hs = DegToRad(180.0f) / 2.0f;
	BOOL inHold = theHold.RestoreOrRedoing();

	if(lightNode && !inHold){
		ObjectState Os  = lightNode->EvalWorldState(t);
		if(Os.obj && Os.obj->SuperClassID() == LIGHT_CLASS_ID) 
		{
			pLight = (LightObject *)Os.obj;
			LightState Ls;
			Interval valid;
			pLight->EvalLightState(t,valid,&Ls);
			hs = (float) DegToRad(Ls.hotsize) / 2.0f;
		}
	}

	return hs;
}

float LightElement::GetFallOff()
{
	LightObject* pLight = NULL;
	TimeValue t = GetCOREInterface()->GetTime();
	float hs = DegToRad(180.0f) / 2.0f;
	BOOL inHold = theHold.RestoreOrRedoing();

	if(lightNode && !inHold){
		ObjectState Os  = lightNode->EvalWorldState(t);
		if(Os.obj && Os.obj->SuperClassID() == LIGHT_CLASS_ID) 
		{
			pLight = (LightObject *)Os.obj;
			LightState Ls;
			Interval valid;
			pLight->EvalLightState(t,valid,&Ls);
			hs = (float) DegToRad(Ls.fallsize) / 2.0f;
		}
	}
	return hs;
}

D3DXVECTOR4 LightElement::GetAttenuation()
{
	//! this is not needed for our support in the material
	return D3DXVECTOR4(0,0,0,0);
}

#pragma endregion

//////////////////////////////////////////////////////////////////////////
// ElementContainer
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
#pragma region IEffectManager
//////////////////////////////////////////////////////////////////////////

void ElementContainer::SetLightParam(D3DXHANDLE handle,TCHAR * name,TCHAR * uiName, bool target,MaxSemantics semantic)
{
	LightElement * EffectParam = new LightElement(target);
	//EffectParam->AddLight(GetCOREInterface()->GetINodeByName("Spot01"));
	EffectParam->SetDevice(m_pd3dDevice);
	AddAParam(EffectParam, handle, name, semantic);
}

void ElementContainer::SetTextureParam(D3DXHANDLE handle, TCHAR * name,TCHAR * uiName, TCHAR * filename, MaxSemantics semantic, bool mappingEnabled, int mappingChannel)
{
	TextureElement * EffectParam = FindTextureElement(filename);

	if(!EffectParam)
	{
		EffectParam  = new TextureElement(handle,name,filename,uiName);
		AddAParam(EffectParam, handle, name, semantic);
		return;
	}
	EffectParam->SetSemantic(semantic);
	EffectParam->SetParamHandle(handle);
	EffectParam->SetUIName(CStr(uiName));
}

void ElementContainer::SetTransformParam(D3DXHANDLE handle, MaxSemantics semantic)
{
	EffectElements * effectParam = new TransformElement();
	AddAParam(effectParam, handle, (TSTR)(handle), semantic);
}

void ElementContainer::SetPoint3Param(D3DXHANDLE handle, TCHAR * paramName, TCHAR * uiName,D3DXVECTOR3 val,float min, float max, float step,MaxSemantics semantic)
{
	Point3 point3 = Point3(val.x, val.y, val.z);
	EffectElements * effectParam = new Point3Element(point3);
	AddAParam(effectParam, handle, (TSTR)(handle), semantic);
}

void ElementContainer::SetPoint4Param(D3DXHANDLE handle, TCHAR * paramName, TCHAR * uiName,D3DXVECTOR4 val,float min, float max, float step,MaxSemantics semantic)
{
	Point4 point4 = Point4(val.x, val.y, val.z, val.w);
	EffectElements * effectParam = new Point4Element(point4);
	AddAParam(effectParam, handle, (TSTR)(handle), semantic);
}

void ElementContainer::SetBooleanParam(D3DXHANDLE handle, TCHAR *paramName, TCHAR *uiName, BOOL val, MaxSemantics semantic)
{
	EffectElements * effectParam = new BoolElement(val);
	AddAParam(effectParam, handle, (TSTR)(handle), semantic);
}

void ElementContainer::SetColorParam(D3DXHANDLE handle,TCHAR * name, TCHAR * uiName,D3DXVECTOR4 color, MaxSemantics semantic)
{
	EffectElements * effectParam = new ColorElement(AColor(color));
	AddAParam(effectParam, handle, name, semantic);
}

void ElementContainer::SetFloatParam(D3DXHANDLE handle, TCHAR * name,TCHAR * uiName, float val,float min, float max, float step,MaxSemantics semantic )
{
	EffectElements * effectParam = new FloatElement(val);
	AddAParam(effectParam, handle, name, semantic);
}

void ElementContainer::SetIntParam(D3DXHANDLE handle, TCHAR * name, TCHAR * uiName,int val, int min, int max, int step,MaxSemantics semantic )
{
	EffectElements * effectParam = new IntElement(val);
	AddAParam(effectParam, handle, name, semantic);
}

void ElementContainer::SetTechnique(D3DXHANDLE handle, TCHAR * techniqueName, bool bDefault)
{
	m_hTechnique = handle;
	m_sTechniqueName = TSTR(techniqueName);
}

#pragma endregion		// IEffectManager inherited functions

//////////////////////////////////////////////////////////////////////////
#pragma region IParameterManager
//////////////////////////////////////////////////////////////////////////

int ElementContainer::GetNumberOfParams()
{
	return NumberOfElements();
}

MaxSemantics ElementContainer::GetParamSemantics(int index)
{
	if(index >= NumberOfElements())
		return kSemanticUnknown;

	return GetElement(index)->GetSemantic();
}

bool ElementContainer::GetParamData(LPVOID data, int index)
{
	if(index >= NumberOfElements())
		return false;

	EffectElements * ele = GetElement(index);
	assert(ele != NULL);

	int eleStyle = ele->GetStyle();
	MaxSemantics semantic = ele->GetSemantic();

	// A transform element requires a pointer to our
	// window to evaluate parameters such as WorldCamPos
	if(eleStyle == EffectElements::kEleTM 
		|| semantic == kSemanticViewportDimensions
		|| semantic == kSemanticWorldCamPos)
	{
		TransformElement * tmEle = static_cast<TransformElement*>(ele);
		tmEle->SetGraphicsWindow(m_pGWindow);
	}

	ele->GetValue(data);

	//switch(ele->GetStyle())
	//{
	//case EffectElements::kEleTM:{
	//	D3DXMATRIX mat;
	//	ele->GetValue((LPVOID)&mat);
	//	*(D3DXMATRIX*)data = mat;
	//	break;
	//							}
	//case EffectElements::kEleFloat:{
	//	float fval;
	//	ele->GetValue((LPVOID)&fval);
	//	*(float*)data = fval;
	//	break;
	//							   }
	//case EffectElements::kEleInt:{
	//	int ival;
	//	ele->GetValue((LPVOID)&ival);
	//	*(int*)data = ival;
	//	break;
	//							 }
	//case EffectElements::kElePoint4:{
	//	Point4 pval;
	//	ele->GetValue((LPVOID)&pval);
	//	*(Point4*)data = pval;
	//	break;
	//								}
	//case EffectElements::kEleBool:{
	//	BOOL bval;
	//	ele->GetValue((LPVOID)&bval);
	//	*(BOOL*)data = bval;
	//	break;
	//							  }
	//case EffectElements::kEleSwatch:{
	//	D3DCOLORVALUE cval;
	//	ele->GetValue((LPVOID)&cval);
	//	*(D3DCOLORVALUE*)data = cval;
	//	break;
	//								}
	//}
	return true;
}

bool ElementContainer::GetParamData(LPVOID data, const TCHAR * paramName)
{
	EffectElements* ele = GetElementByName(paramName);
	if (ele != NULL) 
	{
		ele->GetValue(data);
		return true;
	}
	return false;
}

const TCHAR * ElementContainer::GetParamName(int index)
{
	if(index >= NumberOfElements())
		return NULL;

	return GetElement(index)->GetParameterName().data();
}

int ElementContainer::GetParamType(int index)
{
	if (index >= NumberOfElements())
		return IParameterManager::kPType_Unknown;

	switch (GetElement(index)->GetStyle())
	{
	case EffectElements::kEleBool:
		return IParameterManager::kPType_Bool;
	case EffectElements::kEleSwatch:
		return IParameterManager::kPType_Color;
	case EffectElements::kEleFloat:
		return IParameterManager::kPType_Float;
	case EffectElements::kEleInt:
		return IParameterManager::kPType_Int;
	case EffectElements::kElePoint4:
		return IParameterManager::kPType_Point4;
	}
	return IParameterManager::kPType_Unknown;
}

int ElementContainer::GetParamType(const TCHAR * paramName)
{
	int numberOfParams = GetNumberOfParams();

	for (int i = 0; i < numberOfParams; i++)
	{
		EffectElements * ele = GetElement(i);
		if (ele && (_tcscmp(ele->GetParameterName().data(), paramName)==0))
			return GetParamType(i);
	}
	return IParameterManager::kPType_Unknown;

}

#if (MAX_RELEASE >= 12000)
int ElementContainer::GetParamSize(int index)
{
	return 0;
}
#endif // (MAX_RELEASE >= MAX_RELEASE_R12)

#pragma endregion	// IParameterManager inherited functions

//////////////////////////////////////////////////////////////////////////
#pragma region ILightManager
//////////////////////////////////////////////////////////////////////////

ILightingData * ElementContainer::GetLightingData(D3DXHANDLE handle)
{
	int numberOfLights = NumberOfElementsByType(EffectElements::kEleLight);
	for(int i=0; i<numberOfLights;i++)
	{
		LightElement * le = static_cast<LightElement*>(GetElementByType(i,EffectElements::kEleLight));
		if(_tcsicmp(le->GetParameterName(),handle)==0)
			return (ILightingData*)le;
	}
	return NULL;
}
#pragma endregion		// ILightManager inherited functions

//////////////////////////////////////////////////////////////////////////
#pragma region ElementContainer
//////////////////////////////////////////////////////////////////////////

ElementContainer::ElementContainer()
:	m_pGWindow(NULL)
,	m_pd3dDevice(NULL)
,	m_hTechnique(NULL)
{
	
}

ElementContainer::~ElementContainer()
{
	DeleteAllElements();
}

void ElementContainer::LoadTextureParam(PBBitmap* pBmInfo, IEffectParser* pParser)
{
	if (pParser == NULL) return;

	TextureElement* pTexEle = static_cast<TextureElement*>(GetElementByType(0, EffectElements::kEleTex));
	if (pTexEle != NULL)
	{
		TSTR sTexPath;
		if (pBmInfo != NULL && pBmInfo->bi.Name() != NULL)
		{
			sTexPath = pBmInfo->bi.Name();
		}
		else 
		{
			sTexPath = pTexEle->GetFileName();
		}
		
#if (MAX_RELEASE >= 12000)			
		IFileResolutionManager* pFRM = IFileResolutionManager::GetInstance();
		MaxSDK::Util::Path resolvedPath(_M(sTexPath));
		bool res = pFRM->GetFullFilePath(resolvedPath, MaxSDK::AssetManagement::kBitmapAsset);
#else
		MaxSDK::Util::Path resolvedPath(sTexPath);
		bool res = IPathConfigMgr::GetPathConfigMgr()->SearchForExternalFiles(resolvedPath);
#endif // (MAX_RELEASE >= MAX_RELEASE_R12)
		if(res && !pParser->LoadTexture(m_pGWindow->GetDevice(), pTexEle->GetParameterName().data(), const_cast<TCHAR*>(resolvedPath.GetCStr()), true))
		{
			// TODO: Something notify-wise here
		}
	}
}


VOID WINAPI ColorFillWhite (D3DXVECTOR4* pOut, const D3DXVECTOR2* pTexCoord, const D3DXVECTOR2* pTexelSize, LPVOID pData)
{
    *pOut = D3DXVECTOR4(255.0f, 255.0f, 255.0f, 255.0f);
}

VOID WINAPI ColorFillEnvironment(D3DXVECTOR4* pOut, const D3DXVECTOR2* pTexCoord, const D3DXVECTOR2* pTexelSize, LPVOID pData)
{
	Point3 ambient =  GetCOREInterface()->GetViewportBGColor();
	*pOut = D3DXVECTOR4(ambient.x*255.0f, ambient.y*255.0f, ambient.z*255.0f, 255.0f);
}

bool ElementContainer::CreateTextureFromTIF( TSTR sTexPath, TextureElement* pTexEle, float scale)
{
	ilInit();
	ilEnable(IL_ORIGIN_SET);
	ilOriginFunc(IL_ORIGIN_UPPER_LEFT);

	unsigned int devilImageId;
	ilGenImages(1, &devilImageId);
	ilBindImage(devilImageId);

	if (ilLoadImage(ILstring(sTexPath))) 
	{
		// Make sure there is an alpha channel
		ilConvertImage(IL_BGRA, IL_BYTE);

		int iSrcImageWidth = ilGetInteger(IL_IMAGE_WIDTH);
		int iSrcImageHeight = ilGetInteger(IL_IMAGE_HEIGHT);

		int iDstImageWidth = iSrcImageWidth * scale;
		int iDstImageHeight = iSrcImageHeight * scale;

		iluScale(iDstImageWidth, iDstImageHeight, 1);

		D3DFORMAT eDirectXTextureFormat = D3DFMT_A8R8G8B8;
		D3DXCreateTexture(m_pGWindow->GetDevice(), iDstImageWidth, iDstImageHeight, 1, D3DUSAGE_DYNAMIC, 
			eDirectXTextureFormat, D3DPOOL_DEFAULT , &pTexEle->pTex);

		if (pTexEle->pTex==NULL)
		{
			return false;
		}

		D3DLOCKED_RECT lockedRectNew;
		if(!SUCCEEDED(pTexEle->pTex->LockRect(0, &lockedRectNew, NULL, D3DLOCK_DISCARD )))
		{
			return false;
		}  

		unsigned char* pDstTextureData = (unsigned char*)lockedRectNew.pBits;
		unsigned char* pSrcTextureData = (unsigned char*)ilGetData();

		memcpy(pDstTextureData, pSrcTextureData, (iDstImageWidth * iDstImageHeight * 4) );

		pTexEle->pTex->UnlockRect(0);

		ilDeleteImages(1, &devilImageId);

		return true;
	}

	return false;
}

void ElementContainer::LoadTextureParam(TSTR sTexPath, TextureElement *pTexEle, IEffectParser* pParser, float scale, bool isEnvTexture)
{
	if (pParser == NULL) return;

#if (MAX_RELEASE >= 12000)			
	IFileResolutionManager* pFRM = IFileResolutionManager::GetInstance();
	MaxSDK::Util::Path resolvedPath(_M(sTexPath));
	bool exists = ASSET.Exists(sTexPath, "" );
	bool res = pFRM->GetFullFilePath(resolvedPath, MaxSDK::AssetManagement::kBitmapAsset);
#else
	MaxSDK::Util::Path resolvedPath(sTexPath);
	bool res = IPathConfigMgr::GetPathConfigMgr()->SearchForExternalFiles(resolvedPath);
#endif // (MAX_RELEASE >= MAX_RELEASE_R12)
	if( res  && strcmp(sTexPath, "") != 0 && exists)
	{

		SAFE_RELEASE(pTexEle->pTex);


		const char* szTexExt = rage::ASSET.FindExtensionInPath(sTexPath);
		if(_strcmpi(szTexExt, ".tif") == 0)
		{
			char derivedFilename[1024];
			const char* pFilename = rage::ASSET.FileName(sTexPath);
			MSTR maxFilePath = GetCOREInterface()->GetCurFilePath();
			char maxFileLeadingPath[512];
			rage::ASSET.RemoveNameFromPath(maxFileLeadingPath, sizeof(maxFileLeadingPath), maxFilePath);
			sprintf_s(derivedFilename, 512, "%s/%s", maxFileLeadingPath, pFilename);
			if(!CreateTextureFromTIF(sTexPath, pTexEle, scale) && !CreateTextureFromTIF(derivedFilename, pTexEle, scale))
			{
				if( pTexEle->pTex )
					pTexEle->pTex->Release();

				// Create a pure white texture 
				D3DXCreateTexture( m_pGWindow->GetDevice(), 8, 8, 1, D3DUSAGE_DYNAMIC, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &pTexEle->pTex);
				D3DXFillTexture (pTexEle->pTex, ColorFillWhite, NULL);
				if(!pParser->LoadTexture(pTexEle->pTex, pTexEle->GetParameterName().data()))
				{
					// TODO: Something notify-wise here
				}
			}
			else
			{
				if(!pParser->LoadTexture(pTexEle->pTex, pTexEle->GetParameterName().data()))
				{
					// TODO: Something notify-wise here
				}
			}
		}
		else
		{
			D3DXCreateTextureFromFile( m_pGWindow->GetDevice(), sTexPath, &pTexEle->pTex );
			if( pTexEle->pTex )
			{

				D3DSURFACE_DESC* d3dDesc = new D3DSURFACE_DESC();
				pTexEle->pTex->GetLevelDesc(0, d3dDesc);
				int w;
				int h; 

				if( scale == 0.0f )
				{
					w = d3dDesc->Width * 0.125;
					h = d3dDesc->Height * 0.125;
				}
				else
				{
					w = d3dDesc->Width * scale;
					h = d3dDesc->Height * scale;
				}

				SAFE_RELEASE(pTexEle->pTex);

				//int w = 32;
				//int h = 32;

				//HRESULT hres = D3DXCreateTextureFromFileEx(m_pGWindow->GetDevice(), sTexPath, w, h, D3DX_FROM_FILE, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &pTexEle->pTex);
				HRESULT hres = 	D3DXCreateTextureFromFileEx(m_pGWindow->GetDevice(), sTexPath, w, h, 1, 0, D3DFMT_FROM_FILE, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &pTexEle->pTex);

				if(hres == S_OK)
				{
					if(!pParser->LoadTexture(pTexEle->pTex, pTexEle->GetParameterName().data()))
					{
						// TODO: Something notify-wise here
					}
				}
			}
		}

	}
	else
	{
		SAFE_RELEASE(pTexEle->pTex);

		// Create a pure white texture 
		HRESULT success = D3DXCreateTexture( m_pGWindow->GetDevice(), 8, 8, 1, D3DUSAGE_DYNAMIC, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &pTexEle->pTex);
		if(S_OK==success)
		{
			if(isEnvTexture)
				success = D3DXFillTexture (pTexEle->pTex, ColorFillEnvironment, NULL);
			else
				success = D3DXFillTexture (pTexEle->pTex, ColorFillWhite, NULL);
		}
		BOOL loadSuccess = FALSE;
		if(S_OK==success)
		{
			try
			{
				loadSuccess = pParser->LoadTexture(pTexEle->pTex, pTexEle->GetParameterName().data());
			}
			catch (...)
			{
				loadSuccess = FALSE;
			}
		}
		if(S_OK!=success || !loadSuccess)
		{
			MessageBox(GetCOREInterface()->GetMAXHWnd(), "Texture creation failed for parameter!", pTexEle->GetParameterName().data(), MB_ICONERROR);
		}
	}	
}

void ElementContainer::LoadEnvironmentTextureParam(TSTR sTexPath, IEffectParser* pParser, float scale)
{
	if(scale != scale)
		scale = 0.125f;

	TextureElement* pTexEle = new TextureElement(NULL, "ReflectionTex", NULL, NULL);
	pTexEle->SetParameterName("ReflectionTex");
	if (pTexEle != NULL && m_pGWindow)
	{
		LoadTextureParam(sTexPath, pTexEle, pParser, scale, GetCOREInterface()->GetUseEnvironmentMap());
	}
}


void ElementContainer::LoadTextureParam(TSTR sTexPath, int texId, IEffectParser* pParser, float scale)
{
	if(scale != scale)
		scale = 0.125f;

	TextureElement* pTexEle = static_cast<TextureElement*>(GetElementByType(texId, EffectElements::kEleTex));
	if (pTexEle != NULL)
	{
		LoadTextureParam(sTexPath, pTexEle, pParser, scale);
	}
}

void ElementContainer::SetFloatParam(int floatId, IEffectParser* pParser, float val)
{
	FloatElement* pFloatEle = static_cast<FloatElement*>(GetElementByType(floatId, EffectElements::kEleFloat));
	if (pFloatEle != NULL)
	{
		if(!pFloatEle->SetValue( (void*)&val ))
		{
			// TODO: Get naked
		}
	}
}

void ElementContainer::SetIntParam(int intId, IEffectParser* pParser, int val)
{
	IntElement* pIntEle = static_cast<IntElement*>(GetElementByType(intId, EffectElements::kEleInt));
	if (pIntEle != NULL)
	{
		if(!pIntEle->SetValue( (void*)&val ))
		{
			// TODO: Get naked
		}
	}
}

void ElementContainer::SetPoint4Param(int colId, IEffectParser* pParser, Point4 val)
{
	Point4Element* pPoint4Ele = static_cast<Point4Element*>(GetElementByType(colId, EffectElements::kEleSwatch));
	if (pPoint4Ele != NULL)
	{
		Point4 p = Point4(val.x, val.y, val.z, val.w);
		if(!pPoint4Ele->SetValue( (void*)&p ))
		{
			// TODO: Get naked
		}
	}

}

void ElementContainer::SetColorParam(int colId, IEffectParser* pParser, Point4 val)
{
	ColorElement* pColEle = static_cast<ColorElement*>(GetElementByType(colId, EffectElements::kEleSwatch));
	if (pColEle != NULL)
	{
		AColor col = AColor(val.x, val.y, val.z, val.w);
		if(!pColEle->SetValue( (void*)&col ))
		{
			// TODO: Get naked
		}
	}

}
void ElementContainer::SetFloatParam( const TCHAR * paramName, IEffectParser* pParser, float val )
{
	FloatElement* pFloatEle = static_cast<FloatElement*>(GetElementByName(paramName));
	if (pFloatEle != NULL)
	{
		if(!pFloatEle->SetValue( (void*)&val ))
		{
			// TODO: Get naked
		}
	}
}

void ElementContainer::SetIntParam( const TCHAR * paramName, IEffectParser* pParser, int val)
{
	IntElement* pIntEle = static_cast<IntElement*>(GetElementByName(paramName));
	if (pIntEle != NULL)
	{
		if(!pIntEle->SetValue( (void*)&val ))
		{
			// TODO: Get naked
		}
	}
}

void ElementContainer::SetPoint4Param( const TCHAR * paramName, IEffectParser* pParser, Point4 val)
{
	Point4Element* pPoint4Ele = static_cast<Point4Element*>(GetElementByName(paramName));
	if (pPoint4Ele != NULL)
	{
		Point4 p = Point4(val.x, val.y, val.z, val.w);
		if(!pPoint4Ele->SetValue( (void*)&p ))
		{
			// TODO: Get naked
		}
	}
}

void ElementContainer::SetColorParam( const TCHAR * paramName, IEffectParser* pParser, Point4 val)
{
	ColorElement* pColEle = static_cast<ColorElement*>(GetElementByName(paramName));
	if (pColEle != NULL)
	{
		AColor col = AColor(val.x, val.y, val.z, val.w);
		if(!pColEle->SetValue( (void*)&col ))
		{
			// TODO: Get naked
		}
	}
}

EffectElements* ElementContainer::GetElement(int index)
{
	if(index < 0 || index >= m_dElements.Count())
		return NULL;

	return m_dElements[index];
}

void ElementContainer::DeleteAllElements()
{
	int count = m_dElements.Count();
	for(int i =0; i< count; i++)
	{
		delete m_dElements[i];
	}
	m_dElements.SetCount(0);
}

int ElementContainer::NumberOfElementsByType(int Type)
{
	int count = 0;
	for(int i =0; i< m_dElements.Count(); i++)
	{
		if(m_dElements[i]->GetStyle()==Type)
			count++;
	}
	return count;
}

EffectElements * ElementContainer::GetElementByType(int index, int Type)
{
	int count = 0;

	for(int i =0; i< m_dElements.Count(); i++)
	{
		if(m_dElements[i]->GetStyle()==Type)
		{
			if(count == index)
				return m_dElements[i];
			else
				count++;
		}
	}
	return NULL;
}

EffectElements* ElementContainer::GetElementByName(const TCHAR* name)
{
	int count = 0;
	for(int i =0; i< m_dElements.Count(); i++)
	{
		if(m_dElements[i]->GetParameterName() == (const TSTR)name)
		{
			return m_dElements[i];
		}
	}
	return NULL;
}

TextureElement * ElementContainer::FindTextureElement(TCHAR * name)
{
	int numberOfTextures = NumberOfElementsByType(EffectElements::kEleTex);
	for(int i=0; i< numberOfTextures;i++)
	{
		TextureElement * texEle = static_cast<TextureElement*>(GetElementByType(i,EffectElements::kEleTex));
		if(_tcscmp(texEle->GetMapName().data(), name)==0)
			return texEle;
	}
	return NULL;
}

void ElementContainer::AddAParam(EffectElements* param, D3DXHANDLE handle, TCHAR* name, MaxSemantics semantic)
{
	param->SetSemantic(semantic);
	param->SetParamHandle(handle);
	param->SetParameterName(TSTR(name));
	m_dElements.Append(1, &param);
}

void ElementContainer::FreeD3Dtextures()
{
	int numberOfTextures = NumberOfElementsByType(EffectElements::kEleTex);
	for(int i=0; i< numberOfTextures;i++)
	{
		TextureElement * texEle = static_cast<TextureElement*>(GetElementByType(i,EffectElements::kEleTex));
		if(texEle->pTex)
			SAFE_RELEASE(texEle->pTex);
		if(texEle->pBumpTex)
			SAFE_RELEASE(texEle->pBumpTex);
	}
}

#pragma endregion	// ElementContainers local functions
