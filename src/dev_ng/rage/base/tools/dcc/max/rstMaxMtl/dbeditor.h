#ifndef __RST_DBEDITOR_H__
#define __RST_DBEDITOR_H__

#include "atl/string.h"

#include <commctrl.h>

namespace rage {

#define IMAGE_GROUP 0 
#define IMAGE_GROUP_SEL 1
#define IMAGE_ITEM 2 
#define IMAGE_ITEM_SEL 3

//TL : Adding a code toggle for allowing preset editing/creation/removal through the material interface in MAX.
//Disabling it as presets should no longer be manipulated by the artists, but don't want to remove all of the code
//quite yet until this change has made it's way through all the projects
#define ENABLE_PRESET_EDITING 0

class rageRstShaderDb;

////////////////////////////////////////////////////////////////////////////////
class DbEditor
{
public:

	static DbEditor& GetInst();

	bool Show();
	bool ChoosePreset(HWND m_ParentWnd,atString& r_PresetPath);
	void SetRootPath(const atString& r_RootPath);
	bool AddGroup(const char* r_FullPath);

protected:
	DbEditor();
	~DbEditor() {}

	bool IsGroup(HTREEITEM Item);
	HTREEITEM AddTreeItem(HTREEITEM ParentItem,const char* r_Name,bool IsGroup);
	void GetPath(HTREEITEM Item,atString& r_PathOut);
	HTREEITEM GetItem(const char* r_PathIn);
	HTREEITEM GetCurrentGroup();
	void BuildEntries(HTREEITEM Parent,const atString& Level, const bool usePreloadList);
	bool SelectItem(const atString& r_PathIn);

	void OnInitDialog(HWND Wnd);
	void OnCommandOK();
	void OnCommandClose();

#if ENABLE_PRESET_EDITING
	void OnEntryEditPreset();
	void OnEntryAddPreset(bool shaderEdit=false);
	void OnEntryAddGroup();
	void OnEntryRemove();
#endif //ENABLE_PRESET_EDITING

	void OnHelpAbout();
	bool OnNotifyTree(const NMTREEVIEW* p_NmTreeView);

	static INT_PTR CALLBACK DbChooserDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);

	rageRstShaderDb* mp_DB;
	s32 m_NumDBEntries;
	atString m_PresetPath;
	HTREEITEM m_CurrentSel;
	HWND m_Wnd;
	HWND m_TreeWnd;
	bool m_IsChoosing;
};

} //namespace rage {

#endif //__RST_DBEDITOR_H__
