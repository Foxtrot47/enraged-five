#ifndef __RST_MAX_SHADERVALUENODE_H__
#define __RST_MAX_SHADERVALUENODE_H__

/************************************************************************/
/* class CRstShaderValueNode
/* Encapsulate value, metadata and linking to sub-Values
/* Gunnar Droege
/************************************************************************/

#include "grcore/effect.h"
#include "atl/string.h"
#include "rageMaxMaterial/shaderInstance.h"
//#include "dialogShader.h"

namespace rage {

class CRstShaderValueNode;
class RstMtlMax;
class RstMaxShaderDlg;

typedef atArray<CRstShaderValueNode*> RsAttrNodeArray;

#define SHADERVALNAME_IGNORE NULL
#define SHADERVALNAME_ALL ""

class CRstShaderValueNode
{
public:

	enum eShaderValueType
	{
		eShaderValTypeNone		= 0,
		eShaderValTypeUndefined	= 1<<0,
		eShaderValTypeTexture	= 1<<1,
		eShaderValTypeFloat		= 1<<2,
		eShaderValTypeInt		= 1<<3,
		eShaderValTypeVec2		= 1<<4,
		eShaderValTypeVec3		= 1<<5,
		eShaderValTypeVec4		= 1<<6,
		eShaderValTypeBool		= 1<<7,
		eShaderValTypeString	= 1<<8,
		eShaderValTypeList		= 1<<9,
		eShaderValTypeMatrix43	= 1<<10,
		eShaderValTypeMatrix44	= 1<<11,
		eAllShaderValTypes	= 0xFFFF
	};
	enum eShaderValueFlags
	{
		eShaderValFlagNone				= 0,
		eShaderValFlagIsPreset			= 1<<0,
		eShaderValFlagIsTwoSided		= 1<<1,
		eShaderValFlagHasLighting		= 1<<2,
		eShaderValFlagHasAmbLighting	= 1<<3,
		eShaderValFlagIsHwMode			= 1<<4,
		eAllShaderValFlags				= 0xFFFF
	};
private:
	typedef union
	{
		Texmap* m_Texture;
		f32 m_Float;
		s32 m_Int;
		Vector2 *m_Vector2;
		Vector3 *m_Vector3;
		Vector4 *m_Vector4;
		bool m_Bool;
		void *m_Void;
	} UVal;

	atString m_name;
	ShaderInstanceData::ShaderVar m_rageVar;
	UVal m_value;
	eShaderValueType m_type;
	u32 m_flags;
	int m_RefTargetIndex;
	RstMtlMax *pRstMtlMax;

	CRstShaderValueNode *m_parent;
	RsAttrNodeArray m_inputs;

	// Dialog stuff
	RstMaxShaderDlg* mp_DlgExtra;
	HWND m_MaxDataHWnd;

	// cleanup
	bool mb_deleteMe;

public:

	static eShaderValueType gGrcEffectTypeLUT[];

	CRstShaderValueNode(const RstMtlMax *rstMtlMax)
		: m_type(eShaderValTypeNone)
		, pRstMtlMax(const_cast<RstMtlMax*>(rstMtlMax))
		, m_name("Shader Parameters")
//		, m_rageVar(ShaderVar(grcEffect::VT_NONE, 0))
		, m_flags((eShaderValueFlags)NULL)
		, m_parent(NULL)
		, m_RefTargetIndex(NULL)
		, mb_deleteMe(true)
		, mp_DlgExtra(NULL)
		, m_MaxDataHWnd(NULL)
	{
		m_value.m_Void = NULL;
	}
	//copy constructor
	//Shallow
	CRstShaderValueNode(const RstMtlMax *rstMtlMax, CRstShaderValueNode &cp, CRstShaderValueNode *parent);
	CRstShaderValueNode(
		const RstMtlMax *rstMtlMax,
		atString &name,
		ShaderInstanceData::ShaderVar rageVar,
		eShaderValueType type,
		u32 flags,
		int refTargetIndex
		);
	~CRstShaderValueNode();

	eShaderValueType Type(){return m_type;}
	UVal &Value(){return m_value;}
	u32 &Flags(){return m_flags;}
	RsAttrNodeArray &Inputs(){return m_inputs;}
	atString &Name(){return m_name;}
	int &RefTargetIndex(){return m_RefTargetIndex;}
	ShaderInstanceData::ShaderVar &GetShaderVar(){return m_rageVar;}
	CRstShaderValueNode *&Parent(){return m_parent;}
	bool &DeleteMe(){return mb_deleteMe;}
	RstMaxShaderDlg *&Dialog(){return mp_DlgExtra;}
	HWND &MaxDataHWnd(){return m_MaxDataHWnd;}
	void ToggleDialog();
	void DeleteDialog();
	CRstShaderValueNode *GetRoot()
	{
		CRstShaderValueNode *ret = m_parent;
		while(ret!=NULL)
			ret = ret->m_parent;
		return ret;
	}
	void ClearInputs()
	{
		m_inputs.clear();
	}
	
	RstMaxShaderDlg* CreateShaderParamDialog(IMtlParams* pMtlParams, grcEffect *p_Shader=NULL);
	BOOL OpenShaderParamDialog();

};

class CRstShaderValueNodeMtl : public CRstShaderValueNode
{
	
};

class CRstShaderValueNodeTexture : public CRstShaderValueNode
{

};

} //namespace rage
#endif //__RST_MAX_SHADERVALUENODE_H__