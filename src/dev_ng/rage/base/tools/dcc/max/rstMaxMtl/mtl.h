#ifndef __RST_MAX_MTL_WRAPPER_H__
#define __RST_MAX_MTL_WRAPPER_H__

#define RSTMATERIAL_CLASS_ID Class_ID(0x75e85713, 0x1f0b306c)
#define	RSTMTL_HW_SHADER 1

#define SHADER_ATTR_TREE 0

#include "MaterialPresets/MaterialPreset.h"
#include "IHardwareMaterial.h"

// rage includes

#include "atl/array.h"
#include "atl/string.h"
#include "atl/map.h"
#include "grcore/effect.h"
#include "grmodel/shader.h"
#include "rexBase/value.h"



#pragma warning( disable: 4265 )
#pragma warning( disable: 4263 )
#pragma warning( disable: 4062 )
#include "IViewportManager.h" 
#include "idx9vertexshader.h"
#include "IStdDualVS.h"
#include "id3d9graphicswindow.h"
#include "RTMax.h"

#include "RstShaderValueNode.h"
#include "rageMaxMaterial/shaderInstance.h"

#if RSTMTL_HW_SHADER
// DX related includes
#include "effectElements.h"
#include <IMaterialViewportShading.h> 
#include "notify.h"
#define NSUBTEX 1
#endif // RSTMTL_HW_SHADER

#ifdef RSTMAXMTL_DLL
#define RSTMAXMTL_EXPORT __declspec( dllexport )
#else
#ifdef RSTMAXMTL_NOIMPORT
#define RSTMAXMTL_EXPORT 
#else
#define RSTMAXMTL_EXPORT __declspec( dllimport )
#endif //RSTMAXMTL_NOIMPORT
#endif //RSTMAXMTL_DLL

#include "IRageShaderMaterial.h"

#define		N_ID_CHANNELS	16		// number of ids in stdMat#define


namespace rage {

class fiStream;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
#if RSTMTL_HW_SHADER
class RstMtlMax : 
	public Mtl, 
	public IRageShaderMaterial, 
	public IDX9DataBridge, 
	public IDX9VertexShader, 
	public IStdDualVSCallback, 
	public IMaterialViewportShading
//	,public PostLoadCallback
#else
class RstMtlMax : public Mtl, public IRageShaderMaterial
#endif //RSTMTL_HW_SHADER
{
public:


	static ClassDesc2* Creator();

	friend class RstMaxMtlDlg;
	friend class RstMaxShaderDlg;
	friend class RstMaxErrorDlg;
	friend class RstMaxMtlPresetDlg;

	RstMtlMax();
	RSTMAXMTL_EXPORT RstMtlMax(ShaderInstanceData* instanceData, atString shaderName);
	~RstMtlMax();
#if RSTMTL_HW_SHADER
	static void D3dPreNotifyProc( void *param, NotifyInfo *info );
	static void D3dPostNotifyProc( void *param, NotifyInfo *info );
#endif // RSTMTL_HW_SHADER
	
#pragma region Animatable
	//from Animatable
	virtual Class_ID ClassID() { return RSTMATERIAL_CLASS_ID; }
	virtual void DeleteThis() { delete this; }
	virtual int NumSubs();
	virtual Animatable* SubAnim(int i);
#pragma endregion

#pragma region ReferenceTarget
	//from ReferenceTarget
#if ( MAX_RELEASE < 9000 ) 
	virtual RefTargetHandle Clone(RemapDir &remap = NoRemap());
#else
	virtual RefTargetHandle	Clone(RemapDir &remap = DefaultRemapDir());
#endif
#pragma endregion

#pragma region ReferenceMaker
	//from ReferenceMaker

	virtual RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message);
	virtual int NumRefs();
	virtual RefTargetHandle GetReference(int i);
	virtual void SetReference(int i,RefTargetHandle rtarg);
	// custom added method
	virtual int GetReferenceIndex(RefTargetHandle rtarg);
#pragma endregion

#pragma region ISubMap
	//from ISubMap
	virtual int NumSubTexmaps();
	virtual Texmap* GetSubTexmap(int i);
	virtual void SetSubTexmap(int i,Texmap *m);
	virtual int SubTexmapOn(int i);

	void UpdateMapName(s32 Index,const atString& r_MapName);
#pragma endregion

#pragma region MtlBase
	//from MtlBase
#if RSTMTL_HW_SHADER
	virtual void Update(TimeValue t, Interval& valid);
#else
	virtual void Update(TimeValue t, Interval& valid) {}
#endif // RSTMTL_HW_SHADER
	virtual void Reset();
	virtual Interval Validity(TimeValue t) { return FOREVER; }
	virtual ParamDlg* CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp);
	virtual IOResult Load(ILoad *iload);
	virtual IOResult Save(ISave *isave);

	void SetTexOps(Material *mtl, int i, int type);
	void SetHWTexOps(IHardwareMaterial *mtl, int i, int type);
	void DiscardTexHandles();
	BOOL SupportTexDisplay() { return TRUE; }
	BOOL SupportsMultiMapsInViewport() { return TRUE; }
	void ActivateTexDisplay(BOOL  onoff) {;}
	DWORD_PTR GetActiveTexHandle(TimeValue t,TexHandleMaker& thmaker);
	void SetupGfxMultiMaps(TimeValue t,Material *mtl,MtlMakerCallback &cb);
	void SetupGfxMultiMapsForHW(TimeValue t, Material *mtl, MtlMakerCallback &cb, IHardwareMaterial *pIHWMat, BOOL bShader);
	ULONG LocalRequirements(int subMtlNum); 

#pragma endregion

#pragma region Mtl
	//from Mtl
	virtual void GetClassName(TSTR& s) { s = TSTR(_T("Rage Shader")); }  
	virtual Color GetAmbient(int mtlNum=0, BOOL backFace=FALSE) { return Color(0.0f,0.0f,0.0f); } 
	virtual Color GetDiffuse(int mtlNum=0, BOOL backFace=FALSE) { return Color(0.58f,0.58f,0.58f); } 	    
	virtual Color GetSpecular(int mtlNum=0, BOOL backFace=FALSE) { return Color(0.0f,0.0f,0.0f); } 
	virtual float GetShininess(int mtlNum=0, BOOL backFace=FALSE) { return 0.0f; }
	virtual	float GetShinStr(int mtlNum=0, BOOL backFace=FALSE) { return 0.0f; }
	virtual float GetXParency(int mtlNum=0, BOOL backFace=FALSE) { return 0.0f; }
	virtual BOOL GetSelfIllumColorOn(  int  mtlNum = 0,BOOL  backFace = FALSE) { return FALSE; }

	virtual void SetAmbient(Color c, TimeValue t) {}
	virtual void SetDiffuse(Color c, TimeValue t) {}		
	virtual void SetSpecular(Color c, TimeValue t) {}
	virtual void SetShininess(float v, TimeValue t) {}
	virtual void Shade(ShadeContext& sc);
	bool EvalColorStdChannel(ShadeContext &  sc,int  stdID,Color & outClr);
	bool EvalMonoStdChannel(ShadeContext &  sc,int  stdID,float& outVal);




#pragma endregion

#pragma region Tesselation
	
	enum TesselationType
	{
		TESS_NONE = 0,
		TESS_VERTS = 1,
		TESS_VERTS_NORMALS = 2
	};

#pragma endregion

#pragma region PostLoadCallback
// 	void  	proc (ILoad *iload)
// 	{
// 		if(m_ShaderInstanceData)
// 			m_ShaderInstanceData->LoadAssetPaths();
// 	}
#pragma endregion

#pragma region ExposedMethods
	//unique exposed methods
	RSTMAXMTL_EXPORT const atString& GetShaderName() const { return m_ShaderName; }
	RSTMAXMTL_EXPORT s32 GetVariableCount();
	RSTMAXMTL_EXPORT void GetVariableName(s32 i,atString& r_name);			// The variable's display name
	RSTMAXMTL_EXPORT void GetShaderParameterName(s32 i,atString& r_name);	// The variable's name as it appears in the shader parameters
	RSTMAXMTL_EXPORT grcEffect::VarType GetVariableType(s32 i);
	RSTMAXMTL_EXPORT TypeBase::eVariableType GetPresetVariableType(s32 i);
	RSTMAXMTL_EXPORT void GetVariable(s32 i,atString& r_texmap);
	RSTMAXMTL_EXPORT void GetVariable(s32 i,f32& r_float);
	RSTMAXMTL_EXPORT void GetVariable(s32 i,s32& r_int);
	RSTMAXMTL_EXPORT void GetVariable(s32 i,Vector2& r_vec);
	RSTMAXMTL_EXPORT void GetVariable(s32 i,Vector3& r_vec);
	RSTMAXMTL_EXPORT void GetVariable(s32 i,Vector4& r_vec);
	RSTMAXMTL_EXPORT void GetVariable(s32 i,bool& r_bool);

	RSTMAXMTL_EXPORT void SetShaderName(const atString& r_ShaderName);
	RSTMAXMTL_EXPORT void SetVariable(s32 i,const atString& r_texmap);
	RSTMAXMTL_EXPORT void SetVariable(s32 i,const f32& r_float);
	RSTMAXMTL_EXPORT void SetVariable(s32 i,const s32& r_int);
	RSTMAXMTL_EXPORT void SetVariable(s32 i,const Vector2& r_vec);
	RSTMAXMTL_EXPORT void SetVariable(s32 i,const Vector3& r_vec);
	RSTMAXMTL_EXPORT void SetVariable(s32 i,const Vector4& r_vec);
	RSTMAXMTL_EXPORT void SetVariable(s32 i,const bool& r_bool);

	RSTMAXMTL_EXPORT void GetAlphaTextureName(s32 i,atString& r_texmap);
	RSTMAXMTL_EXPORT void SetAlphaTextureName(s32 i,atString& r_textureName);
	RSTMAXMTL_EXPORT s32  GetAlphaTextureCount();

	RSTMAXMTL_EXPORT const atString GetTextureTemplate(s32 i);
	RSTMAXMTL_EXPORT const atString GetTextureTemplateRelative(s32 i);
	RSTMAXMTL_EXPORT bool GetTextureTemplateOverride(s32 i);
	
	RSTMAXMTL_EXPORT bool GetShaderValue(const atString& r_Name, rexValue& r_ValueGet);
	RSTMAXMTL_EXPORT bool GetShaderValue(const grmVariableInfo& r_VarInfo,rexValue& r_ValueGet);
	RSTMAXMTL_EXPORT TesselationType GetTesselationType() const { return m_tesselationType; }

#if !SHADER_ATTR_TREE
	RSTMAXMTL_EXPORT bool IsPreset() const { return m_IsPreset; }
	RSTMAXMTL_EXPORT bool IsTwoSided() const { return m_IsTwoSided; }
	RSTMAXMTL_EXPORT void SetIsTwoSided(bool twoSided) { m_IsTwoSided = twoSided; }
	RSTMAXMTL_EXPORT bool HasAmbLighting() { return m_HasAmbLighting;}
#else
	RSTMAXMTL_EXPORT bool IsPreset() const { return m_shaderValRoot->Flags()&CRstShaderValueNode::eShaderValFlagIsPreset; }
	RSTMAXMTL_EXPORT bool IsTwoSided() const { return m_shaderValRoot->Flags()&CRstShaderValueNode::eShaderValFlagIsTwoSided; }
	RSTMAXMTL_EXPORT bool HasAmbLighting() const { return m_shaderValRoot->Flags()&CRstShaderValueNode::eShaderValFlagHasAmbLighting; }
#endif
	RSTMAXMTL_EXPORT bool IsAlphaShader();
	RSTMAXMTL_EXPORT bool IsTerrainShader();
	RSTMAXMTL_EXPORT bool IsBlendShader();
	RSTMAXMTL_EXPORT bool IsTintShader();
	RSTMAXMTL_EXPORT bool IsTreeShader();
	RSTMAXMTL_EXPORT bool IsCameraOrientedShader();

	RSTMAXMTL_EXPORT void SetRefreshTextures(bool refresh) {m_RefreshTextures = refresh;}
	RSTMAXMTL_EXPORT void Redraw() { UpdateParameters(true); }

	RSTMAXMTL_EXPORT void GetVariableInfoTextureOutputFormats(s32 i, atString& r_TextureOutputFormats);
#pragma endregion

#pragma region FPI
	//FPInterfaceDesc* GetDesc(){ return NULL; }

	//Function Publishing Interface concrete implementation 
	TSTR	FpGetVariableName(int idx);//{return "None";}										// The variable's display name
	TSTR	FpGetShaderParameterName(int idx);								// The variable's name as it appears in the shader parameters
	int		FpGetVariableCount() { return GetVariableCount(); }
	TSTR	FpGetShaderName();
	TSTR	FpGetVariableType(int idx); //{ return "None";}
	TSTR	FpGetTextureVariableValue(int idx);
	float	FpGetFloatVariableValue(int idx);
	int		FpGetIntVariableValue(int idx);
	bool	FpGetBoolVariableValue(int idx);
	Point2	FpGetVector2VariableValue(int idx);
	Point3	FpGetVector3VariableValue(int idx);
	Point4	FpGetVector4VariableValue(int idx);

		// The follow functions are unsafe versions of the above functions, "unsafe" means that they do NOT actually create the grmShader
	// to get the data, instead it uses whatever data it can find stored in the scene.  This is often bad and wrong, so it is not a 
	// good idea to use these functions.  BUT in rare situations where you do not have the shaders required, it can still be useful
	// to get at the data, for example when looking at a scene from one project on another project
	TSTR	FpUnsafeGetVariableName(int idx);										// The variable's display name
	TSTR	FpUnsafeGetShaderParameterName(int idx);								// The variable's name as it appears in the shader parameters
	int		FpUnsafeGetVariableCount();
	int		FpUnsafeGetTextureVariableCount();
	int		FpUnsafeGetFloatVariableCount();
	int		FpUnsafeGetIntVariableCount();
	int		FpUnsafeGetBoolVariableCount();
	int		FpUnsafeGetVector2VariableCount();
	int		FpUnsafeGetVector3VariableCount();
	int		FpUnsafeGetVector4VariableCount();
	TSTR	FpUnsafeGetShaderName();
	TSTR	FpUnsafeGetVariableType(int idx);
	TSTR	FpUnsafeGetTextureVariableValue(int idx);
	float	FpUnsafeGetFloatVariableValue(int idx);
	int		FpUnsafeGetIntVariableValue(int idx);
	bool	FpUnsafeGetBoolVariableValue(int idx);
	Point2	FpUnsafeGetVector2VariableValue(int idx);
	Point3	FpUnsafeGetVector3VariableValue(int idx);
	Point4	FpUnsafeGetVector4VariableValue(int idx);

	int		FpGetRawTextureCount();
	int		FpGetRawTextureAlphaCount();
	bool	FpGetRawTextureHasAlpha( int idx );
	TSTR	FpGetRawTextureValue( int idx );
	TSTR	FpGetRawTextureAlphaValue( int idx );

#if !RSTMTL_HW_SHADER
	FPInterface* GetInterface(Interface_ID id)
	{
		if( id == RSM_INTERFACE )
			return (IRageShaderMaterial*)this;
		else
			return NULL;
	}
#endif

#pragma endregion

#if RSTMTL_HW_SHADER

	BaseInterface* GetInterface(Interface_ID id);
	
#pragma region ViewableRageShader 
	
	bool GetDevice();
	HRESULT CreateEffectParser();
	void Draw(INode* curNode);
	bool LoadEffect(TCHAR* fname);
	void LoadEffectTextures();
	void LoadTexmap(Texmap* texMap, int texIdx);
	void LoadEnvironmentTexmap(Texmap* texMap);
	int GetMatIndex(Mtl * m_Mtl);
	void SetTextureScaleValue(float texScale) { m_TexScaleMultiplier = texScale; }
	float GetTextureScaleValue() { return m_TexScaleMultiplier; }
#if !SHADER_ATTR_TREE
	void SetLightingMode( bool lighting ) ;
	bool GetLightingMode() { return m_HasLighting; }
	bool GetHasAmbLighting() { return m_HasAmbLighting; }
	bool UsesDiffuseMapAlpha() { return m_UseDiffuseMapAlpha;}
	void SetUseDiffuseMapAlpha( bool useIt );
	void SetIsPreset( bool preset ) { m_IsPreset = preset; }
	void SetIsHwMode( bool hwMode ) { m_IsHwMode = hwMode; }
	void SetHasAmbLighting(bool ambLight);
#else
	void SetLightingMode( bool lighting ) {
		if(lighting)
			m_shaderValRoot->Flags() |= CRstShaderValueNode::eShaderValFlagHasLighting;
		else if(m_shaderValRoot->Flags()&CRstShaderValueNode::eShaderValFlagHasLighting)
			m_shaderValRoot->Flags() ^= (s32)CRstShaderValueNode::eShaderValFlagHasLighting;
	}
	void SetIsPreset( bool preset) {
		if(preset)
			m_shaderValRoot->Flags() |= CRstShaderValueNode::eShaderValFlagIsPreset;
		else if(m_shaderValRoot->Flags()&CRstShaderValueNode::eShaderValFlagIsPreset)
			m_shaderValRoot->Flags() ^= (s32)CRstShaderValueNode::eShaderValFlagIsPreset;
	}
	bool GetLightingMode() { return m_shaderValRoot->Flags()&CRstShaderValueNode::eShaderValFlagHasLighting; }
	void SetIsTwoSided(bool twoSided)
	{ 
		if(twoSided)
			m_shaderValRoot->Flags() |= CRstShaderValueNode::eShaderValFlagIsTwoSided;
		else if(m_shaderValRoot->Flags()&CRstShaderValueNode::eShaderValFlagIsTwoSided)
			m_shaderValRoot->Flags() ^= (s32)CRstShaderValueNode::eShaderValFlagIsTwoSided;
	}
	void SetHasAmbLighting(bool ambLight)
	{ 
		if(ambLight)
			m_shaderValRoot->Flags() |= CRstShaderValueNode::eShaderValFlagHasAmbLighting;
		else if(m_shaderValRoot->Flags()&CRstShaderValueNode::eShaderValFlagHasAmbLighting)
			m_shaderValRoot->Flags() ^= (s32)CRstShaderValueNode::eShaderValFlagHasAmbLighting;
	}
	void SetIsHwMode(bool hwMode)
	{
		if(hwMode)
			m_shaderValRoot->Flags() |= CRstShaderValueNode::eShaderValFlagIsHwMode;
		else if(m_shaderValRoot->Flags()&CRstShaderValueNode::eShaderValFlagIsHwMode)
			m_shaderValRoot->Flags() ^= (s32)CRstShaderValueNode::eShaderValFlagIsHwMode;
	}

#endif
#pragma endregion

#pragma region IDX9DataBridge
	// from IDX9DataBridge
	ParamDlg * CreateEffectDlg(HWND hWnd, IMtlParams * imp) {return NULL;}
	void DisableUI(){};
	void SetDXData(IHardwareMaterial * pHWMtl, Mtl * pMtl){};
	TCHAR * GetName(){return _T("RageHardwareShader");}
	float GetDXVersion(){return 9.0f;}
#pragma endregion

#pragma region IDX9VertexShader
	// from IDX9VertexShader

	/** The meat of the interface.  This function will be executed just prior to
	rendering the given mesh/inode.
	Our responsibilities are to initialize and draw the mesh using 
	our applied effects file.
	@param mesh The result of the object evaluation on the Nodes object
	@param node The node currently being rendered
	@return S_OK on success, or the appropriate DX error message */
	HRESULT Initialize(Mesh *mesh, INode *node);
	/** See Above - The one difference is the MNMesh, which is a result
		of evaluating to a PolyObject rather than an Editable Mesh */
	HRESULT Initialize(MNMesh *mnmesh, INode *node);

	/** Called by Max to pass us a pointer to the current GW.
		@param d3dgw A pointer to the current graphics window */
	HRESULT ConfirmDevice(ID3D9GraphicsWindow *d3dgw);
	HRESULT ConfirmPixelShader(IDX9PixelShader *pps);
	bool CanTryStrips();
	int GetNumMultiPass();

	LPDIRECT3DVERTEXSHADER9 GetVertexShaderHandle(int numPass) { return 0; }
	HRESULT SetVertexShader(ID3D9GraphicsWindow *d3dgw, int numPass);

	// Draw 3D mesh as TriStrips
	bool	DrawMeshStrips(ID3D9GraphicsWindow *d3dgw, MeshData *data);

	// Draw 3D mesh as wireframe
	bool	DrawWireMesh(ID3D9GraphicsWindow *d3dgw, WireMeshData *data);

	// Draw 3D lines
	void	StartLines(ID3D9GraphicsWindow *d3dgw, WireMeshData *data);
	void	AddLine(ID3D9GraphicsWindow *d3dgw, DWORD *vert, int vis);
	bool	DrawLines(ID3D9GraphicsWindow *d3dgw);
	void	EndLines(ID3D9GraphicsWindow *d3dgw, GFX_ESCAPE_FN fn);

	// Draw 3D triangles
	void	StartTriangles(ID3D9GraphicsWindow *d3dgw, MeshFaceData *data);
	void	AddTriangle(ID3D9GraphicsWindow *d3dgw, DWORD index, int *edgeVis);
	bool	DrawTriangles(ID3D9GraphicsWindow *d3dgw);
	void	EndTriangles(ID3D9GraphicsWindow *d3dgw, GFX_ESCAPE_FN fn);

#pragma endregion

#pragma region IStdDualVSCallback
	// from IStdDualVSCallback

	ReferenceTarget *GetRefTarg();

	/** Allows the plugin to define a custom vertex shader cache
		We dont, and just return a max-defined version */
	VertexShaderCache *CreateVertexShaderCache();

	/** Callback functions called whenever the mesh we are applied to
		(which is stored in stdDualVS) is invalidated
		@param pNode the node to render
		@param pMesh The result of the node object evaluation */
	HRESULT  InitValid(Mesh* pMesh, INode *pNode);

	/** Callback functions called whenever the mnmesh we are applied to
		(which is stored in stdDualVS) is invalidated
		@param pNode the node to render
		@param pMesh The result of the node object evaluation */
	HRESULT  InitValid(MNMesh* pMNMmesh, INode *pNode);

	/** A callback on node deletion, allows us to release our render mesh cache
		@param pNode The node being deleted */
	void	 DeleteRenderMeshCache(INode * pNode);
#pragma endregion

#pragma region IMaterialViewportShading
	
	ShadingModel m_hwRendering;
	bool IsShadingModelSupported (ShadingModel model) const { return true; }
	ShadingModel  GetCurrentShadingModel () const { return m_hwRendering; }
	bool  SetCurrentShadingModel (ShadingModel model) { m_hwRendering = model; return true; };
	int  GetSupportedMapLevels () const { return 1; } // we have no submaps

#pragma endregion // IMaterialViewportShading functions

	void UpdateParameters( bool bRedraw );
	void UpdateMaterialPresetParameters( bool bRedraw );
	bool HasLighting() { return GetLightingMode(); }// GunnarD: Redundant method (GetLightingMode())?

	IMtlParams *GetMtlParams(){return mp_MtlParam;}

#endif // RSTMTL_HW_SHADER

static atArray<atArray<atString>> m_GtaRuleList;
protected:

	enum
	{
		CURRENT_FILE_VERSION = 7
	};


	enum
	{
		FLAG_NONE = 0,
		FLAG_ISPRESET = 1,
		FLAG_ISTWOSIDED = 2,
		FLAG_OVERRIDETEXSCALE = 4,
		FLAG_HASLIGHTING = 8,
		FLAG_HASAMBLIGHTING = 16,
	};

	s32 GetDataSize();
	static void WriteString(fiStream* p_Save,const atString& r_Write);
	static void ReadString(fiStream* p_Load,atString& r_Read);
	ShaderInstanceData::ShaderVar* GetVar(const atString& name);
	ShaderInstanceData::ShaderVar* GetVar(const grmVariableInfo& r_VarInfo);
	grcEffect::VarType TranslateVarType(grcEffect::VarType);

	void UpdateBorderColor(BOOL bShader);

	atArray<CRstShaderValueNode*> m_lastNodeSearchResult;
	void ClearNodeSearchResult(){m_lastNodeSearchResult.clear();}
	int RstMtlMax::FindShaderValueNode(CRstShaderValueNode* currNode, bool breakOnFirstFind=true, const char *name=SHADERVALNAME_IGNORE, s32 type=CRstShaderValueNode::eAllShaderValTypes, s32 flags=CRstShaderValueNode::eAllShaderValFlags, void *value=NULL, int refTargetIndex=-1);
	atArray<CRstShaderValueNode*> &GetLastNodeSearchResult(){return m_lastNodeSearchResult;}
public:
	CRstShaderValueNode *FindSingleShaderValueNode(CRstShaderValueNode* currNode, const char *name=SHADERVALNAME_IGNORE, s32 type=CRstShaderValueNode::eAllShaderValTypes, s32 flags=CRstShaderValueNode::eAllShaderValFlags, void *value=NULL, int refTargetIndex=-1)
	{
		ClearNodeSearchResult();
		FindShaderValueNode(currNode,true,name,type,flags,value,refTargetIndex);
		if(GetLastNodeSearchResult().GetCount()<=0)
			return NULL;
		return GetLastNodeSearchResult()[0];
	}
	ShaderInstanceData* GetShaderInstanceData() const { return m_ShaderInstanceData;}
	void RecreateInstanceData(ShaderInstanceData::eInstanceDataType type);
	atString&	GetCurrentShaderName() { return m_ShaderName; }
	void SetupUI();
protected:
	void RebuildReferences();
	void RebuildReferenceHierarchy(CRstShaderValueNode* currNode);
	bool NeedsRebuildReferences()
	{
#if SHADER_ATTR_TREE
		return m_refsNeedsRebuild;
#else	
		return false;
#endif
	}
	void SetNeedsRebuildReferences()
	{
#if SHADER_ATTR_TREE
		m_refsNeedsRebuild = true;
#endif
	}
	CRstShaderValueNode *AppendShaderValueNode(CRstShaderValueNode *newNode, CRstShaderValueNode *parentNode=NULL)
	{
#if SHADER_ATTR_TREE
		if(!parentNode)
			parentNode=m_shaderValRoot;
		parentNode->Inputs().PushAndGrow(newNode);
		return (CRstShaderValueNode *)(parentNode->Inputs()[parentNode->Inputs().GetCount()-1]);
#endif
	}

	RstMaxShaderDlg* CreateShaderParamDialog();
	void ConstructShaderParamDialog(grcEffect* p_Shader, rageRstShaderDbEntry *dbEntry);
	void ConstructShaderParamDialog(rageRstShaderDbEntry *dbEntry);
	
	RstMaxMtlPresetDlg* RstMtlMax::CreateMaterialPresetDialog();
	void ConstructMaterialPresetDialog(rageRstShaderDbEntry *dbEntry);

	void ExportHierarchy(CRstShaderValueNode* currNode, fiStream *p_MemStream);
	int GetHierarchyDataSize(CRstShaderValueNode* currNode);

	HWND m_MtlEditWnd;
	IMtlParams* mp_MtlParam;
	RstMaxMtlDlg* mp_MtlDlg;

	// old attribute system
#if !SHADER_ATTR_TREE
	RstMaxShaderDlg* mp_DlgExtra;
	RstMaxMtlPresetDlg* mp_DlgMtlPreset;
	RstMaxErrorDlg* mp_DlgError;

	bool m_IsPreset;
	bool m_IsTwoSided;
	bool m_HasLighting;
	bool m_HasAmbLighting;
	bool m_UseDiffuseMapAlpha;
	bool m_IsHwMode;

	TesselationType m_tesselationType;

	bool m_RefreshTextures;

	atString m_parserErrorMsg;
#else
	// new attribute system
	CRstShaderValueNode *m_shaderValRoot;
	atArray<CRstShaderValueNode *> m_references;
	bool m_refsNeedsRebuild;

#endif
#define NTEXHANDLES 2
	TexHandle *texHandle[NTEXHANDLES];
	short texOpsType[NTEXHANDLES];
	int numTexHandlesUsed;
	Interval texHandleValid;
	
	Interval texHandleValidArray[NTEXHANDLES];
		
	// composite of shader/mtl channel types
	int channelTypes[ STD2_NMAX_TEXMAPS ];
	int stdIDToChannel[ N_ID_CHANNELS ];

	MapUsageType	mapsUsage[NTEXHANDLES]; // indicate the usage of maps.
	short useSubForTex[NTEXHANDLES];

	bool m_OverrideTexScale;
	float m_TexScaleMultiplier;			// Scale DX textures by this multiplier
#if RSTMTL_HW_SHADER
	// DX related members
	ElementContainer m_ElemContainer;	// This container class will hold all the parameters loaded by shaders

	Interval		ivalid;				// The current validity interval of our material
	//Texmap			*tex[NSUBTEX];		// Array of textures.  For this demo, we have 1

	LPDIRECT3DDEVICE9 m_pD3DDevice;		// A pointer to the current D3D device

	IStdDualVS		*stdDualVS;			// An interface supplied by Max to process Max geometry for submission
public:
	ElementContainer GetElementContainer() { return m_ElemContainer; }

	IRenderMeshCache *m_pMeshCache;		// An interface supplied by max to cache and submit vertex caches
	IEffectParser	*m_EffectParser;		// The actual document parser.  The processes a document, loads parameters etc
	
	bool m_RefreshElements;				// If the mesh changes, this will be set in our InitValid callback
	bool m_debugParseSuccess;			// Store the fx file parse result

private:
	atString			m_ShaderName;
	ShaderInstanceData* m_ShaderInstanceData;
	
#endif	// RSTMTL_HW_SHADER
};

} // namespace rage {

#endif //__RST_MAX_MTL_WRAPPER_H__
