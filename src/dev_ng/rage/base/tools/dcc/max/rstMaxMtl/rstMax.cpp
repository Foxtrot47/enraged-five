

#include "rstMaxMtl/rstMax.h"
#include "rstMaxMtl/resource.h"
#include "atl/string.h"
#include "rstplugin.h"
#include "system/exec.h"
#include "rstMaxMtl/utility.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;
atString RstMax::m_ShaderPath;
atString RstMax::m_PresetPath;
atArray<atString> RstMax::m_PresetList;
atString RstMax::m_ShaderName;
atString RstMax::m_AlphaName;

atArray<const char*> RstMax::ValueChangedCallbacks;


//////////////////////////////////////////////////////////////////////////////////////////////////////////
class rstRstUtilityClassDesc : public ClassDesc
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new RstMax(); }
	const TCHAR *	ClassName() { return _T("Rst Utility"); }
	SClass_ID		SuperClassID() { return UTILITY_CLASS_ID; }
	Class_ID		ClassID() { return RSTUTILITY_CLASS_ID; }
	const TCHAR* 	Category() { return _T(""); }
};

static rstRstUtilityClassDesc RstUtilityClassDesc;

/////////////////////////////////////////////////////////////////////////////////////////////////
ClassDesc* RstMax::Creator()
{
	return &RstUtilityClassDesc;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RstMax::~RstMax()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void RstMax::BeginEditParams(Interface *ip,IUtil *iu)
{
	m_hWnd = ip->AddRollupPage(	g_hDllInstance, 
								MAKEINTRESOURCE(IDD_DLG_OPTIONS),
								RstMaxDlgProc,
								_T("rstMax Settings"));
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void RstMax::EndEditParams(Interface *ip,IUtil *iu)
{
	ip->DeleteRollupPage(m_hWnd);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void RstMax::DeleteThis()
{
	delete this;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void RstMax::InitPaths(const MCHAR* shaderPath, const MCHAR* presetPath, const MCHAR* defaultShaderName, const MCHAR* defaultAlphaShaderName)
{
#if MAX_RELEASE < 16000
	m_ShaderPath = atString(shaderPath);
	m_PresetPath = atString(presetPath);
	m_ShaderName = atString(defaultShaderName);
	m_AlphaName = atString(defaultAlphaShaderName);
#else
	m_ShaderPath = atString(MSTR(shaderPath).ToCStr());
	m_PresetPath = atString(MSTR(presetPath).ToCStr());
	m_ShaderName = atString(MSTR(defaultShaderName).ToCStr());
	m_AlphaName = atString(MSTR(defaultAlphaShaderName).ToCStr());
#endif
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void RstMax::InitShaderLib()
{
	grcTextureFactory::CreateStringTextureFactory(true);
	grcTextureFactory::InitClass();

	rageRstMaxShaderPlugin* p_Plugin = rageRstMaxShaderPlugin::GetInstance();
	rageRstShaderList* p_ShaderList = p_Plugin->GetShaderList();

	p_ShaderList->SetShaderPath(m_ShaderPath);
	p_ShaderList->BuildShaderList();

	grcEffect::Preload(m_ShaderPath);

	if(!p_Plugin->SetShaderDbPath(m_PresetPath))
	{
		char msgBuffer[2048];
		sprintf_s(msgBuffer, 2048, "Failed to initialize RAGE Shader Preset library from :\n%s\nAre you missing a preset preload.list file?", (const char*)m_PresetPath);

		MessageBox(GetCOREInterface()->GetMAXHWnd(), 
#if MAX_RELEASE < 16000
			msgBuffer,
#else
			MSTR::FromCStr(msgBuffer),
#endif
			_T("RstMaxMtl Error"),
			MB_ICONHAND | MB_OK);

	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////
RSTMTL_EXPORT const atString& RstMax::GetShaderPath()
{
	return m_ShaderPath;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RSTMTL_EXPORT const atString& RstMax::GetPresetPath()
{
	return m_PresetPath;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RSTMTL_EXPORT const atString& RstMax::GetDefaultShaderName()
{
	return m_ShaderName; 
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RSTMTL_EXPORT const atString& RstMax::GetDefaultAlphaShaderName() 
{ 
	return m_AlphaName; 
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RSTMTL_EXPORT grcEffect* RstMax::FindShader(const atString& ShaderName)
{
	if(ShaderName == "")
		return NULL;

	rageRstMaxShaderPlugin* p_ShaderPlugin = rageRstMaxShaderPlugin::GetInstance();

	if(!p_ShaderPlugin)
		return NULL;

	rageRstShaderList* p_ShaderList = p_ShaderPlugin->GetShaderList();

	const char* p_Ext = strrchr(ShaderName,'.');
	grcEffect* p_Shader = NULL;

	if(p_Ext && (stricmp(p_Ext,".sps") == 0) )
	{
		rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(ShaderName);

		if(!p_Entry)
			return NULL;

		p_Shader = &p_Entry->m_Preset->GetBasis();
	}
	else
	{
		p_Shader = p_ShaderList->FindShader(ShaderName);
	}

	return p_Shader;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
s32 RstMax::GetShaderCount()
{
	rageRstShaderList* p_ShaderList = rageRstMaxShaderPlugin::GetInstance()->GetShaderList();

	return p_ShaderList->GetShaderCount();
}

/////////////////////////////////////////////////////////////////////////////////////////////////
const char* RstMax::GetShaderName(s32 Index) 
{
	rageRstShaderList* p_ShaderList = rageRstMaxShaderPlugin::GetInstance()->GetShaderList();

	return p_ShaderList->GetShaderName(Index);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
s32 RstMax::GetPresetCount()
{
	if(m_PresetList.GetCount() == 0)
	{
		rageRstMaxShaderPlugin* p_Plugin = rageRstMaxShaderPlugin::GetInstance();
		rageRstShaderDb* p_DB = p_Plugin->GetShaderDb();

		s32 Count = p_DB->BuildEntryList("", true);

		m_PresetList.Reset();

		for(s32 i=0;i<Count;i++)
		{
			const char* p_Name = p_DB->GetEntryName(i);

			if((strcmp(p_Name,".") == 0) || (strcmp(p_Name,"..") == 0))
			{
				continue;
			}

			m_PresetList.PushAndGrow(atString(p_Name));
		}
	}

	return m_PresetList.GetCount();
}

/////////////////////////////////////////////////////////////////////////////////////////////////
const char* RstMax::GetPresetName(s32 Index)
{
	GetPresetCount();
	
	return m_PresetList[Index];
}

/////////////////////////////////////////////////////////////////////////////////////////////////

rageRstShaderDbEntry* RstMax::GetDbEntry(const atString& r_Name)
{
	return rageRstMaxShaderPlugin::GetInstance()->GetShaderDb()->GetItem(r_Name);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK RstMax::RstMaxDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_INITDIALOG:
		{
			HWND hText = GetDlgItem(hWnd,IDC_EDT_DIR);
#if MAX_RELEASE < 16000
			const MCHAR* p_cText = m_ShaderPath;
#else
			const MCHAR* p_cText = MSTR::FromUTF8(m_ShaderPath);
#endif
			SetWindowText(hText,p_cText);

			hText = GetDlgItem(hWnd,IDC_EDT_SHADERNAME);
			ICustEdit* p_Edit = GetICustEdit(hText);
#if MAX_RELEASE < 16000
			const MCHAR* p_cShaderName = m_ShaderName;
#else
			const MCHAR* p_cShaderName = MSTR::FromUTF8(m_ShaderName);
#endif
			p_Edit->SetText(p_cShaderName);
			ReleaseICustEdit(p_Edit);

			hText = GetDlgItem(hWnd,IDC_EDT_ALPHA_NAME);
			p_Edit = GetICustEdit(hText);
#if MAX_RELEASE < 16000
			const MCHAR* p_cAlphaName = m_AlphaName;
#else
			const MCHAR* p_cAlphaName = MSTR::FromUTF8(m_AlphaName);
#endif
			p_Edit->SetText(p_cAlphaName);
			ReleaseICustEdit(p_Edit);
		}
		return TRUE;

	case WM_CUSTEDIT_ENTER:

		{
			ICustEdit* p_Edit = GetICustEdit((HWND)lParam);
			MCHAR Buffer[1024];
			p_Edit->GetText(Buffer,1024);

			switch(wParam)
			{
			case IDC_EDT_SHADERNAME:
				m_ShaderName = Buffer;
				break;
			case IDC_EDT_ALPHA_NAME:
#if MAX_RELEASE < 16000
				m_AlphaName = Buffer;
#else
				m_AlphaName = MSTR(Buffer).ToCStr();
#endif
				break;
			}

			ReleaseICustEdit(p_Edit);
		}

		return TRUE;

	case WM_COMMAND:
		
		switch(LOWORD(wParam))
		{
		case IDC_BTN_DIR:
			{
				BROWSEINFO BrowseInfo;
				MCHAR Path[MAX_PATH];

#if MAX_RELEASE < 16000
				strcpy(Path,m_ShaderPath);
#else
				_tcscpy(Path,MSTR::FromUTF8(m_ShaderPath));
#endif

				BrowseInfo.hwndOwner = hWnd;
				BrowseInfo.pidlRoot = NULL;
				BrowseInfo.pszDisplayName = Path;
				BrowseInfo.lpszTitle = _T("Choose new shader path");
				BrowseInfo.ulFlags = 0;
				BrowseInfo.lpfn = NULL;
				BrowseInfo.lParam = NULL;
				BrowseInfo.iImage = NULL;

				LPITEMIDLIST p_ItemList = SHBrowseForFolder(&BrowseInfo);

				if(p_ItemList && SHGetPathFromIDList(p_ItemList,Path))
				{
#if MAX_RELEASE < 16000
					if((strlen(Path) > 0) && (Path[strlen(Path) - 1] != '\\'))
					{
						strcat(Path,_T("\\"));
					}

					m_ShaderPath = (atString(Path));
#else
					if((_tcslen(Path) > 0) && (Path[_tcslen(Path) - 1] != '\\'))
					{
						_tcscat(Path,_T("\\"));
					}

					RstMax::SetShaderPath(atString(MSTR(Path).ToCStr()));
#endif
					SetWindowText(GetDlgItem(hWnd,IDC_EDT_DIR),Path);
				}
			}
#if 0
		case IDC_BTN_PRESET:
			{
				BROWSEINFO BrowseInfo;
				char Path[MAX_PATH];

				strcpy(Path,m_PresetPath);

				BrowseInfo.hwndOwner = hWnd;
				BrowseInfo.pidlRoot = NULL;
				BrowseInfo.pszDisplayName = Path;
				BrowseInfo.lpszTitle = "Choose new preset path";
				BrowseInfo.ulFlags = 0;
				BrowseInfo.lpfn = NULL;
				BrowseInfo.lParam = NULL;
				BrowseInfo.iImage = NULL;

				LPITEMIDLIST p_ItemList = SHBrowseForFolder(&BrowseInfo);

				if(p_ItemList && SHGetPathFromIDList(p_ItemList,Path))
				{
					if((strlen(Path) > 0) && (Path[strlen(Path) - 1] != '\\'))
					{
						strcat(Path,"\\");
					}

					RstMax::SetPresetPath(atString(Path));
					SetWindowText(GetDlgItem(hWnd,IDC_EDT_PRESET),Path);
				}
			}
#endif
		}

		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void RstMax::NotifyValueChangedCallbacks(const char* shaderName, const char* attributeName)
{
	for( int callbackIndex = 0; callbackIndex < ValueChangedCallbacks.GetCount(); ++callbackIndex )
	{
		std::string scriptString = ValueChangedCallbacks[callbackIndex];
		FPValue returnValue;
		BOOL success = FALSE;
		if(GetCOREInterface()->GetMAXHWnd())
		{
			try
			{
				success = ExecuteMaxScript(scriptString.c_str());
			}
			catch (...)
			{
				MessageBox(GetCOREInterface()->GetMAXHWnd(), _T("Unsafe Max SDK function crash caught."), _T("Exception Caught"), NULL);
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void RstMax::AddValueChangedCallback(const char* pScript)
{
	size_t length = strlen(pScript)+1;
	char* callback = new char[length];
	strcpy_s(callback, length, pScript);

	ValueChangedCallbacks.PushAndGrow(callback);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void RstMax::RemoveValueChangedCallback(const char* pScript)
{
	std::string scriptString(pScript);
	for(int callbackIndex = 0; callbackIndex < ValueChangedCallbacks.GetCount(); ++callbackIndex)
	{
		const char* callbackScript = ValueChangedCallbacks[callbackIndex];
		if ( stricmp(callbackScript, pScript) == 0)
		{
			ValueChangedCallbacks.Delete(callbackIndex);
			callbackIndex--;
		}
	}
}

