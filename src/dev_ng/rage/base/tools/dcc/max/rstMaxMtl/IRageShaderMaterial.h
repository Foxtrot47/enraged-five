
#ifndef _IRAGESHADERMATERIAL_H_
#define _IRAGESHADERMATERIAL_H_

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
#include "iFnPub.h"

#define RSM_INTERFACE Interface_ID(0x3e957f76, 0x29d6229d)
#define GetRageShaderMtlInterface(obj) \
	((IRageShaderMaterial*)obj->GetInterface(RSM_INTERFACE))

enum { 
		rsm_FpGetShaderName, rsm_FpGetVariableCount, rsm_FpGetVariableName, rsm_FpGetVariableType,
		rsm_FpGetTextureVariableValue, rsm_FpGetFloatVariableValue, rsm_FpGetIntVariableValue,
		rsm_FpGetBoolVariableValue, rsm_FpGetVector2VariableValue, rsm_FpGetVector3VariableValue,
		rsm_FpGetVector4VariableValue,

		rsm_FpUnsafeGetShaderName, rsm_FpUnsafeGetVariableCount, rsm_FpUnsafeGetVariableName, rsm_FpUnsafeGetVariableType,
		rsm_FpUnsafeGetTextureVariableValue, rsm_FpUnsafeGetFloatVariableValue, rsm_FpUnsafeGetIntVariableValue,
		rsm_FpUnsafeGetBoolVariableValue, rsm_FpUnsafeGetVector2VariableValue, rsm_FpUnsafeGetVector3VariableValue,
		rsm_FpUnsafeGetVector4VariableValue,

		rsm_FpGetRawTextureCount, rsm_FpGetRawTextureAlphaCount,
		rsm_FpGetRawTextureHasAlpha,
		rsm_FpGetRawTextureValue, rsm_FpGetRawTextureAlphaValue,
};

#pragma warning(push)
// #pragma warning(disable:4265)
// #pragma warning(disable:4100)

class IRageShaderMaterial : public FPMixinInterface
{
	BEGIN_FUNCTION_MAP
		FN_0( rsm_FpGetShaderName, TYPE_TSTR_BV, FpGetShaderName);
		
		FN_0( rsm_FpGetVariableCount, TYPE_INT, FpGetVariableCount);
		FN_1( rsm_FpGetVariableName, TYPE_TSTR_BV, FpGetVariableName, TYPE_INT);
		FN_1( rsm_FpGetVariableType, TYPE_TSTR_BV, FpGetVariableType, TYPE_INT);

		FN_1( rsm_FpGetTextureVariableValue, TYPE_TSTR_BV, FpGetTextureVariableValue, TYPE_INT );
		FN_1( rsm_FpGetFloatVariableValue, TYPE_FLOAT, FpGetFloatVariableValue, TYPE_INT );
		FN_1( rsm_FpGetIntVariableValue, TYPE_INT, FpGetIntVariableValue, TYPE_INT );
		FN_1( rsm_FpGetBoolVariableValue, TYPE_BOOL, FpGetBoolVariableValue, TYPE_INT );

		FN_1( rsm_FpGetVector2VariableValue, TYPE_POINT2_BV, FpGetVector2VariableValue, TYPE_INT);
		FN_1( rsm_FpGetVector3VariableValue, TYPE_POINT3_BV, FpGetVector3VariableValue, TYPE_INT);
		FN_1( rsm_FpGetVector4VariableValue, TYPE_POINT4_BV, FpGetVector4VariableValue, TYPE_INT);

		FN_0( rsm_FpUnsafeGetShaderName, TYPE_TSTR_BV, FpUnsafeGetShaderName);
		
		FN_0( rsm_FpUnsafeGetVariableCount, TYPE_INT, FpUnsafeGetVariableCount);
		FN_1( rsm_FpUnsafeGetVariableName, TYPE_TSTR_BV, FpUnsafeGetVariableName, TYPE_INT);
		FN_1( rsm_FpUnsafeGetVariableType, TYPE_TSTR_BV, FpUnsafeGetVariableType, TYPE_INT);

		FN_1( rsm_FpUnsafeGetTextureVariableValue, TYPE_TSTR_BV, FpUnsafeGetTextureVariableValue, TYPE_INT );
		FN_1( rsm_FpUnsafeGetFloatVariableValue, TYPE_FLOAT, FpUnsafeGetFloatVariableValue, TYPE_INT );
		FN_1( rsm_FpUnsafeGetIntVariableValue, TYPE_INT, FpUnsafeGetIntVariableValue, TYPE_INT );
		FN_1( rsm_FpUnsafeGetBoolVariableValue, TYPE_BOOL, FpUnsafeGetBoolVariableValue, TYPE_INT );

		FN_1( rsm_FpUnsafeGetVector2VariableValue, TYPE_POINT2_BV, FpUnsafeGetVector2VariableValue, TYPE_INT);
		FN_1( rsm_FpUnsafeGetVector3VariableValue, TYPE_POINT3_BV, FpUnsafeGetVector3VariableValue, TYPE_INT);
		FN_1( rsm_FpUnsafeGetVector4VariableValue, TYPE_POINT4_BV, FpUnsafeGetVector4VariableValue, TYPE_INT);
	
		FN_0( rsm_FpGetRawTextureCount, TYPE_INT, FpGetRawTextureCount );
		FN_0( rsm_FpGetRawTextureAlphaCount, TYPE_INT, FpGetRawTextureAlphaCount );
		FN_1( rsm_FpGetRawTextureHasAlpha, TYPE_BOOL, FpGetRawTextureHasAlpha, TYPE_INT );
		FN_1( rsm_FpGetRawTextureValue, TYPE_TSTR_BV, FpGetRawTextureValue, TYPE_INT );
		FN_1( rsm_FpGetRawTextureAlphaValue, TYPE_TSTR_BV, FpGetRawTextureAlphaValue, TYPE_INT );
	END_FUNCTION_MAP

		FPInterfaceDesc* GetDesc();
	
public:
	virtual TSTR	FpGetShaderName() = 0;
	virtual int		FpGetVariableCount() = 0;
	virtual TSTR	FpGetVariableName(int idx) = 0;				// The variable's display name
	virtual TSTR	FpGetShaderParameterName(int idx) = 0;		// The variable's name as it appears in the shader parameters
	virtual TSTR	FpGetVariableType(int idx) = 0;
	virtual TSTR	FpGetTextureVariableValue(int idx) = 0;
	virtual float	FpGetFloatVariableValue(int idx) = 0;
	virtual int		FpGetIntVariableValue(int idx) = 0;
	virtual bool	FpGetBoolVariableValue(int idx) = 0;
	virtual Point2	FpGetVector2VariableValue(int idx) = 0;
	virtual Point3	FpGetVector3VariableValue(int idx) = 0;
	virtual Point4	FpGetVector4VariableValue(int idx) = 0;

	virtual TSTR	FpUnsafeGetShaderName() = 0;
	virtual int		FpUnsafeGetVariableCount() = 0;
	virtual int		FpUnsafeGetTextureVariableCount() = 0;
	virtual int		FpUnsafeGetIntVariableCount() = 0;
	virtual int		FpUnsafeGetFloatVariableCount() = 0;
	virtual int		FpUnsafeGetBoolVariableCount() = 0;
	virtual int		FpUnsafeGetVector2VariableCount() = 0;
	virtual int		FpUnsafeGetVector3VariableCount() = 0;
	virtual int		FpUnsafeGetVector4VariableCount() = 0;
	virtual TSTR	FpUnsafeGetVariableName(int idx) = 0;				// The variable's display name
	virtual TSTR	FpUnsafeGetShaderParameterName(int idx) = 0;		// The variable's name as it appears in the shader parameters
	virtual TSTR	FpUnsafeGetVariableType(int idx) = 0;
	virtual TSTR	FpUnsafeGetTextureVariableValue(int idx) = 0;
	virtual float	FpUnsafeGetFloatVariableValue(int idx) = 0;
	virtual int		FpUnsafeGetIntVariableValue(int idx) = 0;
	virtual bool	FpUnsafeGetBoolVariableValue(int idx) = 0;
	virtual Point2	FpUnsafeGetVector2VariableValue(int idx) = 0;
	virtual Point3	FpUnsafeGetVector3VariableValue(int idx) = 0;
	virtual Point4	FpUnsafeGetVector4VariableValue(int idx) = 0;

	virtual int		FpGetRawTextureCount() = 0;
	virtual int		FpGetRawTextureAlphaCount() = 0;
	virtual bool	FpGetRawTextureHasAlpha( int idx ) = 0;
	virtual TSTR	FpGetRawTextureValue( int idx ) = 0;
	virtual TSTR	FpGetRawTextureAlphaValue( int idx ) = 0;
};

#pragma warning(pop)

#endif //_IRAGESHADERMATERIAL_H_

