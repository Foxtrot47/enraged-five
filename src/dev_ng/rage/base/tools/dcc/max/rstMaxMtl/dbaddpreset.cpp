#include "rstMaxMtl/dbaddpreset.h"
#include "rstMaxMtl/dialog.h"
#include "rstMaxMtl/rstMax.h"
#include "rstMaxMtl/maxeditdlg.h"
#include "grcore/texture.h"
#include "rstplugin.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

////////////////////////////////////////////////////////////////////////////////
DbAddPreset::DbAddPreset():
	m_Wnd(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////
bool DbAddPreset::DoModal(HWND ParentWnd,atString& r_Path,atString& r_NewPresetName, const char *pShaderName, const char *pComment)
{
	m_Path = r_Path;
	m_ShaderName = pShaderName;

	DialogRes DialogCreator(false);

	//fill in the dialog

	s32 CurrentY = 10;
	s32 DialogWidth = 200;
	s32 HalfDialogWidth = DialogWidth >> 1;
	s32 ID = 0;

	DialogCreator.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,atString("Name:"),MaxEditDialog::g_CtrlBorder,CurrentY,HalfDialogWidth - MaxEditDialog::g_CtrlBorder - MaxEditDialog::g_CtrlGapX,MaxEditDialog::g_StaticHeight,ID++);
	m_EdtName = ID++;
	DialogCreator.AddControl(DialogRes::CTRL_EDIT,WS_VISIBLE|WS_CHILD|SS_LEFT,r_NewPresetName,HalfDialogWidth,CurrentY,HalfDialogWidth - MaxEditDialog::g_CtrlBorder,MaxEditDialog::g_StaticHeight,m_EdtName);
	CurrentY += MaxEditDialog::g_StaticHeight + MaxEditDialog::g_CtrlGapY;

	DialogCreator.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,atString("Shader:"),MaxEditDialog::g_CtrlBorder,CurrentY,HalfDialogWidth - MaxEditDialog::g_CtrlBorder - MaxEditDialog::g_CtrlGapX,MaxEditDialog::g_StaticHeight,ID++);
	m_LstShaders = ID++;
	atString ShaderName;
	if(pShaderName)
		ShaderName = pShaderName;
	DialogCreator.AddControl(DialogRes::CTRL_COMBOBOX,WS_VISIBLE|WS_VSCROLL|WS_CHILD|CBS_HASSTRINGS|CBS_DROPDOWNLIST,ShaderName,HalfDialogWidth,CurrentY,HalfDialogWidth - MaxEditDialog::g_CtrlBorder,MaxEditDialog::g_StaticHeight,m_LstShaders);
	CurrentY += MaxEditDialog::g_StaticHeight + MaxEditDialog::g_CtrlGapY;

	DialogCreator.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,atString("Comment:"),MaxEditDialog::g_CtrlBorder,CurrentY,HalfDialogWidth - MaxEditDialog::g_CtrlBorder - MaxEditDialog::g_CtrlGapX,MaxEditDialog::g_StaticHeight,ID++);
	m_EdtComment = ID++;
	DialogCreator.AddControl(DialogRes::CTRL_EDIT,WS_VISIBLE|WS_CHILD|SS_LEFT,atString(pComment),HalfDialogWidth,CurrentY,HalfDialogWidth - MaxEditDialog::g_CtrlBorder,MaxEditDialog::g_StaticHeight,m_EdtComment);
	CurrentY += MaxEditDialog::g_StaticHeight + MaxEditDialog::g_CtrlGapY;

	CurrentY += 10;
	m_BtnOk = ID++;
	DialogCreator.AddControl(DialogRes::CTRL_BUTTON,WS_VISIBLE|WS_CHILD,atString("OK"),HalfDialogWidth - MaxEditDialog::g_ButtonWidth - MaxEditDialog::g_CtrlGapX,CurrentY,MaxEditDialog::g_ButtonWidth,MaxEditDialog::g_ButtonHeight,m_BtnOk);
	m_BtnCancel = ID++;
	DialogCreator.AddControl(DialogRes::CTRL_BUTTON,WS_VISIBLE|WS_CHILD,atString("Cancel"),HalfDialogWidth + MaxEditDialog::g_CtrlGapX,CurrentY,MaxEditDialog::g_ButtonWidth,MaxEditDialog::g_ButtonHeight,m_BtnCancel);

	if(pShaderName)
		DialogCreator.SetTitle("Edit Preset Type");
	else
		DialogCreator.SetTitle("New Preset");
	DialogCreator.SetSize(DialogWidth,CurrentY + 20);

	//create and show it

	DLGTEMPLATE* p_DlgTemplate = DialogCreator.GetTemplate();

	if(DialogBoxIndirectParam(	g_hDllInstance,
								p_DlgTemplate,
								ParentWnd,
								DbAddPresetDlgProc,
								(LPARAM)this) == 0)
	{
		return false;
	}

	r_NewPresetName = m_PresetName;

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void DbAddPreset::OnInitDialog(HWND Wnd)
{
	m_Wnd = Wnd;

	HWND LstShadersWnd = GetDlgItem(Wnd,m_LstShaders);
	COMBOBOXINFO Info;
	RECT Size;

	Info.cbSize = sizeof(COMBOBOXINFO);

	GetComboBoxInfo(LstShadersWnd,&Info);
	GetClientRect(Info.hwndList,&Size);
	SetWindowPos(Info.hwndList,HWND_TOP,0,0,Size.right,200,SWP_NOMOVE|SWP_NOOWNERZORDER);
	SetFocus(GetDlgItem(Wnd, 0));

	s32 i,ShaderCount = RstMax::GetShaderCount(), preSelection=0;
	const char* p_ShaderName;
	atString preSelectShaderWithExt;
	if(m_ShaderName)
	{
		preSelectShaderWithExt = m_ShaderName;
		preSelectShaderWithExt += ".fx";
	}

	for(i=0;i<ShaderCount;i++)
	{
		p_ShaderName = RstMax::GetShaderName(i);
		if(m_ShaderName && preSelectShaderWithExt==p_ShaderName)
			preSelection = i;
		SendMessage(LstShadersWnd,CB_ADDSTRING,0,(LPARAM)p_ShaderName);
	}
	SendMessage(LstShadersWnd, CB_SETCURSEL, preSelection, NULL);
}

////////////////////////////////////////////////////////////////////////////////
void DbAddPreset::OnCommand(s32 ID)
{
	if(ID == m_BtnOk)
	{
		//check that the data is ok

		//do we have a shader name?

		char PresetName[NAME_LENGTH];
		GetWindowText(GetDlgItem(m_Wnd,m_EdtName),PresetName,NAME_LENGTH);

		if(PresetName[0] == '\0')
		{
			MessageBox(m_Wnd,"Please enter a preset name","Error",MB_OK);
			return;
		}

		//check that the preset doesnt already exist

		//create our new shader
		atString FullPath = m_Path;
		if(FullPath.length()!=0)
			FullPath += "/";

		m_PresetName = PresetName;
		m_PresetName += ".sps";

		FullPath += m_PresetName;

		rageRstShaderDb* p_ShaderDb = rageRstMaxShaderPlugin::GetInstance()->GetShaderDb();
		rageRstShaderDbEntry* p_Entry = p_ShaderDb->GetItem(FullPath);

		if(p_Entry)
		{
			int overridePreset = MessageBox(m_Wnd, "This preset already exists. Override?", "Override?", MB_YESNO);
			if(overridePreset!=IDYES)
				return;
		}

		//have we selected a source shader
		HWND ListWnd = GetDlgItem(m_Wnd,m_LstShaders);
		s32 ShaderIdx = (s32)SendMessage(ListWnd,CB_GETCURSEL,0,0);

		if(ShaderIdx == CB_ERR)
		{
			MessageBox(m_Wnd,"Please select a source shader","Error",MB_OK);
			return;
		}

		//we got here so the data must be ok
		char ShaderName[NAME_LENGTH];
		if(SendMessage(ListWnd,CB_GETLBTEXTLEN,(WPARAM)ShaderIdx,(LPARAM)0) >= NAME_LENGTH)
		{
			MessageBox(m_Wnd,"Shader name too long","Error",MB_OK);
			return;
		}

		SendMessage(ListWnd,CB_GETLBTEXT,(WPARAM)ShaderIdx,(LPARAM)ShaderName);

		//only seems to currently work for shadert shaders...

		char* p_Index;
		strlwr(ShaderName);
		p_Index = strstr(ShaderName,".fx");
		if(p_Index != NULL)
		{
			*p_Index = '\0';
		}

		int saveToPeloadFile = IDNO;
		if(!p_Entry)
			saveToPeloadFile = MessageBox(m_Wnd, "Do you want to try to save the preset to the preload.list file?", "Save?", MB_YESNO);

		char Comment[NAME_LENGTH];
		GetWindowText(GetDlgItem(m_Wnd,m_EdtComment),Comment,NAME_LENGTH);

		bool saveSuccess = p_ShaderDb->CreateEntry(FullPath,ShaderName,	saveToPeloadFile==IDYES, Comment);
		if(!saveSuccess)
		{
			MessageBox(m_Wnd,"Preset failed to save. Check folder permissions.","Error",MB_ICONWARNING);
			return;
		}

		EndDialog(m_Wnd,1);
	}
	else if(ID == m_BtnCancel)
	{
		//close the dialog without saving the data back to the preset

		EndDialog(m_Wnd,0);
	}
}

////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK DbAddPreset::DbAddPresetDlgProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam) 
{
	DbAddPreset* p_AddPreset = (DbAddPreset*)GetWindowLongPtr(DlgWnd,DWLP_USER);

	switch(Msg)
	{
	case WM_INITDIALOG:

		SetWindowLongPtr(DlgWnd,DWLP_USER,LParam);
		p_AddPreset = (DbAddPreset*)LParam;
		p_AddPreset->OnInitDialog(DlgWnd);
		return TRUE;

	case WM_COMMAND:

		if(p_AddPreset)
		{
			p_AddPreset->OnCommand(LOWORD(WParam));
		}

		return TRUE;

	case WM_CLOSE:

		EndDialog(DlgWnd,1);
		return TRUE;
	}

	return FALSE;
}
