#include "rstMaxMtl/dialogShader.h"
#include "rstMaxMtl/mtl.h"
#include "rstMaxMtl/utility.h"
#include "rstMaxMtl/dialogMtl.h"

using namespace rage;

#if SHADER_ATTR_TREE
u32 RstMaxShaderDlg::m_TexButtonID = 1100;
#endif
extern HINSTANCE g_hDllInstance;

rage::RstMaxShaderDlg::Vector4Wdgt rage::RstMaxShaderDlg::dummyVector4Wdgt;
rage::RstMaxShaderDlg::Vector3Wdgt rage::RstMaxShaderDlg::dummyVector3Wdgt;
rage::RstMaxShaderDlg::Vector2Wdgt rage::RstMaxShaderDlg::dummyVector2Wdgt;
rage::RstMaxShaderDlg::TextureWdgt rage::RstMaxShaderDlg::dummyTextureWdgt;
rage::RstMaxShaderDlg::FloatSpinnerWdgt rage::RstMaxShaderDlg::dummyFloatWdgt;


//////////////////////////////////////////////////////////////////////////////////////////////////////////
RstMaxShaderDlg::RstMaxShaderDlg(RstMtlMax* p_Mtl, IMtlParams* p_MtlParams):
	mp_Mtl(p_Mtl),
	m_Wnd(NULL),
	mp_MtlParams(p_MtlParams),
	m_ChangeFunctor(NULL),
	m_Initialising(false)
{
	m_DadMgr.Init(this);
	Reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RstMaxShaderDlg::~RstMaxShaderDlg()
{
	Reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ReferenceTarget* RstMaxShaderDlg::GetThing() 
{
	return mp_Mtl; 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxShaderDlg::DeleteThis() 
{
#if !SHADER_ATTR_TREE
	mp_Mtl->mp_DlgExtra = NULL; 
#else
	mp_Mtl->m_shaderValRoot->Dialog() = NULL; 
#endif
//	mp_Mtl->mp_MtlDlg = NULL;
	delete this; 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxShaderDlg::Reset()
{
	if(m_Wnd)
	{
		mp_MtlParams->DeleteRollupPage(m_Wnd);
		DestroyWindow(m_Wnd);
	}

	m_DialogRes.Reset();
	m_FloatSpinners.Reset();
	m_IntSpinners.Reset();
	m_Vector2s.Reset();
	m_Vector3s.Reset();
	m_Vector4s.Reset();
	m_Textures.Reset();

	m_LabelWidth = 60;
	m_Width = 218;	//this is a fixed size for a material rollup in max (left in this variable till Im
					//sure this is correct
	m_ControlHeight = 10;
	m_BorderWidth = 5;
	m_ControlGap = 3;
	m_Height = m_BorderWidth;
	m_ID = 1000;
	m_Wnd = NULL;
	m_ChangeFunctor = NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RstMaxShaderDlg::FindSubTexFromHWND(HWND hwnd)
{
	s32 i,Count = m_Textures.GetCount();

	for(i=0;i<Count;i++)
	{
		if(hwnd == GetDlgItem(m_Wnd,m_Textures[i].ButtonID))
		{
			return i;
		}

		if(hwnd == GetDlgItem(m_Wnd,m_Textures[i].ButtonAlphaID))
		{
			return i + Count;
		}
	}

	return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxShaderDlg::AddStatic(const atString& r_Name)
{
	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,(m_Width - (m_BorderWidth << 1)) >> 1,m_ControlHeight,m_ID++);

	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxShaderDlg::AddStaticString(const atString& r_Name,const atString& r_String, grcEffect::VarType ctrlSubst)
{
	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID++);
	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD,r_String,m_LabelWidth + m_BorderWidth + m_ControlGap,m_Height,m_Width - (m_LabelWidth + m_BorderWidth + 2 * m_ControlGap),m_ControlHeight,m_ID++);

	switch(ctrlSubst)
	{
	case grcEffect::VT_FLOAT:
		m_FloatSpinners.PushAndGrow(dummyFloatWdgt);
		break;
	case grcEffect::VT_TEXTURE:
		m_Textures.PushAndGrow(dummyTextureWdgt);
		break;
	case grcEffect::VT_VECTOR2:
		m_Vector2s.PushAndGrow(dummyVector2Wdgt);
		break;
	case grcEffect::VT_VECTOR3:
		m_Vector3s.PushAndGrow(dummyVector3Wdgt);
		break;
	case grcEffect::VT_VECTOR4:
		m_Vector4s.PushAndGrow(dummyVector4Wdgt);
		break;
	}

	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxShaderDlg::AddStaticString(const atString& r_Name,const atString& r_String, TypeBase::eVariableType ctrlSubst)
{
	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID++);
	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD,r_String,m_LabelWidth + m_BorderWidth + m_ControlGap,m_Height,m_Width - (m_LabelWidth + m_BorderWidth + 2 * m_ControlGap),m_ControlHeight,m_ID++);

	switch(ctrlSubst)
	{
	case TypeBase::kFloat:
		m_FloatSpinners.PushAndGrow(dummyFloatWdgt);
		break;
	case TypeBase::kTextureMap:
		m_Textures.PushAndGrow(dummyTextureWdgt);
		break;
	case TypeBase::kVector2:
		m_Vector2s.PushAndGrow(dummyVector2Wdgt);
		break;
	case TypeBase::kVector3:
		m_Vector3s.PushAndGrow(dummyVector3Wdgt);
		break;
	case TypeBase::kVector4:
		m_Vector4s.PushAndGrow(dummyVector4Wdgt);
		break;
	}

	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
#if SHADER_ATTR_TREE
void RstMaxShaderDlg::AddFloatSpinner(const atString& r_Name,CRstShaderValueNode *pVal,float Min,float Max,float Step)
#else
void RstMaxShaderDlg::AddFloatSpinner(const atString& r_Name,s32 idx,float Min,float Max,float Step)
#endif
{
	s32 m_SpinnerSize = 20;

	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,m_LabelWidth + m_BorderWidth + m_ControlGap,m_Height,m_Width - (m_LabelWidth + m_BorderWidth + 2 * m_ControlGap + m_SpinnerSize),m_ControlHeight,m_ID + 1);
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,m_Width - m_SpinnerSize - m_BorderWidth,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 2);

	FloatSpinnerWdgt FloatSpinner;

#if SHADER_ATTR_TREE
	FloatSpinner.m_pVal = pVal;
#else
	FloatSpinner.Idx = idx;
#endif
	FloatSpinner.EditID = m_ID + 1;
	FloatSpinner.SpinnerID = m_ID + 2;
	FloatSpinner.Min = Min;
	FloatSpinner.Max = Max;
	FloatSpinner.Step = Step;

	m_FloatSpinners.PushAndGrow(FloatSpinner);

	m_ID += 3;
	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
#if SHADER_ATTR_TREE
void RstMaxShaderDlg::AddIntSpinner(const atString& r_Name,CRstShaderValueNode *pVal,s32 Min,s32 Max,s32 Step)
#else
void RstMaxShaderDlg::AddIntSpinner(const atString& r_Name,s32 idx,s32 Min,s32 Max,s32 Step)
#endif
{
	s32 m_SpinnerSize = 20;

	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,m_LabelWidth + m_BorderWidth + m_ControlGap,m_Height,m_Width - (m_LabelWidth + m_BorderWidth + 2 * m_ControlGap + m_SpinnerSize),m_ControlHeight,m_ID + 1);
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,m_Width - m_SpinnerSize - m_BorderWidth,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 2);

	IntSpinnerWdgt IntSpinner;

#if SHADER_ATTR_TREE
	IntSpinner.m_pVal = pVal;
#else
	IntSpinner.Idx = idx;
#endif
	IntSpinner.EditID = m_ID + 1;
	IntSpinner.SpinnerID = m_ID + 2;
	IntSpinner.Min = Min;
	IntSpinner.Max = Max;
	IntSpinner.Step = Step;

	m_IntSpinners.PushAndGrow(IntSpinner);

	m_ID += 3;
	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
#if SHADER_ATTR_TREE
void RstMaxShaderDlg::AddMapSelect(const atString& r_Name,CRstShaderValueNode *pVal,const atString& r_MapName, s32 texRefNum)
#else
void RstMaxShaderDlg::AddMapSelect(const atString& r_Name,s32 idx,const atString& r_MapName, atString& r_TemplateName)
#endif
{
	s32 Pos1 = m_BorderWidth + m_LabelWidth + m_ControlGap;
	s32 Pos2 = ((m_Width - ((m_BorderWidth << 1) + m_LabelWidth + m_ControlGap))) / 2;
	s32 Pos3 = Pos1 + Pos2 + (m_BorderWidth << 1) + m_ControlGap;
	s32 Pos4 = Pos2;
	s32 width2 = 15;
	TextureWdgt Texture;

	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);
s32 texButtId = m_ID+1;
#if SHADER_ATTR_TREE
	texButtId = m_TexButtonID + texRefNum;
#endif
	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,r_MapName,Pos1,m_Height,Pos2,m_ControlHeight,texButtId);
	s32 templateLabelId = m_ID+2;
	m_DialogRes.AddControl(atString("CustButton"),WS_CHILD,atString(""),Pos3,m_Height,Pos4,m_ControlHeight,templateLabelId);
	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_TemplateName,Pos3,m_Height,m_LabelWidth,m_ControlHeight,templateLabelId);

#if SHADER_ATTR_TREE
//	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,">",Pos3,m_Height,width2,m_ControlHeight,m_ID + 2);

//	Texture.ButtonSubAttrDialogID = m_ID + 2;
	Texture.m_pVal = pVal;
#else
	Texture.Idx = idx;
	m_ID += 3;
#endif

	Texture.ButtonID = texButtId;
	Texture.ButtonAlphaID = -1;

	m_Textures.PushAndGrow(Texture);

	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
#if SHADER_ATTR_TREE
void RstMaxShaderDlg::AddMapSelect(const atString& r_Name,CRstShaderValueNode *pVal,const atString& r_MapName,const atString& r_AlphaName, s32 texRefNum1, s32 texRefNum2)
#else
void RstMaxShaderDlg::AddMapSelect(const atString& r_Name,s32 idx,const atString& r_MapName,const atString& r_AlphaName, atString& r_TemplateName)
#endif
{
	s32 Pos1 = m_BorderWidth + m_LabelWidth + m_ControlGap;
	s32 Pos2 = ((m_Width - ((m_BorderWidth << 1) + m_LabelWidth + m_ControlGap))) / 2;
	s32 Pos3 = Pos1 + Pos2;
	s32 Pos4 = Pos2;

	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);
	s32 texButtId1 = m_ID+1;
	s32 texButtId2 = m_ID+2;
#if SHADER_ATTR_TREE
	texButtId1 = m_TexButtonID + texRefNum1;
	texButtId2 = m_TexButtonID + texRefNum2;
#endif
	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,r_MapName,Pos1,m_Height,Pos2,m_ControlHeight,texButtId1);
	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,r_AlphaName,Pos3,m_Height,Pos4,m_ControlHeight,texButtId2);

	TextureWdgt Texture;

#if SHADER_ATTR_TREE
	Texture.m_pVal = pVal;
#else
	Texture.Idx = idx;
	m_ID += 3;
#endif
	Texture.ButtonID = texButtId1;
	Texture.ButtonAlphaID = texButtId2;

	m_Textures.PushAndGrow(Texture);

	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
#if SHADER_ATTR_TREE
void RstMaxShaderDlg::AddVector2Select(const atString& r_Name,CRstShaderValueNode *pVal,float Min,float Max,float Step)
#else
void RstMaxShaderDlg::AddVector2Select(const atString& r_Name,s32 idx,float Min,float Max,float Step)
#endif
{
	s32 m_SpinnerSize = 10;

	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);

	s32 Width = (m_Width - (m_LabelWidth + m_BorderWidth + m_ControlGap)) / 5;
	s32 ControlWidth = Width - (m_SpinnerSize + m_ControlGap);
	s32 Xpos = m_LabelWidth + m_BorderWidth + m_ControlGap;

	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 1);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 2);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 3);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 4);
		
	Vector2Wdgt Vec2;

#if SHADER_ATTR_TREE
	Vec2.m_pVal = pVal;
#else
	Vec2.Idx = idx;
#endif

	Vec2.Float[0].EditID = m_ID + 1;
	Vec2.Float[0].SpinnerID = m_ID + 2;
	Vec2.Float[0].Min = Min;
	Vec2.Float[0].Max = Max;
	Vec2.Float[0].Step = Step;
	Vec2.Float[1].EditID = m_ID + 3;
	Vec2.Float[1].SpinnerID = m_ID + 4;
	Vec2.Float[1].Min = Min;
	Vec2.Float[1].Max = Max;
	Vec2.Float[1].Step = Step;
	
	m_Vector2s.PushAndGrow(Vec2);

	m_ID += 5;
	m_Height += m_ControlHeight + m_ControlGap;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
#if SHADER_ATTR_TREE
void RstMaxShaderDlg::AddVector3Select(const atString& r_Name,CRstShaderValueNode *pVal,float Min,float Max,float Step)
#else
void RstMaxShaderDlg::AddVector3Select(const atString& r_Name,s32 idx,float Min,float Max,float Step)
#endif
{
	s32 m_SpinnerSize = 10;

	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);

	s32 Width = (m_Width - (m_LabelWidth + m_BorderWidth + m_ControlGap)) / 5;
	s32 ControlWidth = Width - (m_SpinnerSize + m_ControlGap);
	s32 Xpos = m_LabelWidth + m_BorderWidth + m_ControlGap;

	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 1);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 2);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 3);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 4);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 5);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 6);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,atString("..."),Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 7);

	Vector3Wdgt Vec3;

#if SHADER_ATTR_TREE
	Vec3.m_pVal = pVal;
#else
	Vec3.Idx = idx;
#endif

	Vec3.Float[0].EditID = m_ID + 1;
	Vec3.Float[0].SpinnerID = m_ID + 2;
	Vec3.Float[0].Min = Min;
	Vec3.Float[0].Max = Max;
	Vec3.Float[0].Step = Step;
	Vec3.Float[1].EditID = m_ID + 3;
	Vec3.Float[1].SpinnerID = m_ID + 4;
	Vec3.Float[1].Min = Min;
	Vec3.Float[1].Max = Max;
	Vec3.Float[1].Step = Step;
	Vec3.Float[2].EditID = m_ID + 5;
	Vec3.Float[2].SpinnerID = m_ID + 6;
	Vec3.Float[2].Min = Min;
	Vec3.Float[2].Max = Max;
	Vec3.Float[2].Step = Step;
	Vec3.ButtonID = m_ID + 7;

	m_Vector3s.PushAndGrow(Vec3);

	m_ID += 8;
	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
#if SHADER_ATTR_TREE
void RstMaxShaderDlg::AddVector4Select(const atString& r_Name,CRstShaderValueNode *pVal,float Min,float Max,float Step)
#else
void RstMaxShaderDlg::AddVector4Select(const atString& r_Name,s32 idx,float Min,float Max,float Step)
#endif
{
	s32 m_SpinnerSize = 5;
	s32 m_ButtonSize = 10;		// For colour picker

	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);

	s32 Width = (m_Width - (m_LabelWidth + m_BorderWidth + m_ControlGap + m_ButtonSize)) / 4;	// Leave enough room for button
	s32 ControlWidth = Width - (m_SpinnerSize + m_ControlGap);
	s32 Xpos = m_LabelWidth + m_BorderWidth + m_ControlGap;

	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 1);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 2);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 3);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 4);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 5);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 6);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,ControlWidth,m_ControlHeight,m_ID + 7);
	Xpos+=ControlWidth;
	m_DialogRes.AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,r_Name,Xpos,m_Height,m_SpinnerSize,m_ControlHeight,m_ID + 8);
	Xpos+=m_SpinnerSize + m_ControlGap;
	m_DialogRes.AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD,atString("..."),Xpos,m_Height,m_ButtonSize,m_ControlHeight,m_ID + 9);

	Vector4Wdgt Vec4;

#if SHADER_ATTR_TREE
	Vec4.m_pVal = pVal;
#else
	Vec4.Idx = idx;
#endif

	Vec4.Float[0].EditID = m_ID + 1;
	Vec4.Float[0].SpinnerID = m_ID + 2;
	Vec4.Float[0].Min = Min;
	Vec4.Float[0].Max = Max;
	Vec4.Float[0].Step = Step;
	Vec4.Float[1].EditID = m_ID + 3;
	Vec4.Float[1].SpinnerID = m_ID + 4;
	Vec4.Float[1].Min = Min;
	Vec4.Float[1].Max = Max;
	Vec4.Float[1].Step = Step;
	Vec4.Float[2].EditID = m_ID + 5;
	Vec4.Float[2].SpinnerID = m_ID + 6;
	Vec4.Float[2].Min = Min;
	Vec4.Float[2].Max = Max;
	Vec4.Float[2].Step = Step;
	Vec4.Float[3].EditID = m_ID + 7;
	Vec4.Float[3].SpinnerID = m_ID + 8;
	Vec4.Float[3].Min = Min;
	Vec4.Float[3].Max = Max;
	Vec4.Float[3].Step = Step;
	Vec4.ButtonID = m_ID + 9;

	m_Vector4s.PushAndGrow(Vec4);

	m_ID += 10;
	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#if SHADER_ATTR_TREE
void RstMaxShaderDlg::AddBoolSelect(const atString& r_Name,CRstShaderValueNode *pVal)
#else
void RstMaxShaderDlg::AddBoolSelect(const atString& r_Name,s32 idx)
#endif
{
	s32 m_ButtonSize = 20;

	m_DialogRes.AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,m_BorderWidth,m_Height,m_LabelWidth,m_ControlHeight,m_ID);

	s32 Width = (m_Width - (m_LabelWidth + m_BorderWidth + m_ControlGap)) / 5;
	s32 ControlWidth = Width - (m_ButtonSize + m_ControlGap);
	s32 Xpos = m_LabelWidth + m_BorderWidth + m_ControlGap;

	m_DialogRes.AddControl(atString("BUTTON"),WS_VISIBLE|WS_CHILD|BS_CHECKBOX,atString(""),Xpos,m_Height,m_ButtonSize,m_ControlHeight,m_ID + 1);

	BoolWdgt BoolButton;

#if SHADER_ATTR_TREE
	BoolButton.m_pVal = pVal;
#else
	BoolButton.Idx = idx;
#endif
	BoolButton.ButtonID = m_ID + 1;

	m_Bools.PushAndGrow(BoolButton);

	m_ID += 2;
	m_Height += m_ControlHeight + m_ControlGap;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
HWND RstMaxShaderDlg::Create(TSTR title)
{
	assert(!m_Wnd);

	m_DialogRes.SetSize(m_Width,m_Height);

	m_Wnd = mp_MtlParams->AddRollupPage(g_hDllInstance,
										m_DialogRes.GetTemplate(),
										RstMaxMtlDlgProc,
										title,
										(LPARAM)this);

	return m_Wnd;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK RstMaxShaderDlg::RstMaxMtlDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	RstMaxShaderDlg* p_ParamDlg = (RstMaxShaderDlg*)GetWindowLongPtr(DlgWnd,GWLP_USERDATA);

	switch(msg)
	{
	case WM_INITDIALOG:
		((RstMaxShaderDlg*)lParam)->OnInitDialog(DlgWnd);
		return TRUE;
	case CC_SPINNER_CHANGE:
		p_ParamDlg->OnSpinnerChange(LOWORD(wParam));
		return FALSE;
	case WM_COMMAND:
		p_ParamDlg->OnButtonPressed(LOWORD(wParam));
		return FALSE;		
	case WM_DESTROY:
		p_ParamDlg->OnDestroy();
		return FALSE;
	case TTN_SHOW:
		return FALSE;
	};

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxShaderDlg::UpdateMapName(s32 Index,const atString& r_MapName)
{
	char Buffer[8192];
	strcpy(Buffer,r_MapName);
#if SHADER_ATTR_TREE
	for(atArray<TextureWdgt>::iterator iTex = m_Textures.begin();iTex<m_Textures.end();iTex++)
	{
		if((*iTex).ButtonID==(m_TexButtonID + Index))
		{
			(*iTex).p_ButCtrl->SetText(Buffer);
		}
	}
#else
	s32 Count = m_Textures.GetCount();

	if(	Index < 0 ||
		Index >= (Count << 1))
	{
		return;
	}

	if(Index < Count)
	{
		if(m_Textures[Index].Idx == -1)
			return;
		m_Textures[Index].p_ButCtrl->SetText(Buffer);
	}
	else
	{
		if(m_Textures[Index - Count].Idx == -1 || NULL==m_Textures[Index - Count].p_ButAlphaCtrl)
			return;
		m_Textures[Index - Count].p_ButAlphaCtrl->SetText(Buffer);
	}
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxShaderDlg::OnInitDialog(HWND DlgWnd)
{
	m_Initialising = true;
	m_Wnd = DlgWnd;
	SetWindowLongPtr(DlgWnd,GWLP_USERDATA,(LONG_PTR)this);

	ShaderInstanceData* pShaderInstanceData = mp_Mtl->GetShaderInstanceData();

	// set up the spinner ui information

	s32 i,j,Count;

	Count = m_FloatSpinners.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_FloatSpinners[i].Idx==-1)
			continue;
		m_FloatSpinners[i].p_SpinCtrl = GetISpinner(GetDlgItem(DlgWnd,m_FloatSpinners[i].SpinnerID));
		m_FloatSpinners[i].p_EditCtrl = GetICustEdit(GetDlgItem(DlgWnd,m_FloatSpinners[i].EditID));

		m_FloatSpinners[i].p_SpinCtrl->SetScale(m_FloatSpinners[i].Step);
		m_FloatSpinners[i].p_SpinCtrl->SetLimits(m_FloatSpinners[i].Min,m_FloatSpinners[i].Max);
#if SHADER_ATTR_TREE
		m_FloatSpinners[i].p_SpinCtrl->SetResetValue(m_FloatSpinners[i].m_pVal->Value().m_Float);
		m_FloatSpinners[i].p_SpinCtrl->SetValue(m_FloatSpinners[i].m_pVal->Value().m_Float,FALSE);
#else
		m_FloatSpinners[i].p_SpinCtrl->SetResetValue(pShaderInstanceData->GetFloat(m_FloatSpinners[i].Idx));
		m_FloatSpinners[i].p_SpinCtrl->SetValue(pShaderInstanceData->GetFloat(m_FloatSpinners[i].Idx),FALSE);
#endif
		m_FloatSpinners[i].p_SpinCtrl->LinkToEdit(GetDlgItem(DlgWnd,m_FloatSpinners[i].EditID),EDITTYPE_FLOAT);
	}

	Count = m_IntSpinners.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_IntSpinners[i].Idx==-1)
			continue;
		m_IntSpinners[i].p_SpinCtrl = GetISpinner(GetDlgItem(DlgWnd,m_IntSpinners[i].SpinnerID));
		m_IntSpinners[i].p_EditCtrl = GetICustEdit(GetDlgItem(DlgWnd,m_IntSpinners[i].EditID));

		m_IntSpinners[i].p_SpinCtrl->SetScale((float)m_IntSpinners[i].Step);
		m_IntSpinners[i].p_SpinCtrl->SetLimits(m_IntSpinners[i].Min,m_IntSpinners[i].Max);
#if SHADER_ATTR_TREE
		m_IntSpinners[i].p_SpinCtrl->SetResetValue(m_IntSpinners[i].m_pVal->Value().m_Int);
		m_IntSpinners[i].p_SpinCtrl->SetValue(m_IntSpinners[i].m_pVal->Value().m_Int,FALSE);
#else
		m_IntSpinners[i].p_SpinCtrl->SetResetValue(pShaderInstanceData->GetInt(m_IntSpinners[i].Idx));
		m_IntSpinners[i].p_SpinCtrl->SetValue(pShaderInstanceData->GetInt(m_IntSpinners[i].Idx),FALSE);
#endif
		m_IntSpinners[i].p_SpinCtrl->LinkToEdit(GetDlgItem(DlgWnd,m_IntSpinners[i].EditID),EDITTYPE_INT);
	}

	Count = m_Vector2s.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_Vector2s[i].Idx==-1)
			continue;
		for(j=0;j<2;j++)
		{
			m_Vector2s[i].Float[j].p_SpinCtrl = GetISpinner(GetDlgItem(DlgWnd,m_Vector2s[i].Float[j].SpinnerID));
			m_Vector2s[i].Float[j].p_EditCtrl = GetICustEdit(GetDlgItem(DlgWnd,m_Vector2s[i].Float[j].EditID));

			m_Vector2s[i].Float[j].p_SpinCtrl->SetScale(m_Vector2s[i].Float[j].Step);
			m_Vector2s[i].Float[j].p_SpinCtrl->SetLimits(m_Vector2s[i].Float[j].Min,m_Vector2s[i].Float[j].Max);
#if SHADER_ATTR_TREE
			Vector2 &Vec = *m_Vector2s[i].m_pVal->Value().m_Vector2;
			m_Vector2s[i].Float[j].p_SpinCtrl->SetValue(Vec[j],FALSE);
			m_Vector2s[i].Float[j].p_SpinCtrl->SetResetValue(Vec[j]);
#else
			m_Vector2s[i].Float[j].p_SpinCtrl->SetValue(pShaderInstanceData->GetVector2(m_Vector2s[i].Idx)[j],FALSE);
			m_Vector2s[i].Float[j].p_SpinCtrl->SetResetValue(pShaderInstanceData->GetVector2(m_Vector2s[i].Idx)[j]);
#endif
			m_Vector2s[i].Float[j].p_SpinCtrl->LinkToEdit(GetDlgItem(DlgWnd,m_Vector2s[i].Float[j].EditID),EDITTYPE_FLOAT);
		}
	}

	Count = m_Vector3s.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_Vector3s[i].Idx==-1)
			continue;
		for(j=0;j<3;j++)
		{
			m_Vector3s[i].Float[j].p_SpinCtrl = GetISpinner(GetDlgItem(DlgWnd,m_Vector3s[i].Float[j].SpinnerID));
			m_Vector3s[i].Float[j].p_EditCtrl = GetICustEdit(GetDlgItem(DlgWnd,m_Vector3s[i].Float[j].EditID));

			m_Vector3s[i].Float[j].p_SpinCtrl->SetScale(m_Vector3s[i].Float[j].Step);
			m_Vector3s[i].Float[j].p_SpinCtrl->SetLimits(m_Vector3s[i].Float[j].Min,m_Vector3s[i].Float[j].Max);
#if SHADER_ATTR_TREE
			Vector3 &Vec = *m_Vector3s[i].m_pVal->Value().m_Vector3;
			m_Vector3s[i].Float[j].p_SpinCtrl->SetValue(Vec[j],FALSE);
			m_Vector3s[i].Float[j].p_SpinCtrl->SetResetValue(Vec[j]);
#else
			m_Vector3s[i].Float[j].p_SpinCtrl->SetValue(pShaderInstanceData->GetVector3(m_Vector3s[i].Idx)[j],FALSE);
			m_Vector3s[i].Float[j].p_SpinCtrl->SetResetValue(pShaderInstanceData->GetVector3(m_Vector3s[i].Idx)[j]);
#endif
			m_Vector3s[i].Float[j].p_SpinCtrl->LinkToEdit(GetDlgItem(DlgWnd,m_Vector3s[i].Float[j].EditID),EDITTYPE_FLOAT);
		}
	}

	Count = m_Vector4s.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_Vector4s[i].Idx==-1)
			continue;
		for(j=0;j<4;j++)
		{
			m_Vector4s[i].Float[j].p_SpinCtrl = GetISpinner(GetDlgItem(DlgWnd,m_Vector4s[i].Float[j].SpinnerID));
			m_Vector4s[i].Float[j].p_EditCtrl = GetICustEdit(GetDlgItem(DlgWnd,m_Vector4s[i].Float[j].EditID));

			m_Vector4s[i].Float[j].p_SpinCtrl->SetScale(m_Vector4s[i].Float[j].Step);
			m_Vector4s[i].Float[j].p_SpinCtrl->SetLimits(m_Vector4s[i].Float[j].Min,m_Vector4s[i].Float[j].Max);
#if SHADER_ATTR_TREE
			Vector4 &Vec = *m_Vector4s[i].m_pVal->Value().m_Vector4;
			m_Vector4s[i].Float[j].p_SpinCtrl->SetValue(Vec[j],FALSE);
			m_Vector4s[i].Float[j].p_SpinCtrl->SetResetValue(Vec[j]);
#else
			m_Vector4s[i].Float[j].p_SpinCtrl->SetValue(pShaderInstanceData->GetVector4(m_Vector4s[i].Idx)[j],FALSE);
			m_Vector4s[i].Float[j].p_SpinCtrl->SetResetValue(pShaderInstanceData->GetVector4(m_Vector4s[i].Idx)[j]);
#endif
			m_Vector4s[i].Float[j].p_SpinCtrl->LinkToEdit(GetDlgItem(DlgWnd,m_Vector4s[i].Float[j].EditID),EDITTYPE_FLOAT);
		}
	}

	Count = m_Textures.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_Textures[i].Idx==-1)
			continue;
		HWND ButWnd = GetDlgItem(DlgWnd,m_Textures[i].ButtonID);
		m_Textures[i].p_ButCtrl = GetICustButton(ButWnd);
		m_Textures[i].p_ButCtrl->SetDADMgr(&m_DadMgr);

		ButWnd = GetDlgItem(DlgWnd,m_Textures[i].ButtonAlphaID);
		HWND SubButWnd = GetDlgItem(DlgWnd,m_Textures[i].ButtonSubAttrDialogID);
		
		if(ButWnd)
		{
			m_Textures[i].p_ButAlphaCtrl = GetICustButton(ButWnd);
			m_Textures[i].p_ButAlphaCtrl->SetDADMgr(&m_DadMgr);
		}
		else if(SubButWnd)
		{
			m_Textures[i].p_SutSubAttrCtrl = GetICustButton(SubButWnd);
		}
		else
		{
			m_Textures[i].p_ButAlphaCtrl = NULL;
		}
	}

	Count = m_Bools.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_Bools[i].Idx==-1)
			continue;
#if SHADER_ATTR_TREE
		CheckDlgButton(DlgWnd, m_Bools[i].ButtonID, m_Bools[i].m_pVal->Value().m_Bool ? BST_CHECKED : BST_UNCHECKED);
#else
		CheckDlgButton(DlgWnd, m_Bools[i].ButtonID, pShaderInstanceData->GetBool(m_Bools[i].Idx) ? BST_CHECKED : BST_UNCHECKED);
#endif
	}

	m_Initialising = false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxShaderDlg::OnSpinnerChange(s32 SpinnerID)
{
	ShaderInstanceData* pShaderInstanceData = mp_Mtl->GetShaderInstanceData();
	if(m_Initialising) return;

	s32 i,j,SpinnerCount = m_FloatSpinners.GetCount();

	mp_Mtl->NotifyDependents(FOREVER,PART_MTL,REFMSG_NODE_MATERIAL_CHANGED);

	for(i=0;i<SpinnerCount;i++)
	{
		if(m_FloatSpinners[i].SpinnerID == SpinnerID)
		{
			float Val = m_FloatSpinners[i].p_SpinCtrl->GetFVal();
#if SHADER_ATTR_TREE
			m_FloatSpinners[i].m_pVal->Value().m_Float = Val;
#else
			pShaderInstanceData->SetFloat( m_FloatSpinners[i].Idx, Val );
#endif
#if RSTMTL_HW_SHADER
			mp_Mtl->UpdateParameters( true );
#endif // RSTMTL_HW_SHADER

			if ( m_ChangeFunctor != NULL )
			{
				m_ChangeFunctor(NULL, NULL);
			}
			
			return;
		}
	}

	SpinnerCount = m_IntSpinners.GetCount();

	for(i=0;i<SpinnerCount;i++)
	{
		if(m_IntSpinners[i].SpinnerID == SpinnerID)
		{
#if SHADER_ATTR_TREE
			m_IntSpinners[i].m_pVal->Value().m_Int = m_IntSpinners[i].p_SpinCtrl->GetIVal();
#else
			pShaderInstanceData->SetInt( m_IntSpinners[i].Idx, m_IntSpinners[i].p_SpinCtrl->GetIVal() );
#endif
#if RSTMTL_HW_SHADER
			mp_Mtl->UpdateParameters( true );
#endif // RSTMTL_HW_SHADER

			if ( m_ChangeFunctor != NULL )
			{
				m_ChangeFunctor(NULL, NULL);
			}
			
			return;
		}
	}

	SpinnerCount = m_Vector2s.GetCount();

	for(i=0;i<SpinnerCount;i++)
	{
		for(j=0;j<2;j++)
		{
			if(m_Vector2s[i].Float[j].SpinnerID == SpinnerID)
			{
				float Val = m_Vector2s[i].Float[j].p_SpinCtrl->GetFVal();
#if SHADER_ATTR_TREE
				Vector2 &Vec = *m_Vector2s[i].m_pVal->Value().m_Vector2;
				Vec[j] = Val;
#else
				pShaderInstanceData->GetVector2( m_Vector2s[i].Idx )[j] = Val;
#endif
#if RSTMTL_HW_SHADER
				mp_Mtl->UpdateParameters( true );
#endif // RSTMTL_HW_SHADER

				if ( m_ChangeFunctor != NULL )
				{
					m_ChangeFunctor(NULL, NULL);
				}
				
				return;
			}
		}
	}

	SpinnerCount = m_Vector3s.GetCount();

	for(i=0;i<SpinnerCount;i++)
	{
		for(j=0;j<3;j++)
		{
			if(m_Vector3s[i].Float[j].SpinnerID == SpinnerID)
			{
				float Val = m_Vector3s[i].Float[j].p_SpinCtrl->GetFVal();
#if SHADER_ATTR_TREE
				Vector3 &Vec = *m_Vector3s[i].m_pVal->Value().m_Vector3;
				Vec[j] = Val;
#else
				pShaderInstanceData->GetVector3(m_Vector3s[i].Idx)[j] = Val;
#endif
#if RSTMTL_HW_SHADER
				mp_Mtl->UpdateParameters( true );
#endif // RSTMTL_HW_SHADER

				if ( m_ChangeFunctor != NULL )
				{
					m_ChangeFunctor(NULL, NULL);
				}
				
				return;
			}
		}
	}

	SpinnerCount = m_Vector4s.GetCount();

	for(i=0;i<SpinnerCount;i++)
	{
		for(j=0;j<4;j++)
		{
			if(m_Vector4s[i].Float[j].SpinnerID == SpinnerID)
			{
				float Val = m_Vector4s[i].Float[j].p_SpinCtrl->GetFVal();
#if SHADER_ATTR_TREE
				Vector4 &Vec = *m_Vector4s[i].m_pVal->Value().m_Vector4;
				Vec[j] = Val;
#else
				pShaderInstanceData->GetVector4(m_Vector4s[i].Idx)[j] = Val;
#endif	
#if RSTMTL_HW_SHADER
				mp_Mtl->UpdateParameters( true );
#endif // RSTMTL_HW_SHADER

				if ( m_ChangeFunctor != NULL )
				{
					m_ChangeFunctor(NULL, NULL);
				}
				
				return;
			}
		}
	}

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxShaderDlg::OnButtonPressed(s32 ButtonID)
{
	s32 i,TextureCount = m_Textures.GetCount();
	ShaderInstanceData* pShaderInstanceData = mp_Mtl->GetShaderInstanceData();

	for(i=0;i<TextureCount;i++)
	{
		if(m_Textures[i].ButtonID == ButtonID)
		{
#if SHADER_ATTR_TREE
			Texmap *pTex = m_Textures[i].m_pVal->Value().m_Texture;
			PostMessage(mp_Mtl->m_MtlEditWnd,WM_TEXMAP_BUTTON,m_Textures[i].m_pVal->RefTargetIndex(),(LPARAM)mp_Mtl);
#else
			PostMessage(mp_Mtl->m_MtlEditWnd,WM_TEXMAP_BUTTON,m_Textures[i].Idx,(LPARAM)mp_Mtl);
#endif
			return;
		}

		if(m_Textures[i].ButtonAlphaID == ButtonID)
		{
#if SHADER_ATTR_TREE
			// Find the subtexture
			CRstShaderValueNode *pNode = mp_Mtl->FindSingleShaderValueNode(m_Textures[i].m_pVal, NULL,CRstShaderValueNode::eShaderValTypeTexture);
			if(!pNode)
				return;
			Texmap *pTex = pNode->Value().m_Texture;
			PostMessage(mp_Mtl->m_MtlEditWnd,WM_TEXMAP_BUTTON,pNode->RefTargetIndex(),(LPARAM)mp_Mtl);
#else
			PostMessage(mp_Mtl->m_MtlEditWnd,WM_TEXMAP_BUTTON,m_Textures[i].Idx + TextureCount,(LPARAM)mp_Mtl);
#endif
			return;
		}
#if SHADER_ATTR_TREE
		if(m_Textures[i].ButtonSubAttrDialogID == ButtonID)
		{
			m_Textures[i].m_pVal->ToggleDialog();
		}
#endif
	}	

	s32 Count = m_Vector3s.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_Vector3s[i].ButtonID == ButtonID)
		{
			DWORD Get = RGB(	(char)(m_Vector3s[i].Float[0].p_SpinCtrl->GetFVal() * 255.0f),
								(char)(m_Vector3s[i].Float[1].p_SpinCtrl->GetFVal() * 255.0f),
								(char)(m_Vector3s[i].Float[2].p_SpinCtrl->GetFVal() * 255.0f));
			IPoint2 Pos(10,10);
			HSVDlg_Do(m_Wnd,&Get,&Pos,NULL,"Choose colour");

			m_Vector3s[i].Float[0].p_SpinCtrl->SetValue((float)GetRValue(Get) / 255.0f,FALSE);
			m_Vector3s[i].Float[1].p_SpinCtrl->SetValue((float)GetGValue(Get) / 255.0f,FALSE);
			m_Vector3s[i].Float[2].p_SpinCtrl->SetValue((float)GetBValue(Get) / 255.0f,FALSE);
			// DHM -- 2009/02/11
			// This change ensures that the material parameters are also changed rather than
			// just the UI spinners.
#if SHADER_ATTR_TREE
			Vector3 &Vec = *m_Vector3s[i].m_pVal->Value().m_Vector3;
			Vec[0] = (float)GetRValue(Get) / 255.0f;
			Vec[1] = (float)GetGValue(Get) / 255.0f;
			Vec[2] = (float)GetBValue(Get) / 255.0f;
#else
			pShaderInstanceData->SetVector3(m_Vector3s[i].Idx, Vector3( (float)GetRValue(Get) / 255.0f, (float)GetGValue(Get) / 255.0f, (float)GetBValue(Get) / 255.0f ) );
#endif

#if RSTMTL_HW_SHADER
			mp_Mtl->UpdateParameters( true );
#endif // RSTMTL_HW_SHADER
		}
	}

	#define GetAValue(rgb)      ((BYTE)((rgb)>>24))

	Count = m_Vector4s.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_Vector4s[i].ButtonID == ButtonID)
		{
			DWORD Get = RGB(	(char)(m_Vector4s[i].Float[0].p_SpinCtrl->GetFVal() * 255.0f),
								(char)(m_Vector4s[i].Float[1].p_SpinCtrl->GetFVal() * 255.0f),
								(char)(m_Vector4s[i].Float[2].p_SpinCtrl->GetFVal() * 255.0f));

			Get |= (char)(m_Vector4s[i].Float[3].p_SpinCtrl->GetFVal() * 255.0f) << 24;

			IPoint2 Pos(10,10);
			HSVDlg_Do(m_Wnd,&Get,&Pos,NULL,"Choose colour");

			m_Vector4s[i].Float[0].p_SpinCtrl->SetValue((float)GetRValue(Get) / 255.0f,FALSE);
			m_Vector4s[i].Float[1].p_SpinCtrl->SetValue((float)GetGValue(Get) / 255.0f,FALSE);
			m_Vector4s[i].Float[2].p_SpinCtrl->SetValue((float)GetBValue(Get) / 255.0f,FALSE);
			m_Vector4s[i].Float[3].p_SpinCtrl->SetValue((float)GetAValue(Get) / 255.0f,FALSE);
			// DHM -- 2009/02/11
			// This change ensures that the material parameters are also changed rather than
			// just the UI spinners.
#if SHADER_ATTR_TREE
			Vector4 &Vec = *m_Vector4s[i].m_pVal->Value().m_Vector4;
			Vec[0] = (float)GetRValue(Get) / 255.0f;
			Vec[1] = (float)GetGValue(Get) / 255.0f;
			Vec[2] = (float)GetBValue(Get) / 255.0f;
			Vec[3] = (float)GetAValue(Get) / 255.0f;
#else
			pShaderInstanceData->SetVector4( m_Vector4s[i].Idx, Vector4( (float)GetRValue(Get) / 255.0f, (float)GetGValue(Get) / 255.0f, (float)GetBValue(Get) / 255.0f, (float)GetAValue(Get) / 255.0f ) );
#endif

#if RSTMTL_HW_SHADER
			mp_Mtl->UpdateParameters( true );
#endif // RSTMTL_HW_SHADER
		}
	}

	Count = m_Bools.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_Bools[i].ButtonID == ButtonID)
		{
#if SHADER_ATTR_TREE
			if(m_Bools[i].m_pVal->Value().m_Bool)
			{
				m_Bools[i].m_pVal->Value().m_Bool = false;
				CheckDlgButton(m_Wnd, m_Bools[i].ButtonID, BST_UNCHECKED);
			}
			else
			{
				m_Bools[i].m_pVal->Value().m_Bool = true;
				CheckDlgButton(m_Wnd, m_Bools[i].ButtonID, BST_CHECKED);
			}
#else
			if( pShaderInstanceData->GetBool( m_Bools[i].Idx ) )
			//if(mp_Mtl->m_Bools[m_Bools[i].Idx])
			{
				pShaderInstanceData->GetBool( m_Bools[i].Idx ) = false;
				CheckDlgButton(m_Wnd, m_Bools[i].ButtonID, BST_UNCHECKED);
			}
			else
			{
				pShaderInstanceData->GetBool( m_Bools[i].Idx ) = true;
				CheckDlgButton(m_Wnd, m_Bools[i].ButtonID, BST_CHECKED);
			}
#endif
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxShaderDlg::OnDestroy()
{
	s32 i,j,Count = m_FloatSpinners.GetCount();
	for(i=0;i<Count;i++)
	{
		ReleaseISpinner(m_FloatSpinners[i].p_SpinCtrl);
		ReleaseICustEdit(m_FloatSpinners[i].p_EditCtrl);
	}

	Count = m_IntSpinners.GetCount();
	for(i=0;i<Count;i++)
	{
		ReleaseISpinner(m_IntSpinners[i].p_SpinCtrl);
		ReleaseICustEdit(m_IntSpinners[i].p_EditCtrl);
	}

	Count = m_Vector2s.GetCount();
	for(i=0;i<Count;i++)
	{
		for(j=0;j<2;j++)
		{
			ReleaseISpinner(m_Vector2s[i].Float[j].p_SpinCtrl);
			ReleaseICustEdit(m_Vector2s[i].Float[j].p_EditCtrl);
		}
	}

	Count = m_Vector3s.GetCount();
	for(i=0;i<Count;i++)
	{
		for(j=0;j<3;j++)
		{
			ReleaseISpinner(m_Vector3s[i].Float[j].p_SpinCtrl);
			ReleaseICustEdit(m_Vector3s[i].Float[j].p_EditCtrl);
		}
	}

	Count = m_Vector4s.GetCount();
	for(i=0;i<Count;i++)
	{
		for(j=0;j<4;j++)
		{
			ReleaseISpinner(m_Vector4s[i].Float[j].p_SpinCtrl);
			ReleaseICustEdit(m_Vector4s[i].Float[j].p_EditCtrl);
		}
	}

	Count = m_Textures.GetCount();
	for(i=0;i<Count;i++)
	{
		if(m_Textures[i].p_ButCtrl)
		{
			ReleaseICustButton(m_Textures[i].p_ButCtrl);
			m_Textures[i].p_ButCtrl = NULL;
		}
		if(m_Textures[i].p_ButAlphaCtrl)
		{
			ReleaseICustButton(m_Textures[i].p_ButAlphaCtrl);
			m_Textures[i].p_ButAlphaCtrl = NULL;
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
RstMaxErrorDlg::RstMaxErrorDlg(RstMtlMax* p_Mtl, IMtlParams* p_MtlParams, atString errormsg):
mp_Mtl(p_Mtl),
m_Wnd(NULL),
mp_MtlParams(p_MtlParams)
{
//	m_DadMgr.Init(this);
//	Reset();
	m_DialogRes.AddControl(DialogRes::CTRL_EDIT,WS_VISIBLE|WS_CHILD|WS_BORDER|WS_HSCROLL|WS_VSCROLL|ES_MULTILINE,errormsg,5,5,208,50,1);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
RstMaxErrorDlg::~RstMaxErrorDlg()
{
	Reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
HWND RstMaxErrorDlg::Create(TSTR title)
{
	assert(!m_Wnd);

	m_DialogRes.SetSize(218,60);

	m_Wnd = mp_MtlParams->AddRollupPage(g_hDllInstance,
		m_DialogRes.GetTemplate(),
		NULL,
		title,
		(LPARAM)this);

	return m_Wnd;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxErrorDlg::DeleteThis() 
{
#if !SHADER_ATTR_TREE
	mp_Mtl->mp_DlgError = NULL;
#endif
	//	mp_Mtl->mp_MtlDlg = NULL;
	delete this; 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ReferenceTarget* RstMaxErrorDlg::GetThing() 
{
	return mp_Mtl; 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxErrorDlg::Reset()
{
	if(m_Wnd)
	{
		mp_MtlParams->DeleteRollupPage(m_Wnd);
		DestroyWindow(m_Wnd);
	}
	m_Wnd = NULL;
}
