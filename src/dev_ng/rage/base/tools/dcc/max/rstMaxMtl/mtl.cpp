#pragma warning( disable: 4265 )
#pragma warning( disable: 4263 )

#include "MaterialPresets/MaterialPresetManager.h"

#include <maxscript/maxscript.h>
#include <maxscript/mxsplugin/mxsplugin.h>


#include "rstMaxMtl/mtl.h"
#include "rstMaxMtl/rstMax.h"
#include "rstMaxMtl/resource.h"
#include "rstMaxMtl/dialogMtl.h"
#include "rstMaxMtl/dialogMtlPreset.h"
#include "rstMaxMtl/utility.h"
#include "rstMaxMtl/dbeditor.h"
#include "file/device.h"
#include "file/stream.h"
#include "rst/shaderlist.h"
#include "rst/shaderdb.h"
#include "grcore/texturestring.h"
#include "rexMaxUtility/utility.h"

#include "ID3D9GraphicsWindow.h"
#include "IHardwareMaterial.h"

#include "rageMaxMaterial/shaderInstance.h"
#include "rageMaxMaterial/ShaderInstancePreset.h"
#include "rageMaxMaterial/ShaderInstanceRageShader.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

static bool sThrowWarning = true;
static float sGlobalTexScaleMultiplier = 0.5;
static const char* sAlphaMapPropertyname = "ExposeAlphaMap";
//////////////////////////////////////////////////////////////////////////////////////////////////////////
class rstRstMtlMaxClassDesc : public ClassDesc2
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new RstMtlMax(); }
	const TCHAR *	ClassName() { return _T("Rage Shader"); }
	SClass_ID		SuperClassID() { return MATERIAL_CLASS_ID; }
	Class_ID		ClassID() { return RSTMATERIAL_CLASS_ID; }
#if RSTMTL_HW_SHADER
	const TCHAR* 	Category() { return _T("HWmaterial"); }
#else
	const TCHAR* 	Category() { return _T("material"); }
#endif
	const TCHAR*	InternalName() { return _T("Rage Shader"); }
	HINSTANCE		HInstance() { return g_hDllInstance; }
};

atArray<atArray<atString>> RstMtlMax::m_GtaRuleList;

static rstRstMtlMaxClassDesc rsmClassDesc;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

static FPInterfaceDesc rsm_mixininterface( RSM_INTERFACE,
	_T("rageShaderMaterial"), 0,
	&rsmClassDesc, FP_MIXIN,

	rsm_FpGetShaderName, _T("rsmGetShaderName"), 0, TYPE_TSTR_BV, 0, 0,

	rsm_FpGetVariableCount, _T("rsmGetVariableCount"), 0, TYPE_INT, 0, 0,

	rsm_FpGetVariableName, _T("rsmGetVariableName"), 0, TYPE_TSTR_BV, 0, 1,
		_T("index"), 0, TYPE_INT,

	rsm_FpGetVariableType, _T("rsmGetVariableType"), 0, TYPE_TSTR_BV, 0, 1,
		_T("index"), 0, TYPE_INT,

	rsm_FpGetTextureVariableValue, _T("rsmGetTextureVariableValue"), 0, TYPE_TSTR_BV, 0, 1,
		_T("index"), 0, TYPE_INT,

	rsm_FpGetFloatVariableValue, _T("rsmGetFloatVariableValue"), 0, TYPE_FLOAT, 0, 1,
		_T("index"), 0, TYPE_INT,

	rsm_FpGetIntVariableValue, _T("rsmGetIntVariableValue"), 0, TYPE_INT, 0, 1,
		_T("index"), 0, TYPE_INT,

	rsm_FpGetBoolVariableValue, _T("rsmGetBoolVariableValue"), 0, TYPE_BOOL, 0, 1,
		_T("index"), 0, TYPE_INT,

	rsm_FpGetVector2VariableValue, _T("rsmGetVector2VariableValue"), 0, TYPE_POINT2_BV, 0, 1,
		_T("index"), 0, TYPE_INT,

	rsm_FpGetVector3VariableValue, _T("rsmGetVector3VariableValue"), 0, TYPE_POINT3_BV, 0, 1,
		_T("index"), 0, TYPE_INT,

	rsm_FpGetVector4VariableValue, _T("rsmGetVector4VariableValue"), 0, TYPE_POINT4_BV, 0, 1,
		_T("index"), 0, TYPE_INT,

	end
);

FPInterfaceDesc* IRageShaderMaterial::GetDesc()
{
	return &rsm_mixininterface;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ClassDesc2* RstMtlMax::Creator()
{
	static rstRstMtlMaxClassDesc classDesc;

	return &classDesc;
}
#if RSTMTL_HW_SHADER 
void RstMtlMax::D3dPreNotifyProc( void *param, NotifyInfo *info )
{
	RstMtlMax* mtl = (RstMtlMax*)param;

	char* name = mtl->GetName();
	char* fullname = mtl->GetFullName();
	
	
	int cacheSize = mtl->m_pMeshCache->GetCacheSize();
	
	for( int i=0; i<cacheSize; ++i )
	{
		IRenderMesh* activeRenderMesh = mtl->m_pMeshCache->GetActiveRenderMesh(i);
		Mesh * activeMesh = mtl->m_pMeshCache->GetActiveMesh(i);
		activeRenderMesh->Evaluate( NULL, NULL, 0, false );
		activeRenderMesh->Invalidate();
	}

	if( mtl->m_EffectParser )
	{
		mtl->m_EffectParser->OnLostDevice();
		mtl->m_EffectParser->DestroyParser();
		mtl->m_EffectParser = NULL;
	}

	mtl->m_ElemContainer.FreeD3Dtextures();
	mtl->m_ElemContainer.DeleteAllElements();
}

void RstMtlMax::D3dPostNotifyProc( void *param, NotifyInfo *info )
{
	RstMtlMax* mtl = (RstMtlMax*)param;
	if( mtl->m_EffectParser )
	{
		mtl->m_EffectParser->OnResetDevice();
	}
}
#endif // RSTMTL_HW_SHADER

//////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetLightingMode( bool lighting ) 
{ 
	m_HasLighting = lighting;
	UpdateParameters(true);
	if(mp_MtlDlg)
		mp_MtlDlg->SetDialogShaderFlag(IDC_CHK_LIGHTING, lighting);
}
void RstMtlMax::SetUseDiffuseMapAlpha( bool useIt ) { 
	m_UseDiffuseMapAlpha = useIt; 
	UpdateParameters(true);
	if(mp_MtlDlg)
		mp_MtlDlg->SetDialogShaderFlag(IDC_CHK_DIFFUSEALPHA, useIt);
}
void RstMtlMax::SetHasAmbLighting(bool ambLight) 
{
	m_HasAmbLighting = ambLight;
	UpdateParameters(true);
	if(mp_MtlDlg)
		mp_MtlDlg->SetDialogShaderFlag(IDC_CHK_AMB_LIGHTING, ambLight);
}



//////////////////////////////////////////////////////////////////////////
// Hierarchy helper functions

void CopyHierarchy(CRstShaderValueNode* currNode, RstMtlMax *dstmtl, atArray<CRstShaderValueNode*> src, atArray<CRstShaderValueNode*> dst)
{
	dst.Reset();
	dst.Resize(src.GetCount());

//	int numTextures = 0;
	for(int i=0;i<src.GetCount();i++)
	{
		dst[i] = new CRstShaderValueNode(dstmtl, *src[i], currNode);
// 		if(dst[i]->Type()==CRstShaderValueNode::eShaderValTypeTexture)
// 		{
// 			//dstmtl->m_references.PushAndGrow(dst[i], 8);
// 			dstmtl->ReplaceReference(src[i]->RefTargetIndex(), src[i]->Value().m_Texture);
// 			numTextures++;
// 		}
		for(int i=0;i<dst[i]->Inputs().GetCount();i++)
			CopyHierarchy(dst[i], dstmtl, src[i]->Inputs(), dst[i]->Inputs());
	}
}

void ResetCleanupFlags(CRstShaderValueNode* currNode)
{
	for(int i=0;i<currNode->Inputs().GetCount();i++)
	{
		ResetCleanupFlags(currNode->Inputs()[i]);
	}
	currNode->DeleteMe() = true;
}

bool DeleteHierarchy(CRstShaderValueNode* currNode, bool onlyFlaggedNodes=false, bool onlyChildren=false)
{
	for(int i=0;i<currNode->Inputs().GetCount();i++)
	{
		if(DeleteHierarchy(currNode->Inputs()[i], onlyFlaggedNodes, false))// false as children are all supposed to be deleted
			currNode->Inputs().Delete(i);
	}
	if(!onlyChildren && (!onlyFlaggedNodes || currNode->DeleteMe()))
	{
		delete currNode;
		return true;
	}
	return false;
}

int RstMtlMax::FindShaderValueNode(CRstShaderValueNode* currNode, bool breakOnFirstFind, const char *thename, s32 type, s32 flags, void *value, int refTargetIndex)
{
#if SHADER_ATTR_TREE
	if(	(currNode->Type()&type) && 
		(CRstShaderValueNode::eShaderValFlagNone==currNode->Flags() || currNode->Flags()&flags ) )
	{
		if(	// NOT root node!
			currNode!=m_shaderValRoot &&
			// Name checking
			((thename!=SHADERVALNAME_IGNORE && (!strcmp(thename,SHADERVALNAME_ALL) || !strcmp(thename, currNode->Name().c_str()))) ||
			// value checking
			(value!=NULL && value==(void*)currNode->Value().m_Void) ||
			// ref target equalness
			(refTargetIndex!=-1 && refTargetIndex==currNode->RefTargetIndex()))
			)
		{
			m_lastNodeSearchResult.PushAndGrow(currNode);
			if(breakOnFirstFind)
			{
				return m_lastNodeSearchResult.GetCount();
			}
		}
	}
	for(int i=0;i<currNode->Inputs().GetCount();i++)
	{
		int childHits = FindShaderValueNode(currNode->Inputs()[i], breakOnFirstFind, thename,type,flags,value,refTargetIndex);
		if(childHits>0 && breakOnFirstFind)
			return childHits;
	}
#endif
	return m_lastNodeSearchResult.GetCount();
}

void RstMtlMax::ExportHierarchy(CRstShaderValueNode* currNode, fiStream *p_MemStream)
{
	if(	CRstShaderValueNode::eShaderValTypeNone!=currNode->Type() && 
		CRstShaderValueNode::eShaderValTypeTexture!=currNode->Type())
	{
		s32 WriteSize;
		WriteString(p_MemStream,currNode->Name());
		p_MemStream->WriteInt((s32*)&currNode->GetShaderVar().VarType,1);
		p_MemStream->WriteInt(&currNode->GetShaderVar().Idx,1);

		switch(currNode->GetShaderVar().VarType)
		{
		case grcEffect::VT_FLOAT:
			WriteSize = 4;
			p_MemStream->WriteInt(&WriteSize,1);
			p_MemStream->WriteFloat(&currNode->Value().m_Float,1);
			break;
		case grcEffect::VT_INT_DONTUSE:
			WriteSize = 4;
			p_MemStream->WriteInt(&WriteSize,1);
			p_MemStream->WriteInt(&currNode->Value().m_Int,1);
			break;
		case grcEffect::VT_VECTOR2:
			{
				WriteSize = 8;
				p_MemStream->WriteInt(&WriteSize,1);
				Vector2 &Vec = *currNode->Value().m_Vector2;
				p_MemStream->WriteFloat(&(Vec[0]),1);
				p_MemStream->WriteFloat(&(Vec[1]),1);
			}
			break;
		case grcEffect::VT_VECTOR3:
			{
				WriteSize = 12;
				p_MemStream->WriteInt(&WriteSize,1);
				Vector3 &Vec = *currNode->Value().m_Vector3;
				p_MemStream->WriteFloat(&(Vec[0]),1);
				p_MemStream->WriteFloat(&(Vec[1]),1);
				p_MemStream->WriteFloat(&(Vec[2]),1);
			}
			break;
		case grcEffect::VT_VECTOR4:
			{
				WriteSize = 16;
				p_MemStream->WriteInt(&WriteSize,1);
				Vector4 &Vec = *currNode->Value().m_Vector4;
				p_MemStream->WriteFloat(&(Vec[0]),1);
				p_MemStream->WriteFloat(&(Vec[1]),1);
				p_MemStream->WriteFloat(&(Vec[2]),1);
				p_MemStream->WriteFloat(&(Vec[3]),1);
			}
			break;
		case grcEffect::VT_BOOL_DONTUSE:
			{
				WriteSize = 4;
				int iTmp = (int)currNode->Value().m_Int;
				p_MemStream->WriteInt(&WriteSize, 1);
				p_MemStream->WriteInt(&iTmp, 1);
				break;
			}
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		default:
			WriteSize = 0;
			p_MemStream->WriteInt(&WriteSize,1);
			break;
		}
	}

	for(int i=0;i<currNode->Inputs().GetCount();i++)
	{
		ExportHierarchy(currNode->Inputs()[i], p_MemStream);
	}
}

int RstMtlMax::GetHierarchyDataSize(CRstShaderValueNode* currNode)
{
	s32 WriteSize = 0;
	if(currNode->Type()>CRstShaderValueNode::eShaderValTypeUndefined && currNode->Type()<CRstShaderValueNode::eShaderValTypeString)
	{
		WriteSize += currNode->Name().GetLength() + 12; //key + type + index
		WriteSize += 4; // data size

		switch(currNode->GetShaderVar().VarType)
		{
		case grcEffect::VT_FLOAT:
			WriteSize += 4;
			break;
		case grcEffect::VT_INT_DONTUSE:
			WriteSize += 4;
			break;
		case grcEffect::VT_VECTOR2:
			WriteSize += 8;
			break;
		case grcEffect::VT_VECTOR3:
			WriteSize += 12;
			break;
		case grcEffect::VT_VECTOR4:
			WriteSize += 16;
			break;
		case grcEffect::VT_BOOL_DONTUSE:
			WriteSize += 4;
		case grcEffect::VT_TEXTURE:
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		default:
			WriteSize += 0;
		}
	}

	for(int i=0;i<currNode->Inputs().GetCount();i++)
	{
		WriteSize += GetHierarchyDataSize(currNode->Inputs()[i]);
	}
	return WriteSize;
}

//////////////////////////////////////////////////////////////////////////
// Not optimal but for works for 2 layers
//                    6(!)
//                 4<
//              2<    7
//           1<    5
//              3<
//                ...

void RstMtlMax::RebuildReferenceHierarchy(CRstShaderValueNode* currNode)
{
#if SHADER_ATTR_TREE
	for(int i=0;i<currNode->Inputs().GetCount();i++)
	{
		if(CRstShaderValueNode::eShaderValTypeTexture==currNode->Inputs()[i]->Type())
		{
//			assert(m_references.GetCount()<=currNode->RefTargetIndex() && !m_references[currNode->RefTargetIndex()]);
			for(RsAttrNodeArray::iterator iNode = m_references.begin();iNode<m_references.end();iNode++)
				if((*iNode)==currNode->Inputs()[i])
					continue;
			m_references.PushAndGrow(currNode->Inputs()[i]);
		}
	}
	for(int i=0;i<currNode->Inputs().GetCount();i++)
	{
		RebuildReferenceHierarchy(currNode->Inputs()[i]);
	}
#endif
}
void RstMtlMax::RebuildReferences()
{
#if SHADER_ATTR_TREE
	m_references.clear();
	RebuildReferenceHierarchy(m_shaderValRoot);
	m_refsNeedsRebuild = false;
#endif
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
RstMtlMax::RstMtlMax()
	: mp_MtlDlg(NULL)
	, mp_MtlParam(NULL)
	, m_RefreshTextures(false)
#if !SHADER_ATTR_TREE
	, m_IsPreset(true)
	, mp_DlgExtra(NULL)
	, mp_DlgError(NULL)
	, mp_DlgMtlPreset(NULL)
	, m_IsTwoSided(false)
	, m_HasLighting(false)
	, m_HasAmbLighting(false)
	, m_IsHwMode(true)
	, m_UseDiffuseMapAlpha(false)
#else
	, m_refsNeedsRebuild(true)
#endif
{
#if	MAX_RELEASE > MAX_RELEASE_R9
	for(int i = 0; i <NTEXHANDLES; ++i)
	{
		texHandle[i] = NULL;
		mapsUsage[i] = MAPUSAGE_UNDEFINED;
		//borderColor[i] = 0;
		texHandleValidArray[i].SetEmpty();
	}
	//texHandle[0] = texHandle[1] = NULL;
	texHandleValid.SetEmpty();
#if !RSTMTL_HW_SHADER
	SetMtlFlag(MTL_TEX_DISPLAY_ENABLED);
#else
	//for (int i=0; i<NSUBTEX; i++) tex[i] = NULL;
	ivalid = FOREVER;
	m_pD3DDevice = NULL;

	m_RefreshElements = false;
	m_TexScaleMultiplier = 0.125f;
	m_EffectParser = NULL;

	m_hwRendering = Hardware;
	m_debugParseSuccess = true;

	m_pMeshCache = IRenderMeshCache::GetRenderMeshCache();
	m_pMeshCache->SetMeshType(IRenderMesh::kMesh);

	// TL TODO - Should we always be creating a rageShader instance data to start?
	m_ShaderInstanceData = new ShaderInstanceRageShader();
	
	//if (!loading) 
	//	Reset();

	SetMtlFlag(MTL_HW_MAT_ENABLED);

	stdDualVS = (IStdDualVS*) CreateInstance(REF_MAKER_CLASS_ID, STD_DUAL_VERTEX_SHADER);
	if(stdDualVS)
	{
		stdDualVS->SetCallback((IStdDualVSCallback*)this);
	}

	RegisterNotification(D3dPreNotifyProc, this, NOTIFY_D3D_PRE_DEVICE_RESET);
	RegisterNotification(D3dPostNotifyProc, this, NOTIFY_D3D_POST_DEVICE_RESET);
	
#endif
#endif // MAX_RELEASE > MAX_RELEASE_R9
#if SHADER_ATTR_TREE
	m_shaderValRoot = new CRstShaderValueNode(this);
	SetIsPreset(true);
	SetIsHwMode(true);
#endif
}

RstMtlMax::RstMtlMax(ShaderInstanceData* instanceData, atString shaderName)
	: mp_MtlDlg(NULL)
	, mp_MtlParam(NULL)
	, m_RefreshTextures(false)
#if !SHADER_ATTR_TREE
	, m_IsPreset(true)
	, mp_DlgExtra(NULL)
	, mp_DlgError(NULL)
	, mp_DlgMtlPreset(NULL)
	, m_IsTwoSided(false)
	, m_HasLighting(false)
	, m_HasAmbLighting(false)
	, m_IsHwMode(true)
	, m_UseDiffuseMapAlpha(false)
#else
	, m_refsNeedsRebuild(true)
#endif
{
	
#if	MAX_RELEASE > MAX_RELEASE_R9
	for(int i = 0; i <NTEXHANDLES; ++i)
	{
		texHandle[i] = NULL;
		mapsUsage[i] = MAPUSAGE_UNDEFINED;
		//borderColor[i] = 0;
		texHandleValidArray[i].SetEmpty();
	}
	//texHandle[0] = texHandle[1] = NULL;
	texHandleValid.SetEmpty();
#if !RSTMTL_HW_SHADER
	SetMtlFlag(MTL_TEX_DISPLAY_ENABLED);
#else
	//for (int i=0; i<NSUBTEX; i++) tex[i] = NULL;
	ivalid = FOREVER;
	m_pD3DDevice = NULL;

	m_RefreshElements = false;
	m_TexScaleMultiplier = 0.125f;
	m_EffectParser = NULL;

	m_hwRendering = Hardware;
	m_debugParseSuccess = true;

	m_pMeshCache = IRenderMeshCache::GetRenderMeshCache();
	m_pMeshCache->SetMeshType(IRenderMesh::kMesh);

	// TL TODO - Should we always be creating a rageShader instance data to start?
	m_ShaderInstanceData = new ShaderInstanceRageShader();

	if (instanceData)
		m_ShaderInstanceData->Copy(*instanceData);

	//if (!loading) 
	//	Reset();

	SetMtlFlag(MTL_HW_MAT_ENABLED);

	stdDualVS = (IStdDualVS*) CreateInstance(REF_MAKER_CLASS_ID, STD_DUAL_VERTEX_SHADER);
	if(stdDualVS)
	{
		stdDualVS->SetCallback((IStdDualVSCallback*)this);
	}

	RegisterNotification(D3dPreNotifyProc, this, NOTIFY_D3D_PRE_DEVICE_RESET);

#endif
#endif // MAX_RELEASE > MAX_RELEASE_R9
#if SHADER_ATTR_TREE
	m_shaderValRoot = new CRstShaderValueNode(this);
	SetIsPreset(true);
	SetIsHwMode(true);
#endif
	SetShaderName(shaderName);
}

RstMtlMax::~RstMtlMax()
{
#if RSTMTL_HW_SHADER
	UnRegisterNotification(D3dPreNotifyProc, this, NOTIFY_D3D_PRE_DEVICE_RESET);
	UnRegisterNotification(D3dPostNotifyProc, this, NOTIFY_D3D_POST_DEVICE_RESET);
#endif // RSTMTL_HW_SHADER
#if SHADER_ATTR_TREE
	DeleteHierarchy(m_shaderValRoot);
	m_shaderValRoot = NULL;
#else
	delete m_ShaderInstanceData;
	if(mp_DlgExtra)
	{
		delete mp_DlgExtra;
	}
	if (mp_DlgMtlPreset)
	{
		delete mp_DlgMtlPreset;
	}
	delete mp_DlgError;
	mp_DlgExtra = NULL;
	mp_DlgError = NULL;
	mp_DlgMtlPreset = NULL;
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RstMtlMax::NumSubs()
{
	return NumRefs();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
Animatable* RstMtlMax::SubAnim(int i)
{
	return (Animatable*)GetReference(i);
}

RefResult RstMtlMax::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message) 
{
//	switch (message) 
//	{
//	case REFMSG_CHANGE:
//		texHandleValid.SetEmpty();
//		DiscardTexHandles(); 
//	}

	return REF_SUCCEED;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle RstMtlMax::Clone(RemapDir &remap)
{
	RstMtlMax* p_New = new RstMtlMax;
	ShaderInstanceData* pNewShaderInstanceData = p_New->GetShaderInstanceData();

	p_New->m_ShaderName = m_ShaderName;
#if RSTMTL_HW_SHADER
	p_New->m_OverrideTexScale = m_OverrideTexScale;
	p_New->m_TexScaleMultiplier = m_TexScaleMultiplier;
#endif //RSTMTL_HW_SHADER

#if !SHADER_ATTR_TREE
	// Make sure to setup the correct instance data type, it is created as RageShader by default.
	if (GetShaderInstanceData()->GetInstanceType() != p_New->GetShaderInstanceData()->GetInstanceType())
	{
		p_New->RecreateInstanceData(GetShaderInstanceData()->GetInstanceType());
		pNewShaderInstanceData = p_New->GetShaderInstanceData();
	}

	*pNewShaderInstanceData = *GetShaderInstanceData();
	p_New->m_IsTwoSided = m_IsTwoSided;

	s32 count = m_ShaderInstanceData->GetTextureCount();

	for(s32 i=0;i<count;i++)
	{
		pNewShaderInstanceData->AddTexture(NULL);
		p_New->ReplaceReference(i,remap.CloneRef(GetShaderInstanceData()->GetTexture(i)));
	}

	s32 alphaCount = m_ShaderInstanceData->GetTextureAlphaCount();


	for(s32 i=0;i<alphaCount;i++)
	{
		pNewShaderInstanceData->AddTextureAlpha(NULL);
		p_New->ReplaceReference(i + count,remap.CloneRef(GetShaderInstanceData()->GetTextureAlpha(i)));
	}
#else
	p_New->SetIsPreset(IsPreset());
	p_New->VerifyData();
	AssertMsg(m_shaderValRoot, "RstMtlMax::m_shaderValRoot is undefined");
	AssertMsg(p_New->m_shaderValRoot, "New copied RstMtlMax::m_shaderValRoot is undefined");

	CopyHierarchy(m_shaderValRoot, p_New, m_shaderValRoot->Inputs(), p_New->m_shaderValRoot->Inputs());

	RebuildReferences();
#endif
	BaseClone(this,p_New,remap);

	return p_New;
}

//////////////////////////////////////////////////////////////////////////

RstMaxMtlPresetDlg* RstMtlMax::CreateMaterialPresetDialog()
{
#if !SHADER_ATTR_TREE
	if (mp_DlgMtlPreset)
	{
		delete mp_DlgMtlPreset;
	}

	mp_DlgMtlPreset = new RstMaxMtlPresetDlg(this, mp_MtlParam);

	return mp_DlgMtlPreset;
#endif

	return NULL;
}

//////////////////////////////////////////////////////////////////////////
void RstMtlMax::ConstructMaterialPresetDialog(rageRstShaderDbEntry *dbEntry)
{
	RstMaxMtlPresetDlg* pPresetDlg = NULL;
	pPresetDlg = CreateMaterialPresetDialog();
	pPresetDlg->Create();
}

//////////////////////////////////////////////////////////////////////////

RstMaxShaderDlg* RstMtlMax::CreateShaderParamDialog()
{
#if !SHADER_ATTR_TREE
	if(mp_DlgExtra)
	{
		delete mp_DlgExtra;
	}

	mp_DlgExtra = new RstMaxShaderDlg(this, mp_MtlParam);
	//mp_DlgExtra->SetMtlDlg(mp_MtlDlg);
	return mp_DlgExtra;
#endif
	return NULL;
}

//////////////////////////////////////////////////////////////////////////

void RstMtlMax::ConstructShaderParamDialog(grcEffect* p_Shader, rageRstShaderDbEntry *dbEntry)
{
	const char* p_Name = p_Shader->GetEffectName();

	RstMaxShaderDlg* pShaderDlg = NULL;

	grcInstanceData *pPresetEntry = NULL;
	if(dbEntry)
		pPresetEntry = dbEntry->m_Preset;

#if !SHADER_ATTR_TREE

	if(mp_DlgError)
	{
		delete mp_DlgError;
		mp_DlgError = NULL;
	}

	// If we used to be a material preset, make sure to clean up the UI.
	if (mp_DlgMtlPreset)
	{
		delete mp_DlgMtlPreset;
		mp_DlgMtlPreset = NULL;
	}

	if(!m_debugParseSuccess)
	{
		mp_DlgError = new RstMaxErrorDlg(this, mp_MtlParam, m_parserErrorMsg);
		mp_DlgError->Create();
		return;
	}

	pShaderDlg = CreateShaderParamDialog();
	pShaderDlg->SetControlValueChangeFunctor(RstMax::NotifyValueChangedCallbacks);

	s32 i,NumVars;
	s32 NumFloats = 0;
	s32 NumInts = 0;
	s32 NumTextures = 0;
	s32 NumVector2s = 0;
	s32 NumVector3s = 0;
	s32 NumVector4s = 0;
	s32 NumBools = 0;

	NumVars = p_Shader->GetInstancedVariableCount();

	bool IsAlpha = IsAlphaShader();

	for(i=0;i<NumVars;i++)
	{
		bool Locked = false;

		grmVariableInfo VarInfo;

		p_Shader->GetInstancedVariableInfo(i,VarInfo);


		//		if(VarInfo.m_Flags.IsSet(grmVariableInfo::FLG_PRESET_ONLY))
		//		{
		//			Locked = true;
		//		}

#if ENABLE_PRESET_EDITING
		if(m_ShaderInstanceData->m_PresetOverrides.Find(atString(VarInfo.m_Name))!= -1)
			Locked = true;
#endif
		//		if(IsPreset)
		//			continue;

		grcEffectVar handle = p_Shader->LookupVar(VarInfo.m_Name);

		atString Label(VarInfo.m_UiName);
		Label += ":";

		switch(VarInfo.m_Type)
		{
		case grcEffect::VT_FLOAT:

			if(IsPreset() && Locked)
			{
				float Var;
				p_Shader->GetVar(*pPresetEntry, handle,Var);
				char Output[64];
				sprintf_s(Output, 64, "%0.3f",Var);
				mp_DlgExtra->AddStaticString(Label, atString(Output), VarInfo.m_Type);
			}
			else
			{
				mp_DlgExtra->AddFloatSpinner(	Label,
					NumFloats,
					VarInfo.m_UiMin,
					VarInfo.m_UiMax,
					VarInfo.m_UiStep);
			}
			NumFloats++;
			break;

		case grcEffect::VT_TEXTURE:
			{
				atString TextureTemplate = atString(VarInfo.m_TCPTemplate);
				atString diff = rstMaxUtility::GetTexMapName(m_ShaderInstanceData->GetTexture(NumTextures));
				atString alph;
				if(IsAlpha)
					alph = rstMaxUtility::GetTexMapName(m_ShaderInstanceData->GetTextureAlpha(NumTextures));
				
				if(IsPreset() && Locked)
				{
					grcTexture *pTmpVal = NULL;
					if(pPresetEntry)
						p_Shader->GetVar(*pPresetEntry,handle,pTmpVal);
					if(atString("none")==diff && pTmpVal)
					{
						char buffer[256];
						sprintf_s(buffer, 256, "%s (NOT RESOLVED)", pTmpVal->GetName());
						diff = buffer;
					}

					mp_DlgExtra->AddStaticString(Label,diff, VarInfo.m_Type);
				}
				else
				{
					if(IsAlpha)
						mp_DlgExtra->AddMapSelect(	Label,NumTextures,diff, alph, TextureTemplate);
					else
						mp_DlgExtra->AddMapSelect(	Label,NumTextures,diff,TextureTemplate);
				}
				NumTextures++;
				break;
			}
		case grcEffect::VT_VECTOR2:
			if(IsPreset() && Locked)
			{
				Vector2 Var;
				p_Shader->GetVar(*pPresetEntry, handle, Var);
				char Output[64];
				sprintf_s(Output, 64, "%0.2f %0.2f", Var[0], Var[1]);
				mp_DlgExtra->AddStaticString(Label, atString(Output), VarInfo.m_Type);
			}
			else
			{
				mp_DlgExtra->AddVector2Select(  Label,
					NumVector2s,
					VarInfo.m_UiMin,
					VarInfo.m_UiMax,
					VarInfo.m_UiStep);
			}
			NumVector2s++;
			break;
		case grcEffect::VT_VECTOR3:

			if(IsPreset() && Locked)
			{
				Vector3 Var;
				p_Shader->GetVar(*pPresetEntry, handle,Var);
				char Output[64];
				sprintf_s(Output, 64, "%0.2f %0.2f %0.2f",Var[0],Var[1],Var[2]);
				mp_DlgExtra->AddStaticString(Label, atString(Output), VarInfo.m_Type);
			}
			else
			{
				mp_DlgExtra->AddVector3Select(	Label,
					NumVector3s,
					VarInfo.m_UiMin,
					VarInfo.m_UiMax,
					VarInfo.m_UiStep);
			}
			NumVector3s++;
			break;

		case grcEffect::VT_VECTOR4: 

			if(IsPreset() && Locked)
			{
				Vector4 Var;
				p_Shader->GetVar(*pPresetEntry, handle,Var);
				char Output[64];
				sprintf_s(Output, 64, "%0.2f %0.2f %0.2f %0.2f",Var.x, Var.y, Var.z, Var.w);
				mp_DlgExtra->AddStaticString(Label,atString(Output), VarInfo.m_Type);
			}
			else
			{
				mp_DlgExtra->AddVector4Select(	Label,
					NumVector4s,
					VarInfo.m_UiMin,
					VarInfo.m_UiMax,
					VarInfo.m_UiStep);
			}
			NumVector4s++;
			break;

		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		default:
			mp_DlgExtra->AddStatic(atString(VarInfo.m_UiName));
			break;
		}
	}

	mp_DlgExtra->Create();

#else

	pShaderDlg = m_shaderValRoot->CreateShaderParamDialog(mp_MtlParam, p_Shader);

#endif
}

void RstMtlMax::ConstructShaderParamDialog(rageRstShaderDbEntry *dbEntry)
{
	const char* p_Name = dbEntry->m_MaterialPreset->GetFriendlyName();
	const MaterialPreset* p_MaterialPreset = dbEntry->m_MaterialPreset;

	RstMaxShaderDlg* pShaderDlg = NULL;

#if !SHADER_ATTR_TREE

	if(mp_DlgError)
	{
		delete mp_DlgError;
		mp_DlgError = NULL;
	}
	if(!m_debugParseSuccess)
	{
		mp_DlgError = new RstMaxErrorDlg(this, mp_MtlParam, m_parserErrorMsg);
		mp_DlgError->Create();
		return;
	}

	pShaderDlg = CreateShaderParamDialog();
	pShaderDlg->SetControlValueChangeFunctor(RstMax::NotifyValueChangedCallbacks);

	s32 i;
	s32 NumFloats = 0;
	s32 NumInts = 0;
	s32 NumTextures = 0;
	s32 NumVector2s = 0;
	s32 NumVector3s = 0;
	s32 NumVector4s = 0;
	s32 NumBools = 0;

	const MaterialPreset& fullPreset = *(p_MaterialPreset);
	bool IsAlpha = fullPreset.IsAlphaShader();
	const atArray<TypeBase*>& variableArray = fullPreset.GetVariableArray();

	for(i=0;i<variableArray.GetCount();i++)
	{
		TypeBase* variable = variableArray[i];
		bool Locked = variable->IsLocked();

		atString Label(variable->GetName());
		Label += ":";

		switch(variable->GetType())
		{
		case TypeBase::kFloat:
			{
				FloatType* floatType = static_cast<FloatType*>(variable);
				if(Locked)
				{
					float Var = floatType->GetValue();
					char Output[64];
					sprintf_s(Output,64,"%0.3f",Var);
					mp_DlgExtra->AddStaticString(Label, atString(Output), variable->GetType());
				}
				else
				{
					mp_DlgExtra->AddFloatSpinner(	Label,
						NumFloats,
						floatType->GetMinValue(),
						floatType->GetMaxValue(),
						floatType->GetStep());
				}
				NumFloats++;
				break;
			}

		case TypeBase::kTextureMap:
			{
				TexMapType* texMapType = static_cast<TexMapType*>(variable);

				if(Locked)
				{
					const atString& texture = texMapType->GetValue();
					mp_DlgExtra->AddStaticString(Label, texture, TypeBase::kTextureMap);
				}
				else
				{
					atString textureTemplate = texMapType->GetTextureTemplate();
					atString diff = rstMaxUtility::GetTexMapName(m_ShaderInstanceData->GetTexture(NumTextures));
					if(IsAlpha)
					{
						atString alpha = rstMaxUtility::GetTexMapName(m_ShaderInstanceData->GetTextureAlpha(NumTextures));					
						mp_DlgExtra->AddMapSelect(Label, NumTextures, diff, alpha, textureTemplate);
					}
					else
						mp_DlgExtra->AddMapSelect(Label, NumTextures, diff, textureTemplate);
				}
				NumTextures++;
				break;
			}
		case TypeBase::kVector2:
			{
				Vector2Type* vector2Type = static_cast<Vector2Type*>(variable);
				if(Locked)
				{
					const Vector2& Var = vector2Type->GetValue();
					char Output[64];
					sprintf_s(Output,64,"%0.2f %0.2f", Var[0], Var[1]);
					mp_DlgExtra->AddStaticString(Label, atString(Output), TypeBase::kVector2);
				}
				else
				{
					mp_DlgExtra->AddVector2Select(Label,
						NumVector2s,
						vector2Type->GetMinValue(),
						vector2Type->GetMaxValue(), 
						vector2Type->GetStep());
				}
				NumVector2s++;
				break;
			}
		case TypeBase::kVector3:
			{
				Vector3Type* vector3Type = static_cast<Vector3Type*>(variable);
				if(Locked)
				{
					const Vector3& Var = vector3Type->GetValue();
					char Output[64];
					sprintf_s(Output,64,"%0.2f %0.2f %0.2f",Var[0],Var[1],Var[2]);
					mp_DlgExtra->AddStaticString(Label, atString(Output), vector3Type->GetType());				}
				else
				{
					mp_DlgExtra->AddVector3Select(	Label,
						NumVector3s,
						vector3Type->GetMinValue(),
						vector3Type->GetMaxValue(), 
						vector3Type->GetStep());
				}
				NumVector3s++;
				break;
			}

		case TypeBase::kVector4: 
			{
				Vector4Type* vector4Type = static_cast<Vector4Type*>(variable);
				if(Locked)
				{
					const Vector4& Var = vector4Type->GetValue();
					char Output[64];
					sprintf_s(Output,64,"%0.2f %0.2f %0.2f",Var[0],Var[1],Var[2]);
					mp_DlgExtra->AddStaticString(Label,atString(Output), vector4Type->GetType());
				}
				else
				{
					mp_DlgExtra->AddVector4Select(	Label,
						NumVector4s,
						vector4Type->GetMinValue(),
						vector4Type->GetMaxValue(), 
						vector4Type->GetStep());
				}
				NumVector4s++;
				break;
			}

		default:
			break;
		}
	}

	mp_DlgExtra->Create();

	// Create the material preset dialog as well.
	ConstructMaterialPresetDialog(dbEntry);

#else

	pShaderDlg = m_shaderValRoot->CreateShaderParamDialog(mp_MtlParam, p_Shader);

#endif
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RstMtlMax::NumRefs()
{
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetTextureCount() +m_ShaderInstanceData->GetTextureAlphaCount();
#else
	if(NeedsRebuildReferences())
	{
		RebuildReferences();
	}
	return m_references.GetCount();
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle RstMtlMax::GetReference(int i)
{
	if((i<0)||(i>=NumRefs()))
	{
		//assert(false);
		return NULL;
	}

#if !SHADER_ATTR_TREE
	s32 TexCount = m_ShaderInstanceData->GetTextureCount();

	if(i < TexCount)
	{
		return m_ShaderInstanceData->GetTexture(i);
	}

	return m_ShaderInstanceData->GetTextureAlpha(i - TexCount);
#else
	if(NeedsRebuildReferences())
	{
		RebuildReferences();
	}
	assert(i<m_references.GetCount());
	if(i<m_references.GetCount())
		return m_references[i]->Value().m_Texture;
	return NULL;
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RstMtlMax::GetReferenceIndex(RefTargetHandle rtarg)
{
#if SHADER_ATTR_TREE
	assert((rtarg == NULL) || (rtarg->SuperClassID() == TEXMAP_CLASS_ID));

	ClearNodeSearchResult();
	FindShaderValueNode(m_shaderValRoot, true, SHADERVALNAME_IGNORE, CRstShaderValueNode::eShaderValTypeTexture, CRstShaderValueNode::eAllShaderValFlags, (void*)rtarg);
	if(GetLastNodeSearchResult().GetCount()<1)
		return NULL;
	return GetLastNodeSearchResult()[0]->RefTargetIndex();
#else 
	return 0;
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetReference(int i,RefTargetHandle rtarg)
{
	assert((rtarg == NULL) || (rtarg->SuperClassID() == TEXMAP_CLASS_ID));


#if !SHADER_ATTR_TREE
	assert(i<NumRefs());
	if(i>=NumRefs())
	{
		return;
	}
	assert(i < (m_ShaderInstanceData->GetTextureCount() << 1));
	s32 TexCount = m_ShaderInstanceData->GetTextureCount();

	if(i < TexCount)
	{
		m_ShaderInstanceData->SetTexture(i, (Texmap*)rtarg);
	}
	else
	{
		m_ShaderInstanceData->SetTextureAlpha(i - TexCount, (Texmap*)rtarg);
	}
#else
	if(NeedsRebuildReferences())
	{
		RebuildReferences();
	}
	assert(i<m_references.GetCount());
	if(i<m_references.GetCount())
		m_references[i]->Value().m_Texture = (Texmap*)rtarg;
#endif

	NotifyDependents(FOREVER,PART_ALL,REFMSG_CHANGE);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RstMtlMax::NumSubTexmaps()
{
	return NumRefs();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
Texmap* RstMtlMax::GetSubTexmap(int i)
{
	return (Texmap*)GetReference(i);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::UpdateMapName(s32 Index,const atString& r_MapName)
{
#if !SHADER_ATTR_TREE
	if(mp_DlgExtra)
		mp_DlgExtra->UpdateMapName(Index,r_MapName);
#else
	if(NeedsRebuildReferences())
	{
		RebuildReferences();
	}
	if(m_references[Index] && m_references[Index]->Parent() && m_references[Index]->Parent()->Dialog())
		m_references[Index]->Parent()->Dialog()->UpdateMapName(Index,r_MapName);
#endif
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetSubTexmap(int i,Texmap *m)
{
	//crashes way down in the max code if we dont do this...

	MtlBase* p_Texmap = GetActiveTexmap();

	if(p_Texmap && (p_Texmap != m))
		GetCOREInterface()->DeActivateTexture(p_Texmap,this);

	//
	if(m)
		ReplaceReference(i,m);
	else
		DeleteReference(i);

	if(mp_MtlDlg)
	{
		s32 TotalCount = NumSubTexmaps();

		if(i >= TotalCount)
			return;

		if(m)
			UpdateMapName(i,rstMaxUtility::GetTexMapName(m));
		else
			UpdateMapName(i,atString("none"));
	}

#if	MAX_RELEASE > MAX_RELEASE_R9
	texHandleValid.SetEmpty();
#if !RSTMTL_HW_SHADER
	DiscardTexHandles();
#endif // RSTMTL_HW_SHADER
#endif // MAX_RELEASE > MAX_RELEASE_R9
}			

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RstMtlMax::SubTexmapOn(int i)
{
	if((i==0) || (i==1))
		return 1;

	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::Reset()
{
	m_ShaderName = atString("");

#if !SHADER_ATTR_TREE
	s32 i,Count = m_ShaderInstanceData->GetTextureCount();

	for(i=0;i<Count;i++)
	{
		DeleteReference(i);
	}

	m_ShaderInstanceData->Reset();

#else
	for(int i=0;i<NumRefs();i++)
	{
		DeleteReference(i);
	}
#endif
	if(mp_MtlDlg)
	{
		mp_MtlDlg->Reset();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ParamDlg* RstMtlMax::CreateParamDlg(HWND MtlEditWnd,IMtlParams* p_MtlParam)
{
	m_MtlEditWnd = MtlEditWnd;
	mp_MtlParam = p_MtlParam;
	mp_MtlDlg = new RstMaxMtlDlg(const_cast<RstMtlMax*>(this), p_MtlParam);

	return mp_MtlDlg;
}

#if RSTMTL_HW_SHADER
#define MTL_HDR_CHUNK 0x4000
#else
#define MTL_HDR_CHUNK 0
#endif // RSTMTL_HW_SHADER

//////////////////////////////////////////////////////////////////////////////////////////////////////////
IOResult RstMtlMax::Load(ILoad *iload)
{
	
	int ChunkDepth = iload->CurChunkDepth();

	IOResult Res = iload->OpenChunk();
	
	bool bLoadStdMtl = true;
	for(int i =0; i <m_GtaRuleList.GetCount(); ++i)
	{
		if(strcmp(m_GtaRuleList[i][0], "LoadStdMtl") == 0)
		{
			if((strcmp(m_GtaRuleList[i][1], "false") == 0 || strcmp(m_GtaRuleList[i][1], "False") == 0))
				bLoadStdMtl = false;
		}
	}
	
	
	
	if(iload->CurChunkID() == MTL_HDR_CHUNK)
	{
		if(bLoadStdMtl)
		{
			try
			{
				Res = Mtl::Load(iload);
			}
			catch (...)
			{
				int NewChunkDepth = iload->CurChunkDepth();

				while(NewChunkDepth > ChunkDepth)
				{
					iload->CloseChunk();		

					NewChunkDepth--;
				}

				return IO_OK;
			}
		}
		else
		{
			if(sThrowWarning)
			{
				HWND hWnd = NULL;
				MessageBox(hWnd, "Standard Materials are not being loaded for this session.  If this is not your intention then change the LoadStdMtls entry in \\<your_max_dir>\\plugcfg\\GtaConfig.xml.  You will need to restart max for the changes to take affect.", "Standard Materials", MB_OK);
				sThrowWarning = false;
		
			}
		}
	}
	
	iload->CloseChunk();

	if(Res != IO_OK)
		return Res;

#if SHADER_ATTR_TREE
	DeleteHierarchy(m_shaderValRoot);
	m_shaderValRoot = NULL;
	m_shaderValRoot = new CRstShaderValueNode(this);
	SetNeedsRebuildReferences();
#endif

	iload->OpenChunk();

	assert(iload->CurChunkID() == 1);

	char* p_Data;

	s32 DataCount,DataSize;
	ULONG Read;
	iload->Read(&DataSize,4,&Read);

	if(Read != 4)
		return IO_ERROR;

	p_Data = new char[DataSize];
	iload->Read(p_Data,DataSize,&Read);

	if(Read != DataSize)
	{
		delete[] p_Data;
		return IO_ERROR;
	}

	bool TranslateType = false;
	char Filename[1024];
	fiDeviceMemory DevMemory;
	fiDeviceMemory::MakeMemoryFileName(Filename,1024,p_Data,DataSize,false,"rstmaxmtl load buffer");
	fiStream* p_MemStream = fiStream::Create(Filename,DevMemory);

	//version
	s32 Version;
	p_MemStream->ReadInt(&Version,1);

	if(Version == 1)
	{
		SetIsPreset(false);
		SetIsTwoSided(false);
	}
	else if(Version >= 2)
	{
		if(Version == 2)
		{
			TranslateType = true;
		}

		s32 Flags;
		p_MemStream->ReadInt(&Flags,1);

		if(Flags & FLAG_ISPRESET)
			SetIsPreset(true);
		else
			SetIsPreset(false);

		if(Flags & FLAG_ISTWOSIDED)
			SetIsTwoSided(true);
		else
			SetIsTwoSided(false);
		if(Version >= 4)
		{
			if(Flags & FLAG_OVERRIDETEXSCALE)
				m_OverrideTexScale = true;
			else
				m_OverrideTexScale = false;
		}

		if(Version >= 6)
		{
			if(Flags & FLAG_HASLIGHTING)
				SetLightingMode(true);
			else
				SetLightingMode(false);
		}

		if(Version >= 7)
		{
			if(Flags & FLAG_HASAMBLIGHTING)
				SetHasAmbLighting(true);
			else
				SetHasAmbLighting(false);
		}
		
	}
	else
	{
		p_MemStream->Close();
		delete[] p_Data;
		return IO_ERROR;
	}

	m_ShaderInstanceData->Reset();
	//num tex refs
#if SHADER_ATTR_TREE
	assert(NumRefs() == 0);
#endif

	p_MemStream->ReadInt(&DataCount,1);

	for(int i=0;i<DataCount;i++)
	{
#if !SHADER_ATTR_TREE
		m_ShaderInstanceData->AddTexture(NULL);
		m_ShaderInstanceData->AddTextureAlpha(NULL);
#else
		CRstShaderValueNode *lastNode = AppendShaderValueNode(new CRstShaderValueNode(this, atString(""), ShaderVar(), CRstShaderValueNode::eShaderValTypeTexture, 0, i));
		assert(lastNode);
		lastNode->Parent() = m_shaderValRoot;
#endif
	}
	//shader name
	ReadString(p_MemStream,m_ShaderName);

	// Make sure to create the proper shader instance type.
	if (rstMaxUtility::IsPreset(m_ShaderName))
	{
		ShaderInstanceData* newShaderInstanceData = new ShaderInstancePreset();

		// We need to do this again because the shader name comes after the texture count
		// so it won't be correct for material presets.
		for(int i=0;i<DataCount;i++)
		{
			newShaderInstanceData->AddTexture(NULL);
			newShaderInstanceData->AddTextureAlpha(NULL);
		}

		delete m_ShaderInstanceData;
		m_ShaderInstanceData = NULL;
		m_ShaderInstanceData = newShaderInstanceData;
	}
	
	// Retire gta_ shaders...
	m_ShaderName.Replace("gta_", "");

#if HACK_GTA4
	//convert this to a sps file...

	char Temp[1024];
	strcpy(Temp,m_ShaderName);
	char* p_Dot = strrchr(Temp,'.');

	if(p_Dot)
	{
		p_Dot++;

		if(stricmp(p_Dot,"fx") == 0)
		{
			*p_Dot = '\0';
			strcat(Temp,"sps");
			m_ShaderName = Temp;
			SetIsPreset(true);
		}
	}
#endif //HACK_GTA4

	//variables
	p_MemStream->ReadInt(&DataCount,1);
#if SHADER_ATTR_TREE
	int textureRefs = 0;
#endif
	if ( rstMaxUtility::IsPreset(m_ShaderName) )
	{
		//This is currently a temporary stopgap measure to highlight any missing presets
		//in the scene.  Until we get a proper system in place to fix up these references,
		//we will need to watch closely for any moved/deleted material presets.  Max files
		//can lose data since we won't know how to bind the saved data to.
		MaterialPreset* preset = MaterialPresetManager::GetCurrent().LoadPresetFromLibrary(m_ShaderName);
		if ( preset == NULL )
		{
			HWND hWnd = NULL;
			char buffer[2048];
			sprintf_s(buffer, "Unable to find material preset %s.  Please contact Tools.", m_ShaderName.c_str());
			MessageBox(hWnd, buffer, "Material Preset Not Found", MB_OK);
		}

		for(int i=0;i<DataCount;i++)
		{
			ShaderInstanceData::ShaderVar ReadShaderVar;
			atString VarName;

			ReadString(p_MemStream,VarName);
			p_MemStream->ReadInt((s32*)&ReadShaderVar.VarType,1);
			p_MemStream->ReadInt(&ReadShaderVar.Idx,1);
			p_MemStream->ReadInt(&DataSize,1);

			if(TranslateType)
			{
				ReadShaderVar.VarType = TranslateVarType(ReadShaderVar.VarType);
			}

			TypeBase::eVariableType presetVarType = (TypeBase::eVariableType) ReadShaderVar.VarType;

			switch(presetVarType)
			{
			case TypeBase::kFloat:

				if(DataSize != sizeof(f32))
					break;

				{	
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetFloatCount() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddFloat(NULL);
					}

					p_MemStream->ReadFloat(&m_ShaderInstanceData->GetFloat(ReadShaderVar.Idx),1);
					DataSize -= sizeof(f32);
				}

				break;

			case TypeBase::kInt:

				if(DataSize != sizeof(s32))
					break;

				{	
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetIntCount() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddInt(NULL);
					}

					p_MemStream->ReadInt(&m_ShaderInstanceData->GetInt(ReadShaderVar.Idx),1);
					DataSize -= sizeof(s32);
				}

				break;

			case TypeBase::kVector2:
				if(DataSize != (sizeof(f32) * 2))
					break;

				{
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetVector2Count() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddVector2(Vector2(0.0f,0.0f));
					}

					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector2(ReadShaderVar.Idx)[0]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector2(ReadShaderVar.Idx)[1]),1);
					DataSize -= (sizeof(f32) * 2);
				}

				break;
				case TypeBase::kVector3:

				if(DataSize != (sizeof(f32) * 3))
					break;

				{	
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetVector3Count() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddVector3(Vector3(0.0f,0.0f,0.0f));
					}

					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector3(ReadShaderVar.Idx)[0]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector3(ReadShaderVar.Idx)[1]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector3(ReadShaderVar.Idx)[2]),1);
					DataSize -= (sizeof(f32) * 3);
				}

				break;

				case TypeBase::kVector4: 

				if(DataSize != (sizeof(f32) * 4))
					break;

				{	
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetVector4Count() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddVector4(Vector4(0.0f));
					}

					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector4(ReadShaderVar.Idx)[0]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector4(ReadShaderVar.Idx)[1]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector4(ReadShaderVar.Idx)[2]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector4(ReadShaderVar.Idx)[3]),1);
					DataSize -= (sizeof(f32) * 4);
				}

				break;

				case TypeBase::kTextureMap:
			default:
				break;
			}

			m_ShaderInstanceData->GetVariableMap().Insert(VarName,ReadShaderVar);

			//seek across any extra data size left over
			s32 CurrentPos = p_MemStream->Tell();
			CurrentPos += DataSize;
			p_MemStream->Seek(CurrentPos);

		}
	}
	else
	{
		for(int i=0;i<DataCount;i++)
		{
			ShaderInstanceData::ShaderVar ReadShaderVar;
			atString VarName;

			ReadString(p_MemStream,VarName);
			p_MemStream->ReadInt((s32*)&ReadShaderVar.VarType,1);
			p_MemStream->ReadInt(&ReadShaderVar.Idx,1);
			p_MemStream->ReadInt(&DataSize,1);

			if(TranslateType)
			{
				ReadShaderVar.VarType = TranslateVarType(ReadShaderVar.VarType);
			}

#if SHADER_ATTR_TREE
			if(ReadShaderVar.Type!=grcEffect::VT_TEXTURE)
			{
				CRstShaderValueNode *lastNode = AppendShaderValueNode(new CRstShaderValueNode(this, VarName, ReadShaderVar, CRstShaderValueNode::gGrcEffectTypeLUT[ReadShaderVar.Type], 0, 0));
				assert(lastNode);
				lastNode->Parent() = m_shaderValRoot;
				switch(ReadShaderVar.Type)
				{
				case grcEffect::VT_FLOAT:
					p_MemStream->ReadFloat(&lastNode->Value().m_Float,1);
					break;
				case grcEffect::VT_INT_DONTUSE:
					p_MemStream->ReadInt(&lastNode->Value().m_Int,1);
					break;
				case grcEffect::VT_VECTOR2:
					{
						Vector2 &Vec = *lastNode->Value().m_Vector2;
						p_MemStream->ReadFloat(&(Vec[0]),1);
						p_MemStream->ReadFloat(&(Vec[1]),1);
					}
					break;
				case grcEffect::VT_VECTOR3:
					{
						Vector3 &Vec = *lastNode->Value().m_Vector3;
						p_MemStream->ReadFloat(&(Vec[0]),1);
						p_MemStream->ReadFloat(&(Vec[1]),1);
						p_MemStream->ReadFloat(&(Vec[2]),1);
					}
					break;
				case grcEffect::VT_VECTOR4: 
					{
						Vector4 &Vec = *lastNode->Value().m_Vector4;
						p_MemStream->ReadFloat(&(Vec[0]),1);
						p_MemStream->ReadFloat(&(Vec[1]),1);
						p_MemStream->ReadFloat(&(Vec[2]),1);
						p_MemStream->ReadFloat(&(Vec[3]),1);
					}
					break;
				case grcEffect::VT_BOOL_DONTUSE:
					p_MemStream->ReadInt(&lastNode->Value().m_Int, 1);
					break;
				}
			}
			else
			{
				CRstShaderValueNode *lastNode = FindSingleShaderValueNode(m_shaderValRoot,SHADERVALNAME_ALL,CRstShaderValueNode::eShaderValTypeTexture,CRstShaderValueNode::eAllShaderValFlags,NULL,textureRefs++);
				assert(lastNode);
				lastNode->Name() = VarName;
				lastNode->GetShaderVar() = ReadShaderVar;
			}
#else
			switch(ReadShaderVar.VarType)
			{
			case grcEffect::VT_FLOAT:

				if(DataSize != sizeof(f32))
					break;

				{	
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetFloatCount() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddFloat(NULL);
					}

					p_MemStream->ReadFloat(&m_ShaderInstanceData->GetFloat(ReadShaderVar.Idx),1);
					DataSize -= sizeof(f32);
				}

				break;

			case grcEffect::VT_INT_DONTUSE:

				if(DataSize != sizeof(s32))
					break;

				{	
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetIntCount() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddInt(NULL);
					}

					p_MemStream->ReadInt(&m_ShaderInstanceData->GetInt(ReadShaderVar.Idx),1);
					DataSize -= sizeof(s32);
				}

				break;

			case grcEffect::VT_VECTOR2:
				if(DataSize != (sizeof(f32) * 2))
					break;

				{
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetVector2Count() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddVector2(Vector2(0.0f,0.0f));
					}

					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector2(ReadShaderVar.Idx)[0]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector2(ReadShaderVar.Idx)[1]),1);
					DataSize -= (sizeof(f32) * 2);
				}

				break;
			case grcEffect::VT_VECTOR3:

				if(DataSize != (sizeof(f32) * 3))
					break;

				{	
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetVector3Count() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddVector3(Vector3(0.0f,0.0f,0.0f));
					}

					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector3(ReadShaderVar.Idx)[0]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector3(ReadShaderVar.Idx)[1]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector3(ReadShaderVar.Idx)[2]),1);
					DataSize -= (sizeof(f32) * 3);
				}

				break;

			case grcEffect::VT_VECTOR4: 

				if(DataSize != (sizeof(f32) * 4))
					break;

				{	
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetVector4Count() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddVector4(Vector4(0.0f));
					}

					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector4(ReadShaderVar.Idx)[0]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector4(ReadShaderVar.Idx)[1]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector4(ReadShaderVar.Idx)[2]),1);
					p_MemStream->ReadFloat(&(m_ShaderInstanceData->GetVector4(ReadShaderVar.Idx)[3]),1);
					DataSize -= (sizeof(f32) * 4);
				}

				break;

			case grcEffect::VT_BOOL_DONTUSE:
				if(DataSize != (sizeof(int)))
					break;

				{
					s32 Add = ReadShaderVar.Idx - m_ShaderInstanceData->GetBoolCount() + 1;
					for(int j=0; j<Add; j++)
					{
						m_ShaderInstanceData->AddBool(NULL);
					}

					int iTmpVal;
					p_MemStream->ReadInt(&iTmpVal, 1);
					m_ShaderInstanceData->GetBool(ReadShaderVar.Idx) = (bool)iTmpVal;
					DataSize -= (sizeof(int));
				}

				break;

			case grcEffect::VT_TEXTURE:
			case grcEffect::VT_MATRIX43:
			case grcEffect::VT_MATRIX44:
			default:
				break;
			}

			m_ShaderInstanceData->GetVariableMap().Insert(VarName,ReadShaderVar);
#endif //SHADER_ATTR_TREE

			//seek across any extra data size left over
			s32 CurrentPos = p_MemStream->Tell();
			CurrentPos += DataSize;
			p_MemStream->Seek(CurrentPos);

		}
	}

#if SHADER_ATTR_TREE
	RebuildReferences();
#endif
	
	// tex scale multiplier
	m_TexScaleMultiplier = 0.125f;
	if((Version > 4))
	{
		p_MemStream->ReadFloat(&m_TexScaleMultiplier,1);
		DataSize -= (sizeof(float));
	}

	if((Version >= 5))
	{
		// Render mode
		s32 hwEnabled;
		p_MemStream->ReadInt(&hwEnabled,1);
		if( hwEnabled )
			this->SetMtlFlag( MTL_HW_MAT_ENABLED, TRUE );
		else
			this->SetMtlFlag( MTL_HW_MAT_ENABLED, FALSE );
	}
	p_MemStream->Close();

	delete[] p_Data;
	
	iload->CloseChunk();
	return Res;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
IOResult RstMtlMax::Save(ISave *isave)
{

	IOResult Res = IO_OK;

	//standard material data

	isave->BeginChunk(MTL_HDR_CHUNK);
	Res = Mtl::Save(isave);
	isave->EndChunk();

	if(Res != IO_OK)
	{
		return Res;
	}

	//rstmtlmax data

#if !SHADER_ATTR_TREE
	s32 i,WriteSize;
#endif
	s32 DataCount,DataSize = GetDataSize();
	char* p_Data = new char[DataSize];

	isave->BeginChunk(1);

	char Filename[1024];
	fiDeviceMemory::MakeMemoryFileName(Filename,1024,p_Data,DataSize,false,"rstmaxmtl load buffer");
	fiDeviceMemory DevMemory;
	fiStream* p_MemStream = fiStream::Create(Filename,DevMemory);

	//version
	s32 Version = CURRENT_FILE_VERSION;
	p_MemStream->WriteInt(&Version,1);

	//flags
	s32 Flags = IsPreset ()? FLAG_ISPRESET : 0;
	Flags |= IsTwoSided() ? FLAG_ISTWOSIDED : 0;

	Flags |= m_OverrideTexScale ? FLAG_OVERRIDETEXSCALE : 0;
	Flags |= HasLighting() ? FLAG_HASLIGHTING : 0;
	Flags |= HasAmbLighting() ? FLAG_HASAMBLIGHTING : 0;

	p_MemStream->WriteInt(&Flags,1);

	//num tex refs
#if !SHADER_ATTR_TREE
	DataCount = m_ShaderInstanceData->GetTextureCount();
#else
	DataCount = NumRefs();
#endif
	p_MemStream->WriteInt(&DataCount,1);

	//shader name
	WriteString(p_MemStream,m_ShaderName);

	if ( rstMaxUtility::IsPreset(m_ShaderName) )
	{
		rageRstShaderDbEntry* pEntry = RstMax::GetDbEntry(m_ShaderName);
		MaterialPreset* pMaterialPreset = pEntry->m_MaterialPreset;

		//variables
#if !SHADER_ATTR_TREE
		DataCount = pMaterialPreset->GetNumUnlockedVariables();
#else
		ClearNodeSearchResult();
		DataCount = FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_ALL); // Find ALL variables
#endif
		p_MemStream->WriteInt(&DataCount,1);

		DataCount = m_ShaderInstanceData->GetVariableMap().GetNumSlots();

		for(i=0;i<DataCount;i++)
		{
			atMap<atString,ShaderInstanceData::ShaderVar>::Entry* p_Entry = m_ShaderInstanceData->GetVariableMap().GetEntry(i);

			while(p_Entry)
			{
				if (pMaterialPreset)
				{
					const TypeBase* pPresetVar = pMaterialPreset->GetVariable(p_Entry->key, true);

					// If it's a locked variable, ignore it because it's coming from the template so we don't need to save it.
					if (pPresetVar->IsLocked())
					{
						p_Entry = p_Entry->next;
						continue;
					}
				}

				WriteString(p_MemStream,p_Entry->key);
				p_MemStream->WriteInt((s32*)&p_Entry->data.VarType,1);
				p_MemStream->WriteInt(&p_Entry->data.Idx,1);

				TypeBase::eVariableType varType = (TypeBase::eVariableType) p_Entry->data.VarType;
				switch(varType)
				{
				case TypeBase::kFloat:
					WriteSize = 4;
					p_MemStream->WriteInt(&WriteSize,1);
					p_MemStream->WriteFloat(&m_ShaderInstanceData->GetFloat(p_Entry->data.Idx),1);
					break;
				case TypeBase::kInt:
					WriteSize = 4;
					p_MemStream->WriteInt(&WriteSize,1);
					p_MemStream->WriteInt(&m_ShaderInstanceData->GetInt(p_Entry->data.Idx),1);
					break;
				case TypeBase::kVector2:
					WriteSize = 8;
					p_MemStream->WriteInt(&WriteSize,1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector2(p_Entry->data.Idx)[0]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector2(p_Entry->data.Idx)[1]),1);
					break;
				case TypeBase::kVector3:
					WriteSize = 12;
					p_MemStream->WriteInt(&WriteSize,1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector3(p_Entry->data.Idx)[0]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector3(p_Entry->data.Idx)[1]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector3(p_Entry->data.Idx)[2]),1);
					break;
				case TypeBase::kVector4:
					WriteSize = 16;
					p_MemStream->WriteInt(&WriteSize,1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector4(p_Entry->data.Idx)[0]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector4(p_Entry->data.Idx)[1]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector4(p_Entry->data.Idx)[2]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector4(p_Entry->data.Idx)[3]),1);
					break;
				case TypeBase::kTextureMap:
				default:
					WriteSize = 0;
					p_MemStream->WriteInt(&WriteSize,1);
				}

				p_Entry = p_Entry->next;
			}
		}
	}
	else
	{
		//variables
#if !SHADER_ATTR_TREE
		DataCount = m_ShaderInstanceData->GetVariableMap().GetNumUsed();
#else
		ClearNodeSearchResult();
		DataCount = FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_ALL); // Find ALL variables
#endif
		p_MemStream->WriteInt(&DataCount,1);

		DataCount = m_ShaderInstanceData->GetVariableMap().GetNumSlots();

		for(i=0;i<DataCount;i++)
		{
			atMap<atString,ShaderInstanceData::ShaderVar>::Entry* p_Entry = m_ShaderInstanceData->GetVariableMap().GetEntry(i);

			while(p_Entry)
			{
				WriteString(p_MemStream,p_Entry->key);
				p_MemStream->WriteInt((s32*)&p_Entry->data.VarType,1);
				p_MemStream->WriteInt(&p_Entry->data.Idx,1);

				switch(p_Entry->data.VarType)
				{
				case grcEffect::VT_FLOAT:
					WriteSize = 4;
					p_MemStream->WriteInt(&WriteSize,1);
					p_MemStream->WriteFloat(&m_ShaderInstanceData->GetFloat(p_Entry->data.Idx),1);
					break;
				case grcEffect::VT_INT_DONTUSE:
					WriteSize = 4;
					p_MemStream->WriteInt(&WriteSize,1);
					p_MemStream->WriteInt(&m_ShaderInstanceData->GetInt(p_Entry->data.Idx),1);
					break;
				case grcEffect::VT_VECTOR2:
					WriteSize = 8;
					p_MemStream->WriteInt(&WriteSize,1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector2(p_Entry->data.Idx)[0]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector2(p_Entry->data.Idx)[1]),1);
					break;
				case grcEffect::VT_VECTOR3:
					WriteSize = 12;
					p_MemStream->WriteInt(&WriteSize,1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector3(p_Entry->data.Idx)[0]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector3(p_Entry->data.Idx)[1]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector3(p_Entry->data.Idx)[2]),1);
					break;
				case grcEffect::VT_VECTOR4:
					WriteSize = 16;
					p_MemStream->WriteInt(&WriteSize,1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector4(p_Entry->data.Idx)[0]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector4(p_Entry->data.Idx)[1]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector4(p_Entry->data.Idx)[2]),1);
					p_MemStream->WriteFloat(&(m_ShaderInstanceData->GetVector4(p_Entry->data.Idx)[3]),1);
					break;
				case grcEffect::VT_BOOL_DONTUSE:
					{
						WriteSize = 4;
						int iTmp = (int)m_ShaderInstanceData->GetBool(p_Entry->data.Idx);
						p_MemStream->WriteInt(&WriteSize, 1);
						p_MemStream->WriteInt(&iTmp, 1);
						break;
					}
				case grcEffect::VT_TEXTURE:
				case grcEffect::VT_MATRIX43:
				case grcEffect::VT_MATRIX44:
				default:
					WriteSize = 0;
					p_MemStream->WriteInt(&WriteSize,1);
				}

				p_Entry = p_Entry->next;
			}
		}
	}
	
#if SHADER_ATTR_TREE
	// DataCount stays the same
	for(RsAttrNodeArray::iterator iRef=m_references.begin();iRef<m_references.end();iRef++)
	{
		CRstShaderValueNode *currNode = (*iRef);
		assert(CRstShaderValueNode::eShaderValTypeTexture==currNode->Type());
		s32 WriteSize = 0;
		WriteString(p_MemStream,currNode->Name());
		p_MemStream->WriteInt((s32*)&currNode->GetShaderVar().Type,1);
		p_MemStream->WriteInt(&currNode->GetShaderVar().Idx,1);
		p_MemStream->WriteInt(&WriteSize,1);
	}
	ExportHierarchy(m_shaderValRoot, p_MemStream);
#endif
	// Tex scale multiplier
	p_MemStream->WriteFloat(&m_TexScaleMultiplier,1);

	// Render mode
	s32 hwEnabled = this->TestMtlFlag( MTL_HW_MAT_ENABLED );
	p_MemStream->WriteInt(&hwEnabled,1);

	//write it out
	p_MemStream->Close();

	s32 TellSize = p_MemStream->Tell();

	assert(TellSize == DataSize);

	ULONG Written;
	isave->Write(&DataSize,4,&Written);
	if(Written != 4)
	{
		delete[] p_Data;
		return IO_ERROR;
	}

	isave->Write(p_Data,DataSize,&Written);
	if(Written != DataSize)
		Res = IO_ERROR;

	delete[] p_Data;

	isave->EndChunk();
	return Res;
}
//#if !RSTMTL_HW_SHADER 
//////////////////////////////////////////////////////////////////////////////////////////////////////////
DWORD_PTR RstMtlMax::GetActiveTexHandle(TimeValue t,TexHandleMaker& thmaker)
{
	return NULL;
}

#define BMIDATA(x) ((UBYTE *)((BYTE *)(x) + sizeof(BITMAPINFOHEADER)))


struct TextOps {
	UBYTE colorOp;
	UBYTE colorAlphaSource;
	UBYTE colorScale;
	UBYTE alphaOp;
	UBYTE alphaAlphaSource;
	UBYTE alphaScale;
};


// TBD: these values have to be corrected.
#define TXOP_MODULATE 0
#define TXOP_ALPHABLEND 1
#define TXOP_OPACITY 2
// newly added in 3ds max 11
#define TXOP_OPACITY2 3
#define TXOP_DISABLE 4
#define TXOP_USETEX 5
#define TXOP_USETEX2	6

static TextOps txops[] = {
	{ GW_TEX_MODULATE, GW_TEX_TEXTURE, GW_TEX_SCALE_1X, GW_TEX_LEAVE, GW_TEX_TEXTURE, GW_TEX_SCALE_1X }, 
	{ GW_TEX_ALPHA_BLEND, GW_TEX_TEXTURE, GW_TEX_SCALE_1X, GW_TEX_LEAVE,  GW_TEX_TEXTURE, GW_TEX_SCALE_1X }, 
	{ GW_TEX_LEAVE, GW_TEX_TEXTURE, GW_TEX_SCALE_1X, GW_TEX_REPLACE, GW_TEX_TEXTURE, GW_TEX_SCALE_1X }, 
};

void RstMtlMax::SetTexOps(Material *mtl, int i, int type) {
	TextureInfo *ti = &mtl->texture[i];
	ti->colorOp = txops[type].colorOp;
	ti->colorAlphaSource = txops[type].colorAlphaSource;
	ti->colorScale = txops[type].colorScale;
	ti->alphaOp = txops[type].alphaOp;
	ti->alphaAlphaSource = txops[type].alphaAlphaSource;
	ti->alphaScale = txops[type].alphaScale;
	texOpsType[i] = type;
}

static Color whiteCol(1.0f, 1.0f, 1.0f);

void RstMtlMax::SetHWTexOps(IHardwareMaterial *pIHWMat3, int ntx, int type) {
	// TODO: Populate
	pIHWMat3->SetTextureColorArg(ntx, 1, D3DTA_TEXTURE);
	pIHWMat3->SetTextureColorArg(ntx, 2, D3DTA_CURRENT);
	pIHWMat3->SetTextureAlphaArg(ntx, 1, D3DTA_TEXTURE);
	pIHWMat3->SetTextureAlphaArg(ntx, 2, D3DTA_CURRENT);

	//SetBorderColor(ntx, pIHWMat3);
	switch (type) {
case TXOP_USETEX:
	pIHWMat3->SetTextureColorOp(ntx, D3DTOP_MODULATE ); 
	pIHWMat3->SetTextureAlphaOp(ntx, D3DTOP_SELECTARG2);
	break;
case TXOP_USETEX2:
	pIHWMat3->SetTextureColorOp(ntx, D3DTOP_MODULATE ); 
	pIHWMat3->SetTextureAlphaOp(ntx, D3DTOP_SELECTARG1);
	break;
case TXOP_MODULATE:
default:
	pIHWMat3->SetTextureColorOp(ntx, D3DTOP_MODULATE);
	pIHWMat3->SetTextureAlphaOp(ntx, D3DTOP_SELECTARG2);
	pIHWMat3->SetDiffuseColor(whiteCol);
	pIHWMat3->SetAmbientColor(whiteCol);
	break;
case TXOP_ALPHABLEND:
	pIHWMat3->SetTextureColorOp(ntx, D3DTOP_BLENDTEXTUREALPHA);
	pIHWMat3->SetTextureAlphaOp(ntx, D3DTOP_SELECTARG2);
	break;
case TXOP_OPACITY:
	pIHWMat3->SetTextureColorOp(ntx, D3DTOP_SELECTARG2);
	pIHWMat3->SetTextureAlphaOp(ntx, D3DTOP_SELECTARG1);
	break;
case TXOP_OPACITY2:
	pIHWMat3->SetTextureColorOp(ntx, D3DTOP_BLENDCURRENTALPHA);
	pIHWMat3->SetTextureAlphaOp(ntx, D3DTOP_SELECTARG1);
	break;
case TXOP_DISABLE: // Disables output from this texture stage and all stages with a higher index
	pIHWMat3->SetTextureColorOp(ntx, D3DTOP_DISABLE);
	pIHWMat3->SetTextureAlphaOp(ntx, D3DTOP_DISABLE);
	}
	pIHWMat3->SetTextureTransformFlag(ntx, D3DTTFF_COUNT2);
}

void RstMtlMax::DiscardTexHandles() {
	for (int i=0; i<2; i++) {
		if (texHandle[i]) {
			texHandle[i]->DeleteThis();
			texHandle[i] = NULL;
		}
	}
	texHandleValid.SetEmpty();
}

static void StuffAlpha(BITMAPINFO *bi, float amt, float opac, Color fill) {
	int w = bi->bmiHeader.biWidth;
	int h = bi->bmiHeader.biHeight;
	int npix = w*h;
	UBYTE *a = BMIDATA(bi);
	Color24 col;
	col.r = (int)(fill.r*255.0f);
	col.g = (int)(fill.g*255.0f);
	col.b = (int)(fill.b*255.0f);
	if (amt<1.0f) {
		int ia = int(amt*255.0f);
		int	ic = int( (1.0f-amt)*opac*255.0f);
		for (int j=0; j<npix; j++) {
			a[3] = ((( (a[0]+a[1]+a[2])/3 )*ia )>>8) + ic;
			a[0] = col.b;
			a[1] = col.g;
			a[2] = col.r;
			a += 4;
		}
	}
	else {
		for (int j=0; j<npix; j++) {
			a[3] = (a[0]+a[1]+a[2])/3;
			a[0] = col.b;
			a[1] = col.g;
			a[2] = col.r;
			a += 4;
		}
	}
}

static void StuffAlphaInto(BITMAPINFO *bia, BITMAPINFO *bi, float amt, float opac) {
	int w = bi->bmiHeader.biWidth;
	int h = bi->bmiHeader.biHeight;
	int npix = w*h;
	UBYTE *a = BMIDATA(bia);
	UBYTE *b = BMIDATA(bi);
	if (amt<1.0f) {
		int ia = int(amt*255.0f);
		int	ic = int( (1.0f-amt)*opac*255.0f);
		for (int j=0; j<npix; j++) {
			b[3] = ((( (a[0]+a[1]+a[2])/3 )*ia )>>8) + ic;
			a += 4;
			b += 4;
		}
	}
	else {
		for (int j=0; j<npix; j++) {
			b[3] = (a[0]+a[1]+a[2])/3;
			a += 4;
			b += 4;
		}
	}
}

float GetOpacity(TimeValue t)
{
	return 0.5f;
}

static MapUsageType usageIdxTbl[NTEXHANDLES] = {MAPUSAGE_DIFFUSE, MAPUSAGE_OPACITY,};
static bool IsCombinable(int i, int j)
{
	if( i < 0 || i >= NTEXHANDLES || j < 0 || j >= NTEXHANDLES)
		return false;
	int combinedUsage = (int)(usageIdxTbl[i]);
	combinedUsage |= (int)usageIdxTbl[j];
	if(combinedUsage == MAPUSAGE_DIFFUSE_OPACITY ||
		combinedUsage == MAPUSAGE_SPECULAR_COLOR_GLOSS ||
		combinedUsage == MAPUSAGE_SPECULAR_LEVEL_GLOSS ||
		combinedUsage == MAPUSAGE_BUMP_GLOSS ||
		combinedUsage == MAPUSAGE_SELF_ILLUM_GLOSS ||
		combinedUsage == MAPUSAGE_OPACITY_SPLEVEL)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// check whether b1 has the same size as b2.
static bool IsSameSize(BITMAPINFO* b1, BITMAPINFO* b2)
{
	if(!b1 || !b2)
	{
		return false;
	}
	return (b1->bmiHeader.biWidth == b2->bmiHeader.biWidth &&
		b1->bmiHeader.biHeight == b2->bmiHeader.biHeight);
}

// check the tx1 map use the same uv settings as the tx2 map;
static bool IsSameUV(Texmap* tx1, Texmap* tx2)
{
	if(!tx1 || !tx2)
	{
		return false;
	}

	if( tx1->GetTextureTiling() != tx2->GetTextureTiling() ||
		tx1->GetUVWSource() != tx2->GetUVWSource() )
	{
		return false;
	}

	if(tx1->GetUVWSource() == UVWSRC_EXPLICIT &&
		tx1->GetMapChannel() != tx2->GetMapChannel())
	{
		return false;
	}

	Matrix3 m1, m2;
	tx1->GetUVTransform(m1);
	tx2->GetUVTransform(m2);

	if(!(m1 == m2))
	{
		return false;
	}

	return true;
}

static void PackOpacityIntoAlpha(BITMAPINFO *bia, BITMAPINFO *bi, float amt, float opac) 
{
	int npix = bi->bmiHeader.biWidth*bi->bmiHeader.biHeight;
	UBYTE *a = BMIDATA(bia);
	UBYTE *b = BMIDATA(bi);
	if (amt<1.0f) {
		int	ic = int( (1.0f-amt)*opac*255.0f);
		float ia = amt/3.0f;
		for (int j=0; j<npix; j++) {
			b[3] = (UBYTE)((a[0] + a[1] + a[2])*ia + ic);
			a += 4;
			b += 4;
		}
	}
	else {
		for (int j=0; j<npix; j++) {
			b[3] = (a[0]+a[1]+a[2])/3;
			a += 4;
			b += 4;
		}
	}
}

static void BlendMapWithMaterialByAmount(BITMAPINFO* bi, const Color& matCol, float alpha)
{
	if(alpha < 0.0f || alpha > 1.0f || !bi)
		return;

	float beta = 1.0f - alpha;
	UBYTE R = (UBYTE)(matCol.r*255*beta);
	UBYTE G = (UBYTE)(matCol.g*255*beta);
	UBYTE B = (UBYTE)(matCol.b*255*beta);

	int npix = bi->bmiHeader.biWidth*bi->bmiHeader.biHeight;
	UBYTE *pPixel = BMIDATA(bi);

	for (int j=0; j<npix; j++)
	{
		pPixel[0] = (UBYTE)(pPixel[0]*alpha + B);
		pPixel[1] = (UBYTE)(pPixel[1]*alpha + G);
		pPixel[2] = (UBYTE)(pPixel[2]*alpha + R);
		pPixel += 4;
	}
}

static void PackOpacityAsAlpha(BITMAPINFO *bi, float amt, float opac, Color fill) 
{
	int npix = bi->bmiHeader.biWidth*bi->bmiHeader.biHeight;
	UBYTE *a = BMIDATA(bi);
	Color24 col;
	col.r = (int)(fill.r*255.0f);
	col.g = (int)(fill.g*255.0f);
	col.b = (int)(fill.b*255.0f);
	if (amt<1.0f) {
		int	ic = int( (1.0f-amt)*opac*255.0f);
		float ia = amt/3.0f;
		for (int j=0; j<npix; j++) {
			a[3] = (UBYTE)((a[0] + a[1] + a[2])*ia + ic);
			a[0] = col.b;
			a[1] = col.g;
			a[2] = col.r;
			a += 4;
		}
	}
	else {
		for (int j=0; j<npix; j++) {
			a[3] = (a[0]+a[1]+a[2])/3;
			a[0] = col.b;
			a[1] = col.g;
			a[2] = col.r;
			a += 4;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ULONG RstMtlMax::LocalRequirements(int subMtlNum)
{
	return MTLREQ_TRANSP | MTLREQ_TRANSP_IN_VP; 
}


static void GetMapUsageIndex(MapUsageType usage, int &a, int &b)
{

	a = b = -1;
	for(int i = 0; i < NTEXHANDLES; ++i)
	{
		if(usage & usageIdxTbl[i])
		{
			if(a < 0)
			{
				a = i;
			}
			else
			{
				b = i;
				break;
			}
		}
	}
}


enum MapValidState
{
	MAPVALIDSTATE_NOTEXIST,
	MAPVALIDSTATE_VALID,
	MAPVALIDSTATE_INVALID,
	MAPVALIDSTATE_SEEKING_COMPANION,
	MAPVALIDSTATE_FOUND_COMPANION,
	MAPVALIDSTATE_NO_COMPANION,
};

void RstMtlMax::UpdateBorderColor(BOOL bShader)
{
	/*
	Color borderCol;
	borderCol = GetDiffuse();
	borderColor[0] = D3DCOLOR_ARGB(0, int(255*borderCol.r+0.5),int(255*borderCol.g+0.5),int(255*borderCol.b+0.5));
	float fv = GetOpacity();
	fv *= (1.0f - (*maps)[stdIDToChannel[ ID_OP ]].amount);
	if(fv < 0){
		fv = 0.0f;
	}
	if(fv > 1.0f){
		fv = 1.0f;
	}
	borderColor[1] = D3DCOLOR_ARGB(int(255*fv + 0.5), 0,0,0);

	if(bShader){
		borderColor[2] = 0;
		//specular color
		borderCol = GetSpecular();
		borderColor[3] = D3DCOLOR_ARGB(0, int(255*borderCol.r+0.5),int(255*borderCol.g+0.5),int(255*borderCol.b+0.5));
		// glossiness
		fv = GetShininess();
		fv *= (1.0f - (*maps)[stdIDToChannel[ ID_SH ]].amount);
		borderColor[4] = D3DCOLOR_ARGB(int(255*fv + 0.5), 0,0,0);
		
		// specular level
		fv = GetShinStr();
		fv *= (1.0f - (*maps)[stdIDToChannel[ ID_SS ]].amount);
		int iv = (int)fv;
		fv = fv - iv;
		borderColor[5] = D3DCOLOR_ARGB(0, 0,int(255*fv + 0.5),iv);//use blue channel store integer part, green channel store fractional part.
		
		// Self-Illumination
		borderCol = IsSelfIllumColorOn()?GetSelfIllumColor(0, FALSE):GetDiffuse()*GetSelfIllum(0, FALSE);
		borderColor[6] = D3DCOLOR_ARGB(0, int(255*borderCol.r+0.5),int(255*borderCol.g+0.5),int(255*borderCol.b+0.5));
	}
	*/
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetupGfxMultiMapsForHW(TimeValue t, Material *mtl, MtlMakerCallback &cb, IHardwareMaterial *pIHWMat, BOOL bShader)
{
	IHardwareMaterial3 *pIHWMat3 = (IHardwareMaterial3 *)GetProperty(PROPID_HARDWARE_MATERIAL);
	DbgAssert(pIHWMat3);
	// if user switched from "best to "good" or "off", discard all texHandles.
	static BOOL sbLastShader = FALSE;
	if(sbLastShader != bShader){
		sbLastShader = bShader;
		if(!bShader){
			DiscardTexHandles();
		}
	}

	/* Get Texmap info and setup dx material info */
	UpdateBorderColor(bShader);
	Texmap *tx[NTEXHANDLES]; 
	int diffChan = stdIDToChannel[ ID_DI ];
#if !SHADER_ATTR_TREE
	tx[0] = (m_ShaderInstanceData->GetTextureCount() > 0) ? m_ShaderInstanceData->GetTexture(0) : NULL;
#else
	tx[0] = (Texmap *)GetReference(0);
#endif
	
	int opacChan = stdIDToChannel[ ID_OP ];
#if !SHADER_ATTR_TREE
	tx[1] = (m_ShaderInstanceData->GetTextureAlphaCount() > 0) ? m_ShaderInstanceData->GetTextureAlpha(0) : NULL;
#else
	tx[1] = (Texmap *)GetReference(1);
#endif
	

	float opac = tx[1]?1.0f:GetOpacity(t);
	if(!tx[0]){
		pIHWMat3->SetDiffuseColor(GetDiffuse(),opac);
		pIHWMat3->SetAmbientColor(GetAmbient(),opac);
	}

	/* Try to use cached data if valid */
	if (texHandleValid.InInterval(t))
	{
		pIHWMat3->SetNumTexStages(numTexHandlesUsed); //it will init tx stage as disable...good news.
		for (int i = 0; i < numTexHandlesUsed; i++) 
		{
			if (texHandle[i]) 
			{
				pIHWMat3->SetTexture(i, texHandle[i]->GetHandle(), i == 0 ? MAPUSAGE_DIFFUSE : MAPUSAGE_OPACITY); 
				//Texmap *tx = tx[i];
				// Kludge to pass in the TextureStage number
				mtl->texture[0].useTex = i;
				cb.GetGfxTexInfoFromTexmap(t, mtl->texture[0], tx[i]); 		
				SetHWTexOps(pIHWMat3, i, texOpsType[i]);
			}
		}
		return;
	}
	else 
	{	
		// if not in best mode, we discard all tex handles so that the map sequence will not be changed.
		if(!bShader)
			DiscardTexHandles();
		/* set the remaining valid texture handles.*/
		int nsupport = cb.NumberTexturesSupported(); 
		BITMAPINFO *bmi[NTEXHANDLES];
		bool bmiValidity[NTEXHANDLES];
		int nmaps = 0;
		MapValidState mapValidState[NTEXHANDLES];
		Interval  valid[NTEXHANDLES];
		int ntx = 0;
		int mapStage[NTEXHANDLES];
		for (int i = 0; i < NTEXHANDLES; i++) 
		{
			if (tx[i])
			{
				nmaps++;
				mapValidState[i] = MAPVALIDSTATE_SEEKING_COMPANION;
			}
			else
			{
				mapValidState[i] = MAPVALIDSTATE_NOTEXIST;
			}
			bmi[i] = NULL;
			valid[i].SetEmpty();
			mapStage[i] = -1;
		}

		if (nmaps == 0)
		{
			DiscardTexHandles();
			pIHWMat3->SetNumTexStages(0);
			return;
		}
		texHandleValid.SetInfinite();
		// loop through the existing texHandles
		bool bValidTexCache = numTexHandlesUsed?true:false;
		for (int i = 0; i < numTexHandlesUsed; ++i) 
		{
			if(texHandle[i] && texHandleValidArray[i].InInterval(t))
			{
				int a, b;
				GetMapUsageIndex(mapsUsage[i], a, b);
				
				if(a >= 0 && b >= 0 && tx[a] && tx[b])// if the usage is a combined type. set it.
				{
					mapValidState[a] = mapValidState[b] = MAPVALIDSTATE_VALID;

				}
				else if(a >= 0 && b < 0 && tx[a])
				{	
					mapValidState[a] = MAPVALIDSTATE_SEEKING_COMPANION;
				}
				else // the cached info doesn't match the existing info, clear all.
				{
					DiscardTexHandles();
					bValidTexCache = false;
					break;
				}
			}
		}

		
		if(bValidTexCache)
		{
			for(int i = 0; i < NTEXHANDLES; ++i)
			{
				if(mapValidState[i] == MAPVALIDSTATE_SEEKING_COMPANION)
				{
					for(int j = i + 1; j < NTEXHANDLES; ++j)
					{
						if(mapValidState[j] == MAPVALIDSTATE_SEEKING_COMPANION 
							|| mapValidState[j] == MAPVALIDSTATE_INVALID)
						{
							if(tx[i] && tx[j] && IsCombinable(i,j) && IsSameUV(tx[i], tx[j]))
							{
								mapValidState[j] = mapValidState[i] = MAPVALIDSTATE_FOUND_COMPANION;
								break;
							}
						}
					}
					if(mapValidState[i] == MAPVALIDSTATE_SEEKING_COMPANION) //not found any companion to combine with...
					{
						mapValidState[i] = MAPVALIDSTATE_NO_COMPANION;
					}
				}
			}

			for (int i = 0; i < numTexHandlesUsed; ++i) // numTexHandlesUsed is less than nmaps.
			{
				if(texHandle[i] && texHandleValidArray[i].InInterval(t))
				{
					int a, b;
					GetMapUsageIndex(mapsUsage[i], a, b);
					BOOL bSet = FALSE;
					if(a >= 0 && b >= 0)
					{
						if(tx[a] && tx[b])
						{		
							tx[a] = tx[b] = NULL;
							bSet = TRUE;
						}
						else
						{
							DbgAssert(0);// tx[a,b] must be exists, or sth must be wrong.
							texHandle[i]->DeleteThis();
							texHandle[i] = NULL;
							texHandleValidArray[i].SetEmpty();
							mapsUsage[i] = MAPUSAGE_UNDEFINED;
						}
					}
					else if(a >= 0 )
					{
						if(mapValidState[a] == MAPVALIDSTATE_NO_COMPANION)
						{
							DbgAssert(tx[a]);
							tx[a] = NULL;
							bSet = TRUE;
						}
						else // free the texhandle for later combination.
						{
							texHandle[i]->DeleteThis();
							texHandle[i] = NULL;
							texHandleValidArray[i].SetEmpty();
							mapsUsage[i] = MAPUSAGE_UNDEFINED;
						}
					}

					if(bSet)
					{
						texOpsType[ntx] = texOpsType[i];
						mapsUsage[ntx] = mapsUsage[i];
						useSubForTex[ntx] = useSubForTex[i];
						texHandleValidArray[ntx] = texHandleValidArray[i];
						texHandleValid &= texHandleValidArray[ntx];
						texHandle[ntx] = texHandle[i];
						if(ntx != i)
						{
							mapsUsage[i] = MAPUSAGE_UNDEFINED;
							useSubForTex[i] = -1;
							texHandleValidArray[i].SetEmpty();
							texHandle[i] = NULL;
						}
						++ ntx;
					}
				}
			}
		}

		// first to retrieve all the dib bitmaps.
		int oldNtx = ntx;
		bool bMono[NTEXHANDLES] = {false, true};
		for(int i = 0; i < NTEXHANDLES; ++i)
		{
			if(tx[i])
			{
				if (nsupport > ntx) 
				{
					bmi[i] = tx[i]->GetVPDisplayDIB(t, cb, valid[i],  bMono[i]);
					if(bmi[i])
					{
						mapStage[i] = ntx;
						++ ntx;
					}
					else
					{
						mapStage[i] = -1;
					}
				}
			}
		}

		// diffuse and opacity
		float diffAmount = 1.0f;
		if (tx[0]) 
		{
			if(diffAmount < 1.0f){
				BlendMapWithMaterialByAmount(bmi[0], GetDiffuse(), diffAmount);
			}
			texOpsType[mapStage[0]] = TXOP_USETEX;

			if (bmi[0])
			{
				useSubForTex[mapStage[0]] = diffChan;
				mapsUsage[mapStage[0]] = MAPUSAGE_DIFFUSE;
			}
		}
		bool bContainOpacity = false;

		// Im forcing the opacity amount to 1.0f here 
		if (tx[0] && tx[1] && IsSameUV(tx[0], tx[1]) && IsSameSize(bmi[0], bmi[1])) 
		{
			if (bmi[1] && bmi[0])
			{
				
				PackOpacityIntoAlpha(bmi[1], bmi[0], 1.0f, GetOpacity(t));
				valid[0] &= valid[1];
				bContainOpacity = true;
				texOpsType[mapStage[0]] = TXOP_USETEX2;
				bmiValidity[0] = true;
				bmiValidity[1] = true;
#if HACK_GTA4
				// Freeing can cause crashes while running in debug (not in release)
				// Not sure why we are freeing it anyway, not our reposonsibility as far 
				// as I can tell and potentially dangerous.
				
				//free(bmi[1]);
				//bmi[1] = NULL;
#else
				free(bmi[1]);
				bmi[1] = NULL;
#endif // HACK_GTA4

			}
		}
		else if (bmi[1]) 
		{
			
			PackOpacityAsAlpha(bmi[1], 1.0f, GetOpacity(t),whiteCol);
			useSubForTex[mapStage[1]] = opacChan;
			texOpsType[mapStage[1]] = TXOP_OPACITY;
			mapsUsage[mapStage[1]] = MAPUSAGE_OPACITY;
			bmiValidity[1] = true;
		}

		if (bmi[0])	
		{
			mapsUsage[mapStage[0]] = bContainOpacity?MAPUSAGE_DIFFUSE_OPACITY: MAPUSAGE_DIFFUSE;
			bmiValidity[0] = true;
		}

		int stageCount = oldNtx;
		for(int i = 0; i < NTEXHANDLES; ++i)
		{
			if(bmi[i])
			{
				texHandleValid &= valid[i];
				texHandle[stageCount] = cb.MakeHandle(bmi[i]); 
				bmi[i] = NULL;
				mapsUsage[stageCount] = mapsUsage[mapStage[i]];// stageCount always <= mapStage[i], so the following lines won't cause problem.
				mapsUsage[stageCount] = (MapUsageType)((int)mapsUsage[stageCount]|(int)MAPUSAGE_SUPPORT_PROGRAMMABLE_PIPELINE);
				texHandleValidArray[stageCount] = valid[i];
				useSubForTex[stageCount] = useSubForTex[mapStage[i]]; 
				texOpsType[stageCount] = texOpsType[mapStage[i]]; // the same as above.
				++ stageCount;
			}
		}
		pIHWMat3->SetNumTexStages(stageCount);
		for(int i = 0; i < stageCount; ++i)
		{	
			pIHWMat3->SetTexture(i, texHandle[i]->GetHandle(), mapsUsage[i]); // no usage info.
			//Texmap *tx = (*maps)[useSubForTex[i]].map;
			// Kludge to pass in the TextureStage number
			mtl->texture[0].useTex = i;
			cb.GetGfxTexInfoFromTexmap(t, mtl->texture[0], tx[i]);
			SetHWTexOps(pIHWMat3, i, texOpsType[i]);
		}

		DbgAssert(stageCount < nsupport);
		
		numTexHandlesUsed = stageCount;
		// clear the unused states
		for(int i = 0; i < NTEXHANDLES; ++ i)
		{
			if(!bmiValidity[i])
			{
				mapsUsage[i] = MAPUSAGE_UNDEFINED;
				useSubForTex[i] = -1;
				texOpsType[i] = TXOP_DISABLE;
				if (texHandle[i]) 
				{
					DbgAssert(0);

					texHandle[i]->DeleteThis();
					texHandle[i] = NULL;
				}
				texHandleValidArray[i].SetEmpty();
			}
		}

		/* This appears to be flawed.  It will Delete the alpha texture
			handle if the diffuse is unresolved
		numTexHandlesUsed = stageCount;
		// clear the unused states
		for(int i = numTexHandlesUsed; i < NTEXHANDLES; ++ i)
		{
			mapsUsage[i] = MAPUSAGE_UNDEFINED;
			useSubForTex[i] = -1;
			texOpsType[i] = TXOP_DISABLE;
			if (texHandle[i]) 
			{
				DbgAssert(0);
				
				texHandle[i]->DeleteThis();
				texHandle[i] = NULL;
			}
			texHandleValidArray[i].SetEmpty();
		}
		*/
	}// !InInterval(t)

	return;
}
/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////
ULONG RstMtlMax::LocalRequirements(int subMtlNum)
{ 
	if ( IsAlphaShader() )
		return MTLREQ_TRANSP_IN_VP; 

	return 0;
}
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetupGfxMultiMaps(TimeValue t,Material *mtl,MtlMakerCallback &cb)
{
	if(m_hwRendering == ShadingModel::Hardware)
		return;

	IHardwareMaterial *pIHWMat = (IHardwareMaterial *)GetProperty(PROPID_HARDWARE_MATERIAL);
	//BOOL bShader = (GetIViewportShadingMgr()->GetViewportShadingLimits() == IViewportShadingMgr::kBest);
	if (pIHWMat) {
		SetupGfxMultiMapsForHW(t, mtl,cb, pIHWMat, true);
		//return;
	}
	else {


		if (texHandleValid.InInterval(t)) {
			mtl->texture.SetCount(numTexHandlesUsed);
			for (int i=0; i<numTexHandlesUsed; i++) {
				if (texHandle[i]) 
				{
					mtl->texture[i].textHandle = texHandle[i]->GetHandle();
					Texmap *tx = NULL;

#if !SHADER_ATTR_TREE
					if(i==0)
						tx = (m_ShaderInstanceData->GetTextureCount() > 0) ? m_ShaderInstanceData->GetTexture(0) : NULL;
					else
						tx = (m_ShaderInstanceData->GetTextureAlphaCount() > 0) ? m_ShaderInstanceData->GetTextureAlpha(0) : NULL;
#else
					tx = (Texmap *)GetReference(i);
#endif

					cb.GetGfxTexInfoFromTexmap(t, mtl->texture[i], tx ); 		
					SetTexOps(mtl,i,texOpsType[i]);
				}
			}
			return;
		}
		else {
			DiscardTexHandles();
		}

		Texmap *tx[2];
#if !SHADER_ATTR_TREE
		tx[0] = (m_ShaderInstanceData->GetTextureCount() > 0) ? m_ShaderInstanceData->GetTexture(0) : NULL;
		tx[1] = (m_ShaderInstanceData->GetTextureAlphaCount() > 0) ? m_ShaderInstanceData->GetTextureAlpha(0) : NULL;
#else
		tx[0] = (Texmap *)GetReference(0);
		tx[1] = (Texmap *)GetReference(1);
#endif

		int nsupport = cb.NumberTexturesSupported();

		BITMAPINFO *bmi[NTEXHANDLES];

		int nmaps=0;
		for (int i=0; i<NTEXHANDLES; i++) {
			if (tx[i]) nmaps ++;
			bmi[i] = NULL;
		}
		mtl->texture.SetCount(nmaps);
		if (nmaps==0) 
			return;
		for (int i=0; i<nmaps; i++)
			mtl->texture[i].textHandle = NULL;
		texHandleValid.SetInfinite();
		Interval  valid;
		BOOL needDecal = FALSE;
		int ntx = 0;
		int op = 0; 

		int forceW = 0;
		int forceH = 0;
		if (tx[0]) {
			cb.GetGfxTexInfoFromTexmap(t, mtl->texture[0], tx[0]); 		
			TextureInfo &ti = mtl->texture[0];
			if (ti.tiling[0]==GW_TEX_NO_TILING||ti.tiling[1]==GW_TEX_NO_TILING)
				needDecal = TRUE;
			op = needDecal?TXOP_ALPHABLEND:TXOP_MODULATE;
		bmi[0] = tx[0]->GetVPDisplayDIB(t,cb,valid,FALSE); 
			if (bmi[0]) {
				texHandleValid &= valid;
				ntx = 1;
				forceW = bmi[0]->bmiHeader.biWidth;
				forceH = bmi[0]->bmiHeader.biHeight;
			}
		}

		if (tx[1]) {
			cb.GetGfxTexInfoFromTexmap(t, mtl->texture[ntx], tx[1]); 		
			
			if (nsupport>ntx) {
				bmi[1] = tx[1]->GetVPDisplayDIB(t,cb,valid,TRUE); 
				if (bmi[1]) {
					texHandleValid &= valid;
					StuffAlpha(bmi[1], 1.0f, GetOpacity(t),Color(1.0f,0.0f,0.0f));
					texHandle[ntx] = cb.MakeHandle(bmi[1]); 
					bmi[1] = NULL; 
					mtl->texture[ntx].textHandle = texHandle[ntx]->GetHandle();
					SetTexOps(mtl,ntx,TXOP_OPACITY);
					ntx++;
				}
			}
			else {
				if (!needDecal) {
					TextureInfo ti;
					// Not really correct to combine channels for different UV's but what the heck.
					bmi[1] = tx[1]->GetVPDisplayDIB(t,cb,valid,TRUE, forceW, forceH); 
					if (bmi[1]) {
						texHandleValid &= valid;
						StuffAlphaInto(bmi[1], bmi[0], 1.0f, GetOpacity(t));
						op = TXOP_OPACITY;
						free(bmi[1]);
						bmi[1] = NULL;
					}
				}
			}
		}

		if (bmi[0])	
		{
			texHandle[0] = cb.MakeHandle(bmi[0]); 
			bmi[0] = NULL; 
			mtl->texture[0].textHandle = texHandle[0]->GetHandle();
			SetTexOps(mtl,0,op);
		}

		mtl->texture.SetCount(ntx);
		numTexHandlesUsed = ntx;
	}
}
//#endif // RSTMTL_HW_SHADER 
//////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 RstMtlMax::GetDataSize()
{
#if !SHADER_ATTR_TREE
	s32 i,Count;
#endif
	s32 Size = 0;

	//version
	Size += 4;

	//flags
	Size += 4;

	//num tex refs
	Size += 4;

	//shader name
	Size += m_ShaderName.GetLength() + 4;

	//shader variables
#if !SHADER_ATTR_TREE
	Count = m_ShaderInstanceData->GetVariableMap().GetNumSlots();

#endif
	Size += 4;

#if !SHADER_ATTR_TREE

	if ( rstMaxUtility::IsPreset(m_ShaderName) )
	{
		rageRstShaderDbEntry* pEntry = RstMax::GetDbEntry(m_ShaderName);
		MaterialPreset* pMaterialPreset = pEntry->m_MaterialPreset;

		for(i=0;i<Count;i++)
		{
			atMap<atString,ShaderInstanceData::ShaderVar>::Entry* p_Entry = m_ShaderInstanceData->GetVariableMap().GetEntry(i);

			while(p_Entry)
			{
				// Don't account size for locked variables, we aren't saving them.
				if (pMaterialPreset)
				{
					const TypeBase* pPresetVar = pMaterialPreset->GetVariable(p_Entry->key, true);
					if (pPresetVar->IsLocked())
					{
						p_Entry = p_Entry->next;
						continue;
					}
				}

				Size += p_Entry->key.GetLength() + 12; //key + type + index
				Size += 4; // data size


				TypeBase::eVariableType type = (TypeBase::eVariableType) p_Entry->data.VarType;

				switch(type)
				{
				case TypeBase::kFloat:
				case TypeBase::kInt:
					Size += 4;
					break;
				case TypeBase::kVector2:
					Size += 8;
					break;
				case TypeBase::kVector3:
					Size += 12;
					break;
				case TypeBase::kVector4: 
					Size += 16;
					break;
				case TypeBase::kTextureMap:
				default:
					break;
				}

				p_Entry = p_Entry->next;
			}
		}
	}
	else
	{
		for(i=0;i<Count;i++)
		{
			atMap<atString,ShaderInstanceData::ShaderVar>::Entry* p_Entry = m_ShaderInstanceData->GetVariableMap().GetEntry(i);

			while(p_Entry)
			{
				Size += p_Entry->key.GetLength() + 12; //key + type + index
				Size += 4; // data size

				switch(p_Entry->data.VarType)
				{
				case grcEffect::VT_FLOAT:
				case grcEffect::VT_INT_DONTUSE:
					Size += 4;
					break;
				case grcEffect::VT_VECTOR2:
					Size += 8;
					break;
				case grcEffect::VT_VECTOR3:
					Size += 12;
					break;
				case grcEffect::VT_VECTOR4: 
					Size += 16;
					break;
				case grcEffect::VT_BOOL_DONTUSE:
					Size += 4;
				case grcEffect::VT_TEXTURE:
				case grcEffect::VT_MATRIX43:
				case grcEffect::VT_MATRIX44:
				default:
					break;
				}

				p_Entry = p_Entry->next;
			}
		}
	}
#else
	Size += GetHierarchyDataSize(m_shaderValRoot);
#endif

	// tex scale multiplier
	Size += 4;

	// Render mode
	Size +=4;


	return Size;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::Shade(ShadeContext& sc)
{
	// Set the material editor preview to the the first texture in the texmap array (if one exists)
	AColor mval(1.0f,1.0f,1.0f,0.5f);

	if((NumRefs() > 0) && (GetReference(0) != NULL))
	{
		mval = ((Texmap *)GetReference(0))->EvalColor(sc);
	}

	sc.out.c = sc.DiffuseIllum() * mval;
	sc.out.t = Color(0.0f,0.0f,0.0f);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RstMtlMax::EvalColorStdChannel(ShadeContext &  sc,int  stdID,Color & outClr)
{
	outClr.r = outClr.g = outClr.b = 0;

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool RstMtlMax::EvalMonoStdChannel(ShadeContext &  sc,int  stdID,float& outVal)
{
	outVal = 0.0f;

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 RstMtlMax::GetVariableCount()
{	
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		return variables.GetCount(); 
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
		{
			return -1;
		}

		return p_Shader->GetInstancedVariableCount();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::GetVariableName(s32 i,atString& r_name)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		r_name = variables[i]->GetName(); 
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
		{
			return;
		}

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);

		r_name = VarInfo.m_UiName;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::GetShaderParameterName(s32 i,atString& r_name)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		r_name = variables[i]->GetName(); 
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
		{
			return;
		}

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);

		r_name = VarInfo.m_Name;
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
TypeBase::eVariableType RstMtlMax::GetPresetVariableType(s32 i)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		return variables[i]->GetType();
	}

	return TypeBase::kUndefined;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
grcEffect::VarType RstMtlMax::GetVariableType(s32 i)
{
	grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

	if(!p_Shader)
	{
		return grcEffect::VT_INT_DONTUSE;
	}

	grmVariableInfo VarInfo;

	p_Shader->GetInstancedVariableInfo(i,VarInfo);

	return VarInfo.m_Type;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ShaderInstanceData::ShaderVar* RstMtlMax::GetVar(const atString& name)
{
	ShaderInstanceData::ShaderVar* p_OldVar;

#if !SHADER_ATTR_TREE
	p_OldVar = m_ShaderInstanceData->GetVariableMap().Access(name);
#else
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,name);
	p_OldVar = &pNode->GetShaderVar();
#endif

	return p_OldVar;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ShaderInstanceData::ShaderVar* RstMtlMax::GetVar(const grmVariableInfo& r_VarInfo)
{
	ShaderInstanceData::ShaderVar* p_OldVar;

#if !SHADER_ATTR_TREE
	p_OldVar = m_ShaderInstanceData->GetVariableMap().Access(r_VarInfo.m_Name);
	if(!p_OldVar)
	{
		p_OldVar = m_ShaderInstanceData->GetVariableMap().Access(r_VarInfo.m_ActualName);
	}
#else
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,r_VarInfo.m_Name);
	p_OldVar = &pNode->GetShaderVar();
#endif

	return p_OldVar;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::GetVariable(s32 i,atString& r_texmap)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;
	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();
		p_OldVar = GetVar(variables[i]->GetVariableName());

		if(!p_OldVar || (p_OldVar->VarType != varType))
		{
			m_ShaderInstanceData->MapInstanceData(this, p_Entry);
		}
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
		{
			return;
		}

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
		{
			m_ShaderInstanceData->MapInstanceData(this, p_Entry);
		}
	}

#if !SHADER_ATTR_TREE
	r_texmap = rexMaxUtility::GetTexMapFullPath(m_ShaderInstanceData->GetTexture(p_OldVar->Idx));
#else
	ShaderVar* p_OldVar;
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	p_OldVar = &pNode->GetShaderVar();

	if(!p_OldVar || (p_OldVar->Type != VarInfo.m_Type))
	{
		VerifyData();
	}

	r_texmap = rexMaxUtility::GetTexMapFullPath(pNode->Value().m_Texture);
#endif
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::GetVariable(s32 i,f32& r_float)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;
	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();
		p_OldVar = GetVar(variables[i]->GetVariableName());

		if(!p_OldVar || (p_OldVar->VarType != varType))
		{
			m_ShaderInstanceData->MapInstanceData(this, p_Entry);
		}
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE
	r_float = m_ShaderInstanceData->GetFloat(p_OldVar->Idx);
#else
	ShaderVar* p_OldVar;
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	p_OldVar = &pNode->GetShaderVar();

	if(!p_OldVar || (p_OldVar->Type != VarInfo.m_Type))
	{
		VerifyData();
	}

	r_float = pNode->Value().m_Float;
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::GetVariable(s32 i,s32& r_int)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;
	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();
		p_OldVar = GetVar(variables[i]->GetVariableName());

		if(!p_OldVar || (p_OldVar->VarType != varType))
		{
			m_ShaderInstanceData->MapInstanceData(this, p_Entry);
		}
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE
	r_int = m_ShaderInstanceData->GetInt(p_OldVar->Idx);
#else
	ShaderVar* p_OldVar;
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	p_OldVar = &pNode->GetShaderVar();

	if(!p_OldVar || (p_OldVar->Type != VarInfo.m_Type))
	{
		VerifyData();
	}

	r_int = pNode->Value().m_Int;
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::GetVariable(s32 i,Vector2& r_vec)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;
	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();
		p_OldVar = GetVar(variables[i]->GetVariableName());

		if(!p_OldVar || (p_OldVar->VarType != varType))
		{
			m_ShaderInstanceData->MapInstanceData(this, p_Entry);
		}
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);
 		//VerifyData();

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE
	r_vec = m_ShaderInstanceData->GetVector2(p_OldVar->Idx);
#else
	ShaderVar* p_OldVar;
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	p_OldVar = &pNode->GetShaderVar();

	if(!p_OldVar || (p_OldVar->Type != VarInfo.m_Type))
	{
		VerifyData();
	}

	r_vec = *pNode->Value().m_Vector2;
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::GetVariable(s32 i,Vector3& r_vec)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;
	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();
		p_OldVar = GetVar(variables[i]->GetVariableName());

		if(!p_OldVar || (p_OldVar->VarType != varType))
		{
			m_ShaderInstanceData->MapInstanceData(this, p_Entry);
		}
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE

	r_vec = m_ShaderInstanceData->GetVector3(p_OldVar->Idx);
#else
	ShaderVar* p_OldVar;
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	p_OldVar = &pNode->GetShaderVar();

	if(!p_OldVar || (p_OldVar->Type != VarInfo.m_Type))
	{
		VerifyData();
	}

	r_vec = *pNode->Value().m_Vector3;
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::GetVariable(s32 i,Vector4& r_vec)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;
	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();
		p_OldVar = GetVar(variables[i]->GetVariableName());

		if(!p_OldVar || (p_OldVar->VarType != varType))
		{
			m_ShaderInstanceData->MapInstanceData(this, p_Entry);
		}
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}
#if !SHADER_ATTR_TREE
	r_vec = m_ShaderInstanceData->GetVector4(p_OldVar->Idx);
#else
	ShaderVar* p_OldVar;
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	p_OldVar = &pNode->GetShaderVar();

	if(!p_OldVar || (p_OldVar->Type != VarInfo.m_Type))
	{
		VerifyData();
	}

	r_vec = *pNode->Value().m_Vector4;
#endif
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::GetVariable(rage::s32 i, bool &r_bool)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;
	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();
		p_OldVar = GetVar(variables[i]->GetVariableName());

		if(!p_OldVar || (p_OldVar->VarType != varType))
		{
			m_ShaderInstanceData->MapInstanceData(this, p_Entry);
		}
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);
 		//VerifyData();

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE
	r_bool = m_ShaderInstanceData->GetBool(p_OldVar->Idx);
#else
	ShaderVar* p_OldVar;
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	p_OldVar = &pNode->GetShaderVar();

	if(!p_OldVar || (p_OldVar->Type != VarInfo.m_Type))
	{
		VerifyData();
	}

	r_bool = pNode->Value().m_Bool;
#endif
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::GetAlphaTextureName(s32 i,atString& r_texmap)
{
#if !SHADER_ATTR_TREE

	if ( i >= m_ShaderInstanceData->GetTextureAlphaCount() )
	{
		return;
	}

	r_texmap = rexMaxUtility::GetTexMapFullPath(m_ShaderInstanceData->GetTextureAlpha(i));
#else
	ShaderVar* p_OldVar;
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	p_OldVar = &pNode->GetShaderVar();

	if(!p_OldVar || (p_OldVar->Type != VarInfo.m_Type))
	{
		VerifyData();
	}

	r_texmap = rexMaxUtility::GetTexMapFullPath(pNode->Value().m_Texture);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetAlphaTextureName(s32 i, atString& textureName)
{
	if (m_ShaderInstanceData->GetTextureAlpha(i) != NULL)
	{
		// Check to see if the alpha texture is the same pointer as the base texture.
		// There was a bug where they were both pointing to the same texture so this will create a new one and set it
		// in the alpha slot so they are properly set back to unique textures.
		if (m_ShaderInstanceData->GetTextureAlpha(i) == m_ShaderInstanceData->GetTexture(i))
		{
			BitmapTex* p_bitmapTex = NewDefaultBitmapTex();
			p_bitmapTex->SetMapName(textureName.c_str());

			int alphaTexIndex = m_ShaderInstanceData->GetTextureCount() + i;
			SetSubTexmap(alphaTexIndex, p_bitmapTex);

			m_ShaderInstanceData->SetTextureAlpha(i, p_bitmapTex);
		}

		rexMaxUtility::SetTexMapFullPath(m_ShaderInstanceData->GetTextureAlpha(i), textureName);
	}
	else
	{
		// If we don't have a texture in this slot, then create a new one with the texture name.
		BitmapTex* p_bitmapTex = NewDefaultBitmapTex();
		p_bitmapTex->SetMapName(textureName.c_str());

		int alphaTexIndex = m_ShaderInstanceData->GetTextureCount() + i;
		SetSubTexmap(alphaTexIndex, p_bitmapTex);

		m_ShaderInstanceData->SetTextureAlpha(i, p_bitmapTex);

		rexMaxUtility::SetTexMapFullPath(m_ShaderInstanceData->GetTextureAlpha(i), textureName);
	}

	if(mp_MtlDlg)
		mp_MtlDlg->SetupUI();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 RstMtlMax::GetAlphaTextureCount()
{	
	return m_ShaderInstanceData->GetTextureAlphaCount();
}

const atString RstMtlMax::GetTextureTemplate(s32 i)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		TexMapType* texMapType = static_cast<TexMapType*>(variables[i]);
		if ( texMapType != NULL )
		{
			return texMapType->GetTextureTemplate();
		}
	}
	else
	{
		grcEffect::VarType type = GetVariableType(i);
		
		switch(type)
		{
		case grcEffect::VT_TEXTURE:
			{
				grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

				if(!p_Shader)
					return atString("");

				grmVariableInfo VarInfo;
				p_Shader->GetInstancedVariableInfo(i,VarInfo);
				return atString(VarInfo.m_TCPTemplate);
			}
		}
	}
	
	return atString("");
}

const atString RstMtlMax::GetTextureTemplateRelative(s32 i)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		TexMapType* texMapType = static_cast<TexMapType*>(variables[i]);
		if ( texMapType != NULL )
		{
			return texMapType->GetTextureTemplateRelative();
		}
	}
	else
	{
		grcEffect::VarType type = GetVariableType(i);

		switch(type)
		{
		case grcEffect::VT_TEXTURE:
			{
				grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

				if(!p_Shader)
					return atString("");

				grmVariableInfo VarInfo;
				p_Shader->GetInstancedVariableInfo(i,VarInfo);
				return atString(VarInfo.m_TCPTemplateRelative);
			}
		}
	}
	return atString("");
}

bool RstMtlMax::GetTextureTemplateOverride(s32 i)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		TexMapType* texMapType = static_cast<TexMapType*>(variables[i]);
		if ( texMapType != NULL )
		{
			return texMapType->GetTextureTemplateOverride();
		}
	}
	else
	{
		grcEffect::VarType type = GetVariableType(i);
		switch(type)
		{
		case grcEffect::VT_TEXTURE:
			{
				grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

				if(!p_Shader)
					return atString("");

				grmVariableInfo VarInfo;
				p_Shader->GetInstancedVariableInfo(i,VarInfo);
				return VarInfo.m_TCPAllowOverride == 1 ? true : false;
			}
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetShaderName(const atString& r_ShaderName)
{
	char newShaderName[1024];
	strcpy(newShaderName,r_ShaderName.c_str());
	char* p_Dot = strrchr(newShaderName,'.');

	if(p_Dot == NULL)
	{
		//Assume the .sps extension is desired.
		strcat(newShaderName,".sps");
	}

	//To be able to swap between the .sps and material presets, cache the previous
	//shader/preset name.
	rageRstShaderDbEntry* p_PreviousEntry = RstMax::GetDbEntry(m_ShaderName);
	MaterialPreset* p_PreviousMaterialPreset = NULL;
	grcEffect* p_PreviousShader = NULL;

	if ( p_PreviousEntry != NULL)
	{
		if (p_PreviousEntry->m_MaterialPreset == NULL)
		{
			if ( p_PreviousEntry->m_Preset != NULL )
			{
				p_PreviousShader = &p_PreviousEntry->m_Preset->GetBasis();
				const char* p_Name = p_PreviousEntry->m_Preset->GetShaderName();

				char buff[512];
				sprintf_s( buff, 512, "%s/fx_max/%s.fx", (RstMax::GetShaderPath()).c_str(), p_Name );

				LoadEffect(buff);
				if(IsPreset())
				{
					p_PreviousShader = &p_PreviousEntry->m_Preset->GetBasis();
				}
			}
			else
			{
				p_PreviousShader = RstMax::FindShader(m_ShaderName);
			}
		}
		else
		{
			p_PreviousMaterialPreset = p_PreviousEntry->m_MaterialPreset;
		}		
	}

	m_ShaderName = newShaderName;
	m_ShaderName.Replace("\\", "/");

	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		m_ShaderInstanceData->MapInstanceData(this, p_Entry, p_PreviousShader, p_PreviousMaterialPreset);

		// Need to do this after because the textures for a material preset aren't setup until after calling MapInstanceData.
		LoadEffectTextures();
	}
	else
	{
		SetIsPreset(true);

		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry, p_PreviousShader, p_PreviousMaterialPreset);
	}

	if(mp_MtlDlg)
		mp_MtlDlg->SetupUI();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetVariable(s32 i,const atString& r_texmap)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		m_ShaderInstanceData->MapInstanceData(this, p_Entry);
		
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();

		p_OldVar = GetVar(variables[i]->GetVariableName());
		if ( !p_OldVar || p_OldVar->VarType != varType )
			return;
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);
		
		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

	char Buffer[8192];

	strcpy_s(Buffer,8192,r_texmap.c_str());

	BitmapTex* p_bitmapTex = NewDefaultBitmapTex();
	p_bitmapTex->SetMapName(Buffer);

	SetSubTexmap(p_OldVar->Idx,p_bitmapTex);

	if(mp_MtlDlg)
		mp_MtlDlg->SetupUI();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetVariable(s32 i,const f32& r_float)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();

		p_OldVar = GetVar(variables[i]->GetVariableName());
		if ( !p_OldVar || p_OldVar->VarType != varType )
			return;
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE
	m_ShaderInstanceData->GetFloat(p_OldVar->Idx) = r_float;
#else
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	pNode->Value().m_Float = r_float;
#endif

	if(mp_MtlDlg)
		mp_MtlDlg->SetupUI();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetVariable(s32 i,const s32& r_int)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();

		p_OldVar = GetVar(variables[i]->GetVariableName());
		if ( !p_OldVar || p_OldVar->VarType != varType )
			return;
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE
	m_ShaderInstanceData->GetInt(p_OldVar->Idx) = r_int;
#else
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	pNode->Value().m_Int = r_int;
#endif

	if(mp_MtlDlg)
		mp_MtlDlg->SetupUI();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetVariable(s32 i,const Vector2& r_vec)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();

		p_OldVar = GetVar(variables[i]->GetVariableName());
		if ( !p_OldVar || p_OldVar->VarType != varType )
			return;
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE
	m_ShaderInstanceData->GetVector2(p_OldVar->Idx) = r_vec;
#else
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	pNode->Value().m_Vector2 = new Vector2(r_vec);
#endif

	if(mp_MtlDlg)
		mp_MtlDlg->SetupUI();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetVariable(s32 i,const Vector3& r_vec)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();

		p_OldVar = GetVar(variables[i]->GetVariableName());
		if ( !p_OldVar || p_OldVar->VarType != varType )
			return;
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE
	m_ShaderInstanceData->GetVector3(p_OldVar->Idx) = r_vec;
#else
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	pNode->Value().m_Vector3 = new Vector3(r_vec);
#endif

	if(mp_MtlDlg)
		mp_MtlDlg->SetupUI();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetVariable(s32 i,const Vector4& r_vec)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();

		p_OldVar = GetVar(variables[i]->GetVariableName());
		if ( !p_OldVar || p_OldVar->VarType != varType )
			return;
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE
	m_ShaderInstanceData->GetVector4(p_OldVar->Idx) = r_vec;
#else
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	pNode->Value().m_Vector4 = new Vector4(r_vec);
#endif

	if(mp_MtlDlg)
		mp_MtlDlg->SetupUI();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetVariable(s32 i,const bool& r_bool)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
	ShaderInstanceData::ShaderVar* p_OldVar = NULL;

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		grcEffect::VarType varType = (grcEffect::VarType) variables[i]->GetType();

		p_OldVar = GetVar(variables[i]->GetVariableName());
		if ( !p_OldVar || p_OldVar->VarType != varType )
			return;
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		p_OldVar = GetVar(VarInfo);

		if(!p_OldVar || (p_OldVar->VarType != VarInfo.m_Type))
			return;
	}

#if !SHADER_ATTR_TREE
	m_ShaderInstanceData->GetBool(p_OldVar->Idx) = r_bool;
#else
	CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot,VarInfo.m_Name);
	pNode->Value().m_Bool = r_bool;
#endif

	if(mp_MtlDlg)
		mp_MtlDlg->SetupUI();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
RSTMAXMTL_EXPORT bool RstMtlMax::GetShaderValue(const atString& r_Name,rexValue& r_ValueGet)
{
	const ShaderInstanceData::ShaderVar* p_ShaderVar = GetVar(r_Name);

	if(!p_ShaderVar)
	{
		return false;
	}

	TypeBase::eVariableType presetVarType = (TypeBase::eVariableType) p_ShaderVar->VarType;
	switch(presetVarType)
	{
	case TypeBase::kFloat:
		r_ValueGet.SetValue(m_ShaderInstanceData->GetFloat(p_ShaderVar->Idx));
		break;
	case TypeBase::kTextureMap:
		{
			atString TexPath;
			atString AlphaMap;

			bool IsAlpha = IsAlphaShader();

			if(IsAlpha)
				AlphaMap = rexMaxUtility::GetTexMapFullPath(m_ShaderInstanceData->GetTextureAlpha(p_ShaderVar->Idx));

			TexPath = rexMaxUtility::GetTexMapFullPath(m_ShaderInstanceData->GetTexture(p_ShaderVar->Idx));

			if(AlphaMap != "")
			{
				TexPath += "+";
				TexPath += AlphaMap;
			}

			r_ValueGet.SetValue(TexPath);
		}
		break;
	case TypeBase::kInt:
		r_ValueGet.SetValue(m_ShaderInstanceData->GetInt(p_ShaderVar->Idx));
		break;
	case TypeBase::kVector2:
		{
			if(m_ShaderInstanceData->GetVector2Count() >= p_ShaderVar->Idx)
			{
				char Buffer[256];
				sprintf_s(Buffer, 256, "%f %f", m_ShaderInstanceData->GetVector2(p_ShaderVar->Idx)[0],
					m_ShaderInstanceData->GetVector2(p_ShaderVar->Idx)[1]);
				r_ValueGet.SetValue(atString(Buffer));
			}
			else
			{
				r_ValueGet.SetValue("1.0f, 1.0f");
			}
		}

		break;
	case TypeBase::kVector3:
		{
			if(m_ShaderInstanceData->GetVector3Count() >= p_ShaderVar->Idx)
			{
				char Buffer[256];
				sprintf_s(Buffer, 256, "%f %f %f",	m_ShaderInstanceData->GetVector3(p_ShaderVar->Idx)[0],
					m_ShaderInstanceData->GetVector3(p_ShaderVar->Idx)[1],
					m_ShaderInstanceData->GetVector3(p_ShaderVar->Idx)[2]);
				r_ValueGet.SetValue(atString(Buffer));
			}
			else
			{
				r_ValueGet.SetValue("1.0f 1.0f 1.0f");
			}
		}

		break;
	case TypeBase::kVector4: 
		{
			if(m_ShaderInstanceData->GetVector4Count() >= p_ShaderVar->Idx)
			{
				char Buffer[256];
				sprintf_s(Buffer, 256, "%f %f %f %f",	m_ShaderInstanceData->GetVector4(p_ShaderVar->Idx)[0],
					m_ShaderInstanceData->GetVector4(p_ShaderVar->Idx)[1],
					m_ShaderInstanceData->GetVector4(p_ShaderVar->Idx)[2],
					m_ShaderInstanceData->GetVector4(p_ShaderVar->Idx)[3]);
				r_ValueGet.SetValue(atString(Buffer));
			}
			else
			{
				r_ValueGet.SetValue("1.0f 1.0f 1.0f 1.0f");
			}
		}

		break;

	default:
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RSTMAXMTL_EXPORT bool RstMtlMax::GetShaderValue(const grmVariableInfo& r_VarInfo,rexValue& r_ValueGet)
{
	grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

#if !SHADER_ATTR_TREE
	const ShaderInstanceData::ShaderVar* p_ShaderVar = GetVar(r_VarInfo);

	if(!p_ShaderVar || !p_Shader)
	{
		return false;
	}

	switch(p_ShaderVar->VarType)
	{
	case grcEffect::VT_FLOAT:
		r_ValueGet.SetValue(m_ShaderInstanceData->GetFloat(p_ShaderVar->Idx));
		break;
	case grcEffect::VT_TEXTURE:
		{
			atString TexPath;
			atString AlphaMap;

			bool IsAlpha = IsAlphaShader();

			if(IsAlpha)
				AlphaMap = rexMaxUtility::GetTexMapFullPath(m_ShaderInstanceData->GetTextureAlpha(p_ShaderVar->Idx));

			TexPath = rexMaxUtility::GetTexMapFullPath(m_ShaderInstanceData->GetTexture(p_ShaderVar->Idx));

			if(AlphaMap != "")
			{
				TexPath += "+";
				TexPath += AlphaMap;
			}

			r_ValueGet.SetValue(TexPath);
		}
		break;
	case grcEffect::VT_INT_DONTUSE:
		r_ValueGet.SetValue(m_ShaderInstanceData->GetInt(p_ShaderVar->Idx));
		break;
	case grcEffect::VT_VECTOR2:
		{
			if(m_ShaderInstanceData->GetVector2Count() >= p_ShaderVar->Idx)
			{
				char Buffer[256];
				sprintf_s(Buffer,256,"%f %f", m_ShaderInstanceData->GetVector2(p_ShaderVar->Idx)[0],
										m_ShaderInstanceData->GetVector2(p_ShaderVar->Idx)[1]);
				r_ValueGet.SetValue(atString(Buffer));
			}
			else
			{
				r_ValueGet.SetValue("1.0f, 1.0f");
			}
		}

		break;
	case grcEffect::VT_VECTOR3:
		{
			if(m_ShaderInstanceData->GetVector3Count() >= p_ShaderVar->Idx)
			{
				char Buffer[256];
				sprintf_s(Buffer,256,"%f %f %f",	m_ShaderInstanceData->GetVector3(p_ShaderVar->Idx)[0],
											m_ShaderInstanceData->GetVector3(p_ShaderVar->Idx)[1],
											m_ShaderInstanceData->GetVector3(p_ShaderVar->Idx)[2]);
				r_ValueGet.SetValue(atString(Buffer));
			}
			else
			{
				r_ValueGet.SetValue("1.0f 1.0f 1.0f");
			}
		}

		break;
	case grcEffect::VT_VECTOR4: 
		{
			if(m_ShaderInstanceData->GetVector4Count() >= p_ShaderVar->Idx)
			{
				char Buffer[256];
				sprintf_s(Buffer,256,"%f %f %f %f",	m_ShaderInstanceData->GetVector4(p_ShaderVar->Idx)[0],
												m_ShaderInstanceData->GetVector4(p_ShaderVar->Idx)[1],
												m_ShaderInstanceData->GetVector4(p_ShaderVar->Idx)[2],
												m_ShaderInstanceData->GetVector4(p_ShaderVar->Idx)[3]);
				r_ValueGet.SetValue(atString(Buffer));
			}
			else
			{
				r_ValueGet.SetValue("1.0f 1.0f 1.0f 1.0f");
			}
		}

		break;
	case grcEffect::VT_BOOL_DONTUSE:
		{
			if(m_ShaderInstanceData->GetBoolCount() >= p_ShaderVar->Idx)
			{
				char Buffer[256];
				sprintf_s(Buffer,256,"%d", m_ShaderInstanceData->GetBool(p_ShaderVar->Idx)?1:0);
				r_ValueGet.SetValue(atString(Buffer));
			}
			else
			{
				r_ValueGet.SetValue("0");
			}
		}

		break;

	case grcEffect::VT_MATRIX43:
	case grcEffect::VT_MATRIX44:
	default:
		return false;
	}
#else

CRstShaderValueNode *pNode = FindSingleShaderValueNode(
	m_shaderValRoot, 
	r_VarInfo.m_Name, 
	CRstShaderValueNode::gGrcEffectTypeLUT[r_VarInfo.m_Type]
);
if(!pNode)
	return false;

const ShaderVar* p_ShaderVar = &pNode->GetShaderVar();

if(!p_ShaderVar || !p_Shader)
{
	return false;
}

switch(p_ShaderVar->Type)
{
case grcEffect::VT_FLOAT:
	r_ValueGet.SetValue(pNode->Value().m_Float);
	break;
case grcEffect::VT_TEXTURE:
	{
		atString TexPath;
		atString AlphaMap;

		bool IsAlpha = IsAlphaShader();

		Texmap *pTex = (Texmap *)GetReference(pNode->RefTargetIndex());
		assert(pTex);

// 		if(IsAlpha)
// 			AlphaMap = rexMaxUtility::GetTexMapFullPath(m_TextureAlphas[p_ShaderVar->Idx]);
// 
		TexPath = rexMaxUtility::GetTexMapFullPath(pTex);

// 		if(AlphaMap != "")
// 		{
// 			TexPath += "+";
// 			TexPath += AlphaMap;
// 		}

		r_ValueGet.SetValue(TexPath);
	}
	break;
case grcEffect::VT_INT_DONTUSE:
	r_ValueGet.SetValue(pNode->Value().m_Int);
	break;
case grcEffect::VT_VECTOR2:
	{
		char Buffer[256];
		Vector2 &Vec = *pNode->Value().m_Vector2;
		sprintf(Buffer,"%f %f", Vec[0],Vec[1]);
		r_ValueGet.SetValue(atString(Buffer));
	}

	break;
case grcEffect::VT_VECTOR3:
	{
		char Buffer[256];
		Vector3 &Vec = *pNode->Value().m_Vector3;
		sprintf(Buffer,"%f %f %f", Vec[0],Vec[1],Vec[2]);
		r_ValueGet.SetValue(atString(Buffer));
	}

	break;
case grcEffect::VT_VECTOR4: 
	{
		char Buffer[256];
		Vector4 &Vec = *pNode->Value().m_Vector4;
		sprintf(Buffer,"%f %f %f %f", Vec[0],Vec[1],Vec[2],Vec[3]);
		r_ValueGet.SetValue(atString(Buffer));
	}

	break;
case grcEffect::VT_BOOL_DONTUSE:
	{
		char Buffer[256];
		sprintf(Buffer,"%s", pNode->Value().m_Bool?"TRUE":"FALSE");
		r_ValueGet.SetValue(atString(Buffer));
	}

	break;

case grcEffect::VT_MATRIX43:
case grcEffect::VT_MATRIX44:
default:
	return false;
}

#endif
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::WriteString(fiStream* p_Save,const atString& r_Write)
{
	s32 WriteSize = r_Write.GetLength();
	p_Save->WriteInt(&WriteSize,1);
	p_Save->Write(r_Write,WriteSize);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::ReadString(fiStream* p_Load,atString& r_Read)
{
	s32 ReadSize;
	p_Load->ReadInt(&ReadSize,1);
	char* p_Buffer = new char[ReadSize + 1];
	p_Buffer[ReadSize] = '\0';
	p_Load->Read(p_Buffer,ReadSize);
	r_Read = atString(p_Buffer);
	delete[] p_Buffer;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::SetupUI()
{
	if (mp_MtlDlg)
		mp_MtlDlg->SetupUI();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
grcEffect::VarType RstMtlMax::TranslateVarType(grcEffect::VarType Var)
{
	enum grmVarType { 
		VT_INT,
		VT_FLOAT, 
		VT_VECTOR2, 
		VT_VECTOR3, 
		VT_VECTOR4, 
		VT_MATRIX43, 
		VT_MATRIX44, 
		VT_COLOR32, 
		VT_TEXTURE,
		VT_BOOL,
	};

	grmVarType OldVarType = (grmVarType)Var;

	switch(OldVarType)
	{
	case VT_INT: return grcEffect::VT_INT_DONTUSE;
	case VT_FLOAT: return grcEffect::VT_FLOAT;
	case VT_VECTOR2: return grcEffect::VT_VECTOR2;
	case VT_VECTOR3: return grcEffect::VT_VECTOR3;
	case VT_VECTOR4: return grcEffect::VT_VECTOR4;
	case VT_MATRIX43: return grcEffect::VT_MATRIX43;
	case VT_MATRIX44: return grcEffect::VT_MATRIX44;
	case VT_COLOR32: return grcEffect::VT_BOOL_DONTUSE;
	case VT_TEXTURE: return grcEffect::VT_TEXTURE;
	case VT_BOOL: return grcEffect::VT_BOOL_DONTUSE;
	}

	return Var;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RSTMAXMTL_EXPORT bool RstMtlMax::IsAlphaShader()
{
	if(IsPreset())
	{
		rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);

		if(!p_Entry)
			return false;

		if ( p_Entry->IsMaterialPreset() )
		{
			return p_Entry->m_MaterialPreset->IsAlphaShader();
		}
		else if(p_Entry->m_Preset)
		{
			if( p_Entry->m_Preset->GetDrawBucket() > 0 || p_Entry->m_Preset->GetBasis().GetPropertyValue( sAlphaMapPropertyname, 0 ) == 1 )
				return true;
		}
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(p_Shader && ( p_Shader->GetDrawBucket() > 0 || p_Shader->GetPropertyValue( sAlphaMapPropertyname, 0 ) == 1 ) )
			return true;
	}

	return false;
}

RSTMAXMTL_EXPORT bool RstMtlMax::IsTerrainShader()
{
	rageRstShaderDbEntry* dbEntry = RstMax::GetDbEntry(m_ShaderName);
	const char* shaderName = NULL;

	if (dbEntry->IsMaterialPreset())
		shaderName = dbEntry->m_MaterialPreset->GetShaderName();
	else
		shaderName = m_ShaderName;

	if( strstr( shaderName, "terrain_" ) )
		return true;

	return false;
}

RSTMAXMTL_EXPORT bool RstMtlMax::IsBlendShader()
{
	rageRstShaderDbEntry* dbEntry = RstMax::GetDbEntry(m_ShaderName);
	const char* shaderName = NULL;

	if (dbEntry->IsMaterialPreset())
		shaderName = dbEntry->m_MaterialPreset->GetShaderName();
	else
		shaderName = m_ShaderName;

	if( strstr( shaderName, "blend_" ) )
		return true;

	return false;
}

RSTMAXMTL_EXPORT bool RstMtlMax::IsTintShader()
{
	rageRstShaderDbEntry* dbEntry = RstMax::GetDbEntry(m_ShaderName);
	const char* shaderName = NULL;
	
	if (dbEntry != NULL && dbEntry->IsMaterialPreset())
		shaderName = dbEntry->m_MaterialPreset->GetShaderName();
	else
		shaderName = m_ShaderName;

	if( strstr( shaderName, "_tnt" ) )
		return true;

	return false;
}

RSTMAXMTL_EXPORT bool RstMtlMax::IsTreeShader()
{
	rageRstShaderDbEntry* dbEntry = RstMax::GetDbEntry(m_ShaderName);
	const char* shaderName = NULL;

	if (dbEntry->IsMaterialPreset())
		shaderName = dbEntry->m_MaterialPreset->GetShaderName();
	else
		shaderName = m_ShaderName;

	if( strstr( shaderName, "trees_" ) )
		return true;

	return false;
}

RSTMAXMTL_EXPORT bool RstMtlMax::IsCameraOrientedShader()
{
	if( strstr( m_ShaderName, "_camera_" ) )
		return true;

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RSTMAXMTL_EXPORT void RstMtlMax::GetVariableInfoTextureOutputFormats(s32 i, atString& r_TextureOutputFormats)
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
		
		const TexMapType* pTextureMapType = static_cast<const TexMapType*>(variables[i]);
		if ( pTextureMapType )
		{
			r_TextureOutputFormats = pTextureMapType->GetTextureFormat();
		}
	}
	else
	{
		grcEffect* p_Shader = RstMax::FindShader(m_ShaderName);

		if(!p_Shader)
			return;

		m_ShaderInstanceData->MapInstanceData(this, p_Entry);

		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);

		r_TextureOutputFormats = VarInfo.m_TextureOutputFormats;
	}	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

TSTR RstMtlMax::FpGetVariableType(int idx)
{
	if ( rstMaxUtility::IsPreset(m_ShaderName) )
	{
		rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
		if ( p_Entry && p_Entry->IsMaterialPreset() )
		{
			const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
			grcEffect::VarType type = (grcEffect::VarType) variables[idx]->GetType();

			switch(type)
			{
			case grcEffect::VT_BOOL_DONTUSE:
				return TSTR("VT_BOOL_DONTUSE");
			case grcEffect::VT_FLOAT:
				return TSTR("VT_FLOAT");
			case grcEffect::VT_INT_DONTUSE:
				return TSTR("VT_INT_DONTUSE");
			case grcEffect::VT_MATRIX43:
				return TSTR("VT_MATRIX43");
			case grcEffect::VT_MATRIX44:
				return TSTR("VT_MATRIX44");
			case grcEffect::VT_STRING:
				return TSTR("VT_STRING");
			case grcEffect::VT_TEXTURE:
				return TSTR("VT_TEXTURE");
			case grcEffect::VT_VECTOR2:
				return TSTR("VT_VECTOR2");
			case grcEffect::VT_VECTOR3:
				return TSTR("VT_VECTOR3");
			case grcEffect::VT_VECTOR4:
				return TSTR("VT_VECTOR4");
			default:
				return TSTR("undefined");
			}
		}

		return TSTR("undefined");
	}
	else
	{
		grcEffect::VarType varType = GetVariableType(idx);
		switch(varType)
		{
		case grcEffect::VT_BOOL_DONTUSE:
			return TSTR("VT_BOOL_DONTUSE");
		case grcEffect::VT_FLOAT:
			return TSTR("VT_FLOAT");
		case grcEffect::VT_INT_DONTUSE:
			return TSTR("VT_INT_DONTUSE");
		case grcEffect::VT_MATRIX43:
			return TSTR("VT_MATRIX43");
		case grcEffect::VT_MATRIX44:
			return TSTR("VT_MATRIX44");
		case grcEffect::VT_STRING:
			return TSTR("VT_STRING");
		case grcEffect::VT_TEXTURE:
			return TSTR("VT_TEXTURE");
		case grcEffect::VT_VECTOR2:
			return TSTR("VT_VECTOR2");
		case grcEffect::VT_VECTOR3:
			return TSTR("VT_VECTOR3");
		case grcEffect::VT_VECTOR4:
			return TSTR("VT_VECTOR4");
		default:
			return TSTR("undefined");
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

TSTR RstMtlMax::FpGetVariableName(int idx)
{
	atString atVarName;
	GetVariableName(idx, atVarName);
	return TSTR((const char*)atVarName);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

TSTR RstMtlMax::FpGetShaderParameterName(int idx)
{
	atString atVarName;
	GetShaderParameterName(idx, atVarName);
	return TSTR((const char*)atVarName);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

TSTR RstMtlMax::FpGetShaderName()
{
	return TSTR((const char*)GetShaderName());
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

TSTR RstMtlMax::FpGetTextureVariableValue(int idx)
{
	atString atTextureValue;
	GetVariable(idx, atTextureValue);
	return TSTR((const char*)atTextureValue);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

float RstMtlMax::FpGetFloatVariableValue(int idx)
{
	float fVal;
	GetVariable(idx, fVal);
	return fVal;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

int	RstMtlMax::FpGetIntVariableValue(int idx)
{
	int iVal;
	GetVariable(idx, iVal);
	return iVal;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

bool RstMtlMax::FpGetBoolVariableValue(int idx)
{
	bool bVal;
	GetVariable(idx, bVal);
	return bVal;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

Point2 RstMtlMax::FpGetVector2VariableValue(int idx)
{
	Vector2 v2Val;
	GetVariable(idx, v2Val);
	return Point2(v2Val.x, v2Val.y);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

Point3 RstMtlMax::FpGetVector3VariableValue(int idx)
{
	Vector3 v3Val;
	GetVariable(idx, v3Val);
	return Point3(v3Val.x, v3Val.y, v3Val.z);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

Point4 RstMtlMax::FpGetVector4VariableValue(int idx)
{
	Vector4 v4Val;
	GetVariable(idx, v4Val);
	return Point4(v4Val.x, v4Val.y, v4Val.z, v4Val.w);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
TSTR	RstMtlMax::FpUnsafeGetVariableName(int idx)
{
	// No way to get this at this time
	return TSTR("Unknowable");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
TSTR	RstMtlMax::FpUnsafeGetShaderParameterName(int idx)
{
	// No way to get this at this time
	return TSTR("Unknowable");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int		RstMtlMax::FpUnsafeGetVariableCount()
{	
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetTextureCount()
		+ m_ShaderInstanceData->GetFloatCount()
		+ m_ShaderInstanceData->GetIntCount()
		+ m_ShaderInstanceData->GetVector2Count()
		+ m_ShaderInstanceData->GetVector3Count()
		+ m_ShaderInstanceData->GetVector4Count()
		+ m_ShaderInstanceData->GetBoolCount();
#else
	ClearNodeSearchResult();
	return FindShaderValueNode(m_shaderValRoot,false);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int		RstMtlMax::FpUnsafeGetTextureVariableCount()
{	
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetTextureCount();
#else
	ClearNodeSearchResult();
	return FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeTexture);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int		RstMtlMax::FpUnsafeGetFloatVariableCount()
{	
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetFloatCount();
#else
	ClearNodeSearchResult();
	return FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeFloat);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int		RstMtlMax::FpUnsafeGetIntVariableCount()
{	
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetIntCount();
#else
	return FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeInt);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int		RstMtlMax::FpUnsafeGetBoolVariableCount()
{	
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetBoolCount();
#else
	ClearNodeSearchResult();
	return FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeBool);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int		RstMtlMax::FpUnsafeGetVector2VariableCount()
{	
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetVector2Count();
#else
	ClearNodeSearchResult();
	return FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeVec2);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int		RstMtlMax::FpUnsafeGetVector3VariableCount()
{	
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetVector3Count();
#else
	ClearNodeSearchResult();
	return FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeVec3);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int		RstMtlMax::FpUnsafeGetVector4VariableCount()
{	
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetVector4Count();
#else
	ClearNodeSearchResult();
	return FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeVec4);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
TSTR	RstMtlMax::FpUnsafeGetShaderName()
{
	return FpGetShaderName();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
TSTR	RstMtlMax::FpUnsafeGetVariableType(int idx)
{
	// No way to get this at this time
	return TSTR("Unknowable");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
TSTR	RstMtlMax::FpUnsafeGetTextureVariableValue(int idx)
{
#if !SHADER_ATTR_TREE
	if((m_ShaderInstanceData->GetTextureCount() > idx) && (m_ShaderInstanceData->GetTexture(idx) != NULL))
	{
		return TSTR(rexMaxUtility::GetTexMapFullPath(m_ShaderInstanceData->GetTexture(idx)).c_str());
	}
	return TSTR("Invalid texture index");
#else
	ClearNodeSearchResult();
	FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeTexture);
	atArray<CRstShaderValueNode*> &pNodeArray = GetLastNodeSearchResult();
	if(idx>pNodeArray.GetCount())
		return TSTR("Invalid texture index");
	return TSTR(rexMaxUtility::GetTexMapFullPath(pNodeArray[idx]->Value().m_Texture));
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
float	RstMtlMax::FpUnsafeGetFloatVariableValue(int idx)
{
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetFloat(idx);
#else
	ClearNodeSearchResult();
	FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeFloat);
	atArray<CRstShaderValueNode*> &pNodeArray = GetLastNodeSearchResult();
	if(idx>pNodeArray.GetCount())
		return -1;
	return pNodeArray[idx]->Value().m_Float;
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int		RstMtlMax::FpUnsafeGetIntVariableValue(int idx)
{
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetInt(idx);
#else
	ClearNodeSearchResult();
	FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeInt);
	atArray<CRstShaderValueNode*> &pNodeArray = GetLastNodeSearchResult();
	if(idx>pNodeArray.GetCount())
		return -1;
	return pNodeArray[idx]->Value().m_Int;
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
bool	RstMtlMax::FpUnsafeGetBoolVariableValue(int idx)
{
#if !SHADER_ATTR_TREE
	return m_ShaderInstanceData->GetBool(idx);
#else
	ClearNodeSearchResult();
	FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeBool);
	atArray<CRstShaderValueNode*> &pNodeArray = GetLastNodeSearchResult();
	if(idx>pNodeArray.GetCount())
		return false;
	return pNodeArray[idx]->Value().m_Bool;
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
Point2	RstMtlMax::FpUnsafeGetVector2VariableValue(int idx)
{
#if !SHADER_ATTR_TREE
	Vector2 v2Val = m_ShaderInstanceData->GetVector2(idx);
	return Point2(v2Val.x, v2Val.y);
#else
	ClearNodeSearchResult();
	FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeVec2);
	atArray<CRstShaderValueNode*> &pNodeArray = GetLastNodeSearchResult();
	if(idx>pNodeArray.GetCount())
		return Point2();
	Vector2 &v2Val = *pNodeArray[idx]->Value().m_Vector2;
	return Point2(v2Val.x, v2Val.y);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
Point3	RstMtlMax::FpUnsafeGetVector3VariableValue(int idx)
{
#if !SHADER_ATTR_TREE
	Vector3 v3Val = m_ShaderInstanceData->GetVector3(idx);
	return Point3(v3Val.x, v3Val.y, v3Val.z);
#else
	ClearNodeSearchResult();
	FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeVec3);
	atArray<CRstShaderValueNode*> &pNodeArray = GetLastNodeSearchResult();
	if(idx>pNodeArray.GetCount())
		return Point3(0,0,0);
	Vector3 &vVal = *pNodeArray[idx]->Value().m_Vector3;
	return Point3(vVal.x, vVal.y, vVal.z);
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
Point4	RstMtlMax::FpUnsafeGetVector4VariableValue(int idx)
{
#if !SHADER_ATTR_TREE
	Vector4 v4Val = m_ShaderInstanceData->GetVector4(idx);
	return Point4(v4Val.x, v4Val.y, v4Val.z, v4Val.w);
#else
	ClearNodeSearchResult();
	FindShaderValueNode(m_shaderValRoot,false,SHADERVALNAME_IGNORE,CRstShaderValueNode::eShaderValTypeVec4);
	atArray<CRstShaderValueNode*> &pNodeArray = GetLastNodeSearchResult();
	if(idx>pNodeArray.GetCount())
		return Point4(0,0,0,0);
	Vector4 &vVal = *pNodeArray[idx]->Value().m_Vector4;
	return Point4(vVal.x, vVal.y, vVal.z, vVal.w);
#endif
}

int		
RstMtlMax::FpGetRawTextureCount()
{
	return ( m_ShaderInstanceData->GetTextureCount() );
}

int
RstMtlMax::FpGetRawTextureAlphaCount()
{
	return ( m_ShaderInstanceData->GetTextureAlphaCount()  );
}

bool	
RstMtlMax::FpGetRawTextureHasAlpha( int idx )
{
	return ( m_ShaderInstanceData->GetTextureAlphaCount() > idx );
}

TSTR	
RstMtlMax::FpGetRawTextureValue( int idx )
{
	Assertf( idx >= 0, "Index out of range." );
	Assertf( idx < m_ShaderInstanceData->GetTextureCount(), "Index out of range." );
	if ( idx < 0 || idx >= m_ShaderInstanceData->GetTextureCount() )
		return ( "" );

	Texmap* pMap = m_ShaderInstanceData->GetTexture(idx);
	if ( !pMap )
		return ( "" );
	if ( Class_ID(BMTEX_CLASS_ID,0) != pMap->ClassID() )
		return ( "" );
	
	BitmapTex* pBmpTex = (BitmapTex*)pMap;
	const char* pFilename = pBmpTex->GetMapName( );

	return ( pFilename );
}

TSTR	
RstMtlMax::FpGetRawTextureAlphaValue( int idx )
{
	Assertf( idx >= 0, "Index out of range." );
	Assertf( idx < m_ShaderInstanceData->GetTextureAlphaCount(), "Index out of range." );
	if ( idx < 0 || idx >= m_ShaderInstanceData->GetTextureAlphaCount() )
		return ( "" );

	Texmap* pMap = m_ShaderInstanceData->GetTextureAlpha(idx);
	if ( !pMap )
		return ( "" );
	if ( Class_ID(BMTEX_CLASS_ID,0) != pMap->ClassID() )
		return ( "" );

	BitmapTex* pBmpTex = (BitmapTex*)pMap;
	const char* pFilename = pBmpTex->GetMapName( );

	return ( pFilename );
}


#if RSTMTL_HW_SHADER

BaseInterface* RstMtlMax::GetInterface(Interface_ID id)
{
	if (id == DX9_VERTEX_SHADER_INTERFACE_ID) {
		return static_cast<IDX9VertexShader *>(this);
	}
	else if (id == VIEWPORT_SHADER_CLIENT_INTERFACE) {
		return static_cast<IDXDataBridge*>(this);
	}
	else if(id == VIEWPORT_SHADER9_CLIENT_INTERFACE) {
		return static_cast<IDXDataBridge*>(this);
	}
	else if (id == IID_MATERIAL_VIEWPORT_SHADING) {
		return static_cast<IMaterialViewportShading*>(this);
	}
	else if( id == RSM_INTERFACE ) {
		return static_cast<IRageShaderMaterial*>(this);
	}
	else return Mtl::GetInterface(id);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

bool RstMtlMax::GetDevice()
{
	GraphicsWindow* GW = NULL;
	ViewExp* View = NULL;

	View = GetCOREInterface()->GetActiveViewport();
	if(View)
	{
		GW = View->getGW();

		if(GW)
		{
			ID3D9GraphicsWindow *D3DGW = (ID3D9GraphicsWindow *)GW->GetInterface(D3D9_GRAPHICS_WINDOW_INTERFACE_ID);
			
			if(D3DGW)
			{
				m_pD3DDevice = D3DGW->GetDevice();
				m_ElemContainer.SetGWWindow(D3DGW);
				m_ElemContainer.SetD3Device(m_pD3DDevice);

				return m_pD3DDevice != NULL;
			}
		}
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
HRESULT RstMtlMax::CreateEffectParser()
{	
	IParserLoader * parserLoader = GetParserLoader();
	// Why do we get parser number 1?  Well, it turns out that parser 0 didnt seem to
	// support textures.  The number set here is hardcoded, but in production
	// you will want to read the 
	// string ParamID = "0x00?"  included at the header of the .fx files
	m_EffectParser = parserLoader->GetParser(0);
	if(!m_EffectParser)
		return D3DERR_INVALIDCALL;

	return D3D_OK;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMtlMax::Draw(INode* curNode)
{
	char* pName = curNode->GetName();
	if( !m_debugParseSuccess )
		return;

	
	TimeValue t = GetCOREInterface()->GetTime();

	int idxCache = m_pMeshCache->SetCachedMesh(NULL, curNode, t, m_RefreshElements);
	INode * activeNode = m_pMeshCache->GetActiveNode(idxCache);
	IRenderMesh * activeRenderMesh = m_pMeshCache->GetActiveRenderMesh(idxCache);
	Mesh * activeMesh = m_pMeshCache->GetActiveMesh(idxCache);

	Mtl* nodeMtl = curNode->GetMtl();

	Color  col = GetCOREInterface()->GetViewportBGColor();
	D3DCOLOR bkgColor = col.toRGB();

	if(!m_pD3DDevice && !GetDevice()) 
		return;

	int w = 0, h = 0;
	m_ElemContainer.GetWinDimension(w, h);
	
	if(!m_EffectParser)
	{

		char buff[512];
		rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);
		if( p_Entry )
		{
			const char* p_Name = NULL;
			if ( p_Entry->IsMaterialPreset() )
			{
				p_Name = p_Entry->m_MaterialPreset->GetShaderName();
			}
			else
			{
				grcEffect* p_Shader = &p_Entry->m_Preset->GetBasis();
				if( p_Shader )
				{
					p_Name = p_Entry->m_Preset->GetShaderName();
				}
			}

			if ( p_Name != NULL )
			{

				sprintf_s( buff, 512, "%s/fx_max/%s.fx", (RstMax::GetShaderPath()).c_str(), p_Name );
				if (!LoadEffect(buff))
				{
					//Unable to load the effect.  Possibly a preset's shader name is incorrect.
					return;
				}

				UpdateParameters(true);
			}
		}

	}

	if(m_EffectParser)
	{

		if(!m_EffectParser->PreRender(m_pD3DDevice, activeRenderMesh, &m_ElemContainer, &m_ElemContainer, bkgColor, w, h))
		{
			// DrawError();
			return;
		}

		if(!activeRenderMesh->Evaluate(m_pD3DDevice, activeMesh, GetMatIndex(nodeMtl), false))
			return;


		if(!m_EffectParser->Render(m_pD3DDevice,activeRenderMesh, m_ElemContainer.GetTechniqueName()))
		{
			// DrawError();
			return;
		}
	}	
}

void RstMtlMax::LoadEffectTextures()
{
	bool isTerrain = IsTerrainShader();

#if !SHADER_ATTR_TREE
	for(int i=0;i<m_ShaderInstanceData->GetTextureCount();i++)
	{
		if( isTerrain )
			LoadTexmap( m_ShaderInstanceData->GetTexture(i), i );
		else
		{
			if( i == 0)
			{
				LoadTexmap(m_ShaderInstanceData->GetTexture(i), i);

				if( m_ShaderInstanceData->GetTextureAlphaCount() > i )
					LoadTexmap( m_ShaderInstanceData->GetTextureAlpha(i), i+1 );
				else
					LoadTexmap( NULL, i+1 );
			}
			else
				LoadTexmap( m_ShaderInstanceData->GetTexture(i), i+1 );
		}
	}
#else
	for(int i=0;i<NumRefs();i++)
	{
		if( isTerrain )
			LoadTexmap((Texmap*)GetReference(i), i );
		else
		{
			if( i == 0)
			{
				LoadTexmap((Texmap*)GetReference(i), i );

				// 					if( m_TextureAlphas.GetCount() > i )
				// 						LoadTexmap( m_TextureAlphas[i], i+1 );
				// 					else
				LoadTexmap( NULL, i+1 );
			}
			else
				LoadTexmap((Texmap*)GetReference(i), i+1 );
		}
	}
#endif
}

bool RstMtlMax::LoadEffect(TCHAR* fname)
{
	// Reset our effect parser
	if(m_EffectParser)
	{
		m_EffectParser->DestroyParser();
		m_EffectParser = NULL;
	}

	// Delete all previous elements
	m_ElemContainer.DeleteAllElements();

	if (fname != NULL)
	{
		if(!m_pD3DDevice && !GetDevice()) 
			return false;

		CreateEffectParser();
#if (MAX_RELEASE >= 12000)	
		IFileResolutionManager* pFRM = IFileResolutionManager::GetInstance();
		MaxSDK::Util::Path path(_M(fname));
		if (!pFRM->GetFullFilePath(path, MaxSDK::AssetManagement::kOtherAsset))
		{
			// the bitmap file could not be found using 3ds Max asset
			// resolution rules. The value of path is left unchanged
			return false;
		}
		
		MaxSDK::AssetManagement::AssetUser assetUser;
		assetUser.GetFullFilePath( path );

		m_debugParseSuccess = true;
		m_parserErrorMsg = "";
		if (!m_EffectParser->LoadEffect(m_pD3DDevice, &m_ElemContainer, fname, true, true) ||
			!m_EffectParser->ParseEffectFile(m_pD3DDevice, assetUser, &m_ElemContainer))
#else
		if (!m_EffectParser->LoadEffect(m_pD3DDevice, &m_ElemContainer, fname, true, true) ||
			!m_EffectParser->ParseEffectFile(m_pD3DDevice, fname, &m_ElemContainer))
#endif // (MAX_RELEASE >= MAX_RELEASE_R12)	
		{
			// TODO: You will want to post this error somewhere no doubt
			TSTR lastError = fname;
			lastError.Append(":\n");
			lastError.Append(m_EffectParser->GetLastParserError());
			lastError.Append("\r\n");
//			MessageBox(GetCOREInterface()->GetMAXHWnd(), lastError, "Shader parse error", MB_ICONERROR);
			m_parserErrorMsg = lastError.data();
			m_EffectParser->DestroyParser();
			m_EffectParser = NULL;
			m_debugParseSuccess = false;
			return false;
		}

		// If we want to put our texture into the scene, we should assign it to the 
		// new effect
		LoadEffectTextures();
	}
	return true;
}

void RstMtlMax::LoadTexmap(Texmap* texMap, int texIdx)
{
	m_ElemContainer.LoadTextureParam((rexMaxUtility::GetTexMapFullPath(texMap)).c_str(), texIdx, m_EffectParser, m_TexScaleMultiplier);
}

void RstMtlMax::LoadEnvironmentTexmap(Texmap* texMap)
{
	// LPXO: All textrues have to be registered through the param system so they get cleaned up.  DONT uncomment this without having a plan 
	// on how you are going to achieve that.
	//m_ElemContainer.LoadEnvironmentTextureParam((rexMaxUtility::GetTexMapFullPath(texMap)).c_str(), m_EffectParser, m_TexScaleMultiplier);
}

int RstMtlMax::GetMatIndex(Mtl * pMtl)
{
	int				i;
	Mtl* Std = NULL;
	StdMat2* Shader = NULL;

	if(pMtl->IsMultiMtl())
	{
		Shader = (StdMat2 *)GetRefTarg();

		// I use the SubAnims here, as I do not want any NULL material introduced that are not visible to the user
		// this can happen when you have N materials and you select N+1 mat ID - the material will add a NULL material 
		// to the list to compensate - this screws with the index into the paramblock
		for(i=0; i < pMtl->NumSubs(); i++)
		{	
			Std = (Mtl*)pMtl->SubAnim(i);

			if(Std!=NULL)
			{
				if(Std->NumSubMtls()>0)
				{

					for(int j=0; j< Std->NumSubMtls();j++)
					{
						Mtl * subMtl = Std->GetSubMtl(j);
						if(subMtl == Shader)
						{
							Std = subMtl;
							break;
						}

					}
				}
				
				MSPlugin* plugin = (MSPlugin*)((ReferenceTarget*)Std)->GetInterface(I_MAXSCRIPTPLUGIN);
				ReferenceTarget * targ = NULL;
				if(plugin){
					targ = plugin->get_delegate();
					Std = (Mtl*)targ;
				}
				
				if(Std == Shader)
				{
					return(i+1);
					/*
					int id=0;
					// this gets interesting - the MatID are editable !! must get them from the PAramblock
					IParamBlock2 * pblock = pMtl->GetParamBlock(0);	// there is only one
					if(pblock)
					{
						ParamBlockDesc2 * pd = pblock->GetDesc();
						
						for(int j=0;j<pd->count;j++)
						{
							
							if(_tcsicmp(_T("materialIDList"),pd->paramdefs[j].int_name)==0)	//not localised
							{
								//int id;
								pblock->GetValue(j,0,id,FOREVER,i); 
								id = id+1;	//for some reason this is stored as a zero index, when a 1's based index is used
							}
							
							
						}
						

					}
					return(id);
					*/
				}
			}
		}
	}
	return(0);
}


//////////////////////////////////////////////////////////////////////////
// IDX9VertexShader functions
//	The IDX9VertexShader interface is actual implementation
//	part of the material.  These functions result in something being
//	actually drawn to screen.  The majority of the functions can actually
//	be safely ignored, but the Initialize functions at the bottom of this
//	section are called by dxgfx to let us set up our effects for rendering.
#pragma region IDX9VertexShader
//////////////////////////////////////////////////////////////////////////

HRESULT RstMtlMax::ConfirmDevice(ID3D9GraphicsWindow *d3dgw)
{
	m_ElemContainer.SetGWWindow(d3dgw);
	return S_OK;
}

HRESULT RstMtlMax::ConfirmPixelShader(IDX9PixelShader *pps)
{
	return S_OK;
}


inline bool RstMtlMax::CanTryStrips()
{
	return true;
}

inline int RstMtlMax::GetNumMultiPass()
{
	return 1;
}


inline HRESULT RstMtlMax::SetVertexShader(ID3D9GraphicsWindow *d3dgw, int numPass)
{
	return S_OK;
}

inline bool RstMtlMax::DrawMeshStrips(ID3D9GraphicsWindow *d3dgw, MeshData *data)
{
	// Return true here to tell max that we will take full control of the drawing
	// Prevents vertex colour toggle from causing z-fighting
	return true;
}

inline bool RstMtlMax::DrawWireMesh(ID3D9GraphicsWindow *d3dgw, WireMeshData *data)
{
	// Return true to prevent double drawing
	return true;
}

inline void RstMtlMax::StartLines(ID3D9GraphicsWindow *d3dgw, WireMeshData *data)
{
}

inline void RstMtlMax::AddLine(ID3D9GraphicsWindow *d3dgw, DWORD *vert, int vis)
{
}

inline bool RstMtlMax::DrawLines(ID3D9GraphicsWindow *d3dgw)
{
	return false;
}

inline void RstMtlMax::EndLines(ID3D9GraphicsWindow *d3dgw, GFX_ESCAPE_FN fn)
{
}

inline void RstMtlMax::StartTriangles(ID3D9GraphicsWindow *d3dgw, MeshFaceData *data)
{
}

inline void RstMtlMax::AddTriangle(ID3D9GraphicsWindow *d3dgw, DWORD index, int *edgeVis)
{
}

inline bool RstMtlMax::DrawTriangles(ID3D9GraphicsWindow *d3dgw)
{
	return false;
}

inline void RstMtlMax::EndTriangles(ID3D9GraphicsWindow *d3dgw, GFX_ESCAPE_FN fn)
{
}

HRESULT RstMtlMax::Initialize(Mesh *mesh, INode *node)
{
	
	if(stdDualVS)
	{
		stdDualVS->Initialize(mesh, node);
	}
	Draw(node);
	
	return S_OK;
}

HRESULT RstMtlMax::Initialize(MNMesh *mnmesh, INode *node)
{
	if(stdDualVS)
	{
		stdDualVS->Initialize(mnmesh, node);
	}
	Draw(node);
	
	return S_OK;
}

#pragma endregion  // IDX9VertexShader inherited functions

//////////////////////////////////////////////////////////////////////////
// IStdDualVSCallback functions
//	This interface is by our caching mechanism for
//	shader-compatible mesh data.
#pragma region IStdDualVSCallback
//////////////////////////////////////////////////////////////////////////

ReferenceTarget *RstMtlMax::GetRefTarg()
{
	return this;
}

VertexShaderCache *RstMtlMax::CreateVertexShaderCache()
{
	return new VertexShaderCache;
}

HRESULT RstMtlMax::InitValid(Mesh *mesh, INode *node)
{
	int iCacheIdx = m_pMeshCache->SetCachedMesh(mesh,node,GetCOREInterface()->GetTime(),m_RefreshElements);
	m_pMeshCache->GetActiveRenderMesh(iCacheIdx)->Invalidate();
	return(S_OK);
}

HRESULT RstMtlMax::InitValid(MNMesh *mnmesh, INode *node)
{
	HRESULT hr = S_OK;
	int iCacheIdx = m_pMeshCache->SetCachedMNMesh(mnmesh,node,GetCOREInterface()->GetTime(),m_RefreshElements);
	m_pMeshCache->GetActiveRenderMesh(iCacheIdx)->Invalidate();
	return hr;
}
void RstMtlMax::DeleteRenderMeshCache(INode * node)
{
	m_pMeshCache->DeleteRenderMeshCache(node);
}

//bool updateAll = false;
void RstMtlMax::Update(TimeValue t, Interval& valid) 
{	
	if (!ivalid.InInterval(t)) {

		ivalid.SetInfinite();

		}

	bool textureIsValid = true;
	if(m_RefreshTextures)
	{
		for(int i=0; i<m_ShaderInstanceData->GetTextureCount(); i++)
		{
			Interval texsValid = (valid & m_ShaderInstanceData->GetTexture(i)->Validity(t));
			if(texsValid.Empty())
				textureIsValid = false;
		}
		/*
		for(atArray<Texmap*>::iterator it = m_Textures.begin();it<m_Textures.end();it++)
		{
			Interval texsValid = (valid & (*it)->Validity(t));
	// 		if((updateAll || texsValid.Empty()) && (*it)->ClassID()==Class_ID(BMTEX_CLASS_ID, 0))
	// 			dynamic_cast<BitmapTex*>(*it)->ReloadBitmapAndUpdate();
			if(texsValid.Empty())
				textureIsValid = false;
		}
		*/
	}

	valid &= ivalid;

	if(!textureIsValid)
		UpdateParameters(false);
}

#pragma endregion // IStdDualVS functions

void RstMtlMax::RecreateInstanceData(ShaderInstanceData::eInstanceDataType type)
{
	if (m_ShaderInstanceData)
		delete m_ShaderInstanceData;

	switch(type)
	{
	case ShaderInstanceData::eInstanceDataType::INSTANCE_PRESET:
		m_ShaderInstanceData = new ShaderInstancePreset();
		break;
	case ShaderInstanceData::eInstanceDataType::INSTANCE_RAGESHADER:
		m_ShaderInstanceData = new ShaderInstanceRageShader();
		break;
	default:
		m_ShaderInstanceData = new ShaderInstanceRageShader();
		break;
	}
}

void RstMtlMax::UpdateMaterialPresetParameters( bool bRedraw )
{
	Interface *ip = GetCOREInterface();

	//variables
	int NumFloats = 0; 
	int NumInts = 0;
	int	NumTextures = 0;
	int NumVec2s = 0;
	int NumVec3s = 0;
	int NumVec4s = 0;
	bool IsTerrain = IsTerrainShader();
	bool IsBlend = IsBlendShader();

	Vector4 vCurrVec4;
	Vector3 vCurrVec3;
	Vector2 vCurrVec2;
	float fCurrVar;

#if RSTMTL_HW_SHADER
	// Update the lighting flag
	m_ElemContainer.SetIntParam("maxLightingToggle", m_EffectParser, HasLighting() ? 1 : 0 );
	m_ElemContainer.SetIntParam("maxAmbLightingToggle", m_EffectParser, HasAmbLighting() ? 1 : 0 );
	m_ElemContainer.SetIntParam("useAlphaFromDiffuse", m_EffectParser, UsesDiffuseMapAlpha() ? 1 : 0 );
#endif // RSTMTL_HW_SHADER

	const MaterialPreset& fullPreset = *(MaterialPresetManager::GetCurrent().LoadPresetFromLibrary(m_ShaderName));
	const atArray<TypeBase*>& variableArray = fullPreset.GetVariableArray();

	for(int i=0;i<variableArray.GetCount();i++)
	{
		TypeBase* variable = variableArray[i];
		bool Locked = variable->IsLocked();

		switch(variable->GetType())
		{
		case TypeBase::kFloat:
			{
				FloatType* floatType = static_cast<FloatType*>(variable);
				fCurrVar = floatType->GetValue();
				m_ElemContainer.SetFloatParam(floatType->GetVariableName(), m_EffectParser, fCurrVar);
				NumFloats++;
				break;
			}

		case TypeBase::kTextureMap:
			{
#if RSTMTL_HW_SHADER

				// Terrain is a special case as it is not derived from
				// the megashader
				if(NumTextures > m_ShaderInstanceData->GetTextureCount()-1)
					continue;

				Texmap *colourMap = m_ShaderInstanceData->GetTexture(NumTextures);

				if( IsTerrain || IsBlend )
					LoadTexmap( colourMap, NumTextures );
				else
				{

					// All shaders need a default alpha texture channel
					// which is populated with white if the shader does not 
					// have one
					if( i == 0 )
					{
						LoadTexmap( colourMap, NumTextures );

						if( m_ShaderInstanceData->GetTextureAlphaCount() > NumTextures )
							LoadTexmap( m_ShaderInstanceData->GetTextureAlpha(NumTextures), NumTextures+1 );
						else
							LoadTexmap( NULL, NumTextures+1 );
					}
					else 
						LoadTexmap( colourMap, NumTextures+1 );
				}
#endif // RSTMTL_HW_SHADER
			}
			NumTextures++;
			break;	
		case TypeBase::kVector2:
			{
				Vector2Type* vector2Type = static_cast<Vector2Type*>(variable);
				vCurrVec2 = vector2Type->GetValue();
				m_ElemContainer.SetColorParam(vector2Type->GetVariableName(), m_EffectParser, Point4(vCurrVec2.x, vCurrVec2.y, 0.0f, 0.0f));
				NumVec2s++;
				break;
			}
		case TypeBase::kVector3:
			{
				Vector3Type* vector3Type = static_cast<Vector3Type*>(variable);
				vCurrVec3 = vector3Type->GetValue();
				m_ElemContainer.SetColorParam(vector3Type->GetVariableName(), m_EffectParser, Point4(vCurrVec3.x, vCurrVec3.y, vCurrVec3.z, 0.0f));
				NumVec4s++;
				break;
			}
		case TypeBase::kVector4:
			{
				Vector4Type* vector4Type = static_cast<Vector4Type*>(variable);
				vCurrVec4 = vector4Type->GetValue();
				m_ElemContainer.SetColorParam(vector4Type->GetVariableName(), m_EffectParser, Point4(vCurrVec4.x, vCurrVec4.y, vCurrVec4.z, vCurrVec4.w));
				NumVec4s++;
				break;
			}
		default:
			break;
		}			
	}

	if(ip->GetUseEnvironmentMap())
		LoadEnvironmentTexmap(ip->GetEnvironmentMap());
	else
		LoadEnvironmentTexmap(NULL);

	if(bRedraw)
		ip->RedrawViews( GetCOREInterface()->GetTime() );
}

void RstMtlMax::UpdateParameters( bool bRedraw )
{
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(m_ShaderName);

	if ( p_Entry && p_Entry->IsMaterialPreset() )
	{
		UpdateMaterialPresetParameters(true);
		return;
	}

	grcEffect* p_Shader = NULL;
	Interface *ip = GetCOREInterface();

	if(IsPreset())
	{
		if(!p_Entry)
			return;

		p_Shader = &p_Entry->m_Preset->GetBasis();
	}
	else
	{
		p_Shader = RstMax::FindShader(m_ShaderName);
	}

	if(!p_Shader)
		return;

	//variables
	int NumVars = p_Shader->GetInstancedVariableCount();
	int NumFloats = 0; 
	int NumInts = 0;
	int	NumTextures = 0;
	int NumVec3s = 0;
	int NumVec4s = 0;
	bool IsTerrain = IsTerrainShader();
	bool IsBlend = IsBlendShader();

	Vector4 vCurrVec4;
	Vector3 vCurrVec3;

#if RSTMTL_HW_SHADER
	// Update the lighting flag
	m_ElemContainer.SetIntParam("maxLightingToggle", m_EffectParser, HasLighting() ? 1 : 0 );
	m_ElemContainer.SetIntParam("maxAmbLightingToggle", m_EffectParser, HasAmbLighting() ? 1 : 0 );
	m_ElemContainer.SetIntParam("useAlphaFromDiffuse", m_EffectParser, UsesDiffuseMapAlpha() ? 1 : 0 );
#endif // RSTMTL_HW_SHADER

	for(int i=0;i<NumVars;i++)
	{
		grmVariableInfo VarInfo;
		p_Shader->GetInstancedVariableInfo(i,VarInfo);
		grcEffectVar handle = p_Shader->LookupVar(VarInfo.m_Name);

#if !SHADER_ATTR_TREE
		switch(VarInfo.m_Type)
		{
		case grcEffect::VT_FLOAT:
			float fCurrVar;

			GetVariable(i, fCurrVar);
			m_ElemContainer.SetFloatParam( VarInfo.m_ActualName, m_EffectParser, fCurrVar);
			NumFloats++;
			break;
		case grcEffect::VT_TEXTURE:
			{
#if RSTMTL_HW_SHADER
				// Terrain is a special case as it is not derived from
				// the megashader
				if(NumTextures > m_ShaderInstanceData->GetTextureCount()-1)
					continue;

				Texmap *colourMap = m_ShaderInstanceData->GetTexture(NumTextures);

				if( IsTerrain || IsBlend )
					LoadTexmap( colourMap, NumTextures );
				else
				{

					// All shaders need a default alpha texture channel
					// which is populated with white if the shader does not 
					// have one
					if( i == 0 )
					{
						LoadTexmap( colourMap, NumTextures );

						if( m_ShaderInstanceData->GetTextureAlphaCount() > NumTextures )
							LoadTexmap( m_ShaderInstanceData->GetTextureAlpha(NumTextures), NumTextures+1 );
						else
							LoadTexmap( NULL, NumTextures+1 );
					}
					else 
						LoadTexmap( colourMap, NumTextures+1 );
				}
#endif // RSTMTL_HW_SHADER
			}
			NumTextures++;
			break;	
		case grcEffect::VT_VECTOR2:
			{
				Vector2 vFloat2;
				GetVariable( i, vFloat2 );
				m_ElemContainer.SetColorParam(VarInfo.m_ActualName, m_EffectParser, Point4(vFloat2.x, vFloat2.y, 0.0f, 0.0f));
			}
			break;
		case grcEffect::VT_VECTOR3:
			GetVariable(i, vCurrVec3);
			m_ElemContainer.SetColorParam(VarInfo.m_ActualName, m_EffectParser, Point4(vCurrVec3.x, vCurrVec3.y, vCurrVec3.z, 0.0f));
			NumVec4s++;
			break;
		case grcEffect::VT_VECTOR4:
			GetVariable(i, vCurrVec4);
			m_ElemContainer.SetColorParam(VarInfo.m_ActualName, m_EffectParser, Point4(vCurrVec4.x, vCurrVec4.y, vCurrVec4.z, vCurrVec4.w));
			NumVec4s++;
			break;
		case grcEffect::VT_BOOL_DONTUSE:
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		case grcEffect::VT_INT:
		default:
			break;
		}			
#else
		switch(VarInfo.m_Type)
		{
		case grcEffect::VT_FLOAT:
			{
				CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot, VarInfo.m_Name);
				if(!pNode)
					continue;
				m_ElemContainer.SetFloatParam(VarInfo.m_Name, m_EffectParser, pNode->Value().m_Float);
				NumFloats++;
			}
			break;
		case grcEffect::VT_INT_DONTUSE:
			break;
		case grcEffect::VT_TEXTURE:
#if RSTMTL_HW_SHADER
			if( IsTerrain )
				LoadTexmap((Texmap*)GetReference(NumTextures), NumTextures );
			else
			{

				// All shaders need a default alpha texture channel
				// which is populated with white if the shader does not 
				// have one
				if( i == 0 )
				{
					LoadTexmap((Texmap*)GetReference(NumTextures), NumTextures );

					//				if( m_TextureAlphas.GetCount() > NumTextures )
					//					LoadTexmap( m_TextureAlphas[NumTextures], NumTextures+1 );
					//				else
					LoadTexmap( NULL, NumTextures+1 );
				}
				else 
					LoadTexmap((Texmap*)GetReference(NumTextures), NumTextures+1 );
			}
#endif // RSTMTL_HW_SHADER
			NumTextures++;
			break;	
		case grcEffect::VT_VECTOR2:
			break;
		case grcEffect::VT_VECTOR3:
			break;
		case grcEffect::VT_VECTOR4:
			{
				CRstShaderValueNode *pNode = FindSingleShaderValueNode(m_shaderValRoot, VarInfo.m_Name);
				if(!pNode)
					continue;
				Vector4 &vCurrVar = *pNode->Value().m_Vector4;
				m_ElemContainer.SetColorParam(VarInfo.m_Name, m_EffectParser, Point4(vCurrVar.x, vCurrVar.y, vCurrVar.z, vCurrVar.w));
			}
			break;
		case grcEffect::VT_BOOL_DONTUSE:
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		default:

			break;
		}			
#endif
	}

	if(ip->GetUseEnvironmentMap())
		LoadEnvironmentTexmap(ip->GetEnvironmentMap());
	else
		LoadEnvironmentTexmap(NULL);

	if(bRedraw)
		ip->RedrawViews( GetCOREInterface()->GetTime() );
}
#endif // RSTMTL_HW_SHADER
