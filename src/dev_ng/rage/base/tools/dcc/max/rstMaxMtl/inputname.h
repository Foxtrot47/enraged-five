#ifndef __RST_INPUTNAME_H__
#define __RST_INPUTNAME_H__

#include "atl/string.h"

namespace rage {

////////////////////////////////////////////////////////////////////////////////
class InputName
{
public:
	bool DoModal(HWND ParentWnd,const atString& r_Message,atString& r_Return);

protected:
	void OnInitDialog(HWND Wnd);
	void OnOk();

	static INT_PTR CALLBACK InputNameDlgProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam);

	HWND m_Wnd;
	atString m_Message;
	atString m_Return;
};

} //namespace rage {

#endif //__RST_INPUTNAME_H__