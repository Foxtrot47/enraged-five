#ifndef __RST_MAX_DIALOGSHADER_H__
#define __RST_MAX_DIALOGSHADER_H__

#include "rstMaxMtl/dialog.h"
#include "vector/vector3.h"
#include "rstMaxMtl/mtl.h"
#include "MaterialPresets/MaterialPreset.h"

namespace rage {

class RstMtlMax;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
class RstMaxShaderDlg : public ParamDlg
{
public:
	friend class RstMaxMtlDlg;

	RstMaxShaderDlg(RstMtlMax* p_Mtl, IMtlParams* p_MtlParams);
	virtual ~RstMaxShaderDlg();

	// from ParamDlg
	Class_ID ClassID() { return RSTMATERIAL_CLASS_ID; }
	void SetThing(ReferenceTarget *m) {}
	ReferenceTarget* GetThing();
	void SetTime(TimeValue t) {}
	void ReloadDialog() {}
	void DeleteThis();
	void ActivateDlg(BOOL onOff) {}
	int FindSubTexFromHWND(HWND hwnd);
	//

//	void SetMtlDlg(RstMaxMtlDlg* p_MtlDlg) { mp_MtlDlg = p_MtlDlg; }

	void AddStatic(const atString& r_Name);
	void AddStaticString(const atString& r_Name,const atString& r_String, grcEffect::VarType ctrlSubst);
	void AddStaticString(const atString& r_Name,const atString& r_String, TypeBase::eVariableType ctrlSubst);
#if SHADER_ATTR_TREE
	void AddFloatSpinner(const atString& r_Name,CRstShaderValueNode *pVal,float Min,float Max,float Step);
	void AddIntSpinner(const atString& r_Name,CRstShaderValueNode *pVal,s32 Min,s32 Max,s32 Step);
	void AddMapSelect(const atString& r_Name,CRstShaderValueNode *pVal,const atString& r_MapName, s32 texRefNum);
	void AddMapSelect(const atString& r_Name,CRstShaderValueNode *pVal,const atString& r_MapName,const atString& r_AlphaName, s32 texRefNum1, s32 texRefNum2);
	void AddVector2Select(const atString& r_Name,CRstShaderValueNode *pVal,float Min, float Max, float Step);
	void AddVector3Select(const atString& r_Name,CRstShaderValueNode *pVal,float Min,float Max,float Step);
	void AddVector4Select(const atString& r_Name,CRstShaderValueNode *pVal,float Min,float Max,float Step);
	void AddBoolSelect(const atString& r_Name,CRstShaderValueNode *pVal);
#else
	void AddFloatSpinner(const atString& r_Name,s32 Idx,float Min,float Max,float Step);
	void AddIntSpinner(const atString& r_Name,s32 Idx,s32 Min,s32 Max,s32 Step);
	void AddMapSelect(const atString& r_Name,s32 Idx,const atString& r_MapName, atString& r_TemplateName);
	void AddMapSelect(const atString& r_Name,s32 Idx,const atString& r_MapName,const atString& r_AlphaName, atString& r_TemplateName);
	void AddVector2Select(const atString& r_Name,s32 Idx,float Min, float Max, float Step);
	void AddVector3Select(const atString& r_Name,s32 Idx,float Min,float Max,float Step);
	void AddVector4Select(const atString& r_Name,s32 Idx,float Min,float Max,float Step);
	void AddBoolSelect(const atString& r_Name,s32 Idx);
#endif
	void Reset();
	HWND Create(TSTR title="Shader Parameters");

	static INT_PTR CALLBACK RstMaxMtlDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);
	void UpdateMapName(s32 Index,const atString& r_MapName);

	typedef void (*ControlValueChangeEventFunctor)(const char* shaderName, const char* attributeName);
	void SetControlValueChangeFunctor(ControlValueChangeEventFunctor func) { m_ChangeFunctor = func; }

protected:

	void OnInitDialog(HWND DlgWnd);
	void OnSpinnerChange(s32 SpinnerID);
	void OnButtonPressed(s32 ButtonID);
	void OnDestroy();

	ControlValueChangeEventFunctor m_ChangeFunctor;

	TexDADMgr m_DadMgr;
	DialogRes m_DialogRes;
	HWND m_Wnd;
	RstMtlMax* mp_Mtl;
	IMtlParams* mp_MtlParams;
	s32 m_Width;
	s32 m_Height;
	s32 m_ControlHeight;
	s32 m_ControlGap;
	s32 m_BorderWidth;
	s32 m_LabelWidth;
	u32 m_ID;
#if SHADER_ATTR_TREE
	static u32 m_TexButtonID;
#endif
	bool m_Initialising;

	struct FloatSpinnerWdgt
	{
#if SHADER_ATTR_TREE
		CRstShaderValueNode *m_pVal;
#else
		s32 Idx;
#endif
		f32 Min;
		f32 Max;
		f32 Step;

		u32 SpinnerID;
		u32 EditID;
		ISpinnerControl* p_SpinCtrl;
		ICustEdit* p_EditCtrl;
		FloatSpinnerWdgt(){Idx=-1;}
	};
	static FloatSpinnerWdgt dummyFloatWdgt;

	struct IntSpinnerWdgt
	{
#if SHADER_ATTR_TREE
		CRstShaderValueNode *m_pVal;
#else
		s32 Idx;
#endif
		s32 Min;
		s32 Max;
		s32 Step;

		u32 SpinnerID;
		u32 EditID;
		ISpinnerControl* p_SpinCtrl;
		ICustEdit* p_EditCtrl;
		IntSpinnerWdgt(){Idx=-1;}
	};
	static IntSpinnerWdgt dummyIntWdgt;

	struct TextureWdgt
	{
#if SHADER_ATTR_TREE
		CRstShaderValueNode *m_pVal;
#else
		s32 Idx;
#endif
		s32 ButtonID;
		ICustButton* p_ButCtrl;
		s32 ButtonAlphaID;
		ICustButton* p_ButAlphaCtrl;

		ICustButton* p_SutSubAttrCtrl;
		s32 ButtonSubAttrDialogID;
		TextureWdgt(){Idx=-1;}
	};
	static TextureWdgt dummyTextureWdgt;

	struct Vector2Wdgt
	{
#if SHADER_ATTR_TREE
		CRstShaderValueNode *m_pVal;
#else
		s32 Idx;
#endif
		FloatSpinnerWdgt Float[2];
		Vector2Wdgt(){Idx=-1;}
	};
	static Vector2Wdgt dummyVector2Wdgt;

	struct Vector3Wdgt
	{
#if SHADER_ATTR_TREE
		CRstShaderValueNode *m_pVal;
#else
		s32 Idx;
#endif
		FloatSpinnerWdgt Float[3];
		s32 ButtonID;
		Vector3Wdgt(){Idx=-1;}
	};
	static Vector3Wdgt dummyVector3Wdgt;

	struct Vector4Wdgt
	{
#if SHADER_ATTR_TREE
		CRstShaderValueNode *m_pVal;
#else
		s32 Idx;
#endif
		FloatSpinnerWdgt Float[4];
		s32 ButtonID;
		Vector4Wdgt(){Idx=-1;}
	};
	static Vector4Wdgt dummyVector4Wdgt;

	struct BoolWdgt
	{
#if SHADER_ATTR_TREE
		CRstShaderValueNode *m_pVal;
#else
		s32 Idx;
#endif
		s32 ButtonID;
		BoolWdgt(){Idx=-1;}
	};
	static BoolWdgt dummyBoolWdgt;


	atArray<FloatSpinnerWdgt> m_FloatSpinners;
	atArray<IntSpinnerWdgt> m_IntSpinners;
	atArray<TextureWdgt> m_Textures;
	atArray<Vector2Wdgt> m_Vector2s;
	atArray<Vector3Wdgt> m_Vector3s;
	atArray<Vector4Wdgt> m_Vector4s;
	atArray<BoolWdgt> m_Bools;
};

class RstMaxErrorDlg : public ParamDlg
{
public:
	friend class RstMaxMtlDlg;

	RstMaxErrorDlg(RstMtlMax* p_Mtl, IMtlParams* p_MtlParams, atString errormsg);
	virtual ~RstMaxErrorDlg();
	void Reset();

	// from ParamDlg
	Class_ID ClassID() { return RSTMATERIAL_CLASS_ID; }
	void SetThing(ReferenceTarget *m) {}
	ReferenceTarget* GetThing();
	void SetTime(TimeValue t) {}
	void ReloadDialog() {}
	void DeleteThis();
	void ActivateDlg(BOOL onOff) {}
	//
	HWND Create(TSTR title="Shader parse errors");

	DialogRes m_DialogRes;
	HWND m_Wnd;
	RstMtlMax* mp_Mtl;
	IMtlParams* mp_MtlParams;

};

} //namespace rage {

#endif //__RST_MAX_DIALOGSHADER_H__
