#ifndef __RST_DBADDPRESET_H__
#define __RST_DBADDPRESET_H__

#include "atl/string.h"

namespace rage {

struct rageRstShaderDbEntry;

////////////////////////////////////////////////////////////////////////////////
class DbAddPreset
{
public:
	DbAddPreset();

	bool DoModal(HWND ParentWnd,atString& r_Path,atString& r_NewPresetName, const char *pShaderName=NULL, const char *pComment=NULL);

protected:
	void OnInitDialog(HWND Wnd);
	void OnCommand(s32 ID);

	static INT_PTR CALLBACK DbAddPresetDlgProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam);

	atString m_PresetName;
	atString m_Path;
	atString m_ShaderName;
	HWND m_Wnd;
	s32 m_BtnOk;
	s32 m_BtnCancel;
	s32 m_LstShaders;
	s32 m_EdtName;
	s32 m_EdtComment;
};

} //namespace rage {

#endif //__RST_DBADDPRESET_H__