#include <3dsmaxport.h>

#include "rstMaxMtl/dialogMtlPreset.h"
#include "rstMaxMtl/mtl.h"
#include "rstMaxMtl/utility.h"
#include "rstMaxMtl/dialogMtl.h"
#include "rstMaxMtl/rstMax.h"

#include "MaterialPresets/MaterialPresetManager.h"
#include "MaterialPresets/MaterialPreset.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RstMaxMtlPresetDlg::RstMaxMtlPresetDlg(RstMtlMax* p_Mtl, IMtlParams* p_MtlParams):
	mp_Mtl(p_Mtl),
	m_Wnd(NULL),
	mp_MtlParams(p_MtlParams)
{
	Reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RstMaxMtlPresetDlg::~RstMaxMtlPresetDlg()
{
	Reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ReferenceTarget* RstMaxMtlPresetDlg::GetThing() 
{
	return mp_Mtl; 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RstMaxMtlPresetDlg::DeleteThis() 
{
#if !SHADER_ATTR_TREE
	mp_Mtl->mp_DlgMtlPreset = NULL;
#else
	mp_Mtl->m_shaderValRoot->Dialog() = NULL; 
#endif
	//	mp_Mtl->mp_MtlDlg = NULL;
	delete this;
}

void RstMaxMtlPresetDlg::Reset()
{
	if(m_Wnd)
	{
		mp_MtlParams->DeleteRollupPage(m_Wnd);
		DestroyWindow(m_Wnd);
	}

	m_Wnd = NULL;
}

HWND RstMaxMtlPresetDlg::Create(TSTR title)
{
	assert(!m_Wnd);	

	m_Wnd = mp_MtlParams->AddRollupPage(g_hDllInstance,
		MAKEINTRESOURCE(IDD_DLG_MTLPRESETS),
		RstMaxMtlPresetDlgProc,
		title,
		(LPARAM)this);

	return m_Wnd;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK RstMaxMtlPresetDlg::RstMaxMtlPresetDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	RstMaxMtlPresetDlg* p_ParamDlg = DLGetWindowLongPtr<RstMaxMtlPresetDlg*>( DlgWnd );

	switch(msg)
	{
	case WM_INITDIALOG:
		((RstMaxMtlPresetDlg*)lParam)->OnInitDialog(DlgWnd);
		return TRUE;
	case WM_COMMAND:
		p_ParamDlg->OnButtonPressed(LOWORD(wParam));
		return FALSE;
	case TTN_SHOW:
		return FALSE;
	};

	return FALSE;
}

void RstMaxMtlPresetDlg::OnInitDialog(HWND DlgWnd)
{
	m_Wnd = DlgWnd;
	SetWindowLongPtr(DlgWnd,GWLP_USERDATA,(LONG_PTR)this);
}

void RstMaxMtlPresetDlg::OnButtonPressed(s32 ButtonID)
{
	switch (ButtonID)
	{
	case IDC_BTN_SAVE_MATPRESET:
		OnSavePreset();
		break;
	case IDC_BTN_UNLOCK_MATPRESET:
		OnUnlockPreset();
		break;
	}
}

void RstMaxMtlPresetDlg::OnSavePreset()
{
	if (!mp_Mtl)
		return;

	TCHAR szFilters[] = _T("Material Presets (*.mpo)\0*.mpo\0\0");
	TCHAR szFilePathName[RAGE_MAX_PATH] = _T("default");
	TCHAR szInitialDirectory[RAGE_MAX_PATH] = _T("");

	const char* cachedPresetPath = MaterialPresetManager::GetCurrent().GetCachedTemplateSavePath();

	if (cachedPresetPath && strlen(cachedPresetPath) > 0)
	{
		safecpy(szInitialDirectory, cachedPresetPath, RAGE_MAX_PATH);
	}
	else
	{
		if (sysGetEnv("RS_PROJROOT", szInitialDirectory, RAGE_MAX_PATH) == true)
		{
			safecat(szInitialDirectory, "\\assets\\metadata\\materials", RAGE_MAX_PATH);
		}
	}

	OPENFILENAME ofn = {0};
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = m_Wnd;
	ofn.hInstance = NULL;
	ofn.lpstrFilter = szFilters;
	ofn.lpstrCustomFilter = NULL;
	ofn.nFilterIndex = 0;
	ofn.lpstrFile = szFilePathName;
	ofn.lpstrInitialDir = szInitialDirectory;
	ofn.lpstrDefExt = _T("mpo");
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrTitle = _T("Save Material Preset File...");
	ofn.Flags = OFN_OVERWRITEPROMPT;
	ofn.pvReserved = 0;
	ofn.dwReserved = 0;
	ofn.FlagsEx = 0;

	if (GetSaveFileName(&ofn) == FALSE)
		return;

	// Cache the last path
	char cachedSavePath[RAGE_MAX_PATH];
	ASSET.RemoveNameFromPath(cachedSavePath, RAGE_MAX_PATH, szFilePathName);
	MaterialPresetManager::GetCurrent().SetCachedTemplateSavePath(cachedSavePath);

	const MaterialPreset& fullPreset = *(MaterialPresetManager::GetCurrent().LoadPresetFromLibrary(mp_Mtl->GetShaderName()));

	atString savePath = atString(szFilePathName);

	// Make sure we are saving to the metadata directory.
	if (MaterialPresetManager::GetCurrent().IsPathInPresetDirectory(savePath))
	{
		MaterialPreset* savePreset = rage_new MaterialPreset();
		savePreset->Copy(fullPreset, true);

		atString relativePresetPath = MaterialPresetManager::GetCurrent().MakeRelativePresetPath(savePath);
		
		char relativePresetPathNoExt[RAGE_MAX_PATH];
		ASSET.RemoveExtensionFromPath(relativePresetPathNoExt, sizeof(relativePresetPathNoExt), relativePresetPath.c_str());

		// Fill our save preset up with data from Max (shaderinstancedata).
		PopulateMaterialPreset(*savePreset);

		MaterialPreset* newPreset = rage_new MaterialPreset();

		newPreset->SetPresetLevel(kObject);
		newPreset->SetShaderName(fullPreset.GetShaderName());
		newPreset->SetFriendlyName(relativePresetPathNoExt);

		if (fullPreset.GetPresetLevel() == kObject)
		{
			// Don't parent to another .mpo file.
			newPreset->SetParentName(savePreset->GetParentName());
		}
		else
		{
			// Re-parent the saved .mpo file to the current preset.
			atString newParentPath;
			const char* newParentName = mp_Mtl->GetShaderName();
			newParentPath = MaterialPresetManager::GetCurrent().GetPresetPath();
			newParentPath += newParentName;
			newPreset->SetParentName(newParentPath.c_str());
		}

		// Generate our diff'd preset that only leaves us with variables that were set specific to the .mpo file.
		MaterialPresetManager::GetCurrent().GenerateDiffPreset(fullPreset, *savePreset, *newPreset);

		// We don't need any property values at the .mpo level.
		newPreset->ClearProperties();

		MaterialPresetManager::GetCurrent().SavePreset(savePath, *newPreset);
		mp_Mtl->SetShaderName(relativePresetPath);

		delete newPreset;
		delete savePreset;
	}
	else
	{
		char buffer[1024];
		sprintf_s(buffer, 1024, "Preset could not be saved. Please save it under the preset directory %s", MaterialPresetManager::GetCurrent().GetPresetPath());
		MessageBox(m_Wnd, buffer, "Save Preset Error", MB_ICONWARNING);
	}
}

void RstMaxMtlPresetDlg::PopulateMaterialPreset(MaterialPreset& materialPreset)
{
	RstMtlMax* p_RstMtl = (RstMtlMax*)mp_Mtl;
	rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(p_RstMtl->GetShaderName());
	if ( p_Entry == NULL || p_Entry->m_MaterialPreset == NULL )
		return;

	const MaterialPreset& basePreset = *(p_Entry->m_MaterialPreset);

	const atArray<TypeBase*>& variables = p_Entry->m_MaterialPreset->GetVariableArray();
	s32 i,NumVars = variables.GetCount();
	for(i=0;i<NumVars;i++)
	{
		TypeBase* variable = variables[i];
		TypeBase::eVariableType presetVarType = variable->GetType();
		int varIndex = variable->GetIndex();
		atString varName(variable->GetName());  //Rename this...
		atString variableName(variable->GetVariableName());  //Actual variable name.
		switch(presetVarType)
		{
		case TypeBase::kFloat:
			{
				const FloatType* baseFloatType = basePreset.GetFloat(varIndex);

				f32 val;
				p_RstMtl->GetVariable(varIndex,val);

				FloatType* parentFloat = NULL;
				if (materialPreset.FindFloat(baseFloatType->GetName(), parentFloat))
					parentFloat->SetValue(val);
			}

			break;
		case TypeBase::kInt:
			{
				const IntType* baseIntType = basePreset.GetInt(varIndex);

				f32 val;
				p_RstMtl->GetVariable(varIndex,val);

				IntType* parentInt = NULL;
				if (materialPreset.FindInt(baseIntType->GetName(), parentInt))
					parentInt->SetValue(val);
			}

			break;
		case TypeBase::kTextureMap:
			{
				const TexMapType* baseTexMapType = basePreset.GetTexMap(variable->GetIndex());

				atString val;
				p_RstMtl->GetVariable(varIndex,val);

				TexMapType* parentTexMap = NULL;
				if (materialPreset.FindTexMap(baseTexMapType->GetName(), parentTexMap))
					parentTexMap->SetValue(val);
			}

			break;

		case TypeBase::kVector2:
			{
				const Vector2Type* baseVector2Type = basePreset.GetVector2(variable->GetIndex());

				Vector2 val;
				Point2 retval;
				p_RstMtl->GetVariable(varIndex, val);

				Vector2Type* parentVec2 = NULL;
				if (materialPreset.FindVector2(baseVector2Type->GetName(), parentVec2))
					parentVec2->SetValue(val);
			}

			break;

		case TypeBase::kVector3:
			{
				const Vector3Type* baseVector3Type = basePreset.GetVector3(variable->GetIndex());

				Vector3 val;
				p_RstMtl->GetVariable(varIndex,val);

				Vector3Type* parentVec3 = NULL;
				if (materialPreset.FindVector3(baseVector3Type->GetName(), parentVec3))
					parentVec3->SetValue(val);
			}

			break;

		case TypeBase::kVector4:
			{
				const Vector4Type* baseVector4Type = basePreset.GetVector4(variable->GetIndex());

				Vector4 val;
				p_RstMtl->GetVariable(varIndex,val);

				Vector4Type* parentVec4 = NULL;
				if (materialPreset.FindVector4(baseVector4Type->GetName(), parentVec4))
					parentVec4->SetValue(val);
			}

			break;

		default:
			break;
		}
	}
}

void RstMaxMtlPresetDlg::OnUnlockPreset()
{
	MaterialPreset* fullPreset = MaterialPresetManager::GetCurrent().LoadPresetFromLibrary(mp_Mtl->GetShaderName());

	if (!fullPreset)
		return;

	for (int i = 0; i < fullPreset->GetNumVariables(); i++)
	{
		TypeBase* var = const_cast<TypeBase*>(fullPreset->GetVariable(i));
		var->SetLocked(false);
	}

	mp_Mtl->mp_MtlDlg->SetupUI();
}
