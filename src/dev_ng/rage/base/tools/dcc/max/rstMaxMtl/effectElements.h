//**************************************************************************/
// Copyright (c) 1998-2008 Autodesk, Inc.
// All rights reserved.
// 
// These coded instructions, statements, and computer programs contain
// unpublished proprietary information written by Autodesk, Inc., and are
// protected by Federal copyright law. They may not be disclosed to third
// parties or copied or duplicated in any form, in whole or in part, without
// the prior written consent of Autodesk, Inc.

/**********************************************************************
*<
FILE: EffectElements.h

DESCRIPTION:		This project represents a fairly simple hardware shader
					for 3ds Max 2008.  Each chunk of necessary implementatino
					has been broken down in order to show how the interlocking
					parts are required to fit together to produce the final effect.
					
					This header contains all the definitions needed to create
					and manage the parameters defined in the loaded effects
					files.  DemoHWMaterial will pass an instance of the 
					ElementContainer to the effect parser, and it will use
					the IEffectManager interface to set the necessary data
					on this class

					The EffectElement class is the base of the interface
					between the Max scene and the CG shader parameters

CREATED BY:			Stephen Taylor, DevTech M&E

HISTORY:

*>	Copyright (c) 2008, All Rights Reserved.
**********************************************************************/

#ifndef _EFFECT_ELEMENTS_H
#define _EFFECT_ELEMENTS_H

//////////////////////////////////////////////////////////////////////////
#include <RTMax.h>
#include <id3d9graphicswindow.h>

#if (MAX_RELEASE >= 12000)
#include "IFileResolutionManager.h"
#endif 
class EffectElements;
class TextureElement;

//////////////////////////////////////////////////////////////////////////
// ElementContainer.
//  This is the interface class implementing the IEffectManager interface
//	This classes responsibilities are accepting parameters from the effect
//	parser and storing them to be animated/exposed to the user whatever.
//
//	IEffectManager:  This is the inwards interface.  A pointer to this
//		interface is passed to the effect parser.  The parser calls
//		the Set*Param functions to give the EffectManager a parameter
//		to rememeber when one is encountered in the fx file.
//	IParameterManager: This is the outwards interface.  A pointer to this
//		interface is passed to somebody, and they can use it to query
//		the ParameterManager to fill in the values for various parameters
//
//	This base of this class was taken from DxStdMtl2 sample files.
//////////////////////////////////////////////////////////////////////////

class ElementContainer 
	:	public IEffectManager		// Interface allows setting of parameters
	,	public IParameterManager	// Interface allows reading of parameters
	,	public ILightManager		// Interface specific for accessing light data
{
	Tab<EffectElements*> m_dElements;		// An array of all parameters for the current effect

	ID3D9GraphicsWindow	*m_pGWindow;		// A pointer to the rendering graphics window.
	
	D3DXHANDLE			m_hTechnique;		// Not sure what this is used for :-)
	TSTR				m_sTechniqueName;	// The name of the above technique
public:
	LPDIRECT3DDEVICE9	m_pd3dDevice;		// The D3Device used by m_pGWindow
	ElementContainer();
	~ElementContainer();

	//////////////////////////////////////////////////////////////////////////
#pragma region IEffectManager
	//////////////////////////////////////////////////////////////////////////
	UINT GetDirectXVersion(){return IEffectManager::kDirectX9;}

	/** Set values */
	void SetFloatParam(D3DXHANDLE handle, TCHAR * name,TCHAR * uiName, float val,float min, float max, float step,MaxSemantics semantic );
	void SetIntParam(D3DXHANDLE handle, TCHAR * name, TCHAR * uiName,int val, int min, int max, int step,MaxSemantics semantic );
	void SetColorParam(D3DXHANDLE handle,TCHAR * name, TCHAR * uiName,D3DXVECTOR4 color, MaxSemantics semantic);
	void SetTextureParam(D3DXHANDLE handle, TCHAR * name,TCHAR * uiName, TCHAR * filename, MaxSemantics semantic, bool mappingEnabled, int mappingChannel);
	void SetLightParam(D3DXHANDLE handle,TCHAR * name,TCHAR * uiName, bool target,MaxSemantics semantic);
	void SetTransformParam(D3DXHANDLE handle, MaxSemantics semantic);
	void SetPoint3Param(D3DXHANDLE handle, TCHAR * paramName, TCHAR * uiName,D3DXVECTOR3 val,float min, float max, float step,MaxSemantics semantic);
	void SetPoint4Param(D3DXHANDLE handle, TCHAR * paramName, TCHAR * uiName,D3DXVECTOR4 val,float min, float max, float step,MaxSemantics semantic);
	void SetGeometryParam(D3DXHANDLE handle,MaxSemantics semantic){};
	void SetEnvironmentParam(D3DXHANDLE handle, MaxSemantics semantic){};
	void SetBooleanParam(D3DXHANDLE handle, TCHAR *paramName, TCHAR *uiName, BOOL val, MaxSemantics semantic);
	void SetTechnique(D3DXHANDLE handle, TCHAR * techniqueName, bool bDefault);

#pragma endregion		// IEffectManager inherited functions

	//////////////////////////////////////////////////////////////////////////
#pragma region IParameterManager
	//////////////////////////////////////////////////////////////////////////

	/** Get values */
	int GetNumberOfParams();
	virtual MaxSemantics GetParamSemantics(int index);
	const TCHAR * GetParamName(int index);
	bool GetParamData(LPVOID data, int index);
	bool GetParamData(LPVOID data, const TCHAR * paramName);
	int GetParamType(int index);
	int GetParamType(const TCHAR* paramName);
#if (MAX_RELEASE >= 12000)
	int GetParamSize(int index);
#endif // (MAX_RELEASE >= MAX_RELEASE_R12)

#pragma endregion	// IParameterManager inherited functions

	//////////////////////////////////////////////////////////////////////////
#pragma region ILightManager
	//////////////////////////////////////////////////////////////////////////

	/** Get the lighting data for the named parameter */
	ILightingData * GetLightingData(const TCHAR* paramName);

#pragma endregion		// ILightingManager functions

	/** Basic management functions, called only by DemoHWMaterial */
	inline void SetGWWindow(ID3D9GraphicsWindow* pGWnd) { m_pGWindow = pGWnd; }
	inline void SetD3Device(LPDIRECT3DDEVICE9 pDevice)	{ m_pd3dDevice = pDevice; }

	inline void GetWinDimension(int& w, int& h)			{ if (m_pGWindow) m_pGWindow->GetWindowDimension(w, h); }
	inline const char* GetTechniqueName() { return m_sTechniqueName.data(); }

	void LoadTextureParam(PBBitmap* pBmInfo, IEffectParser* pParser);
	void LoadTextureParam(TSTR sTexPath, int texId, IEffectParser* pParser, float scale);
	void LoadTextureParam(TSTR sTexPath, TextureElement *pTexEle, IEffectParser* pParser, float scale, bool isEnvTexture=false);
	void LoadEnvironmentTextureParam(TSTR sTexPath, IEffectParser* pParser, float scale);
	
	void SetFloatParam( int floatId, IEffectParser* pParser, float val );
	void SetIntParam(int intId, IEffectParser* pParser, int val);
//	void SetBooleanParam(int boolId, IEffectParser* pParser, bool val);
	void SetPoint4Param(int colId, IEffectParser* pParser, Point4 val);
	void SetColorParam(int colId, IEffectParser* pParser, Point4 val);
	
	void SetFloatParam( const TCHAR * paramName, IEffectParser* pParser, float val );
	void SetIntParam( const TCHAR * paramName, IEffectParser* pParser, int val);
//	void SetBooleanParam( const TCHAR * paramName, IEffectParser* pParser, bool val);
	void SetPoint4Param( const TCHAR * paramName, IEffectParser* pParser, Point4 val);
	void SetColorParam( const TCHAR * paramName, IEffectParser* pParser, Point4 val);
	void DeleteAllElements();

	void FreeD3Dtextures();

private:
	inline int NumberOfElements(){return m_dElements.Count();}
	int NumberOfElementsByType(int Type);
	EffectElements* GetElement(int index);
	EffectElements* GetElementByType(int index, int Type);
	EffectElements* GetElementByName(const TCHAR* name);
	
	TextureElement * FindTextureElement(TCHAR * name);
	void AddAParam(EffectElements* param, D3DXHANDLE handle, TCHAR* name, MaxSemantics semantic);


	bool CreateTextureFromTIF( TSTR sTexPath, TextureElement* pTexEle, float scale);

};

//////////////////////////////////////////////////////////////////////////
// Class EffectElements
//	This class is the base for the classes that contain the data set and read
//	from ElementContainer through the IParameterManager and IEffectManager interfaces
//	This is taken almost verbatim from the DxStdMtl2 sample files
//////////////////////////////////////////////////////////////////////////
class EffectElements
{
	TSTR UIName;				//Named used for the Dialog resource
	TSTR ParameterName;			//The actual parameter name in the effect file
	D3DXHANDLE hParameter;		//A handle to the parameter
	int elementStyle;			//The type of element present
	bool valid;					//Is the element up to date
	MaxSemantics semantic;		//The elements semantic
	INode * renderNode;			//Used for Geometry parameters


public:

	enum{
		kEleSwatch,
		kEleSpin,
		kEleTex,
		kEleLight,
		kEleLightProp,	//this is a light element but it represent actual light properties not the node
		kEleTM,
		kEleGeom,
		kEleEnviron,
		kEleBool,
		kEleFloat,
		kElePoint3,
		kElePoint4,
		kEleInt,
	};

	EffectElements();
	virtual ~EffectElements() {};

	void SetGenericData(D3DXHANDLE handle, int uiID, int DlgInt, TCHAR * UIName, TCHAR * paramName, LPDIRECT3DDEVICE9 dev);

	inline void SetUIName(TSTR &Name){UIName = Name;}
	inline TSTR& GetUIName(){return UIName;}
	inline void SetParameterName(const TSTR &Name) {ParameterName = Name;}
	inline TSTR& GetParameterName(){return ParameterName;}
	inline void SetParamHandle(D3DXHANDLE Handle){hParameter = Handle;}
	inline D3DXHANDLE GetParamHandle(){return hParameter;}
	inline void SetStyle(int style){elementStyle = style;}
	virtual int GetStyle(){return elementStyle;}
	inline bool GetParamValid(){return valid;}
	inline void SetSemantic(MaxSemantics s){semantic = s;}
	inline MaxSemantics GetSemantic(){return semantic;}
	inline void SetRenderNode(INode * node){renderNode = node;}
	inline INode* GetRenderNode(){return renderNode;}

	virtual bool SetValue(LPD3DXEFFECT pEffect, IParamBlock2 * pblock){return false;}
	virtual bool SetValue(LPD3DXEFFECT pEffect, StdMat2 * map){return false;}
	virtual bool SetValue(void * val)=0;
	virtual bool GetValue(void * data)=0;

};

#endif // _EFFECT_ELEMENTS_H
