//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// RST Max Mtl Entry Point
//
// This file hooks the rst max material up to max
// 
//////////////////////////////////////////////////////////////////////////////////////////////////////

#include "rstMaxMtl/mtl.h"
#include "rstMaxMtl/rstMax.h"
#if HACK_GTA4 
#include "rstMaxMtl/utility.h"
#endif // HACK_GTA4
#include "grmodel/shader.h"
#include "system/param.h"
#include "rstplugin.h"

HINSTANCE g_hDllInstance;
int controlsInit = FALSE;
bool libInitialized = FALSE;

using namespace rage;

#if MAX_RELEASE < 16000
static char g_Description[1024];
#else
static const MCHAR g_Description[1024];
#endif

#ifdef OUTSOURCE_TOOLS

#include "lmclient.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "lm_attr.h"
#ifdef PC
#define LICPATH "32127@10.11.31.5"
#define LICPATHREMOTE "32127@194.74.241.228"
#else
#define LICPATH "@localhost:license.dat:."
#endif /* PC */

#define FEATURE "f1"
VENDORCODE code;
LM_HANDLE *lm_job;

static void init(struct flexinit_property_handle **handle)
{
#ifndef NO_ACTIVATION_SUPPORT
	struct flexinit_property_handle *ourHandle;
	int stat;

	if ((stat = lc_flexinit_property_handle_create(&ourHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit_property_handle_create() failed: %d\n", stat);
		exit(1);
	}
	if ((stat = lc_flexinit_property_handle_set(ourHandle,
		(FLEXINIT_PROPERTY_TYPE)FLEXINIT_PROPERTY_USE_TRUSTED_STORAGE,
		(FLEXINIT_VALUE_TYPE)1)) != 0)
	{
		fprintf(stderr, "lc_flexinit_property_handle_set failed: %d\n", stat);
		exit(1);
	}
	if ((stat = lc_flexinit(ourHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit failed: %d\n", stat);
		exit(1);
	}
	*handle = ourHandle;
#endif /* NO_ACTIVATION_SUPPORT */
}

static void cleanup(struct flexinit_property_handle *initHandle)
{
#ifndef NO_ACTIVATION_SUPPORT
	int stat;

	if ((stat = lc_flexinit_cleanup(initHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit_cleanup failed: %d\n", stat);
	}
	if ((stat = lc_flexinit_property_handle_free(initHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit_property_handle_free failed: %d\n", stat);
	}
#endif /* NO_ACTIVATION_SUPPORT */
}

#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////
#if USE_RAGE_ALLOCATORS

void *operator_new(size_t size,size_t align) 
{
	void *result = ::rage::sysMemAllocator::GetCurrent().Allocate(size,align);

	RAGE_TRACKING_TALLY(result,::rage::MEMTYPE_GAME_VIRTUAL);

#if !__FINAL
	if (result == ::rage::sysMemAllocator::sm_BreakOnAddr)
		__debugbreak();
#endif

	return result;
}

void operator_delete(void *ptr) 
{
	if (ptr) 
	{
		RAGE_TRACKING_UNTALLY(ptr);
		::rage::sysMemAllocator::GetCurrent().Free(ptr);
	}
}

void* operator new(size_t size) {
	return operator_new(size,16);
}


void* operator new[](size_t size) {
	return operator_new(size,16);
}

void operator delete(void *ptr) {
	operator_delete(ptr);
}

void operator delete[](void *ptr) {
	operator_delete(ptr);
}

#endif //USE_RAGE_ALLOCATOR

//////////////////////////////////////////////////////////////////////////////////////////////////////
BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	g_hDllInstance = hinstDLL;

#if ( MAX_RELEASE < 9000 ) 
	sprintf(g_Description,"Rst Max Material version %0.2f built with rage release %d on %s at %s",0.01f * PLUGIN_VERSION,RAGE_RELEASE,__DATE__,__TIME__);

	if (!controlsInit) 
	{
		char* name = "rstMaxMtl.dlm";
		char* params[1];

		params[0] = name;

		sysParam::Init(1,params);

		controlsInit = TRUE;
		InitCustomControls(g_hDllInstance);
		InitCommonControls();
	}

	if(rageRstMaxShaderPlugin::GetInstance() == NULL)
	{
		RstMax::InitPaths();
		new rageRstMaxShaderPlugin();
		RstMax::InitShaderLib();
	}
#endif

	return (TRUE);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return _T(g_Description);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) int LibNumberClasses()
{
	return 2;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) {
		case 0: return rage::RstMtlMax::Creator();
		case 1: return rage::RstMax::Creator();
		default: return 0;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ULONG CanAutoDefer()
{
	return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
#if ( MAX_RELEASE >= 9000 ) 

__declspec( dllexport ) int LibInitialize(void)
{
	return 1;
}


__declspec( dllexport ) int LibShutdown(void)
{
	return 1;
}

#endif //( MAX_RELEASE >= 9000 ) 
//////////////////////////////////////////////////////////////////////////////////////////////////////
TCHAR *GetString(int id)
{
	static TCHAR buf[NAME_LENGTH];

	if (g_hDllInstance)
		return LoadString(g_hDllInstance, id, buf, sizeof(buf)) ? buf : NULL;

	return NULL;
}
