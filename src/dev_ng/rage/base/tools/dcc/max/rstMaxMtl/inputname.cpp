#include "rstMaxMtl/inputname.h"
#include "rstMaxMtl/resource.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

////////////////////////////////////////////////////////////////////////////////
bool InputName::DoModal(HWND ParentWnd,const atString& r_Message,atString& r_Return)
{
	m_Message = r_Message;

	s32 Ret = DialogBoxParam(	g_hDllInstance,
								MAKEINTRESOURCE(IDD_DLG_INPUTTEXT),
								GetCOREInterface()->GetMAXHWnd(),
								InputNameDlgProc,
								(LPARAM)this);

	if(Ret != 1)
	{
		return false;
	}

	r_Return = m_Return;
	return true;	
}

////////////////////////////////////////////////////////////////////////////////
void InputName::OnInitDialog(HWND Wnd)
{
	m_Wnd = Wnd;

	SetWindowText(GetDlgItem(Wnd,IDC_STC_INPUTMESSAGE),m_Message);
	SetWindowText(GetDlgItem(Wnd,IDC_EDT_INPUTTEXT),"");
}

////////////////////////////////////////////////////////////////////////////////
void InputName::OnOk()
{
	char CurrentText[512];
	GetWindowText(GetDlgItem(m_Wnd,IDC_EDT_INPUTTEXT),CurrentText,512);
	m_Return = CurrentText;

	if(m_Return == atString(""))
		EndDialog(m_Wnd,0);
	else
		EndDialog(m_Wnd,1);
}

////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK InputName::InputNameDlgProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam) 
{
	InputName* p_InputName = (InputName*)GetWindowLongPtr(DlgWnd,DWLP_USER);

	switch(Msg)
	{
	case WM_INITDIALOG:

		SetWindowLongPtr(DlgWnd,DWLP_USER,LParam);
		p_InputName = (InputName*)LParam;
		p_InputName->OnInitDialog(DlgWnd);
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(WParam))
		{
		case IDOK:
			p_InputName->OnOk();
			return TRUE;
		case IDCANCEL:
			EndDialog(DlgWnd,0);
			return TRUE;
		}
	}

	return FALSE;
}
