#ifndef __RST_MAX_UTILITY_H__
#define __RST_MAX_UTILITY_H__

#include "atl/string.h"
#include "rexGeneric/objectMesh.h"
#include "grmodel/model.h"
#include "mesh/mesh.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace rstMaxUtility {

atString GetTexMapName(Texmap* p_Texmap);
grmModelGeom* CreateRageModelFromMaxModel(INode* p_Node,grmShaderGroup* p_ShaderGroup);
grmShaderGroup* CreateRageShaderGroupFromMaxModel(INode* p_Node);
rexResult ConvertGenericMeshToRageRenderMesh(rexObjectGenericMesh *inputMesh,mshMesh& outputMesh);
grmShaderGroup* CreateMat(const atArray<rexObjectGenericMesh::MaterialInfo>& MatList);
void LogOut(const atString& r_Path);
void SetNormalMapFlip(bool onOff);
bool GetNormalMapFlip();
void LoadGtaConfig(atArray<atArray<atString>>& GtaAttList);
void GetSPSVars(const char *presetFileName, atArray<atString> &varNames);
bool IsPreset(const char* fileName);
bool IsPresetExtension(const char* ext);
atString ToLowerCase( const char * path );
} //namespace rstMaxUtility {

} //namespace rage {

#endif __RST_MAX_UTILITY_H__
