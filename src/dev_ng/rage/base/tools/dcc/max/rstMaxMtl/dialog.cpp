#include "rstMaxMtl/dialog.h"

using namespace rage;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
DialogRes::DialogRes(bool IsChild):
	mp_Template(NULL),
	m_IsChild(IsChild)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
DialogRes::~DialogRes()
{
	delete[] mp_Template;

	s32 i,ItemCount = 0;

	for(i=0;i<ItemCount;i++)
	{
		delete m_ItemList[i];
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void DialogRes::Reset()
{
	delete[] mp_Template;
	mp_Template = NULL;

	s32 i,ItemCount = 0;

	for(i=0;i<ItemCount;i++)
	{
		delete m_ItemList[i];
	}

	m_ItemList.Reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void DialogRes::AddControl(u16 ClassID,u32 Style,const atString& r_Text,s32 PosX,s32 PosY,s32 Width,s32 Height,u16 ID)
{
	ItemInfo* p_AddItemInfo = new ItemInfo;

	if(	DialogRes::CTRL_BUTTON==ClassID || 
		DialogRes::CTRL_COMBOBOX==ClassID)
	{
		Style |= WS_TABSTOP;
	}

	p_AddItemInfo->CreateControl(ClassID,Style,r_Text,PosX,PosY,Width,Height,ID);

	m_ItemList.PushAndGrow(p_AddItemInfo,m_ItemList.GetCount() + 1);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void DialogRes::AddControl(const atString& r_ClassName,u32 Style,const atString& r_Text,s32 PosX,s32 PosY,s32 Width,s32 Height,u16 ID)
{
	ItemInfo* p_AddItemInfo = new ItemInfo;

	if(	strstr(r_ClassName, "CustEdit") ||
		strstr(r_ClassName, "CustButton"))
	{
		Style |= WS_TABSTOP;
	}

	p_AddItemInfo->CreateControl(r_ClassName,Style,r_Text,PosX,PosY,Width,Height,ID);

	m_ItemList.PushAndGrow(p_AddItemInfo,m_ItemList.GetCount() + 1);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
DLGTEMPLATE* DialogRes::GetTemplate()
{
	if(mp_Template)
	{
		return (DLGTEMPLATE*)mp_Template;
	}

	s32 FontNameSize;
	WCHAR* p_FontName = RageStringToUnicode(atString("MS Shell Dlg"),FontNameSize);

	s32 TitleSize = 0;
	WCHAR* p_Title = RageStringToUnicode(m_Title,TitleSize);

	s32 TemplateSize = sizeof(DLGTEMPLATE) + 2 * 2; //dialog info

	TemplateSize += 2 + FontNameSize;
	TemplateSize += TitleSize;

	s32 PadSize = TemplateSize;

	TemplateSize = (TemplateSize + 3) & ~3;
	PadSize = TemplateSize - PadSize;

	s32 i,ItemCount = m_ItemList.GetCount();

	for(i=0;i<ItemCount;i++)
	{
		TemplateSize += m_ItemList[i]->GetSize();
	}

	mp_Template = (u8*)new u16[TemplateSize >> 1];

	u8* p_Data = mp_Template;
	
	DLGTEMPLATE* p_DlgTemplate = (DLGTEMPLATE*)p_Data;

	if(m_IsChild)
		p_DlgTemplate->style = DS_SETFONT|WS_VISIBLE|WS_CHILD;
	else
		p_DlgTemplate->style = DS_SETFONT|WS_VISIBLE|WS_OVERLAPPED|WS_CAPTION|WS_SYSMENU;

	p_DlgTemplate->dwExtendedStyle = 0;
	p_DlgTemplate->cdit = ItemCount;
	p_DlgTemplate->x = 0;
	p_DlgTemplate->y = 0;
	p_DlgTemplate->cx = m_Width;
	p_DlgTemplate->cy = m_Height;

	p_Data += sizeof(DLGTEMPLATE);

	*((u16*)p_Data) = 0; p_Data += 2; // menu
	*((u16*)p_Data) = 0; p_Data += 2; // class

	memcpy(p_Data,p_Title,TitleSize); //title
	p_Data += TitleSize;

	*((u16*)p_Data) = 8; p_Data += 2; // font size

	memcpy(p_Data,p_FontName,FontNameSize); //font
	p_Data += FontNameSize;

	//padding
	memset(p_Data,0,PadSize);
	p_Data += PadSize;

	s32 ItemTemplateSize;

	for(i=0;i<ItemCount;i++)
	{
		ItemTemplateSize = m_ItemList[i]->GetSize();
		memcpy(p_Data,m_ItemList[i]->GetData(),ItemTemplateSize);
		p_Data+=ItemTemplateSize;
	}

	delete[] p_FontName;
	delete[] p_Title;

	return (DLGTEMPLATE*)mp_Template;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
WCHAR* DialogRes::RageStringToUnicode(const atString& r_In,s32& r_Size)
{
	char* p_Text = new char[r_In.GetLength() + 1];
	memcpy(p_Text,r_In,r_In.GetLength());
	p_Text[r_In.GetLength()] = '\0';

	r_Size = MultiByteToWideChar(CP_THREAD_ACP,0,p_Text,-1,NULL,0);

	WCHAR* p_TextOut = new WCHAR[r_Size];

	MultiByteToWideChar(CP_THREAD_ACP,0,p_Text,-1,p_TextOut,r_Size);

	delete[] p_Text;

	r_Size <<= 1;

	return p_TextOut;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
DialogRes::ItemInfo::ItemInfo():
	mp_ItemTemplate(NULL)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
DialogRes::ItemInfo::~ItemInfo()
{
	delete[] mp_ItemTemplate;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void DialogRes::ItemInfo::CreateControl(const atString& r_ClassName,u32 Style,const atString& r_Text,s32 PosX,s32 PosY,s32 Width,s32 Height,u16 ID)
{
	s32 TitleSize;
	WCHAR* p_Wide = DialogRes::RageStringToUnicode(r_Text,TitleSize);

	s32 ClassSize;
	WCHAR* p_ClassWide = DialogRes::RageStringToUnicode(r_ClassName,ClassSize);

	m_Size = sizeof(DLGITEMTEMPLATE);
	m_Size += ClassSize;				// for class
	m_Size += TitleSize;			// for title
	m_Size += 2;					// for creation data
	m_Size = ((m_Size + 3) & ~3);	// padding to DWORD

	mp_ItemTemplate = (u8*)new u16[m_Size >> 1];
	memset(mp_ItemTemplate,0,m_Size);

	u8* p_Data = mp_ItemTemplate;

	DLGITEMTEMPLATE* p_DlgItemTemplate = (DLGITEMTEMPLATE*)p_Data;

	p_DlgItemTemplate->style = Style;
	p_DlgItemTemplate->dwExtendedStyle = 0;
	p_DlgItemTemplate->x = PosX;
	p_DlgItemTemplate->y = PosY;
	p_DlgItemTemplate->cx = Width;
	p_DlgItemTemplate->cy = Height;
	p_DlgItemTemplate->id = ID;

	p_Data += sizeof(DLGITEMTEMPLATE);

	memcpy(p_Data,p_ClassWide,ClassSize); p_Data += ClassSize;	// class
	memcpy(p_Data,p_Wide,TitleSize); p_Data += TitleSize;	// title

	*((s16*)p_Data) = 0; p_Data += 2; // creation data

	delete[] p_Wide;
	delete[] p_ClassWide;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void DialogRes::ItemInfo::CreateControl(u16 ClassID,u32 Style,const atString& r_Text,s32 PosX,s32 PosY,s32 Width,s32 Height,u16 ID)
{
	s32 TitleSize;
	WCHAR* p_Wide = DialogRes::RageStringToUnicode(r_Text,TitleSize);

	m_Size = sizeof(DLGITEMTEMPLATE);
	m_Size += 4;					// for class
	m_Size += TitleSize;			// for title
	m_Size += 2;					// for creation data
	m_Size = ((m_Size + 3) & ~3);	// padding to DWORD

	mp_ItemTemplate = (u8*)new u16[m_Size >> 1];
	memset(mp_ItemTemplate,0,m_Size);

	u8* p_Data = mp_ItemTemplate;

	DLGITEMTEMPLATE* p_DlgItemTemplate = (DLGITEMTEMPLATE*)p_Data;

	p_DlgItemTemplate->style = Style;
	p_DlgItemTemplate->dwExtendedStyle = 0;
	p_DlgItemTemplate->x = PosX;
	p_DlgItemTemplate->y = PosY;
	p_DlgItemTemplate->cx = Width;
	p_DlgItemTemplate->cy = Height;
	p_DlgItemTemplate->id = ID;

	p_Data += sizeof(DLGITEMTEMPLATE);

	*((u16*)p_Data) = 0xFFFF; p_Data += 2;					// class
	*((u16*)p_Data) = ClassID; p_Data += 2;					// class
	memcpy(p_Data,p_Wide,TitleSize); p_Data += TitleSize;	// title

	*((s16*)p_Data) = 0; p_Data += 2; // creation data

	delete[] p_Wide;
}