#ifndef __RST_MAX_DIALOG_H__
#define __RST_MAX_DIALOG_H__

#include "atl/string.h"
#include "atl/array.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// this is a generic dialog resource builder (returns a DLGTEMPLATE)
//////////////////////////////////////////////////////////////////////////////////////////////////////////
class DialogRes
{
public:
	DialogRes(bool IsChild = true);
	~DialogRes();

	//static const s32 STD_BTN_WIDTH;
	//static const s32 STD_BTN_HEIGHT;

	enum
	{
		CTRL_BUTTON = 0x0080,
		CTRL_EDIT = 0x0081,
		CTRL_STATIC = 0x0082,
		CTRL_LISTBOX = 0x0083,
		CTRL_SCROLLBAR = 0x0084,
		CTRL_COMBOBOX = 0x0085,
	};

	void Reset();
	void SetSize(s32 Width,s32 Height) { m_Width = Width; m_Height = Height; }
	void SetTitle(const char* r_Title) { m_Title = atString(r_Title); }

	void AddControl(u16 ClassID,u32 Style,const atString& r_Text,s32 PosX,s32 PosY,s32 Width,s32 Height,u16 ID);
	void AddControl(const atString& r_ClassName,u32 Style,const atString& r_Text,s32 PosX,s32 PosY,s32 Width,s32 Height,u16 ID);

	DLGTEMPLATE* GetTemplate();

protected:

	static WCHAR* RageStringToUnicode(const atString& r_In,s32& r_Size);

	class ItemInfo
	{
	public:
		ItemInfo();
		~ItemInfo();

		void CreateControl(u16 ClassID,u32 Style,const atString& r_Text,s32 PosX,s32 PosY,s32 Width,s32 Height,u16 ID);
		void CreateControl(const atString& r_ClassName,u32 Style,const atString& r_Text,s32 PosX,s32 PosY,s32 Width,s32 Height,u16 ID);

		s32 GetSize() const { return m_Size; }
		const u8* GetData() const { return mp_ItemTemplate; }

	protected:
		u8* mp_ItemTemplate;
		s32 m_Size;
	};

	bool m_IsChild;
	s32 m_Width;
	s32 m_Height;
	u8* mp_Template;
	atArray<ItemInfo*> m_ItemList;
	atString m_Title;
};

} //namespace rage {

#endif __RST_MAX_DIALOG_H__
