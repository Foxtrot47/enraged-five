#pragma warning( disable: 4265 )
#pragma warning( disable: 4263 )

#include "MaterialPresets/MaterialPreset.h"
#include "MaterialPresets/MaterialPresetManager.h"

#include <maxscript/maxscript.h>
#include <maxscript/foundation/3dmath.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/maxwrapper/mxsmaterial.h>
#include <maxscript/foundation/Numbers.h>
#include <maxscript/foundation/Name.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last

using namespace rage;

#include "atl/string.h"
#include "rstMaxMtl/rstMax.h"
#include "rstMaxMtl/dbeditor.h"
#include "rstMaxMtl/utility.h"
#include "rstMaxMtl/mtl.h"
#include "rstMaxMtl/rstplugin.h"

#include "rexMaxUtility/defshader.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////
def_visible_primitive(RstSetNormalMapFlip,"RstSetNormalMapFlip");
def_visible_primitive(RstLibChooser,"RstLibChooser");
def_visible_primitive(RstLibAddGroup,"RstLibAddGroup");
def_visible_primitive(RstGetShaderPath,"RstGetShaderPath");
//def_visible_primitive(RstGetPresetPath,"RstGetPresetPath");
def_visible_primitive(RstGetDefaultShader,"RstGetDefaultShader");
def_visible_primitive(RstGetDefaultAlphaShader,"RstGetDefaultAlphaShader");

def_visible_primitive(RstGetIsAlphaShader,"RstGetIsAlphaShader");
def_visible_primitive(RstGetIsTintShader, "RstGetIsTintShader");
def_visible_primitive(RstGetAlphaTextureName, "RstGetAlphaTextureName");
def_visible_primitive(RstSetAlphaTextureName, "RstSetAlphaTextureName");
def_visible_primitive(RstGetShaderName,"RstGetShaderName");
def_visible_primitive(RstGetVariableCount,"RstGetVariableCount");
def_visible_primitive(RstGetVariableName,"RstGetVariableName");
def_visible_primitive(RstGetVariableType,"RstGetVariableType");
def_visible_primitive(RstGetVariable,"RstGetVariable");
def_visible_primitive(RstSetTextureRefresh,"RstSetTextureRefresh");
def_visible_primitive(RstRefreshMtl,"RstRefreshMtl");

def_visible_primitive(RstSetUseDiffuseAlpha,"RstSetUseDiffuseAlpha");
def_visible_primitive(RstGetUseDiffuseAlpha,"RstGetUseDiffuseAlpha");

def_visible_primitive(RstSetShaderName,"RstSetShaderName");
def_visible_primitive(RstSetVariable,"RstSetVariable");


def_visible_primitive(RstSetTextureScaleValue,"RstSetTextureScaleValue");
def_visible_primitive(RstSetHwRenderMode,"RstSetHwRenderMode");
def_visible_primitive(RstGetHwRenderMode,"RstGetHwRenderMode");

def_visible_primitive(RstGetVariableTextureOutputFormats, "RstGetVariableTextureOutputFormats");

def_visible_primitive(RstSetLightingMode, "RstSetLightingMode");
def_visible_primitive(RstGetLightingMode, "RstGetLightingMode");
def_visible_primitive(RstSetHasAmbLighting, "RstSetHasAmbLighting");
def_visible_primitive(RstGetHasAmbLighting, "RstGetHasAmbLighting");

def_visible_primitive( RstGetIsTwoSided, "RstGetIsTwoSided" );
def_visible_primitive( RstSetIsTwoSided, "RstSetIsTwoSided" );
def_visible_primitive( RstCreateRstMtlFromStdMtl, "RstCreateRstMtlFromStdMtl" );

def_visible_primitive( RstGetRawTextureCount, "RstGetRawTextureCount" );
def_visible_primitive( RstGetRawTextureAlphaCount, "RstGetRawTextureAlphaCount" );
def_visible_primitive( RstGetRawTextureHasAlpha, "RstGetRawTextureHasAlpha" );
def_visible_primitive( RstGetRawTextureValue, "RstGetRawTextureValue" );
def_visible_primitive( RstGetRawTextureAlphaValue, "RstGetRawTextureAlphaValue" );

def_visible_primitive(RstRegisterValueChangedCallback, "RstRegisterValueChangedCallback");
def_visible_primitive(RstRemoveValueChangedCallback, "RstRemoveValueChangedCallback");

def_visible_primitive(RstGetTextureTemplate, "RstGetTextureTemplate");
def_visible_primitive(RstGetTextureTemplateRelative, "RstGetTextureTemplateRelative");
def_visible_primitive(RstGetTextureTemplateOverride, "RstGetTextureTemplateOverride");
def_visible_primitive(RstVerifyShaderVars, "RstVerifyShaderVars");

def_visible_primitive(RstSaveMaterialPreset, "RstSaveMaterialPreset");
def_visible_primitive(RstLockMaterialPresetValue, "RstLockMaterialPresetValue");
def_visible_primitive(RstGetMaterialPresetParent, "RstGetMaterialPresetParent");
def_visible_primitive(RstGetMaterialPresetVariableLocked, "RstGetMaterialPresetVariableLocked");
def_visible_primitive(RstReloadMaterialPresets, "RstReloadMaterialPresets");
def_visible_primitive(RstSetMaterialPresetAssetDirectory, "RstSetMaterialPresetAssetDirectory");
def_visible_primitive(RstGetMaterialPresetPath, "RstGetMaterialPresetPath");

def_visible_primitive(RstInitialiseShaderLibrary, "RstInitialiseShaderLibrary");

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstSetNormalMapFlip_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetNormalMapFlip", 1, count );
	type_check( arg_list[0], Boolean, "RstSetNormalMapFlip <bool>" );
	rstMaxUtility::SetNormalMapFlip(arg_list[0]->to_bool());

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstLibChooser_cf(Value** arg_list, int count)
{
	if(DbEditor::GetInst().Show())
		return &true_value;
	else
		return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstLibAddGroup_cf(Value** arg_list, int count)
{
	check_arg_count( "RstLibAddGroup", 1, count );
	type_check( arg_list[0], String, _T("RstLibAddGroup <string>") );

	const MCHAR* p_GroupPath = arg_list[0]->to_string();

	if(DbEditor::GetInst().AddGroup(p_GroupPath))
		return &true_value;
	else
		return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetShaderPath_cf(Value** arg_list, int count)
{
	char TempPath[MAX_PATH];
	strcpy(TempPath,RstMax::GetShaderPath());	
#if MAX_RELEASE < 16000
	return_protected(new String(TempPath));
#else
	return_protected(new String(MSTR::FromCStr(TempPath)));
#endif
}

#if 0

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetPresetPath_cf(Value** arg_list, int count)
{
	char TempPath[MAX_PATH];
	strcpy(TempPath,RstMax::GetPresetPath());		
	return_protected(new String(TempPath));
}

#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetDefaultShader_cf(Value** arg_list, int count)
{
	char TempPath[MAX_PATH];
	strcpy(TempPath,RstMax::GetDefaultShaderName());
#if MAX_RELEASE < 16000
	return_protected(new String(TempPath));
#else
	return_protected(new String(MSTR::FromCStr(TempPath)));
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetDefaultAlphaShader_cf(Value** arg_list, int count)
{
	char TempPath[MAX_PATH];
	strcpy(TempPath,RstMax::GetDefaultAlphaShaderName());	
#if MAX_RELEASE < 16000
	return_protected(new String(TempPath))
#else
	return_protected(new String(MSTR::FromCStr(TempPath)));
#endif;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetIsAlphaShader_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetIsAlphaShader", 1, count );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
	{
		return &false_value;
	}

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(p_MtlMax->IsAlphaShader())
		return &true_value;

	return &false_value;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////

Value* RstGetIsTintShader_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetIsTintShader", 1, count );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
	{
		return &false_value;
	}

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(p_MtlMax->IsTintShader())
		return &true_value;

	return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetShaderName_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetShaderName", 1, count );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)

	{
		return &false_value;
	}

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	char TempName[MAX_PATH];
	strcpy(TempName,p_MtlMax->GetShaderName());	
#if MAX_RELEASE < 16000
	return_protected(new String(TempName));
#else
	return_protected(new String(MSTR::FromCStr(TempName)));
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstSetTextureRefresh_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetTextureRefresh", 2, count );
	type_check( arg_list[1], Boolean, _T("RstSetTextureRefresh <mtl> <bool>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	bool toggle = arg_list[1]->to_bool();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	p_MtlMax->SetRefreshTextures( toggle );
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstRefreshMtl_cf(Value** arg_list, int count)
{
	check_arg_count( "RstRefreshMtl", 1, count );
	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	p_MtlMax->UpdateParameters( true );
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstSetUseDiffuseAlpha_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetUseDiffuseAlpha", 2, count );
	type_check( arg_list[1], Boolean, _T("RstSetUseDiffuseAlpha <mtl> <bool>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	bool toggle = arg_list[1]->to_bool();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	p_MtlMax->SetUseDiffuseMapAlpha(toggle);
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetUseDiffuseAlpha_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetUseDiffuseAlpha", 1, count );
	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	return p_MtlMax->UsesDiffuseMapAlpha()?&true_value:&false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetVariableCount_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetVariableCount", 1, count);

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
	{
		return &undefined;
	}

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	return_protected(new Integer(p_MtlMax->GetVariableCount()));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetVariableName_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetVariableName", 2, count);
	type_check( arg_list[1], Integer, _T("RstGetVariableName <mtl> <idx>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;

	atString name;
	char Buffer[1024];
	p_MtlMax->GetVariableName(index,name);
	strcpy(Buffer,name);
#if MAX_RELEASE < 16000
	return_protected(new String(Buffer));
#else
	return_protected(new String(MSTR::FromCStr(Buffer)));
#endif

}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetVariableType_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetVariableType", 2, count);
	type_check( arg_list[1], Integer, _T("RstGetVariableType <mtl> <idx>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;

	TypeBase::eVariableType presetType = p_MtlMax->GetPresetVariableType(index); 
	if ( presetType != TypeBase::kUndefined )
	{
		switch(presetType)
		{
		case TypeBase::kFloat:
			return_protected(new String(_T("float")));
			break;
		case TypeBase::kVector2:
			return_protected(new String(_T("vector2")));
			break;
		case TypeBase::kVector3:
			return_protected(new String(_T("vector3")));
			break;
		case TypeBase::kTextureMap:
			return_protected(new String(_T("texmap")));
			break;
		case TypeBase::kVector4: 
			return_protected(new String(_T("vector4")));
			break;
		}
	}
	else
	{
		grcEffect::VarType type = p_MtlMax->GetVariableType(index);

		switch(type)
		{
		case grcEffect::VT_FLOAT:
			return_protected(new String(_T("float")));
			break;
		case grcEffect::VT_VECTOR2:
			return_protected(new String(_T("vector2")));
			break;
		case grcEffect::VT_VECTOR3:
			return_protected(new String(_T("vector3")));
			break;
		case grcEffect::VT_TEXTURE:
			return_protected(new String(_T("texmap")));
			break;
		case grcEffect::VT_VECTOR4: 
			return_protected(new String(_T("vector4")));
			break;
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		case grcEffect::VT_STRING:
		case grcEffect::VT_NONE:
			//		case grcEffect::VT_COLOR32:
		case grcEffect::VT_BOOL_DONTUSE:
		case grcEffect::VT_INT_DONTUSE:
		case grcEffect::VT_COUNT:
			//		case grcEffect::VT_SIZE:
			break;
		}
	}

	return &undefined;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetVariable_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetVariable", 2, count);
	type_check( arg_list[1], Integer, _T("RstGetVariable <mtl> <idx>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;

	TypeBase::eVariableType presetType = p_MtlMax->GetPresetVariableType(index); 
	if ( presetType != TypeBase::kUndefined )
	{
		switch(presetType)
		{
		case TypeBase::kFloat:
			{
				f32 val;
				p_MtlMax->GetVariable(index,val);
				return_protected(Float::intern(val));
			}
		case TypeBase::kVector2:
			{
				Vector2 val;
				Point2 retval;
				p_MtlMax->GetVariable(index, val);

				retval.x = val.x;
				retval.y = val.y;

				return_protected(new Point2Value(retval));
			}
		case TypeBase::kVector3:
			{
				Vector3 val;
				Point3 retval;
				p_MtlMax->GetVariable(index,val);

				retval.x = val.x;
				retval.y = val.y;
				retval.z = val.z;

				return_protected(new Point3Value(retval));
			}
		case TypeBase::kTextureMap:
			{
				atString val;
				char Buffer[1024];
				p_MtlMax->GetVariable(index,val);
				strcpy(Buffer,val);
#if MAX_RELEASE < 16000
				return_protected(new String(Buffer));
#else
				return_protected(new String(MSTR::FromCStr(Buffer)));
#endif

			}
		case TypeBase::kVector4: 
			{
				Vector4 val;
				Point4 retval;
				p_MtlMax->GetVariable(index,val);

				retval.x = val.x;
				retval.y = val.y;
				retval.z = val.z;
				retval.w = val.w;

				return_protected(new Point4Value(retval));
			}
		case TypeBase::kInt:
			break;
		}
	}
	else
	{
		grcEffect::VarType type = p_MtlMax->GetVariableType(index);

		switch(type)
		{
		case grcEffect::VT_FLOAT:
			{
				f32 val;
				p_MtlMax->GetVariable(index,val);
				return_protected(Float::intern(val));
			}
		case grcEffect::VT_VECTOR2:
			{
				Vector2 val;
				Point2 retval;
				p_MtlMax->GetVariable(index, val);

				retval.x = val.x;
				retval.y = val.y;

				return_protected(new Point2Value(retval));
			}
		case grcEffect::VT_VECTOR3:
			{
				Vector3 val;
				Point3 retval;
				p_MtlMax->GetVariable(index,val);

				retval.x = val.x;
				retval.y = val.y;
				retval.z = val.z;

				return_protected(new Point3Value(retval));
			}
		case grcEffect::VT_TEXTURE:
			{
				atString val;
				char Buffer[1024];
				p_MtlMax->GetVariable(index,val);
				strcpy(Buffer,val);
#if MAX_RELEASE < 16000
				return_protected(new String(Buffer));
#else
				return_protected(new String(MSTR::FromCStr(Buffer)));
#endif
			}
		case grcEffect::VT_VECTOR4: 
			{
				Vector4 val;
				Point4 retval;
				p_MtlMax->GetVariable(index,val);

				retval.x = val.x;
				retval.y = val.y;
				retval.z = val.z;
				retval.w = val.w;

				return_protected(new Point4Value(retval));
			}
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		case grcEffect::VT_STRING:
		case grcEffect::VT_NONE:
			//		case grcEffect::VT_COLOR32:
		case grcEffect::VT_BOOL_DONTUSE:
		case grcEffect::VT_INT_DONTUSE:
		case grcEffect::VT_COUNT:
			//		case grcEffect::VT_SIZE:
			break;
		}
	}

	return &undefined;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetAlphaTextureName_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetAlphaTextureName", 2, count);
	type_check( arg_list[1], Integer, _T("RstGetAlphaTextureName <mtl> <idx>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetAlphaTextureCount()))
		return &undefined;

	TypeBase::eVariableType presetType = p_MtlMax->GetPresetVariableType(index); 
	if ( presetType != TypeBase::kUndefined )
	{
		switch(presetType)
		{
		case TypeBase::kTextureMap:
			{
				atString val;
				MCHAR Buffer[1024];
				p_MtlMax->GetAlphaTextureName(index,val);
#if MAX_RELEASE < 16000
				strcpy(Buffer,val);
#else
				_tcscpy(Buffer,MSTR::FromUTF8(val));
#endif
				return_protected(new String(Buffer));
			}
		default:
			break;
		}
	}
	else
	{
		grcEffect::VarType type = p_MtlMax->GetVariableType(index);

		switch(type)
		{
		case grcEffect::VT_TEXTURE:
			{
				atString val;
				MCHAR Buffer[1024];
				p_MtlMax->GetAlphaTextureName(index,val);
#if MAX_RELEASE < 16000
				strcpy(Buffer,val);
#else
				_tcscpy(Buffer,MSTR::FromUTF8(val));
#endif
				return_protected(new String(Buffer));
			}
		default:
			break;
		}
	}
	

	return &undefined;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstSetAlphaTextureName_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetAlphaTextureName", 3, count);
	type_check( arg_list[1], Integer, _T("RstSetAlphaTextureName <mtl> <idx> <string>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetAlphaTextureCount()))
		return &false_value;

#if MAX_RELEASE < 16000
	p_MtlMax->SetAlphaTextureName(index, atString(arg_list[2]->to_string()));
#else
	p_MtlMax->SetAlphaTextureName(index, atString(MSTR(arg_list[2]->to_string()).ToCStr()));
#endif

	return &true_value;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstSetShaderName_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetShaderName", 2, count);
	type_check( arg_list[1], String, _T("RstSetShaderName <mtl> <string>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;	
#if MAX_RELEASE < 16000
	p_MtlMax->SetShaderName(atString(arg_list[1]->to_string()));
#else
	p_MtlMax->SetShaderName(atString(MSTR(arg_list[1]->to_string()).ToCStr()));
#endif

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstSetVariable_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetVariable", 3, count);
	type_check( arg_list[1], Integer, "RstSetVariable <mtl> <idx> <val>" );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;

	TypeBase::eVariableType presetType = p_MtlMax->GetPresetVariableType(index); 
	if ( presetType != TypeBase::kUndefined )
	{
		switch(presetType)
		{
		case TypeBase::kFloat:
			{
				f32 val = arg_list[2]->to_float();
				p_MtlMax->SetVariable(index,val);
				return &true_value;
			}
		case TypeBase::kVector2:
			{
				Vector2 val;
				Point2 inval = arg_list[2]->to_point2();

				val.x = inval.x;
				val.y = inval.y;

				p_MtlMax->SetVariable(index, val);

				return &true_value;
			}
		case TypeBase::kVector3:
			{
				Vector3 val;
				Point3 inval = arg_list[2]->to_point3();

				val.x = inval.x;
				val.y = inval.y;
				val.z = inval.z;

				p_MtlMax->SetVariable(index,val);

				return &true_value;
			}
		case TypeBase::kTextureMap:
			{
				
#if MAX_RELEASE < 16000
				MCHAR* p_inval = arg_list[2]->to_string();
				atString val(p_inval);
#else
				const MCHAR* p_inval = strValue->to_string();
				atString val(MSTR(p_inval).ToCStr());
#endif
				
				p_MtlMax->SetVariable(index,val);
				return &true_value;
			}
		case TypeBase::kVector4:
			{
				Vector4 val;
				Point4 inval = arg_list[2]->to_point4();

				val.x = inval.x;
				val.y = inval.y;
				val.z = inval.z;
				val.w = inval.w;

				p_MtlMax->SetVariable(index,val);

				return &true_value;
			}
		case TypeBase::kInt:
			break;
		}
	}
	else
	{
		grcEffect::VarType type = p_MtlMax->GetVariableType(index);

		switch(type)
		{
		case grcEffect::VT_FLOAT:
			{
				f32 val = arg_list[2]->to_float();
				p_MtlMax->SetVariable(index,val);
				return &true_value;
			}
		case grcEffect::VT_VECTOR2:
			{
				Vector2 val;
				Point2 inval = arg_list[2]->to_point2();

				val.x = inval.x;
				val.y = inval.y;

				p_MtlMax->SetVariable(index, val);

				return &true_value;
			}
		case grcEffect::VT_VECTOR3:
			{
				Vector3 val;
				Point3 inval = arg_list[2]->to_point3();

				val.x = inval.x;
				val.y = inval.y;
				val.z = inval.z;

				p_MtlMax->SetVariable(index,val);

				return &true_value;
			}
		case grcEffect::VT_TEXTURE:
			{
#if MAX_RELEASE < 16000
				MCHAR* p_inval = arg_list[2]->to_string();
				atString val(p_inval);
#else
				const MCHAR* p_inval = strValue->to_string();
				atString val(MSTR(p_inval).ToCStr());
#endif
				p_MtlMax->SetVariable(index,val);
				return &true_value;
			}
		case grcEffect::VT_VECTOR4:
			{
				Vector4 val;
				Point4 inval = arg_list[2]->to_point4();

				val.x = inval.x;
				val.y = inval.y;
				val.z = inval.z;
				val.w = inval.w;

				p_MtlMax->SetVariable(index,val);

				return &true_value;
			}
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		case grcEffect::VT_STRING:
		case grcEffect::VT_NONE:
		case grcEffect::VT_BOOL_DONTUSE:
		case grcEffect::VT_INT_DONTUSE:
		case grcEffect::VT_COUNT:
			break;
		}
	}
	

	return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstSetTextureScaleValue_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetTextureScaleValue", 2, count);
	type_check( arg_list[1], Float, _T("RstSetTextureScaleValue <mtl> <float>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	float scale = arg_list[1]->to_float();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;
#if RSTMTL_HW_SHADER
	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	if(p_MtlMax->GetTextureScaleValue() != scale )
	{
		p_MtlMax->SetTextureScaleValue( scale );
		p_MtlMax->UpdateParameters( true );
	}
	
#endif //RSTMTL_HW_SHADER

	return &true_value;
}

Value* RstSetHwRenderMode_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetHwRenderMode", 2, count);
	type_check( arg_list[1], Boolean, _T("RstSetHwRenderMode <mtl> <bool>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	bool hw = arg_list[1]->to_bool();

	if( hw )
	{
		p_MtlBase->SetMtlFlag( MTL_HW_MAT_ENABLED );
	}
	else
	{
		p_MtlBase->ClearMtlFlag( MTL_HW_MAT_ENABLED );
	}

	return &true_value;
}

Value* RstGetHwRenderMode_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetHwRenderMode", 1, count);
	type_check( arg_list[0], MAXMaterial, _T("RstSetHwRenderMode <mtl>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->TestMtlFlag( MTL_HW_MAT_ENABLED ))
	{
		return &true_value;
	}

	return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetVariableTextureOutputFormats_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetVariableTextureOutputFormats", 2, count);
	type_check( arg_list[1], Integer, _T("RstGetVariableTextureOutputFormats <mtl> <idx>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;

	atString textureOutputFormats;
	char Buffer[1024];
	p_MtlMax->GetVariableInfoTextureOutputFormats(index,textureOutputFormats);
	strcpy(Buffer,textureOutputFormats);

#if MAX_RELEASE < 16000
	return_protected(new String(Buffer));
#else
	return_protected(new String(MSTR::FromCStr(Buffer)));
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstSetLightingMode_cf( Value** arg_list, int count )
{
	check_arg_count( "RstSetLightingMode", 2, count);
	type_check( arg_list[1], Boolean, _T("RstSetLightingMode <mtl> <bool>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	bool toggle = arg_list[1]->to_bool();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;
#if RSTMTL_HW_SHADER
	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	p_MtlMax->SetLightingMode( toggle );
	p_MtlMax->UpdateParameters( true );
#endif // RSTMTL_HW_SHADER
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetLightingMode_cf( Value** arg_list, int count )
{
	check_arg_count( "RstGetLightingMode", 1, count);
	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	bool toggle = p_MtlMax->GetLightingMode();
	
	if( toggle )
		return &true_value;
	else
		return &false_value;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstSetHasAmbLighting_cf( Value** arg_list, int count )
{
	check_arg_count( "RstSetHasAmbLighting", 2, count);
	type_check( arg_list[1], Boolean, _T("RstSetHasAmbLighting <mtl> <bool>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	bool toggle = arg_list[1]->to_bool();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;
#if RSTMTL_HW_SHADER
	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	p_MtlMax->SetHasAmbLighting( toggle );
#endif // RSTMTL_HW_SHADER
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetHasAmbLighting_cf( Value** arg_list, int count )
{
	check_arg_count( "RstGetHasAmbLighting", 1, count);
	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	bool toggle = p_MtlMax->GetHasAmbLighting();

	if( toggle )
		return &true_value;
	else
		return &false_value;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////

Value* RstGetIsTwoSided_cf( Value** arg_list, int count )
{
	check_arg_count( "RstGetIsTwoSided", 1, count );

	MtlBase* pMtlBase = arg_list[0]->to_mtlbase();
	if ( RSTMATERIAL_CLASS_ID != pMtlBase->ClassID() )
		throw RuntimeError( _T("Invalid Rage Material.") );

	RstMtlMax* pMtlMax = static_cast<RstMtlMax*>( pMtlBase );
	bool twoSided = pMtlMax->IsTwoSided();

	if ( twoSided )
		return ( &true_value );
	else
		return ( &false_value );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

Value* RstSetIsTwoSided_cf( Value** arg_list, int count )
{
	check_arg_count( "RstSetIsTwoSided", 2, count );
	type_check( arg_list[1], Boolean, _T("RstGetRawTextureCount <mtl> <bool>") );

	MtlBase* pMtlBase = arg_list[0]->to_mtlbase();
	bool toggle = arg_list[1]->to_bool();
	if ( RSTMATERIAL_CLASS_ID != pMtlBase->ClassID() )
		throw RuntimeError( _T("Invalid Rage Material.") );

	RstMtlMax* pMtlMax = static_cast<RstMtlMax*>( pMtlBase );
	pMtlMax->SetIsTwoSided(toggle);
	pMtlMax->UpdateParameters( true );

	return ( &true_value );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

Value* RstCreateRstMtlFromStdMtl_cf( Value** arg_list, int count )
{
	check_arg_count( "RstCreateRstMtlFromStdMtl", 1, count );

	Mtl* p_Mtl = arg_list[0]->to_mtl();
	if(p_Mtl->IsSubClassOf(Class_ID(DMTL_CLASS_ID,0)))
	{
		StdMat* p_StdMtl = (StdMat*)p_Mtl;
		RstMtlMax* rageMtl = new RstMtlMax();

		DefShader::GetInst().GetMaterial(p_StdMtl, *rageMtl, false);
		return MAXMaterial::intern( rageMtl );
	}
	return &undefined;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

Value*
RstGetRawTextureCount_cf( Value** arg_list, int count )
{
	check_arg_count( "RstGetRawTextureCount", 1, count );
	//type_check( arg_list[0], Material, "RstGetRawTextureCount <mtl>" );

	MtlBase* pMtlBase = arg_list[0]->to_mtlbase();
	if( RSTMATERIAL_CLASS_ID != pMtlBase->ClassID() )
		return ( &undefined );

	RstMtlMax* pMtlMax = (RstMtlMax*)pMtlBase;
	return ( new Integer( pMtlMax->FpGetRawTextureCount() ) );
}

Value*
RstGetRawTextureAlphaCount_cf( Value** arg_list, int count )
{
	check_arg_count( "RstGetRawTextureAlphaCount", 1, count );
	//type_check( arg_list[0], Material, "RstGetRawTextureAlphaCount <mtl>" );

	MtlBase* pMtlBase = arg_list[0]->to_mtlbase();
	if( RSTMATERIAL_CLASS_ID != pMtlBase->ClassID() )
		return ( &undefined );

	RstMtlMax* pMtlMax = (RstMtlMax*)pMtlBase;
	return ( new Integer( pMtlMax->FpGetRawTextureAlphaCount() ) );
}

Value*
RstGetRawTextureHasAlpha_cf( Value** arg_list, int count )
{
	check_arg_count( "RstGetRawTextureHasAlpha", 2, count );
	//type_check( arg_list[0], Material, "RstGetRawTextureHasAlpha <mtl> <idx>" );
	type_check( arg_list[1], Integer, _T("RstGetRawTextureHasAlpha <mtl> <idx>") );

	MtlBase* pMtlBase = arg_list[0]->to_mtlbase();
	if( RSTMATERIAL_CLASS_ID != pMtlBase->ClassID() )
		return ( &undefined );

	RstMtlMax* pMtlMax = (RstMtlMax*)pMtlBase;
	int idx = arg_list[1]->to_int();
	if ( !(idx > 0 && idx <= pMtlMax->FpGetRawTextureAlphaCount()) )
		return ( &false_value );

	return ( pMtlMax->FpGetRawTextureHasAlpha( idx-1 ) ? &true_value : &false_value );
}

Value*
RstGetRawTextureValue_cf( Value** arg_list, int count )
{
	check_arg_count( "RstGetRawTextureValue", 2, count );
	//type_check( arg_list[0], Material, "RstGetRawTextureValue <mtl> <idx>" );
	type_check( arg_list[1], Integer, _T("RstGetRawTextureValue <mtl> <idx>") );

	MtlBase* pMtlBase = arg_list[0]->to_mtlbase();
	if( RSTMATERIAL_CLASS_ID != pMtlBase->ClassID() )
		return ( &undefined );

	RstMtlMax* pMtlMax = (RstMtlMax*)pMtlBase;
	int idx = arg_list[1]->to_int();
	if ( !(idx > 0 && idx <= pMtlMax->FpGetRawTextureCount()) )
		return ( &undefined );

	return_protected ( new String( pMtlMax->FpGetRawTextureValue( idx-1 ) ) );
}

Value*
RstGetRawTextureAlphaValue_cf( Value** arg_list, int count )
{
	check_arg_count( "RstGetRawTextureAlphaValue", 2, count );
	//type_check( arg_list[0], Material, "RstGetRawTextureAlphaValue <mtl> <idx>" );
	type_check( arg_list[1], Integer, _T("RstGetRawTextureAlphaValue <mtl> <idx>") );

	MtlBase* pMtlBase = arg_list[0]->to_mtlbase();
	if( RSTMATERIAL_CLASS_ID != pMtlBase->ClassID() )
		return ( &undefined );

	RstMtlMax* pMtlMax = (RstMtlMax*)pMtlBase;
	int idx = arg_list[1]->to_int();
	if ( !pMtlMax->FpGetRawTextureHasAlpha( idx ) )
		return ( &undefined );

	if ( !(idx > 0 && idx <= pMtlMax->FpGetRawTextureAlphaCount()) )
		return ( &undefined );

	return_protected ( new String( pMtlMax->FpGetRawTextureAlphaValue( idx-1 ) ) );
}

Value*
RstRegisterValueChangedCallback_cf( Value** arg_list, int count )
{
	check_arg_count( "RstRegisterValueChangedCallback", 1, count );
	type_check( arg_list[0], String, _T("RstRegisterValueChangedCallback <string>") );

	const MCHAR* pScript = arg_list[0]->to_string();

	if ( pScript == NULL )
	{
		return new String(_T("Script was NULL; unable to set a callback."));
	}

#if MAX_RELEASE < 16000
	std::string scriptString(pScript);
#else
	std::string scriptString(MSTR(pScript).ToCStr());
#endif

	FPValue returnValue;
	BOOL success = FALSE;
	if(GetCOREInterface()->GetMAXHWnd())
	{
		try
		{
#if MAX_RELEASE < 16000
			success = ExecuteMAXScriptScript((char*)scriptString.c_str(),0, &returnValue);
#else
			success = ExecuteMAXScriptScript(MSTR::FromCStr(scriptString.c_str()),0, &returnValue);
#endif
		}
		catch (...)
		{
			MessageBox(GetCOREInterface()->GetMAXHWnd(), _T("Unsafe Max SDK function crash caught."), _T("Phew!"), NULL);
		}
	}
	if(success)
	{
		RstMax::AddValueChangedCallback(scriptString.c_str());

		switch (returnValue.type)
		{
		case TYPE_STRING:
			return new String(returnValue.s);
			break;
		case TYPE_BOOL:
			if ( returnValue.b )
				return &true_value;
			else
				return &false_value;
			break;
		}
	}
	return new String(_T("Max script function execution failed:"));
}


Value*
RstRemoveValueChangedCallback_cf( Value** arg_list, int count )
{
	check_arg_count( "RstRemoveValueChangedCallback", 1, count );
	type_check( arg_list[0], String, _T("RstRemoveValueChangedCallback <string>") );

	const MCHAR* pScript = arg_list[0]->to_string();

	if ( pScript == NULL )
	{
		return new String(_T("Script was NULL; unable to set a callback."));
	}
#if MAX_RELEASE < 16000
	RstMax::RemoveValueChangedCallback(pScript);
#else
	RstMax::RemoveValueChangedCallback(MSTR(pScript).ToCStr());
#endif

	return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetTextureTemplate_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetTextureTemplate", 2, count );
	type_check( arg_list[1], Integer, _T("RstGetTextureTemplate <mtl> <idx>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;
	
	atString texTemplate = p_MtlMax->GetTextureTemplate(index);
#if MAX_RELEASE < 16000
	return new String(texTemplate.c_str());
#else
	return new String(MSTR::FromCStr(texTemplate.c_str()));
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetTextureTemplateRelative_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetTextureTemplateRelative", 2, count );
	type_check( arg_list[1], Integer, _T("RstGetTextureTemplateRelative <mtl> <idx>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;

	atString texTemplate = p_MtlMax->GetTextureTemplateRelative(index);
#if MAX_RELEASE < 16000
	return new String(texTemplate.c_str());
#else
	return new String(MSTR::FromCStr(texTemplate.c_str()));
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RstGetTextureTemplateOverride_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetTextureTemplateOverride", 2, count );
	type_check( arg_list[1], Integer, _T("RstGetTextureTemplateOverride <mtl> <idx>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	s32 index = arg_list[1]->to_int() - 1;

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;

	if(	(index < 0) ||
		(index >= p_MtlMax->GetVariableCount()))
		return &undefined;

	bool allowOverride = p_MtlMax->GetTextureTemplateOverride(index);
	if(allowOverride)
		return &true_value;
	else
		return &false_value;
}

Value* RstVerifyShaderVars_cf(Value** arg_list, int count)
{
	check_arg_count( "RstVerifyShaderVars", 1, count );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &false_value;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	if(p_MtlMax)
	{
		//p_MtlMax->VerifyData();
		rageRstShaderDbEntry* p_Entry = RstMax::GetDbEntry(p_MtlMax->GetShaderName());

		if(!p_Entry)
		{
			return &false_value;
		}
		p_MtlMax->GetShaderInstanceData()->MapInstanceData( p_MtlMax, p_Entry );
		return &true_value;
	}
	else
	{
		return &false_value;
	}
}

//////////////////////////////////////////////////////////////////////////
Value* RstSaveMaterialPreset_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSaveMaterialPreset", 2, count );
	type_check( arg_list[1], String, _T("RstSaveMaterialPreset <mtl> <filename>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &false_value;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	if(p_MtlMax)
	{
		bool isPreset = MaterialPresetManager::GetCurrent().IsMaterialPreset(p_MtlMax->GetShaderName());
		
		if (isPreset)
		{
			const MaterialPreset& fullPreset = *(MaterialPresetManager::GetCurrent().LoadPresetFromLibrary(p_MtlMax->GetShaderName()));
#if MAX_RELEASE < 16000
			bool saveResult = MaterialPresetManager::GetCurrent().SavePreset(atString(arg_list[1]->to_string()), fullPreset);
#else
			bool saveResult = MaterialPresetManager::GetCurrent().SavePreset(atString(MSTR(arg_list[1]->to_string()).ToCStr()), fullPreset);
#endif

			if (saveResult)
				return &true_value;
			else
				return &false_value;
		}
	}

	return &false_value;
}

//////////////////////////////////////////////////////////////////////////
Value* RstLockMaterialPresetValue_cf(Value** arg_list, int count)
{
	check_arg_count( "RstLockMaterialPresetValue", 3, count );

	type_check( arg_list[1], Integer, _T("RstLockMaterialPresetValue <mtl> <idx> <locked>") );
	type_check( arg_list[2], Boolean, _T("RstLockMaterialPresetValue <mtl> <idx> <locked>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &false_value;

	int index = arg_list[1]->to_int();
	bool locked = arg_list[2]->to_bool();

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	if(p_MtlMax)
	{
		bool isPreset = MaterialPresetManager::GetCurrent().IsMaterialPreset(p_MtlMax->GetShaderName());

		if (isPreset)
		{
			MaterialPreset* fullPreset = MaterialPresetManager::GetCurrent().LoadPresetFromLibrary(p_MtlMax->GetShaderName());
			if (fullPreset)
			{
				if (index < 0 || index >= fullPreset->GetNumVariables())
					return &false_value;

				TypeBase* var = const_cast<TypeBase*>(fullPreset->GetVariable(index));

				if (var)
				{
					var->SetLocked(locked);

					// Refresh the UI so it picks up the locked values.
					p_MtlMax->UpdateParameters(false);
					p_MtlMax->SetupUI();

					return &true_value;
				}
			}
		}
	}

	return &false_value;
}

//////////////////////////////////////////////////////////////////////////
Value* RstGetMaterialPresetParent_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetMaterialPresetParent", 1, count );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	if (p_MtlMax)
	{
		bool isPreset = MaterialPresetManager::GetCurrent().IsMaterialPreset(p_MtlMax->GetShaderName());
		if (isPreset)
		{
			MaterialPreset* fullPreset = MaterialPresetManager::GetCurrent().LoadPresetFromLibrary(p_MtlMax->GetShaderName());
			if (fullPreset)
#if MAX_RELEASE < 16000
				return new String(fullPreset->GetParentName());
#else
				return new String(MSTR::FromCStr(fullPreset->GetParentName()));
#endif
		}
	}

	return &undefined;
}

//////////////////////////////////////////////////////////////////////////
Value* RstGetMaterialPresetVariableLocked_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetMaterialPresetVariableLocked", 2, count );
	
	type_check( arg_list[1], Integer, _T("RstGetMaterialPresetVariableLocked <mtl> <idx>") );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &false_value;

	int index = arg_list[1]->to_int();

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	if (p_MtlMax)
	{
		bool isPreset = MaterialPresetManager::GetCurrent().IsMaterialPreset(p_MtlMax->GetShaderName());

		if (isPreset)
		{
			MaterialPreset* fullPreset = MaterialPresetManager::GetCurrent().LoadPresetFromLibrary(p_MtlMax->GetShaderName());

			if (index < 0 || index >= fullPreset->GetNumVariables())
				return &false_value;

			TypeBase* var = const_cast<TypeBase*>(fullPreset->GetVariable(index));

			if (var)
			{
				if (var->IsLocked())
					return &true_value;
			}
		}
	}

	return &false_value;
}

//////////////////////////////////////////////////////////////////////////
Value* RstReloadMaterialPresets_cf(Value** arg_list, int count)
{
	MaterialPresetManager::GetCurrent().ReloadPresetLibrary();
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////
Value* RstSetMaterialPresetAssetDirectory_cf(Value** arg_list, int count)
{
	check_arg_count( "RstSetMaterialPresetAssetDirectory", 1, count );

	type_check( arg_list[0], String, _T("RstSetMaterialPresetAssetDirectory <path>") );
#if MAX_RELEASE < 16000
	const char* pPresetDirectory = arg_list[0]->to_string();
#else
	const char* pPresetDirectory = MSTR(arg_list[0]->to_string()).ToCStr();
#endif

	if (pPresetDirectory)
		MaterialPresetManager::GetCurrent().SetAssetPath(atString(pPresetDirectory));
	else
		return &false_value;

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////
Value* RstGetMaterialPresetPath_cf(Value** arg_list, int count)
{
	check_arg_count( "RstGetMaterialPresetPath", 1, count );

	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(p_MtlBase->ClassID() != RSTMATERIAL_CLASS_ID)
		return &undefined;

	RstMtlMax* p_MtlMax = (RstMtlMax*)p_MtlBase;
	if (p_MtlMax)
	{
		bool isPreset = MaterialPresetManager::GetCurrent().IsMaterialPreset(p_MtlMax->GetShaderName());

		if (isPreset)
		{
			const char* metadataPath = MaterialPresetManager::GetCurrent().GetMetadataPath();
			
			atString fullPath = atString(metadataPath);
			fullPath += "/";
			fullPath += p_MtlMax->GetShaderName();
#if MAX_RELEASE < 16000
			return new String(fullPath.c_str());
#else
			return new String(MSTR::FromCStr(fullPath.c_str()));
#endif
		}
	}

	return &undefined;
}

//////////////////////////////////////////////////////////////////////////
Value* RstInitialiseShaderLibrary_cf(Value** arg_list, int count)
{
	check_arg_count( "RstInitialiseShaderLibrary", 4, count );

	type_check( arg_list[0], String, _T("RstInitialiseShaderLibrary <shaderPath> <presetPath> <defaultShader> <defaultAlphaShader>") );
	type_check( arg_list[1], String, _T("RstInitialiseShaderLibrary <shaderPath> <presetPath> <defaultShader> <defaultAlphaShader>") );
	type_check( arg_list[2], String, _T("RstInitialiseShaderLibrary <shaderPath> <presetPath> <defaultShader> <defaultAlphaShader>") );
	type_check( arg_list[3], String, _T("RstInitialiseShaderLibrary <shaderPath> <presetPath> <defaultShader> <defaultAlphaShader>") );

	const MCHAR* pShaderPath = arg_list[0]->to_string();
	const MCHAR* pPresetPath = arg_list[1]->to_string();
	const MCHAR* pDefaultShader = arg_list[2]->to_string();
	const MCHAR* pDefaultAlphaShader = arg_list[3]->to_string();

	char* name = "rstMaxMtl.dlm";
	char* params[1];

	params[0] = name;

	sysParam::Init(1,params);

	if(rageRstMaxShaderPlugin::GetInstance() == NULL)
	{
		RstMax::InitPaths(pShaderPath, pPresetPath, pDefaultShader, pDefaultAlphaShader);
		rstMaxUtility::LoadGtaConfig(RstMtlMax::m_GtaRuleList);
		new rageRstMaxShaderPlugin();
		try
		{
			RstMax::InitShaderLib();
		}
		catch(...)
		{
			char msgBuffer[2048];
			sprintf_s(msgBuffer, 2048, "Failed to initialise shader library.  PC shaders may be corrupt, contact tools.");
			MessageBox(GetCOREInterface()->GetMAXHWnd(), 
#if MAX_RELEASE < 16000
				msgBuffer,
#else
				MSTR::FromCStr(msgBuffer),
#endif
				_T("RstMaxMtl Warning"),
				MB_ICONERROR | MB_OK);
		}
	}


	return &true_value;
}

BOOL RstMax::ExecuteMaxScript(const char* maxScript)
{
	FPValue returnValue;
#if MAX_RELEASE < 16000
	BOOL success = ExecuteMAXScriptScript(const_cast<char*>(maxScript),0, &returnValue);
#else
	BOOL success = ExecuteMAXScriptScript(MSTR::FromCStr(maxScript),0, &returnValue);
#endif
	return success;
}
