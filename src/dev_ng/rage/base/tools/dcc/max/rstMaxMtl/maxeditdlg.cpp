#include "maxeditdlg.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

////////////////////////////////////////////////////////////////////////////////
s32 MaxEditDialog::g_CtrlBorder(10);
s32 MaxEditDialog::g_StaticHeight(10);
s32 MaxEditDialog::g_CtrlGapX(5);
s32 MaxEditDialog::g_CtrlGapY(5);
s32 MaxEditDialog::g_ButtonHeight(14);
s32 MaxEditDialog::g_ButtonWidth(50);
s32 MaxEditDialog::g_SpinnerSize(15);
s32 MaxEditDialog::g_SpinButtSize(8);

/////////////////////////////////////////////////////////////////////////////////////
MaxEditDialog::MaxEditDialog():
	  DialogRes(false)
{
}

/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::Reset()
{
	m_ID = 0;
	m_BtnOk = -1;
	m_BtnCancel = -1;
}

/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::AddStatic(const atString& r_Label,const atString& r_Static,const WidgetRect& r_Dim)
{
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Label,r_Dim.X,r_Dim.Y,(r_Dim.Width >> 1) - g_CtrlGapX,r_Dim.Height,m_ID++);
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_LEFT,r_Static,r_Dim.X + (r_Dim.Width >> 1),r_Dim.Y,r_Dim.Width >> 1,r_Dim.Height,m_ID++);
}

/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::AddTitles(const WidgetRect& r_Dim)
{
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_LEFT,atString("Locked |"),r_Dim.X,r_Dim.Y,30,r_Dim.Height,m_ID++);
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,atString("Variable |"),r_Dim.X+30,r_Dim.Y,(r_Dim.Width >> 1) - g_CtrlGapX-30,r_Dim.Height,m_ID++);
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_LEFT,atString("Value"),r_Dim.X + (r_Dim.Width >> 1),r_Dim.Y,r_Dim.Width >> 1,r_Dim.Height,m_ID++);
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_ETCHEDHORZ,atString(""),r_Dim.X,r_Dim.Y+r_Dim.Height,r_Dim.Width,r_Dim.Height,m_ID++);
}
/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::AddFloatSpin(const atString& r_Label,f32& r_Value,f32 Min,f32 Max,f32 Step,const WidgetRect& r_Dim, bool &r_Locked)
{
	AddControl(DialogRes::CTRL_BUTTON,BS_AUTOCHECKBOX|WS_VISIBLE|WS_CHILD,atString(""),r_Dim.X,r_Dim.Y,10,r_Dim.Height,m_ID+3);
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Label,r_Dim.X+10,r_Dim.Y,(r_Dim.Width >> 1) - g_CtrlGapX-10,r_Dim.Height,m_ID);
	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),r_Dim.X + (r_Dim.Width >> 1),r_Dim.Y,(r_Dim.Width >> 1) - 10,r_Dim.Height,m_ID + 1);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),r_Dim.X + r_Dim.Width - 10,r_Dim.Y,10,r_Dim.Height,m_ID + 2);

	FloatSpinWdgt FloatSpin;

	FloatSpin.p_Value = &r_Value;
	FloatSpin.EditID = m_ID + 1;
	FloatSpin.SpinnerID = m_ID + 2;
	FloatSpin.Min = Min;
	FloatSpin.Max = Max;
	FloatSpin.Step = Step;

	BoolWdgt Add;
	Add.p_Bool = &r_Locked;
	Add.ButtonID = m_ID+3;
	m_Bools.PushAndGrow(Add);

	m_ID += 4;

	m_FloatSpins.PushAndGrow(FloatSpin);
}

/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::AddIntSpin(const atString& r_Label,s32& r_Value,s32 Min,s32 Max,s32 Step,const WidgetRect& r_Dim)
{
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Label,r_Dim.X,r_Dim.Y,(r_Dim.Width >> 1) - g_CtrlGapX,r_Dim.Height,m_ID);
	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),r_Dim.X + (r_Dim.Width >> 1),r_Dim.Y,(r_Dim.Width >> 1) - 30,r_Dim.Height,m_ID + 1);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),r_Dim.X + r_Dim.Width - 30,r_Dim.Y,30,r_Dim.Height,m_ID + 2);

	IntSpinWdgt IntSpin;

	IntSpin.p_Value = &r_Value;
	IntSpin.EditID = m_ID + 1;
	IntSpin.SpinnerID = m_ID + 2;
	IntSpin.Min = Min;
	IntSpin.Max = Max;
	IntSpin.Step = Step;

	m_ID += 3;

	m_IntSpins.PushAndGrow(IntSpin);
}

/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::AddStringEdit(const atString& r_Label,atString& r_Value,const WidgetRect& r_Dim, bool &r_Locked)
{
	AddControl(DialogRes::CTRL_BUTTON,BS_AUTOCHECKBOX|WS_VISIBLE|WS_CHILD,atString(""),r_Dim.X,r_Dim.Y,10,r_Dim.Height,m_ID+1);
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Label,r_Dim.X+10,r_Dim.Y,(r_Dim.Width >> 1) - g_CtrlGapX-10,r_Dim.Height,m_ID+2);
	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD|SS_LEFT,r_Value,r_Dim.X + (r_Dim.Width >> 1),r_Dim.Y,r_Dim.Width >> 1,r_Dim.Height,m_ID);

	StringWdgt Add;

	Add.p_String = &r_Value;
	Add.EditID = m_ID++;
	Add.controlType = STRINGEDIT_EDIT;
	m_Edits.PushAndGrow(Add);

	BoolWdgt Add1;
	Add1.p_Bool = &r_Locked;
	Add1.ButtonID = m_ID++;
	m_Bools.PushAndGrow(Add1);

	// label 
	m_ID++;
}

/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::AddTexButton(const atString& r_Label,atString& r_Value,const WidgetRect& r_Dim, bool &r_Locked)
{
	AddControl(DialogRes::CTRL_BUTTON,BS_AUTOCHECKBOX|WS_VISIBLE|WS_CHILD,atString(""),r_Dim.X,r_Dim.Y,10,r_Dim.Height,m_ID+1);
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Label,r_Dim.X+10,r_Dim.Y,(r_Dim.Width >> 1) - g_CtrlGapX-10,r_Dim.Height,m_ID+2);
	AddControl(atString("CustButton"),WS_VISIBLE|WS_CHILD|SS_LEFT,r_Value,r_Dim.X + (r_Dim.Width >> 1),r_Dim.Y,r_Dim.Width >> 1,r_Dim.Height,m_ID);

	StringWdgt Add;

	Add.p_String = &r_Value;
	Add.EditID = m_ID++;
	Add.controlType = STRINGEDIT_BUTTON;
	m_Edits.PushAndGrow(Add);

	BoolWdgt Add1;
	Add1.p_Bool = &r_Locked;
	Add1.ButtonID = m_ID++;
	m_Bools.PushAndGrow(Add1);

	// label 
	m_ID++;
}


/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::AddCheckBox(const atString& r_Label,bool& r_Value,const WidgetRect& r_Dim)
{
	AddControl(DialogRes::CTRL_BUTTON,BS_AUTOCHECKBOX|WS_VISIBLE|WS_CHILD,r_Label,r_Dim.X,r_Dim.Y,r_Dim.Width,r_Dim.Height,m_ID);

	BoolWdgt Add;
	Add.p_Bool = &r_Value;
	Add.ButtonID = m_ID++;
	m_Bools.PushAndGrow(Add);
}

void MaxEditDialog::AddVector2Select(const atString& r_Name,float *r_Value,float Min, float Max, float Step,const WidgetRect& r_Dim, bool &r_Locked)
{
	AddControl(DialogRes::CTRL_BUTTON,BS_AUTOCHECKBOX|WS_VISIBLE|WS_CHILD,atString(""),r_Dim.X,r_Dim.Y,10,r_Dim.Height,m_ID+5);
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,r_Dim.X+10,r_Dim.Y,(r_Dim.Width >> 1) - g_CtrlGapX-10,r_Dim.Height,m_ID);

	u32 ctrlOffset = r_Dim.X + (r_Dim.Width >> 1);

	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),ctrlOffset	,r_Dim.Y,g_SpinnerSize,r_Dim.Height,m_ID + 1);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinnerSize),r_Dim.Y,g_SpinButtSize,r_Dim.Height,m_ID + 2);

	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinButtSize)	,r_Dim.Y,g_SpinnerSize,r_Dim.Height,m_ID + 3);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinnerSize),r_Dim.Y,g_SpinButtSize,r_Dim.Height,m_ID + 4);

	FloatSpinWdgt FloatSpin;
	FloatSpin.p_Value = &r_Value[0];
	FloatSpin.EditID = m_ID + 1;
	FloatSpin.SpinnerID = m_ID + 2;
	FloatSpin.Min = Min;
	FloatSpin.Max = Max;
	FloatSpin.Step = Step;

	FloatSpinWdgt FloatSpin2;
	FloatSpin2.p_Value = &r_Value[1];
	FloatSpin2.EditID = m_ID + 3;
	FloatSpin2.SpinnerID = m_ID + 4;
	FloatSpin2.Min = Min;
	FloatSpin2.Max = Max;
	FloatSpin2.Step = Step;

	BoolWdgt Add;
	Add.p_Bool = &r_Locked;
	Add.ButtonID = m_ID+5;
	m_Bools.PushAndGrow(Add);

	m_ID += 6;

	m_FloatSpins.PushAndGrow(FloatSpin);
	m_FloatSpins.PushAndGrow(FloatSpin2);
}
void MaxEditDialog::AddVector3Select(const atString& r_Name,float *r_Value,float Min,float Max,float Step,const WidgetRect& r_Dim, bool &r_Locked)
{
	AddControl(DialogRes::CTRL_BUTTON,BS_AUTOCHECKBOX|WS_VISIBLE|WS_CHILD,atString(""),r_Dim.X,r_Dim.Y,10,r_Dim.Height,m_ID+7);
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,r_Dim.X+10,r_Dim.Y,(r_Dim.Width >> 1) - g_CtrlGapX-10,r_Dim.Height,m_ID);

	u32 ctrlOffset = r_Dim.X + (r_Dim.Width >> 1);

	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),ctrlOffset	,r_Dim.Y,g_SpinnerSize,r_Dim.Height,m_ID + 1);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinnerSize),r_Dim.Y,g_SpinButtSize,r_Dim.Height,m_ID + 2);

	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinButtSize)	,r_Dim.Y,g_SpinnerSize,r_Dim.Height,m_ID + 3);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinnerSize),r_Dim.Y,g_SpinButtSize,r_Dim.Height,m_ID + 4);

	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinButtSize)	,r_Dim.Y,g_SpinnerSize,r_Dim.Height,m_ID + 5);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinnerSize),r_Dim.Y,g_SpinButtSize,r_Dim.Height,m_ID + 6);

	FloatSpinWdgt FloatSpin;
	FloatSpin.p_Value = &r_Value[0];
	FloatSpin.EditID = m_ID + 1;
	FloatSpin.SpinnerID = m_ID + 2;
	FloatSpin.Min = Min;
	FloatSpin.Max = Max;
	FloatSpin.Step = Step;

	FloatSpinWdgt FloatSpin2;
	FloatSpin2.p_Value = &r_Value[1];
	FloatSpin2.EditID = m_ID + 3;
	FloatSpin2.SpinnerID = m_ID + 4;
	FloatSpin2.Min = Min;
	FloatSpin2.Max = Max;
	FloatSpin2.Step = Step;

	FloatSpinWdgt FloatSpin3;
	FloatSpin3.p_Value = &r_Value[2];
	FloatSpin3.EditID = m_ID + 5;
	FloatSpin3.SpinnerID = m_ID + 6;
	FloatSpin3.Min = Min;
	FloatSpin3.Max = Max;
	FloatSpin3.Step = Step;

	BoolWdgt Add;
	Add.p_Bool = &r_Locked;
	Add.ButtonID = m_ID+7;
	m_Bools.PushAndGrow(Add);

	m_ID += 8;

	m_FloatSpins.PushAndGrow(FloatSpin);
	m_FloatSpins.PushAndGrow(FloatSpin2);
	m_FloatSpins.PushAndGrow(FloatSpin3);
}
void MaxEditDialog::AddVector4Select(const atString& r_Name,float *r_Value,float Min,float Max,float Step,const WidgetRect& r_Dim, bool &r_Locked)
{
	AddControl(DialogRes::CTRL_BUTTON,BS_AUTOCHECKBOX|WS_VISIBLE|WS_CHILD,atString(""),r_Dim.X,r_Dim.Y,10,r_Dim.Height,m_ID+9);
	AddControl(DialogRes::CTRL_STATIC,WS_VISIBLE|WS_CHILD|SS_RIGHT,r_Name,r_Dim.X+10,r_Dim.Y,(r_Dim.Width >> 1) - g_CtrlGapX-10,r_Dim.Height,m_ID);

	u32 ctrlOffset = r_Dim.X + (r_Dim.Width >> 1);

	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),ctrlOffset	,r_Dim.Y,g_SpinnerSize,r_Dim.Height,m_ID + 1);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinnerSize),r_Dim.Y,g_SpinButtSize,r_Dim.Height,m_ID + 2);

	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinButtSize)	,r_Dim.Y,g_SpinnerSize,r_Dim.Height,m_ID + 3);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinnerSize),r_Dim.Y,g_SpinButtSize,r_Dim.Height,m_ID + 4);

	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinButtSize)	,r_Dim.Y,g_SpinnerSize,r_Dim.Height,m_ID + 5);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinnerSize),r_Dim.Y,g_SpinButtSize,r_Dim.Height,m_ID + 6);

	AddControl(atString("CustEdit"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinButtSize),r_Dim.Y,g_SpinnerSize,r_Dim.Height,m_ID + 7);
	AddControl(atString("SpinnerControl"),WS_VISIBLE|WS_CHILD,atString(""),(ctrlOffset+=g_SpinnerSize),r_Dim.Y,g_SpinButtSize,r_Dim.Height,m_ID + 8);

	FloatSpinWdgt FloatSpin;
	FloatSpin.p_Value = &r_Value[0];
	FloatSpin.EditID = m_ID + 1;
	FloatSpin.SpinnerID = m_ID + 2;
	FloatSpin.Min = Min;
	FloatSpin.Max = Max;
	FloatSpin.Step = Step;

	FloatSpinWdgt FloatSpin2;
	FloatSpin2.p_Value = &r_Value[1];
	FloatSpin2.EditID = m_ID + 3;
	FloatSpin2.SpinnerID = m_ID + 4;
	FloatSpin2.Min = Min;
	FloatSpin2.Max = Max;
	FloatSpin2.Step = Step;

	FloatSpinWdgt FloatSpin3;
	FloatSpin3.p_Value = &r_Value[2];
	FloatSpin3.EditID = m_ID + 5;
	FloatSpin3.SpinnerID = m_ID + 6;
	FloatSpin3.Min = Min;
	FloatSpin3.Max = Max;
	FloatSpin3.Step = Step;

	FloatSpinWdgt FloatSpin4;
	FloatSpin4.p_Value = &r_Value[3];
	FloatSpin4.EditID = m_ID + 7;
	FloatSpin4.SpinnerID = m_ID + 8;
	FloatSpin4.Min = Min;
	FloatSpin4.Max = Max;
	FloatSpin4.Step = Step;

	BoolWdgt Add;
	Add.p_Bool = &r_Locked;
	Add.ButtonID = m_ID+9;
	m_Bools.PushAndGrow(Add);

	m_ID += 10;

	m_FloatSpins.PushAndGrow(FloatSpin);
	m_FloatSpins.PushAndGrow(FloatSpin2);
	m_FloatSpins.PushAndGrow(FloatSpin3);
	m_FloatSpins.PushAndGrow(FloatSpin4);
}


/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::AddOKAndCancel(const WidgetRect& r_Dim)
{
	m_BtnOk = m_ID++;
	AddControl(DialogRes::CTRL_BUTTON,WS_VISIBLE|WS_CHILD,atString("OK"),r_Dim.X,r_Dim.Y,(r_Dim.Width >> 1) - MaxEditDialog::g_CtrlGapX,r_Dim.Height,m_BtnOk);
	m_BtnCancel = m_ID++;
	AddControl(DialogRes::CTRL_BUTTON,WS_VISIBLE|WS_CHILD,atString("Cancel"),r_Dim.X + (r_Dim.Width >> 1) + MaxEditDialog::g_CtrlGapX,r_Dim.Y,(r_Dim.Width >> 1) - (MaxEditDialog::g_CtrlGapX << 1),r_Dim.Height,m_BtnCancel);
}

/////////////////////////////////////////////////////////////////////////////////////
bool MaxEditDialog::DoModal(HWND ParentWnd)
{
	DLGTEMPLATE* p_DlgTemplate = GetTemplate();

	s32 ret = DialogBoxIndirectParam(	g_hDllInstance,
										p_DlgTemplate,
										ParentWnd,
										MaxEditDlgProc,
										(LPARAM)this);

	return (ret == 1) ? true : false;
}

/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::Finish(bool Save)
{
	if(Save)
	{
		SaveValues();
	}

	s32 i,Count;

	Count = m_IntSpins.GetCount();
	for(i=0;i<Count;i++)
	{
		ReleaseISpinner(m_IntSpins[i].p_SpinCtrl);
		ReleaseICustEdit(m_IntSpins[i].p_EditCtrl);
	}

	Count = m_FloatSpins.GetCount();
	for(i=0;i<Count;i++)
	{
		ReleaseISpinner(m_FloatSpins[i].p_SpinCtrl);
		ReleaseICustEdit(m_FloatSpins[i].p_EditCtrl);
	}

	Count = m_Edits.GetCount();
	for(i=0;i<Count;i++)
	{

		if(m_Edits[i].controlType==STRINGEDIT_EDIT)
			ReleaseICustEdit(m_Edits[i].p_EditCtrl);
		else
			ReleaseICustButton(m_Edits[i].p_ButtonCtrl);
	}

	EndDialog(m_Wnd,Save ? 1 : 0);
}

/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::SaveValues()
{
	s32 i,Count;

	Count = m_IntSpins.GetCount();
	for(i=0;i<Count;i++)
	{
		*m_IntSpins[i].p_Value = m_IntSpins[i].p_SpinCtrl->GetIVal();
	}

	Count = m_FloatSpins.GetCount();
	for(i=0;i<Count;i++)
	{
		*m_FloatSpins[i].p_Value = m_FloatSpins[i].p_SpinCtrl->GetFVal();
	}

	Count = m_Edits.GetCount();
	for(i=0;i<Count;i++)
	{
		char Buffer[NAME_LENGTH];
		if(m_Edits[i].controlType==STRINGEDIT_EDIT)
		{
			m_Edits[i].p_EditCtrl->GetText(Buffer,NAME_LENGTH);
			m_Edits[i].p_String->Set(Buffer, strlen(Buffer),0);
		}
		else
		{
			MSTR btnText;
			m_Edits[i].p_ButtonCtrl->GetText(btnText);
			m_Edits[i].p_String->Set(btnText.data(), btnText.Length(),0);
		}
	}

	Count = m_Bools.GetCount();
	for(i=0;i<Count;i++)
	{
		*m_Bools[i].p_Bool = (IsDlgButtonChecked(m_Wnd,m_Bools[i].ButtonID) == BST_CHECKED) ? true : false;
	}
}

/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::OnInitDialog(HWND Wnd)
{
	m_Wnd = Wnd;

	s32 i,Count;

	Count = m_IntSpins.GetCount();
	for(i=0;i<Count;i++)
	{
		HWND EditWnd = GetDlgItem(m_Wnd,m_IntSpins[i].EditID);
		HWND SpinWnd = GetDlgItem(m_Wnd,m_IntSpins[i].SpinnerID);

		m_IntSpins[i].p_SpinCtrl = GetISpinner(SpinWnd);
		m_IntSpins[i].p_EditCtrl = GetICustEdit(EditWnd);

		m_IntSpins[i].p_SpinCtrl->SetScale((float)m_IntSpins[i].Step);
		m_IntSpins[i].p_SpinCtrl->SetLimits(m_IntSpins[i].Min,m_IntSpins[i].Max);
		m_IntSpins[i].p_SpinCtrl->SetValue(*m_IntSpins[i].p_Value,FALSE);
		m_IntSpins[i].p_SpinCtrl->SetResetValue(*m_IntSpins[i].p_Value);
		m_IntSpins[i].p_SpinCtrl->LinkToEdit(EditWnd,EDITTYPE_INT);
	}

	Count = m_FloatSpins.GetCount();
	for(i=0;i<Count;i++)
	{
		HWND EditWnd = GetDlgItem(m_Wnd,m_FloatSpins[i].EditID);
		HWND SpinWnd = GetDlgItem(m_Wnd,m_FloatSpins[i].SpinnerID);

		m_FloatSpins[i].p_SpinCtrl = GetISpinner(SpinWnd);
		m_FloatSpins[i].p_EditCtrl = GetICustEdit(EditWnd);

		m_FloatSpins[i].p_SpinCtrl->SetScale(m_FloatSpins[i].Step);
		m_FloatSpins[i].p_SpinCtrl->SetLimits(m_FloatSpins[i].Min,m_FloatSpins[i].Max);
		m_FloatSpins[i].p_SpinCtrl->SetValue(*m_FloatSpins[i].p_Value,FALSE);
		m_FloatSpins[i].p_SpinCtrl->SetResetValue(*m_FloatSpins[i].p_Value);
		m_FloatSpins[i].p_SpinCtrl->LinkToEdit(EditWnd,EDITTYPE_FLOAT);
	}

	Count = m_Edits.GetCount();
	for(i=0;i<Count;i++)
	{
		HWND EditWnd = GetDlgItem(m_Wnd,m_Edits[i].EditID);
		if(m_Edits[i].controlType==STRINGEDIT_EDIT)
		{
			m_Edits[i].p_EditCtrl = GetICustEdit(EditWnd);
			m_Edits[i].p_EditCtrl->SetText((char*)((const char*)(*m_Edits[i].p_String)));
		}
		else
		{
			m_Edits[i].p_ButtonCtrl = GetICustButton(EditWnd);
			m_Edits[i].p_ButtonCtrl->SetText((char*)(*m_Edits[i].p_String->c_str()));
		}
	}

	Count = m_Bools.GetCount();
	for(i=0;i<Count;i++)
	{
		if(*m_Bools[i].p_Bool)
			CheckDlgButton(m_Wnd,m_Bools[i].ButtonID, BST_CHECKED);
	}
}

/////////////////////////////////////////////////////////////////////////////////////
void MaxEditDialog::OnCommand(s32 ID, HWND DlgWnd)
{
	if((m_BtnOk != -1) && (ID == m_BtnOk))
	{
		Finish(true);
	}
	else if((m_BtnCancel != -1) && (ID == m_BtnCancel))
	{
		Finish(false);
	}
	StringWdgt *pWdgt = NULL;
	for(int i=0;i<m_Edits.size();i++)
		if(m_Edits[i].EditID==ID)
			pWdgt = &m_Edits[i];
	if(pWdgt)
	{
		BOOL newMat, canceled;
		MtlBase* pBaseMtl = GetCOREInterface()->DoMaterialBrowseDlg(DlgWnd, BROWSE_MAPSONLY,newMat, canceled);
		BitmapTex *pTex = dynamic_cast<BitmapTex *>(pBaseMtl);
		if(!canceled && pTex)
		{
			CStr p, f, e;
			CStr name(pTex->GetMapName());
			SplitFilename(name, &p, &f, &e);
			pWdgt->p_ButtonCtrl->SetText(f);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK MaxEditDialog::MaxEditDlgProc(HWND DlgWnd,UINT Msg,WPARAM WParam,LPARAM LParam)
{
	MaxEditDialog* p_EditDlg = (MaxEditDialog*)GetWindowLongPtr(DlgWnd,DWLP_USER);

	switch(Msg)
	{
	case WM_INITDIALOG:

		SetWindowLongPtr(DlgWnd,DWLP_USER,LParam);

		p_EditDlg = (MaxEditDialog*)LParam;
		p_EditDlg->OnInitDialog(DlgWnd);
		return TRUE;

	case WM_COMMAND:

		if(p_EditDlg)
			p_EditDlg->OnCommand(LOWORD(WParam), DlgWnd);
		return TRUE;

	case WM_CLOSE:

		p_EditDlg->Finish(false);
		return TRUE;
	}

	return FALSE;
}
