#ifndef __RST_MAX_DIALOGMTLPRESET_H__
#define __RST_MAX_DIALOGMTLPRESET_H__

#include "rstMaxMtl/dialog.h"
#include "rstMaxMtl/mtl.h"

namespace rage {

class RstMtlMax;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
class RstMaxMtlPresetDlg : public ParamDlg
{
public:
	friend class RstMaxMtlDlg;

	RstMaxMtlPresetDlg(RstMtlMax* p_Mtl, IMtlParams* p_MtlParams);
	virtual ~RstMaxMtlPresetDlg();

	// from ParamDlg
	Class_ID ClassID() { return RSTMATERIAL_CLASS_ID; }
	void SetThing(ReferenceTarget *m) {}
	ReferenceTarget* GetThing();
	void SetTime(TimeValue t) {}
	void ReloadDialog() {}
	void DeleteThis();
	void ActivateDlg(BOOL onOff) {}

	void Reset();
	HWND Create(TSTR title="Material Template");

	void OnInitDialog(HWND DlgWnd);
	void OnButtonPressed(s32 ButtonID);

	void OnSavePreset();
	void OnUnlockPreset();

	void PopulateMaterialPreset(MaterialPreset& materialPreset);

	static INT_PTR CALLBACK RstMaxMtlPresetDlgProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);

protected:
	HWND m_Wnd;
	RstMtlMax* mp_Mtl;
	IMtlParams* mp_MtlParams;
};

} // namespace rage {

#endif //__RST_MAX_DIALOGMTL_H__

