/************************************************************************/
/* class CRstShaderValueNode
/* Encapsulate value, metadata and linking to sub-Values
/* Gunnar Droege
/************************************************************************/
#include "RstShaderValueNode.h"
#include "dialogShader.h"
#include "utility.h"
#include "../rageMaxDataStore/IMaxDataStore.h"

using namespace rage;

CRstShaderValueNode::eShaderValueType CRstShaderValueNode::gGrcEffectTypeLUT[] =
{
	eShaderValTypeNone,
	eShaderValTypeInt,
	eShaderValTypeFloat,
	eShaderValTypeVec2,
	eShaderValTypeVec3, 
	eShaderValTypeVec4, 
	eShaderValTypeTexture, 
	eShaderValTypeBool,
	eShaderValTypeMatrix43,
	eShaderValTypeMatrix44, 
	eShaderValTypeString
};

//copy constructor
//Shallow
CRstShaderValueNode::CRstShaderValueNode(const RstMtlMax *rstMtlMax, CRstShaderValueNode &cp, CRstShaderValueNode *parent)
: pRstMtlMax(const_cast<RstMtlMax*>(rstMtlMax))
, m_type(cp.m_type)
, m_rageVar(cp.m_rageVar)
, m_flags(cp.m_flags)
, m_parent(parent)
, mb_deleteMe(true)
, m_name(cp.m_name)
, m_RefTargetIndex(cp.m_RefTargetIndex)
, mp_DlgExtra(NULL)
, m_MaxDataHWnd(NULL)
{
	switch(m_type)
	{
	case eShaderValTypeVec2:
		m_value.m_Vector2 = new Vector2(*cp.m_value.m_Vector2);
	case eShaderValTypeVec3:
		m_value.m_Vector3 = new Vector3(*cp.m_value.m_Vector3);
	case eShaderValTypeVec4:
		m_value.m_Vector4 = new Vector4(*cp.m_value.m_Vector4);
		break;
	default:
		m_value = cp.Value();
		break;
	}
// 	for(RsAttrNodeArray::iterator iInput = cp.Inputs().begin();iInput<cp.Inputs().end();iInput++)
// 		m_inputs.PushAndGrow((*iInput));
}

CRstShaderValueNode::CRstShaderValueNode(
					const RstMtlMax *rstMtlMax,
					atString &name,
					ShaderInstanceData::ShaderVar rageVar,
					eShaderValueType type,
					u32 flags,
					int refTargetIndex
					)
					: m_name(name)
					, pRstMtlMax(const_cast<RstMtlMax*>(rstMtlMax))
					, m_rageVar(rageVar)
					, m_type(type)
					, m_parent(NULL)
					, m_flags(flags)
					, m_RefTargetIndex(refTargetIndex)
					, mb_deleteMe(true)
					, mp_DlgExtra(NULL)
					, m_MaxDataHWnd(NULL)
{
	m_value.m_Void = NULL;
// 	if(type==eShaderValTypeTexture)
// 	{
// 		pRstMtlMax->SetReference(pRstMtlMax->NumRefs()+1, m_value);
// 	}
}

void HierarchyCloseAllDialogs(CRstShaderValueNode* currNode, bool onlyChildren=false)
{
	for(int i=0;i<currNode->Inputs().GetCount();i++)
	{
		HierarchyCloseAllDialogs(currNode->Inputs()[i],false);
	}
	if(!onlyChildren)
		currNode->DeleteDialog();
}

CRstShaderValueNode::~CRstShaderValueNode()
{
	// in case we stored a pointer:
	switch(m_type)
	{
	case eShaderValTypeVec2:
		delete m_value.m_Vector2;
	case eShaderValTypeVec3:
		delete m_value.m_Vector3;
	case eShaderValTypeVec4:
		delete m_value.m_Vector4;
		break;
	default:
		// gracefully fuck off
		break;
	}
	HierarchyCloseAllDialogs(this);
}

void CRstShaderValueNode::DeleteDialog()
{
	if(mp_DlgExtra)
	{
		delete mp_DlgExtra;
		mp_DlgExtra = NULL;
	}
	if(m_MaxDataHWnd)
	{
//		theIMaxDataStore.DeleteRollupPage(m_MaxDataHWnd, pRstMtlMax->GetMtlParams());
		m_MaxDataHWnd = NULL;
	}
}

void CRstShaderValueNode::ToggleDialog()
{
	if(mp_DlgExtra || m_MaxDataHWnd)
		DeleteDialog();
	else
		CreateShaderParamDialog(pRstMtlMax->GetMtlParams());
}

RstMaxShaderDlg* CRstShaderValueNode::CreateShaderParamDialog(IMtlParams* pMtlParams, grcEffect *p_Shader)
{
	HierarchyCloseAllDialogs(this);
	if(mp_DlgExtra)
	{
		delete mp_DlgExtra;
	}

#if SHADER_ATTR_TREE

	if(m_inputs.GetCount()>0)
	{
		mp_DlgExtra = new RstMaxShaderDlg(pRstMtlMax, pMtlParams);

	//	s32 i,NumVars;
		s32 NumTextures = 0;
	//	bool IsAlpha = IsAlphaShader();

		for(RsAttrNodeArray::iterator iNode = m_inputs.begin(); iNode<m_inputs.end();iNode++)
		{
			CRstShaderValueNode *pNode = (*iNode);
			grmVariableInfo VarInfo;
			bool varInfoFound = false;
			if(p_Shader)
			{
				for(int k=0;k<p_Shader->GetInstancedVariableCount();k++)
				{
					p_Shader->GetInstancedVariableInfo(k,VarInfo);
					if(!strcmp(VarInfo.m_Name, pNode->Name().c_str()))
					{
						varInfoFound = true;
						break;
					}
				}
				grcEffectVar handle = p_Shader->LookupVar(VarInfo.m_Name);
			}

			atString Label = pNode->Name();
			Label += ":";

			switch(pNode->Type())
			{
			case CRstShaderValueNode::eShaderValTypeFloat:
				{
					float min=0, max=100, step=0.1f;
					if(varInfoFound)
					{
						min = VarInfo.m_UiMin;
						max = VarInfo.m_UiMax;
						step = VarInfo.m_UiStep;
					}
					mp_DlgExtra->AddFloatSpinner(Label,pNode,min,max,step);
				}
				break;
			case CRstShaderValueNode::eShaderValTypeTexture:
				{
					if(pRstMtlMax->IsAlphaShader())
					{
						// Find first sub texture of texture
						CRstShaderValueNode *pSubTexNode = pRstMtlMax->FindSingleShaderValueNode(pNode, SHADERVALNAME_ALL, CRstShaderValueNode::eShaderValTypeTexture);
						if(!pNode || !pSubTexNode)
							continue;
						mp_DlgExtra->AddMapSelect(	Label,
							pNode,
							rstMaxUtility::GetTexMapName(pNode->Value().m_Texture),//mp_Mtl->m_Textures[NumTextures]),
							rstMaxUtility::GetTexMapName(pSubTexNode->Value().m_Texture), //mp_Mtl->m_TextureAlphas[NumTextures])
							pNode->RefTargetIndex(),
							pSubTexNode->RefTargetIndex()
							);
					}
					else
					{
						mp_DlgExtra->AddMapSelect(	Label,
							pNode,
							rstMaxUtility::GetTexMapName((Texmap*)pRstMtlMax->GetReference(pNode->RefTargetIndex())),
							pNode->RefTargetIndex()
							);
					}
				}
				break;

			case CRstShaderValueNode::eShaderValTypeVec2:
				{
					float min=0, max=100, step=0.1f;
					if(varInfoFound)
					{
						min = VarInfo.m_UiMin;
						max = VarInfo.m_UiMax;
						step = VarInfo.m_UiStep;
					}
					mp_DlgExtra->AddVector2Select(Label,pNode,min,max,step);
				}
				break;
			case CRstShaderValueNode::eShaderValTypeVec3:
				{
					float min=0, max=100, step=0.1f;
					if(varInfoFound)
					{
						min = VarInfo.m_UiMin;
						max = VarInfo.m_UiMax;
						step = VarInfo.m_UiStep;
					}
					mp_DlgExtra->AddVector3Select(Label,pNode,min,max,step);
				}
				break;
			case CRstShaderValueNode::eShaderValTypeVec4: 
				{
					float min=0, max=100, step=0.1f;
					if(varInfoFound)
					{
						min = VarInfo.m_UiMin;
						max = VarInfo.m_UiMax;
						step = VarInfo.m_UiStep;
					}
					mp_DlgExtra->AddVector4Select(Label,pNode,min,max,step);
				}
				break;
	// 		case grcEffect::VT_MATRIX34:
	// 		case grcEffect::VT_MATRIX44:
			default:
				mp_DlgExtra->AddStatic(atString(pNode->Name()));
				break;
			}
		}

		atString namebuffer(Name()," inputs");
		mp_DlgExtra->Create(namebuffer.c_str());
	}
// 	else
// 		mp_DlgExtra->AddStatic("No inputs to display.");

	// needs rageMaxDataStore.lib
// 	std::string buffer;
// 	if(Type()==eShaderValTypeTexture && Value().m_Texture)
// 	{
// 		theIMaxDataStore.GetClassFromRefTarget((ReferenceTarget*)Value().m_Texture, buffer);
// 		//		theIMaxDataStore.EditDataModal(Value().m_Texture);
// 		m_MaxDataHWnd = theIMaxDataStore.AddRollupPage(Value().m_Texture, pMtlParams);
// 	}
// 

	pMtlParams->GetMtlEditorRollup()->UpdateLayout();

#endif
	return mp_DlgExtra;
}

BOOL CRstShaderValueNode::OpenShaderParamDialog()
{
	mp_DlgExtra->Create();
	return TRUE;
}