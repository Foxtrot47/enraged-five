#ifndef __RST_MAX_H__
#define __RST_MAX_H__

#define RSTUTILITY_CLASS_ID Class_ID(0x75e85713, 0x1f0b306c)

#ifdef RSTMAXMTL_DLL
#define RSTMTL_EXPORT __declspec( dllexport )
#else
#ifdef RSTMAXMTL_NOIMPORT
#define RSTMTL_EXPORT 
#else
#define RSTMTL_EXPORT __declspec( dllimport )
#endif //RSTMAXMTL_NOIMPORT
#endif //RSTMAXMTL_DLL

#include <utilapi.h>
#include "grmodel/shader.h"
#include "rst/shaderplugin.h"

namespace rage {

class atString;

/////////////////////////////////////////////////////////////////////////////////////////////////
class RstMax : public UtilityObj
{
public:
	static ClassDesc* Creator();

	virtual ~RstMax();

	//from UtilityObj

	virtual void BeginEditParams(Interface *ip,IUtil *iu);
	virtual void EndEditParams(Interface *ip,IUtil *iu);
	virtual void DeleteThis();

	//

	static void InitPaths(const MCHAR* shaderPath, const MCHAR* presetPath, const MCHAR* defaultShaderName, const MCHAR* defaultAlphaShaderName);
	static void InitShaderLib();

	//if you are trying to use the shader library from a different
	//plugin/dll then always go through these functions to make sure
	//you are using the correct instance of the shader library contained
	//in rstMaxMtl

	RSTMTL_EXPORT static grcEffect* FindShader(const atString& ShaderName);
	RSTMTL_EXPORT static s32 GetShaderCount();
	RSTMTL_EXPORT static const char* GetShaderName(s32 Index);

	RSTMTL_EXPORT static s32 GetPresetCount();
	RSTMTL_EXPORT static const char* GetPresetName(s32 Index);

	RSTMTL_EXPORT static rageRstShaderDbEntry* GetDbEntry(const atString& r_Name);

	RSTMTL_EXPORT static const atString& GetShaderPath();
	RSTMTL_EXPORT static const atString& GetPresetPath();
	RSTMTL_EXPORT static const atString& GetDefaultShaderName();
	RSTMTL_EXPORT static const atString& GetDefaultAlphaShaderName();

	//Helper Functions for Handling Value Change Events in Dialogs.
	static void NotifyValueChangedCallbacks(const char* shaderName, const char* attributeName);
	static void AddValueChangedCallback(const char* script);
	static void RemoveValueChangedCallback(const char* script);
	static BOOL ExecuteMaxScript(const char* maxScript);

protected:
	static INT_PTR CALLBACK RstMaxDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	static atString m_ShaderPath;
	static atString m_PresetPath;
	static atString m_ShaderName;
	static atString m_AlphaName;
	static atArray<atString> m_PresetList;
	static atArray<const char*> ValueChangedCallbacks;

	HWND m_hWnd;
};

} //namespace rage {

#endif __RST_MAX_H__
