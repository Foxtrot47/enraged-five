#ifndef __REX_MAX_ROCKSTAR_CONVERTERDRAWABLEMESH_H__
#define __REX_MAX_ROCKSTAR_CONVERTERDRAWABLEMESH_H__

#include "rexmaxtogeneric/convertermesh.h"
#include "rexgeneric/objectmesh.h"

namespace rage {

////////////////////////////////////////////////////////////////////////////////////////////////
class rexConverterMaxDrawableMeshHierarchy : public rexConverterMaxMeshHierarchy
{
public:
	virtual rexConverter *CreateNew() const  { return new rexConverterMaxDrawableMeshHierarchy; }
	
protected:
	virtual rexObjectHierarchy *CreateHierarchyNode() const { return new rexObjectGenericMeshHierarchyDrawable; }
};

} // namespace rage {

#endif //__REX_MAX_ROCKSTAR_CONVERTERDRAWABLEMESH_H__
