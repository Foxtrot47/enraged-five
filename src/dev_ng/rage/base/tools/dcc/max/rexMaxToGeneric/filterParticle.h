// 
// rexMayaToGeneric/filterParticle.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REX_MAX_ROCKSTAR_FILTERPARTICLE_H__
#define __REX_MAX_ROCKSTAR_FILTERPARTICLE_H__

#include "rexbase/filter.h"
#include "rexmax/object.h"
#include "rexmaxutility/utility.h"

#include "rageMaxParticle/Particle.h"

namespace rage {

	/////////////////////////////////////////////////////////////////////////////////////

	class rexFilterMaxParticle : public rexFilter
	{
	public:
		virtual bool AcceptObject(const rexObject& inputObject) const 
		{ 
			const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>(&inputObject);
			if(!p_Obj)
				return false;

			if( rexMaxUtility::IsType(p_Obj->GetMaxNode(),RAGEPARTICLE_CLASS_ID) )
				return true;
			else
				return false;
		}

		virtual rexFilter* CreateNew() const { return new rexFilterMaxParticle; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

}//End namespace rage

#endif //__REX_MAX_ROCKSTAR_FILTERPARTICLE_H__
