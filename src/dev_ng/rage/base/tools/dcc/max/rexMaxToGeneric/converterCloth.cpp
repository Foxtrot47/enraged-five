#include "rexBase/module.h"
#include "rexMax/object.h"
#include "rexMaxUtility/utility.h"
#include "rexMaxUtility/maxobj.h"
#include "MaxUtil/MaxUtil.h"

#include "rexGeneric/objectCloth.h"
#include "rexMaxToGeneric/converterCloth.h"
#include "rageMaxLODHierarchy/HierarchyInterface.h"
#include "rageMaxDataStore/IFpMaxDataStore.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyClothMesh::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexResult result( rexResult::PERFECT );

	rexConverterMaxClothPiece* conv = dynamic_cast<rexConverterMaxClothPiece*>( module.m_Converter );

	if( !conv )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	conv->SetMesh( atString(value.toString()) );

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyClothBounds::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexResult result( rexResult::PERFECT );

	rexConverterMaxClothPiece* conv = dynamic_cast<rexConverterMaxClothPiece*>( module.m_Converter );

	if( !conv )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	conv->AppendBound(atString(value.toString()));

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterMaxEnvCloth::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& /*defaultName*/ ) const
{
	rexResult result (rexResult::PERFECT);

	rexObjectGenericClothMeshGroup *pClothGroup = new rexObjectGenericClothMeshGroup();
	outputObject = pClothGroup;

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterMaxCharCloth::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& /*defaultName*/ ) const
{
	rexResult result (rexResult::PERFECT);

	rexObjectGenericCharClothMesh *pClothGroup = new rexObjectGenericCharClothMesh();
	outputObject = pClothGroup;

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterMaxClothPiece::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
{
	bool bUseInputObjects = true;

	m_PieceName = defaultName;

	rexResult result (rexResult::PERFECT);

	mp_ClothInfo = new rexObjectGenericClothMeshInfo();

	mp_ClothInfo->SetName( defaultName );

	if(!bUseInputObjects)
	{
		mp_ClothInfo->m_ClothMeshName = m_MeshName;
	}
	else
	{
		int nObjects = inputObjectArray.GetCount();
		for(int objIdx=0; objIdx<nObjects; objIdx++)
		{
			rexObject* subObject = NULL;
			ConvertSubObject(*inputObjectArray[objIdx], subObject);
		}
	}

	outputObject = mp_ClothInfo;

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxClothPiece::ConvertSubObject(const rexObject& object,rexObject*& newObject) const
{
	rexResult result(rexResult::PERFECT);
	const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>( &object );

	if(!p_Obj)
	{
		result.Set(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);
		return result;
	}

	if(m_ProgressBarTextCB)
	{
		static char message[1024];
		sprintf(message,"Converting mesh data at %s",p_Obj->GetMaxNode()->GetName());
		(*m_ProgressBarTextCB)(message);
	}

	INode* p_Node = p_Obj->GetMaxNode();

	return ConvertSubObject(p_Node,newObject);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxClothPiece::ConvertSubObject(INode* p_Node,rexObject*& newObject) const
{
	rexResult result(rexResult::PERFECT);

	atString Out("exporting cloth mesh: ");
	Out += p_Node->GetName();
	result.AddMessageCtx(p_Node->GetName(), Out);

	INode* p_RootBone = MaxUtil::GetSkinRootBone(MaxUtil::GetSkin(p_Node));

	if(!IsAcceptableHierarchyNode(p_Node))
	{
		return result;
	}

	if(rexMaxUtility::IsType(p_Node,Class_ID(TRIOBJ_CLASS_ID,0)))
	{
		//This is a mesh object
		// either no child relation ship defined or NOT a renderable child
		if(rexMaxUtility::GetBoolProperty(p_Node, "optimseMeshForCloth"))
		{
			// Get the MaxDataStore interface so we can read node attributes
			IFpMaxDataStore* pDataStore = GetMaxDataStoreInterface();
			// Get the name of the particle effect
			int attrIndex = pDataStore->GetAttrIndex("Gta Object", "Is Cloth");
			if( pDataStore )
			{
				if(m_bFoundMesh && 	pDataStore->GetAttrBool(p_Node, attrIndex))
				{
					//There is more than one mesh object tagged to this cloth piece
					result.Combine( rexResult::ERRORS );
					result.AddErrorCtx(p_Node->GetName(), "More than one mesh object has been tagged in the cloth piece '%s'", (const char*)m_PieceName);
					return result;
				}
				m_bFoundMesh = true;
			}

			mp_ClothInfo->m_ClothMeshName = p_Node->GetName();
			if(!rexMaxUtility::CheckMaxMeshContinuinity(p_Node, result))
			{
				result.AddErrorCtx(p_Node->GetName(),"Check for mesh %s failed. See former errors and revise the mesh: https://devstar.rockstargames.com/wiki/index.php/Cloth_Export_Errors#Cloth_mesh_consistency", mp_ClothInfo->m_ClothMeshName.c_str());
			}
		}
	}
	else
	{
		if( rexMaxUtility::IsCollisionObject(p_Node) )
		{
			INode* pBoundParentNode = p_Node->GetParentNode();
			if(pBoundParentNode)
			{
				mp_ClothInfo->m_BoneNames.PushAndGrow( rexMaxUtility::ReplaceWhitespaceCharacters(pBoundParentNode->GetName() ) );
			}
			else
			{
				result.Combine( rexResult::WARNINGS );
				result.AddWarningCtx(p_Node->GetName(), "Cloth piece '%s' has the bound '%s' tagged to influence the cloth which has no parent, this bound will not be included in the collision set for the cloth piece",
					(const char*)m_PieceName, (const char*)p_Node->GetName());
			}
		}
	}

	rexMaxUtility::SetSkinBindPose(p_RootBone,true);

	rexObjectGenericMesh* mesh = new rexObjectGenericMesh();

	if(!mesh)
		return rexResult(rexResult::ERRORS,rexResult::OUT_OF_MEMORY);

	rexMaxUtility::ConvertMaxMeshNodeIntoGenericMesh(	p_Node,
		mesh,
		result,
		mp_ClothInfo->m_ContainedObjects.GetCount(),
		false,
		false,
		true,
		false);

	newObject = mesh;

	mp_ClothInfo->m_ContainedObjects.PushAndGrow(newObject,mp_ClothInfo->m_ContainedObjects.GetCount() + 1);

	rexMaxUtility::SetSkinBindPose(p_RootBone,false);

	s32 i,ChildCount = p_Node->NumberOfChildren();

	for(i=0;i<ChildCount;i++)
	{
		INode* p_ChildNode = p_Node->GetChildNode(i);

		rexObject* subObject = NULL;
		ConvertSubObject(p_ChildNode,subObject);
	}

	IFpSceneHierarchy* pRsSceneLink = GetSceneLinkInterface();
	if(pRsSceneLink)
	{
		INodePtr pLowerLOD = pRsSceneLink->GetParent(eLinkType_DRAWLOD, p_Node);
		if(NULL!=pLowerLOD)
		{
			rexObject* subObject = NULL;
			ConvertSubObject(pLowerLOD,subObject);
		}
	}


	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexConverterMaxClothPiece::IsAcceptableHierarchyNode(INode* p_Node) const
{
	return rexMaxUtility::IsType(p_Node,Class_ID(TRIOBJ_CLASS_ID,0)) || rexMaxUtility::IsCollisionObject(p_Node);
}
