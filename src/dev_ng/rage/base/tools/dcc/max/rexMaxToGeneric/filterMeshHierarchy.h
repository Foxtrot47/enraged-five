#ifndef __REX_MAX_ROCKSTAR_FILTERMESHHIERARCHY_H__
#define __REX_MAX_ROCKSTAR_FILTERMESHHIERARCHY_H__

#include "rexbase/filter.h"
#include "rexmax/object.h"
#include "rexmaxutility/utility.h"

namespace rage {

class rexFilterMaxMeshHierarchy : public rexFilter
{
public:

	virtual bool AcceptObject(const rexObject& inputObject) const 
	{ 
		const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>(&inputObject);

		if(!p_Obj)
			return false;

		// ALupinacci 1/13/09: This filter has been disabled because the mesh hierarchy converter
		// now handles its own filtering, allowing it to skip over invalid nodes and proceed onto
		// its children nodes as it sees fit
		return true; //return rexMaxUtility::IsType(p_Obj->GetMaxNode(),Class_ID(TRIOBJ_CLASS_ID,0));
	}

	virtual rexFilter* CreateNew() const { return new rexFilterMaxMeshHierarchy; }
};

} // namespace rage {

#endif //__REX_MAX_ROCKSTAR_FILTERMESHHIERARCHY_H__
