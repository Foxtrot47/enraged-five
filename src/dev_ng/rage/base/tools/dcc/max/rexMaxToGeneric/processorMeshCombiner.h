
//
// rexMaxToGeneric/processMeshCombiner.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexProcessorMaxMeshCombiner
//			 -- consolidates mesh hierarchies into fewer meshes using a binary space partition
//
////////////////////////////////////////////////////////////////////////////////////////
//
//		INHERITS FROM:
//				rexProcessor
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAX_ROCKSTAR_MESH_COMBINER_H__
#define __REX_MAX_ROCKSTAR_MESH_COMBINER_H__

#include "rexBase/processor.h"
#include "rexBase/property.h"
#include "rexBase/object.h"
#include "rexGeneric/processorMeshCombiner.h"
#include <float.h>
/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexProcessorMaxBoundCombiner : public rexProcessorMeshCombiner
{
public:
	rexProcessorMaxBoundCombiner() : m_MaxBoundPolyCountToCombine(0), rexProcessorMeshCombiner()
	{
		rexProcessorMeshCombiner::SetBoundMode(true);
#if HACK_GTA4
		m_ExportAsComposite = false;
		m_Enabled = true;
#endif // HACK_GTA4
	}

	virtual rexResult		Process( rexObject* object ) const;
#if HACK_GTA4
	virtual rexProcessor*	CreateNew() const { rexProcessorMaxBoundCombiner* c = new rexProcessorMaxBoundCombiner; c->m_BoundMode = m_BoundMode; c->m_CombineAcrossBones = m_CombineAcrossBones; c->m_ExportAsComposite = m_ExportAsComposite; return c; }
#else
#endif // HACK_GTA4
	void SetBoundMaxPolyCountToCombine( int maxPolyCountToCombine )				{ m_MaxBoundPolyCountToCombine = maxPolyCountToCombine; }
	int  GetBoundMaxPolyCountToCombine() const									{ return m_MaxPolyCountToCombine; }
#if HACK_GTA4
	void SetExportAsComposite( bool ExportAsComposite ) { m_ExportAsComposite = ExportAsComposite; }
	bool GetExportAsComposite() { return m_ExportAsComposite; }
	void SetEnabled( bool Enabled ) { m_Enabled = Enabled; }
	bool GetEnabled() { return m_Enabled; }
#endif // HACK_GTA4
	void SetProgressSize(s32 Size) const;
	void SetProgressPos(s32 Pos) const;

protected:
#if HACK_GTA4
	void CombineMeshes(atArray< rexObject* >& r_InOut) const;
#else
	void CombineMeshes(atArray< rexObjectGenericMesh* >& r_Meshes) const;
#endif // HACK_GTA4
	int		m_MaxBoundPolyCountToCombine;
#if HACK_GTA4
	bool	m_ExportAsComposite;
	bool	m_Enabled;
#endif // HACK_GTA4
};

///////////////////////////////////////////////////////////////////////////////////////

class rexPropertyMaxBoundMaxPolyCountToCombine : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxBoundMaxPolyCountToCombine; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

///////////////////////////////////////////////////////////////////////////////////////

#endif // __REX_MAX_ROCKSTAR_MESH_COMBINER_H__
