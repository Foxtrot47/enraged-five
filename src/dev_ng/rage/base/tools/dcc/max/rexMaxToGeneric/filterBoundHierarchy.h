#ifndef __REX_MAX_ROCKSTAR_FILTERBOUNDHIERARCHY_H__
#define __REX_MAX_ROCKSTAR_FILTERBOUNDHIERARCHY_H__

#include "rexbase/filter.h"
#include "rexmax/object.h"
#include "rexmaxutility/maxobj.h"

namespace rage {

class rexFilterMaxBoundHierarchy : public rexFilter
{
public:
	virtual bool AcceptObject(const rexObject& inputObject) const 
	{ 
		return true;

#if 0
		const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>(&inputObject);

		if(!p_Obj)
			return false;

		if(	rexMaxUtility::IsType(p_Obj->GetMaxNode(),COLLISION_MESH_CLASS_ID) ||
			rexMaxUtility::IsType(p_Obj->GetMaxNode(),COLLISION_BOX_CLASS_ID) ||
			rexMaxUtility::IsType(p_Obj->GetMaxNode(),COLLISION_SPHERE_CLASS_ID))
		{
			return true;
		}
		
		return false;
#endif
	}
	
	virtual rexFilter* CreateNew() const { return new rexFilterMaxBoundHierarchy; }
};

} //namespace rage {

#endif //__REX_MAX_ROCKSTAR_FILTERBOUNDHIERARCHY_H__
