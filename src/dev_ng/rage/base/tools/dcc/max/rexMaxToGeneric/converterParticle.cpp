// 
// rexMaxToGeneric/converterParticle.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
#include "rexMax/object.h"
#include "rexmaxutility/utility.h"

#include "rexGeneric/objectParticle.h"
#include "rexMaxToGeneric/converterParticle.h"

#include "rageMaxParticle/Particle.h"
#include "rageMaxDataStore/IFpMaxDataStore.h"

namespace rage {

	///////////////////////////////////////////////////////////////////////////////

	rexResult rexConverterMaxParticle::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
	{
		rexResult result( rexResult::PERFECT );

		// Get the MaxDataStore interface so we can read node attributes
		IFpMaxDataStore* pDataStore = GetMaxDataStoreInterface();
		if( !pDataStore )
		{
			result.Set( rexResult::ERRORS, rexResult::CANNOT_CREATE_OBJECT );
			result.AddError( "Could not get MaxDataStore interface! Please make sure the rageMaxDataStore plug-in is loaded." );
			return result;
		}

		rexObjectGenericParticleGroup* particleGroup = CreateNewParticleGroup();

		int inputObjectCount = inputObjectArray.GetCount();

		if( m_ProgressBarObjectCountCB )
			(*m_ProgressBarObjectCountCB)( inputObjectCount );

		for( int a = 0; a < inputObjectCount; a++ )
		{		
			if( m_ProgressBarObjectCurrentIndexCB )
				(*m_ProgressBarObjectCurrentIndexCB)( a );

			rexMaxObject* p_Obj = dynamic_cast<rexMaxObject*>(inputObjectArray[a]);
			if(!p_Obj)
				continue;

			INode* pNode = p_Obj->GetMaxNode();
			if(!pNode)
				continue;

			if( rexMaxUtility::IsType(pNode,RAGEPARTICLE_CLASS_ID) )
			{
				if( m_ProgressBarTextCB )
				{
					static char message[1024];
					sprintf( message, "Converting particle data at %s", pNode->GetName() );
					(*m_ProgressBarTextCB) ( message );
				}

				rexObjectGenericParticle* particle = new rexObjectGenericParticle;
				particle->SetName( pNode->GetName() );

				// Get the local transform
				Matrix34 localMat;
				rexMaxUtility::GetLocalMatrix(pNode, 0, localMat);
				particle->m_LocalMatrix = localMat;

				// Get the name of the particle effect
				int attrIndex = pDataStore->GetAttrIndex("RAGE Particle", "Name");
				if( attrIndex >= 0 )
					particle->m_EffectName = pDataStore->GetAttrStr(pNode, attrIndex);

				// Get the effect's trigger type
				attrIndex = pDataStore->GetAttrIndex("RAGE Particle", "Trigger");
				if( attrIndex >= 0 )
					particle->m_TriggerType = pDataStore->GetAttrInt(pNode, attrIndex);

				// Determine if the effect is attached to a bone or not
				bool attachToAll = false;
				attrIndex = pDataStore->GetAttrIndex("RAGE Particle", "Attach to All");
				if( attrIndex >= 0 )
					attachToAll = pDataStore->GetAttrBool(pNode, attrIndex);

				if( !attachToAll )
				{
					// If not attached to all, find the bone this node is parented to and
					// get its information so that we can correctly generate the bone's ID
					// in the serialization step
					INode* pBoneNode = rexMaxUtility::GetParentBone(pNode);
					if( pBoneNode != NULL )
					{
						rexObjectGenericParticle::AttachBone* pAttachBone = new rexObjectGenericParticle::AttachBone();
						pAttachBone->m_Name = rexMaxUtility::ReplaceWhitespaceCharacters(pBoneNode->GetName());
						pAttachBone->m_Tag = rexMaxUtility::GetBoneID(pBoneNode);
						pAttachBone->m_IsRoot = rexMaxUtility::IsRootSkelNode(pBoneNode);

						particle->m_AttachBone = pAttachBone;
					}
				}

				// Get the effect's scale
				attrIndex = pDataStore->GetAttrIndex("RAGE Particle", "Scale");
				if( attrIndex >= 0 )
					particle->m_Scale = pDataStore->GetAttrFloat(pNode, attrIndex);

				// Get the effect's probability
				attrIndex = pDataStore->GetAttrIndex("RAGE Particle", "Probability");
				if( attrIndex >= 0 )
					particle->m_Probability = pDataStore->GetAttrInt(pNode, attrIndex);

				// Get the effect's tint (if it tinted)
				attrIndex = pDataStore->GetAttrIndex("RAGE Particle", "Has Tint");
				if( attrIndex >= 0 )
					particle->m_IsTinted = pDataStore->GetAttrBool(pNode, attrIndex);

				if( particle->m_IsTinted )
				{
					attrIndex = pDataStore->GetAttrIndex("RAGE Particle", "R");
					if( attrIndex >= 0 )
						particle->m_TintColor.x = (float)pDataStore->GetAttrInt(pNode, attrIndex);

					attrIndex = pDataStore->GetAttrIndex("RAGE Particle", "G");
					if( attrIndex >= 0 )
						particle->m_TintColor.y = (float)pDataStore->GetAttrInt(pNode, attrIndex);

					attrIndex = pDataStore->GetAttrIndex("RAGE Particle", "B");
					if( attrIndex >= 0 )
						particle->m_TintColor.z = (float)pDataStore->GetAttrInt(pNode, attrIndex);
				}

				particleGroup->m_ContainedObjects.PushAndGrow( particle,(u16)inputObjectCount );
			}
		}

		outputObject = particleGroup;

		return result;
	}

	///////////////////////////////////////////////////////////////////////////////

}//End namespace rage
