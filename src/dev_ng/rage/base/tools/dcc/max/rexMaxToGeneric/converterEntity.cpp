#include "rexBase/module.h"
#include "rexMaxToGeneric/converterEntity.h"
#include "rexRage/serializerEntity.h"
#include "rexGeneric/objectEntity.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxEntity::Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const
{
	rexResult result(rexResult::PERFECT);

	rexObjectGenericEntity* entity = new rexObjectGenericEntity;
	entity->SetName(defaultName);

	outputObject = entity;
	
	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxEntityTypeFileName::ModifyObject(rexObject& obj,rexValue value,const atArray<rexObject*>& objects) const
{
	rexObjectGenericEntity* entity = dynamic_cast<rexObjectGenericEntity*>(&obj);

	if(!entity)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	entity->m_EntityTypeFileName = value.toString();
	
	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxEntitySkipBoundSectionOfTypeFile::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexObjectGenericEntity* entity = dynamic_cast<rexObjectGenericEntity*>( &obj );

	if( !entity )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	entity->m_SkipBoundSectionOfTypeFile = value.toBool();

	return rexResult( rexResult::PERFECT );
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxEntityWriteBoundSituations::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>(module.m_Serializer);

	if( !ser )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	ser->SetWriteBoundSituations(value.toBool());

	return rexResult( rexResult::PERFECT );
}
