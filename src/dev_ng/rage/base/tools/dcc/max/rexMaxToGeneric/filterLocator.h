// 
// rexMayaToGeneric/filterLocator.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REX_MAX_ROCKSTAR_FILTERLOCATOR_H__
#define __REX_MAX_ROCKSTAR_FILTERLOCATOR_H__

#include "rexbase/filter.h"
#include "rexmax/object.h"
#include "rexmaxutility/utility.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexFilterMaxLocator : public rexFilter
{
public:
	virtual bool AcceptObject(const rexObject& inputObject) const 
	{ 
		const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>(&inputObject);
		if(!p_Obj)
			return false;

		if( rexMaxUtility::IsType(p_Obj->GetMaxNode(),Class_ID(DUMMY_CLASS_ID ,0)) ||
			rexMaxUtility::IsType(p_Obj->GetMaxNode(),Class_ID(POINTHELP_CLASS_ID ,0), true) )
		{
			return true;
		}
		else
			return false;
	}
	
	virtual rexFilter* CreateNew() const { return new rexFilterMaxLocator; }
};

/////////////////////////////////////////////////////////////////////////////////////

}//End namespace rage

#endif //__REX_MAX_ROCKSTAR_FILTERLOCATOR_H__
