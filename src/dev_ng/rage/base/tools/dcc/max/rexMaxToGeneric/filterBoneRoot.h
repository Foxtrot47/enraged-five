#ifndef __REX_MAX_ROCKSTAR_FILTERBONEROOT_H__
#define __REX_MAX_ROCKSTAR_FILTERBONEROOT_H__

#include "rexbase/filter.h"
#include "rexmax/object.h"

namespace rage {

class rexFilterMaxBoneRoot : public rexFilter
{
public:
	virtual bool AcceptObject(const rexObject& inputObject) const 
	{ 
		// for RegisterModule "Skeleton" and "Animation" I seem to need to have
		// a filter set up - but the filter does nothing

		return true;
/*
		const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>(&inputObject);

		if(!p_Obj)
			return false;

		bool RetVal = false;

		INode* p_Node = p_Obj->GetMaxNode();
		TCHAR* p_Name = p_Node->GetName();

		return (rexMaxUtility::GetParent(p_Obj->GetMaxNode()) == NULL) ? true : false;
*/
	}
	
	virtual rexFilter* CreateNew() const { return new rexFilterMaxBoneRoot; }
};

} //namespace rage {

#endif //__REX_MAX_ROCKSTAR_FILTERBONEROOT_H__