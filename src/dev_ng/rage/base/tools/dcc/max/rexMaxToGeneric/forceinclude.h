#ifndef __MAX_FORCEINCLUDE_H__
#define __MAX_FORCEINCLUDE_H__

//we have to include max first because it defines some functions
//that are macros in the rage headers (Assert)

// This is required since we pulled out stlport.
// MS version of stl with 2008 sucks for debug.
#if defined(_DEBUG) && _MSC_VER < 1600
#define _HAS_ITERATOR_DEBUGGING 0
#endif

#include <max.h>
//#include <resource.h>
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <modstack.h>
#include <iskin.h>
#include <inode.h>
#include <notetrck.h>
#include <cs/keytrack.h>

#if defined(_DEBUG) && defined(_M_X64)
#include "forceinclude/win64_tooldebug.h"
#elif defined(_DEBUG)
#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG) && defined(_M_X64)
#include "forceinclude/win64_toolrelease.h"
#elif defined(NDEBUG)
#include "forceinclude/win32_toolrelease.h"
#elif defined(_M_X64)
#include "forceinclude/win64_toolbeta.h"
#else
#include "forceinclude/win32_toolbeta.h"
#endif

#endif //__MAX_FORCEINCLUDE_H__
