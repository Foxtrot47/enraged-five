#ifndef __REX_MAX_ROCKSTAR_PROPERTYBASIC_H__
#define __REX_MAX_ROCKSTAR_PROPERTYBASIC_H__

#include "rexBase/property.h"
#include "rexBase/result.h"
#include "rexBase/value.h"

namespace rage {

////////////////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMaxName : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxName; }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMaxOutputPath : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxOutputPath; }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMaxExportData : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxExportData; }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMaxInputNodes : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxInputNodes; }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMaxChannelsToWrite : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxChannelsToWrite; }
};

/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMaxExportCtrlFile : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxExportCtrlFile; }
};

/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyBoundExportPrimitivesInBvh : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundExportPrimitivesInBvh; }
};

#if HACK_GTA4
/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyBoundHasSecondSurface : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundHasSecondSurface; }
};

/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyBoundSecondSurfaceMaxHeight : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundSecondSurfaceMaxHeight; }
};

/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyBoundForceComposite : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundForceComposite; }
};

/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyBoundDisableOptimisation : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundDisableOptimisation; }
};

#endif //HACK_GTA4

} // using namespace rage { 

#endif //__REX_MAX_ROCKSTAR_PROPERTYBASIC_H__
