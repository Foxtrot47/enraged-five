#include "rexMax/object.h"
#include "rexmaxutility/utility.h"
#include "rexmaxutility/maxobj.h"
#include "rexMaxToGeneric/converterSkeleton.h"
#include "rexGeneric/objectSkeleton.h"
#include "rexrage/serializerSkeleton.h"
#include "MaxUtil/MaxUtil.h"

using namespace rage;

std::vector<Class_ID> rexConverterMaxSkeleton::m_vBoneTypes;
atMap<INode*,s32> rexConverterMaxSkeleton::m_sBoneOffsetMap;
bool rexConverterMaxSkeleton::s_bLimitToSkinBones = false;
atArray<atString> rexConverterMaxSkeleton::m_sSpecTrackBones;

static int sNumInputBones = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertySkeletonFlipConstraints::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxSkeleton* conv = dynamic_cast<rexConverterMaxSkeleton*>(module.m_Converter);

	if(!conv)
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	conv->FlipConstraints(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertySkeletonBoneTypes::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxSkeleton* conv = dynamic_cast<rexConverterMaxSkeleton*>(module.m_Converter);

	if(!conv)
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	rexConverterMaxSkeleton::SetSkeletonBoneTypes(value.toString());

	return rexResult(rexResult::PERFECT);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertySkeletonRootAtOrigin::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxSkeleton* conv = dynamic_cast<rexConverterMaxSkeleton*>(module.m_Converter);

	if(!conv)
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	conv->SetRootAtOrigin(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertySkeletonUseBoneIDs::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexSerializer* ser = dynamic_cast<rexSerializer*>( module.m_Serializer );

	if( ser )
	{
		ser->SetUseBoneIDs( value.toBool() );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertySkeletonAuthoredOrient::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxSkeleton* conv = dynamic_cast<rexConverterMaxSkeleton*>(module.m_Converter);

	if(!conv)
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	conv->SetAuthoredOrientation(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertySkeletonLimitToSkinBones::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxSkeleton::SetLimitToSkinBones(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexConverterMaxSkeleton::rexConverterMaxSkeleton():
	m_RootAtOrigin(false),
	m_FlipConstraints(false)
{
	s_bLimitToSkinBones = false;
	MaxSkeletonIterator::FlushCachedData();
	m_sSpecTrackBones.clear();
}

void rexConverterMaxSkeleton::SetSkeletonBoneTypes(const char *classIdList)
{
	std::vector<char *> classIdObjs;
	char buffer[4096];
	memcpy_s(buffer, 4096, classIdList, strlen(classIdList));
	char *currEnd = &buffer[strlen(classIdList)];
	while(currEnd)
	{
		*currEnd = '\0';
		currEnd = strrchr(buffer, '|');
		if(currEnd)
			classIdObjs.push_back(currEnd+1);
		else
			classIdObjs.push_back(buffer);
	}

	std::vector<char *>::iterator it = classIdObjs.begin();
	for(;it!=classIdObjs.end();it++)
	{
		char *partA = (*it);
		char *split = strrchr(partA, ',');
		char *partB = split+1;
		*split = '\0';
		int partAInt = atoi(partA);
		int partBInt = atoi(partB);
		m_vBoneTypes.push_back(Class_ID(partAInt,partBInt));
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxSkeleton::Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const
{
	rexResult RetVal(rexResult::PERFECT);

	atString Out("exporting skeleton: ");
	Out += defaultName;
	RetVal.AddMessage(Out);

	s32 InputObjectCount = inputObjectArray.GetCount();

	Assert(InputObjectCount == 1);

	if(InputObjectCount < 1)
		return RetVal;

	if(m_ProgressBarObjectCountCB)
		(*m_ProgressBarObjectCountCB)(InputObjectCount);

	if(m_ProgressBarObjectCurrentIndexCB)
		(*m_ProgressBarObjectCurrentIndexCB)(0);

	rexMaxObject* p_Obj = dynamic_cast<rexMaxObject*>(inputObjectArray[0]);

	if(!p_Obj)
		return RetVal;

	if(m_ProgressBarTextCB)
	{
		static char Message[1024];
		sprintf(Message,"Converting skeleton data at %s",p_Obj->GetMaxNode()->GetName());
		(*m_ProgressBarTextCB)(Message);
	}

	INode* p_Node = p_Obj->GetMaxNode();

	rexObjectGenericSkeleton* newSkeleton = new rexObjectGenericSkeleton;
	newSkeleton->SetName(rexMaxUtility::ReplaceWhitespaceCharacters(defaultName));
	newSkeleton->m_SkeletonType = "skel";
	newSkeleton->m_AuthoredOrientation = GetAuthoredOrientation();

	ISkin* p_Skin = MaxUtil::GetSkin(p_Node);

	if(p_Skin != NULL)
	{
		p_Node = MaxUtil::GetSkinRootBone(p_Skin);
	}

	sNumInputBones = 0;

	rexObjectGenericSkeleton::Bone* p_Bone = new rexObjectGenericSkeleton::Bone;

	rexMaxUtility::SetSkinBindPose(p_Node,true);

	RetVal.Combine(ConvertRec(p_Node,*p_Bone,true));

	rexMaxUtility::SetSkinBindPose(p_Node,false);

	newSkeleton->m_RootBone = p_Bone;
	outputObject = newSkeleton;

	return RetVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxSkeleton::ConvertRec(INode* p_Node,rexObjectGenericSkeleton::Bone& r_Bone,bool IsRoot) const
{
	rexResult res;
	sNumInputBones++;

	r_Bone.m_Name = rexMaxUtility::ReplaceWhitespaceCharacters(p_Node->GetName()); 
	r_Bone.m_IsJoint = true;

	Matrix34 MatTemp;
	Matrix34 MatParent;
	MatParent.Identity();

	Object* p_Constraint = NULL;
#if HACK_GTA4
	Object* p_RotationConstraint[3] = { NULL, NULL, NULL };
	Object* p_TranslationConstraint[3] = { NULL, NULL, NULL };
#endif // HACK_GTA4

#if (MAX_RELEASE < 12000)
	RefList& r_Refs = p_Node->GetRefList();
	RefListItem* p_RefItem = r_Refs.FirstItem();
	
	while(p_RefItem)
	{
		Class_ID classID = p_RefItem->maker->ClassID();

		if(p_RefItem->maker->SuperClassID() == PARAMETER_BLOCK2_CLASS_ID)
		{
			RefList& r_Refs2 = ((IParamBlock2*)p_RefItem->maker)->GetRefList();
			RefListItem* p_RefItem2 = r_Refs2.FirstItem();

			while(p_RefItem2)
			{
				Class_ID classID2 = p_RefItem2->maker->ClassID();

				if(classID2 == CONSTRAINT_CLASS_ID)
				{
					p_Constraint = (Object*)p_RefItem2->maker;
				}

				p_RefItem2 = p_RefItem2->next;
			}
		}

		p_RefItem = p_RefItem->next;
	}
#else
	DependentIterator di(p_Node);
    ReferenceMaker* p_RefMaker = NULL;
    while ( NULL!=(p_RefMaker=di.Next()) ) 
	{
            if( p_RefMaker->SuperClassID() == PARAMETER_BLOCK2_CLASS_ID )
			{
				DependentIterator di2( (IParamBlock2*)p_RefMaker );
				ReferenceMaker* p_RefMaker2 = NULL;
				while ( NULL!=(p_RefMaker2=di2.Next()) ) 
				{
					if( p_RefMaker2->ClassID() == CONSTRAINT_CLASS_ID )
					{
						p_Constraint = (Object*)p_RefMaker2;
					}
				}
			}
    }
#endif // (MAX_RELEASE < MAX_RELEASE_R12)

#if HACK_GTA4
	// The new constraint helpers (RsRotationConstraint and RsTranslationConstraint)
	// are children of the bone they are constraining; which differs from the
	// existing Constraint helper.  Here we iterate through our first level of
	// children to see if we can find any Rotation or Translation constraints.
	if ( !p_Constraint )
	{
		int numChildren = p_Node->NumberOfChildren( );
		for ( int nChild = 0; nChild < numChildren; ++nChild )
		{
			INode* pChildNode = p_Node->GetChildNode( nChild );
			if ( NULL == pChildNode )
				continue;

			Object* pObjectRef = pChildNode->GetObjectRef( );
			Class_ID classID = pObjectRef->ClassID( );
			if ( ROTATION_CONSTRAINT_CLASS_ID == classID )
			{
				// Determine axis of constraint.
				IParamBlock2* pParamBlock = (static_cast<Animatable*>(pObjectRef))->GetParamBlock(0);
				int axis = -1;
				pParamBlock->GetValue( rotation_constraint_axis, 0, axis, FOREVER );
				--axis; // MaxScript axes are 1-based
				assert( axis >= constraint_axis_x );
				assert( axis <= constraint_axis_z );
				assert( !p_Constraint ); // Ensure we aren't using both new/old constraints on bone
				assert( NULL == p_RotationConstraint[axis] ); // ensure single constraint per axis
				p_RotationConstraint[axis] = pObjectRef;
			}
			else if ( TRANSLATION_CONSTRAINT_CLASS_ID == classID )
			{
				// Determine axis of constraint.
				IParamBlock2* pParamBlock = (static_cast<Animatable*>(pObjectRef))->GetParamBlock(0);
				int axis = -1;
				pParamBlock->GetValue( translation_constraint_axis, 0, axis, FOREVER );
				--axis; // MaxScript axes are 1-based
				assert( axis >= constraint_axis_x );
				assert( axis <= constraint_axis_z );
				assert( !p_Constraint ); // Ensure we aren't using both new/old constraints on bone
				assert( NULL == p_TranslationConstraint[axis] ); // ensure single constraint per axis
				p_TranslationConstraint[axis] = pObjectRef;
			}
			else if ( CONSTRAINT_CLASS_ID == classID )
			{
				p_Constraint = pObjectRef;
			}
		}
	}
#endif // HACK_GTA4

	r_Bone.m_Channels.Reset();
	r_Bone.m_Channels.Reserve(9);
	rexObjectGenericSkeleton::Bone::ChannelInfo& r_InfoX = r_Bone.m_Channels.Append();
	rexObjectGenericSkeleton::Bone::ChannelInfo& r_InfoY = r_Bone.m_Channels.Append();
	rexObjectGenericSkeleton::Bone::ChannelInfo& r_InfoZ = r_Bone.m_Channels.Append();
	rexObjectGenericSkeleton::Bone::ChannelInfo& r_InfoTransX = r_Bone.m_Channels.Append();
	rexObjectGenericSkeleton::Bone::ChannelInfo& r_InfoTransY = r_Bone.m_Channels.Append();
	rexObjectGenericSkeleton::Bone::ChannelInfo& r_InfoTransZ = r_Bone.m_Channels.Append();
	r_InfoX.m_Name = "rotX";
	r_InfoX.m_IsLocked = false;
	r_InfoY.m_Name = "rotY";
	r_InfoY.m_IsLocked = false;
	r_InfoZ.m_Name = "rotZ";
	r_InfoZ.m_IsLocked = false;

	r_InfoTransX.m_Name = "transX";
	r_InfoTransX.m_IsLocked = false;
	r_InfoTransX.m_IsLimited = false;
	r_InfoTransY.m_Name = "transY";
	r_InfoTransY.m_IsLocked = false;
	r_InfoTransY.m_IsLimited = false;
	r_InfoTransZ.m_Name = "transZ";
	r_InfoTransZ.m_IsLocked = false;
	r_InfoTransZ.m_IsLimited = false;

	rexObjectGenericSkeleton::Bone::ChannelInfo& r_InfoScaleX = r_Bone.m_Channels.Append();
	rexObjectGenericSkeleton::Bone::ChannelInfo& r_InfoScaleY = r_Bone.m_Channels.Append();
	rexObjectGenericSkeleton::Bone::ChannelInfo& r_InfoScaleZ = r_Bone.m_Channels.Append();
	r_InfoScaleX.m_Name = "scaleX";
	r_InfoScaleX.m_IsLocked = false;
	r_InfoScaleX.m_IsLimited = false;
	r_InfoScaleY.m_Name = "scaleY";
	r_InfoScaleY.m_IsLocked = false;
	r_InfoScaleY.m_IsLimited = false;
	r_InfoScaleZ.m_Name = "scaleZ";
	r_InfoScaleZ.m_IsLocked = false;
	r_InfoScaleZ.m_IsLimited = false;

	if(p_Constraint)
	{
		IParamBlock2* p_ParamBlock = ((Animatable*)p_Constraint)->GetParamBlock(0);

		if(m_FlipConstraints)
		{
			r_InfoX.m_IsLimited = true;
			p_ParamBlock->GetValue(constraint_zmin,0,r_InfoX.m_Min,FOREVER);
			p_ParamBlock->GetValue(constraint_zmax,0,r_InfoX.m_Max,FOREVER);

			r_InfoX.m_Min *= DtoR;
			r_InfoX.m_Max *= DtoR;

			r_InfoZ.m_IsLimited = true;
			p_ParamBlock->GetValue(constraint_xmin,0,r_InfoZ.m_Min,FOREVER);
			p_ParamBlock->GetValue(constraint_xmax,0,r_InfoZ.m_Max,FOREVER);

			r_InfoZ.m_Min *= DtoR;
			r_InfoZ.m_Max *= DtoR;
		}
		else
		{
			r_InfoX.m_IsLimited = true;
			p_ParamBlock->GetValue(constraint_xmin,0,r_InfoX.m_Min,FOREVER);
			p_ParamBlock->GetValue(constraint_xmax,0,r_InfoX.m_Max,FOREVER);

			r_InfoX.m_Min *= DtoR;
			r_InfoX.m_Max *= DtoR;

			r_InfoZ.m_IsLimited = true;
			p_ParamBlock->GetValue(constraint_zmin,0,r_InfoZ.m_Min,FOREVER);
			p_ParamBlock->GetValue(constraint_zmax,0,r_InfoZ.m_Max,FOREVER);

			r_InfoZ.m_Min *= DtoR;
			r_InfoZ.m_Max *= DtoR;
		}

		r_InfoY.m_IsLimited = true;
		p_ParamBlock->GetValue(constraint_ymin,0,r_InfoY.m_Min,FOREVER);
		p_ParamBlock->GetValue(constraint_ymax,0,r_InfoY.m_Max,FOREVER);

		r_InfoY.m_Min *= DtoR;
		r_InfoY.m_Max *= DtoR;
	}
	else
	{
		r_InfoX.m_IsLimited = false;
		r_InfoY.m_IsLimited = false;
		r_InfoZ.m_IsLimited = false;
	}
#if HACK_GTA4
	// Single-axis Rotation and Translation Constraint support.
	// Rotation Constraints for x, y, z axes.
	if ( p_RotationConstraint[constraint_axis_x] || 
		p_RotationConstraint[constraint_axis_y] ||
		p_RotationConstraint[constraint_axis_z] )
	{
		// We have at least one constrained axis; ensure the others are 
		// constrained or set to zero; hence else clauses.
		r_InfoX.m_IsLimited = true;
		r_InfoY.m_IsLimited = true;
		r_InfoZ.m_IsLimited = true;

		if ( p_RotationConstraint[constraint_axis_x] ) 
		{
			IParamBlock2* p_ParamBlock = (static_cast<Animatable*>(p_RotationConstraint[constraint_axis_x]))->GetParamBlock(0);
			p_ParamBlock->GetValue( rotation_constraint_min, 0, r_InfoX.m_Min, FOREVER );
			p_ParamBlock->GetValue( rotation_constraint_max, 0, r_InfoX.m_Max, FOREVER );
		}
		else
		{
			r_InfoX.m_Min = 0.0f;
			r_InfoX.m_Max = 0.0f;
		}

		if ( p_RotationConstraint[constraint_axis_y] )
		{
			IParamBlock2* p_ParamBlock = (static_cast<Animatable*>(p_RotationConstraint[constraint_axis_y]))->GetParamBlock(0);
			p_ParamBlock->GetValue( rotation_constraint_min, 0, r_InfoY.m_Min, FOREVER );
			p_ParamBlock->GetValue( rotation_constraint_max, 0, r_InfoY.m_Max, FOREVER );
		}
		else
		{
			r_InfoY.m_Min = 0.0f;
			r_InfoY.m_Max = 0.0f;
		}

		if ( p_RotationConstraint[constraint_axis_z] )
		{
			IParamBlock2* p_ParamBlock = (static_cast<Animatable*>(p_RotationConstraint[constraint_axis_z]))->GetParamBlock(0);
			p_ParamBlock->GetValue( rotation_constraint_min, 0, r_InfoZ.m_Min, FOREVER );
			p_ParamBlock->GetValue( rotation_constraint_max, 0, r_InfoZ.m_Max, FOREVER );
		}
		else
		{
			r_InfoZ.m_Min = 0.0f;
			r_InfoZ.m_Max = 0.0f;
		}
	}

	// Translation Constraints for x, y, z axes.
	if ( p_TranslationConstraint[constraint_axis_x] || 
		p_TranslationConstraint[constraint_axis_y] || 
		p_TranslationConstraint[constraint_axis_z] ) 
	{
		// We have at least one constrained axis; ensure the others are 
		// constrained or set to zero; hence else clauses.
		r_InfoTransX.m_IsLimited = true;
		r_InfoTransY.m_IsLimited = true;
		r_InfoTransZ.m_IsLimited = true;

		if ( p_TranslationConstraint[constraint_axis_x] )
		{
			IParamBlock2* p_ParamBlock = (static_cast<Animatable*>(p_TranslationConstraint[constraint_axis_x]))->GetParamBlock(0);
			p_ParamBlock->GetValue( translation_constraint_min, 0, r_InfoTransX.m_Min, FOREVER );
			p_ParamBlock->GetValue( translation_constraint_max, 0, r_InfoTransX.m_Max, FOREVER );
		}
		else
		{
			r_InfoTransX.m_Min = 0;
			r_InfoTransX.m_Max = 0;
		}

		if ( p_TranslationConstraint[constraint_axis_y] )
		{
			IParamBlock2* p_ParamBlock = (static_cast<Animatable*>(p_TranslationConstraint[constraint_axis_y]))->GetParamBlock(0);
			p_ParamBlock->GetValue( translation_constraint_min, 0, r_InfoTransY.m_Min, FOREVER );
			p_ParamBlock->GetValue( translation_constraint_max, 0, r_InfoTransY.m_Max, FOREVER );
		}
		else
		{
			r_InfoTransY.m_Min = 0;
			r_InfoTransY.m_Max = 0;
		}

		if ( p_TranslationConstraint[constraint_axis_z] )
		{
			IParamBlock2* p_ParamBlock = (static_cast<Animatable*>(p_TranslationConstraint[constraint_axis_z]))->GetParamBlock(0);
			p_ParamBlock->GetValue( translation_constraint_min, 0, r_InfoTransZ.m_Min, FOREVER );
			p_ParamBlock->GetValue( translation_constraint_max, 0, r_InfoTransZ.m_Max, FOREVER );
		}
		else
		{
			r_InfoTransZ.m_Min = 0;
			r_InfoTransZ.m_Max = 0;
		}
	}
#endif // HACK_GTA4

	r_Bone.m_BoneID = rexMaxUtility::GetBoneID(p_Node);
	
	rexMaxUtility::GetBoneTags(p_Node, r_Bone.m_BoneTags);

	INode* p_ParentNode = rexMaxUtility::GetParent(p_Node);

	if(p_ParentNode)
	{
		rexMaxUtility::CopyMaxMatrixToRageMatrix(p_ParentNode->GetNodeTM(0),MatParent);
	}
	else
	{
		MatParent.Identity();
	}

	rexMaxUtility::GetLocalMatrix(p_Node,0,MatTemp);

	r_Bone.m_Offset = MatTemp.GetVector(3);

	if(GetAuthoredOrientation())
	{
		r_Bone.SetBoneMatrix(MatTemp);
	}
	else
	{
		MatParent.Transform3x3(r_Bone.m_Offset);
	}

	if(IsRoot && m_RootAtOrigin)
	{
		r_Bone.m_Offset = Vector3(0.0f,0.0f,0.0f);
	}

	r_Bone.m_JointOrient = MatTemp;
	r_Bone.m_JointOrient.d.Zero();

	s32 i,ChildCount = p_Node->NumberOfChildren();

	for(i=0;i<ChildCount;i++)
	{
		INode* p_ChildNode = p_Node->GetChildNode(i);

		if(rexConverterMaxSkeleton::IsValidSkelBone(p_ChildNode, &res))
		{
			rexObjectGenericSkeleton::Bone* p_ChildBone = r_Bone.AddChild(p_ChildNode->GetName(),M34_IDENTITY);
			res.Combine(ConvertRec(p_ChildNode,*p_ChildBone));
		}
	}

	return res;
}


bool rexConverterMaxSkeleton::IsValidSkelBone(INodePtr pNode, rexResult *res)
{
	MSTR keyStr("tag");
	MSTR valueStr;
	if(MaxUtil::GetUserPropString(pNode, keyStr.data(), valueStr) && 0==strcmp(valueStr.data(), "DO_NOT_EXPORT"))
	{
		if(res)
		{
			char msg[512];
			sprintf(msg, "Bone \"%s\" has \"DO_NOT_EXPORT\" tag. Exluding from Export.", pNode->GetName());
			res->AddMessageCtx(pNode->GetName(), msg);
		}
		return false;
	}
	if(m_vBoneTypes.size()>0)
	{
		char *name = pNode->GetName();
		//dont allow the footsteps node through into the skeleton
		Class_ID pId = pNode->GetObjectRef()->ClassID();
		std::vector<Class_ID>::const_iterator it = m_vBoneTypes.begin();
		for (;it!=m_vBoneTypes.end();it++)
		{
			if((*it)==pId)
				return true;
		}
		return false;
	}
	else
		return rexMaxUtility::IsValidSkelNode(pNode);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexConverterMaxSkeleton::MaxSkeletonIterator::IsValid(INode* p_MaxNode, bool limitToSkin)
{
	bool found = true;
	if(limitToSkin && m_pSkin)
	{
		found = false;
		for(int k=0;k<m_pSkin->GetNumBones();k++)
		{
			found = 0==strcmp(m_pSkin->GetBone(k)->GetName(), p_MaxNode->GetName());
			if(found)
				break;
		}
	}
	found = found && rexConverterMaxSkeleton::IsValidSkelBone(p_MaxNode);
	if(m_bLimitToSpecFileBones)
		found = found && rexConverterMaxSkeleton::SpecFileBonesContain(p_MaxNode->GetName());

	return (found);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
s32 rexConverterMaxSkeleton::MaxSkeletonIterator::GetObjectCount() const
{
	MaxSkeletonIterator It(mp_MaxRoot ? mp_MaxRoot : mp_MaxNext, m_pSkin, m_bLimitToSpecFileBones);

	s32 ObjectCount = 0;

	while(It.GetNextObject())
	{
		ObjectCount++;
	}

	return ObjectCount;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
s32 rexConverterMaxSkeleton::MaxSkeletonIterator::GetBoneOffset(ISkin* p_Skin,INode* p_BoneNode)
{
	INode* p_Bone;
	if(	m_sBoneOffsetMap.Access(p_BoneNode))
	{
		s32 cachedVal = static_cast<s32>(*(m_sBoneOffsetMap.Access(p_BoneNode)));
 		DebugPrint("Re-using bone %s: with cached value: %d\n", p_BoneNode->GetName(), cachedVal);
		return cachedVal;
	}
	
	INodePtr rootBone = MaxUtil::GetSkinRootBone(p_Skin);
	DebugPrint("Trying to find bone underneath root bone:%s \n", rootBone->GetName());
	MaxSkeletonIterator It(rootBone, p_Skin, m_bLimitToSpecFileBones);
	s32 Val = 0;

	p_Bone = It.GetNextObject();
	while(p_Bone)
	{
		char *name = p_Bone->GetName();
		if(p_Bone == p_BoneNode)
		{
			DebugPrint("Caching bone %s: with value: %d\n", name, Val);
			m_sBoneOffsetMap.Insert(p_BoneNode, Val);
			return Val;
		}

		bool validBone = It.IsValid(p_Bone, rexConverterMaxSkeleton::GetLimitToSkinBones());
		if(validBone)
			Val++;
 		DebugPrint("Iterating over Bone %s. Current Val: %d | ", name, Val);
		p_Bone = It.GetNextObject();
	}
	m_sBoneOffsetMap.Insert(p_BoneNode, 0);
	return 0;
}
