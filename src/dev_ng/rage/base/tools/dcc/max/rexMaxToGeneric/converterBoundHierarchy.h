#ifndef __REX_MAX_ROCKSTAR_CONVERTERBOUNDHIERARCHY_H__
#define __REX_MAX_ROCKSTAR_CONVERTERBOUNDHIERARCHY_H__

#include "rexmaxtogeneric/converterbasic.h"
#include "rexGeneric/objectBound.h"
#include "rexBase/property.h"



namespace rage {

/* PURPOSE:
	property to pass through surface type information for a specific bounds node.
	If there are two object called Box01 and Box02 with tarmac and gravel surface types
	respectively then the surface property string would we "Box01=tarmac:Box02=gravel"
*/
class rexPropertyBoundSurfaceType : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundSurfaceType; }
};


/* PURPOSE:
	property to turn on/off export of bounds data as bvh geom
*/
class rexPropertyMaxBoundSetGeometryBoundsExportAsBvhGeom : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxBoundSetGeometryBoundsExportAsBvhGeom; }
};

/* PURPOSE:
	property to turn on/off export of bounds data as an octree
*/
class rexPropertyMaxBoundZeroPrimitiveCentroids : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxBoundZeroPrimitiveCentroids; }
};


/* PURPOSE:
	property to turn on/off export of bounds data as a bvh grid
*/
class rexPropertyMaxBoundSetGeometryBoundsExportAsBvhGrid : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxBoundSetGeometryBoundsExportAsBvhGrid; }
};

/* PURPOSE:
	property to turn on/off export of bounds data as a composite if there are multiple bounds nodes
	in the export item
*/
class rexPropertyBoundForceExportAsComposite : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundForceExportAsComposite; }
};

/* PURPOSE:
	property to turn on/off export of bounds data as a composite even if there is only a single bounds node
*/
class rexPropertyBoundExportAsComposite : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundExportAsComposite; }
};

/* PURPOSE:
	property to turn on/off export of bounds data in world space
*/
class rexPropertyBoundExportWorldSpace : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundExportWorldSpace; }
};

/* PURPOSE:
	property to set the octree maximum height
*/
class rexPropertyBoundOctreeMaxHeight : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundOctreeMaxHeight; }
};

/* PURPOSE:
	property to set the octree maximum per cell
*/
class rexPropertyBoundOctreeMaxPerCell : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundOctreeMaxPerCell; }
};

class rexPropertyBoundUseFiltering  : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundUseFiltering; }
};

/* PURPOSE:
property to set the octree maximum per cell
*/
class rexPropertyBoundSetToNodeName : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundSetToNodeName; }
};
#if HACK_GTA4
/* PURPOSE:
property to set the collision to be relative to the parent 
*/
class rexPropertyBoundExportObjectSpace : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundExportObjectSpace; }
};

/* PURPOSE:
property to set the collision to be relative to the root most parent (used by milos)
*/
class rexPropertyBoundExportMiloSpace : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundExportMiloSpace; }
};


/* PURPOSE:
let the converter know if we want to scoop up mesh tint from CPV 
*/
class rexPropertyBoundHasMeshTint : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundHasMeshTint; }
};

/* PURPOSE:
property to set the colour template file path used for colour ID lookup 
*/
class rexPropertyBoundColourTemplate : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundColourTemplate; }
};
#endif

/* PURPOSE:
	converts max bounds data into generic rex bounds data
*/
class rexConverterMaxBoundHierarchy : public rexConverter
{
	enum BoundFlags
	{
		COLLISION_WEAPONS		= BIT(0),
		COLLISION_MOVER			= BIT(1),
		COLLISION_RIVER			= BIT(2),
		COLLISION_SPARE			= BIT(3),// was camera
		COLLISION_FOLIAGE		= BIT(4),
		COLLISION_HORSE			= BIT(5),
		COLLISION_COVER			= BIT(6),
		COLLISION_VEHICLE		= BIT(7),
		COLLISION_STAIR_SLOPE	= BIT(8),
		COLLISION_MATERIAL_ONLY	= BIT(9)
	};

public:
	rexConverterMaxBoundHierarchy();

	void SetExportAsComposite(bool ExportAsComposite) { m_ExportAsComposite = ExportAsComposite; }
	void SetForceExportAsComposite(bool ForceExportAsComposite) { m_ForceExportAsComposite = ForceExportAsComposite; }
	void SetExportAsOctree(bool ExportAsOctree) { m_ExportAsOctree = ExportAsOctree; }
#if HACK_GTA4
	void SetExportAsBVH(bool ExportAsBVH) { m_ExportAsBVH = ExportAsBVH; }
	void SetPrimitiveBoundExportPrimitivesInBvh(bool ExportPrimsInBvh)  { m_ExportPrimsInBvh = ExportPrimsInBvh; }
#endif // HACK_GTA4
	void SetExportWorldSpace(bool ExportWorldSpace) { m_ExportWorldSpace = ExportWorldSpace; }
#if HACK_GTA4
	void SetExportObjectSpace(bool ExportObjectSpace) { m_ExportObjectSpace = ExportObjectSpace; }
	void SetExportMiloSpace(bool ExportMiloSpace) { m_ExportMiloSpace = ExportMiloSpace; }
#endif // HACK_GTA4
	void SetBoundToNodeName(bool ToNodeName) { m_ToNodeName = ToNodeName; }
	void AddSurfaceType(const atString& r_ObjectName,const atString& r_SurfaceType);
	void GetSurfaceType(const atString& r_BoundName,atString& r_SurfaceType) const;
#if HACK_GTA4
	void	SetHasMeshTint( bool b )					{ m_HasMeshTint = b; }
	bool	GetHasMeshTint( ) const						{ return m_HasMeshTint; }
#endif // HACK_GTA4



protected:
	virtual rexResult Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const;
	virtual rexResult ConvertSubObject(const rexObject& object,rexObjectGenericBound*& newObject) const;
	virtual bool IsAcceptableHierarchyNode(INode* p_Node) const;
	virtual rexConverter* CreateNew() const { return new rexConverterMaxBoundHierarchy; }
	rexResult ConvertMaxNode(INode* p_Node,rexObjectGenericBound*& newObject,bool Root = false) const;

	mutable bool m_CompositeExport;
	bool m_ExportAsComposite;
	bool m_ForceExportAsComposite;
	bool m_ExportAsOctree;
#if HACK_GTA4
	bool m_ExportAsBVH;
	bool m_ExportObjectSpace;
	bool m_ExportPrimsInBvh;
	bool m_ExportMiloSpace;
#endif // HACK_GTA4
	bool m_ExportWorldSpace;
	bool m_ToNodeName;
	atMap<atString,atString> m_BoundsSurface;
#if HACK_GTA4
	// Marked for deletion along with accessors
	//atMap<atString,s32> m_BoundsRoomID;
	bool m_HasMeshTint;
#endif // HACK_GTA4
};

} //namespace rage {

#endif //__REX_MAX_ROCKSTAR_CONVERTERBOUNDHIERARCHY_H__
