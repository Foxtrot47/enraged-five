#include "MaxUtil/MaxUtil.h"
#include "rexMaxUtility/utility.h"
#include "rexMax/object.h"
#include "rexMaxUtility/maxobj.h"
#include "rexMaxToGeneric/converterFragment.h"
#include "rexGeneric/objectFragment.h"
#include "rexGeneric/objectBound.h"
#include "rageMaxLODHierarchy/HierarchyInterface.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxFragmentNewStyle::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexResult Result(rexResult::PERFECT);

	rexConverterMaxFragment* conv = dynamic_cast<rexConverterMaxFragment*>(module.m_Converter);

	if(!conv)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	conv->SetNewStyleExport(value.toBool());

	return Result;
}

////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyMaxFragmentBreakableGlassNode::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexResult RetVal(rexResult::PERFECT);

	rexConverterMaxFragment* conv = dynamic_cast<rexConverterMaxFragment*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	//Get the semicolon seperated list of node names
	atString nodeNameString(value.toString());

	//Parse the list of node names and grab the MAX nodes for each.
	int lengthPlusOne = nodeNameString.length()+1;
	char* szBuffer = new char[lengthPlusOne];
	ZeroMemory(szBuffer, sizeof(char)*lengthPlusOne);
	strncpy(szBuffer, nodeNameString.c_str(), nodeNameString.length());
	char* szNodeName = strtok(szBuffer, ";");
	while(szNodeName)
	{
		INode* p_Node = rexMaxUtility::FindNode(GetCOREInterface()->GetRootNode(), atString(szNodeName));
		if(p_Node)
			conv->AppendBreakableGlassNode(p_Node);
		else
		{
			RetVal.Combine(rexResult::ERRORS);
			RetVal.AddError("Failed to locate breakable glass node '%s' in the scene.", szNodeName);
		}
		szNodeName = strtok(NULL, ";");
	}
	delete [] szBuffer;

	return RetVal;

}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxFragment::Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const
{
	rexResult RetVal(rexResult::PERFECT);

	s32 InputObjectCount = inputObjectArray.GetCount();

	Assert(InputObjectCount == 1);

	if(InputObjectCount < 1)
	{
		RetVal.Combine(rexResult::ERRORS);
		RetVal.AddError("Rex fragment object got invalid object selection.");
		return RetVal;
	}

	rexMaxObject* p_Obj = dynamic_cast<rexMaxObject*>(inputObjectArray[0]);

	if(!p_Obj)
		return RetVal;

	INode* p_Node = p_Obj->GetMaxNode();
	ISkin* p_Skin = MaxUtil::GetSkin(p_Node);

	if(p_Skin != NULL)
	{
		p_Node = MaxUtil::GetSkinRootBone(p_Skin);
	}

	rexObjectGenericFragmentHierarchy* p_Hier = new rexObjectGenericFragmentHierarchy;
	p_Hier->SetName("");
	p_Hier->SetFullPath("");

	rexObjectGenericFragmentHierarchy* p_Root = new rexObjectGenericFragmentHierarchy;
	p_Root->SetName("root");
	p_Root->SetFullPath("/root");
	p_Hier->m_ContainedObjects.PushAndGrow(p_Root);

	
	if(m_NewStyleExport)
	{
		ConvertRecNewStyle(p_Node,p_Hier,atString(""));
	}
	else
	{
		ConvertRec(p_Node,p_Hier,atString(""));
	}

	outputObject = p_Hier;

	return RetVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxFragment::ConvertRec(INode* p_Node,rexObjectGenericFragmentHierarchy* p_Root,const atString& Path) const
{
	Object* p_Object = p_Node->GetObjectRef();

	//shouldnt add anything that's going to become a bounds really...

	if(rexMaxUtility::IsCollisionObject(p_Node))
	{
		return;
	}

	bool CreateFragment = false;
	s32 i,Count = p_Node->NumberOfChildren();

	for(i=0;i<Count;i++)
	{
		INode* p_ChildNode = p_Node->GetChildNode(i);

		if(rexMaxUtility::IsCollisionObject(p_ChildNode))
		{
			CreateFragment = true;
		}		
	}
	IFpSceneHierarchy* pRsSceneLink = GetSceneLinkInterface();
	if(pRsSceneLink)
	{
		Tab<INodePtr> children;
		pRsSceneLink->GetChildren(eLinkType_CLOTHCOLLISION, p_Node, children);
		if(children.Count()>0)
			CreateFragment = true;
	}

	atString NewPath;
	NewPath = Path;
	rexMaxUtility::GetNodeFullPath(p_Node,NewPath);

	if(CreateFragment)
	{
#if HACK_GTA4
		// This value is pulled from user defined properties on the bone node.  Need to speak with physics about 
		// a better way of doing this
		atString IsDamageString = rexMaxUtility::GetProperty(p_Node,"isdamage");

		rexObjectGenericFragmentHierarchy* p_Hier = new rexObjectGenericFragmentHierarchy(IsDamageString == "true" ? true : false);
#else
		rexObjectGenericFragmentHierarchy* p_Hier = new rexObjectGenericFragmentHierarchy;
#endif // HACK_GTA4
		p_Hier->SetName(rexMaxUtility::ReplaceWhitespaceCharacters(p_Node->GetName()));
		p_Hier->SetFullPath(NewPath);

		if( m_breakableGlassNodes.Find( p_Node ) != -1 )
		{
			p_Hier->SetBreakableGlassFlag( true );
		}

		// Set the glass type (if the node is tagged with one)
		p_Hier->m_GlassType = rexMaxUtility::GetProperty(p_Node, "glasstype");

		p_Root->m_ContainedObjects.PushAndGrow(p_Hier);
	}

	for(i=0;i<Count;i++)
	{
		INode* p_ChildNode = p_Node->GetChildNode(i);

		ConvertRec(p_ChildNode,p_Root,NewPath);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxFragment::ConvertRecNewStyle(INode* p_Node,rexObjectGenericFragmentHierarchy* p_Root,const atString& Path) const
{
}
