#ifndef __REX_MAX_ROCKSTAR_CONVERTERCLOTH_H__
#define __REX_MAX_ROCKSTAR_CONVERTERCLOTH_H__

#include "rexBase/converter.h"
#include "rexBase/filter.h"
#include "rexBase/property.h"
#include "rexGeneric/objectCloth.h"
#include "rexMax/object.h"

namespace rage
{

/* PURPOSE:
	property to set the cloth mesh associated with a cloth grouping
*/
class rexPropertyClothMesh : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyClothMesh; }
};

/* PURPOSE:
	property to set the bounds the will effect the cloth mesh in a cloth grouping
*/
class rexPropertyClothBounds : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyClothBounds; }
};

/* PURPOSE:
	converter for environmental cloth goup
*/
class rexConverterMaxEnvCloth : public rexConverter
{
public:
	virtual rexConverter* CreateNew() const  { return new rexConverterMaxEnvCloth; }

protected:
	virtual rexResult Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
};

/* PURPOSE:
	converter for character cloth group
*/
class rexConverterMaxCharCloth : public rexConverter
{
public:
	virtual rexConverter* CreateNew() const  { return new rexConverterMaxCharCloth; }

protected:
	virtual rexResult Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
};

/* PURPOSE:
*/
class rexConverterMaxClothPiece : public rexConverter
{
public:
	virtual rexConverter* CreateNew() const  { return new rexConverterMaxClothPiece; }

	rexConverterMaxClothPiece() : m_bFoundMesh(false){};

	void	SetMesh(const atString& meshName) { m_MeshName = meshName; }
	void	AppendBound(const atString& boundName) { m_BoundNames.PushAndGrow( boundName); }

protected:
	virtual rexResult Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexResult ConvertSubObject(const rexObject& object,rexObject*& newObject) const;
	virtual rexResult ConvertSubObject(INode* p_Node,rexObject*& newObject) const;
	virtual bool IsAcceptableHierarchyNode(INode* p_Node) const;

private:
	atString			m_MeshName;
	atArray<atString>	m_BoundNames;

	mutable bool		m_bFoundMesh;
	mutable atString	m_PieceName;
	mutable rexObjectGenericClothMeshInfo *mp_ClothInfo;
};


class rexFilterCloth : public rexFilter
{
public:

	virtual bool AcceptObject(const rexObject& inputObject) const 
	{ 
		const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>(&inputObject);

		if(!p_Obj)
			return false;

		if( rexMaxUtility::IsType(p_Obj->GetMaxNode(),Class_ID(TRIOBJ_CLASS_ID,0)) ||
			(p_Obj->GetMaxNode()->GetObjectRef()->ClassID() == COLLISION_BOX_CLASS_ID) ||
			(p_Obj->GetMaxNode()->GetObjectRef()->ClassID() == COLLISION_CAPSULE_CLASS_ID) ||
			(p_Obj->GetMaxNode()->GetObjectRef()->ClassID() == COLLISION_SPHERE_CLASS_ID) ||
			(p_Obj->GetMaxNode()->GetObjectRef()->ClassID() == COLLISION_MESH_CLASS_ID) ||
			(p_Obj->GetMaxNode()->GetObjectRef()->ClassID() == COLLISION_CYLINDER_CLASS_ID) )
			return true;
		else
			return false;
	}

	virtual rexFilter* CreateNew() const { return new rexFilterCloth; }
};

}//end namespace rage

#endif //__REX_MAX_ROCKSTAR_CONVERTERCLOTH_H__

