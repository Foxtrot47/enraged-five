#ifndef __REX_MAX_ROCKSTAR_CONVERTERBASIC_H__
#define __REX_MAX_ROCKSTAR_CONVERTERBASIC_H__

#include "rexBase/converter.h"
#include "rexGeneric/objectHierarchy.h"

namespace rage {

/* PURPOSE:
	base class for some of the max converters to manage hierarchies automatically
*/
class rexConverterMaxHierarchy : public rexConverter
{
public:
	virtual rexResult Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const;
	virtual rexResult ConvertSubObject(const rexObject& object,rexObject*& newObject) const = 0;
	virtual bool IsAcceptableHierarchyNode(INode* p_Node) const = 0;
	virtual rexObjectHierarchy* CreateHierarchyNode() const = 0;


	//////////////////////////////////////////////////////////////////////////////////////////////////////
	class MaxIterator
	{
	public:
		MaxIterator(INode* p_MaxRoot): mp_MaxNext(p_MaxRoot),mp_MaxRoot(NULL) {}
		virtual ~MaxIterator() {}

		virtual INode* GetNextObject();
		virtual s32 GetObjectCount() const;
		INodePtr GetValidChildRec(const INodePtr p_CurrRoot, int startIndex=0);

	protected:
		virtual bool IsValid(INode* p_MaxNode, bool limitToSkin=false) = 0;
		virtual MaxIterator* Create(INode* p_MaxRoot) const = 0;

		INode* mp_MaxRoot;
		INode* mp_MaxNext;
		atArray<s32> m_CurrentChild;
	};
};

} //namespace rage {

#endif //__REX_MAX_ROCKSTAR_CONVERTERBASIC_H__
