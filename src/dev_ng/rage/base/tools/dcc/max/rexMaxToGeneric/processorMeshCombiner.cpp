#include "rexBase/module.h"
#include "rexBase/object.h"
#include "rexGeneric/objectBound.h"
#include "rexGeneric/objectMesh.h"
#include "rexMaxToGeneric/processorMeshCombiner.h"
#include "vector/vector3.h"
#include "phbound/boundgeom.h"

///////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

//////////////////////////////////////////////////////////////////////////////////////////////////////

struct rexMaxPoly
{
	s32 MeshIdx;
#if HACK_GTA4
	atArray<s32> PolyIdxList;
#else 
	s32 PolyIdx;
#endif // HACK_GTA4
	Vector3 Pos;
	u32 flags;
};

#if HACK_GTA4  
// Surface normal calculation
static void GetTriNormal(const Vector3& a,const Vector3& b,const Vector3& c, Vector3& norm)
{
	Vector3 AB, BC;
	AB.Subtract(a,b);
	BC.Subtract(c,b);
	AB.Normalize();
	BC.Normalize();
	norm.Cross(AB,BC);
	norm.Normalize();
}
#endif // HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////

class rexMaxBinaryCell
{
public:

	enum
	{
		NUM_CHILDREN = 2
	};

	enum
	{
		CELL_AXIS_X = 0,
		CELL_AXIS_Y,
		CELL_AXIS_Z
	};

	rexMaxBinaryCell()
	{
		for( int a = 0; a < NUM_CHILDREN; a++ )
			m_Children[a] = NULL;
		m_Center.Zero();
		m_Dimensions.Zero();
		m_Count = 0;
	}

	~rexMaxBinaryCell()
	{
		for( int a = 0; a < NUM_CHILDREN; a++ )
			delete m_Children[a];
	}

	void Init( const Vector3& center, const Vector3& dimensions, float leafCellMaxSideLength )
	{
		m_Dimensions = dimensions;
		m_Center = center;

		float absX = fabsf( dimensions.x ), absY = fabsf( dimensions.y ), absZ = fabsf( dimensions.z );

		if(( absX > leafCellMaxSideLength ) || ( absY > leafCellMaxSideLength ) || ( absZ > leafCellMaxSideLength ))
		{
			Vector3 newCellSize(dimensions);

			s32 axis = CELL_AXIS_X;

			if((absY > absX) && (absY > absZ))
				axis = CELL_AXIS_Y;
			if((absZ > absX) && (absZ > absY))
				axis = CELL_AXIS_Z;

			newCellSize[axis] /= 2;

			for( int a = 0; a < NUM_CHILDREN; a++ )
			{
				m_Children[a] = new rexMaxBinaryCell();

				Vector3 newCenter(center);

				switch(a)
				{
				case 0:
					newCenter[axis] = center[axis] + (newCellSize[axis] / 2.0f);
					break;
				case 1:
					newCenter[axis] = center[axis] - (newCellSize[axis] / 2.0f);
					break;
				}

				m_Children[a]->Init( newCenter, newCellSize, leafCellMaxSideLength);
			}
		}
	}

	float GetDistance(const rexMaxPoly& r_Poly )
	{
		Vector3 Diff = m_Center - r_Poly.Pos;

		return Diff.Mag();
	}

	bool IsInside(const rexMaxPoly& r_Poly )
	{
		Vector3 Min = m_Center - (m_Dimensions / 2.0f);
		Vector3 Max = m_Center + (m_Dimensions / 2.0f);

		for(int a = 0;a < 3;a++)
		{
			if(r_Poly.Pos[a] > Max[a])
				return false;

			if(r_Poly.Pos[a] < Min[a])
				return false;
		}

		return true;
	}

	bool AddPrimitive(const rexMaxPoly& r_Poly )
	{
		m_Count++;

		if(m_Children[0] == NULL)
		{
			m_MeshInfo.PushAndGrow(r_Poly);
		}
		else
		{
			bool Found = false;

			for( int a = 0; a < NUM_CHILDREN; a++ )
			{
				if(m_Children[a]->IsInside(r_Poly))
				{
					Found = true;
					m_Children[a]->AddPrimitive(r_Poly);
				}
			}

			if(!Found)
			{
				//TODO: figure out a better solution to this problem

				//not quite sure how we get here - i think it might be rounding errors
				//causing the boxes to get slightly smaller
				//but we need to choose one side to go to...

				float DistA = m_Children[0]->GetDistance(r_Poly);
				float DistB = m_Children[1]->GetDistance(r_Poly);

				if(DistA > DistB)
					m_Children[1]->AddPrimitive(r_Poly);
				else
					m_Children[0]->AddPrimitive(r_Poly);
			}
		}

		return true;
	}

	void GetCellList(s32 maxPrimCountToCombine,atArray<rexMaxBinaryCell*>& r_CellList)
	{
		if((m_Count <= maxPrimCountToCombine) || (m_Children[0] == NULL) || (m_Children[1] == NULL))
		{
			if(m_Count > 0)
				r_CellList.PushAndGrow(this);
		}
		else
		{
			for(s32 a=0;a<NUM_CHILDREN;a++)
				m_Children[a]->GetCellList(maxPrimCountToCombine,r_CellList);
		}
	}

	void GetPolyList(atArray<rexMaxPoly>& r_PolyList)
	{
		s32 a,Count = m_MeshInfo.GetCount();

		for(a=0;a<Count;a++)
		{
#if HACK_GTA4
			r_PolyList.PushAndGrow(m_MeshInfo[a],r_PolyList.GetCount() == 0 ? Count : r_PolyList.GetCount());
#else
			r_PolyList.PushAndGrow(m_MeshInfo[a]);
#endif // HACK_GTA4
		}

		for(a=0;a<NUM_CHILDREN;a++)
		{
			if(m_Children[a])
				m_Children[a]->GetPolyList(r_PolyList);
		}
	}

	rexMaxBinaryCell* m_Children[NUM_CHILDREN];
	Vector3 m_Center, m_Dimensions;
	atArray<rexMaxPoly> m_MeshInfo;
	s32 m_Count;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////

class rexMaxBinaryTree : public rexMaxBinaryCell
{
public:
	rexMaxBinaryTree(const rexProcessorMaxBoundCombiner* p_Combiner): m_TotalPolyCountIn(0),m_Combiner(p_Combiner) {}

#if HACK_GTA4 
	// Look for polys that share an edge and remove ones where the polys point towards each other (given an acceptable value)
	// Most of the differences in this file relate to using this polyset.  It seems it would be more sensible to pick up this
	// issue in the source max file rather than having to compute this every time an artist exports.  It also seems a little 
	// dangerous.  Marked for review (LPXO)
	void CreatePolySet( rexObjectGenericMesh& mesh, atArray<rexMaxPoly>& r_PolySet)
	{
		s32 a,b,c,Count = mesh.m_Primitives.GetCount();
		atArray<bool> Used;
		Used.Resize(Count);

		for(a=0;a<Count;a++)
			Used[a] = false; 

		m_TotalPolyCountIn += Count;

		for(a = 0;a < Count;a++)
		{
			if(Used[a])
				continue;

			Used[a] = true;

			rexMaxPoly Poly;

			Poly.MeshIdx = m_Meshes.GetCount() - 1;
			Poly.PolyIdxList.PushAndGrow(a);

			Poly.flags = mesh.m_surfaceFlags;

			for(b=a;b<Count;b++)
			{
				if(Used[b])
					continue;

				//does this polygon share an edge? (assume triangles)
				
				s32 matchVertCount = 0;
				Assert(mesh.m_Primitives[a].m_Adjuncts.GetCount() == 3);

				s32 vertA = mesh.m_Primitives[b].m_Adjuncts[0].m_VertexIndex;
				s32 vertB = mesh.m_Primitives[b].m_Adjuncts[1].m_VertexIndex;
				s32 vertC = mesh.m_Primitives[b].m_Adjuncts[2].m_VertexIndex;
				s32 NonMatchVert = 0;

				for(c=0;c<3;c++)
				{
					s32 vertCheck = mesh.m_Primitives[a].m_Adjuncts[c].m_VertexIndex;

					if((vertCheck == vertA) || (vertCheck == vertB) || (vertCheck == vertC))
					{
						matchVertCount++;
					}
					else NonMatchVert =  vertCheck;
				}
				
				if(matchVertCount < 2)
					continue;

				if(matchVertCount == 3)
				{
					Used[b] = true;
					continue;
				}

				//is the angle of the two face normals more than 180 degrees?

				Vector3 A1 = mesh.m_Vertices[mesh.m_Primitives[a].m_Adjuncts[2].m_VertexIndex].m_Position - mesh.m_Vertices[mesh.m_Primitives[a].m_Adjuncts[0].m_VertexIndex].m_Position;
				Vector3 A2 = mesh.m_Vertices[mesh.m_Primitives[a].m_Adjuncts[2].m_VertexIndex].m_Position - mesh.m_Vertices[mesh.m_Primitives[a].m_Adjuncts[1].m_VertexIndex].m_Position;
				A1.Normalize();
				A2.Normalize();

				Vector3 B1 = mesh.m_Vertices[mesh.m_Primitives[b].m_Adjuncts[2].m_VertexIndex].m_Position - mesh.m_Vertices[mesh.m_Primitives[b].m_Adjuncts[0].m_VertexIndex].m_Position;
				Vector3 B2 = mesh.m_Vertices[mesh.m_Primitives[b].m_Adjuncts[2].m_VertexIndex].m_Position - mesh.m_Vertices[mesh.m_Primitives[b].m_Adjuncts[1].m_VertexIndex].m_Position;
				B1.Normalize();
				B2.Normalize();

				Vector3 AN;
				Vector3 BN;

				AN.Cross(A1,A2);
				AN.Normalize();
				BN.Cross(B1,B2);
				BN.Normalize();

				float Angle = Acosf(AN.Dot(BN));
				float K = BN.Dot(mesh.m_Vertices[mesh.m_Primitives[b].m_Adjuncts[0].m_VertexIndex].m_Position);
				float Dist = BN.Dot(mesh.m_Vertices[NonMatchVert].m_Position) - K;

				if(Dist < 0.0f)
					Angle = (2 * PI) - Angle;

				if(Angle > 3.5f) //experimental hard coded value...
				{
					Used[b] = true;
					Poly.PolyIdxList.PushAndGrow(b);
				}
				
				//Used[b] = true;
				//Poly.PolyIdxList.PushAndGrow(b);
			}

			r_PolySet.PushAndGrow(Poly,Count);
		}
	}
#endif // HACK_GTA4

	bool AddMesh( rexObjectGenericMesh& mesh )
	{
		m_Meshes.PushAndGrow(&mesh);
		
#if HACK_GTA4
		atArray<rexMaxPoly> PolySet;

		CreatePolySet(mesh,PolySet);

		s32 a,b,c,PolyCount,Count = PolySet.GetCount();

		for(a=0;a<Count;a++)
		{
			PolyCount = PolySet[a].PolyIdxList.GetCount();
			PolySet[a].Pos = Vector3(0,0,0);

			s32 TotalAdjCount = 0;

			for(b = 0;b<PolyCount;b++)
			{
				s32 AdjCount = mesh.m_Primitives[a].m_Adjuncts.GetCount();
				TotalAdjCount += AdjCount;

				for(c = 0;c < AdjCount;c++)
				{
					PolySet[a].Pos.Add(mesh.m_Vertices[mesh.m_Primitives[PolySet[a].PolyIdxList[b]].m_Adjuncts[c].m_VertexIndex].m_Position);
				}
			}

			PolySet[a].Pos /= (float)TotalAdjCount;

			AddPrimitive(PolySet[a]);
		}
#else
	
		rexMaxPoly Poly;
		Poly.MeshIdx = m_Meshes.GetCount() - 1;

		s32 a,b,Count = mesh.m_Primitives.GetCount();

		m_TotalPolyCountIn += Count;

		for(a = 0;a < Count;a++)
		{
			s32 AdjCount = mesh.m_Primitives[a].m_Adjuncts.GetCount();
			Poly.Pos = Vector3(0,0,0);

			for(b = 0;b < AdjCount;b++)
			{
				Poly.Pos.Add(mesh.m_Vertices[mesh.m_Primitives[a].m_Adjuncts[b].m_VertexIndex].m_Position);
			}

			Poly.Pos /= AdjCount;
			Poly.PolyIdx = a;

			AddPrimitive(Poly);
		}
#endif // HACK_GTA4

		return true;
	}

	static int SortPolyList(const rexMaxPoly* p_A,const rexMaxPoly* p_B)
	{
		int diff = (s32)p_A->flags - (s32)p_B->flags;

		if(diff != 0)
			return diff;

		return p_B->MeshIdx - p_A->MeshIdx;
	}
#if HACK_GTA4
	rexObject* CreateMeshFromPolyList(rexMaxBinaryCell* p_Cell,s32& Polycount,atArray<rexMaxPoly>& PolyList)
	{
		//atArray<rexMaxPoly> PolyList;
		//p_Cell->GetPolyList(PolyList);

		s32 a,b,c,d,PolyIdxCount,AdjCount,Count = PolyList.GetCount();
		s32 OutPolyCount = 0;

		for(a=0;a<Count;a++)
			OutPolyCount += PolyList[a].PolyIdxList.GetCount();

		if(Count == 0)
			return NULL;

		Polycount += OutPolyCount;
		
		u32 flags =  PolyList[0].flags;	

		Assert(Count == p_Cell->m_Count);

		rexObjectGenericBound* p_NewBound = new rexObjectGenericBound;
		p_NewBound->m_Type = phBound::GEOMETRY;
		rexObjectGenericMesh* p_NewMesh = p_NewBound;

		Vector3 boundBoxMin = Vector3(-p_Cell->m_Dimensions.x / 2.0f, -p_Cell->m_Dimensions.y / 2.0f, -p_Cell->m_Dimensions.z / 2.0f);
		Vector3 boundBoxMax = Vector3(p_Cell->m_Dimensions.x / 2.0f, p_Cell->m_Dimensions.y / 2.0f, p_Cell->m_Dimensions.z / 2.0f);

		p_NewMesh->m_surfaceFlags = flags;

		p_NewMesh->m_Matrix.Identity();
		p_NewMesh->m_ParentBoneMatrix.Identity();
		p_NewMesh->m_Center = p_Cell->m_Center;
		p_NewMesh->m_BoundBoxMax = boundBoxMax;
		p_NewMesh->m_BoundBoxMin = boundBoxMin;
		p_NewMesh->m_PivotPoint = p_Cell->m_Center;
		p_NewMesh->m_IsSkinned = false;
		p_NewMesh->m_IsValid = true;
		p_NewMesh->m_ChildOfLevelInstanceNode = false;

		s32 PolyOffset = 0;

		atArray<s32,0,unsigned int> TempMaterialOld2New;
		atArray<s32,0,unsigned int> TempVertexOld2New;
		atArray<s32,0,unsigned int> TempNormalOld2New;
		atArray<s32,0,unsigned int> TempColourOld2New;

		PolyList.QSort(0,Count,SortPolyList);

		p_NewMesh->m_Primitives.Reset();
		p_NewMesh->m_Primitives.Reserve(OutPolyCount);

		p_NewMesh->m_Materials.Reset();
		p_NewMesh->m_Materials.Reserve(32);
		p_NewMesh->m_Vertices.Reset();
		p_NewMesh->m_Vertices.Reserve(1024);
		p_NewMesh->m_Normals.Reset();
		p_NewMesh->m_Normals.Reserve(1024);
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		//p_NewMesh->m_colourTemplate = m_Meshes[PolyList[0].MeshIdx]->m_colourTemplate;
		p_NewMesh->m_colourTemplate.Reset();
		p_NewMesh->m_colourTemplate.Reserve(32);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS

		s32 NumMats = m_Meshes[PolyList[0].MeshIdx]->m_Materials.GetCount();

		TempMaterialOld2New.Reset();
		TempMaterialOld2New.Resize(NumMats);

		for(b=0;b<NumMats;b++)
			TempMaterialOld2New[b] = -1;

		s32 NumVerts = m_Meshes[PolyList[0].MeshIdx]->m_Vertices.GetCount();

		TempVertexOld2New.Reset();
		TempVertexOld2New.Resize(NumVerts);

		for(b=0;b<NumVerts;b++)
			TempVertexOld2New[b] = -1;

		s32 NumNorms = m_Meshes[PolyList[0].MeshIdx]->m_Normals.GetCount();
 
		TempNormalOld2New.Reset();
		TempNormalOld2New.Resize(NumNorms);

		for(b=0;b<NumNorms;b++)
			TempNormalOld2New[b] = -1;

		s32 NumCols = m_Meshes[PolyList[0].MeshIdx]->m_colourTemplate.GetCount();

		TempColourOld2New.Reset();
		TempColourOld2New.Resize(NumCols);

		for(b=0;b<NumCols;b++)
			TempColourOld2New[b] = -1;

		for(a=0;a<Count;a++)
		{
			if(PolyList[a].MeshIdx != PolyList[PolyOffset].MeshIdx)
			{
				for(b=PolyOffset;b<a;b++)
				{
					PolyIdxCount = PolyList[b].PolyIdxList.GetCount();

					for(d=0;d<PolyIdxCount;d++)
					{
						rexObjectGenericMesh::PrimitiveInfo NewPrimitive = m_Meshes[PolyList[b].MeshIdx]->m_Primitives[PolyList[b].PolyIdxList[d]];

						NewPrimitive.m_MaterialIndex = TempMaterialOld2New[NewPrimitive.m_MaterialIndex];

						AdjCount = NewPrimitive.m_Adjuncts.GetCount();

						for(c=0;c<AdjCount;c++)
						{
							NewPrimitive.m_Adjuncts[c].m_VertexIndex = TempVertexOld2New[NewPrimitive.m_Adjuncts[c].m_VertexIndex];
							NewPrimitive.m_Adjuncts[c].m_NormalIndex = TempNormalOld2New[NewPrimitive.m_Adjuncts[c].m_NormalIndex];
						}

						p_NewMesh->m_Primitives.Append() = NewPrimitive;
					}
				}

				NumMats = m_Meshes[PolyList[a].MeshIdx]->m_Materials.GetCount();

				TempMaterialOld2New.Reset();
				TempMaterialOld2New.Resize(NumMats);

				for(b=0;b<NumMats;b++)
					TempMaterialOld2New[b] = -1;

				NumVerts = m_Meshes[PolyList[a].MeshIdx]->m_Vertices.GetCount();

				TempVertexOld2New.Reset();
				TempVertexOld2New.Resize(NumVerts);

				for(b=0;b<NumVerts;b++)
					TempVertexOld2New[b] = -1;

				NumNorms = m_Meshes[PolyList[a].MeshIdx]->m_Normals.GetCount();

				TempNormalOld2New.Reset();
				TempNormalOld2New.Resize(NumNorms);

				for(b=0;b<NumNorms;b++)
					TempNormalOld2New[b] = -1;

				PolyOffset = a;
			}

			rexObjectGenericMesh& r_Mesh = *m_Meshes[PolyList[a].MeshIdx];
			PolyIdxCount = PolyList[a].PolyIdxList.GetCount();
				
			for(d=0;d<PolyIdxCount;d++)
			{
				rexObjectGenericMesh::PrimitiveInfo& r_Primitive = r_Mesh.m_Primitives[PolyList[a].PolyIdxList[d]];
				atArray<rexObjectGenericMesh::AdjunctInfo>& r_Adjuncts = r_Primitive.m_Adjuncts;

				if(TempMaterialOld2New[r_Primitive.m_MaterialIndex] == -1)
				{
					s32 MatCount = p_NewMesh->m_Materials.GetCount();
					s32 FoundIdx = -1;

					for(b=0;b<MatCount;b++)
					{
						if(p_NewMesh->m_Materials[b].m_Name == r_Mesh.m_Materials[r_Primitive.m_MaterialIndex].m_Name)
						{
							FoundIdx = b;
							break;
						}
					}

					if(FoundIdx == -1)
					{
						if(p_NewMesh->m_Materials.GetCount() < 255)
						{
							TempMaterialOld2New[r_Primitive.m_MaterialIndex] = p_NewMesh->m_Materials.GetCount();
							p_NewMesh->m_Materials.Grow(p_NewMesh->m_Materials.GetCount()) = r_Mesh.m_Materials[r_Primitive.m_MaterialIndex];
							
						}
						else
						{
							TempMaterialOld2New[r_Primitive.m_MaterialIndex] = 0;
						}
					}
					else
					{
						TempMaterialOld2New[r_Primitive.m_MaterialIndex] = FoundIdx;
					}
					
				}

				AdjCount = r_Adjuncts.GetCount();

				for(b=0;b<AdjCount;b++)
				{
					if(TempVertexOld2New[r_Adjuncts[b].m_VertexIndex] == -1)
					{
						TempVertexOld2New[r_Adjuncts[b].m_VertexIndex] = p_NewMesh->m_Vertices.GetCount();

						p_NewMesh->m_Vertices.Grow(p_NewMesh->m_Vertices.GetCount()) = r_Mesh.m_Vertices[r_Adjuncts[b].m_VertexIndex];
					}

					if(TempNormalOld2New[r_Adjuncts[b].m_NormalIndex] == -1)
					{
						TempNormalOld2New[r_Adjuncts[b].m_NormalIndex] = p_NewMesh->m_Normals.GetCount();

						p_NewMesh->m_Normals.Grow(p_NewMesh->m_Normals.GetCount()) = r_Mesh.m_Normals[r_Adjuncts[b].m_NormalIndex];
					}
				}
			}
		}

		for(b=PolyOffset;b<Count;b++)
		{
			PolyIdxCount = PolyList[b].PolyIdxList.GetCount();

			for(d=0;d<PolyIdxCount;d++)
			{
				rexObjectGenericMesh::PrimitiveInfo NewPrimitive = m_Meshes[PolyList[b].MeshIdx]->m_Primitives[PolyList[b].PolyIdxList[d]];

				NewPrimitive.m_MaterialIndex = TempMaterialOld2New[NewPrimitive.m_MaterialIndex];

				AdjCount = NewPrimitive.m_Adjuncts.GetCount();

				for(c=0;c<AdjCount;c++)
				{
					NewPrimitive.m_Adjuncts[c].m_VertexIndex = TempVertexOld2New[NewPrimitive.m_Adjuncts[c].m_VertexIndex];
					NewPrimitive.m_Adjuncts[c].m_NormalIndex = TempNormalOld2New[NewPrimitive.m_Adjuncts[c].m_NormalIndex];
				}

				p_NewMesh->m_Primitives.Append() = NewPrimitive;
			}
		}
		
		return p_NewMesh;
	}

	//copied from rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundGeometry( phBoundGeometry *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const

	void convertGenericToExportedBound(phBoundGeometry *&bnd, const rexObjectGenericBound *inputBound)
	{
		rexResult retval(rexResult::PERFECT);

		int numverts = inputBound->m_Vertices.GetCount(),
			nummaterials = 1,
			numpolys = inputBound->m_Primitives.GetCount(),
			i, j;

		int generatedPolyCount = 0;
		float conversionFactor = rexSerializer::GetUnitTypeLinearConversionFactor();
		for(i = 0; i < numpolys; i++)
		{
			// convenience variable
			const rexObjectGenericMesh::PrimitiveInfo &prim = inputBound->m_Primitives[i];

			int polyverts = prim.m_Adjuncts.GetCount();
			int prims = polyverts - 2;
			bool boundQuad = inputBound->primitiveIsQuad(i);
			if( boundQuad )
				generatedPolyCount += 1;
			else if( prims > 0 )
				generatedPolyCount += prims;
		}

		//	Displayf( "Name %s", (const char*) inputBound->m_FullPath );
		//	Displayf("%f %f %f %f", inputBound->m_ParentBoneMatrix.a.x, inputBound->m_ParentBoneMatrix.a.y, inputBound->m_ParentBoneMatrix.a.z);
		//	Displayf("%f %f %f %f", inputBound->m_ParentBoneMatrix.b.x, inputBound->m_ParentBoneMatrix.b.y, inputBound->m_ParentBoneMatrix.b.z);
		//	Displayf("%f %f %f %f", inputBound->m_ParentBoneMatrix.c.x, inputBound->m_ParentBoneMatrix.c.y, inputBound->m_ParentBoneMatrix.c.z);
		//	Displayf("%f %f %f %f", inputBound->m_ParentBoneMatrix.d.x, inputBound->m_ParentBoneMatrix.d.y, inputBound->m_ParentBoneMatrix.d.z);

		bnd = new phBoundGeometry();

		const int numPerVertAttribs=0;
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
	#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		bnd->Init(numverts, numPerVertAttribs, 1, 0, generatedPolyCount, 1);
	#else
		bnd->Init(numverts, numPerVertAttribs, 1, generatedPolyCount, 1);
	#endif
#else
	#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		bnd->Init(numverts, numPerVertAttribs, 1, 0, generatedPolyCount);
	#else
		bnd->Init(numverts, numPerVertAttribs, 1, generatedPolyCount);
	#endif
#endif
		// set vertices
		for(i = 0; i < numverts; i++)
		{
			Vector3 v;
			inputBound->m_Matrix.Transform( inputBound->m_Vertices[i].m_Position, v );
			inputBound->m_ParentBoneMatrix.UnTransform( v );
			//		Displayf( "Vertex %d: (%f, %f, %f)", i, v.x, v.y, v.z );
			v.Scale(conversionFactor);
			bnd->SetVertex(i, RCC_VEC3V(v));
		}

		// set extra vertices to <0,0,0>, so that NAN exceptions aren't raised in Fourtified functions
		Vector3 zeroVector(0.0f,0.0f,0.0f);
		for(i = numverts; i < Fourtify(numverts); i++)
			bnd->SetVertex(i, RCC_VEC3V(zeroVector));

		// set materials
//		for(i = 0; i < nummaterials; i++)
//		{
//			phMaterialMgr::Id mtl = materials[i];
//
//			bnd->SetMaterialId(i, mtl);
//		}
		phPolygon poly;
		int matIndex, polyVertCount, polyTriCount, physPolyCount = 0;
		u32 polyStartVert, i0, i1, i2, i3;

		// set polygons
		for(i = 0; i < numpolys; i++)
		{
			// convenience variable
			const rexObjectGenericMesh::PrimitiveInfo &prim = inputBound->m_Primitives[i];

			// set the physics material
			// material index of the primitive is -1 if the
			// maya material failed to be converted to a phMaterial.
			// in this case, use the default material (with index 0)
			// otherwise, look up the appropriate material in the maya material->phMaterial mapping
			matIndex = 0;

			polyVertCount = prim.m_Adjuncts.GetCount();

			if( polyVertCount )
			{
				polyStartVert = (u32)prim.m_Adjuncts[0].m_VertexIndex;

				if( !(inputBound->primitiveIsQuad(i)) )
				{
					// convert this polygon into triangles
					polyTriCount = polyVertCount - 2;
					for(j = 1; j <= polyTriCount; j++)
					{
						i0 = (u32)prim.m_Adjuncts[j].m_VertexIndex;
						i1 = (u32)prim.m_Adjuncts[j+1].m_VertexIndex;

						Vector3 polyNormal;
						Vector3 polyCenter;
						polyCenter.Add( inputBound->m_Vertices[ i0 ].m_Position, inputBound->m_Vertices[ i1 ].m_Position );
						polyCenter.Add( inputBound->m_Vertices[ polyStartVert ].m_Position );
						polyCenter.Scale( 1.0f / 3.0f );
						GetTriNormal( inputBound->m_Vertices[ i0 ].m_Position, polyCenter, inputBound->m_Vertices[ i1 ].m_Position, polyNormal );
						float dotProduct = polyNormal.Dot( prim.m_FaceNormal );
						if( dotProduct < 0.0f )
							poly.InitTriangle(polyStartVert, i1, i0, bnd->GetVertexPointer()[polyStartVert], bnd->GetVertexPointer()[i1], bnd->GetVertexPointer()[i0]);					
						else
							poly.InitTriangle(i0, i1, polyStartVert, bnd->GetVertexPointer()[i0], bnd->GetVertexPointer()[i1], bnd->GetVertexPointer()[polyStartVert]);
#if POLY_MAX_VERTICES == 4
						poly.SetMaterialIndex((u8) matIndex );
						Vector3 vecTemp = VEC3V_TO_VECTOR3(bnd->GetPolygonUnitNormal(physPolyCount));//, bnd->GetVertexPointer());
#else
						bnd->SetPolygonMaterialIndex(physPolyCount, matIndex);
						Vector3 vecTemp = polyNormal;
#endif // POLY_MAX_VERTICES == 4

						
						if(vecTemp.Mag2() < 0.9f)
							//if(poly.GetNormal().Mag2() < 0.9f)
						{
							// TODO: write a warning to log (polygon [index] has non-unit normal, determine type of poly by using IsTriangle()) - wil
							retval.Set(rexResult::WARNINGS, rexResult::CONVERSION);
						}
						else
							bnd->SetPolygon(physPolyCount++, poly);
					}
				}
				else
				{
#if POLY_MAX_VERTICES == 3
					retval.Combine(rexResult::ERRORS);
					retval.AddError("Attempt to export a quad.  Not supported");
#else 
					// the last index for a quad CANNOT be zero (that's how we
					// determine if a poly is a tri or a quad) so we have to
					// rotate the vertices.
					if(0 == polyStartVert)
					{
						i0 = polyStartVert;
						i1 = (u32)prim.m_Adjuncts[1].m_VertexIndex;
						i2 = (u32)prim.m_Adjuncts[2].m_VertexIndex;
						i3 = (u32)prim.m_Adjuncts[3].m_VertexIndex;
					}
					else
					{
						i0 = (u32)prim.m_Adjuncts[1].m_VertexIndex;
						i1 = (u32)prim.m_Adjuncts[2].m_VertexIndex;
						i2 = (u32)prim.m_Adjuncts[3].m_VertexIndex;
						i3 = polyStartVert;
					}

					Vector3 polyNormal;
					Vector3 polyCenter;
					polyCenter.Add( inputBound->m_Vertices[ i0 ].m_Position, inputBound->m_Vertices[ i1 ].m_Position );
					polyCenter.Add( inputBound->m_Vertices[ i2 ].m_Position );
					polyCenter.Add( inputBound->m_Vertices[ i3 ].m_Position );
					polyCenter.Scale( 0.25f );
					GetTriNormal( inputBound->m_Vertices[ i0 ].m_Position, polyCenter, inputBound->m_Vertices[ i1 ].m_Position, polyNormal );
					float dotProduct = polyNormal.Dot( prim.m_FaceNormal );
					if( dotProduct < 0.0f )
						poly.InitQuad(i2, i1, i0, i3, bnd->GetVertexPointer()[i2], bnd->GetVertexPointer()[i1], bnd->GetVertexPointer()[i0], bnd->GetVertexPointer()[i3]);
					else
						poly.InitQuad(i0, i1, i2, i3, bnd->GetVertexPointer()[i0], bnd->GetVertexPointer()[i1], bnd->GetVertexPointer()[i2], bnd->GetVertexPointer()[i3]);
					poly.SetMaterialIndex((u8) matIndex );

					Vector3 vecTemp = VEC3V_TO_VECTOR3(poly.GetUnitNormal());//, bnd->GetVertexPointer());
					if(vecTemp.Mag2() < 0.9f)
						//				if(poly.GetNormal().Mag2() < 0.9f)
					{
						// TODO: write a warning to log (polygon [index] has non-unit normal, determine type of poly by using IsTriangle()) - wil
						retval.Set(rexResult::WARNINGS, rexResult::CONVERSION);
					}
					else
					{
						bnd->SetPolygon(physPolyCount++, poly);
					}
#endif // POLY_MAX_VERTICES == 4
				}
			}
			else // no adjuncts
			{
				Assert( 0 && "Poly with no adjuncts!" );
			}
		}

		// do postprocessing on bound
		bnd->DecreaseNumPolys(physPolyCount);
		bnd->PostLoadCompute();

		// We used to just use the centroid offset as the center of gravity, but CalcCGOffset should give us a much better value.

		Vector3 newCGOffset;
		bnd->CalcCGOffset(RC_VEC3V(newCGOffset));
		bnd->SetCGOffset(RCC_VEC3V(newCGOffset));
		bnd->ComputeNeighbors(NULL);
		bnd->WeldVertices(1e-3f);
#if POLY_MAX_VERTICES == 4
		bnd->ConvertTrianglesToQuads();
#endif // POLY_MAX_VERTICES == 4
		bnd->RemoveDegeneratePolys();	
	}

	typedef atArray<rexMaxPoly> rexMaxPolyList;

	void RecCompositeOutput(rexObject*& p_Out,rexMaxBinaryCell* p_Cell,s32 PolyLimit,s32& Polycount)
	{
		if(p_Cell == NULL)
			return;

		atArray<rexMaxPoly> PolyList;
		p_Cell->GetPolyList(PolyList);
		s32 a,Count = PolyList.GetCount();
		s32 OutPolyCount = 0;
		for(a=0;a<Count;a++)
			OutPolyCount += PolyList[a].PolyIdxList.GetCount();
	
		atArray<rexObject*> NewObjects;
		bool Allow = true;

		if(OutPolyCount > (PolyLimit * 2) || (OutPolyCount == 0)) 
		{
			// If we have no children all these polys will just be lost next recursion
			Assert(p_Cell->m_Children[0] != 0 && p_Cell->m_Children[1] != 0);

			Allow = false;
		}
		else
		{
			atArray<rexMaxPolyList*> polyLists;
			PolyList.QSort(0, Count, SortPolyList);

			polyLists.PushAndGrow(new rexMaxPolyList());
			int polyListIndex = 0;

			for(int i = 0; i < Count; i++)
			{
				const rexMaxPoly &poly = PolyList[i];	

				if(polyLists[polyListIndex]->empty())
				{
					polyLists[polyListIndex]->PushAndGrow(poly);
				}
				else
				{
					const rexMaxPoly &lastPoly = polyLists[polyListIndex]->back();

					if(lastPoly.flags == poly.flags)
					{
						polyLists[polyListIndex]->PushAndGrow(poly);
					}
					else
					{
						polyLists.PushAndGrow(new rexMaxPolyList());
						polyListIndex++;
						polyLists[polyListIndex]->PushAndGrow(poly);
					}
				}
			}	

			for(int group = 0; group < polyLists.GetCount(); group++)
			{

				rexObject *p_New = CreateMeshFromPolyList(p_Cell, Polycount, *polyLists[group]);
				phBoundGeometry* p_Test = NULL;

				if(p_New)
					convertGenericToExportedBound(p_Test,(rexObjectGenericBound*)p_New);

				if(p_Test && (p_Test->GetNumPolygons() > PolyLimit || p_Test->GetNumVertices() > PolyLimit))
				{
					Allow = false;
					delete p_Test;
					break;
				}

				NewObjects.PushAndGrow(p_New);
			}

			for(int i = 0; i < polyLists.GetCount(); i++)
			{
				delete polyLists[i];
			}
		}

		if(Allow)
		{
			if(!NewObjects.empty())
			{
				for(int i = 0; i < NewObjects.GetCount(); i++)
				{
					rexObject *obj = NewObjects[i];
					rexObjectGenericMesh* test = (rexObjectGenericMesh*)(NewObjects[i]);
					if(p_Out)
						p_Out->m_ContainedObjects.PushAndGrow(obj);
					else
						p_Out = obj;
				}
			}
		}
		else
		{
			for(int i = 0; i < NewObjects.GetCount(); i++)
			{
				delete NewObjects[i];
			}

			RecCompositeOutput(p_Out,p_Cell->m_Children[0],PolyLimit,Polycount);
			RecCompositeOutput(p_Out,p_Cell->m_Children[1],PolyLimit,Polycount);
		}

/*
		//old, less accurate way of doing things...

		if((OutPolyCount < PolyLimit) || ((p_Cell->m_Children[0] == NULL) && (p_Cell->m_Children[1] == NULL)))
		{
			rexObject* p_New = CreateMeshFromPolyList(p_Cell,Polycount);

			if(p_New)
			{
				if(p_Out)
					p_Out->m_ContainedObjects.PushAndGrow(p_New);
				else
					p_Out = p_New;
			}
		}
		else
		{
			RecCompositeOutput(p_Out,p_Cell->m_Children[0],PolyLimit,Polycount);
			RecCompositeOutput(p_Out,p_Cell->m_Children[1],PolyLimit,Polycount);
		}*/
	}

	rexObject* CreateMeshFromCell(rexMaxBinaryCell* p_Cell,s32& Polycount, bool ExportAsComposite)
	{
		Polycount = 0;

		if(!p_Cell)
			return NULL;
	
		if(ExportAsComposite)
		{
			rexObject* p_Out = NULL;
			RecCompositeOutput(p_Out,p_Cell,8 * 1024,Polycount);

			return p_Out;
		}
		else
		{
			atArray<rexMaxPoly> PolyList;
			p_Cell->GetPolyList(PolyList);
			return CreateMeshFromPolyList(p_Cell,Polycount,PolyList);
		}
	}
#else
	rexObjectGenericMesh* CreateMeshFromCell(rexMaxBinaryCell* p_Cell,s32& Polycount)
	{
		Polycount = 0;

		if(!p_Cell)
			return NULL;

		atArray<rexMaxPoly> PolyList;

		p_Cell->GetPolyList(PolyList);

		s32 a,b,c,AdjCount,Count = PolyList.GetCount();

		Polycount += Count;

		Assert(Count == p_Cell->m_Count);

		rexObjectGenericBound* p_NewBound = new rexObjectGenericBound;
		p_NewBound->m_Type = phBound::GEOMETRY;
		rexObjectGenericMesh* p_NewMesh = p_NewBound;

		//p_NewMesh->m_BoneIndex
		//p_NewMesh->m_BoneCount
		//p_NewMesh->m_LODGroupIndex
		//p_NewMesh->m_LODLevel
		//p_NewMesh->m_GroupIndex
		//p_NewMesh->m_LODThreshold
		//p_NewMesh->m_GroupID
		p_NewMesh->m_Matrix.Identity();
		p_NewMesh->m_ParentBoneMatrix.Identity();
		p_NewMesh->m_Center = p_Cell->m_Center;
		p_NewMesh->m_PivotPoint = p_Cell->m_Center;
		p_NewMesh->m_IsSkinned = false;
		p_NewMesh->m_IsValid = true;
		//p_NewMesh->m_HasMultipleTextureCoordinates
		p_NewMesh->m_ChildOfLevelInstanceNode = false;
		//p_NewMesh->m_SpecialFlags
		//p_NewMesh->m_SkinBoneMatrices
		//p_NewMesh->m_InstAttrNames
		//p_NewMesh->m_InstAttrValues
		//p_NewMesh->m_BlindDataIds
		//p_NewMesh->m_BlindDataTypes
		//p_NewMesh->m_BlendTargets

		s32 PolyOffset = 0;
		atMap<s32,s32> VertexOld2New;
		atMap<s32,s32> NormalOld2New;
		atMap<s32,s32> MaterialOld2New;

		PolyList.QSort(0,Count,SortPolyList);

		s32* p_New;
		s32 LastMatIdx = -1;
		s32 LastMatIdxNew = -1;
		s32 LastVertIdx = -1;
		s32 LastVertIdxNew = -1;
		s32 LastNormIdx = -1;
		s32 LastNormIdxNew = -1;

		p_NewMesh->m_Primitives.Reset();
		p_NewMesh->m_Primitives.Reserve(Count);

		p_NewMesh->m_Materials.Reset();
		p_NewMesh->m_Materials.Reserve(8);
		p_NewMesh->m_Vertices.Reset();
		p_NewMesh->m_Vertices.Reserve(32);
		p_NewMesh->m_Normals.Reset();
		p_NewMesh->m_Normals.Reserve(32);

		for(a=0;a<Count;a++)
		{
			if(PolyList[a].MeshIdx != PolyList[PolyOffset].MeshIdx)
			{
				LastMatIdx = -1;
				LastMatIdxNew = -1;
				LastVertIdx = -1;
				LastVertIdxNew = -1;
				LastNormIdx = -1;
				LastNormIdxNew = -1;

				for(b=PolyOffset;b<a;b++)
				{
					rexObjectGenericMesh::PrimitiveInfo NewPrimitive = m_Meshes[PolyList[b].MeshIdx]->m_Primitives[PolyList[b].PolyIdx];

					if(LastMatIdx != NewPrimitive.m_MaterialIndex)
					{
						LastMatIdx = NewPrimitive.m_MaterialIndex;
						p_New = MaterialOld2New.Access(NewPrimitive.m_MaterialIndex);
						Assert(p_New);
						LastMatIdxNew = *p_New;
					}

					NewPrimitive.m_MaterialIndex = LastMatIdxNew;

					AdjCount = NewPrimitive.m_Adjuncts.GetCount();

					for(c=0;c<AdjCount;c++)
					{
						if(LastVertIdx != NewPrimitive.m_Adjuncts[c].m_VertexIndex)
						{
							LastVertIdx = NewPrimitive.m_Adjuncts[c].m_VertexIndex;
							p_New = VertexOld2New.Access(NewPrimitive.m_Adjuncts[c].m_VertexIndex);
							Assert(p_New);
							LastVertIdxNew = *p_New;
						}

						if(LastNormIdx != NewPrimitive.m_Adjuncts[c].m_NormalIndex)
						{
							LastNormIdx = NewPrimitive.m_Adjuncts[c].m_NormalIndex;
							p_New = NormalOld2New.Access(NewPrimitive.m_Adjuncts[c].m_NormalIndex);
							Assert(p_New);
							LastNormIdxNew = *p_New;
						}

						NewPrimitive.m_Adjuncts[c].m_VertexIndex = LastVertIdxNew;
						NewPrimitive.m_Adjuncts[c].m_NormalIndex = LastNormIdxNew;
					}

					p_NewMesh->m_Primitives.Append() = NewPrimitive;
				}

				VertexOld2New.Kill();
				NormalOld2New.Kill();
				MaterialOld2New.Kill();
				LastMatIdx = -1;
				LastMatIdxNew = -1;
				LastVertIdx = -1;
				LastVertIdxNew = -1;
				LastNormIdx = -1;
				LastNormIdxNew = -1;

				PolyOffset = a;
			}

			rexObjectGenericMesh& r_Mesh = *m_Meshes[PolyList[a].MeshIdx];
			rexObjectGenericMesh::PrimitiveInfo& r_Primitive = r_Mesh.m_Primitives[PolyList[a].PolyIdx];
			atArray<rexObjectGenericMesh::AdjunctInfo>& r_Adjuncts = r_Primitive.m_Adjuncts;

			if(LastMatIdx != r_Primitive.m_MaterialIndex)
			{
				p_New = MaterialOld2New.Access(r_Primitive.m_MaterialIndex);

				if(!p_New)
				{
					LastMatIdx = r_Primitive.m_MaterialIndex;

					s32 MatCount = p_NewMesh->m_Materials.GetCount();
					s32 FoundIdx = -1;

					for(b=0;b<MatCount;b++)
					{
						if(p_NewMesh->m_Materials[b].m_Name == r_Mesh.m_Materials[r_Primitive.m_MaterialIndex].m_Name)
						{
							FoundIdx = b;
							break;
						}
					}

					if(FoundIdx == -1)
					{
						MaterialOld2New.Insert(r_Primitive.m_MaterialIndex,p_NewMesh->m_Materials.GetCount());
						p_NewMesh->m_Materials.Grow(p_NewMesh->m_Materials.GetCount()) = r_Mesh.m_Materials[r_Primitive.m_MaterialIndex];
					}
					else
					{
						MaterialOld2New.Insert(r_Primitive.m_MaterialIndex,FoundIdx);
					}
				}
			}

			AdjCount = r_Adjuncts.GetCount();

			for(b=0;b<AdjCount;b++)
			{
				if(LastVertIdx != r_Adjuncts[b].m_VertexIndex)
				{
					LastVertIdx = r_Adjuncts[b].m_VertexIndex;

					p_New = VertexOld2New.Access(r_Adjuncts[b].m_VertexIndex);

					if(!p_New)
					{
						VertexOld2New.Insert(r_Adjuncts[b].m_VertexIndex,p_NewMesh->m_Vertices.GetCount());

						p_NewMesh->m_Vertices.Grow(p_NewMesh->m_Vertices.GetCount()) = r_Mesh.m_Vertices[r_Adjuncts[b].m_VertexIndex];
					}
				}

				if(LastNormIdx != r_Adjuncts[b].m_NormalIndex)
				{
					LastNormIdx = r_Adjuncts[b].m_NormalIndex;

					p_New = NormalOld2New.Access(r_Adjuncts[b].m_NormalIndex);

					if(!p_New)
					{
						NormalOld2New.Insert(r_Adjuncts[b].m_NormalIndex,p_NewMesh->m_Normals.GetCount());

						p_NewMesh->m_Normals.Grow(p_NewMesh->m_Normals.GetCount()) = r_Mesh.m_Normals[r_Adjuncts[b].m_NormalIndex];
					}
				}
			}
		}

		LastMatIdx = -1;
		LastMatIdxNew = -1;
		LastVertIdx = -1;
		LastVertIdxNew = -1;
		LastNormIdx = -1;
		LastNormIdxNew = -1;

		for(b=PolyOffset;b<Count;b++)
		{
			rexObjectGenericMesh::PrimitiveInfo NewPrimitive = m_Meshes[PolyList[b].MeshIdx]->m_Primitives[PolyList[b].PolyIdx];

			if(LastMatIdx != NewPrimitive.m_MaterialIndex)
			{
				LastMatIdx = NewPrimitive.m_MaterialIndex;
				p_New = MaterialOld2New.Access(NewPrimitive.m_MaterialIndex);
				Assert(p_New);
				LastMatIdxNew = *p_New;
			}

			NewPrimitive.m_MaterialIndex = LastMatIdxNew;

			AdjCount = NewPrimitive.m_Adjuncts.GetCount();

			for(c=0;c<AdjCount;c++)
			{
				if(LastVertIdx != NewPrimitive.m_Adjuncts[c].m_VertexIndex)
				{
					LastVertIdx = NewPrimitive.m_Adjuncts[c].m_VertexIndex;
					p_New = VertexOld2New.Access(NewPrimitive.m_Adjuncts[c].m_VertexIndex);
					Assert(p_New);
					LastVertIdxNew = *p_New;
				}

				if(LastNormIdx != NewPrimitive.m_Adjuncts[c].m_NormalIndex)
				{
					LastNormIdx = NewPrimitive.m_Adjuncts[c].m_NormalIndex;
					p_New = NormalOld2New.Access(NewPrimitive.m_Adjuncts[c].m_NormalIndex);
					Assert(p_New);
					LastNormIdxNew = *p_New;
				}

				NewPrimitive.m_Adjuncts[c].m_VertexIndex = LastVertIdxNew;
				NewPrimitive.m_Adjuncts[c].m_NormalIndex = LastNormIdxNew;
			}

			p_NewMesh->m_Primitives.Append() = NewPrimitive;
		}

		return p_NewMesh;
	}
#endif // HACK_GTA4

#if HACK_GTA4
	void CombineMeshes(s32 maxPrimCountToCombine, bool ExportAsComposite )
#else
	void CombineMeshes(s32 maxPrimCountToCombine)
#endif // HACK_GTA4
	{
		atArray<rexMaxBinaryCell*> CellList;

		GetCellList(maxPrimCountToCombine,CellList);

		s32 a,Count = CellList.GetCount();
		s32 TotalPolyCountOut = 0;
		s32 PolyCountOut = 0;

		//construct the new meshes
		m_Combiner->SetProgressSize( Count );

		for(a = 0;a < Count;a++)
		{
			m_Combiner->SetProgressPos( a );
#if HACK_GTA4
			rexObject* p_NewObject = CreateMeshFromCell(CellList[a],PolyCountOut,ExportAsComposite);
			rexObjectGenericMesh* p_NewMesh = dynamic_cast<rexObjectGenericMesh*>(p_NewObject);
#else
			rexObjectGenericMesh* p_NewMesh = CreateMeshFromCell(CellList[a],PolyCountOut);
#endif

			if(p_NewMesh)
			{
				char cName[64];
				sprintf(cName,"Merge_%d",a);
				
				p_NewMesh->SetMeshName(cName);

				TotalPolyCountOut += PolyCountOut;
#if !HACK_GTA4
				if(p_NewMesh)
					m_MeshesOut.PushAndGrow(p_NewMesh);
#endif // HACK_GTA4
			}
#if HACK_GTA4
			if(p_NewObject)
				m_ObjOut.PushAndGrow(p_NewObject);
#endif // HACK_GTA4
		}

		Assertf( TotalPolyCountOut == m_TotalPolyCountIn, "Mismatched number of input polys (%d) and output polys (%d) [maxPrimCountToCombine=%d]", m_TotalPolyCountIn, TotalPolyCountOut, maxPrimCountToCombine );
	}

	const rexProcessorMaxBoundCombiner* m_Combiner;
	s32 m_TotalPolyCountIn;
	atArray<rexObjectGenericMesh*> m_Meshes;
#if HACK_GTA4
	atArray<rexObject*> m_ObjOut;
#else
	atArray<rexObjectGenericMesh*> m_MeshesOut;
#endif
};

bool FindEligibleParent(rexObjectGenericBound* boundPrim, rexObjectGenericBound* bound)
{
	bool eligibleParentFound = false;
		
	if(bound && boundPrim)
	{
		if(boundPrim->m_Matrix.d.x < (bound->m_Center.x + bound->m_BoundBoxMax.x) &&
			boundPrim->m_Matrix.d.x > (bound->m_Center.x + bound->m_BoundBoxMin.x) &&
			boundPrim->m_Matrix.d.y < (bound->m_Center.y + bound->m_BoundBoxMax.y) &&
			boundPrim->m_Matrix.d.y > (bound->m_Center.y + bound->m_BoundBoxMin.y) &&
			boundPrim->m_surfaceFlags == bound->m_surfaceFlags)
		{
			bound->m_ContainedObjects.PushAndGrow(boundPrim);
			eligibleParentFound = true;
			
		}
	}
	if(!eligibleParentFound)
	{
		for( int p = 0; p < bound->m_ContainedObjects.GetCount(); p++ )
		{
			rexObjectGenericBound* childbound = dynamic_cast<rexObjectGenericBound*>(bound->m_ContainedObjects[p]);
			eligibleParentFound = FindEligibleParent(boundPrim, childbound);
			if(eligibleParentFound)
			{
				break;
			}
		}
	}
	return eligibleParentFound;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void rexProcessorMaxBoundCombiner::SetProgressSize(s32 Size) const
{
	if(m_ProgressBarObjectCountCB)
		(*(m_ProgressBarObjectCountCB)) ( Size );
}

void rexProcessorMaxBoundCombiner::SetProgressPos(s32 Pos) const
{
	if(m_ProgressBarObjectCurrentIndexCB)
		(*(m_ProgressBarObjectCurrentIndexCB)) ( Pos );
}

#if HACK_GTA4
void rexProcessorMaxBoundCombiner::CombineMeshes(atArray< rexObject* >& r_InOut) const
#else
void rexProcessorMaxBoundCombiner::CombineMeshes(atArray< rexObjectGenericMesh* >& r_Meshes) const
#endif // HACK_GTA4
{
#if HACK_GTA4
	int objCount = r_InOut.GetCount();
	if( objCount <= 0 )
#else
	atArray< rexObjectGenericMesh* > combinedMeshes;

	int meshCount = r_Meshes.GetCount();
	if( meshCount <= 1 )
#endif
		return;

	Vector3 min;
	Vector3 max;

	bool First = true;
#if HACK_GTA4
	for( int m = 0; m < objCount; m++ )
	{
		rexObjectGenericMesh* mesh = dynamic_cast<rexObjectGenericMesh*>( r_InOut[ m ] );
#else
	for( int m = 0; m < meshCount; m++ )
	{
		rexObjectGenericMesh* mesh = dynamic_cast<rexObjectGenericMesh*>( r_Meshes[ m ] );
#endif // HACK_GTA4
		s32 a,b,c,Count = mesh->m_Primitives.GetCount();

		for(a = 0;a < Count;a++)
		{
			s32 AdjCount = mesh->m_Primitives[a].m_Adjuncts.GetCount();

			for(b = 0;b < AdjCount;b++)
			{
				Vector3 newVal = mesh->m_Vertices[mesh->m_Primitives[a].m_Adjuncts[b].m_VertexIndex].m_Position;

				if(First)
				{
					First = false;
					min = newVal;
					max = newVal;
				}
				else
				{
					for(c=0;c<3;c++)
					{
						if(newVal[c] > max[c])
							max[c] = newVal[c];
						if(newVal[c] < min[c])
							min[c] = newVal[c];
					}
				}
			}
		}
	}

	Vector3 dimension = max - min;
	Vector3 center = min + (dimension / 2.0f);

	rexMaxBinaryTree binaryTree(this);
	float cellSize = 25.0f;
	binaryTree.Init( center, dimension, cellSize );
#if HACK_GTA4
	for( int m = 0; m < objCount; m++ )
	{
		rexObjectGenericMesh* mesh = dynamic_cast<rexObjectGenericMesh*>(r_InOut[ m ]);
#else
	for( int m = 0; m < meshCount; m++ )
	{
		rexObjectGenericMesh* mesh = r_Meshes[ m ];
#endif // HACK_GTA4
		Assert( mesh );
		AssertVerify( binaryTree.AddMesh( *mesh ) );
	}

#if HACK_GTA4
	binaryTree.CombineMeshes( m_MaxBoundPolyCountToCombine, m_ExportAsComposite );
	atArray<rexObject*> boundPrims;;
	for( int m = 0; m < objCount; m++ )
	{
		rexObjectGenericBound* bound = dynamic_cast<rexObjectGenericBound*>(r_InOut[ m ]);
		if(bound)
		{
			if(bound->IsValidMesh())
				delete r_InOut[ m ];
			else
				boundPrims.PushAndGrow(r_InOut[ m ]);
		}
		else
			delete r_InOut[ m ];
	}

	r_InOut.Reset();

	int primCount = boundPrims.GetCount();

	objCount = binaryTree.m_ObjOut.GetCount();
	
	atArray<rexObjectGenericBound*> primBackupBounds;
	for( int m = 0; m < primCount; m++ )
	{
		if(m_ExportAsComposite)
		{
			bool eligableParentFound = false;
			rexObjectGenericBound* boundPrim = dynamic_cast<rexObjectGenericBound*>(boundPrims[m]);
			
			for( int p = 0; p < objCount; p++ )
			{
				rexObjectGenericBound* bound = dynamic_cast<rexObjectGenericBound*>(binaryTree.m_ObjOut[p]);
				eligableParentFound = FindEligibleParent(boundPrim, bound);
				if(eligableParentFound)
					break;
			}
			
			if(!eligableParentFound)
			{
				rexObjectGenericBound* boundPrim = dynamic_cast<rexObjectGenericBound*>(boundPrims[m]);
				for(int p = 0; p < primBackupBounds.GetCount(); p++)
				{
					if(primBackupBounds[p]->m_surfaceFlags == boundPrim->m_surfaceFlags)
					{
						primBackupBounds[p]->m_ContainedObjects.PushAndGrow(boundPrim);
						eligableParentFound = true;
						break;
					}
				}

				if(!eligableParentFound)
				{
					rexObjectGenericBound* primBackupBound = new rexObjectGenericBound;
					primBackupBound->m_Type = phBound::GEOMETRY;
					primBackupBound->m_surfaceFlags = boundPrim->m_surfaceFlags;
					primBackupBound->m_ContainedObjects.PushAndGrow(boundPrim);
					primBackupBounds.PushAndGrow(primBackupBound);
					eligableParentFound = true;
				}
			}
		}
		else
		{
			r_InOut.PushAndGrow(boundPrims[m]);
		}
	}

	for( int m = 0; m < objCount; m++ )
	{
		r_InOut.PushAndGrow(binaryTree.m_ObjOut[m]);
	}

	for(int p = 0; p < primBackupBounds.GetCount(); p++)
	{
		if(r_InOut.GetCount() > 0)
		{
			rexObjectGenericBound* bound = dynamic_cast<rexObjectGenericBound*>(r_InOut[0]);
			bound->m_ContainedObjects.PushAndGrow(primBackupBounds[p]);
		}
		else
		{
			r_InOut.PushAndGrow(primBackupBounds[p]);
		}
	}
	
#else
	binaryTree.CombineMeshes( m_MaxBoundPolyCountToCombine );

	for( int m = 0; m < meshCount; m++ )
	{
		delete r_Meshes[ m ];
	}

	r_Meshes.Reset();

	meshCount = binaryTree.m_MeshesOut.GetCount();

	for( int m = 0; m < meshCount; m++ )
	{
		r_Meshes.PushAndGrow(binaryTree.m_MeshesOut[m]);
	}
#endif // HACK_GTA4
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexProcessorMaxBoundCombiner::Process( rexObject* object ) const
{
	if(!m_Enabled)
	{
		return rexResult( rexResult::PERFECT );
	}

	if(m_MaxBoundPolyCountToCombine > 0)
	{
		if( m_ProgressBarTextCB )
		{
			static char message[1024];
			sprintf( message, "Combining Mesh Data" );
			(*m_ProgressBarTextCB) ( message );
		}

		rexObjectGenericMeshHierarchy* hier = dynamic_cast<rexObjectGenericMeshHierarchy*>( object );
		if( hier )
		{
			if( m_MaxBoundPolyCountToCombine == 0 )
				return rexResult( rexResult::PERFECT );

#if HACK_GTA4 			
			
			atArray< rexObjectGenericMesh* > MeshesIn;
			hier->GetMeshes( MeshesIn );

			if(MeshesIn.GetCount() > 0)
			{
				s32 a,Count = MeshesIn.GetCount();

				atArray< rexObject* > Meshes;
				Meshes.Reset();
				Meshes.Resize(Count);

				for(a=0;a<Count;a++)
					Meshes[a] = MeshesIn[a];
#else
			atArray< rexObjectGenericMesh* > Meshes;
			hier->GetMeshes( Meshes );

			if(Meshes.GetCount() > 1)
			{
				s32 a,Count = Meshes.GetCount();
#endif // HACK_GTA4
				CombineMeshes( Meshes );

				hier->m_ContainedObjects.Reset();

				Count = Meshes.GetCount();

				for(a=0;a<Count;a++)
				{
					hier->m_ContainedObjects.PushAndGrow(Meshes[a]);
				}
			}

			return rexResult( rexResult::PERFECT );
		}

		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}
	else
	{
		return rexProcessorMeshCombiner::Process(object);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyMaxBoundMaxPolyCountToCombine::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorMaxBoundCombiner* processor = dynamic_cast<rexProcessorMaxBoundCombiner*>( module.m_Processors[ a ] );
		if( processor )
		{
			processor->SetBoundMaxPolyCountToCombine( value.toInt( processor->GetBoundMaxPolyCountToCombine() ) );
		}
	}
	return rexResult( rexResult::PERFECT );
}

} // namespace rage
