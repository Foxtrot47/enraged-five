#include "rexMaxToGeneric/propertyBasic.h"
#include "rexMaxToGeneric/converterAnimation.h"
#include "rexMaxToGeneric/converterSkeleton.h"
#include "rexMaxToGeneric/converterBoundHierarchy.h"
#include "rexMaxToGeneric/processorMeshCombiner.h"
#include "rexGeneric/objectEntity.h"
#include "rexGeneric/processorMeshCombiner.h"
#include "rexBase/module.h"
#include "rexMax/object.h"
#include "rexmaxutility/utility.h"
#include "rexRage/serializerAnimation.h"
#include "rexRAGE/serializerBound.h"
#include "rexRage/serializerSkeleton.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxName::ModifyObject(rexObject& obj,rexValue value,const atArray<rexObject*>& objects) const
{
	rexObjectGenericEntityComponent* comp = dynamic_cast<rexObjectGenericEntityComponent*>(&obj);

	if(!comp)
		return rexResult( rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	comp->SetName(rexMaxUtility::ReplaceWhitespaceCharacters(value.toString()));

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxOutputPath::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	if(module.m_Serializer)
	{
		module.m_Serializer->SetOutputPath( atString(value.toString()) );
		return rexResult(rexResult::PERFECT);
	}

	return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxExportData::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	if(module.m_Serializer)
	{
		module.m_Serializer->SetExportData(value.toBool(module.m_Serializer->GetExportData()));
		return rexResult(rexResult::PERFECT);
	}

	return rexResult(rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxInputNodes::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	module.m_InputObjects.Reset();

	int numInputObj = objects.GetCount();

	rexMaxObject *obj = NULL;
	for(int i = 0; i < numInputObj; i++)
	{
		obj = dynamic_cast<rexMaxObject*>(objects[i]);
		Assert(obj);
		if(obj)
			module.m_InputObjects.PushAndGrow(new rexMaxObject(*obj),(u16) numInputObj);
	}
	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxChannelsToWrite::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
//	rexConverterMaxAnimation* animConv = dynamic_cast<rexConverterMaxAnimation*>( module.m_Converter );
	rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
	rexSerializerRAGESkeleton* skelSer = dynamic_cast<rexSerializerRAGESkeleton*>( module.m_Serializer );

	if( value.toString() )
	{
		atString channelsString(value.toString());
		int charCount = channelsString.GetLength();
		atString currChannelInputString, currChannelOutputString;
		bool readingInputString = true, readingChannelID = false, appliesToRootsOnly = false;
		int currChannelGroup = -1, currChannelID = 0;
		atString channelIDBuffer;

		for( int a = 0; a < charCount; a++ )
		{
			if( channelsString[ a ] == '(' )
			{
				if( animSer )
					currChannelGroup = animSer->AddChannelGroup();
				if( skelSer )
					currChannelGroup = skelSer->AddChannelGroup();
//				if( animConv )
//					currChannelGroup = animConv->AddChannelGroup();
			}
			else if( channelsString[ a ] == ')' )
			{
				currChannelGroup = -1;
			}
			else if( channelsString[ a ] == '[' )
			{
				readingChannelID = true;
			}
			else if( channelsString[ a ] == ']' )
			{
				readingChannelID = false;
				currChannelID = atoi( channelIDBuffer );
				channelIDBuffer.Reset();
			}
			else if( readingChannelID )
			{
				channelIDBuffer += channelsString[ a ];
			}
			else if( readingInputString )
			{
				if(( channelsString[ a ] == ';' ) || ( a == ( charCount - 1 ) ))
				{
					if( animSer )
						animSer->AddChannelToWrite( currChannelInputString, currChannelInputString, currChannelID, appliesToRootsOnly, currChannelGroup ); // input and output name same
					if( skelSer )
						skelSer->AddChannelToWrite( currChannelInputString, currChannelInputString, currChannelID, appliesToRootsOnly, currChannelGroup ); // input and output name same
//					if( animConv )
//						animConv->AddChannelToWrite( currChannelInputString, currChannelInputString, currChannelID, appliesToRootsOnly, currChannelGroup ); // input and output name same
					currChannelID++;
					currChannelInputString.Reset();
					appliesToRootsOnly = false;
				}
				else if( channelsString[ a ] == '|' )
				{
					readingInputString = false;
				}
				else if( channelsString[ a ] == '*' )
				{
					appliesToRootsOnly = true;
				}
				else
				{
					currChannelInputString += channelsString[ a ];
				}
			}
			else
			{
				if( channelsString[ a ] == ';' || ( a == ( charCount - 1 ) ))
				{
					if( animSer )
						animSer->AddChannelToWrite( currChannelInputString, currChannelOutputString, currChannelID, appliesToRootsOnly, currChannelGroup );
					if( skelSer )
						skelSer->AddChannelToWrite( currChannelInputString, currChannelOutputString, currChannelID, appliesToRootsOnly, currChannelGroup );
//					if( animConv )
//						animConv->AddChannelToWrite( currChannelInputString, currChannelOutputString, currChannelID, appliesToRootsOnly, currChannelGroup );
					currChannelID++;
					currChannelInputString.Reset();
					currChannelOutputString.Reset();
					readingInputString = true;
					appliesToRootsOnly = false;
				}
				else if( channelsString[ a ] == '*' )
				{
					appliesToRootsOnly = true;
				}
				else
				{
					currChannelOutputString += channelsString[ a ];
				}
			}
		}
	}
	return rexResult( rexResult::PERFECT );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxExportCtrlFile::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexResult result = rexResult::PERFECT;

	rexConverterMaxAnimation* animConv = dynamic_cast<rexConverterMaxAnimation*>( module.m_Converter );
	rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
	rexSerializerRAGESkeleton* skelSer = dynamic_cast<rexSerializerRAGESkeleton*>( module.m_Serializer );
	rexConverterMaxSkeleton* skelConv = dynamic_cast<rexConverterMaxSkeleton*>( module.m_Converter );

	if( value.toString() )
	{
		/*
		if(animConv)
		{
			bool bRes = animConv->LoadAnimExportCtrlFile( (const char*)(value.toString()) );
			if(!bRes)
			{
				result = rexResult::ERRORS;
				result.AddMessage("Failed to load animation export control file : '%s'", (const char*)(value.toString()));
				return result;
			}
			animConv->SetUseAnimCtrlExportFile(true);
		}*/

		if(animSer)
		{
			bool bRes = animSer->LoadAnimExportCtrlFile( (const char*)(value.toString()) );
			if(!bRes)
			{
				result = rexResult::ERRORS;
				result.AddError("Failed to load animation export control file : '%s'", (const char*)(value.toString()));
				return result;
			}
			animSer->SetUseAnimCtrlExportFile(true);
		}

		if(skelSer)
		{
			bool bRes = skelSer->LoadAnimExportCtrlFile( (const char*)(value.toString()) );
			if(!bRes)
			{
				result = rexResult::ERRORS;
				result.AddError("Failed to load animation export control file : '%s'", (const char*)(value.toString()));
				return result;
			}
			skelSer->SetUseAnimCtrlExportFile(true);
		}

		if(skelConv)
		{
			bool bRes = skelConv->LoadAnimExportCtrlFile( (const char*)(value.toString()) );
			if(!bRes)
			{
				result = rexResult::ERRORS;
				result.AddError("Failed to load animation export control file : '%s'", (const char*)(value.toString()));
				return result;
			}
			skelConv->SetUseAnimCtrlExportFile(true);
		}
	}

	return result;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundExportPrimitivesInBvh::SetupModuleInstance( rexModule &module, rexValue value, const atArray<rexObject*>& /*objects*/) const
{
	int processorCount = module.m_Processors.GetCount();
	for ( int i = 0; i < processorCount; ++i )
	{
		rexProcessorMeshCombiner* processor = dynamic_cast< rexProcessorMeshCombiner* >( module.m_Processors[ i ] );
		if ( processor )
		{
			processor->SetPrimitiveBoundExportPrimitivesInBvh( value.toBool( processor->GetPrimitiveBoundExportPrimitivesInBvh() ) );
		}
	}

	rexSerializerRAGEPhysicsBound* ser = dynamic_cast< rexSerializerRAGEPhysicsBound* >( module.m_Serializer );
	if ( ser )
	{
		ser->SetPrimitiveBoundExportPrimitivesInBvh( value.toBool( ser->GetPrimitiveBoundExportPrimitivesInBvh() ) );
	}

	rexConverterMaxBoundHierarchy* conv = dynamic_cast< rexConverterMaxBoundHierarchy* >( module.m_Converter );
	if ( conv )
	{
		conv->SetPrimitiveBoundExportPrimitivesInBvh( value.toBool() );
	}

	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
} 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundHasSecondSurface::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast< rexSerializerRAGEPhysicsBound* >( module.m_Serializer );

	if(ser)
	{
		ser->SetHasSecondSurface(value.toBool());
	}

	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundSecondSurfaceMaxHeight::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast< rexSerializerRAGEPhysicsBound* >( module.m_Serializer );

	if(ser)
	{
		ser->SetSecondSurfaceMaxHeight(value.toFloat());
	}

	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundForceComposite::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast< rexSerializerRAGEPhysicsBound* >( module.m_Serializer );

	if(ser)
	{
		ser->SetForceCompositeBound(value.toBool());
	}

	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundDisableOptimisation::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorMaxBoundCombiner* proc = dynamic_cast<rexProcessorMaxBoundCombiner*>(module.m_Processors[a]);

		if(proc)
		{
			proc->SetEnabled(!value.toBool());
		}
	}

	rexSerializerRAGEPhysicsBound* ser = dynamic_cast< rexSerializerRAGEPhysicsBound* >( module.m_Serializer );

	if(ser)
	{
		ser->SetOptimiseGeometry(!value.toBool());
	}

	return rexResult(rexResult::PERFECT);
}