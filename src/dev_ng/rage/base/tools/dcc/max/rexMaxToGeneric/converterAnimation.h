#ifndef __REX_MAX_ROCKSTAR_CONVERTERANIMATION_H__
#define __REX_MAX_ROCKSTAR_CONVERTERANIMATION_H__

#include "atl/map.h"
#include "rexBase/converter.h"
#include "rexBase/property.h"
#include "rexGeneric/objectAnimation.h"
#include "rexMax/object.h"
#include "rexBase/animExportCtrl.h"

namespace rage {

/* PURPOSE:
	property to switch on/off export of the mover track
*/
class rexPropertyAnimMoverTrack : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyAnimMoverTrack; }
};

/* PURPOSE:
	property to set the alternate mover track
*/
class rexPropertyAnimExtraMoverTrack : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyAnimExtraMoverTrack; }
};

/* PURPOSE:
	property to switch on/off export of selected animated nodes only
*/
class rexPropertyAnimExportSelected : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyAnimExportSelected; }
};

/* PURPOSE:
	property to set the mover node for an animation sequence
*/
class rexPropertyMaxAnimationMoverNode : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxAnimationMoverNode; }
};

/* PURPOSE:
	property to set the mover node for an animation sequence
*/
class rexPropertyMaxAnimationMoveRotToRoot : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxAnimationMoveRotToRoot; }
};

/* PURPOSE:
	property to set the authored orientation on
*/
class rexPropertyMaxAnimationAuthoredOrient : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxAnimationAuthoredOrient; }
};

/* PURPOSE:
property to set the compression tolerance
*/
class rexPropertyMaxAnimationCompressionTolerance : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxAnimationCompressionTolerance; }
};

/* PURPOSE:
property to set the project flags saved into an animation file
*/
class rexPropertyMaxAnimationLocalSpace : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxAnimationLocalSpace; }
};

/* PURPOSE:
property to set the project flags saved into an animation file
*/
class rexPropertyMaxAnimationEndFrame : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxAnimationEndFrame; }
};

/* PURPOSE:
property to set the project flags saved into an animation file
*/
class rexPropertyMaxAnimationStartFrame : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxAnimationStartFrame; }
};

/* PURPOSE:
property to set the project flags saved into an animation file
*/
class rexPropertyMaxAnimationProjectFlags : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxAnimationProjectFlags; }
};

/* PURPOSE:
property to set the project flags saved into an animation file
*/
class rexPropertyMaxAnimationUvAnimIndex : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxAnimationUvAnimIndex; }
};

/* PURPOSE:
property to set which max nodes should have morph target weight animation tracks exported from
*/
class rexPropertyMaxAnimationMorphWeightTracks : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxAnimationMorphWeightTracks; }
};

/* PURPOSE:
property to set which expression control nodes to export by directly specifying the node names 
in a semicolon separated list.  NOTE : The maximum string length for this list of nodes is 256 characters,
if the list is longer than this use the file version of the property.
*/
class rexPropertyAnimationExprControlNodes : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationExprControlNodes; }
};

/* PURPOSE:
property to set which expression control nodes to export by supplying a file containing the node names
*/
class rexPropertyAnimationExprControlNodesFile : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationExprControlNodesFile; }
};

/* PURPOSE:
	converts 3dsmax information into generic rex animation data
*/
class rexConverterMaxAnimation : public rexConverter
{
public:
	rexConverterMaxAnimation();

	virtual rexResult Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const;
	virtual rexConverter* CreateNew() const { return new rexConverterMaxAnimation; }

	void SetExportMoverTrack(bool ExportMoverTrack) { m_ExportAnimMoverTrack = ExportMoverTrack; }
	void SetExtraMoverTrack(atString& r_ExtraMoverTrack) { m_ExtraMoverTrack = r_ExtraMoverTrack; }
	void SetExportSelected(bool ExportSelected) { m_ExportAnimSelected = ExportSelected; }
	void SetExportMoveRotFromRoot(bool ExportMoveRotFromRoot) { m_ExportAnimMoveRotFromRoot = ExportMoveRotFromRoot; }
	void SetExportLocalSpace(bool ExportAnimLocalSpace) { m_ExportAnimLocalSpace = ExportAnimLocalSpace; }
	void SetMoverNode(INode* p_Node) { mp_MoverNode = p_Node; }
	void SetExportUvIndex(s32 Index) { m_ExportUVAnimIndex = Index; }
	void SetMorphWeightTracks(atString& r_MorphWeightTracks) { m_MorphWeightTracks = r_MorphWeightTracks; }
	void SetEndFrame(s32 EndFrame) { m_EndFrame = EndFrame; m_OverrideEndFrame=true; }
	void SetStartFrame(s32 StartFrame) { m_StartFrame = StartFrame; m_OverrideStartFrame=true; }

	void SetExprControlNodes(atString& r_ExprControlNodes) { m_ExprControlNodes = r_ExprControlNodes; }

	bool	GetUseAnimCtrlExportFile() const { return m_ExportCtrlSpec; }
	void	SetUseAnimCtrlExportFile(bool bUse) { m_ExportCtrlSpec = bUse; }
	bool	LoadAnimExportCtrlFile(const char* filePath)
	{
		return m_AnimExportCtrlSpec.LoadFromXML(filePath);
	}

protected:
	rexResult CalcFrameMatricesFromNode(INode* p_Node,atArray<Matrix34*>& FrameMatrices,bool IsRoot,bool IsMover = false) const;
#if HACK_GTA4
	void CalcGenericValuesFromNode(INode* p_Node,atArray<atArray<Vector3>>& FrameVectors) const;
#endif // HACK_GTA4
	rexResult InnerNodeToChunk(INode* p_Node,rexObjectGenericAnimation::ChunkInfo* p_Chunk,bool IsRoot) const;
	void InnerNodeToTrack(INode* p_Node,atArray<rexObjectGenericAnimation::TrackInfo*>& tracks,bool IsRoot,bool IsMover = false) const;
#if HACK_GTA4
	void GenericNodeToChunk(INode* p_Node,rexObjectGenericAnimation::ChunkInfo* p_Chunk, int NumTracks = 0) const;
#endif // HACK_GTA4
	rexResult GetAnimInfoRec(rexObjectGenericAnimation::ChunkInfo* p_Chunk,INode* p_Node,bool IsRoot = false) const;
#if HACK_GTA4
	void GetAnimVisemeInfoRec(rexObjectGenericAnimation::ChunkInfo* p_Chunk,INode* p_Node,bool IsRoot = false) const;
#endif // HACK_GTA4
	void GetMoverInfoRec(rexObjectGenericAnimation* mp_Animation,INode* p_Node) const;
	void GetTagInfoRec(rexObjectGenericAnimation* mp_Animation,INode* p_Node) const;
	void GetVisibilityInfoRec(rexObjectGenericAnimation::ChunkInfo* p_Chunk,INode* p_Node) const;
	void GetMorphTargetInfoRec(rexObjectGenericAnimation* mp_Animation,INode* p_Node) const;
	void GetUVAnimRec(rexObjectGenericAnimation* mp_Animation,INode* p_Node) const;
	void GetInterpolationInfoRec(rexObjectGenericAnimation* mp_Animation,INode* p_Node) const;
	rexResult GetExprControlData(rexObjectGenericAnimation* mp_Animation) const;
	rexResult ExportRootChuck(INode* p_RootNode,rexMaxObject* p_Obj) const;

	void InitTracksInfo(int NumFrames,atArray<rexObjectGenericAnimation::TrackInfo*>& tracks,const atArray<atString>& trackNames,const atArray<int>& trackIDs) const;

	void AddNodeToTracks(const atArray<Matrix34*>& r_SourceMatrices,int StartFrame,int EndFrame,atArray<rexObjectGenericAnimation::TrackInfo*>& r_Tracks) const;
	void AddNodeToTrackAtFrame(const Matrix34* r_SourceMatrix,rexObjectGenericAnimation::TrackInfo& r_Track) const;
#if HACK_GTA4
	void AddGenericNodeToTracks(const atArray<atArray<Matrix34*>>& r_GenericValues,int StartFrame,int EndFrame,atArray<rexObjectGenericAnimation::TrackInfo*>& r_Tracks) const;
#endif // HACK_GTA4

	bool IsAnimated(rexObjectGenericAnimation::ChunkInfo* p_Chunk) const;
#if HACK_GTA4
	bool IsViseme(Object* p_Object) const;
#endif // HACK_GTA4

	void DoRootTransformMove(rexObjectGenericAnimation::ChunkInfo* p_RootChunk) const;
	void IncludeFigureMode(rexObjectGenericAnimation::ChunkInfo* p_RootChunk) const;

	mutable float m_StartFrame;
	mutable float m_EndFrame;
	mutable rexObjectGenericAnimationSegment* mp_AnimSegment;
	mutable rexObjectGenericAnimation* mp_Animation;

	mutable atMap<rexObjectGenericAnimation::ChunkInfo*,atArray<Matrix34*> > m_outChunkData;
#if HACK_GTA4
	mutable atMap<rexObjectGenericAnimation::ChunkInfo*,atArray<atArray<Matrix34*>> > m_outGenericChunkData;
#endif // HACK_GTA4
	mutable Matrix34 m_MatSetRoot;

	atString m_ExtraMoverTrack;
	atString m_MorphWeightTracks;
	bool m_ExportAnimMoverTrack;
	bool m_ExportAnimSelected;
	bool m_ExportAnimMoveRotFromRoot;
	bool m_ExportAnimLocalSpace;
	mutable bool m_IsCamera;
	INode* mp_MoverNode;
	s32 m_ExportUVAnimIndex;
	atString m_ExprControlNodes;

	bool m_OverrideEndFrame; // Allows us to override the end frame, by default it just grabs it from the animationrange scrubber
	bool m_OverrideStartFrame; // Allows us to override the start frame, by default it just grabs it from the animationrange scrubber

	bool				m_ExportCtrlSpec;
	AnimExportCtrlSpec	m_AnimExportCtrlSpec;
};

} //namespace rage {

#endif //__REX_MAX_ROCKSTAR_CONVERTERANIMATION_H__
