#ifndef __REX_MAX_ROCKSTAR_CONVERTERMESH_H__
#define __REX_MAX_ROCKSTAR_CONVERTERMESH_H__

#include "rexmaxtogeneric/converterbasic.h"
#include "rexBase/property.h"

namespace rage {

class rexObjectHierarchy;

/* PURPOSE:
property to set on/off the export of the mesh for use with a skeleton
*/
class rexPropertyMeshOptimiseMaterials : public rexProperty
{
public:
	virtual rexResult SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty* CreateNew() const { return new rexPropertyMeshOptimiseMaterials; }
};

/* PURPOSE:
	property to set on/off the export of the mesh for use with a skeleton
*/
class rexPropertyMeshForSkeleton : public rexProperty
{
public:
	virtual rexResult SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty* CreateNew() const { return new rexPropertyMeshForSkeleton; }
};

/* PURPOSE:
	property to set on/off the export of the mesh for use with a skeleton
*/
class rexPropertyMeshSkinOffset : public rexProperty
{
public:
	virtual rexResult SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty* CreateNew() const { return new rexPropertyMeshSkinOffset; }
};

/* PURPOSE:
	property to set on/off the export of the mesh in ascii format
*/
class rexPropertyMeshAsAscii : public rexProperty
{
public:
	virtual rexResult SetupShell(const atString& objectName,rexShell& shell,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty* CreateNew() const { return new rexPropertyMeshAsAscii; }
};

/* PURPOSE:
property to turn on/off export of mesh data in world space
*/
class rexPropertyMeshExportWorldSpace : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshExportWorldSpace; }
};

/* PURPOSE:
property to set which map channel data is exported into the generic mesh blind data channels
*/
class rexPropertyMeshBlindVertexDataChannels : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshBlindVertexDataChannels; }
};

#if HACK_GTA4
/* PURPOSE:
property to set custom rules for vertex colour data on export
*/
class rexPropertyMeshCustomVertexColour : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshCustomVertexColour; }
};

/* PURPOSE:
property to set custom rules for vertex colour data on export
*/
class rexPropertyMeshGenerateTintIndices : public rexProperty
{
public:
	//virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshGenerateTintIndices; }
	virtual rexResult		ModifyProcessedObject( rexObject& object, rexValue value, const atArray<rexObject*>& objects  ) const;
};

/* PURPOSE:
property to set custom rules for vertex colour data on export
*/
class rexPropertyBoundGenerateMaterials : public rexProperty
{
public:
	//virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundGenerateMaterials; }
	virtual rexResult		ModifyProcessedObject( rexObject& object, rexValue value, const atArray<rexObject*>& objects  ) const;
};
#endif // HACK_GTA4

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyCullVertexChannels : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyCullVertexChannels; }
};


/* PURPOSE:
property to set how many additional UV channels to force export
*/
class rexPropertyForcedAdditionalUVCount : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyForcedAdditionalUVCount; }
};

/* PURPOSE:
property to set the need to limit the bones skinned to from this mesh to the set in the skeleton spec file
*/
class rexPropertyLimitToSkelSpecFileBones : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyLimitToSkelSpecFileBones; }
};


/* PURPOSE:
property to set the use of weighted normals acorss neighbouring polys.
*/
class rexPropertyUseWeightedNormals : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyUseWeightedNormals; }
};


/* PURPOSE:
	converts max mesh data into the generic rex mesh format
*/
class rexConverterMaxMeshHierarchy : public rexConverter
{
public:
	rexConverterMaxMeshHierarchy();

	void SetIsForSkeleton(bool IsForSkeleton) { m_IsForSkeleton = IsForSkeleton; }
	void SetSkinOffset(bool SkinOffset) { m_SkinOffset = SkinOffset; }
	void SetExportWorldSpace(bool ExportWorldSpace) { m_ExportWorldSpace = ExportWorldSpace; }
	void SetOptimiseMaterials(bool OptimiseMaterials) { m_OptimiseMaterials = OptimiseMaterials; }
	void SetForcedAdditionalUVCount(int ForcedAdditionalUVCount) { m_ForcedAdditionalUVCount = ForcedAdditionalUVCount; }
	void SetCustomVertexColour(atString& CustomVertexColour) { m_CustomVertexColour = CustomVertexColour; }

	void		SetCullVertexChannels(const int b)						{ m_CullVertexChannels = b; }
	int			GetCullVertexChannels() const							{ return m_CullVertexChannels; }

	void		SetLimitToSpecFileBones(const bool b)					{ m_LimitToSpecFileBones = b; }
	bool		GetLimitToSpecFileBones() const							{ return m_LimitToSpecFileBones; }

	void		SetUseWeightedNormals(const bool b)					{ m_UseWeightedNormals = b; }
	bool		GetUseWeightedNormals() const							{ return m_UseWeightedNormals; }
	

	void AddBlindDataExport(const atString bdName, int bdVDataChnId, int bdRuntimeId, float bdDefault)
							{
								m_BlindDataNames.PushAndGrow(bdName);
								m_BlindDataVDataChnIds.PushAndGrow(bdVDataChnId);
								m_BlindDataRuntimeIds.PushAndGrow(bdRuntimeId);
								m_BlindDataDefaults.PushAndGrow(bdDefault);
							}

protected:
	virtual rexResult Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const;
	virtual rexResult ConvertSubObject(const rexObject& object,rexObject*& newObject) const;
	virtual rexResult ConvertSubObject(INode* p_Node,rexObject*& newObject) const;
	virtual bool IsAcceptableHierarchyNode(INode* p_Node) const;

	mutable rexObjectHierarchy* mp_Hier;
	bool m_IsForSkeleton;
	bool m_SkinOffset;
	bool m_ExportWorldSpace;
	bool m_OptimiseMaterials;
	bool m_LimitToSpecFileBones;
	bool m_UseWeightedNormals;
	int	 m_CullVertexChannels;
	int	 m_ForcedAdditionalUVCount;

	atString m_CustomVertexColour;

	atArray<atString>	m_BlindDataNames;
	atArray<int>		m_BlindDataVDataChnIds;
	atArray<int>		m_BlindDataRuntimeIds;
	atArray<float>		m_BlindDataDefaults;
};

} //namespace rage {

#endif //__REX_MAX_ROCKSTAR_CONVERTERMESH_H__
