Project rexMaxToGeneric
Template max_configurations.xml

ConfigurationType library

ForceInclude ForceInclude.h

IncludePath $(RAGE_DIR)\stlport\STLport-5.0RC5\src
IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\tools\dcc\libs
IncludePath $(RAGE_DIR)\base\tools\dcc\max
IncludePath $(RAGE_DIR)\framework\tools\src\dcc\libs
IncludePath $(RAGE_DIR)\base\tools\libs

Files {
	ConverterAnimation.cpp
	ConverterAnimation.h
	ConverterBasic.cpp
	ConverterBasic.h
	ConverterBoundHierarchy.cpp
	ConverterBoundHierarchy.h
	ConverterCloth.cpp
	ConverterCloth.h
	ConverterDrawableMesh.h
	ConverterEdgeModel.cpp
	ConverterEdgeModel.h
	ConverterEntity.cpp
	ConverterEntity.h
	ConverterFragment.cpp
	ConverterFragment.h
	ConverterLocator.cpp
	ConverterLocator.h
	ConverterMesh.cpp
	ConverterMesh.h
	ConverterParticle.cpp
	ConverterParticle.h
	ConverterSkeleton.cpp
	ConverterSkeleton.h
	FilterBoneRoot.h
	FilterBoundHierarchy.h
	FilterLocator.h
	FilterMeshHierarchy.h
	FilterParticle.h
	ForceInclude.h
	ProcessorMeshCombiner.cpp
	ProcessorMeshCombiner.h
	PropertyBasic.cpp
	PropertyBasic.h
}