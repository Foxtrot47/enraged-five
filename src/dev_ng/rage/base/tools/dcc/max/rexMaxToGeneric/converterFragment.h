#ifndef __REX_MAX_ROCKSTAR_CONVERTERFRAGMENT_H__
#define __REX_MAX_ROCKSTAR_CONVERTERFRAGMENT_H__

#include "atl/array.h"
#include "rexBase/converter.h"
#include "rexBase/property.h"

namespace rage {

class rexObjectGenericFragmentHierarchy;

/* PURPOSE:
property to turn on new style fragment exporting
*/
class rexPropertyMaxFragmentNewStyle : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxFragmentNewStyle; }
};

/* PURPOSE:
property to allow marking nodes in the fragment hierarchy that effect breakable glass
*/
class rexPropertyMaxFragmentBreakableGlassNode : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxFragmentBreakableGlassNode; }
};

/* PURPOSE:
	converts from max data to the rex generic fragment format
*/
class rexConverterMaxFragment : public rexConverter
{
public:
	rexConverterMaxFragment(): m_NewStyleExport(false) {}

	virtual rexResult Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const;
	virtual rexConverter* CreateNew() const { return new rexConverterMaxFragment; }

	void SetNewStyleExport(bool On) { m_NewStyleExport = On; }
	void AppendBreakableGlassNode( INode* p_Node ) { m_breakableGlassNodes.PushAndGrow( p_Node ); }

protected:
	void ConvertRec(INode* p_Node,rexObjectGenericFragmentHierarchy* p_Root,const atString& Path) const;
	void ConvertRecNewStyle(INode* p_Node,rexObjectGenericFragmentHierarchy* p_Root,const atString& Path) const;

	bool m_NewStyleExport;

	atArray<INode*>	m_breakableGlassNodes;
};

} //namespace rage {

#endif //__REX_MAX_ROCKSTAR_CONVERTERFRAGMENT_H__
