#include "rexMax/object.h"
#include "rexmaxutility/utility.h"
#include "rexmaxutility/maxobj.h"
#include "rexMaxToGeneric/converterBoundHierarchy.h"
#if HACK_GTA4
// Required for use of rexProcessorMaxBoundCombiner
#include "rexMaxToGeneric/processorMeshCombiner.h"
#endif // HACK_GTA4
#include "rexRage/serializerBound.h"
#include "rageMaxLODHierarchy/HierarchyInterface.h"

using namespace rage;

enum
{
	BUFFER_LENGTH = 4096
};

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundSurfaceType::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexResult Result(rexResult::PERFECT);

	rexConverterMaxBoundHierarchy* conv = dynamic_cast<rexConverterMaxBoundHierarchy*>(module.m_Converter);

	if(!conv)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	char Item[BUFFER_LENGTH];
	const char* p_BoundList = value.toString();
	const char* p_End;
	const char* p_SurfaceType;
	s32 Length;

	while(*p_BoundList)
	{
		p_End = strchr(p_BoundList,':');

		if(p_End)
		{
			Length = p_End - p_BoundList;
		}
		else
		{
			Length = (int)strlen(p_BoundList);
		}

		if(Length >= BUFFER_LENGTH)
		{
			return Result;
		}

		Item[Length] = 0;

		strncpy(Item,p_BoundList,Length);

		char* p_Equals = strchr(Item,'=');

		if(p_Equals)
		{
			p_SurfaceType = p_Equals + 1;
			*p_Equals = '\0';
		}
		else
		{
			return Result;
		}

		//m_BoundsSurface.Insert(Item,p_SurfaceType);
		conv->AddSurfaceType(atString(Item),atString(p_SurfaceType));

		p_BoundList += Length + 1;
	}

	return Result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxBoundSetGeometryBoundsExportAsBvhGeom::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>(module.m_Serializer);
#if HACK_GTA4
	// Used to set export as BVH
	rexConverterMaxBoundHierarchy* conv = dynamic_cast<rexConverterMaxBoundHierarchy*>(module.m_Converter);

	if(!ser || !conv)
#else
	if(!ser)
#endif // HACK_GTA4
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	ser->SetGeometryBoundsExportAsBvhGeom(value.toBool( ser->GetGeometryBoundsExportAsBvhGeom()));
#if HACK_GTA4
	conv->SetExportAsBVH(value.toBool());
#endif // HACK_GTA4

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxBoundSetGeometryBoundsExportAsBvhGrid::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>(module.m_Serializer);
#if HACK_GTA4
	// Used to set export as BVH
	rexConverterMaxBoundHierarchy* conv = dynamic_cast<rexConverterMaxBoundHierarchy*>(module.m_Converter);

	if(!ser || !conv)
#else
	if(!ser)
#endif // HACK_GTA4
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	ser->SetGeometryBoundsExportAsBvhGrid(value.toBool( ser->GetGeometryBoundsExportAsBvhGrid()));
#if HACK_GTA4
	conv->SetExportAsBVH(value.toBool());
#endif // HACK_GTA4

	return rexResult(rexResult::PERFECT);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundExportAsComposite::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexConverterMaxBoundHierarchy* conv = dynamic_cast<rexConverterMaxBoundHierarchy*>(module.m_Converter);
#if HACK_GTA4
	// Set additional SetExportAsComposite flag for all processors
	// I dont see any reason not to return on !conv so it can be added on next integration
	if(conv)
		conv->SetExportAsComposite(value.toBool());

	s32 i,Count = module.m_Processors.GetCount();

	for(i=0;i<Count;i++)
	{
		rexProcessorMaxBoundCombiner* proc = dynamic_cast<rexProcessorMaxBoundCombiner*>(module.m_Processors[i]);

		if(proc)
			proc->SetExportAsComposite(value.toBool());	
	}
#else
	if(!conv)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	conv->SetExportAsComposite(value.toBool());
#endif // HACK_GTA4

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundForceExportAsComposite::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexConverterMaxBoundHierarchy* conv = dynamic_cast<rexConverterMaxBoundHierarchy*>(module.m_Converter);

	if(!conv)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	conv->SetForceExportAsComposite(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundExportWorldSpace::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexConverterMaxBoundHierarchy* conv = dynamic_cast<rexConverterMaxBoundHierarchy*>(module.m_Converter);

	if(!conv)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	conv->SetExportWorldSpace(value.toBool());

	return rexResult(rexResult::PERFECT);
}

#if HACK_GTA4
////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundExportObjectSpace::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexConverterMaxBoundHierarchy* conv = dynamic_cast<rexConverterMaxBoundHierarchy*>(module.m_Converter);

	if(!conv)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	conv->SetExportObjectSpace(value.toBool());

	return rexResult(rexResult::PERFECT);
}
////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundExportMiloSpace::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexConverterMaxBoundHierarchy* conv = dynamic_cast<rexConverterMaxBoundHierarchy*>(module.m_Converter);

	if(!conv)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	conv->SetExportMiloSpace(value.toBool());

	return rexResult(rexResult::PERFECT);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundHasMeshTint::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexConverterMaxBoundHierarchy* conv = dynamic_cast< rexConverterMaxBoundHierarchy* >( module.m_Converter );

	if(conv)
	{
		conv->SetHasMeshTint(value.toBool());
	}

	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

}
#endif // HACK_GTA4
////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxBoundZeroPrimitiveCentroids::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>(module.m_Serializer);

	if(!ser)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	ser->SetZeroPrimitiveCentroids(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundOctreeMaxHeight::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundOctreeMaxPerCell::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundUseFiltering::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>(module.m_Serializer);

	if(!ser)
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	ser->SetUseBoundFiltering(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundSetToNodeName::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexConverterMaxBoundHierarchy* conv = dynamic_cast<rexConverterMaxBoundHierarchy*>(module.m_Converter);

	if(!conv)
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	conv->SetBoundToNodeName(value.toBool());

	return rexResult(rexResult::PERFECT);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexConverterMaxBoundHierarchy::rexConverterMaxBoundHierarchy():
	m_ExportAsComposite(false),
	m_ForceExportAsComposite(false),
	m_ExportAsOctree(false),
#if HACK_GTA4
	m_ExportAsBVH(false),
#endif // HACK_GTA4
	m_ExportWorldSpace(true),
#if HACK_GTA4
	m_ExportObjectSpace(false),
	m_HasMeshTint(false),
#endif // HACK_GTA4
	m_ToNodeName(true),
	m_ExportMiloSpace(false)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxBoundHierarchy::AddSurfaceType(const atString& r_ObjectName,const atString& r_SurfaceType)
{
	m_BoundsSurface.Insert(r_ObjectName,r_SurfaceType);
}

////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxBoundHierarchy::GetSurfaceType(const atString& r_BoundName,atString& r_SurfaceType) const
{
	const atString* p_SurfaceType = m_BoundsSurface.Access(r_BoundName);

	if(!p_SurfaceType)
	{
		r_SurfaceType = atString("default");
		return;
	}

	r_SurfaceType = *p_SurfaceType;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxBoundHierarchy::Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const
{
	rexResult result(rexResult::PERFECT);

	// get module-specific scene data here and fill out data structures as necessary
	int inputObjectCount = inputObjectArray.GetCount();

	// create object 
	rexObjectHierarchy* hier = new rexObjectGenericBoundHierarchy;
	hier->SetName(rexMaxUtility::ReplaceWhitespaceCharacters(defaultName));
	Assert(hier);

	// root composite object
	rexObject* root;
	rexObjectGenericBound* bnd = NULL;

	if(	m_ForceExportAsComposite || (m_ExportAsComposite && (inputObjectCount > 1)))
	{
		m_CompositeExport = true;
	}
	else
	{
		m_CompositeExport = false;
	}

	if(m_ForceExportAsComposite && (inputObjectCount == 1))
	{
		bnd = new rexObjectGenericBound();
		bnd->SetMeshName(rexMaxUtility::ReplaceWhitespaceCharacters(defaultName));

		bnd->m_ParentBoneMatrix.Identity();
		bnd->m_Type = phBound::SPHERE;
		bnd->m_Radius = 0.0f;
		bnd->m_Matrix.Identity();
		bnd->m_Center = Vector3(0.0f,0.0f,0.0f);

		hier->m_ContainedObjects.PushAndGrow(bnd,(u16)1);
		root = bnd;
	}
	else
	{
		root = hier;
	}

	if(m_ProgressBarObjectCountCB)
		(*m_ProgressBarObjectCountCB)(inputObjectCount);

	for(int a=0;a<inputObjectCount;a++)
	{		
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)(a);

		rexObjectGenericBound* subObject = NULL;
		rexResult thisResult = ConvertSubObject(*inputObjectArray[a],subObject);
		
		result.Combine(thisResult);
		
		if(thisResult.OK() && subObject)
		{
			root->m_ContainedObjects.PushAndGrow(subObject,(u16)inputObjectCount);
#if HACK_GTA4
			// Additional or on m_ExportAsBVH
			if(m_ToNodeName && (m_CompositeExport || m_ExportAsOctree || m_ExportAsBVH || (inputObjectCount == 1)))
#else
			if(m_ToNodeName && (m_CompositeExport || m_ExportAsOctree || (inputObjectCount == 1)))
#endif // HACK_GTA4
			{
				subObject->SetMeshName(rexMaxUtility::ReplaceWhitespaceCharacters(defaultName));

				if(a == 0)
				{
					if(bnd && subObject)
					{
						bnd->m_FullPath = subObject->m_FullPath;
					}
#if HACK_GTA4
					// Additional or on !m_ExportAsBVH
					if(!m_ExportAsOctree && !m_ExportAsBVH)
#else
					if(!m_ExportAsOctree)
#endif // HACK_GTA4
						root = subObject;
				}
			}
		}
	}

	// set up return values
	outputObject = hier;
	
	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxBoundHierarchy::ConvertSubObject(const rexObject& object,rexObjectGenericBound*& newObject) const
{
	rexResult result(rexResult::PERFECT);
	const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>( &object );

	if(!p_Obj)
	{
		result.Set(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);
		return result;
	}

	INode* p_Node = p_Obj->GetMaxNode();

	if(m_ProgressBarTextCB)
	{
		static char message[1024];
		sprintf(message,"Converting bounds data at %s",p_Node->GetName());
		(*m_ProgressBarTextCB)(message);
	}

	rexMaxUtility::SetSkinBindPose(p_Node,true);

	if(IsAcceptableHierarchyNode(p_Node))
	{
		result &= ConvertMaxNode(p_Node,newObject,true);
	}

	rexMaxUtility::SetSkinBindPose(p_Node,false);

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////
bool rexConverterMaxBoundHierarchy::IsAcceptableHierarchyNode(INode* p_Node) const
{
	return true;
}

static bool s_bMapTemplateLoaded = false;

static void HELPER_SetMatrices(INodePtr p_Node, rexObjectGenericBound *bnd, bool exportWorldSpace, bool exportMiloSpace)
{
	rexMaxUtility::CopyMaxMatrixToRageMatrix(p_Node->GetNodeTM(0),bnd->m_Matrix);
	bnd->m_ParentBoneMatrix.Identity();

	if(!exportWorldSpace)
	{
		INodePtr parentNode = p_Node->GetParentNode();
		if(parentNode)
			rexMaxUtility::CopyMaxMatrixToRageMatrix(parentNode->GetNodeTM(0),bnd->m_ParentBoneMatrix);
	}
	if(exportMiloSpace)
	{
		rexMaxUtility::CopyMaxMatrixToRageMatrix(p_Node->GetObjectTM(0),bnd->m_Matrix);

		INode* p_RootNode = GetCOREInterface()->GetRootNode();
		INode* p_OctreeRootParent = p_Node;

		while(p_OctreeRootParent->GetParentNode() != p_RootNode)
		{
			p_OctreeRootParent = p_OctreeRootParent->GetParentNode();
		}
		DebugPrint("Octree milo parent name: %s\n", p_OctreeRootParent->GetName());
		rexMaxUtility::CopyMaxMatrixToRageMatrix(p_OctreeRootParent->GetNodeTM(0),bnd->m_ParentBoneMatrix);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxBoundHierarchy::ConvertMaxNode(INode* p_Node,rexObjectGenericBound*& newObject,bool Root) const
{
	rexResult retval(rexResult::PERFECT);
	rexObjectGenericBound *bnd = new rexObjectGenericBound();
	atString SurfaceType;

	//nj_01 on standard setting (5000 poly size) resulted in 126 files at 30mb
	//changing MaxPerCell to 110 resulted in 27mb

	//nj_01 on standard setting (10000 poly size) resulted in 55 files at 30mb
	//changing MaxPerCell to 110 resulted in 27mb

	GetSurfaceType(atString(p_Node->GetName()),SurfaceType);
#if HACK_GTA4
	// Marked for deletion
	//s32 RoomID = GetRoomID(p_Node->GetName()); 
#endif // HACK_GTA4

	bnd->SetMeshName(rexMaxUtility::ReplaceWhitespaceCharacters(p_Node->GetName()));
	bnd->m_ParentBoneMatrix.Identity();

	BOOL b = false;

	if(!p_Node->GetUserPropBool("weapons", b) || (p_Node->GetUserPropBool("weapons", b) && b))
	{
		bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_WEAPONS;
	}

	if(!p_Node->GetUserPropBool("mover", b) || (p_Node->GetUserPropBool("mover", b) && b))
	{
		bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_MOVER;
	}

	if(p_Node->GetUserPropBool("river", b) && b)
	{
		bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_RIVER;
	}

	if(p_Node->GetUserPropBool("foliage", b) && b)
	{
		bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_FOLIAGE;
	}

	if(p_Node->GetUserPropBool("stair_slope", b) && b)
	{
		bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_STAIR_SLOPE;
	}

	if(p_Node->GetUserPropBool("material", b) && b)
	{
		bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_MATERIAL_ONLY;
	}
	

	// OK, so the horse, cover and vehicle flags are a bit special.
	// They were added after others so to prevent migration issues they are only implied if 'mover' is on
	if( (bnd->m_surfaceFlags & rexConverterMaxBoundHierarchy::COLLISION_MOVER) == rexConverterMaxBoundHierarchy::COLLISION_MOVER )
	{
		if (!p_Node->GetUserPropBool("horse", b) || (p_Node->GetUserPropBool("horse", b) && b))
		{
			bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_HORSE;
		}
		if (!p_Node->GetUserPropBool("cover", b) || (p_Node->GetUserPropBool("cover", b) && b))
		{
			bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_COVER;
		}
		if (!p_Node->GetUserPropBool("vehicle", b) || (p_Node->GetUserPropBool("vehicle", b) && b))
		{
			bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_VEHICLE;
		}
	}
	else
	{
		if(p_Node->GetUserPropBool("horse", b) && b)
		{
			bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_HORSE;
		}
		if(p_Node->GetUserPropBool("cover", b) && b)
		{
			bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_COVER;
		}
		if(p_Node->GetUserPropBool("vehicle", b) && b)
		{
			bnd->m_surfaceFlags |= rexConverterMaxBoundHierarchy::COLLISION_VEHICLE;
		}
	}

	if(!p_Node->GetParentNode())
	{
		retval.AddError("Collision node doesn't have parent!", p_Node->GetName());
		retval.Combine(rexResult::ERRORS);
	}
	else
		bnd->m_ParentBoneName = p_Node->GetParentNode()->GetName();

	Object* p_Object = p_Node->GetObjectRef();
#if HACK_GTA4
	// You can't query a GTA_WHEEL collision object for it's param block since it's really
	// just a grid
	IParamBlock2* p_ParamBlock = NULL;
	if(p_Object->ClassID() != GTAWHEEL_CLASS_ID)
		p_ParamBlock = ((Animatable*)p_Object)->GetParamBlock(0);
#else
	IParamBlock2* p_ParamBlock = ((Animatable*)p_Object)->GetParamBlock(0);
#endif


	IFpSceneHierarchy* pRsSceneLink = GetSceneLinkInterface();
	if(pRsSceneLink && NULL!=pRsSceneLink->GetParent(eLinkType_CLOTHCOLLISION, p_Node))
	{
		bnd->m_IsClothCollision = true;
	}

	if(p_Object->ClassID() == COLLISION_BOX_CLASS_ID)
	{

#if HACK_GTA4
		if(m_ExportAsOctree || m_ExportAsBVH && !m_ExportPrimsInBvh)
#else
		if(m_ExportAsOctree)
#endif //HACK_GTA4
		{
			rexMaxUtility::ConvertCollisionBoxToRageMesh(p_Node,bnd,SurfaceType,m_ExportWorldSpace);
		}
		else
		{
			bnd->m_Type = phBound::BOX;
			
			if(p_ParamBlock)
			{
				p_ParamBlock->GetValue(rsobjpb_colbox_length,0,bnd->m_Width,FOREVER);
				p_ParamBlock->GetValue(rsobjpb_colbox_width,0,bnd->m_Height,FOREVER);
				p_ParamBlock->GetValue(rsobjpb_colbox_height,0,bnd->m_Depth,FOREVER);
			}

			bnd->m_Depth *= 2.0f;
			bnd->m_Width *= 2.0f;
			bnd->m_Height *= 2.0f;

			HELPER_SetMatrices(p_Node, bnd, m_ExportWorldSpace, m_ExportMiloSpace);

			if(m_ExportMiloSpace || !m_ExportAsBVH || m_ExportObjectSpace)
				bnd->m_Matrix.DotTranspose(bnd->m_ParentBoneMatrix);

			bnd->m_ParentBoneMatrix.Identity();

			if(m_CompositeExport || m_ForceExportAsComposite)
			{
				bnd->m_ParentBoneMatrix = bnd->m_Matrix;
			}

			bnd->m_Center.Zero();

			rexObjectGenericMesh::MaterialInfo& r_MatInfo = bnd->m_Materials.Grow();

			r_MatInfo.m_Name = ":";
			r_MatInfo.m_Name += SurfaceType;
			r_MatInfo.m_TypeName = "rage_default";
		}
	}
	else if(p_Object->ClassID() == COLLISION_CAPSULE_CLASS_ID)
	{
		bnd->m_Type = phBound::CAPSULE;
	
		if(p_ParamBlock)
		{
			p_ParamBlock->GetValue(rsobjpb_colcapsule_radius,0,bnd->m_Radius,FOREVER);
			p_ParamBlock->GetValue(rsobjpb_colcapsule_length,0,bnd->m_Height,FOREVER);
		}

		HELPER_SetMatrices(p_Node, bnd, m_ExportWorldSpace, m_ExportMiloSpace);

		if(m_ExportMiloSpace || !m_ExportAsBVH || m_ExportObjectSpace)
			bnd->m_Matrix.DotTranspose(bnd->m_ParentBoneMatrix);

		bnd->m_ParentBoneMatrix.Identity();

		if(m_CompositeExport || m_ForceExportAsComposite)
		{
			bnd->m_ParentBoneMatrix = bnd->m_Matrix;
		}

		rexObjectGenericMesh::MaterialInfo& r_MatInfo = bnd->m_Materials.Grow();

		r_MatInfo.m_Name = ":";
		r_MatInfo.m_Name += SurfaceType;
		r_MatInfo.m_TypeName = "rage_default";
	}
	else if(p_Object->ClassID() == COLLISION_SPHERE_CLASS_ID)
	{
		bnd->m_Type = phBound::SPHERE;

		if(p_ParamBlock)
		{
			p_ParamBlock->GetValue(rsobjpb_colsphere_radius,0,bnd->m_Radius,FOREVER);
		}

		HELPER_SetMatrices(p_Node, bnd, m_ExportWorldSpace, m_ExportMiloSpace);

		if(m_ExportMiloSpace || !m_ExportAsBVH || m_ExportObjectSpace)
			bnd->m_Matrix.DotTranspose(bnd->m_ParentBoneMatrix);

		bnd->m_ParentBoneMatrix.Identity();

		if(m_CompositeExport || m_ForceExportAsComposite)
		{
			bnd->m_ParentBoneMatrix = bnd->m_Matrix;
		}

		rexObjectGenericMesh::MaterialInfo& r_MatInfo = bnd->m_Materials.Grow();

		r_MatInfo.m_Name = ":";
		r_MatInfo.m_Name += SurfaceType;
		r_MatInfo.m_TypeName = "rage_default";
	}
	else if(p_Object->ClassID() == COLLISION_CYLINDER_CLASS_ID)
	{
		bnd->m_Type = phBound::CYLINDER;

		if(p_ParamBlock)
		{
			p_ParamBlock->GetValue(rsobjpb_colcylinder_radius,0,bnd->m_Radius,FOREVER);
			bnd->m_Radius2 = bnd->m_Radius;		// NOTE: The cylinder shape in Max does not currently support dual radii.

			p_ParamBlock->GetValue(rsobjpb_colcylinder_length,0,bnd->m_Height,FOREVER);
			p_ParamBlock->GetValue(rsobjpb_colcylinder_capcurve,0,bnd->m_CapCurve,FOREVER);
			p_ParamBlock->GetValue(rsobjpb_colcylinder_sidecurve,0,bnd->m_SideCurve,FOREVER);
		}

		HELPER_SetMatrices(p_Node, bnd, m_ExportWorldSpace, m_ExportMiloSpace);

		if(m_ExportMiloSpace || !m_ExportAsBVH || m_ExportObjectSpace)
			bnd->m_Matrix.DotTranspose(bnd->m_ParentBoneMatrix);

		bnd->m_ParentBoneMatrix.Identity();

		if(m_CompositeExport || m_ForceExportAsComposite)
		{
			bnd->m_ParentBoneMatrix = bnd->m_Matrix;
		}

		rexObjectGenericMesh::MaterialInfo& r_MatInfo = bnd->m_Materials.Grow();

		r_MatInfo.m_Name = ":";
		r_MatInfo.m_Name += SurfaceType;
		r_MatInfo.m_TypeName = "rage_default";
	}
	else if(p_Object->ClassID() == COLLISION_DISC_CLASS_ID)
	{
		bnd->m_Type = phBound::DISC;

		if(p_ParamBlock)
		{
			p_ParamBlock->GetValue(rsobjpb_coldisc_radius,0,bnd->m_Radius,FOREVER);
			p_ParamBlock->GetValue(rsobjpb_coldisc_length,0,bnd->m_Width,FOREVER);
		}

		HELPER_SetMatrices(p_Node, bnd, m_ExportWorldSpace, m_ExportMiloSpace);

		if(m_ExportMiloSpace || !m_ExportAsBVH || m_ExportObjectSpace)
			bnd->m_Matrix.DotTranspose(bnd->m_ParentBoneMatrix);

		bnd->m_ParentBoneMatrix.Identity();

		if(m_CompositeExport || m_ForceExportAsComposite)
		{
			bnd->m_ParentBoneMatrix = bnd->m_Matrix;
		}

		rexObjectGenericMesh::MaterialInfo& r_MatInfo = bnd->m_Materials.Grow();

		r_MatInfo.m_Name = ":";
		r_MatInfo.m_Name += SurfaceType;
		r_MatInfo.m_TypeName = "rage_default";
	}
	else if(p_Object->ClassID() == COLLISION_MESH_CLASS_ID)
	{
		bnd->m_Type = phBound::GEOMETRY;

		HELPER_SetMatrices(p_Node, bnd, m_ExportWorldSpace, m_ExportMiloSpace);

		// ConvertMaxMeshNodeIntoGenericMesh has additional parameters in our branch
		if(!rexMaxUtility::ConvertMaxMeshNodeIntoGenericMesh(p_Node,bnd,retval,0,true,m_ExportWorldSpace,true,false,m_ExportObjectSpace,SurfaceType,0,true,0,atString(""),m_HasMeshTint))
		{
			delete bnd;
			return retval;
		}
		else
		{
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			bnd->m_colourTemplate = rexMaxUtility::GetColourTemplate();		
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
			//had to remove this line with latest rage as it seems to have changed...
			//rexMaxUtility::GetLocalMatrix(p_Node,0.0f,bnd->m_Matrix);
			bnd->m_Matrix.Identity();

			if(m_CompositeExport)
			{
				bnd->m_ParentBoneMatrix = bnd->m_Matrix;
			}

			//Check if the user has set a specific margin for the bullet collision system
			float userMargin;
			if(rexMaxUtility::GetBoundMargin(p_Node, userMargin))
				bnd->m_Margin = userMargin;
		}
	}
#if HACK_GTA4
	// Instanced wheel hack, see comment below
	else if(p_Object->ClassID() == GTAWHEEL_CLASS_ID)
	{
		//nasty hack as we need a bound to appear for the wheels in order for the fragment
		//heirarchy to come out

		bnd->m_Type = phBound::SPHERE;
		bnd->m_Radius = 1.0f;

		//bnd->m_Matrix.DotTranspose(bnd->m_ParentBoneMatrix);
		bnd->m_Matrix.Identity();
		bnd->m_ParentBoneMatrix.Identity();

		//if(m_CompositeExport || m_ForceExportAsComposite)
		{
			bnd->m_ParentBoneMatrix = bnd->m_Matrix;
		}

		rexObjectGenericMesh::MaterialInfo& r_MatInfo = bnd->m_Materials.Grow();

		r_MatInfo.m_Name = ":";
		r_MatInfo.m_Name += SurfaceType;
		r_MatInfo.m_TypeName = "rage_default";
	}
#endif // HACK_GTA4
	else
	{
		bnd->m_Type = phBound::GEOMETRY;
		// ConvertMaxMeshNodeIntoGenericMesh has additional parameters in our branch
		if(!rexMaxUtility::ConvertMaxMeshNodeIntoGenericMesh(p_Node,bnd,retval,0,true,m_ExportWorldSpace,true,false,m_ExportObjectSpace,SurfaceType,0,true,0))
		{
			delete bnd;
			return retval;
		}

		HELPER_SetMatrices(p_Node, bnd, m_ExportWorldSpace, m_ExportMiloSpace);

		//Check if the user has set a specific margin for the bullet collision system
		float userMargin;
		if(rexMaxUtility::GetBoundMargin(p_Node, userMargin))
			bnd->m_Margin = userMargin;
	}

	if(bnd->IsValidMesh() && bnd->GetTriCount()>100000)
	{
		retval.Combine(rexResult::ERRORS);
		retval.AddErrorCtx(p_Node->GetName(),"The collision mesh %s has over 100,000 faces. Please reduce.", p_Node->GetName());
	}

	rexMaxUtility::GetNodeFullPath(p_Node,bnd->m_FullPath);

	atString Out("exporting bounds: ");
	Out += p_Node->GetName();
	retval.AddMessageCtx(p_Node->GetName(), Out);

	newObject = bnd;
	return retval;
}
