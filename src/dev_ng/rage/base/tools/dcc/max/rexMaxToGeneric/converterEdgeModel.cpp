#include "MaxUtil/MaxUtil.h"
#include "rexMax/object.h"
#include "rexMaxUtility/utility.h"
#include "rexMaxToGeneric/converterEdgeModel.h"
#include "rexGeneric/objectEdgeModel.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexConverterMaxEdgeModelHierarchy::rexConverterMaxEdgeModelHierarchy()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxEdgeModelHierarchy::Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const
{
	rexResult result(rexResult::PERFECT);

	// create object 
	mp_Hier = new rexObjectGenericEdgeModelHierarchy;

	Assert(mp_Hier);

	mp_Hier->SetName(rexMaxUtility::ReplaceWhitespaceCharacters(defaultName));

	// get module-specific scene data here and fill out data structures as necessary
	int inputObjectCount = inputObjectArray.GetCount();

	if(m_ProgressBarObjectCountCB)
		(*m_ProgressBarObjectCountCB)(inputObjectCount);

	for(int a=0;a<inputObjectCount;a++)
	{		
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)(a);

		rexObject* subObject = NULL;
		rexResult thisResult = ConvertSubObject(*inputObjectArray[a],subObject);
		
		result.Combine(thisResult);
	}

	// set up return values
	outputObject = mp_Hier;
	
	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxEdgeModelHierarchy::ConvertSubObject(const rexObject& object,rexObject*& newObject) const
{
	rexResult result(rexResult::PERFECT);
	const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>( &object );

	if(!p_Obj)
	{
		result.Set(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);
		return result;
	}

	if(m_ProgressBarTextCB)
	{
		static char message[1024];
		sprintf(message,"Converting mesh data at %s",p_Obj->GetMaxNode()->GetName());
		(*m_ProgressBarTextCB)(message);
	}

	INode* p_Node = p_Obj->GetMaxNode();

	return ConvertSubObject(p_Node,newObject);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxEdgeModelHierarchy::ConvertSubObject(INode* p_Node,rexObject*& newObject) const
{
	rexResult result(rexResult::PERFECT);

	atString Out("exporting mesh: ");
	Out += p_Node->GetName();
	result.AddMessageCtx(p_Node->GetName(), Out);

	INode* p_RootBone = MaxUtil::GetSkinRootBone(MaxUtil::GetSkin(p_Node));

	if(!IsAcceptableHierarchyNode(p_Node))
	{
		return result;
	}

	rexMaxUtility::SetSkinBindPose(p_RootBone,true);

	rexObjectGenericMesh* mesh = new rexObjectGenericMesh();

	if(!mesh)
		return rexResult(rexResult::ERRORS,rexResult::OUT_OF_MEMORY);

	rexMaxUtility::ConvertMaxMeshNodeIntoGenericMesh(	p_Node,
														mesh,
														result,
														mp_Hier->m_ContainedObjects.GetCount(),
														false,
														false,
														true,
														false);

	newObject = mesh;

	mp_Hier->m_ContainedObjects.PushAndGrow(newObject,mp_Hier->m_ContainedObjects.GetCount() + 1);

	rexMaxUtility::SetSkinBindPose(p_RootBone,false);

	s32 i,ChildCount = p_Node->NumberOfChildren();

	for(i=0;i<ChildCount;i++)
	{
		INode* p_ChildNode = p_Node->GetChildNode(i);

		rexObject* subObject = NULL;
		ConvertSubObject(p_ChildNode,subObject);
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexConverterMaxEdgeModelHierarchy::IsAcceptableHierarchyNode(INode* p_Node) const
{
	return rexMaxUtility::IsType(p_Node,Class_ID(TRIOBJ_CLASS_ID,0));
}
