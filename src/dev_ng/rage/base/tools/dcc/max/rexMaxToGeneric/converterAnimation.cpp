#include "rexmaxutility/utility.h"
#include "rexMaxUtility/maxMorpher.h"
#include "rexMaxToGeneric/converterAnimation.h"
#include "rexMaxToGeneric/converterSkeleton.h"
#include "rexGeneric/objectAnimation.h"
#include "rexBase/module.h"
#include "rexRage/serializerAnimation.h"
#include "vectormath/legacyconvert.h"
#include <decomp.h>

using namespace rage;

static const s32 TicksPerSec = 4800;
#define VISIBILITY_TRACK_REFNUM 4
#define ANIMATION_TRANSFORM_LIMIT 500000

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyAnimMoverTrack::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetExportMoverTrack(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyAnimExtraMoverTrack::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetExtraMoverTrack(atString(value.toString()));

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyAnimExportSelected::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetExportSelected(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxAnimationMoverNode::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	if(objects.GetCount())
	{
		rexMaxObject* p_MaxObj = dynamic_cast<rexMaxObject*>(objects[0]);

		if(p_MaxObj)
			conv->SetMoverNode(p_MaxObj->GetMaxNode());
		else
			return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);
	}

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxAnimationMoveRotToRoot::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetExportMoveRotFromRoot(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxAnimationAuthoredOrient::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetAuthoredOrientation(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxAnimationCompressionTolerance::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexSerializerRAGEAnimation* ser = dynamic_cast<rexSerializerRAGEAnimation*>(module.m_Serializer);

	if( !ser )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	ser->SetCompressionErrorTolerance(value.toFloat());

	return rexResult( rexResult::PERFECT );
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxAnimationProjectFlags::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexSerializerRAGEAnimation* ser = dynamic_cast<rexSerializerRAGEAnimation*>(module.m_Serializer);

	if( !ser )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	ser->SetProjectFlags((u16)value.toInt());

	return rexResult( rexResult::PERFECT );
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxAnimationLocalSpace::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetExportLocalSpace(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxAnimationEndFrame::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetEndFrame(value.toInt());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxAnimationStartFrame::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetStartFrame(value.toInt());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxAnimationUvAnimIndex::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetExportUvIndex(value.toInt());

	return rexResult(rexResult::PERFECT);
}
////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMaxAnimationMorphWeightTracks::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetMorphWeightTracks(atString(value.toString()));

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyAnimationExprControlNodes::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	conv->SetExprControlNodes(atString(value.toString()));

	return rexResult(rexResult::PERFECT);
}
////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyAnimationExprControlNodesFile::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxAnimation* conv = dynamic_cast<rexConverterMaxAnimation*>(module.m_Converter);

	if(!conv)
		return rexResult(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);

	atString controlNodeFilePath(value.toString());
	if(!controlNodeFilePath.length())
	{
		rexResult result(rexResult::ERRORS);
		result.AddError("No filename specified for property 'AnimExpressionControlNodesFile'");
		return result;
	}

	fiStream* pStream = fiStream::Open((const char*)controlNodeFilePath);
	if(!pStream)
	{
		rexResult result(rexResult::ERRORS);
		result.AddError("Failed to open file '%s' specified in property'AnimExpressionControlNodesFile'", (const char*)controlNodeFilePath);
		return result;
	}

	char szBuffer[512];
	atString ctrlNodeString;

	while(fgetline(szBuffer, 512, pStream))
	{
		if(strlen(szBuffer))
		{
			ctrlNodeString += szBuffer;
			ctrlNodeString += ";";
		}
	}

	//Remove the last semicolon added when building up the string
	ctrlNodeString.Truncate(ctrlNodeString.length() - 1);

	conv->SetExprControlNodes(ctrlNodeString);

	pStream->Close();

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////
rexConverterMaxAnimation::rexConverterMaxAnimation():
	m_ExportAnimMoverTrack(true),
	m_ExportAnimSelected(false),
	m_ExportAnimMoveRotFromRoot(false),
	m_ExportCtrlSpec(false),
	m_IsCamera(false),
	m_ExportAnimLocalSpace(false),
	m_ExtraMoverTrack(""),
	m_MorphWeightTracks(""),
	mp_MoverNode(NULL),
	m_ExportUVAnimIndex(-1),
	m_OverrideEndFrame(false),
	m_OverrideStartFrame(false)
{
}

#if HACK_GTA4
// The generic functions were added for viseme export
////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::CalcGenericValuesFromNode(INode* p_Node,atArray<atArray<Vector3>>& FrameVectors) const
{
	
	Object* pObj = p_Node->GetObjectRef();
	IDerivedObject* p_derived = (IDerivedObject*)pObj;
	Animatable* p_Anim = (Animatable*)p_derived;
	float visemeValue;

	int numVisemeChannels = FrameVectors.size();
	int numFrames = (int)m_EndFrame - (int)m_StartFrame;
	for(int i=0; i<numVisemeChannels; i++)
	{
		FrameVectors[i].Grow(numFrames);
		FrameVectors[i].Resize(numFrames);
	}
	

	Interval keyFrames = p_Anim->GetTimeRange(TIMERANGE_ALL);
	//Modifier* p_Morpher;
	int numMorphs = p_derived->NumModifiers();
	for(int i=0; i<numMorphs; ++i)
	{
		Modifier* p_Morpher =  p_derived->GetModifier(i);
		Animatable* p_Anim = (Animatable*)p_Morpher;
		int numSubAnims = p_Anim->NumSubs();
		std::string morpher = p_Morpher->GetName();
		int frameVectorsVisemeIdx = 0;
		if(numSubAnims >= numVisemeChannels)
		{
			for (int j=0; j<numVisemeChannels+1; j++)
			{
				Animatable* p_SubAnim = p_Anim->SubAnim(j);
				if(p_SubAnim)
				{
					std::string subAnimname = p_Anim->SubAnimName(j);
					Class_ID classID = p_SubAnim->ClassID();
					BOOL isAnimated = p_SubAnim->IsAnimated();
					if(isAnimated)
					{
						
						//WStr sClassName;
						//p_SubAnim->GetClassNameA(sClassName);
						
						IParamBlock* paramBlock = (IParamBlock*)p_SubAnim;//->GetParamBlock(l);
						if(paramBlock)
						{
							int numParams = paramBlock->NumParams();
							for(int m=0; m<numParams; m++)
							{
								//ParamID thisParamID = paramBlock->IndextoID(m);
								TimeValue fTime;
								Interval intVal;
								intVal.SetEmpty();
								for(int p=(int)m_StartFrame; p<(int)m_EndFrame; ++p)
								{
									float time =((f32)p) / rexMaxUtility::GetFPS();
									fTime = (TimeValue)(time * TicksPerSec);
									
									BOOL success = paramBlock->GetValue(m, fTime, visemeValue, intVal);
									visemeValue = (float)visemeValue / 100.0f;
									if (visemeValue > 1.0f)
										visemeValue = 1.0f;
									FrameVectors[frameVectorsVisemeIdx][p] = Vector3(visemeValue, 0.0, 0.0);
										
								}
							}
							
						}
						++frameVectorsVisemeIdx;
					}
				}
			}
		}
	}
}
#endif // HACK_GTA4
////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxAnimation::CalcFrameMatricesFromNode(INode* p_Node,atArray<Matrix34*>& FrameMatrices,bool IsRoot,bool IsMover) const
{
	Matrix34 MatRemove;
	s32 i,NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;

	MatRemove.Identity();
	rexResult retVal = rexResult::PERFECT;

	if (NumFrames != 0)
	{
		FrameMatrices.Grow(NumFrames);
		FrameMatrices.Resize(NumFrames);
		for(int initIndex = 0;initIndex<NumFrames;initIndex++)
			FrameMatrices[initIndex] = new Matrix34();

		char* pName = p_Node->GetName();

		//take off very first matrix if we are in local space and the root

		if(m_ExportAnimLocalSpace && IsRoot)
		{
			rexMaxUtility::GetLocalMatrix(p_Node,0,MatRemove);
			MatRemove.Inverse();
		}
		float ticksPerFrame = (float)((float)rexMaxUtility::GetTicksPerSec() / rexMaxUtility::GetFPS());

		if(GetAuthoredOrientation())
		{
			for(i=(s32)m_StartFrame;i<(s32)m_EndFrame;i++)
			{
				u32 currentFrame = (u32)i - (u32)m_StartFrame;
				Matrix34 &Mat = *FrameMatrices[currentFrame];
				rexMaxUtility::GetLocalMatrix(p_Node,((f32)i) / rexMaxUtility::GetFPS(),Mat);
				Mat.DotFromLeft(MatRemove);

				if(Mat.d.Mag()>ANIMATION_TRANSFORM_LIMIT)
				{
					retVal.Combine(rexResult::ERRORS);
					retVal.AddErrorCtx(p_Node->GetName(),"Transform of node %s is over Magnitude of %d at frame %d.", p_Node->GetName(), ANIMATION_TRANSFORM_LIMIT, i);
				}

				if(m_IsCamera)
				{
					Matrix34 matRotCam;
					matRotCam.Identity();
					matRotCam.MakeRotateX(PI / -2.0f);

					Vector3 pos = Mat.d;
					Mat.d.Zero();
					Mat.DotFromLeft(matRotCam);
					Mat.d = pos;
				}
			}
		}
		else
		{
			//original bone positions...

			Matrix34 MatBone,MatBoneInv;
			Matrix34 MatBoneParent,MatBoneParentInv;
			Matrix34 PreDot,PostDot;

			rexMaxUtility::SetSkinBindPose(p_Node,true);

			MatBoneParent.Identity();
			MatBoneParentInv.Identity();

			INode* p_ParentNode = rexMaxUtility::GetParent(p_Node);
			while(p_ParentNode)
			{
				Matrix34 MatJoint;
				rexMaxUtility::GetLocalMatrix(p_ParentNode,0.0f,MatJoint);
				MatBoneParent.Dot3x3(MatJoint);

				p_ParentNode = rexMaxUtility::GetParent(p_ParentNode);
			}

			MatBoneParentInv = MatBoneParent;
			MatBoneParentInv.Inverse();

			rexMaxUtility::GetLocalMatrix(p_Node,0.0f,MatBone);

			MatBoneInv = MatBone;
			MatBoneInv.Inverse();

			PreDot = MatBoneParentInv;
			PreDot.d.Zero();
			PreDot.Dot(MatBoneInv);

			PostDot = MatBone;
			PostDot.d.Zero();
			PostDot.Dot(MatBoneParent);

			rexMaxUtility::SetSkinBindPose(p_Node,false);

			////////////////

			for(i=(s32)m_StartFrame;i<(s32)m_EndFrame;i++)
			{
				Matrix34 &Mat = *FrameMatrices[(u32)i - (u32)m_StartFrame];

				//get the local rotation of the bone at this time

				rexMaxUtility::GetLocalMatrix(p_Node,((f32)i) / rexMaxUtility::GetFPS(),Mat);
				//Mat.DotFromLeft(MatRemove);

				Matrix34 MatChange;
				MatChange.DotTranspose(Mat,MatBone);

				Vector3 VecMove = Mat.d;

				//sort out the rotations so they are relative to the bones	  
				
				MatChange.d.Zero();

				Mat.Dot(PreDot,MatChange);
				Mat.Dot(PostDot);

				if(Mat.d.Mag()>ANIMATION_TRANSFORM_LIMIT)
				{
					retVal.Combine(rexResult::ERRORS);
					retVal.AddErrorCtx(p_Node->GetName(),"Transform of node %s is over Magnitude of %d at frame %d.", p_Node->GetName(), ANIMATION_TRANSFORM_LIMIT, i);
				}

				Mat.d = VecMove;
			}
		}
	}
	return retVal;
}

#if HACK_GTA4
////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::GenericNodeToChunk(INode* p_Node,rexObjectGenericAnimation::ChunkInfo* p_Chunk, int NumTracks) const
{
	atArray<atArray<Vector3>> FrameVectors;
	
	FrameVectors.Grow(NumTracks);
	FrameVectors.Resize(NumTracks);

	CalcGenericValuesFromNode(p_Node,FrameVectors);

	atArray<atArray<Matrix34*>> FrameMatrices;

	int numFrames = (u32)m_EndFrame - (u32)m_StartFrame;
	FrameMatrices.Grow(NumTracks);
	FrameMatrices.Resize(NumTracks);

	for(int i=0; i<NumTracks; ++i)
	{
		//atArray<Matrix34> FrameMatrices;

		FrameMatrices[i].Grow(numFrames);
		FrameMatrices[i].Resize(numFrames);
		for(int initIndex = 0;initIndex<numFrames;initIndex++)
			FrameMatrices[i][initIndex] = new Matrix34();
		
		
		for(int j=0; j<numFrames; ++j)
		{
			Matrix34 tempMatrix;
			tempMatrix.Zero();
			tempMatrix.d = FrameVectors[i][j];
			(*FrameMatrices[i][j]) = tempMatrix;
		}
		//m_outChunkData.Insert(p_Chunk,FrameMatrices);
	}
	m_outGenericChunkData.Insert(p_Chunk,FrameMatrices);
}
#endif // HACK_GTA4
////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxAnimation::InnerNodeToChunk(INode* p_Node,rexObjectGenericAnimation::ChunkInfo* p_Chunk,bool IsRoot) const
{
	rexResult retVal = rexResult::PERFECT;
	atArray<Matrix34*> FrameMatrices;

	retVal.Combine(CalcFrameMatricesFromNode(p_Node,FrameMatrices,IsRoot));

	m_outChunkData.Insert(p_Chunk,FrameMatrices);
	return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::InnerNodeToTrack(INode* p_Node,atArray<rexObjectGenericAnimation::TrackInfo*>& tracks,bool IsRoot,bool IsMover) const
{
	atArray<Matrix34*> FrameMatrices;

	CalcFrameMatricesFromNode(p_Node,FrameMatrices,IsRoot,IsMover);

	AddNodeToTracks(FrameMatrices,(u32)m_StartFrame,(u32)m_EndFrame,tracks);
}

////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::GetInterpolationInfoRec(rexObjectGenericAnimation* mp_Animation,INode* p_Node) const
{
	rexObjectGenericAnimation::TrackInfoInt* p_TrackInfoF = new rexObjectGenericAnimation::TrackInfoInt;
	s32 i,NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;

	p_TrackInfoF->ResetKeys(NumFrames);

	for(i=0;i<NumFrames;i++)
	{
		p_TrackInfoF->AppendIntKey(0);
	}

	s32 TimeFactor = (s32)(rexMaxUtility::GetTicksPerSec() / rexMaxUtility::GetFPS());
	float Counter = 1.0f;

	Control* p_Control = p_Node->GetTMController()->GetPositionController();
	Class_ID classID = p_Control->ClassID();

	if(classID == Class_ID(0x118f7e02,0xffee238a))
	{
		Control* p_ControlX = (Control*)p_Control->SubAnim(0);
		Control* p_ControlY = (Control*)p_Control->SubAnim(1);
		Control* p_ControlZ = (Control*)p_Control->SubAnim(2);

		if(p_ControlX->ClassID() == Class_ID(HYBRIDINTERP_FLOAT_CLASS_ID, 0))
		{
			IKeyControl *ikeysX = GetKeyControlInterface(p_ControlX);
			IKeyControl *ikeysY = GetKeyControlInterface(p_ControlY);
			IKeyControl *ikeysZ = GetKeyControlInterface(p_ControlZ);
			int Count;
			IBezFloatKey key;

			Count = ikeysX->GetNumKeys();
			for(i=0;i<Count;i++)
			{
				ikeysX->GetKey(i, &key);

				if((GetInTanType(key.flags) == 2) || (GetOutTanType(key.flags) == 2))
					p_TrackInfoF->SetIntKey(((s32)(key.time / TimeFactor)) - (s32)m_StartFrame,1);
			}

			Count = ikeysY->GetNumKeys();
			for(i=0;i<Count;i++)
			{
				ikeysY->GetKey(i, &key);

				if((GetInTanType(key.flags) == 2) || (GetOutTanType(key.flags) == 2))
					p_TrackInfoF->SetIntKey(((s32)(key.time / TimeFactor)) - (s32)m_StartFrame,1);
			}

			Count = ikeysZ->GetNumKeys();
			for(i=0;i<Count;i++)
			{
				ikeysZ->GetKey(i, &key);

				if((GetInTanType(key.flags) == 2) || (GetOutTanType(key.flags) == 2))
					p_TrackInfoF->SetIntKey(((s32)(key.time / TimeFactor)) - (s32)m_StartFrame,1);
			}
		}
	}

	p_TrackInfoF->m_TrackID = rexObjectGenericAnimation::TRACK_INFO;
	p_TrackInfoF->m_Name = "projectData1";
	p_TrackInfoF->m_StartTime = 0.0f;
	p_TrackInfoF->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;

	rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1);
	rp_TrackInfo = p_TrackInfoF;
}


////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::GetUVAnimRec(rexObjectGenericAnimation* mp_Animation,INode* p_Node) const
{
	atArray<Texmap*> TexMapList;

	if(!rexMaxUtility::GetMainTexMapsFromMaxMeshNode(p_Node,TexMapList))
		return;

	Matrix3 matTrans,matTransBegin;
	s32 CurrentFrame = rexMaxUtility::GetAnimFrame();
	s32 i,j;

	for(i=0;i<TexMapList.GetCount();i++)
	{
		if(!TexMapList[i])
			continue;

		if(i != m_ExportUVAnimIndex)
			continue;
		
		///
#if HACK_GTA4
		// animation range is calculated in rexConverterMaxAnimation::Convert already
		s32 StartFrame = (s32)m_StartFrame;
		s32 EndFrame = (s32)m_EndFrame + 1;
		s32 NumFrames = (s32)EndFrame - (s32)StartFrame;
#else
		Control* p_ControlCoord = (Control*)TexMapList[i]->SubAnim(0);
		Control* p_ControlParam = (Control*)p_ControlCoord->SubAnim(0);
		Control* p_ControlU = (Control*)p_ControlParam->SubAnim(0);
		Control* p_ControlV = (Control*)p_ControlParam->SubAnim(1);

		Interval theIntervalU;
		Interval theIntervalV;

		theIntervalU = p_ControlU->GetTimeRange(TIMERANGE_ALL);
		theIntervalV = p_ControlV->GetTimeRange(TIMERANGE_ALL);

		if (theIntervalU.Start() < 0)
			theIntervalU.SetStart(0);
		if (theIntervalV.Start() < 0)
			theIntervalV.SetStart(0);

		int startInt;
		int endInt;

		if (theIntervalU.Start() < theIntervalV.Start() && theIntervalU.Start() >= 0)
			startInt = static_cast<int>(floorf(theIntervalU.Start()));
		else
			startInt = static_cast<int>(floorf(theIntervalV.Start()));

		if (theIntervalU.End() > theIntervalV.End())
			endInt = static_cast<int>(floorf(theIntervalU.End()));
		else
			endInt = static_cast<int>(floorf(theIntervalV.End()));

		int fps = rexMaxUtility::GetFPS();
		s32 StartFrame = startInt / (rexMaxUtility::GetFPS());//m_StartFrame;
		s32 EndFrame = endInt / (rexMaxUtility::GetFPS());
		StartFrame = StartFrame / 5;
		EndFrame = EndFrame / 5;
		m_StartFrame = StartFrame;
		m_EndFrame = EndFrame;
		s32 NumFrames = EndFrame - StartFrame;
#endif // HACK_GTA4
		

		if(!rexMaxUtility::IsTexMapAnimated(TexMapList[i],StartFrame,EndFrame))
			continue;

		rexObjectGenericAnimation::TrackInfoVector3* p_TrackInfoV3Col1 = new rexObjectGenericAnimation::TrackInfoVector3;
		rexObjectGenericAnimation::TrackInfoVector3* p_TrackInfoV3Col2 = new rexObjectGenericAnimation::TrackInfoVector3;

		p_TrackInfoV3Col1->ResetKeys(NumFrames);
		p_TrackInfoV3Col2->ResetKeys(NumFrames);

		for(j=0;j<NumFrames;j++)
		{
			p_TrackInfoV3Col1->AppendVector3Key(Vector3(1.0f,0.0f,0.0f));
			p_TrackInfoV3Col2->AppendVector3Key(Vector3(0.0f,1.0f,0.0f));
		}

		rexMaxUtility::SetAnimFrame(StartFrame);

		TexMapList[i]->GetUVTransform(matTransBegin);
		matTransBegin.Invert();

		s32 TicksPerFrame = (s32)(((float)TicksPerSec) * (1.0f / rexMaxUtility::GetFPS()));

		for(j=0;j<NumFrames;j++)
		{
			rexMaxUtility::SetAnimFrame(j + StartFrame);

			TexMapList[i]->Update((TimeValue)((f32)((s32)j + (s32)StartFrame) * (f32)TicksPerFrame),FOREVER);
			TexMapList[i]->GetUVTransform(matTrans);

//			matTrans = matTransBegin * matTrans;

			Point3 trans = matTrans.GetTrans();

			Vector3 Col1 = Vector3(matTrans.GetColumn(0).x,matTrans.GetColumn(0).y,matTrans.GetColumn(0).w);
#if HACK_GTA4
			// The z component of the Vector was inverted at some point as a fix
			Vector3 Col2 = Vector3(matTrans.GetColumn(1).x,matTrans.GetColumn(1).y,1.0f - matTrans.GetColumn(1).w);
#else
			Vector3 Col2 = Vector3(matTrans.GetColumn(1).x,matTrans.GetColumn(1).y,matTrans.GetColumn(1).w);
#endif // HACK_GTA4

			p_TrackInfoV3Col1->SetVector3Key(j - StartFrame,Col1);
			p_TrackInfoV3Col2->SetVector3Key(j - StartFrame,Col2);
		}

		p_TrackInfoV3Col1->m_TrackID = rexObjectGenericAnimation::TRACK_TRANSLATE;
		p_TrackInfoV3Col1->m_Name = "shaderSlideU";
		p_TrackInfoV3Col1->m_StartTime = 0.0f;
		p_TrackInfoV3Col1->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;

		p_TrackInfoV3Col2->m_TrackID = rexObjectGenericAnimation::TRACK_TRANSLATE;
		p_TrackInfoV3Col2->m_Name = "shaderSlideV";
		p_TrackInfoV3Col2->m_StartTime = 0.0f;
		p_TrackInfoV3Col2->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;

		mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1) = p_TrackInfoV3Col1;
		mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1) = p_TrackInfoV3Col2;
	}

	rexMaxUtility::SetAnimFrame(CurrentFrame);
}

////////////////////////////////////////////////////////////////////////////////////////////////

// Should match eAnimEvent in AnimFlags.cpp
enum eAnimEvent
{
	// The start of the first internal loop
	ANIM_EVENT_LOOPSTART_1		= (1<<0),

	// The end of the first internal loop
	ANIM_EVENT_LOOPSTOP_1		= (1<<1),

	// The start of the first internal loop
	ANIM_EVENT_LOOPSTART_2		= (1<<2), 

	// The end of the second internal loop
	ANIM_EVENT_LOOPSTOP_2		= (1<<3), 

	// Must hit this frame. If we miss this frame rewind to this frame and start playing
	ANIM_EVENT_CRITICAL_FRAME	= (1<<4),

	// Foot step events
	ANIM_EVENT_FOOT_HEEL_L		= (1<<5), 
	ANIM_EVENT_FOOT_HEEL_R		= (1<<6), 
	ANIM_EVENT_FOOT_TOE_L		= (1<<7), 
	ANIM_EVENT_FOOT_TOE_R		= (1<<8), 

	// Fire weapon event
	ANIM_EVENT_FIRE_1			= (1<<9), 
	ANIM_EVENT_FIRE_2			= (1<<10), 

	// Object events
	ANIM_EVENT_CREATE_OBJECT	= (1<<11), 
	ANIM_EVENT_DESTROY_OBJECT	= (1<<12), 
	ANIM_EVENT_RELEASE_OBJECT	= (1<<13), 

#if HACK_GTA4
	// Used to denote the point in the animation after which the animation is interruptable
	ANIM_EVENT_INTERRUPTIBLE	= (1<<14), 

	// Bits 16 to 32 are reserved for user events
	ANIM_EVENT_FIRST_RESERVED_FOR_USER = (1<<15), 
#else 
	// Bits 15 to 32 are reserved for user events
	ANIM_EVENT_FIRST_RESERVED_FOR_USER = (1<<14),
#endif
};

// Should match eAnimAmbientFlag in AnimFlags.cpp
enum eAnimAmbientFlag
{
	// Full body that can be played whilst the ped is standing.
	ANIM_AMB_STANDING		=	(1<<0),

	// Upper body that can be played whilst the ped is walking
	ANIM_AMB_WALKING		=	(1<<1),

	// Upper body that can be played whilst the ped is walking.
	ANIM_AMB_RUNNING		=	(1<<2),

	// Full body anim that can be played whilst the ped is ducking.
	ANIM_AMB_DUCKING 		=	(1<<3),

	// Replaces the walk cycle for one instance (e.g tripping up)
	ANIM_AMB_WALKCYCLE		= 	(1<<4),

	// Upper body anim that can be played when the ped is aiming. (e.g gun jam, flinch)
	ANIM_AMB_AIMING 		=	(1<<5),

	// E.g. hand signals or pointing when buddies are nearby whilst in combat.
	ANIM_AMB_BUDDYNEARBY	=	(1<<6)
};

void rexConverterMaxAnimation::GetTagInfoRec(rexObjectGenericAnimation* mp_Animation,INode* p_Node) const
{
	s32 i,Count = p_Node->NumNoteTracks();
	DefNoteTrack * p_Notetrack;
	
	if(Count < 1)
		return;

	s32 TimeFactor = (s32)(rexMaxUtility::GetTicksPerSec() / rexMaxUtility::GetFPS());
	float Counter = 1.0f;
	
	char p_Note[8192];

	for(i=0;i<Count;i++)
	{
		p_Notetrack = (DefNoteTrack *)p_Node->GetNoteTrack(i);
		if(p_Notetrack) 
		{
			// "EventTrack"
			if (i == 0)
			{
#if HACK_GTA4
				// Empty track was being set to false so I removed it entirely
				// ANIM_EVENT_INTERRUPTIBLE has be included here 
				// !strstr has been replaced with stricmp inside "if(Failed)" as a fix
				rexObjectGenericAnimation::TrackInfoInt* p_TrackInfoF = new rexObjectGenericAnimation::TrackInfoInt;
				s32 NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;
				p_TrackInfoF->ResetKeys(NumFrames);
				s32 j;
				for(j=0;j<NumFrames;j++)
				{
					p_TrackInfoF->AppendIntKey(0);
				}
				
				int k;
				for(k=0;k<p_Notetrack->keys.Count();k++)
				{
					NoteKey* p_NoteKey = *(p_Notetrack->keys.Addr(k));			
					strncpy(p_Note,p_NoteKey->note,8191);
					strlwr(p_Note);
					s32 Val = 0;
					bool Failed = true;

					if(strstr(p_Note,"loopstart_1"))
					{
						Val |= ANIM_EVENT_LOOPSTART_1;
						Failed = false;
					}
					if(strstr(p_Note,"loopstop_1"))
					{
						Val |= ANIM_EVENT_LOOPSTOP_1;
						Failed = false;
					}
					if(strstr(p_Note,"loopstart_2"))
					{
						Val |= ANIM_EVENT_LOOPSTART_2;
						Failed = false;
					}
					if(strstr(p_Note,"loopstop_2"))
					{
						Val |= ANIM_EVENT_LOOPSTOP_2;
						Failed = false;
					}
					if(strstr(p_Note,"critical_frame"))
					{
						Val |= ANIM_EVENT_CRITICAL_FRAME;
						Failed = false;
					}
					if(strstr(p_Note,"foot_heel_l"))
					{
						Val |= ANIM_EVENT_FOOT_HEEL_L;
						Failed = false;
					}
					if(strstr(p_Note,"foot_heel_r"))
					{
						Val |= ANIM_EVENT_FOOT_HEEL_R;
						Failed = false;
					}
					if(strstr(p_Note,"foot_toe_l"))
					{
						Val |= ANIM_EVENT_FOOT_TOE_L;
						Failed = false;
					}
					if(strstr(p_Note,"foot_toe_r"))
					{
						Val |= ANIM_EVENT_FOOT_TOE_R;
						Failed = false;
					}
					if(strstr(p_Note,"fire_1"))
					{
						Val |= ANIM_EVENT_FIRE_1;
						Failed = false;
					}
					if(strstr(p_Note,"fire_2"))
					{
						Val |= ANIM_EVENT_FIRE_2;
						Failed = false;
					}
					if(strstr(p_Note,"create_object"))
					{
						Val |= ANIM_EVENT_CREATE_OBJECT;
						Failed = false;
					}					
					if(strstr(p_Note,"destroy_object"))
					{
						Val |= ANIM_EVENT_DESTROY_OBJECT;
						Failed = false;
					}
					if(strstr(p_Note,"release_object"))
					{
						Val |= ANIM_EVENT_RELEASE_OBJECT;
						Failed = false;
					}

					if(strstr(p_Note,"interruptible"))
					{
						Val |= ANIM_EVENT_INTERRUPTIBLE;
						Failed = false;
					}

					if (Failed)
					{
						if(stricmp(p_Note,"0") != 0)
						{
							Val |= atoi(p_Note);

						}
					}

					s32 KeyFrame = p_NoteKey->time / TimeFactor;
					if(KeyFrame < m_EndFrame)
					{
						p_TrackInfoF->SetIntKey((s32)KeyFrame - (s32)m_StartFrame,Val);
					}
				}

				
				p_TrackInfoF->m_TrackID = rexObjectGenericAnimation::TRACK_INFO;
				// projectData0 == ANIM_TRACK_EVENTS (see eAnimFlags AnimFlags.h)
				p_TrackInfoF->m_Name = "projectData0"; 
				p_TrackInfoF->m_StartTime = 0.0f;
				p_TrackInfoF->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
				rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1);
				rp_TrackInfo = p_TrackInfoF;
#else
				bool emptyTrack = true;
				rexObjectGenericAnimation::TrackInfoInt* p_TrackInfoF = new rexObjectGenericAnimation::TrackInfoInt;
				s32 NumFrames = m_EndFrame - m_StartFrame;
				p_TrackInfoF->ResetKeys(NumFrames);
				s32 j;
				for(j=0;j<NumFrames;j++)
				{
					p_TrackInfoF->AppendIntKey(0);
				}
				
				int k;
				for(k=0;k<p_Notetrack->keys.Count();k++)
				{
					NoteKey* p_NoteKey = *(p_Notetrack->keys.Addr(k));			
					strncpy(p_Note,p_NoteKey->note,1024);
					strlwr(p_Note);
					s32 Val = 0;
					bool Failed = true;

					if(strstr(p_Note,"loopstart_1"))
					{
						Val |= ANIM_EVENT_LOOPSTART_1;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"loopstop_1"))
					{
						Val |= ANIM_EVENT_LOOPSTOP_1;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"loopstart_2"))
					{
						Val |= ANIM_EVENT_LOOPSTART_2;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"loopstop_2"))
					{
						Val |= ANIM_EVENT_LOOPSTOP_2;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"critical_frame"))
					{
						Val |= ANIM_EVENT_CRITICAL_FRAME;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"foot_heel_l"))
					{
						Val |= ANIM_EVENT_FOOT_HEEL_L;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"foot_heel_r"))
					{
						Val |= ANIM_EVENT_FOOT_HEEL_R;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"foot_toe_l"))
					{
						Val |= ANIM_EVENT_FOOT_TOE_L;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"foot_toe_r"))
					{
						Val |= ANIM_EVENT_FOOT_TOE_R;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"fire_1"))
					{
						Val |= ANIM_EVENT_FIRE_1;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"fire_2"))
					{
						Val |= ANIM_EVENT_FIRE_2;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"create_object"))
					{
						Val |= ANIM_EVENT_CREATE_OBJECT;
						Failed = false;
						emptyTrack = false;
					}					
					if(strstr(p_Note,"destroy_object"))
					{
						Val |= ANIM_EVENT_DESTROY_OBJECT;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"release_object"))
					{
						Val |= ANIM_EVENT_RELEASE_OBJECT;
						Failed = false;
						emptyTrack = false;
					}

					if (Failed)
					{
						// For testing purposes can just type in a number (well except "0")
						if(!strstr(p_Note,"0"))
						{
							Val |= atoi(p_Note);

							emptyTrack = false;
						}

					}

					s32 KeyFrame = p_NoteKey->time / TimeFactor;
					if(KeyFrame < m_EndFrame)
					{
						p_TrackInfoF->SetIntKey(KeyFrame - m_StartFrame,Val);
					}
				}

				if (!emptyTrack)
				{
					p_TrackInfoF->m_TrackID = rexObjectGenericAnimation::TRACK_INFO;
					// projectData0 == ANIM_TRACK_EVENTS (see eAnimFlags AnimFlags.h)
					p_TrackInfoF->m_Name = "projectData0"; 
					p_TrackInfoF->m_StartTime = 0.0f;
					p_TrackInfoF->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
					rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1);
					rp_TrackInfo = p_TrackInfoF;
					
				}			
#endif // HACK_GTA4	
			}

			// "AmbientTrack"
			if (i == 2)
			{
				bool emptyTrack = true;
				rexObjectGenericAnimation::TrackInfoInt* p_TrackInfoF = new rexObjectGenericAnimation::TrackInfoInt;
				s32 NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;
				p_TrackInfoF->ResetKeys(NumFrames);
				s32 j;
				for(j=0;j<NumFrames;j++)
				{
					p_TrackInfoF->AppendIntKey(0);
				}

				for(int k=0;k<p_Notetrack->keys.Count();k++)
				{
					NoteKey* p_NoteKey = *(p_Notetrack->keys.Addr(k));			
					strncpy(p_Note,p_NoteKey->note,1024);
					strlwr(p_Note);
					
					s32 Val = 0;
					bool Failed = true;

					if(strstr(p_Note,"standing"))
					{
						Val |= ANIM_AMB_STANDING;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"walking"))
					{
						Val |= ANIM_AMB_WALKING;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"running"))
					{
						Val |= ANIM_AMB_RUNNING;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"ducking"))
					{
						Val |= ANIM_AMB_DUCKING;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"walkcycle"))
					{
						Val |= ANIM_AMB_WALKCYCLE;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"aiming"))
					{
						Val |= ANIM_AMB_AIMING;
						Failed = false;
						emptyTrack = false;
					}
					if(strstr(p_Note,"buddynearby"))
					{
						Val |= ANIM_AMB_BUDDYNEARBY;
						Failed = false;
						emptyTrack = false;
					}

					if(Failed)
					{
						// For testing purposes can just type in a number (well except "0")
						if(stricmp(p_Note,"0") != 0)
						{
							Val |= atoi(p_Note);
							emptyTrack = false;
						}
					}

					s32 KeyFrame = p_NoteKey->time / TimeFactor;
					if(KeyFrame < m_EndFrame)
					{
						p_TrackInfoF->SetIntKey((s32)KeyFrame - (s32)m_StartFrame,Val);
					}					
				}

				if (!emptyTrack)
				{
					p_TrackInfoF->m_TrackID = rexObjectGenericAnimation::TRACK_INFO;
					// projectData2 == ANIM_TRACK_AMBIENT_FLAGS (see eAnimFlags AnimFlags.h)
					p_TrackInfoF->m_Name = "projectData2"; 
					p_TrackInfoF->m_StartTime = 0.0f;
					p_TrackInfoF->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
					rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1);
					rp_TrackInfo = p_TrackInfoF;
				}
			}

			// "AudioTrack"
			if (i == 3)
			{
				bool emptyTrack = true;
				rexObjectGenericAnimation::TrackInfoInt* p_TrackInfoF = new rexObjectGenericAnimation::TrackInfoInt;
				s32 NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;
				p_TrackInfoF->ResetKeys(NumFrames);
				s32 j;
				for(j=0;j<NumFrames;j++)
				{
					p_TrackInfoF->AppendIntKey(0);
				}

				for(int k=0;k<p_Notetrack->keys.Count();k++)
				{
					NoteKey* p_NoteKey = *(p_Notetrack->keys.Addr(k));			
					strncpy(p_Note,p_NoteKey->note,1024);
					strlwr(p_Note);

					if(stricmp(p_Note,"0") != 0)
					{
						emptyTrack = false;

						// Convert animation name into a hash key
						s32 Val = 0;
						Val = atStringHash(p_Note);
						s32 KeyFrame = p_NoteKey->time / TimeFactor;
						if(KeyFrame < m_EndFrame)
						{
							p_TrackInfoF->SetIntKey((s32)KeyFrame - (s32)m_StartFrame,Val);
						}
					}
				}

				if (!emptyTrack)
				{
					p_TrackInfoF->m_TrackID = rexObjectGenericAnimation::TRACK_INFO;
					// projectData3 == ANIM_TRACK_AUDIO (see eAnimFlags AnimFlags.h)
					p_TrackInfoF->m_Name = "projectData3"; 
					p_TrackInfoF->m_StartTime = 0.0f;
					p_TrackInfoF->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
					rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1);
					rp_TrackInfo = p_TrackInfoF;
				}
			}

			// "GestureSpeakerTrack"
			if (i == 4)
			{
				bool emptyTrack = true;
				rexObjectGenericAnimation::TrackInfoInt* p_TrackInfoF = new rexObjectGenericAnimation::TrackInfoInt;
				s32 NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;
				p_TrackInfoF->ResetKeys(NumFrames);
				s32 j;
				for(j=0;j<NumFrames;j++)
				{
					p_TrackInfoF->AppendIntKey(0);
				}

				for(int k=0;k<p_Notetrack->keys.Count();k++)
				{
					NoteKey* p_NoteKey = *(p_Notetrack->keys.Addr(k));			
					strncpy(p_Note,p_NoteKey->note,1024);
					strlwr(p_Note);

					if(stricmp(p_Note,"0") != 0)
					{
						emptyTrack = false;

						// Convert animation name into a hash key
						s32 Val = 0;
						Val = atStringHash(p_Note);
						s32 KeyFrame = p_NoteKey->time / TimeFactor;
						if(KeyFrame < m_EndFrame)
						{
							p_TrackInfoF->SetIntKey((s32)KeyFrame - (s32)m_StartFrame,Val);
						}
					}
				}

				if (!emptyTrack)
				{
					p_TrackInfoF->m_TrackID = rexObjectGenericAnimation::TRACK_INFO;
					// projectData4 == ANIM_TRACK_GESTURE_SPEAKER (see eAnimFlags AnimFlags.h)
					p_TrackInfoF->m_Name = "projectData4"; 
					p_TrackInfoF->m_StartTime = 0.0f;
					p_TrackInfoF->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
					rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1);
					rp_TrackInfo = p_TrackInfoF;
				}
			}

			// "GestureListenerTrack"
			if (i == 5)
			{
				bool emptyTrack = true;
				rexObjectGenericAnimation::TrackInfoInt* p_TrackInfoF = new rexObjectGenericAnimation::TrackInfoInt;
				s32 NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;
				p_TrackInfoF->ResetKeys(NumFrames);
				s32 j;
				for(j=0;j<NumFrames;j++)
				{
					p_TrackInfoF->AppendIntKey(0);
				}

				for(int k=0;k<p_Notetrack->keys.Count();k++)
				{
					NoteKey* p_NoteKey = *(p_Notetrack->keys.Addr(k));			
					strncpy(p_Note,p_NoteKey->note,1024);
					strlwr(p_Note);

					if(stricmp(p_Note,"0") != 0)
					{
						emptyTrack = false;

						// Convert animation name into a hash key
						s32 Val = 0;
						Val = atStringHash(p_Note);
						s32 KeyFrame = p_NoteKey->time / TimeFactor;
						if(KeyFrame < m_EndFrame)
						{
							p_TrackInfoF->SetIntKey((s32)KeyFrame - (s32)m_StartFrame,Val);
						}
					}
				}

				if (!emptyTrack)
				{
					p_TrackInfoF->m_TrackID = rexObjectGenericAnimation::TRACK_INFO;
					// projectData5 == ANIM_TRACK_GESTURE_LISTENER (see eAnimFlags AnimFlags.h)
					p_TrackInfoF->m_Name = "projectData5";
					p_TrackInfoF->m_StartTime = 0.0f;
					p_TrackInfoF->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
					rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1);
					rp_TrackInfo = p_TrackInfoF;
				}
			}


			// "FacialSpeakerTrack"
			if (i == 6)
			{
				bool emptyTrack = true;
				rexObjectGenericAnimation::TrackInfoInt* p_TrackInfoF = new rexObjectGenericAnimation::TrackInfoInt;
				s32 NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;
				p_TrackInfoF->ResetKeys(NumFrames);
				s32 j;
				for(j=0;j<NumFrames;j++)
				{
					p_TrackInfoF->AppendIntKey(0);
				}

				for(int k=0;k<p_Notetrack->keys.Count();k++)
				{
					NoteKey* p_NoteKey = *(p_Notetrack->keys.Addr(k));			
					strncpy(p_Note,p_NoteKey->note,1024);
					strlwr(p_Note);

					if(stricmp(p_Note,"0") != 0)
					{
						emptyTrack = false;

						// Convert animation name into a hash key
						s32 Val = 0;
						Val = atStringHash(p_Note);
						s32 KeyFrame = p_NoteKey->time / TimeFactor;
						if(KeyFrame < m_EndFrame)
						{
							p_TrackInfoF->SetIntKey((s32)KeyFrame - (s32)m_StartFrame,Val);
						}
					}
				}

				if (!emptyTrack)
				{
					p_TrackInfoF->m_TrackID = rexObjectGenericAnimation::TRACK_INFO;
					// projectData6 == ANIM_TRACK_FACIAL_SPEAKER (see eAnimFlags AnimFlags.h)
					p_TrackInfoF->m_Name = "projectData6"; 
					p_TrackInfoF->m_StartTime = 0.0f;
					p_TrackInfoF->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
					rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1);
					rp_TrackInfo = p_TrackInfoF;
				}
			}

			// "FacialListenerTrack"
			if (i == 7)
			{
				bool emptyTrack = true;
				rexObjectGenericAnimation::TrackInfoInt* p_TrackInfoF = new rexObjectGenericAnimation::TrackInfoInt;
				s32 NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;
				p_TrackInfoF->ResetKeys(NumFrames);
				s32 j;
				for(j=0;j<NumFrames;j++)
				{
					p_TrackInfoF->AppendIntKey(0);
				}

				for(int k=0;k<p_Notetrack->keys.Count();k++)
				{
					NoteKey* p_NoteKey = *(p_Notetrack->keys.Addr(k));			
					strncpy(p_Note,p_NoteKey->note,1024);
					strlwr(p_Note);

					if(stricmp(p_Note,"0") != 0)
					{
						emptyTrack = false;

						// Convert animation name into a hash key
						s32 Val = 0;
						Val = atStringHash(p_Note);
						s32 KeyFrame = p_NoteKey->time / TimeFactor;
						if(KeyFrame < m_EndFrame)
						{
							p_TrackInfoF->SetIntKey((s32)KeyFrame - (s32)m_StartFrame,Val);
						}
					}
				}

				if (!emptyTrack)
				{
					p_TrackInfoF->m_TrackID = rexObjectGenericAnimation::TRACK_INFO;
					// projectData7 == ANIM_TRACK_FACIAL_LISTENER (see eAnimFlags AnimFlags.h)
					p_TrackInfoF->m_Name = "projectData7";
					p_TrackInfoF->m_StartTime = 0.0f;
					p_TrackInfoF->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
					rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = mp_Animation->m_RootChunks[0]->m_Tracks.Grow(1);
					rp_TrackInfo = p_TrackInfoF;
				}
			}

#if 0
			//commented out temporarily as this doesnt compile against the head of rage
			//Greg 2/10/2006


			// MUST_FIX_THIS(Jonathon - Temporary code to test vismeme runtime implementation);
			// "Visemes"
			if (i > 6)
			{
				if (mp_Animation->m_BlendShapeChunk == 0)
				{
					mp_Animation->m_BlendShapeChunk = new rexObjectGenericAnimation::ChunkInfo();
				}

				rexObjectGenericAnimation::TrackInfoFloat* p_TrackInfoF = new rexObjectGenericAnimation::TrackInfoFloat;
				s32 NumFrames = m_EndFrame - m_StartFrame;
				p_TrackInfoF->ResetKeys(NumFrames);
				s32 j;
				for(j=0;j<NumFrames;j++)
				{
					p_TrackInfoF->AppendFloatKey(0.0f);
				}

				for(int k=0;k<p_Notetrack->keys.Count();k++)
				{
					NoteKey* p_NoteKey = *(p_Notetrack->keys.Addr(k));
					strncpy(p_Note,p_NoteKey->note,1024);
					strlwr(p_Note);
					float Val = 0.0f;

					// Convert float string to float value
					Val = (float)atof(p_Note);					

					s32 KeyFrame = p_NoteKey->time / TimeFactor;
					if(KeyFrame < m_EndFrame)
					{
						int k = KeyFrame - (int)m_StartFrame;
						p_TrackInfoF->SetFloatKey(k,Val);
				
						//static char message[1024];
						//sprintf( message, "%d = %f\n", k, Val );
						//OutputDebugStringA(message);
					}
				}

				p_TrackInfoF->m_TrackID = rexObjectGenericAnimation::TRACK_VISEMES;
				// phoneme == ANIM_TRACK_GESTURE_SPEAKER (see eAnimFlags AnimFlags.h)

				char buffer[65];
				int r = 10;
				_itoa(i-7, buffer, r);
				p_TrackInfoF->m_Name = buffer; 
				p_TrackInfoF->m_StartTime = 0.0f;
				p_TrackInfoF->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
				rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = mp_Animation->m_BlendShapeChunk->m_Tracks.Grow(1);
				rp_TrackInfo = p_TrackInfoF;
			}

#endif 
		}
	}
}

void rexConverterMaxAnimation::GetVisibilityInfoRec(rexObjectGenericAnimation::ChunkInfo* p_Chunk,INode* p_Node) const
{
	Control *pMaxControl = p_Node->GetVisController();

	if(pMaxControl)
	{

		// Find and delete former scale tracks
		atArray<rexObjectGenericAnimation::TrackInfo*>::iterator it = p_Chunk->m_Tracks.begin();
		for(;it!=p_Chunk->m_Tracks.end();it++)
		{
			rexObjectGenericAnimation::TrackInfo* pTrack = *it;
			if(pTrack->m_TrackID==rexObjectGenericAnimation::TRACK_VISIBILITY)
			{
				p_Chunk->m_Tracks.erase(it);
			}
		}

		s32 TimeFactor = (s32)(rexMaxUtility::GetTicksPerSec() / rexMaxUtility::GetFPS());
		float Counter = 1.0f;

		rexObjectGenericAnimation::TrackInfoFloat* p_TrackInfoFloat = new rexObjectGenericAnimation::TrackInfoFloat;
		s32 NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;
		p_TrackInfoFloat->ResetKeys(NumFrames);
		s32 j;
		for(j=0;j<NumFrames;j++)
		{
			TimeValue t = (m_StartFrame+j) * TimeFactor;
			float v;
			pMaxControl->GetValue(t, &v, FOREVER);
			p_TrackInfoFloat->AppendFloatKey(v);
		}

		p_TrackInfoFloat->m_TrackID = rexObjectGenericAnimation::TRACK_VISIBILITY;
		p_TrackInfoFloat->m_Name = "visibility"; 
		p_TrackInfoFloat->m_StartTime = mp_Animation->m_StartTime;
		p_TrackInfoFloat->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
		rexObjectGenericAnimation::TrackInfo*& rp_TrackInfo = p_Chunk->m_Tracks.Grow(1);
		rp_TrackInfo = p_TrackInfoFloat;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////

static const float TIME_TO_FRAME_TOLERANCE = 0.001f;

void rexConverterMaxAnimation::GetMorphTargetInfoRec(rexObjectGenericAnimation* mp_Animation,INode* p_Node) const
{
	int nFrames = int((mp_Animation->m_EndTime - mp_Animation->m_StartTime + TIME_TO_FRAME_TOLERANCE) * mp_Animation->m_FramesPerSecond) + 1;
	float deltaTime = 1.0f / mp_Animation->m_FramesPerSecond;

	Modifier* p_MorphModifier = rexMaxUtility::GetMorph(p_Node);
	if(p_MorphModifier)
	{
		MorphR3* p_Morpher = static_cast<MorphR3*>(p_MorphModifier);

		atArray< rexObjectGenericAnimation::TrackInfoFloat* >	targetTracks;

		int chanCount = p_Morpher->chanBank.size();
		int activeCount = 0;

		//Initialize a track for each active target
		for(int chanIdx=0; chanIdx<chanCount; chanIdx++)
		{
			morphChannel* p_MorphChn = &p_Morpher->chanBank[chanIdx];
			if(!p_MorphChn->mActive)
			{
#if HACK_GTA4
				// Im unsure about this one.  I suspect it has to do with viseme export but the commit comment for this
				// change (CL #91879) describes a seperate fix.
				break;
#else
				continue;
#endif // HACK_GTA4
			}

			activeCount++;

			rexObjectGenericAnimation::TrackInfoFloat* pTrack = new rexObjectGenericAnimation::TrackInfoFloat();
			pTrack->m_TrackID = rexObjectGenericAnimation::TRACK_BLENDSHAPE;
			pTrack->m_Name = p_MorphChn->mName;
			pTrack->m_IsLocked = false;
			pTrack->m_StartTime = mp_Animation->m_StartTime;
			pTrack->m_EndTime = mp_Animation->m_EndTime;
			pTrack->ResetKeys(nFrames);

			targetTracks.PushAndGrow(pTrack);
		}

		//Sample the values of each target controller.
		float start = mp_Animation->m_StartTime;
		float end = mp_Animation->m_EndTime + TIME_TO_FRAME_TOLERANCE;
		Interval iVal;

		for(float t=start; t<=end; t+=deltaTime)
		{
			for(int chanIdx=0; chanIdx<chanCount; chanIdx++)
			{
				morphChannel* p_MorphChn = &p_Morpher->chanBank[chanIdx];
				if(!p_MorphChn->mActive)
				{
#if HACK_GTA4
					// Im unsure about this one.  I suspect it has to do with viseme export but the commit comment for this
					// change (CL #91879) describes a seperate fix.
					break;
#else
					continue;
#endif // HACK_GTA4
				}

				rexObjectGenericAnimation::TrackInfoFloat* pTrack = targetTracks[chanIdx];

				Control* p_MorphCtrl = p_MorphChn->cblock->GetController(0);
				Assert(p_MorphCtrl);

				TimeValue tv = (TimeValue)(t * rexMaxUtility::GetTicksPerSec());
				float weight;
				p_MorphCtrl->GetValue(tv, &weight, iVal);

				//Weight values from max are expressed in the 0-100 range.
				pTrack->AppendFloatKey(weight / 100.0f);
			}
		}

		if(!mp_Animation->m_GenericChunk)
			mp_Animation->m_GenericChunk = new rexObjectGenericAnimation::ChunkInfo();

		for(int trackIdx=0; trackIdx<targetTracks.GetCount(); trackIdx++)
			mp_Animation->m_GenericChunk->m_Tracks.PushAndGrow( targetTracks[trackIdx] );
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////

void rexConverterMaxAnimation::GetMoverInfoRec(rexObjectGenericAnimation* mp_Animation,INode* p_Node) const
{
	atArray<atString> TrackNames;
	atArray<int> TrackIDs;

	TrackNames.PushAndGrow(atString("translate"));
	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_TRANSLATE);
	TrackNames.PushAndGrow(atString("rotate"));
	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_ROTATE);

	rexObjectGenericAnimation::MoverChunkInfo *pMoverChunkInfo = new rexObjectGenericAnimation::MoverChunkInfo();

	InitTracksInfo((s32)m_EndFrame - (s32)m_StartFrame,pMoverChunkInfo->m_MoverTracks,TrackNames,TrackIDs);
	InnerNodeToTrack(p_Node,pMoverChunkInfo->m_MoverTracks,true,true);

	mp_Animation->m_MoverChunks.PushAndGrow(pMoverChunkInfo);
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxAnimation::GetAnimInfoRec(rexObjectGenericAnimation::ChunkInfo* p_Chunk,INode* p_Node,bool IsRoot) const
{
	atArray<atString> TrackNames;
	atArray<int> TrackIDs;

	rexResult retVal = rexResult::PERFECT;

	if(!p_Node)
	{
		retVal.Combine(rexResult::ERRORS);
		retVal.AddError("No Node given to rexConverterMaxAnimation::GetAnimInfoRec()");
		return retVal;
	}

	rexObjectGenericAnimation::ChunkInfo* p_ChildChunk = p_Chunk;
	s32 NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;

	if(rexConverterMaxSkeleton::IsValidSkelBone(p_Node, &retVal))
	{
		if(!IsRoot)
		{
			p_ChildChunk = new rexObjectGenericAnimation::ChunkInfo;

			p_ChildChunk->m_Parent = p_Chunk;
			p_Chunk->m_Children.PushAndGrow(p_ChildChunk);
		}

		if(m_IsCamera)
		{
			TrackNames.PushAndGrow(atString("cameraTranslation"));
			TrackNames.PushAndGrow(atString("cameraRotation"));
		}
		else
		{
			TrackNames.PushAndGrow(atString("translate"));
			TrackNames.PushAndGrow(atString("rotate"));
			TrackNames.PushAndGrow(atString("scale"));
		}

		TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_TRANSLATE);
		TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_ROTATE);
		TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_SCALE);

		p_ChildChunk->m_Name = rexMaxUtility::ReplaceWhitespaceCharacters(atString(p_Node->GetName()));
		p_ChildChunk->m_ShortName = p_ChildChunk->m_Name;
		p_ChildChunk->m_IsJoint = true;
		p_ChildChunk->m_BoneIndex = 0;
		p_ChildChunk->m_Scale = Vector3(1.0f,1.0f,1.0f);
		p_ChildChunk->m_BoneID = rexMaxUtility::GetBoneID(p_Node);

		InitTracksInfo(NumFrames,p_ChildChunk->m_Tracks,TrackNames,TrackIDs);
		retVal.Combine(InnerNodeToChunk(p_Node,p_ChildChunk,IsRoot));

		GetVisibilityInfoRec(p_ChildChunk, p_Node);
	}

	s32 ChildCount = p_Node->NumberOfChildren();
	for(s32 i=0;i<ChildCount;i++)
	{
		INode* p_ChildNode = p_Node->GetChildNode(i);

		if(m_ExportAnimSelected)
			p_ChildNode = rexMaxUtility::GetNextSelected(p_ChildNode);

		if(!p_ChildNode)
			continue;

		retVal.Combine(GetAnimInfoRec(p_ChildChunk,p_ChildNode));
	}

	return retVal;
}
#if HACK_GTA4
////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::GetAnimVisemeInfoRec(rexObjectGenericAnimation::ChunkInfo* p_Chunk,INode* p_Node,bool IsRoot) const
{
	atArray<atString> TrackNames;
	atArray<int> TrackIDs;

	if(!p_Node)
		return;

	TrackNames.PushAndGrow(atString("0"));
	TrackNames.PushAndGrow(atString("1"));
	TrackNames.PushAndGrow(atString("2"));
	TrackNames.PushAndGrow(atString("3"));
	TrackNames.PushAndGrow(atString("4"));
	TrackNames.PushAndGrow(atString("5"));
	TrackNames.PushAndGrow(atString("6"));

	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_VISEMES);
	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_VISEMES);
	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_VISEMES);
	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_VISEMES);
	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_VISEMES);
	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_VISEMES);
	TrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_VISEMES);


	p_Chunk->m_Name = rexMaxUtility::ReplaceWhitespaceCharacters(atString(p_Node->GetName()));//"viseme";
	p_Chunk->m_ShortName = p_Chunk->m_Name;
	//p_Chunk->m_IsJoint = true;
	p_Chunk->m_BoneIndex = 0;
	p_Chunk->m_Scale = Vector3(1.0f,1.0f,1.0f);
	p_Chunk->m_BoneID = "viseme";

	s32 NumFrames = (s32)m_EndFrame - (s32)m_StartFrame;

	InitTracksInfo(NumFrames,p_Chunk->m_Tracks,TrackNames,TrackIDs);
	GenericNodeToChunk(p_Node,p_Chunk, TrackIDs.GetCount());
	
}
#endif // HACK_GTA4

////////////////////////////////////////////////////////////////////////////////////////////////
bool rexConverterMaxAnimation::IsAnimated(rexObjectGenericAnimation::ChunkInfo* p_Chunk) const
{
	s32 i,j,CountKeys,Count = p_Chunk->m_Tracks.GetCount();

	for(i=0;i<Count;i++)
	{
		switch(p_Chunk->m_Tracks[i]->m_TrackID)
		{
		case rexObjectGenericAnimation::TRACK_TRANSLATE:
		case rexObjectGenericAnimation::TRACK_FACIAL_TRANSLATION:
		case rexObjectGenericAnimation::TRACK_SCALE:
			{
				rexObjectGenericAnimation::TrackInfoVector3* p_Track = (rexObjectGenericAnimation::TrackInfoVector3*)p_Chunk->m_Tracks[i];

				Vector3 VecFirst,VecCompare;
				CountKeys = p_Track->GetNumKeys();

				if(CountKeys <= 0)
					break;

				p_Track->GetVector3Key(0,VecFirst);

				for(j=1;j<CountKeys;j++)
				{
					p_Track->GetVector3Key(j,VecCompare);

					if(VecFirst != VecCompare)
					{
						return true;
					}
				}
			}

			break;

		case rexObjectGenericAnimation::TRACK_ROTATE:
		case rexObjectGenericAnimation::TRACK_FACIAL_ROTATION:
			rexObjectGenericAnimation::TrackInfoQuaternion* p_Track = (rexObjectGenericAnimation::TrackInfoQuaternion*)p_Chunk->m_Tracks[i];

			{
				Quaternion QuatFirst,QuatCompare;
				Vector3 VecFirst,VecCompare; //need these as quaternion doesnt currently have compare functions

				CountKeys = p_Track->GetNumKeys();

				if(CountKeys <= 0)
					break;

				p_Track->GetQuaternionKey(0,QuatFirst);
				QuatFirst.ToEulers(VecFirst);

				for(j=1;j<CountKeys;j++)
				{
					p_Track->GetQuaternionKey(j,QuatCompare);
					QuatCompare.ToEulers(VecCompare);

					if(VecFirst != VecCompare)
					{
						return true;
					}
				}
			}

			break;
		}
	}

	return false;
}
#if HACK_GTA4
// Looks for a modifier named "VisemeMorph".  A little hacked
// we should set a property for this.  Marked for review (LPXO)
////////////////////////////////////////////////////////////////////////////////////////
bool rexConverterMaxAnimation::IsViseme(Object* p_Object) const
{
	

	if(p_Object->SuperClassID() != GEN_DERIVOB_CLASS_ID)
		return false;

	IDerivedObject* p_derived = (IDerivedObject*)p_Object;
	
	if (p_derived)
	{
		Animatable* p_Anim = (Animatable*)p_derived;
		if (p_Anim)
		{
			//Modifier* p_Morpher;
			int numMorphs = p_derived->NumModifiers();
			for(int i=0; i<numMorphs; ++i)
			{
				Modifier* p_Morpher =  p_derived->GetModifier(i);
				MSTR morpher = p_Morpher->GetName();
				if (strcmp((char*)morpher, "VisemeMorph")==0)
					return true;
			}
		}
	}

	return false;
}
#endif // HACK_GTA4

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxAnimation::ExportRootChuck(INode* p_Node,rexMaxObject* p_Obj) const
{
	if(!p_Node)
		return rexResult::ERRORS;

	rexResult retVal = rexResult::PERFECT;

	m_outChunkData.Kill();

	Object* p_Object = p_Node->GetObjectRef();
	Class_ID classID = p_Object->ClassID();

#if HACK_GTA4
	bool bIsViseme = IsViseme(p_Object);
#endif // HACK_GTA4	

	if((classID == Class_ID(SIMPLE_CAM_CLASS_ID,0)) || (classID == Class_ID(LOOKAT_CAM_CLASS_ID,0)))
		m_IsCamera = true;
	else
		m_IsCamera = false;

	bool IsRoot = (p_Node == p_Obj->GetMaxNode()) && (strcmp(rexMaxUtility::GetBoneID(p_Node),"#0") == 0);

	mp_AnimSegment->m_DontExportRoot = true;

	rexObjectGenericAnimation::ChunkInfo* p_Chunk = new rexObjectGenericAnimation::ChunkInfo;
#if HACK_GTA4
	rexObjectGenericAnimation::ChunkInfo* p_BogusChunk = new rexObjectGenericAnimation::ChunkInfo;	
	
	if(bIsViseme)
	{
		GetAnimVisemeInfoRec(p_Chunk,p_Node,IsRoot);
		retVal.Combine(GetAnimInfoRec(p_BogusChunk,p_Node,IsRoot));
		
	}
	else
		retVal.Combine(GetAnimInfoRec(p_Chunk,p_Node,IsRoot));
#else
	retVal.Combine(GetAnimInfoRec(p_Chunk,p_Node,IsRoot));
#endif // HACK_GTA4

	//move the matrices from the cache to the chunks

	s32 i,Count = m_outChunkData.GetNumSlots();
#if HACK_GTA4
	// More viseme related code
	s32 vCount = m_outGenericChunkData.GetNumSlots();
	
	for(i=0;i<vCount;i++)
	{
		const atMap<rexObjectGenericAnimation::ChunkInfo*,atArray<atArray<Matrix34*>> >::Entry* p_Entry = m_outGenericChunkData.GetEntry(i);

		while(p_Entry)
		{
			AddGenericNodeToTracks(p_Entry->data,(s32)m_StartFrame,(s32)m_EndFrame,p_Entry->key->m_Tracks);

			p_Entry = p_Entry->next;
		}
	}
#endif // HACK_GTA4
	
	for(i=0;i<Count;i++)
	{
		const atMap<rexObjectGenericAnimation::ChunkInfo*,atArray<Matrix34*> >::Entry* p_Entry = m_outChunkData.GetEntry(i);

		while(p_Entry)
		{
				AddNodeToTracks(p_Entry->data,(s32)m_StartFrame,(s32)m_EndFrame,p_Entry->key->m_Tracks);

			p_Entry = p_Entry->next;
		}
	}
	

	if(m_ExportAnimMoverTrack && IsRoot && IsAnimated(p_Chunk))
	{
		INode* p_ExtraNode = NULL;

		if(m_ExtraMoverTrack != atString(""))
			p_ExtraNode = rexMaxUtility::FindNode(GetCOREInterface()->GetRootNode(),m_ExtraMoverTrack);

		if(p_ExtraNode)
			GetMoverInfoRec(mp_Animation,p_ExtraNode);
		else
			GetMoverInfoRec(mp_Animation,p_Node);
	}

	if(m_MorphWeightTracks != atString(""))
	{
		INode* p_MorphTrackNode = NULL;
		p_MorphTrackNode = rexMaxUtility::FindNode(GetCOREInterface()->GetRootNode(),m_MorphWeightTracks);
		if(p_MorphTrackNode)
			GetMorphTargetInfoRec(mp_Animation,p_MorphTrackNode);
	}

	mp_AnimSegment->m_ChunkNames.PushAndGrow(p_Chunk->m_Name,1);
#if HACK_GTA4
	if(bIsViseme)
	{
		mp_Animation->m_RootChunks.PushAndGrow(p_BogusChunk,1);
		mp_Animation->m_GenericChunk = p_Chunk;
	}
	else
		mp_Animation->m_RootChunks.PushAndGrow(p_Chunk,1);
#endif // HACK_GTA4
	return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////
struct ExpressionCtrlTrackData
{
	INode*	mp_Node;
	atArray< atString > m_attrNames;
	atArray< rexObjectGenericAnimation::TrackInfo* > m_tracks;
};

rexResult rexConverterMaxAnimation::GetExprControlData(rexObjectGenericAnimation* mp_Animation) const
{
	rexResult RetVal(rexResult::PERFECT);

	if(m_ExprControlNodes.length() <= 0)
		return RetVal;

	int nFrames = (int)m_EndFrame - (int)m_StartFrame;

	float st, et;
	st = rexMaxUtility::GetAnimStartTime();
	et = rexMaxUtility::GetAnimEndTime();

	//Create the generic chunk (if it doesn't already exist)
	if(!mp_Animation->m_GenericChunk )
		mp_Animation->m_GenericChunk = new rexObjectGenericAnimation::ChunkInfo();

	atArray<INode*> exprCtrlNodes;
	
	//Split the semi-colon delimited string of expression control node names
	char* szBuffer = new char[m_ExprControlNodes.length()];
	strcpy(szBuffer, m_ExprControlNodes.c_str());
	char* szNodeName = strtok(szBuffer, ";");
	while(szNodeName)
	{
		INode* p_Node = rexMaxUtility::FindNode(GetCOREInterface()->GetRootNode(), atString(szNodeName));
		if(p_Node)
			exprCtrlNodes.PushAndGrow(p_Node);
		else
		{
			RetVal.Combine(rexResult::ERRORS);
			RetVal.AddError("Failed to locate expression control node '%s' in the scene.", szNodeName);
		}
		szNodeName = strtok(NULL, ";");
	}
	delete [] szBuffer;

	atArray<ExpressionCtrlTrackData*> nodeTrackData;

	int exprCtrlNodeCount = exprCtrlNodes.GetCount();
	for(int nodeIdx = 0; nodeIdx < exprCtrlNodeCount; nodeIdx++)
	{
		INode* p_Node = exprCtrlNodes[nodeIdx];

		ExpressionCtrlTrackData* pData = new ExpressionCtrlTrackData();
		pData->mp_Node = p_Node;

		//Add the translation track
		rexObjectGenericAnimation::TrackInfo* pTransTrackInfo = new rexObjectGenericAnimation::TrackInfoVector3();
		pTransTrackInfo->m_TrackID = rexObjectGenericAnimation::TRACK_FACIAL_TRANSLATION;
		pTransTrackInfo->m_Name = p_Node->GetName();
		pTransTrackInfo->m_StartTime = st;
		pTransTrackInfo->m_EndTime = et;
		pTransTrackInfo->ResetKeys(nFrames);

		pData->m_tracks.PushAndGrow( pTransTrackInfo );
		pData->m_attrNames.PushAndGrow(atString("translate"));

		mp_Animation->m_GenericChunk->m_Tracks.PushAndGrow(pTransTrackInfo);		

		//Add the rotation track
		rexObjectGenericAnimation::TrackInfo* pRotTrackInfo = new rexObjectGenericAnimation::TrackInfoQuaternion();
		pRotTrackInfo->m_TrackID = rexObjectGenericAnimation::TRACK_FACIAL_ROTATION;
		pRotTrackInfo->m_Name = p_Node->GetName();
		pRotTrackInfo->m_StartTime = st;
		pRotTrackInfo->m_EndTime = et;
		pRotTrackInfo->ResetKeys(nFrames);

		pData->m_tracks.PushAndGrow( pRotTrackInfo );
		pData->m_attrNames.PushAndGrow(atString("rotate"));

		mp_Animation->m_GenericChunk->m_Tracks.PushAndGrow(pRotTrackInfo);

		Assert(pData->m_attrNames.GetCount() == pData->m_tracks.GetCount());
		nodeTrackData.PushAndGrow(pData);
	}

	//Sample the control data..
	atArray<Matrix34*> FrameMatrices;
	FrameMatrices.Grow(nFrames);
	FrameMatrices.Resize(nFrames);
	for(int initIndex = 0;initIndex<nFrames;initIndex++)
		FrameMatrices[initIndex] = new Matrix34();

	int nNodes = nodeTrackData.GetCount();
	for(int nodeIdx = 0; nodeIdx < nNodes; nodeIdx++)
	{
		ExpressionCtrlTrackData* pData = nodeTrackData[nodeIdx];
		::Control* pControl = pData->mp_Node->GetTMController();

		for(float frame = m_StartFrame; frame < m_EndFrame; frame++)
		{
			TimeValue time = (int)(frame / rexMaxUtility::GetFPS()) * rexMaxUtility::GetTicksPerSec();

			::Matrix3 transform(1);
			Interval valid;
			pControl->GetValue(time, &transform, valid, CTRL_RELATIVE);

			Matrix34 rageTransform;
			rexMaxUtility::CopyMaxMatrixToRageMatrix(transform, rageTransform);
			(*FrameMatrices[(int)frame - (int)m_StartFrame]) = rageTransform;
		}

		AddNodeToTracks(FrameMatrices, (int)m_StartFrame, (int)m_EndFrame, pData->m_tracks);
	}

	return RetVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxAnimation::Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const
{
	rexResult RetVal(rexResult::PERFECT);
	atString Out("exporting animation: ");

	Out += defaultName;
	RetVal.AddMessage(Out);

	s32 InputObjectCount = inputObjectArray.GetCount();

	if(m_ProgressBarObjectCountCB)
		(*m_ProgressBarObjectCountCB)(InputObjectCount);

	//If there are no input objects, and we are not exporting expression based control data
	//then just early out...
	if( (InputObjectCount == 0) && (m_ExprControlNodes.length() == 0) )
		return RetVal;
#if HACK_GTA4
	// This same check is done late ron in the function in the head
	rexMaxObject* p_Obj = dynamic_cast<rexMaxObject*>(inputObjectArray[0]);

	if(!p_Obj)
		return RetVal;
#endif // HACK_GTA4
	mp_AnimSegment = new rexObjectGenericAnimationSegment;
	mp_AnimSegment->SetName(defaultName);
	mp_AnimSegment->m_StartTime = rexMaxUtility::GetAnimStartTime();
	if(m_OverrideStartFrame){ mp_AnimSegment->m_StartTime = (m_StartFrame / rexMaxUtility::GetFPS()); } // Override
	mp_AnimSegment->m_EndTime = rexMaxUtility::GetAnimEndTime();
	if(m_OverrideEndFrame){ mp_AnimSegment->m_EndTime = (m_EndFrame / rexMaxUtility::GetFPS()); } // Override
	mp_AnimSegment->m_Autoplay = true;
	mp_AnimSegment->m_Looping = true;
	mp_AnimSegment->m_IgnoreChildBones = false;

	mp_Animation = new rexObjectGenericAnimation;
	mp_Animation->m_StartTime = 0.0f;
	if(m_OverrideStartFrame){ mp_Animation->m_StartTime = (m_StartFrame / rexMaxUtility::GetFPS()); } // Override
	mp_Animation->m_EndTime = rexMaxUtility::GetAnimEndTime() - mp_Animation->m_StartTime;
	if(m_OverrideEndFrame){ mp_Animation->m_EndTime = (m_EndFrame / rexMaxUtility::GetFPS()); } // Override
	mp_Animation->m_FramesPerSecond = rexMaxUtility::GetFPS();
	mp_Animation->m_ParentMatrix.Identity();
	mp_Animation->m_ContainedObjects.PushAndGrow(mp_AnimSegment,1);
	mp_Animation->m_AuthoredOrientation = GetAuthoredOrientation();

#if HACK_GTA4
	// As far as I can tell we are doing exactly the same thing except for an else on if(m_ExportUVAnimIndex != -1)
	// and there's no check for if( InputObjectCount ) since the function would early exit prior to this check anyway
	INode* p_Node = p_Obj->GetMaxNode();
	TCHAR* p_Name = p_Node->GetName();
	INode* p_NodeRoot = p_Node;

	m_StartFrame = (mp_Animation->m_StartTime * (float)mp_Animation->m_FramesPerSecond);
	m_EndFrame = (mp_Animation->m_EndTime * (float)mp_Animation->m_FramesPerSecond);
	m_EndFrame +=2;

	atArray<Texmap*> TexMapList;
	rexMaxUtility::GetMainTexMapsFromMaxMeshNode(p_Node,TexMapList);
	bool foundAnimation = false;

	if(TexMapList.GetCount() > 0)
	{
		s32 CurrentFrame = rexMaxUtility::GetAnimFrame();
		s32 j;

		for(j=0;j<TexMapList.GetCount();j++)
		{
			if(!TexMapList[j])
				continue;

			Control* p_ControlCoord = (Control*)TexMapList[j]->SubAnim(0);
			Control* p_ControlParam = (Control*)p_ControlCoord->SubAnim(0);
			Control* p_ControlU = (Control*)p_ControlParam->SubAnim(0);
			Control* p_ControlV = (Control*)p_ControlParam->SubAnim(1);

			char* texMapName = TexMapList[j]->GetFullName();
			char* subAnimName = p_ControlCoord->SubAnimName(0);
			char* subsubAnimName = p_ControlParam->SubAnimName(1);
			Interval theIntervalU;
			Interval theIntervalV;

			theIntervalU = p_ControlU->GetTimeRange(TIMERANGE_ALL);
			theIntervalV = p_ControlV->GetTimeRange(TIMERANGE_ALL);

			// new indicator to decide to bail out
			if(theIntervalU.Duration()<=0 || theIntervalV.Duration()<=0)
				continue;

			if (theIntervalU.Start() < 0)
				theIntervalU.SetStart(0);
			if (theIntervalV.Start() < 0)
				theIntervalV.SetStart(0);

			int startInt;
			int endInt;

			if (theIntervalU.Start() < theIntervalV.Start() && theIntervalU.Start() >= 0)
				startInt = static_cast<int>(floorf((float)theIntervalU.Start()));
			else
				startInt = static_cast<int>(floorf((float)theIntervalV.Start()));

			if (theIntervalU.End() > theIntervalV.End())
				endInt = static_cast<int>(floorf((float)theIntervalU.End()));
			else
				endInt = static_cast<int>(floorf((float)theIntervalV.End()));

			if(startInt>=endInt)
				continue;

			foundAnimation = true;

			int fps = static_cast<int>(rexMaxUtility::GetFPS());
			float start = (float)startInt / (float)TicksPerSec; 
			float end = (float)endInt / (float)TicksPerSec;

			m_StartFrame = (int)(start * rexMaxUtility::GetFPS());
			m_EndFrame = (int)(end * rexMaxUtility::GetFPS());

			mp_Animation->m_StartTime = 0.0f;
			mp_Animation->m_EndTime = (m_EndFrame/mp_Animation->m_FramesPerSecond) - mp_Animation->m_StartTime;
			mp_AnimSegment->m_EndTime = (m_EndFrame/mp_Animation->m_FramesPerSecond);
		}
	}

	bool DoSelectedExport = false;

	if(m_ExportAnimSelected)
	{
		p_Node = rexMaxUtility::GetNextSelected(p_Node);

		if(p_Node != p_NodeRoot)
		{
			DoSelectedExport = true;
		}
	}

	if(DoSelectedExport)
	{
		p_Node = p_Node->GetParentNode();
		s32 i,ChildCount = p_Node->NumberOfChildren();

		for(i=0;i<ChildCount;i++)
		{
			INode* p_ChildNode = p_Node->GetChildNode(i);

			if(p_ChildNode->Selected())
			{
				RetVal.Combine(ExportRootChuck(p_ChildNode,p_Obj));
			}
		}
	}
	else
	{
		RetVal.Combine(ExportRootChuck(p_Node,p_Obj));
	}

	GetTagInfoRec(mp_Animation,p_NodeRoot);

	if(m_ExportUVAnimIndex != -1)
		GetUVAnimRec(mp_Animation,p_Node);

	if(m_IsCamera)
		GetInterpolationInfoRec(mp_Animation,p_Node);

#else
	
	m_StartFrame = s32(mp_Animation->m_StartTime * (float)mp_Animation->m_FramesPerSecond);
	m_EndFrame = s32(mp_Animation->m_EndTime * (float)mp_Animation->m_FramesPerSecond);
	m_EndFrame +=2;

	if( InputObjectCount )
	{
		Assert(InputObjectCount == 1);

		rexMaxObject* p_Obj = dynamic_cast<rexMaxObject*>(inputObjectArray[0]);

		if(!p_Obj)
			return RetVal;

		INode* p_Node = p_Obj->GetMaxNode();
		TCHAR* p_Name = p_Node->GetName();
		INode* p_NodeRoot = p_Node;

		if(m_ExportUVAnimIndex != -1)
		{
			atArray<Texmap*> TexMapList;
			rexMaxUtility::GetMainTexMapsFromMaxMeshNode(p_Node,TexMapList);

			if(TexMapList.GetCount() > 0)
			{
				s32 CurrentFrame = rexMaxUtility::GetAnimFrame();
				s32 j;

				for(j=0;j<TexMapList.GetCount();j++)
				{
					if(!TexMapList[j])
						continue;

					if(j != m_ExportUVAnimIndex)
						continue;

					Control* p_ControlCoord = (Control*)TexMapList[j]->SubAnim(0);
					Control* p_ControlParam = (Control*)p_ControlCoord->SubAnim(0);
					Control* p_ControlU = (Control*)p_ControlParam->SubAnim(0);
					Control* p_ControlV = (Control*)p_ControlParam->SubAnim(1);

					char* texMapName = TexMapList[j]->GetFullName();
					char* subAnimName = p_ControlCoord->SubAnimName(0);
					char* subsubAnimName = p_ControlParam->SubAnimName(1);
					Interval theIntervalU;
					Interval theIntervalV;

					theIntervalU = p_ControlU->GetTimeRange(TIMERANGE_ALL);
					theIntervalV = p_ControlV->GetTimeRange(TIMERANGE_ALL);

					if (theIntervalU.Start() < 0)
						theIntervalU.SetStart(0);
					if (theIntervalV.Start() < 0)
						theIntervalV.SetStart(0);

					int startInt;
					int endInt;

					if (theIntervalU.Start() < theIntervalV.Start() && theIntervalU.Start() >= 0)
						startInt = static_cast<int>(floorf(theIntervalU.Start()));
					else
						startInt = static_cast<int>(floorf(theIntervalV.Start()));

					if (theIntervalU.End() > theIntervalV.End())
						endInt = static_cast<int>(floorf(theIntervalU.End()));
					else
						endInt = static_cast<int>(floorf(theIntervalV.End()));

					int fps = rexMaxUtility::GetFPS();
					m_StartFrame = startInt / (rexMaxUtility::GetFPS());
					m_EndFrame = endInt / (rexMaxUtility::GetFPS());

					m_StartFrame = static_cast<int>(m_StartFrame / 5);
					m_EndFrame = static_cast<int>(m_EndFrame / 5);

					mp_Animation->m_StartTime = 0.0f;
					mp_Animation->m_EndTime = (m_EndFrame/mp_Animation->m_FramesPerSecond) - mp_Animation->m_StartTime;
					mp_AnimSegment->m_EndTime = (m_EndFrame/mp_Animation->m_FramesPerSecond);
				}
			}
		}
		
		bool DoSelectedExport = false;

		if(m_ExportAnimSelected)
		{
			p_Node = rexMaxUtility::GetNextSelected(p_Node);

			if(p_Node != p_NodeRoot)
			{
				DoSelectedExport = true;
			}
		}

		if(DoSelectedExport)
		{
			p_Node = p_Node->GetParentNode();
			s32 i,ChildCount = p_Node->NumberOfChildren();

			for(i=0;i<ChildCount;i++)
			{
				INode* p_ChildNode = p_Node->GetChildNode(i);

				if(p_ChildNode->Selected())
				{
					ExportRootChuck(p_ChildNode,p_Obj);
				}
			}
		}
		else
		{
			ExportRootChuck(p_Node,p_Obj);
		}

		GetTagInfoRec(mp_Animation,p_NodeRoot);
		
		if(m_ExportUVAnimIndex != -1)
			GetUVAnimRec(mp_Animation,p_Node);

		if(m_IsCamera)
			GetInterpolationInfoRec(mp_Animation,p_Node);
	}
#endif // HACK_GTA4
	RetVal.Combine(GetExprControlData(mp_Animation));

	outputObject = mp_Animation;

	return RetVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::IncludeFigureMode(rexObjectGenericAnimation::ChunkInfo* p_RootChunk) const
{
	atArray<Matrix34*>& r_RootMatrices = *m_outChunkData.Access(p_RootChunk);
	s32 i;

	for(i=0;i<(m_EndFrame - m_StartFrame);i++)
	{
		Vector3 PosSave = r_RootMatrices[i]->d;
		Matrix34 RootMatDelta = m_MatSetRoot;
		RootMatDelta.Transpose();

		r_RootMatrices[i]->d = Vector3(0,0,0);
		r_RootMatrices[i]->Dot(RootMatDelta);
		r_RootMatrices[i]->d = PosSave;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::DoRootTransformMove(rexObjectGenericAnimation::ChunkInfo* p_RootChunk) const
{
	atArray<Matrix34*>& r_RootMatrices = *m_outChunkData.Access(p_RootChunk);
	atArray<Matrix34*>* r_ChildMatrices[32];

	s32 i,j,NumChildren,Count = m_outChunkData.GetNumSlots();

	NumChildren = 0;

	for(i=0;i<Count;i++)
	{
		atMap<rexObjectGenericAnimation::ChunkInfo*,atArray<Matrix34*> >::Entry* p_Entry = m_outChunkData.GetEntry(i);

		while(p_Entry)
		{
			if(p_Entry->key->m_Parent == p_RootChunk)
			{
				r_ChildMatrices[NumChildren++] = &p_Entry->data;
			}

			p_Entry = p_Entry->next;
		}
	}

	for(i=0;i<(m_EndFrame - m_StartFrame);i++)
	{
		Vector3 PosSave = r_RootMatrices[i]->d;
		r_RootMatrices[i]->d = Vector3(0.0f,0.0f,0.0f);

		Matrix34 RootMatDelta = m_MatSetRoot;
		RootMatDelta.Inverse();
		RootMatDelta.DotFromLeft(*r_RootMatrices[i]);

		(*r_RootMatrices[i]) = m_MatSetRoot;
		r_RootMatrices[i]->d = PosSave;

		for(j=0;j<NumChildren;j++)
		{
			(*r_ChildMatrices[j])[i]->Dot(RootMatDelta);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::InitTracksInfo(int NumFrames,atArray<rexObjectGenericAnimation::TrackInfo*>& r_Tracks,const atArray<atString>& r_TrackNames,const atArray<int>& r_TrackIDs) const
{
	s32 TrackCount = r_TrackNames.GetCount();
	r_Tracks.Reset();
	r_Tracks.Reserve(TrackCount);

	for(s32 i=0;i<TrackCount;i++)
	{
		rexObjectGenericAnimation::TrackInfo* p_Track = 0;

		switch(r_TrackIDs[i])
		{
		case rexObjectGenericAnimation::TRACK_TRANSLATE:
		case rexObjectGenericAnimation::TRACK_FACIAL_TRANSLATION:
		case rexObjectGenericAnimation::TRACK_SCALE:
			p_Track = new rexObjectGenericAnimation::TrackInfoVector3();
			break;

		case rexObjectGenericAnimation::TRACK_ROTATE:
		case rexObjectGenericAnimation::TRACK_FACIAL_ROTATION:
			p_Track = new rexObjectGenericAnimation::TrackInfoQuaternion();
			break;
#if HACK_GTA4
		// More viseme related changes
		case rexObjectGenericAnimation::TRACK_VISEMES:
			p_Track = new rexObjectGenericAnimation::TrackInfoFloat();
			break;
#endif // HACK_GTA4
		}

		if(!p_Track)
			continue;

		p_Track->m_Name = r_TrackNames[i];
		p_Track->m_TrackID = r_TrackIDs[i];
		p_Track->m_StartTime = rexMaxUtility::GetAnimStartTime();
		p_Track->m_EndTime = rexMaxUtility::GetAnimEndTime();
		p_Track->m_IsLocked = false;
		p_Track->ResetKeys(NumFrames);

		r_Tracks.Append() = p_Track;
	}
}

#if HACK_GTA4
// More viseme related changes
void rexConverterMaxAnimation::AddGenericNodeToTracks(const atArray<atArray<Matrix34*>>& r_GenericValues,int StartFrame,int EndFrame,atArray<rexObjectGenericAnimation::TrackInfo*>& r_Tracks) const
{
	for(s32 f=StartFrame;f<EndFrame;f++)
	{
		s32 TrackCount = r_Tracks.GetCount();

		for(s32 t=0;t<TrackCount;t++)
		{
			AddNodeToTrackAtFrame(r_GenericValues[t][f - StartFrame],*(r_Tracks[t]));
		}

		// cleaning up due to pointers being passed now.
		for(s32 t=0;t<TrackCount;t++)
			delete r_GenericValues[t][f - StartFrame];
	}
}
#endif // HACK_GTA4

////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::AddNodeToTracks(const atArray<Matrix34*>& r_SourceMatrices,int StartFrame,int EndFrame,atArray<rexObjectGenericAnimation::TrackInfo*>& r_Tracks) const
{
	s32 TrackCount = r_Tracks.GetCount();
	for(s32 t=0;t<TrackCount;t++)
	{
		if(	r_Tracks[t]->m_TrackID == rexObjectGenericAnimation::TRACK_TRANSLATE ||
			r_Tracks[t]->m_TrackID == rexObjectGenericAnimation::TRACK_FACIAL_TRANSLATION ||
			r_Tracks[t]->m_TrackID == rexObjectGenericAnimation::TRACK_SCALE ||
			r_Tracks[t]->m_TrackID == rexObjectGenericAnimation::TRACK_ROTATE ||
			r_Tracks[t]->m_TrackID == rexObjectGenericAnimation::TRACK_FACIAL_ROTATION ||
			r_Tracks[t]->m_TrackID == rexObjectGenericAnimation::TRACK_VISEMES)
		{
			r_Tracks[t]->ResetKeys(EndFrame-StartFrame);
		}
	}
	for(s32 f=StartFrame;f<EndFrame;f++)
	{
		for(s32 t=0;t<TrackCount;t++)
		{
			AddNodeToTrackAtFrame(r_SourceMatrices[f - StartFrame],*(r_Tracks[t]));
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////
void rexConverterMaxAnimation::AddNodeToTrackAtFrame(const Matrix34* p_SourceMatrix,rexObjectGenericAnimation::TrackInfo& r_Track) const
{
	const Matrix34& r_SourceMatrix = *p_SourceMatrix;
	switch(r_Track.m_TrackID)
	{
	case rexObjectGenericAnimation::TRACK_TRANSLATE:
	case rexObjectGenericAnimation::TRACK_FACIAL_TRANSLATION:
		{
			Vector3 v = r_SourceMatrix.GetVector(3);
			r_Track.AppendVector3Key(v);
		}
		break;
	case rexObjectGenericAnimation::TRACK_SCALE:
		{
			Matrix3 maxMatrix;
			rexMaxUtility::CopyRageMatrixToMaxMatrix(r_SourceMatrix,maxMatrix);
			Vec3V matrixScale = ScaleFromMat33VTranspose(RCC_MAT34V(r_SourceMatrix).GetMat33());
			Vector3 v = Vector3(matrixScale.GetX().Getf(),matrixScale.GetY().Getf(),matrixScale.GetZ().Getf());
			r_Track.AppendVector3Key(v);
		}
		break;
	case rexObjectGenericAnimation::TRACK_ROTATE:
	case rexObjectGenericAnimation::TRACK_FACIAL_ROTATION:
		{
			// We now support scaling so we need to take this into account with the rotations
			Mat34V mat = RCC_MAT34V(r_SourceMatrix);
			for(int i=0;i<3;i++)
			{
				if(MagSquared(mat[i]).Getf()==0)
				{
					Scale(mat,Vec3V(V_ZERO),mat);
					break;
				}
			}

			QuatV q = QuatVFromMat33VSafe(mat.GetMat33(), QuatV(V_IDENTITY));
			r_Track.AppendQuaternionKey(RCC_QUATERNION(q));
		}
		break;
#if HACK_GTA4
// More viseme related changes
	case rexObjectGenericAnimation::TRACK_VISEMES:
		{
			Vector3 v = r_SourceMatrix.GetVector(3);
			r_Track.AppendFloatKey(v.x);
		}
		break;
#endif
	default:
		return;
	}
}
