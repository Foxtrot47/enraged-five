// 
// rexMaxToGeneric/converterParticle.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REX_MAX_ROCKSTAR_CONVERTERPARTICLE_H__
#define __REX_MAX_ROCKSTAR_CONVERTERPARTICLE_H__

#include "atl/array.h"
#include "rexmaxtogeneric/converterbasic.h"
#include "rexGeneric/objectParticle.h"

namespace rage {

	/////////////////////////////////////////////////////////////////////////////////////

	class rexConverterMaxParticle : public rexConverter
	{
	public:
		rexConverterMaxParticle() : rexConverter() {}

		virtual rexResult		Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
		virtual rexConverter*   CreateNew() const  { return new rexConverterMaxParticle; }

		virtual rexObjectGenericParticleGroup* CreateNewParticleGroup() const { return new rexObjectGenericParticleGroup; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

}//End namespace rage

#endif //__REX_MAX_ROCKSTAR_CONVERTERPARTICLE_H__
