#ifndef __REX_MAX_ROCKSTAR_CONVERTERENTITY_H__
#define __REX_MAX_ROCKSTAR_CONVERTERENTITY_H__

#include "rexBase/converter.h"
#include "rexBase/property.h"

namespace rage {

/* PURPOSE:
	property to set the filename of the entity type file that is created (usually "entity.type")
*/
class rexPropertyMaxEntityTypeFileName : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxEntityTypeFileName; }
};

/* PURPOSE:
	property to set on/off the skipping of the bounds sections. This is used in a fragment export
*/
class rexPropertyMaxEntitySkipBoundSectionOfTypeFile : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxEntitySkipBoundSectionOfTypeFile; }
};

/* PURPOSE:
	property to set on/off the writing of the bounds situations
*/
class rexPropertyMaxEntityWriteBoundSituations : public rexProperty
{
public:
	virtual rexResult SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMaxEntityWriteBoundSituations; }
};

/* PURPOSE:
	converts from max data to the rex generic entity format
*/
class rexConverterMaxEntity : public rexConverter
{
public:
	virtual rexResult			Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexConverter*		CreateNew() const { return new rexConverterMaxEntity; }
};

} //namespace rage {

#endif //__REX_MAX_ROCKSTAR_CONVERTERENTITY_H__
