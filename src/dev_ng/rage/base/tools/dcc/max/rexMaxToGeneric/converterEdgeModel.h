#ifndef __REX_MAX_ROCKSTAR_CONVERTEREDGEMODEL_H__
#define __REX_MAX_ROCKSTAR_CONVERTEREDGEMODEL_H__

#include "rexmaxtogeneric/converterbasic.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////
class rexConverterMaxEdgeModelHierarchy : public rexConverter
{
public:
	rexConverterMaxEdgeModelHierarchy();
protected:
	virtual rexResult Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const;
	virtual rexResult ConvertSubObject(const rexObject& object,rexObject*& newObject) const;
	virtual rexResult ConvertSubObject(INode* p_Node,rexObject*& newObject) const;
	virtual bool IsAcceptableHierarchyNode(INode* p_Node) const;
	virtual rexConverter* CreateNew() const { return new rexConverterMaxEdgeModelHierarchy; }

	mutable rexObjectHierarchy* mp_Hier;
};

} // namespace rage {

#endif //__REX_MAX_ROCKSTAR_CONVERTEREDGEMODEL_H__
