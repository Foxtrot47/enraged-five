// 
// rexMayaToGeneric/converterLocator.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "rexMax/object.h"
#include "rexmaxutility/utility.h"
#include "rexmaxutility/maxobj.h"

#include "rexGeneric/objectLocator.h"
#include "rexMaxToGeneric/converterLocator.h"

namespace rage {

///////////////////////////////////////////////////////////////////////////////

rexResult rexConverterMaxLocator::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	rexObjectGenericLocatorGroup* locatorGroup = CreateNewLocatorGroup();

	int inputObjectCount = inputObjectArray.GetCount();
	
	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );

	for( int a = 0; a < inputObjectCount; a++ )
	{		
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)( a );

		rexMaxObject* p_Obj = dynamic_cast<rexMaxObject*>(inputObjectArray[a]);
		if(!p_Obj)
			continue;

		INode* pNode = p_Obj->GetMaxNode();
		if(!pNode)
			continue;

		if( rexMaxUtility::IsType(pNode,Class_ID(DUMMY_CLASS_ID ,0)) ||
			rexMaxUtility::IsType(pNode,Class_ID(POINTHELP_CLASS_ID ,0), true) )
		{
			if( m_ProgressBarTextCB )
			{
				static char message[1024];
				sprintf( message, "Converting locator data at %s", pNode->GetName() );
				(*m_ProgressBarTextCB) ( message );
			}

			rexObjectGenericLocator* locator = new rexObjectGenericLocator;
			
			//TODO : Get the proper bone index
			locator->m_BoneIndex = 0;
			locator->SetName( pNode->GetName() );

			Matrix34 WorldMat;
			rexMaxUtility::CopyMaxMatrixToRageMatrix(pNode->GetNodeTM(0),WorldMat);
			locator->m_Matrix = WorldMat;

			//Add the locator to the group
			locatorGroup->m_ContainedObjects.PushAndGrow( locator,(u16)inputObjectCount );
		}
	}

	outputObject = locatorGroup;

	return result;
}

///////////////////////////////////////////////////////////////////////////////

}//End namespace rage
