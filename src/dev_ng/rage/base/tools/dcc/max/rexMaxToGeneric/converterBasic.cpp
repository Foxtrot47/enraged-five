#include "rexMaxToGeneric/converterBasic.h"
#include "rexmaxutility/utility.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxHierarchy::Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const
{
	rexResult result(rexResult::PERFECT);

	// create object 
	rexObjectHierarchy* hier = CreateHierarchyNode();
	hier->SetName(rexMaxUtility::ReplaceWhitespaceCharacters(defaultName));
	Assert(hier);

	// get module-specific scene data here and fill out data structures as necessary
	int inputObjectCount = inputObjectArray.GetCount();

	if(m_ProgressBarObjectCountCB)
		(*m_ProgressBarObjectCountCB)(inputObjectCount);

	for(int a=0;a<inputObjectCount;a++)
	{		
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)(a);

		rexObject* subObject = NULL;
		rexResult thisResult = ConvertSubObject(*inputObjectArray[a],subObject);
		
		result.Combine(thisResult);
		
		if(thisResult.OK() && subObject)
			hier->m_ContainedObjects.PushAndGrow(subObject,(u16)inputObjectCount);
	}

	// set up return values
	outputObject = hier;
	
	return result;
}

/************************************************************************/
/* This is geting the next valid child even further down the hierarchy and the local index of the child */
/************************************************************************/
INodePtr rexConverterMaxHierarchy::MaxIterator::GetValidChildRec(const INodePtr p_CurrRoot, int startIndex)
{
	//try and go down the hierarchy
	s32 i,ChildCount = p_CurrRoot->NumberOfChildren();
	for(i=startIndex;i<ChildCount;i++)
	{
		m_CurrentChild.PushAndGrow(i,m_CurrentChild.GetCount() + 1);
		INodePtr pChild = p_CurrRoot->GetChildNode(i);
		if(IsValid(pChild))
			return pChild;

		INode* pValidChild = GetValidChildRec(pChild);
		if(pValidChild)
			return pValidChild;

		// if not valid or recursing, take the index off again.
		m_CurrentChild.Pop();
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
INode* rexConverterMaxHierarchy::MaxIterator::GetNextObject()
{
	if(!mp_MaxNext)
	{
		return NULL;
	}

	INode* p_MaxReturn = mp_MaxNext;
	const char* p_Ret = p_MaxReturn->GetName();

	bool bFound = false;

	//try and go down the hierarchy, local index of child to parent as we later go up again and use that.
	INodePtr p_Child = GetValidChildRec(mp_MaxNext);
	if(p_Child)
	{
		mp_MaxNext = p_Child;
		bFound = true;
	}
#if HACK_GTA4
	if(!bFound && !mp_MaxRoot)
	{
		mp_MaxRoot = mp_MaxNext;
		mp_MaxNext = NULL;

		if(IsValid(mp_MaxRoot))
			return mp_MaxRoot;
		else
			return NULL;
	}
#endif // HACK_GTA4

	while(!bFound)
	{
		//keep going up the hierarchy until we can go to the next sibling

		mp_MaxNext = rexMaxUtility::GetParent(mp_MaxNext);

		if(!mp_MaxNext)
		{
			return p_MaxReturn;
		}

		const char* p_Test = mp_MaxNext->GetName();
		s32 CurrentChild = m_CurrentChild.Pop() + 1;

		INodePtr p_Child = GetValidChildRec(mp_MaxNext, CurrentChild);
		if(p_Child)
		{
			mp_MaxNext = p_Child;
			bFound = true;
		}
		//have we got to the end of the iteration?
		if(!bFound && (mp_MaxNext == mp_MaxRoot))
		{
			mp_MaxNext = NULL;
			break;
		}
	}

	if(!mp_MaxRoot)
	{
		mp_MaxRoot = p_MaxReturn;
	}

	return p_MaxReturn;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
s32 rexConverterMaxHierarchy::MaxIterator::GetObjectCount() const
{
	MaxIterator* p_It = Create(mp_MaxRoot ? mp_MaxRoot : mp_MaxNext);

	s32 ObjectCount = 0;

	while(p_It->GetNextObject())
	{
		ObjectCount++;
	}

	delete p_It;

	return ObjectCount;
}
