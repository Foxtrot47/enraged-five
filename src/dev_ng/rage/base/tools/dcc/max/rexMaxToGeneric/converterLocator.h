// 
// rexMayaToGeneric/converterLocator.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REX_MAX_ROCKSTAR_CONVERTERLOCATOR_H__
#define __REX_MAX_ROCKSTAR_CONVERTERLOCATOR_H__

#include "atl/array.h"
#include "rexmaxtogeneric/converterbasic.h"
#include "rexGeneric/objectLocator.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterMaxLocator : public rexConverter
{
public:
	rexConverterMaxLocator() : rexConverter() {}

	virtual rexResult		Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexConverter*   CreateNew() const  { return new rexConverterMaxLocator; }
	
	virtual rexObjectGenericLocatorGroup* CreateNewLocatorGroup() const { return new rexObjectGenericLocatorGroup; }
};

/////////////////////////////////////////////////////////////////////////////////////

}//End namespace rage

#endif //__REX_MAX_ROCKSTAR_CONVERTERLOCATOR_H__
