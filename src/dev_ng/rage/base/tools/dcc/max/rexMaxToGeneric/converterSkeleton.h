#ifndef __REX_MAX_ROCKSTAR_CONVERTERSKELETON_H__
#define __REX_MAX_ROCKSTAR_CONVERTERSKELETON_H__

#include "atl/array.h"
#include "rexBase/converter.h"
#include "converterBasic.h"
#include "rexBase/property.h"
#include "rexGeneric/objectSkeleton.h"
#include <vector>

using namespace std;

namespace rage {

////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertySkeletonRootAtOrigin : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonRootAtOrigin; }
};
////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertySkeletonBoneTypes : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonBoneTypes; }
};

////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertySkeletonUseBoneIDs : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonUseBoneIDs; }
};

////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertySkeletonAuthoredOrient : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonAuthoredOrient; }
};

////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertySkeletonFlipConstraints : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonFlipConstraints; }
};
////////////////////////////////////////////////////////////////////////////////////////////////
class rexPropertySkeletonLimitToSkinBones : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonLimitToSkinBones; }
};

////////////////////////////////////////////////////////////////////////////////////////////////
class rexConverterMaxSkeleton : public rexConverter
{
public:
	rexConverterMaxSkeleton();

	virtual rexResult Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const;
	virtual rexConverter* CreateNew() const { return new rexConverterMaxSkeleton; }

	void SetRootAtOrigin(bool RootAtOrigin) { m_RootAtOrigin = RootAtOrigin; }
	void FlipConstraints(bool FlipConstraints) { m_FlipConstraints = FlipConstraints; }
	static void SetSkeletonBoneTypes(const char *classIdList);
	static void SetLimitToSkinBones(bool doLimit){s_bLimitToSkinBones = doLimit;}
	static bool GetLimitToSkinBones(){return s_bLimitToSkinBones;}

	bool		GetOmitMissingTracks() const { return m_bOmitMissingTracks; }
	void		SetOmitMissingTracks(bool bUse) { m_bOmitMissingTracks = bUse; }
	bool		GetUseAnimCtrlExportFile() const { return m_bUseAnimCtrlExportFile; }
	void		SetUseAnimCtrlExportFile(bool bUse) { m_bUseAnimCtrlExportFile = bUse; }
	bool		LoadAnimExportCtrlFile(const char* filePath)
	{
		bool loadSuccess = m_AnimExportCtrlSpec.LoadFromXML(filePath);
		for(int i=0;i<m_AnimExportCtrlSpec.GetNumTrackSpecs();i++)
			m_sSpecTrackBones.PushAndGrow(atString(m_AnimExportCtrlSpec.GetTrackSpec(i).GetNameExpr()), m_AnimExportCtrlSpec.GetNumTrackSpecs());
		return loadSuccess;
	}
	static bool	SpecFileBonesContain(const char* boneName)
	{
		atString testString(boneName);
		if(testString==atString("SKEL_ROOT"))
			testString = "root";
		return m_sSpecTrackBones.Find(testString)>=0;
	}
	static bool IsValidSkelBone(INodePtr pNode, rexResult *res=NULL);

	class MaxSkeletonIterator : public rexConverterMaxHierarchy::MaxIterator
	{
	public:
		MaxSkeletonIterator(INode* p_MaxRoot, ISkin* p_Skin=NULL, bool limitToSpecFileBones=false)
			: rexConverterMaxHierarchy::MaxIterator(p_MaxRoot)
			, m_pSkin(p_Skin)
			, m_bLimitToSpecFileBones(limitToSpecFileBones)
		{
		}
		rexConverterMaxHierarchy::MaxIterator* Create(INode* p_MaxRoot) const { return new MaxSkeletonIterator(p_MaxRoot); }
		/*	PURPOSE
			get the bone id assigned to a max node (tag=[num] in the user properties in max)
		*/
		s32 GetBoneOffset(ISkin* p_Skin,INode* p_BoneNode);
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		s32 GetObjectCount() const;

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		static void FlushCachedData() { m_sBoneOffsetMap.Reset(); }
	protected:
		virtual bool IsValid(INode* p_MaxNode, bool limitToSkin=false);
		ISkin* m_pSkin;
		bool m_bLimitToSpecFileBones;
	};

protected:
	rexResult ConvertRec(INode* p_Node,rexObjectGenericSkeleton::Bone& r_BoneParent,bool IsRoot = false) const;

	bool m_RootAtOrigin;
	bool m_FlipConstraints;
	static bool s_bLimitToSkinBones;
	static std::vector<Class_ID> m_vBoneTypes;
	static atMap<INode*,s32> m_sBoneOffsetMap;
	static atArray<atString> m_sSpecTrackBones;

	// track spec stuff
	bool				m_bUseAnimCtrlExportFile;
	bool				m_bOmitMissingTracks;
	AnimExportCtrlSpec	m_AnimExportCtrlSpec;
};

} //namespace rage {

#endif //__REX_MAX_ROCKSTAR_CONVERTERSKELETON_H__
