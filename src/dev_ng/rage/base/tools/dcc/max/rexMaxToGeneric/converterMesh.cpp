#include <guplib.h>

#include "rexMax/object.h"

#include "rexMaxUtility/utility.h"
#include "rexMaxUtility/maxMorpher.h"
#include "rexMaxUtility/maxobj.h"
#include "rexMaxToGeneric/converterMesh.h"
#include "rexMaxToGeneric/converterSkeleton.h"
#include "rexRage/serializerMesh.h"
#if HACK_GTA4
#include "rexRage/serializerEntity.h"
#endif // HACK_GTA4

#include "rexMax/wrapper.h"
#include "rexGeneric/objectBound.h"
#include "rexGeneric/objectMesh.h"

#include <string>
#include <queue>

#include "MaxUtil/MaxUtil.h"
#include "rageMaxLODHierarchy/HierarchyInterface.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshCustomVertexColour::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexConverterMaxMeshHierarchy* conv = dynamic_cast<rexConverterMaxMeshHierarchy*>(module.m_Converter);

	if(conv)
		conv->SetCustomVertexColour(atString(value.toString()));

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ApplyMeshTintData( const atArray<rexObjectGenericMesh*>& meshes )
{
	atArray<Vector4, 0, u32> colVertList;
	atArray<int, 0, u32> palIdxList;
	for( int a = 0; a < meshes.GetCount(); a++ )
	{	
		rexObjectGenericMesh* mesh = meshes[a];
		int primCount = mesh->m_Primitives.GetCount();
		for( int b = 0; b < primCount; b++ )
		{
			rexObjectGenericMesh::MaterialInfo& material = mesh->m_Materials[mesh->m_Primitives[b].m_MaterialIndex];

			int adjunctsCount = mesh->m_Primitives[ b ].m_Adjuncts.GetCount();
			for( int c = 0; c < adjunctsCount; c++ )
			{
				Vector4 temp = mesh->m_Primitives[ b ].m_Adjuncts[ c ].m_TintColour;
				colVertList.PushAndGrow(mesh->m_Primitives[ b ].m_Adjuncts[ c ].m_TintColour);
			}
		}
	}

	// Now generate our mesh hiearchy wide palette 
	atArray<ColourQuantizer::ColourRGB> colourPalette;
	if( colVertList.GetCount() > 3 )
		rexUtility::GenerateColourPaletteFromArray( colVertList, colourPalette, palIdxList, 256 );

	// Loop back through the meshes and update the palette index for each vert
	int palIdx = 0;
	for( int a = 0; a < meshes.GetCount(); a++ )
	{	
		rexObjectGenericMesh* mesh = meshes[a];
		int primCount = mesh->m_Primitives.GetCount();

		for( int b = 0; b < primCount; b++ )
		{
			rexObjectGenericMesh::MaterialInfo& material = mesh->m_Materials[mesh->m_Primitives[b].m_MaterialIndex];
			int adjunctsCount = mesh->m_Primitives[ b ].m_Adjuncts.GetCount();
			for( int c = 0; c < adjunctsCount; c++ )
			{
				// Only apply tint colour to the verts if the primitives material has a tint shader applied
				if (material.m_TintShaderApplied)
				{
					int temp = palIdxList[palIdx];
					if(material.m_TreeShaderApplied)
						mesh->m_Primitives[ b ].m_Adjuncts[ c ].m_Colors[ 1 ].w = (float(palIdxList[palIdx]) + 0.5f) / 255.0f;
					else
						mesh->m_Primitives[ b ].m_Adjuncts[ c ].m_Colors[ 0 ].z = (float(palIdxList[palIdx]) + 0.5f) / 255.0f;
				}
				palIdx++;
			}
		}
	}	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshGenerateTintIndices::ModifyProcessedObject( rexObject& object, rexValue value, const atArray<rexObject*>& objects  ) const
{
	if( value.toBool() )
	{	
		rexObjectGenericMeshHierarchy* hier = dynamic_cast<rexObjectGenericMeshHierarchy*>( &object );

		// Build a lod map of meshes
		atMap<int,atArray<rexObjectGenericMesh*>> lodMap;

		int childCount = hier->m_ContainedObjects.GetCount();
		for( int a = 0; a < childCount; a++ )
		{	
			rexObjectGenericMesh* mesh = dynamic_cast<rexObjectGenericMesh*>( hier->m_ContainedObjects[ a ] );
			
			if(lodMap.Access(mesh->m_LODLevel))
			{
				lodMap.Access(mesh->m_LODLevel)->PushAndGrow(mesh);
			}	
			else
			{
				atArray<rexObjectGenericMesh*> meshes;
				meshes.PushAndGrow(mesh);
				lodMap.Insert(mesh->m_LODLevel, meshes);
			}
		}

		// Collect the tint data from all meshes that share a lod level into a single palette
		for(int i=0; i<lodMap.GetNumUsed(); i++)
		{
			const atMap<int,atArray<rexObjectGenericMesh*>>::Entry* p_Entry = lodMap.GetEntry(i);
			const atArray<rexObjectGenericMesh*>& meshes = p_Entry->data;

			ApplyMeshTintData(meshes);
		}
	}
	return rexResult(rexResult::PERFECT);
}
template <class T> class BigArray : public atArray<T,0,unsigned int> {};
struct BoundMaterialInfo
{
	atString m_Name;
	int		 m_ProcModIdx;
};

rexResult GenerateBoundMaterialsRec( rexObject& object )
{
	rexResult result(rexResult::PERFECT);

	rexObjectGenericBound* bound = dynamic_cast<rexObjectGenericBound*>(&object);
	Assert(bound);
	if(bound)
	{
		int primCount = bound->m_Primitives.GetCount();

		atArray<rexObjectGenericMesh::MaterialInfo> materials;

		atArray<Vector4, 0, u32> tintColors;
		atArray<int, 0, u32> palIdxList;
		atArray<ColourQuantizer::ColourRGB> colourPalette;

		bool hasTintColours = false;
		for( int b = 0; b < primCount; b++ )
		{
			rexObjectGenericMesh::MaterialInfo matInfo = bound->m_Materials[bound->m_Primitives[ b ].m_MaterialIndex];
			int adjunctsCount = bound->m_Primitives[ b ].m_Adjuncts.GetCount();

			for( int c = 0; c < adjunctsCount; c++ )
			{	
				Vector4 tintCol = bound->m_Primitives[ b ].m_Adjuncts[ c ].m_TintColour;
				tintColors.PushAndGrow(tintCol);
				if(!hasTintColours && tintCol != Vector4(0.0f, 0.0f, 0.0f, 0.0f))
				{
					hasTintColours = true;
				}
			}
		}
		if(hasTintColours)
		{
			rexUtility::GenerateColourPaletteFromArray( tintColors, colourPalette, palIdxList, 32 );

			BigArray<rexObjectGenericMesh::ProceduralModifier> oldProcModList;
			for( int i = 0; i < primCount; i++ )
			{
				rexObjectGenericMesh::PrimitiveInfo* primInfo = &bound->m_Primitives[ i ];
				rexObjectGenericMesh::ProceduralModifier procMod;
				float scalez = 0.0f;
				float scalexyz = 0.0f;
				float density = 0.0f;
				Vector4 colRGB = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
				int adjunctsCount = primInfo->m_Adjuncts.GetCount();
				for( int j = 0; j < adjunctsCount; j++ )
				{
					colRGB.x += colourPalette[palIdxList[(i*adjunctsCount)+j]].r;
					colRGB.y += colourPalette[palIdxList[(i*adjunctsCount)+j]].g;
					colRGB.z += colourPalette[palIdxList[(i*adjunctsCount)+j]].b;

					scalexyz += primInfo->m_Adjuncts[ j ].m_ProceduralScaleXYZ;
					scalez += primInfo->m_Adjuncts[ j ].m_ProceduralScaleZ;
					density += primInfo->m_Adjuncts[ j ].m_ProceduralDensity;
				}

				procMod.m_ScaleZ = Clamp<float>(scalez / (float)adjunctsCount, 0.0f, 1.0f);
				procMod.m_ScaleXYZ = Clamp<float>(scalexyz / (float)adjunctsCount, 0.0f, 1.0f);
				procMod.m_Density = Clamp<float>(density / (float)adjunctsCount, 0.0f, 1.0f);

				procMod.m_TintColour = Vector3(
					Clamp<float>(colRGB.x / (float)adjunctsCount, 0.0f, 255.0f),
					Clamp<float>(colRGB.y / (float)adjunctsCount, 0.0f, 255.0f),
					Clamp<float>(colRGB.z / (float)adjunctsCount, 0.0f, 255.0f)
				);
				oldProcModList.PushAndGrow(procMod);
			}

			// Generate the new material list taking into account tint colour, density and scale
			atArray<rexObjectGenericMesh::MaterialInfo> oldMaterialList;
			atArray<rexObjectGenericMesh::MaterialInfo> newMaterialList;
			for( int i = 0; i < bound->m_Materials.GetCount(); i++ )
			{
				oldMaterialList.PushAndGrow( bound->m_Materials[i] );
			}

			bound->m_Materials.Reset();

			BigArray<BoundMaterialInfo>  bigBoundMaterialList;

			// First collapse it out to a one material per face
			for( int i = 0; i < primCount; i++ )
			{
				rexObjectGenericMesh::PrimitiveInfo* primInfo = &bound->m_Primitives[ i ];

				BoundMaterialInfo boundMatInfo;
				boundMatInfo.m_Name = oldMaterialList[primInfo->m_MaterialIndex].m_Name;
				boundMatInfo.m_ProcModIdx = i;

				bigBoundMaterialList.PushAndGrow(boundMatInfo);
				primInfo->m_MaterialIndex = i;
			}

			// Optimise procedural modifier list
			for( int i = 0; i < bigBoundMaterialList.GetCount(); i++ )
			{
				bool found = false;
				for( int j = 0; j < bound->m_ProceduralModifiers.GetCount(); j++ )
				{	
					if( bound->m_ProceduralModifiers[j] == oldProcModList[i] )
					{
						bigBoundMaterialList[i].m_ProcModIdx = j;
						found = true;
						break;
					}
				}
				if( !found )
				{
					bigBoundMaterialList[i].m_ProcModIdx = bound->m_ProceduralModifiers.GetCount();
					bound->m_ProceduralModifiers.PushAndGrow(oldProcModList[i]);
				}
			}
			oldProcModList.Reset();
			
			// Build a colour template containing the scaling and density values packed into alpha component
			for(int i = 0; i < bound->m_ProceduralModifiers.GetCount(); i++)
			{
				Vector4 col = Vector4(0.0f);
				col.x = bound->m_ProceduralModifiers[i].m_TintColour.x;
				col.y = bound->m_ProceduralModifiers[i].m_TintColour.y;
				col.z = bound->m_ProceduralModifiers[i].m_TintColour.z;
				
				int scaleDensity = round(bound->m_ProceduralModifiers[i].m_ScaleXYZ * 0xf);
				scaleDensity |= round(bound->m_ProceduralModifiers[i].m_ScaleZ * 0x3) << 4;
				scaleDensity |= round(bound->m_ProceduralModifiers[i].m_Density * 0x3) << 6;
				col.w = (float)scaleDensity;
				bound->m_colourTemplate.PushAndGrow(col);
			}
			
			// Optimise the colour template
			atArray<Vector4> optimisedColTemplate;
			for( int i = 0; i < bigBoundMaterialList.GetCount(); i++ )
			{
				Vector4 existing = bound->m_colourTemplate[bigBoundMaterialList[i].m_ProcModIdx];

				bool found = false;
				for( int j = 0; j < optimisedColTemplate.GetCount(); j++ )
				{
					Vector4 opti = optimisedColTemplate[j];

					if( optimisedColTemplate[j].x == bound->m_colourTemplate[bigBoundMaterialList[i].m_ProcModIdx].x &&
						optimisedColTemplate[j].y == bound->m_colourTemplate[bigBoundMaterialList[i].m_ProcModIdx].y &&
						optimisedColTemplate[j].z == bound->m_colourTemplate[bigBoundMaterialList[i].m_ProcModIdx].z &&
						optimisedColTemplate[j].w == bound->m_colourTemplate[bigBoundMaterialList[i].m_ProcModIdx].w )
					{
						bigBoundMaterialList[i].m_ProcModIdx = j;
						found = true;
						break;
					}

				}
				if(!found)
				{
					bigBoundMaterialList[i].m_ProcModIdx = optimisedColTemplate.GetCount();
					optimisedColTemplate.PushAndGrow( existing );
				}
			}

			// Then build our list of optimised materials
			for( int b = 0; b < primCount; b++ )
			{
				bool found = false;
				for( int c = 0; c < newMaterialList.GetCount(); c++ )
				{
					if( bigBoundMaterialList[b].m_Name == newMaterialList[c].m_Name && bound->m_ProceduralModifiers[bigBoundMaterialList[b].m_ProcModIdx] == bound->m_ProceduralModifiers[newMaterialList[c].m_ProceduralModIdx] )
					{
						bound->m_Primitives[ b ].m_MaterialIndex = c;
						found  = true;
						break;
					}
				}

				if( !found )
				{
					bound->m_Primitives[ b ].m_MaterialIndex = newMaterialList.GetCount();
					rexObjectGenericMesh::MaterialInfo matInfo;
					matInfo.m_Name = bigBoundMaterialList[b].m_Name;
					matInfo.m_ProceduralModIdx = bigBoundMaterialList[b].m_ProcModIdx;
					newMaterialList.PushAndGrow(matInfo);
				}
			}
			bigBoundMaterialList.Reset();

			bound->m_colourTemplate.Reset();
			if(hasTintColours)
			{
				for( int i = 0; i < optimisedColTemplate.GetCount(); i++ )
				{
					bound->m_colourTemplate.PushAndGrow( optimisedColTemplate[i] );
				}
			}

			// Finally build our final material list with the colour palette index suffix'd
			bound->m_Materials.Reset();
			for( int i = 0; i < newMaterialList.GetCount(); i++ )
			{
				//Set the colour index
				char buffer [32];
				if(hasTintColours)
				{
					_itoa(newMaterialList[i].m_ProceduralModIdx+1,buffer,10);
				}
				else
				{
					_itoa(newMaterialList[i].m_ProceduralModIdx,buffer,10);
				}
				newMaterialList[i].m_Name += "|";
				newMaterialList[i].m_Name += buffer;
				bound->m_Materials.PushAndGrow(newMaterialList[i]);
			}
			//}

			if(bound->m_Materials.GetCount() > 255)
			{
				result.Combine(rexResult::ERRORS);
				result.AddError("More than 255 materials have been generated from procedural type, collision type, procedural tint and procedural scaling and density. \nSee https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Excessive_collision_materials");
			}
			
			for(int i=0; i<bound->m_ContainedObjects.GetCount(); i++)
			{
				result.Combine(GenerateBoundMaterialsRec( *(bound->m_ContainedObjects[i]) ));
			}
		}
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundGenerateMaterials::ModifyProcessedObject( rexObject& object, rexValue value, const atArray<rexObject*>& objects  ) const
{
	rexResult result(rexResult::PERFECT);
	rexObjectGenericMeshHierarchy* hier = dynamic_cast<rexObjectGenericMeshHierarchy*>( &object );

	int childCount = hier->m_ContainedObjects.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		rexObjectGenericBound* bound = dynamic_cast<rexObjectGenericBound*>( hier->m_ContainedObjects[ a ] );
		GenerateBoundMaterialsRec( *bound );
	}
	

	return result;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshOptimiseMaterials::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexSerializerRAGEEntity* serE = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );
	rexConverterMaxMeshHierarchy* conv = dynamic_cast<rexConverterMaxMeshHierarchy*>(module.m_Converter);

	// The only time we use the non-default values (true is default) is for objects with UV animation
	// This, somewhat confusingly, means that materials on objects with UV anims will have an optimised 
	// material comparison (see line 1577 in objectMesh.cpp) when searching for duplicates while building 
	// the matInfoArray.  The opposite behavour is exhibited for the converter.  Further investigation required
	if(ser)
		ser->SetOptimiseMaterials( value.toBool() );
	if(serE)
		serE->SetOptimiseMaterials( value.toBool() );
	if(conv)
		conv->SetOptimiseMaterials(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshForSkeleton::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxMeshHierarchy* conv = dynamic_cast<rexConverterMaxMeshHierarchy*>(module.m_Converter);

	if(!conv)
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	conv->SetIsForSkeleton(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshSkinOffset::SetupModuleInstance(rexModule& module,rexValue value,const atArray<rexObject*>& objects) const
{
	rexConverterMaxMeshHierarchy* conv = dynamic_cast<rexConverterMaxMeshHierarchy*>(module.m_Converter);

	if(!conv)
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	conv->SetSkinOffset(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshAsAscii::SetupShell(const atString& objectName,rexShell& shell,rexValue value,const atArray<rexObject*>& objects) const
{
	rexSerializerRAGEMesh::SetAsciiMode(value.toBool());

	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshExportWorldSpace::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexConverterMaxMeshHierarchy* conv = dynamic_cast<rexConverterMaxMeshHierarchy*>(module.m_Converter);

	if(!conv)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	conv->SetExportWorldSpace(value.toBool());

	return rexResult(rexResult::PERFECT);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyCullVertexChannels::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterMaxMeshHierarchy* conv = dynamic_cast<rexConverterMaxMeshHierarchy*>( module.m_Converter );

	if( conv )
	{
		conv->SetCullVertexChannels( value.toInt( conv->GetCullVertexChannels() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyForcedAdditionalUVCount::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterMaxMeshHierarchy* conv = dynamic_cast<rexConverterMaxMeshHierarchy*>( module.m_Converter );

	if( conv )
	{
		conv->SetForcedAdditionalUVCount( value.toInt() );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyLimitToSkelSpecFileBones::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterMaxMeshHierarchy* conv = dynamic_cast<rexConverterMaxMeshHierarchy*>( module.m_Converter );

	if( conv )
	{
		conv->SetLimitToSpecFileBones( value.toBool() );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyUseWeightedNormals::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterMaxMeshHierarchy* conv = dynamic_cast<rexConverterMaxMeshHierarchy*>( module.m_Converter );

	if( conv )
	{
		conv->SetUseWeightedNormals( value.toBool() );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshBlindVertexDataChannels::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexConverterMaxMeshHierarchy* conv = dynamic_cast<rexConverterMaxMeshHierarchy*>(module.m_Converter);

	if(!conv)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	if( value.toString() )
	{
		std::string bdParamsStr = value.toString();

		size_t startParen = bdParamsStr.find_first_of('(');
		size_t endParen = bdParamsStr.find_last_of(')');
		if( (startParen == std::string::npos) ||
			(endParen == std::string::npos) )
		{
			return rexResult( rexResult::WARNINGS, rexResult::INVALID_INPUT );
		}

		size_t cIdx = startParen+1;
		while(cIdx < endParen)
		{
			atString bdNameStr;
			atString bdVDataChnIdStr;
			atString bdRuntimeIdStr;
			atString bdDefaultStr;

			//Parse out the name associated with this blind map channel data.
			while( (bdParamsStr[cIdx] != '|') &&
				   (bdParamsStr[cIdx] != ')') )
			{
				bdNameStr += bdParamsStr[cIdx++];
			}
			cIdx++;
			
			//Parse out the MAX vertex data channel id this data should be pulled from.
			while( (bdParamsStr[cIdx] != '|') &&
				   (bdParamsStr[cIdx] != ')') )
			{
				bdVDataChnIdStr += bdParamsStr[cIdx++];
			}
			cIdx++;

			//Parse out the RAGE blind data identifier that should be used to identify
			//this data at runtime.
			while( (bdParamsStr[cIdx] != '|') &&
				   (bdParamsStr[cIdx] != ')') )
			{
				bdRuntimeIdStr += bdParamsStr[cIdx++];
			}
			cIdx++;
			
			//Parse out the default value to use for this data if it is not present in the
			//mesh
			while( (bdParamsStr[cIdx] != ';') &&
				   (bdParamsStr[cIdx] != ')') )
			{
				bdDefaultStr += bdParamsStr[cIdx++];
			}
			cIdx++;

			//Convert the vertex data channel id
			int maxVDataChnId = (int)atoi((const char*)bdVDataChnIdStr);

			//Convert the runtime id
			int runtimeId = (int)atoi((const char*)bdRuntimeIdStr);

			//Convert the default value
			float defaultVal = (float)atof((const char*)bdDefaultStr);
		
			conv->AddBlindDataExport(bdNameStr, maxVDataChnId, runtimeId, defaultVal);
		}
	}
	
	return rexResult(rexResult::PERFECT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexConverterMaxMeshHierarchy::rexConverterMaxMeshHierarchy():
	m_IsForSkeleton(false),
	m_SkinOffset(true),
	m_ExportWorldSpace(false),
	m_OptimiseMaterials(true),
	m_CullVertexChannels(NO_VERTEX_CHANNEL_CULLING),
	m_ForcedAdditionalUVCount(0),
	m_LimitToSpecFileBones(false),
	m_UseWeightedNormals(true)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxMeshHierarchy::Convert(const atArray<rexObject*>& inputObjectArray,rexObject*& outputObject,const atString& defaultName) const
{
	rexResult result(rexResult::PERFECT);

	// create object
	rexObjectGenericMeshHierarchyDrawable* p_Hier = new rexObjectGenericMeshHierarchyDrawable;
//	p_Hier->m_IsLOD = true;

	mp_Hier = p_Hier;

	Assert(mp_Hier);

	mp_Hier->SetName(rexMaxUtility::ReplaceWhitespaceCharacters(defaultName));

	// get module-specific scene data here and fill out data structures as necessary
	int inputObjectCount = inputObjectArray.GetCount();

	if(m_ProgressBarObjectCountCB)
		(*m_ProgressBarObjectCountCB)(inputObjectCount);

	for(int a=0;a<inputObjectCount;a++)
	{
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)(a);

		rexObject* subObject = NULL;
		rexResult thisResult = ConvertSubObject(*inputObjectArray[a],subObject);

		result.Combine(thisResult);
	}

	// set up return values
	outputObject = mp_Hier;

	s32 i,Count = p_Hier->m_ContainedObjects.GetCount();

	for(i=0;i<Count;i++)
	{
 		rexObjectGenericMesh* pCurrMesh = (rexObjectGenericMesh*)p_Hier->m_ContainedObjects[i];
// 		atArray<float> LodThresholds;
// 		float lodin = pCurrMesh->m_LODThreshold;
// 		float lodout = pCurrMesh->m_LODOUTThreshold;
// 		LodThresholds.PushAndGrow(lodin, 2);
// 		LodThresholds.Push(lodout);
// 	
// 		int currLodGroup = pCurrMesh->m_LODGroupIndex;
// 		int currLodLevel = pCurrMesh->m_LODLevel;
// 		while(p_Hier->m_LODThresholds.GetCount()<(currLodGroup+1))
// 			p_Hier->m_LODThresholds.Grow(1);
// 		while(p_Hier->m_LODThresholds[currLodGroup].GetCount()<(currLodLevel+1))
// 			p_Hier->m_LODThresholds[currLodGroup].Grow(1);
// 		p_Hier->m_LODThresholds[currLodGroup][currLodLevel] = LodThresholds;
// 
		if(false == pCurrMesh->m_optimize)
			p_Hier->m_optimize = false;
	}

	return result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexConverterMaxMeshHierarchy::ConvertSubObject(const rexObject& object,rexObject*& newObject) const
{
	rexResult result(rexResult::PERFECT);
	const rexMaxObject* p_Obj = dynamic_cast<const rexMaxObject*>( &object );

	if(!p_Obj)
	{
		result.Set(rexResult::WARNINGS,rexResult::TYPE_INCOMPATIBLE);
		return result;
	}

	if(m_ProgressBarTextCB)
	{
		static char message[1024];
		sprintf(message,"Converting mesh data at %s",p_Obj->GetMaxNode()->GetName());
		(*m_ProgressBarTextCB)(message);
	}

	INode* p_Node = p_Obj->GetMaxNode();

	return ConvertSubObject(p_Node,newObject);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Utility function that will get the bone that is at the root of the skeleton that has
//// been tagged for export as part of the entity the supplied node is being exported for.
INode* GetTaggedRootBone(INode* p_Node)
{
	atArray<INode*> taggedSkeletonRootNodes;

	//Traverse through the tagged nodes looking for all the nodes that have been
	//tagged as skeleton roots.
	RexMaxWrapper& wrapper = RexMaxWrapper::GetInst();
	RexMaxGraphItem& graphRoot = wrapper.GetGraphRoot();

	std::queue<RexMaxGraphItem*>	graphTraversalQueue;

	//Seed the traversal queue with the children of the graph root
	RexMaxGraphItem* pChildItem = graphRoot.GetChild();
	if(!pChildItem)
		return NULL;

	graphTraversalQueue.push(pChildItem);
	while(pChildItem)
	{
		pChildItem = pChildItem->GetNext();
		if(pChildItem)
			graphTraversalQueue.push(pChildItem);
	}

	//Traverse through the modules that have been tagged for export
	//and locate any instances of the skeleton module
	while(!graphTraversalQueue.empty())
	{
		RexMaxGraphItem* pItem = graphTraversalQueue.front();
		graphTraversalQueue.pop();

		const atString& itemType = pItem->GetType();
		if(strcmpi((const char*)itemType, "Skeleton") == 0)
		{
			if(pItem->GetNodes().GetCount())
				taggedSkeletonRootNodes.PushAndGrow(pItem->GetNodes()[0]);
		}
		else
		{
			RexMaxGraphItem* pChildItem = pItem->GetChild();
			if(pChildItem)
			{
				graphTraversalQueue.push(pChildItem);
				while(1)
				{

					pChildItem = pChildItem->GetNext();
					if(pChildItem)
						graphTraversalQueue.push(pChildItem);
					else
						break;
				}
			}
		}
	}

	//Find the tagged skeleton root for the supplied node
	INode *pNodeSkelRoot = NULL;
	INode *pCurNode = p_Node;
	for(int i=0; i<taggedSkeletonRootNodes.GetCount(); i++)
	{
		while(pCurNode && (pCurNode != taggedSkeletonRootNodes[i]))
			pCurNode = pCurNode->GetParentNode();

		if(pCurNode != NULL)
		{
			pNodeSkelRoot = pCurNode;
			break;
		}
	}

	return pNodeSkelRoot;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterMaxMeshHierarchy::ConvertSubObject(INode* p_Node,rexObject*& newObject) const
{
	rexResult result(rexResult::PERFECT);

	if(IsAcceptableHierarchyNode(p_Node))
	{
		atString Out("exporting mesh: ");
		Out += p_Node->GetName();
		result.AddMessageCtx(p_Node->GetName(), Out);

		INode* p_RootBone = MaxUtil::GetSkinRootBone(MaxUtil::GetSkin(p_Node));

		rexMaxUtility::SetSkinBindPose(p_RootBone,true);

		rexObjectGenericMesh* mesh = new rexObjectGenericMesh();

		if(!mesh)
			return rexResult(rexResult::ERRORS,rexResult::OUT_OF_MEMORY);

		IFpSceneHierarchy* pRsSceneLink = GetSceneLinkInterface();
		INode *pBoneLink = p_Node;
		if(pRsSceneLink)
		{
			Tab<INodePtr> children;
			do 
			{
				children.ZeroCount();
				pRsSceneLink->GetChildren(eLinkType_DRAWLOD, pBoneLink, children);
				if(children.Count()>0)
					pBoneLink = children[0];
			} while (children.Count()>0);
		}

		INode* p_TaggedRootBone = GetTaggedRootBone(p_Node);
		if(pBoneLink)
			p_TaggedRootBone = GetTaggedRootBone(pBoneLink);
		if(p_TaggedRootBone)
			DebugPrint("Using %s for root bone of model %s\n", p_TaggedRootBone->GetName(), p_Node->GetName());
		rexConverterMaxSkeleton::MaxSkeletonIterator it(p_TaggedRootBone);

		//Determine the number of bones used in the skeleton being exported with this entity (if any)
		mesh->m_BoneCount = it.GetObjectCount();

		// Determine if this mesh is going to be a damaged mesh.
		atString IsDamageString = rexMaxUtility::GetProperty(p_Node,"isdamage");
		mesh->m_DamageState = IsDamageString == "true" ? true : false;

		//Determine the bone index this node represents in the skeleton
		s32 boneIndex = 0;
		if(!m_ExportWorldSpace)
		{
			INode* p_BoneNode = it.GetNextObject();
			while(p_BoneNode != NULL)
			{
				if(p_BoneNode == p_Node)
					break;
				else if(pBoneLink && p_BoneNode == pBoneLink)
					break;

				p_BoneNode = it.GetNextObject();
				++boneIndex;
			}
		}
 
		bool success = 
			rexMaxUtility::ConvertMaxMeshNodeIntoGenericMesh(
				p_Node,
				mesh,
				result,
				boneIndex,
				false,
				(m_ExportWorldSpace || m_bCombiningAcrossBones), //If we are combining across bones then export the mesh data in world space
				m_SkinOffset,
				m_IsForSkeleton,
				false,
				atString(""),
				0,
				m_OptimiseMaterials,
				0,
				m_CustomVertexColour,
				false,
				m_CullVertexChannels,
				m_ForcedAdditionalUVCount,
				m_LimitToSpecFileBones,
				m_UseWeightedNormals
			);
		if(!success)
		{
			result.Combine(rexResult::ERRORS);
			result.AddErrorCtx(p_Node->GetName(), "Problem converting node %s to generic mesh.", p_Node->GetName());
			return result;
		}

		rexMaxUtility::GetNodeFullPath(p_Node,mesh->m_FullPath);

		//Convert and blind map channel data that has been specified for export
		int blindChnCount = m_BlindDataNames.GetCount();
		if(blindChnCount)
		{
			for(int i=0; i<blindChnCount; i++)
			{
				mesh->m_BlindDataIds.PushAndGrow(m_BlindDataRuntimeIds[i]);
				mesh->m_BlindDataTypes.PushAndGrow(rexValue::FLOAT);
			}

			Object* p_Object = p_Node->EvalWorldState(0).obj;

			TriObject* p_TriObject = NULL;
			if(p_Object && p_Object->ClassID() == (Class_ID(TRIOBJ_CLASS_ID,0)))
			{
				p_TriObject = (TriObject*)p_Object;
			}
			else if(p_Object && p_Object->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID,0)))
			{
				p_TriObject = (TriObject*)p_Object->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID,0));
			}

			Mesh* p_Mesh = NULL;
			if(p_TriObject)
				p_Mesh = &(p_TriObject->GetMesh());

			if(p_Mesh)
			{
				int nVerts = p_Mesh->numVerts;
				for(int vIdx=0; vIdx<nVerts; vIdx++)
				{
					rexObjectGenericMesh::VertexInfo& gnrcVI = mesh->m_Vertices[vIdx];
					gnrcVI.m_BlindData.Resize(blindChnCount);

					for(int bIdx=0; bIdx<blindChnCount; bIdx++)
					{
						float blindValue;

						int vDataChnId = m_BlindDataVDataChnIds[bIdx];
						if(p_Mesh->vdSupport.GetSize() > (vDataChnId-1) && 
							p_Mesh->vdSupport[vDataChnId-1])
						{
							PerData& meshPerData = p_Mesh->vData[vDataChnId-1];
							float* pBlindValue = static_cast<float*>(meshPerData.Addr(vIdx));
							blindValue = *pBlindValue;
						}
						else
						{
							//The mesh doesn't have data for the specified cannel, so use the default
							blindValue = m_BlindDataDefaults[bIdx];
						}

						gnrcVI.m_BlindData[bIdx] = blindValue;
					}
				}
			}
		}
		
		atArray<Modifier*> morphModifiers;
		rexMaxUtility::GetMorphs(p_Node, &morphModifiers);
		int iTargetCount = 0;

		if (morphModifiers.GetCount() > 0)
		{
			for (int m = 0; m < morphModifiers.GetCount(); m++)
			{
				MorphR3* p_Morpher = static_cast<MorphR3*>(morphModifiers[m]);
				int nChan = p_Morpher->chanBank.size();

				for (int c = 0; c < nChan; c++)
				{
					morphChannel* p_MorphChn = &p_Morpher->chanBank[c];
#if HACK_GTA4
					// Bail if an inactive slot is hit.  Again this is probably related to 
					// the bogus rigs we got from IM but the checkin comment for these
					// changes is unrelated
					if (!p_MorphChn->mActive)
						break;

					iTargetCount++;
#else
					if(p_MorphChn->mActive)
						iTargetCount++;
#endif
				}
			}

			//Get the mesh for the base object
			TriObject* p_BaseTriObject = NULL;
			Mesh* p_BaseMesh = NULL;
			Object* p_BaseObject = p_Node->EvalWorldState(0).obj;
			if(p_BaseObject->ClassID() == (Class_ID(TRIOBJ_CLASS_ID,0)))
			{
				p_BaseTriObject = (TriObject*)p_BaseObject;
			}
			else if(p_BaseObject->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID,0)))
			{
				p_BaseTriObject = (TriObject*)p_BaseObject->ConvertToType(0,Class_ID(TRIOBJ_CLASS_ID,0));
			}

			Assert(p_BaseTriObject);
			p_BaseMesh = &(p_BaseTriObject->GetMesh());
			p_BaseMesh->buildNormals();

			if(iTargetCount)
			{
				//Allocate and initialize the vertex delta arrays
				int vertCount = mesh->m_Vertices.GetCount();
				for(int vertIdx=0; vertIdx<vertCount; vertIdx++)
				{
					mesh->m_Vertices[vertIdx].m_TargetDeltas.Reserve(iTargetCount);
					mesh->m_Vertices[vertIdx].m_TargetNormalDeltas.Reserve(iTargetCount);
					for(int i=0; i<iTargetCount;i++)
					{
						mesh->m_Vertices[vertIdx].m_TargetDeltas.Append().Set(0.0f,0.0f,0.0f);
						mesh->m_Vertices[vertIdx].m_TargetNormalDeltas.Append().Set(0.0f,0.0f,0.0f);
					}
				}

#if HACK_GTA4
				// This is required because the meshes that are skinned in max live in
				// a flat hierarchy but we want the verts relative to the bones they are
				// skinned to
				Matrix3 ModContext,ModContextInverse;
				ModContext.IdentityMatrix();

				ISkin* p_Skin = MaxUtil::GetSkin(p_Node);
				if(p_Skin)
				{
					INode* p_SkinRootBone = MaxUtil::GetSkinRootBone(p_Skin);
					Matrix3 BoneInitTMB,BoneTM;
					p_Skin->GetBoneInitTM(p_SkinRootBone,BoneInitTMB,false);
					BoneTM = p_SkinRootBone->GetNodeTM(0);

					BoneInitTMB.Invert();

					ModContext = BoneTM * BoneInitTMB;
				}

				ModContextInverse = ModContext;
				ModContextInverse.Invert();
#endif // HACK_GTA4

				int iTargetIndex = 0;

				for (int m = 0; m < morphModifiers.GetCount(); m++)
				{
					MorphR3* p_Morpher = static_cast<MorphR3*>(morphModifiers[m]);

					for(int chnIdx=0; chnIdx<MR3_NUM_CHANNELS; chnIdx++)
					{
						morphChannel* p_MorphChn = &p_Morpher->chanBank._First[chnIdx];
						if(!p_MorphChn->mActive)
							break;

						Assert(p_MorphChn->mNumPoints == vertCount);
						Mesh targetMesh = *p_BaseMesh;
#if HACK_GTA4
						ModContextInverse.TransformPoints(targetMesh.verts,vertCount);
#endif // HACK_GTA4
						for(int vertIdx=0; vertIdx<vertCount; vertIdx++)
						{
							targetMesh.verts[vertIdx] += p_MorphChn->mDeltas._First[vertIdx];
						}
#if HACK_GTA4
						ModContext.TransformPoints(targetMesh.verts,vertCount);
#endif // HACK_GTA4
						targetMesh.buildNormals();

						//Store the name of the target
						rexObjectGenericMesh::BlendTargetInfo& btInfo = mesh->m_BlendTargets.Grow();

						// TALL: More than 1 modifier denotes micro morphs.
						// Not sure if this will hold up long term, all depends on how we use the modifiers in the future.
						if (morphModifiers.GetCount() > 1)
							btInfo.m_Name = MaxUtil::GetUniqueBlendshapeMicroMorphName(p_Node, p_MorphChn->mName);
						else
							btInfo.m_Name = MaxUtil::GetUniqueBlendShapeName(p_Node, p_MorphChn->mName);

						btInfo.m_Name = rexMaxUtility::ReplaceWhitespaceCharacters(btInfo.m_Name);

						for(int vertIdx=0; vertIdx<vertCount; vertIdx++)	
						{
							//Point3 &deltaPt = p_MorphChn->mDeltas._First[vertIdx];
							Point3 &deltaPt = targetMesh.verts[vertIdx] - p_BaseMesh->verts[vertIdx];
							Point3 &deltaNrmPt = targetMesh.getNormal(vertIdx) - p_BaseMesh->getNormal(vertIdx);
							mesh->m_Vertices[vertIdx].m_TargetDeltas[iTargetIndex].Set( deltaPt.x*100.0f, deltaPt.y*100.0f, deltaPt.z*100.0f );
							mesh->m_Vertices[vertIdx].m_TargetNormalDeltas[iTargetIndex].Set( deltaNrmPt.x, deltaNrmPt.y, deltaNrmPt.z );
						}

						iTargetIndex++;
					}
				}

				mesh->m_InstAttrNames.PushAndGrow(atString("notristrip"));
				mesh->m_InstAttrValues.PushAndGrow(true);
			}
		}

		newObject = mesh;

		mp_Hier->m_ContainedObjects.PushAndGrow(newObject,mp_Hier->m_ContainedObjects.GetCount() + 1);

		rexMaxUtility::SetSkinBindPose(p_RootBone,false);
	}

	s32 i,ChildCount = p_Node->NumberOfChildren();

	for(i=0;i<ChildCount;i++)
	{
		INode* p_ChildNode = p_Node->GetChildNode(i);

		rexObject* subObject = NULL;
		ConvertSubObject(p_ChildNode,subObject);
	}

	return result;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexConverterMaxMeshHierarchy::IsAcceptableHierarchyNode(INode* p_Node) const
{
	return rexMaxUtility::IsType(p_Node,Class_ID(TRIOBJ_CLASS_ID,0));
}

