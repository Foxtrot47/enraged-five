#include "shaderInstance.h"

namespace rage
{

class ShaderInstanceRageShader : public ShaderInstanceData
{
public:
	ShaderInstanceRageShader();
	virtual ~ShaderInstanceRageShader() {}

	virtual void MapInstanceData( Mtl* p_Temp, rageRstShaderDbEntry *dbEntry, grcEffect* p_PreviousShaderInstance = NULL, const MaterialPreset* previousPreset=NULL);
};

}