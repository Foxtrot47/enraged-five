#include "rstMaxMtl/dbeditor.h"

#include "ShaderInstanceRageShader.h"

using namespace rage;

ShaderInstanceRageShader::ShaderInstanceRageShader() 
	: ShaderInstanceData()
{
	m_InstanceType = INSTANCE_RAGESHADER;
}

void ShaderInstanceRageShader::MapInstanceData( Mtl* p_Temp, rageRstShaderDbEntry *dbEntry, grcEffect* p_PreviousShaderInstance, const MaterialPreset* p_PreviousPreset)
{
	s32 i,numVars;
	s32 numFloats = 0;
	s32 numInts = 0;
	s32 numTextures = 0;
	s32 numVector2s = 0;
	s32 numVector3s = 0;
	s32 numVector4s = 0;
	s32 numBools = 0;

	atMap<atString,ShaderVar> newVariables;
	atArray<Texmap*> newTextures;
	atArray<Texmap*> newTextureAlphas;
	atArray<f32> newFloats;
	atArray<s32> newInts;
	atArray<Vector2> newVector2s;
	atArray<Vector3> newVector3s;
	atArray<Vector4> newVector4s;
	atArray<bool> newBools;

	grcEffect* p_ShaderInstance = &dbEntry->m_Preset->GetBasis();

	numVars = p_ShaderInstance->GetInstancedVariableCount();
	bool isAlpha = IsAlphaShader(dbEntry);

	grcInstanceData *pPresetEntry = NULL;
	if(dbEntry)
		pPresetEntry = dbEntry->m_Preset;

	for(i=0;i<numVars;i++)
	{
		grmVariableInfo *pVarInfo;
		p_ShaderInstance->GetInstancedVariableInfo(i,pVarInfo);
		grmVariableInfo &varInfo = *pVarInfo;
		grcEffectVar handle = p_ShaderInstance->LookupVar(varInfo.m_Name);

		ShaderVar newVar;
		ShaderVar* p_OldVar = GetVar(varInfo);

		newVar.VarType = varInfo.m_Type;

		grcEffect::VarType p_OldVarType = grcEffect::VT_NONE;

		if ( p_OldVar )
		{
			if (p_PreviousPreset != NULL)
			{		
				p_OldVarType = ConvertPresetEnum((TypeBase::eVariableType) p_OldVar->VarType);
			}
			else
			{
				p_OldVarType = p_OldVar->VarType;
			}
		}

		bool isMtlVar = pPresetEntry && pPresetEntry->GetPresetFlag(handle);
		if(isMtlVar)
			p_OldVar = NULL;

		switch(varInfo.m_Type)
		{
		case grcEffect::VT_FLOAT:
			newVar.Idx = numFloats;
			newFloats.Grow(1);

			if(	p_OldVar && 
				(p_OldVar->Idx != -1) &&
				(p_OldVarType == grcEffect::VT_FLOAT))
			{
				newFloats[numFloats] = m_Floats[p_OldVar->Idx];
			}
			else
			{
				if(isMtlVar && pPresetEntry)
					p_ShaderInstance->GetVar(*pPresetEntry,handle,newFloats[numFloats]);
				else
					p_ShaderInstance->GetVar(handle,newFloats[numFloats]);
			}

			numFloats++;
			break;
		case grcEffect::VT_INT_DONTUSE:
			newVar.Idx = numInts;
			newInts.Grow(1);

			if(	p_OldVar && 
				(p_OldVar->Idx != -1) &&
				(p_OldVarType == grcEffect::VT_INT_DONTUSE))
				newInts[numInts] = m_Ints[p_OldVar->Idx];
			else
			{
				//Ints are now being stored as floats, so until they work there way out of the
				//system get the float data and push it into the old integer field
				float tmpVal;
				if(isMtlVar && pPresetEntry)
					p_ShaderInstance->GetVar(*pPresetEntry,handle,tmpVal);
				else
					p_ShaderInstance->GetVar(handle, tmpVal);
				newInts[numInts] = (int)tmpVal;
			}
			numInts++;
			break;
		case grcEffect::VT_TEXTURE:
			newVar.Idx = numTextures;
			newTextures.Grow(1);

			if(isAlpha)
			{
				newTextureAlphas.Grow(1);
				newTextureAlphas[numTextures] = NULL;
			}

			if(	p_OldVar && 
				(p_OldVar->Idx != -1) &&
				(p_OldVarType == grcEffect::VT_TEXTURE))
			{
				newTextures[numTextures] = m_Textures[p_OldVar->Idx];
				m_Textures[p_OldVar->Idx] = NULL;	//setting to NULL means that the texmap wont get 

				if(isAlpha)
				{
					if(p_OldVar->Idx < m_TextureAlphas.GetCount())
					{
						newTextureAlphas[numTextures] = m_TextureAlphas[p_OldVar->Idx];
						m_TextureAlphas[p_OldVar->Idx] = NULL;
					}
				}
			}
			else
			{
				Texmap *pTex = NULL;

#if ENABLE_PRESET_EDITING
				if(isMtlVar)
				{
					grcTexture *pTmpVal = NULL;
					if(pPresetEntry)
						p_ShaderInstance->GetVar(*pPresetEntry,handle,pTmpVal);
					if(pTmpVal)
					{
						const char *assetName = pTmpVal->GetName();
						if((s32)GetTextureCount()>numTextures && m_Textures[numTextures])
						{
							BitmapTex *pBmp = dynamic_cast<BitmapTex *>(m_Textures[numTextures]);
							if(pBmp && NULL!=strstr(pBmp->GetMapName(), assetName))
							{
								pBmp->SetName(assetName);
								pTex = pBmp;
							}
						}
						if(!pTex)
							pTex = SearchInAssetsAndPaths(atString(assetName));
					}
				}
#endif

				newTextures[numTextures] = pTex;
				if(isAlpha)
					newTextureAlphas[numTextures] = NULL;
			}
			numTextures++;
			break;
		case grcEffect::VT_VECTOR2:
			newVar.Idx = numVector2s;
			newVector2s.Grow(1);

			if( p_OldVar &&
				(p_OldVar->Idx != -1) &&
				(p_OldVarType == grcEffect::VT_VECTOR2) &&
				(p_OldVar->Idx < m_Vector2s.GetCount()))
			{
				newVector2s[numVector2s] = m_Vector2s[p_OldVar->Idx];
			}
			else
			{
				if(isMtlVar && pPresetEntry)
					p_ShaderInstance->GetVar(*pPresetEntry,handle,newVector2s[numVector2s]);
				else
					p_ShaderInstance->GetVar(handle,newVector2s[numVector2s]);
			}
			numVector2s++;
			break;
		case grcEffect::VT_VECTOR3:
			newVar.Idx = numVector3s;
			newVector3s.Grow(1);

			if(	p_OldVar && 
				(p_OldVar->Idx != -1) &&
				(p_OldVarType == grcEffect::VT_VECTOR3) &&
				(p_OldVar->Idx < m_Vector3s.GetCount()))
			{
				newVector3s[numVector3s] = m_Vector3s[p_OldVar->Idx];
			}
			else
			{
				if(isMtlVar && pPresetEntry)
					p_ShaderInstance->GetVar(*pPresetEntry,handle,newVector3s[numVector3s]);
				else
					p_ShaderInstance->GetVar(handle,newVector3s[numVector3s]);
			}

			numVector3s++;
			break;
		case grcEffect::VT_VECTOR4:
			newVar.Idx = numVector4s;
			newVector4s.Grow(1);

			if(	p_OldVar && 
				(p_OldVar->Idx != -1) &&
				(p_OldVarType == grcEffect::VT_VECTOR4) &&
				(p_OldVar->Idx < m_Vector4s.GetCount()))
			{
				newVector4s[numVector4s] = m_Vector4s[p_OldVar->Idx];
			}
			else
			{
				if(isMtlVar && pPresetEntry)
					p_ShaderInstance->GetVar(*pPresetEntry,handle,newVector4s[numVector4s]);
				else
					p_ShaderInstance->GetVar(handle,newVector4s[numVector4s]);
			}

			numVector4s++;
			break;
		case grcEffect::VT_BOOL_DONTUSE:
			newVar.Idx = numBools;
			newBools.Grow(1);

			if( p_OldVar &&
				(p_OldVar->Idx != -1) &&
				(p_OldVarType == grcEffect::VT_BOOL_DONTUSE) &&
				(p_OldVar->Idx < m_Bools.GetCount()))
				newBools[numBools] = m_Bools[p_OldVar->Idx];
			else
			{
				//Bools are now being stored as floats, so until they work there way out of the
				//system get the float data and push it into the old boolean field
				float tmpVal;
				if(isMtlVar && pPresetEntry)
					p_ShaderInstance->GetVar(*pPresetEntry,handle,tmpVal);
				else
					p_ShaderInstance->GetVar(handle, tmpVal);
				if(tmpVal == 0.0f)
					newBools[numBools] = false;
				else
					newBools[numBools] = true;
			}

			numBools++;
			break;
		case grcEffect::VT_MATRIX43:
		case grcEffect::VT_MATRIX44:
		default:
			newVar.Idx = -1;
			break;
		}
		newVariables.Insert(atString(varInfo.m_Name),newVar);
	}

	//for any textures that have gone, make sure that the refs are deleted

	s32 numPrevTextures = m_Textures.GetCount();
	for(i=0;i<numPrevTextures;i++)
	{
		if(m_Textures[i] && (newTextures.GetCount()<=i || m_Textures[i]!=newTextures[i]))
			p_Temp->DeleteReference(i);
	}

	s32 numPrevAlphaTextures = m_TextureAlphas.GetCount();
	for(i=0;i<numPrevAlphaTextures;i++)
	{
		if(m_TextureAlphas[i] && (newTextureAlphas.GetCount()<=i || m_TextureAlphas[i]!=newTextureAlphas[i]))
			p_Temp->DeleteReference(numPrevTextures + i);
	}

	p_Temp->NotifyDependents(FOREVER,PART_TEXMAP,REFMSG_CHANGE);

	//set the new variables

	if(newTextures.size())
	{
		m_Textures = newTextures;
		for(int texindex=0; texindex<m_Textures.size(); texindex++)
		{
			if(p_Temp->GetSubTexmap(texindex)!=m_Textures[texindex])
				p_Temp->SetSubTexmap(texindex, m_Textures[texindex]);
		}
	}
	if(newTextureAlphas.size())
	{
		m_TextureAlphas = newTextureAlphas;
		for(int texindex=0; texindex<m_TextureAlphas.GetCount(); texindex++)
		{
			int alphaTexIndex = m_Textures.size() + texindex;
			if(p_Temp->GetSubTexmap(alphaTexIndex)!=m_TextureAlphas[texindex])
				p_Temp->SetSubTexmap(alphaTexIndex, m_TextureAlphas[texindex]);
		}
	}
	if(newFloats.size())
		m_Floats = newFloats;
	if(newInts.size())
		m_Ints = newInts;
	if(newVector2s.size())
		m_Vector2s = newVector2s;
	if(newVector3s.size())
		m_Vector3s = newVector3s;
	if(newVector4s.size())
		m_Vector4s = newVector4s;
	if(newBools.size())
		m_Bools = newBools;

	//copies the entire contents of the map...

	m_Variables.Kill();

	s32 NumSlots = newVariables.GetNumSlots();

	for(i=0;i<NumSlots;i++)
	{
		atMap<atString,ShaderVar>::Entry* p_Entry = newVariables.GetEntry(i);

		while(p_Entry)
		{
			m_Variables.Insert(p_Entry->key,p_Entry->data);

			p_Entry = p_Entry->next;
		}
	}
}