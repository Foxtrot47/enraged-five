#ifndef __MAX_MATERIALDATA_H__
#define __MAX_MATERIALDATA_H__

#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "atl/array.h"
#include "atl/map.h"
#include "grcore/effect.h"
#include "grmodel/shader.h"
#include "rst/shaderdb.h"
#include "MaterialPresets/MaterialPreset.h"

class RageMaxNitroMtl;

namespace rage 
{
	class SceneAssetEnumStore : public AssetEnumCallback 
	{
	public:
		void RecordAsset (const MaxSDK::AssetManagement::AssetUser &asset)
		{
			MSTR filename = asset.GetFileName();
			CStr p, f, e;
			CStr name(filename.data());
			SplitFilename(name, &p, &f, &e);
			m_nameAssetTable.Insert(atString(f.data()), asset.GetId());
		}

		atMap<atString, MaxSDK::AssetManagement::AssetId>  m_nameAssetTable;
	};

	class ShaderInstanceData
	{
	public:
		friend class ShaderInstancePreset;
		friend class ShaderInstanceRageShader;

		ShaderInstanceData();
		virtual ~ShaderInstanceData();

		ShaderInstanceData& operator=( const ShaderInstanceData& that );

		void Copy(ShaderInstanceData& that);
		void Reset();

		static void LoadAssetPaths();
		BitmapTex *SearchInAssetsAndPaths(atString asset);

		enum eTextureType
		{
			TEXTURE_DIFFUSE,
			TEXTURE_BUMP,
			TEXTURE_SPECULAR,
			TEXTURE_ENVIRONMENT,
			TEXTURE_PALETTE,
			TEXTURE_DISTANCE
		};

		enum eInstanceDataType
		{
			INSTANCE_PRESET,
			INSTANCE_RAGESHADER
		};

		struct ShaderVar
		{
			grcEffect::VarType	VarType;
			eTextureType		SubType;
			s32					Idx;
		};

		virtual void MapInstanceData( Mtl* p_Temp, rageRstShaderDbEntry *dbEntry, grcEffect* p_PreviousShaderInstance = NULL, const MaterialPreset* previousPreset=NULL) = 0;

		u32							GetTextureCount()			{ return m_Textures.GetCount(); }
		u32							GetTextureAlphaCount()		{ return m_TextureAlphas.GetCount(); }
		u32							GetFloatCount()				{ return m_Floats.GetCount(); }
		u32							GetIntCount()				{ return m_Ints.GetCount(); }
		u32							GetVector2Count()			{ return m_Vector2s.GetCount(); }
		u32							GetVector3Count()			{ return m_Vector3s.GetCount(); }
		u32							GetVector4Count()			{ return m_Vector4s.GetCount(); }
		u32							GetBoolCount()				{ return m_Bools.GetCount(); }


		Texmap*						GetTexture( u32 idx ) 		{ return m_Textures.GetCount()>(int)idx?m_Textures[idx]:NULL; }
		Texmap*						GetTextureAlpha( u32 idx ) 	{ return m_TextureAlphas.GetCount()>(int)idx?m_TextureAlphas[idx]:NULL; }
		f32&						GetFloat( u32 idx ) 		{ assert(m_Floats.GetCount()>(int)idx); return m_Floats[idx]; }
		s32&						GetInt( u32 idx ) 			{ assert(m_Ints.GetCount()>(int)idx); return m_Ints[idx]; }
		Vector2&					GetVector2( u32 idx )  		{ assert(m_Vector2s.GetCount()>(int)idx); return m_Vector2s[idx]; }
		Vector3&					GetVector3( u32 idx )  		{ assert(m_Vector3s.GetCount()>(int)idx); return m_Vector3s[idx]; }
		Vector4&					GetVector4( u32 idx )  		{ assert(m_Vector4s.GetCount()>(int)idx); return m_Vector4s[idx]; }
		bool&						GetBool( u32 idx )  		{ assert(m_Bools.GetCount()>(int)idx); return m_Bools[idx]; }

		void						SetTexture(u32 idx, Texmap* texmap)			{ assert(m_Textures.GetCount()>(int)idx); m_Textures[idx] = texmap; }
		void						SetTextureAlpha(u32 idx, Texmap* texmap)	{ assert(m_TextureAlphas.GetCount()>(int)idx); m_TextureAlphas[idx] = texmap; }
		void						SetFloat(u32 idx, f32 f)					{ assert(m_Floats.GetCount()>(int)idx); m_Floats[idx] = f; }
		void						SetInt(u32 idx, s32 i)						{ assert(m_Ints.GetCount()>(int)idx); m_Ints[idx] = i; }
		void						SetVector2(u32 idx, Vector2 vec2)			{ assert(m_Vector2s.GetCount()>(int)idx); m_Vector2s[idx] = vec2; }
		void						SetVector3(u32 idx, Vector3 vec3)			{ assert(m_Vector3s.GetCount()>(int)idx); m_Vector3s[idx] = vec3; }
		void						SetVector4(u32 idx, Vector4 vec4)			{ assert(m_Vector4s.GetCount()>(int)idx); m_Vector4s[idx] = vec4; }
		void						SetBool(u32 idx, bool b)					{ assert(m_Bools.GetCount()>(int)idx); m_Bools[idx] = b; }

		void						AddTexture( Texmap* texmap )			{ m_Textures.PushAndGrow(texmap); }
		void						AddTextureAlpha( Texmap* texmap )		{ m_TextureAlphas.PushAndGrow(texmap); }
		void						AddFloat( f32 f )						{ m_Floats.PushAndGrow(f); }
		void						AddInt( s32 i )							{ m_Ints.PushAndGrow(i); }
		void						AddVector2( Vector2 vec2 )				{ m_Vector2s.PushAndGrow(vec2); }
		void						AddVector3( Vector3 vec3 )				{ m_Vector3s.PushAndGrow(vec3); }
		void						AddVector4( Vector4 vec4 )				{ m_Vector4s.PushAndGrow(vec4); }
		void						AddBool( bool b )						{ m_Bools.PushAndGrow(b); }

		atMap<atString,ShaderVar>&	GetVariableMap() { return m_Variables; }
		ShaderVar*					GetVar(const grmVariableInfo& r_VarInfo);
		ShaderVar*					GetVar(const atString& varName);

		TypeBase::eVariableType		ConvertEffectEnum(grcEffect::VarType type);
		grcEffect::VarType			ConvertPresetEnum(TypeBase::eVariableType type);

		bool						IsAlphaShader(rageRstShaderDbEntry* pDbEntry);

		bool						IsMaterialPreset()	{ return m_InstanceType == INSTANCE_PRESET; }
		eInstanceDataType			GetInstanceType() { return m_InstanceType; }

		atArray<atString>		m_PresetOverrides;
	private:
		atArray<Texmap*>		m_Textures;
		atArray<Texmap*>		m_TextureAlphas;
		atArray<f32>			m_Floats;
		atArray<s32>			m_Ints;
		atArray<Vector2>		m_Vector2s;
		atArray<Vector3>		m_Vector3s;
		atArray<Vector4>		m_Vector4s;
		atArray<bool>			m_Bools;

		atMap<atString,ShaderVar> m_Variables;

		static SceneAssetEnumStore s_sceneBmps;

	private:
		eInstanceDataType		m_InstanceType;
	};

} // namespace rage

#endif // __MAX_MATERIALDATA_H__