#include "file/device.h"
#include "shaderInstance.h"
#include "grcore/texturestring.h"
#include "rageMaxNitroMtl/rageMaxNitroMtl.h"
#include "rstMaxMtl/mtl.h"
#include "rstMaxMtl/dbeditor.h"
#include "AssetManagement/iassetmanager.h"
#include <IPathConfigMgr.h>

#include "MaterialPresets/MaterialPreset.h"
#include "MaterialPresets/MaterialPresetManager.h"
#include "rexMaxUtility/utility.h"

using namespace rage;

SceneAssetEnumStore ShaderInstanceData::s_sceneBmps;
static const char* sAlphaMapPropertyname = "ExposeAlphaMap";

const char *s_allowedExtensions[] = 
{
	".tif",
	".bmp",
	".tga",
	".jpg"
};
int s_numExtensions = sizeof(s_allowedExtensions)/sizeof(s_allowedExtensions[0]);

static bool registeredLoadCallback = false;
void LoadedNewMaxScene(void *param, NotifyInfo *info)
{
	ShaderInstanceData::LoadAssetPaths();
}


ShaderInstanceData::ShaderInstanceData(  )
{
#if !ENABLE_PRESET_EDITING
	if(!registeredLoadCallback)
	{
		RegisterNotification(LoadedNewMaxScene, NULL, NOTIFY_FILE_POST_OPEN_PROCESS);
		registeredLoadCallback = true;
	}
#endif

	m_Variables.Reset();
}

ShaderInstanceData::~ShaderInstanceData()
{

}

ShaderInstanceData& ShaderInstanceData::operator=( const ShaderInstanceData& that )
{
	if (this == &that)      // Same object?
		return *this;

	m_Floats = that.m_Floats;
	m_Ints = that.m_Ints;
	m_Vector2s = that.m_Vector2s;
	m_Vector3s = that.m_Vector3s;
	m_Vector4s = that.m_Vector4s;
	m_Bools = that.m_Bools;

	m_Variables = that.m_Variables;

	return *this;
}

void ShaderInstanceData::Copy(ShaderInstanceData& that)
{
	m_Floats = that.m_Floats;
	m_Ints = that.m_Ints;
	m_Vector2s = that.m_Vector2s;
	m_Vector3s = that.m_Vector3s;
	m_Vector4s = that.m_Vector4s;
	m_Bools = that.m_Bools;

	m_Variables = that.m_Variables;
	m_TextureAlphas = that.m_TextureAlphas;
	m_Textures = that.m_Textures;
}

bool ShaderInstanceData::IsAlphaShader(rageRstShaderDbEntry* pDbEntry)
{
	if(!pDbEntry)
		return false;

	if ( pDbEntry->IsMaterialPreset() )
	{
		return pDbEntry->m_MaterialPreset->IsAlphaShader();
	}
	else if(pDbEntry->m_Preset)
	{
		if( pDbEntry->m_Preset->GetDrawBucket() > 0 || pDbEntry->m_Preset->GetBasis().GetPropertyValue( sAlphaMapPropertyname, 0 ) == 1 )
			return true;
	}
}

void ShaderInstanceData::LoadAssetPaths()
{
	s_sceneBmps.m_nameAssetTable.Kill();
	GetCOREInterface()->EnumAuxFiles(s_sceneBmps, FILE_ENUM_ALL);
}

using namespace MaxSDK::AssetManagement;

BitmapTex *ShaderInstanceData::SearchInAssetsAndPaths(atString asset)
{
	BitmapTex *pTex = NULL;
//	const char *pPath;
	const MaxSDK::AssetManagement::AssetId* pID = s_sceneBmps.m_nameAssetTable.Access(asset);
	if(pID)
	{
		MaxSDK::AssetManagement::AssetUser user = MaxSDK::AssetManagement::IAssetManager::GetInstance()->GetAsset(*pID);
		const MSTR& assetname = user.GetFileName();
		pTex = NewDefaultBitmapTex();
		pTex->SetMap(user);
		char *newmapfile = pTex->GetMapName();
	}
	else
	{
		BitmapInfo bi;
//Load the selected image
		for(int i=0;i<s_numExtensions;i++)
		{
			atString fullAssetName(asset, s_allowedExtensions[i]);
			IFileResolutionManager* pFRM = IFileResolutionManager::GetInstance();
			MaxSDK::Util::Path path(_M(fullAssetName.c_str()));
			if (pFRM->GetFullFilePath(path, MaxSDK::AssetManagement::kBitmapAsset))
			{
				const char *filename = path.GetCStr();
// 				const char *lastBackslash = strrchr(filename, '\\');
// 				const char *lastForwardslash = strrchr(filename, '/');
// 				if(lastBackslash)
// 				{
// 					filename = lastBackslash;
// 					filename++;
// 				}
// 				else if(lastForwardslash)
// 				{
// 					filename = lastForwardslash;
// 					filename++;
// 				}
				pTex = NewDefaultBitmapTex();
				pTex->SetMapName(filename);
				pTex->SetName(asset.c_str());
				break;
			}
			else
			{
				std::string pTextureFolder(getenv("RS_PROJROOT"));
				pTextureFolder.append("\\art\\textures");

				const char* foundFile = fiDeviceLocal::GetInstance().FindFile(pTextureFolder.c_str(), fullAssetName);
				/*bool found = false;
				WIN32_FIND_DATA findData;
				HANDLE findFileHandle = FindFirstFile( pTexturesFolder, &findData );
				if (findFileHandle != INVALID_HANDLE_VALUE 
					&& stricmp(fullAssetName, findData.cFileName) == 0)
				{
					found = true;
				}
				else
				{
					while( FindNextFile(findFileHandle, &findData) )
					{
						if (stricmp(fullAssetName, findData.cFileName) == 0)
						{
							found = true;
							break;
						}
					}
				}*/

				if (foundFile != NULL)
				{
					pTex = NewDefaultBitmapTex();
					pTex->SetMapName(foundFile);
					pTex->SetName(asset.c_str());
				}
			}
		}
	}
	return pTex;
}

void ShaderInstanceData::Reset()
{
	m_Variables.Kill();
	m_Floats.Reset();
	m_Ints.Reset();
	m_Vector3s.Reset();
	m_Vector4s.Reset();
	m_Bools.Reset();
	m_Textures.Reset();
	m_TextureAlphas.Reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ShaderInstanceData::ShaderVar* ShaderInstanceData::GetVar(const grmVariableInfo& r_VarInfo)
{
	ShaderVar* p_OldVar;

	p_OldVar = m_Variables.Access(r_VarInfo.m_Name);
	if(!p_OldVar)
	{
		p_OldVar = m_Variables.Access(r_VarInfo.m_ActualName);
	}
	return p_OldVar;
}


ShaderInstanceData::ShaderVar* ShaderInstanceData::GetVar(const atString& varName)
{
	ShaderVar* p_OldVar = m_Variables.Access(varName);
	return p_OldVar;
}



TypeBase::eVariableType ShaderInstanceData::ConvertEffectEnum(grcEffect::VarType type)
{
	switch(type)
	{
	case grcEffect::VT_FLOAT:
		return TypeBase::kFloat;

	case grcEffect::VT_INT_DONTUSE:
		return TypeBase::kInt;

	case grcEffect::VT_VECTOR2:
		return TypeBase::kVector2;

	case grcEffect::VT_VECTOR3:
		return TypeBase::kVector3;

	case grcEffect::VT_VECTOR4:
		return TypeBase::kVector4;

	case grcEffect::VT_TEXTURE:
		return TypeBase::kTextureMap;

	default:
		return TypeBase::kUndefined;
	}
}

grcEffect::VarType ShaderInstanceData::ConvertPresetEnum(TypeBase::eVariableType type)
{
	switch(type)
	{
	case TypeBase::kFloat:
		return grcEffect::VT_FLOAT;

	case TypeBase::kInt:
		return grcEffect::VT_INT_DONTUSE;

	case TypeBase::kVector2:
		return grcEffect::VT_VECTOR2;

	case TypeBase::kVector3:
		return grcEffect::VT_VECTOR3;

	case TypeBase::kVector4:
		return grcEffect::VT_VECTOR4;

	case TypeBase::kTextureMap:
		return grcEffect::VT_TEXTURE;

	default:
		return grcEffect::VT_NONE;
	}
}