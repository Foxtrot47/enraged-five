#include "ShaderInstancePreset.h"

using namespace rage;

ShaderInstancePreset::ShaderInstancePreset()
	: ShaderInstanceData()
{
	m_InstanceType = INSTANCE_PRESET;
}

void ShaderInstancePreset::MapInstanceData( Mtl* p_Temp, rageRstShaderDbEntry *dbEntry, grcEffect* p_PreviousShaderInstance, const MaterialPreset* p_PreviousPreset)
{
	if (!dbEntry)
	{
		Reset();
		return;
	}

	if (!dbEntry->m_MaterialPreset)
	{
		Reset();
		return;
	}

	s32 i;

	s32 numFloats = 0;
	s32 numInts = 0;
	s32 numTextures = 0;
	s32 numVector2s = 0;
	s32 numVector3s = 0;
	s32 numVector4s = 0;
	s32 numBools = 0;

	atMap<atString,ShaderVar> newVariables;
	atArray<Texmap*> newTextures;
	atArray<Texmap*> newTextureAlphas;
	atArray<f32> newFloats;
	atArray<s32> newInts;
	atArray<Vector2> newVector2s;
	atArray<Vector3> newVector3s;
	atArray<Vector4> newVector4s;
	atArray<bool> newBools;

	MaterialPreset& fullPreset = *(dbEntry->m_MaterialPreset);
	bool isAlpha = fullPreset.IsAlphaShader();

	const atArray<TypeBase*>& variableArray = fullPreset.GetVariableArray();
	for(i = 0; i < variableArray.GetCount(); i++)
	{
		TypeBase* varInfo = variableArray[i];
		ShaderVar newVar;

		//This is a hack so to maintain backward compatibility.
		newVar.VarType = (grcEffect::VarType) varInfo->GetType();

		ShaderVar* p_OldVar = GetVar(varInfo->GetVariableName());
		TypeBase::eVariableType p_OldVarType = TypeBase::kUndefined;

		if ( p_OldVar )
		{
			p_OldVarType = (TypeBase::eVariableType) p_OldVar->VarType;
		}

		switch(varInfo->GetType())
		{
		case TypeBase::kFloat:
			{
				newVar.Idx = numFloats;
				newFloats.Grow(1);

				FloatType* pFloatType = static_cast<FloatType*>(varInfo);

				if(	p_OldVar && 
					(p_OldVar->Idx != -1) &&
					(p_OldVarType == TypeBase::kFloat) &&
					(p_OldVar->Idx < m_Floats.GetCount()) &&
					(!varInfo->IsLocked()))
				{
					if (pFloatType->GetValue() == m_Floats[p_OldVar->Idx])
						newFloats[numFloats] = m_Floats[p_OldVar->Idx];
					else
						newFloats[numFloats] = pFloatType->GetValue();
				}
				else
				{
					newFloats[numFloats] = pFloatType->GetValue();
				}

				numFloats++;
			}
			break;
		case TypeBase::kInt:
			{
				newVar.Idx = numInts;
				newInts.Grow(1);

				IntType* pIntType = static_cast<IntType*>(varInfo);

				if(	p_OldVar && 
					(p_OldVar->Idx != -1) &&
					(p_OldVarType == TypeBase::kInt) &&
					(!varInfo->IsLocked()))
				{
					if (pIntType->GetValue() == m_Ints[p_OldVar->Idx])
						newInts[numInts] = m_Ints[p_OldVar->Idx];
					else
						newInts[numInts] = pIntType->GetValue();
				}
				else
				{
					//Ints are now being stored as floats, so until they work there way out of the
					//system get the float data and push it into the old integer field
					newInts[numInts] = pIntType->GetValue();
				}

				numInts++;
			}
			break;
		case TypeBase::kVector2:
			{
				newVar.Idx = numVector2s;
				newVector2s.Grow(1);

				Vector2Type* pVector2Type = static_cast<Vector2Type*>(varInfo);

				if( p_OldVar &&
					(p_OldVar->Idx != -1) &&
					(p_OldVarType == TypeBase::kVector2) &&
					(p_OldVar->Idx < m_Vector2s.GetCount()) &&
					(!varInfo->IsLocked()))
				{
					if (pVector2Type->GetValue() == m_Vector2s[p_OldVar->Idx])
						newVector2s[numVector2s] = m_Vector2s[p_OldVar->Idx];
					else
						newVector2s[numVector2s] = pVector2Type->GetValue();
				}
				else
				{
					newVector2s[numVector2s] = pVector2Type->GetValue();
				}

				numVector2s++;
			}
			break;
		case TypeBase::kVector3:
			{
				newVar.Idx = numVector3s;
				newVector3s.Grow(1);

				Vector3Type* pVector3Type = static_cast<Vector3Type*>(varInfo);

				if(	p_OldVar && 
					(p_OldVar->Idx != -1) &&
					(p_OldVarType == TypeBase::kVector3) &&
					(p_OldVar->Idx < m_Vector3s.GetCount()) &&
					(!varInfo->IsLocked()))
				{
					if (pVector3Type->GetValue() == m_Vector3s[p_OldVar->Idx])
						newVector3s[numVector3s] = m_Vector3s[p_OldVar->Idx];
					else
						newVector3s[numVector3s] = pVector3Type->GetValue();
				}
				else
				{
					newVector3s[numVector3s] = pVector3Type->GetValue();
				}

				numVector3s++;
			}
			break;
		case TypeBase::kVector4:
			{
				newVar.Idx = numVector4s;
				newVector4s.Grow(1);

				Vector4Type* pVector4Type = static_cast<Vector4Type*>(varInfo);

				if(	p_OldVar && 
					(p_OldVar->Idx != -1) &&
					(p_OldVarType == TypeBase::kVector4) &&
					(p_OldVar->Idx < m_Vector4s.GetCount()) &&
					(!varInfo->IsLocked()))
				{
					if (pVector4Type->GetValue() == m_Vector4s[p_OldVar->Idx])
						newVector4s[numVector4s] = m_Vector4s[p_OldVar->Idx];
					else
						newVector4s[numVector4s] = pVector4Type->GetValue();
				}
				else
				{
					newVector4s[numVector4s] = pVector4Type->GetValue();
				}

				numVector4s++;
			}
			break;
		case TypeBase::kTextureMap:
			{
				newVar.Idx = numTextures;
				newTextures.Grow(1);

				if(isAlpha)
				{
					newTextureAlphas.Grow(1);
					newTextureAlphas[numTextures] = NULL;
				}

				if(	p_OldVar && 
					(p_OldVar->Idx != -1) &&
					(p_OldVarType == TypeBase::kTextureMap) &&
					(!varInfo->IsLocked()))
				{
					TexMapType* pTexMapType = static_cast<TexMapType*>(varInfo);
					BitmapTex *pBmp = dynamic_cast<BitmapTex *>(m_Textures[p_OldVar->Idx]);
					const char* textureMap = pTexMapType->GetValue().c_str();

					// We have a previous texture but now it's a different name, swap it out.
					if(pBmp && strlen(textureMap) > 0 && stricmp(pBmp->GetMapName(), textureMap) != 0)
					{
						char Buffer[8192];
						strcpy(Buffer, pTexMapType->GetValue().c_str());

						pBmp = NewDefaultBitmapTex();
						pBmp->SetMapName(Buffer);

						newTextures[numTextures] = (Texmap*)pBmp;
					}
					else
					{
						newTextures[numTextures] = m_Textures[p_OldVar->Idx];
						m_Textures[p_OldVar->Idx] = NULL;	//setting to NULL means that the texmap wont get 
					}

					if(isAlpha)
					{
						if(p_OldVar->Idx < m_TextureAlphas.GetCount())
						{
							newTextureAlphas[numTextures] = m_TextureAlphas[p_OldVar->Idx];
							m_TextureAlphas[p_OldVar->Idx] = NULL;
						}
					}
				}
				else
				{
					Texmap *pTex = NULL;
					TexMapType* pTexMapType = static_cast<TexMapType*>(varInfo);

					if((s32)GetTextureCount()>numTextures && m_Textures[numTextures])
					{
						BitmapTex *pBmp = dynamic_cast<BitmapTex *>(m_Textures[numTextures]);

						const char* textureMap = pTexMapType->GetValue().c_str();

						// If the texture name changes, make sure to re-create the bmp,
						// just changing the name doesn't seem to update it in the viewport or material window.
						if(pBmp && stricmp(pBmp->GetMapName(), textureMap) != 0)
						{
							char Buffer[8192];
							strcpy(Buffer, pTexMapType->GetValue().c_str());

							pBmp = NewDefaultBitmapTex();
							pBmp->SetMapName(Buffer);
						}

						pTex = pBmp;
					}
					else if (pTexMapType->GetValue().GetLength() > 0)
					{
						char Buffer[8192];
						strcpy(Buffer, pTexMapType->GetValue().c_str());

						BitmapTex* p_bitmapTex = NewDefaultBitmapTex();
						p_bitmapTex->SetMapName(Buffer);

						pTex = p_bitmapTex;
					}

					newTextures[numTextures] = pTex;
					if(isAlpha)
						newTextureAlphas[numTextures] = NULL;
				}
				numTextures++;
			}
			break;
		default:
			newVar.Idx = -1;
			break;
		}

		newVariables.Insert(varInfo->GetVariableName(), newVar);
	}

	s32 numPrevTextures = m_Textures.GetCount();
	for(i=0;i<numPrevTextures;i++)
	{
		if(m_Textures[i] && (newTextures.GetCount()<=i || m_Textures[i]!=newTextures[i]))
			p_Temp->DeleteReference(i);
	}

	s32 numPrevAlphaTextures = m_TextureAlphas.GetCount();
	for(i=0;i<numPrevAlphaTextures;i++)
	{
		if(m_TextureAlphas[i] && (newTextureAlphas.GetCount()<=i || m_TextureAlphas[i]!=newTextureAlphas[i]))
			p_Temp->DeleteReference(numPrevTextures + i);
	}

	p_Temp->NotifyDependents(FOREVER,PART_TEXMAP,REFMSG_CHANGE);

	//set the new variables

	if(newTextures.size())
	{
		// Resize the texture list otherwise all kinds of things go haywire.
		if (m_Textures.size() < newTextures.size())
		{
			int diffSize = newTextures.size() - m_Textures.size();
			int originalSize = m_Textures.size();
			m_Textures.ResizeGrow(originalSize + diffSize);

			// Go through the new entries and make sure they are empty.
			for (int i = originalSize; i < (originalSize + diffSize); i++)
				m_Textures[i] = NULL;
		}

		for(int texindex=0; texindex<newTextures.size(); texindex++)
		{
			if(p_Temp->GetSubTexmap(texindex)!=newTextures[texindex])
				p_Temp->SetSubTexmap(texindex, newTextures[texindex]);
		}

		m_Textures = newTextures;
	}
	if(newTextureAlphas.size())
	{
		// Resize the alpha texture list otherwise all kinds of things go haywire.
		if (m_TextureAlphas.size() < newTextureAlphas.size())
		{
			int diffSize = newTextureAlphas.size() - m_TextureAlphas.size();
			int originalSize = m_TextureAlphas.size();
			m_TextureAlphas.ResizeGrow(originalSize + diffSize);

			// Go through the new entries and make sure they are empty.
			for (int i = originalSize; i < (originalSize + diffSize); i++)
				m_TextureAlphas[i] = NULL;
		}

		for(int texindex=0; texindex<newTextureAlphas.GetCount(); texindex++)
		{
			int alphaTexIndex = newTextureAlphas.size() + texindex;
			if(m_TextureAlphas.size() == 0 || p_Temp->GetSubTexmap(alphaTexIndex)!=newTextureAlphas[texindex])
				p_Temp->SetSubTexmap(alphaTexIndex, newTextureAlphas[texindex]);
		}

		m_TextureAlphas = newTextureAlphas;
	}
	if(newFloats.size())
		m_Floats = newFloats;
	if(newInts.size())
		m_Ints = newInts;
	if(newVector2s.size())
		m_Vector2s = newVector2s;
	if(newVector3s.size())
		m_Vector3s = newVector3s;
	if(newVector4s.size())
		m_Vector4s = newVector4s;
	if(newBools.size())
		m_Bools = newBools;

	//copies the entire contents of the map...

	m_Variables.Kill();

	s32 NumSlots = newVariables.GetNumSlots();

	for(i=0;i<NumSlots;i++)
	{
		atMap<atString,ShaderVar>::Entry* p_Entry = newVariables.GetEntry(i);

		while(p_Entry)
		{
			m_Variables.Insert(p_Entry->key,p_Entry->data);

			p_Entry = p_Entry->next;
		}
	}
}