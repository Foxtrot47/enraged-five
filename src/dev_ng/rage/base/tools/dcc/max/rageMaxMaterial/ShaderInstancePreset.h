#include "shaderInstance.h"

namespace rage 
{

class ShaderInstancePreset : public ShaderInstanceData
{
public:
	ShaderInstancePreset();
	virtual ~ShaderInstancePreset() {}

	virtual void MapInstanceData( Mtl* p_Temp, rageRstShaderDbEntry *dbEntry, grcEffect* p_PreviousShaderInstance = NULL, const MaterialPreset* previousPreset=NULL);
};

}