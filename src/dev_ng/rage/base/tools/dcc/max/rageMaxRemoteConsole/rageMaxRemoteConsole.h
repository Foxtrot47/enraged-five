//
// Filename:	rageMaxRemoteConsole.h
// Description:	C/C++ API Network protocol for establishing a connection and transferring data.
//

#ifndef __RAGEMAXREMOTECONNECT_H__
#define __RAGEMAXREMOTECONNECT_H__

// --- Include Files ------------------------------------------------------------
#include "Max.h"
#include "ifnpub.h"
#include "gup.h"
#include "resource.h"
#include "Win32Res.h"

// Local headers
#pragma warning(push)
#pragma warning(disable:4311)
#include "rageRemoteConsole\RemoteConsole.h"
#pragma warning(pop)

// --- Defines ------------------------------------------------------------------
#ifdef RAGEMAXREMOTECONNECTION_EXPORTS
#define RAGEMAXREMOTECONNECTION_API __declspec( dllexport )
#else
#define RAGEMAXREMOTECONNECTION_API __declspec( dllimport )
#endif

// --- Globals ----------------------------------------------------------------
ClassDesc* GetMaxRemoteConnectionClassDesc();

#define REMOTE_CONNECTION_CLASS_ID Class_ID(0x315efd2b, 0x537d516b)
#define REMOTE_CONNECTION_INTERFACE	Interface_ID(0x19ceeede, 0x4a4ba6a)

#define GetMaxRemoteConnectionInterface(cd) \
	(IMaxRemoteConnection*)(cd)->GetFPInterface(ANIMEXPREXPORT_INTERFACE)

//-----------------------------------------------------------------------------

class IMaxRemoteConnection : public FPStaticInterface
{
public:
	virtual ~IMaxRemoteConnection() {}

	virtual BOOL Export(Tab<INode*>& nodeArr, TCHAR* outPutPath, TCHAR* trackIdFilePath, BOOL bIndividualBones, BOOL bRecursive) = 0;


	virtual bool Connect() = 0;
	virtual bool IsConnected() = 0;
	virtual void Disconnect() = 0;

	virtual void LogMessages(bool enable) = 0;

	virtual bool WidgetExists(TCHAR* widgetName) = 0;

	virtual TCHAR* WriteFloatWidget(TCHAR* widgetName, float value) = 0;
	virtual TCHAR* WriteBoolWidget(TCHAR* widgetName, bool value) = 0;
	virtual TCHAR* WriteIntWidget(TCHAR* widgetName, int value) = 0;
	virtual TCHAR* WriteStringWidget(TCHAR* widgetName, TCHAR* value) = 0;
	virtual TCHAR* WriteVector3Widget(TCHAR* widgetName, Point3 value) = 0;
	virtual TCHAR* WriteVector4Widget(TCHAR* widgetName, Point4 value) = 0;

	virtual void WriteFreeCameraStream(Point3 position, Point3 up, Point3 forward, float fov) = 0;

	virtual float ReadFloatWidget(TCHAR* widgetName) = 0;
	virtual bool ReadBoolWidget(TCHAR* widgetName) = 0;
	virtual int ReadIntWidget(TCHAR* widgetName) = 0;
	virtual TCHAR* ReadStringWidget(TCHAR* widgetName) = 0;
	virtual Point3 ReadVector3Widget(TCHAR* widgetName) = 0;
	virtual Point4 ReadVector4Widget(TCHAR* widgetName) = 0;

	virtual TCHAR* SendCommand(TCHAR* command) = 0;
	virtual void SendSyncCommand() = 0;
};

class MaxRemoteConnection : public GUP, public IMaxRemoteConnection
{
public:
	DWORD	Start();
	void	Stop();

	//IMaxAnimExprExport Interface
	enum
	{
		em_Export,

		connectFn, 
		disconnectFn,
		isConnectedFn,

		logMessagesFn,

		widgetExistsFn,

		writeFloatFn,
		writeBoolFn,
		writeIntFn,
		writeStringFn,

		writeVector3Fn,
		writeVector4Fn,

		writeFreeCameraStreamFn,

		readFloatFn,
		readBoolFn,
		readIntFn,
		readStringFn,
		readVector4Fn,

		sendCommandFn,
		sendSyncCommandFn,

		isConnectedToGameFn,
		getExecutableNameFn,
		getPlatformFn,
		getBuildConfigFn,
		getGameIPFn
	};

	BOOL Export(Tab<INode*>& nodeArr, TCHAR* outputPath, TCHAR* trackIdFilePath, BOOL bIndividualBones, BOOL bRecursive)
	{

		return TRUE;
	}

	bool Connect();
	bool IsConnected();
	void Disconnect();
	void LogMessages(bool enable);

	bool WidgetExists(TCHAR* widgetName);

	TCHAR* WriteFloatWidget(TCHAR* widgetName,float value);
	TCHAR* WriteBoolWidget(TCHAR* widgetName,bool value);
	TCHAR* WriteIntWidget(TCHAR* widgetName,int value);
	TCHAR* WriteStringWidget(TCHAR* widgetName, TCHAR* value);
	TCHAR* WriteVector3Widget(TCHAR* widgetName, Point3 value);
	TCHAR* WriteVector4Widget(TCHAR* widgetName, Point4 value);

	void WriteFreeCameraStream(Point3 position, Point3 up, Point3 forward, float fov);

	float ReadFloatWidget(TCHAR* widgetName);
	bool ReadBoolWidget(TCHAR* widgetName);
	int ReadIntWidget(TCHAR* widgetName);
	TCHAR* ReadStringWidget(TCHAR* widgetName);
	Point3 ReadVector3Widget(TCHAR* widgetName);
	Point4 ReadVector4Widget(TCHAR* widgetName);

	TCHAR* SendCommand(TCHAR* command);
	void SendSyncCommand();
	bool IsConnectedToGame();
	TCHAR* GetExecutableName();
	TCHAR* GetPlatform();
	TCHAR* GetBuildConfig();
	TCHAR* GetGameIP();

	DECLARE_DESCRIPTOR(MaxRemoteConnection)

	BEGIN_FUNCTION_MAP
		FN_0(connectFn, TYPE_BOOL, Connect)
		VFN_0(disconnectFn, Disconnect)
		FN_0(isConnectedFn, TYPE_BOOL, IsConnected)
		VFN_1(logMessagesFn, LogMessages, TYPE_BOOL)

		FN_1(widgetExistsFn, TYPE_BOOL, WidgetExists, TYPE_STRING)
		VFN_2(writeFloatFn, WriteFloatWidget, TYPE_STRING, TYPE_FLOAT)
		VFN_2(writeBoolFn, WriteBoolWidget, TYPE_STRING, TYPE_BOOL)
		VFN_2(writeIntFn, WriteIntWidget, TYPE_STRING, TYPE_INT)
		VFN_2(writeStringFn, WriteStringWidget, TYPE_STRING, TYPE_STRING)

		VFN_2(writeVector3Fn, WriteVector3Widget, TYPE_STRING, TYPE_POINT3)
		VFN_2(writeVector4Fn, WriteVector4Widget, TYPE_STRING, TYPE_POINT4)

		VFN_4(writeFreeCameraStreamFn, WriteFreeCameraStream, TYPE_POINT3, TYPE_POINT3, TYPE_POINT3, TYPE_FLOAT)

		FN_1(readFloatFn, TYPE_FLOAT, ReadFloatWidget, TYPE_STRING)
		FN_1(readBoolFn, TYPE_BOOL, ReadBoolWidget, TYPE_STRING)
		FN_1(readIntFn, TYPE_INT, ReadIntWidget, TYPE_STRING)
		FN_1(readStringFn, TYPE_STRING, ReadStringWidget, TYPE_STRING)
		FN_1(readVector4Fn, TYPE_POINT4, ReadVector4Widget, TYPE_STRING)

		FN_1(sendCommandFn, TYPE_STRING, SendCommand, TYPE_STRING)
		VFN_0(sendSyncCommandFn, SendSyncCommand)
		FN_0(isConnectedToGameFn, TYPE_BOOL, IsConnectedToGame)
		FN_0(getExecutableNameFn, TYPE_STRING, GetExecutableName)
		FN_0(getPlatformFn, TYPE_STRING, GetPlatform)
		FN_0(getBuildConfigFn, TYPE_STRING, GetBuildConfig)
		FN_0(getGameIPFn, TYPE_STRING, GetGameIP)
	END_FUNCTION_MAP

	static void LogToListener(const char* message);

private:
	rage::RemoteConsole m_Connection;
	int m_ReferenceCount;
};




#endif // !__RAGEMAXREMOTECONNECT_H__
