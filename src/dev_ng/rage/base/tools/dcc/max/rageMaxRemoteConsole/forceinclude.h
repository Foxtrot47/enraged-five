#ifndef __MAX_FORCEINCLUDE_H__
#define __MAX_FORCEINCLUDE_H__

//this file is included through the forceinclude setting
//in the project so is included in every file

//we have to include max first because it defines some functions
//that are macros in the rage headers (Assert)

// MAX includes
#include <max.h>
#include <istdplug.h>
#include <iparamb2.h>
#include <maxapi.h>
#include <maxscript\maxscript.h>
#include <maxscript\maxwrapper\maxclasses.h>
#include <maxscript\foundation\numbers.h>
#include <maxscript\foundation\strings.h>
#include <maxscript\macros\define_instantiation_functions.h>
#include <maxscript\util\listener.h>

#ifdef UNUSED_PARAM
#undef UNUSED_PARAM
#endif // UNUSED_PARAM

#if defined(_DEBUG) && defined(_M_X64)
	#include "forceinclude/win64_tooldebug.h"
#elif defined(_DEBUG)
	#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG) && defined(_M_X64)
	#include "forceinclude/win64_toolrelease.h"
#elif defined(NDEBUG)
	#include "forceinclude/win32_toolrelease.h"
#elif defined(_M_X64)
	#include "forceinclude/win64_toolbeta.h"
#else
	#include "forceinclude/win32_toolbeta.h"
#endif

#pragma warning( disable: 4800 )
#pragma warning( disable: 4265 )
#pragma warning( disable: 4263 )
#pragma warning( disable: 4251 )

#endif //__MAX_FORCEINCLUDE_H__
