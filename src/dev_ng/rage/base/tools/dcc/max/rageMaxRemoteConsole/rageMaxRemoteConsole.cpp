//
// File:: rageMaxRemoteConsole.cpp
// Description::
//
// Author:: Kevin Weinberg <kevin.weinberg@rockstarsandiego.com>
// Date:: 8 December 2010
//

// Local headers
#include "rageMaxRemoteConsole.h"

#include "atl\string.h"
#include "vector\vector3.h"
#include "vector\vector4.h"
// Max headers

using namespace rage;

// --- MaxRemoteConnection ----------------------------------------------------------------

DWORD MaxRemoteConnection::Start()
{
	m_ReferenceCount = 0;

	RemoteConsole::LogMessageCallback logMessageFunctor;
	logMessageFunctor.Bind(&MaxRemoteConnection::LogToListener);
	m_Connection.SetLogMessageCallback(logMessageFunctor);
	return GUPRESULT_KEEP;
}

//-----------------------------------------------------------------------------

void MaxRemoteConnection::Stop()
{
}

bool MaxRemoteConnection::Connect()
{
	if ( m_ReferenceCount == 0)
	{
		m_ReferenceCount++;
		return m_Connection.Connect();
	}
	else
	{
		m_ReferenceCount++;

		if ( m_Connection.IsConnected() == false )
			return m_Connection.Connect();

		return true; //Already connected.
	}
}

bool MaxRemoteConnection::IsConnected()
{
	return m_Connection.IsConnected();
}

void MaxRemoteConnection::Disconnect()
{
	if (m_ReferenceCount > 0)
		m_ReferenceCount--;

	if ( m_ReferenceCount == 0 )
	{
		m_Connection.Disconnect();
	}
}

void MaxRemoteConnection::LogMessages(bool enable)
{
	m_Connection.LogMessages(enable);
}

bool MaxRemoteConnection::WidgetExists(TCHAR* widgetName)
{
	return m_Connection.WidgetExists(widgetName);
}

TCHAR* MaxRemoteConnection::WriteFloatWidget(TCHAR* widgetName, float value)
{
	atString result;
	m_Connection.WriteFloatWidget(widgetName, value, result);

	TCHAR* output = new TCHAR[result.GetLength()];
	strcpy(output, result.c_str());
	return output;
}

TCHAR* MaxRemoteConnection::WriteBoolWidget(TCHAR* widgetName, bool value)
{
	atString result;
	m_Connection.WriteBoolWidget(widgetName, value, result);

	TCHAR* output = new TCHAR[result.GetLength()];
	strcpy(output, result.c_str());
	return output;
}

TCHAR* MaxRemoteConnection::WriteIntWidget(TCHAR* widgetName, int value)
{
	atString result;
	m_Connection.WriteIntWidget(widgetName, value, result);

	TCHAR* output = new TCHAR[result.GetLength()];
	strcpy(output, result.c_str());
	return output;
}

TCHAR* MaxRemoteConnection::WriteStringWidget(TCHAR* widgetName, TCHAR* value)
{
	atString result;
	m_Connection.WriteStringWidget(widgetName, value, result);

	TCHAR* output = new TCHAR[result.GetLength()];
	strcpy(output, result.c_str());
	return output;
}

TCHAR* MaxRemoteConnection::WriteVector3Widget(TCHAR* widgetName, Point3 value)
{
	atString result;
	rage::Vec3V vector(value.x, value.y, value.z);
	m_Connection.WriteVector3Widget(widgetName, vector, result);

	TCHAR* output = new TCHAR[result.GetLength()];
	strcpy(output, result.c_str());
	return output;
}

TCHAR* MaxRemoteConnection::WriteVector4Widget(TCHAR* widgetName, Point4 value)
{
	atString result;
	rage::Vec4V vector(value.x, value.y, value.z, value.w);
	m_Connection.WriteVector4Widget(widgetName, vector, result);

	TCHAR* output = new TCHAR[result.GetLength()];
	strcpy(output, result.c_str());
	return output;
}

void MaxRemoteConnection::WriteFreeCameraStream(Point3 position, Point3 up, Point3 forward, float fov)
{
	if ( m_Connection.WidgetExists("Camera/Create camera widgets") == true )
	{
		m_Connection.PressWidgetButton("Camera/Create camera widgets");
	}

	datGrowBuffer growBuffer;
	growBuffer.Init(NULL, 0);
	growBuffer.Preallocate(1024);

	m_Connection.WriteStream(position.x, growBuffer);
	m_Connection.WriteStream(position.y, growBuffer);
	m_Connection.WriteStream(position.z, growBuffer);
	m_Connection.WriteStream(up.x, growBuffer);
	m_Connection.WriteStream(up.y, growBuffer);
	m_Connection.WriteStream(up.z, growBuffer);
	m_Connection.WriteStream(forward.x, growBuffer);
	m_Connection.WriteStream(forward.y, growBuffer);
	m_Connection.WriteStream(forward.z, growBuffer);

	m_Connection.WriteStream(fov, growBuffer); // FOV

	m_Connection.WriteDataStream(m_Connection.GetWidgetId("Camera/Free camera/Camera Override Data"), growBuffer.GetBuffer(), growBuffer.Length());
}

float MaxRemoteConnection::ReadFloatWidget(TCHAR* widgetName)
{
	return m_Connection.ReadFloatWidget(widgetName);
}

bool MaxRemoteConnection::ReadBoolWidget(TCHAR* widgetName)
{
	return m_Connection.ReadBoolWidget(widgetName);
}

int MaxRemoteConnection::ReadIntWidget(TCHAR* widgetName)
{
	return m_Connection.ReadIntWidget(widgetName);
}

TCHAR* MaxRemoteConnection::ReadStringWidget(TCHAR* widgetName)
{
	atString result;
	m_Connection.ReadStringWidget(widgetName, result);

	TCHAR* output = new TCHAR[result.GetLength()];
	strcpy(output, result.c_str());
	return output;
}

Point3 MaxRemoteConnection::ReadVector3Widget(TCHAR* widgetName)
{
	Vec3V_Out vector = m_Connection.ReadVector3Widget(widgetName);
	Point3 result( vector.GetElemf(0), vector.GetElemf(1), vector.GetElemf(2));
	return result;
}

Point4 MaxRemoteConnection::ReadVector4Widget(TCHAR* widgetName)
{
	Vec4V_Out vector = m_Connection.ReadVector4Widget(widgetName);
	Point4 result( vector.GetElemf(0), vector.GetElemf(1), vector.GetElemf(2), vector.GetElemf(3));
	return result;
}

TCHAR* MaxRemoteConnection::SendCommand(TCHAR* command)
{
	atString result;
	m_Connection.SendCommand(command, result);

	TCHAR* resultArray = NULL;
	if ( result.GetLength() != 0 )
	{
		const int kSize = result.GetLength() + 32;
		resultArray = new TCHAR[kSize];
		strcpy_s(resultArray, kSize, result.c_str());
	}
	
	return resultArray;
}

void MaxRemoteConnection::SendSyncCommand()
{
	m_Connection.SendSyncCommand();
}

bool MaxRemoteConnection::IsConnectedToGame()
{
	Connect();

	bool status = false;

	//NOTE:  The m_Connection.IsConnected() call will fire off a ping message.
	//This ping message will be sent to the game and get a response back.
	//If IsConnected() has failed, then the game and RAG are not connected.
	if ( m_Connection.IsConnected() == true )
	{
		 status = m_Connection.IsConnectedToGame();
	}

	Disconnect();
	return status;
}

TCHAR* MaxRemoteConnection::GetExecutableName()
{
	atString exeName;
	m_Connection.GetExecutableName(exeName);

	TCHAR* resultArray = NULL;
	if ( exeName.GetLength() != 0 )
	{
		const int kSize = exeName.GetLength()+1;
		resultArray = new TCHAR[kSize];
		strcpy_s(resultArray, kSize, exeName.c_str());
	}

	return resultArray;
}

TCHAR* MaxRemoteConnection::GetPlatform()
{
	atString platform;
	m_Connection.GetPlatform(platform);

	TCHAR* resultArray = NULL;
	if ( platform.GetLength() != 0 )
	{
		const int kSize = platform.GetLength()+1;
		resultArray = new TCHAR[kSize];
		strcpy_s(resultArray, kSize, platform.c_str());
	}

	return resultArray;
}

TCHAR* MaxRemoteConnection::GetBuildConfig()
{
	atString buildConfig;
	m_Connection.GetBuildConfig(buildConfig);

	TCHAR* resultArray = NULL;
	if ( buildConfig.GetLength() != 0 )
	{
		const int kSize = buildConfig.GetLength()+1;
		resultArray = new TCHAR[kSize];
		strcpy_s(resultArray, kSize, buildConfig.c_str());
	}

	return resultArray;
}

TCHAR* MaxRemoteConnection::GetGameIP()
{
	atString ip;
	m_Connection.GetGameIP(ip);

	TCHAR* resultArray = NULL;
	if ( ip.GetLength() != 0 )
	{
		const int kSize = ip.GetLength()+1;
		resultArray = new TCHAR[kSize];
		strcpy_s(resultArray, kSize, ip.c_str());
	}

	return resultArray;
}

void MaxRemoteConnection::LogToListener(const char* message)
{
	mprintf(message);
	mprintf("\n");
}

//-----------------------------------------------------------------------------

MaxRemoteConnection sRageMaxRemoteConnectionGUP (
	REMOTE_CONNECTION_INTERFACE, _T("RemoteConnection"), 0, NULL, FP_CORE,
	MaxRemoteConnection::connectFn, _T("Connect"), 0, TYPE_BOOL, 0, 0,
	MaxRemoteConnection::disconnectFn, _T("Disconnect"), 0, TYPE_VOID, 0, 0,
	MaxRemoteConnection::isConnectedFn, _T("IsConnected"), 0, TYPE_BOOL, 0, 0,
	MaxRemoteConnection::logMessagesFn, _T("LogMessages"), 0, TYPE_VOID, 0, 1,
		_T("enable"), 0, TYPE_BOOL,
	MaxRemoteConnection::widgetExistsFn, _T("WidgetExists"), 0, TYPE_BOOL, 0, 1,
		_T("widgetName"), 0, TYPE_STRING,
	MaxRemoteConnection::writeFloatFn, _T("WriteFloatWidget"), 0, TYPE_VOID, 0, 2,
		_T("widgetName"), 0, TYPE_STRING,
		_T("value"), 0, TYPE_FLOAT,
	MaxRemoteConnection::writeBoolFn, _T("WriteBoolWidget"), 0, TYPE_VOID, 0, 2,
		_T("widgetName"), 0, TYPE_STRING,
		_T("value"), 0, TYPE_BOOL,
	MaxRemoteConnection::writeIntFn, _T("WriteIntWidget"), 0, TYPE_VOID, 0, 2,
		_T("widgetName"), 0, TYPE_STRING,
		_T("value"), 0, TYPE_INT,
	MaxRemoteConnection::writeStringFn, _T("WriteStringWidget"), 0, TYPE_VOID, 0, 2,
		_T("widgetName"), 0, TYPE_STRING,
		_T("value"), 0, TYPE_STRING,
	MaxRemoteConnection::writeVector3Fn, _T("WriteVector3Widget"), 0, TYPE_VOID, 0, 2,
		_T("widgetName"), 0, TYPE_STRING,
		_T("value"), 0, TYPE_POINT3,
	MaxRemoteConnection::writeVector4Fn, _T("WriteVector4Widget"), 0, TYPE_VOID, 0, 2,
		_T("widgetName"), 0, TYPE_STRING,
		_T("value"), 0, TYPE_POINT4,
	MaxRemoteConnection::writeFreeCameraStreamFn, _T("WriteFreeCameraStream"), 0, TYPE_VOID, 0, 4,
		_T("position"), 0, TYPE_POINT3,
		_T("up"), 0, TYPE_POINT3,
		_T("forward"), 0, TYPE_POINT3,
		_T("fov"), 0, TYPE_FLOAT,
	MaxRemoteConnection::readFloatFn, _T("ReadFloatWidget"), 0, TYPE_VOID, 0, 1,
		_T("widgetName"), 0, TYPE_STRING,
	MaxRemoteConnection::readBoolFn, _T("ReadBoolWidget"), 0, TYPE_VOID, 0, 1,
		_T("widgetName"), 0, TYPE_STRING,
	MaxRemoteConnection::readIntFn, _T("ReadIntWidget"), 0, TYPE_VOID, 0, 1,
		_T("widgetName"), 0, TYPE_STRING,
	MaxRemoteConnection::readStringFn, _T("ReadStringWidget"), 0, TYPE_VOID, 0, 1,
		_T("widgetName"), 0, TYPE_STRING,
	MaxRemoteConnection::readVector4Fn, _T("ReadVector4Widget"), 0, TYPE_VOID, 0, 1,
		_T("widgetName"), 0, TYPE_STRING,
	MaxRemoteConnection::sendCommandFn, _T("SendCommand"), 0, TYPE_STRING, 0, 1,
		_T("command"), 0, TYPE_STRING,
	MaxRemoteConnection::sendSyncCommandFn, _T("SendSyncCommand"), 0, TYPE_VOID, 0, 0,
	MaxRemoteConnection::isConnectedToGameFn, _T("IsConnectedToGame"), 0, TYPE_BOOL, 0, 0,
	MaxRemoteConnection::getExecutableNameFn, _T("GetExecutableName"), 0, TYPE_STRING, 0, 0,
	MaxRemoteConnection::getPlatformFn, _T("GetPlatform"), 0, TYPE_STRING, 0, 0,
	MaxRemoteConnection::getBuildConfigFn, _T("GetBuildConfig"), 0, TYPE_STRING, 0, 0,
	MaxRemoteConnection::getGameIPFn, _T("GetGameIP"), 0, TYPE_STRING, 0, 0,
	end
	);
//-----------------------------------------------------------------------------

class RageMaxAnimExprExportGUPClassDesc : public ClassDesc2
{
public:
	int				IsPublic()						{ return 1; }
	void*			Create(BOOL loading = FALSE)	{ return &sRageMaxRemoteConnectionGUP; } 
	const TCHAR*	ClassName()						{ return _T("MaxRemoteConnection"); }
	SClass_ID		SuperClassID()					{ return GUP_CLASS_ID; } 
	Class_ID		ClassID()						{ return REMOTE_CONNECTION_CLASS_ID; }
	const TCHAR*	Category()						{ return GetString(IDS_CATEGORY); }
};

static RageMaxAnimExprExportGUPClassDesc AnimExprExportGUPDesc;

//-----------------------------------------------------------------------------

ClassDesc* GetMaxRemoteConnectionClassDesc()
{
	return &AnimExprExportGUPDesc;
}

