//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by rageMaxRemoteConnect.rc
//
#define IDS_LODATTRIBS_NAME             14
#define IDS_LODATTR                     15
#define IDS_LODCONTAINER                16
#define IDS_LODCONTAINERMOD             17
#define IDS_LODDRAWABLEATTRIBS_NAME     18
#define IDS_SCENELODHIERARCHY_NAME      19
#define IDS_DRAWABLELODHIERARCHY_NAME   20
#define IDS_LODDRAWABLECHILDATTRIBS_NAME 21
#define IDS_REMOTE_CONNECTION_CLASS_NAME 22
#define IDS_CATEGORY                    23
#define IDS_DESCRIPTION                 101
#define IDS_AUTHOR                      102
#define IDD_LODDATA                     111
#define IDD_DIALOG_LOD                  114
#define IDC_TREE_LOD                    1029
#define IDC_BTN_REFRESH                 1030
#define IDC_PICK_PARENT                 1032
#define IDC_PARENTNAME                  1033
#define IDC_RESET_PARENT                1034

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
