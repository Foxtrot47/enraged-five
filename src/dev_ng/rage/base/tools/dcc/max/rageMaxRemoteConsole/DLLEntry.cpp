//
// filename:	DLLEntry.cpp
// author:		Kevin Weinberg
// date:		8 December 2010
//

// --- Include Files ------------------------------------------------------------

// 3D Studio Max SDK headers
#include "Max.h"

// Local headers
#include "rageMaxRemoteConsole.h"
#include "resource.h"
#include "Win32Res.h"

// --- Constants ----------------------------------------------------------------
const int	sC_cnt_NumberOfClasses = 1;

// --- Globals ------------------------------------------------------------------

HINSTANCE	g_hInstance = NULL;
int			g_ControlsInit = FALSE;

// --- Code ---------------------------------------------------------------------

//
// name:		DllMain
// description:	DLL Plugin Entry-Point
//
BOOL WINAPI 
DllMain( HINSTANCE hInstDLL, ULONG dwReason, LPVOID plReserved )
{
	// Store the DLLs instance handle
	g_hInstance = hInstDLL;

	if ( FALSE == g_ControlsInit )
	{
	#if MAX_VERSION_MAJOR < 11 
		InitCustomControls(g_hInstance);	// Initialize MAX's custom controls
	#endif //MAX_VERSION_MAJOR

		// Initialise platform common controls
		InitCommonControls( );
		g_ControlsInit = TRUE;
	}

	return TRUE;
}


RAGEMAXREMOTECONNECTION_API int 
LibInitialize( void )
{
	return ( 1 );
}


RAGEMAXREMOTECONNECTION_API int
LibShutdown( void )
{
	return ( 1 );
}


//
// name:		LibDescription
// description:	Return plugin description string (Max API)
//
RAGEMAXREMOTECONNECTION_API const TCHAR* 
LibDescription()
{
	return GetStringResource( g_hInstance, IDS_DESCRIPTION );
}


//
// name:		LibNumberClasses
// description:	Return number of classes registered by plugin (Max API)
//
RAGEMAXREMOTECONNECTION_API int 
LibNumberClasses( )
{
	return sC_cnt_NumberOfClasses;
}


//
// name:		LibClassDesc
// description:	Return indexed class description class
//
RAGEMAXREMOTECONNECTION_API ClassDesc* 
LibClassDesc( int nClassIndex )
{
	assert( nClassIndex < sC_cnt_NumberOfClasses );
	switch ( nClassIndex )
	{
	case 0: 
		return ( GetMaxRemoteConnectionClassDesc() );
	}
	return ( NULL );
}


//
// name:		LibVersion
// description:	Return version of Max SDK plugin was compiled with
//
RAGEMAXREMOTECONNECTION_API ULONG 
LibVersion( )
{
	return ( VERSION_3DSMAX );
}


//
// name:		CanAutoDefer
// description:
//
RAGEMAXREMOTECONNECTION_API ULONG 
CanAutoDefer( ) 
{ 
	return 0; 
}

// DLLEntry.cpp
