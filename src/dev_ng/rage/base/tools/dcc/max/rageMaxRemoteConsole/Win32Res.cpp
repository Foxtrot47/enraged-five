//
// filename:	Win32Res.cpp
// author:		David Muir
// date:		15 January 2007
// description:	
//

// --- Include Files ------------------------------------------------------------

// Local headers
#include "Win32Res.h"

// --- Constants ----------------------------------------------------------------
const int sC_cnt_StringResourceBufSize	= 256;

// --- Code ---------------------------------------------------------------------

//
// name:		GetStringResource
// description:	Return string from Win32 DLL resources given resource ID
//
TCHAR* GetStringResource( HINSTANCE hInst, int nResID )
{
	static TCHAR buf[ sC_cnt_StringResourceBufSize ];

	if ( hInst )
		return LoadString( hInst, nResID, buf, sizeof(buf) ) ? buf : NULL;
	return ( NULL );
}


TCHAR *GetString(int id)
{
	static TCHAR buf[sC_cnt_StringResourceBufSize];

	if (g_hInstance)
		return LoadString(g_hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	return NULL;
}



// End of file
