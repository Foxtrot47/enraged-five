//
// filename:	Win32Res.h
// author:		David Muir
// date:		15 January 2007
// description:	Win32 binary resource accessors interface
//

#ifndef INC_WIN32RES_H_
#define INC_WIN32RES_H_

// --- Include Files ------------------------------------------------------------

// Platform SDK headers
#pragma warning(push)
#pragma warning(disable:4668)
#include <Windows.h>
#pragma warning(pop)

// --- Globals ------------------------------------------------------------------
extern HINSTANCE	g_hInstance;

TCHAR* GetStringResource( HINSTANCE hInst, int nResID );
TCHAR *GetString(int id);

#endif // !INC_WIN32RES_H_

// End of file
