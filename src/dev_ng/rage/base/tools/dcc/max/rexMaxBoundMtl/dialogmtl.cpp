#include "dialogMtl.h"
#include "resource.h"
#include <3dsmaxport.h>

extern HINSTANCE g_hDllInstance;
#if HACK_GTA4
extern TCHAR *GetString(int id);
#endif // HACK_GTA4

using namespace rage;

#if HACK_GTA4

//This array is meant to match up within the enumeration in RexBoundMtlMaxDlg.
//These strings are used so MaxScript can query plug-in data about each flag
//instead of forcing MaxScript to hardcode it.
char* RexBoundMtlMaxDlg::RexBoundFlagNames[FLAG_MAXBOUNDMTLFLAGS] = {
	"Stairs",
	"Non Climbable",
	"See Through",
	"Shoot Through",
	"Does Not Provide Cover",
	"Path",
	"Non Camera Collidable",
	"Shoot Through FX",
	"No Decal",
	"No Navmesh",
	"No Ragdoll",
	"Reserved for Tools",
	"No Particle Effects",
	"Too Steep",
	"No Network Spawn",
	"Non Camera Collidable Allow Clipping"
};

class InternRefObjPick : public PickObjectProc 
{
	RexBoundMtlMaxDlg *intRefObj;
public:		

	BOOL Filter(INode* p_Node);
	BOOL Pick(INode* p_Node);	

	void EnterMode();
	void ExitMode();

	void SetRefdObj(RexBoundMtlMaxDlg *l) { intRefObj = l; }
};

BOOL InternRefObjPick::Filter(INode* node)
{
	if (node == NULL)
		return FALSE;
	Object* obj = node->EvalWorldState(0).obj;
	if (obj->ClassID() != MILOROOM_CLASS_ID)
		return FALSE;

	return TRUE;
}

BOOL InternRefObjPick::Pick(INode* node)
{
	intRefObj->GetThing()->ReplaceReference(0,node);

	if(node)
		intRefObj->GetMiloButton()->SetText(node->GetName());
	else
		intRefObj->GetMiloButton()->SetText("");

	intRefObj->GetMiloButton()->SetCheck(false);
	intRefObj->GetMtlParams()->EndPickMode();
	return FALSE;
}

void InternRefObjPick::EnterMode()
{
	if(intRefObj->GetMiloButton())
		intRefObj->GetMiloButton()->SetCheck(true);
}

void InternRefObjPick::ExitMode()
{
	if(intRefObj->GetMiloButton())
		intRefObj->GetMiloButton()->SetCheck(false);
}

static InternRefObjPick theSelRef;
#endif // HACK_GTA4

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RexBoundMtlMaxDlg::RexBoundMtlMaxDlg(RexBoundMtlMax* p_Mtl,IMtlParams* p_MtlParams):
	mp_Mtl(p_Mtl),
	mp_MtlParams(p_MtlParams),
#if HACK_GTA4
	m_Wnd(NULL),
	mp_PedDensitySpin(NULL),
	mp_MiloButton(NULL),
	previousMode(NULL),
	m_Picking(false)
#else
	m_Wnd(NULL)
#endif // HACK_GTA4
{
	m_Wnd = mp_MtlParams->AddRollupPage(g_hDllInstance,
										MAKEINTRESOURCE(IDD_DLG_MATBNDEDIT),
										RexBoundMtlMaxProc,
										_T("RexBoundMtl"),
										(LPARAM)this);

	SetupUI();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RexBoundMtlMaxDlg::~RexBoundMtlMaxDlg()
{
#if HACK_GTA4
	if(mp_Mtl)
		mp_Mtl->ClearDialog();
#endif // HACK_GTA4
}

#if HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::Refresh()
{
	if(mp_PedDensitySpin)
	{
		mp_PedDensitySpin->SetValue(mp_Mtl->m_Density,FALSE);
		SetWindowText(GetDlgItem(m_Wnd,IDC_CBO_NAME),mp_Mtl->m_CollisionName);
		SetWindowText(GetDlgItem(m_Wnd,IDC_CBO_PROCEDURAL),mp_Mtl->m_ProceduralName);
	}
}
#endif // HACK_GTA4

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::SetThing(ReferenceTarget *m)
{
	mp_Mtl = (RexBoundMtlMax*)m;
}

#if HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::ClearMiloRoomName()
{
	mp_MiloButton->SetText("none");
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::ClearMiloRoom()
{
	mp_Mtl->ReplaceReference(0,NULL);
	ClearMiloRoomName();
}
#endif // HACK_GTA4

/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RexBoundMtlMaxDlg::FindSubTexFromHWND(HWND hwnd)
{
}
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK RexBoundMtlMaxDlg::RexBoundMtlMaxProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	RexBoundMtlMaxDlg* p_ParamDlg = DLGetWindowLongPtr<RexBoundMtlMaxDlg*>( DlgWnd );

	switch(msg)
	{
	case CC_SPINNER_CHANGE:
		if (LOWORD(wParam) == IDC_SPN_PEDMULTIPLIER)
		{
			p_ParamDlg->OnUpdatePedDensity();
		}
		break;

	case WM_INITDIALOG:

		((RexBoundMtlMaxDlg*)lParam)->OnInitDialog(DlgWnd);
		return TRUE;
#if HACK_GTA4
	case WM_DESTROY:

		p_ParamDlg->OnDestroy();
		break;

#endif // HACK_GTA4
	case WM_COMMAND:
#if HACK_GTA4
		if((LOWORD(wParam) == IDC_BTN_MILOROOM))
		{
			theSelRef.SetRefdObj(p_ParamDlg);
			p_ParamDlg->mp_MtlParams->SetPickMode(&theSelRef);
			return TRUE;
		}

		if(LOWORD(wParam) == IDC_BTN_RESET)
		{
			p_ParamDlg->ClearMiloRoom();
		}

		if((LOWORD(wParam) == IDC_CHK_STAIRS) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkStairs();
		if((LOWORD(wParam) == IDC_CHK_NONCLIMABLE) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkClimable();
		if((LOWORD(wParam) == IDC_CHK_SEETHROUGH) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkSeeThrough();
		if((LOWORD(wParam) == IDC_CHK_SHOOTTHROUGH) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkShootThrough();
		if((LOWORD(wParam) == IDC_CHK_WET) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkWet();
		if((LOWORD(wParam) == IDC_CHK_SPARE1) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkPath();
		if((LOWORD(wParam) == IDC_CHK_SPARE2) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkNoCamColl();
		if((LOWORD(wParam) == IDC_CHK_SHOOT_THROUGH_FX ) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkShootThroughFX();
		if((LOWORD(wParam) == IDC_CHK_NODECAL) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkNoDecal();
		if((LOWORD(wParam) == IDC_CHK_NONAVMESH) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkNoNavmesh();
		if((LOWORD(wParam) == IDC_CHK_NORAGDOLL) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkNoRagdoll();
		if((LOWORD(wParam) == IDC_CHK_PROCCULLIMMUNE) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkProcCullImmune();
		if((LOWORD(wParam) == IDC_CHK_NOPTFX) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkNoParticleEffects();
		if((LOWORD(wParam) == IDC_CHK_TOO_STEEP) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkToSteepForPed();
		if((LOWORD(wParam) == IDC_CHK_NONETSPAWN) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkNoNetworkSpawn();
		if((LOWORD(wParam) == IDC_CHK_NONCAMERACOLLIDABLEALLOWCLIPPING) && (HIWORD(wParam) == BN_CLICKED))
			p_ParamDlg->OnChkNoCamCollAllowClip();
		
		
		

#endif // HACK_GTA4
		if(LOWORD(wParam) == IDC_CBO_NAME)
		{
			switch(HIWORD(wParam))
			{
			case CBN_EDITCHANGE:
				p_ParamDlg->OnUpdateCollisionName();
				return TRUE;
			case CBN_SELCHANGE:
				p_ParamDlg->OnSelChangeCollisionName();
				return TRUE;
			case CBN_SETFOCUS:
				p_ParamDlg->OnCollisionNameSetFocus();
				return TRUE;
			case CBN_KILLFOCUS:
				p_ParamDlg->OnCollisionNameKillFocus();
				p_ParamDlg->OnUpdateCollisionName();
				return TRUE;
			}
		}

		if(LOWORD(wParam) == IDC_CBO_PROCEDURAL)
		{
			switch(HIWORD(wParam))
			{
			case CBN_EDITCHANGE:
				p_ParamDlg->OnUpdateProceduralName();
			case CBN_SELCHANGE:
				p_ParamDlg->OnSelChangeProceduralName();
				return TRUE;
			case CBN_SETFOCUS:
				p_ParamDlg->OnProceduralNameSetFocus();
				return TRUE;
			case CBN_KILLFOCUS:
				p_ParamDlg->OnProceduralNameKillFocus();
				p_ParamDlg->OnUpdateProceduralName();
				return TRUE;
			}
		}
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::Reset()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::SetupUI()
{
	SetWindowText(GetDlgItem(m_Wnd,IDC_CBO_NAME),mp_Mtl->m_CollisionName);
	SetWindowText(GetDlgItem(m_Wnd,IDC_CBO_PROCEDURAL),mp_Mtl->m_ProceduralName);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::SetupShader(const atString& r_ShaderName,bool IsPreset)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::UpdateMapName(s32 Index,const atString& r_MapName)
{
}

#if HACK_GTA4
// More clean up
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnDestroy()
{
	if(mp_PedDensitySpin)
	{
		ReleaseICustButton(mp_MiloButton);
		mp_Mtl->m_Density = mp_PedDensitySpin->GetIVal();
		ReleaseISpinner(mp_PedDensitySpin);
		ReleaseICustEdit(mp_PedDensityEdit);
		mp_PedDensitySpin = NULL;
		mp_PedDensityEdit = NULL;
	}
}
#endif // HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnInitDialog(HWND DlgWnd)
{
#if HACK_GTA4
	mp_MiloButton = GetICustButton(GetDlgItem(DlgWnd,IDC_BTN_MILOROOM));
	mp_MiloButton->SetType(CBT_CHECK);
	mp_MiloButton->SetButtonDownNotify(TRUE);
	mp_MiloButton->SetHighlightColor(GREEN_WASH);

	INode* p_Node = (INode*)mp_Mtl->GetReference(0);

	if (p_Node)
	{
		const char* p_Name = p_Node->GetName();
		mp_MiloButton->SetText((char*)p_Name);
	}

	mp_PedDensityEdit = GetICustEdit(GetDlgItem(DlgWnd,IDC_EDT_PEDMULTIPLIER));
	mp_PedDensitySpin = GetISpinner(GetDlgItem(DlgWnd,IDC_SPN_PEDMULTIPLIER));

	mp_PedDensitySpin->SetLimits(0.0f,7.0f);
	mp_PedDensitySpin->SetScale(1.0f);
	mp_PedDensitySpin->SetValue(mp_Mtl->m_Density,FALSE);
	mp_PedDensitySpin->LinkToEdit(GetDlgItem(DlgWnd,IDC_EDT_PEDMULTIPLIER),EDITTYPE_INT);
#endif // HACK_GTA4

#if HACK_GTA4
	// LPXO: 64-bit fix
	DLSetWindowLongPtr( DlgWnd, this );
#else
	SetWindowLongPtr(DlgWnd,GWLP_USERDATA, (LONG)this);
#endif // HACK_GTA4

	const char* p_Name;
	s32 i,Count = mp_Mtl->m_CollisionList.GetCount();
	HWND ListWnd = GetDlgItem(DlgWnd,IDC_CBO_NAME);

	for(i=0;i<Count;i++)
	{
		p_Name = mp_Mtl->m_CollisionList[i];
		SendMessage(ListWnd,CB_ADDSTRING,0,(LPARAM)p_Name);
	}

	Count = mp_Mtl->m_ProceduralList.GetCount();
	ListWnd = GetDlgItem(DlgWnd,IDC_CBO_PROCEDURAL);

	for(i=0;i<Count;i++)
	{
		p_Name = mp_Mtl->m_ProceduralList[i];
		SendMessage(ListWnd,CB_ADDSTRING,0,(LPARAM)p_Name);
	}
#if HACK_GTA4
	if(mp_Mtl->m_Flags & (1 << FLAG_ISSTAIRS))
		CheckDlgButton(DlgWnd,IDC_CHK_STAIRS,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_ISNONCLIMABLE))
		CheckDlgButton(DlgWnd,IDC_CHK_NONCLIMABLE,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_SEETHROUGH))
		CheckDlgButton(DlgWnd,IDC_CHK_SEETHROUGH,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_SHOOTTHROUGH))
		CheckDlgButton(DlgWnd,IDC_CHK_SHOOTTHROUGH,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_DOESNTPROVIDECOVER))
		CheckDlgButton(DlgWnd,IDC_CHK_WET,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_PATH))
		CheckDlgButton(DlgWnd,IDC_CHK_SPARE1,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_NOCAMCOLL))
		CheckDlgButton(DlgWnd,IDC_CHK_SPARE2,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_SHOOT_THROUGH_FX))
		CheckDlgButton(DlgWnd,IDC_CHK_SHOOT_THROUGH_FX ,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_NODECAL))
		CheckDlgButton(DlgWnd,IDC_CHK_NODECAL,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_NONAVMESH))
		CheckDlgButton(DlgWnd,IDC_CHK_NONAVMESH,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_NORAGDOLL))
		CheckDlgButton(DlgWnd,IDC_CHK_NORAGDOLL,BST_CHECKED);
	if(mp_Mtl->m_ProcCullImmune)
		CheckDlgButton(DlgWnd,IDC_CHK_PROCCULLIMMUNE,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_NOPTFX))
		CheckDlgButton(DlgWnd,IDC_CHK_NOPTFX,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_TOOSTEEP))
		CheckDlgButton(DlgWnd,IDC_CHK_TOO_STEEP,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_NONETSPAWN))
		CheckDlgButton(DlgWnd,IDC_CHK_NONETSPAWN,BST_CHECKED);
	if(mp_Mtl->m_Flags & (1 << FLAG_NOCAMCOLLALLOWCLIP))
		CheckDlgButton(DlgWnd,IDC_CHK_NONCAMERACOLLIDABLEALLOWCLIPPING,BST_CHECKED);
	
	
#endif // HACK_GTA4
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnUpdateCollisionName()
{
	char Get[256];
	GetWindowText(GetDlgItem(m_Wnd,IDC_CBO_NAME),Get,256);
	if(stricmp(Get,mp_Mtl->m_CollisionName) != 0)
	{
		mp_Mtl->m_CollisionName = Get;

#if HACK_GTA4
		s32 populationDensity = RexBoundMtlMax::GetPopDensityFromName(mp_Mtl->m_CollisionName);
		mp_Mtl->m_Density = populationDensity;
		mp_PedDensitySpin->SetValue(populationDensity,FALSE);
#endif //HACK_GTA4

	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnSelChangeCollisionName()
{
	HWND DlgItem = GetDlgItem(m_Wnd,IDC_CBO_NAME);

	s32 Index = SendMessage(DlgItem,CB_GETCURSEL,0,0);

	if(Index != CB_ERR)
	{
		char Get[1024];
		SendMessage(DlgItem,CB_GETLBTEXT,Index,(LPARAM)Get);
		mp_Mtl->m_CollisionName = Get;
#if HACK_GTA4
		s32 populationDensity = RexBoundMtlMax::GetPopDensityFromName(mp_Mtl->m_CollisionName);
		mp_Mtl->m_Density = populationDensity;
		mp_PedDensitySpin->SetValue(populationDensity,FALSE);
#endif // HACK_GTA4
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnCollisionNameSetFocus()
{
	DisableAccelerators();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnCollisionNameKillFocus()
{
	EnableAccelerators();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnUpdatePedDensity()
{
	int pedDensity = mp_PedDensitySpin->GetIVal();
	mp_Mtl->m_Density = pedDensity;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnUpdateProceduralName()
{
	char Get[256];
	GetWindowText(GetDlgItem(m_Wnd,IDC_CBO_PROCEDURAL),Get,256);

	mp_Mtl->m_ProceduralName = Get;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnSelChangeProceduralName()
{
	HWND DlgItem = GetDlgItem(m_Wnd,IDC_CBO_PROCEDURAL);

	s32 Index = SendMessage(DlgItem,CB_GETCURSEL,0,0);

	if(Index != CB_ERR)
	{
		char Get[1024];
		SendMessage(DlgItem,CB_GETLBTEXT,Index,(LPARAM)Get);
		mp_Mtl->m_ProceduralName = Get;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnProceduralNameSetFocus()
{
	DisableAccelerators();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnProceduralNameKillFocus()
{
	EnableAccelerators();
}
#if HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkStairs()
{
	if(m_Wnd)
	{
		if(IsDlgButtonChecked(m_Wnd,IDC_CHK_STAIRS) == BST_CHECKED)
			mp_Mtl->m_Flags |= (1 << FLAG_ISSTAIRS);
		else
			mp_Mtl->m_Flags &= ~(1 << FLAG_ISSTAIRS);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkClimable()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_NONCLIMABLE) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_ISNONCLIMABLE);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_ISNONCLIMABLE);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkSeeThrough()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_SEETHROUGH) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_SEETHROUGH);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_SEETHROUGH);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkShootThrough()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_SHOOTTHROUGH) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_SHOOTTHROUGH);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_SHOOTTHROUGH);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkWet()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_WET) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_DOESNTPROVIDECOVER);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_DOESNTPROVIDECOVER);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkPath()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_SPARE1) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_PATH);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_PATH);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkNoCamColl()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_SPARE2) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_NOCAMCOLL);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_NOCAMCOLL);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkShootThroughFX()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_SHOOT_THROUGH_FX ) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_SHOOT_THROUGH_FX);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_SHOOT_THROUGH_FX);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkNoDecal()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_NODECAL) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_NODECAL);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_NODECAL);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkNoNavmesh()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_NONAVMESH) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_NONAVMESH);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_NONAVMESH);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkNoRagdoll()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_NORAGDOLL) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_NORAGDOLL);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_NORAGDOLL);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkProcCullImmune()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_PROCCULLIMMUNE) == BST_CHECKED)
		mp_Mtl->m_ProcCullImmune = true;
	else
		mp_Mtl->m_ProcCullImmune = false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkNoParticleEffects()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_NOPTFX) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_NOPTFX);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_NOPTFX);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMaxDlg::OnChkToSteepForPed()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_TOO_STEEP) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_TOOSTEEP);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_TOOSTEEP);
}

void RexBoundMtlMaxDlg::OnChkNoNetworkSpawn()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_NONETSPAWN) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_NONETSPAWN);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_NONETSPAWN);
}

void RexBoundMtlMaxDlg::OnChkNoCamCollAllowClip()
{
	if(IsDlgButtonChecked(m_Wnd,IDC_CHK_NONCAMERACOLLIDABLEALLOWCLIPPING) == BST_CHECKED)
		mp_Mtl->m_Flags |= (1 << FLAG_NOCAMCOLLALLOWCLIP);
	else
		mp_Mtl->m_Flags &= ~(1 << FLAG_NOCAMCOLLALLOWCLIP);
}

#endif // HACK_GTA4

