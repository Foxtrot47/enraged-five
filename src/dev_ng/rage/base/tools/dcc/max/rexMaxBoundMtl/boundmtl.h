#ifndef __REX_MAX_MTL_BOUND_H__
#define __REX_MAX_MTL_BOUND_H__

#define REXBND_MATERIAL_CLASS_ID Class_ID(0x1e2e6fe1, 0x51600e4c)
#if HACK_GTA4
// MILO Room helper and MILO root helper
#define MILOROOM_CLASS_ID Class_ID(0x35636012, 0x7abf523a)
#define MILO_CLASS_ID	Class_ID(0x5591738d, 0x54e3342e)
#endif // HACK_GTA4

#include "atl/string.h"
#include "file/stream.h"

#include "IRageBoundMaterial.h"

#ifdef RSTMAXMTL_DLL
#define REXBOUNDMAXMTL_EXPORT __declspec( dllexport )
#else
#ifdef RSTMAXMTL_NOIMPORT
#define REXBOUNDMAXMTL_EXPORT
#else
#define REXBOUNDMAXMTL_EXPORT __declspec( dllimport )
#endif //RSTMAXMTL_NOIMPORT
#endif //RSTMAXMTL_DLL

namespace rage {

//////////////////////////////////////////////////////////////////////////////////////////////////////////
class RexBoundMtlMax : public Mtl, public IRageBoundMaterial
{
public:
	static ClassDesc2* Creator();

	friend class RexBoundMtlMaxDlg;

	RexBoundMtlMax();

	//from Animatable

	virtual Class_ID ClassID() { return REXBND_MATERIAL_CLASS_ID; }
	virtual void DeleteThis() { delete this; }

	//from ReferenceTarget
#if ( MAX_RELEASE < 9000 ) 
	virtual RefTargetHandle Clone(RemapDir &remap = NoRemap());
#else
	virtual RefTargetHandle	Clone(RemapDir &remap = DefaultRemapDir());
#endif

	REXBOUNDMAXMTL_EXPORT bool IsSame(const RexBoundMtlMax& that);

#if HACK_GTA4
	// Implemented for milo room reference
	int NumRefs() { return 1; }
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
#endif // HACK_GTA4

	//from ReferenceMaker
#if HACK_GTA4
	// Implemented to clean up milo room reference
	virtual RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message);
#else
	virtual RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message) { return REF_SUCCEED; }
#endif // HACK_GTA4

	//from MtlBase

	virtual void Update(TimeValue t, Interval& valid) {}
	virtual void Reset();
	virtual Interval Validity(TimeValue t) { return FOREVER; }
	virtual ParamDlg* CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp);
	virtual IOResult Load(ILoad *iload);
	virtual IOResult Save(ISave *isave);

	//from Mtl

	virtual void GetClassName(TSTR& s) { s = TSTR(_T("RexBoundMtl")); }
	virtual Color GetAmbient(int mtlNum=0, BOOL backFace=FALSE) { return GetDiffuse(mtlNum, backFace); }
	virtual Color GetDiffuse(int mtlNum=0, BOOL backFace=FALSE);
	virtual Color GetSpecular(int mtlNum=0, BOOL backFace=FALSE) { return GetDiffuse(mtlNum, backFace); }
	virtual float GetShininess(int mtlNum=0, BOOL backFace=FALSE) { return 1.0f; }
	virtual	float GetShinStr(int mtlNum=0, BOOL backFace=FALSE) { return 1.0f; }
	virtual float GetXParency(int mtlNum=0, BOOL backFace=FALSE) { return 0.0f; }
	virtual void SetAmbient(Color c, TimeValue t) {}
	virtual void SetDiffuse(Color c, TimeValue t) {if(m_pMaterialColour)delete m_pMaterialColour; m_pMaterialColour = new Color(c);}
	virtual void SetSpecular(Color c, TimeValue t) {}
	virtual void SetShininess(float v, TimeValue t) {}
	virtual void Shade(ShadeContext& sc);

	//unique exposed methods
#if HACK_GTA4	
	void ClearDialog() { mp_MtlDlg = NULL; }
#endif //HACK_GTA4

	REXBOUNDMAXMTL_EXPORT const atString& GetCollisionName() const { return m_CollisionName; }

#if HACK_GTA4
	// Implement the function to set the density and refresh the dlg
	REXBOUNDMAXMTL_EXPORT void SetCollisionName(const char* p_in);
#else
	REXBOUNDMAXMTL_EXPORT void SetCollisionName(const char* p_in) { m_CollisionName = p_in; }
#endif //HACK_GTA4

	REXBOUNDMAXMTL_EXPORT const atString& GetProceduralName() const { return m_ProceduralName; }
	REXBOUNDMAXMTL_EXPORT void SetProceduralName(const char* p_in) { m_ProceduralName = p_in; }

#if HACK_GTA4
	// Accessor functions for new members
	REXBOUNDMAXMTL_EXPORT s32 GetFlags() const { return m_Flags; }
	REXBOUNDMAXMTL_EXPORT bool GetProcCullImmune() const { return m_ProcCullImmune; }
	REXBOUNDMAXMTL_EXPORT void SetProcCullImmune(bool bProcCullImmune) { m_ProcCullImmune = bProcCullImmune; }
	REXBOUNDMAXMTL_EXPORT s32 GetPopDensity() const { return (s32)((m_Density < 0.0f) ? 0.0f : m_Density); }
	REXBOUNDMAXMTL_EXPORT void SetPopDensity(s32 density) { m_Density = density; }
	REXBOUNDMAXMTL_EXPORT int GetRoomID() const;

	REXBOUNDMAXMTL_EXPORT void SetFlags(s32 Flags) { m_Flags = Flags; }

	REXBOUNDMAXMTL_EXPORT static void AddCollisionName(const atString& r_NewCollision,s32 DefPopDensity);
	REXBOUNDMAXMTL_EXPORT static s32 GetPopDensityFromName(const atString& r_Procedural);
#else
	REXBOUNDMAXMTL_EXPORT static void AddCollisionName(const atString& r_NewCollision);
#endif // HACK_GTA4

	REXBOUNDMAXMTL_EXPORT static void AddProceduralName(const atString& r_NewProcedural);

	//IRageBoundMaterial FP Interface methods
	TSTR FpGetCollisionName() const { return TSTR((const char*)m_CollisionName); }
	void FpSetCollisionName(TSTR colName) { m_CollisionName = colName.data(); }
	TSTR FpGetProceduralName() const { return TSTR((const char*)m_ProceduralName); }
	void FpSetProceduralName(TSTR procName) { m_ProceduralName = procName.data(); }

	FPInterface* GetInterface(Interface_ID id)
	{
		if( id == RAGEBOUNDMTL_INTERFACE )
			return (IRageBoundMaterial*)this;
		else
			return NULL;
	}

	static atArray<atString>& GetProceduralList() { return  m_ProceduralList; }
	static atArray<atString>& GetCollisionList() { return  m_CollisionList; }

protected:
	enum
	{
		CURRENT_FILE_VERSION = 6
	};

	s32 GetDataSize();
	static void WriteString(fiStream* p_Save,const atString& r_Write);
	static void ReadString(fiStream* p_Load,atString& r_Read);

	HWND m_MtlEditWnd;
	RexBoundMtlMaxDlg* mp_MtlDlg;
	atString m_CollisionName;
	atString m_ProceduralName;

#if HACK_GTA4
	s32 m_Flags; // Additional flags e.g. "Doesn't Provide Cover"
	s32 m_Density; // Ped density mulitiplier
	INode* mp_miloRoom; // Room assignment.  Required for spawning cameras for cutscenes
	bool m_ProcCullImmune;
#endif // HACK_GTA4

	static atArray<atString> m_CollisionList;
#if HACK_GTA4
	static atArray<s32> m_CollPopDensityList;
#endif // HACK_GTA4
	static atArray<atString> m_ProceduralList;

	Color *m_pMaterialColour;
};

} //namespace rage {

#endif //__REX_MAX_MTL_BOUND_H__
