#include "boundmtl.h"
#include "dialogMtl.h"
#include "file/device.h"

using namespace rage;

extern HINSTANCE g_hDllInstance;

atArray<atString> RexBoundMtlMax::m_CollisionList;
atArray<atString> RexBoundMtlMax::m_ProceduralList;
#if HACK_GTA4
atArray<s32> RexBoundMtlMax::m_CollPopDensityList;
#endif // HACK_GTA4

#define REXBOUNDMTL_COLOUR_CHUNKID 2

//////////////////////////////////////////////////////////////////////////////////////////////////////////
class rstRexBoundMtlMaxClassDesc : public ClassDesc2
{
public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new RexBoundMtlMax(); }
	const TCHAR *	ClassName() { return _T("RexBoundMtl"); }
	SClass_ID		SuperClassID() { return MATERIAL_CLASS_ID; }
	Class_ID		ClassID() { return REXBND_MATERIAL_CLASS_ID; }
	const TCHAR* 	Category() { return _T("material"); }
	const TCHAR*	InternalName() { return _T("RexBoundMtl"); }
	HINSTANCE		HInstance() { return g_hDllInstance; }
};

static rstRexBoundMtlMaxClassDesc rbmClassDesc;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

static FPInterfaceDesc rbm_mixininterface( RAGEBOUNDMTL_INTERFACE,
	_T("rageBoundMaterial"), 0,
	&rbmClassDesc, FP_MIXIN,

	rbm_FpGetCollisionName, _T("rbmGetCollisionName"), 0, TYPE_TSTR_BV, 0, 0,
	
	rbm_FpSetCollisionName, _T("rbmSetCollisionName"), 0, TYPE_VOID, 0, 1,
		_T("name"), 0, TYPE_TSTR_BV,
	
	rbm_FpGetProceduralName, _T("rbmGetProceduralName"), 0, TYPE_TSTR_BV, 0, 0,
	
	rbm_FpSetProceduralName, _T("rbmSetProceduralName"), 0, TYPE_VOID, 0, 1,
		_T("name"), 0, TYPE_TSTR_BV,

	end
);


FPInterfaceDesc* IRageBoundMaterial::GetDesc()
{
	return &rbm_mixininterface;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ClassDesc2* RexBoundMtlMax::Creator()
{
	return &rbmClassDesc;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
#if HACK_GTA4
RexBoundMtlMax::RexBoundMtlMax():
	m_Flags(0),
	m_Density(-1),
	mp_MtlDlg(NULL),
	mp_miloRoom(NULL),
	m_ProcCullImmune(false),
	m_pMaterialColour(NULL)
#else
RexBoundMtlMax::RexBoundMtlMax()
#endif // HACK_GTA4
{
	m_CollisionName = "";
	m_ProceduralName = "";
}

bool RexBoundMtlMax::IsSame(const RexBoundMtlMax& that)
{
	if(GetProceduralName() == that.GetProceduralName() &&
		GetReference(0) == const_cast<RexBoundMtlMax&>(that).GetReference(0) &&
		GetPopDensity() == that.GetPopDensity() &&
		GetFlags() == that.GetFlags() &&
		GetCollisionName() == that.GetCollisionName() &&
		GetProcCullImmune() == that.GetProcCullImmune() &&
		GetPopDensity() == that.GetPopDensity())
	{
		return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle RexBoundMtlMax::Clone(RemapDir &remap)
{
	RexBoundMtlMax* p_New = new RexBoundMtlMax;

	BaseClone(this,p_New,remap);

	p_New->m_CollisionName = m_CollisionName;
	p_New->m_ProceduralName = m_ProceduralName;
#if HACK_GTA4
	p_New->m_Flags = m_Flags;
	p_New->m_Density = m_Density;
	p_New->m_ProcCullImmune = m_ProcCullImmune;
	p_New->mp_miloRoom = mp_miloRoom;
#endif // HACK_GTA4
	return p_New;
}

#if HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle RexBoundMtlMax::GetReference(int i)
{
	if(i!=0)
		return NULL;

	return mp_miloRoom;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMax::SetReference(int i, RefTargetHandle rtarg)
{
	if(i != 0) return;
	INode* p_Node = (INode*)rtarg;

	if(p_Node == NULL)
	{
		mp_miloRoom = NULL;
		return;
	}

	Object* pObj = p_Node->GetObjectRef();

	if ( ( pObj->ClassID() == MILOROOM_CLASS_ID ) ) //miloroom
	{
		mp_miloRoom = (INode *)rtarg;
	}
	else
	{
		mp_miloRoom = NULL;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RefResult RexBoundMtlMax::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message)
{
	switch (message) 
	{
	case REFMSG_TARGET_DELETED:
		if(hTarget == mp_miloRoom)
		{
			mp_miloRoom = NULL;

			if(mp_MtlDlg)
				mp_MtlDlg->ClearMiloRoomName();
		}
	}

	return REF_SUCCEED;
}
#endif // HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////////
Color RexBoundMtlMax::GetDiffuse(int mtlNum, BOOL backFace)
{
	if(!m_pMaterialColour)
	{
		unsigned int val = 0;
		const char* p_src = m_CollisionName;

		while(*p_src)
		{
			val += *p_src;
			p_src++;
		}

		val = val % 16;

		Color vals[16];

		vals[0] = Color(1.0f,1.0f,1.0f);
		vals[1] = Color(1.0f,0.0f,0.0f);
		vals[2] = Color(0.0f,1.0f,0.0f);
		vals[3] = Color(0.0f,0.0f,1.0f);
		vals[4] = Color(0.0f,1.0f,1.0f);
		vals[5] = Color(1.0f,0.0f,1.0f);
		vals[6] = Color(1.0f,1.0f,0.0f);
		vals[7] = Color(0.0f,0.0f,0.0f);

		vals[8] = Color(0.7f,0.7f,0.7f);
		vals[9] = Color(0.7f,0.0f,0.0f);
		vals[10] = Color(0.0f,0.7f,0.0f);
		vals[11] = Color(0.0f,0.0f,0.7f);
		vals[12] = Color(0.0f,0.7f,0.7f);
		vals[13] = Color(0.7f,0.0f,0.7f);
		vals[14] = Color(0.7f,0.7f,0.0f);
		vals[15] = Color(0.3f,0.3f,0.3f);

		m_pMaterialColour = new Color(vals[val]);
	}
	return *m_pMaterialColour;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMax::Reset()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
ParamDlg* RexBoundMtlMax::CreateParamDlg(HWND hwMtlEdit, IMtlParams *imp)
{
	m_MtlEditWnd = hwMtlEdit;
	mp_MtlDlg = new RexBoundMtlMaxDlg(this,imp);

	return mp_MtlDlg;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
IOResult RexBoundMtlMax::Load(ILoad *iload)
{
	int ChunkDepth = iload->CurChunkDepth();

	IOResult Res = iload->OpenChunk();

	try
	{
		Res = Mtl::Load(iload);
	}
	catch (...)
	{
		int NewChunkDepth = iload->CurChunkDepth();

		while(NewChunkDepth > ChunkDepth)
		{
			iload->CloseChunk();

			NewChunkDepth--;
		}

		return IO_OK;
	}

	iload->CloseChunk();

	if(Res != IO_OK)
		return Res;

	iload->OpenChunk();

	assert(iload->CurChunkID() == 1);

	char* p_Data;

	s32 DataSize;
	ULONG Read;
	iload->Read(&DataSize,4,&Read);

	if(Read != 4)
		return IO_ERROR;

	p_Data = new char[DataSize];
	iload->Read(p_Data,DataSize,&Read);

	if(Read != DataSize)
	{
		delete[] p_Data;
		return IO_ERROR;
	}

	char Filename[1024];
	fiDeviceMemory DevMemory;
	fiDeviceMemory::MakeMemoryFileName(Filename,1024,p_Data,DataSize,false,"rexmaxboundmtl load buffer");
	fiStream* p_MemStream = fiStream::Create(Filename,DevMemory);

	//version
	s32 Version;
	p_MemStream->ReadInt(&Version,1);
#if HACK_GTA4
	m_Flags = 0;
#endif // HACK_GTA4
	if(Version > 0)
	{
		ReadString(p_MemStream,m_CollisionName);

		if(Version > 1)
		{
			ReadString(p_MemStream,m_ProceduralName);
#if HACK_GTA4
			// Read in the additional bound data
			if(Version > 2)
			{
				p_MemStream->ReadInt(&m_Flags,1);

				if(Version < 5)
				{
					m_Flags = m_Flags & ~(1 << RexBoundMtlMaxDlg::FLAG_DOESNTPROVIDECOVER);
				}

				if(Version > 3)
				{
					p_MemStream->ReadInt(&m_Density,1);
					if(Version > 5)
					{
						int iProcull = 0;
						p_MemStream->ReadInt(&iProcull,1);
						m_ProcCullImmune = iProcull == 1 ? true : false;
					}
				}
				else
				{
					m_Density = GetPopDensityFromName(m_CollisionName);
				}
			}
#endif // HACK_GTA4
		}
	}
	else
	{
		Res = IO_ERROR;
	}

	p_MemStream->Close();
	delete[] p_Data;

	iload->CloseChunk();

	USHORT nextChunkID = iload->PeekNextChunkID();
	if(0!=nextChunkID)
	{
		Res = iload->OpenChunk();
		if(IO_OK!=Res)
			return Res;

		if(REXBOUNDMTL_COLOUR_CHUNKID == iload->CurChunkID())
		{
			Color *pSavedColour = new Color;
			ULONG colorSize = sizeof(Color);
			iload->Read(pSavedColour,colorSize,&Read);
			if(Read != colorSize)
			{
				delete pSavedColour;
				return IO_ERROR;
			}
			m_pMaterialColour = pSavedColour;
		}
		iload->CloseChunk();
	}

	return Res;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
IOResult RexBoundMtlMax::Save(ISave *isave)
{
	//get our data together

	s32 Version = CURRENT_FILE_VERSION;

	s32 DataSize = GetDataSize();
	char* p_Data = new char[DataSize];
	char Filename[1024];
	fiDeviceMemory::MakeMemoryFileName(Filename,1024,p_Data,DataSize,false,"rexmaxboundmtl save buffer");
	fiDeviceMemory DevMemory;

	fiStream* p_MemStream = fiStream::Create(Filename,DevMemory);
	p_MemStream->WriteInt(&Version,1);
	WriteString(p_MemStream,m_CollisionName);
	WriteString(p_MemStream,m_ProceduralName);
#if HACK_GTA4
	p_MemStream->WriteInt(&m_Flags,1);
	p_MemStream->WriteInt(&m_Density,1);
	int iProcCull = m_ProcCullImmune ? 1 : 0;
	p_MemStream->WriteInt(&iProcCull,1);
#endif // HACK_GTA4

	s32 TellSize = p_MemStream->Tell();
	if(TellSize != DataSize)
		return IO_ERROR;

	p_MemStream->Close();

	//write it out

	IOResult Res = IO_OK;

	isave->BeginChunk(0);
	Res = Mtl::Save(isave);
	isave->EndChunk();

	isave->BeginChunk(1);

	ULONG Written;
	isave->Write(&DataSize,4,&Written);

	if(Written != 4)
	{
		Res = IO_ERROR;
	}
	else
	{
		isave->Write(p_Data,DataSize,&Written);
		if(Written != DataSize)
			Res = IO_ERROR;
	}

	delete[] p_Data;

	isave->EndChunk();

	if(m_pMaterialColour)
	{
		isave->BeginChunk(REXBOUNDMTL_COLOUR_CHUNKID);
		isave->Write(m_pMaterialColour,sizeof(Color),&Written);
		isave->EndChunk();
	}
	return Res;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMax::Shade(ShadeContext& sc)
{
	AColor mval(1.0f,1.0f,1.0f,0.5f);

	sc.out.c = sc.DiffuseIllum() * mval;
	sc.out.t = Color(0.0f,0.0f,0.0f);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
#if HACK_GTA4
void RexBoundMtlMax::AddCollisionName(const atString& r_NewCollision,s32 DefPopDensity)
#else
void RexBoundMtlMax::AddCollisionName(const atString& r_NewCollision)
#endif // HACK_GTA4
{
	m_CollisionList.PushAndGrow(r_NewCollision);
#if HACK_GTA4
	// Store the default ped density value
	m_CollPopDensityList.PushAndGrow(DefPopDensity);
#endif // HACK_GTA4
}

#if HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMax::SetCollisionName(const char* p_in) 
{ 
	m_CollisionName = p_in; 
	m_Density = GetPopDensityFromName(m_CollisionName); 

	if(mp_MtlDlg)
		mp_MtlDlg->Refresh();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
int RexBoundMtlMax::GetRoomID() const
{
	if(!mp_miloRoom)
		return NULL;

	INode* p_Parent = mp_miloRoom->GetParentNode();

	if(p_Parent->GetObjectRef()->ClassID() != MILO_CLASS_ID)
		return NULL;

	s32 i,Count = p_Parent->NumberOfChildren();
	s32 MiloRoomCount = 0;

	for(i=0;i<Count;i++)
	{
		INode* p_Check = p_Parent->GetChildNode(i);

		if(p_Check->GetObjectRef()->ClassID() == MILOROOM_CLASS_ID)
			MiloRoomCount++;

		if(p_Check == mp_miloRoom)
			return MiloRoomCount;
	}

	return 0;
}

#endif // HACK_GTA4

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMax::AddProceduralName(const atString& r_NewProcedural)
{
	m_ProceduralList.PushAndGrow(r_NewProcedural);
}

#if HACK_GTA4
// Get the default ped density for a material give it's name
//////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 RexBoundMtlMax::GetPopDensityFromName(const atString& r_Procedural)
{
	s32 i,Count = m_CollisionList.GetCount();

	for(i=0;i<Count;i++)
	{
		if(stricmp(r_Procedural,m_CollisionList[i]) == 0)
		{
			return m_CollPopDensityList[i];
		}
	}

	return 0;
}
#endif // HACK_GTA4
//////////////////////////////////////////////////////////////////////////////////////////////////////////
s32 RexBoundMtlMax::GetDataSize()
{
	s32 Size = 4;

	Size += m_CollisionName.GetLength() + 4;
	Size += m_ProceduralName.GetLength() + 4;
#if HACK_GTA4
	// Required for material flags and ped density
	Size += 4; // Material flags
	Size += 4; // Ped density
	Size += 4; // ProCullImmune
#endif // HACK_GTA4

	return Size;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMax::WriteString(fiStream* p_Save,const atString& r_Write)
{
	s32 WriteSize = r_Write.GetLength();
	p_Save->WriteInt(&WriteSize,1);
	p_Save->Write(r_Write,WriteSize);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexBoundMtlMax::ReadString(fiStream* p_Load,atString& r_Read)
{
	s32 ReadSize;
	p_Load->ReadInt(&ReadSize,1);
	char* p_Buffer = new char[ReadSize + 1];
	p_Buffer[ReadSize] = '\0';
	p_Load->Read(p_Buffer,ReadSize);
	r_Read = atString(p_Buffer);
	delete[] p_Buffer;
}
