// /IRageBoundMaterial.h

#ifndef __IRAGEBOUNDMATERIAL_H__
#define __IRAGEBOUNDMATERIAL_H__

#define RAGEBOUNDMTL_INTERFACE Interface_ID (0x39a029c0, 0xb534533)

#define GetRageBoundMtlInterface(obj) \
	((IRageBoundMaterial*)obj->GetInterface(RAGEBOUNDMTL_INTERFACE))

#pragma warning(push)
#pragma warning(disable:4265)
#pragma warning(disable:4100)

enum
{
	rbm_FpGetCollisionName, rbm_FpSetCollisionName, rbm_FpGetProceduralName, rbm_FpSetProceduralName
};

class IRageBoundMaterial : public FPMixinInterface
{
	BEGIN_FUNCTION_MAP
		FN_0( rbm_FpGetCollisionName, TYPE_TSTR_BV, FpGetCollisionName);
		VFN_1( rbm_FpSetCollisionName, FpSetCollisionName, TYPE_TSTR_BV);
		FN_0( rbm_FpGetProceduralName, TYPE_TSTR_BV, FpGetProceduralName);
		VFN_1( rbm_FpSetProceduralName, FpSetProceduralName, TYPE_TSTR_BV);
	END_FUNCTION_MAP

	FPInterfaceDesc* GetDesc();
	
public:
	virtual TSTR	FpGetCollisionName() const = 0;
	virtual void	FpSetCollisionName(TSTR colName) = 0;
	virtual TSTR	FpGetProceduralName() const = 0;
	virtual void	FpSetProceduralName(TSTR procName) = 0;
};

#pragma warning(pop)

#endif //__IRAGEBOUNDMATERIAL_H__