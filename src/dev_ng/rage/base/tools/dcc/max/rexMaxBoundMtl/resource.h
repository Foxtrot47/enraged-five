//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by rexMaxBoundMtl.rc
//
#define IDD_DLG_MATBNDEDIT              101
#define IDS_INTERN_REF_PICK_MODE        102
#define IDC_COMBO1                      1002
#define IDC_CBO_NAME                    1002
#define IDC_COMBO2                      1003
#define IDC_CBO_PROCEDURAL              1003
#define IDC_CHK_STAIRS                  1004
#define IDC_CHK_CLIMABLE                1005
#define IDC_CHK_NONCLIMABLE             1005
#define IDC_CHK_SEETHROUGH              1006
#define IDC_CHK_SHOOTTHROUGH            1007
#define IDC_CHK_WET                     1008
#define IDC_CHK_SPARE1                  1009
#define IDC_CHK_SPARE2                  1010
#define IDC_CHK_SHOOT_THROUGH_FX        1011
#define IDC_SLIDER1                     1012
#define IDC_SPN_PEDMULTIPLIER           1014
#define IDC_CUSTOM2                     1015
#define IDC_EDT_PEDMULTIPLIER           1015
#define IDC_BTN_MILOROOM                1017
#define IDC_CUSTOM3                     1019
#define IDC_BTN_RESET                   1019
#define IDC_CHK_NONAVMESH               1020
#define IDC_CHK_NODECAL                 1021
#define IDC_CHK_NO                      1022
#define IDC_CHK_NORAGDOLL               1022
#define IDC_CHK_NORAGDOLL2              1023
#define IDC_CHK_PROCCULLIMMUNE          1023
#define IDC_CHK_NOPTFX                  1024
#define IDC_CHK_TOO_STEEP               1025
#define IDC_CHK_NOPTFX2                 1026
#define IDC_CHK_NONETSPAWN              1026
#define IDC_CHK_NONCAMERACOLLIDABLEALLOWCLIPPING 1027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
