#ifndef __REX_MAX_BOUND_DIALOGMTL_H__
#define __REX_MAX_BOUND_DIALOGMTL_H__

#include "boundmtl.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////////////////////////////////////
class RexBoundMtlMaxDlg : public ParamDlg
{
public:
#if HACK_GTA4

	enum
	{
		FLAG_ISSTAIRS,
		FLAG_ISNONCLIMABLE,
		FLAG_SEETHROUGH,
		FLAG_SHOOTTHROUGH,
		FLAG_DOESNTPROVIDECOVER,
		FLAG_PATH,
		FLAG_NOCAMCOLL,
		FLAG_SHOOT_THROUGH_FX,
		FLAG_NODECAL,
		FLAG_NONAVMESH,
		FLAG_NORAGDOLL,
		FLAG_RESERVEDFORTOOLS,
		FLAG_NOPTFX,
		FLAG_TOOSTEEP,
		FLAG_NONETSPAWN,
		FLAG_NOCAMCOLLALLOWCLIP,
		FLAG_MAXBOUNDMTLFLAGS
	};

	//NOTE: If you are adding, removing or reordering this enumeration
	//ensure that it matches up with the contents of gtaMaterial.h
	//and the below array of names.
	static char* RexBoundFlagNames[FLAG_MAXBOUNDMTLFLAGS];

#endif // HACK_GTA4
	friend class RexBoundMtlMax;

	RexBoundMtlMaxDlg(RexBoundMtlMax* p_Mtl,IMtlParams* p_MtlParams);
	virtual ~RexBoundMtlMaxDlg();

#if HACK_GTA4
	void Refresh();
#endif // HACK_GTA4

	// from ParamDlg
	virtual Class_ID ClassID() { return REXBND_MATERIAL_CLASS_ID; }
	virtual void SetThing(ReferenceTarget *m);
	virtual ReferenceTarget* GetThing() { return mp_Mtl; }
	virtual void SetTime(TimeValue t) {}
	virtual	void ReloadDialog() {}
	virtual void DeleteThis() { delete this; }
	virtual void ActivateDlg(BOOL onOff) {}
	//virtual int FindSubTexFromHWND(HWND hwnd);

	static INT_PTR CALLBACK RexBoundMtlMaxProc(HWND DlgWnd,UINT msg,WPARAM wParam,LPARAM lParam);
#if HACK_GTA4
	ICustButton* GetMiloButton() { return mp_MiloButton; }
	IMtlParams* GetMtlParams() { return mp_MtlParams; }
	bool IsPicking() { return m_Picking; }
	void SetPicking(bool Picking) { m_Picking = Picking; }
	void ClearMiloRoomName();
	void ClearMiloRoom();
#endif // HACK_GTA4

protected:
	void Reset();
	void SetupUI();
	void SetupShader(const atString& r_ShaderName,bool IsPreset);
	void UpdateMapName(s32 Index,const atString& r_MapName);

	void OnInitDialog(HWND DlgWnd);
#if HACK_GTA4
	void OnDestroy();
#endif // HACK_GTA4
	void OnUpdateCollisionName();
	void OnSelChangeCollisionName();
	void OnCollisionNameSetFocus();
	void OnCollisionNameKillFocus();
	void OnUpdatePedDensity();

	void OnUpdateProceduralName();
	void OnSelChangeProceduralName();
	void OnProceduralNameSetFocus();
	void OnProceduralNameKillFocus();
#if HACK_GTA4
	void OnChkStairs();
	void OnChkClimable();
	void OnChkSeeThrough();
	void OnChkShootThrough();
	void OnChkWet();
	void OnChkPath();
	void OnChkNoCamColl();
	void OnChkShootThroughFX();
	void OnChkNoDecal();
	void OnChkNoNavmesh();
	void OnChkNoRagdoll();
	void OnChkProcCullImmune();
	void OnChkNoParticleEffects();
	void OnChkToSteepForPed();
	void OnChkNoNetworkSpawn();
	void OnChkNoCamCollAllowClip();
#endif // HACK_GTA4
	HWND m_Wnd;
	RexBoundMtlMax* mp_Mtl;
	IMtlParams* mp_MtlParams;
#if HACK_GTA4
	CommandMode *previousMode;
	//IMtlParams* mp_MtlParams;
	ISpinnerControl* mp_PedDensitySpin;
	ICustEdit* mp_PedDensityEdit;
	ICustButton* mp_MiloButton;
	bool m_Picking;
#endif // HACK_GTA4
};

} // namespace rage {

#endif //__REX_MAX_BOUND_DIALOGMTL_H__
