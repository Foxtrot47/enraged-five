Project rexMaxBoundMtl
Template max_configurations.xml

OutputFileName rexBoundMtl.dlm
ConfigurationType dll

Define RSTMAXMTL_DLL
Define USING_RAGE

EmbeddedLibAll comctl32.lib acap.lib biped.lib bmm.lib composite.lib core.lib crowd.lib CustDlg.lib edmodel.lib expr.lib flt.lib geom.lib gfx.lib gup.lib IGame.lib imageViewers.lib ManipSys.lib maxnet.lib Maxscrpt.lib maxutil.lib MenuMan.lib menus.lib mesh.lib MNMath.lib Paramblk2.lib particle.lib ParticleFlow.lib physique.lib Poly.lib RenderUtil.lib rtmax.lib SpringSys.lib tessint.lib viewfile.lib zlibdll.lib

ForceInclude ForceInclude.h

IncludePath $(RAGE_DIR)\stlport\STLport-5.0RC5\src
IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\tools\dcc\max
IncludePath $(RAGE_DIR)\base\tools\dcc\libs
IncludePath $(RAGE_DIR)\base\tools\libs

Files {
	BoundMtl.cpp
	BoundMtl.cpp
	DialogMtl.cpp
	DialogMtl.h
	Entry.cpp
	ForceInclude.h
	IRageBoundMaterial.h
	Resource.h
	RexMaxBoundMtl.def
	RexMaxBoundMtl.rc
	Script.cpp
}