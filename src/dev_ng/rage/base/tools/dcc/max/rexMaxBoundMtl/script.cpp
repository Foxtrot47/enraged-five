#include "atl/string.h"
#include "atl/array.h"
#include "boundmtl.h"

using namespace rage;

#include <maxscript/maxscript.h>
#include <maxscript/foundation/3dmath.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/foundation/Numbers.h>
#include <maxscript/foundation/Name.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last

#include <string>
#include "dialogMtl.h"

def_visible_primitive(RexGetCollisionName,"RexGetCollisionName");
def_visible_primitive(RexSetCollisionName,"RexSetCollisionName");
def_visible_primitive(RexAddCollisionOption,"RexAddCollisionOption");

def_visible_primitive(RexGetProceduralName,"RexGetProceduralName");
def_visible_primitive(RexSetProceduralName,"RexSetProceduralName");
def_visible_primitive(RexAddProceduralOption,"RexAddProceduralOption");

#if HACK_GTA4
def_visible_primitive(RexGetRoomNode,"RexGetRoomNode");
def_visible_primitive(RexSetRoomNode,"RexSetRoomNode");

def_visible_primitive(RexIsCollisionEqual,"RexIsCollisionEqual");

def_visible_primitive(RexGetCollisionFlags,"RexGetCollisionFlags");
def_visible_primitive(RexSetCollisionFlags,"RexSetCollisionFlags");

def_visible_primitive(RexGetCollisionFlagBit, "RexGetCollisionFlagBit");
def_visible_primitive(RexGetCollisionFlagNames, "RexGetCollisionFlagNames");

def_visible_primitive(RexSetPopDensity,"RexSetPopDensity");
def_visible_primitive(RexGetPopDensity,"RexGetPopDensity");

def_visible_primitive(RexGetProceduralList, "RexGetProceduralList");
def_visible_primitive(RexGetCollisionList, "RexGetCollisionList");

def_visible_primitive(RexClearProceduralList, "RexClearProceduralList");
def_visible_primitive(RexClearCollisionList, "RexClearCollisionList");

def_visible_primitive(RexGetProcCullImmune,"RexGetProcCullImmune");
def_visible_primitive(RexSetProcCullImmune,"RexSetProcCullImmune");

#endif // HACK_GTA4

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexGetCollisionName_cf(Value** arg_list, int count)
{
	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(!p_MtlBase || p_MtlBase->ClassID() != REXBND_MATERIAL_CLASS_ID)
	{
		return &undefined;
	}

	RexBoundMtlMax* p_MtlMax = (RexBoundMtlMax*)p_MtlBase;

	char Buffer[1024];
	strcpy(Buffer,p_MtlMax->GetCollisionName());

	return_protected(new String(Buffer));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexSetCollisionName_cf(Value** arg_list, int count)
{
	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	char* p_name = arg_list[1]->to_string();

	if(!p_MtlBase || p_MtlBase->ClassID() != REXBND_MATERIAL_CLASS_ID)
	{
		return &undefined;
	}

	RexBoundMtlMax* p_MtlMax = (RexBoundMtlMax*)p_MtlBase;

	p_MtlMax->SetCollisionName(p_name);

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexAddCollisionOption_cf(Value** arg_list, int count)
{
#if HACK_GTA4
	// We need to set the default ped density value as well
	RexBoundMtlMax::AddCollisionName(atString(arg_list[0]->to_string()),arg_list[1]->to_int());
#else 
	RexBoundMtlMax::AddCollisionName(atString(arg_list[0]->to_string()));
#endif // HACK_GTA4

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexGetProceduralName_cf(Value** arg_list, int count)
{
	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();

	if(!p_MtlBase || p_MtlBase->ClassID() != REXBND_MATERIAL_CLASS_ID)
	{
		return &undefined;
	}

	RexBoundMtlMax* p_MtlMax = (RexBoundMtlMax*)p_MtlBase;

	char Buffer[1024];
	strcpy(Buffer,p_MtlMax->GetProceduralName());

	return_protected(new String(Buffer));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexSetProceduralName_cf(Value** arg_list, int count)
{
	MtlBase* p_MtlBase = arg_list[0]->to_mtlbase();
	char* p_name = arg_list[1]->to_string();

	if(!p_MtlBase || p_MtlBase->ClassID() != REXBND_MATERIAL_CLASS_ID)
	{
		return &undefined;
	}

	RexBoundMtlMax* p_MtlMax = (RexBoundMtlMax*)p_MtlBase;

	p_MtlMax->SetProceduralName(p_name);

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexAddProceduralOption_cf(Value** arg_list, int count)
{
	RexBoundMtlMax::AddProceduralName(atString(arg_list[0]->to_string()));

	return &true_value;
}
#if HACK_GTA4
// All the new get/set functions for the new bound material parameters
//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexGetRoomNode_cf(Value** arg_list, int count)
{
	Mtl* p_mtl = arg_list[0]->to_mtl();

	if(p_mtl->ClassID() != REXBND_MATERIAL_CLASS_ID)
		return &undefined;

	RexBoundMtlMax* p_bndMtl = (RexBoundMtlMax*)p_mtl;

	return_protected(new MAXNode((INode*)(p_bndMtl->GetReference(0))));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexSetRoomNode_cf(Value** arg_list, int count)
{
	Mtl* p_mtl = arg_list[0]->to_mtl();
	INode* p_node = arg_list[1]->to_node();

	if(p_mtl->ClassID() != REXBND_MATERIAL_CLASS_ID)
		return &false_value;

	RexBoundMtlMax* p_bndMtl = (RexBoundMtlMax*)p_mtl;

	p_bndMtl->SetReference(0,p_node);

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexGetProcCullImmune_cf(Value** arg_list, int count)
{
	Mtl* p_mtl = arg_list[0]->to_mtl();

	if(p_mtl->ClassID() != REXBND_MATERIAL_CLASS_ID)
		return &undefined;

	RexBoundMtlMax* p_bndMtl = (RexBoundMtlMax*)p_mtl;

	if(p_bndMtl->GetProcCullImmune())
	{
		return &true_value;
	}

	return &false_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexSetProcCullImmune_cf(Value** arg_list, int count)
{
	Mtl* p_mtl = arg_list[0]->to_mtl();

	if(p_mtl->ClassID() != REXBND_MATERIAL_CLASS_ID)
		return &undefined;

	RexBoundMtlMax* p_bndMtl = (RexBoundMtlMax*)p_mtl;

	p_bndMtl->SetProcCullImmune(arg_list[1]->to_bool());

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexGetCollisionFlags_cf(Value** arg_list, int count)
{
	Mtl* p_mtl = arg_list[0]->to_mtl();

	if(p_mtl->ClassID() != REXBND_MATERIAL_CLASS_ID)
		return &undefined;

	RexBoundMtlMax* p_bndMtl = (RexBoundMtlMax*)p_mtl;

	return_protected(Integer::intern((unsigned int)p_bndMtl->GetFlags()));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexSetCollisionFlags_cf(Value** arg_list, int count)
{
	Mtl* p_mtl = arg_list[0]->to_mtl();

	if(p_mtl->ClassID() != REXBND_MATERIAL_CLASS_ID)
		return &undefined;

	RexBoundMtlMax* p_bndMtl = (RexBoundMtlMax*)p_mtl;

	p_bndMtl->SetFlags(arg_list[1]->to_int());

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexGetCollisionFlagBit_cf(Value** arg_list, int count)
{
	char* flagName = arg_list[0]->to_string();
	
	for(int index = 0; index < RexBoundMtlMaxDlg::FLAG_MAXBOUNDMTLFLAGS; ++index)
	{
		if ( stricmp(RexBoundMtlMaxDlg::RexBoundFlagNames[index], flagName) == 0 )
			return_protected(Integer::intern(index));
	}

	return &undefined;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexGetCollisionFlagNames_cf(Value** arg_list, int count)
{
	Array* p_Ret = new Array( RexBoundMtlMaxDlg::FLAG_MAXBOUNDMTLFLAGS );
	for(int index = 0; index < RexBoundMtlMaxDlg::FLAG_MAXBOUNDMTLFLAGS; ++index)
	{
		p_Ret->append(new String(RexBoundMtlMaxDlg::RexBoundFlagNames[index]));
	}

	return p_Ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexSetPopDensity_cf(Value** arg_list, int count)
{
	Mtl* p_mtl = arg_list[0]->to_mtl();

	if(p_mtl->ClassID() != REXBND_MATERIAL_CLASS_ID)
		return &undefined;

	RexBoundMtlMax* p_bndMtl = (RexBoundMtlMax*)p_mtl;

	p_bndMtl->SetPopDensity(arg_list[1]->to_int());

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexGetPopDensity_cf(Value** arg_list, int count)
{
	Mtl* p_mtl = arg_list[0]->to_mtl();

	if(p_mtl->ClassID() != REXBND_MATERIAL_CLASS_ID)
		return &undefined;

	RexBoundMtlMax* p_bndMtl = (RexBoundMtlMax*)p_mtl;

	return_protected(Integer::intern((unsigned int)p_bndMtl->GetPopDensity()));
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexIsCollisionEqual_cf(Value** arg_list, int count)
{
	MtlBase* p_MtlA = arg_list[0]->to_mtlbase();
	MtlBase* p_MtlB = arg_list[1]->to_mtlbase();

	if(!p_MtlA || p_MtlA->ClassID() != REXBND_MATERIAL_CLASS_ID)
		return &false_value;

	if(!p_MtlB || p_MtlB->ClassID() != REXBND_MATERIAL_CLASS_ID)
		return &false_value;

	if(p_MtlA == p_MtlB)
		return &true_value;

	RexBoundMtlMax* p_MtlMaxA = (RexBoundMtlMax*)p_MtlA;
	RexBoundMtlMax* p_MtlMaxB = (RexBoundMtlMax*)p_MtlB;

	if(stricmp(p_MtlMaxA->GetCollisionName(),p_MtlMaxB->GetCollisionName()) != 0)
		return &false_value;

	if(stricmp(p_MtlMaxA->GetProceduralName(),p_MtlMaxB->GetProceduralName()) != 0)
		return &false_value;

	if(p_MtlMaxA->GetFlags() != p_MtlMaxB->GetFlags())
		return &false_value;

	if(p_MtlMaxA->GetPopDensity() != p_MtlMaxB->GetPopDensity())
		return &false_value;

	if(p_MtlMaxA->GetReference(0) != p_MtlMaxB->GetReference(0))
		return &false_value;

	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexGetProceduralList_cf(Value** arg_list, int count)
{
	atArray<atString>& procList = RexBoundMtlMax::GetProceduralList();
	Array* p_Ret = new Array( procList.GetCount() );
	for(int i=0;i<procList.GetCount();i++)
	{
		p_Ret->append(new String((char*)((const char*)procList[i].c_str())));
	}
	return p_Ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexGetCollisionList_cf(Value** arg_list, int count)
{
	atArray<atString>& collList = RexBoundMtlMax::GetCollisionList();
	Array* p_Ret = new Array( collList.GetCount() );
	for(int i=0;i<collList.GetCount();i++)
	{
		p_Ret->append(new String((char*)((const char*)collList[i].c_str())));
	}
	return p_Ret;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexClearProceduralList_cf(Value** arg_list, int count)
{
	RexBoundMtlMax::GetProceduralList().clear();
	
	return &true_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
Value* RexClearCollisionList_cf(Value** arg_list, int count)
{
	RexBoundMtlMax::GetCollisionList().clear();

	return &true_value;
}

#endif // HACK_GTA4
