#ifndef __REX_MAXROCKSTAR_GRAPH_H__
#define __REX_MAXROCKSTAR_GRAPH_H__

#include "rexMax/wrapper.h"

namespace rage {

/* PURPOSE:
	register the correct modules for a max to rage export
*/
class RexMaxRageWrapper : public RexMaxWrapper
{
public:
	RexMaxRageWrapper();

	virtual	void RegisterModules(rexShellMax& r_Shell);
};

} // namespace rage {

#endif //__REX_MAXROCKSTAR_GRAPH_H__
