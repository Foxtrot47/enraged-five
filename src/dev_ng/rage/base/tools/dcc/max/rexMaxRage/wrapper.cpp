#include "rexMaxRage/wrapper.h"
#include "rexmaxtogeneric/filterMeshHierarchy.h"
#include "rexmaxtogeneric/filterBoneRoot.h"
#include "rexmaxtogeneric/filterBoundHierarchy.h"
#include "rexmaxtogeneric/filterLocator.h"
#include "rexmaxtogeneric/filterParticle.h"

#include "rexmaxtogeneric/converterDrawableMesh.h"
#include "rexmaxtogeneric/converterBoundHierarchy.h"
#include "rexmaxtogeneric/converterEntity.h"
#include "rexmaxtogeneric/converterSkeleton.h"
#include "rexmaxtogeneric/converterAnimation.h"
#include "rexmaxtogeneric/converterFragment.h"
#include "rexmaxtogeneric/converterEdgeModel.h"
#include "rexmaxtogeneric/converterLocator.h"
#include "rexmaxtogeneric/converterParticle.h"
#include "rexmaxtogeneric/converterCloth.h"
#include "rexmaxtogeneric/processorMeshCombiner.h"

#include "rexmaxtogeneric/propertyBasic.h"

#include "rexmaxutility/utility.h"

#include "rexgeneric/processorMeshCombiner.h"

#include "rexrage/serializerMesh.h"
#include "rexrage/serializerEntity.h"
#include "rexrage/serializerSkeleton.h"
#include "rexrage/serializerAnimation.h"
#include "rexrage/serializerBound.h"
#include "rexRAGE/serializerEdgeModel.h"
#include "rexrage/serializerParticle.h"
#include "rexBase/animExportCtrl.h"

using namespace rage;

static RexMaxRageWrapper g_RexMaxRageWrapper;
RexMaxWrapper& rage::g_Wrapper = g_RexMaxRageWrapper;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
RexMaxRageWrapper::RexMaxRageWrapper()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void RexMaxRageWrapper::RegisterModules(rexShellMax& r_Shell)
{
	INIT_PARSER;
	rage::AnimExportCtrlSpec::RegisterParsableClasses();

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//global options
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	//rexUtility::SetTextureExportWantedFunction(rexMaxUtility::TextureExportNeeded);
	rexUtility::SetExportTextureFunction((rage::rexUtilityTextureExportFunction)rexMaxUtility::ExportTexture);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//filter declarations
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	rexFilterMaxMeshHierarchy filterMeshHierarchy;
	rexFilterMaxBoneRoot filterBoneRoot;
	rexFilterMaxBoundHierarchy filterBoundHierarchy;
	rexFilterMaxLocator filterLocator;
	rexFilterMaxParticle filterParticle;
	rexFilterCloth filterCloth;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//converter declarations
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	rexConverterMaxDrawableMeshHierarchy converterMeshHierarchy;
	rexConverterMaxBoundHierarchy converterBoundHierarchy;
	rexConverterMaxEntity converterEntity;
	rexConverterMaxSkeleton converterSkeleton;
	rexConverterMaxAnimation converterAnimation;
	rexConverterMaxFragment converterFragment;
	rexConverterMaxEdgeModelHierarchy converterEdgeModel;
	rexConverterMaxLocator converterLocator;
	rexConverterMaxParticle converterParticle;

	rexConverterMaxEnvCloth converterEnvCloth;
	rexConverterMaxCharCloth converterCharCloth;
	rexConverterMaxClothPiece converterClothPiece;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//processor declarations
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	rexProcessorMeshCombiner processorMeshCombiner;
	processorMeshCombiner.SetCombineAcrossBones(false);
	
	//experimental collision combiner...
	rexProcessorMaxBoundCombiner processorBoundCombiner;
	processorBoundCombiner.SetBoundMode(true);
	processorBoundCombiner.SetCombineAcrossBones(false);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//serialiser declarations
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	rexSerializerRAGEMesh serializerMesh;
	serializerMesh.SetUseBoneIDs(true);
	rexSerializerRAGEPhysicsBound serializerBound;
	serializerBound.SetUseBoneIDs(true);
	rexSerializerRAGEEntity serializerEntity;
	serializerEntity.SetUseBoneIDs(true);
	//serializerEntity.SetFragmentWriteMeshes(true);
	rexSerializerRAGESkeleton serializerSkeleton;
	serializerSkeleton.SetUseBoneIDs(true);
	rexSerializerRAGEAnimation serializerAnimation;
	serializerAnimation.SetUseBoneIDs(true);
	rexSerializerRAGEEdgeModel serializerEdgeModel;
	rexSerializerRAGEParticle serializerParticle;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//properties declarations
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	//basic properties applicable to all modules
	rexPropertyMaxInputNodes								propertyInputNodes;
	rexPropertyMaxName										propertyName;
	rexPropertyMaxOutputPath								propertyOutputPath;
	rexPropertyMaxExportData								propertyExportData;

	//drawable mesh properties
	rexPropertyTextureOutputPath							propertyTextureOutputPath;
	rexPropertyMeshSetExportTextures						propertyMeshSetExportTextures;
	rexPropertyMeshSetExportEnityTextureList				propertyMeshSetExportEntityTextureList;
	rexPropertyMeshEntityTextureListKeepExtensions			propertyMeshEntityTextureListKeepExtensions;
	rexPropertyMeshCombineMaxPolyCountToCombine				propertyMeshMaxPolyCountToCombine;
	rexPropertyMeshCombineMaxPolyCountOfCombinedMesh		propertyMeshMaxPolyCountOfCombinedMesh;
	rexPropertyMeshCombineMaxDistance						propertyMeshCombineMaxDistance;
	rexPropertyMeshCombineMaxLODThresholdRatio				propertyMeshCombineMaxLODThresholdRatio;
	rexPropertyMeshForSkeleton								propertyMeshForSkeleton;
	rexPropertyMeshAsAscii									propertyMeshAsAscii;
	rexPropertyMeshSkinOffset								propertyMeshSkinOffset;
	rexPropertyMeshExportWorldSpace							propertyMeshExportWorldSpace;
	rexPropertyCullVertexChannels							propertyCullVertexChannels;
	rexPropertyForcedAdditionalUVCount						propertyForcedAdditionalUVCount;
	rexPropertyMeshSetExportEnityTextureList				propertyMeshExportTextureList;
	rexPropertyLimitToSkelSpecFileBones						propertyLimitToSkelSpecFileBones;
	rexPropertyUseWeightedNormals							propertyUseWeightedNormals;
	rexPropertyMeshOptimiseMaterials						propertyMeshOptimiseMaterials;
	rexPropertyMeshBlindVertexDataChannels					propertyMeshBlindVertexDataChannels;

	// For rules for custom vertex colours.  Not fully fleshed out, see rexMaxUtility/utility.cpp ConvertMaxMeshNodeIntoGenericMesh()
	// once Ive checked in HACK_GTA4 wrapped version.  This is one of the areas where there are significant changes to the head of RAGE
	rexPropertyMeshCustomVertexColour						propertyMeshCustomVertexColour;
	rexPropertyMeshGenerateTintIndices						propertyMeshGenerateTintIndices;
	rexPropertyMeshRemoveDegenerates						propertyMeshRemoveDegenerates;
	rexPropertyMeshWeldWithoutQuantize						propertyMeshWeldWithoutQuantize;
	rexPropertyMeshWeld										propertyMeshWeld;
	rexPropertyMeshSplitBlendsIntoUniqueMeshes				propertyMeshSplitBlendsIntoUniqueMeshes;
	rexPropertyMeshMicroMorphs								propertyMeshMicroMorphs;
	rexPropertyExpectedVertexCount							propertyExpectedVertexCount;

	rexPropertyMeshCombineAcrossBones						propertyMeshCombineAcrossBones;
	rexPropertyMeshCombineMergeMaterials					propertyMeshCombineMergeMaterials;

	//bounds properties
//	rexPropertyBoundQuadrify								propertyBoundQuadrify;
	rexPropertyBoundExportAsComposite						propertyBoundExportAsComposite;
	rexPropertyBoundForceExportAsComposite					propertyBoundForceExportAsComposite;
	rexPropertyBoundExportWorldSpace						propertyBoundExportWorldSpace;
	rexPropertyBoundSurfaceType								propertyBoundSurfaceType;
	rexPropertyBoundOctreeMaxHeight							propertyBoundOctreeMaxHeight;
	rexPropertyBoundOctreeMaxPerCell						propertyBoundOctreeMaxPerCell;
	rexPropertyBoundUseFiltering							propertyBoundUseFiltering;
	rexPropertyMaxBoundZeroPrimitiveCentroids				propertyMaxBoundZeroPrimitiveCentroids;
	rexPropertyBoundSetToNodeName							propertyBoundSetToNodeName;
	rexPropertyMaxBoundMaxPolyCountToCombine				propertyMaxBoundMaxPolyCountToCombine;
	rexPropertyApplyBulletShrinkMargin						propertyApplyBulletShrinkMargin;
	rexPropertySplitOnMaterialLimit							propertySplitOnMaterialLimit;
	rexPropertyBoundCheckFlags								propertyBoundCheckFlags;
	rexPropertyMaxBoundSetGeometryBoundsExportAsBvhGeom		propertyBoundSetGeometryBoundsExportAsBvhGeom;
	rexPropertyMaxBoundSetGeometryBoundsExportAsBvhGrid		propertyBoundSetGeometryBoundsExportAsBvhGrid;
	rexPropertyBoundExportPrimitivesInBvh					propertyBoundExportPrimsInBvh;

	// The second surface properties required for second surfaces on bounds
	rexPropertyBoundHasSecondSurface						propertyBoundHasSecondSurface;
	rexPropertyBoundSecondSurfaceMaxHeight					propertyBoundSecondSurfaceMaxHeight;
	rexPropertyBoundForceComposite							propertyBoundForceComposite;
	rexPropertyBoundExportObjectSpace						propertyBoundExportObjectSpace;
	rexPropertyBoundExportMiloSpace							propertyBoundExportMiloSpace;
	// Ground painting colour map														
	rexPropertyBoundHasMeshTint								propertyBoundHasMeshTint;
	rexPropertyBoundGenerateMaterials						propertyBoundGenerateMaterials;
	rexPropertyBoundDisableOptimisation						propertyBoundDisableOptimisation;
	rexPropertyBoundExportPerVertAttributes					propertyBoundExportPerVertAttributes;

	//entity properties
	rexPropertyMaxEntityTypeFileName						propertyEntityTypeFileName;
	rexPropertyMaxEntitySkipBoundSectionOfTypeFile			propertyEntitySkipBoundSectionOfTypeFile;
	rexPropertyMaxEntityWriteBoundSituations				propertyMaxEntityWriteBoundSituations;
	rexPropertyEntityBoundListName							propertyEntityBoundListName;
	rexPropertyEntityFragmentWriteMeshes					propertyEntityFragmentWriteMeshes;
	rexPropertyEntityGroupBoundsByBone						propertyEntityGroupBoundsByBone;
	rexPropertyEntityAllowEmptyFragSection					propertyEntityAllowEmptyFragSection;
	rexPropertyEntityWriteShadingGroup						propertyEntityWriteShadingGroup;
	rexPropertyEntitySplitFlagGroupsToDrawables				propertyEntitySplitFlagGroupsToDrawables;
	rexPropertyEntitySplitLODGroupsByFlags					propertyEntitySplitLODGroupsByFlags;

	// Set for all vehicles.  Adds "forceLoadCommonDrawable 1" added to the top of fragments section
	// of the entity.type file.  This has to do with how we have a common drawable (main skinned chassis) for
	// each vehicle as well as instanced mesh components such as wheels, chopper blades etc 
	rexPropertyFragmentForceLoadCommonDrawable				propertyFragmentForceLoadCommonDrawable;

	rexPropertyErrorOnClothWithoutVertexTuning				propertyErrorOnClothWithoutVertexTuning;
	//
	rexPropertyMaxExportCtrlFile							propertyExportCtrlFile;
	rexPropertyMaxChannelsToWrite							propertyChannelsToWrite;

	//animation properties

	rexPropertyMaxAnimationCompressionTolerance				propertyMaxAnimationCompressionTolerance;
	rexPropertyMaxAnimationMoverNode						propertyAnimMover;
	rexPropertyAnimMoverTrack								propertyAnimMoverTrack;
	rexPropertyAnimExtraMoverTrack							propertyAnimExtraMoverTrack;
	rexPropertyAnimExportSelected							propertyAnimExportSelected;
	rexPropertyMaxAnimationMoveRotToRoot					propertyMaxAnimationMoveRotToRoot;
	rexPropertyMaxAnimationAuthoredOrient					propertyMaxAnimationAuthoredOrient;
	rexPropertyMaxAnimationProjectFlags						propertyMaxAnimationProjectFlags;
	rexPropertyMaxAnimationLocalSpace						propertyMaxAnimationLocalSpace;
	rexPropertyMaxAnimationUvAnimIndex						propertyMaxAnimationUvAnimIndex;
	rexPropertyMaxAnimationMorphWeightTracks				propertyMaxAnimationMorphWeightTracks;
	rexPropertyAnimationMaximumBlockSize					propertyAnimationMaximumBlockSize;
	rexPropertyAnimationExprControlNodes					propertyAnimationExprControlNodes;
	rexPropertyAnimationExprControlNodesFile				propertyAnimationExprControlNodesFile;
	rexPropertyMaxAnimationEndFrame							propertyMaxAnimationEndFrame;
	rexPropertyMaxAnimationStartFrame						propertyMaxAnimationStartFrame;

	//skeleton properties
	rexPropertySkeletonRootAtOrigin							propertySkeletonRootAtOrigin;
	rexPropertySkeletonWriteLimitAndLockInfo				propertySkeletonWriteLimitAndLockInfo;
	rexPropertySkeletonOmitMissingTracks					propertySkeletonOmitMissingTracks;
	rexPropertySkeletonUseBoneIDs							propertySkeletonUseBoneIDs;
	rexPropertySkeletonBoneTypes							propertySkeletonBoneTypes;
	rexPropertySkeletonAuthoredOrient						propertySkeletonAuthoredOrient;
	rexPropertySkeletonFlipConstraints						propertySkeletonFlipConstraints;
	rexPropertySkeletonLimitToSkinBones						propertySkeletonLimitToSkinBones;

	//fragment properties
	rexPropertyMaxFragmentNewStyle							propertyMaxFragmentNewStyle;
	rexPropertyMaxFragmentBreakableGlassNode				propertyMaxFragmentBreakableGlassNode;

	//cloth properties
	rexPropertyClothMesh									propertyClothMesh;
	rexPropertyClothBounds									propertyClothBounds;


	//module registration
	r_Shell.RegisterModule("Entity",converterEntity,serializerEntity,true,1);
	r_Shell.RegisterModule("Fragment",filterBoundHierarchy,converterFragment,true);
	r_Shell.RegisterModule("Skeleton",filterBoneRoot,converterSkeleton,serializerSkeleton,true);
	r_Shell.RegisterModule("Animation",filterBoneRoot,converterAnimation,serializerAnimation,true);
	r_Shell.RegisterModule("Mesh",filterMeshHierarchy,converterMeshHierarchy,processorMeshCombiner,serializerMesh,true);
	r_Shell.RegisterModule("Bound",filterBoundHierarchy,converterBoundHierarchy,processorBoundCombiner,serializerBound,true);
	r_Shell.RegisterModule("EdgeModel",filterMeshHierarchy,converterEdgeModel,serializerEdgeModel,true);
	r_Shell.RegisterModule("Locator",filterLocator,converterLocator, true);
	r_Shell.RegisterModule("Particle",filterParticle,converterParticle,serializerParticle,true);

	r_Shell.RegisterModule("EnvCloth", converterEnvCloth, true);
	r_Shell.RegisterModule("CharCloth", converterCharCloth, true);
	r_Shell.RegisterModule("ClothPiece", filterCloth, converterClothPiece, true);

	//properties registration

	//basic properties

	r_Shell.RegisterProperty("Nodes",propertyInputNodes);
	r_Shell.RegisterProperty("Name",propertyName);
	r_Shell.RegisterProperty("OutputPath",propertyOutputPath);
	r_Shell.RegisterProperty("ExportData",propertyExportData);

	//mesh properties

	r_Shell.RegisterProperty("TextureOutputPath",propertyTextureOutputPath);
	r_Shell.RegisterProperty("ExportTextures",propertyMeshSetExportTextures);
	r_Shell.RegisterProperty("ExportEntityTextureList", propertyMeshSetExportEntityTextureList);
	r_Shell.RegisterProperty("EntityTextureListKeepExtensions", propertyMeshEntityTextureListKeepExtensions);
	r_Shell.RegisterProperty("MaxPolyCountToCombine",propertyMeshMaxPolyCountToCombine);
	r_Shell.RegisterProperty("MaxPolyCountOfCombined",propertyMeshMaxPolyCountOfCombinedMesh);
	r_Shell.RegisterProperty("MaxDistanceToCombine",propertyMeshCombineMaxDistance);
	r_Shell.RegisterProperty("MeshForSkeleton",propertyMeshForSkeleton);
	r_Shell.RegisterProperty("MeshAsAscii",propertyMeshAsAscii);
	r_Shell.RegisterProperty("MeshSkinOffset",propertyMeshSkinOffset);
	r_Shell.RegisterProperty("MeshExportWorldSpace",propertyMeshExportWorldSpace);
	r_Shell.RegisterProperty("CullVertexChannels",propertyCullVertexChannels);
	r_Shell.RegisterProperty("ForcedAdditionalUVCount",propertyForcedAdditionalUVCount);
	r_Shell.RegisterProperty("LimitToSkelSpecFileBones", propertyLimitToSkelSpecFileBones);
	r_Shell.RegisterProperty("UseWeightedNormals", propertyUseWeightedNormals);
	r_Shell.RegisterProperty("MeshExportTextureList",propertyMeshExportTextureList);
	r_Shell.RegisterProperty("MeshOptimiseMaterials",propertyMeshOptimiseMaterials);
	r_Shell.RegisterProperty("MeshBlindVertexDataChannels",propertyMeshBlindVertexDataChannels);
	r_Shell.RegisterProperty("CombineAcrossBones",propertyMeshCombineAcrossBones);
	r_Shell.RegisterProperty("CombineMergeMaterials",propertyMeshCombineMergeMaterials);

	r_Shell.RegisterProperty("MeshCustomVertexColour",propertyMeshCustomVertexColour);
	r_Shell.RegisterProperty("MeshGenerateTintIndices",propertyMeshGenerateTintIndices);
	r_Shell.RegisterProperty("MeshRemoveDegenerates",propertyMeshRemoveDegenerates);
	r_Shell.RegisterProperty("MeshWeldWithoutQuantize",propertyMeshWeldWithoutQuantize);
	r_Shell.RegisterProperty("MeshWeld",propertyMeshWeld);
	r_Shell.RegisterProperty("MeshSplitBlendsIntoUniqueMeshes",propertyMeshSplitBlendsIntoUniqueMeshes);
	r_Shell.RegisterProperty("MeshMicroMorphs",propertyMeshMicroMorphs);
	r_Shell.RegisterProperty("ExpectedVertexCount",propertyExpectedVertexCount);

	// RAGE physics bounds

	r_Shell.RegisterProperty("ExportAsComposite",propertyBoundExportAsComposite);
	r_Shell.RegisterProperty("ForceExportAsComposite",propertyBoundForceExportAsComposite);
	r_Shell.RegisterProperty("BoundExportWorldSpace",propertyBoundExportWorldSpace);
	r_Shell.RegisterProperty("SurfaceType",propertyBoundSurfaceType);
	r_Shell.RegisterProperty("BoundOctreeMaxHeight",propertyBoundOctreeMaxHeight);
	r_Shell.RegisterProperty("BoundOctreeMaxPerCell",propertyBoundOctreeMaxPerCell);
	r_Shell.RegisterProperty("BoundZeroPrimitiveCentroids",propertyMaxBoundZeroPrimitiveCentroids);
	r_Shell.RegisterProperty("BoundUseFiltering",propertyBoundUseFiltering);
//	r_Shell.RegisterProperty("BoundQuadrify",propertyBoundQuadrify);
	r_Shell.RegisterProperty("BoundSetToNodeName",propertyBoundSetToNodeName);
	r_Shell.RegisterProperty("BoundMaxPolyCountToCombine",propertyMaxBoundMaxPolyCountToCombine);
	r_Shell.RegisterProperty("ApplyBulletShrinkMargin", propertyApplyBulletShrinkMargin);
	r_Shell.RegisterProperty("SplitOnMaterialLimit", propertySplitOnMaterialLimit);
	r_Shell.RegisterProperty("BoundCheckFlags", propertyBoundCheckFlags);
	r_Shell.RegisterProperty("ExportAsBvhGrid", propertyBoundSetGeometryBoundsExportAsBvhGrid);
	r_Shell.RegisterProperty("ExportAsBvhGeom", propertyBoundSetGeometryBoundsExportAsBvhGeom);

	r_Shell.RegisterProperty( "BoundHasSecondSurface", propertyBoundHasSecondSurface );
	r_Shell.RegisterProperty( "BoundSecondSurfaceMaxHeight", propertyBoundSecondSurfaceMaxHeight );
	r_Shell.RegisterProperty( "BoundForceComposite", propertyBoundForceComposite );
	r_Shell.RegisterProperty( "BoundExportObjectSpace", propertyBoundExportObjectSpace );
	r_Shell.RegisterProperty( "BoundExportMiloSpace", propertyBoundExportMiloSpace );
	r_Shell.RegisterProperty( "BoundHasMeshTint", propertyBoundHasMeshTint );
	r_Shell.RegisterProperty( "BoundGenerateMaterials",propertyBoundGenerateMaterials);
	r_Shell.RegisterProperty( "BoundExportPrimitivesInBvh", propertyBoundExportPrimsInBvh );
	r_Shell.RegisterProperty( "BoundDisableOptimisation", propertyBoundDisableOptimisation );
	r_Shell.RegisterProperty( "BoundExportPerVertAttributes", propertyBoundExportPerVertAttributes );

	//entity properties

	r_Shell.RegisterProperty("EntityTypeFileName",propertyEntityTypeFileName);
	r_Shell.RegisterProperty("SkipBoundSectionOfTypeFile",propertyEntitySkipBoundSectionOfTypeFile);
	r_Shell.RegisterProperty("EntityWriteBoundSituations",propertyMaxEntityWriteBoundSituations);
	r_Shell.RegisterProperty("EntityBoundListName",propertyEntityBoundListName);
	r_Shell.RegisterProperty("EntityFragmentWriteMeshes",propertyEntityFragmentWriteMeshes);
	r_Shell.RegisterProperty("EntityGroupBoundsByBone",propertyEntityGroupBoundsByBone);
	r_Shell.RegisterProperty("EntityAllowEmptyFragSection",propertyEntityAllowEmptyFragSection);
	r_Shell.RegisterProperty("EntityWriteShadingGroup",propertyEntityWriteShadingGroup);
	r_Shell.RegisterProperty("EntityErrorOnClothWithoutVertexTuning",propertyErrorOnClothWithoutVertexTuning);
	r_Shell.RegisterProperty("EntitySplitFlagGroupsToDrawables",propertyEntitySplitFlagGroupsToDrawables);
	r_Shell.RegisterProperty("EntitySplitLODGroupsByFlags",propertyEntitySplitLODGroupsByFlags);

	r_Shell.RegisterProperty( "FragmentForceLoadCommonDrawable", propertyFragmentForceLoadCommonDrawable);

	//animation properties

	r_Shell.RegisterProperty("Mover",propertyAnimMover);
	r_Shell.RegisterProperty("ExportCtrlFile",propertyExportCtrlFile);
	r_Shell.RegisterProperty("ChannelsToExport",propertyChannelsToWrite);
	r_Shell.RegisterProperty("AnimMoverTrack",propertyAnimMoverTrack);
	r_Shell.RegisterProperty("AnimExtraMoverTrack",propertyAnimExtraMoverTrack);
	r_Shell.RegisterProperty("AnimExportSelected",propertyAnimExportSelected);
	r_Shell.RegisterProperty("AnimMoveRotToRoot",propertyMaxAnimationMoveRotToRoot);
	r_Shell.RegisterProperty("AnimAuthoredOrient",propertyMaxAnimationAuthoredOrient);
	r_Shell.RegisterProperty("AnimCompressionTolerance",propertyMaxAnimationCompressionTolerance);
	r_Shell.RegisterProperty("AnimProjectFlags",propertyMaxAnimationProjectFlags);
	r_Shell.RegisterProperty("AnimLocalSpace",propertyMaxAnimationLocalSpace);
	r_Shell.RegisterProperty("AnimUvAnimIndex",propertyMaxAnimationUvAnimIndex);
	r_Shell.RegisterProperty("AnimMorphWeightTracks",propertyMaxAnimationMorphWeightTracks);
	r_Shell.RegisterProperty("AnimMaximumBlockSize",propertyAnimationMaximumBlockSize);
	r_Shell.RegisterProperty("AnimExpressionControlNodes",propertyAnimationExprControlNodes);
	r_Shell.RegisterProperty("AnimExpressionFileControlNodes",propertyAnimationExprControlNodesFile);
	r_Shell.RegisterProperty("AnimEndFrame",propertyMaxAnimationEndFrame);
	r_Shell.RegisterProperty("AnimStartFrame",propertyMaxAnimationStartFrame);

	//skeleton properties

	r_Shell.RegisterProperty("SkeletonRootAtOrigin",propertySkeletonRootAtOrigin);
	r_Shell.RegisterProperty("SkeletonWriteLimitAndLockInfo",propertySkeletonWriteLimitAndLockInfo);
	r_Shell.RegisterProperty("SkeletonOmitMissingTracks",propertySkeletonOmitMissingTracks);
	r_Shell.RegisterProperty("SkeletonBoneTypes",propertySkeletonBoneTypes);
	r_Shell.RegisterProperty("SkeletonUseBoneIDs",propertySkeletonUseBoneIDs);
	r_Shell.RegisterProperty("SkeletonAuthoredOrient",propertySkeletonAuthoredOrient);
	r_Shell.RegisterProperty("SkeletonFlipConstraints",propertySkeletonFlipConstraints);
	r_Shell.RegisterProperty("SkeletonLimitToSkinBones",propertySkeletonLimitToSkinBones);

	//fragment properties
	r_Shell.RegisterProperty("FragmentNewStyle",propertyMaxFragmentNewStyle);
	r_Shell.RegisterProperty("FragmentBreakableGlass",propertyMaxFragmentBreakableGlassNode);

	//cloth properties
	r_Shell.RegisterProperty("ClothMesh", propertyClothMesh);
	r_Shell.RegisterProperty("ClothBounds", propertyClothBounds);
}
