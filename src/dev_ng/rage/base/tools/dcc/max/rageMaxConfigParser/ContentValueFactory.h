#ifndef __RAGEMAXCONFIGPARSER_CONTENTNODEFACTORY_H__
#define __RAGEMAXCONFIGPARSER_CONTENTNODEFACTORY_H__

//
// filename:	ContentValueFactory.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		23 February 2010
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/name.h>
#include "maxscript/compiler/parser.h"
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include "maxscript/macros/local_class.h"
//#include "maxscrpt/funcs.h"
#include "maxscript/macros/local_abstract_generic_functions.h"
#include "maxscript/macros/local_implementations.h"

// configParser headers
#include "configParser/contentNode.h"

//PURPOSE
// This is a utility class used internally to construct Content*Value MaxScript
// Value objects for ContentNode classes.  This means we centralise the 
// construction when adding a new node type we only need to change stuff in this
// class.
//
// Magic. 
//
class ContentValueFactory
{
public:
	static Value* CreateNode( const configParser::ContentNode* pNode );

private:
	// Hide default and copy constructors and assignment operator. 
	ContentValueFactory( );
	ContentValueFactory( const ContentValueFactory& copy );
	ContentValueFactory& operator=( ContentValueFactory& copy );
};

#endif // __RAGEMAXCONFIGPARSER_CONTENTNODEFACTORY_H__
