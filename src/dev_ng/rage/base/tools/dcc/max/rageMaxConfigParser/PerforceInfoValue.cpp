//
// filename:	PerforceInfoValue.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		20 April 2011
// description:	
//

// Local headers
#include "PerforceInfoValue.h"

// --- PerforceInfoValue Instance -------------------------------------------

local_visible_class_instance( PerforceInfoValue, "PerforceInfo" )

Value*
PerforceInfoValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Perforce Info values cannot be created in MaxScript." );
	return ( &undefined );
}

// --- PerforceInfoValue Methods --------------------------------------------

PerforceInfoValue::PerforceInfoValue( const configParser::PerforceInfo* pInfo )
{
	tag = class_tag(PerforceInfoValue);
	m_sName = new String( pInfo->GetName() );
	m_sUserName = new String( pInfo->GetUsername() );
	m_sPort = new String( pInfo->GetServer() );
	m_sWorkspace = new String( pInfo->GetWorkspace() );
}

PerforceInfoValue::~PerforceInfoValue( )
{
}

void
PerforceInfoValue::collect( ) 
{ 
	delete this; 
}

void
PerforceInfoValue::sprin1( CharStream* s )
{
	s->printf( "PerforceInfo(\n" );
	s->printf( "\tname:      \"%s\",\n", m_sName->to_string() );
	s->printf( "\tusername:  \"%s\",\n", m_sUserName->to_string() );
	s->printf( "\tserver:    \"%s\",\n", m_sPort->to_string() );
	s->printf( "\tworkspace: \"%s\",\n", m_sWorkspace->to_string() );
	s->printf( ")\n" );
}

void
PerforceInfoValue::gc_trace( )
{
	Value::gc_trace( );
	if ( m_sName->is_not_marked() )
		m_sName->gc_trace( );
	if ( m_sUserName->is_not_marked() )
		m_sUserName->gc_trace( );
	if ( m_sPort->is_not_marked() )
		m_sPort->gc_trace( );
	if ( m_sWorkspace->is_not_marked() )
		m_sWorkspace->gc_trace( );
}

Value*
PerforceInfoValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( m_sName );
	if ( 0 == _stricmp( arg->string, "server" ) )
		return_protected( m_sPort );
	if ( 0 == _stricmp( arg->string, "username" ) )
		return_protected( m_sUserName );
	if ( 0 == _stricmp( arg->string, "workspace" ) )
		return_protected( m_sWorkspace );
	
	return Value::get_property( arg_list, count );
}

Value*
PerforceInfoValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "PerforceInfo objects are read-only.", this );
	return ( &undefined );
}

// PerforceInfoValue.cpp
