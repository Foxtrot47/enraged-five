//
// filename:	ContentVehicleValues.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		15 June 2010
// description:	
//

// Local headers
#include "ContentVehicleValues.h"
#include "ContentValueFactory.h"

// --- ContentVehiclesRpfValue Instance --------------------------------------

local_visible_class_instance( ContentVehiclesRpfValue, "ContentVehiclesRpf" )

Value*
ContentVehiclesRpfValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentVehiclesRpfValue Methods ---------------------------------------

ContentVehiclesRpfValue::ContentVehiclesRpfValue( const configParser::ContentNodeVehiclesRpf* pNode )
	: m_pNode( pNode )
{
}

void
ContentVehiclesRpfValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentVehiclesRpf ( *no node* )" );
		return;
	}

	s->printf( "ContentVehiclesRpf(\n" );
	s->printf( "\tname:           \"%s\",\n", m_pNode->GetName() );
	TCHAR path[_MAX_PATH] = {0};
	m_pNode->GetAbsolutePath(path, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", path);
	s->printf( "\tinputs:         [%d]\n", m_pNode->GetNumInputs() );
	s->printf( "\toutput:         [%d]\n", m_pNode->GetNumOutputs() );
	TCHAR txd[_MAX_PATH] = {0};
	m_pNode->GetSharedTxd(txd, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", txd);
	TCHAR txdIn[_MAX_PATH] = {0};
	m_pNode->GetSharedTxdInput(txdIn, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", txdIn);
	TCHAR friendname[_MAX_PATH] = {0};
	m_pNode->GetFriendlyName(friendname, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", friendname);
	s->printf( ")\n" );
}

void
ContentVehiclesRpfValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentVehiclesRpfValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
	if ( 0 == _stricmp( arg->string, "path" ) )
	{
		TCHAR path[_MAX_PATH] = {0};
		m_pNode->GetAbsolutePath(path, _MAX_PATH);
		return_protected( new String( path ) );
	}
	if ( 0 == _stricmp( arg->string, "extension" ) )
		return_protected( new String( m_pNode->GetExtension() ) );
	if ( 0 == _stricmp( arg->string, "filename" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetFilename(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "stack_path" ) )
		return ( m_pNode->GetStackPath() ? &true_value : &false_value );
	if ( 0 == _stricmp( arg->string, "compress" ) )
		return ( m_pNode->GetCompress() ? &true_value : &false_value );
	if ( 0 == _stricmp( arg->string, "friendlyname" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetFriendlyName(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "sharedtxd" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetSharedTxd(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "sharedtxdinput" ) )
	{
		TCHAR txdIn[_MAX_PATH] = {0};
		m_pNode->GetSharedTxdInput(txdIn, _MAX_PATH);
		return_protected( new String( txdIn ) );
	}
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			  it != m_pNode->EndInputs();
			  ++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}
	if ( 0 == _stricmp( arg->string, "outputs" ) )
	{
		Array* pOutputs = new Array( (int)m_pNode->GetNumOutputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginOutputs();
			it != m_pNode->EndOutputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pOutputs->append( pNodeValue );
		}
		return_protected( pOutputs );
	}

	return ( Value::get_property( arg_list, count ) );
}

Value*
ContentVehiclesRpfValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}

// --- ContentVehiclesZipValue Instance --------------------------------------

local_visible_class_instance( ContentVehiclesZipValue, "ContentVehiclesZip" )

Value*
ContentVehiclesZipValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentVehiclesZipValue Methods ---------------------------------------

ContentVehiclesZipValue::ContentVehiclesZipValue( const configParser::ContentNodeVehiclesZip* pNode )
: m_pNode( pNode )
{
}

void
ContentVehiclesZipValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentVehiclesZip ( *no node* )" );
		return;
	}

	s->printf( "ContentVehiclesZip(\n" );
	s->printf( "\tname:           \"%s\",\n", m_pNode->GetName() );
	TCHAR path[_MAX_PATH] = {0};
	m_pNode->GetAbsolutePath(path, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", path);
	s->printf( "\tinputs:         [%d]\n", m_pNode->GetNumInputs() );
	TCHAR txd[_MAX_PATH] = {0};
	m_pNode->GetSharedTxd(txd, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", txd);
	TCHAR txdIn[_MAX_PATH] = {0};
	m_pNode->GetSharedTxdInput(txdIn, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", txdIn);
	TCHAR friendname[_MAX_PATH] = {0};
	m_pNode->GetFriendlyName(friendname, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", friendname);
	s->printf( ")\n" );
}

void
ContentVehiclesZipValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentVehiclesZipValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
	if ( 0 == _stricmp( arg->string, "path" ) )
	{
		TCHAR path[_MAX_PATH] = {0};
		m_pNode->GetAbsolutePath(path, _MAX_PATH);
		return_protected( new String( path ) );
	}
	if ( 0 == _stricmp( arg->string, "extension" ) )
		return_protected( new String( m_pNode->GetExtension() ) );
	if ( 0 == _stricmp( arg->string, "filename" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetFilename(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
// 	if ( 0 == _stricmp( arg->string, "stack_path" ) )
// 		return ( m_pNode->GetStackPath() ? &true_value : &false_value );
	if ( 0 == _stricmp( arg->string, "compress" ) )
		return ( m_pNode->GetCompress() ? &true_value : &false_value );
	if ( 0 == _stricmp( arg->string, "friendlyname" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetFriendlyName(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "sharedtxd" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetSharedTxd(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "sharedtxdinput" ) )
	{
		TCHAR txdIn[_MAX_PATH] = {0};
		m_pNode->GetSharedTxdInput(txdIn, _MAX_PATH);
		return_protected( new String( txdIn ) );
	}
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			it != m_pNode->EndInputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}

	return ( Value::get_property( arg_list, count ) );
}

Value*
ContentVehiclesZipValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}
// ContentMapValues.cpp



// --- ContentVehiclesDirectoryValue Instance --------------------------------------

local_visible_class_instance( ContentVehiclesDirectoryValue, "ContentVehiclesDirectory" )

Value*
ContentVehiclesDirectoryValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentVehiclesDirectoryValue Methods ---------------------------------------

ContentVehiclesDirectoryValue::ContentVehiclesDirectoryValue( const configParser::ContentNodeVehiclesDirectory* pNode )
	: m_pNode( pNode )
{
}

void
ContentVehiclesDirectoryValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentVehiclesDirectory ( *no node* )" );
		return;
	}

	s->printf( "ContentVehiclesDirectory(\n" );
	TCHAR apath[_MAX_PATH] = {0};
	m_pNode->GetAbsolutePath( apath, _MAX_PATH );
	s->printf( "\tpath:            \"%s\",\n", apath);
	s->printf( "\tinputs:         [%d]\n", m_pNode->GetNumInputs() );
	TCHAR txd[_MAX_PATH] = {0};
	m_pNode->GetSharedTxd(txd, _MAX_PATH);
	s->printf( "\tSharedTxd:      \"%s\",\n", txd);
	TCHAR txdIn[_MAX_PATH] = {0};
	m_pNode->GetSharedTxdInput(txdIn, _MAX_PATH);
	s->printf( "\tSharedTxdInput: \"%s\",\n", txdIn);
	TCHAR friendname[_MAX_PATH] = {0};
	m_pNode->GetFriendlyName(friendname, _MAX_PATH);
	s->printf( "\tFriendlyName:   \"%s\",\n", friendname);
	s->printf( ")\n" );
}

void
ContentVehiclesDirectoryValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentVehiclesDirectoryValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
	if ( 0 == _stricmp( arg->string, "path" ) )
	{
		TCHAR path[_MAX_PATH] = {0};
		m_pNode->GetAbsolutePath(path, _MAX_PATH);
		return_protected( new String( path ) );
	}
	if ( 0 == _stricmp( arg->string, "friendlyname" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetFriendlyName(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "sharedtxd" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetSharedTxd(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "sharedtxdinput" ) )
	{
		TCHAR txdIn[_MAX_PATH] = {0};
		m_pNode->GetSharedTxdInput(txdIn, _MAX_PATH);
		return_protected( new String( txdIn ) );
	}
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			it != m_pNode->EndInputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}

	return ( Value::get_property( arg_list, count ) );
}

Value*
ContentVehiclesDirectoryValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}


// --- ContentVehicleModZipValue Instance ------------------------------------------

local_visible_class_instance( ContentVehicleModZipValue, "ContentVehicleModZip" )

Value*
ContentVehicleModZipValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentVehicleModZipValue Methods -------------------------------------------

ContentVehicleModZipValue::ContentVehicleModZipValue( const configParser::ContentNodeVehicleModZip* pNode )
: m_pNode( pNode )
{
}

void
ContentVehicleModZipValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentVehicleModZip ( *no node* )" );
		return;
	}

	s->printf( "ContentVehicleModZip(\n" );
	s->printf( "\tname:     \"%s\",\n", m_pNode->GetName() );
	TCHAR path[_MAX_PATH] = {0};
	m_pNode->GetAbsolutePath(path, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", path);
	s->printf( "\tinputs:   [%d]\n", m_pNode->GetNumInputs() );
	s->printf( "\toutput:   [%d]\n", m_pNode->GetNumOutputs() );
//	s->printf( "\tchildren: [%d]\n", m_pNode->GetNumChildren() ); 
	s->printf( ")\n" );
}

void
ContentVehicleModZipValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentVehicleModZipValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
	if ( 0 == _stricmp( arg->string, "path" ) )
	{
		TCHAR path[_MAX_PATH] = {0};
		m_pNode->GetAbsolutePath(path, _MAX_PATH);
		return_protected( new String( path ) );
	}
	if ( 0 == _stricmp( arg->string, "extension" ) )
		return_protected( new String( m_pNode->GetExtension() ) );
	if ( 0 == _stricmp( arg->string, "filename" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetFilename(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "compress" ) )
		return ( m_pNode->GetCompress() ? &true_value : &false_value );	
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
// 	if ( arg == n_children )
// 	{
// 		Array* pChildren = new Array( (int)m_pNode->GetNumChildren() );
// 		for ( configParser::ContentNodeGroup::NodeContainerConstIter it = m_pNode->BeginChildren();
// 			it != m_pNode->EndChildren();
// 			++it )
// 		{
// 			// Use the factory to create the desired MaxScript Value content node.
// 			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
// 			pChildren->append( pNodeValue );
// 		}
// 		return_protected( pChildren );
// 	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			it != m_pNode->EndInputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}
	if ( 0 == _stricmp( arg->string, "outputs" ) )
	{
		Array* pOutputs = new Array( (int)m_pNode->GetNumOutputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginOutputs();
			it != m_pNode->EndOutputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pOutputs->append( pNodeValue );
		}
		return_protected( pOutputs );
	}

	return ( Value::get_property( arg_list, count ) );
}

Value*
ContentVehicleModZipValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}

