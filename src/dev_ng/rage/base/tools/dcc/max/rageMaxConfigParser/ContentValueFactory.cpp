//
// filename:	ContentValueFactory.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		23 February 2010
// description:	
//

// Local headers
#include "ContentValueFactory.h"

#include "ContentDependencyDirectoryValue.h"
#include "ContentDependencyFileValue.h"
#include "ContentDirectoryValue.h"
#include "ContentFileValue.h"
#include "ContentGroupValue.h"
#include "ContentLevelValue.h"
#include "ContentMapValues.h"
#include "ContentRpfValue.h"
#include "ContentVehicleValues.h"
#include "ContentUnknownValue.h"
#include "ContentZipValue.h"

#include "configParser/contentNode.h"
#include "configParser/contentNodeCore.h"
#include "configParser/contentNodeDependencyDirectory.h"
#include "configParser/contentNodeDependencyFile.h"
#include "configParser/contentNodeGroup.h"
#include "configParser/contentNodeLevel.h"
#include "configParser/contentNodeMaps.h"
#include "configParser/contentNodeVehicles.h"
#include "configParser/contentNodeUnknown.h"

using namespace configParser;

Value* 
ContentValueFactory::CreateNode( const ContentNode* pNode )
{
	if ( dynamic_cast<const ContentNodeMapZip*>( pNode ) )
	{
		const ContentNodeMapZip* pMapNode = dynamic_cast<const ContentNodeMapZip*>( pNode );
		return ( new ContentMapZipValue( pMapNode ) );
	}
	else if ( dynamic_cast<const ContentNodeMapProcessedZip*>( pNode ) )
	{
		const ContentNodeMapProcessedZip* pMapNode = dynamic_cast<const ContentNodeMapProcessedZip*>( pNode );
		return ( new ContentMapProcessedZipValue( pMapNode ) );
	}
	else if ( dynamic_cast<const ContentNodeVehiclesRpf*>( pNode ) )
	{
		const ContentNodeVehiclesRpf* pRpfNode = dynamic_cast<const ContentNodeVehiclesRpf*>( pNode );
		return ( new ContentVehiclesRpfValue( pRpfNode ) );
	}
	else if ( dynamic_cast<const ContentNodeVehiclesZip*>( pNode ) )
	{
		const ContentNodeVehiclesZip* pVehZipNode = dynamic_cast<const ContentNodeVehiclesZip*>( pNode );
		return ( new ContentVehiclesZipValue( pVehZipNode ) );
	}
	else if ( dynamic_cast<const ContentNodeVehiclesDirectory*>( pNode ) )
	{
		const ContentNodeVehiclesDirectory* pVehZipNode = dynamic_cast<const ContentNodeVehiclesDirectory*>( pNode );
		return ( new ContentVehiclesDirectoryValue( pVehZipNode ) );
	}
	else if ( dynamic_cast<const ContentNodeVehicleModZip*>( pNode ) )
	{
		const ContentNodeVehicleModZip* pVehZipNode = dynamic_cast<const ContentNodeVehicleModZip*>( pNode );
		return ( new ContentVehicleModZipValue( pVehZipNode ) );
	}
	else if ( dynamic_cast<const ContentNodeRpf*>( pNode ) )
	{
		const ContentNodeRpf* pRpfNode = dynamic_cast<const ContentNodeRpf*>( pNode );
		return ( new ContentRpfValue( pRpfNode ) );
	}
	else if ( dynamic_cast<const ContentNodeZip*>( pNode ) )
	{
		const ContentNodeZip* pMapNode = dynamic_cast<const ContentNodeZip*>( pNode );
		return ( new ContentZipValue( pMapNode ) );
	}
	else if ( dynamic_cast<const ContentNodeFile*>( pNode ) )
	{
		const ContentNodeFile* pFileNode = dynamic_cast<const ContentNodeFile*>( pNode );
		return ( new ContentFileValue( pFileNode ) );
	}
	else if ( dynamic_cast<const ContentNodeDirectory*>( pNode ) )
	{
		const ContentNodeDirectory* pDirNode = dynamic_cast<const ContentNodeDirectory*>( pNode );
		return ( new ContentDirectoryValue( pDirNode ) );
	}
	else if ( dynamic_cast<const ContentNodeLevel*>( pNode ) )
	{
		const ContentNodeLevel* pLevelNode = dynamic_cast<const ContentNodeLevel*>( pNode );
		return ( new ContentLevelValue( pLevelNode ) );
	}
	else if ( dynamic_cast<const ContentNodeGroup*>( pNode ) )
	{
		const ContentNodeGroup* pGroupNode = dynamic_cast<const ContentNodeGroup*>( pNode );
		return ( new ContentGroupValue( pGroupNode ) );
	}
	else if ( dynamic_cast<const ContentNodeMap*>( pNode ) )
	{
		const ContentNodeMap* pMapNode = dynamic_cast<const ContentNodeMap*>( pNode );
		return ( new ContentMapValue( pMapNode ) );
	}
	else if ( dynamic_cast<const ContentNodeUnknown*>( pNode ) )
	{
		const ContentNodeUnknown* pUnknownNode = dynamic_cast<const ContentNodeUnknown*>( pNode );
		return ( new ContentUnknownValue( pUnknownNode ) );
	}
	else if ( dynamic_cast<const ContentNodeDependencyDirectory*>( pNode ) )
	{
		const ContentNodeDependencyDirectory* pDependencyDirectoryNode = dynamic_cast<const ContentNodeDependencyDirectory*>( pNode );
		return ( new ContentDependencyDirectoryValue( pDependencyDirectoryNode ) );
	}
	else if ( dynamic_cast<const ContentNodeDependencyFile*>( pNode ) )
	{
		const ContentNodeDependencyFile* pDependencyFileNode = dynamic_cast<const ContentNodeDependencyFile*>( pNode );
		return ( new ContentDependencyFileValue( pDependencyFileNode ) );
	}

	// Default
	return ( &undefined );
}

// ContentNodeFactory.cpp
