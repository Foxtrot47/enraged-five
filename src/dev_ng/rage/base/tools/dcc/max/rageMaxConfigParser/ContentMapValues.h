#ifndef __RAGEMAXCONFIGPARSER_CONTENTMAPVALUES_H__
#define __RAGEMAXCONFIGPARSER_CONTENTMAPVALUES_H__

//
// filename:	ContentGroupValue.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		19 February 2010
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// STL includes
#include <string>

// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/name.h>
#include "maxscript/compiler/parser.h"
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include "maxscript/macros/local_class.h"
//#include "maxscrpt/funcs.h"
#include "maxscript/macros/local_abstract_generic_functions.h"
#include "maxscript/macros/local_implementations.h"

// configParser headers
#include "configParser/contentNodeMaps.h"

//declare_local_generic_class
local_applyable_class (ContentMapValue)

//PURPOSE
//
class ContentMapValue : public Value
{
public:
	ContentMapValue( const configParser::ContentNodeMap* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentMapValue ) ); }
					classof_methods( ContentMapValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	static std::string get_maptype_str(configParser::ContentNodeMap::eMapType mapType);
	const configParser::ContentNodeMap* m_pNode;
};

local_applyable_class (ContentMapZipValue)

//PURPOSE
//
class ContentMapZipValue : public Value
{
public:
	ContentMapZipValue( const configParser::ContentNodeMapZip* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentMapZipValue ) ); }
	classof_methods( ContentMapZipValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeMapZip* m_pNode;
};

local_applyable_class (ContentMapProcessedZipValue)

//PURPOSE
//
class ContentMapProcessedZipValue : public Value
{
public:
	ContentMapProcessedZipValue( const configParser::ContentNodeMapProcessedZip* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentMapProcessedZipValue ) ); }
	classof_methods( ContentMapProcessedZipValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeMapProcessedZip* m_pNode;
};

#endif // __RAGEMAXCONFIGPARSER_CONTENTMAPVALUES_H__
