// 
// /pluginMain.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "resource.h"
#define RAGEMAXCONFIGPARSER_API __declspec( dllexport )

HINSTANCE	g_hDllInstance;
int			g_controlsInit = FALSE;
bool		g_libInitialized = FALSE;


//-----------------------------------------------------------------------------

TCHAR *GetString(int id)
{
	static TCHAR buf[1024];

	if (g_hDllInstance)
		return LoadString(g_hDllInstance, id, buf, sizeof(buf)) ? buf : NULL;

	return NULL;
}

//-----------------------------------------------------------------------------

BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	g_hDllInstance = hinstDLL;

#if ( MAX_RELEASE < 9000 ) 
	sprintf_s(g_description,"RageMaxScriptExt built with rage release %d on %s at %s",RAGE_RELEASE,__DATE__,__TIME__);

	if (!g_controlsInit) 
	{
		g_controlsInit = TRUE;
		InitCustomControls(g_hDllInstance);
		InitCommonControls();

		// Initialize RAGE
		char* argv[1];
		argv[0] = "rageMaxScriptExt.dlx";
		sysParam::Init( 1, argv );

		INIT_PARSER;
	}
#endif

	return (TRUE);
}

RAGEMAXCONFIGPARSER_API ClassDesc* 
LibClassDesc( int i )
{
	return ( NULL );
}

RAGEMAXCONFIGPARSER_API int 
LibNumberClasses( )
{
	return ( 0 );
}

RAGEMAXCONFIGPARSER_API const TCHAR* 
LibDescription( )
{
	return ( GetString( IDS_DESCRIPTION ) );
}

RAGEMAXCONFIGPARSER_API ULONG 
LibVersion()
{
	return VERSION_3DSMAX;
}

#if ( MAX_RELEASE >= 9000 ) 

RAGEMAXCONFIGPARSER_API int 
LibInitialize( void )
{
	//According to the SDK docs LibInitialize "should" only be called once but this isn't strictly enforced
	//so make sure we only initialize once
	if(!g_libInitialized)
	{
		//Perform any custom plugin initialization here
		g_libInitialized = TRUE;
	}
	return ( 1 );
}

RAGEMAXCONFIGPARSER_API int 
LibShutdown( )
{
	return ( 1 );
}

#endif //( MAX_RELEASE >= 9000 ) 



//-----------------------------------------------------------------------------

