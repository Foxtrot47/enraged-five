#ifndef __RAGEMAXCONFIGPARSER_CONTENTVEHICLEVALUES_H__
#define __RAGEMAXCONFIGPARSER_CONTENTVEHICLEVALUES_H__

//
// filename:	ContentVehicleValues.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		15 June 2010
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/name.h>
#include "maxscript/compiler/parser.h"
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include "maxscript/macros/local_class.h"
//#include "maxscrpt/funcs.h"
#include "maxscript/macros/local_abstract_generic_functions.h"
#include "maxscript/macros/local_implementations.h"

// configParser headers
#include "configParser/contentNodeVehicles.h"

//declare_local_generic_class

local_applyable_class (ContentVehiclesRpfValue)
local_applyable_class (ContentVehiclesZipValue)
local_applyable_class (ContentVehiclesDirectoryValue)
local_applyable_class (ContentVehicleModZipValue)

//PURPOSE
//
class ContentVehiclesRpfValue : public Value
{
public:
	ContentVehiclesRpfValue( const configParser::ContentNodeVehiclesRpf* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentVehiclesRpfValue ) ); }
					classof_methods( ContentVehiclesRpfValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeVehiclesRpf* m_pNode;
};

class ContentVehiclesZipValue : public Value
{
public:
	ContentVehiclesZipValue( const configParser::ContentNodeVehiclesZip* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentVehiclesZipValue ) ); }
	classof_methods( ContentVehiclesZipValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeVehiclesZip* m_pNode;
};

class ContentVehiclesDirectoryValue : public Value
{
public:
	ContentVehiclesDirectoryValue( const configParser::ContentNodeVehiclesDirectory* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentVehiclesDirectoryValue ) ); }
	classof_methods( ContentVehiclesDirectoryValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeVehiclesDirectory* m_pNode;
};

//PURPOSE
//
class ContentVehicleModZipValue : public Value
{
public:
	ContentVehicleModZipValue( const configParser::ContentNodeVehicleModZip* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentVehicleModZipValue ) ); }
	classof_methods( ContentVehicleModZipValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeVehicleModZip* m_pNode;
};

#endif // __RAGEMAXCONFIGPARSER_CONTENTVEHICLEVALUES_H__
