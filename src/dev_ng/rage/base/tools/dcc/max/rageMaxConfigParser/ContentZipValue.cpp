//
// filename:	ContentZipValue.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		3 February 2011
// description:	
//

// Local headers
#include "ContentZipValue.h"
#include "ContentValueFactory.h"

// --- ContentGroupValue Instance -------------------------------------------

local_visible_class_instance( ContentZipValue, "ContentZip" )

Value*
ContentZipValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentGroupValue Methods --------------------------------------------

ContentZipValue::ContentZipValue( const configParser::ContentNodeZip* pNode )
	: m_pNode( pNode )
{
}

void
ContentZipValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentZip ( *no node* )" );
		return;
	}

	s->printf( "ContentZip(\n" );
	s->printf( "\tname:      \"%s\",\n", m_pNode->GetName() );
	TCHAR path[_MAX_PATH] = {0};
	m_pNode->GetAbsolutePath(path, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", path);
	s->printf( "\textension: \"%s\",\n", m_pNode->GetExtension() );
	TCHAR fn[_MAX_PATH] = {0};
	m_pNode->GetFilename(fn, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", fn);
	s->printf( "\tcompress:  \"%s\",\n", m_pNode->GetCompress() ? "true" : "false" );
	s->printf( "\tinputs:    [%d]\n", m_pNode->GetNumInputs() );
	s->printf( "\toutput:    [%d]\n", m_pNode->GetNumOutputs() );
	s->printf( ")\n" );
}

void
ContentZipValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentZipValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
	if ( 0 == _stricmp( arg->string, "path" ) )
	{
		TCHAR path[_MAX_PATH] = {0};
		m_pNode->GetAbsolutePath(path, _MAX_PATH);
		return_protected( new String( path ) );
	}
	if ( 0 == _stricmp( arg->string, "extension" ) )
		return_protected( new String( m_pNode->GetExtension() ) );
	if ( 0 == _stricmp( arg->string, "filename" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetFilename(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "compress" ) )
		return ( m_pNode->GetCompress() ? &true_value : &false_value );
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			  it != m_pNode->EndInputs();
			  ++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}
	if ( 0 == _stricmp( arg->string, "outputs" ) )
	{
		Array* pOutputs = new Array( (int)m_pNode->GetNumOutputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginOutputs();
			it != m_pNode->EndOutputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pOutputs->append( pNodeValue );
		}
		return_protected( pOutputs );
	}

	return Value::get_property( arg_list, count );
}

Value*
ContentZipValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}

// ContentGroupValue.cpp
