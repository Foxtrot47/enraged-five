#ifndef __RAGEMAXCONFIGPARSER_CONTENTZIPVALUE_H__
#define __RAGEMAXCONFIGPARSER_CONTENTZIPVALUE_H__

//
// filename:	ContentZipValue.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		3 February 2011
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/name.h>
#include "maxscript/compiler/parser.h"
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include "maxscript/macros/local_class.h"
//#include "maxscrpt/funcs.h"
#include "maxscript/macros/local_abstract_generic_functions.h"
#include "maxscript/macros/local_implementations.h"

// configParser headers
#include "configParser/contentNodeCore.h"

local_applyable_class (ContentZipValue)

//PURPOSE
//
class ContentZipValue : public Value
{
public:
	ContentZipValue( const configParser::ContentNodeZip* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentZipValue ) ); }
					classof_methods( ContentZipValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeZip* m_pNode;
};

#endif // __RAGEMAXCONFIGPARSER_CONTENTZIPVALUE_H__
