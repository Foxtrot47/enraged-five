#ifndef __RAGEMAXCONFIGPARSER_PERFORCEINFOVALUE_H__
#define __RAGEMAXCONFIGPARSER_PERFORCEINFOVALUE_H__

//
// filename:	PerforceInfoValue.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		20 April 2011
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/name.h>
#include <maxscript/compiler/parser.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include <maxscript/macros/local_class.h>
//#include "maxscrpt/funcs.h"
#include <maxscript/macros/local_abstract_generic_functions.h>
#include <maxscript/macros/local_implementations.h>

// configParser headers
#include "configParser/configGameView.h"

//declare_local_generic_class
local_applyable_class (PerforceInfoValue)

//PURPOSE
//
class PerforceInfoValue : public Value
{
public:
	PerforceInfoValue( const configParser::PerforceInfo* pInfo );
	~PerforceInfoValue( );

	ValueMetaClass* local_base_class( ) { return ( class_tag( PerforceInfoValue ) ); }
	classof_methods( PerforceInfoValue, Value );
	void			collect( );
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	String*			m_sName;
	String*			m_sUserName;
	String*			m_sPort;
	String*			m_sWorkspace;
};

#endif // __RAGEMAXCONFIGPARSER_PERFORCEINFOVALUE_H__
