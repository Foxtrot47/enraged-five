#ifndef __RAGEMAXCONFIGPARSER_CONTENTDEPENDENCYDIRECTORYVALUE_H__
#define __RAGEMAXCONFIGPARSER_CONTENTDEPENDENCYDIRECTORYVALUE_H__

//
// filename:	ContentDependencyDirectoryValue.h
// author:		Jonny Rivers <jonathan.rivers@rockstarnorth.com>
// date:		18 November 2011
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/name.h>
#include "maxscript/compiler/parser.h"
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include "maxscript/macros/local_class.h"
//#include "maxscrpt/funcs.h"
#include "maxscript/macros/local_abstract_generic_functions.h"
#include "maxscript/macros/local_implementations.h"

// configParser headers
#include "configParser/contentNodeDependencyDirectory.h"

//declare_local_generic_class
local_applyable_class (ContentDependencyDirectoryValue)

//PURPOSE
//
class ContentDependencyDirectoryValue : public Value
{
public:
	ContentDependencyDirectoryValue( const configParser::ContentNodeDependencyDirectory* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentDependencyDirectoryValue ) ); }
					classof_methods( ContentDependencyDirectoryValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeDependencyDirectory* m_pNode;
};

#endif // __RAGEMAXCONFIGPARSER_CONTENTDEPENDENCYDIRECTORYVALUE_H__
