#ifndef __RAGEMAXCONFIGPARSER_CONTENTRPFVALUE_H__
#define __RAGEMAXCONFIGPARSER_CONTENTRPFVALUE_H__

//
// filename:	ContentRpfValue.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		23 February 2010
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/name.h>
#include <maxscript/compiler/parser.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include <maxscript/macros/local_class.h>
//#include "maxscrpt/funcs.h"
#include <maxscript/macros/local_abstract_generic_functions.h>
#include <maxscript/macros/local_implementations.h>

// configParser headers
#include "configParser/contentNodeCore.h"

local_applyable_class (ContentRpfValue)

//PURPOSE
//
class ContentRpfValue : public Value
{
public:
	ContentRpfValue( const configParser::ContentNodeRpf* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentRpfValue ) ); }
					classof_methods( ContentRpfValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeRpf* m_pNode;
};

#endif // __RAGEMAXCONFIGPARSER_CONTENTRPFVALUE_H__
