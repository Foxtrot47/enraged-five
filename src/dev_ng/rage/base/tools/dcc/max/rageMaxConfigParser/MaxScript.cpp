
// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/Name.h>
#include <maxscript/compiler/parser.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last

// configParser headers
#include "configParser/configParser.h"
#include "configParser/configCoreView.h"
#include "configParser/configGameView.h"
#include "configParser/contentModel.h"
#include "configParser/configUserType.h"
using namespace configParser;

// Local headers
#include "ContentValueFactory.h"
#include "ContentGroupValue.h"
#include "PerforceInfoValue.h"

// DHM -- Undef "GetUserName" because Winbase.h sucks.
#undef GetUserName

// Main ConfigGameView object.
static ConfigCoreView gs_CoreView;
static ConfigGameView gs_GameView;

//---------------------------------------------------------------------------

// Define a macro to ease updating these MaxScript functions.
// This one reads a std::string value from the Game View.
#define RETURN_VIEW_STRING_OR_UNDEFINED( func, view ) \
	char value[_MAX_PATH] = {0}; \
	if ( view.func( value, _MAX_PATH ) ) \
		return_protected( new String( value ) ); \
	return ( &undefined );

// Define a macro to ease updating these MaxScript functions.
// This one reads a int value from the Game View
#define RETURN_VIEW_FLAG_OR_UNDEFINED( func, view ) \
	unsigned int value = 0; \
	if ( view.func( value ) ) \
		return_protected( new Integer( value ) ); \
	return ( &undefined );

// Define a macro to ease updating these MaxScript functions.
// This one reads a std::string value from the Game View.
#define RETURN_VIEW_BOOL( func, view ) \
	return( view.func() ? &true_value : &false_value );
	
// Define a macro to ease updating these MaxScript functions.
// This one reads a std::string value from the Game View.
#define RETURN_VIEW_INT( func, view ) \
	return_protected( new Integer( view.func() ) );


// This defines the MaxScript function and also the implementation.
// Makes adding/removing functions a breeze.
//  pubfunc: MaxScript public function name.
//  gvfunc: ConfigGameView method to invoke.
#define MAXSCRIPT_FUNC_GAMEVIEW_STRING( pubfunc, gvfunc ) \
	def_visible_primitive( pubfunc, #pubfunc ); \
	\
	Value* pubfunc##_cf( Value** arg_list, int count ) \
	{ \
		RETURN_VIEW_STRING_OR_UNDEFINED( gvfunc, gs_GameView ); \
	}

// This defines the MaxScript function and also the implementation.
// Makes adding/removing functions a breeze.
//  pubfunc: MaxScript public function name.
//  gvfunc: ConfigGameView method to invoke.
#define MAXSCRIPT_FUNC_GAMEVIEW_BOOL( pubfunc, gvfunc ) \
	def_visible_primitive( pubfunc, #pubfunc ); \
	\
	Value* pubfunc##_cf( Value** arg_list, int count )\
	{ \
		RETURN_VIEW_BOOL( gvfunc, gs_GameView ); \
	}

// This defines the MaxScript function and also the implementation.
// Makes adding/removing functions a breeze.
//  pubfunc: MaxScript public function name.
//  gvfunc: ConfigGameView method to invoke.
#define MAXSCRIPT_FUNC_GAMEVIEW_INT( pubfunc, gvfunc ) \
	def_visible_primitive( pubfunc, #pubfunc ); \
	\
	Value* pubfunc##_cf( Value** arg_list, int count )\
{ \
	RETURN_VIEW_INT( gvfunc, gs_GameView ); \
}

#define MAXSCRIPT_FUNC_GAMEVIEW_FLAG( pubfunc, gvfunc ) \
	def_visible_primitive( pubfunc, #pubfunc ); \
	\
	Value* pubfunc##_cf( Value** arg_list, int count )\
{ \
	RETURN_VIEW_FLAG_OR_UNDEFINED( gvfunc, gs_GameView ); \
}

//---------------------------------------------------------------------------

// MaxScript Function Publishing
// MaxScript Function Publishing: Generic Queries
def_visible_primitive( RsConfigQuery,		"RsConfigQuery" );

// MaxScript Function Publishing: Global Data
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsConfigGetToolsRootDir,	GetToolsRootDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsConfigGetToolsBinDir,		GetToolsBinDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsConfigGetToolsConfigDir,	GetToolsConfigDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsConfigGetToolsLibDir,		GetToolsLibDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsConfigGetToolsRuby,		GetToolsRuby );

// MaxScript Function Publishing: Project Data
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetName,			GetProjectName );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetUIName,			GetProjectUIName );
MAXSCRIPT_FUNC_GAMEVIEW_BOOL( RsProjectHasLevels,			HasLevels );
MAXSCRIPT_FUNC_GAMEVIEW_BOOL( RsProjectIsEpisodic,			IsEpisodic );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetRootDir,		GetProjectRootDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetBuildDir,		GetBuildDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetMetadataDir,	GetMetadataDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetAssetsDir,		GetAssetsDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetExportDir,		GetExportDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetProcessedDir,	GetProcessedDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetCommonDir,		GetCommonDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetShaderDir,		GetShaderDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetSourceDir,		GetSourceDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetScriptDir,		GetScriptDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetArtDir,			GetArtDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetPreviewDir,		GetPreviewDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetAudioDir,		GetAudioDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetCacheDir,		GetCacheDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetNetworkStreamDir, GetNetworkStreamDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetNetworkGenericStreamDir, GetNetworkGenericStreamDir );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsProjectGetMapTexDir,		GetMapTexDir );

// MaxScript Function Publishing; Project Labels
MAXSCRIPT_FUNC_GAMEVIEW_INT( RsProjectGetNumLabels, GetNumLabels );
def_visible_primitive( RsProjectGetLabelTypeAt, "RsProjectGetLabelTypeAt" );
def_visible_primitive( RsProjectGetLabelNameAt, "RsProjectGetLabelNameAt" );
def_visible_primitive( RsProjectGetLabelName, "RsProjectGetLabelName" );

// MaxScript Function Publishing; Project Targets
MAXSCRIPT_FUNC_GAMEVIEW_INT( RsProjectGetNumTargets,		GetNumTargets );
def_visible_primitive( RsProjectGetTargetKey, "RsProjectGetTargetKey" );
def_visible_primitive( RsProjectGetTargetEnabled, "RsProjectGetTargetEnabled" );
def_visible_primitive( RsProjectSetTargetEnabled, "RsProjectSetTargetEnabled" );
def_visible_primitive( RsProjectGetTargetDir, "RsProjectGetTargetDir" );
def_visible_primitive( RsProjectContentSave, "RsProjectContentSave");

// Maxscript Function Publishing; Perforce Information
MAXSCRIPT_FUNC_GAMEVIEW_BOOL( RsProjectGetPerforceIntegration, GetPerforceIntegration );
def_visible_primitive( RsProjectSetPerforceIntegration, "RsProjectSetPerforceIntegration" );
MAXSCRIPT_FUNC_GAMEVIEW_INT( RsProjectGetNumPerforces,		GetNumPerforces );
def_visible_primitive( RsProjectGetPerforceInfo, "RsProjectGetPerforceInfo" );

// MaxScript Function Publishing: Project Content Tree
def_visible_primitive( RsProjectContentReload, "RsProjectContentReload" );
def_visible_primitive( RsProjectContentRoot, "RsProjectContentRoot" );
def_visible_primitive( RsProjectContentFind, "RsProjectContentFind" );
def_visible_primitive( RsProjectContentFindAll, "RsProjectContentFindAll" );

// MaxScript Function Publishing: User Information
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsUserGetUserName,			GetUserName );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsUserGetEmailAddress,			GetUserEmailAddress );
MAXSCRIPT_FUNC_GAMEVIEW_STRING( RsUserGetStudio,			GetStudio );
MAXSCRIPT_FUNC_GAMEVIEW_FLAG( RsUserGetUserFlags,			GetUserFlags );
def_visible_primitive( RsUserGetFriendlyName, "RsUserGetFriendlyName" );
def_visible_primitive( RsUserIsSuperUser, "RsUserIsSuperUser");
//---------------------------------------------------------------------------

Value*
RsConfigQuery_cf( Value** arg_list, int count )
{
	check_arg_count( RsConfigQuery, 1, count );
	type_check( arg_list[0], String, "XPath configuration data expression." );

	char* pExpr = arg_list[0]->to_string( );
	std::string sExpr( pExpr );

	ConfigModelData data;
	if ( gs_CoreView.query( sExpr.c_str(), data ) )
	{
		switch ( data.GetType() )
		{
		case ConfigModelData::CONFIGMODEL_DATA_BOOL:
			{
				bool bv;
				data.GetValue( bv );
				return ( bv ? &true_value : &false_value );
			}
			break;
		case ConfigModelData::CONFIGMODEL_DATA_INT:
			{
				int nv;
				data.GetValue( nv );
				return ( Integer::intern( nv ) );
			}
			break;
		case ConfigModelData::CONFIGMODEL_DATA_FLOAT:
			{
				double fv;
				data.GetValue( fv );
				return ( Float::intern( fv ) );
			}
			break;
		case ConfigModelData::CONFIGMODEL_DATA_STRING:
			{
				TCHAR sv[_MAX_PATH];
				data.GetValue( sv, _MAX_PATH );
				return_protected( new String( sv ) );
			}
			break;
		case ConfigModelData::CONFIGMODEL_DATA_EMPTY:
		default:
			// Break out and return 'undefined'.
			break;
		}
	}

	return ( &undefined );
}

Value*
RsProjectGetLabelTypeAt_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectGetLabelTypeAt, 1, count );
	type_check( arg_list[0], Integer, "Label to return the type for." );

	int index = arg_list[0]->to_int( ) - 1;
	if ( ( index >= 0 ) && ( index < gs_GameView.GetNumLabels() ) )
	{
		char type[_MAX_PATH] = {0};
		bool result = gs_GameView.GetLabelTypeAt( index, type, _MAX_PATH );
		if ( result )
		{
			return_protected( new String( type ) );
		}
		else
		{
			return ( &undefined );
		}
	}

	return ( &undefined );
}

Value*
RsProjectGetLabelNameAt_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectGetLabelNameAt, 1, count );
	type_check( arg_list[0], Integer, "Label to return the name for." );

	int index = arg_list[0]->to_int( ) - 1;
	if ( ( index >= 0 ) && ( index < gs_GameView.GetNumLabels() ) )
	{
		char name[_MAX_PATH] = {0};
		bool result = gs_GameView.GetLabelNameAt( index, name, _MAX_PATH );
		if ( result )
		{
			return_protected( new String( name ) );
		}
		else
		{
			return ( &undefined );
		}
	}

	return ( &undefined );
}

Value*
RsProjectGetLabelName_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectGetLabel, 1, count );
	type_check( arg_list[0], String, "Label type to return the name for." );

	const char* type = arg_list[0]->to_string();
	char name[_MAX_PATH] = {0};
	bool result = gs_GameView.GetLabelName( type, name, _MAX_PATH );
	if ( result )
	{
		return_protected( new String( name ) );
	}

	return ( &undefined );
}

Value*
RsProjectGetTargetKey_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectGetTargetKey, 1, count );
	type_check( arg_list[0], Integer, "Target index to return key for." );

	int index = arg_list[0]->to_int( );
	if ( ( index >= 0 ) && ( index < gs_GameView.GetNumTargets() ) )
	{
		char target[_MAX_PATH] = {0};
		bool result = gs_GameView.GetTargetKey( index, target, _MAX_PATH );
		if ( result )
		{
			return_protected( new String( target ) );
		}
		else
		{
			return ( &undefined );
		}
	}

	return ( &undefined );
}

Value*
RsProjectGetTargetEnabled_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectGetTargetEnabled, 1, count );
	type_check( arg_list[0], String, "Target key to return enabled flag for." );

	char* pPlatform = arg_list[0]->to_string( );
	bool result = gs_GameView.GetTargetEnabled( pPlatform );

	return ( result ? &true_value : &false_value );
}

Value*
RsProjectSetTargetEnabled_cf( Value** arg_list, int count )
{	
	check_arg_count( RsProjectSetTargetEnabled, 2, count );
	type_check( arg_list[0], String, "Target key to return enabled flag for." );
	type_check( arg_list[1], Boolean, "Enabled flag." );

	char* pPlatform = arg_list[0]->to_string( );
	bool enabled = arg_list[1]->to_bool( );
	bool result = gs_GameView.SetTargetEnabled( pPlatform, enabled );

	return ( enabled ? &true_value : &false_value );
}

Value*
RsProjectGetTargetDir_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectGetTargetDir, 1, count );
	type_check( arg_list[0], String, "Target key to return directory for." );

	char path[_MAX_PATH] = {0};
	char* pPlatform = arg_list[0]->to_string( );
	bool result = gs_GameView.GetTargetDir( pPlatform, path, _MAX_PATH );

	if ( result )
	{
		return_protected( new String( path ) );
	}
	return ( &undefined );
}

Value*
RsProjectSetPerforceIntegration_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectSetPerforceIntegration, 1, count );
	type_check( arg_list[0], Boolean, "Enable flag for toolchain Perforce integration." );

	BOOL enabled = arg_list[0]->to_bool( );
	gs_GameView.SetPerforceIntegration( enabled ? true : false );

	return ( &ok );
}

Value*
RsProjectGetPerforceInfo_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectGetPerforceInfo, 1, count );
	type_check( arg_list[0], Integer, "Perforce index to return information for." );

	int index = arg_list[0]->to_int( );
	if ( ( index >= 0 ) && ( index < gs_GameView.GetNumPerforces() ) )
	{
		PerforceInfo* p4Info = new PerforceInfo();
		bool result = gs_GameView.GetPerforceInfo( index, *p4Info );
		if ( result )
		{
			PerforceInfoValue* pValue = new PerforceInfoValue( p4Info );
			delete p4Info;
			return_protected( pValue );
		}
		delete p4Info;
	}
	return ( &undefined );
}

//PURPOSE
// The RsProjectContentReload MaxScript function forces a reload of the 
// content tree and returns the root node.
//
//EXAMPLE
// RsProjectContentReload() => ContentGroup( "root", ... )
//
Value*
RsProjectContentReload_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectContentReload, 0, count );
	const ContentModel& contentModel = gs_GameView.ReloadContentModel( );
	const ContentNodeGroup& groupRoot = contentModel.GetRoot( );
	Value* pGroupRoot = new ContentGroupValue( const_cast<ContentNodeGroup*>( &groupRoot ) );

	return_protected( pGroupRoot );
}

//PURPOSE
// The RsProjectContentRoot MaxScript function returns the root node of the
// content tree.
//
//EXAMPLE
// RsProjectContentRoot() => ContentGroup( "root", ... )
//
Value*
RsProjectContentRoot_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectContentRoot, 0, count );
	const ContentModel& contentModel = gs_GameView.GetContentModel( );
	const ContentNodeGroup& groupRoot = contentModel.GetRoot( );
	Value* pGroupRoot = new ContentGroupValue( const_cast<ContentNodeGroup*>( &groupRoot ) );

	return_protected( pGroupRoot );
}

//PURPOSE
// The RsProjectContentFind MaxScript function will search for content nodes
// from the root of the content tree.
//
//EXAMPLE
// RsProjectContentFind "name" [type:"typename"]
//
Value*
RsProjectContentFind_cf( Value** arg_list, int count )
{
	check_arg_count_with_keys( "RsProjectContentFind", 1, count );
	type_check( arg_list[0], String, "RsProjectContentFind \"name\" [type:\"typename\"]" );
	char* pName = arg_list[0]->to_string();
	Value* pType = key_arg_or_default( type, &undefined );

	if ( pType != &undefined )
		type_check( pType, String, "RsProjectContentFind \"name\" [type:\"typename\"]" );
		
	std::string name( pName );
	std::string type( pType != &undefined ? pType->to_string() : "" );

	ContentModel& contentModel = gs_GameView.GetContentModel( );
	ContentNodeGroup& group = contentModel.GetRoot( );
	ContentNode* pNode = group.FindFirst( name, type );
		
	if ( pNode )
		return_protected( ContentValueFactory::CreateNode( pNode ) );
	return ( &undefined );
}

//PURPOSE
// The RsProjectContentFindAll MaxScript function will search for all content
// nodes from the root of the content tree.  It will return an Array of all
// nodes of the specified type.
//
//EXAMPLE
// RsProjectContentFindAll type:"typename"
//
Value*
RsProjectContentFindAll_cf( Value** arg_list, int count )
{
	Value* pType = key_arg( type );
	type_check( pType, String, "RsProjectContentFindAll type:\"typename\"" );

	std::string type( pType->to_string() );
	ContentModel& contentModel = gs_GameView.GetContentModel( );
	ContentNodeGroup& group = contentModel.GetRoot( );
	std::vector<ContentNode*> nodes;

	group.FindAll( type, nodes );

	Array* pArray = new Array( (int)nodes.size() );
	for ( std::vector<ContentNode*>::const_iterator it = nodes.begin();
		  it != nodes.end();
		  ++it )
	{
		// Use the factory to create the desired MaxScript Value content node.
		Value* pNodeValue = ContentValueFactory::CreateNode( *it );
		pArray->append( pNodeValue );
	}
	return_protected( pArray );
}

//PURPOSE
// RsProjectContentSave 
// Will flush out the settings
//
//EXAMPLE
// RsProjectContentSave()
//
Value*
RsProjectContentSave_cf( Value** arg_list, int count )
{
	check_arg_count( RsProjectContentSave, 0, count );
	return gs_GameView.Save()?&true_value:&false_value;
}

Value*
RsUserGetFriendlyName_cf( Value** arg_list, int count )
{
	unsigned int flags = 0;
	gs_GameView.GetUserFlags( flags );
	unsigned int inversePseudoMask = gs_GameView.GetUserPseudoTypeMask() ^ 0xFFFFFFFF;
	unsigned int maskedFileType = flags & inversePseudoMask;
	for (int i = 0; i < gs_GameView.GetNumUserTypes(); i++)
	{
		configParser::UserType userType;
		gs_GameView.GetUserType(i, userType);
		if (userType.GetIsPseudo())
			continue;
		unsigned int maskedComboType = userType.GetMask() & inversePseudoMask;

		if (maskedComboType == maskedFileType)
		{
			return_protected( new String( userType.GetFriendlyName() ) )
		}
	}

	return ( &undefined );
}

Value*
RsUserIsSuperUser_cf( Value** arg_list, int count )
{
	check_arg_count( "RsUserIsSuperUser(void)", 0, count );
	unsigned int flags = 0;
	gs_GameView.GetUserFlags( flags );

	return ( flags&(1<<15) ? &true_value : &false_value );
}
