//
// filename:	ContentMapValue.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		19 February 2010
// description:	
//

// Local headers
#include "ContentMapValues.h"
#include "ContentValueFactory.h"

// --- ContentMapValue Instance ---------------------------------------------

local_visible_class_instance( ContentMapValue, "ContentMap" )

Value*
ContentMapValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentMapValue Methods ---------------------------------------------

ContentMapValue::ContentMapValue( const configParser::ContentNodeMap* pNode )
	: m_pNode( pNode )
{
}

void
ContentMapValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentMap ( *no node* )" );
		return;
	}

	s->printf( "ContentMap(\n" );
	s->printf( "\tname:        \"%s\",\n", m_pNode->GetName() );
	TCHAR path[_MAX_PATH] = {0};
	m_pNode->GetAbsolutePath(path, _MAX_PATH);
	s->printf( "\tpath:        \"%s\",\n", path);
	s->printf( "\tprefix:      \"%s\",\n", m_pNode->GetPrefix() );
	s->printf( "\tdefinitions: \"%s\",\n", m_pNode->GetDefinitions() ? "true" : "false" );
	s->printf( "\tinstances:   \"%s\",\n", m_pNode->GetInstances() ? "true" : "false" );
	s->printf( "\tdata:        \"%s\",\n", m_pNode->GetData() ? "true" : "false" );
	s->printf( "\tpedantic:    \"%s\",\n", m_pNode->GetPedantic() ? "true" : "false" );
	s->printf( "\tseed:		   \"%u\",\n", m_pNode->GetSeed() );
	s->printf( "\tmaptype:     \"%s\",\n", get_maptype_str(m_pNode->GetMapType()).c_str());
	s->printf( "\tinputs:      [%d]\n", m_pNode->GetNumInputs() );
	s->printf( "\toutput:      [%d]\n", m_pNode->GetNumOutputs() );
	s->printf( "\tchildren:    [%d]\n", m_pNode->GetNumChildren() ); 
	s->printf( ")\n" );
}

void
ContentMapValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentMapValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
	if ( 0 == _stricmp( arg->string, "path" ) )
	{
		TCHAR path[_MAX_PATH] = {0};
		m_pNode->GetAbsolutePath(path, _MAX_PATH);
		return_protected( new String( path ) );
	}
	if ( 0 == _stricmp( arg->string, "prefix" ) )
		return_protected( new String( m_pNode->GetPrefix() ) );
	if ( 0 == _stricmp( arg->string, "definitions" ) )
		return ( m_pNode->GetDefinitions() ? &true_value : &false_value );
	if ( 0 == _stricmp( arg->string, "instances" ) )
		return ( m_pNode->GetInstances() ? &true_value : &false_value );
	if ( 0 == _stricmp( arg->string, "data" ) )
		return ( m_pNode->GetData() ? &true_value : &false_value );
	if ( 0 == _stricmp( arg->string, "pedantic" ) )
		return ( m_pNode->GetPedantic() ? &true_value : &false_value );
	if ( 0 == _stricmp( arg->string, "seed" ) )
		return ( Integer64::intern( m_pNode->GetSeed() ) );
	if ( 0 == _stricmp( arg->string, "maptype" ) )
	{
		configParser::ContentNodeMap::eMapType mapType = m_pNode->GetMapType( );
		std::string mapTypeStr = get_maptype_str(mapType);
		return_protected( Name::intern( mapTypeStr.c_str() ));
	}
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			  it != m_pNode->EndInputs();
			  ++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}
	if ( 0 == _stricmp( arg->string, "outputs" ) )
	{
		Array* pOutputs = new Array( (int)m_pNode->GetNumOutputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginOutputs();
			it != m_pNode->EndOutputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pOutputs->append( pNodeValue );
		}
		return_protected( pOutputs );
	}

	return ( Value::get_property( arg_list, count ) );
}

Value*
ContentMapValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}

// --- ContentMapZipValue Instance ------------------------------------------

local_visible_class_instance( ContentMapZipValue, "ContentMapZip" )

Value*
ContentMapZipValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentMapZipValue Methods -------------------------------------------

ContentMapZipValue::ContentMapZipValue( const configParser::ContentNodeMapZip* pNode )
	: m_pNode( pNode )
{
}

void
ContentMapZipValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentMapZip ( *no node* )" );
		return;
	}

	s->printf( "ContentMapZip(\n" );
	s->printf( "\tname:     \"%s\",\n", m_pNode->GetName() );
	TCHAR path[_MAX_PATH] = {0};
	m_pNode->GetAbsolutePath(path, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", path);
	s->printf( "\tviaboundsprocessor: \"%s\",\n", m_pNode->GetViaBoundsProcessor() ? "true" : "false" );
	s->printf( "\tinputs:   [%d]\n", m_pNode->GetNumInputs() );
	s->printf( "\toutput:   [%d]\n", m_pNode->GetNumOutputs() );
	s->printf( "\tchildren: [%d]\n", m_pNode->GetNumChildren() ); 
	s->printf( ")\n" );
}

void
ContentMapZipValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentMapZipValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
	if ( 0 == _stricmp( arg->string, "path" ) )
	{
		TCHAR path[_MAX_PATH] = {0};
		m_pNode->GetAbsolutePath(path, _MAX_PATH);
		return_protected( new String( path ) );
	}
	if ( 0 == _stricmp( arg->string, "extension" ) )
		return_protected( new String( m_pNode->GetExtension() ) );
	if ( 0 == _stricmp( arg->string, "filename" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetFilename(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "compress" ) )
		return ( m_pNode->GetCompress() ? &true_value : &false_value );	
	if ( 0 == _stricmp( arg->string, "viaboundsprocessor" ) )
		return ( m_pNode->GetViaBoundsProcessor() ? &true_value : &false_value );	
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
	if ( arg == n_children )
	{
		Array* pChildren = new Array( (int)m_pNode->GetNumChildren() );
		for ( configParser::ContentNodeGroup::NodeContainerConstIter it = m_pNode->BeginChildren();
			it != m_pNode->EndChildren();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pChildren->append( pNodeValue );
		}
		return_protected( pChildren );
	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			it != m_pNode->EndInputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}
	if ( 0 == _stricmp( arg->string, "outputs" ) )
	{
		Array* pOutputs = new Array( (int)m_pNode->GetNumOutputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginOutputs();
			it != m_pNode->EndOutputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pOutputs->append( pNodeValue );
		}
		return_protected( pOutputs );
	}

	return ( Value::get_property( arg_list, count ) );
}

Value*
ContentMapZipValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}


// --- ContentMapProcessedZipValue Instance ------------------------------------------

local_visible_class_instance( ContentMapProcessedZipValue, "ContentMapProcessedZip" )

Value*
ContentMapProcessedZipValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentMapProcessedZipValue Methods -------------------------------------------

ContentMapProcessedZipValue::ContentMapProcessedZipValue( const configParser::ContentNodeMapProcessedZip* pNode )
: m_pNode( pNode )
{
}

void
ContentMapProcessedZipValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentMapProcessedZip ( *no node* )" );
		return;
	}

	s->printf( "ContentMapZip(\n" );
	s->printf( "\tname:     \"%s\",\n", m_pNode->GetName() );
	TCHAR path[_MAX_PATH] = {0};
	m_pNode->GetAbsolutePath(path, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", path);
	s->printf( "\tinputs:   [%d]\n", m_pNode->GetNumInputs() );
	s->printf( "\toutput:   [%d]\n", m_pNode->GetNumOutputs() );
	s->printf( ")\n" );
}

void
ContentMapProcessedZipValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentMapProcessedZipValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
	if ( 0 == _stricmp( arg->string, "path" ) )
	{
		TCHAR path[_MAX_PATH] = {0};
		m_pNode->GetAbsolutePath(path, _MAX_PATH);
		return_protected( new String( path ) );
	}
	if ( 0 == _stricmp( arg->string, "extension" ) )
		return_protected( new String( m_pNode->GetExtension() ) );
	if ( 0 == _stricmp( arg->string, "filename" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetFilename(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "compress" ) )
		return ( m_pNode->GetCompress() ? &true_value : &false_value );	
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
	if ( arg == n_children )
	{
		Array* pChildren = new Array( (int)m_pNode->GetNumChildren() );
		for ( configParser::ContentNodeGroup::NodeContainerConstIter it = m_pNode->BeginChildren();
			it != m_pNode->EndChildren();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pChildren->append( pNodeValue );
		}
		return_protected( pChildren );
	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			it != m_pNode->EndInputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}
	if ( 0 == _stricmp( arg->string, "outputs" ) )
	{
		Array* pOutputs = new Array( (int)m_pNode->GetNumOutputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginOutputs();
			it != m_pNode->EndOutputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pOutputs->append( pNodeValue );
		}
		return_protected( pOutputs );
	}

	return ( Value::get_property( arg_list, count ) );
}

Value*
ContentMapProcessedZipValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}

/*static*/std::string ContentMapValue::get_maptype_str(configParser::ContentNodeMap::eMapType mapType)
{
	switch ( mapType )
	{
	case configParser::ContentNodeMap::PROPS:
		return "props";
	case configParser::ContentNodeMap::INTERIOR:
		return "interior";
	case configParser::ContentNodeMap::CONTAINER:
		return "container";
	case configParser::ContentNodeMap::CONTAINER_PROPS:
		return "container_props";
	case configParser::ContentNodeMap::CONTAINER_LOD:
		return "container_lod";
	case configParser::ContentNodeMap::CONTAINER_OCCL:
		return "container_occlusion";
	case configParser::ContentNodeMap::ASSET_COMBINE:
		return "asset_combine";
	case configParser::ContentNodeMap::UNKNOWN:
	default:
		return "unknown";
	}
}

// ContentMapValues.cpp
