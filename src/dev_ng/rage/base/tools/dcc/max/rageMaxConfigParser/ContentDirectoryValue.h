#ifndef __RAGEMAXCONFIGPARSER_CONTENTDIRECTORYVALUE_H__
#define __RAGEMAXCONFIGPARSER_CONTENTDIRECTORYVALUE_H__

//
// filename:	ContentDirectoryValue.h
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		27 September 2012
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/name.h>
#include "maxscript/compiler/parser.h"
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include "maxscript/macros/local_class.h"
//#include "maxscrpt/funcs.h"
#include "maxscript/macros/local_abstract_generic_functions.h"
#include "maxscript/macros/local_implementations.h"

// configParser headers
#include "configParser/contentNode.h"
#include "configParser/contentNodeCore.h"

//declare_local_generic_class
local_applyable_class (ContentDirectoryValue)

//PURPOSE
//
class ContentDirectoryValue : public Value
{
public:
	ContentDirectoryValue( const configParser::ContentNodeDirectory* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentDirectoryValue ) ); }
					classof_methods( ContentDirectoryValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeDirectory* m_pNode;
};

#endif // __RAGEMAXCONFIGPARSER_CONTENTDEPENDENCYDIRECTORYVALUE_H__
