//
// filename:	ContentGroupValue.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		19 February 2010
// description:	
//

// Local headers
#include "ContentGroupValue.h"
#include "ContentValueFactory.h"

// --- ContentGroupValue Instance -------------------------------------------

local_visible_class_instance( ContentGroupValue, "ContentGroup" )

Value*
ContentGroupValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentGroupValue Methods --------------------------------------------

ContentGroupValue::ContentGroupValue( const configParser::ContentNodeGroup* pNode )
	: m_pNode( pNode )
{
}

void
ContentGroupValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentGroup ( *no node* )" );
		return;
	}

	s->printf( "ContentGroup(\n" );
	s->printf( "\tname: \"%s\",\n", m_pNode->GetName() );
	TCHAR path[_MAX_PATH] = {0};
	m_pNode->GetAbsolutePath(path, _MAX_PATH);
	s->printf( "\tpath: \"%s\",\n", path );
	s->printf( "\tinputs: [%d]\n", m_pNode->GetNumInputs() );
	s->printf( "\toutput: [%d]\n", m_pNode->GetNumOutputs() );
	s->printf( "\tchildren: [%d]\n", m_pNode->GetNumChildren() ); 
	s->printf( ")\n" );
}

void
ContentGroupValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentGroupValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
	if ( 0 == _stricmp( arg->string, "path" ) )
	{
		TCHAR path[_MAX_PATH] = {0};
		m_pNode->GetAbsolutePath(path, _MAX_PATH);
		return_protected( new String( path ) );
	}
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
	if ( arg == n_children )
	{
		Array* pChildren = new Array( (int)m_pNode->GetNumChildren() );
		for ( configParser::ContentNodeGroup::NodeContainerConstIter it = m_pNode->BeginChildren();
			  it != m_pNode->EndChildren();
			  ++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pChildren->append( pNodeValue );
		}
		return_protected( pChildren );
	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			  it != m_pNode->EndInputs();
			  ++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}
	if ( 0 == _stricmp( arg->string, "outputs" ) )
	{
		Array* pOutputs = new Array( (int)m_pNode->GetNumOutputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginOutputs();
			it != m_pNode->EndOutputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pOutputs->append( pNodeValue );
		}
		return_protected( pOutputs );
	}

	return Value::get_property( arg_list, count );
}

Value*
ContentGroupValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}

// ContentGroupValue.cpp
