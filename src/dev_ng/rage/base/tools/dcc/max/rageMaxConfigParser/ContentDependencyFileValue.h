#ifndef __RAGEMAXCONFIGPARSER_CONTENTDEPENDENCYFILEVALUE_H__
#define __RAGEMAXCONFIGPARSER_CONTENTDEPENDENCYFILEVALUE_H__

//
// filename:	ContentDependencyFileValue.h
// author:		Jonny Rivers <jonathan.rivers@rockstarnorth.com>
// date:		29 November 2011
// description:	
//

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

// --- Includes -----------------------------------------------------------------

// 3dsmax SDK headers
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/foundation/name.h>
#include "maxscript/compiler/parser.h"
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include "maxscript/macros/local_class.h"
//#include "maxscrpt/funcs.h"
#include "maxscript/macros/local_abstract_generic_functions.h"
#include "maxscript/macros/local_implementations.h"

// configParser headers
#include "configParser/contentNodeDependencyFile.h"

//declare_local_generic_class
local_applyable_class (ContentDependencyFileValue)

//PURPOSE
//
class ContentDependencyFileValue : public Value
{
public:
	ContentDependencyFileValue( const configParser::ContentNodeDependencyFile* pNode );

	ValueMetaClass* local_base_class( ) { return ( class_tag( ContentDependencyFileValue ) ); }
					classof_methods( ContentDependencyFileValue, Value );
	void			collect( ) { delete this; }
	void			sprin1( CharStream* s );
	void			gc_trace( );
	BOOL			_is_collection( ) { return ( 0 ); }

	Value*			get_property( Value** arg_list, int count );
	Value*			set_property( Value** arg_list, int count );

private:
	const configParser::ContentNodeDependencyFile* m_pNode;
};

#endif // __RAGEMAXCONFIGPARSER_CONTENTDEPENDENCYFILEVALUE_H__
