//
// filename:	ContentUnknownValue.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		22 February 2010
// description:	
//

// Local headers
#include "ContentUnknownValue.h"
#include "ContentValueFactory.h"

// --- ContentGroupValue Instance -------------------------------------------

local_visible_class_instance( ContentUnknownValue, "ContentUnknown" )

Value*
ContentUnknownValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentGroupValue Methods --------------------------------------------

ContentUnknownValue::ContentUnknownValue( const configParser::ContentNodeUnknown* pNode )
	: m_pNode( pNode )
{
}

void
ContentUnknownValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentUnknown ( *no node* )" );
		return;
	}

	s->printf( "ContentUnknown(\n" );
	s->printf( "\tname: \"%s\",\n", m_pNode->GetName() );
	s->printf( "\ttype: \"%s\",\n", m_pNode->GetType() );
	s->printf( ")\n" );
}

void
ContentUnknownValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentUnknownValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			  it != m_pNode->EndInputs();
			  ++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}
	if ( 0 == _stricmp( arg->string, "outputs" ) )
	{
		Array* pOutputs = new Array( (int)m_pNode->GetNumOutputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginOutputs();
			it != m_pNode->EndOutputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pOutputs->append( pNodeValue );
		}
		return_protected( pOutputs );
	}

	return Value::get_property( arg_list, count );
}

Value*
ContentUnknownValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}

// ContentGroupValue.cpp
