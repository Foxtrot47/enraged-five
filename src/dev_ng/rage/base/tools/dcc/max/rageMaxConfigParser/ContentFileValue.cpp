//
// filename:	ContentFileValue.cpp
// author:		David Muir <david.muir@rockstarnorth.com>
// date:		23 February 2010
// description:	
//

// Local headers
#include "ContentFileValue.h"
#include "ContentValueFactory.h"

// --- ContentGroupValue Instance -------------------------------------------

local_visible_class_instance( ContentFileValue, "ContentFile" )

Value*
ContentFileValueClass::apply( Value** arg_list, int count, CallContext* cc )
{
	throw RuntimeError( "Content tree nodes cannot be created in MaxScript." );
	return ( &undefined );
}

// --- ContentGroupValue Methods --------------------------------------------

ContentFileValue::ContentFileValue( const configParser::ContentNodeFile* pNode )
	: m_pNode( pNode )
{
}

void
ContentFileValue::sprin1( CharStream* s )
{
	assert( m_pNode );
	if ( !m_pNode )
	{
		s->puts( "ContentFile ( *no node* )" );
		return;
	}

	s->printf( "ContentFile(\n" );
	s->printf( "\tname:      \"%s\",\n", m_pNode->GetName());
	TCHAR path[_MAX_PATH] = {0};
	m_pNode->GetAbsolutePath(path, _MAX_PATH);
	s->printf( "\tpath:      \"%s\",\n", path);
	s->printf( "\textension: \"%s\",\n", m_pNode->GetExtension());
	TCHAR fn[_MAX_PATH] = {0};
	m_pNode->GetFilename(fn, _MAX_PATH);
	s->printf( "\tfilename:  \"%s\",\n", fn );
	s->printf( "\tinputs:    [%d]\n", m_pNode->GetNumInputs() );
	s->printf( "\toutput:    [%d]\n", m_pNode->GetNumOutputs() );
	s->printf( ")\n" );
}

void
ContentFileValue::gc_trace( )
{
	Value::gc_trace( );
}

Value*
ContentFileValue::get_property( Value** arg_list, int count )
{
	Name* arg = (Name*)arg_list[0];

	if ( arg == n_name )
		return_protected( new String( m_pNode->GetName() ) );
	if ( 0 == _stricmp( arg->string, "type" ) )
		return_protected( new String( m_pNode->GetType() ) );
 	if ( 0 == _stricmp( arg->string, "path" ) )
	{
		TCHAR path[_MAX_PATH] = {0};
		m_pNode->GetAbsolutePath(path, _MAX_PATH);
 		return_protected( new String( path ) );
	}
	if ( 0 == _stricmp( arg->string, "extension" ) )
		return_protected( new String( m_pNode->GetExtension() ) );
	if ( 0 == _stricmp( arg->string, "filename" ) )
	{
		TCHAR fn[_MAX_PATH] = {0};
		m_pNode->GetFilename(fn, _MAX_PATH);
		return_protected( new String( fn ) );
	}
	if ( 0 == _stricmp( arg->string, "parent" ) )
	{
		Value* pNodeValue = ContentValueFactory::CreateNode( m_pNode->GetParent() );
		return_protected( pNodeValue );
	}
	if ( 0 == _stricmp( arg->string, "inputs" ) )
	{
		Array* pInputs = new Array( (int)m_pNode->GetNumInputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginInputs();
			  it != m_pNode->EndInputs();
			  ++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pInputs->append( pNodeValue );
		}
		return_protected( pInputs );
	}
	if ( 0 == _stricmp( arg->string, "outputs" ) )
	{
		Array* pOutputs = new Array( (int)m_pNode->GetNumOutputs() );
		for ( configParser::ContentNode::NodeContainerConstIter it = m_pNode->BeginOutputs();
			it != m_pNode->EndOutputs();
			++it )
		{
			// Use the factory to create the desired MaxScript Value content node.
			Value* pNodeValue = ContentValueFactory::CreateNode( *it );
			pOutputs->append( pNodeValue );
		}
		return_protected( pOutputs );
	}

	return Value::get_property( arg_list, count );
}

Value*
ContentFileValue::set_property( Value** arg_list, int count )
{
	throw RuntimeError( "The content tree is read-only.", this );
	return ( &undefined );
}

// ContentFileValue.cpp
