Project rageMaxDataStore
Template max_configurations.xml

OutputFileName rageMaxDataStore.dlu
ConfigurationType dll

Define MAXDATASTORE_EXPORTS

EmbeddedLibAll comctl32.lib core.lib geom.lib gfx.lib mesh.lib maxutil.lib maxscrpt.lib paramblk2.lib menus.lib gup.lib

ForceInclude ForceInclude.h

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\tools\libs\rageDataStoreAttributes

Files {
	Folder "Header Files" { 
		AttrTVNode.h
		Configuration.h
		CustAttribData.h
		DataInstance.h
		DialogClass.h
		EditorUtility.h
		IAttrib.h
		IConnection.h
		IFpMaxDataStore.h
		IMaxDataStore.h
		MaxDataMgr.h
		MaxDataStore.h
		Resource.h
		RightClickMenu.h
		RuleListDialog.h
		Rules.h
	}
	Folder "Resource Files" {	
		dmaman48x48.bmp
		MaxDataStore.rc
		rockstar.bmp
	}
	Folder "Source Files" {
		AttrTVNode.cpp
		Config.cpp
		Configuration.cpp
		CustAttribData.cpp
		DataInstance.cpp
		DialogClass.cpp
		DllEntry.cpp
		EditorUtility.cpp
		IMaxDataStore.cpp
		MaxDataMgr.cpp
		MaxDataStore.cpp
		rageMaxDataStore.def
		RightClickMenu.cpp
		RuleListDialog.cpp
		Script.cpp
	}	
}