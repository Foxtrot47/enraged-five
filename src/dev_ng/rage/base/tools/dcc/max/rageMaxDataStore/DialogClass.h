//
//
//    Filename: DialogClass.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Class for doing standard dialog stuff. Created because MFC and Max have problems with each other
//
//
#ifndef INC_DIALOG_CLASS_H_
#define INC_DIALOG_CLASS_H_

#pragma warning(push)
#pragma warning(disable:4668)
#include <windows.h>
#pragma warning(pop)

// using namespace
using namespace rage;

//
//   Class Name: DialogClass
//  Description: Class for creating Dialogs. Created because MFC is shit and also it clashes with Max
//    Functions: 
//
//
class DialogClass
{
public:
	DialogClass(s32 resId) : m_resId(resId), m_hWnd(NULL) {}

	s32 DoModal(HINSTANCE hInstance, HWND hParent);
	virtual BOOL DlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {return FALSE;}

	virtual void OnInitDialog(){}
	virtual void OnCommand(s32 id, s32 notifyCode) {}
	virtual void OnCancel() {EndDialog(m_hWnd, 0);}
	virtual void OnOK() {EndDialog(m_hWnd, 1);}
	virtual void UpdateControls() {}
	virtual void UpdateData() {}

protected:
	s32 m_resId;
	HWND m_hWnd;

	// functions
	static INT_PTR CALLBACK InternalDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
};


#endif // INC_DIALOG_CLASS_H_
