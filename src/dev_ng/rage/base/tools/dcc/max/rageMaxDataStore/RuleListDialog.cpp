//
//
//    Filename: RuleListDialog.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Dialog for editing rules for attribute classes
//
//
#pragma warning (disable : 4786)
#pragma warning (disable : 4251)

#include "MaxDataMgr.h"
#include "RuleListDialog.h"

#ifdef MAX8
#define XREFOBJ_CLASS_ID_PART Class_ID(0x92aab38c,0)
#else
#define XREFOBJ_CLASS_ID_PART XREFOBJ_CLASS_ID
#endif

const char *SelectionRules::pRuleTypes[] = {
	"Object is of type",
	"Object is of super class",
	"Scene file is in directory",
	"Node name contains text",
	"XRef file is in directory",
	"XRef name contains text",
	"Object has child of type",
	"Object has modifier",
	"Object has animation"
};

const char *SelectionRules::pNotRuleTypes[] = {
	"Object is not of type",
	"Object is not of super class",
	"Scene file is not in directory",
	"Node name does not contains text",
	"XRef file is not in directory",
	"XRef name does not contains text",
	"Object does not have child of type",
	"Object does not have modifier",
	"Object does not have animation"
};

const char *SelectionRules::pRuleNames[] = {
	"ObjectType",
	"ObjectSuperClass",
	"InDirectory",
	"TextInName",
	"XRefInDirectory",
	"XRefTextInName",
	"ObjectChildType",
	"HasModifier",
	"HasAnimation"
};
// --- Rule --------------------------------------------------------------------------------------------------------

bool Rule::HasAnimation(INode *pNode)
{
	//check if this, or any of its children has animation on it
	if(pNode->IsAnimated() == TRUE)
	{
		return true;
	}

	return false;
}

bool Rule::IsValidObjectChildType(INode *pNode)
{
	int iNumChildren = pNode->NumberOfChildren();
	INode* pChild;
	Object *pObject;
	TSTR nameStr;

	for(int i=0;i<iNumChildren;i++)
	{
		pChild = pNode->GetChildNode(i);
		pObject = pChild->EvalWorldState(0).obj;
		pObject->GetClassName(nameStr);

		if(str == (const char *)nameStr)
		{
			return true;
		}
	}

	return false;
}

bool Rule::IsValidObjectType(ReferenceTarget *pRef)
{
	TSTR nameStr;
	pRef->GetClassName(nameStr);

	//seems to be an inconsistency here between the name of the class
	//reported by GetClassList and the name stored in the actual classes created
	if((strcmp(str.c_str(),"Bones") == 0) && (strcmp(nameStr,"Bone") == 0))
	{
		return true;
	}

	if((strcmp(str.c_str(),"Point") == 0) && (strcmp(nameStr,"PointHelper") == 0))
	{
		return true;
	}

	if((strcmp(str.c_str(),"Editable Poly") == 0) && (strcmp(nameStr,"PolyMeshObject") == 0))
	{
		return true;
	}

	//special case for lights as all lights are really of the class "Light"
	//with a subtype for omni, point and directional

	if((strcmp(nameStr,"Light") == 0))
	{	
		Interval valid;
		LightState ls;

		LightObject* p_lo = (LightObject*)pRef;

		p_lo->EvalLightState(0,valid,&ls);

		if((ls.type == OMNI_LGT) && (strcmp(str.c_str(),"Omni") == 0))
		{
			return true;
		}

		if((ls.type == SPOT_LGT) && (strcmp(str.c_str(),"Spot") == 0))
		{
			return true;
		}

		if((ls.type == DIRECT_LGT) && (strcmp(str.c_str(),"Direct") == 0))
		{
			return true;
		}

		if((ls.type == AMBIENT_LGT) && (strcmp(str.c_str(),"Ambient") == 0))
		{
			return true;
		}

		return false;
	}

	return (str == (const char *)nameStr);
}

bool Rule::HasModiferOfType(INode *pNode)
{
	Object* pRef = pNode->GetObjectRef();

	if(pRef->SuperClassID() == GEN_DERIVOB_CLASS_ID)
	{
		IDerivedObject* p_derived = (IDerivedObject*)pRef;

		int iNumMod = p_derived->NumModifiers();

		for(int i=0;i<iNumMod;i++)
		{
			if(strcmp(p_derived->GetModifier(i)->GetName(),str.c_str())==0)
			{
				return true;
			}
		}
	}

	return false;
}

bool Rule::IsValidObjectSuperClass(ReferenceTarget *pRef)
{
	TSTR nameStr;
	SClass_ID sc = pRef->SuperClassID();

	switch(sc)
	{
	case GEOMOBJECT_CLASS_ID:
		return (str == "Geometry");
	case SHAPE_CLASS_ID:
		return (str == "Shape");
	case CAMERA_CLASS_ID:
		return (str == "Camera");
	case LIGHT_CLASS_ID:
		return (str == "Light");
	case HELPER_CLASS_ID:
		return (str == "Helper");
	case SYSTEM_CLASS_ID:
		return (str == "System");
	case TEXMAP_CLASS_ID:
		return (str == "textureMap");
	}
	return false;
}

bool Rule::IsInDirectory(Interface *ip)
{
	if(ip == NULL)
		return false;
	TSTR path = ip->GetCurFilePath();

	return (_strnicmp((const char *)path, str.c_str(), str.length()) == 0);
}

bool Rule::NodeContainsString(INode *pNode)
{
	return (strstr(pNode->GetName(), str.c_str()) != NULL);
}

bool Rule::XRefIsInDirectory(INode *pNode)
{
	Object *pObj = pNode->GetObjectRef();

	if(pObj->SuperClassID()==SYSTEM_CLASS_ID && pObj->ClassID()==XREFOBJ_CLASS_ID_PART)
	{
#if (MAX_RELEASE >= 12000)
		MaxSDK::AssetManagement::AssetUser *pXRef = (MaxSDK::AssetManagement::AssetUser*)pObj;

		return (_strnicmp((const char *)pXRef->GetFileName(), str.c_str(), str.length()) == 0);
#else
		IXRefObject *pXRef = (IXRefObject*)pObj;

		return (_strnicmp((const char *)pXRef->GetFileName(), str.c_str(), str.length()) == 0);
#endif //MAX_RELEASE >= 12000
	}
	return false;
}

bool Rule::XRefNameContainsString(INode *pNode)
{
	Object *pObj = pNode->GetObjectRef();

	if(pObj->SuperClassID()==SYSTEM_CLASS_ID && pObj->ClassID()==XREFOBJ_CLASS_ID_PART)
	{
		IXRefObject *pXRef = (IXRefObject*)pObj;
		return (strstr((const char *)pXRef->GetObjName(), str.c_str()) != NULL);
	}
	return false;
}

// --- SelectionRules ----------------------------------------------------------------------------------------------

bool SelectionRules::IsValid(RefTargetHandle pRef)
{
	Interface *ip = GetCOREInterface();
	INode *pNode = dynamic_cast<INode*>(pRef);
	ReferenceTarget *pObject = pRef;
	if (NULL != pNode)
	{
		pObject = pNode->EvalWorldState(0).obj;
	}

	for(s32 i=0; i<int(m_rules.size()); i++)
	{
		Rule& rule = m_rules[i];
		switch(rule.type)
		{
		case OBJECT_TYPE:
			if(rule.IsValidObjectType(pObject) != rule.affirm)
				return false;
			break;
		case OBJECT_SUPERCLASS:
			if(rule.IsValidObjectSuperClass(pObject) != rule.affirm)
				return false;
			break;
		case IN_DIRECTORY:
			if(rule.IsInDirectory(ip) != rule.affirm)
				return false;
			break;
		case CONTAINS_TEXT:
			if(!pNode || rule.NodeContainsString(pNode) != rule.affirm)
				return false;
			break;
		case XREF_IN_DIRECTORY:
			if(!pNode || rule.XRefIsInDirectory(pNode) != rule.affirm)
				return false;
			break;
		case XREF_CONTAINS_TEXT:
			if(!pNode || rule.XRefNameContainsString(pNode) != rule.affirm)
				return false;
			break;
		case OBJECT_CHILD_TYPE:
			if(!pNode || rule.IsValidObjectChildType(pNode) != rule.affirm)
				return false;
			break;
		case OBJECT_HAS_MODIFIER:
			if(!pNode || rule.HasModiferOfType(pNode) != rule.affirm)
				return false;
			break;
		case OBJECT_HAS_ANIMATION:
			if(!pNode || rule.HasAnimation(pNode) != rule.affirm)
				return false;
			break;
		}
	}
	return true;
}

// --- ConstructRuleDialog -----------------------------------------------------------------------------------------

//
//        name: ConstructRuleDialog::OnInitDialog
// description: Called on initialisation of dialog. Sets up controls
//
void ConstructRuleDialog::OnInitDialog()
{
	m_hRuleCombo = GetDlgItem(m_hWnd, IDC_RULETYPE_COMBO);

	m_hModifierListCombo = GetDlgItem(m_hWnd, IDC_MODIFIERLIST_COMBO);
	m_hClassListCombo = GetDlgItem(m_hWnd, IDC_CLASSLIST_COMBO);
	m_hSClassListCombo = GetDlgItem(m_hWnd, IDC_SUPERCLASSLIST_COMBO);
	m_hClassLabel = GetDlgItem(m_hWnd, IDC_CLASS_STATIC);
	m_hSClassLabel = GetDlgItem(m_hWnd, IDC_SCLASS_STATIC);

	m_hDirLabel = GetDlgItem(m_hWnd, IDC_DIR_STATIC);
	m_hFileEdit = GetDlgItem(m_hWnd, IDC_FILE_EDIT);
	m_hFileButton = GetDlgItem(m_hWnd, IDC_FILE_BUTTON);

	m_hTextLabel = GetDlgItem(m_hWnd, IDC_TEXT_STATIC);
	m_hTextEdit = GetDlgItem(m_hWnd, IDC_TEXT_EDIT);

	InitSuperClassListCombo();
	InitModifierListCombo();
}

//
//        name: ConstructRuleDialog::OnCommand
// description: Called whenever user interacts with the UI eg presses button etc
//          in: id = id of control
//				notifyCode = notification code
//
void ConstructRuleDialog::OnCommand(s32 id, s32 notifyCode)
{
	switch(id)
	{
	case IDC_SUPERCLASSLIST_COMBO:
		if(notifyCode == CBN_SELCHANGE)
			InitClassListCombo();
		break;
	case IDC_RULETYPE_COMBO:
		if(notifyCode == CBN_SELCHANGE)
			EnableControls();
		break;
	case IDC_FILE_BUTTON:
		FindDir();
		break;
	case IDC_NEGATIVE_CHECK:
		{
			s32 sel = SendMessage(m_hRuleCombo, CB_GETCURSEL, 0, 0);
			InitRuleTypeCombo();
			SendMessage(m_hRuleCombo, CB_SETCURSEL, sel, 0);
		}
		break;
	}
}

//
//        name: ConstructRuleDialog::OnOK
// description: Check if dialog has been filled out correctly
//
void ConstructRuleDialog::OnOK()
{
	if(m_rule.str.empty())
	{
		switch(m_rule.type)
		{
		case OBJECT_TYPE:
			MessageBox(m_hWnd, "Rule requires an object class to be selected", "Attribute classes setup", MB_OK);
			break;
		case IN_DIRECTORY:
		case XREF_IN_DIRECTORY:
			MessageBox(m_hWnd, "Rule requires a directory name", "Attribute classes setup", MB_OK);
			break;
		case CONTAINS_TEXT:
		case XREF_CONTAINS_TEXT:
			MessageBox(m_hWnd, "Rule requires some text", "Attribute classes setup", MB_OK);
			break;
		case OBJECT_CHILD_TYPE:
			MessageBox(m_hWnd, "Rule requires an object class to be selected", "Attribute classes setup", MB_OK);
			break;
		case OBJECT_HAS_MODIFIER:
			MessageBox(m_hWnd, "Rule requires a modifier to be selected", "Attribute classes setup", MB_OK);
			break;
		case OBJECT_HAS_ANIMATION:
			DialogClass::OnOK();
			break;
		}
		return;
	}
	DialogClass::OnOK();
}

//
//        name: ConstructRuleDialog::UpdateControls
// description: Updates controls with data
//
void ConstructRuleDialog::UpdateControls()
{
	// do negative check first then construct rules combo box and then select an item from the 
	// rules combo box. This will not work in any other order
	CheckDlgButton(m_hWnd, IDC_NEGATIVE_CHECK, static_cast<UINT>(!m_rule.affirm));
	InitRuleTypeCombo();
	SendMessage(m_hRuleCombo, CB_SETCURSEL, m_rule.type, 0);

	switch(m_rule.type)
	{
	case OBJECT_TYPE:
	case OBJECT_SUPERCLASS:
	case OBJECT_CHILD_TYPE:
		// work out which super class to use
		{
			DllDir& dllDir = m_ip->GetDllDir();
			ClassDirectory& classDir = dllDir.ClassDir();

			for(s32 i=0; i<6; i++)
			{
				SClass_ID superClassId = SendMessage(m_hSClassListCombo, CB_GETITEMDATA, i, 0);
				SubClassList* pSubClassList = classDir.GetClassList(superClassId);
				
				if(pSubClassList == NULL)
					continue;
				if(pSubClassList->FindClass(m_rule.str.c_str()) != -1)
				{
					SendMessage(m_hSClassListCombo, CB_SETCURSEL, i, 0);
					break;
				}
			}
		}
		InitClassListCombo();
		SendMessage(m_hClassListCombo, CB_SELECTSTRING, -1, (LPARAM)m_rule.str.c_str());
		break;
	case IN_DIRECTORY:
	case XREF_IN_DIRECTORY:
		SendMessage(m_hFileEdit, WM_SETTEXT, 0, (LPARAM)m_rule.str.c_str());
		break;
	case CONTAINS_TEXT:
	case XREF_CONTAINS_TEXT:
		SendMessage(m_hTextEdit, WM_SETTEXT, 0, (LPARAM)m_rule.str.c_str());
		break;
	case OBJECT_HAS_MODIFIER:
		SendMessage(m_hModifierListCombo, CB_SELECTSTRING, -1, (LPARAM)m_rule.str.c_str());
		break;
	case OBJECT_HAS_ANIMATION:
		m_rule.str = "";
		break;
	}
	EnableControls();
}

//
//        name: ConstructRuleDialog::UpdateData
// description: Updates data from controls
//
void ConstructRuleDialog::UpdateData()
{
	char str[256];

	m_rule.type = (RuleType)SendMessage(m_hRuleCombo, CB_GETCURSEL, 0, 0);
	m_rule.affirm = (IsDlgButtonChecked(m_hWnd, IDC_NEGATIVE_CHECK) == TRUE) ? false : true;
	switch(m_rule.type)
	{
	case OBJECT_TYPE:
	case OBJECT_CHILD_TYPE:
		SendMessage(m_hClassListCombo, WM_GETTEXT, 256, (LPARAM)&str[0]);
		m_rule.str = str;
		break;
	case OBJECT_SUPERCLASS:
		SendMessage(m_hSClassListCombo, WM_GETTEXT, 256, (LPARAM)&str[0]);
		m_rule.str = str;
		break;
	case IN_DIRECTORY:
	case XREF_IN_DIRECTORY:
		SendMessage(m_hFileEdit, WM_GETTEXT, 256, (LPARAM)&str[0]);
		m_rule.str = str;
		break;
	case CONTAINS_TEXT:
	case XREF_CONTAINS_TEXT:
		SendMessage(m_hTextEdit, WM_GETTEXT, 256, (LPARAM)&str[0]);
		m_rule.str = str;
		break;
	case OBJECT_HAS_MODIFIER:
		SendMessage(m_hModifierListCombo, WM_GETTEXT, 256, (LPARAM)&str[0]);
		m_rule.str = str;
		break;
	case OBJECT_HAS_ANIMATION:
		m_rule.str = "";
		break;
	}
}

//
//        name: ConstructRuleDialog::InitRuleTypeCombo
// description: Initialise the rule type combo
//
void ConstructRuleDialog::InitRuleTypeCombo()
{
	const char **ppRules;
	s32 i;

	if(IsDlgButtonChecked(m_hWnd, IDC_NEGATIVE_CHECK))
		ppRules = SelectionRules::pNotRuleTypes;
	else
		ppRules = SelectionRules::pRuleTypes;

	// empty combo box
	SendMessage(m_hRuleCombo, CB_RESETCONTENT, 0, 0);
	// add each rule into combo
	for(i = 0; i < NUM_RULES; i++)
	{
		SendMessage(m_hRuleCombo, CB_ADDSTRING, 0, (LPARAM)(ppRules[i]));
	}
	// select first item in combo
	SendMessage(m_hRuleCombo, CB_SETCURSEL, 0, 0);
}

//
//        name: ConstructRuleDialog::InitSuperClassListCombo
// description: Construct the superClass list
//
void ConstructRuleDialog::InitSuperClassListCombo()
{
	// empty combo box
	SendMessage(m_hSClassListCombo, CB_RESETCONTENT, 0, 0);

	SendMessage(m_hSClassListCombo, CB_INSERTSTRING, 0, (LPARAM)"Geometry");
	SendMessage(m_hSClassListCombo, CB_SETITEMDATA, 0, GEOMOBJECT_CLASS_ID);
	SendMessage(m_hSClassListCombo, CB_INSERTSTRING, 1, (LPARAM)"Shape");
	SendMessage(m_hSClassListCombo, CB_SETITEMDATA, 1, SHAPE_CLASS_ID);
	SendMessage(m_hSClassListCombo, CB_INSERTSTRING, 2, (LPARAM)"Camera");
	SendMessage(m_hSClassListCombo, CB_SETITEMDATA, 2, CAMERA_CLASS_ID);
	SendMessage(m_hSClassListCombo, CB_INSERTSTRING, 3, (LPARAM)"Light");
	SendMessage(m_hSClassListCombo, CB_SETITEMDATA, 3, LIGHT_CLASS_ID);
	SendMessage(m_hSClassListCombo, CB_INSERTSTRING, 4, (LPARAM)"Helper");
	SendMessage(m_hSClassListCombo, CB_SETITEMDATA, 4, HELPER_CLASS_ID);
	SendMessage(m_hSClassListCombo, CB_INSERTSTRING, 5, (LPARAM)"System");
	SendMessage(m_hSClassListCombo, CB_SETITEMDATA, 5, SYSTEM_CLASS_ID);

	// select first item in combo
	SendMessage(m_hSClassListCombo, CB_SETCURSEL, 0, 0);

	InitClassListCombo();
}

void ConstructRuleDialog::InitModifierListCombo()
{
	int iVal = 0;

	SendMessage(m_hModifierListCombo, CB_RESETCONTENT, 0, 0);

	DllDir& dllDir = m_ip->GetDllDir();
	ClassDirectory& classDir = dllDir.ClassDir();
	SubClassList* pSubClassList = classDir.GetClassList(OSM_CLASS_ID);
	s32 entry;
	entry = pSubClassList->GetFirst(ACC_PUBLIC);
	while(entry != -1)
	{
		ClassEntry& classEntry = (*pSubClassList)[entry];

		//SendMessage(m_hModifierListCombo, CB_INSERTSTRING, iVal, (LPARAM)((const char *)classEntry.ClassName());
		SendMessage(m_hModifierListCombo, CB_ADDSTRING, 0, (LPARAM)((const char *)classEntry.ClassName()));

		entry = pSubClassList->GetNext(ACC_PUBLIC);
		iVal++;
	}
}

//
//        name: ConstructRuleDialog::InitClassListCombo
// description: Construct the class type list
//
void ConstructRuleDialog::InitClassListCombo()
{
	if(m_rule.type == OBJECT_SUPERCLASS)
		return;
	s32 sel = SendMessage(m_hSClassListCombo, CB_GETCURSEL, 0, 0);
	SClass_ID superClassId = SendMessage(m_hSClassListCombo, CB_GETITEMDATA, sel, 0);
	DllDir& dllDir = m_ip->GetDllDir();
	ClassDirectory& classDir = dllDir.ClassDir();
	SubClassList* pSubClassList = classDir.GetClassList(superClassId);
	s32 entry;

	// empty combo box
	SendMessage(m_hClassListCombo, CB_RESETCONTENT, 0, 0);

	if(superClassId == GEOMOBJECT_CLASS_ID)
	{
		SendMessage(m_hClassListCombo, CB_ADDSTRING, 0, (LPARAM)"Editable Mesh");
		SendMessage(m_hClassListCombo, CB_ADDSTRING, 0, (LPARAM)"Editable Poly");
	}

	entry = pSubClassList->GetFirst(ACC_PUBLIC);
	while(entry != -1)
	{
		ClassEntry& classEntry = (*pSubClassList)[entry];
		SendMessage(m_hClassListCombo, CB_ADDSTRING, 0, (LPARAM)((const char *)classEntry.ClassName()));
		entry = pSubClassList->GetNext(ACC_PUBLIC);
	}
}

//
//        name: ConstructRuleDialog::EnableControls
// description: Enable/Disable controls depending on what rule type is selected
//
void ConstructRuleDialog::EnableControls()
{
	s32 sel = SendMessage(m_hRuleCombo, CB_GETCURSEL, 0, 0);

	bool enableSuperClassControls = (sel == OBJECT_TYPE || sel == OBJECT_SUPERCLASS || sel == OBJECT_CHILD_TYPE);
	bool enableClassControls = (sel == OBJECT_TYPE || sel == OBJECT_CHILD_TYPE);
	bool enableDirectoryControls = (sel == IN_DIRECTORY || sel == XREF_IN_DIRECTORY);
	bool enableTextControls = (sel == CONTAINS_TEXT || sel == XREF_CONTAINS_TEXT);
	bool enableModifierControls = (sel == OBJECT_HAS_MODIFIER);

	EnableWindow(m_hSClassLabel, enableSuperClassControls);
	EnableWindow(m_hSClassListCombo, enableSuperClassControls);
	EnableWindow(m_hClassListCombo, enableClassControls);
	EnableWindow(m_hClassLabel, enableClassControls);
	EnableWindow(m_hDirLabel, enableDirectoryControls);
	EnableWindow(m_hFileEdit, enableDirectoryControls);
	EnableWindow(m_hFileButton, enableDirectoryControls);
	EnableWindow(m_hTextEdit, enableTextControls);
	EnableWindow(m_hTextLabel, enableTextControls);
	EnableWindow(m_hModifierListCombo, enableModifierControls);
}

//
//        name: ConstructRuleDialog::FindDir
// description: Ask user to select a directory
//
void ConstructRuleDialog::FindDir()
{
	char path[MAX_PATH];
	SendMessage(m_hFileEdit, WM_GETTEXT, MAX_PATH, (LPARAM)&path[0]);
	m_ip->ChooseDirectory(m_hWnd, "Choose Directory", &path[0]);
	if(path[0] != 0)
		SendMessage(m_hFileEdit, WM_SETTEXT, 0, (LPARAM)&path[0]);
}

// --- RuleListDialog ----------------------------------------------------------------------------------------------

void RuleListDialog::OnInitDialog()
{
	m_hName = GetDlgItem(m_hWnd, IDC_NAME_EDIT);
	m_hClassCombo = GetDlgItem(m_hWnd, IDC_CLASS_COMBO);
	m_hRuleList = GetDlgItem(m_hWnd, IDC_RULE_LIST);
	m_hDeleteButton = GetDlgItem(m_hWnd, IDC_DELRULE_BUTTON);
	m_hEditButton = GetDlgItem(m_hWnd, IDC_EDITRULE_BUTTON);

	// initialise max data manager if it hasn't already been
	theMaxDataMgr.Init();

	// get attribute class list and add each to the combo box
	const dmat::AttributeManager& mgr = theMaxDataMgr.GetAttributeManager();
	std::list<std::string> classList;
	std::list<std::string>::iterator iClass;

	mgr.GetClassList(classList);

	// empty combo box
	SendMessage(m_hClassCombo, CB_RESETCONTENT, 0, 0);
	// add each class into combo
	for(iClass = classList.begin(); iClass != classList.end(); iClass++)
	{
		if(mgr.IsDialogAvailable((*iClass)))
		{
			SendMessage(m_hClassCombo, CB_ADDSTRING, 0, (LPARAM)((*iClass).c_str()));
		}
	}


}

void RuleListDialog::OnCommand(s32 id, s32 notifyCode)
{
	switch(id)
	{
	case IDC_ADDRULE_BUTTON:
		AddRule();
		break;
	case IDC_DELRULE_BUTTON:
		DeleteRule();
		break;
	case IDC_EDITRULE_BUTTON:
		EditRule();
		break;
	case IDC_RULE_LIST:
		if(notifyCode == LBN_SELCHANGE)
			EnableButtons();
		else if(notifyCode == LBN_DBLCLK)
			EditRule();
		break;
	}
}

//
//        name: ConstructRuleDialog::OnOK
// description: Check if dialog has been filled out correctly
//
void RuleListDialog::OnOK()
{
	if(m_rules.GetName().empty())
	{
		MessageBox(m_hWnd, "The selection rule does not have a name", "Attribute Classes Setup", MB_OK);
		return;
	}
	if(m_rules.GetClassName().empty())
	{
		MessageBox(m_hWnd, "An attribute class has not been selected", "Attribute Classes Setup", MB_OK);
		return;
	}
	if(m_rules.GetSize() == 0)
	{
		MessageBox(m_hWnd, "The selection rule requires at least one rule", "Attribute Classes Setup", MB_OK);
		return;
	}
	DialogClass::OnOK();
}

void RuleListDialog::UpdateControls()
{
	SendMessage(m_hName, WM_SETTEXT, 0, (LPARAM)m_rules.GetName().c_str());
	SendMessage(m_hClassCombo, CB_SELECTSTRING, -1, (LPARAM)m_rules.GetClassName().c_str());

	// clear list box and add in list box entries
	SendMessage(m_hRuleList, LB_RESETCONTENT, 0, 0);
	for(s32 i=0; i<m_rules.GetSize(); i++)
	{
		InsertRuleIntoList(m_rules.GetRule(i), -1);
	}

	EnableButtons();
}

void RuleListDialog::UpdateData()
{
	char str[256];

	SendMessage(m_hName, WM_GETTEXT, 256, (LPARAM)(&str[0]));
	m_rules.SetName(str);
	SendMessage(m_hClassCombo, WM_GETTEXT, 256, (LPARAM)(&str[0]));
	m_rules.SetClassName(str);
}

//
//        name: RuleListDialog::EnableButtons
// description: Decide whether to enable the delete and edit buttons
//
void RuleListDialog::EnableButtons()
{
	BOOL selected = (SendMessage(m_hRuleList, LB_GETCURSEL, 0, 0) != LB_ERR);

	EnableWindow(m_hDeleteButton, selected);
	EnableWindow(m_hEditButton, selected);
}

//
//        name: RuleListDialog::InsertRuleIntoList
// description: Insert string into rule list
//          in: 
//         out: 
//
void RuleListDialog::InsertRuleIntoList(Rule& rule, s32 posn)
{
	std::string ruleStr;
	if(rule.affirm)
		ruleStr = std::string(SelectionRules::pRuleTypes[rule.type]);
	else
		ruleStr = std::string(SelectionRules::pNotRuleTypes[rule.type]);
	ruleStr += ": " + rule.str;

	SendMessage(m_hRuleList, LB_INSERTSTRING, posn, reinterpret_cast<LPARAM>(ruleStr.c_str()));
}

//
//        name: RuleListDialog::AddRule, DeleteRule, EditRule
// description: Functions called when buttons are pressed
//
void RuleListDialog::AddRule()
{
	ConstructRuleDialog dlg;

	dlg.m_ip = m_ip;
	dlg.m_rule.type = OBJECT_TYPE;
	dlg.m_rule.str = "Editable Mesh";
	dlg.m_rule.affirm = true;
	if(dlg.DoModal(hInstance, m_hWnd))
	{
		m_rules.AddRule(dlg.m_rule);
		InsertRuleIntoList(dlg.m_rule, -1);
	}
}

void RuleListDialog::DeleteRule()
{
	s32 sel = SendMessage(m_hRuleList, LB_GETCURSEL, 0, 0);

	assert(sel != LB_ERR);

	SendMessage(m_hRuleList, LB_DELETESTRING, sel, 0);
	m_rules.DeleteRule(sel);

//	UpdateButtons();
}

void RuleListDialog::EditRule()
{
	s32 sel = SendMessage(m_hRuleList, LB_GETCURSEL, 0, 0);

	assert(sel != LB_ERR);

	Rule& rule = m_rules.GetRule(sel);
	ConstructRuleDialog dlg;

	dlg.m_ip = m_ip;
	dlg.m_rule = rule; 

	if(dlg.DoModal(hInstance, m_hWnd))
	{
		rule = dlg.m_rule;
		SendMessage(m_hRuleList, LB_DELETESTRING, sel, 0);
		InsertRuleIntoList(dlg.m_rule, sel);
		SendMessage(m_hRuleList, LB_SETCURSEL, sel, 0);
	}
}

