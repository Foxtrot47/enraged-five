//
//
//    Filename: CustAttribData.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#ifndef INC_CUSTATTRIB_DATA_H_
#define INC_CUSTATTRIB_DATA_H_

// My headers
#include "resource.h"
#include "DataInstance.h"

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define CUSTATTRIB_ATTR_NAME "Attributes"

#define CUSTATTRIB_ATTR_CLASSID		Class_ID(0x63856999, 0x623e4fde)
//MAXDATASTORE_API
class CustAttribData : public CustAttrib
{
public:
	CustAttribData() : m_hWnd(NULL), m_invalid(false) {}

	DataInstance& GetData() {return m_attr;}
	void SetInvalid() {m_invalid = true;}
	bool IsInvalid() {return m_invalid;}

	// from CustAttrib
#if (MAX_RELEASE >= 12000)
	const MCHAR* GetName() {return CUSTATTRIB_ATTR_NAME;}
#else
	TCHAR* GetName() {return CUSTATTRIB_ATTR_NAME;}
#endif
	bool CheckCopyAttribTo(ICustAttribContainer *to);
	HWND GetRollupHWnd() {return m_hWnd;}
	static void RemoveRollup(HWND hWnd, Interface* ip);
	static void RemoveRollup(HWND hWnd, IMtlParams *iMtlPrms);

	// File operations
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);

	// from ReferenceMaker
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,	PartID& partID,  RefMessage message);

	void BeginEditParams(IObjParam *ip,ULONG flags,Animatable *prev);
	void EndEditParams(IObjParam *ip, ULONG flags, Animatable *next);
	void BeginEditParams(IMtlParams *iMtlPrms,ULONG flags,Animatable *prev);

	SClass_ID SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
	Class_ID ClassID() {return CUSTATTRIB_ATTR_CLASSID;}

#if MAX_VERSION_MAJOR >= 9 
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());
#else
	ReferenceTarget* Clone(RemapDir &remap = NoRemap());
#endif

//	void BaseClone(ReferenceTarget* from,ReferenceTarget* to,RemapDir & remap);
	void DeleteThis() { delete this; }

private:
	DataInstance m_attr;
	bool m_invalid;
	HWND m_hWnd;
};


//
//
//
class CustAttribDataClassDesc : public ClassDesc2 {
	public:
	int 			IsPublic() {return 1;}
	void *			Create(BOOL loading) {return new CustAttribData;}
	const TCHAR *	ClassName() { return GetString(IDS_ATTRIBUTE_NAME); }
	SClass_ID		SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
	Class_ID 		ClassID() {return CUSTATTRIB_ATTR_CLASSID;}
	const TCHAR* 	Category() {return _T("");}
	BOOL			NeedsToSave(){return TRUE;}
	const TCHAR*	InternalName() { return _T("Attributes"); }	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance() { return hInstance; }			// returns owning module handle
	};

#endif