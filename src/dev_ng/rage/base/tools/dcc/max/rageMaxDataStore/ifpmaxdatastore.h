//IFpMaxDataStore.h

//Defines the function publishing interface which can be used by other plugins to interact with the data
//store through MAX's function publishing system.

#ifndef __IFPMAXDATASTORE_H__
#define __IFPMAXDATASTORE_H__

#define FP_MAXDATASTORE_INTERFACE Interface_ID(0x3b486351, 0x4b0ecf98)

#define GetMaxDataStoreInterface() \
	(IFpMaxDataStore*)GetCOREInterface( FP_MAXDATASTORE_INTERFACE );

class IFpMaxDataStore : public FPStaticInterface
{
public:
	enum { em_getAttrClass, em_getAttrIndex, em_getAttrType,
			em_getAttrName, em_getNumAttr, 
			em_getAttrInt, em_getAttrStr, em_getAttrFloat, em_getAttrBool, em_getAttr, 
			em_setAttrInt, em_setAttrStr, em_setAttrFloat, em_setAttrBool, em_setAttr,
			em_getGuid
			};
public:
	virtual ~IFpMaxDataStore() {}
public:
	virtual TSTR	GetAttrClass(INode *pNode) = 0;
	virtual int		GetAttrIndex(TSTR className, TSTR attrName) = 0;
	virtual TSTR	GetAttrType(TSTR className, int attrIndex) = 0;
	virtual TSTR	GetAttrName(TSTR className, int attrIndex) = 0;
	virtual int		GetNumAttr(TSTR className) = 0;

	virtual int		GetAttrInt(INode *pNode, int attrIndex) = 0;
	virtual TSTR	GetAttrStr(INode *pNode, int attrIndex) = 0;
	virtual float	GetAttrFloat(INode *pNode, int attrIndex) = 0;
	virtual bool	GetAttrBool(INode *pNode, int attrIndex) = 0;
	virtual Value*	GetAttr(INode *pNode, int attrIndex) = 0;

	virtual void	SetAttrInt(INode *pNode, int attrIndex, int val) = 0;
	virtual void	SetAttrStr(INode *pNode, int attrIndex, TSTR val) = 0;
	virtual void	SetAttrFloat(INode *pNode, int attrIndex, float val) = 0;
	virtual void	SetAttrBool(INode *pNode, int attrIndex, bool val) = 0;
	virtual Value*	SetAttr(INode *pNode, int attrIndex, Value *val) = 0;

	virtual TSTR	GetGuidString(INode *pNode) = 0;
};

#endif //__IFPMAXDATASTORE_H__
