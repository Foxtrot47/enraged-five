//
//
//    Filename: INodeDataStore.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 25/06/99 14:52 $
//   $Revision: 1 $
// Description: The data an INode needs to store
//
//
#ifndef INC_INODE_DATA_STORE
#define INC_INODE_DATA_STORE

// my headers
#include <AttribMgr.h>

#define GUID_STRING_LEN 40

// STD headers
#pragma warning (disable :4786)
#include <string>

// using namespace
using namespace rage;

#ifdef MAXDATASTORE_EXPORTS
#define MAXDATASTORE_API __declspec(dllexport)
//#elif MAXDATASTORE_LOCAL
//#define MAXDATASTORE_API
#else
#define MAXDATASTORE_API __declspec(dllimport)
#endif


// CString bytetohex(byte item)
// {
// 	CString result;
// 	result.Format(_T("%02.2x"),item);
// 	return result.Right(2);
// }
// 
// CString G_GuidToString(byte *Guid)
// {
// 	CString result = _T("");
// 
// 	result += bytetohex(Guid[3]);
// 	result += bytetohex(Guid[2]);
// 	result += bytetohex(Guid[1]);
// 	result += bytetohex(Guid[0]);
// 	result += _T("-");
// 
// 	result += bytetohex(Guid[5]);
// 	result += bytetohex(Guid[4]);
// 	result += _T("-");
// 
// 	result += bytetohex(Guid[7]);
// 	result += bytetohex(Guid[6]);
// 	result += _T("-");
// 
// 	result += bytetohex(Guid[8]);
// 	result += bytetohex(Guid[9]);
// 	result += _T("-");
// 
// 	result += bytetohex(Guid[10]);
// 	result += bytetohex(Guid[11]);
// 	result += bytetohex(Guid[12]);
// 	result += bytetohex(Guid[13]);
// 	result += bytetohex(Guid[14]);
// 	result += bytetohex(Guid[15]);
// 
// 	return result;
// }
//
//   Class Name: DataInstance
// Base Classes: 
//  Description: Class to store extra information to be attached to a node thru the MaxDataStore class
//    Functions: 
//
//
class MAXDATASTORE_API DataInstance
{
public:
	DataInstance();
	DataInstance(const DataInstance &orig);
	~DataInstance();

	void AddAttributes(RefTargetHandle rtarg);
	void AddAttributes(const std::string &className);
	dmat::AttributeInst *GetAttributes() const {return m_pAttributes;}
	void SetAttributes(dmat::AttributeInstMultiple* p_attribNew);
	dmat::AttributeDialog* ConstructDialog();
	dmat::AttributeDialog* ConstructRollupPage(bool inMaterialEditor=false);
	const std::string& GetClassName() const {return m_attributeClassName;}

	void EditModal() const;
	GUID GetGuid()
	{
		CreateGuid();
		return m_guid;
	}
	void ResetGuid()
	{
		m_guid = GUID_NULL;
		CreateGuid();
	}

	const std::string GetGuidString()
	{
		OLECHAR guidString[GUID_STRING_LEN]={0};
		int num = ::StringFromGUID2(m_guid, guidString, GUID_STRING_LEN);
		int len;
		int slength = (int)wcslen(guidString) + 1;
		len = WideCharToMultiByte(CP_ACP, 0, guidString, slength, 0, 0, 0, 0);
		char* buf = new char[len];
		WideCharToMultiByte(CP_ACP, 0, guidString, slength, buf, len, 0, 0);
		std::string r(buf);
		delete[] buf;
		return r;
	}

	DataInstance &operator=(const DataInstance &copy);

	friend class CustAttribData;

private:
	BOOL IsGuidInitialised()
	{
		return sizeof(m_guid)== sizeof(GUID);
	}
	void CreateGuid()
	{
		if(!IsGuidInitialised() || m_guid==GUID_NULL)
		{
			memset(&m_guid, 0, sizeof(GUID));
			CoCreateGuid(&m_guid);
		}
	}
	void SetGuid(GUID g)
	{
		m_guid = g;
	}

	dmat::AttributeInst *m_pAttributes;
	std::string m_attributeClassName;
	GUID m_guid;
};


#endif // INC_INODE_DATA_STORE