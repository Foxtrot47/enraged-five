//
//
//    Filename: IMaxDataStore.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Interface class to the MaxDataStore
//
//
#ifndef INC_IMAXDATASTORE_H_
#define INC_IMAXDATASTORE_H_

// STL headers
#include <string>

// using namespace
using namespace rage;

#ifdef MAXDATASTORE_EXPORTS
#define MAXDATASTORE_API __declspec(dllexport)
#else
#define MAXDATASTORE_API __declspec(dllimport)
#endif

class DataInstance;

// Attribute Plugin forward declarations
namespace dmat
{
	class AttributeClass;
}

//
//   Class Name: IMaxDataStore
//  Description: Interface class to MaxDataStore classes
//
class MAXDATASTORE_API IMaxDataStore
{
public:

	s32 GetAttribIndex(RefTargetHandle rtarg,const char* p_cName);

	DataInstance* AddData(RefTargetHandle rtarg);
	DataInstance* GetData(RefTargetHandle rtarg);
	DataInstance* GetDataHasModified(RefTargetHandle rtarg,bool& bChanged);
	void CopyData(RefTargetHandle to, RefTargetHandle from);

	GUID GetDataGuid(RefTargetHandle rtarg);
	std::string IMaxDataStore::GetDataGuidString(RefTargetHandle rtarg);

	void EditDataModal(RefTargetHandle rtarg);
	HWND AddRollupPage(RefTargetHandle rtarg, Interface *ip);
	void DeleteRollupPage(HWND hWnd, Interface *ip);
	HWND AddRollupPage(RefTargetHandle rtarg, IMtlParams *ip);
	void DeleteRollupPage(HWND hWnd, IMtlParams *ip);
	void EditDataWithDialog(RefTargetHandle rtarg, void *pDlgData);

	void GetClassFromRefTarget( RefTargetHandle rtarg, std::string& sClassName );
	bool IsClassAvailable( const std::string& sClassName ); 
	dmat::AttributeClass* GetClass( const std::string& sClassName );
};

extern MAXDATASTORE_API IMaxDataStore theIMaxDataStore;

#endif // INC_IMAXDATASTORE_H_