//
//
//    Filename: MaxDataStore.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Class holding attributes data for all the objects that require it
//
//

#include "MaxDataStore.h"
#include "MaxDataMgr.h"
#include "DataInstance.h"
#include "RightClickMenu.h"
#include <attriberror.h>

//#define REGISTER_INODES_ALWAYS


//#define DATASTORE_PROP_KEY	"ds"
#define DATASTORE_APPDATA_KEY 0x1023

// save/load
#define FILE_SLOT_QUEUE_ID		0x7825
#define FILE_SLOT_QUEUE_POSN_ID	0x7826
#define FILE_REFS_ARRAY_ID		0x7827
#define FILE_ATTRIBUTE_ID		0x7828

//
//        name: grow_vector
// description: Template function that grows a vector and places a default value 
//          in: vec = reference to vector
//				size = size want vector to be
//				value = value to place in new entries
//         out: 
//
template<class T> void grow_vector(std::vector<T>& vec, s32 size, T value)
{
	while(vec.size() <= size)
		vec.push_back(value);
}




MaxDataStore theMaxDataStore;
static MaxDataStoreClassDesc theMaxDataStoreClassDesc;

// --- DataStorePostLoadCallback ------------------------------------------------------------------------

class DataStorePostLoadCallback : public PostLoadCallback
{
public:
	virtual void proc(ILoad *iload);
};

//
//        name: DataStorePostLoadCallback::proc
// description: Postload procedure. Tells MaxDataStore finished loading
//
void DataStorePostLoadCallback::proc(ILoad *iload)
{
	theMaxDataStore.PostLoad(iload);
}


// --- MaxDataStoreClassDesc ----------------------------------------------------------------------------

ClassDesc* GetMaxDataStoreDesc() 
{
	return &theMaxDataStoreClassDesc;
}

// called whenever reset is selected
void MaxDataStoreClassDesc::ResetClassParams (BOOL fileReset) 
{
	theMaxDataStore.Reset();
	theMaxDataMgr.Reset();
}
// return pointer to 'theMaxDataStore' instead of creating a new version
void *MaxDataStoreClassDesc::Create (BOOL loading) 
{
	return &theMaxDataStore;
}
// this gets called everytime a save is called even when the instance of the MaxDataStore is referenced
// by the scene. ditto for 'Load'
IOResult MaxDataStoreClassDesc::Save(ISave *isave)
{
	return theMaxDataStore.Save(isave);
}
IOResult MaxDataStoreClassDesc::Load(ILoad *iload)
{
	return theMaxDataStore.Load(iload);
}


// --- MaxDataStore ------------------------------------------------------------------------------------

//
//        name: MaxDataStore::MaxDataStore
// description: Constructor
//
MaxDataStore::MaxDataStore()
{
	Interface *ip = GetCOREInterface();
	INode *pIRoot = ip->GetRootNode();

	m_pCopy = NULL;
	m_hMainWnd = NULL;
	m_queuePosn = 0;
#ifdef REGISTER_INODES_ALWAYS
	AddReference(pIRoot);
#endif
	AddItemToRightClickMenu(ip);
}

//
//        name: MaxDataStore::MaxDataStore
// description: Destructor
//
MaxDataStore::~MaxDataStore()
{
	Reset();
}

//
//        name: MaxDataStore::Save
// description: 
//          in: 
//         out: 
//
IOResult MaxDataStore::Save(ISave *isave)
{
	u32 length;
	s32 i;
	s32 imaker;

	isave->BeginChunk(FILE_REFS_ARRAY_ID);
	for(i = 0; i < NumRefs(); i++)
	{
		RefTargetHandle ref = GetReference(i);
		// if not a valid reference then set reference to null.
		// Added this check in because the materials do fucking stupid things, like calling 
		// ReplaceReference for the MaxDataStore
		if(!IsValidRefId(ref, i))
		{
			ref = NULL;
		}
		imaker = isave->GetRefID(ref);
		isave->Write(&imaker, sizeof(s32), (ULONG*)&length);
	}
	isave->EndChunk();

	// for every node with data attached
	for(i = 0; i < m_dataArray.size(); i++)
	{
		if(m_dataArray[i])
		{
			s32 size;
			char *pData = NULL;
			if(m_dataArray[i]->GetAttributes())
				pData = m_dataArray[i]->GetAttributes()->CreateRawData(size);
			else
				size = 0;

			isave->BeginChunk(FILE_ATTRIBUTE_ID);
			isave->Write(&i, sizeof(s32), (ULONG*)&length);		// data index
			isave->Write(&size, sizeof(s32), (ULONG*)&length);	// size of attribute data
			if(size)
				isave->Write(pData, size, (ULONG*)&length);				// attribute data 
			isave->WriteCString(m_dataArray[i]->GetClassName().c_str());	// attribute class name
			isave->EndChunk();

			delete[] pData;
		}
	}

	return IO_OK;
}
//
//        name: MaxDataStore::Load
// description: 
//          in: 
//         out: 
//
IOResult MaxDataStore::Load(ILoad *iload)
{
	DataStorePostLoadCallback *pPostLoadCallback = new DataStorePostLoadCallback;
	// set loading and register postload callback
	m_isLoading = true;
	iload->RegisterPostLoadCallback(pPostLoadCallback);

	// load slot queue
	IOResult res;
	u32 length;

	while (IO_OK == (res = iload->OpenChunk())) {
		switch(iload->CurChunkID())  
		{
		case FILE_REFS_ARRAY_ID:
			{
				// get num references
				u32 size = (u32)iload->CurChunkLength() / sizeof(u32);
				s32 *pArray = new s32[size+1];

				// read reference ids into array
				res = iload->Read(pArray, size*sizeof(s32), (ULONG*)&length);
				// construct ref ids vector and allocate space for data
				m_loadedRefIDs = std::vector<s32>(pArray, pArray + size);
				m_loadedData.assign(size, NULL);
				delete[] pArray;
			}
			break;
		case FILE_ATTRIBUTE_ID:
			{
				s32 size = (s32)iload->CurChunkLength();
				char *pData = new char[size+1];
				s32 index;
				s32 dataSize;
				char *pName;
				char *pAttributes;

				res = iload->Read(pData, size, (ULONG*)&length);
				// add a end of string character at the end of the data so that the class name has an 
				// ending
				pData[size] = '\0';

				// get index, datasize, attribute data and attribute class name
				index = *reinterpret_cast<s32 *>(pData);
				dataSize = *reinterpret_cast<s32 *>(pData + 4);
				pAttributes = pData + 8;
				pName = pAttributes + dataSize;
				
				if(index < int(m_loadedData.size()))
				{
					assert(!m_loadedData[index]);
					m_loadedData[index] = new DataInstance;
					m_loadedData[index]->AddAttributes(pName);
					if(m_loadedData[index]->GetAttributes())
						m_loadedData[index]->GetAttributes()->ReadRawData(pAttributes, dataSize);
				}
				delete[] pData;
			}
			break;
		default:
			break;
		}
		iload->CloseChunk();
		if (res != IO_OK) 
			return res;
	}

	return IO_OK;
}

//
//        name: MaxDataStore::PostLoad
// description: This is called after everything else has loaded. It sets up the reference pointers,
//				copies the attribute data across and deletes all loading related arrays
//          in: 
//         out: 
//
void MaxDataStore::PostLoad(ILoad *iload)
{
	RefTargetHandle refTarg;
	s32 oldSlot, newSlot;
	s32 i;

	m_isLoading = false;

	for(i=0; i<int(m_loadedRefIDs.size()); i++)
	{
		refTarg = static_cast<RefTargetHandle>(iload->GetAddr(m_loadedRefIDs[i]));
		if(refTarg == NULL)
			continue;
		oldSlot = GetIdFromRef(refTarg);
		refTarg->RemoveAppDataChunk(ClassID(), SuperClassID(), DATASTORE_APPDATA_KEY);

		newSlot = AddReference(refTarg);
		CreateDataInstance(newSlot);
		if(oldSlot != -1 && m_loadedData[oldSlot])
			*(m_dataArray[newSlot]) = *(m_loadedData[oldSlot]);
	}

	// delete all temporary loading data
	for(i=0; i<int(m_loadedData.size()); i++)
	{
		if(m_loadedData[i])
			delete m_loadedData[i];
	}
	m_loadedRefIDs.clear();
	m_loadedData.clear();
}

//
//        name: MaxDataStore::NumRefs
// description: Return number of references
//
int MaxDataStore::NumRefs()
{
	return (int)m_slotQueue.size();
}
//
//        name: MaxDataStore::GetReference
// description: Return pointer to a reference
//          in: i = reference number
//         out: 
//
RefTargetHandle MaxDataStore::GetReference(int i) 
{
	if(i < int(m_refArray.size()))
		return m_refArray[i];

	return NULL;
}

//
//        name: MaxDataStore::SetReference
// description: Set a reference
//          in: i = number of reference
//				pTarg = pointer to reference targer
//         out: 
//
void MaxDataStore::SetReference(int i, RefTargetHandle pTarg) 
{
	grow_vector(m_refArray, i, (RefTargetHandle)NULL);

	m_refArray[i] = pTarg;
}

//
//        name: MaxDataStore::NotifyRefChanged
// description: Called whenever a reference is changed. Use this to catch whenever a new node is created
//          in: 
//         out: 
//
RefResult MaxDataStore::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
										 PartID& partID,RefMessage message)
{
	switch(message)
	{
#ifdef REGISTER_INODES_ALWAYS
	case REFMSG_NODE_LINK:
		{
			// don't do anything if loading
			if(m_isLoading)
				break;
			INode *pINode = (INode *)hTarget;
			INode *pIChild;

			for(s32 i=0; i<pINode->NumberOfChildren(); i++)
			{
				pIChild = pINode->GetChildNode(i);
				AddReference(pIChild);
			}
		}
		break;
#endif
	case REFMSG_TARGET_DELETED:
		RemoveReference(hTarget);
		break;
	}

	return REF_SUCCEED;
}

//
//        name: MaxDataStore::AddReference
// description: Add a reference
//          in: pTarg = pointer to reference to add
//
s32 MaxDataStore::AddReference(RefTargetHandle pTarg)
{
	AppDataChunk *pDataChunk = pTarg->GetAppDataChunk(ClassID(), SuperClassID(), DATASTORE_APPDATA_KEY);
	if(pDataChunk)
		return *static_cast<s32 *>(pDataChunk->data);

	// get next available slot
	u32 slot = GetNextSlot();

	// ensure there is no data in that slot
	if(slot < m_dataArray.size() && m_dataArray[slot])
	{
		delete m_dataArray[slot];
		m_dataArray[slot] = NULL;
	}

	// Add AppDataChunk
	u32 *pData = static_cast<u32 *>(malloc(4));
	*pData = slot;
	pTarg->AddAppDataChunk(ClassID(), SuperClassID(), DATASTORE_APPDATA_KEY, 4, pData);

	ReplaceReference(slot, pTarg);
	return slot;
}

//
//        name: MaxDataStore::RemoveReference
// description: Remove reference. This assumes that the reference has already been removed from the
//				reference target
//          in: pTarg = pointer to reference to remove
//
void MaxDataStore::RemoveReference(RefTargetHandle pTarg)
{
	AppDataChunk *pDataChunk = pTarg->GetAppDataChunk(ClassID(), SuperClassID(), DATASTORE_APPDATA_KEY);
	s32 id;

	if(!pDataChunk)
	{
		// do this because materials delete their data chunks before removing from the scene
		id = FindRef(pTarg);
		if(id == -1)
			return;
	}
	else
	{
		id = *static_cast<s32 *>(pDataChunk->data);
	}

	SetReference(id, NULL);
	ReleaseSlot(id);
}

//
//        name: MaxDataStore::CreateDataInstance
// description: Create a DataInstance in the DataInstance array at index 'slot' 
//          in: slot = slot number
//
void MaxDataStore::CreateDataInstance(s32 slot)
{
	// ensure all new elements of NodeDataArray are NULL
	grow_vector(m_dataArray, slot, static_cast<DataInstance *>(NULL));

	if(m_dataArray[slot])
		return;
	m_dataArray[slot] = new DataInstance;
}

//
//        name: MaxDataStore::GetNextSlot
// description: Return the next array slot available
//
s32 MaxDataStore::GetNextSlot()
{
	// get slot from queue
	if(m_queuePosn == m_slotQueue.size())
		m_slotQueue.push_back(m_queuePosn);
	return m_slotQueue[m_queuePosn++];
}

//
//        name: MaxDataStore::ReleaseSlot
// description: Free up array slot for reuse
//          in: slot = slot index to release
//
void MaxDataStore::ReleaseSlot(s32 slot)
{
	// delete slot data
	if(m_dataArray[slot])
		delete m_dataArray[slot];
	m_dataArray[slot] = NULL;

	// add slot back onto queue
	m_queuePosn--;
	m_slotQueue[m_queuePosn] = slot;
}

//
//        name: MaxDataStore::Reset
// description: Called when reset is selected. Tidys up MaxDataStore arrays etc. This is mainly debugging
//
void MaxDataStore::Reset()
{
	s32 i=0;

	for(i=0; i<int(m_dataArray.size()); i++)
		if(m_dataArray[i])
			delete m_dataArray[i];
	for(i=0; i<int(m_refArray.size()); i++)
		ReplaceReference(i, NULL);

	if(m_pCopy)
		delete m_pCopy;
	m_pCopy = NULL;

	m_slotQueue.clear();
	m_dataArray.clear();
	m_refArray.clear();

	m_queuePosn = 0;

#ifdef REGISTER_INODES_ALWAYS
	Interface *ip = GetCOREInterface();
	INode *pIRoot = ip->GetRootNode();

	AddReference(pIRoot);
#endif
}

//
//        name: MaxDataStore::GetIdFromRef
// description: Return the id for a reference target by getting the app data chunk
//          in: pTarg = pointer to ref target
//		   out: id of ref target
//
s32 MaxDataStore::GetIdFromRef(RefTargetHandle pTarg)
{
	AppDataChunk *pDataChunk = pTarg->GetAppDataChunk(ClassID(), SuperClassID(), DATASTORE_APPDATA_KEY);
	if(pDataChunk)
		return *static_cast<s32 *>(pDataChunk->data);
	return -1;
}

//
//        name: IsValidRefId
// description: Return if slot is valid for this reference
//          in: pTarg = pointer to reference target
//				id = slot id
//         out: is valid
//
bool MaxDataStore::IsValidRefId(RefTargetHandle pTarg, s32 id)
{
	return id == GetIdFromRef(pTarg);
}

//
//        name: MaxDataStore::FindRef
// description: Return the id for a reference target by searching through the reference target array
//          in: pTarg = pointer to ref target
//		   out: id of ref target
//
s32 MaxDataStore::FindRef(RefTargetHandle pTarg)
{
	for(s32 i=0; i<int(m_refArray.size()); i++)
	{
		if(m_refArray[i] == pTarg)
			return i;
	}
	return -1;
}

//
//        name: MaxDataStore::AddItemToRightClickMenu
// description: Add a edit attributes item to right click menu
//          in: ip = pointer to interface
//
void MaxDataStore::AddItemToRightClickMenu(Interface *ip)
{
	RightClickMenuManager *pManager = ip->GetRightClickMenuManager();

	pManager->Register(&theAttributeMenuItem);
}

//
//        name: MaxDataStore::AddData
// description: Add the correct attribute data to the reference passed in
//          in: rtarg = pointer to reference target
//		   out: returns index of data
//
s32 MaxDataStore::AddData(RefTargetHandle rtarg)
{
	s32 id = AddReference(rtarg);
	CreateDataInstance(id);
	m_dataArray[id]->AddAttributes(rtarg);
	return id;
}

//
//        name: MaxDataStore::EditINodeData
// description: Edit the data attached to an INode
//          in: rtarg = pointer to reference target
//
void MaxDataStore::EditDataModal(RefTargetHandle rtarg)
{
	// Get the main window for Max as it is required later on
/*	if(m_hMainWnd == NULL)
	{
		Interface *ip = GetCOREInterface();
		m_hMainWnd = ip->GetMAXHWnd();
	}

	s32 id = AddData(rtarg);
	m_dataArray[id]->EditModal();*/
	TSTR name;
	DataInstance *pData;

	rtarg->GetClassName(name);
	if(rtarg->SuperClassID() == BASENODE_CLASS_ID)
	{
		name = static_cast<INode*>(rtarg)->GetName();
	}

	pData = theMaxDataMgr.AddAttributes(rtarg);
	if(pData)
		pData->EditModal();
}

//
//        name: MaxDataStore::CopyINodeData
// description: Copy attribute data from INode
//
void MaxDataStore::CopyData(RefTargetHandle rtarg)
{
	s32 id = GetIdFromRef(rtarg);

	if(m_pCopy)
		delete m_pCopy;

	if(id != -1 && m_dataArray[id])
		m_pCopy = new DataInstance(*(m_dataArray[id]));
	else
	{
		m_pCopy = new DataInstance;
		m_pCopy->AddAttributes(rtarg);
	}
}

//
//        name: MaxDataStore::PasteINodeData
// description: Paste attribute data onto INode
//
void MaxDataStore::PasteData(RefTargetHandle rtarg)
{
	assert(m_pCopy);

	s32 id = AddReference(rtarg);

	CreateDataInstance(id);
	*m_dataArray[id] = *m_pCopy;
}

//
//        name: MaxDataStore::IsCopyAvailable
// description: Return if there is a DataInstance available for pasting onto this INode
//          in: pINode= pointer to INode
//
bool MaxDataStore::IsCopyAvailable(RefTargetHandle rtarg)
{
	if(m_pCopy == NULL)
		return false;

	std::string nodeClassName;

	// if INode has a different attribute class from the copy then return false
	theMaxDataMgr.GetClassFromRefTarget(rtarg, nodeClassName);

	if(nodeClassName != m_pCopy->GetClassName())
		return false;

	return true;
}

