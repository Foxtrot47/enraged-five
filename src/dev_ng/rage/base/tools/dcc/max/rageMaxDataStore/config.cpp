//
//
//    Filename: config.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Two functions loading and saving MaxDataStore configuration files
//
//
#pragma warning (disable : 4786)
#pragma warning (disable : 4251)

#include "MaxDataMgr.h"

// My headers
#include "Configuration.h"
// RAGE headers
#include "parser/manager.h"
#include "parser/tree.h"
// STD headers
#include <sstream>
#include <fstream>

// using namespace
using namespace rage;

//
//        name: MaxDataMgr::LoadRule
// description: Load a rule from a XML element
//          in: pRuleNode = XML element
//				selRule = selection rules to add rule to
//
void MaxDataMgr::LoadRule(parTreeNode* pRuleNode, SelectionRules& selRule)
{
	atArray<parAttribute>& attributes = pRuleNode->GetElement().GetAttributes();
	Rule rule;

	// default values
	rule.type = OBJECT_TYPE;
	rule.affirm = true;

	// Get type and if positive or negative from attributes
	for(int i = 0; i < attributes.GetCount(); ++i)
	{
		if(!stricmp(attributes[i].GetName(), "Type"))
		{
			bool foundMatchingRule = false;
			for(int j=0; j<NUM_RULES; j++)
			{
				if(!stricmp(attributes[i].GetStringValue(), SelectionRules::pRuleNames[j]))
				{
					foundMatchingRule = true;
					rule.type = static_cast<RuleType>(j);
					break;
				}
			}
			// if rule type not found
			if(!foundMatchingRule)
			{
				std::string errorMsg = "Unrecognised rule type in file the configuration file: " + std::string(attributes[i].GetStringValue());
				MessageBox(m_hMainWnd, errorMsg.c_str(), "MaxDataStore", MB_OK);
			}
		}
		else if(!stricmp(attributes[i].GetName(), "Positive"))
		{
			if(!stricmp(attributes[i].GetStringValue(), "false"))
				rule.affirm = false;
		}
	}

	rule.str = std::string(pRuleNode->GetData());
	selRule.AddRule(rule);
}

//
//        name: MaxDataMgr::LoadSelectionRule
// description: Load selection rules from a XML element
//          in: pSelRuleNode = XML element
//
void MaxDataMgr::LoadSelectionRule(parTreeNode* pSelRuleNode)
{
	atArray<parAttribute>& attributes = pSelRuleNode->GetElement().GetAttributes();
	SelectionRules selRule;

	// Get name and class name from attributes
	for(int i = 0; i < attributes.GetCount(); ++i)
	{
		if(!stricmp(attributes[i].GetName(), "Name"))
			selRule.SetName(std::string(attributes[i].GetStringValue()));
		else if(!stricmp(attributes[i].GetName(), "Class"))
			selRule.SetClassName(std::string(attributes[i].GetStringValue()));
	}

	// rules are children of selection rule element
	for(parTreeNode::ChildNodeIterator it = pSelRuleNode->BeginChildren(); it != pSelRuleNode->EndChildren(); ++it)
	{
		if(!stricmp((*it)->GetElement().GetName(), "rule"))
			LoadRule((*it), selRule);
	}

	m_rules.AddSelectionRule(selRule);
}

//
//        name: MaxDataMgr::LoadRules
// description: Extract rules from a XML element
//          in: pRuleListNode = XML element containing all the selection rules
//
void MaxDataMgr::LoadRules(parTreeNode* pRuleListNode)
{
	for(parTreeNode::ChildNodeIterator it = pRuleListNode->BeginChildren(); it != pRuleListNode->EndChildren(); ++it)
	{
		if(!stricmp((*it)->GetElement().GetName(), "selRule"))
			LoadSelectionRule((*it));
	}
}

//
//        name: MaxDataMgr::LoadConfigFile
// description: Load the manager configuration file
//
void MaxDataMgr::LoadConfigFile()
{
	// attempt to load XML ini file
	parTree* pXmlTree = PARSER.LoadTree(Config::GetConfigFile().c_str(), "xml");
	if(pXmlTree == NULL)
	{
		std::stringstream sstr;
		sstr << "Failed to load Attribute Script File " << Config::GetConfigFile() << ". Scene attributes will be lost on saving file. Not safe to continue." << std::ends;
		MessageBox(m_hMainWnd, sstr.str().c_str(), "MaxDataStore", MB_ICONEXCLAMATION);
		throw MAXException("File load error", -666);
		return;
	}

	// get file element
	parTreeNode* pFileNode = pXmlTree->GetRoot()->FindChildWithName("file");
	if ( pFileNode != NULL )
	{
		//if the filename has no path then
		//add the standard path on
		m_attributeFile = pFileNode->GetData();

		if((strchr(m_attributeFile.c_str(),'\\') == NULL) && (strchr(m_attributeFile.c_str(),'/') == NULL))
		{
			m_attributeFile = Config::GetConfigPath() + m_attributeFile;
		}
	}

	// get rule list if it exists 
	parTreeNode* pRuleListNode = pXmlTree->GetRoot()->FindChildWithName("rulelist");
	if(pRuleListNode != NULL)
	{
		LoadRules(pRuleListNode);
	}

	// cleanup
	delete pXmlTree;
}

//
//        name: MaxDataMgr::SaveConfigFile
// description: Save the manager configuration file
//
void MaxDataMgr::SaveConfigFile()
{
	// Bail out if not writable
	DWORD fileAttributes = GetFileAttributes(Config::GetConfigFile().c_str());
	if(fileAttributes & FILE_ATTRIBUTE_READONLY)
	{
		return;
	}

	// construct XML hierarchy
	parTree* pXmlTree = new parTree();
	parTreeNode* pRootNode = pXmlTree->CreateRoot();
	pRootNode->GetElement().SetName("datastore");
	parTreeNode* pFileNode = new parTreeNode("file");
	parTreeNode* pRuleListNode = new parTreeNode("rulelist");
	s32 i, j;

	pFileNode->AppendAsChildOf(pRootNode);
	pRuleListNode->AppendAsChildOf(pRootNode);

	// attribute file
	const char *lastLeaf = strrchr(m_attributeFile.c_str(), '\\');
	lastLeaf++;
	pFileNode->SetData(lastLeaf, (u32)strlen(lastLeaf));
	
	// rules elements
	for(i=0; i<m_rules.GetSize(); i++)
	{
		SelectionRules& selRule = m_rules.GetSelectionRule(i);
		parTreeNode* pSelRuleNode = new parTreeNode("selRule");

		pSelRuleNode->GetElement().AddAttribute("Name", selRule.GetName().c_str());
		pSelRuleNode->GetElement().AddAttribute("Class", selRule.GetClassName().c_str());
		pSelRuleNode->AppendAsChildOf(pRuleListNode);

		for(j=0; j<selRule.GetSize(); j++)
		{
			Rule& rule = selRule.GetRule(j);
			parTreeNode* pRuleNode = new parTreeNode("rule");

			pRuleNode->GetElement().AddAttribute("Type", SelectionRules::pRuleNames[rule.type]);
			pRuleNode->GetElement().AddAttribute("Positive", rule.affirm);

			pRuleNode->SetData(rule.str.c_str(), (u32)strlen(rule.str.c_str()));
			pRuleNode->AppendAsChildOf(pSelRuleNode);
		}
	}

	// write buffer to a file
	PARSER.SaveTree(Config::GetConfigFile().c_str(), "xml", pXmlTree);

	// cleanup
	delete pXmlTree;
}
