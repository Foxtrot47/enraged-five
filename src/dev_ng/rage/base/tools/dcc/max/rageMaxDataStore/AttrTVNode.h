//
//
//    Filename: AttrTVNode.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#ifndef INC_ATTR_TVNODE_H_
#define INC_ATTR_TVNODE_H_

#include "resource.h"
// My headers
#include "DataInstance.h"

#define ATTRTVNODE_CLASS_ID Class_ID(0x485628f2, 0x24e267b8)

extern TCHAR *GetString(int id);


//
//   Class Name: AttrTVNode
// Base Classes: 
//  Description: Track view node with attribute data attached
//    Functions: 
//
//
class AttrTVNode : public ITrackViewNode 
{
	DataInstance m_attr;
	RefTargetHandle m_ref;
	s32 m_id;

public:
	AttrTVNode(s32 id) : m_ref(NULL), m_id(id) {}
	~AttrTVNode();

	void SetId(s32 id) {m_id = id;}
	Class_ID GetClassID() {return Class_ID(0x234, m_id);}
	DataInstance& GetData() {return m_attr;}
	void DeleteThisTVNode();

	void AddNode(ITrackViewNode *node, TCHAR *name, Class_ID cid, int pos=TVNODE_APPEND){}
	void AddController(Control *c, TCHAR *name, Class_ID cid, int pos=TVNODE_APPEND) {}
	int FindItem(Class_ID cid) {return -1;}
	void RemoveItem(int i) {}
	void RemoveItem(Class_ID cid) {}
	Control *GetController(int i) {return NULL;}
	Control *GetController(Class_ID cid) {return NULL;}
	ITrackViewNode *GetNode(int i) {return NULL;}
	ITrackViewNode *GetNode(Class_ID cid) {return NULL;}
	int NumItems() {return 0;}
	void SwapPositions(int i1, int i2) {}
	void HideChildren(BOOL hide) {}
	
	TCHAR *GetName(int i) { return _T("AttributeTVN"); }
	void SetName(int i,TCHAR *name) {}
	
	void RegisterTVNodeNotify(TVNodeNotify *notify) {}
	void UnRegisterTVNodeNotify(TVNodeNotify *notify) {}
	
	Class_ID ClassID() {return ATTRTVNODE_CLASS_ID;}
	SClass_ID SuperClassID() {return REF_TARGET_CLASS_ID;}
	void DeleteThis() {delete this;}
	
	// references
	int NumRefs() {return 1;}
	RefTargetHandle GetReference(int i) {return m_ref;}
	void SetReference(int i, RefTargetHandle ref) {m_ref = ref;}
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
		PartID& partID,  RefMessage message);
	BOOL IsRealDependency(RefTargetHandle ref) {return FALSE;}

	//IO
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);
	
	// is this tvn is hidden?
	BOOL BypassTreeView() {return FALSE;}
};


AttrTVNode* CreateAttrTVNode();
void FixAttrTVNode(AttrTVNode* pNode);
//
//   Class Name: AttrTVNodeClassDesc
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class AttrTVNodeClassDesc : public ClassDesc 
{
public:
	int IsPublic() {return 1;}
	void *Create(BOOL loading = FALSE) {return CreateAttrTVNode();}
	const TCHAR *ClassName() {return _T("AttributeTVN");}
	SClass_ID SuperClassID() {return REF_TARGET_CLASS_ID;}
	Class_ID ClassID() {return ATTRTVNODE_CLASS_ID;}
	const TCHAR* Category() {return GetString(IDS_CATEGORY);}
	void ResetClassParams (BOOL fileReset) {}
};


#endif // INC_ATTR_TVNODE_H_