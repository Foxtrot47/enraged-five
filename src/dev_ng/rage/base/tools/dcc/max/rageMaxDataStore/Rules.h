//
//
//    Filename: Rules.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Classes/Structures describing class selection rules
//
//
#ifndef INC_RULES_H_
#define INC_RULES_H_

#pragma warning (disable : 4786)

// STD headers
#include <string>
#include <vector>

// using namespace
using namespace rage;

class ReferenceTarget;

enum RuleType {
	OBJECT_TYPE = 0,
	OBJECT_SUPERCLASS,
	IN_DIRECTORY,
	CONTAINS_TEXT,
	XREF_IN_DIRECTORY,
	XREF_CONTAINS_TEXT,
	OBJECT_CHILD_TYPE,
	OBJECT_HAS_MODIFIER,
	OBJECT_HAS_ANIMATION,
	NUM_RULES,
};

typedef struct {
	RuleType type;
	std::string str;
	Class_ID cid;
	bool affirm;

	bool IsValidObjectType(ReferenceTarget *pRef);
	bool IsValidObjectSuperClass(ReferenceTarget *pRef);
	bool IsValidObjectChildType(INode *pNode);
	bool IsInDirectory(Interface *ip);
	bool NodeContainsString(INode *pNode);
	bool XRefIsInDirectory(INode *pNode);
	bool XRefNameContainsString(INode *pNode);
	bool HasModiferOfType(INode *pNode);
	bool HasAnimation(INode *pNode);
} Rule;


class SelectionRules
{
public:
	SelectionRules() {}
	SelectionRules(std::string name) : m_name(name) {}

	void SetName(std::string name) {m_name = name;}
	const std::string& GetName() {return m_name;}
	void SetClassName(std::string name) {m_className = name;}
	const std::string& GetClassName() {return m_className;}

	void AddRule(Rule& rule) {m_rules.push_back(rule);}
	void DeleteRule(s32 i) {m_rules.erase(m_rules.begin() + i);}
	Rule& GetRule(s32 i) {return m_rules[i];}
	s32 GetSize() {return (s32)m_rules.size();}

	bool IsValid(RefTargetHandle pNode);

	static const char *pRuleTypes[];
	static const char *pNotRuleTypes[];
	static const char *pRuleNames[];

private:
	std::string m_name;
	std::string m_className;
	std::vector<Rule> m_rules;

};


class SelectionRulesList
{
public:
	void AddSelectionRule(SelectionRules& rules) {m_selectionRules.push_back(rules);}
	void DeleteSelectionRule(s32 i) {m_selectionRules.erase(m_selectionRules.begin() + i);}
	SelectionRules& GetSelectionRule(s32 i) {return m_selectionRules[i];}
	s32 GetSize() {return (s32)m_selectionRules.size();}
	void Clear() {m_selectionRules.clear();}
private:
	std::string m_name;
	std::vector<SelectionRules> m_selectionRules;
};



#endif // INC_RULES_H_