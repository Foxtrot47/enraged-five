//
//
//    Filename: EditorUtility.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Utility plugin used to edit editor preferences
//
//
#pragma warning (disable : 4786)
#pragma warning (disable : 4251)

// my headers
#include "Configuration.h"
#include "MaxDataMgr.h"
#include "EditorUtility.h"
#include "Rules.h"
#include "RuleListDialog.h"

static EditorUtility theEditorUtility;
static EditorUtilityClassDesc theEditorUtilityClassDesc;

NameMaker *EditorUtility::m_pNameMaker = NULL;

INT_PTR CALLBACK PreferencesDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);


// --- EditorUtilityClassDesc ----------------------------------------------------------------------------

ClassDesc* GetEditorUtilityDesc() 
{
	return &theEditorUtilityClassDesc;
}

void *EditorUtilityClassDesc::Create(BOOL loading)
{
	return &theEditorUtility;
}

void EditorUtilityClassDesc::ResetClassParams (BOOL fileReset)
{
	theMaxDataMgr.Reset();
	theEditorUtility.Reset();
}

// --- EditorUtility -------------------------------------------------------------------------------------

//
//        name: EditorUtility::EditorUtility
// description: Constructor
//
EditorUtility::EditorUtility() : m_ip(NULL)
{
}

//
//        name: EditorUtility::~EditorUtility
// description: Destructor
//
EditorUtility::~EditorUtility()
{
}

//
//        name: EditorUtility::Reset
// description: Reset the name maker
//
void EditorUtility::Reset()
{
	if(m_pNameMaker)
	{
		delete m_pNameMaker;
		m_pNameMaker = NULL;
	}
}

//
//        name: EditorUtility::BeginEditParams
// description: 
//          in: 
//         out: 
//
void EditorUtility::BeginEditParams(Interface *ip,IUtil *iu)
{
	m_ip = ip;

	theMaxDataMgr.Init();

	// construct name maker if it doesn't already exist and add names to it
	if(m_pNameMaker == NULL)
	{
		m_pNameMaker = ip->NewNameMaker(FALSE);
		// add names to name maker
		SelectionRulesList& rules = theMaxDataMgr.GetRules();

		for(s32 i=0; i<rules.GetSize(); i++)
		{
			TSTR name = rules.GetSelectionRule(i).GetName().c_str();
			m_pNameMaker->AddName(name);
		}
	}

	m_hPanel = m_ip->AddRollupPage(
		hInstance,
		MAKEINTRESOURCE(IDD_PREFERENCES_PANEL),
		PreferencesDlgProc,
		_T("Preferences"));

}

//
//        name: EditorUtility::EndEditParams
// description: 
//
void EditorUtility::EndEditParams(Interface *ip,IUtil *iu)
{
	m_ip->DeleteRollupPage(m_hPanel);
	m_ip = NULL;

}

//
//        name: EditorUtility::InitUI
// description: Initialise the user interface (add items to list)
//
void EditorUtility::InitUI(HWND hWnd)
{
	HWND hList = GetDlgItem(hWnd, IDC_RULES_LIST);

	// get rules list
	SelectionRulesList& rules = theMaxDataMgr.GetRules();

	// clear list box and add in list box entries
	SendMessage(hList, LB_RESETCONTENT, 0, 0);
	for(s32 i=0; i<rules.GetSize(); i++)
	{
		SendMessage(hList, LB_INSERTSTRING, -1, reinterpret_cast<LPARAM>(rules.GetSelectionRule(i).GetName().c_str()));
	}

	UpdateUI(hWnd);
}

//
//        name: EditorUtility::UpdateUI
// description: Update editor utility UI
//
void EditorUtility::UpdateUI(HWND hWnd)
{
	HWND hFile = GetDlgItem(hWnd, IDC_ATTRIBFILE_EDIT);
	HWND hList = GetDlgItem(hWnd, IDC_RULES_LIST);
	HWND hDelete = GetDlgItem(hWnd, IDC_DELETE_BUTTON);
	HWND hEdit = GetDlgItem(hWnd, IDC_EDIT_BUTTON);
	HWND hCopy = GetDlgItem(hWnd, IDC_COPY_BUTTON);
	BOOL selected = (SendMessage(hList, LB_GETCURSEL, 0, 0) != LB_ERR);

	SendMessage(hFile, WM_SETTEXT, 0, reinterpret_cast<LPARAM>(theMaxDataMgr.GetAttributesFilename().c_str()));
	EnableWindow(hDelete, selected);
	EnableWindow(hEdit, selected);
	EnableWindow(hCopy, selected);
}

//
//        name: EditorUtility::EditAttributeFile
// description: Allow user to edit the attribute file
//
void EditorUtility::EditAttributeFile()
{
	HINSTANCE hInst = ShellExecute(m_hPanel, "open", theMaxDataMgr.GetAttributesFilename().c_str(), NULL, NULL, SW_SHOWNORMAL);
	if((s32)hInst > 32)
		return;

	MessageBox(m_hPanel, (std::string("Could not open ") + theMaxDataMgr.GetAttributesFilename()).c_str(), 
			"Attribute Class Editor", MB_OK|MB_ICONERROR);
}

//
//        name: EditorUtility::FindAttributeFile
// description: Bring up the file finder to find a attribute configuration file
//
void EditorUtility::FindAttributeFile()
{
    //
    // Get rendered files name with an open file dialog
    //
    OPENFILENAME ofn;
    char imageFilename[MAX_PATH];
    char title[32] = "Select Attribute file";
    FilterList filterList;

    // construct list of filters
    filterList.Append(_T("Attribute Files (*.att)"));
    filterList.Append(_T("*.att"));
    filterList.Append(_T("All Files (*.*)"));
    filterList.Append(_T("*.*"));

    imageFilename[0] = '\0';

	// set everything to zero
	memset(&ofn, 0, sizeof(OPENFILENAME));

    ofn.lStructSize = sizeof(OPENFILENAME);
    ofn.lpstrFilter = filterList;
    ofn.lpstrFile = imageFilename;
    ofn.nMaxFile = MAX_PATH;
	ofn.lpstrInitialDir = Config::GetConfigPath().c_str();

    ofn.lpstrTitle = title;
    ofn.Flags = OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST|OFN_HIDEREADONLY;

    if(GetOpenFileName(&ofn) == FALSE)
        return;

	theMaxDataMgr.SetAttributesFilename(ofn.lpstrFile);

	// update the user interface
	UpdateUI(m_hPanel);
}

//
//        name: EditorUtility::AddRule
// description: Add a new rule
//
void EditorUtility::AddRule()
{
	HWND hList = GetDlgItem(m_hPanel, IDC_RULES_LIST);
	SelectionRulesList& rules = theMaxDataMgr.GetRules();
	RuleListDialog dlg;
	
	dlg.m_ip = m_ip;
	dlg.m_rules.SetName("Rule");
	dlg.m_rules.SetClassName("");

	if(dlg.DoModal(hInstance, m_ip->GetMAXHWnd()))
	{
		TSTR name = dlg.m_rules.GetName().c_str();
		if(m_pNameMaker->NameExists(name))
			m_pNameMaker->MakeUniqueName(name);
		else
			m_pNameMaker->AddName(name);
		dlg.m_rules.SetName((const char *)name);

		rules.AddSelectionRule(dlg.m_rules);
		SendMessage(hList, LB_INSERTSTRING, -1, reinterpret_cast<LPARAM>(dlg.m_rules.GetName().c_str()));
	}
}

//
//        name: EditorUtility::DeleteRule
// description: Delete the currently selected rule
//
void EditorUtility::DeleteRule()
{
	HWND hList = GetDlgItem(m_hPanel, IDC_RULES_LIST);
	s32 sel = SendMessage(hList, LB_GETCURSEL, 0, 0);
	SelectionRulesList& rules = theMaxDataMgr.GetRules();

	assert(sel != LB_ERR);

	SendMessage(hList, LB_DELETESTRING, sel, 0);
	rules.DeleteSelectionRule(sel);

	UpdateUI(m_hPanel);
}

//
//        name: EditorUtility::CopyRule
// description: Copy the currently selected rule
//
void EditorUtility::CopyRule()
{
	HWND hList = GetDlgItem(m_hPanel, IDC_RULES_LIST);
	s32 sel = SendMessage(hList, LB_GETCURSEL, 0, 0);
	SelectionRulesList& rules = theMaxDataMgr.GetRules();
	assert(sel != LB_ERR);

	SelectionRules selRule = rules.GetSelectionRule(sel);
	TSTR name = selRule.GetName().c_str();

	m_pNameMaker->MakeUniqueName(name);
	selRule.SetName((const char *)name);

	rules.AddSelectionRule(selRule);
	SendMessage(hList, LB_INSERTSTRING, -1, reinterpret_cast<LPARAM>((const char *)name));

	UpdateUI(m_hPanel);
}

//
//        name: EditorUtility::EditRule
// description: Edit the currently selected rule
//
void EditorUtility::EditRule()
{
	HWND hList = GetDlgItem(m_hPanel, IDC_RULES_LIST);
	s32 sel = SendMessage(hList, LB_GETCURSEL, 0, 0);

	assert(sel != LB_ERR);

	SelectionRulesList& rules = theMaxDataMgr.GetRules();
	SelectionRules& selRule = rules.GetSelectionRule(sel);
	RuleListDialog dlg;

	dlg.m_ip = m_ip;
	dlg.m_rules = selRule; 

	if(dlg.DoModal(hInstance, m_ip->GetMAXHWnd()))
	{
		if(selRule.GetName() != dlg.m_rules.GetName())
		{
			TSTR name = dlg.m_rules.GetName().c_str();
			if(m_pNameMaker->NameExists(name))
				m_pNameMaker->MakeUniqueName(name);
			else
				m_pNameMaker->AddName(name);
			dlg.m_rules.SetName((const char *)name); 
		}
		selRule = dlg.m_rules;
		SendMessage(hList, LB_DELETESTRING, sel, 0);
		SendMessage(hList, LB_INSERTSTRING, sel, reinterpret_cast<LPARAM>(selRule.GetName().c_str()));
		SendMessage(hList, LB_SETCURSEL, sel, 0);
	}
}

//#include <process.h>

//
//        name: PreferencesDlgProc
// description: Windows dialog proc
//
static INT_PTR CALLBACK PreferencesDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg) {
	case WM_INITDIALOG:
        // Version number
        {
            TSTR buf;
            char *pVersion = GetString(IDS_VERSION);
            buf.printf("Version %s", pVersion);
            SetDlgItemText(hWnd, IDC_VERSION, buf);
        }
		theEditorUtility.InitUI(hWnd);
		break;

	case WM_DESTROY:
		break;

	case WM_COMMAND:
    	switch(LOWORD(wParam)) 
        {
		case IDC_ATTRIBFILE_BUTTON:
			theEditorUtility.FindAttributeFile();
			break;
		case IDC_EDITFILE_BUTTON:
			theEditorUtility.EditAttributeFile();
			break;
		case IDC_RULES_LIST:
			switch(HIWORD(wParam))
			{
			case LBN_SELCHANGE:
				theEditorUtility.UpdateUI(hWnd);
				break;
			case LBN_DBLCLK:
				theEditorUtility.EditRule();
				break;
			}
			break;
		case IDC_ADD_BUTTON:
			theEditorUtility.AddRule();
			break;
		case IDC_DELETE_BUTTON:
			theEditorUtility.DeleteRule();
			break;
		case IDC_EDIT_BUTTON:
			theEditorUtility.EditRule();
			break;
		case IDC_COPY_BUTTON:
			theEditorUtility.CopyRule();
			break;
        default:
            break;
        }
		break;

	default:
		return FALSE;
	}
	return TRUE;
}

