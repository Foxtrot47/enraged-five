//
//
//    Filename: DialogClass.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Class handling dialogs
//
//
#include "DialogClass.h"


//
//        name: DoModal
// description: Create popup dialog
//          in: parent window
//
s32 DialogClass::DoModal(HINSTANCE hInstance, HWND hParent)
{
	return DialogBoxParam(hInstance, MAKEINTRESOURCE(m_resId), hParent, &DialogClass::InternalDlgProc, (LPARAM)this);
}

//
//        name: InternalDlgProc
// description: The Dlg procedure function which does all the work
//
INT_PTR CALLBACK DialogClass::InternalDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	DialogClass *pDialog = NULL;

	if(msg != WM_INITDIALOG)
		pDialog = (DialogClass *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	switch(msg)
	{
	case WM_INITDIALOG:
		pDialog = (DialogClass *)lParam;
		SetWindowLongPtr(hWnd, GWLP_USERDATA, lParam);
		pDialog->m_hWnd = hWnd;
		pDialog->OnInitDialog();
		pDialog->UpdateControls();
		break;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			pDialog->UpdateData();
			pDialog->OnOK();
			break;
		case IDCANCEL:
			pDialog->OnCancel();
			break;
		default:
			pDialog->OnCommand(LOWORD(wParam), HIWORD(wParam));
			break;
		}
		break;
	case WM_DESTROY:
		pDialog->m_hWnd = NULL;
		break;
	default:
		break;
	}
	if(pDialog)
		return pDialog->DlgProc(hWnd, msg, wParam, lParam);
	else
		return FALSE;
}
