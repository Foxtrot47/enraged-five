//
//
//    Filename: RightClickMenu.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 25/06/99 14:52 $
//   $Revision: 1 $
// Description: RightClickMenu handler for attributes
//
//
#ifndef INC_RIGHT_CLICK_MENU_H_
#define INC_RIGHT_CLICK_MENU_H_

class MaxDataStore;

//
//   Class Name: AttributeMenuItem
// Base Classes: RightClickMenu
//  Description: Handler for the right click menu 'Edit attributes' item
//    Functions: 
//
//
class AttributeMenuItem : public RightClickMenu
{
public:
	void Init(RightClickMenuManager* pManager, HWND hWnd, IPoint2 m);
	void Selected(unsigned int id);

private:
	Interface *m_ip;
	INode *m_pSelected;
};


extern AttributeMenuItem theAttributeMenuItem;


#endif // INC_RIGHT_CLICK_MENU_H_