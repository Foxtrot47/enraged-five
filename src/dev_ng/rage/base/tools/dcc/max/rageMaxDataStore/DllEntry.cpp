/**********************************************************************
 *<
	FILE: DllEntry.cpp

	DESCRIPTION:Contains the Dll Entry stuff

	CREATED BY: 

	HISTORY: 

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/
#include "resource.h"

// My includes
#include "Configuration.h"
// RAGE includes
#include "system/param.h"
#include "parser/manager.h"
// STD includes
#include <sstream>

// using namespace
using namespace rage;

extern ClassDesc* GetEditorUtilityDesc();
extern ClassDesc* GetCustAttribDataClassDesc();
extern ClassDesc* GetMaxDataMgrDesc();
extern ClassDesc* GetAttrTVNodeDesc();

extern TCHAR *GetString(int id);

HINSTANCE hInstance;
int controlsInit = FALSE;

#ifdef OUTSOURCE_TOOLS

#include "lmclient.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "lm_attr.h"
#ifdef PC
#define LICPATH "32127@10.11.31.5"
#define LICPATHREMOTE "32127@194.74.241.228"
#else
#define LICPATH "@localhost:license.dat:."
#endif /* PC */

#define FEATURE "f3"
VENDORCODE code;
LM_HANDLE *lm_job;

static void init(struct flexinit_property_handle **handle)
{
#ifndef NO_ACTIVATION_SUPPORT
	struct flexinit_property_handle *ourHandle;
	int stat;

	if ((stat = lc_flexinit_property_handle_create(&ourHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit_property_handle_create() failed: %d\n", stat);
		exit(1);
	}
	if ((stat = lc_flexinit_property_handle_set(ourHandle,
		(FLEXINIT_PROPERTY_TYPE)FLEXINIT_PROPERTY_USE_TRUSTED_STORAGE,
		(FLEXINIT_VALUE_TYPE)1)) != 0)
	{
		fprintf(stderr, "lc_flexinit_property_handle_set failed: %d\n", stat);
		exit(1);
	}
	if ((stat = lc_flexinit(ourHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit failed: %d\n", stat);
		exit(1);
	}
	*handle = ourHandle;
#endif /* NO_ACTIVATION_SUPPORT */
}

static void cleanup(struct flexinit_property_handle *initHandle)
{
#ifndef NO_ACTIVATION_SUPPORT
	int stat;

	if ((stat = lc_flexinit_cleanup(initHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit_cleanup failed: %d\n", stat);
	}
	if ((stat = lc_flexinit_property_handle_free(initHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit_property_handle_free failed: %d\n", stat);
	}
#endif /* NO_ACTIVATION_SUPPORT */
}

#endif

BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	hInstance = hinstDLL;				// Hang on to this DLL's instance handle.

	if (!controlsInit) {
		controlsInit = TRUE;

#if MAX_VERSION_MAJOR < 11 
		InitCustomControls(hInstance);	// Initialize MAX's custom controls
#endif //MAX_VERSION_MAJOR

		InitCommonControls();			// Initialize Win95 controls

		// Initialize RAGE
		char* argv[1];
		argv[0] = "rageMaxDataStore.dlu";
		sysParam::Init( 1, argv );

		// Initialize the RAGE parser
		INIT_PARSER;
	}
			
	return (TRUE);
}

__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_LIBDESCRIPTION);
}

//TODO: Must change this number when adding a new class
__declspec( dllexport ) int LibNumberClasses()
{
	return 4;
}

__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) {
		case 0: 
			return GetEditorUtilityDesc();
		case 1:
			return GetCustAttribDataClassDesc();
		case 2:
			return GetMaxDataMgrDesc();
		case 3:
			return GetAttrTVNodeDesc();
		default: 
			return 0;
	}
}

__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

__declspec( dllexport ) int LibInitialize( void )
{
	int nResult = 1; // Assume successful

#ifdef OUTSOURCE_TOOLS

	char feature[64];
	char * output = NULL;
	struct flexinit_property_handle *initHandle;
	int nlic = 1;

	init(&initHandle);

	if (lc_new_job(0, lc_new_job_arg2, &code, &lm_job))
	{
		lc_perror(lm_job, "lc_new_job failed");
		cleanup(initHandle);
		exit(lc_get_errno(lm_job));
	}

	strcpy(feature, FEATURE);

	(void)lc_set_attr(lm_job, LM_A_LICENSE_DEFAULT, (LM_A_VAL_TYPE)LICPATHREMOTE);
	if(lc_checkout(lm_job, feature, "1.0", nlic, LM_CO_NOWAIT, &code, LM_DUP_NONE))
	{
		(void)lc_set_attr(lm_job, LM_A_LICENSE_DEFAULT, (LM_A_VAL_TYPE)LICPATH);

		if(lc_checkout(lm_job, feature, "1.0", nlic, LM_CO_NOWAIT, &code, LM_DUP_NONE))
		{
			lc_perror(lm_job, "checkout failed");
			cleanup(initHandle);
			exit (lc_get_errno(lm_job));
		}
	}

#endif //OUTSOURCE_TOOLS


	try
	{
		Config::Init();
	}
	catch ( ... )
	{
		std::stringstream sstr;
		sstr << "Unknown exception loading plugin.  MaxDataStore plugin disabled." << std::ends;
		MessageBox( NULL, sstr.str().c_str(), GetString(IDS_DATASTORE_NAME), MB_OK );
		nResult = 0;
	}

	return nResult;
}

__declspec( dllexport ) int LibShutdown( void )
{	
	int nResult = 1; // Assume successful

	return nResult;
}

TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	return NULL;
}
