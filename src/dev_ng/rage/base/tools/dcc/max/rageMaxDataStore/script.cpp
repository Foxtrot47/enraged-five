//
//
//    Filename: script.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: MaxScript interface to the attribute code
//
//
#pragma warning (disable : 4786)
#pragma warning (disable : 4251)

// My headers
#include "MaxDataMgr.h"
#include "DataInstance.h"

// STD headers
#include <string>

// DLL function


// ------------------------------------------------------------------------------
// 	Define our new functions
//	This will call the section operation code
// ------------------------------------------------------------------------------
def_visible_primitive( get_attr_class,				"GetAttrClass");
def_visible_primitive( get_attr_index,				"GetAttrIndex");

// These are used for platform attributes to store the global index as opposed to the class index.
def_visible_primitive( get_global_attr_index,		"GetGlobalAttrIndex");
def_visible_primitive( get_class_attr_index,		"GetClassAttrIndex");

def_visible_primitive( get_attr_type,				"GetAttrType");
def_visible_primitive( get_attr_name,				"GetAttrName");
def_visible_primitive( get_attr_parent_class_name,	"GetAttrParentClassName");
def_visible_primitive( get_attr_immediate_parent_class_name,	"GetAttrImmediateParentClassName");
def_visible_primitive( get_attr_base_class_name,	"GetAttrBaseClassName");
def_visible_primitive( get_attr_func,				"GetAttrFunc");
def_visible_primitive( set_attr_func,				"SetAttrFunc");
def_visible_primitive( delete_attr_func,			"DeleteAttrFunc");
def_visible_primitive( get_num_attr,				"GetNumAttr");
def_visible_primitive( get_attr,					"GetAttr");
def_visible_primitive( set_attr,					"SetAttr");
def_visible_primitive( edit_attr,					"EditAttr");
def_visible_primitive( copy_attr,					"CopyAttrs");
def_visible_primitive( paste_attr,					"PasteAttrs");
def_visible_primitive( del_attr,					"DelAttrs");
def_visible_primitive( get_attr_default,			"GetAttrDefault");
def_visible_primitive( get_control_data,			"GetControlData");
def_visible_primitive( get_control_type,			"GetControlType");
def_visible_primitive( get_attr_guid,				"GetAttrGuid");
def_visible_primitive( reset_attr_guid,				"ResetAttrGuid");
def_visible_primitive( edit_selected_data_model,	"InitAttrSystem");


enum eAttrFunctorType
{
	eFunctorInit,
	eFunctorChange,
	eFunctorNum
};

typedef std::map<std::string, std::string> ObjectFuncMap;

ObjectFuncMap g_ObjectInitFuncMap;
ObjectFuncMap g_ObjectChangeFuncMap;


void *AttributeInitCallback(const char* attributeName)
{
	ObjectFuncMap::iterator funcStringIterator = g_ObjectInitFuncMap.find(attributeName);
	if(funcStringIterator == g_ObjectInitFuncMap.end())
		DebugPrint("nothing in map?");
	else
		DebugPrint((*funcStringIterator).second.c_str());
	std::string scriptString((*funcStringIterator).second);
	scriptString.append(" \"");
	scriptString.append(attributeName);
	scriptString.append("\"");
	FPValue returnValue;
	BOOL success = FALSE;
	if(GetCOREInterface()->GetMAXHWnd())
	{
		try
		{
			success = ExecuteMAXScriptScript(const_cast<char*>(scriptString.c_str()),0, &returnValue);
		}
		catch (...)
		{
			MessageBox(GetCOREInterface()->GetMAXHWnd(), "Unsafe Max SDK function crash caught.", "Phew!", NULL);
		}
	}
	if(success)
	{
		switch (returnValue.type)
		{
		case TYPE_STRING:
			return new std::string(returnValue.s);
			break;
		case TYPE_BOOL:
			return new bool(returnValue.b);
			break;
		}
	}
	std::string *returnString = new std::string("Max script function execution failed:");
	//returnString->append(scriptString);
	return returnString;
}

void *AttributeChangeCallback(const char* attributeName, dmat::AttributeValue val)
{
	// Temp fixing crash bug 29175 =(
	return new String("RageMaxDataStore Attribute callbacks are disabled at the moment.");

	ObjectFuncMap::iterator funcStringIterator = g_ObjectChangeFuncMap.find(attributeName);
	if(funcStringIterator == g_ObjectChangeFuncMap.end())
		DebugPrint("nothing in map?");
	else
		DebugPrint((*funcStringIterator).second.c_str());
	std::string scriptString((*funcStringIterator).second);
	scriptString.append(" \"");
	scriptString.append(attributeName);
	scriptString.append("\" ");
	switch(val.GetType())
	{
	case dmat::AttributeValue::eAttrTypeBool:
		if((bool)val)
			scriptString.append("true");
		else
			scriptString.append("false");
		break;
	case dmat::AttributeValue::eAttrTypeString:
		scriptString.append("\"");
		scriptString.append(val);
		scriptString.append("\"");
		break;
	case dmat::AttributeValue::eAttrTypeInt:
		{
			char buffer[64];
			sprintf(buffer, "%d", (int)val);
			scriptString.append(buffer);
		}
		break;
	case dmat::AttributeValue::eAttrTypeFloat:
		{
			char buffer[64];
			sprintf(buffer, "%f", (float)val);
			scriptString.append(buffer);
		}
		break;
	default:
		scriptString.append("\"error\"");
	}
	FPValue returnValue;
	BOOL success = FALSE;
	if(GetCOREInterface()->GetMAXHWnd())
	{
		try
		{
			success = ExecuteMAXScriptScript(const_cast<char*>(scriptString.c_str()),0, &returnValue);
		}
		catch (...)
		{
			MessageBox(GetCOREInterface()->GetMAXHWnd(), "Unsafe Max SDK function crash caught.", "Phew!", NULL);
		}
	}
	if(success)
	{
		switch (returnValue.type)
		{
		case TYPE_STRING:
			return new std::string(returnValue.s);
			break;
		case TYPE_BOOL:
			return new bool(returnValue.b);
			break;
		}
	}
	std::string *returnString = new std::string("Max script function execution failed:");
	returnString->append(scriptString);
	return returnString;
}

void AppendInitCallback(std::string attrName, std::string maxscriptfunction)
{
	g_ObjectInitFuncMap.insert(std::make_pair(attrName, maxscriptfunction));
}
void DeleteInitCallback(std::string attrName)
{
	ObjectFuncMap::iterator funcStringIterator = g_ObjectInitFuncMap.find(attrName);
	if(funcStringIterator == g_ObjectInitFuncMap.end())
		DebugPrint("value not found");
	else
		g_ObjectInitFuncMap.erase(funcStringIterator);
}
std::string FindInitCallback(std::string attrName)
{
	ObjectFuncMap::iterator funcStringIterator = g_ObjectInitFuncMap.find(attrName);
	if(funcStringIterator == g_ObjectInitFuncMap.end())
		return "nothing in map?";
	else
		return (*funcStringIterator).second.c_str();
}
ObjectFuncMap &GetAttrInitFuncMap(){return g_ObjectInitFuncMap;}

// change
void *AttributeChangeCallback(const char* attributeName, dmat::AttributeValue val);
void AppendChangeCallback(std::string attrName, std::string maxscriptfunction)
{
	g_ObjectChangeFuncMap.insert(std::make_pair(attrName, maxscriptfunction));
}
void DeleteChangeCallback(std::string attrName)
{
	ObjectFuncMap::iterator funcStringIterator = g_ObjectChangeFuncMap.find(attrName);
	if(funcStringIterator == g_ObjectChangeFuncMap.end())
		DebugPrint("value not found");
	else
		g_ObjectChangeFuncMap.erase(funcStringIterator);
}
std::string FindChangeCallback(std::string attrName)
{
	ObjectFuncMap::iterator funcStringIterator = g_ObjectChangeFuncMap.find(attrName);
	if(funcStringIterator == g_ObjectChangeFuncMap.end())
		return "nothing in map?";
	else
		return (*funcStringIterator).second.c_str();
}
ObjectFuncMap &GetAttrChangeFuncMap(){return g_ObjectChangeFuncMap;}


//
//        name: *get_attr_class_cf
// description: Function for return the attribute class for a node
//
Value *get_attr_class_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr_class, 1, count);

	// Check to see if the arguments match up to what we expect
	// GunnarD: taken out for general reftarg handling
	RefTargetHandle pRef = NULL;
	if(arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &undefined;

	std::string className;
	theMaxDataMgr.GetClassFromRefTarget(pRef, className);

	// if class is not defined then return undefined
	if(!theMaxDataMgr.IsClassAvailable(className))
		return &undefined;

	// need to create a non-const char* because of bloody MaxScriptSDK
	char *pName = new char[className.length() + 1];
	String *pString;

	strcpy(pName, className.c_str());
	
	pString = new String(pName);
	delete[] pName;

	return_protected(pString);
}

//
//        name: *get_attr_index_cf
// description: Get the index of an attribute in a class
//
Value *get_attr_index_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr_index, 2, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'GetAttrIndex [class name] [attribute name]'
	type_check(arg_list[0], String, "GetAttrIndex <class name> <attribute name>");
	type_check(arg_list[1], String, "GetAttrIndex <class name> <attribute name>");

	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(arg_list[0]->to_string());
	if(pAClass == NULL)
		return &undefined;

	const char *pAttrName = arg_list[1]->to_string();

	for(s32 i=0; i<pAClass->GetSize(); i++)
	{
		if(!strcmp(pAttrName, (*pAClass)[pAClass->GetId(i)].GetName().c_str()))
			return_protected(Integer::intern(i+1));
	}

	return &undefined;
}

Value *get_global_attr_index_cf(Value** arg_list, int count)
{
	check_arg_count(get_global_attr_index, 2, count);

	type_check(arg_list[0], String, "GetGlobalAttrIndex <class name> <attribute name>");
	type_check(arg_list[1], String, "GetGlobalAttrIndex <class name> <attribute name>");

	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(arg_list[0]->to_string());
	if(pAClass == NULL)
		return &undefined;

	const char *pAttrName = arg_list[1]->to_string();

	for(s32 i=0; i<pAClass->GetSize(); i++)
	{
		if(!strcmp(pAttrName, (*pAClass)[pAClass->GetId(i)].GetName().c_str()))
		{
			s32 id = (*pAClass).GetId(i);
			return_protected(Integer::intern(id));
		}
	}

	return &undefined;
}

Value *get_class_attr_index_cf(Value** arg_list, int count)
{
	check_arg_count(get_class_attr_index, 1, count);

	type_check(arg_list[0], Integer, "GetClassAttrIndex <attribute index>");

	s32 index = (s32)arg_list[0]->to_int();

	std::list<std::string> classList;
	theMaxDataMgr.GetAttributeManager().GetClassList(classList);

	std::list<std::string>::iterator classListIterator;

	for (classListIterator = classList.begin(); classListIterator != classList.end(); classListIterator++)
	{
		dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass((*classListIterator));

		if (pAClass)
		{
			s32 classAttribute = pAClass->GetIndex(index);
			if (classAttribute != -1)
				return_protected(Integer::intern(classAttribute + 1));
		}
	}

	return &undefined;
}

//
//        name: *get_attr_type_cf
// description: Get the type of an attribute in a class
//
Value *get_attr_type_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr_type, 2, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'GetAttrType [class name] [attribute index]'
	type_check(arg_list[0], String, "GetAttrType <class name> <attribute index>");
	type_check(arg_list[1], Integer, "GetAttrType <class name> <attribute index>");

	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(arg_list[0]->to_string());
	if(pAClass == NULL)
		return &undefined;

	s32 index = arg_list[1]->to_int() - 1;

	// check attribute array index
	if(index < 0 || index >= pAClass->GetSize())
		throw RuntimeError ("Index out of range: ", arg_list[1]);

	switch((*pAClass)[pAClass->GetId(index)].GetType())
	{
	case dmat::Attribute::INT:
		return_protected(new String("int"));
	case dmat::Attribute::FLOAT:
		return_protected(new String("float"));
	case dmat::Attribute::BOOL:
		return_protected(new String("bool"));
	case dmat::Attribute::STRING:
		return_protected(new String("string"));
	}
	return &undefined;
}

//
//        name: *get_attr_name_cf
// description: Get the type of an attribute in a class
//
Value *get_attr_name_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr_name, 2, count);
	
	// Check to see if the arguments match up to what we expect
	// We want to use 'GetAttrName [class name] [attribute index]'
	type_check(arg_list[0], String, "GetAttrName <class name> <attribute index>");
	type_check(arg_list[1], Integer, "GetAttrName <class name> <attribute index>");
	
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(arg_list[0]->to_string());
	if(pAClass == NULL)
		return &undefined;
	
	s32 index = arg_list[1]->to_int() - 1;
	
	// check attribute array index
	if(index < 0 || index >= pAClass->GetSize())
		throw RuntimeError ("Index out of range: ", arg_list[1]);
	
	// need to create a non-const char* because of bloody MaxScriptSDK
	const char *pName = (*pAClass)[pAClass->GetId(index)].GetName().c_str();
	
	char *pNameCopy = new char[strlen(pName) + 1];
	String *pString;
	
	strcpy(pNameCopy, pName);
	
	pString = new String(pNameCopy);
	delete[] pNameCopy;
				
	return_protected(pString);
}

Value *get_attr_base_class_name_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr_name, 2, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'GetAttrName [class name] [attribute index]'
	type_check(arg_list[0], String, "GetAttrName <class name> <attribute index>");
	type_check(arg_list[1], Integer, "GetAttrName <class name> <attribute index>");

	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(arg_list[0]->to_string());
	if(pAClass == NULL)
		return &undefined;

	s32 index = arg_list[1]->to_int() - 1;

	// check attribute array index
	if(index < 0 || index >= pAClass->GetSize())
		throw RuntimeError ("Index out of range: ", arg_list[1]);

	// need to create a non-const char* because of bloody MaxScriptSDK
	//const char *pName = pAClass->GetItemClass(index).GetName().c_str();
	String *pString = new String(pAClass->GetItemBaseClass(index)->GetName().c_str());

	return_protected(pString);
}

//
//        name: *get_attr_name_cf
// description: Get the type of an attribute in a class
//
Value *get_attr_parent_class_name_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr_name, 2, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'GetAttrName [class name] [attribute index]'
	type_check(arg_list[0], String, "GetAttrName <class name> <attribute index>");
	type_check(arg_list[1], Integer, "GetAttrName <class name> <attribute index>");

	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(arg_list[0]->to_string());
	if(pAClass == NULL)
		return &undefined;

	s32 index = arg_list[1]->to_int() - 1;

	// check attribute array index
	if(index < 0 || index >= pAClass->GetSize())
		throw RuntimeError ("Index out of range: ", arg_list[1]);

	// need to create a non-const char* because of bloody MaxScriptSDK
	//const char *pName = pAClass->GetItemClass(index).GetName().c_str();
	String *pString = new String(pAClass->GetItemClass(index)->GetName().c_str());

	return_protected(pString);
}


Value *get_attr_immediate_parent_class_name_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr_name, 1, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'GetAttrName [class name] [attribute index]'
	type_check(arg_list[0], String, "GetAttrName <class name> <attribute index>");

	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(arg_list[0]->to_string());
	if(pAClass == NULL)
		return &undefined;

	if(pAClass->GetParent())
	{
		String *pString = new String(pAClass->GetParent()->GetName().c_str());

		return_protected(pString);
	}

	return &undefined;
}

//
//        name: *get_attr_initFunc_cf
// description:
//Get the type of an attribute in a class
//
Value *get_attr_func_cf(Value** arg_list, int count)
{
	DebugPrint("get_attr_func_cf()\n");
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr_func, 3, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'GetAttrName [class name] [attribute index]'
	type_check(arg_list[0], String, "GetAttrFunc <object type string> <functor type string> <attribute name string>");
	type_check(arg_list[1], Integer, "GetAttrFunc <object type string> <functor type integer> <attribute name string>");
	type_check(arg_list[2], String, "GetAttrFunc <object type string> <functor type string> <attribute name string>");
	char* objectType = arg_list[0]->to_string();
	int funcType = arg_list[1]->to_int();
	char* name = arg_list[2]->to_string();

	// get the attr id from class 
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(objectType);
	if(pAClass == NULL)
		return &undefined;

	s32 i=0;
	for(; i<pAClass->GetSize(); i++)
	{
		if(!strcmp(name, (*pAClass)[pAClass->GetId(i)].GetName().c_str()))
		{
			break;
		}
	}
	if(i>=pAClass->GetSize())
		return_protected(new String("Attribute not available. Belongs to another Object type?"));
	s32 id = pAClass->GetId(i);
	DebugPrint("id to look for %d\n", id);

	// get the name of the control through the id
	std::string r("no value found");
	dmat::AttributeDialogDesc *pDlgDesc = theMaxDataMgr.GetDialogDesc(objectType);
	if(!pDlgDesc)
		return_protected(new String("No value found"));
	dmat::AttributeControlDesc *pCtrlDsc = pDlgDesc->GetValueControl(id, &pDlgDesc->GetList());
	if(pCtrlDsc)
	{
		switch(funcType)
		{
		case eFunctorInit:
			{
				if(pCtrlDsc->IsInitFunctorBound())
				{
					return_protected(new String(FindInitCallback(name).c_str()));
				}
				else
					return_protected(new String("Functor is not bound"));
			}
			break;
		case eFunctorChange:
			{
				if(pCtrlDsc->IsChangeFunctorBound())
				{
					return_protected(new String(FindChangeCallback(name).c_str()));
				}
				else
					return_protected(new String("Functor is not bound"));
			}
			break;
		default:
			return_protected(new String("functor type not supported"));
		}
	}
	return_protected(new String("No value found"));
}
//
//        name: *set_attr_initFunc_cf
// description: Set the type of an attribute in a class
//
Value *set_attr_func_cf(Value** arg_list, int count)
{
	DebugPrint("get_attr_func_cf()\n");
	// Make sure we have the correct number of arguments (1)
	check_arg_count(set_attr_func, 4, count);

	// Check to see if the arguments match up to what we expect
	type_check(arg_list[0], String,  "SetAttrFunc <object type string> <functor type integer> <attribute name string> <function name string>");
	type_check(arg_list[1], Integer, "SetAttrFunc <object type string> <functor type integer> <attribute name string> <function name string>");
	type_check(arg_list[2], String,  "SetAttrFunc <object type string> <functor type integer> <attribute name string> <function name string>");
	type_check(arg_list[3], String,  "SetAttrFunc <object type string> <functor type integer> <attribute name string> <function name string>");
	char* objectType = arg_list[0]->to_string();
	int funcType = arg_list[1]->to_int();
	char* name = arg_list[2]->to_string();
	char* functionName = arg_list[3]->to_string();

	// get the attr id from class 
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(objectType);
	if(pAClass == NULL)
		return &undefined;

	s32 i=0;
	for(; i<pAClass->GetSize(); i++)
	{
		if(!strcmp(name, (*pAClass)[pAClass->GetId(i)].GetName().c_str()))
		{
			break;
		}
	}
	if(i>=pAClass->GetSize())
		return_protected(new String("Attribute not available. Belongs to another Object type?"));
	s32 id = pAClass->GetId(i);
	DebugPrint("id to look for %d\n", id);

	// get the name of the control through the id
	std::string r("no value found");
	dmat::AttributeDialogDesc *pDlgDesc = theMaxDataMgr.GetDialogDesc(objectType);
	if(!pDlgDesc)
		return_protected(new String("No value found"));
	dmat::AttributeControlDesc *pCtrlDsc = pDlgDesc->GetValueControl(id, &pDlgDesc->GetList());
	if(pCtrlDsc)
	{
		switch(funcType)
		{
		case eFunctorInit:
			{
				pCtrlDsc->GetInitFunctor() = &AttributeInitCallback;
				AppendInitCallback(name, functionName);
				return_protected(new String("Callback set"));
				break;
			}
		case eFunctorChange:
			{
				pCtrlDsc->GetChangeFunctor() = &AttributeChangeCallback;
				AppendChangeCallback(name, functionName);
				return_protected(new String("Callback set"));
				break;
			}
		default:
			return_protected(new String("functor type not supported"));
		}
	}
	return_protected(new String("No value found"));
}

//
//        name: *delete_attr_initFunc_cf
// description: Delete the type of an attribute in a class
//
Value *delete_attr_func_cf(Value** arg_list, int count)
{
	DebugPrint("delete_attr_func_cf()\n");
	// Make sure we have the correct number of arguments (1)
	check_arg_count(delete_attr_func, 3, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'GetAttrName [class name] [attribute index]'
	type_check(arg_list[0], String,  "DeleteAttrFunc <object type string> <functor type integer> <attribute name string>");
	type_check(arg_list[1], Integer, "DeleteAttrFunc <object type string> <functor type integer> <attribute name string>");
	type_check(arg_list[2], String,  "DeleteAttrFunc <object type string> <functor type integer> <attribute name string>");
	char* objectType = arg_list[0]->to_string();
	int funcType = arg_list[1]->to_int();
	char* name = arg_list[2]->to_string();

	// get the attr id from class 
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(objectType);
	if(pAClass == NULL)
		return &undefined;

	s32 i=0;
	for(; i<pAClass->GetSize(); i++)
	{
		if(!strcmp(name, (*pAClass)[pAClass->GetId(i)].GetName().c_str()))
		{
			break;
		}
	}
	if(i>=pAClass->GetSize())
		return_protected(new String("Attribute not available. Belongs to another Object type?"));
	s32 id = pAClass->GetId(i);
	DebugPrint("id to look for %d\n", id);

	// get the name of the control through the id
	std::string r("no value found");
	dmat::AttributeDialogDesc *pDlgDesc = theMaxDataMgr.GetDialogDesc(objectType);
	if(!pDlgDesc)
		return_protected(new String("No value found"));
	dmat::AttributeControlDesc *pCtrlDsc = pDlgDesc->GetValueControl(id, &pDlgDesc->GetList());
	if(pCtrlDsc)
	{
		switch(funcType)
		{
		case eFunctorInit:
			{
				pCtrlDsc->UnbindInitFunctor();
				DeleteInitCallback(name);
				return_protected(new String("Functor got unbound"));
				break;
			}
		case eFunctorChange:
			{
				pCtrlDsc->UnbindChangeFunctor();
				DeleteChangeCallback(name);
				return_protected(new String("Functor got unbound"));
				break;
			}
			break;
		default:
			return_protected(new String("functor type not supported"));
		}
	}
	return_protected(new String("No value found"));
}

Value *get_attr_guid_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr, 1, count);

	RefTargetHandle pRef = NULL;
	if(arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &undefined;

	DataInstance *pDataInstance = theMaxDataMgr.AddAttributes(pRef);
	// if data instance exists

	if(pDataInstance)
	{
		return new String(pDataInstance->GetGuidString().data());
	}
	return &undefined;
}

Value *reset_attr_guid_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr, 1, count);

	RefTargetHandle pRef = NULL;
	if(arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &undefined;

	DataInstance *pDataInstance = theMaxDataMgr.AddAttributes(pRef);
	// if data instance exists

	if(pDataInstance)
	{
		pDataInstance->ResetGuid();
		return &true_value;
	}
	else
		return &false_value;
}
//
//        name: *get_num_attr_cf
// description: Get the index of an attribute in a class
//
Value *get_num_attr_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_num_attr, 1, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'GetNumAttr [class name]'
	type_check(arg_list[0], String, "GetNumAttr <class name>");

	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(arg_list[0]->to_string());
	if(pAClass == NULL)
		return &undefined;

	return_protected(Integer::intern(pAClass->GetSize()));
}

//
//        name: *edit_selected_data_model_cf
// description: Workaround to initialize the internal data, mimics the initialization performed by the old attribute editor from quad
//
Value *edit_selected_data_model_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (0)
	check_arg_count(edit_selected_data_model, 0, count);

	Interface* ip = GetCOREInterface();
	theMaxDataMgr.StoreSelectedNodes(ip);
	theMaxDataMgr.EditSelectedDataModal(false);
	theMaxDataMgr.RestoreSelectedNodes(ip, TRUE);

	return &true_value;
}

Value *GetAttributeValue(RefTargetHandle pRef, s32 index)
{
	std::string className;

	// get class name from node and check if it is available. if it is not return undefined
	theMaxDataMgr.GetClassFromRefTarget(pRef, className);
	if(!theMaxDataMgr.IsClassAvailable(className))
		return &undefined;

	// get attribute class
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(className);

	// check attribute array index
	if(index < 0 || index >= pAClass->GetSize())
		throw RuntimeError ("Index out of range: ",Integer::intern(index));

	s32 id = pAClass->GetId(index);

	DataInstance *pDataInstance = theMaxDataMgr.AddAttributes(pRef);
	// if data instance exists

	//if the attributes attached are out of date

	if(pDataInstance)
	{
		if(pDataInstance->GetClassName() != className)
		{
			theMaxDataMgr.DeleteAttributes(pRef);
			pDataInstance = NULL;
		}
	}

	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
			return &undefined;
		const dmat::AttributeClass& aClass = pInst->GetClass();

		switch(aClass[id].GetType())
		{
		case dmat::Attribute::INT:
			{
				s32 value;
				pInst->GetValue(index, value);
				return_protected(Integer::intern(value));
			}
			break;
		case dmat::Attribute::FLOAT:
			{
				float value;
				pInst->GetValue(index, value);
				return_protected(Float::intern(value));
			}
			break;
		case dmat::Attribute::BOOL:
			{
				bool value;
				pInst->GetValue(index, value);
				if(value)
					return &true_value;
				else
					return &false_value;
			}
			break;
		case dmat::Attribute::STRING:
			{
				// need to create a non-const char* because of bloody MaxScriptSDK
				const char *pName;
				pInst->GetValue(index, pName);
				char *pNameCopy = new char[strlen(pName) + 1];
				String *pString;

				strcpy(pNameCopy, pName);

				pString = new String(pNameCopy);
				delete[] pNameCopy;

				return_protected(pString);
			}
			break;
		default:
			return &undefined;
		}

	}
	// otherwise return default value
	else
	{
		switch((*pAClass)[id].GetType())
		{
		case dmat::Attribute::INT:
			{
				s32 value = (*pAClass)[id].GetDefault();
				return_protected(Integer::intern(value));
			}
			break;
		case dmat::Attribute::FLOAT:
			{
				float value = (*pAClass)[id].GetDefault();
				return_protected(Float::intern(value));
			}
			break;
		case dmat::Attribute::BOOL:
			{
				bool value = (*pAClass)[id].GetDefault();
				if(value)
					return &true_value;
				else
					return &false_value;
			}
			break;
		case dmat::Attribute::STRING:
			{
				// need to create a non-const char* because of bloody MaxScriptSDK
				const char *pName = (*pAClass)[id].GetDefault();
				char *pNameCopy = new char[strlen(pName) + 1];
				String *pString;

				strcpy(pNameCopy, pName);

				pString = new String(pNameCopy);
				delete[] pNameCopy;

				return_protected(pString);
			}
			break;
		default:
			return &undefined;
		}
	}
}

//
//        name: *get_attr_cf
// description: Get the attribute from an object
//
Value *get_attr_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(get_attr, 2, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'GetAttr [node] [index]'
// 	type_check(arg_list[0], MAXNode, "GetAttr <node> <index>");
	type_check(arg_list[1], Integer, "GetAttr <RefTarget> <index>");

	RefTargetHandle pRef = NULL;
	if(arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &false_value;

	s32 index = arg_list[1]->to_int() - 1;

	return GetAttributeValue(pRef, index);
}



//
//        name: *set_attr
// description: Get the index of an attribute in a class
//
Value *set_attr_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(set_attr, 3, count);

	// Check to see if the arguments match up to what we expect
	// GunnarD: taken out for general reftarg handling
// 	type_check(arg_list[0], MAXNode, "SetAttr <node> <index> <value>");
	type_check(arg_list[1], Integer, "SetAttr <RefTarget> <index> <value>");

	RefTargetHandle pRef = NULL;
	if(arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &false_value;

	s32 index = arg_list[1]->to_int() - 1;
	std::string className;
	const dmat::AttributeClass* pAClass; 
	dmat::Attribute::Type attrType;
	s32 iAttr = 0;
	float fAttr = 0.0f;
	bool bAttr = false;
	char *sAttr = NULL;

	// get class name from node and check if it is available. if it is not return undefined
	theMaxDataMgr.GetClassFromRefTarget(pRef, className);
	if(!theMaxDataMgr.IsClassAvailable(className))
		return &false_value;

	pAClass = theMaxDataMgr.GetClass(className);
	assert(pAClass);

	// check attribute array index
	if(index < 0 || index >= pAClass->GetSize())
		throw RuntimeError ("Index out of range: ", arg_list[1]);

	// convert argument to correct type. This is done before the attributes are added as this may cause an exception
	// and the attribute track view node creation is buggered
	attrType = (*pAClass)[pAClass->GetId(index)].GetType();
	switch(attrType)
	{
	case dmat::Attribute::INT:
		iAttr = arg_list[2]->to_int();
		break;
	case dmat::Attribute::FLOAT:
		fAttr = arg_list[2]->to_float();
		break;
	case dmat::Attribute::BOOL:
		bAttr = (arg_list[2]->to_bool() == 1) ? true : false;
		break;
	case dmat::Attribute::STRING:
		sAttr = arg_list[2]->to_string();
		break;
	}

	DataInstance *pDataInstance = theMaxDataMgr.AddAttributes(pRef);
	dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
	assert(pInst);
	
	switch(attrType)
	{
	case dmat::Attribute::INT:
		pInst->SetValue(index, iAttr);
		break;
	case dmat::Attribute::FLOAT:
		pInst->SetValue(index, fAttr);
		break;
	case dmat::Attribute::BOOL:
		pInst->SetValue(index, bAttr);
		break;
	case dmat::Attribute::STRING:
		pInst->SetValue(index, sAttr);
		break;
	}
	
	return &true_value;
}

//
//        name: *edit_attr_cf
// description: Bring up a modal dialog to edit an attribute class
//
Value *edit_attr_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(edit_attr, 1, count);

	// Check to see if the arguments match up to what we expect
	// GunnarD: taken out for general reftarg handling
	RefTargetHandle pRef = NULL;
	if(arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &false_value;

	std::string className;

	theMaxDataMgr.GetClassFromRefTarget(pRef, className);

	// if class is not defined then return undefined
	if(!theMaxDataMgr.IsClassAvailable(className))
		return &false_value;

	theMaxDataMgr.EditDataModal(pRef);

	return &true_value;
}

Value *copy_attr_cf(Value** arg_list, int count)
{
	int numArgs = count_with_keys();

	if(numArgs>0)
	{
		type_check(arg_list[0], MAXNode, "If supplied, first argument has to be a node.");
		INode *pNode = arg_list[0]->to_node();
		if(!pNode)
			return_protected(&false_value);
		theMaxDataMgr.CopyData(pNode);
	}
	else
	{
		Interface* ip = GetCOREInterface();
		if(ip->GetSelNodeCount() < 1)
			return &false_value;
		theMaxDataMgr.CopyData(ip->GetSelNode(0));
	}

	return &true_value;
}

Value *del_attr_cf(Value** arg_list, int count)
{
	int numArgs = count_with_keys();

	if(numArgs>0)
	{
		type_check(arg_list[0], MAXNode, "If supplied, first argument has to be a node.");
		INode *pNode = arg_list[0]->to_node();
		if(!pNode)
			return_protected(&false_value);
		theMaxDataMgr.DeleteAttributes(pNode);
	}
	else
	{
		Interface* ip = GetCOREInterface();
		if(ip->GetSelNodeCount() < 1)
			return &false_value;
		theMaxDataMgr.DeleteAttributes(ip->GetSelNode(0));
	}

	return &true_value;
}

Value *paste_attr_cf(Value** arg_list, int count)
{
	int numArgs = count_with_keys();

	if(numArgs>0)
	{
		type_check(arg_list[0], MAXNode, "If supplied, first argument has to be a node.");
		INode *pNode = arg_list[0]->to_node();
		if(!pNode)
			return_protected(&false_value);
		theMaxDataMgr.PasteData(pNode);
	}
	else
	{
		Interface* ip = GetCOREInterface();
		s32 i,numSelected = ip->GetSelNodeCount();
		INode* pSelected;

		for(i=0; i<numSelected; i++)
		{
			pSelected = ip->GetSelNode(i);
			Object* pObj = pSelected->EvalWorldState(ip->GetTime()).obj;
			if(!theMaxDataMgr.IsCopyAvailable(pSelected))
				break;
		}

		if(i == numSelected)
		{
			for(s32 i=0; i<ip->GetSelNodeCount(); i++)
			{	
				theMaxDataMgr.PasteData(ip->GetSelNode(i));
			}
		}
		else
		{
			return &false_value;		
		}
	}

	return &true_value;
}

Value *GetAttributeDefault(dmat::AttributeClass* pAClass, s32 index)
{
	dmat::Attribute& attribute = pAClass->GetItem(index);
	dmat::Attribute::Type attrType = attribute.GetType();
	dmat::AttributeValue attrValue = attribute.GetDefault();

	switch(attrType)
	{
	case dmat::Attribute::INT:
		{
			s32 value = (s32)attrValue;
			return_protected(Integer::intern(value));
		}
		break;
	case dmat::Attribute::FLOAT:
		{
			float value = (float)attrValue;
			return_protected(Float::intern(value));
		}
		break;
	case dmat::Attribute::BOOL:
		{
			bool value = attrValue;
			if(value)
				return ( &true_value );
			else
				return ( &false_value );
		}
		break;
	case dmat::Attribute::STRING:
		{
			// need to create a non-const char* because of bloody MaxScriptSDK
			const char *pName = attrValue;
			char *pNameCopy = new char[strlen(pName) + 1];
			String *pString;

			strcpy(pNameCopy, pName);

			pString = new String(pNameCopy);
			delete[] pNameCopy;

			return_protected(pString);
		}
		break;
	default:
		return ( &false_value );
	}
}
Value *get_attr_default_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (2)
	check_arg_count(get_num_attr, 2, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'GetNumAttr [class name]'
	type_check(arg_list[0], String, "GetAttrDefault <class name> <attr index>");
	type_check(arg_list[1], Integer, "GetAttrDefault <class name> <attr index>");

	dmat::AttributeClass* pAClass = theMaxDataMgr.GetClass(arg_list[0]->to_string());
	if (NULL == pAClass)
		return &undefined;
	s32 index = arg_list[1]->to_int() - 1;

	// check attribute array index
	if (index < 0 || index >= pAClass->GetSize())
		throw RuntimeError ("Index out of range: ", arg_list[1]);

	return GetAttributeDefault(pAClass, index);
}

Value *get_control_type_cf(Value** arg_list, int count)
{
	DebugPrint("get_control_type_cf()\n");

	// Check to see if the arguments match up to what we expect
	type_check(arg_list[0], String, "GetControlType <object type string> <attribute name string> [<node>]");
	type_check(arg_list[1], String, "GetControlType <object type string> <attribute name string> [<node>]");
	char* objectType = arg_list[0]->to_string();
	char* name = arg_list[1]->to_string();

	// get the attr id from class 
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(objectType);
	if(pAClass == NULL)
		return &undefined;

	s32 i=0;
	for(; i<pAClass->GetSize(); i++)
	{
		if(!strcmp(name, (*pAClass)[pAClass->GetId(i)].GetName().c_str()))
		{
			break;
		}
	}
	if(i>=pAClass->GetSize())
	{
		mprintf("Attribute %s not available in %s. Belongs to another Object type?\n", name, objectType);
		return &undefined;
	}
	s32 id = pAClass->GetId(i);
	DebugPrint("id to look for %d\n", id);

	// get the name of the control through the id
	std::string r("no value found");
	dmat::AttributeDialogDesc *pDlgDesc = theMaxDataMgr.GetDialogDesc(objectType);
	if(!pDlgDesc)
	{
		mprintf("No Dialog description found for %s.", objectType);
		return &undefined;
	}

	dmat::AttributeControlDesc *pCtrlDsc = pDlgDesc->GetValueControl(id);
	if(pCtrlDsc)
	{
		switch(pCtrlDsc->GetType())
		{
		case dmat::AttributeControlDesc::SPINNER:
			return_protected(new String("spinner"));
			break;
		case dmat::AttributeControlDesc::GROUPBOX:
			return_protected(new String("groupbox"));
			break;
		case dmat::AttributeControlDesc::EDITBOX:
			return_protected(new String("editbox"));
			break;
		case dmat::AttributeControlDesc::COMBOBOX:
		case dmat::AttributeControlDesc::TCOMBOBOX:
			return_protected(new String("combobox"));
			break;
		case dmat::AttributeControlDesc::CHECKBOX:
			return_protected(new String("checkbox"));
			break;
		case dmat::AttributeControlDesc::FILEBROWSER:
			return_protected(new String("filebrowser"));
			break;
		default:
			break;
		}
	}
//	mprintf("Control type could not be resolved.\n");
	return &undefined;
}

Value *get_control_data_cf(Value** arg_list, int count)
{
	DebugPrint("get_control_data()\n");
	// Make sure we have the correct number of arguments (1)
//	check_arg_count(get_control_limits, 2, count);

	// Check to see if the arguments match up to what we expect
	type_check(arg_list[0], String, "GetControlData <object type string> <attribute name string> [<node>]");
	type_check(arg_list[1], String, "GetControlData <object type string> <attribute name string> [<node>]");
	char* objectType = arg_list[0]->to_string();
	char* name = arg_list[1]->to_string();

	RefTargetHandle pRef = NULL;
	if(count>2)
	{
		if(arg_list[2]->is_kind_of(class_tag(MAXWrapper)))
			pRef = ((MAXWrapper*)arg_list[2])->GetReference(0);
		else
		{
			mprintf("GetControlData <object type string> <attribute name string> [<node>]\n");
			return &undefined;
		}
	}

	// get the attr id from class 
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(objectType);
	if(pAClass == NULL)
		return &undefined;

	s32 i=0;
	for(; i<pAClass->GetSize(); i++)
	{
		if(!strcmp(name, (*pAClass)[pAClass->GetId(i)].GetName().c_str()))
		{
			break;
		}
	}
	if(i>=pAClass->GetSize())
	{
		mprintf("Attribute %s not available in %s. Belongs to another Object type?\n", name, objectType);
		return &undefined;
	}
	s32 id = pAClass->GetId(i);
	DebugPrint("id to look for %d\n", id);

	// get the name of the control through the id
	std::string r("no value found");
	dmat::AttributeDialogDesc *pDlgDesc = theMaxDataMgr.GetDialogDesc(objectType);
	if(!pDlgDesc)
	{
		mprintf("No Dialog description found for %s.", objectType);
		return &undefined;
	}

	dmat::AttributeControlDesc *pCtrlDsc = pDlgDesc->GetValueControl(id);
	std::string maxstring;
	if(!pCtrlDsc)
	{
		mprintf("Attribute %s invisible. no defaults available.\n", name);
		Value *val;
		if(pRef)
			val = GetAttributeValue(pRef, i);
		else
			val = GetAttributeDefault(pAClass, i);
		MCHAR buffer[2048];
		if(val->is_kind_of(class_tag(Float)))
			sprintf(buffer, "[%f,%f,%f]", -10000, 10000, val->to_float());
		else if(val->is_kind_of(class_tag(Integer)))
			sprintf(buffer, "[%d,%d,%d]", -10000, 10000, val->to_int());
		else if(val->is_kind_of(class_tag(String)))
			sprintf(buffer, "%s", val->to_string());
		maxstring.append(buffer);
		return_protected(new String(maxstring.data()));
	}
	else
	{
		switch(pCtrlDsc->GetType())
		{
		case dmat::AttributeControlDesc::TCOMBOBOX:
			{
				dmat::StringComboBoxControlDesc *pCombo = dynamic_cast<dmat::StringComboBoxControlDesc *>(pCtrlDsc);
				return_protected(new String(pCombo->GetEntriesString().c_str()));
			}
			break;
		case dmat::AttributeControlDesc::COMBOBOX:
			{
				dmat::ComboBoxControlDesc *pCombo = dynamic_cast<dmat::ComboBoxControlDesc *>(pCtrlDsc);
				return_protected(new String(pCombo->GetEntriesString().c_str()));
			}
			break;
		case dmat::AttributeControlDesc::SPINNER:
			{
				dmat::AttributeValueControlDesc *pValCtrlDsc = NULL;
				if(pCtrlDsc)
					pValCtrlDsc = pCtrlDsc->AsValueControl();
				if(!pValCtrlDsc)
				{
					mprintf("Couldn't convert control to Value class.\n");
					return &undefined;
				}
				Value *val;
				if(pRef)
					val = GetAttributeValue(pRef, i);
				else
					val = GetAttributeDefault(pAClass, i);
				switch(pValCtrlDsc->GetEditType())
				{
				case dmat::Attribute::Type::FLOAT:
					{
						dmat::FloatAttribControlDesc *pFloat = dynamic_cast<dmat::FloatAttribControlDesc *>(pCtrlDsc);
						float minf;
						float maxf;
						pFloat->GetLimits(minf, maxf);
						MCHAR buffer[2048];
						sprintf(buffer, "[%f,%f,%f]", minf, maxf, val->to_float());
						maxstring.append(buffer);
						return_protected(new String(maxstring.data()));
					}
					break;
				case dmat::Attribute::Type::INT:
					{
						dmat::IntAttribControlDesc *pInt = dynamic_cast<dmat::IntAttribControlDesc *>(pCtrlDsc);
						int mini;
						int maxi;
						float vali;
						pInt->GetLimits(mini, maxi);
						MCHAR buffer[2048];
						sprintf(buffer, "[%d,%d,%d]", mini, maxi, val->to_int());
						maxstring.append(buffer);
						return_protected(new String(maxstring.data()));
					}
					break;
				}
			}
			break;
		default:
			mprintf("Control type not supported.\n");
			return &undefined;
			break;
		}
	}
	return &undefined;
}
