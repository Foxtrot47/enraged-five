//
//
//    Filename: CustAttribData.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: 
//
//
#pragma warning (disable : 4786)
#pragma warning (disable : 4251)

#include "MaxDataMgr.h"
#include "CustAttribData.h"
#include <AttribDialog.h>

#define CUSTATTRIB_ATTR_CHUNK		0x4e52
#define CUSTATTRIB_ATTR_GUID_CHUNK	0x7829

CustAttribDataClassDesc theCustAttribDataDesc;
ClassDesc* GetCustAttribDataClassDesc() {return &theCustAttribDataDesc;}


//
//        name: AttributeDialogProc
// description: Dialog procedure for the attribute dialog
//
BOOL CALLBACK RollupDialogProc( HWND hwndDlg, UINT msg, WPARAM wParam, LPARAM lParam )
{
	dmat::AttributeDialog *pDialog;
	switch(msg)
	{
	case WM_INITDIALOG:
		pDialog = (dmat::AttributeDialog *)lParam;
		pDialog->InitDialog(hwndDlg);
		SetWindowLongPtr(hwndDlg, GWLP_USERDATA, lParam);
		break;
	case WM_NOTIFY:
		break;

	case WM_COMMAND:
		// codes sent when focus is given to an edit box
		if(HIWORD(wParam) == 0x100)
			DisableAccelerators();
		// and when edit box loses focus
		else if(HIWORD(wParam) == 0x200)
			EnableAccelerators();
		/*pDialog = (dmat::AttributeDialog *)GetWindowLong(hwndDlg, GWL_USERDATA);
		if(pDialog)
			pDialog->SyncWithControls();*/
		break;

	case WM_CLOSE:
		pDialog = (dmat::AttributeDialog *)GetWindowLongPtr(hwndDlg, GWLP_USERDATA);
		if(pDialog)
			pDialog->SyncWithControls();
		break;

	}
	return FALSE;
}
 
RefResult CustAttribData::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,	PartID& partID,  RefMessage message)
{
	

	return REF_SUCCEED;
}

//
//        name: PostLoadCallback
// description: PostLoadCallback used to delete TVNodes with no attribute data attached
//
class RemoveCustAttribPostLoadCallback : public PostLoadCallback
{
public:
	RemoveCustAttribPostLoadCallback(CustAttribData *pAttrib) : m_pCustAttrib(pAttrib) {}
	void proc(ILoad *iload)
	{
	}
private:
	CustAttribData* m_pCustAttrib;
};
/*
void CustAttribData::BaseClone(ReferenceTarget* from,ReferenceTarget* to,RemapDir & remap)
{
	//CustAttrib::BaseClone(from,to,remap);
	printf("as\n");
}
*/
//
//        name: CustAttribData::Clone
// description: copy function
//
ReferenceTarget *CustAttribData::Clone(RemapDir &remap)
{
	CustAttribData* pCustAttrib = new CustAttribData;

	pCustAttrib->GetData() = m_attr;

//	BaseClone(this,pCustAttrib,remap);

	return pCustAttrib;
}

//
//        name: CustAttribData::BeginEditParams
// description: create UI for editing
//
void CustAttribData::BeginEditParams(IObjParam *ip,ULONG flags,Animatable *prev)
{
// 	m_hWnd = NULL;
// 
// #if 0

	dmat::AttributeDialog *pDialog;
	
	//pDialog = NULL;
	pDialog = static_cast<dmat::AttributeDialog *>(m_attr.ConstructRollupPage());

	if(pDialog == NULL)
		return;

	DLGTEMPLATE* dlgTemplate = pDialog->GetTemplate();
	m_hWnd = ip->AddRollupPage(hInstance, 
					pDialog->GetTemplate(),
					(DLGPROC)RollupDialogProc,
					"Attributes",
					(LPARAM)pDialog);

//#endif
}

//
//        name: CustAttribData::EndEditParams
// description: remove UI for editing
//
void CustAttribData::EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)
{
	if(m_hWnd)
	{
		RemoveRollup(m_hWnd, ip);
		m_hWnd = NULL;
	}
}

//
//        name: CustAttribData::RemoveRollup
// description: remove UI for editing
//
void CustAttribData::RemoveRollup(HWND hWnd, Interface* ip)
{
	if(hWnd == NULL)
		return;

	dmat::AttributeDialog *pDialog = (dmat::AttributeDialog *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
//	if(pDialog)
//		pDialog->SyncWithControls();
	ip->DeleteRollupPage(hWnd);
	if(pDialog)
		delete pDialog;
}

//
//        name: CustAttribData::BeginEditParams
// description: create UI for editing
//
void CustAttribData::BeginEditParams(IMtlParams *iMtlPrms,ULONG flags,Animatable *prev)
{
	// 	m_hWnd = NULL;
	// 
	// #if 0

	dmat::AttributeDialog *pDialog;

	//pDialog = NULL;
	pDialog = static_cast<dmat::AttributeDialog *>(m_attr.ConstructRollupPage(true));

	if(pDialog == NULL)
		return;

	DLGTEMPLATE* dlgTemplate = pDialog->GetTemplate();
	m_hWnd = iMtlPrms->AddRollupPage(hInstance, 
		pDialog->GetTemplate(),
		(DLGPROC)RollupDialogProc,
		"Attributes",
		(LPARAM)pDialog);

	//#endif
}

//
//        name: CustAttribData::RemoveRollup
// description: remove UI for editing
//
void CustAttribData::RemoveRollup(HWND hWnd, IMtlParams *iMtlPrms)
{
	if(hWnd == NULL)
		return;

	dmat::AttributeDialog *pDialog = (dmat::AttributeDialog *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	//	if(pDialog)
	//		pDialog->SyncWithControls();
	iMtlPrms->DeleteRollupPage(hWnd);
	if(pDialog)
		delete pDialog;
}

//
//        name: CustAttribData::CheckCopyAttribTo
// description: returns if we can copy the attributes
//
bool CustAttribData::CheckCopyAttribTo(ICustAttribContainer *to)
{
	return false;
}

//
//        name: CustAttribData::Load
// description: load attribute data
//
IOResult CustAttribData::Load(ILoad *iload)
{
	// load slot queue
	IOResult res;
	unsigned long length;

	while (IO_OK == (res = iload->OpenChunk())) {
		switch(iload->CurChunkID())  
		{
		case CUSTATTRIB_ATTR_CHUNK:
			{
				s32 size = (s32)iload->CurChunkLength();
				char *pData = new char[size+1];
				s32 dataSize;
				char *pName;
				char *pAttributes;

				res = iload->Read(pData, size, &length);
				// add a end of string character at the end of the data so that the class name has an 
				// ending
				pData[size] = '\0';

				// get index, datasize, attribute data and attribute class name
				dataSize = *reinterpret_cast<s32 *>(pData);
				pAttributes = pData + 4;
				pName = pAttributes + dataSize;
				m_attr.AddAttributes(pName);
				if(m_attr.GetAttributes())
					m_attr.GetAttributes()->ReadRawData(pAttributes, dataSize);
				else
				{
					Interface *ip = GetCOREInterface();
					SetInvalid();
					//if(ip)
					//	iload->RegisterPostLoadCallback(new RemoveCustAttribPostLoadCallback(this));
				}
				delete[] pData;
			}
			break;
		case CUSTATTRIB_ATTR_GUID_CHUNK:
			{
				s32 size = (s32)iload->CurChunkLength();
				void *guidBuffer = new byte[size+1];
				res = iload->Read(guidBuffer, size, (ULONG*)&length);
				if(res!=IO_OK)
					mprintf("Failed to read in GUID for Attribute Datainstance.");
				else
					m_attr.SetGuid(*(GUID *)guidBuffer);
			}
			break;
		default:
			break;
		}
		iload->CloseChunk();
		if (res != IO_OK) 
			return res;
	}
	return IO_OK;
}

//
//        name: CustAttribData::Save
// description: save attribute data
//
IOResult CustAttribData::Save(ISave *isave)
{
	s32 size;
	unsigned long length;
	char *pData = NULL;

	if(m_attr.GetAttributes())
		pData = m_attr.GetAttributes()->CreateRawData(size);
	else
		size = 0;
	
	isave->BeginChunk(CUSTATTRIB_ATTR_CHUNK);
	isave->Write(&size, sizeof(s32), &length);	// size of attribute data
	if(size)
		isave->Write(pData, size, &length);				// attribute data 
	isave->WriteCString(m_attr.GetClassName().c_str());	// attribute class name
	isave->EndChunk();

	isave->BeginChunk(CUSTATTRIB_ATTR_GUID_CHUNK);
	isave->Write(&m_attr.GetGuid(), sizeof(GUID), (ULONG*)&length);
	isave->EndChunk();
	
	delete[] pData;

	return IO_OK;
}


