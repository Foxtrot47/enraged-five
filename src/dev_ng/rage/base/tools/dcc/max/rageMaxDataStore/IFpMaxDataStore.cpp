//
//
//    Filename: IFPMaxDataStore.cpp
//     Creator: Gunnar Droege
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Static Max Interface class to MaxDataStore plugin
//
//
// Windows resource IDs
// #pragma warning (disable : 4786)
// #pragma warning (disable : 4251)

#include "resource.h"
// My headers
#include "IfpMaxDataStore.h"
#include "MaxDataMgr.h"
#include "DataInstance.h"
#include <DialogTemplate.h>
#include <AttribDialog.h>
#include "CustAttribData.h"

extern HINSTANCE hInstance;
//-----------------------------------------------------------------------------

class FPMaxDataStore : public IFpMaxDataStore
{
public:
	TSTR	GetAttrClass(INode *pNode);
	int		GetAttrIndex(TSTR className, TSTR attrName);
	TSTR	GetAttrType(TSTR className, int attrIndex);
	TSTR	GetAttrName(TSTR className, int attrIndex);
	int		GetNumAttr(TSTR className);

	int		GetAttrInt(INode *pNode, int attrIndex);
	TSTR	GetAttrStr(INode *pNode, int attrIndex);
	float	GetAttrFloat(INode *pNode, int attrIndex);
	bool	GetAttrBool(INode *pNode, int attrIndex);
	Value*	GetAttr(INode *pNode, int attrIndex);

	void	SetAttrInt(INode *pNode, int attrIndex, int val);
	void	SetAttrStr(INode *pNode, int attrIndex, TSTR val);
	void	SetAttrFloat(INode *pNode, int attrIndex, float val);
	void	SetAttrBool(INode *pNode, int attrIndex, bool val);
	Value*	SetAttr(INode *pNode, int attrIndex, Value *val);

	TSTR	GetGuidString(INode *pNode);

	DECLARE_DESCRIPTOR(FPMaxDataStore);
	BEGIN_FUNCTION_MAP;
	FN_1(IFpMaxDataStore::em_getAttrClass, TYPE_TSTR_BV, GetAttrClass, TYPE_INODE);
	FN_2(IFpMaxDataStore::em_getAttrIndex, TYPE_INT, GetAttrIndex, TYPE_TSTR_BV, TYPE_TSTR_BV);
	FN_2(IFpMaxDataStore::em_getAttrType, TYPE_TSTR_BV, GetAttrType, TYPE_TSTR_BV, TYPE_INT);
	FN_2(IFpMaxDataStore::em_getAttrName, TYPE_TSTR_BV, GetAttrName, TYPE_TSTR_BV, TYPE_INT);
	FN_1(IFpMaxDataStore::em_getNumAttr, TYPE_INT, GetNumAttr, TYPE_TSTR_BV);
	
	FN_2(IFpMaxDataStore::em_getAttrInt, TYPE_INT, GetAttrInt, TYPE_INODE, TYPE_INT);
	FN_2(IFpMaxDataStore::em_getAttrStr, TYPE_TSTR_BV, GetAttrStr, TYPE_INODE, TYPE_INT);
	FN_2(IFpMaxDataStore::em_getAttrFloat, TYPE_FLOAT, GetAttrFloat, TYPE_INODE, TYPE_INT);
	FN_2(IFpMaxDataStore::em_getAttrBool, TYPE_BOOL, GetAttrBool, TYPE_INODE, TYPE_INT);
	FN_2(IFpMaxDataStore::em_getAttr, TYPE_VALUE, GetAttr, TYPE_INODE, TYPE_INT);

	VFN_3(IFpMaxDataStore::em_setAttrInt, SetAttrInt, TYPE_INODE, TYPE_INT, TYPE_INT);
	VFN_3(IFpMaxDataStore::em_setAttrStr, SetAttrStr, TYPE_INODE, TYPE_INT, TYPE_STRING);
	VFN_3(IFpMaxDataStore::em_setAttrFloat, SetAttrFloat, TYPE_INODE, TYPE_INT, TYPE_FLOAT);
	VFN_3(IFpMaxDataStore::em_setAttrBool, SetAttrBool, TYPE_INODE, TYPE_INT, TYPE_BOOL);
	FN_3(IFpMaxDataStore::em_setAttr, TYPE_VALUE, SetAttr, TYPE_INODE, TYPE_INT, TYPE_VALUE);

	FN_1(IFpMaxDataStore::em_getGuid, TYPE_STRING, GetGuidString, TYPE_INODE, TYPE_INT, TYPE_VALUE);

	END_FUNCTION_MAP;
};

FPMaxDataStore FpMaxDataStoreDesc(
								  FP_MAXDATASTORE_INTERFACE,
								  _T("RsDataStore"),
								  IDS_DATASTORE_NAME,
								  NULL,
								  FP_CORE,

								  IFpMaxDataStore::em_getAttrClass, _T("getAttrClass"), 0, TYPE_TSTR_BV, 0, 1,
								  _T("node"), 0, TYPE_INODE,
								  IFpMaxDataStore::em_getAttrIndex, _T("getAttrIndex"), 0 ,TYPE_INT, 0, 2,
								  _T("class name"), 0, TYPE_TSTR_BV,
								  _T("attrib name"), 0, TYPE_TSTR_BV,
								  IFpMaxDataStore::em_getAttrType, _T("getAttrType"), 0, TYPE_TSTR_BV, 0, 2,
								  _T("class name"), 0, TYPE_TSTR_BV,
								  _T("attrib index"), 0, TYPE_INT,
								  IFpMaxDataStore::em_getAttrName, _T("getAttrName"), 0, TYPE_TSTR_BV, 0, 2,
								  _T("class name"), 0, TYPE_TSTR_BV,
								  _T("attrib index"), 0, TYPE_INT,
								  IFpMaxDataStore::em_getNumAttr, _T("getNumAttr"), 0, TYPE_INT, 0, 1,
								  _T("class name"), 0, TYPE_TSTR_BV,
								  IFpMaxDataStore::em_getAttrInt, _T("getAttrInt"), 0, TYPE_INT, 0, 2,
								  _T("node"), 0, TYPE_INODE,
								  _T("attrib index"), 0, TYPE_INT,
								  IFpMaxDataStore::em_getAttrStr, _T("getAttrStr"), 0, TYPE_TSTR_BV, 0, 2,
								  _T("node"), 0, TYPE_INODE,
								  _T("attrib index"), 0, TYPE_INT,
								  IFpMaxDataStore::em_getAttrFloat, _T("getAttrFloat"), 0, TYPE_FLOAT, 0, 2,
								  _T("node"), 0, TYPE_INODE,
								  _T("attrib index"), 0, TYPE_INT,
								  IFpMaxDataStore::em_getAttrBool, _T("getAttrBool"), 0, TYPE_BOOL, 0, 2,
								  _T("node"), 0, TYPE_INODE,
								  _T("attrib index"), 0, TYPE_INT,
								  IFpMaxDataStore::em_getAttr, _T("getAttr"), 0, TYPE_VALUE, 0, 2,
								  _T("node"), 0, TYPE_INODE,
								  _T("attrib index"), 0, TYPE_INT,
								  IFpMaxDataStore::em_setAttrInt, _T("setAttrInt"), 0, TYPE_VOID, 0, 3,
								  _T("node"), 0, TYPE_INODE,
								  _T("attrib index"), 0, TYPE_INT,
								  _T("value"), 0, TYPE_INT,
								  IFpMaxDataStore::em_setAttrStr, _T("setAttrStr"), 0, TYPE_VOID, 0, 3,
								  _T("node"), 0, TYPE_INODE,
								  _T("attrib index"), 0, TYPE_INT,
								  _T("value"), 0, TYPE_STRING,
								  IFpMaxDataStore::em_setAttrFloat, _T("setAttrFloat"), 0, TYPE_VOID, 0, 3,
								  _T("node"), 0, TYPE_INODE,
								  _T("attrib index"), 0, TYPE_INT,
								  _T("value"), 0, TYPE_FLOAT,
								  IFpMaxDataStore::em_setAttrBool, _T("setAttrBool"), 0, TYPE_VOID, 0, 3,
								  _T("node"), 0, TYPE_INODE,
								  _T("attrib index"), 0, TYPE_INT,
								  _T("value"), 0, TYPE_BOOL,
								  IFpMaxDataStore::em_setAttr, _T("setAttr"), 0, TYPE_VALUE, 0, 3,
								  _T("node"), 0, TYPE_INODE,
								  _T("attrib index"), 0, TYPE_INT,
								  _T("value"), 0, TYPE_VALUE,
								  IFpMaxDataStore::em_getGuid, _T("getGuid"), 0, TYPE_TSTR_BV, 0, 1,
								  _T("node"), 0, TYPE_INODE,

								  end
								  );

//////////////////////////////////////////////////////////////////////////
// Helpers
//////////////////////////////////////////////////////////////////////////

DataInstance* GetDatInstanceFromNode(INode *pNode, dmat::AttributeClass *&pAClass)
{
	std::string className;
	theMaxDataMgr.GetClassFromRefTarget(pNode, className);

	if(!theMaxDataMgr.IsClassAvailable(className))
	{
		return 0;	
	}

	pAClass = theMaxDataMgr.GetClass(className);

	DataInstance *pDataInstance = theMaxDataMgr.AddAttributes(pNode);

	if(pDataInstance)
	{
		if(pDataInstance->GetClassName() != className)
		{
			//Attributes are out of date..
			return NULL;
		}

		return pDataInstance;
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////
// Script function definitions
//////////////////////////////////////////////////////////////////////////

TSTR FPMaxDataStore::GetAttrClass(INode *pNode)
{
	std::string className;
	theMaxDataMgr.GetClassFromRefTarget(pNode, className);

	// if class is not defined then return undefined
	if(!theMaxDataMgr.IsClassAvailable(className))
		return _T("");
	else
		return TSTR(className.c_str());
}

int FPMaxDataStore::GetAttrIndex(TSTR className, TSTR attrName)
{
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass((const char*)className);
	if(pAClass == NULL)
		return -1;

	for(int i=0; i<pAClass->GetSize(); i++)
	{
		if(!_strcmpi((const char*)attrName, (*pAClass)[pAClass->GetId(i)].GetName().c_str()))
			return i;
	}

	return -1;
}
TSTR FPMaxDataStore::GetAttrType(TSTR className, int attrIndex)
{
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass((const char*)className);
	if(pAClass == NULL)
		return _T("");

	if(attrIndex < 0 || attrIndex > pAClass->GetSize())
		return _T("");

	switch((*pAClass)[pAClass->GetId(attrIndex)].GetType())
	{
	case dmat::Attribute::INT:
		return _T("int");
	case dmat::Attribute::FLOAT:
		return _T("float");
	case dmat::Attribute::BOOL:
		return _T("bool");
	case dmat::Attribute::STRING:
		return _T("string");
	}

	return _T("");
}

TSTR FPMaxDataStore::GetAttrName(TSTR className, int attrIndex)
{
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass((const char*)className);
	if(pAClass == NULL)
		return _T("");

	if(attrIndex < 0 || attrIndex > pAClass->GetSize())
		return _T("");

	return _T((*pAClass)[pAClass->GetId(attrIndex)].GetName().c_str());
}

int FPMaxDataStore::GetNumAttr(TSTR className)
{
	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass((const char*)className);
	if(pAClass == NULL)
		return -1;

	return (pAClass->GetSize());
}

//////////////////////////////////////////////////////////////////////////
// getattr functions
//////////////////////////////////////////////////////////////////////////

int	FPMaxDataStore::GetAttrInt(INode *pNode, int attrIndex)
{
	dmat::AttributeClass *pAClass = NULL;
	std::string className;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	int id = pAClass->GetId(attrIndex);

	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
		{
			return 0;
		}

		const dmat::AttributeClass& aClass = pInst->GetClass();

	int id = pAClass->GetId(attrIndex);
		if(aClass[id].GetType() == dmat::Attribute::INT)
		{
			int value;
			pInst->GetValue(attrIndex, value);
			return value;
		}
		else
		{
			//The method was called for an attribute that isn't an int type
			return 0;
		}
	}
	else
	{
		int value = (*pAClass)[id].GetDefault();
		return value;
	}
}

TSTR FPMaxDataStore::GetAttrStr(INode *pNode, int attrIndex)
{
	dmat::AttributeClass *pAClass = NULL;
	std::string className;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	int id = pAClass->GetId(attrIndex);

	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
			return _T("");

		const dmat::AttributeClass& aClass = pInst->GetClass();

		if(aClass[id].GetType() == dmat::Attribute::STRING)
		{
			const char *pValue;
			pInst->GetValue(attrIndex, pValue);
			return _T(pValue);
		}
		else
		{
			//The method was called for an attribute that isn't a string type
			return _T("");
		}
	}
	else
	{
		const char *pValue = (*pAClass)[id].GetDefault();
		return _T(pValue);
	}
}

float FPMaxDataStore::GetAttrFloat(INode *pNode, int attrIndex)
{
	dmat::AttributeClass *pAClass = NULL;
	std::string className;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	int id = pAClass->GetId(attrIndex);

	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
			return 0.0f;

		const dmat::AttributeClass& aClass = pInst->GetClass();

		if(aClass[id].GetType() == dmat::Attribute::FLOAT)
		{
			float value;
			pInst->GetValue(attrIndex, value);
			return value;
		}
		else
		{
			//The method was called for an attribute that isn't an int type
			return 0.0f;
		}
	}
	else
	{
		float value = (*pAClass)[id].GetDefault();
		return value;
	}
}

bool FPMaxDataStore::GetAttrBool(INode *pNode, int attrIndex)
{
	dmat::AttributeClass *pAClass = NULL;
	std::string className;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	int id = pAClass->GetId(attrIndex);

	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
			return false;

		const dmat::AttributeClass& aClass = pInst->GetClass();

		if(aClass[id].GetType() == dmat::Attribute::BOOL)
		{
			bool value;
			pInst->GetValue(attrIndex, value);
			return value;
		}
		else
		{
			//The method was called for an attribute that isn't an int type
			return 0.0f;
		}
	}
	else
	{
		bool value = (*pAClass)[id].GetDefault();
		return value;
	}
}

Value* FPMaxDataStore::GetAttr(INode *pNode, int attrIndex)
{
	dmat::AttributeClass *pAClass = NULL;
	std::string className;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	int id = pAClass->GetId(attrIndex);

	Value *ret = &undefined;
	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
			return &undefined;

		const dmat::AttributeClass& aClass = pInst->GetClass();

		switch(aClass[id].GetType())
		{
		case dmat::Attribute::STRING:
			const char *pValue;
			pInst->GetValue(attrIndex, pValue);
			ret = new String(pValue);
			break;
		case dmat::Attribute::BOOL:
			bool val;
			pInst->GetValue(attrIndex, val);
			if(val)
				ret = &true_value;
			else
				ret = &false_value;
			break;
		case dmat::Attribute::INT:
			s32 intval;
			pInst->GetValue(attrIndex, intval);
			ret = new Integer(intval);
			break;
		case dmat::Attribute::FLOAT:
			float floatval;
			pInst->GetValue(attrIndex, floatval);
			ret = new Float(floatval);
			break;
		}
	}

	return ret;
}

//////////////////////////////////////////////////////////////////////////
// Setters
//////////////////////////////////////////////////////////////////////////

void FPMaxDataStore::SetAttrInt(INode *pNode, int attrIndex, int val)
{
	dmat::AttributeClass *pAClass = NULL;
	std::string className;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	int id = pAClass->GetId(attrIndex);
	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
		{
			return;
		}

		const dmat::AttributeClass& aClass = pInst->GetClass();

		if(aClass[id].GetType() == dmat::Attribute::INT)
		{
			pInst->SetValue(attrIndex, val);
		}
	}
}

void FPMaxDataStore::SetAttrStr(INode *pNode, int attrIndex, TSTR val)
{
	dmat::AttributeClass *pAClass = NULL;
	std::string className;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	int id = pAClass->GetId(attrIndex);

	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
			return ;

		const dmat::AttributeClass& aClass = pInst->GetClass();

		if(aClass[id].GetType() == dmat::Attribute::STRING)
		{
			pInst->SetValue(attrIndex,val.data());
		}
	}
}

void FPMaxDataStore::SetAttrFloat(INode *pNode, int attrIndex, float val)
{
	dmat::AttributeClass *pAClass = NULL;
	std::string className;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	int id = pAClass->GetId(attrIndex);

	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
			return;

		const dmat::AttributeClass& aClass = pInst->GetClass();

		if(aClass[id].GetType() == dmat::Attribute::FLOAT)
		{
			pInst->SetValue(attrIndex, val);
		}
	}
}

void FPMaxDataStore::SetAttrBool(INode *pNode, int attrIndex, bool val)
{
	dmat::AttributeClass *pAClass = NULL;
	std::string className;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	int id = pAClass->GetId(attrIndex);

	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
			return;

		const dmat::AttributeClass& aClass = pInst->GetClass();

		if(aClass[id].GetType() == dmat::Attribute::BOOL)
		{
			pInst->SetValue(attrIndex, val);
		}
	}
}

Value *FPMaxDataStore::SetAttr(INode *pNode, int attrIndex, Value* val)
{
	dmat::AttributeClass *pAClass = NULL;
	std::string className;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	int id = pAClass->GetId(attrIndex);

	Value *ret = &undefined;
	if(pDataInstance)
	{
		dmat::AttributeInst *pInst = pDataInstance->GetAttributes();
		if(pInst == NULL)
			return &undefined;

		const dmat::AttributeClass& aClass = pInst->GetClass();

		switch(aClass[id].GetType())
		{
		case dmat::Attribute::STRING:
			{
				const char *pValue = val->to_string();
				if(pValue)
				{
					pInst->SetValue(attrIndex, pValue);
					return &true_value;
				}
				break;
			}
		case dmat::Attribute::BOOL:
			{
				bool pValue = val->to_bool();
				if(pValue)
				{
					pInst->SetValue(attrIndex, pValue);
					return &true_value;
				}
				break;
			}
		case dmat::Attribute::INT:
			{
				s32 pValue = val->to_int();
				if(pValue)
				{
					pInst->SetValue(attrIndex, pValue);
					return &true_value;
				}
				break;
			}
		case dmat::Attribute::FLOAT:
			{
				float pValue = val->to_float();
				if(pValue)
				{
					pInst->SetValue(attrIndex, pValue);
					return &true_value;
				}
				break;
			}
		}
	}

	return &false_value;
}

TSTR FPMaxDataStore::GetGuidString(INode *pNode)
{
	dmat::AttributeClass *pAClass = NULL;
	DataInstance *pDataInstance = GetDatInstanceFromNode(pNode, pAClass);
	if(pDataInstance)
		return pDataInstance->GetGuidString().c_str();

	return "";
}