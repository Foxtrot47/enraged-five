//
//
//    Filename: RightClickMenu.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 25/06/99 14:52 $
//   $Revision: 1 $
// Description: 
//
//

#pragma warning (disable : 4251)

#include "RightClickMenu.h"
#include "MaxDataMgr.h"

#define SEPARATOR_ID	0x0
#define EDIT_ATTRIBUTES_ID	0x34
#define COPY_ATTRIBUTES_ID	0x35
#define PASTE_ATTRIBUTES_ID	0x36


AttributeMenuItem theAttributeMenuItem;

//
//        name: AttributeMenuItem::Init
// description: Called when right mouse button is pressed
//          in: 
//         out: 
//
void AttributeMenuItem::Init(RightClickMenuManager *pManager, HWND hWnd, IPoint2 m)
{
	s32 numSelected;
	s32 i;

	m_ip = GetCOREInterface();
	if(m_ip == NULL)
		return;

	numSelected = m_ip->GetSelNodeCount();

	if(numSelected > 0)
	{
		pManager->AddMenu(this, MF_SEPARATOR, SEPARATOR_ID, _T(""));
		pManager->BeginSubMenu(_T("Attributes"));

		if(numSelected == 1)
		{
			std::string className;
			s32 disabledFlags = 0;
			m_pSelected = m_ip->GetSelNode(0);
			
			theMaxDataMgr.GetClassFromRefTarget(m_pSelected, className);
			if(!theMaxDataMgr.IsClassAvailable(className) || !theMaxDataMgr.IsDialogAvailable(className))
				disabledFlags = MF_DISABLED|MF_GRAYED;
			
			pManager->AddMenu(this, MF_STRING | disabledFlags, EDIT_ATTRIBUTES_ID, _T("Edit"));
			pManager->AddMenu(this, MF_STRING | disabledFlags, COPY_ATTRIBUTES_ID, _T("Copy"));
		}

		for(i=0; i<numSelected; i++)
		{
			m_pSelected = m_ip->GetSelNode(i);
			Object* pObj = m_pSelected -> EvalWorldState(m_ip->GetTime()).obj;
			if(!theMaxDataMgr.IsCopyAvailable(m_pSelected))
				break;
		}

		// if all nodes are valid for paste allow paste entry otherwise gray out entry
		if(i == numSelected)
			pManager->AddMenu(this, MF_STRING, PASTE_ATTRIBUTES_ID, _T("Paste"));
		else
			pManager->AddMenu(this, MF_STRING|MF_DISABLED|MF_GRAYED, PASTE_ATTRIBUTES_ID, _T("Paste"));

		pManager->EndSubMenu();
	}
}


void AttributeMenuItem::Selected(unsigned int id)
{
	switch(id)
	{
	case EDIT_ATTRIBUTES_ID:
		theMaxDataMgr.EditDataModal(m_pSelected);
		break;
	case COPY_ATTRIBUTES_ID:
		theMaxDataMgr.CopyData(m_pSelected);
		break;
	case PASTE_ATTRIBUTES_ID:
		{
			for(s32 i=0; i<m_ip->GetSelNodeCount(); i++)
			{
				theMaxDataMgr.PasteData(m_ip->GetSelNode(i));
			}
		}
		break;
	default:
		break;
	}
}