#ifndef __MAX_FORCEINCLUDE_H__
#define __MAX_FORCEINCLUDE_H__

//this file is included through the forceinclude setting
//in the project so is included in every file

//we have to include max first because it defines some functions
//that are macros in the rage headers (Assert)

// MAX includes
#include <max.h>
#include <tvnode.h>
#include <modstack.h>
#include <iparamm2.h>
#include <custattrib.h>
#include <icustattribcontainer.h>
#include <guplib.h>
#include <notify.h>
#include <utilapi.h>
#include <imenuman.h>
#include <maxscript/maxscript.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/foundation/Numbers.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/compiler/Parser.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last
#include <XRef/iXrefObj.h>

#if defined(_DEBUG) && defined(_M_X64)
	#include "forceinclude/win64_tooldebug.h"
#elif defined(_DEBUG)
	#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG) && defined(_M_X64)
	#include "forceinclude/win64_toolrelease.h"
#elif defined(NDEBUG)
	#include "forceinclude/win32_toolrelease.h"
#elif defined(_M_X64)
	#include "forceinclude/win64_toolbeta.h"
#else
	#include "forceinclude/win32_toolbeta.h"
#endif

#pragma warning( disable: 4800 )
#pragma warning( disable: 4512 )
#pragma warning( disable: 4265 )
#pragma warning( disable: 4263 )
#pragma warning( disable: 4251 )
#pragma warning( disable: 4062 )

#endif //__MAX_FORCEINCLUDE_H__
