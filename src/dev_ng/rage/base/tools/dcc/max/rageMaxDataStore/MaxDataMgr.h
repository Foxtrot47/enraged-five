//
//
//    Filename: MaxDataMgr.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 25/06/99 14:52 $
//   $Revision: 1 $
// Description: 
//
//
#ifndef INC_MAXDATA_MGR_H_
#define INC_MAXDATA_MGR_H_

#define USE_RAGE_PARSER 1

// C++ headers
#include <vector>
// My headers
#include "resource.h"
#include "Rules.h"
#include <AttribMgr.h>

// class declarations
class AttrTVNode;
class DataInstance;
class CustAttribData;

namespace rage
{
	class parTreeNode;
}

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define MAXDATAMGR_CLASS_ID Class_ID(0x8365571, 0x4c71aa6)

#define MAXDATAMGR_ACTIONTABLEID	0x78e035b2
#define MAXDATAMGR_ACTIONCONTEXTID	0x5c215a78
#define MAXDATAMGR_MENUCONTEXTID	0x1cb40f69


typedef std::map<std::string, std::string> ObjectFuncMap;

enum MaxDataMgrActions {
	AddAttributeActionId,
	DeleteAttributeActionId,
	EditAttributeActionId,
	CopyAttributeActionId,
	PasteAttributeActionId,
	EditAttributeOldActionId
};

class MaxDataMgr : public GUP, public ReferenceMaker
{
public:
	MaxDataMgr();
	~MaxDataMgr();

	// GUP Methods
	DWORD	Start();
	void	Stop();
	DWORD_PTR	Control( DWORD parameter );

	FPInterfaceDesc* GetDesc();

	// ReferenceMaker methods
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle ref);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
		PartID& partID,  RefMessage message);

	// MaxDataMgr methods
	void Init();
	void LoadConfigFile();
	void SaveConfigFile();

	void SetAttributesFilename(const std::string& filename);
	const std::string& GetAttributesFilename();

	void LoadScriptFile();
	bool HasLoadedScript() {return m_hasLoadedScript;}
	HWND GetMainWnd() {return m_hMainWnd;}
	void Reset();

	dmat::AttributeInst *CreateInstance(const std::string &className);
	dmat::AttributeClass *GetClass(const std::string &className);
//	const dmat::AttributeDialogDesc *GetConstDialogDesc(const std::string &dialogName);
	dmat::AttributeDialogDesc *GetDialogDesc(const std::string &dialogName);
	bool IsClassAvailable(const std::string &className);
	bool IsDialogAvailable(const std::string &dialogName);
	void GetClassFromRefTarget(RefTargetHandle rtarg,std::string& className);
	bool IsRefTargetThisClass(Animatable* rtarg, const std::string& className);
	const dmat::AttributeManager& GetAttributeManager() {return m_attributeManager;}

	void StoreSelectedNodes(Interface* ip, bool forceDeselect = false);
	void RestoreSelectedNodes(Interface* ip, bool redraw);
	s32 GetNumSelectedNodes() {return m_selectedNodes.Count();}
	INode* GetSelectedNode(s32 i) {return m_selectedNodes[i];}

	// Add data to object
	DataInstance* GetAttributes(RefTargetHandle ref);
	DataInstance* AddAttributes(RefTargetHandle ref);
	void DeleteAttributes(RefTargetHandle ref);
	void EditDataModal(RefTargetHandle rtarg);
	BOOL EditSelectedDataModal(bool bLaunchDialog=true);

	void CopyData(RefTargetHandle rtarg);
	void PasteData(RefTargetHandle rtarg);
	bool IsCopyAvailable(RefTargetHandle rtarg);

	// Selection rules
	SelectionRulesList& GetRules() {return m_rules;}

	// access custattrib etc
	CustAttribData* GetCustAttribFromCorrectRef(RefTargetHandle ref);
	CustAttribData* GetCustAttrib(Animatable* pAnimatable);
	CustAttribData* AddCustAttrib(Animatable* pAnimatable);

	BOOL LaunchAttributeEditor();

private:
	std::string m_attributeFile;
	dmat::AttributeManager m_attributeManager;
	bool m_hasInitialised;
	bool m_hasLoadedScript;
	HWND m_hMainWnd;
	INodeTab m_selectedNodes;
	bool m_selectedNodesStored;
	DataInstance* m_pCopy;
	SelectionRulesList m_rules;

	// XML loading stuff
	void LoadRule(rage::parTreeNode* pRuleNode, SelectionRules& selRule);
	void LoadSelectionRule(rage::parTreeNode* pSelRuleNode);
	void LoadRules(rage::parTreeNode* pRuleListNode);

	void SyncData(RefTargetHandle ref);
};

extern MaxDataMgr theMaxDataMgr;

//
//   Class Name: MaxDataMgrClassDesc
// Base Classes: ClassDesc
//  Description: 
//    Functions: 
//
//
class MaxDataMgrClassDesc : public ClassDesc2
{
public:
	int IsPublic() {return 1;}
	void *Create(BOOL loading = FALSE) {return &theMaxDataMgr;}
	const TCHAR *ClassName() {return GetString(IDS_MAXDATAMGR_NAME);}
	SClass_ID SuperClassID() {return GUP_CLASS_ID;}
	Class_ID ClassID() {return MAXDATAMGR_CLASS_ID;}
	const TCHAR* Category() {return GetString(IDS_CATEGORY);}
	const TCHAR* InternalName() { return _T(GetString(IDS_MAXDATAMGR_NAME)); }	// returns fixed parsable name (scripter-visible name)
	HINSTANCE HInstance() { return hInstance; }				// returns owning module handle

	int NumActionTables() {return 1;}
	ActionTable* GetActionTable(int i);
};

extern MaxDataMgrClassDesc theMaxDataMgrDesc;

#endif