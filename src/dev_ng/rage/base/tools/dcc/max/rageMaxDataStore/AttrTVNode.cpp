//
//
//    Filename: AttrTVNode.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Track view node containing attribute data
//
//
#include "AttrTVNode.h"

#define TVNODE_CLASSID_CHUNK	0x4e91
#define TVNODE_ATTR_CHUNK		0x4e92


AttrTVNodeClassDesc theAttrTVNodeDesc;
ClassDesc* GetAttrTVNodeDesc() {return &theAttrTVNodeDesc;}

//
//        name: PostLoadCallback
// description: PostLoadCallback used to delete TVNodes with no attribute data attached
//
class RemoveTVNodePostLoadCallback : public PostLoadCallback
{
public:
	RemoveTVNodePostLoadCallback(AttrTVNode *pTVNode) : m_pTVNode(pTVNode) {}
	void proc(ILoad *iload)
	{
		m_pTVNode->DeleteThisTVNode();

		delete this;
	}
private:
	AttrTVNode* m_pTVNode;
};

//
//   Class Name: RemoveTVNodeRestore
// Base Classes: RestoreObj
//  Description: 
//
class RemoveTVNodeRestore : public RestoreObj
{
public:
	RemoveTVNodeRestore(AttrTVNode *pTVChild, ITrackViewNode *pTVParent, const char *pName);

	void Restore(int isUndo);
	void Redo();
	int Size() {return sizeof(RemoveTVNodeRestore) + m_name.Length();}
	TSTR Description() {return _T("Remove TVNode");}

private:
	AttrTVNode *m_pTVChild;
	ITrackViewNode *m_pTVParent;
	TSTR m_name;
};

// --- RemoveTVNodeRestore -----------------------------------------------------------------------------------------

RemoveTVNodeRestore::RemoveTVNodeRestore(AttrTVNode *pTVChild, ITrackViewNode *pTVParent, const char *pName) :
m_pTVChild(pTVChild), m_pTVParent(pTVParent), m_name(pName)
{
}

void RemoveTVNodeRestore::Restore(int isUndo)
{
	m_pTVParent->AddNode(m_pTVChild, m_name, m_pTVChild->GetClassID());
}

void RemoveTVNodeRestore::Redo()
{
	m_pTVParent->RemoveItem(m_pTVChild->GetClassID());
}

// --- AttrTVNode -------------------------------------------------------------------------------------------------

AttrTVNode::~AttrTVNode()
{
}


static s32 nextAttrId = 0;
AttrTVNode* CreateAttrTVNode() 
{
	return new AttrTVNode(nextAttrId++);
}

void FixAttrTVNode(AttrTVNode* pNode)
{
	pNode->SetId(nextAttrId++);
}

ITrackViewNode* GetParentTVNode(RefTargetHandle ref)
{
#if (MAX_RELEASE >= 12000)
	DependentIterator pItem(ref);
	ReferenceMaker* maker = NULL;
	while(NULL != (maker = pItem.Next()))
	{
		if(maker->ClassID() == TVNODE_CLASS_ID)
			return static_cast<ITrackViewNode*>(maker);
	}
	return NULL;
#else
	RefListItem *pItem = ref->GetRefList().FirstItem();
	while(pItem)
	{
		if(pItem->maker->ClassID() == TVNODE_CLASS_ID)
			return static_cast<ITrackViewNode*>(pItem->maker);
		pItem = pItem->next;
	}
	return NULL;
#endif	//MAX_RELEASE >= 12000
}

//
//        name: AttrTVNode::DeleteThisTVNode
// description: Delete TVNode throught the parent node
//          in: 
//         out: 
//
void AttrTVNode::DeleteThisTVNode()
{
	ITrackViewNode* pParent = GetParentTVNode(this);

	if(pParent)
	{
		s32 index = pParent->FindItem(GetClassID());
		if(index == -1)
			return;
		// If not the same node then ignore
		if(this != pParent->GetNode(index))
			return;

		TSTR name = pParent->GetName(index);
		SetAFlag(A_LOCK_TARGET);
		pParent->RemoveItem(index);
		if(theHold.Holding())
			theHold.Put(new RemoveTVNodeRestore(this, pParent, name));
		ClearAFlag(A_LOCK_TARGET);
	}
}

//
//        name: AttrTVNode::NotifyRefChanged
// description: Called when reference changes
//
RefResult AttrTVNode::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,  RefMessage message) 
{
	switch(message)
	{
	case REFMSG_TARGET_DELETED:
		if(hTarget == m_ref)
		{
			m_ref = NULL;
			DeleteThisTVNode();
		}
		break;
	}
	return REF_SUCCEED;
}


//
//        name: AttrTVNode::Save/Load
// description: Load and save
//
IOResult AttrTVNode::Save(ISave *isave)
{
	ULONG length;
	s32 size;
	char *pData = NULL;
	
	isave->BeginChunk(TVNODE_CLASSID_CHUNK);
	isave->Write((ULONG*)&m_id, sizeof(s32), &length);
	isave->EndChunk();
	
	if(m_attr.GetAttributes())
		pData = m_attr.GetAttributes()->CreateRawData(size);
	else
		size = 0;
	
	isave->BeginChunk(TVNODE_ATTR_CHUNK);
	isave->Write((ULONG*)&size, sizeof(s32), &length);	// size of attribute data
	if(size)
		isave->Write((ULONG*)pData, size, &length);				// attribute data 
	isave->WriteCString(m_attr.GetClassName().c_str());	// attribute class name
	isave->EndChunk();
	
	delete[] pData;

	return IO_OK;
}
IOResult AttrTVNode::Load(ILoad *iload)
{
	// load slot queue
	IOResult res;
	ULONG length;

	while (IO_OK == (res = iload->OpenChunk())) {
		switch(iload->CurChunkID())  
		{
		case TVNODE_CLASSID_CHUNK:
			res = iload->Read((ULONG*)&m_id, sizeof(s32), &length);
			if(res == IO_OK)
			{
				if(m_id > nextAttrId)
					nextAttrId = m_id+1;
			}
			break;
		case TVNODE_ATTR_CHUNK:
			{
				s32 size = (s32)iload->CurChunkLength();
				char *pData = new char[size+1];
				s32 dataSize;
				char *pName;
				char *pAttributes;

				res = iload->Read((ULONG*)pData, size, &length);
				// add a end of string character at the end of the data so that the class name has an 
				// ending
				pData[size] = '\0';

				// get index, datasize, attribute data and attribute class name
				dataSize = *reinterpret_cast<s32 *>(pData);
				pAttributes = pData + 4;
				pName = pAttributes + dataSize;
				m_attr.AddAttributes(pName);
				if(m_attr.GetAttributes())
					m_attr.GetAttributes()->ReadRawData(pAttributes, dataSize);
				else
				{
					Interface *ip = GetCOREInterface();
					if(ip)
						iload->RegisterPostLoadCallback(new RemoveTVNodePostLoadCallback(this));
				}
				delete[] pData;
			}
			break;
		default:
			break;
		}
		iload->CloseChunk();
		if (res != IO_OK) 
			return res;
	}
	return IO_OK;
}
