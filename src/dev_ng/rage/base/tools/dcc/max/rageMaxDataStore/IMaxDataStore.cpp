//
//
//    Filename: IMaxDataStore.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Interface class to MaxDataStore plugin
//
//
// Windows resource IDs
#pragma warning (disable : 4786)
#pragma warning (disable : 4251)

#include "resource.h"
// My headers
#include "IMaxDataStore.h"
#include "MaxDataMgr.h"
#include "DataInstance.h"
#include <DialogTemplate.h>
#include <AttribDialog.h>
#include "CustAttribData.h"

extern HINSTANCE hInstance;

MAXDATASTORE_API IMaxDataStore theIMaxDataStore;

//
//        name: AttributeDialogProc
// description: Dialog procedure for the max rollup parent
//
BOOL CALLBACK NullDialogProc( HWND hwndDlg, UINT msg, WPARAM wParam, LPARAM lParam )
{
//	return DefWindowProc(hwndDlg, msg, wParam, lParam);
	return FALSE;
}
 

//
//        name: AttributeDialogProc
// description: Dialog procedure for the attribute dialog
//
/*BOOL CALLBACK RollupDialogProc( HWND hwndDlg, UINT msg, WPARAM wParam, LPARAM lParam )
{
	dmat::AttributeDialog *pDialog;
	switch(msg)
	{
	case WM_INITDIALOG:
		pDialog = (dmat::AttributeDialog *)lParam;
		pDialog->InitDialog(hwndDlg);
		SetWindowLong(hwndDlg, GWL_USERDATA, lParam);
		break;
	case WM_NOTIFY:
		break;

	case WM_COMMAND:
		if(HIWORD(wParam) == 0x100)
			DisableAccelerators();
		else if(HIWORD(wParam) == 0x200)
			EnableAccelerators();
		pDialog = (dmat::AttributeDialog *)GetWindowLong(hwndDlg, GWL_USERDATA);
		if(pDialog)
			pDialog->SyncWithControls();
		break;
	}
//	return DefWindowProc(hwndDlg, msg, wParam, lParam);
	return FALSE;
}*/
 

 

// --- IMaxDataStore ----------------------------------------------------------------------------------------------

//
//        name: IMaxDataStore::AddData, GetData, EditDataModal
// description: Interface functions
//

s32 IMaxDataStore::GetAttribIndex(RefTargetHandle rtarg,const char* p_cName)
{
	DataInstance data;
	DataInstance *pData = GetData(rtarg);

	if(pData == NULL)
	{
		data.AddAttributes(rtarg);
		pData = &data;
	}

	if(pData == NULL)
	{
		return -1;
	}

	dmat::AttributeInst *pInst = pData->GetAttributes();
	const dmat::AttributeClass& aClass = pInst->GetClass();
	std::string className;
	theMaxDataMgr.GetClassFromRefTarget(rtarg, className);

	dmat::AttributeClass *pAClass = theMaxDataMgr.GetClass(className);

	if(pAClass == NULL)
		return -1;

	for(s32 i=0; i<pAClass->GetSize(); i++)
	{
		if(!strcmp(p_cName, (*pAClass)[pAClass->GetId(i)].GetName().c_str()))
		{
			return i;
		}
	}

	return -1;
}

DataInstance *IMaxDataStore::AddData(RefTargetHandle rtarg)
{
	return theMaxDataMgr.AddAttributes(rtarg);
}

DataInstance* IMaxDataStore::GetDataHasModified(RefTargetHandle rtarg,bool& bChanged)
{
	DataInstance* pData = theMaxDataMgr.GetAttributes(rtarg);
	bChanged = false;

	if(pData)
	{
		std::string className;
		theMaxDataMgr.GetClassFromRefTarget(rtarg, className);

		//if the class name has changed then re-create the attributes
		//for the new type, and warn the user

		if(pData->GetClassName() != className)
		{
			theMaxDataMgr.DeleteAttributes(rtarg);
			pData = theMaxDataMgr.AddAttributes(rtarg);
			bChanged = true;
		}
	}

	return pData;
}

DataInstance *IMaxDataStore::GetData(RefTargetHandle rtarg) 
{
	bool bThrowAway;

	return GetDataHasModified(rtarg,bThrowAway);
}

void IMaxDataStore::EditDataModal(RefTargetHandle rtarg)
{
	theMaxDataMgr.EditDataModal(rtarg);
}

GUID IMaxDataStore::GetDataGuid(RefTargetHandle rtarg)
{
	DataInstance *pData = theMaxDataMgr.GetAttributes(rtarg);
	if(pData)
		return pData->GetGuid();
	else
		return GUID_NULL;
}
std::string IMaxDataStore::GetDataGuidString(RefTargetHandle rtarg)
{
	DataInstance *pData = theMaxDataMgr.GetAttributes(rtarg);
	if(pData)
		return pData->GetGuidString();
	else
		return "";
}

//
//        name: IMaxDataStore::CopyData
// description: Copy attribute data from one object to another
//          in: to = object to copy data to
//				from = object to copy data from
//
void IMaxDataStore::CopyData(RefTargetHandle to, RefTargetHandle from)
{
	DataInstance *pFromData = theMaxDataMgr.GetAttributes(from);
	DataInstance *pToData = theMaxDataMgr.AddAttributes(to);
	
	if(pFromData && pToData)
		*pToData = *pFromData;
}

//
//        name: IMaxDataStore::AddRollupPage/DeleteRollupPage
// description: Create and remove rollup pages
//
HWND IMaxDataStore::AddRollupPage(RefTargetHandle rtarg, Interface *ip)
{
//#if 0
	CustAttribData* pAttrib = theMaxDataMgr.GetCustAttribFromCorrectRef(rtarg);

	if(pAttrib)
	{
		pAttrib->BeginEditParams((IObjParam*)ip, 0, NULL);
		return pAttrib->GetRollupHWnd();
	}
//#endif
	return NULL;
}
void IMaxDataStore::DeleteRollupPage(HWND hWnd, Interface *ip)
{
//#if 0
	CustAttribData::RemoveRollup(hWnd, ip);
//#endif
}
HWND IMaxDataStore::AddRollupPage(RefTargetHandle rtarg, IMtlParams *iMtlPrms)
{
	DataInstance *pData = theMaxDataMgr.AddAttributes(rtarg);
	CustAttribData* pAttrib = theMaxDataMgr.GetCustAttribFromCorrectRef(rtarg);
	if(pAttrib)
	{
		pAttrib->BeginEditParams(iMtlPrms, 0, NULL);
		return pAttrib->GetRollupHWnd();
	}
	return NULL;
}
void IMaxDataStore::DeleteRollupPage(HWND hWnd, IMtlParams *iMtlPrms)
{
	CustAttribData::RemoveRollup(hWnd, iMtlPrms);
}

void IMaxDataStore::EditDataWithDialog(RefTargetHandle rtarg, void *pDlgData)
{
	dmat::AttributeDialog *pDialog = static_cast<dmat::AttributeDialog *>(pDlgData);
	DataInstance *pData = theMaxDataMgr.AddAttributes(rtarg);
	if(pData == NULL)
		return;

	pDialog->Edit(pData->GetAttributes());
}


//
// name:		IMaxDataStore::GetClassFromRefTarget
// description:	Return attribute class from Reference Target pointer
//          in: rtarg = pointer to reference target
//		   out:	className = string to fill with class name
//
// This method simply forwards the call to this plugins MaxDataStore static
// object.
//
void IMaxDataStore::GetClassFromRefTarget( RefTargetHandle rtarg, std::string& className )
{
	theMaxDataMgr.GetClassFromRefTarget( rtarg, className );
}


//
// name:		IMaxDataStore::IsClassAvailable
// description: Returns is a class or dialog is available
//          in: name = name of class/dialog
//         out: is available
//
// This method simply forwards the call to this plugins MaxDataStore static
// object.
//
bool IMaxDataStore::IsClassAvailable( const std::string& sClassName )
{
	return theMaxDataMgr.IsClassAvailable( sClassName );
}

//
//        name: IMaxDataStore::GetClass
// description: Get attribute class descriptor from name
//          in: className = name of class
//         out: pointer to attribute class 
//
// This method simply forwards the call to this plugins MaxDataStore static
// object.
//
dmat::AttributeClass* IMaxDataStore::GetClass( const std::string& sClassName )
{
	return theMaxDataMgr.GetClass( sClassName );
}
