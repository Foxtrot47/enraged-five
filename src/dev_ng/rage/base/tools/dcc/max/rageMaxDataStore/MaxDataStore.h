//
//
//    Filename: MaxDataStore.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 25/06/99 14:52 $
//   $Revision: 1 $
// Description: 
//
//

#ifndef __MAXDATASTORE__H
#define __MAXDATASTORE__H

// get rid of identifier too long warning
#pragma warning (disable : 4786)

// My headers
#include "resource.h"
// STD headers
#include <vector>

// using namespace
using namespace rage;

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define MAXDATASTORE_CLASS_ID	Class_ID(0xcf00870a, 0x8fb18b1c)

class DataInstance;

//
//   Class Name: MaxDataStore
// Base Classes: ReferenceTarget
//  Description: 
//    Functions: 
//
//
class MaxDataStore : public ReferenceTarget {

public:
	HWND m_hMainWnd;

	//Constructor/Destructor
	MaxDataStore();
	~MaxDataStore();
	
	void DeleteThis() {}

	// standard reference functions
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle pTarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message);
	BOOL IsRealDependency(RefTargetHandle pTarg) {return FALSE;}
	// my reference functions
	s32 AddReference(RefTargetHandle pTarg);
	void RemoveReference(RefTargetHandle pTarg);
	s32 GetIdFromRef(RefTargetHandle pTarg);
	bool IsValidRefId(RefTargetHandle pTarg, s32 id);
	s32 FindRef(RefTargetHandle pTarg);
	// get slot position
	s32 GetNextSlot();
	void ReleaseSlot(s32 slot);
	void CreateDataInstance(s32 slot);

	void Reset();	// called after Reset is requested. Does loads of tidy up

	// IO functions
	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);
	void PostLoad(ILoad *iload);

	s32 AddData(RefTargetHandle rtarg);
	DataInstance *GetData(s32 i) {return m_dataArray[i];}
	void EditDataModal(RefTargetHandle rtarg);

	void CopyData(RefTargetHandle rtarg);
	void PasteData(RefTargetHandle rtarg);
	bool IsCopyAvailable(RefTargetHandle rtarg);

	void AddItemToRightClickMenu(Interface *ip);

private:
	std::vector<RefTargetHandle> m_refArray;
	std::vector<DataInstance *> m_dataArray;
	DataInstance *m_pCopy;

	std::vector<u32> m_slotQueue;
	s32 m_queuePosn;

	bool m_isLoading;
	std::vector<s32> m_loadedRefIDs;
	std::vector<DataInstance *> m_loadedData;
};


//
//   Class Name: MaxDataStoreClassDesc
// Base Classes: 
//  Description: 
//    Functions: 
//
//
class MaxDataStoreClassDesc : public ClassDesc 
{
public:
	int IsPublic() {return 1;}
	void *Create(BOOL loading = FALSE);
	const TCHAR *ClassName() {return GetString(IDS_DATASTORE_NAME);}
	SClass_ID SuperClassID() {return REF_TARGET_CLASS_ID;}
	Class_ID ClassID() {return MAXDATASTORE_CLASS_ID;}
	const TCHAR* Category() {return GetString(IDS_CATEGORY);}
	void ResetClassParams (BOOL fileReset);

	BOOL NeedsToSave() {return TRUE;}
	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);
};



extern MaxDataStore theMaxDataStore;

#endif // __MAXDATASTORE__H
