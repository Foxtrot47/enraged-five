//
// filename:	Configuration.h
// description:	Configuration Data for MaxDataStore plugin
//

#ifndef INC_CONFIGURATION_H_
#define INC_CONFIGURATION_H_

// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>

/**
 * @brief Configuration settings class
 *
 * This object is responsible for getting the plug-in's configuration
 * information from the module settings.
 */
class Config
{
public:
	static void Init();
public:
	static const std::string	GetConfigPath()		{ return m_configPath; }
	static const std::string	GetConfigFile()		{ return m_configFile; }
private:
	static std::string m_configPath;
	static std::string m_configFile;
};

#endif // !INC_CONFIGURATION_H_

// End of file
