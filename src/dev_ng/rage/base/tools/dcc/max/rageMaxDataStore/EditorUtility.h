//
//
//    Filename: EditorUtility.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Utility plugin for editing preferences
//
//
#ifndef INC_EDITOR_UTILITY_H_
#define INC_EDITOR_UTILITY_H_

// C++ headers
#include <string>
// My headers
#include "resource.h"

#define EDITOR_UTILITY_CLASS_ID	Class_ID(0x5f021c74, 0x3bcc06b2)

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

//
//   Class Name: EditorUtility
// Base Classes: 
//  Description: Class used to edit editor preferences
//    Functions: 
//
//
class EditorUtility : public UtilityObj
{
public:
	EditorUtility();
	~EditorUtility();

	void BeginEditParams(Interface *ip,IUtil *iu);
	void EndEditParams(Interface *ip,IUtil *iu);

	void DeleteThis() {}
	void Reset();

	void InitUI(HWND hWnd);
	void UpdateUI(HWND hWnd);
	void EditAttributeFile();
	void FindAttributeFile();
	void AddRule();
	void DeleteRule();
	void EditRule();
	void CopyRule();

private:
	Interface *m_ip;
	HWND m_hPanel;
	static NameMaker *m_pNameMaker;
};


//
//   Class Name: EditorUtilityClassDesc
// Base Classes: ClassDesc
//  Description: 
//    Functions: 
//
//
class EditorUtilityClassDesc : public ClassDesc
{
public:
	int IsPublic() {return 1;}
	void *Create(BOOL loading = FALSE);
	const TCHAR *ClassName() {return GetString(IDS_PREFERENCES_NAME);}
	SClass_ID SuperClassID() {return UTILITY_CLASS_ID;}
	Class_ID ClassID() {return EDITOR_UTILITY_CLASS_ID;}
	const TCHAR* Category() {return GetString(IDS_CATEGORY);}
	void ResetClassParams (BOOL fileReset);

};



#endif // INC_EDITOR_UTILITY_H_