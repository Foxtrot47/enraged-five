//
//
//    Filename: IAttrib.h
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 29/04/99 9:44 $
//   $Revision: 2 $
// Description: Interface to attribute material
//
//
#ifndef _I_ATTRIB_H_
#define _I_ATTRIB_H_

#define ATTRIBMTL_CLASS_ID	Class_ID(0x89524891, 0x795b0fa0)

#define MAX_ATTRIBUTES				12

// references
#define ATTRIB_PBLOCK_REF_ID	0

// parameters
#define ATTRIB_PB_COLOUR_ID		0
#define ATTRIB_PB_INDEX			1


#endif // _I_ATTRIB_H_
