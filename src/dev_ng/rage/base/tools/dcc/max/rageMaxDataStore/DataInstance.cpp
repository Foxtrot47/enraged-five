//
//
//    Filename: DataInstance.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 25/06/99 14:52 $
//   $Revision: 1 $
// Description: Attribute storage class
//
//
#pragma warning (disable : 4786)
#pragma warning (disable : 4251)

#include "MaxDataMgr.h"
#include "DataInstance.h"

extern HINSTANCE hInstance;

//
//        name: AttributeDialogProc
// description: Dialog procedure for the attribute dialog
//
INT_PTR CALLBACK AttributeDialogProc( HWND hwndDlg, UINT msg, WPARAM wParam, LPARAM lParam )
{
	static dmat::AttributeDialog *pDialog = NULL;
	switch(msg)
	{
	case WM_INITDIALOG:
		{
			pDialog = (dmat::AttributeDialog *)lParam;
			pDialog->InitDialog(hwndDlg);
		}
		break;
	case WM_COMMAND:
		{
			if(!pDialog)
				return FALSE;
			dmat::AttributeDialog::AttributeControl *pAttr = pDialog->FindControl((HWND)lParam);
			if(pAttr)
				pAttr->ProcessWindowMessage(hwndDlg, msg, wParam, lParam);
			switch(LOWORD(wParam))
			{
			case IDOK:
				pDialog->SyncWithControls();
				EndDialog(hwndDlg, 1);
				return TRUE;
			case IDCANCEL:
				EndDialog(hwndDlg, 0);
				pDialog = NULL;
				return TRUE;
			}
		}
		break;
	case WM_CLOSE:
	case WM_DESTROY:
		pDialog = NULL;
		break;
	}
	return FALSE;
}
 

// --- DataInstance -----------------------------------------------------------------------------------

//
//        name: DataInstance::DataInstance
// description: Constructor
//          in: pINode = pointer to INode
//
DataInstance::DataInstance() 
: m_pAttributes(NULL)
{
	memset(&m_guid, 0, sizeof(GUID));
	CreateGuid();
}

//
//        name: DataInstance::DataInstance
// description: Copy constructor
//
DataInstance::DataInstance(const DataInstance &orig)
{
	if(orig.m_pAttributes)
		m_pAttributes = new dmat::AttributeInst(*orig.m_pAttributes);
	else
		m_pAttributes = NULL;
	m_attributeClassName = orig.m_attributeClassName;

	memset(&m_guid, 0, sizeof(GUID));
	CreateGuid();
}

//
//        name: DataInstance::~DataInstance
// description: destructor
//
DataInstance::~DataInstance()
{
	if(m_pAttributes)
		delete m_pAttributes;
}

//
//        name: &DataInstance::operator=
//
DataInstance &DataInstance::operator=(const DataInstance &copy)
{
	if(m_pAttributes)
		delete m_pAttributes;
	if(copy.m_pAttributes)
		m_pAttributes = new dmat::AttributeInst(*copy.m_pAttributes);
	else
		m_pAttributes = NULL;
	m_attributeClassName = copy.m_attributeClassName;
	return *this;
}

void DataInstance::SetAttributes(dmat::AttributeInstMultiple* p_attribNew)
{
	if(p_attribNew)
	{
		m_pAttributes->updateFromMultiple(p_attribNew);
	}
	else
	{
		delete m_pAttributes;
		m_pAttributes = NULL;
	}
}

//
//        name: DataInstance::AddAttributes
// description: Add an attribute block
//          in: pINode = pointer to INode
//
void DataInstance::AddAttributes(RefTargetHandle rtarg)
{
	if(m_pAttributes)
		return;
	theMaxDataMgr.GetClassFromRefTarget(rtarg, m_attributeClassName);

	m_pAttributes = theMaxDataMgr.CreateInstance(m_attributeClassName);

}

//
//        name: DataInstance::AddAttributes
// description: Add an attribute block
//          in: className = name of attribute class
//
void DataInstance::AddAttributes(const std::string &className)
{
	if(m_pAttributes)
		delete m_pAttributes;

	m_attributeClassName = className;

	m_pAttributes = theMaxDataMgr.CreateInstance(m_attributeClassName);
}

//
//        name: DataInstance::EditModal
// description: Edit data
//
void DataInstance::EditModal() const
{
	const dmat::AttributeDialogDesc *pDialogDesc = theMaxDataMgr.GetDialogDesc(m_attributeClassName);
	dmat::AttributeDialog dialog;

	if(m_pAttributes == NULL || pDialogDesc == NULL)
		return;

	dialog.ConstructDialog(hInstance, m_pAttributes, pDialogDesc, (m_attributeClassName + " Attributes").c_str());

	DialogBoxIndirectParam(hInstance, dialog.GetTemplate(), theMaxDataMgr.GetMainWnd(), &AttributeDialogProc, (LPARAM)&dialog);
}

//
//        name: DataInstance::ConstructDialog
// description: Create a dialog for the data instance
//         out: pointer to created dialog. This dialog must be deleted by the user
//
dmat::AttributeDialog* DataInstance::ConstructDialog()
{
	const dmat::AttributeDialogDesc *pDialogDesc = theMaxDataMgr.GetDialogDesc(m_attributeClassName);

	if(m_pAttributes == NULL || pDialogDesc == NULL)
		return NULL;

	// create dialog
	dmat::AttributeDialog *pDialog = new dmat::AttributeDialog;
	pDialog->ConstructDialog(hInstance, m_pAttributes, pDialogDesc);

	return pDialog;
}

//
//        name: DataInstance::ConstructRollup
// description: Create a Max rollup page for the data instance
//         out: pointer to created dialog. This dialog must be deleted by the user
//
dmat::AttributeDialog* DataInstance::ConstructRollupPage(bool inMaterialEditor)
{
	const dmat::AttributeDialogDesc *pDialogDesc = theMaxDataMgr.GetDialogDesc(m_attributeClassName);

	if(m_pAttributes == NULL || pDialogDesc == NULL)
		return NULL;

	// create rollup
	dmat::AttributeDialog *pDialog = new dmat::AttributeDialog;
	pDialog->ConstructMaxRollup(m_pAttributes, pDialogDesc,inMaterialEditor);

	return pDialog;
}

