//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MaxDataStore.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_DATASTORE_NAME              3
#define IDS_PARAMS                      4
#define IDS_PREFERENCES_NAME            5
#define IDS_VERSION                     6
#define IDS_ATTRIBUTE_NAME              7
#define IDS_ADD_ATTR                    8
#define IDS_MAXDATAMGR_NAME             9
#define IDS_COPY_ATTR                   10
#define IDS_PASTE_ATTR                  11
#define IDS_EDIT_ATTR                   12
#define IDS_DELETE_ATTR                 13
#define IDS_LODATTRIBS_NAME             14
#define IDS_LODATTR                     15
#define IDS_MODULESETTING_CONFIGPATH    16
#define IDS_MODULESETTING_CONFIGFILE    17
#define IDS_MODULESETTING_ATTRFILE      18
#define IDD_PANEL                       101
#define IDD_BLANK                       102
#define IDD_RULES                       103
#define IDD_PREFERENCES_PANEL           104
#define IDD_CONSTRUCT                   105
#define IDB_DMAMAN                      111
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_ATTRIBFILE_EDIT             1001
#define IDC_ATTRIBFILE_BUTTON           1002
#define IDC_RULES_LIST                  1003
#define IDC_ADD_BUTTON                  1004
#define IDC_DELETE_BUTTON               1005
#define IDC_NAME_EDIT                   1005
#define IDC_EDIT_BUTTON                 1006
#define IDC_RULE_LIST                   1006
#define IDC_DELRULE_BUTTON              1007
#define IDC_COPY_BUTTON                 1007
#define IDC_EDITRULE_BUTTON             1008
#define IDC_CLASS_COMBO                 1009
#define IDC_ADDRULE_BUTTON              1011
#define IDC_RULETYPE_COMBO              1012
#define IDC_LOD_TEXT2                   1013
#define IDC_CLASSLIST_COMBO             1013
#define IDC_SUPERCLASSLIST_COMBO        1014
#define IDC_VERSION                     1015
#define IDC_FILE_EDIT                   1015
#define IDC_FILE_BUTTON                 1016
#define IDC_CLASS_STATIC                1017
#define IDC_SCLASS_STATIC               1018
#define IDC_TEXT_EDIT                   1019
#define IDC_DIR_STATIC                  1020
#define IDC_TEXT_STATIC                 1021
#define IDC_NEGATIVE_CHECK              1022
#define IDC_AUTHOR_TEXT                 1024
#define IDC_HTML2                       1026
#define IDC_DATETIMEPICKER1             1027
#define IDC_EDITFILE_BUTTON             1029
#define IDC_MODIFIERLIST_COMBO          1031
#define IDC_PICK_PARENT                 1032
#define IDC_PARENTNAME                  1033
#define IDC_RESET_PARENT                1034
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_SPIN                        1496
#define IDS_EDIT_ATTR_OLD				1497

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
