//
//
//    Filename: IConnection.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Interface header for AIConnection
//
//
#ifndef INC_ICONNECTION_H_
#define INC_ICONNECTION_H_

// Class IDs
#define AICONNECTION_CLASS_ID	Class_ID(0x74ee77f9, 0x7cb85a7d)
#define AILINK_CLASS_ID			Class_ID(0x52773091, 0x3b567e78)
#define AIPATH_CLASS_ID			Class_ID(0x760c599b, 0x39d03776)

// AIConnection parameters
// Paramblock2 name
enum { aiconnection_params, ailink_params, aipath_params}; 

// AIConnection Paramblock2 parameter list
enum { 
	ai_radius,
	aic_objectlist
};
// AILink Paramblock2 parameter list
enum { 
	ail_objectlist
};

// AIPath Paramblock2 parameter list
enum { 
	ai_connectionlist,
	ai_valid
};

// AILink References
enum {
	ail_control1 = 0,
	ail_control2,
	ail_paramblock,
	ail_otherlink,
	ail_numrefs
};

#endif // INC_ICONNECTION_H_