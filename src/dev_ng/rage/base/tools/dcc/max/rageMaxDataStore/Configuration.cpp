//
// filename:	Configuration.cpp
// description:	Configuration Data for MaxDataStore plugin
//

// --- Include Files ------------------------------------------------------------

// Local headers
#include "Configuration.h"
#include "resource.h"

// --- Using Directives ---------------------------------------------------------

using namespace rage;

// --- Globals ------------------------------------------------------------------

extern TCHAR* GetString( int id );

// --- Config Code -------------------------------------------------

std::string Config::m_configPath		= "";
std::string Config::m_configFile		= "";

/**
 * @brief Gets all configuration settings from the module settings, throwing
 * an exception if any module setting isn't found.
 */
void Config::Init()
{

	// DHM - 15 May 2009
	// Moving to Collada we need to start using the rageMaxDataStore as soon as
	// possible.  Just hacking in out previous support to fetch the 
	// MaxDataStore.xml file.

	m_configFile = GetCOREInterface()->GetDir( APP_PLUGCFG_DIR ) + std::string( "\\MaxDataStore.xml" );
	MaxSDK::Util::Path configPath( m_configFile.c_str() );
	m_configPath = std::string( configPath.RemoveLeaf().GetCStr() ) + std::string( "\\" );
}

// End of file
