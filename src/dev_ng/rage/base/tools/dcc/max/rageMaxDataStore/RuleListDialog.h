//
//
//    Filename: RuleListDialog.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Dialog containing list of rules
//
//
#ifndef INC_RULE_LIST_DIALOG
#define INC_RULE_LIST_DIALOG

#include "DialogClass.h"
#include "resource.h"

// STD headers
#pragma warning (disable : 4786)
#include <string>



//
//   Class Name: ConstructRuleDialog
// Base Classes: DialogClass
//  Description: Dialog for creating a single rule
//
class ConstructRuleDialog : public DialogClass
{
public:
	ConstructRuleDialog() : DialogClass(IDD_CONSTRUCT) {}

	virtual void OnInitDialog();
	virtual void OnCommand(s32 id, s32 notifyCode);
	virtual void OnOK();
	virtual void UpdateControls();
	virtual void UpdateData();

	void InitRuleTypeCombo();
	void InitSuperClassListCombo();
	void InitClassListCombo();
	void InitModifierListCombo();
	void EnableControls();
	void FindDir();

	// data
	Rule m_rule;

	// controls
	HWND m_hModifierListCombo;
	HWND m_hRuleCombo;
	HWND m_hClassListCombo;
	HWND m_hSClassListCombo;
	HWND m_hClassLabel;
	HWND m_hSClassLabel;
	HWND m_hDirLabel;
	HWND m_hFileEdit;
	HWND m_hFileButton;
	HWND m_hTextLabel;
	HWND m_hTextEdit;

	Interface *m_ip;
};


//
//   Class Name: RuleListDialog
// Base Classes: DialogClass
//  Description: Dialog for creating a collection of rules for selecting an attribute class
//
class RuleListDialog : public DialogClass
{
public:
	RuleListDialog() : DialogClass(IDD_RULES) {}

	virtual void OnInitDialog();
	virtual void OnCommand(s32 id, s32 notifyCode);
	virtual void OnOK();
	virtual void UpdateControls();
	virtual void UpdateData();

	void InsertRuleIntoList(Rule& rule, s32 posn);
	void EnableButtons();
	void AddRule();
	void EditRule();
	void DeleteRule();

	// data
	SelectionRules m_rules;

	// controls
	HWND m_hName;
	HWND m_hClassCombo;
	HWND m_hRuleList;
	HWND m_hDeleteButton;
	HWND m_hEditButton;

	Interface *m_ip;
};

#endif //INC_RULE_LIST_DIALOG


