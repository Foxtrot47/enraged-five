//
//
//    Filename: MaxDataMgr.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 25/06/99 14:52 $
//   $Revision: 1 $
// Description: 
//
//
#pragma warning (disable : 4786)
#pragma warning (disable : 4251)
// My headers
#include "MaxDataMgr.h"
#include "CustAttribData.h"
#include "RightClickMenu.h"
#include "EditorUtility.h"
#include "IFpMaxDataStore.h"

#include "Configuration.h"
// STD headers
#include <fstream>
//#include "script.h"
#include "system/exec.h"

#define INVALID_CLASSNAME	"_INVALID"
#define STD_ATTRIBUTES_FILE	"max.att"

// config file tags
#define ATTRIBUTE_FILE_TAG	"attrib"

MaxDataMgr theMaxDataMgr;

MaxDataMgrClassDesc theMaxDataMgrDesc;
ClassDesc* GetMaxDataMgrDesc() {return &theMaxDataMgrDesc;}

extern INT_PTR CALLBACK AttributeDialogProc( HWND hwndDlg, UINT msg, WPARAM wParam, LPARAM lParam );

//
// ACTION stuff
//
// action callback function
class MaxDataStoreActionCB : public ActionCallback
{
public:
	void SetInterfacePtr(Interface* ip) {m_ip = ip;}
	BOOL ExecuteAction(int id){
		BOOL rt = TRUE;

		if(id == DeleteAttributeActionId)
			theMaxDataMgr.StoreSelectedNodes(m_ip, true);
		else
			theMaxDataMgr.StoreSelectedNodes(m_ip);

		switch(id)
		{
		case AddAttributeActionId:
			{
				for(s32 i=0; i<theMaxDataMgr.GetNumSelectedNodes(); i++)
				{
					INode* pNode = theMaxDataMgr.GetSelectedNode(i);
					theMaxDataMgr.AddAttributes(pNode);
				}
			}
			break;

		case DeleteAttributeActionId:
			{
				for(s32 i=0; i<theMaxDataMgr.GetNumSelectedNodes(); i++)
				{
					INode* pNode = theMaxDataMgr.GetSelectedNode(i);
					theMaxDataMgr.DeleteAttributes(pNode);
				}
			}
			break;

		case EditAttributeActionId:
			{
				//can now edit if there are multiple files selected
				//as long as they are all of the same class
				rt = theMaxDataMgr.LaunchAttributeEditor();
			}
			break;

		case EditAttributeOldActionId:
			{
				//can now edit if there are multiple files selected
				//as long as they are all of the same class
				rt = theMaxDataMgr.EditSelectedDataModal();
			}
			break;


		case CopyAttributeActionId:
			{
				if(theMaxDataMgr.GetNumSelectedNodes() == 1)
					theMaxDataMgr.CopyData(theMaxDataMgr.GetSelectedNode(0));
				else
					rt = FALSE;
			}
			break;

		case PasteAttributeActionId:
			{
				for(s32 i=0; i<theMaxDataMgr.GetNumSelectedNodes(); i++)
				{
					INode* pNode = theMaxDataMgr.GetSelectedNode(i);
					if(theMaxDataMgr.IsCopyAvailable(pNode))
						theMaxDataMgr.PasteData(pNode);
				}
			}
			break;

		default:
			rt = FALSE;
			break;
		}

		theMaxDataMgr.RestoreSelectedNodes(m_ip, TRUE);

		return rt;
	}
private:
	Interface* m_ip;
};

// 
// action table class
//
class MaxDataStoreActionTable : public ActionTable
{
public:
	MaxDataStoreActionTable(ActionTableId id, ActionContextId contextId, TSTR& name, 
		HACCEL hDefaults, int numIds, ActionDescription* pOps, HINSTANCE hInst, Interface* ip) :
	ActionTable(id, contextId, name, hDefaults, numIds, pOps, hInst),
	m_ip(ip) {}

	BOOL IsEnabled(int cmdId)
	{
		std::string className;
		std::string classNameSave;
		BOOL rt = TRUE;

		switch(cmdId)
		{
		case AddAttributeActionId:
			{
				s32 count = 0;
				for(s32 i=0; i<m_ip->GetSelNodeCount(); i++)
				{
					INode* pNode = m_ip->GetSelNode(i);
					theMaxDataMgr.GetClassFromRefTarget(pNode, className);
					if(className == INVALID_CLASSNAME || theMaxDataMgr.GetAttributes(pNode))
						continue;
					count++;
				}
				if(count == 0)
					return FALSE;
			}
			break;

		case DeleteAttributeActionId:
			{
				s32 count = 0;
				for(s32 i=0; i<m_ip->GetSelNodeCount(); i++)
				{
					INode* pNode = m_ip->GetSelNode(i);
					if(theMaxDataMgr.GetAttributes(pNode))
						count++;
				}
				if(count == 0)
					return FALSE;
			}
			break;

		case EditAttributeOldActionId:
		case EditAttributeActionId:

			{
				//can now edit if there are multiple files selected
				//as long as they are all of the same class

				int iSize = m_ip->GetSelNodeCount();

				if(iSize < 1)
				{
					return FALSE;
				}

				theMaxDataMgr.GetClassFromRefTarget(m_ip->GetSelNode(0), classNameSave);

				for(int i=0;i<iSize;i++)
				{
					theMaxDataMgr.GetClassFromRefTarget(m_ip->GetSelNode(i), className);

					//disable if class isnt available
					if(!theMaxDataMgr.IsClassAvailable(className) || !theMaxDataMgr.IsDialogAvailable(className))
					{
						return FALSE;
					}

					//disable if this class name is not the same as the first selection
					if(classNameSave != className)
					{
						return FALSE;
					}
				}
			}
			break;

		case CopyAttributeActionId:
			{
				// disable is node count is not 1
				if(m_ip->GetSelNodeCount() != 1)
					return FALSE;
				// disable is class isnt available
				theMaxDataMgr.GetClassFromRefTarget(m_ip->GetSelNode(0), className);
				if(!theMaxDataMgr.IsClassAvailable(className) || !theMaxDataMgr.IsDialogAvailable(className))
					return FALSE;
			}
			break;

		case PasteAttributeActionId:
			{
				if(m_ip->GetSelNodeCount() == 0)
					return FALSE;
				for(s32 i=0; i<m_ip->GetSelNodeCount(); i++)
				{
					INode* pNode = m_ip->GetSelNode(i);
					if(!theMaxDataMgr.IsCopyAvailable(pNode))
						return FALSE;
				}
			}
			break;
		}
		return rt;
	}

private:
	Interface* m_ip;
};

MaxDataStoreActionCB theMaxDataStoreActionCB;

//
// name:		BuildActionTable
// description:	Build an action table for MaxDataStore actions
//
ActionTable* BuildActionTable()
{
	static ActionDescription aActions[] = {
		AddAttributeActionId, IDS_ADD_ATTR, IDS_ADD_ATTR, IDS_CATEGORY,
		DeleteAttributeActionId, IDS_DELETE_ATTR, IDS_DELETE_ATTR, IDS_CATEGORY,
		EditAttributeActionId, IDS_EDIT_ATTR, IDS_EDIT_ATTR, IDS_CATEGORY,
		CopyAttributeActionId, IDS_COPY_ATTR, IDS_COPY_ATTR, IDS_CATEGORY,
		PasteAttributeActionId, IDS_PASTE_ATTR, IDS_PASTE_ATTR, IDS_CATEGORY,
		EditAttributeOldActionId, IDS_EDIT_ATTR_OLD, IDS_EDIT_ATTR_OLD, IDS_CATEGORY
	};
	TSTR name = GetString(IDS_DATASTORE_NAME);

    ActionTable* pTab = new MaxDataStoreActionTable(MAXDATAMGR_ACTIONTABLEID, MAXDATAMGR_ACTIONCONTEXTID, name, NULL,
        6, aActions, hInstance, GetCOREInterface());        
    GetCOREInterface()->GetActionManager()->RegisterActionContext (MAXDATAMGR_ACTIONCONTEXTID, "MaxDataStore");
    return pTab;
}


ActionTable* GetActionTable()
{
	static ActionTable* pActionTable = NULL;
	if(pActionTable == NULL)
		pActionTable = BuildActionTable();
	return pActionTable;
}

ActionTable* MaxDataMgrClassDesc::GetActionTable(int i)
{
	return ::GetActionTable();
}

void AddQuadMenuItems(Interface* ip)
{
	IMenuManager* pMenuMan = ip->GetMenuManager();
	bool newlyRegistered = pMenuMan->RegisterMenuBarContext(MAXDATAMGR_MENUCONTEXTID, "MaxDataStore Context");
	
	IMenu* p_FindMenu = pMenuMan->FindMenu("Attributes");

	while(p_FindMenu)
	{
		pMenuMan->UnRegisterMenu(p_FindMenu);
		p_FindMenu = pMenuMan->FindMenu("Attributes");
	}

	// if menu already registered then return
//	if(!newlyRegistered)
//		return;
	
	// get main viewport quad menu
	IQuadMenuContext *pContext = (IQuadMenuContext*)pMenuMan->GetContext(kViewportQuad);
	// get no button pressed menu context
	IQuadMenu* pQMenu = pContext->GetRightClickMenu(IQuadMenuContext::kNonePressed);
	IMenu* pMenu = pQMenu->GetMenu(0);
	
	ActionTable* pActionTable = GetActionTable();
	ActionItem* pAddAction = pActionTable->GetAction( AddAttributeActionId );
	ActionItem* pEditAction = pActionTable->GetAction( EditAttributeActionId );
	ActionItem* pCopyAction = pActionTable->GetAction( CopyAttributeActionId );
	ActionItem* pPasteAction = pActionTable->GetAction( PasteAttributeActionId );
	
	// create sub menu
	IMenu* pSubMenu = GetIMenu();
	pSubMenu->SetTitle("Attributes");
	pMenuMan->RegisterMenu(pSubMenu, 0);

	// create menu items
	IMenuItem* pAddItem = GetIMenuItem();
	pAddItem->SetActionItem(pAddAction);
	IMenuItem* pEditItem = GetIMenuItem();
	pEditItem->SetActionItem(pEditAction);
	IMenuItem* pCopyItem = GetIMenuItem();
	pCopyItem->SetActionItem(pCopyAction);
	IMenuItem* pPasteItem = GetIMenuItem();
	pPasteItem->SetActionItem(pPasteAction);
	
	// add them to submenu
	pSubMenu->AddItem(pAddItem);
	pSubMenu->AddItem(pEditItem);
	pSubMenu->AddItem(pCopyItem);
	pSubMenu->AddItem(pPasteItem);
	
     // Create a new menu item to hold the sub-menu
	IMenuItem* pSubMenuItem = GetIMenuItem();
	pSubMenuItem->SetSubMenu(pSubMenu);
	pMenu->AddItem(pSubMenuItem);

	pMenuMan->UpdateMenuBar();
}

//
//        name: MaxDataMgr::MaxDataMgr
// description: Constructor
//
MaxDataMgr::MaxDataMgr()
{
	m_hasLoadedScript = false;
	m_hasInitialised = false;
	m_attributeFile = STD_ATTRIBUTES_FILE;
	m_hMainWnd = NULL;
	m_pCopy = NULL;
	m_selectedNodesStored = false;
}

//
//        name: MaxDataMgr::~MaxDataMgr
// description: Destructor
//
MaxDataMgr::~MaxDataMgr()
{
}

DWORD MaxDataMgr::Start()
{
	Interface* ip = GetCOREInterface();

	// action shit
	ip->GetActionManager()->ActivateActionTable(&theMaxDataStoreActionCB, MAXDATAMGR_ACTIONTABLEID);
	theMaxDataStoreActionCB.SetInterfacePtr(ip);
	// add attribute menu to quad menu
	AddQuadMenuItems(ip);

	// Load rules from script file etc
	Init();

	return GUPRESULT_KEEP;
}

void MaxDataMgr::Stop()
{
	if(m_hasInitialised)
		SaveConfigFile();
}

DWORD_PTR MaxDataMgr::Control(DWORD parameter)
{
	return 0;
}

FPInterfaceDesc* MaxDataMgr::GetDesc() { return NULL; }

RefTargetHandle GetOldRef(RefTargetHandle ref)
{
	if(ref && ref->SuperClassID() == BASENODE_CLASS_ID)
	{
		INode* pNode = (INode*)ref;

		TCHAR* pName = pNode->GetName();

		ref = pNode->GetObjectRef();
	}

	while(ref && ref->SuperClassID() == GEN_DERIVOB_CLASS_ID)
	{
		IDerivedObject *pDerObj = (IDerivedObject*) ref;
		ref = pDerObj->GetObjRef();
	}

	return ref;
}

//
// name:		GetCorrectRef
// description:	Given a reference return the correct reference the attributes should be added to
RefTargetHandle GetCorrectRef(RefTargetHandle ref)
{
	RefTargetHandle oldRef = GetOldRef(ref);
	CustAttribData* p_oldData = theMaxDataMgr.GetCustAttrib(oldRef);
	CustAttribData* p_newData = theMaxDataMgr.GetCustAttrib(ref);

	if(!p_newData && p_oldData)
	{
		p_newData = theMaxDataMgr.AddCustAttrib(ref);
		p_newData->GetData() = p_oldData->GetData();

		theMaxDataMgr.DeleteAttributes(oldRef);
	}

	return ref;
}

//
// name:		NumRefs
// description:	Return number of references maxdatamgr makes
int MaxDataMgr::NumRefs()
{
	return 0;
}

//
// name:		GetReference
// description:	Return pointer to reference
RefTargetHandle MaxDataMgr::GetReference(int i)
{
	return NULL;
}

//
// name:		SetReference
// description:	Set reference pointer
void MaxDataMgr::SetReference(int i, RefTargetHandle ref)
{
}

//
// name:		NotifyRefChanged
// description:	React to a reference being changed
RefResult MaxDataMgr::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,PartID& partID,RefMessage message) 
{
	return REF_SUCCEED;
}

//
//        name: MaxDataMgr::Init
// description: Initialise the data manager. Load config file
//          in: 
//         out: 
//
void MaxDataMgr::Init()
{
	if(m_hMainWnd == NULL)
	{
		Interface *ip = GetCOREInterface();
		if(ip)
		{
			m_hMainWnd = ip->GetMAXHWnd();
		}
	}

	if(!m_hasInitialised)
	{
		LoadConfigFile();
		m_hasInitialised = true;
		LoadScriptFile();
	}
}

//
//        name: MaxDataMgr::SetAttributesFilename/GetAttributesFilename
// description: Set or Get the attribute filename
//
void MaxDataMgr::SetAttributesFilename(const std::string& filename) 
{
	Init(); 
	m_attributeFile = filename;
}
const std::string& MaxDataMgr::GetAttributesFilename() 
{
	Init(); 
	return m_attributeFile;
}

//
//        name: MaxDataMgr::LoadScriptFile
// description: Load the attribute class file
//
void MaxDataMgr::LoadScriptFile()
{
	m_hasLoadedScript = true;
	HWND hMaxWnd = NULL;
	Interface *ip = GetCOREInterface();
	if(ip)
		hMaxWnd = ip->GetMAXHWnd();

	try {
		m_attributeManager.ReadScriptFile( m_attributeFile.c_str() );
	}
	catch (dmat::AttributeScriptError err) {
		std::ostringstream serror;
		serror << "ERROR: " << err.GetError() << ", Line " << err.GetLineNumber();
		MessageBox(hMaxWnd, serror.str().c_str(), "Attribute Script File Script Error", MB_OK);
		m_hasLoadedScript = false;
	}
	catch (dmat::AttributeFileError err) {
		std::stringstream sstr;
		sstr << "Failed to load Attribute Script File: \n\"" << m_attributeFile.c_str() << "\".\nScene attributes will be lost on saving file. Not safe to continue." << std::ends;
		MessageBox(m_hMainWnd, sstr.str().c_str(), "MaxDataStore", MB_ICONEXCLAMATION);
		throw MAXException("File load error", -666);
		m_hasLoadedScript = false;
	}
}

//
//        name: *MaxDataMgr::CreateInstance
// description: Create an instance of a attribute class
//          in: className = name of class
//         out: attribute class instance
//
dmat::AttributeInst *MaxDataMgr::CreateInstance(const std::string &className)
{
	Init();
	return m_attributeManager.CreateInstance(className);
}

//
//        name: *MaxDataMgr::GetClass
// description: Get attribute class descriptor from name
//          in: className = name of class
//         out: pointer to attribute class 
//
dmat::AttributeClass *MaxDataMgr::GetClass(const std::string &className)
{
	Init();
	return m_attributeManager.GetClass(className);
}

// 
//        name: *MaxDataMgr::GetDialogDesc
// description: Return a dialog description
//          in: dialogName = name of dialog
// 
// const dmat::AttributeDialogDesc *MaxDataMgr::GetConstDialogDesc(const std::string &dialogName)
// {
// 	Init(); 
// 	return m_attributeManager.GetDialogDesc(dialogName);
// }
//
//        name: *MaxDataMgr::GetDialogDesc
// description: Return a dialog description
//          in: dialogName = name of dialog
//
dmat::AttributeDialogDesc *MaxDataMgr::GetDialogDesc(const std::string &dialogName)
{
	Init(); 
	return m_attributeManager.GetDialogDesc(dialogName);
}

//
//        name: MaxDataMgr::IsClassAvailable/IsDialogAvailable
// description: Returns is a class or dialog is available
//          in: name = name of class/dialog
//         out: is available
//
bool MaxDataMgr::IsClassAvailable(const std::string &name) 
{
	Init(); 
	return m_attributeManager.IsClassAvailable(name);
}
bool MaxDataMgr::IsDialogAvailable(const std::string &name) 
{
	Init(); 
	return m_attributeManager.IsDialogAvailable(name);
}
//
//        name: MaxDataMgr::Reset
// description: Reset the max data manager
//
void MaxDataMgr::Reset()
{
	if(m_hasInitialised)
		SaveConfigFile();
	m_attributeManager.Clear();
	m_rules.Clear();
	m_pCopy = NULL;
	m_hasInitialised = false;
	m_hasLoadedScript = false;
}

//
//        name: MaxDataMgr::GetClassFromRefTarget
// description: Return the attribute class from a reference target pointer
//          in: rtarg = pointer to reference target
//				className = string to fill with class name
//
void MaxDataMgr::GetClassFromRefTarget(RefTargetHandle rtarg,std::string& className)
{
	Init();
	if(rtarg->SuperClassID() == BASENODE_CLASS_ID || rtarg->SuperClassID() == TEXMAP_CLASS_ID)
	{
		TSTR name;
//		INode *pINode = static_cast<INode *>(rtarg);

		// parse rules and return attributes
		for(s32 i=0; i<m_rules.GetSize(); i++)
		{
			SelectionRules& selRule = m_rules.GetSelectionRule(i);
			if(selRule.IsValid(rtarg))
			{
				className = selRule.GetClassName();
				return;
			}
		}
		INode *pINode = dynamic_cast<INode *>(rtarg);
		if(pINode)
		{
			Object *pObject =  pINode->EvalWorldState(0).obj;
			pObject->GetClassName(name);
			className = (const char *)name;
			return;
		}
		// If class does not exist then set class name to invalid
		if(!IsClassAvailable(className))
			className = INVALID_CLASSNAME;
	}
	else
	{
		TSTR name;
		rtarg->GetClassName(name);
		className = (const char *)name;
	}
}

//
//        name: MaxDataMgr::IsRefTargetThisClass
// description: Return if reference target uses the provided attribute class
//          in: rtarg = pointer to reference target
//				className = string to fill with class name
//
bool MaxDataMgr::IsRefTargetThisClass(Animatable* rtarg, const std::string& className)
{
	Init();
	if(rtarg->SuperClassID() == BASENODE_CLASS_ID)
	{
		TSTR name;
		INode *pINode = static_cast<INode *>(rtarg);

		// parse rules and return attributes
		for(s32 i=0; i<m_rules.GetSize(); i++)
		{
			SelectionRules& selRule = m_rules.GetSelectionRule(i);
			if(selRule.GetClassName() == className && selRule.IsValid(pINode))
			{
				return true;
			}
		}
		return false;
	}
	else
	{
		TSTR name;
		rtarg->GetClassName(name);
		return (className == (const char *)name);
	}
}

//
//        name: MaxDataMgr::GetCustAttrib
// description: Get custom attribute from animatable
//          in: ref = pointer to reference target
//
CustAttribData* MaxDataMgr::GetCustAttrib(Animatable* pAnimatable)
{
	//old style tv node. baaaaaaaaaaaaaaaaad!
//	if(pAnimatable->ClassID() == Class_ID(0x485628f2, 0x24e267b8))
//	{
//		return NULL;
//	}

	if(!pAnimatable)
	{
		return NULL;
	}

	ICustAttribContainer* pContainer = pAnimatable->GetCustAttribContainer();

	if(pContainer)
	{
		for(s32 i=0; i<pContainer->GetNumCustAttribs(); i++)
		{
			CustAttrib* pCustAttrib = pContainer->GetCustAttrib(i);
			const MCHAR *attrName = pCustAttrib->GetName();
			if(pCustAttrib && !strcmp(attrName, CUSTATTRIB_ATTR_NAME))
			{
				return (CustAttribData*)pCustAttrib;
			}
		}
	}

	return NULL;
}

//
//        name: MaxDataMgr::AddCustAttrib
// description: Add custom attribute to animatable
//          in: ref = pointer to reference target
//
CustAttribData* MaxDataMgr::AddCustAttrib(Animatable* pAnimatable)
{
	ICustAttribContainer* pContainer = pAnimatable->GetCustAttribContainer();
	CustAttribData* pCustAttrib;

	if(pContainer == NULL)
	{
		pAnimatable->AllocCustAttribContainer();
		pContainer = pAnimatable->GetCustAttribContainer();
	}
	if(pContainer)
	{
		for(s32 i=0; i<pContainer->GetNumCustAttribs(); i++)
		{
			CustAttrib* pCustAttrib = pContainer->GetCustAttrib(i);
			const MCHAR *attrName = pCustAttrib->GetName();
			if(pCustAttrib && !strcmp(attrName, CUSTATTRIB_ATTR_NAME))
			{
				return (CustAttribData*)pCustAttrib;
			}
		}
	}
	else
		return NULL;

	pCustAttrib = (CustAttribData*)::CreateInstance(CUST_ATTRIB_CLASS_ID, CUSTATTRIB_ATTR_CLASSID);

	pContainer->InsertCustAttrib(0, pCustAttrib);
	for(s32 i=0; i<pContainer->GetNumCustAttribs(); i++)
	{
		CustAttrib* pCustAttrib = pContainer->GetCustAttrib(i);
		const MCHAR *attrName = pCustAttrib->GetName();
		DebugPrint("Container now has CustAttr data of name %s\n", attrName);
	}
	return pCustAttrib;
}

//
// name:		GetCustAttribFromCorrectRef
// description:	Given a reference target handle find the attributes for that reference
//
CustAttribData* MaxDataMgr::GetCustAttribFromCorrectRef(RefTargetHandle ref)
{
	RefTargetHandle ref2 = GetCorrectRef(ref);

	return GetCustAttrib(ref2);
}

//
//        name: MaxDataMgr::GetAttributes
// description: get attributes to reference
//          in: ref = pointer to object to reference
//         out: pointer to attributes
//
DataInstance* MaxDataMgr::GetAttributes(RefTargetHandle ref)
{
	CustAttribData* pCustAttrib = GetCustAttribFromCorrectRef(ref);

	// if node exists return data
	if(pCustAttrib)
		return &pCustAttrib->GetData();

	return NULL;
}

//
//        name: MaxDataMgr::AddAttributes
// description: Add attributes to reference
//          in: ref = pointer to object to reference
//				name = name of track view node
//         out: pointer to new attributes
//
DataInstance* MaxDataMgr::AddAttributes(RefTargetHandle ref)
{
	std::string className;

	// if no attribute class available for object then return NULL
	GetClassFromRefTarget(ref, className);
	if(!theMaxDataMgr.IsClassAvailable(className))
		return NULL;

	CustAttribData* pCustAttrib = GetCustAttribFromCorrectRef(ref);

	DataInstance *pData;

	// if node exists return data
	if(pCustAttrib)
	{
		//should do a check here to see if the class of the object
		//has not been changed

		// Is attribute data invalid
		if(pCustAttrib->IsInvalid())
		{
			//remove the dialog reference
			HWND hWnd = pCustAttrib->GetRollupHWnd();
			if(hWnd)
			{
				SetWindowLongPtr(hWnd,GWLP_USERDATA,0);
			}	

			DeleteAttributes(ref);
		}
		else
		{
			DataInstance* p_Ret = &pCustAttrib->GetData();

			if(p_Ret->GetClassName() == className)
				return p_Ret;

			DeleteAttributes(ref);
		}
	}

	theHold.Begin();

	RefTargetHandle ref2 = GetCorrectRef(ref);

	pCustAttrib = AddCustAttrib(ref2);
	pData = &pCustAttrib->GetData();
	pData->AddAttributes(ref);

	if(pData->GetAttributes() == NULL)
	{
		// shouldn't get here
		theHold.Cancel();
		return NULL;
	}
	else
		theHold.Accept("Add Attributes");

	return pData;
}

//
//        name: MaxDataMgr::AddAttributes
// description: Add attributes to reference
//          in: ref = pointer to object to reference
//				name = name of track view node
//         out: pointer to new attributes
//
void MaxDataMgr::DeleteAttributes(RefTargetHandle ref)
{
	CustAttribData* pCustAttrib = GetCustAttribFromCorrectRef(ref);

	if(pCustAttrib)
	{
		theHold.Begin();
		
		ref = GetCorrectRef(ref);
		ICustAttribContainer* pContainer = ref->GetCustAttribContainer();

		if(pContainer)
		{
			for(s32 i=0; i<pContainer->GetNumCustAttribs(); i++)
			{
				if(pCustAttrib == pContainer->GetCustAttrib(i))
				{
					pContainer->RemoveCustAttrib(i);
					break;
				}
			}
		}

		theHold.Accept("Delete Attributes");
	}
}

//
//        name: EditSelectedDataModal
// description: Edits Attributes of multiple selected objects together
//				as long as they are all of the same class
//

BOOL MaxDataMgr::LaunchAttributeEditor()
{
	EditSelectedDataModal(false);

	bool res=true;
	FileStream* pFile = new FileStream();

	char cToolsRoot[MAX_PATH];
	sysGetEnv("RS_TOOLSROOT", cToolsRoot, MAX_PATH);

	char cSciptPath[MAX_PATH];
	sprintf(cSciptPath, "%s/dcc/current/max2012/scripts/pipeline/ui/RsAttributeEditor.ms", cToolsRoot);

	pFile->open(cSciptPath , "rb");
	ExecuteScript(pFile, &res);
	pFile->close();

	return TRUE;
}

BOOL MaxDataMgr::EditSelectedDataModal(bool bLaunchDialog)
{
	std::string className;
	std::string classNameSave;
	BOOL rt;
	dmat::AttributeInstMultiple* mp_attMult = NULL;

	int iSize = m_selectedNodes.Count();

	rt = TRUE;

	if(iSize < 1)
	{
		rt = FALSE;
	}
	else
	{
		//for each one, first check that the attached attributes
		//are for the correct class (in case the user has changed
		//the type of object since the attributes were attached)

		GetClassFromRefTarget(m_selectedNodes[0], classNameSave);

		for(int i=0;i<iSize;i++)
		{
			SyncData(m_selectedNodes[i]);

			GetClassFromRefTarget(m_selectedNodes[i], className);

			DataInstance *pData = theMaxDataMgr.AddAttributes(m_selectedNodes[i]);

			if(pData->GetClassName() != className)
			{
				char cMsg[4096];

				sprintf(cMsg,"Attributes of %s will be changed from %s to %s. Continue?",m_selectedNodes[i]->GetName(),pData->GetClassName().c_str(),className.c_str());

				if(MessageBox(GetCOREInterface()->GetMAXHWnd(),cMsg,"Oh dear...",MB_YESNO) == IDNO)
				{
					return FALSE;
				}

				theMaxDataMgr.DeleteAttributes(m_selectedNodes[i]);
			}

			//disable if class isnt available
			if(!IsClassAvailable(className) || !IsDialogAvailable(className))
			{
				rt = FALSE;
			}

			//disable if this class name is not the same as the first selection
			if(classNameSave != className)
			{
				rt = FALSE;
			}
		}

		if(rt == TRUE)
		{
			DataInstance *pData = theMaxDataMgr.AddAttributes(m_selectedNodes[0]);

			if(!pData)
			{
				return FALSE;
			}

			//we need to create a temporary AttributeInstMultiple from the
			//data in each selected AttributeInst first, open the dialog
			//and then after weve done our changes, copy the data back
			//into each of the AttributeInst's

			//create the multiple instance from the class type of the
			//first class (assuming we have got through checking that
			//all the selected objects are of the same class)
			//copy the data from the first selected object

			mp_attMult = new dmat::AttributeInstMultiple(*(pData->GetAttributes()));

			//merge in each of selected objects data, turning off
			//the universal flag if a data item in the added
			//instance is different
			for(s32 i=1; i<iSize; i++)
			{
				DataInstance *pDataSel = theMaxDataMgr.AddAttributes(m_selectedNodes[i]);

				if(!pDataSel)
				{
					return FALSE;
				}

				mp_attMult->merge(pDataSel->GetAttributes());
			}		

			//edit the multiple instance
			const dmat::AttributeDialogDesc *pDialogDesc = theMaxDataMgr.GetDialogDesc(pData->GetClassName());
			dmat::AttributeDialog dialog;

			if((pData->GetAttributes() == NULL || pDialogDesc == NULL) == false)
			{
				dialog.ConstructDialog2(hInstance, mp_attMult, pDialogDesc, (pData->GetClassName() + " Attributes").c_str());
				
				if(bLaunchDialog)
				{
					DialogBoxIndirectParam(hInstance, dialog.GetTemplate(), theMaxDataMgr.GetMainWnd(), &AttributeDialogProc, (LPARAM)&dialog);
				}
			}

			//copy back into the attribute instances from the multiple one
			for(s32 i=0; i<iSize; i++)
			{
				DataInstance *pDataSel = theMaxDataMgr.AddAttributes(m_selectedNodes[i]);
				pDataSel->SetAttributes(mp_attMult);
			}

			//this should sync the rollup to the attributes
			for(s32 i=0;i<iSize;i++)
			{
				SyncData(m_selectedNodes[i]);
			}

			//delete the multiple instance
			delete mp_attMult;
		}
	}

	return rt;
}

//
//        name: SyncData
// description: update attributes if rollup UI is displayed
//          in: rtarg = pointer to reference target
//
void MaxDataMgr::SyncData(RefTargetHandle ref)
{
	CustAttribData* pCustAttrib = GetCustAttribFromCorrectRef(ref);

	if(pCustAttrib)
	{
		HWND hWnd = pCustAttrib->GetRollupHWnd();
		if(hWnd)
		{
			dmat::AttributeDialog *pDialog = (dmat::AttributeDialog *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
			if(pDialog)
				pDialog->SyncWithAttributes();
		}
	}

}

//
//        name: EditDataModal
// description: Edit attribute data in a modal dialog
//          in: rtarg = pointer to reference target
//
void MaxDataMgr::EditDataModal(RefTargetHandle ref)
{
	SyncData(ref);
			
	DataInstance *pData = theMaxDataMgr.AddAttributes(ref);

	if(pData)
	{
		pData->EditModal();

		SyncData(ref);
	}
}

//
//        name: MaxDataStore::CopyData
// description: Copy attribute data from INode
//
void MaxDataMgr::CopyData(RefTargetHandle rtarg)
{
	if(m_pCopy)
		delete m_pCopy;

	CustAttribData* pCustAttrib = GetCustAttribFromCorrectRef(rtarg);
	if(pCustAttrib)
	{
		HWND hWnd = pCustAttrib->GetRollupHWnd();
		if(hWnd)
		{
			dmat::AttributeDialog *pDialog = (dmat::AttributeDialog *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
			if(pDialog)
				pDialog->SyncWithControls();
		}
	}

	DataInstance *pData = GetAttributes(rtarg);
	
	if(pData)
		m_pCopy = new DataInstance(*pData);
	else
	{
		m_pCopy = new DataInstance;
		m_pCopy->AddAttributes(rtarg);
	}
}

//
//        name: MaxDataStore::PasteData
// description: Paste attribute data onto INode
//
void MaxDataMgr::PasteData(RefTargetHandle ref)
{
	assert(m_pCopy);

	CustAttribData* pCustAttrib = GetCustAttribFromCorrectRef(ref);

	if(!pCustAttrib)
	{
		pCustAttrib = AddCustAttrib(ref);
	}

	(pCustAttrib->GetData()) = *m_pCopy;

	HWND hWnd = pCustAttrib->GetRollupHWnd();
	if(hWnd)
	{
		dmat::AttributeDialog *pDialog = (dmat::AttributeDialog *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
		if(pDialog)
			pDialog->Edit(pCustAttrib->GetData().GetAttributes());
	}

}

//
//        name: MaxDataStore::IsCopyAvailable
// description: Return if there is a DataInstance available for pasting onto this reference target
//          in: pINode= pointer to INode
//
bool MaxDataMgr::IsCopyAvailable(RefTargetHandle rtarg)
{
	if(m_pCopy == NULL)
		return false;

	std::string className;

	// if INode has a different attribute class from the copy then return false
	theMaxDataMgr.GetClassFromRefTarget(rtarg, className);

	if(className != m_pCopy->GetClassName())
		return false;

	return true;
}



//
//        name: MaxDataStore::StoreSelectedNodes
// description: Store all the selected nodes in a temporary list and deselect everything
void MaxDataMgr::StoreSelectedNodes(Interface* ip, bool forceDeselect)
{
	INode* pNode;
	for(s32 i=0; i<ip->GetSelNodeCount(); i++)
	{
		pNode = ip->GetSelNode(i);
		m_selectedNodes.Append(1, &pNode, TRUE);
	}

	if(forceDeselect)
	{
		ip->ClearNodeSelection(FALSE);
		m_selectedNodesStored = true;
		return;
	}
	// don't bother unselecting stuff if there is only one object selected
	if(ip->GetSelNodeCount() == 1)
		return;
	
	// If there is more than one node selected and one of them doesn't have attributes then clear
	// max selection
	for(s32 i=0; i<ip->GetSelNodeCount(); i++)
	{
		if(GetAttributes(ip->GetSelNode(i)) == NULL)
		{
			ip->ClearNodeSelection(FALSE);
			m_selectedNodesStored = true;
			break;
		}
	}

}
//
//        name: MaxDataStore::RestoreSelectedNodes
// description: Select all nodes in temporary list
void MaxDataMgr::RestoreSelectedNodes(Interface* ip, bool redraw)
{
	if(m_selectedNodesStored)
	{
		ip->SelectNodeTab(m_selectedNodes, TRUE, redraw);
	}
	m_selectedNodes.Delete(0, m_selectedNodes.Count());
	m_selectedNodesStored = false;
}

