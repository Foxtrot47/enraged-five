// 
// /maxDosCommand.h
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef _MAX_DOS_COMMAND_H_

#include <windows.h>
#include <string>
#include <sstream>

namespace rage
{

//-----------------------------------------------------------------------------

class RageMaxDosCmd
{
public:
	RageMaxDosCmd();
	virtual ~RageMaxDosCmd();

public:
	BOOL	Open(LPCSTR pszCommandLine, LPCSTR pszStartPath = NULL);
	BOOL	Printf(LPCSTR pszFormat, ...);
	void	SetWaitTime(DWORD dwWaitTime) { m_dwWaitTime = dwWaitTime; }
	BOOL	WaitForProcessToTerminate(DWORD dwTimwout=INFINITE);
	DWORD	GetProcessExitCode() const { return m_dwProcessExitCode; }
	
	virtual void Close();

protected:
	BOOL LaunchChild(LPCSTR pszCmdLine, LPCSTR pszStartPath, HANDLE hStdOut, HANDLE hStdIn, HANDLE hStdErr);
	int	 RedirectStdout();
	void DestroyHandle(HANDLE& rhObject);

	static DWORD WINAPI OutputThread(LPVOID lpvThreadParam);

protected:
	virtual void WriteStdOut(LPCSTR pszOutput, DWORD dwCount) {};
	virtual void WriteStdError(LPCSTR pszError, DWORD dwCount) {};

private:
	HANDLE	m_hThread;
	DWORD	m_dwThreadId;
	HANDLE	m_hEvtStop;
	DWORD	m_dwWaitTime;	

	HANDLE	m_hStdInWrite;
	HANDLE	m_hStdOutRead;
	HANDLE	m_hChildProcess;
	DWORD	m_dwProcessExitCode;

	std::string	m_strBuffer;
};

//-----------------------------------------------------------------------------

class RageMaxDosCmdStringRedirect : public RageMaxDosCmd
{
public:
	RageMaxDosCmdStringRedirect();
	virtual ~RageMaxDosCmdStringRedirect() {};

public:
	const char* GetResultBuffer() const { return m_strBuffer.c_str(); }

protected:
	virtual void WriteStdOut(LPCSTR pszOutput, DWORD dwCount);
	virtual void WriteStdError(LPCSTR pszError, DWORD dwCount);

private:
	std::string m_strBuffer;
};

//-----------------------------------------------------------------------------

class RageMaxDosCmdFileRedirect : public RageMaxDosCmd
{
public:
	RageMaxDosCmdFileRedirect();
	virtual ~RageMaxDosCmdFileRedirect() {};
	
public:
	void	SetOutputFile(HANDLE hFile) {m_outFile = hFile; }
	
protected:
	virtual void WriteStdOut(LPCSTR pszOutput, DWORD dwCount);
	virtual void WriteStdError(LPCSTR pszError, DWORD dwCount);

private:
	HANDLE			m_outFile;
};

//-----------------------------------------------------------------------------

} //End namespace rage

#endif //_MAX_DOS_COMMAND_H

