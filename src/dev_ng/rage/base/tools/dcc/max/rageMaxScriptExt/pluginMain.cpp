// 
// /pluginMain.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#include "atl/string.h"
#include "parser/manager.h"
#include "system/param.h"

#define RAGE_SCRIPT_PATH_TAG	"rageMaxScriptRoot"
#define RAGEMAXSCRIPT_API __declspec( dllexport )

using namespace rage;

HINSTANCE	g_hDllInstance;
int			g_controlsInit = FALSE;
bool		g_libInitialized = FALSE;
static char g_description[1024];
char		g_rageScriptPathRoot[MAX_PATH];

//-----------------------------------------------------------------------------

BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	g_hDllInstance = hinstDLL;

#if ( MAX_RELEASE < 9000 ) 
	sprintf_s(g_description,"RageMaxScriptExt built with rage release %d on %s at %s",RAGE_RELEASE,__DATE__,__TIME__);

	if (!g_controlsInit) 
	{
		g_controlsInit = TRUE;
		InitCustomControls(g_hDllInstance);
		InitCommonControls();

		// Initialize RAGE
		char* argv[1];
		argv[0] = "rageMaxScriptExt.dlx";
		sysParam::Init( 1, argv );

		INIT_PARSER;
	}
#endif

	return (TRUE);
}

//-----------------------------------------------------------------------------

RAGEMAXSCRIPT_API ClassDesc* LibClassDesc( int i )
{
	return ( NULL );
}

//-----------------------------------------------------------------------------

RAGEMAXSCRIPT_API int LibNumberClasses( )
{
	return ( 0 );
}

//-----------------------------------------------------------------------------

RAGEMAXSCRIPT_API const TCHAR* LibDescription()
{
	return g_description;
}


//-----------------------------------------------------------------------------

RAGEMAXSCRIPT_API ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

//-----------------------------------------------------------------------------

#if ( MAX_RELEASE >= 9000 ) 

RAGEMAXSCRIPT_API int LibInitialize(void)
{
	//According to the SDK docs LibInitialize "should" only be called once but this isn't strictly enforced
	//so make sure we only initialize once
	if(!g_libInitialized)
	{
		sprintf_s(g_description,"RageMaxScriptExt built with rage release %d on %s at %s",RAGE_RELEASE,__DATE__,__TIME__);

		//Perform any custom plugin initialization here
		g_libInitialized = TRUE;

		// Initialize RAGE
		char* argv[1];
		argv[0] = "rageMaxScriptExt.dlx";
		sysParam::Init( 1, argv );

		INIT_PARSER;
	}
	return 1;
}


RAGEMAXSCRIPT_API int LibShutdown(void)
{
	SHUTDOWN_PARSER;
	return 1;
}

#endif //( MAX_RELEASE >= 9000 ) 

//-----------------------------------------------------------------------------

TCHAR *GetString(int id)
{
	static TCHAR buf[1024];

	if (g_hDllInstance)
		return LoadString(g_hDllInstance, id, buf, sizeof(buf)) ? buf : NULL;

	return NULL;
}

//-----------------------------------------------------------------------------

