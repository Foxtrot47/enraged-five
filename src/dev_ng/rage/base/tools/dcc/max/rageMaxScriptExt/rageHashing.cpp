// 
// rageHashing.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
//

// RAGE headers
#include "atl/map.h"
#include "string/stringhash.h"

// 3dsmax SDK headers
#include "maxscript/maxscript.h"
#include "maxscript/foundation/numbers.h"
#include "maxscript/foundation/arrays.h"
#include "maxscript/foundation/strings.h"
#include "maxscript/foundation/name.h"
#include "maxscript/compiler/parser.h"
#include "maxscript/macros/define_instantiation_functions.h" // LPXO: define me last
#include "maxscript/macros/local_implementations.h" 

def_visible_primitive( atStringHash, "atStringHash" );
def_visible_primitive( atHash16, "atHash16" );
def_visible_primitive( atHash16U, "atHash16U" );

Value* 
atStringHash_cf( Value** arg_list, int count )
{
	// atStringHash "string" [seed:13]
	check_arg_count_with_keys(atStringHash, 1, count);
	type_check( arg_list[0], String, "atStringHash <string>" );

	Value* seed = key_arg_or_default(seed, &undefined);
	if (seed == &undefined)
	{
		seed = Integer64::intern(0);
	}
	type_check(seed, Integer64, "atStringHashseed:");

	unsigned int hashVal = rage::atStringHash(arg_list[0]->to_string(), (unsigned int)seed->to_int64());
	return_protected( Integer64::intern( hashVal ) );
}

Value* 
atHash16_cf( Value** arg_list, int count )
{
	check_arg_count( atHash16, 1, count );
	type_check( arg_list[0], String, "atHash16 <string>" );
	short hashVal = rage::atHash16(arg_list[0]->to_string());
	return_protected( Integer64::intern( hashVal ) );
}

Value*
atHash16U_cf( Value** arg_list, int count )
{
	check_arg_count( atHash16U, 1, count );
	type_check( arg_list[0], String, "atHash16U <string>" );
	unsigned short hashVal = rage::atHash16U( arg_list[0]->to_string() );
	return_protected( Integer64::intern( hashVal ) );
}
