// 
// /maxDosCmd.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#include "string/string.h"

#include "maxDosCmd.h"

using namespace rage;

//-----------------------------------------------------------------------------

RageMaxDosCmd::RageMaxDosCmd()
	: m_hStdInWrite(NULL)
	, m_hStdOutRead(NULL)	
	, m_hChildProcess(NULL)
	, m_hThread(NULL)
	, m_hEvtStop(NULL)
	, m_dwThreadId(0)
	, m_dwWaitTime(100)
	, m_dwProcessExitCode(0)
{
}

//-----------------------------------------------------------------------------

RageMaxDosCmd::~RageMaxDosCmd()
{
	Close();
}

//-----------------------------------------------------------------------------

BOOL RageMaxDosCmd::Open(LPCSTR pszCommandLine, LPCSTR pszStartPath)
{
	HANDLE hStdoutReadTmp;				// parent stdout read handle
	HANDLE hStdoutWrite, hStderrWrite;	// child stdout write handle
	HANDLE hStdinWriteTmp;				// parent stdin write handle
	HANDLE hStdinRead;					// child stdin read handle
	SECURITY_ATTRIBUTES sa;

	Close();
	hStdoutReadTmp = NULL;
	hStdoutWrite = hStderrWrite = NULL;
	hStdinWriteTmp = NULL;
	hStdinRead = NULL;

	// Set up the security attributes struct.
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	BOOL bOK = FALSE;
	__try
	{
		// Create a child stdout pipe.
		if (!::CreatePipe(&hStdoutReadTmp, &hStdoutWrite, &sa, 0))
			__leave;

		// Create a duplicate of the stdout write handle for the std
		// error write handle. This is necessary in case the child
		// application closes one of its std output handles.
		if (!::DuplicateHandle(
			::GetCurrentProcess(),
			hStdoutWrite,
			::GetCurrentProcess(),
			&hStderrWrite,
			0, TRUE,
			DUPLICATE_SAME_ACCESS))
			__leave;

		// Create a child stdin pipe.
		if (!::CreatePipe(&hStdinRead, &hStdinWriteTmp, &sa, 0))
			__leave;

		// Create new stdout read handle and the stdin write handle.
		// Set the inheritance properties to FALSE. Otherwise, the child
		// inherits the these handles; resulting in non-closeable
		// handles to the pipes being created.
		if (!::DuplicateHandle(
			::GetCurrentProcess(),
			hStdoutReadTmp,
			::GetCurrentProcess(),
			&m_hStdOutRead,
			0, FALSE,			// make it uninheritable.
			DUPLICATE_SAME_ACCESS))
			__leave;

		if (!::DuplicateHandle(
			::GetCurrentProcess(),
			hStdinWriteTmp,
			::GetCurrentProcess(),
			&m_hStdInWrite,
			0, FALSE,			// make it uninheritable.
			DUPLICATE_SAME_ACCESS))
			__leave;

		// Close inheritable copies of the handles we do not want to
		// be inherited.
		DestroyHandle(hStdoutReadTmp);
		DestroyHandle(hStdinWriteTmp);

		// launch the child process
		if (!LaunchChild(pszCommandLine, pszStartPath,
			hStdoutWrite, hStdinRead, hStderrWrite))
			__leave;

		// Child is launched. Close the parents copy of those pipe
		// handles that only the child should have open.
		// Make sure that no handles to the write end of the stdout pipe
		// are maintained in this process or else the pipe will not
		// close when the child process exits and ReadFile will hang.
		DestroyHandle(hStdoutWrite);
		DestroyHandle(hStdinRead);
		DestroyHandle(hStderrWrite);

		// Launch a thread to receive output from the child process.
		m_hEvtStop = ::CreateEvent(NULL, TRUE, FALSE, NULL);
		m_hThread = ::CreateThread(
			NULL, 0,
			OutputThread,
			this,
			0,
			&m_dwThreadId);
		if (!m_hThread)
			__leave;

		bOK = TRUE;
	}

	__finally
	{
		if (!bOK)
		{
			DWORD dwOsErr = ::GetLastError();
			char szMsg[40];
			::sprintf(szMsg, "Redirect console error: %x\r\n", dwOsErr);
			WriteStdError(szMsg, (DWORD)strlen(szMsg));
			DestroyHandle(hStdoutReadTmp);
			DestroyHandle(hStdoutWrite);
			DestroyHandle(hStderrWrite);
			DestroyHandle(hStdinWriteTmp);
			DestroyHandle(hStdinRead);
			Close();
			::SetLastError(dwOsErr);
		}
	}

	return bOK;
}

//-----------------------------------------------------------------------------

void RageMaxDosCmd::Close()
{
	if (m_hThread != NULL)
	{
		// this function might be called from redir thread
		if (::GetCurrentThreadId() != m_dwThreadId)
		{
			Assert(m_hEvtStop != NULL);
			::SetEvent(m_hEvtStop);
			if (::WaitForSingleObject(m_hThread, 5000) == WAIT_TIMEOUT)
			{
				const char* msg = "The redir thread is dead\r\n";
				WriteStdError(msg, (DWORD)strlen(msg));
				::TerminateThread(m_hThread, -2);
			}
		}

		DestroyHandle(m_hThread);
	}

	DestroyHandle(m_hEvtStop);
	DestroyHandle(m_hChildProcess);
	DestroyHandle(m_hStdInWrite);
	DestroyHandle(m_hStdOutRead);
	m_dwThreadId = 0;
}

//-----------------------------------------------------------------------------

BOOL RageMaxDosCmd::Printf(LPCTSTR pszFormat, ...)
{
	char buf[2048];

	if (!m_hStdInWrite)
		return FALSE;

	va_list argList;
	va_start(argList, pszFormat);
	vformatf(buf,sizeof(buf),pszFormat,argList);
	va_end(argList);

	DWORD dwWritten;
	return ::WriteFile(m_hStdInWrite, (LPCTSTR)buf,
		(DWORD)strlen(buf), &dwWritten, NULL);
}

//-----------------------------------------------------------------------------

BOOL RageMaxDosCmd::WaitForProcessToTerminate(DWORD dwTimwout)
{
    if(WaitForSingleObject(m_hThread, dwTimwout) == WAIT_TIMEOUT)
		return FALSE;
	else
		return TRUE;
}

//-----------------------------------------------------------------------------

BOOL RageMaxDosCmd::LaunchChild(LPCTSTR pszCmdLine,
								LPCTSTR pszStartPath,
								HANDLE hStdOut,
								HANDLE hStdIn,
								HANDLE hStdErr)
{
	PROCESS_INFORMATION pi;
	STARTUPINFO si;

	Assert(m_hChildProcess == NULL);

	// Set up the start up info struct.
	::ZeroMemory(&si, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.hStdOutput = hStdOut;
	si.hStdInput = hStdIn;
	si.hStdError = hStdErr;
	si.wShowWindow = SW_HIDE;
	si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;

	// Note that dwFlags must include STARTF_USESHOWWINDOW if we
	// use the wShowWindow flags. This also assumes that the
	// CreateProcess() call will use CREATE_NEW_CONSOLE.

	// Launch the child process.
	if (!::CreateProcess(
		NULL,
		(LPTSTR)pszCmdLine,
		NULL, NULL,
		TRUE,
		CREATE_NEW_CONSOLE,
		NULL, 
		pszStartPath,
		&si,
		&pi))
		return FALSE;

	m_hChildProcess = pi.hProcess;
	// Close any unuseful handles
	::CloseHandle(pi.hThread);
	return TRUE;
}

//-----------------------------------------------------------------------------

// redirect the child process's stdout:
// return: 1: no more data, 0: child terminated, -1: os error
int RageMaxDosCmd::RedirectStdout()
{
	Assert(m_hStdOutRead != NULL);
	for (;;)
	{
		DWORD dwAvail = 0;
		if (!::PeekNamedPipe(m_hStdOutRead, NULL, 0, NULL,
			&dwAvail, NULL))			// error
			break;

		if (!dwAvail)					// not data available
			return 1;

		char szOutput[256];
		DWORD dwRead = 0;
		if (!::ReadFile(m_hStdOutRead, szOutput, min(255, dwAvail),
			&dwRead, NULL) || !dwRead)	// error, the child might ended
			break;

		szOutput[dwRead] = 0;
		WriteStdOut(szOutput, dwRead);
	}

	DWORD dwError = ::GetLastError();
	if (dwError == ERROR_BROKEN_PIPE ||	// pipe has been ended
		dwError == ERROR_NO_DATA)		// pipe closing in progress
	{
		return 0;	// child process ended
	}

	const char* msg = "Read stdout pipe error\r\n";
	WriteStdError(msg, (DWORD)strlen(msg));
	return -1;		// os error
}

//-----------------------------------------------------------------------------

void RageMaxDosCmd::DestroyHandle(HANDLE& rhObject)
{
	if (rhObject != NULL)
	{
		::CloseHandle(rhObject);
		rhObject = NULL;
	}
}

//-----------------------------------------------------------------------------

// thread to receive output of the child process
DWORD WINAPI RageMaxDosCmd::OutputThread(LPVOID lpvThreadParam)
{
	HANDLE aHandles[2];
	int nRet;
	RageMaxDosCmd* pRedir = (RageMaxDosCmd*) lpvThreadParam;

	Assert(pRedir != NULL);
	aHandles[0] = pRedir->m_hChildProcess;
	aHandles[1] = pRedir->m_hEvtStop;

	for (;;)
	{
		// redirect stdout till there's no more data.
		nRet = pRedir->RedirectStdout();
		if (nRet <= 0)
			break;

		// check if the child process has terminated.
		DWORD dwRc = ::WaitForMultipleObjects(
			2, aHandles, FALSE, pRedir->m_dwWaitTime);
		if (WAIT_OBJECT_0 == dwRc)		// the child process ended
		{
			nRet = pRedir->RedirectStdout();
			if (nRet > 0)
				nRet = 0;

			GetExitCodeProcess(pRedir->m_hChildProcess,&pRedir->m_dwProcessExitCode);
			break;
		}
		if (WAIT_OBJECT_0+1 == dwRc)	// m_hEvtStop was signalled
		{
			nRet = 1;	// cancelled
			break;
		}
	}

	// close handles
	pRedir->Close();
	return nRet;
}

//-----------------------------------------------------------------------------

RageMaxDosCmdStringRedirect::RageMaxDosCmdStringRedirect()
{
	m_strBuffer.reserve(8096);
}
	
void RageMaxDosCmdStringRedirect::WriteStdOut(LPCSTR pszOutput, DWORD dwCount)
{
	m_strBuffer.append(pszOutput);
}

void RageMaxDosCmdStringRedirect::WriteStdError(LPCSTR pszError, DWORD dwCount)
{
	m_strBuffer.append(pszError);
}

//-----------------------------------------------------------------------------

RageMaxDosCmdFileRedirect::RageMaxDosCmdFileRedirect()
	: m_outFile(INVALID_HANDLE_VALUE )
{
}
	
void RageMaxDosCmdFileRedirect::WriteStdOut(LPCSTR pszOutput, DWORD dwCount)
{
	if(m_outFile != INVALID_HANDLE_VALUE)
	{
		DWORD dwBytesWritten = 0;
		WriteFile(m_outFile, pszOutput, dwCount, &dwBytesWritten, NULL);
		Assert(dwBytesWritten == dwCount);
	}
}

void RageMaxDosCmdFileRedirect::WriteStdError(LPCSTR pszError, DWORD dwCount)
{
	if(m_outFile != INVALID_HANDLE_VALUE)
	{
		DWORD dwBytesWritten = 0;
		WriteFile(m_outFile, pszError, dwCount, &dwBytesWritten, NULL);
		Assert(dwBytesWritten == dwCount);
	}
}

//-----------------------------------------------------------------------------

