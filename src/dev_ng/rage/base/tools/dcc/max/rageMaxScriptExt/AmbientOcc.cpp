// 
// /AmbientOcc.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#include <maxscript/maxscript.h>
#include <maxscript/foundation/numbers.h>
#include <maxscript/foundation/arrays.h>
#include <maxscript/foundation/name.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last

#include "math.h"

#include <list>

#define EPSILON 0.001f

//-----------------------------------------------------------------------------

def_visible_primitive(RageCalcAmbientOcc,"RageCalcAmbientOcc");

//-----------------------------------------------------------------------------

struct VertInfo
{
	std::list<int> m_FaceList;
};

struct FaceInfo
{
	Point3 m_Center;
};

//-----------------------------------------------------------------------------

TriObject* GetTriObject(INode* node,bool& deleteAfter)
{
	Interface* ip = GetCOREInterface();
	deleteAfter = false;
	Object* obj = node->EvalWorldState(ip->GetTime()).obj;

	if(obj->ClassID() == Class_ID(EDITTRIOBJ_CLASS_ID,0))
	{
		return (TriObject*)obj;
	}
	else
	{
		if (obj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
		{
			TriObject* tri = (TriObject*) obj->ConvertToType(ip->GetTime(),Class_ID(TRIOBJ_CLASS_ID, 0));

			if(tri == NULL) return NULL;
			if(obj != tri) deleteAfter = true;

			return tri;
		}
	}

	return NULL;
}

//-----------------------------------------------------------------------------

Mesh* GetMesh(Object *pObject)
{
	TriObject *tri = NULL;
	Class_ID classID;

	classID = pObject->ClassID();

	if(classID == (Class_ID(EDITTRIOBJ_CLASS_ID, 0))) 
	{ 
		tri = (TriObject *) pObject;
	}

	if(!tri)
	{
		return NULL;
	}

	Mesh& mesh = tri->GetMesh();

	return &mesh;
}

//-----------------------------------------------------------------------------

bool intersect_triangle(Point3 orig,Point3 dir,Point3 vert0,Point3 vert1,Point3 vert2,float& r_dist)
{
	if((orig == vert0) || (orig == vert1) || (orig == vert2))
		return false;

	Point3 e1,e2,h,s,q,n;
	float a,f,u,d,v,t,denom;	
	bool cull_backfaces = true;
	bool extend_to_infinity = true;

	e1 = vert2 - vert0;
	e2 = vert1 - vert0;

	h = CrossProd(dir,e2);
	a = DotProd(e1,h);

	if((cull_backfaces) && (a >= 0.0f)) return false;
	if((a > -EPSILON) && (a < EPSILON)) return false;

	f = 1.0f / a;
	s = orig - vert0;
	u = f * DotProd(s,h);

	if((u < 0.0f) || (u > 1.0f)) return false;

	q = CrossProd(s,e1);
	v = f * DotProd(dir,q);

	if((v < 0.0f) || ((u + v) > 1.0f)) return false;

	t = f * DotProd(e2,q);

	if(t < 0.0f) return false;

	if((extend_to_infinity == false) && (t > 1.0f)) return false;

	n = CrossProd(e1,e2);
	d = -DotProd(n,vert0);
	denom = DotProd(n,dir);
	r_dist = -(d + DotProd(n,orig)) / denom;

	return true;
}

//-----------------------------------------------------------------------------

float get_angle(Point3 v1,Point3 v2)
{
	float arg;
	float ptot2 = LengthSquared(v1) * LengthSquared(v2);

	if(ptot2 <= 0.0f) 
	{
		arg = 0.0f;
	}
	else
	{
		arg = DotProd(v1,v2) / sqrt(ptot2);
		if(arg >  1.0f) arg =  1.0f;
		if(arg < -1.0f) arg = -1.0f;
	}

	return acos( arg ); 
}

//-----------------------------------------------------------------------------

Value *RageCalcAmbientOcc_cf(Value** arg_list, int count)
{
	INode* p_Target = arg_list[0]->to_node();
	Array* p_Array = (Array*)arg_list[1];
	float Falloff = arg_list[2]->to_float();
	float Ratio = arg_list[3]->to_float();
	bool triToDelete;
	TriObject* p_TriObjectTo = GetTriObject(p_Target,triToDelete);
	Mesh* p_meshTo = GetMesh(p_TriObjectTo);
	Point3* p_FaceNormals = new Point3[p_meshTo->numFaces];
	Point3* p_VertNormals = new Point3[p_meshTo->numVerts];
	VertInfo* p_VertInfo = new VertInfo[p_meshTo->numVerts];
	FaceInfo* p_FaceInfo = new FaceInfo[p_meshTo->numFaces];

	Matrix3 matA = p_Target->GetObjectTM(0);

	int i,k,l,vars,varCount,Count = p_Array->size;

	varCount = 1;

	Point3* pVertsTo = new Point3[p_meshTo->numVerts];
	memcpy(pVertsTo,p_meshTo->verts,sizeof(Point3) * p_meshTo->numVerts); 
	matA.TransformPoints(pVertsTo,p_meshTo->numVerts);

	p_meshTo->setNumVertCol(p_meshTo->numVerts);
	p_meshTo->setNumVCFaces(p_meshTo->numFaces);

	memset(p_VertNormals,0,sizeof(Point3) * p_meshTo->numVerts);

	for(i=0;i<p_meshTo->numFaces;i++)
	{
		Face* pFace = &p_meshTo->faces[i];

		p_meshTo->vcFace[i].t[0] = pFace->v[0];
		p_meshTo->vcFace[i].t[1] = pFace->v[1];
		p_meshTo->vcFace[i].t[2] = pFace->v[2];

		p_FaceInfo[i].m_Center = pVertsTo[pFace->v[0]];
		p_FaceInfo[i].m_Center += pVertsTo[pFace->v[1]];
		p_FaceInfo[i].m_Center += pVertsTo[pFace->v[2]];
		p_FaceInfo[i].m_Center /= 3.0f;

		p_FaceNormals[i] = CrossProd(pVertsTo[pFace->v[1]] - pVertsTo[pFace->v[0]], pVertsTo[pFace->v[2]] - pVertsTo[pFace->v[0]]);
		p_FaceNormals[i] = p_FaceNormals[i].Normalize();

		p_VertInfo[pFace->v[0]].m_FaceList.push_back(i);
		p_VertInfo[pFace->v[1]].m_FaceList.push_back(i);
		p_VertInfo[pFace->v[2]].m_FaceList.push_back(i);

		p_VertNormals[pFace->v[0]] += p_FaceNormals[i];
		p_VertNormals[pFace->v[1]] += p_FaceNormals[i];
		p_VertNormals[pFace->v[2]] += p_FaceNormals[i];
	}

	for(i=0;i<p_meshTo->numVerts;i++)
	{
		//face angle part

		std::list<int>::iterator begin = p_VertInfo[i].m_FaceList.begin();
		std::list<int>::iterator end = p_VertInfo[i].m_FaceList.end();
		std::list<int>::iterator prev;

		prev = begin;
		begin++;

		float Angle = PI;

		int FaceCount = 0;

		while(begin != end)
		{
			float DistanceAfter = Length((p_FaceInfo[*prev].m_Center + p_FaceNormals[*prev]) - (p_FaceInfo[*begin].m_Center + p_FaceNormals[*begin]));
			float DistanceCentre = Length(p_FaceInfo[*prev].m_Center - p_FaceInfo[*begin].m_Center);

			float NewAngle = get_angle(p_FaceNormals[*prev],p_FaceNormals[*begin]);

			if(DistanceAfter < DistanceCentre)
				NewAngle = PI - NewAngle;
			else
				NewAngle = PI;

			if(NewAngle < Angle)
				Angle = NewAngle;

			FaceCount++;
			begin++;
		}

		Angle = Angle / PI;

		//collision detection part

		p_VertNormals[i] = Normalize(p_VertNormals[i]);

		float ValueOut = 0.0f;

		std::list<int>::iterator check = p_VertInfo[i].m_FaceList.begin();
	
		varCount = FaceCount + 1;

		for(vars=0;vars<varCount;vars++)
		{
			Point3 NormalCheck;

			if(vars == 0)
				NormalCheck = p_VertNormals[i];
			else
			{				
				NormalCheck = p_FaceNormals[*check];
				check++;
			}

			float Distance = Falloff;	

			for(k=0;k<Count;k++)
			{
				INode* p_nodeCheck = (*p_Array)[k]->to_node();
				bool triCheckDelete;
				TriObject* p_TriObjectCheck = GetTriObject(p_nodeCheck,triCheckDelete);
				Mesh* p_meshCheck = GetMesh(p_TriObjectCheck);
				Matrix3 matCheck = p_nodeCheck->GetObjectTM(0);

				Point3* pVertsCheck = new Point3[p_meshCheck->numVerts];
				memcpy(pVertsCheck,p_meshCheck->verts,sizeof(Point3) * p_meshCheck->numVerts); 
				matCheck.TransformPoints(pVertsCheck,p_meshCheck->numVerts);

				for(l=0;l<p_meshCheck->numFaces;l++)
				{
					Face* pFaceCheck = &p_meshCheck->faces[l];
					float FoundDistance;

					if(intersect_triangle(	pVertsTo[i],
											NormalCheck,
											pVertsCheck[pFaceCheck->v[0]],
											pVertsCheck[pFaceCheck->v[1]],
											pVertsCheck[pFaceCheck->v[2]],
											FoundDistance))
					{
						if(FoundDistance < Distance)
							Distance = FoundDistance;
					}
				}

				if(triCheckDelete)
					delete p_TriObjectCheck;

				delete[] pVertsCheck;
			}

			Distance = min(Distance,Falloff);
			ValueOut += (Distance / Falloff);
		}

		ValueOut /= varCount;
		ValueOut = ValueOut * ValueOut;

		//write vertex colour out

		if(Angle < ValueOut)
			ValueOut = Angle;

		p_meshTo->vertCol[i] = VertColor(ValueOut,ValueOut,ValueOut);
	}

	if(triToDelete)
		delete p_TriObjectTo;

	delete[] p_FaceInfo;
	delete[] p_VertInfo;
	delete[] pVertsTo;
	delete[] p_VertNormals;
	delete[] p_FaceNormals;

	return &true_value;
}

//-----------------------------------------------------------------------------


