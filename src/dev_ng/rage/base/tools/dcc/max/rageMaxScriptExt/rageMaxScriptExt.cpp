// 
// /rageMaxScriptExt.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
//
#include "maxscript/maxscript.h"
#include "maxscript/foundation/numbers.h"
#include "maxscript/foundation/arrays.h"
#include "maxscript/foundation/strings.h"
#include "maxscript/foundation/name.h"
#include "maxscript/compiler/parser.h"
#include "maxscript/macros/define_instantiation_functions.h" // LPXO: define me last
#include "maxscript/macros/local_implementations.h" 

#include "atl/string.h"

#if 1
#include "fragment/type.h"
#include "fragment/manager.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "cranimation/animation.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "grcore/texturedefault.h"
#include "phcore/materialmgrimpl.h"
#endif

#include "maxDosCmd.h"

using namespace rage;
using namespace std;

extern char g_rageScriptPathRoot[];

def_visible_primitive(RageGetEnvVar, "RageGetEnvVar");

def_visible_primitive(RageDOSCommand, "RageDOSCommand");
def_visible_primitive(RageDOSCommandRedirectToString, "RageDOSCommandRedirectToString");
def_visible_primitive(RageDOSCommandRedirectToFile, "RageDOSCommandRedirectToFile");

def_visible_primitive(RageFileIn, "RageFileIn");

#if 0
def_visible_primitive(RageCreateTuneFile, "RageCreateTuneFile");

static fragManager* gp_FragMgr = NULL; // only needs to be initialised once. Constructor sets global instance. (Pure elegance?)
static phLevelNew*	ms_pLevelNew = NULL;
static phSimulator*	ms_pPhysSim = NULL;

//-----------------------------------------------------------------------------

Value* RageCreateTuneFile_cf(Value **arg_list, int count)
{
	check_arg_count(RageCreateTuneFile, 2, count);
	type_check(  arg_list[0], String, "RageCreateTuneFile <entity.type file path> <save file path>");
	type_check(  arg_list[1], String, "RageCreateTuneFile <entity.type file path> <save file path>");

	const char* typeFile = arg_list[0]->to_string();
	const char* filename = arg_list[1]->to_string();

	Value *retVal = &false_value;

#pragma region fromRageBuilder
	//////////////////////////////////////////////////////////////////////////
	// from ragebuilder
	//////////////////////////////////////////////////////////////////////////

	char currentDirectory[256];
	// 	if(GetCurrentDirectory(256, &currentDirectory[0]))
	ASSET.SetPath("X:/temp/");//&currentDirectory[0]);
	//	crAnimation::InitClass();

	grcDevice::InitSingleton();

	//////////////////////////////////////////////////////////////////////////
	// out of rageplatform::init()
	// initialise factories
	grcTextureFactoryDefault::CreatePagedTextureFactory();
	grmModelFactory::CreateStandardModelFactory();
	grmShaderFactory::CreateStandardShaderFactory();
	grmModelFactory::GetVertexConfigurator()->SetPackNormals(false);
	const char *ms_shaderpath = "X:/gta5/build/dev_ng/common/shaders/";
	const char *ms_shaderdbpath = "X:/gta5/build/dev_ng/common/shaders/db/";
	grmShaderFactory::GetInstance().PreloadShaders(ms_shaderpath);
	grcMaterialLibrary::SetCurrent(grcMaterialLibrary::Preload(ms_shaderdbpath));
	//////////////////////////////////////////////////////////////////////////

	// We have to enable ref counting before creating the frag manager, because we are going to turn it on later,
	// then we get a crash during shutdown.
	phConfig::EnableRefCounting();

	// create a physics level

	ms_pLevelNew = new phLevelNew;
	ms_pLevelNew->SetActiveInstance();
	ms_pLevelNew->SetExtents(Vec3V(-10000.0f, -10000.0f, -3000.0f), Vec3V(10000.0f, 10000.0f, 3000.0f));
	ms_pLevelNew->SetMaxObjects(64);
	ms_pLevelNew->SetMaxActive(64);
	ms_pLevelNew->SetNumOctreeNodes(128);
	ms_pLevelNew->Init();

	ms_pPhysSim = new phSimulator;
	ms_pPhysSim->SetActiveInstance();
//	ms_pPhysSim->SetMaxInstBehaviors(1);
	phSimulator::InitParams params;
	params.maxManagedColliders = 64;
	params.scratchpadSize = 1024 * 1024;
	params.maxManifolds = 64; 
	params.maxCompositeManifolds = params.maxManifolds / 3;
	params.maxContacts = params.maxManifolds * 4;
	params.maxExternVelManifolds = 64;
	params.maxInstBehaviors = (u16)1;
// 	const int maxNumConstraints = 32 + numManagedConstraints;
// 	params.maxConstraints = maxNumConstraints;
	ms_pPhysSim->Init(ms_pLevelNew, params);
	ms_pPhysSim->GetConstraintMgr()->CreateConstraints(32);

	phSimulator::InitClass();

	// initialise material manager

	//	GtaMaterialMgr::Create();

	phMaterialMgrImpl<phMaterial>::Create();

	if(!gp_FragMgr)
	{
		gp_FragMgr = new fragManager();
		gp_FragMgr->AddCachePool(1024, 1024);
	}

	crAnimation::InitClass();

	grmModelFactory::CreateStandardModelFactory();
	grmShaderFactory::CreateStandardShaderFactory();

	//////////////////////////////////////////////////////////////////////////
	// END from ragebuilder
	//////////////////////////////////////////////////////////////////////////
#pragma endregion

	fragType* type = fragType::Load(typeFile,NULL,new fragType);

	ASSET.CreateLeadingPath( filename );
	if ( fiStream* S = ASSET.Create( filename, "tune" ) )
	{
		fiAsciiTokenizer T;
		T.Init( filename, S );

		type->SaveASCII( T );

		S->Close();

		retVal = &true_value;
	}

	if(gp_FragMgr)
	{
		delete gp_FragMgr;
		gp_FragMgr = NULL;
	}

	if(ms_pPhysSim)
	{
		delete ms_pPhysSim;
		ms_pPhysSim = NULL;
	}

	if(ms_pLevelNew)
	{
		ms_pLevelNew->Shutdown();
		delete ms_pLevelNew;
		ms_pLevelNew = NULL;
	}


	return retVal;
}
#endif


//-----------------------------------------------------------------------------

Value* RageGetEnvVar_cf(Value **arg_list, int count)
{
	check_arg_count(RageGetEnvVar, 1, count);
	type_check( arg_list[0], String, "Expected string argument for the environment variale to retrieve.");

	Value* pStrEnvVar = arg_list[0];
	const char* envVarName = pStrEnvVar->to_string();

	const char* envVarValue = getenv(envVarName);
	if(!envVarValue)
	{
		return &undefined;
	}
	else
	{
		return_protected(new String((TCHAR*)envVarValue));
	}
}

//-----------------------------------------------------------------------------

Value* RageDOSCommand_cf(Value **arg_list, int count)
{
	check_arg_count_with_keys(RageDOSCommand, 1, count);
	TCHAR* params = arg_list[0]->to_string();

	Value* key;

	// Look for a starting path to launch the command from
	Value* def_name(startpath);
	key = key_arg(startpath);
	TCHAR* startpath = (key == &unsupplied) ? 0 : key->to_string();

	// Look for a wait-for-completion override 
	const bool DEFAULT_WAIT_FOR_COMPLETION = false;
	Value* def_name(donotwait);
	key = key_arg(donotwait);
	BOOL donotwait = (key == &unsupplied) ? DEFAULT_WAIT_FOR_COMPLETION : key->to_bool();

	// Look for the redirect output override
	const bool DEFAULT_REDIRECT_OUTPUT = false;
	Value* def_name(redirect);
	key = key_arg(redirect);
	BOOL redirect = (key == &unsupplied) ? DEFAULT_REDIRECT_OUTPUT : key->to_bool();

	// Look for a prompt string
	Value* def_name(prompt);
	key = key_arg(prompt);
	TCHAR* prompt = (key == &unsupplied) ? 0 : key->to_string();

	PROCESS_INFORMATION pi;
	STARTUPINFO si;

	ZeroMemory( &pi, sizeof(pi) );
	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);

	//The user would like to redirect the output to a temporary file
	char cmdTempFilePath[MAX_PATH];
	if( redirect )
	{
		char tempPath[MAX_PATH];
		GetTempPath(MAX_PATH, tempPath);
		sprintf_s(cmdTempFilePath,"%sRageDOSCommand.txt", tempPath);
	}

	GetCOREInterface()->PushPrompt( prompt ? prompt : params ) ;

	// You may want to adjust the arguments passed to cmd here,
	// type 'cmd /?' from the console for additional documentation
	TSTR args("cmd /e:on /d /c ");

	// Make sure we have a quoted command string
	if ( (params[0] != '\"') || (params[_tcslen(params)-1] != '\"') )
	{
		args.append("\"");
		args.append(params);
		
		if(redirect)
		{
			args.append(" > ");
			args.append(cmdTempFilePath);
		}

		args.append("\"");
	}
	else
	{
		args.append(params);
	}

	BOOL ret = CreateProcess(
		NULL,					
		args.data(),		 
		NULL,					 
		NULL,					
		false,					
		CREATE_NO_WINDOW,		
		NULL,					
		startpath,
		&si,					
		&pi);					

	DWORD ExitCode = 0;

	if (ret && !donotwait)
	{
		ret = (WAIT_OBJECT_0 == WaitForSingleObject( pi.hProcess, INFINITE ));
		GetExitCodeProcess(pi.hProcess,&ExitCode);
	}

	GetCOREInterface()->PopPrompt();

	// Close process and thread handles. 
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );

	if (ret) 
	{
		return_protected(new Integer(ExitCode));
	}
	else 
	{
		mprintf("ERROR: CreateProcess(%s) failed!\n", args.data());
		return_protected(new Integer(0));
	}
}

//-----------------------------------------------------------------------------

Value* RageDOSCommandRedirectToString_cf(Value **arg_list, int count)
{
	check_arg_count_with_keys(RageDOSCommand, 1, count);
	TCHAR* params = arg_list[0]->to_string();

	Value* key;

	// Look for a starting path to launch the command from
	Value* def_name(startpath);
	key = key_arg(startpath);
	TCHAR* startpath = (key == &unsupplied) ? 0 : key->to_string();

	// Look for a prompt string
	Value* def_name(prompt);
	key = key_arg(prompt);
	TCHAR* prompt = (key == &unsupplied) ? 0 : key->to_string();

	GetCOREInterface()->PushPrompt( prompt ? prompt : params ) ;

	PROCESS_INFORMATION pi;
	STARTUPINFO si;

	ZeroMemory( &pi, sizeof(pi) );
	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);

	// You may want to adjust the arguments passed to cmd here,
	// type 'cmd /?' from the console for additional documentation
	TSTR args("cmd /e:on /d /c ");

	// Make sure we have a quoted command string
	if ( (params[0] != '\"') || (params[_tcslen(params)-1] != '\"') )
	{
		args.append("\"");
		args.append(params);
		args.append("\"");
	}
	else
	{
		args.append(params);
	}

	GetCOREInterface()->PopPrompt();

	RageMaxDosCmdStringRedirect rmdc;
	rmdc.Open((LPCSTR)args.data(), (LPCSTR)startpath);

	if(rmdc.WaitForProcessToTerminate())
	{
		//Return an array where the first element is the exit code of the child process
		//and the second element is a string containing the output of the child process
		DWORD exitCode;
		exitCode = rmdc.GetProcessExitCode();

		one_typed_value_local(Array* rArray);
		vl.rArray = new Array(2);

		vl.rArray->append( new Integer(exitCode) );
		vl.rArray->append( new String((TCHAR*)rmdc.GetResultBuffer()) );
		
		return_value(vl.rArray);
	}
	else
	{
		//Something abnormal happened...
		return &undefined;
	}
}

//-----------------------------------------------------------------------------

Value* RageDOSCommandRedirectToFile_cf(Value **arg_list, int count)
{
	check_arg_count_with_keys(RageDOSCommand, 2, count);

	TCHAR* params = arg_list[0]->to_string();
	TCHAR* outFilePath = arg_list[1]->to_string();

	Value* key;

	// Look for a starting path to launch the command from
	Value* def_name(startpath);
	key = key_arg(startpath);
	TCHAR* startpath = (key == &unsupplied) ? 0 : key->to_string();

	// Look for a prompt string
	Value* def_name(prompt);
	key = key_arg(prompt);
	TCHAR* prompt = (key == &unsupplied) ? 0 : key->to_string();

	GetCOREInterface()->PushPrompt( prompt ? prompt : params ) ;

	PROCESS_INFORMATION pi;
	STARTUPINFO si;

	ZeroMemory( &pi, sizeof(pi) );
	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);

	// You may want to adjust the arguments passed to cmd here,
	// type 'cmd /?' from the console for additional documentation
	TSTR args("cmd /e:on /d /c ");

	// Make sure we have a quoted command string
	if ( (params[0] != '\"') || (params[_tcslen(params)-1] != '\"') )
	{
		args.append("\"");
		args.append(params);
		args.append("\"");
	}
	else
	{
		args.append(params);
	}

	GetCOREInterface()->PopPrompt();

	//Create the output file
	HANDLE hRedirectFile = INVALID_HANDLE_VALUE;
	DWORD outFileAttr = GetFileAttributes((LPCSTR)outFilePath);
	if( (outFileAttr != INVALID_FILE_ATTRIBUTES) && (outFileAttr & FILE_ATTRIBUTE_READONLY) )
	{
		mprintf("Error : 'RageDOSCommandRedirectToFile' redirection file '%s' is read-only\n", outFilePath);
		return &undefined;
	}
	else
	{
		hRedirectFile = CreateFile((LPCSTR)outFilePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
		if(hRedirectFile == INVALID_HANDLE_VALUE)
		{
			DWORD errCode = GetLastError();
			mprintf("Error : 'RageDOSCommandRedirectToFile' an error (%d) occured while creating the redirection file '%s'\n", errCode, outFilePath);
			return &undefined;
		}
	}

	RageMaxDosCmdFileRedirect rmdc;

	rmdc.SetOutputFile(hRedirectFile);
	rmdc.Open((LPCSTR)args.data(), (LPCSTR)startpath);

	if(rmdc.WaitForProcessToTerminate())
	{
		//Return an array where the first element is the exit code of the child process
		//and the second element is a string containing the output of the child process
		DWORD exitCode;
		exitCode = rmdc.GetProcessExitCode();
		CloseHandle(hRedirectFile);
		return_protected ( new Integer(exitCode) );
	}
	else
	{
		//Something abnormal happened...
		CloseHandle(hRedirectFile);
		return &undefined;
	}
}

//-----------------------------------------------------------------------------

static Value* file_in(CharStream* source, int quiet)
{
	init_thread_locals();
	push_alloc_frame();
	three_typed_value_locals(Parser* parser, Value* code, Value* result);
	save_current_frames();
//	set_error_trace_back_active(FALSE);

	CharStream* out = thread_local(current_stdout);
	if (!quiet)
		source->log_to(out);

	// loop through stream compiling & evaluating all expressions
	try
	{
		// make a new compiler instance
		vl.parser = new Parser (out);
		source->flush_whitespace();
		while (!source->at_eos() || vl.parser->back_tracked)
		{
			vl.code = vl.parser->compile(source);
			vl.result = vl.code->eval();
			if (!quiet)
				vl.result->print();
			source->flush_whitespace();
		}
		if (vl.parser->expr_level != 0 || vl.parser->back_tracked && vl.parser->token != t_end)
			throw RuntimeError ("Unexpected end of file during fileIn");
		source->close();
	}
	catch (...)
	{
		// catch any errors and tell what file we are in if any
		out->puts("Error occurred during fileIn: ");
		source->sprin1(out);
		out->puts("\n");
		source->close();
		pop_alloc_frame();
		throw;
	}

	// if there was one, return last expression in stream as result
	if (vl.result == NULL)
		vl.result = &ok;
	else
		vl.result = vl.result->get_heap_ptr();
	pop_alloc_frame();
	return_value(vl.result);
}

//-----------------------------------------------------------------------------

Value* RageFileIn_cf(Value **arg_list, int count)
{
	check_arg_count_with_keys(RageFileIn_cf, 1, count);
	two_typed_value_locals(FileStream* file, Value* result);
	Value* quiet = key_arg_or_default(quiet, &true_value);

	type_check( quiet, Boolean, "RageFileIn quiet:");
	type_check( arg_list[0], String, "RageFileIn - Expected string argument for the relative script path");

	Value* pStrPathValue = arg_list[0];
	const char* szRelativeScriptPath = pStrPathValue->to_string();

	if(strlen(g_rageScriptPathRoot) == 0)
	{
		mprintf("Error : 'RageFileIn' the RAGE script path could not be determined.");
		return &false_value;
	}

	char szFullScriptPath[MAX_PATH];
	_snprintf_s(szFullScriptPath,MAX_PATH,"%s%s", g_rageScriptPathRoot, szRelativeScriptPath);

	DWORD fAttr = GetFileAttributes(szFullScriptPath);
	if(fAttr == INVALID_FILE_ATTRIBUTES)
		throw RuntimeError( "RageFileIn requested file does not exist : ", szFullScriptPath );

	vl.file = (new FileStream)->open( szFullScriptPath, "rt");
	if (vl.file == (FileStream*)&undefined)
		throw RuntimeError( "RageFileIn cannot open file : ",  szFullScriptPath );

	try
	{
		vl.result = file_in( vl.file, (quiet == &true_value) );
	}
	catch (...)
	{
		vl.file->close();
		throw;
	}

	return_value(vl.result);
}

//-----------------------------------------------------------------------------
