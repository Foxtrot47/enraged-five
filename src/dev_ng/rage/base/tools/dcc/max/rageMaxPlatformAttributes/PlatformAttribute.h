#ifndef PLATFORM_ATTRIBUTE_H_
#define PLATFORM_ATTRIBUTE_H_

#include <string>
#include <vector>

#include "rageDataStoreAttributes/attribute.h"

using namespace rage;

#define CUST_PLATFORM_ATTRIB_NAME "PlatformAttributes"
#define CUST_PLATFORM_ATTRIB_CLASS_ID Class_ID(0x6ae22068, 0x796770e3)

// Platform Attribute
class PlatformAttribute
{
public:
	PlatformAttribute(dmat::AttributeValue* value, s32 index, std::string platform) :
		m_attributeValue(value),
		m_attributeIndex(index),
		m_platformString(platform)
	{}

	~PlatformAttribute()
	{
		ClearAttributeValue();
	}

	void SetAttributeValue(dmat::AttributeValue* value) { m_attributeValue = value; }
	void SetAttributeIndex(s32 index)					{ m_attributeIndex = index; }
	void SetPlatform(std::string platform)				{ m_platformString = platform; }

	dmat::AttributeValue*	GetAttributeValue() { return m_attributeValue; }
	s32						GetAttributeIndex()	{ return m_attributeIndex; }
	std::string&			GetPlatform()		{ return m_platformString; }

	void Set(dmat::AttributeValue* value, s32 index, std::string platform)
	{
		ClearAttributeValue();

		m_attributeValue = value;
		m_attributeIndex = index;
		m_platformString = platform;
	}

	void ClearAttributeValue()
	{
		if (m_attributeValue)
			delete m_attributeValue;
		m_attributeValue = NULL;
	}

private:
	dmat::AttributeValue*	m_attributeValue;
	s32						m_attributeIndex;
	std::string				m_platformString;
};

typedef std::vector<PlatformAttribute> PlatformAttributeArray;
typedef std::vector<PlatformAttribute>::iterator PlatformAttributeIterator;

typedef std::map<std::string, std::vector<PlatformAttribute>*> PlatformAttributeMap;
typedef std::map<std::string, std::vector<PlatformAttribute>*>::iterator PlatformAttributeMapIterator;
typedef std::map<std::string, std::vector<PlatformAttribute>*>::const_iterator PlatformAttributeMapConstIterator;

// Platform Attribute Data
class PlatformAttributeData
{
public:
	PlatformAttributeData() {}

	void AddPlatformAttribute(PlatformAttribute* platformAttribute);
	PlatformAttribute* FindPlatformAttribute(s32 index, std::string platform);

	bool DeletePlatformAttributes(std::string platform);

	void GetPlatforms(std::vector<std::string>* platformList);
	std::vector<PlatformAttribute>* GetAttributes(std::string platform);

	bool ContainsAttributes() { return m_platformAttributesMap.size() > 0; }
	s32 GetNumAttributes();

	char* CreateRawData(s32 &size);

private:
	PlatformAttributeMap m_platformAttributesMap;
};

// Custom Attribute
class CustPlatformAttributeData : public CustAttrib
{
public:
	CustPlatformAttributeData() : m_invalid(false) {}

	PlatformAttributeData& GetData()	{ return m_data; }
	void SetInvalid()					{ m_invalid = true; }
	bool IsInvalid()					{ return m_invalid; }

#if (MAX_RELEASE >= 12000)
	const MCHAR* GetName() {return CUST_PLATFORM_ATTRIB_NAME;}
#endif

	// File operations
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);

	// from ReferenceMaker
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,	PartID& partID,  RefMessage message);

	SClass_ID SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
	Class_ID ClassID() {return CUST_PLATFORM_ATTRIB_CLASS_ID;}

private:
	PlatformAttributeData m_data;
	bool m_invalid;
};

class CustPlatformAttributeDataClassDesc : public ClassDesc2 {
public:
	int 			IsPublic() {return 1;}
	void *			Create(BOOL loading) {return new CustPlatformAttributeData;}
	const TCHAR *	ClassName() { return _T("PlatformAttributes"); }
	SClass_ID		SuperClassID() {return CUST_ATTRIB_CLASS_ID;}
	Class_ID 		ClassID() {return CUST_PLATFORM_ATTRIB_CLASS_ID;}
	const TCHAR* 	Category() {return _T("");}
	BOOL			NeedsToSave(){return TRUE;}
	const TCHAR*	InternalName() { return _T("PlatformAttributes"); }
	HINSTANCE		HInstance() { return hInstance; }
};

#endif