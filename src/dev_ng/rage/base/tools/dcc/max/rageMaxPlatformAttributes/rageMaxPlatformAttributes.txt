Project rageMaxPlatformAttributes
Template max_configurations.xml

OutputFileName rageMaxPlatformAttributes.dlu
ConfigurationType dll

Define USING_RAGE

EmbeddedLibAll comctl32.lib core.lib geom.lib gfx.lib mesh.lib maxutil.lib maxscrpt.lib paramblk2.lib menus.lib gup.lib

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\tools\dcc\libs
IncludePath $(RAGE_DIR)\base\tools\dcc\max
IncludePath $(RAGE_DIR)\base\tools\libs
IncludePath $(RAGE_DIR)\base\tools\libs\rageDataStoreAttributes

ForceInclude forceinclude.h

Files {
	Folder "Header Files" {
		3dsmaxsdk_preinclude.h
		forceinclude.h
		IFpMaxPlatformAttributes.h
		rageMaxPlatformAttributes.h
		PlatformAttribute.h
	}
	Folder "Resource Files" {
		resource.h
		rageMaxPlatformAttributes.def
		rageMaxPlatformAttributes.rc
	}
	Folder "Source Files" {
		DllEntry.cpp
		IFpMaxPlatformAttributes.h
		PlatformAttribute.cpp
		rageMaxPlatformAttributes.cpp
		script.cpp
	}
}