//IFpMaxDataStore.h

//Defines the function publishing interface which can be used by other plugins to interact with the data
//store through MAX's function publishing system.

#ifndef __IFPMAXPLATFORMATTRIBUTES_H__
#define __IFPMAXPLATFORMATTRIBUTES_H__

#define FP_MAXPLATFORMATTRIBUTES_INTERFACE Interface_ID(0x4b4374b5, 0x3cfb424b)

#define GetPlatformAttributesInterface() \
	(IFpMaxPlatformAttributes*)GetCOREInterface( FP_MAXPLATFORMATTRIBUTES_INTERFACE );

class IFpMaxPlatformAttributes : public FPStaticInterface
{
public:
	enum { em_getPlatformAttr, em_getPlatformAttrInt, em_getPlatformAttrFloat, em_getPlatformAttrBool, em_getPlatformAttrStr,
			em_getPlatformAttrPlatforms, em_getPlatformAttrCount, em_getPlatformAttrValueAt, em_getPlatformAttrIndexAt };
public:
	virtual ~IFpMaxPlatformAttributes() {}

public:
	//virtual PlatformAttribute*	GetPlatformAttr(INode* pNode, int attrIndex, TSTR platformName) = 0;
	virtual int					GetPlatformAttrInt(INode* pNode, int attrIndex, TSTR platformName) = 0;
	virtual float				GetPlatformAttrFloat(INode* pNode, int attrIndex, TSTR platformName) = 0;
	virtual bool				GetPlatformAttrBool(INode* pNode, int attrIndex, TSTR platformName) = 0;
	virtual TSTR				GetPlatformAttrStr(INode* pNode, int attrIndex, TSTR platformName) = 0;

	virtual Value*				GetPlatformAttrPlatforms(INode* pNode) = 0;
	virtual int					GetPlatformAttrCount(INode* pNode, TSTR platformName) = 0;
	//virtual PlatformAttribute*	GetPlatformAttrValueAt(INode* pNode, int index, TSTR platformName) = 0;
	virtual s32					GetPlatformAttrIndexAt(INode* pNode, int index, TSTR platformName) = 0;
};

#endif //__IFPMAXPLATFORMATTRIBUTES_H__