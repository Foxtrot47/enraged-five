
// My headers
#include "IFpMaxPlatformAttributes.h"
#include "rageMaxPlatformAttributes.h"

class FPMaxPlatformAttributes : public IFpMaxPlatformAttributes
{
public:
	int		GetPlatformAttrInt(INode* pNode, int attrIndex, TSTR platformName);
	float	GetPlatformAttrFloat(INode* pNode, int attrIndex, TSTR platformName);
	bool	GetPlatformAttrBool(INode* pNode, int attrIndex, TSTR platformName);
	TSTR	GetPlatformAttrStr(INode* pNode, int attrIndex, TSTR platformName);

	int		GetPlatformAttrCount(INode* pNode, TSTR platformName);
	int		GetPlatformAttrIndexAt(INode* pNode, int index, TSTR platformName);
	Value*	GetPlatformAttrPlatforms(INode* pNode);

	DECLARE_DESCRIPTOR(FPMaxPlatformAttributes);
	BEGIN_FUNCTION_MAP;
	
	FN_3(IFpMaxPlatformAttributes::em_getPlatformAttrInt, TYPE_INT, GetPlatformAttrInt, TYPE_INODE, TYPE_INT, TYPE_TSTR_BV);
	FN_3(IFpMaxPlatformAttributes::em_getPlatformAttrFloat, TYPE_FLOAT, GetPlatformAttrFloat, TYPE_INODE, TYPE_INT, TYPE_TSTR_BV);
	FN_3(IFpMaxPlatformAttributes::em_getPlatformAttrBool, TYPE_BOOL, GetPlatformAttrBool, TYPE_INODE, TYPE_INT, TYPE_TSTR_BV);
	FN_3(IFpMaxPlatformAttributes::em_getPlatformAttrStr, TYPE_TSTR_BV, GetPlatformAttrStr, TYPE_INODE, TYPE_INT, TYPE_TSTR_BV);

	FN_1(IFpMaxPlatformAttributes::em_getPlatformAttrPlatforms, TYPE_VALUE, GetPlatformAttrPlatforms, TYPE_INODE);
	FN_2(IFpMaxPlatformAttributes::em_getPlatformAttrCount, TYPE_INT, GetPlatformAttrCount, TYPE_INODE, TYPE_TSTR_BV);
	FN_3(IFpMaxPlatformAttributes::em_getPlatformAttrIndexAt, TYPE_INT, GetPlatformAttrIndexAt, TYPE_INODE, TYPE_INT, TYPE_TSTR_BV);

	END_FUNCTION_MAP;
};

FPMaxPlatformAttributes FPMaxPlatformAttributesDesc(
									FP_MAXPLATFORMATTRIBUTES_INTERFACE,
									_T("RsPlatformAttributes"),
									0,
									NULL,
									FP_CORE,

									FPMaxPlatformAttributes::em_getPlatformAttrInt, _T("getPlatformAttrInt"), 0, TYPE_INT, 0, 3,
									_T("node"), 0, TYPE_INODE,
									_T("index"), 0, TYPE_INT,
									_T("platform"), 0, TYPE_TSTR_BV,
								  
									FPMaxPlatformAttributes::em_getPlatformAttrFloat, _T("getPlatformAttrFloat"), 0, TYPE_FLOAT, 0, 3,
									_T("node"), 0, TYPE_INODE,
									_T("index"), 0, TYPE_INT,
									_T("platform"), 0, TYPE_TSTR_BV,

									FPMaxPlatformAttributes::em_getPlatformAttrBool, _T("getPlatformAttrBool"), 0, TYPE_BOOL, 0, 3,
									_T("node"), 0, TYPE_INODE,
									_T("index"), 0, TYPE_INT,
									_T("platform"), 0, TYPE_TSTR_BV,

									FPMaxPlatformAttributes::em_getPlatformAttrStr, _T("getPlatformAttrStr"), 0, TYPE_TSTR_BV, 0, 3,
									_T("node"), 0, TYPE_INODE,
									_T("index"), 0, TYPE_INT,
									_T("platform"), 0, TYPE_TSTR_BV,

									FPMaxPlatformAttributes::em_getPlatformAttrPlatforms, _T("getPlatformAttrPlatforms"), 0, TYPE_VALUE, 0, 1,
									_T("node"), 0, TYPE_INODE,

									FPMaxPlatformAttributes::em_getPlatformAttrCount, _T("getPlatformAttrCount"), 0, TYPE_INT, 0, 2,
									_T("node"), 0, TYPE_INODE,
									_T("platform"), 0, TYPE_TSTR_BV,

									FPMaxPlatformAttributes::em_getPlatformAttrIndexAt, _T("getPlatformAttrIndexAt"), 0, TYPE_INT, 0, 3,
									_T("node"), 0, TYPE_INODE,
									_T("index"), 0, TYPE_INT,
									_T("platform"), 0, TYPE_TSTR_BV,

									end
									);

int FPMaxPlatformAttributes::GetPlatformAttrInt(INode* pNode, int attrIndex, TSTR platformName)
{
	PlatformAttributeData* pAttributeData = thePlatformAttributes.GetPlatformAttributeData(pNode);

	if (!pAttributeData)
		return 0;

	platformName.toLower();

	PlatformAttribute* pPlatformAttribute = pAttributeData->FindPlatformAttribute(attrIndex, std::string(platformName.data()));

	if (!pPlatformAttribute)
		return 0;

	dmat::AttributeValue* pValue = pPlatformAttribute->GetAttributeValue();

	if (!pPlatformAttribute)
		return 0;

	return ((int)*pValue);
}

float FPMaxPlatformAttributes::GetPlatformAttrFloat(INode* pNode, int attrIndex, TSTR platformName)
{
	PlatformAttributeData* pAttributeData = thePlatformAttributes.GetPlatformAttributeData(pNode);

	if (!pAttributeData)
		return 0.0f;

	platformName.toLower();

	PlatformAttribute* pPlatformAttribute = pAttributeData->FindPlatformAttribute(attrIndex, std::string(platformName.data()));

	if (!pPlatformAttribute)
		return 0.0f;

	dmat::AttributeValue* pValue = pPlatformAttribute->GetAttributeValue();

	if (!pPlatformAttribute)
		return 0.0f;

	return ((float)*pValue);
}

bool FPMaxPlatformAttributes::GetPlatformAttrBool(INode* pNode, int attrIndex, TSTR platformName)
{
	PlatformAttributeData* pAttributeData = thePlatformAttributes.GetPlatformAttributeData(pNode);

	if (!pAttributeData)
		return false;

	platformName.toLower();

	PlatformAttribute* pPlatformAttribute = pAttributeData->FindPlatformAttribute(attrIndex, std::string(platformName.data()));

	if (!pPlatformAttribute)
		return false;

	dmat::AttributeValue* pValue = pPlatformAttribute->GetAttributeValue();

	if (!pPlatformAttribute)
		return false;

	return ((bool)*pValue);
}

TSTR FPMaxPlatformAttributes::GetPlatformAttrStr(INode* pNode, int attrIndex, TSTR platformName)
{
	PlatformAttributeData* pAttributeData = thePlatformAttributes.GetPlatformAttributeData(pNode);

	if (!pAttributeData)
		return CStr("");

	platformName.toLower();

	PlatformAttribute* pPlatformAttribute = pAttributeData->FindPlatformAttribute(attrIndex, std::string(platformName.data()));

	if (!pPlatformAttribute)
		return CStr("");

	dmat::AttributeValue* pValue = pPlatformAttribute->GetAttributeValue();

	if (!pPlatformAttribute)
		return CStr("");

	return CStr((const char*)*pValue);
}

Value* FPMaxPlatformAttributes::GetPlatformAttrPlatforms(INode* pNode)
{
	PlatformAttributeData* pAttributeData = thePlatformAttributes.GetPlatformAttributeData(pNode);

	if (!pAttributeData)
		return NULL;

	std::vector<std::string> platformList;
	pAttributeData->GetPlatforms(&platformList);

	Array* retPlatformList = new Array((int)platformList.size());
	for(int i = 1; i < platformList.size() + 1; i++)
	{
		// List of platforms is 0 index based but fucking max arrays are 1 index based.
		retPlatformList->append(new String(platformList[i-1].c_str()));
	}

	return retPlatformList;
}

int FPMaxPlatformAttributes::GetPlatformAttrCount(INode* pNode, TSTR platformName)
{
	PlatformAttributeData* pAttributeData = thePlatformAttributes.GetPlatformAttributeData(pNode);
	int attrCount = 0;

	platformName.toLower();

	if (pAttributeData)
	{
		std::vector<PlatformAttribute>* pAttributeList = pAttributeData->GetAttributes(std::string(platformName.data()));
		if (pAttributeList)
			attrCount = (s32)pAttributeList->size();
	}

	return attrCount;
}

int FPMaxPlatformAttributes::GetPlatformAttrIndexAt(INode* pNode, int index, TSTR platformName)
{
	int attrIndex = -1;
	platformName.toLower();

	// Get base attribute data from the node.
	DataInstance* pDataInstance = thePlatformAttributes.GetAttributeData(pNode);
	if (!pDataInstance)
		return -1;

	// Get the attributes.
	dmat::AttributeInst* pAttributes = pDataInstance->GetAttributes();
	if (!pAttributes)
		return -1;

	const dmat::AttributeClass& attributeClass = pAttributes->GetClass();
	PlatformAttributeData* pPlatformAttributeData = thePlatformAttributes.GetPlatformAttributeData(pNode);

	if (pPlatformAttributeData)
	{
		std::vector<PlatformAttribute>* pAttributeList = pPlatformAttributeData->GetAttributes(std::string(platformName.data()));

		if (!pAttributeList || index > pAttributeList->size())
			return -1;

		// Because we store the global attribute index, we need to resolve it back to the class index.
		// This index will match what is returned when using base attributes ex. GetAttrIndex "Gta Object" "Breakable"
		s32 globalAttrIndex = pAttributeList->at(index).GetAttributeIndex();
		s32 classAttrIndex = attributeClass.GetClassId(globalAttrIndex);

		attrIndex = classAttrIndex;
	}

	return attrIndex;
}