#ifndef _MAX_PLATFORM_ATTRIBUTES_H_
#define _MAX_PLATFORM_ATTRIBUTES_H_

#include "3dsmaxsdk_preinclude.h"
#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

#include "PlatformAttribute.h"
#include "rageMaxDataStore/DataInstance.h"

#include <vector>

#define MAX_PLATFORM_ATTRIBUTES_CLASS_ID	Class_ID(0xc6dddcae, 0x40e75aca)

class rageMaxPlatformAttributes : public GUP, public ReferenceMaker
{
public:
	//Constructor/Destructor
	rageMaxPlatformAttributes();
	~rageMaxPlatformAttributes();

	HWND m_hMainWnd;

	// GUP Methods
	DWORD		Start();
	void		Stop();
	DWORD_PTR	Control( DWORD parameter );

	// ReferenceMaker methods
	int NumRefs();
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle pTarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message);

	PlatformAttributeData* GetPlatformAttributeData(Animatable* pAnimtable);
	CustPlatformAttributeData* AddCustAttrib(Animatable* pAnimtable);

	void AddAttribute(RefTargetHandle ref, char* platform, s32 attIndex, dmat::AttributeValue* value);

	void AddAttributes(RefTargetHandle ref, char* platform, s32 attIndex, bool val);
	void AddAttributes(RefTargetHandle ref, char* platform, s32 attIndex, float val);
	void AddAttributes(RefTargetHandle ref, char* platform, s32 attIndex, s32 val);
	void AddAttributes(RefTargetHandle ref, char* platform, s32 attIndex, const char* val);

	bool HasAttributes(RefTargetHandle ref);

	DataInstance* GetAttributeData(RefTargetHandle ref);
	const char* GetAttributeClass(RefTargetHandle ref);

	// Loading/Saving
	IOResult Save(ISave *isave);
	IOResult Load(ILoad *iload);
	void PostLoad(ILoad *iload);
};

extern rageMaxPlatformAttributes thePlatformAttributes;

extern TCHAR *GetString(int id);

class rageMaxPlatformAttributesClassDesc : public ClassDesc2 
{
public:
	virtual int IsPublic() 							{ return TRUE; }
	virtual void* Create(BOOL) 						{ return new rageMaxPlatformAttributes(); }
	virtual const TCHAR *	ClassName() 			{ return GetString(IDS_CLASS_NAME); }
	virtual SClass_ID SuperClassID() 				{ return GUP_CLASS_ID; }
	virtual Class_ID ClassID() 						{ return MAX_PLATFORM_ATTRIBUTES_CLASS_ID; }
	virtual const TCHAR* Category() 				{ return GetString(IDS_CATEGORY); }

	virtual const TCHAR* InternalName() 			{ return _T("rageMaxPlatformAttributes"); }
	virtual HINSTANCE HInstance() 					{ return hInstance; }
};

extern rageMaxPlatformAttributesClassDesc thePlatformAttributesDesc;

extern HINSTANCE hInstance;

#endif //_MAX_PLATFORM_ATTRIBUTES_H_
