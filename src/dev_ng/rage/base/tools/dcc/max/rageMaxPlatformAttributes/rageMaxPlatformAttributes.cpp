#include "CustAttrib.h"
#include "ICustAttribContainer.h"

#include "rageMaxPlatformAttributes.h"
#include "PlatformAttribute.h"

#include "rageMaxDataStore/CustAttribData.h"

rageMaxPlatformAttributes thePlatformAttributes;
rageMaxPlatformAttributesClassDesc thePlatformAttributesDesc;

#include <string>

ClassDesc* GetrageMaxPlatformAttributesDesc() { return &thePlatformAttributesDesc; }

rageMaxPlatformAttributes::rageMaxPlatformAttributes()
{

}

rageMaxPlatformAttributes::~rageMaxPlatformAttributes()
{

}

DWORD rageMaxPlatformAttributes::Start()
{
	return GUPRESULT_KEEP;
}

void rageMaxPlatformAttributes::Stop()
{
}

DWORD_PTR rageMaxPlatformAttributes::Control(DWORD parameter)
{
	return 0;
}

IOResult rageMaxPlatformAttributes::Save(ISave *isave)
{
	return IO_OK;
}

IOResult rageMaxPlatformAttributes::Load(ILoad *iload)
{
	return IO_OK;
}

void rageMaxPlatformAttributes::PostLoad(ILoad *iload)
{
	
}

int rageMaxPlatformAttributes::NumRefs()
{
	return 0;
}

RefTargetHandle rageMaxPlatformAttributes::GetReference(int i)
{
	return NULL;
}

void rageMaxPlatformAttributes::SetReference(int i, RefTargetHandle pTarg)
{
	
}

RefResult rageMaxPlatformAttributes::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message)
{
	return REF_SUCCEED;
}

PlatformAttributeData* rageMaxPlatformAttributes::GetPlatformAttributeData(Animatable* pAnimtable)
{
	ICustAttribContainer* pContainer = pAnimtable->GetCustAttribContainer();

	if (pContainer)
	{
		for (s32 i = 0; i < pContainer->GetNumCustAttribs(); i++)
		{
			CustAttrib* pCustAttrib = pContainer->GetCustAttrib(i);
			const MCHAR* attrName = pCustAttrib->GetName();
			if (pCustAttrib && !strcmp(attrName, CUST_PLATFORM_ATTRIB_NAME))
			{
				CustPlatformAttributeData* pCustPlatformAttrib = (CustPlatformAttributeData*)pCustAttrib;

				if (pCustPlatformAttrib)
					return &pCustPlatformAttrib->GetData();
				else
					return NULL;
			}
		}
	}

	return NULL;
}

DataInstance* rageMaxPlatformAttributes::GetAttributeData(RefTargetHandle ref)
{
	CustAttribData* p_caData = NULL;
	ICustAttribContainer* custAttrib = ref->GetCustAttribContainer();
	DataInstance* pDataInstance = NULL;

	if (custAttrib)
	{
		int numAttributes = custAttrib->GetNumCustAttribs();

		for (int i = 0; i < numAttributes; i++)
		{
#if (MAX_RELEASE >= 12000)
			const MCHAR* p_cAttribName = custAttrib->GetCustAttrib(i)->GetName();
#else
			TCHAR* p_cAttribName = custAttrib->GetCustAttrib(i)->GetName();
#endif
			if (strcmp(p_cAttribName, "Attributes") == 0)
			{
				p_caData = (CustAttribData*)custAttrib->GetCustAttrib(i);
				pDataInstance = &p_caData->GetData();
			}
		}
	}

	return pDataInstance;
}

const char* rageMaxPlatformAttributes::GetAttributeClass(RefTargetHandle ref)
{
	DataInstance* pDataInstance = GetAttributeData(ref);

	const char* pClassName = NULL;

	if (pDataInstance)
		pClassName = pDataInstance->GetClassName().c_str();

	return pClassName;
}

CustPlatformAttributeData* rageMaxPlatformAttributes::AddCustAttrib(Animatable* pAnimtable)
{
	ICustAttribContainer* pContainer = pAnimtable->GetCustAttribContainer();
	CustPlatformAttributeData* pCustAttrib = NULL;

	if (pContainer == NULL)
	{
		pAnimtable->AllocCustAttribContainer();
		pContainer = pAnimtable->GetCustAttribContainer();
	}

	if (pContainer)
	{
		for (s32 i = 0; i < pContainer->GetNumCustAttribs(); i++)
		{
			CustAttrib* pCustAttrib = pContainer->GetCustAttrib(i);
			const MCHAR* attrName = pCustAttrib->GetName();
			if (pCustAttrib && !strcmp(attrName, CUST_PLATFORM_ATTRIB_NAME))
			{
				return (CustPlatformAttributeData*)pCustAttrib;
			}
		}
	}

	pCustAttrib = (CustPlatformAttributeData*)::CreateInstance(CUST_ATTRIB_CLASS_ID, CUST_PLATFORM_ATTRIB_CLASS_ID);

	if (pCustAttrib)
	{
		pContainer->InsertCustAttrib(0, pCustAttrib);

		for (s32 i = 0; i < pContainer->GetNumCustAttribs(); i++)
		{
			CustAttrib* pCustAttrib = pContainer->GetCustAttrib(i);
			const MCHAR* attrName = pCustAttrib->GetName();
			DebugPrint("Container now has CustAttr data of name %s\n", attrName);
		}
	}

	return pCustAttrib;
}

bool rageMaxPlatformAttributes::HasAttributes(RefTargetHandle ref)
{
	PlatformAttributeData* pAttributeData = GetPlatformAttributeData(ref);

	if (pAttributeData)
		return pAttributeData->ContainsAttributes();

	return false;
}

void rageMaxPlatformAttributes::AddAttribute(RefTargetHandle ref, char* platform, s32 attIndex, dmat::AttributeValue* value)
{
	PlatformAttributeData* attributeData = GetPlatformAttributeData(ref);

	if (attributeData == NULL)
	{
		CustPlatformAttributeData* pCustAttrib = AddCustAttrib(ref);
		assert(pCustAttrib);
		attributeData = &pCustAttrib->GetData();
	}

	PlatformAttribute* attribute = attributeData->FindPlatformAttribute(attIndex, std::string(platform));

	if (!attribute)
	{
		attribute = new PlatformAttribute(value, attIndex, std::string(strlwr(platform)));
		attributeData->AddPlatformAttribute(attribute);
	}
	else
	{
		attribute->Set(value, attIndex, std::string(platform));
	}
}

void rageMaxPlatformAttributes::AddAttributes(RefTargetHandle ref, char* platform, s32 attIndex, bool val)
{
	AddAttribute(ref, platform, attIndex, new dmat::AttributeValue(val));
}

void rageMaxPlatformAttributes::AddAttributes(RefTargetHandle ref, char* platform, s32 attIndex, float val)
{
	AddAttribute(ref, platform, attIndex, new dmat::AttributeValue(val));
}

void rageMaxPlatformAttributes::AddAttributes(RefTargetHandle ref, char* platform, s32 attIndex, s32 val)
{
	AddAttribute(ref, platform, attIndex, new dmat::AttributeValue(val));
}

void rageMaxPlatformAttributes::AddAttributes(RefTargetHandle ref, char* platform, s32 attIndex, const char* val)
{
	AddAttribute(ref, platform, attIndex, new dmat::AttributeValue(val));
}