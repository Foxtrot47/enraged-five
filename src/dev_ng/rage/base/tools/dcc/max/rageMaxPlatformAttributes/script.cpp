#include "rageMaxPlatformAttributes.h"

#include <maxscript/maxscript.h>  
#include <maxscript/maxwrapper/maxclasses.h> 
#include <maxscript/foundation/Numbers.h> 
#include <maxscript/foundation/strings.h> 
#include <maxscript/foundation/structs.h> 
#include <maxscript/foundation/hashtable.h> 
#include <maxscript/foundation/3dmath.h> 
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last

def_visible_primitive( get_platform_attr,				"GetPlatformAttr");
def_visible_primitive( set_platform_attr,				"SetPlatformAttr");
def_visible_primitive( has_platform_attr,				"HasPlatformAttr");

def_visible_primitive( get_platform_attr_platforms,		"GetPlatformAttrPlatforms");
def_visible_primitive( get_platform_attr_count,			"GetPlatformAttrCount");
def_visible_primitive( get_platform_attr_value_at,		"GetPlatformAttrValueAt");
def_visible_primitive( get_platform_attr_index_at,		"GetPlatformAttrIndexAt");

def_visible_primitive( delete_platform_attr,			"DeletePlatformAttr");
def_visible_primitive( delete_all_platform_attr,		"DeleteAllPlatformAttr");



Value *get_platform_attr_cf(Value** arg_list, int count)
{
	check_arg_count(get_platform_attr, 3, count);

	type_check(arg_list[1], Integer, "GetPlatformAttr <RefTarget> <index> <platform>");
	type_check(arg_list[2], String, "GetPlatformAttr <RefTarget> <index> <platform>");

	s32 index = arg_list[1]->to_int() - 1;
	char* platform = arg_list[2]->to_string();
	platform = _strlwr(platform);

	RefTargetHandle pRef = NULL;
	if (arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &undefined;

	DataInstance* pDataInstance = thePlatformAttributes.GetAttributeData(pRef);
	if (!pDataInstance)
		return &undefined;

	dmat::AttributeInst* pAttributes = pDataInstance->GetAttributes();
	if (!pAttributes)
		return &undefined;

	const dmat::AttributeClass& attributeClass = pAttributes->GetClass();
	const dmat::Attribute& attribute = attributeClass.GetItem(index);

	// We want to get the actual attribute ID (global attribute id, not class attribute id)
	// and we'll store this. Because we don't have direct access to the maxDataStore, I need to handle it this way.
	s32 globalId = attributeClass.GetId(index);
	if (globalId == -1)
		throw RuntimeError ("Index stored for platform attribute isn't associated with the attributes class on the object: ", arg_list[1]);

	PlatformAttributeData* pPlatformAttributeData = thePlatformAttributes.GetPlatformAttributeData(pRef);

	// We don't have any custom platform attributes stored in this ref.
	if (!pPlatformAttributeData)
		return &undefined;

	PlatformAttribute* pPlatformAttribute = pPlatformAttributeData->FindPlatformAttribute(globalId, std::string(platform));

	// There is no platform attribute for this index/platform.
	if (!pPlatformAttribute)
		return &undefined;

	dmat::AttributeValue attributeValue = *pPlatformAttribute->GetAttributeValue();

	switch(attributeValue.GetType())
	{
	case dmat::AttributeValue::eAttrTypeInt:
		{
			return_protected(Integer::intern((s32)attributeValue));
		}
		break;
	case dmat::AttributeValue::eAttrTypeFloat:
		{
			return_protected(Float::intern((float)attributeValue));
		}
		break;
	case dmat::AttributeValue::eAttrTypeBool:
		{
			bool value = (bool)attributeValue;
			if(value)
				return &true_value;
			else
				return &false_value;
		}
		break;
	case dmat::AttributeValue::eAttrTypeString:
		{
			// need to create a non-const char* because of bloody MaxScriptSDK
			const char *pName = (const char*)attributeValue;
			char *pNameCopy = new char[strlen(pName) + 1];
			String *pString;

			strcpy(pNameCopy, pName);

			pString = new String(pNameCopy);
			delete[] pNameCopy;

			return_protected(pString);
		}
		break;
	default:
		return &undefined;
	}
	
	return &undefined;
}

Value *set_platform_attr_cf(Value** arg_list, int count)
{
	check_arg_count(set_platform_attr, 4, count);

	type_check(arg_list[1], Integer, "SetPlatformAttr <RefTarget> <index> <value> <platform>");
	type_check(arg_list[3], String, "SetPlatformAttr <RefTarget> <index> <value> <platform>");

	RefTargetHandle pRef = NULL;
	if (arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &false_value;

	s32 index = arg_list[1]->to_int() - 1;
	char* platform = arg_list[3]->to_string();
	platform = _strlwr(platform);

	DataInstance* pDataInstance = thePlatformAttributes.GetAttributeData(pRef);
	if (!pDataInstance)
		return &false_value;

	dmat::AttributeInst* pAttributes = pDataInstance->GetAttributes();
	if (!pAttributes)
		return &false_value;

	const dmat::AttributeClass& attributeClass = pAttributes->GetClass();
	const dmat::Attribute& attribute = attributeClass.GetItem(index);

	// We want to get the actual attribute ID (global attribute id, not class attribute id)
	// and we'll store this. Because we don't have direct access to the maxDataStore, I need to handle it this way.
	s32 globalId = attributeClass.GetId(index);
	if (globalId == -1)
		throw RuntimeError ("Index stored for platform attribute isn't associated with the attributes class on the object: ", arg_list[1]);

	dmat::Attribute::Type attrType;

	s32 iAttr = 0;
	float fAttr = 0.0f;
	bool bAttr = false;
	char *sAttr = NULL;

	attrType = attribute.GetType();
	switch(attrType)
	{
	case dmat::Attribute::INT:
		iAttr = arg_list[2]->to_int();
		break;
	case dmat::Attribute::FLOAT:
		fAttr = arg_list[2]->to_float();
		break;
	case dmat::Attribute::BOOL:
		bAttr = (arg_list[2]->to_bool() == 1) ? true : false;
		break;
	case dmat::Attribute::STRING:
		sAttr = arg_list[2]->to_string();
		break;
	}

	switch (attrType)
	{
	case dmat::Attribute::INT:
		thePlatformAttributes.AddAttributes(pRef, platform, globalId, iAttr);
		break;
	case dmat::Attribute::FLOAT:
		thePlatformAttributes.AddAttributes(pRef, platform, globalId, fAttr);
		break;
	case dmat::Attribute::BOOL:
		thePlatformAttributes.AddAttributes(pRef, platform, globalId, bAttr);
		break;
	case dmat::Attribute::STRING:
		thePlatformAttributes.AddAttributes(pRef, platform, globalId, sAttr);
		break;
	default:
		break;
	}

	return &true_value;
}

Value *has_platform_attr_cf(Value** arg_list, int count)
{
	check_arg_count(has_platform_attr, 2, count);

	type_check(arg_list[1], Integer, "HasPlatformAttr <RefTarget> <index>");

	RefTargetHandle pRef = NULL;
	if (arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &false_value;

	s32 index = arg_list[1]->to_int() - 1;

	DataInstance* pDataInstance = thePlatformAttributes.GetAttributeData(pRef);
	if (!pDataInstance)
		return &false_value;

	dmat::AttributeInst* pAttributes = pDataInstance->GetAttributes();
	if (!pAttributes)
		return &false_value;

	const dmat::AttributeClass& attributeClass = pAttributes->GetClass();
	const dmat::Attribute& attribute = attributeClass.GetItem(index);

	// We want to get the actual attribute ID (global attribute id, not class attribute id)
	// and we'll store this. Because we don't have direct access to the maxDataStore, I need to handle it this way.
	s32 globalId = attributeClass.GetId(index);
	if (globalId == -1)
		throw RuntimeError ("Index stored for platform attribute isn't associated with the attributes class on the object: ", arg_list[1]);

	PlatformAttributeData* pPlatformAttributeData = thePlatformAttributes.GetPlatformAttributeData(pRef);

	// We don't have any custom platform attributes stored in this ref.
	if (!pPlatformAttributeData)
		return &false_value;

	std::vector<std::string> platformList;
	pPlatformAttributeData->GetPlatforms(&platformList);

	bool hasAttributes = false;
	for (int i = 0; i < platformList.size(); i++)
	{
		PlatformAttribute* pPlatformAttribute = pPlatformAttributeData->FindPlatformAttribute(globalId, platformList[i]);

		// There is no platform attribute for this index/platform.
		if (pPlatformAttribute)
			hasAttributes = true;
	}

	if (hasAttributes)
		return &true_value;
	else
		return &false_value;
}

Value *get_platform_attr_platforms_cf(Value** arg_list, int count)
{
	check_arg_count(get_platform_attr_platforms, 1, count);

	RefTargetHandle pRef = NULL;
	if (arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &undefined;

	PlatformAttributeData* pPlatformAttributeData = thePlatformAttributes.GetPlatformAttributeData(pRef);

	if (!pPlatformAttributeData)
		return &undefined;

	std::vector<std::string> platformList;
	pPlatformAttributeData->GetPlatforms(&platformList);

	Array* retPlatformList = new Array((int)platformList.size());
	for(int i = 0; i < platformList.size(); i++)
	{ 
		retPlatformList->append(new String(platformList[i].c_str()));
	}

	return retPlatformList;
}

Value *get_platform_attr_count_cf(Value** arg_list, int count)
{
	check_arg_count(get_platform_attr_count, 2, count);

	type_check(arg_list[1], String, "GetPlatformAttrCount <RefTarget> <platform>");

	char* platform = arg_list[1]->to_string();
	platform = _strlwr(platform);

	RefTargetHandle pRef = NULL;
	if (arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &undefined;

	s32 attrCount = 0;
	PlatformAttributeData* pPlatformAttributeData = thePlatformAttributes.GetPlatformAttributeData(pRef);

	if (pPlatformAttributeData)
	{
		std::vector<PlatformAttribute>* pAttributeList = pPlatformAttributeData->GetAttributes(std::string(platform));
		if (pAttributeList)
			attrCount = (s32)pAttributeList->size();
	}
	
	return_protected(Integer::intern(attrCount));
}

Value *get_platform_attr_value_at_cf(Value** arg_list, int count)
{
	check_arg_count(get_platform_attr_value_at, 3, count);

	type_check(arg_list[1], String, "GetPlatformAttrValueAt <RefTarget> <platform> <index>");
	type_check(arg_list[2], Integer, "GetPlatformAttrValueAt <RefTarget> <platform> <index>");

	char* platform = arg_list[1]->to_string();
	platform = _strlwr(platform);
	s32 index = arg_list[2]->to_int();

	RefTargetHandle pRef = NULL;
	if (arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &undefined;

	PlatformAttributeData* pPlatformAttributeData = thePlatformAttributes.GetPlatformAttributeData(pRef);

	if (pPlatformAttributeData)
	{
		std::vector<PlatformAttribute>* pAttributeList = pPlatformAttributeData->GetAttributes(std::string(platform));

		if (!pAttributeList || index > pAttributeList->size())
			return &undefined;

		dmat::AttributeValue attributeValue = *(pAttributeList->at(index)).GetAttributeValue();

		switch(attributeValue.GetType())
		{
		case dmat::AttributeValue::eAttrTypeInt:
			{
				return_protected(Integer::intern((s32)attributeValue));
			}
			break;
		case dmat::AttributeValue::eAttrTypeFloat:
			{
				return_protected(Float::intern((float)attributeValue));
			}
			break;
		case dmat::AttributeValue::eAttrTypeBool:
			{
				bool value = (bool)attributeValue;
				if(value)
					return &true_value;
				else
					return &false_value;
			}
			break;
		case dmat::AttributeValue::eAttrTypeString:
			{
				// need to create a non-const char* because of bloody MaxScriptSDK
				const char *pName = (const char*)attributeValue;
				char *pNameCopy = new char[strlen(pName) + 1];
				String *pString;

				strcpy(pNameCopy, pName);

				pString = new String(pNameCopy);
				delete[] pNameCopy;

				return_protected(pString);
			}
			break;
		default:
			return &undefined;
		}
	}

	return &undefined;
}

Value *get_platform_attr_index_at_cf(Value** arg_list, int count)
{
	check_arg_count(get_platform_attr_index_at, 3, count);

	type_check(arg_list[1], String, "GetPlatformAttrIndexAt <RefTarget> <platform> <index>");
	type_check(arg_list[2], Integer, "GetPlatformAttrIndexAt <RefTarget> <platform> <index>");

	char* platform = arg_list[1]->to_string();
	platform = _strlwr(platform);
	s32 index = arg_list[2]->to_int();

	RefTargetHandle pRef = NULL;
	if (arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &undefined;

	// Get base attribute data from the node.
	DataInstance* pDataInstance = thePlatformAttributes.GetAttributeData(pRef);
	if (!pDataInstance)
		return &undefined;

	// Get the attributes.
	dmat::AttributeInst* pAttributes = pDataInstance->GetAttributes();
	if (!pAttributes)
		return &undefined;

	const dmat::AttributeClass& attributeClass = pAttributes->GetClass();
	PlatformAttributeData* pPlatformAttributeData = thePlatformAttributes.GetPlatformAttributeData(pRef);

	if (pPlatformAttributeData)
	{
		std::vector<PlatformAttribute>* pAttributeList = pPlatformAttributeData->GetAttributes(std::string(platform));

		if (!pAttributeList || index > pAttributeList->size())
			return &undefined;

		// Because we store the global attribute index, we need to resolve it back to the class index.
		// This index will match what is returned when using base attributes ex. GetAttrIndex "Gta Object" "Breakable"
		s32 attrIndex = pAttributeList->at(index).GetAttributeIndex();
		s32 classAttrIndex = attributeClass.GetClassId(attrIndex);

		return_protected(Integer::intern(classAttrIndex + 1));
	}

	return &undefined;
}

Value *delete_platform_attr_cf(Value** arg_list, int count)
{
	check_arg_count(delete_platform_attr, 2, count);

	RefTargetHandle pRef = NULL;
	if (arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &false_value;

	type_check(arg_list[1], Integer, "GetPlatformAttr <RefTarget> <index> <platform>");
	type_check(arg_list[2], String, "GetPlatformAttr <RefTarget> <index> <platform>");

	// TODO - Hook up ability to delete specific platform attributes.
	// Not sure if we should use the index that the attributes are stored in the array or the "attribute index"
	// that is used throughout the rageMaxDataStore.

	return &undefined;
}

Value *delete_all_platform_attr_cf(Value** arg_list, int count)
{
	check_arg_count(delete_all_platform_attr, 2, count);

	type_check(arg_list[1], String, "DeleteAllPlatformAttr <RefTarget> <platform>");

	char* platform = arg_list[1]->to_string();
	platform = _strlwr(platform);

	RefTargetHandle pRef = NULL;
	if (arg_list[0]->is_kind_of(class_tag(MAXWrapper)))
		pRef = ((MAXWrapper*)arg_list[0])->GetReference(0);
	else
		return &undefined;

	PlatformAttributeData* pPlatformAttributeData = thePlatformAttributes.GetPlatformAttributeData(pRef);

	if (pPlatformAttributeData)
	{
		bool deleteResult = pPlatformAttributeData->DeletePlatformAttributes(std::string(platform));
		if (deleteResult)
			return &true_value;
	}

	return &false_value;
}