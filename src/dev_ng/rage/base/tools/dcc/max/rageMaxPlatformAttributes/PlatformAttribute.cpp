#include "PlatformAttribute.h"
#include <string>

#define CUSTPLATFORMATTRIB_ATTR_CHUNK		0x4e52

CustPlatformAttributeDataClassDesc thePlatformAttributeDataDesc;

ClassDesc* GetCustPlatformAttributeDataDesc() { return &thePlatformAttributeDataDesc; }

void PlatformAttributeData::AddPlatformAttribute(PlatformAttribute* platformAttribute)
{
	// Check to see if we already have a list of attributes for this platform.
	PlatformAttributeMapIterator iPlatform = m_platformAttributesMap.find(platformAttribute->GetPlatform());
	if(iPlatform == m_platformAttributesMap.end())
	{
		// Create a new list, add the attribute and add it to the map.
		std::vector<PlatformAttribute>* attributeList = new std::vector<PlatformAttribute>();
		attributeList->push_back(*platformAttribute);
		m_platformAttributesMap.insert(std::make_pair(platformAttribute->GetPlatform(), attributeList));
	}
	else
	{
		// Add the new attribute to the current platform specific list.
		std::vector<PlatformAttribute>* attributeList = ((*iPlatform).second);
		attributeList->push_back(*platformAttribute);
	}
}

PlatformAttribute* PlatformAttributeData::FindPlatformAttribute(s32 index, std::string platform)
{
	PlatformAttributeMapIterator iPlatform = m_platformAttributesMap.find(platform);
	if(iPlatform == m_platformAttributesMap.end())
		return NULL;

	std::vector<PlatformAttribute>* attributeList = ((*iPlatform).second);
	PlatformAttributeIterator iAttribute;

	for(iAttribute = attributeList->begin(); iAttribute != attributeList->end(); iAttribute++)
	{
		PlatformAttribute* attribute = &(*iAttribute);
		if (attribute->GetAttributeIndex() == index)
			return attribute;
	}

	return NULL;
}

bool PlatformAttributeData::DeletePlatformAttributes(std::string platform)
{
	PlatformAttributeMapIterator iPlatform = m_platformAttributesMap.find(platform);

	// No such platform stored.
	if(iPlatform == m_platformAttributesMap.end())
		return false;
	else
		m_platformAttributesMap.erase(iPlatform);

	return true;
}

void PlatformAttributeData::GetPlatforms(std::vector<std::string>* platformList)
{
	PlatformAttributeMapIterator iPlatform;
	s32 numAttributes = 0;

	for (iPlatform = m_platformAttributesMap.begin(); iPlatform != m_platformAttributesMap.end(); iPlatform++)
	{
		std::string platform = ((*iPlatform).first);
		platformList->push_back(platform);
	}
}

std::vector<PlatformAttribute>* PlatformAttributeData::GetAttributes(std::string platform)
{
	PlatformAttributeMapIterator iPlatform = m_platformAttributesMap.find(platform);
	if(iPlatform == m_platformAttributesMap.end())
		return NULL;

	return ((*iPlatform).second);
}

s32 PlatformAttributeData::GetNumAttributes()
{
	PlatformAttributeMapIterator iPlatform;
	s32 numAttributes = 0;

	for (iPlatform = m_platformAttributesMap.begin(); iPlatform != m_platformAttributesMap.end(); iPlatform++)
	{
		std::vector<PlatformAttribute>* attributeList = ((*iPlatform).second);
		numAttributes += (s32)attributeList->size();
	}

	return numAttributes;
}

char* PlatformAttributeData::CreateRawData(s32 &size)
{
	PlatformAttributeMapIterator iPlatform;
	
	size = 4;	// Space for number of attributes being saved

	for (iPlatform = m_platformAttributesMap.begin(); iPlatform != m_platformAttributesMap.end(); iPlatform++)
	{
		std::vector<PlatformAttribute>* attributeList = ((*iPlatform).second);
		PlatformAttributeIterator iAttribute;

		for (iAttribute = attributeList->begin(); iAttribute != attributeList->end(); iAttribute++)
		{
			PlatformAttribute* pAttribute = &(*iAttribute);
			
			size += 16;		// Space for ID, size of platform string, value type, and size of data
			size += (s32)strlen(pAttribute->GetPlatform().c_str()) + 1;	// Space for platform string
			size += pAttribute->GetAttributeValue()->size();			// Space for value data
		}
	}

	char* pData = new char[size];
	char* pData2 = pData;

	s32 numAttributes = GetNumAttributes();

	*(s32*)pData2 = numAttributes;
	pData2 += 4;

	for (iPlatform = m_platformAttributesMap.begin(); iPlatform != m_platformAttributesMap.end(); iPlatform++)
	{
		std::vector<PlatformAttribute>* attributeList = ((*iPlatform).second);
		PlatformAttributeIterator iAttribute;

		for (iAttribute = attributeList->begin(); iAttribute != attributeList->end(); iAttribute++)
		{
			PlatformAttribute* pAttribute = &(*iAttribute);

			// Store the attribute index
			*(s32*)pData2 = pAttribute->GetAttributeIndex();
			pData2 += 4;

			// Store the platform string length
			s32 platformStringLen = strlen(pAttribute->GetPlatform().c_str()) + 1;
			*(s32*)pData2 = platformStringLen;
			pData2 += 4;

			// Store the value type
			s32 valueType = pAttribute->GetAttributeValue()->GetType();
			*(s32*)pData2 = valueType;
			pData2 += 4;

			// Store the data size
			s32 valueSize = (s32)pAttribute->GetAttributeValue()->size();
			*(s32*)pData2 = valueSize;
			pData2 += 4;

			// Store the platform string
			memcpy(pData2, pAttribute->GetPlatform().c_str(), strlen(pAttribute->GetPlatform().c_str()));
			pData2[platformStringLen - 1] = '\0';
			pData2 += platformStringLen;

			// Store the attribute value
			memcpy(pData2, pAttribute->GetAttributeValue()->GetRawDataPtr(), valueSize);
			pData2 += valueSize;
		}
	}

	return pData;
}

IOResult CustPlatformAttributeData::Load(ILoad *iload)
{
	// load slot queue
	IOResult res;
	unsigned long length;

	while (IO_OK == (res = iload->OpenChunk())) {
		switch(iload->CurChunkID())  
		{
		case CUSTPLATFORMATTRIB_ATTR_CHUNK:
			{
				s32 size = (s32)iload->CurChunkLength();
				char *pReadData = new char[size+1];
				char* pData = pReadData;

				res = iload->Read(pReadData, size, &length);

				// Read the total data size of the chunk
				s32 dataSize = *reinterpret_cast<s32 *>(pData);
				pData += 4;

				// Read the number of attributes saved
				s32 numAttributes = *reinterpret_cast<s32 *>(pData);
				pData += 4;

				for (int i = 0; i < numAttributes; i++)
				{
					// Attribute Index
					s32 attributeIndex = *reinterpret_cast<s32 *>(pData);
					pData += 4;

					// Platform string length
					s32 platformStringLen = *reinterpret_cast<s32 *>(pData);
					pData += 4;

					// Attribute type
					dmat::AttributeValue::eAttrType attributeType = (dmat::AttributeValue::eAttrType)*reinterpret_cast<s32 *>(pData);
					pData += 4;

					// Size of total attribute size
					s32 attributeDataSize = *reinterpret_cast<s32 *>(pData);
					pData += 4;

					// Platform string
					char* platform = pData;
					pData += platformStringLen;

					// Attribute value pointer.
					char* pValueData = pData;
					
					dmat::AttributeValue* attributeValue = NULL;

					switch(attributeType)
					{
					case dmat::AttributeValue::eAttrType::eAttrTypeBool:
						attributeValue = new dmat::AttributeValue(true);
						attributeValue->SetFromRawData(pValueData);
						break;
					case dmat::AttributeValue::eAttrType::eAttrTypeInt:
						attributeValue = new dmat::AttributeValue(0);
						attributeValue->SetFromRawData(pValueData);
						break;
					case dmat::AttributeValue::eAttrType::eAttrTypeFloat:
						attributeValue = new dmat::AttributeValue(0.0f);
						attributeValue->SetFromRawData(pValueData);
						break;
					case dmat::AttributeValue::eAttrType::eAttrTypeString:
						attributeValue = new dmat::AttributeValue(pValueData);
						break;
					}

					if (attributeValue != NULL)
					{
						PlatformAttribute* newAttr = new PlatformAttribute(attributeValue, attributeIndex, std::string(platform));
						m_data.AddPlatformAttribute(newAttr);
					}

					pData += attributeDataSize;
				}

				delete[] pReadData;
			}
			break;
		default:
			break;
		}
		iload->CloseChunk();
		if (res != IO_OK) 
			return res;
	}
	return IO_OK;
}

IOResult CustPlatformAttributeData::Save(ISave *isave)
{
	s32 size;
	unsigned long length;
	char *pData = NULL;

	if (m_data.GetNumAttributes() > 0)
	{
		pData = m_data.CreateRawData(size);

		isave->BeginChunk(CUSTPLATFORMATTRIB_ATTR_CHUNK);
		isave->Write(&size, sizeof(s32), &length);
		isave->Write(pData, size, &length);
		isave->EndChunk();

		delete [] pData;
	}

	return IO_OK;
}

RefResult CustPlatformAttributeData::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget,	PartID& partID,  RefMessage message)
{
	return REF_SUCCEED;
}