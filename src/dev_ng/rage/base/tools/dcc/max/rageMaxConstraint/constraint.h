#ifndef __INTERIOR__H
#define __INTERIOR__H

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;

#define CONSTRAINT_CLASS_ID	Class_ID(0x350739c4, 0x51515b0e)
#define CONSTRAINT_PBLOCK_REF 0

#ifdef CONSTRAINT_EXPORTS
#define CONSTRAINT_API __declspec(dllexport)
#else
#define CONSTRAINT_API __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////
enum 
{
	constraint_params 
}; 

/////////////////////////////////////////////////////////////////////////////////////////////////
enum 
{ 
	constraint_xmin, 
	constraint_xmax,
	constraint_ymin, 
	constraint_ymax,
	constraint_zmin, 
	constraint_zmax,
	constraint_pick,
};	

/////////////////////////////////////////////////////////////////////////////////////////////////
class Constraint : public HelperObject
/////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	Constraint();
	~Constraint();

	// From BaseObject
	CreateMouseCallBack* GetCreateMouseCallBack();
	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	TCHAR *GetObjectName() { return GetString(IDS_CLASS_NAME); }
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	void GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);

	// Animatable methods
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return pblock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == constraint_params); return pblock2;}
	void DeleteThis() { delete this; }
	Class_ID ClassID() { return CONSTRAINT_CLASS_ID; }  
	void GetClassName(TSTR& s){	s = GetString(IDS_CLASS_NAME);}

	int NumRefs() { return 1; }
	RefTargetHandle GetReference(int i);
	void SetReference(int i, RefTargetHandle rtarg);
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message);
	ObjectState Eval(TimeValue t);

	// From ref
	RefTargetHandle Clone(RemapDir& remap = DefaultRemapDir());

	// From object
	void InitNodeName(TSTR& s){	s = GetString(IDS_CLASS_NAME);}

	// From SimpleObject
	BOOL OKtoDisplay(TimeValue t);
	void InvalidateUI();

	IParamBlock2* pblock2;
	INode *refNode;

	void Draw(GraphicsWindow *gw, INode *inode, TimeValue t);
	void TargetChanged() { m_TargetChanged = true; }
	void CheckTarget(INode* owner);

protected:
	bool m_TargetChanged;
};	

#endif // __INTERIOR__H
