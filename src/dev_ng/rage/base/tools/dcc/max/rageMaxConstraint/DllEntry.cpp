#include "constraint.h"

extern ClassDesc2* GetConstraintDesc();

HINSTANCE	hInstance;
int			g_controlsInit = FALSE;
BOOL		g_libInitialized = FALSE;

/////////////////////////////////////////////////////////////////////////////////////////////////
BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved)
{
	hInstance = hinstDLL;

#if (MAX_RELEASE < 9000)
	if(!g_controlsInit) 
	{
		g_controlsInit = TRUE;
		InitCustomControls(hInstance);
		InitCommonControls();
	}
#endif

	return (TRUE);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_LIBDESCRIPTION);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) int LibNumberClasses()
{
	return 1;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) 
	{
		case 0: return GetConstraintDesc();
		default: return 0;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

/////////////////////////////////////////////////////////////////////////////////////////////////

#if( MAX_RELEASE >= 9000 )

__declspec( dllexport ) int LibInitialize(void)
{
	//According to the SDK docs LibInitialize "should" only be called once but this isn't strictly enforced
	//so make sure we only initialize once
	if(!g_libInitialized)
	{
		//Perform any custom plugin initialization here
		g_libInitialized = TRUE;

		if(!g_controlsInit) 
		{
			g_controlsInit = TRUE;

#if MAX_VERSION_MAJOR < 11
			InitCustomControls(hInstance);
#endif //MAX_VERSION_MAJOR

			InitCommonControls();
		}
	}
	return 1;
}

__declspec( dllexport ) int LibShutdown(void)
{
	return 1;
}

#endif //( MAX_RELEASE >= 9000 ) 

/////////////////////////////////////////////////////////////////////////////////////////////////
TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if(hInstance)
	{
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	}

	return NULL;
}
/////////////////////////////////////////////////////////////////////////////////////////////////
__declspec( dllexport ) ULONG CanAutoDefer()
{
	return 0;
}
