
#include "constraint.h"

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamm2.h"
#include "Simpobj.h"

static const float g_LineLength(0.3f);

/////////////////////////////////////////////////////////////////////////////////////////////////
class ConstraintClassDesc : public ClassDesc2 
/////////////////////////////////////////////////////////////////////////////////////////////////
{
	public:
	int 			IsPublic() { return TRUE; }
	void *			Create(BOOL loading = FALSE) { return new Constraint(); }
	const TCHAR *	ClassName() { return GetString(IDS_CLASS_NAME); }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return CONSTRAINT_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_CATEGORY); }

	const TCHAR*	InternalName() { return _T("Gta Water"); }
	HINSTANCE		HInstance() { return hInstance; }
};

/////////////////////////////////////////////////////////////////////////////////////////////////
static ConstraintClassDesc ConstraintDesc;

/////////////////////////////////////////////////////////////////////////////////////////////////
ClassDesc2* GetConstraintDesc()
{
	return &ConstraintDesc;
}

class PickNodeAccessor : PBAccessor
{
public:
	void Set(PB2Value& v, ReferenceMaker* owner, ParamID id, int tabIndex, TimeValue t)
	{
		Constraint* cons = (Constraint*)owner;
	
		cons->TargetChanged();
	}
};

PickNodeAccessor pickupnode_accessor;

ParamBlockDesc2 constraintDesc (constraint_params, _T("constraint Parameters"), IDS_CLASS_NAME, &ConstraintDesc, P_AUTO_CONSTRUCT | P_AUTO_UI, CONSTRAINT_PBLOCK_REF,
						 IDD_PANEL, IDS_PARAMS, 0, 0, NULL,
						 constraint_xmin, _T("Xmin"), TYPE_WORLD, P_ANIMATABLE, IDS_LENGTH,
							p_default,		-90.0f,
							p_range, 		float(-1.0E30), 0.0f, 
							p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_XMINEDIT, IDC_XMINSPINNER, 1.0f,
							end,
						 constraint_xmax, _T("XMax"), TYPE_WORLD, P_ANIMATABLE, IDS_LENGTH,
							p_default,		90.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_XMAXEDIT, IDC_XMAXSPINNER, 1.0f,
							end,
						 constraint_ymin, _T("YMin"), TYPE_WORLD, P_ANIMATABLE, IDS_LENGTH,
							p_default,		-90.0f,
							p_range, 		float(-1.0E30), 0.0f, 
							p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_YMINEDIT, IDC_YMINSPINNER, 1.0f,
							end,
						 constraint_ymax, _T("YMax"), TYPE_WORLD, P_ANIMATABLE, IDS_LENGTH,
							p_default,		90.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_YMAXEDIT, IDC_YMAXSPINNER, 1.0f,
							end,
						 constraint_zmin, _T("ZMin"), TYPE_WORLD, P_ANIMATABLE, IDS_LENGTH,
							p_default,		-90.0f,
							p_range, 		float(-1.0E30), 0.0f, 
							p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_ZMINEDIT, IDC_ZMINSPINNER, 1.0f,
							end,
						 constraint_zmax, _T("ZMax"), TYPE_WORLD, P_ANIMATABLE, IDS_LENGTH,
							p_default,		90.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_ZMAXEDIT, IDC_ZMAXSPINNER, 1.0f,
							end,

						constraint_pick,  _T("pickUpNode"), 		TYPE_INODE,  0,	IDS_LOOKAT_PICKNODE,
							p_ui, 			TYPE_PICKNODEBUTTON, IDC_BTN_NODE,
							p_accessor,		&pickupnode_accessor,
							p_prompt,		IDS_LOOKAT_PICKNODE_PROMPT,
							end,
							
						 end
						 );

/////////////////////////////////////////////////////////////////////////////////////////////////
Constraint::Constraint()
	:m_TargetChanged(false)
	,pblock2(NULL)
{
	ConstraintDesc.MakeAutoParamBlocks(this);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
Constraint::~Constraint()
{
	//delete m_pTriObject;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Constraint::CheckTarget(INode* owner)
{
	if(!m_TargetChanged)
	{
		return;
	}

	m_TargetChanged = false;

	INode* parent = NULL;
	Interval ivalid;
	pblock2->GetValue(constraint_pick, 0, parent, ivalid);

	if(parent)
	{
		Matrix3 mat;

		mat = parent->GetNodeTM(0);
		owner->SetNodeTM(0,mat);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle Constraint::GetReference(int i)
{
	switch(i)
	{
	case CONSTRAINT_PBLOCK_REF:
		return pblock2;
	}

	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Constraint::SetReference(int i, RefTargetHandle rtarg)
{
	switch (i)
	{
	case CONSTRAINT_PBLOCK_REF:
		pblock2 = (IParamBlock2*)rtarg; 
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RefResult Constraint::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID,RefMessage message)
{
	switch(message)
	{
	case REFMSG_CHANGE:
		if(partID & PART_TM)
		{
			TargetChanged();
		}
	}

	return REF_SUCCEED;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
ObjectState Constraint::Eval(TimeValue t)
{
	return ObjectState(this);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Constraint::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	HelperObject::BeginEditParams(ip,flags,prev);
	ConstraintDesc.BeginEditParams(ip,this,flags,prev);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Constraint::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	HelperObject::EndEditParams(ip,flags,next);
	ConstraintDesc.EndEditParams(ip,this,flags,next);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Constraint::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	Interval ivalid;
	CheckTarget(inode);

	Matrix3 local2world;
	Box3 localBox;

	local2world = inode->GetObjectTM(t);
	
	GetLocalBoundBox(t, inode, vp, localBox);

	box = localBox * local2world;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Constraint::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	box.Init();

	box.pmin.x = box.pmin.y = box.pmin.z = -g_LineLength;
	box.pmax.x = box.pmax.y = box.pmax.z = g_LineLength;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
int Constraint::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 mat;

	CheckTarget(inode);

	Interval ivalid;

	mat = inode->GetObjectTM(t);

	gw->setTransform(mat);

	Draw(gw, inode, t);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Constraint::Draw(GraphicsWindow *gw, INode *inode, TimeValue t)
{
	float amin,amax;
	float segmentSize = DegToRad(9.0f);
	float segments;
	int i,numSegments;
	float angleFrom,angleTo;
	Interval ivalid;
	Point3 pt[2];
	char acMinAsStr[5];
	char acMaxAsStr[5];

	DrawLineProc lineProc(gw);

	// ****************************************************************
	// x
	// ****************************************************************
	pblock2->GetValue(constraint_xmin, 0, amin, ivalid);
	pblock2->GetValue(constraint_xmax, 0, amax, ivalid);
	if((amax - amin) > 0.1f)
	{
		sprintf(acMinAsStr, "%d", (int)amin);
		sprintf(acMaxAsStr, "%d", (int)amax);

		amin = DegToRad(amin);
		amax = DegToRad(amax);

		if(amin > amax)
			amax = amin;

		pblock2->SetValue(constraint_xmax, 0, RadToDeg(amax));

		// The min line
		gw->setColor(LINE_COLOR, 1.0f, 0.5f, 0.5f);
		pt[0].x = 0.0f;
		pt[0].y = 0.0f;
		pt[0].z = 0.0f;

		pt[1].x = 0.0f;
		pt[1].y = g_LineLength * cos(amin);
		pt[1].z = g_LineLength * sin(amin);

		lineProc.proc(pt,2);
		gw->text(&pt[1], acMinAsStr);

		// The min arrow
		if(amin < -0.1f)
		{
			pt[0].x = pt[1].x;
			pt[0].y = (1.0f + (0.04f * amin)) * g_LineLength * cos(0.95f * amin);
			pt[0].z = (1.0f + (0.04f * amin)) * g_LineLength * sin(0.95f * amin);

			lineProc.proc(pt,2);

			pt[0].x = pt[1].x;
			pt[0].y = (1.0f - (0.04f * amin)) * g_LineLength * cos(0.95f * amin);
			pt[0].z = (1.0f - (0.04f * amin)) * g_LineLength * sin(0.95f * amin);

			lineProc.proc(pt,2);
		}

		// The max line
		pt[0].x = 0.0f;
		pt[0].y = 0.0f;
		pt[0].z = 0.0f;

		pt[1].x = 0.0f;
		pt[1].y = g_LineLength * cos(amax);
		pt[1].z = g_LineLength * sin(amax);

		lineProc.proc(pt,2);
		gw->text(&pt[1], acMaxAsStr);

		// The max arrow
		if(amax > 0.1f)
		{
			pt[0].x = pt[1].x;
			pt[0].y = (1.0f + (0.04f * amax)) * g_LineLength * cos(0.95f * amax);
			pt[0].z = (1.0f + (0.04f * amax)) * g_LineLength * sin(0.95f * amax);

			lineProc.proc(pt,2);

			pt[0].x = pt[1].x;
			pt[0].y = (1.0f - (0.04f * amax)) * g_LineLength * cos(0.95f * amax);
			pt[0].z = (1.0f - (0.04f * amax)) * g_LineLength * sin(0.95f * amax);

			lineProc.proc(pt,2);
		}

		// The segments	
		segments = (amax - amin) / segmentSize;
		numSegments = (int)segments;

		angleFrom = amin;
		angleTo = amin + segmentSize;

		for(i=0;i<numSegments;i++)
		{
			pt[0].x = 0.0f;
			pt[0].y = g_LineLength * cos(angleFrom);
			pt[0].z = g_LineLength * sin(angleFrom);

			pt[1].x = 0.0f;
			pt[1].y = g_LineLength * cos(angleTo);
			pt[1].z = g_LineLength * sin(angleTo);

			angleFrom = angleTo;
			angleTo += segmentSize;

			lineProc.proc(pt,2);
		}

		pt[0].x = 0.0f;
		pt[0].y = g_LineLength * cos(angleFrom);
		pt[0].z = g_LineLength * sin(angleFrom);

		pt[1].x = 0.0f;
		pt[1].y = g_LineLength * cos(amax);
		pt[1].z = g_LineLength * sin(amax);

		lineProc.proc(pt,2);

		// The Zero line
		gw->setColor(LINE_COLOR, 1.0f, 0.0f, 0.0f);
		pt[0].x = 0.0f;
		pt[0].y = 0.0f;
		pt[0].z = 0.0f;

		pt[1].x = 0.0f;
		pt[1].y = g_LineLength * cos(0.0f);
		pt[1].z = g_LineLength * sin(0.0f);

		lineProc.proc(pt,2);
		gw->text(&pt[1], "0");
	}

	// ****************************************************************
	// y
	// ****************************************************************
	pblock2->GetValue(constraint_ymin, 0, amin, ivalid);
	pblock2->GetValue(constraint_ymax, 0, amax, ivalid);
	if((amax - amin) > 0.1f)
	{
		sprintf(acMinAsStr, "%d", (int)amin);
		sprintf(acMaxAsStr, "%d", (int)amax);

		amin = DegToRad(amin);
		amax = DegToRad(amax);

		if(amin > amax)
			amax = amin;

		pblock2->SetValue(constraint_ymax, 0, RadToDeg(amax));

		// The min line
		gw->setColor(LINE_COLOR, 0.5f, 1.0f, 0.5f);
		pt[0].x = 0.0f;
		pt[0].y = 0.0f;
		pt[0].z = 0.0f;

		pt[1].x = g_LineLength * cos(amin);
		pt[1].y = 0.0f;
		pt[1].z = g_LineLength * sin(amin);

		lineProc.proc(pt,2);
		gw->text(&pt[1], acMinAsStr);

		// The min arrow
		if(amin < -0.1f)
		{
			pt[0].x = (1.0f + (0.04f * amin)) * g_LineLength * cos(0.95f * amin);
			pt[0].y = pt[1].y;
			pt[0].z = (1.0f + (0.04f * amin)) * g_LineLength * sin(0.95f * amin);

			lineProc.proc(pt,2);

			pt[0].x = (1.0f - (0.04f * amin)) * g_LineLength * cos(0.95f * amin);
			pt[0].y = pt[1].y;
			pt[0].z = (1.0f - (0.04f * amin)) * g_LineLength * sin(0.95f * amin);

			lineProc.proc(pt,2);
		}

		// The max line
		pt[0].x = 0.0f;
		pt[0].y = 0.0f;
		pt[0].z = 0.0f;

		pt[1].x = g_LineLength * cos(amax);
		pt[1].y = 0.0f;
		pt[1].z = g_LineLength * sin(amax);

		lineProc.proc(pt,2);
		gw->text(&pt[1], acMaxAsStr);

		// The max arrow
		if(amax > 0.1f)
		{
			pt[0].x = (1.0f + (0.04f * amax)) * g_LineLength * cos(0.95f * amax);
			pt[0].y = pt[1].y;
			pt[0].z = (1.0f + (0.04f * amax)) * g_LineLength * sin(0.95f * amax);

			lineProc.proc(pt,2);

			pt[0].x = (1.0f - (0.04f * amax)) * g_LineLength * cos(0.95f * amax);
			pt[0].y = pt[1].y;
			pt[0].z = (1.0f - (0.04f * amax)) * g_LineLength * sin(0.95f * amax);

			lineProc.proc(pt,2);
		}

		// The segments
		segments = (amax - amin) / segmentSize;
		numSegments = (int)segments;

		angleFrom = amin;
		angleTo = amin + segmentSize;

		for(i=0;i<numSegments;i++)
		{
			pt[0].x = g_LineLength * cos(angleFrom);
			pt[0].y = 0.0f;
			pt[0].z = g_LineLength * sin(angleFrom);

			pt[1].x = g_LineLength * cos(angleTo);
			pt[1].y = 0.0f;
			pt[1].z = g_LineLength * sin(angleTo);

			angleFrom = angleTo;
			angleTo += segmentSize;

			lineProc.proc(pt,2);
		}

		pt[0].x = g_LineLength * cos(angleFrom);
		pt[0].y = 0.0f;
		pt[0].z = g_LineLength * sin(angleFrom);

		pt[1].x = g_LineLength * cos(amax);
		pt[1].y = 0.0f;
		pt[1].z = g_LineLength * sin(amax);

		lineProc.proc(pt,2);

		// The Zero line
		gw->setColor(LINE_COLOR, 0, 1, 0);
		pt[0].x = 0.0f;
		pt[0].y = 0.0f;
		pt[0].z = 0.0f;

		pt[1].x = g_LineLength * cos(0.0f);
		pt[1].y = 0.0f;
		pt[1].z = g_LineLength * sin(0.0f);

		lineProc.proc(pt,2);
		gw->text(&pt[1], "0");
	}

	// ****************************************************************
	// z
	// ****************************************************************
	pblock2->GetValue(constraint_zmin, 0, amin, ivalid);
	pblock2->GetValue(constraint_zmax, 0, amax, ivalid);
	if((amax - amin) > 0.1f)
	{
		sprintf(acMinAsStr, "%d", (int)amin);
		sprintf(acMaxAsStr, "%d", (int)amax);

		amin = DegToRad(amin);
		amax = DegToRad(amax);

		if(amin > amax)
			amax = amin;

		pblock2->SetValue(constraint_zmax, 0, RadToDeg(amax));

		// The min line
		gw->setColor(LINE_COLOR, 0.5f, 0.5f, 1.0f);

		pt[0].x = 0.0f;
		pt[0].y = 0.0f;
		pt[0].z = 0.0f;

		pt[1].x = g_LineLength * cos(amin);
		pt[1].y = g_LineLength * sin(amin);
		pt[1].z = 0.0f;

		lineProc.proc(pt,2);
		gw->text(&pt[1], acMinAsStr);

		// The min arrow
		if(amin < -0.1f)
		{
			pt[0].x = (1.0f + (0.04f * amin)) * g_LineLength * cos(0.95f * amin);
			pt[0].y = (1.0f + (0.04f * amin)) * g_LineLength * sin(0.95f * amin);
			pt[0].z = pt[1].z;

			lineProc.proc(pt,2);

			pt[0].x = (1.0f - (0.04f * amin)) * g_LineLength * cos(0.95f * amin);
			pt[0].y = (1.0f - (0.04f * amin)) * g_LineLength * sin(0.95f * amin);
			pt[0].z = pt[1].z;

			lineProc.proc(pt,2);
		}

		// The max line
		pt[0].x = 0.0f;
		pt[0].y = 0.0f;
		pt[0].z = 0.0f;

		pt[1].x = g_LineLength * cos(amax);
		pt[1].y = g_LineLength * sin(amax);
		pt[1].z = 0.0f;

		lineProc.proc(pt,2);
		gw->text(&pt[1], acMaxAsStr);

		// The max arrow
		if(amax > 0.1f)
		{
			pt[0].x = (1.0f + (0.04f * amax)) * g_LineLength * cos(0.95f * amax);
			pt[0].y = (1.0f + (0.04f * amax)) * g_LineLength * sin(0.95f * amax);
			pt[0].z = pt[1].z;

			lineProc.proc(pt,2);

			pt[0].x = (1.0f - (0.04f * amax)) * g_LineLength * cos(0.95f * amax);
			pt[0].y = (1.0f - (0.04f * amax)) * g_LineLength * sin(0.95f * amax);
			pt[0].z = pt[1].z;

			lineProc.proc(pt,2);
		}

		// The segments
		segments = (amax - amin) / segmentSize;
		numSegments = (int)segments;

		angleFrom = amin;
		angleTo = amin + segmentSize;

		for(i=0;i<numSegments;i++)
		{
			pt[0].x = g_LineLength * cos(angleFrom);
			pt[0].y = g_LineLength * sin(angleFrom);
			pt[0].z = 0.0f;

			pt[1].x = g_LineLength * cos(angleTo);
			pt[1].y = g_LineLength * sin(angleTo);
			pt[1].z = 0.0f;

			angleFrom = angleTo;
			angleTo += segmentSize;

			lineProc.proc(pt,2);
		}

		pt[0].x = g_LineLength * cos(angleFrom);
		pt[0].y = g_LineLength * sin(angleFrom);
		pt[0].z = 0.0f;

		pt[1].x = g_LineLength * cos(amax);
		pt[1].y = g_LineLength * sin(amax);
		pt[1].z = 0.0f;

		lineProc.proc(pt,2);

		// The Zero line
		gw->setColor(LINE_COLOR, 0, 0, 1);

		pt[0].x = 0.0f;
		pt[0].y = 0.0f;
		pt[0].z = 0.0f;

		pt[1].x = g_LineLength * cos(0.0f);
		pt[1].y = g_LineLength * sin(0.0f);
		pt[1].z = 0.0f;

		lineProc.proc(pt,2);
		gw->text(&pt[1], "0");
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
int Constraint::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 objMat = inode->GetObjectTM(t);
	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(objMat);
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	Draw(gw, inode, t);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
class ConstraintCreateCallBack : public CreateMouseCallBack 
/////////////////////////////////////////////////////////////////////////////////////////////////
{
	Constraint *ob;
	Point3 p0,p1;
	IPoint2 sp0, sp1;
	BOOL square;
public:
	int proc( ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat );
	void SetObj(Constraint *obj) { ob = obj; }
};

float SnapValue(float fVal)
{
	int iVal = (int)fVal;
	iVal>>=1;
	iVal<<=1;

	return (float)iVal;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
int ConstraintCreateCallBack::proc(ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat) 
{
	Point3 pTemp;

	sp0 = m;

	p0 = vpt->SnapPoint(m,m,NULL,SNAP_IN_3D);

	p1 = p0 + Point3(.01,.01,.01);

	pTemp = float(.5)*(p0+p1);

	pTemp[0] = SnapValue(pTemp[0]);
	pTemp[1] = SnapValue(pTemp[1]);
	pTemp[2] = SnapValue(pTemp[2]);

	mat.SetTrans(pTemp);				

	return CREATE_STOP;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
static ConstraintCreateCallBack constraintCreateCB;
/////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////
CreateMouseCallBack* Constraint::GetCreateMouseCallBack() 
{
	constraintCreateCB.SetObj(this);
	return(&constraintCreateCB);
}

/////////////////////////////////////////////////////////////////////////////////////////////////
BOOL Constraint::OKtoDisplay(TimeValue t) 
{
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
void Constraint::InvalidateUI() 
{
	IParamMap2 *pMapParam = pblock2->GetMap();
	if (pMapParam) 
		pMapParam->Invalidate();
}

/////////////////////////////////////////////////////////////////////////////////////////////////
RefTargetHandle Constraint::Clone(RemapDir& remap) 
{
	Constraint* newob = new Constraint();
	newob->ReplaceReference(0,pblock2->Clone(remap));

	BaseClone(this, newob, remap);

	return(newob);
}
