//
//
//    Filename: VolumeCreateMod.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Methods for Collision Volume create modifier
//
//
// Max headers
#include "Max.h"
#include "iparamm2.h"
// my headers
#include "VolumeCreateMod.h"
#include "CollisionControllers.h"
#include "ICollision.h"
#include "CollisionMesh.h"
#include "resource.h"

#define IDC_SELSINGLE	1100
#define IDC_SELPOLY		1101
#define IDC_SELELEMENT	1102

// selection info
const TCHAR *pSelTypeNames[] = {"Vertices", "Faces"};
const int pSelTypes[] = {IMESHSEL_OBJECT, IMESHSEL_VERTEX, IMESHSEL_FACE};

// static members
VolumeCreateMod *VolumeCreateMod::m_pCurrentModifier = NULL;
HWND VolumeCreateMod::m_hPanel = NULL;
HIMAGELIST VolumeCreateMod::m_hImageList = NULL;
ICustToolbar *VolumeCreateMod::m_pToolbar = NULL;
ICustButton *VolumeCreateMod::m_pSingleButton = NULL;
ICustButton *VolumeCreateMod::m_pPolyButton = NULL;
ICustButton *VolumeCreateMod::m_pElementButton = NULL;
ICustButton *VolumeCreateMod::m_pSphereButton = NULL;
ICustButton *VolumeCreateMod::m_pSphereArrayButton = NULL;
ICustButton *VolumeCreateMod::m_pBoxButton = NULL;
ICustButton *VolumeCreateMod::m_pMeshButton = NULL;
ISpinnerControl *VolumeCreateMod::m_pXSpinner = NULL;
ISpinnerControl *VolumeCreateMod::m_pYSpinner = NULL;
ISpinnerControl *VolumeCreateMod::m_pZSpinner = NULL;
ISpinnerControl *VolumeCreateMod::m_pRadiusSpinner = NULL;
int VolumeCreateMod::m_xsize = 1;
int VolumeCreateMod::m_ysize = 1;
int VolumeCreateMod::m_zsize = 1;
float VolumeCreateMod::m_radius = 10.0f;
BOOL VolumeCreateMod::m_maximise = FALSE;

INT_PTR CALLBACK CreateVolumeProc(HWND hWnd, unsigned int message, WPARAM wParam, LPARAM lParam);


//--- ClassDesc ------------------------------------------------------------------------

static VolumeCreateModClassDesc VolumeCreateModDesc;

ClassDesc* GetVolumeCreateModDesc() {return &VolumeCreateModDesc;}
void VolumeCreateModClassDesc::ResetClassParams (BOOL fileReset) {}

const TCHAR * VolumeCreateModClassDesc::ClassName() 
{
	return GetString(IDS_CREATE_MODIFIER);
}

const TCHAR* VolumeCreateModClassDesc::Category()
{
	return GetString(IDS_DMA_CATEGORY);
}

// --- VolumeCreateMod ------------------------------------------------------------------------------------------------

//
//        name: VolumeCreateMod::VolumeCreateMod
// description: Constructor
//
VolumeCreateMod::VolumeCreateMod()
{
}

//
//        name: VolumeCreateMod::~VolumeCreateMod
// description: destructor
//
VolumeCreateMod::~VolumeCreateMod()
{
}


void VolumeCreateMod::GetClassName(TSTR& s)
{
	s= TSTR(GetString(IDS_CREATE_MODIFIER)); 
}  

TCHAR *VolumeCreateMod::GetObjectName() 
{
	return GetString(IDS_CREATE_MODIFIER); 
}

// from StdMeshSelectModifier
const char **VolumeCreateMod::GetSelTypeNames()
{
	return pSelTypeNames;
}
const int *VolumeCreateMod::GetSelTypes()
{
	return pSelTypes;
}
int VolumeCreateMod::GetNumSelTypes()
{
	return 2;
}

//
//        name: NumSubObjTypes
// description: New sub object handling
//
int VolumeCreateMod::NumSubObjTypes() 
{
	return 2;
}
//
//        name: GetSubObjType
// description: New sub object handling
//
ISubObjType *VolumeCreateMod::GetSubObjType(int i)
{
	static GenSubObjType subObjTypes[] = {
		GenSubObjType("Vertices", "SubObjectIcons", 0),
		GenSubObjType("Faces", "SubObjectIcons", 2)
	};
	if(i == -1)
		return NULL;
	return &subObjTypes[i];
}

//
//        name: LocalDataChanged
// description: Called whenever data changes
//
void VolumeCreateMod::LocalDataChanged()
{
	StdMeshSelectModifier::LocalDataChanged();
	UpdateUI();
}

//
//        name: VolumeCreateMod::BeginEditParams
// description: Called when modifier UI appears
//
void VolumeCreateMod::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev )
{
	m_pCurrentModifier = this;

	SetupSubObjEdit(ip);
}

//
//        name: VolumeCreateMod::EndEditParams
// description: Called when modifier UI disappears
//
void VolumeCreateMod::EndEditParams( IObjParam *ip, ULONG flags,Animatable *next)
{
	if(m_hPanel)
	{
		RemoveRollup();
		m_hPanel = NULL;
	}

	ClearSubObjEdit();

	m_pCurrentModifier = NULL;
}

//
//        name: VolumeCreateMod::ActivateSubobjSel
// description: Setup rollup page and call MeshSelect setup
//          in: 
//         out: 
//
void VolumeCreateMod::ActivateSubobjSel(int level, XFormModes& modes)
{
	if(level != m_selLevel && m_hPanel)
	{
		RemoveRollup();
		m_hPanel = NULL;
	}

	if(level != 0)
	{
		if(m_hPanel == NULL)
		{
			AddRollup(level);
		}
	}
	StdMeshSelectModifier::ActivateSubobjSel(level, modes);

	if(level != 0)
		UpdateUI();

}

RefTargetHandle VolumeCreateMod::Clone(RemapDir& remap)
{
	VolumeCreateMod* pCopy = new VolumeCreateMod;	

	BaseClone(this, pCopy, remap);
	pCopy->m_selLevel = m_selLevel;
	return(pCopy);
}

void VolumeCreateMod::ModifyObject(TimeValue t, ModContext &mc, ObjectState * os, INode *node) 
{
	Interval valid = LocalValidity(t);

	assert(os->obj->IsSubClassOf(triObjectClassID));
	// Get TriObject version of ObjectState
	TriObject *tobj = (TriObject*)os->obj;
	Mesh &mesh = tobj->mesh;
	bool topologyChanged = false;

	// get modContext if it doesn't exist then create a new mod context
	StdMeshSelectData *pSelData  = static_cast<StdMeshSelectData*>(mc.localData);
	if (!pSelData) 
		mc.localData = pSelData = new StdMeshSelectData(mesh);

	// if the modContext is not valid then pass it a new mesh
	if(!pSelData->GetMesh())
	{
		pSelData->SetCache(mesh);
	}

	ModifyMesh(mesh, (StdMeshSelectData*)mc.localData);

	tobj->UpdateValidity(SELECT_CHAN_NUM,valid);
	tobj->UpdateValidity(TOPO_CHAN_NUM,valid);
	tobj->UpdateValidity(SUBSEL_TYPE_CHAN_NUM,FOREVER);
}


void GetVertSelBoundingBox(Mesh& mesh, Box3& bbox, Matrix3& mtx = Matrix3(1))
{
	int i;
	bbox.Init();

	for(i=0; i<mesh.numVerts; i++)
	{
		if(mesh.vertSel[i])
			bbox += mesh.verts[i] * mtx;
	}
}

void GetFaceSelBoundingBox(Mesh& mesh, Box3& bbox, Matrix3& mtx = Matrix3(1))
{
	int i;
	bbox.Init();

	for(i=0; i<mesh.numFaces; i++)
	{
		if(mesh.faceSel[i])
		{
			bbox += mesh.verts[mesh.faces[i].v[0]] * mtx;
			bbox += mesh.verts[mesh.faces[i].v[1]] * mtx;
			bbox += mesh.verts[mesh.faces[i].v[2]] * mtx;
		}
	}
}

void VolumeCreateMod::CreateMesh()
{
	CreateVolumes(&VolumeCreateMod::CreateMesh);
}

//
//        name: VolumeCreateMod::CreateVolumes
// description: Create collision volumes using the function given
//          in: func = pointer to member function to call to create a volume
//
void VolumeCreateMod::CreateVolumes(void (VolumeCreateMod::*func)(StdMeshSelectData *pSelData, INode *pNode))
{
#if 1
	if(m_ip == NULL)
		m_ip = (IObjParam*)GetCOREInterface();

	TimeValue t = m_ip->GetTime();
	ModContextList modList;
	INodeTab nodeList;
	int i;

	m_ip->GetModContexts(modList, nodeList);

	for(i=0; i<modList.Count(); i++)
	{
		StdMeshSelectData *pSelData = static_cast<StdMeshSelectData*>(modList[i]->localData);
		(this->*func)(pSelData, nodeList[i]);
	}

	nodeList.DisposeTemporary();
#endif
}

//
//        name: VolumeCreateMod::CreateSphere
// description: Create a collision sphere from the selected vertices in a mesh
//
void VolumeCreateMod::CreateSphere(StdMeshSelectData *pSelData, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Mesh *pMesh = pSelData->GetMesh();
	BitArray tempVertSel;
	Box3 bbox;
	Point3 centre;
	int i;
	float radius = 0.0f;
	float distance;
	Matrix3 objMat = pNode->GetObjectTM(t);
	Matrix3 localMat = pNode->GetNodeTM(t);
	Matrix3 sphereMat(1);

	if(!CanCreateSphere(pSelData))
		return;

	objMat = objMat * Inverse(localMat);
	pSelData->GetTransformedBoundBox(this, objMat, bbox);

	centre = bbox.Center()*localMat;

	tempVertSel = pSelData->GetTempVertSel(GetSelTypes()[m_selLevel]);
	// get radius of selected points
	// if vertex selection
	for(i=0; i<pMesh->numVerts; i++)
	{
		if(tempVertSel[i])
		{
			distance = Length(pMesh->verts[i]*localMat - centre);
			if(distance > radius)
				radius = distance;
		}
	}

	theHold.Begin();

	Object *pSphere = static_cast<Object *>(m_ip->CreateInstance(HELPER_CLASS_ID, COLLISION_SPHERE_CLASS_ID));
	INode *pCreatedNode = m_ip->CreateObjectNode(pSphere);

	pNode->AttachChild(pCreatedNode);

	// set volume transformation matrix
	sphereMat.SetTrans(centre);
	pCreatedNode->SetNodeTM(t, sphereMat);

	// set radius
	pSphere->GetParamBlockByID(colsphere_params)->SetValue(colsphere_radius, 0, radius);

	// set controller
	Control *pOrienControl = new StaticRotationControl;
	Control *pTMControl = pCreatedNode->GetTMController();
	pTMControl->SetRotationController(pOrienControl);

	theHold.Accept("Create Volume");

	m_ip->RedrawViews(t);
}

//
//        name: VolumeCreateMod::CreateSphereArray
// description: Create an array of spheres to fill the bounding box of the selected vertices
//
void VolumeCreateMod::CreateSphereArray(StdMeshSelectData *pSelData, INode *pNode)
{
	Box3 bbox;
	Point3 centre;
	float radius;
	TimeValue t = m_ip->GetTime();
	Matrix3 objMat = pNode->GetObjectTM(t);
	Matrix3 localMat = pNode->GetNodeTM(t);
	Matrix3 sphereMat(1);

	if(!CanCreateSphereArray(pSelData))
		return;

	localMat.NoScale();
	objMat *= Inverse(localMat);
	pSelData->GetTransformedBoundBox(this, objMat, bbox);

	int i,j,k;
	int xsize, ysize, zsize;
	Point3 offset, posn;

	// get number of spheres in each direction
	xsize = m_pXSpinner->GetIVal();
	ysize = m_pYSpinner->GetIVal();
	zsize = m_pZSpinner->GetIVal();
	radius = m_pRadiusSpinner->GetFVal();
	centre = bbox.Center()*localMat;


	// get the maximum radius so that the spheres fit in the collision box
	if(bbox.Width().x/2 < radius)
		radius = bbox.Width().x/2;
	if(bbox.Width().y/2 < radius)
		radius = bbox.Width().y/2;
	if(bbox.Width().z/2 < radius)
		radius = bbox.Width().z/2;

	// work out the distance between each sphere 
	if(xsize > 1)
		offset.x = (bbox.Width().x - 2 * radius) / (xsize-1);
	else
		offset.x =0;
	if(ysize > 1)
		offset.y = (bbox.Width().y - 2 * radius) / (ysize-1);
	else
		offset.y =0;
	if(zsize > 1)
		offset.z = (bbox.Width().z - 2 * radius) / (zsize-1);
	else
		offset.z =0;

	theHold.Begin();

	
	for(i=0; i<xsize; i++)
	{
		for(j=0; j<ysize; j++)
		{
			for(k=0; k<zsize; k++)
			{
				// create sphere and set its radius
				Object *pSphere = static_cast<Object *>(m_ip->CreateInstance(HELPER_CLASS_ID, COLLISION_SPHERE_CLASS_ID));
				pSphere->GetParamBlockByID(colsphere_params)->SetValue(colsphere_radius, 0, radius);

				// create node to attach to sphere
				INode *pCreatedNode = m_ip->CreateObjectNode(pSphere);

				pNode->AttachChild(pCreatedNode);

				// set volume transformation matrix
				posn = (bbox.pmin + Point3(radius, radius, radius) + offset * Point3(i,j,k)) * localMat;
				sphereMat.SetTrans(posn);
				pCreatedNode->SetNodeTM(t, sphereMat);

				// set controller
				Control *pOrienControl = new StaticRotationControl;
				Control *pTMControl = pCreatedNode->GetTMController();
				pTMControl->SetRotationController(pOrienControl);

			}
		}
	}

	theHold.Accept("Create Volumes");

	m_ip->RedrawViews(t);
}


//
//        name: VolumeCreateMod::CreateObjectAlignedBox, CreateWorldAlignedBox
// description: Creates a collision box aligned to either the object TM or the world TM
//
void VolumeCreateMod::CreateObjectAlignedBox(StdMeshSelectData *pSelData, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Matrix3 localMat = pNode->GetNodeTM(t);

	localMat.NoScale();

	CreateBox(pSelData, pNode, localMat);
}

void VolumeCreateMod::CreateWorldAlignedBox(StdMeshSelectData *pSelData, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Matrix3 objMat = pNode->GetObjectTM(t);

	CreateBox(pSelData, pNode, Matrix3(1));
}

void VolumeCreateMod::CreateBox(StdMeshSelectData *pSelData, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Matrix3 localMat = pNode->GetNodeTM(t);

	CreateBox(pSelData,pNode,localMat);
}

void VolumeCreateMod::CreateCapsule(StdMeshSelectData *pSelData, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Matrix3 localMat = pNode->GetNodeTM(t);

	CreateCapsule(pSelData,pNode,localMat);
}

void VolumeCreateMod::CreateCapsule(StdMeshSelectData *pSelData, INode *pNode, Matrix3& localMat)
{
	TimeValue t = m_ip->GetTime();
	Box3 bbox;
	Point3 centre, size;
	Matrix3 objMat = pNode->GetObjectTM(t);

	// return if cannot create box
	if(!CanCreateBox(pSelData))
		return;

	objMat *= Inverse(localMat);
	pSelData->GetTransformedBoundBox(this, objMat, bbox);

	centre = bbox.Center()*localMat;
	size = bbox.Width() / 2.0f;

	theHold.Begin();

	Object *pBox = static_cast<Object *>(m_ip->CreateInstance(HELPER_CLASS_ID, COLLISION_CAPSULE_CLASS_ID));
	INode *pCreatedNode = m_ip->CreateObjectNode(pBox);

	pNode->AttachChild(pCreatedNode);

	// set volume transformation matrix
	localMat.SetTrans(centre);
	pCreatedNode->SetNodeTM(t, localMat);

	// set width,length and height
	pBox->GetParamBlockByID(colbox_params)->SetValue(colcapsule_radius, 0, size.x / 2.0f);
	pBox->GetParamBlockByID(colbox_params)->SetValue(colcapsule_length, 0, size.y);

	theHold.Accept("Create Volume");

	m_ip->RedrawViews(t);
}


void VolumeCreateMod::CreateCylinder( StdMeshSelectData *pSelData, INode *pNode )
{
	TimeValue t = m_ip->GetTime();
	Matrix3 localMat = pNode->GetNodeTM(t);

	CreateCylinder(pSelData,pNode,localMat);
}

void VolumeCreateMod::CreateCylinder( StdMeshSelectData *pSelData, INode *pNode, Matrix3& localMat )
{
	TimeValue t = m_ip->GetTime();
	Box3 bbox;
	Point3 centre, size;
	Matrix3 objMat = pNode->GetObjectTM(t);

	// return if cannot create box
	if(!CanCreateBox(pSelData))
		return;

	objMat *= Inverse(localMat);
	pSelData->GetTransformedBoundBox(this, objMat, bbox);

	centre = bbox.Center()*localMat;
	size = bbox.Width() / 2.0f;

	theHold.Begin();

	Object *pBox = static_cast<Object *>(m_ip->CreateInstance(HELPER_CLASS_ID, COLLISION_CYLINDER_CLASS_ID));
	INode *pCreatedNode = m_ip->CreateObjectNode(pBox);

	pNode->AttachChild(pCreatedNode);

	// set volume transformation matrix
	localMat.SetTrans(centre);
	pCreatedNode->SetNodeTM(t, localMat);

	// set width,length and height
	pBox->GetParamBlockByID(colbox_params)->SetValue(colcylinder_radius, 0, size.x / 2.0f);
	pBox->GetParamBlockByID(colbox_params)->SetValue(colcylinder_length, 0, size.y);

	theHold.Accept("Create Volume");

	m_ip->RedrawViews(t);
}

void VolumeCreateMod::CreateBox(StdMeshSelectData *pSelData, INode *pNode, Matrix3& localMat)
{
	TimeValue t = m_ip->GetTime();
	Box3 bbox;
	Point3 centre, size;
	Matrix3 objMat = pNode->GetObjectTM(t);

	// return if cannot create box
	if(!CanCreateBox(pSelData))
		return;

	objMat *= Inverse(localMat);
	pSelData->GetTransformedBoundBox(this, objMat, bbox);

	centre = bbox.Center()*localMat;
	size = bbox.Width() / 2.0f;

	theHold.Begin();

	Object *pBox = static_cast<Object *>(m_ip->CreateInstance(HELPER_CLASS_ID, COLLISION_BOX_CLASS_ID));
	INode *pCreatedNode = m_ip->CreateObjectNode(pBox);

	pNode->AttachChild(pCreatedNode);

	// set volume transformation matrix
	localMat.SetTrans(centre);
	pCreatedNode->SetNodeTM(t, localMat);

	// set width,length and height
	pBox->GetParamBlockByID(colbox_params)->SetValue(colbox_length, 0, size.x);
	pBox->GetParamBlockByID(colbox_params)->SetValue(colbox_width, 0, size.y);
	pBox->GetParamBlockByID(colbox_params)->SetValue(colbox_height, 0, size.z);

	// set controller
//	Control *pOrienControl = new StaticRotationControl;
//	Control *pTMControl = pCreatedNode->GetTMController();
//	pTMControl->SetRotationController(pOrienControl);

	theHold.Accept("Create Volume");

	m_ip->RedrawViews(t);
}

void VolumeCreateMod::CreateShad(StdMeshSelectData *pSelData, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Matrix3 nodeMat = pNode->GetNodeTM(t);
	Matrix3 objMat = pNode->GetObjectTM(t);
	int i;

	// return if cannot create box
	if(!CanCreateMesh(pSelData))
		return;

	theHold.Begin();

	CollisionMesh *pColMesh = static_cast<CollisionMesh *>(m_ip->CreateInstance(HELPER_CLASS_ID, COLLISION_MESHSHAD_CLASS_ID));
	TriObject *pNewTri = static_cast<TriObject *>(m_ip->CreateInstance(GEOMOBJECT_CLASS_ID, Class_ID(TRIOBJ_CLASS_ID, 0)));
	BitArray faceSel = pSelData->GetFaceSel();

	pColMesh->ReplaceReference(COLMESH_REF_TRIOBJ, pNewTri);

	Mesh& colMesh = static_cast<TriObject *>(pColMesh->GetReference(COLMESH_REF_TRIOBJ))->mesh;

	colMesh.DeepCopy(pSelData->GetMesh(), GEOM_CHANNEL|TOPO_CHANNEL);

	// transform vertices
	objMat *= Inverse(nodeMat);
	for(i=0; i<colMesh.numVerts; i++)
		colMesh.verts[i] = colMesh.verts[i] * objMat;

	// remove unselected faces and isolated vertices
	for(i=0; i<colMesh.numFaces; i++)
	{
		if(!faceSel[i])
			colMesh.faces[i].flags |= FACE_WORK;
	}
	colMesh.DeleteFlaggedFaces();
	colMesh.DeleteIsoVerts();

	// update arrays
	colMesh.InvalidateGeomCache();
	colMesh.buildBoundingBox();
	colMesh.buildNormals();

	// create node once collision mesh has been completely built
	INode *pCreatedNode = m_ip->CreateObjectNode(pColMesh);
	pNode->AttachChild(pCreatedNode);
	pCreatedNode->SetNodeTM(t, nodeMat);

	pCreatedNode->SetObjectRef(pNewTri);
	ConvertToShadMesh(pCreatedNode,m_ip);

	theHold.Accept("Create Volume");

	m_ip->RedrawViews(t);
}

//
//        name: VolumeCreateMod::CreateMesh
// description: Create a collision mesh object
//
void VolumeCreateMod::CreateMesh(StdMeshSelectData *pSelData, INode *pNode)
{
#if 1

	TimeValue t = m_ip->GetTime();
	Matrix3 nodeMat = pNode->GetNodeTM(t);
	Matrix3 objMat = pNode->GetObjectTM(t);
	int i;

	// return if cannot create box
	if(!CanCreateMesh(pSelData))
		return;

	theHold.Begin();

	CollisionMesh *pColMesh = static_cast<CollisionMesh *>(m_ip->CreateInstance(HELPER_CLASS_ID, COLLISION_MESH_CLASS_ID));
	TriObject *pNewTri = static_cast<TriObject *>(m_ip->CreateInstance(GEOMOBJECT_CLASS_ID, Class_ID(TRIOBJ_CLASS_ID, 0)));
	BitArray faceSel = pSelData->GetFaceSel();

	pColMesh->ReplaceReference(COLMESH_REF_TRIOBJ, pNewTri);

	Mesh& colMesh = static_cast<TriObject *>(pColMesh->GetReference(COLMESH_REF_TRIOBJ))->mesh;

	colMesh.DeepCopy(pSelData->GetMesh(), GEOM_CHANNEL|TOPO_CHANNEL);

	// transform vertices
	objMat *= Inverse(nodeMat);
	for(i=0; i<colMesh.numVerts; i++)
		colMesh.verts[i] = colMesh.verts[i] * objMat;

	// remove unselected faces and isolated vertices
	for(i=0; i<colMesh.numFaces; i++)
	{
		if(!faceSel[i])
			colMesh.faces[i].flags |= FACE_WORK;
	}
	colMesh.DeleteFlaggedFaces();
	colMesh.DeleteIsoVerts();

	// update arrays
	colMesh.InvalidateGeomCache();
	colMesh.buildBoundingBox();
	colMesh.buildNormals();

	// create node once collision mesh has been completely built
	INode *pCreatedNode = m_ip->CreateObjectNode(pColMesh);
	pNode->AttachChild(pCreatedNode);
	pCreatedNode->SetNodeTM(t, nodeMat);

	pCreatedNode->SetObjectRef(pNewTri);
	ConvertToColMesh(pCreatedNode,m_ip);

	theHold.Accept("Create Volume");

	m_ip->RedrawViews(t);
#endif
}

//
//        name: VolumeCreateMod::CanCreateVolumes
// description: Return if it is possible to create collision volumes using a given function
//
bool VolumeCreateMod::CanCreateVolumes(bool (VolumeCreateMod::*func)(StdMeshSelectData *pSelData))
{
	TimeValue t = m_ip->GetTime();
	ModContextList modList;
	INodeTab nodeList;
	int i;

	m_ip->GetModContexts(modList, nodeList);

	for(i=0; i<modList.Count(); i++)
	{
		StdMeshSelectData *pSelData = static_cast<StdMeshSelectData*>(modList[i]->localData);
		if(pSelData)
		{
			if((this->*func)(pSelData))
			{
				nodeList.DisposeTemporary();
				return true;
			}
		}
	}
	nodeList.DisposeTemporary();
	return false;
}

//
//        name: CanCreateSphere,CanCreateSphereArray,CanCreateBox
// description: Return if it is possible to create a collision volume from the selected vertices in a mesh
//
bool VolumeCreateMod::CanCreateSphere(StdMeshSelectData *pSelData)
{
	Box3 bbox;
	Point3 width;

	pSelData->GetLocalBoundBox(this, bbox);

	width = bbox.Width();
	if((width.x > 0 || width.y > 0 || width.z > 0) && pSelData->GetMesh())
		return true;
	return false;
}

bool VolumeCreateMod::CanCreateSphereArray(StdMeshSelectData *pSelData)
{
	return CanCreateBox(pSelData);
}

bool VolumeCreateMod::CanCreateBox(StdMeshSelectData *pSelData)
{
	Box3 bbox;
	Point3 width;

	pSelData->GetLocalBoundBox(this, bbox);

	width = bbox.Width();
	if(width.x > 0 && width.y > 0 && width.z > 0 && pSelData->GetMesh())
		return true;
	return false;
}
bool VolumeCreateMod::CanCreateMesh(StdMeshSelectData *pSelData)
{
	if(GetSelTypes()[m_selLevel] == IMESHSEL_FACE && pSelData->GetFaceSel().NumberSet() > 0 && pSelData->GetMesh())
		return true;
	return false;
}

//
//        name: VolumeCreateMod::AddRollup
// description: Add the modifier rollup page
//
void VolumeCreateMod::AddRollup(int level)
{
	int meshSelLevel = GetSelTypes()[level];
	// vtx
	if(meshSelLevel == IMESHSEL_VERTEX)
	{
		m_hPanel = m_ip->AddRollupPage(
			hInstance, 
			MAKEINTRESOURCE(IDD_COL_MODIFIER_VTX_PANEL),
			CreateVolumeProc,
			_T("Collision Volume Creation"),
			(LPARAM)this);
	}
	// face
	else if(meshSelLevel == IMESHSEL_FACE)
	{
		m_hPanel = m_ip->AddRollupPage(
			hInstance, 
			MAKEINTRESOURCE(IDD_COL_MODIFIER_FACE_PANEL),
			CreateVolumeProc,
			_T("Collision Volume Creation"),
			(LPARAM)this);
	}

	SetDlgItemText(m_hPanel, IDC_VERSION, TSTR("Version ") + GetString(IDS_VERSION));
	
	m_pSphereButton = GetICustButton(GetDlgItem(m_hPanel, IDC_SPHERE_BUTTON));
	m_pSphereArrayButton = GetICustButton(GetDlgItem(m_hPanel, IDC_SPHEREARRAY_BUTTON));
	m_pBoxButton = GetICustButton(GetDlgItem(m_hPanel, IDC_BOX_BUTTON));
	m_pXSpinner = SetupIntSpinner(m_hPanel, IDC_XSIZE_SPIN, IDC_XSIZE_EDIT, 1, 100, m_xsize);
	m_pYSpinner = SetupIntSpinner(m_hPanel, IDC_YSIZE_SPIN, IDC_YSIZE_EDIT, 1, 100, m_ysize);
	m_pZSpinner = SetupIntSpinner(m_hPanel, IDC_ZSIZE_SPIN, IDC_ZSIZE_EDIT, 1, 100, m_zsize);
	m_pRadiusSpinner = SetupFloatSpinner(m_hPanel, IDC_RADIUS_SPIN, IDC_RADIUS_EDIT, 0.01f, 10000.0f, m_radius);

	if(meshSelLevel == 2)
	{
		m_pMeshButton = GetICustButton(GetDlgItem(m_hPanel, IDC_MESH_BUTTON));

		LoadFaceToolbar();

		m_pToolbar = GetICustToolbar(GetDlgItem(m_hPanel, IDC_FACE_TOOLBAR));
		m_pToolbar->SetImage(m_hImageList);
		m_pToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON, 0,0,0,0, 16,15, 24,23,IDC_SELSINGLE));
		m_pToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON, 1,1,1,1, 16,15, 24,23,IDC_SELPOLY));
		m_pToolbar->AddTool(ToolButtonItem(CTB_CHECKBUTTON, 2,2,2,2, 16,15, 24,23,IDC_SELELEMENT));

		m_pSingleButton = m_pToolbar->GetICustButton(IDC_SELSINGLE);
		m_pPolyButton = m_pToolbar->GetICustButton(IDC_SELPOLY);
		m_pElementButton = m_pToolbar->GetICustButton(IDC_SELELEMENT);

		SetFaceSelection(GetFaceSelLevel());
	}
	else
		m_pMeshButton = NULL;

}

//
//        name: VolumeCreateMod::RemoveRollup
// description: Remove the modifier rollup page
//
void VolumeCreateMod::RemoveRollup()
{
	m_xsize = m_pXSpinner->GetIVal();
	m_ysize = m_pYSpinner->GetIVal();
	m_zsize = m_pZSpinner->GetIVal();

	ReleaseICustButton(m_pSphereButton);
	ReleaseICustButton(m_pSphereArrayButton);
	ReleaseICustButton(m_pBoxButton);
	ReleaseISpinner(m_pXSpinner);
	ReleaseISpinner(m_pYSpinner);
	ReleaseISpinner(m_pZSpinner);
	ReleaseISpinner(m_pRadiusSpinner);
	if(m_pMeshButton)
	{
		ReleaseICustButton(m_pMeshButton);
		ReleaseICustButton(m_pSingleButton);
		ReleaseICustButton(m_pPolyButton);
		ReleaseICustButton(m_pElementButton);
		ReleaseICustToolbar(m_pToolbar);
	}

	m_ip->DeleteRollupPage(m_hPanel);
}

//
//        name: VolumeCreateMod::UpdateUI
// description: Update the button states
//
void VolumeCreateMod::UpdateUI()
{
	assert(m_pCurrentModifier == this);
	
	if(CanCreateVolumes(&VolumeCreateMod::CanCreateSphere))
		m_pSphereButton->Enable();
	else
		m_pSphereButton->Disable();
	
	if(CanCreateVolumes(&VolumeCreateMod::CanCreateSphereArray))
		m_pSphereArrayButton->Enable();
	else
		m_pSphereArrayButton->Disable();

	if(CanCreateVolumes(&VolumeCreateMod::CanCreateBox))
		m_pBoxButton->Enable();
	else
		m_pBoxButton->Disable();

	if(m_pMeshButton)
	{
		if(CanCreateVolumes(&VolumeCreateMod::CanCreateMesh))
			m_pMeshButton->Enable();
		else
			m_pMeshButton->Disable();
	}
}

//
//        name: VolumeCreateMod::LoadFaceToolbar
// description: Loads the imagelist for the face selection toolbar
//
void VolumeCreateMod::LoadFaceToolbar()
{
	if(m_hImageList == NULL)
	{
		HBITMAP hBitmap = LoadBitmap(hInstance,MAKEINTRESOURCE(IDB_FACESEL));
		HBITMAP hMask = LoadBitmap(hInstance,MAKEINTRESOURCE(IDB_FACESEL_MASK));
		
		m_hImageList = ImageList_Create(16,15,TRUE, 3, 0);
		ImageList_Add(m_hImageList,hBitmap,hMask);
		DeleteObject(hBitmap);
		DeleteObject(hMask);
	}
}

//
//        name: VolumeCreateMod::SetFaceSelection
// description: Set the face selection type
//          in: type = selection type (single poly,face,element)
//
void VolumeCreateMod::SetFaceSelection(int selection)
{	
	int old = GetFaceSelLevel();
	
	SetFaceSelLevel(selection);

	switch (selection) {
	case FACESEL_SINGLE:
		m_pSingleButton->SetCheck(TRUE);
		m_pPolyButton->SetCheck(FALSE);
		m_pElementButton->SetCheck(FALSE);
		break;
	case FACESEL_POLY:
		m_pSingleButton->SetCheck(FALSE);
		m_pPolyButton->SetCheck(TRUE);
		m_pElementButton->SetCheck(FALSE);
		break;
	case FACESEL_ELEMENT:
		m_pSingleButton->SetCheck(FALSE);
		m_pPolyButton->SetCheck(FALSE);
		m_pElementButton->SetCheck(TRUE);
		break;
	}
	
	if (old != selection) {
		NotifyDependents(FOREVER, PART_DISPLAY, REFMSG_CHANGE);
		UpdateWindow(m_hPanel);
		m_ip->RedrawViews(m_ip->GetTime(),REDRAW_NORMAL);
	}
}



// --- Windows callback -------------------------------------------------------------------------------------------

INT_PTR CALLBACK CreateVolumeProc(HWND hWnd, unsigned int message, WPARAM wParam, LPARAM lParam)
{
	static VolumeCreateMod *pModifier;
	if(pModifier == NULL && message != WM_INITDIALOG)
		return TRUE;

	switch(message)
	{
	case WM_INITDIALOG:
		pModifier = (VolumeCreateMod *)lParam;
		break;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_SELSINGLE:
			pModifier->SetFaceSelection(FACESEL_SINGLE);
			break;
		case IDC_SELPOLY:
			pModifier->SetFaceSelection(FACESEL_POLY);
			break;
		case IDC_SELELEMENT:
			pModifier->SetFaceSelection(FACESEL_ELEMENT);
			break;
		case IDC_SPHERE_BUTTON:
			pModifier->CreateVolumes(&VolumeCreateMod::CreateSphere);
			break;
		case IDC_SPHEREARRAY_BUTTON:
			pModifier->CreateVolumes(&VolumeCreateMod::CreateSphereArray);
			break;
		case IDC_BOX_BUTTON:
			pModifier->CreateVolumes(&VolumeCreateMod::CreateObjectAlignedBox);
			break;
		case IDC_MESH_BUTTON:
			pModifier->CreateVolumes(&VolumeCreateMod::CreateMesh);
			break;
		default:
			break;
		}
		break;

	case WM_CLOSE:
		break;

	default:
		return FALSE;
	}
	return TRUE;

}
