/**********************************************************************
 *<
	FILE: CollisionCapsule.cpp

	DESCRIPTION:	Appwizard generated plugin

	CREATED BY: 

	HISTORY: 

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/

// C header
#include <float.h>
// My headers
#include "collisionCapsule.h"
#include "CollisionControllers.h"
#include "resource.h"
#include <simpobj.h>

#define NUM_CIRCLE_SEGS 24

static CollisionCapsuleClassDesc theCollisionCapsuleDesc;
ClassDesc* GetCollisionCapsuleDesc() {return &theCollisionCapsuleDesc; }

static ParamBlockDescID descVer1[] = 
{
	{ TYPE_FLOAT, NULL, FALSE, 0}, //radius controlled by a spinner
	{ TYPE_INT,   NULL, FALSE, 1},	  //Type of sphere
};

static ParamVersionDesc oldVersions[] =
{
	ParamVersionDesc(descVer1, 2, 1)
};

//#define AUTO_CREATE

//
// Max3 paramblock2 version
//
ParamBlockDesc2 colCapsuleDesc (colcapsule_params, _T("ColCapsule parameters"), IDS_COLCAPSULE, &theCollisionCapsuleDesc, P_AUTO_CONSTRUCT | P_AUTO_UI, 0,
						 IDD_COL_CAPSULE_PANEL, IDS_PARAMS, 0, 0, NULL,
						 colcapsule_radius, _T("Radius"), TYPE_WORLD, 0, IDS_RADIUS,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_RADIUS_EDIT, IDC_RADIUS_SPIN, 0.01f,
							end,
						 colcapsule_length, _T("Length"), TYPE_WORLD, 0, IDS_LENGTH,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_LENGTH_EDIT, IDC_LENGTH_SPIN, 0.01f,
							end,
						end);

//
//        name: CollisionSphereClassDesc::OkToCreate
// description: Return if it is OK to create a collision sphere i.e there is one object selected
//
BOOL CollisionCapsuleClassDesc::OkToCreate(Interface *ip)
{
	//return theSelectCommandMode.OkToCreate(ip);
	return TRUE;
}

int CollisionCapsuleClassDesc::BeginCreate(Interface *ip)
{
	//assert(theSelectCommandMode.OkToCreate(ip));

	theSelectCommandMode.SetObjectClassIDs(SuperClassID(), ClassID());
	theSelectCommandMode.Begin((IObjParam*)ip);
	ip->PushCommandMode(&theSelectCommandMode);

	return TRUE;
}

int CollisionCapsuleClassDesc::EndCreate(Interface *ip)
{
	theSelectCommandMode.End((IObjParam*)ip);
	ip->RemoveMode(&theSelectCommandMode);

	return TRUE;
}

const TCHAR* CollisionCapsuleClassDesc::ClassName() 
{
	return GetString(IDS_COLCAPSULE);
}

const TCHAR* CollisionCapsuleClassDesc::Category()
{
	return GetString(IDS_CATEGORY);
}	

/*****************************************************************************
 ** NAME       || CollisionCapsule()
 ** PURPOSE    || Constructor. Set up the pblock.
 ** PARAMETERS || 
 ** RETURNS    || 
 *****************************************************************************/
CollisionCapsule::CollisionCapsule():
	pBlock2(NULL)
{
	theCollisionCapsuleDesc.MakeAutoParamBlocks(this);
}

/*****************************************************************************
 ** NAME       || Clone()
 ** PURPOSE    || Clones the object
 ** PARAMETERS || 
 ** RETURNS    || 
 *****************************************************************************/
RefTargetHandle CollisionCapsule::Clone(RemapDir& remap) 
{
	CollisionCapsule* pClone = new CollisionCapsule();

	BaseClone(this, pClone, remap);
	pClone->ReplaceReference(PBLOCK_REF_NUM, remap.CloneRef(pBlock2));
	return pClone;
}

void CollisionCapsule::InitNodeName(TSTR& s)
{
	s = GetString(IDS_COLCAPSULE_NODE);
}

TCHAR *CollisionCapsule::GetObjectName() 
{
	return GetString(IDS_COLCAPSULE);
}

void CollisionCapsule::GetClassName(TSTR& s)
{
	s = GetString(IDS_COLCAPSULE);
}

/*****************************************************************************
 ** NAME       || Display()
 ** PURPOSE    || Display the sphere
 ** PARAMETERS || The time, the inode pointer, and some other stuff
 ** RETURNS    || An int (zero)
 *****************************************************************************/
int CollisionCapsule::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);

	SetHelperColour(inode, gw);

	DrawCircles(gw, inode, t);

	return 0;
}

/*****************************************************************************
 ** NAME       || drawCircles()
 ** PURPOSE    || Draw in the circles
 ** PARAMETERS || A GraphicsWindow pointer
 ** RETURNS    || Nowt.
 *****************************************************************************/
void CollisionCapsule::DrawCircles(GraphicsWindow *gw, INode *inode, TimeValue t)
{
	if(!m_isDisplayed)
		return;

	Interval ivalid;

	float radius;
	float length;

	pBlock2->GetValue(colcapsule_radius, 0, radius, ivalid);
	pBlock2->GetValue(colcapsule_length, 0, length, ivalid);

	if(m_displayMode == 0)
	{
		DrawLineProc lineProc(gw);
		float u,v;
		Point3 pt[NUM_CIRCLE_SEGS + 1];
		int i,j;

		float oneOverRootTwo = (float(1))/(float(sqrt(2.0f)));
		float sqrtRad = radius * oneOverRootTwo;
		float multiplier = float(TWOPI) / float(NUM_CIRCLE_SEGS);

		// length lines

		for (i = 0; i <= NUM_CIRCLE_SEGS; i++) 
		{
			u = float(i) * multiplier;
			pt[0].x = (float)cos(u) * radius;
			pt[0].y = length / 2.0f;
			pt[0].z = (float)sin(u) * radius;

			pt[1].x = (float)cos(u) * radius;
			pt[1].y = length / -2.0f;
			pt[1].z = (float)sin(u) * radius;

			lineProc.proc(pt,2);
		}

		// top hemisphere
		for(j = 0;j < NUM_CIRCLE_SEGS;j++)
		{
			v = float(j) * multiplier;

			for (i = 0; i <= NUM_CIRCLE_SEGS; i++) 
			{
				u = float(i) * multiplier / 2.0f;

				float sep = (float)cos(u) * radius;

				pt[i].x = (float)sin(v) * sep;
				pt[i].y = (length / 2.0f) + (float)sin(u) * radius;
				pt[i].z = (float)cos(v) * sep;
			}

			lineProc.proc(pt,NUM_CIRCLE_SEGS+1);
		}

		// bottom hemisphere
		for(j = 0;j < NUM_CIRCLE_SEGS;j++)
		{
			v = float(j) * multiplier;

			for (i = 0; i <= NUM_CIRCLE_SEGS; i++) 
			{
				u = (float(TWOPI) / 2.0f) + float(i) * (multiplier / 2.0f);

				float sep = (float)cos(u) * radius;

				pt[i].x = (float)sin(v) * sep;
				pt[i].y = (length / -2.0f) + (float)sin(u) * radius;
				pt[i].z = (float)cos(v) * sep;
			}

			lineProc.proc(pt,NUM_CIRCLE_SEGS+1);
		}

	}
	else
	{
#define PB_RADIUS	0
#define PB_SEGS		1

		if(!inode)
		{
			return;
		}

		SimpleObject* pSimple = (SimpleObject*)CreateInstance(GEOMOBJECT_CLASS_ID,Class_ID(SPHERE_CLASS_ID,0));

		if(!pSimple)
		{
			return;
		}

		pSimple->pblock->SetValue(PB_RADIUS, 0, radius);	
		pSimple->pblock->SetValue(PB_SEGS, 0, NUM_CIRCLE_SEGS);

		// Build the mesh but we are going to overwrite the mesh below. Saves us from creating our own SimpleObject type.
		pSimple->BuildMesh(t);
		
		BOOL needDelete;

		class NullView : public View 
		{
		public:

			Point2 ViewToScreen(Point3 p) { return Point2(p.x,p.y); }
			NullView() 
			{
				worldToView.IdentityMatrix();
				screenW=640.0f; screenH = 480.0f;
			}
		};

		NullView nullView;

		// Draw our new version of the capsule
		DrawSolid(pSimple->mesh, NUM_CIRCLE_SEGS, 1, 1, 0, radius, length, 0, 0, 0, FALSE);

		// Transform the shape we just created so it matches our wireframe shape.
		Matrix3 mat = inode->GetObjectTM(t);
		mat.PreRotateX(DegToRad(90));
		float r2 = 2.0f * radius;
		mat.PreTranslate(Point3(0.0f, 0.0f, ( -(length + r2) / 2 ) ));
		gw->setTransform(mat);

		Mesh* pMesh = pSimple->GetRenderMesh(t,inode,nullView,needDelete);
		pMesh->render(gw,inode->Mtls(),NULL,COMP_ALL);

		if(needDelete)
		{
			delete pMesh;
		}

		delete pSimple;
	}
}

#define ALLF  4
void AddFace(Face *f,int a,int b,int c,int evis,int smooth_group)
{
	f[0].setSmGroup(smooth_group);
	f[0].setMatID((MtlID)0);     /*default */
	if (evis==0) f[0].setEdgeVisFlags(1,1,0);
	else if (evis==1) f[0].setEdgeVisFlags(0,1,1);
	else if (evis==2) f[0].setEdgeVisFlags(0,0,1);
	else if (evis==ALLF) f[0].setEdgeVisFlags(1,1,1);
	else f[0].setEdgeVisFlags(1,0,1); 
	f[0].setVerts(a,b,c);
}

/*****************************************************************************
 ** NAME       || DrawSolid()
 ** PURPOSE    || Shamelessly stolen from the Max Extended Primitives "Capsule" creation code.
 ** PARAMETERS || Shit loads of stuff....
 ** RETURNS    || Nothin'
 *****************************************************************************/
void CollisionCapsule::DrawSolid(Mesh &mesh,
					int segs, int smooth, int llsegs, int doPie, float radius1,
					float height,float pie1, float pie2, int genUVs, BOOL usePhysUVs)
{
	float r2 = 2.0f * radius1;
	height += (height < 0.0f ? -r2:r2);

	Point3 p;
	BOOL minush=(height<0.0f);
	if (minush) height=-height;
	int ix,jx,ic = 1;
	int nf=0,nv=0, lsegs,VertexPerLevel,capsegs=(int)(segs/2.0f),csegs=0;
	float delta,ang;	
	float totalPie, startAng = 0.0f;	

	if (doPie) doPie = 1;
	else doPie = 0; 

	lsegs = llsegs-1 + 2*capsegs;

	// Make pie2 < pie1 and pie1-pie2 < TWOPI
	while (pie1 < pie2) pie1 += TWOPI;
	while (pie1 > pie2+TWOPI) pie1 -= TWOPI;
	if (pie1==pie2) totalPie = TWOPI;
	else totalPie = pie1-pie2;		
	int nfaces,ntverts,levels=csegs*2+(llsegs-1);
	int capv=segs,sideedge=capsegs+csegs,*edgelstr,*edgelstl,totlevels;
	// capv=vertex in one cap layer
	totlevels=levels+capsegs*2+2;
	int	tvinslice=totlevels+totlevels-2;
	if (doPie) {
		delta    = totalPie/(float)(segs);
		startAng = pie2; capv++;
		VertexPerLevel=segs+2;
		nfaces=2*segs*(levels+1)+(sideedge+llsegs)*4;
		ntverts=tvinslice+2*(segs+1);
		// 2 faces between every 2 vertices, with 2 ends, except in central cap)
	} else {
		delta = (float)2.0*PI/(float)segs;
		VertexPerLevel=segs;
		nfaces=2*segs*(levels+1);
		ntverts=2*(segs+1)+llsegs-1;
	}

	edgelstl=new int[totlevels];
	edgelstr=new int[totlevels];
	int lastlevel=totlevels-1,dcapv=capv-1,dvertper=VertexPerLevel-1;
	edgelstr[0]=0;edgelstl[0]=0;
	edgelstr[1]=1;
	edgelstl[1]=capv;
	int i;
	for (i=2;i<=sideedge;i++)
	{ edgelstr[i]=edgelstr[i-1]+capv;
	edgelstl[i]=edgelstr[i]+dcapv;
	}
	while ((i<lastlevel)&&(i<=totlevels-sideedge))
	{ edgelstr[i]=edgelstr[i-1]+VertexPerLevel;
	edgelstl[i]=edgelstr[i]+dcapv;
	i++;
	}
	while (i<lastlevel)
	{ edgelstr[i]=edgelstr[i-1]+capv;
	edgelstl[i]=edgelstr[i]+dcapv;
	i++;
	}
	edgelstl[lastlevel]=(edgelstr[lastlevel]=edgelstl[i-1]+((doPie &&(sideedge==1))?2:1));
	int nverts=edgelstl[lastlevel]+1;

	nfaces+=2*segs*(2*capsegs-1);
	if (height<0) delta = -delta;

	mesh.setNumVerts(nverts);
	mesh.setNumFaces(nfaces);
	mesh.setSmoothFlags(smooth != 0);
	if (genUVs) 
	{ ntverts+=nverts;
	mesh.setNumTVerts(ntverts);
	mesh.setNumTVFaces(nfaces);
	} 
	else 
	{ mesh.setNumTVerts(0);
	mesh.setNumTVFaces(0);
	}

	mesh.setSmoothFlags((smooth != 0) | ((doPie != 0) << 1));
	// bottom vertex 
	mesh.setVert(nv, Point3(0.0f,0.0f,height));
	mesh.setVert(nverts-1, Point3(0.0f,0.0f,0.0f));		
	float ru,cang,sang,botz;
	//deltacsegs=PI/(2.0f*csegs);
	int msegs=segs,deltaend=nverts-capv-1;
	// Bottom cap vertices
	ang = startAng;	 
	if (!doPie) msegs--;
	float rincr=PI/(2.0f*capsegs),aincr;
	for (jx = 0; jx<=msegs; jx++) 
	{ cang=(float)cos(ang);
	sang=(float)sin(ang);
	for(ix=1; ix<=sideedge; ix++)
	{	ru=radius1*(float)sin(aincr=(rincr*(float)ix));
	if (jx==0)
	{ p.z = height-radius1*(1.0f-(float)cos(aincr));
	} else p.z=mesh.verts[edgelstr[ix]].z;
	botz=height-p.z;
	if ((doPie)&&((jx==0)&&(ix==sideedge)))
	{ mesh.setVert(edgelstl[ix]+1,Point3(0.0f,0.0f,p.z));
	mesh.setVert(edgelstl[lastlevel-ix]+1,Point3(0.0f,0.0f,botz));
	}
	p.x = cang*ru;
	p.y = sang*ru;	
	mesh.setVert(edgelstr[ix]+jx, p);
	mesh.setVert(edgelstr[lastlevel-ix]+jx,Point3(p.x,p.y,botz));
	}
	ang += delta;
	}
	//top layer done, now reflect sides down 
	int sidevs,startv=edgelstr[sideedge],deltav;				
	if (llsegs>1)
	{ float topd=mesh.verts[startv].z,sincr=(height-2.0f*(height-topd))/llsegs;
	for (sidevs=0;sidevs<VertexPerLevel;sidevs++)
	{ p=mesh.verts[startv];
	deltav=VertexPerLevel;
	for (ic=1;ic<llsegs;ic++)
	{ p.z =topd-sincr*ic;
	mesh.setVert(startv+deltav, p);
	deltav+=VertexPerLevel;
	}
	startv++;
	}
	}
	int lasttvl=0,lasttvr=0;
	if (genUVs)
	{ int tvcount=0,nexttv;
	float udenom= usePhysUVs ? 1.0f : 2.0f*radius1;
	for (i=0;i<=sideedge;i++)
	{	nexttv=edgelstr[i];
	while (nexttv<=edgelstl[i])
	{ mesh.setTVert(tvcount++,(radius1+mesh.verts[nexttv].x)/udenom,(radius1+mesh.verts[nexttv].y)/udenom,0.0f);
	nexttv++;
	}
	}
	int iseg,hcount=0,lastedge=(sideedge==1?lastlevel-2:lastlevel-1);
	float hlevel;
	float heightScale = usePhysUVs ? height-2.0f*radius1 : 1.0f;
	float radiusScale = usePhysUVs ? ((float)2.0f * PI * radius1) : 1.0f;
	if (doPie)
		radiusScale = radiusScale * ((float) fabs(totalPie) / (2.0f*PI));

	for (i=sideedge;i<=lastlevel-sideedge;i++)
	{ hlevel=1.0f-hcount++/(float)llsegs;
	for (iseg=0;iseg<=segs;iseg++)
		mesh.setTVert(tvcount++,radiusScale*((float)iseg/segs),heightScale*hlevel,0.0f);
	}
	i--;
	while (i<=lastlevel)
	{	nexttv=edgelstr[i];
	while (nexttv<=edgelstl[i])
	{ mesh.setTVert(tvcount++,(radius1+mesh.verts[nexttv].x)/udenom,(radius1+mesh.verts[nexttv].y)/udenom,0.0f);
	nexttv++;
	}
	i++;
	}
	if (doPie)
	{ lasttvl=lasttvr=tvcount;
	float u,v;
	mesh.setTVert(tvcount++,0.0f,usePhysUVs ? height : 1.0f,0.0f);
	for (i=sideedge;i<=sideedge+llsegs;i++)
	{ mesh.setTVert(tvcount++,0.0f,mesh.verts[edgelstl[i]].z/(usePhysUVs ? 1.0f : height) ,0.0f);
	}
	mesh.setTVert(tvcount++,0.0f,0.0f,0.0f);
	for (i=1;i<lastlevel;i++)
	{ u=(float)sqrt(mesh.verts[edgelstl[i]].x*mesh.verts[edgelstl[i]].x+mesh.verts[edgelstl[i]].y*mesh.verts[edgelstl[i]].y)/(usePhysUVs ? 1.0f : radius1);
	v=mesh.verts[edgelstl[i]].z/(usePhysUVs ? 1.0f : height);
	mesh.setTVert(tvcount++,u,v,0.0f);
	mesh.setTVert(tvcount++,u,v,0.0f);
	}
	}
	}	
	int lvert=(doPie?segs+1:segs);
	int t0,t1,b0,b1,tvt0=0,tvt1=0,tvb0=1,tvb1=2,fc=0,smoothgr=(smooth?4:0),vseg=segs+1;
	int tvcount=0,lowerside=lastlevel-sideedge,onside=0;
	BOOL ok,wrap;
	// Now make faces ---
	for (int clevel=0;clevel<lastlevel-1;clevel++)
	{ t1=(t0=edgelstr[clevel])+1;
	b1=(b0=edgelstr[clevel+1])+1;
	ok=!doPie; wrap=FALSE;
	if ((clevel>0)&&((doPie)||(onside==1))) {tvt0++;tvt1++;tvb0++,tvb1++;}
	if (clevel==1) {tvt0=1;tvt1=2;}
	if (clevel==sideedge)
	{tvt1+=lvert;tvt0+=lvert;tvb0+=vseg;tvb1+=vseg;onside++;}
	else if (clevel==lowerside)
	{tvt1+=vseg;tvt0+=vseg;tvb0+=lvert;tvb1+=lvert;onside++;}
	while ((b0<edgelstl[clevel+1])||ok)
	{ if (b1==edgelstr[clevel+2]) 
	{ b1=edgelstr[clevel+1]; 
	t1=edgelstr[clevel];
	ok=FALSE;wrap=(onside!=1);}
	if (smooth) smoothgr=4;
	if (genUVs) mesh.tvFace[fc].setTVerts(tvt0,tvb0,(wrap?tvb1-segs:tvb1));
	AddFace(&mesh.faces[fc++],t0,b0,b1,0,smoothgr);
	if (clevel>0)
	{ if (genUVs)
	{ if (wrap) mesh.tvFace[fc].setTVerts(tvt0++,tvb1-segs,tvt1-segs);
	else mesh.tvFace[fc].setTVerts(tvt0++,tvb1,tvt1);
	tvt1++;
	}
	AddFace(&mesh.faces[fc++],t0,b1,t1,1,smoothgr);
	t0++;t1++;
	}
	b0++;b1++;tvb0++,tvb1++;
	}
	}
	smoothgr=(smooth?4:0);
	t1=(t0=edgelstr[lastlevel-1])+1;b0=edgelstr[lastlevel];
	int lastpt=(doPie?lastlevel-1:lastlevel);
	if ((doPie)||(onside==1)) {tvt0++;tvt1++;tvb0++,tvb1++;}
	if (sideedge==1) {tvt1+=vseg;tvt0+=vseg;tvb0+=lvert;tvb1+=lvert;onside++;}
	while (t0<edgelstl[lastpt])
	{ if ((!doPie)&&(t1==edgelstr[lastlevel]))
	{ t1=edgelstr[lastlevel-1];tvt1-=segs;}
	if (genUVs) mesh.tvFace[fc].setTVerts(tvt0++,tvb0,tvt1++);
	AddFace(&mesh.faces[fc++],t0,b0,t1,1,smoothgr);
	t0++;t1++;
	}
	int chv=edgelstl[sideedge]+1,botcap=lastlevel-sideedge;
	int chb=edgelstl[botcap]+1,chm0,chm1,last=0,sg0=(smooth?2:0),sg1=(smooth?1:0);
	if (doPie)
	{int topctv=lasttvl+1,tvcount=topctv+llsegs+2;
	for (i=1;i<=lastlevel;i++)
	{ if (i<=sideedge)
	{ if (genUVs)
	{ mesh.tvFace[fc].setTVerts(tvcount,topctv,lasttvl);lasttvl=tvcount++;
	mesh.tvFace[fc+1].setTVerts(lasttvr,topctv,tvcount);lasttvr=tvcount++;
	}
	AddFace(&mesh.faces[fc++],edgelstl[i],chv,edgelstl[last],(i==1?1:2),sg0);
	AddFace(&mesh.faces[fc++],edgelstr[last],chv,edgelstr[i],(i==1?3:2),sg1);
	}
	else if (i<=botcap)
	{ if (genUVs)
	{ topctv++;
	mesh.tvFace[fc].setTVerts(lasttvl,tvcount,topctv);
	mesh.tvFace[fc+1].setTVerts(lasttvl,topctv,topctv-1);lasttvl=tvcount++;
	mesh.tvFace[fc+2].setTVerts(topctv-1,topctv,tvcount);
	mesh.tvFace[fc+3].setTVerts(topctv-1,tvcount,lasttvr);lasttvr=tvcount++;
	}
	AddFace(&mesh.faces[fc++],edgelstl[last],edgelstl[i],chm1=(edgelstl[i]+1),0,sg0);
	AddFace(&mesh.faces[fc++],edgelstl[last],chm1,chm0=(edgelstl[last]+1),1,sg0);
	AddFace(&mesh.faces[fc++],chm0,chm1,edgelstr[i],0,sg1);
	AddFace(&mesh.faces[fc++],chm0,edgelstr[i],edgelstr[last],1,sg1);
	}
	else
	{if (genUVs)
	{	if (i==lastlevel) tvcount=topctv+1;
	mesh.tvFace[fc].setTVerts(tvcount,topctv,lasttvl);
	if (i<lastlevel) lasttvl=tvcount++;
	mesh.tvFace[fc+1].setTVerts(lasttvr,topctv,tvcount);lasttvr=tvcount++;
	}
	AddFace(&mesh.faces[fc++],edgelstl[i],chb,edgelstl[last],(i==lastlevel?3:2),sg0);
	AddFace(&mesh.faces[fc++],edgelstr[last],chb,edgelstr[i],(i==lastlevel?1:2),sg1);
	}
	last++;
	}
	}

	if (minush)
		for (i=0;i<nverts;i++) mesh.verts[i].z-=height;
	if (edgelstr) delete []edgelstr;
	if (edgelstl) delete []edgelstl;
	assert(fc==mesh.numFaces);
	//	assert(nv==mesh.numVerts);
	mesh.InvalidateTopologyCache();
}
	
/*****************************************************************************
 ** NAME       || HitTest()
 ** PURPOSE    || See if the object's been hit. Arse!
 ** PARAMETERS || Shit loads of stuff....
 ** RETURNS    || An int - zero if it's not been hit, else one.
 *****************************************************************************/
int CollisionCapsule::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;

	DWORD savedLimits;

	GraphicsWindow *gw = vpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(inode->GetObjectTM(t));
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	DrawCircles(gw, inode, t);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

/*****************************************************************************
 ** NAME       || GetWorldBoundBox()
 ** PURPOSE    || Sets box to be the world bounding box.
 ** PARAMETERS || Stuff
 ** RETURNS    || Nothing (box is passed by reference)
 *****************************************************************************/
void CollisionCapsule::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	//The object can handle this by transforming the 8 points of its local 
	//bounding box into world space and take the minimums and maximums of 
	//the result.  Although this isn't necessarily the tightest bounding box 
	//of the objects points in world space, it is close enough.

	Matrix3 local2world = inode -> GetObjectTM(t);

	Point3 minPoint((float)FLT_MAX, (float)FLT_MAX, (float)FLT_MAX);
	Point3 maxPoint = -minPoint;

	Box3 localBox;
	
	GetLocalBoundBox(t, inode, vp, localBox);
	
	Point3 worldP;

	int i;

	for (i = 0; i < 8; i++)
	{
		worldP = local2world*localBox[i];
		if (worldP.x < minPoint.x)
		{
			minPoint.x = worldP.x;
		}
		if (worldP.y < minPoint.y)
		{
			minPoint.y = worldP.y;
		}
		if (worldP.z < minPoint.z)
		{
			minPoint.z = worldP.z;
		}

		if (worldP.x > maxPoint.x)
		{
			maxPoint.x = worldP.x;
		}
		if (worldP.y > maxPoint.y)
		{
			maxPoint.y = worldP.y;
		}
		if (worldP.z > maxPoint.z)
		{
			maxPoint.z = worldP.z;
		}
	}

	box = Box3(minPoint, maxPoint);

}

/*****************************************************************************
 ** NAME       || GetLocalBoundBox()
 ** PURPOSE    || Sets box to be the local bounding box.
 ** PARAMETERS || Stuff
 ** RETURNS    || Nothing (box is passed by reference)
 *****************************************************************************/
void CollisionCapsule::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	float radius;
	float length;
	Interval ivalid;

	pBlock2->GetValue(colcapsule_radius,t,radius,ivalid);
	pBlock2->GetValue(colcapsule_length,t,length,ivalid);

	Point3 maxPoint((float)radius, (float)radius + (length / 2.0f), (float)radius);
	Point3 minPoint = -maxPoint;

	box = Box3(minPoint, maxPoint);
}

/*****************************************************************************
 ** NAME       || SetReference()
 ** PURPOSE    || Sets up pBlock.
 ** PARAMETERS || int i, and a RefTargetHandle
 ** RETURNS    || Nothing
 *****************************************************************************/
void CollisionCapsule::SetReference(int i, RefTargetHandle rTarg)
{
	switch (i) 
	{
	case PBLOCK_REF_NUM: 	 
		pBlock2 = (IParamBlock2*)rTarg;
		break;
	}
}

RefTargetHandle CollisionCapsule::GetReference(int i)
{
	if (i == PBLOCK_REF_NUM)
	{
		return pBlock2;
	}
	return NULL;
}

int CollisionCapsule::NumRefs()
{
	return 1;
}

/*****************************************************************************
 ** NAME       || BeginEditParams()
 ** PURPOSE    || Read the manual!
 ** PARAMETERS || Some stuff.
 ** RETURNS    || Nothing
 *****************************************************************************/
void CollisionCapsule::BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev)
{
	theCollisionCapsuleDesc.BeginEditParams(ip, this, flags, prev);

	colCapsuleDesc.SetUserDlgProc(new CollisionVolumeDlgProc());
}

/*****************************************************************************
 ** NAME       || EndEditParams()
 ** PURPOSE    || Read the manual.
 ** PARAMETERS || The usual gubbins
 ** RETURNS    || Nothing
 *****************************************************************************/
void CollisionCapsule::EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)
{
	theCollisionCapsuleDesc.EndEditParams(ip, this, flags, next);
}

//
//        name: CollisionSphere::SetupFromSelected
// description: Setup collision sphere from selected Node
//          in: pSelected = pointer to selected node
//         out: 
//
BOOL CollisionCapsule::SetupFromSelected(INode *pSelected, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Box3 local, world;
	Matrix3 mat = pSelected->GetNodeTM(t);
	Point3 centre;
	float radius = 0;

	mat.NoScale();

	GetGroupLocalBoundBox(pSelected, local, mat);
	GetGroupWorldBoundBox(pSelected, world);

	// work out centre and radius of collision sphere
	centre = world.Center();
	GetGroupRadius(pSelected, radius, local.Center(), mat);

	// set collision sphere node transformation matrix
	mat.SetTrans(centre);
	pNode->SetNodeTM(t, mat);
	// set radius
	pBlock2->SetValue(colcapsule_radius, t, radius);
	pBlock2->SetValue(colcapsule_length, t, local.Width().y);

	// set controller
//	Control *pOrienControl = new StaticRotationControl;
//	Control *pTMControl = pNode->GetTMController();
//	pTMControl->SetRotationController(pOrienControl);

	return TRUE;
}
