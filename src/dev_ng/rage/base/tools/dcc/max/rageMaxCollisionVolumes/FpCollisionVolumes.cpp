//
// File:: FpCollisionVolumes.cpp
// Description:: Published function implementation for the rageMaxCollisionVolumes plugin.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 31 May 2010
//

// 3dsmax headers
#include "max.h"

// Local headers
#include "IFpCollisionVolumes.h"
#include "resource.h"
#include "SelectCMode.h"


// Extern actual function in script.cpp
extern int GetCollPolyCount( INode* pNode );

// Function publishing implementation class.
class FpCollisionVolumes : public IFpCollisionVolumes
{
public:
	int		GetPolycount( INode* pNode );
	INode*	CreateOnNode( INode* pNode, ClassDesc *type );
	void	SetUseWireframeColour( INode* pNode, BOOL onOff );
	BOOL	GetUseWireframeColour( INode* pNode );
	void	SetNodeColour( INode* pNode, Color *pCol );
	Color	GetNodeColour( INode* pNode );

	DECLARE_DESCRIPTOR(FpCollisionVolumes);
	BEGIN_FUNCTION_MAP;
	FN_1( IFpCollisionVolumes::em_getPolyCount, TYPE_INT, GetPolycount, TYPE_INODE );
	FN_2( IFpCollisionVolumes::em_CreateOnNode, TYPE_INODE, CreateOnNode, TYPE_INODE, TYPE_CLASS );
	VFN_2( IFpCollisionVolumes::em_SetUseWireframeColour, SetUseWireframeColour, TYPE_INODE, TYPE_BOOL );
	FN_1( IFpCollisionVolumes::em_GetUseWireframeColour, TYPE_BOOL, GetUseWireframeColour, TYPE_INODE );
	VFN_2( IFpCollisionVolumes::em_SetNodeColour, SetNodeColour, TYPE_INODE, TYPE_COLOR );
	FN_1( IFpCollisionVolumes::em_GetNodeColour, TYPE_COLOR_BR, GetNodeColour, TYPE_INODE );
	END_FUNCTION_MAP;
};

FpCollisionVolumes FpCollisionVolumesDesc(
								  FP_COLLISIONVOLUMES_INTERFACE,
								  _T("CollisionVolumes"),
								  IDS_COLLISIONVOLUMES_NAME,
								  NULL,
								  FP_CORE,

								  IFpCollisionVolumes::em_getPolyCount, _T("GetPolyCount"), 0, TYPE_INT, 0, 1,
								  _T("node"), 0, TYPE_INODE,

								  IFpCollisionVolumes::em_CreateOnNode, _T("CreateOnNode"), 0, TYPE_INODE, 0, 2,
								  _T("node"), 0, TYPE_INODE,
								  _T("type_class"), 0, TYPE_CLASS,

								  IFpCollisionVolumes::em_SetUseWireframeColour, _T("SetUseWireframeColour"), 0, TYPE_VOID, 0, 2,
								  _T("node"), 0, TYPE_INODE,
								  _T("onOff"), 0, TYPE_BOOL,

								  IFpCollisionVolumes::em_GetUseWireframeColour, _T("GetUseWireframeColour"), 0, TYPE_BOOL, 0, 1,
								  _T("node"), 0, TYPE_INODE,

								  IFpCollisionVolumes::em_SetNodeColour, _T("SetNodeColour"), 0, TYPE_VOID, 0, 2,
								  _T("node"), 0, TYPE_INODE,
								  _T("colour"), 0, TYPE_COLOR,

								  IFpCollisionVolumes::em_GetNodeColour, _T("GetNodeColour"), 0, TYPE_COLOR_BR, 0, 1,
								  _T("node"), 0, TYPE_INODE,

								  end
								  );

int
FpCollisionVolumes::GetPolycount( INode* pNode )
{
	return ( GetCollPolyCount( pNode ) );
}

INode *
FpCollisionVolumes::CreateOnNode( INode* pNode, ClassDesc *type )
{
	CollisionHelperObject *pColl = (CollisionHelperObject *)CreateInstance(HELPER_CLASS_ID, type->ClassID());
	if(!pColl)
	{
		char buffer[4096];
		sprintf("Failed to create type %s in node %s!", type->ClassName(), pNode->GetName());
		MessageBox(GetCOREInterface()->GetMAXHWnd(), buffer, "Error", NULL);
		return NULL;
	}

	INode *pNewNode = GetCOREInterface()->CreateObjectNode(pColl);
	if(pNewNode)
		pColl->CreateFromSelected(pNode, pNewNode);
	else
	{
		delete pColl;
		return NULL;
	}

	return pNewNode;
}


void	
FpCollisionVolumes::SetUseWireframeColour( INode* pNode, BOOL onOff )
{
	if(!pNode)
		return;
	Object *pObj = pNode->EvalWorldState(0).obj;
	CollisionHelperObject *pCollObj = dynamic_cast<CollisionHelperObject *>(pObj);
	if(!pObj || !pCollObj)
		return;

	pCollObj->SetUseWireframeColour(onOff);
}
BOOL	
FpCollisionVolumes::GetUseWireframeColour( INode* pNode )
{
	if(!pNode)
		return FALSE;
	Object *pObj = pNode->EvalWorldState(0).obj;
	CollisionHelperObject *pCollObj = dynamic_cast<CollisionHelperObject *>(pObj);
	if(!pObj || !pCollObj)
		return FALSE;

	return pCollObj->UsesWireColor();
}
void	
FpCollisionVolumes::SetNodeColour( INode* pNode, Color *pCol )
{
	if(!pNode)
		return;
	Object *pObj = pNode->EvalWorldState(0).obj;
	CollisionHelperObject *pCollObj = dynamic_cast<CollisionHelperObject *>(pObj);
	if(!pObj || !pCollObj)
		return;

	pCollObj->SetNodeColour(pCol);
}
Color 
FpCollisionVolumes::GetNodeColour( INode* pNode )
{
	if(!pNode)
		return Color(0,0,0);
	Object *pObj = pNode->EvalWorldState(0).obj;
	CollisionHelperObject *pCollObj = dynamic_cast<CollisionHelperObject *>(pObj);
	if(!pObj || !pCollObj)
		return Color(0,0,0);

	return pCollObj->GetNodeColour();
}
