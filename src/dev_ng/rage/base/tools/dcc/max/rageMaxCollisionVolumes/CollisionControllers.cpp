//
//
//    Filename: CollisionControllers.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Collision Volume controllers
//
//
#include "CollisionControllers.h"
#include "resource.h"

#define POSN_CHUNK		0x7147
#define QUAT_CHUNK		0x7148

static StaticPositionControlClassDesc staticPositionControlDesc;
ClassDesc *GetStaticPositionControlDesc() {return &staticPositionControlDesc;}
static StaticRotationControlClassDesc staticRotationControlDesc;
ClassDesc *GetStaticRotationControlDesc() {return &staticRotationControlDesc;}

const TCHAR* StaticPositionControlClassDesc::ClassName() 
{
	return GetString(IDS_STATIC_POSN);
}

const TCHAR* StaticPositionControlClassDesc::Category()
{
	return GetString(IDS_DMA_CATEGORY);
}

void StaticRotationControl::GetClassName(TSTR& s)
{
	s = GetString(IDS_STATIC_ROTATION);
}

const TCHAR* StaticRotationControlClassDesc::ClassName() 
{
	return GetString(IDS_STATIC_ROTATION);
}

const TCHAR* StaticRotationControlClassDesc::Category()
{
	return GetString(IDS_DMA_CATEGORY);
}	

// --- StaticPositionControl -------------------------------------------------------------------------------------------

//
//        name: StaticControl::StaticControl
// description: Constructor
//
StaticPositionControl::StaticPositionControl() : m_posn(0,0,0)
{
}

//
//        name: StaticControl::~StaticControl
// description: Destructor
//
StaticPositionControl::~StaticPositionControl()
{
}

void StaticPositionControl::GetClassName(TSTR& s)
{	
	s = GetString(IDS_STATIC_POSN);
}

//
//        name: StaticPositionControl::Clone
// description: Clone control
//
RefTargetHandle StaticPositionControl::Clone(RemapDir& remap)
{
	StaticPositionControl *pControl = new StaticPositionControl;
	pControl->m_posn = m_posn;
	CloneControl(pControl, remap);
	return pControl;
}

//
//        name: StaticPositionControl::Copy
// description: Copy data from another controller
//
void StaticPositionControl::Copy(Control *pFrom)
{
	Interval ivalid;
	pFrom->GetValue(0, m_posn, ivalid);
}

BOOL StaticPositionControl::ChangeParents(TimeValue t,const Matrix3& oldP,const Matrix3& newP,const Matrix3& tm)
{
	Matrix3 rel = oldP * Inverse(newP);

	m_posn = m_posn * rel;

	return TRUE;
}

void StaticPositionControl::SetValue(TimeValue t, void *val, int commit, GetSetMethod method)
{
/*
	Point3 *pPoint = (Point3 *)val;

	if(method == CTRL_ABSOLUTE)
	{		
		m_posn = *pPoint;
	}

	if(method == CTRL_RELATIVE)
	{
		m_posn += *pPoint;
		NotifyDependents(FOREVER,PART_ALL,REFMSG_CHANGE);
	}
*/
}

//
//        name: StaticPositionControl::GetValue
// description: Return position
//
void StaticPositionControl::GetValue(TimeValue t, void *val, Interval &valid, GetSetMethod method)
{
	if(method == CTRL_ABSOLUTE)
	{
		Point3 *pPoint = (Point3 *)val;
		*pPoint = m_posn;
	}

	if(method == CTRL_RELATIVE)
	{
		Matrix3 *mat = (Matrix3*)val;
		mat->PreTranslate(m_posn);
	}
}

//
//        name: StaticPositionControl::Load, Save
// description: Load and save control information
//
IOResult StaticPositionControl::Load(ILoad *iload)
{
	unsigned long length;
	IOResult res;
	while (IO_OK==(res=iload->OpenChunk())) 
	{
		switch(iload->CurChunkID())
		{
		case POSN_CHUNK:
			res = iload->Read(&m_posn,sizeof(Point3), &length);
			break;
		}
		iload->CloseChunk();
		if (res!=IO_OK) 
			return res;
	}
	return IO_OK;

}
IOResult StaticPositionControl::Save(ISave *isave)
{
	unsigned long length;
	IOResult res;

	isave->BeginChunk(POSN_CHUNK);
	res = isave->Write(&m_posn, sizeof(Point3), &length);
	isave->EndChunk();

	return res;
}


// --- StaticRotationControl -------------------------------------------------------------------------------------------

//
//        name: StaticRotationControl::StaticRotationControl
// description: Constructor
//
StaticRotationControl::StaticRotationControl() : m_rotation(0.0f,0.0f,0.0f, 1.0f)
{
}

//
//        name: StaticRotationControl::~StaticRotationControl
// description: Destructor
//
StaticRotationControl::~StaticRotationControl()
{
}

//
//        name: StaticRotationControl::Clone
// description: Clone control
//
RefTargetHandle StaticRotationControl::Clone(RemapDir& remap)
{
	StaticRotationControl *pControl = new StaticRotationControl;
	pControl->m_rotation = m_rotation;
	CloneControl(pControl, remap);
	return pControl;
}

//
//        name: StaticRotationControl::Copy
// description: Copy data from another controller
//
void StaticRotationControl::Copy(Control *pFrom)
{
	Interval ivalid;
	pFrom->GetValue(0, m_rotation, ivalid);
}

BOOL StaticRotationControl::ChangeParents(TimeValue t,const Matrix3& oldP,const Matrix3& newP,const Matrix3& tm)
{
	Matrix3 rel = oldP * Inverse(newP);
	Matrix3 premat;
	m_rotation.MakeMatrix(premat);

	premat = premat * rel;

	m_rotation = Quat(premat);

	return TRUE;
}

//
//        name: StaticRotationControl::GetValue
// description: Return position
//
void StaticRotationControl::GetValue(TimeValue t, void *val, Interval &valid, GetSetMethod method)
{
	if(method == CTRL_ABSOLUTE)
	{
		Quat *pQuat = (Quat *)val;
		*pQuat = m_rotation;
	}

	if(method == CTRL_RELATIVE)
	{
		Matrix3 premat;
		m_rotation.MakeMatrix(premat);

		Matrix3 *mat = (Matrix3*)val;
		*mat = premat * *mat;
	}
}

void StaticRotationControl::SetValue(TimeValue t, void *val, int commit, GetSetMethod method)
{
/*
	if(method == CTRL_ABSOLUTE)
	{
		Quat *pQuat = (Quat *)val;
		m_rotation = *pQuat;
	}
*/
}

//
//        name: StaticRotationControl::Load, Save
// description: Load and save control information
//
IOResult StaticRotationControl::Load(ILoad *iload)
{
	unsigned long length;
	IOResult res;
	while (IO_OK==(res=iload->OpenChunk())) 
	{
		switch(iload->CurChunkID())
		{
		case QUAT_CHUNK:
			res = iload->Read(&m_rotation,sizeof(Quat), &length);
			break;
		}
		iload->CloseChunk();
		if (res!=IO_OK) 
			return res;
	}
	return IO_OK;

}
IOResult StaticRotationControl::Save(ISave *isave)
{
	unsigned long length;
	IOResult res;

	isave->BeginChunk(QUAT_CHUNK);
	res = isave->Write(&m_rotation, sizeof(Quat), &length);
	isave->EndChunk();

	return res;
}


