/**********************************************************************
 *<
	FILE: CollisionDisc.cpp

	DESCRIPTION:	Appwizard generated plugin

	CREATED BY: 

	HISTORY: 

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/

// C header
#include <float.h>
// My headers
#include "CollisionDisc.h"
#include "CollisionControllers.h"
#include <simpobj.h>

#define NUM_CIRCLE_SEGS 24

static CollisionDiscClassDesc theCollisionDiscDesc;
ClassDesc* GetCollisionDiscDesc() {return &theCollisionDiscDesc; }

static ParamBlockDescID descVer1[] = 
{
	{ TYPE_FLOAT, NULL, FALSE, 0}, //radius controlled by a spinner
	{ TYPE_INT,   NULL, FALSE, 1},	  //Type of sphere
};

static ParamVersionDesc oldVersions[] =
{
	ParamVersionDesc(descVer1, 2, 1)
};

//#define AUTO_CREATE

//
// Max3 paramblock2 version
//
ParamBlockDesc2 colDiscDesc (coldisc_params, _T("Collision_Disc parameters"), IDS_COLLDISC, &theCollisionDiscDesc, P_AUTO_CONSTRUCT | P_AUTO_UI, 0,
						 IDD_COL_CAPSULE_PANEL, IDS_PARAMS, 0, 0, NULL,
						 coldisc_radius, _T("Radius"), TYPE_WORLD, 0, IDS_RADIUS,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_RADIUS_EDIT, IDC_RADIUS_SPIN, 0.01f,
							end,
						 coldisc_length, _T("Length"), TYPE_WORLD, 0, IDS_LENGTH,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_LENGTH_EDIT, IDC_LENGTH_SPIN, 0.01f,
							end,
						end);

//
//        name: CollisionSphereClassDesc::OkToCreate
// description: Return if it is OK to create a collision sphere i.e there is one object selected
//
BOOL CollisionDiscClassDesc::OkToCreate(Interface *ip)
{
	//return theSelectCommandMode.OkToCreate(ip);
	return TRUE;
}

int CollisionDiscClassDesc::BeginCreate(Interface *ip)
{
	//assert(theSelectCommandMode.OkToCreate(ip));

	theSelectCommandMode.SetObjectClassIDs(SuperClassID(), ClassID());
	theSelectCommandMode.Begin((IObjParam*)ip);
	ip->PushCommandMode(&theSelectCommandMode);

	return TRUE;
}

int CollisionDiscClassDesc::EndCreate(Interface *ip)
{
	theSelectCommandMode.End((IObjParam*)ip);
	ip->RemoveMode(&theSelectCommandMode);

	return TRUE;
}

const TCHAR* CollisionDiscClassDesc::ClassName() 
{
	return GetString(IDS_COLLDISC);
}

const TCHAR* CollisionDiscClassDesc::Category()
{
	return GetString(IDS_CATEGORY);
}	

/*****************************************************************************
 ** NAME       || CollisionDisc()
 ** PURPOSE    || Constructor. Set up the pblock.
 ** PARAMETERS || 
 ** RETURNS    || 
 *****************************************************************************/
CollisionDisc::CollisionDisc():
	pBlock2(NULL)
{
	theCollisionDiscDesc.MakeAutoParamBlocks(this);
}

/*****************************************************************************
 ** NAME       || Clone()
 ** PURPOSE    || Clones the object
 ** PARAMETERS || 
 ** RETURNS    || 
 *****************************************************************************/
RefTargetHandle CollisionDisc::Clone(RemapDir& remap) 
{
	CollisionDisc* pClone = new CollisionDisc();

	BaseClone(this, pClone, remap);
	pClone->ReplaceReference(PBLOCK_REF_NUM, remap.CloneRef(pBlock2));
	return pClone;
}

void CollisionDisc::InitNodeName(TSTR& s)
{
	s = GetString(IDS_COLLDISC_NODE);
}

TCHAR *CollisionDisc::GetObjectName() 
{
	return GetString(IDS_COLLDISC);
}

void CollisionDisc::GetClassName(TSTR& s)
{
	s = GetString(IDS_COLLDISC);
}

/*****************************************************************************
 ** NAME       || Display()
 ** PURPOSE    || Display the disc
 ** PARAMETERS || The time, the inode pointer, and some other stuff
 ** RETURNS    || An int (zero)
 *****************************************************************************/
int CollisionDisc::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);

	SetHelperColour(inode, gw);

	DrawCircles(gw, inode, t);

	return 0;
}

/*****************************************************************************
 ** NAME       || drawCircles()
 ** PURPOSE    || Draw in the circles
 ** PARAMETERS || A GraphicsWindow pointer
 ** RETURNS    || Nowt.
 *****************************************************************************/
void CollisionDisc::DrawCircles(GraphicsWindow *gw, INode *inode, TimeValue t)
{
	if(!m_isDisplayed)
		return;

	Interval ivalid;

	float radius;
	float length;

	pBlock2->GetValue(coldisc_radius, 0, radius, ivalid);
	pBlock2->GetValue(coldisc_length, 0, length, ivalid);

//	if(m_displayMode == 0)
	{
		DrawLineProc lineProc(gw);
		float u,v;
		Point3 circlePoints1[NUM_CIRCLE_SEGS + 1];
		Point3 circlePoints2[NUM_CIRCLE_SEGS + 1];
		int i,j;

		float oneOverRootTwo = (float(1))/(float(sqrt(2.0f)));
		float sqrtRad = radius * oneOverRootTwo;
		float multiplier = float(TWOPI) / float(NUM_CIRCLE_SEGS);

		// length lines
		for (i = 0; i <= NUM_CIRCLE_SEGS; i++) 
		{
			Point3 pt[2];
			u = float(i) * multiplier;
			pt[0].x = length / 2.0f;
			pt[0].y = (float)cos(u) * radius;
			pt[0].z = (float)sin(u) * radius;

			pt[1].x = length / -2.0f;
			pt[1].y = (float)cos(u) * radius;
			pt[1].z = (float)sin(u) * radius;

			lineProc.proc(pt,2);
			circlePoints1[i] = pt[0];
			circlePoints2[i] = pt[1];
		}
		lineProc.proc(circlePoints1,NUM_CIRCLE_SEGS + 1);
		lineProc.proc(circlePoints2,NUM_CIRCLE_SEGS + 1);
	}
// 	else
// 	{
// #define PB_RADIUS	0
// #define PB_SEGS		1
// 
// 		if(!inode)
// 		{
// 			return;
// 		}
// 
// 		SimpleObject* pSimple = (SimpleObject*)CreateInstance(GEOMOBJECT_CLASS_ID,Class_ID(SPHERE_CLASS_ID,0));
// 
// 		if(!pSimple)
// 		{
// 			return;
// 		}
// 
// 		pSimple->pblock->SetValue(PB_RADIUS,0, radius);				
// 		pSimple->pblock->SetValue(PB_SEGS,0, NUM_CIRCLE_SEGS);				
// 		pSimple->BuildMesh(t);
// 
// 		BOOL needDelete;
// 
// 		class NullView : public View 
// 		{
// 		public:
// 
// 			Point2 ViewToScreen(Point3 p) { return Point2(p.x,p.y); }
// 			NullView() 
// 			{
// 				worldToView.IdentityMatrix();
// 				screenW=640.0f; screenH = 480.0f;
// 			}
// 		};
// 
// 		NullView nullView;
// 
// 		Mesh* pMesh = pSimple->GetRenderMesh(t,inode,nullView,needDelete);
// 
// 		pMesh->render(gw,inode->Mtls(),NULL,COMP_ALL);
// 
// 		if(needDelete)
// 		{
// 			delete pMesh;
// 		}
// 
// 		delete pSimple;
// 	}
};
	
/*****************************************************************************
 ** NAME       || HitTest()
 ** PURPOSE    || See if the object's been hit. Arse!
 ** PARAMETERS || Shit loads of stuff....
 ** RETURNS    || An int - zero if it's not been hit, else one.
 *****************************************************************************/
int CollisionDisc::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;

	DWORD savedLimits;

	GraphicsWindow *gw = vpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(inode->GetObjectTM(t));
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	DrawCircles(gw, inode, t);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

/*****************************************************************************
 ** NAME       || GetWorldBoundBox()
 ** PURPOSE    || Sets box to be the world bounding box.
 ** PARAMETERS || Stuff
 ** RETURNS    || Nothing (box is passed by reference)
 *****************************************************************************/
void CollisionDisc::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	//The object can handle this by transforming the 8 points of its local 
	//bounding box into world space and take the minimums and maximums of 
	//the result.  Although this isn't necessarily the tightest bounding box 
	//of the objects points in world space, it is close enough.

	Matrix3 local2world = inode -> GetObjectTM(t);

	Point3 minPoint((float)FLT_MAX, (float)FLT_MAX, (float)FLT_MAX);
	Point3 maxPoint = -minPoint;

	Box3 localBox;
	
	GetLocalBoundBox(t, inode, vp, localBox);
	
	Point3 worldP;

	int i;

	for (i = 0; i < 8; i++)
	{
		worldP = local2world*localBox[i];
		if (worldP.x < minPoint.x)
		{
			minPoint.x = worldP.x;
		}
		if (worldP.y < minPoint.y)
		{
			minPoint.y = worldP.y;
		}
		if (worldP.z < minPoint.z)
		{
			minPoint.z = worldP.z;
		}

		if (worldP.x > maxPoint.x)
		{
			maxPoint.x = worldP.x;
		}
		if (worldP.y > maxPoint.y)
		{
			maxPoint.y = worldP.y;
		}
		if (worldP.z > maxPoint.z)
		{
			maxPoint.z = worldP.z;
		}
	}

	box = Box3(minPoint, maxPoint);

}

/*****************************************************************************
 ** NAME       || GetLocalBoundBox()
 ** PURPOSE    || Sets box to be the local bounding box.
 ** PARAMETERS || Stuff
 ** RETURNS    || Nothing (box is passed by reference)
 *****************************************************************************/
void CollisionDisc::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	float radius;
	float length;
	Interval ivalid;

	pBlock2->GetValue(coldisc_radius,t,radius,ivalid);
	pBlock2->GetValue(coldisc_length,t,length,ivalid);

	Point3 maxPoint((float)radius, (length / 2.0f), (float)radius);
	Point3 minPoint = -maxPoint;

	box = Box3(minPoint, maxPoint);
}

/*****************************************************************************
 ** NAME       || SetReference()
 ** PURPOSE    || Sets up pBlock.
 ** PARAMETERS || int i, and a RefTargetHandle
 ** RETURNS    || Nothing
 *****************************************************************************/
void CollisionDisc::SetReference(int i, RefTargetHandle rTarg)
{
	switch (i) 
	{
	case PBLOCK_REF_NUM: 	 
		pBlock2 = (IParamBlock2*)rTarg;
		break;
	}
}

RefTargetHandle CollisionDisc::GetReference(int i)
{
	if (i == PBLOCK_REF_NUM)
	{
		return pBlock2;
	}
	return NULL;
}

int CollisionDisc::NumRefs()
{
	return 1;
}

/*****************************************************************************
 ** NAME       || BeginEditParams()
 ** PURPOSE    || Read the manual!
 ** PARAMETERS || Some stuff.
 ** RETURNS    || Nothing
 *****************************************************************************/
void CollisionDisc::BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev)
{
	theCollisionDiscDesc.BeginEditParams(ip, this, flags, prev);

	colDiscDesc.SetUserDlgProc(new CollisionVolumeDlgProc());
}

/*****************************************************************************
 ** NAME       || EndEditParams()
 ** PURPOSE    || Read the manual.
 ** PARAMETERS || The usual gubbins
 ** RETURNS    || Nothing
 *****************************************************************************/
void CollisionDisc::EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)
{
	theCollisionDiscDesc.EndEditParams(ip, this, flags, next);
}

//
//        name: CollisionSphere::SetupFromSelected
// description: Setup collision sphere from selected Node
//          in: pSelected = pointer to selected node
//         out: 
//
BOOL CollisionDisc::SetupFromSelected(INode *pSelected, INode *pNode)
{
	TimeValue t = GetCOREInterface()->GetTime();
	Box3 local, world;
	Matrix3 mat = pSelected->GetNodeTM(t);
	Point3 centre;
	float radius = 0;

	mat.NoScale();

	GetGroupLocalBoundBox(pSelected, local, mat);
	GetGroupWorldBoundBox(pSelected, world);

	// work out centre and radius of collision sphere
	centre = world.Center();
	GetGroupRadius(pSelected, radius, local.Center(), mat);

	// set collision sphere node transformation matrix
	mat.SetTrans(centre);
	pNode->SetNodeTM(t, mat);
	// set radius
	pBlock2->SetValue(coldisc_radius, t, radius);
	pBlock2->SetValue(coldisc_length, t, local.Width().x);

	return TRUE;
}
