/**********************************************************************
 *<
	FILE: CollisionBox.h

	DESCRIPTION:	Template Utility

	CREATED BY:

	HISTORY:

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/

#ifndef INC_COLLISION_BOX_H
#define INC_COLLISION_BOX_H

#include "Max.h"
#include "istdplug.h"
#include "iparamm2.h"

#include "SelectCMode.h"
#include "ICollision.h"

extern TCHAR *GetString(int id);

extern HINSTANCE hInstance;


//N.B.: The box's actual length, corner-to-corner, is 2*LENGTH. The values stored
//are kinda like the radius - form the middle to the edge.

/************************************************************************************
 **
 ** Class CollisionBox. The collision box object.
 **
 ************************************************************************************/
class CollisionBox : public CollisionHelperObject
{

public:

	IParamBlock2 *pBlock2;		

	CollisionBox();

	void GetNameSuffix(TSTR &s) {s = "-ColBox";}
	BOOL SetupFromSelected(INode *pSelected, INode *pNode);
	void DrawBox(INode* inode,GraphicsWindow *gw);

	// from Object
	ObjectState Eval(int){return ObjectState(this);}
	void InitNodeName(TSTR& s);

	// from BaseObject
	TCHAR *GetObjectName();
	CreateMouseCallBack* GetCreateMouseCallBack(void){return NULL;}
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
	void GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );

	// from ReferenceTarget
	RefTargetHandle Clone(RemapDir& remap);

	// from ReferenceMaker
	void SetReference(int i, RefTargetHandle rTarg);
	RefTargetHandle GetReference(int i);
	int NumRefs();
	RefResult NotifyRefChanged(Interval, RefTargetHandle, PartID&, RefMessage){return REF_SUCCEED;}

	// from Animatable
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return pBlock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == colbox_params); return pBlock2;}
	void BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev=NULL);
	void EndEditParams(IObjParam *ip, ULONG flags, Animatable *next=NULL);
	void DeleteThis() {delete this;}
	Class_ID ClassID(){return COLLISION_BOX_CLASS_ID;}
	void GetClassName(TSTR& s);
};

class CollisionBoxClassDesc : public ClassDesc2 
{

public:

	int IsPublic() {return 1;}
	void* Create(BOOL loading = FALSE) {return new CollisionBox();}
	const TCHAR* ClassName();
	SClass_ID SuperClassID() {return HELPER_CLASS_ID;}
	Class_ID ClassID(){return COLLISION_BOX_CLASS_ID;}
	const TCHAR* Category();
	void ResetClassParams (BOOL fileReset){}
	BOOL OkToCreate(Interface *ip);
	int BeginCreate(Interface *ip);
	int EndCreate(Interface *ip);

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName()				{ return _T("ColBox"); }
	HINSTANCE		HInstance()					{ return hInstance; }
};



#endif // INC_COLLISION_BOX_H
