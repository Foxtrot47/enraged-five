/*******************************************************************************
 *
 *    Filename : DllEntry.cpp
 *     Creator : 
 *     $Author : $
 *       $Date : $
 *   $Revision : $
 * Description : 
 *
 *******************************************************************************/

#include <Max.h>
#include "resource.h"
#include "CollisionMesh.h"

ClassDesc* GetCollisionDiscDesc();
ClassDesc* GetCollisionCapsuleDesc();
ClassDesc* GetCollisionCylinderDesc();
ClassDesc* GetCollisionSphereDesc();
ClassDesc* GetCollisionBoxDesc();
ClassDesc* GetCollisionPlaneDesc();
ClassDesc* GetCollisionMeshDesc();
ClassDesc* GetCollisionMeshDummyDesc();
ClassDesc* GetCollisionMeshShadowDesc();
ClassDesc* GetVolumeCreateModDesc();
ClassDesc* GetStaticPositionControlDesc();
ClassDesc* GetStaticRotationControlDesc();
extern TCHAR *GetString(int id);

HINSTANCE hInstance;
int controlsInit = FALSE;
bool g_libInitialized = FALSE;

#ifdef OUTSOURCE_TOOLS

#include "lmclient.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "lm_attr.h"
#ifdef PC
#define LICPATH "32127@10.11.31.5"
#define LICPATHREMOTE "32127@194.74.241.228"
#else
#define LICPATH "@localhost:license.dat:."
#endif /* PC */

#define FEATURE "f2"
VENDORCODE code;
LM_HANDLE *lm_job;

static void init(struct flexinit_property_handle **handle)
{
#ifndef NO_ACTIVATION_SUPPORT
	struct flexinit_property_handle *ourHandle;
	int stat;

	if ((stat = lc_flexinit_property_handle_create(&ourHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit_property_handle_create() failed: %d\n", stat);
		exit(1);
	}
	if ((stat = lc_flexinit_property_handle_set(ourHandle,
		(FLEXINIT_PROPERTY_TYPE)FLEXINIT_PROPERTY_USE_TRUSTED_STORAGE,
		(FLEXINIT_VALUE_TYPE)1)) != 0)
	{
		fprintf(stderr, "lc_flexinit_property_handle_set failed: %d\n", stat);
		exit(1);
	}
	if ((stat = lc_flexinit(ourHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit failed: %d\n", stat);
		exit(1);
	}
	*handle = ourHandle;
#endif /* NO_ACTIVATION_SUPPORT */
}

static void cleanup(struct flexinit_property_handle *initHandle)
{
#ifndef NO_ACTIVATION_SUPPORT
	int stat;

	if ((stat = lc_flexinit_cleanup(initHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit_cleanup failed: %d\n", stat);
	}
	if ((stat = lc_flexinit_property_handle_free(initHandle)) != 0)
	{
		fprintf(stderr, "lc_flexinit_property_handle_free failed: %d\n", stat);
	}
#endif /* NO_ACTIVATION_SUPPORT */
}

#endif


/*******************************************************************************
 *        name : DllMain()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
BOOL WINAPI DllMain(HINSTANCE hinstDLL, ULONG fdwReason, LPVOID lpvReserved)
{
	hInstance = hinstDLL;				// Hang on to this DLL's instance handle.

#if (MAX_RELEASE < 9000)
	if (controlsInit == FALSE) 
	{
		controlsInit = TRUE;
		InitCustomControls(hInstance);	// Initialize MAX's custom controls
		InitCommonControls();			// Initialize Win95 controls
	}
#endif
			
	return (TRUE);
}

/*******************************************************************************
 *        name : LibDescription()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_LIBDESCRIPTION);
}

/*******************************************************************************
 *        name : LibNumberClasses()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
__declspec( dllexport ) int LibNumberClasses()
{
	return 12;
}

/*******************************************************************************
 *        name : LibClassDesc()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
__declspec( dllexport ) ClassDesc* LibClassDesc(int i)
{
	switch(i) 
	{
	case 0:
		return GetCollisionSphereDesc();
	case 1:
		return GetCollisionBoxDesc();
	case 2:
		return GetCollisionPlaneDesc();
	case 3:
		return GetCollisionMeshDesc();
	case 4:
		return GetStaticPositionControlDesc();
	case 5:
		return GetStaticRotationControlDesc();
	case 6:
		return GetVolumeCreateModDesc();
	case 7:
		return GetCollisionMeshShadowDesc();
	case 8:
		return GetCollisionCapsuleDesc();
	case 9:
		return GetCollisionCylinderDesc();
	case 10:
		return GetCollisionDiscDesc();
	case 11:
		return GetCollisionMeshDummyDesc();
	default: 
		return 0;
	}
}

/*******************************************************************************
 *        name : LibVersion()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
__declspec( dllexport ) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}


#if( MAX_RELEASE >= 9000 )

/*******************************************************************************
 *        name : LibInit()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
__declspec( dllexport ) int LibInitialize(void)
{
	//According to the SDK docs LibInitialize "should" only be called once but this isn't strictly enforced
	//so make sure we only initialize once
	if(!g_libInitialized)
	{
#ifdef OUTSOURCE_TOOLS

		char feature[64];
		char * output = NULL;
		struct flexinit_property_handle *initHandle;
		int nlic = 1;

		init(&initHandle);

		if (lc_new_job(0, lc_new_job_arg2, &code, &lm_job))
		{
			lc_perror(lm_job, "lc_new_job failed");
			cleanup(initHandle);
			exit(lc_get_errno(lm_job));
		}

		strcpy(feature, FEATURE);

		(void)lc_set_attr(lm_job, LM_A_LICENSE_DEFAULT, (LM_A_VAL_TYPE)LICPATHREMOTE);
		if(lc_checkout(lm_job, feature, "1.0", nlic, LM_CO_NOWAIT, &code, LM_DUP_NONE))
		{
			(void)lc_set_attr(lm_job, LM_A_LICENSE_DEFAULT, (LM_A_VAL_TYPE)LICPATH);

			if(lc_checkout(lm_job, feature, "1.0", nlic, LM_CO_NOWAIT, &code, LM_DUP_NONE))
			{
				lc_perror(lm_job, "checkout failed");
				cleanup(initHandle);
				exit (lc_get_errno(lm_job));
			}
		}

#endif //OUTSOURCE_TOOLS

		//Perform any custom plugin initialization here
		g_libInitialized = TRUE;

		if (controlsInit == FALSE) 
		{
			controlsInit = TRUE;

#if MAX_VERSION_MAJOR < 11 
			InitCustomControls(hInstance);	// Initialize MAX's custom controls
#endif //MAX_VERSION_MAJOR

			InitCommonControls();			// Initialize Win95 controls
		}
	}
	return 1;
}

/*******************************************************************************
 *        name : LibShutdown()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
__declspec( dllexport ) int LibShutdown(void)
{
	return 1;
}

#endif //( MAX_RELEASE >= 9000 ) 

/*******************************************************************************
 *        name : GetString()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
TCHAR *GetString(int id)
{
	static TCHAR buf[256];

	if (hInstance)
	{
		if (LoadString(hInstance, id, buf, sizeof(buf)))
		{
			return buf;
		}
		else
		{
			return NULL;
		}
	}

	return NULL;
}
