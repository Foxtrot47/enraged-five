/**********************************************************************
 *<
	FILE: CollisionSphere.cpp

	DESCRIPTION:	Appwizard generated plugin

	CREATED BY: 

	HISTORY: 

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/

// C header
#include <float.h>
// My headers
#include "collisionSphere.h"
#include "CollisionControllers.h"
#include "resource.h"
#include <simpobj.h>

#define NUM_CIRCLE_SEGS 24

static CollisionSphereClassDesc theCollisionSphereDesc;
ClassDesc* GetCollisionSphereDesc(){return &theCollisionSphereDesc;}

static ParamBlockDescID descVer1[] = 
{
	{ TYPE_FLOAT, NULL, FALSE, 0}, //radius controlled by a spinner
	{ TYPE_INT,   NULL, FALSE, 1},	  //Type of sphere
};

static ParamVersionDesc oldVersions[] =
{
	ParamVersionDesc(descVer1, 2, 1)
};

//
// Max3 paramblock2 version
//
ParamBlockDesc2 colSphereDesc (colsphere_params, _T("ColSphere parameters"), IDS_COLSPHERE, &theCollisionSphereDesc, P_AUTO_CONSTRUCT | P_AUTO_UI, 0,
						 IDD_COL_SPHERE_PANEL, IDS_PARAMS, 0, 0, NULL,
						 colsphere_radius, _T("Radius"), TYPE_WORLD, 0, IDS_RADIUS,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_RADIUS_EDIT, IDC_RADIUS_SPIN, 0.01f,
							end,
						 colsphere_type, _T("Type"), TYPE_INT, 0, IDS_TYPE,
							p_default,		0,
							p_ui,			TYPE_RADIO, 3, IDC_NORMAL, IDC_SIMPLE, IDC_HIERARCHICAL, 
							p_vals,			colsphere_normal, colsphere_simple, colsphere_hierarchical,
							end, 
						end);

//
//        name: CollisionSphereClassDesc::OkToCreate
// description: Return if it is OK to create a collision sphere i.e there is one object selected
//
BOOL CollisionSphereClassDesc::OkToCreate(Interface *ip)
{
	//return theSelectCommandMode.OkToCreate(ip);
	return true;
}

int CollisionSphereClassDesc::BeginCreate(Interface *ip)
{
	//assert(theSelectCommandMode.OkToCreate(ip));

	theSelectCommandMode.SetObjectClassIDs(SuperClassID(), ClassID());
	theSelectCommandMode.Begin((IObjParam*)ip);
	ip->PushCommandMode(&theSelectCommandMode);

	return TRUE;
}

int CollisionSphereClassDesc::EndCreate(Interface *ip)
{
	theSelectCommandMode.End((IObjParam*)ip);
	ip->RemoveMode(&theSelectCommandMode);

	return TRUE;
}

const TCHAR* CollisionSphereClassDesc::ClassName() 
{
	return GetString(IDS_COLSPHERE);
}

const TCHAR* CollisionSphereClassDesc::Category()
{
	return GetString(IDS_CATEGORY);
}	

/*****************************************************************************
 ** NAME       || CollisionSphere()
 ** PURPOSE    || Constructor. Set up the pblock.
 ** PARAMETERS || 
 ** RETURNS    || 
 *****************************************************************************/
CollisionSphere::CollisionSphere():
	pBlock2(NULL)
{
	theCollisionSphereDesc.MakeAutoParamBlocks(this);
}

/*****************************************************************************
 ** NAME       || Clone()
 ** PURPOSE    || Clones the object
 ** PARAMETERS || 
 ** RETURNS    || 
 *****************************************************************************/
RefTargetHandle CollisionSphere::Clone(RemapDir& remap) 
{
	CollisionSphere* pClone = new CollisionSphere();

	BaseClone(this, pClone, remap);
	pClone->ReplaceReference(PBLOCK_REF_NUM, remap.CloneRef(pBlock2));
	return pClone;
}

void CollisionSphere::InitNodeName(TSTR& s)
{
	s = GetString(IDS_COLSPHERE_NODE);
}

TCHAR *CollisionSphere::GetObjectName() 
{
	return GetString(IDS_COLSPHERE);
}

void CollisionSphere::GetClassName(TSTR& s)
{
	s = GetString(IDS_COLSPHERE);
}

/*****************************************************************************
 ** NAME       || Display()
 ** PURPOSE    || Display the sphere
 ** PARAMETERS || The time, the inode pointer, and some other stuff
 ** RETURNS    || An int (zero)
 *****************************************************************************/
int CollisionSphere::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);

	SetHelperColour(inode, gw);

	DrawCircles(gw, inode, t);

	return 0;
}

/*****************************************************************************
 ** NAME       || drawCircles()
 ** PURPOSE    || Draw in the circles
 ** PARAMETERS || A GraphicsWindow pointer
 ** RETURNS    || Nowt.
 *****************************************************************************/
void CollisionSphere::DrawCircles(GraphicsWindow *gw, INode *inode, TimeValue t)
{
	if(!m_isDisplayed)
		return;

	Interval ivalid;

	float radius;
	pBlock2 -> GetValue(colsphere_radius, 0, radius, ivalid);

	if(m_displayMode == 0)
	{
		DrawLineProc lineProc(gw);
		float u;
		Point3 pt[NUM_CIRCLE_SEGS+1];
		int i;

		float oneOverRootTwo = (float(1))/(float(sqrt(2.0f)));
		float sqrtRad = radius * oneOverRootTwo;
		float multiplier = float(TWOPI) / float(NUM_CIRCLE_SEGS);

		// XY
		for (i = 0; i <= NUM_CIRCLE_SEGS; i++) 
		{
			u = float(i) * multiplier;
			pt[i].x = (float)cos(u) * radius;
			pt[i].y = (float)sin(u) * radius;
			pt[i].z = 0.0f;
		}
		lineProc.proc(pt,NUM_CIRCLE_SEGS+1);

		// YZ
		for (i = 0; i <= NUM_CIRCLE_SEGS; i++) 
		{
			u = float(i) * multiplier;
			pt[i].x = 0.0f;
			pt[i].y = (float)cos(u) * radius;
			pt[i].z = (float)sin(u) * radius;
		}
		lineProc.proc(pt,NUM_CIRCLE_SEGS+1);

		// ZX
		for (i = 0; i <= NUM_CIRCLE_SEGS; i++)
		{
			u = float(i) * multiplier;
			pt[i].x = (float)cos(u) * radius;
			pt[i].y = 0.0f;
			pt[i].z = (float)sin(u) * radius;
		}
		lineProc.proc(pt,NUM_CIRCLE_SEGS+1);

		{
			//Maybe...
			for (i = 0; i <= NUM_CIRCLE_SEGS; i++)
			{
				u = float(i) * multiplier;
				pt[i].x = (float)cos(u) * radius;
				pt[i].y = (float)sin(u) * sqrtRad;
				pt[i].z = (float)sin(u) * sqrtRad;
			}
			lineProc.proc(pt,NUM_CIRCLE_SEGS+1);
			for (i = 0; i <= NUM_CIRCLE_SEGS; i++)
			{
				u = float(i) * multiplier;
				pt[i].x =   (float)cos(u) * radius;
				pt[i].y = -((float)sin(u) * sqrtRad);
				pt[i].z =   (float)sin(u) * sqrtRad;
			}
			lineProc.proc(pt,NUM_CIRCLE_SEGS+1);


			for (i = 0; i <= NUM_CIRCLE_SEGS; i++)
			{
				u = float(i) * multiplier;
				pt[i].x = (float)sin(u) * sqrtRad;
				pt[i].y = (float)sin(u) * sqrtRad;
				pt[i].z = (float)cos(u) * radius;
			}
			lineProc.proc(pt,NUM_CIRCLE_SEGS+1);
			for (i = 0; i <= NUM_CIRCLE_SEGS; i++)
			{
				u = float(i) * multiplier;
				pt[i].x = (float)sin(u) * sqrtRad;
				pt[i].y = -(float)sin(u) * sqrtRad;
				pt[i].z = (float)cos(u) * radius;
			}
			lineProc.proc(pt,NUM_CIRCLE_SEGS+1);


			for (i = 0; i <= NUM_CIRCLE_SEGS; i++)
			{
				u = float(i) * multiplier;
				pt[i].x = (float)sin(u) * sqrtRad;
				pt[i].y = (float)cos(u) * radius;
				pt[i].z = (float)sin(u) * sqrtRad;
			}
			lineProc.proc(pt,NUM_CIRCLE_SEGS+1);
			for (i = 0; i <= NUM_CIRCLE_SEGS; i++)
			{
				u = float(i) * multiplier;
				pt[i].x = (float)sin(u) * sqrtRad;
				pt[i].y = (float)cos(u) * radius;
				pt[i].z = -(float)sin(u) * sqrtRad;
			}
			lineProc.proc(pt,NUM_CIRCLE_SEGS+1);
		}

		// This gives us a nice camera facing circle to clearly show the sphere's boundry
		Matrix3 nodeMat = inode->GetObjectTM(t);
		Matrix3 invCamMat;
		float camMat[4][4];
		int persp;
		float hither, yon;

		gw->getCameraMatrix(camMat, &invCamMat, &persp, &hither, &yon);
		invCamMat.NoTrans();
		invCamMat.NoScale();
		nodeMat.NoTrans();
		nodeMat.NoScale();

		invCamMat = nodeMat*invCamMat;

		// XY - Camera facing
		for (i = 0; i <= NUM_CIRCLE_SEGS; i++)
		{
			u = float(i) * multiplier;
			pt[i].x = (float)cos(u) * radius;
			pt[i].y = (float)sin(u) * radius;
			pt[i].z = 0.0f;

			pt[i]=pt[i]*invCamMat;
		}
		lineProc.proc(pt,NUM_CIRCLE_SEGS+1);
	}
	else
	{
#if 0
		Mesh mesh;

		Point3 p;	
		int ix,na,nb,nc,nd,jx,kx;
		int nf=0,nv=0;
		float delta, delta2;
		float a,alt,secrad,secang,b,c;
		int segs = NUM_CIRCLE_SEGS;
		float startAng = 0.0f;

		ivalid = FOREVER;

		float basedelta = 2.0f * PI / (float)segs;
		delta2 = basedelta;
		delta  = basedelta;
		int rows;
		rows = (segs/2-1);

		int realsegs=segs;
		int nverts = rows * realsegs + 2;
		int nfaces = rows * realsegs * 2;
		mesh.setNumVerts(nverts);
		mesh.setNumFaces(nfaces);
		int lastvert=nverts-1;

	//	mesh.InvalidateStrips();

		mesh.setVert(nv, 0.0f, 0.0f, radius);
		nv++;

		alt=delta;
		for(ix=1; ix<=rows; ix++) 
		{		
			a = (float)cos(alt)*radius;		
			secrad = (float)sin(alt)*radius;
			secang = startAng;

			for(jx=0; jx<segs; ++jx) 
			{
				b = (float)cos(secang)*secrad;
				c = (float)sin(secang)*secrad;
				mesh.setVert(nv++,b,c,a);
				secang+=delta2;
				
			}
			alt+=delta;		
		}

		mesh.setVert(nv++, 0.0f, 0.0f,-radius);

		// Make top conic cap
		for(ix=1; ix<=segs; ++ix) 
		{
			nc=(ix==segs)?1:ix+1;

			mesh.faces[nf].setEdgeVisFlags(1,1,1);
			mesh.faces[nf].setSmGroup(1);
			mesh.faces[nf].setMatID(0);
			mesh.faces[nf].setVerts(0, ix, nc);
			nf++;
		}

		// Make midsection
		int lastrow=rows-1,lastseg=segs-1,almostlast=lastseg-1;

		for(ix=1; ix<rows; ++ix) 
		{
			jx=(ix-1)*segs+1;

			for(kx=0; kx<segs; ++kx) 
			{
				na = jx+kx;
				nb = na+segs;
				nc = (kx==lastseg)? jx+segs: nb+1;
				nd = (kx==lastseg)? jx : na+1;
				
				mesh.faces[nf].setEdgeVisFlags(1,1,0);
				mesh.faces[nf].setSmGroup(1);
				mesh.faces[nf].setMatID(0);
				mesh.faces[nf].setVerts(na,nb,nc);
				nf++;

				mesh.faces[nf].setEdgeVisFlags(0,1,1);
				mesh.faces[nf].setSmGroup(1);
				mesh.faces[nf].setMatID(0);
				mesh.faces[nf].setVerts(na,nc,nd);
				nf++;
			}
	 	}

		// Make bottom conic cap
		na = mesh.getNumVerts()-1;
		int botsegs = segs;
		jx = (rows-1)*segs+1;lastseg=botsegs-1;
		for(ix=0; ix<botsegs; ++ix) 
		{
			nc = ix + jx;
			nb = nc+1;

			mesh.faces[nf].setEdgeVisFlags(1,1,1);
			mesh.faces[nf].setSmGroup(1);
			mesh.faces[nf].setMatID(0);
			mesh.faces[nf].setVerts(na, nb, nc);

			nf++;
		}

		IMaxDataStore imax;
		dmat::AttributeInst *pInst = GetAttributes(inode);

		int iValColour = 15;		
		int idxColour;

		if(strcmp(GetAttrClassName(inode),"Gta Collision") == 0)
		{
			if(GetDisplayMode() == 1)
			{
				idxColour = imax.GetAttribIndex(inode,"Day Brightness");

				if(idxColour != -1)
					pInst->GetValue(idxColour, iValColour);
			}
			else
			{
				idxColour = imax.GetAttribIndex(inode,"Night Brightness");

				if(idxColour != -1)
					pInst->GetValue(idxColour, iValColour);
			}
		}

		mesh.setNumTVerts(0);
		mesh.setNumTVFaces(0);
		mesh.buildNormals();
		mesh.DeleteFlaggedFaces();
		mesh.DeleteIsoVerts();
		mesh.InvalidateGeomCache();
		mesh.BuildStripsAndEdges();
		mesh.InvalidateTopologyCache();

		inode->SetMtl(NULL);
		inode->SetWireColor(RGB(iValColour * 17,iValColour * 17,iValColour * 17));

		mesh.render(gw,inode->Mtls(),NULL,COMP_ALL);

#else

#define PB_RADIUS	0
#define PB_SEGS		1

		if(!inode)
		{
			return;
		}

		SimpleObject* pSimple = (SimpleObject*)CreateInstance(GEOMOBJECT_CLASS_ID,Class_ID(SPHERE_CLASS_ID,0));

		if(!pSimple)
		{
			return;
		}

		pSimple->pblock->SetValue(PB_RADIUS,0, radius);				
		pSimple->pblock->SetValue(PB_SEGS,0, NUM_CIRCLE_SEGS);				
		pSimple->BuildMesh(t);

		BOOL needDelete;

		class NullView : public View 
		{
		public:

			Point2 ViewToScreen(Point3 p) { return Point2(p.x,p.y); }
			NullView() 
			{
				worldToView.IdentityMatrix();
				screenW=640.0f; screenH = 480.0f;
			}
		};

		NullView nullView;

		Mesh* pMesh = pSimple->GetRenderMesh(t,inode,nullView,needDelete);

		pMesh->render(gw,inode->Mtls(),NULL,COMP_ALL);

		if(needDelete)
		{
			delete pMesh;
		}

		delete pSimple;

#endif
	}
};
	
/*****************************************************************************
 ** NAME       || HitTest()
 ** PURPOSE    || See if the object's been hit. Arse!
 ** PARAMETERS || Shit loads of stuff....
 ** RETURNS    || An int - zero if it's not been hit, else one.
 *****************************************************************************/
int CollisionSphere::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;

	DWORD savedLimits;

	GraphicsWindow *gw = vpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(inode->GetObjectTM(t));
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	DrawCircles(gw, inode, t);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

/*****************************************************************************
 ** NAME       || GetWorldBoundBox()
 ** PURPOSE    || Sets box to be the world bounding box.
 ** PARAMETERS || Stuff
 ** RETURNS    || Nothing (box is passed by reference)
 *****************************************************************************/
void CollisionSphere::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	//The object can handle this by transforming the 8 points of its local 
	//bounding box into world space and take the minimums and maximums of 
	//the result.  Although this isn't necessarily the tightest bounding box 
	//of the objects points in world space, it is close enough.

	Matrix3 local2world = inode -> GetObjectTM(t);

	Point3 minPoint((float)FLT_MAX, (float)FLT_MAX, (float)FLT_MAX);
	Point3 maxPoint = -minPoint;

	Box3 localBox;
	
	GetLocalBoundBox(t, inode, vp, localBox);
	
	Point3 worldP;

	int i;

	for (i = 0; i < 8; i++)
	{
		worldP = local2world*localBox[i];
		if (worldP.x < minPoint.x)
		{
			minPoint.x = worldP.x;
		}
		if (worldP.y < minPoint.y)
		{
			minPoint.y = worldP.y;
		}
		if (worldP.z < minPoint.z)
		{
			minPoint.z = worldP.z;
		}

		if (worldP.x > maxPoint.x)
		{
			maxPoint.x = worldP.x;
		}
		if (worldP.y > maxPoint.y)
		{
			maxPoint.y = worldP.y;
		}
		if (worldP.z > maxPoint.z)
		{
			maxPoint.z = worldP.z;
		}
	}

	box = Box3(minPoint, maxPoint);

}

/*****************************************************************************
 ** NAME       || GetLocalBoundBox()
 ** PURPOSE    || Sets box to be the local bounding box.
 ** PARAMETERS || Stuff
 ** RETURNS    || Nothing (box is passed by reference)
 *****************************************************************************/
void CollisionSphere::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	
	float radius;	
	Interval ivalid;

	pBlock2 -> GetValue(colsphere_radius, t, radius, ivalid);

	Point3 maxPoint((float)radius, (float)radius, (float)radius);
	Point3 minPoint = -maxPoint;

	box = Box3(minPoint, maxPoint);
}


/*****************************************************************************
 ** NAME       || SetReference()
 ** PURPOSE    || Sets up pBlock.
 ** PARAMETERS || int i, and a RefTargetHandle
 ** RETURNS    || Nothing
 *****************************************************************************/
void CollisionSphere::SetReference(int i, RefTargetHandle rTarg)
{
	switch (i) 
	{
	case PBLOCK_REF_NUM: 	 
		pBlock2 = (IParamBlock2*)rTarg;
		break;
	}
}

RefTargetHandle CollisionSphere::GetReference(int i)
{
	if (i == PBLOCK_REF_NUM)
	{
		return pBlock2;
	}
	return NULL;
}

int CollisionSphere::NumRefs()
{
	return 1;
}

/*****************************************************************************
 ** NAME       || BeginEditParams()
 ** PURPOSE    || Read the manual!
 ** PARAMETERS || Some stuff.
 ** RETURNS    || Nothing
 *****************************************************************************/
void CollisionSphere::BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev)
{
	theCollisionSphereDesc.BeginEditParams(ip, this, flags, prev);

	colSphereDesc.SetUserDlgProc(new CollisionVolumeDlgProc());
}

/*****************************************************************************
 ** NAME       || EndEditParams()
 ** PURPOSE    || Read the manual.
 ** PARAMETERS || The usual gubbins
 ** RETURNS    || Nothing
 *****************************************************************************/
void CollisionSphere::EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)
{
	theCollisionSphereDesc.EndEditParams(ip, this, flags, next);
}


//
//        name: CollisionSphere::SetupFromSelected
// description: Setup collision sphere from selected Node
//          in: pSelected = pointer to selected node
//         out: 
//
BOOL CollisionSphere::SetupFromSelected(INode *pSelected, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Box3 local, world;
	Matrix3 mat = pSelected->GetNodeTM(t);
	Point3 centre;
	float radius = 0;

	mat.NoScale();

	GetGroupLocalBoundBox(pSelected, local, mat);
	GetGroupWorldBoundBox(pSelected, world);

	// work out centre and radius of collision sphere
	centre = world.Center();
	GetGroupRadius(pSelected, radius, local.Center(), mat);

	// set collision sphere node transformation matrix
	mat.SetTrans(centre);
	pNode->SetNodeTM(t, mat);
	// set radius
	pBlock2->SetValue(colsphere_radius, t, radius);

	// set controller
	Control *pOrienControl = new StaticRotationControl;
	Control *pTMControl = pNode->GetTMController();
	pTMControl->SetRotationController(pOrienControl);

	return TRUE;
}

/*******************************************************************************************/
/*******************************************************************************************/
/*******************************************************************************************/


