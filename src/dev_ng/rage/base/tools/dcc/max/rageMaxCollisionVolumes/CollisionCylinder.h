//
// filename:	CollisionCylinder.h
// description:	Defines Collision Cylinder class and associated descriptor
//

#ifndef INC_COLLISIONCYLINDER_H_
#define INC_COLLISIONCYLINDER_H_

// --- Include Files ------------------------------------------------------------

// 3D Studio Max SDK headers
#include "Max.h"
#include "istdplug.h"
#include "iparamm2.h"

// Local headers
#include "SelectCMode.h"
#include "ICollision.h"

// --- Structure/Class Definitions ----------------------------------------------

//
// Class:		CollisionCylinder
// Description:	The collision cylinder object
//
class CollisionCylinder : public CollisionHelperObject
{
public:
	CollisionCylinder( );

	void GetNameSuffix(TSTR &s) { s = "-ColCylinder"; }
	BOOL SetupFromSelected(INode *pSelected, INode *pNode);
	void DrawCircles(GraphicsWindow *gw, INode *inode, TimeValue t);

	// from Object
	ObjectState Eval(int){return ObjectState(this);}
	void InitNodeName(TSTR& s);

	// from BaseObject
	TCHAR *GetObjectName();
	CreateMouseCallBack* GetCreateMouseCallBack(void){return NULL;}
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
	void GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );

	// from ReferenceTarget
	RefTargetHandle Clone(RemapDir& remap);

	// from ReferenceMaker
	void SetReference(int i, RefTargetHandle rTarg);
	RefTargetHandle GetReference(int i);
	int NumRefs();
	RefResult NotifyRefChanged(Interval,RefTargetHandle,PartID &,RefMessage){return REF_SUCCEED;}

	// from Animatable
	int NumParamBlocks() {return 1;}
	IParamBlock2* GetParamBlock(int i) {assert(i == 0); return pBlock2;}
	IParamBlock2* GetParamBlockByID(short id) {assert(id == colsphere_params); return pBlock2;}
	void BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev=NULL);
	void EndEditParams(IObjParam *ip, ULONG flags, Animatable *next=NULL);
	void DeleteThis() {delete this;}
	Class_ID ClassID(){return COLLISION_CYLINDER_CLASS_ID;}
	void GetClassName(TSTR& s);

private:
	IParamBlock2 *pBlock2;	
};


//
//   Class Name: CollisionCylinderClassDesc
//  Description: Class descriptor
//
class CollisionCylinderClassDesc : public ClassDesc2 
{
public:
	int				IsPublic( ) { return 1; }	
	void*			Create( BOOL loading = FALSE ) { return new CollisionCylinder; }	
	const TCHAR*	ClassName( );
	SClass_ID		SuperClassID( ) { return HELPER_CLASS_ID; }	
	Class_ID		ClassID( ) { return COLLISION_CYLINDER_CLASS_ID; }	
	const TCHAR*	Category( );
	void			ResetClassParams( BOOL fileReset ) { }
	BOOL			OkToCreate( Interface *ip );
	int				BeginCreate( Interface *ip );
	int				EndCreate( Interface *ip );

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName( )				{ return _T("ColCylinder"); }
	HINSTANCE		HInstance( )				{ return hInstance; }
};

// --- Globals ------------------------------------------------------------------

extern TCHAR* GetString( int id );
extern HINSTANCE hInstance;

#endif // !INC_COLLISIONCYLINDER_H_
