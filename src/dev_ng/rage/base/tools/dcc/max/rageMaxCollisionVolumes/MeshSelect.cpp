//
//
//    Filename: MeshSelect.cpp
//     Creator: Adam Fowler
//     $Author: Adam $
//       $Date: 7/06/99 14:50 $
//   $Revision: 6 $
// Description: Implementation of standard mesh select local data and restore objects
//
//
// Max header
#include <Max.h>
// my headers
#include "MeshSelect.h"

// id for mesh select blocks
#define MESH_LOCAL_VERTSELECT_CHUNK 0x1087
#define MESH_LOCAL_FACESELECT_CHUNK 0x1088
#define MESH_LOCAL_EDGESELECT_CHUNK 0x1089

// static member variables
IObjParam *StdMeshSelectModifier::m_ip = NULL;
SelectModBoxCMode *StdMeshSelectModifier::m_pSelectMode = NULL;

// --- StdMeshSelectModifier ----------------------------------------------------------------------

//
//        name: StdMeshSelectModifier::StdMeshSelectModifier
// description: Constructor
//
StdMeshSelectModifier::StdMeshSelectModifier()
{
	m_selLevel = 1;
	m_faceSel = FACESEL_POLY;
	m_selectByVertex = false;
	m_selectThreshold = 1.0f;
}


//
//        name: StdMeshSelectModifier::~StdMeshSelectModifier
// description: Destructor
//
StdMeshSelectModifier::~StdMeshSelectModifier()
{
}


//
//        name: StdMeshSelectModifier::SetupSubObjEdit, ClearSubObjEdit
// description: Setup the sub-object selection and clear the sub-object selection
//          in: num = number of sub-object selection types
//
void StdMeshSelectModifier::SetupSubObjEdit(IObjParam *ip)
{
	m_ip = ip;

	// Add our sub object type
	//m_ip->RegisterSubObjectTypes(GetSelTypeNames(), GetNumSelTypes());

	// Restore the selection level.
	m_ip->SetSubObjectLevel(m_selLevel);

	// Disable show end result.
	m_ip->EnableShowEndResult(FALSE);
}

void StdMeshSelectModifier::ClearSubObjEdit(void)
{
	// remove select mode
	m_ip->DeleteMode(m_pSelectMode);

	// delete select mode
	if (m_pSelectMode) 
		delete m_pSelectMode;
	m_pSelectMode = NULL;

	// Enable show end result
	m_ip->EnableShowEndResult(TRUE);

	m_ip = NULL;
}

//
//        name: VolumeCreateMod::GetValidity
// description: Return the validity interval of the modifier
//          in: t = time
//         out: validity interval
//
Interval StdMeshSelectModifier::GetValidity(TimeValue t)
{
	// if being edited, return NEVER forces a cache to be built 
	// after previous modifier.
	if (TestAFlag(A_MOD_BEING_EDITED))
	{
		return NEVER;  
	}
	return FOREVER;
}

//
//        name: StdMeshSelectModifier::ModifyMesh
// description: Change selection data and setup display flags
//          in: mesh = reference to mesh
//				pData = pointer to local data
//
void StdMeshSelectModifier::ModifyMesh(Mesh &mesh, StdMeshSelectData *pData)
{

	// set the vertex selection for the mesh
	mesh.vertSel = pData->GetVertSel();
	mesh.faceSel = pData->GetFaceSel();
	mesh.edgeSel = pData->GetEdgeSel();

	// switch on vertex ticks if sub-object selection on
	mesh.dispFlags = 0;
	switch(GetSelTypes()[m_selLevel])
	{
	case IMESHSEL_VERTEX:
		mesh.SetDispFlag(DISP_VERTTICKS|DISP_SELVERTS);
		mesh.selLevel = MESH_VERTEX;
		break;
	case IMESHSEL_FACE:
		if(m_faceSel == FACESEL_SINGLE)
			mesh.SetDispFlag(DISP_SELFACES);
		else
			mesh.SetDispFlag(DISP_SELPOLYS);
		mesh.selLevel = MESH_FACE;
		break;
	case IMESHSEL_EDGE:
		mesh.SetDispFlag(DISP_SELEDGES);
		mesh.selLevel = MESH_EDGE;
		break;
	default:
		mesh.selLevel = MESH_OBJECT;
		break;
	}

}

//
//        name: StdMeshSelectModifier::NotifyInputChanged
// description: Called whenever the input mesh has changed. i.e. A vertex has been added
//
void StdMeshSelectModifier::NotifyInputChanged(Interval changeInt, PartID partID, RefMessage message, ModContext *mc)
{
	if(message == REFMSG_CHANGE)
	{
		if(mc->localData)
			((StdMeshSelectData *)mc->localData)->FreeCache();
	}
}

//
//        name: StdMeshSelectModifier::GetSelLevel,SetSelLevel
// description: Access version for selection level
//
DWORD StdMeshSelectModifier::GetSelLevel()
{
	return m_selLevel;
}

void StdMeshSelectModifier::SetSelLevel(DWORD level)
{
	m_selLevel = level;
	NotifyDependents(FOREVER, PART_SUBSEL_TYPE|PART_DISPLAY, REFMSG_CHANGE);
}

//
//        name: StdMeshSelectModifier::LocalDataChanged
// description: Called whenever the select set changes
//          in: 
//         out: 
//
void StdMeshSelectModifier::LocalDataChanged()
{
	NotifyDependents(FOREVER, PART_SELECT, REFMSG_CHANGE);
}


//
//
// Sub Object selection
//
//
//
//        name: StdMeshSelectModifier::HitTest
// description: Mouse collision code
//
int StdMeshSelectModifier::HitTest(TimeValue t, INode* pINode, int type, int crossing, 
					int flags, IPoint2 *p, ViewExp *vpt, ModContext* mc)
{	
	Interval valid = FOREVER;
	int savedLimits, res = 0;
	GraphicsWindow *gw = vpt->getGW();
	HitRegion hr;
	
	// Setup GW
	MakeHitRegion(hr,type, crossing,4,p);
	gw->setHitRegion(&hr);
	
	// Normally these methods expect world coordinates.  
	// However if this matrix is set to an objects transformation matrix you can pass objects 
	// space coordinates and they will be transformed into world space (and then put into 
	// screen space when they are drawn).

	Matrix3 mat = pINode->GetObjectTM(t);
	gw->setTransform(mat);	
	

	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setRndLimits(gw->getRndLimits() & ~GW_BACKCULL);
	gw->clearHitCode();
		
	SubObjHitList hitList;
	MeshSubHitRec *rec;	
	StdMeshSelectData *pData = (StdMeshSelectData*)mc->localData;
	if(pData == NULL)
		return 0;
	Mesh *pMesh = pData->GetMesh();
	if (pMesh == NULL)
		return 0;

	switch(GetSelTypes()[m_selLevel])
	{
	case IMESHSEL_VERTEX:
		flags |= SUBHIT_VERTS;
		break;
	case IMESHSEL_FACE:
		if(m_selectByVertex)
			flags |= SUBHIT_VERTS|SUBHIT_USEFACESEL;
		else
			flags |= SUBHIT_FACES;
		break;
	case IMESHSEL_EDGE:
		flags |= SUBHIT_EDGES;
		break;
	default:
		break;
	}

	res = pMesh->SubObjectHitTest(gw, gw->getMaterial(), &hr, flags, hitList);

	rec = hitList.First();
	while (rec) 
	{
		// rec->index is the index of the verSel/faceSel/edgeSel Bitarray
		vpt->LogHit(pINode,mc,rec->dist,rec->index,NULL);
		rec = rec->Next();
	}

	gw->setRndLimits(savedLimits);	
	
	return res;	
}

void StdMeshSelectModifier::ActivateSubobjSel(int level, XFormModes& modes)
{
	// store selection level
	SetSelLevel(level);

	// Create sub object editing mode.
	if(!m_pSelectMode)
		m_pSelectMode= new SelectModBoxCMode(this,m_ip);

	// If selection level doesn't equal 0 (i.e object level) fill in modes with 
	// our sub-object modes
	if (level != 0) 
	{
		modes = XFormModes(NULL,NULL,NULL,NULL,NULL,m_pSelectMode);
	}

	NotifyDependents(FOREVER, PART_SUBSEL_TYPE|PART_DISPLAY, REFMSG_CHANGE);
	m_ip->PipeSelLevelChanged();
	NotifyDependents(FOREVER, SELECT_CHANNEL|DISP_ATTRIB_CHANNEL|SUBSEL_TYPE_CHANNEL, REFMSG_CHANGE);
}


//
//        name: StdMeshSelectModifier::VertHitsToFaceHits
// description: Construct a face hit record from a vertex hit record
//          in: 
//         out: 
//
HitRecord *StdMeshSelectModifier::VertHitsToFaceHits(HitRecord *pHitRec,BOOL all)
{	
	HitRecord *ptr = NULL;
	
	while (pHitRec) {		
		Mesh &mesh = *(((StdMeshSelectData*)pHitRec->modContext->localData)->GetMesh());
		AdjEdgeList edgeList(mesh);		
		
		// Add faces adjacent to this vertex
		DWORDTab &list = edgeList[pHitRec->hitInfo];
		for (int i=0; i<list.Count(); i++) {
			if (edgeList.edges[list[i]].f[0] != UNDEFINED) {
				ptr = new HitRecord(			
					ptr,
					pHitRec->nodeRef,
					pHitRec->modContext,
					pHitRec->distance,
					edgeList.edges[list[i]].f[0],
					NULL);
			}
			if (edgeList.edges[list[i]].f[1] != UNDEFINED) {
				ptr = new HitRecord(				
					ptr,
					pHitRec->nodeRef,
					pHitRec->modContext,
					pHitRec->distance,
					edgeList.edges[list[i]].f[1],
					NULL);
			}
		}
		
		if (!all) 
			break;
		pHitRec = pHitRec->Next();
	}
	return ptr;
}

//
//        name: *StdMeshSelectModifier::BuildAdjFaceList
// description: Construct a list of adjacent faces
//          in: mesh = mesh that contains faces
//         out: 
//
AdjFaceList *BuildAdjFaceList(Mesh &mesh)
{
	AdjEdgeList edgeList(mesh);
	return new AdjFaceList(mesh, edgeList);
}

// 
// parse hit record and set information in ModContext attached dependent on the data 
// in the hit record
//
void StdMeshSelectModifier::SelectSubComponent(HitRecord *pHitRec, BOOL selected, BOOL all, BOOL invert)
{
	StdMeshSelectData *pSelData = NULL;
	HitRecord *pFaceHitRec = NULL;
	ModContextList modList;
	INodeTab nodeList;
	BitArray array;
	TimeValue t = m_ip->GetTime();
	int i;

	m_ip->GetModContexts(modList,nodeList);

	// work out face selection from vertex selection if so required
	if(GetSelTypes()[m_selLevel] == IMESHSEL_FACE && m_selectByVertex)
	{
		pHitRec = pFaceHitRec = VertHitsToFaceHits(pHitRec, all);
		all = TRUE;
	}

	for(i = 0; i<modList.Count(); i++)
	{
		pSelData = static_cast<StdMeshSelectData*>(modList[i]->localData);
		HitRecord *pHR = pHitRec;

		if(pSelData == NULL)
			continue;

		// get current selection
		switch(GetSelTypes()[m_selLevel])
		{
		case IMESHSEL_VERTEX:
			array = pSelData->GetVertSel();
			break;
		case IMESHSEL_FACE:
			array = pSelData->GetFaceSel();
			break;
		case IMESHSEL_EDGE:
			array = pSelData->GetEdgeSel();
			break;
		default:
			assert(0);
		}

		if(GetSelTypes()[m_selLevel] == IMESHSEL_FACE &&
			(m_faceSel == FACESEL_POLY || m_faceSel == FACESEL_ELEMENT))
		{
			Mesh *pMesh = pSelData->GetMesh();
			AdjFaceList *pAdjFaces;
			if (!pMesh) 
				continue;
			pAdjFaces = BuildAdjFaceList(*pMesh);
			// initialize selection array
			array = pSelData->GetFaceSel();
			BitArray tempArray(array.GetSize());
			tempArray.ClearAll();				

			while(pHR)
			{
				// check is right mod-context
				if(pHR->modContext->localData != pSelData)
					continue;

				if (m_faceSel == FACESEL_POLY)
					pMesh->PolyFromFace (pHR->hitInfo, tempArray, 90.0f, FALSE, pAdjFaces);
				else
					pMesh->ElementFromFace (pHR->hitInfo, tempArray, pAdjFaces);


				if (!all) 
					break;

				pHR = pHR->Next();
			}

			if (invert) tempArray ^= array;
			else if (selected) tempArray |= array;
			else tempArray = array & ~tempArray;
			array = tempArray;
		}
		else
		{
			while(pHR)
			{
				// check is right mod-context
				if(pHR->modContext->localData != pSelData)
				{
					pHR = pHR->Next();
					continue;
				}
	
				//the selected state doesnt seem to work so
				//we are just going to have to invert I think

				//state = selected;

				if(array[pHR->hitInfo] == TRUE)
				{
					array.Set(pHR->hitInfo, FALSE);
				}
				else
				{
					//if(invert)
					//	state = !(array)[pHR->hitInfo];
					array.Set(pHR->hitInfo, TRUE);
				}
				
				if (!all) 
					break;

				pHR = pHR->Next();
			}
		}
		// set selection
		switch(GetSelTypes()[m_selLevel])
		{
		case IMESHSEL_VERTEX:
			pSelData->SetVertSel(array, static_cast<IMeshSelect *>(this), t);
			break;
		case IMESHSEL_FACE:
			pSelData->SetFaceSel(array, static_cast<IMeshSelect *>(this), t);
			break;
		case IMESHSEL_EDGE:
			pSelData->SetEdgeSel(array, static_cast<IMeshSelect *>(this), t);
			break;
		default:
			assert(0);
		}
	}

	// remove face selection
	if(GetSelTypes()[m_selLevel] == IMESHSEL_FACE && m_selectByVertex)
	{
		HitRecord *ptr;
		while(pFaceHitRec)
		{
			ptr = pFaceHitRec;
			pFaceHitRec = pFaceHitRec->Next();
			delete ptr;
		}
	}

	nodeList.DisposeTemporary();
	LocalDataChanged();
}

//
// These are the three functions called from the edit menu when
// in selection mode
//
void StdMeshSelectModifier::ClearSelection(int m_selLevel)
{
	ModContextList list;
	INodeTab nodes;
	BitArray array;
	TimeValue t = m_ip->GetTime();

	if(GetSelTypes()[m_selLevel] == IMESHSEL_OBJECT)
		return;

	m_ip->GetModContexts(list,nodes);
	StdMeshSelectData *pSelData;
	
	for (int i=0; i<list.Count(); i++) {
		pSelData = (StdMeshSelectData*)list[i]->localData;

		switch(GetSelTypes()[m_selLevel])
		{
		case IMESHSEL_VERTEX:
			array.SetSize(pSelData->GetVertSel().GetSize());
			array.ClearAll();
			pSelData->SetVertSel(array, static_cast<IMeshSelect *>(this), t); 
			break;
		case IMESHSEL_FACE:
			array.SetSize(pSelData->GetFaceSel().GetSize());
			array.ClearAll();
			pSelData->SetFaceSel(array, static_cast<IMeshSelect *>(this), t); 
			break;
		case IMESHSEL_EDGE:
			array.SetSize(pSelData->GetEdgeSel().GetSize());
			array.ClearAll();
			pSelData->SetEdgeSel(array, static_cast<IMeshSelect *>(this), t); 
			break;
		}
	}

	nodes.DisposeTemporary();
	LocalDataChanged();
}

void StdMeshSelectModifier::SelectAll(int m_selLevel)
{
	ModContextList list;
	INodeTab nodes;
	BitArray array;
	TimeValue t = m_ip->GetTime();
	
	if(GetSelTypes()[m_selLevel] == IMESHSEL_OBJECT)
		return;

	m_ip->GetModContexts(list,nodes);
	StdMeshSelectData *pSelData;
	for (int i=0; i<list.Count(); i++) {
		pSelData = (StdMeshSelectData*)list[i]->localData;		

		switch(GetSelTypes()[m_selLevel])
		{
		case IMESHSEL_VERTEX:
			array.SetSize(pSelData->GetVertSel().GetSize());
			array.SetAll();
			pSelData->SetVertSel(array, static_cast<IMeshSelect *>(this), t); 
			break;
		case IMESHSEL_FACE:
			array.SetSize(pSelData->GetFaceSel().GetSize());
			array.SetAll();
			pSelData->SetFaceSel(array, static_cast<IMeshSelect *>(this), t); 
			break;
		case IMESHSEL_EDGE:
			array.SetSize(pSelData->GetEdgeSel().GetSize());
			array.SetAll();
			pSelData->SetEdgeSel(array, static_cast<IMeshSelect *>(this), t); 
			break;
		}
	}

	nodes.DisposeTemporary();
	LocalDataChanged();
}

void StdMeshSelectModifier::InvertSelection(int m_selLevel)
{
	ModContextList list;
	INodeTab nodes;	
	BitArray array;
	TimeValue t = m_ip->GetTime();

	if(GetSelTypes()[m_selLevel] == IMESHSEL_OBJECT)
		return;

	m_ip->GetModContexts(list,nodes);
	StdMeshSelectData *pSelData;
	for (int i=0; i<list.Count(); i++) {
		pSelData = (StdMeshSelectData*)list[i]->localData;

		switch(GetSelTypes()[m_selLevel])
		{
		case IMESHSEL_VERTEX:
			array = pSelData->GetVertSel();
			pSelData->SetVertSel(~array, this, t);
			break;
		case IMESHSEL_FACE:
			array = pSelData->GetFaceSel();
			pSelData->SetFaceSel(~array, this, t);
			break;
		case IMESHSEL_EDGE:
			array = pSelData->GetEdgeSel();
			pSelData->SetEdgeSel(~array, this, t);
			break;
		}
	}

	nodes.DisposeTemporary();
	LocalDataChanged();
}

//
//        name: StdMeshSelectModifier::GetSubObjectCenters, GetSubObjectTMs
// description: Return the centre of the sub object selection and the transformation matrix of the sub object selection
//
void StdMeshSelectModifier::GetSubObjectCenters(SubObjAxisCallback *pCallback,TimeValue t,INode *pNode,ModContext *mc)
{
	StdMeshSelectData *pSelData = static_cast<StdMeshSelectData*>(mc->localData);
	if(pSelData == NULL)
		return;
	Box3 box;

	pSelData->GetLocalBoundBox(this, box);
	box = box * pNode->GetObjectTM(t);
	pCallback->Center (box.Center(),0);
}
void StdMeshSelectModifier::GetSubObjectTMs(SubObjAxisCallback *pCallback,TimeValue t,INode *pNode,ModContext *mc)
{
	StdMeshSelectData *pSelData = static_cast<StdMeshSelectData*>(mc->localData);
	if(pSelData == NULL)
		return;
	Box3 box;
	Matrix3 tm(1);

	pSelData->GetLocalBoundBox(this, box);
	box = box * pNode->GetObjectTM(t);
	tm.SetTrans(box.Center());
	pCallback->TM(tm,0);
}

//
//        name: StdMeshSelectModifier::LoadLocalData,SaveLocalData
// description: Load/Save modifier local data
//
IOResult StdMeshSelectModifier::LoadLocalData(ILoad *iload, LocalModData **ppModData)
{
	IOResult res;
	StdMeshSelectData *pData = static_cast<StdMeshSelectData *>(*ppModData);

	while (IO_OK == (res = iload->OpenChunk())) {
		switch(iload->CurChunkID())  
		{
		case MESH_LOCAL_VERTSELECT_CHUNK:
			{
				// create local data if not already created
				if(pData == NULL)
				{
					pData = new StdMeshSelectData;
					*ppModData = pData;
				}
				// vertex selected
				BitArray array;
				res = array.Load(iload);
				pData->SetVertSel(array, static_cast<IMeshSelect *>(this), 0);
			}
			break;
		case MESH_LOCAL_FACESELECT_CHUNK:
			{
				// create local data if not already created
				if(pData == NULL)
				{
					pData = new StdMeshSelectData;
					*ppModData = pData;
				}
				// face selected
				BitArray array;
				res = array.Load(iload);
				pData->SetFaceSel(array, static_cast<IMeshSelect *>(this), 0);
			}
			break;
		case MESH_LOCAL_EDGESELECT_CHUNK:
			{
				// create local data if not already created
				if(pData == NULL)
				{
					pData = new StdMeshSelectData;
					*ppModData = pData;
				}
				// edge selected
				BitArray array;
				res = array.Load(iload);
				pData->SetEdgeSel(array, static_cast<IMeshSelect *>(this), 0);
			}
			break;
		default:
			break;
		}
		iload->CloseChunk();
		if (res != IO_OK) 
			return res;
	}

	return IO_OK;
}
IOResult StdMeshSelectModifier::SaveLocalData(ISave *isave, LocalModData *pModData)
{
	StdMeshSelectData *pData = static_cast<StdMeshSelectData *>(pModData);
	if(pData)
	{
		isave->BeginChunk(MESH_LOCAL_VERTSELECT_CHUNK);
		pData->GetVertSel().Save(isave);
		isave->EndChunk();
		isave->BeginChunk(MESH_LOCAL_FACESELECT_CHUNK);
		pData->GetFaceSel().Save(isave);
		isave->EndChunk();
		isave->BeginChunk(MESH_LOCAL_EDGESELECT_CHUNK);
		pData->GetEdgeSel().Save(isave);
		isave->EndChunk();
	}
	return IO_OK;
}

// --- StdMeshSelectData --------------------------------------------------------------------------

//
//        name: StdMeshSelectData::StdMeshSelectData
// description: Copy constructor
//
StdMeshSelectData::StdMeshSelectData(const StdMeshSelectData &meshSelData) : m_isHeld(false) 
{
	m_vertSel = meshSelData.m_vertSel; 
	m_faceSel = meshSelData.m_faceSel;
	m_edgeSel = meshSelData.m_edgeSel;
	m_pMesh = NULL; 
}

//
//        name: VCBlendData::VCBlendData
// description: Constructor. copies selection arrays
//          in: mesh = pointer to mesh
//
StdMeshSelectData::StdMeshSelectData(Mesh &mesh) : m_isHeld(false) 
{
	m_vertSel = mesh.vertSel; 
	m_faceSel = mesh.faceSel;
	m_edgeSel = mesh.edgeSel;
	m_pMesh = NULL; 
}

//
//        name: VCBlendData::~VCBlendData
// description: Destructor
//          in: 
//         out: 
//
StdMeshSelectData::~StdMeshSelectData()
{
	FreeCache();

}

//
//        name: VCBlendData::Clone
// description: Produce a clone of the mod context
//          in: 
//         out: 
//
LocalModData* StdMeshSelectData::Clone(void)
{ 
	return new StdMeshSelectData(*this);
}

//
//        name: VCBlendData::SetCache
// description: This sets the pointer to the cached mesh. It makes sure the vertex/face/edge 
//				selection arrays are the correct size
//          in: mesh = reference of mesh to cache
//
void StdMeshSelectData::SetCache(Mesh &mesh) 
{
	if(m_pMesh) 
		delete m_pMesh; 
	m_pMesh = new Mesh(mesh);

	m_vertSel.SetSize(mesh.vertSel.GetSize(),TRUE);
	m_faceSel.SetSize(mesh.faceSel.GetSize(),TRUE);
	m_edgeSel.SetSize(mesh.edgeSel.GetSize(),TRUE);
}

//
//        name: VCBlendData::FreeCache
// description: 
//          in: 
//         out: 
//
void StdMeshSelectData::FreeCache()
{
	if(m_pMesh) 
		delete m_pMesh; 
	m_pMesh = NULL;
}


//
//        name: StdMeshSelectData::SetVertSel
// description: Set vertex/face/edge selection
//
void StdMeshSelectData::SetVertSel(BitArray &set, IMeshSelect *pMod, TimeValue t) 
{ 
	if (theHold.Holding() && !IsHeld()) 
		theHold.Put(new MeshSelRestore(pMod, this, IMESHSEL_VERTEX));
	m_vertSel = set;
}
void StdMeshSelectData::SetFaceSel(BitArray &set, IMeshSelect *pMod, TimeValue t) 
{ 
	if (theHold.Holding() && !IsHeld()) 
		theHold.Put(new MeshSelRestore(pMod, this, IMESHSEL_FACE));
	m_faceSel = set;
}
void StdMeshSelectData::SetEdgeSel(BitArray &set, IMeshSelect *pMod, TimeValue t) 
{ 
	if (theHold.Holding() && !IsHeld()) 
		theHold.Put(new MeshSelRestore(pMod, this, IMESHSEL_EDGE));
	m_edgeSel = set;
}

//
//        name: StdMeshSelectData::TempVertSel
// description: Return a vertex selection array based on either vertex, face, edges currently being selected
//
BitArray StdMeshSelectData::GetTempVertSel(int selLevel)
{
	BitArray vertSel;
	int i, j;

	vertSel.SetSize (m_vertSel.GetSize());

	if(m_pMesh == NULL)
	{
		vertSel.ClearAll();
		return vertSel;
	}

	switch (selLevel) {
	case IMESHSEL_OBJECT:
		vertSel.SetAll();
		break;
	case IMESHSEL_VERTEX:
		return m_vertSel;
		break;

	case IMESHSEL_EDGE:
		vertSel.ClearAll ();
		for (i=0; i<m_pMesh->numFaces; i++) {
			for (j=0; j<3; j++) {
				if (m_edgeSel[i*3+j]) {
					vertSel.Set(m_pMesh->faces[i].v[j]);
					vertSel.Set(m_pMesh->faces[i].v[(j+1)%3]);
				}
			}
		}
		break;

	default:
		vertSel.ClearAll ();
		for (i=0; i<m_pMesh->numFaces; i++) {
			if (!m_faceSel[i]) 
				continue;
			vertSel.Set(m_pMesh->faces[i].v[0]);
			vertSel.Set(m_pMesh->faces[i].v[1]);
			vertSel.Set(m_pMesh->faces[i].v[2]);
		}
		break;
	}
	return vertSel;
}

//
//        name: StdMeshSelectData::GetLocalBoundBox
// description: Get bounding box of selected elements
//
void StdMeshSelectData::GetLocalBoundBox(StdMeshSelectModifier *pMod, Box3& box)
{
	box.Init();

	if(m_pMesh == NULL)
		return;

	BitArray array = GetTempVertSel(pMod->GetSelTypes()[pMod->GetSelLevel()]);
	int i;

	for(i=0; i<array.GetSize(); i++)
	{
		if(array[i])
			box += m_pMesh->getVert(i);
	}
}

//
//        name: StdMeshSelectData::GetTransformedBoundBox
// description: Get bounding box of selected elements transformed
//
void StdMeshSelectData::GetTransformedBoundBox(StdMeshSelectModifier *pMod, Matrix3& mat, Box3& box)
{
	box.Init();

	if(m_pMesh == NULL)
		return;

	BitArray array = GetTempVertSel(pMod->GetSelTypes()[pMod->GetSelLevel()]);
	int i;

	for(i=0; i<array.GetSize(); i++)
	{
		if(array[i])
			box += m_pMesh->getVert(i) * mat;
	}
}

//
// --- MeshSelRestore ----------------------------------------------------------------------------
//
MeshSelRestore::MeshSelRestore(IMeshSelect *pM, StdMeshSelectData *pData, int level)
{
	m_pMod = pM;
	m_pSelData = pData;
	m_level = level;
	m_pSelData->SetHeld(true);
	switch(m_level)
	{
	case IMESHSEL_VERTEX:
		m_usel = m_pSelData->GetVertSel();
		break;
	case IMESHSEL_FACE:
		m_usel = m_pSelData->GetFaceSel();
		break;
	case IMESHSEL_EDGE:
		m_usel = m_pSelData->GetEdgeSel();
		break;
	default:
		assert(0);
	}
}

void MeshSelRestore::Restore(int isUndo)
{
	if (isUndo) {
		switch(m_level)
		{
		case IMESHSEL_VERTEX:
			m_rsel = m_pSelData->GetVertSel();
			break;
		case IMESHSEL_FACE:
			m_rsel = m_pSelData->GetFaceSel();
			break;
		case IMESHSEL_EDGE:
			m_rsel = m_pSelData->GetEdgeSel();
			break;
		}
	}
	switch(m_level)
	{
	case IMESHSEL_VERTEX:
		m_pSelData->SetVertSel(m_usel, m_pMod, 0);
		break;
	case IMESHSEL_FACE:
		m_pSelData->SetFaceSel(m_usel, m_pMod, 0);
		break;
	case IMESHSEL_EDGE:
		m_pSelData->SetEdgeSel(m_usel, m_pMod, 0);
		break;
	}
	m_pMod->LocalDataChanged();
}

void MeshSelRestore::Redo()
{
	switch(m_level)
	{
	case IMESHSEL_VERTEX:
		m_pSelData->SetVertSel(m_rsel, m_pMod, 0);
		break;
	case IMESHSEL_FACE:
		m_pSelData->SetFaceSel(m_rsel, m_pMod, 0);
		break;
	case IMESHSEL_EDGE:
		m_pSelData->SetEdgeSel(m_rsel, m_pMod, 0);
		break;
	}
	m_pMod->LocalDataChanged();
}


