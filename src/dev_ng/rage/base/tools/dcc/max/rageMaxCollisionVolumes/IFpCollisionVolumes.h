//
// File:: IFpCollisionVolumes.h
// Description:: Function publishing interface for the rageMaxCollisionVolumes plugin.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 31 May 2010
//

#ifndef __IFPCOLLISIONVOLUMES_H__
#define __IFPCOLLISIONVOLUMES_H__

#include "maxscript/kernel/value.h"

#define FP_COLLISIONVOLUMES_INTERFACE Interface_ID(0x59645c5, 0xba30af8)

#define GetCollisionVolumesInterface() \
	(IFpCollisionVolumes*)GetCOREInterface( FP_COLLISIONVOLUMES_INTERFACE );

class IFpCollisionVolumes : public FPStaticInterface
{
public:
	virtual ~IFpCollisionVolumes() {}

	virtual int		GetPolycount( INode* pNode ) = 0;
	virtual INode*	CreateOnNode( INode* pNode, ClassDesc *type ) = 0;
	virtual void	SetUseWireframeColour( INode* pNode, BOOL onOff ) = 0;
	virtual BOOL	GetUseWireframeColour( INode* pNode ) = 0;
	virtual void	SetNodeColour( INode* pNode, Color *pCol ) = 0;
	virtual Color	GetNodeColour( INode* pNode ) = 0;

	enum { 
		em_getPolyCount, 
		em_CreateOnNode, 
		em_SetUseWireframeColour,
		em_GetUseWireframeColour,
		em_SetNodeColour,
		em_GetNodeColour
	};
};

#endif //__IFPCOLLISIONVOLUMES_H__
