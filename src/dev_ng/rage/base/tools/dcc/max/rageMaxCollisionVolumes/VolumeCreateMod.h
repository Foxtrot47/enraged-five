//
//
//    Filename: VolumeCreateMod.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Modifier plugin used to create collision volumes
//
//
#ifndef INC_VOLUME_CREATE_H_
#define INC_VOLUME_CREATE_H_

#include <Max.h>
#include "MeshSelect.h"

extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define VOLUMECREATE_CLASS_ID	Class_ID(0x873f7c, 0x98e0ff8)


//
//   Class Name: VolumeCreateMod
// Base Classes: StdMeshSelectModifier
//  Description: 
//    Functions: 
//
//
class VolumeCreateMod : public StdMeshSelectModifier {

	static HWND m_hPanel;
	static HIMAGELIST m_hImageList;
	static ICustToolbar *m_pToolbar;
	static ICustButton *m_pSingleButton;
	static ICustButton *m_pPolyButton;
	static ICustButton *m_pElementButton;
	static ICustButton *m_pSphereButton;
	static ICustButton *m_pSphereArrayButton;
	static ICustButton *m_pBoxButton;
	static ICustButton *m_pMeshButton;
	static ISpinnerControl *m_pXSpinner;
	static ISpinnerControl *m_pYSpinner;
	static ISpinnerControl *m_pZSpinner;
	static ISpinnerControl *m_pRadiusSpinner;
	static int m_xsize, m_ysize, m_zsize;
	static float m_radius;
	static BOOL m_maximise;

public:
	static VolumeCreateMod *m_pCurrentModifier;

	//Constructor/Destructor
	VolumeCreateMod();
	~VolumeCreateMod();

	// Collision object creation
	void CreateMesh();

	void CreateVolumes(void (VolumeCreateMod::*func)(StdMeshSelectData *pSelData, INode *pNode));
	void CreateSphere(StdMeshSelectData *pSelData, INode *pNode);
	void CreateSphereArray(StdMeshSelectData *pSelData, INode *pNode);
	void CreateObjectAlignedBox(StdMeshSelectData *pSelData, INode *pNode);
	void CreateWorldAlignedBox(StdMeshSelectData *pSelData, INode *pNode);
	void CreateBox(StdMeshSelectData *pSelData, INode *pNode);
	void CreateBox(StdMeshSelectData *pSelData, INode *pNode, Matrix3& localMat);
	void CreateCapsule(StdMeshSelectData *pSelData, INode *pNode);
	void CreateCapsule(StdMeshSelectData *pSelData, INode *pNode, Matrix3& localMat);
	void CreateCylinder(StdMeshSelectData *pSelData, INode *pNode);
	void CreateCylinder(StdMeshSelectData *pSelData, INode *pNode, Matrix3& localMat);
	void CreateMesh(StdMeshSelectData *pSelData, INode *pNode);
	void CreateShad(StdMeshSelectData *pSelData, INode *pNode);
	bool CanCreateVolumes(bool (VolumeCreateMod::*func)(StdMeshSelectData *pSelData));
	bool CanCreateSphere(StdMeshSelectData *pSelData);
	bool CanCreateSphereArray(StdMeshSelectData *pSelData);
	bool CanCreateBox(StdMeshSelectData *pSelData);
	bool CanCreateMesh(StdMeshSelectData *pSelData);

	// UI
	void AddRollup(int level);
	void RemoveRollup();
	void UpdateUI();
	void LoadFaceToolbar();
	void SetFaceSelection(int selection);

	// from StdMeshSelectModifier
	virtual const char **GetSelTypeNames();
	virtual const int *GetSelTypes();
	virtual int GetNumSelTypes();
	virtual void LocalDataChanged();

	// From Animatable
	void GetClassName(TSTR& s);
	virtual Class_ID ClassID() { return VOLUMECREATE_CLASS_ID;}		
	TCHAR *GetObjectName();
	void BeginEditParams(IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams(IObjParam *ip, ULONG flags,Animatable *next);

	//From Modifier
	void ModifyObject(TimeValue t, ModContext &mc, ObjectState *os, INode *node);

	// From BaseObject
	BOOL ChangeTopology() {return TRUE;}
	void ActivateSubobjSel(int level, XFormModes& modes);
	int NumSubObjTypes();
	ISubObjType *GetSubObjType(int i);

	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());

	RefResult NotifyRefChanged( Interval changeInt,RefTargetHandle hTarget, 
		PartID& partID, RefMessage message) {return REF_SUCCEED;}
};



//
//   Class Name: VolumeCreateModClassDesc
// Base Classes: ClassDesc
//  Description: Class description
//    Functions: 
//
//
class VolumeCreateModClassDesc : public ClassDesc 
{
public:
	int 			IsPublic() {return 1;}
	void *			Create(BOOL loading = FALSE) {return new VolumeCreateMod();}
	const TCHAR *	ClassName();
	SClass_ID		SuperClassID() {return OSM_CLASS_ID;}
	Class_ID		ClassID() {return VOLUMECREATE_CLASS_ID;}
	const TCHAR* 	Category();
	void			ResetClassParams (BOOL fileReset);

	// Hardwired name, used by MAX Script as unique identifier
	const TCHAR*	InternalName()				{ return _T("VolumeCreate"); }
	HINSTANCE		HInstance()					{ return hInstance; }


};


#endif //INC_VOLUME_CREATE_H_
