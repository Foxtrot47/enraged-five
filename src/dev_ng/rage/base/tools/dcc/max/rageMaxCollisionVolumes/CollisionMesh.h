//
//
//    Filename: CollisionMesh.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: A collision volume based on a polygon mesh 
//
//
#ifndef INC_COLLISION_MESH_H
#define INC_COLLISION_MESH_H

#include "Max.h"
#include "istdplug.h"
#include "iparamm.h"
#include "meshadj.h"
#include "meshdelta.h"
#include <gup.h>

#include "SelectCMode.h"
#include "ICollision.h"

TCHAR *GetString(int id);

extern HINSTANCE hInstance;


#define COLLISIONMESH_ACTIONTABLEID		0x7d1e39ed
#define COLLISIONMESH_ACTIONCONTEXTID	0x727b51cc
#define COLLISIONMESH_MENUCONTEXTID		0x14f30ffc

enum CollisionMeshActions {
	ConvertToEditableMeshActionId,
	ConvertToColMeshActionId,
	ConvertToShadMeshActionId
};

//////////////////////////////////////////////////////////////////////////
// forward declarations
//////////////////////////////////////////////////////////////////////////
void AddQuadMenuItems(Interface* ip);

class CollisionMeshDummyGUP : public GUP
{
public:
	DWORD Start();
	void Stop() {}

	void DeleteThis()
	{
		delete this;
	}
};

//
//   Class Name: CollisionMesh
// Base Classes: CollisionHelperObject
//  Description: 
//    Functions: 
//
//
////////////////////////////////////////////////////////////////////////////////////////////
class CollisionMesh : public CollisionHelperObject
////////////////////////////////////////////////////////////////////////////////////////////
{
	friend class VolumeCreateMod;

public:

	static void InitQuad();

	CollisionMesh();

	virtual void GetNameSuffix(TSTR &s) {s = "-ColMesh";}
	BOOL SetupFromSelected(INode *pSelected, INode *pNode);
	void DrawMesh(INode* inode,GraphicsWindow *gw, TimeValue t, Matrix3& objMat, Color lineCol, bool avoidGWFlags=false);
	void CopyMesh(TriObject *pTriObject, Matrix3& mat = Matrix3(1));

	Interval ChannelValidity(TimeValue t, int nchan) { return FOREVER; }

	// from Object
	ObjectState Eval(int){return ObjectState(this);}
	virtual void InitNodeName(TSTR& s);
	int CanConvertToType(Class_ID obtype);
	Object* ConvertToType(TimeValue t, Class_ID obtype);

//	SClass_ID SuperClassID() { return GEOMOBJECT_CLASS_ID; }

	// from BaseObject
	virtual TCHAR *GetObjectName();
	CreateMouseCallBack* GetCreateMouseCallBack(void){return NULL;}
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt,ModContext* mc);
	void GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box );
	void GetSubObjectCenters(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc);
	void GetSubObjectTMs(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc);		

	int NumSubObjTypes();
	ISubObjType *GetSubObjType(int i);
	void ActivateSubobjSel(int level, XFormModes& modes );
	virtual void SelectSubComponent(HitRecord *hitRec, BOOL selected, BOOL all, BOOL invert=FALSE);
	void ClearSelection(int sl);

	// from ReferenceTarget
	RefTargetHandle Clone(RemapDir& remap);

	// from ReferenceMaker
	int NumRefs() {return 1;}
	RefTargetHandle GetReference(int i) {return m_pTriObject;}
	void SetReference(int i, RefTargetHandle rtarg) {m_pTriObject = (TriObject *)rtarg;}
	RefResult NotifyRefChanged(Interval, RefTargetHandle, PartID&, unsigned int){return REF_SUCCEED;}

	// from Animatable
	void BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev=NULL);
	void EndEditParams(IObjParam *ip, ULONG flags, Animatable *next=NULL);
	void DeleteThis() {delete this;}
	virtual Class_ID ClassID(){return COLLISION_MESH_CLASS_ID;}
	virtual void GetClassName(TSTR& s);

	Mesh& GetMesh() { return m_pTriObject->GetMesh(); }
	MeshTempData *TempData ();
	void InvalidateTempData (PartID parts=PART_TOPO|PART_GEOM);
	void UpdateSurfaceSpinner (TimeValue t, HWND hWnd, int idSpin);
	DWORD GetMatIndex ();
	void SetMatIndex (DWORD index);
	void ApplyMeshDelta (MeshDelta & md, MeshDeltaUser *mdu, TimeValue t);
	void LocalDataChanged();

	static MoveModBoxCMode *moveMode;
	static RotateModBoxCMode *rotMode;
	static UScaleModBoxCMode *uscaleMode;
	static NUScaleModBoxCMode *nuscaleMode;
	static SquashModBoxCMode *squashMode;
	static SelectModBoxCMode *selectMode;

	MeshTempData *tempData;
	int m_selLevel;
	TriObject* m_pTriObject;
	HWND m_hWnd;
	HWND m_hFaceWnd;
};

////////////////////////////////////////////////////////////////////////////////////////////
class CollisionMeshClassDesc : public ClassDesc2
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	CollisionMeshClassDesc();

	int IsPublic() {return 1;}
	virtual void* Create(BOOL loading = FALSE) { return new CollisionMesh(); }
	virtual const TCHAR* ClassName();
	SClass_ID SuperClassID() {return HELPER_CLASS_ID;}
	Class_ID ClassID(){return COLLISION_MESH_CLASS_ID;}
	const TCHAR* Category();
	virtual const TCHAR*	InternalName()				{ return _T("ColMesh"); }

//	void ResetClassParams(BOOL fileReset){}
	BOOL OkToCreate(Interface *ip);
	int BeginCreate(Interface *ip);
	int EndCreate(Interface *ip);

	HINSTANCE		HInstance()					{ return hInstance; }

	virtual int NumActionTables() {return 1;}
	ActionTable* GetActionTable(int i);
};

class CollisionMeshDummyClassDesc : public ClassDesc2
{
   int          IsPublic()     { return FALSE; }   
   void*        Create(BOOL loading = FALSE) { return new CollisionMeshDummyGUP(); }   
   const TCHAR* ClassName()    { return _T("ColMeshDummy"); }   
   SClass_ID    SuperClassID() { return GUP_CLASS_ID; }   
   Class_ID     ClassID()      { return COLLISION_MESHHELPER_CLASS_ID; }   
   const TCHAR* Category()     { return _T("ColMeshDummy"); }    
   const TCHAR* InternalName() { return _T("ColMeshDummy"); }       
   HINSTANCE    HInstance()    { return hInstance; } 
};

////////////////////////////////////////////////////////////////////////////////////////////
class ShadowMesh : public CollisionMesh
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	void GetNameSuffix(TSTR &s) {s = "-ShadMesh";}
	Class_ID ClassID(){return COLLISION_MESHSHAD_CLASS_ID;}
	void GetClassName(TSTR& s);
	TCHAR *GetObjectName();
	void InitNodeName(TSTR& s);
};

////////////////////////////////////////////////////////////////////////////////////////////
class CollisionMeshShadowClassDesc : public CollisionMeshClassDesc
////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	void* Create(BOOL loading = FALSE) {return new ShadowMesh();}
	Class_ID ClassID(){return COLLISION_MESHSHAD_CLASS_ID;}
	const TCHAR*	InternalName()				{ return _T("ShadMesh"); }
	const TCHAR* ClassName();
	virtual int NumActionTables() {return 0;}
};

// functions for converting from collision mesh to editable mesh and vice versa
void ConvertToEditableMesh(INode *pNode, Interface *ip);
void ConvertToColMesh(INode *pNode, Interface *ip);
void ConvertToShadMesh(INode *pNode, Interface *ip);

#endif // INC_COLLISION_MESH_H

