//
// filename:	CollisionCylinder.cpp
// description:	Collision Cylinder class and associated descriptor implementation
//

// --- Include Files ------------------------------------------------------------

#include "CollisionCylinder.h"

// 3D Studio Max SDK headers
#include "simpobj.h"

// Game headers
#include "CollisionControllers.h"
#include "resource.h"

// --- Constants ----------------------------------------------------------------

const int sC_cnt_NumberCircleSegments	= 24;
const int sC_idx_ParamBlockRadius		= 0;	//!< Radius parameter block index
const int sC_idx_ParamBlockHeight		= 1;
const int sC_idx_ParamBlockSegments		= 2;	//!<

// --- Static Globals -----------------------------------------------------------

static CollisionCylinderClassDesc theCollisionCylinderDesc;

static ParamBlockDescID descVer1[] = 
{
	{ TYPE_FLOAT, NULL, FALSE, 0},  // radius controlled by a spinner
	{ TYPE_INT,   NULL, FALSE, 1},  // Type of sphere
};

static ParamVersionDesc oldVersions[] =
{
	ParamVersionDesc( descVer1, 2, 1 )
};

// --- Globals ------------------------------------------------------------------

//
// Max3 paramblock2 version
//
ParamBlockDesc2 colCylinderDesc( colcylinder_params, _T("ColCylinder parameters"), 
								 IDS_COLCYLINDER, &theCollisionCylinderDesc, 
								 P_AUTO_CONSTRUCT | P_AUTO_UI, 0,
								 IDD_COL_CYLINDER_PANEL, IDS_PARAMS, 0, 0, 
								 NULL, 

								 colcylinder_radius, 
								 _T("Radius"), TYPE_WORLD, 0, IDS_RADIUS, 
								 p_default,		10.0f,
								 p_range, 		0.0f, float(1.0E30), 
								 p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_RADIUS_EDIT, IDC_RADIUS_SPIN, 0.01f,
								 end,

								 colcylinder_length, 
								 _T("Length"), TYPE_WORLD, 0, IDS_LENGTH,
								 p_default,		10.0f,
								 p_range, 		0.0f, float(1.0E30), 
								 p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_LENGTH_EDIT, IDC_LENGTH_SPIN, 0.01f,
								 end,

								 colcylinder_capcurve, 
								 _T("Capcurve"), TYPE_FLOAT, 0, IDS_CAPCURVE,
								 p_default,		0.0f,
								 p_range,		0.0f, 1.0f,
								 p_ui,			TYPE_SPINNER, EDITTYPE_FLOAT, IDC_CAPCURVE_EDIT, IDC_CAPCURVE_SPIN, 0.01f,
								 end,

								 colcylinder_sidecurve, 
								 _T("Sidecurve"), TYPE_FLOAT, 0, IDS_SIDECURVE,
								 p_default,		0.0f,
								 p_range,		0.0f, 1.0f,
								 p_ui,			TYPE_SPINNER, EDITTYPE_FLOAT, IDC_SIDECURVE_EDIT, IDC_SIDECURVE_SPIN, 0.01f,
								 end,

								 end );

// --- Code ---------------------------------------------------------------------

//-------------------------------------------------------------------------------
// CollisionCylinderClassDesc Class Functions
//-------------------------------------------------------------------------------

BOOL CollisionCylinderClassDesc::OkToCreate( Interface *ip )
{
	assert( ip );

	return TRUE;
}

int CollisionCylinderClassDesc::BeginCreate( Interface *ip )
{
	assert( ip );

	theSelectCommandMode.SetObjectClassIDs( SuperClassID(), ClassID() );
	theSelectCommandMode.Begin(  static_cast<IObjParam*>( ip ) );
	ip->PushCommandMode( &theSelectCommandMode );

	return TRUE;
}

int CollisionCylinderClassDesc::EndCreate( Interface *ip )
{
	assert( ip );

	theSelectCommandMode.End( static_cast<IObjParam*>( ip ) );
	ip->RemoveMode( &theSelectCommandMode );

	return TRUE;
}

const TCHAR* CollisionCylinderClassDesc::ClassName( ) 
{
	return GetString( IDS_COLCYLINDER );
}

const TCHAR* CollisionCylinderClassDesc::Category( )
{
	return GetString( IDS_CATEGORY );
}	

//-------------------------------------------------------------------------------
// CollisionCylinder Class Functions
//-------------------------------------------------------------------------------

/*****************************************************************************
** NAME       || CollisionCylinder()
** PURPOSE    || Constructor. Set up the pblock.
** PARAMETERS || 
** RETURNS    || 
*****************************************************************************/
CollisionCylinder::CollisionCylinder( )
	: pBlock2( NULL )
{
	theCollisionCylinderDesc.MakeAutoParamBlocks( this );
}

/*****************************************************************************
** NAME       || Clone()
** PURPOSE    || Clones the object
** PARAMETERS || 
** RETURNS    || 
*****************************************************************************/
RefTargetHandle CollisionCylinder::Clone( RemapDir& remap ) 
{
	CollisionCylinder* pClone = new CollisionCylinder;

	BaseClone( this, pClone, remap );
	pClone->ReplaceReference( PBLOCK_REF_NUM, remap.CloneRef( pBlock2 ) );
	return pClone;
}

void CollisionCylinder::InitNodeName( TSTR& s )
{
	s = GetString( IDS_COLCYLINDER_NODE );
}

TCHAR* CollisionCylinder::GetObjectName( ) 
{
	return GetString( IDS_COLCYLINDER );
}

void CollisionCylinder::GetClassName( TSTR& s )
{
	s = GetString( IDS_COLCYLINDER );
}

/*****************************************************************************
** NAME       || Display()
** PURPOSE    || Display the sphere
** PARAMETERS || The time, the inode pointer, and some other stuff
** RETURNS    || An int (zero)
*****************************************************************************/
int CollisionCylinder::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);

	SetHelperColour(inode, gw);

	DrawCircles(gw, inode, t);

	return 0;
}

/*****************************************************************************
** NAME       || drawCircles()
** PURPOSE    || Draw in the circles
** PARAMETERS || A GraphicsWindow pointer
** RETURNS    || Nowt.
*****************************************************************************/
void CollisionCylinder::DrawCircles(GraphicsWindow *gw, INode *inode, TimeValue t)
{
	if(!m_isDisplayed)
		return;

	Interval ivalid;

	float radius;
	float length;

	pBlock2->GetValue(colcylinder_radius, 0, radius, ivalid);
	pBlock2->GetValue(colcylinder_length, 0, length, ivalid);

	if ( 0 == m_displayMode )
	{
		DrawLineProc lineProc(gw);
		Point3 pt[sC_cnt_NumberCircleSegments + 1];

		float oneOverRootTwo = (float(1))/(float(sqrt(2.0f)));
		float sqrtRad = radius * oneOverRootTwo;
		float multiplier = float(TWOPI) / float(sC_cnt_NumberCircleSegments);

		// Draw the cylinder's length lines
		for ( int i = 0; i <= sC_cnt_NumberCircleSegments; ++i ) 
		{
			float u = float(i) * multiplier;
			pt[0].x = (float)cos(u) * radius;
			pt[0].y = length / 2.0f;
			pt[0].z = (float)sin(u) * radius;

			pt[1].x = (float)cos(u) * radius;
			pt[1].y = length / -2.0f;
			pt[1].z = (float)sin(u) * radius;

			lineProc.proc( pt, 2 );
		}

		// Top cylinder's end
		for( int j = 0; j < sC_cnt_NumberCircleSegments; ++j )
		{
			float v = float(j) * multiplier;
			float u = float(j+1) * multiplier;
			pt[0].x = (float)cos(v) * radius;
			pt[0].y = length / 2.0f;
			pt[0].z = (float)sin(v) * radius;

			pt[1].x = (float)cos(u) * radius;
			pt[1].y = length / 2.0f;
			pt[1].z = (float)sin(u) * radius;

			lineProc.proc( pt, 2 );
		}

		// Bottom cylinder's end
		for( int j = 0; j < sC_cnt_NumberCircleSegments; ++j )
		{
			float v = float(j) * multiplier;
			float u = float(j+1) * multiplier;
			pt[0].x = (float)cos(v) * radius;
			pt[0].y = -length / 2.0f;
			pt[0].z = (float)sin(v) * radius;

			pt[1].x = (float)cos(u) * radius;
			pt[1].y = -length / 2.0f;
			pt[1].z = (float)sin(u) * radius;

			lineProc.proc( pt, 2 );
		}
	}
	else
	{
		if(!inode)
		{
			return;
		}

		SimpleObject* pSimple = (SimpleObject*)CreateInstance(GEOMOBJECT_CLASS_ID,Class_ID(CYLINDER_CLASS_ID,0));

		if(!pSimple)
		{
			return;
		}

		Matrix3 mat = inode->GetObjectTM(t);
		mat.PreRotateX(DegToRad(90));
		mat.PreTranslate(Point3(0.0f, 0.0f, -length / 2.0f));
		gw->setTransform(mat);

		pSimple->pblock->SetValue(sC_idx_ParamBlockRadius, 0, radius);	
		pSimple->pblock->SetValue(sC_idx_ParamBlockHeight, 0, length);
		pSimple->pblock->SetValue(sC_idx_ParamBlockSegments, 0, 0);
		pSimple->BuildMesh(t);

		BOOL needDelete;

		class NullView : public View 
		{
		public:

			Point2 ViewToScreen(Point3 p) { return Point2(p.x,p.y); }
			NullView() 
			{
				worldToView.IdentityMatrix();
				screenW=640.0f; screenH = 480.0f;
			}
		};

		NullView nullView;

		Mesh* pMesh = pSimple->GetRenderMesh(t,inode,nullView,needDelete);

		pMesh->render(gw,inode->Mtls(),NULL,COMP_ALL);

		if(needDelete)
		{
			delete pMesh;
		}

		delete pSimple;
	}
};

/*****************************************************************************
** NAME       || HitTest()
** PURPOSE    || See if the object's been hit. Arse!
** PARAMETERS || Shit loads of stuff....
** RETURNS    || An int - zero if it's not been hit, else one.
*****************************************************************************/
int CollisionCylinder::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;

	DWORD savedLimits;

	GraphicsWindow *gw = vpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(inode->GetObjectTM(t));
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	DrawCircles(gw, inode, t);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

/*****************************************************************************
** NAME       || GetWorldBoundBox()
** PURPOSE    || Sets box to be the world bounding box.
** PARAMETERS || Stuff
** RETURNS    || Nothing (box is passed by reference)
*****************************************************************************/
void CollisionCylinder::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	//The object can handle this by transforming the 8 points of its local 
	//bounding box into world space and take the minimums and maximums of 
	//the result.  Although this isn't necessarily the tightest bounding box 
	//of the objects points in world space, it is close enough.

	Matrix3 local2world = inode -> GetObjectTM(t);

	Point3 minPoint((float)FLT_MAX, (float)FLT_MAX, (float)FLT_MAX);
	Point3 maxPoint = -minPoint;

	Box3 localBox;

	GetLocalBoundBox(t, inode, vp, localBox);

	Point3 worldP;

	int i;

	for (i = 0; i < 8; i++)
	{
		worldP = local2world*localBox[i];
		if (worldP.x < minPoint.x)
		{
			minPoint.x = worldP.x;
		}
		if (worldP.y < minPoint.y)
		{
			minPoint.y = worldP.y;
		}
		if (worldP.z < minPoint.z)
		{
			minPoint.z = worldP.z;
		}

		if (worldP.x > maxPoint.x)
		{
			maxPoint.x = worldP.x;
		}
		if (worldP.y > maxPoint.y)
		{
			maxPoint.y = worldP.y;
		}
		if (worldP.z > maxPoint.z)
		{
			maxPoint.z = worldP.z;
		}
	}

	box = Box3(minPoint, maxPoint);

}

/*****************************************************************************
** NAME       || GetLocalBoundBox()
** PURPOSE    || Sets box to be the local bounding box.
** PARAMETERS || Stuff
** RETURNS    || Nothing (box is passed by reference)
*****************************************************************************/
void CollisionCylinder::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	float fRadius;
	float fLength;
	Interval ivalid;

	pBlock2->GetValue( colcylinder_radius, t, fRadius, ivalid );
	pBlock2->GetValue( colcylinder_length, t, fLength, ivalid );

	Point3 maxPoint( fRadius, (fLength / 2.0f), fRadius );
	Point3 minPoint = -maxPoint;

	box = Box3( minPoint, maxPoint );
}

/*****************************************************************************
** NAME       || SetReference()
** PURPOSE    || Sets up pBlock.
** PARAMETERS || int i, and a RefTargetHandle
** RETURNS    || Nothing
*****************************************************************************/
void CollisionCylinder::SetReference(int i, RefTargetHandle rTarg)
{
	switch (i) 
	{
	case PBLOCK_REF_NUM: 	 
		pBlock2 = (IParamBlock2*)rTarg;
		break;
	}
}

RefTargetHandle CollisionCylinder::GetReference(int i)
{
	if (i == PBLOCK_REF_NUM)
	{
		return pBlock2;
	}
	return NULL;
}

int CollisionCylinder::NumRefs()
{
	return 1;
}

/*****************************************************************************
** NAME       || BeginEditParams()
** PURPOSE    || Read the manual!
** PARAMETERS || Some stuff.
** RETURNS    || Nothing
*****************************************************************************/
void CollisionCylinder::BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev)
{
	theCollisionCylinderDesc.BeginEditParams(ip, this, flags, prev);

	colCylinderDesc.SetUserDlgProc(new CollisionVolumeDlgProc());
}

/*****************************************************************************
** NAME       || EndEditParams()
** PURPOSE    || Read the manual.
** PARAMETERS || The usual gubbins
** RETURNS    || Nothing
*****************************************************************************/
void CollisionCylinder::EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)
{
	theCollisionCylinderDesc.EndEditParams(ip, this, flags, next);
}

//
//        name: CollisionCylinder::SetupFromSelected
// description: Setup collision cylinder from selected Node
//          in: pSelected = pointer to selected node
//         out: 
//
BOOL CollisionCylinder::SetupFromSelected(INode *pSelected, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Box3 local, world;
	Matrix3 mat = pSelected->GetNodeTM(t);
	Point3 centre;
	float radius = 0;

	mat.NoScale();

	GetGroupLocalBoundBox(pSelected, local, mat);
	GetGroupWorldBoundBox(pSelected, world);

	// work out centre and radius of collision sphere
	centre = world.Center();
	GetGroupRadius(pSelected, radius, local.Center(), mat);

	// set collision sphere node transformation matrix
	mat.SetTrans(centre);
	pNode->SetNodeTM(t, mat);
	// set radius
	pBlock2->SetValue(colcylinder_radius, t, radius);

	// set controller
	//	Control *pOrienControl = new StaticRotationControl;
	//	Control *pTMControl = pNode->GetTMController();
	//	pTMControl->SetRotationController(pOrienControl);

	return TRUE;
}

//-------------------------------------------------------------------------------
// Global Functions
//-------------------------------------------------------------------------------

ClassDesc* GetCollisionCylinderDesc( ) 
{
	return &theCollisionCylinderDesc; 
}

// End of file
