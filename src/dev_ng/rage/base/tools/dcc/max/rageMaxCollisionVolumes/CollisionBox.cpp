/*******************************************************************************
 *
 *    Filename : CollisionBox.cpp
 *     Creator : 
 *     $Author : $
 *       $Date : $
 *   $Revision : $
 * Description : 
 *
 *******************************************************************************/

// C header
#include <float.h>
// My headers
#include "collisionBox.h"
#include "CollisionControllers.h"
#include "resource.h"

static CollisionBoxClassDesc theCollisionBoxDesc;
ClassDesc* GetCollisionBoxDesc(){return &theCollisionBoxDesc;}

// list of descriptors
static ParamBlockDescID descVer1[] = 
{
	{ TYPE_FLOAT, NULL, FALSE, 0 },	//length controlled by a spinner
	{ TYPE_FLOAT, NULL, FALSE, 1 },	//width  controlled by a spinner
	{ TYPE_FLOAT, NULL, FALSE, 2 },	//height controlled by a spinner
};

static ParamVersionDesc oldVersions[] =
{
	ParamVersionDesc(descVer1, 3, 1)
};

//
// Max3 paramblock2 version
//

ParamBlockDesc2 colBoxDesc (colbox_params, _T("ColBox parameters"), IDS_COLBOX, &theCollisionBoxDesc, P_AUTO_CONSTRUCT | P_AUTO_UI, 0,
							IDD_COL_BOX_PANEL, IDS_PARAMS, 0, 0, NULL,
							colbox_length, _T("Length"), TYPE_WORLD, 0, IDS_LENGTH,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_BOX_LENGTH_EDIT, IDC_BOX_LENGTH_SPIN, 0.01f,
							end,
							colbox_width, _T("Width"), TYPE_WORLD, 0, IDS_WIDTH,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_BOX_WIDTH_EDIT, IDC_BOX_WIDTH_SPIN, 0.01f,
							end,
							colbox_height, _T("Height"), TYPE_WORLD, 0, IDS_HEIGHT,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_BOX_HEIGHT_EDIT, IDC_BOX_HEIGHT_SPIN, 0.01f,
							end,
							end);

//
//        name: CollisionSphereClassDesc::OkToCreate
// description: Return if it is OK to create a collision sphere i.e there is one object selected
//
BOOL CollisionBoxClassDesc::OkToCreate(Interface *ip)
{
	return theSelectCommandMode.OkToCreate(ip);
}

int CollisionBoxClassDesc::BeginCreate(Interface *ip)
{
	assert(theSelectCommandMode.OkToCreate(ip));

	theSelectCommandMode.SetObjectClassIDs(SuperClassID(), ClassID());
	theSelectCommandMode.Begin((IObjParam*)ip);
	ip->PushCommandMode(&theSelectCommandMode);

	return TRUE;
}

int CollisionBoxClassDesc::EndCreate(Interface *ip)
{
	theSelectCommandMode.End((IObjParam*)ip);
	ip->RemoveMode(&theSelectCommandMode);

	return TRUE;
}

const TCHAR* CollisionBoxClassDesc::ClassName() 
{
	return GetString(IDS_COLBOX);
}

const TCHAR* CollisionBoxClassDesc::Category()
{
	return GetString(IDS_CATEGORY);
}



/*******************************************************************************
 *        name : CollisionBox::CollisionBox()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
CollisionBox::CollisionBox():
	pBlock2(NULL)
{
	theCollisionBoxDesc.MakeAutoParamBlocks(this);
}

void CollisionBox::InitNodeName(TSTR& s)
{	
	s = GetString(IDS_COLBOX_NODE);
}

TCHAR *CollisionBox::GetObjectName() 
{
	return GetString(IDS_COLBOX);
}

void CollisionBox::GetClassName(TSTR& s)
{
	s = GetString(IDS_COLBOX);
}

/*******************************************************************************
 *        name : CollisionBox::Clone()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
RefTargetHandle CollisionBox::Clone(RemapDir& remap) 
{
	CollisionBox* pClone = new CollisionBox();

	BaseClone(this, pClone, remap);
	pClone->ReplaceReference(PBLOCK_REF_NUM, remap.CloneRef(pBlock2));
	return(pClone);
}

/*******************************************************************************
 *        name : CollisionBox::Display()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
int CollisionBox::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);
	SetHelperColour(inode, gw);

	DrawBox(inode,gw);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////
static void MakeQuad(Face *f, int a, int b , int c , int d, int sg) 
{
	int sm = 1<<sg;

	f[0].setVerts( a, c, b);
	f[0].setSmGroup(sm);
	f[0].setEdgeVisFlags(1,1,0);

	f[1].setVerts( c, d, b);
	f[1].setSmGroup(sm);
	f[1].setEdgeVisFlags(1,1,0);	
}

void addBox(Mesh& r_mesh,Point3 pntCentre,Point3 pntDimension,int& iVertOffset,int& iFaceOffset)
{
	r_mesh.setVert(iVertOffset + 0, Point3(pntCentre.x - pntDimension.x,pntCentre.y + pntDimension.y,pntCentre.z - pntDimension.z));
	r_mesh.setVert(iVertOffset + 1, Point3(pntCentre.x + pntDimension.x,pntCentre.y + pntDimension.y,pntCentre.z - pntDimension.z));
	r_mesh.setVert(iVertOffset + 2, Point3(pntCentre.x - pntDimension.x,pntCentre.y - pntDimension.y,pntCentre.z - pntDimension.z));
	r_mesh.setVert(iVertOffset + 3, Point3(pntCentre.x + pntDimension.x,pntCentre.y - pntDimension.y,pntCentre.z - pntDimension.z));
	r_mesh.setVert(iVertOffset + 4, Point3(pntCentre.x - pntDimension.x,pntCentre.y + pntDimension.y,pntCentre.z + pntDimension.z));
	r_mesh.setVert(iVertOffset + 5, Point3(pntCentre.x + pntDimension.x,pntCentre.y + pntDimension.y,pntCentre.z + pntDimension.z));
	r_mesh.setVert(iVertOffset + 6, Point3(pntCentre.x - pntDimension.x,pntCentre.y - pntDimension.y,pntCentre.z + pntDimension.z));
	r_mesh.setVert(iVertOffset + 7, Point3(pntCentre.x + pntDimension.x,pntCentre.y - pntDimension.y,pntCentre.z + pntDimension.z));

	MakeQuad(&(r_mesh.faces[iFaceOffset + 0]),iVertOffset + 0,iVertOffset + 2,iVertOffset + 1,iVertOffset + 3,1);
	MakeQuad(&(r_mesh.faces[iFaceOffset + 2]),iVertOffset + 5,iVertOffset + 7,iVertOffset + 4,iVertOffset + 6,2);
	MakeQuad(&(r_mesh.faces[iFaceOffset + 4]),iVertOffset + 0,iVertOffset + 4,iVertOffset + 2,iVertOffset + 6,3);
	MakeQuad(&(r_mesh.faces[iFaceOffset + 6]),iVertOffset + 6,iVertOffset + 7,iVertOffset + 2,iVertOffset + 3,4);
	MakeQuad(&(r_mesh.faces[iFaceOffset + 8]),iVertOffset + 5,iVertOffset + 1,iVertOffset + 7,iVertOffset + 3,5);
	MakeQuad(&(r_mesh.faces[iFaceOffset + 10]),iVertOffset + 4,iVertOffset + 0,iVertOffset + 5,iVertOffset + 1,6);

	iVertOffset += 8;
	iFaceOffset += 12;
}

/*******************************************************************************
 *        name : CollisionBox::DrawBox()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionBox::DrawBox(INode* inode,GraphicsWindow *gw)
{
	if(!m_isDisplayed)
		return;

	float length, width, height;
	Interval ivalid;

	pBlock2 -> GetValue(colbox_length, 0, length, ivalid);
	pBlock2 -> GetValue(colbox_width , 0, width , ivalid);
	pBlock2 -> GetValue(colbox_height, 0, height, ivalid);

	if(m_displayMode != 0)
	{
#if 1
		Mesh mesh;

		mesh.setNumVerts(8);
		mesh.setNumFaces(12);
		mesh.InvalidateTopologyCache();

		int iVertOffset = 0;
		int iFaceOffset = 0;

		addBox(mesh,Point3(0.0f,0.0f,0.0f),Point3(length,width,height),iVertOffset,iFaceOffset);
/*
		IMaxDataStore imax;
		dmat::AttributeInst *pInst = GetAttributes(inode);

		int iValColour = 15;		
		int idxColour;

		if(strcmp(GetAttrClassName(inode),"Gta Collision") == 0)
		{
			if(GetDisplayMode() == 1)
			{
				idxColour = imax.GetAttribIndex(inode,"Day Brightness");

				if(idxColour != -1)
					pInst->GetValue(idxColour, iValColour);
			}
			else
			{
				idxColour = imax.GetAttribIndex(inode,"Night Brightness");

				if(idxColour != -1)
					pInst->GetValue(idxColour, iValColour);
			}
		}
*/
		mesh.setNumTVerts(0);
		mesh.setNumTVFaces(0);
		mesh.buildNormals();
		mesh.DeleteFlaggedFaces();
		mesh.DeleteIsoVerts();
		mesh.InvalidateGeomCache();
		mesh.BuildStripsAndEdges();

		inode->SetMtl(NULL);
		//inode->SetWireColor(RGB(iValColour * 17,iValColour * 17,iValColour * 17));
		inode->SetWireColor(RGB(20,20,20));

		mesh.render(gw,inode->Mtls(),NULL,COMP_ALL);
#endif
	}

	//else
	{
		DrawLineProc lineProc(gw);
		Point3 pt[3];

		//Bottom square:
		pt[0] = Point3(-length, -width, -height);
		pt[1] = Point3(-length, width, -height);
		lineProc.proc(pt,2);

		pt[0] = pt[1];
		pt[1] = Point3(length, width, -height);
		lineProc.proc(pt,2);

		pt[0] = pt[1];
		pt[1] = Point3(length, -width, -height);
		lineProc.proc(pt,2);

		pt[0] = pt[1];
		pt[1] = Point3(-length, -width, -height);
		lineProc.proc(pt,2);

		//Top square
		pt[0] = Point3(-length, -width, height);
		pt[1] = Point3(-length, width, height);
		lineProc.proc(pt,2);

		pt[0] = pt[1];
		pt[1] = Point3(length, width, height);
		lineProc.proc(pt,2);

		pt[0] = pt[1];
		pt[1] = Point3(length, -width, height);
		lineProc.proc(pt,2);

		pt[0] = pt[1];
		pt[1] = Point3(-length, -width, height);
		lineProc.proc(pt,2);
		
		//Pillars:
		pt[0] = Point3(-length, -width, -height);
		pt[1] = Point3(-length, -width, height);
		lineProc.proc(pt,2);

		pt[0] = Point3(-length, width, -height);
		pt[1] = Point3(-length, width, height);
		lineProc.proc(pt,2);

		pt[0] = Point3(length, -width, -height);
		pt[1] = Point3(length, -width, height);
		lineProc.proc(pt,2);

		pt[0] = Point3(length, width, -height);
		pt[1] = Point3(length, width, height);
		lineProc.proc(pt,2);
	}
};

/*******************************************************************************
 *        name : CollisionBox::HitTest()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
int CollisionBox::HitTest(TimeValue t, INode* inode, int type, int crossing, 
							 int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;

	DWORD savedLimits;

	GraphicsWindow *gw = vpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(inode->GetObjectTM(t));
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	DrawBox(inode,gw);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

/*******************************************************************************
 *        name : CollisionBox::GetWorldBoundBox()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionBox::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	//The object can handle this by transforming the 8 points of its local 
	//bounding box into world space and take the minimums and maximums of 
	//the result.  Although this isn't necessarily the tightest bounding box 
	//of the objects points in world space, it is close enough.

	Matrix3 local2world = inode -> GetObjectTM(t);

	Point3 minPoint((float)FLT_MAX, (float)FLT_MAX, (float)FLT_MAX);
	Point3 maxPoint = -minPoint;

	Box3 localBox;
	
	GetLocalBoundBox(t, inode, vp, localBox);
	
	Point3 worldP;

	int i;

	for (i = 0; i < 8; i++)
	{
		worldP = local2world*localBox[i];
		if (worldP.x < minPoint.x)
		{
			minPoint.x = worldP.x;
		}
		if (worldP.y < minPoint.y)
		{
			minPoint.y = worldP.y;
		}
		if (worldP.z < minPoint.z)
		{
			minPoint.z = worldP.z;
		}

		if (worldP.x > maxPoint.x)
		{
			maxPoint.x = worldP.x;
		}
		if (worldP.y > maxPoint.y)
		{
			maxPoint.y = worldP.y;
		}
		if (worldP.z > maxPoint.z)
		{
			maxPoint.z = worldP.z;
		}
	}

	box = Box3(minPoint, maxPoint);

}

/*******************************************************************************
 *        name : CollisionBox::GetLocalBoundBox()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionBox::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	
	float l, w, h;
	Interval ivalid;

	pBlock2 -> GetValue(colbox_length, t, l, ivalid);
	pBlock2 -> GetValue(colbox_width,  t, w, ivalid);
	pBlock2 -> GetValue(colbox_height, t, h, ivalid);

	Point3 maxPoint((float)l, (float)w, (float)h);
	Point3 minPoint = -maxPoint;

	box = Box3(minPoint, maxPoint);
}

/*******************************************************************************
 *        name : CollisionBox::SetReference()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionBox::SetReference(int i, RefTargetHandle rTarg)
{
	switch (i) 
	{
	case PBLOCK_REF_NUM:
		pBlock2 = (IParamBlock2*)rTarg;
		break;
	}
}

RefTargetHandle CollisionBox::GetReference(int i)
{
	if (i == PBLOCK_REF_NUM)
	{
		return pBlock2;
	}
	return NULL;
}

int CollisionBox::NumRefs()
{
	return 1;
}

/*******************************************************************************
 *        name : CollisionBox::BeginEditParams()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionBox::BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev)
{
	theCollisionBoxDesc.BeginEditParams(ip, this, flags, prev);

	colBoxDesc.SetUserDlgProc(new CollisionVolumeDlgProc());
}

/*******************************************************************************
 *        name : CollisionBox::EndEditParams()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionBox::EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)
{
	theCollisionBoxDesc.EndEditParams(ip, this, flags, next);
}

//
//        name: CollisionSphere::SetupFromSelected
// description: Setup collision sphere from selected Node
//          in: pSelected = pointer to selected node
//         out: 
//
BOOL CollisionBox::SetupFromSelected(INode *pSelected, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Box3 local, world;
	Matrix3 mat = pSelected->GetNodeTM(t);
	Point3 centre;
	Point3 size;

	mat.NoScale();

	GetGroupLocalBoundBox(pSelected, local, mat);
	GetGroupWorldBoundBox(pSelected, world);

	// work out centre and size of collision box
	centre = world.Center();
	size = local.Width();

	// set collision sphere node transformation matrix
	mat.SetTrans(centre);
	pNode->SetNodeTM(t, mat);

	// set box sizes
	pBlock2->SetValue(colbox_length, t, size.x);
	pBlock2->SetValue(colbox_width, t, size.y);
	pBlock2->SetValue(colbox_height, t, size.z);


	// set controller
	Control *pOrienControl = new StaticRotationControl;
	Control *pTMControl = pNode->GetTMController();
	pTMControl->SetRotationController(pOrienControl);

	return TRUE;
}