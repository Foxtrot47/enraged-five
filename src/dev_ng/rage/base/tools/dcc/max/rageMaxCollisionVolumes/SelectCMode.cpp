//
//
//    Filename: SelectCMode.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Command mode that creates an object from a selected object. 
//
//
// my headers
#include "SelectCMode.h"
#include "resource.h"

#pragma warning (disable : 4786)
// STD headers
#include <set>

#define COLL_COLOUR_USAGE_CHUNK 1000

BOOL CollisionHelperObject::m_isDisplayed = TRUE;
int CollisionHelperObject::m_displayMode = 0;
Interface *CollisionHelperObject::m_ip = NULL;

SelectCommandMode theSelectCommandMode;
CollisionVolumeDlgProc theCollisionVolumeDlgProc;


// ----- CollisionHelperObject -----------------------------------------------------------------

CollisionHelperObject::CollisionHelperObject() 
: m_allowMultiple(TRUE)
, m_useWireframeCol(FALSE)
, m_fR(1)
, m_fG(1)
, m_fB(0)
{
	m_ip = GetCOREInterface();
}

IOResult CollisionHelperObject::Load(ILoad *iload)
{
	ULONG nb;   
	IOResult res;   
	while (IO_OK==(res=iload->OpenChunk()))   
	{     
		switch(iload->CurChunkID())
		{
		case COLL_COLOUR_USAGE_CHUNK:
			res=iload->Read(&m_useWireframeCol, sizeof(int), &nb);
			if (res!=IO_OK)
				break;
			res=iload->Read(&m_fR, sizeof(float), &nb);
			if (res!=IO_OK)
				break;
			res=iload->Read(&m_fG, sizeof(float), &nb);
			if (res!=IO_OK)
				break;
			res=iload->Read(&m_fB, sizeof(float), &nb);
			break;
		}
		iload->CloseChunk();
		if (res!=IO_OK)
			return res;
	}	
	return IO_OK;}

IOResult CollisionHelperObject::Save(ISave *isave)
{
	ULONG nb;
	isave->BeginChunk(COLL_COLOUR_USAGE_CHUNK);
	isave->Write(&m_useWireframeCol, sizeof(int), &nb);
	isave->Write(&m_fR, sizeof(float), &nb);
	isave->Write(&m_fG, sizeof(float), &nb);
	isave->Write(&m_fB, sizeof(float), &nb);
	isave->EndChunk();
	return IO_OK;
}

//
//        name: CollisionHelperObject::CreateFromSelected
// description: Setup collision object from selected node
//          in: pSelected = pointer to selected node
//				pNode = pointer to collision object node
//         out: 
//
BOOL CollisionHelperObject::CreateFromSelected(INode *pSelected, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Matrix3 mat = pSelected->GetObjectTM(t);
	INode *pChild;
	Object *pObj;
	int i;
	TSTR name, suffix;

	pNode->SetNodeTM(t, mat);

	// if only one version of the volume is allowed as a child delete the old version
	if(m_allowMultiple == FALSE)
	{
		for(i=0; i<pSelected->NumberOfChildren(); i++)
		{
			pChild = pSelected->GetChildNode(i);
			pObj = pChild->GetObjectRef();
			if(pObj && pObj->ClassID() == ClassID())
				pChild->Delete(t, TRUE);
		}
	}
	pSelected->AttachChild(pNode);

	// set node name
	GetNameSuffix(suffix);
	name = TSTR(pSelected->GetName()) + suffix;
	if(m_allowMultiple)
		m_ip->MakeNameUnique(name);
	pNode->SetName(name);

	return SetupFromSelected(pSelected, pNode);
}

//
//        name: CollisionHelperObject::GetGroupLocalBoundBox, GetGroupWorldBoundBox
// description: Returns the bounding box for the group in local coordinates
//          in: pNode = parent node
//				box = reference to box
//				mat = local matrix 
//
void CollisionHelperObject::GetGroupLocalBoundBox(INode *pNode, Box3 &box, Matrix3 &mat)
{
	INode *pChild;
	int i;

	if(pNode->IsGroupHead())
	{

		for(i=0; i<pNode->NumberOfChildren(); i++)
		{
			pChild = pNode->GetChildNode(i);
			if(pChild->IsGroupMember())
				GetGroupLocalBoundBox(pChild, box, mat);
		}
	}
	else
	{
		Matrix3 localMat = pNode->GetObjectTM(m_ip->GetTime());
		Object* pObj = pNode -> EvalWorldState(m_ip->GetTime()).obj;
		ViewExp *pView = m_ip->GetActiveViewport();
		Box3 localBox;

		pObj->GetLocalBoundBox(m_ip->GetTime(), pNode, pView, localBox);
		localMat *= Inverse(mat);
		box += localBox * localMat;
	}

}

void CollisionHelperObject::GetGroupWorldBoundBox(INode *pNode, Box3 &box)
{
	INode *pChild;
	int i;

	if(pNode->IsGroupHead())
	{
		for(i=0; i<pNode->NumberOfChildren(); i++)
		{
			pChild = pNode->GetChildNode(i);
			if(pChild->IsGroupMember())
				GetGroupWorldBoundBox(pChild, box);
		}
	}
	else
	{
		Object* pObj = pNode -> EvalWorldState(m_ip->GetTime()).obj;
		ViewExp *pView = m_ip->GetActiveViewport();
		Box3 worldBox;

		pObj->GetWorldBoundBox(m_ip->GetTime(), pNode, pView, worldBox);
		box += worldBox;
	}
}

//
//        name: CollisionHelperObject::GetGroupRadius
// description: Returns the radius needed to encase all the mesh data in this node
//          in: pNode = pointer to node
//				radius = reference to radius value to fill
//				centre = centre from which radius starts
//				mat = local matrix
//         out: 
//
void CollisionHelperObject::GetGroupRadius(INode *pNode, float &radius, Point3 &centre, Matrix3 &mat)
{
	INode *pChild;
	int i;

	if(pNode->IsGroupHead())
	{

		for(i=0; i<pNode->NumberOfChildren(); i++)
		{
			pChild = pNode->GetChildNode(i);
			if(pChild->IsGroupMember())
				GetGroupRadius(pChild, radius, centre, mat);
		}
	}
	else
	{
		Matrix3 localMat = pNode->GetObjectTM(m_ip->GetTime());
		Object* pObj = pNode -> EvalWorldState(m_ip->GetTime()).obj;
		ViewExp *pView = m_ip->GetActiveViewport();
		TriObject *pTri;
		Mesh *pMesh;
		Point3 vertex;
		float length;

		localMat *= Inverse(mat);

		if(pObj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
		{
			pTri = (TriObject *)pObj->ConvertToType(m_ip->GetTime(), Class_ID(TRIOBJ_CLASS_ID, 0));
			pMesh = &pTri->mesh;

			for(i=0; i<pMesh->numVerts;i++)
			{
				vertex = pMesh->verts[i] * localMat;
				length = Length(vertex - centre);
				if(length > radius)
					radius = length;
			}
		}
	}

}

//
//        name: CollisionHelperObject::SetDisplayed
// description: Set the collision objects to display or not
//          in: isDisplayed = display or not
//         out: 
//
void CollisionHelperObject::SetDisplayed(BOOL isDisplayed)
{
	m_isDisplayed = isDisplayed; 
	if(m_ip)
		m_ip->ForceCompleteRedraw();
}

void CollisionHelperObject::SetDisplayMode(int mode)
{
	m_displayMode = mode;

	if(m_ip)
		m_ip->ForceCompleteRedraw();
}


Color CollisionHelperObject::SetHelperColour(INodePtr pNode, GraphicsWindow *gw)
{
	Color retVal;
	if (pNode->Selected()) 
	{
		gw->setColor(LINE_COLOR, 1,1,1);
		retVal = Color(1,1,1);
	}
	else if(m_useWireframeCol)
	{
		COLORREF col = (COLORREF)pNode->GetWireColor();
		float r = ((float)GetRValue(col)) / 255.0f;
		float g = ((float)GetGValue(col)) / 255.0f;
		float b = ((float)GetBValue(col)) / 255.0f;
		retVal = Color(r,g,b);
		gw->setColor(LINE_COLOR, r,g,b);
	}
	else if(!pNode->IsFrozen()) 
	{
		gw->setColor(LINE_COLOR, m_fR, m_fG, m_fB);
		retVal = Color(m_fR, m_fG, m_fB);
	}
	return retVal;
}

//
//        name: SelectCreateProc::proc
// description: Callback for mouse callback class
//          in: 
//         out: 
//
int SelectCreateProc::proc(HWND hWnd, int msg, int point, int flags, IPoint2 m ) 
{
	switch (msg)
	{
	case MOUSE_POINT:

		{
			Point3 pnt = GetCOREInterface()->GetActiveViewport()->GetPointOnCP(m);
			//Point3 pnt = GetCOREInterface()->GetActiveViewport()->MapScreenToView(m,GetPerspMouseSpeed());

			if(theSelectCommandMode.CreateSingle(pnt))
			{
				m_ip->RemoveMode(NULL);
			}
		}

		break;

    case MOUSE_PROPCLICK:
		// right click while between creations
		m_ip->RemoveMode(NULL);
		break;
	}	
	return TRUE;
}

RefResult SelectCommandMode::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, PartID& partID, RefMessage message)
{
	switch (message) {		
	case REFMSG_TARGET_SELECTIONCHANGE:		
	case REFMSG_TARGET_DELETED:			
		if (m_ip && !m_selecting) 
			m_ip->StopCreating();
		break;
		
	}
	return REF_SUCCEED;
}

//
//        name: IsNodeMemberOfGroup
// description: Returns if node is a member of a group with specified group head 
//          in: pGrpHead = pointer to group head
//				pNode = pointer to node
//         out: TRUE/FALSE
//
BOOL IsNodeMemberOfGroup(INode *pGrpHead, INode *pNode)
{
	assert(pGrpHead->IsGroupHead());

	if(pNode->IsGroupMember() == FALSE)
		return FALSE;
	while(pNode->IsGroupMember())
	{
		if(pNode == pGrpHead)
			return TRUE;
		pNode = pNode->GetParentNode();
	}
	if(pNode == pGrpHead)
		return TRUE;
	return FALSE;
}

//
//        name: *GetNodeGroupHead
// description: Return the group head for a node
//          in: 
//         out: 
//
INode *GetNodeGroupHead(INode *pNode)
{
	while(pNode->IsGroupMember())
	{
		pNode = pNode->GetParentNode();
	}
	return pNode;
}

//
//        name: DoesGroupContainGeometry
// description: Returns if group contains a node with geometry
//          in: pGrpHead = ponter to group head
//
BOOL DoesGroupContainGeometry(INode *pGrpHead, TimeValue t)
{
	int i;
	INode *pNode;
	Object *pObj;

	assert(pGrpHead->IsGroupHead());

	for(i=0; i<pGrpHead->NumberOfChildren(); i++)
	{
		pNode = pGrpHead->GetChildNode(i);
		if(pNode->IsGroupMember())
		{
			if(pNode->IsGroupHead())
			{
				if(DoesGroupContainGeometry(pNode, t))
					return TRUE;
			}
			else
			{
				pObj = pNode->EvalWorldState(t).obj;
				if(pObj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
					return TRUE;
			}
		}
	}
	return FALSE;
}

//
//        name: SelectCommandMode::OkToCreateWithNode
// description: Returns if it is OK to create a volume for a certain node
//
BOOL SelectCommandMode::OkToCreateWithNode(Interface *ip, INode *pNode)
{
	Object *pObj;

	if(pNode->IsGroupHead())
	{
		if(DoesGroupContainGeometry(pNode, ip->GetTime()))
			return TRUE;	
	}
	else
	{
		pObj = pNode->EvalWorldState(ip->GetTime()).obj;
		if(pObj && pObj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
			return TRUE;
	}
	return FALSE;
}

//
//        name: SelectCommandMode::OkToCreate
// description: Returns if it is OK to create something using this command mode
//          in: 
//         out: 
//
BOOL SelectCommandMode::OkToCreate(Interface *ip)
{
	int numSelected = ip->GetSelNodeCount();
	int i;

	for(i=0; i<numSelected; i++)
	{
		if(OkToCreateWithNode(ip, ip->GetSelNode(i)))
			return TRUE;
	}
	return FALSE;
}

//
//        name: SelectCommandMode::GetSelectedNodesHead
// description: Return one of the selected nodes. The one that is group head for all the 
//				other selected nodes
//          in: ip = pointer to interface
//         out: 
//
INode *SelectCommandMode::GetSelectedNodesHead(Interface *ip)
{
	INode *pNode = NULL;
	BOOL foundHead;
	int i, j;
	int numSelected = ip->GetSelNodeCount();

	// if number selected is 1 then return that node
	if(numSelected == 1)
		return ip->GetSelNode(0);

	// go through selected nodes and if they are group head for all nodes return that node
	for(i=0; i<numSelected; i++)
	{
		pNode = ip->GetSelNode(i);
		if(pNode->IsGroupHead())
		{
			foundHead = TRUE;
			for(j=0; j<numSelected; j++)
			{
				if(i==j)
					continue;
				if(!IsNodeMemberOfGroup(pNode, ip->GetSelNode(j)))
				{
					foundHead = FALSE;
					break;
				}
			}
			if(foundHead == TRUE && DoesGroupContainGeometry(pNode, ip->GetTime()))
			{
				return pNode;
			}
		}
	}
	return NULL;
}

bool SelectCommandMode::CreateSingle(const Point3& r_pnt)
{
	if(m_doSingle)
	{
		INode *pNode;

		m_pObj = (CollisionHelperObject *)m_ip->CreateInstance(m_superClassId, m_classId);
		pNode = m_ip->CreateObjectNode((Object *)m_pObj);

		ReplaceReference(0, pNode);
		m_pObj->BeginEditParams(m_ip, BEGIN_EDIT_CREATE, NULL);

		m_ip->RedrawViews(m_ip->GetTime());

		Matrix3 mat;
		mat.IdentityMatrix();
		mat.SetTrans(r_pnt);

		pNode->SetNodeTM(0,mat);

		return true;
	}

	return false;
}

void SelectCommandMode::Begin(IObjParam *ip) 
{
	//INode *pSelected = GetSelectedNodesHead(ip);
	INode *pNode = NULL;
	CollisionHelperObject *pObj = NULL;
	std::set<INode *> m_selectedSet;
	std::set<INode *>::iterator iNode;
	int numSelected = ip->GetSelNodeCount();
	int i;

	m_selecting = FALSE; 
	m_ip = ip;
	m_proc.Init(m_ip);
	
	if(numSelected == 0)
	{
		m_doSingle = true;
		return;
	}
	else
	{
		m_doSingle = false;
	}

	for(i=0; i<numSelected; i++)
	{
		pNode = ip->GetSelNode(i);
		m_selectedSet.insert(GetNodeGroupHead(pNode));
	}

	theHold.Begin();
	bool atLeastOneCreationSucceeded = false;
	for(iNode = m_selectedSet.begin(); iNode != m_selectedSet.end(); iNode++)
	{
		if(!OkToCreateWithNode(ip, *iNode))
			continue;
		CollisionHelperObject *tempObj = (CollisionHelperObject *)m_ip->CreateInstance(m_superClassId, m_classId);
		if(tempObj!=NULL)
		{
			pObj = tempObj;
			pNode = m_ip->CreateObjectNode((Object *)pObj);

			pObj->m_ip = m_ip;
			pObj->CreateFromSelected(*iNode, pNode);

			m_ip->SelectNode(pNode);

			atLeastOneCreationSucceeded = true;
		}
	}
	
	if( atLeastOneCreationSucceeded )
	{
		theHold.Accept("Create Volume(s)");
		m_pObj = pObj;
		ReplaceReference(0, pNode);
		m_pObj->BeginEditParams(m_ip, BEGIN_EDIT_CREATE, NULL);
	}
	else
	{
		//A new collision volume couldn't be created, so don't add the create to the undo stack
		theHold.Cancel();
	}

	m_ip->RedrawViews(m_ip->GetTime());
}

void SelectCommandMode::End(IObjParam *i)
{
	if (m_pObj) 
	{
		m_pObj->EndEditParams(i,END_EDIT_REMOVEUI,NULL);
		m_pObj->m_ip = NULL;
	}
	DeleteAllRefsFromMe();
	m_ip  = NULL;
	m_pObj = NULL;
}

void CollisionVolumeDlgProc::DeleteThis()
{
	delete this;
}

INT_PTR CollisionVolumeDlgProc::DlgProc(TimeValue t,IParamMap2 *map,HWND hWnd,unsigned int msg,WPARAM wParam,LPARAM lParam)
{
	switch (msg) 
	{
	case WM_INITDIALOG:
		{
			TSTR version = TSTR("Version ") + GetString(IDS_VERSION);
			SetDlgItemText(hWnd, IDC_VERSION, version);

			CheckDlgButton(hWnd, IDC_DISPLAY_CHECK, CollisionHelperObject::IsDisplayed());

			int iMode = CollisionHelperObject::GetDisplayMode();

			switch(iMode)
			{
			case 0:
				CheckDlgButton(hWnd, IDC_RDO_WIREFRAME, TRUE);
				CheckDlgButton(hWnd, IDC_RDO_VERTEXCOL, FALSE);
				CheckDlgButton(hWnd, IDC_RDO_ILLUMCOL, FALSE);
				break;
			case 1:
				CheckDlgButton(hWnd, IDC_RDO_WIREFRAME, FALSE);
				CheckDlgButton(hWnd, IDC_RDO_VERTEXCOL, TRUE);
				CheckDlgButton(hWnd, IDC_RDO_ILLUMCOL, FALSE);
				break;
			case 2:
				CheckDlgButton(hWnd, IDC_RDO_WIREFRAME, FALSE);
				CheckDlgButton(hWnd, IDC_RDO_VERTEXCOL, FALSE);
				CheckDlgButton(hWnd, IDC_RDO_ILLUMCOL, TRUE);
				break;
			}
		}
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_DISPLAY_CHECK:
			CollisionHelperObject::SetDisplayed(IsDlgButtonChecked(hWnd, IDC_DISPLAY_CHECK));
			break;
		case IDC_RDO_WIREFRAME:
			CollisionHelperObject::SetDisplayMode(0);
			break;
		case IDC_RDO_VERTEXCOL:
			CollisionHelperObject::SetDisplayMode(1);
			break;
		case IDC_RDO_ILLUMCOL:
			CollisionHelperObject::SetDisplayMode(2);
			break;
		}

		break;
		default:
			return FALSE;
	}
	return TRUE;
}
