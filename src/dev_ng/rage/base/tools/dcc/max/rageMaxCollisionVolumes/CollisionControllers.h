//
//
//    Filename: CollisionControllers.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: File containing collision volume transformation controllers
//
//
#ifndef INC_COLLISION_CONTROLLERS_H_
#define INC_COLLISION_CONTROLLERS_H_

// Max headers
#include <Max.h>

#define STATIC_POSITION_CONTROL_CLASS_ID Class_ID(0x43e539ef, 0x2e324139)
#define STATIC_ROT_CONTROL_CLASS_ID Class_ID(0x76d71c70, 0x115941b6)
#define STATIC_WORLD_ROT_CONTROL_CLASS_ID Class_ID(0x52713032, 0x760e3891)

TCHAR *GetString(int id);

//
//   Class Name: StaticPositionControl
// Base Classes: Control
//  Description: 
//    Functions: 
//
//
class StaticPositionControl : public Control
//class StaticPositionControl : public StdControl
{
private:
	Point3 m_posn;

public:
	StaticPositionControl();
	~StaticPositionControl();

	// my methods
	void SetPosn(Point3 posn) {m_posn = posn;}

	// from class Control
	void GetValue(TimeValue t, void *val, Interval &valid, GetSetMethod method);
	void SetValue(TimeValue t, void *val, int commit, GetSetMethod method);
	void Copy(Control *pFrom);
	BOOL IsReplaceable() {return FALSE;}
	BOOL InheritsParentTransform() {return FALSE;}
	BOOL IsLeaf() {return TRUE;}
	BOOL IsKeyable() {return FALSE;}
	BOOL CanApplyEaseMultCurves() {return FALSE;}
	BOOL ChangeParents(TimeValue t,const Matrix3& oldP,const Matrix3& newP,const Matrix3& tm);

	// from class ReferenceTarget
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());

	RefResult NotifyRefChanged(Interval interval, RefTargetHandle rtarg, PartID& partID,RefMessage message) {return REF_SUCCEED;}

	// from class Animatable
	void DeleteThis() {delete this;}
	SClass_ID SuperClassID() {return CTRL_POINT3_CLASS_ID;}	
	Class_ID ClassID(){return STATIC_POSITION_CONTROL_CLASS_ID;}
	void GetClassName(TSTR& s);
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);
};

class StaticPositionControlClassDesc : public ClassDesc 
{
public:
	int IsPublic() {return 0;}	
	void* Create(BOOL loading = FALSE) {return new StaticPositionControl();}	
	const TCHAR* ClassName();
	SClass_ID SuperClassID() {return CTRL_POINT3_CLASS_ID;}	
	Class_ID ClassID(){return STATIC_POSITION_CONTROL_CLASS_ID;}
	const TCHAR* Category();
};


//
//   Class Name: StaticRotationControl
// Base Classes: Control
//  Description: 
//    Functions: 
//
//
class StaticRotationControl : public Control
{
protected:
	Quat m_rotation;

public:
	StaticRotationControl();
	~StaticRotationControl();

	// my methods
	void SetOrien(Quat rotation) {m_rotation = rotation;}

	// from class Control
	void GetValue(TimeValue t, void *val, Interval &valid, GetSetMethod method);
	void SetValue(TimeValue t, void *val, int commit, GetSetMethod method);
	void Copy(Control *pFrom);
	BOOL IsReplaceable() {return FALSE;}
	BOOL InheritsParentTransform() {return FALSE;}
	BOOL IsLeaf() {return TRUE;}
	BOOL IsKeyable() {return FALSE;}
	BOOL CanApplyEaseMultCurves() {return FALSE;}
	BOOL ChangeParents(TimeValue t,const Matrix3& oldP,const Matrix3& newP,const Matrix3& tm);

	// from class ReferenceTarget
	ReferenceTarget* Clone(RemapDir &remap = DefaultRemapDir());

	RefResult NotifyRefChanged(Interval interval, RefTargetHandle rtarg, PartID& partID,RefMessage message) {return REF_SUCCEED;}

	// from class Animatable
	void DeleteThis() {delete this;}
	SClass_ID SuperClassID() {return CTRL_QUAT_CLASS_ID;}	
	Class_ID ClassID(){return STATIC_ROT_CONTROL_CLASS_ID;}
	void GetClassName(TSTR& s);
	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);

};

class StaticRotationControlClassDesc : public ClassDesc 
{
public:
	int IsPublic() {return 0;}	
	void* Create(BOOL loading = FALSE) {return new StaticRotationControl();}	
	const TCHAR* ClassName();
	SClass_ID SuperClassID() {return CTRL_QUAT_CLASS_ID;}	
	Class_ID ClassID(){return STATIC_ROT_CONTROL_CLASS_ID;}	
	const TCHAR* Category();
};


#endif // INC_COLLISION_CONTROLLERS_H_
