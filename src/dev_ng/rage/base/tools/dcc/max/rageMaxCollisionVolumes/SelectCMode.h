//
//
//    Filename: SelectCMode.h
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Command Mode that creates an object from a selected object
//
//
#ifndef _SELECT_CMODE_H_
#define _SELECT_CMODE_H_

#include <Max.h>
#include "iparamm2.h"

// command mode id
#define CID_SELECTCREATE	CID_USER + 0x347610	

extern TCHAR *GetString(int id);

//
//   Class Name: CreateFromSelectedObject
// Base Classes: 
//  Description: Base class used in conjunction with SelectCommandMode
//    Functions: 
//
//
class CollisionHelperObject : public HelperObject
{
protected:
	static BOOL m_isDisplayed;
	static int m_displayMode;
	BOOL m_allowMultiple;
	BOOL m_useWireframeCol;
	float m_fR,m_fG,m_fB;

public:
	static Interface *m_ip;

	CollisionHelperObject();

	IOResult Load(ILoad *iload);
	IOResult Save(ISave *isave);

	BOOL CreateFromSelected(INode *pSelected, INode *pNode);
	void GetGroupLocalBoundBox(INode *pSelected, Box3 &box, Matrix3 &mat);
	void GetGroupWorldBoundBox(INode *pSelected, Box3 &box);
	void GetGroupRadius(INode *pNode, float &radius, Point3 &centre, Matrix3 &mat);
	static void SetDisplayed(BOOL isDisplayed);
	static BOOL IsDisplayed(void) {return m_isDisplayed;}

	static void SetDisplayMode(int isWireframe);
	static int GetDisplayMode(void) {return m_displayMode;}

	virtual void GetNameSuffix(TSTR &s) = 0;
	virtual BOOL SetupFromSelected(INode *pSelected, INode *pNode) = 0;

	Color SetHelperColour(INodePtr pNode, GraphicsWindow *gw);
	void SetUseWireframeColour(bool onOff)
	{
		m_useWireframeCol = onOff;
	}
	int UsesWireColor()
	{
		return m_useWireframeCol;
	}
	void SetNodeColour(Color *pCol)
	{
		Color &col = *pCol;
		m_fR = col.r;
		m_fG = col.g;
		m_fB = col.b;
	}
	Color GetNodeColour()
	{
		Color rgb;
		rgb.r = m_fR;
		rgb.g = m_fG;
		rgb.b = m_fB;
		return rgb;
	}
};

//
//   Class Name: CollisionVolumeDlgProc
// Base Classes: 
//  Description: Standard collision volume parameter map dialog proc
//    Functions: 
//
//
class CollisionVolumeDlgProc : public ParamMap2UserDlgProc
{
public:
	
	INT_PTR DlgProc(TimeValue t,IParamMap2 *map,HWND hWnd,unsigned int msg,WPARAM wParam,LPARAM lParam);
	void DeleteThis();
	//void Update(TimeValue t){};
};


//
//   Class Name: SelectCreateProc
// Base Classes: 
//  Description: Mouse callback for creating collision volumes, does nothing
//    Functions: 
//
//
class SelectCreateProc : public MouseCallBack {
public:
	IObjParam *m_ip;
	void Init(IObjParam *ip) {m_ip=ip;}
	int proc(HWND hWnd, int msg, int point, int flags, IPoint2 m );
};

//
//   Class Name: SelectCommandMode
// Base Classes: 
//  Description: Command mode that creates an object from a selected object
//    Functions: 
//
//
class SelectCommandMode : public CommandMode, ReferenceMaker
{
	SelectCreateProc m_proc;
	INode *m_pNode;
	IObjParam *m_ip;
	CollisionHelperObject *m_pObj;
	Class_ID m_classId;
	SClass_ID m_superClassId;
	BOOL m_selecting;
	bool m_doSingle;
	
public:		

	bool CreateSingle(const Point3& r_pnt);

	void SetObjectClassIDs(SClass_ID sid, Class_ID id) {m_superClassId = sid; m_classId = id;}

	BOOL OkToCreate(Interface *ip);
	BOOL OkToCreateWithNode(Interface *ip, INode *pNode);
	INode *GetSelectedNodesHead(Interface *ip);
	void Begin(IObjParam *i);
	void End(IObjParam *i);
	
	int Class() {return CREATE_COMMAND;}
	int ID() { return CID_SELECTCREATE; }
	MouseCallBack *MouseProc(int *pNumPoints) {*pNumPoints = 0; return &m_proc;}
	ChangeForegroundCallback *ChangeFGProc() {return CHANGE_FG_SELECTED;}
	BOOL ChangeFG(CommandMode *oldMode) {return TRUE;}
	void EnterMode() {}
	void ExitMode() {}
	
	int NumRefs() {return 1;}
	RefTargetHandle GetReference(int i) {return m_pNode;}
	void SetReference(int i, RefTargetHandle rtarg) {m_pNode = (INode*)rtarg;}
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
		PartID& partID,  RefMessage message);		
};

extern SelectCommandMode theSelectCommandMode;
extern CollisionVolumeDlgProc theCollisionVolumeDlgProc;

#endif
