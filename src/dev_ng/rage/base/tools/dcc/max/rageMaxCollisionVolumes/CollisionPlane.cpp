
/**********************************************************************
 *<
	FILE: CollisionPlane.cpp

	DESCRIPTION:	Appwizard generated plugin

	CREATED BY: 

	HISTORY: 

 *>	Copyright (c) 1997, All Rights Reserved.
 **********************************************************************/

// C header
#include <float.h>
// My headers
#include "collisionPlane.h"
#include "CollisionControllers.h"
#include "resource.h"

static CollisionPlaneClassDesc theCollisionPlaneDesc;
ClassDesc* GetCollisionPlaneDesc(){	return &theCollisionPlaneDesc;}

//List of descriptors:
static ParamBlockDescID descVer1[] = 
{
	{ TYPE_FLOAT, NULL, FALSE, 0 },	//length controlled by a spinner
	{ TYPE_FLOAT, NULL, FALSE, 1 },	//width  controlled by a spinner
	{ TYPE_INT,   NULL, FALSE, 2 },	//plane  controlled by a radioBox
};

static ParamVersionDesc oldVersions[] =
{
	ParamVersionDesc(descVer1, 3, 1)
};

//
// Max3 paramblock2 version
//
ParamBlockDesc2 colPlaneDesc (colplane_params, _T("ColPlane parameters"), IDS_COLPLANE, &theCollisionPlaneDesc, P_AUTO_CONSTRUCT | P_AUTO_UI, 0,
						 IDD_COL_PLANE_PANEL, IDS_PARAMS, 0, 0, NULL,
						 colplane_length, _T("Length"), TYPE_WORLD, 0, IDS_LENGTH,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_LENGTH_EDIT, IDC_LENGTH_SPIN, 0.01f,
							end,
						 colplane_width, _T("Width"), TYPE_WORLD, 0, IDS_WIDTH,
							p_default,		10.0f,
							p_range, 		0.0f, float(1.0E30), 
							p_ui,			TYPE_SPINNER, EDITTYPE_POS_UNIVERSE, IDC_WIDTH_EDIT, IDC_WIDTH_SPIN, 0.01f,
							end,
						 colplane_orien, _T("Orientation"), TYPE_INT, 0, IDS_ORIEN,
							p_default,		0,
							p_ui,			TYPE_RADIO, 3, IDC_XY_RADIO, IDC_XZ_RADIO, IDC_YZ_RADIO, 
							p_vals,			colplane_xy, colplane_xz, colplane_yz,
							end, 
						end);

//
//        name: CollisionSphereClassDesc::OkToCreate
// description: Return if it is OK to create a collision sphere i.e there is one object selected
//
BOOL CollisionPlaneClassDesc::OkToCreate(Interface *ip)
{
//	Allow creation from create panel button
	return TRUE;
}

int CollisionPlaneClassDesc::BeginCreate(Interface *ip)
{
	theSelectCommandMode.SetObjectClassIDs(SuperClassID(), ClassID());
	theSelectCommandMode.Begin((IObjParam*)ip);
	ip->PushCommandMode(&theSelectCommandMode);

	return TRUE;
}

int CollisionPlaneClassDesc::EndCreate(Interface *ip)
{
	theSelectCommandMode.End((IObjParam*)ip);
	ip->RemoveMode(&theSelectCommandMode);

	return TRUE;
}

const TCHAR* CollisionPlaneClassDesc::ClassName() 
{
	return GetString(IDS_COLPLANE);
}	

const TCHAR* CollisionPlaneClassDesc::Category()
{
	return GetString(IDS_CATEGORY);
}

/*****************************************************************************
 ** NAME       || CollisionPlane()
 ** PURPOSE    || Constructor. Set up the pblock.
 ** PARAMETERS || 
 ** RETURNS    || 
 *****************************************************************************/
CollisionPlane::CollisionPlane():
	pBlock2(NULL)
{
	theCollisionPlaneDesc.MakeAutoParamBlocks(this);
}

/*****************************************************************************
 ** NAME       || Clone()
 ** PURPOSE    || Clones the object
 ** PARAMETERS || 
 ** RETURNS    || 
 *****************************************************************************/
RefTargetHandle CollisionPlane::Clone(RemapDir& remap) 
{
	CollisionPlane* pClone = new CollisionPlane();
	BaseClone(this, pClone, remap);
	pClone->ReplaceReference(PBLOCK_REF_NUM, remap.CloneRef(pBlock2));
	return pClone;
}

void CollisionPlane::InitNodeName(TSTR& s)
{
	s = GetString(IDS_COLPLANE_NODE);
}

TCHAR *CollisionPlane::GetObjectName() 
{
	return GetString(IDS_COLPLANE);
}

void CollisionPlane::GetClassName(TSTR& s)
{
	s = GetString(IDS_COLPLANE);
}

/*******************************************************************************
 *        name : CollisionPlane::Display()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
int CollisionPlane::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);

	SetHelperColour(inode, gw);

	DrawPlane(gw);

	return 0;
}

/*******************************************************************************
 *        name : CollisionPlane::drawPlane()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionPlane::DrawPlane(GraphicsWindow *gw)
{
	if(!m_isDisplayed)
		return;

	//For xy plane, x = length, y = width.
	//For xz plane, x = width,  z = length.
	//For yz plane, y = length, z = width.
	
	DrawLineProc lineProc(gw);
	Point3 pts[12];
	float l, w;
	int plane;
	Interval ivalid = FOREVER;
	TimeValue t = 0;

	pBlock2 -> GetValue(colplane_length, t, l, ivalid);
	pBlock2 -> GetValue(colplane_width , t, w, ivalid);
	pBlock2 -> GetValue(colplane_orien,  t, plane , ivalid);

	switch (plane)
	{
	case colplane_xy:
		//Edges:
		pts[0] = Point3(-l, -w, 0.0f);
		pts[1] = Point3(-l, w, 0.0f);

		pts[2] = pts[1];
		pts[3] = Point3(l, w, 0.0f);

		pts[4] = pts[3];
		pts[5] = Point3(l, -w, 0.0f);

		pts[6] = pts[5];
		pts[7] = Point3(-l, -w, 0.0f);

		//Diagonals:
		pts[8] = Point3(-l, -w, 0.0f);
		pts[9] = Point3(l, w, 0.0f);

		pts[10] = Point3(l, -w, 0.0f);
		pts[11] = Point3(-l, w, 0.0f);
		break;
	case colplane_xz:
		//Edges:
		pts[0] = Point3(-w, 0.0f, -l);
		pts[1] = Point3(-w, 0.0f, l);

		pts[2] = pts[1];
		pts[3] = Point3(w, 0.0f, l);

		pts[4] = pts[3];
		pts[5] = Point3(w, 0.0f, -l);

		pts[6] = pts[5];
		pts[7] = Point3(-w, 0.0f, -l);

		//Diagonals:
		pts[8] = Point3(-w, 0.0f, -l);
		pts[9] = Point3(w, 0.0f, l);

		pts[10] = Point3(w, 0.0f, -l);
		pts[11] = Point3(-w, 0.0f, l);
		break;
	case colplane_yz:
		//Edges:
		pts[0] = Point3(0.0f, -l, -w);
		pts[1] = Point3(0.0f, -l, w);

		pts[2] = pts[1];
		pts[3] = Point3(0.0f, l, w);

		pts[4] = pts[3];
		pts[5] = Point3(0.0f, l, -w);

		pts[6] = pts[5];
		pts[7] = Point3(0.0f, -l, -w);

		//Diagonals:
		pts[8] = Point3(0.0f, -l, -w);
		pts[9] = Point3(0.0f, l, w);

		pts[10] = Point3(0.0f, l, -w);
		pts[11] = Point3(0.0f, -l, w);
		break;
	default:
		break;
	}

	for(int lineIndex=0;lineIndex<12; lineIndex+=2)
	{
		Point3 pt[3] = {pts[lineIndex], pts[lineIndex+1], Point3()};
		lineProc.proc(pt,2);
	}
};
	
/*******************************************************************************
 *        name : CollisionPlane::HitTest()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
int CollisionPlane::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;

	DWORD savedLimits;

	GraphicsWindow *gw = vpt->getGW();

	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(inode->GetObjectTM(t));
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	DrawPlane(gw);

	int res = gw->checkHitCode();

	gw->setRndLimits(savedLimits);

	return res;
}

/*******************************************************************************
 *        name : CollisionPlane::GetWorldBoundBox()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionPlane::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	//The object can handle this by transforming the 8 points of its local 
	//bounding box into world space and take the minimums and maximums of 
	//the result.  Although this isn't necessarily the tightest bounding box 
	//of the objects points in world space, it is close enough.

	Matrix3 local2world = inode -> GetObjectTM(t);

	Point3 minPoint((float)FLT_MAX, (float)FLT_MAX, (float)FLT_MAX);
	Point3 maxPoint = -minPoint;

	Box3 localBox;
	
	GetLocalBoundBox(t, inode, vp, localBox);
	
	Point3 worldP;

	int i;

	for (i = 0; i < 8; i++)
	{
		worldP = local2world*localBox[i];
		if (worldP.x < minPoint.x)
		{
			minPoint.x = worldP.x;
		}
		if (worldP.y < minPoint.y)
		{
			minPoint.y = worldP.y;
		}
		if (worldP.z < minPoint.z)
		{
			minPoint.z = worldP.z;
		}

		if (worldP.x > maxPoint.x)
		{
			maxPoint.x = worldP.x;
		}
		if (worldP.y > maxPoint.y)
		{
			maxPoint.y = worldP.y;
		}
		if (worldP.z > maxPoint.z)
		{
			maxPoint.z = worldP.z;
		}
	}

	box = Box3(minPoint, maxPoint);

}

/*******************************************************************************
 *        name : CollisionPlane::GetLocalBoundBox()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionPlane::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	float l, w;
	int plane;
	Interval ivalid;

	pBlock2 -> GetValue(colplane_length, t, l, ivalid);
	pBlock2 -> GetValue(colplane_width,  t, w, ivalid);
	pBlock2 -> GetValue(colplane_orien,  t, plane, ivalid);

	Point3 maxPoint;

	//We need some depth for the bounding box, so we look at the length and width,
	//and use this to generate a depth.
	float boxDepth;
	if (l > w)
	{
		boxDepth = w/10;
	}
	else
	{
		boxDepth = l/10;
	}

	//For xy plane, x = length, y = width.
	//For xz plane, x = width,  z = length.
	//For yz plane, y = length, z = width.
	
	switch (plane)
	{
	case colplane_xy:
		maxPoint = Point3(l, w, boxDepth);
		break;
	case colplane_xz:
		maxPoint = Point3(w, boxDepth, l);
		break;
	case colplane_yz:
		maxPoint = Point3(boxDepth, l, w);
		break;
	default:
		maxPoint = Point3(0.0f, 0.0f, 0.0f);
		break;
	}
	Point3 minPoint = -maxPoint;

	box = Box3(minPoint, maxPoint);
}

/*******************************************************************************
 *        name : CollisionPlane::SetReference()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionPlane::SetReference(int i, RefTargetHandle rTarg)
{
	switch (i) 
	{
	case PBLOCK_REF_NUM: 	 
		pBlock2 = (IParamBlock2*)rTarg;
		break;
	}
}

RefTargetHandle CollisionPlane::GetReference(int i)
{
	if (i == PBLOCK_REF_NUM)
	{
		return pBlock2;
	}
	return NULL;
}

int CollisionPlane::NumRefs()
{
	return 1;
}

/*******************************************************************************
 *        name : CollisionPlane::BeginEditParams()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionPlane::BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev)
{
	theCollisionPlaneDesc.BeginEditParams(ip, this, flags, prev);

	CollisionPlaneDlgProc* proc = new CollisionPlaneDlgProc();
	proc->SetPlanePointer(this);

	colPlaneDesc.SetUserDlgProc(proc);
}

/*******************************************************************************
 *        name : CollisionPlane::EndEditParams()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionPlane::EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)
{
	theCollisionPlaneDesc.EndEditParams(ip, this, flags, next);
}


/*******************************************************************************
 *        name : CollisionPlane::Flip()
 * description : Swaps the length and width over
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionPlane::Flip()
{
	Interval ivalid = FOREVER;

	TimeValue t = 0;

	float w, l;

	pBlock2 -> GetValue(colplane_length, t, l, ivalid);
	pBlock2 -> GetValue(colplane_width , t, w, ivalid);

	pBlock2 -> SetValue(colplane_length, t, float(w));
	pBlock2 -> SetValue(colplane_width, t, float(l));
}

INT_PTR CollisionPlaneDlgProc::DlgProc(TimeValue t,IParamMap2 *map,HWND hWnd,unsigned int msg,WPARAM wParam,LPARAM lParam)
{
	BOOL rt = CollisionVolumeDlgProc::DlgProc(t, map, hWnd, msg, wParam, lParam);
	switch (msg) 
	{
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_FLIP_BUTTON:
			if (pPlane)
				pPlane -> Flip();
			break;
		}
		break;
	default:
		return FALSE;
	}
	return rt;
}

//
//        name: CollisionSphere::SetupFromSelected
// description: Setup collision sphere from selected Node
//          in: pSelected = pointer to selected node
//         out: 
//
BOOL CollisionPlane::SetupFromSelected(INode *pSelected, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Box3 local, world;
	Matrix3 mat = pSelected->GetNodeTM(t);
	Point3 centre;
	Point3 size;

	mat.NoScale();

	GetGroupLocalBoundBox(pSelected, local, mat);
	GetGroupWorldBoundBox(pSelected, world);

	// work out centre and size of collision box
	centre = world.Center();
	size = local.Width() / 2.0f;

	// set collision sphere node transformation matrix
	mat.SetTrans(centre);
	pNode->SetNodeTM(t, mat);

	if ((size.z <= size.x) && (size.z <= size.y))
	{
		//xy plane:
		pBlock2 -> SetValue(colplane_orien,  t, colplane_xy);
		pBlock2 -> SetValue(colplane_length, t, size.x);
		pBlock2 -> SetValue(colplane_width,  t, size.y);
	}
	else if ((size.y <= size.x) && (size.y <= size.z))
	{
		//xz plane:
		pBlock2 -> SetValue(colplane_orien,  t, colplane_xz);
		pBlock2 -> SetValue(colplane_length, t, size.z);
		pBlock2 -> SetValue(colplane_width,  t, size.x);
	}
	else
	{
		//yz plane:
		pBlock2 -> SetValue(colplane_orien,  t, colplane_yz);
		pBlock2 -> SetValue(colplane_length, t, size.y);
		pBlock2 -> SetValue(colplane_width,  t, size.z);
	}

	// set controller
	Control *pOrienControl = new StaticRotationControl;
	Control *pTMControl = pNode->GetTMController();
	pTMControl->SetRotationController(pOrienControl);

	return TRUE;
}

