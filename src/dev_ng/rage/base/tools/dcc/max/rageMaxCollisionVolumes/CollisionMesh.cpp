/*******************************************************************************
 *
 *    Filename : CollisionMesh.cpp
 *     Creator : 
 *     $Author : $
 *       $Date : $
 *   $Revision : $
 * Description : 
 *
 *******************************************************************************/

// C header
#include <float.h>
// Max header
#include <Max.h>
#include <imenuman.h>
// My headers
#include "CollisionMesh.h"
#include "CollisionControllers.h"
#include "resource.h"
#include "IContainerObject.h"
#include "MaxUtil/MaxUtil.h"

static CollisionMeshClassDesc CollisionMeshDesc;
ClassDesc* GetCollisionMeshDesc() {return &CollisionMeshDesc;}

static CollisionMeshShadowClassDesc CollisionMeshShadowDesc;
ClassDesc* GetCollisionMeshShadowDesc() {return &CollisionMeshShadowDesc;}

static CollisionMeshDummyClassDesc CollisionMeshDummyDesc;
ClassDesc* GetCollisionMeshDummyDesc() {return &CollisionMeshDummyDesc;}

static GenSubObjType SOT_Face(1);

MoveModBoxCMode *CollisionMesh::moveMode;
RotateModBoxCMode *CollisionMesh::rotMode;
UScaleModBoxCMode *CollisionMesh::uscaleMode;
NUScaleModBoxCMode *CollisionMesh::nuscaleMode;
SquashModBoxCMode *CollisionMesh::squashMode;
SelectModBoxCMode *CollisionMesh::selectMode;

#define SL_OBJECT 0
#define SL_FACE 1

#define MAX_MATID	0xffff

//
// ACTION stuff
//
// action callback function
class CollisionMeshActionCB : public ActionCallback
{
public:
	void SetInterfacePtr(Interface* ip) {m_ip = ip;}
	BOOL ExecuteAction(int id){
		int i;
		switch(id)
		{
		case ConvertToEditableMeshActionId:
			for(i=0; i<m_ip->GetSelNodeCount(); i++)
			{
				ConvertToEditableMesh(m_ip->GetSelNode(i), m_ip);
			}
			m_ip->RedrawViews(m_ip->GetTime());
			break;
		case ConvertToColMeshActionId:
			for(i=0; i<m_ip->GetSelNodeCount(); i++)
			{
				ConvertToColMesh(m_ip->GetSelNode(i), m_ip);
			}
			m_ip->RedrawViews(m_ip->GetTime());
			break;
		case ConvertToShadMeshActionId:
			for(i=0; i<m_ip->GetSelNodeCount(); i++)
			{
				ConvertToShadMesh(m_ip->GetSelNode(i), m_ip);
			}
			m_ip->RedrawViews(m_ip->GetTime());
			break;
		}

		return TRUE;
	}
private:
	Interface* m_ip;
};

// 
// action table class
//
class CollisionMeshActionTable : public ActionTable
{
public:
	CollisionMeshActionTable(ActionTableId id, ActionContextId contextId, TSTR& name, 
		HACCEL hDefaults, int numIds, ActionDescription* pOps, HINSTANCE hInst, Interface* ip) :
	ActionTable(id, contextId, name, hDefaults, numIds, pOps, hInst),
	m_ip(ip) {}

	BOOL IsItemVisible(int cmdId)
	{
		int i;
		switch(cmdId)
		{
		case ConvertToEditableMeshActionId:
			// return false if any of the selected objects are not Collision meshes
			for(i=0; i<m_ip->GetSelNodeCount(); i++)
			{
				if(	(m_ip->GetSelNode(i)->GetObjectRef()->ClassID() != COLLISION_MESH_CLASS_ID) &&
					(m_ip->GetSelNode(i)->GetObjectRef()->ClassID() != COLLISION_MESHSHAD_CLASS_ID))
					return FALSE;
			}
			break;
		case ConvertToColMeshActionId:
		case ConvertToShadMeshActionId:
			// return false if cannot convert any of the selected objects into a tri object
			for(i=0; i<m_ip->GetSelNodeCount(); i++)
			{
				if(!m_ip->GetSelNode(i)->GetObjectRef()->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID,0)))
					return FALSE;
			}
			break;
		}
		return TRUE;
	}

private:
	Interface* m_ip;
};

CollisionMeshActionCB theCollisionMeshActionCB;

//
// name:		BuildActionTable
// description:	Build an action table for MaxDataStore actions
//
ActionTable* BuildActionTable()
{
	static ActionDescription aActions[] = {
		ConvertToEditableMeshActionId, IDS_TOEDITMESH_ATTR, IDS_TOEDITMESH_ATTR, IDS_CATEGORY,
		ConvertToColMeshActionId, IDS_TOCOLMESH_ATTR, IDS_TOCOLMESH_ATTR, IDS_CATEGORY,
		ConvertToShadMeshActionId, IDS_TOSHADMESH_ATTR, IDS_TOSHADMESH_ATTR, IDS_CATEGORY
	};

	TSTR name = GetString(IDS_CATEGORY);

    ActionTable* pTab = new CollisionMeshActionTable(COLLISIONMESH_ACTIONTABLEID,COLLISIONMESH_ACTIONCONTEXTID,name,NULL,3,aActions,hInstance,GetCOREInterface());
    GetCOREInterface()->GetActionManager()->RegisterActionContext(COLLISIONMESH_ACTIONCONTEXTID, "Collision Volumes");
    return pTab;
}


ActionTable* GetActionTable()
{
	static ActionTable* pActionTable = NULL;

	if(pActionTable == NULL)
		pActionTable = BuildActionTable();

	return pActionTable;
}

const TCHAR* CollisionMeshClassDesc::ClassName() 
{ 
	return GetString(IDS_COLMESH); 
}

ActionTable* CollisionMeshClassDesc::GetActionTable(int i)
{
	return ::GetActionTable();
}

const TCHAR* CollisionMeshClassDesc::Category() 
{ 
	return GetString(IDS_CATEGORY); 
}

bool hasActionItem(IMenu* menu,ActionItem* item)
{
	for(int i=0;i<menu->NumItems(); i++)
	{
		IMenuItem* pItem = menu->GetItem(i);

		if(pItem->GetActionItem() == item)
		{
			return true;
		}
	}

	return false;
}

DWORD CollisionMeshDummyGUP::Start()
{
	CollisionMesh::InitQuad();

	return GUPRESULT_KEEP;
}

void AddQuadMenuItems(Interface* ip)
{
	IMenuManager* pMenuMan = ip->GetMenuManager();
	bool newlyRegistered = pMenuMan->RegisterMenuBarContext(COLLISIONMESH_MENUCONTEXTID, "MaxDataStore Context");
	
	// if menu already registered then return
	if(!newlyRegistered)
		return;
	
	// get main viewport quad menu
	IQuadMenuContext *pContext = (IQuadMenuContext*)pMenuMan->GetContext(kViewportQuad);

	// get no button pressed menu context
	IQuadMenu* pQMenu = pContext->GetRightClickMenu(IQuadMenuContext::kNonePressed);
	IMenu* pMenu = pQMenu->GetMenu(0);
	
	ActionTable* pActionTable = GetActionTable();
	ActionItem* pToEditableMeshAction = pActionTable->GetAction( ConvertToEditableMeshActionId );
	ActionItem* pToColMeshAction = pActionTable->GetAction( ConvertToColMeshActionId );
	ActionItem* pToShadMeshAction = pActionTable->GetAction( ConvertToShadMeshActionId );
	
	// create menu items
	IMenuItem* pToEditableMeshItem = GetIMenuItem();
	pToEditableMeshItem->SetActionItem(pToEditableMeshAction);
	IMenuItem* pToColMeshItem = GetIMenuItem();
	pToColMeshItem->SetActionItem(pToColMeshAction);
	IMenuItem* pToShadMeshItem = GetIMenuItem();
	pToShadMeshItem->SetActionItem(pToShadMeshAction);
	
	// add them to submenu

	IMenu* pConvertSubMenu=NULL;
	for(int i=0; i<pMenu->NumItems(); i++)
	{
		TSTR text;
		IMenuItem* pItem = pMenu->GetItem(i);
		pConvertSubMenu = pItem->GetSubMenu();
		if(pConvertSubMenu)
		{
			if(pConvertSubMenu->GetTitle() == TSTR("Convert To:"))
				break;
			pConvertSubMenu = NULL;
		}
	}

	if(pConvertSubMenu)
	{
		pConvertSubMenu->AddItem(pToEditableMeshItem);
		pConvertSubMenu->AddItem(pToColMeshItem);
		pConvertSubMenu->AddItem(pToShadMeshItem);
	}
	else
	{
		pMenu->AddItem(pToEditableMeshItem);
		pMenu->AddItem(pToColMeshItem);
		pMenu->AddItem(pToShadMeshItem);
	}

	pMenuMan->UpdateMenuBar();
}

CollisionMeshClassDesc::CollisionMeshClassDesc()
{
}

BOOL CollisionMeshClassDesc::OkToCreate(Interface *ip)
{
	return theSelectCommandMode.OkToCreate(ip);
}

int CollisionMeshClassDesc::BeginCreate(Interface *ip)
{
	assert(theSelectCommandMode.OkToCreate(ip));

	theSelectCommandMode.SetObjectClassIDs(SuperClassID(), ClassID());
	theSelectCommandMode.Begin((IObjParam*)ip);
	ip->PushCommandMode(&theSelectCommandMode);

	return TRUE;
}

int CollisionMeshClassDesc::EndCreate(Interface *ip)
{
	theSelectCommandMode.End((IObjParam*)ip);
	ip->RemoveMode(&theSelectCommandMode);

	return TRUE;
}

/*******************************************************************************
 *        name : CollisionMesh::CollisionMesh()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
CollisionMesh::CollisionMesh() 
: m_pTriObject(NULL)
,tempData(NULL)
,m_hFaceWnd(NULL)
{
	CollisionMeshDesc.MakeAutoParamBlocks(this);
	m_useWireframeCol = TRUE;
}

void CollisionMesh::InitQuad()
{
	static bool initialisedActions = false;
	//	static bool initialisedActions = true;
	Interface* ip = GetCOREInterface();

	if(!initialisedActions)
	{
		// action shit
		ip->GetActionManager()->ActivateActionTable(&theCollisionMeshActionCB, COLLISIONMESH_ACTIONTABLEID);
		theCollisionMeshActionCB.SetInterfacePtr(ip);
		// add attribute menu to quad menu
		AddQuadMenuItems(ip);

		initialisedActions = true;
	}
}

/*******************************************************************************
 *        name : CollisionMesh::Clone()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
RefTargetHandle CollisionMesh::Clone(RemapDir& remap) 
{
	CollisionMesh* pClone = new CollisionMesh();
	BaseClone(this, pClone, remap);
	pClone->ReplaceReference(COLMESH_REF_TRIOBJ, remap.CloneRef(m_pTriObject));
	return(pClone);
}

void CollisionMesh::InitNodeName(TSTR& s)
{
	s = GetString(IDS_COLMESH_NODE);
}

TCHAR *CollisionMesh::GetObjectName() 
{
	return GetString(IDS_COLMESH);
}

void CollisionMesh::GetClassName(TSTR& s)
{
	s = GetString(IDS_COLMESH);
}

MeshTempData *CollisionMesh::TempData () {
	if (!tempData) tempData = new MeshTempData(&(GetMesh()));
	return tempData;
}

void CollisionMesh::InvalidateTempData (PartID parts) {
	if (tempData) tempData->Invalidate (parts);
	if (parts & PART_TOPO) GetMesh().InvalidateTopologyCache();
	if (parts & PART_GEOM) GetMesh().InvalidateGeomCache ();
}

/*******************************************************************************
 *        name : CollisionMesh::Display()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
int CollisionMesh::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags)
{
	GraphicsWindow *gw = vpt->getGW();
	Color lineCol = SetHelperColour(inode, gw);

	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);

	DrawMesh(inode,gw, t, mat, lineCol);

	return 0;
}


/*******************************************************************************
 *        name : CollisionMesh::DrawMesh()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionMesh::DrawMesh(INode* inode,GraphicsWindow *gw, TimeValue t, Matrix3& objMat, Color lineCol, bool avoidGWFlags)
{
	if(m_pTriObject==NULL)
		return;

	Mesh& mesh = m_pTriObject->mesh;

	if(!m_isDisplayed)
		return;

	DWORD savedLimits = gw->getRndLimits();
	float selCol[] = {1,1,1};
	float *col = inode->Selected() ? selCol : lineCol;

	if(m_displayMode == 0)
	{
		if(!avoidGWFlags)
		{
			DWORD oldRndLimits = savedLimits;
			oldRndLimits &= ~GW_COLOR_VERTS;
			gw->setRndLimits(oldRndLimits);
		}
		DrawLineProc lineProc(gw);
		int i;
		Point3 pt[4];
		Point3 normal;
		Matrix3 invCamMat;
		float camMat[4][4];
		int persp;
		float hither, yon;

		// get face normal array built if not already built
		m_pTriObject->mesh.checkNormals(TRUE);

		gw->getCameraMatrix(camMat, &invCamMat, &persp, &hither, &yon);
		invCamMat.NoTrans();
		invCamMat.NoScale();
		objMat.NoTrans();
		objMat.NoScale();

		invCamMat = objMat*Inverse(invCamMat);

		lineProc.SetLineColor(col[0],col[1],col[2]);

		for(i=0; i<mesh.numFaces; i++)
		{
			normal = mesh.getFaceNormal(i);
			if((normal * invCamMat)[2] < 0)
				continue;
			pt[0] = mesh.verts[mesh.faces[i].v[0]];
			pt[1] = mesh.verts[mesh.faces[i].v[1]];
			pt[2] = mesh.verts[mesh.faces[i].v[2]];
			pt[3] = mesh.verts[mesh.faces[i].v[0]];
			lineProc.proc(pt,4);
		}
	}
	else
	{
		if(!avoidGWFlags)
		{
			DWORD oldRndLimits = savedLimits;
//			oldRndLimits |= GW_COLOR_VERTS;				// turn on vertex colors
			oldRndLimits &= ~GW_SHADE_CVERTS;			// turn off vertex color shading
//			oldRndLimits |= GW_ILLUM;
			oldRndLimits |= GW_FLAT;
			gw->setRndLimits(oldRndLimits);
		}

		if(m_displayMode == 1)
		{	
			mesh.setVCDisplayData(0);
		}
		else
		{
			mesh.setVCDisplayData(MAP_SHADING);
		}

		mesh.render(gw,inode->Mtls(),NULL,COMP_ALL,inode->NumMtls());

	}
	if(!avoidGWFlags)
	{
		gw->setRndLimits(savedLimits);
	}
};
	
/*******************************************************************************
 *        name : CollisionMesh::HitTest()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
int CollisionMesh::HitTest(TimeValue t, INode* inode, int type, int crossing, 
							 int flags, IPoint2 *p, ViewExp *vpt)
{
	HitRegion hitRegion;
	DWORD savedLimits;
	GraphicsWindow *gw = vpt->getGW();
	Matrix3 objMat = inode->GetObjectTM(t);
	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setTransform(objMat);
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();	

	int res = FALSE;
	if(m_displayMode == 0)
	{
		DrawMesh(inode,gw, t, objMat, Color(1,1,1), true);
		res = gw->checkHitCode();
	}
	else
	{
		if(m_pTriObject)
			res = m_pTriObject->mesh.select( gw, inode->Mtls(), &hitRegion, flags & HIT_ABORTONHIT );
	}

	gw->setRndLimits(savedLimits);

	return res;
}

int CollisionMesh::HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt,ModContext* mc)
{
	static int hitLevel[] = {0,SUBHIT_FACES};
	Interval valid;
	int savedLimits, res = 0;
	GraphicsWindow *gw = vpt->getGW();
	HitRegion hr;
	bool ignoreBackfaces = false;
	bool selByVert = false;
	int pickBoxSize = 4;

	// Setup GW
	MakeHitRegion(hr,type, crossing, pickBoxSize, p);
	gw->setHitRegion(&hr);
	Matrix3 mat = inode->GetObjectTM(t);
	gw->setTransform(mat);
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);

	if (ignoreBackfaces) gw->setRndLimits(gw->getRndLimits() |  GW_BACKCULL);
	else gw->setRndLimits(gw->getRndLimits() & ~GW_BACKCULL);
	gw->clearHitCode();	

	SubObjHitList hitList;
	MeshSubHitRec *rec;
	DWORD hitLev = hitLevel[m_selLevel];
	int localSelByVert = selByVert;

	if (hitLev == SUBHIT_VERTS) {
		BitArray oldHide;
		if (ignoreBackfaces) {
			BOOL flip = mat.Parity();
			oldHide = GetMesh().vertHide;
			BitArray faceBack;
			faceBack.SetSize (GetMesh().numFaces);
			faceBack.ClearAll ();
			for (int i=0; i<GetMesh().numFaces; i++) {
				DWORD *vv = GetMesh().faces[i].v;
				IPoint3 A[3];
				for (int j=0; j<3; j++) gw->wTransPoint (&(GetMesh().verts[vv[j]]), &(A[j]));
				IPoint3 d1 = A[1] - A[0];
				IPoint3 d2 = A[2] - A[0];
				if (flip) {
					if ((d1^d2).z > 0) continue;
				} else {
					if ((d1^d2).z < 0) continue;
				}
				for (int j=0; j<3; j++) GetMesh().vertHide.Set (vv[j]);
				faceBack.Set (i);
			}
			for (int i=0; i<GetMesh().numFaces; i++) {
				if (faceBack[i]) continue;
				DWORD *vv = GetMesh().faces[i].v;
				for (int j=0; j<3; j++) GetMesh().vertHide.Clear (vv[j]);
			}
			GetMesh().vertHide |= oldHide;
		}
		DWORD thisFlags = flags | hitLev;
		if (localSelByVert) thisFlags |= SUBHIT_USEFACESEL;
		res = GetMesh().SubObjectHitTest(gw, gw->getMaterial(), &hr, thisFlags, hitList);
		if (ignoreBackfaces) GetMesh().vertHide = oldHide;
	} else {
		res = GetMesh().SubObjectHitTest(gw, gw->getMaterial(), &hr, flags|hitLev, hitList);
	}

	rec = hitList.First();
	while (rec) {
		vpt->LogHit(inode,mc,rec->dist,rec->index,NULL);
		rec = rec->Next();
	}

	gw->setRndLimits(savedLimits);	
	return res;
}

/*******************************************************************************
 *        name : CollisionMesh::GetWorldBoundBox()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionMesh::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	Matrix3 local2world = inode -> GetObjectTM(t);
	Box3 localBox;
	
	GetLocalBoundBox(t, inode, vp, localBox);

	box = localBox * local2world;
}

/*******************************************************************************
 *        name : CollisionMesh::GetLocalBoundBox()
 * description : 
 *          in : 
 *         out : 
 *******************************************************************************/
void CollisionMesh::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vp, Box3& box )
{
	if(m_pTriObject)
		box = m_pTriObject->mesh.getBoundingBox();
	else
		box.Init();
}

int CollisionMesh::NumSubObjTypes()
{
	return 1;
}

ISubObjType *CollisionMesh::GetSubObjType(int i)
{
	static bool initialized = false;
	if(!initialized)
	{
		initialized = true;
		SOT_Face.SetName("Face");
	}

	switch(i)
	{
	case -1:
		if(GetSubObjectLevel() > 0)
			return GetSubObjType(GetSubObjectLevel()-1);
		break;
	case 0:
		return &SOT_Face;
	}

	return NULL;
}

DWORD CollisionMesh::GetMatIndex()
{
	DWORD mat = UNDEFINED;
	for (int j=0; j<GetMesh().getNumFaces(); j++) {
		if (!GetMesh().faceSel[j]) continue;
		if (mat==UNDEFINED) mat = GetMesh().getFaceMtlIndex(j);
		else if (GetMesh().getFaceMtlIndex(j) != mat) return UNDEFINED;
	}
	return mat;
}

void CollisionMesh::ApplyMeshDelta (MeshDelta & md, MeshDeltaUser *mdu, TimeValue t) 
{
	//md.Apply(GetMesh());
	//mdu->LocalDataChanged(md.PartsChanged());
}


void CollisionMesh::SetMatIndex(DWORD index)
{
	MeshDelta tmd(GetMesh());
	for (int j=0; j<GetMesh().getNumFaces(); j++) {
		if (GetMesh().faceSel[j]) tmd.SetMatID (j,(MtlID)index);
	}
	//ApplyMeshDelta (tmd, m_pTriObject, m_ip->GetTime());
	tmd.Apply(GetMesh());
	m_ip->RedrawViews(m_ip->GetTime());
}

void CollisionMesh::UpdateSurfaceSpinner(TimeValue t, HWND hWnd, int idSpin)
{
	ISpinnerControl *spin = GetISpinner (GetDlgItem (hWnd, idSpin));
	if (!spin) return;

	int num=0;
	float fval = 0.0f;
	DWORD ival = 0;
	bool isInt=false;
	bool indeterminate=false;

	switch (idSpin) 
	{
	case IDC_MAT_IDSPIN:
		isInt = true;
		ival = GetMatIndex();
		if (ival == UNDEFINED) indeterminate = true;
		else ival++;	// Always display one higher.
		num = 1;
		break;
	}

	spin->SetIndeterminate (indeterminate);
	// Set the actual value:
	if (isInt) spin->SetValue (int(ival), FALSE);
	else spin->SetValue (fval, FALSE);
	ReleaseISpinner(spin);
}

INT_PTR CALLBACK CollMeshFaceProc(HWND hWnd, unsigned int msg, WPARAM wParam, LPARAM lParam)
{
#pragma warning(suppress:4312)
	CollisionMesh *cm = (CollisionMesh*)GetWindowLongPtr(hWnd,GWLP_USERDATA);
	ISpinnerControl *spin;

	switch (msg) 
	{
	case WM_INITDIALOG:
		cm = (CollisionMesh*)lParam;
		SetWindowLongPtr(hWnd,GWLP_USERDATA,lParam);

		SetupIntSpinner(hWnd, IDC_MAT_IDSPIN, IDC_MAT_ID, 1, MAX_MATID, 1);
		break;

	case WM_PAINT:

		if (GetDlgItem (hWnd, IDC_MAT_IDSPIN)) {
			cm->UpdateSurfaceSpinner(cm->m_ip->GetTime(), hWnd, IDC_MAT_IDSPIN);
		}

		break;
	case CC_SPINNER_CHANGE:
		spin = (ISpinnerControl*)lParam;
		switch (LOWORD(wParam)) 
		{
		case IDC_MAT_IDSPIN:
			cm->SetMatIndex(spin->GetIVal()-1);
			break;
		}
	}

	return FALSE;
}

void CollisionMesh::ActivateSubobjSel(int level, XFormModes& modes)
{
	m_selLevel = level;

	switch(level)
	{
	case 0:
		
		if(m_hFaceWnd)
		{
			m_ip->DeleteRollupPage(m_hFaceWnd);
			m_hFaceWnd = NULL;
		}

		break;
	case 1:
		modes = XFormModes(moveMode,rotMode,nuscaleMode,uscaleMode,squashMode,selectMode);

		m_hFaceWnd = m_ip->AddRollupPage(	hInstance, 
											MAKEINTRESOURCE(IDD_COL_MESH_PANEL_FACE),
											CollMeshFaceProc,
											_T("Face"),
											(LPARAM)this);

		break;
	}

	m_ip->PipeSelLevelChanged();
	NotifyDependents(FOREVER,PART_DISPLAY,REFMSG_CHANGE);
}

void CollisionMesh::LocalDataChanged()
{
	SendMessage(m_hFaceWnd,WM_PAINT,0,0);
	m_ip->RedrawViews(m_ip->GetTime());
	NotifyDependents(FOREVER, PART_SELECT, REFMSG_CHANGE);
}

void CollisionMesh::GetSubObjectCenters(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc)
{
	Matrix3 tm = node->GetObjectTM(t);

	if (m_selLevel == SL_OBJECT) return;

	Tab<Point3> *centers = TempData()->ClusterCenters(MESH_FACE);
	for (int i=0; i<centers->Count(); i++) cb->Center((*centers)[i]*tm,i);
}

void CollisionMesh::GetSubObjectTMs(SubObjAxisCallback *cb,TimeValue t,INode *node,ModContext *mc)
{
	Matrix3 tm, otm = node->GetObjectTM(t);
	int i,ct;

	switch(m_selLevel) 
	{
	case SL_OBJECT:
		break;

	default:
		ct = TempData()->ClusterNormals(MESH_FACE)->Count();
		for (i=0; i<ct; i++) {
			tm = TempData()->ClusterTM (i) * otm;
			cb->TM(tm,i);
		}
		break;
	}
}

void CollisionMesh::ClearSelection(int sl) 
{
	BitArray sel;
	sel.SetSize (GetMesh().getNumFaces());
	sel.ClearAll ();
	GetMesh().faceSel = sel;
	LocalDataChanged();
}

void CollisionMesh::SelectSubComponent(HitRecord *hitRec, BOOL selected, BOOL all, BOOL invert)
{
	if(!m_pTriObject) return;
	if (m_selLevel == SL_OBJECT) return;

	if (!m_ip) return;
	m_ip->ClearCurNamedSelSet ();
	TimeValue t = m_ip->GetTime();

	AdjFaceList *af=NULL;
	BitArray nsel;
	HitRecord *hr;

	switch (m_selLevel) 
	{
	case SL_FACE:

		nsel = GetMesh().faceSel;

		for (hr=hitRec; hr != NULL; hr = hr->Next()) 
		{
			nsel.Set (hr->hitInfo, invert ? !GetMesh().faceSel[hr->hitInfo] : selected);
			if (!all) break;
		}

		GetMesh().faceSel = nsel;
		break;
	}

	LocalDataChanged();
}

//
//        name: CollisionMesh::CanConvertToType
// description: Returns if can convert collision mesh to other types of object. Can convert to TriObject(Editable mesh)
//
int CollisionMesh::CanConvertToType(Class_ID obtype)
{
/*	
	Don't know if I want to do this. It could fuck up a whole load of other plugins. Which expect objects that
	convert to EditableMeshes to be geometry.

	if(obtype == Class_ID(TRIOBJ_CLASS_ID, 0))
		return 1;*/
	return Object::CanConvertToType(obtype);
}

//
//        name: CollisionMesh::ConvertToType
// description: Convert collision mesh to other object. Can only convert to TriObject
//
Object* CollisionMesh::ConvertToType(TimeValue t, Class_ID obtype)
{
/*
	See ConConvertToType()

	if(obtype == Class_ID(TRIOBJ_CLASS_ID, 0))
	{
		TriObject *pTri = CreateNewTriObject();
		pTri->mesh = m_mesh;
		pTri->SetChannelValidity(TOPO_CHAN_NUM,ObjectValidity(t));
		pTri->SetChannelValidity(GEOM_CHAN_NUM,ObjectValidity(t));		
		return pTri;
	}*/
	return Object::ConvertToType(t,obtype);
}

//
//        name: CollisionMesh::CopyMesh
// description: Copy mesh from a tri object
//          in: pTriObject = pointer to tri object
//
void CollisionMesh::CopyMesh(TriObject *pTri, Matrix3& mat)
{
	TriObject *pNewTri = CreateNewTriObject();
	int i;

	ReplaceReference(COLMESH_REF_TRIOBJ, pNewTri);

	m_pTriObject->mesh.DeepCopy(&pTri->mesh, ALL_CHANNELS);
	//m_pTriObject->mesh.DeepCopy(&pTri->mesh, GEOM_CHANNEL|TOPO_CHANNEL|VERTCOLOR_CHANNEL);

	// transform vertices
	for(i=0; i< m_pTriObject->mesh.numVerts; i++)
		m_pTriObject->mesh.verts[i] = m_pTriObject->mesh.verts[i] * mat;
	m_pTriObject->mesh.InvalidateGeomCache();
	m_pTriObject->mesh.BuildStripsAndEdges();
}

//
//        name: CollisionSphere::SetupFromSelected
// description: Setup collision sphere from selected Node
//          in: pSelected = pointer to selected node
//         out: 
//
BOOL CollisionMesh::SetupFromSelected(INode *pSelected, INode *pNode)
{
	TimeValue t = m_ip->GetTime();
	Matrix3 mat = pSelected->GetNodeTM(t);
	Matrix3 objMat = pSelected->GetObjectTM(t);
	Point3 centre;
	Point3 size;
	Object *pObject = pSelected->EvalWorldState(t).obj;
	TriObject *pTri = (TriObject *)pObject->ConvertToType(t, Class_ID(TRIOBJ_CLASS_ID,0));

	pNode->SetWireColor(RGB(255,0,0));

	if(pTri == NULL)
		return FALSE;

	objMat *= Inverse(mat);
	
	CopyMesh(pTri, objMat);

	if(pTri != pObject)
		delete pTri;

	pNode->SetNodeTM(t, mat);

	// set controllers
//	Control *pOrienControl = new StaticRotationControl;
//	Control *pPosnControl = new StaticPositionControl;
//	Control *pTMControl = pNode->GetTMController();
//	pTMControl->SetPositionController(pPosnControl);
//	pTMControl->SetRotationController(pOrienControl);

	return TRUE;
}


//
//        name: ConvertToEditableMesh
// description: Convert collision mesh to an editable mesh
//          in: pNode = pointer to node
//
void ConvertToEditableMesh(INode *pNode, Interface *ip)
{
	TimeValue t = ip->GetTime();
	Object *pObj = pNode->EvalWorldState(t).obj;

	if(	(pObj->ClassID() == COLLISION_MESH_CLASS_ID) ||
		(pObj->ClassID() == COLLISION_MESHSHAD_CLASS_ID))
	{
		theHold.Begin();

		TriObject *pTriObj = static_cast<TriObject*>(pObj->GetReference(COLMESH_REF_TRIOBJ));
		Mesh& mesh = pTriObj->GetMesh();

		mesh.InvalidateGeomCache();
		mesh.BuildStripsAndEdges();

		pNode->SetObjectRef(pTriObj);
		// This causes collision to ping to the origin if it has not been parented to an object.  But only after the file has been saved and re-loaded.
		if(pNode->GetParentNode() != GetCOREInterface()->GetRootNode() && 
			!MaxUtil::IsContainer(pNode->GetParentNode()))
			pNode->SetTMController(NewDefaultMatrix3Controller());

		theHold.Accept("Convert to Editable Mesh");
	}
}
//
//        name: ConvertToColMesh
// description: Convert editable mesh to a collision mesh
//          in: pNode = pointer to node
//
void ConvertToColMesh(INode *pNode, Interface *ip)
{
	TimeValue t = ip->GetTime();
	Object *pObject = pNode->EvalWorldState(t).obj;

	if(pObject->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID,0)))
	{
		theHold.Begin();
		
		Matrix3 mat = pNode->GetParentTM(t);
		Matrix3 objMat = pNode->GetObjectTM(t);
		TriObject *pTri = static_cast<TriObject *>(pObject->ConvertToType(t, Class_ID(TRIOBJ_CLASS_ID,0)));
		CollisionMesh *pMesh = new CollisionMesh;

		assert(pTri != NULL);

		if(	pNode->GetParentNode() == GetCOREInterface()->GetRootNode() ||
			MaxUtil::IsContainer(pNode->GetParentNode()))
		{
			objMat.IdentityMatrix();
		}
		else
		{
			objMat *= Inverse(mat);
		}
	
		pMesh->CopyMesh(pTri, objMat);

		if(pTri != pObject)
			delete pTri;

		pNode->SetObjectRef(pMesh);

		if(	pNode->GetParentNode() != GetCOREInterface()->GetRootNode() && 
			!MaxUtil::IsContainer(pNode->GetParentNode()))
		{
			pNode->SetNodeTM(t, mat);
		}

		theHold.Accept("Convert to Collision Mesh");
	}
}

//
//        name: ConvertToShadMesh
// description: Convert editable mesh to a collision mesh
//          in: pNode = pointer to node
//
void ConvertToShadMesh(INode *pNode, Interface *ip)
{
	TimeValue t = ip->GetTime();
	Object *pObject = pNode->EvalWorldState(t).obj;

	if(pObject->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID,0)))
	{
		theHold.Begin();
		
		Matrix3 mat = pNode->GetParentTM(t);
		Matrix3 objMat = pNode->GetObjectTM(t);
		TriObject *pTri = static_cast<TriObject *>(pObject->ConvertToType(t, Class_ID(TRIOBJ_CLASS_ID,0)));
		ShadowMesh *pMesh = new ShadowMesh;

		assert(pTri != NULL);
		
		objMat *= Inverse(mat);
	
		pMesh->CopyMesh(pTri, objMat);

		if(pTri != pObject)
			delete pTri;

		pNode->SetObjectRef(pMesh);
		pNode->SetNodeTM(t, mat);

		theHold.Accept("Convert to Shadow Mesh");
	}
}

INT_PTR CALLBACK CollMeshProc(HWND hWnd, unsigned int message, WPARAM wParam, LPARAM lParam)
{
	switch (message) 
	{
	case WM_INITDIALOG:
		{
			TSTR version = TSTR("Version ") + GetString(IDS_VERSION);
			SetDlgItemText(hWnd, IDC_VERSION, version);

			CheckDlgButton(hWnd, IDC_DISPLAY_CHECK, CollisionHelperObject::IsDisplayed());

			int iMode = CollisionHelperObject::GetDisplayMode();

			CheckDlgButton(hWnd, IDC_RDO_WIREFRAME, iMode == 0);
			CheckDlgButton(hWnd, IDC_RDO_VERTEXCOL, iMode == 1);
			CheckDlgButton(hWnd, IDC_RDO_ILLUMCOL, iMode == 2);
		}
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_DISPLAY_CHECK:
			CollisionHelperObject::SetDisplayed(IsDlgButtonChecked(hWnd, IDC_DISPLAY_CHECK));
			break;
		case IDC_RDO_WIREFRAME:
			CollisionHelperObject::SetDisplayMode(0);
			break;
		case IDC_RDO_VERTEXCOL:
			CollisionHelperObject::SetDisplayMode(1);
			break;
		case IDC_RDO_ILLUMCOL:
			CollisionHelperObject::SetDisplayMode(2);
			break;
		default:
			return FALSE;
		}
	}

	return FALSE;
}

void CollisionMesh::BeginEditParams(IObjParam *ip, ULONG flags, Animatable *prev)
{
	m_hWnd = 0;
	m_selLevel = 0;
	m_ip = ip;
	m_hFaceWnd = (HWND)0;

	HelperObject::BeginEditParams(ip,flags,prev);
	CollisionMeshDesc.BeginEditParams(ip,this,flags,prev);

	m_hWnd = ip->AddRollupPage(	hInstance, 
								MAKEINTRESOURCE(IDD_COL_MESH_PANEL),
								CollMeshProc,
								_T("Collision Mesh"),
								(LPARAM)this);

	SetWindowPos(m_hWnd,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
	InvalidateRect(m_hWnd,NULL,TRUE);

	moveMode       = new MoveModBoxCMode(this,ip);
	rotMode        = new RotateModBoxCMode(this,ip);
	uscaleMode     = new UScaleModBoxCMode(this,ip);
	nuscaleMode    = new NUScaleModBoxCMode(this,ip);
	squashMode     = new SquashModBoxCMode(this,ip);
	selectMode     = new SelectModBoxCMode(this,ip);
}

void CollisionMesh::EndEditParams(IObjParam *ip, ULONG flags, Animatable *next)
{
	if (moveMode) delete moveMode;
	moveMode = NULL;
	if (rotMode) delete rotMode;
	rotMode = NULL;
	if (uscaleMode) delete uscaleMode;
	uscaleMode = NULL;
	if (nuscaleMode) delete nuscaleMode;
	nuscaleMode = NULL;
	if (squashMode) delete squashMode;
	squashMode = NULL;
	if (selectMode) delete selectMode;
	selectMode = NULL;

	if(tempData) delete tempData;
	tempData = NULL;

	if(m_hWnd)
	{
		ip->DeleteRollupPage(m_hWnd);
		m_hWnd = NULL;
	}

	if(m_hFaceWnd)
	{
		m_ip->DeleteRollupPage(m_hFaceWnd);
		m_hFaceWnd = NULL;
	}

	CollisionMeshDesc.EndEditParams(ip,this,flags,next);
	HelperObject::EndEditParams(ip,flags,next);
}

const TCHAR* CollisionMeshShadowClassDesc::ClassName() 
{ 
	return GetString(IDS_SHADMESH); 
}

void ShadowMesh::GetClassName(TSTR& s) 
{
	s = GetString(IDS_SHADMESH); 
}

TCHAR *ShadowMesh::GetObjectName() 
{ 
	return GetString(IDS_SHADMESH); 
}

void ShadowMesh::InitNodeName(TSTR& s) 
{ 
	s = GetString(IDS_SHADMESHNODE); 
}
