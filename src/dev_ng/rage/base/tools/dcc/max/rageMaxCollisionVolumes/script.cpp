//
//
//    Filename: script.cpp
//     Creator: Adam Fowler
//     $Author: $
//       $Date: $
//   $Revision: $
// Description: Max script interface to section utility
//
//

#include <list>

// max headers
#include <max.h>
#include <modstack.h>
#include <maxscript/maxscript.h>
#include <maxscript/maxwrapper/maxclasses.h>
#include <maxscript/foundation/Numbers.h>
#include <maxscript/foundation/3dmath.h>
#include <maxscript/foundation/strings.h>
#include <maxscript/maxwrapper/mxsmaterial.h>
#include <maxscript/macros/define_instantiation_functions.h> // LPXO: define me last

// my headers
#include "CollisionMesh.h"
#include "VolumeCreateMod.h"

//GRS we get exceptions in these maxscript functions when they return if optimisations
//are on here. This seems unique to max 2009 as we are not getting the same errors in 2011
#pragma optimize( "", off ) //PRAGMA-OPTIMIZE-ALLOW

// ------------------------------------------------------------------------------
// 	Define our new functions
//	This will call the section operation code
// ------------------------------------------------------------------------------
def_visible_primitive( mesh_to_col,				"mesh2col" );
def_visible_primitive( col_to_mesh,				"col2mesh" );
def_visible_primitive( setcolmat,				"setcolmat" );
def_visible_primitive( getcolmat,				"getcolmat" );
def_visible_primitive( add_quad_entries,		"addCollEntriesToQuadMenu" );
def_visible_primitive( volumecreatemod,			"volumecreatemod" );
def_visible_primitive( volumecreateshad,		"volumecreateshad" );
def_visible_primitive( volumecreatecube,		"volumecreatecube" );
def_visible_primitive( volumecreatesphere,		"volumecreatesphere" );
def_visible_primitive( volumecreatecapsule,		"volumecreatecapsule" );
def_visible_primitive( volumecreatecylinder,	"volumecreatecylinder" );
def_visible_primitive( colpolycount,			"GetCollPolyCount" );
def_visible_primitive( GetCollCostAsFaceCount,		"GetCollCostAsFaceCount");
def_visible_primitive( createBoundsFromBndFile, "CreateBoundsFromBndFile" );
def_visible_primitive( getColMesh,				"getColMesh" );

Value *add_quad_entries_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (0)
	check_arg_count(add_quad_entries_cf, 0, count);

	AddQuadMenuItems(GetCOREInterface());
	return &true_value;
}

int 
GetCollPolyCount(INode* pNode)
{
	if(!pNode)
		return 0;

	//find all children of this node that are collision meshes

	int iNumChildren = pNode->NumberOfChildren();
	int iCollCount = 0;

	Object* p_obj = pNode->GetObjectRef();

	if(p_obj->ClassID() == COLLISION_MESH_CLASS_ID)
	{
		CollisionMesh* p_coll = (CollisionMesh*)p_obj;

		if(p_coll->m_pTriObject)
		{
			iCollCount += p_coll->m_pTriObject->GetMesh().getNumFaces();
		}
	}
	else if(p_obj->ClassID() == COLLISION_BOX_CLASS_ID)
	{
		iCollCount += 12;
	}
	else if (
		(p_obj->ClassID() == COLLISION_SPHERE_CLASS_ID) ||
		(p_obj->ClassID() == COLLISION_CAPSULE_CLASS_ID) ||
		(p_obj->ClassID() == COLLISION_PLANE_CLASS_ID) ||
		(p_obj->ClassID() == COLLISION_CYLINDER_CLASS_ID) )
	{
		iCollCount++;
	}

	for(int i=0;i<iNumChildren;i++)
	{
		INode* pChild = pNode->GetChildNode(i);

		iCollCount += GetCollPolyCount(pChild);
	}

	return iCollCount;
}

int 
GetCollCostAsFaceCount(INode* pNode)
{
	if(!pNode)
		return 0;

	//find all children of this node that are collision meshes

	int iNumChildren = pNode->NumberOfChildren();
	int iCollCount = 0;

	Object* p_obj = pNode->GetObjectRef();

	if(p_obj->ClassID() == COLLISION_MESH_CLASS_ID)
	{
		CollisionMesh* p_coll = (CollisionMesh*)p_obj;

		if(p_coll->m_pTriObject)
		{
			iCollCount += p_coll->m_pTriObject->GetMesh().getNumFaces();
		}
	}
	else if(p_obj->ClassID() == COLLISION_BOX_CLASS_ID)
	{
		iCollCount += 2;
	}
	else if (
		(p_obj->ClassID() == COLLISION_SPHERE_CLASS_ID) ||
		(p_obj->ClassID() == COLLISION_CAPSULE_CLASS_ID) ||
		(p_obj->ClassID() == COLLISION_PLANE_CLASS_ID) ||
		(p_obj->ClassID() == COLLISION_CYLINDER_CLASS_ID) )
	{
		iCollCount++;
	}

	for(int i=0;i<iNumChildren;i++)
	{
		INode* pChild = pNode->GetChildNode(i);

		iCollCount += GetCollCostAsFaceCount(pChild);
	}

	return iCollCount;
}

Value* GetCollCostAsFaceCount_cf(Value** arg_list, int count)
{
	return_protected(new Integer(GetCollCostAsFaceCount(arg_list[0]->to_node())));
}
//
//        name: *volumecreatemod_cf
// description: creates a collision mesh for the selected node assuming there are some selected faces
//
Value *volumecreatecube_cf(Value** arg_list, int count)
{
	type_check(arg_list[0], MAXModifier, "volumecreatecube [node]");

	Modifier *pMod = arg_list[0]->to_modifier();

	if(pMod->ClassID() == VOLUMECREATE_CLASS_ID)
	{
		VolumeCreateMod *pVolMod = dynamic_cast<VolumeCreateMod*>(pMod);

		if(pVolMod)
		{
			pVolMod->SetSelLevel(IMESHSEL_FACE);
			pVolMod->CreateVolumes(&VolumeCreateMod::CreateBox);

			return &true_value;
		}
	}

	return &false_value;
}

//
//        name: volumecreatecapsule_cf
// description: creates a collision mesh for the selected node assuming there
//				are some selected faces
//
Value *volumecreatecapsule_cf(Value** arg_list, int count)
{
	type_check(arg_list[0], MAXModifier, "volumecreatecapsule [node]");

	Modifier *pMod = arg_list[0]->to_modifier();

	VolumeCreateMod *pVolMod = dynamic_cast<VolumeCreateMod*>(pMod);

	if(pVolMod)
	{
		pVolMod->SetSelLevel(IMESHSEL_FACE);
		pVolMod->CreateVolumes(&VolumeCreateMod::CreateCapsule);

		return &true_value;
	}

	return &false_value;
}

//
//        name: volumecreatecylinder_cf
// description: creates a collision cylinder for the selected node assuming
//				there are some selected faces
//
Value* volumecreatecylinder_cf( Value** arg_list, int count )
{
	type_check(arg_list[0], MAXModifier, "volumecreatecylinder [node]");

	Modifier *pMod = arg_list[0]->to_modifier();

	VolumeCreateMod *pVolMod = dynamic_cast<VolumeCreateMod*>(pMod);

	if(pVolMod)
	{
		pVolMod->SetSelLevel( IMESHSEL_FACE );
		pVolMod->CreateVolumes( &VolumeCreateMod::CreateCylinder );

		return &true_value;
	}

	return &false_value;
}

//
//        name: *volumecreatemod_cf
// description: creates a collision mesh for the selected node assuming there are some selected faces
//
Value *volumecreatesphere_cf(Value** arg_list, int count)
{
	type_check(arg_list[0], MAXModifier, "volumecreatesphere [node]");

	Modifier *pMod = arg_list[0]->to_modifier();

	VolumeCreateMod *pVolMod = dynamic_cast<VolumeCreateMod*>(pMod);

	if(pVolMod)
	{
		pVolMod->SetSelLevel(IMESHSEL_FACE);
		pVolMod->CreateVolumes(&VolumeCreateMod::CreateSphere);

		return &true_value;
	}

	return &false_value;
}

//
//        name: *volumecreatemod_cf
// description: creates a collision mesh for the selected node assuming there are some selected faces
//
Value *volumecreateshad_cf(Value** arg_list, int count)
{
	type_check(arg_list[0], MAXModifier, "volumecreateshad [node]");

	Modifier *pMod = arg_list[0]->to_modifier();

	VolumeCreateMod *pVolMod = dynamic_cast<VolumeCreateMod*>(pMod);

	if(pVolMod)
	{
		pVolMod->SetSelLevel(IMESHSEL_FACE);
		pVolMod->CreateVolumes(&VolumeCreateMod::CreateShad);

		return &true_value;
	}

	return &false_value;
}

//
//        name: *volumecreatemod_cf
// description: creates a collision mesh for the selected node assuming there are some selected faces
//
Value *volumecreatemod_cf(Value** arg_list, int count)
{
	type_check(arg_list[0], MAXModifier, "volumecreatemod [node]");

	Modifier *pMod = arg_list[0]->to_modifier();

	VolumeCreateMod *pVolMod = dynamic_cast<VolumeCreateMod*>(pMod);

	if(pVolMod)
	{
		pVolMod->SetSelLevel(IMESHSEL_FACE);
		pVolMod->CreateMesh();

		return &true_value;
	}

	return &false_value;
}

//
//        name: *mesh_to_col_cf
// description: Section the whole scene script command
//
Value *mesh_to_col_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(mesh_to_col, 1, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'mesh2col [node]'
	type_check(arg_list[0], MAXNode, "mesh2col [node]");

	INode *pNode = arg_list[0]->to_node();

	Object *pObj = pNode->GetObjectRef();

	// stop object being deleted as it's UI might still be displayed
	pObj->SetAFlag(A_LOCK_TARGET);
	ConvertToColMesh(pNode, MAXScript_interface);
	pObj->ClearAFlag(A_LOCK_TARGET);
	pObj->MaybeAutoDelete();

	MAXScript_interface->RedrawViews(MAXScript_interface->GetTime());

	return &true_value;
}

//
//        name: *section_scene_cf
// description: Section the whole scene script command
//
Value *col_to_mesh_cf(Value** arg_list, int count)
{
	// Make sure we have the correct number of arguments (1)
	check_arg_count(col_to_mesh, 1, count);

	// Check to see if the arguments match up to what we expect
	// We want to use 'col2mesh [node]'
	type_check(arg_list[0], MAXNode, "col2mesh [node]");

	INode *pNode = arg_list[0]->to_node();

	ConvertToEditableMesh(pNode, MAXScript_interface);

	MAXScript_interface->RedrawViews(MAXScript_interface->GetTime());

	return &true_value;
}

Value *setcolmat_cf(Value** arg_list, int count)
{
	INode *pNode = arg_list[0]->to_node();
	pNode->SetMtl(arg_list[1]->to_mtl());

	return &true_value;
}

Value *getcolmat_cf(Value** arg_list, int count)
{
	INode *pNode = arg_list[0]->to_node();
	return MAXMaterial::intern(pNode->GetMtl());
}

Value* colpolycount_cf(Value** arg_list, int count)
{
	return_protected(new Integer(GetCollPolyCount(arg_list[0]->to_node())));
}

Value* getColMesh_cf(Value** arg_list, int count)
{
	INode *pNode = arg_list[0]->to_node();
	Object* p_obj = pNode->GetObjectRef();

	if(p_obj->ClassID() == COLLISION_MESH_CLASS_ID)
	{
		CollisionMesh* p_coll = (CollisionMesh*)p_obj;

		if(p_coll->m_pTriObject)
		{
			return_protected(new MeshValue(&p_coll->m_pTriObject->GetMesh()));
		}
	}
	return &undefined;
}

struct BndVert
{
	float fX,fY,fZ;
};

struct BndTri
{
	float iA,iB,iC,iMat;
};

#define LINE_BUFFER_LEN (4096)

////////////////////////////////////////////////////////////////////////////////////////////////
Value *createBoundsFromBndFile_cf(Value** arg_list, int count)
{
	check_arg_count( CreateBoundsFromBndFile, 3, count );
	type_check( arg_list[0], String, "CreateBoundsFromBndFile srcfilename objectname materiallist" );
	type_check( arg_list[1], String, "CreateBoundsFromBndFile srcfilename objectname materiallist" );
	type_check( arg_list[2], Array, "CreateBoundsFromBndFile srcfilename objectname materiallist" );

	const char* p_Filename = arg_list[0]->to_string();
	char p_ObjectName[1024];
	strcpy( p_ObjectName, arg_list[1]->to_string() );

	Array* p_RetList = (Array*)arg_list[2];

	FILE* pBndFile;
	pBndFile = fopen( p_Filename, "r" );
	if ( !pBndFile )
		return ( &undefined );

	char LineBuffer[LINE_BUFFER_LEN];

	enum
	{
		READ_VERTS,
		READ_MATERIALS,
		READ_TRIS
	};

	int CurrentState = READ_VERTS;
	float fX,fY,fZ;
	int iA,iB,iC,iD,iMat;
	int i,NumFields;

	std::list<BndVert> Verts;
	std::list<BndTri> Tris;
	std::list<std::string> Mats;

	bool ReadOne = false;
	int MatCount = 0;

	while ( !feof( pBndFile ) )
	{
		fgets( LineBuffer, LINE_BUFFER_LEN, pBndFile );
		if ( 0 == _stricmp( "", LineBuffer ) )
			continue;

		switch(CurrentState)
		{
		case READ_VERTS:
			{
				NumFields = sscanf(LineBuffer,"v %f %f %f",&fX,&fY,&fZ);

				if(NumFields != 3)
				{
					if(ReadOne)
					{
						MatCount--;
						Mats.push_back(LineBuffer);
						ReadOne = false;
						CurrentState = READ_MATERIALS;
						continue;
					}
					else
					{
						sscanf(LineBuffer,"materials: %d",&MatCount);
					}
				}
				else
				{
					BndVert Vert;

					Vert.fX = fX;
					Vert.fY = fY;
					Vert.fZ = fZ;

					Verts.push_back(Vert);

					ReadOne = true;
				}
			}

			break;
		case READ_MATERIALS:
			{
				if(MatCount == 0)
				{
					ReadOne = false;
					CurrentState = READ_TRIS;
				}
				else
				{
					MatCount--;
					Mats.push_back(LineBuffer);
				}
			}

			break;
		case READ_TRIS:
			{
				NumFields = sscanf(LineBuffer,"tri %d %d %d %d",&iA,&iB,&iC,&iMat);

				if(NumFields != 4)
					NumFields = sscanf(LineBuffer,"quad %d %d %d %d %d",&iA,&iB,&iC,&iD,&iMat);

				if((NumFields != 4) && (NumFields != 5))
				{
					if(ReadOne)
					{
						ReadOne = false;
						CurrentState = READ_MATERIALS;
						continue;
					}
				}
				else
				{
					BndTri Tri;

					if(NumFields == 4)
					{
						Tri.iA = iA;
						Tri.iB = iB;
						Tri.iC = iC;
						Tri.iMat = iMat;

						Tris.push_back(Tri);
					}
					else
					{
						Tri.iA = iA;
						Tri.iB = iB;
						Tri.iC = iC;
						Tri.iMat = iMat;

						Tris.push_back(Tri);

						Tri.iA = iA;
						Tri.iB = iC;
						Tri.iC = iD;
						Tri.iMat = iMat;

						Tris.push_back(Tri);
					}

					ReadOne = true;
				}
			}

			break;
		}
	}
	fclose( pBndFile );

	//

	Interface* p_I = GetCOREInterface();
	TriObject* p_TriObject = CreateNewTriObject();

	//fill in the mesh info

	Mesh& r_mesh = p_TriObject->mesh;

	r_mesh.setNumFaces( (int) Tris.size());
	r_mesh.setNumVerts( (int) Verts.size());
	r_mesh.InvalidateTopologyCache();

	i = 0;
	std::list<BndVert>::iterator vBegin = Verts.begin();
	std::list<BndVert>::iterator vEnd = Verts.end();

	while(vBegin != vEnd)
	{
		r_mesh.setVert(i,vBegin->fX,vBegin->fY,vBegin->fZ);

		i++;
		vBegin++;
	}

	i = 0;
	std::list<BndTri>::iterator tBegin = Tris.begin();
	std::list<BndTri>::iterator tEnd = Tris.end();

	while(tBegin != tEnd)
	{
		r_mesh.faces[i].v[0] = tBegin->iA;
		r_mesh.faces[i].v[1] = tBegin->iB;
		r_mesh.faces[i].v[2] = tBegin->iC;
		r_mesh.faces[i].flags = EDGE_ALL;

		i++;
		tBegin++;
	}

	r_mesh.setNumTVerts(0);
	r_mesh.setNumTVFaces(0);
	r_mesh.buildNormals();
	r_mesh.DeleteFlaggedFaces();
	r_mesh.DeleteIsoVerts();
	r_mesh.InvalidateGeomCache();
	r_mesh.BuildStripsAndEdges();
	r_mesh.buildBoundingBox();

	//

	INode* p_Node = p_I->CreateObjectNode(p_TriObject);
	p_Node->SetName(p_ObjectName);

	std::list<std::string>::iterator begin = Mats.begin();
	std::list<std::string>::iterator end = Mats.end();

	while(begin != end)
	{
		p_RetList->append(new String((char*)begin->c_str()));

		begin++;
	}

	return new MAXNode(p_Node);
}
