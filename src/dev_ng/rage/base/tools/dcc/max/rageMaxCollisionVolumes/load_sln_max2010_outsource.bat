SET MAX_DEPLOY=%RS_TOOLSROOT%\dcc\outsource\max2010

SET VISUAL_STUDIO="C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe"
SET VISUAL_STUDIO_X86="C:\Program Files (x86)\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe"
SET SOLUTION="%CD%\rageMaxCollisionVolumes_2008.sln"
  
IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 
       start "" %VISUAL_STUDIO_X86% %SOLUTION%
) ELSE (
       start "" %VISUAL_STUDIO% %SOLUTION%
)