/*******************************************************************************
 *
 *    Filename : ICollision.h
 *     Creator : Barnaby.
 *     $Author : $
 *       $Date : $
 *   $Revision : $
 * Description : 
 *
 *******************************************************************************/

#ifndef INC_ICOLLISION_H_
#define INC_ICOLLISION_H_

/*********************************************************************************/

//The three class IDs 
#define COLLISION_BOX_CLASS_ID      Class_ID(0x7fca381e, 0x47a93bdf)
#define COLLISION_SPHERE_CLASS_ID   Class_ID(0x9e12205, 0x62dc4b42)
#define COLLISION_PLANE_CLASS_ID    Class_ID(0x22327812, 0x696e2c57)
#define COLLISION_MESH_CLASS_ID		Class_ID(0x5d2707b2, 0xb5c5d30)
#define COLLISION_MESHSHAD_CLASS_ID	Class_ID(0x1d7b732d, 0x3ad952fc)
#define COLLISION_MESHHELPER_CLASS_ID		Class_ID(0x32553e9a, 0x184362cf)
#define COLLISION_CAPSULE_CLASS_ID	Class_ID(0x5745733c, 0x7f435158)
#define COLLISION_CYLINDER_CLASS_ID	Class_ID(0x695d2eb9, 0x77fb6428)
#define CULL_SPHERE_CLASS_ID		Class_ID(0x770447c0, 0x89f79fe)
#define COLLISION_DISC_CLASS_ID		Class_ID(0x1c9774c7, 0xa34be14)

#define PBLOCK_REF_NUM 0

/*********************************************************************************/

//PLANES

//Planes have a length, width and an orientation.  

//The length and width are from the *CENTRE* of the plane to the edge, *NOT* from
//edge to edge. 

// Paramblock2 name
enum { colplane_params}; 
// Paramblock2 parameter list
enum { 
	colplane_length, 
	colplane_width,
	colplane_orien
};

// orientations of plane
enum {
	colplane_xy,
	colplane_xz,
	colplane_yz
};

//For xy plane, x = length, y = width.
//For xz plane, x = width,  z = length.
//For yz plane, y = length, z = width.

/*********************************************************************************/

//SPHERES

//Spheres have two values - a radius and a type.

// Paramblock2 name
enum { colsphere_params}; 
// Paramblock2 parameter list
enum { 
	colsphere_radius, 
	colsphere_type 
};

//Types of sphere:
enum {
	colsphere_normal,
	colsphere_simple,
	colsphere_hierarchical
};

/*********************************************************************************/

//CAPSULES

//Spheres have two values - a radius and a type.

// Paramblock2 name
enum { colcapsule_params}; 
// Paramblock2 parameter list
enum { 
	colcapsule_radius, 
	colcapsule_length 
};

/*********************************************************************************/

//CYLINDERS

// Paramblock2 name
enum 
{ 
	colcylinder_params
}; 

// Paramblock2 parameter list
enum 
{ 
	colcylinder_radius, 
	colcylinder_length,
	colcylinder_capcurve,
	colcylinder_sidecurve
};

/*********************************************************************************/

//BOXES

//Boxes have length, width and height.

//The length, width and height are from the *CENTRE* of the plane to the edge, 
//*NOT* from edge to edge. 

// Paramblock2 name
enum { colbox_params}; 
// Paramblock2 parameter list
enum { 
	colbox_length, 
	colbox_width,
	colbox_height
};


/*********************************************************************************/

// CULLING SPHERES

//Spheres have one value - a radius 

// Paramblock2 name
enum { cullsphere_params}; 
// Paramblock2 parameter list
enum { 
	cullsphere_radius, 
};


/*********************************************************************************/

// COLLISION MESH

// Collision mesh has one reference. They reference a TriObject for their Mesh

// reference indices
#define COLMESH_REF_TRIOBJ			0

enum { colmesh_params };

/*********************************************************************************/

//DISCS

// Paramblock2 name
enum 
{ 
	coldisc_params
}; 

// Paramblock2 parameter list
enum 
{ 
	coldisc_radius, 
	coldisc_length
};

/*********************************************************************************/

#endif
