// 
// rexGeneric/objectLocator.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectGenericLocator -- class with data suitable for describing a locator
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject (REX datBase Library)
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_LOCATOR_H__
#define __REX_GENERIC_LOCATOR_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexGeneric/objectEntity.h"
#include "vector/matrix34.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObjectGenericLocator : public rexObjectGenericEntityComponent
{
public:
	rexObjectGenericLocator() : rexObjectGenericEntityComponent() { m_BoneIndex = 0; m_GroupIndex = -1; m_Matrix.Identity(); }
	
	virtual rexObject* CreateNew()   { return new rexObjectGenericLocator; }

	int						m_BoneIndex, m_GroupIndex;
	Matrix34				m_Matrix;

	atArray<atString>		m_AttributeNames;
	atArray<rexValue>		m_AttributeValues;
};

class rexObjectGenericLocatorGroup : public rexObject
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericLocatorGroup; }
	virtual const char* GetEntityGroupTag() const { return "locator"; }
};

/////////////////////////////////////////////////////////////////////////////////////
//Specialized locator group types...

class rexObjectGenericSoundLocatorGroup : public rexObjectGenericLocatorGroup
{
public:
	virtual rexObject* CreateNew()	 { return new rexObjectGenericSoundLocatorGroup; }
	virtual const char* GetEntityGroupTag() const { return "soundlocator"; }
};

class rexObjectGenericCharacterInstanceLocatorGroup : public rexObjectGenericLocatorGroup
{
public:
	virtual rexObject* CreateNew()	{ return new rexObjectGenericCharacterInstanceLocatorGroup; }
	virtual const char* GetEntityGroupTag() const { return "characterinstancelocator"; }
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
