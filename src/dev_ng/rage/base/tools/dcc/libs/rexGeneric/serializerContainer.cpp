#include "rexBase/object.h"
#include "rexGeneric/serializerContainer.h"

#include "file/stream.h"

///////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

bool rexSerializerGenericContainer::RegisterObjectType( rexObject& objType, rexSerializerGenericContainerCallback callback )
{	
    int idx = findType(objType);
    if (idx>=0)
    {
        delete m_ObjectTypes[idx];
        m_ObjectTypes[idx] = objType.CreateNew();
        m_Callbacks[idx]= callback;
    }
    else
    {
        m_ObjectTypes.PushAndGrow( objType.CreateNew() );
        m_Callbacks.PushAndGrow( callback );
    }
	return true;
}

rexResult rexSerializerGenericContainer::WriteObjectInformation( rexObject& object, fiStream* s ) const
{
    int idx = findType(object);

    if (idx>=0)
    {
		if( m_Callbacks[ idx ] )
		{
			return m_Callbacks[ idx ]( object, s );
		}
		else  // if no callback, assume it's registered to pass to child objects
		{
			rexResult result = rexResult::PERFECT;

			int count = object.m_ContainedObjects.GetCount();
			for( int b = 0; b < count; b++ )
			{
				result.Combine(WriteObjectInformation( *object.m_ContainedObjects[ b ], s ));
				if(result.Errors())
				{
					//Early out
					return result;
				}
			}

			return result;
		}
	}

	return rexResult::ERRORS;
}

int rexSerializerGenericContainer::findType(rexObject& object) const
{
    int objTypeCount = m_ObjectTypes.GetCount();
    int idx=0;
    while ((idx<objTypeCount) && (!object.IsSameType( *m_ObjectTypes[ idx ] )) )
    {
        idx++;
    }
    if (idx<objTypeCount)
    {
        return idx;
    }
    else
    {
        return -1;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////

rexSerializerGenericContainer::~rexSerializerGenericContainer()
{
	while( m_ObjectTypes.GetCount() )
		delete m_ObjectTypes.Pop();
}

} // namespace rage
