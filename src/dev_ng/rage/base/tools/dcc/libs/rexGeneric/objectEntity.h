// 
// rexGeneric/objectEntity.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectGenericEntity -- class with data suitable for describing an entity
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_ENTITY_H__
#define __REX_GENERIC_ENTITY_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/object.h"
#include "atl/array.h"
#include "atl/string.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObjectGenericEntityComponent : public rexObject
{
public:
	virtual	void SetName( const char * name )				{ m_Name = name; }
	const atString& GetName() const							{ return m_Name; }

	virtual	void SetFullPath( const char * fullPath )		{ m_FullPath = fullPath; }
	const atString& GetFullPath() const						{ return m_FullPath; }

protected:
	atString	m_Name;
	atString	m_FullPath;
};

/////////////////////

class rexObjectGenericEntity : public rexObjectGenericEntityComponent
{
public:
	rexObjectGenericEntity() : rexObjectGenericEntityComponent(), m_Referenced(false), m_SkipBoundSectionOfTypeFile(false) 
	{ 
		m_NoOfCurrentlyConstructedInstances++; 
//		Displayf("Created an instance of a rexObjectGenericEntity, currently %d active instances\n", m_NoOfCurrentlyConstructedInstances);
	}
	virtual ~rexObjectGenericEntity()
	{ 
		m_NoOfCurrentlyConstructedInstances--; 
//		Displayf("Deleted an instance of a rexObjectGenericEntity, currently %d active instances\n", m_NoOfCurrentlyConstructedInstances);
	}


	virtual rexObject* CreateNew()   { return new rexObjectGenericEntity; }

	atString	m_EntityTypeFileName;
	bool		m_Referenced;
	bool		m_SkipBoundSectionOfTypeFile;
	atString	m_EntityClass;

private:
	static int	m_NoOfCurrentlyConstructedInstances;
};



} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////


#endif
