// 
// rexGeneric/objectLevel.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Rmworld Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		AGE 
//			atl, core, data, vector
//		REX
//			Base Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectRmworldDistrict 
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_RMWORLD_LEVEL_H__
#define __REX_RMWORLD_LEVEL_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/object.h"
#include "rexGeneric/objectBound.h"
#include "rexGeneric/objectEntity.h"
#include "rexGeneric/objectMesh.h"
#include "atl/array.h"
#include "atl/string.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectRmworldDistrict : public rexObjectGenericEntityComponent
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectRmworldDistrict; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericMeshHierarchyPortals : public rexObjectGenericMeshHierarchy
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericMeshHierarchyPortals; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericMeshHierarchyWater : public rexObjectGenericMeshHierarchy
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericMeshHierarchyWater; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericMeshHierarchyOccluders : public rexObjectGenericMeshHierarchy
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericMeshHierarchyOccluders; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericMeshHierarchyRoomVolumes : public rexObjectGenericMeshHierarchy
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericMeshHierarchyRoomVolumes; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectRoomType : public rexObject
{
public:
	rexObjectRoomType() : rexObject()	{ m_AmbientSoundVolume = 1.0f; m_ReverbType = 4; m_ReverbVolume = 0.4f; m_UserData0 = 0.0f; m_UserData1 = 0.0f; }
	virtual rexObject* CreateNew()		{ return new rexObjectRoomType; }

	atString						m_Name;
	atString						m_AmbientSoundName;
	float							m_AmbientSoundVolume;
	int								m_ReverbType;
	float							m_ReverbVolume;
	float							m_UserData0, m_UserData1;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectRmworldDistrictProxyMeshHierarchy : public rexObjectGenericMeshHierarchy
{
public:
	rexObjectRmworldDistrictProxyMeshHierarchy() : rexObjectGenericMeshHierarchy() { }

	virtual rexObject* CreateNew()   { return new rexObjectRmworldDistrictProxyMeshHierarchy; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectRmworldDistrictNavMeshHierarchy : public rexObjectGenericBoundHierarchy
{
public:
	rexObjectRmworldDistrictNavMeshHierarchy() : rexObjectGenericBoundHierarchy() { }

	virtual rexObject* CreateNew()   { return new rexObjectRmworldDistrictNavMeshHierarchy; }
};

/////////////////////////////////////////////////////////////////////////////////////

}	// namespace rage

#endif
