#include "rexBase/utility.h"
#include "rexGeneric/objectMesh.h"
#include "atl/map.h"
#include "grmodel/grmodel_config.h"
#include "rmcore/lodgroup.h"
#include "rexBase/result.h"

#if USE_MTL_SYSTEM
#include "rageShaderMaterial/rageShaderMaterial.h"
#endif
//#include "math/amath.h"

#include <malloc.h>

namespace rage {

template int	rexObjectGenericMesh::GetVertexPositions( atArray<Vector3>& vertArray, bool worldSpace ) const;
template int	rexObjectGenericMesh::GetVertexPositions( atArray<Vector3, 0, unsigned>& vertArray, bool worldSpace ) const;

const float rexObjectGenericMesh::VertexInfo::WEIGHT_TOLERANCE=0.01f;
int rexObjectGenericMesh::VertexInfo::sm_MaxMatricesPerVertex=0;
int rexObjectGenericMesh::m_NoOfCurrentlyConstructedInstances = 0;

char *sRexMeshBucketMaskOffsets[] =
{
	"MESHBUCKET_RENDER", 
	"MESHBUCKET_SHADOW", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
	"MESHBUCKET_UNUSED", 
};
int GetRexMeshBucketBit(const char *flagString)
{
	for(int k=0;k<16;k++)
		if(0==strcmp(flagString, sRexMeshBucketMaskOffsets[k]))
			return 1 << k;
	return -1;
}


/////////////////////////////////////////////////////////////////////////////////////
// internal helper function forward declarations
/////////////////////////////////////////////////////////////////////////////////////

bool IsConvex(const Vector3 &a, const Vector3 &b, const Vector3 &c, const Vector3 &d);
bool CoLinearByAngle(const Vector3 &a, const Vector3 &b, const Vector3 &c, const float minTheta);
bool CloseEnough(const Vector3 &a, const Vector3 &b, const float range);
void GetTriNormal(const Vector3& a,const Vector3& b,const Vector3& c, Vector3& norm);
bool IsPolyPlanar(const Vector3& v0, const Vector3& v1, const Vector3& v2, const Vector3& v3);


/////////////////////////////////////////////////////////////////////////////////////
// class rexObjectGenericMesh
/////////////////////////////////////////////////////////////////////////////////////

int rexObjectGenericMesh::GetAdjunctCount() const
{
	int adjCount = 0;
	int primCount = m_Primitives.GetCount();
	for( int a = 0; a < primCount; a++ )
	{
		adjCount += m_Primitives[ a ].m_Adjuncts.GetCount();
	}
	return adjCount;
}

/////////////////////////////////////////////

int rexObjectGenericMesh::GetTriCount() const
{
	int triCount = 0;
	int primCount = m_Primitives.GetCount();
	for( int a = 0; a < primCount; a++ )
	{
		triCount += ( m_Primitives[ a ].m_Adjuncts.GetCount() - 2 );
	}
	return triCount;
}

/////////////////////////////////////////////

void rexObjectGenericMesh::PreallocateArrays(int vertexCount,int normalCount,int primCount,int matCount)
{   
	// sort of a yucky way to do this - resize to capacity to ensure a call
	// to grow will actually grow the array, then resize back to original count:
	int originalCount=m_Vertices.GetCount();
	m_Vertices.Resize(m_Vertices.GetCapacity());
	m_Vertices.Grow(vertexCount?vertexCount:16);
	m_Vertices.Resize(originalCount);

	originalCount=m_Normals.GetCount();
	m_Normals.Resize(m_Normals.GetCapacity());
	m_Normals.Grow(normalCount?normalCount:16);
	m_Normals.Resize(originalCount);

	originalCount=m_Primitives.GetCount();
	m_Primitives.Resize(m_Primitives.GetCapacity());
	m_Primitives.Grow(primCount?primCount:16);
	m_Primitives.Resize(originalCount);

	originalCount=m_Materials.GetCount();
	m_Materials.Resize(m_Materials.GetCapacity());
	m_Materials.Grow((u16) (matCount?matCount:16));
	m_Materials.Resize(originalCount);
}

bool rexObjectGenericMesh::Assimilate( const rexObjectGenericMesh& that, bool handleMaterials, bool acrossBones )
{
	int a;
	int vertexCount = that.m_Vertices.GetCount();
	int normalCount = that.m_Normals.GetCount();
	int primCount = that.m_Primitives.GetCount();
	int materialCount = that.m_Materials.GetCount();

	int thisOriginalVertexCount = m_Vertices.GetCount();
	int thisOriginalNormalCount = m_Normals.GetCount();
	int thisOriginalMaterialCount = m_Materials.GetCount();
	int thisOriginalPrimCount = m_Primitives.GetCount();

	atMap<atString, int> matMap;
	if( handleMaterials )
	{
		for( int b = 0; b < thisOriginalMaterialCount; b++ )
		{
			const atString& matName = m_Materials[ b ].m_Name;
			int* matIndex = matMap.Access( matName );
			if( matIndex )
			{
				Assert( *matIndex >= 0 && *matIndex < thisOriginalMaterialCount );			
			}
			else
			{
				matMap.Insert( matName, b );
			}
		}
	}

	int someBone = -1;

	if ( acrossBones )
	{
		if (m_BoneIndex != -1)
		{
			for( a = 0; a < thisOriginalVertexCount; a++ )
			{
				rexObjectGenericMesh::VertexInfo& vertexInfo = m_Vertices[a];
				if (m_Vertices[a].m_BoneWeights.GetCount() == 0)
				{
					vertexInfo.m_BoneIndices.Reset();
					vertexInfo.m_BoneIndices.Resize(m_BoneCount);
					vertexInfo.m_BoneWeights.Reset();
					vertexInfo.m_BoneWeights.Resize(m_BoneCount);

					for (unsigned int b=0;b<m_BoneCount;b++)
					{
						vertexInfo.m_BoneIndices[b]=-1;
						vertexInfo.m_BoneWeights[b]=0.0f;
					}

					vertexInfo.m_BoneIndices[0]=m_BoneIndex;
					vertexInfo.m_BoneWeights[0]=1.0f;
				}

				if (someBone == -1)
				{
					someBone = vertexInfo.m_BoneIndices[0];
				}
				else if (someBone != vertexInfo.m_BoneIndices[0])
				{
					m_IsSkinned = true;
				}
			}
		}
	}

	//If the mesh has been marked as skinned make sure the mesh data is relative to the tagged
	//root, otherwise the mesh will potentially render with an incorrect offset from the skeleton
	//if there is no mesh in the hierarchy at the level of the root node (or a joint that has 
	//the same transform as the root)
	if( m_IsSkinned == true && m_bTaggedRootMatrixIsValid )
	{
		for( int v = 0; v < thisOriginalVertexCount; ++v )
		{
			m_Matrix.Transform( m_Vertices[v].m_Position );
			m_TaggedRootMatrix.UnTransform( m_Vertices[v].m_Position );
		}

		for( int a = 0; a < thisOriginalNormalCount; ++a)
		{
			m_Matrix.Transform3x3( m_Normals[a] );
			m_TaggedRootMatrix.UnTransform3x3( m_Normals[a] );
		}

		m_BoneIndex = 0;
		
		//HACK OF THE HACK... this is being done so objects can be exported as though the origin of the meshes world is at the 
		// tagged root matrix so then object placement in the world can be handled by external systems.
		if (m_bDataRelativeToRaggedRoot)
		{
			m_ParentBoneMatrix = m_TaggedRootMatrix;
		}
		else
		{
			m_ParentBoneMatrix.Identity();
		}		
		m_Matrix = m_TaggedRootMatrix;
	}

	bool convertIntoMySpace = true;
	if (acrossBones)
	{
		// If the other mesh has a smaller bone index, we'll convert into its space instead of vice versa
		if (that.m_BoneIndex != -1 && that.m_BoneIndex < m_BoneIndex)
		{
			convertIntoMySpace = false;

			for ( int v = 0; v < thisOriginalVertexCount; ++v )
			{
				m_Matrix.Transform( m_Vertices[ v ].m_Position );
				that.m_Matrix.UnTransform( m_Vertices[ v ].m_Position );
			}

			for( int a = 0; a < thisOriginalNormalCount; ++a )
			{
				m_Matrix.Transform3x3( m_Normals[ a ] );
				that.m_Matrix.UnTransform3x3( m_Normals[ a ] );
			}

			m_BoneIndex = that.m_BoneIndex;
			m_ParentBoneMatrix = that.m_ParentBoneMatrix;
			m_Matrix = that.m_Matrix;
		}
	}

	for( a = 0; a < vertexCount; a++ )
	{
		rexObjectGenericMesh::VertexInfo newVertexInfo;
		if (convertIntoMySpace)
		{
			that.m_Matrix.Transform( that.m_Vertices[ a ].m_Position, newVertexInfo.m_Position );
			m_Matrix.UnTransform( newVertexInfo.m_Position );
		}
		else
		{
			newVertexInfo.m_Position = that.m_Vertices[ a ].m_Position;
		}

		if ( acrossBones )
		{
			if (that.m_BoneIndex != -1)
			{
				newVertexInfo.m_BoneIndices.Reset();
				newVertexInfo.m_BoneIndices.Resize(m_BoneCount);
				newVertexInfo.m_BoneWeights.Reset();
				newVertexInfo.m_BoneWeights.Resize(m_BoneCount);

				if (that.m_Vertices[ a ].m_BoneIndices.GetCount() == 0)
				{
					for (unsigned int b=0;b<m_BoneCount;b++)
					{
						newVertexInfo.m_BoneIndices[b]=-1;
						newVertexInfo.m_BoneWeights[b]=0.0f;
					}

					newVertexInfo.m_BoneIndices[0]=that.m_BoneIndex;
					newVertexInfo.m_BoneWeights[0]=1.0f;
				}
				else
				{
					for (unsigned int b=0;b<m_BoneCount;b++)
					{
						newVertexInfo.m_BoneIndices[b]=that.m_Vertices[a].m_BoneIndices[b];
						newVertexInfo.m_BoneWeights[b]=that.m_Vertices[a].m_BoneWeights[b];
					}
				}
			}

			if (someBone == -1)
			{
				someBone = newVertexInfo.m_BoneIndices[0];
			}
			else if (someBone != newVertexInfo.m_BoneIndices[0])
			{
				m_IsSkinned = true;
			}

			if (someBone != -1 && m_SkinBoneMatrices.GetCount() == 0)
			{
				m_SkinBoneMatrices.Reset();
				m_SkinBoneMatrices.Resize(m_BoneCount);

				// initialize all matrices to identity:
				for( unsigned int a = 0; a < m_BoneCount; a++ )			
					m_SkinBoneMatrices[a].Identity();
			}
		}

		//Copy the blind data associated with this vertex across to the new vertex
		for(int i=0; i<that.m_Vertices[ a ].m_BlindData.GetCount(); i++)
			newVertexInfo.m_BlindData.PushAndGrow(that.m_Vertices[a].m_BlindData[i]);

		m_Vertices.Grow(vertexCount)=newVertexInfo;
	}
	for( a = 0; a < normalCount; a++ )
	{
		Vector3 newNormal;

		if (convertIntoMySpace)
		{
			that.m_Matrix.Transform3x3( that.m_Normals[ a ], newNormal );
			m_Matrix.UnTransform3x3( newNormal );
			m_Normals.Grow(normalCount)=newNormal;
		}
		else
		{
			m_Normals.Grow(normalCount)=that.m_Normals[ a ];
		}
	}
	for( a = 0; a < materialCount; a++ )
	{
		if( handleMaterials )
		{
			const atString& matName = that.m_Materials[ a ].m_Name;
			int* matIndex = matMap.Access( matName );
			if( matIndex )
			{
				Assert( *matIndex >= 0 && *matIndex < m_Materials.GetCount() );			
				MaterialInfo& thisMat = m_Materials[ *matIndex ];
				const MaterialInfo& mat = that.m_Materials[ a ];

				int matPrimCount = mat.m_PrimitiveIndices.GetCount();
				for( int b = 0; b < matPrimCount; b++ )
				{
					thisMat.m_PrimitiveIndices.Grow(matPrimCount)=mat.m_PrimitiveIndices[ b ] + thisOriginalPrimCount;
				}            			
			}
			else
			{
				matMap.Insert( matName, m_Materials.GetCount() );
				MaterialInfo& mat = m_Materials.Grow((u16) materialCount);
				mat = that.m_Materials[ a ];
				int matPrimCount = mat.m_PrimitiveIndices.GetCount();
				for( int b = 0; b < matPrimCount; b++ )
				{
					mat.m_PrimitiveIndices[ b ] += thisOriginalPrimCount;
				}            			
			}		
		}
		else
		{
			MaterialInfo& mat = m_Materials.Grow((u16) materialCount);
			mat = that.m_Materials[ a ];
			int matPrimCount = mat.m_PrimitiveIndices.GetCount();
			for( int b = 0; b < matPrimCount; b++ )
			{
				mat.m_PrimitiveIndices[ b ] += thisOriginalPrimCount;
			}
		}
	}
	for( a = 0; a < primCount; a++ )
	{
		PrimitiveInfo& prim = m_Primitives.Grow(primCount);
		prim = that.m_Primitives[ a ];

		Vector3 newFaceNormal;
		if (convertIntoMySpace)
		{
			that.m_Matrix.Transform3x3( that.m_Primitives[ a ].m_FaceNormal, newFaceNormal );
			m_Matrix.UnTransform3x3( newFaceNormal );
			prim.m_FaceNormal = newFaceNormal;
		}
		
		if( prim.m_MaterialIndex >= 0 )
		{
			if( handleMaterials )
			{
				int* matIndex = matMap.Access( that.m_Materials[ prim.m_MaterialIndex ].m_Name );
				Assert( matIndex );
				Assert( *matIndex >= 0 && *matIndex < m_Materials.GetCount() );
				prim.m_MaterialIndex = *matIndex;
			}
			else
			{
				prim.m_MaterialIndex += thisOriginalMaterialCount;
			}
		}

		int adjCount = prim.m_Adjuncts.GetCount();
		for( int b = 0; b < adjCount; b++ )
		{
			AdjunctInfo& adj = prim.m_Adjuncts[ b ];
			adj.m_NormalIndex += thisOriginalNormalCount;
			adj.m_VertexIndex += thisOriginalVertexCount;
		}		
	}
	if( that.m_LODThreshold > m_LODThreshold )
	{
		m_LODThreshold = that.m_LODThreshold;
		m_LODOUTThreshold = that.m_LODOUTThreshold;
		m_LODGroupIndex = that.m_LODGroupIndex;
	}
	if( false==that.m_optimize)
		m_optimize = false;
	return true;
}

/////////////////////////////////////////////


// static
rexResult rexObjectGenericMesh::ReadVertexInfo(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(rogmVertexInfo);

	int i = 0;
	int subItemsRead = 0, subItemsToRead = 0;
	void *itemArray = NULL;
	rexObjectGenericMesh::VertexInfo *viBuf = static_cast<rexObjectGenericMesh::VertexInfo*>(items);

	for(i = 0; i < (int)numItemsToRead; i++)
	{
		subItemsRead = 0;
		if( (status = ReadVector3(obj, subItemsRead, 1, &(viBuf[i].m_Position))).Errors() )
			return status;
		if(1 != subItemsRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, roatInt, ReadInt, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(viBuf[i].m_BoneIndices, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsRead, subItemsToRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if(subItemsRead != subItemsToRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, roatFloat, ReadFloat, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(viBuf[i].m_BoneWeights, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsRead, subItemsToRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if(subItemsRead != subItemsToRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		/*
		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, roatFloat, ReadFloat, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(viBuf[i].m_FloatBlindData, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsToRead, subItemsRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		*/
	}

	itemsRead = i;

	// NOTE: not using FINISH_IMPL_READ macro
	return obj->FinishReadChunk();
}

// static
rexResult rexObjectGenericMesh::WriteVertexInfo(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(rogmVertexInfo);

	int i = 0;
	const rexObjectGenericMesh::VertexInfo *viBuf = static_cast<const rexObjectGenericMesh::VertexInfo*>(items);

	for(i = 0; i < numItems; i++)
	{
		if( (status = WriteVector3(obj, 1, &viBuf[i].m_Position)).Errors() )
			return status;

		if( (status = StartWriteATArray(obj, roatInt, WriteInt, viBuf[i].m_BoneIndices.GetCount())).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, viBuf[i].m_BoneIndices.GetCount(), GET_ATARRAY_REF_ADDR(viBuf[i].m_BoneIndices))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}

		if( (status = StartWriteATArray(obj, roatFloat, WriteFloat, viBuf[i].m_BoneWeights.GetCount())).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, viBuf[i].m_BoneWeights.GetCount(), GET_ATARRAY_REF_ADDR(viBuf[i].m_BoneWeights))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}

		/*
		if( (status = StartWriteATArray(obj, roatFloat, WriteFloat, viBuf[i].m_FloatBlindData.GetCount())).Errors())
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, viBuf[i].m_FloatBlindData.GetCount(), GET_ATARRAY_REF_ADDR(viBuf[i].m_FloatBlindData))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		*/
	}

	// NOTE: not using FINISH_IMPL_WRITE macro
	curChunk->size += totalBytesWritten;
	return obj->FinishWriteChunk();
}

// static
rexResult rexObjectGenericMesh::ReadAdjunctInfo(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(rogmAdjunctInfo);

	int i = 0;
	int subItemsRead = 0, subItemsToRead = 0;
	void *itemArray = NULL;
	rexObjectGenericMesh::AdjunctInfo *aiBuf = static_cast<rexObjectGenericMesh::AdjunctInfo*>(items);

	for(i = 0; i < (int)numItemsToRead; i++)
	{
		subItemsRead = 0;
		if( (status = ReadInt(obj, subItemsRead, 2, &(aiBuf[i].m_VertexIndex))).Errors() )
			return status;
		if(2 != subItemsRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, roatVector4, ReadVector4, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(aiBuf[i].m_Colors, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsRead, subItemsToRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if(subItemsRead != subItemsToRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, roatVector2, ReadVector2, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(aiBuf[i].m_TextureCoordinates, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsRead, subItemsToRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if(subItemsRead != subItemsToRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
	}

	itemsRead = i;

	// NOTE: not using FINISH_IMPL_READ macro
	return obj->FinishReadChunk();
}

// static
rexResult rexObjectGenericMesh::WriteAdjunctInfo(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(rogmAdjunctInfo);

	int i = 0;
	const rexObjectGenericMesh::AdjunctInfo *aiBuf = static_cast<const rexObjectGenericMesh::AdjunctInfo*>(items);

	for(i = 0; i < numItems; i++)
	{
		if( (status = WriteInt(obj, 2, &aiBuf[i].m_VertexIndex)).Errors() )
			return status;

		if( (status = StartWriteATArray(obj, roatVector4, WriteVector4, aiBuf[i].m_Colors.GetCount())).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, aiBuf[i].m_Colors.GetCount(), GET_ATARRAY_REF_ADDR(aiBuf[i].m_Colors))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		
		if( (status = StartWriteATArray(obj, roatVector2, WriteVector2, aiBuf[i].m_TextureCoordinates.GetCount())).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, aiBuf[i].m_TextureCoordinates.GetCount(), GET_ATARRAY_REF_ADDR(aiBuf[i].m_TextureCoordinates))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
	}

	// NOTE: not using FINISH_IMPL_WRITE macro
	curChunk->size += totalBytesWritten;
	return obj->FinishWriteChunk();
}

// static
rexResult rexObjectGenericMesh::ReadPrimitiveInfo(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(rogmPrimitiveInfo);

	int i = 0;
	int subItemsRead = 0, subItemsToRead = 0;
	void *itemArray = NULL;
	rexObjectGenericMesh::PrimitiveInfo *piBuf = static_cast<rexObjectGenericMesh::PrimitiveInfo*>(items);

	for(i = 0; i < (int)numItemsToRead; i++)
	{
		subItemsRead = 0;
		if( (status = ReadInt(obj, subItemsRead, 1, &(piBuf[i].m_MaterialIndex))).Errors() )
			return status;
		if(1 != subItemsRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, rogmAdjunctInfo, ReadAdjunctInfo, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(piBuf[i].m_Adjuncts, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsRead, subItemsToRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if(subItemsRead != subItemsToRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
	}

	itemsRead = i;

	// NOTE: not using FINISH_IMPL_READ macro
	return obj->FinishReadChunk();
}

// static
rexResult rexObjectGenericMesh::WritePrimitiveInfo(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(rogmPrimitiveInfo);

	int i = 0;
	const rexObjectGenericMesh::PrimitiveInfo *piBuf = static_cast<const rexObjectGenericMesh::PrimitiveInfo*>(items);

	for(i = 0; i < numItems; i++)
	{
		if( (status = WriteInt(obj, 1, &piBuf[i].m_MaterialIndex)).Errors() )
			return status;

		if( (status = StartWriteATArray(obj, rogmAdjunctInfo, WriteAdjunctInfo, piBuf[i].m_Adjuncts.GetCount())).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, piBuf[i].m_Adjuncts.GetCount(), GET_ATARRAY_REF_ADDR(piBuf[i].m_Adjuncts))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
	}

	// NOTE: not using FINISH_IMPL_WRITE macro
	curChunk->size += totalBytesWritten;
	return obj->FinishWriteChunk();
}

// static
rexResult rexObjectGenericMesh::ReadMaterialInfo(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(rogmMaterialInfo);

	int i = 0;
	int subItemsRead = 0, subItemsToRead = 0;
	void *itemArray = NULL;
	rexObjectGenericMesh::MaterialInfo *miBuf = static_cast<rexObjectGenericMesh::MaterialInfo*>(items);

	for(i = 0; i < (int)numItemsToRead; i++)
	{
		subItemsRead = 0;
		if( (status = ReadATString(obj, subItemsRead, 2, &(miBuf[i].m_Name))).Errors() )
			return status;
		if(2 != subItemsRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, roatATString, ReadATString, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(miBuf[i].m_AttributeNames, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsRead, subItemsToRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if(subItemsRead != subItemsToRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, roatRexValue, ReadRexValue, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(miBuf[i].m_AttributeValues, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsRead, subItemsToRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if(subItemsRead != subItemsToRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, roatATString, ReadATString, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(miBuf[i].m_InputTextureFileNames, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsRead, subItemsToRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if(subItemsRead != subItemsToRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, roatATString, ReadATString, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(miBuf[i].m_OutputTextureFileNames, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsRead, subItemsToRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if(subItemsRead != subItemsToRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

		subItemsRead = 0;
		subItemsToRead = 0;
		itemArray = NULL;
		if( (status = StartReadATArray(obj, roatInt, ReadInt, subItemsToRead)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(miBuf[i].m_PrimitiveIndices, subItemsToRead, itemArray);
		if( (status = ReadATArray(obj, subItemsRead, subItemsToRead, itemArray)).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if(subItemsRead != subItemsToRead)
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
	}

	itemsRead = i;

	// NOTE: not using FINISH_IMPL_READ macro
	return obj->FinishReadChunk();
}

// static
rexResult rexObjectGenericMesh::WriteMaterialInfo(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(rogmMaterialInfo);

	int i = 0;
	const rexObjectGenericMesh::MaterialInfo *miBuf = static_cast<const rexObjectGenericMesh::MaterialInfo*>(items);

	for(i = 0; i < numItems; i++)
	{
		if( (status = WriteATString(obj, 2, &miBuf[i].m_Name)).Errors() )
			return status;

		if( (status = StartWriteATArray(obj, roatATString, WriteATString, miBuf[i].m_AttributeNames.GetCount())).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, miBuf[i].m_AttributeNames.GetCount(), GET_ATARRAY_REF_ADDR(miBuf[i].m_AttributeNames))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}

		if( (status = StartWriteATArray(obj, roatRexValue, WriteRexValue, miBuf[i].m_AttributeValues.GetCount())).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, miBuf[i].m_AttributeValues.GetCount(), GET_ATARRAY_REF_ADDR(miBuf[i].m_AttributeValues))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}

		if( (status = StartWriteATArray(obj, roatATString, WriteATString, miBuf[i].m_InputTextureFileNames.GetCount())).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, miBuf[i].m_InputTextureFileNames.GetCount(), GET_ATARRAY_REF_ADDR(miBuf[i].m_InputTextureFileNames))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}

		if( (status = StartWriteATArray(obj, roatATString, WriteATString, miBuf[i].m_OutputTextureFileNames.GetCount())).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, miBuf[i].m_OutputTextureFileNames.GetCount(), GET_ATARRAY_REF_ADDR(miBuf[i].m_OutputTextureFileNames))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}

		if( (status = StartWriteATArray(obj, roatInt, WriteInt, miBuf[i].m_PrimitiveIndices.GetCount())).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(obj, miBuf[i].m_PrimitiveIndices.GetCount(), GET_ATARRAY_REF_ADDR(miBuf[i].m_PrimitiveIndices))).Errors() )
		{
			obj->CloseTempFile();
			return status;
		}
	}

	// NOTE: not using FINISH_IMPL_WRITE macro
	curChunk->size += totalBytesWritten;
	return obj->FinishWriteChunk();
}

/////////////////////////////////////////////

bool rexObjectGenericMesh::primitiveIsQuad(int i) const
{
	if( i < 0 || i >= m_Primitives.GetCount())
		return false;

	if( m_Primitives[i].m_Adjuncts.GetCount() != 4 )
		return false;

	Vector3 verts[4];

	for(unsigned int v = 0; v < 4; v++ )
		verts[v].Set( m_Vertices[m_Primitives[i].m_Adjuncts[v].m_VertexIndex].m_Position );

	return IsPolyPlanar(verts[0], verts[1], verts[2], verts[3]);
}

/////////////////////////////////////////////

// virtual
rexResult rexObjectGenericMesh::Save(atString &filename)
{
	static const s32 magic = rogmGenericMesh;

	// reuse the current magic stack, if anything's there
	m_MagicStack.Reset();

	if( 0 == filename.GetLength() )
		return rexResult(rexResult::ERRORS, rexResult::CANNOT_CREATE_FILE);

	rexResult status;

	if( (status = CreateTempFile(filename)).Errors() )
		return status;

	if( (status = StartWriteChunk(magic, 1)).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// start saving code

	// atString					m_Name;
	if( (status = WriteATString(this, 1, &m_Name)).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// atArray<VertexInfo>		m_Vertices;
	if( (status = StartWriteATArray(this, rogmVertexInfo, WriteVertexInfo, m_Vertices.GetCount())).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if( (status = WriteATArray(this, m_Vertices.GetCount(), GET_ATARRAY_REF_ADDR(m_Vertices))).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// atArray<Vector3>			m_Normals;
	if( (status = StartWriteATArray(this, roatVector3, WriteVector3, m_Normals.GetCount())).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if( (status = WriteATArray(this, m_Normals.GetCount(), GET_ATARRAY_REF_ADDR(m_Normals))).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// atArray<PrimitiveInfo>	m_Primitives;
	if( (status = StartWriteATArray(this, rogmPrimitiveInfo, WritePrimitiveInfo, m_Primitives.GetCount())).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if( (status = WriteATArray(this, m_Primitives.GetCount(), GET_ATARRAY_REF_ADDR(m_Primitives))).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// atArray<MaterialInfo>	m_Materials;
	if( (status = StartWriteATArray(this, rogmMaterialInfo, WriteMaterialInfo, m_Materials.GetCount())).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if( (status = WriteATArray(this, m_Materials.GetCount(), GET_ATARRAY_REF_ADDR(m_Materials))).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// int						m_BoneIndex, m_LODGroupIndex, m_LODLevel;
	if( (status = WriteInt(this, 3, &m_BoneIndex)).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// Matrix34					m_Matrix;
	if( (status = WriteMatrix34(this, 1, &m_Matrix)).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// Vector3					m_Center;
	if( (status = WriteVector3(this, 1, &m_Center)).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// bool						m_IsSkinned:1, m_IsValid:1;
	bool bbuf[2];
	bbuf[0] = m_IsSkinned;
	bbuf[1] = m_IsValid;
	if( (status = WriteBool(this, 2, &bbuf)).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// atArray<Matrix34>		m_SkinBoneMatrices;
	if( (status = StartWriteATArray(this, roatMatrix34, WriteMatrix34, m_SkinBoneMatrices.GetCount())).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if( (status = WriteATArray(this, m_SkinBoneMatrices.GetCount(), GET_ATARRAY_REF_ADDR(m_SkinBoneMatrices))).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// atArray<atString>			m_InstAttrNames;
	if( (status = StartWriteATArray(this, roatATString, WriteATString, m_InstAttrNames.GetCount())).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if( (status = WriteATArray(this, m_InstAttrNames.GetCount(), GET_ATARRAY_REF_ADDR(m_InstAttrNames))).Errors() )
	{
		CloseTempFile();
		return status;
	}

	// atArray<rexValue>			m_InstAttrValues;
	if( (status = StartWriteATArray(this, roatATString, WriteATString, m_InstAttrValues.GetCount())).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if( (status = WriteATArray(this, m_InstAttrValues.GetCount(), GET_ATARRAY_REF_ADDR(m_InstAttrValues))).Errors() )
	{
		CloseTempFile();
		return status;
	}


	// finish saving code

	if( (status = FinishWriteChunk()).Errors() )
	{
		CloseTempFile();
		return status;
	}

	if( (status = CloseTempFile()).Errors() )
		return status;

	// deallocate magic stack
	m_MagicStack.Reset();

	return rexResult(rexResult::PERFECT);
}

/////////////////////////////////////////////

// virtual
rexResult rexObjectGenericMesh::Load(atString &filename)
{
	static const s32 magic = rogmGenericMesh;

	// reuse the current magic stack, if anything's there
	m_MagicStack.Reset();

	if( 0 == filename.GetLength() )
		return rexResult(rexResult::ERRORS, rexResult::FILE_NOT_FOUND);

	rexResult status;

	if( (status = OpenTempFile(filename)).Errors() )
		return status;

	s32 filemagic = 0;
	u32 sizeToRead = 0, numItems = 0;

	if( (status = StartReadChunk(filemagic, sizeToRead, numItems)).Errors() )
	{
		CloseTempFile();
		return status;
	}

	if(filemagic != magic || numItems != 1)
	{
		CloseTempFile();
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
	}

	// start loading code

	int subItemsRead = 0, subItemsToRead = 0;
	void *itemArray = NULL;

	// atString					m_Name;
	subItemsRead = 0;
	if( (status = ReadATString(this, subItemsRead, 1, &m_Name)).Errors() )
		return status;
	if(1 != subItemsRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	// atArray<VertexInfo>		m_Vertices;
	subItemsRead = 0;
	subItemsToRead = 0;
	itemArray = NULL;
	if( (status = StartReadATArray(this, rogmVertexInfo, ReadVertexInfo, subItemsToRead)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	READ_ATARRAY_REF_HELPER(m_Vertices, subItemsToRead, itemArray);
	if( (status = ReadATArray(this, subItemsRead, subItemsToRead, itemArray)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if(subItemsRead != subItemsToRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	// atArray<Vector3>			m_Normals;
	subItemsRead = 0;
	subItemsToRead = 0;
	itemArray = NULL;
	if( (status = StartReadATArray(this, roatVector3, ReadVector3, subItemsToRead)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	READ_ATARRAY_REF_HELPER(m_Normals, subItemsToRead, itemArray);
	if( (status = ReadATArray(this, subItemsRead, subItemsToRead, itemArray)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if(subItemsRead != subItemsToRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	// atArray<PrimitiveInfo>	m_Primitives;
	subItemsRead = 0;
	subItemsToRead = 0;
	itemArray = NULL;
	if( (status = StartReadATArray(this, rogmPrimitiveInfo, ReadPrimitiveInfo, subItemsToRead)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	READ_ATARRAY_REF_HELPER(m_Primitives, subItemsToRead, itemArray);
	if( (status = ReadATArray(this, subItemsRead, subItemsToRead, itemArray)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if(subItemsRead != subItemsToRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	// atArray<MaterialInfo>	m_Materials;
	subItemsRead = 0;
	subItemsToRead = 0;
	itemArray = NULL;
	if( (status = StartReadATArray(this, rogmMaterialInfo, ReadMaterialInfo, subItemsToRead)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	READ_ATARRAY_REF_HELPER(m_Materials, subItemsToRead, itemArray);
	if( (status = ReadATArray(this, subItemsRead, subItemsToRead, itemArray)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if(subItemsRead != subItemsToRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	// int						m_BoneIndex, m_LODGroupIndex, m_LODLevel;
	subItemsRead = 0;
	if( (status = ReadInt(this, subItemsRead, 3, &m_BoneIndex)).Errors() )
		return status;
	if(3 != subItemsRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	// Matrix34					m_Matrix;
	subItemsRead = 0;
	if( (status = ReadMatrix34(this, subItemsRead, 1, &m_Matrix)).Errors() )
		return status;
	if(1 != subItemsRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	// Vector3					m_Center;
	subItemsRead = 0;
	if( (status = ReadVector3(this, subItemsRead, 1, &m_Center)).Errors() )
		return status;
	if(1 != subItemsRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	// bool						m_IsSkinned:1, m_IsValid:1;
	bool bbuf[2];

	subItemsRead = 0;
	if( (status = ReadBool(this, subItemsRead, 2, &bbuf)).Errors() )
		return status;
	if(2 != subItemsRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	m_IsSkinned = bbuf[0];
	m_IsValid = bbuf[1];

	// atArray<Matrix34>		m_SkinBoneMatrices;
	subItemsRead = 0;
	subItemsToRead = 0;
	itemArray = NULL;
	if( (status = StartReadATArray(this, roatMatrix34, ReadMatrix34, subItemsToRead)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	READ_ATARRAY_REF_HELPER(m_SkinBoneMatrices, subItemsToRead, itemArray);
	if( (status = ReadATArray(this, subItemsRead, subItemsToRead, itemArray)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if(subItemsRead != subItemsToRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	//atArray<atString>			m_InstAttrNames;
	subItemsRead = 0;
	subItemsToRead = 0;
	itemArray = NULL;
	if( (status = StartReadATArray(this, roatATString, ReadATString, subItemsToRead)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	READ_ATARRAY_REF_HELPER(m_InstAttrNames, subItemsToRead, itemArray);
	if( (status = ReadATArray(this, subItemsRead, subItemsToRead, itemArray)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if(subItemsRead != subItemsToRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	//atArray<rexValue>			m_InstAttrValues;
	subItemsRead = 0;
	subItemsToRead = 0;
	itemArray = NULL;
	if( (status = StartReadATArray(this, roatATString, ReadATString, subItemsToRead)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	READ_ATARRAY_REF_HELPER(m_InstAttrValues, subItemsToRead, itemArray);
	if( (status = ReadATArray(this, subItemsRead, subItemsToRead, itemArray)).Errors() )
	{
		CloseTempFile();
		return status;
	}
	if(subItemsRead != subItemsToRead)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	// finish loading code

	if( (status = FinishReadChunk()).Errors() )
	{
		CloseTempFile();
		return status;
	}

	if( (status = CloseTempFile()).Errors() )
		return status;

	// deallocate magic stack
	m_MagicStack.Reset();

	return rexResult(rexResult::PERFECT);
}

/////////////////////////////////////////////////////////////////////////////////////
// class rexObjectGenericMesh::MaterialInfo
/////////////////////////////////////////////////////////////////////////////////////

bool rexObjectGenericMesh::MaterialInfo::IsSame( const rexObjectGenericMesh::MaterialInfo& that, bool 
#if USE_MTL_SYSTEM
												nonOptimizedCheck
#endif
												) const
{
#if USE_MTL_SYSTEM
	// If we are using the material system then the only thing we care about is what the material says
	if (m_Material && that.m_Material)
	{
		if (!m_Material->IsEqual(*that.m_Material))
			return false;
		else
		{
			if (!nonOptimizedCheck)
			{
				//The rageShaderMaterials are equal which means this is the same material
				return true;
			}
		}
	}
#endif

	if( m_MtlFileName != that.m_MtlFileName )
		return false;
	if( m_TypeName != that.m_TypeName )
		return false;
	if( m_IsLayered != that.m_IsLayered )
		return false;
	if( m_AttributeNames.GetCount() != that.m_AttributeNames.GetCount() )
		return false;
	if( m_AttributeValues.GetCount() != that.m_AttributeValues.GetCount() )
		return false;
	if( m_InputTextureFileNames.GetCount() != that.m_InputTextureFileNames.GetCount() )
		return false;
	if( m_OutputTextureFileNames.GetCount() != that.m_OutputTextureFileNames.GetCount() )
		return false;
#if USE_MTL_SYSTEM
	if (m_Material && that.m_Material && !m_Material->IsEqual(*that.m_Material))
		return false;
#endif

	int a, count;

	count = m_AttributeNames.GetCount();
	for( a = 0; a < count; a++ )
	{
		if( m_AttributeNames[ a ] != that.m_AttributeNames[ a ] )
			return false;
	}
	count = m_AttributeValues.GetCount();
	for( a = 0; a < count; a++ )
	{
		if( m_AttributeValues[ a ] != that.m_AttributeValues[ a ] )
			return false;
	}
	count = m_InputTextureFileNames.GetCount();
	for( a = 0; a < count; a++ )
	{
		if( m_InputTextureFileNames[ a ] != that.m_InputTextureFileNames[ a ] )
			return false;
	}
	count = m_OutputTextureFileNames.GetCount();
	for( a = 0; a < count; a++ )
	{
		if( m_OutputTextureFileNames[ a ] != that.m_OutputTextureFileNames[ a ] )
			return false;
	}

	return true;
}

IRageShaderMaterial * rexObjectGenericMesh::MaterialInfo::GetMaterial()
{
#if USE_MTL_SYSTEM
	if (m_Material)
	{
		return m_Material;
	}

	if(m_MtlFileName != "null")
	{
		// I have a material filename, so load it
		m_Material = new rageShaderMaterial;
		if (!m_Material->LoadFromMTL(m_MtlFileName))
		{
			delete m_Material;
			m_Material = NULL;
			return NULL;
		}
	}
#endif
	return m_Material;
}

/////////////////////////////////////////////////////////////////////////////////////
// class rexObjectGenericMeshHierarchy
/////////////////////////////////////////////////////////////////////////////////////

int rexObjectGenericMeshHierarchy::GetMeshCount() const
{
	int count = 0;
	int childCount = m_ContainedObjects.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		rexObjectGenericMesh* mesh = dynamic_cast<rexObjectGenericMesh*>( m_ContainedObjects[ a ] );
		rexObjectGenericMeshHierarchy* hier = dynamic_cast<rexObjectGenericMeshHierarchy*>( m_ContainedObjects[ a ] );
		if( mesh )
			count++;
		else if( hier )
			count += hier->GetMeshCount();
	}
	return count;
}

bool rexObjectGenericMeshHierarchy::Consolidate()
{
	bool retval = true;

#if 0
	int childCount = m_ContainedObjects.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		rexObjectGenericMesh* mesh = dynamic_cast<rexObjectGenericMesh*>( m_ContainedObjects[ a ] );
		rexObjectGenericMeshHierarchy* hier = dynamic_cast<rexObjectGenericMeshHierarchy*>( m_ContainedObjects[ a ] );

		if( mesh )
			retval &= mesh->Consolidate();
		else if( hier )
			retval &= hier->Consolidate();
	}
#endif
	return retval;
}

bool rexObjectGenericMesh::Consolidate()
{
	for( int a = 0; a < ( m_Materials.GetCount() - 1 ) /*this changes, so call the function every time*/; a++ )
	{
		for( int b = a + 1; b < m_Materials.GetCount() /*this changes, so call the function every time*/; b++ )
		{
			if( m_Materials[ a ].IsSame( m_Materials[ b ] ) )
			{
				int bPrimCount = m_Materials[ b ].m_PrimitiveIndices.GetCount();
				for( int c = 0; c < bPrimCount; c++ )
				{
					m_Materials[ a ].m_PrimitiveIndices.Grow(bPrimCount)=m_Materials[ b ].m_PrimitiveIndices[ c ];
					m_Primitives[ m_Materials[ b ].m_PrimitiveIndices[ c ] ].m_MaterialIndex = a;
				}
				m_Materials.Delete( b-- );
				for( int d = b + 1; d < m_Materials.GetCount(); d++ )
				{
					int bPrimCount = m_Materials[ d ].m_PrimitiveIndices.GetCount();
					for( int c = 0; c < bPrimCount; c++ )
					{
						m_Primitives[ m_Materials[ d ].m_PrimitiveIndices[ c ] ].m_MaterialIndex--;
					}
				}
			}
		}
	}
	return true;
}

bool rexObjectGenericMesh::MakeOptimizedBoundingBox( Matrix34& bbMatrix, float& bbLength, float& bbHeight, float& bbWidth, bool worldSpace ) const
{
	atArray< Vector3 > verts;
	GetVertexPositions( verts, worldSpace );
	return rexUtility::MakeOptimizedBoundingBox( verts, bbMatrix, bbLength, bbHeight, bbWidth );
}

bool rexObjectGenericMesh::MakeOptimizedBoundingSphere( Vector3& center, float& radius, bool worldSpace ) const
{
	atArray< Vector3 > verts;
	GetVertexPositions( verts, worldSpace );
	return rexUtility::MakeOptimizedBoundingSphere( verts, center, radius );
}

template<typename T> int rexObjectGenericMesh::GetVertexPositions( T& vertArray, bool worldSpace ) const
{
	int vertCount = m_Vertices.GetCount();
	for( int v = 0; v < vertCount; v++ )
	{		
		if( worldSpace )
			m_Matrix.Transform( m_Vertices[ v ].m_Position, vertArray.Grow((u16) vertCount) );
		else
			vertArray.Grow((u16) vertCount)=m_Vertices[ v ].m_Position;
	}
	return vertCount;
}

void rexObjectGenericMeshHierarchy::GetMaterials( atArray<rexObjectGenericMesh::MaterialInfo>& matInfoArray, bool includeLevelInstanceObjects, bool nonOptimizedMTLCompare) const
{
	atArray< atArray< atArray< rexObjectGenericMesh *> > >					meshesByLODGroupAndLevel;
	atArray< atArray< atArray<rexObjectGenericMesh::sThresholdPair > > >	thresholdsByLODGroupAndLevel;
	atArray< rexObjectGenericMesh* >										nonLODMeshes;

	GetLODsMeshesAndMaterials( meshesByLODGroupAndLevel, thresholdsByLODGroupAndLevel, nonLODMeshes, matInfoArray, includeLevelInstanceObjects, nonOptimizedMTLCompare);
}

// void rexObjectGenericMeshHierarchy::GetLODMeshesAndMaterials( atArray< atArray< atArray< rexObjectGenericMesh* > > >& meshesByLODGroupAndLevel, atArray< atArray< atArray< float > > >& lodThresholds, atArray< rexObjectGenericMesh* >& nonLODMeshes, atArray<rexObjectGenericMesh::MaterialInfo>& matInfoArray, bool includeLevelInstanceObjects, bool nonOptimizedMTLCompare) const
// {
//	atArray< atArray< atArray< rexObjectGenericMesh *> > >	meshesByLODGroupAndLevel;

//	GetLODsMeshesAndMaterials( meshesByLODGroupAndLevel, lodThresholds, nonLODMeshes, matInfoArray, includeLevelInstanceObjects, nonOptimizedMTLCompare);

// 	int meshLODGroupCount = meshesByLODGroupAndLevel.GetCount();
// 	for( int lg = 0; lg < meshLODGroupCount; lg++ )
// 	{
// 		int meshLODLevelCount = meshesByLODGroupAndLevel[ lg ].GetCount();
// 		
// 		while( meshesByLODLevel.GetCount() < meshLODLevelCount )
// 			meshesByLODLevel.Grow((u16) meshLODGroupCount);
// 
// 		for( int l = 0; l < meshLODLevelCount; l++ )
// 		{
// 			int meshCount = meshesByLODGroupAndLevel[ lg ][ l ].GetCount();
// 			for( int m = 0; m < meshCount; m++ )
// 				meshesByLODLevel[ l ].PushAndGrow( meshesByLODGroupAndLevel[ lg ][ l ][ m ], (u16) meshLODLevelCount );
// 		}
// 	}
//}

void rexObjectGenericMeshHierarchy::GetMeshes( atArray< rexObjectGenericMesh* >& meshes, bool includeLevelInstanceObjects ) const
{
	int childCount = m_ContainedObjects.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		rexObjectGenericMeshHierarchy* hier = dynamic_cast<rexObjectGenericMeshHierarchy*>( m_ContainedObjects[ a ] );
		rexObjectGenericMesh *mesh = dynamic_cast<rexObjectGenericMesh*>( m_ContainedObjects[ a ] );
		if( hier )
			hier->GetMeshes( meshes, includeLevelInstanceObjects );
		else if( mesh && ( includeLevelInstanceObjects || !mesh->m_ChildOfLevelInstanceNode ) )
			meshes.PushAndGrow( mesh, (u16) childCount );
	}
}

bool IsAlreadyInLodHierarchy(atArray< atArray< atArray< rexObjectGenericMesh*> > >& meshesByLODGroupAndLevel, rexObjectGenericMesh* pMesh)
{
	for( int a = 0; a < meshesByLODGroupAndLevel.GetCount(); a++ )
	{
		int lodLevels = Min( meshesByLODGroupAndLevel[ a ].GetCount(), MAX_LOD );
		for( int b = 0; b < lodLevels; b++ )
		{
			int meshCount = meshesByLODGroupAndLevel[ a ][ b ].GetCount();
			for( int c = 0; c < meshCount; c++ )
			{
				if(pMesh == meshesByLODGroupAndLevel[ a ][ b ][ c ])
					return true;
			}
		}
	}
	return false;
}

bool rexObjectGenericMesh::GetLODsMeshesAndMaterials( 
	atArray< atArray< atArray< rexObjectGenericMesh*> > >& meshesByLODGroupAndLevel, 
	atArray< atArray< atArray<rexObjectGenericMesh::sThresholdPair > > >& thresholdsByLODGroupAndLevel, 
	atArray<rexObjectGenericMesh*>& nonLODMeshes, 
	atArray<rexObjectGenericMesh::MaterialInfo>& matInfoArray, 
	bool includeLevelInstanceObjects, 
	bool nonOptimizedMTLCompare,
	rexResult *res) const
{
	if( m_Materials.GetCount() && ( includeLevelInstanceObjects || !m_ChildOfLevelInstanceNode ) )
	{
		rexObjectGenericMesh* pNonConstObject = const_cast<rexObjectGenericMesh*>(this);
		if( m_LODGroupIndex < 0 || m_LODLevel < 0 )
		{
			nonLODMeshes.PushAndGrow(pNonConstObject,(u16) LOD_COUNT ); // need to set a flag for combining?
		}
		else
		{
			int lodGroupCount = meshesByLODGroupAndLevel.GetCount();

			if(res && IsAlreadyInLodHierarchy(meshesByLODGroupAndLevel, pNonConstObject))
			{
				res->Combine( rexResult::ERRORS );
				res->AddError("node %s was already added to the tree of rex inputs.", pNonConstObject->GetMeshName().c_str());
			}
			else
			{
				if( m_LODGroupIndex >= lodGroupCount )
				{
					int growCount=Max(1,m_LODGroupIndex);
					for( int a = lodGroupCount; a <= m_LODGroupIndex; a++ )
					{
						atArray< atArray< rexObjectGenericMesh*> >& group = meshesByLODGroupAndLevel.Grow((u16) growCount);					
						for( int b = 0; b < LOD_COUNT; b++ )
							group.Grow(LOD_COUNT);
					}
				}
				if( m_LODLevel >= 0 && m_LODLevel < LOD_COUNT )
					meshesByLODGroupAndLevel[ m_LODGroupIndex ][ m_LODLevel ].PushAndGrow( pNonConstObject ); // need to set a flag for combining?
			}

			int lodGroupCountThresh = thresholdsByLODGroupAndLevel.GetCount();
			if( m_LODGroupIndex >= lodGroupCountThresh )
			{
				int growCount=Max(1,m_LODGroupIndex);
				for( int a = lodGroupCount; a <= m_LODGroupIndex; a++ )
				{
					atArray< atArray<rexObjectGenericMesh::sThresholdPair> >& group = thresholdsByLODGroupAndLevel.Grow((u16) growCount);					
					while(group.size()<LOD_COUNT)
						group.Grow(LOD_COUNT);
				}
			}
			if( m_LODLevel >= 0 && m_LODLevel < LOD_COUNT )
			{
				rexObjectGenericMesh::sThresholdPair threshs(this->m_LODThreshold,this->m_LODOUTThreshold);
				atArray<rexObjectGenericMesh::sThresholdPair> &thresholdArrayPerMesh = thresholdsByLODGroupAndLevel[ m_LODGroupIndex ][ m_LODLevel ];
				if(thresholdArrayPerMesh.GetCount()<=0)
					thresholdArrayPerMesh.Grow(1);

				// only override if new values aren't undefined
				if(threshs.lodIn>=0.0 || threshs.lodOut>=0.0)
					thresholdsByLODGroupAndLevel[ m_LODGroupIndex ][ m_LODLevel ][0] = threshs; // need to set a flag for combining?
			}
		}

		int matCount = m_Materials.GetCount();
		for( int a = 0; a < matCount; a++ )
		{
			// keep duplicates out
			int listCount = matInfoArray.GetCount();
			bool pushItRealGood = true;
			for( int b = 0; b < listCount; b++ )
			{						
				if( matInfoArray[ b ].IsSame( m_Materials[ a ], nonOptimizedMTLCompare))
				{
					pushItRealGood = false;
					break;
				}
			}
			if( pushItRealGood )
				matInfoArray.Grow((u16) matCount) = m_Materials[ a ];
		}
		return true;
	} // if( mesh && m_Materials.GetCount() )
	return false;
}
int rexObjectGenericMeshHierarchy::GetLODsMeshesAndMaterials( 
	atArray< atArray< atArray< rexObjectGenericMesh*> > >& meshesByLODGroupAndLevel, 
	atArray< atArray< atArray<rexObjectGenericMesh::sThresholdPair > > >& thresholdsByLODGroupAndLevel, 
	atArray< rexObjectGenericMesh*>& nonLODMeshes, 
	atArray<rexObjectGenericMesh::MaterialInfo>& matInfoArray, 
	bool includeLevelInstanceObjects, 
	bool nonOptimizedMTLCompare,
	rexResult *res) const
{
	int retval = 0;

// 	if( m_IsLOD )
// 	{
// 		int thresholdGroupCount = m_LODThresholds.GetCount();
// 		for( int iGroup = 0; iGroup < thresholdGroupCount; iGroup++ )
// 		{
// 			const atArray<atArray< float > >& srcThresholds = m_LODThresholds[iGroup];
// 			atArray<atArray< float > >& thresholds = thresholdsByLODGroupAndLevel.Grow();
// 
// 			int thresholdCount = srcThresholds.GetCount();
// 			for( int iLevel = 0; iLevel < thresholdCount; iLevel++ )
// 			{
// 				thresholds.PushAndGrow(srcThresholds[iLevel], 1);
// 			}
// 		}
// 	}

	int childCount = m_ContainedObjects.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		rexObjectGenericMeshHierarchy* hier = dynamic_cast<rexObjectGenericMeshHierarchy*>( m_ContainedObjects[ a ] );
		if( hier )
		{
			retval += hier->GetLODsMeshesAndMaterials( meshesByLODGroupAndLevel, thresholdsByLODGroupAndLevel, nonLODMeshes, matInfoArray, includeLevelInstanceObjects, nonOptimizedMTLCompare, res);
		}
		else
		{
			rexObjectGenericMesh *mesh = dynamic_cast<rexObjectGenericMesh*>( m_ContainedObjects[ a ] );
			if(mesh)
			{
				bool getSucceed = mesh->GetLODsMeshesAndMaterials(meshesByLODGroupAndLevel,thresholdsByLODGroupAndLevel, nonLODMeshes,matInfoArray,includeLevelInstanceObjects,nonOptimizedMTLCompare, res);
				if(getSucceed)
					retval++;
			}
		}
		// adapter->Unload here
	}

	return retval;
}

void rexObjectGenericMeshHierarchy::GetLODGroupDepths( atArray< int >& lodGroupDepths ) const
{
//	if( m_IsLOD )
		lodGroupDepths.PushAndGrow( m_LODDepth );

	int childCount = m_ContainedObjects.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		rexObjectGenericMeshHierarchy* hier = dynamic_cast<rexObjectGenericMeshHierarchy*>( m_ContainedObjects[ a ] );
		if( hier )
			hier->GetLODGroupDepths( lodGroupDepths );
	}
}

void rexObjectGenericMeshHierarchy::GetLODGroupChildIndices( atArray< int >& lodGroupChildIndices ) const
{
//	if( m_IsLOD )
		lodGroupChildIndices.PushAndGrow( m_ChildIndex );

	int childCount = m_ContainedObjects.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		rexObjectGenericMeshHierarchy* hier = dynamic_cast<rexObjectGenericMeshHierarchy*>( m_ContainedObjects[ a ] );
		if( hier )
			hier->GetLODGroupChildIndices( lodGroupChildIndices );
	}
}

/////////////////////////////////////////////////////////////////////////////////////
// internal helper functions
/////////////////////////////////////////////////////////////////////////////////////

bool IsConvex(const Vector3 &a, const Vector3 &b, const Vector3 &c, const Vector3 &d)
{
	Vector3 cross[4];
	Vector3 AB, BC, CD, DA;

	AB.Subtract(b,a);
	BC.Subtract(c,b);
	CD.Subtract(d,c);

	AB.Normalize();
	BC.Normalize();
	CD.Normalize();

	cross[0].Cross(AB,BC);
	cross[1].Cross(BC,CD);
	if (cross[0].Dot(cross[1]) <= 0)
		return false;

	DA.Subtract(a,d);
	DA.Normalize();
	cross[2].Cross(CD,DA);
	if (cross[1].Dot(cross[2]) <= 0)
		return false;

	cross[3].Cross(DA,AB);
	if (cross[2].Dot(cross[3]) <= 0)
		return false;

	return true;
}

/////////////////////////////////////////////

bool CoLinearByAngle(const Vector3 &a, const Vector3 &b, const Vector3 &c, const float minTheta)
{
	Vector3 AB, BC;
	float dot, theta;

	AB.Subtract(b,a);
	BC.Subtract(c,b);

	AB.Normalize();
	BC.Normalize();

	dot = fabsf(Clamp(AB.Dot(BC),-1.0f,1.0f));
	theta = acosf(dot);

	return (theta < minTheta);
}

/////////////////////////////////////////////

bool CloseEnough(const Vector3 &a, const Vector3 &b, const float range)
{
	return (b-a).Mag2() <= range;
}

/////////////////////////////////////////////

void GetTriNormal(const Vector3& a,const Vector3& b,const Vector3& c, Vector3& norm)
{
	Vector3 AB, BC;
	AB.Subtract(a,b);
	BC.Subtract(c,b);
	AB.Normalize();
	BC.Normalize();
	norm.Cross(AB,BC);
	norm.Normalize();
}

/////////////////////////////////////////////

bool IsPolyPlanar(const Vector3& v0, const Vector3& v1, const Vector3& v2, const Vector3& v3)
{
	Vector3 n1, n2;
	GetTriNormal( v1, v2, v0, n1 );
	GetTriNormal( v2, v3, v0, n2 );
	if ( !CloseEnough(n1, n2, 1e-6f) )
		return false;

	// if we're non-convex then this quad's no good
	if (!IsConvex(v0,v1,v2,v3) )
		return false;

	// three of the points are colinear, which will lead to a world of troubles
	// down the line
	const float minAngle = 0.05f;	// 0.05 radius = 3 degrees
	if (CoLinearByAngle(v0,v1,v2,5e-4f) || CoLinearByAngle(v1,v2,v3,minAngle) ||
		CoLinearByAngle(v2,v3,v0,5e-4f) || CoLinearByAngle(v3,v0,v1,minAngle))
	{
		return false;
	}

	return true;
}

} // namespace rage

////////////////////////////////////////////////////////////////////////////////////////////////////////////////