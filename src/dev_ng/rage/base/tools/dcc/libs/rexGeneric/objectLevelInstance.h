// 
// rexGeneric/objectLevelInstance.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		AGE 
//			atl, core, data, vector
//		REX
//			Base Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectLevelInstance 
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_LEVEL_INSTANCE_H__
#define __REX_LEVEL_INSTANCE_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/object.h"
#include "rexGeneric/objectEntity.h"
#include "rexGeneric/objectMesh.h"
#include "atl/array.h"
#include "atl/string.h"
#include "vector/matrix34.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObjectLevelInstance : public rexObjectGenericEntityComponent
{
public:
	enum SubType
	{
		INVALID = -1,
		BANGER_ROOT_GROUP,
		BANGER_CHILD,
		BANGER_UNDAMAGED_GROUP,
		BANGER_DAMAGED_GROUP,
		BANGER_BREAKABLE_GROUP,
		INSTANCED_OBJECT
	};

	rexObjectLevelInstance( const atString& name, SubType subType ) : rexObjectGenericEntityComponent()  
	{ 
		m_Name = name;
		m_Matrix.Identity(); 
		m_SubType = subType;

		switch( subType )
		{
			case BANGER_ROOT_GROUP:
			{
				m_Data.m_RootGroupData.m_MinMoveForce = 0.0f;
				break;
			}
			case BANGER_CHILD:
			{
				m_Data.m_BangerChildData.m_DamagedMass = 250.0f;
				m_Data.m_BangerChildData.m_DamageHealth = 1000.0f;
				m_Data.m_BangerChildData.m_MinDamageForce = 100.0f;
				m_Data.m_BangerChildData.m_PristineMass = 500.0f;
				break;
			}
			case BANGER_UNDAMAGED_GROUP:
			case BANGER_DAMAGED_GROUP:
			case BANGER_BREAKABLE_GROUP:
			{
				m_Data.m_BangerGroupData.m_MinHitForce = 100.0f;
				m_Data.m_BangerGroupData.m_BreakHealth = 1000.0f;
				m_Data.m_BangerGroupData.m_ForceTransmissionScaleUp = 0.25f;
				m_Data.m_BangerGroupData.m_ForceTransmissionScaleDown = 0.25f;
				m_Data.m_BangerGroupData.m_ShatterBreakChance = 0.0f;
				m_Data.m_BangerGroupData.m_NonPristineMaxFrameTicks = 2000;
				break;
			}
			case INSTANCED_OBJECT:
			{
				break;
			}
			default:
				Assert(0);
				break;
		};
	}
	
	virtual rexObject* CreateNew()   { return new rexObjectLevelInstance( m_Name, m_SubType ); }

	atString	m_Name;	
	Matrix34	m_Matrix;
	SubType		m_SubType;

	atArray<atString>	m_MeshNames;

	union 
	{
		struct 
		{
			float m_MinMoveForce;
		} m_RootGroupData;
		struct 
		{
			float m_MinDamageForce, m_DamageHealth, m_PristineMass, m_DamagedMass;			
		} m_BangerChildData;
		struct 
		{
			float m_MinHitForce, m_BreakHealth, m_ForceTransmissionScaleUp, m_ForceTransmissionScaleDown, m_ShatterBreakChance;			
			int m_NonPristineMaxFrameTicks;
		} m_BangerGroupData;
		struct 
		{
		} m_InstanceObjectData;
	} m_Data;	
};

class rexObjectLevelInstanceGroup : public rexObject
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectLevelInstanceGroup; }
};

}	// namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
