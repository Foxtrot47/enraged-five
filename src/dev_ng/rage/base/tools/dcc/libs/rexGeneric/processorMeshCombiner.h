// 
// rexGeneric/processMeshCombiner.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexProcessorMeshCombiner
//			 -- consolidates mesh hierarchies into fewer meshes
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexProcessor
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_PROCESSOR_MESH_COMBINER_H__
#define __REX_GENERIC_PROCESSOR_MESH_COMBINER_H__

#include "rexBase/processor.h"
#include "rexBase/property.h"
#include <float.h>
/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexProcessorMeshCombiner : public rexProcessor
{
public:
	rexProcessorMeshCombiner() : rexProcessor()
	{
		m_MaxPolyCountToCombine = 0;
		m_MaxPolyCountForCombinedMesh = 0;
		m_MaxDistanceToCombineMesh = -1 /* < 0 = INFINITE*/;
		m_MaxLODThresholdRatio = -1.0f;
		m_BoundMode = false;
		m_PrimitiveBoundCompositeMaxDistance = 0.0f;
		m_PrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio = 0.0f;
		m_MergeMaterials = false;
		m_BoundMode = false;
		m_CombineAcrossBones = false;
		m_ExportPrimitivesInBvh = false;
	}

	virtual rexResult		Process( rexObject* object ) const;
	virtual rexProcessor*	CreateNew() const { rexProcessorMeshCombiner* c = new rexProcessorMeshCombiner; c->m_BoundMode = m_BoundMode; c->m_CombineAcrossBones = m_CombineAcrossBones; return c; }
	
	void SetMaxPolyCountToCombine( int maxPolyCountToCombine )				{ m_MaxPolyCountToCombine = maxPolyCountToCombine; }
	int  GetMaxPolyCountToCombine() const									{ return m_MaxPolyCountToCombine; }

	void SetMaxPolyCountForCombinedMesh( int maxPolyCountForCombinedMesh )	{ m_MaxPolyCountForCombinedMesh = maxPolyCountForCombinedMesh; }
	int  GetMaxPolyCountForCombinedMesh() const								{ return m_MaxPolyCountForCombinedMesh; }

	void SetMaxDistanceToCombineMesh( float maxDist )						{ m_MaxDistanceToCombineMesh = maxDist; }
	float GetMaxDistanceToCombineMesh() const								{ return m_MaxDistanceToCombineMesh; }

	// for instance, if 0.5 used, would allow combination for inter-lod-group meshes where the thresholds were within 50% of each other
	void SetMaxLODThresholdRatio( float f )									{ m_MaxLODThresholdRatio = f; }
	float GetMaxLODThresholdRatio() const									{ return m_MaxLODThresholdRatio; }

	void SetCombineAcrossBones( bool b )									{ m_CombineAcrossBones = b; }
	bool GetCombineAcrossBones() const										{ return m_CombineAcrossBones; }

	void SetMergeMaterials( bool b )										{ m_MergeMaterials = b; }
	bool GetMergeMaterials() const											{ return m_MergeMaterials; }

	// bound mode sets different mesh comparisons and allows for bound prim composition
	void SetBoundMode( bool b )												{ m_BoundMode = b; }
	bool GetBoundMode() const												{ return m_BoundMode; }

	void	SetPrimitiveBoundCompositeMaxDistance( float f )	{ m_PrimitiveBoundCompositeMaxDistance = f; }
	float	GetPrimitiveBoundCompositeMaxDistance()				{ return m_PrimitiveBoundCompositeMaxDistance; }

	void	SetPrimitiveBoundExportPrimitivesInBvh( bool b )	{ m_ExportPrimitivesInBvh = b; }
	bool	GetPrimitiveBoundExportPrimitivesInBvh() const		{ return m_ExportPrimitivesInBvh; }

	void	SetPrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio( float f )	{ m_PrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio = f; }
	float	GetPrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio()			{ return m_PrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio; }

protected:
	int		m_MaxPolyCountToCombine, m_MaxPolyCountForCombinedMesh;
	float	m_MaxDistanceToCombineMesh, m_MaxLODThresholdRatio;
	float	m_PrimitiveBoundCompositeMaxDistance, m_PrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio;
	bool	m_ExportPrimitivesInBvh;
	bool	m_CombineAcrossBones;
	bool	m_MergeMaterials;
	bool	m_BoundMode;
};

///////////////////////////////////////////////////////////////////////////////////////

class rexPropertyMeshCombineMaxPolyCountToCombine : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshCombineMaxPolyCountToCombine; }
};

/////////////////////////////

class rexPropertyMeshCombineMaxPolyCountOfCombinedMesh : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshCombineMaxPolyCountOfCombinedMesh; }
};

/////////////////////////////

class rexPropertyMeshCombineMaxDistance : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshCombineMaxDistance; }
};

/////////////////////////////

class rexPropertyMeshCombineMaxLODThresholdRatio : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshCombineMaxLODThresholdRatio; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundSetPrimitiveBoundCompositeMaxDistance : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundSetPrimitiveBoundCompositeMaxDistance; }
};


/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundSetPrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundSetPrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyMeshCombineAcrossBones : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshCombineAcrossBones; }
};

//////

class rexPropertyMeshCombineMergeMaterials : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshCombineMergeMaterials; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

///////////////////////////////////////////////////////////////////////////////////////

#endif
