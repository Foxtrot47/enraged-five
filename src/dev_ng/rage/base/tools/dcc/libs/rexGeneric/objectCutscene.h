// 
// rexGeneric/objectCutscene.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		
////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_CUTSCENE_H__
#define __REX_GENERIC_CUTSCENE_H__

#include "rexBase/object.h"

#include "atl/array.h"
#include "atl/string.h"


////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	rexObjectCutscene - class with data suitable for describing a cutscene
//
//	INHERITS FROM: rexObject
////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObjectCutscene : public rexObject
{
public:
	rexObjectCutscene();

	virtual rexObject	*CreateNew() { return new rexObjectCutscene; }
	virtual	void		SetName( const char *name )		{ mName = name; }
	const atString		&GetName() const				{ return mName; }

	void				SetBookmarks(const char *s)	{ mBookmarks = s; }
	const atString		&GetBookmarks() const			{ return mBookmarks; }
	void				SetNumBookmarks(int count)	{ mNumBookmarks = count; }
	int					GetNumBookmarks() const		{ return mNumBookmarks; }

protected:
	atString	mName;		// To be used as sub-dir

	// For bookmarks
	atString	mBookmarks;
	int			mNumBookmarks;
};

////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	rexObjectCSCast - class with data suitable for describing a cast.
//
//	INHERITS FROM: rexObject
////////////////////////////////////////////////////////////////////////////////

class rexObjectCSCast : public rexObject
{
public:
	// atArray requires the default constructor.
	rexObjectCSCast();
	rexObjectCSCast(bool ref, const char *category, const char *type, const char *name);

	virtual rexObject	*CreateNew() { return new rexObjectCSCast; }

	bool operator == (const rexObjectCSCast &cast) const;

	// Data field
	bool		mReferenced;
	atString	mCategory;
	atString	mType;
	atString	mName;
};

////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	rexObjectCSData - class with data suitable for describing a CS data.
//
//	INHERITS FROM: rexObject
////////////////////////////////////////////////////////////////////////////////

class rexObjectCSData : public rexObject
{
public:
	// atArray requires the default constructor.
	rexObjectCSData();
	rexObjectCSData(const char *type, const char *filename);

	virtual rexObject	*CreateNew() { return new rexObjectCSData; }

	bool operator == (const rexObjectCSData &data) const;

	// Data field
	atString	mType;
	atString	mFileName;
};

////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	rexObjectCSEvent - class with data suitable for describing a CS event.
//
//	INHERITS FROM: rexObject
////////////////////////////////////////////////////////////////////////////////

class rexObjectCSEvent : public rexObject
{
public:
	rexObjectCSEvent();

	virtual rexObject	*CreateNew() { return new rexObjectCSEvent; }

	virtual	void	SetName(const char *name)			{ mName = name; }
	const atString	&GetName() const					{ return mName; }

	void			SetType(const char *type)			{ mType = type; }
	const atString	&GetType() const					{ return mType; }
	void			SetFPS(float fps)					{ mFPS = fps; }
	float			GetFPS() const						{ return mFPS; }
	void			SetSceneStartFrame(int f)	{ mSceneStartFrame = f; }
	int				GetSceneStartFrame() const	{ return mSceneStartFrame; }
	void			SetStartFrame(int f)				{ mStartFrame = f; }
	int				GetStartFrame() const				{ return mStartFrame; }
	void			SetEndFrame(int f)					{ mEndFrame = f; }
	int				GetEndFrame() const					{ return mEndFrame; }
	void			SetEventString(const char *s)		{ mEventString = s; }
	const atString	&GetEventString() const				{ return mEventString; }
	void			SetEventGenericFloat(float f)		{ mEventFloat = f; }
	float			GetEventGenericFloat() const		{ return mEventFloat; }
	void			SetStreamName(const char *s)		{ mStreamName = s; }
	const atString	&GetStreamName() const				{ return mStreamName; }
	void			SetEmitterName(const char *s)		{ mEmitterName = s; }
	const atString	&GetEmitterName() const				{ return mEmitterName; }
	void			SetEmitterType(const char *s)		{ mEmitterType = s; }
	const atString	&GetEmitterType() const				{ return mEmitterType; }
	void			SetParentActor(const char *s)		{ mParentActor = s; }
	const atString	&GetParentActor() const				{ return mParentActor; }
	void			SetParentBone(const char *s)		{ mParentBone = s; }
	const atString	&GetParentBone() const				{ return mParentBone; }
	void			SetClipNear(float f)				{ mClipNear = f; }
	float			GetClipNear() const					{ return mClipNear; }
	void			SetClipFar(float f)					{ mClipFar = f; }
	float			GetClipFar() const					{ return mClipFar; }
	void			SetOffsetX(float f)					{ mOffsetX = f; }
	float			GetOffsetX() const					{ return mOffsetX; }
	void			SetOffsetY(float f)					{ mOffsetY = f; }
	float			GetOffsetY() const					{ return mOffsetY; }
	void			SetOffsetZ(float f)					{ mOffsetZ = f; }
	float			GetOffsetZ() const					{ return mOffsetZ; }

protected:
	// Data field
	atString	mName;
	atString	mType;
	float		mFPS;
	int			mSceneStartFrame;
	int			mStartFrame;
	int			mEndFrame;

	// For generic event
	atString	mEventString;
	float		mEventFloat;

	// For audio event
	atString	mStreamName;

	// For particle event
	atString	mEmitterName;
	atString	mEmitterType;
	atString	mParentActor;
	atString	mParentBone;
	float		mOffsetX;
	float		mOffsetY;
	float		mOffsetZ;

	// For clip-plane event
	float		mClipNear;
	float		mClipFar;
};

////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	rexObjectCSCamera - class with data suitable for describing a CS camera.
//
//	INHERITS FROM: rexObject
////////////////////////////////////////////////////////////////////////////////

class rexObjectCSCamera : public rexObject
{
public:
			rexObjectCSCamera();
	virtual	~rexObjectCSCamera();

	struct FrameData
	{
		int		mNumChannels;
		float	*mData;
	};

	virtual rexObject	*CreateNew() { return new rexObjectCSCamera; }

	virtual	void	SetName( const atString& name ) { mName = name; }
	const atString	&GetName() const { return mName; }

	void	SetMagic( int magic ) { mMagic = magic; }
	void	SetFlags( int flags ) { mFlags = flags; }
	void	SetNumFrames( int frames ) { mNumFrames = frames; }
	void	SetNumChannels( int channels ) { mNumChannels = channels; }
	void	SetStride( float x, float y, float z ) { mStride[0] = x; mStride[1] = y; mStride[2] = z; }
	void	SetFrameData( int index, const FrameData &frame ) { mFrames[index] = frame; }

	int			GetMagic() const { return mMagic; }
	int			GetFlags() const { return mFlags; }
	int			GetNumFrames() const { return mNumFrames; }
	int			GetNumChannels() const { return mNumChannels; }
	const float	*GetStride() const { return &(mStride[0]); }
	FrameData	*GetFrame(int index) const { return ( index > mNumFrames? NULL : &(mFrames[index]) ); }

	void		Init();
	void		SetFrameData( int index, int channels, const float *data );

	// Data field
protected:
	atString	mName;

	int			mMagic;
	int			mFlags;
	int			mNumFrames;
	int			mNumChannels;
	float		mStride[3];
	FrameData	*mFrames;
};

////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	rexObjectCSLight - class with data suitable for describing a CS light.
//
//	INHERITS FROM: rexObject
////////////////////////////////////////////////////////////////////////////////

class rexObjectCSLight : public rexObject
{
public:
	virtual	void	SetName( const atString& name )	{ mName = name; }
	const atString	&GetName() const				{ return mName; }

	virtual rexObject	*CreateNew() { return new rexObjectCSLight; }

	// Data field
protected:
	atString	mName;
};

} // namespace rage

#endif
