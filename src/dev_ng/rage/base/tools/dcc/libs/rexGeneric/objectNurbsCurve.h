// 
// rexGeneric/objectNurbsCurve.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectGenericNurbsCurve -- class with data suitable for describing a NURBS curve
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject (REX datBase Library)
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_NURBS_CURVE_H__
#define __REX_GENERIC_NURBS_CURVE_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexGeneric/objectEntity.h"
#include "vector/matrix34.h"


/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObjectGenericNurbsCurve : public rexObjectGenericEntityComponent
{
public:
	rexObjectGenericNurbsCurve() : rexObjectGenericEntityComponent() 
	{ 
		m_BoneIndex = 0; 
		m_GroupIndex = -1; 
		m_Degree = 0;
		m_Type = NC_INVALID;
	}
	
	// This must match mayaCurveForm.
	enum Type
	{
		NC_INVALID = -1,
		NC_OPEN,
		NC_CLOSED,
		NC_PERIODIC
	};

	virtual rexObject* CreateNew()   { return new rexObjectGenericNurbsCurve; }

	int			m_BoneIndex, m_GroupIndex;
	int			m_Degree;
	Type		m_Type;
	
	atArray<Vector3> m_ControlVertices;
	atArray<float>	 m_Knots;
};

class rexObjectGenericNurbsCurveGroup : public rexObject
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericNurbsCurveGroup; }

	const atString& GetName() const { return m_Name; }
	void			SetName(const atString& name) { m_Name = name; }

protected:
	atString	m_Name;
};

} // namespace rage


/////////////////////////////////////////////////////////////////////////////////////

#endif
