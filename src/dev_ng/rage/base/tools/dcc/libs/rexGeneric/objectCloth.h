// 
// rexGeneric/objectCloth.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_CLOTH_H__
#define __REX_GENERIC_CLOTH_H__

#include "rexBase/object.h"
#include "rexBase/value.h"
#include "rexBase/objectAdapter.h"
#include "atl/array.h"
#include "atl/string.h"

namespace rage
{

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericClothMeshGroup : public rexObject
{
public:
	virtual rexObject* CreateNew()	 { return new rexObjectGenericClothMeshGroup; }
	virtual const char* GetGroupTag() const { return "cloth"; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericCharClothMesh : public rexObjectGenericClothMeshGroup
{
public:
	virtual rexObject* CreateNew()	{ return new rexObjectGenericCharClothMesh; }
	virtual const char* GetGroupTag() const { return "charcloth"; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericClothMeshInfo : public rexObjectGenericMeshHierarchy
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericClothMeshInfo; }

	atString			m_ClothMeshName;
	atArray<atString>	m_BoneNames;
};

}//End namespace rage

#endif //__REX_GENERIC_CLOTH_H__
