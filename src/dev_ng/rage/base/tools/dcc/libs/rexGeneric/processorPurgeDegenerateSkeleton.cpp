// 
// rexGeneric/processorPurgeDegenerateSkeleton.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
///////////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/module.h"
#include "rexBase/object.h"
#include "rexGeneric/objectSkeleton.h"
#include "processorPurgeDegenerateSkeleton.h"

namespace rage 
{

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexProcessorPurgeDegenerateSkeleton::Process( rexObject* object ) const
{
	if(!m_bPurgeEnabled)
		return rexResult::PERFECT;

	rexObjectGenericSkeleton* skel = dynamic_cast<rexObjectGenericSkeleton*>( object );
	if( skel )
	{
		if(skel->GetBoneCount() == 1)
		{
			//If there is only a single bone in the hierarchy then it is considered 
			//to be degenerate, remove the bone...
			delete skel->m_RootBone;
			skel->m_RootBone = NULL;
		}
		return rexResult::PERFECT;
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyEnablePurgeDegenerateSkeleton::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorPurgeDegenerateSkeleton* processor = dynamic_cast<rexProcessorPurgeDegenerateSkeleton*>( module.m_Processors[ a ] );
		if( processor )
		{
			processor->SetPurgeEnabled( value.toBool( processor->GetPurgeEnabled() ) );
		}
	}

	return rexResult::PERFECT;
}

///////////////////////////////////////////////////////////////////////////////////////////

}; //End namespace rage
