// 
// rexGeneric/objectAnimation.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectGenericAnimation -- class with data suitable for describing an animation
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject (REX datBase Library)
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_ANIMATION_H__
#define __REX_GENERIC_ANIMATION_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "atl/array.h"
#include "rexBase/object.h"
#include "rexGeneric/objectEntity.h"
#include "vector/vector3.h"
#include "vector/matrix44.h"
#include "vector/quaternion.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericAnimation : public rexObject
{
public:
	rexObjectGenericAnimation() : rexObject()
	{
		m_ParentMatrix.Identity();
		m_StartTime = m_EndTime = 0.f;
		m_FramesPerSecond = 30.f;
		m_AuthoredOrientation = true;
		m_MultiMoverMode = false;
		m_GenericChunk = 0;
	}

	virtual ~rexObjectGenericAnimation()
	{
		while( m_RootChunks.GetCount() )
			delete m_RootChunks.Pop();

		if( m_GenericChunk )
			delete m_GenericChunk;
	}

	virtual rexObject* CreateNew()   { return new rexObjectGenericAnimation; }

	enum
	{
		TRACK_UNKNOWN,
		TRACK_TRANSLATE,
		TRACK_ROTATE,
		TRACK_SCALE,
		TRACK_DISTANCE,
		TRACK_INFO,
		TRACK_BLENDSHAPE,
		TRACK_VISEMES,
		TRACK_CAMERA_DOF,
		TRACK_COLOUR,
        TRACK_DIRECTION,
		TRACK_ANIMATED_NORMAL_MAPS,
		TRACK_FACIAL_CONTROL,
		TRACK_FACIAL_TRANSLATION,
		TRACK_FACIAL_ROTATION,
		TRACK_LIGHT_COLOUR,
		TRACK_CAMERA_FOV,
		TRACK_LIGHT_DIRECTION,
		TRACK_CAMERA_DOF_NEAR_IN,
		TRACK_CAMERA_DOF_NEAR_OUT,
		TRACK_CAMERA_DOF_FAR_IN,
		TRACK_CAMERA_DOF_FAR_OUT,
		TRACK_PARTICLE,
		TRACK_VISIBILITY
	};

	struct TrackInfo
	{
		TrackInfo()
		{
			m_IsLocked = false;
			m_StartTime = m_EndTime = 0.f;
		}
		virtual ~TrackInfo() { }

		virtual TrackInfo* Clone(void) const = 0;

		virtual void GetVector3Key(int, Vector3&) const {}
		virtual void SetVector3Key(int, const Vector3&) {}
		virtual void AppendVector3Key(const Vector3&) {}

		virtual void GetQuaternionKey(int, Quaternion&) const {}
		virtual void SetQuaternionKey(int, const Quaternion&) {}
		virtual void AppendQuaternionKey(const Quaternion&) {}

		virtual void GetFloatKey(int, float&) const {}
		virtual void SetFloatKey(int, const float) {}
		virtual void AppendFloatKey(const float) {}

		virtual void GetIntKey(int, s32&) const {}
		virtual void SetIntKey(int, const s32) {}
		virtual void AppendIntKey(const s32) {}

		virtual int GetNumKeys(void) const = 0;
		virtual void ResetKeys(int) = 0;

		int			m_TrackID;
		atString		m_Name;
		atString		m_VarName;
		bool			m_IsLocked;
		float			m_StartTime, m_EndTime;
	};

	struct TrackInfoVector3 : public TrackInfo
	{
		TrackInfo* Clone(void) const
		{
			return new TrackInfoVector3(*this);
		}
		void GetVector3Key(int k, Vector3& v) const
		{
			v = m_Keys[Clamp(k, 0, m_Keys.GetCount()-1)];
		}
		void SetVector3Key(int k, const Vector3& v)
		{
			m_Keys[k] = v;
		}
		void AppendVector3Key(const Vector3& v)
		{
			m_Keys.Append() = v;
		}
		int GetNumKeys(void) const
		{
			return m_Keys.GetCount();
		}
		void ResetKeys(int n)
		{
			m_Keys.Reset();
			m_Keys.Reserve(n);
		}

	private:
		atArray<Vector3> m_Keys;
	};

	struct TrackInfoQuaternion : public TrackInfo
	{
		TrackInfo* Clone(void) const
		{
			return new TrackInfoQuaternion(*this);
		}
		void GetQuaternionKey(int k, Quaternion& q) const
		{
			q = m_Keys[Clamp(k, 0, m_Keys.GetCount()-1)];
		}
		void SetQuaternionKey(int k, const Quaternion& q)
		{
			m_Keys[k] = q;
		}
		void AppendQuaternionKey(const Quaternion& q)
		{
			m_Keys.Append() = q;
		}
		int GetNumKeys(void) const
		{
			return m_Keys.GetCount();
		}
		void ResetKeys(int n)
		{
			m_Keys.Reset();
			m_Keys.Reserve(n);
		}

	private:
		atArray<Quaternion> m_Keys;
	};


	struct TrackInfoFloat : public TrackInfo
	{
		TrackInfo* Clone(void) const
		{
			return new TrackInfoFloat(*this);
		}
		void GetFloatKey(int k, float& f) const
		{
			f = m_Keys[Clamp(k, 0, m_Keys.GetCount()-1)];
		}
		void SetFloatKey(int k, const float f)
		{
			m_Keys[k] = f;
		}
		void AppendFloatKey(const float f)
		{
			m_Keys.Append() = f;
		}
		int GetNumKeys(void) const
		{
			return m_Keys.GetCount();
		}
		void ResetKeys(int n)
		{
			m_Keys.Reset();
			m_Keys.Reserve(n);
		}

	private:
		atArray<float> m_Keys;
	};

	struct TrackInfoInt : public TrackInfo
	{
		TrackInfo* Clone(void) const
		{
			return new TrackInfoInt(*this);
		}
		void GetIntKey(int k, s32& f) const
		{
			f = m_Keys[Clamp(k, 0, m_Keys.GetCount()-1)];
		}
		void SetIntKey(int k, const s32 f)
		{
			m_Keys[k] = f;
		}
		void AppendIntKey(const s32 f)
		{
			m_Keys.Append() = f;
		}
		int GetNumKeys(void) const
		{
			return m_Keys.GetCount();
		}
		void ResetKeys(int n)
		{
			m_Keys.Reset();
			m_Keys.Reserve(n);
		}

	private:
		atArray<s32> m_Keys;
	};

	struct ChunkInfo
	{
		ChunkInfo() 
		{
			m_BoneIndex=0;
			m_BoneID=0;
			m_BoneMatrix.Identity();
			m_InitialExclusiveMatrix.Identity();
			m_InitialTransformationMatrix.Identity();
			m_Scale.Set(1.0f);
			m_IsJoint=false;
			m_Parent=NULL;
			m_bSuppressWarnings=false;
		}

		atString				m_Name;
		atString				m_ShortName;
		atArray<TrackInfo*>		m_Tracks;

		// this is for bone animations
		~ChunkInfo()			{ while( m_Tracks.GetCount() ) delete m_Tracks.Pop(); while( m_Children.GetCount() ) delete m_Children.Pop(); }
		int						m_BoneIndex;
		atString				m_BoneID;
		atArray<ChunkInfo*>		m_Children;
		Matrix44				m_BoneMatrix;
		Matrix44				m_InitialExclusiveMatrix, m_InitialTransformationMatrix;
		Vector3					m_Scale;
		bool					m_IsJoint;
		ChunkInfo*				m_Parent;
		bool					m_bSuppressWarnings;


		float	GetStartTime() const
		{
			int trackCount = m_Tracks.GetCount();
			float minTime = FLT_MAX;
			for(int t = 0; t < trackCount; t++)
			{
				if(m_Tracks[t]->m_StartTime < minTime)
					minTime = m_Tracks[t]->m_StartTime;
			}
			return (minTime == FLT_MAX) ? 0.f : minTime;
		}

		float	GetEndTime() const
		{
			int trackCount = m_Tracks.GetCount();
			float maxTime = -1.f;
			for(int t = 0; t < trackCount; t++)
			{
				if(m_Tracks[t]->m_EndTime > maxTime )
					maxTime = m_Tracks[t]->m_EndTime;
			}
			return (maxTime < 0.f) ? 0.f : maxTime;
		}

		int GetDescendantCount() const
		{
			int childCount = m_Children.GetCount();
			int count = childCount;
			for( int a = 0; a < childCount; a++ )
			{
				count += m_Children[ a ]->GetDescendantCount();
			}
			return count;
		}

		void GetChunkNamesRecursively( atArray<atString>& childChunkNames ) // depth first
		{
			childChunkNames.PushAndGrow( m_Name );
			int childCount = m_Children.GetCount();
			for( int c = 0; c < childCount; c++ )
				m_Children[ c ]->GetChunkNamesRecursively( childChunkNames );
		}
	};

	float						m_StartTime, m_EndTime;
	float						m_FramesPerSecond;
	bool						m_AuthoredOrientation;

	atArray<ChunkInfo*>			m_RootChunks;
	Matrix34					m_ParentMatrix;

	// Array of generic chunk data.  Currently used for blend shape data
	ChunkInfo*					m_GenericChunk;

	struct MoverChunkInfo
	{
		virtual ~MoverChunkInfo()
		{
			for(int i=0; i<m_MoverTracks.GetCount(); i++)
				delete m_MoverTracks[i];
		}
		atString				m_MoverId;
		atString				m_MoverName;
		atArray<TrackInfo*>		m_MoverTracks;
	};

	atArray<MoverChunkInfo*>	m_MoverChunks;
	bool						m_MultiMoverMode;

	
	ChunkInfo* GetChunkByName( const atString& name, ChunkInfo* currChunk = NULL ) const
	{
		if( !m_RootChunks.GetCount() )
			return NULL;

		if( !currChunk )
		{
			int rootCount = m_RootChunks.GetCount();
			for( int r = 0; r < rootCount; r++ )
			{
				ChunkInfo* chunk = GetChunkByName( name, m_RootChunks[ r ] );
				if( chunk )
					return chunk;
			}
			return NULL;
		}

		if( !strcmp( name, currChunk->m_Name ))
			return currChunk;

		int childCount = currChunk->m_Children.GetCount();
		for( int a = 0; a < childCount; a++ )
		{
			ChunkInfo* chunk = GetChunkByName( name, currChunk->m_Children[ a ] );
			if( chunk )
				return chunk;
		}

		return NULL;
	}

	int GetIndexByChunkName( const atString& name, ChunkInfo* currChunk = NULL, int* currIdx = 0 ) const
	{
		if( !m_RootChunks.GetCount() )
			return -1;

		if( !currChunk )
		{
			int startIdx = 0;

			int rootCount = m_RootChunks.GetCount();
			for( int r = 0; r < rootCount; r++)
			{
				int retIdx = GetIndexByChunkName( name, m_RootChunks[ r ], &startIdx );
				if( retIdx >= 0 )
					return retIdx;
			}
			return -1;
		}

		if( !strcmp( name, currChunk->m_Name ))
			return *currIdx;

		(*currIdx)++;

		int childCount = currChunk->m_Children.GetCount();
		for( int a = 0; a < childCount; a++ )
		{
			int retIdx  = GetIndexByChunkName( name, currChunk->m_Children[ a ], currIdx );
			if( retIdx >= 0 )
				return retIdx;
		}

		return -1;
	}

	void GetTimeExtents( const atArray<atString>& chunkNames, float& startTime, float& endTime )
	{
		startTime = FLT_MAX;
		endTime = -1;

		int chunkCount = chunkNames.GetCount();
		for( int a = 0; a < chunkCount; a++ )
		{
			const rexObjectGenericAnimation::ChunkInfo* chunk = GetChunkByName( chunkNames[ a ] );
			if( chunk )
			{
				float chunkStart = chunk->GetStartTime();
				float chunkEnd = chunk->GetEndTime();

				if( chunkStart < startTime )
					startTime = chunkStart;

				if( chunkEnd > endTime )
					endTime = chunkEnd;
			}
		}

		if( startTime < 0 || startTime == FLT_MAX )
			startTime = 0;
		if( endTime < 0 )
			endTime = 0;
	}
};

class rexObjectGenericAnimationSegment : public rexObjectGenericEntityComponent
{
public:
	rexObjectGenericAnimationSegment() : rexObjectGenericEntityComponent() 
	{ m_StartTime = -1.0f; m_EndTime = -1.0f; m_Autoplay = false; m_Looping = false; m_IgnoreChildBones = false; m_DontExportRoot = false; }

	virtual rexObject* CreateNew()   { return new rexObjectGenericAnimationSegment; }

	int GetChunkCount( const rexObjectGenericAnimation& animation ) const
	{
		int count = m_ChunkNames.GetCount();
		if( !m_IgnoreChildBones )
		{
			int c = count;
			for( int a = 0; a < c; a++ )
			{
				rexObjectGenericAnimation::ChunkInfo* chunk = animation.GetChunkByName( m_ChunkNames[ a ] );
				if( chunk )
					count += chunk->GetDescendantCount();
			}
		}
		return count;
	}

	rexObjectGenericAnimation::ChunkInfo* GetChunkByIndex( const rexObjectGenericAnimation& animation, int index ) const
	{
		Assert( index >= 0 );
		int rootCount = m_ChunkNames.GetCount();

		int currCount = 0;

		if( m_IgnoreChildBones )
		{
			if( index < rootCount )
				return animation.GetChunkByName( m_ChunkNames[ index ] );
			return NULL;
		}

		for( int r = 0; r < rootCount; r++ )
		{
			rexObjectGenericAnimation::ChunkInfo* chunk = animation.GetChunkByName( m_ChunkNames[ r ] );
			if( !chunk )
				return NULL;

			atArray<atString> chunkNames;
			chunk->GetChunkNamesRecursively( chunkNames );
			int chunkCount = chunkNames.GetCount();
			for( int c = 0; c < chunkCount; c++ )
			{
				if( index == currCount++ )
					return animation.GetChunkByName( chunkNames[ c ] );
			}
		}
		return NULL;
	}

	float				m_StartTime, m_EndTime;
	atArray<atString>	m_ChunkNames;
	bool				m_Autoplay, m_Looping, m_IgnoreChildBones, m_DontExportRoot;
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif

