// 
// rexGeneric/objectEdgeModel.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectGenericEdgeModel -- class with data suitable for describing an edgemodel
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_EDGEMODEL_H__
#define __REX_GENERIC_EDGEMODEL_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/object.h"
#include "rexGeneric/objectMesh.h"

#include "atl/array.h"
#include "atl/string.h"

#include "vector/matrix34.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObjectGenericEdgeModelHierarchy : public rexObjectGenericMeshHierarchy
{
	// the contained objects are all rexObjectGenericMesh 

public:
	rexObjectGenericEdgeModelHierarchy() : rexObjectGenericMeshHierarchy(), m_AllowMultipleEdgeModels(false)  { }
	virtual rexObject* CreateNew()   { return new rexObjectGenericEdgeModelHierarchy; }	

public:
	bool		m_AllowMultipleEdgeModels;
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
