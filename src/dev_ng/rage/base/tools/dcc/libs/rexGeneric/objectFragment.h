// 
// rexGeneric/objectFragment.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		AGE 
//			atl, core, data, vector
//		REX
//			Base Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectFragment 
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_OBJECT_FRAGMENT_H__
#define __REX_OBJECT_FRAGMENT_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/object.h"
#include "rexGeneric/objectFragment.h"
#include "rexGeneric/objectMesh.h"
#include "atl/array.h"
#include "atl/string.h"
#include "vector/matrix34.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObjectGenericFragmentHierarchy : public rexObjectHierarchy
{
public:
	rexObjectGenericFragmentHierarchy(bool damageState = false) 
		: rexObjectHierarchy()
		, m_DamageState(damageState)
		, m_BreakableGlass(false)

	{
		m_GlassType = "";

		if (damageState)
		{
			++sm_DamageStates;
		}
	}

	~rexObjectGenericFragmentHierarchy()
	{
		if (m_DamageState)
		{
			--sm_DamageStates;
		}
	}

	virtual rexObject* CreateNew()   { return new rexObjectGenericFragmentHierarchy(m_DamageState); }

	atArray< atArray< rexObjectGenericMesh*> >	m_MeshesByLODLevel;
	atArray< atArray< rexObjectGenericMesh::sThresholdPair > >	m_ThresholdsByLODLevel;

	bool GetDamageState() const { return m_DamageState;	}
	
	void SetBreakableGlassFlag(bool bGlass) { m_BreakableGlass = bGlass; }
	bool IsBreakableGlass() const { return m_BreakableGlass; }
	
	static bool GetDamageStateExists()
	{
		// There is a damage state if this value is 2 or greater. Why not 1 you ask? Because REX creates
		// a dummy one at the beginning of the export process that accounts for number 1.
		return sm_DamageStates > 1;
	}

	atString m_GlassType;

private:
	bool m_DamageState;
	bool m_BreakableGlass;

	static int sm_DamageStates;
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif // __REX_OBJECT_FRAGMENT_H__
