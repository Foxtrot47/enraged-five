
#include "rexBase/converter.h"
#include "rexBase/module.h"
#include "rexBase/object.h"
#include "rexGeneric/objectBound.h"
#include "rexGeneric/objectMesh.h"
#include "rexGeneric/processorMeshCombiner.h"
#include "vector/vector3.h"

///////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexOctreeCell
{
public:
	rexOctreeCell()
	{
		for( int a = 0; a < 8; a++ )
			m_Children[a] = NULL;
		m_Center.Zero();
		m_Dimensions.Zero();
		m_Parent = NULL;
	}

	~rexOctreeCell()
	{
		for( int a = 0; a < 8; a++ )
			delete m_Children[a];
	}

	void InitOctree( const Vector3& center, const Vector3& dimensions, float leafCellMaxSideLength, rexOctreeCell* parent = NULL )
	{
		m_Dimensions = dimensions;
		m_Center = center;
		m_Parent = parent;

		float absX = fabsf( dimensions.x ), absY = fabsf( dimensions.y ), absZ = fabsf( dimensions.z );

		if( !(( absX < leafCellMaxSideLength ) || ( absY < leafCellMaxSideLength ) || ( absZ < leafCellMaxSideLength )))
		{
			Vector3 newCellSize( dimensions );
			newCellSize.Scale( 0.5f );

			for( int a = 0; a < 8; a++ )
			{
				rexOctreeCell* newCell = new rexOctreeCell;
				m_Children[ a ] = newCell;

				Vector3 newCenter;
				if( a & 1 )  // if odd, then on right half
					newCenter.x = center.x + ( newCellSize.x * 0.5f );
				else
					newCenter.x = center.x - ( newCellSize.x * 0.5f );

				if( ( a & 3 ) < 2 ) // if a mod 4 < 2, then it's on the top half
					newCenter.y = center.y + ( newCellSize.y * 0.5f );
				else
					newCenter.y = center.y - ( newCellSize.y * 0.5f );

				if( a < 4 )
					newCenter.z = center.z - ( newCellSize.z * 0.5f );
				else
					newCenter.z = center.z + ( newCellSize.z * 0.5f );

				newCell->InitOctree( newCenter, newCellSize, leafCellMaxSideLength, this );
			}
		}		
	}

	bool AddMesh( rexObjectGenericMesh& mesh )
	{
#if 0
		float minX = m_Center.x - ( m_Dimensions.x * 0.5f ), maxX = m_Center.x + ( m_Dimensions.x * 0.5f );
		float minY = m_Center.y - ( m_Dimensions.y * 0.5f ), maxY = m_Center.y + ( m_Dimensions.y * 0.5f );
		float minZ = m_Center.z - ( m_Dimensions.z * 0.5f ), maxZ = m_Center.z + ( m_Dimensions.z * 0.5f );

		if( ( mesh.m_Center.x < minX ) || ( mesh.m_Center.x > maxX ) || ( mesh.m_Center.y < minY ) || ( mesh.m_Center.y > maxY ) || ( mesh.m_Center.z < minZ ) || ( mesh.m_Center.z > maxZ ) )
			return false;
#endif
		bool xLess = ( mesh.m_Center.x < m_Center.x );
		bool yLess = ( mesh.m_Center.y < m_Center.y );
		bool zLess = ( mesh.m_Center.z < m_Center.z );

		if( m_Children[ 0 ] )
		{
			int childIndex = -1;
			if( xLess && !yLess && zLess )
				childIndex = 0;
			else if( !xLess && !yLess && zLess )
				childIndex = 1;
			else if( xLess && yLess && zLess )
				childIndex = 2;
			else if( !xLess && yLess && zLess )
				childIndex = 3;
			else if( xLess && !yLess && !zLess )
				childIndex = 4;
			else if( !xLess && !yLess && !zLess )
				childIndex = 5;
			else if( xLess && yLess && !zLess )
				childIndex = 6;
			else if( !xLess && yLess && !zLess )
				childIndex = 7;

			Assert( childIndex >= 0 );
			return m_Children[ childIndex ]->AddMesh( mesh );
		}

		m_InputMeshes.PushAndGrow( &mesh );
		return true;
	}

	void PrintInputMeshes()
	{
		int meshCount = m_InputMeshes.GetCount();
		if( meshCount )
			Displayf( "Octree Cell at (%.02f, %.02f, %.02f) has %d meshes", m_Center.x, m_Center.y, m_Center.z, meshCount );

		for( int a = 0; a < meshCount; a++ )
		{
			Displayf( "\tMesh '%s' center at (%.02f, %.02f, %.02f)", (const char*)m_InputMeshes[ a ]->GetMeshName(), m_InputMeshes[ a ]->m_Center.x, m_InputMeshes[ a ]->m_Center.y, m_InputMeshes[ a ]->m_Center.z );
		}

		if( m_Children[ 0 ] )
		{
			for( int a = 0; a < 8; a++ )
				m_Children[ a ]->PrintInputMeshes();
		}
	}

	// a class for caching meshes that need to be combined once the list is complete
	struct AssimilateList
	{
		AssimilateList() 
		{
			m_TriCount		=0;
			m_PrimCount		=0;
			m_VertCount		=0;
			m_NormalCount	=0;
			m_MaterialCount	=0;
			m_OriginalMesh	=NULL;
			m_IsAssimilated	=false;
		}

		void AddToList(AssimilateList& list)
		{
			Assert(IsAssimilated()==false);
			m_TriCount		+=list.GetTriCount();
			m_PrimCount		+=list.GetPrimCount();
			m_VertCount		+=list.GetVertCount();
			m_NormalCount	+=list.GetNormalCount();
			m_MaterialCount	+=list.GetMaterialCount();

			m_List.Grow()=&list;
		}

		void Assimilate(bool boundMode, bool acrossBones)
		{
			// first pass - make sure meshes that aren't assimilated, get assimilated (borgify it!):
			int countPrim=0;
			int countVert=0;
			int countNormal=0;
			int countMaterial=0;

			if (m_List.GetCount())
			{
				for (int i=0;i<m_List.GetCount();i++)
				{
					if (m_List[i]->IsAssimilated()==false)
						m_List[i]->Assimilate(boundMode,acrossBones);
					countPrim+=m_List[i]->GetPrimCount();
					countVert+=m_List[i]->GetVertCount();
					countNormal+=m_List[i]->GetNormalCount();
					countMaterial+=m_List[i]->GetMaterialCount();
				}

				// second pass - should be safe to assimilate now:
				m_OriginalMesh->PreallocateArrays(countVert,countNormal,countPrim,countMaterial);
				for (int i=0;i<m_List.GetCount();i++)
				{
					m_OriginalMesh->Assimilate(*m_List[i]->m_OriginalMesh,boundMode,acrossBones);
				}
			}
			m_IsAssimilated=true;
		}

		bool IsAssimilated() const					{return m_IsAssimilated;}
		int GetTriCount() const						{return m_OriginalMesh->GetTriCount() + (m_IsAssimilated?0:m_TriCount);}
		int GetVertCount() const					{return m_OriginalMesh->m_Vertices.GetCount() + (m_IsAssimilated?0:m_VertCount);}
		int GetPrimCount() const					{return m_OriginalMesh->m_Primitives.GetCount() + (m_IsAssimilated?0:m_PrimCount);}
		int GetNormalCount() const					{return m_OriginalMesh->m_Normals.GetCount() + (m_IsAssimilated?0:m_NormalCount);}
		int GetMaterialCount() const				{return m_OriginalMesh->m_Materials.GetCount() + (m_IsAssimilated?0:m_MaterialCount);}
		atArray<atString>& GetSpecialFlags() const	{return m_OriginalMesh->m_SpecialFlags;}
		bool IsValidMesh() const					{return m_OriginalMesh->IsValidMesh();}
		float GetLodThreshold() const				{return m_OriginalMesh->m_LODThreshold;}
		float GetLodOutThreshold() const			{return m_OriginalMesh->m_LODOUTThreshold;}
		bool GetToBeOptimized() const				{return m_OriginalMesh->m_optimize;}
		bool GetChildOfLevelInstanceNode() const	{return m_OriginalMesh->m_ChildOfLevelInstanceNode;}
		int  GetBoneIndex() const					{return m_OriginalMesh->m_BoneIndex;}
		//int GetGroupIndex() const					{return m_OriginalMesh->m_GroupIndex;} // GunnarD:Retired
		int  GetLodGroupIndex() const				{return m_OriginalMesh->m_LODGroupIndex;}
		const Vector3& GetCenter() const			{return m_OriginalMesh->m_Center;}
		int GetLodLevel() const						{return m_OriginalMesh->m_LODLevel;}
		bool HasMultipleTextureCoordinates() const	{return m_OriginalMesh->m_HasMultipleTextureCoordinates;}
		int GetRenderPriority() const				{return m_OriginalMesh->m_RenderPriority;}

		bool							m_IsAssimilated;
		int								m_TriCount;
		int								m_VertCount;
		int								m_PrimCount;
		int								m_NormalCount;
		int								m_MaterialCount;
		rexObjectGenericMesh*			m_OriginalMesh;
		atArray<AssimilateList*>		m_List;
	};

	bool MeshesShouldBeCombined( const AssimilateList& mesh1, const AssimilateList& mesh2, int maxPrimCountToCombine, int maxPrimCountOfCombinedMesh, float maxDistanceToCombineMesh, float maxThresholdRatio, bool boundMode, bool mergeMaterials, float primBoundCompositeMaxDist, float /*primBoundCompositeMaxDistToRadiusRatio*/, bool acrossBones, bool exportPrimsInBvh ) const
	{
		if ( exportPrimsInBvh )
		{
			bool isMesh1Bound = NULL != dynamic_cast< rexObjectGenericBound* >( mesh1.m_OriginalMesh ) && mesh1.m_OriginalMesh->m_IsValid;
			int attridx1 = 
				mesh1.m_OriginalMesh->m_InstAttrNames.Find( atString("nocombine") );
			bool isMesh1Combinable = true;
			if ( ( attridx1 != -1 ) && ( mesh1.m_OriginalMesh->m_InstAttrValues[ attridx1 ].GetType() == rexValue::BOOLEAN ) )
				isMesh1Combinable = mesh1.m_OriginalMesh->m_InstAttrValues[ attridx1 ].vBoolean;
				
			bool isMesh2Bound = NULL != dynamic_cast< rexObjectGenericBound* >( mesh2.m_OriginalMesh ) && mesh2.m_OriginalMesh->m_IsValid;
			int attridx2 = 
				mesh2.m_OriginalMesh->m_InstAttrNames.Find( atString("nocombine") );
			bool isMesh2Combinable = true;
			if ( ( attridx2 != -1 ) && ( mesh2.m_OriginalMesh->m_InstAttrValues[ attridx2 ].GetType() == rexValue::BOOLEAN ) )
				isMesh2Combinable = mesh2.m_OriginalMesh->m_InstAttrValues[ attridx2 ].vBoolean;

			return 
				( isMesh1Bound && isMesh1Combinable ) && 
				( isMesh2Bound && isMesh2Combinable );
		}

		bool isValidMesh = mesh1.IsValidMesh();
		if( ( isValidMesh != mesh2.IsValidMesh() ) || ( !isValidMesh && !(boundMode || mergeMaterials) ))
			return false;

		int flagCount1 = mesh1.GetSpecialFlags().GetCount();
		int flagCount2 = mesh2.GetSpecialFlags().GetCount();

		if( flagCount1 != flagCount2 )
			return false;

		for( int a = 0; a < flagCount1; a++ )
		{
			if( mesh1.GetSpecialFlags()[ a ] != mesh2.GetSpecialFlags()[ a ] )
				return false;
		}

		if( isValidMesh )
		{
			bool nocMesh1 = false; 
			bool nocMesh2 = false;
			int  nocAttrIdx; 
			
			//Check for a nocombine attribute set on mesh 1
			nocAttrIdx = mesh1.m_OriginalMesh->m_InstAttrNames.Find(atString("nocombine"));
			if( (nocAttrIdx != -1) && (mesh1.m_OriginalMesh->m_InstAttrValues[nocAttrIdx].GetType() == rexValue::BOOLEAN) )
				nocMesh1 = mesh1.m_OriginalMesh->m_InstAttrValues[nocAttrIdx].vBoolean;

			//Check for a nocombine attribute set on mesh 2
			nocAttrIdx = mesh2.m_OriginalMesh->m_InstAttrNames.Find(atString("nocombine"));
			if( (nocAttrIdx != -1) && (mesh2.m_OriginalMesh->m_InstAttrValues[nocAttrIdx].GetType() == rexValue::BOOLEAN) )
				nocMesh2 = mesh2.m_OriginalMesh->m_InstAttrValues[nocAttrIdx].vBoolean;

			if( nocMesh1 || nocMesh2 )
				return false;

			int mesh1TriCount = mesh1.GetTriCount();
			int mesh2TriCount = mesh2.GetTriCount();
			float thresholdRatio = FLT_MAX;
			if ( mesh1.GetLodThreshold() && mesh2.GetLodThreshold() ) 
			{
				thresholdRatio = ( Max( mesh1.GetLodThreshold(), mesh2.GetLodThreshold() ) / Min( mesh1.GetLodThreshold(), mesh2.GetLodThreshold() ) );
			}
// 			if ( mesh1.GetLodOutThreshold() && mesh2.GetLodOutThreshold() ) 
// 			{
// 				thresholdRatio = ( Max( mesh1.GetLodOutThreshold(), mesh2.GetLodOutThreshold() ) / Min( mesh1.GetLodOutThreshold(), mesh2.GetLodOutThreshold() ) );
// 			}
			Assert( thresholdRatio >= 1.0f );
			
			int maxVertCountToCombine = 65535;
			int mesh1VertCount = mesh1.GetVertCount();
			int mesh2VertCount = mesh2.GetVertCount();

			return (    
				/* not too many polys already */ 			(( maxPrimCountToCombine < 0 ) || (( mesh1TriCount <= maxPrimCountToCombine ) && ( mesh2TriCount <= maxPrimCountToCombine ) && ( mesh1TriCount + mesh2TriCount < maxPrimCountOfCombinedMesh  )))
				&& /* not too many verts already */ 		(( maxVertCountToCombine < 0 ) || (( mesh1VertCount <= maxVertCountToCombine ) && ( mesh2VertCount <= maxVertCountToCombine ) && ( mesh1VertCount + mesh2VertCount < maxVertCountToCombine  )))
				&& /* not too far away */					(( maxDistanceToCombineMesh < 0 ) || ( mesh2.GetCenter().Dist( mesh1.GetCenter() ) <= maxDistanceToCombineMesh ))
				&& /* on same bone */					 	( acrossBones || mesh1.GetBoneIndex() == mesh2.GetBoneIndex() )
//				&& /* in same room */					 	( mesh1.GetGroupIndex() == mesh2.GetGroupIndex() )
				&& /* in same LOD Group/thresholds are similar */ 	( boundMode || ( mesh1.GetLodGroupIndex() == mesh2.GetLodGroupIndex() ) || ( ( maxThresholdRatio >= 0.0f ) && thresholdRatio <= ( 1.0f + maxThresholdRatio )) )
				&& /* in same LOD level */					( boundMode || ( mesh1.GetLodLevel() == mesh2.GetLodLevel() ) )
				&& /* both have same number of UV sets */	( (boundMode || mergeMaterials) || ( mesh1.HasMultipleTextureCoordinates() == mesh2.HasMultipleTextureCoordinates() ) )
				&& /* neither are level instance*/			( !mesh1.GetChildOfLevelInstanceNode() && !mesh2.GetChildOfLevelInstanceNode() )
				&& /* render priority not equal*/			( mesh1.GetRenderPriority() == mesh2.GetRenderPriority())
			);
		}
		else
		{
			return (    
				/* not too far away */						(( primBoundCompositeMaxDist < 0 ) || ( mesh2.GetCenter().Dist( mesh1.GetCenter() ) <= primBoundCompositeMaxDist ))
				&& /* on same bone */					 	( mesh1.GetBoneIndex() == mesh2.GetBoneIndex() )
// 				&& /* in same room */					 	( mesh1.GetGroupIndex() == mesh2.GetGroupIndex() )
				&& /* neither are level instance*/			( !mesh1.GetChildOfLevelInstanceNode() && !mesh2.GetChildOfLevelInstanceNode() )
			);
		}
	}

	void CombineMeshes( int maxPrimCountToCombine, int maxPrimCountOfCombinedMesh, float maxDistanceToCombineMesh, float maxThresholdRatio, bool boundMode, bool mergeMaterials, float primBoundCompositeMaxDist, float primBoundCompositeMaxDistToRadiusRatio, bool acrossBones, bool exportPrimsInBvh )
	{
		// combine the meshes inside my area
		int inputMeshCount = m_InputMeshes.GetCount();
		
	
		atMap< rexObjectGenericMesh*, AssimilateList> assimilateMap;

		for( int a = 0; a < inputMeshCount; a++ )
		{
			Assert( m_InputMeshes[ a ] );

			bool combined = false;

			int newMeshCount = m_CombinedMeshes.GetCount();
			for( int n = 0; n < newMeshCount; n++ )
			{
				if (!assimilateMap.Access(m_InputMeshes[a]))
				{
					assimilateMap[m_InputMeshes[a]].m_OriginalMesh=m_InputMeshes[a];
				}

				if (!assimilateMap.Access(m_InputMeshes[n]))
				{
					assimilateMap[m_InputMeshes[n]].m_OriginalMesh=m_InputMeshes[n];
				}

				if( MeshesShouldBeCombined( assimilateMap[m_InputMeshes[ a ]], assimilateMap[m_CombinedMeshes[ n ]], maxPrimCountToCombine, maxPrimCountOfCombinedMesh, maxDistanceToCombineMesh, maxThresholdRatio, boundMode, mergeMaterials, primBoundCompositeMaxDist, primBoundCompositeMaxDistToRadiusRatio, acrossBones, exportPrimsInBvh ))
				{
					if ( exportPrimsInBvh )
					{
						rexObjectGenericBound* bound = dynamic_cast<rexObjectGenericBound*>( m_InputMeshes[ a ] );
						if( bound && bound->m_IsValid )
							m_CombinedMeshes[ n ]->m_ContainedObjects.PushAndGrow( bound );
					}
					else
					if( m_InputMeshes[ a ]->IsValidMesh() )
					{
						assimilateMap[m_CombinedMeshes[n]].AddToList(assimilateMap[m_InputMeshes[ a ]]);
					}

					m_InputMeshes[ a ]->m_IsValid = false;
					combined = true;
					break;
				}
			}
			if( !combined )
			{				
				m_CombinedMeshes.PushAndGrow( m_InputMeshes[ a ] );
				m_InputMeshes[ a ]->m_DeleteContainedObjectsOnDestruction = false;
			}
		}

		// make sure the list of objects is assimilated:
		atMap< rexObjectGenericMesh*, AssimilateList>::Iterator iterator=assimilateMap.CreateIterator();
		for (iterator.Start();!iterator.AtEnd();iterator.Next())
		{
			if (iterator.GetData().IsAssimilated()==false)
				iterator.GetData().Assimilate((boundMode || mergeMaterials),acrossBones);
		}
		assimilateMap.Kill();

		// go through my children.....
		for( int c = 0; c < 8; c++ )
		{
			if( m_Children[ c ] )
			{
				// tell them to combine their meshes
				m_Children[ c ]->CombineMeshes( maxPrimCountToCombine, maxPrimCountOfCombinedMesh, maxDistanceToCombineMesh, maxThresholdRatio, boundMode, mergeMaterials, primBoundCompositeMaxDist, primBoundCompositeMaxDistToRadiusRatio, acrossBones, exportPrimsInBvh );

				// then see if i can combine any of their combined meshes into mine
				int childMeshCount = m_Children[ c ]->m_CombinedMeshes.GetCount();
				for( int a = 0; a < childMeshCount; a++ )
				{
					rexObjectGenericMesh* childMesh = m_Children[ c ]->m_CombinedMeshes[ a ];

					bool combined = false;

					int thisMeshCount = m_CombinedMeshes.GetCount();
					for( int n = 0; n < thisMeshCount; n++ )
					{
						Assert( m_CombinedMeshes[ n ] );
						rexObjectGenericMesh* thisMesh = m_CombinedMeshes[ n ];

						if (!assimilateMap.Access(thisMesh))
						{
							assimilateMap[thisMesh].m_OriginalMesh=thisMesh;
						}

						if (!assimilateMap.Access(childMesh))
						{
							assimilateMap[childMesh].m_OriginalMesh=childMesh;
						}

						if( MeshesShouldBeCombined( assimilateMap[childMesh], assimilateMap[thisMesh], maxPrimCountToCombine, maxPrimCountOfCombinedMesh, maxDistanceToCombineMesh, maxThresholdRatio, boundMode, mergeMaterials, primBoundCompositeMaxDist, primBoundCompositeMaxDistToRadiusRatio, acrossBones, exportPrimsInBvh ))
						{
							if ( exportPrimsInBvh )
							{
								rexObjectGenericBound* bound = dynamic_cast<rexObjectGenericBound*>( childMesh );

								if( bound && bound->m_IsValid )
									thisMesh->m_ContainedObjects.PushAndGrow( bound, 64 );
							}
							else
							if( childMesh->IsValidMesh() )
							{
								assimilateMap[thisMesh].AddToList(assimilateMap[childMesh]);
							}

							childMesh->m_IsValid = false;

							combined = true;
							break;
						}
					}
					if( !combined )
					{
						m_CombinedMeshes.PushAndGrow( childMesh, (u16) childMeshCount );
					}
				}
			}
			else 
				break;
		}

		// make sure the list of objects is assimilated:
		atMap< rexObjectGenericMesh*, AssimilateList>::Iterator iterator2=assimilateMap.CreateIterator();
		for (iterator2.Start();!iterator2.AtEnd();iterator2.Next())
		{
			if (iterator2.GetData().IsAssimilated()==false)
				iterator2.GetData().Assimilate((boundMode || mergeMaterials),acrossBones);
		}
		assimilateMap.Kill();

	}

	rexOctreeCell* m_Children[8];
	rexOctreeCell* m_Parent;
	Vector3 m_Center, m_Dimensions;
	atArray<rexObjectGenericMesh*> m_InputMeshes, m_CombinedMeshes;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////

static void CombineMeshes( atArray< rexObjectGenericMesh* >& meshes, int maxPrimCountToCombine, int maxPrimCountOfCombinedMesh, float maxDistanceToCombineMesh, 
						   float maxThresholdRatio, bool boundMode, bool mergeMaterials, float primBoundCompositeMaxDist, float primBoundCompositeMaxDistToRadiusRatio, bool acrossBones, bool exportPrimsInBvh )
{
	Assert( maxPrimCountOfCombinedMesh >= maxPrimCountToCombine );

	atArray< rexObjectGenericMesh* > combinedMeshes;

	int meshCount = meshes.GetCount();
	if( meshCount <= 1 )
		return;

	Vector3 center( 0, 0, 0 );
	for( int m = 0; m < meshCount; m++ )
	{
		rexObjectGenericMesh* mesh = meshes[ m ];
		Assert( mesh );
		center.Add( mesh->m_Center );
	}
	center.Scale( 1.0f / meshCount );		

	float maxDeltaX = 1.0f, maxDeltaY = 1.0f, maxDeltaZ = 1.0f;

	for( int m = 0; m < meshCount; m++ )
	{
		rexObjectGenericMesh* mesh = dynamic_cast<rexObjectGenericMesh*>( meshes[ m ] );
		Assert( mesh );
		Vector3 delta; 
		delta.Subtract( mesh->m_Center, center );
		maxDeltaX = Max( maxDeltaX, fabsf( delta.x ) );
		maxDeltaY = Max( maxDeltaY, fabsf( delta.y ) );
		maxDeltaZ = Max( maxDeltaZ, fabsf( delta.z ) );
	}

	rexOctreeCell octree;
	float cellSize = Min( 50.0f/rexSerializer::GetUnitTypeLinearConversionFactor(), Max( maxDeltaX, maxDeltaY, maxDeltaX ) );
	octree.InitOctree( center, Vector3( maxDeltaX * 2, maxDeltaY * 2, maxDeltaZ * 2 ), cellSize );

	for( int m = 0; m < meshCount; m++ )
	{
		rexObjectGenericMesh* mesh = meshes[ m ];
		Assert( mesh );
		AssertVerify( octree.AddMesh( *mesh ) );
	}

	octree.CombineMeshes( 
		maxPrimCountToCombine, 
		maxPrimCountOfCombinedMesh, 
		maxDistanceToCombineMesh, 
		maxThresholdRatio,
		boundMode,
		mergeMaterials, 
		primBoundCompositeMaxDist, 
		primBoundCompositeMaxDistToRadiusRatio, 
		acrossBones, 
		exportPrimsInBvh );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexProcessorMeshCombiner::Process( rexObject* object ) const
{
	if( m_ProgressBarTextCB )
	{
		static char message[1024];
		sprintf( message, "Combining Mesh Data" );
		(*m_ProgressBarTextCB) ( message );
	}

	rexObjectGenericMeshHierarchy* hier = dynamic_cast<rexObjectGenericMeshHierarchy*>( object );
	if( hier )
	{
		int maxPolyCountToCombine = m_MaxPolyCountToCombine;
		int maxPolyCountForCombinedMesh = m_MaxPolyCountForCombinedMesh;
		float maxDistanceToCombineMesh = m_MaxDistanceToCombineMesh;
		const char *rmc = getenv("REX_MEGA_COMBINE");
		if (rmc && atoi(rmc) && !maxPolyCountToCombine) {
			char buffer[256];
			strcpy(buffer,rmc);
			maxPolyCountToCombine = atoi(strtok(buffer, ","));
			maxPolyCountForCombinedMesh = atoi(strtok(NULL, ","));
			maxDistanceToCombineMesh = (float)atof(strtok(NULL, ","));
			printf("MeshCombiner: Using override parameters %d, %d, %f\n", maxPolyCountToCombine, maxPolyCountForCombinedMesh, maxDistanceToCombineMesh);
		}

		// combine meshes if thresholds set
		if( maxPolyCountToCombine == 0 && maxPolyCountForCombinedMesh == 0 )
			return rexResult( rexResult::PERFECT ); 

		atArray< rexObjectGenericMesh* >						meshes;

		hier->GetMeshes( meshes );
		CombineMeshes( meshes, maxPolyCountToCombine, maxPolyCountForCombinedMesh, maxDistanceToCombineMesh, m_MaxLODThresholdRatio, 
					   m_BoundMode, m_MergeMaterials, m_PrimitiveBoundCompositeMaxDistance, m_PrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio, m_CombineAcrossBones, m_ExportPrimitivesInBvh );

		hier->Consolidate();

		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyMeshCombineMaxPolyCountToCombine::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorMeshCombiner* processor = dynamic_cast<rexProcessorMeshCombiner*>( module.m_Processors[ a ] );
		if( processor )
		{
			processor->SetMaxPolyCountToCombine( value.toInt( processor->GetMaxPolyCountToCombine() ) );
		}
	}
	return rexResult( rexResult::PERFECT );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyMeshCombineMaxPolyCountOfCombinedMesh::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorMeshCombiner* processor = dynamic_cast<rexProcessorMeshCombiner*>( module.m_Processors[ a ] );
		if( processor )
		{
			processor->SetMaxPolyCountForCombinedMesh( value.toInt( processor->GetMaxPolyCountForCombinedMesh() ) );
		}
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyMeshCombineMaxDistance::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorMeshCombiner* processor = dynamic_cast<rexProcessorMeshCombiner*>( module.m_Processors[ a ] );
		if( processor )
		{
			processor->SetMaxDistanceToCombineMesh( value.toFloat( processor->GetMaxDistanceToCombineMesh() ) );
		}
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyMeshCombineMaxLODThresholdRatio::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorMeshCombiner* processor = dynamic_cast<rexProcessorMeshCombiner*>( module.m_Processors[ a ] );
		if( processor )
		{
			processor->SetMaxLODThresholdRatio( value.toFloat( processor->GetMaxLODThresholdRatio() ) );
		}
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyBoundSetPrimitiveBoundCompositeMaxDistance::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorMeshCombiner* processor = dynamic_cast<rexProcessorMeshCombiner*>( module.m_Processors[ a ] );
		if( processor )
		{
			processor->SetPrimitiveBoundCompositeMaxDistance( value.toFloat( processor->GetPrimitiveBoundCompositeMaxDistance() ) );
		}
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyBoundSetPrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorMeshCombiner* processor = dynamic_cast<rexProcessorMeshCombiner*>( module.m_Processors[ a ] );
		if( processor )
		{
			processor->SetPrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio( value.toFloat( processor->GetPrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio() ) );
		}
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

rexResult  rexPropertyMeshCombineAcrossBones::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorMeshCombiner* processor = dynamic_cast<rexProcessorMeshCombiner*>( module.m_Processors[ a ] );
		if( processor )
		{
			processor->SetCombineAcrossBones( value.toBool( processor->GetCombineAcrossBones() ) );
		}
	}
	
	//Set the state of the combine across bones property in the converter as well
	rexConverter* conv = dynamic_cast<rexConverter*>(module.m_Converter);
	if(conv)
	{
		conv->SetCombineAcrossBones(value.toBool());	
	}

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyMeshCombineMergeMaterials::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	int processorCount = module.m_Processors.GetCount();
	for( int a = 0; a < processorCount; a++ )
	{
		rexProcessorMeshCombiner* processor = dynamic_cast<rexProcessorMeshCombiner*>( module.m_Processors[ a ] );
		if( processor )
		{
			processor->SetMergeMaterials( value.toBool( processor->GetMergeMaterials() ) );
		}
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

} // namespace rage
