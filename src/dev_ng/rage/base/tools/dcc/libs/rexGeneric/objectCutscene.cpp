
#include "objectCutscene.h"

#include "system/magicnumber.h"


namespace rage {

// rexObjectCutscene
rexObjectCutscene::rexObjectCutscene()
{
	mName = "";
	mBookmarks = "";
	mNumBookmarks = 0;
}

// rexObjectCSCast
rexObjectCSCast::rexObjectCSCast()
{
	mReferenced = false;
	mCategory = "";
	mType = "";
	mName = "";
}

rexObjectCSCast::rexObjectCSCast(bool ref, const char *cat, const char *type, const char *name)
{
	mReferenced = ref;
	mCategory = cat;
	mType = type;
	mName = name;
}

bool rexObjectCSCast::operator==(const rexObjectCSCast& cast) const
{
	return( mReferenced == cast.mReferenced &&
			mCategory == cast.mCategory &&
			mType == cast.mType &&
			mName == cast.mName );
}

// rexObjectCSData
rexObjectCSData::rexObjectCSData()
{
	mType = "";
	mFileName = "";
}

rexObjectCSData::rexObjectCSData(const char *type, const char *filename)
{
	mType = type;
	mFileName = filename;
}

bool rexObjectCSData::operator==(const rexObjectCSData& data) const
{
	return( mType == data.mType && mFileName == data.mFileName );
}

// rexObjectCSEvent
rexObjectCSEvent::rexObjectCSEvent()
:	rexObject()
{
	mName = "";
	mType = "";
	mFPS = 30.0f;
	mSceneStartFrame = 0;
	mStartFrame = 0;
	mEndFrame = 0;

	mEventString = "";

	mStreamName = "";

	mEmitterName = "";
	mEmitterType = "";
	mParentActor = "";
	mParentBone = "";
	mOffsetX = 0.0f;
	mOffsetY = 0.0f;
	mOffsetZ = 0.0f;

	mClipNear = 0.01f;
	mClipFar = 500.0f;
}

// rexObjectCSCamera
rexObjectCSCamera::rexObjectCSCamera()
:	rexObject()
{
	mName = "";
	mMagic = MAKE_MAGIC_NUMBER('A','N','I','2');
	mFlags = 0;
	mNumFrames = 0;
	mNumChannels = 0;
	mStride[0] = mStride[1] = mStride[2] = 0.0f;
	mFrames = NULL;
}

rexObjectCSCamera::~rexObjectCSCamera()
{
	if( mFrames != NULL )
	{
		for( int i = 0; i < mNumFrames; i++ )
		{
			delete [] mFrames[i].mData;
		}
		delete [] mFrames;
	}
	mFrames = NULL;
}

void rexObjectCSCamera::Init()
{
	if( mNumFrames == 0 || mNumChannels == 0 )
		return;

	mFrames = new FrameData[ mNumFrames ];
	for( int i = 0; i < mNumFrames; i++ )
	{
		mFrames[i].mNumChannels = mNumChannels;
		mFrames[i].mData = new float[ mNumChannels ];
	}
}

void rexObjectCSCamera::SetFrameData(int index, int channels, const float* data)
{
	if( index > mNumFrames || channels != mNumChannels )
		return;

	for( int i = 0; i < mNumChannels; i++ )
	{
		mFrames[index].mData[i] = data[i];
	}
}


// rexObjectCSLight

} // namespace rage


