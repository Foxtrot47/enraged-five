// 
// rexGeneric/serializerContainer.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSerializerGenericContainer
//			 -- base class that allows for hooks to be added in order to extra contained objects info
//			    for output in container object file.  
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexSerializer
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_SERIALIZER_GENERICCONTAINER_H__
#define __REX_GENERIC_SERIALIZER_GENERICCONTAINER_H__

#include "rexBase/serializer.h"

#include "atl/functor.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class fiStream;

/////////////////////////////////////////////////////////////////////////////////////

typedef Functor2Ret<rexResult, rexObject&, fiStream*> rexSerializerGenericContainerCallback;

/////////////////////////////////////////////////////////////////////////////////////

class rexSerializerGenericContainer : public rexSerializer
{
public:
	rexSerializerGenericContainer() : rexSerializer() {}
	bool	RegisterObjectType( rexObject& objType, rexSerializerGenericContainerCallback callback = rexSerializerGenericContainerCallback() );

	rexResult WriteObjectInformation( rexObject& object, fiStream* s ) const;

	virtual ~rexSerializerGenericContainer();
	
protected:
	atArray<rexObject*>									m_ObjectTypes;
	atArray<rexSerializerGenericContainerCallback>		m_Callbacks;

private:
    int findType(rexObject& object) const;

};

} // namespace rage

#endif
