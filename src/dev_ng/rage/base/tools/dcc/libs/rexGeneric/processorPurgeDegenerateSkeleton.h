// 
// rexGeneric/processorPurgeDegenerateSkeleton.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexProcessorPurgeDegenerateSkeleton
//			 -- Purges skeletons which are degenerate (only have a single node in them)
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexProcessor
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_PROCESSOR_PURGE_DEG_SKEL_H__
#define __REX_GENERIC_PROCESSOR_PURGE_DEG_SKEL_H__

#include "rexBase/processor.h"
#include "rexBase/property.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage 
{
	class rexProcessorPurgeDegenerateSkeleton : public rexProcessor
	{
	public:
		rexProcessorPurgeDegenerateSkeleton() : rexProcessor()  { m_bPurgeEnabled=false; }

		virtual rexResult		Process( rexObject* object ) const;
		virtual rexProcessor*	CreateNew() const { return new rexProcessorPurgeDegenerateSkeleton; }

		bool	GetPurgeEnabled() { return m_bPurgeEnabled;}
		void	SetPurgeEnabled(bool bEnable) { m_bPurgeEnabled = bEnable; }

	private:
		bool m_bPurgeEnabled;
	};

/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyEnablePurgeDegenerateSkeleton : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyEnablePurgeDegenerateSkeleton(); }
	};

/////////////////////////////////////////////////////////////////////////////////////

};//End namespace rage

#endif __REX_GENERIC_PROCESSOR_PURGE_DEG_SKEL_H__
