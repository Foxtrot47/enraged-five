// 
// rexGeneric/objectPrimitve.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REX_GENERIC_PRIMITIVE_H__
#define __REX_GENERIC_PRIMITIVE_H__

#include "rexGeneric/objectEntity.h"
#include "vector/matrix34.h"

namespace rage {

//-----------------------------------------------------------------------------

class rexObjectGenericPrimitive : public rexObjectGenericEntityComponent
{
public:
	enum GenericPrimitiveType
	{
		PRIM_BOX,
		PRIM_SPHERE,
		PRIM_CAPSULE,
		PRIM_UNKNOWN
	};

	rexObjectGenericPrimitive()
		: m_BoneIndex(0)
	{
		m_Matrix.Identity();
	}
	virtual rexObject* CreateNew()   { return new rexObjectGenericPrimitive; }
	virtual GenericPrimitiveType GetPrimType() const { return PRIM_UNKNOWN; }

public:
	Matrix34				m_Matrix;
	int						m_BoneIndex;
};

//-----------------------------------------------------------------------------

class rexObjectGenericPrimitiveBox : public rexObjectGenericPrimitive
{
public:
	rexObjectGenericPrimitiveBox()
		: m_Width(0.0f)
		, m_Height(0.0f)
		, m_Depth(0.0f)
	{
	}
	virtual rexObject* CreateNew()   { return new rexObjectGenericPrimitiveBox; }
	virtual GenericPrimitiveType GetPrimType() const { return PRIM_BOX; }

public:
	float	m_Width;
	float	m_Height;
	float	m_Depth;
};

//-----------------------------------------------------------------------------

class rexObjectGenericPrimitiveSphere : public rexObjectGenericPrimitive
{
public:
	rexObjectGenericPrimitiveSphere()
		: m_Radius(0.0f)
	{}

	virtual rexObject* CreateNew()   { return new rexObjectGenericPrimitiveSphere; }
	virtual GenericPrimitiveType GetPrimType() const { return PRIM_SPHERE; }

public:
	float	m_Radius;
};

//-----------------------------------------------------------------------------

class rexObjectGenericPrimitiveGroup : public rexObject
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericPrimitiveGroup; }
};

//-----------------------------------------------------------------------------

}

#endif //__REX_GENERIC_PRIMITIVE_H__

