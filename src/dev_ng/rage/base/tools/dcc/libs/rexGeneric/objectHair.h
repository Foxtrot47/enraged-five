// 
// rexGeneric/objectHair.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_HAIR_H__
#define __REX_GENERIC_HAIR_H__

#include "rexBase/object.h"
#include "rexBase/value.h"
#include "rexBase/objectAdapter.h"
#include "atl/array.h"
#include "atl/string.h"

namespace rage
{

class rexObjectGenericHairMeshGroup : public rexObject
{
public:
	virtual rexObject* CreateNew()	 { return new rexObjectGenericHairMeshGroup; }
};

class rexObjectGenericHairMeshInfo : public rexObjectGenericEntityComponent
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericHairMeshInfo; }

	atString			m_HairMeshName;
};

}//End namespace rage

#endif //__REX_GENERIC_CLOTH_H__
