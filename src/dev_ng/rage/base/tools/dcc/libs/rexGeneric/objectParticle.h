// 
// rexGeneric/objectParticle.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectGenericParticle -- class with data suitable for describing a particle effect
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject (REX datBase Library)
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_PARTICLE_H__
#define __REX_GENERIC_PARTICLE_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexGeneric/objectEntity.h"
#include "vector/matrix34.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

	class rexObjectGenericParticle : public rexObjectGenericEntityComponent
	{
	public:
		struct AttachBone
		{
			AttachBone()
			: m_Name( "" ),
			  m_Tag( "" ),
			  m_IsRoot( false )
			{
			}

			atString	m_Name;		// The name of the bone
			atString	m_Tag;		// The optional text the bone is tagged with
			bool		m_IsRoot;	// Is this the root bone in a skeleton hierarchy?
		};

	public:
		rexObjectGenericParticle()
		: rexObjectGenericEntityComponent(),
		  m_EffectName( "" ),
		  m_TriggerType( 0 ),
		  m_AttachBone( NULL ),
		  m_Scale( 1.0f ),
		  m_Probability( 100 ),
		  m_IsTinted( false )
		{
			m_LocalMatrix.Identity();
			m_TintColor.Zero();
		}

		~rexObjectGenericParticle()
		{
			if( m_AttachBone )
			{
				delete m_AttachBone;
				m_AttachBone = NULL;
			}
		}

		virtual rexObject* CreateNew()   { return new rexObjectGenericParticle; }

		Matrix34				m_LocalMatrix;		// The effect's local transform
		atString				m_EffectName;		// The name of the particle effect used
		int						m_TriggerType;		// The type of trigger used for this effect (ie. ambient fx, collision fx, etc.)
		AttachBone*				m_AttachBone;		// The bone this effect is attached to (if any)
		float					m_Scale;			// The scale of the effect
		int						m_Probability;		// The probability of the effect playing
		bool					m_IsTinted;			// Is this instance of the effect tinted by the color below?
		Vector3					m_TintColor;		// The color to tint the effect
	};

	class rexObjectGenericParticleGroup : public rexObject
	{
	public:
		virtual rexObject* CreateNew()   { return new rexObjectGenericParticleGroup; }
	};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
