// 
// rexGeneric/objectMesh.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectGenericMesh -- class with data suitable for describing a mesh with materials
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject (REX datBase Library)
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_MESH_H__
#define __REX_GENERIC_MESH_H__

/////////////////////////////////////////////////////////////////////////////////////

#include <LIMITS.H>
#include "rexBase/object.h"
#include "rexBase/value.h"
#include "rexBase/objectAdapter.h"
#include "atl/array.h"
#include "atl/string.h"
#include "system/magicnumber.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vector/matrix44.h"
#include "rexGeneric/objectEntity.h"
#include "rexGeneric/objectHierarchy.h"
#include "MaterialPresets/MaterialPreset.h"

#define MAX_LOD 4

class IRageShaderMaterial;

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

enum rexObjectGenericMeshMagic
{
	rogmGenericMesh			= MAKE_MAGIC_NUMBER('R','O','G','M'),
	rogmVertexInfo			= MAKE_MAGIC_NUMBER('R','M','V','I'),
	rogmAdjunctInfo			= MAKE_MAGIC_NUMBER('R','M','A','I'),
	rogmPrimitiveInfo		= MAKE_MAGIC_NUMBER('R','M','P','I'),
	rogmMaterialInfo		= MAKE_MAGIC_NUMBER('R','M','M','I'),
};

extern char *sRexMeshBucketMaskOffsets[];

int GetRexMeshBucketBit(const char *flagString);

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericMesh : public rexObjectAdapter
{
public:
	template <class T> class BigArray : public atArray<T,0,unsigned int> {};

	// Default constructor
	rexObjectGenericMesh() : rexObjectAdapter()  
	{
		m_IsSkinned = false; 
		m_BoneIndex = 0;
		m_BoneCount = 0;
		m_LODGroupIndex = m_LODLevel = 0; 
		m_IsValid = true; 
//		m_GroupIndex = -1; // GunnarD:Retired
		m_ChildOfLevelInstanceNode = false;
//		m_WriteToFragmentOnly = false;
		m_Name = "rexObjectGenericMesh.m_Name Not set";
		m_FullPath = "rexObjectGenericMesh.m_FullPath Not set";
		m_LODThreshold = -1.0f;
		m_LODOUTThreshold = -1.0f;
		m_optimize = true;
		m_GroupID = "rexObjectGenericMesh.m_GroupID Not set";
		m_DamageState = false;

		m_Matrix.Identity();
		m_ParentBoneMatrix.Identity();
		m_Center = Vector3(0.0f, 0.0f, 0.0f);
		m_PivotPoint = Vector3(0.0f, 0.0f, 0.0f);
		m_FragmentRootOffset = Vector3(0.0f, 0.0f, 0.0f);

		m_RenderPriority = -1;

		m_bTaggedRootMatrixIsValid = false;
		m_bDataRelativeToRaggedRoot = false;
		m_TaggedRootMatrix.Identity();

		m_surfaceFlags = 0;

		// Debug stuff
		m_NoOfCurrentlyConstructedInstances++; 

		m_RenderBucketMask = 0xFF;

//		Displayf("Created an instance of a rexObjectGenericMesh (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
	}

	// Copy constructor
	rexObjectGenericMesh(const rexObjectGenericMesh& obSource) : rexObjectAdapter(obSource)
	{
		SetMeshName(obSource.GetMeshName());
		m_FullPath = obSource.m_FullPath;				// The Maya DAG path
		m_Vertices = obSource.m_Vertices;				// Verts
		m_Normals = obSource.m_Normals;					// Vertex normals, not in vertices for no good reason
		m_Primitives = obSource.m_Primitives;			// Faces
		m_Materials = obSource.m_Materials;				// The materials for the mesh.  Always at least one of these
		m_BoneIndex = obSource.m_BoneIndex;			// ?
		m_BoneCount = obSource.m_BoneCount;
		m_LODGroupIndex = obSource.m_LODGroupIndex;		// ?
		m_LODLevel = obSource.m_LODLevel;				// ?
		// m_GroupIndex = obSource.m_GroupIndex;			// GunnarD:Retired
		m_LODThreshold = obSource.m_LODThreshold;			// LOD swapping threshold to be put into the .type
		m_LODOUTThreshold = obSource.m_LODOUTThreshold;			// LOD swapping out threshold to be put into the .type
		m_optimize = obSource.m_optimize;					// flag whether mesh is to be optimised by rage
		m_GroupID = obSource.m_GroupID;				// ?
		m_Matrix = obSource.m_Matrix;				// World space matrix of the mesh
		m_ParentBoneMatrix = obSource.m_ParentBoneMatrix;		// ?
		m_FragmentRootOffset = obSource.m_FragmentRootOffset;
		m_Center = obSource.m_Center;				// Center of bounding sphere?
		m_PivotPoint = obSource.m_PivotPoint;			// Maya pivot?
		m_IsSkinned = obSource.m_IsSkinned;			// If the mesh is skinned
		m_IsValid = obSource.m_IsValid;			// Ignore mesh if not valid
		m_HasMultipleTextureCoordinates = obSource.m_HasMultipleTextureCoordinates;	// Has multiple UV sets
		m_ChildOfLevelInstanceNode = obSource.m_ChildOfLevelInstanceNode;			// ?
//		m_WriteToFragmentOnly = obSource.m_WriteToFragmentOnly; // Whether to write this mesh into the fragment section only
		m_DamageState = obSource.m_DamageState;

		m_SpecialFlags = obSource.m_SpecialFlags;			// ?
		m_SkinBoneMatrices = obSource.m_SkinBoneMatrices;		// Matrices of some bones
		
		m_InstAttrNames = obSource.m_InstAttrNames;		// Custom attributes names  on the mesh
		m_InstAttrValues = obSource.m_InstAttrValues;		// Custom attributes values on the mesh

		m_BlindDataIds = obSource.m_BlindDataIds;		// The rageIDs of the blinddata on this mesh
		m_BlindDataTypes = obSource.m_BlindDataTypes;	// The datatypes of the blinddata on this mesh

		m_RenderPriority = obSource.m_RenderPriority;// The render priority (-1 == not prioritized 0 == top priority >0 == other priorities)

		m_bTaggedRootMatrixIsValid = obSource.m_bTaggedRootMatrixIsValid;
		m_bDataRelativeToRaggedRoot = obSource.m_bDataRelativeToRaggedRoot;
		m_TaggedRootMatrix = obSource.m_TaggedRootMatrix;

		//TODO : Add serialization of the blendtargets
		m_BlendTargets = obSource.m_BlendTargets;			// ?

		m_colourTemplate = obSource.m_colourTemplate;
		m_BoundBoxMin = obSource.m_BoundBoxMin;
		m_BoundBoxMax = obSource.m_BoundBoxMax;

		m_surfaceFlags = obSource.m_surfaceFlags;

		m_RenderBucketMask = obSource.m_RenderBucketMask;
	}


	virtual ~rexObjectGenericMesh()
	{
		for(int i=0;i<m_BlendTargets.GetCount();i++)
		{
			if(m_BlendTargets[i].m_GenericMesh) 
			{
				delete m_BlendTargets[i].m_GenericMesh;
			}
		}
		// Debug stuff
		m_NoOfCurrentlyConstructedInstances--; 
//		Displayf("Deleted an instance of a rexObjectGenericMesh (%p), currently %d active instances\n", this, m_NoOfCurrentlyConstructedInstances);
	}
	
	virtual bool IsValidMesh() const  { return m_IsValid; }
		
	virtual rexObject* CreateNew()   { return new rexObjectGenericMesh; }	

	// NOTE: any time the order or number of data items in any of these classes is changed,
	// the Save and Load methods and ReadXXX/WriteXXX static functions must be updated

	struct VertexInfo
	{
		Vector3					m_Position;			// Vertex position
		atArray<int>			m_BoneIndices;		// Bones that effect this vertex
		atArray<float>			m_BoneWeights;		// Weights for each bone
		atArray<Vector3>		m_TargetDeltas;		// One delta for each blend target
		atArray<Vector3>		m_TargetNormalDeltas; //One normal delta for each blend target
		atArray<rexValue>		m_BlindData;		// Values of the blind data on this vertex
		float					m_SecondSurfaceDisplacement;	// The displacement along the z of the second surface (for collision mesh)
		static int sm_MaxMatricesPerVertex;			// should be set by higher level code (constant)
		static const float WEIGHT_TOLERANCE;		// constant magic number
	};

	struct AdjunctInfo								// Adjunct is extra info for a vertex
	{
		int						m_VertexIndex;		// Vertex this adjuct applies to, one adjuct can only have one vert, but one vert can have many adjucts
		int						m_NormalIndex;		// Location in Normal Array of the normal for this adjunct
		atArray<Vector4>		m_Colors;			// colors
		atArray<atString>		m_ColorSetNames;	// name of color set
		atArray<Vector2>		m_TextureCoordinates;	// UV of adjunct
		atArray<atString>		m_TextureCoordinateSetNames;	// Maya name of UV set
		Vector4					m_TintColour;		// Cheap vertex colour channel to be templatised during processing
		float					m_ProceduralDensity;// Density value for procedural data
		float					m_ProceduralScaleZ;  // Scale value for procedural data
		float					m_ProceduralScaleXYZ;  // Scale value for procedural data
	};

	struct PrimitiveInfo							// A primitive is really a face
	{
		int						m_MaterialIndex;	// Index into material info array
		atArray<AdjunctInfo>	m_Adjuncts;			// Adjucts(/verts) of face
		Vector3					m_FaceNormal;		// Face normal
	};

	struct MaterialInfo								// Material is the same as a shader
	{
		bool IsSame( const MaterialInfo& that, bool nonOptimizedCheck = false) const;
		MaterialInfo &operator=( const MaterialInfo &that )
		{
			m_MtlFileName = that.m_MtlFileName; // rageShaderMaterial support GML
			m_Material = that.m_Material;
			m_MaterialPreset = that.m_MaterialPreset;

			m_Name = that.m_Name;
            m_PhysicsMaterialName = that.m_PhysicsMaterialName;         // [ERWINVIENNA]
			m_TypeName = that.m_TypeName;
			m_AttributeNames = that.m_AttributeNames;
			m_AttributeValues = that.m_AttributeValues;
			m_InputTextureFileNames = that.m_InputTextureFileNames;
			m_OutputTextureFileNames = that.m_OutputTextureFileNames;
			m_PrimitiveIndices = that.m_PrimitiveIndices;
			m_IsLayered = that.m_IsLayered;
			m_RageTypes = that.m_RageTypes;
			m_UiHints = that.m_UiHints;
			m_TextureHints = that.m_TextureHints;
			m_BlendTargetIndices = that.m_BlendTargetIndices;
			m_ProcCullImmune = that.m_ProcCullImmune;
			m_ProceduralModIdx = that.m_ProceduralModIdx;
			m_BillboardCollapse = that.m_BillboardCollapse;
			m_FacingCollapse = that.m_FacingCollapse;
			m_TintShaderApplied = that.m_TintShaderApplied;
			m_TreeShaderApplied = that.m_TreeShaderApplied;
			m_CollapsedMaterialPreset = that.m_CollapsedMaterialPreset;
			m_DCCMtlID = that.m_DCCMtlID;
			
			return *this;
		}

		// Constructor
		MaterialInfo()
		{
			initDefaults();
		}

		void initDefaults (void)
		{
			m_MtlFileName = "null"; // ""; // rageShaderMaterial support GML
			m_Material = NULL;

			m_Name = "rexObjectGenericMesh::MaterialInfo.m_Name Not set";
			m_TypeName = "rexObjectGenericMesh::MaterialInfo.m_TypeName Not set";
			m_PhysicsMaterialName = "rexObjectGenericMesh::MaterialInfo.m_PhysicsMaterialName Not set";              // [ERWINVIENNA]
			m_IsLayered = false;
			m_ProcCullImmune = false;
			m_ProceduralModIdx = 0;
			m_BillboardCollapse = false;
			m_FacingCollapse	= false;
			m_TintShaderApplied = false;
			m_TreeShaderApplied = false;
			m_CollapsedMaterialPreset = false;
			m_DCCMtlID = -1;
		}

		atString				m_MtlFileName;					// rageShaderMaterial support GML
		IRageShaderMaterial *	m_Material;						// rageShaderMaterial object GML
		MaterialPreset			m_MaterialPreset;

		IRageShaderMaterial *	GetMaterial();

		atString				m_Name;							// Name of shader, as seen in the extra attributes in Maya
        atString                m_PhysicsMaterialName;          // name of the physical material, stored in an extra attribute in the maya-shader [ERWINVIENNA]
		atString				m_TypeName;						// ?
		atArray<atString>		m_AttributeNames;				// Names of attributes the shader has
		atArray<rexValue>		m_AttributeValues;				// Values of attributes the shader has
		atArray<atString>		m_RageTypes;					// ?
		atArray<atString>		m_UiHints;						// ?
		atArray<atString>		m_InputTextureFileNames;		// ?
		atArray<atString>		m_OutputTextureFileNames;		// ?
		atArray<atString>		m_TextureHints;					// ?
		BigArray<int>			m_PrimitiveIndices;				// Indices into primitive(/face) array
		bool					m_IsLayered;					// Deprecated
		bool					m_ProcCullImmune;				// Allows an alternate material to eb set for small polys

		int						m_ProceduralModIdx;				// The material's index into the procedural modifiers
		
		//TODO : Add to WriteMaterialInfo and ReadMaterialInfo
		atArray<int>			m_BlendTargetIndices;			// ?
		bool					m_BillboardCollapse;			// Whether to collapse this face for in-game billboards
		bool					m_FacingCollapse;				// Whether to collapse this triangle batch for in-game camera facing meshes

		bool					m_TintShaderApplied;			// Whether the material has a tint shader applied to it.
		bool					m_TreeShaderApplied;			// Whether the material has a tree shader applied to it.

		bool					m_CollapsedMaterialPreset;		// Whether we want to make a fully collapsed preset or just the values that don't exist in parent templates.

		int						m_DCCMtlID;						// The Mtl identifier in the DCC package to map against
	};

	struct BlendTargetInfo
	{
		BlendTargetInfo() : m_GenericMesh(NULL) {};
		atString				m_Name;
		rexObjectGenericMesh*	m_GenericMesh;
		Matrix34				m_ToBaseObjectTransform;
	};

	struct BlendTargetDelta
	{
		atArray<int>			m_VertexIndices;
		atArray<Vector3>		m_Deltas;

		Vector3 GetDelta(int vertIndex) const
		{
			for( int i = 0; i < m_VertexIndices.GetCount(); i++ )
			{
				if( m_VertexIndices[i] == vertIndex )
				{
					return m_Deltas[i];
				}
			}
			return VEC3_ZERO;
		}
	};

	struct ProceduralModifier
	{
		Vector3 m_TintColour;
		float m_ScaleZ;
		float m_ScaleXYZ;
		float m_Density;

		bool operator==( const ProceduralModifier &that )
		{
			return ( this->m_TintColour == that.m_TintColour && this->m_ScaleZ == that.m_ScaleZ && this->m_ScaleXYZ == that.m_ScaleXYZ && this->m_Density == that.m_Density );
		}
	};
	
private:
	atString					m_Name;					// Can be anything, usually set to the name of the transform if coming from Maya
public:
	const atString&				GetMeshName() const {return m_Name;}
	atString					GetMeshName(){return m_Name;}
	void						SetMeshName(const char *strName) {m_Name = strName; }
	atString					m_FullPath;				// The Maya DAG path
	BigArray<VertexInfo>		m_Vertices;				// Verts
	BigArray<Vector3>			m_Normals;				// Vertex normals, not in vertices for no good reason
	BigArray<PrimitiveInfo>		m_Primitives;			// Faces
	atArray<MaterialInfo>		m_Materials;			// The materials for the mesh.  Always at least one of these
	int							m_BoneIndex;			// The index of this meshes bone in the attached skeleton.
	unsigned int				m_BoneCount;			// The number of bones in the skeleton (not just on this object)
	int							m_RenderPriority;		// The render priority (-1 == not prioritized 0 == top priority >0 == other priorities)
	int							m_LODGroupIndex;		// an ID by which lod meshes can be grouped into separate groups per global .type file group.
	int							m_LODLevel;				// The actual level of detail
	//int							m_GroupIndex;		// used for tracking index of meshes in group. 
														// Retired as current code determines the writing of meshes and hence the index only when writing the mesh
														// Apart from that it didnt seem to be used anyway.
	float						m_LODThreshold;			// The threshold value where the model comes in 
	float						m_LODOUTThreshold;		// The threshold value where the model fades out
	bool						m_optimize;
	atString					m_GroupID;				// ?
	Matrix34					m_Matrix;				// World space matrix of the mesh
	Matrix34					m_ParentBoneMatrix;		// ?
	Vector3						m_FragmentRootOffset;	// optional offset to fragment root, used for cloth
	Vector3						m_Center;				// Center of bounding sphere?
	Vector3						m_BoundBoxMin,m_BoundBoxMax;			// bound calculated in ConvertMaxMeshNodeIntoGenericMesh. 
														// If flag is set and mesh is animating, the bound reflects the max bounds of the animation
	Vector3						m_PivotPoint;			// Maya pivot?
	bool						m_IsSkinned:1;			// If the mesh is skinned
	mutable bool				m_IsValid:1;			// Ignore mesh if not valid
	bool						m_HasMultipleTextureCoordinates;	// Has multiple UV sets
	bool						m_ChildOfLevelInstanceNode;			// ?
	bool						m_DamageState;			// If the mesh is a damaged mesh in a fragment.

	bool						m_bDataRelativeToRaggedRoot;	//Flag to indicate that the data exported needs to be relative to the tagged root so exterinal systems can place the object in the world
	bool						m_bTaggedRootMatrixIsValid;		//Flag to indicate if the tagged root matrix value is valid for use during mesh processing.
	Matrix34					m_TaggedRootMatrix;				// World space matrix of the tagged rex root node for the hierarchy this mesh is a part of
//	bool						m_WriteToFragmentOnly;	// Whether to write this mesh into the fragment section only
														// GunnarD: Replaced by exclusive type group entry
	atArray<atString>			m_SpecialFlags;			// used to separate meshes into various lod groups
	atArray<atString>			m_ExclusiveTypeGroups;			// used to separate meshes into various lod groups
	atArray<Matrix34>			m_SkinBoneMatrices;		// Matrices of some bones
	
	atArray<atString>			m_InstAttrNames;		// Custom attributes names  on the mesh
	atArray<rexValue>			m_InstAttrValues;		// Custom attributes values on the mesh

	atArray<int>					m_BlindDataIds;		// The rageIDs of the blinddata on this mesh
	atArray<rexValue::valueTypes>	m_BlindDataTypes;	// The datatypes of the blinddata on this mesh

	//TODO : Add serialization of the blendtargets
	atArray<BlendTargetInfo>	m_BlendTargets;			// ?

	atArray<Vector4>			m_colourTemplate;		// Required by ground painting code
	atArray<ProceduralModifier> m_ProceduralModifiers;  // Required for tinting, scaling and changing density of procedural geo
	u32							m_surfaceFlags;			// mesh level flags (currently used collision types)
	u16							m_RenderBucketMask;

	struct sThresholdPair
	{
	public: 
		float lodIn;
		float lodOut;
		sThresholdPair()
			:lodIn(-1)
			,lodOut(-1)
		{}
		sThresholdPair(float in, float out)
		{
			lodIn = in;
			lodOut = out;
		}
	};
	
	int GetAdjunctCount() const;

	int GetTriCount() const;
	
	void PreallocateArrays(int vertexCount,int normalCount,int primCount,int matCount);
	bool Assimilate( const rexObjectGenericMesh& that, bool handleMaterials, bool acrossBones );
	bool Consolidate();
	
	bool MakeOptimizedBoundingBox( Matrix34& bbMatrix, float& bbLength, float& bbHeight, float& bbWidth, bool worldSpace = true ) const;
	bool MakeOptimizedBoundingSphere( Vector3& center, float& radius, bool worldSpace = true ) const;
	void AddSpecialFlag(atString &flag)
	{
		m_SpecialFlags.PushAndGrow(flag, 5);
	}
	void AddExclusiveTypeGroup(atString flag)
	{
		m_ExclusiveTypeGroups.PushAndGrow(flag, 5);
	}
	bool ExclusiveTypeGroupsContain(atString flag) const
	{
		if(m_ExclusiveTypeGroups.size()==0)
			return true;
		return -1 != m_ExclusiveTypeGroups.Find(flag);
	}
	bool GetLODsMeshesAndMaterials( 
		atArray< atArray< atArray< rexObjectGenericMesh*> > >& meshesByLODGroupAndLevel, 
		atArray< atArray< atArray<rexObjectGenericMesh::sThresholdPair > > >& thresholdsByLODGroupAndLevel, 
		atArray< rexObjectGenericMesh*>& nonLODMeshes, 
		atArray<rexObjectGenericMesh::MaterialInfo>& matInfoArray, 
		bool includeLevelInstanceObjects, 
		bool nonOptimizedMTLCompare,
		rexResult *res=NULL) const;

	template<typename T> int	GetVertexPositions( T& vertArray, bool worldSpace ) const;

	// static functions
protected:
	// for rexObjectAdapter
	static rexResult ReadVertexInfo(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteVertexInfo(rexObjectAdapter *obj, int numItems, const void *items);
	static rexResult ReadAdjunctInfo(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteAdjunctInfo(rexObjectAdapter *obj, int numItems, const void *items);
	static rexResult ReadPrimitiveInfo(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WritePrimitiveInfo(rexObjectAdapter *obj, int numItems, const void *items);
	static rexResult ReadMaterialInfo(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteMaterialInfo(rexObjectAdapter *obj, int numItems, const void *items);
	
// member functions
public:
	// for rexObjectAdapter
	static rexObjectAdapter *CreateAdapter(void) { return new rexObjectGenericMesh(); }

	bool primitiveIsQuad(int i) const;

// virtual functions
public:
	virtual rexResult Save(atString &filename);
	virtual rexResult Load(atString &filename);

private:
	static int	m_NoOfCurrentlyConstructedInstances;
};

/////////////////////////////////////////////

class rexObjectGenericMeshHierarchy : public rexObjectHierarchy
{
	// the contained objects are all rexObjectGenericMesh or rexObjectGenericLODInfo
	// thresholds, when present, have indices in same order as m_ContainedObjects
public:
	rexObjectGenericMeshHierarchy() : 
	  rexObjectHierarchy(),
	  m_optimize(true),
	  m_LODDepth(0),
	  m_ChildIndex(0)
	{
		m_Matrix.Identity(); 
	}

	virtual rexObject* CreateNew()   { return new rexObjectGenericMeshHierarchy; }

	int  GetLODsMeshesAndMaterials( 
		atArray< atArray< atArray< rexObjectGenericMesh*> > >& meshesByLODGroupAndLevel, 
		atArray< atArray< atArray< rexObjectGenericMesh::sThresholdPair > > >& thresholdsByLODGroupAndLevel,	
		atArray< rexObjectGenericMesh*>& nonLODMeshes,	
		atArray<rexObjectGenericMesh::MaterialInfo>& matInfoArray, 
		bool includeLevelInstanceObjects = false, 
		bool nonOptimizedMTLCompare = false,
		rexResult *res=NULL) const;
	void GetMaterials( atArray<rexObjectGenericMesh::MaterialInfo>& matInfoArray, bool includeLevelInstanceObjects = false, bool nonOptimizedMTLCompare = false) const;
	void GetMeshes( atArray< rexObjectGenericMesh* >& meshes, bool includeLevelInstanceObjects = false ) const;
	void GetLODGroupDepths( atArray< int >& lodGroupDepths ) const;
	void GetLODGroupChildIndices( atArray< int >& lodGroupChildIndices ) const;

	virtual bool Consolidate();  // removes duplicated materials, normals, vertices, etc.

	int GetMeshCount() const;

	bool			m_optimize;
	int				m_LODDepth, m_ChildIndex;
	Matrix44		m_Matrix;             // inclusive Matrix (world space)
};

/////////////////////////////////////////////////////////////////////////////////////

/// this is only defined so the type can be used to invoke the proper serializer

class rexObjectGenericMeshHierarchyDrawable : public rexObjectGenericMeshHierarchy
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericMeshHierarchyDrawable; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
