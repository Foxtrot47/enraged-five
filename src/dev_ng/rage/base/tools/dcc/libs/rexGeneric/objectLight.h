// 
// rexGeneric/objectLight.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectGenericLight -- class with data suitable for describing a light
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject (REX datBase Library)
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_LIGHT_H__
#define __REX_GENERIC_LIGHT_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexGeneric/objectEntity.h"
#include "vector/matrix34.h"
#include "vector/vector4.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObjectGenericLight : public rexObjectGenericEntityComponent
{
public:
	rexObjectGenericLight() : rexObjectGenericEntityComponent() { m_BoneIndex = 0; m_RoomIndex = -1; m_Location.Zero(); m_Direction.Zero(); m_Color.Zero(); m_LightType = LIGHT_AMBIENT; m_Intensity = 0.0f; m_SpotAngle = 0.0f; m_CastShadowRange = 0.0f; }

	virtual rexObject* CreateNew()   { return new rexObjectGenericLight; }

	enum LightType
	{
		LIGHT_DIRECTIONAL,
		LIGHT_SPOTLIGHT,
		LIGHT_AMBIENT,
		LIGHT_POINT,
	};

	int			m_BoneIndex, m_RoomIndex;
	Vector3		m_Location, m_Direction;
	Vector4		m_Color;
	LightType	m_LightType;
	float		m_Intensity;
	float		m_SpotAngle;
	float		m_CastShadowRange;
	int			m_DecayRate;		// 0=none, 1=linear, 2=quadratic, 3=cubic -- ie 1/pow(distance,m_DecayRate)
};

class rexObjectGenericLightGroup : public rexObject
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectGenericLightGroup; }
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
