// 
// rexGeneric/objectbound.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Generic Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectGenericBound -- class with data suitable for describing a bound
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_GENERIC_BOUND_H__
#define __REX_GENERIC_BOUND_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/object.h"

#include "atl/array.h"
#include "atl/string.h"

#include "rexGeneric/objectMesh.h"

#include "phBound/bound.h"
#include "phbullet/CollisionMargin.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericBound : public rexObjectGenericMesh
{
public:
	rexObjectGenericBound() : rexObjectGenericMesh(),
		m_Type(phBound::INVALID_TYPE),
		m_Radius(0.0f),
		m_Radius2(0.0f),
		m_Width(0.0f),
		m_Height(0.0f),
		m_Depth(0.0f),
		m_Margin(CONVEX_DISTANCE_MARGIN),
		m_CapCurve(0.0f),
		m_SideCurve(0.0f),
		m_ParentBoneName("DummyBoneName"),
		m_IsClothCollision(false)
	{}

	virtual bool IsValidMesh() const	{ return m_IsValid && ( m_Type == phBound::GEOMETRY ); }
	const atString &GetParentBoneName() const	{ return m_ParentBoneName; }

public:
	phBound::BoundType	m_Type;
	atString			m_ParentBoneName;
	bool				m_IsClothCollision;
	float				m_Radius;		// sphere radius, capsule radius, cylinder radius1
	float				m_Radius2;		// cylinder radius2
	float				m_Width;		// box width
	float				m_Height;		// box height, capsule length, cylinder length
	float				m_Depth;		// box depth
	float				m_Margin;		// bullet collision system margin
	float				m_CapCurve;		// cylinder cap curvature (0..1)
	float				m_SideCurve;	// cylinder side curvature (0..1)
};

/////////////////////////////////////////////////////////////////////////////////////

class rexObjectGenericBoundHierarchy : public rexObjectGenericMeshHierarchy
{
	// the contained objects are all rexObjectGenericBound or rexObjectGenericLODInfo
	// thresholds, when present, have indices in same order as m_ContainedObjects
public:
	rexObjectGenericBoundHierarchy() : rexObjectGenericMeshHierarchy() { }

	virtual rexObject* CreateNew()   { return new rexObjectGenericBoundHierarchy; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
