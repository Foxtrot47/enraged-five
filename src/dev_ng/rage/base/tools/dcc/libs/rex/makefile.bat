call ..\rex\setmayapath.bat
set XDEFINE=NT_PLUGIN
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\tools\dcc\maya %RAGE_DIR%\base\tools\dcc\libs
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% curve cranimation phbound phcore phbullet parser init
set LIBS=%LIBS% rexBase rexGeneric rexMaya rexRAGE rexMayaRAGE rexMayaToGeneric
set LIBS=%LIBS% devil libtiff
set TESTER_BASE=rex_maya60
set TESTERS=REX
set RESOURCES=rexMemory.h