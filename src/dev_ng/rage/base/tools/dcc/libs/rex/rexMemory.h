// rexMemory.h

#ifndef _REX_MEMORY_H_
#define _REX_MEMORY_H_

#define USE_RAGE_ALLOCATORS 0

#if USE_RAGE_ALLOCATORS

#include "system/rageroot.h"
#include "system/memory.h"
#include "system/new.h"
#include "data/struct.h"
#include "diag/tracker.h"
#include "system/stockAllocator.h"

static void InitGameHeap() 
{
	// This bit of terror prevents the stock allocator from ever being destroyed
	// on shutdown, so we are immune to object cleanup order.
	static char theAllocatorBuffer[sizeof(::rage::stockAllocator)];
	::rage::sysMemAllocator& theAllocator = *(::new (theAllocatorBuffer) ::rage::stockAllocator);

	::rage::sysMemAllocator::SetCurrent(theAllocator);
	::rage::sysMemAllocator::SetMaster(theAllocator);
}


#if __WIN32
#pragma warning(disable: 4073)
#pragma init_seg(lib)
#endif
static struct InitGameHeap_t {
	InitGameHeap_t() { 
		InitGameHeap(); 
		rage::sysMemAllocator::GetCurrent().BeginLayer();
	}
	~InitGameHeap_t() {
		// Don't do leak dumps if byte swapping, there are too many things we can't clean up properly.
		using namespace rage;
#if __WIN32PC
		if (!g_ByteSwap) {
#endif
			rage::sysMemAllocator::GetCurrent().EndLayer("global",NULL);
#if __WIN32PC
		}
#endif

	}
} InitGameHeapInstance;

#endif //USE_RAGE_ALLOCATORS

#endif //_REX_MEMORY_H_

