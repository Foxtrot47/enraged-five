//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// REX Maya Entry Point
//
// This file hooks the REX Wrapper up to Maya
// 
//////////////////////////////////////////////////////////////////////////////////////////////////////

//#pragma warning(push)
//#if _MSC_VER >= 1400
//#pragma warning(disable: 4005) // warning C4005: 'strcasecmp' : macro redefinition
//							   // rage\base\src\string\string.h declares strcasecmp, and so does MTypes.h
//							   // but they are currently differenct
//#endif

#include "system/param.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include "rexMayaRAGE/wrapper.h"
#pragma warning(pop)

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

// specify additional libraries to link to
#pragma comment(lib, "Foundation.lib")
#pragma comment(lib, "OpenMaya.lib")
#pragma comment(lib, "OpenMayaAnim.lib")
#pragma comment(lib, "OpenMayaUI.lib")
#pragma comment(lib, "OpenGL32.lib")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// this part allows us to register the MLL (maya plugin)
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MFnPlugin.h>
#include <maya/MSceneMessage.h>
#pragma warning(pop)

#pragma warning(push)
#pragma warning (disable:4324)
#include "rexMaya/mayaUtility.h"
#pragma warning(pop)

#include "cranimation/animation.h"

#include "rexMemory.h"

void killPlugin( void * ) 
{
	rage::rexMayaUtility::EndRootSkeleton();
}

MStatus __declspec(dllexport) initializePlugin( MObject obj )
{
	MStatus   status;
    MFnPlugin plugin( obj, "REX RAGE Exporter", RAGE_RELEASE_STRING " - " __DATE__ " - " __TIME__, "Any");

	rage::sysParam::Init(0,NULL);

	// Initialize Animation System:
	rage::crAnimation::InitClass();

	status = plugin.registerCommand( "REX", rage::rexWrapperMayaRAGE::creator );

	// Tell MAYA to let us know when it shuts down
	MSceneMessage::addCallback( MSceneMessage::kMayaExiting, killPlugin );

    return status;
}

MStatus __declspec(dllexport) uninitializePlugin( MObject obj )
{
	MStatus   status;
	MFnPlugin plugin( obj );

	// Shutdown Animation System:
	rage::crAnimation::ShutdownClass();

	status = plugin.deregisterCommand( "REX" );

	if(!status)
	{
		status.perror("deregisterCommand");
	}

	return status;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
#if USE_RAGE_ALLOCATORS

void *operator_new(size_t size,size_t align) 
{
	void *result = ::rage::sysMemAllocator::GetCurrent().Allocate(size,align);

	RAGE_TRACKING_TALLY(result,::rage::MEMTYPE_GAME_VIRTUAL);

#if !__FINAL
	if (result == ::rage::sysMemAllocator::sm_BreakOnAddr)
		__debugbreak();
#endif

	return result;
}

void operator_delete(void *ptr) 
{
	if (ptr) 
	{
		RAGE_TRACKING_UNTALLY(ptr);
		::rage::sysMemAllocator::GetCurrent().Free(ptr);
	}
}

void* operator new(size_t size) {
	return operator_new(size,16);
}


void* operator new[](size_t size) {
	return operator_new(size,16);
}

void operator delete(void *ptr) {
	operator_delete(ptr);
}

void operator delete[](void *ptr) {
	operator_delete(ptr);
}
#endif // USE_RAGE_ALLOCATORS
//////////////////////////////////////////////////////////////////////////////////////////////////////

