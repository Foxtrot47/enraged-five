// 
// rageRst/shaderplugin.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "shaderplugin.h"

#include "file/asset.h"
#include "system/xtl.h"

using namespace rage;

rageRstShaderPlugin *rageRstShaderPlugin::sm_This = 0;

rageRstShaderPlugin::rageRstShaderPlugin() : m_RageLoop(false, false) {
	Assert(sm_This == 0);
	sm_This = this;
	m_RageLoop.EnableRender(true);
	ASSET.SetPath("");
	// Create a hidden window class
	WNDCLASS c;
	c.lpszClassName = "RAGE Hidden Window Class";
	c.lpfnWndProc = DefWindowProc;
	c.hInstance = 0;//GetCurrentProcess();
	c.hCursor = 0;
	c.hIcon = 0;
	c.hbrBackground = 0;
	c.lpszMenuName = 0;
	c.style = 0;
	c.cbClsExtra = 0;
	c.cbWndExtra = 0;
	m_HiddenWindowAtom = RegisterClass(&c);
	// Create the hidden window (just in case)
//	Displayf("Atom -- %d", m_HiddenWindowAtom);
	m_HiddenWindowHandle = (u32) CreateWindow((const char *)m_HiddenWindowAtom,"Rage Hidden Window",0, 0, 0, 256, 256,0, 0, 0, 0);
	SendMessage((HWND) m_HiddenWindowHandle, WM_SHOWWINDOW, 0, 0);
//	Displayf("Handle -- %d", m_HiddenWindowHandle);
	m_RageLoop.Init(m_HiddenWindowHandle, 0, true);
	m_ShaderList = new rageRstShaderList;
	m_ShaderDb = new rageRstShaderDb;
}

rageRstShaderPlugin::~rageRstShaderPlugin() {
	sm_This = 0;
	
	rageRstRageLoopCriticalSection cs;
	delete m_ShaderDb;
	delete m_ShaderList;
	cs.Exit();
	
	m_RageLoop.Shutdown();
	if ( m_HiddenWindowAtom ) {
		SendMessage((HWND) m_HiddenWindowHandle, WM_CLOSE, 0, 0);
		m_HiddenWindowHandle = 0;
		UnregisterClass((const char *)m_HiddenWindowAtom, 0);
		m_HiddenWindowAtom = 0;
	}
	
}

