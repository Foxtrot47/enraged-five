// 
// rst/shaderplugin.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RST_SHADERPLUGIN_H
#define RST_SHADERPLUGIN_H

#include "rageloop.h"
#include "shaderdb.h"
#include "shaderlist.h"

namespace rage {

class rageRstShaderPlugin {
public:
	rageRstShaderPlugin();
	~rageRstShaderPlugin();

	// PURPOSE: Set the root path of the shader database
	void SetShaderDbPath(const char *path)	{ m_ShaderDb->SetShaderDbPath(path); }

	rageRstShaderList *GetShaderList()			{ return m_ShaderList; }
	rageRstRageLoop *GetRageLoop()				{ return &m_RageLoop; }
	rageRstShaderDb *GetShaderDb()				{ return m_ShaderDb; }

	u32	GetHiddenWindow() const					{ return m_HiddenWindowHandle; }

	static rageRstShaderPlugin *GetInstance()	{ return sm_This; }

protected:
	u32						m_HiddenWindowAtom;
	u32						m_HiddenWindowHandle;
	rageRstShaderList		*m_ShaderList;
	rageRstShaderDb			*m_ShaderDb;
	rageRstRageLoop			m_RageLoop;

	static rageRstShaderPlugin *sm_This;
};

}	// namespace rage


#endif // RST_SHADERPLUGIN_H
