// 
// rageRst/rageloop.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rageloop.h"
#include "shaderplugin.h"

#include "atl/string.h"
#include "atl/array.h"
#include "grcore/effect.h"
#include "grcore/font.h"
#include "grmodel/setup.h"
#include "math/random.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "system/param.h"
#include "system/timer.h"

#if __WIN32
#include  <io.h>
#include "system/xtl.h"
#endif

using namespace rage;

sysCriticalSectionToken	rageRstRageLoop::sm_UpdateDrawToken;
bool						rageRstRageLoop::sm_AutoUpdate = true;
bool						rageRstRageLoop::sm_AutoRender = true;
volatile bool				rageRstRageLoop::sm_ExitThread = false;
sysIpcSema					rageRstRageLoop::sm_ThreadActivate;
sysIpcSema					rageRstRageLoop::sm_ThreadDead;
sysIpcSema					rageRstRageLoop::sm_WaitForInit;
long ( __stdcall *rageRstRageLoop::sm_DefaultWndProc)(::HWND__ *, unsigned, unsigned, long);
rageRstRageLoop *				rageRstRageLoop::sm_This = 0;

rageRstRageLoop::rageRstRageLoop(bool initUpdate, bool initRender) {
	Assert("Only one instance allowed" && sm_This == 0);
	sm_This = this;
	m_UpdateThread = 0;
	m_Window = 0;
	m_InitUpdate = initUpdate;
	m_InitDraw = initRender;
}

rageRstRageLoop::~rageRstRageLoop() {
	if ( IsWindowActive() )
		Shutdown();
	sm_This = 0;
}

void rageRstRageLoop::Init(u32 attachWindowHandle, u32 parentWindowHandle, bool wantDevice) {
#if __WIN32
	AttachToWindow( attachWindowHandle, parentWindowHandle );
#endif

	sm_AutoUpdate = m_InitUpdate;
	sm_AutoRender = m_InitDraw;

	XPARAM(nographics);
	wantDevice ? PARAM_nographics.Set(0) : PARAM_nographics.Set("");
	
	sm_ExitThread = false;

	// Create an activation sema for when we want to wake it up
	sm_ThreadActivate = sysIpcCreateSema(false);
	sm_ThreadDead = sysIpcCreateSema(false);

	sm_WaitForInit = sysIpcCreateSema(false);

	// Create a thread to handle the Update/Draw & kick it off
	m_UpdateThread = sysIpcCreateThread( HeartBeat, this, 4096, PRIO_LOWEST, "[RAGE] HeartBeat" );
	
	// wait for graphics init before allowing execution to continue - this will prevent users
	// from accessing graphics before the initialization is done:
	sysIpcWaitSema(sm_WaitForInit);
}

void rageRstRageLoop::AttachToWindow(u32 attachWindowHandle, u32 parentWindowHandle) {
	extern HWND g_hwndMain;
	extern HWND g_hwndParent;
	extern HWND g_hwndOverride;
	// See if we need to attach to a window
	if ( attachWindowHandle != 0 ) {
		// Release previous window if already attached
		if ( (long) RageWndProc == GetWindowLong( g_hwndMain, GWL_WNDPROC ) ) {
			SetWindowLong( g_hwndMain, GWL_WNDPROC, (long) sm_DefaultWndProc );
		}
		g_hwndMain = (HWND) attachWindowHandle;
		g_hwndOverride = g_hwndMain;
		// Try to hijack the message pump
		sm_DefaultWndProc = (long ( __stdcall *)(HWND, unsigned, unsigned, long)) GetWindowLong( g_hwndMain, GWL_WNDPROC );
		SetWindowLong( g_hwndMain, GWL_WNDPROC, (long) RageWndProc );
		m_Window = (HWND__ *) g_hwndMain;
	}
	if ( parentWindowHandle != 0 ) {
		g_hwndParent = (HWND) parentWindowHandle;
	}
}

void rageRstRageLoop::Shutdown() {

	EnableRender(false);
	EnableUpdate(false);
	// Wait for update thread to shutdown
	sm_ExitThread = true;
	// Make sure the thread is running
	sysIpcSignalSema(sm_ThreadActivate);
	// Now, wait on thread to shutdown
	sysIpcWaitSema(sm_ThreadDead);

	// Kill the thread semaphore
	sysIpcDeleteSema(sm_ThreadActivate);
	sysIpcDeleteSema(sm_ThreadDead);

	// Restore window proc
	if ( m_Window )
		SetWindowLong( (HWND) m_Window, GWL_WNDPROC, (long) sm_DefaultWndProc );

	m_UpdateThread = 0;
}

void rageRstRageLoop::Update() {
	m_Setup.BeginUpdate();
	ClientUpdate();
	m_Setup.EndUpdate();	
}

void rageRstRageLoop::Draw() {
	m_Setup.BeginDraw(true);
	ClientDraw();
	m_Setup.EndDraw();
}

void rageRstRageLoop::EnableUpdate(bool update) {
	sysCriticalSection cs(sm_UpdateDrawToken);
	sm_AutoUpdate = update;
	// Wake up thread if it went into inactive state
	if ( sm_AutoRender == true || sm_AutoUpdate == true )
		sysIpcSignalSema(sm_ThreadActivate);
}

void rageRstRageLoop::EnableRender(bool render) {
	sysCriticalSection cs(sm_UpdateDrawToken);
	sm_AutoRender = render;
	// Wake up thread if it went into inactive state
	if ( sm_ThreadActivate && (sm_AutoRender == true || sm_AutoUpdate == true) )
		sysIpcSignalSema(sm_ThreadActivate);
}

bool rageRstRageLoop::IsWindowActive() {
	return (m_UpdateThread != 0 && sm_ExitThread == false);
}

void rageRstRageLoop::ClientUpdate() {
}

void rageRstRageLoop::ClientDraw() {
	float red = g_DrawRand.GetFloat();
	float green = g_DrawRand.GetFloat();
	float blue = g_DrawRand.GetFloat();

	const grcFont& font=grcFont::GetCurrent();
	font.DrawScaled(280.f, 230.f, 0, Color32(red,green,blue),1,1,"Hello World");

}

DECLARE_THREAD_FUNC( rageRstRageLoop::HeartBeat ) {

	// Create our critical section, but exit it immediately
	sysCriticalSection cs(sm_UpdateDrawToken);
	cs.Exit();	

	INIT_PARSER;

	atString shaderDBPath;

	//Fallback on trying to set the asset path from the RAGE_MODULE_PATH environment variable
	const char *rmp = getenv("RAGE_MODULE_PATH");
	if (rmp) {
		const char *rmpSlash = strchr(rmp+3,'\\');
		char tmpAssets[64];
		safecpy(tmpAssets,rmp,rmpSlash-rmp+2);
		safecat(tmpAssets,"assets\\tune\\shaders\\lib",sizeof(tmpAssets));
		grcEffect::SetDefaultPath(tmpAssets);
		// Displayf("*** Configuring asset path from RAGE_MODULE_PATH env var to [%s]",tmpAssets);
	}

	rageRstRageLoop *task = static_cast<rageRstRageLoop *>(ptr);
	task->m_Setup.Init(shaderDBPath, "");
	task->m_Setup.BeginGfx(true);
	task->m_Setup.CreateDefaultFactories();		

	// initialization is done, let the parent thread continue:
	sysIpcSignalSema(sm_WaitForInit);

	// Main loop
	sysTimer t;
	bool exitNow = sm_ExitThread;
	while ( exitNow == false ) {
		float ms = t.GetMsTime();
		// Clamp update rate to a fixed time to prevent plugin thread from eating all cycles
		// NOTE: Hardcoding to 30fps -- this is easy enough to change later if people need it
		const float cTargetFrameTime = (1.f / 30.f) * 1000.f;
		if ( ms < cTargetFrameTime ) {
			sysIpcSleep((int) (cTargetFrameTime - ms));
		}
		t.Reset();
		// We use the critical section to completely lock out update & draw so that
		//	higher level plugin code can have more worry free access to rage data
		cs.Enter();
		bool update = sm_AutoUpdate;
		bool draw = sm_AutoRender;
		if ( update == false && draw == false ) {
			// If we don't need to update, go into a "sleep" mode until needed
			cs.Exit();
			sysIpcWaitSema( sm_ThreadActivate );
			cs.Enter();
		}
		if ( update ) {
			task->Update();
		}
		if ( draw ) {
			task->Draw();
		}
		cs.Exit();
		exitNow = sm_ExitThread;
	}	

	SHUTDOWN_PARSER;

	// Shut down rage renderer
	task->m_Setup.DestroyFactories();
	task->m_Setup.EndGfx();

	sysIpcSignalSema(sm_ThreadDead);
}

long __stdcall rageRstRageLoop::RageWndProc(::HWND__ *hwnd,unsigned msg,unsigned wParam,long lParam) {
	// Cache this for later use:
	long ( __stdcall *defWndProc)(::HWND__ *, unsigned, unsigned, long) = sm_DefaultWndProc;
	if ( sm_This->IsWindowActive() ) {
		switch (msg) {
			case WM_CLOSE: {
				u32 hiddenWindow = rageRstShaderPlugin::GetInstance()->GetHiddenWindow();
				sysCriticalSection cs(sm_UpdateDrawToken);
				if ( hiddenWindow != (u32) hwnd ) {
					sm_This->AttachToWindow(hiddenWindow, 0);
				}
				break;
			}
			case WM_SHOWWINDOW: {
				sm_This->EnableRender(wParam != 0);
				break;
			}
			default:
				break;
		}
	}
	return defWndProc((HWND) hwnd, msg, wParam, lParam);
}
