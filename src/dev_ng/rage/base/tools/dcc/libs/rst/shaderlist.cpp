// 
// rageRst/shaderlist.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "shaderlist.h"

#include "file/device.h"
#include "file/stream.h"
#include "file/token.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"

using namespace rage;

rageRstShaderList::rageRstShaderList() {
	// EMPTY
}

rageRstShaderList::~rageRstShaderList() {
	ClearNameList();

	// Delete the shaders
	for (int i=0; i<m_Shaders.GetNumSlots(); i++) {
		atMap<ConstString, grcEffect *>::Entry *e = m_Shaders.GetEntry(i);
		while (e) {
			delete e->data;
			e = e->next;
		}
	}
}

void rageRstShaderList::SetShaderPath(const char *path) {
	grmShaderFactory::SetShaderLibPath(path);
}

void rageRstShaderList::BuildShaderList() {
	ASSET.PushFolder(grmShaderFactory::GetShaderLibPath());

	// Gotta 1st support the hack to deal with AB's suckage...
	fiStream *S = ASSET.Open("preload","list");
	ClearNameList();
	if (S) {
		fiTokenizer T(grmShaderFactory::GetShaderLibPath(),S);
		char shaderName[128];
		// Mimic the callback functionality of t
		fiFindData data;
		data.m_LastWriteTime = 0;
		data.m_Attributes = 0;
		data.m_Size = 0;
		while (T.GetToken(shaderName,sizeof(shaderName))) {
			strcpy(data.m_Name, shaderName);
			AddFileToList(data, this);
		}
		S->Close();
	}
	else {
		// Do this the traditional way
		// Clear the old name list so that new path doesn't append
		ASSET.EnumFiles(".", rageRstShaderList::AddFileToList, this);
	}
	ASSET.PopFolder();
}

grcEffect *rageRstShaderList::FindShader(const char *name) {
	char filename[128];
	strcpy(filename, name);
	// Strip off .fx extension for later use
	char *ext = strstr(filename, ".fx");
	if ( ext )
		*ext = '\0';

	grcEffect **foundShader = m_Shaders.Access(filename);
	grcEffect *shader = 0;
	if ( foundShader == 0 ) {
		// Try loading a shader
		shader = grcEffect::Create(filename);
		if (!shader)
			return NULL;
		m_Shaders.Insert(ConstString(filename), shader);
	}
	else {
		shader = *foundShader;
	}

	return shader;
}

void rageRstShaderList::ClearNameList() {
	for (int i = 0; i < m_ShaderNames.GetCount(); ++i) {
		free( m_ShaderNames[i] );
	}
	m_ShaderNames.Reset();
}

void rageRstShaderList::AddFileToList(const fiFindData &data, void *userArg) {
	rageRstShaderList *shaderList = static_cast<rageRstShaderList *> (userArg);
	const char *ext = strchr(data.m_Name,'.');
	if ( ext )
		ext++;
	// See if we need have a shader
	if ( (ext != 0) && ((stricmp(ext,"fx") == 0) || (stricmp(ext,"fx2") == 0) || (stricmp(ext,"shadert") == 0)) ) {
		if ( shaderList->m_ShaderNames.GetCount() >= shaderList->m_ShaderNames.GetCapacity() - 1 ) {
			shaderList->m_ShaderNames.Grow( 32 ) = _strdup(data.m_Name);
		}
		else
			shaderList->m_ShaderNames.Append() = _strdup(data.m_Name);
	}
}

