// 
// rst/dlltask.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RST_DLLTASK_H
#define RST_DLLTASK_H

namespace rage {

// PURPOSE: Abstract base class used to contain the "main loop" functionality
//	of a DLL.  These funcions are virtual and can be used to manually update
//	and draw a particular rage session across a DLL
//	Goal is to make RAGE easier to hook up as a DLL

class rageRstDllTask {
public:
	rageRstDllTask()				{};
	virtual ~rageRstDllTask()		{};
	
	// PURPOSE: Init RAGE, pass in window handles if you want rage to attach
	//	to a particular window, otherwise, leave alone for a new window
	virtual void Init(u32 attachWindowHandle=0xffff, u32 parentWindowHandle=0xffff) = 0;
	// PURPOSE: Kill RAGE -- Init & shutdown should be able to be called multiple 
	//	times within one session
	virtual void Shutdown() = 0;
	
	// Allow the thread to automatically update the task
	virtual void EnableRender(bool render) = 0;
	virtual void EnableUpdate(bool update) = 0;
};

}	// namespace rage
#endif	// RST_DLLTASK_H
