// 
// rageRst/shaderdb.cpp 
// 
// Copyright (C) 1999-2004 Rockstar Games.  All Rights Reserved. 
// 
#include "rst/shaderdb.h"

#include "file/asset.h"
#include "file/device.h"
#include "file/token.h"
#include "grmodel/shader.h"
#include "string/string.h"

#include "MaterialPresets/MaterialPreset.h"
#include "MaterialPresets/MaterialPresetManager.h"
#include "rstMaxMtl/utility.h"

#include <malloc.h>

using namespace rage;

rageRstShaderDb::rageRstShaderDb() {
	// Init with a fairly large size
	m_CurrentItems.Reserve(1024);
	m_LockedEntry = 0;
}

rageRstShaderDb::~rageRstShaderDb() {
	// Kill all of the presets
	for (int i=0; i<m_Presets.GetNumSlots(); i++) {
		atMap<ConstString, rageRstShaderDbEntry *>::Entry *e = m_Presets.GetEntry(i);
		while (e) {
			MATLIB.Destroy(e->data->m_Preset->GetMaterialName());
			delete e->data;
			e = e->next;
		}
	}
}

bool rageRstShaderDb::SetShaderDbPath(const char *path) 
{
	grcMaterialLibrary* pMtlLib = grcMaterialLibrary::Preload(path);

	atString presetErrors = MaterialPresetManager::GetCurrent().GetErrors();
	if(presetErrors.length()>0)
		MessageBox(GetCOREInterface()->GetMAXHWnd(), presetErrors.c_str(), "Material preset library initialisation error", MB_ICONERROR);

	MaterialPresetManager::GetCurrent().SetPresetPath(atString(MaterialPresetManager::GetCurrent().GetMetadataPath()));

	if(pMtlLib)
	{
		grcMaterialLibrary::SetCurrent(pMtlLib);
		return true;
	}
	else
	{
		return false;
	}
}

#if HACK_GTA4
int rageRstShaderDb::BuildEntryList( const char* level, const bool usePresetList ) {
#else
int rageRstShaderDb::BuildEntryList( const char* /*level*/ ) {
#endif // HACK_GTA4
	// Clear existing list
	if ( m_CurrentItems.GetCount() )
		m_CurrentItems.Resize(0);
#if HACK_GTA4	
	fiFindData data;
	if ( usePresetList )
	{
		const char *presetRootPath = MATLIB.GetRootPath() ? MATLIB.GetRootPath() : ".";
		char presetSubPath[256];
		formatf( presetSubPath, sizeof(presetSubPath), "%s%s", presetRootPath, level );

		// Get the list of shaders from the preload.list file first.
		atArray<atString> presetNames;
		ASSET.PushFolder(presetRootPath);

		fiStream *S = ASSET.Open("preload","list");
		if (S) {
			char presetName[128];
			while(fgetline(presetName,sizeof(presetName),S))
				presetNames.PushAndGrow(atString(atString("/"), presetName));

			S->Close();
		}
		ASSET.PopFolder();

		const fiDevice *d = fiDevice::GetDevice(presetSubPath,true);
		if ( d ) {
			fiHandle handle = d->FindFileBegin(presetSubPath,data);
			if (fiIsValidHandle(handle)) {
				do {
					const char *ext = strrchr(data.m_Name, '.');
					int presetIndex = -1;
					if(ext && ( (stricmp(ext,".sps") == 0) || rstMaxUtility::IsPresetExtension(ext)))
					{
						char buffer[256];
						sprintf_s(buffer, 256, "%s/%s", level, data.m_Name);
						presetIndex = presetNames.Find(atString(buffer));
					}
					if (	(data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY || 
						presetIndex!=-1	) 
					{
						ShaderDbItem &item = m_CurrentItems.Grow();
						formatf(item.m_Name, sizeof(item.m_Name), "%s", data.m_Name);
						item.m_IsGroup = (data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY;
					}
				} while (d->FindFileNext(handle,data));
				d->FindFileEnd(handle);
			}
			else {
				Warningf("ShaderDb -- Could not get directory for path %s", presetSubPath);
			}
		}
		else {
			Errorf("ShaderDb -- Device failure on item %s", presetSubPath);
		}
	}
	else
	{
		const fiDevice *d = fiDevice::GetDevice(level ,true);
		if ( d ) {
			fiHandle handle = d->FindFileBegin(level ,data);
			if (fiIsValidHandle(handle)) {
				do {
					const char *ext = strrchr(data.m_Name, '.');
					if(ext && rstMaxUtility::IsPresetExtension(ext))
					{
						char buffer[256];
						sprintf_s(buffer, 256, "%s/%s", level, data.m_Name);
						ShaderDbItem &item = m_CurrentItems.Grow();
						formatf(item.m_Name, sizeof(item.m_Name), "%s", data.m_Name);
						item.m_IsGroup = false;
					}
					else if ((data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
					{
						ShaderDbItem &item = m_CurrentItems.Grow();
						formatf(item.m_Name, sizeof(item.m_Name), "%s", data.m_Name);
						item.m_IsGroup = true;
					}
				} while (d->FindFileNext(handle,data));
				d->FindFileEnd(handle);
			}
			else {
				Warningf("ShaderDb -- Could not get directory for path %s", level);
			}
		}
		else {
			Errorf("ShaderDb -- Device failure on item %s", level);
		}
	}
	
#else
	int matCount = MATLIB.GetCount();
	for(int i=0; i<matCount; i++)
	{
		grcInstanceData* pData = MATLIB.GetEntry(i);
		ShaderDbItem& item = m_CurrentItems.Grow();
		formatf(item.m_Name, sizeof(item.m_Name), "%s", pData->GetMaterialName());
		item.m_IsGroup = false;
	}
#endif // HACK_GTA4	
	return m_CurrentItems.GetCount();
}

bool rageRstShaderDb::CreateGroup( const char *folder ) {
	char buff[256];
	formatf( buff, sizeof(buff), "%s/%s", MATLIB.GetRootPath() ? MATLIB.GetRootPath() : ".", folder );
	const fiDevice *d = fiDevice::GetDevice(buff,false);
	bool retVal = false;
	if ( d ) {
		retVal = d->MakeDirectory(buff);
	}
	if ( retVal == false ) {
		Errorf("Could not create group -- %s", folder);
	}
	return retVal;
}

bool rageRstShaderDb::DeleteGroup( const char *folder ) {
	char buff[256];
	formatf( buff, sizeof(buff), "%s/%s", MATLIB.GetRootPath() ? MATLIB.GetRootPath() : ".", folder );
	const fiDevice *d = fiDevice::GetDevice(buff,false);
	bool retVal = false;
	if ( d ) {
		retVal = d->DeleteDirectory(buff, true, false);
	}
	if ( retVal == false ) {
		Errorf("Could not delete group -- %s", folder);
	}
	return retVal;
}

bool rageRstShaderDb::CreateEntry( const char *name, const char *shaderName, BOOL saveToPeloadFile, const char *pComment ) {
	bool saveSuccess = true;
	bool presetAlreadyRegistered = false;
		
	grcInstanceData *preset = MATLIB.Create( name, shaderName );
	if( !preset )
		return false;

	if(pComment && strlen(pComment))
		preset->Comment = StringDuplicate(pComment);

	int entryIndex = MATLIB.LookupLocalIndex(atStringHash(name));
	if(entryIndex!=-1)
	{
		grcInstanceData *oldPreset = MATLIB.GetEntry(entryIndex);
		preset->TryMapVariablesFrom(*oldPreset);
		MATLIB.SetEntryUnsafe(entryIndex, preset);
		// replacing material lib entry
		rageRstShaderDbEntry *entry = GetItem( name );
		Assert(entry);
		entry->m_Preset = preset;
		delete oldPreset;
	}
	else
	{
		saveSuccess = MATLIB.AddNewEntry(name,preset);
		if(!saveSuccess)
		{
			MATLIB.DeleteEntry(name);
			return false;
		}
		
		rageRstShaderDbEntry *pEntry = CreateEntryForPreset(preset);
		if(!pEntry)
		{
			MATLIB.DeleteEntry(name);
			return false;
		}
	}

	saveSuccess = MATLIB.Save(preset->GetMaterialName());
	if(!saveSuccess)
	{
		DeleteEntry(name);
		MATLIB.DeleteEntry(name);
		return false;
	}

	if(saveSuccess && saveToPeloadFile)
	{
		saveSuccess = MATLIB.SaveToPreloadFile(name);
	}
	if(!saveSuccess)
	{
		DeleteEntry(name);
		MATLIB.DeleteEntry(name);
		return false;
	}

	return saveSuccess;
}

bool rageRstShaderDb::DeleteEntry( const char *name ) {
	bool retVal = false;
	char buff[256];
	formatf( buff, sizeof(buff), "%s/%s", MATLIB.GetRootPath() ? MATLIB.GetRootPath() : ".", name );
	const fiDevice *d = fiDevice::GetDevice(buff,false);
	if ( d ) {
		retVal = d->Delete(buff);
	}
	rageRstShaderDbEntry *entry = GetItem( name );
	if ( entry && retVal ) 
	{
		char buff[2048];
		StringNormalize(buff, name, sizeof(buff));
		m_Presets.Delete( buff );
		MATLIB.DeleteEntry(name);
		delete entry;
	}
	return retVal;
}

bool rageRstShaderDb::LockEntry( const char *name ) {
	if ( m_LockedEntry != 0 ) 
	{
		Warningf("Must first unlock previous entry");
		return false;
	}

	rageRstShaderDbEntry *entry = GetItem( name );
	bool retVal = false;
	if ( entry ) 
	{
		ASSET.PushFolder(MATLIB.GetRootPath());
		const char *ext = "sps";
		const char *fileExt = strrchr(name, '.');
		if(fileExt)
		{
			ext = fileExt;
		}
		u32 attrs = ASSET.GetAttributes(name, ext);
		ASSET.PopFolder();
		if(0 == (attrs & FILE_ATTRIBUTE_READONLY))
			retVal = MATLIB.Reload( name );
	}
	else 
	{
		Warningf("Could not find entry for %s", name);
	}
	if ( retVal ) 
	{
		m_LockedEntry = entry;
	}
	return retVal;
}

bool rageRstShaderDb::ReloadEntry( const char *name )
{
	rageRstShaderDbEntry *entry = GetItem( name );
	return MATLIB.Reload( name );
}

bool rageRstShaderDb::UnlockEntry( const char *name, bool acceptChanges ) {
	rageRstShaderDbEntry *entry = GetItem( name );
	if ( entry && entry != m_LockedEntry ) {
		Warningf("Can't unlock an entry that's not locked");
		entry = 0;
	}
	bool retVal = false;
	if ( entry ) 
	{
		if ( acceptChanges == false ) 
		{
			// Reload defaults
			retVal = MATLIB.Reload(entry->m_Preset->GetMaterialName());
		}
		else 
		{
			retVal = MATLIB.Save(entry->m_Preset->GetMaterialName());
		}
	}
	if ( retVal ) 
	{
		m_LockedEntry = 0;
	}
	return retVal;
}

rageRstShaderDbEntry *rageRstShaderDb::GetItem( const char *entryName ) {
	char buff[2048];
	StringNormalize(buff, entryName, sizeof(buff));
	rageRstShaderDbEntry **entryPtr = m_Presets.Access( buff );
	rageRstShaderDbEntry *entry = 0;
	if ( entryPtr == 0 ) 
	{
		if ( MaterialPresetManager::IsMaterialPreset(entryName))
		{
			entry = CreateEntryForPreset( atString(entryName) );
		}
		else
		{
			grcInstanceData *preset = MATLIB.LookupLocal(buff);
			if ( preset ) 
			{
				entry = CreateEntryForPreset( preset );
			}
		}
	}
	else {
		entry = *entryPtr;
	}
	return entry;	
}

rageRstShaderDbEntry *rageRstShaderDb::CreateEntryForPreset( atString& presetName )
{
	rageRstShaderDbEntry *entry = new rageRstShaderDbEntry;
	entry->m_MaterialPreset = MaterialPresetManager::GetCurrent().LoadPresetFromLibrary(presetName);
	entry->m_PresetName = presetName;

	if ( entry->m_MaterialPreset == NULL )
		return NULL;

	atString spsFile(entry->m_MaterialPreset->GetShaderName());
	spsFile += ".sps";

	grcInstanceData *preset = MATLIB.LookupLocal(spsFile.c_str());
	if ( preset != NULL )
	{
		grcEffect *shader = &preset->GetBasis();
		if ( shader ) 
		{
			entry->m_Preset = preset;
		}
	}
	
	char buff[2048] = {0};
	StringNormalize(buff, presetName.c_str(), sizeof(buff));
	m_Presets.Insert( ConstString(buff), entry );

	return entry;
}

rageRstShaderDbEntry *rageRstShaderDb::CreateEntryForPreset( grcInstanceData *preset ) {
	rageRstShaderDbEntry *entry = new rageRstShaderDbEntry;
	entry->m_Preset = preset;
	grcEffect *shader = &entry->m_Preset->GetBasis();
	if ( shader ) {
		entry->m_TotalVariableCount = shader->GetInstancedVariableCount();
		int dbOnlyCount = 0;
		for (int i=0; i < entry->m_TotalVariableCount; ++i) {
			grmVariableInfo info;
			shader->GetInstancedVariableInfo(i, info);
			if ( info.m_IsMaterial )
				dbOnlyCount++;
		}
		entry->m_DbOnlyCount = dbOnlyCount;
	}

	char buff[2048] = {0};
	StringNormalize(buff, preset->GetMaterialName(), sizeof(buff));
	m_Presets.Insert( ConstString(buff), entry );

	return entry;
}
