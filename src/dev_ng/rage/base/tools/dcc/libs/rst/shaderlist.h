// 
// rst/shaderlist.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RST_SHADERLIST_H
#define RST_SHADERLIST_H

#include "atl/array.h"
#include "atl/map.h"
#include "file/asset.h"
#include "string/string.h"

namespace rage {

class grcEffect;
struct fiFindData;

class rageRstShaderList {
public:
	rageRstShaderList();
	~rageRstShaderList();

	void SetShaderPath(const char *path);
	void BuildShaderList();
	inline s32 GetShaderCount() const;
	inline const char *GetShaderName(s32 idx) const;

	// Looks up a shader, if it doesn't exist, it will load it
	grcEffect *FindShader(const char *name);
protected:
	void ClearNameList();

	// Called by file enumeration code
	static void AddFileToList(const fiFindData &data,void *userArg);
	
	atArray<char *>	m_ShaderNames;
	atMap<ConstString, grcEffect *> m_Shaders;
};

s32 rageRstShaderList::GetShaderCount() const {
	return m_ShaderNames.GetCount();
}

const char *rageRstShaderList::GetShaderName(s32 idx) const {
	return m_ShaderNames[idx];
}

}	// namespace rage

#endif	// RST_SHADERLIST_H
