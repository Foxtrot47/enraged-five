// 
// rst/shaderdb.h 
// 
// Copyright (C) 1999-2004 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RST_SHADERDB_H
#define RST_SHADERDB_H

#include "atl/array.h"
#include "atl/string.h"
#include "atl/map.h"

class MaterialPreset;

namespace rage {

#define MATLIB		(*grcMaterialLibrary::GetCurrent())

class grcInstanceData;

struct rageRstShaderDbEntry {

	rageRstShaderDbEntry() : m_MaterialPreset(NULL), m_Preset(NULL) { } 
	
	grcInstanceData*	m_Preset;
	MaterialPreset*		m_MaterialPreset;
	atString			m_PresetName;
	atString			m_FullPath;
	s32					m_TotalVariableCount;
	s32					m_DbOnlyCount;

	bool				IsMaterialPreset() { return m_MaterialPreset != NULL; }
};

class rageRstShaderDb {
public:
	rageRstShaderDb();
	~rageRstShaderDb();
	
	// PURPOSE: Build up the top level info for the shader entries for the current level
	// PARAMS: level - the "relative path" to the current level of the database (from the database root)
	// RETURNS: The number of items in this level of the database
	int BuildEntryList( const char *level, const bool usePreloadList );

	// PURPOSE: Set the root path of the shader database
	// PARAMS: path - the explicit path to the root of the shader database
	bool SetShaderDbPath(const char *path);

	// PURPOSE: Create a new group of preset entries
	// PARAMS: folder - complete relative path from the db root to the folder to create
	bool CreateGroup( const char *folder );

	// PURPOSE: Delete an existing group of preset entries
	// PARAMS: folder - complete relative path from the db root to the folder to create
	bool DeleteGroup( const char *folder );

	// PURPOSE: Create a new preset entry
	// PARAMS: name - complete relative path + name of shader entry
	//			shaderName - rage shader name to use for entry
	bool CreateEntry( const char *name, const char *shaderName, BOOL saveToPeloadFile=FALSE, const char *pComment=NULL );

	// PURPOSE: Delete an entry preset
	// PARAMS: name - complete relative path + name of shader entry
	bool DeleteEntry( const char *name );

	// PURPOSE: Reload an entry preset from disc
	// PARAMS: name - complete relative path + name of shader entry
	bool ReloadEntry( const char *name );

	// PURPOSE: Lock an entry in the database
	// PARAMS: name - comple relative path + name of shader entry
	// RETURNS: true on success, false on failure (entry doesn't exist, file already locked, etc.)
	bool LockEntry( const char *name );

	// PURPOSE: Unlock an entry in the database and make it live
	// PARAMS: name - comple relative path + name of shader entry
	//			acceptChanges - whether changes should be made persistent or not
	// RETURNS: true on success, false on failure (entry doesn't exist, file already locked, etc.)
	bool UnlockEntry( const char *name, bool acceptChanges );

	// RETURNS: The RAGE shader preset matching the specified name
	// PARAMS: entryName - the name of the preset's entry within the database 
	//	(including relative pathing from root)
	rageRstShaderDbEntry *GetItem( const char *entryName );

	// RETURNS: The name of the specified shader db item
	inline const char *GetEntryName( int idx ) const;

	// RETURNS: True if specified shader entry actually refers to a new group of shaders
	inline bool IsGroup( int idx ) const;

	// RETURNS: True if specified entry is currently locked
	inline bool IsLocked( const rageRstShaderDbEntry *entry ) const;

protected:
	rageRstShaderDbEntry *CreateEntryForPreset( grcInstanceData *preset );
	rageRstShaderDbEntry *CreateEntryForPreset( atString& presetName );

	struct ShaderDbItem {
		ShaderDbItem() : m_IsGroup(false) { /*EMPTY*/ }
		char m_Name[256];
		bool m_IsGroup;
	};

	atArray<ShaderDbItem>						m_CurrentItems;
	atMap<ConstString, rageRstShaderDbEntry *>	m_Presets;
	const char *								m_ShaderDbPath;
	rageRstShaderDbEntry *						m_LockedEntry;
};

inline const char *rageRstShaderDb::GetEntryName( int idx ) const {
	return m_CurrentItems[idx].m_Name;
}

inline bool rageRstShaderDb::IsGroup( int idx ) const {
	return m_CurrentItems[idx].m_IsGroup;
}

inline bool rageRstShaderDb::IsLocked( const rageRstShaderDbEntry *entry ) const {
	return (entry == m_LockedEntry);
}

}		// namespace rage

#endif	// RST_SHADERDB_H
