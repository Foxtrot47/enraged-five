// 
// rst/rageloop.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RST_SIMPLETASK_H
#define RST_SIMPLETASK_H

#include "grmodel/setup.h"
#include "system/criticalsection.h"
#include "system/wndproc.h"

namespace rage {

class rageRstRageLoopCriticalSection;

// PURPOSE: Provide bare bones RAGE update & draw functionality
class rageRstRageLoop {
friend rageRstRageLoopCriticalSection;
public:
	rageRstRageLoop(bool initUpdate = true, bool initRender = true);
	virtual ~rageRstRageLoop();

	virtual void Init(u32 attachWindowHandle=0, u32 parentWindowHandle=0, bool wantDevice=true);
	virtual void AttachToWindow(u32 attachWindowHandle=0, u32 parentWindowHandle=0);
	virtual void Shutdown();
	virtual void EnableRender(bool render);
	virtual void EnableUpdate(bool update);

	virtual bool IsWindowActive();

protected:
	virtual void Update();
	virtual void Draw();

	virtual void ClientUpdate();
	virtual void ClientDraw();

	static DECLARE_THREAD_FUNC( HeartBeat );

	// We need to watch the windows message pump to see when the user closes it
	static long __stdcall RageWndProc(::HWND__ *hwnd,unsigned msg,unsigned wParam,long lParam);

	sysIpcThreadId	m_UpdateThread;
	::HWND__ *		m_Window;
	grmSetup 		m_Setup;
	bool			m_InitUpdate;
	bool			m_InitDraw;

	// Thread related data
	static sysCriticalSectionToken	sm_UpdateDrawToken;
	static bool							sm_AutoUpdate;
	static bool							sm_AutoRender;
	static volatile bool				sm_ExitThread;
	static sysIpcSema					sm_ThreadActivate;
	static sysIpcSema					sm_ThreadDead;
	static sysIpcSema					sm_WaitForInit;
	static long ( __stdcall *sm_DefaultWndProc)(::HWND__ *, unsigned, unsigned, long);

	// Window proc related data
	static rageRstRageLoop					*sm_This;
};

// PURPOSE: Use this critical section in every function you use to interface with
//	data touched in the update/draw portions of the code.  This will prevent thread
//	syncing issues
class rageRstRageLoopCriticalSection : public sysCriticalSection {
public:
	rageRstRageLoopCriticalSection() : sysCriticalSection(rageRstRageLoop::sm_UpdateDrawToken) {}
};

}	// namespace rage

#endif	// RST_SIMPLETASK_H
