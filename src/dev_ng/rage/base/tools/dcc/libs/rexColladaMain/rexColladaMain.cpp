/*
* $Id$
*
* Sample, output the content of a Collada file in the TTY
*
* Copyright Sony Computer Entertainment Inc. 2005
* All Rights Reserved.
*/

#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <dae.h>
#include <dom/domCOLLADA.h>
#pragma warning(pop)
#pragma comment(lib, "../devil/libtiff.lib")
#include "intRegisterElements.h"
#include "rexGeneric/objectMesh.h"
#include "rexGeneric/objectHierarchy.h"
#include "rexRage/serializerMesh.h"
#include "rexBase/rexReport.h"
#include "rageCollada/rageColladaScene.h"
#include "rageCollada/rageRolladaScene.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"
#include "rexBase/rexTimerLog.h"
#include "rexObjectGenericScene.h"
#include <stdio.h>

#define ATARRAY_GROW_SIZE 3000

//#define SAMPLE_FILE "T:/temp/ColladaLand/Arrow2_Animated.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Texturing7_Bump.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Triangle.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TwoTrianglesFormingAQuad.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TwoTrianglesFormingAQuadWithVertexFaceNormals.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Cube.xml.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/CrazyPoly.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TwoTriangles.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/DonutsWithBakedTransforms.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/DonutsWithoutBakedTransforms.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Text.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Brothel.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/rageGirl.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Jasper.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Mini.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Gun.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/SimpleSkeleton1.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TexturedQuadsWithTwoTextures.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/ColourTriangle.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TexturedQuad.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TexturedQuadWithTwoTextures.dae"
//#define SAMPLE_FILE "C:/soft/rage/base/tools/COLLADA_DOM/samples/data/cube.dae"
//#define SAMPLE_FILE "../../../../../soft/rage/base/tools/COLLADA_DOM/samples/data/cube.dae"
//#define SAMPLE_FILE "../COLLADA_DOM/samples/data/cube.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/MultipleUV3.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Texturing7_Bump.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Texturing3.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/f_ott_bh_lng_nr_002_PH.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_BMW3.DAE"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_City.DAE"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3Ds_Hippo.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_K9.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_LivingRoom.DAE"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_Ruby.DAE"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_Yacht.DAE"
//#define SAMPLE_FILE "T:/temp/ColladaLand/MC4Block.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Prostitute001_ready.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Prostitute001_ready.rollada"
//#define SAMPLE_FILE "T:/temp/ColladaLand/streetcorner.dae"
#define SAMPLE_FILE "T:/temp/ColladaLand/streetcorner.rollada"
#define REPORT printf

using namespace rage;

void printError(int res);
void printUsage();
void printDomObject(daeElement *pDomObj);

int giTotalNoOfVerts = 0;
int giTotalNoOfPolys = 0;

string SourceTexturePathAndFilenameToExportedTexturePathAndFilename(const string& strSourcePathAndFilename)
{
	// printf("SourceTexturePathAndFilenameToExportedTexturePathAndFilename(\"%s\")\n", strSourcePathAndFilename.c_str());
	// Convert input texture into an exported texture
	const char* pcPosOfLastDot = strrchr(strSourcePathAndFilename.c_str(), '.');
	const char* pcPosOfLastSlash = strrchr(strSourcePathAndFilename.c_str(), '/') + 1;
	char acExportedTextureFilename[255];
	for(int i=0; i<255; i++) acExportedTextureFilename[i] = '\0';
	if(pcPosOfLastSlash && pcPosOfLastDot)
	{
		strncpy(acExportedTextureFilename, pcPosOfLastSlash, (pcPosOfLastDot - pcPosOfLastSlash));
	}
	else
	{
		return strSourcePathAndFilename;
	}
	// printf("returning \"%s\"\n", acExportedTextureFilename);
	return string(acExportedTextureFilename);
}

rexObjectGenericMesh::MaterialInfo CreateRexGenericMaterial(const rageColladaMaterial* pobRageColladaMaterial, const rageRolladaScene* pobRageRolladaScene = NULL)
{
	rexObjectGenericMesh::MaterialInfo obReturnMe;

	// Do I have anything to work from?
	if(pobRageColladaMaterial)
	{
		// Ok, I have a Collada material, is there a Rollada version or it?
		string strColldadaMaterialName = pobRageColladaMaterial->GetName();
		obReturnMe.m_Name = strColldadaMaterialName.c_str();

		// Is there Rollada data to work from?
		if(pobRageRolladaScene)
		{
			const rageRolladaNode*	pobRolladaMaterial	= pobRageRolladaScene->GetRolladaNode(strColldadaMaterialName.c_str());
			if(pobRolladaMaterial)
			{
				// I have a Rollada version of the material, but is it any better than the Collada version?
				// It is "better" if it have Rage Material Attributes, these out-rank the Collada version
				const rageRolladaParam*	pobRolladaAssignedPresetParam = pobRolladaMaterial->GetRolladaParam("Assigned_Preset");
				if(pobRolladaAssignedPresetParam)
				{
					// Bingo, the RolladaMaterial has a Rage Preset, so it out ranks the collada version
					obReturnMe.m_TypeName = pobRolladaAssignedPresetParam->GetValue().c_str();

					// Get number of variables
					const rageRolladaParam*	pobRolladaVarCountParam = pobRolladaMaterial->GetRolladaParam("__rs__varcount");
					Assertf(pobRolladaVarCountParam, "Unable to find number of Material Variables for %s on shader %s", pobRolladaAssignedPresetParam->GetValue().c_str(), pobRageColladaMaterial->GetName());

					int iNoOfVariables = atoi(pobRolladaVarCountParam->GetValue().c_str());
					for(int iVarNo = 0; iVarNo<iNoOfVariables; iVarNo++)
					{
						// Get all the details of this variables
						char acVarAttributePrefix[255];
						sprintf(acVarAttributePrefix, "__rs__var%d_", iVarNo);

						// Name
						const rageRolladaParam*	pobRolladaVarNameParam = pobRolladaMaterial->GetRolladaParam((string(acVarAttributePrefix) +"name").c_str());
						Assertf(pobRolladaVarNameParam, "Unable to find name of shader variable %d on %s on shader %s", iVarNo, pobRolladaAssignedPresetParam->GetValue().c_str(), pobRageColladaMaterial->GetName());
						string strVarName = pobRolladaVarNameParam->GetValue();

						// Type
						const rageRolladaParam*	pobRolladaVarTypeParam = pobRolladaMaterial->GetRolladaParam((string(acVarAttributePrefix) +"type").c_str());
						Assertf(pobRolladaVarTypeParam, "Unable to find type of shader variable %d on %s on shader %s", iVarNo, pobRolladaAssignedPresetParam->GetValue().c_str(), pobRageColladaMaterial->GetName());
						string strVarType = pobRolladaVarTypeParam->GetValue();

						// Value
						const rageRolladaParam*	pobRolladaVarValueParam = pobRolladaMaterial->GetRolladaParam((string(acVarAttributePrefix) +"value").c_str());
						Assertf(pobRolladaVarValueParam, "Unable to find value of shader variable %d on %s on shader %s", iVarNo, pobRolladaAssignedPresetParam->GetValue().c_str(), pobRageColladaMaterial->GetName());
						string strVarValue = pobRolladaVarValueParam->GetValue();

						// Fix slashes
						for(unsigned i=0; i<strVarValue.size(); i++)
						{
							if(strVarValue[i] == '\\') 
							{
								strVarValue[i] = '/';
							}
						}

						// Add it to the rexObjectGenericMesh::MaterialInfo 
						obReturnMe.m_AttributeNames.PushAndGrow(strVarName.c_str());
						obReturnMe.m_RageTypes.PushAndGrow(strVarType.c_str());
						string strExportedVersionOfTextureFilename = SourceTexturePathAndFilenameToExportedTexturePathAndFilename(strVarValue);
						obReturnMe.m_AttributeValues.PushAndGrow(atString(strExportedVersionOfTextureFilename.c_str()));
						obReturnMe.m_InputTextureFileNames.PushAndGrow(atString(strVarValue.c_str()));
						obReturnMe.m_OutputTextureFileNames.PushAndGrow(atString(strExportedVersionOfTextureFilename.c_str()));
					}
				}
			}
		}

		// Am I filled in from the Rollada data?
		if(obReturnMe.m_AttributeNames.GetCount() == 0)
		{
			// Didn't get anything from the Rollada data, so use the collada data
			string strTexturePathAndFilename = pobRageColladaMaterial->GetSimpleTexturePathAndFilename();

			// Do I actually have a valid texture?
			if(strTexturePathAndFilename != "")
			{
				// Fix slashes
				for(unsigned i=0; i<strTexturePathAndFilename.size(); i++)
				{
					if(strTexturePathAndFilename[i] == '\\') 
					{
						strTexturePathAndFilename[i] = '/';
					}
				}

				obReturnMe.m_TypeName = "default.sps";
				obReturnMe.m_AttributeNames.PushAndGrow("diffuseTex");
				obReturnMe.m_RageTypes.PushAndGrow("grcTexture");
				if(strTexturePathAndFilename == "")
				{
					printf("Oddness %s\n", strTexturePathAndFilename.c_str());
				}
				obReturnMe.m_AttributeValues.PushAndGrow(atString(SourceTexturePathAndFilenameToExportedTexturePathAndFilename(strTexturePathAndFilename).c_str()));
				obReturnMe.m_InputTextureFileNames.PushAndGrow(strTexturePathAndFilename.c_str());
				obReturnMe.m_OutputTextureFileNames.PushAndGrow(atString(SourceTexturePathAndFilenameToExportedTexturePathAndFilename(strTexturePathAndFilename).c_str()));
			}
		}
	}

	// Setup acceptable defaults for anything not filled in yet
	if(obReturnMe.m_AttributeNames.GetCount() == 0)
	{
		// No texture has been found, so use good defaults
		obReturnMe.m_Name = "Unknown";
		obReturnMe.m_TypeName = "default.sps";
		obReturnMe.m_AttributeNames.PushAndGrow("diffuseTex");
		obReturnMe.m_AttributeValues.PushAndGrow(atString("no_texture_assigned"));
		obReturnMe.m_RageTypes.PushAndGrow("grcTexture");
	}

	return obReturnMe;
}

rexObjectGenericScene* ConvertRageColladaSceneIntoRexGenericScene(const rageColladaScene* pobRageColladaScene, const rageRolladaScene* pobRageRolladaScene = NULL)
{
	rexObjectGenericScene* pobRexGenericScene = new rexObjectGenericScene();

	// Go through meshes, adding as I go
	const vector<rageColladaMesh*>&	obArrayOfSceneMeshes = pobRageColladaScene->GetArrayOfMeshes();
	vector<rageColladaMesh*>::const_iterator it = obArrayOfSceneMeshes.begin();
	int iMeshNo = 0;
	rexTimerLog::StartTimer("Processing Meshes");
	for(; it != obArrayOfSceneMeshes.end(); it++)
	{
		iMeshNo++;
		// Progress indictor
		// printf("%4d/%4d\t\n", iMeshNo, obArrayOfSceneMeshes.size());

		// Get matrix
		rexTimerLog::StartTimer("Getting Matrix");
		rageColladaMesh*	pobColladaMesh = *it;
		const GLdouble*		adColladaMatrix = pobColladaMesh->GetMatrix();
		Matrix34 obRageMatrix(	(float)adColladaMatrix[0], (float)adColladaMatrix[4], (float)adColladaMatrix[8],
								(float)adColladaMatrix[1], (float)adColladaMatrix[5], (float)adColladaMatrix[9],
								(float)adColladaMatrix[2], (float)adColladaMatrix[6], (float)adColladaMatrix[10],
								(float)adColladaMatrix[3], (float)adColladaMatrix[7], (float)adColladaMatrix[11]);
		rexTimerLog::EndTimer("Getting Matrix");

		// Create a rexObjectGenericMesh out of the submeshes
		vector<rageColladaSubMesh*>	obArrayOfMeshSubMeshes = pobColladaMesh->GetArrayOfSubMeshes();
		vector<rageColladaSubMesh*>::const_iterator obSubMeshIt = obArrayOfMeshSubMeshes.begin();
		int iSubMeshNo = 0;
		rexTimerLog::StartTimer("Processing Sub Meshes");
		for(; obSubMeshIt != obArrayOfMeshSubMeshes.end(); obSubMeshIt++)
		{
			iSubMeshNo++;

			// Construct mesh
			rexObjectGenericMesh* pobRexGenericMesh = new rexObjectGenericMesh();
			rageColladaSubMesh*	pobColladaSubMesh = *obSubMeshIt;

			// Progress indictor
			printf("Mesh : %4d/%4d\tSub Mesh : %4d/%4d\tVerts : %d\n", iMeshNo, obArrayOfSceneMeshes.size(), iSubMeshNo, obArrayOfMeshSubMeshes.size(), pobColladaSubMesh->GetVertices()->GetArrayOfVertexPositions().size());

			// Fake material
			rexObjectGenericMesh::MaterialInfo obMaterialFaker = CreateRexGenericMaterial(pobColladaSubMesh->GetMaterial(), pobRageRolladaScene);

			// Get on with the geometry
			if(pobColladaSubMesh->HaveVertices())
			{
				// I have verts, so draw me!
				//***********************************************************
				// Create Rage Vertices
				//***********************************************************
				// Add the vertices to the generic REX mesh
				rexTimerLog::StartTimer("Adding Verts");
				pobRexGenericMesh->m_Vertices.Reserve(pobColladaSubMesh->GetVertices()->GetArrayOfVertexPositions().size());
				for(unsigned v=0; v<pobColladaSubMesh->GetVertices()->GetArrayOfVertexPositions().size(); v++)
				{
					// Create a vert and add it to the rage mesh
					rexObjectGenericMesh::VertexInfo obVertFaker;
					rageColladaPoint obVertexPosition = pobColladaSubMesh->GetVertices()->GetArrayOfVertexPositions()[v];
					obVertFaker.m_Position = Vector3(obVertexPosition.GetX(), obVertexPosition.GetY(), obVertexPosition.GetZ());
					pobRexGenericMesh->m_Vertices.PushAndGrow(obVertFaker, ATARRAY_GROW_SIZE);
					giTotalNoOfVerts++;
				}
				rexTimerLog::EndTimer("Adding Verts");


				// Indices
//				int iNoOfPolyIndicesPerVertex = pobColladaSubMesh->GetNoOfPolygonVertexInputs();
//				int iPolygonVertexIndexNo = pobColladaSubMesh->GetPolygonVertexInputNo();
//				int iPolygonVertexNormalIndexNo = pobColladaSubMesh->GetPolygonVertexNormalInputNo();
//				int iPolygonVertexColourIndexNo = pobColladaSubMesh->GetPolygonVertexColourInputNo();
//				int iPolygonVertexTexCoordsIndexNo = pobColladaSubMesh->GetPolygonVertexTexCoordsInputNo();

				rexTimerLog::StartTimer("Adding Polys");
				// For every poly in the sub mesh
				unsigned iNoOfFaces = pobColladaSubMesh->GetArrayOfFaces().size();
				for(unsigned p=0; p<iNoOfFaces; p++)
				{
					// Progress indictor
					giTotalNoOfPolys++;
					// printf("%4d/%4d\t%4d/%4d\t%4d/%4d\n", iMeshNo, obArrayOfSceneMeshes.size(), iSubMeshNo, obArrayOfMeshSubMeshes.size(), p, pobColladaSubMesh->GetArrayOfFaces().size());
					// Create a Rex PrimitiveInfo to hold the poly information
					rexObjectGenericMesh::PrimitiveInfo obPrimitiveFaker;
					obPrimitiveFaker.m_FaceNormal = Vector3(0.0f, 0.0f, 0.0f);
//					const int* pobPolyIndices = pobColladaSubMesh->GetArrayOfFaces()[p];
					int iVertsPerFace = pobColladaSubMesh->GetNumberOfVerticesPerFace(p);
					
					// For every vert in the poly in the sub mesh
					for(int v = 0; v<iVertsPerFace; v++)
					{
						// Construct an adjuct to represent the poly-vertex
						rexObjectGenericMesh::AdjunctInfo obAdjunctFaker;

						// Normal
						const rageColladaVector* pobNormal = pobColladaSubMesh->GetFaceVertexNormal(p, v);
						if(pobNormal)
						{
							obAdjunctFaker.m_NormalIndex = pobRexGenericMesh->m_Normals.GetCount();
							pobRexGenericMesh->m_Normals.PushAndGrow(Vector3(pobNormal->GetX(), pobNormal->GetY(), pobNormal->GetZ()), ATARRAY_GROW_SIZE);
						}

						// Colour
						const rageColladaColor* pobColour = pobColladaSubMesh->GetFaceVertexColour(p, v);
						if(pobColour)
						{
							obAdjunctFaker.m_Color = Vector4(pobColour->GetR(), pobColour->GetG(), pobColour->GetB(), pobColour->GetA());
						}
						else
						{
							obAdjunctFaker.m_Color = Vector4(0.5f, 0.5f, 0.5f, 1.0f);
						}

						// UV
						const rageColladaUV* pobUV = pobColladaSubMesh->GetFaceVertexUV(p, v);
						if(pobUV)
						{
							obAdjunctFaker.m_TextureCoordinates.PushAndGrow(Vector2(pobUV->GetU(), pobUV->GetV()), ATARRAY_GROW_SIZE);
							obAdjunctFaker.m_TextureCoordinateSetNames.PushAndGrow("Fake UVs", ATARRAY_GROW_SIZE);
						}

						// Position
						int iVertexNumber = pobColladaSubMesh->GetGlobalVertexNumber(p, v);
						obAdjunctFaker.m_VertexIndex = iVertexNumber;

						// Add the adjuct to the rex primitive
						obPrimitiveFaker.m_Adjuncts.PushAndGrow(obAdjunctFaker, ATARRAY_GROW_SIZE);
					}
					// Add the primitive to the mesh
					obPrimitiveFaker.m_MaterialIndex = 0;
					obMaterialFaker.m_PrimitiveIndices.PushAndGrow(pobRexGenericMesh->m_Primitives.GetCount(), ATARRAY_GROW_SIZE);
					pobRexGenericMesh->m_Primitives.PushAndGrow(obPrimitiveFaker, ATARRAY_GROW_SIZE);
				}
				rexTimerLog::EndTimer("Adding Polys");
			}
			pobRexGenericMesh->m_Materials.PushAndGrow(obMaterialFaker);
			pobRexGenericMesh->SetMeshName(pobColladaMesh->GetName().c_str());

			// Add the mesh to the scene
			pobRexGenericScene->AddMesh(pobRexGenericMesh, obRageMatrix);
		}
		rexTimerLog::EndTimer("Processing Sub Meshes");
	}
	printf("Done\n");
	rexTimerLog::EndTimer("Processing Meshes");

	return pobRexGenericScene;
}

rexObjectGenericScene* ConvertRageRolladaSceneIntoRexGenericScene(const rageRolladaScene* pobRageRolladaScene)
{
	return ConvertRageColladaSceneIntoRexGenericScene(pobRageRolladaScene->GetColladaScene(), pobRageRolladaScene);
}


// int main(int argc, const char *argv[])
int Main()
{
	giTotalNoOfVerts = 0;
	giTotalNoOfPolys = 0;
	sysTimer obTimer;
	obTimer.Reset();

	// Disable stupid REX "features"
	rexUtility::SetHackTimeStampsOfGeneratedTextures(false);

	const char *pcFilename = SAMPLE_FILE;
	if(sysParam::GetArgCount() > 1)
	{
		pcFilename = sysParam::GetArg(1);
	}

	// Make sure all my slashes are standard
	int iFilenameLength = (int)strlen(pcFilename);
	char acFilename[255];
	strcpy(acFilename, pcFilename);
	for(int i=0; i<iFilenameLength; i++) 
	{
		if(acFilename[i] == '\\')
		{
			acFilename[i] = '/';
		}
	}

	// Convert to lower case
	strlwr(acFilename);
	pcFilename = acFilename;

	// Have I been given a Rollada or Collada file?
	rageRolladaScene* pobRolladaScene = new rageRolladaScene();
	const char* pcFileExtension = strrchr(acFilename, '.');
	if(!strcmp(pcFileExtension, ".dae"))
	{
		// Collada file
		// Open Collada scene
		rexTimerLog::StartTimer("Collada To Collada");
		pobRolladaScene->CreateFromColladaFile(acFilename);
		rexTimerLog::EndTimer("Collada To Collada");
	}
	else if(!strcmp(pcFileExtension, ".rollada"))
	{
		// Rollada file
		// Open Rollada scene
		rexTimerLog::StartTimer("Collada To Rollada");
		pobRolladaScene->CreateFromRolladaFile(acFilename);
		rexTimerLog::EndTimer("Collada To Rollada");
	}
	else
	{
		// Unknown file type
		Assertf(false, "Unknown file extension %s on %s", pcFileExtension, acFilename);
	}

	// Get as generic scene
	rexTimerLog::StartTimer("Rollada To Rex Generic");
	rexObjectGenericScene* pobRexGenericScene = ConvertRageRolladaSceneIntoRexGenericScene(pobRolladaScene);
	delete pobRolladaScene;
	rexTimerLog::EndTimer("Rollada To Rex Generic");

	// Get the name from the filename
	char acJustFilename[255];
	strcpy(acJustFilename, strrchr(pcFilename, '/') + 1);
	(*strrchr(acJustFilename, '.')) = '\0';
	pobRexGenericScene->SetSceneName(acJustFilename);

	// Get the path from the filename
	atString strPath;
	strPath.Set(atString(pcFilename), 0, (int)(strrchr(pcFilename, '/') - pcFilename));
	printf("strPath = %s\n", strPath);
    
	// Export
	rexTimerLog::StartTimer("Rex Generic To RageRuntime");
	pobRexGenericScene->Export(strPath);
	rexTimerLog::EndTimer("Rex Generic To RageRuntime");

	// Clean up
	delete pobRexGenericScene;

#ifdef OLD_COLLADA_METHOD
	// Instantiate the reference implementation
	DAE* pobColladaAPI = new DAE;
	
	//register my colladaViewer integration objects
//	pobColladaAPI->setIntegrationLibrary(&intRegisterElements);

	//load the collada files specified in the arguments
	const char *pcFilename = SAMPLE_FILE;
	if(sysParam::GetArgCount() > 1)
	{
		pcFilename = sysParam::GetArg(1);
	}

	// Make sure all my slashes are standard
	int iFilenameLength = (int)strlen(pcFilename);
	char acFilename[255];
	strcpy(acFilename, pcFilename);
	for(int i=0; i<iFilenameLength; i++) 
	{
		if(acFilename[i] == '\\')
		{
			acFilename[i] = '/';
		}
	}
	pcFilename = &acFilename[0];
	printf("pcFilename = %s\n", pcFilename);

	char acFileURI[255];
	strcpy(acFileURI, "file:///");
	strcat(acFileURI, pcFilename);

	// Make sure it is all lower case, otherwise the COLLADA DOM goes crazy
	strlwr(acFileURI);

	char acTimerLog[255];
	strcpy(acTimerLog, pcFilename);
	strcat(acTimerLog, ".timer.xml");
	rexTimerLog::Open(acTimerLog);
	rexTimerLog::StartTimer("The Whole Process");

	// Keep a report of what happens during export
//	rexReport::Open((pcFilename + ".ExportReport.html").asChar());
	rexReport::Open("c:/ColladaToRage.ExportReport.html");
	fiStream::SetCreateCallback(rexReport::AddToListOfCreatedFiles);
	fiStream::SetOpenCallback(rexReport::AddToListOfOpenedFiles);

	// Tell the user what is happening
	Displayf("About to convert %s\n", pcFilename);

	//in case there are no argument the main should add a sample file to the arguments
	rexTimerLog::StartTimer("Loading Collada File");
	printf("collada->load(\"%s\")\n", acFileURI);
	int res = pobColladaAPI->load(acFileURI);
	if (res != DAE_OK)
	{
		REPORT("Runtime database failed to initialize from %s : Error %d (%s)\n\n",pcFilename, res, daeErrorString(res));
		return -1;
	}
	else
	{
		REPORT("Runtime database initialized from %s.\n\n",pcFilename);
	}
	rexTimerLog::EndTimer("Loading Collada File");

	// Get scene and convert to Rage
	unsigned int iNumberOfScenes = getSceneCount(pobColladaAPI);
	for (unsigned int i=0;i<iNumberOfScenes;i++)
	{
		// Do it the other way
		daeElement *pAsset = NULL;
		int res = pobColladaAPI->getDatabase()->getAsset(&pAsset,i,NULL,"scene");
		if (res == DAE_OK)
		{
			rageColladaScene* pobScene = new rageColladaScene();
			rexTimerLog::StartTimer("Collada To RageCollada");
			pobScene->CreateFromColladaNode((domScene*)pAsset);
			rexTimerLog::EndTimer("Collada To RageCollada");

			// Get as generic scene
			rexTimerLog::StartTimer("RageCollada To Rex Generic");
			rexObjectGenericScene* pobRexGenericScene = ConvertRageColladaSceneIntoRexGenericScene(pobScene);
			delete pobScene;
			rexTimerLog::EndTimer("RageCollada To Rex Generic");

			// Get the name from the filename
			char acJustFilename[255];
			strcpy(acJustFilename, strrchr(pcFilename, '/') + 1);
			(*strrchr(acJustFilename, '.')) = '\0';
			printf("acJustFilename = %s\n", acJustFilename);
			pobRexGenericScene->SetSceneName(acJustFilename);

			// Get the path from the filename
			atString strPath;
			strPath.Set(atString(pcFilename), 0, (int)(strrchr(pcFilename, '/') - pcFilename));
			printf("strPath = %s\n", strPath);
		    
			// Export
			rexTimerLog::StartTimer("Rex Generic To RageRuntime");
			pobRexGenericScene->Export(strPath);
			rexTimerLog::EndTimer("Rex Generic To RageRuntime");

			// Clean up
			delete pobRexGenericScene;
		}
	}

	delete pobColladaAPI;
#endif

	// Close report
	rexTimerLog::EndTimer("The Whole Process");
	rexTimerLog::Close();
	rexReport::Close();
	printf("Time taken : %f seconds (%2d:%2d)\n", obTimer.GetTime(), (int)obTimer.GetTime() / 60, (int)obTimer.GetTime() % (int)60);
	printf("Processed %d vertices in %d polygons\n", giTotalNoOfVerts, giTotalNoOfPolys);
	printf("Vertices Per Second = %f\n", (float)giTotalNoOfVerts / obTimer.GetTime());
	printf("Polygons Per Second = %f\n", (float)giTotalNoOfPolys / obTimer.GetTime());

	return 0;
}

//-------------------------------------------------------
// Output functions

void printError(int res)
{
	REPORT("Error %i: %s\n",res,daeErrorString(res));
}

void printUsage()
{
	REPORT("Usage: colladaRTImport [file]\n\nImports a default collada XML file or the supplied file\nand display the content of the file\n\n");
}

void printDomObject(daeElement *pDomObj)
{
	REPORT("type: %s name: %s\n",pDomObj->getTypeName(),pDomObj->getID());
	//specific integration object printing:
	pDomObj->print(stdout);
}

