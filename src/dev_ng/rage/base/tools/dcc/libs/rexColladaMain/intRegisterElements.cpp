/*
* $Id$
*
* Copyright Sony Computer Entertainment Inc. 2005
* All Rights Reserved.
*/
#pragma warning(default:4263)

#include "intRegisterElements.h"

void intRegisterElements()
{
  /*intCOLLADA::registerElement();
  intAccessor::registerElement();
  intArray::registerElement();
  intInput::registerElement();
  intParam::registerElement();
  intSource::registerElement();
  intSource::intTechnique::registerElement();
  */
  intMesh::registerElement();
  /*
  intCombiner::registerElement();
  intCombiner::intV::registerElement();
  intJoints::registerElement();
  intJoints::intInput::registerElement();
  intLines::registerElement();
  intLines::intP::registerElement();
  intLinestrips::registerElement();
  intLinestrips::intP::registerElement();
  intPolygons::registerElement();
  intPolygons::intP::registerElement();
  intPolygons::intP::intH::registerElement();
  intTriangles::registerElement();
  intTriangles::intP::registerElement();
  intTrifans::registerElement();
  intTrifans::intP::registerElement();
  intTristrips::registerElement();
  intTristrips::intP::registerElement();
  intVertices::registerElement();
  intVertices::intInput::registerElement();
  intLookat::registerElement();
  intMatrix::registerElement();
  intPerspective::registerElement();
  intRotate::registerElement();
  intScale::registerElement();
  intSkew::registerElement();
  intTranslate::registerElement();
  intImage::registerElement();
  intLight::registerElement();
*/  intMaterial::registerElement();/*
  intPass::registerElement();
  intPass::intInput::registerElement();
  intShader::registerElement();
  intShader::intTechnique::registerElement();
  intTexture::registerElement();
  intTexture::intTechnique::registerElement();
  intTexture::intTechnique::intInput::registerElement();
  intCode::registerElement();
  intEntry::registerElement();
  intEntry::intParam::registerElement();
  intProgram::registerElement();
  intCamera::registerElement();
  intCamera::intTechnique::registerElement();
  intCamera::intTechnique::intOptics::registerElement();
  intCamera::intTechnique::intImager::registerElement();
  intInstance::registerElement();
  intChannel::registerElement();
  intSampler::registerElement();
  intSampler::intInput::registerElement();
  intSkin::registerElement();
  intAsset::registerElement();
  intAsset::intAuthor::registerElement();
  intAsset::intAuthoring_tool::registerElement();
  intAsset::intCreated::registerElement();
  intAsset::intModified::registerElement();
  intAsset::intRevision::registerElement();
  intAsset::intSource_data::registerElement();
  intAsset::intCopyright::registerElement();
  intAsset::intTitle::registerElement();
  intAsset::intSubject::registerElement();
  intAsset::intKeywords::registerElement();
  intAsset::intComments::registerElement();
  intAsset::intUp_axis::registerElement();
  intAsset::intUnit::registerElement();
  intExtra::registerElement();
  intExtra::intTechnique::registerElement();
  intBoundingbox::registerElement();
  intBoundingbox::intMin::registerElement();
  intBoundingbox::intMax::registerElement();
  intNode::registerElement();*/
  intScene::registerElement();
/*  intAnimation::registerElement();
  intController::registerElement();
  intGeometry::registerElement();
  intLibrary::registerElement();*/
}

