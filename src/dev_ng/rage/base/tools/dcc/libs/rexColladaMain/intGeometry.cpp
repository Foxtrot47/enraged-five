/*
* $Id$
*
* Copyright Sony Computer Entertainment Inc. 2005
* All Rights Reserved.
*/
#pragma warning(disable : 4100)
#pragma warning(push)
#pragma warning(disable : 4263)
#include "dae/daeDom.h"
#include "dom/domGeometry.h"
#pragma warning(pop)
#include "math/random.h"
#include "vector/color32.h"
#include "intGeometry.h"
#include "rexGeneric/objectMesh.h"

daeMetaElement * intGeometry::_Meta = NULL;

// ********************************************************
// ***** GENERATED INTERFACE - do NOT touch ***************
// ********************************************************

daeElementRef intGeometry::create(daeInt bytes)
{
//	printf("daeElementRef intGeometry::create(daeInt bytes)\n");
	intGeometryRef ref = new(bytes) intGeometry;
	return ref;
}

daeMetaElement *intGeometry::registerElement()
{
//	printf("daeMetaElement *intGeometry::registerElement()\n");
    if ( _Meta != NULL ) return _Meta;
    
    _Meta = new daeMetaElement;
    _Meta->setName( "geometry" );
	_Meta->registerConstructor(intGeometry::create);

	domGeometry::_Meta->setMetaIntegration(_Meta);

	_Meta->setElementSize(sizeof(intGeometry));
	_Meta->validate();

	return _Meta;
}


// ********************************************************
// ***** INTEGRATION INTERFACE ****************************
// ********************************************************

// IMPORT

void intGeometry::createFrom(daeElementRef element)
{
//	printf("void intGeometry::createFrom(daeElementRef element)\n");
	// INSERT CODE TO CREATE YOUR USER DATA HERE
	// m_pobRexGenericMesh = new myRuntimeClass;
	m_pobRexGenericMesh = new rexObjectGenericMesh();
	_element = element;
}

void intGeometry::fromCOLLADA()
{
//	printf("void intGeometry::fromCOLLADA()\n");
	// INSERT CODE TO TRANSLATE TO YOUR RUNTIME HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericMesh;
	// local->foo = element->foo;
	// local->bar = element->subelem[0]->bar;
	domGeometry* geometryElement = (domGeometry*)(domElement*)_element;
	domMesh *meshElement = geometryElement->getMesh();

	// Fake a name
	daeURI* pobBaseURI = _element->getBaseURI();
	//printf("pobBaseURI->filepath = %s\n", pobBaseURI->filepath);
	//printf("pobBaseURI->file = %s\n", pobBaseURI->file);

	// Get the name from the file name
	char* pcName = new char[strlen(pobBaseURI->file)];
	strcpy(pcName, pobBaseURI->file);
	char* pcStartOfExtension = strrchr(pcName, '.');
	(*pcStartOfExtension) = '\0';
	// printf("pcName = %s\n", pcName);
	m_pobRexGenericMesh->SetMeshName(pcName);

	// Get the verts out of Collada and into Rage
	// First, get the vertices element in the collada mesh
	domVertices* pobVertices = meshElement->getVertices();

	// The vertices tag should have an "input" tag called "POSITION"
	// Get what information is there out of the verts
	atArray<Vector3> obAVertexPositions;
	atArray<Vector3> obAVertexNormals;
	atArray<Color32> obAVertexColours;
	for(int i=0;i<(int)pobVertices->getInput_array().getCount(); i++)
	{
		//printf("pobVertices->getInput_array()[%d]->getSemantic() = %s\n", i, pobVertices->getInput_array()[i]->getSemantic());
		//printf("pobVertices->getInput_array()[%d]->getSource()   = %s\n", i, pobVertices->getInput_array()[i]->getSource());
		//printf("pobVertices->getInput_array()[%d]->getValue()   = %s\n", i, pobVertices->getInput_array()[i]->getValue());
		if(!strcmp(pobVertices->getInput_array()[i]->getSemantic(), "POSITION"))
		{
			//***********************************************************
			// Getting Vertex POSITION
			//***********************************************************
			// Found the POSITION tag, so get what it points too
			const xsAnyURI* obURIForSourceOfVertexPositions = &(pobVertices->getInput_array()[i]->getSource());

			// Get the source
			domSource* pobSourceOfVertexPositions = (domSource*)(domElement*)obURIForSourceOfVertexPositions->element;

			// The first element in the float_array_array of the POSITION source tag is the array of positions
			daeDoubleArray* obColladaVertexPositionArray = &(pobSourceOfVertexPositions->getFloat_array_array()[0]->getValue());
			int iNoOfVertices = obColladaVertexPositionArray->getCount()/3;
			for(int i=0; i<iNoOfVertices; i++)
			{
				// Extract the position info into something more friendly
				obAVertexPositions.PushAndGrow(Vector3((float)obColladaVertexPositionArray->get((i*3) + 0), (float)obColladaVertexPositionArray->get((i*3) + 1), (float)obColladaVertexPositionArray->get((i*3) + 2)));
			}
		}
		else if(!strcmp(pobVertices->getInput_array()[i]->getSemantic(), "NORMAL"))
		{
			//***********************************************************
			// Getting Vertex normals
			//***********************************************************
			// Found the NORMAL tag, so get what it points too
			const xsAnyURI* obURIForSourceOfVertexNormals = &(pobVertices->getInput_array()[i]->getSource());

			// Get the source
			domSource* pobSourceOfVertexNormals = (domSource*)(domElement*)obURIForSourceOfVertexNormals->element;

			// The first element in the float_array_array of the Normal source tag is the array of Normals
			daeDoubleArray* obColladaVertexNormalArray = &(pobSourceOfVertexNormals->getFloat_array_array()[0]->getValue());
			int iNoOfVertices = obColladaVertexNormalArray->getCount()/3;
			for(int i=0; i<iNoOfVertices; i++)
			{
				// Extract the Normal info into something more friendly
				obAVertexNormals.PushAndGrow(Vector3((float)obColladaVertexNormalArray->get((i*3) + 0), (float)obColladaVertexNormalArray->get((i*3) + 1), (float)obColladaVertexNormalArray->get((i*3) + 2)));
			}
		}
		else if(!strcmp(pobVertices->getInput_array()[i]->getSemantic(), "COLOR"))
		{
			//***********************************************************
			// Getting Vertex colours
			//***********************************************************
			// Found the COLOR tag, so get what it points too
			const xsAnyURI* obURIForSourceOfVertexColours = &(pobVertices->getInput_array()[i]->getSource());

			// Get the source
			domSource* pobSourceOfVertexColours = (domSource*)(domElement*)obURIForSourceOfVertexColours->element;

			// The first element in the float_array_array of the Colour source tag is the array of Colours
			daeDoubleArray* obColladaVertexColourArray = &(pobSourceOfVertexColours->getFloat_array_array()[0]->getValue());
			int iNoOfVertices = obColladaVertexColourArray->getCount()/4;
			for(int i=0; i<iNoOfVertices; i++)
			{
				// Extract the Colour info into something more friendly
				obAVertexColours.PushAndGrow(Color32((float)obColladaVertexColourArray->get((i*4) + 0), (float)obColladaVertexColourArray->get((i*4) + 1), (float)obColladaVertexColourArray->get((i*4) + 2), (float)obColladaVertexColourArray->get((i*4) + 3)));
			}
		}
	}

	//***********************************************************
	// Create Rage Vertices
	//***********************************************************
	// Add the vertices to the generic REX mesh
	for(int i=0; i<obAVertexPositions.GetCount(); i++)
	{
		// Create a vert and add it to the rage mesh
		rexObjectGenericMesh::VertexInfo obVertFaker;
		obVertFaker.m_Position = obAVertexPositions[i];
		m_pobRexGenericMesh->m_Vertices.PushAndGrow(obVertFaker);
	}

	//***********************************************************
	// Getting Face-Vertex normals and colours
	//***********************************************************
	// Now get the face-vertex normals from the collada mesh
	domPolygons* pobPolygons = meshElement->getPolygons_array()[0];

	// The polygons tag should have an "input" tag called "NORMAL"
	atArray<Vector3> obAFaceVertexNormals;
	atArray<Color32> obAFaceVertexColours;
	atArray<Vector2> obAFaceVertexTexCoords;
	int iNoOfPolygonInputs = -1;
	int iPolygonVertexInputNo = -1;
	int iPolygonNormalInputNo = -1;
	int iPolygonColourInputNo = -1;
	int iPolygonTexCoordsInputNo = -1;
	for(int i=0;i<(int)pobPolygons->getInput_array().getCount(); i++)
	{
		domInput_Array& obInputArray = pobPolygons->getInput_array();
		domInput* pobInput = obInputArray[i];
		//printf("pobPolygons->getInput_array()[%d]->getSemantic() = %s\n", i, pobInput->getSemantic());
		//printf("pobPolygons->getInput_array()[%d]->getSource()   = %s\n", i, pobInput->getSource());
		//printf("pobPolygons->getInput_array()[%d]->getValue()   = %s\n", i, pobInput->getValue());
		//printf("pobPolygons->getInput_array()[%d]->getIdx()      = %d\n", i, pobInput->getIdx());
		// Is it the highest idx yet?
		if(iNoOfPolygonInputs < (int)pobInput->getIdx())
		{
			// Yep, so use it
			iNoOfPolygonInputs = pobInput->getIdx();
		}
		if(!strcmp(pobInput->getSemantic(), "VERTEX"))
		{
			iPolygonVertexInputNo = pobInput->getIdx();
		}
		else if(!strcmp(pobInput->getSemantic(), "NORMAL"))
		{
			// Found the NORMAL tag, so get what it points too
			iPolygonNormalInputNo = pobInput->getIdx();
			const xsAnyURI* obURIForSourceOfFaceVertexNormals = &(pobInput->getSource());

			// Get the source
			domSource* pobSourceOfFaceVertexNormals = (domSource*)(domElement*)obURIForSourceOfFaceVertexNormals->element;

			// The first element in the float_array_array of the Normal source tag is the array of Normals
			daeDoubleArray* obColladaFaceVertexNormalArray = &(pobSourceOfFaceVertexNormals->getFloat_array_array()[0]->getValue());
			int iNoOfFaceNormals = obColladaFaceVertexNormalArray->getCount()/3;
			for(int i=0; i<iNoOfFaceNormals; i++)
			{
				// Extract the Normal info into something more friendly
				obAFaceVertexNormals.PushAndGrow(Vector3((float)obColladaFaceVertexNormalArray->get((i*3) + 0), (float)obColladaFaceVertexNormalArray->get((i*3) + 1), (float)obColladaFaceVertexNormalArray->get((i*3) + 2)));
			}
		}
		else if(!strcmp(pobInput->getSemantic(), "COLOR"))
		{
			// Found the Colour tag, so get what it points too
			iPolygonColourInputNo = pobInput->getIdx();
			const xsAnyURI* obURIForSourceOfFaceVertexColours = &(pobInput->getSource());

			// Get the source
			domSource* pobSourceOfFaceVertexColours = (domSource*)(domElement*)obURIForSourceOfFaceVertexColours->element;

			// The first element in the float_array_array of the Colour source tag is the array of Colours
			daeDoubleArray* obColladaFaceVertexColourArray = &(pobSourceOfFaceVertexColours->getFloat_array_array()[0]->getValue());
			int iNoOfFaceColours = obColladaFaceVertexColourArray->getCount()/4;
			for(int i=0; i<iNoOfFaceColours; i++)
			{
				// Extract the Colour info into something more friendly
				obAFaceVertexColours.PushAndGrow(Color32((float)obColladaFaceVertexColourArray->get((i*4) + 0), (float)obColladaFaceVertexColourArray->get((i*4) + 1), (float)obColladaFaceVertexColourArray->get((i*4) + 2), (float)obColladaFaceVertexColourArray->get((i*4) + 3)));
			}
		}
		else if(!strcmp(pobInput->getSemantic(), "TEXCOORD"))
		{
			// Found the TEXCOORD tag, so get what it points too
			iPolygonTexCoordsInputNo = pobInput->getIdx();
			const xsAnyURI* obURIForSourceOfFaceVertexTexCoords = &(pobInput->getSource());

			// Get the source
			domSource* pobSourceOfFaceVertexTexCoords = (domSource*)(domElement*)obURIForSourceOfFaceVertexTexCoords->element;

			// The first element in the float_array_array of the TexCoord source tag is the array of TexCoords
			daeDoubleArray* obColladaFaceVertexTexCoordArray = &(pobSourceOfFaceVertexTexCoords->getFloat_array_array()[0]->getValue());
			int iNoOfFaceTexCoords = obColladaFaceVertexTexCoordArray->getCount()/2;
			for(int i=0; i<iNoOfFaceTexCoords; i++)
			{
				// Extract the TexCoord info into something more friendly
				obAFaceVertexTexCoords.PushAndGrow(Vector2((float)obColladaFaceVertexTexCoordArray->get((i*2) + 0), (float)obColladaFaceVertexTexCoordArray->get((i*2) + 1)));
			}
		}
	}

	// Increase number of inputs by one
	iNoOfPolygonInputs++;

	//***********************************************************
	// Create Rage Normals
	//***********************************************************
	// Add the normals to the mesh
	if(obAFaceVertexNormals.GetCount() > 0)
	{
		for(int i=0; i<obAFaceVertexNormals.GetCount(); i++)
		{
			// Create a vert and add it to the rage mesh
			m_pobRexGenericMesh->m_Normals.PushAndGrow(obAFaceVertexNormals[i]);
		}
	}
	else if(obAVertexNormals.GetCount() > 0)
	{
		for(int i=0; i<obAVertexNormals.GetCount(); i++)
		{
			// Create a vert and add it to the rage mesh
			m_pobRexGenericMesh->m_Normals.PushAndGrow(obAVertexNormals[i]);
		}
	}
	else
	{
		m_pobRexGenericMesh->m_Normals.PushAndGrow(Vector3(0.0f, 0.0f, 0.0f));
	}

	//***********************************************************
	// Get Collada faces and turn them into Rage faces
	//***********************************************************
	// Create the "primitives" (faces)
	// For every poly....
	mthRandom obRandomNumberGenerator;
	int iNoOfPrimitives = pobPolygons->getP_array().getCount();
	for(int i=0; i<iNoOfPrimitives; i++)
	{
		// ...construct a Rage Primitive
		rexObjectGenericMesh::PrimitiveInfo obFaceFaker;
		obFaceFaker.m_MaterialIndex = 0;
		obFaceFaker.m_FaceNormal = Vector3(0.0f, 0.0f, 0.0f);

		// Get points
		domPolygons::domP* obAArrayOfPoints = pobPolygons->getP_array()[i];

		// For every vertex in the poly...
		int iStepper = iNoOfPolygonInputs;
		for(int v=0; v<(int)obAArrayOfPoints->getValue().getCount(); v += iStepper)
		{
			// ...Get vertex info
			// Get pos
			int iVertexPositionIndex = obAArrayOfPoints->getValue()[v + iPolygonVertexInputNo];

			// Get normal
			int iVertexNormalIndex = 0;
			if(obAFaceVertexNormals.GetCount() > 0)
			{
				iVertexNormalIndex = obAArrayOfPoints->getValue()[v + iPolygonNormalInputNo];
			}
			else if(obAVertexNormals.GetCount() > 0)
			{
				iVertexNormalIndex = iVertexPositionIndex;
			}

			// Construct an adjuct out the information so far
			rexObjectGenericMesh::AdjunctInfo obAdjunctFaker;
			obAdjunctFaker.m_VertexIndex = iVertexPositionIndex;
			obAdjunctFaker.m_NormalIndex = iVertexNormalIndex;

			// Colour
			// Do I have face vertex colours
			if(( obAFaceVertexColours.GetCount() > 0 ) && (iPolygonColourInputNo > 0))
			{
				// Got face vertex colour, so use them
				const Color32& obColour = obAFaceVertexColours[obAArrayOfPoints->getValue()[v + iPolygonColourInputNo]];
				obAdjunctFaker.m_Color.Set(obColour.GetRedf(), obColour.GetGreenf(), obColour.GetBluef(), obColour.GetAlphaf());
			}
			else if( obAVertexColours.GetCount() > 0 )
			{
				// No face vertex colours, but I do have just normal vertex colours, so use them
				const Color32& obColour = obAVertexColours[obAArrayOfPoints->getValue()[v + iPolygonVertexInputNo]];
				obAdjunctFaker.m_Color.Set(obColour.GetRedf(), obColour.GetGreenf(), obColour.GetBluef(), obColour.GetAlphaf());
			}
			else
			{
				// No vertex colours at all, so use mid grey
				obAdjunctFaker.m_Color.Set(0.5f, 0.5f, 0.5f, 1.0f);
			}

			// UVs
			if(obAFaceVertexTexCoords.GetCount() > 0 )
			{
				// Got UVs, so use them
				obAdjunctFaker.m_TextureCoordinates.PushAndGrow(obAFaceVertexTexCoords[obAArrayOfPoints->getValue()[v + iPolygonTexCoordsInputNo]]);
				obAdjunctFaker.m_TextureCoordinateSetNames.PushAndGrow("Fake UVs");
			}

			
			// Add the adjuct to the poly
			obFaceFaker.m_Adjuncts.PushAndGrow(obAdjunctFaker);

			// Add the normal
			obFaceFaker.m_FaceNormal += m_pobRexGenericMesh->m_Normals[obAdjunctFaker.m_NormalIndex];
		}

		// Average the normal
		obFaceFaker.m_FaceNormal.Normalize();

		// Add the face to the mesh
		m_pobRexGenericMesh->m_Primitives.PushAndGrow(obFaceFaker);
	}

	// Fake up a material
	rexObjectGenericMesh::MaterialInfo stFakedUpMaterial;
	for(int i=0; i<iNoOfPrimitives; i++)
	{
		stFakedUpMaterial.m_PrimitiveIndices.PushAndGrow(i);
	}
	stFakedUpMaterial.m_Name = "fakedupmaterial";
	stFakedUpMaterial.m_TypeName = "rage_default.shadert";

	// Fake up an attribute
	stFakedUpMaterial.m_AttributeNames.PushAndGrow("diffuseTex");
	stFakedUpMaterial.m_AttributeValues.PushAndGrow("crate");
	stFakedUpMaterial.m_RageTypes.PushAndGrow("grcTexture");

	stFakedUpMaterial.m_InputTextureFileNames.PushAndGrow("T:/GiantHamster.jpg");
	m_pobRexGenericMesh->m_Materials.PushAndGrow(stFakedUpMaterial);

/*	
	rexObjectGenericMesh *local = (rexObjectGenericMesh *)m_pobRexGenericMesh;
	daeDoubleArray *floatArray = NULL;
	if (meshElement->getSource()_array[0]->array_array.getCount())
	{
		// Convert old style array to typed float array
		int arrayCount = meshElement->getSource()_array[0]->array_array.getCount();
		int index;
		for(index =0;index<arrayCount;index++) {
			meshElement->getSource()_array[0]->createAndPlace("float_array");
			daeStringRefArray *stringArray =
				&(meshElement->getSource()_array[0]->array_array[index]->getValue());
			int faCnt = meshElement->getSource()_array[0]->float_array_array.getCount();
			floatArray = 
				&(meshElement->getSource()_array[0]->getFloat_array_array()[faCnt-1]->getValue());
			int k, scnt = stringArray->getCount();
			floatArray->setCount(stringArray->getCount());
		
			for(k=0;k<scnt;k++)
				sscanf(stringArray->get(k),"%f",
					   (daeChar*)floatArray->getRawData() + k*sizeof(daeFloat));
		}
	}
	else if ( meshElement->getSource()_array[0]->float_array_array.getCount())
	{
		floatArray = &(meshElement->getSource()_array[0]->getFloat_array_array()[0]->getValue());
	}
	else
	{
		fprintf(stderr,"unsupported array type\n");
		return;
	}
	
	local->_iVertexCount = floatArray->getCount()/3;
	local->_pVertices = new float[local->_iVertexCount*3];

	memcpy(local->_pVertices,((float*)(void*)floatArray->getRawData()) ,local->_iVertexCount*3*sizeof(float));
	int poly;
	for (poly=0; poly<meshElement->polygons_array.getCount(); poly++)
	{
		domPolygons *polygons = meshElement->getPolygons_array()[poly];
		int polygonCount = polygons->count;
		//get the number of component by vertex
		int numCoords = polygons->getInput_array().getCount();
		int i;
		for (i=0;i<polygonCount;i++)
		{
			myPolygon myPoly;
			domPolygons::domP *p = polygons->getP_array()[i];
#ifdef POLYGONS_MIXED_CONTENT_MODEL_HOLES
			daeString pString = p->getValue().get(0);
			daeInt foo;
			daeInt iCnt;
			daeString s = pString;
			iCnt = 0;
			daeInt cCnt;
			
			while(sscanf(s,"%d%n",&foo,&cCnt) > 0) {
				iCnt++;
				s += cCnt;
				if (s[0] == '\0')
					break;
			}
			myPoly._iIndexCount = iCnt/numCoords + 1;
			myPoly._pIndexes = new unsigned short[myPoly._iIndexCount];
			int j;
			s = pString;
			int k;
			for (j=0;j<(int)myPoly._iIndexCount-1;j++) {
				sscanf(s,"%d%n",&(myPoly._pIndexes[j]),&cCnt);
				s += cCnt;
				for(k=0;k<numCoords-1;k++) {
					sscanf(s,"%d%n",&foo,&cCnt);
					s += cCnt;
				}
			}
			myPoly._pIndexes[j] = myPoly._pIndexes[0];				
#else
			myPoly._iIndexCount = p->getValue().getCount()/numCoords;
			//I can even modify the geometry here, for instance 
			//the simple engine used here worked based of line strips geometries.
			//so I close the line strip by adding the 1st index at the end of the list of indexes of the
			//polygon
			myPoly._iIndexCount++; 
			myPoly._pIndexes = new unsigned short[myPoly._iIndexCount];
			int j;
			for (j=0;j<(int)myPoly._iIndexCount-1;j++)
				myPoly._pIndexes[j] = (unsigned short)((int*)(void*)p->getValue().getRawData())[j*numCoords];
			myPoly._pIndexes[j] = myPoly._pIndexes[0];
#endif
			local->_vPolygons.push_back(myPoly);
		}
	}
*/
}

void intGeometry::fromCOLLADAPostProcess()
{
//	printf("void intGeometry::fromCOLLADAPostProcess()\n");
	// INSERT CODE TO POST PROCESS HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericMesh;
	// local->renderingContext = MYGLOBAL::getRenderingContext;
	// MYGLOBAL::registerInstance(local);
}

// EXPORT

void intGeometry::createTo(void *userData)
{
//	printf("void intGeometry::createTo(void *userData)\n");
	// INSERT CODE TO CREATE COLLADA DOM OBJECTS HERE
	// _element = new domGeometry;
	// m_pobRexGenericMesh = userData;
}

void intGeometry::toCOLLADA()
{
//	printf("void intGeometry::toCOLLADA()\n");
	// INSERT CODE TO TRANSLATE TO YOUR RUNTIME HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericMesh;
	// element->foo = local->foo;
	// element->subelem[0]->bar = local->bar;
}

void intGeometry::toCOLLADAPostProcess()
{
//	printf("void intGeometry::toCOLLADAPostProcess()\n");
	// INSERT CODE TO POST PROCESS HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericMesh;
	// local->renderingContext = MYGLOBAL::getRenderingContext;
	// MYGLOBAL::registerInstance(local);
}

// ********************************************************
// ***** USER CODE ****************************************
// ********************************************************
intGeometry::~intGeometry()
{
//	printf("intGeometry::~intGeometry()\n");
/*	if (m_pobRexGenericMesh && m_pobRexGenericMesh->_pVertices)
	{
		delete[] m_pobRexGenericMesh->_pVertices;
		int i;
		for (i=0;i<(int)m_pobRexGenericMesh->_vPolygons.size();i++)
		{
			delete[] m_pobRexGenericMesh->_vPolygons[i]._pIndexes;
		}
		delete m_pobRexGenericMesh;
	}
*/
	delete m_pobRexGenericMesh;
}

