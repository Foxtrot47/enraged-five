#pragma once
#include "rexGeneric/objectMesh.h"
#include "rexGeneric/objectSkeleton.h"

/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectGenericScene -- class with data suitable for describing a scene of other generic object
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				nothing
//			
/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObjectGenericScene
{
public:
	rexObjectGenericScene(void);
	~rexObjectGenericScene(void);

	// Accessors
	const atString&				GetSceneName() const {return m_Name;}

	// Modifiers
	void AddMesh(rexObjectGenericMesh* pobMesh, const Matrix34& obMatrix = M34_IDENTITY);
	void AddSkeleton(rexObjectGenericSkeleton* pobSkeleton);
	void						SetSceneName(const atString& strName) {m_Name = strName; }

	// Operators
	void Export(const char* pcDestinationFolder) const;

	// A scene may have many instances of one mesh, so store this information in a seperate class
	class rexObjectGenericMeshInstance
	{
	public:
		rexObjectGenericMeshInstance()
			: m_strName("rexObjectGenericMeshInstance.m_strName Not set"), m_obGenericMesh(NULL)
		{
			m_obMatrix.Identity();
		}
		atString					m_strName;				// Can be anything, usually set to the name of the transform in maya/collada
		Matrix34					m_obMatrix;				// World space matrix of the instance
		rexObjectGenericMesh*		m_obGenericMesh;		// The actually mesh info WARNING!!! this is shared between many instances
	};

private:
	// A scene has meshes, lots of meshes
	atArray<rexObjectGenericMesh*>					m_obAArrayOfMeshes;
	atArray<rexObjectGenericMeshInstance*>			m_obAArrayOfMeshInstances;

	// And possibly a skeleton or lots of skeletons
	atArray<rexObjectGenericSkeleton*>				m_obAArrayOfSkeletons;

	atString					m_Name;					// Can be anything, usually set to the name of the file it is generated from

};

}
