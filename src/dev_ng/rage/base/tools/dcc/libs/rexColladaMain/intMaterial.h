/*
* $Id$
*
* Copyright Sony Computer Entertainment Inc. 2005
* All Rights Reserved.
*/

#ifndef __intMaterial_h__
#define __intMaterial_h__
#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include "dae/daeIntegrationObject.h"
#include "dae/daeMetaElement.h"
#include "dom/domNode.h"
#pragma warning(pop)

#include "rexGeneric/objectMesh.h"

using namespace rage;

class intMaterial;

typedef daeSmartRef<intMaterial> intMaterialRef;
typedef daeTArray<intMaterialRef> intMaterialArray;

class intMaterial : public daeIntegrationObject
{
public: // VIRTUAL INTEGRATION INTERFACE
	// This Method is used to create tool/runtime object
	virtual void createFrom(daeElementRef element);
	
	// This method translate from COLLADA to tool/runtime object
	virtual void fromCOLLADA();

	// This method is used as a second pass post process on tool/runtime objs
	virtual void fromCOLLADAPostProcess();

		// This Method is used to create tool/runtime object
	virtual void createTo(void *userData);

	// This method translate from COLLADA to tool/runtime object
	virtual void toCOLLADA();

	// This method is used as a second pass post process on tool/runtime objs
	virtual void toCOLLADAPostProcess();

public: // STATIC METHODS
	static daeElementRef create(daeInt bytes);
	static daeMetaElement * registerElement();
	
public: // STATIC MEMBERS
	static daeMetaElement* _Meta;

public: // USER CODE
	virtual ~intMaterial();
	rexObjectGenericMesh::MaterialInfo getMaterial() const { return m_obRexGenericMaterial; }

private: // USER CODE
	rexObjectGenericMesh::MaterialInfo m_obRexGenericMaterial;
	daeElement* _element;
};

#endif

