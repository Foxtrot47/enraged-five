/*
* $Id$
*
* Copyright Sony Computer Entertainment Inc. 2005
* All Rights Reserved.
*/
#pragma warning(disable : 4100)
#pragma warning(push)
#pragma warning(disable : 4263)
#include "dae/daeDom.h"
#include "dom/domMaterial.h"
#include "dom/domshader.h"
#include "dom/dompass.h"
#include "dom/dominput.h"
#pragma warning(pop)

#include "intMaterial.h"
// #include "intGeometry.h"

daeMetaElement * intMaterial::_Meta = NULL;

// ********************************************************
// ***** GENERATED INTERFACE - do NOT touch ***************
// ********************************************************

daeElementRef intMaterial::create(daeInt bytes)
{
	printf("daeElementRef intMaterial::create(daeInt bytes)\n");
	intMaterialRef ref = new(bytes) intMaterial;
	return ref;
}

daeMetaElement *intMaterial::registerElement()
{
	printf("daeMetaElement *intMaterial::registerElement()\n");
    if ( _Meta != NULL ) return _Meta;
    
    _Meta = new daeMetaElement;
    _Meta->setName( "geometry" );
	_Meta->registerConstructor(intMaterial::create);

	domMaterial::_Meta->setMetaIntegration(_Meta);

	_Meta->setElementSize(sizeof(intMaterial));
	_Meta->validate();

	return _Meta;
}


// ********************************************************
// ***** INTEGRATION INTERFACE ****************************
// ********************************************************

// IMPORT

void intMaterial::createFrom(daeElementRef element)
{
	printf("void intMaterial::createFrom(daeElementRef element)\n");
	// INSERT CODE TO CREATE YOUR USER DATA HERE
	_element = element;
}

void intMaterial::fromCOLLADA()
{
	printf("void intMaterial::fromCOLLADA()\n");

	// Get Material
	domMaterial* pobMaterialElement = (domMaterial*)(domElement*)_element;

	// Get the name of the material
	m_obRexGenericMaterial.m_Name = pobMaterialElement->getName();

	// Fill in a "good" rage material.  Something better needs to be done here really
	m_obRexGenericMaterial.m_TypeName = "default.sps";

	// Find the texutre used, if one is used
	atString strTexturePathAndFilename = "";
	if(pobMaterialElement->getShader_array().getCount() > 0)
	{
		// Got a shader
		domShader* pobShader = pobMaterialElement->getShader_array()[0];
		for(unsigned t=0; t<pobShader->getTechnique_array().getCount(); t++)
		{
			// Get a technique
			domShader::domTechnique* pobTechnique = pobShader->getTechnique_array()[t];
			if(!strcmp(pobTechnique->getProfile(), "COMMON"))
			{
				// It is the common technique
				// Does it have a pass?
				for(unsigned p=0; p<pobTechnique->getPass_array().getCount(); p++)
				{
					// Get a pass
					domPass* pobPass = pobTechnique->getPass_array()[p];

					// Next look at the inputs
					for(unsigned i=0; i<pobPass->getInput_array().getCount(); i++)
					{
						// Get input
						domPass::domInput* pobInput = pobPass->getInput_array()[i];
						// Next look at the inputs
						if(!strcmp(pobInput->getSemantic(), "TEXTURE"))
						{
							// Bingo!  Found a texture!  So use it
							// Follow the URI to the texture
							const xsAnyURI* obURIForSourceOfTexture = &(pobInput->getSource());

							// Get the source
							domTexture* pobSourceOfTexture = (domTexture*)(domElement*)obURIForSourceOfTexture->element;

							// Find the common Technique of the source
							for(unsigned tt=0; tt<pobSourceOfTexture->getTechnique_array().getCount(); tt++)
							{
								// Get a technique
								domTexture::domTechnique* pobTechnique = pobSourceOfTexture->getTechnique_array()[tt];
								if(!strcmp(pobTechnique->getProfile(), "COMMON"))
								{
									// It is the common technique
									// Does it have an IMAGE?
									// Look at the inputs
									for(unsigned ii=0; ii<pobTechnique->getInput_array().getCount(); ii++)
									{
										// Get input
										domTexture::domTechnique::domInput* pobInput = pobTechnique->getInput_array()[ii];

										// Is it an IMAGE
										if(!strcmp(pobInput->getSemantic(), "IMAGE"))
										{
											// Bingo!  Found a file!  So use it
											// Follow the URI to the texture
											const xsAnyURI* obURIForSourceOfFile = &(pobInput->getSource());

											// Get the image
											domImage* pobSourceOfFile = (domImage*)(domElement*)obURIForSourceOfFile->element;

											// Get the filename
											strTexturePathAndFilename = atString(pobSourceOfFile->getSource().filepath, pobSourceOfFile->getSource().file);
											break;
										}
										if(strTexturePathAndFilename != "") break;
									}
								}
								if(strTexturePathAndFilename != "") break;
							}
						}
						if(strTexturePathAndFilename != "") break;
					}
					if(strTexturePathAndFilename != "") break;
				}
			}
			if(strTexturePathAndFilename != "") break;
		}
		//if(pobShader->getShader_array().getCount() > 0)
		//{
		//	domShader* pobShader = pobMaterialElement->getShader_array()[0];
		//}
	}

	// After all that, did I find any textures?
	if(strTexturePathAndFilename != "")
	{
		printf("strTexturePathAndFilename = %s\n", (const char*)strTexturePathAndFilename);

		// Give it a texture
		m_obRexGenericMaterial.m_InputTextureFileNames.PushAndGrow(strTexturePathAndFilename);

		// Convert input texture into an exported texture
		int iPosOfLastDot = strTexturePathAndFilename.GetLength() - 1;
		for(; iPosOfLastDot>0; iPosOfLastDot--)
		{
			if(strTexturePathAndFilename[iPosOfLastDot] == '.')
			{
				break;
			}
		}
		int iPosOfLastSlash = strTexturePathAndFilename.GetLength() - 1;
		for(; iPosOfLastSlash>0; iPosOfLastSlash--)
		{
			if(strTexturePathAndFilename[iPosOfLastSlash] == '/')
			{
				break;
			}
		}
		iPosOfLastSlash++;
		char acExportedTextureFilename[255];
		for(int i=0; i<255; i++) acExportedTextureFilename[i] = '\0';
		strncpy(acExportedTextureFilename, &strTexturePathAndFilename[iPosOfLastSlash], (iPosOfLastDot - iPosOfLastSlash));

		// Assign texture to material
		m_obRexGenericMaterial.m_AttributeNames.PushAndGrow("diffuseTex");
		m_obRexGenericMaterial.m_AttributeValues.PushAndGrow(atString(acExportedTextureFilename));
		m_obRexGenericMaterial.m_RageTypes.PushAndGrow("grcTexture");
	}
	else
	{
		// No texture has been found, so use good defaults
		m_obRexGenericMaterial.m_AttributeNames.PushAndGrow("diffuseTex");
		m_obRexGenericMaterial.m_AttributeValues.PushAndGrow("no_texture_assigned");
		m_obRexGenericMaterial.m_RageTypes.PushAndGrow("grcTexture");
	}
}

void intMaterial::fromCOLLADAPostProcess()
{
	printf("void intMaterial::fromCOLLADAPostProcess()\n");
	// INSERT CODE TO POST PROCESS HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_obRexGenericMaterial;
	// local->renderingContext = MYGLOBAL::getRenderingContext;
	// MYGLOBAL::registerInstance(local);
}

// EXPORT

void intMaterial::createTo(void *userData)
{
	printf("void intMaterial::createTo(void *userData)\n");
	// INSERT CODE TO CREATE COLLADA DOM OBJECTS HERE
	// _element = new domMaterial;
	// m_obRexGenericMaterial = userData;
}

void intMaterial::toCOLLADA()
{
	printf("void intMaterial::toCOLLADA()\n");
	// INSERT CODE TO TRANSLATE TO YOUR RUNTIME HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_obRexGenericMaterial;
	// element->foo = local->foo;
	// element->subelem[0]->bar = local->bar;
}

void intMaterial::toCOLLADAPostProcess()
{
	printf("void intMaterial::toCOLLADAPostProcess()\n");
	// INSERT CODE TO POST PROCESS HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_obRexGenericMaterial;
	// local->renderingContext = MYGLOBAL::getRenderingContext;
	// MYGLOBAL::registerInstance(local);
}

// ********************************************************
// ***** USER CODE ****************************************
// ********************************************************
intMaterial::~intMaterial()
{
	printf("intMaterial::~intMaterial()\n");
}

