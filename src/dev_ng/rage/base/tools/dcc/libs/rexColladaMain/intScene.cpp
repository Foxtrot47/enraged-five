/*
* $Id$
*
* Copyright Sony Computer Entertainment Inc. 2005
* All Rights Reserved.
*/
#pragma warning(disable : 4100)
#pragma warning(push)
#pragma warning(disable : 4263)
#include "dae/daeDom.h"
#include "dom/domScene.h"
#pragma warning(pop)

#include "intScene.h"
#include "intMesh.h"
#include "rexObjectGenericScene.h"

daeMetaElement * intScene::_Meta = NULL;

// ********************************************************
// ***** GENERATED INTERFACE - do NOT touch ***************
// ********************************************************

daeElementRef intScene::create(daeInt bytes)
{
	printf("daeElementRef intScene::create(daeInt bytes)\n");
	intSceneRef ref = new(bytes) intScene;
	return ref;
}

daeMetaElement *intScene::registerElement()
{
	printf("daeMetaElement *intScene::registerElement()\n");
	if ( _Meta != NULL ) return _Meta;

	_Meta = new daeMetaElement;
	_Meta->setName( "geometry" );
	_Meta->registerConstructor(intScene::create);

	domScene::_Meta->setMetaIntegration(_Meta);

	_Meta->setElementSize(sizeof(intScene));
	_Meta->validate();

	return _Meta;
}


// ********************************************************
// ***** INTEGRATION INTERFACE ****************************
// ********************************************************

// IMPORT

void intScene::createFrom(daeElementRef element)
{
	printf("void intScene::createFrom(daeElementRef element)\n");
	// INSERT CODE TO CREATE YOUR USER DATA HERE
	// m_pobRexGenericScene = new myRuntimeClass;
	m_pobRexGenericScene = new rexObjectGenericScene();
	_element = element;
}

// A function that works through the node heirarchy of a scene, adding meshes as it goes
void intScene::ProcessSceneNode(domNode* pobNode, const Matrix34& obParentMatrix, rexObjectGenericSkeleton::Bone* pobParentJoint)
{
	// Update matrix for this node
	Matrix34 obCurrentMatrix = obParentMatrix;
	// Does the node have a matrix?  If so, just use it
	if(pobNode->getMatrix_array().getCount() > 0)
	{
		domFloat4x4& obColladaMatrix = pobNode->getMatrix_array()[0]->getValue();
		obCurrentMatrix.Dot(Matrix34(	(float)obColladaMatrix[0], (float)obColladaMatrix[4], (float)obColladaMatrix[8],
			(float)obColladaMatrix[1], (float)obColladaMatrix[5], (float)obColladaMatrix[9],
			(float)obColladaMatrix[2], (float)obColladaMatrix[6], (float)obColladaMatrix[10],
			(float)obColladaMatrix[3], (float)obColladaMatrix[7], (float)obColladaMatrix[11])
			);
	}
	else if(pobNode->getContents().getCount() > 0)
	{
		// Ok, there is no baked matrix but there is individual opperations, 
		// so I need to construct a matrix out of its parts
		for(int iOperationNo=0; iOperationNo<(int)pobNode->getContents().getCount(); iOperationNo++)
		{
			daeElement* pobOperationElement = pobNode->getContents()[iOperationNo];
			obCurrentMatrix = ApplyColladaMatrixOperation(obCurrentMatrix, pobOperationElement);
		}
	}

	// obCurrentMatrix.Print();


	// What does this node point to?
	for(int intstanceNo=0; intstanceNo<(int)pobNode->getInstance_array().getCount(); intstanceNo++)
	{
		// Get the instance
		domInstance* pobInstance = pobNode->getInstance_array()[intstanceNo];

		// What does the instance point to?
		const xsAnyURI* obURIForInstance = &(pobInstance->getUrl());

		// Is it a mesh?
		if(!strcmp(obURIForInstance->element->getTypeName(), "geometry"))
		{
			// Bingo, a mesh
			// Get the geometry
			domGeometry* pobGeometry = (domGeometry*)(domElement*)obURIForInstance->element;

			// Get Mesh
			domMesh* pobMesh = pobGeometry->getMesh();

			// Ok, the geometry has its own converter, so I should be able to use that to get a rexObjectGenericMesh version of the mesh
			daeIntegrationObject* pobIntegrationObj = pobMesh->getIntObject();
			intMesh* pobIntMesh =(intMesh *)pobIntegrationObj;
			rexObjectGenericMesh* pobRexGenericMesh = pobIntMesh->getMesh();

			// Give the rexObjectGenericMesh a better name
			pobRexGenericMesh->SetMeshName(pobNode->getName());

			// Give the rexObjectGenericMesh a better matrix
			// Add it to the scene
			m_pobRexGenericScene->AddMesh(pobRexGenericMesh, obCurrentMatrix);
		}
	}

	// Am I a joint?
	if(pobNode->getType() == NODETYPE_JOINT)
	{
		// I am a joint, so do some joint work

		// The matrix for a joint is split in three parts, a transformation applied before 
		// anything else, a matrix to be applied BEFORE animation data and a matrix to be 
		// applied AFTER animation data.  When this is exported from Maya you are just given 
		// a series of translates and rotates, and there is no good and right way to know 
		// at what point animation data will be appplied.  BUT due to the limitations of the 
		// in game runtime animation system, I can make assumptions about what happens 
		// before and what happens after.  This is the order things are exported in:
		//
		//exportTranslation(element, "translate", translation, true);

		// The pre-anim matrix starts here

		//exportTranslation(element, "rotatePivotTranslation", rotatePivotTranslation, false);
		//exportTranslation(element, "rotatePivot", rotatePivot, false);
		//exportRotation(element, "jointOrient", jointOrientation, presence.jointOrient, presence.jointOrientNode);
		//exportRotation(element, "rotate", rotation, presence.rot, presence.rotNode);

		// The Engine only allows rotation to be exported, so anything BEFORE this point 
		// forms the pre-anim matrix and anything AFTER this point forms the post-anim matrix

		//exportRotation(element, "rotateAxis", rotationAxis, presence.rotAxis, presence.rotAxisNode); // Also called rotate orient in documentation
		//exportTranslation(element, "rotatePivotInverse", rotatePivot * -1, false);
		//exportTranslation(element, "scalePivotTranslation", scalePivotTranslation, false);
		//exportTranslation(element, "scalePivot", scalePivot, false);
		//CSkewHelper::exportTransform(element->node, shear);
		//exportScale(element);
		//exportTranslation(element, "scalePivotInverse", scalePivot * -1, false);

		// The function IsItAPreAnimTransformation is used to decide which side of the anim 
		// divide a transformaion happens
		Matrix34 obInitialMatrix = M34_IDENTITY;
		Matrix34 obPreAnimationMatrix = M34_IDENTITY;
		Matrix34 obPostAnimationMatrix = M34_IDENTITY;
		for(int iOperationNo=0; iOperationNo<(int)pobNode->getContents().getCount(); iOperationNo++)
		{
			daeElement* pobOperationElement = pobNode->getContents()[iOperationNo];
			const char* pcTransformationSid = NULL;
			if(!strcmp(pobOperationElement->getTypeName(), "translate"))
			{
				pcTransformationSid = ((domTranslate*)pobOperationElement)->getSid();
			}
			else if(!strcmp(pobOperationElement->getTypeName(), "rotate"))
			{
				pcTransformationSid = ((domRotate*)pobOperationElement)->getSid();
			}
			else if(!strcmp(pobOperationElement->getTypeName(), "scale"))
			{
				pcTransformationSid = ((domScale*)pobOperationElement)->getSid();
			}

			if(pcTransformationSid)
			{
				switch(IsItAPreAnimTransformation(pcTransformationSid))
				{
				case INITIAL:
					{
						obInitialMatrix = ApplyColladaMatrixOperation(obPreAnimationMatrix, pobOperationElement);
						break;
					}
				case PRE_ANIMATION:
					{
						obPreAnimationMatrix = ApplyColladaMatrixOperation(obPreAnimationMatrix, pobOperationElement);
						break;
					}
				case POST_ANIMATION:
					{
						obPostAnimationMatrix = ApplyColladaMatrixOperation(obPostAnimationMatrix, pobOperationElement);
						break;
					}
				}
			}
		}

		// Ok, got the joint's matrices so construct a joint out of them
		rexObjectGenericSkeleton::Bone* pobCurrentJoint = new rexObjectGenericSkeleton::Bone();
		pobCurrentJoint->m_IsJoint = true;			// true if a joint, false otherwise, eg. transforms that don't animate
		//		atArray<ChannelInfo>m_Channels;			// Bones have degrees of freedom, these are called channels.  This contains names of channels, if locked and limits
		pobCurrentJoint->m_JointOrient = obPreAnimationMatrix;		// Joint orient - static rotation added to a joint BEFORE the animation data
		pobCurrentJoint->m_ScaleOrient = obPostAnimationMatrix;		// Scale orient/Rotate axis - applied AFTER animated rotation and BEFORE child joints are considered
		pobCurrentJoint->m_Parent = pobParentJoint;			// Parent bone
		pobCurrentJoint->m_Name = pobNode->getName();				// Name similar to the name in Maya but "cleaned" by magic
		pobCurrentJoint->m_BoneID = pobNode->getName();			// Text that is hashed to produce the bone ID, usually the name of the bone but not always
		pobCurrentJoint->SetBoneMatrix(M34_IDENTITY);

		// If I have a parent, my offest is a little more complicated
		Matrix34 obOffsetMatrix = obInitialMatrix;
		rexObjectGenericSkeleton::Bone* pobAncestorJoint = pobParentJoint;
		while(pobAncestorJoint)
		{
			obOffsetMatrix.Dot(pobAncestorJoint->m_JointOrient);
			obOffsetMatrix.Dot(pobAncestorJoint->m_ScaleOrient);
			pobAncestorJoint = pobAncestorJoint->m_Parent;
		}
		pobCurrentJoint->m_Offset = obOffsetMatrix.GetVector(3);			// Offset to child (this) from parent (m_Parent)

		// Am I a root joint?
		if(pobParentJoint)
		{
			// Not a root joint add to current skeleton
			pobParentJoint->m_Children.PushAndGrow( pobCurrentJoint ); 
		}
		else
		{
			// Root joint skeleton, so add to the scene
			rexObjectGenericSkeleton* pobSkeleton = new rexObjectGenericSkeleton();
			pobSkeleton->m_RootBone = pobCurrentJoint;
			pobSkeleton->SetName(pobCurrentJoint->m_Name);
			m_pobRexGenericScene->AddSkeleton(pobSkeleton);
		}

		// For future joints, set me as the parent node
		pobParentJoint = pobCurrentJoint;
	}

	// Does the node have any children nodes?
	for(int n=0;n<(int)pobNode->getNode_array().getCount(); n++)
	{
		// Get node
		domNode* pobChildNode = pobNode->getNode_array()[n];

		// Process that node
		ProcessSceneNode(pobChildNode, obCurrentMatrix, pobParentJoint);
	}
}

void intScene::fromCOLLADA()
{
	printf("void intScene::fromCOLLADA()\n");
	// INSERT CODE TO TRANSLATE TO YOUR RUNTIME HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericScene;
	// local->foo = element->foo;
	// local->bar = element->subelem[0]->bar;

	// Get the name from the file name
	daeURI* pobBaseURI = _element->getBaseURI();
	char* pcName = new char[strlen(pobBaseURI->file)];
	strcpy(pcName, pobBaseURI->file);
	char* pcStartOfExtension = strrchr(pcName, '.');
	(*pcStartOfExtension) = '\0';
	// printf("pcName = %s\n", pcName);
	m_pobRexGenericScene->SetSceneName(pcName);

	// Get the scene
	domScene* pobSceneElement = (domScene*)(domElement*)_element;

	// A scene has nodes, so go through them
	for(int n=0;n<(int)pobSceneElement->getNode_array().getCount(); n++)
	{
		// Get node
		domNode* pobNode = pobSceneElement->getNode_array()[n];

		// Process that node
		ProcessSceneNode(pobNode, M34_IDENTITY);
	}
}

void intScene::fromCOLLADAPostProcess()
{
	printf("void intScene::fromCOLLADAPostProcess()\n");
	// INSERT CODE TO POST PROCESS HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericScene;
	// local->renderingContext = MYGLOBAL::getRenderingContext;
	// MYGLOBAL::registerInstance(local);
}

// EXPORT

void intScene::createTo(void *userData)
{
	printf("void intScene::createTo(void *userData)\n");
	// INSERT CODE TO CREATE COLLADA DOM OBJECTS HERE
	// _element = new domScene;
	// m_pobRexGenericScene = userData;
}

void intScene::toCOLLADA()
{
	printf("void intScene::toCOLLADA()\n");
	// INSERT CODE TO TRANSLATE TO YOUR RUNTIME HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericScene;
	// element->foo = local->foo;
	// element->subelem[0]->bar = local->bar;
}

void intScene::toCOLLADAPostProcess()
{
	printf("void intScene::toCOLLADAPostProcess()\n");
	// INSERT CODE TO POST PROCESS HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericScene;
	// local->renderingContext = MYGLOBAL::getRenderingContext;
	// MYGLOBAL::registerInstance(local);
}

// ********************************************************
// ***** USER CODE ****************************************
// ********************************************************
intScene::~intScene()
{
	printf("intScene::~intScene()\n");
	/*	if (m_pobRexGenericScene && m_pobRexGenericScene->_pVertices)
	{
	delete[] m_pobRexGenericScene->_pVertices;
	int i;
	for (i=0;i<(int)m_pobRexGenericScene->_vPolygons.size();i++)
	{
	delete[] m_pobRexGenericScene->_vPolygons[i]._pIndexes;
	}
	delete m_pobRexGenericScene;
	}
	*/
	delete m_pobRexGenericScene;
}

// The matrix for a joint is split in three parts, a transformation applied before 
// anything else, a matrix to be applied BEFORE animation data and a matrix to be 
// applied AFTER animation data.  When this is exported from Maya you are just given 
// a series of translates and rotates, and there is no good and right way to know 
// at what point animation data will be appplied.  BUT due to the limitations of the 
// in game runtime animation system, I can make assumptions about what happens 
// before and what happens after.  This is the order things are exported in:
//
//exportTranslation(element, "translate", translation, true);

// The pre-anim matrix starts here

//exportTranslation(element, "rotatePivotTranslation", rotatePivotTranslation, false);
//exportTranslation(element, "rotatePivot", rotatePivot, false);
//exportRotation(element, "jointOrient", jointOrientation, presence.jointOrient, presence.jointOrientNode);
//exportRotation(element, "rotate", rotation, presence.rot, presence.rotNode);

// The Engine only allows rotation to be exported, so anything BEFORE this point 
// forms the pre-anim matrix and anything AFTER this point forms the post-anim matrix

//exportRotation(element, "rotateAxis", rotationAxis, presence.rotAxis, presence.rotAxisNode); // Also called rotate orient in documentation
//exportTranslation(element, "rotatePivotInverse", rotatePivot * -1, false);
//exportTranslation(element, "scalePivotTranslation", scalePivotTranslation, false);
//exportTranslation(element, "scalePivot", scalePivot, false);
//CSkewHelper::exportTransform(element->node, shear);
//exportScale(element);
//exportTranslation(element, "scalePivotInverse", scalePivot * -1, false);

// The function IsItAPreAnimTransformation is used to decide which side of the anim 
// divide a transformaion happens
intScene::BoneMatrixStage  intScene::IsItAPreAnimTransformation(const char* pcTransformationSid) const
{
	// Pre animation transforms
	if(!strcmp(pcTransformationSid, "translate"))
	{
		return INITIAL;
	}
	else if(!strcmp(pcTransformationSid, "rotatePivotTranslation"))
	{
		return PRE_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "rotatePivot"))
	{
		return PRE_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "jointOrientX"))
	{
		return PRE_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "jointOrientY"))
	{
		return PRE_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "jointOrientZ"))
	{
		return PRE_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "rotateX"))
	{
		return PRE_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "rotateY"))
	{
		return PRE_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "rotateZ"))
	{
		return PRE_ANIMATION;
	}
	// Post animation transforms
	else if(!strcmp(pcTransformationSid, "rotateAxisX"))
	{
		return POST_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "rotateAxisY"))
	{
		return POST_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "rotateAxisZ"))
	{
		return POST_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "rotatePivotInverse"))
	{
		return POST_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "scalePivotTranslation"))
	{
		return POST_ANIMATION;
	}
	else if(!strcmp(pcTransformationSid, "scalePivot"))
	{
		return POST_ANIMATION;
	}
	// I have no idea what this is!
	else
	{
		Assertf(false, "I do not know how to handle the TransformationSid %s this is very bad and you need to report this to Kevin Rose.  It probably means that you are using a Collada Exporter that Rage is not aware of.", pcTransformationSid);
	}
	return INITIAL;
}

// Used to apply collada based matrix operations to a rage matrix
Matrix34 intScene::ApplyColladaMatrixOperation(const Matrix34& obInMatrix, daeElement* pobOperationElement) const
{
	Matrix34 obMatrix = obInMatrix;
	// printf("pobOperationElement->getTypeName() = %s\n", pobOperationElement->getTypeName());

	if(!strcmp(pobOperationElement->getTypeName(), "translate"))
	{
		// Apply translation to the matirx
		domFloat3& obTranslation = ((domTranslate*)pobOperationElement)->getValue();
		obMatrix.Translate((float)obTranslation[0], (float)obTranslation[1], (float)obTranslation[2]);
	}
	else if(!strcmp(pobOperationElement->getTypeName(), "rotate"))
	{
		// Apply Rotate to the matirx
		domFloat4& obRotate = ((domRotate*)pobOperationElement)->getValue();
		obMatrix.Rotate(Vector3((float)obRotate[0], (float)obRotate[1], (float)obRotate[2]), DtoR * (float)obRotate[3]);
	}
	else if(!strcmp(pobOperationElement->getTypeName(), "scale"))
	{
		// Apply scale to the matirx
		domFloat3& obScale = ((domScale*)pobOperationElement)->getValue();
		Matrix34 obScaleMatrix;
		obScaleMatrix.Identity();
		obScaleMatrix.MakeScale((float)obScale[0], (float)obScale[1], (float)obScale[2]);
		obMatrix.DotFromLeft(obScaleMatrix);
	}
	return obMatrix;
}

