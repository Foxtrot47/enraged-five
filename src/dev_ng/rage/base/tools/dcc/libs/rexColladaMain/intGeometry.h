/*
* $Id$
*
* Copyright Sony Computer Entertainment Inc. 2005
* All Rights Reserved.
*/

#ifndef __intGeometry_h__
#define __intGeometry_h__
#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include "dae/daeIntegrationObject.h"
#include "dae/daeMetaElement.h"
#pragma warning(pop)

#include "rexGeneric/objectMesh.h"

using namespace rage;

class intGeometry;

typedef daeSmartRef<intGeometry> intGeometryRef;
typedef daeTArray<intGeometryRef> intGeometryArray;

class intGeometry : public daeIntegrationObject
{
public: // VIRTUAL INTEGRATION INTERFACE
	// This Method is used to create tool/runtime object
	virtual void createFrom(daeElementRef element);
	
	// This method translate from COLLADA to tool/runtime object
	virtual void fromCOLLADA();

	// This method is used as a second pass post process on tool/runtime objs
	virtual void fromCOLLADAPostProcess();

		// This Method is used to create tool/runtime object
	virtual void createTo(void *userData);

	// This method translate from COLLADA to tool/runtime object
	virtual void toCOLLADA();

	// This method is used as a second pass post process on tool/runtime objs
	virtual void toCOLLADAPostProcess();

public: // STATIC METHODS
	static daeElementRef create(daeInt bytes);
	static daeMetaElement * registerElement();
	
public: // STATIC MEMBERS
	static daeMetaElement* _Meta;

public: // USER CODE
	virtual ~intGeometry();
	rexObjectGenericMesh *getGeometry() { return m_pobRexGenericMesh; }

private: // USER CODE
	rexObjectGenericMesh *m_pobRexGenericMesh;
	daeElement* _element;
};

#endif

