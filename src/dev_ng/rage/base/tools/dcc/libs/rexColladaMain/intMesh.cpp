/*
* $Id$
*
* Copyright Sony Computer Entertainment Inc. 2005
* All Rights Reserved.
*/
#pragma warning(disable : 4100)
#pragma warning(push)
#pragma warning(disable : 4263)
#include "dae/daeDom.h"
#include "dom/domMesh.h"
#pragma warning(pop)
#include "math/random.h"
#include "vector/color32.h"
#include "intMesh.h"
#include "intMaterial.h"
#include "rexGeneric/objectMesh.h"

daeMetaElement * intMesh::_Meta = NULL;

// ********************************************************
// ***** GENERATED INTERPolygon - do NOT touch ***************
// ********************************************************

daeElementRef intMesh::create(daeInt bytes)
{
	//	printf("daeElementRef intMesh::create(daeInt bytes)\n");
	intMeshRef ref = new(bytes) intMesh;
	return ref;
}

daeMetaElement *intMesh::registerElement()
{
	//	printf("daeMetaElement *intMesh::registerElement()\n");
	if ( _Meta != NULL ) return _Meta;

	_Meta = new daeMetaElement;
	_Meta->setName( "geometry" );
	_Meta->registerConstructor(intMesh::create);

	domMesh::_Meta->setMetaIntegration(_Meta);

	_Meta->setElementSize(sizeof(intMesh));
	_Meta->validate();

	return _Meta;
}


// ********************************************************
// ***** INTEGRATION INTERPolygon ****************************
// ********************************************************

// IMPORT

void intMesh::createFrom(daeElementRef element)
{
	//	printf("void intMesh::createFrom(daeElementRef element)\n");
	// INSERT CODE TO CREATE YOUR USER DATA HERE
	// m_pobRexGenericMesh = new myRuntimeClass;
	m_pobRexGenericMesh = new rexObjectGenericMesh();
	_element = element;
}

void intMesh::fromCOLLADA()
{
	//	printf("void intMesh::fromCOLLADA()\n");
	// INSERT CODE TO TRANSLATE TO YOUR RUNTIME HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericMesh;
	// local->foo = element->foo;
	// local->bar = element->subelem[0]->bar;
	domMesh* meshElement = (domMesh*)(domElement*)_element;

	// Fake a name
	daeURI* pobBaseURI = _element->getBaseURI();
	//printf("pobBaseURI->filepath = %s\n", pobBaseURI->filepath);
	//printf("pobBaseURI->file = %s\n", pobBaseURI->file);

	// Get the name from the file name
	char* pcName = new char[strlen(pobBaseURI->file)];
	strcpy(pcName, pobBaseURI->file);
	char* pcStartOfExtension = strrchr(pcName, '.');
	(*pcStartOfExtension) = '\0';
	// printf("pcName = %s\n", pcName);
	m_pobRexGenericMesh->SetMeshName(pcName);

	// Get the verts out of Collada and into Rage
	// First, get the vertices element in the collada mesh
	domVertices* pobVertices = meshElement->getVertices();

	// The vertices tag should have an "input" tag called "POSITION"
	// Get what information is there out of the verts
	atArray<Vector3> obAVertexPositions;
	atArray<Vector3> obAVertexNormals;
	atArray<Color32> obAVertexColours;
	for(int i=0;i<(int)pobVertices->getInput_array().getCount(); i++)
	{
		//printf("pobVertices->getInput_array()[%d]->getSemantic() = %s\n", i, pobVertices->getInput_array()[i]->getSemantic());
		//printf("pobVertices->getInput_array()[%d]->getSource()   = %s\n", i, pobVertices->getInput_array()[i]->getSource());
		//printf("pobVertices->getInput_array()[%d]->getValue()   = %s\n", i, pobVertices->getInput_array()[i]->getValue());
		if(!strcmp(pobVertices->getInput_array()[i]->getSemantic(), "POSITION"))
		{
			//***********************************************************
			// Getting Vertex POSITION
			//***********************************************************
			// Found the POSITION tag, so get what it points too
			const xsAnyURI* obURIForSourceOfVertexPositions = &(pobVertices->getInput_array()[i]->getSource());

			// Get the source
			domSource* pobSourceOfVertexPositions = (domSource*)(domElement*)obURIForSourceOfVertexPositions->element;

			// The first element in the float_array_array of the POSITION source tag is the array of positions
			daeDoubleArray* obColladaVertexPositionArray = &(pobSourceOfVertexPositions->getFloat_array_array()[0]->getValue());
			int iNoOfVertices = obColladaVertexPositionArray->getCount()/3;
			for(int j=0; j<iNoOfVertices; j++)
			{
				// Extract the position info into something more friendly
				obAVertexPositions.PushAndGrow(Vector3((float)obColladaVertexPositionArray->get((j*3) + 0), (float)obColladaVertexPositionArray->get((j*3) + 1), (float)obColladaVertexPositionArray->get((j*3) + 2)));
			}
		}
		else if(!strcmp(pobVertices->getInput_array()[i]->getSemantic(), "NORMAL"))
		{
			//***********************************************************
			// Getting Vertex normals
			//***********************************************************
			// Found the NORMAL tag, so get what it points too
			const xsAnyURI* obURIForSourceOfVertexNormals = &(pobVertices->getInput_array()[i]->getSource());

			// Get the source
			domSource* pobSourceOfVertexNormals = (domSource*)(domElement*)obURIForSourceOfVertexNormals->element;

			// The first element in the float_array_array of the Normal source tag is the array of Normals
			daeDoubleArray* obColladaVertexNormalArray = &(pobSourceOfVertexNormals->getFloat_array_array()[0]->getValue());
			int iNoOfVertices = obColladaVertexNormalArray->getCount()/3;
			for(int j=0; j<iNoOfVertices; j++)
			{
				// Extract the Normal info into something more friendly
				obAVertexNormals.PushAndGrow(Vector3((float)obColladaVertexNormalArray->get((j*3) + 0), (float)obColladaVertexNormalArray->get((j*3) + 1), (float)obColladaVertexNormalArray->get((j*3) + 2)));
			}
		}
		else if(!strcmp(pobVertices->getInput_array()[i]->getSemantic(), "COLOR"))
		{
			//***********************************************************
			// Getting Vertex colours
			//***********************************************************
			// Found the COLOR tag, so get what it points too
			const xsAnyURI* obURIForSourceOfVertexColours = &(pobVertices->getInput_array()[i]->getSource());

			// Get the source
			domSource* pobSourceOfVertexColours = (domSource*)(domElement*)obURIForSourceOfVertexColours->element;

			// The first element in the float_array_array of the Colour source tag is the array of Colours
			daeDoubleArray* obColladaVertexColourArray = &(pobSourceOfVertexColours->getFloat_array_array()[0]->getValue());
			int iNoOfVertices = obColladaVertexColourArray->getCount()/4;
			for(int j=0; j<iNoOfVertices; j++)
			{
				// Extract the Colour info into something more friendly
				obAVertexColours.PushAndGrow(Color32((float)obColladaVertexColourArray->get((j*4) + 0), (float)obColladaVertexColourArray->get((j*4) + 1), (float)obColladaVertexColourArray->get((j*4) + 2), (float)obColladaVertexColourArray->get((j*4) + 3)));
			}
		}
	}

	//***********************************************************
	// Create Rage Vertices
	//***********************************************************
	// Add the vertices to the generic REX mesh
	for(int i=0; i<obAVertexPositions.GetCount(); i++)
	{
		// Create a vert and add it to the rage mesh
		rexObjectGenericMesh::VertexInfo obVertFaker;
		obVertFaker.m_Position = obAVertexPositions[i];
		m_pobRexGenericMesh->m_Vertices.PushAndGrow(obVertFaker);
	}

	//***********************************************************
	// Get polys
	//***********************************************************
	for(unsigned int iPolygonsNo = 0; iPolygonsNo<meshElement->getPolygons_array().getCount(); iPolygonsNo++)
	{
		// Get polygons
		domPolygons* pobPolygons = meshElement->getPolygons_array()[iPolygonsNo];

		//***********************************************************
		// Getting Polygon material info
		//***********************************************************
		const xsAnyURI* obURIForSourceOfMaterial = &(pobPolygons->getMaterial());
		rexObjectGenericMesh::MaterialInfo stFakedUpMaterial;
		if(obURIForSourceOfMaterial->state != daeURI::uri_empty)
		{
			domMaterial* pobSourceOfMaterial = (domMaterial*)(domElement*)obURIForSourceOfMaterial->element;

			// Ok, the material has its own converter, so I should be able to use that to get a rexObjectGenericMesh::MaterialInfo version of the material
			daeIntegrationObject* pobIntegrationObj = pobSourceOfMaterial->getIntObject();
			intMaterial* pobIntMaterial =(intMaterial *)pobIntegrationObj;
			stFakedUpMaterial = pobIntMaterial->getMaterial();
		}
		else
		{
			// No material given in collada, so fake something up
			stFakedUpMaterial.m_Name = "Fake_Material";
			stFakedUpMaterial.m_TypeName = "rage_default.shadert";
			stFakedUpMaterial.m_AttributeNames.PushAndGrow("diffuseTex");
			stFakedUpMaterial.m_AttributeValues.PushAndGrow("no_texture_assigned");
			stFakedUpMaterial.m_RageTypes.PushAndGrow("grcTexture");
		}

		//***********************************************************
		// Get what information I can about the Polygons
		//***********************************************************
		int iPolygonVertexInputNo = -1;
		int iPolygonVertexNormalInputNo = -1;
		int iPolygonVertexColourInputNo = -1;
		int iPolygonVertexTexCoordsInputNo = -1;
		atArray<Vector3> obAPolygonVertexNormals;
		atArray<Color32> obAPolygonVertexColours;
		atArray<Vector2> obAPolygonVertexTexCoords;
		int iNoOfPolygonIndices = -1;
		for(int i=0;i<(int)pobPolygons->getInput_array().getCount(); i++)
		{
			domInput* pobInput = pobPolygons->getInput_array()[i];
			if(((int)pobInput->getIdx()) > iNoOfPolygonIndices) iNoOfPolygonIndices = (int)pobInput->getIdx();
			//printf("pobPolygons->getInput_array()[%d]->getSemantic() = %s\n", i, pobPolygons->getInput_array()[i]->getSemantic());
			//printf("pobPolygons->getInput_array()[%d]->getSource()   = %s\n", i, pobPolygons->getInput_array()[i]->getSource());
			//printf("pobPolygons->getInput_array()[%d]->getValue()   = %s\n", i, pobPolygons->getInput_array()[i]->getValue());
			if(!strcmp(pobInput->getSemantic(), "VERTEX"))
			{
				//***********************************************************
				// Getting Polygon Vertices
				//***********************************************************
				iPolygonVertexInputNo = pobInput->getIdx();
			}
			else if(!strcmp(pobInput->getSemantic(), "NORMAL"))
			{
				//***********************************************************
				// Getting PolygonVertex normals
				//***********************************************************
				iPolygonVertexNormalInputNo = pobInput->getIdx();
				// Found the NORMAL tag, so get what it points too
				const xsAnyURI* obURIForSourceOfPolygonVertexNormals = &(pobPolygons->getInput_array()[i]->getSource());

				// Get the source
				domSource* pobSourceOfPolygonVertexNormals = (domSource*)(domElement*)obURIForSourceOfPolygonVertexNormals->element;

				// The first element in the float_array_array of the Normal source tag is the array of Normals
				daeDoubleArray* obColladaPolygonNormalArray = &(pobSourceOfPolygonVertexNormals->getFloat_array_array()[0]->getValue());
				int iNoOfPolygons = obColladaPolygonNormalArray->getCount()/3;
				for(int j=0; j<iNoOfPolygons; j++)
				{
					// Extract the Normal info into something more friendly
					obAPolygonVertexNormals.PushAndGrow(Vector3((float)obColladaPolygonNormalArray->get((j*3) + 0), (float)obColladaPolygonNormalArray->get((j*3) + 1), (float)obColladaPolygonNormalArray->get((j*3) + 2)));
				}
			}
			else if(!strcmp(pobInput->getSemantic(), "TEXCOORD"))
			{
				//***********************************************************
				// Getting PolygonVertex TexCoords
				//***********************************************************
				// Found the TEXCOORD tag, so get what it points too
				iPolygonVertexTexCoordsInputNo = pobInput->getIdx();
				const xsAnyURI* obURIForSourceOfPolygonVertexTexCoords = &(pobInput->getSource());

				// Get the source
				domSource* pobSourceOfPolygonVertexTexCoords = (domSource*)(domElement*)obURIForSourceOfPolygonVertexTexCoords->element;

				// The first element in the float_array_array of the TexCoord source tag is the array of TexCoords
				daeDoubleArray* obColladaPolygonTexCoordArray = &(pobSourceOfPolygonVertexTexCoords->getFloat_array_array()[0]->getValue());
				int iNoOfPolygonVertexTexCoords = obColladaPolygonTexCoordArray->getCount()/2;
				for(int j=0; j<iNoOfPolygonVertexTexCoords; j++)
				{
					// Extract the TexCoord info into something more friendly
					obAPolygonVertexTexCoords.PushAndGrow(Vector2((float)obColladaPolygonTexCoordArray->get((j*2) + 0), (float)obColladaPolygonTexCoordArray->get((j*2) + 1)));
				}
			}
			else if(!strcmp(pobInput->getSemantic(), "COLOR"))
			{
				//***********************************************************
				// Getting PolygonVertex Colours
				//***********************************************************
				// Found the Colour tag, so get what it points too
				iPolygonVertexColourInputNo = i;
				const xsAnyURI* obURIForSourceOfPolygonVertexColours = &(pobInput->getSource());

				// Get the source
				domSource* pobSourceOfPolygonVertexColours = (domSource*)(domElement*)obURIForSourceOfPolygonVertexColours->element;

				// The first element in the float_array_array of the Colour source tag is the array of Colours
				daeDoubleArray* obColladaPolygonVertexColourArray = &(pobSourceOfPolygonVertexColours->getFloat_array_array()[0]->getValue());
				int iNoOfPolygonColours = obColladaPolygonVertexColourArray->getCount()/4;
				for(int j=0; j<iNoOfPolygonColours; j++)
				{
					// Extract the Colour info into something more friendly
					obAPolygonVertexColours.PushAndGrow(Color32((float)obColladaPolygonVertexColourArray->get((j*4) + 0), (float)obColladaPolygonVertexColourArray->get((j*4) + 1), (float)obColladaPolygonVertexColourArray->get((j*4) + 2), (float)obColladaPolygonVertexColourArray->get((j*4) + 3)));
				}
			}
		}
		iNoOfPolygonIndices++;

		//***********************************************************
		// Create Rage Normals
		//***********************************************************
		// Add the normals to the mesh
		if(obAPolygonVertexNormals.GetCount() > 0)
		{
			for(int i=0; i<obAPolygonVertexNormals.GetCount(); i++)
			{
				// Create a vert and add it to the rage mesh
				m_pobRexGenericMesh->m_Normals.PushAndGrow(obAPolygonVertexNormals[i]);
			}
		}
		else if(obAVertexNormals.GetCount() > 0)
		{
			for(int i=0; i<obAVertexNormals.GetCount(); i++)
			{
				// Create a vert and add it to the rage mesh
				m_pobRexGenericMesh->m_Normals.PushAndGrow(obAVertexNormals[i]);
			}
		}
		else
		{
			m_pobRexGenericMesh->m_Normals.PushAndGrow(Vector3(0.0f, 0.0f, 0.0f));
		}

		// Got all the info I can about the polys, now construct the polys themselves
		//***********************************************************
		// Get Collada Polygons and turn them into Rage Polygons
		//***********************************************************
		// Create the "primitives" (Polygons)
		// For every poly....
		mthRandom obRandomNumberGenerator;
		int iNoOfPrimitives = pobPolygons->getP_array().getCount();
		for(int i=0; i<iNoOfPrimitives; i++)
		{
			// ...construct a Rage Primitive
			rexObjectGenericMesh::PrimitiveInfo obPolygonFaker;
			obPolygonFaker.m_MaterialIndex = 0;
			obPolygonFaker.m_FaceNormal = Vector3(0.0f, 0.0f, 0.0f);

			// Get points
			domPolygons::domP* pobAArrayOfIndices = pobPolygons->getP_array()[i];

			// For every vertex in the poly...
			for(int v=0; v<(int)pobAArrayOfIndices->getValue().getCount(); v += iNoOfPolygonIndices)
			{
				// ...Get vertex info
				// Construct an adjuct to represent the poly-vertex
				rexObjectGenericMesh::AdjunctInfo obAdjunctFaker;

				// Get Vertex Index
				int iVertexIndex = pobAArrayOfIndices->getValue()[v + iPolygonVertexInputNo];
				obAdjunctFaker.m_VertexIndex = iVertexIndex;

				// Get normal
				if(obAPolygonVertexNormals.GetCount() > 0)
				{
					// Got a polygon-vertex normal, so use that
					obAdjunctFaker.m_NormalIndex = pobAArrayOfIndices->getValue()[v + iPolygonVertexNormalInputNo];
				}
				else if(obAVertexNormals.GetCount() > 0)
				{
					// Not got a polygon-vertex normal, but do have a vertex normal, so use that
					obAdjunctFaker.m_NormalIndex = iVertexIndex;
				}
				else
				{
					// Not got any normal info
					obAdjunctFaker.m_NormalIndex = 0;
				}

				// Get Colour
				// Do I have Polygon vertex colours
				if( obAPolygonVertexColours.GetCount() > 0 )
				{
					// Got Polygon vertex colour, so use that
					const Color32& obColour = obAPolygonVertexColours[pobAArrayOfIndices->getValue()[v + iPolygonVertexColourInputNo]];
					obAdjunctFaker.m_Color.Set(obColour.GetRedf(), obColour.GetGreenf(), obColour.GetBluef(), obColour.GetAlphaf());
				}
				else if( obAVertexColours.GetCount() > 0 )
				{
					// No polygon-vertex colour, but I do have a vertex colour, so use that
					const Color32& obColour = obAVertexColours[pobAArrayOfIndices->getValue()[v + iPolygonVertexInputNo]];
					obAdjunctFaker.m_Color.Set(obColour.GetRedf(), obColour.GetGreenf(), obColour.GetBluef(), obColour.GetAlphaf());
				}
				else
				{
					// No vertex colours at all, so use mid grey
					obAdjunctFaker.m_Color.Set(0.5f, 0.5f, 0.5f, 1.0f);
				}

				// UVs
				if(obAPolygonVertexTexCoords.GetCount() > 0 )
				{
					// Got UVs, so use them
					obAdjunctFaker.m_TextureCoordinates.PushAndGrow(obAPolygonVertexTexCoords[pobAArrayOfIndices->getValue()[v + iPolygonVertexTexCoordsInputNo]]);
					obAdjunctFaker.m_TextureCoordinateSetNames.PushAndGrow("Fake UVs");
				}


				// Add the adjuct to the poly
				obPolygonFaker.m_Adjuncts.PushAndGrow(obAdjunctFaker);

				// Add the normal
				obPolygonFaker.m_FaceNormal += m_pobRexGenericMesh->m_Normals[obAdjunctFaker.m_NormalIndex];
			}

			// Average the normal
			obPolygonFaker.m_FaceNormal.Normalize();

			// Assign material
			obPolygonFaker.m_MaterialIndex = iPolygonsNo;
			stFakedUpMaterial.m_PrimitiveIndices.PushAndGrow(m_pobRexGenericMesh->m_Primitives.GetCount());

			// Add the Polygon to the mesh
			m_pobRexGenericMesh->m_Primitives.PushAndGrow(obPolygonFaker);
		}

		// Add material
		m_pobRexGenericMesh->m_Materials.PushAndGrow(stFakedUpMaterial);
	}
	//printf("m_pobRexGenericMesh->m_Materials.GetCount() = %d\n", m_pobRexGenericMesh->m_Materials.GetCount());
	//printf("m_pobRexGenericMesh->m_Primitives.GetCount() = %d\n", m_pobRexGenericMesh->m_Primitives.GetCount());
	//printf("m_pobRexGenericMesh->m_Vertices.GetCount() = %d\n", m_pobRexGenericMesh->m_Vertices.GetCount());
}

void intMesh::fromCOLLADAPostProcess()
{
	//	printf("void intMesh::fromCOLLADAPostProcess()\n");
	// INSERT CODE TO POST PROCESS HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericMesh;
	// local->renderingContext = MYGLOBAL::getRenderingContext;
	// MYGLOBAL::registerInstance(local);
}

// EXPORT

void intMesh::createTo(void *userData)
{
	//	printf("void intMesh::createTo(void *userData)\n");
	// INSERT CODE TO CREATE COLLADA DOM OBJECTS HERE
	// _element = new domMesh;
	// m_pobRexGenericMesh = userData;
}

void intMesh::toCOLLADA()
{
	//	printf("void intMesh::toCOLLADA()\n");
	// INSERT CODE TO TRANSLATE TO YOUR RUNTIME HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericMesh;
	// element->foo = local->foo;
	// element->subelem[0]->bar = local->bar;
}

void intMesh::toCOLLADAPostProcess()
{
	//	printf("void intMesh::toCOLLADAPostProcess()\n");
	// INSERT CODE TO POST PROCESS HERE
	// myRuntimeClassType* local = (myRuntimeClassType*)m_pobRexGenericMesh;
	// local->renderingContext = MYGLOBAL::getRenderingContext;
	// MYGLOBAL::registerInstance(local);
}

// ********************************************************
// ***** USER CODE ****************************************
// ********************************************************
intMesh::~intMesh()
{
	//	printf("intMesh::~intMesh()\n");
	/*	if (m_pobRexGenericMesh && m_pobRexGenericMesh->_pVertices)
	{
	delete[] m_pobRexGenericMesh->_pVertices;
	int i;
	for (i=0;i<(int)m_pobRexGenericMesh->_vPolygons.size();i++)
	{
	delete[] m_pobRexGenericMesh->_vPolygons[i]._pIndexes;
	}
	delete m_pobRexGenericMesh;
	}
	*/
	delete m_pobRexGenericMesh;
}

