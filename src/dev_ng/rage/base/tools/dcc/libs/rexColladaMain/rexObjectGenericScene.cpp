#include "rexGeneric/objectMesh.h"
#include "rexGeneric/objectEntity.h"
#include "rexRage/serializerMesh.h"
#include "rexRage/serializerSkeleton.h"
#include "rexRage/serializerEntity.h"
#include "file/stream.h"
#include "file/asset.h"
#include "rexobjectgenericscene.h"

#define WRITEOUT(a) Write(a, (int)strlen(a))

using namespace rage;

rexObjectGenericScene::rexObjectGenericScene(void)
{
}

rexObjectGenericScene::~rexObjectGenericScene(void)
{
	for(int i=0; i<m_obAArrayOfMeshInstances.GetCount(); i++)
	{
		delete m_obAArrayOfMeshInstances[i];
		m_obAArrayOfMeshInstances[i] = NULL;
	}

	for(int i=0; i<m_obAArrayOfSkeletons.GetCount(); i++)
	{
		// This is a bit poo, but when the entity exports the skeleton, it deletes it!  So need to do that here
		// delete m_obAArrayOfSkeletons[i];
		m_obAArrayOfSkeletons[i] = NULL;
	}
}

void rexObjectGenericScene::AddMesh(rexObjectGenericMesh* pobMesh, const Matrix34& obMatrix ) 
{
	// Add the mesh
	m_obAArrayOfMeshes.PushAndGrow(pobMesh); 

	// Add instance info
	rexObjectGenericMeshInstance* pobInstance = new rexObjectGenericMeshInstance();
	pobInstance->m_obMatrix = obMatrix;

	// Does a mesh with this name already exist?
	int iPostFix = 0;
	atString strPossibleInstanceName = pobMesh->GetMeshName();
	bool bNameIsUnique = true;
	do
	{
		bNameIsUnique = true;
		atString strPossibleInstanceNameAsLowerCase = strPossibleInstanceName;
		strPossibleInstanceNameAsLowerCase.Lowercase();
		for(int i=0; i<m_obAArrayOfMeshInstances.GetCount(); i++)
		{
			atString strExistingNameAsLowerCase = m_obAArrayOfMeshInstances[i]->m_strName;
			strExistingNameAsLowerCase.Lowercase();

			if(strExistingNameAsLowerCase == strPossibleInstanceNameAsLowerCase)
			{
				// Name already used :(
				bNameIsUnique = false;

				// Create a new name
				iPostFix++;
				char acTemp[255];
				sprintf(acTemp, "%s_%d", (const char*)pobMesh->GetMeshName(), iPostFix);
				strPossibleInstanceName = acTemp;
				break;
			}
		}
	} while(!bNameIsUnique);

	// Got a unique name, so use it
	pobInstance->m_strName = strPossibleInstanceName;

	// Add mesh to instance
	pobInstance->m_obGenericMesh = pobMesh;

	// Add instance
	m_obAArrayOfMeshInstances.PushAndGrow(pobInstance);
}

void rexObjectGenericScene::AddSkeleton(rexObjectGenericSkeleton* pobSkeleton)
{
	m_obAArrayOfSkeletons.PushAndGrow(pobSkeleton);
}


void rexObjectGenericScene::Export(const char* pcDestinationFolder) const
{
	Displayf("void rexObjectGenericScene::Export(\"%s\") const", pcDestinationFolder);

	// Standardize the slashes
	atString obStrOutputPath(pcDestinationFolder);
	obStrOutputPath += "/";
	obStrOutputPath += GetSceneName();

	// Fake up a generic entity
	rexObjectGenericEntity obFakeGenericEntity;
	obFakeGenericEntity.m_EntityTypeFileName = GetSceneName();
	obFakeGenericEntity.m_EntityTypeFileName += atString(".type");

	// Add any skeletons that I have to the faked entity
	// Export skeletons
	for(int s=0; s<m_obAArrayOfSkeletons.GetCount(); s++)
	{
		// Add the copy to the entity
		obFakeGenericEntity.m_ContainedObjects.PushAndGrow(m_obAArrayOfSkeletons[s]);

		// Export it
		rexSerializerRAGESkeleton obRexSerializerRAGESkeleton;
//		obRexSerializerRAGESkeleton.SetAsciiMode(true);
		obRexSerializerRAGESkeleton.SetOutputPath(obStrOutputPath);
//		obRexSerializerRAGESkeleton.SetExportTextures(true);
		rexResult obRexResult = obRexSerializerRAGESkeleton.Serialize( *((rexObject*)m_obAArrayOfSkeletons[s]) );
		if(obRexResult.GetResultType() != rexResult::PERFECT)
		{
			Errorf("Errors occured\n");
			obRexResult.Display();
		}
	}

	// Add copies of meshes to faked entity
	// Export meshes
	rexObjectGenericMeshHierarchyDrawable* pobListOfMeshesToExport = new rexObjectGenericMeshHierarchyDrawable();
	pobListOfMeshesToExport->m_DeleteContainedObjectsOnDestruction = true;
	for(int m=0; m<m_obAArrayOfMeshInstances.GetCount(); m++)
	{
		// Get a COPY of the mesh as a generic mesh
		rexObjectGenericMesh* pobGeometry = new rexObjectGenericMesh(*(m_obAArrayOfMeshInstances[m]->m_obGenericMesh));

		// Hack its name and position
		pobGeometry->SetMeshName(m_obAArrayOfMeshInstances[m]->m_strName);
		pobGeometry->m_Matrix = m_obAArrayOfMeshInstances[m]->m_obMatrix;

		// Add the copy to the entity
		pobListOfMeshesToExport->m_ContainedObjects.PushAndGrow(pobGeometry);
	}

	// Make it so that the entity kills its own children, nice
	obFakeGenericEntity.m_ContainedObjects.PushAndGrow(pobListOfMeshesToExport);
	obFakeGenericEntity.m_DeleteContainedObjectsOnDestruction = true;

	// Serialize the entity
	rexSerializerRAGEEntity obRexSerializerRAGEEntity;
	rexUtility::CreatePath( obStrOutputPath );
	obRexSerializerRAGEEntity.SetOutputPath(obStrOutputPath);
	rexResult obRexResult = obRexSerializerRAGEEntity.Serialize( obFakeGenericEntity );
	if(obRexResult.GetResultType() != rexResult::PERFECT)
	{
		Errorf("Errors occured\n");
		obRexResult.Display();
	}

	// Flag textures for export
	rexUtility::SetTextureConvertFlag(false);

	// Generate a list of all the materials in this scene
	atArray<rexObjectGenericMesh::MaterialInfo>		obAAllMaterialsInScene;
	for(int m=0; m<m_obAArrayOfMeshInstances.GetCount(); m++)
	{
		// Get as generic mesh
		rexObjectGenericMesh *pGeometry = m_obAArrayOfMeshInstances[m]->m_obGenericMesh;
		for(int iMeshMaterial=0; iMeshMaterial<pGeometry->m_Materials.GetCount(); iMeshMaterial++)
		{
			// Make sure it doesn't already exist
			bool bAddMe = true;
			for(int i=0; i<obAAllMaterialsInScene.GetCount(); i++)
			{
				if( obAAllMaterialsInScene[i].IsSame( pGeometry->m_Materials[iMeshMaterial] ) )
				{
					bAddMe = false;
					break;
				}
			}
			if(bAddMe)
			{
				// Add it
				obAAllMaterialsInScene.PushAndGrow(pGeometry->m_Materials[iMeshMaterial]);
			}
		}
	}

	// printf("obAAllMaterialsInScene = %d\n", obAAllMaterialsInScene.GetCount());

	// Export meshes
	for(int m=0; m<m_obAArrayOfMeshInstances.GetCount(); m++)
	{
		// Get as generic mesh
		rexObjectGenericMesh *pGeometry = m_obAArrayOfMeshInstances[m]->m_obGenericMesh;

		// Hack its name and position
		pGeometry->SetMeshName(m_obAArrayOfMeshInstances[m]->m_strName);
		pGeometry->m_Matrix = m_obAArrayOfMeshInstances[m]->m_obMatrix;

		// Serialize the mesh
		rexSerializerRAGEMesh obRexSerializerRAGEMesh;
		obRexSerializerRAGEMesh.SetAsciiMode(true);
		obRexSerializerRAGEMesh.SetOutputPath(obStrOutputPath);
		obRexSerializerRAGEMesh.SetExportTextures(true);
		obRexResult = obRexSerializerRAGEMesh.SerializeRecursive( *pGeometry, obAAllMaterialsInScene );
		if(obRexResult.GetResultType() != rexResult::PERFECT)
		{
			Errorf("Errors occured\n");
			obRexResult.Display();
		}

		for(int iMaterial=0; iMaterial<pGeometry->m_Materials.GetCount(); iMaterial++)
		{
			for(int iTexture=0; iTexture<pGeometry->m_Materials[iMaterial].m_OutputTextureFileNames.GetCount(); iTexture++)
			{
				// Get input name
				atString strInputPathAndFilename = pGeometry->m_Materials[iMaterial].m_InputTextureFileNames[iTexture];

				// Get output name
				atString strOutputFilename = pGeometry->m_Materials[iMaterial].m_OutputTextureFileNames[iTexture];

				// Is it worth continuing?
				if((strInputPathAndFilename != "") && (strOutputFilename != "") && (strInputPathAndFilename != "none") && (strOutputFilename != "none"))
				{
					atString strOutputPathAndFilename(obStrOutputPath, atString("/", atString(strOutputFilename, ".dds")));

					// Process textures
					obRexResult = rexUtility::ExportTexture( strInputPathAndFilename, strOutputPathAndFilename, false, false, "" );
					if(obRexResult.GetResultType() != rexResult::PERFECT)
					{
						Errorf("Errors occured\n");
						obRexResult.Display();
					}
				}
			}
		}
	}
}

