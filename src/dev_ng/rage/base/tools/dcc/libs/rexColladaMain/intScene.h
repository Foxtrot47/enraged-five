/*
* $Id$
*
* Copyright Sony Computer Entertainment Inc. 2005
* All Rights Reserved.
*/

#ifndef __intScene_h__
#define __intScene_h__
#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include "dae/daeIntegrationObject.h"
#include "dae/daeMetaElement.h"
#include "dom/domNode.h"
#pragma warning(pop)

#include "rexObjectGenericScene.h"

using namespace rage;

class intScene;

typedef daeSmartRef<intScene> intSceneRef;
typedef daeTArray<intSceneRef> intSceneArray;

class intScene : public daeIntegrationObject
{
public: // VIRTUAL INTEGRATION INTERFACE
	// This Method is used to create tool/runtime object
	virtual void createFrom(daeElementRef element);
	
	// This method translate from COLLADA to tool/runtime object
	virtual void fromCOLLADA();

	// This method is used as a second pass post process on tool/runtime objs
	virtual void fromCOLLADAPostProcess();

		// This Method is used to create tool/runtime object
	virtual void createTo(void *userData);

	// This method translate from COLLADA to tool/runtime object
	virtual void toCOLLADA();

	// This method is used as a second pass post process on tool/runtime objs
	virtual void toCOLLADAPostProcess();

public: // STATIC METHODS
	static daeElementRef create(daeInt bytes);
	static daeMetaElement * registerElement();
	
public: // STATIC MEMBERS
	static daeMetaElement* _Meta;

public: // USER CODE
	virtual ~intScene();
	const rexObjectGenericScene* getScene() { return m_pobRexGenericScene; }

private: // USER CODE
	rexObjectGenericScene *m_pobRexGenericScene;
	daeElement* _element;

	// A function that works through the node heirarchy of a scene, adding meshes as it goes
	void ProcessSceneNode(domNode* pobNode, const Matrix34& obCurrentMatrix, rexObjectGenericSkeleton::Bone* pobParentJoint = NULL);

	// A hacky function that determins which side of an animation a transform belongs
	enum BoneMatrixStage {
		INITIAL,
		PRE_ANIMATION,
		POST_ANIMATION
	};
	BoneMatrixStage IsItAPreAnimTransformation(const char* pcTransformationSid) const;

	// Used to apply collada based matrix operations to a rage matrix
	Matrix34 ApplyColladaMatrixOperation(const Matrix34& obInMatrix, daeElement* pobOperationElement) const;

	// A function that works through the node heirarchy of a skeleton, adding joints as it goes
	//	void ProcessJointNode(domNode* pobNode, const Matrix34& obCurrentMatrix);
};

#endif

