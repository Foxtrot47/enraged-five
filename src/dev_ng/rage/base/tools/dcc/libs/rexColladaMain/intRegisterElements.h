/*
* $Id$
*
* Copyright Sony Computer Entertainment Inc. 2005
* All Rights Reserved.
*/

/*#include "intCOLLADA.h"
#include "intAccessor.h"
#include "intArray.h"
#include "intInput.h"
#include "intParam.h"
#include "intSource.h"
*/
#include "intMesh.h"
/*
#include "intCombiner.h"
#include "intJoints.h"
#include "intLines.h"
#include "intLinestrips.h"
#include "intPolygons.h"
#include "intTriangles.h"
#include "intTrifans.h"
#include "intTristrips.h"
#include "intVertices.h"
#include "intLookat.h"
#include "intMatrix.h"
#include "intPerspective.h"
#include "intRotate.h"
#include "intScale.h"
#include "intSkew.h"
#include "intTranslate.h"
#include "intImage.h"
#include "intLight.h"
*/
#include "intMaterial.h"
/*
#include "intPass.h"
#include "intShader.h"
#include "intTexture.h"
#include "intCode.h"
#include "intEntry.h"
#include "intProgram.h"
#include "intCamera.h"
#include "intInstance.h"
#include "intChannel.h"
#include "intSampler.h"
#include "intSkin.h"
#include "intAsset.h"
#include "intExtra.h"
#include "intBoundingbox.h"
#include "intNode.h"
*/
#include "intScene.h"
/*
#include "intAnimation.h"
#include "intController.h"
#include "intGeometry.h"
#include "intLibrary.h"
*/

void intRegisterElements();

