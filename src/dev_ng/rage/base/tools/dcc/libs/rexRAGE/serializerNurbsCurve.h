// 
// rexRAGE/serializerNurbsCurve.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REX_SERIALIZER_NURBS_CURVE_H__
#define __REX_SERIALIZER_NURBS_CURVE_H__

#include "rexBase/serializer.h"
#include "rexBase/utility.h"

namespace rage {
	//
	// PURPOSE:
	//	Currently not used on its on yet.  Both Entity and Level
	//  write curves directly using WriteCurveInfo().
	class rexSerializerRAGENurbsCurve : public rexSerializer
	{
	public:
		rexSerializerRAGENurbsCurve() : rexSerializer() {}

		virtual rexResult	Serialize( rexObject& object ) const;

		// for use by other serializers
		static rexResult WriteCurveInfo(rexObject& object,const char* fileName, bool bSerializeVersionTwoCurves = false);

		virtual		rexSerializer* CreateNew() const  
		{ 
			rexSerializerRAGENurbsCurve* s = new rexSerializerRAGENurbsCurve();
			return s;
		}
	};
}

#endif // #ifndef __REX_SERIALIZER_NURBS_CURVE_H__
