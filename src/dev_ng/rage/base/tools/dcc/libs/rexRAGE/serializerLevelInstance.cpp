#include "serializerLevelInstance.h"
#include "rexGeneric/objectLevelInstance.h"
#include "rexBase/utility.h"
#include "rexBase/shell.h"

#include "file/asset.h"

#include <string.h>
#include <process.h>

using namespace rage;

rexResult rexSerializerLevelInstance::Serialize( rexObject& object ) const
{	
	rexResult result( rexResult::PERFECT );

	if(rexShell::DoesntSerialise())
	{
		return result;
	}

	// type checking
	rexObjectLevelInstanceGroup* group = dynamic_cast<rexObjectLevelInstanceGroup*>( &object );
	if( !group )
	{
		return result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	ASSET.SetPath( GetOutputPath() );

	// write to our fiStream	
	rexUtility::CreatePath( GetOutputPath() );

//	for( int a = 0; a < 2; a++ )
	for( int a = 0; a < 1; a++ )  // disbled non-banger instances
	{
		fiStream* myStream = GetExportData() ? ASSET.Create( a ? "instance" : "banger", "types" ) : NULL;

		if( GetExportData() && !myStream )
		{
			// Oh dear, something bad has happened
			result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

			// Get full path for debugging
			char acFullPathForDebugging[255];
			ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),(a ? "instance" : "banger"), "types" );

			// Flip the slashes
			for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

			// Error
			result.AddError( "Could not create file '%s'", acFullPathForDebugging);
		}
		else 
		{		
			if( myStream )
			{
				char buffer[512];

				sprintf( buffer, "Version: 1\r\n" );
				myStream->Write( buffer, (int)strlen( buffer ) );

				int objectCount = group->m_ContainedObjects.GetCount();
				for( int b = 0; b < objectCount; b++ )
				{
					atArray<atString> outputPathStack;
					//				outputPathStack.Push( GetOutputPath() );


					outputPathStack.Push( atString("level/dc/bangers") );
//					outputPathStack.Push( a ? "level/dc_dist/instances/props" : "level/dc_dist/instances/bangers" ); // N8HACK -- this is lame, but will be fixed gracefully shortly
					result &= SerializeRecursively( *group->m_ContainedObjects[ b ], myStream, outputPathStack, (a == 0) );
				}

				myStream->Close();			
			}
		}
	}

	return result;
}

static void Indent( fiStream* myStream, int indent )
{
	atString buffer;
	for( int a = 0; a < indent; a++ )
		buffer += "\t";
	myStream->Write( (const char*)buffer, buffer.GetLength() );
}

static atString PathStackToPath( const atArray<atString>& outputPathStack )
{
	atString ret;
	int count = outputPathStack.GetCount();
	for( int a = 0; a < count; a++ )
	{
		ret += outputPathStack[ a ];
		ret += "/";
	}

	return ret;
}

rexResult rexSerializerLevelInstance::SerializeRecursively( rexObject& object, fiStream* myStream, atArray<atString>& outputPathStack, bool doBangers, int indent ) const
{
	rexResult result;
	rexObjectLevelInstance* inst = dynamic_cast<rexObjectLevelInstance*>( &object );
	if( inst )
	{	
		int outputPathCount = outputPathStack.GetCount();
				
		char buffer[4096];

		bool needToOutputCloseBrace = true;
		switch( inst->m_SubType )
		{
			case rexObjectLevelInstance::INSTANCED_OBJECT:
			{
				if( doBangers )
				{
					needToOutputCloseBrace = false;
					break;
				}

				Indent( myStream, indent );
				sprintf( buffer, "Instance %s  {\r\n", (const char*)inst->m_Name );
				myStream->Write( buffer, (int)strlen( buffer ) );

				atString outputPath = PathStackToPath( outputPathStack );				

				Indent( myStream, indent + 1 );
				sprintf( buffer, "entity \"%s%s/entity.type\"\r\n", (const char*)outputPath, (const char*)inst->m_Name );
				myStream->Write( buffer, (int)strlen( buffer ) );

				break;
			}

			case rexObjectLevelInstance::BANGER_ROOT_GROUP:
			{
				if( !doBangers )
				{
					needToOutputCloseBrace = false;
					break;
				}
				Indent( myStream, indent );
				sprintf( buffer, "BangerRootGroup %s  {\r\n", (const char*)inst->m_Name );
				myStream->Write( buffer, (int)strlen( buffer ) );

				Indent( myStream, indent + 1 );
				sprintf( buffer, "MinMoveForce %f\r\n", inst->m_Data.m_RootGroupData.m_MinMoveForce );
				myStream->Write( buffer, (int)strlen( buffer ) );

				outputPathStack.Push( inst->m_Name );

				break;
			}
			case rexObjectLevelInstance::BANGER_BREAKABLE_GROUP:
			{
				if( !doBangers )
				{
					needToOutputCloseBrace = false;
					break;
				}
				Indent( myStream, indent );
				sprintf( buffer, "BangerGroup  {\r\n"  );
				myStream->Write( buffer, (int)strlen( buffer ) );

				Indent( myStream, indent + 1 );
				sprintf( buffer, "MinHitForce %f\r\n", inst->m_Data.m_RootGroupData.m_MinMoveForce );
				myStream->Write( buffer, (int)strlen( buffer ) );

				Indent( myStream, indent + 1 );
				sprintf( buffer, "BreakHealth %f\r\n", inst->m_Data.m_BangerGroupData.m_BreakHealth );
				myStream->Write( buffer, (int)strlen( buffer ) );
				
				Indent( myStream, indent + 1 );
				sprintf( buffer, "ForceTransmissionScaleUp %f\r\n", inst->m_Data.m_BangerGroupData.m_ForceTransmissionScaleUp );
				myStream->Write( buffer, (int)strlen( buffer ) );
				
				Indent( myStream, indent + 1 );
				sprintf( buffer, "ForceTransmissionScaleDown %f\r\n", inst->m_Data.m_BangerGroupData.m_ForceTransmissionScaleDown );
				myStream->Write( buffer, (int)strlen( buffer ) );
				
				Indent( myStream, indent + 1 );
				sprintf( buffer, "ShatterBreakChance %f\r\n", inst->m_Data.m_BangerGroupData.m_ShatterBreakChance );
				myStream->Write( buffer, (int)strlen( buffer ) );
				
				Indent( myStream, indent + 1 );
				sprintf( buffer, "NonPristineMaxFrameTicks %d\r\n", inst->m_Data.m_BangerGroupData.m_NonPristineMaxFrameTicks );
				myStream->Write( buffer, (int)strlen( buffer ) );

				outputPathStack.Push( inst->m_Name );

				break;
			}
			case rexObjectLevelInstance::BANGER_CHILD:
			{
				if( !doBangers )
				{
					needToOutputCloseBrace = false;
					break;
				}
				Indent( myStream, indent );
				sprintf( buffer, "BangerChild  {\r\n"  );
				myStream->Write( buffer, (int)strlen( buffer ) );

				Indent( myStream, indent + 1 );
				sprintf( buffer, "MinDamageForce %f\r\n", inst->m_Data.m_BangerChildData.m_MinDamageForce );
				myStream->Write( buffer, (int)strlen( buffer ) );

				Indent( myStream, indent + 1 );
				sprintf( buffer, "DamageHealth %f\r\n", inst->m_Data.m_BangerChildData.m_DamageHealth );
				myStream->Write( buffer, (int)strlen( buffer ) );

				Indent( myStream, indent + 1 );
				sprintf( buffer, "PristineMass %f\r\n", inst->m_Data.m_BangerChildData.m_PristineMass );
				myStream->Write( buffer, (int)strlen( buffer ) );

				Indent( myStream, indent + 1 );
				sprintf( buffer, "DamagedMass %f\r\n", inst->m_Data.m_BangerChildData.m_DamagedMass );
				myStream->Write( buffer, (int)strlen( buffer ) );

				atString outputPath = PathStackToPath( outputPathStack );				
				atString pristineName, damagedName;

				int childCount = inst->m_ContainedObjects.GetCount();
				for( int a = 0; a < childCount; a++ )
				{
					rexObjectLevelInstance* child = dynamic_cast<rexObjectLevelInstance*>( inst->m_ContainedObjects[ a ] );
					if( child )
					{
						if( child->m_SubType == rexObjectLevelInstance::BANGER_DAMAGED_GROUP )
						{
							damagedName = child->m_Name;
						}
						else if( child->m_SubType == rexObjectLevelInstance::BANGER_UNDAMAGED_GROUP )
						{
							pristineName = child->m_Name;
						}
					}
				}

				if( pristineName.GetLength() )
				{
					Indent( myStream, indent + 1 );
					sprintf( buffer, "pristineEntity \"%s%s/entity.type\"\r\n", (const char*)outputPath, (const char*)pristineName );
					myStream->Write( buffer, (int)strlen( buffer ) );
				}
				
				if( damagedName.GetLength() )
				{
					Indent( myStream, indent + 1 );
					sprintf( buffer, "damagedEntity \"%s%s/entity.type\"\r\n", (const char*)outputPath, (const char*)damagedName );
					myStream->Write( buffer, (int)strlen( buffer ) );
				}				
				break;
			}
			case rexObjectLevelInstance::BANGER_DAMAGED_GROUP:
			case rexObjectLevelInstance::BANGER_UNDAMAGED_GROUP:
			{
				if( !doBangers )
				{
					needToOutputCloseBrace = false;
					break;
				}
				needToOutputCloseBrace = false;
				break;
			}
			default:
				needToOutputCloseBrace = false;
				Assert(0);
				break;
		}
		
		int childCount = inst->m_ContainedObjects.GetCount();
		for( int a = 0; a < childCount; a++ )
		{
			result &= SerializeRecursively( *inst->m_ContainedObjects[ a ], myStream, outputPathStack, doBangers, indent + 1 );
		}

		if( needToOutputCloseBrace )
		{
			Indent( myStream, indent );
			sprintf( buffer, "}\r\n" );
			myStream->Write( buffer, (int)strlen( buffer ) );
		}

		while( outputPathCount < outputPathStack.GetCount() )
			outputPathStack.Pop();
	}
	else
	{
		result.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}
	return result;
}
