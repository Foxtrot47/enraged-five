#define REQUIRE_IOSTREAM

#include "rexRAGE/serializerSkeleton.h"
#include "rexGeneric/objectSkeleton.h"
#include "rexBase/utility.h"
#include "rexBase/module.h"
#include "rexBase/shell.h"

#include "diag/output.h"
#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"
#include "cranimation/animtrack.h"

///////////////////////////////////////////////////////////////////////////////////////////
namespace rage {

rexResult rexSerializerRAGESkeleton::Serialize( rexObject& object ) const
{	
	rexResult result( rexResult::PERFECT );

	if( !GetExportData() )
		return result;

	const rexObjectGenericSkeleton* inputSkeleton = dynamic_cast<const rexObjectGenericSkeleton*>( &object );

	if( !inputSkeleton || !inputSkeleton->m_RootBone )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	if( !inputSkeleton->GetBoneCount() )
		return result;

	rexUtility::CreatePath( GetOutputPath() );
	ASSET.SetPath( GetOutputPath() );

	fiStream* stream = ASSET.Create( inputSkeleton->GetName(), "skel" );
	if( stream )
	{
		fiAsciiTokenizer tok;
		tok.Init( inputSkeleton->GetName(), stream );

		if( m_ProgressBarTextCB )
		{
			static char message[1024];
			sprintf( message, "Serializing skeleton %s.skel", (const char*)inputSkeleton->GetName() );
			(*m_ProgressBarTextCB) ( message );
		}

		const AnimExportCtrlSpec *pSpec = NULL;
		if(m_bUseAnimCtrlExportFile && m_bOmitMissingTracks)
			pSpec = &m_AnimExportCtrlSpec;
		int trackedBoneNum = inputSkeleton->GetBoneCount(pSpec);

		if( m_ProgressBarObjectCountCB )
			(*m_ProgressBarObjectCountCB)( trackedBoneNum );

		char buffer[512];


		sprintf( buffer, "Version: 109\r\n" );
		stream->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "AuthoredOrientation %d\r\n", inputSkeleton->m_AuthoredOrientation );
		stream->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "NumBones %d\r\n", trackedBoneNum );
		stream->Write( buffer, (int)strlen( buffer ) );

		if( inputSkeleton->m_RootBone )
		{			
			atArray<u16> boneIDs;
			boneIDs.Reserve(trackedBoneNum);

			atArray<atString> boneNames;
			boneNames.Reserve(trackedBoneNum);

			int boneIndex = 0;
			int serialsedBones = SerializeRecursively( *inputSkeleton, *inputSkeleton->m_RootBone, boneIDs, boneNames, stream, result, 0, boneIndex);
			if(serialsedBones<0 || boneIndex != trackedBoneNum)
			{
				result.Set( rexResult::ERRORS, rexResult::INVALID_INPUT );
				result.AddError("Skeleton '%s' failed to serialize correctly", inputSkeleton->GetName().c_str());
			}
		}

		stream->Close();
		if(rexShell::DoesntSerialise())
		{
			const char *filename = stream->GetName();
			result.AddMessage("Deleting file %s due to no-serialse flag.", filename);
			remove(filename);
		}
	}	
	else
	{
		// Oh dear, something bad has happened
		result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),inputSkeleton->GetName(), "skel" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		result.AddError( "Could not create file '%s'", acFullPathForDebugging);
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////

void rexSerializerRAGESkeleton::Indent( fiStream* s, int numTabs ) const
{
	for( int a = 0; a < numTabs; a++ )
		s->Write( "\t", 1 );
}

int rexSerializerRAGESkeleton::SerializeUsingControlFile(fiStream* s, int boneIndex, const rexObjectGenericSkeleton::Bone& currBone, rexResult& result) const
{
	char buffer[ 512 ];

	const AnimExportCtrlTrackSpec* trackSpec = NULL;
		
	if(boneIndex == 0)
	{
		//This is the root bone, so try to get the root spec from the control file.
		trackSpec = m_AnimExportCtrlSpec.FindTrackSpec("root");
		if(!trackSpec)
		{
			//The "root" keyword wasn't listed in the control spec, so try getting the spec by the bone name
			trackSpec = m_AnimExportCtrlSpec.FindTrackSpec( (const char*)currBone.m_Name );
		}
	}
	else
	{
		//The bone isn't the root, so retrieve the track spec by name
		trackSpec = m_AnimExportCtrlSpec.FindTrackSpec( (const char*)currBone.m_Name );
		if( (!trackSpec && !m_bOmitMissingTracks) || (trackSpec && (strcmp(trackSpec->GetNameExpr(), "*") == 0)) )
		{
			//Either a track spec wasn't found, or the spec that was found includes all bones, so try
			//to search the bones parent bones looking for a track specification that matches one of them
			//which is marked to include its children.
			rexObjectGenericSkeleton::Bone *pParentBone = currBone.m_Parent;
			while(pParentBone)
			{
				const AnimExportCtrlTrackSpec* parentTrackSpec = m_AnimExportCtrlSpec.FindTrackSpec( (const char*)pParentBone->m_Name );
				if(parentTrackSpec)
				{
					if(parentTrackSpec->IncludesChildren() && (strcmp(parentTrackSpec->GetNameExpr(), "*") != 0))
					{
						//The parent track specification isn't the default fallthrough of "*", and is marked
						//as including its children, so use this track specification
						trackSpec = parentTrackSpec;
						break;
					}
				}
				pParentBone = pParentBone->m_Parent;
			}
		}
	}
	
	if(!trackSpec)
	{
		if(!m_bOmitMissingTracks)
		{
			result.Combine(rexResult::ERRORS);
			result.AddErrorCtx(currBone.m_Name.c_str(), "No animation export specification could be found for the joint '%s'. Add \"SkeletonOmitMissingTracks\" property to have missing ones culled off the skeleton.", (const char*)currBone.m_Name);
			return -1;
		}
		else
			return 1;
	}

	atString cmpTrackOutName;

	int trackCount = trackSpec->GetTrackCount();
	for(int trackIdx=0; trackIdx<trackCount; trackIdx++)
	{
		const AnimExportCtrlTrack& ctrlTrack = trackSpec->GetTrack(trackIdx);
		int cmpCount = ctrlTrack.GetNumComponentParts();
		for(int cmpIdx=0; cmpIdx<cmpCount; cmpIdx++)
		{
			ctrlTrack.BuildComponentOutputName(cmpIdx, cmpTrackOutName);
			sprintf( buffer, "%s ", (const char*)cmpTrackOutName);
			s->Write( buffer, (int)strlen(buffer) );

			atString cmpAttribName(ctrlTrack.GetInputName());
			cmpAttribName += ctrlTrack.GetComponentPart(cmpIdx);

			if( m_WriteLimitAndLockInfo )
			{
				int boneChannelCount = currBone.m_Channels.GetCount();
				bool foundBoneChannel = false;
				for( int b=0; b<boneChannelCount; b++)
				{
					const rexObjectGenericSkeleton::Bone::ChannelInfo& channel = currBone.m_Channels[ b ];
#if HACK_GTA4
					// Entries need to be of the form "transX" rather than "translateX" although we can change this to standardise
					if( strcmpi((const char*)channel.m_Name,(const char*)cmpTrackOutName) == 0 )
#else
					if( strcmpi((const char*)channel.m_Name,(const char*)cmpAttribName) == 0 )
#endif // HACK_GTA4
					{
						if( channel.m_IsLocked )
						{
							sprintf( buffer, "lock %f ", channel.m_Min );
							s->Write( buffer, (int)strlen( buffer ) );
						}
						else if( channel.m_IsLimited )
						{
							sprintf( buffer, "limit %f %f ", channel.m_Min, channel.m_Max );
							s->Write( buffer, (int)strlen( buffer ) );
						}
						foundBoneChannel = true;
						break;
					}
				}
				if( !foundBoneChannel )
				{
					result.AddMessageCtx(currBone.m_Name.c_str(), "Found mismatched channel information");
					return -1;
				}
			}
		}
	}

	return 1;
}

int rexSerializerRAGESkeleton::SerializeUsingChannelMap(fiStream* s, int boneIndex, const rexObjectGenericSkeleton::Bone& currBone, rexResult& result) const
{
	char buffer[ 512 ];

	int channelCount = m_ChannelInputNames.GetCount();
	Assert(( channelCount == m_ChannelOutputNames.GetCount() ) || ( channelCount == m_ChannelSupportedForRootBoneOnly.GetCount() ));

	for( int a = 0; a < channelCount; a++ )
	{
		if( ( boneIndex == 0 ) || !m_ChannelSupportedForRootBoneOnly[ a ] )		
		{
			sprintf( buffer, "%s ", (const char*)m_ChannelOutputNames[ a ] );
			s->Write( buffer, (int)strlen( buffer ) );

			if( m_WriteLimitAndLockInfo )
			{
				int boneChannelCount = currBone.m_Channels.GetCount();
				int foundBoneChannel = false;
				for( int b = 0; b < boneChannelCount; b++ )
				{
					const rexObjectGenericSkeleton::Bone::ChannelInfo& channel = currBone.m_Channels[ b ];
					if( channel.m_Name == m_ChannelInputNames[ a ] )
					{
						if( channel.m_IsLocked )
						{
							sprintf( buffer, "lock %f ", channel.m_Min );
							s->Write( buffer, (int)strlen( buffer ) );
						}
						else if( channel.m_IsLimited )
						{
							sprintf( buffer, "limit %f %f ", channel.m_Min, channel.m_Max );
							s->Write( buffer, (int)strlen( buffer ) );
						}
						foundBoneChannel = true;
						break;
					}
				}
				if( !foundBoneChannel )
				{
					result.AddMessageCtx(currBone.m_Name.c_str(), "Found mismatched channel information");
					return -1;
				}
			}			
		}
	}

	return 1;
}

int rexSerializerRAGESkeleton::SerializeRecursively( const rexObjectGenericSkeleton& skeleton, const rexObjectGenericSkeleton::Bone& currBone, atArray<u16>& boneIDs, atArray<atString>& boneNames, fiStream* s, rexResult& result, int indent, int &boneIndex ) const
{	
	if( m_ProgressBarObjectCurrentIndexCB )
		(*m_ProgressBarObjectCurrentIndexCB)( boneIndex );	

	int childCount = currBone.m_Children.GetCount();
	bool writtenSectionForThisBone = false;

	if(boneIndex == 0 || !m_bUseAnimCtrlExportFile || m_AnimExportCtrlSpec.FindTrackSpec( currBone.m_Name.c_str() ) )
	{
		char buffer[ 512 ];
		Indent( s, indent );
		sprintf( buffer, "bone%d %s {\r\n", boneIndex,(const char*) currBone.m_Name );
		s->Write( buffer, (int)strlen( buffer ) );

		u16 boneID = u16(boneIndex);

		if(GetUseBoneIDs() || currBone.m_BoneID.GetLength())
		{
			// if not root, hash the bone id (or bone name)
			if(boneIndex)
			{
				const char* text = currBone.m_Name;
				if(currBone.m_BoneID.GetLength() && currBone.m_BoneID != ".")
				{
					text = currBone.m_BoneID;
				}
				boneID = crAnimTrack::ConvertBoneNameToId(text);
			}
			else
			{
				if(currBone.m_BoneID.GetLength())
				{
					result.AddMessageCtx(currBone.m_Name.c_str(), "Bone ID '%s' found on root bone '%s' (ignoring, this is a warning only)", (const char*)currBone.m_BoneID, (const char*)currBone.m_Name);
				}
			}
		}

		// check for duplication, or zero (which is reserved for the root)
		if(!boneID && boneIndex)
		{
			result.Combine(rexResult::ERRORS);
			result.AddError("Bone ID '%s' from bone '%s' hashes to value reserved for root bones only", (const char*)currBone.m_BoneID, (const char*)currBone.m_Name);
			return -1;
		}
		for(int i=0; i<boneIDs.GetCount(); i++)
		{
			if(boneID == boneIDs[i])
			{
				result.Combine(rexResult::ERRORS);
				result.AddError("Bone ID '%s' from bone '%s' clashes when hashed with bone[%d] ('%s')", (const char*)currBone.m_BoneID, (const char*)currBone.m_Name, i, (const char*)boneNames[i]);
				return -1;
			}
		}

		// keep record of bone IDs and names
		boneIDs.Append() = boneID;
		boneNames.Append() = currBone.m_Name;

		// output, if not ID not same as index
		if(boneID != boneIndex)
		{
			Indent( s, indent + 1 );
			sprintf( buffer, "boneid %d\r\n", boneID);
			s->Write( buffer, (int)strlen( buffer ) );
		}

		Indent( s, indent + 1 );

		//ignore only the root bone transform
		if (currBone.m_Parent == NULL && GetIgnoreSkeletonRootXform())
		{
			sprintf( buffer, "offset %f %f %f\r\n", 0.0f, 0.0f, 0.0f );
			s->Write( buffer, (int)strlen( buffer ) );

			if( m_ExportEulers )
			{
				Indent( s, indent + 1 );
				sprintf( buffer, "euler %f %f %f\r\n", 0.0f, 0.0f, 0.0f );
				s->Write( buffer, (int)strlen( buffer ) );
			}
		}
		else
		{
			Vector3 offset;
			if( currBone.m_IsJoint )
			{
				offset=currBone.m_Offset;
			}
			else
			{
				offset=currBone.GetBoneMatrix().d;
			}

			offset.Scale(GetUnitTypeLinearConversionFactor());
			sprintf( buffer, "offset %f %f %f\r\n", offset.x, offset.y, offset.z );
			s->Write( buffer, (int)strlen( buffer ) );

			if( m_ExportEulers )
			{
				Vector3 eulers;
				currBone.GetBoneMatrix().ToEulersXYZ( eulers );
				Indent( s, indent + 1 );
				sprintf( buffer, "euler %f %f %f\r\n", eulers.x, eulers.y, eulers.z );
				s->Write( buffer, (int)strlen( buffer ) );
			}
		}
		

		if( 1 )  // TODO - optional export of joint orients?
		{
			Vector3 orient;
			currBone.m_JointOrient.ToEulersXYZ( orient );
			Indent( s, indent + 1 );
			sprintf( buffer, "orient %f %f %f\r\n", orient.x, orient.y, orient.z );
			s->Write( buffer, (int)strlen( buffer ) );
		}

		if( 1 )  // TODO - optional export of scale orients?
		{
			Vector3 sorient;
			currBone.m_ScaleOrient.ToEulersXYZ( sorient );
			Indent( s, indent + 1 );
			sprintf( buffer, "sorient %f %f %f\r\n", sorient.x, sorient.y, sorient.z );
			s->Write( buffer, (int)strlen( buffer ) );
		}

		Indent( s, indent + 1 );

		//Serialize the skeleton data using either the control file, or the channel map generated from the "ChannelsToWrite" property
		if(m_bUseAnimCtrlExportFile)
			SerializeUsingControlFile(s, boneIndex, currBone, result);
		else
			SerializeUsingChannelMap(s, boneIndex, currBone, result);

		sprintf( buffer, "\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );

		writtenSectionForThisBone = true;
		boneIndex ++;
	}
	else if(!m_bOmitMissingTracks)
	{
		result.Combine(rexResult::ERRORS);
		result.AddError("No animation export specification could be found for the joint '%s'. Add \"SkeletonOmitMissingTracks\" property to have missing ones being culled off the skeleton (including children!).", (const char*)currBone.m_Name);
		return -1;
	}

	// Do this now anyway. Even though partial skeletons might be output into one file. bug 434073
	for( int a = 0; a < childCount; a++ )
	{
		int childIndent = indent;
		if(writtenSectionForThisBone)
			childIndent += 1;
		int res = SerializeRecursively( skeleton, *currBone.m_Children[ a ], boneIDs, boneNames, s, result, childIndent, boneIndex );
		if( res < 0 )
			return -1;
	}

	if(writtenSectionForThisBone)
	{
		Indent( s, indent );
		s->Write( "}\r\n", 3 );

		return 1;
	}

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////////
rexResult  rexPropertySkeletonWriteLimitAndLockInfo::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGESkeleton* skelSer = dynamic_cast<rexSerializerRAGESkeleton*>( module.m_Serializer );
	if( skelSer )
	{
		skelSer->SetWriteLimitAndLockInfo( value.toBool( skelSer->GetWriteLimitAndLockInfo() ) );	
		return rexResult( rexResult::PERFECT );
	}

	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////
rexResult  rexPropertySkeletonOmitMissingTracks::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGESkeleton* skelSer = dynamic_cast<rexSerializerRAGESkeleton*>( module.m_Serializer );
	if( skelSer )
	{
		skelSer->SetOmitMissingTracks( value.toBool( skelSer->GetOmitMissingTracks() ) );	
		return rexResult( rexResult::PERFECT );
	}

	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult	rexPropertySkeletonExportEulers::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/  ) const
{
	rexResult retval( rexResult::PERFECT );

	rexSerializerRAGESkeleton* ser = dynamic_cast<rexSerializerRAGESkeleton*>( module.m_Serializer );

	if( ser )
	{
		ser->SetExportEulers( value.toBool( ser->GetExportEulers() ) );
	}
	else
	{
		retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult	rexPropertyIgnoreSkeletonRootXform::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/  ) const
{
	rexResult retval( rexResult::PERFECT );

	rexSerializerRAGESkeleton* ser = dynamic_cast<rexSerializerRAGESkeleton*>( module.m_Serializer );

	if( ser )
	{
		ser->SetIgnoreSkeletonRootXform( value.toBool( ser->GetIgnoreSkeletonRootXform() ) );
	}
	else
	{
		retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	return retval;
}

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////
