// 
// rexRAGE/serializerMesh.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSerializerRAGEMesh
//			 -- generates .mesh files from generic mesh hierarchy information
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexSerializer
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_RMCORE_SERIALIZER_GENERICMESHHIERARCHYTORMCOREMESH_H__
#define __REX_RMCORE_SERIALIZER_GENERICMESHHIERARCHYTORMCOREMESH_H__

#include "mesh/mesh.h"
#include "rexBase/property.h"
#include "rexBase/serializer.h"
#include "rexBase/utility.h"
#include "rexGeneric/objectMesh.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class mshMesh;

class rexSerializerRAGEMesh : public rexSerializer
{
public:
	enum  PolyMode
	{
		POLYGON,
		TRISTRIP,
		TRIANGLE,
	};

	rexSerializerRAGEMesh() 
		: rexSerializer() 
		, m_ExpectedVertexCount("")
	{ 
#if HACK_GTA4
		m_OptimiseMaterials = true;
#endif // HACK_GTA4
		m_ExportTextures = true; 
		m_ExportEntityTextureList = true;
		m_EntityTextureListKeepExtensions = false;
		m_MaxMeshMemorySize = -1; 
		m_ExportRootBoneMeshesRelativeToMeshCenter = false; 
		m_ExportRootBoneMeshesRelativeToPivot = false; 
		m_PolyMode = TRISTRIP; 
		m_UseChildrenOfLevelInstanceNodes = false; 
		m_DisableTriStripping = true;
		m_MaxMatricesPerMaterial = mshMaxMatricesPerMaterial;
		m_UseNonOptimizedMTLCompare = false;
		m_RemoveDegenerates = true;
		m_WeldWithoutQuantize = false;
		m_Weld = true;
		m_SplitBlendsIntoUniqueMeshes = false;
		m_MicroMorphs = false;

		// we set this here since this is dependent on the run time assets:
		 rexObjectGenericMesh::VertexInfo::sm_MaxMatricesPerVertex=mshMaxMatricesPerVertex;
	}

	virtual rexResult	Serialize( rexObject& object ) const;

	virtual rexResult	SerializeRecursive( const rexObject& object, const atArray<rexObjectGenericMesh::MaterialInfo>& materialInfo) const;

	void SetUseChildrenOfLevelInstanceNodes( bool b )						{ m_UseChildrenOfLevelInstanceNodes = b; }
	bool GetUseChildrenOfLevelInstanceNodes() const							{ return m_UseChildrenOfLevelInstanceNodes; }

	void SetDisableTriStripping(bool b ) { m_DisableTriStripping = b; }
	bool GetDisableTriStripping() const { return m_DisableTriStripping; }

	void SetMaxMatricesPerMaterial(int max) { m_MaxMatricesPerMaterial = max; }
	int	 GetMaxMatricesPerMaterial() const { return m_MaxMatricesPerMaterial; }

	static void SetTristrippingMode(bool flag);
	static void SetAsciiMode(bool flag);
	bool HasMeshDegenerateTriangles(mshMesh& mesh) const;

	virtual		rexSerializer* CreateNew() const  
	{ 
		rexSerializerRAGEMesh* s = new rexSerializerRAGEMesh();
		s->SetPolyMode( m_PolyMode );
		s->SetMaxMeshMemorySize( m_MaxMeshMemorySize );
		s->SetExportRootBoneMeshesRelativeToMeshCenter( m_ExportRootBoneMeshesRelativeToMeshCenter );
		return s;
	}

	void		SetTextureOutputPath( const char *s )					{ rexUtility::ConvertBackslashesToSlashes( s, m_TextureOutputPath ); }
	void		SetMaxMeshMemorySize( int s )							{ m_MaxMeshMemorySize = s; }
	void		SetExportRootBoneMeshesRelativeToMeshCenter( bool b )	{ m_ExportRootBoneMeshesRelativeToMeshCenter = b; m_ExportRootBoneMeshesRelativeToPivot = false; }
	void		SetExportRootBoneMeshesRelativeToPivot( bool b )		{ m_ExportRootBoneMeshesRelativeToPivot = b; m_ExportRootBoneMeshesRelativeToMeshCenter = false; }

	bool		GetExportRootBoneMeshesRelativeToMeshCenter()			{ return m_ExportRootBoneMeshesRelativeToMeshCenter; }
	bool		GetExportRootBoneMeshesRelativeToPivot()				{ return m_ExportRootBoneMeshesRelativeToPivot; }

	void		SetPolyMode( PolyMode pm )								{ Assert( pm >= POLYGON && pm <= TRIANGLE ); m_PolyMode = pm; }
	PolyMode	GetPolyMode() const										{ return m_PolyMode; }

	void		SetExportTextures( bool b )								{ m_ExportTextures = b; }
	bool		GetExportTextures() const								{ return m_ExportTextures; }

	void		SetExportEntityTextureList( bool b )					{ m_ExportEntityTextureList = b; }
	bool		GetExportEntityTextureList() const						{ return m_ExportEntityTextureList; }

	void		SetExportEntityTextureListKeepExtenstions(bool b)		{ m_EntityTextureListKeepExtensions = b; }
	bool		GetExportEntityTextureListKeepExtenstions() const		{ return m_EntityTextureListKeepExtensions; }

	void		SetUseNonOptimizedMTLCompare(bool b)					{ m_UseNonOptimizedMTLCompare = b; }
	bool		GetUseNonOptimizedMTLCompare() const					{ return m_UseNonOptimizedMTLCompare; }

	void		SetExpectedVertexCount(const char * b)					{ m_ExpectedVertexCount = atString(b); }
	int			GetExpectedVertexCount(const atString &meshName) const;

	void		SetRemoveDegenerates(bool b)							{ m_RemoveDegenerates = b; }
	bool		GetRemoveDegenerates() const							{ return m_RemoveDegenerates; }

	void		SetWeldWithoutQuantize(bool b)							{ m_WeldWithoutQuantize = b; }
	bool		GetWeldWithoutQuantize() const							{ return m_WeldWithoutQuantize; }

	void		SetWeld(bool b)											{ m_Weld = b; }
	bool		GetWeld() const											{ return m_Weld; }

	void		SetSplitBlendsIntoUniqueMeshes(bool b)					{ m_SplitBlendsIntoUniqueMeshes = b; }
	bool		GetSplitBlendsIntoUniqueMeshes() const					{ return m_SplitBlendsIntoUniqueMeshes; }

	void		SetMicroMorphs(bool b)									{ m_MicroMorphs = b; }
	bool		GetMicroMorphs() const									{ return m_MicroMorphs; }

#if HACK_GTA4
	void		SetOptimiseMaterials(bool b)							{ m_OptimiseMaterials = b; }
	bool		GetOptimiseMaterials() const							{ return m_OptimiseMaterials; }
#endif // HACK_GTA4

protected:
	rexResult	ConvertGenericMeshToRageRenderMesh( const rexObjectGenericMesh *inputMesh,
													const atArray<rexObjectGenericMesh::MaterialInfo>& materialInfo,
													mshMesh& outputMesh) const;
	rexResult	SerializeBlendTargets ( mshMesh& baseMesh, const rexObjectGenericMesh *inputMesh, const atArray<rexObjectGenericMesh::MaterialInfo>& materialInfo) const;
	rexResult	GenerateBlendTargetDeltas( mshMesh& outMesh, bool bCalcVertNormalDeltas = true ) const;

protected:
#if HACK_GTA4
	bool		m_OptimiseMaterials;
#endif // HACK_GTA4 
	bool		m_ExportTextures;
	bool		m_ExportEntityTextureList;
	bool		m_EntityTextureListKeepExtensions;
	atString	m_TextureOutputPath;
#if HACK_GTA4
	// Can't find anywhere this is used.  Marked for deletion
	mutable int	m_MaterialOffset;
#endif // HACK_GTA4
	atString	m_ExpectedVertexCount;
	int			m_MaxMeshMemorySize;
	bool		m_ExportRootBoneMeshesRelativeToMeshCenter, m_ExportRootBoneMeshesRelativeToPivot;
	PolyMode	m_PolyMode;
	bool		m_DisableTriStripping;
	int			m_MaxMatricesPerMaterial;
	bool		m_UseNonOptimizedMTLCompare;
	bool		m_RemoveDegenerates;
	bool		m_WeldWithoutQuantize;
	bool		m_Weld;
	bool		m_UseChildrenOfLevelInstanceNodes, m_LevelInstanceChildrenExportOnlyCPVs;
	bool		m_SplitBlendsIntoUniqueMeshes;
	bool		m_MicroMorphs;
};

///////////////////////////////////////////////////////////////////////////////////

class rexPropertyTextureOutputPath : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyTextureOutputPath; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyMeshSetExportTextures : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshSetExportTextures; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyMeshSetExportEnityTextureList : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshSetExportEnityTextureList; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyMeshEntityTextureListKeepExtensions : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshEntityTextureListKeepExtensions; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyMeshMaxMemorySize : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshMaxMemorySize; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertySetExportRootBoneMeshesRelativeToMeshCenter : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySetExportRootBoneMeshesRelativeToMeshCenter; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertySetExportRootBoneMeshesRelativeToPivot : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySetExportRootBoneMeshesRelativeToPivot; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyDisableTriStripping : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyDisableTriStripping; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertySetMaxMatricesPerMaterial : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySetMaxMatricesPerMaterial; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyExpectedVertexCount : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyExpectedVertexCount; }
};
/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyUseNonOptimizedMTLCompareMesh : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyUseNonOptimizedMTLCompareMesh; }
};

/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMeshRemoveDegenerates : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshRemoveDegenerates; }
};

/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMeshWeldWithoutQuantize : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshWeldWithoutQuantize; }
};

/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMeshWeld : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshWeld; }
};

/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMeshSplitBlendsIntoUniqueMeshes : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshSplitBlendsIntoUniqueMeshes; }
};

/////////////////////////////////////////////////////////////////////////////////////
class rexPropertyMeshMicroMorphs : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshMicroMorphs; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////


#endif
