// 
// rexRAGE/serializerCutscene.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_SERIALIZER_CUTSCENE_H__
#define __REX_SERIALIZER_CUTSCENE_H__

#include "rexBase/serializer.h"

namespace rage {

class atString;
class fiStream;
class rexObjectCutscene;
class rexObjectCSEvent;
class rexObjectGenericAnimationSegment;


////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	csCast - class with data suitable for describing a cutscene cast.
//
////////////////////////////////////////////////////////////////////////////////
class csCast
{
public:
	// atArray requires the default constructor.
	csCast();
	csCast(bool ref, const char *category, const char *type, const char *name);

	bool operator == (const csCast &cast) const;

	// Data field
	bool		mReferenced;
	atString	mCategory;
	atString	mType;
	atString	mName;
};

////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	csData - class with data suitable for describing a CS data.
//
////////////////////////////////////////////////////////////////////////////////
class csData
{
public:
	// atArray requires the default constructor.
	csData();
	csData(const char *type, const char *filename);

	bool operator == (const csData &data) const;

	// Data field
	atString	mType;
	atString	mFileName;
};

////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	csEvent - class with data suitable for describing a CS event.
//
////////////////////////////////////////////////////////////////////////////////
class csEvent
{
public:
	// atArray requires the default constructor.
	csEvent();
	csEvent(const char *typeName, const char* message);
	csEvent(float timecode, const char *nodeName, const char* typeName, const char* message);

	bool	operator == (const csEvent &event) const;
	csEvent	&operator = (const csEvent &event);

	// Data field
	float		mTimeCode;
	atString	mNodeName;
	atString	mTypeName;
	atString	mMessage;
};

////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	rexSerializerCutscene -- save .cs files from generic cutscene information
//
//	INHERITS FROM: rexSerializer
//
//	PUBLIC INTERFACE: [Primary Functionality]
//			
////////////////////////////////////////////////////////////////////////////////
class rexSerializerCutscene : public rexSerializer
{
public:
	rexSerializerCutscene() : rexSerializer() { }

	rexSerializer*		CreateNew() const { return new rexSerializerCutscene(); }
	virtual rexResult	Serialize( rexObject& object ) const; 

protected:
	// Create cutscene assets: cast, data and events.
	void	CreateCutsceneAssets(const rexObjectCutscene *cs) const;
	void	CreateCutsceneAsset(const rexObject *obj) const;
	void	CreateCSAssetCast(const rexObject *obj) const;
	void	CreateCSAssetCamera(const rexObject *obj) const;
	void	CreateCSAssetEventTimeBased(const rexObject *obj) const;
	
	void	CreateCSEventGeneric(const rexObjectCSEvent *event) const;
	void	CreateCSEventGenericFloat(const rexObjectCSEvent *event) const;
	void	CreateCSEventGenericDuration(const rexObjectCSEvent *event) const;
	void	CreateCSEventGenericDurationFloat(const rexObjectCSEvent *event) const;
	
	void	CreateCSEventFadeIn(const rexObjectCSEvent *event) const;
	void	CreateCSEventFadeOut(const rexObjectCSEvent *event) const;
	void	CreateCSEventAudio(const rexObjectCSEvent *event) const;
	void	CreateCSEventParticle(const rexObjectCSEvent *event) const;
	void	CreateCSEventClipPlane(const rexObjectCSEvent *event) const;
	void	CreateCSEventEndScene(const rexObjectCSEvent *event) const;
	bool	FindAnimationSegment(const rexObject *obj, rexObjectGenericAnimationSegment **seg) const;
	void 	AddCast(bool IsRef, const char *category, const char *type, const char *name) const;
	void 	AddData(const char *datatype, const char *dataname) const;
	void 	AddEvent(const char *typeName, const char *message) const;
	void 	AddEvent(float timecode, const char *nodeName, const char *typeName, const char *message) const;
};

////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	rexSerializerCSCamera -- save .anim file for cutscene camera.
//
//	INHERITS FROM: rexSerializer
//
//	PUBLIC INTERFACE: [Primary Functionality]
//			
////////////////////////////////////////////////////////////////////////////////
class rexSerializerCSCamera : public rexSerializer
{
public:
	rexSerializerCSCamera() : rexSerializer() { }

	virtual rexResult	Serialize( rexObject& object ) const; 

	rexSerializer*		CreateNew() const { return new rexSerializerCSCamera(); }

protected:

};

////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
//
//	rexSerializerCSLight -- save .anim file for cutscene light.
//
//	INHERITS FROM: rexSerializer
//
//	PUBLIC INTERFACE: [Primary Functionality]
//			
////////////////////////////////////////////////////////////////////////////////
class rexSerializerCSLight : public rexSerializer
{
public:
	rexSerializerCSLight() : rexSerializer() { }

	virtual rexResult	Serialize( rexObject& object ) const; 

	rexSerializer*		CreateNew() const { return new rexSerializerCSLight(); }

protected:

};


} // namespace rage

#endif
