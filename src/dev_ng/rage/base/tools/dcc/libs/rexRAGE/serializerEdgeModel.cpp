#include "rexGeneric/objectEdgeModel.h"
#include "rexRAGE/serializerEdgeModel.h"
#include "rexBase/utility.h"
#include "rexBase/shell.h"

#include "system/alloca.h"

#include "file/stream.h"
#include "file/asset.h"
#include "rexBase/edgemodel.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"

///////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

rexResult rexSerializerRAGEEdgeModel::Serialize( rexObject& object ) const
{	
	rexResult result( rexResult::PERFECT );
	if( !GetExportData() )
		return result;

	const rexObjectGenericEdgeModelHierarchy* inputMeshHier = dynamic_cast<const rexObjectGenericEdgeModelHierarchy*>( &object );
	if( inputMeshHier )
	{
		ASSET.SetPath( GetOutputPath() );

		int objCount = inputMeshHier->m_ContainedObjects.GetCount();

		if( m_ProgressBarObjectCountCB )
			(*m_ProgressBarObjectCountCB )( objCount );	

		for( int a = 0; a < objCount; a++ )
		{
			if( m_ProgressBarObjectCurrentIndexCB )
				(*m_ProgressBarObjectCurrentIndexCB)( a );	

			result &= SerializeRecursive( *inputMeshHier->m_ContainedObjects[ a ] );
		}		
	}
	else
	{
		result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult	rexSerializerRAGEEdgeModel::SerializeRecursive( rexObject& object ) const
{
	rexResult result( rexResult::PERFECT );

	rexObjectGenericEdgeModelHierarchy* inputMeshHier = dynamic_cast<rexObjectGenericEdgeModelHierarchy*>( &object );
	if( inputMeshHier )
	{
		int objCount = inputMeshHier->m_ContainedObjects.GetCount();
		
		if( m_ProgressBarObjectCountCB )
			(*m_ProgressBarObjectCountCB )( objCount );	

		for( int a = 0; a < objCount; a++ )
		{
			if( m_ProgressBarObjectCurrentIndexCB )
				(*m_ProgressBarObjectCurrentIndexCB)( a );	

			result &= SerializeRecursive( *inputMeshHier->m_ContainedObjects[ a ]  );
		}		
	}
	else 
	{
		rexObjectGenericMesh* inputMesh = dynamic_cast<rexObjectGenericMesh*>( &object );
		if( inputMesh && inputMesh->m_IsValid && ( m_UseChildrenOfLevelInstanceNodes || !inputMesh->m_ChildOfLevelInstanceNode ) )
		{
			if( m_ProgressBarTextCB )
			{
				static char message[1024];
				sprintf( message, "Serializing edge model %s.em", (const char*)inputMesh->GetMeshName() );
				(*m_ProgressBarTextCB) ( message );
			}

			// Create an edgeModel object.
			edgeModel *em = new edgeModel;

			em->m_NumVerts = inputMesh->m_Vertices.GetCount();
			em->m_NumMatrices = inputMesh->m_SkinBoneMatrices.GetCount();
			em->m_Verts = new edgeModel::Vert[em->m_NumVerts];
			em->m_LoadVerts = new Vector3[em->m_NumVerts];

			float conversionFactor=GetUnitTypeLinearConversionFactor();
			for( int i = 0; i < em->m_NumVerts; i++ )
			{
				Vector3 pos(inputMesh->m_Vertices[ i ].m_Position);
				pos.Scale(conversionFactor);
				em->m_Verts[i].Pos = em->m_LoadVerts[i] = pos;
			}

			SetTriangles( *inputMesh, *em );
			em->ComputeAllNormals();

			if( inputMesh->m_IsSkinned )
			{
				int vertCount = inputMesh->m_Vertices.GetCount();

				for( int a = 0; a < vertCount; a++ )
				{
					const rexObjectGenericMesh::VertexInfo& vert = inputMesh->m_Vertices[ a ];
					
					int influenceCount = vert.m_BoneIndices.GetCount();
					Assert( vert.m_BoneWeights.GetCount() == influenceCount );
					
					if( influenceCount )
					{
						float largest = vert.m_BoneWeights[ 0 ];
						int largeIndex = vert.m_BoneIndices[ 0 ];

						for( int b = 1; b < influenceCount; b++ )
						{
							if( vert.m_BoneWeights[ b ] > largest )
							{
								largest = vert.m_BoneWeights[ b ];
								largeIndex = vert.m_BoneIndices[ b ];							
							}
						}

						em->m_Verts[ a ].MatrixInd = largeIndex;
					}
				}

				int mtxCount = inputMesh->m_SkinBoneMatrices.GetCount();
				Matrix34* mtx = (Matrix34*) malloc( sizeof( Matrix34 ) * mtxCount );
				for( int m = 0; m < mtxCount; m++ )
				{
					mtx[ m ].Set( inputMesh->m_SkinBoneMatrices[ m ] );
				}
			}

			em->Build();

			int edgeCount = em->m_Edges.GetCount();
			for( int e = 0; e < edgeCount; e++ )
			{
				if( ( em->m_Edges[ e ].TriInd[ 0 ] < 0 ) || ( em->m_Edges[ e ].TriInd[ 1 ] < 0 ) )
				{					
					inputMesh->m_IsValid = false;
					result.Set( rexResult::ERRORS );
					result.AddErrorCtx((const char*)inputMesh->GetMeshName(), "Edgemodel '%s' is not sealed off!  Will not be exported", (const char*)inputMesh->GetMeshName() );
					break;
				}
			}
			
			if( !rexShell::DoesntSerialise() && inputMesh->m_IsValid )
			{
				if( !em->Save( inputMesh->GetMeshName() ) )
				{
					result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );
					result.AddErrorCtx((const char*)inputMesh->GetMeshName(), "Could not save file '%s.mesh'", (const char*)inputMesh->GetMeshName() );
				}
			}

			delete em->m_LoadVerts;
			delete em;
		}
		else
		{
			result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}
	}		

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct Tri
{
	int vertIndex[ 3 ];
};

void rexSerializerRAGEEdgeModel::SetTriangles(const rexObjectGenericMesh& inputMesh, edgeModel& em) const
{
	// Save adjunct-vertex list, count how many triangles there are.
	atArray<Tri> tris;
	
	int primCount = inputMesh.m_Primitives.GetCount();

	for( int a = 0; a < primCount; a++ )
	{
		const rexObjectGenericMesh::PrimitiveInfo& prim = inputMesh.m_Primitives[ a ];
		int adjCount = prim.m_Adjuncts.GetCount();
		if( adjCount == 3 )
		{				
			Tri& tri = tris.Grow((u16) primCount * 2);
			tri.vertIndex[ 0 ] = prim.m_Adjuncts[ 0 ].m_VertexIndex;
			tri.vertIndex[ 1 ] = prim.m_Adjuncts[ 1 ].m_VertexIndex;
			tri.vertIndex[ 2 ] = prim.m_Adjuncts[ 2 ].m_VertexIndex;
		}
		else if( adjCount == 4 )
		{
			if( prim.m_Adjuncts[ 0 ].m_VertexIndex != 0 ) // i don't get this -- n8
			{
				Tri& tri1 = tris.Grow((u16) primCount * 2);
				tri1.vertIndex[ 0 ] = prim.m_Adjuncts[ 1 ].m_VertexIndex;
				tri1.vertIndex[ 1 ] = prim.m_Adjuncts[ 2 ].m_VertexIndex;
				tri1.vertIndex[ 2 ] = prim.m_Adjuncts[ 3 ].m_VertexIndex;

				Tri& tri2 = tris.Grow((u16) primCount * 2);
				tri2.vertIndex[ 0 ] = prim.m_Adjuncts[ 1 ].m_VertexIndex;
				tri2.vertIndex[ 1 ] = prim.m_Adjuncts[ 3 ].m_VertexIndex;
				tri2.vertIndex[ 2 ] = prim.m_Adjuncts[ 0 ].m_VertexIndex;
			}
			else
			{
				Tri& tri1 = tris.Grow((u16) primCount * 2);
				tri1.vertIndex[ 0 ] = prim.m_Adjuncts[ 0 ].m_VertexIndex;
				tri1.vertIndex[ 1 ] = prim.m_Adjuncts[ 1 ].m_VertexIndex;
				tri1.vertIndex[ 2 ] = prim.m_Adjuncts[ 2 ].m_VertexIndex;

				Tri& tri2 = tris.Grow((u16) primCount * 2);
				tri2.vertIndex[ 0 ] = prim.m_Adjuncts[ 0 ].m_VertexIndex;
				tri2.vertIndex[ 1 ] = prim.m_Adjuncts[ 2 ].m_VertexIndex;
				tri2.vertIndex[ 2 ] = prim.m_Adjuncts[ 3 ].m_VertexIndex;
			}
		}
		else
		{
			Warningf("Edge model triangle #%d has %d adjuncts (either three or four are required).", a, adjCount);
			Warningf("Bad triangle in em %s at %s", inputMesh.GetMeshName(), inputMesh.m_FullPath);

			Tri& tri = tris.Grow((u16) primCount * 2);
			tri.vertIndex[ 0 ] = 0;
			tri.vertIndex[ 1 ] = 0;
			tri.vertIndex[ 2 ] = 0;
		}
	}

	// Allocate triangle list.
	int triCount = tris.GetCount();
	em.m_NumTris = triCount;
	em.m_Tris = new edgeModel::Tri[ triCount ];

	for( int a = 0; a < triCount; a++ )
	{
		em.m_Tris[a].VertInd[ 0 ] = tris[ a ].vertIndex[ 0 ];
		em.m_Tris[a].VertInd[ 1 ] = tris[ a ].vertIndex[ 1 ];
		em.m_Tris[a].VertInd[ 2 ] = tris[ a ].vertIndex[ 2 ];
	}
}

} // namespace rage
