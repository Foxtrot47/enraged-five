// 
// rexRAGE/serializerNurbsCurve.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "serializerNurbsCurve.h"

#include "curve/mayacurve.h"  
#include "rexBase/shell.h"

#include "file/asset.h"
#include "file/token.h"

#include "rexGeneric/objectNurbsCurve.h"

using namespace rage;

rexResult rexSerializerRAGENurbsCurve::Serialize( rexObject& object ) const
{
	return WriteCurveInfo(object,"curves");
}

rexResult rexSerializerRAGENurbsCurve::WriteCurveInfo( rexObject& object, const char* fileName, bool bSerializeVersionTwoCurves )
{
	rexResult retVal ( rexResult::PERFECT );

	if(rexShell::DoesntSerialise())
	{
		retVal.AddMessage("Not serialising NURBS CURVE %s due to flag set.", fileName);
		return retVal;
	}

	const rexObjectGenericNurbsCurveGroup* curveGroup = dynamic_cast<const rexObjectGenericNurbsCurveGroup*>( &object );

	if( !curveGroup )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	fiStream* s = ASSET.Create( fileName, "curves" );
	if( !s )
	{
		// Oh dear, something bad has happened
		rexResult retval( rexResult::ERRORS, rexResult::CANNOT_CREATE_FILE );

		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),fileName,"curves" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		retval.AddError( "Could not create file '%s'", acFullPathForDebugging);
		return retval;
	}

	float conversionFactor=GetUnitTypeLinearConversionFactor();

	char buffer[512];
	int childCount = curveGroup->m_ContainedObjects.GetCount();

	sprintf( buffer, "count %d\r\n", childCount );
	s->Write( buffer, (int)strlen( buffer ) );

	if(bSerializeVersionTwoCurves)
	{
		sprintf( buffer, "version 2\r\n");
		s->Write( buffer, (int)strlen( buffer ) );
	}

	for( int a = 0; a < childCount; a++ )
	{
		const rexObjectGenericNurbsCurve* curve = dynamic_cast<const rexObjectGenericNurbsCurve*>( curveGroup->m_ContainedObjects[ a ] );

		if(bSerializeVersionTwoCurves)
		{
			int cvCount = curve->m_ControlVertices.GetCount();
			Vector3 *verts = new Vector3[cvCount];
			for (int i=0; i<cvCount; i++)
			{
				verts[i] = curve->m_ControlVertices[i];
				verts[i].Scale(conversionFactor);
			}

			mayaCurve *mc = new mayaCurve();
			mc->Init(
				verts,
				cvCount,
				curve->m_Knots.GetElements(),
				curve->m_Knots.GetCount(),
				curve->m_Degree,
				(mayaCurveForm)curve->m_Type);

			fiTokenizer tok("curve", s);
			mc->Save(tok, true, true);  // Save the control vertices and knot vector.
			tok.Put("\n");
			mc->Shutdown();
			delete mc;
			mc = NULL;
			delete[] verts;
			verts = NULL;
		}
		else
		{
			Assert( curve );

			sprintf( buffer, "%s ", (const char*)curve->GetName() );
			s->Write( buffer, (int)strlen( buffer ) );
			sprintf( buffer, "boneindex %d ", curve->m_BoneIndex );
			s->Write( buffer, (int)strlen( buffer ) );
			sprintf( buffer, "roomindex %d ", curve->m_GroupIndex );
			s->Write( buffer, (int)strlen( buffer ) );
			sprintf( buffer, "degree %d ", curve->m_Degree );
			s->Write( buffer, (int)strlen( buffer ) );
			sprintf( buffer, "form %d ", (int)curve->m_Type );
			s->Write( buffer, (int)strlen( buffer ) );

			int cvCount = curve->m_ControlVertices.GetCount();
			sprintf( buffer, "cv %d ", cvCount );
			s->Write( buffer, (int)strlen( buffer ) );
			for( int b = 0; b < cvCount; b++ )
			{
				Vector3 vertex = curve->m_ControlVertices[ b ];
				vertex.Scale(conversionFactor);
				sprintf( buffer, "( %f %f %f ) ", vertex.x, vertex.y, vertex.z );
				s->Write( buffer, (int)strlen( buffer ) );
			}		

			int knotCount = curve->m_Knots.GetCount();
			sprintf( buffer, "knots %d ", knotCount );

			s->Write( buffer, (int)strlen( buffer ) );
			for( int b = 0; b < knotCount; b++ )
			{
				sprintf( buffer, "%f ", curve->m_Knots[ b ] );
				s->Write( buffer, (int)strlen( buffer ) );
			}		

			sprintf( buffer, "\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );
		}
	}

	s->Close();
	if(rexShell::DoesntSerialise())
	{
		const char *filename = s->GetName();
		retVal.AddMessage("Deleting file %s due to no-serialise flag.", filename);
		remove(filename);
	}

	return retVal;
}



