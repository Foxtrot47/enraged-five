#include "serializerCutscene.h"

#include "rexGeneric/objectAnimation.h"
#include "rexGeneric/objectCutscene.h"
#include "rexGeneric/objectEntity.h"
#include "rexBase/utility.h"
#include "rexBase/shell.h"


#include "file/stream.h"
#include "file/asset.h"

namespace rage {

atString			gCutsceneName;
atArray<csCast>		gCastList;
atArray<csData>		gDataList;
atArray<csEvent>	gEventList;


// csCast
csCast::csCast()
{
	mReferenced = false;
	mCategory = "";
	mType = "";
	mName = "";
}

csCast::csCast(bool ref, const char *cat, const char *type, const char *name)
{
	mReferenced = ref;
	mCategory = cat;
	mType = type;
	mName = name;
}

bool csCast::operator==(const csCast& cast) const
{
	return( mReferenced == cast.mReferenced &&
			mCategory == cast.mCategory &&
			mType == cast.mType &&
			mName == cast.mName );
}

// csData
csData::csData()
{
	mType = "";
	mFileName = "";
}

csData::csData(const char *type, const char *filename)
{
	mType = type;
	mFileName = filename;
}

bool csData::operator==(const csData& data) const
{
	return( mType == data.mType && mFileName == data.mFileName );
}

// csEvent
csEvent::csEvent()
{
	mTimeCode = 0.0f;
	mNodeName = "";
	mTypeName = "";
	mMessage = "";
}

csEvent::csEvent(const char *typeName, const char* msg)
{
	mTimeCode = 0.0f;
	mNodeName = "";
	mTypeName = typeName;
	mMessage = msg;
}

csEvent::csEvent(float t, const char *nodeName, const char* typeName, const char* msg)
{
	mTimeCode = t;
	mNodeName = nodeName;
	mTypeName = typeName;
	mMessage = msg;
}

bool csEvent::operator==(const csEvent& event) const
{

	if( mTimeCode == event.mTimeCode && mTimeCode == 0.0f )
	{
		return( mTimeCode == event.mTimeCode &&
				mNodeName == event.mNodeName &&
				mTypeName == event.mTypeName &&
				mMessage == event.mMessage );
	}
	else
	{
		return( mNodeName == event.mNodeName && mTypeName == event.mTypeName );
	}
}

csEvent& csEvent::operator=(const csEvent& event)
{
	mTimeCode = event.mTimeCode;
	mNodeName = event.mNodeName;
	mTypeName = event.mTypeName;
	mMessage = event.mMessage;
	return *this;
}

// rexSerializerCutscene
rexResult rexSerializerCutscene::Serialize( rexObject& obj ) const
{
	rexResult result( rexResult::PERFECT );

	if( !GetExportData() )
		return result;

	const rexObjectCutscene* cs = dynamic_cast<const rexObjectCutscene*>(&obj);
	if( cs == NULL )
		return result;

	gCutsceneName = cs->GetName();

	// Create cutscene assets: cast, data and events.
	CreateCutsceneAssets( cs );

	if(rexShell::DoesntSerialise())
	{
		result.AddMessage("Returning early and not serialising CS file %s due to flag set.", cs->GetName().c_str());
		return result;
	}
	
	// Set output path for .cs files.
	rexUtility::CreatePath( GetOutputPath() );
	ASSET.SetPath( GetOutputPath() );

	// Write cast.cs file.
	char buf[ 512 ];
	fiStream* stream = ASSET.Create( "cast", "cs" );
	if( stream )
	{
		// Write cast list.
		sprintf(buf, "Version: 100\n");
		stream->Write( buf, (int)strlen(buf) );
		int castCount = gCastList.GetCount();
		sprintf( buf, "Cast %d {\n", castCount);
		stream->Write( buf, (int)strlen(buf) );
		for( int i = 0; i < castCount; i++ )
		{
			sprintf(buf , "\t%s %s ",gCastList[i].mReferenced? "ref": "val",(const char*)(gCastList[i].mCategory));
			stream->Write( buf, (int)strlen(buf) );
			if( !gCastList[i].mReferenced )
			{
				sprintf(buf , "%s ", (const char*)(gCastList[i].mType));
				stream->Write( buf, (int)strlen(buf) );
			}
			sprintf(buf , "%s\n", (const char*)(gCastList[i].mName));
			stream->Write( buf, (int)strlen(buf) );
		}
		stream->Write( "}\n", 2 );

		// Write data list.
		int dataCount = gDataList.GetCount();
		sprintf(buf , "Data {\n");
		stream->Write( buf, (int)strlen(buf) );
		for( int i = 0; i <  dataCount; i++ )
		{
			sprintf(buf , "\t%s %s\n", (const char*)(gDataList[i].mType), (const char*)(gDataList[i].mFileName));
			stream->Write( buf, (int)strlen(buf) );
		}
		stream->Write( "}\n", 2 );

		// Write bookmarks.
		if( cs->GetNumBookmarks() > 0 )
		{
			sprintf(buf , "Bookmarks %d {\n", cs->GetNumBookmarks() );
			stream->Write( buf, (int)strlen(buf) );
				sprintf(buf , "\t%s\n", (const char*)(cs->GetBookmarks()));
				stream->Write( buf, (int)strlen(buf) );
			stream->Write( "}\n", 2 );
		}

		stream->Close();
	}	
	else
	{
		// Oh dear, something bad has happened
		result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"cast","cs" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		result.AddError( "Could not create file '%s'", acFullPathForDebugging);
	}

	// Write events.cs file.
	stream = ASSET.Create( "events", "cs" );
	if( stream )
	{
		// Sort event list.
		int count = gEventList.GetCount();
		for( int i = 0; i < count - 1; i++ )
		{
			for( int j = i+1; j < count; j++ )
			{
				if( gEventList[i].mTimeCode > gEventList[j].mTimeCode )
				{
					csEvent temp(gEventList[i].mTimeCode, gEventList[i].mNodeName, gEventList[i].mTypeName, gEventList[i].mMessage);
					gEventList[i] = gEventList[j];
					gEventList[j] = temp;
				}
			}
		}

		// Write event list.
		sprintf(buf, "Version: 101\n");
		stream->Write( buf, (int)strlen(buf) );
		sprintf(buf , "Events {\n");
		stream->Write( buf, (int)strlen(buf) );
		for( int i = 0; i < count; i++ )
		{
			if( gEventList[i].mTimeCode == 0.0f )
			{
				sprintf(buf , "\tStartup_Event %s {\n", (const char*)(gEventList[i].mTypeName));
				stream->Write( buf, (int)strlen(buf) );
			}
			else
			{
				sprintf(buf , "\tEvent %f %s %s {\n", gEventList[i].mTimeCode, (const char*)(gEventList[i].mNodeName), (const char*)(gEventList[i].mTypeName));
				stream->Write( buf, (int)strlen(buf) );
			}
			sprintf(buf , "%s\n\t}\n\n", (const char*)(gEventList[i].mMessage));
			stream->Write( buf, (int)strlen(buf) );
		}
		stream->Write( "}\n", 2 );

		stream->Close();
	}	
	else
	{
		// Oh dear, something bad has happened
		result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"events","cs" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		result.AddError( "Could not create file '%s'", acFullPathForDebugging);
	}


	return result;
}

void rexSerializerCutscene::CreateCutsceneAssets(const rexObjectCutscene *cs) const
{
	// Reset the asset list.
	gCastList.Reset();
	gDataList.Reset();
	gEventList.Reset();

	int childCount = cs->m_ContainedObjects.GetCount();
	for( int i = 0; i < childCount; i++ )
	{
		CreateCutsceneAsset( cs->m_ContainedObjects[i] );
	}
}

void rexSerializerCutscene::CreateCutsceneAsset(const rexObject *obj) const
{
	// Create cutscene assets from this rexObject.
	// If this is an entity, create cast, startup event.
	const rexObjectGenericEntity* cast = dynamic_cast<const rexObjectGenericEntity*>( obj );
	if( cast )
	{
		CreateCSAssetCast(cast);
		return;
	}

	// If this is cs-event, create event.
	const rexObjectCSEvent* event = dynamic_cast<const rexObjectCSEvent*>(obj);
	if( event )
	{
		CreateCSAssetEventTimeBased(event);
		return;
	}

	// If this is cs-camera, create data, startup event.
	const rexObjectCSCamera* cam = dynamic_cast<const rexObjectCSCamera*>(obj);
	if( cam )
	{
		CreateCSAssetCamera(cam);
		return;
	}

	// If this is cs-light, create a data and startup event.


	// Create cutscene assets from children.
	/*
	int childCount = obj->m_ContainedObjects.GetCount();
	for( int i = 0; i < childCount; i++ )
	{
		CreateCutsceneAsset( obj->m_ContainedObjects[i] );
	}
	*/
}

void rexSerializerCutscene::CreateCSAssetCast(const rexObject *obj) const
{
	const rexObjectGenericEntity* cast = dynamic_cast<const rexObjectGenericEntity*>( obj );
	if( cast == NULL )
		return;

	// Create a cast.
	atString nodeName = cast->GetName();
	AddCast(cast->m_Referenced, "ENTITY", nodeName, nodeName);

	// Create a startup event: ENTITYPOS.
	atString msg("\t\tEntity: \t ");
	msg += nodeName;
	msg += "\n\t\tPosition: \t 0.0 0.0 0.0";
	msg += "\n\t\tOrientation: \t 0.0 0.0 0.0";
	AddEvent( "ENTITYPOS", msg );

	// Find an animation segment node for this entity.

	//const rexObjectGenericAnimationSegment* seg = dynamic_cast<const rexObjectGenericAnimationSegment*>( obj );
	rexObjectGenericAnimationSegment* seg = NULL;
	bool segFound = FindAnimationSegment(obj, &seg);
	if( segFound && seg != NULL )
	{
		atString segName = seg->GetName();

		// Create a data.
		atString file = nodeName;
		file += "/";
		file += segName;
		AddData("ANIM", file);

		// Create a startup event: ENTITYANIM.
		atString msg("\t\tEntity: \t ");
		msg += nodeName;
		msg += "\n\t\tAnim: \t ";
		msg += nodeName;
		msg += "/";
		msg += segName;
		AddEvent( "ENTITYANIM", msg );
	}
}

void rexSerializerCutscene::CreateCSAssetCamera(const rexObject *obj) const
{
	const rexObjectCSCamera* camera=dynamic_cast<const rexObjectCSCamera*>(obj);
	if( camera == NULL )
		return;

	// Create a data.
	atString nodeName = camera->GetName();
	AddData("CAMERA", nodeName);

	// Create a startup event: ENTITYPOS.
	atString msg("\t\tAnim: \t ");
	msg += nodeName;
	AddEvent( "CAMERA", msg );
}

void rexSerializerCutscene::CreateCSAssetEventTimeBased(const rexObject *obj) const
{
	// The obj is the top level event node. Its children are the events.
	const rexObjectCSEvent* event = dynamic_cast<const rexObjectCSEvent*>(obj);
	if( event == NULL )
		return;

	if( event->GetType() == "GENERIC" )
		CreateCSEventGeneric(event);
	else if( event->GetType() == "GENERICDURATION" )
		CreateCSEventGenericDuration(event);
	else if( event->GetType() == "GENERICFLOAT" )
		CreateCSEventGenericFloat(event);
	else if( event->GetType() == "GENERICDURATIONFLOAT" )
		CreateCSEventGenericDurationFloat(event);
	else if( event->GetType() == "FADEIN" )
		CreateCSEventFadeIn(event);
	else if( event->GetType() == "FADEOUT" )
		CreateCSEventFadeOut(event);
	else if( event->GetType() == "AUDIOSTREAM" )
		CreateCSEventAudio(event);
	else if( event->GetType() == "EMITPARTICLES" )
		CreateCSEventParticle(event);
	else if( event->GetType() == "CLIPPLANE" )
		CreateCSEventClipPlane(event);
	else if( event->GetType() == "ENDSCENE" )
		CreateCSEventEndScene(event);
}

void rexSerializerCutscene::CreateCSEventGeneric(const rexObjectCSEvent *event) const
{
	float spf = 1.0f / event->GetFPS();
	float ssf = (float)event->GetSceneStartFrame();
	float sf = (float)event->GetStartFrame();
	float timecode = (sf - ssf) * spf;
	char msg[1024];
	sprintf(msg, "\t\tEventString: \t %s",
			(const char*)(event->GetEventString()));
	AddEvent(timecode, event->GetName(), event->GetType(), (const char*)msg);
}

void rexSerializerCutscene::CreateCSEventGenericDuration(const rexObjectCSEvent *event) const
{
	float spf = 1.0f / event->GetFPS();
	float ssf = (float)event->GetSceneStartFrame();
	float sf = (float)event->GetStartFrame();
	float ef = (float)event->GetEndFrame();

	float timecode = (sf - ssf) * spf;
	float duration = (ef - sf) * spf;

	char msg[1024];
	sprintf(msg, "\t\tEventString: \t %s\n\t\tDuration: \t %f", 
			(const char*)(event->GetEventString()), duration); 
	AddEvent(timecode, event->GetName(), event->GetType(), (const char*)msg);
}

void rexSerializerCutscene::CreateCSEventGenericFloat(const rexObjectCSEvent *event) const
{
	float spf = 1.0f / event->GetFPS();
	float ssf = (float)event->GetSceneStartFrame();
	float sf = (float)event->GetStartFrame();
	float timecode = (sf - ssf) * spf;
	char msg[1024];
	sprintf(msg, "\t\tEventString: \t %s\n\t\tFloatValue: \t %f",
			(const char*)(event->GetEventString()),
			event->GetEventGenericFloat());
	AddEvent(timecode, event->GetName(), event->GetType(), (const char*)msg);
}

void rexSerializerCutscene::CreateCSEventGenericDurationFloat(const rexObjectCSEvent *event) const
{
	float spf = 1.0f / event->GetFPS();
	float ssf = (float)event->GetSceneStartFrame();
	float sf = (float)event->GetStartFrame();
	float ef = (float)event->GetEndFrame();

	float timecode = (sf - ssf) * spf;
	float duration = (ef - sf) * spf;

	char msg[1024];
	sprintf(msg, "\t\tEventString: \t %s\n\t\tDuration: \t %f\n\t\tFloatValue: \t %f", 
			(const char*)(event->GetEventString()), 
			duration,
			event->GetEventGenericFloat()); 
	AddEvent(timecode, event->GetName(), event->GetType(), (const char*)msg);
}

void rexSerializerCutscene::CreateCSEventFadeIn(const rexObjectCSEvent *event) const
{
	float spf = 1.0f / event->GetFPS();
	float ssf = (float)event->GetSceneStartFrame();
	float sf = (float)event->GetStartFrame();
	float ef = (float)event->GetEndFrame();
	float timecode = (sf - ssf) * spf;
	float duration = (ef - sf) * spf;
	char msg[1024];
	sprintf(msg, "\t\tDuration: \t %f", duration);
	AddEvent(timecode, event->GetName(), event->GetType(), (const char*)msg);
}

void rexSerializerCutscene::CreateCSEventFadeOut(const rexObjectCSEvent *event) const
{
	float spf = 1.0f / event->GetFPS();
	float ssf = (float)event->GetSceneStartFrame();
	float sf = (float)event->GetStartFrame();
	float ef = (float)event->GetEndFrame();
	float timecode = (sf - ssf) * spf;
	float duration = (ef - sf) * spf;
	char msg[1024];
	sprintf(msg, "\t\tDuration: \t %f", duration);
	AddEvent(timecode, event->GetName(), event->GetType(), (const char*)msg);
}

void rexSerializerCutscene::CreateCSEventAudio(const rexObjectCSEvent *event) const
{
	float spf = 1.0f / event->GetFPS();
	float ssf = (float)event->GetSceneStartFrame();
	float sf = (float)event->GetStartFrame();
	float timecode = (sf - ssf) * spf;
	char msg[1024];
	sprintf(msg, "\t\tAudioStreamName: \t %s",
			(const char*)(event->GetStreamName()));
	AddEvent(timecode, event->GetName(), event->GetType(), (const char*)msg);
}

void rexSerializerCutscene::CreateCSEventParticle(const rexObjectCSEvent *event) const
{
	float spf = 1.0f / event->GetFPS();
	float ssf = (float)event->GetSceneStartFrame();
	float sf = (float)event->GetStartFrame();
	float ef = (float)event->GetEndFrame();
	float timecode = (sf - ssf) * spf;
	float duration = (ef - sf) * spf;
	char msg[1024];
	Vector3 offset(event->GetOffsetX(),event->GetOffsetY(),event->GetOffsetZ());
	offset.Scale(GetUnitTypeLinearConversionFactor());
	sprintf(msg, "\t\tParentActor: \t %s\n\t\tParentBone: \t %s\n\t\tType: \t %s\n\t\tOffset: \t %f %f %f\n\t\tDuration: \t %f", 
			(const char*)(event->GetParentActor()),
			(const char*)(event->GetParentBone()),
			(const char*)(event->GetEmitterType()),
			offset.x,
			offset.y,
			offset.z,
			duration);
	AddEvent(timecode, event->GetName(), event->GetType(), (const char*)msg);
}

void rexSerializerCutscene::CreateCSEventClipPlane(const rexObjectCSEvent *event) const
{
	float spf = 1.0f / event->GetFPS();
	float ssf = (float)event->GetSceneStartFrame();
	float sf = (float)event->GetStartFrame();
	float timecode = (sf - ssf) * spf;
	char msg[1024];
	float near=event->GetClipNear()*GetUnitTypeLinearConversionFactor();
	float far=event->GetClipFar()*GetUnitTypeLinearConversionFactor();
	sprintf(msg, "\t\tClipNear: \t %f\n\t\tClipFar: %f",
			near,
			far);
	AddEvent(timecode, event->GetName(), event->GetType(), (const char*)msg);
}

void rexSerializerCutscene::CreateCSEventEndScene(const rexObjectCSEvent *event) const
{
	float spf = 1.0f / event->GetFPS();
	float sf = (float)event->GetStartFrame();
	float ef = (float)event->GetEndFrame();
	float timecode = (ef - sf) * spf;
	char msg[1024];
	sprintf(msg, ""); 
	AddEvent(timecode, event->GetName(), event->GetType(), (const char*)msg);
}

bool rexSerializerCutscene::FindAnimationSegment(const rexObject *obj, rexObjectGenericAnimationSegment **seg) const
{
	int childCount = obj->m_ContainedObjects.GetCount();
	for( int i = 0; i < childCount; i++ )
	{
		rexObject *thisObj = obj->m_ContainedObjects[i];
		rexObjectGenericAnimationSegment* thisSeg = dynamic_cast<rexObjectGenericAnimationSegment*>( thisObj );
		if( thisSeg != NULL )
		{
			*seg = thisSeg;
			return true;
		}
	}

	for( int i = 0; i < childCount; i++ )
	{
		bool found = FindAnimationSegment(obj->m_ContainedObjects[i], seg);
		if( found )
			return true;
	}

	return false;
}

void rexSerializerCutscene::AddCast(bool ref, const char *category, const char *type, const char *name) const
{
	gCastList.PushAndGrow( csCast(ref, category, type, name) );
}

void rexSerializerCutscene::AddData(const char *type, const char *name) const
{
	gDataList.PushAndGrow( csData(type, name) );
}

void rexSerializerCutscene::AddEvent(const char *type, const char *msg) const
{
	gEventList.PushAndGrow( csEvent(type, msg) );
}

void rexSerializerCutscene::AddEvent(float timecode, const char *name, const char *type, const char *msg) const
{
	gEventList.PushAndGrow( csEvent(timecode, name, type, msg) );
}

// rexSerializerCSCamera
rexResult rexSerializerCSCamera::Serialize( rexObject& object ) const
{
	rexResult result( rexResult::PERFECT );

	if( !GetExportData() )
		return result;

	const rexObjectCSCamera *csCamera = dynamic_cast<const rexObjectCSCamera*>( &object );
	if( csCamera == NULL )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
/*
	// Setup output path.
	rexUtility::CreatePath( GetOutputPath() );
	ASSET.SetPath( GetOutputPath() );

	// Open a file for writing.
	fiStream* s = ASSET.Create( csCamera->GetName(), "anim" );
	if( s )
	{
		int magic = csCamera->GetMagic();
		s->WriteInt( &magic, 1 );
		int flags = csCamera->GetFlags();
		s->WriteInt( &flags, 1 );
		int numFrames = csCamera->GetNumFrames();
		s->WriteInt( &numFrames, 1 );
		int numChannels = csCamera->GetNumChannels();
		s->WriteInt( &numChannels, 1 );
		const float *stride = csCamera->GetStride();
		s->WriteFloat( stride, 3 );

		for( int i = 0; i < numFrames; i++ )
		{
			float *data = csCamera->GetFrame(i)->mData;
			s->WriteFloat( data, numChannels );
		}

		s->Close();
	}
*/
	return result;
}


} // namespace rage
