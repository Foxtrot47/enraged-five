#include "rexGeneric/objectParticle.h"
#include "rexRAGE/serializerParticle.h"
#include "rexBase/shell.h"

#include "cranimation/animation.h"
#include "cranimation/animtrack.h"
#include "file/asset.h"
#include "vector/quaternion.h"

///////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

	rexResult rexSerializerRAGEParticle::Serialize( rexObject& object ) const
	{	
		rexResult result( rexResult::PERFECT );
		if( !GetExportData() )
			return result;

		const rexObjectGenericParticleGroup* particleGroup = dynamic_cast<const rexObjectGenericParticleGroup*>( &object );
		if( particleGroup )
		{
			ASSET.SetPath( GetOutputPath() );

			int objCount = particleGroup->m_ContainedObjects.GetCount();

			if( m_ProgressBarObjectCountCB )
				(*m_ProgressBarObjectCountCB )( objCount );	

			for( int a = 0; a < objCount; a++ )
			{
				if( m_ProgressBarObjectCurrentIndexCB )
					(*m_ProgressBarObjectCurrentIndexCB)( a );	

				rexObjectGenericParticle* particle = dynamic_cast<rexObjectGenericParticle*>( particleGroup->m_ContainedObjects[a] );
				if( particle )
				{
					result &= SerializeParticle( particle );
				}
			}		
		}
		else
		{
			result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}

		return result;
	}

	///////////////////////////////////////////////////////////////////////////////////////////

	rexResult rexSerializerRAGEParticle::SerializeParticle( rexObjectGenericParticle* particle ) const
	{
		rexResult result( rexResult::PERFECT );

		if(rexShell::DoesntSerialise())
		{
			result.AddMessage("Not serialising PARTICLE %s due to flag set.", particle->GetName().c_str());
			return result;
		}

		if( m_ProgressBarTextCB )
		{
			static char message[1024];
			sprintf( message, "Serializing particle %s.particle", (const char*)particle->GetName() );
			(*m_ProgressBarTextCB) ( message );
		}

		// Create a file for the particle
		fiStream* stream = ASSET.Create( (const char*)particle->GetName(), "particle" );
		if( !stream )
		{
			// Oh dear, something bad has happened
			result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

			// Get full path for debugging
			char acFullPathForDebugging[255];
			ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),(const char*)particle->GetName(),"particle" );

			// Flip the slashes
			for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

			// Error
			result.AddError( "Could not create file '%s'", acFullPathForDebugging);

			return result;
		}

		char buffer[512];

		sprintf( buffer, "%s, ", (const char*)particle->GetName() );
		stream->Write( buffer, (int)strlen(buffer) );

		sprintf( buffer, "%f, %f, %f, ", particle->m_LocalMatrix.d.x, particle->m_LocalMatrix.d.y, particle->m_LocalMatrix.d.z );
		stream->Write( buffer, (int)strlen(buffer) );

		sprintf( buffer, "%d, ", 1 );
		stream->Write( buffer, (int)strlen(buffer) );

		Quaternion quat;
		particle->m_LocalMatrix.ToQuaternion( quat );
		sprintf( buffer, "%f, %f, %f, %f, ", quat.x, quat.y, quat.z, quat.w );
		stream->Write( buffer, (int)strlen(buffer) );

		sprintf( buffer, "%s, ", (const char*)particle->m_EffectName );
		stream->Write( buffer, (int)strlen(buffer) );

		sprintf( buffer, "%d, ", particle->m_TriggerType );
		stream->Write( buffer, (int)strlen(buffer) );

		// Calculate the bone ID of the bone this particle is attached to (if any)
		int boneId = -1;
		if( particle->m_AttachBone != NULL )
		{
			if( particle->m_AttachBone->m_IsRoot )
			{
				// Always use 0 for the bone ID of root bones
				boneId = 0;
			}
			else
			{
				// If the bone was tagged with specific text, hash that to get the
				// bone ID, otherwise just hash the bone's name
				atString textToHash;
				if( particle->m_AttachBone->m_Tag.GetLength() > 0 && particle->m_AttachBone->m_Tag != "." )
					textToHash = particle->m_AttachBone->m_Tag;
				else
					textToHash = particle->m_AttachBone->m_Name;

				boneId = crAnimTrack::ConvertBoneNameToId(textToHash);
			}
		}

		sprintf( buffer, "%d, ", boneId );
		stream->Write( buffer, (int)strlen(buffer) );

		sprintf( buffer, "%f, ", particle->m_Scale );
		stream->Write( buffer, (int)strlen(buffer) );

		sprintf( buffer, "%d, ", particle->m_Probability );
		stream->Write( buffer, (int)strlen(buffer) );

		sprintf( buffer, "%d, ", (particle->m_IsTinted ? 1 : 0) );
		stream->Write( buffer, (int)strlen(buffer) );

		sprintf( buffer, "%f, %f, %f", particle->m_TintColor.x, particle->m_TintColor.y, particle->m_TintColor.z );
		stream->Write( buffer, (int)strlen(buffer) );

		stream->Close();

		return result;
	}

} // namespace rage
