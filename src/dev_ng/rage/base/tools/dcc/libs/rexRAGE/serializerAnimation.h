// 
// rexRAGE/serializerAnimation.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSerializerRAGESkeleton
//			 -- generates .skel files from generic skeleton information
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexSerializer
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_RMCORE_SERIALIZER_GENERICANIMATIONTOSAGANIMATION_H__
#define __REX_RMCORE_SERIALIZER_GENERICANIMATIONTOSAGANIMATION_H__

#include "rexBase/property.h"
#include "rexBase/serializer.h"
#include "rexBase/animExportCtrl.h"
#include "rexGeneric/objectAnimation.h"
#include "atl/map.h"
#include "atl/string.h"

namespace rage {
	class atString;
	class crAnimation;
	class crAnimTolerance;
	class fiStream;

	/////////////////////////////////////////////////////////////////////////////////////

	class rexSerializerRAGEAnimation : public rexSerializer
	{
	public:
		rexSerializerRAGEAnimation();
		virtual rexResult	Serialize( rexObject& object ) const; 

		rexSerializer* CreateNew() const {	rexSerializerRAGEAnimation* s = new rexSerializerRAGEAnimation(); 
											s->m_ExportAllUntaggedChunksSeperately = m_ExportAllUntaggedChunksSeperately; 
											s->m_UseBoneIDs = m_UseBoneIDs; 
											return s; }

		void		SetSkipLockedChannel( bool b )							{ m_SkipLockedChannel = b; }
		void		SetSerializeAnimCurves( bool b )						{ m_SerializeAnimCurves = b; }
		void		SetProcessNonJointTransformAndRotates( bool b )			{ m_ProcessNonJointTransformAndRotates = b; }
		void		SetExportMoverChannels( bool b )						{ m_ExportMoverChannels = b; }

		void		SetExportAllUntaggedChunksSeperately( bool b )					{ m_ExportAllUntaggedChunksSeperately = b; }
		bool		GetExportAllUntaggedChunksSeperately() const					{ return m_ExportAllUntaggedChunksSeperately; }

		void		SetUseTimeLine( bool b )				{ m_UseTimeLine = b; }
		bool		GetUseTimeLine() const					{ return m_UseTimeLine; }

		void		SetGimbelLockFix( bool b )				{ m_GimbelLockFix = b; }
		bool		GetGimbelLockFix() const				{ return m_GimbelLockFix; }

		void		SetCompressionErrorTolerance(float f)	{ m_CompressionErrorTolerance = f; }
		float		GetCompressionErrorTolerance() const	{ return m_CompressionErrorTolerance; }

		void		SetNormalizeRoot(bool nrmRoot)			{ m_NormalizeRoot = nrmRoot; }
		bool		GetNormalizeRoot() const				{ return m_NormalizeRoot; }

		void		SetProjectFlags(u16 projectFlags)		{ m_ProjectFlags = projectFlags; }
		u16			GetProjectFlags()						{ return m_ProjectFlags; }

		void		SetCompressionCost(int compCost)		{ m_CompressionCost = compCost; }
		int			GetCompressionCost() const				{ return m_CompressionCost; }

		void		SetDecompressionCost(int decompCost)	{ m_DecompressionCost = decompCost; }
		int			GetDecompressionCost() const			{ return m_DecompressionCost; }

		void		SetMaximumBlockSize(int maxBlockSize)	{ m_MaximumBlockSize = maxBlockSize; }
		int			GetMaximumBlockSize() const				{ return m_MaximumBlockSize; }

		bool		GetSkipLockedChannel() const							{ return m_SkipLockedChannel; }
		bool		GetSerializeAnimCurves() const							{ return m_SerializeAnimCurves; }
		bool		GetProcessNonJointTransformAndRotates() const			{ return m_ProcessNonJointTransformAndRotates; }
		bool		GetExportMoverChannels() const							{ return m_ExportMoverChannels; }

		int			AddChannelGroup()					{ return m_ChannelGroupCount++; }
		void		AddChannelToWrite( const char * channelInputName, const char *channelOutputName, int channelID, bool applyOnlyToRootBone, int channelGroupIndex = -1 )  
		{ m_ChannelInputNames.PushAndGrow( atString(channelInputName) ); m_ChannelOutputNames.PushAndGrow( atString(channelOutputName) ); m_ChannelSupportedForRootBoneOnly.PushAndGrow( applyOnlyToRootBone ); m_ChannelID.PushAndGrow( channelID ); m_ChannelGroup.PushAndGrow( channelGroupIndex ); }

		bool		GetUseAnimCtrlExportFile() const { return m_bUseAnimCtrlExportFile; }
		void		SetUseAnimCtrlExportFile(bool bUse) { m_bUseAnimCtrlExportFile = bUse; }
		bool		LoadAnimExportCtrlFile(const char* filePath)
		{
			return m_AnimExportCtrlSpec.LoadFromXML(filePath);
		}

	protected:
		rexResult	SerializeChunksRecursively( rexObjectGenericAnimation& animation, const rexObjectGenericAnimation::ChunkInfo& chunk ) const;
		rexResult	SerializeUntaggedChunksRecursively( rexObjectGenericAnimation& animation, const rexObjectGenericAnimation::ChunkInfo& chunk, const atArray<atString>& taggedChunks ) const;
		rexResult	SerializeSegment( rexObjectGenericAnimation& animation, rexObjectGenericAnimationSegment& segment ) const;
		rexResult	SerializeAni8(rexObjectGenericAnimation& animation, rexObjectGenericAnimationSegment& segment) const;
		void		ComputeStrideVector( rexObjectGenericAnimation& animation, float st, float et, Vector3& stride) const;
		int			GetTotalDOFs(rexObjectGenericAnimation& animation, rexObjectGenericAnimationSegment& segment, bool skippedLockedChannels) const;
		void		GetNodeDOFCount(const rexObjectGenericAnimation::ChunkInfo& bone, int& count, bool skippedLockedChannels, bool isRootBone) const;
		void		GetDOFCount(const atArray<rexObjectGenericAnimation::TrackInfo*>& channelArray, int& count, bool skippedLockedChannels, bool isRootBone) const;
		void		ExportTransformTracks(const atArray<Matrix34>& am, bool isMover, u16 identifier, bool genericTranslation, bool translateTrack, bool genericRotation, bool rotateTrack, bool scaleTrack, const crAnimTolerance& compressionTolerance, u16 framesPerChunk, crAnimation& inoutAnim) const;
		const AnimExportCtrlTrackSpec* rexSerializerRAGEAnimation::FindAnimCtrlFileTrackSpec(rexObjectGenericAnimation::ChunkInfo* bone, bool bIsRoot) const;

		bool				m_ProcessNonJointTransformAndRotates;
		bool				m_SkipLockedChannel;
		bool				m_SerializeAnimCurves;
		bool				m_UseTimeLine;
		atArray<atString>	m_ChannelInputNames, m_ChannelOutputNames;
		atArray<int>		m_ChannelGroup, m_ChannelID;
		int					m_ChannelGroupCount;
		atArray<bool>		m_ChannelSupportedForRootBoneOnly;

		mutable atMap<atString,bool> m_ChannelInputNameMap;

		bool				m_ExportAllUntaggedChunksSeperately;
		bool				m_ExportMoverChannels;
		bool				m_GimbelLockFix;
		float				m_CompressionErrorTolerance;
		bool				m_NormalizeRoot;
		u16					m_ProjectFlags;
		int					m_CompressionCost;
		int					m_DecompressionCost;
		int					m_MaximumBlockSize;

		bool				m_bUseAnimCtrlExportFile;
		AnimExportCtrlSpec	m_AnimExportCtrlSpec;
	};


	class rexPropertyAnimationSkipLockedChannel : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationSkipLockedChannel; }
	};

	/////////////////////////////

	class rexPropertyAnimationSerializeAnimCurves : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationSerializeAnimCurves; }
	};

	/////////////////////////////

	class rexPropertyAnimationProcessNonJointTransformAndRotates : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationProcessNonJointTransformAndRotates; }
	};

	/////////////////////////////

	class rexPropertyAnimationSetExportAllUntaggedChunksSeperately : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationSetExportAllUntaggedChunksSeperately; }
	};

	/////////////////////////////

	class rexPropertyAnimationExportMoverChannels : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationExportMoverChannels; }
	};

	/////////////////////////////

	class rexPropertyAnimationGimbelLockFix : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationGimbelLockFix; }
	};

	/////////////////////////////

	class rexPropertyAnimationCompressionErrorTolerance : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationCompressionErrorTolerance; }
	};

	/////////////////////////////

	class rexPropertyAnimationNormalizeRoot : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationNormalizeRoot; }
	};
		
	/////////////////////////////

	class rexPropertyAnimationCompressionCost : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationCompressionCost; }
	};

	/////////////////////////////

	class rexPropertyAnimationDecompressionCost : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationDecompressionCost; }
	};

	/////////////////////////////

	class rexPropertyAnimationMaximumBlockSize : public rexProperty
	{
		public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationMaximumBlockSize; }
	};

	/////////////////////////////

} // namespace rage

#endif

