#include "rexGeneric/objectMesh.h"
#include "rexRAGE/serializerMesh.h"
#include "rexBase/module.h"
#include "rexBase/utility.h"
#include "rexBase/shell.h"

#include "file/stream.h"
#include "file/asset.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "vector/geometry.h"

///////////////////////////////////////////////////////////////////////////////////////////
namespace rage {

rexResult rexSerializerRAGEMesh::Serialize( rexObject& object ) const
{	
	rexResult result( rexResult::PERFECT );
	if( !GetExportData() )
		return result;

	const rexObjectGenericMeshHierarchy* inputMeshHier = dynamic_cast<const rexObjectGenericMeshHierarchy*>( &object );
	if( inputMeshHier )
	{
		rexUtility::CreatePath( GetOutputPath() );
		ASSET.SetPath( GetOutputPath() );

		atArray<rexObjectGenericMesh::MaterialInfo> matInfoArray;

		int objCount = inputMeshHier->m_ContainedObjects.GetCount();
		
		inputMeshHier->GetMaterials( matInfoArray, m_UseChildrenOfLevelInstanceNodes, GetUseNonOptimizedMTLCompare());

		for( int a = 0; a < objCount; a++ )
		{
			if( m_ProgressBarObjectCountCB )
				(*m_ProgressBarObjectCountCB )( objCount );	

			if( m_ProgressBarObjectCurrentIndexCB )
				(*m_ProgressBarObjectCurrentIndexCB)( a );	

			result &= SerializeRecursive( *inputMeshHier->m_ContainedObjects[ a ], matInfoArray );
		}		

		// serialize materials
		if( m_ExportTextures )
		{
			int matCount = matInfoArray.GetCount();

			if( m_ProgressBarObjectCountCB )
				(*m_ProgressBarObjectCountCB )( matCount );	

	
			atArray<atString>	entityTextures;

			for( int a = 0; a < matCount; a++ )
			{				
				if( m_ProgressBarObjectCurrentIndexCB )
					(*m_ProgressBarObjectCurrentIndexCB)( a );	

				int inputFileCount = matInfoArray[ a ].m_InputTextureFileNames.GetCount();
				Assert( inputFileCount == matInfoArray[ a ].m_OutputTextureFileNames.GetCount() );

				if( strstr( matInfoArray[ a ].m_TypeName, "animated" ) && ( matInfoArray[ a ].m_InputTextureFileNames.GetCount() > 1 ) )
				{
					if(!rexShell::DoesntSerialise())
					{
						Assert(matInfoArray[ a ].m_AttributeValues[ 0 ].GetATString());
						fiStream* stream = ASSET.Create( *matInfoArray[ a ].m_AttributeValues[ 0 ].GetATString(), "" );
						if( stream )
						{
							char buffer[256];
							sprintf( buffer, "%d\r\n", inputFileCount );
							stream->Write( buffer, (int)strlen( buffer ) );
							for( int t = 0; t < inputFileCount; t++ )
							{
								sprintf( buffer, "%s\r\n", (const char*)matInfoArray[ a ].m_OutputTextureFileNames[ t ] );
								stream->Write( buffer, (int)strlen( buffer ) );
							}		
							stream->Close();
						}
						else
						{
							// Oh dear, something bad has happened
							result.Combine( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

							// Get full path for debugging
							char acFullPathForDebugging[255];
							ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),(*matInfoArray[ a ].m_AttributeValues[ 0 ].GetATString()), "" );

							// Flip the slashes
							for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

							// Error
							result.AddError( "Could not create file '%s'", acFullPathForDebugging);
						}
					}
				}

				for( int b = 0; b < inputFileCount; b++ )
				{					
					atString output = m_TextureOutputPath.GetLength() ? m_TextureOutputPath : m_OutputPath;
					rexUtility::CreatePath( output );
					output += "/";
					output += matInfoArray[ a ].m_OutputTextureFileNames[ b ];
					bool isPS2LightMap = false, hasAlpha = false;

					if( strstr( matInfoArray[ a ].m_TypeName, "alpha" ) )
						hasAlpha = true;

					if( strstr( matInfoArray[ a ].m_TypeName, "lightmap" ) && ( b == 0 ) )
						isPS2LightMap = true;						

					if( m_ProgressBarTextCB )
					{
						static char message[1024];
						sprintf( message, "Converting and Writing Texture %s", (const char*)matInfoArray[ a ].m_OutputTextureFileNames[ b ] );
						(*m_ProgressBarTextCB) ( message );
					}

					atString fallback("");
					atString &hint = matInfoArray[ a ].m_TextureHints.GetCount() ? matInfoArray[ a ].m_TextureHints[ b ] : fallback;
					
					result.Combine(rexUtility::ExportTexture( matInfoArray[ a ].m_InputTextureFileNames[ b ], output, hasAlpha, isPS2LightMap, hint ));
					
					if( m_ExportEntityTextureList )
					{
						atString entityTextureFileName(fiAssetManager::FileName(output));
						if(entityTextures.Find(entityTextureFileName) == -1)
							entityTextures.PushAndGrow(entityTextureFileName);
					}
				}				
			}

			//HACK!!!! 
			//Create a texture list containing only the textures for this entity, this is different than the texture
			//list created through the "ExportTexture" calls (see below), which ends up generating a list of all the
			//textures used by all exported entities.
			if( m_ExportEntityTextureList && !rexShell::DoesntSerialise())
			{
				fiStream *entityTxListStream = NULL;
				entityTxListStream = ASSET.Create("entity", "textures");
				if(!entityTxListStream)
				{
					result.Combine( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

					char debugPath[255];
					ASSET.FullPath(debugPath,sizeof(debugPath),"entity","textures" );

					// Flip the slashes
					for(int s=0; s<sizeof(debugPath); s++) if(debugPath[s] == '\\') debugPath[s] = '/';

					result.AddError( "Could not create file '%s'", debugPath);
					return result;
				}

				char entTxBuf[255];
				for(int i=0;i<entityTextures.GetCount(); i++)
				{		
					sprintf(entTxBuf,"%s\r\n", (const char*)entityTextures[i]);

					if(!GetExportEntityTextureListKeepExtenstions())
					{
						// Remove Extension
						char *pszExtension;
						if ((pszExtension = strstr(entTxBuf, ".dds")) != NULL)
						{
							sprintf(&entTxBuf[(int)(pszExtension - entTxBuf)],"\r\n");
						}
					}

					entityTxListStream->Write( entTxBuf, (int)strlen(entTxBuf) );
				}

				entityTxListStream->Close();
			}//End if if( m_ExportEntityTextureList )
		}//End if(m_ExportTextures)
	} // if( inputMeshHier )
	else
	{
		result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	} // if( inputMeshHier ) else

	return result;
}

static bool s_EnableTristripping = true;

void		rexSerializerRAGEMesh::SetTristrippingMode(bool flag) 
{
	s_EnableTristripping = flag;
}

static bool s_WriteAscii = false;

void		rexSerializerRAGEMesh::SetAsciiMode(bool flag) 
{
	s_WriteAscii = flag;
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult	rexSerializerRAGEMesh::ConvertGenericMeshToRageRenderMesh( const rexObjectGenericMesh *inputMesh,
																	   const atArray<rexObjectGenericMesh::MaterialInfo>& materialInfo,
																	   mshMesh& outputMesh) const
{
	rexResult result(rexResult::PERFECT);

	float conversionFactor=GetUnitTypeLinearConversionFactor();

	Matrix34 centeredTransformMatrix( M34_IDENTITY );
	Matrix34 pivotTransformMatrix( M34_IDENTITY );
	inputMesh->m_Matrix.UnTransform( inputMesh->m_Center, centeredTransformMatrix.d );
	inputMesh->m_Matrix.UnTransform( inputMesh->m_PivotPoint, pivotTransformMatrix.d );

	if( inputMesh->m_IsSkinned )
	{
		outputMesh.MakeSkinned();
		int offsetCount = inputMesh->m_SkinBoneMatrices.GetCount();
		for( int a = 0; a < offsetCount; a++ )
		{
			Vector3 offset=inputMesh->m_SkinBoneMatrices[ a ].d;
			offset.Scale(GetUnitTypeLinearConversionFactor());
			outputMesh.AddOffset(offset);	
		}
	}

// If zero, no extrema are computed (kinda silly)
// If one, bound sphere and AABB are computed (negligible performance difference)
// If two, bound sphere and OOBB are computed (house.mb w/out tristripping went from 10.2 to 14.2 seconds serialization time)
#define COMPUTE_EXTREMA		1

	Vector3 mini(1E10,1E10,1E10), maxi(-1E10,-1E10,-1E10);
#if COMPUTE_EXTREMA
	Mgc::Vector3 *boundPoints = NULL;
	int boundPointCount = 0, boundPointAllocated = 0;
#endif

	// Setup blend target count and copy the names
	int blendTargetCount = inputMesh->m_Vertices[0].m_TargetDeltas.GetCount();
	outputMesh.SetBlendTargetCount(blendTargetCount);
	for( int i = 0; i < blendTargetCount; i++ )
	{
		outputMesh.SetBlendTargetName(i, inputMesh->m_BlendTargets[i].m_Name);
	}

	atArray<ConstString> outputMtlsToCollapse;
	atArray<ConstString> outputMtlsToCenter;

	int mtlCount = inputMesh->m_Materials.GetCount();
	for( int a = 0; a < mtlCount; a++ )
	{	
		const rexObjectGenericMesh::MaterialInfo& matInfo = inputMesh->m_Materials[ a ];

		char buffer[256]="#0";
		
		int matListCount = materialInfo.GetCount();

		for( int s = 0; s < matListCount; s++ )
		{
			if( matInfo.IsSame( materialInfo[ s ], GetUseNonOptimizedMTLCompare()) )
			{
				sprintf( buffer, "#%d", s );
				break;
			}
		}

		int srcPrimCount = matInfo.m_PrimitiveIndices.GetCount();
		int srcPrimOffset = 0;
		const int maxBones = 16384;
		atRangeArray<float,maxBones> weightArray;
		//float* weightArray = new float[maxBones];

		// Introduce extra materials as necessary to avoid crossing matrix limits.
		do {
			mshMaterial &mtl = outputMesh.NewMtl();
			mtl.Name = buffer;

			for (int i=0; i<maxBones; i++)
				weightArray[i] = 0;

			int totalAdj = 0;
			int matrices = 0;
			int lastGoodPrimitive = -1;
			for( int b = srcPrimOffset; b < srcPrimCount; b++ ) {
				const rexObjectGenericMesh::PrimitiveInfo& primInfo = inputMesh->m_Primitives[ matInfo.m_PrimitiveIndices[ b ] ];
				int adjCount = primInfo.m_Adjuncts.GetCount();
				for( int c = 0; c < adjCount; c++ )
				{
					const rexObjectGenericMesh::AdjunctInfo& adjInfo = primInfo.m_Adjuncts[ c ];
					const rexObjectGenericMesh::VertexInfo& vertInfo = inputMesh->m_Vertices[ adjInfo.m_VertexIndex ];
					int boneCount = vertInfo.m_BoneWeights.GetCount();
					for (int w = 0; w < boneCount; w++) {
						if (vertInfo.m_BoneWeights[w] && !weightArray[vertInfo.m_BoneIndices[w]]) {
							weightArray[vertInfo.m_BoneIndices[w]] = vertInfo.m_BoneWeights[w];
							++matrices;
						}
					}
				}
				if (matrices > m_MaxMatricesPerMaterial)
				{
					Displayf("had to insert material break because of matrix limit");
					break;
				}
				totalAdj += adjCount;
				lastGoodPrimitive = b;
			}
			AssertMsg(lastGoodPrimitive != -1,"too many matrices in a single primitive!?!?!?");

			mtl.Prim.Reallocate( lastGoodPrimitive + 1 - srcPrimOffset );
			mtl.Verts.Reallocate( totalAdj );
			Vector2 emptyTexCoord(0,0);

#if COMPUTE_EXTREMA
			// Resize bounding region array so it's always big enough.
			if (boundPointAllocated < boundPointCount + totalAdj) {
				while (boundPointAllocated < boundPointCount + totalAdj) {
					if (boundPointAllocated)
						boundPointAllocated *= 2;
					else
						boundPointAllocated = 1024;
				}
				Mgc::Vector3 *newPoints = new Mgc::Vector3[boundPointAllocated];
				if (boundPointCount) {
					memcpy(newPoints,boundPoints,boundPointCount * sizeof(Mgc::Vector3));
					delete[] boundPoints;
				}
				boundPoints = newPoints;
			}
#endif
		
			for( int b = srcPrimOffset; b <= lastGoodPrimitive; b++ )
			{
				const rexObjectGenericMesh::PrimitiveInfo& primInfo = inputMesh->m_Primitives[ matInfo.m_PrimitiveIndices[ b ] ];
				int adjCount = primInfo.m_Adjuncts.GetCount();

				mshPrimitive& prim = mtl.Prim.Append();

				prim.Type = mshPOLYGON;
				prim.Priority = mtl.Priority;
				prim.Idx.Reallocate( adjCount );

				for( int c = 0; c < adjCount; c++ )
				{
					const rexObjectGenericMesh::AdjunctInfo& adjInfo = primInfo.m_Adjuncts[ c ];
					
					int numUVSets = adjInfo.m_TextureCoordinates.GetCount();
					int numCPVSets = adjInfo.m_Colors.GetCount();

					const rexObjectGenericMesh::VertexInfo& vertInfo = inputMesh->m_Vertices[ adjInfo.m_VertexIndex ];

					Vector3 vertPos( vertInfo.m_Position );
					
					if( m_ExportRootBoneMeshesRelativeToPivot && !inputMesh->m_BoneIndex )
						pivotTransformMatrix.UnTransform( vertPos );							
					else if( m_ExportRootBoneMeshesRelativeToMeshCenter && !inputMesh->m_BoneIndex )
						centeredTransformMatrix.UnTransform( vertPos );
					else
					{
						inputMesh->m_Matrix.Transform( vertPos );
						inputMesh->m_ParentBoneMatrix.UnTransform( vertPos );
					}


					// const bool unique = false;
					// const float tol = 0.0001f;

					int boneCount = vertInfo.m_BoneWeights.GetCount();
					Assertf(boneCount <= maxBones, "%d bones is more than the upper limit of %d bones", boneCount, maxBones);
												
					for( int d = 0; d < boneCount; d++ )
					{
						weightArray[ d ] = 0.0f;
					}
					for( int d = 0; d < boneCount; d++ )
					{
						if (vertInfo.m_BoneIndices[ d ]!=-1 && !weightArray[vertInfo.m_BoneIndices[d]])
							weightArray[vertInfo.m_BoneIndices[ d ]] = vertInfo.m_BoneWeights[ d ];
					}

					Vector3 pos, nrm = inputMesh->m_Normals[ adjInfo.m_NormalIndex ];
					nrm.Normalize();

					if (inputMesh->m_BoneIndex)
					{
						inputMesh->m_Matrix.Transform3x3( nrm );
						inputMesh->m_ParentBoneMatrix.UnTransform3x3( nrm );
						nrm.Normalize();
					}

					mshBinding binding;
					binding.Reset();
					if( inputMesh->m_IsSkinned )
						binding.Init( weightArray, boneCount, mshMaxMatricesPerVertex );
					int uvCount = numUVSets<mshMaxTexCoordSets? numUVSets : mshMaxTexCoordSets;

					vertPos.Scale(conversionFactor);
#if COMPUTE_EXTREMA
					Assert(boundPointCount < boundPointAllocated);
					Mgc::Vector3 &nbv = boundPoints[boundPointCount++];
					nbv.x = vertPos.x;
					nbv.y = vertPos.y;
					nbv.z = vertPos.z;
#endif
					mini.Min(mini,vertPos);
					maxi.Max(maxi,vertPos);

					if(matInfo.m_BillboardCollapse)
					{
						if(adjInfo.m_TextureCoordinates.GetCount()<3)
							result.AddError("Vertex %d of object %s has a billboard material applied, but UV channel 3 for scaling is missing!", adjInfo.m_VertexIndex, inputMesh->GetMeshName());
						else
						{
							float scale = Max(adjInfo.m_TextureCoordinates[2].x,adjInfo.m_TextureCoordinates[2].y);
							mini.Min(mini,vertPos-Vector3(scale,scale,scale));
							maxi.Max(maxi,vertPos+Vector3(scale,scale,scale));
						}
					}

					static Vector4 DefaultColor(1,1,1,1);

					int vertIdx = mtl.AddVertex(
						vertPos,
						binding,
						nrm,
						adjInfo.m_Colors[0],
						(numCPVSets > 1)? adjInfo.m_Colors[1] : DefaultColor,
						(numCPVSets > 2)? adjInfo.m_Colors[2] : DefaultColor,
						uvCount,
						uvCount? &adjInfo.m_TextureCoordinates[0] : &emptyTexCoord,
						0,
						NULL);

					// Copy the blend target deltas into the mshVertex we just added
					for( int target = 0; target < blendTargetCount; target++ )
					{
						mshBlendTargetDelta delta;
						Convert(delta.Position, vertInfo.m_TargetDeltas[target]);
						delta.Position.w = 0;

						if(vertInfo.m_TargetNormalDeltas.GetCount())
						{
							//Vertex normal deltas are already available, so copy them over.
							Convert(delta.Normal, vertInfo.m_TargetNormalDeltas[target]);
						}
						else
						{
							//The delta will be calculated later...
							delta.Normal.Zero();
						}

						delta.Normal.w = 0;
						delta.Tangent.Zero();
						delta.BiNormal.Zero();
						mtl.SetBlendTargetDelta(vertIdx, target, delta);
					}
		
					prim.Idx.Append() = vertIdx;
				}
			}//End for( int b = srcPrimOffset; b <= lastGoodPrimitive; b++ )

			//Now that all of the vertices are added to the material go through and setup the mesh channels for any 
			//blind data attached to the generic mesh
			int blindChannelCount = inputMesh->m_BlindDataIds.GetCount();
			if(blindChannelCount)
			{
				atArray<mshChannel*> mshBlindChannels;

				//Add the channels
				for(int i=0; i<blindChannelCount; i++)
				{
					mtl.AddChannel(inputMesh->m_BlindDataIds[i], 1);
					mshBlindChannels.PushAndGrow((mshChannel*)(&(mtl.GetChannel(i))));
				}
			
				int blindVertIdx = 0;
				for( int b = srcPrimOffset; b <= lastGoodPrimitive; b++ )
				{
					const rexObjectGenericMesh::PrimitiveInfo& primInfo = inputMesh->m_Primitives[ matInfo.m_PrimitiveIndices[ b ] ];
					int adjCount = primInfo.m_Adjuncts.GetCount();

					for( int c = 0; c < adjCount; c++ )
					{
						const rexObjectGenericMesh::AdjunctInfo& adjInfo = primInfo.m_Adjuncts[ c ];
						const rexObjectGenericMesh::VertexInfo& vertInfo = inputMesh->m_Vertices[ adjInfo.m_VertexIndex ];

						for(int i=0; i<blindChannelCount; i++)
						{
							mshChannelData& chnData = mshBlindChannels[i]->Access(blindVertIdx,0);
							if(inputMesh->m_BlindDataTypes[i] == rexValue::FLOAT)
							{
								//Set the value as a float
								chnData.f = vertInfo.m_BlindData[i].toFloat();
							}
							else
							{
								//Set the value as a signed integer
								chnData.s = vertInfo.m_BlindData[i].toInt();
							}
						}

						blindVertIdx++;
					}
				}//End for( int b = srcPrimOffset; ...

				if(blindVertIdx != mtl.Verts.GetCount())
				{
					result.Set( rexResult::ERRORS );
					result.AddErrorCtx(inputMesh->GetMeshName().c_str(), "Vertex count mismatch during mesh blind data serialization, %d blind verts doesn't match up with %d mesh verts",
						blindVertIdx, mtl.Verts.GetCount());
					return result;
				}
			}//End if(blindChannelCount)

			srcPrimOffset = lastGoodPrimitive + 1;

			if(matInfo.m_BillboardCollapse)
			{
				outputMtlsToCollapse.PushAndGrow(mtl.Name, 4);
			}
			if (matInfo.m_FacingCollapse)
			{
				outputMtlsToCenter.PushAndGrow(mtl.Name, 4);
			}
		} while (srcPrimOffset < srcPrimCount);
	}

#if COMPUTE_EXTREMA

	if (boundPointCount) {
		const Mgc::Vector3 extremePoints[] = {
			Mgc::Vector3(mini.x,mini.y,mini.z),
			Mgc::Vector3(maxi.x,maxi.y,maxi.z)
		};
		Mgc::Sphere sphere = Mgc::MinSphere(2, &extremePoints[0]);
		outputMesh.SetBoundSphere(Vector4((float)sphere.Center().x, (float)sphere.Center().y, (float)sphere.Center().z,(float)sphere.Radius()));

		Matrix34 bbMatrix;
	#if COMPUTE_EXTREMA == 2
		Mgc::Box3 box = Mgc::MinBox(boundPointCount, boundPoints);
		bbMatrix.a.x = (float)box.Axis(0).x;
		bbMatrix.a.y = (float)box.Axis(0).y;
		bbMatrix.a.z = (float)box.Axis(0).z;
		bbMatrix.b.x = (float)box.Axis(1).x;
		bbMatrix.b.y = (float)box.Axis(1).y;
		bbMatrix.b.z = (float)box.Axis(1).z;
		bbMatrix.c.x = (float)box.Axis(2).x;
		bbMatrix.c.y = (float)box.Axis(2).y;
		bbMatrix.c.z = (float)box.Axis(2).z;
		bbMatrix.d.x = (float)box.Center().x;
		bbMatrix.d.y = (float)box.Center().y;
		bbMatrix.d.z = (float)box.Center().z;
		outputMesh.SetBoundBox(bbMatrix,Vector3((float)box.Extent(0),(float)box.Extent(1),(float)box.Extent(2)));
	#else	// COMPUTE_EXTREMA == 2
		bbMatrix.Identity();
		bbMatrix.d.Lerp(0.5f,mini,maxi);
		Vector3 size;
		size.Subtract(maxi,mini);
		size.Scale(0.5f);
		outputMesh.SetBoundBox(bbMatrix,size);
	#endif	// COMPUTE_EXTREMA == 2
	}

	if(boundPoints)
		delete [] boundPoints;

#endif	// COMPUTE_EXTREMA
	if(GetWeldWithoutQuantize() && GetWeld())
	{
		outputMesh.WeldWithoutQuantize();
	}
	else if(GetWeld())
	{
		outputMesh.Weld();
	}

	// only do the comparison if the mesh is not the render mesh, and the comparison property was set.
	int expectedVertCount = GetExpectedVertexCount(inputMesh->GetMeshName());
	if(expectedVertCount != -1 && expectedVertCount != outputMesh.GetVertexCount())
	{
		result.Set( rexResult::ERRORS );
		result.AddErrorCtx(inputMesh->GetMeshName().c_str(), "Mesh '%s' has duplicate verts (%d != %d). (https://devstar.rockstargames.com/wiki/index.php/General_Cloth_Setup#Limitations) Check continuous UVs, CPV, smoothing groups and whether you edited normals!", (const char*)inputMesh->GetMeshName(), expectedVertCount, outputMesh.GetVertexCount() );
		return result;
	}

	if (m_PolyMode != POLYGON) {
		outputMesh.Triangulate();
		//outputMesh.ComputeNormals();
		outputMesh.ComputeTanBi(0);
	}

	switch(m_PolyMode)
	{
		case TRIANGLE:
			outputMesh.CacheOptimize();
			break;
		case TRISTRIP:
		{
			bool tristripped = false;

			//First check global cmd line tri-stripping option, this overrides everything else
			if (s_EnableTristripping)
			{
				//.Rule file property will override any per instance tri stripping property.
				if(!m_DisableTriStripping)
				{
					//Lastly check if a per-instance notristrip flag has been set on this mesh
					bool bInstanceNoTriStrip = false;
					int attrIdx = inputMesh->m_InstAttrNames.Find(atString("notristrip"));
					if( (attrIdx != -1) && 
						(inputMesh->m_InstAttrValues[attrIdx].GetType() == rexValue::BOOLEAN) )
							bInstanceNoTriStrip = inputMesh->m_InstAttrValues[attrIdx].vBoolean;

					if(!bInstanceNoTriStrip) 
					{
						outputMesh.Tristrip();
						tristripped = true;
					}
				}
			}
			if (!tristripped)
				outputMesh.CacheOptimize();
			break;
		}
		case POLYGON:
			break;
		default:
			Assert(0);
			break;
	}

	// Do material merge after tristripping because smaller materials should tristrip faster.
	// The assumption here of course is that the duplicate materials were not good candidates
	// for tristripping anyway, so we should check that at some point!
	// We also cannot do this yet for skinned creatures because we would undo the material
	// breaks we just inserted earlier!
	if (!inputMesh->m_IsSkinned)	
		outputMesh.MergeSameMaterials();

	//Finally copy over the blind data ids
	/*
	for(int i=0; i<inputMesh->m_FloatBlindDataIds.GetCount(); i++)
		outputMesh.SetFloatBlindID(i, (mshVertexFloatBlindID)inputMesh->m_FloatBlindDataIds[i]);
	*/

	if( blendTargetCount )
	{
		//Check if vertex normal deltas were already provided in the input mesh
		bool bCalcNormalDeltas = true;
		if(inputMesh->m_Vertices.GetCount())
		{
			if(inputMesh->m_Vertices[0].m_TargetNormalDeltas.GetCount())
				bCalcNormalDeltas = false;
		}

		result.Combine(GenerateBlendTargetDeltas(outputMesh, bCalcNormalDeltas));
	}

	// Reorder vertices by first use; significant win on PS3 and also makes the ascii files a little easier to read.
	outputMesh.ReorderVertices();

	for(int mtlIndex=0;mtlIndex<outputMesh.GetMtlCount();mtlIndex++)
	{
		mshMaterial& mtl = outputMesh.GetMtl(mtlIndex);
		if(outputMtlsToCollapse.Find(mtl.Name)==-1)
			continue;

		mshMaterial::TriangleIterator triIt = mtl.BeginTriangles();
		
		int tricount = 0;
		for( int primIdx=0; primIdx<mtl.Prim.GetCount(); primIdx++ )
		{
			tricount += mtl.Prim[primIdx].GetTriangleCount();
		}

		if( tricount % 2 != 0 )
		{
			result.Set( rexResult::ERRORS );
			result.AddErrorCtx(inputMesh->GetMeshName().c_str(), "Mesh '%s' has been marked as collapsible but has %i tris.  Expecting a billboard.", (const char*)inputMesh->GetMeshName(), tricount );
			return result;
		}
		for (;triIt!=mtl.EndTriangles();)
		{
			int v[6];
			triIt.GetVertIndices(v[0],v[1],v[2]);
			++triIt;
			triIt.GetVertIndices(v[3],v[4],v[5]);
			++triIt;
			Vector3 centre(0.0f, 0.0f, 0.0f);
			for(int k=0;k<6;k++)
				centre += mtl.Verts[v[k]].Pos;
			centre /= 6;
			
			for(int k=0;k<6;k++)
				mtl.Verts[v[k]].Pos = centre;
		}
	}

	for(int mtlIndex=0;mtlIndex<outputMesh.GetMtlCount();mtlIndex++)
	{
		mshMaterial& mtl = outputMesh.GetMtl(mtlIndex);
		if(outputMtlsToCenter.Find(mtl.Name)==-1)
			continue;

		int tricount = 0;
		for( int primIdx=0; primIdx<mtl.Prim.GetCount(); primIdx++ )
		{
			tricount += mtl.Prim[primIdx].GetTriangleCount();
		}

		// Set Position for Polygon Mesh vertices at center with offset in texture coordinates
		
		// Create Array large enough for all triangles and mark as unused
		// Shared indices are considered group of triangles
		// Array to mark indices used - If not already used
		// Array to mark faces used already - Not == Unused

		const u32 FaceUnused = 0xFFFFFFFF;
		mshArray<u32> Faces;
		Faces.Init(FaceUnused, tricount);

		const u32 IndexUnused = 0xFFFFFFFF;
		mshArray<u32> IndexGroups;
		IndexGroups.Init(IndexUnused, tricount * 3);

		u32 uGroupCount = 0;

		do 
		{
			// Loop until all faces are marked used
			// Find free unused face
			mshMaterial::TriangleIterator FreeTriIt;
			u32 uUnusedFace = FaceUnused;
			s32 iFace = 0;

			for (FreeTriIt = mtl.BeginTriangles(); FreeTriIt!=mtl.EndTriangles(); ++FreeTriIt)
			{
				if (Faces[iFace] == FaceUnused)
				{
					// New face group found
					uUnusedFace = iFace;
					Faces[iFace] = uGroupCount;
					break;
				}
				++iFace;
			}

			if (uUnusedFace == FaceUnused)
			{
				// All groups of triangles have been found - Stop and center them
				break;
			}

			// Mark all indices as used
			int v[3];
			FreeTriIt.GetVertIndices(v[0],v[1],v[2]);
			IndexGroups[v[0]] = uGroupCount;
			IndexGroups[v[1]] = uGroupCount;
			IndexGroups[v[2]] = uGroupCount;

			// Loop over and over again until no new indices are marked as unused
			bool bMarkedIndex;
			do
			{
				bMarkedIndex = false;
				s32 iFace = 0;

				for (mshMaterial::TriangleIterator triIt = mtl.BeginTriangles(); triIt!=mtl.EndTriangles(); ++triIt )
				{
					int v[3];
					triIt.GetVertIndices(v[0],v[1],v[2]);
					if ((IndexGroups[v[0]] == uGroupCount) ||
						(IndexGroups[v[1]] == uGroupCount) ||
						(IndexGroups[v[2]] == uGroupCount))
					{
						// Triangle may or may not be part of group already check if any indices are not already marked
						if ((IndexGroups[v[0]] != uGroupCount) ||
							(IndexGroups[v[1]] != uGroupCount) ||
							(IndexGroups[v[2]] != uGroupCount))
						{
							Faces[iFace] = uGroupCount;
							IndexGroups[v[0]] = IndexGroups[v[1]] = IndexGroups[v[2]] = uGroupCount;
							bMarkedIndex = true;
						}
					}
					iFace++;
				}
			} while(bMarkedIndex);

			// Full group found - Center group for camera facing
			Vector3 centre(0.0f, 0.0f, 0.0f);
			u32 uFaceCount = 0;

			// Average all point in group
			mshMaterial::TriangleIterator triIt;
			for (triIt = mtl.BeginTriangles();triIt!=mtl.EndTriangles();++triIt)
			{
				int v[3];
				triIt.GetVertIndices(v[0],v[1],v[2]);
				if (IndexGroups[v[0]] == uGroupCount)
				{
					for(int k=0;k<3;k++)
						centre += mtl.Verts[v[k]].Pos;
					uFaceCount++;
				}
			}

			// Centre of Group
			centre /= ((float)uFaceCount * 3.0f);

			// Assign center to binormal
			for (triIt = mtl.BeginTriangles();triIt!=mtl.EndTriangles();++triIt)
			{
				int v[3];
				triIt.GetVertIndices(v[0],v[1],v[2]);
				if (IndexGroups[v[0]] == uGroupCount)
				{
					for(int k=0;k<3;k++)
					{
						// Hard coded to binormal which means it needs a tangent in order to work which will only work with normal maps
						mtl.Verts[v[k]].TanBi[0].B = Vector3(centre.x, centre.y, centre.z);
					}
				}
			}
			// Start next group
			uGroupCount++;
		} while(true);
	}
	
	if (m_PolyMode != POLYGON && GetRemoveDegenerates())
	{
		outputMesh.RemoveDegenerates();

		if(HasMeshDegenerateTriangles(outputMesh))
		{
			result.AddError("Found degenerate triangles in mesh after all processing!",inputMesh->GetMeshName().c_str());
			result.Combine(rexResult::ERRORS);
		}
	}

	return result;
}

bool IsOnLine(const Vector3 endPoint1, const Vector3 endPoint2, const Vector3 checkPoint)
{
	return	((((double)checkPoint.y - endPoint1.y)) / ((double)(checkPoint.x - endPoint1.x))
		==    ((double)(endPoint2.y - endPoint1.y)) / ((double)(endPoint2.x - endPoint1.x))) &&
			((((double)checkPoint.y - endPoint1.y)) / ((double)(checkPoint.z - endPoint1.z))
		==    ((double)(endPoint2.y - endPoint1.y)) / ((double)(endPoint2.z - endPoint1.z))) &&
			((((double)checkPoint.x - endPoint1.x)) / ((double)(checkPoint.z - endPoint1.z))
		==    ((double)(endPoint2.x - endPoint1.x)) / ((double)(endPoint2.z - endPoint1.z)));
}

bool rexSerializerRAGEMesh::HasMeshDegenerateTriangles(mshMesh& mesh) const
{
	for(int mtlIndex=0;mtlIndex<mesh.GetMtlCount();mtlIndex++)
	{
		mshMaterial& mtl = mesh.GetMtl(mtlIndex);

		mshMaterial::TriangleIterator triIt1;
		for (triIt1 = mtl.BeginTriangles();triIt1!=mtl.EndTriangles();++triIt1)
		{
			int v1[3];
			triIt1.GetVertIndices(v1[0],v1[1],v1[2]);
			if(IsOnLine(mtl.GetVertex(v1[0]).Pos,mtl.GetVertex(v1[1]).Pos,mtl.GetVertex(v1[2]).Pos))
				return true;
		}
	}
	return false;
}


rexResult	rexSerializerRAGEMesh::GenerateBlendTargetDeltas( mshMesh& outMesh, bool bCalcVertNormalDeltas ) const
{
	float conversionFactor=GetUnitTypeLinearConversionFactor();

	rexResult result( rexResult::PERFECT );

	for( int i = 0; i < outMesh.GetBlendTargetCount(); i++ )
	{
		// Make a copy of the output mesh
		mshMesh copy = outMesh;

		// Apply all the position deltas for this target
		for( int mat = 0; mat < copy.GetMtlCount(); mat++ )
		{
			mshMaterial& mtl = copy.GetMtl(mat);
			for( int vert = 0; vert < mtl.GetVertexCount(); vert++ )
			{
				Vector3 vPosDelta;
				Convert(vPosDelta, mtl.GetBlendTargetDelta(vert, i).Position);
				vPosDelta.Scale(conversionFactor);
				Vector3 vPos = mtl.GetPos(vert);
				vPos += vPosDelta;
				mtl.SetPos(vert, vPos);

				//If normal deltas were not supplied in the source mesh this will just
				//be offsetting by zero..
				Vector3 vNrmDelta;
				Convert(vNrmDelta, mtl.GetBlendTargetDelta(vert, i).Normal);
				Vector3 vNrm = mtl.GetNormal(vert);
				vNrm += vNrmDelta;
				mtl.SetNormal(vert, vNrm);
			}
		}

		// Recompute normals
		if(bCalcVertNormalDeltas)
			copy.ComputeNormals();

		// Recompute tangents and bi-normals
		copy.ComputeTanBi(0);

		// Compute the deltas and write them back into the output mesh
		for( int mat = 0; mat < copy.GetMtlCount(); mat++ )
		{
			mshMaterial& copymtl = copy.GetMtl(mat);
			mshMaterial& origmtl = outMesh.GetMtl(mat);
			for( int vert = 0; vert < copymtl.GetVertexCount(); vert++ )
			{
				mshVertex& origvert = origmtl.Verts[vert];
				mshVertex& copyvert = copymtl.Verts[vert];

				// Set the data in thi blend target delta structure for this vertex/target
				if(bCalcVertNormalDeltas)
				{
					Vector3 vNormalDelta = copyvert.Nrm - origvert.Nrm;
					origvert.BlendTargetDeltas[i].Normal.Set(vNormalDelta.x, vNormalDelta.y, vNormalDelta.z, 0);
				}
				
				Vector3 vTangentDelta = copyvert.TanBi[0].T - origvert.TanBi[0].T;
				origvert.BlendTargetDeltas[i].Tangent.Set(vTangentDelta.x, vTangentDelta.y, vTangentDelta.z, 0);

				Vector3 vBinormal = copyvert.TanBi[0].B;
				origvert.BlendTargetDeltas[i].BiNormal.Set(vBinormal.x, vBinormal.y, vBinormal.z, 0);
			}
		}
	}

	return result;
}

rexResult	rexSerializerRAGEMesh::SerializeRecursive( const rexObject& object, const atArray<rexObjectGenericMesh::MaterialInfo>& materialInfo ) const
{
	rexResult result( rexResult::PERFECT );

	const rexObjectGenericMeshHierarchy* inputMeshHier = dynamic_cast<const rexObjectGenericMeshHierarchy*>( &object );
	if( inputMeshHier )
	{
		int objCount = inputMeshHier->m_ContainedObjects.GetCount();
		
		for( int a = 0; a < objCount; a++ )
		{
			if( m_ProgressBarObjectCountCB )
				(*m_ProgressBarObjectCountCB )( objCount );	

			if( m_ProgressBarObjectCurrentIndexCB )
				(*m_ProgressBarObjectCurrentIndexCB)( a );	 

			result &= SerializeRecursive( *inputMeshHier->m_ContainedObjects[ a ], materialInfo );
		}		
	}
	else
	{
		const rexObjectGenericMesh *inputMesh = dynamic_cast<const rexObjectGenericMesh*>( &object );
		if( inputMesh && inputMesh->m_IsValid && inputMesh->GetTriCount() > 0 )
		{
			// remove these two lines to re-enable level instance cpv export -- n8
			if( !( m_UseChildrenOfLevelInstanceNodes || !inputMesh->m_ChildOfLevelInstanceNode ) )
				return result;

			if( m_ProgressBarTextCB )
			{
				static char message[1024];
				sprintf( message, "Serializing mesh %s.%s", (const char*)inputMesh->GetMeshName(), ( m_UseChildrenOfLevelInstanceNodes || !inputMesh->m_ChildOfLevelInstanceNode ) ? "mesh" : "cpv" );
				(*m_ProgressBarTextCB) ( message );
			}

			// convert to mshMesh here and write the data out		
			mshMesh mesh;
			result.Combine(ConvertGenericMeshToRageRenderMesh(inputMesh, materialInfo, mesh));

			//Perform some basic verification on the mesh to ensure it is valid
			int mCount = mesh.GetMtlCount();
			int totalIndices = 0;

			if( !mCount )
			{
				inputMesh->m_IsValid = false;
				result.Combine( rexResult::ERRORS, rexResult::VERIFICATION_FAILURE);
				result.AddError( "No materials found in '%s.mesh'\n", (const char*)inputMesh->GetMeshName() );
			}
			else
			{
				//Count the total numver of vert indices used by the mesh, to ensure it is within the
				//valid range
				for( int m = 0; m < mCount; m++ )
				{
					mshMaterial& mat = mesh.GetMtl(m);

					int primCount = mat.Prim.GetCount();
					if (primCount == 1 && mat.Prim[0].Type == mshTRIANGLES) 
						totalIndices = mat.Prim[0].Idx.GetCount();
					else
					{
						totalIndices = (primCount-1)*2;
						for (int i=0; i<primCount; i++)
							totalIndices += mat.Prim[ i ].Idx.GetCount();
					}
				}

				/*if(totalIndices >= 65535)
				{
					inputMesh->m_IsValid = false;
					result.Set( rexResult::ERRORS, rexResult::VERIFICATION_FAILURE );
					result.AddMessage( "Too many vertex indices (%d) used in '%s.mesh'\n", totalIndices, (const char*)inputMesh->GetMeshName() );
				}*/
			}

			if( inputMesh->m_IsValid )
			{
				if(!rexShell::DoesntSerialise())
				{
					// determine if there are any blend targets that need to be created.
					if( (m_SplitBlendsIntoUniqueMeshes || m_MicroMorphs) && mesh.GetBlendTargetCount() > 0 )
					{
						// LPXO: Here is where the hack that will split one mesh with blends into n meshes will god
						atArray<mshMesh> outputmeshes;
						rexUtility::SplitMeshWithBlends( outputmeshes, &mesh, m_MicroMorphs );
						
						mesh.SetBlendTargetCount(0);
						mshMesh &origmesh = outputmeshes.Insert(0);
						origmesh = mesh;

						for( int i=0; i<outputmeshes.GetCount(); i++ )
						{
							char buffer[256];
							//
							if( i == 0)
							{
								sprintf( buffer, "%s", inputMesh->GetMeshName().c_str() );
								
							}
							else
							{
								const char* pName = mesh.GetBlendTargetName( i-1 );
								if (m_SplitBlendsIntoUniqueMeshes)
									sprintf( buffer, "head_splits/%s",  pName);
								else if (m_MicroMorphs)
									sprintf( buffer, "micro_morphs/%s", pName);

								ASSET.CreateLeadingPath( buffer );
							}
							fiStream* stream = ASSET.Create( buffer, "mesh" );
							if( stream )
							{
								if (!s_WriteAscii) {
									stream->PutCh(26); // i'm binary
									fiBinTokenizer tok;
									tok.Init( buffer, stream );
									mshSerializer serializer( tok, true );
									outputmeshes[i].Serialize( serializer );		
								}
								else 
								{
									fiAsciiTokenizer tok;
									tok.Init( buffer, stream );
									mshSerializer serializer( tok, true );
									outputmeshes[i].Serialize( serializer );		
								}
								stream->Close();
							}
						}
					}
					else if( ( m_UseChildrenOfLevelInstanceNodes || !inputMesh->m_ChildOfLevelInstanceNode ) )
					{
						fiStream* stream = ASSET.Create( inputMesh->GetMeshName(), "mesh" );
						if( stream )
						{
							if (!s_WriteAscii) {
								stream->PutCh(26); // i'm binary
								fiBinTokenizer tok;
								tok.Init( inputMesh->GetMeshName(), stream );
								mshSerializer serializer( tok, true );
								mesh.Serialize( serializer );		
							}
							else 
							{
								fiAsciiTokenizer tok;
								tok.Init( inputMesh->GetMeshName(), stream );
								mshSerializer serializer( tok, true );
								mesh.Serialize( serializer );		
							}
							stream->Close();
						}
						else
						{
							// Oh dear, something bad has happened
							inputMesh->m_IsValid = false;
							result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

							// Get full path for debugging
							char acFullPathForDebugging[255];
							ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),inputMesh->GetMeshName(),"mesh" );

							// Flip the slashes
							for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

							// For some crazy reason, the result of ASSET.FullPath has the extension on twice, so chop it off
							for(int s=sizeof(acFullPathForDebugging) - 1; s>1; s--) 
							{
								if(acFullPathForDebugging[s] == '.') 
								{
									acFullPathForDebugging[s] = '\0'; 
									break;
								}
							}

							// Error
							result.AddError( "Could not create file '%s.mesh'", acFullPathForDebugging);
						}
					}
				}
				else
					result.AddMessageCtx(inputMesh->GetMeshName().c_str(), "not serialising MESH %s due to flag set.", inputMesh->GetMeshName().c_str());
			}

// might have changed state, so save and unload mesh
//				meshPtr->Save();
//				meshPtr->Unload();
		}
		else
		{
			result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		} // if(meshPtr) else
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEMesh::SerializeBlendTargets ( mshMesh& baseMesh, 
														const rexObjectGenericMesh* inputMesh, 
														const atArray<rexObjectGenericMesh::MaterialInfo>& materialInfo) const
{
	rexResult result(rexResult::PERFECT);

	if(rexShell::DoesntSerialise())
	{
		return result;
	}


	atArray<mshMesh*>	targetRenderMeshes;
	atArray<Vector3>	targetOffsets;

	//Convert each blend target from a rex generic mesh, into a rage renderer mesh
	//so we can compute the deta's
	int blendTargetCount = inputMesh->m_BlendTargets.GetCount();
	for(int btIndex = 0; btIndex < blendTargetCount; btIndex++ )
	{
		rexObjectGenericMesh *pGenericTargetMesh = inputMesh->m_BlendTargets[btIndex].m_GenericMesh;
		Assertf( (pGenericTargetMesh), "Missing generic target mesh for base blendshape mesh (%s)", (const char*)inputMesh->GetMeshName());

		mshMesh* pRenderMesh = new mshMesh();
		targetRenderMeshes.PushAndGrow(pRenderMesh);

		result.Combine(ConvertGenericMeshToRageRenderMesh(pGenericTargetMesh, materialInfo, *pRenderMesh));
		if(result.Errors())
			return result;

		//Store the offsets for the positions to transform the blendtargets into the same world
		//space position as the base object.
		Vector3 targetOffset;
		targetOffset = inputMesh->m_BlendTargets[btIndex].m_ToBaseObjectTransform.GetVector(3);
		targetOffsets.PushAndGrow(targetOffset);
	}

	//For each material in the input mesh output a blend shape delta file
	int mtlCount = inputMesh->m_Materials.GetCount();
	for(int mtlIdx = 0; mtlIdx < mtlCount; mtlIdx++)
	{
		//Get the number of targets that modifify the mesh this material is applied to
		const rexObjectGenericMesh::MaterialInfo& gnrcMtlInfo = inputMesh->m_Materials[mtlIdx];
		int targetIndicesCount = gnrcMtlInfo.m_BlendTargetIndices.GetCount();
		if( targetIndicesCount == 0)
			continue;

		const mshMaterial& baseMtl = baseMesh.GetMtl(mtlIdx);
		int baseVertCount = baseMtl.Verts.GetCount();

		for(int targetIndicesIdx = 0; targetIndicesIdx < targetIndicesCount; targetIndicesIdx++)
		{
			//Get the rage renderer mesh for this target
			int targetIdx = gnrcMtlInfo.m_BlendTargetIndices[targetIndicesIdx];

			mshMesh* targetMesh = targetRenderMeshes[ targetIdx ];
			Vector3 targetOffset = targetOffsets[ targetIdx ];

			const mshMaterial& targetMtl = targetMesh->GetMtl(mtlIdx);
			int targetVertCount = targetMtl.Verts.GetCount();

			//Make sure the vertex counts are the same between the target and base meshes
			if(baseVertCount != targetVertCount)
			{
				result = rexResult::ERRORS;
				result.AddError("Cannot determine blendshape deltas, material (%s) vertex count differs between base mesh (%s), and target (%s)",
					targetMtl.Name.m_String, (const char*)inputMesh->GetMeshName(), (const char*) inputMesh->m_BlendTargets[targetIdx].m_GenericMesh->GetMeshName());
				return result;
			}
			
			//Compute the deltas for this mesh
			atArray<Vector3>	posDeltas;
			atArray<Vector3>	nrmDeltas;
			atArray<Vector3>	tanDeltas;
			atArray<Vector3>	biNrmDeltas;
			atArray<Vector4>	cpvDeltas;

			posDeltas.Reserve(baseVertCount);
			nrmDeltas.Reserve(baseVertCount);
			tanDeltas.Reserve(baseVertCount);
			biNrmDeltas.Reserve(baseVertCount);
			cpvDeltas.Reserve(baseVertCount);

			for(int vIndex = 0; vIndex < baseVertCount; vIndex++)
			{
				//Position
				Vector3 posDelta = (targetMtl.Verts[vIndex].Pos - baseMtl.Verts[vIndex].Pos) - targetOffset;
				posDeltas.Push(posDelta);

				//Normal
				Vector3 nrmDelta = targetMtl.Verts[vIndex].Nrm - baseMtl.Verts[vIndex].Nrm;
				nrmDeltas.Push(nrmDelta);

				//Tangents
				Vector3 tanDelta = targetMtl.Verts[vIndex].TanBi[0].T - baseMtl.Verts[vIndex].TanBi[0].T;
				tanDeltas.Push(tanDelta);

				//BiNormal
				//Vector3 biNrmDelta = targetMtl.Verts[vIndex].TanBi[0].B - baseMtl.Verts[vIndex].TanBi[0].B;
				
				//Bi-normal deltas are not useful, so just push in the base object bi-normal
				biNrmDeltas.Push(baseMtl.Verts[vIndex].TanBi[0].B);

				//CPV
				Vector4 cpvDelta;
				cpvDelta.Subtract(targetMtl.Verts[vIndex].Cpv, baseMtl.Verts[vIndex].Cpv);
				cpvDeltas.Push(cpvDelta);
			}

			//Write out the binary .bsd delta file
			char fileNameBuffer[128];
			sprintf(fileNameBuffer, "%sdelta%d",(const char*)inputMesh->GetMeshName(), targetIdx);
			fiStream* stream = ASSET.Create( fileNameBuffer, "bsd" );
			if( stream )
			{
				//Write the ID tag
				char idTag[4] = {' ','D', 'S', 'B'};
				stream->WriteInt((const int*)&idTag[0],1);

				//Write the Version
				//v1 -> Only has pos, normal, bi-nrm, and cpv deltas
				//v2 -> Added tangent deltas.
				int version = 2;
				stream->WriteInt(&version, 1);

				//Write the flags for each vertex delta channel
				char hasPositions, hasNormals, hasTangents, hasBiNormals, hasCPV;
				hasPositions = 1;
				hasNormals = 1;
				hasTangents = 1;
				hasBiNormals = 1;
				hasCPV = 1;

				stream->WriteByte(&hasPositions, 1);
				stream->WriteByte(&hasNormals, 1);
				stream->WriteByte(&hasTangents, 1);
				stream->WriteByte(&hasBiNormals, 1);
				stream->WriteByte(&hasCPV, 1);

				//Write the vertex count
				stream->WriteInt(&baseVertCount, 1);

					//Write the position deltas
					if(hasPositions)
					{
					for(int i = 0; i<baseVertCount; i++)
					{
						stream->WriteFloat(&posDeltas[i][0], 1);
						stream->WriteFloat(&posDeltas[i][1], 1);
						stream->WriteFloat(&posDeltas[i][2], 1);						
					}
				}
				
					//Write the normal deltas
					if(hasNormals)
					{
					for(int i = 0; i<baseVertCount; i++)
					{
						stream->WriteFloat(&nrmDeltas[i][0], 1);
						stream->WriteFloat(&nrmDeltas[i][1], 1);
						stream->WriteFloat(&nrmDeltas[i][2], 1);						
					}
				}

					//Write out the tangent deltas
					if(hasTangents)
					{
					for(int i = 0; i<baseVertCount; i++)
					{
						stream->WriteFloat(&tanDeltas[i][0], 1);
						stream->WriteFloat(&tanDeltas[i][1], 1);
						stream->WriteFloat(&tanDeltas[i][2], 1);
					}
				}
		
					//Write the biNormal deltas
					if(hasBiNormals)
					{
					for(int i = 0; i<baseVertCount; i++)
					{
						stream->WriteFloat(&biNrmDeltas[i][0], 1);
						stream->WriteFloat(&biNrmDeltas[i][1], 1);
						stream->WriteFloat(&biNrmDeltas[i][2], 1);
					}
				}

					//Write the cpv deltas
					if(hasCPV)
					{
					for(int i = 0; i<baseVertCount; i++)
					{
						stream->WriteFloat(&cpvDeltas[i][0], 1);
						stream->WriteFloat(&cpvDeltas[i][1], 1);
						stream->WriteFloat(&cpvDeltas[i][2], 1);
						stream->WriteFloat(&cpvDeltas[i][3], 1);
					}
				}

				stream->Close();
			}
			else
			{
				//Uh oh.. something went wrong and we can't open the file for writing..
				result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

				// Get full path for debugging
				char acFullPathForDebugging[255];
				ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),fileNameBuffer,"bsd" );

				// Flip the slashes
				for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

				// For some crazy reason, the result of ASSET.FullPath has the extension on twice, so chop it off
				for(int s=sizeof(acFullPathForDebugging) - 1; s>1; s--) 
				{
					if(acFullPathForDebugging[s] == '.') 
					{
						acFullPathForDebugging[s] = '\0'; 
						break;
					}
				}

				// Error
				result.AddError( "Could not create file '%s.bsd'", acFullPathForDebugging);
			}

		}

	}

	//Cleanup
	int targetMeshCount = targetRenderMeshes.GetCount();
	for(int a=0; a<targetMeshCount; a++)
		if(targetRenderMeshes[a])
			delete targetRenderMeshes[a];
	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyTextureOutputPath::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetTextureOutputPath( value.toString() );	
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyMeshMaxMemorySize::SetupModuleInstance( rexModule& /*module*/, rexValue /*value*/, const atArray<rexObject*>& /*objects*/ ) const
{
	rexResult result( rexResult::WARNINGS );
	result.AddWarning("MexMemorySize property has been deprecated and is no longer in use, please remove all references to this property");
	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertySetExportRootBoneMeshesRelativeToMeshCenter::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetExportRootBoneMeshesRelativeToMeshCenter( value.toBool( ser->GetExportRootBoneMeshesRelativeToMeshCenter() ) );			
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertySetExportRootBoneMeshesRelativeToPivot::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetExportRootBoneMeshesRelativeToPivot( value.toBool( ser->GetExportRootBoneMeshesRelativeToPivot() ) );			
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyMeshSetExportTextures::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetExportTextures( value.toBool( ser->GetExportTextures() ) );			
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyMeshSetExportEnityTextureList::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetExportEntityTextureList( value.toBool( ser->GetExportEntityTextureList() ) );			
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyMeshEntityTextureListKeepExtensions::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetExportEntityTextureListKeepExtenstions( value.toBool( ser->GetExportEntityTextureListKeepExtenstions() ) );			
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyDisableTriStripping::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetDisableTriStripping( value.toBool( ser->GetDisableTriStripping() ) );			
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertySetMaxMatricesPerMaterial::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetMaxMatricesPerMaterial( value.toInt( ser->GetMaxMatricesPerMaterial() ) );			
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyUseNonOptimizedMTLCompareMesh::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetUseNonOptimizedMTLCompare( value.toBool( ser->GetUseNonOptimizedMTLCompare() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyExpectedVertexCount::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetExpectedVertexCount( value.toString() );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

int	rexSerializerRAGEMesh::GetExpectedVertexCount(const atString &meshName) const
{ 
	atArray<atString> parts;
	atString localvertCount = m_ExpectedVertexCount;
	localvertCount.Split(parts, "|");
	for(atArray<atString>::iterator it = parts.begin();
		it != parts.end();
		it++)
	{
		atArray<atString> keyValue;
		(*it).Split(keyValue, ':', true);
		if(keyValue.GetCount()>0 && keyValue[0]==meshName)
			return atoi(keyValue[1].c_str());
	}
	return -1; 
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshRemoveDegenerates::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetRemoveDegenerates( value.toBool( ser->GetRemoveDegenerates() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshWeldWithoutQuantize::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetWeldWithoutQuantize( value.toBool( ser->GetWeldWithoutQuantize() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshWeld::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetWeld( value.toBool( ser->GetWeld() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshSplitBlendsIntoUniqueMeshes::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetSplitBlendsIntoUniqueMeshes( value.toBool( ser->GetSplitBlendsIntoUniqueMeshes() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyMeshMicroMorphs::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEMesh* ser = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );

	if( ser )
	{
		ser->SetMicroMorphs( value.toBool( ser->GetMicroMorphs() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} // namespace rage
