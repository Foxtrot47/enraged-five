//
// rexRAGE/serializerEntity.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "serializerEntity.h"

#include "serializerNurbsCurve.h"

#include "grmodel/grmodel_config.h"

#if USE_MTL_SYSTEM
#include "rageShaderMaterial/rageShaderMaterialParamTexture.h"
#include "rageShaderMaterial/rageShaderMaterial.h"
#include "rageShaderMaterial/rageShaderMaterialGroup.h"
#endif

#include "rexBase/module.h"
#include "rexBase/object.h"
#include "rexBase/result.h"
#include "rexBase/utility.h"
#include "rexBase/shell.h"
#include "rexGeneric/objectAnimation.h"
#include "rexGeneric/objectBound.h"
#include "rexGeneric/objectEdgeModel.h"
#include "rexGeneric/objectEntity.h"
#include "rexGeneric/objectLight.h"
#include "rexGeneric/objectLocator.h"
#include "rexGeneric/objectMesh.h"
#include "rexGeneric/objectNurbsCurve.h"
#include "rexGeneric/objectSkeleton.h"
#include "rexGeneric/objectCloth.h"
#include "rexGeneric/objectHair.h"
#include "rexGeneric/objectLevel.h"
#include "rexGeneric/objectPrimitive.h"
#include "MaterialPresets/MaterialPresetManager.h"

#if USE_MTL_SYSTEM
//#include "shaderMaterial/shaderMaterialGeoParamValueBool.h"
#include "shaderMaterial/shaderMaterialGeoParamValueFloat.h"
//#include "shaderMaterial/shaderMaterialGeoParamValueInt.h"
#include "shaderMaterial/shaderMaterialGeoParamValueMatrix34.h"
#include "shaderMaterial/shaderMaterialGeoParamValueMatrix44.h"
#include "shaderMaterial/shaderMaterialGeoParamValueTexture.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector2.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector3.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector4.h"
#include "shaderMaterial/shaderMaterialGeoParamValueString.h"
#endif

#include "file/stream.h"
#include "file/asset.h"

#include <map>
#include <string>
#include <sstream>

using namespace rage;
using namespace std;


enum eOptimizeTroolean
{
	OPTIMIZE_FALSE,
	OPTIMIZE_TRUE,
	OPTIMIZE_NOWRITE
};

enum eConsiderTroolean
{
	CONSIDER_ALL,
	CONSIDER_IFNOT,
	CONSIDER_ONLY
};

enum eModelIndex
{
	eIndexMeshBone,
	eIndexMeshInGroup,
	eIndexModelInMesh
};


///////////////////////////////////////////////////////////////////////////////////////////
// Static members
///////////////////////////////////////////////////////////////////////////////////////////

atString rexSerializerRAGEEntity::m_BoundListName = atString("bound");


///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEEntity::Serialize( rexObject& object ) const
{	

	rexResult result( rexResult::PERFECT );

	// type checking
	const rexObjectGenericEntity* inputEntity = dynamic_cast<const rexObjectGenericEntity*>( &object );
	if( inputEntity == NULL )
	{
		return result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	ASSET.SetPath( GetOutputPath() );

	// write to our stream	
	rexUtility::CreatePath( GetOutputPath() );
	fiStream* myStream = GetExportData() ? ASSET.Create( inputEntity->m_EntityTypeFileName.GetLength() ? inputEntity->m_EntityTypeFileName : inputEntity->GetName(), "type" ) : NULL;
	m_SkipBoundSectionOfTypeFile = inputEntity->m_SkipBoundSectionOfTypeFile;

	if( GetExportData() && !myStream )
	{
		// Oh dear, something bad has happened
		result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),(inputEntity->m_EntityTypeFileName.GetLength() ? (const char*)inputEntity->m_EntityTypeFileName : (const char*)inputEntity->GetName()),"type" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		result.AddError( "Could not create file '%s'", acFullPathForDebugging);
	}
	else 
	{		
		if( myStream )
		{
			atString verString( "Version: 103\r\n" );
			myStream->Write( verString, verString.GetLength() );

			if(inputEntity->m_EntityClass.GetLength())
			{
				char buffer[255];
				
				sprintf(buffer, "entityclass {\r\n");
				myStream->Write(buffer, (int)strlen(buffer));
				sprintf(buffer, "\tclass %s\r\n", (const char*)inputEntity->m_EntityClass);
				myStream->Write(buffer, (int)strlen(buffer));
				sprintf(buffer, "}\r\n");
				myStream->Write(buffer, (int)strlen(buffer));
			}
		}

		result &= SerializeRecursive( object, myStream );	
	}	
	
	if( myStream )
		myStream->Close();
	if(rexShell::DoesntSerialise())
	{
		const char *filename = myStream->GetName();
		result.AddMessage("Deleting file %s due to no-serialse flag.", filename);
		remove(filename);
	}

	return result;
}


rexResult rexSerializerRAGEEntity::SerializeRecursive( rexObject& object, fiStream* myStream ) const
{
	rexResult result( rexResult::PERFECT );

	if( myStream )
	{
		int childCount = object.m_ContainedObjects.GetCount();
		for( int a = 0; a < childCount; a++ )
		{
			result.Combine(WriteObjectInformation( *object.m_ContainedObjects[ a ], myStream ));
			if(result.Errors())
			{
				return result;
			}
		}
	}		

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
static void SaveShaderData(const char *path, const char *filename, rexObjectGenericMesh::MaterialInfo& matInfo)
{
	atString file(path);
	file += filename;
	fiStream *s = fiStream::Create(file);
	const int size = 256;
	char buff[size];
	if ( s ) 
	{
		const int count = matInfo.m_AttributeNames.GetCount();
		for (int i = 0; i < count; ++i) {
			// Only send down variables that are really set
			if ( matInfo.m_AttributeNames[i] != "" ) {
				formatf(buff, size, "%s {\r\n\t", (const char *)matInfo.m_AttributeNames[i]);
				s->Write(buff, (int)strlen(buff));
				formatf(buff, size, "%s %s\r\n}\r\n\r\n", (const char *)matInfo.m_RageTypes[i], matInfo.m_AttributeValues[i].toString() );
				s->Write(buff, (int)strlen(buff));
			}
		}
		s->Close();
	}
}

static void SavePresetData(const char *path, const char *filename, rexObjectGenericMesh::MaterialInfo& matInfo)
{
	atString file(path);
	file += filename;

	matInfo.m_MaterialPreset.SetFriendlyName(filename);
	MaterialPresetManager::SavePreset(file, matInfo.m_MaterialPreset);
}

static void Indent(char* buffer, int depth)
{
	memmove(buffer + depth, buffer, (int)strlen(buffer) + 1);

	for (int indent = 0; indent < depth; ++indent)
	{
		buffer[indent] = '\t';
	}
}

static void WriteBlendShapes(const atArray< atArray< atArray< rexObjectGenericMesh*> > >& lodGroups, fiStream* s)
{
//	int lodLevels = Min( lodGroups.GetCount(), MAX_LOD );
	int lodGroupCount = lodGroups.GetCount();
	if( !lodGroupCount )
		return;

	//Blendshapes are only specified for LOD 0 meshes, so look through LOD 0
	//for any valid meshes that contain blendshape information
	int validBlendShapeMeshCount = 0;
	for( int a = 0; a < lodGroupCount; a++ )
	{
		int meshCount = lodGroups[ a ][ 0 ].GetCount();
		for( int b = 0; b < meshCount; b++ )
		{
			const rexObjectGenericMesh *mesh = lodGroups[ a ][ 0 ][ b ];
			if( mesh )
			{
				if(mesh->m_IsValid && mesh->m_BlendTargets.GetCount())
					validBlendShapeMeshCount++;
			}
		}
	}
	//There aren't any, so there is nothing to write out...
	if(!validBlendShapeMeshCount)
		return;

	//Write out the blendshapes section into the entity file
	char buffer[512];
	sprintf( buffer, "blendshapes {\r\n" );
	s->Write( buffer, (int)strlen( buffer ) );

	for( int a = 0; a < lodGroupCount; a++ )
	{
		int meshCount = lodGroups[ a ][ 0 ].GetCount();
		for( int meshIdx = 0 ; meshIdx < meshCount; meshIdx++ )
		{
			const rexObjectGenericMesh *mesh = lodGroups[ a ][ 0 ][ meshIdx ];

			if( mesh && mesh->m_IsValid && mesh->m_BlendTargets.GetCount() )
			{
				sprintf( buffer, "\tblendshape { \r\n");
				s->Write( buffer, (int)strlen( buffer ) );

				sprintf( buffer, "\t\t%s.mesh 0 %d\r\n", (const char*)mesh->GetMeshName(), meshIdx );
				s->Write( buffer, (int)strlen( buffer ) );

				sprintf(buffer, "\t}\r\n");
				s->Write( buffer, (int)strlen( buffer ) );
			}

		}//End for( int meshIdx = 0
	}
	sprintf( buffer,"}\r\n");
	s->Write( buffer, (int)strlen( buffer ) );
}

// GunnarD: bit bastardised to only use last flag as name.
inline string BuildFlagGroupName( rexObjectGenericMesh* pMesh )
{
	string flagGroupName = "lodgroup";//"mesh";
	int nFlags = pMesh->m_SpecialFlags.GetCount();
	
	if(nFlags)
	{
		for(int i=0; i<nFlags; i++)
		{
			char flagName[256];
			
			//Remove any quotes or whitespace from the flag name
			int nameLen = pMesh->m_SpecialFlags[i].GetLength();
			const char* strPtr = (const char*)pMesh->m_SpecialFlags[i];
			int k=0;
			for(int j=0; j<nameLen; j++)
			{
				if( strPtr[j] != '\"' &&
					strPtr[j] != ' ')
				{
					flagName[k++] = strPtr[j];
				}
			}
			flagName[k] = '\0';

			flagGroupName = flagName;
		}
	}

	return flagGroupName;
}

void HELPER_WriteLODThreshold(const atArray<atArray<rexObjectGenericMesh::sThresholdPair>>& thresholds, int a, int indent, float conversionFactor, fiStream* s, bool &useTwoValues, const char * meshString="")
{
	char buffer[1024];
	float threshold1 = 9998.0f, threshold2 = 9999.0f;
	if( thresholds.GetCount() > a )
	{
		const atArray<rexObjectGenericMesh::sThresholdPair> &curLevelLodThresh = thresholds[a];
		// thresholds are defined only in first model per lod level!
		if(curLevelLodThresh.size()>0)
		{
			rexObjectGenericMesh::sThresholdPair inOutPair = curLevelLodThresh[0];
			if(useTwoValues)
			{
				if(inOutPair.lodIn>=0.0f)
					threshold1 = inOutPair.lodIn * conversionFactor;
				if(inOutPair.lodOut>=0.0f)
					threshold2 =inOutPair.lodOut * conversionFactor;
			}
			else
			{
				if(inOutPair.lodOut>=0.0f)
					threshold1 = inOutPair.lodOut * conversionFactor;
			}
		}
	}

	if(useTwoValues)
	{
		if(strcmp(meshString, ""))
		{
			sprintf( buffer, "%s none \r\n", meshString);
			Indent(buffer, indent+2);
			s->Write( buffer, (int)strlen( buffer ) );
			sprintf( buffer, "%f\r\n", threshold1 );
			Indent(buffer, indent+3);
			s->Write( buffer, (int)strlen( buffer ) );
			sprintf( buffer, "%f\r\n", threshold2 );
			Indent(buffer, indent+3);
			s->Write( buffer, (int)strlen( buffer ) );
		}
		else
		{
			sprintf( buffer, "%f\r\n", threshold1);
			Indent(buffer, indent+3);
			s->Write( buffer, (int)strlen( buffer ) );
			sprintf( buffer, "%f\r\n", threshold2);
			Indent(buffer, indent+3);
			s->Write( buffer, (int)strlen( buffer ) );
		}
	}
	else
	{
		if(strcmp(meshString, ""))
			sprintf( buffer, "\t\t%s none %f \r\n", meshString,  threshold1 );
		else
			sprintf( buffer, "\t\t\t%f\r\n",  threshold1);
		Indent(buffer, indent);
		s->Write( buffer, (int)strlen( buffer ) );
	}
}

static rexResult WriteLODGroupMeshSection(atArray< atArray<rexObjectGenericMesh*> >& lodGroups, 
									 const char* sectionName, 
									 atArray< atArray<rexObjectGenericMesh::sThresholdPair > >& thresholds, 
									 fiStream* s, 
									 const char *typeGroupName,
									 int indent = 0, 
									 eModelIndex writeMeshGroupIndex = eIndexMeshBone, 
									 eOptimizeTroolean optimize=OPTIMIZE_NOWRITE, 
									 bool isClothGroup = false
									 )
{
	rexResult result( rexResult::PERFECT );

	char buffer[1024];

	atArray<Vector3,0,unsigned> verts( 0,1000 ); 

	float conversionFactor=rexSerializer::GetUnitTypeLinearConversionFactor();

	int lodLevels = Min( lodGroups.GetCount(), MAX_LOD );
	int a;
	int validMeshCount = 0;
	for( a = 0; a < lodLevels; a++ )
	{
		int meshCount = lodGroups[ a ].GetCount();
		for( int b = 0; b < meshCount; b++ )
		{
			const rexObjectGenericMesh *mesh = lodGroups[ a ][ b ];
			if( mesh && mesh->m_IsValid && mesh->ExclusiveTypeGroupsContain(atString(typeGroupName)) )
			{
				validMeshCount++;
			}
		}
	}
	if(!validMeshCount)
	{
		char msg[4096];
		sprintf(msg, "Skipping mesh group section %s in %s with no valid meshes.", sectionName, typeGroupName);
		result.AddMessage(msg);
		return result;
	}

	sprintf( buffer, "%s {\r\n", sectionName );
	Indent(buffer, indent+1);
	s->Write( buffer, (int)strlen( buffer ) );

	// first mesh of highest lod defined fragment offset
	if(isClothGroup)
	{
		Vector3 vec(0,0,0);
		for(atArray<rexObjectGenericMesh*>::iterator it = lodGroups[0].begin();
			it != lodGroups[0].end();
			it++)
		{
			if((*it)->m_FragmentRootOffset.Mag() > 0)
				vec = (*it)->m_FragmentRootOffset;
		}
		sprintf( buffer, "offset %f %f %f\r\n", vec.x, vec.y, vec.z);
		Indent(buffer, indent+1);
		s->Write( buffer, (int)strlen( buffer ) );
	}

	// optimize feature for cloth #
	if(optimize!=OPTIMIZE_NOWRITE)
	{
		sprintf( buffer, "optimize %d \r\n", optimize==OPTIMIZE_TRUE?1:0 );
		Indent(buffer, indent+2);
		s->Write( buffer, (int)strlen( buffer ) );
	}

	static const char* lodLevelIDStrings[MAX_LOD] = 
	{
		"high",
		"med",
		"low",
		"vlow"
	};

	// We will be changing how we group LOD meshes fairly soon to be more like they are dealt with in the head so 
	// for the time being this can stay like this.
	Vector3 min(LARGE_FLOAT, LARGE_FLOAT, LARGE_FLOAT);
	Vector3 max(-LARGE_FLOAT, -LARGE_FLOAT, -LARGE_FLOAT);
	int validGroupMeshCount = 0;
	for( a = 0; a < lodLevels; a++ )
	{
		int meshCount = lodGroups[ a ].GetCount();
		int validLogLevelMeshCount = 0;
		for( int b = 0; b < meshCount; b++ )
		{
			const rexObjectGenericMesh *mesh = lodGroups[ a ][ b ];
			if( mesh && mesh->m_IsValid && mesh->ExclusiveTypeGroupsContain(atString(typeGroupName)) )
			{
				validLogLevelMeshCount++;
			}
		}
		if( validLogLevelMeshCount )
		{
			sprintf( buffer, "\t\t%s %d\r\n", lodLevelIDStrings[ a ], validLogLevelMeshCount );
			Indent(buffer, indent);
			s->Write( buffer, (int)strlen( buffer ) );

			for( int b = 0; b < meshCount; b++ )
			{
				const rexObjectGenericMesh *mesh = lodGroups[ a ][ b ];
				if( mesh && mesh->m_IsValid && mesh->ExclusiveTypeGroupsContain(atString(typeGroupName)) )
				{
					if( mesh->m_BoundBoxMin.x < min.x )
						min.x = mesh->m_BoundBoxMin.x;
					if( mesh->m_BoundBoxMax.x > max.x )
						max.x = mesh->m_BoundBoxMax.x;

					if( mesh->m_BoundBoxMin.y < min.y )
						min.y = mesh->m_BoundBoxMin.y;
					if( mesh->m_BoundBoxMax.y > max.y )
						max.y = mesh->m_BoundBoxMax.y;

					if( mesh->m_BoundBoxMin.z < min.z )
						min.z = mesh->m_BoundBoxMin.z;
					if( mesh->m_BoundBoxMax.z > max.z )
						max.z = mesh->m_BoundBoxMax.z;

					int vertCount = mesh->m_Vertices.GetCount();
					for( int c = 0; c < vertCount; c++ )
					{
						static Vector3 vert;
						mesh->m_Matrix.Transform( mesh->m_Vertices[ c ].m_Position, vert );
						verts.PushAndGrow( vert,vertCount );
					}

					sprintf( buffer, "\t\t\t%s.mesh ", (const char*)mesh->GetMeshName() );
					Indent(buffer, indent);
					s->Write( buffer, (int)strlen( buffer ) );

					//If the property is set to write out the group index, write that value
					//into the .type file, otherwise use the bone index (which is the default behavior)
					switch(writeMeshGroupIndex)
					{
					case eIndexMeshBone:
						sprintf( buffer, "%d", mesh->m_BoneIndex );
						break;
					case eIndexMeshInGroup:
						sprintf( buffer, "%d", validGroupMeshCount++ );
						break;
					case eIndexModelInMesh:

						if( mesh->m_Materials.GetCount()>1)
						{
							result.AddError("Mesh %s has more than one material The serialisation type does not support this.\n", mesh->GetMeshName().c_str());
							result.Combine(rexResult::ERRORS);
						}

						sprintf( buffer, "%d", 0 );
						break;
					}
					s->Write( buffer, (int)strlen( buffer ) );

					buffer[0] = '\0';
					if( !isClothGroup && mesh->m_RenderBucketMask != 0xFF)
						sprintf( buffer, " mask %X", mesh->m_RenderBucketMask );

					strcat_s(buffer, "\r\n");
					s->Write( buffer, (int)strlen( buffer ) );
				}
			} // meshes

			HELPER_WriteLODThreshold(thresholds, a, indent, conversionFactor, s, isClothGroup);
		}
		else
		{
			// write "none blabla"
			HELPER_WriteLODThreshold(thresholds, a, indent, conversionFactor, s, isClothGroup, lodLevelIDStrings[ a ]);
		}
	}
	for( a = lodLevels; a < MAX_LOD; a++ )
	{
		HELPER_WriteLODThreshold(thresholds, a, indent, conversionFactor, s, isClothGroup, lodLevelIDStrings[ a ]);
	}

	Vector3 center(0,0,0);
	float	radius = 0.0f;

	int vertCount = verts.GetCount();
	if( vertCount )
	{
		//Get the bounding sphere
		rexUtility::MakeOptimizedBoundingSphereFromBBox( min, max, center, radius );

		//Get the bounding AABB
		min.Scale(conversionFactor);
		max.Scale(conversionFactor);
	}

	center.Scale(conversionFactor);
	sprintf( buffer, "\t\tcenter %f %f %f\r\n", center.x, center.y, center.z );
	Indent(buffer, indent);
	s->Write( buffer, (int)strlen( buffer ) );

	sprintf( buffer, "\t\tradius %f\r\n", radius*conversionFactor );
	Indent(buffer, indent);
	s->Write( buffer, (int)strlen( buffer ) );

	sprintf( buffer, "\t\tbox %f %f %f %f %f %f\r\n", min.x, min.y, min.z, max.x, max.y, max.z );
	Indent(buffer, indent);
	s->Write( buffer, (int)strlen( buffer ) );

	sprintf( buffer, "\t}\r\n" );
	Indent(buffer, indent);
	s->Write( buffer, (int)strlen( buffer ) );

	return result;
}

static rexResult WriteMeshGroups(atArray< atArray< atArray< rexObjectGenericMesh*> > >& lodGroups,
								 fiStream* s,
								 const char *typeGroupName,
								 int indent = 0,
								 char *boneString = NULL,
								 eModelIndex writeMeshGroupIndex = eIndexMeshBone
								 )
{
	rexResult result( rexResult::PERFECT );

	char buffer[1024];

	int lodGroupCount = lodGroups.GetCount();

	if( !lodGroupCount )
	{
		sprintf( buffer, typeGroupName);
		strcat( buffer, " none\r\n" );
		Indent(buffer, indent);
		s->Write( buffer, (int)strlen( buffer ) );
		result.AddMessageCtx(typeGroupName, "No meshes in type group %s", typeGroupName);
		return result;
	}

	sprintf( buffer, typeGroupName);
	strcat( buffer, " {\r\n" );
	Indent(buffer, indent+1);
	s->Write( buffer, (int)strlen( buffer ) );

	int validGroupMeshCount = 0;
	for( int a = 0; a < lodGroupCount; a++ )
	{
		int lodLevels = Min( lodGroups[ a ].GetCount(), MAX_LOD );
		for( int b = 0; b < lodLevels; b++ )
		{
			int meshCount = lodGroups[ a ][ b ].GetCount();
			for( int c = 0; c < meshCount; c++ )
			{
				rexObjectGenericMesh *mesh = lodGroups[ a ][ b ][ c ];
				if( mesh && mesh->m_IsValid && mesh->ExclusiveTypeGroupsContain(atString(typeGroupName)) )
				{
					sprintf( buffer, "%s.mesh ", (const char*)mesh->GetMeshName() );
					Indent(buffer, indent+2);
					s->Write( buffer, (int)strlen( buffer ) );

					//If the property is set to write out the group index, write that value
					//into the .type file, otherwise use the bone index (which is the default behavior)
					switch(writeMeshGroupIndex)
					{
					case eIndexMeshBone:
						sprintf( buffer, "%d\r\n", mesh->m_BoneIndex );
						break;
					case eIndexMeshInGroup:
						sprintf( buffer, "%d\r\n", validGroupMeshCount++ );
						break;
					case eIndexModelInMesh:
						sprintf( buffer, "%d\r\n", 0 );
						break;
					}
					s->Write( buffer, (int)strlen( buffer ) );
				}
			}
		}
	}

	if(NULL!=boneString)
	{
		Indent(boneString, indent+2);
		s->Write( boneString, (int)strlen( boneString ) );
	}

	sprintf( buffer, "}\r\n" );
	Indent(buffer, indent+1);
	s->Write( buffer, (int)strlen( buffer ) );

	return result;
}

static void WriteComment(fiStream* s, const char* fmt, ...)
{
	char buffer[1024] = {0};
	buffer[0] = ';'; // the comment char in the rage token system.
	va_list args;
	va_start(args,fmt);
	sprintf( &buffer[1], fmt,args);
	va_end(args);
	strcat(buffer, "\n");
	s->Write( buffer, (int)strlen( buffer ) );
}

static int GetNumValidMeshes(atArray< atArray< atArray< rexObjectGenericMesh*> > >& lodGroups, const char *typeGroupName, bool bSplitLODGroupsByFlags)
{
	//Get the number of "valid" meshes
	int lodGroupCount = lodGroups.GetCount();
	int totalValidMeshCount = 0;
	for( int a = 0; a < lodGroupCount; a++ )
	{
		int lodLevels = Min( lodGroups[ a ].GetCount(), MAX_LOD );
		for( int b = 0; b < lodLevels; b++ )
		{
			int meshCount = lodGroups[ a ][ b ].GetCount();
			for( int c = 0; c < meshCount; c++ )
			{
				rexObjectGenericMesh *mesh = lodGroups[ a ][ b ][ c ];

				if(  mesh && mesh->m_IsValid && 
					(mesh->ExclusiveTypeGroupsContain(atString(typeGroupName)) || bSplitLODGroupsByFlags) 
					)
				{
					totalValidMeshCount++;
				}
			}
		}
	}
	return totalValidMeshCount;
}

static rexResult WriteLODGroups(atArray< atArray< atArray< rexObjectGenericMesh*> > >& lodGroups, 
						   atArray< atArray< atArray< rexObjectGenericMesh::sThresholdPair > > >& thresholds, 
						   fiStream* s,
						   const char *typeGroupName,
						   int indent = 0,
						   eModelIndex writeMeshGroupIndex = eIndexMeshBone, 
						   bool bSplitLODGroupsByFlags = false, 
						   eOptimizeTroolean optimize=OPTIMIZE_TRUE,
						   bool splitFlagGourpsToDrawables=false,
						   char *boneString = NULL,
						   bool allowEmptyFragSection = false
						   )
{
	rexResult result( rexResult::PERFECT );

	char buffer[1024];

	int lodGroupCount = lodGroups.GetCount();

	if( !lodGroupCount )
	{
		sprintf( buffer, typeGroupName);
		strcat( buffer, " none\r\n" );
		Indent(buffer, indent);
		s->Write( buffer, (int)strlen( buffer ) );
		return result;
	}
	
	int totalValidMeshCount = GetNumValidMeshes(lodGroups, typeGroupName, bSplitLODGroupsByFlags);
	
	//Split up the meshes in the lod groups by the flags assigned to them
	//  GunnarD: Flattens out the groups, too, so we can rearrange by flags
	typedef map< string, atArray< atArray<rexObjectGenericMesh*> > > MESHFLAG_LODGROUP_TABLE;
	typedef pair< string,  atArray< atArray<rexObjectGenericMesh*> > > MESHFLAG_LODGROUP_ENTRY;

	typedef map< string, atArray< atArray<rexObjectGenericMesh::sThresholdPair> > > MESHFLAG_MESHTHRESH_TABLE;
	typedef pair< string,  atArray< atArray<rexObjectGenericMesh::sThresholdPair> > > MESHFLAG_MESHTHRESH_ENTRY;

	MESHFLAG_LODGROUP_TABLE	meshFlagLODGroups;
	MESHFLAG_MESHTHRESH_TABLE	meshFlagLODThresholds;

	if(bSplitLODGroupsByFlags)
	{
		for( int a = 0; a < lodGroupCount; a++ )
		{
			int lodLevels = lodGroups[ a ].GetCount();
			for( int b = 0; b < lodLevels; b++ )
			{
				int meshCount = lodGroups[ a ][ b ].GetCount();
				for( int c = 0; c < meshCount; c++ )
				{
					rexObjectGenericMesh *mesh	= lodGroups[ a ][ b ][ c ];
					// thresholds only in first mesh.
					rexObjectGenericMesh::sThresholdPair &threshold	= thresholds[ a ][ b ][ 0 ];

					if( mesh && mesh->m_IsValid )
					{
						string flagGroupName = BuildFlagGroupName(mesh);

						MESHFLAG_LODGROUP_TABLE::iterator it = meshFlagLODGroups.find(flagGroupName);
						if( it != meshFlagLODGroups.end() )
						{
							//An entry exists for this flag group
							atArray< rexObjectGenericMesh*>& lodMeshArray = it->second[b];
							lodMeshArray.PushAndGrow(mesh);
						}
						else
						{
							atArray< atArray< rexObjectGenericMesh*> > tmpArr;
							tmpArr.Resize(lodLevels);
							tmpArr[b].PushAndGrow(mesh);

 							meshFlagLODGroups.insert( MESHFLAG_LODGROUP_ENTRY( flagGroupName, tmpArr));
// 							MESHFLAG_LODGROUP_TABLE::iterator it = insertRes.first;
// 							atArray<rexObjectGenericMesh*>& lodMeshArray = it->second[b];
// 							lodMeshArray.PushAndGrow(mesh);
						}

						MESHFLAG_MESHTHRESH_TABLE::iterator it2 = meshFlagLODThresholds.find(flagGroupName);
						if( it2 != meshFlagLODThresholds.end() )
						{
							//An entry exists for this flag group
							atArray<rexObjectGenericMesh::sThresholdPair>& lodThreshArray = it2->second[b];
							lodThreshArray.PushAndGrow(threshold);
						}
						else
						{
							atArray< atArray<rexObjectGenericMesh::sThresholdPair> > tmpArr;
							tmpArr.Resize(lodLevels);
							tmpArr[b].PushAndGrow(threshold);
							meshFlagLODThresholds.insert( MESHFLAG_MESHTHRESH_ENTRY( flagGroupName, tmpArr));
						}
					}
				}
			}
		}
	}

	int writtenEntityLodGroups = 0;
	if (totalValidMeshCount > 0)
	{
		if(!bSplitLODGroupsByFlags || !splitFlagGourpsToDrawables)
		{
			// extra handling of fragment group mesh serialisation
			char outName[2048] = {0};
			if(0==strcmp("fragments", typeGroupName))
				strcpy(outName, "lodgroup\0");
			else
				strcpy(outName, typeGroupName);

			sprintf( buffer, outName);
			strcat( buffer, " {\r\n" );
			Indent(buffer, indent);
			s->Write( buffer, (int)strlen( buffer ) );

			writtenEntityLodGroups++;
		}

		bool useTwoValuesForThesholds = (0==strcmp(typeGroupName, "ClothPiece"));

		if(!bSplitLODGroupsByFlags)
		{
			for( int lodGroup = 0; lodGroup < lodGroupCount; lodGroup++ )
			{
				// Split the meshes into damage and non-damaged meshes.
				atArray< atArray<rexObjectGenericMesh*> > lodLevels; //= lodGroups[a];
				atArray< atArray<rexObjectGenericMesh*> > damageLevels;

				lodLevels.Resize(lodGroups[lodGroup].GetCount());
				damageLevels.Resize(lodGroups[lodGroup].GetCount());

				for ( int lodLevel = 0; lodLevel < lodGroups[lodGroup].GetCount(); lodLevel++)
				{
					for ( int lod = 0; lod < lodGroups[lodGroup][lodLevel].GetCount(); lod++)
					{
						rexObjectGenericMesh* mesh;
						mesh = lodGroups[lodGroup][lodLevel][lod];

						if (mesh && mesh->IsValidMesh() && mesh->m_DamageState)
						{
							damageLevels[lodLevel].PushAndGrow(mesh);
						}
						else
						{
							lodLevels[lodLevel].PushAndGrow(mesh);
						}
					}
				}

				if (lodLevels.GetCount() > 0)
				{
					rexResult res = WriteLODGroupMeshSection(lodLevels, "mesh", thresholds[lodGroup], s, typeGroupName, indent, writeMeshGroupIndex, optimize, useTwoValuesForThesholds);
					result.Combine(res);
				}

				if (damageLevels.GetCount() > 0)
				{
					rexResult res = WriteLODGroupMeshSection(damageLevels, "mesh|damaged", thresholds[lodGroup], s, typeGroupName, indent, writeMeshGroupIndex, optimize, useTwoValuesForThesholds);
					result.Combine(res);
				}
			}
		}
		else
		{
			WriteComment(s, "lodgroups ignored in favour to filtering by \"extraFlags\"");
			bool firstGroupWritten = false;
			const char *actualTypeGroupName = typeGroupName;
			for(MESHFLAG_LODGROUP_TABLE::iterator it = meshFlagLODGroups.begin();
				it != meshFlagLODGroups.end();
				++it)
			{
				/************************************************************************/
				/* GunnarD: Note the type group name that gets put into the section serialisation changes, too.
				/************************************************************************/ 
				if(splitFlagGourpsToDrawables)
				{
					if(firstGroupWritten)
					{
						// end current one
						sprintf( buffer, "}\r\n" );
						Indent(buffer, indent);
						s->Write( buffer, (int)strlen( buffer ) );
					}
					// begin new one
					WriteComment(s, "multiple lodgroup sections for extra drawables in fragment hierarchies.");
					actualTypeGroupName = it->first.c_str();
					sprintf( buffer, actualTypeGroupName );
					strcat( buffer, " {\r\n" );
					Indent(buffer, indent);
					s->Write( buffer, (int)strlen( buffer ) );
					firstGroupWritten = true;

					writtenEntityLodGroups++;
				}
				atArray< atArray<rexObjectGenericMesh*> >& flagLodGroup = it->second;
				atArray< atArray<rexObjectGenericMesh::sThresholdPair> > &threshs = meshFlagLODThresholds[it->first];
				rexResult res = WriteLODGroupMeshSection( flagLodGroup, "mesh", threshs, s, actualTypeGroupName, indent, writeMeshGroupIndex, optimize, useTwoValuesForThesholds);
				result.Combine(res);
			}
		}

		if(NULL!=boneString)
		{
			Indent(boneString, indent+1);
			s->Write( boneString, (int)strlen( boneString ) );
		}

		sprintf( buffer, "}\r\n" );
		Indent(buffer, indent);
		s->Write( buffer, (int)strlen( buffer ) );
	}

	if(0==writtenEntityLodGroups && !allowEmptyFragSection)
	{
		result.Combine(rexResult::ERRORS);
		result.AddError("No lod groups written to log group section %s! Please send file %s to your tools department.", typeGroupName, s->GetName());
	}

	return result;
}

void rexSerializerRAGEEntity::WriteWorldMeshData(rexObjectGenericMesh *mesh, const char* levelType, fiStream* fs)
{
	char buffer[512];

	//Get the necessary information about the room volume mesh
	Matrix34 bbMatrix;	bbMatrix.Identity();
	float	 bbLength = 0.0f, bbHeight = 0.0f, bbWidth = 0.0f;
	Vector3	 bsCenter( 0, 0, 0 );
	float	 bsRadius = 0.0f;
	mesh->MakeOptimizedBoundingBox( bbMatrix, bbLength, bbHeight, bbWidth );
	mesh->MakeOptimizedBoundingSphere( bsCenter, bsRadius );

	sprintf(buffer,"\t%s %s\r\n\t{\r\n", levelType, (const char*)mesh->GetMeshName());
	fs->Write(buffer, (int)strlen(buffer));
	
	sprintf( buffer, "\t\tMatrix\r\n" );
	fs->Write( buffer, (int)strlen( buffer ) );

	sprintf( buffer, "\t\t{\r\n" );	
	fs->Write( buffer, (int)strlen( buffer ) );

	Matrix34 m( M34_IDENTITY );
#if HACK_GTA4
	if( mesh->m_BoneIndex )
		m = mesh->m_Matrix;
	else
		m.d = mesh->m_Center;
#else
	m = mesh->m_Matrix;   //get the rotations from here
	m.d = mesh->m_Center; //get the translate from here
#endif // HACK_GTA4


	sprintf( buffer, "\t\t\t%f %f %f 0.0\r\n", m.a.x, m.a.y, m.a.z );
	fs->Write( buffer, (int)strlen( buffer ) );
	
	sprintf( buffer, "\t\t\t%f %f %f 0.0\r\n", m.b.x, m.b.y, m.b.z );
	fs->Write( buffer, (int)strlen( buffer ) );
	
	sprintf( buffer, "\t\t\t%f %f %f 0.0\r\n", m.c.x, m.c.y, m.c.z );
	fs->Write( buffer, (int)strlen( buffer ) );
	
	sprintf( buffer, "\t\t\t%f %f %f 1.0\r\n", m.d.x, m.d.y, m.d.z );
	fs->Write( buffer, (int)strlen( buffer ) );

	
	sprintf( buffer, "\t\t}\r\n" );
	fs->Write( buffer, (int)strlen( buffer ) );

	sprintf(buffer,"\t\tmesh %s.mesh\r\n", (const char*)mesh->GetMeshName());
	fs->Write(buffer, (int)strlen(buffer));

	sprintf( buffer, "\t\tboundingBox   \t{ %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f }\r\n", bbMatrix.a.x, bbMatrix.a.y, bbMatrix.a.z, bbLength * 0.5f, bbMatrix.b.x, bbMatrix.b.y, bbMatrix.b.z, bbHeight * 0.5f, bbMatrix.c.x, bbMatrix.c.y, bbMatrix.c.z, bbWidth * 0.5f, bbMatrix.d.x, bbMatrix.d.y, bbMatrix.d.z, 1.0f );
	fs->Write( buffer, (int)strlen( buffer ) );

	sprintf( buffer, "\t\tboundingSphere\t{ %f %f %f %f }\r\n", bsCenter.x, bsCenter.y, bsCenter.z, bsRadius );
	fs->Write( buffer, (int)strlen( buffer ) );

	//BEGIN TEMPORARY CODE
	if(strcmpi(levelType, "RoomVolume") == 0)
	{
		//Write out any flags that have been assigned to the volume
		if(mesh->m_SpecialFlags.GetCount())
		{
			stringstream flagStream;

			flagStream << "\t\tflags { ";
			int flagIdx;

			for(flagIdx = 0; flagIdx < mesh->m_SpecialFlags.GetCount()-1; flagIdx++)
			{
				flagStream << (const char*)mesh->m_SpecialFlags[flagIdx] << " ";
			}
			flagStream << (const char*)mesh->m_SpecialFlags[flagIdx] << " }\r\n";

			sprintf(buffer, "%s", flagStream.str().c_str());
			fs->Write(buffer, (int)strlen(buffer));
		}
		/*else
		{
			sprintf(buffer, "\t\tflags\r\n");
			fs->Write( buffer, (int)strlen(buffer));
		}*/

		sprintf(buffer, "\t\tmembers {\r\n");
		fs->Write( buffer, (int)strlen( buffer ) );

		int lodGroupCount = m_MeshesByLODGroupAndLevel.GetCount();
		for( int a = 0; a < lodGroupCount; a++ )
		{
			int lodLevels = m_MeshesByLODGroupAndLevel[ a ].GetCount();
			for( int b = 0; b < lodLevels; b++ )
			{
				int meshCount = m_MeshesByLODGroupAndLevel[ a ][ b ].GetCount();
				for( int c = 0; c < meshCount; c++ )
				{
					const rexObjectGenericMesh *pMesh = m_MeshesByLODGroupAndLevel[ a ][ b ][ c ];
					if(pMesh && pMesh->IsValidMesh())
					{
//						if(pMesh->m_GroupIndex == mesh->m_GroupIndex) // GunnarD:Retired
						{
							sprintf(buffer, "\t\t\t%s.mesh\r\n", (const char*)pMesh->GetMeshName());
							fs->Write( buffer, (int)strlen( buffer ) );
						}
					}
				}
			}
		}

		sprintf(buffer,"\t\t}\r\n");
		fs->Write( buffer, (int)strlen( buffer ) );
	}
	//END TEMPORARY CODE

	sprintf(buffer,"\t}\r\n");
	fs->Write(buffer, (int)strlen(buffer));
}

rexResult rexSerializerRAGEEntity::WriteRoomVolume(rexObject& object, fiStream* fs)
{
	const rexObjectGenericMeshHierarchyRoomVolumes* inputRoomVolHier = dynamic_cast<const rexObjectGenericMeshHierarchyRoomVolumes*>( &object );

	rexResult retval( rexResult::PERFECT );

	if( inputRoomVolHier )
	{
		int childCount = object.m_ContainedObjects.GetCount();
		for( int a = 0; a < childCount; a++ )
		{
			retval &= WriteRoomVolume( *object.m_ContainedObjects[ a ], fs );
		}
	}
	else
	{
		rexObjectGenericMesh *mesh = dynamic_cast<rexObjectGenericMesh*>( &object );
		if( mesh && mesh->m_IsValid )
		{
			WriteWorldMeshData(mesh, "RoomVolume", fs);
		}
		else
		{
			retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}
	}
	return retval;
}

rexResult rexSerializerRAGEEntity::WriteLevelRoomVolumes( rexObject& object, fiStream* /*s*/)
{
	char buffer[512];

	rexResult retval( rexResult::PERFECT );

	if(rexShell::DoesntSerialise())
		return retval;

	rexObjectGenericMeshHierarchyRoomVolumes* inputRoomVolHier = dynamic_cast<rexObjectGenericMeshHierarchyRoomVolumes*>( &object );
	
	if(inputRoomVolHier)
	{
		fiStream* fs = ASSET.Create( "level", "roomvolumes" );
		if( !fs )
		{
			// Oh dear, something bad has happened
			retval.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

			// Get full path for debugging
			char acFullPathForDebugging[255];
			ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"level","roomvolumes" );

			// Flip the slashes
			for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

			// Error
			retval.AddError( "Could not create file '%s'", acFullPathForDebugging);
			return retval;
		}
		else 
		{		
			sprintf(buffer,"RoomVolumes\r\n{\r\n");
			fs->Write(buffer,(int)strlen(buffer));

			retval.Combine( WriteRoomVolume(*inputRoomVolHier, fs) );

			sprintf(buffer,"}\r\n");
			fs->Write(buffer,(int)strlen(buffer));

			fs->Close();
			return retval;
		}
	}
	else
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

rexResult rexSerializerRAGEEntity::WritePortal(rexObject& object, fiStream* fs)
{
	const rexObjectGenericMeshHierarchyPortals* inputRoomVolHier = dynamic_cast<const rexObjectGenericMeshHierarchyPortals*>( &object );

	rexResult retval( rexResult::PERFECT );

	if( inputRoomVolHier )
	{
		int childCount = object.m_ContainedObjects.GetCount();
		for( int a = 0; a < childCount; a++ )
		{
			retval &= WritePortal( *object.m_ContainedObjects[ a ], fs );
		}
	}
	else
	{
		rexObjectGenericMesh *mesh = dynamic_cast<rexObjectGenericMesh*>( &object );
		if( mesh && mesh->m_IsValid )
		{
			WriteWorldMeshData(mesh, "Portal", fs);
		}
		else
		{
			retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}
	}
	return retval;
}

rexResult rexSerializerRAGEEntity::WriteLevelPortals( rexObject& object, fiStream* /*s*/)
{
	char buffer[512];

	rexResult retval( rexResult::PERFECT );

	if(rexShell::DoesntSerialise())
		return retval;

	rexObjectGenericMeshHierarchyPortals* inputRoomVolHier = dynamic_cast<rexObjectGenericMeshHierarchyPortals*>( &object );
	
	if(inputRoomVolHier)
	{
		fiStream* fs = ASSET.Create( "level", "portals" );
		if( !fs )
		{
			// Oh dear, something bad has happened
			retval.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

			// Get full path for debugging
			char acFullPathForDebugging[255];
			ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"level","portals" );

			// Flip the slashes
			for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

			// Error
			retval.AddError( "Could not create file '%s'", acFullPathForDebugging);
			return retval;
		}
		else 
		{		
			sprintf(buffer,"Portals\r\n{\r\n");
			fs->Write(buffer,(int)strlen(buffer));

			retval.Combine( WritePortal(*inputRoomVolHier, fs) );

			sprintf(buffer,"}\r\n");
			fs->Write(buffer,(int)strlen(buffer));

			fs->Close();
			return retval;
		}
	}
	else
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

enum eWhichLodGroup
{
	GET_RENDER_LOD_GROUP,
	GET_SIMULATION_LOD_GROUP
};

rexResult rexSerializerRAGEEntity::WriteLODAndShaderGroupInfo( rexObject& object, fiStream* s)
{
	rexResult result( rexResult::PERFECT );
	if( !s )
	{
		result.Combine( rexResult::ERRORS );
		result.AddError("No file stream given to object.");
		return result;
	}

	const rexObjectGenericMeshHierarchyDrawable* inputMeshHier = dynamic_cast<const rexObjectGenericMeshHierarchyDrawable*>( &object );

	if( !inputMeshHier )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	atArray< rexObjectGenericMesh* >				nonLODMeshes;
	atArray< rexObjectGenericMesh::MaterialInfo >	matInfoArray;
#if HACK_GTA4
	inputMeshHier->GetLODsMeshesAndMaterials( m_MeshesByLODGroupAndLevel, m_ThresholdsByLODGroupAndLevel, nonLODMeshes, matInfoArray, m_UseChildrenOfLevelInstanceNodes, m_OptimiseMaterials );
#else
	inputMeshHier->GetLODsMeshesAndMaterials( m_MeshesByLODLevel, m_ThresholdsByLODGroupAndLevel, nonLODMeshes, matInfoArray, m_UseChildrenOfLevelInstanceNodes, GetUseNonOptimizedMTLCompare());
#endif // HACK_GTA4
	char buffer[512];

	// WRITE SHADER GROUPS
	int matCount = matInfoArray.GetCount();
	atMap<ConstString, int> shaderHash;

	if( m_WriteShadingGroup && matCount )
	{
		if(GetWriteSeparateShadingGroupFile())
		{
			WriteShaderGroupInfo(object, s);
		}
		else
		{
			sprintf( buffer, "shadinggroup {\r\n" ); 
			s->Write( buffer, (int)strlen( buffer ) );	
			
			//[CLEMENSP]		
			WriteShaderGroupInfo(object, s);

			sprintf( buffer, "}\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );
		}
	}
	else
	{
		sprintf( buffer, "shadinggroup none\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );			
	}
	// END WRITE SHADER GROUPS

	if( nonLODMeshes.GetCount() )
	{
		int nonLODMeshCount = nonLODMeshes.GetCount();
		atArray< atArray<rexObjectGenericMesh*> > &m_MeshesByLODLevel = m_MeshesByLODGroupAndLevel.Grow(1);
		for( int m = 0; m < nonLODMeshCount; m++ )
		{
			int lodLevel = Max( nonLODMeshes[ m ]->m_LODLevel, 0 );
			while(m_MeshesByLODLevel.size()>lodLevel)
				m_MeshesByLODLevel.Grow(1);
			m_MeshesByLODLevel[ lodLevel ].PushAndGrow( nonLODMeshes[ m ],(u16) nonLODMeshCount );
		}
	}

	PrioritizeMeshesInLODGroups();

	eModelIndex modelIndexToWriteOut = GetWriteMeshGroupIndex() ? eIndexMeshInGroup : eIndexMeshBone;
	rexResult res = WriteLODGroups(m_MeshesByLODGroupAndLevel, m_ThresholdsByLODGroupAndLevel, s, "lodgroup", 0, modelIndexToWriteOut, GetSplitLODGroupsByFlags(), inputMeshHier->m_optimize?OPTIMIZE_TRUE:OPTIMIZE_FALSE, m_SplitFlagGroupsToDrawables, NULL, m_AllowEmptyFragSection);
	result.Combine(res);

	//Write out the BlendShapes
	WriteBlendShapes(m_MeshesByLODGroupAndLevel, s);

	return result;
}

void rexSerializerRAGEEntity::PrioritizeMeshesInLODGroups()
{
	if (m_MeshesByLODGroupAndLevel.GetCount())
	{
		//for all groups
		for( int group = 0; group < m_MeshesByLODGroupAndLevel.GetCount(); group++ )
		{
			//for all lods
			atArray< atArray<rexObjectGenericMesh*> >	&meshesByLODLevel = m_MeshesByLODGroupAndLevel[group];
			for( int lod = 0; lod < meshesByLODLevel.GetCount(); lod++ )
			{
				atArray<rexObjectGenericMesh*> orderedList;
				const atArray<rexObjectGenericMesh*>& originalList = meshesByLODLevel[lod];
				//for all meshes 
				for( int msh = 0; msh < originalList.GetCount(); msh++ )
				{
					rexObjectGenericMesh* mesh = originalList[msh];
					if (mesh)
					{
						if (mesh->m_RenderPriority == -1)
						{
							//not prioritized add to the end
							orderedList.PushAndGrow(mesh);
						}
						else
						{
							if (orderedList.GetCount())
							{
								//search for the right place
								int insertIdx = -1;
								for( int omsh = 0; omsh < orderedList.GetCount(); omsh++ )
								{
									if (orderedList[omsh]->m_RenderPriority == -1)
									{
										//we need to go before any unprioritized stuff
										insertIdx = omsh;
										break;
									}
									else if (mesh->m_RenderPriority < orderedList[omsh]->m_RenderPriority)
									{
										//we need to go before any lower priority stuff
										insertIdx = omsh;
										break;
									}
								}

								if (insertIdx == -1)
								{
									//did not find a place to insert so just add to the end 
									//	all priorities are probably the same
									orderedList.PushAndGrow(mesh);
								}
								else
								{
									//insert ourselves before the index
									orderedList.Insert(insertIdx) = mesh;
								}
							}
							else
							{
								//Just add it
								orderedList.PushAndGrow(mesh);
							}
						}
					}
				}
				meshesByLODLevel[lod] = orderedList;
			}
		}
	}
}

//[CLEMENSP]
rexResult rexSerializerRAGEEntity::WriteShaderGroupInfo( rexObject& object, fiStream* s)
{
    if( !s )
        return rexResult::ERRORS;

    const rexObjectGenericMeshHierarchyDrawable* inputMeshHier = dynamic_cast<const rexObjectGenericMeshHierarchyDrawable*>( &object );

    if( !inputMeshHier )
        return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	rexResult result(rexResult::PERFECT);

    atArray< rexObjectGenericMesh* >			nonLODMeshes;
    atArray< rexObjectGenericMesh::MaterialInfo >	matInfoArray;

	m_MeshesByLODGroupAndLevel.Reset();
#if HACK_GTA4
    inputMeshHier->GetLODsMeshesAndMaterials( m_MeshesByLODGroupAndLevel, m_ThresholdsByLODGroupAndLevel, nonLODMeshes, matInfoArray, m_UseChildrenOfLevelInstanceNodes, m_OptimiseMaterials );
#else
	inputMeshHier->GetLODsMeshesAndMaterials( m_MeshesByLODLevel, m_ThresholdsByLODGroupAndLevel, nonLODMeshes, matInfoArray, m_UseChildrenOfLevelInstanceNodes, GetUseNonOptimizedMTLCompare());
#endif // HACK_GTA4
    char buffer[512];
    char svaName[256];

    int shadingGroupCount = m_TextureSets.GetCount();

    bool useTextureSets = true;
    if( shadingGroupCount == 0 )
    {
        useTextureSets = false;
        shadingGroupCount = 1;
    }

    // We gotta create a bunch of little sva files -- one for each shader, might as well do it here...
    atString entityName = rexUtility::BaseFilenameFromPath(s->GetName());
    atString entityPath = rexUtility::PathFromPath(s->GetName());
	atArray<atString> pathLeaves;
	entityPath.Split(pathLeaves, "/", true);
	if(pathLeaves.GetCount()>0)
		entityName =  pathLeaves[pathLeaves.GetCount()-1];

	atString matName = entityPath;
	matName.Truncate(entityPath.GetLength() - 1);
	matName.Set(matName, matName.GetLength() - 6, 6);
	// Convert slashes into something else
	for (s32 iPos = 0; iPos < matName.GetLength(); iPos++)
	{
		if ((matName[iPos] == '/') || (matName[iPos] == '\\'))
		{
			matName[iPos] = 'O';
		}
	}

    // WRITE SHADER GROUPS
    int matCount = matInfoArray.GetCount();
    atMap<ConstString, int> shaderHash;
    if( matCount )
    {
#if !HACK_GTA4
		//////////////////////////////////////////////////////////////////////////
		//Varify the material array -- we cant have any mixing of MTL and SPS
		atArray<int> mtl;
		atArray<int> sps;
		for( int a = 0; a < matCount; a++ )
		{
			if (matInfoArray[a].m_MtlFileName != "null")
			{
				mtl.PushAndGrow(a);
			}
			else
			{
				sps.PushAndGrow(a);
			}
		}

		if (mtl.GetCount() && sps.GetCount())
		{
			result.Set(rexResult::ERRORS);
			Errorf( "MTLs and SPSs don't mix, you seem to have both in this scene." );
			result.AddMessage("MTLs and SPSs don't mix, you seem to have both in this scene.");
			Errorf( "Here is what I have: " );
			result.AddMessage("Here is what I have: ");
			for( int a = 0; a < mtl.GetCount(); a++ )
			{
				int index = mtl[a];
				rexObjectGenericMesh::MaterialInfo& matInfo = matInfoArray[ index ];
				result.AddMessage("Node: %s MTL: %s", (const char *)matInfo.m_Name, (const char *)matInfo.m_MtlFileName);
				Errorf( "MTL: Node: %s File: %s", (const char *)matInfo.m_Name, (const char *)matInfo.m_MtlFileName );
			}
			for( int a = 0; a < sps.GetCount(); a++ )
			{
				int index = sps[a];
				rexObjectGenericMesh::MaterialInfo& matInfo = matInfoArray[ index ];
				result.AddMessage("Node: %s SPS: %s", (const char *)matInfo.m_Name, (const char *)matInfo.m_TypeName);
				Errorf( "SPS: Node: %s File: %s", (const char *)matInfo.m_Name, (const char *)matInfo.m_TypeName );
			}
		}
		//////////////////////////////////////////////////////////////////////////
#endif // !HACK_GTA4

#if USE_MTL_SYSTEM
		if (matInfoArray[0].m_MtlFileName != "null")
		{
			rageShaderMaterialGroup rageShaders;
			for( int a = 0; a < matCount; a++ )
			{
				rexObjectGenericMesh::MaterialInfo& matInfo = matInfoArray[ a ];
				
				rageShaderMaterial * mtl = matInfo.GetMaterial();
				if (!mtl) 
				{	
					rexResult result (rexResult::ERRORS );
					result.AddMessage("Can not find the RAGE shader material file %s.", (const char *)matInfo.m_MtlFileName);
					continue;
				}

				// Set the lightmap data
				for (int i=0; i<rageShaderMaterialParam::LIGHTMAP_TYPE_COUNT; i++)
				{
					ConstString lightmapFileName = matInfo.GetLightmapFileName(i);
					mtl->SetLightmapData(lightmapFileName, i);

					// Copy the lightmaps to the output directory
					char outputLightmapFileName[256];
					ASSET.FullPath(outputLightmapFileName, sizeof(outputLightmapFileName), fiAssetManager::FileName(matInfo.GetLightmapFileName(i)), "");

					fiDeviceLocal::GetInstance().CopySingleFile((const char *)matInfo.GetLightmapFileName(i), outputLightmapFileName);
				}

				rageShaders.AddMaterial((const char *)matInfo.m_MtlFileName, *mtl, true); // Force the material in because we check earlier for if the same
			}

			if(GetWriteSeparateShadingGroupFile())
			{
				atString fullShaderGroupPath = entityPath;
				fullShaderGroupPath += "\\shaders.shadergroup";
				rageShaders.WriteToShaderGroupFile(fullShaderGroupPath);
			}
			else
			{
				fiTokenizer T("entity.type", s);
				rageShaders.WriteToShaderGroupFile(T);
			}

			//Write out the MTL Dependency file
			parTree* pTree = new parTree();
			parTreeNode* pRootNode = pTree->CreateRoot();
			pTree->SetRoot(pRootNode);
			parElement &pRootElm = pRootNode->GetElement();

			pRootElm.SetName("MtlDependencies");
			pRootElm.AddAttribute("OutputPath", ASSET.GetPath());

			for( int a = 0; a < matCount; a++ )
			{
				rexObjectGenericMesh::MaterialInfo& matInfo = matInfoArray[ a ];
				rageShaderMaterial *pMtl = matInfo.GetMaterial();
				if(!pMtl)
					continue;

				if(matInfo.m_MtlFileName != "null")
				{
					parTreeNode *pMtlNode = new parTreeNode();
					parElement& mtlElm = pMtlNode->GetElement();
					mtlElm.SetName("MtlFile");
					mtlElm.AddAttribute("FilePath",(const char*)matInfo.m_MtlFileName);
					pMtlNode->AppendAsChildOf(pRootNode);

					parTreeNode *pTexturesNode = new parTreeNode();
					pTexturesNode->GetElement().SetName("Textures");
					pTexturesNode->AppendAsChildOf(pMtlNode);

					int texCount = matInfo.m_InputTextureFileNames.GetCount();
					for(int b = 0; b < texCount; b++)
					{
						parTreeNode *pTexNode = new parTreeNode();
						parElement& texElm = pTexNode->GetElement();
						texElm.SetName("Texture");
						
						//Get the parameter name this texture was connected to
						const char* texParamName = "";
						for (int p = 0; p < pMtl->GetParamCount(); p++)
						{
							shaderMaterialGeoParamValue* param = pMtl->GetParam(p);
							if (param && param->GetType() == grcEffect::VT_TEXTURE)
							{
								if(_strcmpi( ((shaderMaterialGeoParamValueTexture*)param)->GetValue(),
											   matInfo.m_InputTextureFileNames[b]) == 0)
								{
									texParamName = param->GetName();
								}
							}
						}
				
						texElm.AddAttribute("ShaderParam", texParamName);
						pTexNode->SetData((const char*)matInfo.m_InputTextureFileNames[b], matInfo.m_InputTextureFileNames[b].GetLength());
						pTexNode->AppendAsChildOf(pTexturesNode);
					}
				}
			}

			bool bRes = PARSER.SaveTree("rageMaterials", "dep", pTree, parManager::XML);
			if(!bRes)
			{
				rexResult result;
				result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );
				
				char acFullPathForDebugging[255];
				ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"rageMaterials","dep");

				// Flip the slashes
				for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

				// Error
				result.AddMessage( "Could not create file '%s'", acFullPathForDebugging);

				delete pTree;
				return result;
			}
			delete pTree;
		}
		else
#endif
		{
			fiStream* shFile = s;

			if(GetWriteSeparateShadingGroupFile() && !rexShell::DoesntSerialise())
			{
				shFile = ASSET.Create( "shaders", "shadergroup" );
				if( !shFile )
				{
					rexResult result;

					// Oh dear, something bad has happened
					result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

					// Get full path for debugging
					char acFullPathForDebugging[255];
					ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"shaders","shadergroup");

					// Flip the slashes
					for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

					// Error
					result.AddError( "Could not create file '%s'", acFullPathForDebugging);
					
					return result;
				}
			}

			sprintf( buffer, "\tshadinggroup {\r\n\t\tCount %d\r\n", shadingGroupCount ); 
			shFile->Write( buffer, (int)strlen( buffer ) );			

			for( int sg = 0; sg < shadingGroupCount; sg++ )
			{
				
				sprintf( buffer, "\t\tShaders %d {\r\n\t\t\tVers: 1\n", matCount );
				shFile->Write( buffer, (int)strlen( buffer ) );			

				for( int a = 0; a < matCount; a++ )
				{
					rexObjectGenericMesh::MaterialInfo& matInfo = matInfoArray[ a ];

					rexShell::Report().AddToMaterialIDList(
						entityName.c_str(), matInfo.m_Name.c_str(), a);

					const char *shaderName = matInfo.m_TypeName.c_str();
					const char* extension = ASSET.FindExtensionInPath(matInfo.m_TypeName);
					bool isMaterialPreset = extension != NULL && (MaterialPresetManager::IsMaterialPreset(extension) == true);
					if ( isMaterialPreset )
						shaderName = matInfo.m_MaterialPreset.GetShaderName();

					sprintf( buffer, "\t\t\t%s ", shaderName );
					shFile->Write( buffer, (int)strlen( buffer ) );			
					int foundCount = 0;
					int *count = shaderHash.Access(matInfo.m_TypeName);
					if ( count ) {
						foundCount = *count;
						*count += 1;
					}
					else
					{
						shaderHash.Insert(ConstString(matInfo.m_TypeName.c_str()), 1);
					}

					if ( isMaterialPreset ) 
					{
						//Output the .sva file that the runtime will eventually use so we don't have
						//to change this extension during post-process.  Though maybe we'll want to anyway
						//for consistency's sake.
						sprintf( svaName, "%s_%s_%03d.sva", (const char*)matName, (const char*)rexUtility::BaseFilenameFromPath(matInfo.m_TypeName), foundCount);
						sprintf( buffer, "1 %s\r\n", svaName);

						//Change this to the .mpo file.
						sprintf( svaName, "%s_%s_%03d%s", (const char*)matName, (const char*)rexUtility::BaseFilenameFromPath(matInfo.m_TypeName), foundCount, MaterialPresetManager::m_ExportFileExtension);
					}
					else
					{
  						sprintf( svaName, "%s_%s_%03d.sva", (const char*)matName, (const char*)rexUtility::BaseFilenameFromPath(matInfo.m_TypeName), foundCount);
						sprintf( buffer, "1 %s\r\n", svaName);
					}

					shFile->Write( buffer, (int)strlen( buffer ) );			

					if(!rexShell::DoesntSerialise())
					{
						if ( isMaterialPreset )
						{
							SavePresetData(entityPath, svaName, matInfo);
						}
						else
						{
							SaveShaderData(entityPath, svaName, matInfo);
						}
					}
				}
				sprintf( buffer, "\t\t}\r\n" );
				shFile->Write( buffer, (int)strlen( buffer ) );
			}
			sprintf( buffer, "\t}\r\n" );
			shFile->Write( buffer, (int)strlen( buffer ) );

			if(GetWriteSeparateShadingGroupFile())
			{
				shFile->Close();
			}
		}
    }
    return result;
}

///////////////////////////////////////////////////////////////////////////////////////

static void GatherBoneTagInfo( const rexObjectGenericSkeleton::Bone* bone, atArray<atString>& boneTagIDs, atArray<int>& boneTagIndices, int& currIndex )
{
	Assert( bone );
	int boneTagCount = bone->m_BoneTags.GetCount();
	for( int a = 0; a < boneTagCount; a++ )
	{
		boneTagIDs.PushAndGrow( bone->m_BoneTags[a], (u16) boneTagCount );
		boneTagIndices.PushAndGrow( currIndex, (u16) boneTagCount );
	}
	currIndex++;
	
	int childCount = bone->m_Children.GetCount();
	
	for( int a = 0; a < childCount; a++ )
	{
		GatherBoneTagInfo( bone->m_Children[ a ], boneTagIDs, boneTagIndices, currIndex );
	}
}

///////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEEntity::WriteSkeletonAndBoneTagInfo( rexObject& object, fiStream* s )
{
	if( !s )
		return rexResult::ERRORS;

	const rexObjectGenericSkeleton* inputSkeleton = dynamic_cast<const rexObjectGenericSkeleton*>( &object );

	if( !inputSkeleton )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	
	char buffer[512];
	if( inputSkeleton->m_RootBone ) //&& inputSkeleton->m_RootBone->m_Children.GetCount() ) // don't refer if only one bone
	{
		sprintf( buffer, "skel {\r\n\tskel %s.skel\r\n", (const char*)inputSkeleton->GetName() );
		s->Write( buffer, (int)strlen( buffer ) );

		if(inputSkeleton->m_JointLimits.GetLength())
		{
			sprintf( buffer,"\tjointlimits %s.jlimits\r\n", (const char*)inputSkeleton->m_JointLimits );
			s->Write( buffer, (int)strlen( buffer ) );
		}

		sprintf(buffer, "}\r\n");
		s->Write(buffer, (int)strlen( buffer ) );

		if( inputSkeleton->m_SkeletonType.GetLength() )
		{
			sprintf( buffer, "skeletontype {\r\n\tskeletontype %s\r\n}\r\n", (const char*)inputSkeleton->m_SkeletonType );
			s->Write( buffer, (int)strlen( buffer ) );
		}

		atArray<atString>	boneTagIDs;
		atArray<int>		boneTagIndices;

		int currIndex = 0;
		GatherBoneTagInfo( inputSkeleton->m_RootBone, boneTagIDs, boneTagIndices, currIndex );

		int tagCount = boneTagIDs.GetCount();
		if( tagCount )
		{
			sprintf( buffer, "bonetag {\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );

			for( int a = 0; a < tagCount; a++ )
			{
				sprintf( buffer, "\t%s\t", (const char*)boneTagIDs[ a ] );
				s->Write( buffer, (int)strlen( buffer ) );				
				sprintf( buffer, "%d\r\n", boneTagIndices[ a ] );
				s->Write( buffer, (int)strlen( buffer ) );
			}

			sprintf( buffer, "}\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );
		}
	}
	else
	{
		sprintf( buffer, "skel {\r\n\tskel none\r\n}\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );
	}

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEEntity::WriteAnimationInfo( rexObject& object, fiStream* s )
{
	if( !s )
		return rexResult::ERRORS;

	const rexObjectGenericAnimation* inputAnimation = dynamic_cast<const rexObjectGenericAnimation*>( &object );

	if( !inputAnimation )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	
	char buffer[512];
	int childCount = inputAnimation->m_ContainedObjects.GetCount();
		
	if( childCount )
	{
		int segmentCount = 0;

		for( int a = 0; a < childCount; a++ )
		{
			const rexObjectGenericAnimationSegment* segment = dynamic_cast<rexObjectGenericAnimationSegment*>( inputAnimation->m_ContainedObjects[ a ] );
			if( segment )
				segmentCount++;
		}		

		if( segmentCount )
		{
			sprintf( buffer, "Animation {\r\n\tanim {\r\n\t\tCount %d\r\n", segmentCount );
			s->Write( buffer, (int)strlen( buffer ) );

			for( int a = 0; a < childCount; a++ )
			{
				const rexObjectGenericAnimationSegment* segment = dynamic_cast<const rexObjectGenericAnimationSegment*>( inputAnimation->m_ContainedObjects[ a ] );
				if( segment )
				{
					int chunkCount = segment->m_ChunkNames.GetCount();
					if( chunkCount )
					{
						sprintf( buffer, "\t\tanimation %s ", (const char*)segment->GetName() );
						s->Write( buffer, (int)strlen( buffer ) );

						sprintf( buffer, "%d rootbones ", chunkCount );
						s->Write( buffer, (int)strlen( buffer ) );

						for( int b = 0; b < chunkCount; b++ )
						{
							const rexObjectGenericAnimation::ChunkInfo* chunk = inputAnimation->GetChunkByName( segment->m_ChunkNames[ b ] );
							Assert( chunk );
							sprintf( buffer, "%d ", chunk->m_BoneIndex );
							s->Write( buffer, (int)strlen( buffer ) );
						}

//						sprintf( buffer, "rootsonly\r\n" );
						sprintf( buffer, "\r\n" );
						s->Write( buffer, (int)strlen( buffer ) );
					}
				}					
			}		
			sprintf( buffer, "\t}\r\n}\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );
		}

	}

	return rexResult( rexResult::PERFECT );
}

rexResult rexSerializerRAGEEntity::WritePrimitiveInfo( rexObject& object, fiStream* s)
{
	rexResult result(rexResult::PERFECT);

	if( !s )
		return rexResult::ERRORS;

	const rexObjectGenericPrimitiveGroup* primitiveGroup = dynamic_cast<const rexObjectGenericPrimitiveGroup*>( &object );

	if( !primitiveGroup )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	
	float conversionFactor=rexSerializer::GetUnitTypeLinearConversionFactor();

	char buffer[512];
	int childCount = primitiveGroup->m_ContainedObjects.GetCount();

	if( childCount )
	{
		sprintf( buffer, "primitive {\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );

		for( int a = 0; a < childCount; a++ )
		{
			const rexObjectGenericPrimitive* pPrimBase = dynamic_cast<const rexObjectGenericPrimitive*>( primitiveGroup->m_ContainedObjects[ a ] );
			if(pPrimBase)
			{
				if(pPrimBase->GetPrimType() == rexObjectGenericPrimitive::PRIM_BOX)
				{
					const rexObjectGenericPrimitiveBox* pPrimBox = static_cast<const rexObjectGenericPrimitiveBox*>(pPrimBase);
					sprintf(buffer, "\tbox {\r\n");
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf(buffer, "\t\tname %s\r\n", (const char*)pPrimBox->GetName());
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf(buffer, "\t\twidth %f\r\n", pPrimBox->m_Width * conversionFactor);
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf(buffer, "\t\theight %f\r\n", pPrimBox->m_Height * conversionFactor);
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf(buffer, "\t\tdepth %f\r\n", pPrimBox->m_Depth * conversionFactor);
					s->Write( buffer, (int)strlen( buffer ) );


					Matrix34 m( M34_IDENTITY );
					m = pPrimBox->m_Matrix;

					sprintf( buffer, "\t\tmatrix\r\n" );
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\t{\r\n" );	
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\t\t%f %f %f\r\n", m.a.x, m.a.y, m.a.z );
					s->Write( buffer, (int)strlen( buffer ) );
					
					sprintf( buffer, "\t\t\t%f %f %f\r\n", m.b.x, m.b.y, m.b.z );
					s->Write( buffer, (int)strlen( buffer ) );
					
					sprintf( buffer, "\t\t\t%f %f %f\r\n", m.c.x, m.c.y, m.c.z );
					s->Write( buffer, (int)strlen( buffer ) );
					
					sprintf( buffer, "\t\t\t%f %f %f\r\n", m.d.x * conversionFactor
															 , m.d.y * conversionFactor
															 , m.d.z * conversionFactor);
					s->Write( buffer, (int)strlen( buffer ) );

					
					sprintf( buffer, "\t\t}\r\n" );
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf(buffer, "\t}\r\n");
					s->Write( buffer, (int)strlen( buffer ) );
				}
				else if(pPrimBase->GetPrimType() == rexObjectGenericPrimitive::PRIM_SPHERE)
				{
					const rexObjectGenericPrimitiveSphere* pPrimSphere = static_cast<const rexObjectGenericPrimitiveSphere*>(pPrimBase);
					sprintf(buffer,"\tsphere {\r\n");
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf(buffer, "\t\tname %s\r\n", (const char*)pPrimSphere->GetName());
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf(buffer,"\t\tradius %f\r\n", pPrimSphere->m_Radius * conversionFactor);
					s->Write( buffer, (int)strlen( buffer ) );

					Matrix34 m( M34_IDENTITY );
					m = pPrimSphere->m_Matrix;
					
					sprintf( buffer, "\t\tmatrix\r\n" );
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\t{\r\n" );	
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\t\t%f %f %f\r\n", m.a.x, m.a.y, m.a.z );
					s->Write( buffer, (int)strlen( buffer ) );
					
					sprintf( buffer, "\t\t\t%f %f %f\r\n", m.b.x, m.b.y, m.b.z );
					s->Write( buffer, (int)strlen( buffer ) );
					
					sprintf( buffer, "\t\t\t%f %f %f\r\n", m.c.x, m.c.y, m.c.z );
					s->Write( buffer, (int)strlen( buffer ) );
					
					sprintf( buffer, "\t\t\t%f %f %f\r\n", m.d.x * conversionFactor
															 , m.d.y * conversionFactor
															 , m.d.z * conversionFactor);
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\t}\r\n" );
					s->Write( buffer, (int)strlen( buffer ) );

					sprintf(buffer,"\t}\r\n");
					s->Write( buffer, (int)strlen( buffer ) );
				}
			}
		}

		sprintf( buffer, "}\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );
	}

	return result;
}

rexResult rexSerializerRAGEEntity::WriteLocatorInfo( rexObject& object, fiStream* s )
{
	rexResult result;

	if( !s )
		return rexResult::ERRORS;

	const rexObjectGenericLocatorGroup* locatorGroup = dynamic_cast<const rexObjectGenericLocatorGroup*>( &object );

	if( !locatorGroup )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	
	float conversionFactor=rexSerializer::GetUnitTypeLinearConversionFactor();

	char buffer[512];
	int childCount = locatorGroup->m_ContainedObjects.GetCount();

	if( childCount )
	{
		if(!GetWriteLocatorsAsSeparateFiles())
		{
			sprintf( buffer, "%s {\r\n", locatorGroup->GetEntityGroupTag() );
			s->Write( buffer, (int)strlen( buffer ) );

			for( int a = 0; a < childCount; a++ )
			{
				const rexObjectGenericLocator* locator = dynamic_cast<const rexObjectGenericLocator*>( locatorGroup->m_ContainedObjects[ a ] );

				if( locator )
				{
					Vector3 eulers;
					locator->m_Matrix.ToEulersXYZ( eulers );

					sprintf( buffer, "\t%s ", (const char*)locator->GetName());
					s->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "boneindex %d ", locator->m_BoneIndex );
					s->Write( buffer, (int)strlen( buffer ) );
					Vector3 offset(locator->m_Matrix.d.x, locator->m_Matrix.d.y, locator->m_Matrix.d.z);
					offset.Scale(conversionFactor);
					sprintf( buffer, "offset %f %f %f ",  offset.x, offset.y, offset.z);
					s->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "eulers %f %f %f", eulers.x, eulers.y, eulers.z );
					s->Write( buffer, (int)strlen( buffer ) );

					//Write out any extra attributes associated with the locator
					int extraAttrCount = locator->m_AttributeNames.GetCount();
					sprintf( buffer, " [ ");
					for(int i=0; i<extraAttrCount; i++)
					{
						char tmp[128];
						
						if(i<extraAttrCount-1)
							sprintf(tmp, "%s:%s, ", (const char*)locator->m_AttributeNames[i], (const char*)locator->m_AttributeValues[i].toString());
						else
							sprintf(tmp, "%s:%s", (const char*)locator->m_AttributeNames[i], (const char*)locator->m_AttributeValues[i].toString());
						
						strcat(buffer, tmp);
					}
					strcat(buffer, " ]\r\n");
					s->Write(buffer, (int)strlen( buffer ) );
				}
			}

			sprintf( buffer, "}\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );
		}//End if(!GetWriteLocatorsAsSeparateFiles())
		else
		{
			for( int a = 0; a < childCount; a++ )
			{
				const rexObjectGenericLocator* locator = dynamic_cast<const rexObjectGenericLocator*>( locatorGroup->m_ContainedObjects[ a ] );

				if( locator )
				{
					ASSET.SetPath( GetOutputPath() );
					fiStream* locatorStream = ASSET.Create( (const char*)locator->GetName(), "locator" );
					if( !locatorStream )
					{
						// Oh dear, something bad has happened
						result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

						// Get full path for debugging
						char acFullPathForDebugging[255];
						ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),(const char*)locator->GetName(),"locator" );

						// Flip the slashes
						for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

						// Error
						result.AddError( "Could not create file '%s'", acFullPathForDebugging);
					}
				
					Vector3 eulers;
					locator->m_Matrix.ToEulersXYZ( eulers );

					sprintf( buffer, "%s ", (const char*)locator->GetName());
					locatorStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "boneindex %d ", locator->m_BoneIndex );
					locatorStream->Write( buffer, (int)strlen( buffer ) );
					Vector3 offset(locator->m_Matrix.d.x, locator->m_Matrix.d.y, locator->m_Matrix.d.z);
					offset.Scale(conversionFactor);
					sprintf( buffer, "offset %f %f %f ",  offset.x, offset.y, offset.z);
					locatorStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "eulers %f %f %f", eulers.x, eulers.y, eulers.z );
					locatorStream->Write( buffer, (int)strlen( buffer ) );

					//Write out any extra attributes associated with the locator
					int extraAttrCount = locator->m_AttributeNames.GetCount();
					sprintf( buffer, " [ ");
					for(int i=0; i<extraAttrCount; i++)
					{
						char tmp[128];
						
						if(i<extraAttrCount-1)
							sprintf(tmp, "%s:%s, ", (const char*)locator->m_AttributeNames[i], (const char*)locator->m_AttributeValues[i].toString());
						else
							sprintf(tmp, "%s:%s", (const char*)locator->m_AttributeNames[i], (const char*)locator->m_AttributeValues[i].toString());
						
						strcat(buffer, tmp);
					}
					strcat(buffer, " ]\r\n");
					locatorStream->Write(buffer, (int)strlen( buffer ) );

					locatorStream->Close();
					
				}//End if( locator )
			}
		}
	}
	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

static void GetEdgeModels( rexObject& object, atArray<rexObjectGenericMesh*>& edgeModelList )
{
	rexObjectGenericMesh* mesh = dynamic_cast<rexObjectGenericMesh*>( &object );

	if( mesh )
		edgeModelList.PushAndGrow( mesh );

	const rexObjectGenericEdgeModelHierarchy* inputMeshHier = dynamic_cast<const rexObjectGenericEdgeModelHierarchy*>( &object );
	if( inputMeshHier )
	{
		int childObjCount = object.m_ContainedObjects.GetCount();
		
		for( int a = 0; a < childObjCount; a++ )
		{
			GetEdgeModels( *object.m_ContainedObjects[ a ], edgeModelList );
		}
	}	
}

rexResult rexSerializerRAGEEntity::WriteEdgeModelInfo( rexObject& object, fiStream* s )
{
	if( !s )
		return rexResult::ERRORS;

	const rexObjectGenericEdgeModelHierarchy* inputMeshHier = dynamic_cast<const rexObjectGenericEdgeModelHierarchy*>( &object );

	if( !inputMeshHier )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	int childCount = inputMeshHier->m_ContainedObjects.GetCount();
	for( int a = 0; a < childCount; a++ )
		GetEdgeModels( *inputMeshHier->m_ContainedObjects[ a ], m_EdgeModelList );

	char buffer[512];
	if( m_EdgeModelList.GetCount() )
	{
		sprintf( buffer, "edge {\r\n\t" );
		s->Write( buffer, (int)strlen( buffer ) );

		if(inputMeshHier->m_AllowMultipleEdgeModels)
		{
			// write all of them out
			int emCount = m_EdgeModelList.GetCount();

			sprintf( buffer, "edgeset %d", emCount );
			s->Write( buffer, (int)strlen( buffer ) );

			for( int a = 0; a < emCount; a++ )
			{
				sprintf( buffer, " %s.em", (const char*)m_EdgeModelList[ a ]->GetMeshName() );
				s->Write( buffer, (int)strlen( buffer ) );
			}

			s->Write( "\r\n", 2 );
		}
		else
		{
			// only include one
			sprintf( buffer, "edge %s.em\r\n", (const char*)m_EdgeModelList[ 0 ]->GetMeshName() );
			s->Write( buffer, (int)strlen( buffer ) );
		}

		sprintf( buffer, "}\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );
	}

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

static void GetBounds( rexObject& object, atArray<rexObjectGenericBound*>& boundList, bool useBoundFiltering = false, eConsiderTroolean ignoreOrphaned = CONSIDER_ALL)
{
	rexObjectGenericBound* mesh = dynamic_cast<rexObjectGenericBound*>( &object );

	if( mesh )
	{ 
		//if we are not bound filtering
		bool isCloth = mesh->m_IsClothCollision;
		if (!(useBoundFiltering &&
				(
					mesh->m_Type == phBound::GEOMETRY ||
					mesh->m_Type == phBound::BOX ||
					mesh->m_Type == phBound::CAPSULE ||
					mesh->m_Type == phBound::SPHERE
					)	 
				) 
				&& 
			(
				ignoreOrphaned == CONSIDER_ALL || 
				(ignoreOrphaned == CONSIDER_IFNOT && !isCloth) ||
				(ignoreOrphaned == CONSIDER_ONLY && isCloth)
				)
		   )
		{
			boundList.PushAndGrow( mesh );
		}
	}

	const rexObjectGenericBoundHierarchy* inputMeshHier = dynamic_cast<const rexObjectGenericBoundHierarchy*>( &object );
	if( inputMeshHier )
	{
		int childObjCount = object.m_ContainedObjects.GetCount();
		
		for( int a = 0; a < childObjCount; a++ )
		{
			GetBounds( *object.m_ContainedObjects[ a ], boundList, useBoundFiltering, ignoreOrphaned);
		}
	}	
}

///////////////////////////////////////////////////////////////////////////////////////////

void rexSerializerRAGEEntity::GetFilteredBounds( rexObject& object, const atStringArray& boundFilterTypes, const atMap<atString, atString>& specialFlagMapping, atMap<atString, atString>& boundFilterList )
{
	rexObjectGenericBound* inputBound = dynamic_cast<rexObjectGenericBound*>( &object );

	if (inputBound)
	{
		if (inputBound->m_Type == phBound::GEOMETRY ||
			inputBound->m_Type == phBound::SPHERE ||
			inputBound->m_Type == phBound::CAPSULE ||
			inputBound->m_Type == phBound::BOX
		   )
		{
			for (int material = 0; material < inputBound->m_Materials.GetCount(); material++)
			{
				atString filter("");
				atString specialFlags("");
				//assumed materialName format - boundm,somevalue,anothervalue
				const char* mName = inputBound->m_Materials[material].m_Name.c_str();
				if (mName)
				{
					int strlength = (int)strlen(mName);
					int charIndex = 0;
					while (charIndex < strlength)
					{
						atString atType;
						while( (mName[charIndex] != ',') &&
							(charIndex < (int)strlength) )
						{
							atType += mName[charIndex++];
						}

						if (boundFilterTypes.Find(atType)!= -1)
						{
							//only add unique
							if (!strstr(filter.c_str(),atType.c_str()))
							{
								filter += atType;


								//only one special flag is allowed per type now ... so hack we go...
								if (specialFlagMapping.Access(atType))
								{
									const atString* specialFlag = specialFlagMapping.Access(atType); 
									//assumed format - "specialFlag1", "specialFlag2"
									//if (specialFlags.GetLength() == 0)
									//{
										specialFlags = "\"";
										specialFlags += (*specialFlag);
										specialFlags += "\"";
									//}
									//else
									//{
									//	specialFlags += ", \"";
									//	specialFlags += (*specialFlag);
									//	specialFlags += "\"";
									//}
								}

                                //only add unique
                                if (atType != "" && !boundFilterList.Access(atType))
                                {
									Displayf("%s, %s", atType.c_str(), specialFlags.c_str());
                                    boundFilterList.Insert(atType, specialFlags);
                                }
							}
							else
							{
								Warningf("Multiple taggings with same bound type. [%s] Only processing the unique tag.",mName);
							}
						}
						charIndex++;
					}
				}
			}
		}
		else
		{
			//AssertMsg(0 , "Bound Filtering for anything except GEOMETRY is not implemented yet.");
		}

	}

	const rexObjectGenericBoundHierarchy* inputMeshHier = dynamic_cast<const rexObjectGenericBoundHierarchy*>( &object );
	if( inputMeshHier )
	{
		int childObjCount = object.m_ContainedObjects.GetCount();

		for( int a = 0; a < childObjCount; a++ )
		{
			GetFilteredBounds( *object.m_ContainedObjects[ a ], boundFilterTypes, specialFlagMapping, boundFilterList );
		}
	}	
}

///////////////////////////////////////////////////////////////////////////////////////////

void rexSerializerRAGEEntity::AddBoundSpecialFlagMapping(const atString& materialFlag, const atString& specialFlag)
{
	//only add unique
	if(!m_BoundSpecialFlagMappings.Access(materialFlag))
	{
		m_BoundSpecialFlagMappings.Insert(materialFlag, specialFlag);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////

void rexSerializerRAGEEntity::AddLODMeshSpecialFlagMapping(const atString& materialFlag, const atString& specialFlag)
{
	//Only add unique types
	if(!m_LODMeshFilterMappings.Access(materialFlag))
	{
		m_LODMeshFilterMappings.Insert(materialFlag, specialFlag);
	}
}



rexResult rexSerializerRAGEEntity::WriteLightInfo( rexObject& object, fiStream* /*s*/ )
{
	const rexObjectGenericLightGroup* lightGroup = dynamic_cast<const rexObjectGenericLightGroup*>( &object );
	if( !lightGroup )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	if(!rexShell::DoesntSerialise())
		return rexResult::PERFECT;

	// WRITE DISTRICT.LIGHTS file
	fiStream* ls = ASSET.Create( "entity", "lights" );
	if( !ls )
	{
		// Oh dear, something bad has happened
		rexResult retval;
		retval.Combine( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"entity","lights" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		retval.AddError( "Could not create file '%s'", acFullPathForDebugging);
		return retval;
	}

	char buffer[512];

	int childCount = lightGroup->m_ContainedObjects.GetCount();

	sprintf( buffer, "Count %d\r\n", childCount );
	ls->Write( buffer, (int)strlen( buffer ) );

	for( int a = 0; a < childCount; a++ )
	{
		const rexObjectGenericLight* light = dynamic_cast<const rexObjectGenericLight*>( lightGroup->m_ContainedObjects[ a ] );

		if( light )
		{
			sprintf( buffer, "Light %s {\r\n", (const char*)light->GetName() );
			ls->Write( buffer, (int)strlen( buffer ) );

			if( light->m_LightType == rexObjectGenericLight::LIGHT_DIRECTIONAL )
                sprintf( buffer, "\tType directionalLight\r\n" );
			else if( light->m_LightType == rexObjectGenericLight::LIGHT_POINT )
				sprintf( buffer, "\tType pointLight\r\n" );
			else if( light->m_LightType == rexObjectGenericLight::LIGHT_SPOTLIGHT )
				sprintf( buffer, "\tType spotLight\r\n" );
			else
				sprintf( buffer, "\tType ambientLight\r\n" );
			ls->Write( buffer, (int)strlen( buffer ) );

			sprintf( buffer, "\tLocation %s\r\n", ( light->m_RoomIndex >= 0 ) ? "interior" : "exterior" );
			ls->Write( buffer, (int)strlen( buffer ) );

			sprintf( buffer, "\tRoom %d\r\n", light->m_RoomIndex );
			ls->Write( buffer, (int)strlen( buffer ) );
			
			sprintf( buffer, "\tPosition %f %f %f\r\n", light->m_Location.x, light->m_Location.y, light->m_Location.z );
			ls->Write( buffer, (int)strlen( buffer ) );

			Vector3 dirNormalized( light->m_Direction );
			dirNormalized.Normalize();
			
			sprintf( buffer, "\tDirection %f %f %f\r\n", dirNormalized.x, dirNormalized.y, dirNormalized.z );
			ls->Write( buffer, (int)strlen( buffer ) );

			if( light->m_LightType == rexObjectGenericLight::LIGHT_SPOTLIGHT )
			{
				sprintf( buffer, "\tSpotAngle %f\r\n", light->m_SpotAngle );
				ls->Write( buffer, (int)strlen( buffer ) );
			}

			sprintf( buffer, "\tColor %f %f %f\r\n", light->m_Color.x, light->m_Color.y, light->m_Color.z );//, light->m_Color.w );
			ls->Write( buffer, (int)strlen( buffer ) );

			sprintf( buffer, "\tIntensity %f\r\n", light->m_Intensity );
			ls->Write( buffer, (int)strlen( buffer ) );

			sprintf( buffer, "\tCastShadowRange %f\r\n", light->m_CastShadowRange );
			ls->Write( buffer, (int)strlen( buffer ) );

			fprintf(ls,"\tDecayRate %d\r\n", light->m_DecayRate);

			sprintf( buffer, "}\r\n" );
			ls->Write( buffer, (int)strlen( buffer ) );
		}
	}

	ls->Close();

	return rexResult( rexResult::PERFECT );
}

rexResult rexSerializerRAGEEntity::WriteCurveInfo( rexObject& object, fiStream* /*s*/ )
{
	rexObjectGenericNurbsCurveGroup* curveGroup = dynamic_cast<rexObjectGenericNurbsCurveGroup*>(&object);

	//Hack for backwards compatibility
	//  Previously, all curve groups were hardcoded to write out as "entity.curves", if the curve group name
	//  is simply "Curve", we should write it out using the old "enity.curves" filename, otherwise use the
	//  curve group name
	if(strcmpi(curveGroup->GetName(), "Curve") == 0)
		return rexSerializerRAGENurbsCurve::WriteCurveInfo(object,"entity", GetSerializeVersionTwoNurbsCurves());
	else
		return rexSerializerRAGENurbsCurve::WriteCurveInfo(object, curveGroup->GetName(), GetSerializeVersionTwoNurbsCurves());
}

rexResult rexSerializerRAGEEntity::WriteBoundInfo( rexObject& object, fiStream* s )
{
	if( !s )
		return rexResult::ERRORS;

	rexObjectGenericBoundHierarchy* inputMeshHier = dynamic_cast<rexObjectGenericBoundHierarchy*>( &object );

	if( !inputMeshHier )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	if (!m_SkipBoundSectionOfTypeFile)
	{
		// For standalone group only consider scene rooted bounds
		GetBounds( *inputMeshHier, m_BoundList, GetUseBoundFiltering(), CONSIDER_ONLY); // Consider only orphaned collision for the entity.type group
#if HACK_GTA4
		WriteBoundList( m_BoundList, s, 0, NULL, m_BoundListName.c_str());//, &parentList);
#else
		atMap<atString, atString> boundFilterList;
		if (GetUseBoundFiltering())
		{
			
			int childCount = object.m_ContainedObjects.GetCount();
			for( int a = 0; a < childCount; a++ )
				GetFilteredBounds( *inputMeshHier->m_ContainedObjects[ a ], m_BoundFilterTypes, m_BoundSpecialFlagMappings, boundFilterList );
		}

		if (m_BoundList.GetCount() || boundFilterList.GetNumUsed())
		{
			char buffer[32];
			int depth = 0;
			//open
			buffer[0] = '\0';
			Indent( buffer, depth );
			strcat( buffer, "bound {\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );

			if (m_BoundList.GetCount())
			{
				WriteBoundList(m_BoundList, s, depth, NULL, m_BoundListName.c_str());
			}

			if (GetUseBoundFiltering() && boundFilterList.GetNumUsed())
			{
				WriteFilteredBoundList(boundFilterList, s, depth );
			}

			//close
			buffer[0] = '\0';
			Indent( buffer, depth );
			strcat( buffer, "}\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );
		}	
#endif // HACK_GTA4
		m_BoundList.clear();
		GetBounds( *inputMeshHier, m_BoundList, GetUseBoundFiltering(), CONSIDER_ALL); // Consider only orphaned collision for the entity.type group
	}
	else
		GetBounds( *inputMeshHier, m_BoundList, GetUseBoundFiltering());//, &parentList);


	return rexResult( rexResult::PERFECT );
}

#define MAKE_SINGLE_PART_COMPOSITES 1

// void GetBoundsByBone(atMap<atString, atArray<int>* > &boneBoundMap, atArray<rexObjectGenericBound*> &boundList)
// {
// //	boneBoundMap.Create(pParentList.GetCount());
// 	boundList.QSort
// 	for( int b=0; b<boundList.GetCount() ; b++ )
// 	{
// 		atArray<int> *pArray = *boneBoundMap.Access(boundList[b]->GetParentBoneName());
// 		if(pArray)
// 		{
// 			pArray->PushAndGrow(b);
// 		}
// 		else
// 		{
// 			pArray = new atArray<int>();
// 			pArray->PushAndGrow(b);
// 			boneBoundMap.Insert(boundList[b]->GetParentBoneName(),new atArray<int>());
// 		}
// 	}
// }

static int QSOrtFunc(rexObjectGenericBound* const* ppBoundLeft, rexObjectGenericBound* const* ppBoundRight)
{
	return strcmp((*ppBoundLeft)->GetParentBoneName().c_str(), (*ppBoundRight)->GetParentBoneName().c_str());
}


void rexSerializerRAGEEntity::WriteBoundList( atArray<rexObjectGenericBound*>& boundList, fiStream* s, int depth, atArray<rexObjectGenericMesh*>* /*pParentList*/, const char *boundListName)
{

	if(rexShell::DoesntSerialise())
		return;

	int boundCount = boundList.GetCount();
	int actualCount = 0;
	for( int a = 0; a < boundCount; a++ )
	{
		if( boundList[ a ]->m_IsValid )
		{
			++actualCount;
		}
	}

#if MAKE_SINGLE_PART_COMPOSITES
	if (m_ComposeBounds && actualCount > 0)
#else
	if (m_ComposeBounds && actualCount > 1)
#endif
	{
		char compositeName[256];

		sprintf(compositeName, "composite%d", m_CompositeFileCount);
		++m_CompositeFileCount;

		char buffer[1024] = "";

		// Now write out the actual composite file
		if (fiStream* compositeStream = ASSET.Create( compositeName, "bnd" ))
		{
			buffer[0] = '\0';
			strcat(buffer, "version: 1.10\r\n");
			compositeStream->Write( buffer, (int)strlen( buffer ) );		

			buffer[0] = '\0';
			strcat(buffer, "type: composite\r\n");
			compositeStream->Write( buffer, (int)strlen( buffer ) );		

			sprintf(buffer, "NumBounds: %d\r\n", actualCount);
			compositeStream->Write( buffer, (int)strlen( buffer ) );		

			buffer[0] = '\0';
			strcat(buffer, "materials: 0\r\n");
			compositeStream->Write( buffer, (int)strlen( buffer ) );		

			buffer[0] = '\0';
			strcat(buffer, "\r\n");
			compositeStream->Write( buffer, (int)strlen( buffer ) );		

			buffer[0] = '\0';
			strcat(buffer, "\r\n");
			compositeStream->Write( buffer, (int)strlen( buffer ) );		

			actualCount = 0;
			for( int a = 0; a < boundCount; a++ )
			{
				if( boundList[ a ]->m_IsValid )
				{
					sprintf(buffer, "%s: %d\r\n", boundListName, actualCount);
					compositeStream->Write( buffer, (int)strlen( buffer ) );		

					buffer[0] = '\0';
					strcat(buffer, "name: ");
					strcat(buffer, (const char*)boundList[ a ]->GetMeshName());
					strcat(buffer, "\r\n\r\n");
					compositeStream->Write( buffer, (int)strlen( buffer ) );		

					Matrix34 parentMatrix;
					parentMatrix.Inverse(boundList[ a ]->m_ParentBoneMatrix);

					Matrix34 boundMatrix;
					boundMatrix.Dot(boundList[ a ]->m_Matrix, parentMatrix);

		            float conversionFactor=GetUnitTypeLinearConversionFactor();
                    boundMatrix.d.Scale(conversionFactor);

					buffer[0] = '\0';
					sprintf(buffer, "matrix: %f %f %f\r\n%f %f %f\r\n%f %f %f\r\n%f %f %f\r\n\r\n",
						    boundMatrix.a.x, boundMatrix.a.y, boundMatrix.a.z,
							boundMatrix.b.x, boundMatrix.b.y, boundMatrix.b.z,
							boundMatrix.c.x, boundMatrix.c.y, boundMatrix.c.z,
							boundMatrix.d.x, boundMatrix.d.y, boundMatrix.d.z);
					compositeStream->Write( buffer, (int)strlen( buffer ) );		

					++actualCount;
				}
			}

			compositeStream->Close();
		}

		// Now, put out composite into the .type file
		buffer[0] = '\0';
		Indent( buffer, depth );
		strcat( buffer, "\tbound " );
		strcat( buffer, compositeName);
		strcat( buffer, ".bnd\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );		
#if HACK_GTA4
		buffer[0] = '\0';
		Indent( buffer, depth );
		strcat( buffer, "}\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );	
#endif // HACK_GTA4
	}
	else
	{
		char buffer[512];
		int boundCount = boundList.GetCount();
		if( boundCount )
		{
			atMap<atString,atArray<int>*> boneBoundMap;

			if(m_GroupBoundsByBone)
			{
				boundList.QSort(0, boundList.GetCount(), &QSOrtFunc);
			}

#if HACK_GTA4
			buffer[0] = '\0';
			sprintf( buffer, "%s {\r\n", boundListName );
			Indent( buffer, depth );
			s->Write( buffer, (int)strlen( buffer ) );		
#endif // HACK_GTA4
			for( int a = 0; a < boundCount; a++ )
			{
				if(m_GroupBoundsByBone)// && pParentList)
				{
					if(0==a || (a>0 && boundList[ a ]->GetParentBoneName()!=boundList[ a-1 ]->GetParentBoneName()))
					{
						buffer[0] = '\0';
						if(a>0)
						{
							strcat( buffer, "\t}\n" );
						}
						strcat( buffer, "\t" );
						strcat( buffer, boundList[ a ]->GetParentBoneName() );
						strcat( buffer, " {\n" );
						s->Write( buffer, (int)strlen( buffer ) );		
					}
				}
				if( boundList[ a ]->m_IsValid )
				{
					buffer[0] = '\0';
					Indent( buffer, depth );
					strcat( buffer, "\tbound " );
					strcat( buffer, (const char*)boundList[ a ]->GetMeshName() );		
					strcat( buffer, ".bnd " );

					if(m_WriteBoundSituations || m_WriteBoundFlags)
					{
						if(m_WriteBoundSituations && (USE_GRIDS_ONLY(boundList[ a ]->m_Type != phBound::OCTREEGRID &&)
													  boundList[ a ]->m_Type != phBound::GEOMETRY &&
													  boundList[ a ]->m_Type != phBound::COMPOSITE) )
						{
							Vector3 offset;
							boundList[ a ]->m_ParentBoneMatrix.UnTransform(boundList[ a ]->m_Matrix.d, offset);

							float conversionFactor=GetUnitTypeLinearConversionFactor();
							offset.Scale(conversionFactor);

							char otherBuffer[128];
							sprintf( otherBuffer, "< %f, %f, %f > ", offset.x, offset.y, offset.z );
							strcat( buffer, otherBuffer );
		
							Quaternion parentRotation;
							parentRotation.FromMatrix34(boundList[ a ]->m_ParentBoneMatrix);
							parentRotation.Inverse();
		
							Quaternion boundRotation;
							boundRotation.FromMatrix34(boundList[ a ]->m_Matrix);
		
							boundRotation.MultiplyFromLeft(parentRotation);
		
							if(!m_WriteBoundFlags)
								sprintf( otherBuffer, "< %f, %f, %f, %f >", boundRotation.x, boundRotation.y, boundRotation.z, boundRotation.w );
							else
								sprintf( otherBuffer, "< %f, %f, %f, %f > ", boundRotation.x, boundRotation.y, boundRotation.z, boundRotation.w );

							strcat( buffer, otherBuffer );
						}

						if(m_WriteBoundFlags)
						{
							char otherBuffer[256];

							int flagCount = boundList[ a ]->m_SpecialFlags.GetCount();
							if( flagCount )
							{
								otherBuffer[0] = '\0';
								sprintf(otherBuffer, "[ ");
								for(int b = 0; b < flagCount; b++)
								{
									strcat(otherBuffer, (const char*)boundList[ a ]->m_SpecialFlags[ b ]);
									if(b != (flagCount-1))
										strcat(otherBuffer, ", ");
								}
								strcat(otherBuffer, " ]");
								strcat( buffer, otherBuffer);
							}
						}
					}
					strcat( buffer, "\r\n" );
					s->Write( buffer, (int)strlen( buffer ) );   
				}
			}
			if(m_GroupBoundsByBone)// && pParentList)
			{
				buffer[0] = '\0';
				strcat( buffer, "\t}\n" );
				s->Write( buffer, (int)strlen( buffer ) );	
			}

#if HACK_GTA4
			buffer[0] = '\0';
			Indent( buffer, depth );
			strcat( buffer, "}\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );
#endif // HACK_GTA4
		}
	}
}

void rexSerializerRAGEEntity::WriteFilteredBoundList( const atMap<atString, atString>& boundFilterList, fiStream* s, int depth )
{
	if (boundFilterList.GetNumUsed())
	{
		atMap<atString, atString>::ConstIterator iter = boundFilterList.CreateIterator();
		iter.Start();
		char buffer[512];
		while(!iter.AtEnd())
		{
			buffer[0] = '\0';
			Indent( buffer, depth );
			strcat( buffer, "\tbound " );
			strcat( buffer, (const char*)iter.GetKey() );		
			strcat( buffer, ".bnd " );

			if(m_WriteBoundFlags && iter.GetData() != "")
			{
				char otherBuffer[256];
				otherBuffer[0] = '\0';
				sprintf(otherBuffer, "[ ");
				//assumed to be already in the format - "specialFlag1", "specialFlag2"
				//  see GetFilteredBounds
				strcat(otherBuffer, (const char*)iter.GetData());
				strcat(otherBuffer, " ]\r\n");
				strcat( buffer, otherBuffer);
			}
			else
			{
				strcat( buffer, "\r\n" );
			}

			s->Write( buffer, (int)strlen( buffer ) );
			iter.Next();
		}
	}
}

static rexObjectGenericFragmentHierarchy* FindFragmentParent(const atString& childPath, rexObjectGenericFragmentHierarchy& hierarchy)
{
	// We want the child of "hierarchy" that makes the best parent to "child". That is, we want
	// the parent with the longest DAG path, for which the child's DAG path starts with the full
	// DAG path of the parent

	// Start with the root of hierarchy
	rexObjectGenericFragmentHierarchy* bestParent = &hierarchy;
	int bestLength = bestParent->GetFullPath().GetLength();

	int childCount = hierarchy.m_ContainedObjects.GetCount();

	// Then, for each of its children...
	for ( int a = 0; a < childCount; ++a )
	{
		if ( rexObjectGenericFragmentHierarchy* childHier = dynamic_cast<rexObjectGenericFragmentHierarchy*>(hierarchy.m_ContainedObjects[ a ]) )
		{
			// ...call ourselves on the child...
			if ( rexObjectGenericFragmentHierarchy* bestChildParent = FindFragmentParent(childPath, *childHier) )
			{
				int bestChildParentLength = (int)strlen(bestChildParent->GetFullPath());
#if !HACK_GTA4
				int childPathLength = (int)strlen(childPath);
#endif // HACK_GTA4

				// ...and if the child has a better parent than we had before, it's the new best parent.
				if ( strncmp(bestChildParent->GetFullPath(), childPath, bestChildParentLength) == 0 &&
#if !HACK_GTA4
					childPathLength > bestChildParentLength &&
					childPath[bestChildParentLength] == '|' &&
#endif // HACK_GTA4
					bestChildParentLength > bestLength )
				{
					bestParent = bestChildParent;
					bestLength = bestChildParentLength;
				}
			}
		}
	}

	return bestParent;
}

void rexSerializerRAGEEntity::WriteGroupAndChildHeader( const char* name, fiStream* s, bool isDamageState, int depth)
{
	char buffer[1024] = "";

	if (isDamageState)
	{
		buffer[0] = '\0';
		Indent(buffer, depth);
		strcat(buffer, "damagedChild ");
		strcat(buffer, name);
		strcat(buffer, " {\r\n");
		s->Write( buffer, (int)strlen( buffer ) );		
	}
	else
	{
		buffer[0] = '\0';
		Indent(buffer, depth);
		strcat(buffer, "group ");
		strcat(buffer, name);
		strcat(buffer, " {\r\n");
		s->Write( buffer, (int)strlen( buffer ) );		

		buffer[0] = '\0';
		Indent(buffer, depth + 1);
		strcat(buffer, "child {\r\n");
		s->Write( buffer, (int)strlen( buffer ) );		
	}
}

static int QSortDamageFunc(rexObject* const* ppObjectLeft, rexObject* const* ppObjectRight)
{
	rexObjectGenericFragmentHierarchy* childGroupLeft = dynamic_cast<rexObjectGenericFragmentHierarchy*>(*ppObjectLeft);
	rexObjectGenericFragmentHierarchy* childGroupRight = dynamic_cast<rexObjectGenericFragmentHierarchy*>(*ppObjectRight);

	if (childGroupRight && childGroupRight->GetDamageState())
		return 1;
	else if (childGroupLeft && childGroupLeft->GetDamageState())
		return -1;
	else
		return 0;
}

void rexSerializerRAGEEntity::WriteFragmentHierarchy(rexObjectGenericFragmentHierarchy& fragments, fiStream* s, int depth)
{
	char buffer[1024] = "";

	int childCount = fragments.m_ContainedObjects.GetCount();

	// Sort all the damage parts to be first.
	fragments.m_ContainedObjects.QSort(0, childCount, &QSortDamageFunc);
	int parentBoneIndex = 0;

	// Write out the child bounds
	bool childBoundWritten = false;

	// ...then, write out child fragment data for this group...
	atArray<rexObjectGenericBound*> boundList;

	for ( int a = 0; a < childCount; ++a )
	{
		rexObject* child = fragments.m_ContainedObjects[ a ];

		if (const rexObjectGenericBound* bound = dynamic_cast<rexObjectGenericBound*>(child))
		{
			if (parentBoneIndex == 0)
			{
				parentBoneIndex = bound->m_BoneIndex;
			}
			else if (parentBoneIndex != bound->m_BoneIndex)
			{
				Warningf("Bones %d and %d both have things attached to them under %s", parentBoneIndex, bound->m_BoneIndex, fragments.GetName());
			}
		}

		//eConsiderTroolean considerOrphanedBounds = m_SkipBoundSectionOfTypeFile ? CONSIDER_ALL : CONSIDER_IFNOT;
		// gunnarD: Changed this to always use all bounds that are defined for fragments in the fragment section. 
		// Cloth bounds are going to be duplicate and they have to
		GetBounds( *child, boundList, false, CONSIDER_ALL );
	}

	atArray< atArray< atArray<rexObjectGenericMesh*> > > meshesByLODGroupAndLevel;
	meshesByLODGroupAndLevel.PushAndGrow(fragments.m_MeshesByLODLevel);
	atArray< atArray< atArray<rexObjectGenericMesh::sThresholdPair> > > threshsByLODGroupAndLevel;
	threshsByLODGroupAndLevel.PushAndGrow(fragments.m_ThresholdsByLODLevel);
	int numValidMeshes = GetNumValidMeshes(meshesByLODGroupAndLevel, "fragments", false);

	if (boundList.GetCount() > 0 || 
		(!m_SkipBoundSectionOfTypeFile && numValidMeshes > 0)) // ignore bounds if already written to own section
	{
		childBoundWritten = true;

		WriteGroupAndChildHeader( fragments.GetName(), s, fragments.GetDamageState(), depth );

		if(boundList.GetCount() > 0)
		{
			// Decrease the depth for writing out damage states
			if (fragments.GetDamageState())
			{
				depth -= 1;
			}
#if !HACK_GTA4
			//open
			buffer[0] = '\0';
			Indent( buffer, depth + 2 );
			strcat( buffer, "bound {\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );
#endif // !HACK_GTA4
			WriteBoundList( boundList, s, depth + 2, NULL, "bound" );

#if !HACK_GTA4
			//close
			buffer[0] = '\0';
			Indent( buffer, depth + 2 );
			strcat( buffer, "}\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );
#endif // HACK_GTA4
		}
	}

	// First, write out all the child meshes of this group...
	if (
		((GetFragmentWriteMeshes() && childBoundWritten) || !m_SkipBoundSectionOfTypeFile)
		&& fragments.m_MeshesByLODLevel.GetCount() > 0)
	{
		WriteLODGroups( meshesByLODGroupAndLevel, threshsByLODGroupAndLevel, s, "fragments", depth + 2, eIndexMeshBone, false, OPTIMIZE_TRUE, m_SplitFlagGroupsToDrawables, NULL, m_AllowEmptyFragSection);
	}

	// Write out the edge models
	bool childEdgeWritten = false;

	if (childBoundWritten)
	{
		for ( int a = 0; a < childCount; ++a )
		{
			rexObject* child = fragments.m_ContainedObjects[ a ];

			if (dynamic_cast<rexObjectGenericBound*>(child) == NULL)
			{
				if (rexObjectGenericMesh* edge = dynamic_cast<rexObjectGenericMesh*>(child))
				{
					if (parentBoneIndex == 0)
					{
						parentBoneIndex = edge->m_BoneIndex;
					}
					else if (parentBoneIndex != edge->m_BoneIndex)
					{
						Warningf("Bones %d and %d both have things attached to them under %s", parentBoneIndex, edge->m_BoneIndex, fragments.GetName());
					}

					if (childEdgeWritten)
					{
						Warningf("Child edge model %s ignored! More than one edge model at fragmentation point.", edge->GetMeshName());
					}
					else
					{
						childEdgeWritten = true;

						buffer[0] = '\0';
						Indent(buffer, depth + 2);
						strcat(buffer, "edge {\r\n");
						s->Write( buffer, (int)strlen( buffer ) );		

						buffer[0] = '\0';
						Indent(buffer, depth + 3);
						strcat(buffer, "edge ");
						strcat(buffer, edge->GetMeshName());
						strcat(buffer, ".em\r\n");
						s->Write( buffer, (int)strlen( buffer ) );		

						buffer[0] = '\0';
						Indent(buffer, depth + 2);
						strcat(buffer, "}\r\n");
						s->Write( buffer, (int)strlen( buffer ) );		
					}
				}
			}
		}
	}

	if (childBoundWritten)
	{
		if (fragments.GetDamageState())
		{
			depth += 1;
		}
		else
		{
			if (parentBoneIndex > 0)
			{
				// Write the parent bone index
				buffer[0] = '\0';
				Indent(buffer, depth + 2);
				strcat(buffer, "parentBoneIndex bone ");

				char boneBuffer[256];
				_itoa(parentBoneIndex, boneBuffer, 10);
				strcat(buffer, boneBuffer);
				strcat(buffer, "\r\n");
				s->Write( buffer, (int)strlen( buffer ) );		
			}

			// Close child {
			buffer[0] = '\0';
			Indent(buffer, depth + 1);
			strcat(buffer, "}\r\n");
			s->Write( buffer, (int)strlen( buffer ) );
		}
	}

	// ...finally, write out groups that are children of this group
	for ( int a = 0; a < childCount; ++a )
	{
		rexObject* child = fragments.m_ContainedObjects[ a ];

		if (rexObjectGenericFragmentHierarchy* childGroup = dynamic_cast<rexObjectGenericFragmentHierarchy*>(child))
		{
			AssertMsg(!fragments.GetDamageState(), "Damage states are not allowed to have children.");
			int childGroupDepth = depth;
			if (childBoundWritten || depth == 0)
			{
				++childGroupDepth;
			}
//			if(childGroup->m_MeshesByLODLevel.GetCount()>0)
				WriteFragmentHierarchy(*childGroup, s, childGroupDepth);
		}
	}

	if (childBoundWritten)
	{
		if(fragments.IsBreakableGlass())
		{
			buffer[0] = '\0';
			Indent(buffer, depth+1);
			strcat(buffer,"madeOfGlass\r\n");
			s->Write( buffer, (int)strlen( buffer ) );
		}

		if(fragments.m_GlassType != "")
		{
			buffer[0] = '\0';
			Indent(buffer, depth+1);
			strcat(buffer,"GlassType ");
			strcat(buffer,fragments.m_GlassType.c_str());
			strcat(buffer,"\r\n");
			s->Write( buffer, (int)strlen( buffer ) );
		}

		// Close group {
		buffer[0] = '\0';
		Indent(buffer, depth);
		strcat(buffer, "}\r\n");
		s->Write( buffer, (int)strlen( buffer ) );		
	}
}

void GetFragHierObjectsRecursive(rexObjectGenericFragmentHierarchy* pCurrent, atArray<rexObjectGenericFragmentHierarchy*>& fragHierList, atArray<atString>& meshNames)
{
	int count = pCurrent->m_ContainedObjects.GetCount();
	for(int i=0; i<count; i++)
	{
		rexObjectGenericFragmentHierarchy* pFragHier = dynamic_cast<rexObjectGenericFragmentHierarchy*>(pCurrent->m_ContainedObjects[i]);
		if(pFragHier)
		{
			fragHierList.PushAndGrow(pFragHier);
			GetFragHierObjectsRecursive(pFragHier, fragHierList, meshNames);
		}
	}

	atArray< atArray< rexObjectGenericMesh*> >::iterator outIt = pCurrent->m_MeshesByLODLevel.begin();
	for(; outIt!= pCurrent->m_MeshesByLODLevel.end();outIt++)
	{
		atArray< rexObjectGenericMesh*>::iterator innerIt = (*outIt).begin();
		for(; innerIt!= (*outIt).end();innerIt++)
			meshNames.PushAndGrow((*innerIt)->GetMeshName());
	}
}

rexResult rexSerializerRAGEEntity::WriteFragmentInfo( rexObject& object, fiStream* s )
{
	rexResult result = rexResult::PERFECT;
	if( !s )
		return rexResult::ERRORS;

	rexObjectGenericFragmentHierarchy* inputFragmentHier = dynamic_cast<rexObjectGenericFragmentHierarchy*>( &object );

	if( !inputFragmentHier )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	int childCount = inputFragmentHier->m_ContainedObjects.GetCount();

	char buffer[512];
	if( childCount )
	{
		// Form the fragment children into a hierarchy mimicking the DAG hierarchy in Maya
		m_Fragments.m_DeleteContainedObjectsOnDestruction = false;

		for( int a = 0; a < childCount; a++ )
		{
			if (rexObjectGenericFragmentHierarchy* fragmentHier = dynamic_cast<rexObjectGenericFragmentHierarchy*>(inputFragmentHier->m_ContainedObjects[ a ]) )
			{
				// NOTE: None of them have any children when they come in here, so we know
				// we set up any children that they have, for cleanup at the end of the function
				Assert(fragmentHier->m_ContainedObjects.GetCount() == 0);

				if ( rexObjectGenericFragmentHierarchy* bestParent = FindFragmentParent(fragmentHier->GetFullPath(), m_Fragments) )
				{
					bestParent->m_ContainedObjects.PushAndGrow(fragmentHier);
				}
			}
		}

		// Only proceed if this is not a damaged state fragment object
		if (!m_FragmentHasDamage ||
			!rexObjectGenericFragmentHierarchy::GetDamageStateExists() || 
			 inputFragmentHier->GetDamageState())
		{
			// Put the bounds into the fragment hierarchy
			int boundCount = m_BoundList.GetCount();
			for( int bound = 0; bound < boundCount; ++bound )
			{
				if ( rexObjectGenericFragmentHierarchy* bestParent = FindFragmentParent(m_BoundList[ bound ]->m_FullPath, m_Fragments) )
				{
					bestParent->m_ContainedObjects.PushAndGrow(m_BoundList[ bound ]);
				}
			}

			// Then put the edge models into the fragment hierarchy
			int edgeModelCount = m_EdgeModelList.GetCount();
			for( int edgeModel = 0; edgeModel < edgeModelCount; ++edgeModel )
			{
				if ( rexObjectGenericFragmentHierarchy* bestParent = FindFragmentParent(m_EdgeModelList[ edgeModel ]->m_FullPath, m_Fragments) )
				{
					bestParent->m_ContainedObjects.PushAndGrow(m_EdgeModelList[ edgeModel ]);
				}
			}

			//for all groups
			for( int group = 0; group < m_MeshesByLODGroupAndLevel.GetCount(); group++ )
			{
				atArray< atArray< rexObjectGenericMesh*> > &meshesByLODLevel = m_MeshesByLODGroupAndLevel[group];
				// Then put the meshes in the fragment hierarchy
				int lodLevels = meshesByLODLevel.GetCount();

				for( int lod = 0; lod < lodLevels; ++lod )
				{
					for (int mesh = 0; mesh < meshesByLODLevel[ lod ].GetCount(); ++mesh)
					{
						if ( rexObjectGenericFragmentHierarchy* bestParent = FindFragmentParent(meshesByLODLevel[ lod ][ mesh ]->m_FullPath, m_Fragments) )
						{
							if ( bestParent->m_MeshesByLODLevel.GetCount() == 0 )
							{
								bestParent->m_MeshesByLODLevel.Resize(lodLevels);
							}
							bestParent->m_MeshesByLODLevel[lod].PushAndGrow(meshesByLODLevel[ lod ][ mesh ]);

							if ( bestParent->m_ThresholdsByLODLevel.GetCount() == 0 )
							{
								bestParent->m_ThresholdsByLODLevel.Resize(lodLevels);
							}
							// only one threshold per group
							rexObjectGenericMesh::sThresholdPair &lodValuePair = m_ThresholdsByLODGroupAndLevel[group][ lod ][ 0 ];
							bestParent->m_ThresholdsByLODLevel[lod].PushAndGrow(lodValuePair);
						}
					}
				}
			}

			// Write out the fragment data
			sprintf( buffer, "fragments {\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );		
#if HACK_GTA4
			if (m_FragmentForceLoadCommonDrawable)
			{
				sprintf( buffer, "\tforceLoadCommonDrawable 1\r\n" );
				s->Write( buffer, (int)strlen( buffer ) );		
			}
#endif // HACK_GTA4

			int currpos = s->Tell();
			WriteFragmentHierarchy(*dynamic_cast<rexObjectGenericFragmentHierarchy*>(m_Fragments.m_ContainedObjects[ 0 ]), s);

			atArray<rexObjectGenericFragmentHierarchy*> fragHierList;
			atArray<atString> meshNames;
			GetFragHierObjectsRecursive(&m_Fragments, fragHierList, meshNames);

			if(!m_AllowEmptyFragSection && currpos==s->Tell())
			{
				atString ctxString;
				for(int k=0;k<meshNames.size();k++)
				{
					if(k>0)
						ctxString = atString(ctxString, ",");
					ctxString = atString(ctxString, meshNames[k].c_str());
				}
				atString errMsg(atString("Fragment serialisation failed. No collision on Object(s) "), ctxString);
				errMsg = atString(errMsg, "?");
				result.AddErrorCtx(ctxString, errMsg);
				result.Combine(rexResult::ERRORS);
			}


			sprintf( buffer, "}\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );		

			// Clean up the extra children we gave to the input objects...otherwise the shell will do it for us,
			// and it won't know not to call delete on them!
			/*
			for( int a = 0; a < childCount; a++ )
			{
				while (inputFragmentHier->m_ContainedObjects.GetCount())
				{
					inputFragmentHier->m_ContainedObjects.Pop();
				}
			}
			*/
			
			//Clean up the extra children that have been assigned to the input objects.
			for(int i=0; i<fragHierList.GetCount(); i++)
			{
				while(fragHierList[i]->m_ContainedObjects.GetCount())
					fragHierList[i]->m_ContainedObjects.Pop();
			}
			
		}	
	}

	return result;
}

rexResult rexSerializerRAGEEntity::WriteHairGroupInfo( rexObject& object, fiStream* s)
{
	rexResult result(rexResult::PERFECT);
	char buffer[512];

	if( !s )
		return rexResult::ERRORS;

	rexObjectGenericHairMeshGroup* pHairGroup = dynamic_cast<rexObjectGenericHairMeshGroup*>( &object );
	if(!pHairGroup)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	//Write the hair information out
	sprintf(buffer, "haircomb {\r\n");
	s->Write( buffer, (int)strlen( buffer ) );

	int pieceCount = pHairGroup->m_ContainedObjects.GetCount();
	for( int i=0; i<pieceCount; i++)
	{
		rexObjectGenericHairMeshInfo* pHairPiece = dynamic_cast<rexObjectGenericHairMeshInfo*>( pHairGroup->m_ContainedObjects[i] );
		if(!pHairPiece)
		{
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}

		//Get the index of the mesh for the hair piece
		int hairIdx = -1;
		int meshCount = 0;
		for(int h=0; h<m_MeshesByLODGroupAndLevel.GetCount(); h++)
		{
			for(int j=0; j<m_MeshesByLODGroupAndLevel[h].GetCount(); j++)
			{
				for(int k=0; k<m_MeshesByLODGroupAndLevel[h][j].GetCount(); k++)
				{
					rexObjectGenericMesh* pMesh = m_MeshesByLODGroupAndLevel[h][j][k];
					if(pMesh->GetMeshName() == pHairPiece->m_HairMeshName)
					{
						hairIdx = meshCount;
						break;
					}
					meshCount++;
				}
				if(hairIdx != -1)
					break;
			}
		}

		sprintf( buffer, "\t%s {\r\n", (const char*)pHairPiece->GetName());
		s->Write( buffer, (int)strlen(buffer) );

		sprintf( buffer, "\t\tmesh %s.mesh %d\r\n", (const char*)pHairPiece->m_HairMeshName, hairIdx);
		s->Write( buffer, (int)strlen(buffer) );

		sprintf( buffer, "\t}\r\n");
		s->Write( buffer, (int)strlen(buffer) );
	}
	
	sprintf( buffer, "}\r\n");
	s->Write(buffer, (int)strlen(buffer) );

	return result;

}

rexResult rexSerializerRAGEEntity::WriteClothGroupInfo( rexObject& object, fiStream* s)
{
	rexResult result(rexResult::PERFECT);
	char buffer[512];

	if( !s )
		return rexResult::ERRORS;

	rexObjectGenericClothMeshGroup* pClothGroup = dynamic_cast<rexObjectGenericClothMeshGroup*>( &object );
	if(!pClothGroup)
	{
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	if(!pClothGroup->m_ContainedObjects.GetCount())
		return rexResult::PERFECT;

	bool isCharCloth = false;
	rexObjectGenericCharClothMesh* pCharClothGroup = dynamic_cast<rexObjectGenericCharClothMesh*>( &object );
	if(pCharClothGroup)
	{
		isCharCloth = true;
		WriteComment(s, "Character Cloth group: Contains render/simulation meshes in that order. No LOD info.");
	}
	else
		WriteComment(s, "Environment/vehicle Cloth group: Contains only simulation meshes with LOD info.");

//Write the cloth information out
	sprintf( buffer, "%s {\r\n", pClothGroup->GetGroupTag());
	s->Write( buffer, (int)strlen( buffer ) );

	int pieceCount = pClothGroup->m_ContainedObjects.GetCount();
	for( int i=0; i<pieceCount; i++)
	{
		rexObjectGenericClothMeshInfo* pClothPiece = dynamic_cast<rexObjectGenericClothMeshInfo*>( pClothGroup->m_ContainedObjects[i] );
		if(!pClothPiece)
		{
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}

		atArray< atArray< atArray< rexObjectGenericMesh*> > >	clothMeshesByLODGroupAndLevel;
		atArray< atArray< atArray< rexObjectGenericMesh::sThresholdPair > > >		clotThresholdsByLODGroupAndLevel;
		atArray< rexObjectGenericMesh* >				nonLODMeshes;
		atArray< rexObjectGenericMesh::MaterialInfo >	matInfoArray;
		pClothPiece->GetLODsMeshesAndMaterials(clothMeshesByLODGroupAndLevel, clotThresholdsByLODGroupAndLevel, nonLODMeshes, matInfoArray );
	
		int boneCount = pClothPiece->m_BoneNames.GetCount();
		char boneString[4096];
		sprintf( boneString, "boneCount %d\r\n", boneCount);
		for(int j=0; j<boneCount; j++)
		{
			char oneBone[512];
			sprintf( oneBone, "\t\t\tbone %s\r\n", (const char*)pClothPiece->m_BoneNames[j]);
			strcat( boneString, oneBone);
		}

		if(isCharCloth)
			WriteMeshGroups(clothMeshesByLODGroupAndLevel, s, pClothPiece->GetName().c_str(), 0, boneString, eIndexMeshBone);
		else
			WriteLODGroups(clothMeshesByLODGroupAndLevel, clotThresholdsByLODGroupAndLevel, s, pClothPiece->GetName().c_str(), 1, eIndexModelInMesh, false, OPTIMIZE_NOWRITE, false, boneString);
	}

	sprintf( buffer, "}\r\n");
	s->Write(buffer, (int)strlen( buffer ) );

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////

rexSerializerRAGEEntity::rexSerializerRAGEEntity() : rexSerializerGenericContainer()
	, m_SkipBoundSectionOfTypeFile(false)
	, m_ComposeBounds(false)
	, m_FragmentHasDamage(false)
	, m_FragmentWriteMeshes(false)
	, m_WriteBoundSituations(false)
	, m_WriteBoundFlags(false)
	, m_CompositeFileCount(0)
	, m_WriteLocatorsAsSeparateFiles(false)
	, m_WriteMeshGroupIndex(false)
	, m_WriteShadingGroup(true)
	, m_WriteSeparateShadingGroupFile(false)
	, m_SplitLODGroupsByFlags(false)
	, m_SplitFlagGroupsToDrawables(false)
	, m_UseNonOptimizedMTLCompare(false)
	, m_UseLODMeshTypes(false)
	, m_SerializeVersionTwoNurbsCurves(false)
	, m_ErrorOnClothWithoutVertexTuning(false)
	, m_GroupBoundsByBone(false)
	, m_AllowEmptyFragSection(false)
#if HACK_GTA4
	, m_FragmentForceLoadCommonDrawable(false)
	, m_OptimiseMaterials(true)
#endif // HACK_GTA4
{
	// these are the data types supported by base rmcore drawable.  other modules can be added by 
	// other projects

	rexObjectGenericMeshHierarchyDrawable mhdType;
	RegisterObjectType( mhdType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteLODAndShaderGroupInfo) );

	rexObjectGenericSkeleton skelType;
	RegisterObjectType( skelType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteSkeletonAndBoneTagInfo) );

	rexObjectGenericAnimation animType;
	RegisterObjectType( animType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteAnimationInfo) );

	rexObjectGenericLocatorGroup locatorGroupType;
	RegisterObjectType( locatorGroupType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteLocatorInfo) );

	rexObjectGenericSoundLocatorGroup locatorSoundGroupType;
	RegisterObjectType( locatorSoundGroupType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteLocatorInfo) );

	rexObjectGenericCharacterInstanceLocatorGroup locatorCharacterInstanceGroupType;
	RegisterObjectType( locatorCharacterInstanceGroupType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteLocatorInfo) );

	rexObjectGenericEdgeModelHierarchy edgeModelType;
	RegisterObjectType( edgeModelType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteEdgeModelInfo) );

	rexObjectGenericBoundHierarchy boundHierType;
	RegisterObjectType( boundHierType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteBoundInfo) );

	rexObjectGenericFragmentHierarchy fragmentHierType;
	RegisterObjectType( fragmentHierType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteFragmentInfo) );

	rexObjectGenericLightGroup lightType;
	RegisterObjectType( lightType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteLightInfo) );

	rexObjectGenericNurbsCurveGroup curveType;
	RegisterObjectType( curveType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteCurveInfo) );

	rexObjectGenericClothMeshGroup clothGroupType;
	RegisterObjectType( clothGroupType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteClothGroupInfo) );

	rexObjectGenericCharClothMesh charClothGroupType;
	RegisterObjectType( charClothGroupType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteClothGroupInfo) );

	rexObjectGenericHairMeshGroup hairGroupType;
	RegisterObjectType( hairGroupType, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteHairGroupInfo) );

	rexObjectGenericMeshHierarchyRoomVolumes roomVolumes;
	RegisterObjectType( roomVolumes, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteLevelRoomVolumes) );

	rexObjectGenericMeshHierarchyPortals portals;
	RegisterObjectType( portals, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WriteLevelPortals) );

	rexObjectGenericPrimitiveGroup primitives;
	RegisterObjectType( primitives, MakeFunctorRet(*this, &rexSerializerRAGEEntity::WritePrimitiveInfo) );

	m_UseChildrenOfLevelInstanceNodes = false;
	//RegisterObjectType<rexObjectGenericEdgeModel*>( WriteInfoForEdgeModel );
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyEntityAllowEmptyFragSection::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetAllowEmptyFragSection( value.toBool( ser->GetAllowEmptyFragSection() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////

void rexSerializerRAGEEntity::AddTextureSet( const atArray< atString >& textureNames )
{
	Assert( (!m_TextureSets.GetCount()) || ( textureNames.GetCount() == m_TextureSets[ 0 ].GetCount() ) );
	m_TextureSets.Grow() = textureNames;
}

////

rexResult  rexPropertyEntityAddTextureSet::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		atArray< atString > textureNames;

		char buffer[ 4096 ];
		strcpy( buffer, value.toString() );

		int len = (int)strlen( buffer );

		if( len )
		{
			int lastSemiColon = -1;

			for( int a = 0; a < len; a++ )
			{
				if( buffer[ a ] == ';' )
				{
					buffer[ a ] = 0;
					textureNames.Grow() = &buffer[ lastSemiColon + 1 ];
					lastSemiColon = a;
				}
			}
			if( (int)strlen( &buffer[ lastSemiColon + 1 ] ) )
				textureNames.Grow() = &buffer[ lastSemiColon + 1 ];
		}

		ser->AddTextureSet( textureNames );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyEntityComposeBounds::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetComposeBounds( value.toBool( ser->GetComposeBounds() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyEntityFragmentHasDamage::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetFragmentHasDamage( value.toBool( ser->GetFragmentHasDamage() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyEntityFragmentWriteMeshes::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetFragmentWriteMeshes( value.toBool( ser->GetFragmentWriteMeshes() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyEntityWriteBoundSituations::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetWriteBoundSituations( value.toBool( ser->GetWriteBoundSituations() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyEntityBoundListName::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetBoundListName( atString(value.toString()) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyEntityWriteBoundFlags::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetWriteBoundFlags( value.toBool( ser->GetWriteBoundFlags() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyEntityGroupBoundsByBone::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetGroupBoundsByBone( value.toBool( ser->GetGroupBoundsByBone() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyEntityWriteLocatorsAsSeparateFiles::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetWriteLocatorsAsSeparateFiles( value.toBool( ser->GetWriteLocatorsAsSeparateFiles() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyEntityWriteCurvesAsSeparateFiles::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetWriteCurvesAsSeparateFiles( value.toBool( ser->GetWriteCurvesAsSeparateFiles() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyEntityWriteMeshGroupIndex::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetWriteMeshGroupIndex( value.toBool( ser->GetWriteMeshGroupIndex() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyEntityWriteShadingGroup::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetWriteShadingGroup( value.toBool( ser->GetWriteShadingGroup() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyEntityWriteSeparateShadingGroupFile::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetWriteSeparateShadingGroupFile( value.toBool( ser->GetWriteSeparateShadingGroupFile() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyEntitySplitLODGroupsByFlags::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetSplitLODGroupsByFlags( value.toBool( ser->GetSplitLODGroupsByFlags() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

#if HACK_GTA4
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyFragmentForceLoadCommonDrawable::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetFragmentForceLoadCommonDrawable( value.toBool( ser->GetFragmentForceLoadCommonDrawable() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}
#endif // HACK_GTA4


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyBoundSpecialFlagMappings::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		//(btMoverOnly=MoverOnly|btCoverOnly=CoverOnly|btCameraOnly=CameraOnly|btShootOnly=HiRes)
		if (value.toString())
		{
			std::string types = value.toString();
			size_t startParen = types.find_first_of('(');
			size_t endParen = types.find_last_of(')');
			if( (startParen == std::string::npos) ||
				(endParen == std::string::npos) )
			{
				return rexResult( rexResult::WARNINGS, rexResult::INVALID_INPUT );
			}
			size_t charIndex = startParen+1;
			while(charIndex < endParen)
			{
				atString materialFlag("");
				atString specialFlag("");
				atString builtString("");
				while( (types[charIndex] != '|') &&
					(types[charIndex] != ')') )
				{
					if (types[charIndex] == '=')
					{
						materialFlag = builtString;
						builtString = "";
						charIndex++;
					}
					else
					{
						builtString += types[charIndex++];
					}					
				}
				specialFlag = builtString;
				ser->AddBoundSpecialFlagMapping(materialFlag, specialFlag);
				charIndex++;
			}
			return rexResult( rexResult::PERFECT );
		}
		else
		{
			return rexResult( rexResult::WARNINGS, rexResult::INVALID_INPUT );
		}
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyBoundGeometryExportType::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		if (value.toString())
		{
			atString exporttype(value.toString());
			ser->SetBoundGeometryExportType(exporttype);
			return rexResult( rexResult::PERFECT );
		}
		else
		{
			return rexResult( rexResult::WARNINGS, rexResult::INVALID_INPUT );
		}
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyUseNonOptimizedMTLCompare::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetUseNonOptimizedMTLCompare( value.toBool( ser->GetUseNonOptimizedMTLCompare() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyUseLODMeshTypes::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetUseLODMeshTypes( value.toBool( ser->GetUseLODMeshTypes() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyLODMeshFilterMappings::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );
	if( ser )
	{
		//(high=lod01|med=lod02)
		if (value.toString())
		{
			std::string types = value.toString();
			size_t startParen = types.find_first_of('(');
			size_t endParen = types.find_last_of(')');
			if( (startParen == std::string::npos) ||
				(endParen == std::string::npos) )
			{
				return rexResult( rexResult::WARNINGS, rexResult::INVALID_INPUT );
			}
			size_t charIndex = startParen+1;
			while(charIndex < endParen)
			{
				atString materialFlag("");
				atString specialFlag("");
				atString builtString("");
				while( (types[charIndex] != '|') &&
					(types[charIndex] != ')') )
				{
					if (types[charIndex] == '=')
					{
						materialFlag = builtString;
						builtString = "";
						charIndex++;
					}
					else
					{
						builtString += types[charIndex++];
					}					
				}
				specialFlag = builtString;
				ser->AddLODMeshSpecialFlagMapping(materialFlag, specialFlag);
				charIndex++;
			}
			return rexResult( rexResult::PERFECT );
		}
		else
		{
			return rexResult( rexResult::WARNINGS, rexResult::INVALID_INPUT );
		}
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertySerializeVersionTwoNurbsCurves::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetSerializeVersionTwoNurbsCurves( value.toBool( ser->GetSerializeVersionTwoNurbsCurves() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyErrorOnClothWithoutVertexTuning::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetErrorOnClothWithoutVertexTuning( value.toBool( ser->GetErrorOnClothWithoutVertexTuning() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyEntitySplitFlagGroupsToDrawables::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* ser = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );

	if( ser )
	{
		ser->SetSplitFlagGroupsToDrawables( value.toBool( ser->GetSplitFlagGroupsToDrawables() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 
