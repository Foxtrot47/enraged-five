#include "serializerLevel.h"

#include "serializerNurbsCurve.h"

#include "rexBase/result.h"
#include "rexBase/shell.h"
#include "rexBase/object.h"
#include "rexBase/objectAdapter.h"
#include "rexBase/utility.h"
#include "rexGeneric/objectAnimation.h"
#include "rexGeneric/objectBound.h"
#include "rexGeneric/objectFX.h"
#include "rexGeneric/objectLevel.h"
#include "rexGeneric/objectLevelInstance.h"
#include "rexGeneric/objectLight.h"
#include "rexGeneric/objectLocator.h"
#include "rexGeneric/objectMesh.h"
#include "rexGeneric/objectNurbsCurve.h"
#include "rexGeneric/objectSkeleton.h"

#include "file/stream.h"
#include "file/asset.h"

#include "atl/array.h"
#include "atl/map.h"

using namespace rage;

static	atArray< atArray< atArray< rexObjectGenericMesh *> > >	sMeshesByLODGroupAndLevel;
static	atArray< rexObjectGenericMesh* >						sNonLODMeshes;
static	atArray< rexObjectGenericMesh::MaterialInfo >			sMatInfoArray;
static	atArray< atArray< atArray< rexObjectGenericMesh::sThresholdPair > > >	sThresholdsByLODGroupAndLevel;
static	atArray<int>											sLodGroupDepths, sLodGroupChildIndices;

static Vector3 sBoundSphereCenter(0,0,0);
float sBoundSphereRadius=0.0f;

///////////////////////////////////////////////////////////////////////////////////////////

bool rexSerializerRmworldDistrict::DistrictExportFinalize( const char* /* levelName */) const
{
	return true;
}

#if 0
static void WriteProxyInfo( rexObject& district, fiStream* fiStream )
{
	Assert( fiStream );
	int childCount = district.m_ContainedObjects.GetCount();
	for( int c = 0; c < childCount; c++ )
	{
		rexObjectRmworldDistrictProxyMeshHierarchy* proxy = dynamic_cast<rexObjectRmworldDistrictProxyMeshHierarchy*>( district.m_ContainedObjects[ c ] );
		if( proxy )
		{
			int proxyChildCount = proxy->m_ContainedObjects.GetCount();
			for( int a = 0; a < proxyChildCount; a++ )
			{
				rexObjectGenericMesh* mesh = dynamic_cast<rexObjectGenericMesh*>( proxy->m_ContainedObjects[ a ] );
				if( mesh && mesh->m_IsValid )
				{
					char buffer[256];
					sprintf( buffer, "\tProxy\r\n\t{\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\tmesh %s.mesh\r\n", (const char*)mesh->m_Name );
					fiStream->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\tmeshType Drawable\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\tMatrix\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "\t\t{\r\n" );	
					fiStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "\t\t\t1.0 0.0 0.0 0.0\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "\t\t\t0.0 1.0 0.0 0.0\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "\t\t\t0.0 0.0 1.0 0.0\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "\t\t\t%f %f %f 1.0\r\n", mesh->m_Center.x, mesh->m_Center.y, mesh->m_Center.z );
					fiStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "\t\t}\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );

					int materialCount = mesh->m_Materials.GetCount();

					sprintf( buffer, "\t\tShaderGroup\r\n\t\t{\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\t\tVersion: 103\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\t\tShadingGroup {\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\t\t\tShadingGroup {\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );

					sprintf( buffer, "\t\t\t\t\tCount 1\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );


					sprintf( buffer, "\t\t\t\t\tShaders %d\r\n\t\t\t\t\t{\r\n", materialCount );
					fiStream->Write( buffer, (int)strlen( buffer ) );

					for( int m = 0; m < materialCount; m++ )
					{
						sprintf( buffer, "\t\t\t\t\t\t%s ", (const char*)mesh->m_Materials[ m ].m_TypeName );
						fiStream->Write( buffer, (int)strlen( buffer ) );

						int attrCount = mesh->m_Materials[ m ].m_AttributeValues.GetCount();
						sprintf( buffer, "%d ", attrCount );
						fiStream->Write( buffer, (int)strlen( buffer ) );			

						for( int b = 0; b < attrCount; b++ )
						{
							sprintf( buffer, "%s ", mesh->m_Materials[ m ].m_AttributeValues[ b ].toString() );
							fiStream->Write( buffer, (int)strlen( buffer ) );			
						}
						sprintf( buffer, "\r\n" );
						fiStream->Write( buffer, (int)strlen( buffer ) );			
					}

					sprintf( buffer, "\t\t\t\t\t}\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "\t\t\t\t}\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "\t\t\t}\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "\t\t}\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );
					sprintf( buffer, "\t}\r\n" );
					fiStream->Write( buffer, (int)strlen( buffer ) );

					// currently only one mesh allowed
					break;
				}

			}
			break;
		}
	}
}
#endif

rexResult rexSerializerRmworldDistrict::Serialize( rexObject& object ) const
{	
	rexResult result( rexResult::PERFECT );

	// type checking
	rexObjectRmworldDistrict* inputLevel = dynamic_cast<rexObjectRmworldDistrict*>( &object );
	if( inputLevel == NULL )
	{
		return result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	////////////////////////////////////

	ASSET.SetPath( GetOutputPath() );

	// write to our fiStream	
	rexUtility::CreatePath( GetOutputPath() );
	fiStream* myStream = GetExportData() ? ASSET.Create( /*inputLevel->GetName()*/"level", "world" ) : NULL;

	sBoundSphereCenter.Zero();
	sBoundSphereRadius = 0.0f;

	int childCount = inputLevel->m_ContainedObjects.GetCount();

	atArray<Vector3,0,unsigned> verts;

	for( int a = 0; a < childCount; a++ )
	{
		rexObjectGenericMeshHierarchyDrawable* meshHier = dynamic_cast< rexObjectGenericMeshHierarchyDrawable* >( inputLevel->m_ContainedObjects[ a ] );
		if( meshHier )
		{
			atArray< atArray< atArray< rexObjectGenericMesh* > > >	meshesByLODGroupAndLevel;
			atArray< rexObjectGenericMesh* >						nonLODMeshes;
			atArray< rexObjectGenericMesh::MaterialInfo >			matInfoArray;
			atArray< atArray< atArray< rexObjectGenericMesh::sThresholdPair > > >					thresholdsByLODGroupAndLevel;

			meshHier->GetLODsMeshesAndMaterials( meshesByLODGroupAndLevel, thresholdsByLODGroupAndLevel, nonLODMeshes, matInfoArray );
			
			int c1 = meshesByLODGroupAndLevel.GetCount();
			for( int mg = 0; mg < c1; mg++ )
			{
				int c2 = meshesByLODGroupAndLevel[ mg ].GetCount();
				for( int ml = 0; ml < c2; ml++ )
				{
					int c3 = meshesByLODGroupAndLevel[ mg ][ ml ].GetCount();
					for( int mm = 0; mm < c3; mm++ )
					{
						rexObjectGenericMesh* mesh = meshesByLODGroupAndLevel[ mg ][ ml ][ mm ];
						Assert( mesh );
						mesh->GetVertexPositions( verts, true );
					}
				}
			}
			c1 = nonLODMeshes.GetCount();
			for( int m = 0; m < c1; m++ )
			{
				rexObjectGenericMesh* mesh = nonLODMeshes[ m ];
				Assert( mesh );
				mesh->GetVertexPositions( verts, true );
			}

			if( verts.GetCount() )
				rexUtility::MakeOptimizedBoundingSphere( verts, sBoundSphereCenter, sBoundSphereRadius );

			break;
		}
	}

	if( GetExportData() && !myStream )
	{
		result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"level","world" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		result.AddError( "Could not create file '%s'", acFullPathForDebugging);
	}
	else 
	{		
		if( myStream )
		{
			atString verString( "LEVEL\r\n{\r\n" );
			myStream->Write( verString, verString.GetLength() );		

			char buffer[512];

			sprintf( buffer, "\tBoundingSphere %f %f %f %f\r\n", sBoundSphereCenter.x, sBoundSphereCenter.y, sBoundSphereCenter.z, sBoundSphereRadius  );
			myStream->Write( buffer, (int)strlen( buffer ) );

			// TODO WriteProxyInfo( object, myStream );
		}

		result &= SerializeRecursive( object, myStream );	

		if( myStream )
		{
			atString verString( "}\r\n" );
			myStream->Write( verString, verString.GetLength() );		
		}
	}

	if( myStream )
		myStream->Close();
	if(rexShell::DoesntSerialise())
	{
		const char *filename = myStream->GetName();
		result.AddMessage("Deleting file %s due to no-serialise flag.", filename);
		remove(filename);
	}

	return result;
}

void WriteFXObjects( const atArray<rexObject*>& fxList )
{

	if(rexShell::DoesntSerialise())
	{
		return;
	}


	fiStream* s = ASSET.Create( "district", "fx" );
	if( !s )
	{
		return;
	}

	char buffer[512];

	int fxCount = fxList.GetCount();

	sprintf( buffer, "count %d\r\n", fxCount );
	s->Write( buffer, (int)strlen( buffer ) );

	for( int a = 0; a < fxCount; a++ )
	{
		const rexObjectRmworldFX* fx = dynamic_cast<const rexObjectRmworldFX*>( fxList[ a ] );
		Assert( fx );

		int locCount = fx->m_LocatorNames.GetCount();
		int curveCount = fx->m_CurveNames.GetCount();
		sprintf( buffer, "%s locators %d", (const char*)fx->GetName(), locCount );
		s->Write( buffer, (int)strlen( buffer ) );

		for( int b = 0; b < locCount; b++ )
		{
			sprintf( buffer, " %s", (const char*)fx->m_LocatorNames[ b ] );
			s->Write( buffer, (int)strlen( buffer ) );
		}

		sprintf( buffer, " curves %d", curveCount );
		s->Write( buffer, (int)strlen( buffer ) );

		for( int b = 0; b < curveCount; b++ )
		{
			sprintf( buffer, " %s", (const char*)fx->m_CurveNames[ b ] );
			s->Write( buffer, (int)strlen( buffer ) );
		}

		sprintf( buffer, "\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );
	}

	s->Close();
}

rexResult rexSerializerRmworldDistrict::SerializeRecursive( rexObject& object, fiStream* myStream ) const
{
	rexResult result( rexResult::PERFECT );

	if( myStream )
	{
		if( m_ProgressBarTextCB )
		{
			static char message[1024];
			sprintf( message, "Serializing level.world" );
			(*m_ProgressBarTextCB) ( message );
		}

		int childCount = object.m_ContainedObjects.GetCount();

		if( m_ProgressBarObjectCountCB )
			(*m_ProgressBarObjectCountCB)( childCount );

		atArray<rexObject*> fxObjects;

		for( int a = 0; a < childCount; a++ )
		{
			if( m_ProgressBarObjectCurrentIndexCB )
				(*m_ProgressBarObjectCurrentIndexCB)( a );	

			if( dynamic_cast<rexObjectRmworldFX*>( object.m_ContainedObjects[ a ] ) )
				fxObjects.PushAndGrow( object.m_ContainedObjects[ a ] );
			else
				WriteObjectInformation( *object.m_ContainedObjects[ a ], myStream );
		}

		WriteFXObjects( fxObjects );
	}		

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
static void Indent( fiStream* s, int indent )
{
	atString st;
	for( int a = 0; a < indent; a++ )
		st += "\t";
	s->Write( st, st.GetLength() );	
}

typedef void (*rexWriteMeshAdditionalInfoFunction)( rexObject* rootObj, fiStream* s, int indent );

static void WriteMesh( const char* meshType, rexObjectGenericMesh* mesh, fiStream* s, int indent, rexObject* rootObject = NULL, rexWriteMeshAdditionalInfoFunction extraFunc = NULL )
{
	char buffer[4096];

	Assert( mesh );

	Matrix34 bbMatrix;	bbMatrix.Identity();
	float	 bbLength = 0.0f, bbHeight = 0.0f, bbWidth = 0.0f;
	Vector3	 bsCenter( 0, 0, 0 );
	float	 bsRadius = 0.0f;
	mesh->MakeOptimizedBoundingBox( bbMatrix, bbLength, bbHeight, bbWidth );
	mesh->MakeOptimizedBoundingSphere( bsCenter, bsRadius );

	Indent( s, indent );
	sprintf( buffer, "Mesh %s\r\n", (const char*)mesh->GetMeshName() );
	s->Write( buffer, (int)strlen( buffer ) );

	Indent( s, indent );
	sprintf( buffer, "{\r\n" );
	s->Write( buffer, (int)strlen( buffer ) );

	Indent( s, indent + 1 );
	sprintf( buffer, "Matrix\r\n" );
	s->Write( buffer, (int)strlen( buffer ) );

	Indent( s, indent + 1 );
	sprintf( buffer, "{\r\n" );	
	s->Write( buffer, (int)strlen( buffer ) );

	
	Matrix34 m( M34_IDENTITY );
	if( mesh->m_BoneIndex )
		m = mesh->m_Matrix;
	else
		m.d = mesh->m_Center;

	Indent( s, indent + 2 );
	sprintf( buffer, "%f %f %f 0.0\r\n", m.a.x, m.a.y, m.a.z );
	s->Write( buffer, (int)strlen( buffer ) );
	Indent( s, indent + 2 );
	sprintf( buffer, "%f %f %f 0.0\r\n", m.b.x, m.b.y, m.b.z );
	s->Write( buffer, (int)strlen( buffer ) );
	Indent( s, indent + 2 );
	sprintf( buffer, "%f %f %f 0.0\r\n", m.c.x, m.c.y, m.c.z );
	s->Write( buffer, (int)strlen( buffer ) );
	Indent( s, indent + 2 );
	sprintf( buffer, "%f %f %f 1.0\r\n", m.d.x, m.d.y, m.d.z );
	s->Write( buffer, (int)strlen( buffer ) );

	Indent( s, indent + 1 );
	sprintf( buffer, "}\r\n" );
	s->Write( buffer, (int)strlen( buffer ) );

	Indent( s, indent + 1 );
	sprintf( buffer, "boneIndex %d\r\n", mesh->m_BoneIndex );
	s->Write( buffer, (int)strlen( buffer ) );

// 	if( mesh->m_GroupIndex >= 0 )
// 	{
// 		Indent( s, indent + 1 );
// 		sprintf( buffer, "roomIndex %d\r\n", mesh->m_GroupIndex );
// 		s->Write( buffer, (int)strlen( buffer ) );
// 
// 		Indent( s, indent + 1 );
// 		sprintf( buffer, "roomType %s\r\n", (const char*)mesh->m_GroupID );
// 		s->Write( buffer, (int)strlen( buffer ) );
// 	}

	Indent( s, indent + 1 );
	sprintf( buffer, "mesh %s.mesh\r\n", (const char*)mesh->GetMeshName() );
	s->Write( buffer, (int)strlen( buffer ) );

	Indent( s, indent + 1 );
	sprintf( buffer, "meshType %s\r\n", meshType );
	s->Write( buffer, (int)strlen( buffer ) );

	if( extraFunc )
	{
		(*extraFunc)( rootObject, s, indent + 1 );
	}

	Indent( s, indent + 1 );
	sprintf( buffer, "boundingBox   \t{ %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f }\r\n", bbMatrix.a.x, bbMatrix.a.y, bbMatrix.a.z, bbLength * 0.5f, bbMatrix.b.x, bbMatrix.b.y, bbMatrix.b.z, bbHeight * 0.5f, bbMatrix.c.x, bbMatrix.c.y, bbMatrix.c.z, bbWidth * 0.5f, bbMatrix.d.x, bbMatrix.d.y, bbMatrix.d.z, 1.0f );
	s->Write( buffer, (int)strlen( buffer ) );

	Indent( s, indent + 1 );
	sprintf( buffer, "boundingSphere\t{ %f %f %f %f }\r\n", bsCenter.x, bsCenter.y, bsCenter.z, bsRadius );
	s->Write( buffer, (int)strlen( buffer ) );

	Indent( s, indent );
	sprintf( buffer, "}\r\n" );
	s->Write( buffer, (int)strlen( buffer ) );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct rexRmworldLevelLODGroupInfo
{
	rexRmworldLevelLODGroupInfo()  { index = -1; parent = NULL; sibling = NULL; child = NULL; }
	~rexRmworldLevelLODGroupInfo()  { delete child; delete sibling; }

	rexRmworldLevelLODGroupInfo* AddSibling()  
	{ 
		rexRmworldLevelLODGroupInfo* l = this;  
		while( l->sibling ) 
			l = l->sibling; 
		l->sibling = new rexRmworldLevelLODGroupInfo; 
		return l->sibling; 
	}

	rexRmworldLevelLODGroupInfo* AddChild()	 
	{ 
		if( !child ) 
		{ 
			child = new rexRmworldLevelLODGroupInfo; 
			return child; 
		} 
		return child->AddSibling();  
	}

	void GetVertices( atArray< Vector3 >& verts, bool onlyIncludeTopLevel = true, bool includeChildren = true )
	{
		int lodLevels = Min( sMeshesByLODGroupAndLevel[ index ].GetCount(), onlyIncludeTopLevel ? 1 : MAX_LOD );
		for( int a = 0; a < lodLevels; a++ )
		{
			int meshCount = sMeshesByLODGroupAndLevel[ index ][ a ].GetCount();
			for( int b = 0; b < meshCount; b++ )
			{
				sMeshesByLODGroupAndLevel[ index ][ a ][ b ]->GetVertexPositions( verts, true );
			}								
		}

		if( includeChildren )
		{
			rexRmworldLevelLODGroupInfo* currChild = child;
			while( currChild )
			{
				currChild->GetVertices( verts, includeChildren );
				currChild = currChild->sibling;
			}
		}
	}

	void Serialize( fiStream* s, int indentDepth = 1 )
	{
		if( index < 0 )
			return;

		static char buffer[256];
		static const char* lodLevelIDStrings[MAX_LOD] = 
		{
			"high",
			"mid",
			"low",
			"vlow"
		};

		int lodLevels = Min( sMeshesByLODGroupAndLevel[ index ].GetCount(), MAX_LOD );

		if( lodLevels > 0 )
		{			
			atArray<Vector3> verts( 30000, 30000 );
			GetVertices( verts );
			
			int numLevelWritten = 0;

			if( verts.GetCount() )
			{
				Matrix34 bbMatrix;	bbMatrix.Identity();
				float	 bbLength = 0.0f, bbHeight = 0.0f, bbWidth = 0.0f;
				Vector3	 bsCenter( 0, 0, 0 );
				float	 bsRadius = 0.0f;

				rexUtility::MakeOptimizedBoundingBox( verts, bbMatrix, bbLength, bbHeight, bbWidth );
				rexUtility::MakeOptimizedBoundingSphere( verts, bsCenter, bsRadius );

				for( int a = 0; a < lodLevels; a++ )
				{
					int meshCount = sMeshesByLODGroupAndLevel[ index ][ a ].GetCount();
					int numMeshWritten = 0;
					for( int b = 0; b < meshCount; b++ )
					{
						rexObjectGenericMesh *mesh = sMeshesByLODGroupAndLevel[ index ][ a ][ b ];
						if( mesh->m_IsValid )
						{
							if( !numMeshWritten )
							{
								if( !numLevelWritten )
								{
									Indent( s, indentDepth );
									sprintf( buffer, "LODGroup group%d\r\n", index );
									s->Write( buffer, (int)strlen( buffer ) );

									Indent( s, indentDepth );
									sprintf( buffer, "{\r\n" );
									s->Write( buffer, (int)strlen( buffer ) );

									Indent( s, indentDepth );
									sprintf( buffer, "\tboundingBox   \t{ %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f }\r\n", bbMatrix.a.x, bbMatrix.a.y, bbMatrix.a.z, bbLength * 0.5f, bbMatrix.b.x, bbMatrix.b.y, bbMatrix.b.z, bbHeight * 0.5f, bbMatrix.c.x, bbMatrix.c.y, bbMatrix.c.z, bbWidth * 0.5f, bbMatrix.d.x, bbMatrix.d.y, bbMatrix.d.z, 1.0f );
									s->Write( buffer, (int)strlen( buffer ) );

									Indent( s, indentDepth );
									sprintf( buffer, "\tboundingSphere\t{ %f %f %f %f }\r\n", bsCenter.x, bsCenter.y, bsCenter.z, bsRadius );
									s->Write( buffer, (int)strlen( buffer ) );

									Indent( s, indentDepth );
									sprintf( buffer, "\tMatrix\r\n" );
									s->Write( buffer, (int)strlen( buffer ) );

									Indent( s, indentDepth );
									sprintf( buffer, "\t{\r\n" );
									s->Write( buffer, (int)strlen( buffer ) );

									Indent( s, indentDepth );
									sprintf( buffer, "\t\t1.0 0.0 0.0 0.0\r\n" );
									s->Write( buffer, (int)strlen( buffer ) );

									Indent( s, indentDepth );
									sprintf( buffer, "\t\t0.0 1.0 0.0 0.0\r\n" );
									s->Write( buffer, (int)strlen( buffer ) );

									Indent( s, indentDepth );
									sprintf( buffer, "\t\t0.0 0.0 1.0 0.0\r\n" );
									s->Write( buffer, (int)strlen( buffer ) );

									Indent( s, indentDepth );
									sprintf( buffer, "\t\t%f %f %f 1.0\r\n", 0.0f, 0.0f, 0.0f );//bsCenter.x, bsCenter.y, bsCenter.z );
									s->Write( buffer, (int)strlen( buffer ) );

									Indent( s, indentDepth );
									sprintf( buffer, "\t}\r\n" );
									s->Write( buffer, (int)strlen( buffer ) );
								}

								Indent( s, indentDepth );
								float thresh = 9999.00f;
								if( index < sThresholdsByLODGroupAndLevel.GetCount() &&  a < sThresholdsByLODGroupAndLevel[ index ].GetCount() )
									thresh = sThresholdsByLODGroupAndLevel[ index ][ a ][ 0 ].lodIn;
								sprintf( buffer, "\t%s %f\r\n", 
									lodLevelIDStrings[ a ], 
									thresh);
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "\t{\r\n" );
								s->Write( buffer, (int)strlen( buffer ) );
							}

							WriteMesh( "Drawable", mesh, s, 2 + indentDepth );
							numMeshWritten++;
						}

					}

					if( child && ( sLodGroupChildIndices[ child->index ] == a ))
					{
						if( !numMeshWritten )
						{
							if( !numLevelWritten )
							{
								Matrix34 bbMatrix;	bbMatrix.Identity();
								float	 bbLength = 0.0f, bbHeight = 0.0f, bbWidth = 0.0f;
								Vector3	 bsCenter( 0, 0, 0 );
								float	 bsRadius = 0.0f;

								child->GetVertices( verts );

								rexUtility::MakeOptimizedBoundingBox( verts, bbMatrix, bbLength, bbHeight, bbWidth );
								rexUtility::MakeOptimizedBoundingSphere( verts, bsCenter, bsRadius );

								Indent( s, indentDepth );
								sprintf( buffer, "LODGroup group%d\r\n", index );
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "{\r\n" );
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "\tboundingBox   \t{ %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f }\r\n", bbMatrix.a.x, bbMatrix.a.y, bbMatrix.a.z, bbLength * 0.5f, bbMatrix.b.x, bbMatrix.b.y, bbMatrix.b.z, bbHeight * 0.5f, bbMatrix.c.x, bbMatrix.c.y, bbMatrix.c.z, bbWidth * 0.5f, bbMatrix.d.x, bbMatrix.d.y, bbMatrix.d.z, 1.0f );
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "\tboundingSphere\t{ %f %f %f %f }\r\n", bsCenter.x, bsCenter.y, bsCenter.z, bsRadius );
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "\tMatrix\r\n" );
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "\t{\r\n" );
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "\t\t1.0 0.0 0.0 0.0\r\n" );
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "\t\t0.0 1.0 0.0 0.0\r\n" );
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "\t\t0.0 0.0 1.0 0.0\r\n" );
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "\t\t%f %f %f 1.0\r\n", 0.0f, 0.0f, 0.0f );//bsCenter.x, bsCenter.y, bsCenter.z );
								s->Write( buffer, (int)strlen( buffer ) );

								Indent( s, indentDepth );
								sprintf( buffer, "\t}\r\n" );
								s->Write( buffer, (int)strlen( buffer ) );
							}

							Indent( s, indentDepth );
							float thresh = 9999.00f;
							if( index < sThresholdsByLODGroupAndLevel.GetCount() &&  a < sThresholdsByLODGroupAndLevel[ index ].GetCount() )
								thresh = sThresholdsByLODGroupAndLevel[ index ][ a ][ 0 ].lodIn;
							sprintf( buffer, "\t%s %f\r\n", 
								lodLevelIDStrings[ a ], 
								thresh);
							s->Write( buffer, (int)strlen( buffer ) );

							Indent( s, indentDepth );
							sprintf( buffer, "\t{\r\n" );
							s->Write( buffer, (int)strlen( buffer ) );
						}

						child->Serialize( s, indentDepth + 2 );

						if( !numMeshWritten )
						{
							Indent( s, indentDepth );
							sprintf( buffer, "\t} %s\r\n", lodLevelIDStrings[ a ] );
							s->Write( buffer, (int)strlen( buffer ) );
							numLevelWritten++;
						}
					}

					if( numMeshWritten )
					{
						Indent( s, indentDepth );
						sprintf( buffer, "\t} %s\r\n", lodLevelIDStrings[ a ] );
						s->Write( buffer, (int)strlen( buffer ) );
						numLevelWritten++;
					}
				}

				if( numLevelWritten )
				{
					for( int a = lodLevels; a < MAX_LOD; a++ )
					{
						if( child && ( sLodGroupChildIndices[ child->index ] == a ))
						{
							Indent( s, indentDepth );
							float thresh = 9999.00f;
							if( index < sThresholdsByLODGroupAndLevel.GetCount() &&  a < sThresholdsByLODGroupAndLevel[ index ].GetCount() )
								thresh = sThresholdsByLODGroupAndLevel[ index ][ a ][ 0 ].lodIn;
							sprintf( buffer, "\t%s %f\r\n", 
								lodLevelIDStrings[ a ], 
								thresh);
							s->Write( buffer, (int)strlen( buffer ) );

							Indent( s, indentDepth );
							sprintf( buffer, "\t{\r\n" );
							s->Write( buffer, (int)strlen( buffer ) );

							child->Serialize( s, indentDepth + 2 );

							Indent( s, indentDepth );
							sprintf( buffer, "\t} %s\r\n", lodLevelIDStrings[ a ] );
							s->Write( buffer, (int)strlen( buffer ) );
							numLevelWritten++;
						}
						else
						{
							Indent( s, indentDepth );
							sprintf( buffer, "%s none 9999 \r\n", lodLevelIDStrings[ a ] );
							s->Write( buffer, (int)strlen( buffer ) );
						}
					}					
				}
			}

			if( child || numLevelWritten )
			{
				Indent( s, indentDepth );
				sprintf( buffer, "} LODGroup group%d\r\n", index );
				s->Write( buffer, (int)strlen( buffer ) );
			}

			if( sibling )
				sibling->Serialize( s, indentDepth );
		}		
	}

	int								index;
	rexRmworldLevelLODGroupInfo*		parent;
	rexRmworldLevelLODGroupInfo*		sibling;
	rexRmworldLevelLODGroupInfo*		child;		
};

///////

static rexResult WriteLODAndShaderGroupInfo( rexObject& object, fiStream* s )
{
	if( !s )
		return rexResult::ERRORS;

	const rexObjectGenericMeshHierarchyDrawable* inputMeshHier = dynamic_cast<const rexObjectGenericMeshHierarchyDrawable*>( &object );

	if( !inputMeshHier )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	sMeshesByLODGroupAndLevel.Reset();
	sThresholdsByLODGroupAndLevel.Reset();
	sNonLODMeshes.Reset();
	sMatInfoArray.Reset();
	sLodGroupDepths.Reset();
	sLodGroupChildIndices.Reset();

	inputMeshHier->GetLODsMeshesAndMaterials( sMeshesByLODGroupAndLevel, sThresholdsByLODGroupAndLevel, sNonLODMeshes, sMatInfoArray );
	inputMeshHier->GetLODGroupDepths( sLodGroupDepths );
	inputMeshHier->GetLODGroupChildIndices( sLodGroupChildIndices );

	char buffer[512];

	if(!rexShell::DoesntSerialise())
	{
		// WRITE LEVEL.SHADER file
		fiStream* ls = ASSET.Create( "level", "shader" );
		if( ls )
		{
			sprintf( buffer, "Version: 103\r\n" );
			ls->Write( buffer, (int)strlen( buffer ) );

			int matCount = sMatInfoArray.GetCount();
			
			if( matCount )
			{
				sprintf( buffer, "ShadingGroup {\r\n\tShadingGroup {\r\n\t\tCount 1\r\n\t\tShaders %d {\r\n", matCount );
				ls->Write( buffer, (int)strlen( buffer ) );			

				for( int a = 0; a < matCount; a++ )
				{
					rexObjectGenericMesh::MaterialInfo& matInfo = sMatInfoArray[ a ];
					int attrCount = matInfo.m_AttributeValues.GetCount();
					sprintf( buffer, "\t\t\t%s ", (const char*)matInfo.m_TypeName );
					ls->Write( buffer, (int)strlen( buffer ) );			
					sprintf( buffer, "%d ", attrCount );
					ls->Write( buffer, (int)strlen( buffer ) );			
					for( int b = 0; b < attrCount; b++ )
					{
						sprintf( buffer, "%s ", matInfo.m_AttributeValues[ b ].toString() );
						ls->Write( buffer, (int)strlen( buffer ) );			
					}
					sprintf( buffer, "\r\n" );
					ls->Write( buffer, (int)strlen( buffer ) );			
				}

				sprintf( buffer, "\t\t}\r\n\t}\r\n}\r\n" );
				ls->Write( buffer, (int)strlen( buffer ) );
			}
			else
			{
				sprintf( buffer, "shadinggroup none\r\n" );
				ls->Write( buffer, (int)strlen( buffer ) );			
			}

			ls->Close();
		}
		else
		{
			rexResult r( rexResult::ERRORS );

			// Get full path for debugging
			char acFullPathForDebugging[255];
			ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"level","shader" );

			// Flip the slashes
			for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

			// Error
			r.AddError( "Could not create file '%s'", acFullPathForDebugging);
			return r;
		}
	}

	// END WRITE LEVEL.SHADER file
	int lodGroupCount = sMeshesByLODGroupAndLevel.GetCount();

	// BUILD LOD HIERARCHY
	rexRmworldLevelLODGroupInfo root;

	int lastFound = 0;
	atArray< rexRmworldLevelLODGroupInfo* > stack;
	stack.PushAndGrow( NULL );
    
	for( int z = 0; z < lodGroupCount; z++ )
	{
		int levelCount = sMeshesByLODGroupAndLevel[ z ].GetCount();
		bool meshFound = false;
		for( int l = 0; l < levelCount; l++ )
		{
			int meshCount = sMeshesByLODGroupAndLevel[ z ][ l ].GetCount();
			for( int m = 0; m < meshCount; m++ )
			{
				if( sMeshesByLODGroupAndLevel[ z ][ l ][ m ]->m_IsValid )
				{
					meshFound = true;
					break;
				}
			}
		}

		if( meshFound )
		{			
			int delta = ( z >= sLodGroupDepths.GetCount() ) ? 0 : ( sLodGroupDepths[ z ] - sLodGroupDepths[ lastFound ] );
			for( int a = 0; a >= delta; a-- )
				stack.Pop();

			rexRmworldLevelLODGroupInfo* currGroup = stack.GetCount() ? stack[ stack.GetCount() - 1 ]  : NULL;

			for( int a = 1; a < delta; a++ )
				stack.PushAndGrow( currGroup );				

			if( currGroup )
				currGroup = currGroup->AddChild();
			else if( root.index >= 0 )
				currGroup = root.AddSibling();
			else
				currGroup = &root;

			currGroup->index = z;
			stack.PushAndGrow( currGroup );
			lastFound = z;
		}
	}

	root.Serialize( s );
	// END WRITE LOD

	// WRITE NON-LOD MESHES
	int nonLODMeshCount = sNonLODMeshes.GetCount();

	for( int a = 0; a < nonLODMeshCount; a++ )
	{		
		rexObjectGenericMesh* mesh = sNonLODMeshes[ a ];
		if( mesh->m_IsValid )
		{
			int vertCount = mesh->m_Vertices.GetCount();
			atArray<Vector3> verts( vertCount, vertCount );
			for( int c = 0; c < vertCount; c++ )
			{
				static Vector3 vert;
				mesh->m_Matrix.Transform( mesh->m_Vertices[ c ].m_Position, vert );
				verts.PushAndGrow( vert );
			}

			WriteMesh( "Drawable", mesh, s, 1 );
		}
	}
	// END WRITE NON-LOD MESHES

	return rexResult( rexResult::PERFECT );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void GatherBoneTagInfo( const rexObjectGenericSkeleton::Bone* bone, atArray<atString>& boneTagIDs, atArray<int>& boneTagIndices, int& currIndex )
{
	Assert( bone );
	int boneTagCount = bone->m_BoneTags.GetCount();
	for( int a = 0; a < boneTagCount; a++ )
	{
		boneTagIDs.PushAndGrow( bone->m_BoneTags[a] );
		boneTagIndices.PushAndGrow( currIndex );
	}
	currIndex++;
	
	int childCount = bone->m_Children.GetCount();
	
	for( int a = 0; a < childCount; a++ )
	{
		GatherBoneTagInfo( bone->m_Children[ a ], boneTagIDs, boneTagIndices, currIndex );
	}
}

///////////////////////////////////////////////////////////////////////////////////////

static rexResult WriteSkeletonAndBoneTagInfo( rexObject& object, fiStream* s )
{
	if( !s )
		return rexResult::ERRORS;

	const rexObjectGenericSkeleton* inputSkeleton = dynamic_cast<const rexObjectGenericSkeleton*>( &object );

	if( !inputSkeleton )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	
	char buffer[512];
	if( inputSkeleton->m_RootBone )
	{
		sprintf( buffer, "skel {\r\n\tskel %s.skel\r\n}\r\n", (const char*)inputSkeleton->GetName() );
		s->Write( buffer, (int)strlen( buffer ) );
		
		atArray<atString>	boneTagIDs;
		atArray<int>		boneTagIndices;

		int currIndex = 0;
		GatherBoneTagInfo( inputSkeleton->m_RootBone, boneTagIDs, boneTagIndices, currIndex );

		int tagCount = boneTagIDs.GetCount();
		if( tagCount )
		{
			sprintf( buffer, "bonetag {\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );

			for( int a = 0; a < tagCount; a++ )
			{
				sprintf( buffer, "\t%s\t", (const char*)boneTagIDs[ a ] );
				s->Write( buffer, (int)strlen( buffer ) );				
				sprintf( buffer, "%d\r\n", boneTagIndices[ a ] );
				s->Write( buffer, (int)strlen( buffer ) );
			}

			sprintf( buffer, "}\r\n" );
			s->Write( buffer, (int)strlen( buffer ) );
		}
	}
	else
	{
		sprintf( buffer, "skel {\r\n\tskel none\r\n}\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );
	}

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////

static rexResult WriteCurveInfo( rexObject& object, fiStream* /*s*/ )
{
	return rexSerializerRAGENurbsCurve::WriteCurveInfo(object,"district");
}

static rexResult WriteLightInfo( rexObject& object, fiStream* /*s*/ )
{
	const rexObjectGenericLightGroup* lightGroup = dynamic_cast<const rexObjectGenericLightGroup*>( &object );
	if( !lightGroup )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	if(rexShell::DoesntSerialise())
	{
		return rexResult::PERFECT;
	}

	// WRITE DISTRICT.LIGHTS file
	fiStream* ls = ASSET.Create( "district", "lights" );
	if( !ls )
	{
		// Oh dear, something bad has happened
		rexResult retval;
		retval.Combine( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"district","lights" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		retval.AddError( "Could not create file '%s'", acFullPathForDebugging);
		return retval;
	}

	char buffer[512];

	int childCount = lightGroup->m_ContainedObjects.GetCount();

	sprintf( buffer, "Count %d\r\n", childCount );
	ls->Write( buffer, (int)strlen( buffer ) );

	for( int a = 0; a < childCount; a++ )
	{
		const rexObjectGenericLight* light = dynamic_cast<const rexObjectGenericLight*>( lightGroup->m_ContainedObjects[ a ] );

		if( light )
		{
			sprintf( buffer, "Light %s {\r\n", (const char*)light->GetName() );
			ls->Write( buffer, (int)strlen( buffer ) );

			if( light->m_LightType == rexObjectGenericLight::LIGHT_DIRECTIONAL )
                sprintf( buffer, "\tType directionalLight\r\n" );
			else if( light->m_LightType == rexObjectGenericLight::LIGHT_POINT )
				sprintf( buffer, "\tType pointLight\r\n" );
			else if( light->m_LightType == rexObjectGenericLight::LIGHT_SPOTLIGHT )
				sprintf( buffer, "\tType spotLight\r\n" );
			else
				sprintf( buffer, "\tType ambientLight\r\n" );
			ls->Write( buffer, (int)strlen( buffer ) );

			sprintf( buffer, "\tLocation %s\r\n", ( light->m_RoomIndex >= 0 ) ? "interior" : "exterior" );
			ls->Write( buffer, (int)strlen( buffer ) );

			sprintf( buffer, "\tRoom %d\r\n", light->m_RoomIndex );
			ls->Write( buffer, (int)strlen( buffer ) );
			
			sprintf( buffer, "\tPosition %f %f %f\r\n", light->m_Location.x, light->m_Location.y, light->m_Location.z );
			ls->Write( buffer, (int)strlen( buffer ) );

			Vector3 dirNormalized( light->m_Direction );
			dirNormalized.Normalize();
			
			sprintf( buffer, "\tDirection %f %f %f\r\n", dirNormalized.x, dirNormalized.y, dirNormalized.z );
			ls->Write( buffer, (int)strlen( buffer ) );

			if( light->m_LightType == rexObjectGenericLight::LIGHT_SPOTLIGHT )
			{
				sprintf( buffer, "\tSpotAngle %f\r\n", light->m_SpotAngle );
				ls->Write( buffer, (int)strlen( buffer ) );
			}

			sprintf( buffer, "\tColor %f %f %f\r\n", light->m_Color.x, light->m_Color.y, light->m_Color.z );//, light->m_Color.w );
			ls->Write( buffer, (int)strlen( buffer ) );

			sprintf( buffer, "\tIntensity %f\r\n", light->m_Intensity );
			ls->Write( buffer, (int)strlen( buffer ) );

			sprintf( buffer, "\tCastShadowRange %f\r\n", light->m_CastShadowRange );
			ls->Write( buffer, (int)strlen( buffer ) );

			fprintf(ls,"\tDecayRate %d\r\n", light->m_DecayRate);

			sprintf( buffer, "}\r\n" );
			ls->Write( buffer, (int)strlen( buffer ) );
		}
	}

	ls->Close();

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

static void GatherAnimationChunkOutputLines( rexObjectGenericAnimation::ChunkInfo* chunk, atArray<atString>& outputLines, atMap<atString,int>& taggedChunkMap, bool isTextureAnimation )
{
	char buffer[ 512 ];
	float startTime = chunk->GetStartTime();
	float endTime = chunk->GetEndTime();

	if( !taggedChunkMap.Access( chunk->m_Name ) && ( startTime < endTime ))
	{
		if( isTextureAnimation )
		{
			int meshCount = sNonLODMeshes.GetCount();
			rexObjectGenericMesh* mesh = NULL;
			for( int m = 0; m < meshCount; m++ )
			{
				atString name("tex_");
				name += sNonLODMeshes[ m ]->GetMeshName();
				if( name == chunk->m_Name )
				{						
					mesh = sNonLODMeshes[ m ];
					break;
				}
			}

			if( !mesh )
			{
				int groupCount = sMeshesByLODGroupAndLevel.GetCount();
				for( int g = 0; g < groupCount; g++ )
				{
					int levelCount = sMeshesByLODGroupAndLevel[ g ].GetCount();
					for( int l = 0; l < levelCount; l++ )
					{
						int meshCount = sMeshesByLODGroupAndLevel[ g ][ l ].GetCount();
						for( int m = 0; m < meshCount; m++ )
						{
							atString name(isTextureAnimation ? "tex_" : "");
							name += sMeshesByLODGroupAndLevel[ g ][ l ][ m ]->GetMeshName();
							if( name == chunk->m_Name )
							{
								mesh = sMeshesByLODGroupAndLevel[ g ][ l ][ m ];
								break;
							}
						}
					}
					if( mesh )
						break;
				}

			}			

			atArray<int> shaderIndices;

			atString& outputLine = outputLines.Grow();

			int matCount = sMatInfoArray.GetCount();
			int meshMatCount = mesh ? mesh->m_Materials.GetCount() : 0;

			for( int mm = 0; mm < meshMatCount; mm++ )
			{
				for( int m = 0; m < matCount; m++ )
				{					
					if( strstr( sMatInfoArray[ m ].m_TypeName, "nimated" ) && sMatInfoArray[ m ].IsSame( mesh->m_Materials[ mm ] ) )
					{
						shaderIndices.PushAndGrow( m );
						break;
					}
				}
			}

			int shaderCount = shaderIndices.GetCount();

			sprintf( buffer, "%s %s.anim %d shaders ", (const char*)chunk->m_Name, (const char*)chunk->m_Name, shaderCount );					
			outputLine += buffer;

			for( int sh = 0; sh < shaderCount; sh++ )
			{
				sprintf( buffer, "%d ", shaderIndices[ sh ] );					
				outputLine += buffer;
			}

			sprintf( buffer, "loop %d autoplay %d\r\n", 1, 1 );					
			outputLine += buffer;
		}
		else // if !isTextureAnimation
		{
			atString& outputLine = outputLines.Grow();

			sprintf( buffer, "\t\tanimation %s ", (const char*)chunk->m_Name );
			outputLine += buffer;

			sprintf( buffer, "%d rootbones %d ", 1, chunk->m_BoneIndex );
			outputLine += buffer;

			sprintf( buffer, "autoplay rootsonly\r\n" );
			outputLine += buffer;
		}

		int childCount = chunk->m_Children.GetCount();
		for( int a = 0; a < childCount; a++ )
		{
			GatherAnimationChunkOutputLines( chunk->m_Children[ a ], outputLines, taggedChunkMap, isTextureAnimation );
		}
	}
}

#if 0
static rexResult WriteAnimationInfo( rexObject& object, fiStream* s_ )
{
	if( !s_ )
		return rexResult::ERRORS;

	const rexObjectGenericAnimation* inputAnimation = dynamic_cast<const rexObjectGenericAnimation*>( &object );

	if( !inputAnimation )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	// determine if this is skeletal or texture
	bool isTextureAnimation = false;
	
	int rootChunkCount = inputAnimation->m_RootChunks.GetCount();

	for( int rc = 0; rc < rootChunkCount; rc++  )
	{
		int channelCount = inputAnimation->m_RootChunks[ rc ]->m_ChannelData.GetCount();
		for( int c = 0; c < channelCount; c++ )
		{
			if( !strcmp( inputAnimation->m_RootChunks[ rc ]->m_ChannelData[ c ].m_Name, "frameExtension" ) )
			{
				isTextureAnimation = true;
				break;
			}
		}
	}
	
	static sWasTextureAnimation = true;
	if( !rootChunkCount )
		isTextureAnimation = !sWasTextureAnimation;
	
	sWasTextureAnimation = isTextureAnimation;

	atArray<atString> outputLines;
	atMap<atString,int> taggedChunkMap;

	// process tagged animations

	int childCount = inputAnimation->m_ContainedObjects.GetCount();
	char buffer[512];

	for( int a = 0; a < childCount; a++ )
	{
		const rexObjectGenericAnimationSegment* segment = dynamic_cast<const rexObjectGenericAnimationSegment*>( inputAnimation->m_ContainedObjects[ a ] );
		if( segment )
		{
			atString& outputLine = outputLines.Grow();
			if( isTextureAnimation ) 
			{
				atArray<int> shaderIndices;

				int chunkCount = segment->m_ChunkNames.GetCount();
				for( int c = 0; c < chunkCount; c++ )
				{
					rexObjectGenericAnimation::ChunkInfo* chunk = inputAnimation->GetChunkByName( segment->m_ChunkNames[ c ] );
					Assert( chunk );

					if( !taggedChunkMap.Access( chunk->m_Name ) )
						taggedChunkMap.Insert( chunk->m_Name, chunk->m_BoneIndex );
					
					int meshCount = sNonLODMeshes.GetCount();
					rexObjectGenericMesh* mesh = NULL;
					for( int m = 0; m < meshCount; m++ )
					{
						atString name = "tex_";
						name += sNonLODMeshes[ m ]->m_Name;
						if( name == chunk->m_Name )
						{						
							mesh = sNonLODMeshes[ m ];
							break;
						}
					}

					if( !mesh )
					{
						int groupCount = sMeshesByLODGroupAndLevel.GetCount();
						for( int g = 0; g < groupCount; g++ )
						{
							int levelCount = sMeshesByLODGroupAndLevel.GetCount();
							for( int l = 0; l < levelCount; l++ )
							{
								int meshCount = sMeshesByLODGroupAndLevel[ g ][ l ].GetCount();
								for( int m = 0; m < meshCount; m++ )
								{
									atString name = "tex_";
									name += sMeshesByLODGroupAndLevel[ g ][ l ][ m ]->m_Name;
									if( name == chunk->m_Name )
									{
										mesh = sMeshesByLODGroupAndLevel[ g ][ l ][ m ];
										break;
									}
								}
							}
							if( mesh )
								break;
						}

					}			

					int matCount = sMatInfoArray.GetCount();
					int meshMatCount = mesh ? mesh->m_Materials.GetCount() : 0;

					for( int mm = 0; mm < meshMatCount; mm++ )
					{
						for( int m = 0; m < matCount; m++ )
						{					
							if( strstr( sMatInfoArray[ m ].m_TypeName, "nimated" ) && sMatInfoArray[ m ].IsSame( mesh->m_Materials[ mm ] ) )
							{
								shaderIndices.PushAndGrow( m );
								break;
							}
						}
					}
				}

				int shaderCount = shaderIndices.GetCount();

				sprintf( buffer, "%s %s.anim %d shaders ", (const char*)segment->GetName(), (const char*)segment->GetName(), shaderCount );					
				outputLine += buffer;

				for( int sh = 0; sh < shaderCount; sh++ )
				{
					sprintf( buffer, "%d ", shaderIndices[ sh ] );					
					outputLine += buffer;
				}

				sprintf( buffer, "loop %d autoplay %d\r\n", segment->m_Looping, segment->m_Autoplay );					
				outputLine += buffer;
			}
			else // !isTextureAnimation
			{
				int boneCount = segment->m_ChunkNames.GetCount();

				sprintf( buffer, "\t\tanimation %s ", (const char*)segment->GetName() );
				outputLine += buffer;

				sprintf( buffer, "%d rootbones ", boneCount );
				outputLine += buffer;

				for( int b = 0; b < boneCount; b++ )
				{
					rexObjectGenericAnimation::ChunkInfo* chunk = inputAnimation->GetChunkByName( segment->m_ChunkNames[ b ] );
					Assert( chunk );
					sprintf( buffer, "%d ", chunk->m_BoneIndex );
					outputLine += buffer;

					if( !taggedChunkMap.Access( chunk->m_Name ) )
						taggedChunkMap.Insert( chunk->m_Name, chunk->m_BoneIndex );			
				}

				sprintf( buffer, "%s\r\n", segment->m_Autoplay ? "autoplay" : "" );
				outputLine += buffer;
			}
		}					
	}		

	/// process the untagged animations
	int rootCount = inputAnimation->m_RootChunks.GetCount();

	for( int rc = 0; rc < rootCount; rc++ )
	{
		rexObjectGenericAnimation::ChunkInfo* chunk = inputAnimation->m_RootChunks[ rc ];
		Assert( chunk );
		GatherAnimationChunkOutputLines( chunk, outputLines, taggedChunkMap, isTextureAnimation );
	}

	// do the actual file output
	fiStream* s;

	if( isTextureAnimation )
	{
		s = ASSET.Create( "district", "shaderc" );
	}
	else
	{
		s = ASSET.Create( "level", "anims" );
	}

	if( !s )
	{
		// Oh dear, something bad has happened
		rexResult retval(rexResult::ERRORS);

		// Get full path for debugging
		char acFullPathForDebugging[255];
		if( isTextureAnimation )
		{
			ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"district","shaderc" );
		}
		else
		{
			ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"level","anims" );
		}

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		retval.AddMessage( "Could not create file '%s'", acFullPathForDebugging);
		return retval;
	}

	int itemCount = outputLines.GetCount();
	if( isTextureAnimation )
	{
		sprintf( buffer, "%d\r\n", itemCount );
		s->Write( buffer, (int)strlen( buffer ) );
	}
	else
	{
		sprintf( buffer, "Version: 100\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );

		sprintf( buffer, "Anims %d\t{\r\n", itemCount );
		s->Write( buffer, (int)strlen( buffer ) );
	}

	for( int i = 0; i < itemCount; i++ )
	{
		s->Write( (const char*)outputLines[ i ], outputLines[ i ].GetLength() );
	}

	if( !isTextureAnimation )
	{
		sprintf( buffer, "}\r\n" );
		s->Write( buffer, (int)strlen( buffer ) );
	}

	s->Close();

	return rexResult( rexResult::PERFECT );
}
#endif


static void GetBounds( rexObject& object, atArray<rexObjectGenericBound*>& boundList )
{
	rexObjectGenericBound* mesh = dynamic_cast<rexObjectGenericBound*>( &object );

	if( mesh && mesh->m_IsValid && !mesh->m_ChildOfLevelInstanceNode )
		boundList.PushAndGrow( mesh );

	const rexObjectGenericBoundHierarchy* inputMeshHier = dynamic_cast<const rexObjectGenericBoundHierarchy*>( &object );
	if( inputMeshHier )
	{
		int childObjCount = object.m_ContainedObjects.GetCount();
		
		for( int a = 0; a < childObjCount; a++ )
		{
			GetBounds( *object.m_ContainedObjects[ a ], boundList );
		}
	}	
}

static rexResult WriteBoundInfo( rexObject& object, fiStream* s )
{
	if( !s )
		return rexResult::ERRORS;

	const rexObjectGenericBoundHierarchy* inputMeshHier = dynamic_cast<const rexObjectGenericBoundHierarchy*>( &object );

	if( !inputMeshHier )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	atArray<rexObjectGenericBound*> boundList;
	int childCount = inputMeshHier->m_ContainedObjects.GetCount();
	for( int a = 0; a < childCount; a++ )
		GetBounds( *inputMeshHier->m_ContainedObjects[ a ], boundList );

	char buffer[512];
	int boundCount = boundList.GetCount();

	if( boundCount )
	{
		for( int a = 0; a < boundCount; a++ )
		{
			sprintf( buffer, "\tBound %s {\r\n", (const char*)boundList[ a ]->GetMeshName() );
			s->Write( buffer, (int)strlen( buffer ) );

			sprintf( buffer, "\t\tboneIndex %d\r\n", boundList[ a ]->m_BoneIndex );		
			s->Write( buffer, (int)strlen( buffer ) );

// 			if( boundList[ a ]->m_GroupIndex >= 0 )
// 			{
// 				sprintf( buffer, "\t\troomIndex %d\r\n", boundList[ a ]->m_GroupIndex );
// 				s->Write( buffer, (int)strlen( buffer ) );
// 
// 				sprintf( buffer, "\t\troomType %s\r\n", (const char*)boundList[ a ]->m_GroupID );
// 				s->Write( buffer, (int)strlen( buffer ) );
// 			}

			int flagCount = boundList[ a ]->m_SpecialFlags.GetCount();
			if( flagCount )
			{
				sprintf( buffer, "\t\tboundFlags %d", flagCount );		
				s->Write( buffer, (int)strlen( buffer ) );

				for( int f = 0; f < flagCount; f++ )
				{
					sprintf( buffer, " %s", (const char*)boundList[ a ]->m_SpecialFlags[ f ] );		
					s->Write( buffer, (int)strlen( buffer ) );
				}

				sprintf( buffer, "\r\n" );		
				s->Write( buffer, (int)strlen( buffer ) );
			}
			
			sprintf( buffer, "\t\tbound %s.bnd\r\n", (const char*)boundList[ a ]->GetMeshName() );		
			s->Write( buffer, (int)strlen( buffer ) );

			sprintf( buffer, "\t}\r\n");
			s->Write( buffer, (int)strlen( buffer ) );		
		}
	}

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

static rexResult WriteNavMeshInfo( rexObject& object, fiStream* )
{
	const rexObjectRmworldDistrictNavMeshHierarchy* inputMeshHier = dynamic_cast<const rexObjectRmworldDistrictNavMeshHierarchy*>( &object );

	if( !inputMeshHier )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	if(rexShell::DoesntSerialise())
	{
		return rexResult::PERFECT;
	}

	fiStream* s = ASSET.Create( "district", "navbounds" );
	if( !s )
	{
		// Oh dear, something bad has happened
		rexResult retval( rexResult::ERRORS );

		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"district","navbounds" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		retval.AddError( "Could not create file '%s'", acFullPathForDebugging);
		return retval;
	}

	char buffer[512];

	sprintf( buffer, "LEVEL\r\n{\r\n" );
	s->Write( buffer, (int)strlen( buffer ) );

	sprintf( buffer, "\tBoundingSphere %f %f %f %f\r\n", sBoundSphereCenter.x, sBoundSphereCenter.y, sBoundSphereCenter.z, sBoundSphereRadius  );
	s->Write( buffer, (int)strlen( buffer ) );

 	atArray<rexObjectGenericBound*> boundList;
	GetBounds( object, boundList );

	int boundCount = boundList.GetCount();

	if( boundCount )
	{
		for( int a = 0; a < boundCount; a++ )
		{
			if( boundList[ a ]->m_IsValid )
			{
				sprintf( buffer, "\tBound %s {\r\n", (const char*)boundList[ a ]->GetMeshName() );
				s->Write( buffer, (int)strlen( buffer ) );

				sprintf( buffer, "\t\tboneIndex %d\r\n", boundList[ a ]->m_BoneIndex );		
				s->Write( buffer, (int)strlen( buffer ) );

// 				if( boundList[ a ]->m_GroupIndex >= 0 )
// 				{
// 					sprintf( buffer, "\t\troomIndex %d\r\n", boundList[ a ]->m_GroupIndex );
// 					s->Write( buffer, (int)strlen( buffer ) );
// 
// 					sprintf( buffer, "\t\troomType %s\r\n", (const char*)boundList[ a ]->m_GroupID );
// 					s->Write( buffer, (int)strlen( buffer ) );
// 				} // GunnarD:Retired

				int flagCount = boundList[ a ]->m_SpecialFlags.GetCount();
				if( flagCount )
				{
					sprintf( buffer, "\t\tboundFlags %d", flagCount );		
					s->Write( buffer, (int)strlen( buffer ) );

					for( int f = 0; f < flagCount; f++ )
					{
						sprintf( buffer, " %s", (const char*)boundList[ a ]->m_SpecialFlags[ f ] );		
						s->Write( buffer, (int)strlen( buffer ) );
					}

					sprintf( buffer, "\r\n" );		
					s->Write( buffer, (int)strlen( buffer ) );
				}

				sprintf( buffer, "\t\tbound %s.bnd\r\n", (const char*)boundList[ a ]->GetMeshName() );		
				s->Write( buffer, (int)strlen( buffer ) );

				sprintf( buffer, "\t} Bound %s\r\n", (const char*)boundList[ a ]->GetMeshName() );
				s->Write( buffer, (int)strlen( buffer ) );		
			}
		}
	}

	sprintf( buffer, "}\r\n" );
	s->Write( buffer, (int)strlen( buffer ) );

	s->Close();

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

static rexResult WriteMeshesAsType( rexObject& object, fiStream* s, const char* meshType, rexObject* rootObject = NULL, rexWriteMeshAdditionalInfoFunction extraFunc = NULL )
{
	if( !s )
		return rexResult::ERRORS;

	const rexObjectGenericMeshHierarchy* inputMeshHier = dynamic_cast<const rexObjectGenericMeshHierarchy*>( &object );

	rexResult retval( rexResult::PERFECT );

	if( inputMeshHier )
	{
		int childCount = object.m_ContainedObjects.GetCount();
		for( int a = 0; a < childCount; a++ )
		{
			retval &= WriteMeshesAsType( *object.m_ContainedObjects[ a ], s, meshType, rootObject, extraFunc );
		}
	}
	else
	{
		rexObjectGenericMesh *mesh = dynamic_cast<rexObjectGenericMesh*>( &object );
		if( mesh && mesh->m_IsValid )
		{
			WriteMesh( meshType, mesh, s, 1, rootObject, extraFunc );
		}
		else
		{
			retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}
	}

	return retval;
}



static rexResult WritePortals( rexObject& object, fiStream* s )
{
	return WriteMeshesAsType( object, s, "Portal" );
}

static rexResult WriteWater( rexObject& object, fiStream* s )
{
	return WriteMeshesAsType( object, s, "Water" );
}

static rexResult WriteRooms( rexObject& object, fiStream* s )
{
	return WriteMeshesAsType( object, s, "Room" );
}

static rexResult WriteOccluders( rexObject& object, fiStream* s )
{
	return WriteMeshesAsType( object, s, "Occluder" );
}

static rexResult WriteLocatorInfo( rexObject& object, fiStream* /*s2*/ )
{
	const rexObjectGenericLocatorGroup* locatorGroup = dynamic_cast<const rexObjectGenericLocatorGroup*>( &object );

	if( !locatorGroup )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	if(rexShell::DoesntSerialise())
	{
		return rexResult::PERFECT;
	}


	fiStream* s = ASSET.Create( "district", "locators" );
	if( !s )
	{
		// Oh dear, something bad has happened
		rexResult retval( rexResult::ERRORS, rexResult::CANNOT_CREATE_FILE );

		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"district","locators" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		retval.AddError( "Could not create file '%s'", acFullPathForDebugging);
		return retval;
	}

	char buffer[512];
	int childCount = locatorGroup->m_ContainedObjects.GetCount();

	sprintf( buffer, "count %d\r\n", childCount );
	s->Write( buffer, (int)strlen( buffer ) );
	
	for( int a = 0; a < childCount; a++ )
	{
		const rexObjectGenericLocator* locator = dynamic_cast<const rexObjectGenericLocator*>( locatorGroup->m_ContainedObjects[ a ] );

		Assert( locator );
		Vector3 eulers;
		locator->m_Matrix.ToEulersXYZ( eulers );

		sprintf( buffer, "%s ", (const char*)locator->GetName() );
		s->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "boneindex %d ", locator->m_BoneIndex );
		s->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "roomindex %d ", locator->m_GroupIndex );
		s->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "offset %f %f %f ", locator->m_Matrix.d.x, locator->m_Matrix.d.y, locator->m_Matrix.d.z );
		s->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "eulers %f %f %f\r\n", eulers.x, eulers.y, eulers.z );
		s->Write( buffer, (int)strlen( buffer ) );
	}
	
	s->Close();

	return rexResult( rexResult::PERFECT );
}



static rexResult WriteLevelInstances( rexObject& object, fiStream* /*s*/ )
{
	rexResult retval( rexResult::PERFECT );

	if(rexShell::DoesntSerialise())
	{
		return retval;
	}


	rexObjectLevelInstanceGroup* group = dynamic_cast<rexObjectLevelInstanceGroup*>( &object );	
	if( group && group->m_ContainedObjects.GetCount() )
	{
		fiStream* bangerStream = ASSET.Create( "level", "bangers" );
		fiStream* instanceStream = ASSET.Create( "level", "instances" );
		
		if( bangerStream && instanceStream )
		{
			char buffer[256];
			int instanceCount = group->m_ContainedObjects.GetCount();
			for( int a = 0; a < instanceCount; a++ )
			{
				rexObjectLevelInstance* inst = dynamic_cast<rexObjectLevelInstance*>( group->m_ContainedObjects[ a ] );
				if( inst )
				{
					if( inst->m_SubType == rexObjectLevelInstance::INSTANCED_OBJECT )
					{
						sprintf( buffer, "Instance\r\n{\r\n" );
						instanceStream->Write( buffer, (int)strlen( buffer ) );

						sprintf( buffer, "\tMatrix\r\n\t{\r\n" );
						instanceStream->Write( buffer, (int)strlen( buffer ) );

						Matrix34& m = inst->m_Matrix;
						sprintf( buffer, "\t\t%f %f %f 0.0\r\n", m.a.x, m.a.y, m.a.z );
						instanceStream->Write( buffer, (int)strlen( buffer ) );
						sprintf( buffer, "\t\t%f %f %f 0.0\r\n", m.b.x, m.b.y, m.b.z );
						instanceStream->Write( buffer, (int)strlen( buffer ) );
						sprintf( buffer, "\t\t%f %f %f 0.0\r\n", m.c.x, m.c.y, m.c.z );
						instanceStream->Write( buffer, (int)strlen( buffer ) );
						sprintf( buffer, "\t\t%f %f %f 1.0\r\n", m.d.x, m.d.y, m.d.z );
						instanceStream->Write( buffer, (int)strlen( buffer ) );

						sprintf( buffer, "\t}\r\n" );
						instanceStream->Write( buffer, (int)strlen( buffer ) );

						sprintf( buffer, "\tInstanceType %s\r\n", (const char*) inst->m_Name );
						instanceStream->Write( buffer, (int)strlen( buffer ) );

						int meshCount = inst->m_MeshNames.GetCount();

						sprintf( buffer, "\tCPV %d\r\n\t{\r\n", meshCount );
						instanceStream->Write( buffer, (int)strlen( buffer ) );

						for( int m = 0; m < meshCount; m++ )
						{
							sprintf( buffer, "\t\t%s\r\n", (const char*)inst->m_MeshNames[ m ] );
							instanceStream->Write( buffer, (int)strlen( buffer ) );
						}

						sprintf( buffer, "\t}\r\n}\r\n" );
						instanceStream->Write( buffer, (int)strlen( buffer ) );
					}
					else
					{
						sprintf( buffer, "Banger\r\n{\r\n" );
						bangerStream->Write( buffer, (int)strlen( buffer ) );

						sprintf( buffer, "\tMatrix\r\n\t{\r\n" );
						bangerStream->Write( buffer, (int)strlen( buffer ) );

						Matrix34& m = inst->m_Matrix;
						sprintf( buffer, "\t\t%f %f %f 0.0\r\n", m.a.x, m.a.y, m.a.z );
						bangerStream->Write( buffer, (int)strlen( buffer ) );
						sprintf( buffer, "\t\t%f %f %f 0.0\r\n", m.b.x, m.b.y, m.b.z );
						bangerStream->Write( buffer, (int)strlen( buffer ) );
						sprintf( buffer, "\t\t%f %f %f 0.0\r\n", m.c.x, m.c.y, m.c.z );
						bangerStream->Write( buffer, (int)strlen( buffer ) );
						sprintf( buffer, "\t\t%f %f %f 1.0\r\n", m.d.x, m.d.y, m.d.z );
						bangerStream->Write( buffer, (int)strlen( buffer ) );

						sprintf( buffer, "\t}\r\n" );
						bangerStream->Write( buffer, (int)strlen( buffer ) );

						sprintf( buffer, "\tInstanceType %s\r\n", (const char*) inst->m_Name );
						bangerStream->Write( buffer, (int)strlen( buffer ) );

						sprintf( buffer, "}\r\n" );
						bangerStream->Write( buffer, (int)strlen( buffer ) );
					}
				}
				else
				{
					retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
				}
			}			
			bangerStream->Close();
			instanceStream->Close();
		}
		else
		{
			if( !bangerStream )
			{
				retval.Combine( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

				// Get full path for debugging
				char acFullPathForDebugging[255];
				ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"level","bangers" );

				// Flip the slashes
				for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

				// Error
				retval.AddError( "Could not create file '%s'", acFullPathForDebugging);
			}

			if( !instanceStream )
			{
				retval.Combine( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

				// Get full path for debugging
				char acFullPathForDebugging[255];
				ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),"level","instances" );

				// Flip the slashes
				for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

				// Error
				retval.AddError( "Could not create file '%s'", acFullPathForDebugging);
			}
		}
	}
	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////

rexSerializerRmworldDistrict::rexSerializerRmworldDistrict() : rexSerializerGenericContainer()
{
	rexObjectGenericMeshHierarchyDrawable mhdType;
	RegisterObjectType( mhdType, MakeFunctorRet(WriteLODAndShaderGroupInfo) );

	rexObjectGenericBoundHierarchy boundHierType;
	RegisterObjectType( boundHierType, MakeFunctorRet(WriteBoundInfo) );

	// rexObjectGenericAnimation animType;
	// RegisterObjectType( animType, WriteAnimationInfo );

	rexObjectGenericMeshHierarchyRoomVolumes roomsType;
	RegisterObjectType( roomsType, MakeFunctorRet(WriteRooms) );

	rexObjectGenericMeshHierarchyOccluders occludersType;
	RegisterObjectType( occludersType, MakeFunctorRet(WriteOccluders) );

	rexObjectGenericMeshHierarchyPortals portalsType;
	RegisterObjectType( portalsType, MakeFunctorRet(WritePortals) );

	rexObjectGenericMeshHierarchyWater waterType;
	RegisterObjectType( waterType, MakeFunctorRet(WriteWater) );

	rexObjectRmworldDistrictNavMeshHierarchy navMeshType;
	RegisterObjectType( navMeshType, MakeFunctorRet(WriteNavMeshInfo) );

	rexObjectGenericLocatorGroup locatorGroupType;
	RegisterObjectType( locatorGroupType, MakeFunctorRet(WriteLocatorInfo) );

	rexObjectGenericNurbsCurveGroup curveGroupType;
	RegisterObjectType( curveGroupType, MakeFunctorRet(WriteCurveInfo) );

	rexObjectGenericLightGroup lightGroupType;
	RegisterObjectType( lightGroupType, MakeFunctorRet(WriteLightInfo) );

	rexObjectLevelInstanceGroup levelInstanceGroup;
	RegisterObjectType( levelInstanceGroup, MakeFunctorRet(WriteLevelInstances) );

	rexObjectRmworldDistrict districtType;
	RegisterObjectType( districtType );
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

rexResult	rexSerializerRmworldRoomType::Serialize( rexObject& object ) const
{	
	rexResult result( rexResult::PERFECT );

	if( !GetExportData() )
		return result;

	const rexObjectRoomType* roomType = dynamic_cast<const rexObjectRoomType*>( &object );

	if( !roomType )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	rexUtility::CreatePath( GetOutputPath() );
	ASSET.SetPath( GetOutputPath() );

	fiStream* fiStream = ASSET.Create( roomType->m_Name, "roomType" );
	if( fiStream )
	{
		fiAsciiTokenizer tok;
		tok.Init( roomType->m_Name, fiStream );

		char buffer[512];

		sprintf( buffer, "RoomType %s\r\n", (const char*) roomType->m_Name );
		fiStream->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "{\r\n" );
		fiStream->Write( buffer, (int)strlen( buffer ) );
		if( roomType->m_AmbientSoundName.GetLength() )
		{
			sprintf( buffer, "\tAmbientSoundName %s\r\n", (const char*)roomType->m_AmbientSoundName );
			fiStream->Write( buffer, (int)strlen( buffer ) );
			sprintf( buffer, "\tAmbientSoundVolume %f\r\n", roomType->m_AmbientSoundVolume );
			fiStream->Write( buffer, (int)strlen( buffer ) );
		}
		sprintf( buffer, "\tReverbType %d\r\n", roomType->m_ReverbType );
		fiStream->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "\tReverbVolume %f\r\n", roomType->m_ReverbVolume );
		fiStream->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "\tUserData1 %f\r\n", roomType->m_UserData0 );
		fiStream->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "\tUserData2 %f\r\n", roomType->m_UserData1 );
		fiStream->Write( buffer, (int)strlen( buffer ) );
		sprintf( buffer, "}\r\n" );
		fiStream->Write( buffer, (int)strlen( buffer ) );

		fiStream->Close();
	}	
	else
	{
		// Oh dear, something bad has happened
		result.Set( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );
		
		// Get full path for debugging
		char acFullPathForDebugging[255];
		ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging),roomType->m_Name, "roomType" );

		// Flip the slashes
		for(int s=0; s<sizeof(acFullPathForDebugging); s++) if(acFullPathForDebugging[s] == '\\') acFullPathForDebugging[s] = '/';

		// Error
		result.AddError( "Could not create file '%s'", acFullPathForDebugging);
	}

	return result;
}
