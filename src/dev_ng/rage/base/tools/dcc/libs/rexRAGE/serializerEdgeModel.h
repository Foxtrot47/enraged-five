// 
// rexRAGE/serializerEdgeModel.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSerializerRAGEEdgeModel
//			 -- generates .em files from generic edge model hierarchy information
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexSerializer
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_RMCORE_SERIALIZER_GENERICEDGEMODELHIERARCHYTORMCOREEDGEMODEL_H__
#define __REX_RMCORE_SERIALIZER_GENERICEDGEMODELHIERARCHYTORMCOREEDGEMODEL_H__

#include "rexBase/serializer.h"
#include "rexGeneric/objectMesh.h"

namespace rage {

class rexObjectGenericEdgeModel;
class edgeModel;

/////////////////////////////////////////////////////////////////////////////////////

class rexSerializerRAGEEdgeModel : public rexSerializer
{
public:
	rexSerializerRAGEEdgeModel() : rexSerializer() { m_UseChildrenOfLevelInstanceNodes = false; }
	virtual rexResult	Serialize( rexObject& object ) const;
	virtual rexSerializer* CreateNew() const { return new rexSerializerRAGEEdgeModel(); }

	void SetUseChildrenOfLevelInstanceNodes( bool b )						{ m_UseChildrenOfLevelInstanceNodes = b; }
	bool GetUseChildrenOfLevelInstanceNodes() const							{ return m_UseChildrenOfLevelInstanceNodes; }

protected:
	rexResult	SerializeRecursive( rexObject& object ) const;
	void		SetTriangles(const rexObjectGenericMesh& inputMesh, edgeModel& em) const;

	bool		m_UseChildrenOfLevelInstanceNodes;
};

} // namespace rage

#endif
