
#define REQUIRE_IOSTREAM

#include "rex/rexMemory.h"
#include "rexRAGE/serializerAnimation.h"
#include "rexBase/module.h"
#include "rexBase/utility.h"
#include "rexBase/shell.h"

#include "file/asset.h"
#include "file/stream.h"

#include "cranimation/animation.h"
#include "cranimation/animTrack.h"
#pragma warning( disable : 4668 )
#include <windows.h>

#include "vectormath/legacyconvert.h"

namespace rage {

	static const float TIME_TO_FRAME_TOLERANCE = 0.001f;

	rexSerializerRAGEAnimation::rexSerializerRAGEAnimation() : rexSerializer()
	{
		m_SkipLockedChannel = false;
		m_SerializeAnimCurves = false;
		m_GimbelLockFix = false;
		m_ChannelGroupCount = 0;
		m_ProcessNonJointTransformAndRotates = false;
		m_ExportAllUntaggedChunksSeperately = true;
		m_UseTimeLine = false;
		m_ExportMoverChannels = false;
		m_NormalizeRoot = true;
		m_CompressionErrorTolerance = SMALL_FLOAT;
		m_ProjectFlags = 0;
		m_CompressionCost = (int)crAnimTolerance::kCompressionCostDefault;
		m_DecompressionCost = (int)crAnimTolerance::kDecompressionCostDefault;
		m_bUseAnimCtrlExportFile = false;
		m_MaximumBlockSize = 0;
	}


	///////////////////////////////////////////////////////////////////////////////////////////

	rexResult rexSerializerRAGEAnimation::Serialize( rexObject& object ) const
	{
		rexResult result( rexResult::PERFECT );

		if( !GetExportData() )
			return result;

		rexUtility::CreatePath( GetOutputPath() );
		ASSET.SetPath( GetOutputPath() );

		rexObjectGenericAnimation* inputAnimation = dynamic_cast<rexObjectGenericAnimation*>( &object );

		if( !inputAnimation )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		int childCount = inputAnimation->m_ContainedObjects.GetCount();
		if( m_ProgressBarObjectCountCB )
			(*m_ProgressBarObjectCountCB )( childCount );

		if( m_ExportAllUntaggedChunksSeperately )
		{
			// gather tagged chunk names
			atArray<atString> taggedChunkNames;
			int segmentCount = inputAnimation->m_ContainedObjects.GetCount();
			for( int s = 0; s < segmentCount; s++ )
			{
				rexObjectGenericAnimationSegment* segment = dynamic_cast<rexObjectGenericAnimationSegment*>( inputAnimation->m_ContainedObjects[ s ] );
				if( segment )
				{
					int chunkCount = segment->m_ChunkNames.GetCount();
					for( int c = 0; c < chunkCount; c++ )
					{
						int taggedChunkCount = taggedChunkNames.GetCount();
						bool found = false;
						for( int tc = 0; tc < taggedChunkCount; tc++ )
						{
							if( taggedChunkNames[ tc ] == segment->m_ChunkNames[ c ] )
							{
								found = true;
								break;
							}
						}
						if( !found )
						{
							taggedChunkNames.PushAndGrow( segment->m_ChunkNames[ c ] );
						}
					}
				}
			}

			// now serialize
			int rootChunkCount = inputAnimation->m_RootChunks.GetCount();
			for( int c = 0; c < rootChunkCount; c++ )
			{
				result &= SerializeUntaggedChunksRecursively( *inputAnimation, *inputAnimation->m_RootChunks[ c ], taggedChunkNames );
			}
		}

		for( int a = 0; a < childCount; a++ )
		{
			if( m_ProgressBarObjectCurrentIndexCB )
				(*m_ProgressBarObjectCurrentIndexCB)( a );

			rexObjectGenericAnimationSegment* segment = dynamic_cast<rexObjectGenericAnimationSegment*>( inputAnimation->m_ContainedObjects[ a ] );
			if( segment )
			{
				if( m_ProgressBarTextCB )
				{
					static char message[1024];
					sprintf( message, "Serializing animation %s.anim", (const char*)segment->GetName() );
					(*m_ProgressBarTextCB) ( message );
				}
				result &= SerializeSegment( *inputAnimation, *segment );
			}
		}

		return result;
	}


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////

	rexResult	rexSerializerRAGEAnimation::SerializeChunksRecursively( rexObjectGenericAnimation& animation, const rexObjectGenericAnimation::ChunkInfo& chunk ) const
	{
		atArray<atString> dummyArray;
		return SerializeUntaggedChunksRecursively( animation, chunk, dummyArray );
	}

	rexResult	rexSerializerRAGEAnimation::SerializeUntaggedChunksRecursively( rexObjectGenericAnimation& animation, const rexObjectGenericAnimation::ChunkInfo& chunk, const atArray<atString>& taggedChunks ) const
	{
		rexResult result( rexResult::PERFECT );

		int taggedChunkCount = taggedChunks.GetCount();
		bool serialize = true;

		for( int tc = 0; tc < taggedChunkCount; tc++ )
		{
			if( taggedChunks[ tc ] == chunk.m_Name )
			{
				serialize = false;
				break;
			}
		}

		if( serialize )
		{
			rexObjectGenericAnimationSegment segment;
			segment.SetName( chunk.m_Name );
			segment.m_Looping = true;
			segment.m_StartTime = chunk.GetStartTime();
			segment.m_EndTime = chunk.GetEndTime();
			segment.m_ChunkNames.PushAndGrow( chunk.m_Name );
			segment.m_Autoplay = true;
			segment.m_IgnoreChildBones = true;

			result &= SerializeSegment( animation, segment );

			int childCount = chunk.m_Children.GetCount();
			for( int c = 0; c < childCount; c++ )
				result &= SerializeUntaggedChunksRecursively( animation, *chunk.m_Children[ c ], taggedChunks );
		}

		return result;
	}

	/////////////////////////////////////////////////////////////////////////

	rexResult rexSerializerRAGEAnimation::SerializeSegment( rexObjectGenericAnimation& animation, rexObjectGenericAnimationSegment& segment ) const
	{
		rexResult result( rexResult::PERFECT );

		result = SerializeAni8(animation, segment);

		return result;
	}

	static rexObjectGenericAnimation::TrackInfo* FindTrackInfo( atArray<rexObjectGenericAnimation::TrackInfo*>& tracks, const char *name )
	{
		int count = tracks.GetCount();
		for( int a = 0; a < count; a++ )
		{
			if( tracks[ a ]->m_Name == name )
				return tracks[ a ];
		}
		return NULL;
	}

	static rexObjectGenericAnimation::TrackInfo* FindNextTrackInfo( atArray<rexObjectGenericAnimation::TrackInfo*>& tracks, const char *name, rexObjectGenericAnimation::TrackInfo* trackInfo )
	{
		int count = tracks.GetCount();
		for( int a = 0; a < count; a++ )
		{
			if( tracks[ a ] == trackInfo)
			{
				for( ++a; a<count; a++ )
				{
					if( tracks[ a ]->m_Name == name )
						return tracks[ a ];
				}
			}
		}
		return NULL;
	}

	/*
	PURPOSE:
	Compute stride vector.
	PARAMS:
	animation -
	st - start time.
	et - end time.
	stride - output.
	RETURN
	none.
	*/
	void rexSerializerRAGEAnimation::ComputeStrideVector(rexObjectGenericAnimation& animation, float st, float et, Vector3& stride) const
	{
		et, st;

		if( !animation.m_RootChunks.GetCount() )
			return;

		Vector3 rootBonePos0(0.0f, 0.0f, 0.0f);
		Vector3 rootBonePos1(0.0f, 0.0f, 0.0f);

		rexObjectGenericAnimation::TrackInfo* t = FindTrackInfo( animation.m_RootChunks[0]->m_Tracks, "translate" );

		if( t )
		{
			t->GetVector3Key(0, rootBonePos0);
			t->GetVector3Key(t->GetNumKeys()-1, rootBonePos1);
		}

#if 0
		int moverChannelCount = animation.m_MoverChannelData.GetCount();
		if( moverChannelCount )
		{
			Matrix34 moverMatrix0, moverMatrix1;
			moverMatrix0.Identity();
			moverMatrix1.Identity();
			Vector3 eulers0(0.0f, 0.0f, 0.0f);
			Vector3 eulers1(0.0f, 0.0f, 0.0f);

			rexObjectGenericAnimation::ChannelInfo* tx = FindChannelInfo( animation.m_MoverChannelData, "translateX" );
			rexObjectGenericAnimation::ChannelInfo* ty = FindChannelInfo( animation.m_MoverChannelData, "translateY" );
			rexObjectGenericAnimation::ChannelInfo* tz = FindChannelInfo( animation.m_MoverChannelData, "translateZ" );
			rexObjectGenericAnimation::ChannelInfo* rx = FindChannelInfo( animation.m_MoverChannelData, "rotateX" );
			rexObjectGenericAnimation::ChannelInfo* ry = FindChannelInfo( animation.m_MoverChannelData, "rotateY" );
			rexObjectGenericAnimation::ChannelInfo* rz = FindChannelInfo( animation.m_MoverChannelData, "rotateZ" );

			if( tx && ty && tz )
			{
				moverMatrix0.d.Set( GetValueForTime( *tx, st ), GetValueForTime( *ty, st ), GetValueForTime( *tz, st ) );
				moverMatrix1.d.Set( GetValueForTime( *tx, et ), GetValueForTime( *ty, et ), GetValueForTime( *tz, et ) );
			}

			if( rx && ry && rz )
			{
				eulers0.Set( GetValueForTime( *rx, st ), GetValueForTime( *ry, st ), GetValueForTime( *rz, st ) );
				eulers1.Set( GetValueForTime( *rx, et ), GetValueForTime( *ry, et ), GetValueForTime( *rz, et ) );
			}

			moverMatrix0.FromEulersXYZ( eulers0 );
			moverMatrix1.FromEulersXYZ( eulers1 );

			moverMatrix0.UnTransform( rootBonePos0 );
			moverMatrix1.UnTransform( rootBonePos1 );
		}
#endif

		stride.Subtract( rootBonePos1, rootBonePos0 );
	}

	/*
	PURPOSE
	Count all valid channels.
	PARAMS
	animation - for which the channels to be counted.
	RETURN
	the total number of valid channels.
	NOTES
	*/
	int rexSerializerRAGEAnimation::GetTotalDOFs(rexObjectGenericAnimation& animation, rexObjectGenericAnimationSegment& segment, bool skippedLockedChannels) const
	{
		int count = 0;
		int chunkCount = segment.GetChunkCount( animation );

		for( int a = 0; a < chunkCount; a++ )
		{
			rexObjectGenericAnimation::ChunkInfo* chunk = segment.GetChunkByIndex( animation, a );
			Assert( chunk );
			GetDOFCount( chunk->m_Tracks, count, skippedLockedChannels, ( a == 0 ) );
		}

		return count;
	}

	/*
	PURPOSE
	Count all valid channels for the given bone.
	PARAMS
	bone - for which the channels to be counted.
	count - output.
	RETURN
	none.
	NOTES
	*/
	void rexSerializerRAGEAnimation::GetNodeDOFCount(const rexObjectGenericAnimation::ChunkInfo& bone, int& count, bool skippedLockedChannels, bool isRootBone) const
	{
		// For this node.
		GetDOFCount( bone.m_Tracks, count, skippedLockedChannels, isRootBone );

		// For child nodes.
		int childCount = bone.m_Children.GetCount();
		for( int c = 0; c < childCount; c++ )
		{
			GetNodeDOFCount( *bone.m_Children[c], count, skippedLockedChannels, false );
		}
	}

	/*
	PURPOSE
	Count all valid channels for the given bone.
	PARAMS
	channelArray - for which the channels to be counted.
	count - output.
	RETURN
	none.
	NOTES
	*/
	void rexSerializerRAGEAnimation::GetDOFCount(const atArray<rexObjectGenericAnimation::TrackInfo*>& trackArray, int& count, bool skippedLockedChannels, bool isRootBone) const
	{
		int totalTracks = trackArray.GetCount();
		for( int i = 0; i < totalTracks; i++ )
		{
			for(char j=0; j<3; j++)
			{
				atString name = trackArray[i]->m_Name;
				name += 'X' + j;
				bool* pOnlyRootBone = m_ChannelInputNameMap.Access(name);
				if( pOnlyRootBone && !( *pOnlyRootBone && !isRootBone ) && ( !skippedLockedChannels || !trackArray[i]->m_IsLocked ))
					count++;
			}
		}
	}

/*
	// TODO --- MOVE TO GET KEY VALUE

	static float GetValueForTime( rexObjectGenericAnimation::TrackInfo& channel, float currTime )
	{
		int keyFrameCount = channel.m_Keyframes.GetCount();
		if( channel.mIsAnimCurve )
		{
			return EvaluateAnimCurve( &channel, currTime );
		}
		else if( !keyFrameCount )
		{
			return channel.m_InitialValue;
		}
		// This is anim data for all frames. Get the frame data.
		else
		{
			bool gotIt = false;
			for( int a = 0; a < keyFrameCount; a++ )
			{
				// we give a tolerance here for precision error to get exact frame
				if( channel.m_Keyframes[ a ].m_Time >= ( currTime - TIME_TO_FRAME_TOLERANCE ) )
				{
					return channel.m_Keyframes[a].m_Value;
				}
			}
			if( !gotIt )
				return channel.m_Keyframes[ keyFrameCount - 1 ].m_Value;
		}
		Assert(0);
		return 0;
	}
*/
	/*
	PURPOSE
	Finds the animation export control file track specification for a given bone.
	PARAMS
	bone - the bone to search for the track specification for.
	bIsRoot - boolean indicating whether the supplied bone is a root bone.
	RETURN
	The export track specification, or NULL if a specification could not be found for the bone.
	NOTES
	*/
	const AnimExportCtrlTrackSpec* rexSerializerRAGEAnimation::FindAnimCtrlFileTrackSpec(rexObjectGenericAnimation::ChunkInfo* bone, bool bIsRoot) const
	{
		const AnimExportCtrlTrackSpec* trackSpec = NULL;

		if(!m_bUseAnimCtrlExportFile)
			return NULL;

		if(bIsRoot)
		{
			trackSpec = m_AnimExportCtrlSpec.FindTrackSpec("root");
			if(!trackSpec)
			{
				//The "root" keyword wasn't listed in the control spec, so try getting the spec by the bone name
				trackSpec = m_AnimExportCtrlSpec.FindTrackSpec( (const char*)bone->m_Name );
			}
		}
		else
		{
			//The bone isn't the root, so retrieve the track spec by name
			trackSpec = m_AnimExportCtrlSpec.FindTrackSpec( (const char*)bone->m_Name );
			if( !trackSpec || (trackSpec && (strcmp(trackSpec->GetNameExpr(),"*") == 0)) )
			{
				//Either a track spec wasn't found, or the spec that was found includes all bones, so try
				//to search the bones parent bones looking for a track specification that matches one of them
				//which is marked to include its children.
				rexObjectGenericAnimation::ChunkInfo* parentBone = bone->m_Parent;
				while(parentBone)
				{
					const AnimExportCtrlTrackSpec* parentTrackSpec = m_AnimExportCtrlSpec.FindTrackSpec( (const char*)parentBone->m_Name );
					if(parentTrackSpec)
					{
						if(parentTrackSpec->IncludesChildren() && (strcmp(parentTrackSpec->GetNameExpr(), "*") != 0))
						{
							//The parent track specification isn't the default fallthrough of "*", and is marked
							//as including its children, so use this track specification
							trackSpec = parentTrackSpec;
							break;
						}
					}
					parentBone = parentBone->m_Parent;
				}
			}
		}
		return trackSpec;
	}

	// Project specific flags used in crAnimation
	// Note that FLAGS_RESERVED_FOR_RAGE_USE_ONLY = 0x0000ffff
	// and that FLAGS_AVAILABLE_FOR_PROJECT_USE = 0xffff0000
	// These will be shifted 1<<16 when stored in crAnimation
	enum eInternalAnimFlag
	{
		// Tracks that extract angular velocity need their translation track to be
		// unwound so that the rotation is removed.
		// Set this flag after unwinding the rotation from the translation.
		ANIM_FLAG_ROTATION_UNWOUND			= (1<<0),

		// Set if the track has an event track
		ANIM_FLAG_EVENT_TRACK				= (1<<1),

		// Set if the track has an independent mover track
		ANIM_FLAG_INDEPENDENT_MOVER_TRACK	= (1<<2),

		// Set if the track has audio hash key track
		ANIM_FLAG_AUDIO_TRACK				= (1<<3),

		// Set if the track has a gesture speaker track
		ANIM_FLAG_GESTURE_SPEAKER_TRACK		= (1<<4),

		// Set if the track has a gesture listener track
		ANIM_FLAG_GESTURE_LISTENER_TRACK	= (1<<5),

		// Set if the track has a facial speaker track
		ANIM_FLAG_FACIAL_SPEAKER_TRACK		= (1<<6),

		// Set if the track has a facial listener track
		ANIM_FLAG_FACIAL_LISTENER_TRACK		= (1<<7),

		// Set if the track has a ambient flags track
		ANIM_FLAG_AMBIENT_FLAGS_TRACK		= (1<<8),

		ANIM_FLAG_MAX						= (1<<15)
	};


	rexResult rexSerializerRAGEAnimation::SerializeAni8(rexObjectGenericAnimation& animation, rexObjectGenericAnimationSegment& segment) const
	{
		rexResult result(rexResult::PERFECT);

		int boneCount = segment.GetChunkCount(animation);
		if( !boneCount && !animation.m_GenericChunk )
		{
			result.Combine( rexResult::ERRORS );
			result.AddError("Animation segment %s has no bones/chunks assigned!", (const char*)segment.GetName());
			return result;
		}

		// get data needed for main compilation
		float startTime, endTime;
		animation.GetTimeExtents(segment.m_ChunkNames, startTime, endTime);

		if(segment.m_StartTime >= 0.f)
		{
			startTime = segment.m_StartTime;
		}

		if(segment.m_EndTime >= 0.f)
		{
			endTime = segment.m_EndTime;
		}

		// ensure endTime is on a m_FramesPerSecond frame
		float duration = endTime-startTime+TIME_TO_FRAME_TOLERANCE;
		int numFrames = static_cast<int>(floorf(duration * animation.m_FramesPerSecond));
		endTime = startTime + (static_cast<float>(numFrames) / animation.m_FramesPerSecond);

		duration = endTime-startTime+TIME_TO_FRAME_TOLERANCE;

		int startFrame = int((startTime-animation.m_StartTime+TIME_TO_FRAME_TOLERANCE) * animation.m_FramesPerSecond);

		if(!numFrames)
		{
			return result;
		}

		// add "zero length" frame at end of animation
		numFrames++;

		float conversionFactor = GetUnitTypeLinearConversionFactor();

		// channel export list

		bool boneTranslate = false; bool boneTranslateRootOnly = true;
		bool boneRotate = false; bool boneRotateRootOnly = true;
		bool boneScale = false; bool boneScaleRootOnly = true;

		//If an animation control file isn't in use, we need to search through the channel arrays setup by the ChannelsToWrite
		//export property to determine if translation and/or rotation channels will be written, and if they are for the root
		//bone only.
		if(!m_bUseAnimCtrlExportFile)
		{
			Assert(m_ChannelInputNames.GetCount() == m_ChannelOutputNames.GetCount());
			Assert(m_ChannelInputNames.GetCount() == m_ChannelSupportedForRootBoneOnly.GetCount());

			m_ChannelInputNameMap.Kill();

			int inputChannelCount = m_ChannelInputNames.GetCount();
			for(int a=0; a<inputChannelCount; a++)
			{
				if(m_ChannelInputNameMap.Access(m_ChannelInputNames[a]))
				{
					result.Combine(rexResult::ERRORS, rexResult::INVALID_INPUT);
					result.AddError("Channel Input Name '%s' added twice!", (const char*) m_ChannelInputNames[a]);
					return result;
				}
				m_ChannelInputNameMap.Insert(m_ChannelInputNames[a], m_ChannelSupportedForRootBoneOnly[a]);

				if(m_ChannelInputNames[a] == "translateX" || m_ChannelInputNames[a] == "translateY" || m_ChannelInputNames[a] == "translateZ")
				{
					boneTranslate = true;
					boneTranslateRootOnly = boneTranslateRootOnly && m_ChannelSupportedForRootBoneOnly[a];
				}
				else if(m_ChannelInputNames[a] == "rotateX" || m_ChannelInputNames[a] == "rotateY" || m_ChannelInputNames[a] == "rotateZ")
				{
					boneRotate = true;
					boneRotateRootOnly = boneRotateRootOnly && m_ChannelSupportedForRootBoneOnly[a];
				}
				else if(m_ChannelInputNames[a] == "scaleX" || m_ChannelInputNames[a] == "scaleY" || m_ChannelInputNames[a] == "scaleZ")
				{
					boneScale = true;
					boneScaleRootOnly = boneScaleRootOnly && m_ChannelSupportedForRootBoneOnly[a];
				}
			}
		}//End if(!m_bUseAnimCtrlExportFile)

		// collect mover data
		int maxMoverCount = animation.m_MoverChunks.GetCount();

		atArray< atArray<Matrix34> > moverMatrixByIndexAndFrame;
		atArray< u16 > moverIds;

		moverMatrixByIndexAndFrame.Reserve(maxMoverCount);
		moverIds.Reserve(maxMoverCount);

		if(!animation.m_MultiMoverMode && maxMoverCount > 1)
		{
			result.Combine(rexResult::ERRORS);
			result.AddError("Multiple movers passed to serializer, but multi-mover option not enabled - mover ids will clash");
			return result;
		}

		for(int i=0; i<maxMoverCount; i++)
		{
			rexObjectGenericAnimation::TrackInfo* tMover = FindTrackInfo(animation.m_MoverChunks[i]->m_MoverTracks, "translate");
			rexObjectGenericAnimation::TrackInfo* rMover = FindTrackInfo(animation.m_MoverChunks[i]->m_MoverTracks, "rotate");

			if(tMover && rMover)
			{
				atArray<Matrix34>& moverMatrices = moverMatrixByIndexAndFrame.Grow();
				u16& moverId = moverIds.Grow();

				moverMatrices.Reserve(numFrames);

				for(int f=0; f<numFrames; f++)
				{
					Matrix34& moverMatrix = moverMatrices.Grow();

					Vector3 moverTrans;
					tMover->GetVector3Key(f+startFrame, moverTrans);
					moverMatrix.d.Set(moverTrans);

					Quaternion moverRot;
					rMover->GetQuaternionKey(f+startFrame, moverRot);
					moverMatrix.FromQuaternion(moverRot);
				}

				moverId = 0;
				if(animation.m_MultiMoverMode)
				{
					const char* text = animation.m_MoverChunks[i]->m_MoverName;
					if(animation.m_MoverChunks[i]->m_MoverId.GetLength() && animation.m_MoverChunks[i]->m_MoverId != ".")
					{
						text = animation.m_MoverChunks[i]->m_MoverId;
					}
					moverId = crAnimTrack::ConvertMoverNameToId(text);

					// check for duplication
					for(int n=0; n<(moverIds.GetCount()-1); n++)
					{
						if(moverId == moverIds[n])
						{
							result.Combine(rexResult::ERRORS);
							result.AddErrorCtx(animation.m_MoverChunks[i]->m_MoverName.c_str(), "Duplicate mover ids found on export, '%s' clashes with another mover, both producing mover id '%d'", (const char*)animation.m_MoverChunks[i]->m_MoverName, moverId);
							return result;
						}
					}
				}
			}
		}

		int moverCount = moverMatrixByIndexAndFrame.GetCount();
		bool hasMoverTracks = moverCount>0;

		// collect bone translate and rotate info, set up bone id hashes
		atArray< atArray<Matrix34> > matricesByBoneAndFrame;
		atArray< u16 > boneIDs;

		matricesByBoneAndFrame.Reserve(boneCount);
		boneIDs.Reserve(boneCount);

		bool boneIDsUsed = false;

		for(int b=0; b<boneCount; b++)
		{
			rexObjectGenericAnimation::ChunkInfo* bone = segment.GetChunkByIndex( animation, b );
			u16 boneIndex = u16(animation.GetIndexByChunkName(bone->m_Name));

			if(!bone)
			{
				result.Combine( rexResult::ERRORS );
				result.AddErrorCtx(bone->m_Name.c_str(), "Animation segment '%s' has improper bone tagged", (const char*)segment.GetName() );
				return result;
			}

			atArray<Matrix34>& boneMatrices = matricesByBoneAndFrame.Grow();
			boneMatrices.Reserve(numFrames);

			rexObjectGenericAnimation::TrackInfo* tbone = FindTrackInfo(bone->m_Tracks, "translate");
			rexObjectGenericAnimation::TrackInfo* rbone = FindTrackInfo(bone->m_Tracks, "rotate");
			rexObjectGenericAnimation::TrackInfo* sbone = FindTrackInfo(bone->m_Tracks, "scale");

			for(int f=0; f<numFrames; f++)
			{
				Matrix34& matrix = boneMatrices.Grow((u16) numFrames);
				matrix.Identity();

				if(rbone)
				{
					Quaternion boneRot;
					rbone->GetQuaternionKey(f+startFrame, boneRot);
					matrix.FromQuaternion(boneRot);
				}

				if(tbone)
				{
					Vector3 boneTrans;
					tbone->GetVector3Key(f+startFrame, boneTrans);
					matrix.d.Set(boneTrans);
				}

				if(sbone)
				{
					Vector3 boneScale;
					sbone->GetVector3Key(f+startFrame, boneScale);
					matrix.a.Scale(boneScale.x);
					matrix.b.Scale(boneScale.y);
					matrix.c.Scale(boneScale.z);
				}
			}

			u16 boneID = boneIndex;

			if(GetUseBoneIDs() || bone->m_BoneID.GetLength())
			{
				// if not root, hash the bone id (or bone name)
				if(boneIndex || segment.m_DontExportRoot)
				{
					const char* text = bone->m_ShortName;
					if(bone->m_BoneID.GetLength() && bone->m_BoneID != ".")
					{
						text = bone->m_BoneID;
					}
					boneID = crAnimTrack::ConvertBoneNameToId(text);
					boneIDsUsed = true;
				}
				else
				{
					if(bone->m_BoneID.GetLength())
					{
						result.AddMessageCtx(bone->m_Name.c_str(), "Bone ID '%s' found on root bone '%s' (ignoring, this is a warning only)", (const char*)bone->m_BoneID, (const char*)bone->m_Name);
					}
				}
			}

			// check for duplication, or zero (which is reserved for the root)
			if(!boneID && boneIndex)
			{
				result.Combine(rexResult::ERRORS);
				result.AddErrorCtx(bone->m_Name.c_str(), "Bone ID '%s' from bone '%s' hashes to value reserved for root bones only", (const char*)bone->m_BoneID, (const char*)bone->m_Name);
				return result;
			}
			for(int i=0; i<boneIDs.GetCount(); i++)
			{
				if(boneID == boneIDs[i])
				{
					rexObjectGenericAnimation::ChunkInfo* boneClash = segment.GetChunkByIndex( animation, i );

					result.Combine(rexResult::ERRORS);
					result.AddErrorCtx(bone->m_Name.c_str(),"Bone ID '%s' from bone '%s' clashes when hashed with bone ID '%s' from bone '%s'", (const char*)bone->m_BoneID, (const char*)bone->m_Name, (const char*)boneClash->m_BoneID, (const char*)boneClash->m_Name);
					return result;
				}
			}

			// assign bone ID
			boneIDs.Append() = boneID;
		}

		// normalize the root and apply the parent transform
		if(boneCount)
		{
			for(int f=0; f<numFrames; f++)
			{
				Matrix34& rootMatrix = matricesByBoneAndFrame[0][f];
				rootMatrix.Dot(animation.m_ParentMatrix);
// We can do the parenting in MB now as we are not using biped
#if !HACK_GTA4
				if(m_NormalizeRoot && hasMoverTracks)
				{
					for(int i=0; i<moverCount; i++)
					{
						Matrix34 moverMatrix(moverMatrixByIndexAndFrame[i][f]);
						moverMatrix.Inverse();

						rootMatrix.Dot(moverMatrix);
					}
				}
#endif // HACK_GTA4
			}
		}

		// adjust translates and rotates
		for( int b=0; b<boneCount; b++)
		{
			const rexObjectGenericAnimation::ChunkInfo* bone = segment.GetChunkByIndex(animation, b);
			if(!bone)
			{
				result.Combine( rexResult::ERRORS );
				result.AddErrorCtx(bone->m_Name.c_str(), "Animation segment '%s' has improper bone tagged", (const char*)segment.GetName() );
				return result;
			}

			if(m_ProcessNonJointTransformAndRotates && !bone->m_IsJoint)
			{
				for(int f=0; f<numFrames; f++)
				{
					// adjust transforms
					Matrix34& boneMatrix = matricesByBoneAndFrame[b][f];

					Matrix44 mTrans;
					mTrans.Dot(bone->m_InitialTransformationMatrix, bone->m_InitialExclusiveMatrix);

					Vector3 vIn;
					vIn = boneMatrix.d;
					vIn -= Vector3(bone->m_InitialTransformationMatrix.d.x, bone->m_InitialTransformationMatrix.d.y, bone->m_InitialTransformationMatrix.d.z);

					Vector3 vOut;
					mTrans.Transform3x3(vIn, vOut);
					vOut += Vector3(bone->m_BoneMatrix.d.x, bone->m_BoneMatrix.d.y, bone->m_BoneMatrix.d.z);

					boneMatrix.d = vOut;

					// adjust rotates
					Matrix34 mRot;
					bone->m_InitialTransformationMatrix.ToMatrix34(mRot);
					mRot.Inverse();

					boneMatrix.Dot3x3(mRot);
				}
			}
		}

		// apply conversion factor
		if(conversionFactor != 1.f)
		{
			for(int f=0; f<numFrames; f++)
			{
				for(int b=0; b<boneCount; b++)
				{
					Matrix34& boneMatrix = matricesByBoneAndFrame[b][f];

					boneMatrix.d *= conversionFactor;
				}

				for(int i=0; i<moverCount; i++)
				{
					Matrix34& moverMatrix = moverMatrixByIndexAndFrame[i][f];

					moverMatrix.d *= conversionFactor;
				}
			}
		}

		// calculate stride vector
		Vector3 stride(0.f, 0.f, 0.f);
		ComputeStrideVector(animation, startTime, endTime, stride);
		stride.Scale(conversionFactor);

		// compression tolerance
		crAnimToleranceSimple compressionTolerance;
		bool isLossless;
		u32 maxBlockSize;

		// Compression can only be enabled for non-64bit builds.  In 64bit builds any user defined compression settings will
		// be discarded and a lossless animation will be exported.
#if !__64BIT 
		compressionTolerance.Init(
			GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError,
			GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError,
			GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError,
			GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError,
			crAnimTolerance::eDecompressionCost(GetDecompressionCost()), crAnimTolerance::eCompressionCost(GetCompressionCost()));
		
		isLossless = InRange(GetCompressionErrorTolerance(), 0.f, SMALL_FLOAT);

		// check if compression is lossy or lossless
		if(m_bUseAnimCtrlExportFile)
		{
			for(int i=0; i<boneCount; i++)
			{
				u16 boneId = u16(boneIDs[i]);
				bool isRoot = !boneId;

				rexObjectGenericAnimation::ChunkInfo* bone = segment.GetChunkByIndex(animation, i);
				const AnimExportCtrlTrackSpec* trackSpec = FindAnimCtrlFileTrackSpec(bone, isRoot);
				if(!trackSpec)
				{
#if HACK_GTA4
					result.Combine(rexResult::WARNINGS);
					result.AddWarning("No animation export specification could be found for the joint '%s'", (const char*)bone->m_Name);
					continue;
#else
					result.Combine(rexResult::ERRORS);
					result.AddMessage("No animation export specification could be found for the joint '%s'", (const char*)bone->m_Name);
					return result;
#endif // HACK_GTA4
				}
				else
				{

				

					const AnimExportCtrlTrack *pTransCtrlTrack = trackSpec->GetTrackByInputName("translate");
					if(pTransCtrlTrack)
					{
						isLossless = isLossless && ((pTransCtrlTrack->GetCompressionTolerance() < 0.f) || InRange(pTransCtrlTrack->GetCompressionTolerance(), 0.f, SMALL_FLOAT));
					}

					const AnimExportCtrlTrack *pRotCtrlTrack = trackSpec->GetTrackByInputName("rotate");
					if(pRotCtrlTrack)
					{
						isLossless = isLossless && ((pRotCtrlTrack->GetCompressionTolerance() < 0.f) || InRange(pRotCtrlTrack->GetCompressionTolerance(), 0.f, SMALL_FLOAT));
					}

					const AnimExportCtrlTrack *pScaleCtrlTrack = trackSpec->GetTrackByInputName("rotate");
					if(pScaleCtrlTrack)
					{
						isLossless = isLossless && ((pScaleCtrlTrack->GetCompressionTolerance() < 0.f) || InRange(pScaleCtrlTrack->GetCompressionTolerance(), 0.f, SMALL_FLOAT));
					}
				}
			}

			if(result == false)
				return result;
		}

		// maximum block size, zero indicates no block size search - just use default frames per chunk
		// can only perform block size calculations if rage allocators are enabled
	#if USE_RAGE_ALLOCATORS
		maxBlockSize = GetMaximumBlockSize();
	#else 
		maxBlockSize = 0;
	#endif //USE_RAGE_ALLOCATORS

#else
		compressionTolerance.Init( SMALL_FLOAT, SMALL_FLOAT, SMALL_FLOAT, SMALL_FLOAT,
			crAnimTolerance::eDecompressionCost(GetDecompressionCost()), crAnimTolerance::eCompressionCost(GetCompressionCost()));

		isLossless = true;
		maxBlockSize = CR_DEFAULT_MAX_BLOCK_SIZE;

#endif //!__64BIT

		// frames per chunk - next multiple of 16, minus 1
		u16 framesPerChunk = (maxBlockSize>0) ? u16(((numFrames+16)&(~15))-1) : crAnimation::sm_DefaultFramesPerChunk;
		u16 numBlocks = 1;

		do 
		{
			// animation used for exporting
			crAnimation exportAnim;

			// duration needs to be based on (numFrames-1)
			float durationOfClip = float(numFrames-1) / animation.m_FramesPerSecond;
			exportAnim.Create(numFrames, durationOfClip, segment.m_Looping, hasMoverTracks, framesPerChunk, isLossless);

			// export bone and mover rotate and translate tracks
			// TODO --- support scale here too
			for(int i=0; i<boneCount; i++)
			{
				u16 boneId = u16(boneIDs[i]);
				bool isMover = false;

				atArray<Matrix34>& am = matricesByBoneAndFrame[i];

				bool isRoot = !boneId;
				bool translateTrack, rotateTrack, scaleTrack, genericTranslateTrack, genericRotateTrack;

				translateTrack = false;
				rotateTrack = false;
				scaleTrack = false;
				genericTranslateTrack = false;
				genericRotateTrack = false;

				if(!m_bUseAnimCtrlExportFile)
				{
					translateTrack = boneTranslate && (isRoot || !boneTranslateRootOnly);
					rotateTrack = boneRotate && (isRoot || !boneRotateRootOnly);
					scaleTrack = boneScale && (isRoot || !boneScaleRootOnly);
				}
				else
				{
					rexObjectGenericAnimation::ChunkInfo* bone = segment.GetChunkByIndex( animation, i );

					const AnimExportCtrlTrackSpec* trackSpec = FindAnimCtrlFileTrackSpec(bone, isRoot);

					if(!trackSpec)
					{
#if HACK_GTA4
						if(!bone->m_bSuppressWarnings)
						{
							result.Combine(rexResult::WARNINGS);
							result.AddWarningCtx(bone->m_Name.c_str(),"No animation export specification could be found for the joint '%s'", (const char*)bone->m_Name);
						}
						continue;
#else
						result.Combine(rexResult::ERRORS);
						result.AddMessageCtx(bone->m_Name.c_str(),"No animation export specification could be found for the joint '%s'", (const char*)bone->m_Name);
						return result;
#endif // HACK_GTA4
					}

					float transCompTol = -1.0f;
					float rotCompTol = -1.0f;
					float scaleCompTol = -1.0f;

					const AnimExportCtrlTrack *pTransCtrlTrack = trackSpec->GetTrackByInputName("translate");
					if(pTransCtrlTrack)
					{
						translateTrack = true;
						float compTol = pTransCtrlTrack->GetCompressionTolerance();
						if(compTol >= 0.0f)
							transCompTol = compTol;
					}

					// override the translate
					pTransCtrlTrack = trackSpec->GetTrackByInputName("genericTranslation");
					if(pTransCtrlTrack)
					{
						genericTranslateTrack = true;
						float compTol = pTransCtrlTrack->GetCompressionTolerance();
						if(compTol >= 0.0f)
							transCompTol = compTol;
					}

					const AnimExportCtrlTrack *pRotCtrlTrack = trackSpec->GetTrackByInputName("rotate");
					if(pRotCtrlTrack)
					{
						rotateTrack = true;
						float compTol = pRotCtrlTrack->GetCompressionTolerance();
						if(compTol >= 0.0f)
							rotCompTol = compTol;
					}

					pRotCtrlTrack = trackSpec->GetTrackByInputName("genericRotation");
					if(pTransCtrlTrack)
					{
						genericRotateTrack = true;
						float compTol = pRotCtrlTrack->GetCompressionTolerance();
						if(compTol >= 0.0f)
							rotCompTol = compTol;
					}

					const AnimExportCtrlTrack *pScaleCtrlTrack = trackSpec->GetTrackByInputName("scale");
					if(pScaleCtrlTrack)
					{
						scaleTrack = true;
						float compTol = pScaleCtrlTrack->GetCompressionTolerance();
						if(compTol >= 0.0f)
							scaleCompTol = compTol;
					}

					// switch to using compression tolerance values from control file, if specified
#if !__64BIT
					compressionTolerance.Init(
						transCompTol >=0.f ? transCompTol : (GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError),
						rotCompTol >=0.f ? rotCompTol : (GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError),
						scaleCompTol >=0.f ? scaleCompTol : (GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError),
						GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError,
						crAnimTolerance::eDecompressionCost(GetDecompressionCost()), crAnimTolerance::eCompressionCost(GetCompressionCost()));
#endif //!__64BIT

				}

				ExportTransformTracks(am, isMover, boneId, genericTranslateTrack, translateTrack, genericRotateTrack, rotateTrack, scaleTrack, compressionTolerance, framesPerChunk, exportAnim);
			}

			if(m_bUseAnimCtrlExportFile)
			{
#if !__64BIT
				// revert compression tolerance settings back to original values for remaining tracks
				compressionTolerance.Init(
					GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError,
					GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError,
					GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError,
					GetCompressionErrorTolerance()>=0.f?GetCompressionErrorTolerance():crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError,
					crAnimTolerance::eDecompressionCost(GetDecompressionCost()), crAnimTolerance::eCompressionCost(GetCompressionCost()));
#endif //!_64BIT
			}

			for(int i=0; i<moverCount; i++)
			{
				u16 moverId = u16(moverIds[i]);
				bool isMover = true;

				atArray<Matrix34>& am = moverMatrixByIndexAndFrame[i];

				ExportTransformTracks(am, isMover, moverId, false, true, false, true, false, compressionTolerance, framesPerChunk, exportAnim);
			}

			// export blend shape track if it exists
			if( animation.m_GenericChunk )
			{
				for( int i = 0; i < animation.m_GenericChunk->m_Tracks.GetCount(); i++ )
				{
					rexObjectGenericAnimation::TrackInfo* track = animation.m_GenericChunk->m_Tracks[i];

					// Hash the name of this track
					u16 trackName = 0;

					// Is the track a blendshape or a viseme!!!
					if (track->m_TrackID == rexObjectGenericAnimation::TRACK_BLENDSHAPE)
					{
						trackName = crAnimTrack::ConvertBoneNameToId(animation.m_GenericChunk->m_Tracks[i]->m_Name);

						// Grab all the keyframes into a local array
						atArray<float> af;
						af.Reserve(numFrames);
						for( int f = 0; f < numFrames; f++ )
						{
							float flt;
							track->GetFloatKey(startFrame + f, flt);
							af.Grow() = flt;
						}

						// Create the animation track
						crAnimTrack* animTrack = new crAnimTrack(kTrackBlendShape, trackName);
						animTrack->CreateFloat(af, compressionTolerance, framesPerChunk);
						exportAnim.CreateTrack(animTrack);
					}
					else if (track->m_TrackID == rexObjectGenericAnimation::TRACK_ANIMATED_NORMAL_MAPS)
					{
						trackName = crAnimTrack::ConvertBoneNameToId(track->m_Name);
						atArray<float> af;
						af.Reserve(numFrames);
						for( int f = 0; f < numFrames; f++ )
						{
							float flt;
							track->GetFloatKey(startFrame + f, flt);
							af.Grow() = flt;
						}

						// Create the animation track
						crAnimTrack* animTrack = new crAnimTrack(kTrackAnimatedNormalMaps, trackName);
						animTrack->CreateFloat(af, compressionTolerance, framesPerChunk);
						exportAnim.CreateTrack(animTrack);
					}
					else if (track->m_TrackID == rexObjectGenericAnimation::TRACK_FACIAL_CONTROL)
					{
						trackName = crAnimTrack::ConvertBoneNameToId(track->m_Name);
						
						Printf("Track Name : %s , Hashed : %d, TRACK_FACIAL_CONTROL\n", (const char*)track->m_Name, (int)trackName);

						atArray<float> af;
						af.Reserve(numFrames);
						for( int f = 0; f < numFrames; f++ )
						{
							float flt;
							track->GetFloatKey(startFrame + f, flt);
							af.Grow() = flt;
						}

						// Create the animation track
						crAnimTrack* animTrack = new crAnimTrack(kTrackFacialControl, trackName);
						animTrack->CreateFloat(af, compressionTolerance, framesPerChunk);
						exportAnim.CreateTrack(animTrack);
					}
					else if (track->m_TrackID == rexObjectGenericAnimation::TRACK_FACIAL_TRANSLATION)
					{
						trackName = crAnimTrack::ConvertBoneNameToId(track->m_Name);

						Printf("Track Name : %s , Hashed : %d, TRACK_FACIAL_TRANSLATION\n", (const char*)track->m_Name, (int)trackName);

						atArray<Vec3V> af;
						af.Reserve(numFrames);
						for( int f = 0; f < numFrames; f++ )
						{
							Vec3V v;
							track->GetVector3Key(startFrame + f, RC_VECTOR3(v));
							af.Grow() = v;
						}

						// Create the animation track
						crAnimTrack* animTrack = new crAnimTrack(kTrackFacialTranslation, trackName);
						animTrack->CreateVector3(af, compressionTolerance, framesPerChunk);
						exportAnim.CreateTrack(animTrack);
					}
					else if (track->m_TrackID == rexObjectGenericAnimation::TRACK_FACIAL_ROTATION)
					{
						trackName = crAnimTrack::ConvertBoneNameToId(track->m_Name);

						Printf("Track Name : %s , Hashed : %d, TRACK_FACIAL_ROTATION\n", (const char*)track->m_Name, (int)trackName);

						atArray<QuatV> af;
						af.Reserve(numFrames);
						for( int f = 0; f < numFrames; f++ )
						{
							QuatV q;
							track->GetQuaternionKey(startFrame + f, RC_QUATERNION(q));
							af.Grow() = q;
						}

						//Create the animation track
						crAnimTrack* animTrack = new crAnimTrack(kTrackFacialRotation, trackName);
						animTrack->CreateQuaternion(af, compressionTolerance, framesPerChunk);
						exportAnim.CreateTrack(animTrack);
					}
					else if (track->m_TrackID == rexObjectGenericAnimation::TRACK_VISEMES)
					{
						//trackName = (u16)atoi(animation.m_GenericChunk->m_Tracks[i]->m_Name);
						trackName = crAnimTrack::ConvertBoneNameToId(track->m_Name);

						//OutputDebugStringA("VISEMES\n");
						// Grab all the keyframes into a local array
						atArray<float> af;
						af.Reserve(numFrames);
						for( int f = 0; f < numFrames; f++ )
						{
							float flt;
							track->GetFloatKey(startFrame + f, flt);
							af.Grow() = flt;

							//static char message[1024];
							//sprintf( message, "%d = %f\n", f, flt);
							//OutputDebugStringA(message);

						}
						//OutputDebugStringA("/VISEMES\n");

						// Create the animation track
						crAnimTrack* animTrack = new crAnimTrack(kTrackVisemes, trackName);
						animTrack->CreateFloat(af, compressionTolerance, framesPerChunk);
						exportAnim.CreateTrack(animTrack);
					}
				}
			}

			s16 projectFlags = m_ProjectFlags;

			if(m_bUseAnimCtrlExportFile)
			{
				for(int b=0; b<boneCount; b++)
				{
					u16 boneId = u16(boneIDs[b]);
					rexObjectGenericAnimation::ChunkInfo* bone = segment.GetChunkByIndex(animation, b);
					bool isRoot = !boneId;

					//Get the animation control track specifications for the bone...
					const AnimExportCtrlTrackSpec* trackSpec = FindAnimCtrlFileTrackSpec(bone, isRoot);

					if(!trackSpec)
					{
#if HACK_GTA4
						if(!bone->m_bSuppressWarnings)
						{
							result.Combine(rexResult::WARNINGS);
							result.AddWarningCtx(bone->m_Name.c_str(),"No animation export specification could be found for the joint '%s'", (const char*)bone->m_Name);
						}
						continue;
#else
						result.Combine(rexResult::ERRORS);
						result.AddMessageCtx(bone->m_Name.c_str(),"No animation export specification could be found for the joint '%s'", (const char*)bone->m_Name);
						return result;
#endif // HACK_GTA4
					}

					//Export the tracks specified in the control file...
					for(int i=0; i<trackSpec->GetTrackCount(); i++)
					{
						const AnimExportCtrlTrack& ctrlTrack = trackSpec->GetTrack(i);

						u8 trackIndex;
						if(crAnimTrack::ConvertTrackNameToIndex(ctrlTrack.GetInputName(), trackIndex) )
						{
							rexObjectGenericAnimation::TrackInfo* gnrcTrack = FindTrackInfo(bone->m_Tracks, ctrlTrack.GetInputName());

							if(gnrcTrack && (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_UNKNOWN || gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_DISTANCE))
							{
								atArray<float> af;
								af.Reserve(numFrames);
								for(int f=0; f<numFrames; f++)
								{
									float flt;
									gnrcTrack->GetFloatKey(startFrame+f, flt);
									af.Grow() = flt;
								}
								if(conversionFactor != 1.f && gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_DISTANCE)
								{
									for(int f=0; f<numFrames; f++)
									{
										af[f] *= conversionFactor;
									}
								}

								crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(boneId));
								animTrack->CreateFloat(af, compressionTolerance, framesPerChunk);

								exportAnim.CreateTrack(animTrack);
							}
							else if(gnrcTrack 
                                && ((gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_TRANSLATE)
                                || (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_DOF)
                                || (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_COLOUR)
								|| (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_LIGHT_DIRECTION)
                                || (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_DIRECTION)) )
							{
								atArray<Vec3V> av;
								av.Reserve(numFrames);
								for(int f=0; f<numFrames; f++)
								{
									Vec3V t;
									gnrcTrack->GetVector3Key(startFrame+f, RC_VECTOR3(t));
									av.Grow() = t;
								}
#if HACK_GTA4
								// only do this on translation as we expect this to go boom on other components
								crAnimTrack* animTrack = NULL;
								u16 meBoneID = u16(b);
								if(NULL!=strstr(gnrcTrack->m_Name, "shaderSlide"))
								{
									// UV animations have an abstract DOF, == no bone ID associated.
									meBoneID = 0;
								}
								else if(gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_TRANSLATE)
								{
									meBoneID = u16(boneIDs[b]);
								}
								animTrack = new crAnimTrack(trackIndex, meBoneID);
#else
								crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(b));
#endif // HACK_GTA4
								animTrack->CreateVector3(av, compressionTolerance, framesPerChunk);
								exportAnim.CreateTrack(animTrack);
							}
							else if(gnrcTrack && (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_ROTATE))
							{
								atArray<QuatV> av;
								av.Reserve(numFrames);
								for(int f=0; f<numFrames; f++)
								{
									QuatV t;
									gnrcTrack->GetQuaternionKey(startFrame+f, RC_QUATERNION(t));
									av.Grow() = t;
								}
#if HACK_GTA4
								crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(boneIDs[b]));
#else
								crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(b));
#endif // HACK_GTA4
								animTrack->CreateQuaternion(av, compressionTolerance, framesPerChunk);
								exportAnim.CreateTrack(animTrack);
							}
							else if(gnrcTrack && (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_SCALE))
							{
								atArray<Vec3V> av;
								av.Reserve(numFrames);
								for(int f=0; f<numFrames; f++)
								{
									Vec3V t;
									gnrcTrack->GetVector3Key(startFrame+f, RC_VECTOR3(t));
									av.Grow() = t;
								}
#if HACK_GTA4
								crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(boneIDs[b]));
#else
								crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(b));
#endif // HACK_GTA4
								animTrack->CreateVector3(av, compressionTolerance, framesPerChunk);
								exportAnim.CreateTrack(animTrack);
							}
							else if(gnrcTrack && ((gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_FOV)
								|| (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_IN)
								|| (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_OUT)
								|| (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_IN)
								|| (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_OUT)
								|| (gnrcTrack->m_TrackID == rexObjectGenericAnimation::TRACK_VISIBILITY)))
							{
								atArray<float> av;
								av.Reserve(numFrames);
								for(int f=0; f<numFrames; f++)
								{
									float t;
									gnrcTrack->GetFloatKey(startFrame+f, t);
									av.Grow() = t;
								}

								crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(b));
								animTrack->CreateFloat(av, compressionTolerance, framesPerChunk);
								exportAnim.CreateTrack(animTrack);
							}
						}
					}
				}
			}
			else //Export using the ChannelsToWrite channel map information
			{
				// search for other known tracks
 				for(int i=0; i<m_ChannelInputNames.GetCount(); i++)
				{
					u8 trackIndex;
					if(crAnimTrack::ConvertTrackNameToIndex(m_ChannelInputNames[i], trackIndex))
					{
						int boneRange = m_ChannelSupportedForRootBoneOnly[i] ? 1 : boneCount;
						for(int b=0; b<boneRange; b++)
						{
							rexObjectGenericAnimation::ChunkInfo* bone = segment.GetChunkByIndex(animation, b);

							rexObjectGenericAnimation::TrackInfo* track = FindTrackInfo(bone->m_Tracks, m_ChannelInputNames[i]);
							while(track)
							{
								if(track->m_TrackID == rexObjectGenericAnimation::TRACK_UNKNOWN || track->m_TrackID == rexObjectGenericAnimation::TRACK_DISTANCE)
								{
									atArray<float> af;
									af.Reserve(numFrames);
									for(int f=0; f<numFrames; f++)
									{
										float flt;
										track->GetFloatKey(startFrame+f, flt);
										af.Grow() = flt;
									}
									if(conversionFactor != 1.f && track->m_TrackID == rexObjectGenericAnimation::TRACK_DISTANCE)
									{
										for(int f=0; f<numFrames; f++)
										{
											af[f] *= conversionFactor;
										}
									}

									u16 trackID = u16(b);
									if(track->m_VarName != "")
									{
										trackID = (u16)atHash16U(track->m_VarName.c_str());
									}

									crAnimTrack* animTrack = new crAnimTrack(trackIndex, trackID);
									animTrack->CreateFloat(af, compressionTolerance, framesPerChunk);

									exportAnim.CreateTrack(animTrack);
								}
								else if((track->m_TrackID == rexObjectGenericAnimation::TRACK_TRANSLATE)
									|| (track->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_DOF)
									|| (track->m_TrackID == rexObjectGenericAnimation::TRACK_COLOUR)
									|| (track->m_TrackID == rexObjectGenericAnimation::TRACK_LIGHT_DIRECTION)
									|| (track->m_TrackID == rexObjectGenericAnimation::TRACK_DIRECTION) )
								{
									atArray<Vec3V> av;
									av.Reserve(numFrames);
									for(int f=0; f<numFrames; f++)
									{
										Vec3V t;
										track->GetVector3Key(startFrame+f, RC_VECTOR3(t));
										av.Grow() = t;
									}
#if HACK_GTA4
									// only do this on translation as we expect this to go boom on other components
									crAnimTrack* animTrack=NULL;
									u16 meBoneID = u16(b);
									if(NULL!=strstr(track->m_Name, "shaderSlide"))
									{
										// UV animations have an abstract DOF, == no bone ID associated.
										meBoneID = 0;
									}
									else if(track->m_TrackID == rexObjectGenericAnimation::TRACK_TRANSLATE)
									{
										meBoneID = u16(boneIDs[b]);
									}
									animTrack = new crAnimTrack(trackIndex, meBoneID);
#else
									crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(b));
#endif // HACK_GTA4
									animTrack->CreateVector3(av, compressionTolerance, framesPerChunk);
									exportAnim.CreateTrack(animTrack);
								}
								else if(track->m_TrackID == rexObjectGenericAnimation::TRACK_ROTATE)
								{
									atArray<QuatV> av;
									av.Reserve(numFrames);
									for(int f=0; f<numFrames; f++)
									{
										QuatV t;
										track->GetQuaternionKey(startFrame+f, RC_QUATERNION(t));
										av.Grow() = t;
									}
#if HACK_GTA4
									crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(boneIDs[b]));
#else
									crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(b));
#endif // HACK_GTA4
									animTrack->CreateQuaternion(av, compressionTolerance, framesPerChunk);
									exportAnim.CreateTrack(animTrack);
								}
								else if(track->m_TrackID == rexObjectGenericAnimation::TRACK_SCALE)
								{
									atArray<Vec3V> av;
									av.Reserve(numFrames);
									for(int f=0; f<numFrames; f++)
									{
										Vec3V t;
										track->GetVector3Key(startFrame+f, RC_VECTOR3(t));
										av.Grow() = t;
									}
#if HACK_GTA4
									crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(boneIDs[b]));
#else
									crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(b));
#endif // HACK_GTA4
									animTrack->CreateVector3(av, compressionTolerance, framesPerChunk);
									exportAnim.CreateTrack(animTrack);
								}
								else if((track->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_FOV)
									|| (track->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_IN)
									|| (track->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_DOF_FAR_OUT)
									|| (track->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_IN)
									|| (track->m_TrackID == rexObjectGenericAnimation::TRACK_CAMERA_DOF_NEAR_OUT)
									|| (track->m_TrackID == rexObjectGenericAnimation::TRACK_VISIBILITY))
								{
									atArray<float> av;
									av.Reserve(numFrames);
									for(int f=0; f<numFrames; f++)
									{
										float t;
										track->GetFloatKey(startFrame+f, t);
										av.Grow() = t;
									}

									crAnimTrack* animTrack = new crAnimTrack(trackIndex, u16(boneIDs[b]));
									animTrack->CreateFloat(av, compressionTolerance, framesPerChunk);
									exportAnim.CreateTrack(animTrack);
								}
								else if(track->m_TrackID == rexObjectGenericAnimation::TRACK_PARTICLE)
								{
									atArray<float> af;
									af.Reserve(numFrames);
									for(int f=0; f<numFrames; f++)
									{
										float t;
										track->GetFloatKey(startFrame+f, t);
										af.Grow() = t;
									}

									crAnimTrack* animTrack = new crAnimTrack(trackIndex, (u16)atHash16U(track->m_VarName.c_str()));
									animTrack->CreateFloat(af, compressionTolerance, framesPerChunk);
									exportAnim.CreateTrack(animTrack);

									track = FindNextTrackInfo(bone->m_Tracks, m_ChannelInputNames[i], track);
									continue;
								}

								break;
							}
						}
					}
				}
			}

			projectFlags |= exportAnim.GetProjectFlags();
			exportAnim.SetProjectFlags(projectFlags);

			if(maxBlockSize)
			{
				exportAnim.Pack(false, false);
			}

			if(!maxBlockSize || exportAnim.GetMaxBlockSize() <= maxBlockSize)
			{
				// save the animation
				if(!rexShell::DoesntSerialise())
				{
					if(!exportAnim.Save(segment.GetName()))
					{
						result.Set(rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE);

						char cFullPath[RAGE_MAX_PATH];
						ASSET.FullWritePath( cFullPath, sizeof(cFullPath), (const char*)segment.GetName(), "anim" );
						
						long dwAttributes = GetFileAttributes(cFullPath);
						if(dwAttributes & FILE_ATTRIBUTE_READONLY)
						{
							result.AddMessage("Failed to create file '%s' - File is read-only", cFullPath);
						}
						else
						{
							result.AddMessage("Failed to create file '%s'", cFullPath);
						}
					}
				}

				// quit the block size search
				break;
			}
			else
			{
				// calculate the current number of blocks, then pick a new frames per chunk value that will add one more block
				u16 newFramesPerChunk = u16((numFrames-2)/++numBlocks);

				const u16 minFramesPerChunk = 16;
				if(newFramesPerChunk > minFramesPerChunk)
				{
					framesPerChunk = ((newFramesPerChunk+16)&(~15))-1;
				}
				else
				{
					result.Set(rexResult::ERRORS);
					result.AddError("Max block size driven frames per chunk search failed '%s.anim'", (const char*)segment.GetName());
					break;
				}
			}
		}
		while(1);

		return result;
	}

	/////////////////////////////////////////////////////////////////////////////////////

	void rexSerializerRAGEAnimation::ExportTransformTracks(const atArray<Matrix34>& am, bool isMover, u16 identifier, bool genericTranslation, bool translateTrack, bool genericRotation, bool rotateTrack, bool scaleTrack, const crAnimTolerance& compressionTolerance, u16 framesPerChunk, crAnimation& inoutAnim) const
	{
		int numFrames = am.GetCount();

		// export translate track
		if(translateTrack || genericTranslation)
		{
			atArray<Vec3V> av;
			av.Reserve(numFrames);
			for(int f=0; f<numFrames; f++)
			{
				av.Grow() = RCC_VEC3V(am[f].d);
			}

			u8 trackID = kTrackBoneTranslation;
			if(isMover) trackID = kTrackMoverTranslation;
			if(genericTranslation) trackID = kTrackGenericTranslation;

			crAnimTrack* translateTrack = new crAnimTrack(trackID, identifier);
			translateTrack->CreateVector3(av, compressionTolerance, framesPerChunk);

			inoutAnim.CreateTrack(translateTrack);
		}

		// export rotate track
		if(rotateTrack || genericRotation)
		{
			atArray<QuatV> aq;
			aq.Reserve(numFrames);
			for(int f=0; f<numFrames; f++)
			{
				Matrix34 frameMatrix = am[f];
				for(int i=0;i<3;i++)
					if(frameMatrix.GetVector(i).Mag2()==0)
						frameMatrix.Scale(0,0,0);

				aq.Grow() = QuatVFromMat33VSafe(RCC_MAT34V(frameMatrix).GetMat33(), QuatV(V_IDENTITY));
			}

			u8 trackID = kTrackBoneRotation;
			if(isMover) trackID = kTrackMoverRotation;
			if(genericTranslation) trackID = kTrackGenericRotation;

			crAnimTrack* rotateTrack = new crAnimTrack(trackID, identifier);
			rotateTrack->CreateQuaternion(aq, compressionTolerance, framesPerChunk);

			inoutAnim.CreateTrack(rotateTrack);
		}

		// export scale track
		if(scaleTrack)
		{
			atArray<Vec3V> av;
			av.Reserve(numFrames);
			for(int f=0; f<numFrames; f++)
			{
				av.Grow() = ScaleFromMat33VTranspose(RCC_MAT34V(am[f]).GetMat33());
			}

			crAnimTrack* scaleTrack = new crAnimTrack(u8(kTrackBoneScale), identifier);
			scaleTrack->CreateVector3(av, compressionTolerance, framesPerChunk);

			inoutAnim.CreateTrack(scaleTrack);
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult  rexPropertyAnimationSkipLockedChannel::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( animSer )
		{
			animSer->SetSkipLockedChannel( value.toBool( animSer->GetSkipLockedChannel() ) );
			return rexResult( rexResult::PERFECT );
		}

		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult  rexPropertyAnimationSerializeAnimCurves::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( !animSer )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		animSer->SetSerializeAnimCurves( value.toBool( animSer->GetSerializeAnimCurves() ) );
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult  rexPropertyAnimationProcessNonJointTransformAndRotates::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( !animSer )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		animSer->SetProcessNonJointTransformAndRotates( value.toBool( animSer->GetProcessNonJointTransformAndRotates() ) );
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult  rexPropertyAnimationSetExportAllUntaggedChunksSeperately::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( !animSer )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		animSer->SetExportAllUntaggedChunksSeperately( value.toBool( animSer->GetExportAllUntaggedChunksSeperately() ) );
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult  rexPropertyAnimationExportMoverChannels::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( !animSer )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		animSer->SetExportMoverChannels( value.toBool( animSer->GetExportMoverChannels() ) );
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////
	rexResult rexPropertyAnimationGimbelLockFix::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( !animSer )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );


		animSer->SetGimbelLockFix( value.toBool( animSer->GetGimbelLockFix() ) );
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////
	rexResult rexPropertyAnimationCompressionErrorTolerance::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( !animSer )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );


		animSer->SetCompressionErrorTolerance( value.toFloat( animSer->GetCompressionErrorTolerance() ) );
		return rexResult( rexResult::PERFECT );
	}
	/////////////////////////////////////////////////////////////////////////////////////
	rexResult rexPropertyAnimationNormalizeRoot::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( !animSer )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );


		animSer->SetNormalizeRoot( value.toBool( animSer->GetNormalizeRoot() ) );
		return rexResult( rexResult::PERFECT );
	}
	/////////////////////////////////////////////////////////////////////////////////////
	rexResult rexPropertyAnimationCompressionCost::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( !animSer )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		animSer->SetCompressionCost( value.toInt( animSer->GetCompressionCost() ) );
		return rexResult( rexResult::PERFECT );
	}
	/////////////////////////////////////////////////////////////////////////////////////
	rexResult rexPropertyAnimationDecompressionCost::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( !animSer )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		animSer->SetDecompressionCost( value.toInt( animSer->GetDecompressionCost() ) );
		return rexResult( rexResult::PERFECT );
	}
	/////////////////////////////////////////////////////////////////////////////////////
	rexResult rexPropertyAnimationMaximumBlockSize::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
		if( !animSer )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		animSer->SetMaximumBlockSize( value.toInt( animSer->GetMaximumBlockSize() ) );
		return rexResult( rexResult::PERFECT );
	}
	/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage
