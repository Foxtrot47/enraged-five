// 
// rexRAGE/serializerBound.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSerializerRAGEPhysicsBound
//			 -- generates .bnd files from generic mesh hierarchy information
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexSerializer
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_RMCORE_SERIALIZER_GENERICBOUNDHIERARCHYTOPHBOUND_H__
#define __REX_RMCORE_SERIALIZER_GENERICBOUNDHIERARCHYTOPHBOUND_H__

#include "phbound/boundgrid.h"
#include "phcore/materialmgr.h"
#include "rexBase/property.h"
#include "rexBase/serializer.h"
#include "rexGeneric/objectMesh.h"
#include "phbound/bounddisc.h"

/////////////////////////////////////////////////////////////////////////////////////
namespace rage {

class phMaterial;
class phBound;
class phBoundBox;
class phBoundSphere;
class phBoundCapsule;
class phBoundComposite;
class phBoundGeometry;
class phBoundCylinder;
USE_GEOMETRY_CURVED_ONLY(class phBoundCurvedGeometry;)
class phBoundOctree;
class phBoundBVH;
class rexObjectGenericBound;

extern atString sBoundCheckIDs[];

/////////////////////////////////////////////////////////////////////////////////////

class rexSerializerRAGEPhysicsBound : public rexSerializer
{
public:
	rexSerializerRAGEPhysicsBound() : rexSerializer() 
	{
		m_ExportGeometryBoundsAsBvhGrid = false;
		m_ExportGeometryBoundsAsBvhGeom = false;
		m_GridCellSize = 300.0f/GetUnitTypeLinearConversionFactor(); 
		m_UseChildrenOfLevelInstanceNodes = false; 
		m_ZeroPrimitiveCentroids = true; 
		m_CheckBoundVertexCount = false;
		m_QuadrifyGeometryBounds = false; 
		m_ApplyBulletShrinkMargin = false;
		m_ExportPrimsInBvh = false;
		m_SplitOnMaterialLimit = false;
		m_HasSecondSurface = false;
		m_SecondSurfaceMaxHeight = 0.0f;
		m_ForceCompositeBounds = false;
		m_OptimiseGeometry = true;
		m_ExportPerVertAttributes = true;

	}

	class FilteredBound {
		public:
			FilteredBound::~FilteredBound()
			{
				delete m_Bound;
				m_Bound = NULL;
			}

			phBound* m_Bound;
			Matrix34 m_Transform;
	};

	virtual rexResult	Serialize( rexObject& object ) const;

	rexResult			SerializeRecursive( rexObject &object ) const;

	//UseBoundFiltering
	rexResult 			   BuildFilteredBoundsRecursive(rexObject& object, atMap< atString, atArray< FilteredBound* > >& outBounds) const;
	rexResult 			   SerializeFilteredBounds(atMap< atString, atArray< FilteredBound* > >& boundMap) const;
	rexResult 			   SerializeBound(const atString& name, phBound* bound) const;
    rexResult 			   GenericBoundToFilteredphBounds(const rexObjectGenericBound *inputBound, atMap< atString, atArray< FilteredBound* > >& outBounds, bool ignoreChildren = false) const;
	rexResult 			   GenericBoundToFilteredPhBoundGeometry(atMap< atString, atArray< FilteredBound* > >& outBounds, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
	rexResult			   GenericBoundToFilteredPhBoundBox(atMap< atString, atArray< FilteredBound* > >& outBounds, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
	rexResult			   GenericBoundToFilteredPhBoundCapsule(atMap< atString, atArray< FilteredBound* > >& outBounds, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
	rexResult			   GenericBoundToFilteredPhBoundSphere(atMap< atString, atArray< FilteredBound* > >& outBounds, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
	rexResult 			   GetBoundFilterType(atArray< atString >& filters, const atString& materialName) const;
	const phBoundGeometry* GetFilteredPhBoundGeometry(atMap< atString, atArray< FilteredBound* > >& outBounds, const atString& filter) const;
	rexResult 			   FillFilteredPhBoundGeometry(phBoundGeometry*& appendedBound, const phBoundGeometry* storedBound, const int curMaterialsIndex, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap) const;
	u32		  			   ConvertToCorrectVertexIndex(u32 indexToConvert, const atArray<int>& vertMapping) const;
	void				   InsertReplaceFilteredPhBoundGeometry(FilteredBound* appendedBound, atMap< atString, atArray< FilteredBound* > >& outBounds, const atString& filter) const;
	void				   InsertFilteredPhBound(FilteredBound* bound, atMap< atString, atArray< FilteredBound* > >& outBounds, const atString& filter) const;
	void				   SetupCompositeTypeAndIncludeFlags(phBoundComposite *composite, const rexObjectGenericBound *inputBound, int partIndex) const;

	void				   SplitBound( const rexObjectGenericBound *inputBound, atArray<rexObjectGenericBound*>& outputBounds ) const;

	rexResult	CreatePhysicsMaterials( atArray<phMaterialMgr::Id> &materials, atArray<int> &materialMap, const rexObjectGenericBound *inputBound ) const;
	rexResult	CreatePhysicsMaterials( atArray< phMaterialMgr::Id >* phmaterials, const atArray< rexObjectGenericMesh::MaterialInfo >& materials ) const;
	rexResult	NewPhysicsMaterial( phMaterialMgr::Id &mat, const rexObjectGenericMesh::MaterialInfo &matInfo ) const;
	rexResult	GenericBoundToPhBound( phBound *&bnd, const rexObjectGenericBound *inputBound, bool ignoreChildren = false, bool firstCall = false, bool hasParent = false ) const;
	rexResult	GenericBoundToGeometry( phBound *&bnd, const rexObjectGenericBound *inputBound, atArray<phMaterialMgr::Id>& materials, atArray<int>& materialMap ) const;
	rexResult	GenericBoundToPhBoundBox( phBoundBox *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
	rexResult	GenericBoundToPhBoundSphere( phBoundSphere *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
	rexResult	GenericBoundToPhBoundCapsule( phBoundCapsule *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
	rexResult	GenericBoundToPhBoundGeometry( phBoundGeometry *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
	rexResult	GenericBoundToPhBoundGeometryWithPrims( phBoundGeometry *&bnd, const rexObjectGenericBound *inputBound ) const;
#if USE_GEOMETRY_CURVED
	rexResult	GenericBoundToPhBoundCurvedGeometry( phBoundCurvedGeometry *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
#endif
#if USE_GRIDS
	rexResult	GenericBoundToPhBoundGrid( phBoundGrid *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap, bool bIsBvhGrid ) const;
#endif
	rexResult	GenericBoundToPhBoundComposite( phBoundComposite *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
	rexResult	GenericBoundToPhBoundCylinder( phBoundCylinder *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap )  const;
	rexResult	GenericBoundToPhBoundBvh( phBoundBVH *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;	
	rexResult	GenericBoundToPhBoundPrimitiveBvh( phBoundBVH *& bnd, const rexObjectGenericBound *inputBound ) const;
	rexResult	GenericBoundToPhBoundDisc( phBoundDisc *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const;
	
	virtual rexSerializer* CreateNew() const  { rexSerializerRAGEPhysicsBound* s = new rexSerializerRAGEPhysicsBound(); 
												s->m_BoundFilterTypes = m_BoundFilterTypes; 
												s->m_UseBoundFiltering = m_UseBoundFiltering; 
												return s; }

	rexResult ValidatePhBound( phBound * pBnd, const char *name) const;

	void	SetGeometryBoundsExportAsBvhGrid( bool b )			{ m_ExportGeometryBoundsAsBvhGrid = b; }
	bool	GetGeometryBoundsExportAsBvhGrid() const			{ return m_ExportGeometryBoundsAsBvhGrid; }

	void	SetGeometryBoundsExportAsBvhGeom( bool b )			{ m_ExportGeometryBoundsAsBvhGeom = b; }
	bool	GetGeometryBoundsExportAsBvhGeom() const			{ return m_ExportGeometryBoundsAsBvhGeom; }

	void	SetGridCellSize( float f )							{ m_GridCellSize = f; }
	float	GetGridCellSize() const								{ return m_GridCellSize; }	

	void	SetUseChildrenOfLevelInstanceNodes( bool b )		{ m_UseChildrenOfLevelInstanceNodes = b; }
	bool	GetUseChildrenOfLevelInstanceNodes() const			{ return m_UseChildrenOfLevelInstanceNodes; }

	void	SetZeroPrimitiveCentroids( bool b )					{ m_ZeroPrimitiveCentroids = b; }
	bool	GetZeroPrimitiveCentroids()	 const					{ return m_ZeroPrimitiveCentroids;	}

	void	SetCheckVertexBoundCounts( bool b)					{ m_CheckBoundVertexCount = b; }
	bool	GetCheckVertexBoundCounts() const					{ return m_CheckBoundVertexCount; } 

	void	SetQuadrifyGeometryBounds( bool b )					{ m_QuadrifyGeometryBounds = b; }
	bool	GetQuadrifyGeometryBounds() const					{ return m_QuadrifyGeometryBounds; }

	void	SetApplyBulletShrinkMargin( bool b )				{ m_ApplyBulletShrinkMargin = b; }
	bool	GetApplyBulletShrinkMargin() const					{ return m_ApplyBulletShrinkMargin; }

	void	SetPrimitiveBoundExportPrimitivesInBvh( bool b )	{ m_ExportPrimsInBvh = b; }
	bool	GetPrimitiveBoundExportPrimitivesInBvh() const		{ return m_ExportPrimsInBvh; }

	void	SetSplitOnMaterialLimit( bool b )					{ m_SplitOnMaterialLimit = b; }
	bool	GetSplitOnMaterialLimit() const						{ return m_SplitOnMaterialLimit; }

	void	SetHasSecondSurface(bool b )						{ m_HasSecondSurface = b; }
	bool	GetHasSecondSurface() const							{ return m_HasSecondSurface; }

	void	SetSecondSurfaceMaxHeight(float f )					{ m_SecondSurfaceMaxHeight = f; }
	float	GetSecondSurfaceMaxHeight() const					{ return m_SecondSurfaceMaxHeight; }

	void	SetForceCompositeBound(bool b )					{ m_ForceCompositeBounds = b; }
	bool	GetForceCompositeBound() const					{ return m_ForceCompositeBounds; }

	void	SetOptimiseGeometry(bool b )					{ m_OptimiseGeometry = b; }
	bool	GetOptimiseGeometry() const						{ return m_OptimiseGeometry; }

	void	SetExportPerVertAttributes( bool b )				{ m_ExportPerVertAttributes = b; }
	bool	GetExportPerVertAttributes() const					{ return m_ExportPerVertAttributes; }

	void	SetBoundCheckFlags(atString boundCheckString);
protected:
	bool	m_ExportGeometryBoundsAsBvhGrid;
	bool	m_ExportGeometryBoundsAsBvhGeom;

	float	m_GridCellSize;
	bool    m_ZeroPrimitiveCentroids;
	bool	m_UseChildrenOfLevelInstanceNodes;
	bool	m_CheckBoundVertexCount;
	bool	m_QuadrifyGeometryBounds;
	bool	m_ApplyBulletShrinkMargin;
	float	m_PrimitiveBoundCompositeMaxDistance;
	float	m_PrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio;
	bool	m_ExportPrimsInBvh;
	bool	m_SplitOnMaterialLimit;
	bool	m_HasSecondSurface;
	bool	m_ForceCompositeBounds;
	float	m_SecondSurfaceMaxHeight;
	atString m_ColourTemplateFile;
	bool	m_OptimiseGeometry;
	bool	m_ExportPerVertAttributes;
	atMap<atString, ScalarV>	m_BoundCheckTolerances;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundSetGeometryBoundsExportAsBvhGrid : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundSetGeometryBoundsExportAsBvhGrid; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundSetGeometryBoundsExportAsBvhGeom : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundSetGeometryBoundsExportAsBvhGeom; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundSetGridCellSize : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundSetGridCellSize; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyZeroPrimitiveCentroids : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyZeroPrimitiveCentroids; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyCheckBoundVertexCounts : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyCheckBoundVertexCounts; }
};	

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyQuadrifyGeometryBounds : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyQuadrifyGeometryBounds; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyApplyBulletShrinkMargin : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyApplyBulletShrinkMargin; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertySplitOnMaterialLimit : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySplitOnMaterialLimit; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundCheckFlags : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundCheckFlags; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundExportPerVertAttributes : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundExportPerVertAttributes; }
};

} // namespace rage

#endif // #ifndef __REX_RMCORE_SERIALIZER_GENERICBOUNDHIERARCHYTOPHBOUND_H__
