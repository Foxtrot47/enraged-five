// 
// rexRAGE/serializerEntity.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSerializerRAGEEntity
//			 -- generates rmcore .type files from generic entity information
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexSerializerGenericContainer
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_RMCORE_SERIALIZER_GENERICENTITYTORMCOREENTITY_H__
#define __REX_RMCORE_SERIALIZER_GENERICENTITYTORMCOREENTITY_H__

#include "rexBase/property.h"
#include "rexGeneric/objectFragment.h"
#include "rexGeneric/serializerContainer.h"
#include "atl/map.h"

namespace rage {

class fiStream;
class rexObjectGenericBound;
class rexObjectGenericFragmentHierarchy;
class rexObjectGenericMesh;
class rexObjectGenericBoundHierarchy;

/////////////////////////////////////////////////////////////////////////////////////

class rexSerializerRAGEEntity : public rexSerializerGenericContainer
{
public:
	rexSerializerRAGEEntity();

	virtual rexResult Serialize( rexObject& object ) const;

	virtual rexSerializer* CreateNew() const  { rexSerializerRAGEEntity* s = new rexSerializerRAGEEntity(); 
												s->m_BoundFilterTypes = m_BoundFilterTypes; 
												s->m_UseBoundFiltering = m_UseBoundFiltering; 
												return s; }

	void AddTextureSet( const atArray< atString >& textureNames );

	void SetUseChildrenOfLevelInstanceNodes( bool b )						{ m_UseChildrenOfLevelInstanceNodes = b; }
	bool GetUseChildrenOfLevelInstanceNodes() const							{ return m_UseChildrenOfLevelInstanceNodes; }

	void SetComposeBounds( bool b )											{ m_ComposeBounds = b; }
	bool GetComposeBounds() const											{ return m_ComposeBounds; }

	void SetAllowEmptyFragSection( bool b )									{ m_AllowEmptyFragSection = b; }
	bool GetAllowEmptyFragSection() const									{ return m_AllowEmptyFragSection; }

	void SetFragmentHasDamage( bool b )										{ m_FragmentHasDamage = b; }
	bool GetFragmentHasDamage() const										{ return m_FragmentHasDamage; }

	void SetFragmentWriteMeshes( bool b )									{ m_FragmentWriteMeshes = b; }
	bool GetFragmentWriteMeshes() const										{ return m_FragmentWriteMeshes; }

	void SetWriteBoundSituations( bool b )									{ m_WriteBoundSituations = b; }
	bool GetWriteBoundSituations() const									{ return m_WriteBoundSituations; }

	void SetWriteBoundFlags( bool b )										{ m_WriteBoundFlags = b; }
	bool GetWriteBoundFlags() const											{ return m_WriteBoundFlags; }

	void SetGroupBoundsByBone(bool b)										{ m_GroupBoundsByBone = b; }
	bool GetGroupBoundsByBone()												{ return m_GroupBoundsByBone;}

	void SetWriteLocatorsAsSeparateFiles( bool b )							{ m_WriteLocatorsAsSeparateFiles = b; }
	bool GetWriteLocatorsAsSeparateFiles() const							{ return m_WriteLocatorsAsSeparateFiles; }

	void SetWriteCurvesAsSeparateFiles( bool b)								{ m_WriteCurvesAsSeparateFiles = b; }
	bool GetWriteCurvesAsSeparateFiles() const								{ return m_WriteCurvesAsSeparateFiles; }

	void SetWriteMeshGroupIndex( bool b)									{ m_WriteMeshGroupIndex = b; }
	bool GetWriteMeshGroupIndex() const										{ return m_WriteMeshGroupIndex; }

	void SetWriteShadingGroup( bool b )										{ m_WriteShadingGroup = b; }
	bool GetWriteShadingGroup() const										{ return m_WriteShadingGroup; }

	void SetWriteSeparateShadingGroupFile( bool b )							{ m_WriteSeparateShadingGroupFile = b; }
	bool GetWriteSeparateShadingGroupFile() const							{ return m_WriteSeparateShadingGroupFile; }

	void SetSplitLODGroupsByFlags( bool b )									{ m_SplitLODGroupsByFlags = b; }
	bool GetSplitLODGroupsByFlags() const									{ return m_SplitLODGroupsByFlags; }

	void SetSplitFlagGroupsToDrawables( bool b )							{ m_SplitFlagGroupsToDrawables = b; }
	bool GetSplitFlagGroupsToDrawables() const								{ return m_SplitFlagGroupsToDrawables; }

	void			SetBoundGeometryExportType(const atString& str)			{ m_BoundGeometryExportType = str; }
	const atString& GetBoundGeometryExportType() const						{ return m_BoundGeometryExportType; }

	void SetUseNonOptimizedMTLCompare(bool b)								{ m_UseNonOptimizedMTLCompare = b; }
	bool GetUseNonOptimizedMTLCompare() const								{ return m_UseNonOptimizedMTLCompare; }

	void SetUseLODMeshTypes(bool b)											{ m_UseLODMeshTypes = b; }
	bool GetUseLODMeshTypes() const											{ return m_UseLODMeshTypes; }

	void SetSerializeVersionTwoNurbsCurves(bool b)							{ m_SerializeVersionTwoNurbsCurves = b; }
	bool GetSerializeVersionTwoNurbsCurves() const							{ return m_SerializeVersionTwoNurbsCurves; }

	void AddBoundSpecialFlagMapping(const atString& materialFlag, const atString& specialFlag);
	void AddLODMeshSpecialFlagMapping(const atString& materialFlag, const atString& specialFlag);

	void SetErrorOnClothWithoutVertexTuning(bool b)							{ m_ErrorOnClothWithoutVertexTuning = b; }
	bool GetErrorOnClothWithoutVertexTuning() const							{ return m_ErrorOnClothWithoutVertexTuning; }

	void SetBoundListName(atString name)									{ m_BoundListName = name; }
	atString GetBoundListName() const										{ return m_BoundListName; }
#if HACK_GTA4
	void SetFragmentForceLoadCommonDrawable( bool b )						{ m_FragmentForceLoadCommonDrawable = b; }
	bool GetFragmentForceLoadCommonDrawable() const							{ return m_FragmentForceLoadCommonDrawable; }

	void SetOptimiseMaterials(bool b)										{ m_OptimiseMaterials = b; }
	bool GetOptimiseMaterials() const										{ return m_OptimiseMaterials; }
#endif // HACK_GTA4

protected:
	rexResult SerializeRecursive( rexObject& object, fiStream* s ) const;

	// Object type handlers
	rexResult WriteLODAndShaderGroupInfo( rexObject& object, fiStream* s );
	void PrioritizeMeshesInLODGroups();

	//[CLEMENSP]
    rexResult WriteShaderGroupInfo( rexObject& object, fiStream* s);
	rexResult WriteSkeletonAndBoneTagInfo( rexObject& object, fiStream* s );
	rexResult WriteAnimationInfo( rexObject& object, fiStream* s );
	rexResult WriteLocatorInfo( rexObject& object, fiStream* s );
	rexResult WriteEdgeModelInfo( rexObject& object, fiStream* s );
	rexResult WriteBoundInfo( rexObject& object, fiStream* s );
	rexResult WriteLightInfo( rexObject& object, fiStream* s );
	rexResult WriteFragmentInfo( rexObject& object, fiStream* s );
	rexResult WriteCurveInfo( rexObject& object, fiStream* s );
	rexResult WriteClothGroupInfo( rexObject& object, fiStream* s);
	rexResult WriteHairGroupInfo( rexObject& object, fiStream* s);
	rexResult WritePrimitiveInfo( rexObject& object, fiStream* s);

	
	void	  WriteWorldMeshData(rexObjectGenericMesh *mesh, const char* levelType, fiStream* fs);
	
	rexResult WriteRoomVolume(rexObject& object, fiStream* fs);
	rexResult WriteLevelRoomVolumes( rexObject& object, fiStream* s);

	rexResult WritePortal(rexObject& object, fiStream* fs);
	rexResult WriteLevelPortals( rexObject& object, fiStream* s);
	
	void WriteFilteredBoundList( const atMap<atString, atString>& boundFilterList, fiStream* s, int depth );
	void WriteBoundList( atArray<rexObjectGenericBound*>& boundList, fiStream* s, int depth, atArray<rexObjectGenericMesh*>* parentList=NULL, const char *boundListName=m_BoundListName.c_str());
	void WriteGroupAndChildHeader( const char* name, fiStream* s, bool isDamageState, int depth);
	void WriteFragmentHierarchy(rexObjectGenericFragmentHierarchy& fragments, fiStream* s, int depth = 0);

	void GetFilteredBounds( rexObject& object, const atStringArray& boundFilterTypes, const atMap<atString, atString>& specialFlagMapping, atMap<atString, atString>& boundFilterList );
#if HACK_GTA4
	bool				m_OptimiseMaterials;
#endif // HACK_GTA4
	bool				m_UseChildrenOfLevelInstanceNodes;
	bool				m_ComposeBounds;
	bool				m_AllowEmptyFragSection;
	static atString		m_BoundListName;
	bool				m_GroupBoundsByBone;
	bool				m_FragmentHasDamage;
	bool				m_FragmentWriteMeshes;
	bool				m_WriteBoundSituations;
	bool				m_WriteBoundFlags;
	bool				m_WriteLocatorsAsSeparateFiles;
	bool				m_WriteCurvesAsSeparateFiles;
	bool				m_WriteMeshGroupIndex;
	bool				m_WriteShadingGroup;
	bool				m_WriteSeparateShadingGroupFile;
	bool				m_SplitLODGroupsByFlags;
	bool				m_SplitFlagGroupsToDrawables;
	bool				m_UseNonOptimizedMTLCompare;
	bool				m_UseLODMeshTypes;
	bool				m_SerializeVersionTwoNurbsCurves;
	bool				m_ErrorOnClothWithoutVertexTuning;

	atMap<atString, atString> m_BoundSpecialFlagMappings;
	atString m_BoundGeometryExportType;
#if HACK_GTA4
	bool				m_FragmentForceLoadCommonDrawable;
#endif // HACK_GTA4

	atMap<atString, atString> m_LODMeshFilterMappings;

	mutable bool		m_SkipBoundSectionOfTypeFile;
	
	atArray< atArray< atString > >				m_TextureSets;

	atArray<rexObjectGenericBound*>				m_BoundList;
	rexObjectGenericFragmentHierarchy			m_Fragments;
	int											m_CompositeFileCount;
	atArray<rexObjectGenericMesh*>				m_EdgeModelList;
//	atArray< atArray<rexObjectGenericMesh*> >	m_MeshesByLODLevel;
	atArray< atArray< atArray< rexObjectGenericMesh*> > >	m_MeshesByLODGroupAndLevel;
	atArray< atArray< float > >					m_Thresholds;
	atArray< atArray< atArray< rexObjectGenericMesh::sThresholdPair > > >		m_ThresholdsByLODGroupAndLevel;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityAllowEmptyFragSection : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityAllowEmptyFragSection; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityAddTextureSet : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityAddTextureSet; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityComposeBounds : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityComposeBounds; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityFragmentHasDamage : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityFragmentHasDamage; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityFragmentWriteMeshes : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityFragmentWriteMeshes; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityGroupBoundsByBone : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityGroupBoundsByBone; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityWriteBoundSituations : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityWriteBoundSituations; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityBoundListName : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityBoundListName; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityWriteBoundFlags : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityWriteBoundFlags; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityWriteLocatorsAsSeparateFiles : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityWriteLocatorsAsSeparateFiles; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityWriteCurvesAsSeparateFiles : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityWriteCurvesAsSeparateFiles; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityWriteMeshGroupIndex : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityWriteMeshGroupIndex; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityWriteShadingGroup : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityWriteShadingGroup; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntityWriteSeparateShadingGroupFile : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityWriteSeparateShadingGroupFile; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntitySplitLODGroupsByFlags : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntitySplitLODGroupsByFlags; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEntitySplitFlagGroupsToDrawables : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntitySplitFlagGroupsToDrawables ; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundSpecialFlagMappings : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundSpecialFlagMappings; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundGeometryExportType : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundGeometryExportType; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyUseNonOptimizedMTLCompare : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyUseNonOptimizedMTLCompare; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyLODMeshFilterMappings : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyLODMeshFilterMappings; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyUseLODMeshTypes : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyUseLODMeshTypes; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertySerializeVersionTwoNurbsCurves: public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySerializeVersionTwoNurbsCurves; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyErrorOnClothWithoutVertexTuning : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyErrorOnClothWithoutVertexTuning; }
};

#if HACK_GTA4
/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyFragmentForceLoadCommonDrawable : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyFragmentForceLoadCommonDrawable; }
};
#endif // HACK_GTA4
/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
