#include "rexBase/module.h"
#include "rexBase/utility.h"
#include "rexBase/shell.h"
#include "rexGeneric/objectBound.h"
#include "rexRAGE/serializerBound.h"

#include "atl/map.h"
#include "system/alloca.h"

#include "file/stream.h"
#include "file/asset.h"
#include "phcore/material.h"
#include "phcore/materialmgrimpl.h"
#include "phbound/bound.h"
#include "phbound/boundbox.h"
#include "phbound/boundbvh.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundcurvedgeom.h"
#include "phbound/boundcylinder.h"
#include "phbound/boundgeom.h"
#include "phbound/boundsphere.h"
#include "file/token.h"

#include "grmodel/grmodel_config.h"
#if USE_MTL_SYSTEM
#include "rageShaderMaterial/rageShaderMaterial.h"
#include "rageShaderMaterial/rageShaderMaterialParamString.h"
//#include "shaderMaterial/shaderMaterialGeoParamValueBool.h"
#include "shaderMaterial/shaderMaterialGeoParamValueFloat.h"
//#include "shaderMaterial/shaderMaterialGeoParamValueInt.h"
#include "shaderMaterial/shaderMaterialGeoParamValueMatrix34.h"
#include "shaderMaterial/shaderMaterialGeoParamValueMatrix44.h"
#include "shaderMaterial/shaderMaterialGeoParamValueTexture.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector2.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector3.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector4.h"
#include "shaderMaterial/shaderMaterialGeoParamValueString.h"
#endif

#include <string.h>


namespace rage {

	atString sBoundCheckIDs[] =
	{
		atString("BOUNDCHECK_ERROR"),
		atString("BOUNDCHECK_ISTHIN"),
		atString("BOUNDCHECK_BADNORMAL"),
		atString("BOUNDCHECK_BADNEIGHBOR")
	};


///////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////

rexResult GetPhysicsMaterialName( const rexObjectGenericMesh::MaterialInfo &matInfo, atString& retName );

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::Serialize( rexObject& object ) const
{
	rexResult retval = rexResult( rexResult::PERFECT );
	if( !GetExportData() )
		return retval;

	phMaterialMgrImpl<phMaterial>::Create();
	// WARNING : The number on the next line is only here as a hack!!!
	// It is to stop the manager's array from filling and having to be resized.  This is a problem because later on 
	// other arrays point to things in this array (see rexSerializerRAGEPhysicsBound::CreatePhysicsMaterials for more information)
	// It really needs to be fixed properly, this should only be a temporary hack so Vienna can get on
	MATERIALMGR.Load(512);
	const rexObjectGenericBoundHierarchy *inputBoundHier = dynamic_cast<const rexObjectGenericBoundHierarchy*>( &object );
	if( inputBoundHier )
	{
		rexUtility::CreatePath( GetOutputPath() );
		ASSET.SetPath( GetOutputPath() );
		
		if (GetUseBoundFiltering())
		{
			int objCount = inputBoundHier->m_ContainedObjects.GetCount();
			/*
			//do the default stuff first
			for( int a = 0; a < objCount; a++ )
			{
				if( m_ProgressBarObjectCountCB )
					(*m_ProgressBarObjectCountCB )( objCount );	

				if( m_ProgressBarObjectCurrentIndexCB )
					(*m_ProgressBarObjectCurrentIndexCB)( a );	

				retval &= SerializeRecursive( *inputBoundHier->m_ContainedObjects[ a ] );
			}
			*/

			phBound *bnd = NULL;
			for( int a = 0; a < objCount; a++ )
			{
				if( m_ProgressBarObjectCountCB )
					(*m_ProgressBarObjectCountCB )( objCount );	

				if( m_ProgressBarObjectCurrentIndexCB )
					(*m_ProgressBarObjectCurrentIndexCB)( a );	

				retval = GenericBoundToPhBound(bnd, dynamic_cast<rexObjectGenericBound*>(inputBoundHier->m_ContainedObjects[ a ]), false, true);
			}

			atMap< atString, atArray< FilteredBound* > > builtBounds;

			//do the filtered stuff
			objCount = inputBoundHier->m_ContainedObjects.GetCount();
			for( int a = 0; a < objCount; a++ )
			{
				if( m_ProgressBarObjectCountCB )
					(*m_ProgressBarObjectCountCB )( objCount );	

				if( m_ProgressBarObjectCurrentIndexCB )
					(*m_ProgressBarObjectCurrentIndexCB)( a );	
				

				retval &= BuildFilteredBoundsRecursive( *inputBoundHier->m_ContainedObjects[ a ], builtBounds);
			}
			
			delete bnd;

			if (retval.OK())
			{
				retval &= SerializeFilteredBounds(builtBounds);
			}
			
			atMap< atString, atArray< FilteredBound* > >::Iterator iter = builtBounds.CreateIterator();
			//clean up our created bounds
			iter.Start();
			while (!iter.AtEnd())
			{
				atArray< FilteredBound* >& boundArray = iter.GetData();
				for( int bound = 0; bound < boundArray.GetCount(); bound++ )
				{
					delete boundArray[bound];
					boundArray[bound] = NULL;
				}
				iter.Next();
			}
		}
		else
		{
			int objCount = inputBoundHier->m_ContainedObjects.GetCount();

			for( int a = 0; a < objCount; a++ )
			{
				if( m_ProgressBarObjectCountCB )
					(*m_ProgressBarObjectCountCB )( objCount );	

				if( m_ProgressBarObjectCurrentIndexCB )
					(*m_ProgressBarObjectCurrentIndexCB)( a );	

				retval &= SerializeRecursive( *inputBoundHier->m_ContainedObjects[ a ] );
			}
		}
	}
	else
	{
		retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	phMaterialMgr::GetInstance().Destroy();
	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////

static void GetTriNormal(const Vector3& a,const Vector3& b,const Vector3& c, Vector3& norm)
{
	Vector3 AB, BC;
	AB.Subtract(a,b);
	BC.Subtract(c,b);
	AB.Normalize();
	BC.Normalize();
	norm.Cross(AB,BC);
	norm.Normalize();
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::SerializeRecursive( rexObject& object ) const
{
	rexResult retval( rexResult::PERFECT );

	const rexObjectGenericBoundHierarchy* inputBoundHier = dynamic_cast<const rexObjectGenericBoundHierarchy*>( &object );
	if( inputBoundHier )
	{
		int objCount = inputBoundHier->m_ContainedObjects.GetCount();
		for( int a = 0; a < objCount; a++ )
		{
			if( m_ProgressBarObjectCountCB )
				(*m_ProgressBarObjectCountCB )( objCount );	

			if( m_ProgressBarObjectCurrentIndexCB )
				(*m_ProgressBarObjectCurrentIndexCB)( a );	

			retval &= SerializeRecursive( *inputBoundHier->m_ContainedObjects[ a ] );
		}
	}
	else
	{
		const rexObjectGenericBound *inputBound = dynamic_cast<const rexObjectGenericBound*>( &object );
		if( inputBound && inputBound->m_IsValid && ( m_UseChildrenOfLevelInstanceNodes || !inputBound->m_ChildOfLevelInstanceNode ) )
		{
			if( m_ProgressBarTextCB )
			{
				static char message[1024];
				sprintf( message, "Serializing bound %s.bnd", (const char*)inputBound->GetMeshName() );
				(*m_ProgressBarTextCB) ( message );
			}

			phBound *bnd = NULL;
			
			retval = GenericBoundToPhBound(bnd, inputBound, false, true);

			if( retval.OK() )
			{				
				if (!(
					  GetUseBoundFiltering() && 
					    (
							inputBound->m_Type == phBound::GEOMETRY ||
							inputBound->m_Type == phBound::BOX ||
							inputBound->m_Type == phBound::CAPSULE ||
							inputBound->m_Type == phBound::SPHERE
					    )	
				      )
				   )
				{
					retval &= SerializeBound(inputBound->GetMeshName(), bnd);
				}				
			}

			delete bnd;
		}
		else
		{
			retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start filtered bounds code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::BuildFilteredBoundsRecursive(rexObject& object, atMap< atString, atArray< FilteredBound* > >& outBounds) const
{
	rexResult retval(rexResult::PERFECT);

	// Start temp code
	rexObjectGenericBound* genBound = dynamic_cast<rexObjectGenericBound*>(&object);
	int childCount = genBound->m_ContainedObjects.GetCount();
	
	for( int a = 0; a < childCount; a++ )
	{
		retval &= BuildFilteredBoundsRecursive( *genBound->m_ContainedObjects[ a ], outBounds );
	}
	// ...end temp code

	const rexObjectGenericBoundHierarchy* inputBoundHier = dynamic_cast<const rexObjectGenericBoundHierarchy*>( &object );
	if( inputBoundHier )
	{
		int objCount = inputBoundHier->m_ContainedObjects.GetCount();
		for( int a = 0; a < objCount; a++ )
		{
			if( m_ProgressBarObjectCountCB )
				(*m_ProgressBarObjectCountCB )( objCount );	

			if( m_ProgressBarObjectCurrentIndexCB )
				(*m_ProgressBarObjectCurrentIndexCB)( a );	

			retval &= BuildFilteredBoundsRecursive( *inputBoundHier->m_ContainedObjects[ a ], outBounds );
		}
	}
	else
	{
		const rexObjectGenericBound *inputBound = dynamic_cast<const rexObjectGenericBound*>( &object );
		if( inputBound && inputBound->m_IsValid && ( m_UseChildrenOfLevelInstanceNodes || !inputBound->m_ChildOfLevelInstanceNode ) )
		{
			retval &= GenericBoundToFilteredphBounds(inputBound, outBounds);
		}
		else
		{
			retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::SerializeFilteredBounds(atMap< atString, atArray< FilteredBound* > >& boundMap) const
{
	rexResult retval(rexResult::PERFECT);

	Matrix34 identity;
	identity.Identity();
	static char builtname[256];
	atMap< atString, atArray< FilteredBound* > >::Iterator iter = boundMap.CreateIterator();
	iter.Start();
	while (!iter.AtEnd())
	{
		atString& flagName = iter.GetKey();
		if (flagName.length())
		{
			atArray< FilteredBound* >& boundArray = iter.GetData();
			
			//figure out object count(polygon), vertex count, and material count
			atMap<phMaterialMgr::Id, int> matIdToArrayIndex;
			atArray<phMaterialMgr::Id> usedUniqueMats;
			int vertCount = 0, matCount = 0, objCount = 0;
			bool secondSurface = 0;

#if PH_MATERIAL_ID_64BIT
			int matcolorCount = 0;
#endif

			for( int bid = 0; bid < boundArray.GetCount(); bid++ )
			{
				if (boundArray[bid]->m_Bound->GetType() == phBound::GEOMETRY)
				{
					phBoundGeometry* boundGeometry = (phBoundGeometry*)boundArray[bid]->m_Bound;
					objCount += boundGeometry->GetNumPolygons();
					vertCount += boundGeometry->GetNumVertices();
#if PH_MATERIAL_ID_64BIT
					matcolorCount += boundGeometry->GetNumMaterialColors();
#endif
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
					secondSurface = (boundGeometry->GetHasSecondSurface() ? true : false);
#endif
					
					int polycnt = boundGeometry->GetNumPolygons();

					for( int ply = 0; ply < polycnt; ply++ )
					{
#if POLY_MAX_VERTICES == 4
						phPolygon& polyGeom = boundGeometry->GetPolygon(ply);
						int mi = polyGeom.GetMaterialIndex();
#else
						int mi = boundGeometry->GetPolygonMaterialIndex(ply);
#endif // POLY_MAX_VERTICES == 4
						phMaterialMgr::Id mid = boundGeometry->GetMaterialId(mi);

						if (!matIdToArrayIndex.Access(mid))
						{
							int index = usedUniqueMats.GetCount();
							usedUniqueMats.PushAndGrow(mid);
							matIdToArrayIndex.Insert(mid, index);
							matCount += 1;
						}
					}
				}
				else if (boundArray[bid]->m_Bound->GetType() == phBound::BOX)
				{
					phBoundBox* boundBox = (phBoundBox*)boundArray[bid]->m_Bound;
					objCount += 1;
					vertCount += 4;

					phMaterialMgr::Id mid = boundBox->GetMaterialId(0);
					if (!matIdToArrayIndex.Access(mid))
					{
						int index = usedUniqueMats.GetCount();
						usedUniqueMats.PushAndGrow(mid);
						matIdToArrayIndex.Insert(mid, index);
						matCount += 1;
					}
				}
				else if (boundArray[bid]->m_Bound->GetType() == phBound::CAPSULE)
				{
					phBoundCapsule* boundCapsule = (phBoundCapsule*)boundArray[bid]->m_Bound;
					objCount += 1;
					vertCount += 2;
					phMaterialMgr::Id mid = boundCapsule->GetMaterialId(0);
					if (!matIdToArrayIndex.Access(mid))
					{
						int index = usedUniqueMats.GetCount();
						usedUniqueMats.PushAndGrow(mid);
						matIdToArrayIndex.Insert(mid, index);
						matCount += 1;
					}
				}
				else if (boundArray[bid]->m_Bound->GetType() == phBound::SPHERE)
				{
					phBoundSphere* boundSphere = (phBoundSphere*)boundArray[bid]->m_Bound;
					objCount += 1;
					vertCount += 1;
					phMaterialMgr::Id mid = boundSphere->GetMaterialId(0);
					if (!matIdToArrayIndex.Access(mid))
					{
						int index = usedUniqueMats.GetCount();
						usedUniqueMats.PushAndGrow(mid);
						matIdToArrayIndex.Insert(mid, index);
						matCount += 1;
					}
				}
				else
				{
					retval.Set( rexResult::ERRORS, rexResult::NOT_IMPLEMENTED );
					retval.AddMessage( "BVH Bound Filtering for bound type %d not implemented yet.", boundArray[bid]->m_Bound->GetType());
				}
			}

			//Create our bvh to fill
			phBoundBVH* boundBVH = new phBoundBVH;

			const int numPerVertAttribs=0;
#if PH_MATERIAL_ID_64BIT
			boundBVH->Init(vertCount,numPerVertAttribs,matCount,matcolorCount,objCount, secondSurface);
#else
			boundBVH->Init(vertCount,numPerVertAttribs,matCount,objCount,secondSurface);
#endif
			//Add materials
			for( int mat = 0; mat < matCount; mat++ )
			{
				boundBVH->SetMaterial(usedUniqueMats[mat], mat);
			}

			//fill our bvh
			int curObj = 0, curVOffset = 0;
			for( int bid = 0; bid < boundArray.GetCount(); bid++ )
			{
				if (boundArray[bid]->m_Bound->GetType() == phBound::GEOMETRY)
				{
					phBoundGeometry* boundGeometry = (phBoundGeometry*)boundArray[bid]->m_Bound;
#if PH_MATERIAL_ID_64BIT
					for(int i=0; i< boundGeometry->GetNumMaterialColors(); i++)
					{
						Color32 col = boundGeometry->GetMaterialColorDirectly(i);
						boundBVH->SetMaterialColor(i, col);
					}
#endif

					int polycnt = boundGeometry->GetNumPolygons();
					for( int ply = 0; ply < polycnt; ply++ )
					{
						phPolygon& polyGeom = boundGeometry->GetPolygon(ply);
						int vi0 = polyGeom.GetVertexIndex(0);
						int vi1 = polyGeom.GetVertexIndex(1);
						int vi2 = polyGeom.GetVertexIndex(2);

						//add the vertex data
						boundBVH->SetVertex(curVOffset+vi0,boundGeometry->GetVertex(vi0));
						boundBVH->SetVertex(curVOffset+vi1,boundGeometry->GetVertex(vi1));
						boundBVH->SetVertex(curVOffset+vi2,boundGeometry->GetVertex(vi2));

						int mi = boundGeometry->GetPolygonMaterialIndex(curObj+ply);
						phMaterialMgr::Id mid = boundGeometry->GetMaterialId(mi);

						//init the object
						phPolygon& poly = boundBVH->GetPolygon(curObj+ply);
						boundBVH->InitPolygonTri(poly, phPolygon::Index(curVOffset+vi0), phPolygon::Index(curVOffset+vi1), phPolygon::Index(curVOffset+vi2));

						//add the material
						int* fIndex = matIdToArrayIndex.Access(mid);
						Assert(fIndex);
#if POLY_MAX_VERTICES == 4
						poly.SetMaterialIndex((u8)(*fIndex));
#else
						boundBVH->SetPolygonMaterialIndex(curObj+ply, *fIndex);
#endif // #if POLY_MAX_VERTICES == 4
					}

					curObj += polycnt;
					curVOffset += boundGeometry->GetNumVertices();
				}
				else if (boundArray[bid]->m_Bound->GetType() == phBound::BOX)
				{
					phBoundBox* boundBox = (phBoundBox*)boundArray[bid]->m_Bound;
					
					Vec3V halfWidth, centre;
					boundBox->GetBoundingBoxHalfWidthAndCenter(halfWidth, centre);
					Vector3 pos = boundArray[bid]->m_Transform.d;

					Vector3 v0 = Vector3(pos.x + halfWidth.GetX().Getf(), pos.y + halfWidth.GetY().Getf(), pos.z + halfWidth.GetZ().Getf());
					Vector3 v1 = Vector3(pos.x - halfWidth.GetX().Getf(), pos.y + halfWidth.GetY().Getf(), pos.z - halfWidth.GetZ().Getf());
					Vector3 v2 = Vector3(pos.x + halfWidth.GetX().Getf(), pos.y - halfWidth.GetY().Getf(), pos.z - halfWidth.GetZ().Getf());
					Vector3 v3 = Vector3(pos.x - halfWidth.GetX().Getf(), pos.y - halfWidth.GetY().Getf(), pos.z + halfWidth.GetZ().Getf());
					
					phMaterialMgr::Id mid = boundBox->GetMaterialId(0);
					
					//add the vertex data
					boundBVH->SetVertex(curVOffset,RCC_VEC3V(v0));
					boundBVH->SetVertex(curVOffset+1,RCC_VEC3V(v1));
					boundBVH->SetVertex(curVOffset+2,RCC_VEC3V(v2));
					boundBVH->SetVertex(curVOffset+3,RCC_VEC3V(v3));

					//init the object
					phPolygon& poly = boundBVH->GetPolygon(curObj);
					boundBVH->InitBox(poly, phPolygon::Index(curVOffset), phPolygon::Index(curVOffset+1), phPolygon::Index(curVOffset+2), phPolygon::Index(curVOffset+3));

					//add the material
					int* fIndex = matIdToArrayIndex.Access(mid);
					Assert(fIndex);
#if POLY_MAX_VERTICES == 4
					poly.SetMaterialIndex((u8)(*fIndex));
#else
					boundBVH->SetPolygonMaterialIndex(curObj, *fIndex);
#endif // #if POLY_MAX_VERTICES == 4

					curObj += 1;
					curVOffset += 4;
				}
				else if (boundArray[bid]->m_Bound->GetType() == phBound::CAPSULE)
				{
					phBoundCapsule* boundCapsule = (phBoundCapsule*)boundArray[bid]->m_Bound;
					const Vec3V start = boundCapsule->GetEndPointA();
					const Vec3V end = boundCapsule->GetEndPointB();
					float radius = boundCapsule->GetRadius();
					phMaterialMgr::Id mid = boundCapsule->GetMaterialId(0);

					//add the vertex data
					boundBVH->SetVertex(curVOffset,start);
					boundBVH->SetVertex(curVOffset+1,end);

					//init the object
					phPolygon& poly = boundBVH->GetPolygon(curObj);
					boundBVH->InitCapsule(poly, phPolygon::Index(curVOffset), phPolygon::Index(curVOffset+1), radius);

					//add the material
					int* fIndex = matIdToArrayIndex.Access(mid);
					Assert(fIndex);
#if POLY_MAX_VERTICES == 4
					poly.SetMaterialIndex((u8)(*fIndex));
#else
					boundBVH->SetPolygonMaterialIndex(curObj, *fIndex);
#endif // #if POLY_MAX_VERTICES == 4

					curObj += 1;
					curVOffset += 2;
				}
				else if (boundArray[bid]->m_Bound->GetType() == phBound::SPHERE)
				{
					phBoundSphere* boundSphere = (phBoundSphere*)boundArray[bid]->m_Bound;
					const Vector3& start = boundArray[bid]->m_Transform.d;
					float radius = boundSphere->GetRadius();
					phMaterialMgr::Id mid = boundSphere->GetMaterialId(0);

					//add the vertex data
					boundBVH->SetVertex(curVOffset,RCC_VEC3V(start));

					//init the object
					phPolygon& poly = boundBVH->GetPolygon(curObj);
					boundBVH->InitSphere(poly, phPolygon::Index(curVOffset), radius);

					//add the material
					int* fIndex = matIdToArrayIndex.Access(mid);
					Assert(fIndex);
#if POLY_MAX_VERTICES == 4
					poly.SetMaterialIndex((u8)(*fIndex));
#else
					boundBVH->SetPolygonMaterialIndex(curObj, *fIndex);
#endif // #if POLY_MAX_VERTICES == 4

					curObj += 1;
					curVOffset += 1;
				}
				else
				{
					retval.Set( rexResult::ERRORS, rexResult::NOT_IMPLEMENTED );
					retval.AddError( "Bound Filtering for anything except GEOMETRY is not implemented yet.");
				}
			}

			Assert(vertCount == curVOffset);
			Assert(objCount == curObj);

			//build the bvh hierarchy and serialize this thing
			boundBVH->CalculateGeomExtents();
			boundBVH->Build();
			formatf(builtname, 256, "%s", flagName.c_str());
			retval &= SerializeBound(atString(builtname), boundBVH);
			delete boundBVH;
		}
		else
		{
			retval.Set( rexResult::ERRORS, rexResult::FILE_ERROR );
			retval.AddError( "Bound flag name is \"\" check the tagging on the bound and make sure it is supported by the rule file.");
		}
		iter.Next();
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::SerializeBound(const atString& name, phBound* bound) const
{
	rexResult retval(rexResult::PERFECT);

	if(rexShell::DoesntSerialise())
		return retval;

	if( bound )
	{
		fiStream* stream = ASSET.Create( name, "bnd" );
		if( stream )
		{
			fiAsciiTokenizer tok;
			tok.Init( name, stream );

			phBound::Save( tok, bound );
			stream->Close();
		}
		else
		{
			// Oh dear, something bad has happened
			retval.Combine( rexResult::ERRORS, rexResult::CANNOT_OPEN_FILE );

			// Get full path for debugging
			char acFullPathForDebugging[255];
			ASSET.FullPath(acFullPathForDebugging,sizeof(acFullPathForDebugging), name,"bnd" );

			// Flip the slashes
			for(u32 s=0; s<strlen(acFullPathForDebugging); s++) 
			{
				if(acFullPathForDebugging[s] == '\\') 
				{
					acFullPathForDebugging[s] = '/';
				}
			}

			// Error
			retval.AddError( "Could not create file '%s'", acFullPathForDebugging);
		}
	}
	else
	{
		// Oh dear, something bad has happened
		retval.Combine( rexResult::ERRORS, rexResult::INVALID_INPUT );

		// Error
		retval.AddError( "phBound* bound parameter must be non NULL!");
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::GenericBoundToFilteredphBounds(const rexObjectGenericBound *inputBound, atMap< atString, atArray< FilteredBound* > >& outBounds, bool /*ignoreChildren*/) const
{
	rexResult retval(rexResult::PERFECT);

	atArray<phMaterialMgr::Id> materials;
	atArray<int> materialMap;
	retval = rexSerializerRAGEPhysicsBound::CreatePhysicsMaterials(materials, materialMap, inputBound);

	if(retval.Errors())
		return retval;

	if (inputBound->m_Type == phBound::GEOMETRY)
	{
		//if (!inputBound->m_BoneIndex)
		{
			retval &= GenericBoundToFilteredPhBoundGeometry(outBounds, inputBound, materials, materialMap);
		}
	}
	if (inputBound->m_Type == phBound::BOX)
	{
		//if (!inputBound->m_BoneIndex)
		{
			retval &= GenericBoundToFilteredPhBoundBox(outBounds, inputBound, materials, materialMap);
		}
	}
	if (inputBound->m_Type == phBound::CAPSULE)
	{
		//if (!inputBound->m_BoneIndex)
		{
			retval &= GenericBoundToFilteredPhBoundCapsule(outBounds, inputBound, materials, materialMap);
		}
	}
	if (inputBound->m_Type == phBound::SPHERE)
	{
		//if (!inputBound->m_BoneIndex)
		{
			retval &= GenericBoundToFilteredPhBoundSphere(outBounds, inputBound, materials, materialMap);
		}
	}
	else
	{
		//retval.Set( rexResult::ERRORS, rexResult::NOT_IMPLEMENTED );
		//retval.AddMessage( "Bound Filtering for anything except GEOMETRY is not implemented yet.");
	}
	
	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::GenericBoundToFilteredPhBoundGeometry(atMap< atString, atArray< FilteredBound* > >& outBounds, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const
{
	if (inputBound->m_Materials.GetCount() == 0)
	{
		Displayf("ART_PROBLEM: bound mesh %s has no material assigned to it. Please check all bound material assignments and assign the default material or a physics material to the mesh.", inputBound->GetMeshName());
	}

	rexResult retval(rexResult::PERFECT);
	for (int material = 0; material < inputBound->m_Materials.GetCount(); material++)
	{
		atArray< atString > filters;
		rexResult filterResult = GetBoundFilterType(filters, inputBound->m_Materials[material].m_Name);
		
		if (!filters.GetCount())
		{
			filterResult.Combine( rexResult::ERRORS, rexResult::INVALID );
			filterResult.AddError( "No filtered bound types found in material name \"%s\" on mesh \"%s\" (%s). Please check tagging and make sure that (%s) is part of the correct bound set (if used).", inputBound->m_Materials[material].m_Name.c_str(), inputBound->GetMeshName().c_str(), inputBound->m_FullPath.c_str(), inputBound->GetMeshName().c_str());
		}

		if (filterResult.OK())
		{
            for (int flt = 0; flt < filters.GetCount(); flt++)
            {
			
			    const phBoundGeometry* storedBound = GetFilteredPhBoundGeometry(outBounds, filters[flt]);

			    phBoundGeometry* appendedBound = NULL;
			    retval &= FillFilteredPhBoundGeometry(appendedBound, storedBound, material, inputBound, materials, materialMap);

			    if (retval.OK())
			    {
					FilteredBound* filteredbound = new FilteredBound();
					filteredbound->m_Bound = appendedBound;
				    InsertReplaceFilteredPhBoundGeometry(filteredbound, outBounds, filters[flt]);
			    }
			    else
			    {
				    if (appendedBound)
				    {
					    //clean up our mess
					    delete appendedBound;
				    }
			    }
            }
		}
		else
		{
			retval &= filterResult;
		}
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::GenericBoundToFilteredPhBoundBox(atMap< atString, atArray< FilteredBound* > >& outBounds, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const
{
	if (inputBound->m_Materials.GetCount() == 0)
	{
		Displayf("ART_PROBLEM: bound mesh %s has no material assigned to it. Please check all bound material assignments and assign the default material or a physics material to the mesh.", inputBound->GetMeshName());
	}

	rexResult retval(rexResult::PERFECT);
	for (int material = 0; material < inputBound->m_Materials.GetCount(); material++)
	{
		atArray< atString > filters;
		rexResult filterResult = GetBoundFilterType(filters, inputBound->m_Materials[material].m_Name);
		
		if( !filters.GetCount() )
		{
			// Add a default for the time being since we are not authoring this in the max scene
			filters.PushAndGrow(atString("default"));
			filterResult.Combine( rexResult::WARNINGS );
			filterResult.AddMessageCtx( inputBound->GetMeshName().c_str(), "No filtered bound types found in material name \"%s\" on mesh \"%s\" (%s). Adding a default.", inputBound->m_Materials[material].m_Name.c_str(), inputBound->GetMeshName().c_str(), inputBound->m_FullPath.c_str());
		}
		

		if (!filters.GetCount())
		{
			filterResult.Combine( rexResult::ERRORS, rexResult::INVALID );
			filterResult.AddMessageCtx( inputBound->GetMeshName().c_str(), "No filtered bound types found in material name \"%s\" on mesh \"%s\" (%s). Please check tagging and make sure that (%s) is part of the correct bound set (if used).", inputBound->m_Materials[material].m_Name.c_str(), inputBound->GetMeshName().c_str(), inputBound->m_FullPath.c_str(), inputBound->GetMeshName().c_str());
		}

		if (filterResult.OK())
		{
			for (int flt = 0; flt < filters.GetCount(); flt++)
			{
				phBoundBox *bndBox = NULL;
				retval &= GenericBoundToPhBoundBox(bndBox, inputBound, materials, materialMap);
				
				if (retval.OK())
				{
					FilteredBound* filteredBound = new FilteredBound();
					filteredBound->m_Bound = bndBox;
					filteredBound->m_Transform = inputBound->m_Matrix;
					InsertFilteredPhBound(filteredBound, outBounds, filters[flt]);
				}
				else
				{
					if (bndBox)
					{
						//clean up our mess
						delete bndBox;
					}
				}
			}
		}
		else
		{
			retval &= filterResult;
		}
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::GenericBoundToFilteredPhBoundCapsule(atMap< atString, atArray< FilteredBound* > >& outBounds, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const
{
	if (inputBound->m_Materials.GetCount() == 0)
	{
		Displayf("ART_PROBLEM: bound mesh %s has no material assigned to it. Please check all bound material assignments and assign the default material or a physics material to the mesh.", inputBound->GetMeshName());
	}

	rexResult retval(rexResult::PERFECT);
	for (int material = 0; material < inputBound->m_Materials.GetCount(); material++)
	{
		atArray< atString > filters;
		rexResult filterResult = GetBoundFilterType(filters, inputBound->m_Materials[material].m_Name);

		if (!filters.GetCount())
		{
			filterResult.Combine( rexResult::ERRORS, rexResult::INVALID );
			filterResult.AddMessageCtx(inputBound->GetMeshName().c_str(),  "No filtered bound types found in material name \"%s\" on mesh \"%s\" (%s). Please check tagging and make sure that (%s) is part of the correct bound set (if used).", inputBound->m_Materials[material].m_Name.c_str(), inputBound->GetMeshName().c_str(), inputBound->m_FullPath.c_str(), inputBound->GetMeshName().c_str());
		}

		if (filterResult.OK())
		{
			for (int flt = 0; flt < filters.GetCount(); flt++)
			{
				phBoundCapsule *bndCapsule = NULL;
				retval &= GenericBoundToPhBoundCapsule(bndCapsule, inputBound, materials, materialMap);

				if (retval.OK())
				{
					FilteredBound* filteredBound = new FilteredBound();
					filteredBound->m_Bound = bndCapsule;
					filteredBound->m_Transform = inputBound->m_Matrix;
					InsertFilteredPhBound(filteredBound, outBounds, filters[flt]);
				}
				else
				{
					if (bndCapsule)
					{
						//clean up our mess
						delete bndCapsule;
					}
				}
			}
		}
		else
		{
			retval &= filterResult;
		}
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::GenericBoundToFilteredPhBoundSphere(atMap< atString, atArray< FilteredBound* > >& outBounds, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const
{
	if (inputBound->m_Materials.GetCount() == 0)
	{
		Displayf("ART_PROBLEM: bound mesh %s has no material assigned to it. Please check all bound material assignments and assign the default material or a physics material to the mesh.", inputBound->GetMeshName());
	}

	rexResult retval(rexResult::PERFECT);
	for (int material = 0; material < inputBound->m_Materials.GetCount(); material++)
	{
		atArray< atString > filters;
		rexResult filterResult = GetBoundFilterType(filters, inputBound->m_Materials[material].m_Name);

		if (!filters.GetCount())
		{
			filterResult.Combine( rexResult::ERRORS, rexResult::INVALID );
			filterResult.AddMessageCtx(inputBound->GetMeshName().c_str(), "No filtered bound types found in material name \"%s\" on mesh \"%s\" (%s). Please check tagging and make sure that (%s) is part of the correct bound set (if used).", inputBound->m_Materials[material].m_Name.c_str(), inputBound->GetMeshName().c_str(), inputBound->m_FullPath.c_str(), inputBound->GetMeshName().c_str());
		}

		if (filterResult.OK())
		{
			for (int flt = 0; flt < filters.GetCount(); flt++)
			{
				phBoundSphere *bndSphere = NULL;
				retval &= GenericBoundToPhBoundSphere(bndSphere, inputBound, materials, materialMap);

				if (retval.OK())
				{
					FilteredBound* filteredBound = new FilteredBound();
					filteredBound->m_Bound = bndSphere;
					filteredBound->m_Transform = inputBound->m_Matrix;
					InsertFilteredPhBound(filteredBound, outBounds, filters[flt]);
				}
				else
				{
					if (bndSphere)
					{
						//clean up our mess
						delete bndSphere;
					}
				}
			}
		}
		else
		{
			retval &= filterResult;
		}
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::GetBoundFilterType(atArray< atString >& filters, const atString& materialName) const
{
	rexResult retval(rexResult::PERFECT);
	
	if (m_BoundFilterTypes.GetCount())
	{
        filters.clear();
        atString added("");
		

		//assumed materialName format - boundm,somevalue,anothervalue
		const char* mName = materialName.c_str();
		if (mName)
		{
			int strLength = (int)strlen(mName);
			int charIndex = 0;
			while (charIndex < strLength)
			{
				atString atType;
				while( (mName[charIndex] != ',') &&
					(charIndex < strLength) )
				{
					atType += mName[charIndex++];
				}
				
				if (m_BoundFilterTypes.Find(atType)!= -1)
				{
					//only add unique
					if (!strstr(added.c_str(),atType.c_str()))
					{
                        filters.PushAndGrow(atType);
						added += atType;
					}
					else
					{
						Warningf("Multiple taggings with same bound type. [%s] Only processing the unique tag.",materialName.c_str());
					}
				}
				charIndex++;
			}
		}
	}
	else
	{
		filters.PushAndGrow(atString("default"));
		retval.Combine( rexResult::WARNINGS );
		retval.AddMessage( "Need rexSerializerRAGEPhysicsBound::m_BoundFilterTypes to have types, default filter has been added." );
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const phBoundGeometry* rexSerializerRAGEPhysicsBound::GetFilteredPhBoundGeometry(atMap< atString, atArray< FilteredBound* > >& outBounds, const atString& filter) const
{
	atArray< FilteredBound* >* boundList = outBounds.Access(filter);
	if (!boundList)
	{
		outBounds.Insert(filter, atArray< FilteredBound* >());
		boundList = outBounds.Access(filter);
	}	

	Assert(boundList);

	const phBoundGeometry* storedBound = NULL;
	for(int bound = 0; bound < boundList->GetCount(); bound++)
	{
		if (((*boundList)[bound])->m_Bound->GetType() == phBound::GEOMETRY)
		{
			storedBound = dynamic_cast<phBoundGeometry*>((*boundList)[bound]->m_Bound);
			break;
		}
	}

	return storedBound;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::FillFilteredPhBoundGeometry(phBoundGeometry*& appendedBound, const phBoundGeometry* storedBound, const int curMaterialsIndex, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap) const
{
	rexResult retval(rexResult::PERFECT);

	char curMaterialName[128];
	phMaterialMgr::GetInstance().GetMaterialName(materials[curMaterialsIndex], curMaterialName, 128);

	int matIndex, polyVertCount;
	atArray<int> primsToAdd;
	atArray<int> vertsToAdd;

	//add all polys that have the correct material to our list
	int numpolys = inputBound->m_Primitives.GetCount();
	
	int truePolyCount = 0;
	for(int poly = 0; poly < numpolys; poly++)
	{
		// set the physics material
		// material index of the primitive is -1 if the
		// maya material failed to be converted to a phMaterial.
		// in this case, use the default material (with index 0)
		// otherwise, look up the appropriate material in the maya material->phMaterial mapping
		matIndex = inputBound->m_Primitives[poly].m_MaterialIndex;
		matIndex = (matIndex >= 0) ? materialMap[matIndex] : 0;
		
		if (materials[matIndex] == materials[curMaterialsIndex])
		{
			//check to see if we have too many polys here.
			if ((primsToAdd.GetCount() + 16 /*allocStep*/ > 65535 /*_CounterMax*/))
			{
				Errorf("Adding the next poly [%d] will cause the polygon limit to be hit for bounds with material(s) [%s]. Please optimize the polygons, split the scene, or ask a tools programmer for help.", poly, curMaterialName);
				break; //get out and dont add no more.
			}

			primsToAdd.PushAndGrow(poly);

			const rexObjectGenericMesh::PrimitiveInfo &prim = inputBound->m_Primitives[poly];
			polyVertCount = prim.m_Adjuncts.GetCount();

			// no adjuncts is really bad
			Assert( polyVertCount && "Poly with no adjuncts!" );

			int prims = polyVertCount - 2;
			bool boundQuad = inputBound->primitiveIsQuad(poly);
			if( boundQuad )
				truePolyCount += 1;
			else if( prims > 0 )
				truePolyCount += prims;

			for(int abju = 0; abju < polyVertCount; abju++)
			{
				if (vertsToAdd.Find(prim.m_Adjuncts[abju].m_VertexIndex) == -1)
				{
					vertsToAdd.PushAndGrow(prim.m_Adjuncts[abju].m_VertexIndex);
				}
			}
		}
	}

	//if we don't have any prims ... this is bad!
	if (!primsToAdd.GetCount())
	{
		retval.Combine( rexResult::ERRORS, rexResult::INVALID_INPUT );
		retval.AddError("No prims with material %s found in bound %s. This should not be.", curMaterialName, inputBound->GetMeshName());
		return retval;
	}

	int totVerts = vertsToAdd.GetCount();
	int totPolys = truePolyCount;
#if	HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	int totMatCols = inputBound->m_colourTemplate.GetCount(); 
#endif
	int totMats = 1; //default to one
	bool addMyMtl = true;

	const Vector3* boundVerts = NULL;
	const phPolygon* boundPolys = NULL;
	atStringArray materialNames;
	if (storedBound)
	{
		//Get already loaded stuff
		boundVerts = (const Vector3*)storedBound->GetVertexPointer();
		boundPolys = storedBound->GetPolygonPointer();
		totVerts += storedBound->GetNumVertices();
		totPolys += storedBound->GetNumPolygons();
		
		// per material
		totMats = (int)storedBound->GetNumMaterials();
		char mtlName[128];
		for (int mtl = 0; mtl < totMats; mtl++)
		{
			storedBound->GetMaterialName(mtl, mtlName, 128);
			materialNames.PushAndGrow(atString(mtlName));

			//if we are the same as the current material - no need to add
			if (!strcmp(curMaterialName, mtlName))
			{
				addMyMtl = false;
			}
		}	

		if (addMyMtl)
		{
			//need to add me to the list
			totMats += 1;
		}
	}

	//create a new bound to store our updated info.
	appendedBound = new phBoundGeometry();

	const int numPerVertAttribs=0;
#if HACK_GTA4
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
	#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		appendedBound->Init(totVerts, numPerVertAttribs, totMats, totMatCols, totPolys, 1);
		for(int i=0; i< inputBound->m_colourTemplate.GetCount(); i++)
		{
			Color32 col = Color32((u32)inputBound->m_colourTemplate[i].x, (u32)inputBound->m_colourTemplate[i].y, (u32)inputBound->m_colourTemplate[i].z, (u32)inputBound->m_colourTemplate[i].w);
			appendedBound->SetMaterialColor(i, col);
		}
	#else
		appendedBound->Init(totVerts, numPerVertAttribs, totMats, totPolys, 1);
	#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
#else
	#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		appendedBound->Init(totVerts, numPerVertAttribs, totMats, 0, totPolys);
	#else
		appendedBound->Init(totVerts, numPerVertAttribs, totMats, totPolys);
	#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
#endif // HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
#else
	appendedBound->Init(totVerts, numPerVertAttribs, totMats, totPolys);
#endif // HACK_GTA4
	
	// build vertex array
	atArray<int> addedVertMapping;
	//init our mapping
	addedVertMapping.Reserve(inputBound->m_Vertices.GetCount());
	for (int i=0; i < inputBound->m_Vertices.GetCount(); i++)
	{
		addedVertMapping.Append() = -1; 
	}

	float conversionFactor = GetUnitTypeLinearConversionFactor();
	for(int addVertIndex = 0; addVertIndex < totVerts; addVertIndex++)
	{
		int numOldVerts = 0;
		if (storedBound)
		{
			numOldVerts = storedBound->GetNumVertices();
		}
		
		if (addVertIndex < numOldVerts)
		{
			Assert(storedBound);
			Assert(boundVerts);

			//these are already converted so no need to do them again
			appendedBound->SetVertex(addVertIndex, RCC_VEC3V(boundVerts[addVertIndex]));
		}
		else
		{
			int arrayIndex = addVertIndex - numOldVerts;
			Vector3 v;
			inputBound->m_Matrix.Transform( inputBound->m_Vertices[vertsToAdd[arrayIndex]].m_Position, v );
			inputBound->m_ParentBoneMatrix.UnTransform( v );
			v.Scale(conversionFactor);
			appendedBound->SetVertex(addVertIndex, RCC_VEC3V(v));
			addedVertMapping[vertsToAdd[arrayIndex]] = addVertIndex;
		}
	}

	// set extra vertices to <0,0,0>, so that NAN exceptions aren't raised in Fourtified functions
	Vector3 zeroVector(0.0f,0.0f,0.0f);
	for(int i = totVerts; i < Fourtify(totVerts); i++)
	{	
		appendedBound->SetVertex(i, RCC_VEC3V(zeroVector)); 
	}

	//build material array and find active material
	int activeMappedMatIndex = -1;
	if (storedBound)
	{
		if (!addMyMtl)
		{
			activeMappedMatIndex = materialNames.Find(atString(curMaterialName));
		}	
		else
		{
			//it should be added at the end
			activeMappedMatIndex = totMats-1;
		}

		//add old mtls
		Assert(materialNames.GetCount() == storedBound->GetNumMaterials());
		for(int mtl = 0; mtl < materialNames.GetCount(); mtl++)
		{
			appendedBound->SetMaterialId(mtl, storedBound->GetMaterialId(mtl));
		}
	}
	else
	{
		//it is the first index == 0
		activeMappedMatIndex = 0;
	}

	if (addMyMtl)
	{
		appendedBound->SetMaterialId(activeMappedMatIndex, materials[curMaterialsIndex]);
	}

	Assert(activeMappedMatIndex != -1);

	//build polygon array
	
#if POLY_MAX_VERTICES == 4 
	u32 polyStartVert, i0, i1, i2, i3;
	u32 convertedI0, convertedI1, convertedI2, convertedI3;
#else
	u32 polyStartVert, i0, i1;
	u32 convertedI0, convertedI1, convertedI2;
#endif // POLY_MAX_VERTICES == 4 
	int physPolyCount = 0;
	phPolygon newPoly;
	for(int curPoly = 0; curPoly < totPolys; curPoly++)
	{
		int numOldPolys = 0;
		if (storedBound)
		{
			numOldPolys = storedBound->GetNumPolygons();
		}

		if (curPoly < numOldPolys)
		{
			Assert(storedBound);
			Assert(boundPolys);

			//these are already converted so no need to do them again
			appendedBound->SetPolygon(curPoly, boundPolys[curPoly]);
			physPolyCount++;
		}
		else
		{
			int arrayIndex = curPoly - numOldPolys;

			// convenience variable
			const rexObjectGenericMesh::PrimitiveInfo &prim = inputBound->m_Primitives[primsToAdd[arrayIndex]];

			polyVertCount = prim.m_Adjuncts.GetCount();

			Assert( polyVertCount );

			polyStartVert = (u32)prim.m_Adjuncts[0].m_VertexIndex;

			if( !(inputBound->primitiveIsQuad(arrayIndex)) )
			{
				// convert this polygon into triangles
				int polyTriCount = polyVertCount - 2;
				for(int j = 1; j <= polyTriCount; j++)
				{
					i0 = (u32)prim.m_Adjuncts[j].m_VertexIndex;
					i1 = (u32)prim.m_Adjuncts[j+1].m_VertexIndex;

					Vector3 polyNormal;
					Vector3 polyCenter;
					polyCenter.Add( inputBound->m_Vertices[ i0 ].m_Position, inputBound->m_Vertices[ i1 ].m_Position );
					polyCenter.Add( inputBound->m_Vertices[ polyStartVert ].m_Position );
					polyCenter.Scale( 1.0f / 3.0f );
					GetTriNormal( inputBound->m_Vertices[ i0 ].m_Position, polyCenter, inputBound->m_Vertices[ i1 ].m_Position, polyNormal );
					float dotProduct = polyNormal.Dot( prim.m_FaceNormal );
					
					//convert vertex indexes
					convertedI0 = ConvertToCorrectVertexIndex(polyStartVert, addedVertMapping);
					convertedI1 = ConvertToCorrectVertexIndex(i0, addedVertMapping);
					convertedI2 = ConvertToCorrectVertexIndex(i1, addedVertMapping);
					
					//init poly
					if( dotProduct < 0.0f )
						appendedBound->InitPolygonTri( newPoly, phPolygon::Index(convertedI0), phPolygon::Index(convertedI2), phPolygon::Index(convertedI1) );
						//newPoly.InitPolygonTri( convertedI0, convertedI2, convertedI1, appendedBound->GetVertex(convertedI0), appendedBound->GetVertex(convertedI1), appendedBound->GetVertex(convertedI2));					
					else
						appendedBound->InitPolygonTri( newPoly, phPolygon::Index(convertedI1), phPolygon::Index(convertedI2), phPolygon::Index(convertedI0) );
						//newPoly.InitTriangle(convertedI1, convertedI2, convertedI0, appendedBound->GetVertex(convertedI1), appendedBound->GetVertex(convertedI2), appendedBound->GetVertex(convertedI0));

					//set material
#if POLY_MAX_VERTICES == 4
					newPoly.SetMaterialIndex((u8) activeMappedMatIndex );
					Vector3 vecTemp = VEC3V_TO_VECTOR3(newPoly.GetUnitNormal());//, bnd->GetVertexPointer());
#else
					appendedBound->SetPolygonMaterialIndex(physPolyCount, activeMappedMatIndex);
					Vector3 vecTemp = polyNormal;//, bnd->GetVertexPointer());
#endif // POLY_MAX_VERTICES == 4
					if(vecTemp.Mag2() < 0.9f)
					{
						// TODO: write a warning to log (polygon [index] has non-unit normal, determine type of poly by using IsTriangle()) - wil
						retval.Set(rexResult::WARNINGS, rexResult::CONVERSION);
					}
					else
					{
						appendedBound->SetPolygon(physPolyCount++, newPoly);
					}
				}
			}
			else
			{
#if POLY_MAX_VERTICES == 3
				retval.Combine(rexResult::ERRORS);
				retval.AddError("Attempt to export a quad.  Not supported");	
#else
				// the last index for a quad CANNOT be zero (that's how we
				// determine if a poly is a tri or a quad) so we have to
				// rotate the vertices.
				if(0 == polyStartVert)
				{
					i0 = polyStartVert;
					i1 = (u32)prim.m_Adjuncts[1].m_VertexIndex;
					i2 = (u32)prim.m_Adjuncts[2].m_VertexIndex;
					i3 = (u32)prim.m_Adjuncts[3].m_VertexIndex;
				}
				else
				{
					i0 = (u32)prim.m_Adjuncts[1].m_VertexIndex;
					i1 = (u32)prim.m_Adjuncts[2].m_VertexIndex;
					i2 = (u32)prim.m_Adjuncts[3].m_VertexIndex;
					i3 = polyStartVert;
				}

				Vector3 polyNormal;
				Vector3 polyCenter;
				polyCenter.Add( inputBound->m_Vertices[ i0 ].m_Position, inputBound->m_Vertices[ i1 ].m_Position );
				polyCenter.Add( inputBound->m_Vertices[ i2 ].m_Position );
				polyCenter.Add( inputBound->m_Vertices[ i3 ].m_Position );
				polyCenter.Scale( 0.25f );
				GetTriNormal( inputBound->m_Vertices[ i0 ].m_Position, polyCenter, inputBound->m_Vertices[ i1 ].m_Position, polyNormal );
				float dotProduct = polyNormal.Dot( prim.m_FaceNormal );

				//convert vertex indexes
				convertedI0 = ConvertToCorrectVertexIndex(i0, addedVertMapping);
				convertedI1 = ConvertToCorrectVertexIndex(i1, addedVertMapping);
				convertedI2 = ConvertToCorrectVertexIndex(i2, addedVertMapping);
				convertedI3 = ConvertToCorrectVertexIndex(i3, addedVertMapping);
				
				//init poly
				if( dotProduct < 0.0f )
					appendedBound->InitPolygonQuad( newPoly, convertedI2, convertedI1, convertedI0, convertedI3 );
					//newPoly.InitQuad(convertedI2, convertedI1, convertedI0, convertedI3, appendedBound->GetVertex(convertedI2), appendedBound->GetVertex(convertedI1), appendedBound->GetVertex(convertedI0), appendedBound->GetVertex(convertedI3));
				else
					appendedBound->InitPolygonQuad( newPoly, convertedI0, convertedI1, convertedI2, convertedI3 );
					//newPoly.InitQuad(convertedI0, convertedI1, convertedI2, convertedI3, appendedBound->GetVertex(convertedI0), appendedBound->GetVertex(convertedI1), appendedBound->GetVertex(convertedI2), appendedBound->GetVertex(convertedI3));

				//set material
				newPoly.SetMaterialIndex((u8) activeMappedMatIndex );

				Vector3 vecTemp = VEC3V_TO_VECTOR3(newPoly.GetUnitNormal());
				if(vecTemp.Mag2() < 0.9f)
				{
					// TODO: write a warning to log (polygon [index] has non-unit normal, determine type of poly by using IsTriangle()) - wil
					retval.Set(rexResult::WARNINGS, rexResult::CONVERSION);
				}
				else
				{
					appendedBound->SetPolygon(physPolyCount++, newPoly);
				}
#endif // POLY_MAX_VERTICES == 3
			}
		}
	}
	
	// do postprocessing on bound
	appendedBound->DecreaseNumPolys(physPolyCount);
	appendedBound->PostLoadCompute();

	// We used to just use the centroid offset as the center of gravity, but CalcCGOffset should give us a much better value.
	Vector3 newCGOffset;
	appendedBound->CalcCGOffset(RC_VEC3V(newCGOffset));
	appendedBound->SetCGOffset(RCC_VEC3V(newCGOffset));
	appendedBound->ComputeNeighbors(NULL);
	appendedBound->WeldVertices(1e-3f);

#if POLY_MAX_VERTICES == 4
	if(m_QuadrifyGeometryBounds)
	{
		appendedBound->ConvertTrianglesToQuads();
	}
#endif // POLY_MAX_VERTICES == 4

	appendedBound->RemoveDegeneratePolys();

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

u32 rexSerializerRAGEPhysicsBound::ConvertToCorrectVertexIndex(u32 indexToConvert, const atArray<int>& vertMapping) const
{
	Assert(indexToConvert >= 0 && indexToConvert < (u32)vertMapping.GetCount());
	Assert(vertMapping[indexToConvert] != -1);
	return vertMapping[indexToConvert];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexSerializerRAGEPhysicsBound::InsertReplaceFilteredPhBoundGeometry(FilteredBound* appendedBound, atMap< atString, atArray< FilteredBound* > >& outBounds, const atString& filter) const
{
	Assert(appendedBound); //we must have this

	atArray< FilteredBound* >* boundList = outBounds.Access(filter);

	Assert(boundList); //must have this as well

	bool replaced = false;
	for(int bound = 0; bound < boundList->GetCount(); bound++)
	{
		if ((*boundList)[bound]->m_Bound->GetType() == appendedBound->m_Bound->GetType())
		{
			//delete the old
			delete (*boundList)[bound];

			//add the new
			(*boundList)[bound] = appendedBound;
			replaced = true;
			break;
		}
	}

	if (!replaced)
	{
		//add it
		boundList->PushAndGrow(appendedBound);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexSerializerRAGEPhysicsBound::InsertFilteredPhBound(FilteredBound* bound, atMap< atString, atArray< FilteredBound* > >& outBounds, const atString& filter) const
{
	Assert(bound); //we must have this

	atArray< FilteredBound* >* boundList = outBounds.Access(filter);

	if (!boundList)
	{
		outBounds.Insert(filter, atArray< FilteredBound* >());
		boundList = outBounds.Access(filter);
	}	

	Assert(boundList); //must have this as well

	//add it
	boundList->PushAndGrow(bound);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::CreatePhysicsMaterials( atArray<phMaterialMgr::Id> &materials, atArray<int> &materialMap, const rexObjectGenericBound *inputBound ) const
{
	rexResult retval(rexResult::PERFECT);

	int m, numMaterials = inputBound->m_Materials.GetCount();
	int numPhMaterials = 0;

	if (numMaterials == 0)
	{
		// create default material (this will push all material references out one element!)   
		materials.PushAndGrow(phMaterialMgr::DEFAULT_MATERIAL_ID);   
		numPhMaterials++;
	}

	// extract physics information from other materials
	for(m = 0; m < numMaterials; m++)
	{
		phMaterialMgr::Id mat = phMaterialMgr::DEFAULT_MATERIAL_ID;
		rexSerializerRAGEPhysicsBound::NewPhysicsMaterial(mat, inputBound->m_Materials[m]);

		if(mat)
		{
			materials.PushAndGrow(mat,(u16) numMaterials);
			numPhMaterials++;
		}
		else
		{
			if (materials.GetCount())
			{
				materials.PushAndGrow( materials[0],(u16) numMaterials );
				numPhMaterials++;
			}
			else
			{
				materials.PushAndGrow( phMaterialMgr::DEFAULT_MATERIAL_ID,(u16) numMaterials );
				numPhMaterials++;
			}

			// TODO: write a warning to the log (failed to create material) - wil
			retval.Combine( rexResult::WARNINGS, rexResult::CONVERSION );
		}

		// this will assign the last created phMaterial for any material that fails to be created
		materialMap.PushAndGrow(numPhMaterials - 1,(u16) numMaterials);
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::CreatePhysicsMaterials( atArray< phMaterialMgr::Id >* phmaterials, const atArray< rexObjectGenericMesh::MaterialInfo >& materials ) const
{
	rexResult retval( rexResult::PERFECT );

	int matcount = materials.GetCount();

	if ( matcount == 0 )
	{
		phmaterials->PushAndGrow( phMaterialMgr::DEFAULT_MATERIAL_ID );
	}

	for ( int i = 0; i < matcount; ++i )
	{
		phMaterialMgr::Id mat = phMaterialMgr::DEFAULT_MATERIAL_ID;
		NewPhysicsMaterial( mat, materials[ i ] );

		if ( mat )
		{
			phmaterials->PushAndGrow( mat, ( u16 ) matcount );
		}
		else
		{
			if ( phmaterials->GetCount() )
			{
				phmaterials->PushAndGrow( ( *phmaterials )[ 0 ], ( u16 ) matcount );
			}
			else
			{
				phmaterials->PushAndGrow( phMaterialMgr::DEFAULT_MATERIAL_ID, ( u16 ) matcount );
			}

			retval.Combine( rexResult::WARNINGS, rexResult::CONVERSION );
		}
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexSerializerRAGEPhysicsBound::NewPhysicsMaterial( phMaterialMgr::Id& mat, const rexObjectGenericMesh::MaterialInfo &matInfo ) const
{
	rexResult retval(rexResult::PERFECT);

	atString materialName;
	
	retval = GetPhysicsMaterialName(matInfo, materialName);
	if(retval.Errors())
		return retval;

	if(materialName.length())
	{
		phMaterial* material = &phMaterialMgr::GetInstance().AllocateMaterial((const char*)materialName);
		if(material)
		{ 
			mat = MATERIALMGR.GetMaterialId(*material);
		}
		else
			retval.Set(rexResult::ERRORS, rexResult::CANNOT_CREATE_OBJECT);
	}

	return retval;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// End filtered bounds code
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// Helper functions
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult GetPhysicsMaterialName( const rexObjectGenericMesh::MaterialInfo &matInfo, atString& retName )
{
	rexResult retval(rexResult::PERFECT);

	std::string materialName;

	// Do the rageShaderMaterial implementation
#if USE_MTL_SYSTEM
	if (matInfo.m_Material)
	{
		for (int i=0; i<matInfo.m_Material->GetParamCount(); i++)
		{
			shaderMaterialGeoParamValue * p = matInfo.m_Material->GetParam(i);
			if (p->GetType() == grcEffect::VT_STRING)
			{
				if (stricmp(p->GetName(), "PhysicsMaterial")==0)
				{
					materialName = ((shaderMaterialGeoParamValueString *)p)->GetValue();

					//We need to append additional material name flags that have been applied due to set-bound tagging
					std::string origMtlName = (const char*)matInfo.m_Name;
					size_t firstCommaIdx = origMtlName.find_first_of(",");
					if(firstCommaIdx != std::string::npos)
					{
						std::string updatedMaterialName = materialName;
						updatedMaterialName += origMtlName.substr(firstCommaIdx);
						materialName = updatedMaterialName.c_str();
					}
				}
			}
		}
	}
	else
#endif
	{
		// first get all values out of array
		// TODO: optimize this...  wil
		int a, attrCount = matInfo.m_AttributeNames.GetCount();
		for(a = 0; a < attrCount; a++)
		{
			if( 0 == strcmp((const char*)matInfo.m_AttributeNames[a], "materialName") )
			{
				materialName = matInfo.m_AttributeValues[a].toString();
			}
		}

		// [ERWINVIENNA] if the physical-material name was set (via an extra attribute in the maya-shader), use that one instead
		if (materialName.size() == 0)
		{
			if (strcmp(matInfo.m_PhysicsMaterialName, "rexObjectGenericMesh::MaterialInfo.m_PhysicsMaterialName Not set") != 0)
			{
				materialName = (const char*)matInfo.m_PhysicsMaterialName;
			}
		}
	}

	if (materialName.size() == 0) 
	{
		const char *longName = matInfo.m_Name;
		// Favor new-sk00l namespaces over old-skool underscore hacks:
		const char *separator = strrchr(longName,':');
		if (!separator)
			separator = strrchr(longName,'_');
		if (separator)
			longName = separator + 1;
		materialName = longName;
	}

	retName = materialName.c_str();

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int GetVertexCount( const rexObjectGenericBound* bound )
{
	switch ( bound->m_Type )
	{
	case phBound::GEOMETRY:
		return bound->m_Vertices.GetCount();
	case phBound::BOX:
		return 4;
	case phBound::SPHERE:
		return 1;
	case phBound::CAPSULE:
		return 2;
	case phBound::CYLINDER:
		return 2;
	case phBound::INVALID_TYPE:
	case phBound::NUM_BOUND_TYPES:
	case phBound::TRIANGLE:
	case phBound::COMPOSITE:
	case phBound::BVH:
	case phBound::DISC:
		return 0;

#if USE_SURFACES
	case phBound::SURFACE: return 0;
#else
	case phBound::REMOVED_SURFACE: FastAssert(0); return 0;
#endif

#if USE_RIBBONS
	case phBound::RIBBON: return 0;
#else
	case phBound::REMOVED_RIBBON: FastAssert(0); return 0;
#endif

#if USE_GRIDS
	case phBound::GRID: return 0;
#else
	case phBound::REMOVED_GRID: FastAssert(0); return 0;
#endif

#if USE_GEOMETRY_CURVED
	case phBound::GEOMETRY_CURVED: return 0;
#else
	case phBound::REMOVED_GEOMETRY_CURVED: FastAssert(0); return 0;
#endif

#if USE_TAPERED_CAPSULE
	case phBound::TAPERED_CAPSULE: return 0;
#else
	case phBound::REMOVED_TAPERED_CAPSULE: FastAssert(0); return 0;
#endif

	case phBound::REMOVED_PROBE: FastAssert(0); return 0;
	}

	return 0;
}

static int GetPrimitiveCount( const rexObjectGenericBound* bound )
{
	if ( bound->m_Type == phBound::GEOMETRY )
		return bound->m_Primitives.GetCount();
	if ( bound->m_Type == phBound::SPHERE ||
		bound->m_Type == phBound::BOX ||
		bound->m_Type == phBound::CAPSULE ||
		bound->m_Type == phBound::CYLINDER )
	{
		return 1;
	}

	return 0;
}

int FindMaterial( const rexObjectGenericMesh::MaterialInfo& mat, const atArray< phMaterialMgr::Id >& /*phmaterials*/, const atArray< rexObjectGenericMesh::MaterialInfo >& materials )
{
	atString mtlName0;
	GetPhysicsMaterialName(mat, mtlName0);

	for ( int i = 0; i < materials.GetCount(); ++i )
	{
		atString mtlName1;
		GetPhysicsMaterialName(materials[ i ], mtlName1);

		if ( mtlName0 ==  mtlName1 )
			return i;
	}

	return 0;
}

static void AddPolygonAsTriangles( phBoundBVH*& bnd, int* physPolyCount, rexResult* retval, const rexObjectGenericBound* inputBound, const rexObjectGenericMesh::PrimitiveInfo &prim, const int startindex, const atArray< phMaterialMgr::Id >& phmaterials, const atArray< rexObjectGenericMesh::MaterialInfo >& materials )
{
	u32 polyStartVert = (u32)prim.m_Adjuncts[0].m_VertexIndex;
	u32 i0, i1;

	int matIndex = prim.m_MaterialIndex;
	const rexObjectGenericMesh::MaterialInfo& mat = inputBound->m_Materials[ matIndex ];
	matIndex = FindMaterial( mat, phmaterials, materials );

	int polyVertCount = prim.m_Adjuncts.GetCount();

	int polyTriCount = polyVertCount - 2;
	for(int j = 1; j <= polyTriCount; j++)
	{
		i0 = (u32)prim.m_Adjuncts[j].m_VertexIndex;
		i1 = (u32)prim.m_Adjuncts[j+1].m_VertexIndex;

		Vector3 polyNormal;
		Vector3 polyCenter;
		polyCenter.Add( inputBound->m_Vertices[ i0 ].m_Position, inputBound->m_Vertices[ i1 ].m_Position );
		polyCenter.Add( inputBound->m_Vertices[ polyStartVert ].m_Position );
		polyCenter.Scale( 1.0f / 3.0f );
		GetTriNormal( inputBound->m_Vertices[ i0 ].m_Position, polyCenter, inputBound->m_Vertices[ i1 ].m_Position, polyNormal );
		float dotProduct = polyNormal.Dot( prim.m_FaceNormal );

		phPolygon poly;

		if( dotProduct < 0.0f )
			bnd->InitPolygonTri(poly, phPolygon::Index(polyStartVert + startindex), phPolygon::Index(i1 + startindex), phPolygon::Index(i0 + startindex) );
		else
			bnd->InitPolygonTri(poly, phPolygon::Index(i0 + startindex), phPolygon::Index(i1 + startindex), phPolygon::Index(polyStartVert + startindex) );

#if POLY_MAX_VERTICES == 4
		poly.SetMaterialIndex((u8) matIndex );
		Vector3 vecTemp = VEC3V_TO_VECTOR3(poly.GetUnitNormal());//, bnd->GetVertexPointer());
#else
		bnd->SetPolygonMaterialIndex(*physPolyCount, matIndex);
		Vector3 vecTemp = polyNormal;//, bnd->GetVertexPointer());
#endif // POLY_MAX_VERTICES == 4
		if(vecTemp.Mag2() < 0.9f)
		{
			// TODO: write a warning to log (polygon [index] has non-unit normal, determine type of poly by using IsTriangle()) - wil
			retval->Set(rexResult::WARNINGS, rexResult::CONVERSION);
		}
		else
			bnd->SetPolygon( ( *physPolyCount )++, poly);
	}
}
#if POLY_MAX_VERTICES == 4
static void AddPolygonAsQuads( phBoundBVH*& bnd, int* physPolyCount, rexResult* retval, const rexObjectGenericBound* inputBound, const rexObjectGenericMesh::PrimitiveInfo &prim, const int startindex, const atArray< phMaterialMgr::Id >& phmaterials, const atArray< rexObjectGenericMesh::MaterialInfo>& materials )
{
	u32 polyStartVert = (u32)prim.m_Adjuncts[0].m_VertexIndex;
	u32 i0, i1, i2, i3;

	int matIndex = prim.m_MaterialIndex;
	const rexObjectGenericMesh::MaterialInfo& mat = inputBound->m_Materials[ matIndex ];
	matIndex = FindMaterial( mat, phmaterials, materials );

	// the last index for a quad CANNOT be zero (that's how we
	// determine if a poly is a tri or a quad) so we have to
	// rotate the vertices.
	if(0 == polyStartVert)
	{
		i0 = polyStartVert;
		i1 = (u32)prim.m_Adjuncts[1].m_VertexIndex;
		i2 = (u32)prim.m_Adjuncts[2].m_VertexIndex;
		i3 = (u32)prim.m_Adjuncts[3].m_VertexIndex;
	}
	else
	{
		i0 = (u32)prim.m_Adjuncts[1].m_VertexIndex;
		i1 = (u32)prim.m_Adjuncts[2].m_VertexIndex;
		i2 = (u32)prim.m_Adjuncts[3].m_VertexIndex;
		i3 = polyStartVert;
	}

	Vector3 polyNormal;
	Vector3 polyCenter;
	polyCenter.Add( inputBound->m_Vertices[ i0 ].m_Position, inputBound->m_Vertices[ i1 ].m_Position );
	polyCenter.Add( inputBound->m_Vertices[ i2 ].m_Position );
	polyCenter.Add( inputBound->m_Vertices[ i3 ].m_Position );
	polyCenter.Scale( 0.25f );
	GetTriNormal( inputBound->m_Vertices[ i0 ].m_Position, polyCenter, inputBound->m_Vertices[ i1 ].m_Position, polyNormal );
	float dotProduct = polyNormal.Dot( prim.m_FaceNormal );

	phPolygon poly;

	if( dotProduct < 0.0f )
		bnd->InitPolygonQuad( poly, i2 + startindex, i1 + startindex, i0 + startindex, i3 + startindex );
	else
		bnd->InitPolygonQuad( poly, i0 + startindex, i1 + startindex, i2 + startindex, i3 + startindex );

	poly.SetMaterialIndex((u8) matIndex );

	Vector3 vecTemp = VEC3V_TO_VECTOR3(poly.GetUnitNormal());//, bnd->GetVertexPointer());
	if(vecTemp.Mag2() < 0.9f)
		//				if(poly.GetNormal().Mag2() < 0.9f)
	{
		// TODO: write a warning to log (polygon [index] has non-unit normal, determine type of poly by using IsTriangle()) - wil
		retval->Set(rexResult::WARNINGS, rexResult::CONVERSION);
	}
	else
	{
		bnd->SetPolygon( ( *physPolyCount )++, poly);
	}
}
#endif // POLY_MAX_VERTICES == 4
static void AddPrimitives( 
						  phBoundBVH* bnd, int* physPolyCount, rexResult* retval, int* curridx, const rexObjectGenericBound*& bound, const atArray< phMaterialMgr::Id >& phmaterials, const atArray< rexObjectGenericMesh::MaterialInfo>& materials, const float conversionFactor )
{
	int startindex = *curridx;

	// set vertices
	int vertcount = bound->m_Vertices.GetCount();
	for(int i = 0; i < vertcount; i++)
	{
		Vector3 v;
		bound->m_Matrix.Transform( bound->m_Vertices[i].m_Position, v );
		bound->m_ParentBoneMatrix.UnTransform( v );

		v.Scale(conversionFactor);
		bnd->SetVertex( *curridx, RCC_VEC3V(v));
		++( *curridx );
	}

	// note: i think the current index may not be able to be used here...
	int polycount = bound->m_Primitives.GetCount();
	for ( int i = 0; i < polycount; ++i )
	{
		const rexObjectGenericMesh::PrimitiveInfo &prim = bound->m_Primitives[ i ];

		if( prim.m_Adjuncts.GetCount() )
		{
			if ( !bound->primitiveIsQuad( i ) )
			{
				// convert to triangles
				AddPolygonAsTriangles( bnd, physPolyCount, retval, bound, prim, startindex, phmaterials, materials );
			}
			else
			{
#if POLY_MAX_VERTICES == 3
				Assert( 0 && "Attempted quad export when POLY_MAX_VERTICES == 3." );
#else
				// quads
				AddPolygonAsQuads( bnd, physPolyCount, retval, bound, prim, startindex, phmaterials, materials );
#endif // POLY_MAX_VERTICES == 4
			}
		}
		else // no adjuncts
		{
			Assert( 0 && "Poly with no adjuncts!" );
		}
	}
}

static void AddBox( phBoundGeometry* bnd, int* curridx, const rexObjectGenericBound* bound, int* physPolyCount, const atArray< phMaterialMgr::Id >& phmaterials, const atArray< rexObjectGenericMesh::MaterialInfo>& materials, const float conversionFactor )
{
	int index = *curridx;

	// add the 4 vertices
	float halfHeight = bound->m_Height * 0.5f * conversionFactor;
	float halfWidth = bound->m_Width * 0.5f * conversionFactor;
	float halfDepth = bound->m_Depth * 0.5f * conversionFactor;

	Vector3 p0( halfWidth,  halfHeight, -halfDepth);
	bound->m_Matrix.Transform(p0);
	bnd->SetVertex( *curridx, RCC_VEC3V(p0) );
	++( *curridx );

	Vector3 p1(-halfWidth,  halfHeight,  halfDepth);
	bound->m_Matrix.Transform(p1);
	bnd->SetVertex( *curridx, RCC_VEC3V(p1) );
	++( *curridx );

	Vector3 p2( halfWidth, -halfHeight,  halfDepth);
	bound->m_Matrix.Transform(p2);
	bnd->SetVertex( *curridx, RCC_VEC3V(p2) );
	++( *curridx );

	Vector3 p3(-halfWidth, -halfHeight, -halfDepth);
	bound->m_Matrix.Transform(p3);
	bnd->SetVertex( *curridx, RCC_VEC3V(p3) );
	++( *curridx );

	phPolygon poly;
	bnd->InitBox( poly, phPolygon::Index(index), phPolygon::Index(index + 1), phPolygon::Index(index + 2), phPolygon::Index(index + 3) );

	int matIndex = 0;
	if ( bound->m_Materials.GetCount() )
	{
		const rexObjectGenericMesh::MaterialInfo& mat = bound->m_Materials[ 0 ];
		matIndex = FindMaterial( mat, phmaterials, materials );
	}
#if POLY_MAX_VERTICES == 4
	poly.SetMaterialIndex( (u8) matIndex );
#else
	bnd->SetPolygonMaterialIndex(*physPolyCount, matIndex);
#endif // POLY_MAX_VERTICES == 4POLY_MAX_VERTICES == 4
	bnd->SetPolygon( ( *physPolyCount )++, poly);
}

static void AddCapsule( phBoundGeometry* bnd, int* curridx, const rexObjectGenericBound* bound, int* physPolyCount, const atArray< phMaterialMgr::Id >& phmaterials, const atArray< rexObjectGenericMesh::MaterialInfo>& materials, const float conversionFactor )
{
	int index = *curridx;

	// add the 2 vertices
	Vector3 yaxis = bound->m_Matrix.b;
	yaxis.Normalize();
	yaxis.Scale( conversionFactor );

	float radius = bound->m_Radius;
	float halfheight = bound->m_Height / 2;
	float height = halfheight;

	Vector3 p0 = bound->m_Matrix.d + ( yaxis * height );
	bnd->SetVertex( *curridx, RCC_VEC3V(p0) );
	++( *curridx );

	Vector3 p1 = bound->m_Matrix.d - ( yaxis * height );
	bnd->SetVertex( *curridx, RCC_VEC3V(p1) );
	++( *curridx );

	phPolygon poly;
	bnd->InitCapsule( poly, phPolygon::Index(index), phPolygon::Index(index + 1), radius );

	int matIndex = 0;
	if ( bound->m_Materials.GetCount() )
	{
		const rexObjectGenericMesh::MaterialInfo& mat = bound->m_Materials[ 0 ];
		matIndex = FindMaterial( mat, phmaterials, materials );
	}

#if POLY_MAX_VERTICES == 4
	poly.SetMaterialIndex( (u8) matIndex );
#else
	bnd->SetPolygonMaterialIndex(*physPolyCount, matIndex);
#endif // POLY_MAX_VERTICES == 4POLY_MAX_VERTICES == 4

	bnd->SetPolygon( ( *physPolyCount )++, poly);
}

static void AddSphere( phBoundGeometry* bnd, int* curridx, const rexObjectGenericBound* bound, int* physPolyCount, const atArray< phMaterialMgr::Id >& phmaterials, const atArray< rexObjectGenericMesh::MaterialInfo>& materials, const float conversionFactor )
{
	int index = *curridx;

	// add the vertex
	Vector3 center = bound->m_Matrix.d;
	center.Scale( conversionFactor );
	bnd->SetVertex( *curridx, RCC_VEC3V(center) );
	++( *curridx );

	phPolygon poly;
	bnd->InitSphere( poly, phPolygon::Index(index), bound->m_Radius );

	int matIndex = 0;
	if ( bound->m_Materials.GetCount() )
	{
		const rexObjectGenericMesh::MaterialInfo& mat = bound->m_Materials[ 0 ];
		matIndex = FindMaterial( mat, phmaterials, materials );
	}

#if POLY_MAX_VERTICES == 4
	poly.SetMaterialIndex( (u8) matIndex );
#else
	bnd->SetPolygonMaterialIndex(*physPolyCount, matIndex);
#endif // POLY_MAX_VERTICES == 4POLY_MAX_VERTICES == 4

	bnd->SetPolygon( ( *physPolyCount )++, poly);
}

static void AddCylinder( phBoundGeometry* bnd, int* curridx, const rexObjectGenericBound* bound, int* physPolyCount, const atArray< phMaterialMgr::Id >& phmaterials, const atArray< rexObjectGenericMesh::MaterialInfo>& materials, const float conversionFactor )
{
	int index = *curridx;
	
	// add the 2 vertices
	Vector3 yaxis = bound->m_Matrix.b;
	yaxis.Normalize();
	yaxis.Scale( conversionFactor );

	float halfheight = bound->m_Height / 2;
	float height = halfheight;

	Vector3 p0 = bound->m_Matrix.d + ( yaxis * height );
	bnd->SetVertex( *curridx, RCC_VEC3V(p0) );
	++( *curridx );

	Vector3 p1 = bound->m_Matrix.d - ( yaxis * height );
	bnd->SetVertex( *curridx, RCC_VEC3V(p1) );
	++( *curridx );

	phPolygon poly;
	bnd->InitCylinder(poly, phPolygon::Index(index), phPolygon::Index(index + 1), bound->m_Radius );

	int matIndex = 0;
	if ( bound->m_Materials.GetCount() )
	{
		const rexObjectGenericMesh::MaterialInfo& mat = bound->m_Materials[ 0 ];
		matIndex = FindMaterial( mat, phmaterials, materials );
	}

#if POLY_MAX_VERTICES == 4
	poly.SetMaterialIndex( (u8) matIndex );
#else
	bnd->SetPolygonMaterialIndex(*physPolyCount, matIndex);
#endif // POLY_MAX_VERTICES == 4POLY_MAX_VERTICES == 4

	bnd->SetPolygon( ( *physPolyCount )++, poly);

}

void rexSerializerRAGEPhysicsBound::SetBoundCheckFlags(atString boundCheckString)
{
	atArray<atString> flagTuples;
	boundCheckString.Split(flagTuples, "|");
	atArray<atString>::iterator flagIt = flagTuples.begin();
	for(;flagIt != flagTuples.end();flagIt++)
	{
		atArray<atString> keyValPair;
		(*flagIt).Split(keyValPair, ":");
		ScalarV val(float(atof(keyValPair[1].c_str())));
		m_BoundCheckTolerances.Insert(keyValPair[0], val);
	}
}

rexResult rexSerializerRAGEPhysicsBound::ValidatePhBound( phBound * pBnd, const char *name) const
{
	rexResult retVal(rexResult::PERFECT);
	phBoundBVH *pBndBvh = dynamic_cast<phBoundBVH *>(pBnd);
	if(!pBndBvh || !pBnd->IsPolygonal())
		return retVal;

	const ScalarV *errorParam = m_BoundCheckTolerances.Access("BOUNDCHECK_ERROR");
	bool throwError = errorParam && NULL != errorParam->Geti();

	pBndBvh->ComputeNeighbors(NULL);

	bool hasInvalidPolys = false;
	atArray<phPolygon*> thinPolys, badNormals, badNeighbors;
	for(int polyIndex=0 ; polyIndex<pBndBvh->GetNumPolygons() ; polyIndex++)
	{
		phPrimitive &bndPrim = pBndBvh->GetPrimitive(polyIndex);
		const char *pTypeName = bndPrim.GetTypeName();
		if(pTypeName)
			pTypeName = NULL;
		if(bndPrim.GetType()!=PRIM_TYPE_POLYGON)
			continue;
		phPolygon &bndPoly = bndPrim.GetPolygon();

		// valid polygon?
		if(	bndPoly.GetVertexIndex(0)>=pBndBvh->GetNumVertices() || bndPoly.GetVertexIndex(0)<0 ||
			bndPoly.GetVertexIndex(1)>=pBndBvh->GetNumVertices() || bndPoly.GetVertexIndex(1)<0 ||
			bndPoly.GetVertexIndex(2)>=pBndBvh->GetNumVertices() || bndPoly.GetVertexIndex(2)<0 )
		{
			hasInvalidPolys = true;
			continue;
		}

		const ScalarV *tolerance = m_BoundCheckTolerances.Access("BOUNDCHECK_ISTHIN");
		if(	tolerance && bndPoly.IsThin(pBndBvh, ScalarV(sqrt(tolerance->Getf()))) ||
			!tolerance && bndPoly.IsThin(pBndBvh))
		{
			thinPolys.PushAndGrow(&bndPoly);
		}

		tolerance = m_BoundCheckTolerances.Access("BOUNDCHECK_BADNORMAL");
		if(	tolerance && bndPoly.HasBadNormal(pBndBvh, *tolerance) ||
			!tolerance && bndPoly.HasBadNormal(pBndBvh))
		{
			badNormals.PushAndGrow(&bndPoly);
		}
		tolerance = m_BoundCheckTolerances.Access("BOUNDCHECK_BADNEIGHBOR");
		if(	pBndBvh->GetNumPolygons()>1 &&
			(!tolerance || NULL!=tolerance->Geti()) && 
			bndPoly.HasBadNeighbors(pBndBvh))
		{
			badNeighbors.PushAndGrow(&bndPoly);
		}
	}
	if(hasInvalidPolys)
	{
		const char *msg = "phBound %s has invalid polygon indeces.";
		if(throwError)
			retVal.AddErrorCtx(name, msg, name);
		else
			retVal.AddWarningCtx(name, msg, name);
		// have to stop here to not try to access those in later error messages
		return retVal;
	}
	atArray<phPolygon*>::iterator polyIt;
	if(thinPolys.GetCount()>0)
	{
		atString msg("phBound %s has polygons with dimensions that are off the valid aspect ratio:\n");
		for(polyIt=thinPolys.begin();polyIt!=thinPolys.end();polyIt++)
		{
			char buffer[265];
			phPolygon &poly = **polyIt;
			Vec3V vertex[] = {
				pBndBvh->GetVertex(poly.GetVertexIndex(0)),
				pBndBvh->GetVertex(poly.GetVertexIndex(1)),
				pBndBvh->GetVertex(poly.GetVertexIndex(2))
			};
			sprintf(buffer, "([%3.2f,%3.2f,%3.2f] | [%3.2f,%3.2f,%3.2f] | [%3.2f,%3.2f,%3.2f])\n",
				vertex[0].GetXf(),vertex[0].GetYf(),vertex[0].GetZf(),
				vertex[1].GetXf(),vertex[1].GetYf(),vertex[1].GetZf(),
				vertex[2].GetXf(),vertex[2].GetYf(),vertex[2].GetZf());
			msg += buffer;
			if(msg.GetLength()>512)
			{
				msg += "...";
				break;
			}
		}
		if(throwError)
			retVal.AddErrorCtx(name, msg.c_str(), name);
		else
			retVal.AddWarningCtx(name, msg.c_str(), name);
	}
	if(badNormals.GetCount()>0)
	{
		atString msg("phBound %s has invalid normals on some polygons");
		for(polyIt=badNormals.begin();polyIt!=badNormals.end();polyIt++)
		{
			char buffer[265];
			phPolygon &poly = **polyIt;
			Vec3V vertex[] = {
				pBndBvh->GetVertex(poly.GetVertexIndex(0)),
				pBndBvh->GetVertex(poly.GetVertexIndex(1)),
				pBndBvh->GetVertex(poly.GetVertexIndex(2))
			};
			sprintf(buffer, "([%3.2f,%3.2f,%3.2f] | [%3.2f,%3.2f,%3.2f] | [%3.2f,%3.2f,%3.2f])\n",
				vertex[0].GetXf(),vertex[0].GetYf(),vertex[0].GetZf(),
				vertex[1].GetXf(),vertex[1].GetYf(),vertex[1].GetZf(),
				vertex[2].GetXf(),vertex[2].GetYf(),vertex[2].GetZf());
			msg += buffer;
			if(msg.GetLength()>512)
			{
				msg += "...";
				break;
			}
		}
		if(throwError)
			retVal.AddErrorCtx(name, msg.c_str(), name);
		else
			retVal.AddWarningCtx(name, msg.c_str(), name);
	}
	if(badNeighbors.GetCount()>0)
	{
		atString msg("phBound %s has polygons with problematic neighbors, check angle between polys:\n");
		for(polyIt=badNeighbors.begin();polyIt!=badNeighbors.end();polyIt++)
		{
			char buffer[265];
			phPolygon &poly = **polyIt;
			Vec3V vertex[] = {
				pBndBvh->GetVertex(poly.GetVertexIndex(0)),
				pBndBvh->GetVertex(poly.GetVertexIndex(1)),
				pBndBvh->GetVertex(poly.GetVertexIndex(2))
			};
			sprintf(buffer, "([%3.2f,%3.2f,%3.2f] | [%3.2f,%3.2f,%3.2f] | [%3.2f,%3.2f,%3.2f])\n",
				vertex[0].GetXf(),vertex[0].GetYf(),vertex[0].GetZf(),
				vertex[1].GetXf(),vertex[1].GetYf(),vertex[1].GetZf(),
				vertex[2].GetXf(),vertex[2].GetYf(),vertex[2].GetZf());
			msg += buffer;
			if(msg.GetLength()>512)
			{
				msg += "...";
				break;
			}
		}
		if(throwError)
			retVal.AddErrorCtx(name, msg.c_str(), name);
		else
			retVal.AddWarningCtx(name, msg.c_str(), name);
	}

	return retVal;
}

//////////////////////////////////////////////////////////////////////////
// End helper functions
//////////////////////////////////////////////////////////////////////////
// generic to phbound
//////////////////////////////////////////////////////////////////////////

static bool IsBvhWithPrimitives( const rexObjectGenericBound* inputBound )
{
	phBound::BoundType type = inputBound->m_Type;

	int count = inputBound->m_ContainedObjects.GetCount();
	for ( int i = 0; i < count; ++i )
	{
		const rexObjectGenericBound* bound = 
			dynamic_cast< const rexObjectGenericBound* >( inputBound->m_ContainedObjects[ i ] );

		if ( type == phBound::GEOMETRY && bound && ( 
				bound->m_Type == phBound::BOX || 
				bound->m_Type == phBound::SPHERE || 
				bound->m_Type == phBound::CAPSULE 
				USE_TAPERED_CAPSULE_ONLY(|| bound->m_Type == phBound::TAPERED_CAPSULE) ) )
		{
			return true;
		}

		if ( bound && bound->m_Type == phBound::GEOMETRY && ( 
			type == phBound::BOX || 
			type == phBound::SPHERE || 
			type == phBound::CAPSULE 
			USE_TAPERED_CAPSULE_ONLY(|| type == phBound::TAPERED_CAPSULE) ) )
		{
			return true;
		}
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBound( phBound *&bnd, const rexObjectGenericBound *inputBound, bool ignoreChildren, bool firstCall, bool hasParent ) const
{
	rexResult retval(rexResult::PERFECT);

	bnd = NULL;
	if( GetSplitOnMaterialLimit() && firstCall  && inputBound->m_Type == phBound::GEOMETRY )
	{
		atArray<phMaterialMgr::Id> materials;
		atArray<int> materialMap;
		retval = rexSerializerRAGEPhysicsBound::CreatePhysicsMaterials(materials, materialMap, inputBound);

		if(retval.Errors())
			return retval;

		phBoundComposite* bndComposite = NULL;
		if( materials.GetCount() > 256 )
		{
			atArray<rexObjectGenericBound*> newboundlist;
			SplitBound( inputBound, newboundlist );
			
			if( newboundlist.GetCount() < 2 )
			{
				retval.Set( rexResult::ERRORS, rexResult::TYPE_INCOMPATIBLE );
				retval.AddError("The bound '%s' has more than 256 materials but failed to be split up.", (const char*)inputBound->GetMeshName());
				for(int i=0; i<newboundlist.GetCount(); i++)
				{
					delete newboundlist[i];
				}
				return retval;
			}

			for( int i=1; i<newboundlist.GetCount(); i++ )
			{
				newboundlist[0]->m_ContainedObjects.PushAndGrow( newboundlist[i] );
			}
			newboundlist[0]->SetMeshName(inputBound->GetMeshName());
			rexObjectGenericBound* tempBound = newboundlist[0];

			materials.Reset();
			materialMap.Reset();
			retval = rexSerializerRAGEPhysicsBound::CreatePhysicsMaterials(materials, materialMap, inputBound);
			if(retval.Errors())
			{
				delete tempBound;
				return retval;
			}

			retval = GenericBoundToPhBoundComposite(bndComposite, tempBound, materials, materialMap);
			// Clean up our turd
			delete tempBound;
		}
		else
		{
			retval = GenericBoundToPhBoundComposite(bndComposite, inputBound, materials, materialMap);
		}
		bnd = bndComposite;
	}
	else if( (inputBound->m_ContainedObjects.GetCount() && !ignoreChildren) || (m_ForceCompositeBounds &&  firstCall ) )
	{
			atArray<phMaterialMgr::Id> materials;
			atArray<int> materialMap;
			retval = rexSerializerRAGEPhysicsBound::CreatePhysicsMaterials(materials, materialMap, inputBound);

			if(retval.Errors())
				return retval;

			phBoundComposite* bndComposite = NULL;
			retval = GenericBoundToPhBoundComposite(bndComposite, inputBound, materials, materialMap);
			bnd = bndComposite;
	}
	else
	{
		atArray<phMaterialMgr::Id> materials;
		atArray<int> materialMap;
		retval = rexSerializerRAGEPhysicsBound::CreatePhysicsMaterials(materials, materialMap, inputBound);

		if(retval.Errors())
			return retval;

		if(GetPrimitiveBoundExportPrimitivesInBvh() && hasParent)
		{
			// LPXO: TODO: This is repeated code.  Factor out
			switch(inputBound->m_Type)
			{
				case(phBound::GEOMETRY):
				{
					retval = GenericBoundToGeometry( bnd, inputBound, materials, materialMap );
					break;
				}
				default:
					retval.Set( rexResult::ERRORS, rexResult::TYPE_INCOMPATIBLE );
					break;
			}
		}
		else
		{
			switch(inputBound->m_Type)
			{
				case(phBound::BOX):
				{
					
					if(GetPrimitiveBoundExportPrimitivesInBvh() && !hasParent)
					{
						phBoundGeometry* bigGeomBound = NULL;
						retval = GenericBoundToPhBoundGeometryWithPrims( bigGeomBound, inputBound );
						phBoundBVH* bvhbnd = new phBoundBVH;
						bvhbnd->Copy( bigGeomBound );	
						bvhbnd->Build();
						delete bigGeomBound;
						bnd = bvhbnd;
					}
					else
					{
						phBoundBox *bndBox = NULL;
						retval = GenericBoundToPhBoundBox(bndBox, inputBound, materials, materialMap);
						bnd = bndBox;
					}
					
					break;
				}
				case(phBound::SPHERE):
				{
					if(GetPrimitiveBoundExportPrimitivesInBvh() && !hasParent)
					{
						phBoundGeometry* bigGeomBound = NULL;
						retval = GenericBoundToPhBoundGeometryWithPrims( bigGeomBound, inputBound );
						phBoundBVH* bvhbnd = new phBoundBVH;
						bvhbnd->Copy( bigGeomBound );	
						bvhbnd->Build();
						delete bigGeomBound;
						bnd = bvhbnd;
					}
					else
					{
						phBoundSphere *bndSphere = NULL;
						retval = GenericBoundToPhBoundSphere(bndSphere, inputBound, materials, materialMap);
						bnd = bndSphere;
					}
					break;
				}
				case(phBound::CAPSULE):
				{
					if(GetPrimitiveBoundExportPrimitivesInBvh() && !hasParent)
					{
						phBoundGeometry* bigGeomBound = NULL;
						retval = GenericBoundToPhBoundGeometryWithPrims( bigGeomBound, inputBound );
						phBoundBVH* bvhbnd = new phBoundBVH;
						bvhbnd->Copy( bigGeomBound );	
						bvhbnd->Build();
						delete bigGeomBound;
						bnd = bvhbnd;
					}
					else
					{
						phBoundCapsule *bndCapsule = NULL;
						retval = GenericBoundToPhBoundCapsule(bndCapsule, inputBound, materials, materialMap);
						bnd = bndCapsule;
					}
					break;
				}
				case(phBound::GEOMETRY):
				{
					retval = GenericBoundToGeometry( bnd, inputBound, materials, materialMap );
					break;
				}
				case(phBound::CYLINDER):
				{
					if(GetPrimitiveBoundExportPrimitivesInBvh() && !hasParent)
					{
						phBoundGeometry* bigGeomBound = NULL;
						retval = GenericBoundToPhBoundGeometryWithPrims( bigGeomBound, inputBound );
						phBoundBVH* bvhbnd = new phBoundBVH;
						bvhbnd->Copy( bigGeomBound );	
						bvhbnd->Build();
						delete bigGeomBound;
						bnd = bvhbnd;
					}
					else
					{
						phBoundCylinder *bndCylinder = NULL;
						retval = GenericBoundToPhBoundCylinder(bndCylinder, inputBound, materials, materialMap);
						bnd = bndCylinder;
						
					}
					break;
				}
#if USE_GEOMETRY_CURVED
				case(phBound::GEOMETRY_CURVED):
				{
					phBoundCurvedGeometry *bndCurvedGeometry = NULL;
					retval = GenericBoundToPhBoundCurvedGeometry(bndCurvedGeometry, inputBound, materials, materialMap);
					bnd = bndCurvedGeometry;
					break;
				}
#endif
				case(phBound::DISC):
				{
					phBoundDisc *bndDisc = NULL;
					retval = GenericBoundToPhBoundDisc(bndDisc, inputBound, materials, materialMap);
					bnd = bndDisc;
					break;
				}
				default:
				retval.Set( rexResult::ERRORS, rexResult::TYPE_INCOMPATIBLE );
				break;
			}
		}
	}

	bnd->m_CompositeCollisionBoundFlag = inputBound->m_surfaceFlags;

	retval.Combine(ValidatePhBound(bnd, inputBound->GetMeshName().c_str()));

	if(retval.OK() && !bnd) // return value says things are OK, but no bound?  return generic error...
		retval.Set( rexResult::ERRORS, rexResult::NO_INFO );
	
	return retval;
}


rexResult rexSerializerRAGEPhysicsBound::GenericBoundToGeometry( phBound *&bnd, const rexObjectGenericBound *inputBound, atArray<phMaterialMgr::Id>& materials, atArray<int>& materialMap ) const
{
	rexResult retval(rexResult::PERFECT);

	if( m_ExportGeometryBoundsAsBvhGrid )
	{
#if USE_GRIDS
		phBoundGrid* grid = NULL;
		retval = GenericBoundToPhBoundGrid(grid, inputBound, materials, materialMap, true);
		bnd = grid;
#else
		Assert(0);
		retval.Set( rexResult::ERRORS, rexResult::CANNOT_CREATE_OBJECT );
		return retval;
#endif
	}
	else if( m_ExportGeometryBoundsAsBvhGeom )
	{
		phBoundBVH* bvh = NULL;
		retval = GenericBoundToPhBoundBvh(bvh, inputBound, materials, materialMap);
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
		bvh->sm_SecondSurfaceMaxHeight = GetSecondSurfaceMaxHeight(); // Not ideal having this here but at least it's isolated
#endif // HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
		bnd = bvh;
	}
	else
	{
		phBoundGeometry *bndGeometry = NULL;
		retval = GenericBoundToPhBoundGeometry(bndGeometry, inputBound, materials, materialMap);
		if ( m_CheckBoundVertexCount && (bndGeometry->GetNumVertices() > MAX_NUM_VERTS) )
		{
			retval.Set( rexResult::ERRORS, rexResult::TYPE_INCOMPATIBLE );
			retval.AddError("The bound '%s' is too complex (%d vertices), we have a limit of %d.", (const char*)inputBound->GetMeshName(), bndGeometry->GetNumVertices(), MAX_NUM_VERTS);
			return retval;
		}

		if (m_ApplyBulletShrinkMargin)
		{
			bndGeometry->SetMarginAndShrink(inputBound->m_Margin, 0.0f);
		}
		else
		{
			bndGeometry->SetMargin(inputBound->m_Margin);
		}

		
		bnd = bndGeometry;
	}
	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundBox( phBoundBox *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const 
{
	rexResult retval(rexResult::PERFECT);

	bnd = new phBoundBox();
	
	if(bnd)
	{
		float conversionFactor=GetUnitTypeLinearConversionFactor();
		Vector3 dimensions(inputBound->m_Width, inputBound->m_Height, inputBound->m_Depth);
		dimensions.Scale(conversionFactor);
		bnd->SetBoxSize(RCC_VEC3V(dimensions));

		if (m_ZeroPrimitiveCentroids)
		{
			bnd->SetCentroidOffset(Vec3V(V_ZERO));
		}
		else
		{
			Vector3 offset;
			inputBound->m_ParentBoneMatrix.UnTransform(inputBound->m_Matrix.d, offset);
			offset.Scale(conversionFactor);
			bnd->SetCentroidOffset(RCC_VEC3V(offset));
		}

		//Assign the material
		if(materialMap.GetCount())
		{
			//Only one material can be assigned for primitive
			bnd->SetMaterial(materials[materialMap[0]]);
		}
		else
		{
			// only use the last material
			bnd->SetMaterial(materials.GetCount()-1);
		}
	}
	else
		retval.Set(rexResult::WARNINGS, rexResult::CANNOT_CREATE_OBJECT);

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundSphere( phBoundSphere *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap )  const
{
	rexResult retval(rexResult::PERFECT);

	bnd = new phBoundSphere();

	if(bnd)
	{
		float conversionFactor=GetUnitTypeLinearConversionFactor();
		float radius=inputBound->m_Radius*conversionFactor;
		bnd->SetSphereRadius(radius);

		if (m_ZeroPrimitiveCentroids)
		{
			bnd->SetCentroidOffset(Vec3V(V_ZERO));
		}
		else
		{
			Vector3 offset;
			inputBound->m_ParentBoneMatrix.UnTransform(inputBound->m_Matrix.d, offset);
			offset.Scale(conversionFactor);
			bnd->SetCentroidOffset(RCC_VEC3V(offset));
		}
		
		//Assign the material
		if(materialMap.GetCount())
		{
			//Only one material can be assigned for primitive
			bnd->SetMaterial(materials[materialMap[0]]);
		}
		else
		{
			// only use the last material
			bnd->SetMaterial(materials.GetCount()-1);
		}
	}
	else
		retval.Set(rexResult::WARNINGS, rexResult::CANNOT_CREATE_OBJECT);

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundCapsule( phBoundCapsule *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap )  const
{
	rexResult retval(rexResult::PERFECT);

	bnd = new phBoundCapsule();

	if(bnd)
	{
		float conversionFactor=GetUnitTypeLinearConversionFactor();
		float radius=inputBound->m_Radius*conversionFactor;
		float height=inputBound->m_Height*conversionFactor;
		bnd->SetCapsuleSize(radius, height);

		if (m_ZeroPrimitiveCentroids)
		{
			bnd->SetCentroidOffset(Vec3V(V_ZERO));
		}
		else
		{
			Vector3 offset;
			inputBound->m_ParentBoneMatrix.UnTransform(inputBound->m_Matrix.d, offset);
			offset.Scale(conversionFactor);
			bnd->SetCentroidOffset(RCC_VEC3V(offset));
		}

		//Assign the material
		if(materialMap.GetCount())
		{
			//Only one material can be assigned for primitive
			bnd->SetMaterial(materials[materialMap[0]]);
		}
		else
		{
			// only use the last material
			bnd->SetMaterial(materials.GetCount()-1);
		}
	}
	else
		retval.Set(rexResult::WARNINGS, rexResult::CANNOT_CREATE_OBJECT);

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundCylinder( phBoundCylinder *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap )  const
{
	rexResult retval(rexResult::PERFECT);

	bnd = new phBoundCylinder();

	if(bnd)
	{
		float conversionFactor=GetUnitTypeLinearConversionFactor();
		float radius=inputBound->m_Radius*conversionFactor;
		float height=inputBound->m_Height*conversionFactor;
		bnd->SetCylinderRadiusAndHalfHeight(radius, height / 2.0f);

		if (m_ZeroPrimitiveCentroids)
		{
			bnd->SetCentroidOffset(Vec3V(V_ZERO));
		}
		else
		{
			Vector3 offset;
			inputBound->m_ParentBoneMatrix.UnTransform(inputBound->m_Matrix.d, offset);
			offset.Scale(conversionFactor);
			bnd->SetCentroidOffset(RCC_VEC3V(offset));
		}

		//Assign the material
		if(materialMap.GetCount())
		{
			//Only one material can be assigned for primitive
			bnd->SetMaterial(materials[materialMap[0]]);
		}
		else
		{
			// only use the last material
			bnd->SetMaterial(materials.GetCount()-1);
		}
	}
	else
		retval.Set(rexResult::WARNINGS, rexResult::CANNOT_CREATE_OBJECT);

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundPrimitiveBvh( phBoundBVH *& bnd, const rexObjectGenericBound *inputBound ) const
{
	rexResult retval(rexResult::PERFECT);

	//int objectCount = inputBound->m_ContainedObjects.GetCount();

	atArray< const rexObjectGenericBound* > bounds;
	atArray< const rexObject* > stack;
	stack.PushAndGrow( inputBound );
	while( stack.GetCount() != 0 )
	{
		const rexObject* elem = stack.Pop();
		const rexObjectGenericBound* bound = 
			dynamic_cast< const rexObjectGenericBound* >( elem );
		if ( bound )
			bounds.PushAndGrow( bound );

		for ( int i = 0; i < elem->m_ContainedObjects.GetCount(); ++i )
		{
			stack.PushAndGrow( elem->m_ContainedObjects[ i ] );
		}
	}

	int numverts = 0;//GetVertexCount( inputBound );
	for ( int i = 0; i < bounds.GetCount(); ++i )
	{
		numverts += GetVertexCount( bounds[ i ] );
	}

	int numpolys = 0; //GetPrimitiveCount( inputBound );
	for ( int i = 0; i < bounds.GetCount(); ++i )
	{
		
		numpolys += GetPrimitiveCount( bounds[ i ] );
	}

	float conversionFactor = GetUnitTypeLinearConversionFactor();

	atArray< rexObjectGenericMesh::MaterialInfo > materials;
	
	for ( int i = 0; i < inputBound->m_Materials.GetCount(); ++i )
	{
		materials.PushAndGrow( inputBound->m_Materials[ i ] );
	}

	// find all the materials
	for ( int i = 0; i < bounds.GetCount(); ++i )
	{
		for ( int j = 0; j < bounds[ i ]->m_Materials.GetCount(); ++j )
		{
			bool found = false;

			atString mtlName0;
			GetPhysicsMaterialName(bounds[ i ]->m_Materials[ j ], mtlName0);

			for ( int k = 0; k < materials.GetCount(); ++k )
			{
				atString mtlName1;
				GetPhysicsMaterialName(materials[ k ], mtlName1);

				if( mtlName0 ==  mtlName1)
				{
					found = true;
					break;
				}
			}
	
			if ( !found )
				materials.PushAndGrow( bounds[ i ]->m_Materials[ j ] );
		}
	}
	
	int nummaterials = int ( materials.size() );

	// now we've got the damn materials create them
	atArray< phMaterialMgr::Id > phmaterials;
	CreatePhysicsMaterials( &phmaterials, materials );

	bnd = new phBoundBVH;

	const int numPerVertAttribs=0;
#if HACK_GTA4
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
	#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		bnd->Init(numverts, numPerVertAttribs, nummaterials, 0, numpolys, 1);
	#else	
		bnd->Init(numverts, numPerVertAttribs, nummaterials, numpolys, 1);
	#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
#else
	#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		bnd->Init(numverts, numPerVertAttribs, nummaterials, 0, numpolys);
	#else
		bnd->Init(numverts, numPerVertAttribs, nummaterials, numpolys);
	#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
#endif // HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
#else
	bnd->Init(numverts, numPerVertAttribs, nummaterials, numpolys);
#endif // HACK_GTA4

	// zero vertices to <0,0,0>, so that NAN exceptions aren't raised in Fourtified functions
	Vector3 zeroVector(0.0f,0.0f,0.0f);
	for(int i = numverts; i < Fourtify(numverts); i++)
		bnd->SetVertex(i, RCC_VEC3V(zeroVector));

	// set materials
	for(int i = 0; i < nummaterials; i++)
	{
		phMaterialMgr::Id mtl = phmaterials[i];

		bnd->SetMaterialId(i, mtl);
	}

	int physPolyCount = 0;
	int curridx = 0;

	for ( int i = 0; i < bounds.GetCount(); ++i )
	{
		if ( bounds[ i ]->m_Type == phBound::GEOMETRY )
		{
			AddPrimitives( bnd, &physPolyCount, &retval, &curridx, bounds[ i ], phmaterials, materials, conversionFactor );
		}
		else
		if ( bounds[ i ]->m_Type == phBound::BOX )
		{ 
			AddBox( bnd, &curridx, bounds[ i ], &physPolyCount, phmaterials, materials, conversionFactor );
		}
		else
		if ( bounds[ i ]->m_Type == phBound::CAPSULE )
		{
			AddCapsule( bnd, &curridx, bounds[ i ], &physPolyCount, phmaterials, materials, conversionFactor );
		}
		else
		if ( bounds[ i ]->m_Type == phBound::SPHERE )
		{
			AddSphere( bnd, &curridx, bounds[ i ], &physPolyCount, phmaterials, materials, conversionFactor );
		}
		else
		if (bounds[ i ]->m_Type == phBound::CYLINDER )
		{
			AddCylinder( bnd, &curridx, bounds[ i ], &physPolyCount, phmaterials, materials, conversionFactor );
		}
	}

	// do post-processing on bound
	//bnd->DecreaseNumPolys(physPolyCount);
	//bnd->PostLoadCompute();

	// We used to just use the centroid offset as the center of gravity, but CalcCGOffset should give us a much better value.
#if 0
	bnd->SetCGOffset(bnd->GetCentroidOffset());
#else
	Vector3 newCGOffset;
	bnd->CalcCGOffset(RC_VEC3V(newCGOffset));
	bnd->SetCGOffset(RCC_VEC3V(newCGOffset));
#endif
	//bnd->ComputeNeighbors(NULL);
	bnd->WeldVertices(1e-3f);

	//if(m_QuadrifyGeometryBounds)
	//{
	//	bnd->ConvertTrianglesToQuads();
	//}

	bnd->RemoveDegeneratePolys();	

	return retval;
}

// Move to utility
void rexSerializerRAGEPhysicsBound::SplitBound( const rexObjectGenericBound *inputBound, atArray<rexObjectGenericBound*>& outputBounds ) const
{
	rexObjectGenericBound *bound0 = new rexObjectGenericBound;
	bound0->m_Type = phBound::GEOMETRY;
	rexObjectGenericBound *bound1 = new rexObjectGenericBound;
	bound1->m_Type = phBound::GEOMETRY;
	rexObjectGenericBound *currentBound;
	
	Matrix34	bbMatrix;
	float		bbLength;
	float		bbHeight;
	float		bbWidth;

	inputBound->MakeOptimizedBoundingBox( bbMatrix, bbLength, bbHeight, bbWidth, true );

	int numpolys = inputBound->m_Primitives.GetCount();
	Vector3 dimensions = Vector3( bbLength, bbHeight, bbWidth );
	//Vector3 bbmid = inputBound->m_BoundBoxMin + ( dimensions / 2.0f );
	Vector3 bbmid = bbMatrix.d;

	int splitAxis = 0;
	float axisLength = 0.0;
	for(int i=0; i < 3; i++)
	{
		if( Abs( dimensions[i] ) > axisLength )
		{
			axisLength =  Abs( dimensions[i] );
			splitAxis = i;
		}
	}
	
	for(int i = 0; i < numpolys; i++)
	{
		// convenience variable
		const rexObjectGenericMesh::PrimitiveInfo &prim = inputBound->m_Primitives[i];

		int polyVertCount = prim.m_Adjuncts.GetCount();

		if( polyVertCount )
		{
			//rexObjectGenericMesh::PrimitiveInfo newprim = new rexObjectGenericMesh::PrimitiveInfo;
			Vector3 vertpos = inputBound->m_Vertices[prim.m_Adjuncts[0].m_VertexIndex].m_Position;
			if( vertpos[splitAxis] < bbmid[splitAxis] )
			{
				currentBound = bound0;
			}
			else
			{
				currentBound = bound1;
			}

			rexObjectGenericMesh::PrimitiveInfo newprim;
			rexObjectGenericMesh::AdjunctInfo adjunct0;
			rexObjectGenericMesh::AdjunctInfo adjunct1;
			rexObjectGenericMesh::AdjunctInfo adjunct2;

			adjunct0.m_VertexIndex = currentBound->m_Vertices.GetCount();
			newprim.m_Adjuncts.PushAndGrow( adjunct0 );
			currentBound->m_Vertices.PushAndGrow( inputBound->m_Vertices[prim.m_Adjuncts[0].m_VertexIndex] );

			adjunct1.m_VertexIndex = currentBound->m_Vertices.GetCount();
			newprim.m_Adjuncts.PushAndGrow( adjunct1 );
			currentBound->m_Vertices.PushAndGrow( inputBound->m_Vertices[prim.m_Adjuncts[1].m_VertexIndex] );

			adjunct2.m_VertexIndex = currentBound->m_Vertices.GetCount();
			newprim.m_Adjuncts.PushAndGrow( adjunct2 );
			currentBound->m_Vertices.PushAndGrow( inputBound->m_Vertices[prim.m_Adjuncts[2].m_VertexIndex] );

			// Split name into the material name and the actual colour
			atString inputName;
			int inputColourIdx;
			atArray<atString> inputTokens;
			Vector4 inputCol = Vector4(0.0f, 0.0f, 0.0f, 1.0f);

			atString tempName = inputBound->m_Materials[prim.m_MaterialIndex].m_Name;
			tempName.Split( inputTokens, "|" );
				inputColourIdx = atoi( inputTokens[inputTokens.GetCount()-1] );
			for(int k=0; k<inputTokens.GetCount()-1; k++)
			{
				inputName = atString( atString(inputName, inputTokens[k]), atString("|") );
			}
			if( inputColourIdx > 0 )
			{
				inputCol = inputBound->m_colourTemplate[inputColourIdx-1];
			}
			
			s32 MatCount = currentBound->m_Materials.GetCount();
			bool matFound = false;
			for(int j=0;j<MatCount;j++)
			{
				
				atString currName;
				int currColourIdx;
				atArray<atString> currTokens;
				Vector4 currCol = Vector4(0.0f, 0.0f, 0.0f, 1.0f);

				currentBound->m_Materials[j].m_Name.Split( currTokens, "|" );
				currColourIdx = atoi( currTokens[currTokens.GetCount()-1] );
				for(int k=0; k<currTokens.GetCount()-1; k++)
				{
					currName = atString( atString(currName, currTokens[k]), atString("|") );
				}
				if( currColourIdx > 0 )
				{
					currCol = currentBound->m_colourTemplate[currColourIdx-1];
				}

				if( currName == inputName )
				{
					if( inputCol == currCol )
					{
						newprim.m_MaterialIndex = j;
						matFound = true;
						break;
					}
					
				}
			}
			if( !matFound )
			{
				int newColourIdx = 0;
				bool colourFound = false;
				if( inputColourIdx > 0 )
				{
					for( int k=0; k<currentBound->m_colourTemplate.GetCount(); k++ )
					{
						if( inputCol == currentBound->m_colourTemplate[k] )
						{
							newColourIdx = k;
							colourFound = true;
							break;
						}
					}

					if( !colourFound )
					{
						currentBound->m_colourTemplate.PushAndGrow( inputCol );
						newColourIdx = currentBound->m_colourTemplate.GetCount();
					}
				}
				
				newprim.m_MaterialIndex = currentBound->m_Materials.GetCount();
				rexObjectGenericMesh::MaterialInfo mat = inputBound->m_Materials[prim.m_MaterialIndex];
				char str[64];
				_itoa( newColourIdx, str, 10 );
				mat.m_Name = atString( inputName, str );
				currentBound->m_Materials.PushAndGrow( mat );
			}
			currentBound->m_Primitives.PushAndGrow( newprim );
		}
	}

	if( bound0->m_Primitives.GetCount() > 0 )
	{
		if( bound0->m_Materials.GetCount() > 256 )
		{
			SplitBound( bound0, outputBounds );
		}
		else
		{
			outputBounds.PushAndGrow( bound0 );
		}
		
	}
	else
	{
		delete bound0;
	}

	if( bound1->m_Primitives.GetCount() > 0 )
	{
		if( bound1->m_Materials.GetCount() > 256 )
		{
			SplitBound( bound1, outputBounds );
		}
		else
		{
			outputBounds.PushAndGrow( bound1 );
		}
	}
	else
	{
		delete bound1;
	}
}


rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundGeometryWithPrims( phBoundGeometry *&bnd, const rexObjectGenericBound *inputBound ) const
{
	rexResult retval(rexResult::PERFECT);

	// LPXO: New code
	atArray< const rexObjectGenericBound* > bounds;
	bounds.PushAndGrow(inputBound);

	// If we're dealing with split meshes we dont want to handle contained objects
	if( !GetSplitOnMaterialLimit() )
	{
		for ( int i = 0; i < inputBound->m_ContainedObjects.GetCount(); ++i )
		{
			const rexObjectGenericBound* bound = 
				dynamic_cast< const rexObjectGenericBound* >(  inputBound->m_ContainedObjects[ i ] );
			if ( bound )
				bounds.PushAndGrow( bound );
		}
	}

	atArray< rexObjectGenericMesh::MaterialInfo > materials;

	for ( int i = 0; i < inputBound->m_Materials.GetCount(); ++i )
	{
		materials.PushAndGrow( inputBound->m_Materials[ i ] );
	}
	
	if( !GetSplitOnMaterialLimit() )
	{
		// find all the materials
		for ( int i = 0; i < bounds.GetCount(); ++i )
		{
			if( bounds[i]->m_Type != phBound::GEOMETRY )
			{
				for ( int j = 0; j < bounds[ i ]->m_Materials.GetCount(); ++j )
				{
					bool found = false;

					atString mtlName0;
					GetPhysicsMaterialName(bounds[ i ]->m_Materials[ j ], mtlName0);

					for ( int k = 0; k < materials.GetCount(); ++k )
					{
						atString mtlName1;
						GetPhysicsMaterialName(materials[ k ], mtlName1);

						if( mtlName0 ==  mtlName1)
						{
							found = true;
							break;
						}
					}

					if ( !found )
					{
						materials.PushAndGrow( bounds[ i ]->m_Materials[ j ] );
					}
				}
			}
		}
	}
	

	if(materials.GetCount() > 256)
	{
		retval.Set( rexResult::ERRORS, rexResult::TYPE_INCOMPATIBLE );
		retval.AddError("Material limit of 256 exceeded on '%s'. https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Excessive_Collision_Materials", (const char*)inputBound->GetMeshName() );
		return retval;
	}

	// now we've got the damn materials create them
	atArray< phMaterialMgr::Id > phmaterials;
	CreatePhysicsMaterials( &phmaterials, materials );


	int numverts = inputBound->m_Vertices.GetCount();
	int numpolys = inputBound->m_Primitives.GetCount();
	int nummaterials = materials.GetCount();
	float conversionFactor = GetUnitTypeLinearConversionFactor();

	int totalnumverts = 0;
	for ( int i = 0; i < bounds.GetCount(); ++i )
	{
		totalnumverts += GetVertexCount( bounds[ i ] );
	}

	int totalnumpolys = 0;
	for ( int i = 0; i < bounds.GetCount(); ++i )
	{
		totalnumpolys += GetPrimitiveCount( bounds[ i ] );
	}

	int i, j;
	bnd = new phBoundGeometry();

	const int numPerVertAttribs= (inputBound->m_colourTemplate.GetCount() > 0 && this->GetExportPerVertAttributes()) ? 1 : 0;

#if HACK_GTA4
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	bnd->Init(totalnumverts, numPerVertAttribs, nummaterials, inputBound->m_colourTemplate.GetCount(), totalnumpolys, m_HasSecondSurface);
	for(i=0; i< inputBound->m_colourTemplate.GetCount(); i++)
	{
		Color32 col = Color32((u32)inputBound->m_colourTemplate[i].x, (u32)inputBound->m_colourTemplate[i].y, (u32)inputBound->m_colourTemplate[i].z, (u32)inputBound->m_colourTemplate[i].w);
		bnd->SetMaterialColor(i, col);
	}
	
	atArray<Color32> tintColours;
	if(numPerVertAttribs > 0)
	{
		tintColours.ResizeGrow(totalnumverts);
		for(i=0; i<inputBound->m_Primitives.GetCount(); i++)
		{
			for(int j=0; j<inputBound->m_Primitives[i].m_Adjuncts.GetCount(); j++)
			{
				u32 vertIdx = inputBound->m_Primitives[i].m_Adjuncts[j].m_VertexIndex;

				float scaleZ = Clamp<float>(inputBound->m_Primitives[i].m_Adjuncts[j].m_ProceduralDensity, 0.0f, 1.0f);
				float scaleXYZ = Clamp<float>(inputBound->m_Primitives[i].m_Adjuncts[j].m_ProceduralScaleXYZ, 0.0f, 1.0f);
				float density = Clamp<float>(inputBound->m_Primitives[i].m_Adjuncts[j].m_ProceduralScaleZ, 0.0f, 1.0f);

				Color32 tintCol = Color32(
						(u32)Clamp<float>(inputBound->m_Primitives[i].m_Adjuncts[j].m_TintColour.x*255, 0.0f, 255.0f),
						(u32)Clamp<float>(inputBound->m_Primitives[i].m_Adjuncts[j].m_TintColour.y*255, 0.0f, 255.0f),
						(u32)Clamp<float>(inputBound->m_Primitives[i].m_Adjuncts[j].m_TintColour.z*255, 0.0f, 255.0f));

				int scaleDensity = round(scaleXYZ * 0xf);
				scaleDensity |= round(scaleZ * 0x3) << 4;
				scaleDensity |= round(density * 0x3) << 6;
				tintCol.SetAlpha(scaleDensity);

				tintColours[vertIdx] = tintCol;
			}
		}
	}
	
	for(i=0; i<tintColours.GetCount(); i++)
	{
		bnd->SetVertexAttrib(i, 0, tintColours[i]);
	}
#else
	bnd->Init(numverts, numPerVertAttribs, nummaterials, generatedPolyCount, m_HasSecondSurface);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	bool bHasSecondSurface = GetHasSecondSurface();//bnd->GetHasSecondSurface();
#else
#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	bnd->Init(numverts, numPerVertAttribs, nummaterials, 0, generatedPolyCount);
#else
	bnd->Init(numverts, numPerVertAttribs, nummaterials, generatedPolyCount);
#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
#endif // HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
#else
	bnd->Init(numverts, numPerVertAttribs, nummaterials, generatedPolyCount);
#endif // HACK_GTA4
	// set extra vertices to <0,0,0>, so that NAN exceptions aren't raised in Fourtified functions
	Vector3 zeroVector(0.0f,0.0f,0.0f);
	for(i = totalnumverts; i < Fourtify(totalnumverts); i++)
		bnd->SetVertex(i, RCC_VEC3V(zeroVector));
	
	// set vertices
	for(i = 0; i < numverts; i++)
	{
		Vector3 v;
		inputBound->m_Matrix.Transform( inputBound->m_Vertices[i].m_Position, v );
		inputBound->m_ParentBoneMatrix.UnTransform( v );
		//		Displayf( "Vertex %d: (%f, %f, %f)", i, v.x, v.y, v.z );
		v.Scale(conversionFactor);
		bnd->SetVertex(i, RCC_VEC3V(v));
#if HACK_GTA4
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
		if( bHasSecondSurface )
			bnd->SetSecondSurfaceVertexDisplacement(i, inputBound->m_Vertices[i].m_SecondSurfaceDisplacement);
#endif // HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
#endif // HACK_GTA4
	}



	

	// set materials
	for(i = 0; i < nummaterials; i++)
	{
		phMaterialMgr::Id mtl = phmaterials[i];

		bnd->SetMaterialId(i, mtl);
	}

	phPolygon poly;
	int matIndex, polyVertCount, polyTriCount, physPolyCount = 0;
#if POLY_MAX_VERTICES == 4
	u32 polyStartVert, i0, i1, i2, i3;
#else
	u32 polyStartVert, i0, i1;
#endif // POLY_MAX_VERTICES == 4

	// set polygons
	for(i = 0; i < numpolys; i++)
	{
		// convenience variable
		const rexObjectGenericMesh::PrimitiveInfo &prim = inputBound->m_Primitives[i];

		// set the physics material
		// material index of the primitive is -1 if the
		// maya material failed to be converted to a phMaterial.
		// in this case, use the default material (with index 0)
		// otherwise, look up the appropriate material in the maya material->phMaterial mapping
		matIndex = prim.m_MaterialIndex;
		polyVertCount = prim.m_Adjuncts.GetCount();

		if( polyVertCount )
		{
			polyStartVert = (u32)prim.m_Adjuncts[0].m_VertexIndex;

			if( !(inputBound->primitiveIsQuad(i)) )
			{
				// convert this polygon into triangles
				polyTriCount = polyVertCount - 2;
				for(j = 1; j <= polyTriCount; j++)
				{
					i0 = (u32)prim.m_Adjuncts[j].m_VertexIndex;
					i1 = (u32)prim.m_Adjuncts[j+1].m_VertexIndex;

					Vector3 polyNormal;
					Vector3 polyCenter;
					polyCenter.Add( inputBound->m_Vertices[ i0 ].m_Position, inputBound->m_Vertices[ i1 ].m_Position );
					polyCenter.Add( inputBound->m_Vertices[ polyStartVert ].m_Position );
					polyCenter.Scale( 1.0f / 3.0f );
					GetTriNormal( inputBound->m_Vertices[ i0 ].m_Position, polyCenter, inputBound->m_Vertices[ i1 ].m_Position, polyNormal );
					float dotProduct = polyNormal.Dot( prim.m_FaceNormal );
					if( dotProduct < 0.0f )
						bnd->InitPolygonTri(poly, phPolygon::Index(polyStartVert), phPolygon::Index(i1), phPolygon::Index(i0) );
					//poly.InitTriangle(polyStartVert, i1, i0, bnd->GetVertex(polyStartVert), bnd->GetVertex(i1), bnd->GetVertex(i0));
					else
						bnd->InitPolygonTri(poly, phPolygon::Index(i0), phPolygon::Index(i1), phPolygon::Index(polyStartVert) );
					//poly.InitTriangle(i0, i1, polyStartVert, bnd->GetVertex(i0), bnd->GetVertex(i1), bnd->GetVertex(polyStartVert));
#if POLY_MAX_VERTICES == 4
					poly.SetMaterialIndex((u8) matIndex );
					Vector3 vecTemp = VEC3V_TO_VECTOR3(poly.GetUnitNormal());//, bnd->GetVertexPointer());
#else
					bnd->SetPolygonMaterialIndex(physPolyCount, matIndex);
					Vector3 vecTemp = polyNormal;//, bnd->GetVertexPointer());
#endif // POLY_MAX_VERTICES == 4
					if(vecTemp.Mag2() < 0.9f)
						//if(poly.GetNormal().Mag2() < 0.9f)
					{
						// TODO: write a warning to log (polygon [index] has non-unit normal, determine type of poly by using IsTriangle()) - wil
						retval.Set(rexResult::WARNINGS, rexResult::CONVERSION);
					}
					else
						bnd->SetPolygon(physPolyCount++, poly);
				}
			}
			else
			{
#if POLY_MAX_VERTICES == 3
				retval.Combine(rexResult::ERRORS);
				retval.AddError("Attempt to export a quad.  Not supported");
#else 
				// the last index for a quad CANNOT be zero (that's how we
				// determine if a poly is a tri or a quad) so we have to
				// rotate the vertices.
				if(0 == polyStartVert)
				{
					i0 = polyStartVert;
					i1 = (u32)prim.m_Adjuncts[1].m_VertexIndex;
					i2 = (u32)prim.m_Adjuncts[2].m_VertexIndex;
					i3 = (u32)prim.m_Adjuncts[3].m_VertexIndex;
				}
				else
				{
					i0 = (u32)prim.m_Adjuncts[1].m_VertexIndex;
					i1 = (u32)prim.m_Adjuncts[2].m_VertexIndex;
					i2 = (u32)prim.m_Adjuncts[3].m_VertexIndex;
					i3 = polyStartVert;
				}

				Vector3 polyNormal;
				Vector3 polyCenter;
				polyCenter.Add( inputBound->m_Vertices[ i0 ].m_Position, inputBound->m_Vertices[ i1 ].m_Position );
				polyCenter.Add( inputBound->m_Vertices[ i2 ].m_Position );
				polyCenter.Add( inputBound->m_Vertices[ i3 ].m_Position );
				polyCenter.Scale( 0.25f );
				GetTriNormal( inputBound->m_Vertices[ i0 ].m_Position, polyCenter, inputBound->m_Vertices[ i1 ].m_Position, polyNormal );
				float dotProduct = polyNormal.Dot( prim.m_FaceNormal );
				if( dotProduct < 0.0f )
					bnd->InitPolygonQuad( poly, i2, i1, i0, i3 );
				//poly.InitQuad(i2, i1, i0, i3, bnd->GetVertex(i2), bnd->GetVertex(i1), bnd->GetVertex(i0), bnd->GetVertex(i3));
				else
					bnd->InitPolygonQuad( poly, i0, i1, i2, i3 );
				//poly.InitQuad(i0, i1, i2, i3, bnd->GetVertex(i0), bnd->GetVertex(i1), bnd->GetVertex(i2), bnd->GetVertex(i3));
				poly.SetMaterialIndex((u8) matIndex );

				Vector3 vecTemp = VEC3V_TO_VECTOR3(poly.GetUnitNormal());//, bnd->GetVertexPointer());
				if(vecTemp.Mag2() < 0.9f)
					//				if(poly.GetNormal().Mag2() < 0.9f)
				{
					// TODO: write a warning to log (polygon [index] has non-unit normal, determine type of poly by using IsTriangle()) - wil
					retval.Set(rexResult::WARNINGS, rexResult::CONVERSION);
				}
				else
				{
					bnd->SetPolygon(physPolyCount++, poly);
				}
#endif // POLY_MAX_VERTICES == 3
			}
		}
		else // no adjuncts
		{
			Assert( 0 && "Poly with no adjuncts!" );
		}
	}
	int curridx = numverts;
	for ( int i = 0; i < bounds.GetCount(); ++i )
	{
		if ( bounds[ i ]->m_Type == phBound::BOX )
		{
			AddBox( bnd, &curridx, bounds[ i ], &physPolyCount, phmaterials, materials, conversionFactor );
		}
		else if ( bounds[ i ]->m_Type == phBound::CAPSULE )
		{
			AddCapsule( bnd, &curridx, bounds[ i ], &physPolyCount, phmaterials, materials, conversionFactor );
		}
		else if ( bounds[ i ]->m_Type == phBound::SPHERE )
		{
			AddSphere( bnd, &curridx, bounds[ i ], &physPolyCount, phmaterials, materials, conversionFactor );
		}
		else
		if (bounds[ i ]->m_Type == phBound::CYLINDER )
		{
			AddCylinder( bnd, &curridx, bounds[ i ], &physPolyCount, phmaterials, materials, conversionFactor );
		}
	}

	// do postprocessing on bound
	bnd->DecreaseNumPolys(physPolyCount);

	// bnd->PostLoadCompute() relies on welded vertices, which isn't guaranteed unless m_OptimiseGeometry is true
	if(m_OptimiseGeometry)
		bnd->PostLoadCompute();

	// We used to just use the centroid offset as the center of gravity, but CalcCGOffset should give us a much better value.
#if 0
	bnd->SetCGOffset(bnd->GetCentroidOffset());
#else
	Vector3 newCGOffset(0.0f,0.0f,0.0f);
	//bnd->CalcCGOffset(RC_VEC3V(newCGOffset));
	bnd->SetCGOffset(RCC_VEC3V(newCGOffset));
#endif

#if	POLY_MAX_VERTICES == 4
	if(m_QuadrifyGeometryBounds)
	{
		bnd->ConvertTrianglesToQuads();
	}
#endif	// POLY_MAX_VERTICES == 3

	return retval;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundBvh ( phBoundBVH *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const
{
	bnd = new phBoundBVH;
	phBoundGeometry* bigGeomBound = NULL;
	
	rexResult r;
	if(GetPrimitiveBoundExportPrimitivesInBvh())
		r = GenericBoundToPhBoundGeometryWithPrims( bigGeomBound, inputBound );
	else
		r = GenericBoundToPhBoundGeometry( bigGeomBound, inputBound, materials, materialMap );

	if( !( r && bigGeomBound ) )
		return r;

	bnd->Copy( bigGeomBound );

	bnd->Build();

	if(m_OptimiseGeometry)
	{
		bnd->WeldVertices(1e-3f);
		bnd->RemoveDegeneratePolys();
	}

	delete bigGeomBound;
	return r;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if USE_GRIDS
rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundGrid( phBoundGrid *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap, bool ASSERT_ONLY(bIsBvhGrid) )  const
{
	bnd = new phBoundGrid;
	phBoundGeometry* bigGeomBound = NULL;

	rexResult r = GenericBoundToPhBoundGeometry( bigGeomBound, inputBound, materials, materialMap );
	if( !( r && bigGeomBound ) )
		return r;

	int minX = (int)floorf( bigGeomBound->GetBoundingBoxMin().GetXf() / m_GridCellSize );
	int maxX = (int)floorf( bigGeomBound->GetBoundingBoxMax().GetXf() / m_GridCellSize );
	int minZ = (int)floorf( bigGeomBound->GetBoundingBoxMin().GetZf() / m_GridCellSize );
	int maxZ = (int)floorf( bigGeomBound->GetBoundingBoxMax().GetZf() / m_GridCellSize );

	bigGeomBound->RemoveDegeneratePolys();
	bigGeomBound->WeldVertices (1e-3f);
	
	Assert(bIsBvhGrid);
	bnd->InitGrid<phBoundBVH>(m_GridCellSize,minX,maxX,minZ,maxZ);

	bnd->AddGeometry( bigGeomBound );
	//if (!bnd->InitOctrees())
	//{
	//	r.Combine(rexResult::ERRORS);
	//	r.AddMessage("An error has occured while converting the bound '%s'.", (const char*)inputBound->GetMeshName());
	//	return r;
	//}

	/*int minotx = bnd->GetMinFirstAxis();
	int maxotx = bnd->GetMaxFirstAxis();
	int minotz = bnd->GetMinSecondAxis();
	int maxotz = bnd->GetMaxSecondAxis();

	for ( int ot1 = minotx; ot1 <= maxotx; ot1++ )
	{
		for ( int ot2 = minotz; ot2 <= maxotz; ot2++ )
		{
			phBoundGeometry* myOctree = static_cast<phBoundGeometry*>(bnd->GetOctree(ot1, ot2));
			myOctree->SetVertEdgeMaterial(myOctree->GetNumMaterials()-1);
		}
	}*/

	delete bigGeomBound;
	return r;
}
#endif // USE_GRIDSF
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundGeometry( phBoundGeometry *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap ) const
{
	rexResult retval(rexResult::PERFECT);

	int numverts = inputBound->m_Vertices.GetCount(),
		nummaterials = materials.GetCount(),
		numpolys = inputBound->m_Primitives.GetCount(),
		i, j;

	int generatedPolyCount = 0;
	float conversionFactor = GetUnitTypeLinearConversionFactor();
	for(i = 0; i < numpolys; i++)
	{
		// convenience variable
		const rexObjectGenericMesh::PrimitiveInfo &prim = inputBound->m_Primitives[i];

		int polyverts = prim.m_Adjuncts.GetCount();
		int prims = polyverts - 2;
		bool boundQuad = inputBound->primitiveIsQuad(i);
		if( boundQuad )
			generatedPolyCount += 1;
		else if( prims > 0 )
			generatedPolyCount += prims;
	}

//	Displayf( "Name %s", (const char*) inputBound->m_FullPath );
//	Displayf("%f %f %f %f", inputBound->m_ParentBoneMatrix.a.x, inputBound->m_ParentBoneMatrix.a.y, inputBound->m_ParentBoneMatrix.a.z);
//	Displayf("%f %f %f %f", inputBound->m_ParentBoneMatrix.b.x, inputBound->m_ParentBoneMatrix.b.y, inputBound->m_ParentBoneMatrix.b.z);
//	Displayf("%f %f %f %f", inputBound->m_ParentBoneMatrix.c.x, inputBound->m_ParentBoneMatrix.c.y, inputBound->m_ParentBoneMatrix.c.z);
//	Displayf("%f %f %f %f", inputBound->m_ParentBoneMatrix.d.x, inputBound->m_ParentBoneMatrix.d.y, inputBound->m_ParentBoneMatrix.d.z);

	bnd = new phBoundGeometry();

	const int numPerVertAttribs=0;
#if HACK_GTA4
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
	#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		bnd->Init(numverts, numPerVertAttribs, nummaterials, inputBound->m_colourTemplate.GetCount(), generatedPolyCount, m_HasSecondSurface);
		for(i=0; i< inputBound->m_colourTemplate.GetCount(); i++)
		{
			Color32 col = Color32((u32)inputBound->m_colourTemplate[i].x, (u32)inputBound->m_colourTemplate[i].y, (u32)inputBound->m_colourTemplate[i].z, (u32)inputBound->m_colourTemplate[i].w);
			bnd->SetMaterialColor(i, col);
		}
	#else
		bnd->Init(numverts, numPerVertAttribs, nummaterials, generatedPolyCount, m_HasSecondSurface);
	#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
	bool bHasSecondSurface = GetHasSecondSurface();//bnd->GetHasSecondSurface();
#else
	#if HACK_GTA4_64BIT_MATERIAL_ID_COLORS
		bnd->Init(numverts, numPerVertAttribs, nummaterials, 0, generatedPolyCount);
	#else
		bnd->Init(numverts, numPerVertAttribs, nummaterials, generatedPolyCount);
	#endif // HACK_GTA4_64BIT_MATERIAL_ID_COLORS
#endif // HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
#else
	bnd->Init(numverts, numPerVertAttribs, nummaterials, generatedPolyCount);
#endif // HACK_GTA4
	// set vertices
	
	for(i = 0; i < numverts; i++)
	{
		Vector3 v;
		inputBound->m_Matrix.Transform( inputBound->m_Vertices[i].m_Position, v );
		inputBound->m_ParentBoneMatrix.UnTransform( v );
//		Displayf( "Vertex %d: (%f, %f, %f)", i, v.x, v.y, v.z );
		v.Scale(conversionFactor);
		bnd->SetVertex(i, RCC_VEC3V(v));
#if HACK_GTA4
#if HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
		if( bHasSecondSurface )
			bnd->SetSecondSurfaceVertexDisplacement(i, inputBound->m_Vertices[i].m_SecondSurfaceDisplacement);
#endif // HACK_GTA4_BOUND_GEOM_SECOND_SURFACE_DATA
#endif // HACK_GTA4
	}



	// set extra vertices to <0,0,0>, so that NAN exceptions aren't raised in Fourtified functions
	Vector3 zeroVector(0.0f,0.0f,0.0f);
	for(i = numverts; i < Fourtify(numverts); i++)
		bnd->SetVertex(i, RCC_VEC3V(zeroVector));

	// set materials
	for(i = 0; i < nummaterials; i++)
	{
		phMaterialMgr::Id mtl = materials[i];
		
		bnd->SetMaterialId(i, mtl);
	}

	phPolygon poly;
	int matIndex, polyVertCount, polyTriCount, physPolyCount = 0;
#if POLY_MAX_VERTICES == 4
	u32 polyStartVert, i0, i1, i2, i3;
#else
	u32 polyStartVert, i0, i1;
#endif // POLY_MAX_VERTICES == 4

	// set polygons
	for(i = 0; i < numpolys; i++)
	{
		// convenience variable
		const rexObjectGenericMesh::PrimitiveInfo &prim = inputBound->m_Primitives[i];

		// set the physics material
		// material index of the primitive is -1 if the
		// maya material failed to be converted to a phMaterial.
		// in this case, use the default material (with index 0)
		// otherwise, look up the appropriate material in the maya material->phMaterial mapping
		matIndex = prim.m_MaterialIndex;
		matIndex = (matIndex >= 0) ? materialMap[matIndex] : 0;

		polyVertCount = prim.m_Adjuncts.GetCount();

		if( polyVertCount )
		{
			polyStartVert = (u32)prim.m_Adjuncts[0].m_VertexIndex;

			if( !(inputBound->primitiveIsQuad(i)) )
			{
				// convert this polygon into triangles
				polyTriCount = polyVertCount - 2;
				for(j = 1; j <= polyTriCount; j++)
				{
					i0 = (u32)prim.m_Adjuncts[j].m_VertexIndex;
					i1 = (u32)prim.m_Adjuncts[j+1].m_VertexIndex;

					Vector3 polyNormal;
					Vector3 polyCenter;
					polyCenter.Add( inputBound->m_Vertices[ i0 ].m_Position, inputBound->m_Vertices[ i1 ].m_Position );
					polyCenter.Add( inputBound->m_Vertices[ polyStartVert ].m_Position );
					polyCenter.Scale( 1.0f / 3.0f );
					GetTriNormal( inputBound->m_Vertices[ i0 ].m_Position, polyCenter, inputBound->m_Vertices[ i1 ].m_Position, polyNormal );
					float dotProduct = polyNormal.Dot( prim.m_FaceNormal );
					if( dotProduct < 0.0f )
						bnd->InitPolygonTri(poly, phPolygon::Index(polyStartVert), phPolygon::Index(i1), phPolygon::Index(i0) );
						//poly.InitTriangle(polyStartVert, i1, i0, bnd->GetVertex(polyStartVert), bnd->GetVertex(i1), bnd->GetVertex(i0));
					else
						bnd->InitPolygonTri(poly, phPolygon::Index(i0), phPolygon::Index(i1), phPolygon::Index(polyStartVert) );
						//poly.InitTriangle(i0, i1, polyStartVert, bnd->GetVertex(i0), bnd->GetVertex(i1), bnd->GetVertex(polyStartVert));
#if POLY_MAX_VERTICES == 4
					poly.SetMaterialIndex((u8) matIndex );
					Vector3 vecTemp = VEC3V_TO_VECTOR3(poly.GetUnitNormal());//, bnd->GetVertexPointer());
#else
					bnd->SetPolygonMaterialIndex(physPolyCount, matIndex);
					Vector3 vecTemp = polyNormal;//, bnd->GetVertexPointer());
#endif // POLY_MAX_VERTICES == 4
					
					if(vecTemp.Mag2() < 0.9f)
					//if(poly.GetNormal().Mag2() < 0.9f)
					{
						// TODO: write a warning to log (polygon [index] has non-unit normal, determine type of poly by using IsTriangle()) - wil
						retval.Set(rexResult::WARNINGS, rexResult::CONVERSION);
					}
					else
						bnd->SetPolygon(physPolyCount++, poly);
				}
			}
			else
			{
#if POLY_MAX_VERTICES == 3
				retval.Combine(rexResult::ERRORS);
				retval.AddError("Attempt to export a quad.  Not supported");
#else 
				// the last index for a quad CANNOT be zero (that's how we
				// determine if a poly is a tri or a quad) so we have to
				// rotate the vertices.
				if(0 == polyStartVert)
				{
					i0 = polyStartVert;
					i1 = (u32)prim.m_Adjuncts[1].m_VertexIndex;
					i2 = (u32)prim.m_Adjuncts[2].m_VertexIndex;
					i3 = (u32)prim.m_Adjuncts[3].m_VertexIndex;
				}
				else
				{
					i0 = (u32)prim.m_Adjuncts[1].m_VertexIndex;
					i1 = (u32)prim.m_Adjuncts[2].m_VertexIndex;
					i2 = (u32)prim.m_Adjuncts[3].m_VertexIndex;
					i3 = polyStartVert;
				}

				Vector3 polyNormal;
				Vector3 polyCenter;
				polyCenter.Add( inputBound->m_Vertices[ i0 ].m_Position, inputBound->m_Vertices[ i1 ].m_Position );
				polyCenter.Add( inputBound->m_Vertices[ i2 ].m_Position );
				polyCenter.Add( inputBound->m_Vertices[ i3 ].m_Position );
				polyCenter.Scale( 0.25f );
				GetTriNormal( inputBound->m_Vertices[ i0 ].m_Position, polyCenter, inputBound->m_Vertices[ i1 ].m_Position, polyNormal );
				float dotProduct = polyNormal.Dot( prim.m_FaceNormal );
				if( dotProduct < 0.0f )
					bnd->InitPolygonQuad( poly, i2, i1, i0, i3 );
					//poly.InitQuad(i2, i1, i0, i3, bnd->GetVertex(i2), bnd->GetVertex(i1), bnd->GetVertex(i0), bnd->GetVertex(i3));
				else
					bnd->InitPolygonQuad( poly, i0, i1, i2, i3 );
					//poly.InitQuad(i0, i1, i2, i3, bnd->GetVertex(i0), bnd->GetVertex(i1), bnd->GetVertex(i2), bnd->GetVertex(i3));
				poly.SetMaterialIndex((u8) matIndex );

				Vector3 vecTemp = VEC3V_TO_VECTOR3(poly.GetUnitNormal());//, bnd->GetVertexPointer());
				if(vecTemp.Mag2() < 0.9f)
//				if(poly.GetNormal().Mag2() < 0.9f)
				{
					// TODO: write a warning to log (polygon [index] has non-unit normal, determine type of poly by using IsTriangle()) - wil
					retval.Set(rexResult::WARNINGS, rexResult::CONVERSION);
				}
				else
				{
					bnd->SetPolygon(physPolyCount++, poly);
				}
#endif // POLY_MAX_VERTICES == 3
			}
		}
		else // no adjuncts
		{
			Assert( 0 && "Poly with no adjuncts!" );
		}
	}

	// do postprocessing on bound
	bnd->DecreaseNumPolys(physPolyCount);
	bnd->PostLoadCompute();

	// We used to just use the centroid offset as the center of gravity, but CalcCGOffset should give us a much better value.
#if 0
	bnd->SetCGOffset(bnd->GetCentroidOffset());
#else
	Vector3 newCGOffset;
	bnd->CalcCGOffset(RC_VEC3V(newCGOffset));
	bnd->SetCGOffset(RCC_VEC3V(newCGOffset));
#endif
	bnd->ComputeNeighbors(NULL);

#if POLY_MAX_VERTICES == 4
	if(m_QuadrifyGeometryBounds)
	{
		bnd->ConvertTrianglesToQuads();
	}
#endif // POLY_MAX_VERTICES == 4	

	if(m_OptimiseGeometry)
	{
		bnd->WeldVertices(1e-3f);
		bnd->RemoveDegeneratePolys();	
	}
	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if USE_GEOMETRY_CURVED
rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundCurvedGeometry( phBoundCurvedGeometry *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap )  const
{
	// NOTE: This function is supposed to take any rexObjectGenericBound and convert it to
	// a phBoundCurvedGeometry. For now, however, we are only supporting setting the curved
	// geometry bound up as a cylinder shape. That is to say: this function assumes the input
	// contains information for a cylinder (height/radius1/radius2), and will setup a curved
	// geometry bound for that cylinder.

	rexResult retval(rexResult::PERFECT);

	bnd = new phBoundCurvedGeometry();
	if (bnd)
	{
		float conversionFactor = GetUnitTypeLinearConversionFactor();

		// Get information needed from input bound
		float height = inputBound->m_Height * conversionFactor;
		float radius1 = inputBound->m_Radius * conversionFactor;
		float radius2 = inputBound->m_Radius2 * conversionFactor;
		int numVerts = 2;		// Hard-coded for cylinder
		int numPolys = 0;		// Hard-coded for cylinder
		int numCurvedEdges = 2;	// Hard-coded for cylinder
		int numCurvedFaces = 3;	// Hard-coded for cylinder
		int numMaterials = 1;
		
		// Determine a good margin for the bound
		float margin = bnd->GetMargin();
		margin = Min(height * 0.25f, margin);
		margin = Min(radius1 * 0.25f, margin);
		margin = Min(radius2 * 0.25f, margin);

		// Inform the bound of its margin
		bnd->SetMargin(margin);

		// Deduct the margin from the shape parameters
		height -= margin * 2.0f;
		radius1 -= margin;
		radius2 -= margin;

		// Initialize the curved geometry bound
		bnd->Init(numVerts, numMaterials, numPolys, numCurvedEdges, numCurvedFaces);
		bnd->AllocateCurvedEdgesAndFaces();

		// Set the vertices
		bnd->SetVertex(0, Vec3V(radius2, 0.0f, -(height * 0.5f)));	// Bottom vert
		bnd->SetVertex(1, Vec3V(radius1, 0.0f, (height * 0.5f)));	// Top vert

		// Set the curved edges
		bnd->SetCurvedEdge(0, 0, 0, radius2, -YAXIS, -XAXIS);	// Bottom curved edge
		bnd->SetCurvedEdge(1, 1, 1, radius1, YAXIS, -XAXIS);	// Top curved edge

		// Set the curved faces
		int vertIndices[2], curvedEdgeIndices[2], curvedEdgePolyIndices[2];

		vertIndices[0] = 0;
		curvedEdgeIndices[0] = 0;
		curvedEdgePolyIndices[0] = 0;
		bnd->SetCurvedFace(0, 1, vertIndices, 1, curvedEdgeIndices, curvedEdgePolyIndices, inputBound->m_CapCurve);	// Bottom curved face

		vertIndices[0] = 1;
		curvedEdgeIndices[0] = 1;
		curvedEdgePolyIndices[0] = 0;
		bnd->SetCurvedFace(1, 1, vertIndices, 1, curvedEdgeIndices, curvedEdgePolyIndices, inputBound->m_CapCurve);	// Top curved face

		vertIndices[0] = 0;
		vertIndices[1] = 1;
		curvedEdgeIndices[0] = 0;
		curvedEdgeIndices[1] = 1;
		curvedEdgePolyIndices[0] = 0;
		curvedEdgePolyIndices[1] = 2;
		bnd->SetCurvedFace(2, 2, vertIndices, 2, curvedEdgeIndices, curvedEdgePolyIndices, inputBound->m_SideCurve);	// Middle curved face

		// Set the center of gravity
		bnd->SetCGOffset( Vec3V(V_ZERO) );

		// Set the material
		if (materialMap.GetCount())
		{
			// Only one material can be assigned for primitive
			bnd->SetMaterial( materials[materialMap[0]] );
		}
		else
		{
			// Only use the last material
			bnd->SetMaterial( materials.GetCount()-1 );
		}

		// Now that the bound is ready, do some post-processing
		bnd->CalculateGeomExtents();
	}
	else
	{
		retval.Set(rexResult::WARNINGS, rexResult::CANNOT_CREATE_OBJECT);
		retval.AddWarningCtx(inputBound->GetMeshName(), "Can't create bounds for input collision %s!", inputBound->GetMeshName());
	}

	return retval;
}
#endif // USE_GEOMETRY_CURVED
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexSerializerRAGEPhysicsBound::SetupCompositeTypeAndIncludeFlags(phBoundComposite *composite, const rexObjectGenericBound *inputBound, int partIndex) const
{
	//if(inputBound->m_surfaceFlags)
	//{
		if(!composite->GetTypeAndIncludeFlags())
		{
			composite->AllocateTypeAndIncludeFlags();

			for( int i = 0; i < composite->GetNumBounds(); i++ )
			{
				composite->SetTypeFlags(i, 0);
				composite->SetIncludeFlags(i, 0);
			}
		}

		composite->SetTypeFlags(partIndex, inputBound->m_surfaceFlags);
	//}
	//else
	//{
	//	if(composite->GetTypeAndIncludeFlags())
	//	{
	//		composite->SetTypeFlags(partIndex, 0);		
	//	}
	//}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult	rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundComposite( phBoundComposite *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id>& /*materials*/, const atArray<int>& /*materialMap*/ ) const
{
	rexResult retval(rexResult::PERFECT);
	
	int childCount = inputBound->m_ContainedObjects.GetCount();
	
	int subboundcount = 0;
	
	for( int c = 0; c < childCount; c++ )
	{
		rexObjectGenericBound* bound = dynamic_cast<rexObjectGenericBound*>( inputBound->m_ContainedObjects[ c ] );
		Assert( bound );
		if(!GetPrimitiveBoundExportPrimitivesInBvh() ||(GetPrimitiveBoundExportPrimitivesInBvh() && bound->m_Type == phBound::GEOMETRY) )
		{
			subboundcount++;
		}
	}
	
	bnd = new phBoundComposite;
	bnd->Init( subboundcount + 1 );
	
	int boundGeoCount = 0;
	for( int c = 0; c < childCount; c++ )
	{
		rexObjectGenericBound* bound = dynamic_cast<rexObjectGenericBound*>( inputBound->m_ContainedObjects[ c ] );
		Assert( bound );
		
		if( !GetPrimitiveBoundExportPrimitivesInBvh() ||(GetPrimitiveBoundExportPrimitivesInBvh() && bound->m_Type == phBound::GEOMETRY) )
		{
			SetupCompositeTypeAndIncludeFlags(bnd, bound, boundGeoCount);
			phBound* childBnd = NULL;
			retval &= GenericBoundToPhBound( childBnd, bound, true );
			Assert( childBnd );
			bnd->SetBound( boundGeoCount, childBnd );
			bnd->SetCurrentMatrix( boundGeoCount, RCC_MAT34V(bound->m_Matrix) );
			boundGeoCount++;
		}
	}		

	phBound* b = NULL;
	retval &= GenericBoundToPhBound( b, inputBound, true, false );

	Assert( b );
	bnd->SetBound( subboundcount, b );
	bnd->SetCurrentMatrix( subboundcount, RCC_MAT34V(inputBound->m_Matrix) );

	SetupCompositeTypeAndIncludeFlags(bnd, inputBound, subboundcount);

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


rexResult rexSerializerRAGEPhysicsBound::GenericBoundToPhBoundDisc( phBoundDisc *&bnd, const rexObjectGenericBound *inputBound, const atArray<phMaterialMgr::Id> &materials, const atArray<int> &materialMap )  const
{
	rexResult retval(rexResult::PERFECT);

	bnd = new phBoundDisc();

	if(bnd)
	{
		float conversionFactor=GetUnitTypeLinearConversionFactor();
		float margin=inputBound->m_Width/2*conversionFactor;
		float radius=inputBound->m_Radius*conversionFactor - margin;
		bnd->SetMargin(margin);
		bnd->SetDiscRadius(radius);

		if (m_ZeroPrimitiveCentroids)
		{
			bnd->SetCentroidOffset(Vec3V(V_ZERO));
		}
		else
		{
			Vector3 offset;
			inputBound->m_ParentBoneMatrix.UnTransform(inputBound->m_Matrix.d, offset);
			offset.Scale(conversionFactor);
			bnd->SetCentroidOffset(RCC_VEC3V(offset));
		}

		//Assign the material
		if(materialMap.GetCount())
		{
			//Only one material can be assigned for primitive
			bnd->SetMaterial(materials[materialMap[0]]);
		}
		else
		{
			// only use the last material
			bnd->SetMaterial(materials.GetCount()-1);
		}
	}
	else
	{
		retval.Set(rexResult::WARNINGS, rexResult::CANNOT_CREATE_OBJECT);
		retval.AddWarningCtx(inputBound->GetMeshName(), "Can't create (disc) bounds for input collision %s!", inputBound->GetMeshName());
	}

	return retval;

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Properties
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundSetGeometryBoundsExportAsBvhGrid::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>( module.m_Serializer );

	if( ser )
	{
		ser->SetGeometryBoundsExportAsBvhGrid( value.toBool( ser->GetGeometryBoundsExportAsBvhGrid() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyBoundSetGeometryBoundsExportAsBvhGeom::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>( module.m_Serializer );

	if( ser )
	{
		ser->SetGeometryBoundsExportAsBvhGeom( value.toBool( ser->GetGeometryBoundsExportAsBvhGeom() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyBoundSetGridCellSize::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>( module.m_Serializer );

	if( ser )
	{
		ser->SetGridCellSize( value.toFloat( ser->GetGridCellSize() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyZeroPrimitiveCentroids::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>( module.m_Serializer );

	if( ser )
	{
		ser->SetZeroPrimitiveCentroids( value.toBool( ser->GetZeroPrimitiveCentroids() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyCheckBoundVertexCounts::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>( module.m_Serializer );

	if( ser )
	{
		ser->SetCheckVertexBoundCounts( value.toBool( ser->GetCheckVertexBoundCounts() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyQuadrifyGeometryBounds::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>( module.m_Serializer );

	if( ser )
	{
		ser->SetQuadrifyGeometryBounds( value.toBool( ser->GetQuadrifyGeometryBounds() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyApplyBulletShrinkMargin::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>( module.m_Serializer );

	if( ser )
	{
		ser->SetApplyBulletShrinkMargin( value.toBool( ser->GetApplyBulletShrinkMargin() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertySplitOnMaterialLimit::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>( module.m_Serializer );

	if( ser )
	{
		ser->SetSplitOnMaterialLimit( value.toBool( ser->GetSplitOnMaterialLimit() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyBoundCheckFlags::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast<rexSerializerRAGEPhysicsBound*>( module.m_Serializer );

	if( ser )
	{
		ser->SetBoundCheckFlags( atString(value.toString()) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyBoundExportPerVertAttributes::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEPhysicsBound* ser = dynamic_cast< rexSerializerRAGEPhysicsBound* >( module.m_Serializer );
	if(ser)
	{
		ser->SetExportPerVertAttributes(value.toBool());
		return rexResult( rexResult::PERFECT );
	}

	return rexResult(rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE);
}

} // namespace rage
