// 
// rexRAGE/serializerLevelInstance.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		AGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSerializerLevelInstance
//			 -- generates old level files
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexSerializer
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_SERIALIZER_LEVEL_INSTANCE_H__
#define __REX_SERIALIZER_LEVEL_INSTANCE_H__

#include "rexBase/serializer.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class fiStream;

class rexSerializerLevelInstance : public rexSerializer
{
public:
	virtual rexResult Serialize( rexObject& object ) const;
	virtual rexSerializer* CreateNew() const  { return new rexSerializerLevelInstance; }
protected:
	rexResult SerializeRecursively( rexObject& object, fiStream* myStream, atArray<atString>& outputPathStack, bool doBangers, int indent = 0 ) const;
};

}	// namespace rage

#endif
