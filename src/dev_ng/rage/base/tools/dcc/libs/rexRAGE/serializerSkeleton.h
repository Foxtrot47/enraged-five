// 
// rexRAGE/serializerSkeleton.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSerializerRAGESkeleton
//			 -- generates .skel files from generic skeleton information
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexSerializer
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_RMCORE_SERIALIZER_GENERICSKELETONTORMCORESKELETON_H__
#define __REX_RMCORE_SERIALIZER_GENERICSKELETONTORMCORESKELETON_H__

#include "rexBase/serializer.h"
#include "rexBase/animExportCtrl.h"
#include "rexGeneric/objectSkeleton.h"
#include "rexBase/property.h"

namespace rage {

class atString;
class fiStream;

/////////////////////////////////////////////////////////////////////////////////////

class rexSerializerRAGESkeleton : public rexSerializer
{
public:
	rexSerializerRAGESkeleton() : rexSerializer()  
	{
		m_IgnoreSkeletonRootXform = false; 
		m_ExportEulers = true; 
		m_WriteLimitAndLockInfo = false; 
		m_ChannelGroupCount = 0; 
		m_bUseAnimCtrlExportFile = false;
		m_bOmitMissingTracks = false;
	}

	virtual rexResult	Serialize( rexObject& object ) const; 

	void				SetIgnoreSkeletonRootXform( bool b )   { m_IgnoreSkeletonRootXform = b; }
	bool				GetIgnoreSkeletonRootXform() const	   { return m_IgnoreSkeletonRootXform; }

	void				SetExportEulers( bool b )   { m_ExportEulers = b; }
	bool				GetExportEulers() const		{ return m_ExportEulers; }

	rexSerializer*		CreateNew() const { return new rexSerializerRAGESkeleton(); }

	void				SetWriteLimitAndLockInfo( bool b )		{ m_WriteLimitAndLockInfo = b; }
	bool				GetWriteLimitAndLockInfo() const		{ return m_WriteLimitAndLockInfo; }

	int					AddChannelGroup()					{ return m_ChannelGroupCount++; }
	void				AddChannelToWrite( const atString& channelInputName, const atString& channelOutputName, int channelID, bool applyOnlyToRootBone, int channelGroupIndex = -1 )  
						{ m_ChannelInputNames.PushAndGrow( channelInputName ); m_ChannelOutputNames.PushAndGrow( channelOutputName ); m_ChannelSupportedForRootBoneOnly.PushAndGrow( applyOnlyToRootBone ); m_ChannelID.PushAndGrow( channelID ); m_ChannelGroup.PushAndGrow( channelGroupIndex ); }

	bool		GetOmitMissingTracks() const { return m_bOmitMissingTracks; }
	void		SetOmitMissingTracks(bool bUse) { m_bOmitMissingTracks = bUse; }
	bool		GetUseAnimCtrlExportFile() const { return m_bUseAnimCtrlExportFile; }
	void		SetUseAnimCtrlExportFile(bool bUse) { m_bUseAnimCtrlExportFile = bUse; }
	bool		LoadAnimExportCtrlFile(const char* filePath)
	{
		return m_AnimExportCtrlSpec.LoadFromXML(filePath);
	}

protected:
	void				Indent( fiStream* s, int numTabs ) const;
	int					SerializeRecursively( const rexObjectGenericSkeleton& skeleton, const rexObjectGenericSkeleton::Bone& currBone, atArray<u16>& boneIDs, atArray<atString>& boneNames, fiStream* s, rexResult& result, int indent, int &boneIndex ) const; // returns boneCount

	int					SerializeUsingControlFile(fiStream* s, int boneIndex, const rexObjectGenericSkeleton::Bone& currBone, rexResult& result) const;
	int					SerializeUsingChannelMap(fiStream* s, int boneIndex,  const rexObjectGenericSkeleton::Bone& currBone, rexResult& result) const;

	bool				m_IgnoreSkeletonRootXform;
	bool				m_ExportEulers;
	bool				m_WriteLimitAndLockInfo;
	atArray<atString>	m_ChannelInputNames, m_ChannelOutputNames;
	atArray<int>		m_ChannelGroup, m_ChannelID;
	int					m_ChannelGroupCount;
	atArray<bool>		m_ChannelSupportedForRootBoneOnly;

	bool				m_bUseAnimCtrlExportFile;
	bool				m_bOmitMissingTracks;
	AnimExportCtrlSpec	m_AnimExportCtrlSpec;
};

/////////////////////////////


class rexPropertySkeletonWriteLimitAndLockInfo : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonWriteLimitAndLockInfo; }
};

/////////////////////////////

class rexPropertySkeletonOmitMissingTracks : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonOmitMissingTracks; }
};

/////////////////////////////
class rexPropertySkeletonExportEulers : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonExportEulers; }
};

/////////////////////////////

class rexPropertyIgnoreSkeletonRootXform : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyIgnoreSkeletonRootXform; }
};
} // namespace rage

/////////////////////////////

#endif
