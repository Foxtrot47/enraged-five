// 
// rexRAGE/serializerLevel.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		AGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSerializerRmworldDistrict
//			 -- generates old rmworld level files
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexSerializerGenericContainer
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

	#ifndef __REX_SERIALIZER_LEVEL_H__
	#define __REX_SERIALIZER_LEVEL_H__

#include "rexGeneric/serializerContainer.h"

namespace rage {

class fiStream;

/////////////////////////////////////////////////////////////////////////////////////

class rexSerializerRmworldDistrict : public rexSerializerGenericContainer
{
public:
	rexSerializerRmworldDistrict();

	virtual rexResult Serialize( rexObject& object ) const;

	virtual rexSerializer* CreateNew() const  { return new rexSerializerRmworldDistrict; }

protected:
	bool			  DistrictExportFinalize( const char *levelName ) const;
	rexResult		  SerializeRecursive( rexObject& object, fiStream* s ) const;

};

/////////////////////////////////////////////////////////////////////////////////////

class rexSerializerRmworldRoomType : public rexSerializer
{
public:
	virtual rexResult	Serialize( rexObject& object ) const; 
	rexSerializer*		CreateNew() const { return new rexSerializerRmworldRoomType; }
};

/////////////////////////////////////////////////////////////////////////////////////

}	// namespace rage

#endif
