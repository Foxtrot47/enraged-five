//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//
#include <file\asset.h>
#include ".\xmlAssetReport.h"
#include ".\rexReport.h"
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>

using namespace rage;

atArray<ConstString>	xmlAssetReport::m_CreatedFiles;
atArray<ConstString>	xmlAssetReport::m_OpenedFiles;
atArray<ConstString>	xmlAssetReport::m_OpenedAsReadOnlyFiles;
__time64_t xmlAssetReport::m_lStartTime;

xmlAssetReport::xmlAssetReport(void)
{
}

xmlAssetReport::~xmlAssetReport(void)
{
}

bool xmlAssetReport::Open()
{
	_time64( &m_lStartTime );
	return true;
}

bool xmlAssetReport::Close(const char* ReportFilename)
{
	// Create file
	ASSET.CreateLeadingPath(ReportFilename);
	// Displayf("xmlAssetReport::Open - About to create report file %s\n", ReportFilename);
	fiStream* fout = fiStream::Create(ReportFilename);
	if(!fout)
	{
		// Unable to create report file
		// I should report an error here, but I'm not sure how yet KR 2/7/05
		// Displayf("xmlAssetReport::Open - Unable to create report file %s, bailing\n", ReportFilename);
		return false;
	}
	// Displayf("xmlAssetReport::Open - Created report file %s successfully\n", ReportFilename);

	// Write header
	OutputString("<?xml version=\"1.0\" standalone=\"yes\"?>\n", fout);
	OutputString("<MC4AssetDataSet>\n", fout);
	OutputString("\t<MayaFiles>\n", fout);
	OutputString("\t\t<MayaFile xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n", fout);	

	struct tm *pobStartTime;
	pobStartTime = _localtime64(&m_lStartTime);
	char acBuffer[255];
	sprintf(acBuffer, "%04d-%02d-%02dT%02d:%02d:%02d.000000-07:00", (pobStartTime->tm_year + 1900), (pobStartTime->tm_mon + 1), pobStartTime->tm_mday, pobStartTime->tm_hour, pobStartTime->tm_min, pobStartTime->tm_sec);
	OutputString("\t\t\t<StartExportTime>", fout); 	OutputString(acBuffer, fout); OutputString("</StartExportTime>\n", fout);

	__time64_t lEndTime;	_time64( &lEndTime );
	struct tm *pobEndTime;
	pobEndTime = _localtime64(&lEndTime);
	sprintf(acBuffer, "%04d-%02d-%02dT%02d:%02d:%02d.000000-07:00", (pobEndTime->tm_year + 1900), (pobEndTime->tm_mon + 1), pobEndTime->tm_mday, pobEndTime->tm_hour, pobEndTime->tm_min, pobEndTime->tm_sec);
	OutputString("\t\t\t<EndExportTime>", fout); 	OutputString(acBuffer, fout); OutputString("</EndExportTime>\n", fout);
	OutputString("\t\t\t<ComputerName>", fout); 	OutputString(getenv("COMPUTERNAME"), fout); OutputString("</ComputerName>\n", fout);
	OutputString("\t\t\t<UserName>", fout); 	OutputString(getenv("USERNAME"), fout); OutputString("</UserName>\n", fout);

	// List created files
	// OutputString("<p><b>Created %d Files</b><br><ol>\n", m_CreatedFiles.GetCount());
	// Displayf("Created %d Files", m_CreatedFiles.GetCount());
	OutputString("\t\t\t<CreatedFiles xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n", fout);
	for(int i=0; i<m_CreatedFiles.GetCount(); i++)
	{
		OutputString("\t\t\t\t<string>", fout);	OutputString(m_CreatedFiles[i], fout);	OutputString("</string>\n", fout);
		// Displayf("%s", m_CreatedFiles[i]);
	}
	OutputString("\t\t\t</CreatedFiles>\n", fout);
	// OutputString("</ol>\n");

	// List opened files
	// Displayf("Opened %d Files", m_OpenedFiles.GetCount());
	OutputString("\t\t\t<OpenedFiles xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n", fout);
	for(int i=0; i<m_OpenedFiles.GetCount(); i++)
	{
		OutputString("\t\t\t\t<string>", fout);	OutputString(m_OpenedFiles[i], fout);	OutputString("</string>\n", fout);
		// Displayf("%s", m_OpenedFiles[i]);
	}
	OutputString("\t\t\t</OpenedFiles>\n", fout);

	// Displayf("OpenedReadOnly %d Files", m_OpenedAsReadOnlyFiles.GetCount());
	OutputString("\t\t\t<OpenedReadOnlyFiles xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n", fout);
	for(int i=0; i<m_OpenedAsReadOnlyFiles.GetCount(); i++)
	{
		OutputString("\t\t\t\t<string>", fout);	OutputString(m_OpenedAsReadOnlyFiles[i], fout);	OutputString("</string>\n", fout);
		// Displayf("%s", m_OpenedAsReadOnlyFiles[i]);
	}
	OutputString("\t\t\t</OpenedReadOnlyFiles>\n", fout);

	// Clear arrays
	m_CreatedFiles.Reset();
	m_OpenedFiles.Reset();
	m_OpenedAsReadOnlyFiles.Reset();

	// Write footer
	OutputString("\t\t</MayaFile>\n", fout);
	OutputString("\t</MayaFiles>\n", fout);
	OutputString("</MC4AssetDataSet>\n", fout);
	fout->Close();

	// Shutdown
	return true;
}

bool xmlAssetReport::Write(const ConstString& strStringToOutput)
{
	return OutputString(strStringToOutput);
}

bool xmlAssetReport::Write(const char *fmt, ...)
{
	va_list args;
	va_start( args, fmt );
	char acBuffer[4096];
	vformatf(acBuffer, 4096, fmt, args);
	ConstString strStringToOutput(acBuffer);
	va_end( args );

	return OutputString(strStringToOutput);
}

bool xmlAssetReport::OutputString(const char* strStringToOutput, fiStream* fout)
{
	// Write out data
	fout->Write(strStringToOutput, (int)strlen(strStringToOutput));
	return true;
}

bool xmlAssetReport::AddToListOfCreatedFiles(const char* pcModifiedFile)
{
	return AddToListOfCreatedFiles(ConstString(pcModifiedFile));
}


bool xmlAssetReport::AddToListOfCreatedFiles(const ConstString& strModifiedFile)
{
	// Is given file already in the list?
	char acTempBuffer[512];
	strcpy(acTempBuffer, strModifiedFile.c_str());
	strlwr(acTempBuffer);

	// Check slashes are standard
	int iModifiedFileLen = (int)strlen(acTempBuffer);
    for(int i=0; i<iModifiedFileLen; i++) if(acTempBuffer[i] == '\\') acTempBuffer[i] = '/';

	ConstString strModifiedFileInLowerCase(acTempBuffer);

	// See if already have the file
	bool bAddMe = true;
	for(int i=0; i<m_CreatedFiles.GetCount(); i++)
	{
		if(m_CreatedFiles[i] == strModifiedFileInLowerCase)
		{
			bAddMe = false;
			break;
		}
	}
	if(bAddMe)
	{
		// Not already in the list of files, so add it
		m_CreatedFiles.PushAndGrow(strModifiedFileInLowerCase);
	}
	return true;
}

bool xmlAssetReport::AddToListOfOpenedFiles(const char* pcModifiedFile, bool bReadOnly)
{
	return AddToListOfOpenedFiles(ConstString(pcModifiedFile), bReadOnly);
}


bool xmlAssetReport::AddToListOfOpenedFiles(const ConstString& strModifiedFile, bool bReadOnly)
{
	// Is given file already in the list?
	char acTempBuffer[512];
	strcpy(acTempBuffer, strModifiedFile.c_str());
	strlwr(acTempBuffer);

	// Check slashes are standard
	int iModifiedFileLen = (int)strlen(acTempBuffer);
	for(int i=0; i<iModifiedFileLen; i++) if(acTempBuffer[i] == '\\') acTempBuffer[i] = '/';

	ConstString strModifiedFileInLowerCase(acTempBuffer);
	
	if(bReadOnly)
	{
		// See if already have the file
		bool bAddMe = true;
		for(int i=0; i<m_OpenedAsReadOnlyFiles.GetCount(); i++)
		{
			if(m_OpenedAsReadOnlyFiles[i] == strModifiedFileInLowerCase)
			{
				bAddMe = false;
				break;
			}
		}
		if(bAddMe)
		{
			// Not already in the list of files, so add it
			m_OpenedAsReadOnlyFiles.PushAndGrow(strModifiedFileInLowerCase);
		}
	}
	else
	{
		// See if already have the file
		bool bAddMe = true;
		for(int i=0; i<m_OpenedFiles.GetCount(); i++)
		{
			if(m_OpenedFiles[i] == strModifiedFileInLowerCase)
			{
				bAddMe = false;
				break;
			}
		}
		if(bAddMe)
		{
			// Not already in the list of files, so add it
			m_OpenedFiles.PushAndGrow(strModifiedFileInLowerCase);
		}
	}
	return true;
}

