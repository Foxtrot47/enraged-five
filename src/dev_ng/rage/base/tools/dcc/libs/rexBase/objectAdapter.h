// 
// rexBase/objectAdapter.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if __TOOL

#ifndef __REXOBJECTADAPTER_H__
#define __REXOBJECTADAPTER_H__

#ifndef ATL_ARRAY_H
#include <atl/array.h>
#endif

#ifndef FILE_HANDLE_H
#include <file/handle.h>
#endif

#include "system/magicnumber.h"

namespace rage {

class rexResult;

///////////////////////////////////////////////////////////////////////////////
// enum rexObjectAdapterTypes
///////////////////////////////////////////////////////////////////////////////

enum rexObjectAdapterTypes
{
	roatBool				= MAKE_MAGIC_NUMBER('B','O','O','L'),
	roatByte				= MAKE_MAGIC_NUMBER('B','Y','T','E'),
	roatShort				= MAKE_MAGIC_NUMBER('S','H','R','_'),
	roatInt					= MAKE_MAGIC_NUMBER('I','N','T','_'),
	roatFloat				= MAKE_MAGIC_NUMBER('F','L','O','_'),
	roatDouble				= MAKE_MAGIC_NUMBER('D','B','L','_'),
	roatVector2				= MAKE_MAGIC_NUMBER('V','E','C','2'),
	roatVector3				= MAKE_MAGIC_NUMBER('V','E','C','3'),
	roatVector4				= MAKE_MAGIC_NUMBER('V','E','C','4'),
	roatMatrix44			= MAKE_MAGIC_NUMBER('M','T','4','4'),
	roatMatrix34			= MAKE_MAGIC_NUMBER('M','T','3','4'),
	roatATString			= MAKE_MAGIC_NUMBER('A','T','S','T'),
	roatATArray				= MAKE_MAGIC_NUMBER('A','T','A','R'),
	roatRexValue			= MAKE_MAGIC_NUMBER('R','V','A','L'),
};

///////////////////////////////////////////////////////////////////////////////
// helper macros
///////////////////////////////////////////////////////////////////////////////

// atArray helpers

#define GET_ATARRAY_REF_ADDR(array) ((array.GetCount()) ? (&((array)[0])) : NULL)

#define GET_ATARRAY_PTR_ADDR(array) ((array->GetCount()) ? (&((array)[0])) : NULL)

#define READ_ATARRAY_REF_HELPER(array, length, addr) \
	{\
	for(int atArrayReadHelperIdx = 0; atArrayReadHelperIdx < (length); atArrayReadHelperIdx++)\
		(array).Grow(256);\
	(addr) = (void*)GET_ATARRAY_REF_ADDR((array));\
	}

#define READ_ATARRAY_PTR_HELPER(array, length, addr) \
	(array)->Reallocate((length));\
	for(int atArrayReadHelperIdx = 0; atArrayReadHelperIdx < (length); atArrayReadHelperIdx++)\
		(array)->Grow(256);\
	(addr) = (void*)GET_ATARRAY_PTR_ADDR((array));

// ReadFunction/WriteFunction helper macros

#define START_IMPL_READ(x) \
	static const s32 magic = (x);\
	if(!obj || !items)\
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);\
	if(!fiIsValidHandle(obj->m_ObjFile))\
		return rexResult(rexResult::ERRORS, rexResult::FILE_NOT_OPEN);\
	s32 filemagic = 0;\
	u32 sizeToRead = 0, numItemsToRead = 0;\
	rexResult status;\
	if( (status = obj->StartReadChunk(filemagic, sizeToRead, numItemsToRead)).Errors() )\
		return status;\
	if(magic != filemagic)\
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);\
	if( 0 == (itemsRead = numItemsToRead) )\
		return rexResult(rexResult::PERFECT);\
	itemsRead = 0;\
	if((u32)numItems < numItemsToRead)\
		return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY); // not really out of memory, but this is the closest error to the condition

#define FINISH_IMPL_READ(a,b) \
	if((a) == (b))\
		return obj->FinishReadChunk();\
	else\
		return rexResult(rexResult::ERRORS);

#define START_IMPL_WRITE(x) \
	static const s32 magic = (x);\
	if(!obj || !items || numItems < 0)\
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);\
	if(!fiIsValidHandle(obj->m_ObjFile))\
		return rexResult(rexResult::ERRORS, rexResult::FILE_NOT_OPEN);\
	rexResult status;\
	if( (status = obj->StartWriteChunk(magic, numItems)).Errors() )\
		return status;\
	if(0 == numItems)\
		return rexResult(rexResult::PERFECT);\
	rexObjectAdapter::ChunkInfo *curChunk = obj->GetCurrentChunkInfo();\
	if(!curChunk)\
		return rexResult(rexResult::ERRORS);\
	u32 totalBytesWritten = 0;

#define FINISH_IMPL_WRITE(a, b) \
	if((a) == (b))\
	{\
		curChunk->size += totalBytesWritten;\
		status = obj->FinishWriteChunk();\
	}\
	else\
	{\
		status.Set(rexResult::ERRORS);\
	}\
	return status;

///////////////////////////////////////////////////////////////////////////////
// class rexObjectAdapter
///////////////////////////////////////////////////////////////////////////////

/*
PURPOSE
	Classes descended from rexObjectAdapter can save themselves to disk in the
	event that memory is required for other tasks.
NOTES
	To see an example implementation of a derived rexObjectAdapter, see the
	roaTest class in the #if 0 block at the bottom of objectAdapter.cpp.
	This class is, as it stands, something of a hack job.  Numerous
	improvements can be made to make it more flexible and error-resistant.
	Ideas:
		* make a ring buffer of adapter objects that allows a set of objects
		  to remain in memory
		* remove read/write static functions, replace with classes that
		  register themselves with their magic numbers
		* need a way to make file opening automatic (necessitates having a
		  copy of the filename in the rexObjectAdapter) - this allows chunks
		  to be used hierarchically; if the file is already open, reads or
		  writes are performed as appropriate
		* remove the difference between Read and Write functions, set global
		  reading/writing mode on object, internal functions to take care of
		  which operation to do
	    * figure out a better way to handle rexArrays 
*/
class rexObjectAdapter : public rexObject
{
// constructor/destructor
public:
	rexObjectAdapter();
	virtual ~rexObjectAdapter();

// type definitions
public:
	// this function will create a new rexObjectAdapter to fill in (this is used by rexObjectAdapterPtr)
	typedef rexObjectAdapter *(*CreatorFunction)(void);
	// a ReadFunction reads a specific type of chunk from the file
	typedef rexResult (*ReadFunction)(rexObjectAdapter* /*obj*/, int& /*itemsRead*/, int /*numItems*/, void* /*items*/);
	// a WriteFunction writes a specific type of chunk to the file
	typedef rexResult (*WriteFunction)(rexObjectAdapter* /*obj*/, int /*numItems*/, const void* /*items*/);

    class ChunkInfo
	{
	public:
		ChunkInfo() { Reset(); }
		ChunkInfo(const ChunkInfo &a) : magic(a.magic), sizeIdx(a.sizeIdx), size(a.size) {}
		~ChunkInfo() {}
		void Reset(void) { magic = NULL; sizeIdx = 0; size = 0; }

		s32 magic;
		u32 sizeIdx;
		u32 size;
	};

// constants
public: // TODO: was protected, look into this
	// NOTE: as a minor optimization, chunks that contain MaxNumItemsInLength
	// or fewer items have their item count stored in the length field itself.
	// the following constants are used to control this behavior.  this impacts
	// the maximum size in bytes of a chunk as follows:
	//
	// MaxSizeOfChunk = ~(NumItemsInLengthMask) - (4 * X)
	//
	// where X is the number of 32-bit fields in the chunk header: 2 fields for
	// chunks containing MaxNumItemsInLength or fewer items, and 3 for all
	// other chunks.
	//
	// NOTE 2: when reading the file, the number of items is not combined into
	// the length field within the current ChunkInfo object.

	enum { MaxNumItemsInLength = 2 };
	enum { NumItemsShift = 30 }; // bit shift to move the number of items to/from the right place
	enum { MaxNumItemsMask = 0x00000003 };
	enum { NumItemsInLengthMask = MaxNumItemsMask << NumItemsShift };

// static data
public: // TODO: was protected, look into this
	static unsigned int fileNum;

// data members
public:
	// NOTE: not storing filename in here because rexObjectAdapterPtr needs it

	atArray<ChunkInfo> m_MagicStack; // chunk info stack
	fiHandle m_ObjFile; // file descriptor
	s32 m_ArrayMagic; // magic number for the contents of the array
	ReadFunction m_ArrayReadFunc; // storage for array reading function
	WriteFunction m_ArrayWriteFunc; // storage for array writing function

// static functions
public: // TODO: was protected, look into this
	static rexResult ReadBool(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteBool(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadByte(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteByte(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadShort(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteShort(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadInt(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteInt(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadFloat(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteFloat(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadDouble(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteDouble(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadVector2(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteVector2(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadVector3(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteVector3(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadVector4(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteVector4(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadMatrix44(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteMatrix44(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadMatrix34(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteMatrix34(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadATString(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteATString(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult ReadRexValue(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);
	static rexResult WriteRexValue(rexObjectAdapter *obj, int numItems, const void *items);

	static rexResult StartReadATArray(rexObjectAdapter *obj, s32 readMagic, ReadFunction readFunc, int &numItems);
	static rexResult ReadATArray(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items);

	static rexResult StartWriteATArray(rexObjectAdapter *obj, s32 writeMagic, WriteFunction writeFunc, int numItems);
	static rexResult WriteATArray(rexObjectAdapter *obj, int numItems, const void *items);

// internal methods
public: // TODO: was protected, look into this
	// chunk info stack manipulation
	ChunkInfo *GetCurrentChunkInfo(void);

	ChunkInfo *PushMagic(s32 newMagic);
	ChunkInfo *PopMagic(u32 &sizeidx, u32 &size);

	bool CurrentMagicMatches(s32 magic);

	// read helpers
	rexResult StartReadChunk(s32 &magic, u32 &size, u32 &numItems);
	rexResult FinishReadChunk(void); // pops top off magic stack

	// write helpers
	rexResult StartWriteChunk(s32 magic, u32 numItems); // pushes new chunk info onto magic stack, saves size location pointer
	rexResult FinishWriteChunk(void); // pops top off magic stack, writes size to file, adds size of child to size of parent

	// i/o helpers
	rexResult ReadBuffer(int bufsize, void *buf, int &bytesRead);
	rexResult WriteBuffer(int bufsize, const void *buf);

// member functions
public: // TODO: was protected, look into this
	rexResult OpenTempFile(atString &filename);
	rexResult CloseTempFile(void);
	rexResult CreateTempFile(atString &filename);
	rexResult DeleteTempFile(atString &filename);

// virtual methods
public:
	virtual rexResult MakeFilename(atString &filename);
	virtual rexResult Save(atString &filename)=0;
	virtual rexResult Load(atString &filename)=0;
}; // class rexObjectAdapter

///////////////////////////////////////////////////////////////////////////////
// class rexObjectAdapterPtr
///////////////////////////////////////////////////////////////////////////////

class rexObjectAdapterPtr : public rexObject
{
// constructor/destructor
private:
	rexObjectAdapterPtr() : rexObject(), m_obj(NULL), m_creator(NULL), m_path() {}
public:
	rexObjectAdapterPtr(rexObjectAdapter *p, rexObjectAdapter::CreatorFunction creator) : rexObject(), m_obj(p), m_creator(creator), m_path() {}
	virtual ~rexObjectAdapterPtr()
	{
		if(m_obj)
		{
			m_obj->CloseTempFile();
			if(deleteTempFiles)
				m_obj->DeleteTempFile(m_path);
			delete m_obj;
		}
	}

// static data
public:
	static bool allowSavingToDisk;
	static bool deleteTempFiles;

// data members
protected:
	rexObjectAdapter *m_obj;
	rexObjectAdapter::CreatorFunction m_creator;
	atString m_path;

// methods
public:
	inline rexObjectAdapter *operator->(void) { return GetObj(); }
	inline rexObjectAdapter *GetObj(void) { if(allowSavingToDisk && !m_obj) Load(); return m_obj; }
	rexResult Save(void);
	rexResult Load(void);
	rexResult Unload(void);

// virtual methods
public:
	virtual rexObject *CreateNew(void) { return NULL; } // this object should never be cloned
}; // class rexObjectAdapterPtr

} // namespace rage

#endif // #ifndef __REXOBJECTADAPTER_H__

#endif // #if __TOOL
