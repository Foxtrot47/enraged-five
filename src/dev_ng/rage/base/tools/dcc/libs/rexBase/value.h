// 
// rexBase/value.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX datBase Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexValue -- class that can represent more than one data type
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_VALUE_H__
#define __REX_BASE_VALUE_H__

#include "result.h"

namespace rage {

class rexValue
{
// subtypes
public:
	enum valueTypes
	{
		UNKNOWN,
		VALUENULL,
		BOOLEAN,
		BYTE,
		SHORT,
		INTEGER,
		FLOAT,
		DOUBLE,
		STRING,
		CONSTSTRING,
		ATSTRING,
	};

// constructor/destructor
public:
	rexValue() : type(UNKNOWN) { }
	rexValue(const bool newBoolean) { SetValue(newBoolean); }
	rexValue(const char newByte) { SetValue(newByte); }
	rexValue(const short newShort) { SetValue(newShort); }
	rexValue(const int newInt) { SetValue(newInt); }
	rexValue(const float newFloat) { SetValue(newFloat); }
	rexValue(const double newDouble) { SetValue(newDouble); }
	rexValue(char *newCharPtr) { SetValue(newCharPtr); }
	rexValue(const char *newConstCharPtr) { SetValue(newConstCharPtr); }
	rexValue(const atString &newATString) { SetValue(newATString); }
	rexValue(const rexValue &that) { SetValue(that); }
	rexValue(valueTypes t)
	{
		type = t;

		switch(t)
		{
		case(UNKNOWN):
		case(VALUENULL):
			break;
		case(BOOLEAN):
			vBoolean = false;
			break;
		case(BYTE):
			vByte = 0;
			break;
		case(SHORT):
			vShort = 0;
			break;
		case(INTEGER):
			vInt = 0;
			break;
		case(FLOAT):
			vFloat = 0.0f;
			break;
		case(DOUBLE):
			vDouble = 0.0;
			break;
		case(STRING):
			vCharPtr = NULL;
			break;
		case(CONSTSTRING):
			vConstCharPtr = NULL;
			break;
		case(ATSTRING):
			vATString.Reset();
			break;
		default:
			type = UNKNOWN;
		};
	}
	~rexValue() {} // no destruction of the contents is performed, the owner of this object is responsible for deleting data...

// member functions
public:
#define RELEASE_ATSTRING if(type == ATSTRING) { vATString.Reset(); }

	void SetValue(const bool newBoolean) { RELEASE_ATSTRING; type = BOOLEAN; vBoolean = newBoolean; }
	void SetValue(const char newByte) { RELEASE_ATSTRING; type = BYTE; vByte = newByte; }
	void SetValue(const short newShort) { RELEASE_ATSTRING; type = SHORT; vShort = newShort; }
	void SetValue(const int newInt) { RELEASE_ATSTRING; type = INTEGER; vInt = newInt; }
	void SetValue(const float newFloat) { RELEASE_ATSTRING; type = FLOAT; vFloat = newFloat; }
	void SetValue(const double newDouble) { RELEASE_ATSTRING; type = DOUBLE; vDouble = newDouble; }
	void SetValue(char *newCharPtr) { RELEASE_ATSTRING; type = STRING; vCharPtr = newCharPtr; }
	void SetValue(const char *newConstCharPtr) { RELEASE_ATSTRING; type = CONSTSTRING; vConstCharPtr = newConstCharPtr; }
	void SetValue(const atString &newATString) { type = ATSTRING; vATString = newATString; }
	void SetValue(const rexValue &that)
	{
		RELEASE_ATSTRING;
		type = that.type;
		switch(type)
		{
		case(UNKNOWN):
		case(VALUENULL):
			break;
		case(BOOLEAN):
			vBoolean = that.vBoolean;
			break;
		case(BYTE):
			vByte = that.vByte;
			break;
		case(SHORT):
			vShort = that.vShort;
			break;
		case(INTEGER):
			vInt = that.vInt;
			break;
		case(FLOAT):
			vFloat = that.vFloat;
			break;
		case(DOUBLE):
			vDouble = that.vDouble;
			break;
		case(STRING):
			vCharPtr = that.vCharPtr;
			break;
		case(CONSTSTRING):
			vConstCharPtr = that.vConstCharPtr;
			break;
		case(ATSTRING):
			vATString = that.vATString;
			break;
		default:
			type = UNKNOWN;
			break;
		}
	}

	valueTypes GetType(void) const { return type; }

	const atString *GetATString(void) const { if(type == ATSTRING) return &vATString; else return NULL; }

	bool isUnknown(void) const { return (UNKNOWN == type); }
	bool isNull(void) const { return (VALUENULL == type); }
	bool isIntegral(void) const { return (BOOLEAN == type || BYTE == type || SHORT == type || INTEGER == type); }
	bool isFloatingPoint(void) const { return (FLOAT == type || DOUBLE == type); }
	bool isNumeric(void) const { return (isIntegral() || isFloatingPoint()); }
	bool isString(void) const { return (STRING == type || CONSTSTRING == type || ATSTRING == type); }

	const char *toString(void) const;
	int toInt( int defaultValue = 0 ) const;
	float toFloat( float defaultValue = 0.0f ) const;
	bool  toBool( bool defaultValue = false ) const;

// static functions
public:
	static const char *typeToString(valueTypes type);

public:
// operators
	operator const char* () const  { return toString(); }
	rexValue &operator=(const rexValue &that) { SetValue(that); return *this; }

// friends
	friend bool operator!=(const rexValue &a, const rexValue &b);
	friend bool operator==(const rexValue &a, const rexValue &b);

// member data
public:
	valueTypes type;
	union
	{
		bool vBoolean;
		char vByte;
		short vShort;
		int vInt;
		float vFloat;
		double vDouble;
		char *vCharPtr;
		const char *vConstCharPtr;
	};
protected:
	atString vATString; // can't put this in the union because it has a copy constructor
};

} // namespace rage

#endif // #ifndef __REX_BASE_VALUE_H__
