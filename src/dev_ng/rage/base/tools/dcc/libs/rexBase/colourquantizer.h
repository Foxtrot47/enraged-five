
#ifndef __COLOUR_QUANTIZER_H__
#define __COLOUR_QUANTIZER_H__

#include "vector/vector3.h"
#include "vector/vector4.h"

#include "atl/array.h"

#define TREEDEPTH 8
#define COLOURBITS 8

namespace rage {

class ColourQuantizer {

public:
	ColourQuantizer() : m_RootNode(NULL), m_LeafLevel(TREEDEPTH), m_TotalLeaves(0) {
		for( int i=0; i<TREEDEPTH; i++ )
		{
			m_Reducelist[i] = NULL;
		}
	}

	struct ColourRGB
	{
		ColourRGB()
		{
			r = 0;
			g = 0;
			b = 0;
		}
		ColourRGB(unsigned long red, unsigned long green, unsigned long blue) 
		{
			this->r = red;
			this->g = green;
			this->b = blue;
		}

		bool operator==(const ColourRGB& colourRGB) const
		{
			return r == colourRGB.r && g == colourRGB.g && b == colourRGB.b;
		}
		unsigned long r;
		unsigned long g;
		unsigned long b;
	};

	struct OctreeNode
	{
		OctreeNode() : col(0, 0, 0), pixelCount(0), index(0), isLeaf(false) {
			for (int i=0; i<COLOURBITS; i++)
				children[i] = NULL;
		}

		~OctreeNode() {
			for (int i=0; i<COLOURBITS; i++)
				if (children[i])
					delete children[i];
		}

		int Insert( Vector3& rgb );

		ColourRGB col;
		bool isLeaf;
		int level;
		unsigned long pixelCount;
		long index;

		struct OctreeNode* children[8];
		OctreeNode* nextNode;
	};

	

	OctreeNode*			CreateOctreeNode( int level );
	void				InsertOctreeNode( OctreeNode** node, ColourRGB& rgb, int depth );

	void				MakeReducible(int level, OctreeNode *node);
	OctreeNode*			GetReducible();
	void				BuildOctree( atArray<ColourQuantizer::ColourRGB, 0, u32>& vertColList );
	void				ReduceTree();
	void				ReduceTreeToSpecifiedLeafCount( u32 leafNodes );
	void				MakePaletteTable(OctreeNode* octreeNode, atArray<ColourRGB>& palette, int* index);
	OctreeNode*			GetRootNode() { return m_RootNode; }
	int					QuantizeColor(OctreeNode *octreeNode, ColourRGB& colour);

private:
	u32					m_TotalLeaves;
	OctreeNode*	m_RootNode;
	OctreeNode*	m_Reducelist[TREEDEPTH];
	unsigned int					m_LeafLevel;
};

}

#endif 