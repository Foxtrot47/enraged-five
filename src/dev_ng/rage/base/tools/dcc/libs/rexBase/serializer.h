// 
// rexBase/serializer.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX datBase Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSerializer -- abstract class with common interface for object serialization
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexLogAndErrorGenerator
//
//		PUBLIC INTERFACE:
//				[Query]
//					virtual bool					IsAcceptableInputObject( const rexObject& /*object*/ ) const = 0;
//
//				[Primary Functionality]
//					virtual bool	Serialize( rexObject* inputObject, const atArray<rexSerializer*>& serializers ) const; 
//						* returns false if an error occurred.  check RetrieveErrorMessages() for details.
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_SERIALIZER_H__
#define __REX_BASE_SERIALIZER_H__

#include "logAndErrorGenerator.h"
#include "result.h"
#include "atl/array.h"
#include "atl/string.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObject;
class rexShell;

/////////////////////////////////////////////////////////////////////////////////////

class rexSerializer : public rexLogAndErrorGenerator
{
public:
	enum EnumUnitTypeLinear
	{
		NONE=-1,
		MILLIMETERS,
		CENTIMETERS,
		METERS,
		INCHES,
		FEET,
		YARDS,
		EVIL_MAGIC_PONG_UNITS
	};
	enum {NUM_UNIT_TYPE_LINEARS=EVIL_MAGIC_PONG_UNITS+1};

	rexSerializer() : rexLogAndErrorGenerator()  
	{ 
		m_ExportData = true; 
		m_UseBoneIDs = true; 
		m_ProgressBarObjectCountCB = m_ProgressBarObjectCurrentIndexCB = NULL; 
		m_ProgressBarTextCB = NULL; 
		m_UseBoundFiltering = false; 
		m_BoundFilterTypes.clear();
	}
	
	virtual rexResult		Serialize( rexObject& object ) const = 0;
	virtual rexSerializer*	CreateNew() const=0;

	void SetOutputPath( const atString& path )  { m_OutputPath = path; }
	void SetExportData( bool b )				{ m_ExportData = b; }
	void SetUseBoneIDs( bool b )				{ m_UseBoneIDs = b; }

	void	SetUseBoundFiltering( bool b )						{ m_UseBoundFiltering = b; }
	bool	GetUseBoundFiltering() const						{ return m_UseBoundFiltering; }

	void	AddBoundFilterType(const char *  str);

	const atString& GetOutputPath() const;
	bool GetExportData() const					{ return m_ExportData; }
	bool GetUseBoneIDs() const					{ return m_UseBoneIDs; }

	virtual void			RegisterProgressBarObjectCountCallback( rexProgressBarCallback cb )			{ m_ProgressBarObjectCountCB = cb; }
	virtual void			RegisterProgressBarObjectCurrentIndexCallback( rexProgressBarCallback cb )	{ m_ProgressBarObjectCurrentIndexCB = cb; }
	virtual void			RegisterProgressBarTextCallback( rexProgressBarTextCallback cb )			{ m_ProgressBarTextCB = cb; }

	// unit type information
	
	static float	GetUnitTypeLinearConversionFactor();
	
	static atString	GetUnitTypeToConvertTo();
	static void		SetUnitTypeToConvertTo(const char * str);

	// sets linear units of our generic classes
	static void		SetBaseUnits(EnumUnitTypeLinear unitType) {sm_BaseUnits=unitType;}

protected:
	atString			m_OutputPath;
	bool				m_ExportData;
	bool				m_UseBoneIDs;

	bool	m_UseBoundFiltering;
	atStringArray m_BoundFilterTypes;

	rexProgressBarCallback		m_ProgressBarObjectCountCB, m_ProgressBarObjectCurrentIndexCB;
	rexProgressBarTextCallback	m_ProgressBarTextCB;

	static EnumUnitTypeLinear	sm_UnitTypeLinearTo;
	static EnumUnitTypeLinear	sm_BaseUnits;
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
