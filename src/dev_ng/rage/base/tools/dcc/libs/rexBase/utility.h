// 
// rexBase/utility.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX datBase Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//
/////////////////////////////////////////////////////////////////////////////////////
//  NAMESPACES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexUtility -- namespace for storing all generic utility functions
//						   
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_UTILITY_H__
#define __REX_BASE_UTILITY_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/mgc.h"
#include "rexBase/result.h"

#include "atl/array.h"
#include "vector/vector3.h"
#include "vector/matrix34.h"
#include <limits.h>
#include "mesh/mesh.h"
#include "rexBase/colourquantizer.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

typedef rexResult (*rexUtilityTextureExportFunction)( const char * inputFileName, const char * outputFileName, bool hasAlpha, bool isPS2Lightmap );
typedef rexResult (*rexUtilityTextureExportWantedFunction)( const char * inputFileName, const char *outputFileName );

namespace rexUtility
{
	void Startup();
	void Shutdown();

	atString	FilenameFromPath( const char * path );
	atString	BaseFilenameFromPath( const char * path );  // w/o extension
	atString	ExtensionFromPath( const char * path );
	atString	PathFromPath( const char * path );
	atString	ToLowerCase( const char * path );

	int			FileNameCompare( const atString& file1, const atString& file2 );

	bool		CreatePath( const char * pathname );
	bool		FileExists( const char * filename );

	void		PopPath( atString& path );

	void		ConvertBackslashesToSlashes( const char *og, atString& dest );

	void		FileCopy( const char * src, const char *dest );

	atString    RemoveNonAlphabeticCharacters( const char * s );
	bool		ContainsNonAlphabeticCharacters( const char * s );
	atString	RemoveWhitespaceCharacters( const char * s );

	template< typename T > bool		MakeOptimizedBoundingBox( T& vertArray, Matrix34& bbMatrix, float& bbLength, float& bbHeight, float& bbWidth );
	template< typename T > bool		MakeOptimizedBoundingSphere( T& vertArray, Vector3& center, float& radius );
	bool		MakeOptimizedBoundingSphereFromBBox( const Vector3& min, const Vector3& max, Vector3& center, float& radius );

	rexResult	ExportTexture( const char * inputFileName, const char * outputFileName, bool hasAlpha, bool isPS2Lightmap, const char * uiHint ); 
	void		SetExportTextureFunction( rexUtilityTextureExportFunction func );
    void        SetTextureExportWantedFunction( rexUtilityTextureExportWantedFunction function);
	void		SetTextureListFileName(const char* name);
	void		SetTextureConvertFlag(bool f);
	bool		IsTextureConvertDisabled();		

	void		SetHackTimeStampsOfGeneratedTextures(bool f);
	bool		HackTimeStampsOfGeneratedTextures();

	// return number of seconds since jan 1, 1901
	// only valid until Feb 28, 2100
	double		GetTimestamp();
	double		GetFileTimestamp( const char * filename );

	int			GetChecksum( const char * string, int numCycles = 1 );

	// Functions to make continuous change.
	void		FindCloseAngle(const Vector3& iR, Vector3 &ioR, const char *order);
	void		FindAlternate(Vector3& iR, Vector3& oR, const char *order);
	float 		BoundAngle(float r);

	// functions for dealing with devil (the 3rd party image library that we use):
	rexResult LoadDevilImage(const char* fileName);
	rexResult ConvertAndSaveDevilImage(const char* fileName,bool hasAlpha);

	rexResult GenerateColourPaletteFromArray( atArray<Vector4, 0, u32>& vertColList, atArray<ColourQuantizer::ColourRGB>& colourPalette, atArray<int, 0, u32>& palIdxList, u32 maxPaletteSize );
	rexResult GenerateImageFromColourPalette( u32 maxPaletteSize, u32 minWidthPixels, const char* paletteFile, atArray<ColourQuantizer::ColourRGB> colourPalette );

	void BeginDevil();
	void EndDevil();


	/*	PURPOSE
	take a single mesh and create an additional mesh for every blend target
	*/
	bool SplitMeshWithBlends( atArray<mshMesh>& r_outputmeshes, mshMesh* p_InputMesh, bool preserveDeltas = false );
};

template<typename T> bool rexUtility::MakeOptimizedBoundingSphere( T& vertArray, Vector3& center, float& radius )
{
	int count = vertArray.GetCount();
	if( count < 1 )
		return false;

	Mgc::Vector3 *pPoints = new Mgc::Vector3[count];

	Vector3 min, max;
	min.Set(FLT_MAX);
	max.Set(-FLT_MAX);

	// Copy the vertArray array into pPoints.
	for (int i = 0; i < count; i++)
	{
		Vector3 v;
		v.Set(vertArray[i]);
		pPoints[i].x = v.x;
		pPoints[i].y = v.y;
		pPoints[i].z = v.z;

		// Record the would-be AABB for comparison purposes.
		min.x = Min(min.x, v.x);
		min.y = Min(min.y, v.y);
		min.z = Min(min.z, v.z);
		max.x = Max(max.x, v.x);
		max.y = Max(max.y, v.y);
		max.z = Max(max.z, v.z);
	}

	Mgc::PointArray points(count, pPoints);


	// Find the best-fit sphere.
	Mgc::Sphere sphere = Mgc::MinSphere(points.m_iQuantity, points.m_akPoint);

	delete[] pPoints;

	// Store the center and radius.
	center.Set((float)sphere.Center().x, (float)sphere.Center().y, (float)sphere.Center().z);
	radius = (float)sphere.Radius();

	return true;
}

template<typename T> bool rexUtility::MakeOptimizedBoundingBox( T& vertArray, Matrix34& bbMatrix, float& bbLength, float& bbHeight, float& bbWidth )
{
	int count = vertArray.GetCount();

	Mgc::Vector3 *pPoints = new Mgc::Vector3[count];

	// Copy the vertArray array into pPoints.
	for (int i = 0; i < count; i++)
	{
		Vector3 v;
		v.Set(vertArray[i]);
		pPoints[i].x = v.x;
		pPoints[i].y = v.y;
		pPoints[i].z = v.z;
	}

	Mgc::PointArray points(count, pPoints);

	// Find the best-fit OOBB.
	Mgc::Box3 box = Mgc::MinBox(points.m_iQuantity, points.m_akPoint);
	delete[] pPoints;

	bbMatrix.a.x = (float)box.Axis(0).x;
	bbMatrix.a.y = (float)box.Axis(0).y;
	bbMatrix.a.z = (float)box.Axis(0).z;
	bbMatrix.b.x = (float)box.Axis(1).x;
	bbMatrix.b.y = (float)box.Axis(1).y;
	bbMatrix.b.z = (float)box.Axis(1).z;
	bbMatrix.c.x = (float)box.Axis(2).x;
	bbMatrix.c.y = (float)box.Axis(2).y;
	bbMatrix.c.z = (float)box.Axis(2).z;
	bbMatrix.d.x = (float)box.Center().x;
	bbMatrix.d.y = (float)box.Center().y;
	bbMatrix.d.z = (float)box.Center().z;

	bbLength = (float)box.Extent(0) * 2.0f;
	bbHeight = (float)box.Extent(1) * 2.0f;
	bbWidth = (float)box.Extent(2) * 2.0f;

	return true;
}

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
