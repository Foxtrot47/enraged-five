// 
// rexBase/animExportCtrl.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "animExportCtrl.h"
#include "animExportCtrl_parser.h"

using namespace std;


//-----------------------------------------------------------------------------

namespace rage 
{

bool AnimExportCtrlSpec::sm_ParsableClassesRegistered = false;

bool AnimExportCtrlSpec::LoadFromXML(const char* filePath)
{
	bool bResult = true;

	fiStream * streamCtrl = fiStream::Open(filePath);
	if (streamCtrl)
	{
		Reset();
		
		if(!PARSER.LoadObject(streamCtrl, *this))
			bResult = false;
	
		streamCtrl->Close();
	}
	else
		bResult = false;

	return bResult;
}

//-----------------------------------------------------------------------------

bool AnimExportCtrlSpec::SaveToXML(const char* filePath) const
{
	bool bResult = false;
	fiStream * streamCtrl = fiStream::Create(filePath);
	if (streamCtrl)
	{
		bResult = PARSER.SaveObject(streamCtrl, this);
		streamCtrl->Close();
	}
	return bResult;
}

//-----------------------------------------------------------------------------

const AnimExportCtrlTrackSpec* AnimExportCtrlSpec::FindTrackSpec(const char* searchName) const
{
	int trackSpecCount = m_TrackSpecs.GetCount();
	for(int i=0; i<trackSpecCount; i++)
	{
		if(StrWildCmp(m_TrackSpecs[i].GetNameExpr(), searchName))
			return &m_TrackSpecs[i];
	}
	return NULL;
}

//-----------------------------------------------------------------------------

int AnimExportCtrlSpec::StrWildCmp(const char *wild, const char *string) const
{
	const char *cp = NULL, *mp = NULL;
	
	if( stricmp( wild, string ) == 0 )
		return 1;
	while ((*string) && (*wild != '*')) 
	{
		if ((*wild != *string) && (*wild != '?')) 
		{
		return 0;
		}
		wild++;
		string++;
	}

	while (*string) 
	{
		if (*wild == '*') 
		{
		if (!*++wild) 
		{
			return 1;
		}
		mp = wild;
		cp = string+1;
		} 
		else if ((*wild == *string) || (*wild == '?')) 
		{
		wild++;
		string++;
		} 
		else 
		{
		wild = mp;
		string = cp++;
		}
	}

	while (*wild == '*') 
	{
		wild++;
	}
	return !*wild;
}

//-----------------------------------------------------------------------------

void AnimExportCtrlTrack::BuildComponentOutputName(int cIdx, atString& compOutputName) const
{
	compOutputName = (const char*)m_outputName; 
	if(cIdx<m_splitComponents.GetCount())
		compOutputName += (const char*)m_splitComponents[cIdx];
}
	
//-----------------------------------------------------------------------------

void AnimExportCtrlTrack::PostLoad()
{
	SplitComponentString();
}

const char*	AnimExportCtrlTrack::GetComponentPart(u32 idx) const 
{
	const char* comp = (idx>=(u32)m_splitComponents.GetCount())?"":m_splitComponents[idx];
	return comp;
}
//-----------------------------------------------------------------------------

void AnimExportCtrlTrack::SplitComponentString()
{
	m_splitComponents.clear();

	if(!m_components)
		return;

	unsigned int cmpStrLen = (unsigned int)strlen((const char*)m_components);
	if(!cmpStrLen)
		return;

	atString cmpName;
	for(unsigned int i=0; i<cmpStrLen; i++)
	{
		if(m_components[i] == ' ')
			continue;
		else if(m_components[i] == ',') 
		{
			m_splitComponents.PushAndGrow(ConstString(cmpName.c_str()));
			cmpName.Reset();
		}
		else
		{
			cmpName += m_components[i];
		}
	}

	m_splitComponents.PushAndGrow(ConstString(cmpName.c_str()));
}

//-----------------------------------------------------------------------------

}//End namespace rage

