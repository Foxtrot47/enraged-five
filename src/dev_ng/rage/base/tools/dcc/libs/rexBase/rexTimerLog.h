// 
// rexBase/rexTimerLog.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#pragma once
#include "atl/string.h"
#include "atl/array.h"
#include "system/timer.h"
#include "file/stream.h"

//=============================================================================
// rexTimerLog
// PURPOSE
//   This calls provides some simple functions for generating timing information
//   during export.
//
//	To use this class first call:
//
//		rexTimerLog::Open(<filename of xml file to write times too>);
//
//	Then at any point after that in the plugin call:
//
//		rexTimerLog::StartTimer(<string - Unique name to identify the timer>)
//
//	to start a timer and :
//
//		rexTimerLog::EndTimer(<string - Unique name to identify the timer>)
//
//	to end it!
//
//	And then at the end of all the timings call rexTimerLog::Close() to write 
//	out all the times.
//	Unless a path is provided to Open, then the timer does nothing and it is safe
//	to call it any and everywhere.  This provides a simple way to globally disable
//	or enable the timers.
//
namespace rage {
class rexTimerLog
{
public:
	rexTimerLog(void);
	~rexTimerLog(void);

	// Open and closing
	static bool Open(const char * LogFilename);
	static bool Close();

	// Outputing
	static bool StartTimer(const char * TimerName);
	static bool EndTimer(const char * TimerName);

	// To see if the timer log is enabled or not
	//	Unless a path is provided to Open, then the timer does nothing and it is safe
	//	to call it any and everywhere.  This provides a simple way to globally disable
	//	or enable the timers.
	static	bool	IsTimerLogEnabled() {return (m_LogFilename != "");}

private:

	// Create a local class for storing active timers in
	class rexTimerLogActiveTimer
	{
	public:
		rexTimerLogActiveTimer(const char * TimerName) 
			{m_TimerName = TimerName; m_SysTimer.Reset(); m_ChildTimerInfo = "";}
		const atString& GetName() const {return m_TimerName;}
		atString GetXMLSafeName() const;
		const sysTimer& GetTimer() const {return m_SysTimer;}
		const atString& GetChildTimerInfo() const {return m_ChildTimerInfo;}
		atString& GetChildTimerInfo() {return m_ChildTimerInfo;}

	private:
		sysTimer	m_SysTimer;
		atString	m_TimerName;
		atString	m_ChildTimerInfo;
	};

	// Output string
	static bool OutputString(const char * strStringToOutput, fiStream* fout = NULL);

	// Filename info
	static atString m_LogFilename;

	// Timers
	static atArray<rexTimerLogActiveTimer* >	m_ActiveTimers;

	// Info for the header
	static atString m_StartTimeAsString;

	// To get the name of the scene being exported
	static  atString	GetSceneName();
};

} // namespace rage
