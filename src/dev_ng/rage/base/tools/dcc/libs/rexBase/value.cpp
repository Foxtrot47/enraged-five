
#include "atl/string.h"
#include "value.h"
#include <stdlib.h>

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

// static
const char *rexValue::typeToString(rexValue::valueTypes type)
{
	switch(type)
	{
	case(rexValue::UNKNOWN):
		return "Unknown";
		break;
	case(rexValue::VALUENULL):
		return "Null";
		break;
	case(rexValue::BOOLEAN):
		return "Boolean";
		break;
	case(rexValue::BYTE):
		return "Byte";
		break;
	case(rexValue::SHORT):
		return "Short";
		break;
	case(rexValue::INTEGER):
		return "Integer";
		break;
	case(rexValue::FLOAT):
		return "Float";
		break;
	case(rexValue::DOUBLE):
		return "Double";
		break;
	case(rexValue::STRING):
		return "String";
		break;
	case(rexValue::CONSTSTRING):
		return "ConstString";
		break;
	case(rexValue::ATSTRING):
		return "atString";
		break;
	default:
		return "type not supported";
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////////

bool operator!=(const rexValue &a, const rexValue &b)
{
	// if types aren't equal, values aren't equal
	// (don't worry about type coercion for now)
	if(a.type != b.type)
		return true;

	bool retval;

	switch(a.type)
	{
	case(rexValue::UNKNOWN):
	case(rexValue::VALUENULL):
		retval = false;
		break;
	case(rexValue::BOOLEAN):
		retval = a.vBoolean != b.vBoolean;
		break;
	case(rexValue::BYTE):
		retval = a.vByte != b.vByte;
		break;
	case(rexValue::SHORT):
		retval = a.vShort != b.vShort;
		break;
	case(rexValue::INTEGER):
		retval = a.vInt != b.vInt;
		break;
	case(rexValue::FLOAT):
		retval = a.vFloat != b.vFloat;
		break;
	case(rexValue::DOUBLE):
		retval = a.vDouble != b.vDouble;
		break;
	case(rexValue::STRING):
		// do crappy comparison for now...
		retval = a.vCharPtr != b.vCharPtr;
		break;
	case(rexValue::CONSTSTRING):
		// do crappy comparison for now...
		retval = a.vConstCharPtr != b.vConstCharPtr;
		break;
	case(rexValue::ATSTRING):
		retval = a.vATString != b.vATString;
		break;
	default:
		retval = true;
	};

	return retval;
}

bool operator==(const rexValue &a, const rexValue &b)
{
	return !(a!=b);
}

/*
			switch(val.type)
			{
			case(rexValue::UNKNOWN):
				printf("Unknown\n");
				break;
			case(rexValue::VALUENULL):
				printf("Null\n");
				break;
			case(rexValue::BOOLEAN):
				printf("%s\n", (val.vBoolean) ? "true" : "false");
				break;
			case(rexValue::BYTE):
				printf("%d\n", val.vByte);
				break;
			case(rexValue::SHORT):
				printf("%d\n", val.vShort);
				break;
			case(rexValue::INTEGER):
				printf("%d\n", val.vInt);
				break;
			case(rexValue::FLOAT):
				printf("%f\n", val.vFloat);
				break;
			case(rexValue::DOUBLE):
				printf("%f\n", val.vDouble);
				break;
			case(rexValue::STRING):
				printf("\"%s\"\n", val.vCharPtr);
				break;
			case(rexValue::CONSTSTRING):
				printf("\"%s\"\n", val.vConstCharPtr);
				break;
			case(rexValue::ATSTRING):
				printf("\"%s\"\n", (const char*)(*val.GetATString()));
				break;
			default:
				printf("type not supported\n");
				break;
			}
*/

///////////////////////////////////////////////////

const char *rexValue::toString(void) const
{
	static char buffer[256];  // it's either this or making sure the caller frees a dynamically allocated one.
	switch( type )
	{
		case BOOLEAN:
		{
			sprintf( buffer, vBoolean ? "True" : "False" );
			return buffer;
			break;
		}
		case BYTE:
		{
			sprintf( buffer, "%c", (char)vByte );
			return buffer;
			break;
		}
		case SHORT:
		{
			sprintf( buffer, "%d", vShort );
			return buffer;
			break;
		}
		case INTEGER:
		{
			sprintf( buffer, "%d", vInt );
			return buffer;
			break;
		}
		case FLOAT:
		{
			sprintf( buffer, "%f", vFloat );
			return buffer;
			break;
		}
		case DOUBLE:
		{
			sprintf( buffer, "%f", vDouble );
			return buffer;
			break;
		}
		case STRING:
		{
			return vCharPtr;
			break;
		}
		case CONSTSTRING:
		{
			return vConstCharPtr;
			break;
		}
		case ATSTRING:
		{
			return (const char*) vATString;
			break;
		}
		default:
		{
			return NULL;
		}
	}
}

///////////////////////////////////////////////////

int rexValue::toInt(int defaultValue) const
{
	int retval = defaultValue;

	switch(type)
	{
	case(UNKNOWN):
	case(VALUENULL):
		break;
	case(BOOLEAN):
		retval = (int)vBoolean;
		break;
	case(BYTE):
		retval = (int)vByte;
		break;
	case(SHORT):
		retval = (int)vShort;
		break;
	case(INTEGER):
		retval = vInt;
		break;
	case(FLOAT):
		retval = (int)vFloat;
		break;
	case(DOUBLE):
		retval = (int)vDouble;
		break;
	case(STRING): 
		retval = atoi( vCharPtr );
		break;
	case(CONSTSTRING): 
		retval = atoi( vConstCharPtr );
		break;
	case(ATSTRING): 
		retval = atoi( vATString );
		break;
	default:
		break;
	}

	return retval;
}

///////////////////////////////////////////////////

float rexValue::toFloat( float defaultValue ) const
{
	float retval = defaultValue;

	switch(type)
	{
	case(UNKNOWN):
	case(VALUENULL):
		break;
	case(BOOLEAN):
		retval = (float)vBoolean;
		break;
	case(BYTE):
		retval = (float)vByte;
		break;
	case(SHORT):
		retval = (float)vShort;
		break;
	case(INTEGER):
		retval = (float)vInt;
		break;
	case(FLOAT):
		retval = vFloat;
		break;
	case(DOUBLE):
		retval = (float)vDouble;
		break;
	case(STRING): 
		retval = (float)atof( vCharPtr );
		break;
	case(CONSTSTRING): 
		retval = (float)atof( vConstCharPtr );
		break;
	case(ATSTRING): 
		retval = (float)atof( vATString );
		break;
	default:
		break;
	}

	return retval;
}

///////////////////////////////////////////////////

bool rexValue::toBool( bool defaultValue ) const
{
	bool retval = defaultValue;

	switch(type)
	{
	case(UNKNOWN):
	case(VALUENULL):
		break;
	case(BOOLEAN):
		retval = vBoolean;
		break;
	case(BYTE):
		retval = ( vByte != 0 );
		break;
	case(SHORT):
		retval = ( vShort != 0 );
		break;
	case(INTEGER):
		retval = ( vInt != 0 );
		break;
	case(FLOAT):
		retval = ( vFloat != 0 );
		break;
	case(DOUBLE):
		retval = ( vDouble != 0 );
		break;
	case(STRING): 
		if( vCharPtr[ 0 ] == 'T' || vCharPtr[ 0 ] == 't' || vCharPtr[ 0 ] == '1' )
			retval = true;
		else if( vCharPtr[ 0 ] == 'F' || vCharPtr[ 0 ] == 'f' || vCharPtr[ 0 ] == '0' )
			retval = false;
		break;
	case(CONSTSTRING):
		if( vConstCharPtr[ 0 ] == 'T' || vConstCharPtr[ 0 ] == 't' || vConstCharPtr[ 0 ] == '1' )
			retval = true;
		else if( vConstCharPtr[ 0 ] == 'F' || vConstCharPtr[ 0 ] == 'f' || vConstCharPtr[ 0 ] == '0' )
			retval = false;
		break;
	case(ATSTRING):
		if( !vATString.GetLength() )
			retval = defaultValue;
		else if( vATString[ 0 ] == 'T' || vATString[ 0 ] == 't' || vATString[ 0 ] == '1' )
			retval = true;
		else if( vATString[ 0 ] == 'F' || vATString[ 0 ] == 'f' || vATString[ 0 ] == '0' )
			retval = false;
		break;
	default:
		break;
	}

	return retval;
}

} // namespace rage

///////////////////////////////////////////////////
