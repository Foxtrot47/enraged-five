// 
// rexBase/shell.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//	rexShell -	this is a generic mechanism for registering modules and properties,
//				and for dispatching objects to these modules for processing.  please
//				refer to rex/rex.cpp for usage example.  
//				this is an abstract class and must be derived for real-world usage.
//				(see rexMaya/shell.h for maya version of derived shell)
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_SHELL_H__
#define __REX_BASE_SHELL_H__

/////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "atl/array.h"
#include "atl/map.h"
#include "atl/string.h"

#include "rexBase/module.h"
#include "rexBase/result.h"
#include "rexBase/serializer.h"
#include "rexBase/value.h"
#include "rexBase/rexReport.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObject;
class rexFilter;
class rexConverter;
class rexSkeleton;
class rexProperty;

// [CHRISK]
class rexHamsterObject;

/////////////////////////////////////////////////////////////////////////////////////////////////////////

// [CHRISK]
void rexSetCurrentHamsterObject(rexHamsterObject* ho);

rexHamsterObject* rexGetCurrentHamsterObject();

class rexShell 
{
public:
	rexShell()
	{ 
		m_ModuleGroupCount = 0; 
		m_ModuleGroupCurrent = -1; 
		s_DontSerialise = false;
	}
	virtual ~rexShell();

	/*  Performs export based on registered modules/properties and scene data
		PARAMS: inputObjects - Root scene objects to process from (if empty, use entire scene)
		RETURNS: status
		NOTES:	inputObjects argument is used for selective export. 
				Selective export is not fully supported at this time.
	*/
	rexResult			Export( const atArray<rexObject*>& inputObjects, bool justExportMesh, bool justExportBound, const char* strPathToJustExportTo );

	/*  Adds to the module filters for export (will create a new filter if none exists)
		PARAMS: filter			- Name of the filter to create (MUST BE SAME NAME AS A MODULE)
				filteredModule	- The module to be filtered OUT.
	*/
	void AddToModuleFilter(const char * filter, const char * filteredModule);

	/* Registers modules for use by exporter

		NOTES:    Modules are combination of rexFilter, rexConverter, and/or rexSerializer which handle export of a 
				  particular object type.  An ID string is used to allow reference by scene Export Data (via node name)
				  to specify which object type a particular sub-scene will be exported as.

		PARAMS: id			   - string used by export data to refer to this module.
				filter		   - accepts or rejects objects for export by this module.
				converter	   - produces generic objects (i.e. mesh) from filtered input objects (i.e. DAG node)
				serializer     - writes generic object data in a specific file format (i.e. RAGE mesh format)
				respectsClaims - specifies whether module should export same input objects as another module within group
								 (this prevents same mesh from being exported as drawable and physics bound)

		SEE ALSO: rexModule, rexFilter, rexConverter, rexSerializer
	*/
	rexResult RegisterModule( const char * id, rexFilter& filter, rexConverter& converter, rexProcessor& processor, rexSerializer& serializer, bool respectsClaims = true, int numObjectsAllowed =  rexModule::INFINITE_OBJECTS_ALLOWED)			
	{ 
		atArray<rexProcessor*> p; 
		p.PushAndGrow( &processor ); 
		return RegisterModuleBase( id, &filter, &converter, &p, &serializer, respectsClaims, numObjectsAllowed  ); 
	}
	rexResult RegisterModule( const char * id, rexFilter& filter, rexConverter& converter, atArray<rexProcessor*>& processors, rexSerializer& serializer, bool respectsClaims,int numObjectsAllowed =  rexModule::INFINITE_OBJECTS_ALLOWED )	
	{ 
		return RegisterModuleBase( id, &filter, &converter, &processors, &serializer, respectsClaims, numObjectsAllowed  );
	}
	rexResult RegisterModule( const char * id, rexFilter& filter, rexConverter& converter, rexSerializer& serializer, bool respectsClaims,int numObjectsAllowed =  rexModule::INFINITE_OBJECTS_ALLOWED )										
	{ 
		return RegisterModuleBase( id, &filter, &converter, NULL, &serializer, respectsClaims, numObjectsAllowed  ); 
	}
	rexResult RegisterModule( const char * id, rexFilter& filter, rexConverter& converter, bool respectsClaims,int numObjectsAllowed =  rexModule::INFINITE_OBJECTS_ALLOWED )																
	{ 
		return RegisterModuleBase( id, &filter, &converter, NULL, NULL, respectsClaims, numObjectsAllowed ); 
	}
	rexResult RegisterModule( const char * id, rexFilter& filter, rexSerializer& serializer, bool respectsClaims,int numObjectsAllowed =  rexModule::INFINITE_OBJECTS_ALLOWED )																
	{ 
		return RegisterModuleBase( id, &filter, NULL, NULL, &serializer, respectsClaims, numObjectsAllowed ); 
	}
	rexResult RegisterModule( const char * id, rexConverter& converter, rexSerializer& serializer, bool respectsClaims,int numObjectsAllowed =  rexModule::INFINITE_OBJECTS_ALLOWED )														
	{ 
		return RegisterModuleBase( id, NULL, &converter, NULL, &serializer, respectsClaims, numObjectsAllowed ); 
	}
	rexResult RegisterModule( const char * id, rexConverter& converter, bool respectsClaims,int numObjectsAllowed =  rexModule::INFINITE_OBJECTS_ALLOWED )																					
	{ 
		return RegisterModuleBase( id, NULL, &converter, NULL, NULL, respectsClaims, numObjectsAllowed ); 
	}
	rexResult RegisterModule( const char * id, rexSerializer& serializer, bool respectsClaims,int numObjectsAllowed =  rexModule::INFINITE_OBJECTS_ALLOWED)		
	{ 
		return RegisterModuleBase( id, NULL, NULL, NULL, &serializer, respectsClaims, numObjectsAllowed ); 
	}
	rexResult RegisterModule( const char * id, bool respectsClaims,int numObjectsAllowed =  rexModule::INFINITE_OBJECTS_ALLOWED)																											
	{ 
		return RegisterModuleBase( id, NULL, NULL, NULL, NULL, respectsClaims, numObjectsAllowed ); 
	}

	rexModule*			GetModule( const char * id ) const;
	
	/* Registers properties for use by exporter

		NOTES:    Properties are used to modify shell and module behaviors, or to modify object data.
				  An ID string is used to allow reference by scene Export Data (via attribute name)
				  to specify which property function to call.  Property functions also get passed
				  the value and node connections the attribute may have.

		PARAMS: id			   - string used by export data to refer to this property.
				property	   - object that contains function(s) to call when property encountered.

		SEE ALSO: rexProperty
	*/
	rexResult			RegisterProperty( const char * id ) // for registering externally handled attributes
							{ return RegisterPropertyBase( id, NULL ); }
	
	rexResult			RegisterProperty( const char * id, const rexProperty& prop )
							{ return RegisterPropertyBase( id, &prop ); }

	static bool			DoesntSerialise(){return s_DontSerialise;}
	static void			DontSerialise(bool DontDo){s_DontSerialise = DontDo;}
	static void			SetTempExportPath(const char *tempPath)
	{
		s_TempExportPath = tempPath;
	}
	static atString		&GetTempExportPath()
	{
		return s_TempExportPath;
	}

	static rexReport&	Report(){return s_RexReport;}

	virtual	void		ExternalLogMessage(const char* /*msg*/, const char* /*context*/=NULL, const char* /*filecontext*/=NULL){}
	virtual	void		ExternalLogWarning(const char* /*msg*/, const char* /*context*/=NULL, const char* /*filecontext*/=NULL){}
	virtual	void		ExternalLogError(const char* /*msg*/, const char* /*context*/=NULL, const char* /*filecontext*/=NULL){}
	virtual	void		ExternalProfileStart(const char* /*context*/=NULL){}
	virtual	void		ExternalProfileEnd(const char* /*context*/=NULL){}

	// the shell iterator is a simple virtual interface for traversing source data
	class Iterator    
	{
	public:
		virtual ~Iterator() {}

		/* Gets next object to process

			NOTES:  Properties are used to modify shell and module behaviors, or to modify object data.
					An ID string is used to allow reference by scene Export Data (via attribute name)
					to specify which property function to call.  Property functions also get passed
					the value and node connections the attribute may have.

			RETURNS: rexObject containing all information necessary to process data.

			PARAMS:  depthDelta - is set to number of levels difference between this node and previous node 
								  in scene hierarchy (i.e. going from a node to its grandparent would set
								  this variable to -2).

			SEE ALSO: rexObject
		*/
		virtual rexObject*	GetNextObject( int& depthDelta )=0;

		/* Gets number of objects to be processed
			RETURNS: number of objects to be processed
		*/
		virtual int			GetObjectCount() const =0;
	};

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // this class contains information for objects to be converted.  in Maya, these are specified by the 
    // RexExportData hierarchy
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    class ExportItemInfo
    {
    public:
        ExportItemInfo();
        ~ExportItemInfo();		

        // functions for reclaiming memory
        bool CleanUpPostFiltration();
        bool CleanUpPostConversion();
        bool CleanUpPostSerialization();

        // this data is populated by GetExportItemInfo()
        atString						m_ModuleID;
        atString						m_ObjectNameDefault;
        atString						m_strBaseRuleFile;		// This string only exists for debug purposes, it contains the name of the rule file used for this item
        atString						m_strMaxOrMayaIdentityString;		// This string only exists for debug purposes, it contains a unique name that can be used by the user to identify this item in Max or Maya

        // this allows for selective export to disable certain modules
        bool							m_ModuleActive;

        // properties' data retrieved from scene Export Data nodes
        atArray<atString>				m_PropertyIDs;		// matches registered
        atArray<atString>				m_PropertyStrings;	// can be unique
        atArray<rexValue>				m_PropertyValues;
        atArray<bool>					m_PropertyLocked;
        atArray<atArray<rexObject*> >	m_PropertyConnectedObjects;

        // child items
        atArray<ExportItemInfo>			m_Children;

        // this data is populated by SetupModuleInstances()
        rexModule*						m_ModuleInstance;   

        // this data is populated by DispatchInputObject() (which adds object to this list if filter accepts object)
        atArray<rexObject*>				m_FilteredObjectList;

        // this data is populated by ConvertObjects()		 (which uses module converter to create)
        rexObject*						m_ConvertedObject;
    };	

protected:

	// this is called by the public RegisterModule functions to allow for more convenient interface
	rexResult			RegisterModuleBase( const char * id, rexFilter* filter, rexConverter* converter, atArray<rexProcessor*>* processors, rexSerializer* serializer, bool respectsClaims, int numOb );
	rexResult			RegisterPropertyBase( const char * id, const rexProperty* property );

	
	/* Exports one item		
		PARAMS: exportInfoRoot - root item to be exported
		RETURNS: status
		SEE ALSO: ExportItemInfo
	*/
	rexResult ExportItem( ExportItemInfo& exportInfoRoot );
	
	// these functions are called by ExportItem()

	/* sets up shell and instantiates modules for use in exporting item
		PARAMS: exportInfoRoot - root item to be exported
			    parentModule   - module that owns this module (may be NULL)
		RETURNS: status
		SEE ALSO: ExportItemInfo, rexModule
	*/
	rexResult SetupShellAndModuleInstances( ExportItemInfo& exportInfo, const ExportItemInfo& obTopMostParentExportItem, rexModule* parentModule = NULL);

	/* applies filter and compiles list of objects to be processed by module
		PARAMS: exportInfoRoot - root item to be exported
		RETURNS: status
		SEE ALSO: ExportItemInfo, rexObject, rexModule, rexFilter
	*/
	rexResult CompileInputObjectLists( ExportItemInfo& exportInfo );

	/* sets up module groups node claims
		PARAMS: exportInfoRoot - root item to be exported
				groupMaps      - set of hash tables to be used to look up claims while traversing scene data
		RETURNS: status
		SEE ALSO: ExportItemInfo, rexModule
	*/
	rexResult CompileGroupInputObjectLists( ExportItemInfo& exportInfo, atArray< atMap< atString, rexModule* > >& groupMaps );
	
	/* routes scene object to appropriate module(s)
		PARAMS: exportInfoRoot		- root item to be exported
				object				- scene object to be dispatched
				groupsCurrentModule - array of modules used to determine which grouped module to send object to
		RETURNS: status
		SEE ALSO: ExportItemInfo, rexObject, rexModule
	*/
	rexResult DispatchInputObject( ExportItemInfo& exportInfo, rexObject* object, atArray< rexModule* >& groupsCurrentModule );

	/* sends scene objects in input object list to converter for processing into generic data types
		PARAMS: exportInfoRoot		- root item to be exported
				parentObject		- scene object to be dispatched (may be NULL)
		RETURNS: status
		SEE ALSO: ExportItemInfo, rexObject, rexConverter
	*/
	rexResult ConvertObjects( ExportItemInfo& exportInfo, rexObject* parentObject = NULL );

	/* applies appropriate properties to converted objects 
		PARAMS: exportInfoRoot		- root item to be exported
		RETURNS: status
		SEE ALSO: ExportItemInfo, rexObject
	*/
	rexResult ApplyObjectModifications( ExportItemInfo& exportInfo );

	/* sends converted objects along to processors
	PARAMS: exportInfoRoot		- root item to be exported
	RETURNS: status
	SEE ALSO: ExportItemInfo, rexObject
	*/
	rexResult ProcessObjects( ExportItemInfo& exportInfo );

	/* applies appropriate properties to converted objects 
	PARAMS: exportInfoRoot		- root item to be exported
	RETURNS: status
	SEE ALSO: ExportItemInfo, rexObject
	*/
	rexResult ApplyProcessedObjectModifications( ExportItemInfo& exportInfo );

	/* sends processed objects to module's serializer for writing to disk in appropriate format
		PARAMS: exportInfoRoot		- root item to be exported
		RETURNS: status
		SEE ALSO: ExportItemInfo, rexObject
	*/
	rexResult SerializeObjects( ExportItemInfo& exportInfo );

	/* applies appropriate properties to converted objects 
	PARAMS: exportInfoRoot		- root item to be exported
	RETURNS: status
	SEE ALSO: ExportItemInfo, rexObject
	*/
	rexResult VerifyObjects( ExportItemInfo& exportInfo );

	virtual bool shouldAddForExportProcessing(rexObject* /*rexObject*/) { return true; }

	// progress bar hooks
	void SetProgressBarItemCountCB( int count ) { UpdateProgressWindowMax( 2, count ); }
	void SetProgressBarCurrValueCB( int value ) { UpdateProgressWindowValue( 2, value ); }

	// hash tables for string to module/property lookup
	atMap<atString, rexModule*>				m_ModuleMap;	
	atMap<atString, rexProperty*>			m_PropertyMap;

	int										m_ModuleGroupCount, m_ModuleGroupCurrent;
	atArray<bool>							m_ModuleGroupRespectsClaims;

	atArray<rexObject*>						m_MasterNodeList;
	static rexReport						s_RexReport;

	static bool								s_DontSerialise;
	static atString							s_TempExportPath;

	class ModuleFilter
	{
	public:
		ModuleFilter(){};
		~ModuleFilter(){};

		void Add(int GroupID);
		bool IsFiltered(int GroupID);

	private:
		atArray<int> m_GroupIDs;
	};

	atMap<u32, ModuleFilter>					m_ModuleFilters;

	bool IsNotFilteredModuleGroup(rexModule* current, atArray< rexModule* >& groupsCurrentModule);

	/////////////////////////////////////////////////////////////////////////////////////////////////
	// these functions are called by base shell functionality, and must be defined by derived shell 
	// (i.e. rexShellMaya).
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	/* gathers export item information, based on input objects.  this allows for selective export.  
	   if input objects array is empty, it is assumed every item in the scene should be exported.  
    */
	virtual void		Reset()  {}

	
	/* 
	Fakes up an entity to allow the export of meshes when no entity really exists

	RETURNS: status
	*/
	rexResult	FakeAnEntity(const atArray<rexObject*>& inputObjects, bool justExportMesh, bool justExportBound, const char* strPathToJustExportTo, atArray<ExportItemInfo>& ExportItems) const;

	/* gathers export item information, based on input objects.  this allows for selective export.  
	   if input objects array is empty, it is assumed every item in the scene should be exported.  
		
		PARAMS: inputObjects		- scene objects to use as a basis for determining which items to gather
									  (if empty, all valid items are gathered)
				exportItems			- array to be populated by function.  after function called, contains all 
									  items to be exported.
		RETURNS: status
		SEE ALSO: ExportItemInfo, rexObject
	*/
	virtual rexResult	GetExportItemInfo( const atArray<rexObject*>& inputObjects, atArray<ExportItemInfo>& exportItems ) = 0;

	/* creates iterator which is used to retrieve scene objects
		PARAMS: exportItem - item to gather objects for
		RETURNS: pointer initialized iterator, ready to be queried.
		SEE ALSO: Iterator, ExportItemInfo
	*/
	virtual Iterator*	GetIterator( ExportItemInfo& exportItem )=0;

	/* returns unique string ID to be used as hash lookup when referring to passed object.
		PARAMS: obj - object to create string ID for
		RETURNS: unique string ID for object
		SEE ALSO: rexObject
	*/
	virtual atString	GetInputObjectHashString( rexObject& obj )=0;

	/* determines whether object is member of (or descendant of member of) input objects list
		PARAMS: obj				- object to verify membership of
			    inputObjects	- list of member objects
				includeChildren - if set, function returns true if object is descendant of a member object.
								  otherwise, function returns true only if object is a member object.
		RETURNS: true if object is a member object (or is descendant of a member object when includeChildren flag set)
		SEE ALSO: rexObject
	*/
	virtual bool		IsObjectPartOfInput( const rexObject& obj, const atArray<rexObject*>& inputObjects, bool includeChildren ) const=0;

	/* copies input objects list from one module to another
		PARAMS: dest			- module to copy input objects to
			    source			- module to copy input objects from

		NOTES:  this is done here so that it is not necessary to implement a copy function for every object type
		
		SEE ALSO: rexModule
	*/
	virtual void		CopyModuleInputObjects( rexModule& dest, const rexModule& source )=0;

	/* creates and displays a progress window with the specified title and number of progress bars
		PARAMS: title			- caption of progress window
			    numValues		- number of progress bars to display in window
		RETURNS: true if successful.  returns false if progress window already open.
		SEE ALSO: CloseProgressWindow, UpdateProgressWindowInfo, UpdateProgressWindowMax, UpdateProgressWindowValue
	*/
	virtual bool		OpenProgressWindow(const char *title, int numValues)=0;
	
	/* closes progress window previously created via OpenProgressWindow() call
		RETURNS: true if successful.  return falses if no window currently open
		SEE ALSO: OpenProgressWindow, UpdateProgressWindowInfo, UpdateProgressWindowMax, UpdateProgressWindowValue
	*/
	virtual bool		CloseProgressWindow(void)=0;

	/* sets progress bar's text display
		PARAMS: index			- progress bar to modify
			    info			- text to display
		RETURNS: true if successful.  returns false if no window currently open or if bar index out of range.
		SEE ALSO: OpenProgressWindow, CloseProgressWindow, UpdateProgressWindowMax, UpdateProgressWindowValue
	*/
	virtual bool		UpdateProgressWindowInfo(int index, const char *info)=0;

	/* sets progress bar's maximum value
		PARAMS: index			- progress bar to modify
			    max				- value to set as bar's maximum
		RETURNS: true if successful.  returns false if no window currently open or if bar index out of range.
		SEE ALSO: OpenProgressWindow, CloseProgressWindow, UpdateProgressWindowInfo, UpdateProgressWindowValue
	*/
	virtual bool		UpdateProgressWindowMax(int index, int max)=0;

	/* sets progress bar's current value
		PARAMS: index			- progress bar to modify
			    value			- value to set as bar's value
		RETURNS: true if successful.  returns false if no window currently open or if bar index out of range.
		SEE ALSO: OpenProgressWindow, CloseProgressWindow, UpdateProgressWindowInfo, UpdateProgressWindowMax
	*/
	virtual bool		UpdateProgressWindowValue(int index, int value)=0;

	// allow derived shells to do specific setup and cleanup
	virtual rexResult	PreExport()		{ return rexResult::PERFECT; }
	virtual rexResult	PostExport()	{ return rexResult::PERFECT; }

	virtual void		SetupExportItems(const atArray<ExportItemInfo>& /*items*/)		{}
	virtual void		CleanupExportItems(const atArray<ExportItemInfo>& /*items*/)	{}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// these currently return functions to be passed along to rexConverter and rexSerializer.  i want to get
	/// rid of these and just have the shell pass a callback function to these items, which would then directly
	/// call the functions above.
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	virtual				rexProgressBarCallback GetProgressBarCurrValueCB()=0;
	virtual				rexProgressBarCallback GetProgressBarItemCountCB()=0;
	virtual				rexProgressBarTextCallback GetProgressBarInfoCB()=0;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
