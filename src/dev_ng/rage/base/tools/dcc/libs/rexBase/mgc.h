//
// rexBase/mgc.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef __REX_BASE_MGC_H__
#define __REX_BASE_MGC_H__

#include "math/amath.h"
#include "math/random.h"
#include <string.h>

#define MAGICFM

// MaxSDK defines these, so clean it up.
#undef DEG_TO_RAD
#undef RAD_TO_DEG

namespace rage {
namespace Mgc {
	typedef double Real;


	// ==================================================================
	// BBOX STUFF....

	class MAGICFM Math
	{
	public:
		// Return -1 if the input is negative, 0 if the input is zero, and +1
		// if the input is positive.
		static int Sign (int iValue);
		static Real Sign (Real fValue);

		// Just computes fValue*fValue.
		static Real Sqr (Real fValue);

		// Generate a random number in [0,1).  The random number generator may
		// be seeded by a first call to UnitRandom with a positive seed.
		static Real UnitRandom (Real fSeed = 0.0f);

		// Generate a random number in [-1,1).  The random number generator may
		// be seeded by a first call to SymmetricRandom with a positive seed.
		static Real SymmetricRandom (Real fSeed = 0.0f);

		// Generate a random number in [min,max).  The random number generator may
		// be seeded by a first call to IntervalRandom with a positive seed.
		static Real IntervalRandom (Real fMin, Real fMax, Real fSeed = 0.0f);

		// Fast evaluation of sin(angle) by polynomial approximations.  The angle
		// must be in [0,pi/2].  The maximum absolute error is about 1.7e-04 for
		// FastSin0 and about 2.3e-09 for FastSin1.  The speed up is about 2 for
		// FastSin0 and about 1.5 for FastSin1.
		static Real FastSin0 (Real fAngle);
		static Real FastSin1 (Real fAngle);

		// Fast evaluation of cos(angle) by polynomial approximations.  The angle
		// must be in [0,pi/2].  The maximum absolute error is about 1.2e-03 for
		// FastCos0 and about 2.3e-09 for FastCos1.  The speed up is about 2 for
		// FastCos0 and about 1.5 for FastCos1.
		static Real FastCos0 (Real fAngle);
		static Real FastCos1 (Real fAngle);

		// Fast evaluation of tan(angle) by polynomial approximations.  The angle
		// must be in [0,pi/4].  The maximum absolute error is about 8.1e-04 for
		// FastTan0 and about 1.9e-08 for FastTan1.  The speed up is about 2.5 for
		// FastTan0 and about 1.75 for FastTan1.
		static Real FastTan0 (Real fAngle);
		static Real FastTan1 (Real fAngle);

		// Fast evaluation of asin(value) by a sqrt times a polynomial.  The value
		// must be in [0,1].  The maximum absolute error is about 6.8e-05 and the
		// speed up is about 2.5.
		static Real FastInvSin (Real fValue);

		// Fast evaluation of acos(value) by a sqrt times a polynomial.  The value
		// must be in [0,1].  The maximum absolute error is about 6.8e-05 and the
		// speed up is about 2.5.
		static Real FastInvCos (Real fValue);

		// Fast evaluation of atan(value) by polynomial approximations.  The value
		// must be in [-1,1].  The maximum absolute error is about 1.2e-05 for
		// FastInvTan0 and about 1.43-08 for FastInvTan1.  The speed up is about
		// 2.2 for FastInvTan0 and about 1.5 for FastInvTan1.
		static Real FastInvTan0 (Real fValue);
		static Real FastInvTan1 (Real fValue);

		// Wrappers for acos and asin, but the input value is clamped to [-1,1]
		// to avoid silent returns of NaN.
		static Real ACos (Real fValue);
		static Real ASin (Real fValue);

		// Wrapper for 1/sqrt to allow a fast implementation to replace it at a
		// later date.
		static Real InvSqrt (Real fValue);

		// Wrappers to hide 'double foo(double)' versus 'float foof(float)'.
		static Real ATan (Real fValue);
		static Real ATan2 (Real fY, Real fX);
		static Real Ceil (Real fValue);
		static Real Cos (Real fValue);
		static Real Exp (Real fValue);
		static Real FAbs (Real fValue);
		static Real Floor (Real fValue);
		static Real Log (Real fValue);
		static Real Pow (Real kBase, Real kExponent);
		static Real Sin (Real fValue);
		static Real Sqrt (Real fValue);
		static Real Tan (Real fValue);

		// common constants
		static const Real MAX_REAL;
		static const Real PI_;  // KPM for some reason won't compile as "PI"
		static const Real TWO_PI;
		static const Real HALF_PI;
		static const Real INV_TWO_PI;
		static const Real DEG_TO_RAD;
		static const Real RAD_TO_DEG;
	};

	//----------------------------------------------------------------------------
	inline Real Math::ACos (Real fValue)
	{
		if ( -1.0f < fValue )
		{
			if ( fValue < 1.0f )
				return (Real)acos(fValue);
			else
				return 0.0f;
		}
		else
		{
			return PI;
		}
	}
	//----------------------------------------------------------------------------
	inline Real Math::ASin (Real fValue)
	{
		if ( -1.0f < fValue )
		{
			if ( fValue < 1.0f )
				return (Real)asin(fValue);
			else
				return -HALF_PI;
		}
		else
		{
			return HALF_PI;
		}
	}
	//----------------------------------------------------------------------------
	inline Real Math::ATan (Real fValue)
	{
		return (Real)atan(fValue);
	}
	//----------------------------------------------------------------------------
	inline Real Math::ATan2 (Real fY, Real fX)
	{
		return (Real)atan2(fY,fX);
	}
	//----------------------------------------------------------------------------
	inline Real Math::Ceil (Real fValue)
	{
		return (Real)ceil(fValue);
	}
	//----------------------------------------------------------------------------
	inline Real Math::Cos (Real fValue)
	{
		return (Real)cos(fValue);
	}
	//----------------------------------------------------------------------------
	inline Real Math::Exp (Real fValue)
	{
		return (Real)exp(fValue);
	}
	//----------------------------------------------------------------------------
	inline Real Math::FAbs (Real fValue)
	{
		return (Real)fabs(fValue);
	}
	//----------------------------------------------------------------------------
	inline Real Math::Floor (Real fValue)
	{
		return (Real)floor(fValue);
	}
	//----------------------------------------------------------------------------
	inline Real Math::InvSqrt (Real fValue)
	{
		return (Real)(1.0f/sqrt(fValue));
	}
	//----------------------------------------------------------------------------
	inline Real Math::Log (Real fValue)
	{
		return (Real)log(fValue);
	}
	//----------------------------------------------------------------------------
	inline Real Math::Pow (Real kBase, Real kExponent)
	{
		return (Real)pow(kBase,kExponent);
	}
	//----------------------------------------------------------------------------
	inline int Math::Sign (int iValue)
	{
		if ( iValue > 0 )
			return 1;

		if ( iValue < 0 )
			return -1;

		return 0;
	}
	//----------------------------------------------------------------------------
	inline Real Math::Sign (Real fValue)
	{
		if ( fValue > 0.0f )
			return 1.0f;

		if ( fValue < 0.0f )
			return -1.0f;

		return 0.0f;
	}
	//----------------------------------------------------------------------------
	inline Real Math::Sin (Real fValue)
	{
		return (Real)sin(fValue);
	}
	//----------------------------------------------------------------------------
	inline Real Math::Sqr (Real fValue)
	{
		return fValue*fValue;
	}
	//----------------------------------------------------------------------------
	inline Real Math::Sqrt (Real fValue)
	{
		return (Real)sqrt(fValue);
	}
	//----------------------------------------------------------------------------
	inline Real Math::Tan (Real fValue)
	{
		return (Real)tan(fValue);
	}
	//----------------------------------------------------------------------------
	//----------------------------------------------------------------------------

	// ==================================================================


	class MAGICFM Minimize1D
	{
	public:
		typedef Real (*Function)(Real,void*);

		Minimize1D (Function oF, int iMaxLevel, int iMaxBracket,
			void* pvUserData = 0);

		int& MaxLevel ();
		int& MaxBracket ();
		void*& UserData ();

		void GetMinimum (Real fT0, Real fT1, Real fTInitial,
			Real& rfTMin, Real& rfFMin);

	protected:
		Function m_oF;
		int m_iMaxLevel, m_iMaxBracket;
		Real m_fTMin, m_fFMin;
		void* m_pvUserData;

		void GetMinimum (Real fT0, Real fF0, Real fT1, Real fF1, int iLevel);

		void GetMinimum (Real fT0, Real fF0, Real fTm, Real fFm, Real fT1,
			Real fF1, int iLevel);

		void GetBracketedMinimum (Real fT0, Real fF0, Real fTm,
			Real fFm, Real fT1, Real fF1, int iLevel);
	};

	class MAGICFM MinimizeND
	{
	public:
		typedef Real (*Function)(const Real*,void*);

		MinimizeND (int iDimensions, Function oF, int iMaxLevel, int iMaxBracket,
			int iMaxIterations, void* pvUserData = 0);

		~MinimizeND ();

		int& MaxLevel ();
		int& MaxBracket ();
		void*& UserData ();

		// find minimum on Cartesian-product domain
		void GetMinimum (const Real* afT0, const Real* afT1,
			const Real* afTInitial, Real* afTMin, Real& rfFMin);

	protected:
		int m_iDimensions;
		Function m_oF;
		int m_iMaxIterations;
		void* m_pvUserData;
		Minimize1D m_kMinimizer;
		Real* m_afDirectionStorage;
		Real** m_aafDirection;
		Real* m_afDConj;
		Real* m_afDCurr;
		Real* m_afTSave;
		Real* m_afTCurr;
		Real m_fFCurr;
		Real* m_afLineArg;

		void ComputeDomain (const Real* afT0, const Real* afT1,
			Real& rfL0, Real& rfL1);

		static Real LineFunction (Real fT, void* pvUserData);
	};
	

	// ==================================================================

	class MAGICFM Vector3 
	{
	public:
		// construction
		Vector3 ();
		Vector3 (Real fX, Real fY, Real fZ);
		Vector3 (Real afCoordinate[3]);
		Vector3 (const Vector3& rkVector);

		// coordinates
		Real x, y, z;

		// access vector V as V[0] = V.x, V[1] = V.y, V[2] = V.z
		//
		// WARNING.  These member functions rely on
		// (1) Vector3 not having virtual functions
		// (2) the data packed in a 3*sizeof(Real) memory block
		Real& operator[] (int i) const;
		operator Real* ();

		// assignment
		Vector3& operator= (const Vector3& rkVector);

		// comparison (supports fuzzy arithmetic when FUZZ > 0)
		bool operator== (const Vector3& rkVector) const;
		bool operator!= (const Vector3& rkVector) const;
		bool operator<  (const Vector3& rkVector) const;
		bool operator<= (const Vector3& rkVector) const;
		bool operator>  (const Vector3& rkVector) const;
		bool operator>= (const Vector3& rkVector) const;

		// arithmetic operations
		Vector3 operator+ (const Vector3& rkVector) const;
		Vector3 operator- (const Vector3& rkVector) const;
		Vector3 operator* (Real fScalar) const;
		Vector3 operator/ (Real fScalar) const;
		Vector3 operator- () const;
		MAGICFM friend Vector3 operator* (Real fScalar, const Vector3& rkVector);

		// arithmetic updates
		Vector3& operator+= (const Vector3& rkVector);
		Vector3& operator-= (const Vector3& rkVector);
		Vector3& operator*= (Real fScalar);
		Vector3& operator/= (Real fScalar);

		// vector operations
		Real Length () const;
		Real SquaredLength () const;
		Real Dot (const Vector3& rkVector) const;
		Real DistSquared (const Vector3& v) const
		{
			Vector3 v2( *this );
			v2 -= v;
			return v2.SquaredLength();
		}
		Real Unitize (Real fTolerance = 1e-06f);
		Vector3 Cross (const Vector3& rkVector) const;
		Vector3 UnitCross (const Vector3& rkVector) const;

		// Gram-Schmidt orthonormalization.
		static void Orthonormalize (Vector3 akVector[/*3*/]);

		// Input W must be initialize to a nonzero vector, output is {U,V,W}
		// an orthonormal basis.  A hint is provided about whether or not W
		// is already unit length.
		static void GenerateOrthonormalBasis (Vector3& rkU, Vector3& rkV,
			Vector3& rkW, bool bUnitLengthW = true);

		// special points
		static const Vector3 ZERO;
		static const Vector3 UNIT_X;
		static const Vector3 UNIT_Y;
		static const Vector3 UNIT_Z;

		// fuzzy arithmetic (set FUZZ > 0 to enable)
		static Real FUZZ;
	};

	//----------------------------------------------------------------------------
	inline Real& Vector3::operator[] (int i) const
	{
		return ((Real*)this)[i];
	}
	//----------------------------------------------------------------------------
	inline Vector3::operator Real* ()
	{
		return (Real*)this;
	}
	//----------------------------------------------------------------------------

	class MAGICFM Box3
	{
	public:
		Box3 ();

		Vector3& Center ();
		const Vector3& Center () const;

		Vector3& Axis (int i);
		const Vector3& Axis (int i) const;
		Vector3* Axes ();
		const Vector3* Axes () const;

		Real& Extent (int i);
		const Real& Extent (int i) const;
		Real* Extents ();
		const Real* Extents () const;

		void ComputeVertices (Vector3 akVertex[8]) const;

	protected:
		Vector3 m_kCenter;
		Vector3 m_akAxis[3];
		Real m_afExtent[3];
	};

	//----------------------------------------------------------------------------
	inline Box3::Box3 ()
	{
		// no initialization for efficiency
	}
	//----------------------------------------------------------------------------
	inline Vector3& Box3::Center ()
	{
		return m_kCenter;
	}
	//----------------------------------------------------------------------------
	inline const Vector3& Box3::Center () const
	{
		return m_kCenter;
	}
	//----------------------------------------------------------------------------
	inline Vector3& Box3::Axis (int i)
	{
		Assert( 0 <= i && i < 3 );
		return m_akAxis[i];
	}
	//----------------------------------------------------------------------------
	inline const Vector3& Box3::Axis (int i) const
	{
		Assert( 0 <= i && i < 3 );
		return m_akAxis[i];
	}
	//----------------------------------------------------------------------------
	inline Vector3* Box3::Axes ()
	{
		return m_akAxis;
	}
	//----------------------------------------------------------------------------
	inline const Vector3* Box3::Axes () const
	{
		return m_akAxis;
	}
	//----------------------------------------------------------------------------
	inline Real& Box3::Extent (int i)
	{
		Assert( 0 <= i && i < 3 );
		return m_afExtent[i];
	}
	//----------------------------------------------------------------------------
	inline const Real& Box3::Extent (int i) const
	{
		Assert( 0 <= i && i < 3 );
		return m_afExtent[i];
	}
	//----------------------------------------------------------------------------
	inline Real* Box3::Extents ()
	{
		return m_afExtent;
	}
	//----------------------------------------------------------------------------
	inline const Real* Box3::Extents () const
	{
		return m_afExtent;
	}
	//----------------------------------------------------------------------------


	class PointArray
	{
	public:
		PointArray (int iQuantity, const Vector3* akPoint)
			:
		m_akPoint(akPoint)
		{
			m_iQuantity = iQuantity;
		}

		int m_iQuantity;
		const Vector3* m_akPoint;
	};

	MAGICFM Box3 MinBox (int iQuantity, const Vector3* akPoint);

	class MAGICFM Matrix3
	{
	public:
		// construction
		Matrix3 ();
		Matrix3 (const Real aafEntry[3][3]);
		Matrix3 (const Matrix3& rkMatrix);
		Matrix3 (Real fEntry00, Real fEntry01, Real fEntry02,
			Real fEntry10, Real fEntry11, Real fEntry12,
			Real fEntry20, Real fEntry21, Real fEntry22);

		// member access, allows use of construct mat[r][c]
		Real* operator[] (int iRow) const;
		operator Real* ();
		Vector3 GetColumn (int iCol) const;

		// assignment and comparison
		Matrix3& operator= (const Matrix3& rkMatrix);
		bool operator== (const Matrix3& rkMatrix) const;
		bool operator!= (const Matrix3& rkMatrix) const;

		// arithmetic operations
		Matrix3 operator+ (const Matrix3& rkMatrix) const;
		Matrix3 operator- (const Matrix3& rkMatrix) const;
		Matrix3 operator* (const Matrix3& rkMatrix) const;
		Matrix3 operator- () const;

		// matrix * vector [3x3 * 3x1 = 3x1]
		Vector3 operator* (const Vector3& rkVector) const;

		// vector * matrix [1x3 * 3x3 = 1x3]
		MAGICFM friend Vector3 operator* (const Vector3& rkVector,
			const Matrix3& rkMatrix);

		// matrix * scalar
		Matrix3 operator* (Real fScalar) const;

		// scalar * matrix
		MAGICFM friend Matrix3 operator* (Real fScalar, const Matrix3& rkMatrix);

		// M0.TransposeTimes(M1) = M0^t*M1 where M0^t is the transpose of M0
		Matrix3 TransposeTimes (const Matrix3& rkM) const;

		// M0.TimesTranspose(M1) = M0*M1^t where M1^t is the transpose of M1
		Matrix3 TimesTranspose (const Matrix3& rkM) const;

		// utilities
		Matrix3 Transpose () const;
		bool Inverse (Matrix3& rkInverse, Real fTolerance = 1e-06f) const;
		Matrix3 Inverse (Real fTolerance = 1e-06f) const;
		Real Determinant () const;

		// SLERP (spherical linear interpolation) without quaternions.  Computes
		// R(t) = R0*(Transpose(R0)*R1)^t.  If Q is a rotation matrix with
		// unit-length axis U and angle A, then Q^t is a rotation matrix with
		// unit-length axis U and rotation angle t*A.
		static Matrix3 Slerp (Real fT, const Matrix3& rkR0, const Matrix3& rkR1);

		// singular value decomposition
		void SingularValueDecomposition (Matrix3& rkL, Vector3& rkS,
			Matrix3& rkR) const;
		void SingularValueComposition (const Matrix3& rkL,
			const Vector3& rkS, const Matrix3& rkR);

		// Gram-Schmidt orthonormalization (applied to columns of rotation matrix)
		void Orthonormalize ();

		// orthogonal Q, diagonal D, upper triangular U stored as (u01,u02,u12)
		void QDUDecomposition (Matrix3& rkQ, Vector3& rkD, Vector3& rkU) const;

		Real SpectralNorm () const;

		// matrix must be orthonormal
		void ToAxisAngle (Vector3& rkAxis, Real& rfRadians) const;
		void FromAxisAngle (const Vector3& rkAxis, Real fRadians);

		// The matrix must be orthonormal.  The decomposition is yaw*pitch*roll
		// where yaw is rotation about the Up vector, pitch is rotation about the
		// Right axis, and roll is rotation about the Direction axis.
		bool ToEulerAnglesXYZ (Real& rfYAngle, Real& rfPAngle,
			Real& rfRAngle) const;
		bool ToEulerAnglesXZY (Real& rfYAngle, Real& rfPAngle,
			Real& rfRAngle) const;
		bool ToEulerAnglesYXZ (Real& rfYAngle, Real& rfPAngle,
			Real& rfRAngle) const;
		bool ToEulerAnglesYZX (Real& rfYAngle, Real& rfPAngle,
			Real& rfRAngle) const;
		bool ToEulerAnglesZXY (Real& rfYAngle, Real& rfPAngle,
			Real& rfRAngle) const;
		bool ToEulerAnglesZYX (Real& rfYAngle, Real& rfPAngle,
			Real& rfRAngle) const;
		void FromEulerAnglesXYZ (Real fYAngle, Real fPAngle, Real fRAngle);
		void FromEulerAnglesXZY (Real fYAngle, Real fPAngle, Real fRAngle);
		void FromEulerAnglesYXZ (Real fYAngle, Real fPAngle, Real fRAngle);
		void FromEulerAnglesYZX (Real fYAngle, Real fPAngle, Real fRAngle);
		void FromEulerAnglesZXY (Real fYAngle, Real fPAngle, Real fRAngle);
		void FromEulerAnglesZYX (Real fYAngle, Real fPAngle, Real fRAngle);

		// eigensolver, matrix must be symmetric
		void EigenSolveSymmetric (Real afEigenvalue[3],
			Vector3 akEigenvector[3]) const;

		static void TensorProduct (const Vector3& rkU, const Vector3& rkV,
			Matrix3& rkProduct);

		static const Real EPSILON;
		static const Matrix3 ZERO;
		static const Matrix3 IDENTITY;

	protected:
		// support for eigensolver
		void Tridiagonal (Real afDiag[3], Real afSubDiag[3]);
		bool QLAlgorithm (Real afDiag[3], Real afSubDiag[3]);

		// support for singular value decomposition
		static const Real ms_fSvdEpsilon;
		static const int ms_iSvdMaxIterations;
		static void Bidiagonalize (Matrix3& kA, Matrix3& kL, Matrix3& kR);
		static void GolubKahanStep (Matrix3& kA, Matrix3& kL, Matrix3& kR);

		// support for spectral norm
		static Real MaxCubicRoot (Real afCoeff[3]);

		Real m_aafEntry[3][3];
	};

	//----------------------------------------------------------------------------
	inline Matrix3::Matrix3 ()
	{
		// For efficiency reasons, do not initialize matrix.
	}
	//----------------------------------------------------------------------------
	inline Real* Matrix3::operator[] (int iRow) const
	{
		return (Real*)&m_aafEntry[iRow][0];
	}
	//----------------------------------------------------------------------------
	inline Matrix3::operator Real* ()
	{
		return &m_aafEntry[0][0];
	}


	// =====================================================

	// SPHERE STUFF....

	class MAGICFM Vector2
	{
	public:
		// construction
		Vector2 ();
		Vector2 (Real fX, Real fY);
		Vector2 (Real afCoordinate[2]);
		Vector2 (const Vector2& rkVector);

		// coordinates
		Real x, y;

		// access vector V as V[0] = V.x, V[1] = V.y
		//
		// WARNING.  These member functions rely on
		// (1) Vector2 not having virtual functions
		// (2) the data packed in a 2*sizeof(Real) memory block
		Real& operator[] (int i) const;
		operator Real* ();

		// assignment
		Vector2& operator= (const Vector2& rkVector);

		// comparison (supports fuzzy arithmetic when FUZZ > 0)
		bool operator== (const Vector2& rkVector) const;
		bool operator!= (const Vector2& rkVector) const;
		bool operator<  (const Vector2& rkVector) const;
		bool operator<= (const Vector2& rkVector) const;
		bool operator>  (const Vector2& rkVector) const;
		bool operator>= (const Vector2& rkVector) const;

		// arithmetic operations
		Vector2 operator+ (const Vector2& rkVector) const;
		Vector2 operator- (const Vector2& rkVector) const;
		Vector2 operator* (Real fScalar) const;
		Vector2 operator/ (Real fScalar) const;
		Vector2 operator- () const;
		MAGICFM friend Vector2 operator* (Real fScalar, const Vector2& rkVector);

		// arithmetic updates
		Vector2& operator+= (const Vector2& rkVector);
		Vector2& operator-= (const Vector2& rkVector);
		Vector2& operator*= (Real fScalar);
		Vector2& operator/= (Real fScalar);

		// vector operations
		Real Length () const;
		Real SquaredLength () const;
		Real Dot (const Vector2& rkVector) const;
		Real Unitize (Real fTolerance = 1e-06f);
		Vector2 Cross () const;  // returns (y,-x)
		Vector2 UnitCross () const;  // returns (y,-x)/sqrt(x*x+y*y)

		// Gram-Schmidt orthonormalization.
		static void Orthonormalize (Vector2 akVector[/*2*/]);

		// special points
		static const Vector2 ZERO;
		static const Vector2 UNIT_X;
		static const Vector2 UNIT_Y;

		// fuzzy arithmetic (set FUZZ > 0 to enable)
		static Real FUZZ;
	};

	//----------------------------------------------------------------------------
	inline Vector2::Vector2 ()
	{
		// For efficiency in construction of large arrays of vectors, the
		// default constructor does not initialize the vector.
	}
	//----------------------------------------------------------------------------
	inline Real& Vector2::operator[] (int i) const
	{
		return ((Real*)this)[i];
	}
	//----------------------------------------------------------------------------
	inline Vector2::operator Real* ()
	{
		return (Real*)this;
	}
	//----------------------------------------------------------------------------
	inline Real Vector2::SquaredLength () const
	{
		return x*x + y*y;
	}
	//----------------------------------------------------------------------------


	//----------------------------------------------------------------------------
	class MAGICFM Sphere
	{
	public:
		Sphere ();

		Vector3& Center ();
		const Vector3& Center () const;

		Real& Radius ();
		const Real& Radius () const;

	protected:
		Vector3 m_kCenter;
		Real m_fRadius;
	};

	//----------------------------------------------------------------------------
	inline Sphere::Sphere ()
	{
		// no initialization for efficiency
	}
	//----------------------------------------------------------------------------
	inline Vector3& Sphere::Center ()
	{
		return m_kCenter;
	}
	//----------------------------------------------------------------------------
	inline const Vector3& Sphere::Center () const
	{
		return m_kCenter;
	}
	//----------------------------------------------------------------------------
	inline Real& Sphere::Radius ()
	{
		return m_fRadius;
	}
	//----------------------------------------------------------------------------
	inline const Real& Sphere::Radius () const
	{
		return m_fRadius;
	}
	//----------------------------------------------------------------------------


	class Support
	{
	public:
		int m_iQuantity;
		int m_aiIndex[4];

		bool Contains (int iIndex, Vector3** apkPoint);
	};

	//----------------------------------------------------------------------------

	MAGICFM Sphere MinSphere (int iQuantity, const Vector3* akPoint);


	//----------------------------------------------------------------------------

	///////////////////////////////////////////////////////////////////////////////////////////////



	///////////////////////////////////////////////////////////////////////////////
}  // namespace Mgc

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
