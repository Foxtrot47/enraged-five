// 
// rexBase/xmlAssetReport.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#pragma once
#include "atl/string.h"
#include "file/stream.h"
#include "vector/matrix34.h"

//=============================================================================
// xmlAssetReport
// PURPOSE
//   This calls provides some simple functions for generating a report 
//   during export.
//
//	To use this class first call:
//
//		xmlAssetReport::Open(<filename of html file to write report too>);
//
//	Then at any point after that in the plugin call:
//
//		xmlAssetReport::Write(<string to report>)
//
//	And then at the end of the export call xmlAssetReport::Close().
//
//	Unless a path is provided to Open, then the the class does nothing and it is safe
//	to call it any and everywhere.  This provides a simple way to globally disable
//	or enable the reporting.
//
namespace rage {
	class xmlAssetReport
	{
	public:
		xmlAssetReport(void);
		~xmlAssetReport(void);

		// Open and closing
		static bool Open();
		static bool Close(const char* ReportFilename);

		// Modifiers
		// Call this function whenever a file is edited, this will allow the exporter to generate a list of all files touched
		static bool AddToListOfCreatedFiles(const char* pcModifiedFile);
		static bool AddToListOfCreatedFiles(const ConstString& strModifiedFile);

		static bool AddToListOfOpenedFiles(const char* pcModifiedFile, bool bReadOnly);
		static bool AddToListOfOpenedFiles(const ConstString& strModifiedFile, bool bReadOnly);

		// Outputing
		static bool Write(const ConstString& strStringToOutput);
		static bool Write(const char *fmt, ...);
	private:

		class PropReference
		{
		public:
			ConstString	m_strPropName;
			Matrix34	m_obMatrix;
			bool		m_bIsGfxOnly;
		};

		// Output string
		static bool OutputString(const char* strStringToOutput, fiStream* fout = NULL);

		// Keep a list of all the files touched by the export, this is neccessary for any automatic version control for the resources
		static atArray<ConstString>	m_CreatedFiles;
		static atArray<ConstString>	m_OpenedFiles;
		static atArray<ConstString>	m_OpenedAsReadOnlyFiles;
		static __time64_t m_lStartTime;
	};

} // namespace rage
