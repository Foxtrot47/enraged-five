//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//
#include "rexBase/xmlAssetReport.h"
#include ".\rexReport.h"
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>

#include "parser/manager.h"

using namespace rage;

rexReport::rexReport(void):
	m_pReportFile(NULL)
{
}

rexReport::~rexReport(void)
{
	delete m_pReportFile;
	m_pReportFile = NULL;
}

bool rexReport::Open(const char *ReportFilename)
{
	m_ReportFilename = ReportFilename;
	if(m_pReportFile)
		delete m_pReportFile;
	m_pReportFile = new RexReportFile();
	return NULL!=m_pReportFile;
}

RexReportSection &rexReport::GetSectionByName(char *sectionName)
{
	RexReportSectionArray::iterator it = m_pReportFile->m_sections.begin();
	for (;it!=m_pReportFile->m_sections.end();++it)
	{
		if( 0==strcmp((*it).m_name, sectionName) )
			return (*it);
	}
	
	RexReportSection newSection;
	newSection.m_name = StringDuplicate(sectionName);
	m_pReportFile->m_sections.PushAndGrow(newSection, 4);
	return m_pReportFile->m_sections.Top();
}

bool rexReport::AddToMaterialIDList(const char *objectName, const char *mtlName, int shaderIndex)
{
	if(!IsReportEnabled())
		return false;

	RexReportSection &section = GetSectionByName("MaterialMap");

	MtlMapEntry *mapEntry = new MtlMapEntry();
	mapEntry->m_objectName = objectName;
	mapEntry->m_mtlName = mtlName;
	mapEntry->m_shaderIndex = shaderIndex;

	section.m_entries.PushAndGrow(mapEntry, 4);

	return true;
}

bool rexReport::Close()
{
	bool bResult = false;
	if(IsReportEnabled())
	{
		fiStream * streamReport = fiStream::Create(m_ReportFilename);
		if (streamReport)
		{
			bResult = PARSER.SaveObject(streamReport, m_pReportFile);
			streamReport->Close();
		}	
		delete m_pReportFile;
		m_pReportFile = NULL;
	}
	m_ReportFilename = "";
	return bResult;
}

bool rexReport::Write(const atString& strStringToOutput)
{
	return OutputString(strStringToOutput);
}

bool rexReport::Write(const char *fmt, ...)
{
	va_list args;
	va_start( args, fmt );
	char acBuffer[4096];
	vformatf(acBuffer, 4096, fmt, args);
	atString strStringToOutput(acBuffer);
	va_end( args );

	return OutputString(strStringToOutput);
}

bool rexReport::OutputString(const atString& strStringToOutput, fiStream* fout)
{
	if(!IsReportEnabled())
	{
		// A Report is not open, so do nothing
		return false;
	}

	bool bGivenFile = (fout != NULL);
	if(!bGivenFile)
	{
		// Not given a stream to write to, so just append to the current file
		// Open file for appending
		fout = fiStream::Open(m_ReportFilename, false);
		if(!fout)
		{
			// Could not open file, maybe it does not exist
			fout = fiStream::Create(m_ReportFilename);
			if(!fout)
			{
				return false;
			}
		}
		else
		{
			// Seek to the end of the file
			fout->Seek(fout->Size());
		}
	}

	// Write out data
	fout->Write(strStringToOutput, strStringToOutput.GetLength());

	if(!bGivenFile)
	{
		// I opened the file, so I better close it
		fout->Close();
	}
	return true;
}
