#include "rexBase/object.h"
#include <typeinfo.h>



namespace rage {

bool	rexObject::IsSameType( const rexObject& obj ) const
{
	return ( typeid( *this ) == typeid( obj ) ) != 0;
}

atArray<rexObject*> sRexObjectList;

rexObject::rexObject()
{
#if 0
	Displayf( "ADD object 0x%x type %s (%d)", (int)this, typeid( *this ).name(), sRexObjectList.GetCount() );
	sRexObjectList.PushAndGrow( this );
	fflush(stdout);
#endif
	m_DeleteContainedObjectsOnDestruction = true;
}

rexObject::~rexObject()
{
	if( m_DeleteContainedObjectsOnDestruction )
	{
		while( m_ContainedObjects.GetCount() )
			delete m_ContainedObjects.Pop();
	}
#if 0
	sRexObjectList.DeleteMatches( this );
	Displayf( "DELETE object 0x%x type %s (%d)", (int)this, typeid( *this ).name(), sRexObjectList.GetCount() );
	fflush(stdout);
#endif
}

rexObject* rexObject::GetRexObject( int i )
{
	return sRexObjectList[ i ];
}

int rexObject::GetRexObjectCount()
{
	return sRexObjectList.GetCount();
}

} // namespace rage
