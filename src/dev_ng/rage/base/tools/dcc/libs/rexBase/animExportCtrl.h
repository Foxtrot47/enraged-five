// 
// rexBase/animExportCtrl.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __REX_BASE_ANIMEXPORTCTRL_H__
#define __REX_BASE_ANIMEXPORTCTRL_H__

#include "parser/manager.h"
#include "atl/array.h"
#include "atl/string.h"

namespace rage 
{

//-----------------------------------------------------------------------------

class AnimExportCtrlTrack
{
public:
	AnimExportCtrlTrack() {};

	AnimExportCtrlTrack(const char* inputName, const char* outputName)
		: m_inputName(inputName)
		, m_outputName(outputName)
		, m_compressionTol(-1.0f)
	{};

	virtual ~AnimExportCtrlTrack() 
	{
		m_splitComponents.Reset();
	};

	const char* GetInputName() const { return (const char*)m_inputName; }
	void		SetInputName(const char* name) { m_inputName = name; }
	
	const char* GetOutputName() const { return (const char*)m_outputName; }
	void		SetOutputName(const char* name) { m_outputName = name; }

	int			GetNumComponentParts() const { return m_splitComponents.size()<1?1:m_splitComponents.size(); }
	const char*	GetComponentPart(u32 idx) const;

	void		BuildComponentOutputName(int cIdx, atString& compOutputName) const;

	float		GetCompressionTolerance() const { return m_compressionTol; }
	void		SetCompressionTolerance(float tol) { m_compressionTol = tol; }

private:
	void		PostLoad();
	void		SplitComponentString();

private:
	ConstString		m_inputName;
	ConstString		m_outputName;
	ConstString		m_components;
	
	float			m_compressionTol;

	atArray<ConstString> m_splitComponents;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

class AnimExportCtrlTrackSpec
{
public:
	AnimExportCtrlTrackSpec() {};
	AnimExportCtrlTrackSpec(const char* nameExpr)
		: m_nameExpr(nameExpr)
		, m_bIncludeChildren(false)
	{};

	virtual ~AnimExportCtrlTrackSpec() 
	{
		m_Tracks.Reset();
	};

	const char*	GetNameExpr() const { return (const char*)m_nameExpr; }
	void		SetNameExpr(const char* expr) {m_nameExpr = expr;}

	bool		IncludesChildren() const { return m_bIncludeChildren; }

	int							GetTrackCount() const { return m_Tracks.GetCount(); }
	const AnimExportCtrlTrack&	GetTrack(int idx) const { return m_Tracks[idx]; }
	const AnimExportCtrlTrack*	GetTrackByInputName(const char* name) const
	{
		for(int i=0; i<m_Tracks.GetCount(); i++)
		{
			if(strcmpi(m_Tracks[i].GetInputName(), name)==0)
				return &m_Tracks[i];
		}
		return NULL;
	}

	void						AppendTrack(const AnimExportCtrlTrack& track) { m_Tracks.PushAndGrow(track); }
	void						AppendTrack(const char* inputName, const char* outputName)
	{
		AnimExportCtrlTrack& track = m_Tracks.Grow();
		track.SetInputName(inputName);
		track.SetOutputName(outputName);
	}

private:
	ConstString						m_nameExpr;
	bool							m_bIncludeChildren;
	atArray<AnimExportCtrlTrack>	m_Tracks;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

class AnimExportCtrlSpec
{
public:
	AnimExportCtrlSpec() {};

	virtual ~AnimExportCtrlSpec() 
	{
		m_TrackSpecs.Reset();
	};

	const	AnimExportCtrlTrackSpec*	FindTrackSpec(const char* searchName) const;
	
	const	AnimExportCtrlTrackSpec&	GetTrackSpec(int idx) const { return m_TrackSpecs[idx]; }
	int									GetNumTrackSpecs() const { return m_TrackSpecs.GetCount(); }

	void								AppendTrackSpec(const AnimExportCtrlTrackSpec& spec) { m_TrackSpecs.PushAndGrow(spec); }

	bool LoadFromXML(const char* filePath);
	bool SaveToXML(const char* filePath) const;

	void Reset()
	{
		m_TrackSpecs.Reset();
	}

	static void RegisterParsableClasses()
	{
		if(!sm_ParsableClassesRegistered)
		{
			REGISTER_PARSABLE_CLASS(AnimExportCtrlTrack);
			REGISTER_PARSABLE_CLASS(AnimExportCtrlTrackSpec);
			REGISTER_PARSABLE_CLASS(AnimExportCtrlSpec);
			sm_ParsableClassesRegistered = true;
		}
	}

	static void UnRegisterParsableClasses()
	{
		if(sm_ParsableClassesRegistered)
		{
			UNREGISTER_PARSABLE_CLASS(AnimExportCtrlSpec);
			UNREGISTER_PARSABLE_CLASS(AnimExportCtrlTrackSpec);
			UNREGISTER_PARSABLE_CLASS(AnimExportCtrlTrack);
			sm_ParsableClassesRegistered = false;
		}
	}

private:
	int StrWildCmp(const char *wild, const char *string) const;

private:
	atArray<AnimExportCtrlTrackSpec>	m_TrackSpecs;
	
	static bool							sm_ParsableClassesRegistered;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

}//End namespace rage

#endif //__REX_BASE_ANIMEXPORTCTRL_H__

