/* exmodel/degemodel.cpp */

#include "edgemodel.h"


#include "file/stream.h"
#include "file/asset.h"
#include "file/token.h"
#include "string/string.h"


namespace rage {

bool edgeModel::Load(const char *filename) {

	static const char OBRACE[] = "{";
	static const char CBRACE[] = "}";

	fiSafeStream S(ASSET.Open(filename,"mod"));
	if (!S)
		return false;

	fiAsciiTokenizer T;
	T.Init(filename,S);

	T.MatchInt("version:");
	m_NumVerts = T.MatchInt("verts:");
	int normals=T.MatchInt("normals:");
	int numColors = T.MatchInt("colors:");
	int tex1s = T.MatchInt("tex1s:");
	int tex2s = T.MatchInt("tex2s:");
	T.MatchInt("tangents:");
	int materials = T.MatchInt("materials:");
	T.MatchInt("adjuncts:");
	m_NumTris = T.MatchInt("primitives:");
	m_NumMatrices = T.MatchInt("matrices:");
	T.MatchInt("reskins:");
	
	int i;
	m_Verts = new Vert[m_NumVerts];
	m_LoadVerts = new Vector3[m_NumVerts];
	for (i=0; i<m_NumVerts; i++) {
		T.MatchVector("v",m_Verts[i].Pos);
		m_LoadVerts[i].Set(m_Verts[i].Pos);

	}

	// Skip the soft normals
	Vector3 n;
	for (i=0; i<normals; i++)
		T.MatchVector("n",n);

	// Skip colors
	Vector4 c;
	while (numColors--)
		T.MatchVector("c",c);
	// Skip Tex1s
	Vector2 t;
	while (tex1s--)
		T.MatchVector("t1",t);
	// Skip Tex2s
	while (tex2s--)
		T.MatchVector("t2",t);
	//Skip Materials
	for (i=0; i<materials; i++) {
		T.GetDelimiter("mtl");
		char brace[64];
		while (T.GetToken(brace,sizeof(brace)) && strcmp(brace,CBRACE))
			;
	}

	int p = 0;
	m_Tris = new Tri[m_NumTris];

	for (i=0; i<materials; i++) {
		T.GetDelimiter("packet");
		int adj = T.GetInt();
		int tris = T.GetInt();
		int lm = T.GetInt();
		T.GetDelimiter(OBRACE);
		
		int j;
		struct Adj { int vi, mi; };
		Adj *a = new Adj[adj];
		for (j=0; j<adj; j++) {
			T.GetDelimiter("adj");
			a[j].vi = T.GetInt();
			T.GetInt();
			T.GetInt();
			T.GetInt();
			T.GetInt();
			a[j].mi=T.GetInt();
		}

		for (j=0; j<tris; j++) {
			T.GetDelimiter("tri");
			Tri &tri = m_Tris[p++];
			for (int k=0; k<3; k++) {
				int t = T.GetInt();
				tri.VertInd[k] = a[t].vi;
			}
		}

		int *mtxs = new int [lm];
		T.GetDelimiter("mtx");
		for (j=0; j<lm; j++)
			mtxs[j]=T.GetInt();
		T.GetDelimiter(CBRACE);

		//Set the one matrix per vertex
		for(j=0;j<adj;j++)
			m_Verts[a[j].vi].MatrixInd=mtxs[a[j].mi];

		delete[] a;
		delete [] mtxs;
	}

	Assert(p == m_NumTris);

	//make sure there is a matrix for every vert
	for(int j=0;j<m_NumVerts;j++)
		if(m_Verts[j].MatrixInd==-1)
			Errorf("not all verts have a matrix");

	//compute all the facet tri normals for coplanar removal
	ComputeAllNormals();

	return true;
}

void edgeModel::ComputeNormal(int t)
{
	Vector3 A;
	Vector3 B;

	A.Subtract(m_Verts[m_Tris[t].VertInd[1]].Pos,m_Verts[m_Tris[t].VertInd[0]].Pos);
	B.Subtract(m_Verts[m_Tris[t].VertInd[2]].Pos,m_Verts[m_Tris[t].VertInd[0]].Pos);
	
	Vector3 normal;
	normal.Cross(A,B);
	normal.Normalize();
	m_Tris[t].Normal=normal;
}

void edgeModel::ComputeAllNormals()
{
	for(int t=0;t<m_NumTris;t++)
		ComputeNormal(t);
}

bool edgeModel::IsDegenerate(int t)
{
	//Tri is degenerate if any two points are the same
	Tri * tri=&m_Tris[t];

	for(int i=0;i<3;i++)
		for(int j=0;j<3;j++)
		{
			if( (i!=j) && (m_Verts[tri->VertInd[i]].Pos==m_Verts[tri->VertInd[j]].Pos) )
			{
				Warningf("Degenerate tri found!!");
				return true;
			}
		}
	return false;
	
}

void edgeModel::FlagErrorTriangles(Edge & e)
{
	//we know this edge has too many faces so flag all the tris it is in
	for(int t=0;t<m_NumTris;t++)
	{
		if(t==326) 
			t=t;
		if(ContainsEdge(t,e))
			m_Tris[t].ErrorTriangle=true;
	}
}

int edgeModel::CountSharedFaces(int v0, int v1)
{
	int count=0;  
	Edge e;
	e.VertInd[0]=v0;
	e.VertInd[1]=v1;

	for(int f=0;f<m_NumTris;f++)
	{
		if(f==326)
			f=f;
		if(ContainsEdge(f,e))
		{
			count++;
		}
	}

	if(count>2)
	{
		MultiFaceError++;
	}
	return count;
}
bool edgeModel::ContainsEdge(int t, Edge e)
{
	int numFound=0;
	//A triangle contains this edge if it has both points in it

	for(int v=0;v<3;v++)
		for(int i=0;i<2;i++)
		{
			Vector3 &A=m_Verts[m_Tris[t].VertInd[v]].Pos;
			Vector3 &B=m_Verts[e.VertInd[i]].Pos;
			if(A==B)
				numFound++;
		}

		if(numFound==2) 
			return true;

	return false;
}

bool edgeModel::ContainsEdge(int t, int e)
{
	return ContainsEdge(t,m_Edges[e]);
}

int edgeModel::AddEdge(int tri,int v0,int v1,bool alwaysAdd /*=true*/)
{
	int count=0;
	//Add an edge if it's not already in the list or if it has more then 2 shared faces

	count=CountSharedFaces(v0,v1);

	if(!alwaysAdd)
	{
		if((count<3))
			for(int i=0;i<m_Edges.GetCount();i++) 
				if( (m_Edges[i].VertInd[0] == v0 && m_Edges[i].VertInd[1] == v1) || 
					(m_Edges[i].VertInd[0] == v1 && m_Edges[i].VertInd[1] == v0) ) 
					return -1;
	}
		
	Edge &E = m_Edges.Grow(256);
	E.VertInd[0]=v0;
	E.VertInd[1]=v1;
	E.TriInd[0]=tri;
	E.TriInd[1]=-1;
	E.SharedFaceCount=count;
	if(count>2)
		E.MultiFaceError=true;
	//if(count==1) NumOrphans++;
	return m_Edges.GetCount()-1;
}
void edgeModel::RemoveEdges()
{
	//Go thru edges and remove simular edges that are not part of an orphaned face or
	//shared by multiple faces
	
	OrphanizedTris=0;
	for(int e=0;e<m_Edges.GetCount();e++)
	{
		if(m_Edges[e].SharedFaceCount>2)
		{
			m_Tris[m_Edges[e].TriInd[0]].IsOrphan=true;
		}
	}

	for(int t=0;t<m_NumTris;t++)
	{
		if(m_Tris[t].IsOrphan)
			OrphanizedTris++;
	}

	//Edges go bye bye
	m_Edges.Reset();

	//NumOrphans=0;

	// add all edges
	for(int i=0;i<m_NumTris;i++) {
		if(!IsDegenerate(i))
		{
			if(OrphanizedTris)
			{
				m_Tris[i].MyEdge[0]=AddEdge(i,m_Tris[i].VertInd[0],m_Tris[i].VertInd[1],true);
				m_Tris[i].MyEdge[1]=AddEdge(i,m_Tris[i].VertInd[1],m_Tris[i].VertInd[2],true);
				m_Tris[i].MyEdge[2]=AddEdge(i,m_Tris[i].VertInd[2],m_Tris[i].VertInd[0],true);
			}
			else
			{
				m_Tris[i].MyEdge[0]=AddEdge(i,m_Tris[i].VertInd[0],m_Tris[i].VertInd[1],false);
				m_Tris[i].MyEdge[1]=AddEdge(i,m_Tris[i].VertInd[1],m_Tris[i].VertInd[2],false);
				m_Tris[i].MyEdge[2]=AddEdge(i,m_Tris[i].VertInd[2],m_Tris[i].VertInd[0],false);

			}
		}
	}
}
bool edgeModel::SameMatrix(int e)
{
	int mtxInd=0;
	int cnt=0;
	//return true if all faces of this edge share the same matrix
	for(int t=0;t<m_NumTris;t++)
	{
		if(ContainsEdge(t,e))
		{
			mtxInd+=m_Verts[m_Tris[t].VertInd[0]].MatrixInd;
			mtxInd+=m_Verts[m_Tris[t].VertInd[1]].MatrixInd;
			mtxInd+=m_Verts[m_Tris[t].VertInd[2]].MatrixInd;
			cnt+=3;
		}
	}

	if( (mtxInd/cnt) == m_Verts[m_Edges[e].VertInd[0]].MatrixInd )
		return true;
	return false;

/*
	int cnt=0;
	int matrix=0;
	Edge &edge=m_Edges[e];

	for(int f=0;f<2;f++)
		for(int v=0;v<3;v++)
		{
			if(edge.TriInd[f]!=-1)
			{
				matrix+=m_Verts[m_Tris[edge.TriInd[f]].VertInd[v]].MatrixInd;
				cnt++;
			}
		}
	if((matrix/cnt) == m_Verts[edge.VertInd[0]].MatrixInd)
		return true;
	return false;*/
}
bool edgeModel::TriContainsVert(int t, int vert)
{
	for(int v=0;v<3;v++)
		if(m_Verts[m_Tris[t].VertInd[v]].Pos==m_Verts[vert].Pos)
			return true;
	return false;
}

void edgeModel::DetectDoubleVerts()
{
	for(int v=0;v<m_NumVerts;v++)
		for(int i=0;i<m_NumVerts;i++)
		{
			if(i!=v)
				if(m_Verts[v].Pos==m_Verts[i].Pos)
				{
					m_Verts[v].IsDouble=true;
				}
		}
}

void edgeModel::DetectDoubleFacedTris()
{
	//Tris are doublefaced if multiple tris have the same points 
	NumDoubleFaced=0;
	//go thru all tris
	for( int t=m_NumTris-1;t>=0;t--)
	{
		//go thru all the others
		for( int f=m_NumTris-1;f>=0;f--)
		{
			if(t!=f)
			{
				if( (TriContainsVert(f,m_Tris[t].VertInd[0])) &&
					(TriContainsVert(f,m_Tris[t].VertInd[1])) &&
					(TriContainsVert(f,m_Tris[t].VertInd[2])) )
				{
					//these two are double faced!
					//Errorf("DoubleFaced Tri Detected!! Please remove one of them");
					NumDoubleFaced++;
					m_Tris[t].IsDoubleFaced=true;
				}
			}
		}
	}
}
void edgeModel::Build() {
	int i;

	//Detect double verts
	DetectDoubleVerts();
	//Detect double faced tris
	DetectDoubleFacedTris();

	// add all edges
	for(i=0;i<m_NumTris;i++) {
		if(!IsDegenerate(i))
		{
			m_Tris[i].MyEdge[0]=AddEdge(i,m_Tris[i].VertInd[0],m_Tris[i].VertInd[1],true);
			m_Tris[i].MyEdge[1]=AddEdge(i,m_Tris[i].VertInd[1],m_Tris[i].VertInd[2],true);
			m_Tris[i].MyEdge[2]=AddEdge(i,m_Tris[i].VertInd[2],m_Tris[i].VertInd[0],true);
		}
	}
	MultiFaceError=0;
	NumOrphans=0;

	//Remove the unnecessary edges
	RemoveEdges();
	int numCoplanar=0;
	if(!OrphanizedTris)
	{
		FindNeighbors();

		//Remove coplaner edges
		//Only if all the verts share the same matrix -for skinning
		numCoplanar=0;
		if( RemoveCoplanar )
		{
			for(i=m_Edges.GetCount()-1;i>=0;i--)
			{
				if(m_Edges[i].TriInd[1]!=-1)
				{
					//Totally equal
					if(m_Tris[m_Edges[i].TriInd[0]].Normal==m_Tris[m_Edges[i].TriInd[1]].Normal)
					{
						if(SameMatrix(i))
						{
							m_Edges.Delete(i);
							numCoplanar++;
						}
					}
					else
					//Tollerance
					if( (m_Edges[i].TriInd[0]!=-1) && (m_Edges[i].TriInd[1]!=-1))
					{
						float ang = m_Tris[m_Edges[i].TriInd[0]].Normal.Angle(m_Tris[m_Edges[i].TriInd[1]].Normal);
						if((fabs(ang))<=CoplanarTollerance) 
						{
							if(SameMatrix(i))
							{
								m_Edges.Delete(i);
								numCoplanar++;
							}
						}
					}
				}
			}
		}
	}
	Displayf("%d triangles %d good edges (%d orphans) %d coplaner removed",m_NumTris,m_Edges.GetCount(),NumOrphans,numCoplanar);
	if(NumDoubleFaced)
		Warningf("There are %d double faced triangles which are unnecessary.",NumDoubleFaced/2);
	if(MultiFaceError)
		Errorf("There are %d edges which have more then two faces!!",MultiFaceError);
	if(OrphanizedTris)
	{
		//Warningf("There are %d orphanized Tris",OrphanizedTris);
		Errorf("This model is not optimized for shadow casting technology!!");
	}
}
void edgeModel::FindNeighbors()
{
	int NumEdgesOrphanedByTri=0;
	int NumNeighbors;
	for(int e=m_Edges.GetCount()-1;e>=0;e--)
	{
		NumNeighbors=0;
		//If edge has more then 2 total shared faces then it is an orphan
		//or if edge's tri is an orphan
		if( (m_Edges[e].SharedFaceCount>2) || (m_Tris[m_Edges[e].TriInd[0]].IsOrphan) )
		{
			m_Edges[e].TriInd[1]=-1;
			if(m_Tris[m_Edges[e].TriInd[0]].IsOrphan)
				NumEdgesOrphanedByTri++;
		}
		else
		{
			
			for(int f=0;f<m_NumTris;f++)
			{
				if(f!=m_Edges[e].TriInd[0])
				{
					//If this edge is in here then we have neighbor
					if( (ContainsEdge(f,e)))
					{
						NumNeighbors++;
						if(m_Edges[e].TriInd[1]==-1)   //If this is the first and hopefully only neighbor 
						{
							m_Edges[e].TriInd[1]=f;
						}
					}
				}
			}
		}
		if(NumNeighbors>1)
		{
			//This is an error, there should only be 2 triangels per edge
			Displayf("You should never see this");
			//m_Edges[e].TriInd[1]=-1;			
		}
		else
			if(NumNeighbors==0)
				NumOrphans++;
	}

	Displayf("NumEdgesOrphanedByTri %d",NumEdgesOrphanedByTri);
}

bool edgeModel::Save(const char *filename) 
{
	fiSafeStream S(ASSET.Create(filename,"em"));
	if (!S)
	{
		return false;
	}

	fiAsciiTokenizer T;
	T.Init(filename,S);

	T.PutDelimiter("version: "); T.Put(100); T.EndLine();
	T.PutDelimiter("verts: "); T.Put(m_NumVerts); T.EndLine();
	T.PutDelimiter("tris: "); T.Put(m_NumTris); T.EndLine();
	T.PutDelimiter("edges: "); T.Put(m_Edges.GetCount()); T.EndLine();
	T.PutDelimiter("optimized: "); T.Put((MultiFaceError)?(0):(1)); T.EndLine();

	T.EndLine();

	int i;
	for (i=0; i<m_NumVerts; i++) {
		T.PutDelimiter("v ");
		T.Put(m_LoadVerts[i]); 
		T.Put(m_Verts[i].MatrixInd);T.EndLine();
	}
	T.EndLine();
	for (i=0; i<m_NumTris; i++) {
		T.PutDelimiter("tri ");
		T.Put(m_Tris[i].VertInd[0]); 
		T.Put(m_Tris[i].VertInd[1]); 
		T.Put(m_Tris[i].VertInd[2]); 
		T.EndLine();
	}
	T.EndLine();
	for (i=0; i<m_Edges.GetCount(); i++) {
		Edge &e = m_Edges[i];
		T.PutDelimiter("e ");
		T.PutShort(e.VertInd[0]);
		T.PutShort(e.VertInd[1]);
		T.PutShort(e.TriInd[0]);
		T.PutShort(e.TriInd[1]);
		T.EndLine();
	}
	T.EndLine();
	return true;
}

void edgeModel::Skin(const Matrix34 *mtx)
{
	for(int v=0;v<m_NumVerts;v++)
	{
		mtx[m_Verts[v].MatrixInd].Transform(m_Verts[v].Pos);
	}
}

#if 0
void edgeModel::Draw()
{
	//Draw all the triangles
	for(int t=0;t<m_NumTris;t++)
	{
		
		grcBegin(drawLineStrip,6);
		vglColorUnFixed(Color32(255,255,255,255));
		grcVertex3f(m_Verts[m_Tris[t].VertInd[0]].Pos);
		vglColorUnFixed(Color32(0,0,255,255));
		grcVertex3f(m_Verts[m_Tris[t].VertInd[1]].Pos);
		vglColorUnFixed(Color32(255,255,255,255));
		grcVertex3f(m_Verts[m_Tris[t].VertInd[1]].Pos);
		vglColorUnFixed(Color32(0,0,255,255));
		grcVertex3f(m_Verts[m_Tris[t].VertInd[2]].Pos);
		vglColorUnFixed(Color32(255,255,255,255));
		grcVertex3f(m_Verts[m_Tris[t].VertInd[2]].Pos);
		vglColorUnFixed(Color32(0,0,255,255));
		grcVertex3f(m_Verts[m_Tris[t].VertInd[0]].Pos);
		grcEnd();

		if(m_Tris[t].IsDoubleFaced)
		{
			grcBegin(drawTris,3);
			vglColorUnFixed(Color32(255,128,0,100));
			grcVertex3f(m_Verts[m_Tris[t].VertInd[0]].Pos);
			grcVertex3f(m_Verts[m_Tris[t].VertInd[1]].Pos);
			grcVertex3f(m_Verts[m_Tris[t].VertInd[2]].Pos);
			grcEnd();
		}

	}

	int firstErrorEdge=-1;
	
	//Edges with Errors
	for(int e=0;e<m_Edges.GetCount();e++)
	{
		if(m_Edges[e].MultiFaceError)
		{
			if(firstErrorEdge==-1)
			{
				firstErrorEdge=e;
			}
			grcBegin(drawLines,2);
			vglColorUnFixed(Color32(255,0,0,255));
			grcVertex3f(m_Verts[m_Edges[e].VertInd[0]].Pos);
			grcVertex3f(m_Verts[m_Edges[e].VertInd[1]].Pos);
			grcEnd();

		}
	}

	int cnt=0;
	if(firstErrorEdge>=0)
	{
		//Sucky Triangles
		FlagErrorTriangles(m_Edges[firstErrorEdge]);
		for(int s=0;s<m_NumTris;s++)
		{
			if(m_Tris[s].ErrorTriangle)
			{
				cnt++;
				vglColorUnFixed(Color32(255,0,0,128));
				grcBegin(drawTris,3);
				grcVertex3f(m_Verts[m_Tris[s].VertInd[0]].Pos);
				grcVertex3f(m_Verts[m_Tris[s].VertInd[1]].Pos);
				grcVertex3f(m_Verts[m_Tris[s].VertInd[2]].Pos);
				grcEnd();
			}
		}
	}
	//Double Verts
	for(int v=0;v<m_NumVerts;v++)
	{
		if(m_Verts[v].IsDouble)
		{
			vglColorUnFixed(Color32(0,255,0,255));
			grcBegin(drawPoint,1);
			grcVertex3f(m_Verts[v].Pos);
			grcEnd();
		}
	}
}
#endif

} // namespace rage

/* End of file: exmodel/degemodel.cpp */
