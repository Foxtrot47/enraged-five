﻿<?xml version="1.0"?>
<ParserSchema xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="schemas/parsermetadata.xsd"
                 generate="class">

  <structdef type="BaseRexReportEntry">
    <string name="m_objectName" type="atString" />
  </structdef>

  <structdef type="MtlMapEntry" base="BaseRexReportEntry">
    <string name="m_mtlName" type="atString" />
    <u32 name="m_shaderIndex" />
  </structdef>

  <structdef type="RexReportSection">
    <string name="m_name" type="atString" />
    <array name="m_entries" type="atArray">
      <pointer type="BaseRexReportEntry" policy="owner"/>
    </array>
  </structdef>
  
  <structdef type="RexReportFile">
    <array name="m_sections" type="atArray">
      <struct type="RexReportSection"/>
    </array>
  </structdef>
  
</ParserSchema>