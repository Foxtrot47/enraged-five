// 
// rexBase/result.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX datBase Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RTTI 
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexResult -- class returned by many operations to give more robust status than
//					 true/false
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_RESULT_H__
#define __REX_BASE_RESULT_H__

/////////////////////////////////////////////////////////////////////////////////////

#define RESULT_TYPE_MASK 0xC0000000
#define INFO_FLAG_MASK 0x3FFFFFFF

#include "atl/array.h"
#include "atl/string.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexShell;

void myInvalidParameterHandler(const wchar_t* expression,
	const wchar_t* function, 
	const wchar_t* file, 
	unsigned int line, 
	uintptr_t /*pReserved*/);

class rexResult
{
public:
	enum eMessageType
	{
		MSGTYPE_DEBUG,
		MSGTYPE_MESSAGE,
		MSGTYPE_WARNING,
		MSGTYPE_ERROR
	};
	enum eResultType
	{
		UNINITIALIZED				= 0x00000000,
		PERFECT						= 0x40000000,
		WARNINGS					= 0x80000000,
		ERRORS						= 0xC0000000,
	};

	enum eInfoFlag
	{
		NO_INFO						= 0x00000000,
		NOT_IMPLEMENTED				= 0x00000001,
		TYPE_INCOMPATIBLE			= 0x00000002,
		INVALID_INPUT				= 0x00000004,
		OUT_OF_MEMORY				= 0x00000008,
		CANNOT_CREATE_OBJECT		= 0x00000010,
		FILE_ERROR					= 0x00000020,
		FILE_NOT_FOUND				= 0x00000040,
		FILE_NOT_OPEN				= 0x00000080,
		CANNOT_OPEN_FILE			= 0x00000100,
		CANNOT_CREATE_FILE			= 0x00000200,
		CANNOT_CREATE_DIRECTORY		= 0x00000400,

		INSUFFICIENT				= 0x00000800,
		CANNOT						= 0x00001000,
		INVALID						= 0x00002000,
		NOT							= 0x00004000,

		// TODO: move all these bits around
		SERIALIZATION				= 0x02000000,
		VERIFICATION_FAILURE		= 0x04000000,
		INITIALIZATION				= 0x08000000,
		TERMINATION					= 0x10000000,
		GATHERING					= 0x20000000,
		CONVERSION					= 0x40000000,
	};

	struct sRexResultMsg
	{
	public:
		eMessageType	mMsgType;
		atString		mMsg;
		atString		mContext;
		atString		mFileContext;
	};

	rexResult()											{ m_Result = UNINITIALIZED; _set_invalid_parameter_handler(myInvalidParameterHandler);}
	rexResult( const rexResult &that )					{ m_Result = that.m_Result; m_Messages = that.m_Messages; _set_invalid_parameter_handler(myInvalidParameterHandler);}
	rexResult( eResultType rt, unsigned int info = NO_INFO )	{ Set( rt ); SetInfo( info ); _set_invalid_parameter_handler(myInvalidParameterHandler);}

	void Reset()										{ m_Result = UNINITIALIZED; m_Messages.Reset(); }
	
	rexResult &Set( eResultType rt )					{ m_Result &= ~RESULT_TYPE_MASK; m_Result |= (rt & RESULT_TYPE_MASK); return *this; }
	rexResult &Set( eResultType rt, unsigned int info )	{ m_Result = (rt & RESULT_TYPE_MASK) | (info & INFO_FLAG_MASK); return *this; }
	rexResult &Set( const rexResult &that )				{ m_Result = that.m_Result; m_Messages = that.m_Messages; return *this; }
	rexResult &SetInfo( unsigned int info )				{ m_Result &= ~INFO_FLAG_MASK; m_Result |= (info & INFO_FLAG_MASK); return *this; }
	rexResult &SetInfoFlag( eInfoFlag f, bool b )		{ if( b ) m_Result |= (f & INFO_FLAG_MASK); else m_Result &= ~(f & INFO_FLAG_MASK); return *this; }

	unsigned int GetResultType() const					{ return (m_Result & RESULT_TYPE_MASK); }
	unsigned int GetInfo() const						{ Assert( m_Result & RESULT_TYPE_MASK ); return m_Result & INFO_FLAG_MASK; }
	bool GetInfoFlag( eInfoFlag f ) const				{ Assert( m_Result & RESULT_TYPE_MASK ); return ( (m_Result & (f & INFO_FLAG_MASK)) != 0 ); }

	bool Warnings()	const								{ Assert( m_Result & RESULT_TYPE_MASK ); return ( (m_Result & RESULT_TYPE_MASK) != PERFECT ); }
	bool Errors() const									{ Assert( m_Result & RESULT_TYPE_MASK ); return ( (m_Result & RESULT_TYPE_MASK) == ERRORS ); }
	bool OK() const										{ Assert( m_Result & RESULT_TYPE_MASK ); return ( (m_Result & RESULT_TYPE_MASK) != ERRORS ); }
	bool Perfect() const								{ Assert( m_Result & RESULT_TYPE_MASK ); return ( (m_Result & RESULT_TYPE_MASK) == PERFECT ); }

	bool Initialized() const							{ return ( (m_Result & RESULT_TYPE_MASK) != UNINITIALIZED ); }

	void AddMessage( const atString &m, const char *context="");
	void AddMessage( const char *fmt,... );
	void AddMessageCtx( const char *context, const char *fmt,... );
	void AddMessageFileCtx( const char *context, const char *filecontext, const char *fmt,... );
	void AddWarning( const atString &m, const char *context="");
	void AddWarning( const char *fmt,... );
	void AddWarningCtx( const char *context, const char *fmt,... );
	void AddWarningFileCtx( const char *context, const char *filecontext, const char *fmt,... );
	void AddError( const atString &m, const char *context="");
	void AddError( const char *fmt,... );
	void AddErrorCtx( const char *context, const char *fmt,... );
	void AddErrorFileCtx( const char *context, const char *filecontext, const char *fmt,... );
	
	int  GetMessageCount() const						{ return m_Messages.GetCount(); }	
	const atString GetMessageByIndex( int a ) const;
	const sRexResultMsg &GetMessagePrimByIndex( int a ) const
	{
		return m_Messages[ a ];
	}

	void Combine( const rexResult &res2 );
	void Combine( const rexResult &res1, const rexResult &res2 );
	void Combine( eResultType rt )
	{
		unsigned int type = (unsigned int)rt & RESULT_TYPE_MASK;
		if(type > (m_Result & RESULT_TYPE_MASK))
			Set((eResultType)type);
	}
	void Combine( eResultType rt, unsigned int info )
	{
		unsigned int type = (unsigned int)rt & RESULT_TYPE_MASK;
		if(type > (m_Result & RESULT_TYPE_MASK))
			Set((eResultType)type);
		m_Result |= (info & INFO_FLAG_MASK);
	}

	rexResult operator&( const rexResult &that )
	{
		rexResult r;
		r.Combine( *this, that );
		return r;
	}

	rexResult &operator&=( const rexResult &that )
	{
		Combine( that );
		return *this;
	}

	rexResult &operator=( const rexResult &that )		{ return Set( that ); }

	operator bool () const								{ return OK(); }

	void Display() const;

	static void SetShell(rexShell *p_Shell)
	{
		sp_Shell = p_Shell;
	}

protected:
	unsigned int			m_Result;
	atArray<sRexResultMsg>	m_Messages;

private:
	static rexShell *sp_Shell;
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
