// 
// rexBase/property.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX datBase Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexProperty -- abstract class for assigning properties to various other objects
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_PROPERTY_H__
#define __REX_BASE_PROPERTY_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "atl/array.h"
#include "atl/string.h"
#include "rexBase/result.h"
#include "rexBase/value.h"

namespace rage {

class rexModule;
class rexObject;
class rexShell;

/////////////////////////////////////////////////////////////////////////////////////

class rexProperty
{		
public:
	virtual rexResult		SetupShell( const atString& /*objectName*/, rexShell& /*shell*/, rexValue /*value*/, const atArray<rexObject*>& /*objects*/  ) const { return rexResult( rexResult::PERFECT ); } 
	virtual rexResult		SetupModuleInstance( rexModule& /*module*/, rexValue /*value*/, const atArray<rexObject*>& /*objects*/  ) const	{ return rexResult( rexResult::PERFECT ); } 
	virtual rexResult		ModifyFilteredObject( rexObject& /*object*/, rexValue /*value*/, const atArray<rexObject*>& /*objects*/  ) const { return rexResult( rexResult::PERFECT ); } 
	virtual rexResult		ModifyObject( rexObject& /*object*/, rexValue /*value*/, const atArray<rexObject*>& /*objects*/  ) const { return rexResult( rexResult::PERFECT ); } 
	virtual rexResult		ModifyProcessedObject( rexObject& /*object*/, rexValue /*value*/, const atArray<rexObject*>& /*objects*/  ) const { return rexResult( rexResult::PERFECT ); } 
	virtual rexResult		VerifyObject( rexObject& /*object*/, rexValue /*value*/, const atArray<rexObject*>& /*objects*/  ) const { return rexResult( rexResult::PERFECT ); } 

	virtual rexProperty*	CreateNew() const=0;
protected:
	rexProperty() {}
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
