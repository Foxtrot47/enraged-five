// 
// rexBase/skeleton.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX datBase Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexSkeleton -- abstract class for passing skeleton information 
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_SKELETON_H__
#define __REX_BASE_SKELETON_H__

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexSkeleton
{
protected:
	rexSkeleton() {}
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
