// 
// rexBase/aconverter.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX datBase Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverter -- abstract class with common interface for object conversion
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_CONVERTER_H__
#define __REX_BASE_CONVERTER_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "logAndErrorGenerator.h"
#include "result.h"
#include "atl/array.h"
#include "atl/string.h"

namespace rage {

class rexObject;

/////////////////////////////////////////////////////////////////////////////////////

class rexConverter : public rexLogAndErrorGenerator
{
public:
	rexConverter() : rexLogAndErrorGenerator()  
	{ 
		m_ProgressBarObjectCountCB = m_ProgressBarObjectCurrentIndexCB = NULL; 
		m_ProgressBarTextCB = NULL; 
		m_bAuthoredOrientation = true;
		m_bGenBoneIdFromFullPath = false;
		m_bCombiningAcrossBones = false;
	}
	
	virtual rexResult		Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const = 0;
	virtual rexConverter*	CreateNew() const=0;

	virtual void			RegisterProgressBarObjectCountCallback( rexProgressBarCallback cb )			{ m_ProgressBarObjectCountCB = cb; }
	virtual void			RegisterProgressBarObjectCurrentIndexCallback( rexProgressBarCallback cb )	{ m_ProgressBarObjectCurrentIndexCB = cb; }
	virtual void			RegisterProgressBarTextCallback( rexProgressBarTextCallback cb )			{ m_ProgressBarTextCB = cb; }

	struct rexConverterInstanceAttr
	{
		atString	m_Name;
		atString	m_Type;
	};

	int								GetInstanceAttrCount() const { return m_InstanceAttributes.GetCount(); }
	const rexConverterInstanceAttr*	GetInstanceAttr(int attrIdx) const { return &m_InstanceAttributes[attrIdx]; }
	const rexConverterInstanceAttr*	GetInstanceAttr(const atString& attrName) const 
	{
		for(int a=0;a<m_InstanceAttributes.GetCount(); a++)
		{
			if(m_InstanceAttributes[a].m_Name == attrName)
				return &m_InstanceAttributes[a];
		}
		return NULL;
	}
	bool AppendInstanceAttr(const atString& attrName, const atString& attrType)
	{
		if(GetInstanceAttr(attrName))
			return false;
		else
		{
			rexConverterInstanceAttr& newAttr = m_InstanceAttributes.Grow();
			newAttr.m_Name = attrName;
			newAttr.m_Type = attrType;
			return true;
		}
	}

	void SetAuthoredOrientation(bool bVal) { m_bAuthoredOrientation = bVal; }
	bool GetAuthoredOrientation() const { return m_bAuthoredOrientation; }

	void SetGenBoneIdFromFullPath(bool bVal) { m_bGenBoneIdFromFullPath = bVal; }
	bool GetGenBoneIdFromFullPath() const { return m_bGenBoneIdFromFullPath; }

	void SetCombineAcrossBones(bool CombineAcrossBones) { m_bCombiningAcrossBones = CombineAcrossBones; }
	bool GetCombineAcrossBones() const { return m_bCombiningAcrossBones; }

protected:

	rexProgressBarCallback m_ProgressBarObjectCountCB, m_ProgressBarObjectCurrentIndexCB;
	rexProgressBarTextCallback m_ProgressBarTextCB;

	atArray<rexConverterInstanceAttr>	m_InstanceAttributes;

	bool	m_bAuthoredOrientation;
	bool	m_bGenBoneIdFromFullPath;
	bool	m_bCombiningAcrossBones;
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
