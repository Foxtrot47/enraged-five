// 
// rexBase/processorr.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX datBase Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexProcessor -- abstract class with common interface for object processing
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_PROCESSOR_H__
#define __REX_BASE_PROCESSOR_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "logAndErrorGenerator.h"
#include "result.h"
#include "atl/array.h"
#include "atl/string.h"

namespace rage {

class rexObject;

/////////////////////////////////////////////////////////////////////////////////////

class rexProcessor : public rexLogAndErrorGenerator
{
public:
	rexProcessor() : rexLogAndErrorGenerator()  { m_ProgressBarObjectCountCB = m_ProgressBarObjectCurrentIndexCB = NULL; m_ProgressBarTextCB = NULL; }
	
	virtual rexResult		Process( rexObject* object ) const = 0;
	virtual rexProcessor*	CreateNew() const=0;

	virtual void			RegisterProgressBarObjectCountCallback( rexProgressBarCallback cb )			{ m_ProgressBarObjectCountCB = cb; }
	virtual void			RegisterProgressBarObjectCurrentIndexCallback( rexProgressBarCallback cb )	{ m_ProgressBarObjectCurrentIndexCB = cb; }
	virtual void			RegisterProgressBarTextCallback( rexProgressBarTextCallback cb )			{ m_ProgressBarTextCB = cb; }

protected:

	rexProgressBarCallback m_ProgressBarObjectCountCB, m_ProgressBarObjectCurrentIndexCB;
	rexProgressBarTextCallback m_ProgressBarTextCB;

};


/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
