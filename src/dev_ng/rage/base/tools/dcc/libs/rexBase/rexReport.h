// 
// rexBase/rexReport.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#pragma once
#ifndef DEF_REXREPORT
#define DEF_REXREPORT
#include "atl/string.h"
#include "file/stream.h"
#include "atl/map.h"

#include "rexReportFile.h"

//typedef struct _xmlTextWriter xmlTextWriter;
typedef ::rage::atArray< RexReportSection, 0, ::rage::u16> RexReportSectionArray;

//=============================================================================
// rexReport
// PURPOSE
//   This calls provides some simple functions for generating a report 
//   during export.
//
//	To use this class first call:
//
//		rexReport::Open(<filename of html file to write report too>);
//
//	Then at any point after that in the plugin call:
//
//		rexReport::Write(<string to report>)
//
//	And then at the end of the export call rexReport::Close().
//
//	Unless a path is provided to Open, then the the class does nothing and it is safe
//	to call it any and everywhere.  This provides a simple way to globally disable
//	or enable the reporting.
//
// 


namespace rage {

	class rexReport
	{
	public:
		rexReport(void);
		~rexReport(void);

		// Open and closing
		bool Open(const char *ReportFilename);
		bool Close();

		// Modifiers
		// Add Entry to MtlMap Section
		bool AddToMaterialIDList(const char *objectName, const char *mtlName, int shaderIndex);

		// Outputing
		bool Write(const atString& strStringToOutput);
		bool Write(const char *fmt, ...);
		// To get the name of the scene being exported
		 atString	GetSceneName();
	private:

		RexReportSection &GetSectionByName(char *sectionName);
		// Output string
		bool OutputString(const atString& strStringToOutput, fiStream* fout = NULL);

		// Filename info
		atString m_ReportFilename;

		RexReportFile *m_pReportFile;

		// To see if the  Report is enabled or not
		//	Unless a path is provided to Open, then the  does nothing and it is safe
		//	to call it any and everywhere.  This provides a simple way to globally disable
		//	or enable the s.
		bool	IsReportEnabled() {return NULL!=m_pReportFile;}
	};

} // namespace rage

#endif //DEF_REXREPORT