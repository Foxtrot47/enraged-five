//
// rexBase/utility.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "rexBase/crc.h"
#include "rexBase/utility.h"
#include "rexBase/shell.h"

#include "atl/map.h"
#include "atl/string.h"
#include "devil/il.h"
#include "devil/ilu.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/stream.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "math/random.h"

#include "system/xtl.h"
#include <io.h>
#include <wtypes.h>

///////////////////////////////////////////////////////////////////////////////////////////

namespace
{
	static inline int isPow2(int n) 
	{
		int m;
		n = (n < 0 ? -n : n);

		for(m = 1; m < n; m *= 2) { ; }

		return( (n < m) ? 0 : 1 );
	}
}

///////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

template bool rexUtility::MakeOptimizedBoundingBox( atArray<Vector3>& v, Matrix34& bbMatrix, float& bbLength, float& bbHeight, float& bbWidth );
template bool rexUtility::MakeOptimizedBoundingSphere( atArray<Vector3>& v, Vector3& center, float& radius );
template bool rexUtility::MakeOptimizedBoundingSphere( atArray<Vector3,0,unsigned>& v, Vector3& center, float& radius );

bool rexUtility::MakeOptimizedBoundingSphereFromBBox( const Vector3& min, const Vector3& max, Vector3& center, float& radius )
{
	int count = 8;

	Mgc::Vector3 *pPoints = new Mgc::Vector3[count];

	pPoints[0] = Mgc::Vector3(min.x,min.y,min.z);
	pPoints[1] = Mgc::Vector3(max.x,min.y,min.z);
	pPoints[2] = Mgc::Vector3(min.x,max.y,min.z);
	pPoints[3] = Mgc::Vector3(max.x,max.y,min.z);
	pPoints[4] = Mgc::Vector3(min.x,min.y,max.z);
	pPoints[5] = Mgc::Vector3(max.x,min.y,max.z);
	pPoints[6] = Mgc::Vector3(min.x,max.y,max.z);
	pPoints[7] = Mgc::Vector3(max.x,max.y,max.z);

	Mgc::PointArray points(count, pPoints);

	// Find the best-fit sphere.
	Mgc::Sphere sphere = Mgc::MinSphere(points.m_iQuantity, points.m_akPoint);

	delete[] pPoints;

	// Store the center and radius.
	center.Set((float)sphere.Center().x, (float)sphere.Center().y, (float)sphere.Center().z);
	radius = (float)sphere.Radius();

	return true;
}

atString rexUtility::FilenameFromPath( const char * path )
{
	int lastSlash = -1;
	int pathLen = (int) strlen(path);
	for( int a = 0; a < pathLen; a++ )
	{
		if( path[ a ] == '/' || path[ a ] == '\\' )
			lastSlash = a;
	}
	return ( lastSlash >= 0 ) ? atString( (const char*)path + lastSlash + 1 ) : atString(path);
}

atString rexUtility::ToLowerCase( const char * path )
{
	char buffer[4096];
	strcpy( buffer, path );
	strlwr( buffer );
	return atString( buffer );
}

void rexUtility::FileCopy( const char * src, const char * dest )
{
	CopyFile( src, dest, false );
}

void rexUtility::PopPath( atString& path )
{
	int lastSlash = -1;
	int pathLen = path.GetLength();
	for( int a = 0; a < pathLen; a++ )
	{
		if( path[ a ] == '/' || path[ a ] == '\\' )
			lastSlash = a;
	}

	if( lastSlash >= 0 )
	{
		atString p( path );
		path.Set( p, 0, lastSlash );
	}
}

int rexUtility::FileNameCompare( const atString& file1, const atString& file2 )
{
	atString f1 = FilenameFromPath( file1 );
	atString f2 = FilenameFromPath( file2 );
	return strcmpi( f1, f2 );
}

atString rexUtility::BaseFilenameFromPath( const char * path )
{
	atString filename(FilenameFromPath( path ));
	atString retString;
	atString extension = ExtensionFromPath( path );
	if ( extension.GetLength() > 0)
		retString.Set( filename, 0, filename.GetLength() - extension.GetLength() - 1);
	else
		retString.Set( filename, 0, filename.GetLength() );

	return retString;
}

atString rexUtility::ExtensionFromPath( const char * path )
{
	int lastPeriod = -1;
	int pathLen = (int) strlen(path);
	for( int a = 0; a < pathLen; a++ )
	{
		if( path[ a ] == '.' )
			lastPeriod = a;
	}
	return ( lastPeriod >= 0 ) ? atString( ((const char*)path) + lastPeriod + 1 ) : atString();
}

atString rexUtility::PathFromPath( const char * path )
{
	atString retString;
	retString.Set( path, (int) strlen(path) - FilenameFromPath( path ).GetLength(), 0 );
	return retString;
}

///////////////////////////////////////////////////////////////////////////////////////////

bool rexUtility::FileExists( const char * filename )
{
	_finddata_t fileInfo;
	intptr_t hdl = _findfirst( filename, &fileInfo );
	_findclose( hdl );

	return (hdl != -1);
}

///////////////////////////////////////////////////////////////////////////////////////////


static bool CreateAssetDirectory(const char *dir)
{
	// First off, is the folder just a load of .s and /s?  If so it is a parent folder and cannot be created
	bool bValidToCreate = false;
	for(unsigned i=0; i<strlen(dir); i++)
	{
		if((dir[i] != '/') && (dir[i] != '\\') && (dir[i] != '.'))
		{
			// Valid
			bValidToCreate = true;
			break;
		}
	}
	if(!bValidToCreate)
	{
		return true;
	}

	// Director does not exist - lets create the directory
	SECURITY_ATTRIBUTES securityAttr;
	memset( &securityAttr, 0, sizeof(SECURITY_ATTRIBUTES) );
	securityAttr.nLength = sizeof(SECURITY_ATTRIBUTES);

	bool bSuccess = (CreateDirectory(dir, &securityAttr) == TRUE);

	// Did it work?
	if(!bSuccess)
	{
		// An error occured creating the directory
		LPVOID lpMsgBuf;
		int iErrorCode = GetLastError();
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
				iErrorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL );
		Warningf("Failed to create folder '%s' error code %d : %s",dir,iErrorCode,lpMsgBuf);

		// Free the buffer.
		LocalFree( lpMsgBuf );
		return false;
	}
	return true;
}

static bool VerifyDirectory(const char *dir)
{
	// Remove trailing slashes
	char path[256];
	strcpy(path, dir);
	int len = (int)strlen(path) - 1;
	if( (path[len] == '/') || (path[len] == '\\') )
		path[len] = '\0';

	// Verify if the directory "dir" exists
	_finddata_t dirData;
	intptr_t hdl = _findfirst( path, &dirData );
	_findclose( hdl );	// Free the handle - we have the info that we need

	// Directory does not exist
	if( hdl == -1 )
		return false;
	else
	{
		// a file with the correct name exists - make sure that it's a directory
		if( ! (dirData.attrib & _A_SUBDIR) )
			return false;
	}
	return true;
}

bool rexUtility::CreatePath( const char * pathname_ )
{
	if( !pathname_ || !pathname_[0] || rexShell::DoesntSerialise())
		return false;

	// Translate the filename to use "/", if "\" is used.
	atString pathname;
	ConvertBackslashesToSlashes(pathname_, pathname);
	// if last char of pathname isn't slash, append one (for parsing)
	if( pathname[ pathname.GetLength() - 1 ] != '/' )
		pathname += '/';

	// Get the maya file path.
	char filePath[256];
	const char* temp = strrchr( pathname, '/' );
	int pos = (int)strlen(pathname);
	if( temp != NULL )
		pos = (int)(temp - pathname);

	strncpy( filePath, pathname, pos );
	strncpy( filePath+pos, "\0", 1);

	// Verify and create dir
	char assetDir[256];
	char* dir = strtok(filePath, "/");
	strcpy(assetDir, dir);
	dir = strtok(NULL, "/");
	if( dir == NULL )
	{
		if( !VerifyDirectory(assetDir) )
		{
			if( !CreateAssetDirectory(assetDir) )
			{
				return false;
			}
		}
	}

	while( dir != NULL )
	{
		strcat(assetDir, "/");
		strcat(assetDir, dir);

		if( !VerifyDirectory(assetDir) )
		{
			if( !CreateAssetDirectory(assetDir) )
			{
				return false;
			}
		}

		dir = strtok(NULL, "/");
	}
	return true;
}

///////////////////////////

atString rexUtility::RemoveNonAlphabeticCharacters( const char * s )
{
	atString retval;
	int len = (int) strlen(s);
	for( int a = 0; a < len; a++ )
	{
		char c = ((const char*)s)[a];
		if( isalpha( c ) )
		{
			retval += c;
		}
	}
	return retval;
}

atString rexUtility::RemoveWhitespaceCharacters( const char * s )
{
	int len = (int) strlen(s);
	atString retval;
	for( int a = 0; a < len; a++ )
	{
		char c = ((const char*)s)[a];
		if( !isspace( c ) )
		{
			retval += c;
		}
	}
	return retval;
}

bool rexUtility::ContainsNonAlphabeticCharacters( const char * s )
{
	int len = (int) strlen(s);
	for( int a = 0; a < len; a++ )
	{
		if( !isalpha( ((const char*)s)[a] ) )
			return true;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static rexUtilityTextureExportFunction	s_TextureExportFunction = NULL;
static rexUtilityTextureExportWantedFunction s_TextureExportWantedFunction = NULL;
static atString							s_TextureListFileName;
static fiStream*						s_TextureListFile = NULL;
static bool							    s_TextureConvertFlag = true;  // export textures as default
static bool							    s_HackTimeStampsOfGeneratedTextures = true;  // export textures as default

ILuint									s_DevilConvertImageNames[2];
bool									s_DevilBegun;


void rexUtility::SetTextureConvertFlag(bool f)
{
	s_TextureConvertFlag = f;
}

void rexUtility::SetHackTimeStampsOfGeneratedTextures(bool f)
{
	s_HackTimeStampsOfGeneratedTextures = f;
}

bool rexUtility::IsTextureConvertDisabled()
{
	return s_TextureConvertFlag;
}

bool rexUtility::HackTimeStampsOfGeneratedTextures()
{
	return s_HackTimeStampsOfGeneratedTextures;
}

void rexUtility::SetExportTextureFunction( rexUtilityTextureExportFunction func )
{
	s_TextureExportFunction = func;
}

void rexUtility::SetTextureExportWantedFunction( rexUtilityTextureExportWantedFunction function)
{
    s_TextureExportWantedFunction = function;
}

rexResult rexUtility::LoadDevilImage(const char* fileName)
{
	rexResult result(rexResult::PERFECT);

	const char *plus = strchr(fileName,'+');
	if (plus)
	{
		char diffuse[512], opacity[512];
		safecpy(diffuse, fileName, (int)(plus - fileName + 1));
		safecpy(opacity, plus + 1, sizeof(opacity));

		// Bind diffuse map
		ilBindImage(s_DevilConvertImageNames[0]);
		if (ilLoadImage((ILstring) diffuse)==FALSE)
		{
			result = rexResult::WARNINGS;
			result.AddWarning("Texture conversion warning. Failed to load diffuse map '%s'", diffuse);
			return result;
		}

		if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
		{
			result = rexResult::WARNINGS;
			result.AddWarning("Texture conversion warning. Unable to convert diffuse map '%s' to RGBA", diffuse);
			return result;
		}
		ILubyte *diffuseData = ilGetData();
		ILuint diffuseWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
		ILuint diffuseHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
		ILint diffuseFormat = ilGetInteger(IL_IMAGE_FORMAT);

		// Bind opacity map
		ilBindImage(s_DevilConvertImageNames[1]);
		if (ilLoadImage((ILstring) opacity)==FALSE)
		{
			ilBindImage(s_DevilConvertImageNames[0]);
			result = rexResult::WARNINGS;
			result.AddWarning("Texture conversion warning.  Failed to load opacity map '%s'", opacity);
			return result;
		}
		if (ilConvertImage(IL_RGBA,IL_UNSIGNED_BYTE) == FALSE)
		{
			result = rexResult::WARNINGS;
			result.AddWarning("Texture conversion warning. Unable to convert opacity map '%s' to RGBA", opacity);
			return result;
		}
	
		ILuint opacityWidth = (ILuint) ilGetInteger(IL_IMAGE_WIDTH);
		ILuint opacityHeight = (ILuint) ilGetInteger(IL_IMAGE_HEIGHT);
		ILuint opacityDepth = (ILuint) ilGetInteger(IL_IMAGE_DEPTH);
		ILint opacityFormat = ilGetInteger(IL_IMAGE_FORMAT);
		ILboolean success = true;

		// Check to make sure they're compatible
		if (opacityWidth != diffuseWidth || opacityHeight != diffuseHeight)
		{
			iluImageParameter(ILU_FILTER,ILU_SCALE_LANCZOS3);
			success = iluScale(diffuseWidth,diffuseHeight,opacityDepth);
		}

		ILubyte *opacityData = ilGetData();
		ilBindImage(s_DevilConvertImageNames[0]);

		if (opacityFormat != IL_RGBA || diffuseFormat != IL_RGBA)
		{
			result = rexResult::WARNINGS;
			result.AddWarning("Texture conversion warning. Devil fn 'ilConvertImage' failed.");
			return result;
		}

		// Merge alpha channel from opacity map into alpha channel of diffuse map
		ILuint count = diffuseWidth * diffuseHeight;
		while (count--)
		{
			// TODO: Could trivially support premultiplied alpha here
			// diffuseData[0] = diffuseData[0];
			// diffuseData[1] = diffuseData[1];
			// diffuseData[2] = diffuseData[2];
			// TODO: This doesn't seem right; I'm taking green out of the opacity map
			// and feeding it into diffuse alpha because the opacity map itself doesn't
			// seem to have transparency.  Arguably I should be using 5/8 green, 2/8 red,
			// and 1/8 blue or some other YUV intensity approximation.
			diffuseData[3] = opacityData[1];
			diffuseData += 4;
			opacityData += 4;
		}

		return result;
	}
	else
	{
		if(ilLoadImage((ILstring) (const char *)fileName))
		{
			return result;
		}
		else
		{
			result = rexResult::WARNINGS;
			result.AddWarning("Texture conversion warning. Failed to load image '%s'", fileName);
			return result;
		}
	}
}

rexResult rexUtility::ConvertAndSaveDevilImage(const char* outputFileName,bool hasAlpha)
{
	rexResult result(rexResult::PERFECT);

	int format = hasAlpha ? IL_DXT5 : IL_DXT1;	// Use this to control which DXT method to use
									// NOTE -- OUR CODE IGNORANTLY KEYS OFF OF ***NAME*** TO
									//	DETERMINE IF TEXTURE HAS ALPHA, WHICH IS TOTALLY WRONG
									//	IGNORING IN LOOP CODE BELOW, AND SET FORMAT BASED ON ALPHA CONTENT

	// TODO: DXT format is generally bad for normal maps --
	//		should we default to uncompressed or compressed???

	// Auto switch format based on alpha content
	format = IL_DXT1;
	ILubyte *work = ilGetData();
	ILuint count = (ILuint) ilGetInteger(IL_IMAGE_WIDTH) * ilGetInteger(IL_IMAGE_HEIGHT);
	ILint val = ilGetInteger(IL_IMAGE_FORMAT);
	if ( val == IL_RGBA || val == IL_BGRA ) {
		for (ILuint i = 0; i < count; ++i) {
			if ( work[3] != 255 ) {
				format = IL_DXT5;
				break;
			}
			work += 4;
		}
	}

	ilSetInteger(IL_DXTC_FORMAT, format);					// Set the dxt format
	ilEnable(IL_FILE_OVERWRITE);							// Set this to overwrite existing file
	iluImageParameter(ILU_FILTER, ILU_SCALE_LANCZOS3);		// filter is Lanczos, same as old image library
	iluBuildMipmaps();										// Generate mip-maps

	ILboolean bSaveRes = ilSaveImage((ILstring) (const char *)outputFileName);	// USE THIS VERSION OF SAVE!  other versions don't
	if(!bSaveRes)
	{
		result = rexResult::WARNINGS;
		result.AddWarning("Texture conversion warning. Failed to write converted texture '%s'", outputFileName);
		return result;
	}

	return result;
}

#if HACK_GTA4
rexResult rexUtility::GenerateColourPaletteFromArray( atArray<Vector4, 0, u32>& vertColList, atArray<ColourQuantizer::ColourRGB>& colourPalette, atArray<int, 0, u32>& palIdxList, u32 maxPaletteSize )
{
	
	rexResult result(rexResult::PERFECT);
	atArray<ColourQuantizer::ColourRGB, 0, u32> rgbVertColList;
	for( int i=0; i<vertColList.GetCount(); i++ )
	{
		ColourQuantizer::ColourRGB colRGB( (unsigned long)(vertColList[i].x*255), (unsigned long)(vertColList[i].y*255), (unsigned long)(vertColList[i].z*255) );
		rgbVertColList.PushAndGrow(colRGB);
	}
	
	ColourQuantizer colourQuant;
	int palIndex = 0;

	colourQuant.BuildOctree( rgbVertColList );
	colourQuant.ReduceTreeToSpecifiedLeafCount( maxPaletteSize );
	colourQuant.MakePaletteTable( colourQuant.GetRootNode(), colourPalette, &palIndex );
	for( int i=0; i<rgbVertColList.GetCount(); i++ )
	{
		int colQ = colourQuant.QuantizeColor( colourQuant.GetRootNode(), rgbVertColList[i] );
		palIdxList.PushAndGrow( colQ );
	}


	return result;
}

rexResult rexUtility::GenerateImageFromColourPalette( u32 /*maxPaletteSize*/, u32 minWidthPixels, const char* paletteFile, atArray<ColourQuantizer::ColourRGB> colourPalette )
{
	rexResult result(rexResult::PERFECT);

	// Write out the colour palette
	ilInit();
	iluInit();

	ILuint imagename;
	int minPaletteCount = 4;
	ilGenImages( 1, &imagename );
	ilBindImage(imagename);

	ilEnable(IL_CONV_PAL);
	ilTexImage( minWidthPixels, minPaletteCount, 1, 4, IL_RGBA, IL_UNSIGNED_BYTE, NULL);
	ilSetInteger(IL_DXTC_FORMAT, IL_DXT_NO_COMP);	
	ilEnable(IL_FILE_OVERWRITE);
	ILubyte *work = ilGetData();

	ColourQuantizer::ColourRGB magicColour(255, 127, 255);
	for( int i=minPaletteCount-1; i>=0; i--)
	{
		for( int j=0; j<(int)minWidthPixels; j++ )
		{
			// Set all rows to be same as the first (default)
			int index = j;
			if( index < colourPalette.GetCount() )
			{
				//  If a pixel is the magic colour, increment the green channel 
				//	to avoid a false palette termination at runtime
				if( magicColour == colourPalette[index] )
					colourPalette[index].g++;

				work[0] = (ILubyte)(colourPalette[index].r);
				work[1] = (ILubyte)(colourPalette[index].g);
				work[2] = (ILubyte)(colourPalette[index].b);
				work[3] = (ILubyte)(255);
			}
			else
			{
				work[0] = (ILubyte)255;
				work[1] = (ILubyte)127;
				work[2] = (ILubyte)255;
				work[3] = (ILubyte)255;
			}
			work+=4;
		}
	}


	ILboolean bSaveRes = ilSaveImage( (ILstring) paletteFile );	// USE THIS VERSION OF SAVE!  other versions don't

	ilBindImage(0);
	ilDeleteImages(1, &imagename) ;
	ilShutDown();

	if(!bSaveRes)
	{
		result.Combine(rexResult::WARNINGS);
		result.AddWarning("Could not save palette %s.", paletteFile);
	}

	return result;
}
#endif // HACK_GTA4

void rexUtility::BeginDevil()
{
	Assert(s_DevilBegun==false);

	ilInit();
	iluInit();
	s_DevilBegun=true;
	// We need to bind images in order to clean up their memory later
	ilGenImages(2, s_DevilConvertImageNames);
	ilBindImage(s_DevilConvertImageNames[0]);
	ilEnable(IL_CONV_PAL);
}

void rexUtility::EndDevil()
{
	Assert(s_DevilBegun==true);

	// Unbind this image & delete it
	ilBindImage(0);
	ilDeleteImages(2, s_DevilConvertImageNames) ;
	s_DevilBegun=false;
	ilShutDown();
}

rexResult rexUtility::ExportTexture( const char * inputFileName, const char * outputFileName, bool hasAlpha, bool isPS2Lightmap, const char *uiHint )
{
	rexResult result(rexResult::PERFECT);
	
	// output texture to texture list:
	// this file contains a list of all textures USED in the scene.
	if (s_TextureListFile!=NULL)
	{
		const char* str=inputFileName;
		char newlinex[] = { 0x0d, 0x0a };
		s_TextureListFile->PutCh('"');
		s_TextureListFile->Write(str,(int)strlen(str));
		s_TextureListFile->Write("\",\"", 3);
		str = outputFileName;
		s_TextureListFile->Write(str,(int)strlen(str));
		s_TextureListFile->Write("\",\"", 3);
		str = uiHint;
		s_TextureListFile->Write(str,(int)strlen(str));
		s_TextureListFile->PutCh('\"');
		s_TextureListFile->Write(newlinex,2);
	}

	if (IsTextureConvertDisabled() || rexShell::DoesntSerialise())
	{
		result.AddMessage("Texture conversion disabled, textures will not be exported.");
		return rexResult::PERFECT;
	}

    if (s_TextureExportWantedFunction)
    {
        if (!(s_TextureExportWantedFunction( inputFileName, outputFileName)))
        {
            result.AddMessage("Texture '%s' does not need to be re-exported.", (const char*)inputFileName);
            return rexResult::PERFECT;
        }
    }

	// export the texture:
	if( s_TextureExportFunction )
	{
		result.Combine((*s_TextureExportFunction)( inputFileName, outputFileName, hasAlpha, isPS2Lightmap ));
	}
	else
	{
		// We need to bind images in order to clean up their memory later
		BeginDevil();

		try
		{
			// Now convert
			result.Combine( LoadDevilImage(inputFileName) );
			if(!result.Errors())
			{
				//First perform some basic validation on the image size
				ILuint width = ilGetInteger(IL_IMAGE_WIDTH);
				ILuint height = ilGetInteger(IL_IMAGE_HEIGHT);

				//(1) Make sure the texture width and height are a power of 2
				if(!isPow2((int)width))
				{
					result.Combine(rexResult::WARNINGS);
					result.AddWarning("Texture conversion error. The image '%s' has a width of %d which is not a power of 2, texture will not be converted.",
						(const char*)inputFileName, width);
				}
				if(!isPow2((int)height))
				{
					result.Combine(rexResult::WARNINGS);
					result.AddWarning("Texture conversion error. The image '%s' has a height of %d which is not a power of 2, texture will not be converted.",
						(const char*)inputFileName, height);
				}

				//(2) Make sure the texture's height is not greater than its width
				if( height > width )
				{
					result.Combine(rexResult::WARNINGS);
					result.AddWarning("Texture conversion warning. The image '%s' has a height of < %d > which is greater than its width < %d >.",
						(const char*)inputFileName, height, width);
				}

				if(!result.Errors())
					result.Combine(ConvertAndSaveDevilImage(outputFileName,hasAlpha));
			}
		}
		catch(...)
		{
			//there are some exceptions being triggered by the devil library
			//this catches them so the export doesnt crash...
			//...obviously there should be some handling for this but I'd
			//rather the exporter didnt fall over because of it

			result.Combine(rexResult::WARNINGS);
			result.AddWarning("Texture conversion warning. The image '%s' has caused an exception in devil.",(const char*)inputFileName);
		}

		EndDevil();
	}

	// output texture list to a file if export was successful:
	if (!result.Errors())
	{
		if(HackTimeStampsOfGeneratedTextures())
		{
			// set file time of output texture to the same time stamp as the (second) input texture:
			const fiDevice& device=fiDeviceLocal::GetInstance();
			const char *plus = strchr(inputFileName,'+');
			if (plus)
			{
				char diffuse[512], opacity[512];
				safecpy(diffuse, (const char*)inputFileName, (int)(plus - inputFileName + 1));
				safecpy(opacity, plus + 1, sizeof(opacity));
				u64 diffuseTimeStamp=device.GetFileTime(diffuse);
				u64 opacityTimeStamp=device.GetFileTime(opacity);
				// Choose the most recent (greatest) date if they differ.  This seems to make
				// the most sense to me for no particular reason (well, it seems useful to me to
				// make a file appear as new as possible for proper dependency checking)
				u64 mostRecent = (diffuseTimeStamp > opacityTimeStamp)? diffuseTimeStamp : opacityTimeStamp;
				if (!mostRecent || !device.SetFileTime(outputFileName,mostRecent))
				{
					result.Combine(rexResult::WARNINGS);
					result.AddWarning("Couldn't set file time for file '%s' using timestamp from either '%s' or '%s'",
						(const char *)outputFileName,diffuse,opacity);
				}
			}
			else 
			{
				u64 timeStamp=device.GetFileTime(inputFileName);
				if (!timeStamp || !device.SetFileTime(outputFileName,timeStamp))
				{
					result.Combine(rexResult::WARNINGS);
					result.AddWarning("couldn't set file time for file '%s' using timestamp from '%s'",(const char*)outputFileName,(const char*)inputFileName);
				}
			}
		}
	}

	return result;
}



void rexUtility::ConvertBackslashesToSlashes( const char * og, atString& dest )
{
	dest = "";
	const char* orig = (const char*)og;
	while( *orig != 0 )
	{
		const char* c = orig ++;
		dest += (*c == '\\') ? '/' : *c;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

// returns number of seconds since jan 1, 1901
// only valid until Feb 28, 2100

double rexUtility::GetTimestamp()
{
	SYSTEMTIME sysTimer;
	GetSystemTime(&sysTimer);

	static int numDaysInMonth[12]={31,28,31,30,31,30,31,31,30,31,30,31};

	int numWholeYears = sysTimer.wYear - 1901;

	// yes i know that this will be a day off after feb 28, 2100 :P
	int numWholeDays = 365 * numWholeYears + ( numWholeYears / 4 );
	int numWholeMonthsThisYear = sysTimer.wMonth - 1;
	for( int a = 0; a < numWholeMonthsThisYear; a++ )
		numWholeDays += ( numDaysInMonth[ a ] + ( ( a == 1 ) && !( sysTimer.wYear % 4 ) ) ); // leap year
	numWholeDays += ( sysTimer.wDay - 1 );

	double seconds = ( numWholeDays * 24.0 * 60.0 * 60.0 );
	seconds += ( sysTimer.wHour * 60.0 * 60.0 );
	seconds += ( sysTimer.wMinute * 60.0 );
	seconds += ( sysTimer.wSecond );
	seconds += ( sysTimer.wMilliseconds * .001 );
	return seconds;
}

// returns number of seconds since jan 1, 1901
// only valid until Feb 28, 2100

double rexUtility::GetFileTimestamp( const char * filename )
{
	WIN32_FIND_DATA finddata;
	HANDLE hnd = FindFirstFile( filename, &finddata );

	if( hnd && hnd != (HANDLE)0xFFFFFFFF )
	{
		const double NUM_SECONDS_BETWEEN_NYD_1601_AND_NYD_1901 = ( 300.0 * 365.0 + 72.0 ) * 24.0 * 60.0 * 60.0;
		LARGE_INTEGER fileBigNum;

		fileBigNum.LowPart = finddata.ftLastWriteTime.dwLowDateTime;
		fileBigNum.HighPart = finddata.ftLastWriteTime.dwHighDateTime;

		// comes in 100 ns intervals since jan 1 1601, thus multiply by 10^-7 and subtract difference to jan 1 1901
		double seconds = fileBigNum.QuadPart * 0.0000001 - NUM_SECONDS_BETWEEN_NYD_1601_AND_NYD_1901;
		return seconds;
	}
	return -1;
}

int	rexUtility::GetChecksum( const char * string, int numCycles )
{
	unsigned int checkValue = startCRC32;

	for( int a = 0; a < numCycles; a++ )
		checkValue = crc32( (unsigned char*)((const char*)string), (int) strlen(string), checkValue );

	return (int)checkValue;
}


///////////////////////////////////////////////////////////////////////////////
// Functions to make continuous change.
///////////////////////////////////////////////////////////////////////////////
void rexUtility::FindCloseAngle(const Vector3& iR, Vector3 &ioR, const char *order)
{
	float c2Pi = (float)(PI * 2.0);
	float c2PiInv = (float)(1.0 / c2Pi);

	Vector3 r1, r2;
	r1.Set(ioR.x, ioR.y, ioR.z);
	FindAlternate(r1, r2, order);

	// Find the solution in range [-PI, PI] of the newEuler.
	float x = r1.x + (float)floor((iR.x - r1.x + PI) * c2PiInv) * c2Pi;
	float y = r1.y + (float)floor((iR.y - r1.y + PI) * c2PiInv) * c2Pi;
	float z = r1.z + (float)floor((iR.z - r1.z + PI) * c2PiInv) * c2Pi;
	r1.Set(x, y, z);

	// Find the alternate solution in range [-PI, PI] of the newEuler.
	x = r2.x + (float)floor((iR.x - r2.x + PI) * c2PiInv) * c2Pi;
	y = r2.y + (float)floor((iR.y - r2.y + PI) * c2PiInv) * c2Pi;
	z = r2.z + (float)floor((iR.z - r2.z + PI) * c2PiInv) * c2Pi;
	r2.Set(x, y, z);

	// Compare E1 with E2 to determine which one is close to the oldE.
	Vector3 dR1 = r1 - iR;
	Vector3 dR2 = r2 - iR;
	if( (fabs(dR1.x) + fabs(dR1.y) + fabs(dR1.z)) < (fabs(dR2.x) + fabs(dR2.y) + fabs(dR2.z)) )
		ioR = r1;
	else
		ioR = r2;
}

void rexUtility::FindAlternate(Vector3& iR, Vector3& oR, const char *order)
{
	if( !strcmp(order, "xyz") || !strcmp(order, "zyx") )
	{
		oR.x = BoundAngle(PI + iR.x);
		oR.y = BoundAngle(PI - iR.y);
		oR.z = BoundAngle(PI + iR.z);
	}
	else if( !strcmp(order, "xzy") || !strcmp(order, "yzx") )
	{
		oR.x = BoundAngle(PI + iR.x);
		oR.y = BoundAngle(PI + iR.y);
		oR.z = BoundAngle(PI - iR.z);
	}
	else if( !strcmp(order, "zxy") || !strcmp(order, "yxz") )
	{
		oR.x = BoundAngle(PI - iR.x);
		oR.y = BoundAngle(PI + iR.y);
		oR.z = BoundAngle(PI + iR.z);
	}
}

float rexUtility::BoundAngle(float r)
{
	float q = r - ((int)(r * (1.0f / (2.0f * PI))) * (2.0f * PI));
	return( q >  PI ? q - (2.0f*PI) : (q < -PI ? q + (2.0f*PI) : q) );
}


void rexUtility::SetTextureListFileName(const char* name)
{
		s_TextureListFileName = name;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexUtility::SplitMeshWithBlends( atArray<mshMesh>& r_outputmeshes, mshMesh* p_InputMesh, bool preserveDeltas )
{
	int blendCount = p_InputMesh->GetBlendTargetCount();
	for( int l = 0; l < blendCount; l++ )
	{
		mshMesh newmesh;
		char buffer[1024];
		sprintf( buffer, "%i", l);
		
		newmesh.SetBoundSphere( p_InputMesh->GetBoundSphere() );
		newmesh.SetBoundBox( p_InputMesh->GetBoundBoxMatrix(), p_InputMesh->GetBoundBoxSize() );
		if (p_InputMesh->IsSkinned())
		{
			newmesh.MakeSkinned();
		}
		
		for( int p=0; p<p_InputMesh->GetMatrixCount(); p++ )
		{
			newmesh.AddOffset( p_InputMesh->GetOffset(p) ); 
		}
		
		for( int k = 0; k < p_InputMesh->GetMtlCount(); k++ )
		{
			mshMaterial &inputMat = p_InputMesh->GetMtl( k );
			mshMaterial &mtl = newmesh.NewMtl();
			for( int n = 0; n < inputMat.Prim.GetCount(); n++ )
			{
				mshPrimitive &newPrim = mtl.Prim.Append();
				newPrim = inputMat.Prim[n];
			}
			mtl.Name = inputMat.Name;
			for( int m = 0; m < inputMat.GetVertexCount(); m++ )
			{
				const mshBlendTargetDelta &blendTarget = inputMat.GetBlendTargetDelta( m, l );
				const mshVertex &vertexInfo = inputMat.GetVertex(m);
				Vector3 pos;
				Vector3 norm;

				if (preserveDeltas)
				{
					pos = Vector3( blendTarget.Position.x, blendTarget.Position.y, blendTarget.Position.z );
					norm = Vector3( blendTarget.Normal.x, blendTarget.Normal.y, blendTarget.Normal.z );
				}
				else
				{
					pos = Vector3( blendTarget.Position.x + vertexInfo.Pos.x, blendTarget.Position.y + vertexInfo.Pos.y, blendTarget.Position.z + vertexInfo.Pos.z);
					norm = Vector3( blendTarget.Normal.x + vertexInfo.Nrm.x, blendTarget.Normal.y + vertexInfo.Nrm.y, blendTarget.Normal.z + vertexInfo.Nrm.z );
				}

				norm.Normalize();
				Vector2* tex = new Vector2[vertexInfo.TexCount];
				for( int t = 0; t < vertexInfo.TexCount; t++ )
				{
					tex[t] = vertexInfo.Tex[t];
				}
				
				mtl.AddVertex(pos, vertexInfo.Binding, norm, vertexInfo.Cpv, vertexInfo.Cpv2, vertexInfo.Cpv3, vertexInfo.TexCount, tex );
				delete [] tex;
			}
			for( int i = 0; i < mshMaxTanBiSets; i++ )
			{
				mtl.ComputeTanBi(i);
			}
			r_outputmeshes.PushAndGrow( newmesh );
		}
	}
	return true;
}

static void CloseTextureListFile()
{
	if (s_TextureListFile)
	{
		s_TextureListFile->Close();
		s_TextureListFile=NULL;
	}
}

void rexUtility::Startup()
{
	CloseTextureListFile();

	if (s_TextureListFileName.GetLength())
	{
		// try creating the leading path:
		ASSET.CreateLeadingPath(s_TextureListFileName);

		s_TextureListFile = fiStream::Create(s_TextureListFileName);
		if (s_TextureListFile==NULL)
		{
			Errorf("couldn't create '%s'",s_TextureListFileName);
		}
		else
		{
			char newLine[] = { 0x0d, 0x0a };
			const char *comment = "# SOURCEFILE, DESTFILE, USAGE_HINT";
			s_TextureListFile->Write(comment, (int)strlen(comment));
			s_TextureListFile->Write(newLine, 2);
			s_TextureListFile->Write(newLine, 2);
		}
	}
}

void rexUtility::Shutdown()
{
	CloseTextureListFile();
}

} // namespace rage
