//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//
#include ".\rextimerlog.h"
#include <stdlib.h>
#include <time.h>

using namespace rage;

atString rexTimerLog::m_LogFilename("");
atString rexTimerLog::m_StartTimeAsString("");
atArray<rexTimerLog::rexTimerLogActiveTimer*>	rexTimerLog::m_ActiveTimers;

rexTimerLog::rexTimerLog(void)
{
}

rexTimerLog::~rexTimerLog(void)
{
}

bool rexTimerLog::Open(const char * LogFilename)
{
	if(m_LogFilename != "")
	{
		// A log is already open, something has gone bad, bail
		// I should report an error here, but I'm not sure how yet KR 1/21/05
		Displayf("rexTimerLog::Open - A log is already open, something has gone bad, bailing\n");
		return false;
	}

	m_LogFilename = LogFilename;

	// Get the time for logging
	__time64_t ltime;	
	_time64( &ltime );	
	char acBuffer[26]; 
	strcpy(acBuffer, _ctime64( &ltime ));
	acBuffer[24] = '\0'; 
	acBuffer[25] = '\0'; 
	m_StartTimeAsString = acBuffer;

	// Create a root timer to time EVERYTHING
	StartTimer("Timed Process");

	return true;
}

bool rexTimerLog::Close()
{
	//Displayf("The Timer stack looks like this at time of close:");
	//for(int i=0; i<m_ActiveTimers.GetCount(); i++)
	//{
	//	Displayf("%d : %s", i, m_ActiveTimers[i]->GetName() );
	//}

	if(!IsTimerLogEnabled())
	{
		// A log is not open, so do nothing
		return false;
	}

	// Create file
	fiStream* fout = fiStream::Create(m_LogFilename);

	// Output header
	OutputString("<TimedExport startTime=\"", fout);
	OutputString(m_StartTimeAsString, fout);
	OutputString("\" endTime=\"", fout);
	__time64_t ltime;	_time64( &ltime );	char acBuffer[26]; strcpy(acBuffer, _ctime64( &ltime )); acBuffer[24] = '\0'; acBuffer[25] = '\0'; 
	OutputString(acBuffer, fout);
	OutputString("\" computerName=\"", fout);
	OutputString(getenv("COMPUTERNAME"), fout);
	OutputString("\" userName=\"", fout);
	OutputString(getenv("USERNAME"), fout);
	OutputString("\">\n<mayaFile value=\"", fout);
	OutputString(GetSceneName(), fout);
	OutputString("\"></mayaFile>\n", fout);


	// Output total timer
	atString strHeader("\n<Timer name=\"");
	strHeader += m_ActiveTimers[0]->GetXMLSafeName();
	strHeader  += "\" time=\"";
	sprintf(acBuffer, "%f", m_ActiveTimers[0]->GetTimer().GetTime());
	strHeader += acBuffer;
	strHeader += "\">";
	OutputString(strHeader, fout);

	// Output everything I have timed so far
	OutputString(m_ActiveTimers[0]->GetChildTimerInfo(), fout);

	// Output footer
	OutputString("\n</Timer></TimedExport>", fout);

	// Close file
	fout->Close();

	m_LogFilename = "";
	return true;
}

bool rexTimerLog::StartTimer(const char * TimerName)
{
	if(!IsTimerLogEnabled())
	{
		// A log is not open, so do nothing
		return false;
	}

	// Add timer
	m_ActiveTimers.PushAndGrow(new rexTimerLogActiveTimer(TimerName));
	return true;
}

bool rexTimerLog::EndTimer(const char * TimerName)
{
	if(!IsTimerLogEnabled())
	{
		// A log is not open, so do nothing
		return false;
	}

	// The timer should be the last one in the array, if not, then a timer somewhere has not killed itself properly
	int iPosInArray = m_ActiveTimers.GetCount() - 1;
	if(m_ActiveTimers[iPosInArray]->GetName() != TimerName)
	{
		// The timer to end is not the latest timer, something bad has happen
		// I should report an error here, but I'm not sure how yet KR 1/21/05
		Displayf("rexTimerLog::EndTimer(\"%s\") - The timer is not the last timer to start, something has gone bad, bailing", TimerName);
		Displayf("The Timer stack looks like this:");
		for(int i=0; i<m_ActiveTimers.GetCount(); i++)
		{
			Displayf("%d : %s", i, m_ActiveTimers[i]->GetName() );
		}

		return false;
	}

	// Ok, get the time from the timer
//	printf("Time : %f %f\n", m_ActiveTimers[iPosInArray]->GetTimer().GetTime(), m_ActiveTimers[iPosInArray]->GetTimer().GetMsTime());
	float fTotalTime = m_ActiveTimers[iPosInArray]->GetTimer().GetTime();

	// Wrap the info I have about my children in my own info
	atString strMyPreviousChildInfo = m_ActiveTimers[iPosInArray]->GetChildTimerInfo();
	m_ActiveTimers[iPosInArray]->GetChildTimerInfo() = "\n<Timer name=\"";
	m_ActiveTimers[iPosInArray]->GetChildTimerInfo() += m_ActiveTimers[iPosInArray]->GetXMLSafeName();
	m_ActiveTimers[iPosInArray]->GetChildTimerInfo() += "\" time=\"";
	char acBuffer[16]; sprintf(acBuffer, "%f", fTotalTime);
	m_ActiveTimers[iPosInArray]->GetChildTimerInfo() += acBuffer;
	m_ActiveTimers[iPosInArray]->GetChildTimerInfo() += "\">";
	m_ActiveTimers[iPosInArray]->GetChildTimerInfo() += strMyPreviousChildInfo;
	m_ActiveTimers[iPosInArray]->GetChildTimerInfo() += ("\n</Timer>");

	// Append my info to my parents
	m_ActiveTimers[iPosInArray - 1]->GetChildTimerInfo() += m_ActiveTimers[iPosInArray]->GetChildTimerInfo();

	// Remove timer from array
	m_ActiveTimers.Delete(iPosInArray);

	//Displayf("I have just end the \"%s\" timer and the timer array looks like this:\n", TimerName);
	//for(int i=0; i<m_ActiveTimers.GetCount(); i++)
	//{
	//	Displayf("*******************************************************\n");
	//	Displayf("%d\t:\t%s\n", i, m_ActiveTimers[i]->GetName());
	//	Displayf("%s", m_ActiveTimers[i]->GetChildTimerInfo());
	//	Displayf("\n*******************************************************\n");
	//}

	return true;
}

bool rexTimerLog::OutputString(const char * strStringToOutput, fiStream* fout)
{
	if(!IsTimerLogEnabled())
	{
		// A log is not open, so do nothing
		return false;
	}

	bool bGivenFile = (fout != NULL);
	if(!bGivenFile)
	{
		// Not given a stream to write to, so just append to the current file
		// Open file for appending
		fout = fiStream::Open(m_LogFilename, false);
		fout->Seek(fout->Size());
	}
	
	// Write out data
    fout->Write(strStringToOutput, (int) strlen(strStringToOutput));

	if(!bGivenFile)
	{
		// I opened the file, so I better close it
		fout->Close();
	}
	return true;
}

atString rexTimerLog::rexTimerLogActiveTimer::GetXMLSafeName() const
{
	atString strXMLSafeName("");
	for(int i=0; i<GetName().GetLength(); i++)
	{
		switch(GetName()[i])
		{
			case '&'	:	strXMLSafeName += "&amp;"; break;
			case '\''	:	strXMLSafeName += "&apos;"; break;
			case '"'	:	strXMLSafeName += "&quot;"; break;
			case '<'	:	strXMLSafeName += "&lt;"; break;
			case '>'	:	strXMLSafeName += "&gt;"; break;
			case '\n'	:	break;
			case '\r'	:	break;
			default	:	strXMLSafeName += GetName()[i];
		}
	}
	return strXMLSafeName;
}
