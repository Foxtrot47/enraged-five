/////////////////////////////////////////////////////////////////////////////////////
//
//	rexShell -	this is a generic mechanism for registering modules and properties,
//				and for dispatching objects to these modules for processing.  please
//				refer to rex/rex.cpp for usage example.  
//				this is an abstract class and must be derived for real-world usage.
//				(see rexMaya/shell.h for maya version of derived shell)
//
/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/module.h"
#include "rexBase/object.h"
#include "rexBase/property.h"
#include "rexBase/shell.h"
#include "rexBase/utility.h"
#include "rexBase/value.h"

#include "system/timer.h"

// [CHRISK]
#include "rexBase/hamsterObject.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

rexHamsterObject*	s_HamsterObject = NULL;
bool				rexShell::s_DontSerialise = false;
atString			rexShell::s_TempExportPath;
rexReport			rexShell::s_RexReport;

void rexSetCurrentHamsterObject(rexHamsterObject* ho)
{
    s_HamsterObject = ho;
}

rexHamsterObject* rexGetCurrentHamsterObject()
{
    return s_HamsterObject;
}

rexShell::~rexShell()
{
	// clean up nodes
	while( m_MasterNodeList.GetCount() ) 
		delete m_MasterNodeList.Pop();

	// cleanup modules and properties in map
	atMap<atString, rexProperty*>::Iterator propertyIterator = m_PropertyMap.CreateIterator();
	propertyIterator.Start();
	while( !propertyIterator.AtEnd() )
	{
		delete propertyIterator.GetData();
		propertyIterator.Next();
	}

	atMap<atString, rexModule*>::Iterator moduleIterator = m_ModuleMap.CreateIterator();
	moduleIterator.Start();
	while( !moduleIterator.AtEnd() )
	{
		delete moduleIterator.GetData();
		moduleIterator.Next();
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexModule* rexShell::GetModule( const char * id ) const
{
	rexModule* const* m = m_ModuleMap.Access( id );
	return m ? *m : NULL;
}

rexResult rexShell::RegisterModuleBase( const char * id, rexFilter* filter, rexConverter* converter, atArray<rexProcessor*>* processors, rexSerializer* serializer, bool respectsClaims, int numObjectsAllowed )
{
	rexResult retval( rexResult::PERFECT );

	// module IDs must be purely alphabetical.  this allows Animation1 + Animation to refer to same type, and gives
	// more flexibility in setting up export rules
	if( rexUtility::ContainsNonAlphabeticCharacters( id ) )  
	{
		retval.Combine( rexResult::ERRORS, rexResult::INVALID_INPUT );
		retval.AddError( "Module IDs must not contain any non-alphabetical characters ( '%s' is invalid )", (const char*)id );
		return retval;
	}

	// this stuff handles the module group registration
	int groupID = ( m_ModuleGroupCurrent < 0 ) ? m_ModuleGroupCount : m_ModuleGroupCurrent; 

	if( m_ModuleGroupCurrent >= 0 )
	{
		m_ModuleGroupRespectsClaims.PushAndGrow( respectsClaims );
	}
	else
	{
		while( m_ModuleGroupRespectsClaims.GetCount() <= m_ModuleGroupCount )
		{
			m_ModuleGroupRespectsClaims.PushAndGrow( respectsClaims );
		}
		m_ModuleGroupCount++;
	}	

	// if there's already something added to this module, replace it
	if( m_ModuleMap.Access( id ) )
	{
		delete *m_ModuleMap.Access( id );
		m_ModuleMap.Delete( id );
	}
	
	atArray<rexProcessor*> newProcessorArray;
	int processorCount = processors ? processors->GetCount() : 0;
	for( int a = 0; a < processorCount; a++ )
	{
		newProcessorArray.PushAndGrow( (*processors)[a]->CreateNew(),(u16) processorCount );
	}
	rexModule* module = new rexModule( filter ? filter->CreateNew() : NULL, converter ? converter->CreateNew() : NULL, newProcessorArray, serializer ? serializer->CreateNew() : NULL, groupID, numObjectsAllowed );
	m_ModuleMap.Insert( atString(id), module );

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShell::RegisterPropertyBase( const char *id, const rexProperty* prop )
{
	rexResult retval( rexResult::PERFECT );
	
	// property IDs must be purely alphabetical.  this allows BoneTag1 + BoneTag to refer to same property type
	// and gives more flexibility in setting up export rules
	if( rexUtility::ContainsNonAlphabeticCharacters( id ) )  // this is so things like Animation1 will still go thru as Animation
	{
		retval.Combine( rexResult::ERRORS, rexResult::INVALID_INPUT );
		retval.AddError( "Property IDs must not contain any non-alphabetical characters ('%s' is invalid)", (const char*)id );
		return retval;
	}
	
	if( m_PropertyMap.Access( id ) )
	{
		delete *m_PropertyMap.Access( id );
		m_PropertyMap.Delete( id );
	}

	m_PropertyMap.Insert( atString(id), prop ? prop->CreateNew() : NULL );
	
	return retval;
}


rexResult	rexShell::FakeAnEntity(const atArray<rexObject*>& inputObjects, bool justExportMesh, bool justExportBound, const char* strPathToJustExportTo, atArray<ExportItemInfo>& ExportItems) const
{
    // [CHRISK]
    if (rexGetCurrentHamsterObject())
    {
        rexGetCurrentHamsterObject()->SetupExportItemInfos(ExportItems, inputObjects);

        return rexResult::PERFECT;
    }

	// Fake an entity
	ExportItemInfo obExportItemInfo;
	obExportItemInfo.m_ModuleID = "Entity";
	obExportItemInfo.m_ObjectNameDefault = "Hamster";
	obExportItemInfo.m_ModuleActive = true;

	obExportItemInfo.m_PropertyIDs.PushAndGrow(atString("OutputPath"));
	obExportItemInfo.m_PropertyStrings.PushAndGrow(atString(strPathToJustExportTo));
	obExportItemInfo.m_PropertyValues.PushAndGrow(atString(strPathToJustExportTo));
	obExportItemInfo.m_PropertyLocked.PushAndGrow(false);

	obExportItemInfo.m_PropertyIDs.PushAndGrow(atString("EntityTypeFileName"));
	obExportItemInfo.m_PropertyStrings.PushAndGrow(atString("FakeEntity"));
	obExportItemInfo.m_PropertyValues.PushAndGrow(atString("FakeEntity"));
	obExportItemInfo.m_PropertyLocked.PushAndGrow(false);

	obExportItemInfo.m_PropertyConnectedObjects.PushAndGrow(atArray<rexObject*>());
	obExportItemInfo.m_PropertyConnectedObjects.PushAndGrow(atArray<rexObject*>());

	if(justExportMesh)
	{
		// Fake up export item info
		ExportItemInfo obExportItemInfoForMesh;
		obExportItemInfoForMesh.m_ModuleID = "Mesh";
		obExportItemInfoForMesh.m_ObjectNameDefault = "Hamster";
		obExportItemInfoForMesh.m_ModuleActive = true;

		obExportItemInfoForMesh.m_PropertyIDs.PushAndGrow(atString("Nodes"));
		obExportItemInfoForMesh.m_PropertyStrings.PushAndGrow(atString(""));
		obExportItemInfoForMesh.m_PropertyValues.PushAndGrow(atString(""));
		obExportItemInfoForMesh.m_PropertyLocked.PushAndGrow(false);

		// Make a copy of inputObjects
		atArray< rexObject* > copyOfInputObjects;
		for(int i=0; i<inputObjects.GetCount(); i++)
		{
			copyOfInputObjects.Grow((u16) 1) = inputObjects[i]->CreateCopy();
		}

		obExportItemInfoForMesh.m_PropertyConnectedObjects.PushAndGrow(copyOfInputObjects);

		obExportItemInfo.m_Children.PushAndGrow(obExportItemInfoForMesh);
	}
	else if(justExportBound)
	{
		// Fake up export item info
		ExportItemInfo obExportItemInfoForBound;
		obExportItemInfoForBound.m_ModuleID = "Bound";
		obExportItemInfoForBound.m_ObjectNameDefault = "Hamster";
		obExportItemInfoForBound.m_ModuleActive = true;

		obExportItemInfoForBound.m_PropertyIDs.PushAndGrow(atString("Nodes"));
		obExportItemInfoForBound.m_PropertyStrings.PushAndGrow(atString(""));
		obExportItemInfoForBound.m_PropertyValues.PushAndGrow(atString(""));
		obExportItemInfoForBound.m_PropertyLocked.PushAndGrow(false);

		// Make a copy of inputObjects
		atArray< rexObject* > copyOfInputObjects;
		for(int i=0; i<inputObjects.GetCount(); i++)
		{
			copyOfInputObjects.Grow((u16) 1) = inputObjects[i]->CreateCopy();
		}

		obExportItemInfoForBound.m_PropertyConnectedObjects.PushAndGrow(copyOfInputObjects);

		obExportItemInfo.m_Children.PushAndGrow(obExportItemInfoForBound);
	}
	ExportItems.PushAndGrow(obExportItemInfo);

	return rexResult::PERFECT;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// main export function

rexResult rexShell::Export( const atArray<rexObject*>& inputObjects, bool justExportMesh, bool justExportBound, const char* strPathToJustExportTo )
{
	rexResult retval( rexResult::PERFECT );
	
	atArray<ExportItemInfo> ExportItems;

	if(justExportMesh || justExportBound || rexGetCurrentHamsterObject())
	{
		FakeAnEntity(inputObjects, justExportMesh, justExportBound, strPathToJustExportTo, ExportItems);
	}
	else
	{
		// let derived shell class execute any custom code desired here
		retval &= PreExport();
		if(!retval)
		{
			return retval;
		}

		// let derived shell class gather export item information (in Maya, these are specified via RexExportData nodes)
		retval &= GetExportItemInfo( inputObjects, ExportItems );
	}
		
	if( retval )
	{	
		// setup progress bars
		int ExportItemCount = ExportItems.GetCount();

		OpenProgressWindow( "REX Export", 3 );		
		UpdateProgressWindowMax( 0, ExportItemCount );
		UpdateProgressWindowValue( 0, 0 );
		UpdateProgressWindowValue( 1, 0 );
		UpdateProgressWindowValue( 2, 0 );

		// allow derived shell to execute custom item setup code here
		SetupExportItems( ExportItems );

		// export each item
		for( int a = 0; a < ExportItemCount; a++ )
		{
			// update progress bar text
			atString winString("Exporting ");
			winString += ExportItems[ a ].m_ModuleID;
			winString += " ";
			winString += ExportItems[ a ].m_ObjectNameDefault;
			UpdateProgressWindowInfo( 0, (const char*)winString );

			// export this item
			retval &= ExportItem( ExportItems[ a ] );
			
			// reclaim converted object memory
			if( ExportItems[ a ].m_ConvertedObject )
			{
				delete ExportItems[ a ].m_ConvertedObject;
				ExportItems[ a ].m_ConvertedObject = NULL;
			}

			// bail if error
			if( !retval )
				break;

            // update progress bar count
			UpdateProgressWindowValue( 0, a + 1 );
		}				

		// Cleanup whatever has set in SetupExportItems for the maya scene.
		CleanupExportItems( ExportItems );

		CloseProgressWindow();
	}

	// let derived shell class execute any custom code desired here
	if(!(justExportMesh || justExportBound || rexGetCurrentHamsterObject()))
	{
		retval &= PostExport();
	}
		
	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// exports one item

rexResult rexShell::ExportItem( ExportItemInfo& exportInfoRoot )
{
	rexResult retval;
    // Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);
	// initialize for this item
	Reset();
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);
	
	// progress bar update
	UpdateProgressWindowMax( 1, 4 );
	UpdateProgressWindowValue( 1, 0 );
	UpdateProgressWindowInfo( 1, "Dispatching Objects" );
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	ExternalProfileStart("SetupShellAndModuleInstances");

	// instantiate modules and apply appropriate properties to shell and modules
	retval  = SetupShellAndModuleInstances( exportInfoRoot, exportInfoRoot );
	if( !retval )
	{
		return retval;
	}
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	ExternalProfileEnd("SetupShellAndModuleInstances");
	ExternalProfileStart("CompileInputObjectLists");

	// determine which scene objects this item must process
	retval &= CompileInputObjectLists( exportInfoRoot );
	if( !retval )
	{
		return retval;
	}
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	ExternalProfileEnd("CompileInputObjectLists");
	ExternalProfileStart("ConvertObjects");

	// update progress bar
	UpdateProgressWindowValue( 1, 1 );
	UpdateProgressWindowInfo( 1, "Converting Objects" );
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	// gather appropriate scene objects and create generic objects (i.e. mesh, skeleton)
	retval &= ConvertObjects( exportInfoRoot );
	if( !retval )
	{
		return retval;
	}
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	ExternalProfileEnd("ConvertObjects");
	ExternalProfileStart("ApplyObjectModifications");

	// apply appropriate properties to converted (generic) objects
	retval &= ApplyObjectModifications( exportInfoRoot );
	if( !retval )
	{
		return retval;
	}
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	ExternalProfileEnd("ApplyObjectModifications");
	ExternalProfileStart("ProcessObjects");

	// update progress bar
	UpdateProgressWindowValue( 1, 2 );
	UpdateProgressWindowInfo( 1, "Processing Objects" );
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	// do special processing on objects
	retval &= ProcessObjects( exportInfoRoot );
	if( !retval )
	{
		return retval;
	}
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	ExternalProfileEnd("ProcessObjects");
	ExternalProfileStart("ApplyProcessedObjectModifications");

	// apply appropriate properties to processed (generic) objects
	retval &= ApplyProcessedObjectModifications( exportInfoRoot );
	if( !retval )
	{
		return retval;
	}
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	ExternalProfileEnd("ApplyProcessedObjectModifications");
	ExternalProfileStart("SerializeObjects");

	// update progress bar
	UpdateProgressWindowValue( 1, 3 );
	UpdateProgressWindowInfo( 1, "Serializing Objects to Disk" );
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	sysTimer T;
	// write converted generic objects to disk using appropriate final format (i.e. RAGE mesh file)
	retval &= SerializeObjects( exportInfoRoot );
	Displayf("SerializeObjects completed in %f seconds",T.GetTime());
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	ExternalProfileEnd("SerializeObjects");
	ExternalProfileStart("VerifyObjects");
	
	// make sure object fits specs
	retval &= VerifyObjects( exportInfoRoot );
	if( !retval )
	{
		return retval;
	}
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	// update progress bar
	UpdateProgressWindowValue( 1, 4 );
	UpdateProgressWindowInfo( 1, "Item Complete" );
	// Displayf("rexShell::ExportItem %s %d", __FILE__, __LINE__);

	ExternalProfileEnd("VerifyObjects");

	Report().Close();

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShell::SetupShellAndModuleInstances( ExportItemInfo& exportInfo, const ExportItemInfo& obTopMostParentExportItem, rexModule* parentModule )
{
	rexResult retval( rexResult::PERFECT );

	// retrieve correct module based on export item module ID
	rexModule** modulePtr = m_ModuleMap.Access( exportInfo.m_ModuleID );
	rexModule* module = modulePtr ? *modulePtr : NULL;
	
	rexModule* moduleInstance = NULL;

	if( module )
	{
		// instantiate module for use exclusively by this item (this prevents properties from being applied globally)
		moduleInstance = module->Clone();
		exportInfo.m_ModuleInstance = moduleInstance;

		// allow child modules to inherit input objects and output path (these can be overwritten via properties)
		if( parentModule )  
		{
			CopyModuleInputObjects( *moduleInstance, *parentModule );
			
			if( moduleInstance->m_Serializer )
			{
				moduleInstance->m_Serializer->SetOutputPath( parentModule->m_Serializer->GetOutputPath() );
				moduleInstance->m_Serializer->SetUseBoneIDs( parentModule->m_Serializer->GetUseBoneIDs() );
			}
		}
		
		// apply properties to shell and module instances
		int propertyCount = exportInfo.m_PropertyIDs.GetCount();
		Assert( ( propertyCount == exportInfo.m_PropertyValues.GetCount() ) && ( propertyCount == exportInfo.m_PropertyConnectedObjects.GetCount() ) );

		for( int b = 0; b < propertyCount; b++ )
		{
			// retrieve correct module based on export item property ID
			const rexProperty* const* property = m_PropertyMap.Access( exportInfo.m_PropertyIDs[ b ] );
		
			if( property && (*property) )
			{
				// do the actual application of properties
				retval &= (*property)->SetupShell( exportInfo.m_ObjectNameDefault, *this, exportInfo.m_PropertyValues[ b ], exportInfo.m_PropertyConnectedObjects[ b ] );
				retval &= (*property)->SetupModuleInstance( *moduleInstance, exportInfo.m_PropertyValues[ b ], exportInfo.m_PropertyConnectedObjects[ b ] );
			}	
			else if( !property )
			{
				// For debug reference the values of the following strings look like this:
				// obTopMostParentExportItem.m_strBaseRuleFile = Agent_SkyBox
				// obTopMostParentExportItem.m_ModuleID = Entity
				// obTopMostParentExportItem.m_strMaxOrMayaIdentityString = |_ExData_:RexExportRules|....
				// obTopMostParentExportItem.m_ObjectNameDefault = skybox
				// (const char*)exportInfo.m_PropertyIDs[ b ] = DoDayToNightProcessing


				retval.Combine( rexResult::WARNINGS );
				retval.AddWarning( "Property ID '%s' not registered in code!", (const char*)exportInfo.m_PropertyIDs[ b ] );
// 				retval.AddWarning( "This means that the '%s' rule file which is used for the %s_%s object (%s) contains a reference to '%s' and REX, the exporter, has no idea what this means.\\\\nTo fix the problem I advise you talk to the last person to edit %s.rule", 
// 					obTopMostParentExportItem.m_strBaseRuleFile ? (const char*)obTopMostParentExportItem.m_strBaseRuleFile : "<undefined base rule file>", 
// 					obTopMostParentExportItem.m_ModuleID ? (const char*)obTopMostParentExportItem.m_ModuleID : "<undefined module id>", 
// 					obTopMostParentExportItem.m_ObjectNameDefault ? (const char*)obTopMostParentExportItem.m_ObjectNameDefault : "<undefined object name>", 
// 					obTopMostParentExportItem.m_strMaxOrMayaIdentityString ? (const char*)obTopMostParentExportItem.m_strMaxOrMayaIdentityString : "<undefined identity string>", 
// 					exportInfo.m_PropertyIDs[ b ] ? (const char*)exportInfo.m_PropertyIDs[ b ] : "<undefined property id>", 
// 					obTopMostParentExportItem.m_strBaseRuleFile ? (const char*)obTopMostParentExportItem.m_strBaseRuleFile : "<undefined base rule file>");
			}
		}
	}

	// recursivly set up child items
	int childCount = exportInfo.m_Children.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		retval &= SetupShellAndModuleInstances( exportInfo.m_Children[ a ], obTopMostParentExportItem, moduleInstance);
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShell::CompileInputObjectLists( ExportItemInfo& exportInfoRoot )
{
	rexResult retval( rexResult::PERFECT );

	// init iterator for traversing source data
	Iterator* iterator = GetIterator( exportInfoRoot );
	Assert( iterator );

	atArray< atMap< atString, rexModule* > >		groupMaps;
	atArray< rexModule* >							groupsCurrentModule;
	atArray< atArray< rexModule* > >				currModuleStack;

	// set up module group stacks
	for( int a = 0; a < m_ModuleGroupCount; a++ )
	{
		groupsCurrentModule.PushAndGrow( NULL,(u16) m_ModuleGroupCount );
		groupMaps.Grow((u16) m_ModuleGroupCount);
		currModuleStack.Grow().PushAndGrow( NULL,(u16) m_ModuleGroupCount );		
	}

	// diverts objects to appropriate grouped modules (assures mesh won't be exported as both drawable & physics bound)
	retval &= CompileGroupInputObjectLists( exportInfoRoot, groupMaps );

	// set up progress bars
	int objectCount = iterator->GetObjectCount();
	UpdateProgressWindowMax( 2, objectCount );

	rexObject* currObject;
	int depthDelta = 0;
	//int lastDepthDelta = 0;
	int count = 0;

	// process each scene object
	while( ( currObject = iterator->GetNextObject( depthDelta ) ) != NULL )
	{
		// update progress bars
		atString hashString = GetInputObjectHashString( *currObject );
		UpdateProgressWindowValue( 2, count++ );
		UpdateProgressWindowInfo( 2, (const char*)hashString );

		bool process = shouldAddForExportProcessing(currObject);
		if(process)
		{
			// store this away for later cleanup
			m_MasterNodeList.PushAndGrow( currObject );
		}
		
		// handle module group stacks and object dispatching (no more than one module in group can handle each node)
		if( m_ModuleGroupCount > 0 )
		{
			Assert( hashString.GetLength() );

			// this stuff basically keeps a hashtable of root objects each module within a group has claimed.  when
			// one of these root objects is encountered, the groups current module is assigned to the module which
			// claimed this as a root object.  any object underneath this root object is then handled by the same module
			// unless it (or its ancestor) is otherwise claimed.

			for( int a = 0; a < m_ModuleGroupCount; a++ )
			{
				bool found = false;
				rexModule** moduleInstance = groupMaps[ a ].Access( hashString );
				
				// has this object been claimed as root object?
				if( moduleInstance )
				{
					// yes it has...set group current module to the one that claimed it
					groupsCurrentModule[ a ] = (*moduleInstance);
					found = true;
				}

				// adjust stack and current module based on depth delta
				if( !depthDelta )
				{					
					// is sibling of previous object.  pop last one and add current
					currModuleStack[ a ].Pop();

					if( !found )
						groupsCurrentModule[ a ] = currModuleStack[ a ].GetCount() ? currModuleStack[ a ][ currModuleStack[ a ].GetCount() - 1 ] : NULL;
				}
				else if( depthDelta > 0 )
				{
					// is descendant of previous object. push current onto stack for each level of hierarchy
					for( int b = 0; b < depthDelta - 1; b++ )
					{
						currModuleStack[ a ].PushAndGrow( groupsCurrentModule[ a ] );						
					}					
				}
				else if( depthDelta < 0 )
				{
					// is ancestor of previous object.  pop stack for each level of hierarchy, then push current
					for( int b = 0; b < abs( depthDelta - 1 ); b++ )
					{
						currModuleStack[ a ].Pop();
					}					

					if( !found )
						groupsCurrentModule[ a ] = currModuleStack[ a ].GetCount() ? currModuleStack[ a ][ currModuleStack[ a ].GetCount() - 1 ] : NULL;
				}			
				
				currModuleStack[ a ].PushAndGrow( groupsCurrentModule[ a ] );
			}
		}
		
		if(process)
		{
			// send object to module to be processed
			retval &= DispatchInputObject( exportInfoRoot, currObject, groupsCurrentModule );
		}
		else
		{
			//Displayf("Skipping Node {%s} should not be added for export processing. Deleteing created rexObject.", hashString.c_str());
			//not processing so delete this rex node
			delete currObject;
			currObject = NULL;
		}
	}
	
	// reclaim iterator memory
	delete iterator;

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////


rexResult rexShell::CompileGroupInputObjectLists( ExportItemInfo& exportInfo, atArray< atMap< atString, rexModule* > >& groupMaps )
{
	rexResult retval( rexResult::PERFECT );
	
	if( !exportInfo.m_ModuleInstance )
		return retval;
	
	// make sure we have a valid group ID
	Assert( exportInfo.m_ModuleInstance->m_GroupID >= 0 && exportInfo.m_ModuleInstance->m_GroupID < m_ModuleGroupCount );

	atMap< atString, rexModule* >& map = groupMaps[ exportInfo.m_ModuleInstance->m_GroupID ];
	rexModule& moduleInstance = *exportInfo.m_ModuleInstance;

	// make sure that there's the allowed number of objects for this module:
	int inputObjectCount = moduleInstance.m_InputObjects.GetCount();
	if (!moduleInstance.IsNumInputObjectsAllowed(inputObjectCount))
	{
		atString string;
		retval.AddError("Module '%s' is only allowed to have %d input object(s), yet it has %d input object(s).",
			(const char*)exportInfo.m_ModuleID,moduleInstance.GetNumInputObjectsAllowed(),inputObjectCount);
		retval.Set(rexResult::ERRORS);
		return retval;
	}

	// go through input objects and register claims into group hash table

	for( int a = 0; a < inputObjectCount; a++ )
	{

		// get the object ID string to use in hash
		atString hashString = GetInputObjectHashString( *moduleInstance.m_InputObjects[ a ] );
		Assert( hashString.GetLength() );
		
		// make sure object isn't already claimed by another module in this group
		if( map.Access( hashString ) )
		{			
			if( m_ModuleGroupRespectsClaims[ moduleInstance.m_GroupID ] )
			{
				retval.Combine( rexResult::WARNINGS );
				retval.AddMessage( "Module '%s' (group %d) has attempted to claim node which has already been claimed (%s)", (const char*)exportInfo.m_ModuleID, moduleInstance.m_GroupID, (const char*)hashString );
			}
		}
		else
		{
			// add it to group's claimed nodes hash table
			map.Insert( hashString, &moduleInstance );
		}			
	}		

	// call this function recursively
	int childCount = exportInfo.m_Children.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		retval &= CompileGroupInputObjectLists( exportInfo.m_Children[ a ], groupMaps );
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShell::DispatchInputObject( ExportItemInfo& exportInfo, rexObject* object, atArray< rexModule* >& groupsCurrentModule )
{
	rexResult retval( rexResult::PERFECT );
		
	if( !exportInfo.m_ModuleInstance )
		return retval;

	// make sure we have a valid group ID
	Assert( exportInfo.m_ModuleInstance->m_GroupID >= 0 && exportInfo.m_ModuleInstance->m_GroupID < m_ModuleGroupCount );	

	bool sendToFilter = false;

	// if this module respects claims...
	if( m_ModuleGroupRespectsClaims[ exportInfo.m_ModuleInstance->m_GroupID ] )
	{
		// ...then pass this along to the filter for consideration if this module is the
		sendToFilter = ( groupsCurrentModule[ exportInfo.m_ModuleInstance->m_GroupID ] == exportInfo.m_ModuleInstance 
						&& IsNotFilteredModuleGroup(groupsCurrentModule[ exportInfo.m_ModuleInstance->m_GroupID ], groupsCurrentModule));
	}
	else
	{
		// otherwise, pass it along if the object is part an input object or a descendant of 
		// an input object.
		sendToFilter = IsObjectPartOfInput( *object, exportInfo.m_ModuleInstance->m_InputObjects, true );
	}
		
	if( sendToFilter )
	{
		// if there is no filter, only pass along objects (to the converter) that are actual input objects
		if( !exportInfo.m_ModuleInstance->m_Filter && IsObjectPartOfInput( *object, exportInfo.m_ModuleInstance->m_InputObjects, false ) )
		{
			//atString hashString = GetInputObjectHashString( *object );
			//Displayf("1. Add [%s] to module {%s}", hashString.c_str(), exportInfo.m_ModuleID.c_str());
			exportInfo.m_FilteredObjectList.PushAndGrow( object );
		}

		// if there is a filter, pass along objects (to the converter) that the filter accepts
		if( exportInfo.m_ModuleInstance->m_Filter && exportInfo.m_ModuleInstance->m_Filter->AcceptObject( *object ) )
		{
			//atString hashString = GetInputObjectHashString( *object );
			//Displayf("2. Add [%s] to module {%s}", hashString.c_str(), exportInfo.m_ModuleID.c_str());
			exportInfo.m_FilteredObjectList.PushAndGrow( object );
		}
	}

	int childCount = exportInfo.m_Children.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		retval &= ApplyObjectModifications( exportInfo.m_Children[ a ] );
	}

	// apply any filtered object modifications
	int propertyCount = exportInfo.m_PropertyIDs.GetCount();
	Assert( ( propertyCount == exportInfo.m_PropertyValues.GetCount() ) && ( propertyCount == exportInfo.m_PropertyConnectedObjects.GetCount() ) );

	// go through properties and apply them to new converted objects
	for( int b = 0; b < propertyCount; b++ )
	{
		// find property based on export item's property IDs
		const rexProperty* const* property = m_PropertyMap.Access( exportInfo.m_PropertyIDs[ b ] );

		if( property && (*property) )
		{
			// apply property
			retval &= (*property)->ModifyFilteredObject( *object, exportInfo.m_PropertyValues[ b ], exportInfo.m_PropertyConnectedObjects[ b ] );
		}	
	}

	for( int a = 0; a < childCount; a++ )
	{
		retval &= DispatchInputObject( exportInfo.m_Children[ a ], object, groupsCurrentModule );
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexShell::IsNotFilteredModuleGroup(rexModule* current, atArray< rexModule* >& groupsCurrentModule)
{
	bool retval = true;
	ModuleFilter* modFilter = m_ModuleFilters.Access(current->m_GroupID);
	if (modFilter)
	{
		for(int mod = 0; mod < groupsCurrentModule.GetCount(); mod++)
		{
			if (groupsCurrentModule[mod])
			{
				if (modFilter->IsFiltered(groupsCurrentModule[mod]->m_GroupID))
				{
					retval = false;
					break;
				}
			}
		}
	}
	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexShell::AddToModuleFilter( const char *filter, const char *filteredModule )
{
	rexModule** found = m_ModuleMap.Access(atString(filter));
	Assert(found);

	rexModule* module = (*found);
	ModuleFilter* modfilter = m_ModuleFilters.Access(module->m_GroupID);
	if (!modfilter)
	{
		m_ModuleFilters.Insert(module->m_GroupID, ModuleFilter());
		modfilter = m_ModuleFilters.Access(module->m_GroupID);
	}
	Assert(modfilter);

	found = m_ModuleMap.Access(filteredModule);
	Assert(found);

	rexModule* module2 = (*found);

	if (!modfilter->IsFiltered(module2->m_GroupID))
	{
		modfilter->Add(module2->m_GroupID);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShell::ConvertObjects( ExportItemInfo& exportInfo, rexObject* parentObject )
{
	if( !exportInfo.m_ModuleInstance )
		return rexResult( rexResult::WARNINGS );

	rexResult retval( rexResult::PERFECT );
	
	if( exportInfo.m_ModuleActive && exportInfo.m_ModuleInstance->m_Converter )
	{
		// setup converter progress bar functions.  i'd like to remove this and have a general callback that does this
		// from here, so the converter doesn't have to know about progress bar
		exportInfo.m_ModuleInstance->m_Converter->RegisterProgressBarObjectCountCallback( GetProgressBarItemCountCB() );
		exportInfo.m_ModuleInstance->m_Converter->RegisterProgressBarObjectCurrentIndexCallback( GetProgressBarCurrValueCB() );
		exportInfo.m_ModuleInstance->m_Converter->RegisterProgressBarTextCallback( GetProgressBarInfoCB() );

		// pass along filtered objects to converter
		retval = exportInfo.m_ModuleInstance->m_Converter->Convert( exportInfo.m_FilteredObjectList, exportInfo.m_ConvertedObject, exportInfo.m_ObjectNameDefault );
		
		if( !retval )
			return retval;

		// make sure i got an object back
		Assert( exportInfo.m_ConvertedObject );

		// if there's a parent, set this as its child
		if( parentObject )
			parentObject->m_ContainedObjects.PushAndGrow( exportInfo.m_ConvertedObject );

	}

	// convert descendants objects
	int childCount = exportInfo.m_Children.GetCount();
	for( int c = 0; c < childCount; c++ )
	{
		retval &= ConvertObjects( exportInfo.m_Children[ c ], exportInfo.m_ConvertedObject );
		if( !retval )
			break;
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShell::ProcessObjects( ExportItemInfo& exportInfo )
{
	if( !exportInfo.m_ModuleInstance )
		return rexResult( rexResult::WARNINGS );

	rexResult retval( rexResult::PERFECT );

	if( exportInfo.m_ModuleActive )
	{
		int processorCount = exportInfo.m_ModuleInstance->m_Processors.GetCount();
		for( int a = 0; a < processorCount; a++ )
		{
			rexProcessor* processor = exportInfo.m_ModuleInstance->m_Processors[ a ];
			Assert( processor );

			// setup Processor progress bar functions.  i'd like to remove this and have a general callback that does this
			// from here, so the Processor doesn't have to know about progress bar
			processor->RegisterProgressBarObjectCountCallback( GetProgressBarItemCountCB() );
			processor->RegisterProgressBarObjectCurrentIndexCallback( GetProgressBarCurrValueCB() );
			processor->RegisterProgressBarTextCallback( GetProgressBarInfoCB() );

			if( exportInfo.m_ConvertedObject )
				retval = processor->Process( exportInfo.m_ConvertedObject );

			if( !retval )
				return retval;
		}
	}
	
	// process descendents' objects
	int childCount = exportInfo.m_Children.GetCount();
	for( int c = 0; c < childCount; c++ )
	{
		retval &= ProcessObjects( exportInfo.m_Children[ c ] );
		if( !retval )
			break;
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShell::ApplyObjectModifications( ExportItemInfo& exportInfo )
{
	rexResult retval( rexResult::PERFECT );

	if( !exportInfo.m_ModuleInstance )
		return retval;

	// apply child objects' properties first so that, if child objects are modified, the parent can be modified
	// based on its children's modifications
	int childCount = exportInfo.m_Children.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		retval &= ApplyObjectModifications( exportInfo.m_Children[ a ] );
	}
	
	if( exportInfo.m_ModuleActive )
	{
		int propertyCount = exportInfo.m_PropertyIDs.GetCount();
		Assert( ( propertyCount == exportInfo.m_PropertyValues.GetCount() ) && ( propertyCount == exportInfo.m_PropertyConnectedObjects.GetCount() ) );

		// go through properties and apply them to new converted objects
		for( int b = 0; b < propertyCount; b++ )
		{
			// find property based on export item's property IDs
			const rexProperty* const* property = m_PropertyMap.Access( exportInfo.m_PropertyIDs[ b ] );
					
			if( property && (*property) )
			{
				// apply property
				if(exportInfo.m_ConvertedObject)
					retval &= (*property)->ModifyObject( *exportInfo.m_ConvertedObject, exportInfo.m_PropertyValues[ b ], exportInfo.m_PropertyConnectedObjects[ b ] );
			}	
		}
	}
	
	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShell::ApplyProcessedObjectModifications( ExportItemInfo& exportInfo )
{
	rexResult retval( rexResult::PERFECT );

	if( !exportInfo.m_ModuleInstance )
		return retval;

	// apply child objects' properties first so that, if child objects are modified, the parent can be modified
	// based on its children's modifications
	int childCount = exportInfo.m_Children.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		retval &= ApplyProcessedObjectModifications( exportInfo.m_Children[ a ] );
	}

	if( exportInfo.m_ModuleActive )
	{
		int propertyCount = exportInfo.m_PropertyIDs.GetCount();
		Assert( ( propertyCount == exportInfo.m_PropertyValues.GetCount() ) && ( propertyCount == exportInfo.m_PropertyConnectedObjects.GetCount() ) );

		// go through properties and apply them to new converted objects
		for( int b = 0; b < propertyCount; b++ )
		{
			// find property based on export item's property IDs
			const rexProperty* const* property = m_PropertyMap.Access( exportInfo.m_PropertyIDs[ b ] );

			if( property && (*property) )
			{
				// apply property
				retval &= (*property)->ModifyProcessedObject( *exportInfo.m_ConvertedObject, exportInfo.m_PropertyValues[ b ], exportInfo.m_PropertyConnectedObjects[ b ] );
			}	
		}
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShell::VerifyObjects( ExportItemInfo& exportInfo )
{
	rexResult retval( rexResult::PERFECT );

	if( !exportInfo.m_ModuleInstance )
		return retval;

	// apply child objects' properties first so that, if child objects are modified, the parent can be modified
	// based on its children's modifications
	int childCount = exportInfo.m_Children.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		retval &= VerifyObjects( exportInfo.m_Children[ a ] );
	}

	if( exportInfo.m_ModuleActive )
	{
		int propertyCount = exportInfo.m_PropertyIDs.GetCount();
		Assert( ( propertyCount == exportInfo.m_PropertyValues.GetCount() ) && ( propertyCount == exportInfo.m_PropertyConnectedObjects.GetCount() ) );

		// go through properties and apply them to new converted objects
		for( int b = 0; b < propertyCount; b++ )
		{
			// find property based on export item's property IDs
			const rexProperty* const* property = m_PropertyMap.Access( exportInfo.m_PropertyIDs[ b ] );

			if( property && (*property) )
			{
				// apply property
				retval &= (*property)->VerifyObject( *exportInfo.m_ConvertedObject, exportInfo.m_PropertyValues[ b ], exportInfo.m_PropertyConnectedObjects[ b ] );
			}	
		}
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShell::SerializeObjects( ExportItemInfo& exportInfo )
{
	rexResult retval( rexResult::PERFECT );

	if( !exportInfo.m_ModuleInstance )
		return retval;

	// serialize child objects first, so that changes to objects the serializer makes can be taken into account
	// by parent object.  (serializers don't generally modify objects, but currently the mesh serializer can
	// set a mesh object's IsValid flag to false if the object is too big after tristripping)
	int childCount = exportInfo.m_Children.GetCount();
	for( int c = 0; c < childCount; c++ )
	{
		retval &= SerializeObjects( exportInfo.m_Children[ c ] );
		if( !retval )
			return retval;
	}

	if( exportInfo.m_ModuleActive && exportInfo.m_ModuleInstance->m_Serializer )
	{
		// setup serializer progress bar functions.  i'd like to remove this and have a general callback that does this
		// from here, so the serializer doesn't have to know about progress bar
		exportInfo.m_ModuleInstance->m_Serializer->RegisterProgressBarObjectCountCallback( GetProgressBarItemCountCB() );
		exportInfo.m_ModuleInstance->m_Serializer->RegisterProgressBarObjectCurrentIndexCallback( GetProgressBarCurrValueCB() );
		exportInfo.m_ModuleInstance->m_Serializer->RegisterProgressBarTextCallback( GetProgressBarInfoCB() );

		// serialize it
		retval &= exportInfo.m_ModuleInstance->m_Serializer->Serialize( *exportInfo.m_ConvertedObject );
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexShell::ExportItemInfo::ExportItemInfo() 
{
	m_ModuleInstance = NULL;
	m_ConvertedObject = NULL;
	m_ModuleActive = true;
}

rexShell::ExportItemInfo::~ExportItemInfo()
{
	CleanUpPostFiltration();
	CleanUpPostConversion();
	CleanUpPostSerialization();

	if( m_ModuleInstance )
		delete m_ModuleInstance;
}

bool rexShell::ExportItemInfo::CleanUpPostFiltration()
{
	if( !m_ModuleInstance )
		return false;

	// delete filter instance and module input oblects
	
	if( m_ModuleInstance->m_Filter )
	{
		delete m_ModuleInstance->m_Filter;
		m_ModuleInstance->m_Filter = NULL;
	}

	while( m_ModuleInstance->m_InputObjects.GetCount() )
		delete m_ModuleInstance->m_InputObjects.Pop();

	return true;
}

bool rexShell::ExportItemInfo::CleanUpPostConversion()
{
	if( !m_ModuleInstance )
		return false;

	// delete converter instance and filtered object list
	if( m_ModuleInstance->m_Converter )
	{
		delete m_ModuleInstance->m_Converter;
		m_ModuleInstance->m_Converter = NULL;
	}

	return true;
}

bool rexShell::ExportItemInfo::CleanUpPostSerialization()
{
	if( !m_ModuleInstance )
		return false;

	// delete serializer instance and converted objects
	if( m_ModuleInstance->m_Serializer )
	{
		delete m_ModuleInstance->m_Serializer;
		m_ModuleInstance->m_Serializer = NULL;
	}

	int propertyConnectedObjectsArrayCount = m_PropertyConnectedObjects.GetCount();

	for( int a = 0; a < propertyConnectedObjectsArrayCount; a++ )
	{
		atArray<rexObject*>& array = m_PropertyConnectedObjects[ a ];
		while( array.GetCount() ) 
		{
			rexObject* obj = array.Pop();
			delete obj;
		}
	}
	
	return true;
}

//////////////////////////////////////////////////////////////////////////

void rexShell::ModuleFilter::Add(int GroupID)
{
	m_GroupIDs.PushAndGrow(GroupID);
}

bool rexShell::ModuleFilter::IsFiltered(int GroupID)
{
	bool retval = false;
	for( int a = 0; a < m_GroupIDs.GetCount(); a++ )
	{
		if (m_GroupIDs[a] == GroupID)
		{
			retval = true;
			break;
		}
	}
	return retval;
}

} // namespace rage
