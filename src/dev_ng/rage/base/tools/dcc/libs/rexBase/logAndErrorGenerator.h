// 
// rexBase/logAndErrorGenerator.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX datBase Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexLogAndErrorGenerator -- abstract class with common functionality for sub-exporters and other objects that generate
//								   error and/or log information
//
////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//					bool BeginLog( fiStream* s );  
//					bool EndLog();
//						* returns false if an error occurred.  check RetrieveErrorMessages() for details.
//
//					const atArray<atString&> RetrieveErrorMessages() const;
//		
//		PROTECTED INTERFACE:
//				[Primary Functionality]
//					bool WriteToLog( const atString& message );
//						* returns false if an error occurred.  check RetrieveErrorMessages() for details.
//
//					void ResetErrorMessages();
//					bool ReportError( const atString& errorMessage );
//						* returns false if an error occurred.  check RetrieveErrorMessages() for details.
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_LOGANDERRORGENERATOR_H__
#define __REX_BASE_LOGANDERRORGENERATOR_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "atl/array.h"
#include "atl/string.h"

namespace rage {

class fiStream;

typedef void (*rexProgressBarCallback)( int value );
typedef void (*rexProgressBarTextCallback)( const char* t );

/////////////////////////////////////////////////////////////////////////////////////

class rexLogAndErrorGenerator
{
public:
	rexLogAndErrorGenerator();
	virtual ~rexLogAndErrorGenerator() {};

	bool BeginLog( fiStream* s );
	bool EndLog();

	const atArray<atString>& RetrieveErrorMessages() const;

protected:
	bool WriteToLog( const atString& message ) const;
	bool ReportError( const atString& errorMessage ) const;	
	void ClearErrorMessages() const;

	mutable atArray<atString>	m_ErrorMessages;
	fiStream*						m_LogStream;
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
