#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "rexBase/result.h"
#include "rexBase/shell.h"
#include "math/amath.h"

#define MAX_MESSAGE_LENGTH 4096

/////////////////////////////////////////////////////

namespace rage {

rexShell *rexResult::sp_Shell = NULL;

void myInvalidParameterHandler(const wchar_t* expression,
	const wchar_t* function, 
	const wchar_t* file, 
	unsigned int line, 
	uintptr_t /*pReserved*/)
{
	printf("Invalid parameter detected in function %s."
		" File: %s Line: %d\n", function, file, line);
	printf("Expression: %s\n", expression);
};

void rexResult::Combine( const rexResult& res1, const rexResult& res2 )
{
	unsigned int r0 = GetResultType(), r1 = res1.GetResultType(), r2 = res2.GetResultType();

	if(r1 || r2)
	{ // only do combination if at least one of the parameter results is initialized
		// can do Max because of the way the result type bits are laid out
		Set((eResultType)Max(r0, r1, r2));

		// NOTE: currently, it is possible to get info flags from an
		// uninitialized rexResult if the other parameter has been initialized
		m_Result |= (( res1.m_Result | res2.m_Result ) & INFO_FLAG_MASK );  // mask out result type

		// combine the messages from both parameters
		int a, messageCount = res1.GetMessageCount();
		for( a = 0; a < messageCount; a++ )
		{
			m_Messages.Grow((u16) messageCount) = res1.m_Messages[ a ];
		}		
		messageCount = res2.GetMessageCount();
		for( a = 0; a < messageCount; a++ )
		{
			m_Messages.Grow((u16) messageCount) = res2.m_Messages[ a ];
		}		
	}
}

void rexResult::Combine( const rexResult& res2 )
{
	unsigned int r0 = GetResultType(), r2 = res2.GetResultType();

	if(r2)
	{ // only do combination if the parameter was initialized
		// can do Max because of the way the result type bits are laid out
		Set((eResultType)Max(r0, r2));

		m_Result |= (res2.m_Result & INFO_FLAG_MASK);  // mask out result type

		// combine the messages
		int a, messageCount = res2.GetMessageCount();
		for( a = 0; a < messageCount; a++ )
		{
			m_Messages.Grow((u16) messageCount) = res2.m_Messages[ a ];
		}
	}
}


void rexResult::AddMessage(const char* fmt,...) 
{
	sRexResultMsg& dest = m_Messages.Grow();

	char buffer[MAX_MESSAGE_LENGTH];
	memset(buffer,0,MAX_MESSAGE_LENGTH);
	va_list args;
	va_start(args,fmt);
	AssertVerify( vsprintf_s(buffer,sizeof(buffer),fmt,args) < MAX_MESSAGE_LENGTH );	
	va_end(args);

	dest.mMsgType = MSGTYPE_MESSAGE;
	dest.mMsg = buffer;
	dest.mContext = atString("");

	if(sp_Shell)
		sp_Shell->ExternalLogMessage(buffer);
}
void rexResult::AddMessageCtx(const char *context, const char* fmt,...) 
{
	sRexResultMsg& dest = m_Messages.Grow();

	char buffer[MAX_MESSAGE_LENGTH];
	memset(buffer,0,MAX_MESSAGE_LENGTH);
	va_list args;
	va_start(args,fmt);
	AssertVerify( vsprintf_s(buffer,sizeof(buffer),fmt,args) < MAX_MESSAGE_LENGTH );	
	va_end(args);

	dest.mMsgType = MSGTYPE_MESSAGE;
	dest.mMsg = buffer;
	dest.mContext = atString("");

	if(sp_Shell)
		sp_Shell->ExternalLogMessage(buffer, context);
}
void rexResult::AddMessageFileCtx(const char *context, const char *filecontext, const char* fmt,...) 
{
	sRexResultMsg& dest = m_Messages.Grow();

	char buffer[MAX_MESSAGE_LENGTH];
	memset(buffer,0,MAX_MESSAGE_LENGTH);
	va_list args;
	va_start(args,fmt);
	AssertVerify( vsprintf_s(buffer,sizeof(buffer),fmt,args) < MAX_MESSAGE_LENGTH );	
	va_end(args);

	dest.mMsgType = MSGTYPE_MESSAGE;
	dest.mMsg = buffer;
	dest.mContext = atString("");

	if(sp_Shell)
		sp_Shell->ExternalLogMessage(buffer, context, filecontext);
}
void rexResult::AddWarning(const char* fmt,...) 
{
	sRexResultMsg& dest = m_Messages.Grow();

	char buffer[MAX_MESSAGE_LENGTH];
	memset(buffer,0,MAX_MESSAGE_LENGTH);

	va_list args;
	va_start(args,fmt);
	AssertVerify( vsprintf_s(buffer,sizeof(buffer),fmt,args) < MAX_MESSAGE_LENGTH );	
	va_end(args);

	dest.mMsgType = MSGTYPE_WARNING;
	dest.mMsg = buffer;
	dest.mContext = atString("");

	if(sp_Shell)
		sp_Shell->ExternalLogWarning(buffer);
}
void rexResult::AddWarningCtx(const char *context, const char* fmt,...) 
{
	sRexResultMsg& dest = m_Messages.Grow();

	char buffer[MAX_MESSAGE_LENGTH];
	memset(buffer,0,MAX_MESSAGE_LENGTH);

	va_list args;
	va_start(args,fmt);
	AssertVerify( vsprintf_s(buffer,sizeof(buffer),fmt,args) < MAX_MESSAGE_LENGTH );	
	va_end(args);

	dest.mMsgType = MSGTYPE_WARNING;
	dest.mMsg = buffer;
	dest.mContext = atString("");

	if(sp_Shell)
		sp_Shell->ExternalLogWarning(buffer, context);
}
void rexResult::AddWarningFileCtx(const char *context, const char *filecontext, const char* fmt,...) 
{
	sRexResultMsg& dest = m_Messages.Grow();

	char buffer[MAX_MESSAGE_LENGTH];
	memset(buffer,0,MAX_MESSAGE_LENGTH);

	va_list args;
	va_start(args,fmt);
	AssertVerify( vsprintf_s(buffer,sizeof(buffer),fmt,args) < MAX_MESSAGE_LENGTH );	
	va_end(args);

	dest.mMsgType = MSGTYPE_WARNING;
	dest.mMsg = buffer;
	dest.mContext = atString("");

	if(sp_Shell)
		sp_Shell->ExternalLogWarning(buffer, context, filecontext);
}
void rexResult::AddError(const char* fmt,...) 
{
	sRexResultMsg& dest = m_Messages.Grow();

	char buffer[MAX_MESSAGE_LENGTH];
	memset(buffer,0,MAX_MESSAGE_LENGTH);

	va_list args;
	va_start(args,fmt);
	AssertVerify( vsprintf_s(buffer,sizeof(buffer),fmt,args) < MAX_MESSAGE_LENGTH );	
	va_end(args);

	dest.mMsgType = MSGTYPE_ERROR;
	dest.mMsg = buffer;
	dest.mContext = atString("");

	if(sp_Shell)
		sp_Shell->ExternalLogError(buffer);
}
void rexResult::AddErrorCtx(const char *context, const char* fmt,...) 
{
	sRexResultMsg& dest = m_Messages.Grow();

	char buffer[MAX_MESSAGE_LENGTH];
	memset(buffer,0,MAX_MESSAGE_LENGTH);

	va_list args;
	va_start(args,fmt);
	AssertVerify( vsprintf_s(buffer,sizeof(buffer),fmt,args) < MAX_MESSAGE_LENGTH );	
	va_end(args);

	dest.mMsgType = MSGTYPE_ERROR;
	dest.mMsg = buffer;
	dest.mContext = atString("");

	if(sp_Shell)
		sp_Shell->ExternalLogError(buffer, context);
}
void rexResult::AddErrorFileCtx(const char *context, const char *filecontext, const char* fmt,...) 
{
	sRexResultMsg& dest = m_Messages.Grow();

	char buffer[MAX_MESSAGE_LENGTH];
	memset(buffer,0,MAX_MESSAGE_LENGTH);

	va_list args;
	va_start(args,fmt);
	AssertVerify( vsprintf_s(buffer,sizeof(buffer),fmt,args) < MAX_MESSAGE_LENGTH );	
	va_end(args);

	dest.mMsgType = MSGTYPE_ERROR;
	dest.mMsg = buffer;
	dest.mContext = atString("");

	if(sp_Shell)
		sp_Shell->ExternalLogError(buffer, context, filecontext);
}
void rexResult::AddMessage( const atString &m, const char *context)
{ 
	if(sp_Shell)
		sp_Shell->ExternalLogMessage(m, context);
	sRexResultMsg msg = {MSGTYPE_MESSAGE, m, atString(context)};
	m_Messages.Grow() = msg;
}
void rexResult::AddWarning( const atString &m, const char *context)
{ 
	if(sp_Shell)
		sp_Shell->ExternalLogWarning(m, context);
	sRexResultMsg msg = {MSGTYPE_WARNING, m, atString(context)};
	m_Messages.Grow() = msg;
}
void rexResult::AddError( const atString &m, const char *context)
{ 
	if(sp_Shell)
		sp_Shell->ExternalLogError(m, context);
	sRexResultMsg msg = {MSGTYPE_ERROR, m, atString(context)};
	m_Messages.Grow() = msg;
}


void rexResult::Display() const
{
	int messageCount = m_Messages.GetCount();
	if( !messageCount)
		return;

	if( Errors() )
	{
		Displayf( "ERRORs occured during export" );
	}
	else if( Warnings() )
	{
		Displayf( "WARNINGs occured during export" );
	}
	else 
	{
		Displayf( "EXTRA INFO:" );
	}
	atString prefixs[] = 
	{
		atString("Debug"), 
		atString("Message"), 
		atString("Warning"), 
		atString("Error")
	};
	for( int m = 0; m < messageCount; m++ )
	{
		//If there are any errors, write out all of them using the "ERROR:" prefix,
		Displayf( "%s:\t%s\n", prefixs[m_Messages[ m ].mMsgType].c_str(), m_Messages[ m ].mMsg.c_str() );
	}
}
const atString rexResult::GetMessageByIndex( int a ) const	
{ 
	char buffer[1024];
	atString prefixs[] = 
	{
		atString("Debug"), 
		atString("Message"), 
		atString("Warning"), 
		atString("Error")
	};
	sprintf_s(buffer, 1024, "%s:\t%s\n", prefixs[m_Messages[ a ].mMsgType].c_str(), m_Messages[ a ].mMsg.c_str());
	if(m_Messages[ a ].mContext!="")
	{
		strcat_s(buffer, " - Context:");
		strcat_s(buffer, m_Messages[ a ].mContext);
	}
	return atString(buffer); 
}
} // namespace rage
