// 
// rexBase/module.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX datBase Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
////////////////////////////////////////////////////////////////////////////////////////
//
//		rexModule -- class for packaging a gatherer, converter, and serializer together
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_BASE_MODULE_H__
#define __REX_BASE_MODULE_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "atl/array.h"
#include "rexBase/filter.h"
#include "rexBase/converter.h"
#include "rexBase/processor.h"
#include "rexBase/serializer.h"

namespace rage {


/////////////////////////////////////////////////////////////////////////////////////

class rexModule
{
public:
	enum {INFINITE_OBJECTS_ALLOWED=-1};
	rexModule( rexFilter* filter, rexConverter* converter, atArray<rexProcessor*>& processors, rexSerializer* serializer, int groupID, int numInstancesAllowed )  
	{ 
		m_Filter = filter; 
		m_Converter = converter; 
		m_Serializer = serializer; 
		m_Processors = processors; 
		m_GroupID = groupID;  
		m_NumInstancesAllowed = numInstancesAllowed;
	}

	~rexModule()
		{ delete m_Filter; delete m_Converter; delete m_Serializer; while( m_Processors.GetCount() ) delete m_Processors.Pop();  }
	
	rexModule* Clone() const  
	{ 
		rexModule* m = new rexModule; 
		m->m_Filter  = m_Filter ? m_Filter->CreateNew() : NULL; 
		m->m_Converter  = m_Converter ? m_Converter->CreateNew() : NULL; 
		int processorCount = m_Processors.GetCount();
		for( int a = 0; a < processorCount; a++ )
			m->m_Processors.PushAndGrow( m_Processors[ a ]->CreateNew(), (u16) processorCount );
		m->m_Serializer = m_Serializer ? m_Serializer->CreateNew() : NULL; 
		m->m_GroupID = m_GroupID;
		m->m_NumInstancesAllowed = m_NumInstancesAllowed;
		Assert( m );
		return m;
	}

	int GetNumInputObjectsAllowed() const
	{
		return m_NumInstancesAllowed;
	}

	bool IsNumInputObjectsAllowed(int numObjects)
	{
		return m_NumInstancesAllowed<0 || numObjects<=m_NumInstancesAllowed;
	}

	rexFilter*				m_Filter;
	rexConverter*			m_Converter;
	atArray<rexProcessor*>	m_Processors;
	rexSerializer*			m_Serializer;
	
	atArray<rexObject*>	m_InputObjects;  // used only by instances
	int					m_NumInstancesAllowed;

	int					m_GroupID;

private:
	rexModule()  
	{ 
		m_Filter = 0; 
		m_Converter = 0; 
		m_Serializer = 0; 
		m_GroupID = -1; 
		m_NumInstancesAllowed = INFINITE_OBJECTS_ALLOWED;
	}
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
