#ifndef __REX_HAMSTER_OBJECT_H__
#define __REX_HAMSTER_OBJECT_H__

//#include "rexMaya/mayahelp.h"
#include "rexBase/shell.h"

namespace rage
{

class rexHamsterObject
{
public:

    rexHamsterObject() {};
    virtual ~rexHamsterObject() {};

    virtual void SetModuleID(atString& moduleID) = 0;
    virtual void SetEntityName(atString name) = 0;
    virtual void SetOutputPath(atString& path) = 0;
    virtual void SetExportAsSingleEntity(bool asSingle) = 0;
    virtual void AddMDagPath(atString& dagPath, const char* exportName) = 0;

    virtual const atString GetMDagPathExportName(const char *dagPath) const = 0;
    virtual const atString GetMDagPathExportName(const rexObject* object) const = 0;
    virtual const atString GetOutputPath() const = 0;
    virtual const atString GetEntityName() const = 0;

    //[CLEMENSP]
    virtual 
    const char* getMainModuleID() const  = 0;

    virtual void SetupInputObjects(atArray<rexObject*>& objects) const = 0;

    virtual void SetupExportItemInfos(atArray<rexShell::ExportItemInfo>& infos, const atArray<rexObject*>& objects) const = 0;

protected:

    virtual void SetupExportItemInfoForSingleEntity(atArray<rexShell::ExportItemInfo>& infos, const atArray<rexObject*>& objects) const = 0;
    virtual void SetupExportItemInfoForEntities(atArray<rexShell::ExportItemInfo>& infos, const atArray<rexObject*>& objects) const  = 0;

};

}

#endif //__REX_HAMSTER_OBJECT_H__
