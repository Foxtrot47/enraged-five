#include "rexBase/serializer.h"
#include "rexBase/shell.h"

namespace rage {

rexSerializer::EnumUnitTypeLinear rexSerializer::sm_UnitTypeLinearTo=rexSerializer::NONE;
rexSerializer::EnumUnitTypeLinear rexSerializer::sm_BaseUnits=rexSerializer::NONE;

extern const char* s_UnitTypeName[rexSerializer::NUM_UNIT_TYPE_LINEARS]=
{
	"MILLIMETERS",
	"CENTIMETERS",
	"METERS",
	"INCHES",
	"FEET",
	"YARDS",
	"EVIL_MAGIC_PONG_UNITS"
};

void rexSerializer::AddBoundFilterType(const char * str)
{
	//Only add unique types
	if (m_BoundFilterTypes.Find(atString(str)) == -1)
	{
		//add to the array
		m_BoundFilterTypes.PushAndGrow(atString(str));
	}
}

const atString& rexSerializer::GetOutputPath() const
{ 
	if(rexShell::DoesntSerialise())
		return rexShell::GetTempExportPath();
	else
		return m_OutputPath; 
}

void rexSerializer::SetUnitTypeToConvertTo(const char *str)
{
	sm_UnitTypeLinearTo=NONE;
	for (int i=0;i<NUM_UNIT_TYPE_LINEARS;i++)
	{
		if (str==s_UnitTypeName[i])
		{
			sm_UnitTypeLinearTo=(EnumUnitTypeLinear)i;
			break;
		}
	}
}

atString rexSerializer::GetUnitTypeToConvertTo()
{
	if (sm_UnitTypeLinearTo==NONE)
	{
		return atString("NONE");
	}

	return atString(s_UnitTypeName[sm_UnitTypeLinearTo]);
}

float rexSerializer::GetUnitTypeLinearConversionFactor()
{
	static float conversionMatrix[NUM_UNIT_TYPE_LINEARS][NUM_UNIT_TYPE_LINEARS] = 
	{	// MILLIMETERS	CENTIMETERS		METERS			INCHES			FEET			YARDS			EVIL_MAGIC_PONG_UNITS
		{  1.0f,		0.1f,			0.001f,			0.0393700787f,	0.0032808399f,	0.0010936133f,	0.0f},			//MILLIMETERS to ?
		{  10.0f,		1.0f,			0.01f,			0.393700787f,	0.032808399f,	0.010936133f,	1.08064516f},	//CENTIMETERS to ?
		{  1000.0f,		100.0f,			1.0f,			39.3700787f,	3.2808399f,		1.0936133f,		0.0f},			//METERS to ?
		{  25.4f,		2.54f,			0.0254f,		1.0f,			0.083333333f,	0.027777778f,	0.0f},			//INCHES to ?
		{  304.8f,		30.48f,			0.348f,			12.0f,			1.0f,			0.333333333f,	0.0f},			//FEET to ?
		{  914.4f,		91.44f,			0.9144f,		36.0f,			3.0f,			1.0f,			0.0f},			//YARDS to ?
		{  0.0f,		0.0f,			0.0f,			0.0f,			0.0f,			0.0f,			1.0f}			//EVIL_MAGIC_PONG_UNITS to ?
	};

	// Displayf("rexSerializer::GetUnitTypeLinearConversionFactor() sm_UnitTypeLinearTo = %d sm_BaseUnits = %d", sm_UnitTypeLinearTo, sm_BaseUnits);
	if (sm_UnitTypeLinearTo==NONE || sm_BaseUnits==NONE)
	{
		return 1.0f;
	}
	else 
	{
		return conversionMatrix[sm_BaseUnits][sm_UnitTypeLinearTo];
	}
}

} // namespace rage
