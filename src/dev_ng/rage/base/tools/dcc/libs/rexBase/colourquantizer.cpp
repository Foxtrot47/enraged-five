#include "colourquantizer.h"


using namespace rage;

unsigned char MASK[COLOURBITS] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
#define BITMASKOP(b,n)   (((b)&MASK[n])>>n)
#define LEVEL(c,d)((BITMASKOP((c).r,(d)))<<2 | BITMASKOP((c).g,(d))<<1 | BITMASKOP((c).b,(d)))

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ColourQuantizer::MakeReducible(int level, OctreeNode *node)
{
	node->nextNode = m_Reducelist[level];
	m_Reducelist[level] = node;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ColourQuantizer::OctreeNode* ColourQuantizer::GetReducible()
{
	OctreeNode *node;

	while (m_Reducelist[m_LeafLevel-1] == NULL) {
		m_LeafLevel--;
	}
	node = m_Reducelist[m_LeafLevel-1];
	m_Reducelist[m_LeafLevel-1] = m_Reducelist[m_LeafLevel-1]->nextNode;
	return node;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ColourQuantizer::OctreeNode* ColourQuantizer::CreateOctreeNode( int level )
{
	OctreeNode* newNode;

	newNode = rage_new OctreeNode();

	newNode->level = level;
	newNode->isLeaf = level == TREEDEPTH;
	if (newNode->isLeaf) 
		m_TotalLeaves++;
	else
		MakeReducible(level, newNode);


	return newNode;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ColourQuantizer::InsertOctreeNode( OctreeNode** node, ColourRGB& rgb, int depth )
{
	if( *node == NULL )
		*node = CreateOctreeNode(depth);

	if( (*node)->isLeaf )
	{
		(*node)->pixelCount++;
		(*node)->col.r += rgb.r;
		(*node)->col.g += rgb.g;
		(*node)->col.b += rgb.b;
	}
	else
	{
		int levelsFromBottom = TREEDEPTH-depth-1;
		int level = LEVEL(rgb, levelsFromBottom);
		InsertOctreeNode( &(*node)->children[level], rgb, depth+1 );
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ColourQuantizer::BuildOctree( atArray<ColourQuantizer::ColourRGB, 0, u32>& vertColList )
{
	m_RootNode = rage_new ColourQuantizer::OctreeNode();
	m_RootNode->level = 0;
	for(int i=0; i<vertColList.GetCount(); i++)
	{
		InsertOctreeNode( &m_RootNode, vertColList[i], 0 );
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ColourQuantizer::ReduceTree()
{
	OctreeNode  *node;
	unsigned long   sumred=0, sumgreen=0, sumblue=0;
	int    childCount=0;
	node = GetReducible();
	for (int i=0; i<COLOURBITS; i++) {
		if (node->children[i]) {
			childCount++;
			sumred += node->children[i]->col.r;	
			sumgreen += node->children[i]->col.g;
			sumblue += node->children[i]->col.b;
			node->pixelCount += node->children[i]->pixelCount;

			delete node->children[i];
			node->children[i] = NULL;
		}
	}
	node->isLeaf = true;
	node->col.r = sumred;
	node->col.g = sumgreen;
	node->col.b = sumblue;
	m_TotalLeaves -= (childCount - 1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ColourQuantizer::ReduceTreeToSpecifiedLeafCount( u32 leafNodes )
{
	while( m_TotalLeaves > leafNodes)
		ReduceTree();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ColourQuantizer::MakePaletteTable(OctreeNode* octreeNode, atArray<ColourRGB>& palette, int *index)
{
	int i;
	if (octreeNode->isLeaf) {
		ColourRGB col(0, 0, 0);

		col.r = (octreeNode->col.r / octreeNode->pixelCount);
		col.g = (octreeNode->col.g / octreeNode->pixelCount);
		col.b = (octreeNode->col.b / octreeNode->pixelCount);
		palette.PushAndGrow(col);
		octreeNode->index = *index;
		(*index)++;
	}
	else {
		for (i = 0; i < COLOURBITS; i++) {
			if (octreeNode->children[i]) {
				MakePaletteTable(octreeNode->children[i], palette, index);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int ColourQuantizer::QuantizeColor(OctreeNode *octreeNode, ColourRGB& colour)
{
	if (octreeNode->isLeaf) {
		return octreeNode->index;
	}
	else {
		return QuantizeColor(octreeNode->children[LEVEL(colour,8-octreeNode->level-1)],colour);
	}
}
