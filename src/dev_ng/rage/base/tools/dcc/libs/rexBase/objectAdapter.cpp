#if __TOOL

#include "result.h"
#include "value.h"
#include "object.h"
#include "objectAdapter.h"

#include "file/device.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"

// __TOOL builds are implicitly __WIN32
// need this to get a temp filename
#include "system/xtl.h"

///////////////////////////////////////////////////////////////////////////////
// class rexObjectAdapter
///////////////////////////////////////////////////////////////////////////////

namespace rage {

// static
unsigned int rexObjectAdapter::fileNum = 0;

rexObjectAdapter::rexObjectAdapter() : rexObject(), m_MagicStack(), m_ObjFile(fiHandleInvalid), m_ArrayMagic(0), m_ArrayReadFunc(NULL), m_ArrayWriteFunc(NULL)
{
}

// virtual
rexObjectAdapter::~rexObjectAdapter()
{
	CloseTempFile();
}

// static
rexResult rexObjectAdapter::ReadBool(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatBool);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems);
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteBool(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatBool);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems);
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE(bytesWritten, numItems);
}

// static
rexResult rexObjectAdapter::ReadByte(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatByte);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems);
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteByte(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatByte);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems);
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE(bytesWritten, numItems);
}

// static
rexResult rexObjectAdapter::ReadShort(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatShort);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems << 1);
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	itemsRead >>= 1;

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteShort(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatShort);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems << 1);
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE(bytesWritten, numItems << 1);
}

// static
rexResult rexObjectAdapter::ReadInt(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatInt);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems << 2);
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	itemsRead >>= 2;

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteInt(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatInt);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems << 2);
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE(bytesWritten, numItems << 2);
}

// static
rexResult rexObjectAdapter::ReadFloat(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatFloat);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems << 2);
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	itemsRead >>= 2;

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteFloat(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatFloat);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems << 2);
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE(bytesWritten, numItems << 2);
}

// static
rexResult rexObjectAdapter::ReadDouble(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatDouble);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems << 3);
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	itemsRead >>= 3;

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteDouble(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatDouble);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems << 3);
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE(bytesWritten, numItems << 3);
}

// static
rexResult rexObjectAdapter::ReadVector2(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatVector2);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems * sizeof(Vector2));
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	itemsRead /= sizeof(Vector2);

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteVector2(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatVector2);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems * sizeof(Vector2));
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE((unsigned int)bytesWritten, numItems * sizeof(Vector2));
}

// static
rexResult rexObjectAdapter::ReadVector3(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatVector3);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems * sizeof(Vector3));
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	itemsRead /= sizeof(Vector3);

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteVector3(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatVector3);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems * sizeof(Vector3));
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE((unsigned int)bytesWritten, numItems * sizeof(Vector3));
}

// static
rexResult rexObjectAdapter::ReadVector4(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatVector4);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems * sizeof(Vector4));
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	itemsRead /= sizeof(Vector4);

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteVector4(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatVector4);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems * sizeof(Vector4));
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE((unsigned int)bytesWritten, numItems * sizeof(Vector4));
}

// static
rexResult rexObjectAdapter::ReadMatrix44(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatMatrix44);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems * sizeof(Matrix44));
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	itemsRead /= sizeof(Matrix44);

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteMatrix44(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatMatrix44);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems * sizeof(Matrix44));
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE((unsigned int)bytesWritten, numItems * sizeof(Matrix44));
}

// static
rexResult rexObjectAdapter::ReadMatrix34(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatMatrix34);

	// read items into buffer
	itemsRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, items, numItems * sizeof(Matrix34));
	if(itemsRead < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	itemsRead /= sizeof(Matrix34);

	FINISH_IMPL_READ(numItemsToRead, (u32)itemsRead);
}

// static
rexResult rexObjectAdapter::WriteMatrix34(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatMatrix34);

	int bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, items, numItems * sizeof(Matrix34));
	if(bytesWritten < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	totalBytesWritten = (u32)bytesWritten;

	FINISH_IMPL_WRITE((unsigned int)bytesWritten, numItems * sizeof(Matrix34));
}

// static
rexResult rexObjectAdapter::ReadATString(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatATString);

	char *str = NULL;
	u32 totalBytesRead = 0, bytesRead = 0, curStrLen = 0, reqStrLen = 0;
	atString *strings = (atString*)items;

	for(u32 i = 0; i < numItemsToRead; i++)
	{
		reqStrLen = 0;

		// read length from file
		bytesRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, &reqStrLen, 4);
		if(bytesRead != 4)
			return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
		totalBytesRead += bytesRead;

		if(reqStrLen)
		{
			if(reqStrLen > curStrLen)
			{
				if(str)
					delete [] str;
				curStrLen = (reqStrLen + 511) & ~511; // round up to next multiple of 512 bytes
				str = new char[curStrLen];
				if(!str)
					return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);
			}

			// read string from file (including terminating NULL)
			bytesRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, str, reqStrLen);
			if(bytesRead != reqStrLen)
			{
				if(str)
					delete [] str;
				return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
			}
			totalBytesRead += bytesRead;

			// set string
			reqStrLen -= 1; // reqStrLen includes the NULL terminator
			strings[i].Set(str, reqStrLen, 0);
		}
		else
		{
			// reset string to 0 length
			strings[i].Reset();
		}

		itemsRead++;
	}

	if(str)
		delete [] str;

	FINISH_IMPL_READ(sizeToRead, totalBytesRead);
}

// static
rexResult rexObjectAdapter::WriteATString(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatATString);

	u32 bytesWritten = 0, curStrLen = 0;
	const atString *strings = (const atString*)items;

	for(u32 i = 0; i < (u32)numItems; i++)
	{
		curStrLen = strings[i].GetLength() + 1; // add 1 for NULL

		bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, &curStrLen, 4);
		if(bytesWritten != 4)
			return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
		totalBytesWritten += bytesWritten;

		if(curStrLen)
		{
			const char *str = (const char*)strings[i];

			bytesWritten = fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, str, curStrLen);
			if(bytesWritten != curStrLen)
				return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
			totalBytesWritten += bytesWritten;
		}
	}

	// NOTE: not using FINISH_IMPL_WRITE macro here because other error checking already done above
	curChunk->size += totalBytesWritten;
	return obj->FinishWriteChunk();
}

// static
rexResult rexObjectAdapter::ReadRexValue(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	START_IMPL_READ(roatRexValue);

	u32 totalBytesRead = 0, bytesRead = 0, headerSize = 0;

	u32 curBufSize = 1024;
	char *buf = new char[curBufSize];
	if(!buf)
		return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);
	u32 strBufSize = 0;
	atString *strbuf = NULL;

	void *readbuf = NULL;

	rexValue *values = (rexValue*)items;
	s32 curMagic = 0, subItemsRead = 0, curSubItem = 0;
	u32 curSize = 0, curNumItems = 0;
	rexObjectAdapter::ReadFunction readfunc = NULL;

	do {
		readbuf = NULL;

		// read header (always try to read 12 bytes, regardless of whether or not the numItems DWORD is there)
		bytesRead = fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, buf, 12);

		// chunk header might be at the end of the file
		// if 0 items in array, there will only be 8 bytes in header due to numItems compression
		if( !(8 == bytesRead || 12 == bytesRead) )
		{
			delete [] buf;
			if(strbuf)
				delete [] strbuf;
			return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
		}

		curMagic = ((s32*)buf)[0];
		curSize = ((u32*)buf)[1];

		// get number of items to read (might be stored in size DWORD)
		headerSize = 8;
		if( NumItemsInLengthMask != (curSize & NumItemsInLengthMask) )
		{
			curNumItems = (curSize >> NumItemsShift) & MaxNumItemsMask;
			curSize = (curSize & ~NumItemsInLengthMask);
		}
		else
		{
			curNumItems = 0;
			curSize &= ~NumItemsInLengthMask;
			if(12 == bytesRead)
			{
				curNumItems = (((u32*)buf)[2]);
				headerSize = 12;
			}
		}
		curSize -= headerSize;

		// seek backwards to start of header
		if( -1 == fiDeviceLocal::GetInstance().Seek(obj->m_ObjFile, -(s32)bytesRead, seekCur) )
		{
			delete [] buf;
			if(strbuf)
				delete [] strbuf;
			return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
		}

		// get correct function and buffer to read subtype items
		switch(curMagic)
		{
		case roatBool:
			readfunc = rexObjectAdapter::ReadBool;
			break;
		case roatByte:
			readfunc = rexObjectAdapter::ReadByte;
			break;
		case roatShort:
			readfunc = rexObjectAdapter::ReadShort;
			break;
		case roatInt:
			readfunc = rexObjectAdapter::ReadInt;
			break;
		case roatFloat:
			readfunc = rexObjectAdapter::ReadFloat;
			break;
		case roatDouble:
			readfunc = rexObjectAdapter::ReadDouble;
			break;
		case roatATString:
			readfunc = rexObjectAdapter::ReadATString;
			if(curNumItems > strBufSize)
			{
				if(strbuf)
					delete [] strbuf;
				strBufSize = curNumItems;
				strbuf = new atString[strBufSize];
				if(!strbuf)
				{
					delete [] buf;
					return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);
				}
			}
			readbuf = (void*)strbuf;
			break;

		// unsupported types in rexValue
		case roatVector2: // fall-through
		case roatVector3: // fall-through
		case roatVector4: // fall-through
		case roatMatrix44: // fall-through
		case roatMatrix34: // fall-through
		case roatATArray: // fall-through
		case roatRexValue: // fall-through
		default:
			delete [] buf;
			if(strbuf)
				delete [] strbuf;
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
			break;
		}

		// realloc buffer to hold items if necessary (if readbuf is already set for this iteration, must be a string read, so skip this step)
		if(!readbuf)
		{
			if(curSize > curBufSize)
			{
				delete [] buf;
				curBufSize = (curSize + 511) & ~511; // keep the size at a multiple of 512 bytes
				buf = new char[curBufSize];
				if(!buf)
				{
					if(strbuf)
						delete [] strbuf;
					return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);
				}
			}
			readbuf = (void*)buf;
		}

		if(!readbuf)
		{
			if(strbuf)
				delete [] strbuf;
			return rexResult(rexResult::ERRORS);
		}

		// read the subtypes into the buffer
		status = readfunc(obj, subItemsRead, curNumItems, readbuf);

		if(status.Errors())
		{
			delete [] buf;
			if(strbuf)
				delete [] strbuf;
			return status;
		}
		else
			totalBytesRead += (headerSize + curSize);

		// for each subtype item
		for(s32 i = 0; i < subItemsRead; curSubItem++, i++)
		{
			// set rexValue with value read from file
			switch(curMagic)
			{
			case roatBool:
				values[curSubItem].SetValue( ((bool*)buf)[i] );
				break;
			case roatByte:
				values[curSubItem].SetValue( buf[i] );
				break;
			case roatShort:
				values[curSubItem].SetValue( ((short*)buf)[i] );
				break;
			case roatInt:
				values[curSubItem].SetValue( ((int*)buf)[i] );
				break;
			case roatFloat:
				values[curSubItem].SetValue( ((float*)buf)[i] );
				break;
			case roatDouble:
				values[curSubItem].SetValue( ((double*)buf)[i] );
				break;
			case roatATString:
				values[curSubItem].SetValue( ((atString*)strbuf)[i] );
				break;

			// unsupported types in rexValue
			case roatVector2: // fall-through
			case roatVector3: // fall-through
			case roatVector4: // fall-through
			case roatMatrix44: // fall-through
			case roatMatrix34: // fall-through
			case roatATArray: // fall-through
			case roatRexValue: // fall-through
			default:
				delete [] buf;
				if(strbuf)
					delete [] strbuf;
				return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
				break;
			}
		} // for(s32 i = 0; i < subItemsRead; itemsRead++, i++)
	} while((u32)curSubItem < numItemsToRead);

	// delete buffers
	delete [] buf;
	if(strbuf)
		delete [] strbuf;

	// set the number of items read
	itemsRead = curSubItem;

	FINISH_IMPL_READ(totalBytesRead, sizeToRead);
}

// static
rexResult rexObjectAdapter::WriteRexValue(rexObjectAdapter *obj, int numItems, const void *items)
{
	START_IMPL_WRITE(roatRexValue);

	int curBufSize = 1024, reqBufSize = 0;
	char *buf = new char[curBufSize];
	if(!buf)
		return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);

	s32 itemsWritten = 0, startRun = 0, endRun = 0;
	rexValue::valueTypes runType = rexValue::UNKNOWN;
	const rexValue *values = (const rexValue*)items;
	rexObjectAdapter::WriteFunction writefunc = NULL;
	bool runIsString = false;

	atArray<atString> strings;

	const void *writebuf = NULL;

	while(itemsWritten < numItems)
	{
		reqBufSize = 0;
		writebuf = NULL;
		strings.Reset(); // clear items in strings array, but don't delete the contents

		// get the next run of a single type (convert all non-atStrings into atStrings)
		startRun = endRun;
		runType = values[startRun].GetType();
		runIsString = values[startRun].isString();
		// endRun++; // NOTE: intentionally comparing to the first item in order to handle conversion & copying for atString run
		while( endRun < numItems && ( values[endRun].GetType() == runType || (runIsString && values[endRun].isString()) ) )
		{
			if(runIsString)
			{
				int len;
				atString &str = strings.Grow((u16) numItems);
				// already checked types above if run is a string, so the only way in here is for the rexValue to be a string
				// NOTE: any type for which rexValue::isString() returns true must be handled in this case, or the write will fail!
				switch(values[endRun].GetType())
				{
				case(rexValue::STRING):
					len = (int)strlen(values[endRun].vCharPtr);
					str.Set(values[endRun].vCharPtr, len, 0);
					break;
				case(rexValue::CONSTSTRING):
					len = (int)strlen(values[endRun].vConstCharPtr);
					str.Set(values[endRun].vConstCharPtr, len, 0);
					break;
				case(rexValue::ATSTRING):
					str.Set(*values[endRun].GetATString(),0);
					break;
				default:
					delete [] buf;
					return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
					break;
				}
			}

			endRun++;
		}

		// get write function for the type of the run & allocate the right size of the buffer
		if(runIsString)
		{
			writefunc = rexObjectAdapter::WriteATString;

			// NOTE: need to do this because there isn't an accessor for the atArray internal array
			writebuf = (const void*)GET_ATARRAY_REF_ADDR(strings);
		}
		else
		{
			switch(runType)
			{
			case(rexValue::BOOLEAN):
				writefunc = rexObjectAdapter::WriteBool;
				reqBufSize = endRun - startRun;
				break;
			case(rexValue::BYTE):
				writefunc = rexObjectAdapter::WriteByte;
				reqBufSize = endRun - startRun;
				break;
			case(rexValue::SHORT):
				writefunc = rexObjectAdapter::WriteShort;
				reqBufSize = (endRun - startRun) << 1;
				break;
			case(rexValue::INTEGER):
				writefunc = rexObjectAdapter::WriteInt;
				reqBufSize = (endRun - startRun) << 2;
				break;
			case(rexValue::FLOAT):
				writefunc = rexObjectAdapter::WriteFloat;
				reqBufSize = (endRun - startRun) << 2;
				break;
			case(rexValue::DOUBLE):
				writefunc = rexObjectAdapter::WriteDouble;
				reqBufSize = (endRun - startRun) << 3;
				break;

			// these were already handled above!
			case(rexValue::STRING): // fall-through
			case(rexValue::CONSTSTRING): // fall-through
			case(rexValue::ATSTRING): // fall-through

			// unsupported types in serialized rexValue
			case(rexValue::UNKNOWN): // fall-through
			case(rexValue::VALUENULL): // fall-through
			default:
				delete [] buf;
				return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
				break;
			} // switch(runType)

			// reallocate buffer if necessary
			if(reqBufSize > curBufSize)
			{
				delete [] buf;
				curBufSize = (reqBufSize + 511) & ~511; // keep the size at a multiple of 512 bytes
				buf = new char[curBufSize];
				if(!buf)
					return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);
			}

			// set the write buffer to use
			writebuf = (const void*)buf;

			for(int i = startRun, j = 0; i < endRun; i++, j++)
			{
				// put each item into buffer for writing
				switch(runType)
				{
				case(rexValue::BOOLEAN):
					((char*)buf)[j] = values[i].vBoolean;
					break;
				case(rexValue::BYTE):
					((char*)buf)[j] = values[i].vByte;
					break;
				case(rexValue::SHORT):
					((short*)buf)[j] = values[i].vShort;
					break;
				case(rexValue::INTEGER):
					((int*)buf)[j] = values[i].vInt;
					break;
				case(rexValue::FLOAT):
					((float*)buf)[j] = values[i].vFloat;
					break;
				case(rexValue::DOUBLE):
					((double*)buf)[j] = values[i].vDouble;
					break;

				// these were already handled above!
				case(rexValue::STRING): // fall-through
				case(rexValue::CONSTSTRING): // fall-through
				case(rexValue::ATSTRING): // fall-through

				// unsupported types in serialized rexValue
				case(rexValue::UNKNOWN): // fall-through
				case(rexValue::VALUENULL): // fall-through
				default:
					delete [] buf;
					return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
					break;
				} // switch(runType)
			} // for(int i = startRun; i < endRun; i++)
		} // if(runIsString)

		// write the buffer to disk
		if( (status = writefunc(obj, (endRun - startRun), writebuf)).Errors() )
		{
			delete [] buf;
			return status;
		}

		itemsWritten += (endRun - startRun);
	}

	delete [] buf;

	totalBytesWritten = 0; // all the subobjects took care of adding the sizes, nothing was written by this function
	FINISH_IMPL_WRITE(itemsWritten, numItems);
}

// static
rexResult rexObjectAdapter::StartReadATArray(rexObjectAdapter *obj, s32 readMagic, ReadFunction readFunc, int &itemsToRead)
{
	static const s32 magic = roatATArray;

	if(!obj || !readMagic || !readFunc)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	if(0 > obj->m_ObjFile)
		return rexResult(rexResult::ERRORS, rexResult::FILE_NOT_OPEN);

	s32 filemagic = 0;
	u32 sizeToRead = 0, numItemsToRead = 0;
	rexResult status;

	if( (status = obj->StartReadChunk(filemagic, sizeToRead, numItemsToRead)).Errors() )
		return status;

	// NOTE: currently, only one array in a row is supported - number of arrays to read must be 1
	if(magic != filemagic || numItemsToRead != 1)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	// atArray chunks store the count of elements in the array directly after the header
	if( 4 != fiDeviceLocal::GetInstance().Read(obj->m_ObjFile, &numItemsToRead, 4) )
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	// set state in object
	// NOTE: state is stored there to attempt to ensure that it is consistent across the ReadATArray calls
	obj->m_ArrayMagic = readMagic;
	obj->m_ArrayReadFunc = readFunc;

	itemsToRead = numItemsToRead;

	return rexResult(rexResult::PERFECT);
}

// static
rexResult rexObjectAdapter::ReadATArray(rexObjectAdapter *obj, int &itemsRead, int numItems, void *items)
{
	if(!obj || !obj->m_ArrayMagic || !obj->m_ArrayReadFunc)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	rexResult status(rexResult::PERFECT);

	if(0 == numItems)
	{
		itemsRead = 0;
	}
	else
	{
		if( (status = obj->m_ArrayReadFunc(obj, itemsRead, numItems, items)).Errors() )
		{
			obj->m_ArrayMagic = 0;
			obj->m_ArrayReadFunc = NULL;

			return status;
		}
	}

	status.Combine( obj->FinishReadChunk() );

	obj->m_ArrayMagic = 0;
	obj->m_ArrayReadFunc = NULL;

	return status;
}

// static
rexResult rexObjectAdapter::StartWriteATArray(rexObjectAdapter *obj, s32 writeMagic, WriteFunction writeFunc, int numItems)
{
	static const s32 magic = roatATArray;

	if(!obj || !writeMagic || !writeFunc)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	if(0 > obj->m_ObjFile)
		return rexResult(rexResult::ERRORS, rexResult::FILE_NOT_OPEN);

	rexResult status;

	// NOTE: currently, only one array in a row is supported - number of arrays to write must be 1
	if( (status = obj->StartWriteChunk(magic, 1)).Errors() )
		return status;

	rexObjectAdapter::ChunkInfo *curChunk = obj->GetCurrentChunkInfo();
	if(!curChunk)
		return rexResult(rexResult::ERRORS);

	// atArray chunks store the count of elements in the array directly after the header
	if( 4 != fiDeviceLocal::GetInstance().Write(obj->m_ObjFile, &numItems, 4) )
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	// update size of chunk
	curChunk->size += 4;

	// set state in object
	// NOTE: state is stored there to attempt to ensure that it is consistent across the ReadATArray calls
	obj->m_ArrayMagic = writeMagic;
	obj->m_ArrayWriteFunc = writeFunc;

	return rexResult(rexResult::PERFECT);
}

// static
rexResult rexObjectAdapter::WriteATArray(rexObjectAdapter *obj, int numItems, const void *items)
{
	if(!obj || !obj->m_ArrayMagic || !obj->m_ArrayWriteFunc)
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);

	rexResult status(rexResult::PERFECT);

	if(0 != numItems)
	{
		if( (status = obj->m_ArrayWriteFunc(obj, numItems, items)).Errors() )
		{
			obj->m_ArrayMagic = 0;
			obj->m_ArrayWriteFunc = NULL;

			return status;
		}
	}

	status = obj->FinishWriteChunk();

	obj->m_ArrayMagic = 0;
	obj->m_ArrayWriteFunc = NULL;

	return status;
}

rexObjectAdapter::ChunkInfo *rexObjectAdapter::GetCurrentChunkInfo(void)
{
	return (m_MagicStack.GetCount()) ? &(m_MagicStack.Top()) : NULL;
}

rexObjectAdapter::ChunkInfo *rexObjectAdapter::PushMagic(s32 newMagic)
{
	rexObjectAdapter::ChunkInfo &newChunk = m_MagicStack.Grow();

	newChunk.magic = newMagic;

	return &newChunk;
}

rexObjectAdapter::ChunkInfo *rexObjectAdapter::PopMagic(u32 &sizeIdx, u32 &size)
{
	if(!m_MagicStack.GetCount())
		return NULL;

	rexObjectAdapter::ChunkInfo &chunk = m_MagicStack.Pop();

	sizeIdx = chunk.sizeIdx;
	size = chunk.size;

	// unused chunks in magic stack are always in reset state
	chunk.Reset();

	// return the new top-of-stack (or NULL if nothing's left)
	if(m_MagicStack.GetCount())
		return &m_MagicStack.Top();
	else
		return NULL;
}

bool rexObjectAdapter::CurrentMagicMatches(s32 magic)
{
	return (m_MagicStack.GetCount()) ? (m_MagicStack.Top().magic == magic) : false;
}

rexResult rexObjectAdapter::StartReadChunk(s32 &magic, u32 &size, u32 &numItems)
{
	if(0 > m_ObjFile)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	int headerSize = 8;
	rexObjectAdapter::ChunkInfo chunk;

	if( 4 != fiDeviceLocal::GetInstance().Read(m_ObjFile, &chunk.magic, 4) )
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	if( 4 != fiDeviceLocal::GetInstance().Read(m_ObjFile, &chunk.size, 4) )
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	if(NumItemsInLengthMask == (chunk.size & NumItemsInLengthMask))
	{
		u32 ni;
		if( 4 != fiDeviceLocal::GetInstance().Read(m_ObjFile, &ni, 4) )
			return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

		headerSize += 4;
		numItems = ni;
	}
	else
	{
		numItems = (chunk.size >> NumItemsShift) & MaxNumItemsMask;
	}

	// NOTE: when reading, the size stored in the ChunkInfo object does not have the length information,
	// and the size of the header has already been subtracted from the value (i.e. the size is the count
	// of bytes remaining in the chunk from the file pointer position)
	magic = chunk.magic;
	size = chunk.size = (chunk.size & ~NumItemsInLengthMask) - headerSize;

	rexObjectAdapter::ChunkInfo *newChunk = PushMagic(chunk.magic);

	if(!newChunk)
		return rexResult(rexResult::ERRORS);

	newChunk->size = chunk.size;

	return rexResult(rexResult::PERFECT);
}

rexResult rexObjectAdapter::FinishReadChunk(void)
{
	// pop top off magic stack
	u32 sizeIdx = 0, size = 0;
	PopMagic(sizeIdx, size);

	// NOTE: not keeping track of chunk sizes during read

	return rexResult(rexResult::PERFECT);
}

rexResult rexObjectAdapter::StartWriteChunk(s32 magic, u32 numItems)
{
	if(0 > m_ObjFile)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	// push new chunk info onto magic stack
	rexObjectAdapter::ChunkInfo *chunk = PushMagic(magic);

	// write magic number to file
	if( 4 != fiDeviceLocal::GetInstance().Write(m_ObjFile, &(chunk->magic), 4) )
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	// save size location pointer
	chunk->sizeIdx = fiDeviceLocal::GetInstance().Seek(m_ObjFile, 0, seekCur);
	if(chunk->sizeIdx < 0)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	// reserve space for size
	if( 4 != fiDeviceLocal::GetInstance().Write(m_ObjFile, &(chunk->size), 4) )
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	// if necessary, write numItems to file
	if(numItems <= MaxNumItemsInLength)
	{
		chunk->size |= (numItems << NumItemsShift);
		chunk->size += 8;
	}
	else
	{
		chunk->size |= NumItemsInLengthMask;
		chunk->size += 12;
		if( 4 != fiDeviceLocal::GetInstance().Write(m_ObjFile, &numItems, 4) )
			return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	}

	return rexResult(rexResult::PERFECT);
}

rexResult rexObjectAdapter::FinishWriteChunk(void)
{
	u32 sizeIdx = 0, size = 0;

	// pop top off magic stack
	rexObjectAdapter::ChunkInfo *top = PopMagic(sizeIdx, size);

	// if there's something on top of the stack, add the size of the just-written chunk to it
	if(top)
	{
		if( ((top->size & ~NumItemsInLengthMask) + (size & ~NumItemsInLengthMask)) > ~NumItemsInLengthMask)
			return rexResult(rexResult::ERRORS); // adding this chunk would overflow the max size! (need a status code for this)
		else
			top->size += (size & ~NumItemsInLengthMask);
	}

	// write size to file

	// NOTE: this seek will not work on PS2 because the offset must be a multiple of 512
	if(-1 == fiDeviceLocal::GetInstance().Seek(m_ObjFile, (int)sizeIdx, seekSet))
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	if(4 != fiDeviceLocal::GetInstance().Write(m_ObjFile, &size, 4))
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	// seek back to the end of the file
	if(-1 == fiDeviceLocal::GetInstance().Seek(m_ObjFile, 0, seekEnd))
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
	else
		return rexResult(rexResult::PERFECT);
}

rexResult rexObjectAdapter::ReadBuffer(int bufsize, void *buf, int &bytesRead)
{
	if(0 > m_ObjFile)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	// NOTE: this read will only work on PS2 if the buffer is 512 bytes and aligned on a 64-byte boundary
	bytesRead = fiDeviceLocal::GetInstance().Read(m_ObjFile, buf, bufsize);

	return rexResult(rexResult::PERFECT);
}

rexResult rexObjectAdapter::WriteBuffer(int bufsize, const void *buf)
{
	if(0 > m_ObjFile)
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	// NOTE: this write will only work on PS2 if the buffer is 512 bytes and aligned on a 64-byte boundary
	int bytesWritten = fiDeviceLocal::GetInstance().Write(m_ObjFile, buf, bufsize);

	if(bytesWritten == bufsize)
		return rexResult(rexResult::PERFECT);
	else
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
}

rexResult rexObjectAdapter::OpenTempFile(atString &filename)
{
	// don't open the file if something is open
	if(fiIsValidHandle(m_ObjFile))
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	// open the file in readonly mode
	if( fiIsValidHandle(m_ObjFile =  fiDeviceLocal::GetInstance().Open(filename, true)) )
		return rexResult(rexResult::PERFECT);
	else
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
}

rexResult rexObjectAdapter::CloseTempFile(void)
{
	if(fiIsValidHandle(m_ObjFile))
		if( 0 != fiDeviceLocal::GetInstance().Close(m_ObjFile) )
			return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	m_ObjFile = fiHandleInvalid;

	return rexResult(rexResult::PERFECT);
}

rexResult rexObjectAdapter::CreateTempFile(atString &filename)
{
	// don't create the file if something is open
	if(fiIsValidHandle(m_ObjFile))
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	if( fiIsValidHandle(m_ObjFile = fiDeviceLocal::GetInstance().Create(filename)) )
		return rexResult(rexResult::PERFECT);
	else
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
}

rexResult rexObjectAdapter::DeleteTempFile(atString &filename)
{
	// don't delete the file if something is open
	if(fiIsValidHandle(m_ObjFile))
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);

	if( fiDeviceLocal::GetInstance().Delete(filename) )
		return rexResult(rexResult::PERFECT);
	else
		return rexResult(rexResult::ERRORS, rexResult::FILE_ERROR);
}

// virtual
rexResult rexObjectAdapter::MakeFilename(atString &filename)
{
	char buf[MAX_PATH];
	int pathlen = 0;

	if( 0 == (pathlen = GetTempPath(MAX_PATH, buf)) )
		return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);

	filename = buf;
	//filename += "rex\\"; // TODO: to put things in a directory, we need to make the directory first

	// NOTE: assuming that we will be writing fewer than 2^16 files (GetTempFileName uses the hex form of the lower 2 bytes of fileNum)
	GetTempFileName((const char*)filename, "REX", ++fileNum, buf);

	filename = buf;

	return rexResult(rexResult::PERFECT);
}

///////////////////////////////////////////////////////////////////////////////
// class rexObjectAdapterPtr
///////////////////////////////////////////////////////////////////////////////
bool rexObjectAdapterPtr::allowSavingToDisk = false;
bool rexObjectAdapterPtr::deleteTempFiles = false;

rexResult rexObjectAdapterPtr::Save(void)
{
	if(!allowSavingToDisk)
		return rexResult(rexResult::PERFECT);

	rexResult status;
	if(0 == m_path.GetLength())
		if(m_obj)
		{
			if( (status = m_obj->MakeFilename(m_path)).Errors() )
				return status;
		}
		else
			return rexResult(rexResult::ERRORS, rexResult::CANNOT_CREATE_FILE);

	return m_obj->Save(m_path);
}

rexResult rexObjectAdapterPtr::Load(void)
{
	if(!allowSavingToDisk)
		return rexResult(rexResult::PERFECT);

	if(m_obj || !m_creator)
		return rexResult(rexResult::ERRORS, rexResult::CANNOT_CREATE_OBJECT);
	if(0 == m_path.GetLength())
		return rexResult(rexResult::ERRORS, rexResult::FILE_NOT_FOUND);

	rexResult status(rexResult::PERFECT);
	m_obj = m_creator();

	if(m_obj)
	{
		if( (status = m_obj->Load(m_path)).Errors() )
		{
			// if there were errors during load, get rid of the object (don't want invalid state to exist in memory)
			delete m_obj;
			m_obj = NULL;
		}
	}
	else
		status.Set(rexResult::ERRORS, rexResult::CANNOT_CREATE_OBJECT);

	return status;
}

rexResult rexObjectAdapterPtr::Unload(void)
{
	if(!allowSavingToDisk)
		return rexResult(rexResult::PERFECT);

	if(!m_obj || 0 == m_path.GetLength())
		return rexResult(rexResult::ERRORS, rexResult::FILE_NOT_FOUND);

	delete m_obj;
	m_obj = NULL;

	return rexResult(rexResult::PERFECT);
}

#if 0
// this code can be used to test the rexObjectAdapter with the below class
{
	roaTest *tester = new roaTest();
	rexObjectAdapterPtr ptr(tester, roaTest::CreateAdapter);
	ptr.Save();
	ptr.Unload();
	ptr.Load();
}

class roaTest : public rexObjectAdapter
{
public:
	roaTest() : rexObjectAdapter() {}
	static rexObjectAdapter *CreateAdapter(void) { return new roaTest(); }
	virtual rexResult Save(atString &filename)
	{
		static const s32 magic = MAKE_MAGIC_NUMBER('T','E','S','T');

		if( 0 == filename.GetLength() )
			return rexResult(rexResult::ERRORS, rexResult::CANNOT_CREATE_FILE);

		rexResult status;

		if( (status = CreateTempFile(filename)).Errors() )
			return status;

		if( (status = StartWriteChunk(magic, 1)).Errors() )
		{
			CloseTempFile();
			return status;
		}

		// start saving code

		int i;
		float f;
		bool bbuf[10];
		char cbuf[10];
		short sbuf[10];
		int ibuf[10];
		float fbuf[10];
		double dbuf[10];
		Vector2 v2buf[10];
		Vector3 v3buf[10];
		Vector4 v4buf[10];
		Matrix44 m44buf[10];
		Matrix34 m34buf[10];
		rexValue rvbuf[15];
		atArray<Vector3> atarVec3;
		atArray<rexValue> atarValue;
		atArray<int> atarEmpty;

		char strbuf[12] = "test string";

		// init data
		for(i = 0, f = 0.0f; i < 10; i++, f += 1.0f)
		{
			bbuf[i] = (bool)(i & 1);
			cbuf[i] = (char)i;
			sbuf[9-i] = (short)i;
			ibuf[i] = i;
			fbuf[9-i] = (float)i;
			dbuf[i] = (double)i;
			v2buf[i].Set(f,f+1.f);
			v3buf[i].Set(f+2.f,f+1.f,f);
			atarVec3.Grow(10) = v3buf[i];
			v4buf[i].Set(f,f+1.f,f+2.f,f+3.f);

			m44buf[i].a.Set(v4buf[i]);
			m44buf[i].b.Set(v4buf[i]);
			m44buf[i].c.Set(v4buf[i]);
			m44buf[i].d.Set(v4buf[i]);

			m34buf[i].a.Set(v3buf[i]);
			m34buf[i].b.Set(v3buf[i]);
			m34buf[i].c.Set(v3buf[i]);
			m34buf[i].d.Set(v3buf[i]);
		}

		rvbuf[0].SetValue(bbuf[1]); // bool
		rvbuf[1].SetValue(cbuf[1]); // byte
		rvbuf[2].SetValue(sbuf[1]); // short
		rvbuf[3].SetValue(ibuf[1]); // int
		rvbuf[4].SetValue(fbuf[1]); // float
		rvbuf[5].SetValue(dbuf[1]); // double
		rvbuf[6].SetValue((char*)strbuf); // string
		rvbuf[7].SetValue("test const string"); // string
		rvbuf[8].SetValue(atString("test atString")); // string
		rvbuf[9].SetValue(ibuf[4]); // int
		rvbuf[10].SetValue(ibuf[5]); // int
		rvbuf[11].SetValue(ibuf[6]); // int
		rvbuf[12].SetValue(ibuf[7]); // int
		rvbuf[13].SetValue(ibuf[8]); // int
		rvbuf[14].SetValue(ibuf[9]); // int

		for(i = 0; i < 15; i++)
			atarValue.Grow(15) = rvbuf[i];

		// write data

		if( (status = WriteBool(this, 10, bbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteByte(this, 10, cbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteShort(this, 10, sbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteInt(this, 10, ibuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteFloat(this, 10, fbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteDouble(this, 10, dbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteVector2(this, 10, v2buf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteVector3(this, 10, v3buf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteVector4(this, 10, v4buf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteMatrix44(this, 10, m44buf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteMatrix34(this, 10, m34buf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteRexValue(this, 15, rvbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}

		if( (status = StartWriteATArray(this, roatVector3, WriteVector3, atarVec3.GetCount())).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(this, atarVec3.GetCount(), GET_ATARRAY_REF_ADDR(atarVec3))).Errors() )
		{
			CloseTempFile();
			return status;
		}

		if( (status = StartWriteATArray(this, roatRexValue, WriteRexValue, atarValue.GetCount())).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(this, atarValue.GetCount(), GET_ATARRAY_REF_ADDR(atarValue))).Errors() )
		{
			CloseTempFile();
			return status;
		}

		if( (status = StartWriteATArray(this, roatInt, WriteInt, atarEmpty.GetCount())).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if( (status = WriteATArray(this, atarEmpty.GetCount(), GET_ATARRAY_REF_ADDR(atarEmpty))).Errors() )
		{
			CloseTempFile();
			return status;
		}

		// finish saving code

		if( (status = FinishWriteChunk()).Errors() )
		{
			CloseTempFile();
			return status;
		}

		if( (status = CloseTempFile()).Errors() )
			return status;

		return rexResult(rexResult::PERFECT);
	}

	virtual rexResult Load(atString &filename)
	{
		static const s32 magic = MAKE_MAGIC_NUMBER('T','E','S','T');

		if( 0 == filename.GetLength() )
			return rexResult(rexResult::ERRORS, rexResult::FILE_NOT_FOUND);

		rexResult status;

		if( (status = OpenTempFile(filename)).Errors() )
			return status;

		s32 filemagic = 0;
		u32 sizeToRead = 0, numItems = 0;

		if( (status = StartReadChunk(filemagic, sizeToRead, numItems)).Errors() )
		{
			CloseTempFile();
			return status;
		}

		if(filemagic != magic || numItems != 1)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		// start loading code

		int i, itemsRead = 0;
		bool bbuf[10];
		char cbuf[10];
		short sbuf[10];
		int ibuf[10];
		float fbuf[10];
		double dbuf[10];
		Vector2 v2buf[10];
		Vector3 v3buf[10];
		Vector4 v4buf[10];
		Matrix44 m44buf[10];
		Matrix34 m34buf[10];

		rexValue rvbuf[15];

		atArray<Vector3> atarVec3;
		atArray<rexValue> atarValue;
		atArray<int> atarEmpty;

		// init data
		for(i = 0; i < 10; i++)
		{
			bbuf[i] = false;
			cbuf[i] = 0;
			sbuf[i] = 0;
			ibuf[i] = 0;
			fbuf[i] = 0.0f;
			dbuf[i] = 0.0;
			v2buf[i].Set(0.0f,0.0f);
			v3buf[i].Set(0.0f,0.0f,0.0f);
			v4buf[i].Set(0.0f,0.0f,0.0f,0.0f);

			m44buf[i].a.Set(0.0f,0.0f,0.0f,0.0f);
			m44buf[i].b.Set(0.0f,0.0f,0.0f,0.0f);
			m44buf[i].c.Set(0.0f,0.0f,0.0f,0.0f);
			m44buf[i].d.Set(0.0f,0.0f,0.0f,0.0f);

			m34buf[i].a.Set(0.0f,0.0f,0.0f);
			m34buf[i].b.Set(0.0f,0.0f,0.0f);
			m34buf[i].c.Set(0.0f,0.0f,0.0f);
			m34buf[i].d.Set(0.0f,0.0f,0.0f);
		}

		// read data

		itemsRead = 0;
		if( (status = ReadBool(this, itemsRead, 10, bbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		itemsRead = 0;
		if( (status = ReadByte(this, itemsRead, 10, cbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);		}

		itemsRead = 0;
		if( (status = ReadShort(this, itemsRead, 10, sbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		itemsRead = 0;
		if( (status = ReadInt(this, itemsRead, 10, ibuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		itemsRead = 0;
		if( (status = ReadFloat(this, itemsRead, 10, fbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		itemsRead = 0;
		if( (status = ReadDouble(this, itemsRead, 10, dbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		itemsRead = 0;
		if( (status = ReadVector2(this, itemsRead, 10, v2buf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		itemsRead = 0;
		if( (status = ReadVector3(this, itemsRead, 10, v3buf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		itemsRead = 0;
		if( (status = ReadVector4(this, itemsRead, 10, v4buf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		itemsRead = 0;
		if( (status = ReadMatrix44(this, itemsRead, 10, m44buf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		itemsRead = 0;
		if( (status = ReadMatrix34(this, itemsRead, 10, m34buf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(10 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		itemsRead = 0;
		if( (status = ReadRexValue(this, itemsRead, 15, rvbuf)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		if(15 != itemsRead)
		{
			CloseTempFile();
			return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
		}

		int itemsToRead;
		void *itemArray;

		itemsToRead = 0;
		itemArray = NULL;
		itemsRead = 0;
		if( (status = StartReadATArray(this, roatVector3, ReadVector3, itemsToRead)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(atarVec3, itemsToRead, itemArray);
		if( (status = ReadATArray(this, itemsRead, itemsToRead, itemArray)).Errors() )
		{
			CloseTempFile();
			return status;
		}

		itemsToRead = 0;
		itemArray = NULL;
		itemsRead = 0;
		if( (status = StartReadATArray(this, roatRexValue, ReadRexValue, itemsToRead)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(atarValue, itemsToRead, itemArray);
		if( (status = ReadATArray(this, itemsRead, itemsToRead, itemArray)).Errors() )
		{
			CloseTempFile();
			return status;
		}

		itemsToRead = 0;
		itemArray = NULL;
		itemsRead = 0;
		if( (status = StartReadATArray(this, roatInt, ReadInt, itemsToRead)).Errors() )
		{
			CloseTempFile();
			return status;
		}
		READ_ATARRAY_REF_HELPER(atarEmpty, itemsToRead, itemArray);
		if( (status = ReadATArray(this, itemsRead, itemsToRead, itemArray)).Errors() )
		{
			CloseTempFile();
			return status;
		}

		// finish loading code

		if( (status = FinishReadChunk()).Errors() )
		{
			CloseTempFile();
			return status;
		}

		if( (status = CloseTempFile()).Errors() )
			return status;

		return rexResult(rexResult::PERFECT);
	}
}; // class roaTest : public rexObjectAdapter

// tester code in this block
#endif // #if 0

} // namespace rage

#endif // #if __TOOL
