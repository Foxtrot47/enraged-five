/////////////////////////////////////////////////////////////////////////////////////

#include "logAndErrorGenerator.h"
#include "file/stream.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

rexLogAndErrorGenerator::rexLogAndErrorGenerator()
{
	m_LogStream = NULL;
}

////////////////

bool rexLogAndErrorGenerator::BeginLog( fiStream* s )
{
	m_LogStream = s;
	return( s != NULL );
}

////////////////

bool rexLogAndErrorGenerator::EndLog()
{
	if( !m_LogStream )
		return false;
	m_LogStream = NULL;
	return true;
}

////////////////

const atArray<atString>& rexLogAndErrorGenerator::RetrieveErrorMessages() const
{
	return m_ErrorMessages;
}

////////////////

bool rexLogAndErrorGenerator::WriteToLog( const atString& message ) const
{
	if( !m_LogStream )
		return false;
	
	int bytesWritten = m_LogStream->Write( (const char*)message, message.GetLength() );
	bytesWritten += m_LogStream->Write( "\r\n", sizeof( "\r\n" ) );

	return( bytesWritten > 0 );
}

////////////////

bool rexLogAndErrorGenerator::ReportError( const atString& errorMessage ) const
{
	m_ErrorMessages.Grow().Set( errorMessage, 0 );
	return true;
}

////////////////

void rexLogAndErrorGenerator::ClearErrorMessages() const
{
	m_ErrorMessages.Reset();
}

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////
