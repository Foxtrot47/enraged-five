//
// rexBase/mgc.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "rexBase/mgc.h"

namespace rage {
namespace Mgc {

static const Real gs_fEpsilon = 1e-06f;
static const Real gs_fOnePlusEpsilon = 1.0f + gs_fEpsilon;

const Real Math::MAX_REAL = FLT_MAX;
const Real Math::PI_ = 4.0f*Math::ATan(1.0f);
const Real Math::TWO_PI = 2.0f*PI_;
const Real Math::HALF_PI = 0.5f*PI_;
const Real Math::INV_TWO_PI = 1.0f/TWO_PI;
const Real Math::DEG_TO_RAD = Math::PI_/180.0f;
const Real Math::RAD_TO_DEG = 1.0f/Math::DEG_TO_RAD;

	//----------------------------------------------------------------------------
MinimizeND::MinimizeND (int iDimensions, Function oF, int iMaxLevel,
	int iMaxBracket, int iMaxIterations, void* pvUserData)
	:
m_kMinimizer(LineFunction,iMaxLevel,iMaxBracket)
{
	Assert( iDimensions >= 1 && oF );

	m_iDimensions = iDimensions;
	m_oF = oF;
	m_iMaxIterations = iMaxIterations;
	m_pvUserData = pvUserData;

	m_afDCurr = NULL;
	m_fFCurr = 0.0f;

	m_afTCurr = new Real[m_iDimensions];
	m_afTSave = new Real[m_iDimensions];
	m_afDirectionStorage = new Real[m_iDimensions*(m_iDimensions+1)];
	m_aafDirection = new Real*[m_iDimensions+1];
	for (int i = 0; i <= m_iDimensions; i++)
		m_aafDirection[i] = &m_afDirectionStorage[i*m_iDimensions];
	m_afDConj = m_aafDirection[m_iDimensions];

	m_afLineArg = new Real[m_iDimensions];
}
//----------------------------------------------------------------------------
MinimizeND::~MinimizeND ()
{
	m_pvUserData = NULL;
	m_afDCurr = NULL;
	m_afDConj = NULL;

	delete[] m_afLineArg;
	m_afLineArg = NULL;
	delete[] m_aafDirection;
	m_aafDirection = NULL;
	delete[] m_afDirectionStorage;
	m_afDirectionStorage = NULL;
	delete[] m_afTSave;
	m_afTSave = NULL;
	delete[] m_afTCurr;
	m_afTCurr = NULL;
}
//----------------------------------------------------------------------------
void MinimizeND::GetMinimum (const Real* afT0, const Real* afT1,
	const Real* afTInitial, Real* afTMin, Real& rfFMin)
{
	// for 1D function callback
	m_kMinimizer.UserData() = this;

	// initial guess
	int iQuantity = m_iDimensions*sizeof(Real);
	m_fFCurr = m_oF(afTInitial,m_pvUserData);
	memcpy(m_afTSave,afTInitial,iQuantity);
	memcpy(m_afTCurr,afTInitial,iQuantity);

	// initialize direction set to the standard Euclidean basis
	memset(m_afDirectionStorage,0,iQuantity*(m_iDimensions+1));
	int i;
	for (i = 0; i < m_iDimensions; i++)
		m_aafDirection[i][i] = 1.0f;

	Real fL0, fL1, fLMin;
	for (int iIter = 0; iIter < m_iMaxIterations; iIter++)
	{
		// find minimum in each direction and update current location
		for (i = 0; i < m_iDimensions; i++)
		{
			m_afDCurr = m_aafDirection[i];
			ComputeDomain(afT0,afT1,fL0,fL1);
			m_kMinimizer.GetMinimum(fL0,fL1,0.0f,fLMin,m_fFCurr);
			for (int j = 0; j < m_iDimensions; j++)
				m_afTCurr[j] += fLMin*m_afDCurr[j];
		}

		// estimate a unit-length conjugate direction
		Real fLength = 0.0f;
		for (i = 0; i < m_iDimensions; i++)
		{
			m_afDConj[i] = m_afTCurr[i] - m_afTSave[i];
			fLength += m_afDConj[i]*m_afDConj[i];
		}

		const Real fEpsilon = 1e-06f;
		fLength = Math::Sqrt(fLength);
		if ( fLength < fEpsilon )
		{
			// New position did not change significantly from old one.
			// Should there be a better convergence criterion here?
			break;
		}

		Real fInvLength = 1.0f/fLength;
		for (i = 0; i < m_iDimensions; i++)
			m_afDConj[i] *= fInvLength;

		// minimize in conjugate direction
		m_afDCurr = m_afDConj;
		ComputeDomain(afT0,afT1,fL0,fL1);
		m_kMinimizer.GetMinimum(fL0,fL1,0.0f,fLMin,m_fFCurr);
		for (i = 0; i < m_iDimensions; i++)
			m_afTCurr[i] += fLMin*m_afDCurr[i];

		// cycle the directions and add conjugate direction to set
		m_afDConj = m_aafDirection[0];
		for (i = 0; i < m_iDimensions; i++)
			m_aafDirection[i] = m_aafDirection[i+1];

		// set parameters for next pass
		memcpy(m_afTSave,m_afTCurr,iQuantity);
	}

	memcpy(afTMin,m_afTCurr,iQuantity);
	rfFMin = m_fFCurr;
}
//----------------------------------------------------------------------------
void MinimizeND::ComputeDomain (const Real* afT0, const Real* afT1,
	Real& rfL0, Real& rfL1)
{
	rfL0 = -Math::MAX_REAL;
	rfL1 = +Math::MAX_REAL;

	for (int i = 0; i < m_iDimensions; i++)
	{
		Real fB0 = afT0[i] - m_afTCurr[i];
		Real fB1 = afT1[i] - m_afTCurr[i];
		Real fInv;
		if ( m_afDCurr[i] > 0.0f )
		{
			// valid t-interval is [B0,B1]
			fInv = 1.0f/m_afDCurr[i];
			fB0 *= fInv;
			if ( fB0 > rfL0 )
				rfL0 = fB0;
			fB1 *= fInv;
			if ( fB1 < rfL1 )
				rfL1 = fB1;
		}
		else if ( m_afDCurr[i] < 0.0f )
		{
			// valid t-interval is [B1,B0]
			fInv = 1.0f/m_afDCurr[i];
			fB0 *= fInv;
			if ( fB0 < rfL1 )
				rfL1 = fB0;
			fB1 *= fInv;
			if ( fB1 > rfL0 )
				rfL0 = fB1;
		}
	}

	// correction if numerical errors lead to values nearly zero
	if ( rfL0 > 0.0f )
		rfL0 = 0.0f;
	if ( rfL1 < 0.0f )
		rfL1 = 0.0f;
}
//----------------------------------------------------------------------------
Real MinimizeND::LineFunction (Real fT, void* pvUserData)
{
	MinimizeND* pkThis = (MinimizeND*) pvUserData;

	for (int i = 0; i < pkThis->m_iDimensions; i++)
	{
		pkThis->m_afLineArg[i] = pkThis->m_afTCurr[i] +
			fT*pkThis->m_afDCurr[i];
	}

	Real fResult = pkThis->m_oF(pkThis->m_afLineArg,pkThis->m_pvUserData);
	return fResult;
}
//----------------------------------------------------------------------------
Minimize1D::Minimize1D (Function oF, int iMaxLevel, int iMaxBracket,
	void* pvUserData)
{
	Assert( oF );
	m_oF = oF;
	m_iMaxLevel = iMaxLevel;
	m_iMaxBracket = iMaxBracket;
	m_pvUserData = pvUserData;
}
//----------------------------------------------------------------------------
void Minimize1D::GetMinimum (Real fT0, Real fT1, Real fTInitial,
	Real& rfTMin, Real& rfFMin)
{
	Assert( fT0 <= fTInitial && fTInitial <= fT1 );

	m_fTMin = Math::MAX_REAL;
	m_fFMin = Math::MAX_REAL;

	Real fF0 = m_oF(fT0,m_pvUserData);
	Real fFInitial = m_oF(fTInitial,m_pvUserData);
	Real fF1 = m_oF(fT1,m_pvUserData);

	GetMinimum(fT0,fF0,fTInitial,fFInitial,fT1,fF1,m_iMaxLevel);

	rfTMin = m_fTMin;
	rfFMin = m_fFMin;
}
//----------------------------------------------------------------------------
void Minimize1D::GetMinimum (Real fT0, Real fF0, Real fTm, Real fFm,
	Real fT1, Real fF1, int iLevel)
{
	if ( fF0 < m_fFMin )
	{
		m_fTMin = fT0;
		m_fFMin = fF0;
	}

	if ( fFm < m_fFMin )
	{
		m_fTMin = fTm;
		m_fFMin = fFm;
	}

	if ( fF1 < m_fFMin )
	{
		m_fTMin = fT1;
		m_fFMin = fF1;
	}

	if ( iLevel-- == 0 )
		return;

	if ( (fT1 - fTm)*(fF0 - fFm) > (fTm - fT0)*(fFm - fF1) )
	{
		// quadratic fit has positive second derivative at midpoint

		if ( fF1 > fF0 )
		{
			if ( fFm >= fF0 )
			{
				// increasing, repeat on [t0,tm]
				GetMinimum(fT0,fF0,fTm,fFm,iLevel);
			}
			else
			{
				// not monotonic, have a bracket
				GetBracketedMinimum(fT0,fF0,fTm,fFm,fT1,fF1,iLevel);
			}
		}
		else if ( fF1 < fF0 )
		{
			if ( fFm >= fF1 )
			{
				// decreasing, repeat on [tm,t1]
				GetMinimum(fTm,fFm,fT1,fF1,iLevel);
			}
			else
			{
				// not monotonic, have a bracket
				GetBracketedMinimum(fT0,fF0,fTm,fFm,fT1,fF1,iLevel);
			}
		}
		else
		{
			// constant, repeat on [t0,tm] and [tm,t1]
			GetMinimum(fT0,fF0,fTm,fFm,iLevel);
			GetMinimum(fTm,fFm,fT1,fF1,iLevel);
		}
	}
	else
	{
		// quadratic fit has nonpositive second derivative at midpoint

		if ( fF1 > fF0 )
		{
			// repeat on [t0,tm]
			GetMinimum(fT0,fF0,fTm,fFm,iLevel);
		}
		else if ( fF1 < fF0 )
		{
			// repeat on [tm,t1]
			GetMinimum(fTm,fFm,fT1,fF1,iLevel);
		}
		else
		{
			// repeat on [t0,tm] and [tm,t1]
			GetMinimum(fT0,fF0,fTm,fFm,iLevel);
			GetMinimum(fTm,fFm,fT1,fF1,iLevel);
		}
	}
}
//----------------------------------------------------------------------------
void Minimize1D::GetMinimum (Real fT0, Real fF0, Real fT1, Real fF1,
	int iLevel)
{
	if ( fF0 < m_fFMin )
	{
		m_fTMin = fT0;
		m_fFMin = fF0;
	}

	if ( fF1 < m_fFMin )
	{
		m_fTMin = fT1;
		m_fFMin = fF1;
	}

	if ( iLevel-- == 0 )
		return;

	Real fTm = 0.5f*(fT0+fT1);
	Real fFm = m_oF(fTm,m_pvUserData);

	if ( fF0 - 2.0f*fFm + fF1 > 0.0f )
	{
		// quadratic fit has positive second derivative at midpoint

		if ( fF1 > fF0 )
		{
			if ( fFm >= fF0 )
			{
				// increasing, repeat on [t0,tm]
				GetMinimum(fT0,fF0,fTm,fFm,iLevel);
			}
			else
			{
				// not monotonic, have a bracket
				GetBracketedMinimum(fT0,fF0,fTm,fFm,fT1,fF1,iLevel);
			}
		}
		else if ( fF1 < fF0 )
		{
			if ( fFm >= fF1 )
			{
				// decreasing, repeat on [tm,t1]
				GetMinimum(fTm,fFm,fT1,fF1,iLevel);
			}
			else
			{
				// not monotonic, have a bracket
				GetBracketedMinimum(fT0,fF0,fTm,fFm,fT1,fF1,iLevel);
			}
		}
		else
		{
			// constant, repeat on [t0,tm] and [tm,t1]
			GetMinimum(fT0,fF0,fTm,fFm,iLevel);
			GetMinimum(fTm,fFm,fT1,fF1,iLevel);
		}
	}
	else
	{
		// quadratic fit has nonpositive second derivative at midpoint

		if ( fF1 > fF0 )
		{
			// repeat on [t0,tm]
			GetMinimum(fT0,fF0,fTm,fFm,iLevel);
		}
		else if ( fF1 < fF0 )
		{
			// repeat on [tm,t1]
			GetMinimum(fTm,fFm,fT1,fF1,iLevel);
		}
		else
		{
			// repeat on [t0,tm] and [tm,t1]
			GetMinimum(fT0,fF0,fTm,fFm,iLevel);
			GetMinimum(fTm,fFm,fT1,fF1,iLevel);
		}
	}
}
//----------------------------------------------------------------------------
void*& Minimize1D::UserData ()
{
	return m_pvUserData;
}
//----------------------------------------------------------------------------
void Minimize1D::GetBracketedMinimum (Real fT0, Real fF0, Real fTm,
	Real fFm, Real fT1, Real fF1, int iLevel)
{
	for (int i = 0; i < m_iMaxBracket; i++)
	{
		// update minimum value
		if ( fFm < m_fFMin )
		{
			m_fTMin = fTm;
			m_fFMin = fFm;
		}

		// test for convergence
		const Real fEps = 1e-08f, fTol = 1e-04f;
		if ( Math::FAbs(fT1-fT0) <= 2.0f*fTol*Math::FAbs(fTm) + fEps )
			break;

		// compute vertex of interpolating parabola
		Real fDT0 = fT0 - fTm, fDT1 = fT1 - fTm;
		Real fDF0 = fF0 - fFm, fDF1 = fF1 - fFm;
		Real fTmp0 = fDT0*fDF1, fTmp1 = fDT1*fDF0;
		Real fDenom = fTmp1 - fTmp0;
		if ( Math::FAbs(fDenom) < fEps )
			return;

		Real fTv = fTm + 0.5f*(fDT1*fTmp1-fDT0*fTmp0)/fDenom;
		Assert( fT0 <= fTv && fTv <= fT1 );
		Real fFv = m_oF(fTv,m_pvUserData);

		if ( fTv < fTm )
		{
			if ( fFv < fFm )
			{
				fT1 = fTm;
				fF1 = fFm;
				fTm = fTv;
				fFm = fFv;
			}
			else
			{
				fT0 = fTv;
				fF0 = fFv;
			}
		}
		else if ( fTv > fTm )
		{
			if ( fFv < fFm )
			{
				fT0 = fTm;
				fF0 = fFm;
				fTm = fTv;
				fFm = fFv;
			}
			else
			{
				fT1 = fTv;
				fF1 = fFv;
			}
		}
		else
		{
			// vertex of parabola is already at middle sample point
			GetMinimum(fT0,fF0,fTm,fFm,iLevel);
			GetMinimum(fTm,fFm,fT1,fF1,iLevel);
		}
	}
}


const Vector3 Vector3::ZERO(0.0f,0.0f,0.0f);
const Vector3 Vector3::UNIT_X(1.0f,0.0f,0.0f);
const Vector3 Vector3::UNIT_Y(0.0f,1.0f,0.0f);
const Vector3 Vector3::UNIT_Z(0.0f,0.0f,1.0f);

Real Vector3::FUZZ = 0.0f;
//----------------------------------------------------------------------------
Vector3::Vector3 (Real fX, Real fY, Real fZ)
{
	x = fX;
	y = fY;
	z = fZ;
}
//----------------------------------------------------------------------------
Vector3::Vector3 (Real afCoordinate[3])
{
	x = afCoordinate[0];
	y = afCoordinate[1];
	z = afCoordinate[2];
}
//----------------------------------------------------------------------------
Vector3::Vector3 (const Vector3& rkVector)
{
	x = rkVector.x;
	y = rkVector.y;
	z = rkVector.z;
}
//----------------------------------------------------------------------------
Vector3& Vector3::operator= (const Vector3& rkVector)
{
	x = rkVector.x;
	y = rkVector.y;
	z = rkVector.z;
	return *this;
}
//----------------------------------------------------------------------------
bool Vector3::operator== (const Vector3& rkVector) const
{
	if ( FUZZ == 0.0f )
	{
		return x == rkVector.x && y == rkVector.y && z == rkVector.z;
	}
	else
	{
		return Math::FAbs(x - rkVector.x) <= FUZZ
			&& Math::FAbs(y - rkVector.y) <= FUZZ
			&& Math::FAbs(z - rkVector.z) <= FUZZ;
	}
}
//----------------------------------------------------------------------------
bool Vector3::operator!= (const Vector3& rkVector) const
{
	if ( FUZZ == 0.0f )
	{
		return x != rkVector.x || y != rkVector.y || z != rkVector.z;
	}
	else
	{
		return Math::FAbs(x - rkVector.x) > FUZZ
			|| Math::FAbs(y - rkVector.y) > FUZZ
			|| Math::FAbs(z - rkVector.z) > FUZZ;
	}
}
//----------------------------------------------------------------------------
bool Vector3::operator< (const Vector3& rkVector) const
{
	Real fXTmp = rkVector.x, fYTmp = rkVector.y, fZTmp = rkVector.z;
	if ( FUZZ > 0.0f )
	{
		if ( Math::FAbs(x - fXTmp) <= FUZZ )
			fXTmp = x;
		if ( Math::FAbs(y - fYTmp) <= FUZZ )
			fYTmp = y;
		if ( Math::FAbs(z - fZTmp) <= FUZZ )
			fZTmp = z;
	}

	// compare z values
	unsigned int uiTest0 = *(unsigned int*)&z;
	unsigned int uiTest1 = *(unsigned int*)&fZTmp;
	if ( uiTest0 < uiTest1 )
		return true;
	if ( uiTest0 > uiTest1 )
		return false;

	// compare y values
	uiTest0 = *(unsigned int*)&y;
	uiTest1 = *(unsigned int*)&fYTmp;
	if ( uiTest0 < uiTest1 )
		return true;
	if ( uiTest0 > uiTest1 )
		return false;

	// compare x values
	uiTest0 = *(unsigned int*)&x;
	uiTest1 = *(unsigned int*)&fXTmp;
	return uiTest0 < uiTest1;
}
//----------------------------------------------------------------------------
bool Vector3::operator<= (const Vector3& rkVector) const
{
	Real fXTmp = rkVector.x, fYTmp = rkVector.y, fZTmp = rkVector.z;
	if ( FUZZ > 0.0f )
	{
		if ( Math::FAbs(x - fXTmp) <= FUZZ )
			fXTmp = x;
		if ( Math::FAbs(y - fYTmp) <= FUZZ )
			fYTmp = y;
		if ( Math::FAbs(z - fZTmp) <= FUZZ )
			fZTmp = z;
	}

	// compare z values
	unsigned int uiTest0 = *(unsigned int*)&z;
	unsigned int uiTest1 = *(unsigned int*)&fZTmp;
	if ( uiTest0 < uiTest1 )
		return true;
	if ( uiTest0 > uiTest1 )
		return false;

	// compare y values
	uiTest0 = *(unsigned int*)&y;
	uiTest1 = *(unsigned int*)&fYTmp;
	if ( uiTest0 < uiTest1 )
		return true;
	if ( uiTest0 > uiTest1 )
		return false;

	// compare x values
	uiTest0 = *(unsigned int*)&x;
	uiTest1 = *(unsigned int*)&fXTmp;
	return uiTest0 <= uiTest1;
}
//----------------------------------------------------------------------------
bool Vector3::operator> (const Vector3& rkVector) const
{
	Real fXTmp = rkVector.x, fYTmp = rkVector.y, fZTmp = rkVector.z;
	if ( FUZZ > 0.0f )
	{
		if ( Math::FAbs(x - fXTmp) <= FUZZ )
			fXTmp = x;
		if ( Math::FAbs(y - fYTmp) <= FUZZ )
			fYTmp = y;
		if ( Math::FAbs(z - fZTmp) <= FUZZ )
			fZTmp = z;
	}

	// compare z values
	unsigned int uiTest0 = *(unsigned int*)&z;
	unsigned int uiTest1 = *(unsigned int*)&fZTmp;
	if ( uiTest0 > uiTest1 )
		return true;
	if ( uiTest0 < uiTest1 )
		return false;

	// compare y values
	uiTest0 = *(unsigned int*)&y;
	uiTest1 = *(unsigned int*)&fYTmp;
	if ( uiTest0 > uiTest1 )
		return true;
	if ( uiTest0 < uiTest1 )
		return false;

	// compare x values
	uiTest0 = *(unsigned int*)&x;
	uiTest1 = *(unsigned int*)&fXTmp;
	return uiTest0 > uiTest1;
}
//----------------------------------------------------------------------------
bool Vector3::operator>= (const Vector3& rkVector) const
{
	Real fXTmp = rkVector.x, fYTmp = rkVector.y, fZTmp = rkVector.z;
	if ( FUZZ > 0.0f )
	{
		if ( Math::FAbs(x - fXTmp) <= FUZZ )
			fXTmp = x;
		if ( Math::FAbs(y - fYTmp) <= FUZZ )
			fYTmp = y;
		if ( Math::FAbs(z - fZTmp) <= FUZZ )
			fZTmp = z;
	}

	// compare z values
	unsigned int uiTest0 = *(unsigned int*)&z;
	unsigned int uiTest1 = *(unsigned int*)&fZTmp;
	if ( uiTest0 > uiTest1 )
		return true;
	if ( uiTest0 < uiTest1 )
		return false;

	// compare y values
	uiTest0 = *(unsigned int*)&y;
	uiTest1 = *(unsigned int*)&fYTmp;
	if ( uiTest0 > uiTest1 )
		return true;
	if ( uiTest0 < uiTest1 )
		return false;

	// compare x values
	uiTest0 = *(unsigned int*)&x;
	uiTest1 = *(unsigned int*)&fXTmp;
	return uiTest0 >= uiTest1;
}
//----------------------------------------------------------------------------
Vector3 Vector3::operator+ (const Vector3& rkVector) const
{
	return Vector3(x+rkVector.x,y+rkVector.y,z+rkVector.z);
}
//----------------------------------------------------------------------------
Vector3 Vector3::operator- (const Vector3& rkVector) const
{
	return Vector3(x-rkVector.x,y-rkVector.y,z-rkVector.z);
}
//----------------------------------------------------------------------------
Vector3 Vector3::operator* (Real fScalar) const
{
	return Vector3(fScalar*x,fScalar*y,fScalar*z);
}
//----------------------------------------------------------------------------
Vector3 Vector3::operator/ (Real fScalar) const
{
	Vector3 kQuot;

	if ( fScalar != 0.0f )
	{
		Real fInvScalar = 1.0f/fScalar;
		kQuot.x = fInvScalar*x;
		kQuot.y = fInvScalar*y;
		kQuot.z = fInvScalar*z;
		return kQuot;
	}
	else
	{
		return Vector3(Math::MAX_REAL,Math::MAX_REAL,Math::MAX_REAL);
	}
}
//----------------------------------------------------------------------------
Vector3 Vector3::operator- () const
{
	return Vector3(-x,-y,-z);
}
//----------------------------------------------------------------------------
Vector3 Mgc::operator* (Real fScalar, const Vector3& rkVector)
{
	return Vector3(fScalar*rkVector.x,fScalar*rkVector.y,
		fScalar*rkVector.z);
}
//----------------------------------------------------------------------------
Vector3& Vector3::operator+= (const Vector3& rkVector)
{
	x += rkVector.x;
	y += rkVector.y;
	z += rkVector.z;
	return *this;
}
//----------------------------------------------------------------------------
Vector3& Vector3::operator-= (const Vector3& rkVector)
{
	x -= rkVector.x;
	y -= rkVector.y;
	z -= rkVector.z;
	return *this;
}
//----------------------------------------------------------------------------
Vector3& Vector3::operator*= (Real fScalar)
{
	x *= fScalar;
	y *= fScalar;
	z *= fScalar;
	return *this;
}
//----------------------------------------------------------------------------
Vector3& Vector3::operator/= (Real fScalar)
{
	if ( fScalar != 0.0f )
	{
		Real fInvScalar = 1.0f/fScalar;
		x *= fInvScalar;
		y *= fInvScalar;
		z *= fInvScalar;
	}
	else
	{
		x = Math::MAX_REAL;
		y = Math::MAX_REAL;
		z = Math::MAX_REAL;
	}

	return *this;
}
//----------------------------------------------------------------------------
Real Vector3::SquaredLength () const
{
	return x*x + y*y + z*z;
}
//----------------------------------------------------------------------------
Real Vector3::Length () const
{
	return Math::Sqrt(x*x + y*y + z*z);
}
//----------------------------------------------------------------------------
Real Vector3::Dot (const Vector3& rkVector) const
{
	return x*rkVector.x + y*rkVector.y + z*rkVector.z;
}
//----------------------------------------------------------------------------
Vector3 Vector3::Cross (const Vector3& rkVector) const
{
	return Vector3(y*rkVector.z-z*rkVector.y,z*rkVector.x-x*rkVector.z,
		x*rkVector.y-y*rkVector.x);
}
//----------------------------------------------------------------------------
Vector3 Vector3::UnitCross (const Vector3& rkVector) const
{
	Vector3 kCross(y*rkVector.z-z*rkVector.y,z*rkVector.x-x*rkVector.z,
		x*rkVector.y-y*rkVector.x);
	kCross.Unitize();
	return kCross;
}
//----------------------------------------------------------------------------
Real Vector3::Unitize (Real fTolerance)
{
	Real fLength = Length();

	if ( fLength > fTolerance )
	{
		Real fInvLength = 1.0f/fLength;
		x *= fInvLength;
		y *= fInvLength;
		z *= fInvLength;
	}
	else
	{
		fLength = 0.0f;
	}

	return fLength;
}
//----------------------------------------------------------------------------
void Vector3::Orthonormalize (Vector3 akVector[/*3*/])
{
	// If the input vectors are v0, v1, and v2, then the Gram-Schmidt
	// orthonormalization produces vectors u0, u1, and u2 as follows,
	//
	//   u0 = v0/|v0|
	//   u1 = (v1-(u0*v1)u0)/|v1-(u0*v1)u0|
	//   u2 = (v2-(u0*v2)u0-(u1*v2)u1)/|v2-(u0*v2)u0-(u1*v2)u1|
	//
	// where |A| indicates length of vector A and A*B indicates dot
	// product of vectors A and B.

	// compute u0
	akVector[0].Unitize();

	// compute u1
	Real fDot0 = akVector[0].Dot(akVector[1]); 
	akVector[1] -= fDot0*akVector[0];
	akVector[1].Unitize();

	// compute u2
	Real fDot1 = akVector[1].Dot(akVector[2]);
	fDot0 = akVector[0].Dot(akVector[2]);
	akVector[2] -= fDot0*akVector[0] + fDot1*akVector[1];
	akVector[2].Unitize();
}
//----------------------------------------------------------------------------
void Vector3::GenerateOrthonormalBasis (Vector3& rkU, Vector3& rkV,
	Vector3& rkW, bool bUnitLengthW)
{
	if ( !bUnitLengthW )
		rkW.Unitize();

	Real fInvLength;

	if ( Math::FAbs(rkW.x) >= Math::FAbs(rkW.y) )
	{
		// W.x or W.z is the largest magnitude component, swap them
		fInvLength = Math::InvSqrt(rkW.x*rkW.x+rkW.z*rkW.z);
		rkU.x = -rkW.z*fInvLength;
		rkU.y = 0.0f;
		rkU.z = +rkW.x*fInvLength;
	}
	else
	{
		// W.y or W.z is the largest magnitude component, swap them
		fInvLength = Math::InvSqrt(rkW.y*rkW.y+rkW.z*rkW.z);
		rkU.x = 0.0f;
		rkU.y = +rkW.z*fInvLength;
		rkU.z = -rkW.y*fInvLength;
	}

	rkV = rkW.Cross(rkU);
}
//----------------------------------------------------------------------------
Vector3::Vector3 ()
{
	// For efficiency in construction of large arrays of vectors, the
	// default constructor does not initialize the vector.
}

//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
void Matrix3::FromAxisAngle (const Vector3& rkAxis, Real fRadians)
{
	Real fCos = Math::Cos(fRadians);
	Real fSin = Math::Sin(fRadians);
	Real fOneMinusCos = 1.0f-fCos;
	Real fX2 = rkAxis.x*rkAxis.x;
	Real fY2 = rkAxis.y*rkAxis.y;
	Real fZ2 = rkAxis.z*rkAxis.z;
	Real fXYM = rkAxis.x*rkAxis.y*fOneMinusCos;
	Real fXZM = rkAxis.x*rkAxis.z*fOneMinusCos;
	Real fYZM = rkAxis.y*rkAxis.z*fOneMinusCos;
	Real fXSin = rkAxis.x*fSin;
	Real fYSin = rkAxis.y*fSin;
	Real fZSin = rkAxis.z*fSin;

	m_aafEntry[0][0] = fX2*fOneMinusCos+fCos;
	m_aafEntry[0][1] = fXYM-fZSin;
	m_aafEntry[0][2] = fXZM+fYSin;
	m_aafEntry[1][0] = fXYM+fZSin;
	m_aafEntry[1][1] = fY2*fOneMinusCos+fCos;
	m_aafEntry[1][2] = fYZM-fXSin;
	m_aafEntry[2][0] = fXZM-fYSin;
	m_aafEntry[2][1] = fYZM+fXSin;
	m_aafEntry[2][2] = fZ2*fOneMinusCos+fCos;
}	//----------------------------------------------------------------------------
Vector3 Matrix3::GetColumn (int iCol) const
{
	Assert( 0 <= iCol && iCol < 3 );
	return Vector3(m_aafEntry[0][iCol],m_aafEntry[1][iCol],
		m_aafEntry[2][iCol]);
}
//----------------------------------------------------------------------------
Vector3 Matrix3::operator* (const Vector3& rkPoint) const
{
	Vector3 kProd;
	for (int iRow = 0; iRow < 3; iRow++)
	{
		kProd[iRow] =
			m_aafEntry[iRow][0]*rkPoint[0] +
			m_aafEntry[iRow][1]*rkPoint[1] +
			m_aafEntry[iRow][2]*rkPoint[2];
	}
	return kProd;
}
//----------------------------------------------------------------------------
Vector3 Mgc::operator* (const Vector3& rkPoint, const Matrix3& rkMatrix)
{
	Vector3 kProd;
	for (int iRow = 0; iRow < 3; iRow++)
	{
		kProd[iRow] =
			rkPoint[0]*rkMatrix.m_aafEntry[0][iRow] +
			rkPoint[1]*rkMatrix.m_aafEntry[1][iRow] +
			rkPoint[2]*rkMatrix.m_aafEntry[2][iRow];
	}
	return kProd;
}
//----------------------------------------------------------------------------

static Real Volume (const Real* afAngle, void* pvUserData)
{
	int iQuantity = ((PointArray*)pvUserData)->m_iQuantity;
	const Vector3* akPoint = ((PointArray*)pvUserData)->m_akPoint;

	Real fCos0 = Math::Cos(afAngle[0]);
	Real fSin0 = Math::Sin(afAngle[0]);
	Real fCos1 = Math::Cos(afAngle[1]);
	Real fSin1 = Math::Sin(afAngle[1]);
	Vector3 kAxis(fCos0*fSin1,fSin0*fSin1,fCos1);
	Matrix3 kRot;
	kRot.FromAxisAngle(kAxis,afAngle[2]);

	Vector3 kMin = akPoint[0]*kRot, kMax = kMin;
	for (int i = 1; i < iQuantity; i++)
	{
		Vector3 kTest = akPoint[i]*kRot;

		if ( kTest.x < kMin.x )
			kMin.x = kTest.x;
		else if ( kTest.x > kMax.x )
			kMax.x = kTest.x;

		if ( kTest.y < kMin.y )
			kMin.y = kTest.y;
		else if ( kTest.y > kMax.y )
			kMax.y = kTest.y;

		if ( kTest.z < kMin.z )
			kMin.z = kTest.z;
		else if ( kTest.z > kMax.z )
			kMax.z = kTest.z;
	}

	Real fVolume = (kMax.x-kMin.x)*(kMax.y-kMin.y)*(kMax.z-kMin.z);
	return fVolume;
}

static void MinimalBoxForAngles (int iQuantity, const Vector3* akPoint,
	Real afAngle[3], Box3& rkBox)
{
	Real fCos0 = Math::Cos(afAngle[0]);
	Real fSin0 = Math::Sin(afAngle[0]);
	Real fCos1 = Math::Cos(afAngle[1]);
	Real fSin1 = Math::Sin(afAngle[1]);
	Vector3 kAxis(fCos0*fSin1,fSin0*fSin1,fCos1);
	Matrix3 kRot;
	kRot.FromAxisAngle(kAxis,afAngle[2]);

	Vector3 kMin = akPoint[0]*kRot, kMax = kMin;
	for (int i = 1; i < iQuantity; i++)
	{
		Vector3 kTest = akPoint[i]*kRot;

		if ( kTest.x < kMin.x )
			kMin.x = kTest.x;
		else if ( kTest.x > kMax.x )
			kMax.x = kTest.x;

		if ( kTest.y < kMin.y )
			kMin.y = kTest.y;
		else if ( kTest.y > kMax.y )
			kMax.y = kTest.y;

		if ( kTest.z < kMin.z )
			kMin.z = kTest.z;
		else if ( kTest.z > kMax.z )
			kMax.z = kTest.z;
	}

	Vector3 kMid = 0.5f*(kMax + kMin);
	Vector3 kRng = 0.5f*(kMax - kMin);

	rkBox.Center() = kRot*kMid;
	rkBox.Axis(0) = kRot.GetColumn(0);
	rkBox.Axis(1) = kRot.GetColumn(1);
	rkBox.Axis(2) = kRot.GetColumn(2);
	rkBox.Extent(0) = kRng.x;
	rkBox.Extent(1) = kRng.y;
	rkBox.Extent(2) = kRng.z;
}

Box3 Mgc::MinBox (int iQuantity, const Vector3* akPoint)
{
	int iMaxLevel = 8;
	int iMaxBracket = 8;
	int iMaxIterations = 32;
	PointArray kPA(iQuantity,akPoint);
	MinimizeND kMinimizer(3,Volume,iMaxLevel,iMaxBracket,iMaxIterations,&kPA);

	Real afA0[3] =
	{
		0.0f,
			0.0f,
			0.0f
	};

	Real afA1[3] =
	{
		Math::PI_,
			Math::HALF_PI,
			Math::PI_
	};

	// compute some samples to narrow down the search region
	Real fMinVolume = Math::MAX_REAL;
	Real afAngle[3], afAInitial[3];
	const int iMax = 3;
	for (int i0 = 0; i0 <= iMax; i0++)
	{
		afAngle[0] = afA0[0] + i0*(afA1[0] - afA0[0])/iMax;
		for (int i1 = 0; i1 <= iMax; i1++)
		{
			afAngle[1] = afA0[1] + i1*(afA1[1] - afA0[1])/iMax;
			for (int i2 = 0; i2 <= iMax; i2++)
			{
				afAngle[2] = afA0[2] + i2*(afA1[2] - afA0[2])/iMax;
				Real fVolume = Volume(afAngle,&kPA);
				if ( fVolume < fMinVolume )
				{
					fMinVolume = fVolume;
					afAInitial[0] = afAngle[0];
					afAInitial[1] = afAngle[1];
					afAInitial[2] = afAngle[2];
				}
			}
		}
	}

	Real afAMin[3], fVMin;
	kMinimizer.GetMinimum(afA0,afA1,afAInitial,afAMin,fVMin);

	Box3 kBox;
	MinimalBoxForAngles(iQuantity,akPoint,afAMin,kBox);
	return kBox;
}


//----------------------------------------------------------------------------
static bool PointInsideSphere (const Vector3& rkP, const Sphere& rkS)
{
	Vector3 kDiff = rkP - rkS.Center();
	Real fTest = kDiff.SquaredLength();
	return fTest <= gs_fOnePlusEpsilon*rkS.Radius();  // theory:  test <= R^2
}
//----------------------------------------------------------------------------

static Sphere ExactSphere2 (const Vector3& rkP0, const Vector3& rkP1)
{
	Sphere kMinimal;
	kMinimal.Center() = 0.5f*(rkP0+rkP1);
	Vector3 kDiff = rkP1 - rkP0;
	kMinimal.Radius() = 0.25f*kDiff.SquaredLength();  // radius gets square-rooted later
	return kMinimal;
}
//----------------------------------------------------------------------------

static Sphere DegenerateTrisSafeSphere3( const Vector3& rkP0, const Vector3& rkP1, const Vector3& rkP2 );
static Sphere DegenerateTrisSafeSphere4( const Vector3& rkP0, const Vector3& rkP1, const Vector3& rkP2, const Vector3& rkP3 );

static Sphere ExactSphere3 (const Vector3& rkP0, const Vector3& rkP1,
	const Vector3& rkP2)
{
	// Compute the circle (in 3D) containing p0, p1, and p2.  The Center() in
	// barycentric coordinates is K = u0*p0+u1*p1+u2*p2 where u0+u1+u2=1.
	// The Center() is equidistant from the three points, so |K-p0| = |K-p1| =
	// |K-p2| = R where R is the radius of the circle.
	//
	// From these conditions,
	//   K-p0 = u0*A + u1*B - A
	//   K-p1 = u0*A + u1*B - B
	//   K-p2 = u0*A + u1*B
	// where A = p0-p2 and B = p1-p2, which leads to
	//   r^2 = |u0*A+u1*B|^2 - 2*Dot(A,u0*A+u1*B) + |A|^2
	//   r^2 = |u0*A+u1*B|^2 - 2*Dot(B,u0*A+u1*B) + |B|^2
	//   r^2 = |u0*A+u1*B|^2
	// Subtracting the last equation from the first two and writing
	// the equations as a linear system,
	//
	// +-                 -++   -+       +-        -+
	// | Dot(A,A) Dot(A,B) || u0 | = 0.5 | Dot(A,A) |
	// | Dot(B,A) Dot(B,B) || u1 |       | Dot(B,B) |
	// +-                 -++   -+       +-        -+
	//
	// The following code solves this system for u0 and u1, then
	// evaluates the third equation in r^2 to obtain r.

	Vector3 kA = rkP0 - rkP2;
	Vector3 kB = rkP1 - rkP2;
	Real fAdA = kA.Dot(kA);
	Real fAdB = kA.Dot(kB);
	Real fBdB = kB.Dot(kB);
	Real fDet = fAdA*fBdB-fAdB*fAdB;

	Sphere kMinimal = DegenerateTrisSafeSphere3( rkP0, rkP1, rkP2 );

	if ( Math::FAbs(fDet) > gs_fEpsilon )
	{
		Sphere kComputed;

		Real fHalfInvDet = 0.5f/fDet;
		Real fU0 = fHalfInvDet*fBdB*(fAdA-fAdB);
		Real fU1 = fHalfInvDet*fAdA*(fBdB-fAdB);
		Real fU2 = 1.0f-fU0-fU1;
		kComputed.Center() = fU0*rkP0 + fU1*rkP1 + fU2*rkP2;
		Vector3 kTmp = fU0*kA + fU1*kB;
		kComputed.Radius() = kTmp.SquaredLength();

		if( kComputed.Radius() < kMinimal.Radius() )
			return kComputed;
	}

	return kMinimal;
}
//----------------------------------------------------------------------------
static Sphere ExactSphere4 (const Vector3& rkP0, const Vector3& rkP1,
	const Vector3& rkP2, const Vector3& rkP3)
{
	// Compute the sphere containing p0, p1, p2, and p3.  The Center() in
	// barycentric coordinates is K = u0*p0+u1*p1+u2*p2+u3*p3 where
	// u0+u1+u2+u3=1.  The Center() is equidistant from the three points, so
	// |K-p0| = |K-p1| = |K-p2| = |K-p3| = R where R is the radius of the
	// sphere.
	//
	// From these conditions,
	//   K-p0 = u0*A + u1*B + u2*C - A
	//   K-p1 = u0*A + u1*B + u2*C - B
	//   K-p2 = u0*A + u1*B + u2*C - C
	//   K-p3 = u0*A + u1*B + u2*C
	// where A = p0-p3, B = p1-p3, and C = p2-p3 which leads to
	//   r^2 = |u0*A+u1*B+u2*C|^2 - 2*Dot(A,u0*A+u1*B+u2*C) + |A|^2
	//   r^2 = |u0*A+u1*B+u2*C|^2 - 2*Dot(B,u0*A+u1*B+u2*C) + |B|^2
	//   r^2 = |u0*A+u1*B+u2*C|^2 - 2*Dot(C,u0*A+u1*B+u2*C) + |C|^2
	//   r^2 = |u0*A+u1*B+u2*C|^2
	// Subtracting the last equation from the first three and writing
	// the equations as a linear system,
	//
	// +-                          -++   -+       +-        -+
	// | Dot(A,A) Dot(A,B) Dot(A,C) || u0 | = 0.5 | Dot(A,A) |
	// | Dot(B,A) Dot(B,B) Dot(B,C) || u1 |       | Dot(B,B) |
	// | Dot(C,A) Dot(C,B) Dot(C,C) || u2 |       | Dot(C,C) |
	// +-                          -++   -+       +-        -+
	//
	// The following code solves this system for u0, u1, and u2, then
	// evaluates the fourth equation in r^2 to obtain r.

	Vector3 kE10 = rkP0 - rkP3;
	Vector3 kE20 = rkP1 - rkP3;
	Vector3 kE30 = rkP2 - rkP3;

	Real aafA[3][3];
	aafA[0][0] = kE10.Dot(kE10);
	aafA[0][1] = kE10.Dot(kE20);
	aafA[0][2] = kE10.Dot(kE30);
	aafA[1][0] = aafA[0][1];
	aafA[1][1] = kE20.Dot(kE20);
	aafA[1][2] = kE20.Dot(kE30);
	aafA[2][0] = aafA[0][2];
	aafA[2][1] = aafA[1][2];
	aafA[2][2] = kE30.Dot(kE30);

	Real afB[3];
	afB[0] = 0.5f*aafA[0][0];
	afB[1] = 0.5f*aafA[1][1];
	afB[2] = 0.5f*aafA[2][2];

	Real aafAInv[3][3];
	aafAInv[0][0] = aafA[1][1]*aafA[2][2]-aafA[1][2]*aafA[2][1];
	aafAInv[0][1] = aafA[0][2]*aafA[2][1]-aafA[0][1]*aafA[2][2];
	aafAInv[0][2] = aafA[0][1]*aafA[1][2]-aafA[0][2]*aafA[1][1];
	aafAInv[1][0] = aafA[1][2]*aafA[2][0]-aafA[1][0]*aafA[2][2];
	aafAInv[1][1] = aafA[0][0]*aafA[2][2]-aafA[0][2]*aafA[2][0];
	aafAInv[1][2] = aafA[0][2]*aafA[1][0]-aafA[0][0]*aafA[1][2];
	aafAInv[2][0] = aafA[1][0]*aafA[2][1]-aafA[1][1]*aafA[2][0];
	aafAInv[2][1] = aafA[0][1]*aafA[2][0]-aafA[0][0]*aafA[2][1];
	aafAInv[2][2] = aafA[0][0]*aafA[1][1]-aafA[0][1]*aafA[1][0];
	Real fDet = aafA[0][0]*aafAInv[0][0] + aafA[0][1]*aafAInv[1][0] +
		aafA[0][2]*aafAInv[2][0];

	Sphere kMinimal = DegenerateTrisSafeSphere4( rkP0, rkP1, rkP2, rkP3 );

	if ( Math::FAbs(fDet) > gs_fEpsilon )
	{
		Real fInvDet = 1.0f/fDet;
		int iRow, iCol;
		for (iRow = 0; iRow < 3; iRow++)
		{
			for (iCol = 0; iCol < 3; iCol++)
				aafAInv[iRow][iCol] *= fInvDet;
		}

		Real afU[4];
		for (iRow = 0; iRow < 3; iRow++)
		{
			afU[iRow] = 0.0f;
			for (iCol = 0; iCol < 3; iCol++)
				afU[iRow] += aafAInv[iRow][iCol]*afB[iCol];
		}
		afU[3] = 1.0f - afU[0] - afU[1] - afU[2];

		Sphere kComputed;
		kComputed.Center() = afU[0]*rkP0 + afU[1]*rkP1 + afU[2]*rkP2 +
			afU[3]*rkP3;
		Vector3 kTmp = afU[0]*kE10 + afU[1]*kE20 + afU[2]*kE30;
		kComputed.Radius() = kTmp.SquaredLength();

		if( kComputed.Radius() < kMinimal.Radius() )
			return kComputed;
	}

	return kMinimal;
}

//----------------------------------------------------------------------------
static Sphere UpdateSupport1 (int i, Vector3** apkPerm, Support& rkSupp)
{
	const Vector3& rkP0 = *apkPerm[rkSupp.m_aiIndex[0]];
	const Vector3& rkP1 = *apkPerm[i];

	Sphere kMinimal = ExactSphere2(rkP0,rkP1);
	rkSupp.m_iQuantity = 2;
	rkSupp.m_aiIndex[1] = i;

	return kMinimal;
}
//----------------------------------------------------------------------------
static Sphere UpdateSupport2 (int i, Vector3** apkPerm, Support& rkSupp)
{
	const Vector3& rkP0 = *apkPerm[rkSupp.m_aiIndex[0]];
	const Vector3& rkP1 = *apkPerm[rkSupp.m_aiIndex[1]];
	const Vector3& rkP2 = *apkPerm[i];

	Sphere akS[3];
	Real fMinRSqr = Math::MAX_REAL;
	int iIndex = -1;

	akS[0] = ExactSphere2(rkP0,rkP2);
	if ( PointInsideSphere(rkP1,akS[0]) )
	{
		fMinRSqr = akS[0].Radius();
		iIndex = 0;
	}

	akS[1] = ExactSphere2(rkP1,rkP2);
	if ( akS[1].Radius() < fMinRSqr )
	{
		if ( PointInsideSphere(rkP0,akS[1]) )
		{
			fMinRSqr = akS[1].Radius();
			iIndex = 1;
		}
	}

	Sphere kMinimal;

	if ( iIndex != -1 )
	{
		kMinimal = akS[iIndex];
		rkSupp.m_aiIndex[1-iIndex] = i;
	}
	else
	{
		kMinimal = ExactSphere3(rkP0,rkP1,rkP2);
		rkSupp.m_iQuantity = 3;
		rkSupp.m_aiIndex[2] = i;
	}

	return kMinimal;
}
//----------------------------------------------------------------------------
static Sphere UpdateSupport3 (int i, Vector3** apkPerm, Support& rkSupp)
{
	const Vector3& rkP0 = *apkPerm[rkSupp.m_aiIndex[0]];
	const Vector3& rkP1 = *apkPerm[rkSupp.m_aiIndex[1]];
	const Vector3& rkP2 = *apkPerm[rkSupp.m_aiIndex[2]];
	const Vector3& rkP3 = *apkPerm[i];

	Sphere akS[6];
	Real fMinRSqr = Math::MAX_REAL;
	int iIndex = -1;

	akS[0] = ExactSphere2(rkP0,rkP3);
	if ( PointInsideSphere(rkP1,akS[0]) && PointInsideSphere(rkP2,akS[0]) )
	{
		fMinRSqr = akS[0].Radius();
		iIndex = 0;
	}

	akS[1] = ExactSphere2(rkP1,rkP3);
	if ( akS[1].Radius() < fMinRSqr
		&& PointInsideSphere(rkP0,akS[1]) && PointInsideSphere(rkP2,akS[1]) )
	{
		fMinRSqr = akS[1].Radius();
		iIndex = 1;
	}

	akS[2] = ExactSphere2(rkP2,rkP3);
	if ( akS[2].Radius() < fMinRSqr
		&& PointInsideSphere(rkP0,akS[2]) && PointInsideSphere(rkP1,akS[2]) )
	{
		fMinRSqr = akS[2].Radius();
		iIndex = 2;
	}

	akS[3] = ExactSphere3(rkP0,rkP1,rkP3);
	if ( akS[3].Radius() < fMinRSqr && PointInsideSphere(rkP2,akS[3]) )
	{
		fMinRSqr = akS[3].Radius();
		iIndex = 3;
	}

	akS[4] = ExactSphere3(rkP0,rkP2,rkP3);
	if ( akS[4].Radius() < fMinRSqr && PointInsideSphere(rkP1,akS[4]) )
	{
		fMinRSqr = akS[4].Radius();
		iIndex = 4;
	}

	akS[5] = ExactSphere3(rkP1,rkP2,rkP3);
	if ( akS[5].Radius() < fMinRSqr && PointInsideSphere(rkP0,akS[5]) )
	{
		fMinRSqr = akS[5].Radius();
		iIndex = 5;
	}

	Sphere kMinimal;

	switch ( iIndex )
	{
	case 0:
		kMinimal = akS[0];
		rkSupp.m_iQuantity = 2;
		rkSupp.m_aiIndex[1] = i;
		break;
	case 1:
		kMinimal = akS[1];
		rkSupp.m_iQuantity = 2;
		rkSupp.m_aiIndex[0] = i;
		break;
	case 2:
		kMinimal = akS[2];
		rkSupp.m_iQuantity = 2;
		rkSupp.m_aiIndex[0] = rkSupp.m_aiIndex[2];
		rkSupp.m_aiIndex[1] = i;
		break;
	case 3:
		kMinimal = akS[3];
		rkSupp.m_aiIndex[2] = i;
		break;
	case 4:
		kMinimal = akS[4];
		rkSupp.m_aiIndex[1] = i;
		break;
	case 5:
		kMinimal = akS[5];
		rkSupp.m_aiIndex[0] = i;
		break;
	default:
		kMinimal = ExactSphere4(rkP0,rkP1,rkP2,rkP3);
		Assert( kMinimal.Radius() <= fMinRSqr );
		rkSupp.m_iQuantity = 4;
		rkSupp.m_aiIndex[3] = i;
		break;
	}

	return kMinimal;
}

//----------------------------------------------------------------------------
static Sphere UpdateSupport4 (int i, Vector3** apkPerm, Support& rkSupp)
{
	const Vector3& rkP0 = *apkPerm[rkSupp.m_aiIndex[0]];
	const Vector3& rkP1 = *apkPerm[rkSupp.m_aiIndex[1]];
	const Vector3& rkP2 = *apkPerm[rkSupp.m_aiIndex[2]];
	const Vector3& rkP3 = *apkPerm[rkSupp.m_aiIndex[3]];
	const Vector3& rkP4 = *apkPerm[i];

	Sphere akS[14];
	Real fMinRSqr = Math::MAX_REAL;
	Real fMaxRSqr = 0;
	int iIndex = -1, iMaxRadiusIndex = -1;

	akS[0] = ExactSphere2(rkP0,rkP4);
	if( akS[0].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[0].Radius();
		iMaxRadiusIndex = 0;
	}

	if ( PointInsideSphere(rkP1,akS[0])
		&&   PointInsideSphere(rkP2,akS[0])
		&&   PointInsideSphere(rkP3,akS[0]) )
	{
		fMinRSqr = akS[0].Radius();
		iIndex = 0;
	}

	akS[1] = ExactSphere2(rkP1,rkP4);
	if( akS[1].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[1].Radius();
		iMaxRadiusIndex = 1;
	}

	if ( akS[1].Radius() < fMinRSqr && PointInsideSphere(rkP0,akS[1])
		&& PointInsideSphere(rkP2,akS[1]) && PointInsideSphere(rkP3,akS[1]) )
	{
		fMinRSqr = akS[1].Radius();
		iIndex = 1;
	}

	akS[2] = ExactSphere2(rkP2,rkP4);
	if( akS[2].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[2].Radius();
		iMaxRadiusIndex = 2;
	}

	if ( akS[2].Radius() < fMinRSqr && PointInsideSphere(rkP0,akS[2])
		&& PointInsideSphere(rkP1,akS[2]) && PointInsideSphere(rkP3,akS[2]) )
	{
		fMinRSqr = akS[2].Radius();
		iIndex = 2;
	}

	akS[3] = ExactSphere2(rkP3,rkP4);
	if( akS[3].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[3].Radius();
		iMaxRadiusIndex = 3;
	}

	if ( akS[3].Radius() < fMinRSqr && PointInsideSphere(rkP0,akS[3])
		&& PointInsideSphere(rkP1,akS[3]) && PointInsideSphere(rkP2,akS[3]) )
	{
		fMinRSqr = akS[3].Radius();
		iIndex = 3;
	}

	akS[4] = ExactSphere3(rkP0,rkP1,rkP4);
	if( akS[4].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[4].Radius();
		iMaxRadiusIndex = 4;
	}

	if ( akS[4].Radius() < fMinRSqr && PointInsideSphere(rkP2,akS[4])
		&& PointInsideSphere(rkP3,akS[4]) )
	{
		fMinRSqr = akS[4].Radius();
		iIndex = 4;
	}

	akS[5] = ExactSphere3(rkP0,rkP2,rkP4);
	if( akS[5].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[5].Radius();
		iMaxRadiusIndex = 5;
	}

	if ( akS[5].Radius() < fMinRSqr && PointInsideSphere(rkP1,akS[5])
		&& PointInsideSphere(rkP3,akS[5]) )
	{
		fMinRSqr = akS[5].Radius();
		iIndex = 5;
	}

	akS[6] = ExactSphere3(rkP0,rkP3,rkP4);
	if( akS[6].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[6].Radius();
		iMaxRadiusIndex = 6;
	}

	if ( akS[6].Radius() < fMinRSqr && PointInsideSphere(rkP1,akS[6])
		&& PointInsideSphere(rkP2,akS[6]) )
	{
		fMinRSqr = akS[6].Radius();
		iIndex = 6;
	}

	akS[7] = ExactSphere3(rkP1,rkP2,rkP4);
	if( akS[7].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[7].Radius();
		iMaxRadiusIndex = 7;
	}

	if ( akS[7].Radius() < fMinRSqr && PointInsideSphere(rkP0,akS[7])
		&& PointInsideSphere(rkP3,akS[7]) )
	{
		fMinRSqr = akS[7].Radius();
		iIndex = 7;
	}

	akS[8] = ExactSphere3(rkP1,rkP3,rkP4);
	if( akS[8].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[8].Radius();
		iMaxRadiusIndex = 8;
	}

	if ( akS[8].Radius() < fMinRSqr && PointInsideSphere(rkP0,akS[8])
		&& PointInsideSphere(rkP2,akS[8]) )
	{
		fMinRSqr = akS[8].Radius();
		iIndex = 8;
	}

	akS[9] = ExactSphere3(rkP2,rkP3,rkP4);
	if( akS[9].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[9].Radius();
		iMaxRadiusIndex = 9;
	}

	if ( akS[9].Radius() < fMinRSqr && PointInsideSphere(rkP0,akS[9])
		&& PointInsideSphere(rkP1,akS[9]) )
	{
		fMinRSqr = akS[9].Radius();
		iIndex = 9;
	}

	akS[10] = ExactSphere4(rkP0,rkP1,rkP2,rkP4);
	if( akS[10].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[10].Radius();
		iMaxRadiusIndex = 10;
	}

	if ( akS[10].Radius() < fMinRSqr && PointInsideSphere(rkP3,akS[10]) )
	{
		fMinRSqr = akS[10].Radius();
		iIndex = 10;
	}

	akS[11] = ExactSphere4(rkP0,rkP1,rkP3,rkP4);
	if( akS[11].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[11].Radius();
		iMaxRadiusIndex = 11;
	}

	if ( akS[11].Radius() < fMinRSqr && PointInsideSphere(rkP2,akS[11]) )
	{
		fMinRSqr = akS[11].Radius();
		iIndex = 11;
	}

	akS[12] = ExactSphere4(rkP0,rkP2,rkP3,rkP4);
	if( akS[12].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[12].Radius();
		iMaxRadiusIndex = 12;
	}

	if ( akS[12].Radius() < fMinRSqr && PointInsideSphere(rkP1,akS[12]) )
	{
		fMinRSqr = akS[12].Radius();
		iIndex = 12;
	}

	akS[13] = ExactSphere4(rkP1,rkP2,rkP3,rkP4);
	if( akS[13].Radius() > fMaxRSqr )
	{
		fMaxRSqr = akS[13].Radius();
		iMaxRadiusIndex = 13;
	}

	if ( akS[13].Radius() < fMinRSqr && PointInsideSphere(rkP0,akS[13]) )
	{
		fMinRSqr = akS[13].Radius();
		iIndex = 13;
	}

	if( iIndex < 0 ) 
		iIndex = iMaxRadiusIndex;

	Assert( iIndex != -1 );

	Sphere kMinimal = akS[iIndex];

	switch ( iIndex )
	{
	case 0:
		rkSupp.m_iQuantity = 2;
		rkSupp.m_aiIndex[1] = i;
		break;
	case 1:
		rkSupp.m_iQuantity = 2;
		rkSupp.m_aiIndex[0] = i;
		break;
	case 2:
		rkSupp.m_iQuantity = 2;
		rkSupp.m_aiIndex[0] = rkSupp.m_aiIndex[2];
		rkSupp.m_aiIndex[1] = i;
		break;
	case 3:
		rkSupp.m_iQuantity = 2;
		rkSupp.m_aiIndex[0] = rkSupp.m_aiIndex[3];
		rkSupp.m_aiIndex[1] = i;
		break;
	case 4:
		rkSupp.m_iQuantity = 3;
		rkSupp.m_aiIndex[2] = i;
		break;
	case 5:
		rkSupp.m_iQuantity = 3;
		rkSupp.m_aiIndex[1] = i;
		break;
	case 6:
		rkSupp.m_iQuantity = 3;
		rkSupp.m_aiIndex[1] = rkSupp.m_aiIndex[3];
		rkSupp.m_aiIndex[2] = i;
		break;
	case 7:
		rkSupp.m_iQuantity = 3;
		rkSupp.m_aiIndex[0] = i;
		break;
	case 8:
		rkSupp.m_iQuantity = 3;
		rkSupp.m_aiIndex[0] = rkSupp.m_aiIndex[3];
		rkSupp.m_aiIndex[2] = i;
		break;
	case 9:
		rkSupp.m_iQuantity = 3;
		rkSupp.m_aiIndex[0] = rkSupp.m_aiIndex[3];
		rkSupp.m_aiIndex[1] = i;
		break;
	case 10:
		rkSupp.m_aiIndex[3] = i;
		break;
	case 11:
		rkSupp.m_aiIndex[2] = i;
		break;
	case 12:
		rkSupp.m_aiIndex[1] = i;
		break;
	case 13:
		rkSupp.m_aiIndex[0] = i;
		break;
	}

	return kMinimal;
}

//----------------------------------------------------------------------------
static Sphere ExactSphere1 (const Vector3& rkP)
{
	Sphere kMinimal;
	kMinimal.Center() = rkP;
	kMinimal.Radius() = 0.0f;
	return kMinimal;
}



typedef Sphere (*UpdateFunction)(int,Vector3**,Support&);
static UpdateFunction gs_aoUpdate[5] =
{
	NULL,
		UpdateSupport1,
		UpdateSupport2,
		UpdateSupport3,
		UpdateSupport4
};

bool Support::Contains (int iIndex, Vector3** apkPoint)
{
	for (int i = 0; i < m_iQuantity; i++)
	{
		Vector3 kDiff = *apkPoint[iIndex] - *apkPoint[m_aiIndex[i]];
		if ( kDiff.SquaredLength() < gs_fEpsilon )
			return true;
	}
	return false;
}

Sphere Mgc::MinSphere (int iQuantity, const Vector3* akPoint)
{
	mthRandom random;  // KPM

	// initialize random number generator
	static bool s_bFirstTime = true;
	if ( s_bFirstTime )
	{
		// KPM: Doesn't exist under GNU. I can't really see the harm in removing it.
		//        srand(367);
		s_bFirstTime = false;
	}

	Sphere kMinimal;
	Support kSupp;

	if ( iQuantity >= 1 )
	{
		// create identity permutation (0,1,...,iQuantity-1)
		Vector3** apkPerm = new Vector3*[iQuantity];
		int i;
		for (i = 0; i < iQuantity; i++)
			apkPerm[i] = (Vector3*)&akPoint[i];

		// generate random permutation
		for (i = iQuantity-1; i > 0; i--)
		{
			//          int j = rand() % (i+1);
			int j = random.GetInt() % (i+1);  // KPM: Use the RAGE version.
			if ( j != i )
			{
				Vector3* pSave = apkPerm[i];
				apkPerm[i] = apkPerm[j];
				apkPerm[j] = pSave;
			}
		}

		kMinimal = ExactSphere1(*apkPerm[0]);
		kSupp.m_iQuantity = 1;
		kSupp.m_aiIndex[0] = 0;
		i = 1;
		while ( i < iQuantity )
		{
			if ( !kSupp.Contains(i,apkPerm) )
			{
				if ( !PointInsideSphere(*apkPerm[i],kMinimal) )
				{
					Sphere kSph = gs_aoUpdate[kSupp.m_iQuantity](i,apkPerm,
						kSupp);
					if ( kSph.Radius() > kMinimal.Radius() )
					{
						kMinimal = kSph;
						i = 0;
						continue;
					}
				}
			}
			i++;
		}

		delete[] apkPerm;
	}
	else
	{
		Assert( false );
	}

	kMinimal.Radius() = Math::Sqrt(kMinimal.Radius());
	return kMinimal;
}

static Sphere DegenerateTrisSafeSphere3( const Vector3& rkP0, const Vector3& rkP1, const Vector3& rkP2 )
{
	// we can get away with this since same point used twice, it will not be used due to it being shortest distance
	return DegenerateTrisSafeSphere4( rkP0, rkP1, rkP2, rkP2 );  
}

static Sphere DegenerateTrisSafeSphere4( const Vector3& rkP0, const Vector3& rkP1, const Vector3& rkP2, const Vector3& rkP3 )
{
	Sphere retVal;

	bool usePt3 = ( rkP2 != rkP3 );
	retVal.Center() = ( rkP0 );
	retVal.Center() += ( rkP1 );
	retVal.Center() += ( rkP2 );
	if( usePt3 )
	{
		retVal.Center() += ( rkP3 );
		retVal.Center() /= 4;
	}
	else
	{
		retVal.Center() /= 3;
	}

	retVal.Radius() = retVal.Center().DistSquared( rkP0 );
	Real d = retVal.Center().DistSquared( rkP1 );
	if( d > retVal.Radius() )
		retVal.Radius() = d;
	d = retVal.Center().DistSquared( rkP2 );
	if( d > retVal.Radius() )
		retVal.Radius() = d;
	if( usePt3 )
	{
		d = retVal.Center().DistSquared( rkP3 );
		if( d > retVal.Radius() )
			retVal.Radius() = d;
	}
	return retVal;
}

	///////////////////////////////////////////////////////////////////////////////
}  // namespace Mgc



} // namespace rage


