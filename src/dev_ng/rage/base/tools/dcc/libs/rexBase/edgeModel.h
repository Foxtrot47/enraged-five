// 
// rexBase/edgeModel.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef EXMODEL_EDGEMODEL_H
#define EXMODEL_EDGEMODEL_H

#include "atl/array.h"
#include "vector/matrix44.h"

namespace rage {

class edgeModel {
public:
	struct Vert{
		Vert()				{Pos.Zero();MatrixInd=0;IsDouble=false;}
		Vector3 Pos;
		int MatrixInd;
		bool IsDouble;
	};
	struct Tri {
		Tri()				{MyEdge[0]=MyEdge[1]=MyEdge[2]=-1;VertInd[0]=VertInd[1]=VertInd[2]=-1;Normal.Zero();IsOrphan=false;IsDoubleFaced=false;ErrorTriangle=false;}
		int VertInd[3];		
		Vector3 Normal;
		int MyEdge[3];
		bool IsOrphan;
		bool IsDoubleFaced;
		bool ErrorTriangle;

	};
	struct Edge {
		Edge()				{VertInd[0]=VertInd[1]=TriInd[0]=TriInd[1]=-1;SharedFaceCount=0;Keep=false;MultiFaceError=false;}
		int VertInd[2];
		int TriInd[2];
		int SharedFaceCount;
		bool Keep;
		bool MultiFaceError;
	};

	edgeModel()					{m_Tris=0;m_Verts=0; CoplanarTollerance = 0.001f; RemoveCoplanar = false; }
	~edgeModel()				{delete [] m_Tris; delete [] m_Verts;}

	void Draw();
	void Skin(const Matrix34 *mtx);
	bool Load(const char *filename);
	bool Save(const char *filename);
	void Build();
	void FindNeighbors();
	bool FoundError()		{return (MultiFaceError || NumDoubleFaced)?(true):(false);}

	//
	void SetCoplanarTollerance( float cpt )	{ CoplanarTollerance = cpt; }
	void SetRemoveCoplanar( bool rmcp )		{ RemoveCoplanar = rmcp; }

//private:
	int AddEdge(int tri,int v0, int v1,bool alwaysAdd=false);
	void RemoveEdges();
	bool IsDegenerate(int tri);
	void ComputeAllNormals();
	void ComputeNormal(int triInd);
	bool ContainsEdge(int tri,int edge);
	bool ContainsEdge(int tri,Edge edge);
	bool SameMatrix(int edgeIndex);
	void DetectDoubleFacedTris();
	void DetectDoubleVerts();
	bool TriContainsVert(int triInd, int vertInd);
	int CountSharedFaces(int v0,int v1);
	void FlagErrorTriangles(Edge & e);

	int m_NumVerts, m_NumTris, m_NumMatrices;
	
	Tri				*m_Tris;
	Vert			*m_Verts;
	Vector3			*m_LoadVerts;
	atArray<Edge>	 m_Edges;
	int NumOrphans;
	int NumDoubleFaced;
	int MultiFaceError;
	int OrphanizedTris;

	float CoplanarTollerance;
	bool RemoveCoplanar;
};

} // namespace rage

#endif // ndef EXMODEL_EDGEMODEL_H

/* End of file: exmodel/edgemodel.h */
