#include "rexMaya/mayaHamsterObject.h"
#include "rexMaya/objectMDagPath.h"
#include "rexMaya/mayaUtility.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MGlobal.h>
#include <Maya/MString.h>
#include <Maya/MSelectionList.h>
#pragma warning(pop)


namespace rage
{

//------------------------------------------------------------------------------
rexMayaHamsterObject::rexMayaHamsterObject() :
//------------------------------------------------------------------------------
m_ExportAsSingleEntity(true)
{
}


//------------------------------------------------------------------------------
rexMayaHamsterObject::~rexMayaHamsterObject()
//------------------------------------------------------------------------------
{
}


//------------------------------------------------------------------------------
void rexMayaHamsterObject::SetModuleID(atString& moduleID)
//------------------------------------------------------------------------------
{
    m_ExportItemInfo.m_ModuleID = moduleID;
}


//------------------------------------------------------------------------------
void rexMayaHamsterObject::SetEntityName(atString name)
//------------------------------------------------------------------------------
{
    m_ExportItemInfo.m_ObjectNameDefault = name;
}



//[CLEMENSP]
//------------------------------------------------------------------------------
const char* rexMayaHamsterObject::getMainModuleID() const
//------------------------------------------------------------------------------
{
    return "Entity";
}


//------------------------------------------------------------------------------
void rexMayaHamsterObject::SetOutputPath(atString& path)
//------------------------------------------------------------------------------
{
   const atString property("OutputPath");

   int i = m_ExportItemInfo.m_PropertyIDs.Find(property);

   if (i >= 0)
   {
       m_ExportItemInfo.m_PropertyStrings[i] = path;
       m_ExportItemInfo.m_PropertyValues[i] = path;
       m_ExportItemInfo.m_PropertyLocked[i] = false;
   }
   else
   {
       m_ExportItemInfo.m_PropertyIDs.PushAndGrow(property);
       m_ExportItemInfo.m_PropertyStrings.PushAndGrow(path);
       m_ExportItemInfo.m_PropertyValues.PushAndGrow(path);
       m_ExportItemInfo.m_PropertyLocked.PushAndGrow(false);
       m_ExportItemInfo.m_PropertyConnectedObjects.PushAndGrow(atArray<rexObject*>());
   }
}


//------------------------------------------------------------------------------
void rexMayaHamsterObject::SetExportAsSingleEntity(bool asSingle)
//------------------------------------------------------------------------------
{
    m_ExportAsSingleEntity = asSingle;
}


//------------------------------------------------------------------------------
void rexMayaHamsterObject::AddMDagPath(atString& dagPath, const char* exportName)
//------------------------------------------------------------------------------
{
    m_MDagPaths.PushAndGrow(dagPath);
    m_ExportNames.PushAndGrow(atString(exportName));
}


//------------------------------------------------------------------------------
const atString rexMayaHamsterObject::GetMDagPathExportName(const char *dagPath) const
//------------------------------------------------------------------------------
{
    int i = m_MDagPaths.Find(atString(dagPath));

    AssertMsg(i >= 0, "MDagPath not found");

    return m_ExportNames[i];
}


//------------------------------------------------------------------------------
const atString rexMayaHamsterObject::GetMDagPathExportName(const rexObject* object) const
//------------------------------------------------------------------------------
{
    const rexObjectMDagPath* objectMDagPath = dynamic_cast<const rexObjectMDagPath*>(object);

    atString dagPath(objectMDagPath->m_DagPath.fullPathName().asChar());

    return GetMDagPathExportName(dagPath);
}


//------------------------------------------------------------------------------
const atString rexMayaHamsterObject::GetOutputPath() const
//------------------------------------------------------------------------------
{
    const atString property("OutputPath");

    int i = m_ExportItemInfo.m_PropertyIDs.Find(property);

    AssertMsg(i >= 0, "OuputPath is not set!");

    return m_ExportItemInfo.m_PropertyStrings[i];
}


//------------------------------------------------------------------------------
const atString rexMayaHamsterObject::GetEntityName() const
//------------------------------------------------------------------------------
{
    return m_ExportItemInfo.m_ObjectNameDefault;
}


//------------------------------------------------------------------------------
void rexMayaHamsterObject::SetupInputObjects(atArray<rexObject*>& objects) const
//------------------------------------------------------------------------------
{
    for (int i = 0; i < m_MDagPaths.GetCount(); i++)
    {
        MString objectPath(m_MDagPaths[i]);
        MSelectionList selList;
        MStatus obStat = selList.add(objectPath);
        if(obStat != MS::kSuccess)
        {
            // Unable to add for some reason
            MGlobal::executeCommand("rageError(\"Unable to find "+ objectPath +" to export\")", false);
        }
        MDagPath obDagPath;
        selList.getDagPath(0, obDagPath);
        objects.Grow((u16) 1) = new rexObjectMDagPath(obDagPath);
    }
}


//------------------------------------------------------------------------------
void rexMayaHamsterObject::SetupExportItemInfos(atArray<rexShell::ExportItemInfo>& infos, 
                                                const atArray<rexObject*>& objects) const
//------------------------------------------------------------------------------
{
    if (m_ExportAsSingleEntity)
    {
        SetupExportItemInfoForSingleEntity(infos, objects);
    }
    else
    {
        SetupExportItemInfoForEntities(infos, objects);
    } 
}

//------------------------------------------------------------------------------
void rexMayaHamsterObject::SetupExportItemInfoForSingleEntity(atArray<rexShell::ExportItemInfo>& infos, 
                                                              const atArray<rexObject*>& objects) const
//------------------------------------------------------------------------------
{
    rexShell::ExportItemInfo obExportItemInfo;
    obExportItemInfo.m_ModuleID = getMainModuleID();
    obExportItemInfo.m_ObjectNameDefault = GetEntityName();
    obExportItemInfo.m_ModuleActive = true;

    obExportItemInfo.m_PropertyIDs.PushAndGrow(atString("OutputPath"));
    obExportItemInfo.m_PropertyStrings.PushAndGrow(GetOutputPath());
    obExportItemInfo.m_PropertyValues.PushAndGrow(GetOutputPath());
    obExportItemInfo.m_PropertyLocked.PushAndGrow(false);

    obExportItemInfo.m_PropertyIDs.PushAndGrow(atString("EntityTypeFileName"));
    obExportItemInfo.m_PropertyStrings.PushAndGrow(GetEntityName());
    obExportItemInfo.m_PropertyValues.PushAndGrow(GetEntityName());
    obExportItemInfo.m_PropertyLocked.PushAndGrow(false);

    obExportItemInfo.m_PropertyConnectedObjects.PushAndGrow(atArray<rexObject*>());
    obExportItemInfo.m_PropertyConnectedObjects.PushAndGrow(atArray<rexObject*>());

    rexShell::ExportItemInfo obExportItemInfoForGeneric;

    obExportItemInfoForGeneric.m_ModuleID = m_ExportItemInfo.m_ModuleID;
    obExportItemInfoForGeneric.m_ObjectNameDefault = m_ExportItemInfo.m_ObjectNameDefault;
    obExportItemInfoForGeneric.m_ModuleActive = m_ExportItemInfo.m_ModuleActive;

    int numProperties = m_ExportItemInfo.m_PropertyIDs.GetCount();

    for (int j = 0; j < numProperties; j++)
    {
        obExportItemInfoForGeneric.m_PropertyIDs.PushAndGrow(m_ExportItemInfo.m_PropertyIDs[j]);
        obExportItemInfoForGeneric.m_PropertyStrings.PushAndGrow(m_ExportItemInfo.m_PropertyStrings[j]);
        obExportItemInfoForGeneric.m_PropertyValues.PushAndGrow(m_ExportItemInfo.m_PropertyValues[j]);
        obExportItemInfoForGeneric.m_PropertyLocked.PushAndGrow(m_ExportItemInfo.m_PropertyLocked[j]);

        if (m_ExportItemInfo.m_PropertyIDs[j] == atString("Nodes"))
        {
            atArray<rexObject*> copyOfObjects;
            for (int i = 0; i < objects.GetCount(); i++)
            {
                copyOfObjects.Grow((u16)1) = objects[i]->CreateCopy();
            }

            obExportItemInfoForGeneric.m_PropertyConnectedObjects.PushAndGrow(copyOfObjects);
        }
        else
        {
            obExportItemInfoForGeneric.m_PropertyConnectedObjects.PushAndGrow(m_ExportItemInfo.m_PropertyConnectedObjects[j]);
        }
    }

    obExportItemInfo.m_Children.PushAndGrow(obExportItemInfoForGeneric);

    infos.PushAndGrow(obExportItemInfo);
}


//------------------------------------------------------------------------------
void rexMayaHamsterObject::SetupExportItemInfoForEntities(atArray<rexShell::ExportItemInfo>& infos, const atArray<rexObject*>& objects) const
//------------------------------------------------------------------------------
{
    for (int i = 0; i < objects.GetCount(); i++)
    {
        rexShell::ExportItemInfo obExportItemInfo;
        obExportItemInfo.m_ModuleID = "Entity";
        obExportItemInfo.m_ObjectNameDefault = "Hamster";
        obExportItemInfo.m_ModuleActive = true;

        obExportItemInfo.m_PropertyIDs.PushAndGrow(atString("OutputPath"));
        obExportItemInfo.m_PropertyStrings.PushAndGrow(GetOutputPath());
        obExportItemInfo.m_PropertyValues.PushAndGrow(GetOutputPath());
        obExportItemInfo.m_PropertyLocked.PushAndGrow(false);

        atString exportName = GetMDagPathExportName(objects[i]);

        obExportItemInfo.m_PropertyIDs.PushAndGrow(atString("EntityTypeFileName"));
        obExportItemInfo.m_PropertyStrings.PushAndGrow(exportName);
        obExportItemInfo.m_PropertyValues.PushAndGrow(exportName);
        obExportItemInfo.m_PropertyLocked.PushAndGrow(false);

        obExportItemInfo.m_PropertyConnectedObjects.PushAndGrow(atArray<rexObject*>());
        obExportItemInfo.m_PropertyConnectedObjects.PushAndGrow(atArray<rexObject*>());

        rexShell::ExportItemInfo obExportItemInfoForGeneric;

        obExportItemInfoForGeneric.m_ModuleID = m_ExportItemInfo.m_ModuleID;
        obExportItemInfoForGeneric.m_ObjectNameDefault = m_ExportItemInfo.m_ObjectNameDefault;
        obExportItemInfoForGeneric.m_ModuleActive = m_ExportItemInfo.m_ModuleActive;

        int numProperties = m_ExportItemInfo.m_PropertyIDs.GetCount();

        for (int j = 0; j < numProperties; j++)
        {
            obExportItemInfoForGeneric.m_PropertyIDs.PushAndGrow(m_ExportItemInfo.m_PropertyIDs[j]);
            obExportItemInfoForGeneric.m_PropertyStrings.PushAndGrow(m_ExportItemInfo.m_PropertyStrings[j]);
            obExportItemInfoForGeneric.m_PropertyValues.PushAndGrow(m_ExportItemInfo.m_PropertyValues[j]);
            obExportItemInfoForGeneric.m_PropertyLocked.PushAndGrow(m_ExportItemInfo.m_PropertyLocked[j]);

            if (m_ExportItemInfo.m_PropertyIDs[j] == atString("Nodes"))
            {
                atArray<rexObject*> copyOfObjects;
                copyOfObjects.Grow((u16)1) = objects[i]->CreateCopy();

                obExportItemInfoForGeneric.m_PropertyConnectedObjects.PushAndGrow(copyOfObjects);
            }
            else
            {
                obExportItemInfoForGeneric.m_PropertyConnectedObjects.PushAndGrow(m_ExportItemInfo.m_PropertyConnectedObjects[j]);
            }
        }

        obExportItemInfo.m_Children.PushAndGrow(obExportItemInfoForGeneric);

        infos.PushAndGrow(obExportItemInfo);
    }
}

} // namespace rage
