#define _BOOL
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MAnimControl.h>
#include <Maya/MBoundingBox.h>
#include <Maya/MDagPath.h>
#include <Maya/MDagPathArray.h>
#include <Maya/MDistance.h>
#include <Maya/MFloatPointArray.h>
#include <Maya/MFloatVectorArray.h>
#include <Maya/MFnDagNode.h>
#include <Maya/MFnIkJoint.h>
#include <Maya/MFnMesh.h>
#include <Maya/MPointArray.h>
#include <Maya/MFnBlendShapeDeformer.h>
#include <Maya/MFnCompoundAttribute.h>
#include <Maya/MFnComponentListData.h>
#include <Maya/MFnPointArrayData.h>
#include <Maya/MFnSingleIndexedComponent.h>
#include <Maya/MCommandResult.h>
#include <Maya/MItMeshFaceVertex.h>

// to include MFnPlugin.h more than once, the MNoVersionString symbol must be defined
#define MNoVersionString
#include <Maya/MFnPlugin.h>
#undef MNoVersionString

#include <Maya/MFnIkHandle.h>
#include <Maya/MFnNumericAttribute.h>
#include <Maya/MFnSet.h>
#include <Maya/MFnSkinCluster.h>
#include <Maya/MFnTransform.h>
#include <Maya/MFnTypedAttribute.h>
#include <Maya/MFnUnitAttribute.h>
#include <Maya/MPxNode.h>
#include <Maya/MPxTransformationMatrix.h>
#include <Maya/MGlobal.h>
#include <Maya/MItDag.h>
#include <Maya/MItDependencyGraph.h>
#include <Maya/MItDependencyNodes.h>
#include <Maya/MItGeometry.h>
#include <Maya/MItMeshPolygon.h>
#include <Maya/MMatrix.h>
#include <Maya/MPlug.h>
#include <Maya/MPlugArray.h>
#include <Maya/MDoubleArray.h>
#include <Maya/MIntArray.h>
#include <Maya/MAnimUtil.h>
#include <Maya/MString.h>
#pragma warning(pop)

#include "mayahelp.h"


#include "atl/map.h"
#include "atl/string.h"

#include "diag/output.h"
#include "file/asset.h"
#include "string/string.h"

#include "rexGeneric/objectMesh.h"

#include "rexMaya/mayaUtility.h"

#include "vector/matrix34.h"
#include "vector/matrix44.h"

#include "shaderMaterial/shaderMaterialGeoParamValueTexture.h"
#include "rageShaderMaterial/rageShaderMaterial.h"

#include <list>
#include <ctype.h>


using namespace rage;

// Useful option to make things easier to export
#define IGNORE_JOINTS_CALLED_UNDERSCORE_NULL true

// For bind pose.

// material map:
typedef atMap<atString,rexObjectGenericMesh::MaterialInfo> rexMaterialMap;
static rexMaterialMap* s_MayaMaterialMap;


void rexMayaUtility::ConstructNamespace()
{
	s_MayaMaterialMap=new rexMaterialMap;
}

void rexMayaUtility::DestructNamespace()
{
	delete s_MayaMaterialMap;
	s_MayaMaterialMap=NULL;
}

// root skeleton map:
typedef atMap<atString,int> rexRootSkeletonMap;
static rexRootSkeletonMap* s_RootSkeletonMap;

void rexMayaUtility::BeginRootSkeleton()
{
	delete s_RootSkeletonMap;
	s_RootSkeletonMap=new rexRootSkeletonMap;
}

void rexMayaUtility::EndRootSkeleton()
{
	if( s_RootSkeletonMap )
		delete s_RootSkeletonMap;
}

void rexMayaUtility::AddRootSkeletonBone(const char* boneDagPathName,int boneIndex)
{
#ifdef DEBUGGING_JOINTS_CALLED_UNDERSCORE_NULL
	Displayf("%s %d void rexMayaUtility::AddRootSkeletonBone(\"%s\", %d)", __FILE__, __LINE__, boneDagPathName, boneIndex);
#endif
	Assert(s_RootSkeletonMap);
	Assert(s_RootSkeletonMap->Access(boneDagPathName)==NULL);
	s_RootSkeletonMap->Insert(atString(boneDagPathName),boneIndex);
}

int rexMayaUtility::GetRootSkeletonBoneIndex(const char* boneDagPathName)
{
#ifdef DEBUGGING_JOINTS_CALLED_UNDERSCORE_NULL
	Displayf("%s %d rexMayaUtility::GetRootSkeletonBoneIndex(\"%s\")", __FILE__, __LINE__, boneDagPathName);
#endif
	Assert(s_RootSkeletonMap);
	int* boneIndex=s_RootSkeletonMap->Access(boneDagPathName);
	if (boneIndex==NULL)
		return -1;
	else 
		return *boneIndex;
}


int rexMayaUtility::GetRootSkeletonNumBones()
{
	return s_RootSkeletonMap->GetNumUsed();
}


///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexMayaUtility::RegisterNodes(MFnPlugin &plugin, rexNodeInfo *nodes)
{
	rexResult retval( rexResult::PERFECT );
	MStatus status( MS::kSuccess );
	MString classification;
	MString *classificationPtr;
	int i;

	for(i = 0; nodes[i].m_Name; i++)
	{
		if(nodes[i].m_Classification)
		{
			classification.set(nodes[i].m_Classification);
			classificationPtr = &classification;
		}
		else
			classificationPtr = NULL;

		switch(nodes[i].m_Type)
		{
		case(MPxNode::kDependNode): // fall-through
		case(MPxNode::kLocatorNode): // fall-through
		case(MPxNode::kManipContainer): // fall-through
		case(MPxNode::kHwShaderNode):
			status = plugin.registerNode(nodes[i].m_Name,
										 *nodes[i].m_ID,
										 nodes[i].m_CreatorFunction,
										 nodes[i].m_InitFunction,
										 (MPxNode::Type)nodes[i].m_Type,
										 classificationPtr);
			break;
		case(MPxNode::kTransformNode):
			status = plugin.registerTransform(nodes[i].m_Name,
											  *nodes[i].m_ID,
											  nodes[i].m_CreatorFunction,
											  nodes[i].m_InitFunction,
											  MPxTransformationMatrix::creator,
											  MPxTransformationMatrix::baseTransformationMatrixId,
											  classificationPtr);
			break;
		default:
			retval.Combine( rexResult::WARNINGS, rexResult::INITIALIZATION | rexResult::TYPE_INCOMPATIBLE | rexResult::CANNOT_CREATE_OBJECT);
			break;
		}

		if( status.error() )
		{
			retval.Combine( rexResult::ERRORS, rexResult::INITIALIZATION );
			retval.AddMessage( status.errorString().asChar() );
			break;
		}
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexMayaUtility::DeregisterNodes(MFnPlugin &plugin, rexNodeInfo *nodes)
{
	rexResult retval( rexResult::PERFECT );
	MStatus status;
	int i;

	for(i = 0; nodes[i].m_Name; i++)
	{
		status = plugin.deregisterNode(*nodes[i].m_ID);
		if( status.error() )
		{
			retval.Set( rexResult::ERRORS, rexResult::TERMINATION );
			break;
		}
	}

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////

bool rexMayaUtility::FindChainRoot(const char *name, MDagPath& root)
{
	if( !name || !name[0] ) 
		return false;

	// Find a Maya node with the given name.
	MDagPath dagpath;
	if( !FindNode(name, dagpath) )
		return false;

	// The dagpath has to be joint or transform.
	if( dagpath.apiType() != MFn::kJoint &&	dagpath.apiType() != MFn::kTransform )
		return false;

	// Traverse up from this dagpath.
	root = dagpath;
	MFnDagNode theDag(root);
	MFn::Type validType = dagpath.apiType();
	while(1)
	{
		MObject parent = theDag.parent(0);
		if( parent.apiType() != validType )
			break;

		MFnDagNode fnParent(parent);
		fnParent.getPath(root);

		// Traverse up
		theDag.setObject(parent);
	};

	return true;
}

bool rexMayaUtility::IsValidBoneNode( const MDagPath& dagpath, bool useOnlyJoints, bool name_NullReturnsFalse )
{
#ifdef DEBUGGING_JOINTS_CALLED_UNDERSCORE_NULL
	Displayf("bool rexMayaUtility::IsValidBoneNode( %s, %d, %d )", dagpath.fullPathName(), useOnlyJoints, name_NullReturnsFalse );
#endif
	if( !dagpath.isValid() )
		return false;

	MObject obj = dagpath.node();

	MStatus status;

	MFnDagNode dag( obj );
	
	atString dagName(dag.name().asChar());
	if( name_NullReturnsFalse && !strcmpi( dagName, "_null" ) )
	{
		return false;
	}

	if( dagpath.apiType() == MFn::kJoint )
		return true;

	if( useOnlyJoints || IsChildOfLevelInstanceObject( dagpath ) || IsLevelInstanceObject( dagpath ) || (( dagpath.apiType() != MFn::kTransform ) && ( dagpath.apiType() != MFn::kLodGroup )))
//	if( useOnlyJoints || (( dagpath.apiType() != MFn::kTransform ) && ( dagpath.apiType() != MFn::kLodGroup )))
		return false;

	int numConnections;

	MPlug plug = dag.findPlug("scale", &status );
	if( !status.error() )
	{
		numConnections = 0;
		MPlug px = plug.child(0, &status);
		numConnections += px.isConnected();
		MPlug py = plug.child(1, &status);
		numConnections += py.isConnected();
		MPlug pz = plug.child(2, &status);
		numConnections += pz.isConnected();

		if( numConnections && !( (px.isLocked() && py.isLocked() && pz.isLocked()) ||
			(!px.isKeyable() && !py.isKeyable() && !pz.isKeyable()) ))
			return true;
	}

	plug = dag.findPlug("translate", &status );
	if( !status.error() )
	{
		numConnections = 0;
		MPlug px = plug.child(0, &status);
		numConnections += px.isConnected();
		MPlug py = plug.child(1, &status);
		numConnections += py.isConnected();
		MPlug pz = plug.child(2, &status);
		numConnections += pz.isConnected();

		if( numConnections && !( (px.isLocked() && py.isLocked() && pz.isLocked()) ||
			(!px.isKeyable() && !py.isKeyable() && !pz.isKeyable()) ))
			return true;
	}


	plug = dag.findPlug("rotate", &status );
	if( !status.error() )
	{
		numConnections = 0;
		MPlug px = plug.child(0, &status);
		numConnections += px.isConnected();
		MPlug py = plug.child(1, &status);
		numConnections += py.isConnected();
		MPlug pz = plug.child(2, &status);
		numConnections += pz.isConnected();

		if( numConnections && !( (px.isLocked() && py.isLocked() && pz.isLocked()) ||
			(!px.isKeyable() && !py.isKeyable() && !pz.isKeyable()) ))
			return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////

void rexMayaUtility::GetMatrix34FromMayaMatrix(Matrix34 &m34, const MMatrix &mm)
{
	float ax = (float) mm(0,0);
	float ay = (float) mm(0,1);
	float az = (float) mm(0,2);
	float bx = (float) mm(1,0);
	float by = (float) mm(1,1);
	float bz = (float) mm(1,2);
	float cx = (float) mm(2,0);
	float cy = (float) mm(2,1);
	float cz = (float) mm(2,2);
	float dx = (float) mm(3,0);
	float dy = (float) mm(3,1);
	float dz = (float) mm(3,2);
	m34.a.Set(ax, ay, az);
	m34.b.Set(bx, by, bz);
	m34.c.Set(cx, cy, cz);
	m34.d.Set(dx, dy, dz);
}

///////////////////////////////////////////////////////////////////////////////////////////

void rexMayaUtility::GetMatrix44FromMayaMatrix(Matrix44 &m44, const MMatrix &mm)
{
	float ax = (float) mm(0,0);
	float ay = (float) mm(0,1);
	float az = (float) mm(0,2);
	float aw = (float) mm(0,3);
	float bx = (float) mm(1,0);
	float by = (float) mm(1,1);
	float bz = (float) mm(1,2);
	float bw = (float) mm(1,3);
	float cx = (float) mm(2,0);
	float cy = (float) mm(2,1);
	float cz = (float) mm(2,2);
	float cw = (float) mm(2,3);
	float dx = (float) mm(3,0);
	float dy = (float) mm(3,1);
	float dz = (float) mm(3,2);
	float dw = (float) mm(3,3);

	m44.a.Set(ax, ay, az, aw);
	m44.b.Set(bx, by, bz, bw);
	m44.c.Set(cx, cy, cz, cw);
	m44.d.Set(dx, dy, dz, dw);
}

///////////////////////////////////////////////////////////////////////////////////////////

bool GetParentJointDagPath( MDagPath dpChild, MDagPath& dpParent )
{
	MStatus status;
	
	MDagPath dpSub(dpChild);
	while(dpSub.length())
	{
		status = dpSub.pop();
		if(status == MS::kSuccess && (dpSub.apiType() == MFn::kJoint))
		{
			dpParent = dpSub;
			return true;
		}
	}
	return false;
}

Matrix44 rexMayaUtility::GetSkeletonWorldMatrix( MDagPath dp )
{

//A more simple way of doing the code bellow, but not fully tested
	//joints derive from transform nodes so this should work.
	Assert(dp.hasFn(MFn::kTransform));

	Matrix44 M;
	M.Identity();
	MFnTransform fnT1( dp );

	MTransformationMatrix localTM;
	localTM = MTransformationMatrix::identity;

	//[R] - Rotation
	MQuaternion rotate;
	fnT1.getRotation(rotate,MSpace::kWorld);
	localTM.rotateBy(rotate, MSpace::kWorld);

	//[T] - Translation
	MVector translate = fnT1.getTranslation(MSpace::kWorld);
	localTM.setTranslation(translate, MSpace::kWorld);

	MMatrix localMat = localTM.asMatrix();
	GetMatrix44FromMayaMatrix(M, localMat);

/*
	MStatus status;
	Matrix44 M;

	if( dp.apiType() == MFn::kJoint )
	{
		//Get a list of all the local-space matrices for the joint, without scale orients or joint orients
		std::list< MMatrix >	localMats;
		MDagPath dpSubPath(dp);
		while(dpSubPath.length())
		{
			if( dpSubPath.apiType() == MFn::kJoint )
			{
				MTransformationMatrix localTM;
				localTM = MTransformationMatrix::identity;

				MFnIkJoint	joint(dpSubPath);

				//[S] - Scale
				double scale[3] ;
				status = joint.getScale(scale);
				localTM.setScale(scale, MSpace::kTransform);


				//[R] - Rotation
				MQuaternion rotate;
				status = joint.getRotation(rotate);
				localTM.rotateBy(rotate, MSpace::kTransform);

				//[IS] - inverse parent scale
				MDagPath parentDp;
				double parentInvScale[3] = {1.0,1.0,1.0};
				if(GetParentJointDagPath(dpSubPath, parentDp))
				{
					MFnIkJoint parentJoint = MFnIkJoint(parentDp);

					MTransformationMatrix invParentMat(parentJoint.transformation().asMatrixInverse());
					invParentMat.getScale(parentInvScale, MSpace::kTransform);
				}
				localTM.addScale(parentInvScale, MSpace::kTransform);

				//[T] - Translation
				MVector translate = joint.translation(MSpace::kTransform, &status);
				localTM.setTranslation(translate, MSpace::kTransform);

				MMatrix localMat = localTM.asMatrix();
				localMats.push_front(localMat);
			}
			dpSubPath.pop();
		}

		//Concatenate the world-space matrix for the joint from the local space mats (without scale or joint orients)
		if(localMats.size() == 1)
		{
			GetMatrix44FromMayaMatrix(M, localMats.front());
		}
		else 
		{
			MMatrix worldMat;
			MMatrix A,B;

			A = localMats.front();
			localMats.pop_front();
			B = localMats.front();
			localMats.pop_front();
			worldMat = B * A;
			while(localMats.size())
			{
				A = localMats.front();
				localMats.pop_front();
				worldMat = A * worldMat;
			}
			GetMatrix44FromMayaMatrix(M, worldMat);
		}
	}
	else
	{
		M.Identity();
		MFnTransform fnT1( dp );

		//Begin New -- Fix for improperly animated level objects
		MEulerRotation tmpRot;
		fnT1.getRotation(tmpRot);
		MMatrix tMat = tmpRot.asMatrix();
		GetMatrix44FromMayaMatrix(M, tMat);
		//End New...

		MPoint rp = fnT1.rotatePivot( MSpace::kWorld );
		M.d.x = (float)rp.x;
		M.d.y = (float)rp.y;
		M.d.z = (float)rp.z;
	}
*/
	return M;
}

Matrix44 rexMayaUtility::GetSkeletonLocalMatrix( MDagPath dp, MDagPath parentDP )
{
	Matrix44 result;

	if( dp.apiType() == MFn::kJoint )
	{
		MFnDagNode fn( dp );

		GetMatrix44FromMayaMatrix( result, fn.transformationMatrix() );
		result.a.w = 0.0f;
		result.b.w = 0.0f;
		result.c.w = 0.0f;
		result.d.w = 1.0f;
	}
	else
	{
		result.Identity();

		MFnTransform fnT2( dp );
		MPoint rp = fnT2.rotatePivot( MSpace::kWorld );
		result.d.x = (float)rp.x;
		result.d.y = (float)rp.y;
		result.d.z = (float)rp.z;

		MFnTransform fnT1( parentDP );
		rp = fnT1.rotatePivot( MSpace::kWorld );
		result.d.x -= (float)rp.x;
		result.d.y -= (float)rp.y;
		result.d.z -= (float)rp.z;
	}
	return result;
}

/*
PURPOSE
	Get the local matrix from the maya dag node.
PARAMS
	m - the matrix to be returned.
	dagnode - where the matrix to get.
RETURN
	none.
NOTES
	The adjust matrix may be applied to the returned matrix, if required.
*/
void rexMayaUtility::GetLocalMatrix(Matrix34 &m, const MFnDagNode &dagnode)
{
	GetMatrix34FromMayaMatrix(m, dagnode.transformationMatrix());
}

bool rexMayaUtility::GetJointOrientMatrices(const MDagPath& dp,Matrix34& jointOrientMat,Matrix34& scaleOrientMat)
{
	if (dp.apiType()!= MFn::kJoint)
		return false;

	MFnIkJoint joint(dp);

	double orientation[2][3];
	MTransformationMatrix::RotationOrder rotOrder[2];

	joint.getOrientation(orientation[0],rotOrder[0]);
	joint.getScaleOrientation(orientation[1],rotOrder[1]);

	// collect matrix:
	for (int i=0;i<2;i++)
	{
		Vector3 orientationFlt;
		orientationFlt[0]=(float)orientation[i][0];
		orientationFlt[1]=(float)orientation[i][1];
		orientationFlt[2]=(float)orientation[i][2];

		Matrix34& mat=i==0?jointOrientMat:scaleOrientMat;

		// get our matrix from the orientation eulers:
		mat.Identity();
		switch (rotOrder[i])
		{
		case MTransformationMatrix::kXYZ:
			mat.FromEulersXYZ(orientationFlt);
			break;
		case MTransformationMatrix::kXZY:
			mat.FromEulersXZY(orientationFlt);
			break;
		case MTransformationMatrix::kYXZ:
			mat.FromEulersYXZ(orientationFlt);
			break;
		case MTransformationMatrix::kYZX:
			mat.FromEulersYZX(orientationFlt);
			break;
		case MTransformationMatrix::kZXY:
			mat.FromEulersZXY(orientationFlt);
			break;
		case MTransformationMatrix::kZYX:
			mat.FromEulersZYX(orientationFlt);
			break;
		default:
			AssertMsg(0,"Undefined transform order");
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////

bool rexMayaUtility::IsAncestorOf(const MDagPath& ancestor, const MDagPath& descendant)
{
	if( ( descendant.fullPathName().length() < 2 ) || !ancestor.isValid() || !descendant.isValid() )
		return false;

	MDagPath descendantParent( descendant );
	descendantParent.pop();

	if( ancestor == descendantParent )
		return true;

	return IsAncestorOf( ancestor, descendantParent );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexMayaUtility::FindNode(const char *name, MDagPath& output)
{
	MItDag iter(MItDag::kBreadthFirst, MFn::kInvalid);
	while( ! iter.isDone() )
	{
		MDagPath dp;
		iter.getPath(dp);
		if(( !strcmp( dp.partialPathName().asChar(), name ) || !strcmp( dp.fullPathName().asChar(), name )))
		{
			output = dp;
    		return true;
		}

		iter.next();
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexMayaUtility::AddAttribute( MObject obj, const char *attName, const char *attValue )
{
	if( obj.isNull() ) return MS::kFailure;

	bool retval = true;

	if( AttributeExists( obj, attName ) )
		retval = DeleteAttribute( obj, attName );
	
	if( !retval )
		return false;

	MFnDagNode fn( obj );

	MString cmd( "addAttr -dt \"string\" -longName " );
	cmd += attName;
	cmd += " ";
	cmd += fn.partialPathName();				

	retval = ( !MGlobal::executeCommand( cmd ).error() );

	if( retval )
		retval = SetAttributeValue( obj, attName, attValue );

	return retval;
}

bool rexMayaUtility::AddAttribute(	MObject obj, const char *attName, float attValue )
{
	char temp[256];
	sprintf( temp, "%f", attValue );

	atString s( temp );	
	return AddAttribute( obj, attName, s );	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexMayaUtility::SetAttributeValue( MObject obj, const char *attName, const char *attValue )
{
	if( obj.isNull() ) return MS::kFailure;

	MFnDagNode fn( obj );
	MString cmd( "setAttr -type \"string\" " );

	cmd += fn.partialPathName();				
	cmd += ".";
	cmd += attName;
	cmd += " \"";
	cmd += attValue;
	cmd += "\"";

	return ( !MGlobal::executeCommand( cmd ).error() );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexMayaUtility::AttributeExists( const MObject obj, const char *attName )
{
	if( obj.isNull() ) return false;

	MStatus status = MS::kSuccess;
	MFnDependencyNode objDN(obj);
	objDN.attribute(MString((const char*)attName, (int) strlen(attName)), &status);

	return !(status.error());
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexMayaUtility::DeleteAttribute( MObject obj, const char *attName )
{
	if( AttributeExists( obj, attName ) )
	{
		MFnDagNode fn( obj );
		MString cmd( "deleteAttr " );

		cmd += fn.partialPathName();				
		cmd += ".";
		cmd += attName;

		return MGlobal::executeCommand( cmd, false );		
	}
	return MS::kFailure;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

rexValue rexMayaUtility::GetAttributeValue( const MObject obj, const char *attName )
{
	rexValue retval;

	if( AttributeExists( obj, attName ) )
	{
		MStatus status = MS::kSuccess;
		MFnDependencyNode dn( obj );
		MObject attrObj;
		MPlug plug;

		attrObj = dn.attribute(MString((const char*)attName, (int) strlen(attName)));
		plug = dn.findPlug(attrObj, &status);

		//printf("\tattribute type: %s\n", attrObj.apiTypeStr());

		MFn::Type t = attrObj.apiType();

		switch(t)
		{
		case(MFn::kNumericAttribute):
			{
				MFnNumericAttribute numAttr( attrObj );

				switch(numAttr.unitType())
				{
				case(MFnNumericData::kBoolean):
					//printf("\t\tval type: boolean\n");
					{
						bool val;
						plug.getValue(val);
						retval.SetValue(val);
					}
					break;
				case(MFnNumericData::kByte):
					//printf("\t\tval type: byte\n");
					{
						char val;
						plug.getValue(val);
						retval.SetValue(val);
					}
					break;
				/*
				case(MFnNumericData::kChar):
					//printf("\t\tval type: char\n");
					break;
				*/
				case(MFnNumericData::kShort):
					//printf("\t\tval type: short\n");
					{
						short val;
						plug.getValue(val);
						retval.SetValue(val);
					}
					break;
				/*
				case(MFnNumericData::k2Short):
					//printf("\t\tval type: 2 short\n");
					break;
				case(MFnNumericData::k3Short):
					//printf("\t\tval type: 3 short\n");
					break;
				*/
				case(MFnNumericData::kLong):
					// this is actually an int, not long
					//printf("\t\tval type: long\n");
					{
						int val;
						plug.getValue(val);
						retval.SetValue(val);
					}
					break;
				/*
				case(MFnNumericData::k2Long):
					// this is actually an int, not long
					//printf("\t\tval type: 2 long\n");
					break;
				case(MFnNumericData::k3Long):
					// this is actually an int, not long
					//printf("\t\tval type: 3 long\n");
					break;
				*/
				case(MFnNumericData::kFloat):
					//printf("\t\tval type: float\n");
					{
						float val;
						plug.getValue(val);
						retval.SetValue(val);
					}
					break;
				/*
				case(MFnNumericData::k2Float):
					//printf("\t\tval type: 2 float\n");
					break;
				case(MFnNumericData::k3Float):
					//printf("\t\tval type: 3 float\n");
					break;
				*/
				case(MFnNumericData::kDouble):
					//printf("\t\tval type: double\n");
					{
						double val;
						plug.getValue(val);
						retval.SetValue(val);
					}
					break;
				/*
				case(MFnNumericData::k2Double):
					//printf("\t\tval type: 2 double\n");
					break;
				case(MFnNumericData::k3Double):
					//printf("\t\tval type: 3 double\n");
					break;
				*/
				default:
					//printf("\t\tval type: unknown\n");
					break;
				}
			}
			break;
		case(MFn::kDoubleLinearAttribute):
			{
				MFnUnitAttribute uAttr( attrObj );

				switch(uAttr.unitType())
				{
				case(MFnUnitAttribute::kAngle):
					// TODO: implement this
					break;
				case(MFnUnitAttribute::kDistance):
					{
						MDistance dist;
						MStatus status;
						status = plug.getValue(dist);
						if(status)
						{
							double distVal = dist.asCentimeters();
							retval.SetValue(distVal);
						}
					}
					break;
				case(MFnUnitAttribute::kTime):
					// TODO: implement this
					break;
				default:
					break;
				}
			}
			break;
		case(MFn::kTypedAttribute):
			// this is probably a string, so just let the default handler get the value
			/*
			{
				MObject val;

				plug.getValue(val);

				//printf("\t\tval type: %s\n", val.apiTypeStr());
			}
			*/
			break;
		default:
			break;
		}

		if(rexValue::UNKNOWN == retval.type)
		{ // if the type still hasn't been set, just get a string
			// TODO: write a type incompatibility warning to the log (when this happens, move normal string handling up?)
			MString str;
			plug.getValue(str);
			retval.SetValue( atString(str.asChar()) );
		}
	}

	//fflush(stdout);

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

atString rexMayaUtility::GetAttributeValueAsString( const MObject obj, const char *attName )
{
	MString resultString;
	if( AttributeExists( obj, attName ) )
	{
		MFnDependencyNode fn( obj );

		MPlug plug = fn.findPlug( MString((const char*)attName, (int) strlen(attName)) );
		plug.getValue(resultString);
	}
	return atString( resultString.asChar() );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

atString rexMayaUtility::FullPathNameToBoneIdString( const MDagPath& dp )
{
	atString resultString;

	MString mayaFullPath = dp.fullPathName();
	const char* fullPath = mayaFullPath.asChar();
	fullPath++; //Skip the leading '|'

	//Don't include the root of the path in the bone id string
	const char* boneIdPath = strchr( fullPath, '|');
	if(boneIdPath)
		resultString = boneIdPath;
	else
		resultString = --fullPath; 

	return resultString;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexMayaUtility::GetAttributeConnections( const MObject obj, const char *attName, atArray<MDagPath>& outputDPArray, bool asSrc, bool asDest )
{
	MFnDependencyNode fnDag( obj );
	MPlug plug = fnDag.findPlug( MString((const char*)attName, (int) strlen(attName)) );
	
	MPlugArray connections;
	plug.connectedTo( connections, asDest, asSrc );

	int connectionCount = connections.length();
	for( int b = 0; b < connectionCount; b++ )
	{
		MString name = connections[ b ].name();
		MObject obj = connections[ b ].node();
		MDagPath dp = MDagPath::getAPathTo( obj );
		if( dp.isValid() )
			outputDPArray.PushAndGrow( dp, (u16) connectionCount );			
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexMayaUtility::GetUserDefinedAttributeNames( const MObject obj, atArray<atString>& outputNames, bool useLongNames )
{
	MStatus status = MS::kSuccess;
	MFnDependencyNode matDN(obj, &status);

	if(status.error())
		return false;

	unsigned int count = matDN.attributeCount();
	for( unsigned int a = 0; a < count; a++)
	{
		MObject attrObj = matDN.attribute(a);
		MFnAttribute attr(attrObj);
		bool userDefined = attr.isDynamic(&status);

		if(status.error())
			return false;

		if(userDefined)
		{
			MString attrName = useLongNames ? attr.name() : attr.shortName();
			outputNames.PushAndGrow( atString(attrName.asChar()), (u16) count );
		}
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

atString rexMayaUtility::DagPathToNodeName( const MDagPath& dp )
{
	return atString(MFnDependencyNode(dp.node()).name().asChar());
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

atString	rexMayaUtility::DagPathToName( const MDagPath& dp )
{	
	atString dpString(dp.partialPathName().asChar());
	atString name;
	
	int length = dpString.GetLength();
	int colonOffset = -1;

	for( int a = 0; a < length; a++ )
	{
		if( dpString[ a ] == ':' )
		{
			colonOffset = a;
			break;
		}
	}

	// WARNING:
	// this code is broken, inNameSpace can never be true.
	// given the lack of comments, it's difficult to figure out what it was supposed to do.
	// plus the problems of code possibly relying on it's current but inconsistent and broken behaviour.
	bool inNameSpace = false;
	for( int a = 0; a < length; a++ )
	{
		char c = dpString[ a ];
		if( inNameSpace )
		{
			if( c == ':' )
				inNameSpace = false;
		}
		else if( c == ':' || c == '|' || c == ' ' ) //|| !isprint( c ) )
		{
			name += '_';
			if( ( colonOffset >= 0 ) && ( c == ':' ))
				inNameSpace = false;
		}
		else
		{
			name += c;
		}
	}
	return name;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexMayaUtility::CollectNodeLimitInfo(const MDagPathArray& dpArr, DpToLimitInfoTable& retTable)
{
	MStatus status;

	int dpCount = dpArr.length();
	for(int i=0; i<dpCount; i++)
	{
		const MDagPath& dp = dpArr[i];
	
		atString fullPath(dp.fullPathName().asChar());
		if(!retTable.Access(fullPath))
		{
			MFnTransform fnTrans(dp, &status);
			rexNodeLimitInfo li;	
		
			//Translation limits
			li.trLimitEnables[rexNodeLimitInfo::MINX] = fnTrans.isLimited(MFnTransform::kTranslateMinX);
			li.trLimitEnables[rexNodeLimitInfo::MAXX] = fnTrans.isLimited(MFnTransform::kTranslateMaxX);

			li.trLimitEnables[rexNodeLimitInfo::MINY] = fnTrans.isLimited(MFnTransform::kTranslateMinY);
			li.trLimitEnables[rexNodeLimitInfo::MAXY] = fnTrans.isLimited(MFnTransform::kTranslateMaxY);

			li.trLimitEnables[rexNodeLimitInfo::MINZ] = fnTrans.isLimited(MFnTransform::kTranslateMinZ);
			li.trLimitEnables[rexNodeLimitInfo::MAXZ] = fnTrans.isLimited(MFnTransform::kTranslateMaxZ);

			//Rotation limits
			li.roLimitEnables[rexNodeLimitInfo::MINX] = fnTrans.isLimited(MFnTransform::kRotateMinX);
			li.roLimitEnables[rexNodeLimitInfo::MAXX] = fnTrans.isLimited(MFnTransform::kRotateMaxX);

			li.roLimitEnables[rexNodeLimitInfo::MINY] = fnTrans.isLimited(MFnTransform::kRotateMinY);
			li.roLimitEnables[rexNodeLimitInfo::MAXY] = fnTrans.isLimited(MFnTransform::kRotateMaxY);

			li.roLimitEnables[rexNodeLimitInfo::MINZ] = fnTrans.isLimited(MFnTransform::kRotateMinZ);
			li.roLimitEnables[rexNodeLimitInfo::MAXZ] = fnTrans.isLimited(MFnTransform::kRotateMaxZ);

			//Scale limits
			li.scLimitEnables[rexNodeLimitInfo::MINX] = fnTrans.isLimited(MFnTransform::kScaleMinX);
			li.scLimitEnables[rexNodeLimitInfo::MAXX] = fnTrans.isLimited(MFnTransform::kScaleMaxX);

			li.scLimitEnables[rexNodeLimitInfo::MINY] = fnTrans.isLimited(MFnTransform::kScaleMinY);
			li.scLimitEnables[rexNodeLimitInfo::MAXY] = fnTrans.isLimited(MFnTransform::kScaleMaxY);

			li.scLimitEnables[rexNodeLimitInfo::MINZ] = fnTrans.isLimited(MFnTransform::kScaleMinZ);
			li.scLimitEnables[rexNodeLimitInfo::MAXZ] = fnTrans.isLimited(MFnTransform::kScaleMaxZ);

			retTable.Insert(fullPath, li);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexMayaUtility::SetNodeLimitEnables(const MDagPathArray& dpArr, bool bEnable)
{
	MStatus status;

	int dpCount = dpArr.length();
	for(int i=0; i<dpCount; i++)
	{
		const MDagPath& dp = dpArr[i];
		MFnTransform fnTrans(dp, &status);

		//Translation limits
		fnTrans.enableLimit(MFnTransform::kTranslateMinX, bEnable);
		fnTrans.enableLimit(MFnTransform::kTranslateMaxX, bEnable);
		fnTrans.enableLimit(MFnTransform::kTranslateMinY, bEnable);
		fnTrans.enableLimit(MFnTransform::kTranslateMaxY, bEnable);
		fnTrans.enableLimit(MFnTransform::kTranslateMinZ, bEnable);
		fnTrans.enableLimit(MFnTransform::kTranslateMaxZ, bEnable);

		//Rotation limits
		fnTrans.enableLimit(MFnTransform::kRotateMinX, bEnable);
		fnTrans.enableLimit(MFnTransform::kRotateMaxX, bEnable);
		fnTrans.enableLimit(MFnTransform::kRotateMinY, bEnable);
		fnTrans.enableLimit(MFnTransform::kRotateMaxY, bEnable);
		fnTrans.enableLimit(MFnTransform::kRotateMinZ, bEnable);
		fnTrans.enableLimit(MFnTransform::kRotateMaxZ, bEnable);

		//Scale limits
		fnTrans.enableLimit(MFnTransform::kScaleMinX, bEnable);
		fnTrans.enableLimit(MFnTransform::kScaleMaxX, bEnable);
		fnTrans.enableLimit(MFnTransform::kScaleMinY, bEnable);
		fnTrans.enableLimit(MFnTransform::kScaleMaxY, bEnable);
		fnTrans.enableLimit(MFnTransform::kScaleMinZ, bEnable);
		fnTrans.enableLimit(MFnTransform::kScaleMaxZ, bEnable);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexMayaUtility::SetNodeLimitEnables(const MDagPathArray& dpArr, const DpToLimitInfoTable& limitInfoTable)
{
	MStatus status;

	int dpCount = dpArr.length();
	for(int i=0; i<dpCount; i++)
	{
		const MDagPath& dp = dpArr[i];
		MFnTransform fnTrans(dp, &status);
		atString fullPath(dp.fullPathName().asChar());

		const rexNodeLimitInfo* li = NULL;
		li = limitInfoTable.Access(fullPath);
		if(li)
		{
			//Do to Maya being completely jacked we need to use the script calls here to enable/disable the
			//joint limits since otherwise if this call is being used in conjunction with the ToggleBindPose
			//call, Maya will execute these commands first before it may have restored the pose of the skeleton
			//which can result in warnings about being unable to achieve the requested pose.

			//Translation limits
			fnTrans.enableLimit(MFnTransform::kTranslateMinX, li->trLimitEnables[rexNodeLimitInfo::MINX]);
			fnTrans.enableLimit(MFnTransform::kTranslateMaxX, li->trLimitEnables[rexNodeLimitInfo::MAXX]);
			fnTrans.enableLimit(MFnTransform::kTranslateMinY, li->trLimitEnables[rexNodeLimitInfo::MINY]);
			fnTrans.enableLimit(MFnTransform::kTranslateMaxY, li->trLimitEnables[rexNodeLimitInfo::MAXY]);
			fnTrans.enableLimit(MFnTransform::kTranslateMinZ, li->trLimitEnables[rexNodeLimitInfo::MINZ]);
			fnTrans.enableLimit(MFnTransform::kTranslateMaxZ, li->trLimitEnables[rexNodeLimitInfo::MAXZ]);
			/*MString cmd;
			cmd = "transformLimits -etx ";
			cmd += li->trLimitEnables[rexNodeLimitInfo::MINX] ? "1 " : "0 ";
			cmd += li->trLimitEnables[rexNodeLimitInfo::MAXX] ? "1 " : "0 ";
			cmd += dp.fullPathName();
			MGlobal::executeCommand(cmd, false, false);

			cmd = "transformLimits -ety ";
			cmd += li->trLimitEnables[rexNodeLimitInfo::MINY] ? "1 " : "0 ";
			cmd += li->trLimitEnables[rexNodeLimitInfo::MAXY] ? "1 " : "0 ";
			cmd += dp.fullPathName();
			MGlobal::executeCommand(cmd, false, false);

			cmd = "transformLimits -etz ";
			cmd += li->trLimitEnables[rexNodeLimitInfo::MINZ] ? "1 " : "0 ";
			cmd += li->trLimitEnables[rexNodeLimitInfo::MAXZ] ? "1 " : "0 ";
			cmd += dp.fullPathName();
			MGlobal::executeCommand(cmd, false, false);*/

			//Rotation limits
			fnTrans.enableLimit(MFnTransform::kRotateMinX, li->roLimitEnables[rexNodeLimitInfo::MINX]);
			fnTrans.enableLimit(MFnTransform::kRotateMaxX, li->roLimitEnables[rexNodeLimitInfo::MAXX]);
			fnTrans.enableLimit(MFnTransform::kRotateMinY, li->roLimitEnables[rexNodeLimitInfo::MINY]);
			fnTrans.enableLimit(MFnTransform::kRotateMaxY, li->roLimitEnables[rexNodeLimitInfo::MAXY]);
			fnTrans.enableLimit(MFnTransform::kRotateMinZ, li->roLimitEnables[rexNodeLimitInfo::MINZ]);
			fnTrans.enableLimit(MFnTransform::kRotateMaxZ, li->roLimitEnables[rexNodeLimitInfo::MAXZ]);
			/*cmd = "transformLimits -erx ";
			cmd += li->roLimitEnables[rexNodeLimitInfo::MINX] ? "1 " : "0 ";
			cmd += li->roLimitEnables[rexNodeLimitInfo::MAXX] ? "1 " : "0 ";
			cmd += dp.fullPathName();
			MGlobal::executeCommand(cmd, false, false);

			cmd = "transformLimits -ery ";
			cmd += li->roLimitEnables[rexNodeLimitInfo::MINY] ? "1 " : "0 ";
			cmd += li->roLimitEnables[rexNodeLimitInfo::MAXY] ? "1 " : "0 ";
			cmd += dp.fullPathName();
			MGlobal::executeCommand(cmd, false, false);

			cmd = "transformLimits -erz ";
			cmd += li->roLimitEnables[rexNodeLimitInfo::MINZ] ? "1 " : "0 ";
			cmd += li->roLimitEnables[rexNodeLimitInfo::MAXZ] ? "1 " : "0 ";
			cmd += dp.fullPathName();
			MGlobal::executeCommand(cmd, false, false);*/


			//Scale limits
			fnTrans.enableLimit(MFnTransform::kScaleMinX, li->scLimitEnables[rexNodeLimitInfo::MINX]);
			fnTrans.enableLimit(MFnTransform::kScaleMaxX, li->scLimitEnables[rexNodeLimitInfo::MAXX]);
			fnTrans.enableLimit(MFnTransform::kScaleMinY, li->scLimitEnables[rexNodeLimitInfo::MINY]);
			fnTrans.enableLimit(MFnTransform::kScaleMaxY, li->scLimitEnables[rexNodeLimitInfo::MAXY]);
			fnTrans.enableLimit(MFnTransform::kScaleMinZ, li->scLimitEnables[rexNodeLimitInfo::MINZ]);
			fnTrans.enableLimit(MFnTransform::kScaleMaxZ, li->scLimitEnables[rexNodeLimitInfo::MAXZ]);
			/*cmd = "transformLimits -esx ";
			cmd += li->scLimitEnables[rexNodeLimitInfo::MINX] ? "1 " : "0 ";
			cmd += li->scLimitEnables[rexNodeLimitInfo::MAXX] ? "1 " : "0 ";
			cmd += dp.fullPathName();
			MGlobal::executeCommand(cmd, false, false);

			cmd = "transformLimits -esy ";
			cmd += li->scLimitEnables[rexNodeLimitInfo::MINY] ? "1 " : "0 ";
			cmd += li->scLimitEnables[rexNodeLimitInfo::MAXY] ? "1 " : "0 ";
			cmd += dp.fullPathName();
			MGlobal::executeCommand(cmd, false, false);

			cmd = "transformLimits -esz ";
			cmd += li->scLimitEnables[rexNodeLimitInfo::MINZ] ? "1 " : "0 ";
			cmd += li->scLimitEnables[rexNodeLimitInfo::MAXZ] ? "1 " : "0 ";
			cmd += dp.fullPathName();
			MGlobal::executeCommand(cmd, false, false);*/
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexMayaUtility::PackVertexColours(const shaderMaterialGeoTemplate* pobTemplate, const MStringArray& asNamesOfMeshsColorSets, const vector<MColorArray>& asVertexColoursOfMeshsColorSets, vector<MColorArray>& obAObDestinationVertexColourSets)
{
	if(pobTemplate && (asVertexColoursOfMeshsColorSets.size() > 0))
	{
		for(int i=0; i<pobTemplate->GetRunTimeVertexColourDescriptions().GetNumberOfDescriptions(); i++)
		{
			// Init the colours
			MColorArray aobVertexColors;
			for(unsigned j=0; j<asVertexColoursOfMeshsColorSets[0].length(); j++)
			{
				aobVertexColors.append(0.0f, 0.0f, 0.0f, 1.0f);
			}

			// Fill in each channel
			for(int iC = 0; iC<4; iC++)
			{
				const rsmRunTimeVertexColourDescription* pobRTVCDescription = pobTemplate->GetRunTimeVertexColourDescriptions().GetDescription(i);
				MString strSourceForChannel = pobRTVCDescription->GetChannelSource(iC);
				MString strSourceNameForChannel = strSourceForChannel.substring(0, strSourceForChannel.index('.') - 1);
				MString strSourceChannelForChannel = strSourceForChannel.substring(strSourceForChannel.index('.') + 1, strSourceForChannel.length());

				// Find source channel
				int iSourceChannel = 0;
				if(strSourceChannelForChannel == "r")
				{
					iSourceChannel = 0;
				}
				if(strSourceChannelForChannel == "g")
				{
					iSourceChannel = 1;
				}
				if(strSourceChannelForChannel == "b")
				{
					iSourceChannel = 2;
				}
				if(strSourceChannelForChannel == "a")
				{
					iSourceChannel = 3;
				}

				// Set default values
				const rsmSourceVertexColourDescription* pobSVCDescription = pobTemplate->GetSourceVertexColourDescriptions().GetDescription(ConstString(strSourceNameForChannel.asChar()));
				if(pobSVCDescription)
				{
					Vector4	obDefaultColour = pobSVCDescription->GetDefaultColour();
					for(unsigned j=0; j<asVertexColoursOfMeshsColorSets[0].length(); j++)
					{
						aobVertexColors[j][iC] = obDefaultColour[iSourceChannel];
					}
				}

				// Should I override with Maya data?
				bool bIgnoreMe = false;
				//for(unsigned iI=0; iI<astrVertexColourSetsToIgnore.length(); iI++)
				//{
				//	if(astrVertexColourSetsToIgnore[iI] == strSourceNameForChannel)
				//	{
				//		bIgnoreMe = true;
				//		break;
				//	}
				//}
				if(!bIgnoreMe)
				{
					// Find source set
					int iSourceIndex = -1;
					for(unsigned j=0; j<asNamesOfMeshsColorSets.length(); j++)
					{
						if(asNamesOfMeshsColorSets[j] == strSourceNameForChannel)
						{
							// Bingo 
							iSourceIndex = j;
							break;
						}
					}

					if(iSourceIndex > -1)
					{
						// Set values
						for(unsigned j=0; j<asVertexColoursOfMeshsColorSets[0].length(); j++)
						{
							aobVertexColors[j][iC] = asVertexColoursOfMeshsColorSets[iSourceIndex][j][iSourceChannel];
						}
					}
				}
			}

			obAObDestinationVertexColourSets.push_back(aobVertexColors);
		}
	}
}



rexResult rexMayaUtility::ConvertMayaMeshNodeIntoGenericMesh( rexObjectGenericMesh *outputMesh, 
															 const MDagPath& dp, 
															 int boneIndex, 
															 const Matrix44* boneMatrix, 
															 int lodGroupIndex, 
															 int lodLevel, 
															 float lodThreshold, 
															 const atString* groupID, 
															 int groupIndex,
															 bool checkNumVertWeights,
															 bool exportTriangles,
															 rexMayaSetCollection::SetMemberGroup* pSets,
															 bool dataRelativeToTaggedRexRoot)
{
	Displayf("%s", dp.fullPathName().asChar());
	// this must be set in higher level code!
	Assert(rexObjectGenericMesh::VertexInfo::sm_MaxMatricesPerVertex>=1);

	rexResult result(rexResult::PERFECT);

	rexMayaSetCollection	&setCollection = rexMayaSetCollectionSingleton::GetInstance();

	if( !outputMesh )
	{
		return rexResult(rexResult::ERRORS, rexResult::INVALID_INPUT);
	}
	if( dp.apiType() != MFn::kMesh )
	{
		return rexResult(rexResult::ERRORS, rexResult::TYPE_INCOMPATIBLE);
	}

	MDagPath parentDP( dp );
	parentDP.pop();

	// check for shapenode names that are potentially too long.
	if(dp.fullPathName().length() > 1024)
	{
		//Only copy the first 256 characters of the name, otherwise we can end up overruning the result message buffer...
		char nameBuf[256];
		strncpy(nameBuf, dp.fullPathName().asChar(), 256);
		if(dp.fullPathName().length() >= 256)
			nameBuf[255] = 0x00;
		else
			nameBuf[dp.fullPathName().length()] = 0x00;

		result.Combine(rexResult::ERRORS);
		result.AddMessage("Detected a node with a full Maya path longer than 1024 characters (%d long). Node = '%s'", dp.fullPathName().length(), nameBuf);
		return result;
	}

	outputMesh->m_HasMultipleTextureCoordinates = false;
	outputMesh->SetMeshName(DagPathToName( parentDP )); // this way, name better matches what artists see in outliner with default settings
	outputMesh->m_FullPath = parentDP.fullPathName().asChar(); // this way, name better matches what artists see in outliner with default settings
	outputMesh->m_BoneIndex = boneIndex;
	outputMesh->m_BoneCount = rexMayaUtility::GetRootSkeletonNumBones();
	outputMesh->m_GroupIndex = groupIndex;
	outputMesh->m_LODThreshold = lodThreshold;
	if( groupID )
		outputMesh->m_GroupID = *groupID;

	outputMesh->m_LODGroupIndex = lodGroupIndex;
	outputMesh->m_LODLevel = lodLevel;

	MObject dpObj = dp.node();
	Matrix34 unTransformMatrix( M34_IDENTITY );
	if( boneMatrix )
	{
		boneMatrix->ToMatrix34( unTransformMatrix );		
		outputMesh->m_Matrix.Set( unTransformMatrix );
	}

	outputMesh->m_ParentBoneMatrix.Identity();

	MDagPath boneDP( parentDP );

	MStatus popStatus = MStatus::kSuccess;
	if (dataRelativeToTaggedRexRoot)
	{
		MStatus status = MStatus::kSuccess;
		bool isTaggedRexRoot = false;
		while (!isTaggedRexRoot)
		{
			popStatus = boneDP.pop();
			if (popStatus != MStatus::kSuccess)
			{
				break;
			}

			MObject node = boneDP.node(&status);
			if (status == MStatus::kSuccess)
			{
				isTaggedRexRoot = IsTaggedRootRageRexExportDataNode(node);
			}

			if (boneDP.apiType() == MFn::kJoint && popStatus == MStatus::kSuccess)
			{
				break;
			}
		}
	}
	else
	{
		while ((boneDP.apiType() != MFn::kJoint && popStatus == MStatus::kSuccess))
		{
			popStatus = boneDP.pop();
		}
	}

	if (popStatus == MStatus::kSuccess)
	{
		MMatrix boneMat = boneDP.inclusiveMatrix();
		Displayf("My parent is: %s", boneDP.fullPathName().asChar());
		GetMatrix34FromMayaMatrix(outputMesh->m_ParentBoneMatrix, boneMat);
	}

	DpToLimitInfoTable limitInfoTable;

	MObject skinClusterObj;
	bool isSkin = FindSkinCluster(dp, skinClusterObj);
	atString savePoseName;
	if( isSkin )
	{
		MStatus status;

		//Temporarily disable the limits on all of the joints which will be adjusted
		//by the ToggleBindPose call.  This eliminates the problem of jonts that are unable
		//to reach the target rotations set in that method due to their limit data.
		MFnSkinCluster fnCluster(skinClusterObj);
		MDagPathArray bonePaths;
		unsigned int affBoneCount = fnCluster.influenceObjects(bonePaths, &status);
		if(affBoneCount)
		{
			CollectNodeLimitInfo(bonePaths, limitInfoTable);
			SetNodeLimitEnables(bonePaths, false);
		}

		// make sure undo is enabled:
		outputMesh->m_IsSkinned = true;

		ToggleBindPose( skinClusterObj, true, savePoseName );
	}		

	MFloatPointArray vList;
	MFloatVectorArray nList;

	MFnMesh mesh( dp );
	mesh.getPoints(vList, MSpace::kWorld);
	mesh.getNormals(nList, MSpace::kWorld);

	MPoint center = mesh.boundingBox().center();

	outputMesh->m_Center.x = (float)center.x;
	outputMesh->m_Center.y = (float)center.y;
	outputMesh->m_Center.z = (float)center.z;

	MFnTransform fnT1( parentDP );
	MPoint rp = fnT1.rotatePivot( MSpace::kWorld );
	outputMesh->m_PivotPoint.x = (float)rp.x;
	outputMesh->m_PivotPoint.y = (float)rp.y;
	outputMesh->m_PivotPoint.z = (float)rp.z;

	Matrix34 im;
	rexMayaUtility::GetMatrix34FromMayaMatrix( im, dp.inclusiveMatrix() );
	im.Transform( outputMesh->m_Center );

	int vertCount = vList.length();

	if (!vertCount)
	{
		result.Set(rexResult::ERRORS, rexResult::TYPE_INCOMPATIBLE);
		result.AddMessage("Object %s has no verts this seems wrong is it a valid object?", outputMesh->m_FullPath.c_str());
		Errorf("Object %s has no vertices and is trying to be exported as a mesh. Please check your scene.", outputMesh->m_FullPath.c_str());
		return result;
	}

	for( int a = 0; a < vertCount; a++ )
	{
		rexObjectGenericMesh::VertexInfo& vertInfo = outputMesh->m_Vertices.Grow(vertCount);
		vertInfo.m_Position.Set( (float) vList[ a ].x, (float) vList[ a ].y, (float) vList[ a ].z );
		unTransformMatrix.UnTransform( vertInfo.m_Position );
	}

	int normalCount = nList.length();

	int zeroLenCount = 0;
	for( int a = 0; a < normalCount; a++ )
	{
		Vector3& normal = outputMesh->m_Normals.Grow(normalCount);
		normal.Set( (float) nList[ a ].x, (float) nList[ a ].y, (float) nList[ a ].z );
		unTransformMatrix.UnTransform3x3( normal );
		normal.Normalize();
		if (normal.IsZero()) 
		{
			//Zero-length normals will crash some resourcing tools, so patch in a non-zero length normal..
			if(a)
			{
				normal.Set( outputMesh->m_Normals[a-1] );	
			}
			else
			{
				normal.Set(0.0f, 1.0f, 0.0f);
			}
			zeroLenCount++;
		}
	}

	if ( zeroLenCount ) 
	{
		MString zlMeshfullName = dp.fullPathName();
		Warningf("Zero Length Normals found while converting mesh '%s' : %d out of %d", zlMeshfullName.asChar(), zeroLenCount, normalCount);
	}

	int instanceNum = 0;
	if( dp.isInstanced() )
	{
		instanceNum = dp.instanceNumber();
	}

	MObjectArray shaders;
	MIntArray indices;
	mesh.getConnectedShaders(instanceNum, shaders, indices);

	int numuvsets = mesh.numUVSets();
	MStringArray uvsetnames;
	if (numuvsets)
		mesh.getUVSetNames(uvsetnames);

	atArray <ConstString> inputNames;
	atArray <ConstString> outputNames;

	inputNames.Resize(uvsetnames.length());
	for (u32 i=0; i<uvsetnames.length(); i++)
	{
		inputNames[i] = uvsetnames[i].asChar();
	}

	// Get source vertex colours
	MStringArray asNamesOfMeshsColorSets;
	mesh.getColorSetNames(asNamesOfMeshsColorSets);

	vector<MColorArray> asVertexColoursOfMeshsColorSets;
	for(unsigned i=0; i<asNamesOfMeshsColorSets.length(); i++)
	{
		MColorArray colors;
		mesh.getFaceVertexColors(colors, &asNamesOfMeshsColorSets[i]);
		asVertexColoursOfMeshsColorSets.push_back(colors);
	}


	// Add face normals as an extra "color" set as they are sometimes needed in there
	MColorArray faceNormalsAsColors;
	for(MItMeshFaceVertex itFV(dp); !itFV.isDone(); itFV.next())
	{
		// Get the "colour"
		MColor obColor;

		// Get face normal
		MVector obFaceNormal;
		mesh.getPolygonNormal(itFV.faceId(), obFaceNormal);
		obColor.r = (float)obFaceNormal.x;
		obColor.g = (float)obFaceNormal.y;
		obColor.b = (float)obFaceNormal.z;
		obColor.a = 1.0f;
		faceNormalsAsColors.append(obColor);
	}
	asNamesOfMeshsColorSets.append("faceNormal");
	asVertexColoursOfMeshsColorSets.push_back(faceNormalsAsColors);

	// On with the rest of the export
	MItMeshPolygon polyIt(dp);

	int polyCount = mesh.numPolygons();

	// [ERWIN] 
	//const bool exportTriangles = true;                 // <== make a command-line-switch out of this variable?
	int totalTriangleCount = 0;

	// determine the number of triangles in the mesh, and work with those instead

	if (exportTriangles)
	{
		for (MItMeshPolygon polygonIt(dp); !polygonIt.isDone(); polygonIt.next())
		{
			int polyTriangleCount;
			polygonIt.numTriangles(polyTriangleCount);

			totalTriangleCount += polyTriangleCount;
		}
	}
	else
	{
		// if we are not exporting triangulized, pretend the number of triangles equals the number of polyons
		totalTriangleCount = polyCount;
	}

	int polyIndex = 0;
	// [/ERWIN]

	//	Displayf( "MESH %s", (const char*)outputMesh->m_Name );

	//Build up a mapping for each polygon to any set is a member of that has been tagged to a REX export node.
	atMap<int, atString> polyIndexToSetPostfixName;
	if(pSets && pSets->GetCount())
	{
		while(!polyIt.isDone())
		{
			atString setPostfixNames;
			
			for(int setIdx=0; setIdx<pSets->GetCount(); setIdx++)
			{
				bool bAddPostfixName = false;
				rexMayaSetCollection::SetMemberInfo &smi = (*pSets)[setIdx];

				if( (smi.m_ComponentData != MObject::kNullObj) &&
					(smi.m_ComponentFaceIndices.GetCount() != 0) )
				{
					//Only a particular faces of this object are tagged in the set, so check
					//if the current face is in the list of set faces.
					if(smi.m_ComponentFaceIndices.Find(polyIt.index()) != -1)
						bAddPostfixName = true;
				}
				else
				{
					//The entire object is marked as a member of the set.
					bAddPostfixName = true;
				}
				
				if(bAddPostfixName)
				{
					//TODO set the post fix name to a comma separated list of the names of the REX Export nodes
					//this set has been tagged to.
					const int		smisetIdx = smi.m_SetIndex;
					const int		rexNodeCount = setCollection.GetNumberOfConnectedREXNodes(smisetIdx);
					for(int rexIdx=0; rexIdx<rexNodeCount; rexIdx++)
					{
						setPostfixNames += ",";
						setPostfixNames += setCollection.GetConnectedREXNodeName(smisetIdx, rexIdx);
					}
				}
			}

			if(setPostfixNames.GetLength())
			{
				polyIndexToSetPostfixName.Insert(polyIt.index(), setPostfixNames);
			}
			polyIt.next();
		}
		polyIt.reset();
	}
		
	atMap<MObject*,int>		mayaShaderObjToMtlmap;
	atMap<atString, int>	shaderNameToMtlmap;

	for(int p=0; (p < polyCount) && !polyIt.isDone(); p++, polyIt.next() )
	{
		// [ERWIN]
		MIntArray triangleVertexIndices;
		int polyTriangleCount = 1;              // if we are not exporting triangulized, pretend the polygon is made up of one big "triangle"

		if (exportTriangles)
		{
			// get triangle-vertex-indices for this polygon

			MPointArray dummyTrianglePoints;		// this is discareded really
			polyIt.getTriangles(dummyTrianglePoints, triangleVertexIndices);

			// getTriangles() returns the triangle-vertex-indices in local-triangle space
			// convert triangle-vertex-indices to polygon-vertex-indices

			MIntArray polygonVertexIndices;
			polyIt.getVertices(polygonVertexIndices);

			for (unsigned i = 0; i < triangleVertexIndices.length(); ++i)
			{
				unsigned n;
				for (n = 0; n < polygonVertexIndices.length(); ++n)
				{
					if (triangleVertexIndices[i] == polygonVertexIndices[n])
					{
						triangleVertexIndices[i] = n;
						break;
					}
				}

				if (n == polygonVertexIndices.length())
				{
					result.Combine( rexResult::ERRORS );
					result.AddMessage( "failed to map triangle-vertex-index to polygon-vertex-index");
					return result;
				}
			}

			AssertMsg(triangleVertexIndices.length()%3 == 0, "triangleVertexIndices-size should be a multiple of 3");
			polyTriangleCount = triangleVertexIndices.length()/3;
		}

		// if we are not exporting triangulized, polyTriangleCount will be 1, so we go through the for-loop exactly once, like in the original code
		for (int triangleIndex = 0; triangleIndex < polyTriangleCount; ++triangleIndex, ++polyIndex)
		{
			int vertexCount = exportTriangles ? 3 : polyIt.polygonVertexCount();
			// [/ERWIN]

			if( vertexCount < 3 )
				continue;

			int mtlIndex = -1;
			//		Displayf( "\tPOLY %d", p );
			// [ERWIN]
			//		    rexObjectGenericMesh::PrimitiveInfo& primInfo = outputMesh->m_Primitives.Grow(polyCount);
			rexObjectGenericMesh::PrimitiveInfo& primInfo = outputMesh->m_Primitives.Grow(totalTriangleCount);
			// [/ERWIN]
			// gather face normal info
			MVector faceNormal;
			polyIt.getNormal( faceNormal, MSpace::kWorld );
			primInfo.m_FaceNormal.Set( (float)faceNormal.x, (float)faceNormal.y, (float)faceNormal.z );
			unTransformMatrix.UnTransform3x3( primInfo.m_FaceNormal );
			if(indices[p] >= 0)
			{
				//So as not to not slow down mesh exporting when set tagging hasn't been employed split the material
				//gathering for each face into either a name (with set tag fixup) based lookup for use with maya set data,
				//or the older (and faster) lookup based on the maya shader object.
				if(pSets && pSets->GetCount())
				{
					//Get any postfix name that should be applied to the material based on its set membership.
					const char* mtlNamePostfix = "";
					atString* atMtlNamePostfix = polyIndexToSetPostfixName.Access(polyIt.index());
					if(atMtlNamePostfix)
						mtlNamePostfix = (const char*)(*atMtlNamePostfix);
					
					MStatus status;
					MFnSet shader(shaders[indices[p]],&status);
					if (status.error())
					{
						status.perror("Not an FnSet");
					}
					else
					{
						//Get the material info for the shader assigned to the face.  Note that 'GetMaterialInfo' uses a cache
						//to determine if the details of the material have already been extracted from the shader, so the performance
						//hit to call this per face should be minimal.
						rexObjectGenericMesh::MaterialInfo& matInfo = outputMesh->m_Materials.Grow();
						if( GetMaterialInfo( matInfo, shader, mtlNamePostfix) )
						{
							//If the resulting material is already in the output mesh then use the existing 
							//material.
							int* matMapIndex = shaderNameToMtlmap.Access(matInfo.m_Name);
							if(!matMapIndex)
							{
								mtlIndex = outputMesh->m_Materials.GetCount() - 1;
								shaderNameToMtlmap.Insert(matInfo.m_Name, mtlIndex);
							}
							else
							{
								//Clear out the memory referenced by matInfo
								memset(&matInfo, 0, sizeof(rexObjectGenericMesh::MaterialInfo));
								matInfo.initDefaults();

								//Pop the material list
								outputMesh->m_Materials.Pop();

								mtlIndex = *matMapIndex;
							}
						}
						else
						{
							//Clear out the memory referenced by matInfo
							memset(&matInfo, 0, sizeof(rexObjectGenericMesh::MaterialInfo));
							matInfo.initDefaults();

							//Failed ot gather the material info from the shader assigned to the face
							outputMesh->m_Materials.Pop();
						}
					}
				}
				else 
				{
					//Set based tagging isn't in use when converting this mesh, so just use the faster lookup against the maya shader object.
					int* matMapIndex = mayaShaderObjToMtlmap.Access(&shaders[indices[p]]);
					if(!matMapIndex)
					{
						MStatus status;
						MFnSet shader(shaders[indices[p]],&status);
						if (status.error())
						{
							status.perror("Not an FnSet");
						}
						else
						{
							rexObjectGenericMesh::MaterialInfo& matInfo = outputMesh->m_Materials.Grow();
							
							if( GetMaterialInfo( matInfo, shader, "") )
							{
								mtlIndex = outputMesh->m_Materials.GetCount() - 1;
								mayaShaderObjToMtlmap.Insert(&shaders[indices[p]], mtlIndex);
							}
							else
							{
								//Clear out the memory referenced by matInfo
								memset(&matInfo, 0, sizeof(rexObjectGenericMesh::MaterialInfo));
								matInfo.initDefaults();

								outputMesh->m_Materials.Pop();
							}
						}
					}
					else
					{
						mtlIndex = *matMapIndex;
					}
				}
			}//End if(indices[p] >= 0)

			primInfo.m_MaterialIndex = mtlIndex;

			atArray <int> materialDataUvIndex;
			atArray <Vector2> materialData;

			// Note: This used to just be called obAObDestinationVertexColourSets, but I added the "Tmp"
			// part as further down, we will no longer use this but a vector<const MColorArray*> that points
			// to its elements, for performance reasons. /FF
			vector<MColorArray> obAObDestinationVertexColourSetsTmp;

			if( mtlIndex >= 0 )
			{
				rexObjectGenericMesh::MaterialInfo& matInfo = outputMesh->m_Materials[ mtlIndex ];

				// Check to see if we are using rageMaterials
				if (matInfo.m_MtlFileName)
				{
					rageShaderMaterial * rageMaterial = matInfo.GetMaterial();
					if (rageMaterial)
					{
						// Order the UVs correctly
						// This puts them in the right order and the correct amount
						if (rageMaterial->UpdateUvSetNames(inputNames, outputNames))
						{
							numuvsets = outputNames.GetCount();
							uvsetnames.clear();
							for (int i=0; i<outputNames.GetCount(); i++)
							{
								uvsetnames.append(MString((const char *)outputNames[i]));
							}
						}

						// Get the material data into the correct uv set
						for (int i=0; i<rageMaterial->GetParamCount(); i++)
						{
							int index = rageMaterial->GetParam(i)->GetParamDescription()->GetMaterialDataUvSetIndex();
							if (index >= 0)
							{
								materialDataUvIndex.Grow() = index;
								rageMaterial->GetParam(i)->CopyValueIntoVector2(materialData.Grow());

								// If this index is greater then the number of uvsets increase the number
								numuvsets = ((index+1) > numuvsets) ? (index+1) : numuvsets;
							}
						}

						// Juggle the vertex colours so they are packed as the shader expects them to be
						const char* pcTemplatePathAndFilename = rageMaterial->GetTemplatePathAndFileName();
						// Displayf("pcTemplatePathAndFilename = %s", pcTemplatePathAndFilename);
						const shaderMaterialGeoTemplate* pobTemplate = SHADER_MATERIAL_GEO_MANAGER.LoadShaderMaterialGeoTemplate(pcTemplatePathAndFilename);
						
						// Pack the VertexColours correctly
						rexMayaUtility::PackVertexColours(pobTemplate, asNamesOfMeshsColorSets, asVertexColoursOfMeshsColorSets, obAObDestinationVertexColourSetsTmp);
					}
				}
				// [ERWIN]
				//			    matInfo.m_PrimitiveIndices.PushAndGrow( p,polyCount );
				matInfo.m_PrimitiveIndices.PushAndGrow(polyIndex, totalTriangleCount);
				// [/ERWIN]
			}
			else
			{
				mtlIndex = -1;
			}

			// The code block further down that does the push_back() if
			// obAObDestinationVertexColourSetPtrs is empty was very expensive, because
			// for each polygon being exported, the whole MColorArray with a value
			// per vertex(?) was copied. To avoid this, we now create an array of
			// pointer to MColorArray objects instead, so that we can just add a pointer
			// instead of creating a whole copy. We do need to be careful to not
			// modify anything these pointers point to, though, but I believe I
			// have checked all current possibilities for this. /FF
			vector<const MColorArray*> obAObDestinationVertexColourSetPtrs;
			const int numVertexColourSets = obAObDestinationVertexColourSetsTmp.size();
			for(int i = 0; i < numVertexColourSets; i++)
			{
				obAObDestinationVertexColourSetPtrs.push_back(&obAObDestinationVertexColourSetsTmp[i]);
			}

			if((obAObDestinationVertexColourSetPtrs.size() == 0) && (asVertexColoursOfMeshsColorSets.size() > 0))
			{
				// Uh-oh, something bad happened, I have no vertex colours to export, but the mesh has some
				// so just copy them across, ignoring the special cases
				for(unsigned int i=0;i<asVertexColoursOfMeshsColorSets.size();i++)
				{
					// Is a special case?
					if(asNamesOfMeshsColorSets[i] != "faceNormal")
					{
						// Not a special case, so copy it across
						// - Update: as mentioned above, we no longer actually copy the MColorArray, because
						//   that was very expensive to do for each polygon. Instead, we just add a pointer.
						//   Nothing here should modify the asVertexColoursOfMeshsColorSets array,
						//   so this should be fine. /FF
						obAObDestinationVertexColourSetPtrs.push_back(&asVertexColoursOfMeshsColorSets[i]);
					}
				}
			}

			for( int v = 0; v < vertexCount; v++ )
			{
				// [ERWIN]
				int polygonVertexIndex = exportTriangles ? triangleVertexIndices[v+triangleIndex*3] : v;

				int vID = polyIt.vertexIndex(polygonVertexIndex);
				int nID = polyIt.normalIndex(polygonVertexIndex);
				// [/ERWIN]

				//			Assert( vID < vertCount && nID < normalCount );  // n8 -- according to maya docs, this should never happen...but it does
				if( vID < vertCount && nID < normalCount )
				{	
					rexObjectGenericMesh::AdjunctInfo& adjInfo = primInfo.m_Adjuncts.Grow(3);
					adjInfo.m_VertexIndex = vID;
					adjInfo.m_NormalIndex = nID;

					for( int a = 0; a < numuvsets; a++ )
					{
						// Check to see if we have material data to add
						bool foundData = false;
						for (int i=0; i < materialDataUvIndex.GetCount(); i++)
						{
							if (a == materialDataUvIndex[i])
							{
								// Push the material data into the texture coord
								char uvSetName[64];
								sprintf(uvSetName, "materialData%d", i);

								adjInfo.m_TextureCoordinateSetNames.PushAndGrow( atString( uvSetName ), (u16) numuvsets );
								Vector2& tc = adjInfo.m_TextureCoordinates.Grow((u16) numuvsets);
								if( a > 0 )
									outputMesh->m_HasMultipleTextureCoordinates = true;

								tc = materialData[i];

								//Sanity check the UV's to make sure they are not out of range.
								if((fabs((float)tc.x) > 4096.0f) || (fabs((float)tc.y) > 4096.0f))
								{
									MString errNodePath = dp.fullPathName();
									result.Combine(rexResult::ERRORS);
									result.AddMessage("Found UV coordinates out of the recommended range (-4096, 4096 ) assigned to mesh '%s' ", errNodePath.asChar() );
									return result;
								}

								foundData = true;
								break;
							}
						}

						// If we did not find material data then add the texture coord.
						if (!foundData)
						{
							adjInfo.m_TextureCoordinateSetNames.PushAndGrow( atString( uvsetnames[ a ].asChar() ), (u16) numuvsets );
							Vector2& tc = adjInfo.m_TextureCoordinates.Grow((u16) numuvsets);
							if( a > 0 )
								outputMesh->m_HasMultipleTextureCoordinates = true;
							tc.Set( 0.0f, 0.0f );
							// [ERWIN]
							mesh.getPolygonUV( p, polygonVertexIndex, tc.x, tc.y, &uvsetnames[a] );
							// [/ERWIN]

							//Sanity check the UV's to make sure they are not out of range.
							if((fabs((float)tc.x) > 4096.0f) || (fabs((float)tc.y) > 4096.0f))
							{
								MString errNodePath = dp.fullPathName();
								result.Combine(rexResult::ERRORS);
								result.AddMessage("Found UV coordinates out of the recommended range (-4096, 4096 ) assigned to mesh '%s' ", errNodePath.asChar() );
								return result;
							}
						}
					}

					if ( obAObDestinationVertexColourSetPtrs.size() == 0)
					{
						adjInfo.m_ColorSetNames.PushAndGrow(atString("Default"));
						Vector4& color = adjInfo.m_Colors.Grow();
						color.Set(1.0f,1.0f,1.0f,1.0f);
					}
					else
					{
						char acPackedColourSetName[64];
						for( unsigned a = 0; a < obAObDestinationVertexColourSetPtrs.size(); a++)
						{
							const MColorArray &colorArrayA = *obAObDestinationVertexColourSetPtrs[a];

//							adjInfo.m_ColorSetNames.PushAndGrow(atString(cpvSetNames[a].asChar()));
							sprintf(acPackedColourSetName, "PackedColorSet%d", a);
							adjInfo.m_ColorSetNames.PushAndGrow(atString(acPackedColourSetName));
							Vector4& color = adjInfo.m_Colors.Grow();
							color.Set(1.0f,1.0f,1.0f,1.0f);
							int ci = -1;
//							mesh.getFaceVertexColorIndex(p,polygonVertexIndex,ci, &cpvSetNames[a]);
							mesh.getFaceVertexColorIndex(p,polygonVertexIndex,ci);
							if( ci != -1 )
							{
								//color.x = (cpvSets[a][ci].r == -1) ? 1.0f : cpvSets[a][ci].r;
								//color.y = (cpvSets[a][ci].g == -1) ? 1.0f : cpvSets[a][ci].g;
								//color.z = (cpvSets[a][ci].b == -1) ? 1.0f : cpvSets[a][ci].b;
								//color.w = (cpvSets[a][ci].a == -1) ? 1.0f : cpvSets[a][ci].a;	
								color.x = (colorArrayA[ci].r == -1) ? 1.0f : colorArrayA[ci].r;
								color.y = (colorArrayA[ci].g == -1) ? 1.0f : colorArrayA[ci].g;
								color.z = (colorArrayA[ci].b == -1) ? 1.0f : colorArrayA[ci].b;
								color.w = (colorArrayA[ci].a == -1) ? 1.0f : colorArrayA[ci].a;	
							}
						}
					}

					//Vector4& color = adjInfo.m_Color;
					//color.Set(1.0f,1.0f,1.0f,1.0f);

					//if(colors.length())
					//{
					//	int ci = -1;
					//	// [ERWIN]
					//	mesh.getFaceVertexColorIndex(p,polygonVertexIndex,ci);
					//	// [/ERWIN]
					//	if(ci != -1)
					//	{
					//		color.x = (colors[ci].r == -1) ? 1.0f : colors[ci].r;
					//		color.y = (colors[ci].g == -1) ? 1.0f : colors[ci].g;
					//		color.z = (colors[ci].b == -1) ? 1.0f : colors[ci].b;
					//		color.w = (colors[ci].a == -1) ? 1.0f : colors[ci].a;						
					//	}
					//}

					//				Displayf( "\t\tVERT %d COLOR (%f, %f, %f, %f)", v, color.x, color.y, color.z, color.w );
				}
				else
				{
					result.Combine( rexResult::ERRORS );
					result.AddMessage( "Mesh %s has adjunct whose vert index or normal index is too large for array!  Cannot Export!", (const char*) DagPathToName( parentDP ) );
					return result;
				}
			}
		}
	}

	// Attach skinning info, if this mesh is a skin.
	if( isSkin )
	{
		MFnSkinCluster fnCluster(skinClusterObj);

		MDagPathArray bonePaths;
		MStatus status;
		unsigned affectingBones = fnCluster.influenceObjects(bonePaths, &status);

		if( affectingBones >= 1 )
		{
			MDagPath root = bonePaths[0];
			FindChainRoot(bonePaths[0].partialPathName().asChar(), root);

			MItDag itDag;
			itDag.reset(root, MItDag::kDepthFirst, MFn::kJoint);

			atArray<int> skinnedJointIndexList;
			atArray<int> skinnedJointIndexListMaya;
			atArray<MDagPath> skinnedJointDagPathList;

			while( !itDag.isDone() )
			{
				// Get each dagpath for each shape item.
				MDagPath dagpath;
				itDag.getPath(dagpath);

#ifdef DEBUGGING_JOINTS_CALLED_UNDERSCORE_NULL
				Displayf("%s %d dagpath = %s", __FILE__, __LINE__, dagpath.fullPathName().asChar());
				for(unsigned uHamster=0; uHamster<bonePaths.length(); uHamster++)
				{
					Displayf("%s %d bonePaths[%d] = %s", __FILE__, __LINE__, uHamster, bonePaths[uHamster].fullPathName().asChar());
				}
#endif
				int boneDagPathIndex=FindDagPathInDagPathArray(dagpath, bonePaths); // check if it's an influencing bone
#ifdef DEBUGGING_JOINTS_CALLED_UNDERSCORE_NULL
				Displayf("%s %d boneDagPathIndex = %d", __FILE__, __LINE__, boneDagPathIndex);
#endif
				int skinnedIndex = GetRootSkeletonBoneIndex(dagpath.fullPathName().asChar());
#ifdef DEBUGGING_JOINTS_CALLED_UNDERSCORE_NULL
				Displayf("%s %d skinnedIndex = %d", __FILE__, __LINE__, skinnedIndex);
#endif
				if( boneDagPathIndex!=-1)
				{
					// make sure we have a bone that is actually part of the exported skeleton:
					if (skinnedIndex == -1)
					{
						result.Combine( rexResult::ERRORS );
						result.AddMessage( "Vertices are influenced by the bone '%s', but that bone is not in the exported skeleton - Therefore I cannot export!", dagpath.fullPathName().asChar() );
						return result;
					}
					skinnedJointIndexList.PushAndGrow(skinnedIndex);
					skinnedJointIndexListMaya.PushAndGrow(boneDagPathIndex);
					skinnedJointDagPathList.PushAndGrow(dagpath);
				}

				// Get the next shape item.
				itDag.next();
			}

			// fillout skin bone matrices:
			if( affectingBones )
			{
				unsigned numBones=GetRootSkeletonNumBones();
				outputMesh->m_SkinBoneMatrices.Reset();
				outputMesh->m_SkinBoneMatrices.Resize(numBones);

				// initialize all matrices to identity:
				for( unsigned int a = 0; a < numBones; a++ )			
					outputMesh->m_SkinBoneMatrices[a].Identity();

				// set matrices based on bones that actually influenced our mesh:
				for( unsigned int a = 0; a < (unsigned)skinnedJointDagPathList.GetCount(); a++ )			
				{
					Matrix44 m = rexMayaUtility::GetSkeletonWorldMatrix( skinnedJointDagPathList[a] );
					Matrix34& new34 = outputMesh->m_SkinBoneMatrices[skinnedJointIndexList[a]];
					m.ToMatrix34( new34 );
					new34.Normalize();
				}
			}

			atArray <int> vertsWithTooManyWeights;

			// Get affecting bones info.
			for( MItGeometry gIter(dp); !gIter.isDone(); gIter.next() )
			{
				MObject vert = gIter.component(&status);
				if( status.error() )
					continue;

				// Get the weights for this vertex (one per influence object)
				MFloatArray wList;
				unsigned influenceCount = 0;
				status = fnCluster.getWeights(dp, vert, wList, influenceCount);
				if( status.error() )
					continue;

				if( influenceCount > affectingBones )
					continue;

				rexObjectGenericMesh::VertexInfo& vertInfo = outputMesh->m_Vertices[ gIter.index() ];

				// resize weight arrays correctly:
				unsigned numBones=GetRootSkeletonNumBones();
				vertInfo.m_BoneIndices.Reset();
				vertInfo.m_BoneIndices.Resize(numBones);
				vertInfo.m_BoneWeights.Reset();
				vertInfo.m_BoneWeights.Resize(numBones);
				for (unsigned int b=0;b<numBones;b++)
				{
					vertInfo.m_BoneIndices[b]=-1;
					vertInfo.m_BoneWeights[b]=0.0f;
				}

				int numWeights=0;
				for( unsigned int a = 0; a < influenceCount; a++ )
				{
					vertInfo.m_BoneIndices[a]=skinnedJointIndexList[ a ];
					if (wList[skinnedJointIndexListMaya[a]]>rexObjectGenericMesh::VertexInfo::WEIGHT_TOLERANCE)
					{
						vertInfo.m_BoneWeights[a]=wList[skinnedJointIndexListMaya[a]];
						numWeights++;
					}
					else
						vertInfo.m_BoneWeights[a]=0.0f;
				}

				// keep track of it if it has too many verts:
				if (checkNumVertWeights && numWeights>rexObjectGenericMesh::VertexInfo::sm_MaxMatricesPerVertex)
				{
					vertsWithTooManyWeights.Grow()=gIter.index();
				}
			}

			// verts with too many weights:
			if (vertsWithTooManyWeights.GetCount()>0)
			{
				// select the verts:
				MString commandName="select -r ";
				atString dagName=DagPathToName( dp );

				for (int i=0;i<vertsWithTooManyWeights.GetCount();i++)
				{
					char vertWeight[32];
					sprintf(vertWeight,"%d",vertsWithTooManyWeights[i]);
					commandName += dagName;
					commandName += ".vtx[";
					commandName += vertWeight;
					commandName += "] ";
				}
				commandName += ";";

				MGlobal::executeCommand(commandName,false,true);

				// create a set with the selected verts:
				MString resultMel;
				MString setName="_ERROR_";
				setName +=  dagName;
				commandName="sets -name " + setName + ";";
				MGlobal::executeCommand(commandName,resultMel,false,true);

				result.Combine( rexResult::ERRORS );
				result.AddMessage( "Mesh %s contains %d vertices that have more than %d weights.  These vertices "
					"have been collected and placed into a set called '%s' in your Maya file.", 
					(const char*) DagPathToName( dp ), vertsWithTooManyWeights.GetCount(),
					rexObjectGenericMesh::VertexInfo::sm_MaxMatricesPerVertex, resultMel.asChar());
			}
		}

		ToggleBindPose(skinClusterObj, false, savePoseName);

		//Restore the joint limit enables that were disabled from the previous call to ToggleBindPose
		SetNodeLimitEnables(bonePaths, limitInfoTable);		
	}

	//This is a pretty bad hack, but there isn't any other way to deal with this.. basically
	//we need to store the transform of the tagged root node of the entity this mesh is a part of for use in the processing steps that may
	//occur later (i.e. mesh combination), but have no way of getting that matrix into the processors besides hanging it off of the meshes 
	//that will be passed into them...
	MDagPath taggedRootNodeDp( parentDP );
	popStatus = MStatus::kSuccess;
	bool isTaggedRootNode = false;
	while (!isTaggedRootNode)
	{
		MStatus status;

		if (taggedRootNodeDp.pop() != MStatus::kSuccess)
			break;

		MObject taggedRootNodeMObj = taggedRootNodeDp.node(&status);
		Assert(!taggedRootNodeMObj.isNull());
		
		isTaggedRootNode = IsTaggedRootRageRexExportDataNode(taggedRootNodeMObj);
	}
	
	if(taggedRootNodeDp.isValid() && isTaggedRootNode)
	{
		outputMesh->m_bTaggedRootMatrixIsValid = true;

		//HACK OF THE HACK... this is being done so objects can be exported as though the origin of the meshes world is at the 
		// tagged root matrix so then object placement in the world can be handled by external systems.
		if (dataRelativeToTaggedRexRoot)
		{
			outputMesh->m_bDataRelativeToRaggedRoot = true;
		}

		MString mstrFullPath = taggedRootNodeDp.fullPathName();
		const char* szFullPath = mstrFullPath.asChar();
		szFullPath;

		Matrix44 taggedRootWorldMatrix = GetSkeletonWorldMatrix(taggedRootNodeDp);
		taggedRootWorldMatrix.ToMatrix34(outputMesh->m_TaggedRootMatrix);
	}
	
	// Make sure the outputmesh actually contains vertices and primitives 
	if( !( outputMesh->m_Vertices.GetCount() && outputMesh->m_Primitives.GetCount() ) )
	{
		result.Combine( rexResult::ERRORS );
		result.AddMessage( "Mesh %s contains no vertices, and/or primitives!", (const char*) DagPathToName( parentDP ) );
	}	

	return result;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMayaUtility::IsTaggedRootRageRexExportDataNode(MObject& node)
{
	MStatus status = MStatus::kSuccess;
	bool retval = false;
	if (node.hasFn(MFn::kDependencyNode))
	{
		MFnDependencyNode depNode(node, &status);
		if (status == MStatus::kSuccess)
		{
			MPlugArray plugArray;
			if(depNode.getConnections(plugArray) == MStatus::kSuccess)//Otherwise there aren't any connections.
			{
				const int numPlugs= plugArray.length();
				for(int plugIdx=0; plugIdx<numPlugs; plugIdx++)
				{
					MPlug localPlug= plugArray[plugIdx];
					MPlugArray oppositePlugArray;
					localPlug.connectedTo(oppositePlugArray, true, false, &status);
					Assert(status == MStatus::kSuccess);

					const int numOppositePlugs= oppositePlugArray.length();
					for(int oppPlugIdx=0; oppPlugIdx<numOppositePlugs; oppPlugIdx++)
					{
						MPlug oppositePlug= oppositePlugArray[oppPlugIdx];
						MObject object= oppositePlug.node(&status);
						Assert(status == MStatus::kSuccess);
						Assert(object.hasFn(MFn::kDependencyNode));
						MFnDependencyNode depNode2(object, &status);
						Assert(status == MStatus::kSuccess);
						MString typeName= depNode2.typeName(&status);
						Assert(status == MStatus::kSuccess);
						if(strcmp(typeName.asChar(), "rageRexExportDataNode") == 0)
						{
							MObject attrib = oppositePlug.attribute(&status);
							Assert(status == MStatus::kSuccess);
							Assert(attrib.hasFn(MFn::kAttribute));
							MFnAttribute attr(attrib, &status);
							if (strstr(attr.shortName().asChar(), "BL_Root_RootNode_1"))
							{
								retval = true;
							}
						}
					}
				}
			}
		}
	}
	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexMayaUtility::GetMaterialInfo( rexObjectGenericMesh::MaterialInfo& matInfo, const MFnSet &shaderSet, const char *materialNamePostfix )
{

	MStatus status;
	MPlug surfaceShader = shaderSet.findPlug("surfaceShader",&status);
	if(status.error()) 
		return false;

	MPlugArray plugs;
	surfaceShader.connectedTo(plugs, true, false, &status);
	if(status.error()) 
		return false;
	if(plugs.length() != 1) 
	{
		if( plugs.length() > 0 )
			Displayf( "Material nodes with multiple shaders not supported!!!!! (material %s)", surfaceShader.name().asChar() );
		return false;  // THIS WILL NOT HANDLE MULTIPLE SHADERS IN SHADER GROUP
	}

	MObject obj = plugs[0].node();
	MFnDependencyNode matDN(obj, &status);

	atString materialName(matDN.name().asChar());
	materialName += materialNamePostfix;

	// see if the material's already in our cache:
	if (s_MayaMaterialMap)
	{
		rexObjectGenericMesh::MaterialInfo* cachedMaterial=s_MayaMaterialMap->Access(materialName);
		if (cachedMaterial)
		{
			matInfo=*cachedMaterial;
			return true;
		}
	}

	matInfo.m_Name = materialName;	

	// rageShaderMaterial support GML [SAN DIEGO]
	MString mtlFileName = "";
	MPlug mtlFileNamePlug = matDN.findPlug("MtlFilename");
	if (!mtlFileNamePlug.isNull())
	{	
		mtlFileNamePlug.getValue(mtlFileName);
		matInfo.m_MtlFileName = mtlFileName.asChar();		
	}

	bool textureOveride = false;
	MPlug textureOveridePlug = matDN.findPlug("TextureOveride");
	if (!textureOveridePlug.isNull())
	{	
		textureOveridePlug.getValue(textureOveride);	
	}

    // [ERWINVIENNA] if the maya-shader has an extra attribute with the name "PhysicMaterial" set,
    // remember the value for when we later serialize the physical-material in bound files
    MPlug physicalMaterialPlug = matDN.findPlug("PhysicMaterial", &status);
    if (status == MStatus::kSuccess)
    {
        MString physicalMaterialName;
        physicalMaterialPlug.getValue(physicalMaterialName);
        matInfo.m_PhysicsMaterialName = physicalMaterialName.asChar();
//		matInfo.m_PhysicsMaterialName+= materialNamePostfix;						//This may be necessary, but my test cases don't use the "PhysicMaterial" plug so I can't test it. In an attempt not to break anything I will keep this commented out. 
	}

	/*
	bool isAnimated = HasTextureAnimation( matDN );
    // NOTE: FIXME!! -- no automatic shader names
	matInfo.m_TypeName = isAnimated ? "rage_animated.shadert" : "rage_default.shadert";

	bool isAlpha = false;

	MPlug alphaPlug = matDN.findPlug("transparency", &status);
	
	if( !status.error() )
	{
		MPlugArray plugs;
		alphaPlug.connectedTo(plugs, true, false, &status);
		if( !status.error() && plugs.length() )
			isAlpha = true;
	}

	bool isLayeredBlendMode = false;

	MPlug colorPlug = matDN.findPlug("color", &status);
	if( !status.error() )
	{
		MPlugArray plugs;
		colorPlug.connectedTo(plugs, true, false, &status);
		if( !status.error() && plugs.length() )
		{
			MObject obj = plugs[0].node();
			
			MFnDependencyNode fnDep( obj, &status );
			if( !status.error() )
			{
				MPlug inPlug = fnDep.findPlug("inputs", &status);
				if( !status.error() )
				{
					unsigned numElems = inPlug.numElements();
					for(unsigned i = 0; i < numElems; i ++)
					{
						// In each element plug:
						// child(2) is the blend mode(blendMode).
						MPlug elemPlug = inPlug.elementByPhysicalIndex(i, &status);

						// First, check if this layer is visible or not.
						int blendMode;
						elemPlug.child(2).getValue(blendMode);
						if( blendMode != 0 )
							isLayeredBlendMode = true;
					}
				}
			}

			// Layered texture
			bool isLayered = obj.hasFn(MFn::kLayeredTexture);
			if( isLayered || obj.hasFn(MFn::kFileTexture) )
			{				
				atArray<atString> fileTextureNames;
				rexMayaUtility::GetFileTexturesFromTexNode( obj, fileTextureNames );
				int texCount = fileTextureNames.GetCount();
				if( isLayered || texCount < 2 )
				{
					for( int a = 0; a < texCount; a++ )
					{					
						atString s( rexUtility::RemoveWhitespaceCharacters( rexMayaUtility::BaseFilenameFromPath( fileTextureNames[ a ] ) ) );
						s += ".dds";
						rexValue val( s );
						matInfo.m_InputTextureFileNames.PushAndGrow( fileTextureNames[ a ], (u16) texCount );
						matInfo.m_OutputTextureFileNames.PushAndGrow( s, (u16) texCount );
					}
				}
				else
				{
					// WHAT IS THIS FOR????
					atString s( matInfo.m_Name );
					s += ".texgroup";
					rexValue val( s );
					// ---
					for( int a = 0; a < texCount; a++ )
					{					
						atString s( rexUtility::RemoveWhitespaceCharacters( rexMayaUtility::BaseFilenameFromPath( fileTextureNames[ a ] ) ) );
						s += ".dds";
						rexValue val( s );

						matInfo.m_InputTextureFileNames.PushAndGrow( fileTextureNames[ a ], (u16) texCount );
						matInfo.m_OutputTextureFileNames.PushAndGrow( s, (u16) texCount );
						atString empty = "";
						matInfo.m_TextureHints.PushAndGrow( empty, (u16) texCount );
					}
				}

				// This is bad -- forcing shaders that rage isn't setup for -- AGENT SPECIFIC!!!
//				if( texCount > 1 )
//				{
//					if( isLayeredBlendMode && !isAnimated )
//					{
//						matInfo.m_TypeName = isAlpha ? "lightmapalpha.shadert" : "lightmap.shadert";
//					}
//				}
			}
		}
	}

	matInfo.m_IsLayered = isLayeredBlendMode && ( matInfo.m_InputTextureFileNames.GetCount() > 1 );

	// This is bad -- forcing shaders that rage isn't setup for
	if( isAlpha && !matInfo.m_IsLayered )
	{
//		if( isAnimated )
//			matInfo.m_TypeName = "animated_alpha.shadert";
//		else
			matInfo.m_TypeName = "rage_alpha.shadert";
	}
	*/

	//Assign default shader values to the material.
	matInfo.m_AttributeNames.Reset();
	matInfo.m_AttributeNames.Resize(1);
	matInfo.m_AttributeValues.Reset();
	matInfo.m_AttributeValues.Resize(1);
	matInfo.m_RageTypes.Reset();
	matInfo.m_RageTypes.Resize(1);
	matInfo.m_UiHints.Reset();
	matInfo.m_UiHints.Resize(1);
	matInfo.m_TypeName = "default.sps";
	matInfo.m_AttributeNames[0] = "DiffuseTex";
	matInfo.m_RageTypes[0] = "grcTexture";
	matInfo.m_AttributeValues[0] = "none";
	matInfo.m_UiHints[0] = "none";

	//Check the extra attributes on the material node for any assigned shader parameters.
	atArray<atString> attrNames;
	rexMayaUtility::GetUserDefinedAttributeNames( obj, attrNames, false );

	unsigned int attrCount = attrNames.GetCount();//matDN.attributeCount();

	if (attrCount == 0)
	{
		MFnDependencyNode node( obj );
		MPlug plug = node.findPlug("color");
		MPlugArray array;
		plug.connectedTo(array, true, false);
		for(unsigned int i =0; i < array.length(); i++)
		{
			MFnDependencyNode node1( array[i].node());
			MPlug p = node1.findPlug("fileTextureName");
			if (!p.isNull())
			{
				MString textureFileName;
				p.getValue(textureFileName);
				matInfo.m_AttributeValues[0] = textureFileName.asChar();
				Displayf("Found file node connected to a lambert: %s", textureFileName.asChar());
				break;
			}
		}
	}

	bool isPhysicsMaterial = false;
	bool useDb = false; 
	for( unsigned int a = 0; a < attrCount; a++ )
	{
		// See if we're in the new shader parsing code...  ( __rs__ == "rage shader" )
		const int rsTagLen = 6;		// __rs__
		if ( !strncmp( attrNames[a], "__rs__", rsTagLen ) )
		{
			// New format listed below -- we use "__rs__" prefix for attributes we care about:
			//	SHORT NAME				VALUE
			//	__rs__shader			The name of the shader to use -------------------| Mutually
			//	__rs__shaderdb			The name of the shader database entry to use ----| Exclusive
			//	__rs__varcount			The number of variables listed
			//	__rs__var0_name			The name of shader variable 0
			//	__rs__var0_type			The RAGE type of shader variable 0
			//	__rs__var0_value		The artist tuned value of variable 0
			//	__rs__var0_hint			The ui hint for the particular value
			//	...						...
			//	REPEAT name, type, value for up to varcount - 1
			const char *name = attrNames[a];	
			name += rsTagLen;
			if ( strcmp( name, "shader" ) == 0 && useDb == false )
			{
				// Informs us which shader to use
				atString typeName = rexMayaUtility::FilenameFromPath( rexMayaUtility::GetAttributeValueAsString( obj, attrNames[a] ) );
				if( typeName != matInfo.m_TypeName && ( typeName.GetLength() ))
				{
					matInfo.m_TypeName = typeName;
					if ( strstr(matInfo.m_TypeName, ".fx") )
						matInfo.m_TypeName = rexUtility::BaseFilenameFromPath(matInfo.m_TypeName);
				}
			}
			else if ( strcmp( name, "shaderdb" ) == 0 ) {
				// Informs us which shader database to use
				atString typeName = rexMayaUtility::GetAttributeValueAsString( obj, attrNames[a] );
				if( typeName != matInfo.m_TypeName && ( typeName.GetLength() ))
				{
					// Strip off any preceding "/" or "\"
					const char *work = typeName;
					while ( *work == '\\' || *work == '/' ) {
						work++;
					}
					matInfo.m_TypeName = work;
				}
				useDb = true;
			}
			else if ( strcmp( name, "varcount") == 0 )
			{
				// Informs us how many variables exist so we can preallocate our arrays
				rexValue val = rexMayaUtility::GetAttributeValue( obj, attrNames[a] );
				int count = val.toInt();
				matInfo.m_AttributeNames.Reset();
				matInfo.m_AttributeNames.Resize(count);
				matInfo.m_AttributeValues.Reset();
				matInfo.m_AttributeValues.Resize(count);
				matInfo.m_RageTypes.Reset();
				matInfo.m_RageTypes.Resize(count);
				matInfo.m_UiHints.Reset();
				matInfo.m_UiHints.Resize(count);
			}
			else if ( strncmp( name, "var", 3 ) == 0 ) {
				// Now, we're into the actual variable data
				name += 3;
				char numberBuff[16];
				char *out = numberBuff;
				while ( *name != '\0' && isdigit(*name) && (out - numberBuff) < (sizeof(numberBuff) - 1) ) {
					*out++ = *name++;
				}
				*out = '\0';
				int varId = atoi(numberBuff);
				Assert("Bad variable format" && *name == '_');
				name++;		// Skip past "_" char
				if ( strcmp( name, "name" ) == 0 )
				{
					matInfo.m_AttributeNames[varId] = rexMayaUtility::GetAttributeValueAsString( obj, attrNames[a] );
				}
				else if ( strcmp( name, "type" ) == 0 )
				{
					matInfo.m_RageTypes[varId] = rexMayaUtility::GetAttributeValueAsString( obj, attrNames[a] );
				}
				else if ( strcmp( name, "value" ) == 0 )
				{
					matInfo.m_AttributeValues[varId] = rexMayaUtility::GetAttributeValue( obj, attrNames[a] );
				}
				else if ( strcmp( name, "hint" ) == 0 )
				{
					matInfo.m_UiHints[varId] = rexMayaUtility::GetAttributeValue( obj, attrNames[a] );
				}
			}
		}
		else if( !strcmpi( attrNames[a], "materialName" ) )
		{
			matInfo.m_TypeName = "PhysicsMaterial";
			isPhysicsMaterial = true;
			matInfo.m_AttributeNames.PushAndGrow( attrNames[a], (u16) attrCount );
			matInfo.m_AttributeValues.PushAndGrow( rexMayaUtility::GetAttributeValue( obj, attrNames[a] ), (u16) attrCount );
		}		
		else if( ( isPhysicsMaterial ) && attrNames[ a ] != "baseColor" )  // n8 -- i get this from method above...this is unnecessary
		{
			matInfo.m_AttributeNames.PushAndGrow( attrNames[a], (u16) attrCount );
			matInfo.m_AttributeValues.PushAndGrow( rexMayaUtility::GetAttributeValue( obj, attrNames[a] ), (u16) attrCount );
		}
	}

	// Deal with texture variables for new shader system
	const int loopCount = matInfo.m_RageTypes.GetCount();
	int texCount = 0;
	if ( loopCount ) 
	{
		// Reset texture list, we're gonna pull them from shader data
		matInfo.m_InputTextureFileNames.Reset();
		matInfo.m_OutputTextureFileNames.Reset();
		matInfo.m_TextureHints.Reset();
	}

	for (int i = 0; i < loopCount; ++i)
	{
		if ( matInfo.m_RageTypes[i] == "grcTexture" )
		{
			atString sourceFile(matInfo.m_AttributeValues[i].toString());
			atString str(rexMayaUtility::BaseFilenameFromPath(sourceFile));
			if ( str != "none" ) {
				str += ".dds";
				atString destFile = str;
				matInfo.m_InputTextureFileNames.PushAndGrow(sourceFile, (u16) ++texCount);
				matInfo.m_OutputTextureFileNames.PushAndGrow(destFile, (u16) texCount);
				matInfo.m_TextureHints.PushAndGrow(matInfo.m_UiHints[i], (u16) texCount );
			}
			matInfo.m_AttributeValues[i].SetValue( str );
		}
	}

	if (mtlFileName != "")
	{
		int texCount = 0;

		matInfo.m_InputTextureFileNames.Reset();
		matInfo.m_OutputTextureFileNames.Reset();

		rageShaderMaterial * mtl = matInfo.GetMaterial();
		if (mtl) 
		{
			for (int p = 0; p < mtl->GetParamCount(); p++)
			{
				shaderMaterialGeoParamValue * param = mtl->GetParam(p);
				if (param && param->GetType() == grcEffect::VT_TEXTURE)
				{
					// If we override this value use it from what is connected in Maya
					if (textureOveride)
					{
						MPlug plug = matDN.findPlug(param->GetParamDescription()->GetUIName());
						if (!plug.isNull())
						{
							MPlugArray connections;
							plug.connectedTo(connections, true, false);
							if (connections.length() == 1)
							{
								if (connections[0].node().apiType() == MFn::kFileTexture)
								{
									MFnDependencyNode textureNode(connections[0].node());
									MPlug filenamePlug = textureNode.findPlug("fileTextureName");
									MString value;
									filenamePlug.getValue(value);

									if (value.length() != 0)
									{
										((shaderMaterialGeoParamValueTexture *)param)->SetValue(value.asChar());
									}
								}
							}
						}
					}

					atString sourceFile(((shaderMaterialGeoParamValueTexture *)param)->GetValue());

					ConstString textureName;
					((shaderMaterialGeoParamValueTexture *)param)->GetTextureName(textureName);
					atString destFile(textureName);

					matInfo.m_InputTextureFileNames.PushAndGrow(sourceFile, (u16) ++texCount);
					matInfo.m_OutputTextureFileNames.PushAndGrow(destFile, (u16) texCount);						
				}
			}
		}
	}

	if (s_MayaMaterialMap)
		s_MayaMaterialMap->Insert(matInfo.m_Name,matInfo);

	return true;
}


/////////////////////////////////////////////////////////////////////////////////////////////////

static bool IsAdded(const char *texFileName, const atArray<atString>& texFileList)
{
	int count = texFileList.GetCount();
	if( count == 0 )
		return false;

	for( int i = 0; i < count; i++ )
	{
		if( texFileName == texFileList[i] )
			return true;
	}

	return false;
}

void rexMayaUtility::GetFileTexturesFromTexNode( MObject obj, atArray<atString>& list )
{
	if( obj.apiType() == MFn::kLayeredTexture )
	{
		GetFileTextureFromLayered(obj, list);
	}
	else if( obj.apiType() == MFn::kFileTexture )
	{
		AddFileTexture(obj, list);
	}
}


void rexMayaUtility::GetFileTextureFromLayered(MObject objTex, atArray<atString>& list)
{
	MStatus status;
	MFnDependencyNode fnTex(objTex);
	MPlug inPlug = fnTex.findPlug("inputs", &status);
	if( status.error() ) 
		return;

	unsigned numElems = inPlug.numElements();
	for(unsigned i = 0; i < numElems; i ++)
	{
		// In each element plug:
		// child(0) is the color(color) which may be from a file texture.
		// child(1) is the alpha(alpha).
		// child(3) is the blend mode(blendMode).
		// child(4) is the visible(isVisible).
		MPlug elemPlug = inPlug.elementByPhysicalIndex(i, &status);

		// First, check if this layer is visible or not.
		bool visible;
		elemPlug.child(3).getValue(visible);
//		if( !visible ) continue;

		MPlugArray pArr;
		elemPlug.child(0).connectedTo(pArr, true, false, &status);
		if( pArr.length() ==1 && (!status.error()) )
			AddFileTexture(pArr[0].node(), list);
	}
}

void rexMayaUtility::AddFileTexture(MObject objTex, atArray<atString>& list)
{
	MStatus status;
	MFnDependencyNode fnTex(objTex);

	int hasFrameExtension = 0;
	MString cmd("getAttr ");
	cmd += fnTex.name();
	cmd += ".useFrameExtension";
	status = MGlobal::executeCommand( cmd, hasFrameExtension, false );

	if( hasFrameExtension && !status.error() )
	{
		MIntArray result;
		cmd.set("keyframe -q -timeChange -valueChange ");
		cmd += fnTex.name();
		cmd += ".frameExtension";
		MGlobal::executeCommand(cmd, result, false);
		int resultCount = result.length();
		if( resultCount )
		{
			int minValue = 0x7FFFFFFF, maxValue = 0x80000000;
			for( int a = 1; a < resultCount; a += 2 )
			{
				if( result[ a ] < minValue )
					minValue = result[ a ];
				if( result[ a ] > maxValue )
					maxValue = result[ a ];
			}
			
			MString texFileName;
			int growCount=(maxValue-minValue)>0?(maxValue-minValue):16;
			for( int a = minValue; a <= maxValue; a++ )
			{
				cmd = "getAttr ";
				cmd += fnTex.name();
				cmd += ".fileTextureName";
				status = MGlobal::executeCommand(cmd, texFileName, false);
				MStringArray splits;
				texFileName.split( '.', splits );
				int splitCount = splits.length();
				MString outTexFileName;
				for( int b = 0; b < splitCount; b++ )
				{
					if( b != splitCount - 2 ) 
						outTexFileName += splits[ b ];
					else
						outTexFileName += a;
					if( b != splitCount - 1 )
						outTexFileName += ".";
				}														  
				list.PushAndGrow( atString(outTexFileName.asChar()),(u16) growCount );
			}
			return;
		}
	}

	MPlug plug = fnTex.findPlug("fileTextureName", &status);
	if( status.error() ) return;

	MString tifName("");
	status = plug.getValue(tifName);
	if( tifName.length() < 5 )  
	{
//		Displayf( "WARNING!!!!!!!! File Texture Node %s has no texture assigned!!!! ", fnTex.name().asChar() );
		status = status;
	}
	if( tifName.length() > 5 && !IsAdded(tifName.asChar(), list) )
	{
		list.PushAndGrow(atString(tifName.asChar()));
	}
}

///////////////////////////////////////////////////////////////////////////////////////////

atString	rexMayaUtility::FilenameFromPath( atString path_ )
{
	MString path( path_ );
	int lastSlash = -1;
	int pathLen = path.length();
	for( int a = 0; a < pathLen; a++ )
	{
		if( path.substring( a, a ) == "/" || path.substring( a, a ) == "\\" )
			lastSlash = a;
	}
	return atString( ( lastSlash >= 0 ) ? path.substring( lastSlash + 1, pathLen - 1 ).asChar() : path.asChar() );
}

atString	rexMayaUtility::BaseFilenameFromPath( atString path )
{
	MString filename( FilenameFromPath( path ) );
	int extLen = ExtensionFromPath( path ).GetLength();
	if ( extLen )
		++extLen;
	return atString( filename.substring( 0, filename.length() - extLen - 1 ).asChar() );
}

atString	rexMayaUtility::ExtensionFromPath( atString path_ )
{
	MString path( path_ );
	int lastPeriod = -1;
	int pathLen = path.length();
	for( int a = 0; a < pathLen; a++ )
	{
		if( path.substring( a, a ) == "." )
			lastPeriod = a;
	}
	return ( lastPeriod >= 0 ) ? atString( path.substring( lastPeriod + 1, pathLen - 1 ).asChar() ) : atString();
}

atString	rexMayaUtility::PathFromPath( atString path_ )
{
	MString path( path_ );
	return atString( path.substring( 0, path.length() - FilenameFromPath( path_ ).GetLength() - 2 ).asChar() );
}

//////////////////////////////////////////////////////////////////////////////////

MDagPath	rexMayaUtility::GetSceneRootDagPath()
{
	MItDag dagIter;
	MDagPath dp = MDagPath::getAPathTo( dagIter.root() );
	return dp;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexMayaUtility::FindSkinCluster(const MDagPath& geom, MObject& cluster)
{
	MStatus status;
	MFnMesh mayaMesh(geom, &status);

	// Find the skinCluster node.
	MPlug inMeshPlug = mayaMesh.findPlug("inMesh", &status);
	if( inMeshPlug.isNull() )		// The mesh is not a skin.
	{
		return false;
	}

	MItDependencyGraph itDG(inMeshPlug, MFn::kSkinClusterFilter, MItDependencyGraph::kUpstream);
	while( !itDG.isDone() )
	{
		if( itDG.thisNode().apiType() == MFn::kSkinClusterFilter )
			break;

		itDG.next();
	}

	const MObject skinClusterObj = itDG.thisNode();
	if( skinClusterObj.apiType() != MFn::kSkinClusterFilter )
	{
		return false;
	}

	// Found it.
	cluster = skinClusterObj;
	return true;
}

int	 rexMayaUtility::FindDagPathInDagPathArray( const MDagPath& dp, const MDagPathArray& dpArray )
{
	int count = dpArray.length();
	for( int a = 0; a < count; a++ )
	{
		if( dp == dpArray[ a ] )
			return a;
	}
	return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexMayaUtility::GetAnimationStartAndEndFrames(MObject obj, int& startFrame, int& endFrame, bool considerUpstreamNodes, bool considerDownstreamNodes )
{
	const int dgDepth = 3;
	int s, e;

	if( considerDownstreamNodes )
	{
		MItDependencyGraph it( obj, MFn::kInvalid, MItDependencyGraph::kDownstream, MItDependencyGraph::kDepthFirst, MItDependencyGraph::kNodeLevel );

		bool found = 0;
		it.next();

		for( int d = 0; d < dgDepth && !it.isDone() && !found; d++ )
		{
			MObject thisObj = it.thisNode();
			MFnDependencyNode fnD( thisObj );
			GetAnimationStartAndEndFrames( fnD, s, e, false, false );
			if( s < startFrame )
				startFrame = s;
			if( e > endFrame )
				endFrame = e;

			if( considerUpstreamNodes )
			{
				MItDependencyGraph mfit( thisObj, MFn::kInvalid, MItDependencyGraph::kUpstream, MItDependencyGraph::kDepthFirst, MItDependencyGraph::kNodeLevel );
				mfit.next();
				while( !mfit.isDone() )
				{
					MObject thisObj = mfit.thisNode();
					MFnDependencyNode fnD( thisObj );
					GetAnimationStartAndEndFrames( fnD, s, e, false, false );
					if( s < startFrame )
						startFrame = s;
					if( e > endFrame )
						endFrame = e;
					mfit.next();
				}
			}

			it.next();
		}
	}
}

void rexMayaUtility::GetAnimationStartAndEndFrames( const MDagPath& dp, int& startFrame, int& endFrame, bool considerUpstreamNodes, bool considerDownstreamNodes )
{
	GetAnimationStartAndEndFrames( dp.fullPathName(), startFrame, endFrame );
	MObject obj = dp.node();

	GetAnimationStartAndEndFrames(obj,startFrame,endFrame,considerUpstreamNodes,considerDownstreamNodes);	
}

void rexMayaUtility::GetAnimationStartAndEndFrames( MFnDependencyNode& fnDep, int& startFrame, int& endFrame, bool considerUpstreamNodes, bool considerDownstreamNodes )
{
	GetAnimationStartAndEndFrames( fnDep.name(), startFrame, endFrame );
	MObject obj = fnDep.object();

	GetAnimationStartAndEndFrames(obj,startFrame,endFrame,considerUpstreamNodes,considerDownstreamNodes);
}

void rexMayaUtility::GetAnimationStartAndEndFrames( MString objName, int& startFrame, int& endFrame )
{
	startFrame = 0x7FFF;
	endFrame = -1;

	MIntArray keyframes;
	MString cmd( "keyframe -q " );
	cmd += objName;
//	Displayf( cmd.asChar() );
	MGlobal::executeCommand( cmd, keyframes, false );
	int keyframeCount = keyframes.length();
	for( int b = 0; b < keyframeCount; b++ )
	{
		if(( keyframes[ b ] < startFrame ) || ( startFrame < 0 ) )
		{
			startFrame = keyframes[ b ];
		}
		if( keyframes[ b ] > endFrame )
		{
			endFrame = keyframes[ b ];
		}
	}

	if( endFrame < 0 )
		endFrame = 0;
	if( startFrame == 0x7FFF )
		startFrame = endFrame;
}

void rexMayaUtility::GetAnimationStartAndEndFrames( const atArray<MDagPath>& animNodes, int& startFrame, int& endFrame )
{
	startFrame = 0x7FFF;
	endFrame = -1;

	int nodeCount = animNodes.GetCount();	
	for( int a = 0; a < nodeCount; a++ )
	{
		int s, e;		
		GetAnimationStartAndEndFrames( animNodes[ a ], s, e, false, false );
		if( s < startFrame )
			startFrame = s;
		if( e > endFrame )
			endFrame = e;
	}

	if( endFrame < 0 )
		endFrame = 0;
	if( startFrame == 0x7FFF )
		startFrame = endFrame;
}

float rexMayaUtility::ConvertTime( const MTime& time )
{
	double value = time.value();
	switch( time.unit() )
	{
		case MTime::kHours:
			return (float)( value * 3600.0f );
		case MTime::kMinutes:
			return (float)( value * 60.0f );
		case MTime::kSeconds:
			return (float) value;
		case MTime::kMilliseconds:
			return (float)( value * 0.001f );
		case MTime::kGames:
			return (float)( value / 15.0f );
		case MTime::kFilm:
			return (float)( value / 24.0f );
		case MTime::kPALFrame:
			return (float)( value / 25.0f );
		case MTime::kNTSCFrame:
			return (float)( value / 30.0f );
		case MTime::kShowScan:
			return (float)( value / 48.0f );
		case MTime::kPALField:
			return (float)( value / 50.0f );
		case MTime::kNTSCField:
			return (float)( value / 60.0f );
		default:
			Assert(0);
	};
	return (float) value;
}

float rexMayaUtility::ConvertTime( float t )
{
	MTime time;
//	time.setUnit( MTime::kNTSCFrame );
	// we want to use Maya's timebase here
	time.setUnit( MAnimControl::currentTime().unit() );
	time.setValue( t );
	return ConvertTime( time );
}

///////////////////////////////////////////////////////////////////

bool rexMayaUtility::IsAnimated( const MDagPath& dp, bool bCheckOnlyAnimCurves)
{
	atString dbgName(dp.partialPathName().asChar());

	//If we are onlychecking if there are keys on the anim curves, then use the
	//built in MAnimUtil::isAnimated method
	if(bCheckOnlyAnimCurves)
		return MAnimUtil::isAnimated(dp);

	MTime startTime = MAnimControl::animationStartTime();
	MTime endTime	= MAnimControl::animationEndTime();

	MFnDependencyNode fnDp(dp.node());
	MPlug tXPlug,tYPlug,tZPlug;
	MPlug rXPlug,rYPlug,rZPlug;
	MPlug sXPlug,sYPlug,sZPlug;

	tXPlug = fnDp.findPlug("translateX");
	tYPlug = fnDp.findPlug("translateY");
	tZPlug = fnDp.findPlug("translateZ");
	rXPlug = fnDp.findPlug("rotateX");
	rYPlug = fnDp.findPlug("rotateY");
	rZPlug = fnDp.findPlug("rotateZ");
	sXPlug = fnDp.findPlug("scaleX");
	sYPlug = fnDp.findPlug("scaleY");
	sZPlug = fnDp.findPlug("scaleZ");

	MVector	baseTrans,baseRot,baseScale;
	MTime curTime = startTime;
	while(curTime != endTime)
	{
		MVector cT,cR,cS;

		MDGContext curDGContext(curTime);

		tXPlug.getValue(cT.x, curDGContext);
		tYPlug.getValue(cT.y, curDGContext);
		tZPlug.getValue(cT.z, curDGContext);

		rXPlug.getValue(cR.x, curDGContext);
		rYPlug.getValue(cR.y, curDGContext);
		rZPlug.getValue(cR.z, curDGContext);

		sXPlug.getValue(cS.x, curDGContext);
		sYPlug.getValue(cS.y, curDGContext);
		sZPlug.getValue(cS.z, curDGContext);

		if(curTime == startTime)
		{
			//First time through store the transform values as the 
			//base transform
			baseTrans = cT;
			baseRot = cR;
			baseScale = cS;
		}
		else
		{
			if( (baseTrans != cT) ||
				(baseRot != cR) ||
				(baseScale != cS) )
			{
				//The transform for the node has changed, so it must be animated
				return true;
			}
		}
		curTime++;
	}
	return false;
}

bool rexMayaUtility::IsAnimated( const atArray<MDagPath>& nodes, bool bCheckOnlyAnimCurves)
{
	int nodeCount = nodes.GetCount();	
	for( int i = 0; i < nodeCount; i++ )
	{
		//If any of the nodes in the array are animated then early out.
		if(rexMayaUtility::IsAnimated(nodes[i], bCheckOnlyAnimCurves))
			return true;
	}
	return false;
}

///////////////////////////////////////////////////////////////////

int  rexMayaUtility::GetDPDepth( MDagPath dp )
{
	if( dp.isValid() )
	{
		if( dp.fullPathName().length() < 2 )
		{
			return 0;
		}
		else
		{
			dp.pop();
			return GetDPDepth( dp ) + 1;
		}
	}
	Assert(0);
	return -1;
}

int	 rexMayaUtility::GetDescendantCount( MDagPath dp )
{
	int total = 0, childCount = dp.childCount();
	for( int a = 0; a < childCount; a++ )
	{
		MDagPath childDP = MDagPath::getAPathTo( dp.child( a ) );
		total += GetDescendantCount( childDP );
	}
	total += childCount;
	return total;
}

/////////////////////////////////

void rexMayaUtility::MessageBoxOK( const char *info, const char *title )
{
	if( MGlobal::mayaState() == MGlobal::kInteractive )
	{
		MString melCmd = "	confirmDialog -title \"" + MString(title) + "\" -message \"" + MString(info) + "\" -button \"OK\"" + 
						 "	 -defaultButton \"OK\" -cancelButton \"OK\" -dismissString \"OK\";";	
		MGlobal::executeCommand( melCmd, false );	
	}
}

bool rexMayaUtility::MessageBoxYesNo( const char *info, const char *title )
{
	if( MGlobal::mayaState() == MGlobal::kInteractive )
	{
		MString melCmd = "	confirmDialog -title \"" + MString(title) + "\" -message \"" + MString(info) + "\" -button \"Yes\" -button \"No\"" + 
						 "	 -defaultButton \"No\" -cancelButton \"No\" -dismissString \"No\";";
		MString result;
		MStatus status = MGlobal::executeCommand( melCmd, result, false );
		return( !status.error() && result == "Yes" );
	}
	else
	{
		return true;
	}
}

/////////////////////////////////

//////////////////////////////////////////
// progress window functions

bool rexMayaUtility::OpenProgressWindow( const char *title, int numValues )
{
	atString cmd;
	char buffer[256];
	cmd = "string $rsdProgressWindow = `window -minimizeButton false -maximizeButton false -resizeToFitChildren true -title \"";
	cmd += title;
	cmd += "\"`;\n";
	cmd += "columnLayout;\n";
	for( int a = 0; a < numValues; a++ )
	{
		cmd += "string $info";
		sprintf( buffer, "%d", a + 1 );
		cmd += buffer;
		cmd += "Label = `text`;\n";
		cmd += "string $progress";
		cmd += buffer;
		cmd += "Control = `progressBar -maxValue 100 -width 525`;\n";
	}
	cmd += "showWindow $rsdProgressWindow;\n";
	for( int a = 0; a < numValues; a++ )
	{
		sprintf( buffer, "%d", a + 1 );
		cmd += "eval (\"text -edit -label \\\"info\\\" \" + $info";
		cmd += buffer;
		cmd += "Label);\n";
	}

	MStatus status = MGlobal::executeCommand(MString(cmd), false);

	return !status.error();
}

bool rexMayaUtility::CloseProgressWindow(void)
{
	MString result;
	MStatus status = MGlobal::executeCommand("eval (\"window -edit -visible false \" + $rsdProgressWindow)", result, false);
	Displayf( "CloseProgressWindow result = %s", result.asChar() );

	return !status.error();
}

bool rexMayaUtility::UpdateProgressWindowInfo(int index, const char * info)
{
	Assert(info);
	
	const int MAX_MESSAGE_LEN = 100;
	
	atString infoN( info );
	if( infoN.GetLength() > MAX_MESSAGE_LEN )
		infoN.Truncate( MAX_MESSAGE_LEN );

	atString cmd;
	
	char buffer[256];

	cmd = "eval (\"text -edit -label \\\"";
	cmd += infoN;	
	cmd += "\\\" \" + $info";
	sprintf( buffer, "%d", index + 1 );
	cmd += buffer;
	cmd += "Label)";

	MStatus status = MGlobal::executeCommand(MString(cmd), false);

	return !status.error();
}

bool rexMayaUtility::UpdateProgressWindowMax(int index, int max)
{
	atString cmd;
	char buffer[256];

	cmd = "eval (\"progressBar -edit -maxValue ";
	sprintf( buffer, "%d", Max( max, 1 ) );
	cmd += buffer;
	cmd += " \" + $progress";
	sprintf( buffer, "%d", index + 1 );
	cmd += buffer;
	cmd += "Control)";

//	Displayf( "CMD: %s", cmd );

	MStatus status = MGlobal::executeCommand(MString(cmd), false);

	return !status.error();
}

bool rexMayaUtility::UpdateProgressWindowValue(int index, int value)
{
	atString cmd;
	char buffer[256];

	cmd = "eval (\"progressBar -edit -progress ";
	sprintf( buffer, "%d", value );
	cmd += buffer;
	cmd += " \" + $progress";
	sprintf( buffer, "%d", index + 1 );
	cmd += buffer;
	cmd += "Control)";

	MStatus status = MGlobal::executeCommand(MString(cmd), false);

	return !status.error();
}

bool rexMayaUtility::IsTypeOrHasTypeAsDescendant( const MDagPath& dagpath, MFn::Type type )
{
	if( dagpath.apiType() == type )
		return true;

	for( unsigned int a = 0; a < dagpath.childCount(); a++ )
	{
		MDagPath dp = MDagPath::getAPathTo( dagpath.child( a ) );
		if( IsTypeOrHasTypeAsDescendant( dp, type ) )
			return true;
	}

	return false;
}

bool rexMayaUtility::HasTypeAsAncestor( const MDagPath& dagpath, MFn::Type type )
{
	MDagPath dpParent( dagpath );
	dpParent.pop();

	if( dpParent.apiType() == type )
		return true;
	else if( dpParent.fullPathName().length() < 2 )
		return false;
	else
		return HasTypeAsAncestor( dpParent, type );
}

static bool GetAncestorWithChildOfType( const MDagPath& dp, MFn::Type type, MDagPath& ancestorDP )
{
	MDagPath dpParent( dp );
	dpParent.pop();

	int childCount = dpParent.childCount();
	for( int a = 0; a < childCount; a++ )
	{
		if( dpParent.child(a).apiType() == type )
		{
			ancestorDP = dpParent;
			return true;
		}
	}
	if( dpParent.fullPathName().length() < 2 )
		return false;
	else
		return GetAncestorWithChildOfType( dpParent, type, ancestorDP );
}

bool rexMayaUtility::IsLevelInstanceObject( const MDagPath& path, bool includeStatic, bool includeDynamic )
{
	// n8todo -- verify documentation standard that shape is always first child of transform
	bool isDynamic = GetAttributeValue( path.node(), "IsBanger" ).toBool();
	return ( path.childCount() && path.child(0).apiType() == MFn::kImplicitSphere ) && (( includeStatic && !isDynamic ) || ( includeDynamic && isDynamic ));
}

bool rexMayaUtility::IsChildOfLevelInstanceObject( const MDagPath& path, bool includeStatic, bool includeDynamic )
{
	MDagPath ancestor;
	if( GetAncestorWithChildOfType( path, MFn::kImplicitSphere, ancestor ) )
	{
		bool isDynamic = GetAttributeValue( ancestor.node(), "IsBanger" ).toBool();
		return (( includeStatic && !isDynamic ) || ( includeDynamic && isDynamic ));
	}
	return false;	
}


// Cutscene
void rexMayaUtility::SetFrame( int frame )
{
	MTime current(double(frame), MAnimControl::currentTime().unit() );
	MAnimControl::setCurrentTime(current);
}

int rexMayaUtility::GetFrame()
{
	return (int) MAnimControl::currentTime().value();
}

void rexMayaUtility::GetMayaSceneStartAndEndFrames(int &startf, int &endf)
{
	double sf = -1.0f;
	double ef = -1.0f;
	MGlobal::executeCommand("playbackOptions -query -min", sf, false);
	MGlobal::executeCommand("playbackOptions -query -max", ef, false);

	startf = int(sf);
	endf = int(ef);
}

void rexMayaUtility::GetMayaSceneStartFrame(float &startf)
{
	double sf = -1.0f;
	MGlobal::executeCommand("playbackOptions -query -min", sf, false);
	startf = float(sf);
}

void rexMayaUtility::GetMayaSceneEndFrame(float &endf)
{
	double ef = -1.0f;
	MGlobal::executeCommand("playbackOptions -query -max", ef, false);
	endf = float(ef);
}

// BindPose
void rexMayaUtility::ToggleBindPose(const MObject& cluster, bool gotoBindPose, atString& savePoseName)
{
	MFnSkinCluster fnCluster(cluster);

	MStatus status;
	MDagPathArray bonePaths;
	unsigned affectingBones = fnCluster.influenceObjects(bonePaths, &status);
	if( affectingBones == 0 )
		return;

	// Turn off IK.
	MGlobal::executeCommand("ikSystem -e -sol false", false);

	// Create a command to select all joints and the parents of skeleton.
	MString cmdSelectJoints("select -replace -hierarchy ");

	// Save/Recover rotations
	unsigned i;
	for( i = 0; i < affectingBones; i++ )
	{
		cmdSelectJoints += bonePaths[i].partialPathName();
		cmdSelectJoints += " ";
	}


	MDagPath root = bonePaths[0];
	FindChainRoot(bonePaths[0].partialPathName().asChar(), root);
	MFnDagNode theDag(root);

	// add root to the select list so that entire tree will goto the bind pose:
	cmdSelectJoints += root.partialPathName();
	cmdSelectJoints += " ";

	// Select all parent nodes of the skeleton root.
	MObject parentObj = theDag.parent(0, &status);
	if( status == MS::kSuccess )
	{
		MFn::Type validType = parentObj.apiType();
		while(1)
		{
			parentObj = theDag.parent(0);
			if( validType == MFn::kWorld || parentObj.apiType() != validType )
				break;

			MFnDagNode fnParent(parentObj);
			MDagPath parentPath;
			fnParent.getPath(parentPath);

			cmdSelectJoints += parentPath.partialPathName();
			cmdSelectJoints += " ";


			// Traverse up
			theDag.setObject(parentObj);
		}
	}

	//Before doing anything check to see if the nodes are storing a bind pose or not
	MStringArray hasPoseCmdRes;
	MGlobal::executeCommand(cmdSelectJoints, false, true);
	MGlobal::executeCommand("dagPose -q -bp", hasPoseCmdRes, false, true);

	if(hasPoseCmdRes.length() != 0)
	{
		if( gotoBindPose )
		{
			MString string;
			string = "dagPose -save -name exportPose";

			// Execute cmd in this order!
			MGlobal::executeCommand(cmdSelectJoints,false,true);
			MStringArray poseName;
			MGlobal::executeCommand(string,poseName,false,true);
			savePoseName=poseName[0].asChar();
			MGlobal::executeCommand("gotoBindPose",false,true);
			MGlobal::executeCommand("rotate 0.0 0.0 0.0",false,true);
		}
		else
		{
			MCommandResult cmdRes;

			// Restore IK.
			MString string;
			string = "dagPose -global -restore ";
			string += savePoseName;
			MGlobal::executeCommand(cmdSelectJoints, cmdRes, false,true);
			MGlobal::executeCommand(string, cmdRes, false,true);
			MGlobal::executeCommand("ikSystem -e -sol true", cmdRes, false,true);

			// Clean up the dagPose node that was created
			MString delDagPoseCmd;
			delDagPoseCmd = "delete ";
			delDagPoseCmd += savePoseName;
			MGlobal::executeCommand(delDagPoseCmd, false);
		}
	}
}

/*
PURPOSE
	Check if animation data for all frames is needed for the given node.
PARAMS
	dp - the node to be checked.
RETURN
	bool.
NOTES
	If the given node is driven by IK, or expression, or constraints
	or setDrivenKey, have to get animation data for all frames.
*/
bool rexMayaUtility::IsFrameDataNeeded(const MDagPath& dp)
{
	bool isDrivenBy = IsNodeDrivenByAnimCurve(dp);
	if( !isDrivenBy ) return true;

	// The node maybe a joint and still driven by IK.
	isDrivenBy = IsJointDrivenByIK(dp);
	return isDrivenBy;
}

/*
PURPOSE
	Check if the given node is linked with animCurve.
PARAMS
	dp - the node to be checked.
RETURN
	bool.
NOTES
	If any attribute is not linked with animCurve, consider this node is not
	driven by animCurve.
*/
bool rexMayaUtility::IsNodeDrivenByAnimCurve(const MDagPath& dp)
{
	bool isDrivenBy = IsNodeDrivenByAnimCurve( dp, "translateX" );
	if( ! isDrivenBy ) return false;

	isDrivenBy = IsNodeDrivenByAnimCurve( dp, "translateY" );
	if( ! isDrivenBy ) return false;

	isDrivenBy = IsNodeDrivenByAnimCurve( dp, "translateZ" );
	if( ! isDrivenBy ) return false;

	isDrivenBy = IsNodeDrivenByAnimCurve( dp, "rotateX" );
	if( ! isDrivenBy ) return false;

	isDrivenBy = IsNodeDrivenByAnimCurve( dp, "rotateY" );
	if( ! isDrivenBy ) return false;

	isDrivenBy = IsNodeDrivenByAnimCurve( dp, "rotateZ" );
	if( ! isDrivenBy ) return false;

	/*
	isDrivenBy = IsNodeDrivenByAnimCurve( dp, "scaleX" );
	if( ! isDrivenBy ) return false;

	isDrivenBy = IsNodeDrivenByAnimCurve( dp, "scaleY" );
	if( ! isDrivenBy ) return false;

	isDrivenBy = IsNodeDrivenByAnimCurve( dp, "scaleZ" );
	if( ! isDrivenBy ) return false;
	*/

	return true;
}

/*
PURPOSE
	Check if the given attribute of the given node is linked with animCurve.
PARAMS
	dp - the node to be checked.
	attr - the attribute to be checked.
RETURN
	bool.
NOTES
	If no connections, consider this case as "Driven-By-Curve".
*/
bool rexMayaUtility::IsNodeDrivenByAnimCurve(const MDagPath& dp, const char* attr)
{
	MStatus status;
	MFnDagNode fn(dp);
	MPlug plug = fn.findPlug(attr, &status);
	if( status.error() )
		return false;

	MPlugArray connections;
	plug.connectedTo( connections, true, false );
	if( connections.length() == 0 )
		return true;

	MObject animCurveObj = connections[0].node();
	if( animCurveObj.hasFn(MFn::kAnimCurve) )  
		return true;

	return false;
}

/*
PURPOSE
	Check if the joint is driven by any IK.
PARAMS
	dpJ - the joint to be checked.
RETURN
	bool.
NOTES
*/
bool rexMayaUtility::IsJointDrivenByIK(const MDagPath& dpJ)
{
	// if not joint, return false.
	if( dpJ.apiType() != MFn::kJoint )
			return false;

	// Loop each ikHandle.
	bool byIK = false;
	MItDependencyNodes itDN(MFn::kIkHandle);
	while( !itDN.isDone() )
	{
		// Loop each joint up from end-joint to start-joint of the ikHandle.
		MObject objIKH = itDN.item();
		MFnIkHandle fnIKH(objIKH);

		// If this ikHandle is not enabled, continue.
		//bool ikEnabled = false;
		//MPlug plug = fnIKH.findPlug("solverEnable");
		//plug.getValue(ikEnabled);
		//if( !ikEnabled ) { itDN.next(); continue; }

		// If this ikHandle is not valid(no start & end joints), continue.
		MDagPath dpSJ, dpEff;
		MStatus status = fnIKH.getStartJoint(dpSJ);
		if( status != MS::kSuccess ) { itDN.next(); continue; }
		status = fnIKH.getEffector(dpEff);
		if( status != MS::kSuccess ) { itDN.next(); continue; }

		dpEff.pop();
		if( dpJ == dpSJ || dpJ == dpEff )
		{
			byIK = true;
			break;
		}

		while( !(dpEff == dpSJ) )
		{
			if( dpJ == dpEff )
			{
				byIK = true;
				break;
			}
			dpEff.pop();
		}

		if( byIK )
			break;

		itDN.next();
	}

	return byIK;
}

/*
PURPOSE
	Check if this attribute is locked or not(non-animatable is as locked).
PARAMS
	fn - the node.
	attr - attribute to be checked.
RETURN
	bool.
NOTES
*/
bool rexMayaUtility::IsAttrLocked(const MFnDagNode& fn, const char* attr, bool falseIfNotKeyable)
{
	MStatus s;
	MPlug plug = fn.findPlug( attr, &s );
	return( s.error() || plug.isLocked() || ( falseIfNotKeyable && !plug.isKeyable() ));
}

/*
PURPOSE
	Display baked keyframe data for the given node and attribute.
PARAMS
	node - the node to be checked.
	attr - attribute to be checked.
RETURN
	bool.
NOTES
*/
#define	R2D	(float)(180.0f/3.1415926f)
void rexMayaUtility::DebugBakeResults(const char* node, const char* attr)
{
	// Check bake results.
	MDagPath dp;
	rexMayaUtility::FindNode(node, dp);
	MFnDagNode fn(dp);
	MPlug plug = fn.findPlug(attr);

	MPlugArray connections;
	plug.connectedTo( connections, true, false );

	MObject animCurveObj = connections[0].node();
	MFnAnimCurve curve( animCurveObj );
	int count = curve.numKeys();
	Printf(" DebugBakeResults: node=%s attr=%s curve=%s keys=%d\n",fn.name().asChar(),attr,curve.name().asChar(),count);
	for( int i = 0; i < count; i++ )
	{
		float t = (float)curve.time(i).value();
		float v = (float)curve.value(i);
		if( strstr(attr, "rotate") )
			Printf(" DebugBakeResults: key%d: t=%f value=%f\n",i,t,v*R2D);
		else
			Printf(" DebugBakeResults: key%d: t=%f value=%f\n",i,t,v);
	}
}

///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

void rexMayaUtility::GetAttributeList( const MObject& node, atArray<atString>& attributes )
{
	MFnDependencyNode fn( node );
	unsigned attCount = fn.attributeCount();
	for( unsigned a = 0; a < attCount; a++ )
	{
		MStatus status;
		MObject att = fn.attribute( a, &status );

		if( !status.error() )
		{
			MPlug plug = fn.findPlug( att, &status );
			if( !status.error() )
			{
				attributes.PushAndGrow( atString(plug.partialName( false, false, false, false, false, true ).asChar()), (u16) attCount );
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////

MPlug rexMayaUtility::FindPlug( MDagPath dp, const char *name, MStatus* retStatus, int dgDepth )
{
	MFnDependencyNode fn( dp.node() );
	return FindPlug( fn, name, retStatus, dgDepth );
}

MPlug rexMayaUtility::FindPlug( MFnDependencyNode fn, const char *name, MStatus* retStatus, int dgDepth )
{
	MStatus status;
	MPlug plug = fn.findPlug( (const char*)name, &status );
	if( !status.error() )
	{
		if( retStatus )
			*retStatus = MStatus( MStatus::kSuccess );
		return plug;
	}
	else // if attribute not on this node...
	{
		// look at dependency nodes (helps for materials, uv slide/rotate, etc)
		// right now, it goes downstream up to dgDepth nodes, and searches
		// upstream from each of those nodes.

		MObject rootObj = fn.object();

		MItDependencyGraph it( rootObj, MFn::kInvalid, MItDependencyGraph::kDownstream, MItDependencyGraph::kDepthFirst, MItDependencyGraph::kNodeLevel );

		bool found = 0;
		for( int d = 0; d < dgDepth && !it.isDone() && !found; d++ )
		{
			MObject thisObj = it.thisNode();
			MFnDependencyNode fnDep( thisObj );
			MPlug plug = fnDep.findPlug((const char*)name, &status);
			if( !status.error()  )
			{
				if( retStatus )
					*retStatus = MStatus( MStatus::kSuccess );

				return plug;
			}
			else
			{
				MItDependencyGraph mfit( thisObj, MFn::kInvalid, MItDependencyGraph::kUpstream, MItDependencyGraph::kDepthFirst, MItDependencyGraph::kNodeLevel );
				while( !mfit.isDone() )
				{
					MObject n = mfit.thisNode();
					MFnDependencyNode fnDep( n );
					MPlug plug = fnDep.findPlug((const char*)name, &status);
					if( !status.error() )
					{
						if( retStatus )
							*retStatus = MStatus( MStatus::kSuccess );
						return plug;
					}
					mfit.next();
				}
			}

			it.next();
		}

		if( !found )
		{
			MObject o = fn.object();
			if( o.hasFn( MFn::kDagNode ) )
			{
				MDagPath dp = MDagPath::getAPathTo( o );
				// if not on dependency nodes, look at children (along with their dependency nodes if boneless)
				int childCount = dp.childCount();
				for( int a = 0; a < childCount; a++ )
				{
					MDagPath childDP = MDagPath::getAPathTo( dp.child(a) );
					MPlug plug = FindPlug( childDP, name, retStatus, dgDepth );
					if( retStatus && !retStatus->error() )
					{
						if( retStatus )
							*retStatus = MStatus( MStatus::kSuccess );
						return plug;
					}
				}
			}
		}
	}

	if( retStatus )
		*retStatus = MStatus( MStatus::kFailure );

	return MPlug();
}

////////////////////////////////////////////////

MObject rexMayaUtility::FindCurve( MPlug plug, bool considerParentPlugs, bool considerChildPlugs, int childIndex )
{
	MObject obj;
	MStatus s;

	MPlugArray connections;
	plug.connectedTo( connections, true, false );
	int conCount = connections.length();
	for( int c = 0; c < conCount; c++ )
	{
		obj = connections[c].node();
		if( obj.hasFn( MFn::kAnimCurve ) )
			return obj;

		obj = FindCurve( connections[ c ], true, true, childIndex );
		if( obj.hasFn( MFn::kAnimCurve ) )
			return obj;
	}

	if( considerParentPlugs && plug.isChild() )
	{
		MPlug parent = plug.parent( &s );
		if( !s.error() && !parent.isNull() )
		{
			int count = parent.numChildren();
			for( int a = 0; a < count; a++ )
			{
				if( parent.child( a ) == plug )
				{
					childIndex = a;
					break;
				}
			}
			obj = FindCurve( parent, true, false, childIndex );
			if( obj.hasFn( MFn::kAnimCurve ) )
				return obj;
		}
	}

	if( considerChildPlugs && plug.isCompound() )
	{
		int count = plug.numChildren();
		if( ( childIndex >= 0 ) && ( childIndex < count ) )
		{
			obj = FindCurve( plug.child( childIndex ), false, true, childIndex );
			if( obj.hasFn( MFn::kAnimCurve ) )
				return obj;
		}
	}

	return obj;
}

////////////////////////////////////////////////


bool rexMayaUtility::HasTextureAnimation( MDagPath dp )
{
	MFnDependencyNode fn( dp.node() );
	return HasTextureAnimation( fn );
}

////////////////////////////////////////////////

bool rexMayaUtility::HasTextureAnimation( MFnDependencyNode fn )
{
	MStatus s;
	MPlug p;
	MObject c;
	p = FindPlug( fn, "frameExtension", &s );
	if( !s.error() )
	{
		c = FindCurve( p );
		if( !c.isNull() )
			return true;
	}

	p = FindPlug( fn, "offsetU", &s );
	if( !s.error() )
	{
		c = FindCurve( p );
		if( !c.isNull() )
			return true;
	}
	
	p = FindPlug( fn, "offsetV", &s );
	if( !s.error() )
	{
		c = FindCurve( p );
		if( !c.isNull() )
			return true;
	}

	p = FindPlug( fn, "rotateUV", &s );
	if( !s.error() )
	{
		c = FindCurve( p );
		if( !c.isNull() )
			return true;
	}

	return false;
}

////////////////////////////////////////////////

void rexMayaUtility::Refresh()
{
	MGlobal::executeCommand( "refresh", false );
}

////////////////////////////////////////////////

bool rexMayaUtility::GetBlendShapeNodeConnectedToMesh( const MDagPath &dp, MObject &blendShapeMObj ) 
{
	MStatus status;

	MFnMesh fnDp(dp);

	//Get the connections to the 'inMesh' plug
	MPlug inMeshPlug = fnDp.findPlug("inMesh", &status);
	Assertf( (status == MS::kSuccess), "Internal Maya failure, failed to locate 'inMesh' plug for the mesh (%s)", fnDp.name().asChar());

	//Iterate upstream from the 'inMesh' plug looking for the first blendshape node that appears (if any)
	MItDependencyGraph itDG(inMeshPlug,  MFn::kBlendShape, MItDependencyGraph::kUpstream, MItDependencyGraph::kBreadthFirst);
	while( !itDG.isDone() )
	{
		if( itDG.thisNode().apiType() == MFn::kBlendShape )
		{
			blendShapeMObj = itDG.thisNode();
			return true;
		}
		itDG.next();
	}
	
	return false;
}

////////////////////////////////////////////////

void rexMayaUtility::GetAllBlendShapeNodes( const MDagPath& dp, atArray<MObject>& outNodes )
{
	MStatus status;
	MFnMesh fnDp(dp, &status);
	if(status != MS::kSuccess)
		return;

	MPlug inMeshPlug = fnDp.findPlug("inMesh", &status);
	Assertf( (status == MS::kSuccess), "Internal Maya failure, failed to locate 'inMesh' plug for the mesh (%s)", fnDp.name().asChar());

	MItDependencyGraph itDG(inMeshPlug,  MFn::kBlendShape, MItDependencyGraph::kUpstream, MItDependencyGraph::kBreadthFirst);
	while( !itDG.isDone() )
	{
		if( itDG.thisNode().apiType() == MFn::kBlendShape && !IsParallelBlender(itDG.thisNode()) )
		{
			bool exists = false;
			for( int i = 0; i < outNodes.GetCount(); i++ )
			{
				if( itDG.thisNode() == outNodes[i] )
				{
					exists = true;
					break;
				}
			}

			if( !exists )
			{
				outNodes.Grow() = itDG.thisNode();
			}
		}
		itDG.next();
	}
}

////////////////////////////////////////////////

int rexMayaUtility::GetBlendTargetCount( const MObject& blendShapeMObj )
{
	MStatus		status;
	MFnBlendShapeDeformer fnBlendShapeDef(blendShapeMObj, &status);
	Assert(status == MS::kSuccess);

	MIntArray indexArray;
	status = fnBlendShapeDef.weightIndexList(indexArray);
	unsigned int numTargets = indexArray.length();	

	return (int)numTargets;
}

////////////////////////////////////////////////

rexResult rexMayaUtility::GetBlendTargetName( int index, const MObject& blendShapeMObj, atString& targetName)
{
	MStatus s;
	rexResult	result(rexResult::PERFECT);

	MFnBlendShapeDeformer fnBlendShapeDef(blendShapeMObj, &s);

	char command[1024];
	sprintf(command, "aliasAttr -q \"%s\";", fnBlendShapeDef.name().asChar());

	MStringArray names;
	MGlobal::executeCommand(command, names, false);
	Assert((unsigned)index < (names.length() / 2));
	targetName = atString(names[index * 2].asChar());

	return result;
}

////////////////////////////////////////////////

float rexMayaUtility::GetBlendTargetWeight( int index, const MObject& blendShapeMObj)
{
	MStatus		status;
	
	MFnBlendShapeDeformer fnBlendShapeDef(blendShapeMObj, &status);
	Assert(status == MS::kSuccess);

	MIntArray indexArray;
	status = fnBlendShapeDef.weightIndexList(indexArray);

	return fnBlendShapeDef.weight(indexArray[index]);
}

////////////////////////////////////////////////

void PrintPlug(MPlug plug, const char* tab)
{
	char temp[1024];
	sprintf(temp, "%s\t", tab);
	Printf("%sPlug %s: %d children, %d elements\n", tab, plug.name().asChar(), plug.numChildren(), plug.numElements());
	Printf("%sChildren:\n", tab);
	for( unsigned int i = 0; i < plug.numChildren(); i++ )
	{
		MPlug child = plug.child(i);
		PrintPlug(child, temp);
	}
	Printf("%sElements:\n", tab);
	for( unsigned int i = 0; i < plug.numElements(); i++ )
	{
		MPlug elem = plug.elementByPhysicalIndex(i);
		PrintPlug(elem, temp);
	}
	Printf("\n");
}

////////////////////////////////////////////////

bool rexMayaUtility::IsParallelBlender(MObject obj)
{
	MStatus status;
	MFnBlendShapeDeformer fnBlendShapeDef(obj, &status);
	Assert(status == MS::kSuccess);
	
	bool bIsParallelBlender = false;	
	MPlug isParallelPlug = fnBlendShapeDef.findPlug("parallelBlender", &status);
	if(status == MS::kSuccess)
	{
		bool bTmp;
		status = isParallelPlug.getValue(bTmp);
		if(status == MS::kSuccess)
			bIsParallelBlender = bTmp;
	}
	return bIsParallelBlender;
}

////////////////////////////////////////////////

rexResult rexMayaUtility::GetBlendTargetDeltas( const MObject& blendShapeMObj, atArray<rexObjectGenericMesh::BlendTargetDelta>& outDeltas )
{
	MStatus s;
	rexResult	result(rexResult::PERFECT);

	// Cast to a blend shape deformer
	MFnBlendShapeDeformer fnBlendShapeDef(blendShapeMObj, &s);
	Printf("Collecting blend targets from node: %s\n", fnBlendShapeDef.name().asChar());

	MObjectArray baseObjects;
	fnBlendShapeDef.getBaseObjects(baseObjects);
	if( baseObjects.length() != 1 )
	{
		result.Set(rexResult::ERRORS);
		result.AddMessage("Multiple base objects(%d) specified the blend shape node (%s).  Only one base object is supported.", baseObjects.length(), fnBlendShapeDef.name().asChar());
		return result;
	}
	
	MFnMesh baseMesh(baseObjects[0]);

	MPlug itPlug = fnBlendShapeDef.findPlug("it");
	MPlug it0 = itPlug.elementByPhysicalIndex(0);
	MPlug itg = it0.child(0);

	for( unsigned int i = 0; i < itg.numElements(); i++ )
	{
		MPlug elem = itg.elementByPhysicalIndex(i);
		MPlug targetData = elem.child(0).elementByPhysicalIndex(0);

		rexObjectGenericMesh::BlendTargetDelta& delta = outDeltas.Grow();
        
		MObject val;
		
		MPlug ipgPlug = targetData.child(0);
		s = ipgPlug.getValue(val);
		if( s )
		{
			// Target exists, need to compute the deltas ourselves
			MFnMesh targetMesh(val);

			MPointArray basePoints, targetPoints;
			baseMesh.getPoints(basePoints);
			targetMesh.getPoints(targetPoints);
			if( basePoints.length() != targetPoints.length() )
			{
				result.Set(rexResult::ERRORS);
				result.AddMessage("Target mesh vertex count(%d) does not match the base mesh vertex count(%d)", targetPoints.length(), basePoints.length());
				return result;
			}

			for( unsigned vert = 0; vert < basePoints.length(); vert++ )
			{
				MPoint baseVert = basePoints[vert];
				MPoint targVert = targetPoints[vert];
				MPoint d = targVert - baseVert;

				delta.m_Deltas.Grow() = Vector3((float)d.x, (float)d.y, (float)d.z);
				delta.m_VertexIndices.Grow() = vert;
			}
		}
		else
		{		
			// Targets have been deleted, grab the deltas from the input attributes
			MPlug iptPlug = targetData.child(1);
			s = iptPlug.getValue(val);
			MFnPointArrayData pad(val);
			for( unsigned int j = 0; j < pad.length(); j++ )
			{
				MPoint pt = pad[j];
				delta.m_Deltas.Grow() = Vector3((float)pt.x, (float)pt.y, (float)pt.z);
			}

			MPlug ictPlug = targetData.child(2);
			s = ictPlug.getValue(val);
			MFnComponentListData cld(val);

			for( unsigned int j = 0; j < cld.length(); j++ )
			{
				MObject comp = cld[j];
				MFnSingleIndexedComponent compFn(comp);

				MIntArray indices;
				compFn.getElements(indices);

				for( unsigned int k = 0; k < indices.length(); k++ )
				{
					delta.m_VertexIndices.Grow() = indices[k];
				}
			}
		}
	}

	return result;
}

////////////////////////////////////////////////

rexResult rexMayaUtility::GetBlendTargetNames( const MObject& blendShapeMObj, atArray<atString>& outNames )
{
	MStatus s;
	rexResult	result(rexResult::PERFECT);

	MFnBlendShapeDeformer fnBlendShapeDef(blendShapeMObj, &s);
	if(s != MS::kSuccess)
	{
		result = rexResult::ERRORS;
		result.AddMessage("GetBlendTargetNames failed, supplied object isn't a blend shape deformer");
		return result;
	}

	MIntArray indexArray;
	s = fnBlendShapeDef.weightIndexList(indexArray);

	MPlug weightsPlug = fnBlendShapeDef.findPlug("weight");

	int nTargets = indexArray.length();
	for(int targetIdx = 0; targetIdx < nTargets; targetIdx++)
	{
		int logicalIdx = indexArray[targetIdx];
		MPlug curWeightPlug = weightsPlug.elementByLogicalIndex( logicalIdx, &s);

		MString aliasName;
		fnBlendShapeDef.getPlugsAlias(curWeightPlug, aliasName);

		outNames.Grow() = atString(aliasName.asChar());
	}

	return result;
}

////////////////////////////////////////////////

void rexMayaUtility::PrintDataType(MFnData::Type type)
{
	switch( type )
	{
		case MFnData::kInvalid:
			Printf("kInvalid");
			break;
		case MFnData::kNumeric:
			Printf("kNumeric");
			break;
		case MFnData::kPlugin:
			Printf("kPlugin");
			break;
		case MFnData::kPluginGeometry:
			Printf("kPluginGeometry");
			break;
		case MFnData::kString:
			Printf("kString");
			break;
		case MFnData::kMatrix:
			Printf("kMatrix");
			break;
		case MFnData::kStringArray:
			Printf("kStringArray");
			break;
		case MFnData::kDoubleArray:
			Printf("kDoubleArray");
			break;
		case MFnData::kIntArray:
			Printf("kIntArray");
			break;
		case MFnData::kPointArray:
			Printf("kPointArray");
			break;
		case MFnData::kVectorArray:
			Printf("kVectorArray");
			break;
		case MFnData::kComponentList:
			Printf("kComponentList");
			break;
		case MFnData::kMesh:
			Printf("kMesh");
			break;
		case MFnData::kLattice:
			Printf("kLattice");
			break;
		case MFnData::kNurbsCurve:
			Printf("kNurbsCurve");
			break;
		case MFnData::kNurbsSurface:
			Printf("kNurbsSurface");
			break;
		case MFnData::kSphere:
			Printf("kSphere");
			break;
		case MFnData::kDynArrayAttrs:
			Printf("kDynArrayAttrs");
			break;
		case MFnData::kDynSweptGeometry:
			Printf("kDynSweptGeometry");
			break;
		case MFnData::kSubdSurface:
			Printf("kSubdSurface");
			break;
		case MFnData::kLast:
			Printf("kLast");
			break;
		default:
			Printf("Unknown");
			break;
	}
}

////////////////////////////////////////////////

bool rexMayaUtility::GetBulletMargin(const MObject& mObj, float &retMargin)
{
	rexValue attrVal = rexMayaUtility::GetAttributeValue(mObj, "margin");
	if(attrVal.GetType() != rexValue::UNKNOWN)
	{
		retMargin = attrVal.toFloat();
		return true;
	}
	else
		return false;
}

////////////////////////////////////////////////
