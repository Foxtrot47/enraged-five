#include "rexmayatimerlog.h"
#include "rexBase/rexReport.h"

// It isn't pretty, but to include MFileIO.h I also need to define _BOOL
#define _BOOL
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MFileIO.h>
#pragma warning(pop)

using namespace rage;

// To get the name of the scene being exported
atString	rexTimerLog::GetSceneName()
{
	return atString(MFileIO::currentFile().asChar());
}

// To get the name of the scene being exported
atString	rexReport::GetSceneName()
{
	return atString(MFileIO::currentFile().asChar());
}

