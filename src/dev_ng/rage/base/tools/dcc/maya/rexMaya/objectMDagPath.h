// 
// rexMaya/objectMDagPatch.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data, vector
//		REX
//			datBase Library
//		MAYA 4.5 Lib
//		
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexObjectMDagPath -- rexObject derived class containing MDagPath
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexObject (REX datBase Library)
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYA_OBJECTMDAGPATH_H__
#define __REX_MAYA_OBJECTMDAGPATH_H__

/////////////////////////////////////////////////////////////////////////////////////

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MDagPath.h>
#pragma warning(pop)
#include "rexBase/object.h"
#include "rexBase/property.h"
#include "vector/matrix44.h"

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexObjectMDagPath : public rexObject
{
public:
	virtual rexObject* CreateNew()   { return new rexObjectMDagPath; }
	virtual rexObject* CreateCopy()   { return new rexObjectMDagPath(this); }

	rexObjectMDagPath() : rexObject() 
	{ 
		m_BoneIndex = 0; 
		m_BoneMatrix.Identity(); 
		m_IsBoneRoot = false; 
		m_LODGroupIndex = -1; 
		m_LODLevel = -1; 
		m_LODThreshold = 9999.0f; 
		m_ChildOfLevelInstanceNode = false; 
		m_GroupIndex = -1; 
	}

	rexObjectMDagPath( const rexObjectMDagPath *that ) 
	{ 
		m_DagPath = that->m_DagPath; 
		m_IsBoneRoot = that->m_IsBoneRoot; 
		m_BoneTags = that->m_BoneTags; 
		m_BoneIndex = that->m_BoneIndex; 
		m_BoneMatrix = that->m_BoneMatrix; 
		m_LODGroupIndex = that->m_LODGroupIndex; 
		m_LODLevel = that->m_LODLevel; 
		m_LODThreshold = that->m_LODThreshold; 
		m_ChildOfLevelInstanceNode = false; 
		m_GroupIndex = -1; 
		m_LODThreshold = 9999.0f;
	}

	rexObjectMDagPath( const MDagPath& dp ) : rexObject()
	{ 
		m_BoneMatrix.Identity(); 
		m_DagPath = dp; 
		m_BoneIndex = 0; 
		m_IsBoneRoot = false; 
		m_LODGroupIndex = -1; 
		m_LODLevel = -1; 
		m_LODThreshold = 9999.0f; 
		m_ChildOfLevelInstanceNode = false; 
		m_GroupIndex = -1; 
	}

	rexObjectMDagPath( const MDagPath& dp, int boneIndex, const Matrix44& boneMatrix, bool isBoneRoot, int lodGroupIndex, int lodLevel, float lodThreshold, int groupIndex, const atString& groupID, bool childOfLevelInstanceNode = false ) : rexObject() 
	{ 
		m_DagPath = dp; 
		m_BoneIndex = boneIndex; 
		m_BoneMatrix = boneMatrix; 
		m_IsBoneRoot = isBoneRoot; 
		m_LODGroupIndex = lodGroupIndex; 
		m_LODLevel = lodLevel; 
		m_LODThreshold = lodThreshold; 
		m_ChildOfLevelInstanceNode = childOfLevelInstanceNode; 
		m_GroupIndex = groupIndex; 
		m_GroupID = groupID; 
	}

	MDagPath			m_DagPath;
	bool				m_IsBoneRoot;
	atArray<atString>	m_BoneTags;
	int					m_BoneIndex;
	Matrix44			m_BoneMatrix;
	int					m_LODGroupIndex, m_LODLevel;
	float				m_LODThreshold;
	bool				m_ChildOfLevelInstanceNode;

	atArray<atString>	m_SpecialFlags;

	atString			m_GroupID;
	int					m_GroupIndex;	
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
