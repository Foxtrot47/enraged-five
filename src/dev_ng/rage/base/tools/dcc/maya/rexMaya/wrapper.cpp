#define REQUIRE_IOSTREAM

#include "diag/output.h"
#include "system/param.h"
#include "rexMaya/wrapper.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"
#include "rexRAGE/serializerMesh.h"
#include "rexMaya/rexMayaTimerLog.h"
#include "rexMaya/mayaSetCollection.h"
#include "rexBase/rexReport.h"
#include "rexBase/animExportCtrl.h"
#include "rexBase/xmlAssetReport.h"
#include "rageShaderMaterial/rageShaderMaterialTemplate.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MArgList.h>
#include <Maya/MFileIO.h>
#include <Maya/MGlobal.h>
#pragma warning(pop)

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// This function parses command line arguments and passes them along to the main export function
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////

using namespace rage;

namespace rage
{
#if __ASSERT
	XPARAM(nopopups);
#endif
	extern const char* s_UnitTypeName[rexSerializer::NUM_UNIT_TYPE_LINEARS];
}

rexWrapperMaya::rexWrapperMaya()
{
	INIT_PARSER;
	rage::rageShaderMaterialTemplate::InitShaderMaterialLibrary();
	rage::AnimExportCtrlSpec::RegisterParsableClasses();
	diagOutput::UseVSFormatOutput();
}

rexWrapperMaya::~rexWrapperMaya()
{
	rage::AnimExportCtrlSpec::UnRegisterParsableClasses();
	rage::rageShaderMaterialTemplate::ShutdownShaderMaterialLibrary();
	SHUTDOWN_PARSER;
}

MStatus rexWrapperMaya::doIt( const MArgList& args ) 
{
	bool selective = false;
	bool notristrip = false, ascii = false;
    bool confirmation = true;
	bool notextureconvertflag = false;  // true = NO TEXTURE CONVERSION
	bool justExportMesh = false;
	MString strMeshToJustExport = "";
	bool justExportBound = false;
	MString strBoundToJustExport = "";
	MString strPathToJustExportTo = "";
	const char* textureListFile = NULL;
	MStringArray array;
	bool bXmlReport = false;
	MString strXmlReportPathAndFilename = "";

#if __OPTIMIZED
	bool showDialogs = ( MGlobal::mayaState() == MGlobal::kInteractive );
	int bSilentRunning;
	MGlobal::executeCommand("rageSilentRunning()", bSilentRunning, false);
	Displayf("REX: showDialogs = %d", showDialogs);
	Displayf("REX: bSilentRunning = %d", bSilentRunning);
	showDialogs = showDialogs & (bSilentRunning == false);
	Displayf("REX: showDialogs = %d", showDialogs);

#if __ASSERT
	Displayf("PARAM_nopopups.Get() = %d", PARAM_nopopups.Get());

	if(showDialogs)
	{
		static const char* gFalseVal = {"false"};
		rage::PARAM_nopopups.Set(gFalseVal);
	}
	else
	{
		static const char* gTrueVal = {"true"};
		rage::PARAM_nopopups.Set(gTrueVal);
	}
	Displayf("PARAM_nopopups.Get() = %d", PARAM_nopopups.Get());
#endif //__ASSERT
#else
	bool showDialogs = true;
#endif //__OPTIMIZED

	// By default Rage exports everything in cms, this seems like madness to me.  Meters would make more sense, but anyway
	rexSerializer::SetUnitTypeToConvertTo("CENTIMETERS");

	// Parse arguments
	Displayf("Total Parms IN (%d)", args.length());

	for( unsigned int i = 0; i < args.length(); i++ )
	{
		Displayf("ARG (%d) = (%s)", i, args.asString(i).asChar());	
		if( args.asString( i ) == "-selective" )
		{
			selective = true;
		}
		else if( args.asString( i ) == "-timerLog" )
		{
			// Time how long the export takes
			rexMayaTimerLog::Open((MFileIO::currentFile() + ".ExportTimes.xml").asChar());
		}		
		else if( args.asString( i ) == "-report" )
		{
			// Keep a report of what happens during export
			rexReport::Open((MFileIO::currentFile() + ".ExportReport.html").asChar());
		}		
		else if( args.asString( i ) == "-xmlreport" )
		{
			// Keep a report of what happens during export
			bXmlReport = true;
			strXmlReportPathAndFilename = args.asString( ++i );
			xmlAssetReport::Open();
		}		
		else if( args.asString( i ) == "-quiet" )
		{
			showDialogs = false;
		}		
        else if ( args.asString( i ) == "-nocompletionconfirmation" )
        {
            confirmation = false;
        }
		else if( args.asString( i ) == "-notristrip" )
		{
			notristrip = true;
		}
		else if( args.asString( i ) == "-ascii" )
		{
			ascii = true;
		}
		else if( args.asString( i ) == "-justExportMesh" )
		{
			justExportMesh = true;
			strMeshToJustExport = args.asString( ++i );
			strPathToJustExportTo = args.asString( ++i );
		}
		else if( args.asString( i ) == "-justExportBound" )
		{
			justExportBound = true;
			strBoundToJustExport = args.asString( ++i );
			strPathToJustExportTo = args.asString( ++i );
		}
		else if( args.asString( i ) == "-silentRunning" )
		{
#if __ASSERT
			static const char* gTrueVal = {"true"};
			rage::PARAM_nopopups.Set(gTrueVal);
#endif
		}
		else if( args.asString( i ) == "-targetUnits" )
		{
			atString strTargetUnits(args.asString( ++i ).asChar());
			bool bValidTargetUnitsGiven = false;
			for (int j=0;j<rexSerializer::NUM_UNIT_TYPE_LINEARS;j++)
			{
				if (strTargetUnits==s_UnitTypeName[j])
				{
					bValidTargetUnitsGiven = true;
					rexSerializer::SetUnitTypeToConvertTo(strTargetUnits);
					break;
				}
			}
			if(!bValidTargetUnitsGiven)
			{
				// Given invalid target units, doh!
				MString strErrorMsg = "Unknown target units given.  Valid options are:\n";
				for (int j=0;j<rexSerializer::NUM_UNIT_TYPE_LINEARS;j++)
				{
					strErrorMsg  += s_UnitTypeName[j];
					strErrorMsg  += "\n";
				}
				strErrorMsg += "The target units are used to define what one centimeter in Maya should be exported as.  Perversely, by default, 1 centimeter is exported as 1.0.  If you want 1 meter to be exported as 1.0 then use \"METERS\"\n";
				MGlobal::displayInfo(strErrorMsg);
				Displayf(strErrorMsg.asChar());
				return MS::kFailure;
			}
		}
		else if (args.asString( i ) == "-notextureconvert")
		{
			notextureconvertflag = true;	// We dont want to export any textures...
		}
		else if( !strnicmp(args.asString( i ).asChar(),"-textureListFile", strlen("-textureListFile") ) )
		{
			args.asString(i).split('=',array);
			if (array.length()>=2)
				textureListFile=array[1].asChar();
			if (textureListFile)
			{
				Displayf("Texture File = (%s)", textureListFile);
			}
		}
		else
		{
			if( !ParseArgs( args, *((int*)&i) ) )
			{
				Displayf("ERROR :Unknown Parameter Passed in!");
				rexMayaTimerLog::EndTimer("MStatus rexWrapperMaya::doIt( const MArgList& args ) ");
				rexMayaTimerLog::Close();
				return MS::kFailure;
			}
		}
	}
	if(ascii)
	{
		Displayf("Exporting as ascii");
	}

#if __ASSERT
	Displayf("PARAM_nopopups.Get() = %d", PARAM_nopopups.Get());
#endif //__ASSERT

	rexMayaTimerLog::StartTimer("MStatus rexWrapperMaya::doIt( const MArgList& args ) ");
	if(rexMayaTimerLog::IsTimerLogEnabled())
	{
		// Add a link to the timer log in the report file
		rexReport::Write("Export Timer Log : <a href=\"%s\">%s</a><p>\n", (MFileIO::currentFile() + ".ExportTimes.xml").asChar(), (MFileIO::currentFile() + ".ExportTimes.xml").asChar());
	}

	fiStream::SetCreateCallback(rexReport::AddToListOfCreatedFiles);
	fiStream::SetOpenCallback(rexReport::AddToListOfOpenedFiles);
	rexShellMaya& shell = *CreateShell();
	shell.SetDisplayDialogs( showDialogs );

	// Sorry about this:
	rexSerializerRAGEMesh::SetTristrippingMode(!notristrip);
	rexSerializerRAGEMesh::SetAsciiMode(ascii);
	rexUtility::SetTextureListFileName(textureListFile);
	rexUtility::SetTextureConvertFlag(notextureconvertflag);

	// By default Maya's API gives you everything in cms, so set the base unit to cms
	rexSerializer::SetBaseUnits(rexSerializer::CENTIMETERS);

	// turn on hourglass if not in quiet mode
	if( showDialogs )
	{
		MGlobal::executeCommand( "waitCursor -state on", false );
	}

	rexUtility::Startup();

	bool ok = Export( shell, selective, justExportMesh, strMeshToJustExport, justExportBound, strBoundToJustExport, strPathToJustExportTo );

	rexUtility::Shutdown();

	// Close report
	rexReport::Close();

	// if not in quiet mode, put up a nice dialog telling the user what's up
	if( showDialogs && confirmation )
	{
		if( ok )
		{
			rexMayaUtility::MessageBoxOK( "Export Completed Successfully!", "REX Complete!" ); // Could Cause ISSUES in Batch  Mode
		}
		else
		{
			rexMayaUtility::MessageBoxOK( "Export Not Completed...Check Output Window for Messages:", "REX ERRORS!"); // Could cause problems in Batch Mode
		}
 
	}
 
	// reset hourglass to regular cursor
	if( showDialogs )
	{
		MGlobal::executeCommand( "waitCursor -state off", false );
	}

	// Close the timer long, if there was one
	rexMayaTimerLog::EndTimer("MStatus rexWrapperMaya::doIt( const MArgList& args ) ");
	rexMayaTimerLog::Close();
	if(bXmlReport)
	{
		xmlAssetReport::Close(strXmlReportPathAndFilename.asChar());
	}

	// Cleanup the shell.
	delete ((rexShellMaya*)&shell);

	return ok ? MS::kSuccess : MS::kFailure;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// This is the main export function.  It takes a rexShell, registers modules and properties, and 
// calls  the rexShell's main export function.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////
bool rexWrapperMaya::Export( rexShellMaya& shell, bool selective, bool justExportMesh, const MString& strMeshToJustExport, bool justExportBound, const MString& strBoundToJustExport, const MString& strPathToJustExportTo )
{
	rexMayaTimerLog::StartTimer("bool rexWrapperMaya::Export( rexShellMaya& shell, bool selective )");
	// allow modules to register
	RegisterModules( shell );

	rexMayaSetCollectionSingleton::Instantiate();

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// selective export setup.  this will allow for specific objects and subobjects to be exported 
	// individually.
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	atArray< rexObject* > inputObjects; 

	if (selective)
	{
		MSelectionList selList;
		MGlobal::getActiveSelectionList( selList );

		int selCount = selList.length();
		for( int a = 0; a < selCount; a++ )
		{
			MDagPath dp;
			selList.getDagPath( a, dp );
			inputObjects.Grow((u16) selCount) = new rexObjectMDagPath( dp );
		}
	}

    if (rexGetCurrentHamsterObject())
    {
        rexGetCurrentHamsterObject()->SetupInputObjects(inputObjects);
    }

	if(justExportMesh)
	{
		MSelectionList selList;
		MStatus obStat = selList.add(strMeshToJustExport);
		if(obStat != MS::kSuccess)
		{
			// Unable to add for some reason
			MGlobal::executeCommand("rageError(\"Unable to find "+ strMeshToJustExport +" to export\")", false);
			return false;
		}
		MDagPath obDagPath;
		selList.getDagPath( 0, obDagPath );
		inputObjects.Grow((u16) 1) = new rexObjectMDagPath( obDagPath );
	}

	if(justExportBound)
	{
		MSelectionList selList;
		MStatus obStat = selList.add(strBoundToJustExport);
		if(obStat != MS::kSuccess)
		{
			// Unable to add for some reason
			MGlobal::executeCommand("rageError(\"Unable to find "+ strBoundToJustExport +" to export\")", false);
			return false;
		}
		MDagPath obDagPath;
		selList.getDagPath( 0, obDagPath );
		inputObjects.Grow((u16) 1) = new rexObjectMDagPath( obDagPath );
	}
	Displayf("*********************** REX start\n");
	fflush(stdout);

	// let the shell do its magic
	rexResult result = shell.Export( inputObjects, justExportMesh, justExportBound, strPathToJustExportTo.asChar() );

	// cleanup selective export arg objects
	while( inputObjects.GetCount() ) 
	{
		delete inputObjects.Pop();
	}

	rexMayaSetCollectionSingleton::Destroy();

	Displayf("*********************** rex finish\n");
	fflush(stdout);

	// display export result in maya output window
	result.Display();
	if(result.Errors())
	{
		// Badness happened
		for( int m = 0; m < result.GetMessageCount(); m++ )
		{
			MGlobal::executeCommand("rageUserError(\""+ MString(result.GetMessageByIndex(m)) +"\")", false);
		}
	}

	rexMayaTimerLog::EndTimer("bool rexWrapperMaya::Export( rexShellMaya& shell, bool selective )");
	return result.OK();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// This is the main registration function.  All the maya base wrapper does is set things up to ignore
// attributes that are handled on export item gathering by rexShellMaya
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexWrapperMaya::RegisterModules( rexShellMaya& shell )
{
	rexMayaTimerLog::StartTimer("void rexWrapperMaya::RegisterModules( rexShellMaya& shell )");
	// attributes to ignore because they are handled by rexShellMaya
	shell.RegisterProperty( "IsGroup" );
	shell.RegisterProperty( "PreExportMEL" );
	shell.RegisterProperty( "PostExportMEL" );
	shell.RegisterProperty( "UsesReferencedSubItems" );
	rexMayaTimerLog::EndTimer("void rexWrapperMaya::RegisterModules( rexShellMaya& shell )");
}
