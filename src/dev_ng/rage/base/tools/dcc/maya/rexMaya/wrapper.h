// 
// rexMaya/wrapper.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Maya REX Wrapper
//
// This file provides hooks to Maya for using as command.  Also has hooks to allow for custimization
// of shell and automatic module/property registry.
// 
//////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYA_WRAPPER_H__
#define __REX_MAYA_WRAPPER_H__

#include "rexMaya/mayahelp.h"
#include "rexMaya/shell.h"
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MPxCommand.h>
#pragma warning(pop)

#include "rexBase/hamsterObject.h"

namespace rage {

class rexWrapperMaya : public MPxCommand
{
public:
	rexWrapperMaya();
	virtual     ~rexWrapperMaya();

//	static void	*creator()			{ return new rexWrapperMaya; }

	bool        isUndoable() const  { return false; }

	MStatus     redoIt()			{ clearResult(); setResult( (int) 1); return MS::kSuccess; }
	MStatus     undoIt()			{ return MS::kSuccess; }

	MStatus      doIt(const MArgList& args);

	virtual bool Export( rexShellMaya& shell, bool selective, bool justExportMesh, const MString& strMeshToJustExport, bool justExportBound, const MString& strBoundToJustExport, const MString& strPathToJustExportTo );
	virtual bool ParseArgs( const MArgList&, int& )  { return false; }
	
	virtual rexShellMaya* CreateShell() const  { return new rexShellMaya; }
	virtual	void RegisterModules( rexShellMaya& shell );
};

} // namespace rage

#endif
