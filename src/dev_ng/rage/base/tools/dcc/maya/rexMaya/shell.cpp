#include "rexMaya/mayahelp.h"
#include "rexMaya/objectMDagPath.h"
#include "rexMaya/shell.h"
#include "rexMaya/mayaUtility.h"

#define _BOOL
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MDGModifier.h>
#include <Maya/MFnDagNode.h>
#include <Maya/MGlobal.h>
#pragma warning(pop)

#include "cranimation/animation.h"
#include "diag/output.h"
#include "string/string.h"

namespace rage {

	// For debugging only
#if __WIN32 && __DEV
#ifdef _DEBUG
#include <string.h>
bool printoutShell=0;
#endif
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexShellMaya::Reset()  
{ 
	m_BoneTagDPs.Reset(); 
	m_BoneTagIDs.Reset(); 
	m_GroupRootDPs.Reset(); 
	m_GroupIDs.Reset(); 
	rexMayaUtility::BeginRootSkeleton();
}


rexResult rexShellMaya::GetExportItemInfo( const atArray<rexObject*>& inputObjects, atArray<ExportItemInfo>& exportItems )
{
	rexResult retval( rexResult::PERFECT );

	atArray<ExportItemInfo> exportRules, exportLocalData;

	bool useRuleTemplate = true;

	// find export data root node
	MDagPath exportDataRoot;
	atString exportDataRootNodeName("|");
	exportDataRootNodeName += m_ExportDataNamespace;
	exportDataRootNodeName += "RexExportData";

	MDagPath exportRulesRoot;
	atString exportRulesRootNodeName("|");
	exportRulesRootNodeName += m_ExportDataNamespace;
	exportRulesRootNodeName += "RexExportRules";

	if( !rexMayaUtility::FindNode( exportDataRootNodeName, exportDataRoot ) )
	{
		retval.Combine( rexResult::ERRORS );
		retval.AddMessage( "%s %d %s Node Not Found...Could Not Find Anything To Export!  Make sure that "
			" the '%s' node is at the root of the Maya scene.", __FILE__, __LINE__, (const char*)exportDataRootNodeName,(const char*)exportDataRootNodeName );
		return retval;
	}

	if( !rexMayaUtility::FindNode( exportRulesRootNodeName, exportRulesRoot ) )
	{
		retval.Combine( m_ErrorWhenRuleTemplateNotFound ? rexResult::ERRORS : rexResult::WARNINGS );
		retval.AddMessage( "%s Node Not Found...using local data only (no centralized rule)!", (const char*)exportRulesRootNodeName );
		if( m_ErrorWhenRuleTemplateNotFound )
		{
			retval.AddMessage( "PLEASE RETAG AND TRY AGAIN.  YOU SEEM TO BE USING OBSOLETE DATA.  PLEASE SEE EXPORTER PROGRAMMER IF PROBLEMS STILL OCCUR." );
		}
		useRuleTemplate = false;
	}

	// get export item info.  if there are inputObjects, this is a selective export, 
	// and module's active flag should default to false
	
	int inputObjectCount = inputObjects.GetCount();
	int childCount = exportDataRoot.childCount();

	for( int a = 0; a < childCount; a++ )
	{
		MObject obj = exportDataRoot.child( a );
		MDagPath dp = MDagPath::getAPathTo( obj );			
		retval &= GetInfoRecursively( dp, useRuleTemplate ? exportLocalData : exportItems, ( inputObjectCount == 0 ) );
	}		

	if( useRuleTemplate )
	{
		childCount = exportRulesRoot.childCount();
		for( int a = 0; a < childCount; a++ )
		{
			MObject obj = exportRulesRoot.child( a );
			MDagPath dp = MDagPath::getAPathTo( obj );			
			retval &= GetInfoRecursively( dp, exportRules, ( inputObjectCount == 0 ) );
		}		
	}

	if( useRuleTemplate )
	{
		CombineExportInfo( exportRules, exportLocalData, exportItems );
	}

	// make sure the data looks good
	int exportItemCount = exportItems.GetCount();
	for( int a = 0; a < exportItemCount; a++ )
	{
		retval &= VerifyExportItems( exportItems[ a ] );
	}

	return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShellMaya::CombineExportInfo( atArray<ExportItemInfo>& exportRules, atArray<ExportItemInfo>& exportLocalData, atArray<ExportItemInfo>& exportItems )
{
	int ruleCount = exportRules.GetCount();
	int localDataCount = exportLocalData.GetCount();
	
	for( int a = 0; a < ruleCount; a++ )
	{
		ExportItemInfo& dest = exportItems.Grow((u16) ruleCount);
		dest.m_ModuleID = exportRules[ a ].m_ModuleID;
		dest.m_ObjectNameDefault = exportRules[ a ].m_ObjectNameDefault;
		dest.m_ModuleActive = exportRules[ a ].m_ModuleActive;
		dest.m_PropertyIDs = exportRules[ a ].m_PropertyIDs;
		dest.m_PropertyStrings = exportRules[ a ].m_PropertyStrings;
		dest.m_PropertyValues = exportRules[ a ].m_PropertyValues;
		dest.m_PropertyConnectedObjects = exportRules[ a ].m_PropertyConnectedObjects;
		dest.m_PropertyLocked = exportRules[ a ].m_PropertyLocked;

		dest.m_strBaseRuleFile = exportRules[ a ].m_strBaseRuleFile;
		dest.m_strMaxOrMayaIdentityString = exportRules[ a ].m_strMaxOrMayaIdentityString;

		int connObjectArrayCount = exportRules[ a ].m_PropertyConnectedObjects.GetCount();
		for( int b = 0; b < connObjectArrayCount; b++ )
		{
			int count = exportRules[ a ].m_PropertyConnectedObjects[ b ].GetCount();
			for( int c = 0; c < count; c++ )
			{
				exportRules[ a ].m_PropertyConnectedObjects[ b ][ c ] = NULL;  // don't clean up twice
			}
		}
	}

	for( int b = 0; b < localDataCount; b++ )
	{
		bool found = false;
		ExportItemInfo& localItemData = exportLocalData[ b ];
		for( int a = 0; a < ruleCount; a++ )
		{
			ExportItemInfo& dest = exportItems[ a ];
			if( ( dest.m_ModuleID == localItemData.m_ModuleID ) && ( dest.m_ObjectNameDefault == localItemData.m_ObjectNameDefault ) )
			{
				found = true;

				int itemPropertyCount = dest.m_PropertyIDs.GetCount();
				int localPropertyCount = localItemData.m_PropertyIDs.GetCount();

				Assert( ( itemPropertyCount == dest.m_PropertyValues.GetCount() ) && ( itemPropertyCount == dest.m_PropertyConnectedObjects.GetCount() ) );
				Assert( ( localPropertyCount == localItemData.m_PropertyValues.GetCount() ) && ( localPropertyCount == localItemData.m_PropertyConnectedObjects.GetCount() ) );

				for( int d = 0; d < localPropertyCount; d++ )
				{						
					bool foundProperty = false;
					int propertyConnectedObjectCount = localItemData.m_PropertyConnectedObjects[ d ].GetCount();

					for( int c = 0; c < itemPropertyCount; c++ )
					{
						if(( dest.m_PropertyStrings[ c ] == localItemData.m_PropertyStrings[ d ] ) && ( ( dest.m_PropertyValues[ c ] == localItemData.m_PropertyValues[ d ] ) || !propertyConnectedObjectCount ))
//						if(( dest.m_PropertyIDs[ c ] == localItemData.m_PropertyIDs[ d ] ) && ( ( dest.m_PropertyValues[ c ] == localItemData.m_PropertyValues[ d ] ) || !propertyConnectedObjectCount ))
						{
							foundProperty = true;
							if( propertyConnectedObjectCount )
							{
								for( int e = 0; e < propertyConnectedObjectCount; e++ )
								{
									dest.m_PropertyConnectedObjects[ c ].PushAndGrow( localItemData.m_PropertyConnectedObjects[ d ][ e ], (u16) propertyConnectedObjectCount );
									localItemData.m_PropertyConnectedObjects[ d ][ e ] = NULL; // don't clean up twice
								}
							}
							else
							{
								if( !dest.m_PropertyLocked[ c ] ) 
									dest.m_PropertyValues[ c ] = localItemData.m_PropertyValues[ d ];
							}
							break;
						}
					}					
					if( !foundProperty )
					{
						dest.m_PropertyIDs.PushAndGrow( localItemData.m_PropertyIDs[ d ],(u16) localPropertyCount );
						dest.m_PropertyValues.PushAndGrow( localItemData.m_PropertyValues[ d ],(u16) localPropertyCount );
						
						atArray< rexObject* >& pco = dest.m_PropertyConnectedObjects.Grow((u16) localPropertyCount);
						for( int e = 0; e < propertyConnectedObjectCount; e++ )
						{
							pco.PushAndGrow( localItemData.m_PropertyConnectedObjects[ d ][ e ],(u16) propertyConnectedObjectCount );
							localItemData.m_PropertyConnectedObjects[ d ][ e ] = NULL; // don't clean up twice
						}
					}
				}
				CombineExportInfo( exportRules[ a ].m_Children, localItemData.m_Children, dest.m_Children );
				break;
			}
		}

		if( !found )
		{
			ExportItemInfo& dest = exportItems.Grow((u16) localDataCount);
			dest.m_ModuleID = localItemData.m_ModuleID;
			dest.m_ObjectNameDefault = localItemData.m_ObjectNameDefault;
			dest.m_ModuleActive = localItemData.m_ModuleActive;
			dest.m_PropertyIDs = localItemData.m_PropertyIDs;
			dest.m_PropertyLocked = localItemData.m_PropertyLocked;
			dest.m_PropertyValues = localItemData.m_PropertyValues;
			dest.m_PropertyConnectedObjects = localItemData.m_PropertyConnectedObjects;
			int connObjectArrayCount = localItemData.m_PropertyConnectedObjects.GetCount();
			for( int z = 0; z < connObjectArrayCount; z++ )
			{
				int count = localItemData.m_PropertyConnectedObjects[ z ].GetCount();
				for( int c = 0; c < count; c++ )
				{
					localItemData.m_PropertyConnectedObjects[ z ][ c ] = NULL;
				}
			}
			atArray<ExportItemInfo> dummy;
			CombineExportInfo( dummy, localItemData.m_Children, dest.m_Children );
		}
	}

	return rexResult::PERFECT;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexShellMaya::shouldAddForExportProcessing(rexObject* rexObject)
{
	Assert(rexObject);
	rexObjectMDagPath* obj = (rexObjectMDagPath*)rexObject;

	//Only consider sets that are not referenced into the current scene.
	MFnDependencyNode fnDep(obj->m_DagPath.node());
	if(fnDep.isFromReferencedFile())
	{
		return false;
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

rexShellMaya::ExportItemInfo* rexShellMaya::GetInfoSelectedObject( const MDagPath& dp, const MDagPath& exportDataRootNode, atArray<ExportItemInfo>& allExportItems )
{
	rexResult retval( rexResult::PERFECT );

	atArray< atString > moduleIDStack;
	atArray< atString > nameStack;
	
	// this goes through and sets up a stack of module IDs and names for evaluating which item
	// a scene node belongs to

	for( MDagPath currDP = dp; rexMayaUtility::IsAncestorOf( exportDataRootNode, currDP ); currDP.pop() )
	{
		MObject obj = currDP.node();
		MFnDagNode fn( currDP );

		if( !rexMayaUtility::GetAttributeValue( obj, "IsGroup" ).toBool() )
		{
			MDagPath parentDP( currDP );
			parentDP.pop();
			MObject parentObj = parentDP.node();

			if( rexMayaUtility::GetAttributeValue( parentObj, "IsGroup" ).toBool() )
			{
				MFnDagNode fnParent( parentDP );
				moduleIDStack.Grow() = RemoveNamespaceAndNonAlphabeticalCharacters( atString(fnParent.name().asChar()) );
				nameStack.Grow() = RemoveNamespaceCharacters( atString(fn.name().asChar()) );
			}
			else
			{
				moduleIDStack.Grow() = nameStack[ nameStack.GetCount() - 1 ];
				nameStack.Grow() = RemoveNamespaceAndNonAlphabeticalCharacters( atString(fn.name().asChar()) );
			}
		}
	}

	int stackCount = nameStack.GetCount();
	Assert( moduleIDStack.GetCount() == stackCount );

	if( stackCount ) // this node is in export data root, so it's a module
	{
		// use id + name stacks to find export item
		ExportItemInfo* item = FindExportItemInfo( moduleIDStack, nameStack, allExportItems );
		Assert( item );
		return item;
	}
	else
	{
		Displayf( "WARNING:  SCENE NODE SELECTIVE EXPORT NOT IMPLEMENTED YET!!!" );
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexShellMaya::GetInfoRecursively( MDagPath& dp, atArray<ExportItemInfo>& currExportItemArray, bool activeDefault, const atString* moduleID )
{
	// this basically converts export data nodes into modules and properties
	rexResult retval( rexResult::PERFECT );
	
	MObject obj = dp.node();
	MFnDagNode fn( dp );

	// if "IsGroup" attribute is true, then each child node is an instance of the module whose ID 
	// is specified in this node, and uses the node name as the default name only (rather than as
	// module ID and default name, as is case with regular nodes)

	atString name;
	name = rexMayaUtility::GetAttributeValueAsString( obj, "EntityName" );
	if(!name.GetLength())
	{
		name = fn.name().asChar();
		if( !ContainsNamespaceString( name ) )
		{
			retval.Set( rexResult::WARNINGS );
			retval.AddMessage( "Improperly named export data node present!  (%s)", dp.fullPathName().asChar() );
			return retval;
		}
	}

	if( rexMayaUtility::GetAttributeValue( obj, "IsGroup" ).toBool() )
	{
		int childCount = dp.childCount();
		for( int a = 0; a < childCount; a++ )
		{
			MObject obj = dp.child( a );
			MDagPath childDP = MDagPath::getAPathTo( obj );						
			name = RemoveNamespaceAndNonAlphabeticalCharacters( name );
			retval &= GetInfoRecursively( childDP, currExportItemArray, activeDefault, &name );
		}
		return retval;
	}

	// create the new item and set its active bit
	ExportItemInfo& exportItem = currExportItemArray.Grow();
	exportItem.m_ModuleActive = activeDefault;
	

	// if "IsGroup" attribute is true, then each child node is an instance of the module whose ID 
	// is specified in this node, and uses the node name as the default name only (rather than as
	// module ID and default name, as is case with regular nodes)

	exportItem.m_ModuleID = moduleID ? *moduleID : RemoveNamespaceAndNonAlphabeticalCharacters( name );
	exportItem.m_ObjectNameDefault = RemoveNamespaceCharacters( name );

	// Fill in useful debug stuff
	exportItem.m_strMaxOrMayaIdentityString = dp.fullPathName().asChar();
	MDagPath obDagPathForSearchingForBaseRuleAttribute = dp;
	exportItem.m_strBaseRuleFile = "";
	while(obDagPathForSearchingForBaseRuleAttribute.length() > 1)
	{
		MFnDependencyNode obFnDependencyNodeOfExportItem(obDagPathForSearchingForBaseRuleAttribute.node());
		MStatus obStatus;
		MPlug obPlugOfBaseRuleAttribute = obFnDependencyNodeOfExportItem.findPlug("BaseRule", &obStatus);
		if(obStatus == MS::kSuccess)
		{
			// Found attribute
			MString obValueOfBaseRuleAttribute;
			obPlugOfBaseRuleAttribute.getValue(obValueOfBaseRuleAttribute);
			exportItem.m_strBaseRuleFile = obValueOfBaseRuleAttribute.asChar();

			if(exportItem.m_strBaseRuleFile == "")
			{
				// Found attribute, but it is blank
				retval.Set( rexResult::ERRORS );
				retval.AddMessage( "The BaseRule attribute on %s is an empty string.  This is impossible.  Please tell a member of your tool team.", obDagPathForSearchingForBaseRuleAttribute.fullPathName().asChar() );
				return retval;
			}

			break;
		}
		obDagPathForSearchingForBaseRuleAttribute.pop();
	}
	if(exportItem.m_strBaseRuleFile == "")
	{
		// Unable to find base rule attribute
		retval.Set( rexResult::ERRORS );
		retval.AddMessage( "Unable to find BaseRule attribute on %s or any of its parents.  This is impossible.\n", dp.fullPathName().asChar() );
		return retval;
	}

	// retrieve user defined attribute names, and convert information to properties
	atArray<atString> attributeNames;
	rexMayaUtility::GetUserDefinedAttributeNames( obj, attributeNames );		


	int attCount = attributeNames.GetCount();
	for( int a = 0; a < attCount; a++ )
	{
		// each property needs ID, Value, and Connected Object array
		exportItem.m_PropertyIDs.PushAndGrow( rexUtility::RemoveNonAlphabeticCharacters( attributeNames[ a ] ), (u16) attCount );
		exportItem.m_PropertyStrings.PushAndGrow( attributeNames[ a ], (u16) attCount ); 
		exportItem.m_PropertyValues.PushAndGrow( rexMayaUtility::GetAttributeValue( obj, attributeNames[ a ] ), (u16) attCount );
		exportItem.m_PropertyLocked.PushAndGrow( rexMayaUtility::IsAttrLocked( fn, attributeNames[ a ], false ), (u16) attCount );
		atArray< rexObject* >& connectionObjectArray = exportItem.m_PropertyConnectedObjects.Grow((u16) attCount);

		atArray<MDagPath> connections;
		rexMayaUtility::GetAttributeConnections( obj, attributeNames[ a ], connections, true, false );
		int connectionCount = connections.GetCount();
		for( int b = 0; b < connectionCount; b++ )
		{
			connectionObjectArray.PushAndGrow( new rexObjectMDagPath( connections[ b ] ),(u16) connectionCount );
		}
	}

	// gather child export items
	int childCount = dp.childCount();
	for( int a = 0; a < childCount; a++ )
	{
		MObject childObj = dp.child( a );
		MDagPath childDp = MDagPath::getAPathTo( childObj );
		retval &= GetInfoRecursively( childDp, exportItem.m_Children, activeDefault );
	}		

	// this allows for one export item to be comprised of data from multiple scenes. (i.e. giant world from
	// individual scene files).  it accomplishes this by finding all matching modules and properties in 
	// referenced scene export data nodes, and combining the connections into the root scene's export data.
	// the only difference between the export data tree of the root scene and the referenced scene(s) is that
	// the root scene's export data root node(s) contains an attribute called "UsesReferencedSubItems" 
	// which is set to true.
	AssimilateReferencedExportDataPropertyConnections( exportItem, obj );

	return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

rexShell::Iterator*	rexShellMaya::GetIterator( ExportItemInfo& exportItem )
{
	// create and setup new maya iterator for use by base shell
	atArray<MDagPath> dpArray;
	GetInputDagPathsRecursively( exportItem, dpArray );
	return new MayaIterator( *this, dpArray, m_BoneTagDPs, m_BoneTagIDs, m_GroupRootDPs, m_GroupIDs, m_SpecialFlagDPs, m_SpecialFlagIDs );
}

void rexShellMaya::GetInputDagPathsRecursively( ExportItemInfo& exportItem, atArray<MDagPath>& dpArray )
{
	if( !exportItem.m_ModuleInstance ) 
		return;

	// this function just makes sure all the nodes tagged by modules, and their descendants, will be
	// considered for export.  it also ensures that no node will be traversed twice

	int inputObjectCount = exportItem.m_ModuleInstance->m_InputObjects.GetCount();
	for( int a = 0; a < inputObjectCount; a++ )
	{
		const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( exportItem.m_ModuleInstance->m_InputObjects[ a ] );
		if( dpObj )
		{
			int dpCount = dpArray.GetCount();
			bool existsOrIsDescendantOfExistingDP = false;

			// make sure this node or an ancestor isn't already set as a root node
			for( int b = 0; b < dpCount; b++ )
			{
				if( rexMayaUtility::IsAncestorOf( dpArray[ b ], dpObj->m_DagPath ) || ( dpArray[ b ] == dpObj->m_DagPath ))
				{
					existsOrIsDescendantOfExistingDP = true;
					break;
				}
					
			}
			if( !existsOrIsDescendantOfExistingDP )
			{
				dpArray.PushAndGrow( dpObj->m_DagPath,(u16) inputObjectCount );
			}
		}
	}

	// scoop up the children's tagged nodes
	int childCount = exportItem.m_Children.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		GetInputDagPathsRecursively( exportItem.m_Children[ a ], dpArray );
	}

	// clean up any dag paths that might have snuck in before their ancestors
	int dpCount = dpArray.GetCount();
	for( int a = 0; a < dpCount; a++ )
	{
		for( int b = 0; b < dpCount; b++ )
		{
			if( a != b )
			{
				if( rexMayaUtility::IsAncestorOf( dpArray[ b ], dpArray[ a ] ) || ( dpArray[ b ] == dpArray[ a ] ))
				{
					// If we want to delete the element, restart the inner loop, making sure the outer loop
					// repeats its pass on this same "a".
					dpArray.Delete( a );
					dpCount--;
					a--;
					break;
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

atString rexShellMaya::GetInputObjectHashString( rexObject& obj )
{
	// this just uses the dag path stuff to create a unique string ID (for use with hash tables)
	atString retval;
	const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &obj );
	if( dpObj )
	{
		retval = dpObj->m_DagPath.fullPathName().asChar();
	}
	return retval;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////



bool rexShellMaya::IsObjectPartOfInput( const rexObject& obj, const atArray<rexObject*>& inputObjects, bool includeChildren ) const
{
	// if includeChildren set, this function returns true if obj is a member of, or a descendent of a member of, the inputObjects
	// array.  if includeChildren not set, function retures true if obj is a member of the inputObjects array

	const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &obj );
	if( dpObj )
	{
		int inputObjectCount = inputObjects.GetCount();
		for( int a = 0; a < inputObjectCount; a++ )
		{			
			const rexObjectMDagPath* dpObj2 = dynamic_cast<const rexObjectMDagPath*>( inputObjects[ a ] );
			if( dpObj2 )
			{
				if( ( dpObj->m_DagPath == dpObj2->m_DagPath ) || includeChildren && rexMayaUtility::IsAncestorOf( dpObj2->m_DagPath, dpObj->m_DagPath ) )
				{
					return true;
				}
			}
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

rexShellMaya::MayaIterator::MayaIterator( const rexShellMaya& pShell, const atArray<MDagPath>& dpArray, const atArray<MDagPath>& boneTagDPs, const atArray<atString>& boneTagIDs, const atArray<MDagPath>& groupRootDPs, const atArray<atString>& groupIDs, const atArray<MDagPath>& specialFlagDPs, const atArray<atString>& specialFlagIDs ) : rexShell::Iterator()
{
	for(int i=0; i<dpArray.GetCount(); i++)
	{
		Displayf("dpArray[%d]        = %s", i, dpArray[i].fullPathName().asChar());
	}
	for(int i=0; i<boneTagDPs.GetCount(); i++)
	{
		Displayf("boneTagDPs[%d]     = %s", i, boneTagDPs[i].fullPathName().asChar());
	}
	for(int i=0; i<boneTagIDs.GetCount(); i++)
	{
		Displayf("boneTagIDs[%d]     = %s", i, boneTagIDs[i]);
	}
	for(int i=0; i<groupRootDPs.GetCount(); i++)
	{
		Displayf("groupRootDPs[%d]   = %s", i, groupRootDPs[i].fullPathName().asChar());
	}
	for(int i=0; i<groupIDs.GetCount(); i++)
	{
		Displayf("groupIDs[%d]       = %s", i, groupIDs[i]);
	}
	for(int i=0; i<specialFlagDPs.GetCount(); i++)
	{
		Displayf("specialFlagDPs[%d] = %s", i, specialFlagDPs[i].fullPathName().asChar());
	}
	for(int i=0; i<specialFlagIDs.GetCount(); i++)
	{
		Displayf("specialFlagIDs[%d] = %s", i, specialFlagIDs[i]);
	}
	// make sure we are setting up bones, meshes, etc. while in frame 0
	rexMayaUtility::SetFrame( 0 );

	// init variables and stacks
	ParentShell = &pShell;
	BoneCount = 0;
	LODGroupCount = 0;
	BoneIndexStack.PushAndGrow( 0 );
	BoneMatrixStack.PushAndGrow( M44_IDENTITY );
	GroupIDStack.PushAndGrow( atString("") );
	GroupIndexStack.PushAndGrow( -1 );
	LODGroupIndexStack.PushAndGrow( -1 );
	LODLevelStack.PushAndGrow( -1 );
	LODThresholdStack.PushAndGrow( 9999.0f );
	SpecialFlagStack.Grow();
	m_IgnoreBonesInLevelInstanceObjects = pShell.m_IgnoreBonesInLevelInstanceObjects;
    
	// setup hash tables with bone tag information in order to speed up checking for user-defined bones
	int boneTagCount = boneTagDPs.GetCount();
	Assert( boneTagCount == boneTagIDs.GetCount() );
	for( int a = 0; a < boneTagCount; a++ )
	{
		atArray<atString>* tagIDs = BoneTagMap.Access( boneTagDPs[ a ].partialPathName().asChar() );

		if( tagIDs )
		{
			tagIDs->PushAndGrow( boneTagIDs[ a ] );
		}
		else
		{
			atArray<atString> array;
			array.PushAndGrow( boneTagIDs[ a ], (u16) boneTagCount );
			Displayf("%s %d Adding %s to bone tag map", __FILE__, __LINE__, boneTagDPs[ a ].partialPathName().asChar());
			BoneTagMap.Insert( atString(boneTagDPs[ a ].partialPathName().asChar()), array );
		}
	}

	// setup hash tables with special flag information in order to speed up assignment
	int specialFlagCount = specialFlagDPs.GetCount();
	Assert( specialFlagCount == specialFlagIDs.GetCount() );
	for( int a = 0; a < specialFlagCount; a++ )
	{
		atArray<atString>* tagIDs = SpecialFlagMap.Access( specialFlagDPs[ a ].partialPathName().asChar() );

		if( tagIDs )
		{
			tagIDs->PushAndGrow( specialFlagIDs[ a ] );
		}
		else
		{
			atArray<atString> array;
			array.PushAndGrow( specialFlagIDs[ a ] );
			SpecialFlagMap.Insert( atString(specialFlagDPs[ a ].partialPathName().asChar()), array );
		}
	}

	// setup hash tables with group information in order to speed up checking for group root nodes
	int groupCount = groupRootDPs.GetCount();
	Assert( groupCount == groupIDs.GetCount() );
	for( int a = 0; a < groupCount; a++ )
	{
		GroupInfo* groupInfo = GroupRootMap.Access( groupRootDPs[ a ].partialPathName().asChar() );

		if( !groupInfo )
		{
			GroupInfo newGroupInfo;
			newGroupInfo.ID = groupIDs[ a ];
			newGroupInfo.Index = a;
			GroupRootMap.Insert( atString(groupRootDPs[ a ].partialPathName().asChar()), newGroupInfo );
		}
	}

	// copy array of root dag paths
	DPArray = dpArray;

	if( !DPArray.GetCount() )
	{
		CurrentDPIndex = -1;  // this means it has no more objects, and will return NULL when asked for object
	}
	else
	{
		// this sets up the iterator with the first root dp.  it will return its descendents' nodes in 
		// depth first order, then reset the iterator to the next root dp, and repeat until all root nodes
		// have been taken care of
		CurrentDPIndex = 0;
		DagIterator.reset( DPArray[0] );
	}

	// gotta have a flag here so i don't advance the maya iterator before grabbing the first node
	PulledFirstObject = false;
}

///////////////////////////

rexObject* rexShellMaya::MayaIterator::GetNextObject( int& depthDelta )
{
	// out of root dps to explore
	if( CurrentDPIndex < 0 )
		return NULL;

	unsigned prevDepth = DagIterator.depth();

	if( PulledFirstObject )
	{
#if 0
		DagIterator.next();
#else  // only use this to exclude old style bangers/instances
		while(! DagIterator.isDone() )
		{
			DagIterator.next();

			MDagPath path;
			DagIterator.getPath( path );

			if( !( ( path.apiType() == MFn::kPluginTransformNode ) || rexMayaUtility::HasTypeAsAncestor( path, MFn::kPluginTransformNode ) ) )
				break;
		}
#endif		
	}

	depthDelta = (int)DagIterator.depth() - (int)prevDepth;
	
	// when iterator is done, move on to the next root dagpath.  if no more, set current DP to -1 and return NULL
	if( DagIterator.isDone() )
	{
		int dpCount = DPArray.GetCount();

		if( ++CurrentDPIndex >= dpCount )
		{
			CurrentDPIndex = -1;
			return NULL;
		}
		else
		{
			DagIterator.reset( DPArray[ CurrentDPIndex ] );
		}
	}

	MDagPath path;
	DagIterator.getPath( path );

	// retrieve all bone tags associated with this node
	atArray<atString>* bonetags = BoneTagMap.Access( path.partialPathName().asChar() );

	// retrieve all special flags associated with this node
	atArray<atString>* specialFlags = SpecialFlagMap.Access( path.partialPathName().asChar() );

	Assert( ParentShell );
	// a node is a bone root (top node in new bone, not root bone) when it is the first object, when it is tagged as a bone,
	// or when it is a joint or animated transform node.
	bool childOfLevelInstanceObject = rexMayaUtility::IsChildOfLevelInstanceObject( path, false, true );
	bool isBoneRoot = (!m_IgnoreBonesInLevelInstanceObjects || !childOfLevelInstanceObject ) && 
					  (!PulledFirstObject) || rexMayaUtility::IsValidBoneNode( path, ParentShell->GetSkeletonUsesOnlyJoints(), ParentShell->GetIgnoreNullJoints() ) || bonetags;
//	Displayf("%s %d %s", __FILE__, __LINE__, path.fullPathName().asChar());
//	Displayf("m_IgnoreBonesInLevelInstanceObjects = %d childOfLevelInstanceObject = %d PulledFirstObject = %d rexMayaUtility::IsValidBoneNode( %s, %d ) = %d %p", m_IgnoreBonesInLevelInstanceObjects, childOfLevelInstanceObject, PulledFirstObject, path.fullPathName().asChar(), ParentShell->GetSkeletonUsesOnlyJoints(), rexMayaUtility::IsValidBoneNode( path, ParentShell->GetSkeletonUsesOnlyJoints() ), bonetags);
	bool isLodGroupNode = ( path.apiType() == MFn::kLodGroup );
	
	MDagPath parent( path );
	parent.pop();
	bool isParentLodGroupNode = ( parent.apiType() == MFn::kLodGroup );
	
	// grab current values for various properties of this particular object
	int currBone = BoneIndexStack.Pop();
	Matrix44 currMatrix = BoneMatrixStack.Pop();
	int currLodGroupIndex = LODGroupIndexStack.Pop();
	int currLodLevel = LODLevelStack.Pop();
	float currLODThreshold = LODThresholdStack.Pop();
	atString currGroupID = GroupIDStack.Pop();
	int currGroupIndex = GroupIndexStack.Pop();
	atArray<atString> currSpecialFlags(SpecialFlagStack.Pop());
	
	// adjust stacks based on traversal depth delta
	if( !depthDelta )
	{
		currBone = BoneIndexStack.GetCount() ? BoneIndexStack[ BoneIndexStack.GetCount() - 1 ] : 0;
		currMatrix = BoneMatrixStack.GetCount() ? BoneMatrixStack[ BoneMatrixStack.GetCount() - 1 ] : M44_IDENTITY;
		currLodGroupIndex = LODGroupIndexStack.GetCount() ? LODGroupIndexStack[ LODGroupIndexStack.GetCount() - 1 ] : -1;
		currLodLevel = LODLevelStack.GetCount() ? LODLevelStack[ LODLevelStack.GetCount() - 1 ] : -1;
		currLODThreshold = LODThresholdStack.GetCount() ? LODThresholdStack[ LODThresholdStack.GetCount() - 1 ] : 9999.0f;
		currGroupID = GroupIDStack.GetCount() ? GroupIDStack[ GroupIDStack.GetCount() - 1 ] : "";
		currGroupIndex = GroupIndexStack.GetCount() ? GroupIndexStack[ GroupIndexStack.GetCount() - 1 ] : -1;
		if( SpecialFlagStack.GetCount() )
			currSpecialFlags = SpecialFlagStack[ GroupIDStack.GetCount() - 1 ];
		else if( currSpecialFlags.GetCount() )
			currSpecialFlags.Reset();
	}
	else if( depthDelta > 0 )
	{
		for( int b = 0; b < depthDelta; b++ )
		{
			BoneIndexStack.PushAndGrow( currBone, (u16) depthDelta );
			BoneMatrixStack.PushAndGrow( currMatrix, (u16) depthDelta );
			LODGroupIndexStack.PushAndGrow( currLodGroupIndex, (u16) depthDelta );
			LODLevelStack.PushAndGrow( currLodLevel, (u16) depthDelta );			
			LODThresholdStack.PushAndGrow( currLODThreshold, (u16) depthDelta );
			GroupIDStack.PushAndGrow( currGroupID, (u16) depthDelta );
			GroupIndexStack.PushAndGrow( currGroupIndex, (u16) depthDelta );
			SpecialFlagStack.PushAndGrow( currSpecialFlags, (u16) depthDelta );
		}				
	}
	else if( depthDelta < 0 )
	{
		for( int b = 0; b < abs( depthDelta ); b++ )
		{
			currBone = BoneIndexStack.Pop();
			currMatrix = BoneMatrixStack.Pop();
			currLodGroupIndex = LODGroupIndexStack.Pop();
			currLodLevel = LODLevelStack.Pop();
			currLODThreshold = LODThresholdStack.Pop();
			currGroupID = GroupIDStack.Pop();
			currGroupIndex = GroupIndexStack.Pop();
			currSpecialFlags = SpecialFlagStack.Pop();
		}

		currBone = BoneIndexStack.GetCount() ? BoneIndexStack[ BoneIndexStack.GetCount() - 1 ] : 0;
		currMatrix = BoneMatrixStack.GetCount() ? BoneMatrixStack[ BoneMatrixStack.GetCount() - 1 ] : M44_IDENTITY;
		currLodGroupIndex = LODGroupIndexStack.GetCount() ? LODGroupIndexStack[ LODGroupIndexStack.GetCount() - 1 ] : -1;
		currLodLevel = LODLevelStack.GetCount() ? LODLevelStack[ LODLevelStack.GetCount() - 1 ] : -1;
		currLODThreshold = LODThresholdStack.GetCount() ? LODThresholdStack[ LODThresholdStack.GetCount() - 1 ] : 9999.0f;
		currGroupID = GroupIDStack.GetCount() ? GroupIDStack[ GroupIDStack.GetCount() - 1 ] : "";
		currGroupIndex = GroupIndexStack.GetCount() ? GroupIndexStack[ GroupIndexStack.GetCount() - 1 ] : -1;
		if( SpecialFlagStack.GetCount() )
			currSpecialFlags = SpecialFlagStack[ SpecialFlagStack.GetCount() - 1 ];
		else if( currSpecialFlags.GetCount() )
			currSpecialFlags.Reset();
	}		

	// see if this node is group root
	GroupInfo* groupInfo = GroupRootMap.Access( path.partialPathName().asChar() );
	if( groupInfo )
	{
		currGroupID = groupInfo->ID;
		currGroupIndex = groupInfo->Index;
	}

//	Displayf("%s %d %s %d isBoneRoot = %d", __FILE__, __LINE__, path.fullPathName().asChar(), currBone, isBoneRoot);
	if( isBoneRoot )
	{
		// need to set up new bone
		currBone = BoneCount++;
		currMatrix = rexMayaUtility::GetSkeletonWorldMatrix( path );
		rexMayaUtility::AddRootSkeletonBone(path.fullPathName().asChar(),currBone);
	}

	if( isLodGroupNode )
	{
		// set up new LOD group
		currLodGroupIndex = LODGroupCount++;		
	}

	if( specialFlags )
	{
		int flagCount = specialFlags->GetCount();
		for( int a = 0; a < flagCount; a++ )
		{
			//Don't add duplicate flags to the currSpecialFlags array (this will occur if an node has been tagged twice)
			if(currSpecialFlags.Find((*specialFlags)[ a ]) == -1)
				currSpecialFlags.PushAndGrow( (*specialFlags)[ a ], (u16) flagCount );
		}
	}

	if( isParentLodGroupNode )
	{
		// find out which LOD level this node is in
		int parentChildCount = parent.childCount();
		currLodLevel = -1;
		for( int a = 0; a < parentChildCount; a++ )
		{
			MDagPath parentChildDP = MDagPath::getAPathTo( parent.child( a ) );
			if( parentChildDP == path )
			{
				currLodLevel = a;
				break;
			}
		}
		Assert( currLodLevel >= 0 );		
	}

	// push current values onto stacks
	BoneIndexStack.PushAndGrow( currBone );
	BoneMatrixStack.PushAndGrow( currMatrix );
	LODGroupIndexStack.PushAndGrow( currLodGroupIndex );
	LODLevelStack.PushAndGrow( currLodLevel );
	LODThresholdStack.PushAndGrow( currLODThreshold );
	GroupIDStack.PushAndGrow( currGroupID );
	GroupIndexStack.PushAndGrow( currGroupIndex );
	SpecialFlagStack.PushAndGrow( currSpecialFlags );

	// set this flag so that the iterator will advance before grabbing next node
	PulledFirstObject = true;

    // return the object to be used by your converters
	rexObjectMDagPath* newObj = new rexObjectMDagPath( path, currBone, currMatrix, isBoneRoot, currLodGroupIndex, currLodLevel, currLODThreshold, currGroupIndex, currGroupID, childOfLevelInstanceObject );
	newObj->m_SpecialFlags = currSpecialFlags;
	if( bonetags )
		newObj->m_BoneTags = *bonetags;

	return newObj;
}

int	rexShellMaya::MayaIterator::GetObjectCount() const
{
	// this is used for setting up progress bar max value
	int total = 0;
	
	int dpCount = DPArray.GetCount();
	for( int a = 0; a < dpCount; a++ )
	{
		total += ( rexMayaUtility::GetDescendantCount( DPArray[ a ] ) + 1 );
	}

	return total;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexShellMaya::OpenProgressWindow(const char *title, int numValues)
{
	if( !m_DisplayDialogs )
		return true;
	return rexMayaUtility::OpenProgressWindow( title, numValues );
}

bool rexShellMaya::CloseProgressWindow(void)
{
	if( !m_DisplayDialogs )
		return true;
	return rexMayaUtility::CloseProgressWindow();
}

bool rexShellMaya::UpdateProgressWindowInfo(int index, const char *info)
{
	if( !m_DisplayDialogs )
		return true;
	return rexMayaUtility::UpdateProgressWindowInfo( index, info );
}

bool rexShellMaya::UpdateProgressWindowMax(int index, int max)
{
	if( !m_DisplayDialogs )
		return true;
	return rexMayaUtility::UpdateProgressWindowMax( index, max );
}

bool rexShellMaya::UpdateProgressWindowValue(int index, int value)
{
	if( !m_DisplayDialogs )
		return true;
	return rexMayaUtility::UpdateProgressWindowValue( index, value );
}

void ProgressBarCountCB( int count )
{
	rexMayaUtility::UpdateProgressWindowMax( 2, count );
}

void ProgressBarCurrIndexCB( int value )
{
	rexMayaUtility::UpdateProgressWindowValue( 2, value );
	if( !( value & 7 ) )
		MGlobal::executeCommand( "refresh", false );
}

void ProgressBarCurrInfoCB( const char* s )
{
	rexMayaUtility::UpdateProgressWindowInfo( 2, s );
}

void rexShellMaya::ProgressBarCountCB( int count )
{
    if( m_DisplayDialogs )
        rage::ProgressBarCountCB(count);
}

void rexShellMaya::ProgressBarCurrIndexCB( int value )
{
    if( m_DisplayDialogs )
        rage::ProgressBarCurrIndexCB(value);
}

void rexShellMaya::ProgressBarCurrInfoCB( const char* s )
{
    if( m_DisplayDialogs )
        rage::ProgressBarCurrInfoCB(s);
}

rexProgressBarCallback	rexShellMaya::GetProgressBarCurrValueCB()
{
    return m_DisplayDialogs ? rage::ProgressBarCurrIndexCB : NULL;
}
rexProgressBarCallback	rexShellMaya::GetProgressBarItemCountCB()
{
    return m_DisplayDialogs ? rage::ProgressBarCountCB : NULL;
}

rexProgressBarTextCallback	rexShellMaya::GetProgressBarInfoCB()
{
    return m_DisplayDialogs ? rage::ProgressBarCurrInfoCB : NULL;
}

bool rexShellMaya::ContainsNamespaceString( const char * s )
{
	return( strstr( s, m_ExportDataNamespace ) != NULL );
}

atString rexShellMaya::RemoveNamespaceCharacters( const char * name )
{
	atString tempname;

	const char* instr = strstr( name, m_ExportDataNamespace );
	if( instr )
		return atString() = instr + m_ExportDataNamespace.GetLength();

	return atString(name);
}

atString rexShellMaya::RemoveNamespaceAndNonAlphabeticalCharacters( const char * name )
{
	return rexUtility::RemoveNonAlphabeticCharacters( RemoveNamespaceCharacters( name ) );
}

rexShellMaya::ExportItemInfo* rexShellMaya::FindExportItemInfo( const atArray<atString>& moduleIDStack_, const atArray<atString>& nameStack_, atArray<ExportItemInfo>& exportItems )
{
	// n8 -- i'll comment this better once i get it to reliably work  (this is for selective export)
	atArray<atString> moduleIDStack(moduleIDStack_);
	atArray<atString> nameStack(nameStack_);

	atString currModuleID = moduleIDStack.Pop();
	atString currName = nameStack.Pop();

	Assert( nameStack.GetCount() == moduleIDStack.GetCount() );

	int itemCount = exportItems.GetCount();

	for( int a = 0; a < itemCount; a++ )
	{
		if( ( currModuleID == exportItems[ a ].m_ModuleID ) && ( currName == exportItems[ a ].m_ObjectNameDefault ) )
		{
			if( nameStack.GetCount() )
				return FindExportItemInfo( moduleIDStack, nameStack, exportItems[ a ].m_Children );
			return &exportItems[ a ];
		}
	}	

	return NULL;
}

void rexShellMaya::SetModuleActiveFlagRecursively( ExportItemInfo& rootItem, bool active )
{
	rootItem.m_ModuleActive = active;
	int childCount = rootItem.m_Children.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		SetModuleActiveFlagRecursively( rootItem.m_Children[ a ], active );
	}
}


void rexShellMaya::CopyModuleInputObjects( rexModule& dest, const rexModule& source )
{
	int objCount = source.m_InputObjects.GetCount();
	for( int a = 0; a < objCount; a++ )
	{
		rexObjectMDagPath* o = dynamic_cast<rexObjectMDagPath*>( source.m_InputObjects[ a ] );
		if( o )
		{
			dest.m_InputObjects.PushAndGrow( new rexObjectMDagPath( o ),(u16) objCount );
		}
	}				
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

bool rexShellMaya::AssimilatePropertyConnections( rexShell::ExportItemInfo& rootExportItem, const rexShell::ExportItemInfo& referencedExportItem )
{
	if( rootExportItem.m_ModuleID == referencedExportItem.m_ModuleID )
	{
		// assimilate all matching properties' connections
		int rootPropertyCount = rootExportItem.m_PropertyIDs.GetCount();
		int refPropertyCount = referencedExportItem.m_PropertyIDs.GetCount();

		for( int r = 0; r < rootPropertyCount; r++ )
		{
			for( int s = 0; s < refPropertyCount; s++ )
			{
				if( rootExportItem.m_PropertyIDs[ r ] == referencedExportItem.m_PropertyIDs[ s ] )
				{
					int connectedObjectCount = referencedExportItem.m_PropertyConnectedObjects[ s ].GetCount();
					for( int t = 0; t < connectedObjectCount; t++ )
					{
						rootExportItem.m_PropertyConnectedObjects[ r ].PushAndGrow( referencedExportItem.m_PropertyConnectedObjects[ s ][ t ],(u16) connectedObjectCount );
					}							
					break;
				}
			}
		}

		// do children
		int rootChildCount = rootExportItem.m_Children.GetCount();
		int refChildCount = referencedExportItem.m_Children.GetCount();
		for( int c = 0; c < rootChildCount; c++ )
		{
			for( int d = 0; d < refChildCount; d++ )
			{
				// if it succesfully assimilates, no need to keep looking for match
				if( AssimilatePropertyConnections( rootExportItem.m_Children[ c ], referencedExportItem.m_Children[ d ] ) )
					break;
			}			
		}
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

void rexShellMaya::AssimilateReferencedExportDataPropertyConnections( rexShell::ExportItemInfo& exportItem, MObject obj )
{
	if( rexMayaUtility::GetAttributeValue( obj, "UsesReferencedSubItems" ).toBool() )
	{
		MDagPath sceneRoot = rexMayaUtility::GetSceneRootDagPath();
		int childCount = sceneRoot.childCount();
		atArray<ExportItemInfo> referencedItemInfo;

		// go through all root nodes to find referenced scenes' export data
		for( int a = 0; a < childCount; a++ )
		{
			MObject childObj = sceneRoot.child( a );
			MDagPath childDP = MDagPath::getAPathTo( childObj );
			MFnDagNode childFn( childObj );

			// is this a referenced scene's export data?
			if( ( strncmp( childFn.name().asChar(), m_ExportDataNamespace, m_ExportDataNamespace.GetLength() ) != 0 ) && ( strstr( childFn.name().asChar(), "RexExportData" ) ) )
			{
				int subExportDataChildCount = childFn.childCount();

				// gather the export items contained in this export data tree into referencedItemInfo
				for( int b = 0; b < subExportDataChildCount; b++ )
				{
					MObject subExportDataChildObj = childFn.child( b );
					MDagPath subExportDataChildDP = MDagPath::getAPathTo( subExportDataChildObj );
					MFnDagNode subExportDataChildFn( subExportDataChildObj );

					if( strstr( subExportDataChildFn.name().asChar(), exportItem.m_ModuleID ) )
					{
						atString name(RemoveNamespaceAndNonAlphabeticalCharacters( atString(subExportDataChildFn.name().asChar()) ));
						GetInfoRecursively( subExportDataChildDP, referencedItemInfo, true, &name );
					}
				}
			}
		}

		// go through referencedItemInfo, and when item matches root scene export data item info, connections for 
		// each attribute are added into the root scene export item information

		int refItemCount = referencedItemInfo.GetCount();
		for( int c = 0; c < refItemCount; c++ )
		{
			AssimilatePropertyConnections( exportItem, referencedItemInfo[ c ] );
		}
	}
}

/*
PURPOSE
	Set up whatever is needed in maya scene.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void rexShellMaya::SetupExportItems(const atArray<ExportItemInfo>& items)
{
	int count = items.GetCount();
	if( count <= 0 )
		return;

	rexMayaUtility::ConstructNamespace();

	int sf = 0;
	int ef = 0;
	rexMayaUtility::GetMayaSceneStartAndEndFrames(sf, ef);
	if( (ef - sf) <= 0 )
		return;

	// Display message.
	ProgressBarCurrInfoCB( "Baking simulation ..." );

	//=========================================================================
	// The example of bakeResults command: bake two skeleton.
	// bakeResults -simulation true -hierarchy below -sampleBy 1 -disableImplicitControl true -preserveOutsideKeys true -sparseAnimCurveBake false -controlPoints false -shape false -t "0:150" {"joint1", "joint5"};
	//=========================================================================

	// Build a DG modifier to bake simulation for animated entities.
	m_BakeCmd = new MDGModifier;
	// bake all selected root and below and all animatable attributes.
//	MString baseCmd("bakeResults -sm true -hi below -sb 1 -dic true -pok true -sac false -cp false -s false ");
	// bake all selected nodes and all animatable attributes.
	MString baseCmd("bakeResults -sm true -sb 1 -dic true -pok true -sac false -cp false -s false ");
	baseCmd += "-t \"";
	baseCmd += sf;
	baseCmd += ":";
	baseCmd += ef;
	baseCmd += "\" { ";

	int nodeCount = 0;
	MString cmd("");
	for( int i = 0; i < count; i++ )
	{
		atArray<MDagPath> dpRoots;
		GetAnimationSegmentRootNodes(items[i], dpRoots);

		int rootCount = dpRoots.GetCount();
		for( int r = 0; r < rootCount; r++ )
		{
// For debugging only
#if __WIN32 && __DEV
#ifdef _DEBUG
if(printoutShell)
Printf(" SetupExportItems: item%d root%d=%s\n",i,r,dpRoots[r].partialPathName().asChar());
#endif
#endif
			BuildCommandFromEntityNode(dpRoots[r], cmd, nodeCount);
		}
	}

	// DISABLED BAKING:
	if(0)
	//	if( nodeCount > 0 )
	// when attempting to export anything with trax baking will result in all translations and rotations being exported with double their value!
	// seems to result from baked result being added back onto trax result.
	// since baking only seems to be required by curve exporting (and that isn't working properly at the moment anyway) disabling seemed quick solution.
	{
		baseCmd += cmd;
		baseCmd += " };";
// For debugging only
#if __WIN32 && __DEV
#ifdef _DEBUG
if(printoutShell)
if(baseCmd.length() < 4096)
Printf(" SetupExportItems: bakeCmd=%s nodeCount=%d\n",baseCmd.asChar(),nodeCount);
#endif
#endif
		// Hide all before baking in order to save time.
		// Can't hide ikHandles, otherwise ik won't be baked.
		//m_BakeCmd->commandToExecute( "HideAll" );
		m_BakeCmd->commandToExecute( "hideShow -geometry -hide;" );
		//m_BakeCmd->commandToExecute( "hideShow -joints -hide;" );
		//m_BakeCmd->commandToExecute( "hideShow -deformers -hide;" );
		m_BakeCmd->commandToExecute( baseCmd );
		m_BakeCmd->doIt();

		// filter the euler curve for rotation attributes
		MStringArray nodeNames;
		cmd.split(',', nodeNames);
		MString filterCmd( "filterCurve -filter euler " );
		for( int i = 0; i < nodeCount; i++ )
		{
			filterCmd += nodeNames[i];
			filterCmd += ".rx ";
			filterCmd += nodeNames[i];
			filterCmd += ".ry ";
			filterCmd += nodeNames[i];
			filterCmd += ".rz ";
		}
		//m_BakeCmd->commandToExecute( filterCmd );
		//m_BakeCmd->doIt();
		MGlobal::executeCommand(filterCmd, false);
	}
}

/*
PURPOSE
	Clean up whatever has set in SetupExportItems.
PARAMS
	none.
RETURNS
	none.
NOTES
*/
void rexShellMaya::CleanupExportItems(const atArray<ExportItemInfo>& /*items*/)
{
	// Delete m_BakeCmd.
	if( m_BakeCmd != NULL )
	{
		// undo it.
		m_BakeCmd->undoIt();

		delete m_BakeCmd;
		m_BakeCmd = NULL;
	}

	// Go to the first frame.
	float sf = 0.0f;
	rexMayaUtility::GetMayaSceneStartFrame( sf );
	rexMayaUtility::SetFrame( (int)sf );

	rexMayaUtility::DestructNamespace();
}

/*
PURPOSE
	Get the entity root node of an entity.
PARAMS
	item - exportItemInfo.
	dpRoots - output root node.
RETURNS
	none.
NOTES
*/
void rexShellMaya::GetAnimationSegmentRootNodes(const ExportItemInfo& item, atArray<MDagPath>& dpRoots)
{
	//if( item.m_ModuleID == "AnimationSegment" )
	if( item.m_ModuleID == "Entity" || item.m_ModuleID == "AnimationSegment" )
	{
		int propCount = item.m_PropertyIDs.GetCount();
		for( int a = 0; a < propCount; a++ )
		{
			if( item.m_PropertyIDs[a] != "Nodes" )
				continue;

			int propObjCount = item.m_PropertyConnectedObjects[a].GetCount();
			for( int b = 0; b < propObjCount; b++ )
			{
				rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( item.m_PropertyConnectedObjects[a][b] );
				if( dpObj )
				{
					int dpCount = dpRoots.GetCount();
					bool found = false;
					for( int d = 0; d < dpCount; d++ )
					{
						if( dpRoots[ d ] == dpObj->m_DagPath )
						{
							found = true;
							break;
						}
					}	
					if( !found )
						dpRoots.PushAndGrow( dpObj->m_DagPath,(u16) propObjCount );
				}
			}
		}
	}

	int childCount = item.m_Children.GetCount();
	for( int a = 0; a < childCount; a++ )
	{
		GetAnimationSegmentRootNodes( item.m_Children[ a ], dpRoots );
	}
}

/*
PURPOSE
	Get entity nodes recursively. Then set cmd string.
PARAMS
	dp - output root node.
	cmd - command string.
	count - output. the total number of nodes.
RETURNS
	bool.
NOTES
*/
void rexShellMaya::BuildCommandFromEntityNode(const MDagPath& dp, MString& cmd, int& count)
{
	if( dp.apiType() == MFn::kTransform || dp.apiType() == MFn::kJoint )
	{
		if( rexMayaUtility::IsFrameDataNeeded(dp) )
		{
			if( count > 0 )	cmd += ", \"";
			else			cmd += "\"";
			cmd += dp.partialPathName();
			cmd += "\"";

			count++;
		}
	}

	int childCount = dp.childCount();
	for( int i = 0; i < childCount; i++ )
	{
		MObject child = dp.child(i);
		MDagPath dpChild = MDagPath::getAPathTo(child);
		BuildCommandFromEntityNode(dpChild, cmd, count);
	}
}

rexResult rexShellMaya::PreExport()
{
	// go thru export data and, if PreExportMEL attribute exists, execute the script
	// if "PreExportMEL" attribute found, execute MEL script specified as value

	rexResult retval( rexResult::PERFECT );

	MDagPath exportDataRoot, exportRulesRoot;
	atString exportDataRootNodeName ("|");
	exportDataRootNodeName += m_ExportDataNamespace;
	exportDataRootNodeName += "RexExportData";
	atString exportDataRulesRootNodeName("|");
	exportDataRulesRootNodeName += m_ExportDataNamespace;
	exportDataRulesRootNodeName += "RexExportRules";

	if( !rexMayaUtility::FindNode( exportDataRootNodeName, exportDataRoot ) )
	{
		retval.Combine( rexResult::ERRORS );
		retval.AddMessage( "%s %d %s Node Not Found...Could Not Find Anything To Export!  Make sure that "
			" the '%s' node is at the root of the Maya scene.", __FILE__, __LINE__, (const char*)exportDataRootNodeName,(const char*)exportDataRootNodeName );
		return retval;
	}

	rexMayaUtility::FindNode( exportDataRulesRootNodeName, exportRulesRoot );

	MItDag it;
	it.reset( exportDataRoot );

	atArray<atString> scriptsToExecute;
	atArray<MDagPath> scriptReferringNode;

	atMap<atString, atString> dpScriptMap;

	bool doingRules = false;

	while( 1 )
	{
		MDagPath thisPath;
		it.getPath( thisPath );
		MObject obj = thisPath.node();

		if( rexMayaUtility::AttributeExists( obj, "PreExportMEL" ) )
		{
			rexValue v = rexMayaUtility::GetAttributeValue( obj, "PreExportMEL" );
			if( v.toString() )
			{
				// use full path minus root node for path comparisons. this will prevent local and template
				// scripts from both being executed
				MString pathNameMinusRoot;
				MStringArray splits;	
				thisPath.fullPathName().split('|', splits);
				int splitCount = splits.length();
				for( int s = 1; s < splitCount; s++ )
				{
					pathNameMinusRoot += splits[ s ];
					if( s < splitCount - 1 )
						pathNameMinusRoot += "|";
				}

				if( !dpScriptMap.Access( pathNameMinusRoot.asChar() ) )
				{
					dpScriptMap.Insert( atString(pathNameMinusRoot.asChar()), atString(v.toString()) );
					scriptsToExecute.PushAndGrow( atString( v.toString() ) );
					scriptReferringNode.PushAndGrow( thisPath );
				}
			}
		}
		it.next();
		if( it.isDone() )
		{
			if( doingRules || !exportRulesRoot.isValid() )
				break;
			it.reset( exportRulesRoot );
			doingRules = true;
		}
	}

	int scriptCount = scriptsToExecute.GetCount();
	for( int a = 0; a < scriptCount; a++ )
	{
		MStatus status = MGlobal::executeCommand( (const char*)scriptsToExecute[ a ], false );
		if( status.error() )
		{
			retval.Combine( rexResult::WARNINGS );
			retval.AddMessage( "Script '%s' (referred to by %s) had errors while executing!", (const char*)scriptsToExecute[ a ], scriptReferringNode[ a ].partialPathName().asChar() );
		}
	}


	// make sure that animation limits are not negative:
	int sf = 0;
	int ef = 0;
	rexMayaUtility::GetMayaSceneStartAndEndFrames(sf, ef);
	if( (ef - sf) <= 0 )
	{
		retval.Set(rexResult::ERRORS);
		retval.AddMessage("End frame (%d) is not greater than start frame (%d)",ef,sf);
		return retval;
	}

	if(ef<0.0f || sf<0.0f)
	{
		retval.Set(rexResult::ERRORS);
		if (ef<0.0f)
			retval.AddMessage("End frame (%d) is a negative value",ef);
		if (sf<0.0f)
			retval.AddMessage("Start frame (%d) is a negative value",sf);
		return retval;
	}

	return retval;
}

rexResult rexShellMaya::PostExport()
{
	// go thru export data and, if PostExportMEL attribute exists, execute the script
	// if "PostExportMEL" attribute found, execute MEL script specified as value

	rexResult retval( rexResult::PERFECT );

	MDagPath exportDataRoot;
	atString exportDataRootNodeName("|");
	exportDataRootNodeName += m_ExportDataNamespace;
	exportDataRootNodeName += "RexExportData";

	if( !rexMayaUtility::FindNode( exportDataRootNodeName, exportDataRoot ) )
	{
		retval.Combine( rexResult::ERRORS );
		retval.AddMessage( "%s %d %s Node Not Found...Could Not Find Anything To Export!  Make sure that "
			" the '%s' node is at the root of the Maya scene.", __FILE__, __LINE__, (const char*)exportDataRootNodeName,(const char*)exportDataRootNodeName );
		return retval;
	}

	MItDag it;
	it.reset( exportDataRoot );

	atArray<atString> scriptsToExecute;
	atArray<MDagPath> scriptReferringNode;
	do
	{
		MDagPath thisPath;
		it.getPath( thisPath );
		MObject obj = thisPath.node();

		if( rexMayaUtility::AttributeExists( obj, "PostExportMEL" ) )
		{
			rexValue v = rexMayaUtility::GetAttributeValue( obj, "PostExportMEL" );
			if( v.toString() )
			{
				scriptsToExecute.PushAndGrow( atString( v.toString() ) );
				scriptReferringNode.PushAndGrow( thisPath );
			}
		}
		it.next();
	} while( !it.isDone() );

	int scriptCount = scriptsToExecute.GetCount();
	for( int a = 0; a < scriptCount; a++ )
	{
		MStatus status = MGlobal::executeCommand( (const char*)scriptsToExecute[ a ], false );
		if( status.error() )
		{
			retval.Combine( rexResult::WARNINGS );
			retval.AddMessage( "Script '%s' (referred to by %s) had errors while executing!", (const char*)scriptsToExecute[ a ], scriptReferringNode[ a ].partialPathName().asChar() );
		}
	}
	return retval;
}

} // namespace rage

