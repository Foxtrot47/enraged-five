// 
// rexMaya/shell.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	rexMayaShell -	this is the Maya version of rexShell, which allows for registration of
//					modules and properties, and for dispatching objects to these modules 
//					for processing
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYA_SHELL_H__
#define __REX_MAYA_SHELL_H__

/////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "atl/array.h"
#include "atl/map.h"
#include "rexBase/shell.h"
#include "vector/matrix44.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MDagPath.h>
#include <Maya/MEulerRotation.h>
#include <Maya/MItDag.h>
#pragma warning(pop)

class MDGModifier;


/////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexShellMaya : public rexShell
{
public:
	friend class MayaIterator;
	rexShellMaya() : rexShell()  { m_DisplayDialogs = true; m_SkeletonUsesOnlyJoints = false; 
									m_ErrorWhenRuleTemplateNotFound = false; m_BakeCmd = NULL; 
									m_IgnoreBonesInLevelInstanceObjects = true; m_IgnoreNullJoints = true; }

	void SetErrorWhenRuleTemplateNotFound( bool b ) { m_ErrorWhenRuleTemplateNotFound = b; }

	/*  allows property to add bones that aren't actually animated transforms and/or joints (i.e. car wheel)
		PARAMS: ID - string to identify bone tag
				dp - DAG path of object to declare a bone
	*/
	void RegisterBoneTag( const char *ID, MDagPath& dp )	{ m_BoneTagDPs.PushAndGrow( dp ); m_BoneTagIDs.PushAndGrow( atString(ID) ); }
	
	/*  allows property to tag hierarchies with special flags
	PARAMS: ID - string to use as special flag
	dp - DAG path of root object to apply flag to
	*/
	void RegisterSpecialFlag( const char *flag, MDagPath& dp )	{ m_SpecialFlagDPs.PushAndGrow( dp ); m_SpecialFlagIDs.PushAndGrow( atString(flag) ); }

	/*  allows property to define groups 
		PARAMS: ID - string to identify group
				dp - DAG path of object to declare root of group
	*/
	void RegisterGlobalObject( const char * ID, MDagPath& dp ) { m_GroupRootDPs.PushAndGrow( dp ); m_GroupIDs.PushAndGrow( atString(ID) ); }

	/*  sets the maya namespace export data nodes are part of
		PARAMS: s - namespace prefix string
	*/
	void SetExportDataNamespace( const char * s )			{ m_ExportDataNamespace = s; }

	/*  sets display dialogs bit.  
		PARAMS: b - value to set
		NOTES: this should probably be in the base shell.
	*/
	void SetDisplayDialogs( bool b )							{ m_DisplayDialogs = b; }

	/*  sets bit which allows shell to only consider joint nodes to be bones
		PARAMS: b - value to set
		NOTES: this should probably be in the base shell.
	*/	
	void SetSkeletonUsesOnlyJoints( bool b )					{ m_SkeletonUsesOnlyJoints = b; }

	/*  retrieves whether shell considers non-joint nodes as bones
		RETURNS: true if only joints can be bones, false if animated transforms and bone tags can be as well
		NOTES: this should probably be in the base shell.
	*/	
	bool GetSkeletonUsesOnlyJoints() const						{ return m_SkeletonUsesOnlyJoints; }

	/*	sets flag which tells the shell to ignore joints that are named "_null"
		PARAMS: b - value to set
	*/
	void SetIgnoreNullJoints( bool b )							{ m_IgnoreNullJoints = b; }

	/* retrieves whether the shell is ignoring joints named "_null"
		RETURNS: true if null joints are being ignored, false otherwise.
	*/
	bool GetIgnoreNullJoints() const							{ return m_IgnoreNullJoints; }

protected:
	
	/////////////////////////////////////////////////////
	// these are the functions required by rexShell
	/////////////////////////////////////////////////////

	/* gathers export item information, based on input objects.  this allows for selective export.  
	   if input objects array is empty, it is assumed every item in the scene should be exported.  
    */
	virtual void		Reset();

	/* gathers export item information, based on input objects.  this allows for selective export.  
	   if input objects array is empty, it is assumed every item in the scene should be exported.  
		
		PARAMS: inputObjects		- scene objects to use as a basis for determining which items to gather
									  (if empty, all valid items are gathered)
				exportItems			- array to be populated by function.  after function called, contains all 
									  items to be exported.
		RETURNS: status
		SEE ALSO: ExportItemInfo, rexObject
	*/
	virtual rexResult	GetExportItemInfo( const atArray<rexObject*>& inputObjects, atArray<ExportItemInfo>& exportItems );

	/* creates iterator which is used to retrieve scene objects
		PARAMS: exportItem - item to gather objects for
		RETURNS: pointer initialized iterator, ready to be queried.
		SEE ALSO: Iterator, ExportItemInfo
	*/
	virtual Iterator*	GetIterator( ExportItemInfo& exportItem );

	/* returns unique string ID to be used as hash lookup when referring to passed object.
		PARAMS: obj - object to create string ID for
		RETURNS: unique string ID for object
		SEE ALSO: rexObject
	*/
	virtual atString	GetInputObjectHashString( rexObject& obj );

	/* determines whether object is member of (or descendant of member of) input objects list
		PARAMS: obj				- object to verify membership of
			    inputObjects	- list of member objects
				includeChildren - if set, function returns true if object is descendant of a member object.
								  otherwise, function returns true only if object is a member object.
		RETURNS: true if object is a member object (or is descendant of a member object when includeChildren flag set)
		SEE ALSO: rexObject
	*/
	virtual bool		IsObjectPartOfInput( const rexObject& obj, const atArray<rexObject*>& inputObjects, bool includeChildren ) const;

	/* copies input objects list from one module to another
		PARAMS: dest			- module to copy input objects to
			    source			- module to copy input objects from

		NOTES:  this is done here so that it is not necessary to implement a copy function for every object type
		
		SEE ALSO: rexModule
	*/
	virtual void		CopyModuleInputObjects( rexModule& dest, const rexModule& source );

	/* creates and displays a progress window with the specified title and number of progress bars
		PARAMS: title			- caption of progress window
			    numValues		- number of progress bars to display in window
		RETURNS: true if successful.  returns false if progress window already open.
		SEE ALSO: CloseProgressWindow, UpdateProgressWindowInfo, UpdateProgressWindowMax, UpdateProgressWindowValue
	*/
	virtual bool		OpenProgressWindow(const char *title, int numValues);

	/* closes progress window previously created via OpenProgressWindow() call
		RETURNS: true if successful.  return falses if no window currently open
		SEE ALSO: OpenProgressWindow, UpdateProgressWindowInfo, UpdateProgressWindowMax, UpdateProgressWindowValue
	*/
	virtual bool		CloseProgressWindow(void);

	/* sets progress bar's text display
		PARAMS: index			- progress bar to modify
			    info			- text to display
		RETURNS: true if successful.  returns false if no window currently open or if bar index out of range.
		SEE ALSO: OpenProgressWindow, CloseProgressWindow, UpdateProgressWindowMax, UpdateProgressWindowValue
	*/
	virtual bool		UpdateProgressWindowInfo(int index, const char *info);

	/* sets progress bar's maximum value
		PARAMS: index			- progress bar to modify
			    max				- value to set as bar's maximum
		RETURNS: true if successful.  returns false if no window currently open or if bar index out of range.
		SEE ALSO: OpenProgressWindow, CloseProgressWindow, UpdateProgressWindowInfo, UpdateProgressWindowValue
	*/
	virtual bool		UpdateProgressWindowMax(int index, int max);

	/* sets progress bar's current value
		PARAMS: index			- progress bar to modify
			    value			- value to set as bar's value
		RETURNS: true if successful.  returns false if no window currently open or if bar index out of range.
		SEE ALSO: OpenProgressWindow, CloseProgressWindow, UpdateProgressWindowInfo, UpdateProgressWindowMax
	*/
	virtual bool		UpdateProgressWindowValue(int index, int value);

    virtual void ProgressBarCountCB( int count );
    virtual void ProgressBarCurrIndexCB( int value );
    virtual void ProgressBarCurrInfoCB( const char* s );

    // allow derived shells to do specific setup and cleanup
	virtual rexResult	PreExport();
	virtual rexResult	PostExport();

	virtual void		SetupExportItems(const atArray<ExportItemInfo>& items);
	virtual void		CleanupExportItems(const atArray<ExportItemInfo>& items);

	void				GetAnimationSegmentRootNodes(const ExportItemInfo& exportItem, atArray<MDagPath>& animSegRootDPs);

	void				BuildCommandFromEntityNode(const MDagPath& dp, MString& cmd, int& count);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// these currently return functions to be passed along to rexConverter and rexSerializer.  i want to get
	/// rid of these and just have the shell pass a callback function to these items, which would then directly
	/// call the functions above.
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	virtual	rexProgressBarCallback		GetProgressBarCurrValueCB();
	virtual	rexProgressBarCallback		GetProgressBarItemCountCB();
	virtual	rexProgressBarTextCallback	GetProgressBarInfoCB();

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// here's the derived version of the iterator.  it basically traverses the maya (sub-)scene in a depth-first
	// fashion and creates objects for rexShell to dispatch and process.
	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	class MayaIterator : public rexShell::Iterator
	{
	public:
		MayaIterator( const rexShellMaya& pShell, const atArray<MDagPath>& dpArray, const atArray<MDagPath>& boneTagDPs, const atArray<atString>& boneTagIDs, const atArray<MDagPath>& groupRootDPs, const atArray<atString>& groupIDs, const atArray<MDagPath>& specialFlagDPs, const atArray<atString>& specialFlagIDs );
		virtual ~MayaIterator() { }

		/* Gets next object to process

			NOTES:  Properties are used to modify shell and module behaviors, or to modify object data.
					An ID string is used to allow reference by scene Export Data (via attribute name)
					to specify which property function to call.  Property functions also get passed
					the value and node connections the attribute may have.

			RETURNS: rexObject containing all information necessary to process data.

			PARAMS:  depthDelta - is set to number of levels difference between this node and previous node 
								  in scene hierarchy (i.e. going from a node to its grandparent would set
								  this variable to -2).

			SEE ALSO: rexObject, Iterator
		*/
		virtual rexObject* GetNextObject( int& depthDelta );

		/* Gets number of objects to be processed
			RETURNS: number of objects to be processed
		*/
		virtual int			GetObjectCount() const;

	protected:				
		struct GroupInfo
		{ 
			atString ID;
			int Index;
		};

		MItDag									DagIterator;		// used to traverse scene data
		atArray<MDagPath>						DPArray;			// root nodes to start traversals from
		atMap< atString, atArray<atString> >	BoneTagMap;			// hash table for bone tags
		atMap< atString, GroupInfo >			GroupRootMap;		// hash table for groups
		atMap< atString, atArray<atString> >	SpecialFlagMap;		// hash table for special flags
		int										CurrentDPIndex;		// stores which root node is being used now
		bool									PulledFirstObject;	// bit set after first object retrieved
		int										BoneCount;			// number of bones in object to this point
		
		int										LODGroupCount;		// number of LOD groups in object to this point
		const rexShellMaya*						ParentShell;		// shell who owns iterator
		int										IKSystemResult;		// result of ik system enabled

		// these are used to store state between levels of hierarchy.
		atArray<int, 0, unsigned>					BoneIndexStack;
		atArray<Matrix44, 0, unsigned>				BoneMatrixStack;
		atArray<MDagPath, 0, unsigned>				BoneDagPathStack;
		MDagPath									BoneRootDagPath;
		atArray<atString, 0, unsigned>				GroupIDStack;
		atArray<int, 0, unsigned>					GroupIndexStack;
		Matrix44									RootBoneMatrix;
		atArray<int, 0, unsigned>					LODGroupIndexStack;
		atArray<int, 0, unsigned>					LODLevelStack;
		atArray<float, 0, unsigned>					LODThresholdStack;
		atArray< atArray<atString>, 0, unsigned>	SpecialFlagStack;

		atArray<MEulerRotation, 0, unsigned>		JointRotations;
		atArray<MDagPath, 0, unsigned>				JointDagPaths;

		bool									m_IgnoreBonesInLevelInstanceObjects;
	};	

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// these functions are for internal use
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	/* Gets dag paths of input object and its descendents
		PARAMS:  exportItem - export item to gather dag paths from
				 dpArray    - storage for dag paths retrieved							  

		SEE ALSO: ExportItemInfo
	*/
	void				GetInputDagPathsRecursively( ExportItemInfo& exportItem, atArray<MDagPath>& dpArray );

	/* Sets module active flag for item and its descendents
		PARAMS:  exportItem - export item to set module active flag for
				 active     - flag value					  

		SEE ALSO: ExportItemInfo
	*/
	void				SetModuleActiveFlagRecursively( ExportItemInfo& exportItem, bool active );

	/* Gathers export info from maya scene RexExportData nodes
		PARAMS:  dp - node to gather info from
				 currExportItemArray - current export item array being appended
				 activeDefault - determines whether module is active by default
				 moduleID - used when parent is group node in order to set module ID from parent 

		RETURNS: status

		SEE ALSO: ExportItemInfo
	*/
	rexResult			GetInfoRecursively( MDagPath& dp, atArray<ExportItemInfo>& currExportItemArray, bool activeDefault, const atString* moduleID = NULL );

	/* returns export item that dag path is part of  (used for selective export)
		PARAMS:  dp - dag path to find export item for
				 exportDataRootNode - dag path with export data root node
				 allExportItems - export items to search through

		RETURNS: pointer to export item that contains dp.  NULL if dp cannot be linked to export item.

		NOTES:  this is not fully operational at this time.

		SEE ALSO: ExportItemInfo
	*/
	ExportItemInfo*		GetInfoSelectedObject( const MDagPath& dp, const MDagPath& exportDataRootNode, atArray<ExportItemInfo>& allExportItems );

	/* returns export item based on moduleID and name information (used by GetInfoSelectedObject())
		PARAMS:  moduleIDStack - stack of existing moduleIDs
				 nameStack - stack of module names
				 exportItems - export items to search through

		RETURNS: pointer to export item that matches name and module ID stack.  NULL if dp cannot be linked to export item.

		NOTES:  this is not fully operational at this time.

		SEE ALSO: ExportItemInfo, GetInfoSelectedObject
	*/
	ExportItemInfo*		FindExportItemInfo( const atArray<atString>& moduleIDStack, const atArray<atString>& nameStack, atArray<ExportItemInfo>& exportItems );

	bool				ContainsNamespaceString( const char *s );

	/* removes namespace prefix from string
		PARAMS:  s - original string
		RETURNS: string stripped of namespace prefix
	*/
	atString			RemoveNamespaceCharacters( const char *s );

	/* removes namespace prefix and all non-alphabetical characters from string 
		PARAMS:  s - original string
		RETURNS: string stripped of namespace prefix and all non-alphabetical characters
	*/
	atString			RemoveNamespaceAndNonAlphabeticalCharacters( const char *s );
	

	// this code allows for an item to be exported using referenced, pre-tagged subitems of the same type.
	// for instance, the Item's export rule and the world's export rule are the exact same, except the
	// world's Item rule file contains a set boolean named "UsesReferencedSubItems".  each Item can
	// then be tagged and exported individually, while the world file references the desired Items and 
	// builds a "super-Item".  
	//
	// this code looks for RexExportData nodes in referenced scenes, and gathers their input nodes for use
	// with the "super-scene" object.
	//
	// this code may not be desirable now that world files are split up, but it seems like it might be handy
	void AssimilateReferencedExportDataPropertyConnections( rexShell::ExportItemInfo& exportItem, MObject obj );
	
	// this does the recursive bit of the referenced scene property assimilation
	bool AssimilatePropertyConnections( rexShell::ExportItemInfo& rootExportItem, const rexShell::ExportItemInfo& referencedExportItem );

	// these take base rules, local overrides, and combines them into a third array (for centralized rules)
	rexResult CombineExportInfo( atArray<ExportItemInfo>& exportRules, atArray<ExportItemInfo>& exportLocalData, atArray<ExportItemInfo>& exportItems );
	
	// this allows us to do a sanity check on the item structure on a per-project basis
	virtual rexResult VerifyExportItems( rexShell::ExportItemInfo& /*rootExportItem*/ ) { return rexResult::PERFECT; }

	bool shouldAddForExportProcessing(rexObject* rexObject);

	// internally used data
	atArray<MDagPath>	m_BoneTagDPs, m_GroupRootDPs, m_SpecialFlagDPs;
	atArray<atString>	m_BoneTagIDs, m_GroupIDs, m_SpecialFlagIDs;	
	atString			m_ExportDataNamespace;
	bool				m_DisplayDialogs;
	bool				m_SkeletonUsesOnlyJoints;
	bool				m_ErrorWhenRuleTemplateNotFound;
	bool				m_IgnoreBonesInLevelInstanceObjects;
	bool				m_IgnoreNullJoints;

	// Used for baking simulation only.
	MDGModifier			*m_BakeCmd;
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////////////////////////


#endif
