#ifndef __REX_MAYA_SET_COLLECTION_H__
#define __REX_MAYA_SET_COLLECTION_H__


#include "atl/singleton.h"
#include "atl/array.h"
#include "atl/string.h"
#include "atl/map.h"
#include "rexMaya/mayahelp.h"
#include "rexBase/result.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include "maya/MFnSet.h"
#pragma warning(pop)


class MObject;

namespace rage
{
	class rexMayaSetCollection
	{
	public:
		struct SetMemberInfo
		{
			int				m_SetIndex;
			MObject			m_ComponentData;									//Will be MObject::kNullObj if the the entire object is part of the set. Otherwise it is a component object that can be used with an iterator (e.g. MItMeshPolygon) to get to all parts of the object which are members of the set.
			atArray<int>	m_ComponentFaceIndices;								//if m_ComponentData is not MObject::kNullObj, this array will contain the indices of the faces included in the set.
			SetMemberInfo() : m_SetIndex(-1), m_ComponentData(MObject::kNullObj) {}
			SetMemberInfo(int setIndex, const MObject &componentData) : m_SetIndex(setIndex), m_ComponentData(componentData) {}
		};

		typedef atArray<SetMemberInfo>		SetMemberGroup;
		typedef atArray<int>				SetSelectionGroup;


		int GetTotalNumberOfSets() const		{ return m_Sets.GetCount(); }
		MFnSet &GetSet(int index)				{ return m_Sets[index].m_Set; }
		const MFnSet &GetSet(int index) const	{ return m_Sets[index].m_Set; }

		int					GetNumberOfConnectedREXNodes(int setIdx) { return m_Sets[setIdx].m_RexExportDataNodes.GetCount(); }
		atString			GetConnectedREXNodeName(int setIdx, int rexNodeIdx);

		void PrintSetContent(int setIndex) const;

		bool IsMemberOfSet(const MFnSet &set, const MDagPath &dagPathToCheck, MObject &componentOut, rexResult &retval) const;
		rexResult FindAllSetsContainingObject(const MDagPath &dagpath, const SetSelectionGroup &atSetsToConsider, SetMemberGroup &setsObjectBelongsTo) const;
		void FilterSetsByRexExportDataNodeNamePrefix(const char *prefix, rexMayaSetCollection::SetSelectionGroup &setIndicesOut) const;
		
		int ConcatenateRexExportDataNodeNames(const SetMemberGroup &setsOfNodesToAdd, char *outString, const int outStringMaxLength, unsigned int numCharsOfNameToSkip=0, const char *delimiter="", const char *namePrefixFilter="") const;

		const char *GetRexExportDataNodeNamePrefix() { return "_ExData_:"; }					//The name of all RexExportDataNodes starts with this prefix.
		const SetSelectionGroup *GetSetsConnectedToRexExportDataNodes() {return &m_RexExportDataNodeSets; }

	protected:
		rexMayaSetCollection();
		rexMayaSetCollection(const rexMayaSetCollection &){}									//This class is intended as a singleton so copying is not an option.
		virtual ~rexMayaSetCollection(){};

	private:
		struct RexExportDataNode																//Holds data about a "rageRexExportDataNode".
		{
			atString								m_Name;										//The name of the MFnDependencyNode that is the "rageRexExportDataNode".
		};

		typedef atArray<RexExportDataNode>	RexExportDataNodes;

		struct SetInfo
		{
			MFnSet									m_Set;
			RexExportDataNodes						m_RexExportDataNodes;
		};

		typedef atArray<SetInfo>					FnSets;
		FnSets										m_Sets;

		rexMayaSetCollection::SetSelectionGroup		m_RexExportDataNodeSets;					//Sets containing RexExportDataNodes as identified by their name prefix.
		
		mutable atMap< unsigned, atMap< unsigned ,SetMemberGroup > > m_DataCacheMapping;
		rexResult CacheSetMembers(int setIndex, const MFnSet &set, MFn::Type apiTypeToCache) const;
		rexResult AttemptCacheRecurse(int setIndex, const MFnSet &set, MFn::Type apiTypeToCache,const MDagPath &dagPath, MObject &component) const;
		rexResult CacheSetMember(int setIndex, const MFnSet &set, MFn::Type apiTypeToCache, const MDagPath &dp, MObject &component) const;
		void GatherSetData();
		void GatherRexNodeData();
	};

	typedef atSingleton<rexMayaSetCollection>	rexMayaSetCollectionSingleton;
}

#endif //__REX_MAYA_SET_COLLECTION_H__
