// 
// rexMaya/mayahelp.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __MAYAHELP_H__
#define __MAYAHELP_H__

#ifndef _BOOL
#define _BOOL
#endif

#define TRUE_AND_FALSE_DEFINED

#endif // #ifndef __MAYAHELP_H__
