#include "mayaSetCollection.h"

#include "rexMaya/mayahelp.h"
#include "string/stringhash.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include "maya/MDagPath.h"
#include "maya/MItDag.h"
#include "maya/MItDependencyGraph.h"
#include "maya/MString.h"
#include "maya/MFnDependencyNode.h"
#include "maya/MStringArray.h"
#include "maya/MGlobal.h"
#include "maya/MSelectionList.h"
#include "maya/MItMeshPolygon.h"
#pragma warning(pop)

namespace rage {


rexMayaSetCollection::rexMayaSetCollection()
{
	GatherSetData();
	GatherRexNodeData();

	m_RexExportDataNodeSets.Reserve(GetTotalNumberOfSets());
	FilterSetsByRexExportDataNodeNamePrefix(GetRexExportDataNodeNamePrefix(), m_RexExportDataNodeSets);
}


/*
	Builds an array of all sets in Maya.

	Based on Maya example file devkit/plug-ins/objExporter.cpp.
*/
void rexMayaSetCollection::GatherSetData()
{	
	//Get all of the sets in Maya using MEL and put them into
	//a selection list:
	MStringArray			result;
	MGlobal::executeCommand("ls -sets", result, false);
	MSelectionList			setList;
	int						length= result.length();

	for(int i=0; i<length; i++)
	{	
		AssertVerify(setList.add(result[i]) == MStatus::kSuccess);
	}

	//Extract each set as an MObject and add it to the array of sets:
	MObject					object;

	length= setList.length();
	m_Sets.Reserve(length);

	for(int i=0; i<length; i++)
	{
		setList.getDependNode(i, object);
		
		//Only consider sets that are not referenced into the current scene.
		MFnDependencyNode fnDep(object);
		if(!fnDep.isFromReferencedFile())
		{
			MFnSet				&fnSet= m_Sets.Append().m_Set;
			AssertVerify(fnSet.setObject(object) == MStatus::kSuccess);
		}

//		Displayf("%s", fnSet.name().asChar());
	}
}

/*
	Prints all top level nodes and their polygon indices (if applicable) of a
	set. Does not go recursively through the set.

INPUTS
	setIndex	- index into the array of sets as build by GatherSetData().
*/
void rexMayaSetCollection::PrintSetContent(int setIndex) const
{
	MSelectionList			setList;
	AssertVerify(m_Sets[setIndex].m_Set.getMembers(setList, true) == MStatus::kSuccess);
	int						length= setList.length();
	MObject					dependNode, component;
	MDagPath				dagPath;

	Displayf("\nSet %d: %s:\n", setIndex, m_Sets[setIndex].m_Set.name().asChar());

	for(int i=0; i<length; i++)
	{
		AssertVerify(setList.getDependNode(i, dependNode) == MStatus::kSuccess);
		AssertVerify(setList.getDagPath(i, dagPath, component) == MStatus::kSuccess);

		Displayf("dependNode API: %s, DAGpath: %s, component API: %s", dependNode.apiTypeStr(), dagPath.partialPathName().asChar(), component==MObject::kNullObj ? "NULL" : component.apiTypeStr());

		if(component != MObject::kNullObj)
		{
			MStatus					status;
			MItMeshPolygon			it(dagPath, component, &status);
			Assert(status == MStatus::kSuccess);										//Otherwise it's probably some other type of iterator we are not handling here.

			for( ; !it.isDone(); it.next())
			{
				Displayf("poly index: %d", it.index());
			}
		}
	}

	Displayf("");
}


/*
	Checks if the passed in object (dagPathToCheck) is a member of the
	supplied set. Unlike maya's isMember() function this function recursively
	goes through all DAGs in the set to see if the object is somehow connected
	into the set (maya only checks the top level node in the set).

INPUTS
	set				- The set whose members to check against.
	dagPathToCheck	- Identifies the object to search for in the set.
	component		- * Will be set to an iterator type in case only part of
					the object is a member of the set. The iterator identifies
					all parts that are members of the set.
					* Will be set to MObject::kNullObj if the entire object is
					part of the set or the object isn't part of the set at all.

RETURNS
	true	- The object (or some part of it) is part of the set.
	false	- No part of the object is a member of the set.
*/
bool rexMayaSetCollection::IsMemberOfSet(const MFnSet &set, const MDagPath &dagPathToCheck, MObject &componentOut, rexResult &retval) const
{
	retval.Combine(rexResult::PERFECT);

	MSelectionList			setList;
	AssertVerify(set.getMembers(setList, true) == MStatus::kSuccess);							//TODO: this can be done as a preprocessing step when gathering the sets.

	int						numSetMembers= setList.length();
	MDagPath				setMemberDagPath;
	MStatus					status;
	int						numMemberInstancesFound= 0;
	bool					fullInstanceFound= false;											//Will be set to true if a full instance (as opposed to just a part of it) of the object is found to be a member of the set.
	MObject					component= MObject::kNullObj;
	
	MFn::Type				apiTypeToCheck= dagPathToCheck.apiType(&status);
	Assert(status == MStatus::kSuccess);

	componentOut= MObject::kNullObj;

	for(int i=0; i<numSetMembers; i++)
	{
		AssertVerify(setList.getDagPath(i, setMemberDagPath, component) == MStatus::kSuccess);

		if(component != MObject::kNullObj)														//If maya returns a component it means only a part of the object (e.g. some faces) is a member of the set. If that's the case no sub tree (DAG) is possibly. This an assumption on my side and not a documented rule. However we could not find a way in maya's UI to create a partial sub DAG nor can I see in the API documentation how such a case could be handled. BTW. if an artist puts multiple faces of one mesh into a set maya exposes it in the API as one mesh with all the selected faces - not as multiple 'copies' of the same mesh with a few selected faces each (even though it may look otherwise in the UI).
		{
			if(setMemberDagPath == dagPathToCheck)
			{
				numMemberInstancesFound++;
				componentOut= component;
			}
		}
		else																					//If maya doesn't return a component the set member can be the root node of a whole DAG so we we have to recursively go through the DAG and test against all its members.
		{
			MItDag				dagIt(MItDag::kBreadthFirst, apiTypeToCheck, &status);			//Iterating over only the node types our source node is of is a minor optimization of an overall expensive process.
			Assert(status == MStatus::kSuccess);
			AssertVerify(dagIt.reset(setMemberDagPath, MItDag::kBreadthFirst, apiTypeToCheck) == MStatus::kSuccess);

			for( ; !dagIt.isDone(); dagIt.next())
			{
				MDagPath		currentDagPath;

				AssertVerify(dagIt.getPath(currentDagPath) == MStatus::kSuccess);
				if(currentDagPath == dagPathToCheck)
				{
					numMemberInstancesFound++;
					fullInstanceFound= true;
				}
			}
		}
	}

	if(numMemberInstancesFound>1 && componentOut!=MObject::kNullObj)
	{
		const char				*formatString= "Two or more (%d) references to the object >%s< were found in the set >%s< - at least one is not the full object (only components of it found). The result may not be what you expect.";
		Errorf(formatString, numMemberInstancesFound, dagPathToCheck.partialPathName().asChar(), set.name().asChar());
		retval= rexResult::ERRORS;
		retval.AddMessage(formatString, numMemberInstancesFound, dagPathToCheck.partialPathName().asChar(), set.name().asChar());
	}

	if(fullInstanceFound)
		componentOut= MObject::kNullObj;														//Obviously the full instance adds all parts of the object and 'overwrites' any individual parts that have potentially been found before.

	return numMemberInstancesFound > 0;
}


rexResult rexMayaSetCollection::FindAllSetsContainingObject(const MDagPath &dagPath, const SetSelectionGroup &/*setsToConsider*/, SetMemberGroup &setsObjectBelongsTo) const
{
	rexResult			retval(rexResult::PERFECT);
	MStatus				status;
	MObject				component;

	MFn::Type apiTypeToCheck= dagPath.apiType(&status);
	Assert(status == MStatus::kSuccess);

	if (m_Sets.GetCount())
	{
		const atMap< unsigned ,SetMemberGroup >* cachedSetMapping = m_DataCacheMapping.Access(apiTypeToCheck);
		if (!cachedSetMapping)
		{
			//cache the data
			for(int i=0; i< m_Sets.GetCount(); i++)
			{
				retval &= CacheSetMembers(i,m_Sets[i].m_Set,apiTypeToCheck);
			}
			cachedSetMapping = m_DataCacheMapping.Access(apiTypeToCheck);
		}

		Assert(cachedSetMapping);

		MString fullpath = dagPath.fullPathName();
		unsigned hash = atStringHash(fullpath.asChar());
		const SetMemberGroup* cachedSetMemberGroup = cachedSetMapping->Access(hash);
		if (cachedSetMemberGroup)
		{
			//found in cache
			setsObjectBelongsTo = (*cachedSetMemberGroup);
		}
	}

	return retval;
}

rexResult rexMayaSetCollection::CacheSetMembers(int setIndex, const MFnSet &set, MFn::Type apiTypeToCache) const
{
	rexResult			retval(rexResult::PERFECT);

	MSelectionList			setList;
	AssertVerify(set.getMembers(setList, true) == MStatus::kSuccess);							//TODO: this can be done as a preprocessing step when gathering the sets.

	int						numSetMembers= setList.length();
	MDagPath				setMemberDagPath;
	MStatus					status;
	MObject					component= MObject::kNullObj;
	MObject					nullobj= MObject::kNullObj;

	//Displayf("\nCache Set: %s\n", set.name().asChar());

	for(int i=0; i<numSetMembers; i++)
	{
		status = setList.getDagPath(i, setMemberDagPath, component);
		if(status != MStatus::kSuccess)
		{
			MObject depNode;
			status = setList.getDependNode(i, depNode);
			if (status == MStatus::kSuccess)
			{
				MFnDependencyNode fnDepNode = MFnDependencyNode(depNode);
				Warningf("Object [%s] has no DagPath. Is object [%s] needed?", fnDepNode.name().asChar(), fnDepNode.name().asChar());
			}
			continue;
		}

		retval &= AttemptCacheRecurse(setIndex,set,apiTypeToCache,setMemberDagPath, component);
	}

	return retval;
}

rexResult rexMayaSetCollection::AttemptCacheRecurse(int setIndex, const MFnSet &set, MFn::Type apiTypeToCache,const MDagPath &dagPath, MObject &component) const
{
	rexResult			retval(rexResult::PERFECT);
	MStatus status;

	MFn::Type apiTypeToCheck = dagPath.apiType(&status);
	Assert(status == MStatus::kSuccess);
	//Displayf("Attempt Cache: %s", dagPath.fullPathName().asChar());

	if (apiTypeToCheck == apiTypeToCache)
	{
		retval &= CacheSetMember(setIndex,set,apiTypeToCache,dagPath,component);
	}
	else
	{
		//If maya doesn't return a component the set member can be the root node of a whole DAG 
		//	so we have to recursively go through the DAG and test against all its members.
		MItDag dagIt(MItDag::kBreadthFirst, apiTypeToCache, &status);
		Assert(status == MStatus::kSuccess);
		AssertVerify(dagIt.reset(dagPath, MItDag::kBreadthFirst, apiTypeToCache) == MStatus::kSuccess);

		for( ; !dagIt.isDone(); dagIt.next())
		{
			MDagPath		currentDagPath;
			AssertVerify(dagIt.getPath(currentDagPath) == MStatus::kSuccess);
			if (currentDagPath.fullPathName() == dagPath.fullPathName())
			{
				//Displayf("Prevent Infinite loop: %s -> %s", dagPath.fullPathName().asChar(), currentDagPath.fullPathName().asChar());
			}
			else
			{
				//Displayf("Attempt Cache Child: %s -> %s", dagPath.fullPathName().asChar(), currentDagPath.fullPathName().asChar());
				retval &= AttemptCacheRecurse(setIndex,set,apiTypeToCache,currentDagPath,component);
			}
		}
	}

	return retval;
}

rexResult rexMayaSetCollection::CacheSetMember(int setIndex, const MFnSet &/*set*/, MFn::Type apiTypeToCache, const MDagPath &dp, MObject &component) const
{
	rexResult retval(rexResult::PERFECT);
	MStatus status;

	atMap< unsigned ,SetMemberGroup >* cachedSetMapping = m_DataCacheMapping.Access(apiTypeToCache);
	if (!cachedSetMapping)
	{
		m_DataCacheMapping.Insert(apiTypeToCache,atMap< unsigned ,SetMemberGroup >());
		cachedSetMapping = m_DataCacheMapping.Access(apiTypeToCache);
	}

	MString fullpath = dp.fullPathName();
	unsigned hash = atStringHash(fullpath.asChar());
	SetMemberGroup* cachedSetMemberGroup = cachedSetMapping->Access(hash);

	//Displayf("Cache: %s with hash %d", fullpath.asChar(), hash);

	bool add = true;
	if (cachedSetMemberGroup)
	{
		//Multiple references of the object
		for(int group = 0; group < cachedSetMemberGroup->GetCount(); ++group)
		{
			if (setIndex == cachedSetMemberGroup->GetElements()[group].m_SetIndex)
			{
				break;
			}
		}
	}
	else
	{
		cachedSetMapping->Insert(hash,SetMemberGroup());
		cachedSetMemberGroup = cachedSetMapping->Access(hash);
	}

	Assert(cachedSetMemberGroup);

	if (add)
	{
		SetMemberInfo &smi = cachedSetMemberGroup->Grow();
		smi.m_SetIndex = setIndex;
		smi.m_ComponentData = component;

		if(component != MObject::kNullObj)
		{
			MItMeshPolygon it(dp, component, &status);
			if(status == MS::kSuccess)
			{
				for( ; !it.isDone(); it.next())
					smi.m_ComponentFaceIndices.PushAndGrow(it.index());
			}
		}
	}

	return retval;
}

/*
	Finds all sets connected to RexExportDataNodes which have names starting
	with prefix. 
	
NOTES
	The specified prefix should include the result of
	GetRexExportDataNodeNamePrefix() as all RexExportDataNodes start with this
	prefix.

INPUTS
	prefix			- The string the names have to start with. It's used as
					a filter. If this is an empty string (i.e. ""), all sets that are connected to
					at least one REX node will be returned.
	setIndicesOut	- Will be filled with the indices of all sets that have
					matching RexExportDataNodes.
*/
void rexMayaSetCollection::FilterSetsByRexExportDataNodeNamePrefix(const char *prefix, rexMayaSetCollection::SetSelectionGroup &setIndicesOut) const
{
	const int				prefixLength= strlen(prefix);

	for(int setIdx=0; setIdx<m_Sets.GetCount(); setIdx++)
	{
		const SetInfo				&setInfo= m_Sets[setIdx];

		if(prefixLength)
		{
			bool						foundMatch= false;
			for(int rexNodeIdx=0; foundMatch==false && rexNodeIdx<setInfo.m_RexExportDataNodes.GetCount(); rexNodeIdx++)
			{
				const char					*nodeName= setInfo.m_RexExportDataNodes[rexNodeIdx].m_Name;

				if(strncmp(nodeName, prefix, prefixLength) == 0)
				{
					setIndicesOut.Push(setIdx);
					foundMatch= true;																//Doesn't matter how many hits a set has, we want the set only once in the target array.
				}
			}
		}
		else
		{
			if(setInfo.m_RexExportDataNodes.GetCount())
				setIndicesOut.Push(setIdx);
		}
	}
}


/*
	Builds a string of all RexExportDataNode names that meet the specified criteria.

INPUTS
	setsOfNodesToAdd	- The indices of the sets whose nodes should be considered.
	outString			- Target buffer for result. Must be big enough to hold the
						resulting string.
	outStringMaxLength	- Size of outString in bytes (including space for terminating 0). 
	numCharsOfNameToSkip- Number of characters at the beginning of the RexExportDataNode
						names that should be skiped when the names get concatenated (in
						order to remove a prefix).
	delimiter			- string that will be inserted between every 2 node names.
	namePrefixFilter	- Only node names beginning with this prefix will be added to resulting
						string.
*/
int rexMayaSetCollection::ConcatenateRexExportDataNodeNames(const SetMemberGroup &setsOfNodesToAdd, char *outString, const int outStringMaxLength, unsigned int numCharsOfNameToSkip, const char *delimiter, const char *namePrefixFilter) const
{
	const int					namePrefixFilterLength= strlen(namePrefixFilter);
	int							destPos= 0;
	unsigned int				numNamesConcatenated= 0;

	Assert(outString!=0 && outStringMaxLength>=1);
	outString[0]= '\0';

	for(int filterIdx=0; filterIdx<setsOfNodesToAdd.GetCount(); filterIdx++)
	{
		const int					setIdx= setsOfNodesToAdd[filterIdx].m_SetIndex;
		const SetInfo				&setInfo= m_Sets[setIdx];

		for(int rexNodeIdx=0; rexNodeIdx<setInfo.m_RexExportDataNodes.GetCount(); rexNodeIdx++)
		{
			const char					*nodeName= setInfo.m_RexExportDataNodes[rexNodeIdx].m_Name;

			if(namePrefixFilterLength==0 || strncmp(nodeName, namePrefixFilter, namePrefixFilterLength)==0)
			{
				Assert(numCharsOfNameToSkip<=strlen(nodeName));									//Maybe this should be an 'if' instead so that it just skips over names that are too short?
				const char					*nodeNamePiece= nodeName+numCharsOfNameToSkip;
				const int					nodeNamePieceLength= strlen(nodeNamePiece);
				const char					*delimiterString= numNamesConcatenated==0 ? "" : delimiter;
				const int					delimiterLength= strlen(delimiterString);
				Assert(destPos+nodeNamePieceLength+delimiterLength  < outStringMaxLength);
				formatf(outString+destPos, outStringMaxLength-destPos, "%s%s", delimiterString, nodeNamePiece);
				numNamesConcatenated++;
				destPos += nodeNamePieceLength + delimiterLength;
			}
		}
	}

	return numNamesConcatenated;
}


/*
	Has to be called after GatherSetData().
*/
void rexMayaSetCollection::GatherRexNodeData()
{
	const int					numSets= GetTotalNumberOfSets();
	MStatus						status;

	for(int setIdx= 0; setIdx<numSets; setIdx++)
	{
		const MFnSet				&currentSet= GetSet(setIdx);
		MPlugArray					plugArray;

		if(currentSet.getConnections(plugArray) == MStatus::kSuccess)							//Otherwise there aren't any connections.
		{
			const int					numPlugs= plugArray.length();

			for(int plugIdx=0; plugIdx<numPlugs; plugIdx++)
			{
				MPlug						localPlug= plugArray[plugIdx];
				MPlugArray					oppositePlugArray;
				localPlug.connectedTo(oppositePlugArray, true, false, &status);
				Assert(status == MStatus::kSuccess);

				const int					numOppositePlugs= oppositePlugArray.length();

				for(int oppPlugIdx=0; oppPlugIdx<numOppositePlugs; oppPlugIdx++)
				{
					MPlug						oppositePlug= oppositePlugArray[oppPlugIdx];
					MObject						object= oppositePlug.node(&status);
					Assert(status == MStatus::kSuccess);
					Assert(object.hasFn(MFn::kDependencyNode));
					MFnDependencyNode			depNode(object, &status);
					Assert(status == MStatus::kSuccess);
					MString						typeName= depNode.typeName(&status);
					Assert(status == MStatus::kSuccess);

					if(strcmp(typeName.asChar(), "rageRexExportDataNode") == 0)
					{
						//						Displayf("Found rageExportDataNode: >%s<, set name: %s, opposite plug name: >%s<.", depNode.name(&status).asChar(), currentSet.name().asChar(), typeName.asChar());

						m_Sets[setIdx].m_RexExportDataNodes.Grow().m_Name= depNode.name(&status).asChar();
#if __ASSERT
						{
							const char				*depNodeName= depNode.name(&status).asChar();
							Assert(status == MStatus::kSuccess);
							const char				*keyWord= GetRexExportDataNodeNamePrefix();
							const int				keywordLength= strlen(keyWord);
							Assert(strncmp(depNodeName, keyWord, keywordLength) == 0);											//Otherwise the node we found isn't what we expect it to be.
						}
#endif
					}
				}
			}
		}
	}
}

atString rexMayaSetCollection::GetConnectedREXNodeName(int setIdx, int rexNodeIdx) 
{ 
	const char* rexNodePrefix = GetRexExportDataNodeNamePrefix();
	const char* fullRexNodeName = (const char*)m_Sets[setIdx].m_RexExportDataNodes[rexNodeIdx].m_Name;
	const char* partialRexNodeName = fullRexNodeName + strlen(rexNodePrefix);
	return atString(partialRexNodeName);
}

} // namespace rage



