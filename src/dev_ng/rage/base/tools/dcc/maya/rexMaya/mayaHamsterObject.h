#ifndef __REX_MAYA_HAMSTER_OBJECT_H__
#define __REX_MAYA_HAMSTER_OBJECT_H__

#include "rexMaya/mayahelp.h"
#include "rexBase/shell.h"
#include "rexBase/hamsterObject.h"

namespace rage
{

class rexMayaHamsterObject : public rexHamsterObject
{
public:

    rexMayaHamsterObject();
    virtual ~rexMayaHamsterObject();

    virtual void SetModuleID(atString& moduleID);
    virtual void SetEntityName(atString name);
    virtual void SetOutputPath(atString& path);
    virtual void SetExportAsSingleEntity(bool asSingle);
    virtual void AddMDagPath(atString& dagPath, const char* exportName);

    virtual const atString GetMDagPathExportName(const char *dagPath) const;
    virtual const atString GetMDagPathExportName(const rexObject* object) const;
    virtual const atString GetOutputPath() const;
    virtual const atString GetEntityName() const;

    //[CLEMENSP]
    virtual 
    const char* getMainModuleID() const;

    virtual void SetupInputObjects(atArray<rexObject*>& objects) const;
    virtual void SetupExportItemInfos(atArray<rexShell::ExportItemInfo>& infos, const atArray<rexObject*>& objects) const;
   
protected:

    virtual void SetupExportItemInfoForSingleEntity(atArray<rexShell::ExportItemInfo>& infos, const atArray<rexObject*>& objects) const;
    virtual void SetupExportItemInfoForEntities(atArray<rexShell::ExportItemInfo>& infos, const atArray<rexObject*>& objects) const;

    bool m_ExportAsSingleEntity;
    rexShell::ExportItemInfo m_ExportItemInfo;
    atArray<atString> m_MDagPaths;
    atArray<atString> m_ExportNames;
};

}

#endif //__REX_MAYA_HAMSTER_OBJECT_H__
