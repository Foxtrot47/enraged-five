// 
// rexMaya/mayaUtility.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya 4.5 Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  NAMESPACES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexMayaUtility -- namespace for storing all Maya specific utility functions
//						   
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYA45_MAYAUTILITY_H__
#define __REX_MAYA45_MAYAUTILITY_H__

#define _BOOL
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MFnData.h>
#include <Maya/MPlug.h>
#include <Maya/MFn.h>
#include <Maya/MColorArray.h>
#include <Maya/MStringArray.h>
#pragma warning(pop)

#include "rexMaya/mayahelp.h"

#include "atl/array.h"
#include "rexBase/result.h"
#include "rexBase/serializer.h"
#include "rexBase/utility.h"
#include "rexGeneric/objectMesh.h"

#include "rexMaya/mayaSetCollection.h"
#include "shaderMaterial/shaderMaterialGeoTemplate.h"


#define NODE_RAGE_PHBOUNDBOX		0x7010
#define NODE_RAGE_PHBOUNDSPHERE		0x7012
#define NODE_RAGE_PHBOUNDCAPSULE	0x7011
#define NODE_RAGE_PHBOUNDCYLINDER	0x7023

/////////////////////////////////////////////////////////////////////////////////////

class MStatus;
class MTypeId;
class MDagPath;
class MDagPathArray;
class MMatrix;
class MObject;
class MFnAttribute;
class MFnPlugin;


class MFnDependencyNode;
class MFnSet;

class MTime;
class MString;
class MFnDagNode;

#include <string>
#include <vector>

using namespace std;

/////////////////////////////////////////////////////////////////////////////////////
namespace rage {

class Matrix34;
class Matrix44;
class atString;
class rexMayaSkeletonInfo;

namespace rexMayaUtility
{
	struct rexNodeInfo
	{
		const char *m_Name;
		const MTypeId *m_ID;
		void *(*m_CreatorFunction)(void);
		MStatus (*m_InitFunction)(void);
		int m_Type;
		const char *m_Classification;
	};

	struct rexNodeLimitInfo
	{
		enum
		{
			MINX = 0,
			MAXX,
			MINY,
			MAXY,
			MINZ,
			MAXZ
		} LimitChannel;

		rexNodeLimitInfo()
		{
			for(int i=0; i<6;i++)
			{
				trLimitEnables[i] = false;
				roLimitEnables[i] = false;
				scLimitEnables[i] = false;
			}
		}
		bool trLimitEnables[6];
		bool roLimitEnables[6];
		bool scLimitEnables[6];
	};

	typedef atMap<atString, rexNodeLimitInfo>	DpToLimitInfoTable;
	
	void CollectNodeLimitInfo(const MDagPathArray& dpArr, DpToLimitInfoTable& retTable);
	void SetNodeLimitEnables(const MDagPathArray& dpArr, bool bEnable);
	void SetNodeLimitEnables(const MDagPathArray& dpArr, const DpToLimitInfoTable& limitInfoTable);

	void ConstructNamespace();
	void DestructNamespace();

	void BeginRootSkeleton();
	void EndRootSkeleton();
	void AddRootSkeletonBone(const char* boneDagPathName,int boneIndex);
	int GetRootSkeletonBoneIndex(const char* boneDagPathName);
	int GetRootSkeletonNumBones();

	rexResult	RegisterNodes(MFnPlugin &plugin, rexNodeInfo *nodes);
	rexResult	DeregisterNodes(MFnPlugin &plugin, rexNodeInfo *nodes);

	bool		IsValidBoneNode( const MDagPath& dagpath, bool useOnlyJoints = false, bool name_NullReturnsFalse = true );

	bool		IsTypeOrHasTypeAsDescendant( const MDagPath& dagpath, MFn::Type type );
	bool		HasTypeAsAncestor( const MDagPath& dagpath, MFn::Type type );

	bool		IsLevelInstanceObject( const MDagPath& path, bool includeStatic = true, bool includeDynamic = true );
	bool		IsChildOfLevelInstanceObject( const MDagPath& path, bool includeStatic = true, bool includeDynamic = true );

	void		GetMatrix34FromMayaMatrix( Matrix34& m34, const MMatrix& mm );
	void		GetMatrix44FromMayaMatrix( Matrix44& m44, const MMatrix& mm );

	Matrix44	GetSkeletonWorldMatrix( MDagPath dp );
	Matrix44	GetSkeletonLocalMatrix( MDagPath dp, MDagPath parentDP );
	void		GetLocalMatrix(Matrix34 &m, const MFnDagNode &dagnode);
	bool		GetJointOrientMatrices(const MDagPath& dp,Matrix34& jointOrientMat,Matrix34& scaleOrientMat);

	bool		IsAncestorOf( const MDagPath& ancestor, const MDagPath& descendant );

	bool		FindNode( const char *name, MDagPath& output );

	bool		AttributeExists( const MObject obj, const char *attName );
	bool		AddAttribute( MObject obj, const char *attName, const char *attValue );
	bool		AddAttribute( MObject obj, const char *attName, float value ); // still stores as stirng
	bool		DeleteAttribute( MObject obj, const char *attName );
	bool		SetAttributeValue( MObject obj, const char *attName, const char *attValue );
	rexValue	GetAttributeValue( const MObject obj, const char *attName );
	atString	GetAttributeValueAsString( const MObject obj, const char *attName );

	atString	FullPathNameToBoneIdString( const MDagPath& dp );

	bool		GetUserDefinedAttributeNames( const MObject obj, atArray<atString>& outputNames, bool useLongNames = true );

	bool		GetAttributeConnections( const MObject obj, const char *attName, atArray<MDagPath>& outputDPArray, bool asSrc = true, bool asDest = true );

	void		PackVertexColours(const shaderMaterialGeoTemplate * pobTemplate, const MStringArray& asNamesOfMeshsColorSets, const vector<MColorArray>& asVertexColoursOfMeshsColorSets, vector<MColorArray>& obAObDestinationVertexColourSets);
	rexResult	ConvertMayaMeshNodeIntoGenericMesh( rexObjectGenericMesh *outputMesh, const MDagPath& dagpath, 
													int boneIndex, const Matrix44* boneMatrix, 
													int lodGroupIndex, int lodLevel, 
													float lodThreshold, const atString* groupID, 
													int groupIndex, bool checkNumVertWeights,
													bool exportTriangles = true,
													rexMayaSetCollection::SetMemberGroup *pSets = NULL,
													bool dataRelativeToTaggedRexRoot = false);
	
	bool		IsTaggedRootRageRexExportDataNode(MObject& node);

	atString	DagPathToName( const MDagPath& dp );
	atString	DagPathToNodeName( const MDagPath& dp );

	bool		IsParallelBlender(MObject obj);
	bool		GetBlendShapeNodeConnectedToMesh( const MDagPath &dp, MObject &blendShapeMObj );
	void		GetAllBlendShapeNodes( const MDagPath& dp, atArray<MObject>& outNodes );
	int			GetBlendTargetCount( const MObject& blendShapeMObj );
	rexResult	GetBlendTargetName( int index, const MObject& blendShapeMObj, atString& targetName);
	float		GetBlendTargetWeight( int index, const MObject& blendShapeMObj);
	rexResult	GetBlendTargetDeltas( const MObject& blendShapeMObj, atArray<rexObjectGenericMesh::BlendTargetDelta>& outDeltas );
	rexResult	GetBlendTargetNames( const MObject& blendShapeMObj, atArray<atString>& outNames );

	bool		GetMaterialInfo( rexObjectGenericMesh::MaterialInfo& matInfo, const MFnSet &shaderSet, const char *materialNamePostfix );
	void		GetFileTexturesFromTexNode( MObject objTex, atArray<atString>& list );
	void		GetFileTextureFromShader(MObject shader, atArray<atString>& list);
	void		GetFileTextureFromLayered(MObject layered, atArray<atString>& list);
	void		AddFileTexture(MObject tex, atArray<atString>& list);

	atString	FilenameFromPath( atString path );
	atString	BaseFilenameFromPath( atString path );  // w/o extension
	atString	ExtensionFromPath( atString path );
	atString	PathFromPath( atString path );

	MDagPath	GetSceneRootDagPath();

	bool		FindSkinCluster(const MDagPath& geom, MObject& cluster);
	bool		FindChainRoot(const char * name, MDagPath& root);

	int			FindDagPathInDagPathArray( const MDagPath& dp, const MDagPathArray& dpArray );

	void		GetAnimationStartAndEndFrames( MFnDependencyNode& fnDep, int& startFrame, int& endFrame, bool considerUpstreamNodes, bool considerDownstreamNodes );
	void		GetAnimationStartAndEndFrames( const MDagPath& dp, int& startFrame, int& endFrame, bool considerUpstreamNodes, bool considerDownstreamNodes );
	void		GetAnimationStartAndEndFrames( MObject obj, int& startFrame, int& endFrame, bool considerUpstreamNodes, bool considerDownstreamNodes );
	void		GetAnimationStartAndEndFrames( MString objectPath, int& startFrame, int& endFrame );
	void		GetAnimationStartAndEndFrames( const atArray<MDagPath>& animNodes, int& startFrame, int& endFrame );

	bool		IsAnimated( const MDagPath& dp, bool bCheckOnlyAnimCurves = true);
	bool		IsAnimated( const atArray<MDagPath>& nodes, bool bCheckOnlyAnimCurves = true);

	float		ConvertTime( const MTime& time );
	float		ConvertTime( float t );

	int         GetDPDepth( MDagPath dp );

	int			GetDescendantCount( MDagPath dp );

	void		MessageBoxOK( const char *info, const char *title );
	bool		MessageBoxYesNo( const char *info, const char *title );

	bool		GetBulletMargin(const MObject& mObj, float &retMargin);

	//////////////////////////////////////////
	// progress window functions

	// XXX - currently, a crash is possible if long info & title strings are passed to OpenProgressWindow (see utility.cpp for details)
	bool	OpenProgressWindow(const char *title, int numValues);
	bool	CloseProgressWindow(void);
	bool	UpdateProgressWindowInfo(int index, const char *info);
	bool	UpdateProgressWindowMax(int index, int max);
	bool	UpdateProgressWindowValue(int index, int value);

	// Cutscene
	void	SetFrame(int frame);
	int		GetFrame();
	void	GetMayaSceneStartAndEndFrames(int &startf, int &endf);
	void	GetMayaSceneStartFrame(float &sf);
	void	GetMayaSceneEndFrame(float &ef);

	// BindPose
	void 	ToggleBindPose(const MObject& cluster, bool gotoBindPose, atString& savePoseName);

	// Check how a node is animated
	bool	IsFrameDataNeeded( const MDagPath& node );
	bool	IsNodeDrivenByAnimCurve( const MDagPath& node );
	bool	IsNodeDrivenByAnimCurve( const MDagPath& node, const char* attr );
	bool	IsJointDrivenByIK( const MDagPath& joint );
	bool	IsAttrLocked(const MFnDagNode& node, const char* attr,bool falseIfNotKeyable = true);
	void	DebugBakeResults(const char* node, const char* attr);

	void	GetAttributeList( const MObject& node, atArray<atString>& attributes );

	MPlug	FindPlug( MDagPath dp, const char *name, MStatus* status = NULL, int dgDepth = 3 );
	MPlug	FindPlug( MFnDependencyNode fn, const char *name, MStatus* status = NULL, int dgDepth = 3 );

	MObject FindCurve( MPlug plug, bool considerParentPlugs = true, bool considerChildPlugs = true, int childIndex = -1 );

	bool	HasTextureAnimation( MDagPath dp );
	bool	HasTextureAnimation( MFnDependencyNode fn );	  
	
	// refreshes the maya GUI - useful for calling in the the debugger
	void	Refresh();

	void	PrintDataType(MFnData::Type type);
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
