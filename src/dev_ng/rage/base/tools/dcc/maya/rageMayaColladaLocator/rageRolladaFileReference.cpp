#include "rageRolladaFileReference.h"

#define USE_DISPLAY_LISTS

rageRolladaFileReference::rageRolladaFileReference(const char* pcFilename)
{
	// Create Rollada file
	m_pobRolladaFile = new rageRolladaFile(pcFilename);
	m_iReferenceCount = 0;
}

rageRolladaFileReference::~rageRolladaFileReference(void)
{
	delete m_pobRolladaFile;
}


