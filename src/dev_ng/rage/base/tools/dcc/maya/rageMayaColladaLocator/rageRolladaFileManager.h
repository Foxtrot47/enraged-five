#pragma once
#include "rageRolladaFileReference.h"

#include <string>
#include <vector>
using namespace std;
using namespace rage;

class rageRolladaFileManager
{
public:
	rageRolladaFileManager(void);
	~rageRolladaFileManager(void);

	// Functions
	const rageRolladaFile*	GetRolladaFile(const string& strPathAndFilename);
	void RemoveReferenceToRolladaFile(const string& strPathAndFilename);

private:
	vector<rageRolladaFileReference*>	m_obVectorOfFileReferences;
};


