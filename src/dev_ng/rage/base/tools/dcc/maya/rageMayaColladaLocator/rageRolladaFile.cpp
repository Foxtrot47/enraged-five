#include "rageRolladaFile.h"

#define USE_DISPLAY_LISTS

rageRolladaFile::rageRolladaFile(const char* pcFilename)
{
	m_bHaveOpenGLDisplayList = false;
	m_pobRolladaScene = NULL;

	// Record the path and filenames
	m_strBaseFilename = pcFilename;
	int iPosOfExtension = m_strBaseFilename.find_last_of('.');
	if(iPosOfExtension != -1)
	{
		m_strBaseFilename.resize(iPosOfExtension);
	}

	// Create Rollada scene
	m_pobRolladaScene = new rageRolladaScene();
	m_pobRolladaScene->CreateFromFile(pcFilename);
}

rageRolladaFile::~rageRolladaFile(void)
{
	DeleteScene();
}

void rageRolladaFile::DeleteDisplayList()
{
	if(m_bHaveOpenGLDisplayList)
	{
		glDeleteLists(m_uiOpenGLDisplayList, 1);
	}
	m_bHaveOpenGLDisplayList = false;
}


void rageRolladaFile::DeleteScene()
{
	DeleteDisplayList();
	delete m_pobRolladaScene;
	m_pobRolladaScene = NULL;
}

void rageRolladaFile::Draw(bool bCurrentTextureState) 
{
	// If texturing state has changed, I need to rethink everything
	if(m_bTexturingStateOnLastDraw != bCurrentTextureState)
	{
		// Texture state has changed, so I need to recompute display list
		DeleteDisplayList();
		m_bTexturingStateOnLastDraw = bCurrentTextureState;
	}

	if(!m_bHaveOpenGLDisplayList)
	{
	#ifdef USE_DISPLAY_LISTS
		m_uiOpenGLDisplayList = glGenLists(1);
		glNewList(m_uiOpenGLDisplayList, GL_COMPILE_AND_EXECUTE);
	#endif

		if(m_pobRolladaScene)
		{
			m_pobRolladaScene->OpenGLDraw();
		}

	#ifdef USE_DISPLAY_LISTS
		glEndList();
		m_bHaveOpenGLDisplayList = true;
	#endif
	}
	else
	{
		glCallList(m_uiOpenGLDisplayList);
	}
}
