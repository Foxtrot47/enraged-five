//
// Copyright (C) Rage
// 
// File: pluginMain.cpp
//
// Author: Maya Plug-in Wizard 2.0
//

// specify additional libraries to link to
#pragma comment(lib, "Foundation.lib")
#pragma comment(lib, "OpenMaya.lib")
#pragma comment(lib, "OpenMayaUI.lib")
#pragma comment(lib, "OpenGL32.lib")

#ifdef _DEBUG
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/lib-dbg/1.4/libcollada_dae_d.lib")
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/lib-dbg/1.4/libcollada_dom_d.lib")
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/lib-dbg/1.4/libcollada_LIBXMLPlugin_d.lib")
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/lib-dbg/1.4/libcollada_stdErrPlugin_d.lib")
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/lib-dbg/1.4/libcollada_STLDatabase_d.lib")
#else
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/lib/1.4/libcollada_dae.lib")
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/lib/1.4/libcollada_dom.lib")
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/lib/1.4/libcollada_LIBXMLPlugin.lib")
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/lib/1.4/libcollada_stdErrPlugin.lib")
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/lib/1.4/libcollada_STLDatabase.lib")
#endif
// #pragma comment(lib, "../3rdParty/COLLADA_DOM/external-libs/libxml2/win32/lib/libxml2.lib")
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/external-libs/libxml2/win32/lib/libxml2_a.lib")
#pragma comment(lib, "../../../3rdParty/COLLADA_DOM/external-libs/zlib/lib/zdll.lib")
#pragma comment(lib, "../../../3rdParty/libxml/lib/iconv_a.lib")
#pragma comment(lib, "../../../3rdParty/Devil/lib/DevIL.lib")
#pragma comment(lib, "../../../3rdParty/Devil/lib/ILUT.lib")
#pragma comment(lib, "../../../3rdParty/Devil/lib/ILU.lib")

#include "rageMayaColladaLocator.h"

#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MFnPlugin.h>
#pragma warning(pop)

MStatus initializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is loaded into Maya.  It 
//		registers all of the services that this plug-in provides with 
//		Maya.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{
	MStatus   status;
	MFnPlugin plugin( obj, "Rage", "7.0", "Any");

	status = plugin.registerNode( "rageMayaColladaLocator", rageMayaColladaLocator::id, rageMayaColladaLocator::creator,
								  rageMayaColladaLocator::initialize, MPxNode::kLocatorNode );
	if (!status) {
		status.perror("registerNode");
		return status;
	}

	rageMayaColladaLocator::gpobRolladaFileManager = new rageRolladaFileManager();

	return status;
}

MStatus uninitializePlugin( MObject obj)
//
//	Description:
//		this method is called when the plug-in is unloaded from Maya. It 
//		deregisters all of the services that it was providing.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{
	MStatus   status;
	MFnPlugin plugin( obj );

	status = plugin.deregisterNode( rageMayaColladaLocator::id );
	if (!status) {
		status.perror("deregisterNode");
		return status;
	}

	delete rageMayaColladaLocator::gpobRolladaFileManager;

	return status;
}

