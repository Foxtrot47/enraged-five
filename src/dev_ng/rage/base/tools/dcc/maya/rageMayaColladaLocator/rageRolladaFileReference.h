#pragma once
#include "rageRolladaFile.h"

#include <vector>
using namespace std;
using namespace rage;

class rageRolladaFileReference
{
public:
	rageRolladaFileReference(const char* pcFilename);
	~rageRolladaFileReference(void);

	// Functions
	void Draw(bool bTextured) const {(const_cast<rageRolladaFile*>(m_pobRolladaFile))->Draw(bTextured);}
	rageColladaBoundingBox	CalcBoundingBox() const {return GetRolladaFile()->CalcBoundingBox();}

	// Accessors
	string GetBaseFilename() const {return GetRolladaFile()->GetBaseFilename();}
	string GetRolladaFilename() const {return GetRolladaFile()->GetRolladaFilename();}
	string GetColladaFilename() const {return GetRolladaFile()->GetColladaFilename();}
	int GetReferenceCount() const {return m_iReferenceCount;}
	const rageRolladaFile* GetRolladaFile() const {return m_pobRolladaFile;}
	
	// Modifiers
	void IncreaseReference() {m_iReferenceCount++; /* Displayf("Increased reference count to %d on %s", m_iReferenceCount, GetBaseFilename().c_str());*/ }
	void DecreaseReference() {m_iReferenceCount--; /* Displayf("Decreased reference count to %d on %s", m_iReferenceCount, GetBaseFilename().c_str());*/ }

private:
	// Members
	rageRolladaFile*			m_pobRolladaFile;
	int							m_iReferenceCount;
};


