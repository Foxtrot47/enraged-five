#pragma once
#include "rageCollada/rageRolladaScene.h"

#include <vector>
using namespace std;
using namespace rage;

class rageRolladaFile
{
public:
	rageRolladaFile(const char* pcFilename);
	~rageRolladaFile(void);

	// Functions
	void Draw(bool bTextured);
	rageColladaBoundingBox	CalcBoundingBox() const {return m_pobRolladaScene->CalcBoundingBox();}

	// Accessors
	string GetBaseFilename() const {return m_strBaseFilename;}
	string GetRolladaFilename() const {return m_strBaseFilename +".rollada";}
	string GetColladaFilename() const {return m_strBaseFilename +".dae";}

private:
	// Functions
	void						DeleteScene();
	void						DeleteDisplayList();

	// Members
	string						m_strBaseFilename;
	rageRolladaScene*			m_pobRolladaScene;
	bool						m_bHaveOpenGLDisplayList;
	bool						m_bTexturingStateOnLastDraw;
	unsigned int				m_uiOpenGLDisplayList;
};


