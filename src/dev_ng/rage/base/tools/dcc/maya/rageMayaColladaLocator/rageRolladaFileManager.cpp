#include "rageRolladaFileManager.h"

rageRolladaFileManager::rageRolladaFileManager(void)
{
}

rageRolladaFileManager::~rageRolladaFileManager(void)
{
	for(unsigned i=0; i<m_obVectorOfFileReferences.size(); i++)
	{
		delete m_obVectorOfFileReferences[i];
	}
}

const rageRolladaFile*	rageRolladaFileManager::GetRolladaFile(const string& strPathAndFilename)
{
	// Get base name
	string strBasePathAndFilename = strPathAndFilename;
	int iPosOfExtension = strBasePathAndFilename.find_last_of('.');
	if(iPosOfExtension != -1)
	{
		strBasePathAndFilename.resize(iPosOfExtension);
	}

	// Now see if I already have this Rollada file
	for(unsigned i=0; i<m_obVectorOfFileReferences.size(); i++)
	{
		if(m_obVectorOfFileReferences[i]->GetBaseFilename() == strBasePathAndFilename)
		{
			// Bingo!
			// Displayf("Sharing %s as it is already loaded", strBasePathAndFilename.c_str());
			m_obVectorOfFileReferences[i]->IncreaseReference();
			return m_obVectorOfFileReferences[i]->GetRolladaFile();
		}
	}

	// If got here then I do not already have this file, so create it
	rageRolladaFileReference* pobRolladaFileReference = new rageRolladaFileReference(strBasePathAndFilename.c_str());
	m_obVectorOfFileReferences.push_back(pobRolladaFileReference);
	pobRolladaFileReference->IncreaseReference();
	return pobRolladaFileReference->GetRolladaFile();
}

void rageRolladaFileManager::RemoveReferenceToRolladaFile(const string& strPathAndFilename)
{
	// Get base name
	string strBasePathAndFilename = strPathAndFilename;
	int iPosOfExtension = strBasePathAndFilename.find_last_of('.');
	if(iPosOfExtension != -1)
	{
		strBasePathAndFilename.resize(iPosOfExtension);
	}

	// Now see if I already have this Rollada file
	for(unsigned i=0; i<m_obVectorOfFileReferences.size(); i++)
	{
		if(m_obVectorOfFileReferences[i]->GetBaseFilename() == strBasePathAndFilename)
		{
			// Bingo!
			// Displayf("Removing reference to %s", strBasePathAndFilename.c_str());
			m_obVectorOfFileReferences[i]->DecreaseReference();

			// Kill it?
			if(m_obVectorOfFileReferences[i]->GetReferenceCount() == 0)
			{
				delete m_obVectorOfFileReferences[i];
				m_obVectorOfFileReferences.erase(m_obVectorOfFileReferences.begin() + i);
				return;
			}
		}
	}
}
