#ifndef _rageMayaColladaLocator
#define _rageMayaColladaLocator
//
// Copyright (C) Rage
// 
// File: rageMayaColladaLocator.h
//
// Dependency Graph Node: rageMayaColladaLocator
//
// Author: Maya Plug-in Wizard 2.0
//
#define MAYA_PLUGIN
#include "rageRolladaFileManager.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxLocatorNode.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h> 
#include <maya/MTypeId.h> 
#include <maya/MBoundingBox.h>
#pragma warning(pop)

#include <vector>
using namespace std;
using namespace rage;

class rageMayaColladaLocator : public MPxLocatorNode
{
public:
						rageMayaColladaLocator();
	virtual				~rageMayaColladaLocator(); 

	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );
	virtual void draw(M3dView & view, const MDagPath & path, M3dView::DisplayStyle style, M3dView::DisplayStatus DisplayStatus );

	static  void*		creator();
	static  MStatus		initialize();

//	virtual bool  isBounded () const {return true;}
	virtual MBoundingBox  boundingBox () const {return m_obBoundingBox;}

public:

	// There needs to be a MObject handle declared for each attribute that
	// the node will have.  These handles are needed for getting and setting
	// the values later.
	static  MObject		m_obColladaPathAndFilenameAttribute;		// Example m_obColladaPathAndFilenameAttribute attribute
	static  MObject		m_obDrawBoundingBoxAttribute;
	static  MObject		m_obAndyWarholModeAttribute;
	static  MObject		m_obTriggerRedrawAttribute;		// Example m_obColladaPathAndFilenameAttribute attribute
	static  MObject		m_obTriggerReloadAttribute;		// Example m_obColladaPathAndFilenameAttribute attribute


	// The typeid is a unique 32bit indentifier that describes this node.
	// It is used to save and retrieve nodes of this type from the binary
	// file format.  If it is not unique, it will cause file IO problems.
	//
	static	MTypeId		id;

	// Share one file manager between all nodes
	static rageRolladaFileManager*	gpobRolladaFileManager;

private:
	MBoundingBox				m_obBoundingBox;
	void						DeleteFile();

	// Keep a handy pointer to the Rollada file, remember however that I DO NOT own this file, gpobRolladaFileManager does
	// This is just a handy pointer so I don't have to keep looking it up all the time
	const rageRolladaFile*	m_pobRolladaFile;

};

#endif

