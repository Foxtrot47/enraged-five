//
// Copyright (C) Rage
// 
// File: rageMayaColladaLocator.cpp
//
// Dependency Graph Node: rageMayaColladaLocator
//
// Author: Maya Plug-in Wizard 2.0
//
#include "rageMayaColladaLocator.h"

#pragma warning(push)
#pragma warning(disable : 4263)
#pragma warning(disable : 4100)
#include <dae.h>
#include <1.4/dom/domCOLLADA.h>
#pragma warning(pop)

#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFileObject.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>
#include <maya/MGlobal.h>
#include <maya/MItDag.h>
#pragma warning(pop)

using namespace rage;

//#define SAMPLE_FILE "T:/temp/ColladaLand/Arrow2_Animated.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Texturing7_Bump.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Triangle.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TwoTrianglesFormingAQuad.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TwoTrianglesFormingAQuadWithVertexFaceNormals.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Cube.xml.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/CrazyPoly.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TwoTriangles.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/DonutsWithBakedTransforms.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/DonutsWithoutBakedTransforms.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Text.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Brothel.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/rageGirl.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Jasper.dae"
//#define SAMPLE_FILE "//krose/share/temp/ColladaLand/Mini.dae"
//#define SAMPLE_FILE "t:/rage/assets/GEOs/GEOTypes/horse/VisibleGeometry/VisibleGeometry.dae"
//#define SAMPLE_FILE "t:/rage/assets/GEOs/GEOTypes/gatlinggun/VisibleGeometry/VisibleGeometry.dae"
//#define SAMPLE_FILE "t:/rage/assets/GEOs/GEOTypes/house/VisibleGeometry/VisibleGeometry.dae"
//#define SAMPLE_FILE "t:/rage/assets//geos/geotypes/saloonii/exterior/visiblegeometry/visiblegeometry.dae"
//#define SAMPLE_FILE "T:/rage/assets/GEOs/GEOInstanceProxies/tree.dae"
//#define SAMPLE_FILE "t:/rage/assets/GEOs/GEOTypes/rockbig/VisibleGeometry/VisibleGeometry.rollada"
//#define SAMPLE_FILE "t:/rage/assets/GEOs/GEOTypes/jasper/VisibleGeometry/VisibleGeometry.dae"
//#define SAMPLE_FILE "t:/rage/assets/GEOs/GEOTypes/maze/VisibleGeometry/VisibleGeometry.dae"
//#define SAMPLE_FILE "t:/rage/assets/GEOs/GEOTypes/mini/VisibleGeometry/VisibleGeometry.dae"
//#define SAMPLE_FILE "t:/rage/assets/GEOs/GEOTypes/ragegirl/VisibleGeometry/VisibleGeometry.dae"
//#define SAMPLE_FILE "t:/rage/assets/GEOs/GEOTypes/Saloon/VisibleGeometry/VisibleGeometry.dae"
//#define SAMPLE_FILE "T:/rage/assets/geos/GEOTypes/tent/Model/Model.rollada"
#define SAMPLE_FILE "t:/rage/assets/geos/GEOInstanceProxies/ElmStocky_Fall_1.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand1.4/Triangle.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand1.4/TriangleTextured.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand1.4/TwoTrianglesInOneMeshTexturedDifferently.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Mini.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Gun.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/SimpleSkeleton1.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TexturedQuadsWithTwoTextures.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/ColourTriangle.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TexturedQuad.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/TexturedQuadWithTwoTextures.dae"
//#define SAMPLE_FILE "C:/soft/rage/base/tools/COLLADA_DOM/samples/data/cube.dae"
//#define SAMPLE_FILE "../../../../../soft/rage/base/tools/COLLADA_DOM/samples/data/cube.dae"
//#define SAMPLE_FILE "../COLLADA_DOM/samples/data/cube.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/MultipleUV3.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Texturing7_Bump.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Texturing3.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/f_ott_bh_lng_nr_002_PH.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_BMW3.DAE"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_City.DAE"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3Ds_Hippo.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_K9.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_LivingRoom.DAE"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_Ruby.DAE"
//#define SAMPLE_FILE "T:/temp/ColladaLand/3DS_Yacht.DAE"
//#define SAMPLE_FILE "T:/temp/ColladaLand/MC4Block.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Prostitute001_ready.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/Prostitute001_ready.rollada"
//#define SAMPLE_FILE "T:/temp/ColladaLand/streetcorner.dae"
//#define SAMPLE_FILE "T:/temp/ColladaLand/streetcorner.rollada"
//#define SAMPLE_FILE "T:/temp/ColladaLand/arm_sherriffsOffice01x.rollada"


MTypeId     rageMayaColladaLocator::id( 0x7020 );
rageRolladaFileManager*	rageMayaColladaLocator::gpobRolladaFileManager;

// Example attributes
// 
MObject     rageMayaColladaLocator::m_obColladaPathAndFilenameAttribute;
MObject     rageMayaColladaLocator::m_obDrawBoundingBoxAttribute;
MObject     rageMayaColladaLocator::m_obAndyWarholModeAttribute;
MObject     rageMayaColladaLocator::m_obTriggerRedrawAttribute;
MObject     rageMayaColladaLocator::m_obTriggerReloadAttribute;

rageMayaColladaLocator::rageMayaColladaLocator() 
{
	m_pobRolladaFile = NULL;
}
rageMayaColladaLocator::~rageMayaColladaLocator() 
{
	DeleteFile();
}

void rageMayaColladaLocator::DeleteFile()
{
	if(m_pobRolladaFile != NULL)
	{
		gpobRolladaFileManager->RemoveReferenceToRolladaFile(m_pobRolladaFile->GetBaseFilename());
	}
}


MStatus rageMayaColladaLocator::compute( const MPlug& plug, MDataBlock& data )
//
//	Description:
//		This method computes the value of the given m_obTriggerRedrawAttribute plug based
//		on the values of the m_obColladaPathAndFilenameAttribute attributes.
//
//	Arguments:
//		plug - the plug to compute
//		data - object that provides access to the attributes for this node
//
{
	MStatus returnStatus;
	// Check which m_obTriggerRedrawAttribute attribute we have been asked to compute.  If this 
	// node doesn't know how to compute it, we must return 
	// MS::kUnknownParameter.
	// 
	if( plug == m_obTriggerReloadAttribute )
	{
		// Get a handle to the m_obColladaPathAndFilenameAttribute attribute that we will need for the
		// computation.  If the value is being supplied via a connection 
		// in the dependency graph, then this call will cause all upstream  
		// connections to be evaluated so that the correct value is supplied.
		MDataHandle m_obColladaPathAndFilenameAttributeData = data.inputValue( m_obColladaPathAndFilenameAttribute, &returnStatus );

		if( returnStatus != MS::kSuccess )
		{
			MGlobal::displayError( "Node rageMayaColladaLocator cannot get value\n" );
		}
		else
		{
			// Read the m_obColladaPathAndFilenameAttribute value from the handle.
			MString strColladaPathAndFilename = m_obColladaPathAndFilenameAttributeData.asString();
			const char* pcColladaPathAndFilename = NULL;
			if(strColladaPathAndFilename == "")
			{
				strColladaPathAndFilename = SAMPLE_FILE;
				pcColladaPathAndFilename = SAMPLE_FILE;
			}
			if(strColladaPathAndFilename != "")
			{
				//printf("About to print\n");
				//printf("strColladaPathAndFilename = %s\n", strColladaPathAndFilename.asChar());
				//printf("Printed\n");
				// Clean up the old data
				DeleteFile();

				// Ok, now this is a bit weird, but for optimization reasons, see if there already is 
				// a rageMayaColladaLocator in the scene with this filename
				// Get a list of all the current rageMayaColladaLocators

				// Does the new filename even exist?
				MFileObject obColladaFile;
				obColladaFile.setFullName(strColladaPathAndFilename);
				if(obColladaFile.exists())
				{
					// File exists, so process it
					// Open the collada file
					// Instantiate the reference implementation
					//load the collada files specified in the arguments
					const char *pcFilename = strColladaPathAndFilename.asChar();

					// Make sure all my slashes are standard
					int iFilenameLength = (int)strlen(pcFilename);
					char acFilename[255];
					strcpy(acFilename, pcFilename);
					for(int i=0; i<iFilenameLength; i++) 
					{
						if(acFilename[i] == '\\')
						{
							acFilename[i] = '/';
						}
					}
					pcFilename = &acFilename[0];
					// cout<<"pcFilename = "<<pcFilename<<endl;

					// Process it
					m_pobRolladaFile = gpobRolladaFileManager->GetRolladaFile(pcFilename);
				}
				else
				{
					MGlobal::displayError("Unable to load file "+ strColladaPathAndFilename);
				}
			}

			//// New file loaded, so recompute bounding box
			m_obBoundingBox.clear();
			if(m_pobRolladaFile)
			{
				rageColladaBoundingBox obBB = m_pobRolladaFile->CalcBoundingBox();
				m_obBoundingBox.expand(MPoint(obBB.GetMin().GetX(), obBB.GetMin().GetY(), obBB.GetMin().GetZ()));
				m_obBoundingBox.expand(MPoint(obBB.GetMax().GetX(), obBB.GetMax().GetY(), obBB.GetMax().GetZ()));
			}

			// Mark the destination plug as being clean.  This will prevent the
			// dependency graph from repeating this calculation until an m_obColladaPathAndFilenameAttribute 
			// of this node changes.
			// 
			data.setClean(plug);
		}
	}
	else if( plug == m_obTriggerRedrawAttribute )
	{
		// Something has changed to trigger a redraw
	}
	else 
	{
		return MS::kUnknownParameter;
	}

	return MS::kSuccess;
}

void* rageMayaColladaLocator::creator()
//
//	Description:
//		this method exists to give Maya a way to create new objects
//      of this type. 
//
//	Return Value:
//		a new object of this type
//
{
	return new rageMayaColladaLocator();
}

MStatus rageMayaColladaLocator::initialize()
//
//	Description:
//		This method is called to create and initialize all of the attributes
//      and attribute dependencies for this node type.  This is only called 
//		once when the node type is registered with Maya.
//
//	Return Values:
//		MS::kSuccess
//		MS::kFailure
//		
{
	// This sample creates a single m_obColladaPathAndFilenameAttribute float attribute and a single
	// m_obTriggerRedrawAttribute float attribute.
	//
	MFnNumericAttribute nAttr;
	MFnTypedAttribute	obStrAttr;
	MStatus				obStatus;

	m_obColladaPathAndFilenameAttribute = obStrAttr.create("ColladaPathAndFilename", "file", MFnData::kString, MObject::kNullObj, &obStatus);
	m_obDrawBoundingBoxAttribute = nAttr.create( "DrawBoundingBox", "dbb", MFnNumericData::kBoolean, false );
	m_obAndyWarholModeAttribute = nAttr.create( "AndyWarhol", "and", MFnNumericData::kBoolean, false );

	m_obTriggerRedrawAttribute = nAttr.create( "TriggerRedraw", "redraw", MFnNumericData::kFloat, 0.0 );
	// Attribute is read-only because it is an m_obTriggerRedrawAttribute attribute
	nAttr.setWritable(false);
	// Attribute will not be written to files when this type of node is stored
	nAttr.setStorable(false);

	m_obTriggerReloadAttribute = nAttr.create( "TriggerReload", "reload", MFnNumericData::kFloat, 0.0 );
	// Attribute is read-only because it is an m_obTriggerRedrawAttribute attribute
	nAttr.setWritable(false);
	// Attribute will not be written to files when this type of node is stored
	nAttr.setStorable(false);

	// Add the attributes we have created to the node
	//
	obStatus = addAttribute( m_obColladaPathAndFilenameAttribute );
		if (!obStatus) { obStatus.perror("addAttribute"); return obStatus;}
	obStatus = addAttribute( m_obDrawBoundingBoxAttribute );
		if (!obStatus) { obStatus.perror("addAttribute"); return obStatus;}
	obStatus = addAttribute( m_obAndyWarholModeAttribute );
		if (!obStatus) { obStatus.perror("addAttribute"); return obStatus;}
	obStatus = addAttribute( m_obTriggerRedrawAttribute );
		if (!obStatus) { obStatus.perror("addAttribute"); return obStatus;}
	obStatus = addAttribute( m_obTriggerReloadAttribute );
		if (!obStatus) { obStatus.perror("addAttribute"); return obStatus;}

	// Set up a dependency between the m_obColladaPathAndFilenameAttribute and the m_obTriggerRedrawAttribute.  This will cause
	// the m_obTriggerRedrawAttribute to be marked dirty when the m_obColladaPathAndFilenameAttribute changes.  The m_obTriggerRedrawAttribute will
	// then be recomputed the next time the value of the m_obTriggerRedrawAttribute is requested.
	//
	obStatus = attributeAffects( m_obColladaPathAndFilenameAttribute, m_obTriggerReloadAttribute );
		if (!obStatus) { obStatus.perror("attributeAffects"); return obStatus;}
	obStatus = attributeAffects( m_obAndyWarholModeAttribute, m_obTriggerRedrawAttribute );
		if (!obStatus) { obStatus.perror("attributeAffects"); return obStatus;}
	obStatus = attributeAffects( m_obDrawBoundingBoxAttribute, m_obTriggerRedrawAttribute );
		if (!obStatus) { obStatus.perror("attributeAffects"); return obStatus;}

	return MS::kSuccess;
}

void rageMayaColladaLocator::draw(M3dView & view,
	const MDagPath&, 
	M3dView::DisplayStyle,
	M3dView::DisplayStatus DisplayStatus )
{
	MStatus obStatus;

	bool bIsWireframe = ((view.displayStyle() == M3dView::kWireFrame) || (view.displayStyle() == M3dView::kPoints) || (view.displayStyle() == M3dView::kBoundingBox));

//	cout<<path.fullPathName().asChar()<<endl;

	// Make sure the m_obTriggerRedrawAttribute is clean
	MPlug obTriggerRedrawAttributePlug( thisMObject(), m_obTriggerRedrawAttribute );
	float fJustSomethingToTriggerTheComputerMethodIfTheFilenameHasChanged;
	obTriggerRedrawAttributePlug.getValue(fJustSomethingToTriggerTheComputerMethodIfTheFilenameHasChanged);

	MPlug obTriggerReloadAttributePlug( thisMObject(), m_obTriggerReloadAttribute );
	obTriggerReloadAttributePlug.getValue(fJustSomethingToTriggerTheComputerMethodIfTheFilenameHasChanged);

	// Get the name of the map Object
	MString strColladaPathAndFilename;
	MPlug obInputTextPlug( thisMObject(), m_obColladaPathAndFilenameAttribute);
	obStatus = obInputTextPlug.getValue(strColladaPathAndFilename);
	if(strColladaPathAndFilename == "")
	{
		strColladaPathAndFilename = SAMPLE_FILE;
	}

	if(strColladaPathAndFilename.length() == 0)
	{
		return; // nothing to do...
	}

	view.beginGL();
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	// Setup Transparency mode
	// glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	glEnable(GL_COLOR_MATERIAL);

	// Bounding box
	MPlug obDrawBoundingBoxPlug( thisMObject(), m_obDrawBoundingBoxAttribute );
	bool bDrawBoundingBox;
	obDrawBoundingBoxPlug.getValue(bDrawBoundingBox);
	if(bDrawBoundingBox || (DisplayStatus == M3dView::kActive) || (DisplayStatus == M3dView::kLead))
	{
		glColor3f(1.0f, 0.0f, 0.0f);
		float fMinX = (float)m_obBoundingBox.min().x;
		float fMinY = (float)m_obBoundingBox.min().y;
		float fMinZ = (float)m_obBoundingBox.min().z;
		float fMaxX = (float)m_obBoundingBox.max().x;
		float fMaxY = (float)m_obBoundingBox.max().y;
		float fMaxZ = (float)m_obBoundingBox.max().z;

		// Enable line stippling
		glEnable(GL_LINE_STIPPLE);
		// Set the stippling pattern
		glLineStipple(3, 0xAAAA);

		// Draw BB
		glBegin(GL_LINE_LOOP);
			glVertex3f(fMinX, fMinY, fMinZ);
			glVertex3f(fMinX, fMaxY, fMinZ);
			glVertex3f(fMinX, fMaxY, fMaxZ);
			glVertex3f(fMinX, fMinY, fMaxZ);
		glEnd();

		glBegin(GL_LINE_LOOP);
			glVertex3f(fMaxX, fMinY, fMinZ);
			glVertex3f(fMaxX, fMaxY, fMinZ);
			glVertex3f(fMaxX, fMaxY, fMaxZ);
			glVertex3f(fMaxX, fMinY, fMaxZ);
		glEnd();

		glBegin(GL_LINES);
			glVertex3f(fMinX, fMinY, fMinZ);
			glVertex3f(fMaxX, fMinY, fMinZ);

			glVertex3f(fMinX, fMaxY, fMinZ);
			glVertex3f(fMaxX, fMaxY, fMinZ);

			glVertex3f(fMinX, fMinY, fMaxZ);
			glVertex3f(fMaxX, fMinY, fMaxZ);

			glVertex3f(fMinX, fMaxY, fMaxZ);
			glVertex3f(fMaxX, fMaxY, fMaxZ);
		glEnd();
	}

	// active and dormant colors are the same...?? hmmm
	// maya's locators have 6 as the dormant,
	// objects - 4 as dormant 
	// all have 18 as active
	if(bIsWireframe)
	{
//		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHTING);
//		gluQuadricDrawStyle(pQuadric, GLU_LINE);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		// Configure and enable light source 1 
		glEnable( GL_LIGHTING ); 
//		glDisable(GL_LIGHTING);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		if(view.displayStyle() == M3dView::kFlatShaded )
		{
			glShadeModel(GL_FLAT);
		}
		else
		{
			glShadeModel(GL_SMOOTH);
		}
	}


	MColor DrawColor;
	switch(DisplayStatus)
	{
	case M3dView::kActive: // ok
		DrawColor.r = 0.85f;
		DrawColor.g = 0.85f;
		DrawColor.b = 0.85f;
		DrawColor.a = 1.0f;
		break;
	case M3dView::kLead:	// ok
		DrawColor.r = 0.95f;
		DrawColor.g = 0.95f;
		DrawColor.b = 0.95f;
		DrawColor.a = 1.0f;
		break;
	case M3dView::kDormant: // same as other locators - could change for map object
		DrawColor.r = 0.5f;
		DrawColor.g = 0.5f;
		DrawColor.b = 0.5f;
		DrawColor.a = 1.0f;
		break;
	case M3dView::kTemplate:
		DrawColor.r = 0.6f;
		DrawColor.g = 0.4f;
		DrawColor.b = 0.4f;
		DrawColor.a = 1.0f;
		break;
	case M3dView::kActiveTemplate:
		DrawColor.r = 0.6f;
		DrawColor.g = 0.6f;
		DrawColor.b = 0.4f;
		DrawColor.a = 1.0f;
		break;
	default: // E.g when the object is constrained
		DrawColor.r = 0.0f;
		DrawColor.g = 0.5f;
		DrawColor.b = 1.0f;
		DrawColor.a = 1.0f;
		break;
	}
	glColor4f(DrawColor.r, DrawColor.g, DrawColor.b, DrawColor.a);
	//glColor4f(0.5f, 0.5f, 0.5f, 0.0f);

	// Is texturing on?
	bool bCurrentTextureState = (!bIsWireframe && view.textureMode());
	if(bCurrentTextureState)
	{
		glEnable(GL_TEXTURE_2D);
	}

	(const_cast<rageRolladaFile*>(m_pobRolladaFile))->Draw(bCurrentTextureState);

	if(bCurrentTextureState)
	{
		glDisable(GL_TEXTURE_2D);
	}
	glPopAttrib();
	view.endGL();
}

