// polySurfaceShape1277694268
// Copyright (C) 2000 Kevin's Plugin Factory 
// 
// File: rageCleanUpSceneCmd.cpp
//
// MEL Command: rageCleanUpScene
//
// Author: Maya SDK Wizard
//
// Includes everything needed to register a simple MEL command with Maya.
// male_medium_shared_low_peek_right_start_shotgun_weapon_m16_materialInfo1
#include <maya/MSimple.h>
#ifdef USE_RECURRENCE
// #include <maya/MObjectArray.h>
#endif
#include <maya/MStringArray.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MDagPathArray.h>
#include <maya/MFnDagNode.h>
#include <maya/MDagPath.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MPlugArray.h>
#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>
#include <maya/MItDependencyGraph.h>
#include <maya/MItDag.h>
#include <maya/MFnAttribute.h>
#include <maya/MFnMesh.h>


#include <ATLComTime.h>

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

#define OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
// #define READ_ONLY
#define TAB_CHAR "\t"

// Set up the thresholds
// #define NEVER_DELETE_NODES_CALLED_NO 20
// #define NEVER_DELETE_NODES_CALLED {"world","groundPlane","hyperGraphLayout","characterPartition","defaultLightList1","defaultHardwareRenderGlobals","ikSystem","strokeGlobals","dynController1","perspShape","topShape","frontShape","sideShape","defaultObjectSet","defaultLayer","globalRender","polyMergeEdgeToolDefaults","polyMergeFaceToolDefaults","swatchShadingGroup","globalCacheControl"};
#define NEVER_DELETE_NODES_CALLED_NO 246
#define NEVER_DELETE_NODES_CALLED {"time1", "world", "renderPartition", "renderGlobalsList1", "defaultLightList1", \
								   "defaultShaderList1", "postProcessList1", "defaultRenderUtilityList1", "lightList1", \
								   "defaultTextureList1", "lambert1", "particleCloud1", "initialShadingGroup", \
								   "initialParticleSE", "initialMaterialInfo", "shaderGlow1", "dof1", "defaultRenderGlobals", \
								   "defaultRenderQuality", "defaultResolution", "defaultLightSet", "defaultObjectSet", \
								   "hardwareRenderGlobals", "groundPlane_transform", "groundPlane", "characterPartition", \
								   "defaultHardwareRenderGlobals", "ikSystem", "hyperGraphInfo", "hyperGraphLayout", \
								   "globalCacheControl", "DefaultGeometryFilter", "objectTypeFilter1", "objectTypeFilter2", \
								   "selectionListOperator1", "objectTypeFilter3", "selectionListOperator2", "objectTypeFilter4", \
								   "selectionListOperator3", "DefaultNURBSObjectsFilter", "objectTypeFilter5", "objectTypeFilter6", \
								   "selectionListOperator4", "DefaultPolygonObjectsFilter", "objectTypeFilter7", \
								   "DefaultSubdivObjectsFilter", "objectTypeFilter8", "DefaultCameraShapesFilter", \
								   "objectTypeFilter9", "DefaultJointsFilter", "objectTypeFilter10", "DefaultIkHandlesFilter", \
								   "objectTypeFilter11", "characterSetsFilter", "objectTypeFilter12", "partitionFilter", \
								   "objectTypeFilter13", "DefaultSetsFilter", "objectTypeFilter14", "defaultSetFilter", \
								   "objectScriptFilter1", "clusterSetsFilter", "objectScriptFilter2", "deformerSetsFilter", \
								   "objectScriptFilter3", "latticeSetsFilter", "objectScriptFilter4", "nonLinearSetsFilter", \
								   "objectScriptFilter5", "skinClusterSetsFilter", "objectScriptFilter6", "jointClusterSetsFilter", \
								   "objectScriptFilter7", "otherDeformerSetsFilter", "objectScriptFilter8", "DefaultAllLightsFilter", \
								   "DefaultOpticalFXFilter", "objectTypeFilter15", "DefaultLightsAndOpticalFXFilter", "DefaultAllLightsFilter1", \
								   "DefaultOpticalFXFilter1", "objectTypeFilter16", "selectionListOperator5", "lightSetFilter", \
								   "DefaultExclLightShapesFilter", "DefaultNonExclLightShapesFilter", "renderableObjectSetFilter", \
								   "renderableObjectShapeFilter", "objectTypeFilter17", "objectTypeFilter18", "selectionListOperator6", \
								   "objectTypeFilter19", "selectionListOperator7", "DefaultShadingGroupsFilter", "objectTypeFilter20", \
								   "DefaultBakeSetsFilter", "objectTypeFilter21", "objectTypeFilter22", "selectionListOperator8", \
								   "DefaultMaterialsFilter", "DefaultTexturesFilter", "DefaultTextures2dFilter", "DefaultTextures3dFilter", \
								   "DefaultMaterialsTexturesLightShapesFilter", "DefaultShaderGlowFilter", "objectTypeFilter23", \
								   "DefaultNoShaderGlowFilter", "objectTypeFilter24", "DefaultLightShapesTexturesFilter", \
								   "DefaultLightLinkingLightFilter", "DefaultAllLightsFilter2", "lightSetFilter1", \
								   "selectionListOperator9", "DefaultLightShapesPostProcsFilter", "DefaultLightShapesFilter", \
								   "DefaultLightShapesPostProcsFilter1", "DefaultShaderGlowFilter1", "objectTypeFilter25", \
								   "selectionListOperator10", "DefaultImagePlanesFilter", "objectTypeFilter26", \
								   "DefaultCameraShapesImagePlanesFilter", "DefaultCameraShapesFilter1", "objectTypeFilter27", \
								   "DefaultImagePlanesFilter1", "objectTypeFilter28", "selectionListOperator11", "DefaultPostProcFilter", \
								   "DefaultTexturesSGFilter", "DefaultTexturesFilter1", "DefaultShadingGroupsFilter1", "objectTypeFilter29", \
								   "selectionListOperator12", "DefaultSGLightShapesTexturesFilter", "DefaultLightShapesTexturesFilter1", \
								   "DefaultShadingGroupsFilter2", "objectTypeFilter30", "selectionListOperator13", "DefaultSGLightShapesFilter", \
								   "DefaultLightShapesPostProcsFilter2", "DefaultShadingGroupsFilter3", "objectTypeFilter31", \
								   "selectionListOperator14", "DefaultBasicRenderNodesFilter", "DefaultMaterialsTexturesLightShapesFilter1", \
								   "DefaultShadingGroupsFilter4", "objectTypeFilter32", "selectionListOperator15", \
								   "DefaultShadingGroupsAndMaterialsFilter", "DefaultMaterialsFilter1", "DefaultShadingGroupsFilter5", \
								   "objectTypeFilter33", "selectionListOperator16", "DefaultMaterialsAndShaderGlowFilter", \
								   "DefaultMaterialsFilter2", "DefaultShaderGlowFilter2", "objectTypeFilter34", "selectionListOperator17", \
								   "DefaultRenderUtilitiesFilter", "DefaultUsesImageFileFilter", "objectScriptFilter9", \
								   "DefaultAllRenderClassificationsFilter", "DefaultAllRenderNodesFilter", "DefaultAllRenderClassificationsFilter1", \
								   "DefaultShadingGroupsFilter6", "objectTypeFilter38", "selectionListOperator21", "DefaultHiddenAttributesFilter", \
								   "objectAttrFilter1", "animCurveFilter", "objectAttrFilter2", "expressionFilter", "objectAttrFilter3", "keyableFilter", \
								   "objectAttrFilter4", "scaleFilter", "objectNameFilter1", "rotateFilter", "objectNameFilter2", "translateFilter", \
								   "objectNameFilter3", "scaleRotateTranslateFilter", "objectAttrFilter5", "objectTypeFilter43", "selectionListOperator26", \
								   "renderableObjectsAndSetsFilter", "renderableObjectShapeFilter1", "selectionListOperator27", "objectTypeFilter44", \
								   "selectionListOperator28", "objectTypeFilter45", "selectionListOperator29", "objectTypeFilter46", "objectTypeFilter47", \
								   "renderableObjectSetFilter1", "selectionListOperator30", "lightLinkingObjectFilter", "renderableObjectsAndSetsFilter1", \
								   "selectionListOperator31", "renderableObjectShapeFilter2", "selectionListOperator32", "objectTypeFilter48", \
								   "selectionListOperator33", "objectTypeFilter49", "selectionListOperator34", "objectTypeFilter50", "objectTypeFilter51", \
								   "renderableObjectSetFilter2", "DefaultShadingGroupsFilter7", "objectTypeFilter52", "selectionListOperator35", \
								   "dynController1", "polyMergeEdgeToolDefaults", "polyMergeFaceToolDefaults", "persp", "perspShape", "top", \
								   "topShape", "front", "frontShape", "side", "sideShape", "lightLinker1", "brush1", "strokeGlobals", "layersFilter", \
								   "objectTypeFilter53", "renderLayersFilter", "objectTypeFilter54", "renderingSetsFilter", "objectTypeFilter55", \
								   "relationshipPanel1LeftAttrFilter", "relationshipPanel1RightAttrFilter", "layerManager", "defaultLayer", \
								   "renderLayerManager", "defaultRenderLayer", "globalRender", "uiConfigurationScriptNode", "sceneConfigurationScriptNode", \
								   "DefaultMaterialsAndShaderGlowFilter1", "DefaultTexturesFilter2""DefaultTexturesFilter3", "DefaultRenderUtilitiesFilter1", \
								   "DefaultLightsAndOpticalFXFilter1", "DefaultCameraShapesImagePlanesFilter1", "DefaultBakeSetsFilter1"};

#define NEVER_IMPORTANT_NODES_CALLED_NO 244
#define NEVER_IMPORTANT_NODES_CALLED {"time1", "world", "renderPartition", "renderGlobalsList1", "defaultLightList1", \
									  "defaultShaderList1", "postProcessList1", "defaultRenderUtilityList1", "lightList1", \
									  "defaultTextureList1", "lambert1", "particleCloud1", "initialShadingGroup", "initialParticleSE", \
									  "initialMaterialInfo", "shaderGlow1", "dof1", "defaultRenderGlobals", "defaultRenderQuality", \
									  "defaultResolution", "defaultLightSet", "defaultObjectSet", "hardwareRenderGlobals", \
									  "groundPlane_transform", "groundPlane", "characterPartition", "defaultHardwareRenderGlobals", \
									  "ikSystem", "hyperGraphInfo", "hyperGraphLayout", "globalCacheControl", "DefaultGeometryFilter", \
									  "objectTypeFilter1", "objectTypeFilter2", "selectionListOperator1", "objectTypeFilter3", \
									  "selectionListOperator2", "objectTypeFilter4", "selectionListOperator3", "DefaultNURBSObjectsFilter", \
									  "objectTypeFilter5", "objectTypeFilter6", "selectionListOperator4", "DefaultPolygonObjectsFilter", \
									  "objectTypeFilter7", "DefaultSubdivObjectsFilter", "objectTypeFilter8", "DefaultCameraShapesFilter", \
									  "objectTypeFilter9", "DefaultJointsFilter", "objectTypeFilter10", "DefaultIkHandlesFilter", \
									  "objectTypeFilter11", "characterSetsFilter", "objectTypeFilter12", "partitionFilter", "objectTypeFilter13", \
									  "DefaultSetsFilter", "objectTypeFilter14", "defaultSetFilter", "objectScriptFilter1", "clusterSetsFilter", \
									  "objectScriptFilter2", "deformerSetsFilter", "objectScriptFilter3", "latticeSetsFilter", \
									  "objectScriptFilter4", "nonLinearSetsFilter", "objectScriptFilter5", "skinClusterSetsFilter", \
									  "objectScriptFilter6", "jointClusterSetsFilter", "objectScriptFilter7", "otherDeformerSetsFilter", \
									  "objectScriptFilter8", "DefaultAllLightsFilter", "DefaultOpticalFXFilter", "objectTypeFilter15", \
									  "DefaultLightsAndOpticalFXFilter", "DefaultAllLightsFilter1", "DefaultOpticalFXFilter1", "objectTypeFilter16", \
									  "selectionListOperator5", "lightSetFilter", "DefaultExclLightShapesFilter", "DefaultNonExclLightShapesFilter", \
									  "renderableObjectSetFilter", "renderableObjectShapeFilter", "objectTypeFilter17", \
									  "objectTypeFilter18", "selectionListOperator6", "objectTypeFilter19", "selectionListOperator7", \
									  "DefaultShadingGroupsFilter", "objectTypeFilter20", "DefaultBakeSetsFilter", "objectTypeFilter21", \
									  "objectTypeFilter22", "selectionListOperator8", "DefaultMaterialsFilter", "DefaultTexturesFilter", \
									  "DefaultTextures2dFilter", "DefaultTextures3dFilter", "DefaultMaterialsTexturesLightShapesFilter", \
									  "DefaultShaderGlowFilter", "objectTypeFilter23", "DefaultNoShaderGlowFilter", "objectTypeFilter24", \
									  "DefaultLightShapesTexturesFilter", "DefaultLightLinkingLightFilter", "DefaultAllLightsFilter2", \
									  "lightSetFilter1", "selectionListOperator9", "DefaultLightShapesPostProcsFilter", "DefaultLightShapesFilter", \
									  "DefaultLightShapesPostProcsFilter1", "DefaultShaderGlowFilter1", "objectTypeFilter25", \
									  "selectionListOperator10", "DefaultImagePlanesFilter", "objectTypeFilter26", \
									  "DefaultCameraShapesImagePlanesFilter", "DefaultCameraShapesFilter1", "objectTypeFilter27", \
									  "DefaultImagePlanesFilter1", "objectTypeFilter28", "selectionListOperator11", "DefaultPostProcFilter", \
									  "DefaultTexturesSGFilter", "DefaultTexturesFilter1", "DefaultShadingGroupsFilter1", "objectTypeFilter29", \
									  "selectionListOperator12", "DefaultSGLightShapesTexturesFilter", "DefaultLightShapesTexturesFilter1", \
									  "DefaultShadingGroupsFilter2", "objectTypeFilter30", "selectionListOperator13", \
									  "DefaultSGLightShapesFilter", "DefaultLightShapesPostProcsFilter2", "DefaultShadingGroupsFilter3", \
									  "objectTypeFilter31", "selectionListOperator14", "DefaultBasicRenderNodesFilter", \
									  "DefaultMaterialsTexturesLightShapesFilter1", "DefaultShadingGroupsFilter4", "objectTypeFilter32", \
									  "selectionListOperator15", "DefaultShadingGroupsAndMaterialsFilter", "DefaultMaterialsFilter1", \
									  "DefaultShadingGroupsFilter5", "objectTypeFilter33", "selectionListOperator16", "DefaultMaterialsAndShaderGlowFilter", \
									  "DefaultMaterialsFilter2", "DefaultShaderGlowFilter2", "objectTypeFilter34", "selectionListOperator17", \
									  "DefaultRenderUtilitiesFilter", "DefaultUsesImageFileFilter", "objectScriptFilter9", \
									  "DefaultAllRenderClassificationsFilter", "DefaultAllRenderNodesFilter", "DefaultAllRenderClassificationsFilter1", \
									  "DefaultShadingGroupsFilter6", "objectTypeFilter38", "selectionListOperator21", "DefaultHiddenAttributesFilter", \
									  "objectAttrFilter1", "animCurveFilter", "objectAttrFilter2", "expressionFilter", "objectAttrFilter3", \
									  "keyableFilter", "objectAttrFilter4", "scaleFilter", "objectNameFilter1", "rotateFilter", "objectNameFilter2", \
									  "translateFilter", "objectNameFilter3", "scaleRotateTranslateFilter", "objectAttrFilter5", "objectTypeFilter43", \
									  "selectionListOperator26", "renderableObjectsAndSetsFilter", "renderableObjectShapeFilter1", \
									  "selectionListOperator27", "objectTypeFilter44", "selectionListOperator28", "objectTypeFilter45", \
									  "selectionListOperator29", "objectTypeFilter46", "objectTypeFilter47", "renderableObjectSetFilter1", \
									  "selectionListOperator30", "lightLinkingObjectFilter", "renderableObjectsAndSetsFilter1", "selectionListOperator31", \
									  "renderableObjectShapeFilter2", "selectionListOperator32", "objectTypeFilter48", "selectionListOperator33", \
									  "objectTypeFilter49", "selectionListOperator34", "objectTypeFilter50", "objectTypeFilter51", \
									  "renderableObjectSetFilter2", "DefaultShadingGroupsFilter7", "objectTypeFilter52", "selectionListOperator35", \
									  "dynController1", "polyMergeEdgeToolDefaults", "polyMergeFaceToolDefaults", "persp", "perspShape", "top", \
									  "topShape", "front", "frontShape", "side", "sideShape", "lightLinker1", "brush1", "strokeGlobals", "layersFilter", \
									  "objectTypeFilter53", "renderLayersFilter", "objectTypeFilter54", "renderingSetsFilter", "objectTypeFilter55", \
									  "relationshipPanel1LeftAttrFilter", "relationshipPanel1RightAttrFilter", "layerManager", "defaultLayer", \
									  "renderLayerManager", "defaultRenderLayer", "globalRender", "DefaultMaterialsAndShaderGlowFilter1", "DefaultTexturesFilter2", \
									  "DefaultTexturesFilter3", "DefaultRenderUtilitiesFilter1", "DefaultLightsAndOpticalFXFilter1", \
									  "DefaultCameraShapesImagePlanesFilter1", "DefaultBakeSetsFilter1"};

// #define NEVER_DELETE_NODES_OF_TYPE_NO 15
// #define NEVER_DELETE_NODES_OF_TYPE {"transform","MpyVolumeShape","MpyEntityShape","MpyNetworkLinkShape","MpyNetworkNodeShape","MpyNetworkShape","hyperGraphInfo","locator","camera","nurbsCurve","time","MpyZoneGridShape","MpyHotSpotShape", "mentalrayItemsList", "materialInfo"};
#define NEVER_DELETE_NODES_OF_TYPE_NO 10
#define NEVER_DELETE_NODES_OF_TYPE {"mentalrayItemsList", "mentalrayGlobals", "blindDataTemplate", "objectRenderFilter", "objectTypeFilter", \
									"reference", "objectAttrFilter", "objectNameFilter", "objectMultiFilter", "objectScriptFilter" };

#define DELETE_WITH_ONE_CONNECTION_NO 6
#define DELETE_WITH_ONE_CONNECTION {"groupId","snapshot","unitConversion","timeToUnitConversion","unitToTimeConversion","displayLayer"};

#define DELETE_WITH_TWO_CONNECTION_NO 0
#define DELETE_WITH_TWO_CONNECTION {""};

unsigned int guiAmIAnImportantNodeFlag = -1;
unsigned int guiKnownImportanceNodeFlag = -1;
const char* gppcNeverDeleteNodesCalled[] = NEVER_DELETE_NODES_CALLED;
const char* gppcNeverImportantNodesCalled[] = NEVER_IMPORTANT_NODES_CALLED;
const char* gppcNeverDeleteNodesOfType[] = NEVER_DELETE_NODES_OF_TYPE;

bool gbOutputReport = false;
MString gobStrReportPathAndFilename = "";
ofstream foutxml;

enum IMPORTANCE
{
	IMPORTANT,
	NOT_IMPORTANT,
	UNKNOWN,
};

// Use helper macro to register a command with Maya.  It creates and
// registers a command that does not support undo or redo.  The 
// created class derives off of MPxCommand.
//
DeclareSimpleCommand( rageCleanUpScene, "Kevin's Plugin Factory", "3.0");

bool IsStringInStringArray(const char* pcString, const char** ppcStringArray, const int iNoOfStrings)
{
//	if(MString(pcString) == "mentalrayItemsList")		cout<<"IsStringInStringArray(\""<<pcString<<"\", const char** ppcStringArray, const int iNoOfStrings)"<<endl;
	for(int iNo=0;iNo<iNoOfStrings;iNo++)
	{
//		if(MString(pcString) == "mentalrayItemsList")		cout<<"Comparing \""<<pcString<<"\" and \""<<ppcStringArray[iNo]<<"\""<<endl;
		if(!strcmp(pcString,ppcStringArray[iNo])) return true;
	}
//	if(MString(pcString) == "mentalrayItemsList")		cout<<"return false;"<<endl;
	return false;
}

MString GetUniqueNodeName(MObject obNodeObject)
{
	// Is it a dag node?
	MStatus obStatus;
	MFnDagNode obDagNode(obNodeObject, &obStatus);
	if(obStatus == MS::kSuccess)
	{
		return obDagNode.fullPathName();
	}
	return MFnDependencyNode(obNodeObject).name().asChar();
}

IMPORTANCE GetImportance(MObject& obNodeObject)
{
	MFnDependencyNode obDepNode(obNodeObject);
	if(obDepNode.isFlagSet(guiKnownImportanceNodeFlag))
	{
		if(obDepNode.isFlagSet(guiAmIAnImportantNodeFlag))
		{
			return IMPORTANT;
		}
		else
		{
			return NOT_IMPORTANT;
		}
	}
	else
	{
		return UNKNOWN;
	}
}

void SetImportance(MObject& obNodeObject, IMPORTANCE eImportance)
{
	// No point repeating myself
	if(GetImportance(obNodeObject) == eImportance) return;
	MFnDependencyNode obDepNode(obNodeObject);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
	if(gbOutputReport)
	{
		foutxml<<"<SetImportance>"<<endl;
		foutxml<<"<Node>"<<GetUniqueNodeName(obNodeObject).asChar()<<"</Node>"<<endl;
		foutxml<<"<Importance>"<<flush;
		switch(eImportance)
		{
			case IMPORTANT:		foutxml<<"IMPORTANT"<<flush; break;
			case NOT_IMPORTANT:	foutxml<<"NOT_IMPORTANT"<<flush; break;
			case UNKNOWN:		foutxml<<"UNKNOWN"<<flush; break;
		}
		foutxml<<"</Importance>"<<endl;
	}
#endif
	if(eImportance == UNKNOWN)
	{
		obDepNode.setFlag(guiKnownImportanceNodeFlag, false);
	}
	else
	{
//		obDepNode.setFlag(guiKnownImportanceNodeFlag, true);

		if(eImportance == IMPORTANT)
		{
//			obDepNode.setFlag(guiAmIAnImportantNodeFlag, true);

			// If I am important, than so is everything I am connected to.  Sounds likely doesn't it?
			// Unfortunately are not that simple, so if I come across a NOT_IMPORTANT node, go no futher with 
			// this optimisation.
//			cout<<"foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance("<<obDepNode.name().asChar()<<", IMPORTANT)"<<endl;
			// Compile a list of nodes I am connected to and they are connected to and so forth
			vector<MObject> obANodeObjectsToMakeImportant;
			vector <MObject>::iterator Iter;
			obANodeObjectsToMakeImportant.push_back(obNodeObject);
			for(unsigned i=0; i<obANodeObjectsToMakeImportant.size(); i++)
			{
				// Get next object in the array
				MObject obCurrentNodeToMakeImportant = obANodeObjectsToMakeImportant[i];
				MFnDependencyNode obFnCurrentNodeToMakeImportant(obCurrentNodeToMakeImportant);

				// Remove from the array
				obANodeObjectsToMakeImportant.erase(obANodeObjectsToMakeImportant.begin() + i);
				i--;

				// If it worth considering further?
				if(GetImportance(obCurrentNodeToMakeImportant) == UNKNOWN)
				{
					// Make important
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
					if(gbOutputReport)
					{
						foutxml<<"<MakingImportant>"<<GetUniqueNodeName(obCurrentNodeToMakeImportant).asChar()<<"</MakingImportant>"<<endl;
					}
#endif
					obFnCurrentNodeToMakeImportant.setFlag(guiKnownImportanceNodeFlag, true);
					obFnCurrentNodeToMakeImportant.setFlag(guiAmIAnImportantNodeFlag, true);

					// Get connected nodes
					MPlugArray obAObMyPlugsWithConnections;
					obFnCurrentNodeToMakeImportant.getConnections(obAObMyPlugsWithConnections);
					for(unsigned int i=0; i<obAObMyPlugsWithConnections.length(); i++)
					{
						// Get the plugs on the other ends of this connection
						MPlugArray obAObPlugsOnTheOtherSideOfConnections;
						obAObMyPlugsWithConnections[i].connectedTo(obAObPlugsOnTheOtherSideOfConnections, true, true);

						// Go through those 
						for(unsigned int j=0; j<obAObPlugsOnTheOtherSideOfConnections.length(); j++)
						{
							MObject obNodeIAmConnectedTo = obAObPlugsOnTheOtherSideOfConnections[j].node();

							if(MFnDependencyNode(obNodeIAmConnectedTo).typeName() != "displayLayer")
							{
								// Is it of UNKNOWN importance?
								if(GetImportance(obNodeIAmConnectedTo) == UNKNOWN)
								{
									// It is worth considering
									obANodeObjectsToMakeImportant.push_back(obNodeIAmConnectedTo);
								}
							}
						}
					}
				}
			}
			// If I am important, AND I am a dag node, then all my parents must also be important, or I'll die 
			// with them
			// So, am I a dag node?
            MDagPathArray obADagPathsToImportantNode;
			if(MDagPath::getAllPathsTo(obNodeObject, obADagPathsToImportantNode) == MS::kSuccess)
			{
				// I am a dag node, and obADagPathsToImportantNode now contains all the paths to me
				// Make all my parents important
				for(unsigned d=0; d<obADagPathsToImportantNode.length(); d++)
				{
					obADagPathsToImportantNode[d].pop();
//					foutxml<<obADagPathsToImportantNode[d].fullPathName().asChar()<<".length() = "<<obADagPathsToImportantNode[d].length()<<endl;
					if(obADagPathsToImportantNode[d].length()>0)
					{
						MFnDependencyNode obParentDependecyNode(obADagPathsToImportantNode[d].node());
						obParentDependecyNode.setFlag(guiKnownImportanceNodeFlag, true);
						obParentDependecyNode.setFlag(guiAmIAnImportantNodeFlag, true);
					}
				}
			}
		}
		else
		{
			if(GetImportance(obNodeObject) == IMPORTANT)
			{
				foutxml<<"Badness?  Trying to make IMPORTANT node "<<GetUniqueNodeName(obNodeObject).asChar()<<" NOT_IMPORTANT"<<endl;
			}
			obDepNode.setFlag(guiKnownImportanceNodeFlag, true);
			obDepNode.setFlag(guiAmIAnImportantNodeFlag, false);
		}
	}
	foutxml<<"</SetImportance>"<<endl;
}

IMPORTANCE AmIAnImportantNodeOrConnectedToAnImportantNode(MObject& obNodeObject)
{
	MStatus status;
	MFnDependencyNode obDepNode(obNodeObject, &status);
	if(status != MS::kSuccess )
	{
		//Couldn't create a dependency node from this MObject, so just mark it as important
		//so we don't try and delete it
		return IMPORTANT;
	}

#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
	if(gbOutputReport)
	{
		foutxml<<"<AmIAnImportantNodeOrConnectedToAnImportantNode>"<<endl;
		foutxml<<"<Node>"<<GetUniqueNodeName(obNodeObject).asChar()<<"</Node>"<<endl;	
	}
#endif

		// Do I already know my importance?
	if(GetImportance(obNodeObject) != UNKNOWN)
	{
		// I already know how important I am, so can just return that
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		if(gbOutputReport)
		{
			/* ofstream foutxml;  foutxml.open((gobStrReportPathAndFilename +".xml").asChar(), ios::app); */	
			{
				if(GetImportance(obNodeObject) == IMPORTANT)
				{
					foutxml<<"<Conclusion>Important because already important</Conclusion>"<<endl;
				}
				else
				{
					foutxml<<"<Conclusion>Not important because already not important</Conclusion>"<<endl;
				}
				foutxml<<"</AmIAnImportantNodeOrConnectedToAnImportantNode>"<<endl;	
			}
		}
#endif
		return GetImportance(obNodeObject);
	}
	int iNoOfConnectedNodesWithAnUnknownState = 0;
	int iNoOfConnectedNodesWithANotImportantState = 0;

	// Message attr
	MPlug messagePlug = obDepNode.findPlug("message", &status);
	if((status != MS::kSuccess) || messagePlug.isNull() )
	{
		//Failed to get the message plug.. so mark the node as important otherwise it could 
		//end up getting deleted
		return IMPORTANT;
	}

	//Walk downstream from the message plug looking for important nodes...
	for( MItDependencyGraph obItDG(messagePlug, MFn::kInvalid, MItDependencyGraph::kDownstream ) ; !obItDG.isDone(); obItDG.next())
	{
		MObject itDgMObj = obItDG.thisNode();
		if( (status != MS::kSuccess) || itDgMObj.isNull() )
		{
			continue;
		}

		IMPORTANCE eImportanceOfConnectedNode = GetImportance(obItDG.thisNode());
		if(eImportanceOfConnectedNode == IMPORTANT)
		{
			// Make important!
			// HAMSTERS obAObIgnoreNodesInHereBecauseIAmCurrentlyWorkingMyselfOut.remove(// HAMSTERS obAObIgnoreNodesInHereBecauseIAmCurrentlyWorkingMyselfOut.length()-1);
			foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, IMPORTANT);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
			if(gbOutputReport)
			{
				foutxml<<"<Conclusion>Important because connected to important node "<<GetUniqueNodeName(obItDG.thisNode()).asChar()<<"</Conclusion>"<<endl;
				foutxml<<"</AmIAnImportantNodeOrConnectedToAnImportantNode>"<<endl;	
			}
#endif
			return IMPORTANT;
		}
		else if(eImportanceOfConnectedNode == UNKNOWN)
		{
			// UNKNOWN, so connected node must be either connected to myself or 
			// one of the nodes I am currently working out
			iNoOfConnectedNodesWithAnUnknownState++;
		}
		else
		{
			// NOT_IMPORTANT, can not tell anything from this
			iNoOfConnectedNodesWithANotImportantState++;
		}
	}

	//Walk upstream from the message plug looking for important nodes...
	for(MItDependencyGraph obItDG(messagePlug, MFn::kInvalid, MItDependencyGraph::kUpstream ); !obItDG.isDone(); obItDG.next())
	{
		IMPORTANCE eImportanceOfConnectedNode = GetImportance(obItDG.thisNode());
		if(eImportanceOfConnectedNode == IMPORTANT)
		{
			// Make important!
			// HAMSTERS obAObIgnoreNodesInHereBecauseIAmCurrentlyWorkingMyselfOut.remove(// HAMSTERS obAObIgnoreNodesInHereBecauseIAmCurrentlyWorkingMyselfOut.length()-1);
			foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, IMPORTANT);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
			if(gbOutputReport)
			{
				foutxml<<"<Conclusion>Important because connected to important node "<<GetUniqueNodeName(obItDG.thisNode()).asChar()<<"</Conclusion>"<<endl;
				foutxml<<"</AmIAnImportantNodeOrConnectedToAnImportantNode>"<<endl;	
			}
#endif
			return IMPORTANT;
		}
		else if(eImportanceOfConnectedNode == UNKNOWN)
		{
			// UNKNOWN, so connected node must be either connected to myself or 
			// one of the nodes I am currently working out
			iNoOfConnectedNodesWithAnUnknownState++;
		}
		else
		{
			// NOT_IMPORTANT, can not tell anything from this
			iNoOfConnectedNodesWithANotImportantState++;
		}
	}

//	cout<<"About to go down on "<<obDepNode.name().asChar()<<endl;
	//Walk downstream from the object looking for important nodes...
	for(MItDependencyGraph obItDG(obNodeObject, MFn::kInvalid, MItDependencyGraph::kDownstream ); !obItDG.isDone(); obItDG.next())
	{
		IMPORTANCE eImportanceOfConnectedNode = GetImportance(obItDG.thisNode());
		if(eImportanceOfConnectedNode == IMPORTANT)
		{
			// Make important!
			// HAMSTERS obAObIgnoreNodesInHereBecauseIAmCurrentlyWorkingMyselfOut.remove(// HAMSTERS obAObIgnoreNodesInHereBecauseIAmCurrentlyWorkingMyselfOut.length()-1);
			foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, IMPORTANT);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
			if(gbOutputReport)
			{
				foutxml<<"<Conclusion>Important because connected to important node "<<GetUniqueNodeName(obItDG.thisNode()).asChar()<<"</Conclusion>"<<endl;
				foutxml<<"</AmIAnImportantNodeOrConnectedToAnImportantNode>"<<endl;	
			}
#endif
			return IMPORTANT;
		}
		else if(eImportanceOfConnectedNode == UNKNOWN)
		{
			// UNKNOWN, so connected node must be either connected to myself or 
			// one of the nodes I am currently working out
			iNoOfConnectedNodesWithAnUnknownState++;
		}
		else
		{
			// NOT_IMPORTANT, can not tell anything from this
			iNoOfConnectedNodesWithANotImportantState++;
		}
	}
//	cout<<"Gone down on "<<obDepNode.name().asChar()<<endl;
//	cout<<"About to go up on "<<obDepNode.name().asChar()<<endl;
	//Walk upstream from the object looking for imporant nodes...
	for(MItDependencyGraph obItDG(obNodeObject, MFn::kInvalid, MItDependencyGraph::kUpstream ); !obItDG.isDone(); obItDG.next())
	{
		IMPORTANCE eImportanceOfConnectedNode = GetImportance(obItDG.thisNode());
		if(eImportanceOfConnectedNode == IMPORTANT)
		{
			// Make important!
			// HAMSTERS obAObIgnoreNodesInHereBecauseIAmCurrentlyWorkingMyselfOut.remove(// HAMSTERS obAObIgnoreNodesInHereBecauseIAmCurrentlyWorkingMyselfOut.length()-1);
			foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, IMPORTANT);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
			if(gbOutputReport)
			{
				foutxml<<"<Conclusion>Important because connected to important node "<<GetUniqueNodeName(obItDG.thisNode()).asChar()<<"</Conclusion>"<<endl;
				foutxml<<"</AmIAnImportantNodeOrConnectedToAnImportantNode>"<<endl;	
			}
#endif
			return IMPORTANT;
		}
		else if(eImportanceOfConnectedNode == UNKNOWN)
		{
			// UNKNOWN, so connected node must be either connected to myself or 
			// one of the nodes I am currently working out
			iNoOfConnectedNodesWithAnUnknownState++;
		}
		else
		{
			// NOT_IMPORTANT, can not tell anything from this
			iNoOfConnectedNodesWithANotImportantState++;
		}
	}
//	cout<<"Gone up on "<<obDepNode.name().asChar()<<endl;


	// If got here then all nodes I am connected to are either unknown or not important, if not important then
	// I am also not important
	if(iNoOfConnectedNodesWithAnUnknownState == 0)
	{
		// Only connected to zero or more not important things
		foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, NOT_IMPORTANT);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		if(gbOutputReport)
		{
			foutxml<<"<Conclusion>Not important because only connected to zero or more not important things</Conclusion>"<<endl;
			foutxml<<"</AmIAnImportantNodeOrConnectedToAnImportantNode>"<<endl;	
		}
#endif
	}
	else if(iNoOfConnectedNodesWithANotImportantState == 0)
	{
		// Only connected to one or more unknown things
		// therefore I must be unknown
		foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, UNKNOWN);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		if(gbOutputReport)
		{
			foutxml<<"<Conclusion>Unknown because only connected to zero or more unknown things</Conclusion>"<<endl;
			foutxml<<"</AmIAnImportantNodeOrConnectedToAnImportantNode>"<<endl;	
		}
#endif
	}
	else
	{
		// Connected to a mixture of unknown and not important things
		// therefore I must be unknown
		foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, UNKNOWN);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		if(gbOutputReport)
		{
			foutxml<<"<Conclusion>Unknown because connected to a mixture of unknown and not important things</Conclusion>"<<endl;
			foutxml<<"</AmIAnImportantNodeOrConnectedToAnImportantNode>"<<endl;	
		}
#endif
	}
	return GetImportance(obNodeObject);
}

IMPORTANCE AmIAnImportantNode(MObject& obNodeObject)
{
	MFnDependencyNode obDepNode(obNodeObject);

	// Is it of already known importance?
	if(GetImportance(obNodeObject) != UNKNOWN)
	{
		// Already important!
		return GetImportance(obNodeObject);
	}

	// Is it referenced?  If so make important because can't delete it even if I wanted to
	if(obDepNode.isFromReferencedFile())
	{
		// Make important!
		foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, IMPORTANT);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		if(gbOutputReport)
		{
			ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::app);	
			fout<<GetUniqueNodeName(obNodeObject).asChar()<<" is important because it is referenced"<<endl;	fout.close();
		}
#endif
		return IMPORTANT;
	}

	// Is it Locked?  If so make important because can't delete it even if I wanted to
/*	if(obDepNode.isLocked())
	{
		// Make important!
		obDepNode.setHAMSTER(guiAmIAnImportantNodeHAMSTER, true);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<obDepNode.name().asChar()<<" is important because it is locked"<<endl;	fout.close();
#endif
		return true;
	}
*/
/*	// Is it DefaultNode?  If so make important because can't delete it even if I wanted to
	if(obDepNode.isDefaultNode())
	{
		// Make important!
		obDepNode.setHAMSTER(guiAmIAnImportantNodeHAMSTER, true);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<obDepNode.name().asChar()<<" is important because it is a default node"<<endl;	fout.close();
#endif
		return true;
	}
*/
	// Is it one of the special nodes?
	if(IsStringInStringArray(obDepNode.name().asChar(), gppcNeverImportantNodesCalled, NEVER_IMPORTANT_NODES_CALLED_NO))
	{
		// Make not important!
		foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, NOT_IMPORTANT);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		if(gbOutputReport)
		{
			ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::app);	
			fout<<GetUniqueNodeName(obNodeObject).asChar()<<" is not important because it is a special node"<<endl;	fout.close();
		}
#endif
		return NOT_IMPORTANT;
	}

	// Is it one of the special types?
	if(IsStringInStringArray(obDepNode.typeName().asChar(), gppcNeverDeleteNodesOfType, NEVER_DELETE_NODES_OF_TYPE_NO))
	{
		// Make important!
		foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, IMPORTANT);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		if(gbOutputReport)
		{
			ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::app);	
			fout<<GetUniqueNodeName(obNodeObject).asChar()<<" is important because it is a special type"<<endl;	fout.close();
		}
#endif
		return IMPORTANT;
	}

	// Is it a Dag node?  They get special treatment
	MStatus obStatus;
	MFnDagNode obDagNode(obNodeObject, &obStatus);
	if(obStatus)
	{
		// It is a dag node

		// Is it a transform without any children, or only other transforms as decendents
		if(obDagNode.typeName() == "transform")
		{
			// It is a transform, what children does it have
			MItDag obItDag;
			// I could make obItDag straight from the MObject, only that doesn't work!  Nice one Alias
			MDagPath obDagPath; MDagPath::getAPathTo(obNodeObject, obDagPath);
			for(obItDag.reset(obDagPath, MItDag::kDepthFirst, MFn::kInvalid); !obItDag.isDone(); obItDag.next())
			{
				// Is it already important?
				if(GetImportance(obItDag.item()) == IMPORTANT)
				{
					// Important children, so important
					// Make important!
					foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, IMPORTANT);
		#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
					if(gbOutputReport)
					{
						ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::app);	
						fout<<obDagNode.fullPathName().asChar()<<" is important because it has an important child "<<MFnDagNode(obItDag.item()).fullPathName().asChar()<<endl;	fout.close();
					}
		#endif
					return IMPORTANT;
				}
				else if((MFnDependencyNode(obItDag.item()).typeName() != "transform") && (!MFnDagNode(obItDag.item()).isIntermediateObject()))
				{
					// None transform child, so important
					// Make important!
					foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, IMPORTANT);
		#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
					if(gbOutputReport)
					{
						ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::app);	
						fout<<obDagNode.fullPathName().asChar()<<" is important because it has nontransform child "<<MFnDagNode(obItDag.item()).fullPathName().asChar()<<endl;	fout.close();
					}
		#endif
					return IMPORTANT;
				}
			}

			// Ok, if I am here then I am a transform whos only children are unimportant transforms
			foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, UNKNOWN);
#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
			if(gbOutputReport)
			{
				ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::app);	
				fout<<obDagNode.fullPathName().asChar()<<" is not important because it does not have any important children"<<endl;	fout.close();
			}
#endif
			return UNKNOWN; 
		}
		else
		{
			// Am I a intermediate node?
			// All non-intermediate nodes are important
			// It is a Dag node, so see if it is intermediate
			if(!obDagNode.isIntermediateObject())
			{
				// Make important!
				foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNodeObject, IMPORTANT);
	#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
				if(gbOutputReport)
				{
					ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::app);	
					fout<<obDagNode.fullPathName().asChar()<<" is important because it is not an intermediate dag node"<<endl;	fout.close();
				}
	#endif
				return IMPORTANT;
			}
		}
	}

	return NOT_IMPORTANT; 
}

MStatus rageCleanUpScene::doIt( const MArgList& args )
//
//	Description:
//		implements the MEL rageCleanUpScene command.
//
//	Arguments:
//		args - the argument list that was passes to the command from MEL
//
//	Return Value:
//		MS::kSuccess - command succeeded
//		MS::kFailure - command failed (returning this value will cause the 
//                     MEL script that is being run to terminate unless the
//                     error is caught using a "catch" statement.
//
{
	//for(int i=0; i<NEVER_DELETE_NODES_CALLED_NO; i++)	cout<<"gppcNeverDeleteNodesCalled["<<i<<"] = "<<gppcNeverDeleteNodesCalled[i]<<endl;
	//for(int i=0; i<NEVER_IMPORTANT_NODES_CALLED_NO; i++)	cout<<"gppcNeverImportantNodesCalled["<<i<<"] = "<<gppcNeverImportantNodesCalled[i]<<endl;
	//for(int i=0; i<NEVER_DELETE_NODES_OF_TYPE_NO; i++)	cout<<"gppcNeverDeleteNodesOfType["<<i<<"] = "<<gppcNeverDeleteNodesOfType[i]<<endl;
#ifdef GENERATE_LIST_OF_NODES_IN_SCENE
	// Get list of nodes in the scene
	int iNoOfNodes = 0;
	for(MItDependencyNodes itDGIter;!itDGIter.isDone();itDGIter.next())
	{
//		fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 1.0 "<<MFnDependencyNode(itDGIter.item()).name().asChar()<<endl;	fout.close();
		// Default node to not important
		iNoOfNodes++;
		cout<<"\""<<GetUniqueNodeName(itDGIter.item()).asChar()<<"\", "<<flush;
	}
	cout<<endl<<iNoOfNodes<<endl;
	return MS::kSuccess;
#endif

	MStatus stat = MS::kSuccess;
	for(unsigned iArgNo = 0; iArgNo < args.length(); iArgNo++)
	{
		if(args.asString(iArgNo) == "-report")
		{
			gobStrReportPathAndFilename = args.asString(iArgNo + 1);
			gbOutputReport = true;
			iArgNo++;
		}
	}

	if(gbOutputReport)
	{
		ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::out);
		char acTimeBuffer[255];
		_snprintf_s(acTimeBuffer, 255, "%.2d/%.2d/%d  %.2d:%.2d:%.2d", COleDateTime::GetCurrentTime().GetDay(), COleDateTime::GetCurrentTime().GetMonth(), COleDateTime::GetCurrentTime().GetYear(), COleDateTime::GetCurrentTime().GetHour(), COleDateTime::GetCurrentTime().GetMinute(), COleDateTime::GetCurrentTime().GetSecond() );
		fout<<acTimeBuffer<<endl;	fout.close();

#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		foutxml.open((gobStrReportPathAndFilename +".xml").asChar(), ios::out);	
		foutxml<<"<rageCleanUpScene>"<<endl;	
		foutxml<<"<CreationTime>"<<acTimeBuffer<<"</CreationTime>"<<endl;	
		/* foutxml.close(); */
#endif
	}

//	ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::out);	fout<<"----------------------------------------------------"<<endl;	fout.close();

	// HAMSTER nodes as important
	// If guiAmIAnImportantNodeHAMSTER == -1, then need to allocate flag
	if(guiAmIAnImportantNodeFlag == -1)
	{
		guiAmIAnImportantNodeFlag = MFnDependencyNode::allocateFlag("rageCleanUpScene", &stat);
		if(stat != MS::kSuccess)
		{
			cout<<"Error allocating flag \"rageCleanUpScene\""<<endl;
			return stat;
		}
		guiKnownImportanceNodeFlag  = MFnDependencyNode::allocateFlag("rageCleanUpScene", &stat);
		if(stat != MS::kSuccess)
		{
			cout<<"Error allocating flag \"rageCleanUpScene\""<<endl;
			return stat;
		}
	}
//	fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"1.0 ----------------------------------------------------"<<endl;	fout.close();

	// Work number of nodes in scene
	int iNoOfNodesAtStart = 0;
	for(MItDependencyNodes itDGIter;!itDGIter.isDone();itDGIter.next()) iNoOfNodesAtStart++;

	// The princple of new clean up scene is harsh, but hopefully good.
	// Basically delete any node that is not somehow effecting an "important" node.
	// "Important" is defined as a node that has some visual effect
	// Go through scene with a MItDependencyNodes
	int iNoOfNodesAtPass1 = 0;
	char acBuffer[256];
	MGlobal::executeCommand("progressWindow  -title \"Granny Killer\"  -progress 0 -minValue 0 -maxValue "+ MString(_itoa(iNoOfNodesAtStart, acBuffer, 10)) +" -status \"Stage 1 : Flagging important nodes\"     -isInterruptable false;");
	for(MItDependencyNodes itDGIter;!itDGIter.isDone();itDGIter.next())
	{
		iNoOfNodesAtPass1++;
		// Handle UI
		MGlobal::executeCommand("progressWindow  -e -progress "+ MString(_itoa(iNoOfNodesAtPass1, acBuffer, 10)) +";");

		{ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 1.0 "<<GetUniqueNodeName(itDGIter.item()).asChar()<<endl;	fout.close();}

		// Default node to not important
		foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(itDGIter.item(), UNKNOWN);

		// Is it deletable?
		AmIAnImportantNode(itDGIter.item());
	}
	{ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"2.0 ----------------------------------------------------"<<endl;	fout.close();}
/*
	int iNoOfNodesAtPass2 = 0;
	MGlobal::executeCommand("progressWindow  -title \"Granny Killer\"  -progress 0 -minValue 0 -maxValue "+ MString(_itoa(iNoOfNodesAtStart, acBuffer, 10)) +" -status \"Stage 2 : Flagging dag nodes\"     -isInterruptable false;");
	for(MItDag itDAGIter;!itDAGIter.isDone();itDAGIter.next())
	{
		iNoOfNodesAtPass2++;
		// Handle UI
		MGlobal::executeCommand("progressWindow  -e -progress "+ MString(_itoa(iNoOfNodesAtPass2, acBuffer, 10)) +";");

//		fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 2.0 "<<MFnDependencyNode(itDGIter.item()).name().asChar()<<endl;	fout.close();

//		cout<<"Stage 2 "<<MFnDagNode(itDAGIter.item()).fullPathName().asChar()<<endl;

		// Default node to important
		SetImportance(itDAGIter.item(), IMPORTANT);
	}
*/
	{ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"3.0 ----------------------------------------------------"<<endl;	fout.close();}

	int iNoOfNodesAtPass3 = 0;
	MGlobal::executeCommand("progressWindow  -e -progress 0 -minValue 0 -maxValue "+ MString(_itoa(iNoOfNodesAtStart, acBuffer, 10)) +" -status \"Stage 3 : Flagging connections to important nodes\";");
	for(MItDependencyNodes itDGIter;!itDGIter.isDone();itDGIter.next())
	{
		iNoOfNodesAtPass3++;

		// Handle UI
		MGlobal::executeCommand("progressWindow  -e -progress "+ MString(_itoa(iNoOfNodesAtPass3, acBuffer, 10)) +";");

		// Is it deletable?
		{ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 3.0 "<<GetUniqueNodeName(itDGIter.item()).asChar()<<endl;	fout.close();}
//		cout<<MFnDependencyNode(itDGIter.item()).name().asChar()<<endl;
		AmIAnImportantNodeOrConnectedToAnImportantNode(itDGIter.item());
	}
	{ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"4.0 ----------------------------------------------------"<<endl;	fout.close();}

	int iNoOfNotImportantNodes = 0;
	MStringArray obAStrDeletedTypes;
	MIntArray obAIntDeletedTypesCounter;
	// Output list of nodes to delete
//	fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"* Not important nodes *"<<endl;	fout.close();
	int iNoOfNodesAtPass4 = 0;
	MStringArray astrNamesOfNodesToDelete;
	MGlobal::executeCommand("progressWindow  -e -progress 0 -minValue 0 -maxValue "+ MString(_itoa(iNoOfNodesAtStart, acBuffer, 10)) +" -status \"Stage 4 : Collating important nodes\";");
	for(MItDependencyNodes itDGIter;!itDGIter.isDone();itDGIter.next())
	{
		iNoOfNodesAtPass4++;

		// Handle UI
		MGlobal::executeCommand("progressWindow  -e -progress "+ MString(_itoa(iNoOfNodesAtPass4, acBuffer, 10)) +";");

		{ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 4.0 "<<GetUniqueNodeName(itDGIter.item()).asChar()<<endl;	fout.close();}
		// Is it important?
		MObject obNode = itDGIter.item();
		MFnDependencyNode obDepNode(obNode);
		MString obStrName = obDepNode.name();
		MStatus obStatus;
		MFnDagNode obDagNode(obNode, &obStatus);
		if(obStatus)
		{
			obStrName = obDagNode.fullPathName();
		}

		// If any nodes are still unknown importance, we've got a problem
		if(GetImportance(obNode) == UNKNOWN)
		{
//			cout<<obStrName.asChar()<<" is still of unknown importance at stage 4"<<endl;
			foutxml<<__FILE__<<":"<<__LINE__<<endl; SetImportance(obNode, NOT_IMPORTANT);
		}

		// Is it not important?
		if((GetImportance(obNode) == NOT_IMPORTANT) && (!obDepNode.isDefaultNode()) && (!IsStringInStringArray(GetUniqueNodeName(obNode).asChar(), gppcNeverDeleteNodesCalled, NEVER_DELETE_NODES_CALLED_NO)))
		{
			// Not important
			iNoOfNotImportantNodes++;

			// HAMSTER it as important so if there is a problem, it will not be considered next time around
			astrNamesOfNodesToDelete.append(obStrName);

			// Log it
			MString obStrType = obDepNode.typeName();

//			fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 4.5.1 "<<i<<" "<<obStrName<<endl;	fout.close();
			bool bFound = false;
			unsigned uNo=0;
			for(uNo=0;uNo<obAStrDeletedTypes.length();uNo++)
			{
				if(obAStrDeletedTypes[uNo] == obStrType)
				{
					// Found it
					bFound = true;
					break;
				}
			}

//			fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 4.5.2 "<<i<<" "<<obStrName<<endl;	fout.close();
			if(bFound)
			{
//				fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 4.5.4 "<<i<<" "<<obStrName<<endl;	fout.close();
				obAIntDeletedTypesCounter[uNo] += 1;
//				fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 4.5.4 "<<i<<" "<<obStrName<<endl;	fout.close();
			}
			else
			{
//				fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 4.5.5 "<<i<<" "<<obStrName<<endl;	fout.close();
				obAStrDeletedTypes.append(obStrType);
				obAIntDeletedTypesCounter.append(1);
//				fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 4.5.6 "<<i<<" "<<obStrName<<endl;	fout.close();
			}
		}
	}
	{ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"5.0 ----------------------------------------------------"<<endl;	fout.close();}

	MGlobal::executeCommand("progressWindow  -e -progress 0 -minValue 0 -maxValue "+ MString(_itoa(astrNamesOfNodesToDelete.length(), acBuffer, 10)) +" -status \"Stage 5 : Deleting not important nodes\";");
	for(unsigned i=0; i<astrNamesOfNodesToDelete.length(); i++)
	{
		// Handle UI
		MGlobal::executeCommand("progressWindow  -e -progress "+ MString(_itoa(i, acBuffer, 10)) +";");

		{ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 5.0 "<<i<<" "<<astrNamesOfNodesToDelete[i].asChar()<<endl;	fout.close();}
		MStatus obStatus;
		MSelectionList	obGetNodeFromName;
		MObject obNode;
		if(obGetNodeFromName.add( astrNamesOfNodesToDelete[i] ) == MS::kSuccess)
		{
//			fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 3.5.-3 "<<i<<endl;	fout.close();
			if(obGetNodeFromName.getDependNode( 0, obNode ) == MS::kSuccess)
			{
//				fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 3.5.-2 "<<i<<endl;	fout.close();
				MFnDependencyNode obDepNode(obNode, &obStatus);
//				fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 3.5.-1 "<<i<<endl;	fout.close();
				if(obStatus == MS::kSuccess)
				{
					if(gbOutputReport)
					{
						ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::app);	
						fout<<"Deleting "<<astrNamesOfNodesToDelete[i].asChar()<<endl;	fout.close();
					}
//					cout<<"delete "<<astrNamesOfNodesToDelete[i].asChar()<<endl;
#ifndef READ_ONLY
					obDepNode.setLocked(false);
//					cout<<obDepNode.name().asChar()<<endl;
					MGlobal::deleteNode(obNode);
#endif
				}
				else
				{
//					fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 3.5.15 "<<i<<endl;	fout.close();
				}
//				fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 3.5.15 "<<i<<endl;	fout.close();
			}
		}
	}
	MGlobal::executeCommand("progressWindow  -endProgress;");

	{ofstream fout; 	fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"End ----------------------------------------------------"<<endl;	fout.close();}

	int iNoOfImportantNodes = 0;
//	fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"* Important nodes *"<<endl;	fout.close();
	int iNoOfNodesAtPass6 = 0;
	for(MItDependencyNodes itDGIter;!itDGIter.isDone();itDGIter.next())
	{
		iNoOfNodesAtPass6++;
//		fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Pass 6.0 "<<MFnDependencyNode(itDGIter.item()).name().asChar()<<endl;	fout.close();
		// Is it important?
		MFnDependencyNode obDepNode(itDGIter.item());

		// Is it already flagged as important?
		MString obStrName = obDepNode.name();
		MStatus obStatus;
		MFnDagNode obDagNode(itDGIter.item(), &obStatus);
		if(obStatus)
		{
			obStrName = obDagNode.fullPathName();
		}
		if(GetImportance(itDGIter.item()) == IMPORTANT)
		{
			// Important
//			fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<"Keeping "<<obStrName<<endl;	fout.close();
//			ofstream fout; fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<obDepNode.name().asChar()<<endl;	fout.close();
			iNoOfImportantNodes++;
		}
	}
//	fout.open("c:/rageCleanUpScene.log", ios::app);	
//	fout<<"No of important nodes   = "<<iNoOfImportantNodes<<endl;	
//	fout<<"No of not important nodes = "<<iNoOfNotImportantNodes<<endl;	
//	fout.close();

	MString strSummary = "Grannies removed : \\\\n\\\\n";
	int iTotal = 0;
	MGlobal::displayInfo("Deleted : ");
	for(unsigned uNo=0;uNo<obAStrDeletedTypes.length();uNo++)
	{
		MString obStrMsg;
		obStrMsg += obAIntDeletedTypesCounter[uNo];
		obStrMsg += " ";
		obStrMsg += obAStrDeletedTypes[uNo];
		obStrMsg += " nodes";
		iTotal += obAIntDeletedTypesCounter[uNo];

		strSummary += obStrMsg +"\\\\n";

		if(gbOutputReport)
		{
			ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::app);	
			fout<<obStrMsg.asChar()<<endl;	fout.close();
		}
		MGlobal::displayInfo(obStrMsg);
//		fout.open("c:/rageCleanUpScene.log", ios::app);	fout<<obStrMsg<<endl;	fout.close();
	}
	MString obStrMsg = "";
	obStrMsg += iTotal;
	obStrMsg += " total nodes removed";
	strSummary += "--------------------------------------------------------------\\\\n"+ obStrMsg +"\\\\n";
	MGlobal::displayInfo(obStrMsg);
	int iDisplayGrannyKillerSummary;
	MGlobal::executeCommand("optionVar -query oiDisplayGrannyKillerSummary", iDisplayGrannyKillerSummary);
	if(iDisplayGrannyKillerSummary)
	{
		MGlobal::executeCommand("rageInfo(\""+ strSummary +"\")", true);
	}
	if(gbOutputReport)
	{
		ofstream fout; fout.open(gobStrReportPathAndFilename.asChar(), ios::app);	
		fout<<obStrMsg.asChar()<<endl;	fout.close();	

#ifdef OUTPUT_WHAT_HAPPENS_TO_EVERY_NODE
		/* foutxml.open((gobStrReportPathAndFilename +".xml").asChar(), ios::app);*/
		foutxml<<"</rageCleanUpScene>"<<endl;	
		foutxml.close();
#endif
	}

//	fout.open("c:/rageCleanUpScene.log", ios::app);	
//	fout<<"No of nodes during pass1 = "<<iNoOfNodesAtPass1<<endl;	
//	fout<<"No of nodes during pass2 = "<<iNoOfNodesAtPass2<<endl;	
//	fout<<"No of nodes during pass3 = "<<iNoOfNodesAtPass3<<endl;	
//	fout<<"No of nodes during pass4 = "<<iNoOfNodesAtPass4<<endl;	
//	fout.close();


	// If guiAmIAnImportantNodeHAMSTER != -1, then need to deallocate flag
	if(guiAmIAnImportantNodeFlag != -1)
	{
		MFnDependencyNode::deallocateFlag("AmIAnImportantNode", guiAmIAnImportantNodeFlag);
		MFnDependencyNode::deallocateFlag("AmIAnImportantNode", guiKnownImportanceNodeFlag);
	}

	return stat;
}

