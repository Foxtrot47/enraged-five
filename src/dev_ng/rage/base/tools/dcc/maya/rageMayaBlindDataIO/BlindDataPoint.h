#pragma once
#include <maya/MStatus.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnDagNode.h>
#include <maya/MItMeshFaceVertex.h>
#include <maya/MItMeshVertex.h>
#include <maya/MSelectionList.h>
#include <maya/MGlobal.h>
#include <maya/MDagPath.h>
#include <maya/MFnMesh.h>

class BlindDataPoint
{
public:
	BlindDataPoint(const MPoint& obPosition, const MIntArray& aiDataTypes, const MFloatArray& afDataValues);
	~BlindDataPoint(void);

	const MPoint& GetPosition() const {return m_obPosition;}
	const MIntArray& GetDataTypes() const {return m_aiDataTypes;}
	const MFloatArray& GetDataValues() const {return m_afDataValues;}

private:
	MPoint m_obPosition;
	MIntArray m_aiDataTypes;
	MFloatArray m_afDataValues;
};
