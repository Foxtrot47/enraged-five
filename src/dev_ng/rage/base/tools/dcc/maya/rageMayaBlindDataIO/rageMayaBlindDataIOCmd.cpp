//
// Copyright (C) 
// 
// File: rageMayaBlindDataIOCmd.cpp
//
// MEL Command: rageMayaBlindDataIO
//
// Author: Maya Plug-in Wizard 2.0
//

// Includes everything needed to register a simple MEL command with Maya.
// 
#define INPUT_BUFFER_SIZE 512

#include "BlindData.h"
#include "BlindDataPoint.h"

#include <maya/MSimple.h>
#include <maya/MStatus.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnDagNode.h>
#include <maya/MItMeshFaceVertex.h>
#include <maya/MItMeshVertex.h>
#include <maya/MSelectionList.h>
#include <maya/MGlobal.h>
#include <maya/MDagPath.h>
#include <maya/MFnMesh.h>

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

// Use helper macro to register a command with Maya.  It creates and
// registers a command that does not support undo or redo.  The 
// created class derives off of MPxCommand.
//
DeclareSimpleCommand( rageMayaBlindDataIO, "", "7.0");

MStatus rageMayaBlindDataIO::doIt( const MArgList& args )
//
//	Description:
//		implements the MEL rageMayaBlindDataIO command.
//
//	Arguments:
//		args - the argument list that was passes to the command from MEL
//
//	Return Value:
//		MS::kSuccess - command succeeded
//		MS::kFailure - command failed (returning this value will cause the 
//                     MEL script that is being run to terminate unless the
//                     error is caught using a "catch" statement.
//
{
	// printf("Doing it\n");
	MStatus stat = MS::kSuccess;

	MObject obMeshNodeObject;
	MString strMeshName;
	MString strOutputFilename;
	MString strInputFilename;

	for(unsigned a=0; a<args.length(); a++)
	{
		if(args.asString(a) == "-mesh")
		{
			strMeshName = args.asString(++a);
		}
		else if(args.asString(a) == "-output")
		{
			strOutputFilename = args.asString(++a);
		}
		else if(args.asString(a) == "-input")
		{
			strInputFilename = args.asString(++a);
		}
	}

	MSelectionList obSelectionListForStringToApiConversions;
	obSelectionListForStringToApiConversions.add(strMeshName);
	MDagPath obMeshDagPath;
	obSelectionListForStringToApiConversions.getDagPath(0, obMeshDagPath);

	// Got mesh, next write out vert data?
	if(strOutputFilename != "")
	{
		ofstream fout; 
		fout.open(strOutputFilename .asChar(), ios::out);
		if(!fout)
		{
			// Badness happened :(
			MGlobal::displayError("Unable to open "+ strOutputFilename+" for write");
			setResult( "rageMayaBlindDataIO :: Unable to open "+ strOutputFilename+" for write\n" );
			return MS::kFailure;
		}

		// Opened file successfully
		MFnMesh obFnMesh(obMeshDagPath);

		vector<BlindDataType> aobBlindDataTypes;

		// Get blind data types
		MIntArray aiBlindDataIds;
		stat = obFnMesh.getBlindDataTypes ( MFn::kMeshVertComponent, aiBlindDataIds );
		if(!stat)
		{
			MGlobal::displayError("Unable get blind data types used on "+ obFnMesh.fullPathName());
			return MS::kFailure;
		}

		// Number of blinddata types
		fout<<aiBlindDataIds.length()<<endl;

		// Get blind data names
		for(unsigned b = 0; b<aiBlindDataIds.length(); b++)
		{
			MStringArray astrLongNames;
			MStringArray astrShortNames;
			MStringArray astrFormatNames;
			stat = obFnMesh.getBlindDataAttrNames (aiBlindDataIds[b], astrLongNames, astrShortNames, astrFormatNames);
			if(!stat)
			{
				MGlobal::displayError("Unable get blind data names used on "+ obFnMesh.fullPathName());
				return MS::kFailure;
			}
			for(unsigned n = 0; n<astrLongNames.length(); n++)
			{
				fout<<aiBlindDataIds[b]<<","<<astrLongNames[n].asChar()<<","<<astrShortNames[n].asChar()<<","<<astrFormatNames[n].asChar()<<endl;
				aobBlindDataTypes.push_back(BlindDataType(aiBlindDataIds[b], astrLongNames[n], astrShortNames[n], astrFormatNames[n]));
			}
		}

		// For every vertex in the mesh
		fout<<obFnMesh.numVertices()<<endl;
		for(MItMeshVertex vertIt(obMeshDagPath);	!vertIt.isDone(); vertIt.next() )
		{
			// Output the position
			MPoint obPos = vertIt.position();
			fout<<obPos.x<<","<<obPos.y<<","<<obPos.z<<","<<flush;

			for(unsigned b = 0; b<aobBlindDataTypes.size(); b++)
			{
				float fBlindData;
				stat = obFnMesh.getFloatBlindData(vertIt.index(), MFn::kMeshVertComponent, aobBlindDataTypes[b].GetBlindDataId(), aobBlindDataTypes[b].GetLongName(), fBlindData);
				if(!stat)
				{
					MGlobal::displayError("Unable get blind data for vertex");
					return MS::kFailure;
				}
				fout<<aobBlindDataTypes[b].GetBlindDataId()<<","<<fBlindData<<","<<flush;
			}

			fout<<endl;
		}
		fout.close();
	}

	// Got mesh, next read in vert data?
	if(strInputFilename != "")
	{
		ifstream fin; 
		fin.open(strInputFilename .asChar(), ios::in);
		if(!fin)
		{
			// Badness happened :(
			MGlobal::displayError("Unable to open "+ strInputFilename+" for read");
			setResult( "rageMayaBlindDataIO :: Unable to open "+ strInputFilename+" for read\n" );
			return MS::kFailure;
		}

		// Opened file successfully
		MFnMesh obFnMesh(obMeshDagPath);

		vector<BlindDataType> aobBlindDataTypes;

		// Get blind data types
		char acLine[INPUT_BUFFER_SIZE];
		fin.getline(acLine, INPUT_BUFFER_SIZE);
		int iNoOfBlindDataLines = atoi(acLine);
		for(int b=0; b<iNoOfBlindDataLines; b++)
		{
			fin.getline(acLine, INPUT_BUFFER_SIZE);
			MString strLine(acLine);
			MStringArray astrLineParts;
			stat = strLine.split(',', astrLineParts);
			if(!stat)
			{
				MGlobal::displayError("Unable to split input string \""+ strLine +"\" on comma");
				return MS::kFailure;
			}
			if(astrLineParts.length() != 4)
			{
				MGlobal::displayError("Error occurred spliting input string \""+ strLine +"\" on comma");
				return MS::kFailure;
			}
			MString strBlindDataId = astrLineParts[0];
			MString strLongName = astrLineParts[1];
			MString strShortName = astrLineParts[2];
			MString strFormatName = astrLineParts[3];

			aobBlindDataTypes.push_back(BlindDataType(atoi(strBlindDataId.asChar()), strLongName, strShortName, strFormatName));
		}


		// Add all the blind data types to the mesh
		for(unsigned b = 0; b<aobBlindDataTypes.size(); b++)
		{
#if _DEBUG
			cout<<aobBlindDataTypes[b].GetBlindDataId()<<","<<aobBlindDataTypes[b].GetLongName().asChar()<<","<<aobBlindDataTypes[b].GetShortName().asChar()<<","<<aobBlindDataTypes[b].GetFormatName().asChar()<<endl;
#endif
			bool bHasBlindDataType = obFnMesh.hasBlindData( MFn::kMeshVertComponent, aobBlindDataTypes[b].GetBlindDataId(), &stat);
			if(!stat)
			{
				MGlobal::displayError("Unable to tell if mesh "+ obFnMesh.fullPathName() +" already has blind data type "+ aobBlindDataTypes[b].GetBlindDataId());
				return MS::kFailure;
			}
			if(!bHasBlindDataType)
			{
				// Add blind data type
				MStringArray astrLongNames; astrLongNames.append(aobBlindDataTypes[b].GetLongName());
				MStringArray astrShortNames; astrShortNames.append(aobBlindDataTypes[b].GetShortName());
				MStringArray astrFormatNames; astrFormatNames.append(aobBlindDataTypes[b].GetFormatName());
				stat = obFnMesh.createBlindDataType ( aobBlindDataTypes[b].GetBlindDataId(), astrLongNames, astrShortNames, astrFormatNames);
			}
		}

		// All the types have been added, so now load the actual data
		vector<const BlindDataPoint*> aobBlindDataPoints;
		fin.getline(acLine, INPUT_BUFFER_SIZE);
		int iNoOfVertLines = atoi(acLine);
		for(int v=0; v<iNoOfVertLines; v++)
		{
			fin.getline(acLine, INPUT_BUFFER_SIZE);
			MString strLine(acLine);
			MStringArray astrLineParts;
			stat = strLine.split(',', astrLineParts);
			if(!stat)
			{
				MGlobal::displayError("Unable to split input string \""+ strLine +"\" on comma");
				return MS::kFailure;
			}

			// Get pos
			float x = (float)atof(astrLineParts[0].asChar());
			float y = (float)atof(astrLineParts[1].asChar());
			float z = (float)atof(astrLineParts[2].asChar());
			MPoint obPosition(x,y,z);
			cout<<x<<","<<y<<","<<z<<","<<flush;

			// Get data
			MIntArray aiDataTypes;
			MFloatArray afDataValues;
			for(unsigned i=3; i<astrLineParts.length(); i++)
			{
				aiDataTypes.append(atoi(astrLineParts[i].asChar()));
				cout<<atoi(astrLineParts[i].asChar())<<","<<flush;
				afDataValues.append((float)atof(astrLineParts[++i].asChar()));
				cout<<atof(astrLineParts[i].asChar())<<","<<flush;
			}
			cout<<endl;

			aobBlindDataPoints.push_back(new BlindDataPoint(obPosition, aiDataTypes, afDataValues));
		}

		// Got all the data points, so next add the data to the mesh
		for(MItMeshVertex vertIt(obMeshDagPath);	!vertIt.isDone(); vertIt.next() )
		{
			// Find the nearest data point
			MPoint obPos = vertIt.position();

			const BlindDataPoint* pobNearestDataPoint = NULL;
			double fDistanceToNearestPoint = 99999999999999.9f;
			for(unsigned v=0; v<aobBlindDataPoints.size(); v++)
			{
				double dDistance = obPos.distanceTo(aobBlindDataPoints[v]->GetPosition());
				if(dDistance < fDistanceToNearestPoint)
				{
					fDistanceToNearestPoint = dDistance;
					pobNearestDataPoint = aobBlindDataPoints[v];
				}
			}

			// Got nearest data point, so copy data across
			if(pobNearestDataPoint != NULL)
			{
				for(unsigned d=0; d<pobNearestDataPoint->GetDataTypes().length(); d++)
				{
					// Get id
					int iTypeId = pobNearestDataPoint->GetDataTypes()[d];

					// Get name
					MString strLongName;
					for(unsigned b = 0; b<aobBlindDataTypes.size(); b++)
					{
						if(aobBlindDataTypes[b].GetBlindDataId() == iTypeId)
						{
							strLongName = aobBlindDataTypes[b].GetLongName();
							break;
						}
					}

					// Get value
					float fValue = pobNearestDataPoint->GetDataValues()[d];

					// Set data value
					obFnMesh.setFloatBlindData(vertIt.index(), MFn::kMeshVertComponent, iTypeId, strLongName, fValue);
				}
			}
		}
	}


	// Since this class is derived off of MPxCommand, you can use the 
	// inherited methods to return values and set error messages
	//
	setResult( "rageMayaBlindDataIO command executed!\n" );
	_flushall();

	return stat;
}
