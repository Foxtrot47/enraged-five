#include "BlindDataPoint.h"

BlindDataPoint::BlindDataPoint(const MPoint& obPosition, const MIntArray& aiDataTypes, const MFloatArray& afDataValues)
{
	m_obPosition = obPosition;
	m_aiDataTypes = aiDataTypes;
	m_afDataValues = afDataValues;
}

BlindDataPoint::~BlindDataPoint(void)
{
}
