#pragma once
#include <maya/MStatus.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnDagNode.h>
#include <maya/MItMeshFaceVertex.h>
#include <maya/MItMeshVertex.h>
#include <maya/MSelectionList.h>
#include <maya/MGlobal.h>
#include <maya/MDagPath.h>
#include <maya/MFnMesh.h>

class BlindDataType
{
public:
	BlindDataType(int iBlindDataId, const MString& strLongName, const MString& strShortName, const MString& strFormatName);
	~BlindDataType(void);

	int GetBlindDataId() const {return m_iBlindDataId;}
	const MString& GetLongName() const {return m_strLongName;}
	const MString& GetShortName() const {return m_strShortName;}
	const MString& GetFormatName() const {return m_strFormatName;}

private:
	int		m_iBlindDataId;
	MString m_strLongName;
	MString m_strShortName;
	MString m_strFormatName;
};
