#include "BlindData.h"

BlindDataType::BlindDataType(int iBlindDataId, const MString& strLongName, const MString& strShortName, const MString& strFormatName)
{
	m_iBlindDataId = iBlindDataId;
	m_strLongName = strLongName;
	m_strShortName = strShortName;
	m_strFormatName = strFormatName;
}

BlindDataType::~BlindDataType(void)
{
}
