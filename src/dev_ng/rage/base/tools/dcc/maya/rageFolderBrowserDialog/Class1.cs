// The following example displays an application that provides the ability to 
// open rich text files (rtf) into the RichTextBox. The example demonstrates 
// using the FolderBrowserDialog to set the default directory for opening files.
// The OpenFileDialog class is used to open the file.
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using rageUsefulCSharpToolClasses;

public class Class1
{
	// The main entry point for the application.
	[STAThread]
	static void Main() 
	{
		string[] astrArgs = Environment.GetCommandLineArgs();
		string strDescription = "";
		string strDefaultFolder = "";
		for(int i=1; i<astrArgs.GetLength(0); i++)
		{
			if(astrArgs[i] == "-description")
			{
				strDescription = astrArgs[++i];
			}
			else if(astrArgs[i] == "-default")
			{
				strDefaultFolder = astrArgs[++i];
			}
			else
			{
				// Error
                System.Console.WriteLine("Error :  rageFolderBrowser [-description <description>] [-default <default>]");
			}
		}
		System.Console.WriteLine(rageSimpleUIElements.FolderBrowser(strDescription, strDefaultFolder));
	}
}

