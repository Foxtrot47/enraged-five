// 
// rageShaderMaterial/rageShaderMaterialMayaParamString.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnTypedAttribute.h>
#include <maya/MDGModifier.h>
#pragma warning(pop)

#include "file/token.h"
#include "file/stream.h"
#include "rageShaderMaterialMayaParamString.h"

extern bool g_bDisplayMGlobalExecuteCommands;


using namespace rage;


bool rageShaderMaterialMayaParamString::WriteScriptMayaInit(const shaderMaterialGeoParamValueString* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	if (stricmp(pobParam->GetParamDescription()->GetUIWidget(), "menu")==0)
	{
		T.StartLine();
		T.PutStr("int $count = 0;");
		T.EndLine();

		T.StartLine();
		T.PutStr("for ($count=0; $count<255; $count++)");
		T.EndLine();

		T.StartBlock();
		{
			T.StartLine();
			T.PutStr("setAttr (%s + \".%s\") $count;", nodeNameMaya, rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar());
			T.EndLine();

			T.StartLine();
			T.PutStr("string $test = `getAttr -as (%s + \".%s\")`;", nodeNameMaya, rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar());
			T.EndLine();

			T.StartLine();
			T.PutStr("if ($test == \"%s\")", pobParam->GetValue());
			T.EndLine();

			T.StartBlock();
			{
				T.StartLine();
				T.PutStr("break;");
				T.EndLine();
			}
			T.EndBlock();
		}
		T.EndBlock();
	}
	else
	{
		T.StartLine();
		T.PutStr("setAttr -type \"string\" (%s + \".%s\") \"%s\";", nodeNameMaya, rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), pobParam->GetValue());
		T.EndLine();
	}
	
	return true;
}

bool rageShaderMaterialMayaParamString::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueString* pobParam)
{
	char acBuffer[1024];
	if (stricmp(pobParam->GetParamDescription()->GetUIWidget(), "menu")==0)
	{
		char szCommand[1024];
		memset(szCommand, 0, sizeof(szCommand));

		fiStream * input = fiStream::Open(pobParam->GetParamDescription()->GetUIHint());
		if (input)
		{
			fiTokenizer T2(pobParam->GetParamDescription()->GetUIHint(), input);

			char szToken[256];
			T2.GetToken(szToken, sizeof(szToken));
			strcpy(szCommand, szToken);

			while(T2.GetToken(szToken, sizeof(szToken)))
			{
				strcat(szCommand, ":");
				strcat(szCommand, szToken);
			}

			input->Close();
			input = NULL;
		}

		formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %s -is true -h false -s false -at enum -en \"%s\" %s;", rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), szCommand, MFnDependencyNode(obNode).name().asChar());
		MGlobal::executeCommand(acBuffer);

		// Get the number of the default value
		MString strDefaultEnumValue = pobParam->GetValue();
		int iDefaultEnumValue = -1;
		MString strEnumValues = szCommand;
		MStringArray astrEnumValues;
		strEnumValues.split(':', astrEnumValues);

		for(unsigned e=0; e<astrEnumValues.length(); e++)
		{
			if(strDefaultEnumValue == astrEnumValues[e])
			{
				iDefaultEnumValue = e;
				break;
			}
		}
		if(iDefaultEnumValue != -1)
		{
			formatf(acBuffer, sizeof(acBuffer), "setAttr %s.%s %d", MFnDependencyNode(obNode).name().asChar(), rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), iDefaultEnumValue);
			MGlobal::executeCommand(acBuffer);		
		}
	}
	else
	{
		MFnTypedAttribute nAttr;
		MStatus status;
		MObject obAttribute = nAttr.create( rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()) +"s",	MFnData::kString,  MObject::kNullObj, &status );
		CHECK_STATUS( status );
		CHECK_STATUS( nAttr.setStorable( true ) );
		CHECK_STATUS( nAttr.setHidden( true ) );
		CHECK_STATUS( nAttr.setReadable( true ) );
		CHECK_STATUS( nAttr.setWritable( true ) );
		CHECK_STATUS( nAttr.setInternal( true ) );

		MDGModifier obModifier;
		CHECK_STATUS(obModifier.addAttribute(obNode, obAttribute));
		CHECK_STATUS(obModifier.doIt());
	}

	return true;
}

bool rageShaderMaterialMayaParamString::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueString* pobParam)
{
	bool retval = false;
	MFnDependencyNode obFnFileNode(obNode);
	MStatus result;
	MPlug plug = obFnFileNode.findPlug(rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), &result);
	if (result == MStatus::kSuccess)
	{
		MString value;
		plug.getValue(value);
		if (!strcmp(value.asChar(), pobParam->GetValue()))
		{
			retval = true;
		}
	}
	return retval;
}
