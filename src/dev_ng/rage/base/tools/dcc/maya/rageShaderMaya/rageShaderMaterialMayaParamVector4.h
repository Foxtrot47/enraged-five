// 
// rageShaderMaterial/rageShaderMaterialMayaParamVector4.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_MATERIAL_MayaParam_VECTOR4_H
#define RAGE_SHADER_MATERIAL_MayaParam_VECTOR4_H

#include "rageShaderMaterialMayaParam.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector4.h"

namespace rage {

class rageShaderMaterialMayaParamVector4
{
public:
	static bool WriteScriptMayaUi(const shaderMaterialGeoParamValueVector4* pobParam, fiTokenizer & T, const char * nodeNameMaya);
	static bool WriteScriptMayaInit(const shaderMaterialGeoParamValueVector4* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueVector4* pobParam);

	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueVector4* pobParam);
};

} // end namespace rage

#endif
