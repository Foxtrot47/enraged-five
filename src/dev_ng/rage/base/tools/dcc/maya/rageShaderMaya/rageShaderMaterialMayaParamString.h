// 
// rageShaderMaterial/shaderMaterialGeoParamValueString.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_MATERIAL_MayaParam_STRING_H
#define RAGE_SHADER_MATERIAL_MayaParam_STRING_H

#include "rageShaderMaterialMayaParam.h"
#include "shaderMaterial/shaderMaterialGeoParamValueString.h"

namespace rage {

class rageShaderMaterialMayaParamString
{
public:
	static bool WriteScriptMayaInit(const shaderMaterialGeoParamValueString* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueString* pobParam);

	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueString* pobParam);
};

} // end namespace rage

#endif
