// 
// rageShaderMaya/rageShaderTexture.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <maya/MStatus.h>
#include <maya/MGlobal.h>
#pragma warning(pop)

#include "rageShaderTexture.h"

int highestPowerOf2(int num)
{
	int power = 0;

	while (num > 1)
	{
		power++;
		num = num >> 1;
	}

	return power;
}

using namespace rage;

rageShaderTexture::rageShaderTexture()
{
	// Initialize everything
	m_bits = NULL;
}

rageShaderTexture::~rageShaderTexture()
{
	delete [] m_bits;
	m_bits = NULL;
}

bool rageShaderTexture::load(const char * fileName)
{
	MImage image;
	MStatus stat = image.readFromFile(fileName);
	if (!stat)
	{
		if((strcmp(fileName, "none") != 0) && (strcmp(fileName, "null") != 0))
		{
			MGlobal::displayWarning("In rageShaderTexture::load(), file not found: \"" + MString(fileName) + "\".");
		}

		// Create a default image
		MStatus stat = image.create(8, 8);
		if (!stat)
		{
			return false;
		}

		// This tries to match the RAGE standard of purple and green
		static bool s_isDefaultInitialized = false;
		static u8 s_aBits[8 * 8 * 4];
		if (!s_isDefaultInitialized)
		{
			for (int i=0; i<8; i++)
			{
				for (int j=0; j<8; j++)
				{
					int index = ((i * 8) + j);

					if (((index+i) % 2) == 0)
					{
						index *= 4;
						s_aBits[index + 0] = 255;
						s_aBits[index + 1] = 0;
						s_aBits[index + 2] = 255;
						s_aBits[index + 3] = 255;
					}
					else
					{
						index *= 4;
						s_aBits[index + 0] = 0;
						s_aBits[index + 1] = 255;
						s_aBits[index + 2] = 0;
						s_aBits[index + 3] = 255;
					}
				}
			}

			s_isDefaultInitialized = true;
		}
		
		memcpy(image.pixels(), s_aBits, 8 * 8 * 4);
	}

	return set( image );
}

bool rageShaderTexture::load(const u8 * pBits,
							 const int nWidth,
							 const int nHeight)
{
	MImage image;

	MStatus stat = image.create(nWidth, nHeight);
	if (!stat)
	{
		return false;
	}
	memcpy(image.pixels(), pBits, 4 * nWidth * nHeight);

	return set( image );
}

bool rageShaderTexture::set(MImage &image)
{
	
	// Deallocate any existing levels
	if (m_bits != NULL)
	{
		delete m_bits;
		m_bits = NULL;
	}

	m_internalFormat = GL_RGBA8;
	m_format = GL_RGBA;
	m_componentFormat = GL_UNSIGNED_BYTE;
	
	// Get the dimension of the texture.
	MStatus stat = image.getSize(m_width, m_height);

	u32 maxWidthLevels  = highestPowerOf2(m_width);
	u32 maxHeightLevels = highestPowerOf2(m_height);

	// Make sure we are a power of two
	bool widthIsExponent = (m_width == (u32) (1 << maxWidthLevels));
	bool heightIsExponent = (m_height == (u32) (1 << maxHeightLevels));

	if (!widthIsExponent || !heightIsExponent)
	{
		MGlobal::displayWarning("In rageShaderTexture::set(), unsupported texture size:");

		// Load the default texture
		return load("none");
	}

	// Allocate the proper amount of memory
	m_bits = new u8 [m_width * m_height * 4];

	// Copy the base level. (the actual file texture)
	memcpy(m_bits, image.pixels(), m_width * m_height * 4);

	specify();

	return true;
}

bool rageShaderTexture::specify()
{
	m_texObj.bind();

	glTexImage2D(m_texObj.target(), 0, m_internalFormat, m_width, m_height, 0, m_format, m_componentFormat, m_bits);
	
	m_texObj.parameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	m_texObj.parameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	m_texObj.parameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
	m_texObj.parameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

	return true;
}

void rageShaderTexture::bind()
{
	m_texObj.bind();
}
