// 
// rageShaderMaterial/rageShaderMaterialMayaParamVector2.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnNumericAttribute.h>
#include <maya/MDGModifier.h>
#pragma warning(pop)

#include "file/token.h"
#include "file/stream.h"
#include "rageShaderMaterialMayaParamVector2.h"

using namespace rage;

bool rageShaderMaterialMayaParamVector2::WriteScriptMayaUi(const shaderMaterialGeoParamValueVector2* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	static int s_UniqueNumber = 0;
	T.StartLine();
	T.PutStr("floatFieldGrp -label \"%s\" -columnWidth3 145 52 52 -numberOfFields 2 \"v2_%d\";", sParamName.asChar(), s_UniqueNumber);
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 2 \"v2_%d\" (%s + \".%sX\");", s_UniqueNumber, nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 3 \"v2_%d\" (%s + \".%sY\");", s_UniqueNumber, nodeNameMaya, sParamName.asChar());
	T.EndLine();
	s_UniqueNumber++;

	return true;
}

bool rageShaderMaterialMayaParamVector2::WriteScriptMayaInit(const shaderMaterialGeoParamValueVector2* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sX\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().x);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sY\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().y);
	T.EndLine();

	return true;
}

bool rageShaderMaterialMayaParamVector2::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueVector2* pobParam)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	MFnNumericAttribute nAttr;
	MStatus status;
	MFnDependencyNode obFnFileNode(obNode);
	if(!obFnFileNode.hasAttribute(sParamName +"X") || !obFnFileNode.hasAttribute(sParamName +"Y"))
	{
		MDGModifier obModifier;

		// Displayf("%s Min = %f Max = %f", sParamName.asChar(), pobParam->GetParamDescription()->GetUIMin(), pobParam->GetParamDescription()->GetUIMax());
		if(!obFnFileNode.hasAttribute(sParamName +"X"))
		{
			MObject obAttributeX = nAttr.create( sParamName +"X", sParamName +"Xs",	MFnNumericData::kFloat, 0, &status );
			CHECK_STATUS( status );
			CHECK_STATUS( nAttr.setStorable( true ) );
			CHECK_STATUS( nAttr.setHidden( true ) );
			CHECK_STATUS( nAttr.setReadable( true ) );
			CHECK_STATUS( nAttr.setWritable( true ) );
			CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
			CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
			CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().x ) );
			CHECK_STATUS( nAttr.setInternal( true ) );
			CHECK_STATUS(obModifier.addAttribute(obNode, obAttributeX));
		}
		
		if(!obFnFileNode.hasAttribute(sParamName +"Y"))
		{
			MObject obAttributeY = nAttr.create( sParamName +"Y", sParamName +"Ys",	MFnNumericData::kFloat, 0, &status );
			CHECK_STATUS( status );
			CHECK_STATUS( nAttr.setStorable( true ) );
			CHECK_STATUS( nAttr.setHidden( true ) );
			CHECK_STATUS( nAttr.setReadable( true ) );
			CHECK_STATUS( nAttr.setWritable( true ) );
			CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
			CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
			CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().y ) );
			CHECK_STATUS( nAttr.setInternal( true ) );
			CHECK_STATUS(obModifier.addAttribute(obNode, obAttributeY));
		}
		CHECK_STATUS(obModifier.doIt());
	}
	return true;
}

bool rageShaderMaterialMayaParamVector2::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueVector2* pobParam)
{
	MFnDependencyNode obFnFileNode(obNode);
	MStatus result;
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());
	MPlug plug = obFnFileNode.findPlug(sParamName + "Xs", &result);
	bool xval = false;
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == pobParam->GetValue().x)
		{
			xval = true;
		}
	}

	plug = obFnFileNode.findPlug(sParamName + "Ys", &result);
	bool yval = false;
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == pobParam->GetValue().y)
		{
			yval = true;
		}
	}
	return (xval && yval);
}
