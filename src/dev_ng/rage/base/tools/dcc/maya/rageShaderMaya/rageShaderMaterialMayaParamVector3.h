// 
// rageShaderMaterial/rageShaderMaterialMayaParamVector3.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_MATERIAL_MayaParam_VECTOR3_H
#define RAGE_SHADER_MATERIAL_MayaParam_VECTOR3_H

#include "rageShaderMaterialMayaParam.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector3.h"

namespace rage {

class rageShaderMaterialMayaParamVector3
{
public:
	static bool WriteScriptMayaUi(const shaderMaterialGeoParamValueVector3* pobParam, fiTokenizer & T, const char * nodeNameMaya);
	static bool WriteScriptMayaInit(const shaderMaterialGeoParamValueVector3* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueVector3* pobParam);

	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueVector3* pobParam);
};

} // end namespace rage

#endif
