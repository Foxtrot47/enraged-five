// 
// rageShaderMaya/rageShaderTexture.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef RAGE_SHADER_TEXTURE
#define RAGE_SHADER_TEXTURE

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MImage.h>
#pragma warning(error : 4668)

#include <GL/gl.h>
#include <GL/glu.h>

namespace rage {

class rageShaderTextureObject
{
public:
	rageShaderTextureObject()
	{
		// Get a texture identifier.
		glGenTextures(1, &m_textureNum);

		// Set up default values for the texture parameters.
		m_minFilterParam = GL_NEAREST;
		m_magFilterParam = GL_LINEAR;
		m_wrapSParam = GL_REPEAT;
		m_wrapTParam = GL_REPEAT;
	}

	~rageShaderTextureObject()
	{
		glDeleteTextures(1, &m_textureNum);
	}

	GLenum target() const {return GL_TEXTURE_2D;}

	GLuint textureNumber() const {return m_textureNum;}

	void bind()
	{
		// Bind the texture.
		glBindTexture(GL_TEXTURE_2D, m_textureNum);

		// Set up the texture parameters.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_minFilterParam);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_magFilterParam);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_wrapSParam);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_wrapTParam);
	}

	void parameter(GLenum pname, GLint param)
	{
		switch (pname)
		{
		case GL_TEXTURE_MIN_FILTER:	m_minFilterParam = param;	break;
		case GL_TEXTURE_MAG_FILTER:	m_magFilterParam = param;	break;
		case GL_TEXTURE_WRAP_S:		m_wrapSParam = param;		break;
		case GL_TEXTURE_WRAP_T:		m_wrapTParam = param;		break;
		}
	}

private:

	// Various parameters. See glTexParameterf() in MSDN for more info.
	GLint m_minFilterParam;
	GLint m_magFilterParam;
	GLint m_wrapSParam;
	GLint m_wrapTParam;
	
	GLuint m_textureNum;
};

class rageShaderTexture
{
public:

	rageShaderTexture();
	~rageShaderTexture();

	bool set(MImage &image);

	bool load(const char * fileName);

	bool load(const u8 * pBits, const int nWidth, const int nHeight);
	
	bool specify();

	u32 textureNumber() {return m_texObj.textureNumber();}

	void bind();

	u8* fetch(u32 s, u32 t)
	{
		if (m_bits == NULL)
			return NULL;
		
		return internalFetch(s, t);
	}
	
	inline bool square()
	{
		return m_width == m_height;
	}

	inline u32 width()
	{
		return m_width;
	}
	
	inline u32 height()
	{
		return m_height;
	}

protected:

	inline u8* internalFetch(u32 s, u32 t)
	{
		Assert((s >= 0) && (s < m_width));
		Assert((t >= 0) && (t < m_height));
		return m_bits + 4 * ((m_width * t) + s);
	}


private:
	
	u32 m_width, m_height;
	rageShaderTextureObject m_texObj;
	
	u8 * m_bits;

	GLint		m_internalFormat;
	GLenum		m_format;
	GLenum		m_componentFormat;
};

} // end using namespace rage

#endif // RAGE_SHADER_TEXTURE
