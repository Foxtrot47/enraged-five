#pragma once
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#pragma warning(error: 4668)

class UserSettings
{
public:
	static MString DefaultImageToUse() {return GetStringOptionVar("rageShaderViewer_DefaultImageToUse");}
	static bool ExpandTexturesOnFileLoad() {return GetOptionVar("rageShaderViewer_ExpandTexturesOnFileLoad");}
	static bool ViewerAlwaysOnTop() {return GetOptionVar("rageShaderViewer_AlwaysOnTop");}
	static bool ViewerAcceptInput() {return GetOptionVar("rageShaderViewer_AcceptInput");}
	static bool SilentRunning() {int bSilentRunning; MGlobal::executeCommand("rageSilentRunning()", bSilentRunning); return (bSilentRunning != 0);}
	static bool ViewVertexColoursInMaya() {return GetOptionVar("rageShaderViewer_ViewVertexColoursInMaya");}
	static bool CreateNewMaterialsFromGeoTemplates() {return GetOptionVar("rageShaderViewer_CreateNewMaterialsFromGeoTemplates");}
	static void CreateNewMaterialsFromGeoTemplates(bool bOptionVar) {return SetOptionVar("rageShaderViewer_CreateNewMaterialsFromGeoTemplates", bOptionVar);}

private:
	static bool GetOptionVar(const char* pcOptionVar) 
	{
		int bOptionVar; 
		MGlobal::getOptionVarValue(pcOptionVar, bOptionVar);
		return (bOptionVar != 0);
	}

	static void SetOptionVar(const char* pcOptionVar, bool bOptionVar) 
	{
		MGlobal::setOptionVarValue(pcOptionVar, bOptionVar);
	}

	static MString GetStringOptionVar(const char* pcOptionVar) 
	{
		MString strOptionVar; 
//		MGlobal::executeCommand(MString("optionVar -q \"")+ pcOptionVar +"\"", bOptionVar);
		MGlobal::getOptionVarValue(pcOptionVar, strOptionVar);
		return strOptionVar;
	}
};



