#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MIntArray.h>
#include <maya/MGlobal.h>
#include <maya/MFnSet.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MItDependencyGraph.h>
#include <maya/MDagPath.h>
#pragma warning(error: 4668)

#include "shaderMaterial/shaderMaterialGeoParamValueTexture.h"
#include "rageShaderMaterial/rageShaderMaterialGroup.h"
#include "rageShaderMaterial/rageShaderMaterialTemplate.h"
#include "rageShaderMaterialViewer/rageShaderMaterialViewer.h"
#include "rageShaderMaya/rageShaderUpdateManager.h"

#include "shaderMaterial/shaderMaterialGeoTemplate.h"
#include "shaderMaterial/shaderMaterialGeoManager.h"

#include "MayaModelMaterialPair.h"



using namespace rage;

extern bool g_bDisplayMGlobalExecuteCommands;

vector<MayaModelMaterialPair*> MayaModelMaterialPair::obMayaModelMaterialPairsThatNeedUpdating;

MayaModelMaterialPair::MayaModelMaterialPair(const MString & sMeshNodeName, const MObject obMeshNode, const MObject obShaderNode, const rageShaderMaterial* pMtl)
{
	if(!pMtl)
	{
		// Creating a pointless MayaModelMaterialPair
		Displayf("Creating a pointless MayaModelMaterialPair");
	}
	m_sMeshNodeName = sMeshNodeName;
	m_obMeshNode = obMeshNode;
	m_obShaderNode = obShaderNode;
	m_pMaterial = pMtl;
	m_pobModelMaterialPair = NULL;

	MDagPath obDagPathToMeshNode;
	MDagPath::getAPathTo(m_obMeshNode, obDagPathToMeshNode);
	MObject obTransformNode = obDagPathToMeshNode.transform();

	// Setup callbacks
	m_MeshNodeDirtyCallbackId = MNodeMessage::addNodeDirtyCallback(m_obMeshNode, &MayaModelMaterialPair::MeshNodeDirtyCallback, this);
	m_MeshTransformNodeDirtyCallbackId = MNodeMessage::addNodeDirtyCallback(obTransformNode, &MayaModelMaterialPair::MeshTransformNodeDirtyCallback, this);

	// Set up child pair
	convertMesh();
	UpdateShader();
	UpdateMeshTransform();
}

MayaModelMaterialPair::~MayaModelMaterialPair(void)
{
	// Displayf("Deleting MayaModelMaterialPair %p", this);
	MMessage::removeCallback(m_MeshNodeDirtyCallbackId);
	MMessage::removeCallback(m_MeshTransformNodeDirtyCallbackId);

	// Because of all the multithreading going on, I can not just kill my children, I have to wait until 
	// noone can see them
	if(m_pobModelMaterialPair)
	{
		m_pobModelMaterialPair->MakeMeWantToDie();
		for(unsigned i=0; i<obMayaModelMaterialPairsThatNeedUpdating.size(); i++)
		{
			if(obMayaModelMaterialPairsThatNeedUpdating[i] == this)
			{
				// Already in the list
				obMayaModelMaterialPairsThatNeedUpdating.erase(obMayaModelMaterialPairsThatNeedUpdating.begin() + i);
				break;
			}
		}
		m_pobModelMaterialPair = NULL;
	}
}

void MayaModelMaterialPair::UpdateShader()
{
	 if(m_pobModelMaterialPair)
	 {
		 // Make sure the textures have been exported.
		 atArray <ConstString> aTextureFileNames;
		 atArray <ConstString> aTextureFormats;
		 m_pMaterial->GetTextureFileNamesAndFormats(aTextureFileNames, aTextureFormats);
		 rageShaderMaterialViewer::ExportTextures(rageShaderUpdateManager::GetRageShadersAssetsFolder(), aTextureFileNames, aTextureFormats);

		 m_pobModelMaterialPair->SetMaterial(m_pMaterial);
	 }
}

void MayaModelMaterialPair::UpdateMesh()
{
	// Do I need to tell MEL to update myself?
	if(obMayaModelMaterialPairsThatNeedUpdating.size() == 0)
	{
		// Noone else in the list, so I need to tell Mel to update me
		MGlobal::executeCommandOnIdle("rageShaderCommand -updateMeshes", g_bDisplayMGlobalExecuteCommands);
	}

	// Am I already in the list of meshes that need updating?
	bool bAlreadyInTheListOfThingsToUpdate = false;
	for(unsigned i=0; i<obMayaModelMaterialPairsThatNeedUpdating.size(); i++)
	{
		if(obMayaModelMaterialPairsThatNeedUpdating[i] == this)
		{
			// Already in the list
			bAlreadyInTheListOfThingsToUpdate = true;
			break;
		}
	}

	if(!bAlreadyInTheListOfThingsToUpdate)
	{
		obMayaModelMaterialPairsThatNeedUpdating.push_back(this);
	}
}


///////////////////////////////////////////////////////
// DESCRIPTION:
// This function gets called by updateMesh()
//
// The function converts a maya MFnMesh object into a 
// rage mshMesh object.
//
///////////////////////////////////////////////////////

bool MayaModelMaterialPair::convertMesh()
{
	// Reset the out mesh
	mshMesh obRageMesh;
	MFnMesh obFnMeshNode(m_obMeshNode);

	// Get the shadingGroup(MEL term)/shadingEngine(API term) that my shader is in
	MStatus rc = MStatus::kSuccess;
	MItDependencyGraph it(m_obShaderNode, 
		MFn::kShadingEngine, 
		MItDependencyGraph::kDownstream, 
		MItDependencyGraph::kBreadthFirst, 
		MItDependencyGraph::kNodeLevel, &rc);
	MObject obMyShadingEngine = ((rc == MStatus::kSuccess ? it.thisNode() : MObject::kNullObj));

	// Get the shaders on my mesh
	MObjectArray aobShaderEnginesConnectedToMesh;
	MIntArray aiFaceToShaderIndices;
	//const char* pcMeshNodeName = obFnMeshNode.name().asChar();
	//const char* pcMeshNodeFullPathName = obFnMeshNode.fullPathName().asChar();
	//Displayf("pcMeshNodeName = %s", pcMeshNodeName);
	//Displayf("numVertices = %d", obFnMeshNode.numVertices());
	//Displayf("numEdges = %d", obFnMeshNode.numEdges());
	//Displayf("numPolygons = %d", obFnMeshNode.numPolygons());
	//Displayf("numFaceVertices = %d", obFnMeshNode.numFaceVertices());
	//Displayf("numUVs = %d", obFnMeshNode.numUVs());
	//Displayf("numColors = %d", obFnMeshNode.numColors());
	//Displayf("numNormals = %d", obFnMeshNode.numNormals());
	//Displayf("numUVSets = %d", obFnMeshNode.numUVSets());
	//Displayf("numColorSets = %d", obFnMeshNode.numColorSets());
	//Displayf("pcMeshNodeFullPathName = %s", pcMeshNodeFullPathName);
	if (obFnMeshNode.getConnectedShaders(0,aobShaderEnginesConnectedToMesh,aiFaceToShaderIndices) != MS::kSuccess) 
	{
		return false;
	}

	MStringArray uvsetnames;
	obFnMeshNode.getUVSetNames(uvsetnames);

	int uvSetCount = 0;
	atArray <ConstString> uvSetNames;
	atArray <ConstString> fixedUvSetNames;
	uvSetNames.Resize(uvsetnames.length());
	for (int i=0; i<uvSetNames.GetCount(); i++)
	{
		uvSetNames[i] = uvsetnames[i].asChar();
	}
	m_pMaterial->UpdateUvSetNames(uvSetNames, fixedUvSetNames);
	uvSetCount = fixedUvSetNames.GetCount();

	atArray <int> aTextureParamNoToUVSetIndices;
	atArray <Vector2> aMaterialData;

	for (int i=0; i<m_pMaterial->GetParamCount(); i++)
	{
		int index = m_pMaterial->GetParam(i)->GetParamDescription()->GetMaterialDataUvSetIndex();
		if (index >= 0)
		{
			if (aTextureParamNoToUVSetIndices.Find(index)>=0)
			{
				char buffer[256];
				formatf(buffer, sizeof(buffer), "Invalid param %s more then one material id references uv set %d", m_pMaterial->GetParam(i)->GetName(), index);
				MGlobal::displayError(buffer);
				continue;
			}

			aTextureParamNoToUVSetIndices.Grow() = index;
			m_pMaterial->GetParam(i)->CopyValueIntoVector2(aMaterialData.Grow());
			uvSetCount = ((index+1) > uvSetCount) ? (index+1) : uvSetCount;
		}
	}

	// Get source vertex colours
	MStringArray asNamesOfMeshsColorSets;
	obFnMeshNode.getColorSetNames(asNamesOfMeshsColorSets);

	vector<MColorArray> asVertexColoursOfMeshsColorSets;
	for(unsigned i=0; i<asNamesOfMeshsColorSets.length(); i++)
	{
		MColorArray colors;
		obFnMeshNode.getFaceVertexColors(colors, &asNamesOfMeshsColorSets[i]);
		asVertexColoursOfMeshsColorSets.push_back(colors);
	}

	MStringArray astrVertexColourSetsToIgnore = GetVertexColourSetsToIgnore();

	vector<MColorArray> obAObDestinationVertexColourSets;
	const shaderMaterialGeoTemplate* pTemplate = m_pMaterial->GetTemplate();
	if(pTemplate && (asVertexColoursOfMeshsColorSets.size() > 0))
	{
		for(int i=0; i<pTemplate->GetRunTimeVertexColourDescriptions().GetNumberOfDescriptions(); i++)
		{
			// Init the colours
			MColorArray aobVertexColors;
			for(unsigned j=0; j<asVertexColoursOfMeshsColorSets[0].length(); j++)
			{
				aobVertexColors.append(0.0f, 0.0f, 0.0f, 1.0f);
			}

			// Fill in each channel
			for(int iC = 0; iC<4; iC++)
			{
				const rsmRunTimeVertexColourDescription* pobRTVCDescription = pTemplate->GetRunTimeVertexColourDescriptions().GetDescription(i);
				MString strSourceForChannel = pobRTVCDescription->GetChannelSource(iC);
				MString strSourceNameForChannel = strSourceForChannel.substring(0, strSourceForChannel.index('.') - 1);
				MString strSourceChannelForChannel = strSourceForChannel.substring(strSourceForChannel.index('.') + 1, strSourceForChannel.length());

				// Find source channel
				int iSourceChannel = 0;
				if(strSourceChannelForChannel == "r")
				{
					iSourceChannel = 0;
				}
				if(strSourceChannelForChannel == "g")
				{
					iSourceChannel = 1;
				}
				if(strSourceChannelForChannel == "b")
				{
					iSourceChannel = 2;
				}
				if(strSourceChannelForChannel == "a")
				{
					iSourceChannel = 3;
				}

				// Set default values
				const rsmSourceVertexColourDescription* pobSVCDescription = pTemplate->GetSourceVertexColourDescriptions().GetDescription(ConstString(strSourceNameForChannel.asChar()));
				if(pobSVCDescription)
				{
					Vector4	obDefaultColour = pobSVCDescription->GetDefaultColour();
					for(unsigned j=0; j<asVertexColoursOfMeshsColorSets[0].length(); j++)
					{
						aobVertexColors[j][iC] = obDefaultColour[iSourceChannel];
					}
				}

				// Should I override with Maya data?
				bool bIgnoreMe = false;
				for(unsigned iI=0; iI<astrVertexColourSetsToIgnore.length(); iI++)
				{
					if(astrVertexColourSetsToIgnore[iI] == strSourceNameForChannel)
					{
						bIgnoreMe = true;
						break;
					}
				}
				if(!bIgnoreMe)
				{
					// Find source set
					int iSourceIndex = -1;
					for(unsigned j=0; j<asNamesOfMeshsColorSets.length(); j++)
					{
						if(asNamesOfMeshsColorSets[j] == strSourceNameForChannel)
						{
							// Bingo 
							iSourceIndex = j;
							break;
						}
					}

					if(iSourceIndex > -1)
					{
						// Set values
						for(unsigned j=0; j<asVertexColoursOfMeshsColorSets[0].length(); j++)
						{
							aobVertexColors[j][iC] = asVertexColoursOfMeshsColorSets[iSourceIndex][j][iSourceChannel];
						}
					}
				}
			}

			obAObDestinationVertexColourSets.push_back(aobVertexColors);
		}
	}
	
	// We only need one material per RAGE mesh
	char mtlIndexName[32];
	rageShaderMaterialGroup::SetMaterialIndex(mtlIndexName, 0);

	mshMaterial & rageMtl = obRageMesh.NewMtl();
	rageMtl.Name = mtlIndexName;

	for (int iMayaPolyNo=0; iMayaPolyNo<obFnMeshNode.numPolygons(); iMayaPolyNo++) 
	{
		// Am I looking at a poly number higher than the highest in aiFaceToShaderIndices
		// NOTE: This should never be possible
		if((u32)iMayaPolyNo > aiFaceToShaderIndices.length())
		{
			continue;
		}

		// Do I not have a shader?
		if (aiFaceToShaderIndices[iMayaPolyNo] == -1) 
		{
			continue;
		}
		
		// Am I the shader this poly has?
		//const char* pcNameOfMyShaderEngine = MFnDependencyNode(obMyShadingEngine).name().asChar();
		//const char* pcNameOfCurrentShaderEngine = MFnDependencyNode(aobShaderEnginesConnectedToMesh[aiFaceToShaderIndices[p]]).name().asChar();
		// // Displayf("Comparing %s and %s", pcNameOfMyShaderEngine, pcNameOfCurrentShaderEngine);
		if (aobShaderEnginesConnectedToMesh[aiFaceToShaderIndices[iMayaPolyNo]] != obMyShadingEngine)
		{
			continue;
		}

		// Get the number of triangles needed to represent this poly
		int iNoOfVertsInPoly = obFnMeshNode.polygonVertexCount(iMayaPolyNo);
		int iNoOfTrianglesInPoly = (iNoOfVertsInPoly - 2);		// The number of triangles is always 2 less than the number of verts, I THINK!

		// Get poly based verts, needed later
		MIntArray aiMeshRelativePolyVertexIndexArray;
		obFnMeshNode.getPolygonVertices(iMayaPolyNo,aiMeshRelativePolyVertexIndexArray);

		// For each triangle in the Maya poly, add to the Rage mesh
		for(int iMayaTriInPolyNo=0; iMayaTriInPolyNo<iNoOfTrianglesInPoly; iMayaTriInPolyNo++)
		{
			// Displayf("iMayaTriInPolyNo = %d", iMayaTriInPolyNo);
			mshPrimitive &prim = rageMtl.Prim.Append();
			prim.Type = mshPOLYGON;
			prim.Priority = rageMtl.Priority;

			int aiMeshRelativeTriangleVertexIndexArray[3];
			obFnMeshNode.getPolygonTriangleVertices(iMayaPolyNo,iMayaTriInPolyNo,aiMeshRelativeTriangleVertexIndexArray);
			for (unsigned iTriangleRelativeVertexIndex=0; iTriangleRelativeVertexIndex<3; iTriangleRelativeVertexIndex++) 
			{
				// Displayf("iTriangleRelativeVertexIndex = %d", iTriangleRelativeVertexIndex);

				// Get MeshRelativeVertexIndex
				int iMeshRelativeVertexIndex = aiMeshRelativeTriangleVertexIndexArray[iTriangleRelativeVertexIndex];
				// Displayf("iMeshRelativeVertexIndex = %d", iMeshRelativeVertexIndex);

				// Get PolyRelativeVertexIndex
				int iPolyRelativeVertexIndex = 0;
				for(;iPolyRelativeVertexIndex<(int)aiMeshRelativePolyVertexIndexArray.length(); iPolyRelativeVertexIndex++)
				{
					if(iMeshRelativeVertexIndex == aiMeshRelativePolyVertexIndexArray[iPolyRelativeVertexIndex]) break;
				}
				// Displayf("iPolyRelativeVertexIndex = %d", iPolyRelativeVertexIndex);

				// Get info about vert
				MPoint pos;
				obFnMeshNode.getPoint(iMeshRelativeVertexIndex, pos, MSpace::kWorld);

				Vector3 P((float)pos.x, (float)pos.y, (float)pos.z);
				// Displayf("Point = %f %f %f", (float)pos.x, (float)pos.y, (float)pos.z);

				MVector MN;
				obFnMeshNode.getFaceVertexNormal(iMayaPolyNo, iMeshRelativeVertexIndex, MN, MSpace::kWorld);
				Vector3 N(float(MN.x),float(MN.y),float(MN.z));
				// Displayf("Normal = %f %f %f", (float)N.x, (float)N.y, (float)N.z);

				// We are going to initalize the texture coords
				atArray <Vector2> TC;
				TC.Resize(uvSetCount);
				for (int i=0; i<uvSetCount; i++)
				{
					bool foundData = false;
					for (int j=0; j<aTextureParamNoToUVSetIndices.GetCount(); j++)
					{
						if (aTextureParamNoToUVSetIndices[j] == i)
						{
							TC[i] = aMaterialData[j];
							foundData = true;
							break;
						}
					}

					if (!foundData)
					{
						MString uvSetName = fixedUvSetNames[i];
						float fXTexCoord;
						float fYTexCoord;
						obFnMeshNode.getPolygonUV(iMayaPolyNo,iPolyRelativeVertexIndex,fXTexCoord,fYTexCoord,&uvSetName);
						// Check the values make sense, sometime Maya gives me madness
						if(!FPIsFinite(fXTexCoord)) fXTexCoord = 0.0f;
						if(!FPIsFinite(fYTexCoord)) fYTexCoord = 0.0f;
						TC[i].x = fXTexCoord;
						TC[i].y = fYTexCoord;
					}
				}

				Vector4 C0(1,1,1,1);
				int iFaceVertexColorIndex;
				obFnMeshNode.getFaceVertexColorIndex(iMayaPolyNo,iPolyRelativeVertexIndex, iFaceVertexColorIndex);
				if ((obAObDestinationVertexColourSets.size() > 0) && (obAObDestinationVertexColourSets[0].length()))
				{
					MColor obColour = obAObDestinationVertexColourSets[0][iFaceVertexColorIndex];
					C0.x = obColour.r;
					C0.y = obColour.g;
					C0.z = obColour.b;
					C0.w = obColour.a;
					//Displayf("%d C0 %f %f %f %f", iFaceVertexColorIndex, obColour.r, obColour.g, obColour.b, obColour.a);
				}
				else
				{
					if ((asVertexColoursOfMeshsColorSets.size() > 0) && (asVertexColoursOfMeshsColorSets[0].length()))
					{
						MColor obColour = asVertexColoursOfMeshsColorSets[0][iFaceVertexColorIndex];
						C0.x = obColour.r;
						C0.y = obColour.g;
						C0.z = obColour.b;
						C0.w = obColour.a;
						//Displayf("%d C0 %f %f %f %f", iFaceVertexColorIndex, obColour.r, obColour.g, obColour.b, obColour.a);
					}
				}

				Vector4 C1(1,1,1,1);
				if ((obAObDestinationVertexColourSets.size() > 1) && (obAObDestinationVertexColourSets[1].length()))
				{
					MColor obColour = obAObDestinationVertexColourSets[1][iFaceVertexColorIndex];
					C1.x = obColour.r;
					C1.y = obColour.g;
					C1.z = obColour.b;
					C1.w = obColour.a;
					//Displayf("%d C1 %f %f %f %f", iFaceVertexColorIndex, obColour.r, obColour.g, obColour.b, obColour.a);
				}
				else
				{
					if ((asVertexColoursOfMeshsColorSets.size() > 1) && (asVertexColoursOfMeshsColorSets[1].length()))
					{
						MColor obColour = asVertexColoursOfMeshsColorSets[1][iFaceVertexColorIndex];
						C1.x = obColour.r;
						C1.y = obColour.g;
						C1.z = obColour.b;
						C1.w = obColour.a;
						//Displayf("%d C1 %f %f %f %f", iFaceVertexColorIndex, obColour.r, obColour.g, obColour.b, obColour.a);
					}
				}

				Vector4 C2(1,1,1,1);
				if ((obAObDestinationVertexColourSets.size() > 2) && (obAObDestinationVertexColourSets[2].length()))
				{
					MColor obColour = obAObDestinationVertexColourSets[2][iFaceVertexColorIndex];
					C2.x = obColour.r;
					C2.y = obColour.g;
					C2.z = obColour.b;
					C2.w = obColour.a;
					//Displayf("%d C2 %f %f %f %f", iFaceVertexColorIndex, obColour.r, obColour.g, obColour.b, obColour.a);
				}
				else
				{
					if ((asVertexColoursOfMeshsColorSets.size() > 2) && (asVertexColoursOfMeshsColorSets[2].length()))
					{
						MColor obColour = asVertexColoursOfMeshsColorSets[2][iFaceVertexColorIndex];
						C2.x = obColour.r;
						C2.y = obColour.g;
						C2.z = obColour.b;
						C2.w = obColour.a;
						//Displayf("%d C2 %f %f %f %f", iFaceVertexColorIndex, obColour.r, obColour.g, obColour.b, obColour.a);
					}
				}

				mshBinding B;
				B.Reset();

				mshIndex vertIdx = rageMtl.AddVertex(P,B,N,C0,C1,C2, TC.GetCount(),(TC.GetCount()>0) ? &TC[0] : NULL);
				prim.Idx.Append() = vertIdx;
			}
		}
	}

	obRageMesh.CleanModel();
	if(obRageMesh.GetMtlCount())
	{
		// I am a mesh worthy of rendering, so make pass me off to the renderer
		if(!m_pobModelMaterialPair)
		{
			m_pobModelMaterialPair = new ModelMaterialPair(m_pMaterial, obRageMesh);
		}
		m_pobModelMaterialPair->SetMesh(obRageMesh);
	}
	else
	{
		// I am not worth rendering, so kill me
		if(m_pobModelMaterialPair)
		{
			m_pobModelMaterialPair->IWantToDie();
			m_pobModelMaterialPair = NULL;
		}
	}

	return true;
}

// Callbacks
void MayaModelMaterialPair::MeshNodeDirtyCallback(MObject & /*obMayaNode*/, void * thisPtr)
{
	((MayaModelMaterialPair*)thisPtr)->UpdateMesh();
}

void	MayaModelMaterialPair::UpdateMeshTransform()
{
	if(m_pobModelMaterialPair)
	{
		MStatus obStatus;
		MDagPath obDagPathToMeshNode;
		obStatus = MDagPath::getAPathTo(m_obMeshNode, obDagPathToMeshNode);
		if (obStatus.error())
		{
			return;
		}

		Matrix34 m34;
		rageShaderUpdateManager::convertMatrix(m34, obDagPathToMeshNode.inclusiveMatrix());
		m_pobModelMaterialPair->UpdateMeshTransform(m34);
	}
}

void MayaModelMaterialPair::MeshTransformNodeDirtyCallback(MObject & /*obMayaNode*/, void * thisPtr)
{
	((MayaModelMaterialPair*)thisPtr)->UpdateMeshTransform();
}

// Update all MayaModelMaterialPair that need updating
void MayaModelMaterialPair::UpdateAllMayaModelMaterialPairsThatNeedUpdating()
{
	while(obMayaModelMaterialPairsThatNeedUpdating.size() > 0)
	{
		// Update it
		obMayaModelMaterialPairsThatNeedUpdating[0]->convertMesh();
		obMayaModelMaterialPairsThatNeedUpdating.erase(obMayaModelMaterialPairsThatNeedUpdating.begin());
	}
}

MStringArray MayaModelMaterialPair::GetVertexColourSetsToIgnore()
{
	MStringArray astrVertexColourSetsToIgnore;
	MGlobal::executeCommand("rageShaderMaya_GetVertexColourSetsToIgnore()", astrVertexColourSetsToIgnore, g_bDisplayMGlobalExecuteCommands);
	return astrVertexColourSetsToIgnore;
}

