// 
// rageShaderMaterial/rageShaderMaterialMayaParamTexture.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_MATERIAL_MayaParam_TEXTURE_H
#define RAGE_SHADER_MATERIAL_MayaParam_TEXTURE_H

#include "rageShaderMaterialMayaParam.h"
#include "shaderMaterial/shaderMaterialGeoParamValueTexture.h"


namespace rage {

class rageShaderMaterialMayaParamTexture
{
public:
	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueTexture* pobParam);

	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueTexture* pobParam);
};

} // end namespace rage

#endif
