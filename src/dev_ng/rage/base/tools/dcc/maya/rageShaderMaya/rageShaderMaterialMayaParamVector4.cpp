// 
// rageShaderMaterial/rageShaderMaterialMayaParamVector4.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnNumericAttribute.h>
#include <maya/MDGModifier.h>
#pragma warning(pop)

#include "file/token.h"
#include "file/stream.h"
#include "rageShaderMaterialMayaParamVector4.h"

using namespace rage;


bool rageShaderMaterialMayaParamVector4::WriteScriptMayaUi(const shaderMaterialGeoParamValueVector4* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	if (stricmp(pobParam->GetParamDescription()->GetUIWidget(), "color")==0)
	{
		T.StartLine();
		T.PutStr("attrControlGrp -attribute (%s + \".%s\");", nodeNameMaya, sParamName.asChar());
		T.EndLine();
		
		T.StartLine();
		T.PutStr("attrControlGrp -attribute (%s + \".%sAlpha\");", nodeNameMaya, sParamName.asChar());
		T.EndLine();
	}
	else
	{

		static int s_UniqueNumber = 0;

		T.StartLine();
		T.PutStr("floatFieldGrp -label \"%s\" -columnWidth5 145 52 52 52 52 -numberOfFields 4 \"v4_%d\";", sParamName.asChar(), s_UniqueNumber);
		T.EndLine();

		T.StartLine();
		T.PutStr("connectControl -index 2 \"v4_%d\" (%s + \".%sX\");", s_UniqueNumber, nodeNameMaya, sParamName.asChar());
		T.EndLine();

		T.StartLine();
		T.PutStr("connectControl -index 3 \"v4_%d\" (%s + \".%sY\");", s_UniqueNumber, nodeNameMaya, sParamName.asChar());
		T.EndLine();

		T.StartLine();
		T.PutStr("connectControl -index 4 \"v4_%d\" (%s + \".%sZ\");", s_UniqueNumber, nodeNameMaya, sParamName.asChar());
		T.EndLine();

		T.StartLine();
		T.PutStr("connectControl -index 5 \"v4_%d\" (%s + \".%sW\");", s_UniqueNumber, nodeNameMaya, sParamName.asChar());
		T.EndLine();

		s_UniqueNumber++;
	}

	return true;
}

bool rageShaderMaterialMayaParamVector4::WriteScriptMayaInit(const shaderMaterialGeoParamValueVector4* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	if (stricmp(pobParam->GetParamDescription()->GetUIWidget(), "color")==0)
	{
		T.StartLine();
		T.PutStr("setAttr (%s + \".%sr\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().x);
		T.EndLine();

		T.StartLine();
		T.PutStr("setAttr (%s + \".%sg\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().y);
		T.EndLine();

		T.StartLine();
		T.PutStr("setAttr (%s + \".%sb\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().z);
		T.EndLine();

		T.StartLine();
		T.PutStr("setAttr (%s + \".%sAlpha\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().w);
		T.EndLine();
	}
	else
	{
		T.StartLine();
		T.PutStr("setAttr (%s + \".%sX\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().x);
		T.EndLine();

		T.StartLine();
		T.PutStr("setAttr (%s + \".%sY\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().y);
		T.EndLine();

		T.StartLine();
		T.PutStr("setAttr (%s + \".%sZ\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().z);
		T.EndLine();

		T.StartLine();
		T.PutStr("setAttr (%s + \".%sW\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().w);
		T.EndLine();
	}

	return true;
}

bool rageShaderMaterialMayaParamVector4::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueVector4* pobParam)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	if (stricmp(pobParam->GetParamDescription()->GetUIWidget(), "color")==0)
	{
		// Create color input attributes
		MFnNumericAttribute nAttr;
		MStatus status;

		// Create color input attributes
		//do I already have the attributes
		MFnDependencyNode obFnFileNode(obNode);
		if (!obFnFileNode.hasAttribute(sParamName.asChar()) && 
			!obFnFileNode.hasAttribute(sParamName +"Alpha")
			)
		{
			MDGModifier obModifier;
			if(!obFnFileNode.hasAttribute(sParamName.asChar()))
			{
				MObject aColorR = nAttr.create( sParamName +"r", sParamName +"r",MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setKeyable( false ) );
				CHECK_STATUS( nAttr.setStorable( false ) );
				CHECK_STATUS( nAttr.setCached( false ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().x ) );
				CHECK_STATUS( nAttr.setRenderSource( true ) );

				MObject aColorG = nAttr.create( sParamName +"g", sParamName +"g", MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setKeyable( false ) );
				CHECK_STATUS( nAttr.setStorable( false ) );
				CHECK_STATUS( nAttr.setCached( false ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().y ) );
				CHECK_STATUS( nAttr.setRenderSource( true ) );

				MObject aColorB = nAttr.create( sParamName +"b", sParamName +"b",MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setKeyable( false ) );
				CHECK_STATUS( nAttr.setStorable( false ) );
				CHECK_STATUS( nAttr.setCached( false ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().z ) );
				CHECK_STATUS( nAttr.setRenderSource( true ) );

				MObject obColourAttribute = nAttr.create( sParamName.asChar(), sParamName.asChar(), aColorR, aColorG, aColorB,	&status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setInternal( true ) );
				CHECK_STATUS( nAttr.setKeyable( false ) );
				CHECK_STATUS( nAttr.setStorable( false ) );
				CHECK_STATUS( nAttr.setCached( false ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().x, pobParam->GetValue().y, pobParam->GetValue().z ) );
				CHECK_STATUS( nAttr.setRenderSource( true ) );
				CHECK_STATUS( nAttr.setUsedAsColor( true ) );
				CHECK_STATUS(obModifier.addAttribute(obNode, obColourAttribute));
			}

			if(!obFnFileNode.hasAttribute(sParamName +"Alpha"))
			{
				MObject obAttributeW = nAttr.create( sParamName +"Alpha", sParamName +"Alphas",	MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setStorable( true ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setReadable( true ) );
				CHECK_STATUS( nAttr.setWritable( true ) );
				CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
				CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().w ) );
				CHECK_STATUS( nAttr.setInternal( true ) );
				CHECK_STATUS(obModifier.addAttribute(obNode, obAttributeW));
			}
			CHECK_STATUS(obModifier.doIt());
		}
	}
	else
	{
		MFnNumericAttribute nAttr;
		MStatus status;
		// Displayf("%s Min = %f Max = %f", sParamName.asChar(), pobParam->GetParamDescription()->GetUIMin(), pobParam->GetParamDescription()->GetUIMax());
		//do I already have the attributes
		MFnDependencyNode obFnFileNode(obNode);
		if (!(obFnFileNode.hasAttribute(sParamName +"X") && 
			  obFnFileNode.hasAttribute(sParamName +"Y") && 
			  obFnFileNode.hasAttribute(sParamName +"Z") && 
			  obFnFileNode.hasAttribute(sParamName +"W") )
			)
		{
			MDGModifier obModifier;
			if(!obFnFileNode.hasAttribute(sParamName +"X"))
			{
				MObject obAttributeX = nAttr.create( sParamName +"X", sParamName +"Xs",	MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setStorable( true ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setReadable( true ) );
				CHECK_STATUS( nAttr.setWritable( true ) );
				CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
				CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().x ) );
				CHECK_STATUS( nAttr.setInternal( true ) );
				CHECK_STATUS(obModifier.addAttribute(obNode, obAttributeX));
			}

			if(!obFnFileNode.hasAttribute(sParamName +"Y"))
			{
				MObject obAttributeY = nAttr.create( sParamName +"Y", sParamName +"Ys",	MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setStorable( true ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setReadable( true ) );
				CHECK_STATUS( nAttr.setWritable( true ) );
				CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
				CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().y ) );
				CHECK_STATUS( nAttr.setInternal( true ) );
				CHECK_STATUS(obModifier.addAttribute(obNode, obAttributeY));
			}

			if(!obFnFileNode.hasAttribute(sParamName +"Z"))
			{
				MObject obAttributeZ = nAttr.create( sParamName +"Z", sParamName +"Zs",	MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setStorable( true ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setReadable( true ) );
				CHECK_STATUS( nAttr.setWritable( true ) );
				CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
				CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().z ) );
				CHECK_STATUS( nAttr.setInternal( true ) );
				CHECK_STATUS(obModifier.addAttribute(obNode, obAttributeZ));
			}

			if(!obFnFileNode.hasAttribute(sParamName +"W"))
			{
				MObject obAttributeW = nAttr.create( sParamName +"W", sParamName +"Ws",	MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setStorable( true ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setReadable( true ) );
				CHECK_STATUS( nAttr.setWritable( true ) );
				CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
				CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().w ) );
				CHECK_STATUS( nAttr.setInternal( true ) );
				CHECK_STATUS(obModifier.addAttribute(obNode, obAttributeW));
			}
			CHECK_STATUS(obModifier.doIt());
		}
		return true;
	}

	return true;
}

bool rageShaderMaterialMayaParamVector4::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueVector4* pobParam)
{
	MFnDependencyNode obFnFileNode(obNode);
	MStatus result;
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());
	MPlug plug = obFnFileNode.findPlug(sParamName + "Xs", &result);
	bool xval = false;
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == pobParam->GetValue().x)
		{
			xval = true;
		}
	}

	plug = obFnFileNode.findPlug(sParamName + "Ys", &result);
	bool yval = false;
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == pobParam->GetValue().y)
		{
			yval = true;
		}
	}

	plug = obFnFileNode.findPlug(sParamName + "Zs", &result);
	bool zval = false;
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == pobParam->GetValue().z)
		{
			zval = true;
		}
	}

	plug = obFnFileNode.findPlug(sParamName + "Ws", &result);
	bool wval = false;
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == pobParam->GetValue().w)
		{
			wval = true;
		}
	}
	return (xval && yval && zval && wval);
}
