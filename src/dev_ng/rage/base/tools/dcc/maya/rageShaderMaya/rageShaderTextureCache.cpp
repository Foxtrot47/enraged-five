// 
// rageShaderMaya/rageShaderTextureCache.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPlug.h>
#include <maya/MFnDependencyNode.h>
#pragma warning(error : 4668)

#include "file/device.h"
#include "file/asset.h"

#include "rageShaderTexture.h"
#include "rageShaderTextureCache.h"

using namespace rage;

rageShaderTextureCacheData::rageShaderTextureCacheData()
{
	m_timestamp = 0; 
	m_texture = NULL; 
}

rageShaderTextureCacheData::~rageShaderTextureCacheData()
{ 
	if (m_texture) 
	{
		delete m_texture;
		m_texture = NULL;
	}
}

bool rageShaderTextureCacheData::isDirty(const char * fileName) const
{
	return m_timestamp != fiDeviceLocal::GetInstance().GetFileTime(fileName);
}

bool rageShaderTextureCacheData::load(const char * fileName)
{
	if (m_texture)
	{
		delete m_texture;
		m_texture= NULL;
	}

	// Create the rageShaderTexture
	m_texture = new rageShaderTexture;

	m_timestamp = fiDeviceLocal::GetInstance().GetFileTime(fileName);

	return m_texture->load(fileName);
}

bool rageShaderTextureCacheData::load(const char * mtlFileName, const u8 * pBits, const int nWidth, const int nHeight)
{
	if (m_texture)
	{
		delete m_texture;
		m_texture= NULL;
	}

	// Create the rageShaderTexture
	m_texture = new rageShaderTexture;

	m_timestamp = fiDeviceLocal::GetInstance().GetFileTime(mtlFileName);

	return m_texture->load(pBits, nWidth, nHeight);
}

rageShaderTextureCache::rageShaderTextureCache()
{
}

rageShaderTextureCache::~rageShaderTextureCache()
{
	atMap<ConstString, rageShaderTextureCacheData*>::Iterator entry = m_textureTable.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		delete entry.GetData();
	}
	m_textureTable.Kill();
}

// Return a reference to the texture. Need to dereference by calling "release".
rageShaderTexture* rageShaderTextureCache::texture(const char * textureFilename)
{
	// Get the texture name
	ConstString textureName(fiAssetManager::FileName(textureFilename));

	// Check if we already have a texCacheData assigned to the given texture name.
	rageShaderTextureCacheData *texCacheData = m_textureTable[textureName];
	bool newTexture = !texCacheData;
	bool textureDirty = texCacheData && texCacheData->isDirty(textureFilename);

	if (newTexture)
	{
		texCacheData = new rageShaderTextureCacheData;

		// Add it to the map.
		m_textureTable[textureName] = texCacheData;
	}

	if (textureDirty || newTexture)
	{
		// Attempt to load the texture from disk and bind it in the OpenGL driver. 
		if (texCacheData->load(textureFilename) == false)
		{
			delete texCacheData;
			texCacheData = NULL;
			m_textureTable.Delete(textureName);
			return NULL;
		}
	}

	return texCacheData->texture();
}

// Return a reference to the texture. Need to dereference by calling "release".
rageShaderTexture* rageShaderTextureCache::texture(const char * mtlFileName, const u8 * pBits, const int nWidth, const int nHeight)
{
	// Get the texture name
	ConstString mtlName(fiAssetManager::FileName(mtlFileName));

	// Check if we already have a texCacheData assigned to the given texture name.
	rageShaderTextureCacheData *texCacheData = m_textureTable[mtlName];
	bool newTexture = !texCacheData;
	bool textureDirty = texCacheData && texCacheData->isDirty(mtlFileName);

	if (newTexture)
	{
		texCacheData = new rageShaderTextureCacheData;
		
		// Add it to the map.
		m_textureTable[mtlName] = texCacheData;
	}

	if (textureDirty || newTexture)
	{
		// Attempt to load the texture from disk and bind it in the OpenGL driver. 
		if (texCacheData->load(mtlFileName, pBits, nWidth, nHeight) == false)
		{
			delete texCacheData;
			texCacheData = NULL;
			m_textureTable.Delete(mtlName);
			return NULL;
		}
	}

	return texCacheData->texture();
}

// Binds using a texture file
rageShaderTexture*  rageShaderTextureCache::bind(const char * textureFileName)
{
	// Get a reference to the texture, allocating it if necessary.
	rageShaderTexture* pTex = texture(textureFileName);

	if (pTex)
	{
		// bind the texture.
		pTex->bind();
		return pTex;
	}
	
	return pTex;
}

// Binds using a piece of memory
rageShaderTexture*  rageShaderTextureCache::bind(const char * mtlFileName, const u8 * pBits, const int nWidth, const int nHeight)
{
	// Get a reference to the texture, allocating it if necessary.
	rageShaderTexture* pTex = texture(mtlFileName, pBits, nWidth, nHeight);

	if (pTex)
	{
		// bind the texture.
		pTex->bind();
		return pTex;
	}

	return pTex;
}
