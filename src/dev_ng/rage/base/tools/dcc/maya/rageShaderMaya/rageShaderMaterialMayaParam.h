// 
// rageShaderMaterial/shaderMaterialGeoParamValue.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef RAGE_SHADER_MATERIAL_MayaParam_H
#define RAGE_SHADER_MATERIAL_MayaParam_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include "maya/MFnDependencyNode.h"
#include "maya/MGlobal.h"
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#pragma warning(pop)

#include "shaderMaterial/shaderMaterialGeoParamValue.h"

#include <string>
using namespace std;

#define CHECK_STATUS(x)	\
{ \
	MStatus _maya_status = (x);	\
	if ( MStatus::kSuccess != _maya_status ) \
{ \
	MString obStrErrorMessage(__FILE__); \
	obStrErrorMessage += ":"; \
	obStrErrorMessage += __LINE__; \
	obStrErrorMessage += ":"; \
	_maya_status.perror (obStrErrorMessage); \
} \
} \

namespace rage {

class rageShaderMaterialMayaParam
{
public:
	// This function adds all the attributes
	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValue* pobParam);

	// This function checks to see if an attribute value is equal to the shader param value
	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValue* pobParam);

	// This function sets up the UI
	static bool WriteScriptMayaUi(const shaderMaterialGeoParamValue* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	// This function initializes the UI
	static bool WriteScriptMayaInit(const shaderMaterialGeoParamValue* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	static MString GetCleanVersionOfString(const char* pcQuestionableString);
};

} // end using namespace rage

#endif
