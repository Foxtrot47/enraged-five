// 
// rageShaderMaterial/rageShaderMaterialMayaParamVector3.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnNumericAttribute.h>
#include <maya/MDGModifier.h>
#pragma warning(pop)

#include "file/token.h"
#include "file/stream.h"

#include "rageShaderMaterialMayaParamVector3.h"

using namespace rage;


bool rageShaderMaterialMayaParamVector3::WriteScriptMayaUi(const shaderMaterialGeoParamValueVector3* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	if (stricmp(pobParam->GetParamDescription()->GetUIWidget(), "color")==0)
	{
		T.StartLine();
		T.PutStr("attrControlGrp -attribute (%s + \".%s\");", nodeNameMaya, sParamName.asChar());
		T.EndLine();
	}
	else
	{
		static int s_UniqueNumber = 0;
		T.StartLine();
		T.PutStr("floatFieldGrp -label \"%s\" -columnWidth4 145 52 52 52 -numberOfFields 3 \"v3_%d\";", sParamName.asChar(), s_UniqueNumber);
		T.EndLine();

		T.StartLine();
		T.PutStr("connectControl -index 2 \"v3_%d\" (%s + \".%sX\");", s_UniqueNumber, nodeNameMaya, sParamName.asChar());
		T.EndLine();

		T.StartLine();
		T.PutStr("connectControl -index 3 \"v3_%d\" (%s + \".%sY\");", s_UniqueNumber, nodeNameMaya, sParamName.asChar());
		T.EndLine();

		T.StartLine();
		T.PutStr("connectControl -index 4 \"v3_%d\" (%s + \".%sZ\");", s_UniqueNumber, nodeNameMaya, sParamName.asChar());
		T.EndLine();
		s_UniqueNumber++;
	}

	return true;
}

bool rageShaderMaterialMayaParamVector3::WriteScriptMayaInit(const shaderMaterialGeoParamValueVector3* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	if (stricmp(pobParam->GetParamDescription()->GetUIWidget(), "color")==0)
	{
		T.StartLine();
		T.PutStr("setAttr -type float3 (%s + \".%s\") %f %f %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().x, pobParam->GetValue().y, pobParam->GetValue().z);
		T.EndLine();
	}
	else
	{
		T.StartLine();
		T.PutStr("setAttr (%s + \".%sX\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().x);
		T.EndLine();

		T.StartLine();
		T.PutStr("setAttr (%s + \".%sY\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().y);
		T.EndLine();

		T.StartLine();
		T.PutStr("setAttr (%s + \".%sZ\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().z);
		T.EndLine();
	}

	return true;
}

bool rageShaderMaterialMayaParamVector3::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueVector3* pobParam)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	if (stricmp(pobParam->GetParamDescription()->GetUIWidget(), "color")==0)
	{
		// Create color input attributes
		MFnNumericAttribute nAttr;
		MStatus status;

		MFnDependencyNode obFnFileNode(obNode);
		if(!obFnFileNode.hasAttribute(sParamName.asChar()))
		{
			// Create color input attributes
			MObject aColorR = nAttr.create( sParamName +"r", sParamName +"r",MFnNumericData::kFloat, 0, &status );
			CHECK_STATUS( status );
			CHECK_STATUS( nAttr.setKeyable( false ) );
			CHECK_STATUS( nAttr.setStorable( false ) );
			CHECK_STATUS( nAttr.setCached( false ) );
			CHECK_STATUS( nAttr.setHidden( true ) );
			CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().x ) );
			CHECK_STATUS( nAttr.setRenderSource( true ) );

			MObject aColorG = nAttr.create( sParamName +"g", sParamName +"g", MFnNumericData::kFloat, 0, &status );
			CHECK_STATUS( status );
			CHECK_STATUS( nAttr.setKeyable( false ) );
			CHECK_STATUS( nAttr.setStorable( false ) );
			CHECK_STATUS( nAttr.setCached( false ) );
			CHECK_STATUS( nAttr.setHidden( true ) );
			CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().y ) );
			CHECK_STATUS( nAttr.setRenderSource( true ) );

			MObject aColorB = nAttr.create( sParamName +"b", sParamName +"b",MFnNumericData::kFloat, 0, &status );
			CHECK_STATUS( status );
			CHECK_STATUS( nAttr.setKeyable( false ) );
			CHECK_STATUS( nAttr.setStorable( false ) );
			CHECK_STATUS( nAttr.setCached( false ) );
			CHECK_STATUS( nAttr.setHidden( true ) );
			CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().z ) );
			CHECK_STATUS( nAttr.setRenderSource( true ) );

			MObject obColourAttribute = nAttr.create( sParamName.asChar(), sParamName.asChar(), aColorR, aColorG, aColorB,	&status );
			CHECK_STATUS( status );
			CHECK_STATUS( nAttr.setInternal( true ) );
			CHECK_STATUS( nAttr.setKeyable( false ) );
			CHECK_STATUS( nAttr.setStorable( false ) );
			CHECK_STATUS( nAttr.setCached( false ) );
			CHECK_STATUS( nAttr.setHidden( true ) );
			CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().x, pobParam->GetValue().y, pobParam->GetValue().z ) );
			CHECK_STATUS( nAttr.setRenderSource( true ) );
			CHECK_STATUS( nAttr.setUsedAsColor( true ) );

			MDGModifier obModifier;
			CHECK_STATUS(obModifier.addAttribute(obNode, obColourAttribute));
			CHECK_STATUS(obModifier.doIt());
		}
	}
	else
	{
		MFnNumericAttribute nAttr;
		MStatus status;
		MFnDependencyNode obFnFileNode(obNode);
		if( !obFnFileNode.hasAttribute(sParamName +"X") ||
			!obFnFileNode.hasAttribute(sParamName +"Y") ||
			!obFnFileNode.hasAttribute(sParamName +"Z")
			)
		{
			MDGModifier obModifier;

			if(!obFnFileNode.hasAttribute(sParamName +"X"))
			{
				MObject obAttributeX = nAttr.create( sParamName +"X", sParamName +"Xs",	MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setStorable( true ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setReadable( true ) );
				CHECK_STATUS( nAttr.setWritable( true ) );
				CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
				CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().x ) );
				CHECK_STATUS( nAttr.setInternal( true ) );
				CHECK_STATUS(obModifier.addAttribute(obNode, obAttributeX));
			}

			if(!obFnFileNode.hasAttribute(sParamName +"Y"))
			{
				MObject obAttributeY = nAttr.create( sParamName +"Y", sParamName +"Ys",	MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setStorable( true ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setReadable( true ) );
				CHECK_STATUS( nAttr.setWritable( true ) );
				CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
				CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().y ) );
				CHECK_STATUS( nAttr.setInternal( true ) );
				CHECK_STATUS(obModifier.addAttribute(obNode, obAttributeY));
			}

			if(!obFnFileNode.hasAttribute(sParamName +"Z"))
			{
				MObject obAttributeZ = nAttr.create( sParamName +"Z", sParamName +"Zs",	MFnNumericData::kFloat, 0, &status );
				CHECK_STATUS( status );
				CHECK_STATUS( nAttr.setStorable( true ) );
				CHECK_STATUS( nAttr.setHidden( true ) );
				CHECK_STATUS( nAttr.setReadable( true ) );
				CHECK_STATUS( nAttr.setWritable( true ) );
				CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
				CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
				CHECK_STATUS( nAttr.setDefault( pobParam->GetValue().z ) );
				CHECK_STATUS( nAttr.setInternal( true ) );
				CHECK_STATUS(obModifier.addAttribute(obNode, obAttributeZ));
			}

			CHECK_STATUS(obModifier.doIt());
		}
		return true;
	}

	return true;
}

bool rageShaderMaterialMayaParamVector3::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueVector3* pobParam)
{
	MFnDependencyNode obFnFileNode(obNode);
	MStatus result;
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());
	MPlug plug = obFnFileNode.findPlug(sParamName + "Xs", &result);
	bool xval = false;
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == pobParam->GetValue().x)
		{
			xval = true;
		}
	}

	plug = obFnFileNode.findPlug(sParamName + "Ys", &result);
	bool yval = false;
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == pobParam->GetValue().y)
		{
			yval = true;
		}
	}
	
	plug = obFnFileNode.findPlug(sParamName + "Zs", &result);
	bool zval = false;
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == pobParam->GetValue().z)
		{
			zval = true;
		}
	}
	return (xval && yval && zval);
}
