// 
// rageShaderMaterial/rageShaderMaterialMayaParamFloat.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_MATERIAL_MayaParam_FLOAT_H
#define RAGE_SHADER_MATERIAL_MayaParam_FLOAT_H

#include "rageShaderMaterialMayaParam.h"
#include "shaderMaterial/shaderMaterialGeoParamValueFloat.h"

namespace rage {

class rageShaderMaterialMayaParamFloat
{
public:
	static bool WriteScriptMayaInit(const shaderMaterialGeoParamValueFloat* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueFloat* pobParam);

	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueFloat* pobParam);
};

} // end namespace rage

#endif
