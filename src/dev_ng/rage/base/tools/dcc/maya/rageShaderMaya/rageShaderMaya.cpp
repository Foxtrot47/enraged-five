// 
// rageShaderMaya65/rageShaderMaya.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// Example Maya commandlines:
// 
// -file "T:/mc4/art/city/la/freeway/LA/geom/road/la_fwy_sandiego_01x.mb"
// -file "T:/mc4/art/city/test_bed/geom/test_bed.mb"
// -file "T:/mc4/art/city/la/hollywood/old_hollywood/geom/block/la_hw_oh_blk_08a.mb"
// -file "N:/mc4/user/jude/la_hw_ww_ucla_01x.mb"
// -file "T:/mc4/tools/maya/8.0/user/projects/default/scenes/Tester_CrashingUVSets.mb"
// -file "T:/mc4/art/city/la/hollywood/beverly_hills/geom/block/la_hw_bh_rodeo_drive_01x.mb"
// -file "n:/mc4/user/kelby/shaderExampleScenes/duplicateNotWorking.mb"
// -file "T:/mc4/tools/maya/8.0/user/projects/default/scenes/Tester_MagicUVSets.mb"
// -file "T:/mc4/tools/maya/8.0/user/projects/default/scenes/Tester_MagicUVSets.ma"
// -file "T:/mc4/art/city/la/hollywood/west_hollywood/geom/block/la_hw_wh_paramount_studios_01x.mb"
// -file "T:/mc4/tools/maya/8.0/user/projects/default/scenes/Tester_MoveMyVerts.ma"
// -file "T:/mc4/art/city/la/beach/santa_monica_beach/geom/inst_facade/la_inst_be_smb_brentwoodhouse_01a.mb"
// -file "T:/mc4/tools/maya/8.0/user/projects/default/scenes/Tester_CityDefault.mb"
// -file "T:/mc4/tools/maya/8.0/user/projects/default/scenes/Tester_VertexColours.mb"
// -file "N:/mc4/user/kelby/RSM_testing/la_hw_ss_blk_04a_badStuff.mb"
// -file "n:/mc4/user/kelby/shaderExampleScenes/extraCPVNotWorking.mb"
// -file "N:/mc4/user/kelby/amoebaAndBlk21/together.mb"
// -command "file -loadReferenceDepth \"none\" -open \"n:/mc4/user/jude/la_hw_ww_ucla_01x.mb\""
// -file "T:/mc4/tools/maya/8.0/user/projects/default/scenes/Tester_CrashingUVSets2.mb"
// -file "N:/mc4/user/kelby/RSM_testing/la_hw_oh_blk_08a.mb"
// -file "T:/mc4/art/city/la/beach/santa_monica_city/geom/block/la_be_smc_blk_05x.mb"
// -file "T:/mc4/tools/maya/8.0/user/projects/default/scenes/Tester_WrongUVSets.mb"
// -file "T:/mc4/tools/maya/8.0/user/projects/default/scenes/Tester_BlackMeshes.mb"
// -file "T:/mc4/art/TechArt/geom/texOverrideTest.mb"
// -file "N:/mc4/user/kelby/RSM_testing/planarMapCrash.mb"
// -file "N:/mc4/user/kelby/RSM_testing/smc_16_texturedCrashesMaya.mb"
// -file "T:/mc4/art/city/la/beach/santa_monica_city/geom/block/la_be_smc_blk_02x.mb"
// -file "n:/mc4/user/jude/la_hw_ww_ucla_01x.mb"
// 
/*

Hide meshes:

string $astrMeshes[] = `ls -long -type "mesh"`;
for($strMesh in $astrMeshes)
{
	setAttr ($strMesh +".visibility") 0;

}

Show meshes:

int $iFile = `fopen "c:/meshes.txt" "w"`;
fprint $iFile ("Wibble\n");
fclose $iFile;
string $astrMeshes[] = `ls -long -type "mesh"`;
for($strMesh in $astrMeshes)
{
	trace("setAttr "+ $strMesh +".visibility 1");
	int $iFile = `fopen "c:/meshes.txt" "a"`;
	fprint $iFile ("setAttr "+ $strMesh +".visibility 1\n");
	fclose $iFile;
	setAttr ($strMesh +".visibility") 1;
	refresh -force;
}


*/
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <maya/MStatus.h>
#include <maya/MGlobal.h>
#include <maya/MHWShaderSwatchGenerator.h>
#pragma warning(push, 3)
#include <maya/MFnPlugin.h>
#pragma warning(pop)

#include	"system/param.h"

#include "rageShaderMaya65.h"
#include "rageShaderCommand.h"
#include "rageShaderMaterialImporter.h"
#include "UserSettings.h"

#include <diag/tracker_file.h>
#include <diag/tracker.h>

#pragma warning(error: 4668)

using namespace rage;

extern bool g_bDisplayMGlobalExecuteCommands;


namespace rage
{
	XPARAM(noquits);
	XPARAM(nopopups);
#if __WIN32PC && __ASSERT
	XPARAM(emailasserts);
#endif
}

//////////////////////////////////////////////////////////////

MStatus initializePlugin( MObject obj )
{
	//	MGlobal::displayInfo("initializePlugin( MObject obj )");

	// Make it so asserts do not kill Maya
	static const char* gTrueVal = {"true"};
#if __OPTIMIZED
	rage::PARAM_noquits.Set(gTrueVal);
	if(UserSettings::SilentRunning())
	{
		rage::PARAM_nopopups.Set(gTrueVal);
	}
#endif
#if __WIN32PC && __ASSERT
	rage::PARAM_emailasserts.Set("krose@rockstarsandiego.com");
#endif

	// Init things
	rageShaderMaya::initalizePlugin();
	
	MFnPlugin plugin( obj, "Alias|Wavefront", "6.5", "Any");
	const MString& swatchName =	MHWShaderSwatchGenerator::initialize();
	const MString UserClassify( "shader/surface/utility/:swatch/"+swatchName );

	// Register the translator with the system
	plugin.registerNode("rageShaderMaya", 
		rageShaderMaya::id, 
		rageShaderMaya::creator, 
		rageShaderMaya::initialize, 
		MPxNode::kHwShaderNode,
		&UserClassify );

	// Register the translator with the system
	plugin.registerFileTranslator("rageShaderMaterialImporter", "none", rageShaderMaterialImporter::creator);

	// Register the command with the system
	plugin.registerCommand("rageShaderCommand", rageShaderCommand::creator);

	// Execute the plugin
	MString command = "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"";
	command += UserClassify;
	command += "\");}\n";
	// MGlobal::displayInfo(command);
	MGlobal::executeCommand( command, g_bDisplayMGlobalExecuteCommands );

	return( MS::kSuccess );
}
//////////////////////////////////////////////////////////////

MStatus uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );
	plugin.deregisterNode( rageShaderMaya::id );

	plugin.deregisterCommand("rageShaderCommand");

	plugin.deregisterFileTranslator("rageShaderMaterialImporter");

	const MString UserClassify( "shader/surface" );
	MString command( "if( `window -exists createRenderNodeWindow` ) {refreshCreateRenderNodeWindow(\"" );
	command += UserClassify;
	command += "\");}\n";
	MGlobal::executeCommand( command, g_bDisplayMGlobalExecuteCommands );

	rageShaderMaya::uninitalizePlugin();

	return( MS::kSuccess );
}
