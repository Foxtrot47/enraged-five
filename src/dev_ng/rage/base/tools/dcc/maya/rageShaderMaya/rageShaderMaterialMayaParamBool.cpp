// 
// rageShaderMaterial/rageShaderMaterialMayaParamBool.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if 0
#include "file/token.h"
#include "file/stream.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnNumericAttribute.h>
#include <maya/MDGModifier.h>
#pragma warning(pop)

#include "rageShaderMaterialMayaParamBool.h"

using namespace rage;

bool rageShaderMaterialMayaParamBool::WriteScriptMayaInit(const shaderMaterialGeoParamValueBool* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	T.StartLine();
	T.PutStr("setAttr (%s + \".%s\") %d;", nodeNameMaya, rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), pobParam->GetValue());
	T.EndLine();

	return true;
}

bool rageShaderMaterialMayaParamBool::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueBool* pobParam)
{
	MFnNumericAttribute nAttr;
	MStatus status;
	MFnDependencyNode obFnFileNode(obNode);
	if(!obFnFileNode.hasAttribute(rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar()))
	{
		MObject obAttribute = nAttr.create( rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()) +"s",	MFnNumericData::kBoolean, pobParam->GetValue(), &status );
		CHECK_STATUS( status );
		CHECK_STATUS( nAttr.setStorable( true ) );
		CHECK_STATUS( nAttr.setHidden( true ) );
		CHECK_STATUS( nAttr.setReadable( true ) );
		CHECK_STATUS( nAttr.setWritable( true ) );
		CHECK_STATUS( nAttr.setInternal( true ) );

		MDGModifier obModifier;
		CHECK_STATUS(obModifier.addAttribute(obNode, obAttribute));
		CHECK_STATUS(obModifier.doIt());
	}
	return true;
}

bool rageShaderMaterialMayaParamBool::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueBool* pobParam)
{
	bool retval = false;
	MFnDependencyNode obFnFileNode(obNode);
	MStatus result;
	MPlug intPlug = obFnFileNode.findPlug(rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), &result);
	if (result == MStatus::kSuccess)
	{
		bool value;
		intPlug.getValue(value);
		if (value == pobParam->GetValue())
		{
			retval = true;
		}
	}
	return retval;
}
#endif
