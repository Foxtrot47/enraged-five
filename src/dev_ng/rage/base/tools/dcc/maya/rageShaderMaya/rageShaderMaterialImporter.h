// 
// /rageShaderMaterialImporter.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_MATERIAL_IMPORTER_H
#define RAGE_SHADER_MATERIAL_IMPORTER_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxFileTranslator.h>
#pragma warning(error: 4668)

///////////////////////////////////////////
// rageShaderMaterialImporter Class //
///////////////////////////////////////////

namespace rage {

class rageShaderMaterialImporter : public MPxFileTranslator
{
public:
	rageShaderMaterialImporter();
	virtual         ~rageShaderMaterialImporter();

	static  void *	creator();

	virtual MStatus  reader ( const MFileObject & file, const MString & optionsString, FileAccessMode mode);

	virtual MPxFileTranslator::MFileKind  identifyFile ( const MFileObject & file, const char* buffer, short size) const;

	virtual bool  haveReadMethod () const {return true;}

	virtual bool  haveWriteMethod () const {return false;}

	virtual bool  haveNamespaceSupport () const {return false;}

	virtual bool  haveReferenceMethod () const {return false;}

	virtual MString  defaultExtension () const {return "mtl";}

	virtual MString  filter () const {return "*.mtl";}

	virtual bool  canBeOpened () const {return true;}

private:

};

} // end using namespace rage

#endif /* GEN_RAGE_SHADER_MATERIAL_MAYA65_H */
