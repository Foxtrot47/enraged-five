// 
// rageShaderMaterial/rageShaderMaterialMayaParamVector2.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_MATERIAL_MayaParam_VECTOR2_H
#define RAGE_SHADER_MATERIAL_MayaParam_VECTOR2_H

#include "rageShaderMaterialMayaParam.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector2.h"

namespace rage {

class rageShaderMaterialMayaParamVector2
{
public:
	static bool WriteScriptMayaUi(const shaderMaterialGeoParamValueVector2* pobParam, fiTokenizer & T, const char * nodeNameMaya);
	static bool WriteScriptMayaInit(const shaderMaterialGeoParamValueVector2* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueVector2* pobParam);

	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueVector2* pobParam);
};

} // end namespace rage

#endif
