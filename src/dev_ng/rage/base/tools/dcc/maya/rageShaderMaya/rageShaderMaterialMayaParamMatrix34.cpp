// 
// rageShaderMaterial/rageShaderMaterialMayaParamMatrix34.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "file/token.h"
#include "file/stream.h"
#include "rageShaderMaterialMayaParamMatrix34.h"

using namespace rage;

extern bool g_bDisplayMGlobalExecuteCommands;

bool rageShaderMaterialMayaParamMatrix34::WriteScriptMayaUi(const shaderMaterialGeoParamValueMatrix34* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	T.StartLine();
	T.PutStr("floatFieldGrp -label \"%s A\" -numberOfFields 3 \"m34A\";", sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 2 \"m34A\" (%s + \".%sAX\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 3 \"m34A\" (%s + \".%sAY\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 4 \"m34A\" (%s + \".%sAZ\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("floatFieldGrp -label \"B\" -numberOfFields 3 \"m34B\";");
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 2 \"m34B\" (%s + \".%sBX\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 3 \"m34B\" (%s + \".%sBY\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 4 \"m34B\" (%s + \".%sBZ\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("floatFieldGrp -label \"C\" -numberOfFields 3 \"m34C\";");
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 2 \"m34C\" (%s + \".%sCX\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 3 \"m34C\" (%s + \".%sCY\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 4 \"m34C\" (%s + \".%sCZ\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("floatFieldGrp -label \"D\" -numberOfFields 3 \"m34D\";");
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 2 \"m34D\" (%s + \".%sDX\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 3 \"m34D\" (%s + \".%sDY\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 4 \"m34D\" (%s + \".%sDZ\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	return true;
}

bool rageShaderMaterialMayaParamMatrix34::WriteScriptMayaInit(const shaderMaterialGeoParamValueMatrix34* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	T.StartLine();
	T.PutStr("setAttr -type float3 (%s + \".%sA\") %f %f %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().a.x, pobParam->GetValue().a.y, pobParam->GetValue().a.z);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr -type float3 (%s + \".%sB\") %f %f %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().b.x, pobParam->GetValue().b.y, pobParam->GetValue().b.z);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr -type float3 (%s + \".%sC\") %f %f %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().c.x, pobParam->GetValue().c.y, pobParam->GetValue().c.z);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr -type float3 (%s + \".%sD\") %f %f %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().d.x, pobParam->GetValue().d.y, pobParam->GetValue().d.z);
	T.EndLine();

	return true;
}

bool rageShaderMaterialMayaParamMatrix34::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueMatrix34* pobParam)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	char acBuffer[1024];
	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sA -is true -h false -s false -at float3 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sAX -is true -h false -s false -at \"float\" -dv 1.0 -parent %sA %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sAY -is true -h false -s false -at \"float\" -dv 0.0 -parent %sA %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sAZ -is true -h false -s false -at \"float\" -dv 0.0 -parent %sA %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sB -is true -h false -s false -at float3 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sBX -is true -h false -s false -at \"float\" -dv 1.0 -parent %sB %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sBY -is true -h false -s false -at \"float\" -dv 0.0 -parent %sB %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sBZ -is true -h false -s false -at \"float\" -dv 0.0 -parent %sB %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sC -is true -h false -s false -at float3 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sCX -is true -h false -s false -at \"float\" -dv 1.0 -parent %sC %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sCY -is true -h false -s false -at \"float\" -dv 0.0 -parent %sC %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sCZ -is true -h false -s false -at \"float\" -dv 0.0 -parent %sC %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sD -is true -h false -s false -at float3 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sDX -is true -h false -s false -at \"float\" -dv 1.0 -parent %sD %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sDY -is true -h false -s false -at \"float\" -dv 0.0 -parent %sD %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sDZ -is true -h false -s false -at \"float\" -dv 0.0 -parent %sD %s;", sParamName.asChar(), sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);
	return true;
}

bool rageShaderMaterialMayaParamMatrix34::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueMatrix34* pobParam)
{
	bool retval = false;
	MFnDependencyNode obFnFileNode(obNode);
	MStatus result;
	Matrix34 param = pobParam->GetValue();
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	MPlug plug = obFnFileNode.findPlug((sParamName + "AX"), &result);
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == param.a.x)
		{
			retval = true;
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "AY"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.a.y)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "AZ"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.a.z)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "BX"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.b.x)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "BY"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.b.y)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "BZ"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.b.z)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "CX"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.c.x)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "CY"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.c.y)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "CZ"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.c.z)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "DX"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.d.x)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "DY"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.d.y)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "DZ"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.d.z)
			{
				retval = true;
			}
		}
	}
	return retval;
}

