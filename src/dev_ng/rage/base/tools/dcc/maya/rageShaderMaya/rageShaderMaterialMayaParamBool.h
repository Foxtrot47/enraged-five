// 
// rageShaderMaterial/rageShaderMaterialMayaParamBool.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if 0
#ifndef RAGE_SHADER_MATERIAL_MayaParam_BOOL_H
#define RAGE_SHADER_MATERIAL_MayaParam_BOOL_H

#include "rageShaderMaterialMayaParam.h"
#include "shaderMaterial/shaderMaterialGeoParamValueBool.h"

namespace rage {

class rageShaderMaterialMayaParamBool
{
public:
	static bool WriteScriptMayaInit(const shaderMaterialGeoParamValueBool* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueBool* pobParam);

	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueBool* pobParam);
};

} // end namespace rage

#endif
#endif
