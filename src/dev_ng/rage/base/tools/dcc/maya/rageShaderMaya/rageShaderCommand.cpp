// 
// /rageShaderCommand.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include "maya/MString.h"
#include "maya/MDoubleArray.h"
#include "maya/MIntArray.h"
#include "maya/MArgList.h"
#include "maya/MGlobal.h"
#pragma warning(pop)

#include "vector/vector3.h"

#include "rageShaderViewerLighting/lightDataManager.h"
#include "rageShaderViewer/rageShaderViewer.h"

#include "rageShaderUpdateManager.h"
#include "rageShaderCommand.h"
#include "rageShaderMaya65.h"

using namespace rage;

extern bool g_bDisplayMGlobalExecuteCommands;

rageShaderCommand::rageShaderCommand()
{
}

rageShaderCommand::~rageShaderCommand()
{
}

void * rageShaderCommand::creator()
{
	return new rageShaderCommand();
}

MStatus rageShaderCommand::doIt ( const MArgList & args )
{
	if (args.length() < 1)
	{
		return MStatus::kFailure;
	}

	bool queryMode = false;
	int startParam = 1;
	MString command = args.asString(0);
	if (command == "-q")
	{
		queryMode	= true;
		startParam	= 2;
		command		= args.asString(1);
	}

	if (command == "-TOD")
	{
		if (!queryMode)
		{
			MString parameter = args.asString(startParam);
			if (parameter=="1")
			{
				return setTODOn(true);
			}
			else if (parameter=="0")
			{
				return setTODOn(false);
			}
			else
			{
				return setTOD((float)args.asDouble(startParam));
			}
		}
		else
		{
			float tod = -1.0f;
			bool isTODOn = getTODOn();
			if (isTODOn)
			{
				tod = getTOD();
			}

			setResult((double)tod);
			return MStatus::kSuccess;
		}
	}
	else if (command == "-ignoreReferencedNodesDuringWireUpTextures")
	{
		if (!queryMode)
		{
			MString parameter = args.asString(startParam);
			if (parameter=="1")
			{
				rageShaderMaya::setIgnoreReferencedNodesDuringWireUpTextures(true);
				return MStatus::kSuccess;
			}
			else if (parameter=="0")
			{
				rageShaderMaya::setIgnoreReferencedNodesDuringWireUpTextures(false);
				return MStatus::kSuccess;
			}
		}
		else
		{
			setResult(rageShaderMaya::getIgnoreReferencedNodesDuringWireUpTextures());
			return MStatus::kSuccess;
		}
	}
	else if (command == "-TODCycle")
	{
		if (!queryMode)
		{
			MString parameter = args.asString(startParam);
			if (parameter=="1")
			{
				return setTODCycle(true);
			}
			else if (parameter=="0")
			{
				return setTODCycle(false);
			}
			else
			{
				return setTODCycleSpeed((float)args.asDouble(startParam));
			}
		}
		else
		{
			float todSpeed = -1.0f;
			bool isTODCycleOn = getTODCycle();
			if (isTODCycleOn)
			{
				todSpeed = getTODCycleSpeed();
			}

			setResult((double)todSpeed);
			return MStatus::kSuccess;
		}
	}
	else if (command == "-ambient")
	{
		if (!queryMode)
		{
			if (args.length() == 4)
			{
				Vector3 c;
				c.x = (float)args.asDouble(startParam);
				c.y = (float)args.asDouble(startParam+1);
				c.z = (float)args.asDouble(startParam+2);
				return setAmbient(c);
			}
		}
		else
		{
			Vector3 c = getAmbient();
			MDoubleArray da;
			da.setLength(3);
			da[0] = (double)c.x;
			da[1] = (double)c.y;
			da[2] = (double)c.z;
			setResult(da);
			return MStatus::kSuccess;
		}
	}
	else if (command == "-diffuse")
	{
		if (!queryMode)
		{
			if (args.length() == 4)
			{
				Vector3 c;
				c.x = (float)args.asDouble(startParam);
				c.y = (float)args.asDouble(startParam+1);
				c.z = (float)args.asDouble(startParam+2);
				return setDiffuse(c);
			}
		}
		else
		{
			Vector3 c = getDiffuse();
			MDoubleArray da;
			da.setLength(3);
			da[0] = (double)c.x;
			da[1] = (double)c.y;
			da[2] = (double)c.z;
			setResult(da);
			return MStatus::kSuccess;
		}
	}
	else if (command == "-background")
	{
		if (!queryMode)
		{
			if (args.length() == 4)
			{
				Vector3 c;
				c.x = (float)args.asDouble(startParam);
				c.y = (float)args.asDouble(startParam+1);
				c.z = (float)args.asDouble(startParam+2);
				return setBackground(c);
			}
		}
		else
		{
			Vector3 c = getBackground();
			MDoubleArray da;
			da.setLength(3);
			da[0] = (double)c.x;
			da[1] = (double)c.y;
			da[2] = (double)c.z;
			setResult(da);
			return MStatus::kSuccess;
		}
	}
	else if (command == "-viewerposition")
	{
		if (!queryMode)
		{
			if (args.length() == 5)
			{
				int data[4];
				data[0] = args.asInt(startParam);
				data[1] = args.asInt(startParam+1);
				data[2] = args.asInt(startParam+2);
				data[3] = args.asInt(startParam+3);
				return setViewerPosition(data);
			}
		}
		else
		{
			int data[4];
			getViewerPosition(data);
			MIntArray da;
			da.setLength(4);
			da[0] = data[0];
			da[1] = data[1];
			da[2] = data[2];
			da[3] = data[3];
			setResult(da);
			return MStatus::kSuccess;
		}
	}
	else if (command == "-global")
	{
		if (!queryMode)
		{
			MString parameter = args.asString(startParam);
			vector<float> aData;

			unsigned iArgsLeft = (args.length()-(startParam+1));
			for (unsigned i=0; i<iArgsLeft; i++)
			{
				aData.push_back((float)args.asDouble((startParam+1) + i));
			}

			if (!setGlobalParam(parameter.asChar(), aData))
			{
				MString result = "Could not set global parameter " + parameter;
				MGlobal::displayError(result);
				setResult(result);
				return MStatus::kFailure;
			}
		}
		else
		{
			MString result = "Query mode is not supported for this";
			MGlobal::displayError(result);
			setResult(result);
			return MStatus::kFailure;
		}
	}
	else if (command == "-listglobals")
	{
		if (!queryMode)
		{
			grcEffect::ComputeDefaultGlobals();
			MStringArray astrReturnMe;
			for(int i=0; i<grcEffect::GetGlobalVarCount(); i++)
			{
				const char* pcVarName = NULL;
				grcEffect::VarType obType;
				grcEffect::GetGlobalVarDesc(i, pcVarName, obType);
				astrReturnMe.append(pcVarName);
			}
			setResult(astrReturnMe);
			return MStatus::kSuccess;
		}
		else
		{
			MString result = "Query mode is not supported for this";
			MGlobal::displayError(result);
			setResult(result);
			return MStatus::kFailure;
		}
	}
	else if (command == "-new")
	{
		if (!queryMode)
		{
			MString parameter = args.asString(startParam);
			MString newRSMNode = newMaterial(parameter.asChar());
			setResult(newRSMNode);
			return MStatus::kSuccess;
		}
		else
		{
			MString result = "Query mode is not supported for this";
			setResult(result);
			return MStatus::kSuccess;
		}
	}
    else if (command == "-newwithtemplate" || command == "-newwithtype" || command == "-new")
	{
		if (!queryMode)
		{
			MString parameter = args.asString(startParam);
			if (parameter != "")
			{
				MString strNodeName;
				MString command;

				// Create the RSM node
				MGlobal::executeCommand("createRageShaderMayaNode", strNodeName, true);

				// Initalize the RSM node
				MString strCommand("newMtl "); 
				strCommand += parameter;
				IssueCommand(strNodeName.asChar(), strCommand.asChar());

				setResult(strNodeName);
				return MStatus::kSuccess;
			}	
		}
		else
		{
			MString result = "Query mode is not supported for this";
			setResult(result);
			return MStatus::kSuccess;
		}
			
	}
	else if (command == "-issueCommand")
	{
		MString strNodeName = args.asString(startParam);
		MString strCommand = args.asString(startParam + 1);
		for(unsigned p=(startParam + 2); p<args.length(); p++)
		{
			strCommand += " ";
			strCommand += args.asString(p);
		}
		setResult(IssueCommand(strNodeName.asChar(), strCommand.asChar()));
		return MStatus::kSuccess;
	}
	else if (command == "-getInstanceParams")
	{
		MString strNodeName = args.asString(startParam);
		MStringArray obAStrHiddenParams = GetParamNames(strNodeName, shaderMaterialGeoParamDescription::GeoInstance);
		setResult(obAStrHiddenParams);
		return MStatus::kSuccess;
	}
	else if (command == "-getTypeParams")
	{
		MString strNodeName = args.asString(startParam);
		MStringArray obAStrHiddenParams = GetParamNames(strNodeName, shaderMaterialGeoParamDescription::GeoType);
		setResult(obAStrHiddenParams);
		return MStatus::kSuccess;
	}
	else if (command == "-getTemplateParams")
	{
		MString strNodeName = args.asString(startParam);
		MStringArray obAStrHiddenParams = GetParamNames(strNodeName, shaderMaterialGeoParamDescription::GeoTemplate);
		setResult(obAStrHiddenParams);
		return MStatus::kSuccess;
	}
	else if (command == "-updateMeshes")
	{
		MayaModelMaterialPair::UpdateAllMayaModelMaterialPairsThatNeedUpdating();
		return MStatus::kSuccess;
	}
	else
	{
		MString result = "Unknown command " + command;
		MGlobal::displayError(result);
		setResult(result);
		return MStatus::kFailure;
	}

	return MStatus::kSuccess;
}

float rageShaderCommand::getTOD()
{
	if (lightDataManager::GetInstance())
	{
		return lightDataManager::GetInstance()->GetTimeOfDay();
	}

	return -1.0f;
}

bool rageShaderCommand::getTODOn()
{
	if (lightDataManager::GetInstance())
	{
		return !lightDataManager::GetInstance()->GetUseDefaultLighting();
	}

	return false;
}

bool rageShaderCommand::getTODCycle()
{
	if (lightDataManager::GetInstance())
	{
		return lightDataManager::GetInstance()->GetCycleTimeOfDay();
	}

	return false;
}

float rageShaderCommand::getTODCycleSpeed()
{
	if (lightDataManager::GetInstance())
	{
		return lightDataManager::GetInstance()->GetCycleSpeed();
	}

	return -1.0f;
}


Vector3 rageShaderCommand::getAmbient()
{
	if (lightDataManager::GetInstance())
	{
		return lightDataManager::GetInstance()->GetDefaultAmbient();
	}

	return Vector3(0,0,0);
}

Vector3 rageShaderCommand::getDiffuse()
{
	if (lightDataManager::GetInstance())
	{
		return lightDataManager::GetInstance()->GetDefaultDiffuse();
	}

	return Vector3(0,0,0);
}

Vector3 rageShaderCommand::getBackground()
{
	return rageShaderViewer::GetInstance()->GetBackGroundColor();
}

void rageShaderCommand::getViewerPosition(int * data)
{
	rageShaderViewer::GetInstance()->GetWindowPosition(data);
}

MStatus rageShaderCommand::setTOD(const float tod)
{
	if (lightDataManager::GetInstance())
	{
		lightDataManager::GetInstance()->SetTimeOfDay(tod);
	}

	return MStatus::kSuccess;
}

MStatus rageShaderCommand::setTODOn(const bool b)
{
	if (lightDataManager::GetInstance())
	{
		lightDataManager::GetInstance()->SetUseDefaultLighting(!b);
	}

	return MStatus::kSuccess;
}

MStatus rageShaderCommand::setTODCycle(const bool b)
{
	if (lightDataManager::GetInstance())
	{
		lightDataManager::GetInstance()->SetCycleTimeOfDay(b);
	}

	return MStatus::kSuccess;
}

MStatus rageShaderCommand::setTODCycleSpeed(const float f)
{
	if (lightDataManager::GetInstance())
	{
		lightDataManager::GetInstance()->SetCycleSpeed(f);
	}

	return MStatus::kSuccess;
}


MStatus rageShaderCommand::setAmbient(const Vector3 & c)
{
	if (lightDataManager::GetInstance())
	{
		lightDataManager::GetInstance()->SetDefaultAmbient(c);
	}

	return MStatus::kSuccess;
}

MStatus rageShaderCommand::setDiffuse(const Vector3 & c)
{
	if (lightDataManager::GetInstance())
	{
		lightDataManager::GetInstance()->SetDefaultDiffuse(c);
	}

	return MStatus::kSuccess;
}

MStatus rageShaderCommand::setBackground(const Vector3 & c)
{
	rageShaderViewer::GetInstance()->SetBackGroundColor(c);
	return MStatus::kSuccess;
}

MStatus rageShaderCommand::setViewerPosition(const int * data)
{
	rageShaderViewer::GetInstance()->SetWindowPosition(data);
	return MStatus::kSuccess;
}

MStatus rageShaderCommand::setGlobalParam(const char * paramName, const vector<float> & data)
{
	// Make sure the viewer is up and running
	rageShaderViewer::GetInstance();

	// Set the global var
	grcEffectGlobalVar var = grcEffect::LookupGlobalVar(paramName, false);
	if(var == grcegvNONE)
	{
		// Param does not exist
		return MS::kFailure;
	}
	grcEffect::SetGlobalVar(var, &data[0], data.size());
	return MStatus::kSuccess;
}

MString rageShaderCommand::newMaterial(const char * paramName)
{
	MString strNodeName;

	// Create the RSM node
	MGlobal::executeCommand("createRageShaderMayaNode", strNodeName, g_bDisplayMGlobalExecuteCommands);

	// Initalize the RSM node
	MString strCommand("loadMtl "); 
	strCommand += paramName;
	IssueCommand(strNodeName.asChar(), strCommand.asChar());

	return strNodeName;
}

MStringArray rageShaderCommand::IssueCommand(const char * pcNodeName, const char * pcCommand)
{
	rageShaderMaya* pobRageShaderMaya = rageShaderMaya::GetRageShaderMayaNodeGivenNodeName(pcNodeName);
	if(pobRageShaderMaya)
	{
		return pobRageShaderMaya->IssueCommand(pcCommand);
	}
	return MStringArray();
}

MStringArray rageShaderCommand::GetParamNames(const MString& strNodeName, shaderMaterialGeoParamDescription::ValueSource _SourceFlags)
{
	rageShaderMaya* pobRageShaderMaya = rageShaderMaya::GetRageShaderMayaNodeGivenNodeName(strNodeName.asChar());
	if(pobRageShaderMaya)
	{
		return pobRageShaderMaya->GetParamNames(_SourceFlags);
	}
	return (MStringArray());
}

