// 
// rageShaderMaterial/rageShaderMaterialMayaParamMatrix44.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "file/token.h"
#include "file/stream.h"
#include "rageShaderMaterialMayaParamMatrix44.h"

using namespace rage;

extern bool g_bDisplayMGlobalExecuteCommands;

bool rageShaderMaterialMayaParamMatrix44::WriteScriptMayaUi(const shaderMaterialGeoParamValueMatrix44* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	T.StartLine();
	T.PutStr("floatFieldGrp -label \"%s A\" -columnWidth5 132 52 52 53 53 -numberOfFields 4 \"m44A\";", sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 2 \"m44A\" (%s + \".%sAX\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 3 \"m44A\" (%s + \".%sAY\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 4 \"m44A\" (%s + \".%sAZ\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 5 \"m44A\" (%s + \".%sAW\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("floatFieldGrp -label \"B\" -columnWidth5 132 52 52 53 53 -numberOfFields 4 \"m44B\";");
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 2 \"m44B\" (%s + \".%sBX\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 3 \"m44B\" (%s + \".%sBY\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 4 \"m44B\" (%s + \".%sBZ\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 5 \"m44B\" (%s + \".%sBW\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("floatFieldGrp -label \"C\" -columnWidth5 132 52 52 53 53 -numberOfFields 4 \"m44C\";");
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 2 \"m44C\" (%s + \".%sCX\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 3 \"m44C\" (%s + \".%sCY\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 4 \"m44C\" (%s + \".%sCZ\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 5 \"m44C\" (%s + \".%sCW\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("floatFieldGrp -label \"D\" -columnWidth5 132 52 52 53 53 -numberOfFields 4 \"m44D\";");
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 2 \"m44D\" (%s + \".%sDX\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 3 \"m44D\" (%s + \".%sDY\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 4 \"m44D\" (%s + \".%sDZ\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	T.StartLine();
	T.PutStr("connectControl -index 5 \"m44D\" (%s + \".%sDW\");", nodeNameMaya, sParamName.asChar());
	T.EndLine();

	return true;
}

bool rageShaderMaterialMayaParamMatrix44::WriteScriptMayaInit(const shaderMaterialGeoParamValueMatrix44* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sAX\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().a.x);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sAY\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().a.y);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sAZ\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().a.z);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sAW\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().a.w);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sBX\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().b.x);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sBY\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().b.y);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sBZ\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().b.z);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sBW\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().b.w);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sCX\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().c.x);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sCY\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().c.y);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sCZ\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().c.z);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sCW\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().c.w);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sDX\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().d.x);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sDY\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().d.y);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sDZ\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().d.z);
	T.EndLine();

	T.StartLine();
	T.PutStr("setAttr (%s + \".%sDW\") %f;", nodeNameMaya, sParamName.asChar(), pobParam->GetValue().d.w);
	T.EndLine();

	return true;
}

bool rageShaderMaterialMayaParamMatrix44::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueMatrix44* pobParam)
{
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	char acBuffer[1024];
	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sAX -is true -h false -s false -at \"float\" -dv 1.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sAY -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sAZ -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sAW -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sBX -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sBY -is true -h false -s false -at \"float\" -dv 1.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sBZ -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sBW -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sCX -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sCY -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sCZ -is true -h false -s false -at \"float\" -dv 1.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sCW -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sDX -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sDY -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sDZ -is true -h false -s false -at \"float\" -dv 0.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %sDW -is true -h false -s false -at \"float\" -dv 1.0 %s;", sParamName.asChar(), MFnDependencyNode(obNode).name().asChar());
	MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

	return true;
}

bool rageShaderMaterialMayaParamMatrix44::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueMatrix44* pobParam)
{
	bool retval = false;
	MFnDependencyNode obFnFileNode(obNode);
	MStatus result;
	Matrix44 param = pobParam->GetValue();
	MString sParamName = rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName());

	MPlug plug = obFnFileNode.findPlug((sParamName + "AX"), &result);
	if (result == MStatus::kSuccess)
	{
		float value;
		plug.getValue(value);
		if (value == param.a.x)
		{
			retval = true;
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "AY"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.a.y)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "AZ"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.a.z)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "AW"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.a.w)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "BX"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.b.x)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "BY"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.b.y)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "BZ"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.b.z)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "BW"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.b.w)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "CX"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.c.x)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "CY"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.c.y)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "CZ"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.c.z)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "CW"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.c.w)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "DX"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.d.x)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "DY"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.d.y)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "DZ"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.d.z)
			{
				retval = true;
			}
		}
	}

	if (retval)
	{
		plug = obFnFileNode.findPlug((sParamName + "DW"), &result);
		if (result == MStatus::kSuccess)
		{
			float value;
			plug.getValue(value);
			if (value == param.d.w)
			{
				retval = true;
			}
		}
	}
	return retval;
}
