// 
// rageShaderMaterial/rageShaderMaterialMayaParamTexture.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnNumericAttribute.h>
#include <maya/MDGModifier.h>
#pragma warning(pop)

#include "file/token.h"
#include "file/stream.h"
#include "rageShaderMaterialMayaParamTexture.h"

using namespace rage;

bool rageShaderMaterialMayaParamTexture::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueTexture* pobParam)
{
	//dont add this as a valid attrib to connect too because it is a runtime created rendertarget
	if (!strncmp(rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), "rt_", 3))
	{
		return true;
	}

	// Create color input attributes
	MFnNumericAttribute nAttr;
	MStatus status;

	// Create color input attributes
	MFnDependencyNode obFnFileNode(obNode);
	if(!obFnFileNode.hasAttribute(rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar()))
	{
		MObject aColorR = nAttr.create( rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()) +"r", rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()) +"r",MFnNumericData::kFloat, 0, &status );
		CHECK_STATUS( status );
		CHECK_STATUS( nAttr.setKeyable( false ) );
		CHECK_STATUS( nAttr.setStorable( false ) );
		CHECK_STATUS( nAttr.setCached( false ) );
		CHECK_STATUS( nAttr.setHidden( false ) );
		CHECK_STATUS( nAttr.setDefault( 1.0f ) );
		CHECK_STATUS( nAttr.setRenderSource( true ) );

		MObject aColorG = nAttr.create( rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()) +"g", rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()) +"g", MFnNumericData::kFloat, 0, &status );
		CHECK_STATUS( status );
		CHECK_STATUS( nAttr.setKeyable( false ) );
		CHECK_STATUS( nAttr.setStorable( false ) );
		CHECK_STATUS( nAttr.setCached( false ) );
		CHECK_STATUS( nAttr.setHidden( false ) );
		CHECK_STATUS( nAttr.setDefault( 1.0f ) );
		CHECK_STATUS( nAttr.setRenderSource( true ) );

		MObject aColorB = nAttr.create( rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()) +"b", rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()) +"b",MFnNumericData::kFloat, 0, &status );
		CHECK_STATUS( status );
		CHECK_STATUS( nAttr.setKeyable( false ) );
		CHECK_STATUS( nAttr.setStorable( false ) );
		CHECK_STATUS( nAttr.setCached( false ) );
		CHECK_STATUS( nAttr.setHidden( false ) );
		CHECK_STATUS( nAttr.setDefault( 1.0f ) );
		CHECK_STATUS( nAttr.setRenderSource( true ) );

		MObject obColourAttribute = nAttr.create( rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), aColorR, aColorG, aColorB,	&status );
		CHECK_STATUS( status );
		CHECK_STATUS( nAttr.setInternal( true ) );
		CHECK_STATUS( nAttr.setKeyable( false ) );
		CHECK_STATUS( nAttr.setStorable( false ) );
		CHECK_STATUS( nAttr.setCached( false ) );
		CHECK_STATUS( nAttr.setHidden( false ) );
		CHECK_STATUS( nAttr.setDefault( 1.0f, 1.0f, 1.0f ) );
		CHECK_STATUS( nAttr.setRenderSource( true ) );
		CHECK_STATUS( nAttr.setUsedAsColor( true ) );

		MDGModifier obModifier;
		CHECK_STATUS(obModifier.addAttribute(obNode, obColourAttribute));
		CHECK_STATUS(obModifier.doIt());
	}

	return true;
}

bool rageShaderMaterialMayaParamTexture::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueTexture* pobParam)
{
	bool retval = false;
	MFnDependencyNode obFnThis(obNode);
	MPlug obInTexturePlug = obFnThis.findPlug(rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()));
	if (!obInTexturePlug.isNull())
	{
		// Get the file node on the other end of the plug
		if(obInTexturePlug.isConnected())
		{
			// It is connected, so get the other end of the connection
			MStatus obStatus;
			MPlugArray aobPlugsConnectedToParam;
			obInTexturePlug.connectedTo(aobPlugsConnectedToParam, true, true, &obStatus);
			CHECK_STATUS( obStatus )

				if(aobPlugsConnectedToParam.length() > 0)
				{
					// Update the parameter to match the connected pieces
					MString value;
					MFnDependencyNode obFnFileNode(aobPlugsConnectedToParam[0].node());
					MPlug filenamePlug = obFnFileNode.findPlug("fileTextureName");
					filenamePlug.getValue(value);
					if (!strcmp(value.asChar(),pobParam->GetValue()))
					{
						retval = true;
					}
				}
		}
	}
	return retval;
}
