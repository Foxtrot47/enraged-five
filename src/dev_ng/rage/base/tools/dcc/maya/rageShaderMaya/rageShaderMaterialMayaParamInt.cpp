// 
// rageShaderMaterial/rageShaderMaterialMayaParamInt.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#if 0
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnNumericAttribute.h>
#include <maya/MDGModifier.h>
#pragma warning(pop)

#include "file/token.h"
#include "file/stream.h"
#include "rageShaderMaterialMayaParamInt.h"

using namespace rage;

extern bool g_bDisplayMGlobalExecuteCommands;

bool rageShaderMaterialMayaParamInt::WriteScriptMayaInit(const shaderMaterialGeoParamValueInt* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	T.StartLine();
	T.PutStr("setAttr (%s + \".%s\") %d;", nodeNameMaya, rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), pobParam->GetValue());
	T.EndLine();

	return true;
}

bool rageShaderMaterialMayaParamInt::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueInt* pobParam)
{
	char acBuffer[1024];
	if (stricmp(pobParam->GetParamDescription()->GetUIWidget(), "menu")==0)
	{
		char szCommand[1024];
		memset(szCommand, 0, sizeof(szCommand));

		int nEnumValueCount = 0;
		fiStream * input = fiStream::Open(pobParam->GetParamDescription()->GetUIHint());
		if (input)
		{
			fiTokenizer T2(pobParam->GetParamDescription()->GetUIHint(), input);

			char szToken[256];
			T2.GetToken(szToken, sizeof(szToken));
			strcpy(szCommand, szToken);
			nEnumValueCount++;

			while(T2.GetToken(szToken, sizeof(szToken)))
			{
				strcat(szCommand, ":");
				strcat(szCommand, szToken);
				nEnumValueCount++;
			}

			input->Close();
			input = NULL;
		}

		formatf(acBuffer, sizeof(acBuffer), "addAttr -ln %s -is true -h false -s false -at enum -en \"%s\" %s;", rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), szCommand, MFnDependencyNode(obNode).name().asChar());
		MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);

		// Get the number of the default value
		int iDefaultEnumValue = pobParam->GetValue();
		MString strEnumValues = szCommand;
		
		if(iDefaultEnumValue >= 0 && iDefaultEnumValue < nEnumValueCount)
		{
			formatf(acBuffer, sizeof(acBuffer), "setAttr %s.%s %d", MFnDependencyNode(obNode).name().asChar(), rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), iDefaultEnumValue);
			MGlobal::executeCommand(acBuffer, g_bDisplayMGlobalExecuteCommands);		
		}
	}
	else
	{
		MFnDependencyNode obFnFileNode(obNode);
		if (!obFnFileNode.hasAttribute(rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar()))
		{
			MFnNumericAttribute nAttr;
			MStatus status;
			MObject obAttribute = nAttr.create( rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()) +"s",	MFnNumericData::kInt, 0, &status );
			CHECK_STATUS( status );
			CHECK_STATUS( nAttr.setStorable( true ) );
			CHECK_STATUS( nAttr.setHidden( true ) );
			CHECK_STATUS( nAttr.setReadable( true ) );
			CHECK_STATUS( nAttr.setWritable( true ) );
			CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
			CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
			CHECK_STATUS( nAttr.setDefault( pobParam->GetValue() ) );
			CHECK_STATUS( nAttr.setInternal( true ) );

			MDGModifier obModifier;
			CHECK_STATUS(obModifier.addAttribute(obNode, obAttribute));
			CHECK_STATUS(obModifier.doIt());
		}
	}
	
	return true;
}

bool rageShaderMaterialMayaParamInt::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueInt* pobParam)
{
	bool retval = false;
	MFnDependencyNode obFnFileNode(obNode);
	MStatus result;
	MPlug intPlug = obFnFileNode.findPlug(rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), &result);
	if (result == MStatus::kSuccess)
	{
		int value;
		intPlug.getValue(value);
		if (value == pobParam->GetValue())
		{
			retval = true;
		}
	}
	return retval;
}
#endif
