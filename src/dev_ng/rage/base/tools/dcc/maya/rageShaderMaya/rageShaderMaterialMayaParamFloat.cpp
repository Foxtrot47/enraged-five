// 
// rageShaderMaterial/rageShaderMaterialMayaParamFloat.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnNumericAttribute.h>
#include <maya/MDGModifier.h>
#include <maya/MPlug.h>
#pragma warning(pop)


#include "file/token.h"
#include "file/stream.h"
#include "rageShaderMaterialMayaParamFloat.h"

using namespace rage;


bool rageShaderMaterialMayaParamFloat::WriteScriptMayaInit(const shaderMaterialGeoParamValueFloat* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	

	T.StartLine();
	T.PutStr("setAttr (%s + \".%s\") %f;", nodeNameMaya, rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), pobParam->GetValue());
	T.EndLine();

	return true;
}

bool rageShaderMaterialMayaParamFloat::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueFloat* pobParam)
{
	// Does the attribute already exist?
	MStatus obStatus;
	MFnDependencyNode obFnNode(obNode);
	MPlug obExistingPlug = obFnNode.findPlug(pobParam->GetName(), &obStatus);
	if((obStatus != MS::kSuccess) || obExistingPlug.isNull())
	{
		MFnNumericAttribute nAttr;
		MStatus status;
		MFnDependencyNode obFnFileNode(obNode);
		if(!obFnFileNode.hasAttribute(rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar()))
		{
			// Displayf("%s Min = %f Max = %f", rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), pobParam->GetParamDescription()->GetUIMin(), pobParam->GetParamDescription()->GetUIMax());
			MObject obAttribute = nAttr.create( rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()) +"s",	MFnNumericData::kFloat, 0, &status );
			CHECK_STATUS( status );
			CHECK_STATUS( nAttr.setStorable( true ) );
			CHECK_STATUS( nAttr.setHidden( true ) );
			CHECK_STATUS( nAttr.setReadable( true ) );
			CHECK_STATUS( nAttr.setWritable( true ) );
			CHECK_STATUS( nAttr.setMin( pobParam->GetParamDescription()->GetUIMin() ) );
			CHECK_STATUS( nAttr.setMax( pobParam->GetParamDescription()->GetUIMax() ) );
			CHECK_STATUS( nAttr.setDefault( pobParam->GetValue() ) );
			CHECK_STATUS( nAttr.setInternal( true ) );

			MDGModifier obModifier;
			CHECK_STATUS(obModifier.addAttribute(obNode, obAttribute));
			CHECK_STATUS(obModifier.doIt());
		}
	}
	return true;
}

bool rageShaderMaterialMayaParamFloat::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueFloat* pobParam)
{
	bool retval = false;
	MFnDependencyNode obFnFileNode(obNode);
	MStatus result;
	MPlug intPlug = obFnFileNode.findPlug(rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar(), &result);
	if (result == MStatus::kSuccess)
	{
		float value;
		intPlug.getValue(value);
		if (value == pobParam->GetValue())
		{
			retval = true;
		}
	}
	return retval;
}
