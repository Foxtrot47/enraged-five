// 
// rageShaderMaya65/rageShaderUpdateManager.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#include <process.h>

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MDagPath.h>
#include <maya/MFnMesh.h>
#include <maya/MFnSet.h>
#include <maya/M3dView.h>
#include <maya/MItDag.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MMatrix.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MGlobal.h>
#include <maya/MFnCamera.h>
#include <maya/MFnPointLight.h>
#include <maya/MFnDirectionalLight.h>
#pragma warning(error: 4668)

#pragma comment(lib, "opengl32.lib")
// #pragma comment(lib, "cg.lib")
// #pragma comment(lib, "cgGL.lib")
// #pragma comment(lib, "Cloth.lib")
#pragma comment(lib, "Foundation.lib")
// #pragma comment(lib, "Image.lib")
// #pragma comment(lib, "IMF.lib")
// #pragma comment(lib, "libfbxfilesdk.lib")
// #pragma comment(lib, "libmocap.lib")
#pragma comment(lib, "OpenMaya.lib")
// #pragma comment(lib, "OpenMayaAnim.lib")
// #pragma comment(lib, "OpenMayaFX.lib")
#pragma comment(lib, "OpenMayaRender.lib")
#pragma comment(lib, "OpenMayaUI.lib")

#include "system/timer.h"
#include "system/wndproc.h"
#include "mesh/mesh.h"
#include "mesh/serialize.h"
#include "file/device.h"
#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"

#include "rageShaderMaterial/rageShaderMaterial.h"
#include "rageShaderMaterial/rageShaderMaterialGroup.h"
#include "shaderMaterial/shaderMaterialGeoParamValueTexture.h"
#include "rageShaderViewer/rageShaderViewer.h"
#include "rageShaderViewer/ModelMaterialPairManager.h"

#include "rageShaderMaya65.h"
#include "rageShaderUpdateManager.h"

using namespace rage;

extern bool g_bDisplayMGlobalExecuteCommands;

#define CHECK_STATUS(x)	\
{ \
	MStatus _maya_status = (x);	\
	if ( MStatus::kSuccess != _maya_status ) \
{ \
	MString obStrErrorMessage(__FILE__); \
	obStrErrorMessage += ":"; \
	obStrErrorMessage += __LINE__; \
	obStrErrorMessage += ":"; \
	_maya_status.perror (obStrErrorMessage); \
} \
} \

#define DEG2RAD (PI / 180.0f)
#define RAD2DEG (180.0f / PI)


rageShaderUpdateManager * rageShaderUpdateManager::sm_pInstance = NULL;

ConstString rageShaderUpdateManager::sm_sRageShadersAssetsFolder;

rageShaderUpdateData::rageShaderUpdateData(rageShaderMaya * pobMayaNode)
{
	m_boIsModifying		= false;
	m_boIsUpdating		= false;
	m_bRemoveAll		= false;
	m_bUpdateShader		= false;
	m_pobMayaNode		= pobMayaNode;
}

rageShaderUpdateData::~rageShaderUpdateData()
{
	for(int m=0; m<m_aMayaModelMaterialPairs.GetCount(); m++)
	{
		delete m_aMayaModelMaterialPairs[m];
		m_aMayaModelMaterialPairs[m] = NULL;
	}
	m_aMayaModelMaterialPairs.Reset();

	m_pobMayaNode = NULL;
}

void rageShaderUpdateData::ReleaseConnectedMesh(const MObject & obMeshNode, const MString & sMeshNodeName)
{
	m_boIsModifying = true;

	// We have to make sure the name matches
	for(int m=0; m<m_aMayaModelMaterialPairs.GetCount(); m++)
	{
		if(m_aMayaModelMaterialPairs[m]->GetMeshNode() == obMeshNode)
		{
			m_aMayaModelMaterialPairs[m]->SetMeshNodeName(sMeshNodeName);
		}
	}

	while (m_boIsUpdating)
	{
		Sleep(100);
	}

	// Make sure we are not try to add this mesh
	int nIndex = m_asAddMeshNodeNames.Find(sMeshNodeName);
	if (nIndex >= 0)
	{
		m_asAddMeshNodeNames.Delete(nIndex);
	}
	else
	{
		if (m_asRemoveMeshNodeNames.Find(sMeshNodeName)<0)
			m_asRemoveMeshNodeNames.Grow()	= sMeshNodeName;
	}

	m_boIsModifying = false;
}

void rageShaderUpdateData::AddModelMaterialPair(const MString & sMeshNodeName)
{
	m_boIsModifying = true;

	while (m_boIsUpdating)
	{
		Sleep(100);
	}

	// Make sure we are not try to add this mesh
	int nIndex = m_asRemoveMeshNodeNames.Find(sMeshNodeName);
	if (nIndex >= 0)
	{
		m_asRemoveMeshNodeNames.Delete(nIndex);
	}

	if (m_asAddMeshNodeNames.Find(sMeshNodeName)<0)
		m_asAddMeshNodeNames.Grow()	= sMeshNodeName;

	m_boIsModifying = false;
}

void rageShaderUpdateData::DeleteModelMaterialPair(const MString & sMeshNodeName)
{
	m_boIsModifying = true;

	while (m_boIsUpdating)
	{
		Sleep(100);
	}

	//m_bRemoveAll = true;
	
	// Make sure we are not try to add this mesh
	int nIndex = m_asAddMeshNodeNames.Find(sMeshNodeName);
	if (nIndex >= 0)
	{
		m_asAddMeshNodeNames.Delete(nIndex);
	}

	if (m_asRemoveMeshNodeNames.Find(sMeshNodeName)<0)
		m_asRemoveMeshNodeNames.Grow() = sMeshNodeName;

	m_boIsModifying = false;
}

void rageShaderUpdateData::UpdateShader()
{
	m_boIsModifying = true;

	while (m_boIsUpdating)
	{
		Sleep(100);
	}

	m_bUpdateShader = true;
	m_boIsModifying = false;
}

void rageShaderUpdateData::Update()
{
	while (m_boIsModifying)
	{
		Sleep(100);
	}

	m_boIsUpdating = true;

	// Check to see if we are removing all of the them first
	/*
	if (m_bRemoveAll)
	{
		for(int m=0; m<m_aMayaModelMaterialPairs.GetCount(); m++)
		{
			//Displayf("Deleting %d/%d", m, m_aMayaModelMaterialPairs.GetCount());
			MayaModelMaterialPair* pobDeleteMe = m_aMayaModelMaterialPairs[m];
			//Displayf("pobDeleteMe = %p", pobDeleteMe);
			delete pobDeleteMe;
			//Displayf("Deleted = %p", pobDeleteMe);
			m_aMayaModelMaterialPairs[m] = NULL;
		}
		m_aMayaModelMaterialPairs.Reset();

		m_bRemoveAll = false;
	}
	*/

	if (m_bUpdateShader)
	{
		for(int m=0; m<m_aMayaModelMaterialPairs.GetCount(); m++)
		{
			m_aMayaModelMaterialPairs[m]->UpdateShader();
		}
		m_bUpdateShader = false;
	}

	while (m_asAddMeshNodeNames.GetCount())
	{
		MObject obMeshNode;
		MItDag dagIterator(MItDag::kDepthFirst, MFn::kMesh);
		while (!dagIterator.isDone())
		{
			if (dagIterator.fullPathName() == m_asAddMeshNodeNames[0])
			{
				obMeshNode = dagIterator.currentItem();
				break;
			}

			dagIterator.next();
		}

		if (!obMeshNode.isNull())
		{
			// Make sure it is not currently here we don't want duplicates
			bool bAlreadyHaveModelMaterialPair = false;
			for(int m=0; m<m_aMayaModelMaterialPairs.GetCount(); m++)
			{
				if(m_aMayaModelMaterialPairs[m]->GetMeshNode() == obMeshNode)
				{
					bAlreadyHaveModelMaterialPair = true;
					break;
				}
			}
			if(!bAlreadyHaveModelMaterialPair)
			{
				MayaModelMaterialPair* pobMayaModelMaterialPair = new MayaModelMaterialPair(m_asAddMeshNodeNames[0], obMeshNode, m_pobMayaNode->thisMObject(), m_pobMayaNode->GetRageMaterial());
				m_aMayaModelMaterialPairs.Grow() = pobMayaModelMaterialPair;
			}
		}

		m_asAddMeshNodeNames.Delete(0);
	}

	while (m_asRemoveMeshNodeNames.GetCount())
	{
		for(int m=0; m<m_aMayaModelMaterialPairs.GetCount(); m++)
		{
			if (m_aMayaModelMaterialPairs[m]->GetMeshNodeName() == m_asRemoveMeshNodeNames[0])
			{
				//Displayf("Deleting %d/%d", m, m_aMayaModelMaterialPairs.GetCount());
				MayaModelMaterialPair* pobDeleteMe = m_aMayaModelMaterialPairs[m];
				//Displayf("pobDeleteMe = %p", pobDeleteMe);
				delete pobDeleteMe;
				//Displayf("Deleted = %p", pobDeleteMe);
				m_aMayaModelMaterialPairs[m] = NULL;
				m_aMayaModelMaterialPairs.Delete(m);
				m--;
			}
		}
		

		m_asRemoveMeshNodeNames.Delete(0);
	}

	m_boIsUpdating = false;
}

///////////////////////////////////////////////////////
// DESCRIPTION:
// Constructor
//
///////////////////////////////////////////////////////

rageShaderUpdateManager::rageShaderUpdateManager()
{
	sm_pInstance	= this;
	m_bDone			= false;
	m_bExit			= false;
	m_nThreadID		= 0;
	m_hThread		= NULL;
}

///////////////////////////////////////////////////////
// DESCRIPTION:
// Destructor
//
///////////////////////////////////////////////////////

rageShaderUpdateManager::~rageShaderUpdateManager()
{
	sm_pInstance = NULL;

	// Delete all the update data
	for (int i=0; i<m_apUpdateData.GetCount(); i++)
	{
		delete m_apUpdateData[i];
		m_apUpdateData[i] = NULL;
	}
	m_apUpdateData.Reset();

	// Delete the shader viewer
	if (rageShaderViewer::GetInstance())
	{
		// Make sure the thread has stopped before we delete
		static int count = 0;
		while(rageShaderViewer::GetInstance()->ViewerIsActive() && !rageShaderViewer::GetInstance()->IsDone() && count < 200)
		{
			// wait for 100 milli seconds
			Sleep(100);
			count++;
		}
	}
}

rageShaderUpdateData *	rageShaderUpdateManager::CreateUpdateData(rageShaderMaya * pobMayaNode)
{
	if (!sm_pInstance)
		return NULL;

	rageShaderUpdateData * pUpdateData = new rageShaderUpdateData(pobMayaNode);
	sm_pInstance->m_apUpdateData.Grow() = pUpdateData;

	return pUpdateData;
}
void					rageShaderUpdateManager::ReleaseUpdateData(const rageShaderUpdateData * pobUpdateData)
{
	if (!sm_pInstance)
		return;

	for (int i=0; i<sm_pInstance->m_apUpdateData.GetCount(); i++)
	{
		if (sm_pInstance->m_apUpdateData[i]==pobUpdateData)
		{
			delete sm_pInstance->m_apUpdateData[i];
			sm_pInstance->m_apUpdateData[i] = NULL;
			sm_pInstance->m_apUpdateData.Delete(i);
			return;
		}
	}
}

///////////////////////////////////////////////////////
// DESCRIPTION:
// This function is the call back from the function RunThread()
//
// The function is the call back function to when the thread is
// started.
//
///////////////////////////////////////////////////////

u32 __stdcall rageShaderUpdateManager::StaticRunThread( void* pParam )
{
	rageShaderUpdateManager* pUpdate = (rageShaderUpdateManager*) pParam;
	return pUpdate->Main();
}

///////////////////////////////////////////////////////
// DESCRIPTION:
// This function is the called from the Main() function.
//
// The function initializes anything that the update class
// needs.
//
///////////////////////////////////////////////////////

void rageShaderUpdateManager::Init()
{
	sm_sRageShadersAssetsFolder = "";
	sm_sRageShadersAssetsFolder = ""; 
}

///////////////////////////////////////////////////////
// DESCRIPTION:
// This function gets called by StaticRunThread()
//
// The function is the main function for this thread
//
///////////////////////////////////////////////////////

u32 __stdcall rageShaderUpdateManager::Main()
{
	try
	{
		Init();

		// Make sure we initialized the viewer
		if (!rageShaderViewer::GetInstance())
		{
			return (0);
		}

		m_bDone = false;
		m_bExit = false;

		sysTimer updateTimer;

		do
		{
			// Update the data
			for (int i=0; i<m_apUpdateData.GetCount(); i++)
			{
				m_apUpdateData[i]->Update();
			}

			// Remove everything from the viewer
			if (rageShaderViewer::GetInstance()->ViewerWantsToClose())
			{
				MGlobal::executeCommand("removeAllFromViewer", g_bDisplayMGlobalExecuteCommands);

				//this function called so we don't keep executing "removeAllFromViewer" forever.
				rageShaderViewer::GetInstance()->ResetViewerWantsToClose();
			}

			if (rageShaderViewer::GetInstance()->ViewerIsActive())
			{
				// the view is up and running
				updateTimer.Reset();

				// Update the camera
				updateCamera();

				// Update the lights
				updateLights();

				// Clamp to 10fps
				float fUpdateTime = updateTimer.GetMsTime();
				if (fUpdateTime < 100.0f)
				{
					u32 uSleepTime = (int)((100.0f - fUpdateTime) + 0.5f);
					Sleep(uSleepTime);
				}
			}
			else
			{
				// Viewer isn't even up, so just sleep for a bit
				Sleep(1000);
			}

		}	while	(!m_bExit);

		// If I am here, it means I've been told to Exit, and the only thing that does that is the plugin when it is uninitialized.  So Maya is probably closing.

		while(rageShaderViewer::GetInstance()->ViewerIsActive() && !rageShaderViewer::GetInstance()->IsDone())
		{
			// wait for 100 milli seconds
			Sleep(100);
		}

		m_bDone = true;
	}
	catch (...) 
	{
		m_bDone = true;
		return(0);
	}

	return(0);
}

///////////////////////////////////////////////////////
// DESCRIPTION:
// This function gets called by rageShaderMaya object
//
// The function starts the thread
//
///////////////////////////////////////////////////////

void rageShaderUpdateManager::RunThread()
{
	// Make sure we are not already running
	if (m_hThread)
		return;

	// Launch the thread which will create the device and check for device state changes
	m_hThread = (HANDLE)_beginthreadex( NULL, 0, StaticRunThread, this, 0, &m_nThreadID );

	SetThreadPriority(m_hThread, THREAD_PRIORITY_LOWEST);
}

///////////////////////////////////////////////////////
// DESCRIPTION:
// This function is a static function.
//
// The function converts a Maya matrix to a RAGE matrix.
//
///////////////////////////////////////////////////////

bool rageShaderUpdateManager::convertMatrix(Matrix34 & m34, const MMatrix & mm) 
{
	float ax = (float) mm(0,0);
	float ay = (float) mm(0,1);
	float az = (float) mm(0,2);
	float bx = (float) mm(1,0);
	float by = (float) mm(1,1);
	float bz = (float) mm(1,2);
	float cx = (float) mm(2,0);
	float cy = (float) mm(2,1);
	float cz = (float) mm(2,2);
	float dx = (float) mm(3,0);
	float dy = (float) mm(3,1);
	float dz = (float) mm(3,2);
	m34.a.Set(ax, ay, az);
	m34.b.Set(bx, by, bz);
	m34.c.Set(cx, cy, cz);
	m34.d.Set(dx, dy, dz);

	return true;
}

///////////////////////////////////////////////////////
// DESCRIPTION:
// This function gets called by the maya callback function.
//
// The function updates the model data manager's camera matrix
//
///////////////////////////////////////////////////////

bool rageShaderUpdateManager::updateCamera()
{
	if (!ModelMaterialPairManager::GetInstance())
	{
		return false;
	}

	try
	{
		MStatus obStatus;
		Matrix34 CameraMatrix;
		MDagPath obMayaCameraDagPath;
		M3dView panel;

		panel = M3dView::active3dView(&obStatus);
		if (obStatus.error())
		{
			return false;
		}

		obStatus = panel.getCamera(obMayaCameraDagPath);
		if (obStatus.error())
		{
			return false;
		}

		obStatus = obMayaCameraDagPath.pop();
		if (obStatus.error())
		{
			return false;
		}

		obStatus = obMayaCameraDagPath.extendToShape();
		if (obStatus.error())
		{
			return false;
		}

		MMatrix mView(obMayaCameraDagPath.inclusiveMatrix(&obStatus));
		if (obStatus.error())
		{
			return false;
		}

		rageShaderUpdateManager::convertMatrix(CameraMatrix, mView);

	//	static Matrix34 s_PrevCameraMatrix = CameraMatrix;
	//	s_PrevCameraMatrix.Interpolate(s_PrevCameraMatrix, CameraMatrix, 0.8f);
		//s_PrevCameraMatrix.d.Lerp(0.1f, s_PrevCameraMatrix.d, CameraMatrix.d);
	//	rageShaderViewer::GetInstance()->SetCamera(s_PrevCameraMatrix);

		rageShaderViewer::GetInstance()->SetCamera(CameraMatrix);
		
		// Update the field of view to match Maya
		MFnCamera obFnMayaCamera(obMayaCameraDagPath.node());
		float fVerticalFieldOfView = (float)obFnMayaCamera.verticalFieldOfView();
		rageShaderViewer::GetInstance()->SetVerticalFieldOfView(fVerticalFieldOfView * RAD2DEG);

		float fAspectRatio = (float)obFnMayaCamera.aspectRatio();
		rageShaderViewer::GetInstance()->SetBoxAspectRatio(fAspectRatio);

		float fNearClipPlane = (float)obFnMayaCamera.nearClippingPlane();
		rageShaderViewer::GetInstance()->SetNearClipPlane(fNearClipPlane);

		float fFarClipPlane = (float)obFnMayaCamera.farClippingPlane();
		rageShaderViewer::GetInstance()->SetFarClipPlane(fFarClipPlane);
	}
	catch (...)
	{
		Assert(0);
		return false;
	}

	return true;
}

///////////////////////////////////////////////////////
// DESCRIPTION:
// This function gets called by the maya callback function.
//
// The function updates the model data manager's lights
//
///////////////////////////////////////////////////////

bool rageShaderUpdateManager::updateLights()
{
	if (!ModelMaterialPairManager::GetInstance())
	{
		return false;
	}

	if (!rageShaderViewer::GetInstance())
	{
		return false;
	}

	try
	{

		MStatus obStatus;
		MDagPath obMayaLightDagPath;
		unsigned int lightCount = 0;
		M3dView panel;
		M3dView::LightingMode lightMode;

		//values
		Matrix34 LightMatrix;
		float intensity = 1.0f;
		float r = 1.0f, g = 1.0f, b = 1.0f;

		panel = M3dView::active3dView(&obStatus);
		if (obStatus.error())
		{
			return false;
		}

		obStatus = panel.getLightingMode(lightMode);
		if (obStatus.error())
		{
			return false;
		}

		rageShaderViewer::GetInstance()->ResetLighting();
		rageShaderViewer::GetInstance()->DisableAmbient();//disable the ambient light

		if (lightMode == M3dView::kLightActive || 
			lightMode == M3dView::kLightAll || 
			lightMode == M3dView::kLightSelected)
		{
			obStatus = panel.getLightCount(lightCount);
			if (obStatus.error())
			{
				return false;
			}

			if (lightCount)
			{
				lightCount = rageShaderViewer::GetInstance()->ValidateLights(lightCount);
				if (lightCount)
				{

					unsigned int invalidLights = 0;
					for (unsigned int lightIndex = 0; lightIndex < lightCount; lightIndex++)
					{
						obStatus = panel.getLightPath(lightIndex, obMayaLightDagPath);
						unsigned int localLightIndex = lightIndex;
						if (obStatus.error())
						{
							invalidLights++;
							rageShaderViewer::GetInstance()->DisableLight(localLightIndex);
							continue;
						}

						MFn::Type typeFlags = obMayaLightDagPath.apiType();
						if (typeFlags == MFn::kPointLight)
						{
							MFnPointLight obFnMayaLight(obMayaLightDagPath.node());
							MMatrix mLight(obMayaLightDagPath.inclusiveMatrix(&obStatus));
							if (obStatus.error())
							{
								invalidLights++;
								rageShaderViewer::GetInstance()->DisableLight(localLightIndex);
								continue;
							}
							rageShaderUpdateManager::convertMatrix(LightMatrix, mLight);
							rageShaderViewer::GetInstance()->SetLightPosition(localLightIndex, LightMatrix.d);

							MColor lightColor = obFnMayaLight.color(&obStatus);
							if (obStatus.error() || !lightColor.get(MColor::kRGB, r, g, b))
							{
								invalidLights++;
								rageShaderViewer::GetInstance()->DisableLight(localLightIndex);
								continue;
							}
							rageShaderViewer::GetInstance()->SetLightColor(localLightIndex, r, g, b);

							intensity = obFnMayaLight.intensity(&obStatus);
							if (obStatus.error())
							{
								invalidLights++;
								rageShaderViewer::GetInstance()->DisableLight(localLightIndex);
								continue;
							}
							//valid when using cubic decay rate in maya
							rageShaderViewer::GetInstance()->SetLightFalloff(localLightIndex, intensity);
							rageShaderViewer::GetInstance()->SetLightType(localLightIndex, grcLightGroup::LTTYPE_POINT);
							MString name = obFnMayaLight.name();
							rageShaderViewer::GetInstance()->SetLightName(localLightIndex, name.asChar());
						}
						else if (typeFlags == MFn::kDirectionalLight)
						{
							MFnDirectionalLight obFnMayaLight(obMayaLightDagPath.node());
							MColor lightColor = obFnMayaLight.color(&obStatus);
							if (obStatus.error() || !lightColor.get(MColor::kRGB, r, g, b))
							{
								invalidLights++;
								rageShaderViewer::GetInstance()->DisableLight(localLightIndex);
								continue;
							}
							rageShaderViewer::GetInstance()->SetLightColor(localLightIndex, r, g, b);

							intensity = obFnMayaLight.intensity(&obStatus);
							if (obStatus.error())
							{
								invalidLights++;
								rageShaderViewer::GetInstance()->DisableLight(localLightIndex);
								continue;
							}
							rageShaderViewer::GetInstance()->SetLightIntensity(localLightIndex, intensity);

							MFloatVector direction = obFnMayaLight.lightDirection(0, MSpace::kWorld, &obStatus);
							if (obStatus.error())
							{
								invalidLights++;
								rageShaderViewer::GetInstance()->DisableLight(localLightIndex);
								continue;
							}
							rageShaderViewer::GetInstance()->SetLightDirection(localLightIndex, Vector3(-direction.x, -direction.y, -direction.z));
							rageShaderViewer::GetInstance()->SetLightType(localLightIndex, grcLightGroup::LTTYPE_DIR);
							MString name = obFnMayaLight.name();
							rageShaderViewer::GetInstance()->SetLightName(localLightIndex, name.asChar());
						}
						else
						{
							invalidLights++;
							rageShaderViewer::GetInstance()->DisableLight(localLightIndex);
							continue;
						}
					}

					if (invalidLights == lightCount)
					{
						rageShaderViewer::GetInstance()->NoLighting();
					}
				}
				else
				{
					rageShaderViewer::GetInstance()->NoLighting();
				}
			}
			else
			{
				rageShaderViewer::GetInstance()->NoLighting();
			}
		}
		else if (lightMode == 5)
		{
			//NOTE: light mode 5 is not defined in the M3dView enum but is used by maya when selecting "Use No Lights" for a panel
			rageShaderViewer::GetInstance()->OnlyAmbient();
		}
	}
	catch (...)
	{
		Assert(0);
		return false;
	}

	return true;
}
