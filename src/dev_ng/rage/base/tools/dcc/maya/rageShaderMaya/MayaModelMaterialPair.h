#pragma once
#include "rageShaderMaterial/rageShaderMaterial.h"
#include "rageShaderViewer/ModelMaterialPair.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MObject.h>
#include <maya/MNodeMessage.h>
#pragma warning(error: 4668)

#include <string>
#include <vector>

using namespace std;

namespace rage {

class MayaModelMaterialPair
{
public:
	MayaModelMaterialPair(const MString & sMeshNodeName, const MObject obMeshNode, const MObject obShaderNode, const rageShaderMaterial* pMtl);
	~MayaModelMaterialPair(void);

	// Accessors
	MObject			GetMeshNode()		const {return m_obMeshNode;}
	const MString & GetMeshNodeName()	const {return m_sMeshNodeName;}

	// Modifiers
	void	SetMeshNodeName(const MString & sMeshNodeName) {m_sMeshNodeName = sMeshNodeName;}
	void	UpdateMesh();
	void	UpdateShader();

	// Callbacks
	static void MeshNodeDirtyCallback(MObject & obMayaNode, void * thisPtr);
	static void MeshTransformNodeDirtyCallback(MObject & obMayaNode, void * thisPtr);

	// Update all MayaModelMaterialPair that need updating
	static void UpdateAllMayaModelMaterialPairsThatNeedUpdating();
	static MStringArray GetVertexColourSetsToIgnore();

private:

	// Modifiers
	void	UpdateMeshTransform();
	// void	UpdateMesh();

	// Functions
	bool	convertMesh();

	// Members
	// WARNING!!!!  I do not own this rageShaderMaterial, I just have a pointer to it.
	// The parent rageShaderMaya owns it
	const rageShaderMaterial*	m_pMaterial;
	ModelMaterialPair*	m_pobModelMaterialPair;
	MObject				m_obMeshNode;
	MObject				m_obShaderNode;
	MString				m_sMeshNodeName;

	// Callbacks
	MCallbackId			m_MeshNodeDirtyCallbackId;
	MCallbackId			m_MeshTransformNodeDirtyCallbackId;

	// Update all MayaModelMaterialPair that need updating
	static vector<MayaModelMaterialPair*> obMayaModelMaterialPairsThatNeedUpdating;
};

}
