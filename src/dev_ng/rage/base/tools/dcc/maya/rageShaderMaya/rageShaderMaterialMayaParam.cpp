// 
// rageShaderMaterial/shaderMaterialGeoParamValue.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#include "file/token.h"
#include "grcore/effect.h"
#include "file/stream.h"
#include "shaderMaterial/shaderMaterialGeoParamValue.h"
#include "shaderMaterial/shaderMaterialGeoParamValueBool.h"
#include "shaderMaterial/shaderMaterialGeoParamValueFloat.h"
#include "shaderMaterial/shaderMaterialGeoParamValueInt.h"
#include "shaderMaterial/shaderMaterialGeoParamValueMatrix34.h"
#include "shaderMaterial/shaderMaterialGeoParamValueMatrix44.h"
#include "shaderMaterial/shaderMaterialGeoParamValueString.h"
#include "shaderMaterial/shaderMaterialGeoParamValueTexture.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector2.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector3.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector4.h"
#include "rageShaderMaterialMayaParam.h"
#include "rageShaderMaterialMayaParamBool.h"
#include "rageShaderMaterialMayaParamFloat.h"
#include "rageShaderMaterialMayaParamInt.h"
#include "rageShaderMaterialMayaParamMatrix34.h"
#include "rageShaderMaterialMayaParamMatrix44.h"
#include "rageShaderMaterialMayaParamTexture.h"
#include "rageShaderMaterialMayaParamVector2.h"
#include "rageShaderMaterialMayaParamVector3.h"
#include "rageShaderMaterialMayaParamVector4.h"
#include "rageShaderMaterialMayaParamString.h"

using namespace rage;


bool rageShaderMaterialMayaParam::WriteScriptMayaUi(const shaderMaterialGeoParamValue* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	T.StartLine();
	T.EndLine();
	switch(pobParam->GetType())
	{
	// case grcEffect::VT_INT:
	case grcEffect::VT_FLOAT:
	case grcEffect::VT_TEXTURE:
	// case grcEffect::VT_BOOL:
	case grcEffect::VT_STRING:
		{
			T.StartLine();
			T.PutStr("string $attributeName = (%s + \".%s\");", nodeNameMaya, rageShaderMaterialMayaParam::GetCleanVersionOfString(pobParam->GetParamDescription()->GetUIName()).asChar());
			T.EndLine();

			T.StartLine();
			T.PutStr("attrControlGrp -attribute $attributeName;");
			T.EndLine();

			break;
		}
	case grcEffect::VT_VECTOR2:
		{
			rageShaderMaterialMayaParamVector2::WriteScriptMayaUi((shaderMaterialGeoParamValueVector2*)pobParam, T, nodeNameMaya);
			break;
		}
	case grcEffect::VT_VECTOR3:
		{
			rageShaderMaterialMayaParamVector3::WriteScriptMayaUi((shaderMaterialGeoParamValueVector3*)pobParam, T, nodeNameMaya);
			break;
		}
	case grcEffect::VT_VECTOR4:
		{
			rageShaderMaterialMayaParamVector4::WriteScriptMayaUi((shaderMaterialGeoParamValueVector4*)pobParam, T, nodeNameMaya);
			break;
		}
	case grcEffect::VT_MATRIX34:
		{
			rageShaderMaterialMayaParamMatrix34::WriteScriptMayaUi((shaderMaterialGeoParamValueMatrix34*)pobParam, T, nodeNameMaya);
			break;
		}
	case grcEffect::VT_MATRIX44:
		{
			rageShaderMaterialMayaParamMatrix44::WriteScriptMayaUi((shaderMaterialGeoParamValueMatrix44*)pobParam, T, nodeNameMaya);
			break;
		}
	}
	return true;
}

bool rageShaderMaterialMayaParam::WriteScriptMayaInit(const shaderMaterialGeoParamValue* pobParam, fiTokenizer & T, const char * nodeNameMaya)
{
	T.StartLine();
	T.EndLine();

	switch(pobParam->GetType())
	{
	/* case grcEffect::VT_INT:
		{
			rageShaderMaterialMayaParamInt::WriteScriptMayaInit((shaderMaterialGeoParamValueInt*)pobParam, T, nodeNameMaya);
			break;
		} */
	case grcEffect::VT_FLOAT:
		{
			rageShaderMaterialMayaParamFloat::WriteScriptMayaInit((shaderMaterialGeoParamValueFloat*)pobParam, T, nodeNameMaya);
			break;
		}
	case grcEffect::VT_VECTOR2:
		{
			rageShaderMaterialMayaParamVector2::WriteScriptMayaInit((shaderMaterialGeoParamValueVector2*)pobParam, T, nodeNameMaya);
			break;
		}
	case grcEffect::VT_VECTOR3:
		{
			rageShaderMaterialMayaParamVector3::WriteScriptMayaInit((shaderMaterialGeoParamValueVector3*)pobParam, T, nodeNameMaya);
			break;
		}
	case grcEffect::VT_VECTOR4:
		{
			rageShaderMaterialMayaParamVector4::WriteScriptMayaInit((shaderMaterialGeoParamValueVector4*)pobParam, T, nodeNameMaya);
			break;
		}
	case grcEffect::VT_TEXTURE:
		{
//			rageShaderMaterialMayaParamTexture::WriteScriptMayaInit((shaderMaterialGeoParamValueTexture*)pobParam, T, nodeNameMaya);
			break;
		}
	/* case grcEffect::VT_BOOL:
		{
			rageShaderMaterialMayaParamBool::WriteScriptMayaInit((shaderMaterialGeoParamValueBool*)pobParam, T, nodeNameMaya);
			break;
		} */
	case grcEffect::VT_MATRIX34:
		{
			rageShaderMaterialMayaParamMatrix34::WriteScriptMayaInit((shaderMaterialGeoParamValueMatrix34*)pobParam, T, nodeNameMaya);
			break;
		}
	case grcEffect::VT_MATRIX44:
		{
			rageShaderMaterialMayaParamMatrix44::WriteScriptMayaInit((shaderMaterialGeoParamValueMatrix44*)pobParam, T, nodeNameMaya);
			break;
		}
	case grcEffect::VT_STRING:
		{
			rageShaderMaterialMayaParamString::WriteScriptMayaInit((shaderMaterialGeoParamValueString*)pobParam, T, nodeNameMaya);
			break;
		}
	}
	return true;
}


bool rageShaderMaterialMayaParam::AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValue* pobParam)
{
	// MGlobal::displayInfo(MString("rageShaderMaterialMayaParam::AddAttributesToMayaNode(MObject obNode, ")+ pobParam->GetName() +")");
	switch(pobParam->GetType())
	{
	/* case grcEffect::VT_INT:
		{
			rageShaderMaterialMayaParamInt::AddAttributesToMayaNode(obNode, (shaderMaterialGeoParamValueInt*)pobParam);
			break;
		} */
	case grcEffect::VT_FLOAT:
		{
			rageShaderMaterialMayaParamFloat::AddAttributesToMayaNode(obNode, (shaderMaterialGeoParamValueFloat*)pobParam);
			break;
		}
	case grcEffect::VT_VECTOR2:
		{
			rageShaderMaterialMayaParamVector2::AddAttributesToMayaNode(obNode, (shaderMaterialGeoParamValueVector2*)pobParam);
			break;
		}
	case grcEffect::VT_VECTOR3:
		{
			rageShaderMaterialMayaParamVector3::AddAttributesToMayaNode(obNode, (shaderMaterialGeoParamValueVector3*)pobParam);
			break;
		}
	case grcEffect::VT_VECTOR4:
		{
			rageShaderMaterialMayaParamVector4::AddAttributesToMayaNode(obNode, (shaderMaterialGeoParamValueVector4*)pobParam);
			break;
		}
	case grcEffect::VT_TEXTURE:
		{
			rageShaderMaterialMayaParamTexture::AddAttributesToMayaNode(obNode, (shaderMaterialGeoParamValueTexture*)pobParam);
			break;
		}
	/* case grcEffect::VT_BOOL:
		{
			rageShaderMaterialMayaParamBool::AddAttributesToMayaNode(obNode, (shaderMaterialGeoParamValueBool*)pobParam);
			break;
		} */
	case grcEffect::VT_MATRIX34:
		{
			rageShaderMaterialMayaParamMatrix34::AddAttributesToMayaNode(obNode, (shaderMaterialGeoParamValueMatrix34*)pobParam);
			break;
		}
	case grcEffect::VT_MATRIX44:
		{
			rageShaderMaterialMayaParamMatrix44::AddAttributesToMayaNode(obNode, (shaderMaterialGeoParamValueMatrix44*)pobParam);
			break;
		}
	case grcEffect::VT_STRING:
		{
			rageShaderMaterialMayaParamString::AddAttributesToMayaNode(obNode, (shaderMaterialGeoParamValueString*)pobParam);
			break;
		}
	}
	return true;
}

bool rageShaderMaterialMayaParam::IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValue* pobParam)
{
	switch(pobParam->GetType())
	{
	/* case grcEffect::VT_INT:
		{
			return rageShaderMaterialMayaParamInt::IsAttributeSameValue(obNode, (shaderMaterialGeoParamValueInt*)pobParam);
		} */
	case grcEffect::VT_FLOAT:
		{
			return rageShaderMaterialMayaParamFloat::IsAttributeSameValue(obNode, (shaderMaterialGeoParamValueFloat*)pobParam);
		}
	case grcEffect::VT_VECTOR2:
		{
			return rageShaderMaterialMayaParamVector2::IsAttributeSameValue(obNode, (shaderMaterialGeoParamValueVector2*)pobParam);
		}
	case grcEffect::VT_VECTOR3:
		{
			return rageShaderMaterialMayaParamVector3::IsAttributeSameValue(obNode, (shaderMaterialGeoParamValueVector3*)pobParam);
		}
	case grcEffect::VT_VECTOR4:
		{
			return rageShaderMaterialMayaParamVector4::IsAttributeSameValue(obNode, (shaderMaterialGeoParamValueVector4*)pobParam);
		}
	case grcEffect::VT_TEXTURE:
		{
			return rageShaderMaterialMayaParamTexture::IsAttributeSameValue(obNode, (shaderMaterialGeoParamValueTexture*)pobParam);
		}
	/* case grcEffect::VT_BOOL:
		{
			return rageShaderMaterialMayaParamBool::IsAttributeSameValue(obNode, (shaderMaterialGeoParamValueBool*)pobParam);
		} */
	case grcEffect::VT_MATRIX34:
		{
			return rageShaderMaterialMayaParamMatrix34::IsAttributeSameValue(obNode, (shaderMaterialGeoParamValueMatrix34*)pobParam);
		}
	case grcEffect::VT_MATRIX44:
		{
			return rageShaderMaterialMayaParamMatrix44::IsAttributeSameValue(obNode, (shaderMaterialGeoParamValueMatrix44*)pobParam);
		}
	case grcEffect::VT_STRING:
		{
			return rageShaderMaterialMayaParamString::IsAttributeSameValue(obNode, (shaderMaterialGeoParamValueString*)pobParam);
		}
	}
	return false;
}

MString rageShaderMaterialMayaParam::GetCleanVersionOfString(const char* pcQuestionableString)
{
	MString strReturnMe = "";

	// Replace any bad chars with "_"
	unsigned iStrLen = strlen(pcQuestionableString);
	for(unsigned i=0; i<iStrLen; i++)
	{
		char c = pcQuestionableString[i];
		if(
			((c >= 'a') && (c <= 'z'))
			||
			((c >= 'A') && (c <= 'Z'))
			||
			((c >= '0') && (c <= '9'))
			)
		{
			// The character is valid, so do nothing
			strReturnMe += MString(&c, 1);
		}
		else
		{
			// Bad character, so replace with an _
			strReturnMe += "_";
		}
	}
	return strReturnMe;
}
