// 
// rageShaderMaya65/rageShaderUpdate.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#ifndef RAGE_SHADER_UPDATE_DATA_H
#define RAGE_SHADER_UPDATE_DATA_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MString.h>
#pragma warning(error: 4668)

#include "atl/string.h"
#include "mesh/mesh.h"
#include "rageShaderMaterial/rageShaderMaterialGroup.h"

/////////////////////////////////////////////////
// Class used to update the RAGE shader viewer //
/////////////////////////////////////////////////

namespace rage {

class mshMesh;
class rageShaderViewer;
class rageShaderMaterial;

//////////////////////////////////////////////////////////////////////////////////////////////////
// This class represents a shader<->model pair and makes sure things are always upto date       //
//////////////////////////////////////////////////////////////////////////////////////////////////
class rageShaderUpdateData
{
public:
	rageShaderUpdateData();
	~rageShaderUpdateData();

	bool				IsValid()				const {return m_Valid;}
	bool				IsLocked()				const {return m_Locked;}
	bool				NeedsUpdate()			const {return m_MeshNeedsUpdate || m_TransformNeedsUpdate || m_ShadersNeedsUpdate;}
	bool				MeshNeedsUpdate()		const {return m_MeshNeedsUpdate;}
	bool				TransformNeedsUpdate()	const {return m_TransformNeedsUpdate;}
	bool				ShadersNeedsUpdate()	const {return m_ShadersNeedsUpdate;}
	bool				NeedsDeleting()			const {return m_NeedsDeleting;}

	void Reset();

	void InitShaderUpdateData(int nIndex, const MObject & shaderNode, const rageShaderMaterial* pMaterial, MObject& obMeshNode);

	void UpdateMesh(MObject & obMeshNode, const MObject & shaderNode);
	void UpdateTransform(const Matrix34 & m34);
	void UpdateShaders(const rageShaderMaterial* pMaterial);
	void UpdateViewer();

	void RefreshViewer();

	void SetValid(bool b)		{m_Valid = b;}
	void SetNeedsDeleting(bool b)  
	{
		m_NeedsDeleting = b;
	}

	rageShaderUpdateData & operator = (const rageShaderUpdateData &d);

	const rageShaderMaterial*	GetMaterial() const {return m_pMaterial;}

private:
	bool								m_Valid;
	bool								m_Locked;
	bool								m_MeshNeedsUpdate;
	bool								m_TransformNeedsUpdate;
	bool								m_ShadersNeedsUpdate;
	bool								m_NeedsDeleting;
	mshMesh								m_Mesh;
	Matrix34							m_Transform;
	rageShaderMaterial*  				m_pMaterial;
	ConstString							m_LightmapFileNames[rageShaderMaterialParam::LIGHTMAP_TYPE_COUNT];

	// Functions
	bool exportTexture(const MString& strInputTifName, const MString& strExportFormat);
	bool fixupLightMaps(MObject& obMayaNode, ConstString * lightmapFileNames);
	bool convertMesh(MObject& obMayaNode, const MObject & shaderNode, mshMesh & rageMesh);


	// Keep a handy pointer to the modelData, remember however that I DO NOT own this modelData, modelDataManager does
	// This is just a handy pointer so I don't have to keep looking it up all the time
	ModelMaterialPair*	m_pobModelData;
};

} // end using namespace rage

#endif /* RAGE_SHADER_UPDATE_DATA_H */
