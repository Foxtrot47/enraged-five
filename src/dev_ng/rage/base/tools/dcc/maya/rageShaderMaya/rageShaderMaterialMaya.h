// 
// rageShaderMaya65/rageShaderUpdate.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#ifndef RAGE_SHADER_MATERIAL_MAYA_H
#define RAGE_SHADER_MATERIAL_MAYA_H

#include "rageShaderMaterial/rageShaderMaterial.h"

namespace rage {

class rageShaderMaterialMaya
{
public:
//	static bool WriteResetScriptMaya(const rageShaderMaterial* pobMaterial, fiTokenizer & T, const char * nodeNameMaya);

	// This function adds all the attributes
	static bool AddToMayaNode(MObject obNode, const rageShaderMaterial& obMaterial, bool* DeletedExistingDataDuringCreation);

	// This function removes all the attributes
	static bool RemoveFromMayaNode(MObject obNode, const rageShaderMaterial& obMaterial);

	// This function fixes up all the texture connections
	static bool ExpandTextureNodes(MObject obNode, const rageShaderMaterial& obMaterial);

private:
	// This function fixes up all the texture connections for a given parameter
	static bool WireUpTextureNode(MObject obNode, const shaderMaterialGeoParamValue& obParam);	
};

} // end using namespace rage

#endif
