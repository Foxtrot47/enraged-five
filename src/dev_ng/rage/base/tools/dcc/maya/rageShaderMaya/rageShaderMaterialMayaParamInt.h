// 
// rageShaderMaterial/rageShaderMaterialMayaParamInt.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if 0
#ifndef RAGE_SHADER_MATERIAL_MayaParam_INT_H
#define RAGE_SHADER_MATERIAL_MayaParam_INT_H

#include "rageShaderMaterialMayaParam.h"
#include "shaderMaterial/shaderMaterialGeoParamValueInt.h"

namespace rage {

class rageShaderMaterialMayaParamInt
{
public:
	static bool WriteScriptMayaInit(const shaderMaterialGeoParamValueInt* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueInt* pobParam);

	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueInt* pobParam);
};

} // end namespace rage

#endif
#endif
