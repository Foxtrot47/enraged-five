// 
// /rageShaderCommand.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_COMMAND_H
#define RAGE_SHADER_COMMAND_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#pragma warning(error: 4668)

#include <vector>

using namespace std;

///////////////////////////////////////////
// rageShaderCommand Class //
///////////////////////////////////////////

namespace rage {

class Vector3;

class rageShaderCommand : public MPxCommand
{
public:
	rageShaderCommand();
	virtual         ~rageShaderCommand();

	static  void *	creator();
	virtual MStatus doIt ( const MArgList & args );
	virtual bool  isUndoable () const {return false;}

private:

	// Accessors
	float getTOD();
	bool getTODOn();
	bool getTODCycle();
	float getTODCycleSpeed();
	Vector3 getAmbient();
	Vector3 getDiffuse();
	Vector3 getBackground();
	void getViewerPosition(int * data);

	// Modifiers
	MStatus setTOD(const float tod);
	MStatus setTODOn(const bool b);
	MStatus setTODCycle(const bool b);
	MStatus setTODCycleSpeed(const float f);
	MStatus setAmbient(const Vector3 & c);
	MStatus setDiffuse(const Vector3 & c);
	MStatus setBackground(const Vector3 & c);
	MStatus setViewerPosition(const int * data);
	MStatus setGlobalParam(const char * paramName, const vector <float> & data);
	MString newMaterial(const char * paramName);
	MStringArray IssueCommand(const char * pcNodeName, const char * pcCommand);
	MStringArray GetParamNames(const MString& strNodeName, shaderMaterialGeoParamDescription::ValueSource _SourceFlags);
};

} // end using namespace rage

#endif /* GEN_RAGE_SHADER_MATERIAL_MAYA65_H */
