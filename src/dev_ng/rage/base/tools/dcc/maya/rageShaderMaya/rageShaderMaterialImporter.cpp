// 
// /rageShaderMaterialImporter.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include "maya/MString.h"
#include "maya/MGlobal.h"
#pragma warning(pop)

#include "rageShaderMaterialImporter.h"

using namespace rage;

extern bool g_bDisplayMGlobalExecuteCommands;

rageShaderMaterialImporter::rageShaderMaterialImporter()
{
}

rageShaderMaterialImporter::~rageShaderMaterialImporter()
{
}

void * rageShaderMaterialImporter::creator()
{
	return new rageShaderMaterialImporter();
}

MStatus  rageShaderMaterialImporter::reader(const MFileObject & file, const MString & , FileAccessMode )
{
	MString command;
	command = "rageShaderCommand -new \"" + file.fullName() + "\";";
	MGlobal::executeCommand(command, g_bDisplayMGlobalExecuteCommands);

	return MStatus::kSuccess;
}

MPxFileTranslator::MFileKind  rageShaderMaterialImporter::identifyFile(const MFileObject & file, const char* , short ) const
{
	if (strstr(file.fullName().asChar(), ".mtl"))
		return MPxFileTranslator::kIsMyFileType;
	else
		return MPxFileTranslator::kNotMyFileType;
}
