// 
// rageShaderMaya65/rageShaderUpdateManager.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#ifndef RAGE_SHADER_UPDATE_MANAGER_H
#define RAGE_SHADER_UPDATE_MANAGER_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MString.h>
#pragma warning(error: 4668)

#include "atl/string.h"
#include "mesh/mesh.h"
#include "MayaModelMaterialPair.h"

#include <vector>
using namespace std;

/////////////////////////////////////////////////
// Class used to update the RAGE shader viewer //
/////////////////////////////////////////////////

namespace rage {

class rageShaderMaya;

class rageShaderUpdateData
{
public:
	rageShaderUpdateData(rageShaderMaya * pobMayaNode);
	~rageShaderUpdateData();

	bool IsShaderInViewer() const {return (m_aMayaModelMaterialPairs.GetCount() != 0);}

	void ReleaseConnectedMesh(const MObject & obMeshNode, const MString & sMeshNodeName);
	void AddModelMaterialPair(const MString & sMeshNodeName);
	void DeleteModelMaterialPair(const MString & sMeshNodeName);
	void UpdateShader();

	void Update();

private:

	rageShaderUpdateData();

	// Sister maya node
	rageShaderMaya * m_pobMayaNode;

	bool				m_boIsUpdating;
	bool				m_boIsModifying;
	bool				m_bRemoveAll;
	bool				m_bUpdateShader;
	atBucket<MString>	m_asAddMeshNodeNames;
	atBucket<MString>	m_asRemoveMeshNodeNames;

	// These MayaModelMaterialPairs exist all over the place, but these are THE real ones, the rest are just pointers to these ones
	atBucket <MayaModelMaterialPair*>	m_aMayaModelMaterialPairs;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
// This class is a manager that contains an array of all the rageShaderUpdateData               //
//////////////////////////////////////////////////////////////////////////////////////////////////
class rageShaderUpdateManager
{
public:
	rageShaderUpdateManager();
	~rageShaderUpdateManager();

	static rageShaderUpdateManager * GetInstance();

	void RunThread();

	bool				IsDone()	const {return m_bDone;}

	void SetExit(bool b) {m_bExit = b;}

	static bool convertMatrix(Matrix34 & m34, const MMatrix & mm);
	static bool convertMatrix(Matrix34 & m34, const MFloatMatrix & mm);

	static const char * GetRageShadersAssetsFolder() {return (const char *)sm_sRageShadersAssetsFolder;}

	static rageShaderUpdateData *	CreateUpdateData(rageShaderMaya * pobMayaNode);
	static void						ReleaseUpdateData(const rageShaderUpdateData * pobUpdateData);

private:

	static u32 __stdcall StaticRunThread( void* pParam );

	u32 __stdcall Main();

	void Init();
	bool updateCamera();
	bool updateLights();

	bool								m_bDone;
	bool								m_bExit;
	u32 								m_nThreadID;
	HANDLE  							m_hThread;
	atBucket <rageShaderUpdateData *>	m_apUpdateData;

	static ConstString				sm_sRageShadersAssetsFolder;
	static rageShaderUpdateManager*	sm_pInstance;
	

	

};

} // end using namespace rage

#endif /* RAGE_SHADER_UPDATE_MANAGER_H */
