// 
// rageShaderMaya/rageShaderTextureCache.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//

#ifndef RAGE_SHADER_TEXTURE_CACHE
#define RAGE_SHADER_TEXTURE_CACHE

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MObject.h>
#pragma warning(error : 4668)

#include "atl/map.h"
#include "string/string.h"

namespace rage {

class rageShaderTexture;

class rageShaderTextureCacheData
{
friend class rageShaderTextureCache;

public:
	rageShaderTextureCacheData();
	
	~rageShaderTextureCacheData();

	rageShaderTexture* texture() { return m_texture; }

	bool isDirty(const char * fileName) const;

	bool load(const char * fileName);

	bool load(const char * mtlFileName, const u8 * pBits, const int nWidth, const int nHeight);

private:
	rageShaderTexture* m_texture;
	u64 m_timestamp;
};

class rageShaderTextureCache
{
public:

	rageShaderTextureCache();
	~rageShaderTextureCache();

	// Returns true if the texture was found and bound; returns false otherwise.
	rageShaderTexture* bind(const char * textureFileName);
	rageShaderTexture* bind(const char * mtlFileName, const u8 * pBits, const int nWidth, const int nHeight);

private:
	// Return a reference to the texture. There's no reference counting yet.
	rageShaderTexture* texture(const char * textureFileName);
	rageShaderTexture* texture(const char * mtlFileName, const u8 * pBits, const int nWidth, const int nHeight);

	atMap <ConstString, rageShaderTextureCacheData*> m_textureTable;
};

} // end using namespace rage

#endif // RAGE_SHADER_TEXTURE_CACHE
