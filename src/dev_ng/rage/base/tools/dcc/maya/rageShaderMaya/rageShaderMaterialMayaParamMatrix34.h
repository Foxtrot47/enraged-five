// 
// rageShaderMaterial/rageShaderMaterialMayaParamMatrix34.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef RAGE_SHADER_MATERIAL_MayaParam_MATRIX34_H
#define RAGE_SHADER_MATERIAL_MayaParam_MATRIX34_H

#include "rageShaderMaterialMayaParam.h"
#include "shaderMaterial/shaderMaterialGeoParamValueMatrix34.h"

namespace rage {

class rageShaderMaterialMayaParamMatrix34
{
public:

	static bool WriteScriptMayaUi(const shaderMaterialGeoParamValueMatrix34* pobParam, fiTokenizer & T, const char * nodeNameMaya);
	static bool WriteScriptMayaInit(const shaderMaterialGeoParamValueMatrix34* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueMatrix34* pobParam);

	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueMatrix34* pobParam);
};

} // end namespace rage

#endif
