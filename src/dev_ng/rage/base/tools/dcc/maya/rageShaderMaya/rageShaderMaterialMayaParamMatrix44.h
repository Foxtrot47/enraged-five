// 
// rageShaderMaterial/rageShaderMaterialMayaParamMatrix44.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGE_SHADER_MATERIAL_MayaParam_MATRIX44_H
#define RAGE_SHADER_MATERIAL_MayaParam_MATRIX44_H

#include "rageShaderMaterialMayaParam.h"
#include "shaderMaterial/shaderMaterialGeoParamValueMatrix44.h"

namespace rage {

class rageShaderMaterialMayaParamMatrix44
{
public:
	static bool WriteScriptMayaUi(const shaderMaterialGeoParamValueMatrix44* pobParam, fiTokenizer & T, const char * nodeNameMaya);
	static bool WriteScriptMayaInit(const shaderMaterialGeoParamValueMatrix44* pobParam, fiTokenizer & T, const char * nodeNameMaya);

	static bool AddAttributesToMayaNode(MObject obNode, const shaderMaterialGeoParamValueMatrix44* pobParam);

	static bool IsAttributeSameValue(MObject obNode, const shaderMaterialGeoParamValueMatrix44* pobParam);
};

} // end namespace rage

#endif
