// 
// rageShaderMaya65/rageShaderMaya65.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef RAGE_SHADER_MAYA65_H
#define RAGE_SHADER_MAYA65_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MObjectArray.h>
#include <maya/MDagPath.h>
#include <maya/MPxHwShaderNode.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnStringData.h>
#include <maya/MMessage.h>
#include <maya/MTimer.h>
#include <maya/MNodeMessage.h>
#include <maya/MFileIO.h>
#pragma warning(error: 4668)

#include "rageShaderMaterial/rageShaderMaterial.h"
#include "shaderMaterial/shaderMaterialGeoParamValueTexture.h"


#include <vector>

using namespace std;

///////////////////////////////////
// Plugin RAGE Shader Class //
///////////////////////////////////

class MFnMesh;

namespace rage {

	class mshMesh;
	class rageShaderMaterialViewer;
	class rageShaderTexture;
	class rageShaderTextureCache;
	class rageShaderUpdateData;
	class rageShaderUpdateManager;

	class rageShaderMaya : public MPxHwShaderNode
	{
	public:
		rageShaderMaya();
		virtual         ~rageShaderMaya();

		//Maya Interface
		static  void *	creator();
		virtual MStatus	compute( const MPlug&, MDataBlock& );
		virtual MStatus	bind(const MDrawRequest&, M3dView& );
		virtual MStatus	unbind(const MDrawRequest& , M3dView& );
		virtual MStatus	geometry( const MDrawRequest& request,	M3dView& , int , unsigned int ,	int , const unsigned int * , int , const int * , const float * , int , const float ** , int , const float ** , int , const float ** );
		virtual MStatus	glBind(const MDagPath& shapePath);
		virtual MStatus	glUnbind(const MDagPath& shapePath);
		virtual MStatus	glGeometry( const MDagPath& shapePath, int prim, unsigned int writable, int indexCount, const unsigned int * indexArray, int vertexCount, const int * vertexIDs, const float * vertexArray, int normalCount, const float ** normalArrays, int colorCount, const float ** colorArrays, int texCoordCount, const float ** texCoordArrays);
		virtual MStatus renderImage ( const MString & imageName, const float region[2][2], int& imageWidth, int& imageHeight);
		virtual MStatus renderSwatchImage ( MImage & image );
		virtual int  getTexCoordSetNames ( MStringArray & names);
		virtual MStatus  getAvailableImages ( const MString & uvSetName, MStringArray & imageNames);
		virtual bool setInternalValueInContext ( const MPlug & plug, const MDataHandle & handle, MDGContext & context);
		virtual void copyInternalData ( MPxNode *node );
		virtual int	normalsPerVertex();
		virtual int	colorsPerVertex();
		virtual bool hasTransparency();
		virtual int	getColorSetNames( MStringArray & names );
		virtual void    postConstructor();

		//Maya Static
		static  void initalizePlugin();
		static	void uninitalizePlugin(void * unused = NULL);
		static  MStatus	initialize();

		//Rage
		void CreateUVSets(MObject obMeshNode);
		void GetUnusedUVSetNames (const MStringArray &usedTextureCoordSetNames, const MStringArray &aObjectUvSetNames, MStringArray &out_CulledUvSetNames);
		void GetTemplateFileName (MString &out_TemplateFilename);
		bool StringArrayHasString (const MString &strFindString, const MStringArray &aStrings);
		void DeleteUVSetFromNode (const MString &strUVSetName, const MString& strMeshNodeName);
		void ReleaseTextures() {m_pCurrentTextureParam = NULL; m_pCurrentTexture = NULL;}
		void MeshConnected(MObject obMeshNode, bool bConnectionMade );

		void DeleteParamAttributesBeforeSave();
		void CreateParamAttributesAfterSave();
		MStringArray	IssueCommand(const char* pcCommand);
		MStringArray	GetParamNames(shaderMaterialGeoParamDescription::ValueSource _SourceFlags) const;

		const rageShaderMaterial* GetRageMaterial() const;

		void breakConnectionToAttrib(const char* attribName);
		void handleDirtyAttribute(const char* attribName);

		void SaveMaterialFile();
		void removeThisNode();

		//Rage Static 
		static  rageShaderMaya* GetRageShaderMayaNodeGivenNodeName(const char* pcNodeName);
		static void RegisterGlobalCallbacks();
		static void UnregisterGlobalCallbacks();
		static void setIgnoreReferencedNodesDuringWireUpTextures(bool ignoreReferencedNodesDuringWireUpTextures) {m_IgnoreReferencedNodesDuringWireUpTextures = ignoreReferencedNodesDuringWireUpTextures;}
		static bool getIgnoreReferencedNodesDuringWireUpTextures() {return m_IgnoreReferencedNodesDuringWireUpTextures;}


		//Data Static 
		static  MTypeId   id;  // The IFF type id

	protected:

		//Data Static 
		// Surface color
		static MObject  aColorR;
		static MObject  aColorG;
		static MObject  aColorB;
		static MObject  aColor;

		// Shader specific attributes
		static MObject		aTextureToDisplayInMaya;
		static MObject		aTextureOverride;
		static MObject  	aMtlFile; //NOTE: This attribute should always be on the RSM because it tells what mtlgeo file to load on scene open
		static MObject		aCommand;
		static MObject		aDisplayTransparency;
		static MObject		aIgnoreCreateUVsOnMeshConnection;

		//Attributes that have to exist because they were saved into old MB files
		static MObject		aPointlessAttributeThatShouldBeDeletedButCantBecauseItIsSavedIntoExistingFiles;
		static MObject		aPointlessAttributeThatShouldBeDeletedButCantBecauseItIsSavedIntoExistingFiles2;
		static MObject  	aPointlessAttributeThatShouldBeDeletedButCantBecauseItIsSavedIntoExistingFiles3;
		static MObject  	aPointlessAttributeThatShouldBeDeletedButCantBecauseItIsSavedIntoExistingFiles4;
		static MObject  	aPointlessAttributeThatShouldBeDeletedButCantBecauseItIsSavedIntoExistingFiles5;//aNewMtlFile
		static MObject  	aPointlessAttributeThatShouldBeDeletedButCantBecauseItIsSavedIntoExistingFiles6;//aSaveMtlFile
		static MObject		aPointlessAttributeThatShouldBeDeletedButCantBecauseItIsSavedIntoExistingFiles7;//aMaterialUIMelScriptFile

	private:

		// Rage
		void bind(bool bEnableLighting);
		void unbind();
		void geometry(const MDagPath & path,
			int prim,
			int indexCount,
			const unsigned int * indexArray,
			int vertexCount,
			const int * vertexIDs,
			const float * vertexArray,
			int normalCount,
			const float ** normalArrays,
			int colorCount,
			const float ** colorArrays,
			int texCoordCount,
			const float ** texCoordArrays);

		bool isLocked() const {return MFnDependencyNode(thisMObject()).isLocked();}

		void loadFromTemplateFile(const MString& strTemplateFilename);
		void loadFromTypeFile(const MString& strTypeFilename);
		void loadFromMTLFile(const MString& strMaterialFilename);
		
		bool createFromTemplateFile(const MString& strTemplateFilename, rageShaderMaterial* pobRageMaterial);
		bool createFromTypeFile(const MString& strTypeFilename, rageShaderMaterial* pobRageMaterial);
		bool createFromMTLFile(const MString& strMaterialFilename, rageShaderMaterial* pobRageMaterial);
		void postCreate(rageShaderMaterial* pobRageMaterial);

		void saveAsNewType();
		void saveType();
		void reloadType();

		bool BrowseForMaterialFileAndSaveToIt();
		bool saveMaterialAndSetMaterialFilenameAttribute(const MString& strMaterialFilename);

		void releaseConnectedMesh(const MObject & obMeshNode, const MString & sMeshNodeName);

		// Accessors
		void addToViewer();
		void AddModelMaterialPair(const MString & sMeshNodeName);
		void removeFromViewer();
		void DeleteModelMaterialPair(const MString & sMeshNodeName);

		// Get and Set attributes
		bool	getThisBoolAttr(const MObject & bObject) const;
		void	getThisStringAttr(const char * attrName, MString & value) const;
		void	getThisIntAttr(const char * attrName, int & value);
		void	getThisBoolAttr(const char * attrName, bool & value);
		void	setThisStringAttr(const char * attrName, const char * value);
		void	setThisIntAttr(const char * attrName, const int value);
		void	setThisBoolAttr(const char * attrName, const bool value);

		MString getFileToOpen(const char* fileFilter, const char* lastLoadPath) const;
		MString	GetMaterialPathAndFilename() const;
		void SetMaterialPathAndFilename(const MString&	strMaterialFilename);
		void SetTextureToDisplayInMaya(const MString & strTextureToDisplayInMaya);
		bool ParametersAttributeValueChanged(const MPlug &obPlugBeingDirtied, const MDataHandle & obDataHandle);
		void CallAttributeWatchScriptFunction(const MString& nodeName, const MString& attribute);
		rageShaderMaterial*				ValidateAndGetRageMaterial();
		bool					IsShaderInViewer() const;
		bool skipSetInternalValueInContext() const;
		void skipSetInternalValueInContext(bool bSkip);

		// UV set handling
		int  getTexCoordSetNamesThatThisMaterialNeeds ( MStringArray & names);
		int  getTexCoordSetNamesThatEveryMeshUsingThisNodeHas ( MStringArray & names);

		// Colour set handling
		MStringArray			GetRequiredVertexColorSetNames() const;
		void UpdateTextureOverrides(void);

		//Static 
		static void				SetPathToLastLoadedMtlFile(const char* pcPathToLastLoadedMtlFile);
		static const char*		GetPathToLastLoadedMtlFile() {return m_strPathToLastLoadedMtlFile.asChar();}
		static void				SetPathToLastLoadedTemplateFile(const char* pcPathToLastLoadedTemplateFile);
		static const char*		GetPathToLastLoadedTemplateFile();
		static void				SetPathToLastLoadedTypeFile(const char* pcPathToLastLoadedTypeFile);
		static const char*		GetPathToLastLoadedTypeFile();

		//Callbacks
		static void GlobalAfterMayaOpenCallback(void * clientData);
		static void GlobalBeforeMayaOpenCallback(void * clientData);
		static void GlobalBeforeMayaSaveCallback(void * clientData);
		static void GlobalAfterMayaSaveCallback(void * clientData);
		static void NodeAboutToDeleteCallback(MObject& node, MDGModifier& modifier, void* clientData);
		static void NodeAttribDirtyCallback( MObject & node, MPlug & plug, void* clientData );
		static void connectionCallback( MPlug & srcPlug, MPlug & destPlug, bool made, void * thisPtr);

		MString getDefaultMtlFileName();
		MObject	GetShaderEngineNode() const;

		//Deprecated 
		MString	GetGeoTypePathAndFileName() const;
		MString	GetGeoTemplatePathAndFileName() const;

		//Data 
		bool					m_bMaterialChangedSinceLastSaved;
		shaderMaterialGeoParamValueTexture *	m_pCurrentTextureParam;
		rageShaderTexture *					m_pCurrentTexture;
		MString								m_strCurrentTextureOverrideFilename; //used for texture override case.

		int						m_nUniqueIndex;

		MCallbackId				m_depGraphUpdateCallbackId;
		MCallbackId				m_ConnectionCallbackId;

		MCallbackId				m_NodeAboutToDeleteCallbackId;
		bool m_DeletedExistingDataDuringCreation; //used for materials

		MString					m_VisibleTextureParam;
		bool m_bNodeIsCopy; //designates a copy/pasted node or a duplicated node.

		rageShaderUpdateData	* m_pobUpdateData; // This is the data for the update thread
		bool m_bSkipSetInternalValueInContext; // During initialization you need to disable the setInternalValueInContext() functionality

		int				m_iUVSetIndexToUseInGeometeryFunctions;
		MStringArray	m_astrUVSetsUsedInGeometeryFunctions;

		//Data Static
		static MCallbackId      m_AttribChange;
		static MCallbackId		sm_MayaExitingCallback;

		static MCallbackId		m_GlobalAfterMayaNewCallbackId;
		static MCallbackId		m_GlobalAfterMayaOpenCallbackId;
		static MCallbackId		m_GlobalAfterMayaImportCallbackId;
		static MCallbackId		m_GlobalAfterMayaReferenceCallbackId;
		static MCallbackId		m_GlobalBeforeMayaNewCallbackId;
		static MCallbackId		m_GlobalBeforeMayaOpenCallbackId;
		static MCallbackId		m_GlobalAfterMayaSaveCallbackId;
		static MCallbackId		m_GlobalBeforeMayaSaveCallbackId;
		static bool				m_GlobalIgnoreConnectionBreaks;
		static rageShaderTextureCache * sm_pTextureCache;
		static MTimer			sm_Timer;
		static rageShaderUpdateManager * sm_pRageShaderUpdateManager;
		static vector <rageShaderMaya *> sm_apRSMNodes;
		static MString			m_strPathToLastLoadedMtlFile;
		static MString			m_strPathToLastLoadedTemplateFile;
		static MString			m_strPathToLastLoadedTypeFile;
		static bool				m_IgnoreReferencedNodesDuringWireUpTextures;

		// This is the contained Rage shader.  NEVER EVER EVER access this directly!!!!  
		//  Always use the accessor ValidateAndGetRageMaterial() as that makes sure the material is valid
		mutable rageShaderMaterial* m_pobRageMaterial;
	};

} // end using namespace rage

#endif /* RAGE_SHADER_MAYA65_H */



