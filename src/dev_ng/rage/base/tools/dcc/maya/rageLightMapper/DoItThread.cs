using System;
using System.Threading;
using RSG.Base;

namespace rageLightMapper
{
	// Delegate that defines the signature for the callback method.
	// The callback is called then DoIt has finished
	public delegate void DoneItCallback();

	/// <summary>
	/// Summary description for DoItThread.
	/// </summary>
	public class DoItThread
	{
		public DoItThread(string strMayaBinary, 
			bool bGenerateBakeSets, string strBakeSetInputMayaFile, string strBakeSetOutputMayaFile, string strBakeSetOutputLogFile, 
			int iMaxNoOfBakeSets, int iWidth, int iHeight, bool bForceSoftEdges, 
			bool bGenerateLightMaps, string strGenerateLightMapsInputMayaFile, string strGenerateLightMapsOutputLogFile, 
			bool bExport, string strExportInputMayaFile, string strExportOutputLogFile,
			DoneItCallback cbDoneItCallback,
			UpdateHtmlLogThread BakeSetHtmlLogThread,
			UpdateHtmlLogThread LightMapHtmlLogThread,
			UpdateHtmlLogThread ExportHtmlLogThread
			)
		{
			m_strMayaBinary						 = strMayaBinary;
			m_bGenerateBakeSets					 = bGenerateBakeSets;
			m_strBakeSetInputMayaFile			 = strBakeSetInputMayaFile;
			m_strBakeSetOutputMayaFile			 = strBakeSetOutputMayaFile;
			m_strBakeSetOutputLogFile			 = strBakeSetOutputLogFile;
			m_iMaxNoOfBakeSets					 = iMaxNoOfBakeSets;
			m_iWidth							 = iWidth;
			m_iHeight							 = iHeight;
			m_bForceSoftEdges					 = bForceSoftEdges;
			m_bGenerateLightMaps				 = bGenerateLightMaps;
			m_strGenerateLightMapsInputMayaFile	 = strGenerateLightMapsInputMayaFile;
			m_strGenerateLightMapsOutputLogFile	 = strGenerateLightMapsOutputLogFile;
			m_bExport							 = bExport;
			m_strExportInputMayaFile			 = strExportInputMayaFile;
			m_strExportOutputLogFile			 = strExportOutputLogFile;		
			m_cbDoneItCallback					 = cbDoneItCallback;

			// I need to keep track of the monitoring threads, so I can kick them off at the various stages
			m_BakeSetHtmlLogThread		= BakeSetHtmlLogThread;
			m_LightMapHtmlLogThread		= LightMapHtmlLogThread;
			m_ExportHtmlLogThread		= ExportHtmlLogThread;
		}

		public void DoIt()
		{
			ThreadStart obDoItThreadDelegate = new ThreadStart(ReallyDoIt);
			Thread obDoItThread = new Thread(obDoItThreadDelegate);
			obDoItThread.Priority = ThreadPriority.Lowest;
			obDoItThread.Start();
		}

		private void ReallyDoIt()
		{
			// Generate BakeSets?
			if(m_bGenerateBakeSets)
			{
				// Create thread to do the work
				MayaThread obMayaBakeSetThread = new MayaThread(m_strMayaBinary, "rageILL.Batch.AddBakeSetsToFile(\\\""+ m_strBakeSetInputMayaFile.Replace("\\", "/") +"\\\", "+ m_iMaxNoOfBakeSets +", "+ m_iWidth +", "+ m_iHeight +", "+ ((m_bForceSoftEdges == true) ? 1 : 0) +")", m_strBakeSetOutputLogFile);
				ThreadStart obGenerateBakeSetsThreadDelegate = new ThreadStart(obMayaBakeSetThread.DoIt);
				Thread obGenerateBakeSetsThread = new Thread(obGenerateBakeSetsThreadDelegate);
				obGenerateBakeSetsThread.Priority = ThreadPriority.AboveNormal;
				obGenerateBakeSetsThread.Start();

				bool bKickedOffMonitor = false;
				while(obGenerateBakeSetsThread.IsAlive)
				{
					if(!bKickedOffMonitor && (obMayaBakeSetThread.HtmlLog != null))
					{
						m_BakeSetHtmlLogThread.StartMonitoringLog(obMayaBakeSetThread.HtmlLog);
						bKickedOffMonitor = true;
					}

					// Console.WriteLine("obGenerateBakeSetsThread.IsAlive");
					Thread.Sleep(500);
				}
				Console.WriteLine("Out of loop obGenerateBakeSetsThread.IsAlive");
			}

			// Generate LightMaps?
			if(m_bGenerateLightMaps)
			{
				// Create thread to do the work
				MayaThread obMayaLightMapsThread = new MayaThread(m_strMayaBinary, "rageILL.Batch.RenderLightMapsFromFile(\\\""+ m_strGenerateLightMapsInputMayaFile.Replace("\\", "/") +"\\\")", m_strGenerateLightMapsOutputLogFile);
				ThreadStart obGenerateLightMapsThreadDelegate = new ThreadStart(obMayaLightMapsThread.DoIt);
				Thread obGenerateLightMapsThread = new Thread(obGenerateLightMapsThreadDelegate);
				obGenerateLightMapsThread.Priority = ThreadPriority.AboveNormal;
				obGenerateLightMapsThread.Start();

				bool bKickedOffMonitor = false;
				while(obGenerateLightMapsThread.IsAlive)
				{
					if(!bKickedOffMonitor && (obMayaLightMapsThread.HtmlLog != null))
					{
						m_LightMapHtmlLogThread.StartMonitoringLog(obMayaLightMapsThread.HtmlLog);
						bKickedOffMonitor = true;
					}
					// Console.WriteLine("obGenerateLightMapsThread.IsAlive");
					Thread.Sleep(500);
				}
				Console.WriteLine("Out of loop obGenerateLightMapsThread.IsAlive");
			}

			// Export?
			if(m_bExport)
			{
				// Create thread to do the work
				MayaThread obMayaLightMapsThread = new MayaThread(m_strMayaBinary, "rageBatchExport(\\\""+ m_strExportInputMayaFile.Replace("\\", "/") +"\\\", \\\"\\\")", m_strExportOutputLogFile);
				ThreadStart obExportThreadDelegate = new ThreadStart(obMayaLightMapsThread.DoIt);
				Thread obExportThread = new Thread(obExportThreadDelegate);
				obExportThread.Priority = ThreadPriority.AboveNormal;
				obExportThread.Start();

				bool bKickedOffMonitor = false;
				while(obExportThread.IsAlive)
				{
					if(!bKickedOffMonitor && (obMayaLightMapsThread.HtmlLog != null))
					{
						m_ExportHtmlLogThread.StartMonitoringLog(obMayaLightMapsThread.HtmlLog);
						bKickedOffMonitor = true;
					}
					Console.WriteLine("obExportThread.IsAlive");
					Thread.Sleep(500);
				}
				Console.WriteLine("Out of loop obExportThread.IsAlive");
			}

			// Finished so do the callback
			if (m_cbDoneItCallback != null)
			{
				m_cbDoneItCallback();
			}
		}


		private string				m_strMayaBinary;
		private bool				m_bGenerateBakeSets;
		private string				m_strBakeSetInputMayaFile;
		private string				m_strBakeSetOutputMayaFile;
		private string				m_strBakeSetOutputLogFile;
		private int					m_iMaxNoOfBakeSets;
		private int					m_iWidth;
		private int					m_iHeight;
		private bool				m_bForceSoftEdges;
		private bool				m_bGenerateLightMaps;
		private string				m_strGenerateLightMapsInputMayaFile;
		private string				m_strGenerateLightMapsOutputLogFile;
		private bool				m_bExport;
		private string				m_strExportInputMayaFile;
		private string				m_strExportOutputLogFile;
		private DoneItCallback		m_cbDoneItCallback;

		private UpdateHtmlLogThread m_BakeSetHtmlLogThread;
		private UpdateHtmlLogThread m_LightMapHtmlLogThread;
		private UpdateHtmlLogThread m_ExportHtmlLogThread;

//		private UpdateHtmlLogThread m_BakeSetHtmlLogThread;
	}
}
