using System;
using System.IO;
using System.Timers;
using System.Threading;
using System.Windows.Forms;
using mshtml;
using RSG.Base;

namespace rageLightMapper
{
	/// <summary>
	/// Summary description for UpdateHtmlLogThread.
	/// </summary>
	public class UpdateHtmlLogThread
	{
		public UpdateHtmlLogThread(System.Windows.Forms.ContainerControl obParentForm, System.Windows.Forms.Control.ControlCollection obParentControlCollection)
		{
			// Initialize the Web Browsers
			m_BakeSetWebBrowser = new AxSHDocVw.AxWebBrowser();

			m_BakeSetWebBrowser.ContainingControl = obParentForm;
			m_BakeSetWebBrowser.Enabled = true;
			m_BakeSetWebBrowser.Location = new System.Drawing.Point(8, 8);
			m_BakeSetWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;

			obParentControlCollection.Add(m_BakeSetWebBrowser);

			m_obLogToMonitor = null;
		}

		public void StartMonitoringLog(rageHtmlLog obHtmlLogToMonitor)
		{
			m_obLogToMonitor = obHtmlLogToMonitor;

			object Dummy = null;

			// Ok this sucks, but using ActiveX controls within .Net is a bit dodgy
			bool bNavigateWorked = false;
			while(!bNavigateWorked)
			{
				try
				{
					bNavigateWorked = true;
					m_BakeSetWebBrowser.Navigate("about:blank", ref Dummy, ref Dummy, ref Dummy, ref Dummy);
				}
				catch(AxHost.InvalidActiveXStateException)
				{
					bNavigateWorked = false;
				}
			}
			

			// Instead of loading the Log when it changes, only check every five seconds
			// otherwise most of the time the Log is constantly reloading 
			// and this is a big hit :(
			m_UpdateWebBrowserTimer = new System.Timers.Timer();
			m_UpdateWebBrowserTimer.Elapsed+=new ElapsedEventHandler(ReloadWebPage);
			m_UpdateWebBrowserTimer.AutoReset = true;

			// Set the Interval to 2.5 seconds.
			m_UpdateWebBrowserTimer.Interval=2500;
			m_UpdateWebBrowserTimer.Enabled=true;
		}

		private void ReloadWebPage(object source, ElapsedEventArgs e)
		{
			if(m_obLogToMonitor == null) return;

			// Make sure the UI editing is thread safe
			// For more info see http://weblogs.asp.net/justin_rogers/articles/126345.aspx
			if (m_BakeSetWebBrowser.InvokeRequired)
			{
				m_BakeSetWebBrowser.BeginInvoke(new MethodInvoker(this.ReloadWebPage));
			}
			else
			{
				ReloadWebPage();
			}
		}

		private void ReloadWebPage()
		{
			if(m_obLogToMonitor == null) return;

			// Update page
			HTMLDocument CurrentDocument = (HTMLDocument)m_BakeSetWebBrowser.Document;
			CurrentDocument.body.innerHTML = m_obLogToMonitor.LogEntriesAsHtmlString +"<SCRIPT LANGUAGE=\"JavaScript\">scroll(0,0)</SCRIPT>";
		}

		private rageHtmlLog	m_obLogToMonitor;
		private rageHtmlLog LogToMonitor
		{
			get
			{
				return m_obLogToMonitor;
			}
			set
			{
				m_obLogToMonitor = value;
			}
		}
		private System.Timers.Timer m_UpdateWebBrowserTimer;
		private AxSHDocVw.AxWebBrowser m_BakeSetWebBrowser;
	}
}
