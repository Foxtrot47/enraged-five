using System;
using RSG.Base.Command;
using RSG.Base.Logging;

namespace rageLightMapper
{
	/// <summary>
	/// Summary description for MayaThread.
	/// </summary>
	public class MayaThread
	{
		public MayaThread(string strMayaBinary, string strMELCommand, string strLogFile)
		{
			LogFile = strLogFile;
			MELCommand = strMELCommand;
			MayaBinary = strMayaBinary;
			m_obMayaBatch = null;
		}

		public void DoIt()
		{
			// Actually do the MEL
			m_obMayaBatch = new rageMayaBatch();
			m_obMayaBatch.SetMayaPathAndFilename(MayaBinary);
			m_obMayaBatch.LogFile = m_strLogFile;
			m_obMayaBatch.SetupEnvironmentFirst = false;
			m_obMayaBatch.UpdateLogFileInRealTime = false;
			m_obMayaBatch.BatchExecuteMel(MELCommand, out m_obStatus);
			m_obMayaBatch = null;
		}


		private string				m_strMELCommand;
		private string MELCommand
		{
			get
			{
				return m_strMELCommand;
			}
			set
			{
				m_strMELCommand = value;
			}
		}
		private string				m_strLogFile;
		private string LogFile
		{
			get
			{
				return m_strLogFile;
			}
			set
			{
				m_strLogFile = value;
			}
		}


		private string				m_strMayaBinary;
		private string MayaBinary
		{
			get
			{
				return m_strMayaBinary;
			}
			set
			{
				m_strMayaBinary = value;
			}
		}

		private rageStatus				m_obStatus;
		public rageStatus ReturnStatus
		{
			get
			{
				return m_obStatus;
			}
		}

		private rageMayaBatch m_obMayaBatch;
		public rageHtmlLog HtmlLog
		{
			get
			{
				if(m_obMayaBatch != null)
				{
					return m_obMayaBatch.HtmlLog;
				}
				else
				{
					return null;
				}
			}
		}
	}
}
