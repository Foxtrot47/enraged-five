using System;
using System.IO;
using System.Timers;
using System.Threading;
using System.Windows.Forms;
using rageUsefulCSharpToolClasses;

namespace rageLightMapper
{
	/// <summary>
	/// Summary description for UpdateHtmlLogThread.
	/// </summary>
	public class UpdateHtmlLogThread
	{
		public UpdateHtmlLogThread(System.Windows.Forms.ContainerControl obParentForm, System.Windows.Forms.Control.ControlCollection obParentControlCollection)
		{
			// Initialize the Web Browsers
			m_BakeSetWebBrowsers = new AxSHDocVw.AxWebBrowser[2];
			m_BakeSetWebBrowsers[0] = new AxSHDocVw.AxWebBrowser();
			m_BakeSetWebBrowsers[1] = new AxSHDocVw.AxWebBrowser();

			m_BakeSetWebBrowsers[0].ContainingControl = obParentForm;
			m_BakeSetWebBrowsers[0].Enabled = true;
			m_BakeSetWebBrowsers[0].Location = new System.Drawing.Point(8, 8);
			m_BakeSetWebBrowsers[0].Dock = System.Windows.Forms.DockStyle.Fill;

			m_BakeSetWebBrowsers[1].ContainingControl = obParentForm;
			m_BakeSetWebBrowsers[1].Enabled = true;
			m_BakeSetWebBrowsers[1].Location = new System.Drawing.Point(8, 8);
			m_BakeSetWebBrowsers[1].Dock = System.Windows.Forms.DockStyle.Fill;

			obParentControlCollection.Add(m_BakeSetWebBrowsers[0]);
			obParentControlCollection.Add(m_BakeSetWebBrowsers[1]);
		}

		public void StartMonitoringFile(string strFileToMonitor)
		{
			FileToMonitor = strFileToMonitor;

			ThreadStart obStartMonitoringFileThreadDelegate = new ThreadStart(StartMonitoringFile);
			Thread obStartMonitoringFileThread = new Thread(obStartMonitoringFileThreadDelegate);
			obStartMonitoringFileThread.Priority = ThreadPriority.Lowest;
			obStartMonitoringFileThread.Start();
		}

		private void StartMonitoringFile()
		{
			// Create a new FileSystemWatcher and set its properties.
			m_obFileSystemWatcher = new FileSystemWatcher();
			m_obFileSystemWatcher.Path = rageFileUtilities.GetLocationFromPath(FileToMonitor);
			m_obFileSystemWatcher.Filter = rageFileUtilities.GetFilenameFromFilePath(FileToMonitor);
			/* Watch for changes in LastAccess and LastWrite times, and 
				the renaming of files or directories. */
			m_obFileSystemWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

			// Add event handlers.
			m_obFileSystemWatcher.Changed += new FileSystemEventHandler(OnFileChanged);
			m_obFileSystemWatcher.Created += new FileSystemEventHandler(OnFileChanged);

			// Begin watching.
			m_obFileSystemWatcher.EnableRaisingEvents = true;
			m_bFileNeedsAReload = true;

			// Instead of loading the file when it changes, only check every five seconds
			// otherwise most of the time the file is constantly reloading 
			// and this is a big hit :(
			m_UpdateWebBrowserTimer = new System.Timers.Timer();
			m_UpdateWebBrowserTimer.Elapsed+=new ElapsedEventHandler(ReloadWebPage);
			m_UpdateWebBrowserTimer.AutoReset = true;
			// Set the Interval to 2.5 seconds.
			m_UpdateWebBrowserTimer.Interval=2500;
			m_UpdateWebBrowserTimer.Enabled=true;
			m_iBakeSetWebBrowserDoubleBufferNumber = 0;
		}

//		public void StopMonitoringFile()
//		{
//			m_UpdateWebBrowserTimer.AutoReset = false;
//			m_bFileNeedsAReload = true;
//		}

		// Define the event handlers.
		private void OnFileChanged(object source, FileSystemEventArgs e)
		{
			if(!File.Exists(FileToMonitor)) return;
			// Specify what is done when a file is changed, created, or deleted.
			// Console.WriteLine("File: " +  e.FullPath + " " + e.ChangeType);
			m_bFileNeedsAReload = true;
		}

		private void ReloadWebPage(object source, ElapsedEventArgs e)
		{
			if(!File.Exists(FileToMonitor)) return;
			// Make sure the UI editing is thread safe
			// For more info see http://weblogs.asp.net/justin_rogers/articles/126345.aspx
			if (m_BakeSetWebBrowsers[m_iBakeSetWebBrowserDoubleBufferNumber].InvokeRequired)
			{
				m_BakeSetWebBrowsers[m_iBakeSetWebBrowserDoubleBufferNumber].BeginInvoke(new MethodInvoker(this.ReloadWebPage));
			}
			else
			{
				ReloadWebPage();
			}
		}

		private void ReloadWebPage()
		{
			if(!File.Exists(FileToMonitor)) return;
			if(m_bFileNeedsAReload)
			{
				System.Object nullObject = 0;
				string str = "";
				System.Object nullObjStr = str;

				// Reload page
				m_BakeSetWebBrowsers[m_iBakeSetWebBrowserDoubleBufferNumber].SendToBack();
				m_BakeSetWebBrowsers[m_iBakeSetWebBrowserDoubleBufferNumber].Navigate(FileToMonitor +"#LogEnd", ref nullObject, ref nullObjStr, ref nullObjStr, ref nullObjStr);

				// Switch buffer
				m_iBakeSetWebBrowserDoubleBufferNumber = 1 - m_iBakeSetWebBrowserDoubleBufferNumber;

				// File loaded, so no need to check
				m_bFileNeedsAReload = false;

				// In a couple of seconds, switch which webbrowser is the front one
				m_SwitchWebBrowsersTimer = new System.Timers.Timer();
				m_SwitchWebBrowsersTimer.Elapsed += new ElapsedEventHandler(SwitchWebBrowsers);
				m_SwitchWebBrowsersTimer.AutoReset = false;
				// Set the Interval to 2 seconds.
				m_SwitchWebBrowsersTimer.Interval=2000;
				m_SwitchWebBrowsersTimer.Enabled=true;
			}
		}

		private void SwitchWebBrowsers(object source, ElapsedEventArgs e)
		{
			if(!File.Exists(FileToMonitor)) return;
			// Make sure the UI editing is thread safe
			// For more info see http://weblogs.asp.net/justin_rogers/articles/126345.aspx
			if (m_BakeSetWebBrowsers[m_iBakeSetWebBrowserDoubleBufferNumber].InvokeRequired)
			{
				m_BakeSetWebBrowsers[m_iBakeSetWebBrowserDoubleBufferNumber].BeginInvoke(new MethodInvoker(this.SwitchWebBrowsers));
			}
			else
			{
				SwitchWebBrowsers();
			}			
		}

		private void SwitchWebBrowsers()
		{
			m_BakeSetWebBrowsers[m_iBakeSetWebBrowserDoubleBufferNumber].SendToBack();
		}

		private string				m_strFileToMonitor;
		private string FileToMonitor
		{
			get
			{
				return m_strFileToMonitor;
			}
			set
			{
				m_strFileToMonitor = value;
			}
		}
		private System.Timers.Timer m_UpdateWebBrowserTimer;
		private System.Timers.Timer m_SwitchWebBrowsersTimer;
		private AxSHDocVw.AxWebBrowser[] m_BakeSetWebBrowsers;
		private int m_iBakeSetWebBrowserDoubleBufferNumber;

		private System.IO.FileSystemWatcher m_obFileSystemWatcher;
		private bool m_bFileNeedsAReload;
	}
}
