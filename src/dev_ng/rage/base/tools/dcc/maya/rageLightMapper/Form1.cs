using System;
using System.IO;
using System.Xml;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using RSG.Base;
using rageLightMapper;

namespace rageLightMapper
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox checkBoxGenerateBakesets;
		private System.Windows.Forms.TextBox textBoxBakeSetInputFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private System.Windows.Forms.TextBox textBoxBakeSetOutputFile;
		private System.Windows.Forms.Button buttonBakeSetOutputFile;
		private System.Windows.Forms.Label labelBakeSetOutputFile;
		private System.Windows.Forms.TextBox textBoxBakeSetLogFile;
		private System.Windows.Forms.Button buttonBakeSetLogFile;
		private System.Windows.Forms.Label labelBakeSetLogFile;
		private System.Windows.Forms.Button buttonGenerateLightMapsInputFile;
		private System.Windows.Forms.TextBox textBoxGenerateLightMapsInputFile;
		private System.Windows.Forms.Label labelGenerateLightMapsInputFile;
		private System.Windows.Forms.TextBox textBoxGenerateLightMapsLogFile;
		private System.Windows.Forms.Button buttonGenerateLightMapsLogFile;
		private System.Windows.Forms.Label labelGenerateLightMapsLogFile;
		private System.Windows.Forms.Button buttonExportInputFile;
		private System.Windows.Forms.TextBox textBoxExportInputFile;
		private System.Windows.Forms.Label labelExportInputFile;
		private System.Windows.Forms.TextBox textBoxExportLogFile;
		private System.Windows.Forms.Button buttonExportLogFile;
		private System.Windows.Forms.Label labelExportLogFile;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button buttonDoIt;
		private System.Windows.Forms.TextBox textBoxMayaBinary;
		private System.Windows.Forms.CheckBox checkBoxGenerateLightMaps;
		private System.Windows.Forms.CheckBox checkBoxExport;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.NumericUpDown numericUpDownNoOfBakeSets;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numericUpDownWidth;
		private System.Windows.Forms.NumericUpDown numericUpDownHeight;
		private System.Windows.Forms.Label label4;
		private UpdateHtmlLogThread m_BakeSetHtmlLogThread;
		private UpdateHtmlLogThread m_LightMapHtmlLogThread;
		private System.Windows.Forms.CheckBox checkBoxSoftEdges;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItemSaveSettings;
		private System.Windows.Forms.MenuItem menuItemOpenSettings;
		private System.Windows.Forms.MenuItem menuItemExit;
		private UpdateHtmlLogThread m_ExportHtmlLogThread;
		private System.Windows.Forms.MenuItem menuItemHelpMenu;
		private System.Windows.Forms.MenuItem menuItemLaunchWiki;
		private System.Windows.Forms.MenuItem menuItemAbout;
		private bool m_bBatchMode;

		public Form1(string[] astrArgs)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//			m_iBakeSetWebBrowserDoubleBufferNumber = 0;
			//			BakeSetWebBrowser0.DownloadComplete += new System.EventHandler(this.BakeSetWebBrowserLoaded);
			//			BakeSetWebBrowser1.DownloadComplete += new System.EventHandler(this.BakeSetWebBrowserLoaded);

			// Disable the DoIt button
			buttonDoIt.Enabled = false;

			// Take a guess at the good Maya path
			this.textBoxMayaBinary.Text = "C:/Program Files/Alias/Maya7.0/bin/mayabatch.exe";
			if(Environment.GetEnvironmentVariable("MAYA_DEV_PATH_6_0") != null)
			{
				this.textBoxMayaBinary.Text = Environment.GetEnvironmentVariable("MAYA_DEV_PATH_6_0") +"/bin/mayabatch.exe";
			}
			this.textBoxMayaBinary.Text = this.textBoxMayaBinary.Text.Replace("\\", "/");

			m_BakeSetHtmlLogThread = new UpdateHtmlLogThread((this as System.Windows.Forms.ContainerControl), tabPage1.Controls);
			m_LightMapHtmlLogThread = new UpdateHtmlLogThread((this as System.Windows.Forms.ContainerControl), tabPage2.Controls);
			m_ExportHtmlLogThread = new UpdateHtmlLogThread((this as System.Windows.Forms.ContainerControl), tabPage3.Controls);

			// Parse the args
			m_bBatchMode = false;
			for(int i=0; i<astrArgs.Length; i++)
			{
				if(astrArgs[i] == "-batch")
				{
					m_bBatchMode = true;
				}
				else if(astrArgs[i] == "-settingsFile")
				{
					LoadSettings(astrArgs[i + 1]);
				}
			}

			if(m_bBatchMode)
			{
				DoIt();
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.textBoxBakeSetInputFile = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkBoxSoftEdges = new System.Windows.Forms.CheckBox();
			this.numericUpDownHeight = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.numericUpDownWidth = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.textBoxBakeSetLogFile = new System.Windows.Forms.TextBox();
			this.buttonBakeSetLogFile = new System.Windows.Forms.Button();
			this.labelBakeSetLogFile = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.numericUpDownNoOfBakeSets = new System.Windows.Forms.NumericUpDown();
			this.textBoxBakeSetOutputFile = new System.Windows.Forms.TextBox();
			this.buttonBakeSetOutputFile = new System.Windows.Forms.Button();
			this.labelBakeSetOutputFile = new System.Windows.Forms.Label();
			this.checkBoxGenerateBakesets = new System.Windows.Forms.CheckBox();
			this.checkBoxGenerateLightMaps = new System.Windows.Forms.CheckBox();
			this.checkBoxExport = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.buttonGenerateLightMapsInputFile = new System.Windows.Forms.Button();
			this.textBoxGenerateLightMapsInputFile = new System.Windows.Forms.TextBox();
			this.labelGenerateLightMapsInputFile = new System.Windows.Forms.Label();
			this.textBoxGenerateLightMapsLogFile = new System.Windows.Forms.TextBox();
			this.buttonGenerateLightMapsLogFile = new System.Windows.Forms.Button();
			this.labelGenerateLightMapsLogFile = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.buttonExportInputFile = new System.Windows.Forms.Button();
			this.textBoxExportInputFile = new System.Windows.Forms.TextBox();
			this.labelExportInputFile = new System.Windows.Forms.Label();
			this.labelExportLogFile = new System.Windows.Forms.Label();
			this.textBoxExportLogFile = new System.Windows.Forms.TextBox();
			this.buttonExportLogFile = new System.Windows.Forms.Button();
			this.buttonDoIt = new System.Windows.Forms.Button();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.button6 = new System.Windows.Forms.Button();
			this.textBoxMayaBinary = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItemOpenSettings = new System.Windows.Forms.MenuItem();
			this.menuItemSaveSettings = new System.Windows.Forms.MenuItem();
			this.menuItemExit = new System.Windows.Forms.MenuItem();
			this.menuItemHelpMenu = new System.Windows.Forms.MenuItem();
			this.menuItemLaunchWiki = new System.Windows.Forms.MenuItem();
			this.menuItemAbout = new System.Windows.Forms.MenuItem();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownHeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownWidth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoOfBakeSets)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.SuspendLayout();
			// 
			// textBoxBakeSetInputFile
			// 
			this.textBoxBakeSetInputFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxBakeSetInputFile.Location = new System.Drawing.Point(100, 20);
			this.textBoxBakeSetInputFile.Name = "textBoxBakeSetInputFile";
			this.textBoxBakeSetInputFile.Size = new System.Drawing.Size(710, 20);
			this.textBoxBakeSetInputFile.TabIndex = 0;
			this.textBoxBakeSetInputFile.Text = "";
			this.textBoxBakeSetInputFile.TextChanged += new System.EventHandler(this.textBoxBakeSetInputFile_TextChanged);
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
			this.button1.Location = new System.Drawing.Point(818, 18);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(32, 23);
			this.button1.TabIndex = 1;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(10, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(88, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "Input Maya File";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.checkBoxSoftEdges);
			this.groupBox1.Controls.Add(this.numericUpDownHeight);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.numericUpDownWidth);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.textBoxBakeSetLogFile);
			this.groupBox1.Controls.Add(this.buttonBakeSetLogFile);
			this.groupBox1.Controls.Add(this.labelBakeSetLogFile);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.numericUpDownNoOfBakeSets);
			this.groupBox1.Controls.Add(this.textBoxBakeSetOutputFile);
			this.groupBox1.Controls.Add(this.buttonBakeSetOutputFile);
			this.groupBox1.Controls.Add(this.labelBakeSetOutputFile);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.textBoxBakeSetInputFile);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(8, 88);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(856, 144);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "BakeSet Generation";
			// 
			// checkBoxSoftEdges
			// 
			this.checkBoxSoftEdges.Location = new System.Drawing.Point(220, 48);
			this.checkBoxSoftEdges.Name = "checkBoxSoftEdges";
			this.checkBoxSoftEdges.Size = new System.Drawing.Size(124, 24);
			this.checkBoxSoftEdges.TabIndex = 17;
			this.checkBoxSoftEdges.Text = "Force Soft Edges";
			// 
			// numericUpDownHeight
			// 
			this.numericUpDownHeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.numericUpDownHeight.Location = new System.Drawing.Point(746, 50);
			this.numericUpDownHeight.Maximum = new System.Decimal(new int[] {
																				4096,
																				0,
																				0,
																				0});
			this.numericUpDownHeight.Name = "numericUpDownHeight";
			this.numericUpDownHeight.Size = new System.Drawing.Size(64, 20);
			this.numericUpDownHeight.TabIndex = 16;
			this.numericUpDownHeight.Value = new System.Decimal(new int[] {
																			  512,
																			  0,
																			  0,
																			  0});
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label4.Location = new System.Drawing.Point(700, 52);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(44, 16);
			this.label4.TabIndex = 15;
			this.label4.Text = "Height";
			this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// numericUpDownWidth
			// 
			this.numericUpDownWidth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.numericUpDownWidth.Location = new System.Drawing.Point(600, 50);
			this.numericUpDownWidth.Maximum = new System.Decimal(new int[] {
																			   4096,
																			   0,
																			   0,
																			   0});
			this.numericUpDownWidth.Name = "numericUpDownWidth";
			this.numericUpDownWidth.Size = new System.Drawing.Size(64, 20);
			this.numericUpDownWidth.TabIndex = 14;
			this.numericUpDownWidth.Value = new System.Decimal(new int[] {
																			 512,
																			 0,
																			 0,
																			 0});
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label2.Location = new System.Drawing.Point(554, 52);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(46, 16);
			this.label2.TabIndex = 13;
			this.label2.Text = "Width";
			this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// textBoxBakeSetLogFile
			// 
			this.textBoxBakeSetLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxBakeSetLogFile.Location = new System.Drawing.Point(100, 110);
			this.textBoxBakeSetLogFile.Name = "textBoxBakeSetLogFile";
			this.textBoxBakeSetLogFile.Size = new System.Drawing.Size(710, 20);
			this.textBoxBakeSetLogFile.TabIndex = 9;
			this.textBoxBakeSetLogFile.Text = "";
			this.textBoxBakeSetLogFile.TextChanged += new System.EventHandler(this.textBoxBakeSetLogFile_TextChanged);
			// 
			// buttonBakeSetLogFile
			// 
			this.buttonBakeSetLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonBakeSetLogFile.Image = ((System.Drawing.Image)(resources.GetObject("buttonBakeSetLogFile.Image")));
			this.buttonBakeSetLogFile.Location = new System.Drawing.Point(818, 108);
			this.buttonBakeSetLogFile.Name = "buttonBakeSetLogFile";
			this.buttonBakeSetLogFile.Size = new System.Drawing.Size(32, 23);
			this.buttonBakeSetLogFile.TabIndex = 10;
			// 
			// labelBakeSetLogFile
			// 
			this.labelBakeSetLogFile.Location = new System.Drawing.Point(10, 112);
			this.labelBakeSetLogFile.Name = "labelBakeSetLogFile";
			this.labelBakeSetLogFile.Size = new System.Drawing.Size(96, 16);
			this.labelBakeSetLogFile.TabIndex = 11;
			this.labelBakeSetLogFile.Text = "Log File";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(10, 52);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(137, 23);
			this.label3.TabIndex = 8;
			this.label3.Text = "Maximun No Of Bake Sets";
			// 
			// numericUpDownNoOfBakeSets
			// 
			this.numericUpDownNoOfBakeSets.Location = new System.Drawing.Point(150, 50);
			this.numericUpDownNoOfBakeSets.Name = "numericUpDownNoOfBakeSets";
			this.numericUpDownNoOfBakeSets.Size = new System.Drawing.Size(48, 20);
			this.numericUpDownNoOfBakeSets.TabIndex = 7;
			this.numericUpDownNoOfBakeSets.Value = new System.Decimal(new int[] {
																					5,
																					0,
																					0,
																					0});
			// 
			// textBoxBakeSetOutputFile
			// 
			this.textBoxBakeSetOutputFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxBakeSetOutputFile.Location = new System.Drawing.Point(100, 80);
			this.textBoxBakeSetOutputFile.Name = "textBoxBakeSetOutputFile";
			this.textBoxBakeSetOutputFile.Size = new System.Drawing.Size(710, 20);
			this.textBoxBakeSetOutputFile.TabIndex = 4;
			this.textBoxBakeSetOutputFile.Text = "";
			this.textBoxBakeSetOutputFile.TextChanged += new System.EventHandler(this.textBoxBakeSetOutputFile_TextChanged);
			// 
			// buttonBakeSetOutputFile
			// 
			this.buttonBakeSetOutputFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonBakeSetOutputFile.Image = ((System.Drawing.Image)(resources.GetObject("buttonBakeSetOutputFile.Image")));
			this.buttonBakeSetOutputFile.Location = new System.Drawing.Point(818, 78);
			this.buttonBakeSetOutputFile.Name = "buttonBakeSetOutputFile";
			this.buttonBakeSetOutputFile.Size = new System.Drawing.Size(32, 23);
			this.buttonBakeSetOutputFile.TabIndex = 5;
			this.buttonBakeSetOutputFile.Click += new System.EventHandler(this.button2_Click);
			// 
			// labelBakeSetOutputFile
			// 
			this.labelBakeSetOutputFile.Location = new System.Drawing.Point(10, 82);
			this.labelBakeSetOutputFile.Name = "labelBakeSetOutputFile";
			this.labelBakeSetOutputFile.Size = new System.Drawing.Size(96, 16);
			this.labelBakeSetOutputFile.TabIndex = 6;
			this.labelBakeSetOutputFile.Text = "Output Maya File";
			// 
			// checkBoxGenerateBakesets
			// 
			this.checkBoxGenerateBakesets.Checked = true;
			this.checkBoxGenerateBakesets.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxGenerateBakesets.Location = new System.Drawing.Point(8, 0);
			this.checkBoxGenerateBakesets.Name = "checkBoxGenerateBakesets";
			this.checkBoxGenerateBakesets.Size = new System.Drawing.Size(120, 24);
			this.checkBoxGenerateBakesets.TabIndex = 4;
			this.checkBoxGenerateBakesets.Text = "Generate Bakesets";
			this.checkBoxGenerateBakesets.CheckedChanged += new System.EventHandler(this.checkBoxGenerateBakesets_CheckedChanged);
			// 
			// checkBoxGenerateLightMaps
			// 
			this.checkBoxGenerateLightMaps.Checked = true;
			this.checkBoxGenerateLightMaps.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxGenerateLightMaps.Location = new System.Drawing.Point(136, 0);
			this.checkBoxGenerateLightMaps.Name = "checkBoxGenerateLightMaps";
			this.checkBoxGenerateLightMaps.Size = new System.Drawing.Size(144, 24);
			this.checkBoxGenerateLightMaps.TabIndex = 5;
			this.checkBoxGenerateLightMaps.Text = "Generate LightMaps";
			this.checkBoxGenerateLightMaps.CheckedChanged += new System.EventHandler(this.checkBoxGenerateLightMaps_CheckedChanged);
			// 
			// checkBoxExport
			// 
			this.checkBoxExport.Checked = true;
			this.checkBoxExport.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxExport.Location = new System.Drawing.Point(288, 0);
			this.checkBoxExport.Name = "checkBoxExport";
			this.checkBoxExport.Size = new System.Drawing.Size(144, 24);
			this.checkBoxExport.TabIndex = 6;
			this.checkBoxExport.Text = "Export";
			this.checkBoxExport.CheckedChanged += new System.EventHandler(this.checkBoxExport_CheckedChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.buttonGenerateLightMapsInputFile);
			this.groupBox2.Controls.Add(this.textBoxGenerateLightMapsInputFile);
			this.groupBox2.Controls.Add(this.labelGenerateLightMapsInputFile);
			this.groupBox2.Controls.Add(this.textBoxGenerateLightMapsLogFile);
			this.groupBox2.Controls.Add(this.buttonGenerateLightMapsLogFile);
			this.groupBox2.Controls.Add(this.labelGenerateLightMapsLogFile);
			this.groupBox2.Location = new System.Drawing.Point(8, 240);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(856, 80);
			this.groupBox2.TabIndex = 9;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "LightMap Generation";
			// 
			// buttonGenerateLightMapsInputFile
			// 
			this.buttonGenerateLightMapsInputFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonGenerateLightMapsInputFile.Image = ((System.Drawing.Image)(resources.GetObject("buttonGenerateLightMapsInputFile.Image")));
			this.buttonGenerateLightMapsInputFile.Location = new System.Drawing.Point(818, 18);
			this.buttonGenerateLightMapsInputFile.Name = "buttonGenerateLightMapsInputFile";
			this.buttonGenerateLightMapsInputFile.Size = new System.Drawing.Size(32, 23);
			this.buttonGenerateLightMapsInputFile.TabIndex = 1;
			this.buttonGenerateLightMapsInputFile.Click += new System.EventHandler(this.buttonGenerateLightMapsInputFile_Click);
			// 
			// textBoxGenerateLightMapsInputFile
			// 
			this.textBoxGenerateLightMapsInputFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxGenerateLightMapsInputFile.Location = new System.Drawing.Point(104, 20);
			this.textBoxGenerateLightMapsInputFile.Name = "textBoxGenerateLightMapsInputFile";
			this.textBoxGenerateLightMapsInputFile.Size = new System.Drawing.Size(710, 20);
			this.textBoxGenerateLightMapsInputFile.TabIndex = 0;
			this.textBoxGenerateLightMapsInputFile.Text = "";
			this.textBoxGenerateLightMapsInputFile.TextChanged += new System.EventHandler(this.textBoxGenerateLightMapsInputFile_TextChanged);
			// 
			// labelGenerateLightMapsInputFile
			// 
			this.labelGenerateLightMapsInputFile.Location = new System.Drawing.Point(10, 22);
			this.labelGenerateLightMapsInputFile.Name = "labelGenerateLightMapsInputFile";
			this.labelGenerateLightMapsInputFile.Size = new System.Drawing.Size(88, 16);
			this.labelGenerateLightMapsInputFile.TabIndex = 2;
			this.labelGenerateLightMapsInputFile.Text = "Input Maya File";
			// 
			// textBoxGenerateLightMapsLogFile
			// 
			this.textBoxGenerateLightMapsLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxGenerateLightMapsLogFile.Location = new System.Drawing.Point(104, 50);
			this.textBoxGenerateLightMapsLogFile.Name = "textBoxGenerateLightMapsLogFile";
			this.textBoxGenerateLightMapsLogFile.Size = new System.Drawing.Size(710, 20);
			this.textBoxGenerateLightMapsLogFile.TabIndex = 12;
			this.textBoxGenerateLightMapsLogFile.Text = "";
			this.textBoxGenerateLightMapsLogFile.TextChanged += new System.EventHandler(this.textBoxGenerateLightMapsLogFile_TextChanged);
			// 
			// buttonGenerateLightMapsLogFile
			// 
			this.buttonGenerateLightMapsLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonGenerateLightMapsLogFile.Image = ((System.Drawing.Image)(resources.GetObject("buttonGenerateLightMapsLogFile.Image")));
			this.buttonGenerateLightMapsLogFile.Location = new System.Drawing.Point(818, 48);
			this.buttonGenerateLightMapsLogFile.Name = "buttonGenerateLightMapsLogFile";
			this.buttonGenerateLightMapsLogFile.Size = new System.Drawing.Size(32, 23);
			this.buttonGenerateLightMapsLogFile.TabIndex = 13;
			// 
			// labelGenerateLightMapsLogFile
			// 
			this.labelGenerateLightMapsLogFile.Location = new System.Drawing.Point(8, 52);
			this.labelGenerateLightMapsLogFile.Name = "labelGenerateLightMapsLogFile";
			this.labelGenerateLightMapsLogFile.Size = new System.Drawing.Size(96, 16);
			this.labelGenerateLightMapsLogFile.TabIndex = 14;
			this.labelGenerateLightMapsLogFile.Text = "Log File";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.buttonExportInputFile);
			this.groupBox3.Controls.Add(this.textBoxExportInputFile);
			this.groupBox3.Controls.Add(this.labelExportInputFile);
			this.groupBox3.Controls.Add(this.labelExportLogFile);
			this.groupBox3.Controls.Add(this.textBoxExportLogFile);
			this.groupBox3.Controls.Add(this.buttonExportLogFile);
			this.groupBox3.Location = new System.Drawing.Point(8, 328);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(856, 80);
			this.groupBox3.TabIndex = 10;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Export";
			// 
			// buttonExportInputFile
			// 
			this.buttonExportInputFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonExportInputFile.Image = ((System.Drawing.Image)(resources.GetObject("buttonExportInputFile.Image")));
			this.buttonExportInputFile.Location = new System.Drawing.Point(818, 18);
			this.buttonExportInputFile.Name = "buttonExportInputFile";
			this.buttonExportInputFile.Size = new System.Drawing.Size(32, 23);
			this.buttonExportInputFile.TabIndex = 1;
			this.buttonExportInputFile.Click += new System.EventHandler(this.button3_Click);
			// 
			// textBoxExportInputFile
			// 
			this.textBoxExportInputFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxExportInputFile.Location = new System.Drawing.Point(100, 20);
			this.textBoxExportInputFile.Name = "textBoxExportInputFile";
			this.textBoxExportInputFile.Size = new System.Drawing.Size(710, 20);
			this.textBoxExportInputFile.TabIndex = 0;
			this.textBoxExportInputFile.Text = "";
			this.textBoxExportInputFile.TextChanged += new System.EventHandler(this.textBoxExportInputFile_TextChanged);
			// 
			// labelExportInputFile
			// 
			this.labelExportInputFile.Location = new System.Drawing.Point(10, 22);
			this.labelExportInputFile.Name = "labelExportInputFile";
			this.labelExportInputFile.Size = new System.Drawing.Size(88, 16);
			this.labelExportInputFile.TabIndex = 2;
			this.labelExportInputFile.Text = "Input Maya File";
			// 
			// labelExportLogFile
			// 
			this.labelExportLogFile.Location = new System.Drawing.Point(10, 52);
			this.labelExportLogFile.Name = "labelExportLogFile";
			this.labelExportLogFile.Size = new System.Drawing.Size(78, 16);
			this.labelExportLogFile.TabIndex = 17;
			this.labelExportLogFile.Text = "Log File";
			// 
			// textBoxExportLogFile
			// 
			this.textBoxExportLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxExportLogFile.Location = new System.Drawing.Point(100, 50);
			this.textBoxExportLogFile.Name = "textBoxExportLogFile";
			this.textBoxExportLogFile.Size = new System.Drawing.Size(710, 20);
			this.textBoxExportLogFile.TabIndex = 15;
			this.textBoxExportLogFile.Text = "";
			this.textBoxExportLogFile.TextChanged += new System.EventHandler(this.textBoxExportLogFile_TextChanged);
			// 
			// buttonExportLogFile
			// 
			this.buttonExportLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonExportLogFile.Image = ((System.Drawing.Image)(resources.GetObject("buttonExportLogFile.Image")));
			this.buttonExportLogFile.Location = new System.Drawing.Point(818, 48);
			this.buttonExportLogFile.Name = "buttonExportLogFile";
			this.buttonExportLogFile.Size = new System.Drawing.Size(32, 23);
			this.buttonExportLogFile.TabIndex = 16;
			// 
			// buttonDoIt
			// 
			this.buttonDoIt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.buttonDoIt.Location = new System.Drawing.Point(8, 872);
			this.buttonDoIt.Name = "buttonDoIt";
			this.buttonDoIt.Size = new System.Drawing.Size(856, 24);
			this.buttonDoIt.TabIndex = 11;
			this.buttonDoIt.Text = "Do It!";
			this.buttonDoIt.Click += new System.EventHandler(this.buttonDoIt_Click);
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox4.Controls.Add(this.button6);
			this.groupBox4.Controls.Add(this.textBoxMayaBinary);
			this.groupBox4.Controls.Add(this.label5);
			this.groupBox4.Location = new System.Drawing.Point(8, 24);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(856, 56);
			this.groupBox4.TabIndex = 11;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "General";
			// 
			// button6
			// 
			this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
			this.button6.Location = new System.Drawing.Point(818, 18);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(32, 23);
			this.button6.TabIndex = 1;
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// textBoxMayaBinary
			// 
			this.textBoxMayaBinary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxMayaBinary.Location = new System.Drawing.Point(100, 20);
			this.textBoxMayaBinary.Name = "textBoxMayaBinary";
			this.textBoxMayaBinary.Size = new System.Drawing.Size(710, 20);
			this.textBoxMayaBinary.TabIndex = 0;
			this.textBoxMayaBinary.Text = "";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(10, 22);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(88, 16);
			this.label5.TabIndex = 2;
			this.label5.Text = "Maya";
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Location = new System.Drawing.Point(8, 416);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(856, 448);
			this.tabControl1.TabIndex = 15;
			// 
			// tabPage1
			// 
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(848, 422);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "BakeSet Generation";
			// 
			// tabPage2
			// 
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(848, 422);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "LightMap Generation";
			// 
			// tabPage3
			// 
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(848, 422);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Export";
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.menuItemHelpMenu});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItemOpenSettings,
																					  this.menuItemSaveSettings,
																					  this.menuItemExit});
			this.menuItem1.Text = "File";
			// 
			// menuItemOpenSettings
			// 
			this.menuItemOpenSettings.Index = 0;
			this.menuItemOpenSettings.Text = "Open Settings";
			this.menuItemOpenSettings.Click += new System.EventHandler(this.menuItemOpenSettings_Click);
			// 
			// menuItemSaveSettings
			// 
			this.menuItemSaveSettings.Index = 1;
			this.menuItemSaveSettings.Text = "Save Settings";
			this.menuItemSaveSettings.Click += new System.EventHandler(this.menuItemSaveSettings_Click);
			// 
			// menuItemExit
			// 
			this.menuItemExit.Index = 2;
			this.menuItemExit.Text = "Exit";
			this.menuItemExit.Click += new System.EventHandler(this.menuItemExit_Click);
			// 
			// menuItemHelpMenu
			// 
			this.menuItemHelpMenu.Index = 1;
			this.menuItemHelpMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							 this.menuItemLaunchWiki,
																							 this.menuItemAbout});
			this.menuItemHelpMenu.Text = "Help";
			// 
			// menuItemLaunchWiki
			// 
			this.menuItemLaunchWiki.Index = 0;
			this.menuItemLaunchWiki.Text = "Light Mapping On The Wiki";
			this.menuItemLaunchWiki.Click += new System.EventHandler(this.menuItemLaunchWiki_Click);
			// 
			// menuItemAbout
			// 
			this.menuItemAbout.Index = 1;
			this.menuItemAbout.Text = "About";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(872, 901);
			this.Controls.Add(this.checkBoxExport);
			this.Controls.Add(this.checkBoxGenerateLightMaps);
			this.Controls.Add(this.checkBoxGenerateBakesets);
			this.Controls.Add(this.buttonDoIt);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.tabControl1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.Text = "rageLightMapper";
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownHeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownWidth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoOfBakeSets)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] astrArgs) 
		{
			Application.Run(new Form1(astrArgs));
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog openFileDialog2 = new OpenFileDialog();
			//			openFileDialog2.InitialDirectory = "T:\\rage\\tools\\Modules\\base\\rage\\exes\\" ;
			openFileDialog2.Filter = "Maya binary files (*.mb)|*.mb|Maya acsii files (*.ma)|*.ma|All files (*.*)|*.*";
			openFileDialog2.FilterIndex = 1;
			openFileDialog2.RestoreDirectory = false;
			openFileDialog2.Multiselect = false;
			openFileDialog2.Title = ("Source Maya File");

			if(openFileDialog2.ShowDialog() != DialogResult.OK)
			{
				// They quit
				return;
			}
			textBoxBakeSetInputFile.Text = openFileDialog2.FileName;
		}

		private void checkBoxGenerateBakesets_CheckedChanged(object sender, System.EventArgs e)
		{
			if(checkBoxGenerateBakesets.Checked)
			{
				groupBox1.Enabled = true;
			}
			else
			{
				groupBox1.Enabled = false;
			}
			EnableDoItButtonIfValid();
		}

		private void textBoxBakeSetInputFile_TextChanged(object sender, System.EventArgs e)
		{
			textBoxBakeSetOutputFile.Text = rageFileUtilities.RemoveFileExtension(textBoxBakeSetInputFile.Text)+ "_withBakeSets.mb";
			EnableDoItButtonIfValid();
		}

		private void checkBoxGenerateLightMaps_CheckedChanged(object sender, System.EventArgs e)
		{
			if(checkBoxGenerateLightMaps.Checked)
			{
				groupBox2.Enabled = true;
			}
			else
			{
				groupBox2.Enabled = false;
			}
			EnableDoItButtonIfValid();
		}

		private void checkBoxExport_CheckedChanged(object sender, System.EventArgs e)
		{
			if(checkBoxExport.Checked)
			{
				groupBox3.Enabled = true;
			}
			else
			{
				groupBox3.Enabled = false;
			}
			EnableDoItButtonIfValid();
		}

		private void button6_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog openFileDialog2 = new OpenFileDialog();
			//			openFileDialog2.InitialDirectory = "T:\\rage\\tools\\Modules\\base\\rage\\exes\\" ;
			openFileDialog2.Filter = "Maya batch|mayabatch.exe";
			openFileDialog2.FilterIndex = 1;
			openFileDialog2.RestoreDirectory = false;
			openFileDialog2.Multiselect = false;
			openFileDialog2.Title = ("Source Maya File");

			if(openFileDialog2.ShowDialog() != DialogResult.OK)
			{
				// They quit
				return;
			}
			textBoxMayaBinary.Text = openFileDialog2.FileName;
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog openFileDialog2 = new OpenFileDialog();
			//			openFileDialog2.InitialDirectory = "T:\\rage\\tools\\Modules\\base\\rage\\exes\\" ;
			openFileDialog2.Filter = "Maya binary files (*.mb)|*.mb|Maya acsii files (*.ma)|*.ma|All files (*.*)|*.*";
			openFileDialog2.FilterIndex = 1;
			openFileDialog2.RestoreDirectory = false;
			openFileDialog2.Multiselect = false;
			openFileDialog2.Title = ("Output Maya File");

			if(openFileDialog2.ShowDialog() != DialogResult.OK)
			{
				// They quit
				return;
			}
			textBoxBakeSetInputFile.Text = openFileDialog2.FileName;
		}

		private void buttonGenerateLightMapsInputFile_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog openFileDialog2 = new OpenFileDialog();
			//			openFileDialog2.InitialDirectory = "T:\\rage\\tools\\Modules\\base\\rage\\exes\\" ;
			openFileDialog2.Filter = "Maya binary files (*.mb)|*.mb|Maya acsii files (*.ma)|*.ma|All files (*.*)|*.*";
			openFileDialog2.FilterIndex = 1;
			openFileDialog2.RestoreDirectory = false;
			openFileDialog2.Multiselect = false;
			openFileDialog2.Title = ("Source Maya File");

			if(openFileDialog2.ShowDialog() != DialogResult.OK)
			{
				// They quit
				return;
			}
			textBoxGenerateLightMapsInputFile.Text = openFileDialog2.FileName;
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog openFileDialog2 = new OpenFileDialog();
			//			openFileDialog2.InitialDirectory = "T:\\rage\\tools\\Modules\\base\\rage\\exes\\" ;
			openFileDialog2.Filter = "Maya binary files (*.mb)|*.mb|Maya acsii files (*.ma)|*.ma|All files (*.*)|*.*";
			openFileDialog2.FilterIndex = 1;
			openFileDialog2.RestoreDirectory = false;
			openFileDialog2.Multiselect = false;
			openFileDialog2.Title = ("Source Maya File");

			if(openFileDialog2.ShowDialog() != DialogResult.OK)
			{
				// They quit
				return;
			}
			textBoxExportInputFile.Text = openFileDialog2.FileName;
		}

		private void buttonDoIt_Click(object sender, System.EventArgs e)
		{
			// Do it!!!!
			DoIt();
		}

		// Modifiers
		private void DoIt()
		{
			// Disable UI
			groupBox1.Enabled = false;
			groupBox2.Enabled = false;
			groupBox3.Enabled = false;
			groupBox4.Enabled = false;
			buttonDoIt.Enabled = false;

			// Force an update on all the tabs
			for(int i=0; i<tabControl1.TabCount; i++)
			{
				tabControl1.SelectedIndex = i;
			}
			tabControl1.SelectedIndex = 0;
			
			// Kick it off
			DoItThread obDoItThread = new DoItThread(textBoxMayaBinary.Text, 
				// Bakeset Generation
				checkBoxGenerateBakesets.Checked, textBoxBakeSetInputFile.Text, textBoxBakeSetOutputFile.Text, textBoxBakeSetLogFile.Text, 
				(int)numericUpDownNoOfBakeSets.Value, (int)numericUpDownWidth.Value, (int)numericUpDownHeight.Value, checkBoxSoftEdges.Checked, 

				// Lightmap Generation
				checkBoxGenerateLightMaps.Checked, textBoxGenerateLightMapsInputFile.Text, textBoxGenerateLightMapsLogFile.Text, 

				// Export
				checkBoxExport.Checked, textBoxExportInputFile.Text, textBoxExportLogFile.Text,

				// Other stuff
				new DoneItCallback(DoneIt),
				m_BakeSetHtmlLogThread,
				m_LightMapHtmlLogThread,
				m_ExportHtmlLogThread);

			obDoItThread.DoIt();
		}

		public void DoneIt()
		{
			// Enable UI
			if(checkBoxGenerateBakesets.Checked) groupBox1.Enabled = true;
			if(checkBoxGenerateLightMaps.Checked) groupBox2.Enabled = true;
			if(checkBoxExport.Checked) groupBox3.Enabled = true;
			groupBox4.Enabled = true;
			buttonDoIt.Enabled = true;

			if(m_bBatchMode)
			{
				Application.Exit();
			}
		}

		private void textBoxBakeSetOutputFile_TextChanged(object sender, System.EventArgs e)
		{
			textBoxExportInputFile.Text = textBoxBakeSetOutputFile.Text;
			textBoxGenerateLightMapsInputFile.Text = textBoxBakeSetOutputFile.Text;
			textBoxBakeSetLogFile.Text = rageFileUtilities.RemoveFileExtension(textBoxBakeSetOutputFile.Text)+ "_BakeSetGenerationLog.html";
			EnableDoItButtonIfValid();
		}

		private void textBoxGenerateLightMapsInputFile_TextChanged(object sender, System.EventArgs e)
		{
			textBoxGenerateLightMapsLogFile.Text = rageFileUtilities.RemoveFileExtension(textBoxGenerateLightMapsInputFile.Text)+ "_GenerateLightMapsGenerationLog.html";
			EnableDoItButtonIfValid();
		}

		private void textBoxExportInputFile_TextChanged(object sender, System.EventArgs e)
		{
			textBoxExportLogFile.Text = rageFileUtilities.RemoveFileExtension(textBoxExportInputFile.Text)+ "_ExportLog.html";
			EnableDoItButtonIfValid();
		}

		private void textBoxBakeSetLogFile_TextChanged(object sender, System.EventArgs e)
		{
			EnableDoItButtonIfValid();
		}

		private void textBoxGenerateLightMapsLogFile_TextChanged(object sender, System.EventArgs e)
		{
			EnableDoItButtonIfValid();
		}

		private void textBoxExportLogFile_TextChanged(object sender, System.EventArgs e)
		{
			EnableDoItButtonIfValid();
		}

		private void EnableDoItButtonIfValid()
		{
			// Only enable the DoIt button if every field is filled in
			if(checkBoxGenerateBakesets.Checked)
			{
				// They want to generate bakesets, so all the bake set boxes need to be filled in
				if((textBoxBakeSetInputFile.Text == "") || (textBoxBakeSetOutputFile.Text == "") || (textBoxBakeSetLogFile.Text == ""))
				{
					// Something is blank, so bail
					buttonDoIt.Enabled = false;
					return;
				}

			}
			if(checkBoxGenerateLightMaps.Checked)
			{
				// They want to Generate LightMaps, so all the bake set boxes need to be filled in
				if((textBoxGenerateLightMapsInputFile.Text == "") || (textBoxGenerateLightMapsLogFile.Text == ""))
				{
					// Something is blank, so bail
					buttonDoIt.Enabled = false;
					return;
				}

			}
			if(checkBoxExport.Checked)
			{
				// They want to generate bakesets, so all the bake set boxes need to be filled in
				if((textBoxExportInputFile.Text == "") || (textBoxExportLogFile.Text == ""))
				{
					// Something is blank, so bail
					buttonDoIt.Enabled = false;
					return;
				}

			}
			buttonDoIt.Enabled = true;
		}

		private void menuItemSaveSettings_Click(object sender, System.EventArgs e)
		{
			// Grab all the settings, and save them
			// Open the settings file
			string strXmlSettingsFile = rageSimpleUIElements.SaveFileBrowser("Settings file to save to", "", "Xml files (*.xml)|*.xml|All files (*.*)|*.*");
			XmlTextWriter obXmlWriter = new XmlTextWriter(strXmlSettingsFile, System.Text.Encoding.UTF8);
			obXmlWriter.Formatting = System.Xml.Formatting.Indented;

			// Start the file
			obXmlWriter.WriteStartDocument();
			obXmlWriter.WriteStartElement("rageLightMapperSettings");
			obXmlWriter.WriteElementString("MayaBinary", textBoxMayaBinary.Text);

			 // Bakeset Generation
			obXmlWriter.WriteElementString("GenerateBakesets", checkBoxGenerateBakesets.Checked.ToString());
			obXmlWriter.WriteElementString("GenerateBakesetsInputFile", textBoxBakeSetInputFile.Text);
			obXmlWriter.WriteElementString("GenerateBakesetsOutputFile", textBoxBakeSetOutputFile.Text);
			obXmlWriter.WriteElementString("GenerateBakesetsLogFile", textBoxBakeSetLogFile.Text);
            obXmlWriter.WriteElementString("NoOfBakesets", numericUpDownNoOfBakeSets.Value.ToString());
			obXmlWriter.WriteElementString("BakesetsWidth", numericUpDownWidth.Value.ToString());
			obXmlWriter.WriteElementString("BakesetsHeight", numericUpDownHeight.Value.ToString());
			obXmlWriter.WriteElementString("BakesetsForceSoftEdges", checkBoxSoftEdges.Checked.ToString());

			 // Lightmap Generation
            obXmlWriter.WriteElementString("GenerateLightMaps", checkBoxGenerateLightMaps.Checked.ToString());
			obXmlWriter.WriteElementString("GenerateLightMapsInputFile", textBoxGenerateLightMapsInputFile.Text);
			obXmlWriter.WriteElementString("GenerateLightMapsLogFile", textBoxGenerateLightMapsLogFile.Text);

            // Export
            obXmlWriter.WriteElementString("Export", checkBoxExport.Checked.ToString());
			obXmlWriter.WriteElementString("ExportInputFile", textBoxExportInputFile.Text);
			obXmlWriter.WriteElementString("ExportLogFile", textBoxExportLogFile.Text);

			// Close the file
			obXmlWriter.WriteEndElement();
			obXmlWriter.WriteEndDocument();
			obXmlWriter.Flush();
			obXmlWriter.Close();
		}

		private void menuItemOpenSettings_Click(object sender, System.EventArgs e)
		{
			string strXmlSettingsFile = rageSimpleUIElements.OpenFileBrowser("Settings file to open", "", "Xml files (*.xml)|*.xml|All files (*.*)|*.*");
			LoadSettings(strXmlSettingsFile);
		}

		private void LoadSettings(string strXmlSettingsFile)
		{
				// Open the settings file
			XmlTextReader obXmlReader = new XmlTextReader(strXmlSettingsFile);
			while (obXmlReader.Read())
			{
				// Is it a tag?
				if (obXmlReader.NodeType == XmlNodeType.Element)
				{
					// what tag is it?
					switch (obXmlReader.Name)
					{
						case "MayaBinary":
							textBoxMayaBinary.Text = obXmlReader.ReadElementString("MayaBinary");
							break;

						// Bakeset Generation
						case "GenerateBakesets":
							checkBoxGenerateBakesets.Checked	= Boolean.Parse(obXmlReader.ReadElementString());
							break;
						case "GenerateBakesetsInputFile":
							textBoxBakeSetInputFile.Text		= obXmlReader.ReadElementString();
							break;
						case "GenerateBakesetsOutputFile":
							textBoxBakeSetOutputFile.Text		= obXmlReader.ReadElementString();
							break;
						case "GenerateBakesetsLogFile":
							textBoxBakeSetLogFile.Text			= obXmlReader.ReadElementString();
							break;
						case "NoOfBakesets":
							numericUpDownNoOfBakeSets.Value		= Int32.Parse(obXmlReader.ReadElementString());
							break;
						case "BakesetsWidth":
							numericUpDownWidth.Value			= Int32.Parse(obXmlReader.ReadElementString());
							break;
						case "BakesetsHeight":
							numericUpDownHeight.Value			= Int32.Parse(obXmlReader.ReadElementString());
							break;
						case "BakesetsForceSoftEdges":
							checkBoxSoftEdges.Checked			= Boolean.Parse(obXmlReader.ReadElementString());
							break;

						// Lightmap Generation
						case "GenerateLightMaps":
							checkBoxGenerateLightMaps.Checked	= Boolean.Parse(obXmlReader.ReadElementString());
							break;
						case "GenerateLightMapsInputFile":
							textBoxGenerateLightMapsInputFile.Text	= obXmlReader.ReadElementString();
							break;
						case "GenerateLightMapsLogFile":
							textBoxGenerateLightMapsLogFile.Text	= obXmlReader.ReadElementString();
							break;
					
						// Export
						case "Export":
							checkBoxExport.Checked				= Boolean.Parse(obXmlReader.ReadElementString());
							break;
						case "ExportInputFile":
							textBoxExportInputFile.Text			= obXmlReader.ReadElementString();
							break;
						case "ExportLogFile":
							textBoxExportLogFile.Text			= obXmlReader.ReadElementString();
							break;
					}
				}
			}
			// Close the file
			obXmlReader.Close();
		}

		private void menuItemExit_Click(object sender, System.EventArgs e)
		{
			// Quit myself
			Application.Exit();
		}

		private void menuItemLaunchWiki_Click(object sender, System.EventArgs e)
		{
			string targetURL = "https://10.1.16.4/tiki/tiki-index.php?page=Light+Mapping";
			System.Diagnostics.Process.Start(targetURL);
		}
	}
}

