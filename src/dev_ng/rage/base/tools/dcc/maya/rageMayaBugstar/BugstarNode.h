// /BugStarNode.h

#ifndef __BUGSTARNODE_H__
#define __BUGSTARNODE_H__

#include <maya/MPxLocatorNode.h> 
#include <maya/MString.h> 
#include <maya/MTypeId.h> 
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>
#include <maya/M3dView.h>

#include <GL/glu.h>

namespace rage
{

	//-----------------------------------------------------------------------------

	class bugStarNode : public MPxLocatorNode
	{
	public:
		bugStarNode();
		virtual ~bugStarNode();

		virtual MStatus	compute( const MPlug& plug, MDataBlock& data );

		virtual void draw( M3dView & view, const MDagPath & path, M3dView::DisplayStyle style, M3dView::DisplayStatus status );

		virtual bool			isBounded() const;
		virtual MBoundingBox    boundingBox() const; 

		static  void*		creator();
		static  MStatus     initialize();

	public: 
		static MTypeId		id;

		static MObject		m_sId;
		static MObject		m_sSummary;
		static MObject		m_sClassId;
		static MObject		m_sStatus;
		static MObject		m_sXpos;
		static MObject		m_sYpos;
		static MObject		m_sZpos;
		static MObject		m_sBugNumber;
		static MObject		m_sOwner;
	private:
	};

	//-----------------------------------------------------------------------------

}//end namespace rage

#endif //__BUGSTARNODE_H__