// /Bugstar.h

#ifndef __BUGSTAR_H__
#define __BUGSTAR_H__

#define PROJECT_ID 112

//-----------------------------------------------------------------------------

static int g_ProjectID = PROJECT_ID;

//-----------------------------------------------------------------------------

class BugstarXML
{
public:
	BugstarXML():m_Initialised(false) {}
	~BugstarXML() { if(m_Initialised) shutdown(); }

	bool init();
	bool shutdown();
	bool reconnect();

	void setProject(int projectID);
	void setUser(int userID);

	int getUserCount();
	bool getUser(int index,int& r_id,char* p_Name,int sizeName);

	int getBugCount();
	bool getBug(int index,int& r_id,int& r_bugnum,char* p_Summary,int sizeSummary,char* p_Description, int sizeDescription,int& r_class,int& r_status,float& r_x,float& r_y,float& r_z);
	void getBugMsg(int index,char* p_Msg);
	
	void resolveBug(int id,const char* p_Comment);

protected:
	bool getToken(const char* p_Buffer,const char* p_Token,char* p_Out,int sizeOut,const char* p_ExtraSearch = NULL);
	void sendCommand(const char* p_cmd,...);
	bool receive();
	void startConnection();
	void endConnection();
	void printLastError();

	enum
	{
		DATA_SIZE = 64 * 1024
	};

	bool m_Initialised;
	int m_projectID;
	int m_userID;
	int m_userListID;
	int m_bugListID;
	SOCKET m_Socket;
	char m_Buf[DATA_SIZE];
};

//-----------------------------------------------------------------------------

#endif //__BUGSTAR_H__