// /BugStarNode.cpp

#include <maya/MPxLocatorNode.h> 
#include <maya/MString.h> 
#include <maya/MTypeId.h> 
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>
#include <maya/M3dView.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnStringData.h>

#include "BugStarNode.h"

namespace rage
{
	
	MTypeId bugStarNode::id( 0xbaaaa );

	MObject bugStarNode::m_sId;
	MObject bugStarNode::m_sSummary;
	MObject bugStarNode::m_sClassId;
	MObject bugStarNode::m_sStatus;
	MObject bugStarNode::m_sXpos;
	MObject bugStarNode::m_sYpos;
	MObject bugStarNode::m_sZpos;
	MObject bugStarNode::m_sBugNumber;
	MObject bugStarNode::m_sOwner;
	
	//-----------------------------------------------------------------------------

	bugStarNode::bugStarNode()
	{
		
	}

	//-----------------------------------------------------------------------------

	bugStarNode::~bugStarNode()
	{
	
	}

	//-----------------------------------------------------------------------------

	void* bugStarNode::creator()
	{
		return new bugStarNode();
	}


	//-----------------------------------------------------------------------------
	
	MStatus bugStarNode::initialize()
	{
		MStatus status, status1;
		MFnTypedAttribute typedFn;
		MFnNumericAttribute numericFn;
		MFnEnumAttribute enumFn;
		MFnStringData stringData;

		m_sId = numericFn.create( "bugId", "id", MFnNumericData::kInt, 0, &status);
		numericFn.setKeyable(false);
		status = addAttribute(m_sId);
		if (status != MS::kSuccess) 
		{
			status.perror("addAttribute");
			return status;
		}

		m_sSummary = typedFn.create( "bugSummary", "sum", MFnData::kString, stringData.create(&status1), &status);
		typedFn.setKeyable(false);
		status = addAttribute(m_sSummary);
		if (status != MS::kSuccess) 
		{
			status.perror("addAttribute");
			return status;
		}

		m_sClassId = enumFn.create( "bugClassId", "cid", 0, &status);
		enumFn.addField("A", 0);
		enumFn.addField("B", 1);
		enumFn.addField("C", 2);
		enumFn.addField("D", 3);
		enumFn.addField("WISH", 4);
		enumFn.addField("TODO", 5);
		enumFn.addField("TASK", 6);
		enumFn.setKeyable(false);
		status = addAttribute(m_sClassId);
		if (status != MS::kSuccess) 
		{
			status.perror("addAttribute");
			return status;
		}

		m_sStatus = enumFn.create( "bugStatus", "sta", 0, &status);
		enumFn.addField("Open", 0);
		enumFn.addField("Fixed", 1);
		enumFn.setKeyable(false);
		status = addAttribute(m_sStatus);
		if (status != MS::kSuccess) 
		{
			status.perror("addAttribute");
			return status;
		}

		m_sXpos = numericFn.create( "bugPosX", "px", MFnNumericData::kDouble, 0.0, &status);
		numericFn.setKeyable(false);
		status = addAttribute(m_sXpos);
		if (status != MS::kSuccess) 
		{
			status.perror("addAttribute");
			return status;
		}

		m_sYpos = numericFn.create( "bugPosY", "py", MFnNumericData::kDouble, 0.0, &status);
		numericFn.setKeyable(false);
		status = addAttribute(m_sYpos);
		if (status != MS::kSuccess) 
		{
			status.perror("addAttribute");
			return status;
		}

		m_sZpos = numericFn.create( "bugPosZ", "pz", MFnNumericData::kDouble, 0.0, &status);
		numericFn.setKeyable(false);
		status = addAttribute(m_sZpos);
		if (status != MS::kSuccess) 
		{
			status.perror("addAttribute");
			return status;
		}

		m_sBugNumber = numericFn.create( "bugNumber", "num", MFnNumericData::kInt, 0, &status);
		numericFn.setKeyable(false);
		status = addAttribute(m_sBugNumber);
		if (status != MS::kSuccess) 
		{
			status.perror("addAttribute");
			return status;
		}

		m_sOwner = typedFn.create( "bugOwner", "own", MFnData::kString, stringData.create(&status1), &status);
		typedFn.setKeyable(false);
		status = addAttribute(m_sOwner);
		if (status != MS::kSuccess) 
		{
			status.perror("addAttribute");
			return status;
		}

		return MS::kSuccess;
	}

	//-----------------------------------------------------------------------------

	MStatus bugStarNode::compute( const MPlug& /*plug*/, MDataBlock& /*data*/ )
	{
		return MS::kUnknownParameter;
	}

	//-----------------------------------------------------------------------------

	void bugStarNode::draw( M3dView & view, const MDagPath & /*path*/, M3dView::DisplayStyle style, M3dView::DisplayStatus status )
	{
		static MColor bugColors[7] = {MColor(1.0f, 0.0f, 0.0f),		//A
									  MColor(0.0f, 0.85f, 0.0f),	//B
									  MColor(0.0f, 0.0f, 1.0f),		//C
									  MColor(1.0f, 1.0f, 0.0f),		//D
									  MColor(1.0f, 0.0, 1.0f),		//WISH
									  MColor(1.0f, 0.0, 1.0f),		//TODO
									  MColor(1.0f, 0.0, 1.0f)		//TASK
									};
		MStatus status1;

		//Figure out what color we should use based on the class of the bug
		int iClassId;
		MObject thisNode = thisMObject();
		MPlug bugClassPlug = MPlug( thisNode, m_sClassId );
		status1 = bugClassPlug.getValue( iClassId );
		if(status1 != MS::kSuccess)
			iClassId = 6;
		
		//Clamp the class id in the event the bugstar database has more class fields than we have setup colors for
		if(iClassId >= 7)
			iClassId = 6;

		view.beginGL();

			GLUquadricObj* qobj = gluNewQuadric();

			if(status == M3dView::kDormant)
				glColor3f(bugColors[iClassId].r, bugColors[iClassId].g, bugColors[iClassId].b);

			gluQuadricDrawStyle( qobj, GLU_LINE );
			gluSphere( qobj, 2.0, 8, 4 );
			
			gluDeleteQuadric(qobj);

		view.endGL();
	}

	//-----------------------------------------------------------------------------

	bool bugStarNode::isBounded() const
	{
		return true;
	}

	//-----------------------------------------------------------------------------

	MBoundingBox bugStarNode::boundingBox() const
	{
		return MBoundingBox( MPoint(-2.0f, -2.0f, -2.0f), MPoint(2.0f, 2.0f, 2.0f) );
	}

	//-----------------------------------------------------------------------------

}//End namespace rage