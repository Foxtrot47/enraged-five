// 
// /pluginMain.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#pragma warning(pop)

#include "BugstarCmds.h"
#include "BugstarNode.h"

using namespace rage;

//-----------------------------------------------------------------------------

MStatus initializePlugin( MObject obj )
{
	MFnPlugin plugin( obj, "Alias|Wavefront", "6.5", "Any");
	
	plugin.registerCommand("bugStarSetProject", bugStarSetProjectCmd::creator, bugStarSetProjectCmd::newSyntax);
	plugin.registerCommand("bugStarGetUsers", bugStarGetUsersCmd::creator, bugStarGetUsersCmd::newSyntax);
	plugin.registerCommand("bugStarGetBugCount", bugStarGetBugCountCmd::creator, bugStarGetBugCountCmd::newSyntax);
	plugin.registerCommand("bugStarGetBugInfo", bugStarGetBugInfoCmd::creator, bugStarGetBugInfoCmd::newSyntax);
	plugin.registerCommand("bugStarResolve", bugStarResolveCmd::creator, bugStarResolveCmd::newSyntax);

	plugin.registerNode("bugstar", bugStarNode::id, &bugStarNode::creator, &bugStarNode::initialize, MPxNode::kLocatorNode);
	
	return( MS::kSuccess );
}

//-----------------------------------------------------------------------------

MStatus uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );
	
	plugin.deregisterCommand("bugStarSetProject");
	plugin.deregisterCommand("bugStarGetUsers");
	plugin.deregisterCommand("bugStarGetBugCount");

	plugin.deregisterNode( bugStarNode::id );

	return( MS::kSuccess );
}

//-----------------------------------------------------------------------------

