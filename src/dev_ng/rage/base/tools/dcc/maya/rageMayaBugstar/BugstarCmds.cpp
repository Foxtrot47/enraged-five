// /BugstarCmds.cpp

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include "maya/MGlobal.h"
#include "maya/MObject.h"
#include "maya/MArgDatabase.h"
#include "maya/MStringArray.h"

#pragma warning(pop)

#include <stdlib.h>

#include <string>
#include <vector>
#include <algorithm>

#include "BugstarCmds.h"

using namespace std;

extern int g_ProjectID;

namespace rage
{

//-----------------------------------------------------------------------------

MSyntax	bugStarSetProjectCmd::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag("-prj", "-projectId", MSyntax::kLong);

	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus	bugStarSetProjectCmd::parseArguments( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	MArgDatabase argDb(syntax(), args);

	if(argDb.isFlagSet("-projectId"))
	{
		status = argDb.getFlagArgument("-projectId", 0, m_projectId);
		if( status != MS::kSuccess )
			return MS::kInvalidParameter;
	}

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus	bugStarSetProjectCmd::doIt( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	//Parse the command arguments
	status = parseArguments( args );
	if(status != MS::kSuccess)
	{
		status.perror("parseArguments failed.");
		return status;
	}

	g_ProjectID = m_projectId;

	return status;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

MSyntax	bugStarGetUsersCmd::newSyntax()
{
	MSyntax syntax;

	//No arguments for now...

	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus	bugStarGetUsersCmd::doIt( const MArgList& args )
{
	MStatus status(MS::kSuccess);

	BugstarXML bugstar;

	if(!bugstar.init())
	{
		status = MS::kFailure;
		status.perror("Failed to initialize BugStar");
		return status;
	}

	bugstar.setProject(g_ProjectID);

	int nUsers = bugstar.getUserCount();

	//Build up an array of the users where each entry takes the form (username):(userid)
	vector< string > bugStarUserNamesAndIds;
	for(int userIdx = 0; userIdx < nUsers; userIdx++)
	{
		int userId;
		char userName[256];

		bugstar.getUser(userIdx, userId, userName, 256);
		
		string userNameAndId(userName);
		userNameAndId.append(":");

		char tmpBuffer[64];
		_itoa_s(userId, tmpBuffer, 64, 10);

		userNameAndId.append(tmpBuffer);

		//For some reason duplicates sometimes show up in the user list.. so check before adding any new entries
		vector< string >::iterator findIt = find( bugStarUserNamesAndIds.begin(), bugStarUserNamesAndIds.end(), userNameAndId );
		if(findIt == bugStarUserNamesAndIds.end())
			bugStarUserNamesAndIds.push_back(userNameAndId);
	}

	//Sort the user list alphabetically
	sort( bugStarUserNamesAndIds.begin(), bugStarUserNamesAndIds.end() );

	//Copy the sorted list over into a maya string array for the MEL return value
	MStringArray userArray;
	for(vector< string >::iterator it = bugStarUserNamesAndIds.begin();
		it != bugStarUserNamesAndIds.end();
		++it)
	{
		userArray.append( MString( it->c_str() ) );
	}
	
	setResult(userArray);

	bugstar.shutdown();

	return status;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

MSyntax	bugStarGetBugCountCmd::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag("-uid", "-userId", MSyntax::kLong);

	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus	bugStarGetBugCountCmd::parseArguments( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	MArgDatabase argDb(syntax(), args);

	if(argDb.isFlagSet("-userId"))
	{
		status = argDb.getFlagArgument("-userId", 0, m_userId);
		if( status != MS::kSuccess )
			return MS::kInvalidParameter;
	}

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus	bugStarGetBugCountCmd::doIt( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	//Parse the command arguments
	status = parseArguments( args );
	if(status != MS::kSuccess)
	{
		status.perror("parseArguments failed.");
		return status;
	}

	BugstarXML bugstar;

	if(!bugstar.init())
	{
		status = MS::kFailure;
		status.perror("Failed to initialize BugStar");
		return status;
	}

	bugstar.setProject(g_ProjectID);
	bugstar.setUser(m_userId);

	MIntArray bugIdArray;

	int nBugs = bugstar.getBugCount();
	setResult(nBugs);

	bugstar.shutdown();

	return status;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


MSyntax	bugStarGetBugInfoCmd::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag("-uid", "-userId", MSyntax::kLong);
	syntax.addFlag("-idx", "-index", MSyntax::kLong);

	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus	bugStarGetBugInfoCmd::parseArguments( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	MArgDatabase argDb(syntax(), args);

	if(argDb.isFlagSet("-userId"))
	{
		status = argDb.getFlagArgument("-userId", 0, m_userId);
		if( status != MS::kSuccess )
			return MS::kInvalidParameter;
	}

	if(argDb.isFlagSet("-index"))
	{
		status = argDb.getFlagArgument("-index", 0, m_bugIndex);
		if( status != MS::kSuccess )
			return MS::kInvalidParameter;
	}

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus	bugStarGetBugInfoCmd::doIt( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	//Parse the command arguments
	status = parseArguments( args );
	if(status != MS::kSuccess)
	{
		status.perror("parseArguments failed.");
		return status;
	}

	BugstarXML bugstar;

	if(!bugstar.init())
	{
		status = MS::kFailure;
		status.perror("Failed to initialize BugStar");
		return status;
	}

	bugstar.setProject(g_ProjectID);
	bugstar.setUser(m_userId);

	MStringArray bugInfoArray;

	int id,classid,bugstatus,bugnum;
	char summary[256];
	char description[2048];
	float x,y,z;

	if(bugstar.getBug(m_bugIndex, id, bugnum, summary, 256, description, 2048, classid, bugstatus, x, y, z))
	{
		// 0 : Id
		MString mstrId;
		mstrId += id;
		bugInfoArray.append(mstrId);

		// 1 : Summary
		bugInfoArray.append(MString(summary));

		// 2 : Class Id
		MString mstrClassId;
		mstrClassId += classid;
		bugInfoArray.append(mstrClassId);

		// 3 : Status
		MString mstrStatus;
		mstrStatus += bugstatus;
		bugInfoArray.append(mstrStatus);

		// 4 : X Pos
		MString mstrXPos;
		mstrXPos += x;
		bugInfoArray.append(mstrXPos);

		// 5 : Y Pos
		MString mstrYPos;
		mstrYPos += y;
		bugInfoArray.append(mstrYPos);

		// 6 : Z Pos
		MString mstrZPos;
		mstrZPos += z;
		bugInfoArray.append(mstrZPos);

		// 7 : Bug number
		MString mstrBugNum;
		mstrBugNum += bugnum;
		bugInfoArray.append(mstrBugNum);

		// 8 : Bug description
		bugInfoArray.append(MString(description));
	}
	else
	{
		char output[8192];
		bugstar.getBugMsg(m_bugIndex,output);

		printf(output);
		printf("\n");	
	}
	
	setResult(bugInfoArray);

	bugstar.shutdown();

	return status;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


MSyntax	bugStarResolveCmd::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag("-uid", "-userId", MSyntax::kLong);
	syntax.addFlag("-bid", "-bugId", MSyntax::kLong);
	syntax.addFlag("-com", "-comment", MSyntax::kString);

	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus	bugStarResolveCmd::parseArguments( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	MArgDatabase argDb(syntax(), args);

	if(argDb.isFlagSet("-userId"))
	{
		status = argDb.getFlagArgument("-userId", 0, m_userId);
		if( status != MS::kSuccess )
			return MS::kInvalidParameter;
	}

	if(argDb.isFlagSet("-bugId"))
	{
		status = argDb.getFlagArgument("-bugId", 0, m_bugId);
		if( status != MS::kSuccess )
			return MS::kInvalidParameter;
	}

	if(argDb.isFlagSet("-comment"))
	{
		status = argDb.getFlagArgument("-comment", 0, m_comment);
		if( status != MS::kSuccess )
			return MS::kInvalidParameter;
	}

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus	bugStarResolveCmd::doIt( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	//Parse the command arguments
	status = parseArguments( args );
	if(status != MS::kSuccess)
	{
		status.perror("parseArguments failed.");
		return status;
	}

	BugstarXML bugstar;

	if(!bugstar.init())
	{
		status = MS::kFailure;
		status.perror("Failed to initialize BugStar");
		return status;
	}

	bugstar.setProject(g_ProjectID);
	bugstar.setUser(m_userId);

	bugstar.resolveBug(m_bugId, m_comment.asChar());

	bugstar.shutdown();

	return status;
}

//-----------------------------------------------------------------------------

} //End namespace rage