// /BugStarCmds.h

#ifndef __BUGSTAR_CMDS_H__
#define __BUGSTAR_CMDS_H__

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MPxCommand.h>

#pragma warning(pop)

#include "Bugstar.h"

//def_visible_primitive( bugstar_setproject , "bugstar_setproject" ); (int - project id)
//def_visible_primitive( bugstar_getusers , "bugstar_getusers" ); (void)
//def_visible_primitive( bugstar_getbugs , "bugstar_getbugs" ); (int - user id)
//def_visible_primitive( bugstar_resolve , "bugstar_resolve" ); (int - user id, int - bug id, string - comment )

namespace rage
{

	//-----------------------------------------------------------------------------

	class bugStarSetProjectCmd : public MPxCommand
	{
	public:
		bugStarSetProjectCmd() 
			: m_projectId(PROJECT_ID)
		{};

		virtual ~bugStarSetProjectCmd() {};

		static	void*	creator() { return new bugStarSetProjectCmd(); }
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual MStatus redoIt() { return MS::kSuccess; }
		virtual MStatus undoIt() { return MS::kSuccess; }

		virtual bool	isUndoable() const { return false; }

		MStatus	parseArguments( const MArgList& args );

	private:
		int		m_projectId;
	};

	//-----------------------------------------------------------------------------

	class bugStarGetUsersCmd : public MPxCommand
	{
	public:
		bugStarGetUsersCmd() {};
		virtual ~bugStarGetUsersCmd() {};

		static	void*	creator() { return new bugStarGetUsersCmd(); }
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual MStatus redoIt() { return MS::kSuccess; }
		virtual MStatus undoIt() { return MS::kSuccess; }

		virtual bool	isUndoable() const { return false; }
	};

	//-----------------------------------------------------------------------------

	class bugStarGetBugCountCmd : public MPxCommand
	{
	public:
		bugStarGetBugCountCmd() {};
		virtual ~bugStarGetBugCountCmd() {};

		static	void*	creator() { return new bugStarGetBugCountCmd(); }
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual MStatus redoIt() { return MS::kSuccess; }
		virtual MStatus undoIt() { return MS::kSuccess; }

		virtual bool	isUndoable() const { return true; }

		MStatus	parseArguments( const MArgList& args );
	private:
		int	m_userId;
	};

	//-----------------------------------------------------------------------------

	class bugStarGetBugInfoCmd : public MPxCommand
	{
	public:
		bugStarGetBugInfoCmd() {};
		virtual ~bugStarGetBugInfoCmd() {};

		static	void*	creator() { return new bugStarGetBugInfoCmd(); }
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual MStatus redoIt() { return MS::kSuccess; }
		virtual MStatus undoIt() { return MS::kSuccess; }

		virtual bool	isUndoable() const { return true; }

		MStatus	parseArguments( const MArgList& args );
	private:
		int	m_userId;
		int m_bugIndex;
	};

	//-----------------------------------------------------------------------------

	class bugStarResolveCmd : public MPxCommand
	{
	public:
		bugStarResolveCmd() {};
		virtual ~bugStarResolveCmd() {};

		static	void*	creator() { return new bugStarResolveCmd(); }
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual MStatus redoIt() { return MS::kSuccess; }
		virtual MStatus undoIt() { return MS::kSuccess; }

		virtual bool	isUndoable() const { return true; }

		MStatus	parseArguments( const MArgList& args );
	private:
		int	m_userId;
		int	m_bugId;
		MString m_comment;
	};

//-----------------------------------------------------------------------------

} //end namespace rage


#endif