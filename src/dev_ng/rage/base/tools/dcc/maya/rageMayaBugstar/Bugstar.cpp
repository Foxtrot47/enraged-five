// /Bugstar.cpp

#include "Windows.h"
#include "stdio.h"

#include "Bugstar.h"

//-----------------------------------------------------------------------------

bool BugstarXML::init()
{
	m_projectID = 0;
	m_userID = 0;

	WSADATA wsaData;

	int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != NO_ERROR)
	{
		printf("Error at WSAStartup()\n");
		return false;
	}

	m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_Socket == INVALID_SOCKET) 
	{
		printf("Error at socket(): %ld\n", WSAGetLastError());
		WSACleanup();
		return false;
	}
	
	//u_long nonBlock = 1;
	//ioctlsocket(m_Socket,FIONBIO,&nonBlock);

	sockaddr_in clientService; 
	clientService.sin_family = AF_INET;
	//clientService.sin_addr.s_addr = inet_addr( "10.0.19.222" );
	clientService.sin_addr.s_addr = inet_addr( "127.0.0.1" );
	clientService.sin_port = htons( 4999 );

	if ( connect( m_Socket, (SOCKADDR*) &clientService, sizeof(clientService) ) == SOCKET_ERROR) 
	{
		int errCode = WSAGetLastError();

		printf( "Failed to connect with error code : %d\n", errCode );
		
		WSACleanup();
		return false;
	}

	startConnection();

	m_Initialised = true;

	return true;
}

//-----------------------------------------------------------------------------

bool BugstarXML::shutdown()
{
	endConnection();
	closesocket(m_Socket);
	WSACleanup();

	m_Initialised = false;

	return true;
}

//-----------------------------------------------------------------------------

bool BugstarXML::reconnect()
{
	if(m_Initialised)
	{
		shutdown();
		if(!init())
			return false;
		if(m_projectID != 0)
			setProject(m_projectID);
		if(m_userID != 0)
			setUser(m_userID);		
	}

	return true;
}

//-----------------------------------------------------------------------------

void BugstarXML::setProject(int projectID)
{
	m_projectID = projectID;

	sendCommand("<command cmd='GetUserList'><param name='project_id'>%d</param></command>",m_projectID);

	if(!receive())
		return ;

	m_userListID = 0;
	sscanf(m_Buf,"<result>%d</result>",&m_userListID);
}

//-----------------------------------------------------------------------------

void BugstarXML::setUser(int userID)
{
	m_userID = userID;

	sendCommand("<command cmd='GetMyBugList'><param name='user_id'>%d</param><param name='project_id'>%d</param></command>",userID,m_projectID);

	if(!receive())
		return ;

	m_bugListID = 0;
	sscanf(m_Buf,"<result>%d</result>",&m_bugListID);
}

//-----------------------------------------------------------------------------

int BugstarXML::getUserCount()
{
	sendCommand("<command cmd='GetUserCount'><param name='list_id'>%d</param></command>",m_userListID);

	if(!receive())
		return 0;

	int UserCount = 0;
	sscanf(m_Buf,"<result>%d</result>",&UserCount);

	return UserCount;
}

//-----------------------------------------------------------------------------

bool BugstarXML::getUser(int index,int& r_id,char* p_Name,int sizeName)
{
	sendCommand("<command cmd='GetUser'><param name='list_id'>%d</param><param name='index'>%d</param></command>",m_userListID,index);

	if(!receive())
		return 0;

	char idstr[128];

	getToken(m_Buf,"id",idstr,128);
	getToken(m_Buf,"text",p_Name,sizeName);

	r_id = atoi(idstr);

	return true;
}

//-----------------------------------------------------------------------------

int BugstarXML::getBugCount()
{
	sendCommand("<command cmd='GetBugListCount'><param name='list_id'>%d</param></command>",m_bugListID);

	if(!receive())
		return 0;

	int BugCount = 0;
	sscanf(m_Buf,"<result>%d</result>",&BugCount);

	return BugCount;
}

//-----------------------------------------------------------------------------

void BugstarXML::getBugMsg(int index,char* p_Msg)
{
	sprintf(p_Msg,"<command cmd='GetBug'><param name='list_id'>%d</param><param name='index'>%d</param></command>",m_bugListID,index);
}

//-----------------------------------------------------------------------------

bool BugstarXML::getBug(int index,int& r_id,int& r_bugnum,
						char* p_Summary,int sizeSummary,
						char* p_Description, int sizeDescription,
						int& r_class,int& r_status,
						float& r_x,float& r_y,float& r_z)
{
	sendCommand("<command cmd='GetBug'><param name='list_id'>%d</param><param name='index'>%d</param></command>",m_bugListID,index);

	if(!receive())
		return 0;

	char buffer[128];

	sscanf(m_Buf,"<result><asset id='%d'>",&r_id);

	getToken(m_Buf,"property",buffer,128,"pt_id='60'");
	r_bugnum = atoi(buffer);
	getToken(m_Buf,"property",p_Summary,sizeSummary,"pt_id='27'");
	getToken(m_Buf,"property",buffer,128,"pt_id='26'");
	r_class = atoi(buffer);
	getToken(m_Buf,"property",buffer,128,"pt_id='31'");
	r_status = atoi(buffer);
	getToken(m_Buf,"property",buffer,128,"pt_id='35'");
	r_x = (float)atoi(buffer);
	getToken(m_Buf,"property",buffer,128,"pt_id='36'");
	r_y = (float)atoi(buffer);
	getToken(m_Buf,"property",buffer,128,"pt_id='37'");
	r_z = (float)atoi(buffer);
	getToken(m_Buf,"property",p_Description,sizeDescription,"pt_id='32'");

	return true;
}

//-----------------------------------------------------------------------------

void BugstarXML::resolveBug(int id,const char* p_Comment)
{
	sendCommand("<command cmd='ResolveBug'><param name='user_id'>%d</param><param name='asset_id'>%d</param><param name='comments'>%s</param></command>",m_userID,id,p_Comment);
	receive();	
}

//-----------------------------------------------------------------------------

void BugstarXML::startConnection()
{
	sendCommand("<?xml version='1.0'?><connection>");
}

//-----------------------------------------------------------------------------

void BugstarXML::endConnection()
{
	sendCommand("</connection>");
}

//-----------------------------------------------------------------------------

bool BugstarXML::getToken(const char* p_Buffer,const char* p_Token,char* p_Out,int sizeOut,const char* p_ExtraSearch)
{
	char beginTok[64];
	char endTok[64];

	sprintf(endTok,"</%s>",p_Token);

	const char* p_Find = NULL;
	const char* p_FindEnd = NULL;

	if(p_ExtraSearch)
	{
		p_Find = strstr(p_Buffer,p_ExtraSearch);
		p_Find = strchr(p_Find,'>');

		if(p_Find)
			p_Find++;

		p_FindEnd = strstr(p_Find,endTok);
	}
	else
	{
		sprintf(beginTok,"<%s>",p_Token);
		p_Find = strstr(p_Buffer,beginTok) + strlen(beginTok);
		p_FindEnd = strstr(p_Buffer,endTok);
	}

	if(!p_Find || !p_FindEnd)
	{
		*p_Out = '\0';
		return false;
	}
	
	int Length = p_FindEnd - p_Find;

	if(Length >= sizeOut)
	{
		Length = sizeOut - 1;
	}

	strncpy(p_Out,p_Find,Length);
	p_Out[Length] = '\0';

	return true;
}

//-----------------------------------------------------------------------------

void BugstarXML::sendCommand(const char* p_cmd,...)
{
	va_list args;
	int len;
	char * buffer;

	va_start( args, p_cmd );
	len = _vscprintf( p_cmd, args ) + 1;
	buffer = new char[len];
	vsprintf( buffer, p_cmd, args );

	int numSent = send(m_Socket,buffer,strlen(buffer),0);

	delete buffer;
}

//-----------------------------------------------------------------------------

bool BugstarXML::receive()
{	
	memset(m_Buf,0,DATA_SIZE);

	char* p_Input = m_Buf;
	int maxTrys = 100;
	int trys = maxTrys;

	do
	{
		int BufferLeft = DATA_SIZE - (p_Input - m_Buf);

		if(BufferLeft <= 0)
			break;

		int bytesRecv = recv( m_Socket, p_Input, BufferLeft, 0 );

		if(bytesRecv == SOCKET_ERROR)
		{
			bytesRecv = 0;
		}

		if(bytesRecv > 0)
			trys = maxTrys;
		else
			trys--;

		if(trys == 0)
			break;

		p_Input += bytesRecv;

		if(strstr(m_Buf,"</result>"))
			return true;

		if ( bytesRecv == 0 || bytesRecv == WSAECONNRESET ) 
		{
			printf( "Connection Closed.\n");
			break;
		}
	}
	while(true);

	reconnect();

	return false;
}

//-----------------------------------------------------------------------------

void BugstarXML::printLastError()
{
	int error = WSAGetLastError();

	switch(error)
	{
	case WSANOTINITIALISED:
		printf("WSANOTINITIALISED");
		break;
	case WSAENETDOWN:
		printf("WSAENETDOWN");
		break;
	case WSAEFAULT:
		printf("WSAEFAULT");
		break;
	case WSAENOTCONN:
		printf("WSAENOTCONN");
		break;
	case WSAEINTR:
		printf("WSAEINTR");
		break;
	case WSAEINPROGRESS:
		printf("WSAEINPROGRESS");
		break;
	case WSAENETRESET:
		printf("WSAENETRESET");
		break;
	case WSAENOTSOCK:
		printf("WSAENOTSOCK");
		break;
	case WSAEOPNOTSUPP:
		printf("WSAEOPNOTSUPP");
		break;
	case WSAESHUTDOWN:
		printf("WSAESHUTDOWN");
		break;
	case WSAEWOULDBLOCK:
		printf("WSAEWOULDBLOCK");
		break;
	case WSAEMSGSIZE:
		printf("WSAEMSGSIZE");
		break;
	case WSAEINVAL:
		printf("WSAEINVAL");
		break;
	case WSAECONNABORTED:
		printf("WSAECONNABORTED");
		break;
	case WSAETIMEDOUT:
		printf("WSAETIMEDOUT");
		break;
	case WSAECONNRESET:
		printf("WSAECONNRESET");
		break;
	}
}

//-----------------------------------------------------------------------------