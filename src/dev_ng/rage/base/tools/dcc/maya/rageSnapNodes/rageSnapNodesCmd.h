#ifndef _rageSnapNodesCmd
#define _rageSnapNodesCmd
//
// Copyright (C) Rockstar San Diego
// 
// File: rageSnapNodesCmd.h
//
// MEL Command: rageSnapNodes
//
// Author: Steven Waller
//

#include "system/xtl.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>

#include <maya/MDagPath.h>
#include <maya/MSelectionList.h>
#include <maya/MVectorArray.h>
#pragma warning(pop)

//class MArgList;

class rageSnapNodes : public MPxCommand
{

public:
				rageSnapNodes();
	virtual		~rageSnapNodes();

	MStatus		doIt( const MArgList& );
	MStatus		redoIt();
	MStatus		undoIt();
	bool		isUndoable() const;
	static		MSyntax addFlags();

	static		void* creator();

private:
	MDagPath		dpDestMesh;
	MSelectionList	editList;
	MVectorArray	vaOrigPos, vaNewPos;
};

#endif
