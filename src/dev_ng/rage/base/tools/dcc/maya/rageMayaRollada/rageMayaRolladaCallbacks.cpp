#include "rageMayaRolladaCallbacks.h"
#include "rageExportRolladaFileCmd.h"
#include "rageImportRolladaFileCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFileIO.h>
#include <maya/MGlobal.h>
#pragma warning(pop)

MCallbackId rageMayaRolladaCallbacks::obBeforeExportCallbackId;
MCallbackId rageMayaRolladaCallbacks::obAfterImportCallbackId;
MCallbackId rageMayaRolladaCallbacks::obAfterOpenCallbackId;

void rageMayaRolladaCallbacks::InitializeCallbacks()
{
	MStatus obStatus;
	obBeforeExportCallbackId = MSceneMessage::addCallback(MSceneMessage::kBeforeExport , &rageMayaRolladaCallbacks::BeforeExportCallback, NULL, &obStatus);
	if(obStatus != MS::kSuccess)
	{
		// Callback addition failed
		MGlobal::displayError("An error occurred registering callback : "+ obStatus.errorString());
	}

	obAfterImportCallbackId = MSceneMessage::addCallback(MSceneMessage::kAfterImport , &rageMayaRolladaCallbacks::AfterImportCallback, NULL, &obStatus);
	if(obStatus != MS::kSuccess)
	{
		// Callback addition failed
		MGlobal::displayError("An error occurred registering callback : "+ obStatus.errorString());
	}

	obAfterOpenCallbackId = MSceneMessage::addCallback(MSceneMessage::kAfterOpen , &rageMayaRolladaCallbacks::AfterOpenCallback, NULL, &obStatus);
	if(obStatus != MS::kSuccess)
	{
		// Callback addition failed
		MGlobal::displayError("An error occurred registering callback : "+ obStatus.errorString());
	}
}

void rageMayaRolladaCallbacks::UninitializeCallbacks()
{
	MMessage::removeCallback(obBeforeExportCallbackId);
	MMessage::removeCallback(obAfterImportCallbackId);
	MMessage::removeCallback(obAfterOpenCallbackId);
}

void rageMayaRolladaCallbacks::BeforeExportCallback(void*)
{
	// Get export filename
	MString strFilename = MFileIO::beforeExportFilename();

	// Get file extension
	MStringArray obAStrFilenameParts;
	strFilename.split('.', obAStrFilenameParts);
	MString strFileExtension = obAStrFilenameParts[obAStrFilenameParts.length() - 1].toLowerCase();

	// Is it a collada file?
	if(strFileExtension == "dae")
	{
		// Bingo!  I'm exporting a collada file, so write out rollada file to go with it
		// Export a rollada file
		rageExportRolladaFile::exportRolladaFile(strFilename);
	}
}

void rageMayaRolladaCallbacks::AfterImportCallback(void*)
{
	// Get import filename
	MString strFilename = MFileIO::beforeImportFilename();
	// MGlobal::displayInfo("MFileIO::beforeImportFilename() = "+ strFilename);

	// Get file extension
	MStringArray obAStrFilenameParts;
	strFilename.split('.', obAStrFilenameParts);
	MString strFileExtension = obAStrFilenameParts[obAStrFilenameParts.length() - 1].toLowerCase();

	// Is it a collada file?
	if(strFileExtension == "dae")
	{
		// Bingo!  I'm importing a collada file, so write out rollada file to go with it
		// import a rollada file
		MString strRolladaFilename = strFilename.substring(0, strFilename.rindex('.')) + "rollada";
		rageImportRolladaFile::importRolladaFile(strRolladaFilename);
	}
}

void rageMayaRolladaCallbacks::AfterOpenCallback(void*)
{
	// Get Open filename
	MString strFilename = MFileIO::beforeOpenFilename();
	// MGlobal::displayInfo("MFileIO::beforeOpenFilename() = "+ strFilename);

	// Get file extension
	MStringArray obAStrFilenameParts;
	strFilename.split('.', obAStrFilenameParts);
	MString strFileExtension = obAStrFilenameParts[obAStrFilenameParts.length() - 1].toLowerCase();

	// Is it a collada file?
	if(strFileExtension == "dae")
	{
		// Bingo!  I'm Opening a collada file, so write out rollada file to go with it
		// Open a rollada file
		MString strRolladaFilename = strFilename.substring(0, strFilename.rindex('.')) + "rollada";
		rageImportRolladaFile::importRolladaFile(strRolladaFilename);
	}
}
