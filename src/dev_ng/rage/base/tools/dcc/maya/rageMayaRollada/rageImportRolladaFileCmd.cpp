//
// Copyright (C) 
// 
// File: rageImportRolladaFileCmd.cpp
//
// MEL Command: rageImportRolladaFile
//
// Author: Maya Plug-in Wizard 2.0
//
#include "parser/manager.h"
#include "rageImportRolladaFileCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPlug.h>
#include <maya/MArgList.h>
#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>
#include <maya/MFnDependencyNode.h>
#pragma warning(pop)

#include <string>
using namespace std;

using namespace rage;

MStatus rageImportRolladaFile::fixupShaderNodeInMaya(const MString& strNodeName, const MString& strMtlPathAndFilename)
{
	// Find the Maya node with the name strNodeName
	MSelectionList obGetNodeFromName;
	MStatus obStatus = obGetNodeFromName.add(strNodeName);
	if(obStatus != MS::kSuccess)
	{
		// Oh dear
		MGlobal::displayInfo("Unable to find shader node "+ strNodeName);
		return obStatus;
	}

	// Node exists, so it is the right type?
	MObject obShaderNode;
	obStatus = obGetNodeFromName.getDependNode(0, obShaderNode);
	if(obStatus != MS::kSuccess)
	{
		// Oh dear
		MGlobal::displayInfo("Unable to get shader node "+ strNodeName);
		return obStatus;
	}

	if(MFnDependencyNode(obShaderNode).typeName() != "rageShaderMaya")
	{
		// It is not the correct type, so change the type of the node to a rageShaderNode
		// In MEL, this is what I want to do:
		//
		//	string $replaceNode = `createNode "rageShaderMaya"`;
		//	disconnectMaterialInfo($shaderNode, $replaceNode);
		//	replaceNode $shaderNode $replaceNode;
		//	delete $shaderNode;
		MString strReplacementNodeName;
		MGlobal::executeCommand("createNode \"rageShaderMaya\"", strReplacementNodeName, false);
		MGlobal::executeCommand("rage_disconnectMaterialInfo(\""+ strNodeName +"\", \""+ strReplacementNodeName +"\")", false);
		MGlobal::executeCommand("replaceNode(\""+ strNodeName +"\", \""+ strReplacementNodeName +"\")", false);
		MGlobal::executeCommand("delete \""+ strNodeName +"\";", false);

		// Get the replacement node as a MObject
		obGetNodeFromName.clear();
		obGetNodeFromName.add(strReplacementNodeName);
		obGetNodeFromName.getDependNode(0, obShaderNode);
	}

	// Set the mtl filename on the shader
	MFnDependencyNode obFnShaderNode(obShaderNode);
	MPlug obMtlFilePlug = obFnShaderNode.findPlug("MtlFilename");
	MString strValue = strMtlPathAndFilename;
	obMtlFilePlug.setValue(strValue);

	return MS::kSuccess;
}

MStatus rageImportRolladaFile::importRolladaFile(const MString& strFilename)
{
	// First off, does the file even exist?
	WIN32_FIND_DATA DirData;
	// Displayf("strSearchString = %s", strSearchString.c_str());
	HANDLE DirFile = FindFirstFile(strFilename.asChar(), &DirData);
	if(DirFile == INVALID_HANDLE_VALUE)
	{
		// File does not exist, so bail
		return MS::kFailure;
	}

	MGlobal::displayInfo("Importing Rollada Scene "+ strFilename);

	// Open file
	INIT_PARSER;
	parTree* pobSceneXMLTree = PARSER.LoadTree(strFilename.asChar(), "");
	parTreeNode* pobSceneRootNode = pobSceneXMLTree->GetRoot();

	// **********************************************************
	// Do the Rollada stuff
	// **********************************************************
	// Shaders
	parTreeNode* pobShadersDataShader = pobSceneRootNode->FindChildWithName("shaders");
	// There is nothing to say you HAVE to have shaders node
	if(pobShadersDataShader)
	{
		// Go through all the shaders under the shaders node
		parTreeNode* pobRolladaShaderNode = pobShadersDataShader->FindChildWithName("shader");
		while(pobRolladaShaderNode)
		{
			// Get info from shader,
			// I do not want to reproduce the mtl loader here, so I could just make a call to the existing one
			// BUT I really don't want the complexity of all that overhead, when 99% of the time I'll never 
			// rage-render or artist-edit the thing.
			// So instead for the import all I need is the "name" (which should correspond to a node name 
			// in Maya) and the mtl file to use

			// Get name
			const parAttribute* pobNameAttribute = (const_cast<parTreeNode*>(pobRolladaShaderNode))->GetElement().FindAttribute("name");
			if(!pobNameAttribute) 
			{	
				MGlobal::displayError("Unable to find name attribute for RolladaNode");
				break;
			}
			string strName = (const_cast<parAttribute*>(pobNameAttribute))->GetStringValue();

			// Get mtl path and file name
			const parTreeNode* pobMtlDataNode = pobRolladaShaderNode->FindChildWithName("mtl");
			if(!pobMtlDataNode)
			{
				MGlobal::displayError(MString("Unable to find mtl node for Rollada material ")+ strName.c_str());
				break;
			}
			const parAttribute* pobMtlPathAndFileNameAttribute = (const_cast<parTreeNode*>(pobMtlDataNode))->GetElement().FindAttribute("filename");
			if(!pobMtlPathAndFileNameAttribute)
			{
				MGlobal::displayError(MString("Unable to find filename attribute for Rollada material ")+ strName.c_str());
				break;
			}
			string strMtlPathAndFilename = (const_cast<parAttribute*>(pobMtlPathAndFileNameAttribute))->GetStringValue();

			// Wire up a shader in Maya
			fixupShaderNodeInMaya(strName.c_str(), strMtlPathAndFilename.c_str());

			// Move onto the next one
			pobRolladaShaderNode = pobShadersDataShader->FindChildWithName("shader", pobRolladaShaderNode);
		}
	}

	// Clean up
	pobSceneRootNode = NULL;
	delete pobSceneXMLTree;
	return MS::kSuccess;
}

MStatus rageImportRolladaFile::doIt( const MArgList& args)
//
//	Description:
//		implements the MEL rageImportRolladaFile command.
//
//	Arguments:
//		args - the argument list that was passes to the command from MEL
//
//	Return Value:
//		MS::kSuccess - command succeeded
//		MS::kFailure - command failed (returning this value will cause the 
//                     MEL script that is being run to terminate unless the
//                     error is caught using a "catch" statement.
//
{
	MStatus stat = MS::kSuccess;

	// Command takes one arg, the filename to import to
	if(args.length() != 1)
	{
		MGlobal::displayError("You need to provide one argument, the filename to import too.");
		return MS::kFailure;
	}

	// Do it
	MString strFilename = args.asString(0);
	return importRolladaFile(strFilename);
}

void* rageImportRolladaFile::creator()
//
//	Description:
//		this method exists to give Maya a way to create new objects
//      of this type. 
//
//	Return Value:
//		a new object of this type
//
{
	return new rageImportRolladaFile();
}

rageImportRolladaFile::rageImportRolladaFile()
//
//	Description:
//		rageImportRolladaFile constructor
//
{}

rageImportRolladaFile::~rageImportRolladaFile()
//
//	Description:
//		rageImportRolladaFile destructor
//
{
}
