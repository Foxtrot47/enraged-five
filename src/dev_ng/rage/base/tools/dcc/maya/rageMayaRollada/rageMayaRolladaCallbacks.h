#pragma once

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MSceneMessage.h>
#pragma warning(pop)

class rageMayaRolladaCallbacks
{
public:
	static void InitializeCallbacks();
	static void UninitializeCallbacks();

	static void rageMayaRolladaCallbacks::BeforeExportCallback(void*);
	static void rageMayaRolladaCallbacks::AfterImportCallback(void*);
	static void rageMayaRolladaCallbacks::AfterOpenCallback(void*);

private:

	static MCallbackId obBeforeExportCallbackId;
	static MCallbackId obAfterImportCallbackId;
	static MCallbackId obAfterOpenCallbackId;
};
