//
// Copyright (C) 
// 
// File: pluginMain.cpp
//
// Author: Maya Plug-in Wizard 2.0
//

#include "rageExportRolladaFileCmd.h"
#include "rageImportRolladaFileCmd.h"
#include "rageMayaRolladaCallbacks.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MFnPlugin.h>
#pragma warning(pop)


MStatus initializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is loaded into Maya.  It 
//		registers all of the services that this plug-in provides with 
//		Maya.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{ 
	MStatus   status;
	MFnPlugin plugin( obj, "", "7.0", "Any");

	status = plugin.registerCommand( "rageExportRolladaFile", rageExportRolladaFile::creator );
	if (!status) {
		status.perror("registerCommand");
		return status;
	}

	status = plugin.registerCommand( "rageImportRolladaFile", rageImportRolladaFile::creator );
	if (!status) {
		status.perror("registerCommand");
		return status;
	}

	rageMayaRolladaCallbacks::InitializeCallbacks();

	return status;
}

MStatus uninitializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is unloaded from Maya. It 
//		deregisters all of the services that it was providing.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{
	MStatus   status;
	MFnPlugin plugin( obj );

	rageMayaRolladaCallbacks::UninitializeCallbacks();

	status = plugin.deregisterCommand( "rageExportRolladaFile" );
	if (!status) {
		status.perror("deregisterCommand");
		return status;
	}

	status = plugin.deregisterCommand( "rageImportRolladaFile" );
	if (!status) {
		status.perror("deregisterCommand");
		return status;
	}

	return status;
}
