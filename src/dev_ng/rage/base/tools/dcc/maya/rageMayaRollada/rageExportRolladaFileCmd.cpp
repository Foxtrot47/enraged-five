//
// Copyright (C) 
// 
// File: rageExportRolladaFileCmd.cpp
//
// MEL Command: rageExportRolladaFile
//
// Author: Maya Plug-in Wizard 2.0
//

#include "rageExportRolladaFileCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MArgList.h>
#include <maya/MGlobal.h>
#pragma warning(pop)

MStatus rageExportRolladaFile::exportRolladaFile(const MString& strFilename)
{
	MGlobal::executeCommand("rageCollada_Save(rageRemoveFileExtension(\""+ strFilename +"\"), false)", true);
	return MS::kSuccess;
}

MStatus rageExportRolladaFile::doIt( const MArgList& args)
//
//	Description:
//		implements the MEL rageExportRolladaFile command.
//
//	Arguments:
//		args - the argument list that was passes to the command from MEL
//
//	Return Value:
//		MS::kSuccess - command succeeded
//		MS::kFailure - command failed (returning this value will cause the 
//                     MEL script that is being run to terminate unless the
//                     error is caught using a "catch" statement.
//
{
	MStatus stat = MS::kSuccess;

	// Command takes one arg, the filename to export to
	if(args.length() != 1)
	{
		MGlobal::displayError("You need to provide one argument, the filename to export too.");
		return MS::kFailure;
	}

	// Do it
	MString strFilename = args.asString(0);
	return exportRolladaFile(strFilename);
}

void* rageExportRolladaFile::creator()
//
//	Description:
//		this method exists to give Maya a way to create new objects
//      of this type. 
//
//	Return Value:
//		a new object of this type
//
{
	return new rageExportRolladaFile();
}

rageExportRolladaFile::rageExportRolladaFile()
//
//	Description:
//		rageExportRolladaFile constructor
//
{}

rageExportRolladaFile::~rageExportRolladaFile()
//
//	Description:
//		rageExportRolladaFile destructor
//
{
}
