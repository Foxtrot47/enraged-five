#ifndef _rageImportRolladaFileCmd
#define _rageImportRolladaFileCmd
//
// Copyright (C) 
// 
// File: rageImportRolladaFileCmd.h
//
// MEL Command: rageImportRolladaFile
//
// Author: Maya Plug-in Wizard 2.0
//

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#pragma warning(pop)

class MArgList;

class rageImportRolladaFile : public MPxCommand
{

public:
				rageImportRolladaFile();
	virtual		~rageImportRolladaFile();

	MStatus		doIt( const MArgList& );

	static		void* creator();

	static MStatus importRolladaFile(const MString& strFilename);

private:
	static MStatus fixupShaderNodeInMaya(const MString& strNodeName, const MString& strMtlPathAndFilename);
};

#endif
