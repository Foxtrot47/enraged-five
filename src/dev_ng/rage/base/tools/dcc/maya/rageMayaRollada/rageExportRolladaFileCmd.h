#ifndef _rageExportRolladaFileCmd
#define _rageExportRolladaFileCmd
//
// Copyright (C) 
// 
// File: rageExportRolladaFileCmd.h
//
// MEL Command: rageExportRolladaFile
//
// Author: Maya Plug-in Wizard 2.0
//

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#pragma warning(pop)

class MArgList;

class rageExportRolladaFile : public MPxCommand
{

public:
				rageExportRolladaFile();
	virtual		~rageExportRolladaFile();

	MStatus		doIt( const MArgList& );

	static		void* creator();

	static MStatus exportRolladaFile(const MString& strFilename);

private:
	// Store the data you will need to undo the command here
	//
};

#endif
