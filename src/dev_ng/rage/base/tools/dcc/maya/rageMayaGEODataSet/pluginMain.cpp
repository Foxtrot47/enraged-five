// 
// /pluginMain.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MGlobal.h>
#include <maya/MFnPlugin.h>
#include <maya/MMessage.h>
#include <maya/MSceneMessage.h>

#pragma warning(pop)

#include "system/param.h"

#include "rageMayaGeoDataSet.h"
#include "rageMayaGeoDataSetCmd.h"

using namespace rageGEO;

//-----------------------------------------------------------------------------

static MCallbackId	sMayaExitingCallbackId	= 0;
static bool			bPluginNeedsCleanup		= true;

//-----------------------------------------------------------------------------

static void mayaDisplayInfoMessage(const char* msg)
{
	MGlobal::displayInfo(msg);
}

static void mayaDisplayWarnMessage(const char* msg)
{
	MGlobal::displayWarning(msg);
}

static void mayaDisplayErrorMessage(const char* msg)
{
	MGlobal::displayError(msg);
}

//-----------------------------------------------------------------------------

static void SceneMayaExitingCallback(void* /*clientData*/)
{
	//Maya doesn't seem to always call the unitializePlugin method when its exiting, so
	//use this method to make sure all resources are freed up.  This helps to eliminate
	//memory leak dumps that really shouldn't be memory leaks.
	if(bPluginNeedsCleanup)
	{
		SHUTDOWN_PARSER;
		SHUTDOWN_GEODATASET;

		bPluginNeedsCleanup = false;
	}
}

//-----------------------------------------------------------------------------

MStatus initializePlugin( MObject obj )
{
	MStatus   status;
	MFnPlugin plugin( obj, "", "6.0", "Any");

	rage::sysParam::Init(0,NULL);

	INIT_PARSER;
	PARSER.Settings().SetFlag(parSettings::CASE_INSENSITIVE_TAG_MATCHING, true);

	INIT_GEODATASET;

	GEODATASET.SetInfoMsgFtor( MakeFunctor(mayaDisplayInfoMessage) );
	GEODATASET.SetWarnMsgFtor( MakeFunctor(mayaDisplayWarnMessage) );
	GEODATASET.SetErrorMsgFtor( MakeFunctor(mayaDisplayErrorMessage) );

	status = plugin.registerCommand("rageMayaGeoDataSet", rageMayaGeoDataSetCmd::creator, rageMayaGeoDataSetCmd::newSyntax);
	if(!status)
	{
		status.perror("registerCommand");
		return status;
	}

	//Register Scene Callbacks
	sMayaExitingCallbackId = MSceneMessage::addCallback(MSceneMessage::kMayaExiting, SceneMayaExitingCallback, NULL, &status);

	return status;
}

//-----------------------------------------------------------------------------

MStatus uninitializePlugin( MObject obj)
{
	MStatus   status;
	MFnPlugin plugin( obj );

	//Unregister Scene Callbacks
	MSceneMessage::removeCallback(sMayaExitingCallbackId);

	status = plugin.deregisterCommand("rageMayaGeoDataSet");
	if (!status) 
	{
		status.perror("deregisterCommand");
		return status;
	}
	
	if(bPluginNeedsCleanup)
	{
		SHUTDOWN_PARSER;
		SHUTDOWN_GEODATASET;

		bPluginNeedsCleanup = false;
	}

	return status;
}

//-----------------------------------------------------------------------------
