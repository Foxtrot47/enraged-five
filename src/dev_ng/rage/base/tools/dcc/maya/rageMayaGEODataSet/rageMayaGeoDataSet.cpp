// 
// /rageMayaGeoDataSet.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#include <deque>

#include "rageMayaGeoDataSet.h"

using namespace std;

namespace rageGEO
{

GeoParamValueVariant GeoParamValueVariant::nullGeoParamValue;

//-----------------------------------------------------------------------------

bool GeoPath::Init(const char* path)
{
	string stdPath(path);
	if(!stdPath.size())
		return false;

	unsigned int pathSepIdx = 0;
	unsigned int tagStart = 0;
	while(pathSepIdx != string::npos)
	{
		pathSepIdx = stdPath.find_first_of("/", tagStart);
		if( (pathSepIdx == string::npos) && (tagStart==0) )
		{
			//The path is just the type name
			m_RootName = stdPath;
			return true;
		}
		else
		{
			int count = string::npos;
			if(pathSepIdx != string::npos)
				count = pathSepIdx - count - 1;

			string pathSection = stdPath.substr(tagStart, count);

			size_t arrIdxStart,arrIdxEnd;
			arrIdxStart = pathSection.find_first_of("[");
			arrIdxEnd = pathSection.find_first_of("]");

			int arrIdx = -1;
			string name;

			if( (arrIdxStart != string::npos) && (arrIdxEnd != string::npos) )
			{
				string strArrIdx = pathSection.substr(arrIdxStart+1, (arrIdxEnd-arrIdxStart)-1);
				arrIdx = atoi(strArrIdx.c_str());
				name = pathSection.substr(0, arrIdxStart);
			}
			else
				name = pathSection;

			if(tagStart == 0)
			{
				m_RootName = name;
			}
			else
			{
				m_PathSections.push_back(GeoPathSection(name.c_str(), arrIdx));
			}

			tagStart = pathSepIdx + 1;
		}
	}

	m_PathString = path;

	return true;
}

//-----------------------------------------------------------------------------

rageMayaGeoDataSet::rageMayaGeoDataSet()
{
	m_InfoMsgFtor = rageGeoDataSetMsgFtor::NullFunctor();
	m_WarnMsgFtor = rageGeoDataSetMsgFtor::NullFunctor();
	m_ErrorMsgFtor = rageGeoDataSetMsgFtor::NullFunctor();
}

//-----------------------------------------------------------------------------

rageMayaGeoDataSet::~rageMayaGeoDataSet()
{
	
}

//-----------------------------------------------------------------------------

void rageMayaGeoDataSet::InitializePaths(const char* pTypePath, const char* pTemplatePath)
{
	m_TypeRootPath = pTypePath;
	if(m_TypeRootPath[m_TypeRootPath.size()-1] != '\\' &&
	   m_TypeRootPath[m_TypeRootPath.size()-1] != '/')
	   m_TypeRootPath += "\\";

	m_TemplateRootPath = pTemplatePath;
	if(m_TemplateRootPath[m_TemplateRootPath.size()-1] != '\\' &&
		m_TemplateRootPath[m_TemplateRootPath.size()-1] != '/')
		m_TemplateRootPath += "\\";

	m_ParamCache.InitializePaths(pTypePath, pTemplatePath);

	DisplayInfoMsg("Initialized GEO Type path to : %s", m_TypeRootPath.c_str());
	DisplayInfoMsg("Initialized GEO Template path to : %s", m_TemplateRootPath.c_str());
}

//-----------------------------------------------------------------------------

bool rageMayaGeoDataSet::ParseGEOTypeName(const char* typeName, vector<GeoTypePathMember>& pathMembers)
{
	string stdTypeName(typeName);
	if(!stdTypeName.size())
		return false;

	unsigned int pathSepIdx = 0;
	unsigned int tagStart = 0;
	while(pathSepIdx != string::npos)
	{
		pathSepIdx = stdTypeName.find_first_of("/", tagStart);
		if( (pathSepIdx == string::npos) && (tagStart==0) )
		{
			//The path is just the type name
			pathMembers.push_back(GeoTypePathMember(stdTypeName, -1));
			return true;
		}
		else
		{
			int count = string::npos;
			if(pathSepIdx != string::npos)
				count = pathSepIdx - count - 1;

			string pathSection = stdTypeName.substr(tagStart, count);

			size_t arrIdxStart,arrIdxEnd;
			arrIdxStart = pathSection.find_first_of("[");
			arrIdxEnd = pathSection.find_first_of("]");

			int arrIdx = -1;
			string name;

			if( (arrIdxStart != string::npos) && (arrIdxEnd != string::npos) )
			{
				string strArrIdx = pathSection.substr(arrIdxStart+1, (arrIdxEnd-arrIdxStart)-1);
				arrIdx = atoi(strArrIdx.c_str());
				name = pathSection.substr(0, arrIdxStart);
			}
			else
				name = pathSection;

			pathMembers.push_back(GeoTypePathMember(name, arrIdx));

			tagStart = pathSepIdx + 1;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

GeoParamValueVariant rageMayaGeoDataSet::TreeNodeToVariant(parTreeNode *pNode, bool bIsTemplateNode, GEOPARAMVALUE_TYPE paramType)
{
	GeoParamValueVariant retVal;
	
	if(!bIsTemplateNode)
	{
		parAttribute *pValueAttr = pNode->GetElement().FindAttribute("value");
		if(!pValueAttr)
		{
			return GeoParamValueVariant::nullGeoParamValue;
		}
		const char* pValue = pValueAttr->GetStringValue();
		
		switch(paramType)
		{
			case TYPE_INT:
				{
					retVal.SetValue((int)atoi(pValue));
					break;
				}
			case TYPE_FLOAT:
				{
					retVal.SetValue((float)atof(pValue));
					break;
				}
			case TYPE_BOOL:
				{
					if(strcmpi(pValue, "true")==0)
						retVal.SetValue(true);
					else
						retVal.SetValue(false);
					break;
				}
			case TYPE_STRING:
				{
					retVal.SetValue(pValue);
					break;
				}
			default:
				retVal = GeoParamValueVariant::nullGeoParamValue;
		}//End switch(paramType)
	}
	else
	{
		parAttribute *pInitAttr = pNode->GetElement().FindAttribute("init");
		if(!pInitAttr)
		{
			return GeoParamValueVariant::nullGeoParamValue;
		}
		const char* pInit = pInitAttr->GetStringValue();

		switch(paramType)
		{
			case TYPE_INT:
				{
					retVal.SetValue((int)atoi(pInit));
					break;
				}
			case TYPE_FLOAT:
				{
					retVal.SetValue((float)atof(pInit));
					break;
				}
			case TYPE_BOOL:
				{
					if(strcmpi(pInit, "true")==0)
						retVal.SetValue(true);
					else
						retVal.SetValue(false);

					break;
				}
			case TYPE_STRING:
				{
					retVal.SetValue(pInit);
					break;
				}
			default:
				retVal = GeoParamValueVariant::nullGeoParamValue;
		}//End swtich(paramType)
	}

	return retVal;
}


//-----------------------------------------------------------------------------

GeoParamValueVariant rageMayaGeoDataSet::GetGEOParamValue(const char* typeName, const char* paramName, GEOPARAMVALUE_TYPE paramType )
{
	list<searchItem>			searchQueue;
	vector<GeoTypePathMember>	pathMembers;
	GeoPath						geoTypePath;

	if(!geoTypePath.Init(typeName))
	{
		DisplayErrorMsg("Supplied GEO path '%s', is not in the correct format, operation aborted.", typeName);
		return GeoParamValueVariant::nullGeoParamValue;
	}

	searchQueue.push_back(searchItem(geoTypePath.GetRootName(), false));

	while(searchQueue.size())
	{
		searchItem curItem = searchQueue.front();
		searchQueue.pop_front();

		if(!curItem.m_bIsTemplate)
		{
			GeoParamValueVariant val = FindGEOParamNodeInType(geoTypePath, paramName, paramType, curItem, searchQueue);
			if(val.GetType() != TYPE_NULL)
				return val;
		}
		else
		{
			GeoParamValueVariant val = FindGEOParamNodeInTemplate(paramName, paramType, curItem, searchQueue);
			if(val.GetType() != TYPE_NULL)
				return val;
		}
	}

	return GeoParamValueVariant::nullGeoParamValue;
}

//-----------------------------------------------------------------------------

bool rageMayaGeoDataSet::FindGeoParameterNode(parTreeNode *pRootNode,
												const char *paramPath,
												GEOPARAMVALUE_TYPE paramType,
												bool bIsTemplate,
												GeoParamValueVariant &retVal)
{
	GeoPath paramGeoPath;
	if(!paramGeoPath.Init(paramPath))
	{
		DisplayErrorMsg("FindGeoParameterNode - Invalid parameter path '%s'\n", paramPath);
		return false;
	}

	parTreeNode *pParamNode = NULL;

	//Is the parameter specification a nested param spec?
	if(!paramGeoPath.GetNumSections())
	{
		if(bIsTemplate)
			pParamNode = pRootNode->FindChildWithAttribute("name", paramGeoPath.GetRootName());
		else
			pParamNode = pRootNode->FindChildWithName(paramGeoPath.GetRootName());
	
		if(pParamNode)
		{
			retVal = TreeNodeToVariant(pParamNode, bIsTemplate, paramType);
			return true;
		}
		else
			return false;
	}
	else
	{
		//The parameter spec is a nested param spec
		if(bIsTemplate)
			pParamNode = pRootNode->FindChildWithAttribute("name", paramGeoPath.GetRootName());
		else
			pParamNode = pRootNode->FindChildWithName(paramGeoPath.GetRootName());

		if(!pParamNode)
		{
			return false;
		}

		for(int i=0; i<paramGeoPath.GetNumSections(); i++)
		{
			if(bIsTemplate)
				pParamNode = pParamNode->FindChildWithAttribute("name", paramGeoPath.GetSectionName(i));
			else
				pParamNode = pParamNode->FindChildWithName(paramGeoPath.GetSectionName(i));
			
			if(!pParamNode)
				return false;
		}

		retVal = TreeNodeToVariant(pParamNode, bIsTemplate, paramType);
		return true;
	}
}

//-----------------------------------------------------------------------------

GeoParamValueVariant rageMayaGeoDataSet::FindGEOParamNodeInType(GeoPath& geoTypePath,
																const char* paramName, 
																GEOPARAMVALUE_TYPE paramType,
																searchItem& curItem,
																list<searchItem>& searchQueue)
{
	GeoParamValueVariant retVal = GeoParamValueVariant::nullGeoParamValue;

	//Look through the type
	string pathToGEOTypeFile = m_TypeRootPath;
	pathToGEOTypeFile += curItem.m_name;

	parTree* pTree = PARSER.LoadTree(pathToGEOTypeFile.c_str(), "geotype");
	if(!pTree)
	{
		DisplayErrorMsg("Failed to load GEO type file '%s.geotype'", pathToGEOTypeFile.c_str());
		return GeoParamValueVariant::nullGeoParamValue;
	}

	parTreeNode *pRoot = pTree->GetRoot();
	if(!pRoot)
	{
		DisplayErrorMsg("Parsing failed on GEO type file '%s.geotype', file has no root node.", pathToGEOTypeFile.c_str());
		delete pTree;
		return GeoParamValueVariant::nullGeoParamValue;
	}

	//If there is a base GEO Type, add it to the search queue, otherwise add the template
	parAttribute *pBaseAttr = pRoot->GetElement().FindAttribute("base");
	if(pBaseAttr)
	{
		//Add the parent type to the search path
		string baseName = pBaseAttr->GetStringValue();
		searchQueue.push_back(searchItem(baseName, false));
	}
	else
	{
		//If the type spec doesn't point to a nested GEO then add the parent template to the search path
		if(!geoTypePath.GetNumSections())
		{
			parAttribute *pTemplateAttr = pRoot->GetElement().FindAttribute("template");
			if(!pTemplateAttr)
			{
				DisplayErrorMsg("Malformed GEO Type file '%s', the type file doesn't have a template attribute specified",
					pathToGEOTypeFile.c_str());
				delete pTree;
				return GeoParamValueVariant::nullGeoParamValue;
			}
			else
			{
				searchQueue.push_back(searchItem(pTemplateAttr->GetStringValue(), true));
			}
		}
	}		

	//If the type path uses contained geos, then loop through the sections of the type path
	//and try to locate the queried parameter under the correct geo section
	if(geoTypePath.GetNumSections())
	{
		bool bFound = true;
		parTreeNode *pCurNode = pRoot;
		for(int i=0; i<geoTypePath.GetNumSections(); i++)
		{
			pCurNode = pCurNode->FindChildWithName(geoTypePath.GetSectionName(i));
			if(pCurNode)
			{
				if(geoTypePath.IsSectionArray(i))
				{
					pCurNode = pCurNode->FindChildWithIndex(geoTypePath.GetSectionArrayIndex(i));
					if(!pCurNode)
					{
						bFound = false;
						break;
					}
				}
			}
			else
			{
				bFound = false;
				break;
			}
		}
		
		if(bFound)
		{
			GeoParamValueVariant paramVal;
			if(FindGeoParameterNode(pCurNode, paramName, paramType, false, paramVal))
			{
				//Found the value of the parameter...
				retVal = paramVal;
			}
			else
			{
				//The parameter wasn't found, so add the geo template for the contained type to the search queue
				parAttribute *pTemplateAttr = pCurNode->GetElement().FindAttribute("template");
				if(pTemplateAttr)
				{
					searchQueue.push_back(searchItem(pTemplateAttr->GetStringValue(), true));
				}
				else
				{
					DisplayErrorMsg("GEO type file '%s' contains GEO node without a template identifier '%s'",
						pathToGEOTypeFile.c_str(), 
						pCurNode->GetElement().GetName());
				}
			}
		}
	}//End if(geoTypePath.GetNumSections())
	else 
	{
		GeoParamValueVariant paramVal;
		if(FindGeoParameterNode(pRoot, paramName, paramType, false, paramVal))
		{
			retVal = paramVal;
		}
	}

	delete pTree;
	return retVal;
}

//-----------------------------------------------------------------------------

GeoParamValueVariant rageMayaGeoDataSet::FindGEOParamNodeInTemplate(const char* paramName, 
																	GEOPARAMVALUE_TYPE paramType,
																	searchItem& curItem,
																	list<searchItem>& searchQueue)
{
	GeoParamValueVariant retVal = GeoParamValueVariant::nullGeoParamValue;
	GeoPath	templatePath;

	if(!templatePath.Init(curItem.m_name.c_str()))
	{
		DisplayErrorMsg("GEO path '%s', is not in the correct format, operation aborted.", curItem.m_name.c_str());
		return GeoParamValueVariant::nullGeoParamValue;
	}

	//Look through the templates..
	string geoTemplateFilePath = m_TemplateRootPath;
	geoTemplateFilePath += templatePath.GetRootName();

	//Load the template...
	parTree* pTree = PARSER.LoadTree(geoTemplateFilePath.c_str(), "geotemplate");
	if(!pTree)
	{
		DisplayErrorMsg("Failed to load GEO template file '%s.geotemplate'", geoTemplateFilePath.c_str());
		return GeoParamValueVariant::nullGeoParamValue;
	}

	parTreeNode *pRoot = pTree->GetRoot();
	if(!pRoot)
	{
		DisplayErrorMsg("Parsing failed on GEO type file '%s.geotemplate', file has no root node.", geoTemplateFilePath.c_str());
		delete pTree;
		return GeoParamValueVariant::nullGeoParamValue;
	}

	//Is this a path to a nested GEO?
	if(!templatePath.GetNumSections())
	{
		//If there is a base GEO Template, add it to the search queue
		parAttribute *pBaseAttr = pRoot->GetElement().FindAttribute("base");
		if(pBaseAttr)
		{
			string baseName = pBaseAttr->GetStringValue();
			searchQueue.push_back(searchItem(baseName, true));
		}

		//The type should be in the root members section of the template
		parTreeNode *pMembersNode = pRoot->FindChildWithName("Members");
		if(pMembersNode)
		{
			GeoParamValueVariant paramVal;
			if(FindGeoParameterNode(pMembersNode, paramName, paramType, true, paramVal))
			{
				retVal = paramVal;
			}
		}//End if(pMembersNode)
		else
		{
			DisplayErrorMsg("Invalid GEO template specification in file '%s', root template has no \"Members\" section.", geoTemplateFilePath.c_str());
		}
	}
	else
	{
		//The Type we are looking for is a nested type
		bool bFound = true;
		parTreeNode *pCurNode = pRoot;
		for(int i=0; i<templatePath.GetNumSections(); i++)
		{
			parTreeNode *pTemplatesNode = pRoot->FindChildWithName("GeoTemplates");
			if(pTemplatesNode)
			{
				pCurNode = pTemplatesNode->FindChildWithAttribute("name", templatePath.GetSectionName(i));
				if(!pCurNode)
				{
					bFound = false;
					break;
				}
			}
		}

		//Did we find the nested type we were looking for?
		if(bFound)
		{
			//Locate the "Members" section of the template
			parTreeNode* pMembersNode = pCurNode->FindChildWithName("Members");
			if(pMembersNode)
			{
				GeoParamValueVariant paramVal;
				if(FindGeoParameterNode(pMembersNode, paramName, paramType, true, paramVal))
				{
					retVal = paramVal;
				}
			}
			else
			{
				DisplayErrorMsg("Invalid GEO template specification in file '%s', nested template '%s' has no \"Members\" section.",
					geoTemplateFilePath.c_str(),
					pCurNode->GetElement().FindAttribute("name")->GetStringValue());
			}
		}
	}//End else

	delete pTree;
	return retVal;
}

//-----------------------------------------------------------------------------

bool rageMayaGeoDataSet::IsGEOParamAvailable(const char* typeName, const char* paramName, GEOPARAMVALUE_TYPE paramType)
{
	GeoParamValueVariant paramVal = GetGEOParamValue(typeName, paramName, paramType);
	if(paramVal.GetType() != TYPE_NULL)
	{
		//Cache the node
		m_ParamCache.CacheNode(typeName, paramName, paramVal);
		return true;
	}
	else
		return false;
}

//-----------------------------------------------------------------------------

bool rageMayaGeoDataSet::GetGEOParamValueAsInt(const char* typeName, const char* paramName, int& retVal)
{
	GeoParamValueVariant paramVal;
	
	//Check the chache first
	paramVal = m_ParamCache.FindNode(typeName, paramName);
	if(paramVal.GetType() != TYPE_NULL)
	{
#ifdef _DEBUG
		DisplayInfoMsg("Retrieved '%s':'%s' from the cache.", typeName, paramName);
#endif //_DEBUG
		retVal = paramVal.GetIntValue();
		return true;
	}
	else
	{
		paramVal = GetGEOParamValue(typeName, paramName, TYPE_INT);
		if(paramVal.GetType() != TYPE_NULL)
		{
			retVal = paramVal.GetIntValue();

			//Cache the value
			m_ParamCache.CacheNode(typeName, paramName, paramVal);
			return true;
		}
		else
			return false;
	}
}

//-----------------------------------------------------------------------------

bool rageMayaGeoDataSet::GetGEOParamValueAsFloat(const char* typeName, const char* paramName, float& retVal)
{
	GeoParamValueVariant paramVal;
	
	//Check the chache first
	paramVal = m_ParamCache.FindNode(typeName, paramName);
	if(paramVal.GetType() != TYPE_NULL)
	{
#ifdef _DEBUG
		DisplayInfoMsg("Retrieved '%s':'%s' from the cache.", typeName, paramName);
#endif //_DEBUG
		retVal = paramVal.GetFloatValue();
		return true;
	}
	else
	{
		paramVal = GetGEOParamValue(typeName, paramName, TYPE_FLOAT);
		if(paramVal.GetType() != TYPE_NULL)
		{
			retVal = paramVal.GetFloatValue();

			//Cache the value
			m_ParamCache.CacheNode(typeName, paramName, paramVal);
			return true;
		}
		else
			return false;
	}
}

//-----------------------------------------------------------------------------

bool rageMayaGeoDataSet::GetGEOParamValueAsBool(const char* typeName, const char* paramName, bool& retVal)
{
	GeoParamValueVariant paramVal;
	
	//Check the chache first
	paramVal = m_ParamCache.FindNode(typeName, paramName);
	if(paramVal.GetType() != TYPE_NULL)
	{
#ifdef _DEBUG
		DisplayInfoMsg("Retrieved '%s':'%s' from the cache.", typeName, paramName);
#endif //_DEBUG
		retVal = paramVal.GetBoolValue();
		return true;
	}
	else
	{
		paramVal = GetGEOParamValue(typeName, paramName, TYPE_BOOL);
		if(paramVal.GetType() != TYPE_NULL)
		{
			retVal = paramVal.GetBoolValue();

			//Cache the value
			m_ParamCache.CacheNode(typeName, paramName, paramVal);
			return true;
		}
		else
			return false;
	}
}

//-----------------------------------------------------------------------------

bool rageMayaGeoDataSet::GetGEOParamValueAsString(const char* typeName, const char* paramName, string& retVal)
{
	GeoParamValueVariant paramVal;
	
	//Check the chache first
	paramVal = m_ParamCache.FindNode(typeName, paramName);
	if(paramVal.GetType() != TYPE_NULL)
	{
#ifdef _DEBUG
		DisplayInfoMsg("Retrieved '%s':'%s' from the cache.", typeName, paramName);
#endif //_DEBUG
		retVal = paramVal.GetStringValue();
		return true;
	}
	else
	{
		paramVal = GetGEOParamValue(typeName, paramName, TYPE_STRING);
		if(paramVal.GetType() != TYPE_NULL)
		{
			retVal = paramVal.GetStringValue();

			//Cache the value
			m_ParamCache.CacheNode(typeName, paramName, paramVal);
			return true;
		}
		else
			return false;
	}
}

//-----------------------------------------------------------------------------

bool rageMayaGeoDataSet::GetGEOParamValueAsVec3(const char* typeName, const char* paramName, Vector3& retVal)
{
	GeoParamValueVariant paramVal;
	
	//Check the chache first
	paramVal = m_ParamCache.FindNode(typeName, paramName);
	if(paramVal.GetType() != TYPE_NULL)
	{
#ifdef _DEBUG
		DisplayInfoMsg("Retrieved '%s':'%s' from the cache.", typeName, paramName);
#endif //_DEBUG
		retVal = paramVal.GetVector3Value();
		return true;
	}
	else
	{
		paramVal = GetGEOParamValue(typeName, paramName, TYPE_VECTOR3);
		if(paramVal.GetType() != TYPE_NULL)
		{
			retVal = paramVal.GetVector3Value();

			//Cache the value
			m_ParamCache.CacheNode(typeName, paramName, paramVal);
			return true;
		}
		else
			return false;
	}
}

//-----------------------------------------------------------------------------

void rageMayaGeoDataSet::DisplayInfoMsg(const char* format, ...)
{
	char buffer[2048];

	va_list args;
	va_start( args, format);

	vsprintf(buffer, format, args);

	m_InfoMsgFtor(buffer);
}

//-----------------------------------------------------------------------------

void rageMayaGeoDataSet::DisplayWarnMsg(const char* format, ...)
{
	char buffer[2048];

	va_list args;
	va_start( args, format);

	vsprintf(buffer, format, args);

	m_WarnMsgFtor(buffer);
}

//-----------------------------------------------------------------------------

void rageMayaGeoDataSet::DisplayErrorMsg(const char* format, ...)
{
	char buffer[2048];

	va_list args;
	va_start( args, format);

	vsprintf(buffer, format, args);

	m_ErrorMsgFtor(buffer);
}

//-----------------------------------------------------------------------------

bool GeoDataSetCache::InitializePaths(const char* typeFilePath, const char* templateFilePath)
{
	FlushCache();

	if(m_CachPathChangeHandles[GEO_TYPE_DIR] != INVALID_HANDLE_VALUE)
	{
		FindCloseChangeNotification(m_CachPathChangeHandles[GEO_TYPE_DIR]);
		m_CachPathChangeHandles[GEO_TYPE_DIR] = INVALID_HANDLE_VALUE;
	}
	if(m_CachPathChangeHandles[GEO_TEMPLATE_DIR] != INVALID_HANDLE_VALUE)
	{
		FindCloseChangeNotification(m_CachPathChangeHandles[GEO_TEMPLATE_DIR]);
		m_CachPathChangeHandles[GEO_TEMPLATE_DIR] = INVALID_HANDLE_VALUE;
	}

	m_CachPathChangeHandles[GEO_TYPE_DIR] = FindFirstChangeNotification(typeFilePath,
																		FALSE,
																		FILE_NOTIFY_CHANGE_LAST_WRITE);
	if(m_CachPathChangeHandles[GEO_TYPE_DIR] == INVALID_HANDLE_VALUE)
	{
		return false;
	}

	m_CachPathChangeHandles[GEO_TEMPLATE_DIR] = FindFirstChangeNotification(templateFilePath,
																			FALSE,
																			FILE_NOTIFY_CHANGE_LAST_WRITE);
	if(m_CachPathChangeHandles[GEO_TEMPLATE_DIR] == INVALID_HANDLE_VALUE)
	{
		return false;
	}

	return true;
}

//-----------------------------------------------------------------------------

void GeoDataSetCache::FlushCache()
{
#ifdef _DEBUG
	GEODATASET.DisplayInfoMsg("Flushing cache..");
#endif //_DEBUG

	m_ParamCache.clear();
}

//-----------------------------------------------------------------------------

string GeoDataSetCache::BuildCacheKey(const char* geoPathStr, const char* paramName) const
{
	//TODO : This really should be optimized, as its kind of a kludgy way to create
	//a key for the cache
	string result;

	result += geoPathStr;
	result += ":::";
	result += paramName;
	
	return result;
}

//-----------------------------------------------------------------------------

GeoParamValueVariant GeoDataSetCache::FindNode(const GeoPath& gp, const char* paramName)
{
	return(FindNode(gp.GetPathAsString(), paramName));
}

//-----------------------------------------------------------------------------

GeoParamValueVariant GeoDataSetCache::FindNode(const char* geoPath, const char* paramName)
{
	bool bFlushCache = false;

	DWORD dwResult = WaitForSingleObject(m_CachPathChangeHandles[GEO_TYPE_DIR], 0);
	if(dwResult == WAIT_OBJECT_0)
	{
		FindNextChangeNotification(m_CachPathChangeHandles[GEO_TYPE_DIR]);

		while(WaitForSingleObject(m_CachPathChangeHandles[GEO_TYPE_DIR], 0) == WAIT_OBJECT_0)
			FindNextChangeNotification(m_CachPathChangeHandles[GEO_TYPE_DIR]);

		bFlushCache = true;
	}

	dwResult = WaitForSingleObject(m_CachPathChangeHandles[GEO_TEMPLATE_DIR], 0);
	if(dwResult == WAIT_OBJECT_0)
	{
		FindNextChangeNotification(m_CachPathChangeHandles[GEO_TEMPLATE_DIR]);

		while(WaitForSingleObject(m_CachPathChangeHandles[GEO_TEMPLATE_DIR], 0) == WAIT_OBJECT_0)
			FindNextChangeNotification(m_CachPathChangeHandles[GEO_TEMPLATE_DIR]);

		bFlushCache = true;
	}

	if(bFlushCache)
	{
		FlushCache();
		return GeoParamValueVariant::nullGeoParamValue;
	}
	else
	{
		string key = BuildCacheKey(geoPath, paramName);

		GEOPARAMCACHE::iterator it = m_ParamCache.find(key);
		if(it == m_ParamCache.end())
			return GeoParamValueVariant::nullGeoParamValue;
		else
			return it->second;
	}
}

//-----------------------------------------------------------------------------

void GeoDataSetCache::CacheNode(const GeoPath& gp, const char* paramName, GeoParamValueVariant& val)
{	
	CacheNode(gp.GetPathAsString(), paramName, val);
}

//-----------------------------------------------------------------------------

void GeoDataSetCache::CacheNode(const char* geoPath, const char* paramName, GeoParamValueVariant& val)
{	
	string key = BuildCacheKey(geoPath, paramName);
	m_ParamCache.insert( pair<string, GeoParamValueVariant>(key, val));
}

//-----------------------------------------------------------------------------

}//End namespace rageGEO
