// 
// /rageMayaGeoDataSetCmd.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "rageMayaGeoDataSet.h"
#include "rageMayaGeoDataSetCmd.h"

using namespace rageGEO;

//-----------------------------------------------------------------------------

#define	kGeoSetPath				"-gp"
#define kGeoSetPathLong			"-geopath"

#define kGeoGetValue			"-gv"
#define kGeoGetValueLong		"-getvalue"

#define kGeoParamExists			"-pe"
#define kGeoParamExistsLong		"-paramexists"

//-----------------------------------------------------------------------------

void* rageMayaGeoDataSetCmd::creator()
{
	return new rageMayaGeoDataSetCmd();
}

//-----------------------------------------------------------------------------

MSyntax rageMayaGeoDataSetCmd::newSyntax()
{
	MSyntax syntax;

	syntax.enableQuery();

	//params: (0) Root GEO Path
	syntax.addFlag(kGeoSetPath, kGeoSetPathLong, MSyntax::kString);

	//params: (0) Type Name, (1) Param Name, (2) Param Type
	syntax.addFlag(kGeoGetValue, kGeoGetValueLong, MSyntax::kString, MSyntax::kString, MSyntax::kString);

	//params: (0) Type Name, (1) Param Name, (2) Param Type
	syntax.addFlag(kGeoParamExists, kGeoParamExistsLong, MSyntax::kString, MSyntax::kString, MSyntax::kString);

	return syntax;
}

//-----------------------------------------------------------------------------

rageMayaGeoDataSetCmd::rageMayaGeoDataSetCmd()
{

}

//-----------------------------------------------------------------------------

rageMayaGeoDataSetCmd::~rageMayaGeoDataSetCmd()
{

}

//-----------------------------------------------------------------------------

MStatus rageMayaGeoDataSetCmd::doIt( const MArgList &args)
{
	MStatus status;
	MArgDatabase argData(syntax(), args);
	char errMsg[256];

	if(argData.isFlagSet(kGeoSetPath, &status))
	{
		if(argData.isQuery(&status))
		{
			MString typePath(GEODATASET.GetTypeRootPath());
			MString templatePath(GEODATASET.GetTemplateRootPath());

			MStringArray resStrArr;
			resStrArr.append(typePath);
			resStrArr.append(templatePath);

			setResult(resStrArr);
			return MS::kSuccess;
		}
		else
		{
			MString geoRootPath;
			argData.getFlagArgument(kGeoSetPath, 0, geoRootPath);

			if( (geoRootPath.asChar()[geoRootPath.length()] != '\\') &&
				(geoRootPath.asChar()[geoRootPath.length()] != '/') )
				geoRootPath += "\\";

			MString geoTypePath = geoRootPath;
			geoTypePath += "GEOTypes";

			MString geoTemplatePath = geoRootPath;
			geoTemplatePath += "GEOTemplates";
			
			if(GetFileAttributes(geoTypePath.asChar()) == INVALID_FILE_ATTRIBUTES)
			{
				//The directory doesn't exit
				sprintf(errMsg,"Supplied type file path '%s' does not exist.", geoTypePath.asChar());
				MGlobal::displayError(errMsg);
				return MS::kFailure;
			}

			if(GetFileAttributes(geoTemplatePath.asChar()) == INVALID_FILE_ATTRIBUTES)
			{
				//The directory doesn't exit
				sprintf(errMsg,"Supplied template file path '%s' does not exist.", geoTemplatePath.asChar());
				MGlobal::displayError(errMsg);
				return MS::kFailure;
			}

			GEODATASET.InitializePaths(geoTypePath.asChar(), geoTemplatePath.asChar());
			return MS::kSuccess;
		}
	}
	else if(argData.isFlagSet(kGeoParamExists, &status))
	{
		MString typeName;
		MString paramName;
		MString paramType;

		status = argData.getFlagArgument(kGeoParamExists, 0, typeName);
		if(status != MS::kSuccess)
		{
			status.perror("getFlagArgument:typeName");
			return MS::kFailure;
		}

		status = argData.getFlagArgument(kGeoParamExists, 1, paramName);
		if(status != MS::kSuccess)
		{
			status.perror("getFlagArgument:paramName");
			return MS::kFailure;
		}
		status = argData.getFlagArgument(kGeoParamExists, 2, paramType);
		if(status != MS::kSuccess)
		{
			status.perror("getFlagArgument:paramType");
			return MS::kFailure;
		}

		return GetGEOParamExists(typeName, paramName, paramType);
	}
	else if(argData.isFlagSet(kGeoGetValue, &status))
	{
		MString typeName;
		MString paramName;
		MString paramType;
		
		status = argData.getFlagArgument(kGeoGetValue, 0, typeName);
		if(status != MS::kSuccess)
		{
			status.perror("getFlagArgument:typeName");
			return MS::kFailure;
		}
		status = argData.getFlagArgument(kGeoGetValue, 1, paramName);
		if(status != MS::kSuccess)
		{
			status.perror("getFlagArgument:paramName");
			return MS::kFailure;
		}
		status = argData.getFlagArgument(kGeoGetValue, 2, paramType);
		if(status != MS::kSuccess)
		{
			status.perror("getFlagArgument:paramType");
			return MS::kFailure;
		}
	
		return GetGEOParamValue(typeName, paramName, paramType);
	}

	return status;
}

//-----------------------------------------------------------------------------

MStatus rageMayaGeoDataSetCmd::GetGEOParamExists(MString& typeName, MString& paramName, MString& paramType)
{
	char errMsgBuf[256];

	GEOPARAMVALUE_TYPE gpt = StringToGeoParamValue(paramType);
	if(gpt == TYPE_NULL)
	{
		sprintf(errMsgBuf,"Unknown parameter type '%s'", paramType.asChar());
		MGlobal::displayError(errMsgBuf);
		return MS::kFailure;
	}

	if(GEODATASET.IsGEOParamAvailable(typeName.asChar(), paramName.asChar(), gpt))
		setResult(true);
	else
		setResult(false);
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus rageMayaGeoDataSetCmd::GetGEOParamValue(MString& typeName, MString& paramName, MString& paramType)
{
	MStatus status = MS::kSuccess;
	char errMsgBuf[256];

	if(paramType == "int")
	{
		int retVal;
		if(GEODATASET.GetGEOParamValueAsInt(typeName.asChar(), paramName.asChar(), retVal))
		{
			setResult(retVal);
			status = MS::kSuccess;
		}
		else
		{
			sprintf(errMsgBuf,"GEO Parameter value %s, does not exist for type %s", paramName.asChar(), typeName.asChar());
			MGlobal::displayError(errMsgBuf);
			status = MS::kFailure;
		}
	}
	else if(paramType == "bool")
	{
		bool retVal;
		if(GEODATASET.GetGEOParamValueAsBool(typeName.asChar(), paramName.asChar(), retVal))
		{
			setResult(retVal);
			status == MS::kSuccess;
		}
		else
		{
			sprintf(errMsgBuf,"GEO Parameter value %s, does not exist for type %s", paramName.asChar(), typeName.asChar());
			MGlobal::displayError(errMsgBuf);
			status = MS::kFailure;
		}
	}
	else if(paramType == "float")
	{
		float retVal;
		if(GEODATASET.GetGEOParamValueAsFloat(typeName.asChar(), paramName.asChar(), retVal))
		{
			setResult(retVal);
			status == MS::kSuccess;
		}
		else
		{
			sprintf(errMsgBuf,"GEO Parameter value %s, does not exist for type %s", paramName.asChar(), typeName.asChar());
			MGlobal::displayError(errMsgBuf);
			status = MS::kFailure;
		}
	}
	else if(paramType == "string")
	{
		string retVal;
		if(GEODATASET.GetGEOParamValueAsString(typeName.asChar(), paramName.asChar(), retVal))
		{
			setResult(retVal.c_str());
			status == MS::kSuccess;
		}
		else
		{
			sprintf(errMsgBuf,"GEO Parameter value %s, does not exist for type %s", paramName.asChar(), typeName.asChar());
			MGlobal::displayError(errMsgBuf);
			status = MS::kFailure;
		}
	}
	else if(paramType == "vector3")
	{
		Vector3 retVal;
		if(GEODATASET.GetGEOParamValueAsVec3(typeName.asChar(), paramName.asChar(), retVal))
		{
			MDoubleArray floatArr;
			floatArr.append(retVal.x);
			floatArr.append(retVal.y);
			floatArr.append(retVal.z);
			setResult(floatArr);
			status == MS::kSuccess;
		}
		else
		{
			sprintf(errMsgBuf,"GEO Parameter value %s, does not exist for type %s", paramName.asChar(), typeName.asChar());
			MGlobal::displayError(errMsgBuf);
			status = MS::kFailure;
		}
	}
	
	return status;
}

//-----------------------------------------------------------------------------

GEOPARAMVALUE_TYPE	rageMayaGeoDataSetCmd::StringToGeoParamValue(MString& paramType)
{
	if(paramType == "bool")
		return TYPE_BOOL;
	else if(paramType == "int")
		return TYPE_INT;
	else if(paramType == "float")
		return TYPE_FLOAT;
	else if(paramType == "string")
		return TYPE_STRING;
	else if(paramType == "vector3")
		return TYPE_VECTOR3;
	else
		return TYPE_NULL;
}

//-----------------------------------------------------------------------------
