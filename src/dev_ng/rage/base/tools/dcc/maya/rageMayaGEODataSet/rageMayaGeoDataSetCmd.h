// 
// /rageMayaGeoDataSetCmd.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef _RAGEMAYAGEODATASETCMD_H_
#define _RAGEMAYAGEODATASETCMD_H_

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include <maya/MGlobal.h>
#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MCommandResult.h>
#include <maya/MDoubleArray.h>
#include <maya/MStringArray.h>

#pragma warning(pop)

#include "rageMayaGeoDataSet.h"

//-----------------------------------------------------------------------------

class rageMayaGeoDataSetCmd : public MPxCommand
{
public:
	rageMayaGeoDataSetCmd();
	virtual ~rageMayaGeoDataSetCmd();

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator();
	static MSyntax	newSyntax();

private:
	rageGEO::GEOPARAMVALUE_TYPE	StringToGeoParamValue(MString& paramType);

	MStatus				GetGEOParamValue(MString& typeName, MString& paramName, MString& paramType);
	MStatus				GetGEOParamExists(MString& typeName, MString& paramName, MString& paramType); 

};

//-----------------------------------------------------------------------------

#endif //_RAGEMAYAGEODATASETCMD_H_
