// 
// /pluginMain.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MGlobal.h>
#include <maya/MFnPlugin.h>
#pragma warning(pop)

#include "rageMayaGeoDataSet.h"
#include "rageMayaGeoDataSetCmds.h"
#include "rageMayaGeoSceneCallbacks.h"
#include "rageMayaGeoInstanceImporter.h"
#include "rageMayaGeoInstance.h"
#include "rageMayaGeoInstanceCmd.h"


//-----------------------------------------------------------------------------

struct mayaPluginCmd
{
	const char*	name;
	void*		(*CreatorPtr)(void);
	MSyntax		(*SyntaxPtr)(void);
};

static const int s_nCmds = 19;
static mayaPluginCmd s_cmds[s_nCmds] = 
{ 
	{"GeoLoadDataSet", &GeoLoadDataSetCmd::creator, &GeoLoadDataSetCmd::newSyntax},
	{"GeoSaveDataSet", &GeoSaveDataSetCmd::creator, &GeoSaveDataSetCmd::newSyntax},
	{"GeoUnLoadDataSet", &GeoUnLoadDataSetCmd::creator, &GeoUnLoadDataSetCmd::newSyntax},
	{"GeoGetTypeMembersOfType", &GeoGetTypeMembersOfTypeCmd::creator, &GeoGetTypeMembersOfTypeCmd::newSyntax},
	{"GeoGetNameFromUUID", &GeoGetNameFromUUIDCmd::creator, &GeoGetNameFromUUIDCmd::newSyntax},
	{"GeoGetUUIDFromName", &GeoGetUUIDFromNameCmd::creator, &GeoGetUUIDFromNameCmd::newSyntax},
	{"GeoGetBaseTypeName", &GeoGetBaseTypeNameCmd::creator, &GeoGetBaseTypeNameCmd::newSyntax},
	{"GeoGetTemplateName", &GeoGetTemplateNameCmd::creator, &GeoGetTemplateNameCmd::newSyntax},
	{"GeoMarkAllTypes", &GeoMarkAllInstancesCmd::creator, &GeoMarkAllInstancesCmd::newSyntax},
	{"GeoDeleteInstance", &GeoDeleteInstanceCmd::creator, &GeoDeleteInstanceCmd::newSyntax},
	{"GeoGetAvailableTypes", &GeoGetAvailableTypesCmd::creator, &GeoGetAvailableTypesCmd::newSyntax},
	{"GeoGetInstanceContainerNamesFromUUID", &GeoGetInstanceContainerNamesFromUUIDCmd::creator, &GeoGetInstanceContainerNamesFromUUIDCmd::newSyntax},
	{"GeoGetInstanceContainerUUIDsFromUUID", &GeoGetInstanceContainerUUIDsFromUUIDCmd::creator, &GeoGetInstanceContainerUUIDsFromUUIDCmd::newSyntax},
	{"GeoCreateInstanceOfType", &GeoCreateInstanceOfTypeCmd::creator, &GeoCreateInstanceOfTypeCmd::newSyntax},
	{"GeoSetInstanceMemberValue", &GeoSetInstanceMemberValueCmd::creator, &GeoSetInstanceMemberValueCmd::newSyntax},
	{"GeoGetDataSetVersion", &GeoGetDataSetVersionCmd::creator, &GeoGetDataSetVersionCmd::newSyntax},
	{"GeoCanContainerHoldGeoInstance", &GeoCanContainerHoldGeoInstanceCmd::creator, &GeoCanContainerHoldGeoInstanceCmd::newSyntax},
	{"GeoMoveInstance", &GeoMoveInstanceCmd::creator, &GeoMoveInstanceCmd::newSyntax},
	{"GeoEditInstance", &GeoEditInstanceCmd::creator, &GeoEditInstanceCmd::newSyntax},
};

//-----------------------------------------------------------------------------

MStatus initializePlugin( MObject obj )
{
	MStatus   status;
	MFnPlugin plugin( obj, "", "6.0", "Any");

	HRESULT hr = CoInitialize(NULL);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MS::kFailure;
	}

	INIT_GEODATASET;

	// *****************************************************************************
	// MEL Commands
	// *****************************************************************************
	for(int i=0; i<s_nCmds; i++)
	{
		status = plugin.registerCommand(s_cmds[i].name, s_cmds[i].CreatorPtr, s_cmds[i].SyntaxPtr);
		if(!status)
		{
			char errMsg[512];
			sprintf_s(errMsg, "Failed to register command '%s'", s_cmds[i].name);
			MGlobal::displayError(errMsg);
			return status;
		}
	}

	//Register Scene Callbacks
	rageMayaGeoSceneCallbacks::InitializeGlobalCallbacks();

	// *****************************************************************************
	// Instance Importer
	// *****************************************************************************
	plugin.registerFileTranslator("rageMayaGeoInstanceImporter", "none", rageMayaGeoInstanceImporter::creator);

	// *****************************************************************************
	// Instance Node
	// *****************************************************************************
	status = plugin.registerTransform( "rageMayaGeoInstance", rageMayaGeoInstance::id, rageMayaGeoInstance::creator, rageMayaGeoInstance::initialize, MPxTransformationMatrix::creator, MPxTransformationMatrix::baseTransformationMatrixId );
	if (!status) 
	{
		status.perror("registerNode");
		return status;
	}
 	status = plugin.registerCommand( "rageMayaGeoInstanceCmd", rageMayaGeoInstanceCmd::creator );
	if (!status) 
	{
		status.perror("registerCommand");
		return status;
	}
	rageMayaGeoInstance::InitializeGlobalCallbacks();

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus uninitializePlugin( MObject obj)
{
	MStatus   status;
	MFnPlugin plugin( obj );

	// *****************************************************************************
	// MEL Commands
	// *****************************************************************************
	rageMayaGeoSceneCallbacks::UninitializeGlobalCallbacks();

	for(int i=0; i<s_nCmds; i++)
	{
		status = plugin.deregisterCommand(s_cmds[i].name);
		if(!status)
		{
			status.perror("deregisterCommand");
			return status;
		}
	}

	if(rageMayaGeoSceneCallbacks::m_bPluginNeedsCleanUp)
	{
		SHUTDOWN_GEODATASET;
		rageMayaGeoSceneCallbacks::m_bPluginNeedsCleanUp = false;
		CoUninitialize();
	}

	// *****************************************************************************
	// Instance Node
	// *****************************************************************************
	rageMayaGeoInstance::UninitializeGlobalCallbacks();
	status = plugin.deregisterNode( rageMayaGeoInstance::id );
	if (!status) 
	{
		status.perror("deregisterNode");
		return status;
	}
	status = plugin.deregisterCommand("rageMayaGeoInstanceCmd");
	if(!status)
	{
		status.perror("deregisterCommand");
		return status;
	}

	return status;
}

//-----------------------------------------------------------------------------
