// 
// /rageMayaGeoDataSet.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __RAGEMAYAGEODATASET_H_
#define __RAGEMAYAGEODATASET_H_

#include <comdef.h>
#include <vector>
#include <string>

#import <mscorlib.tlb> rename("ReportEvent","mgdReportEvent")
#import "rageGEODataSet.tlb" named_guids raw_interfaces_only // imports from 'rage/base/tools/rageGeoDataSet'

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include <maya/MGlobal.h>

#pragma warning(pop)

using namespace rageGEODataSet;
using namespace std;

//-----------------------------------------------------------------------------

void DisplayCOMErrorMaya(HRESULT hr);

//-----------------------------------------------------------------------------

template<class _T>
void VectorRelease(vector<_T>& vc)
{
	vector<_T>::iterator it;
	for(it = vc.begin(); it != vc.end(); ++it)
		(*it)->Release();
}

//-----------------------------------------------------------------------------

class rageMayaGeoDataSet
{
public:
	bool LoadDataSet(const char* dataSetRootPath, bool bShowSplash=true);
	void UnLoadDataSet();
	bool SaveDataSet();

	IGeoDataSet* GetCurrentDataSet()
	{
		return m_pGeoDataSet.GetInterfacePtr();
	}
	
public:
	~rageMayaGeoDataSet();

	static void Instantiate()
	{
		sm_Instance = new rageMayaGeoDataSet();
	}
	static void Destroy()
	{
		delete sm_Instance;
		sm_Instance = NULL;
	}
	static rageMayaGeoDataSet* InstancePtr()
	{
		return sm_Instance;
	}
	static rageMayaGeoDataSet& InstanceRef()
	{
		return *sm_Instance;
	}

protected:
	rageMayaGeoDataSet();

	static rageMayaGeoDataSet* sm_Instance;

private:
	bool   CreateDataSet();
	
private:
	IGeoDataSetPtr	m_pGeoDataSet;
};

#define GEODATASET				::rageMayaGeoDataSet::InstanceRef()
#define GEODATASET_PTR			::rageMayaGeoDataSet::InstancePtr()
#define INIT_GEODATASET			::rageMayaGeoDataSet::Instantiate()
#define SHUTDOWN_GEODATASET		::rageMayaGeoDataSet::Destroy()

//-----------------------------------------------------------------------------

#endif //__RAGEMAYAGEODATASET_H_
