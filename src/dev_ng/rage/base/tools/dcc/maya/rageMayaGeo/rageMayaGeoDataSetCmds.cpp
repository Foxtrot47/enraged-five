// 
// /rageMayaGeoDataSetCmds.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include <string>
#include <map>

#include "rageMayaGeoDataSet.h"
#include "rageMayaGeoDataSetCmds.h"
#include "rageGeoDataSetCommands.h"

using namespace std;

//-----------------------------------------------------------------------------

#define CHECK_COM_ERROR_SETRESULT_RETURN(_hr,_res) \
	{	if(FAILED(_hr))							   \
		{										   \
			DisplayCOMErrorMaya(_hr);			   \
			setResult(_res);					   \
			return MS::kSuccess;				   \
		}										   \
	}

//-----------------------------------------------------------------------------

enum MayaPrintType
{
	MAYA_INFO,
	MAYA_WARN,
	MAYA_ERROR
};

void MayaPrintf(MayaPrintType type, const char* fmt,...)
{
	static char buf[1024];
	
	va_list args;
	va_start(args,fmt);
	vsprintf_s(buf,fmt,args);
	switch(type)
	{
	case MAYA_INFO: 
		MGlobal::displayInfo(buf);
		break;
	case MAYA_WARN:
		MGlobal::displayWarning(buf);
		break;
	case MAYA_ERROR:
		MGlobal::displayError(buf);
		break;
	}
	va_end(args);
}

//-----------------------------------------------------------------------------
//---GeoGetDataSetVersionCmd---------------------------------------------------

MStatus GeoGetDataSetVersionCmd::doIt(const MArgList &args)
{
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);

	setResult(rageGeoDataSetCommands::GetDataSetVersion());
	return MS::kSuccess;
}

MSyntax GeoGetDataSetVersionCmd::newSyntax()
{
	MSyntax syntax;
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoLoadDataSetCmd---------------------------------------------------------

MStatus GeoLoadDataSetCmd::doIt(const MArgList &args)
{
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);

	MString geoRootPath;
	status = argData.getCommandArgument(0, geoRootPath);

	bool bShowSplash = true;
	if(argData.isFlagSet("nsp"))
		bShowSplash = false;

	if(GEODATASET.LoadDataSet(geoRootPath.asChar(), bShowSplash))
		setResult((int)0);
	else
		setResult((int)1);	


	MayaPrintf(MAYA_INFO, "Loaded GEO Data Set from : '%s'", geoRootPath.asChar());

	return MS::kSuccess;
}

MSyntax GeoLoadDataSetCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag("nsp", "nosplash");
	syntax.addArg(MSyntax::kString);
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoUnLoadDataSetCmd-------------------------------------------------------

MStatus GeoUnLoadDataSetCmd::doIt( const MArgList &args)
{
	MArgDatabase argData(syntax(), args);
	GEODATASET.UnLoadDataSet();
	return MS::kSuccess;
}

MSyntax GeoUnLoadDataSetCmd::newSyntax()
{
	MSyntax syntax;
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoSaveDataSetCmd---------------------------------------------------------

MStatus GeoSaveDataSetCmd::doIt( const MArgList &args)
{
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);

	if(GEODATASET.SaveDataSet())
		return MS::kSuccess;
	else
		return MS::kFailure;
}

MSyntax GeoSaveDataSetCmd::newSyntax()
{
	MSyntax syntax;
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoGetTypeMembersOfType---------------------------------------------------

MStatus GeoGetTypeMembersOfTypeCmd::doIt( const MArgList &args)
{
	static pair<string, string> melToIStringPairs[] =
	{
		pair<string, string>("float", "IGeoTypeMemberFloat"),
		pair<string, string>("int", "IGeoTypeMemberInt"),
		pair<string, string>("bool", "IGeoTypeMemberBool"),
		pair<string, string>("string", "IGeoTypeMemberString"),
		pair<string, string>("matrix34", "IGeoTypeMemberMatrix34"),
		pair<string, string>("matrix44", "IGeoTypeMemberMatrix44"),
		pair<string, string>("vector3", "Vector3")
	};
	static map<string, string> melToIStringTable(melToIStringPairs, melToIStringPairs + sizeof(melToIStringPairs) / sizeof(melToIStringPairs[0]));

	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);
	MStringArray resultArr;

	MString typeName;
	status = argData.getCommandArgument(0, typeName);
	MString memberType;
	status = argData.getCommandArgument(1, memberType); 

	bool bInstanceOnly = false;
	if(argData.isFlagSet("ins", &status))
		bInstanceOnly = true;

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetTypeMembersOfTypeCmd, No GeoDataSet has been loaded.");
		setResult(resultArr);
		return MS::kSuccess;
	}

	IGeoDataSetHelperPtr pGeoHelper;
	hr = pGeoHelper.CreateInstance(CLSID_GeoDataSetHelper);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultArr);
	

	_bstr_t interfaceName;
	map<string, string>::iterator it = melToIStringTable.find(memberType.asChar());
	if(it != melToIStringTable.end())
		interfaceName = it->second.c_str();
	else
	{
		MayaPrintf(MAYA_ERROR,"GeoGetTypeMembersOfType, supplied member type of '%s' is not a recognized type", memberType.asChar());
		setResult(resultArr);
		return MS::kSuccess;
	}	

	SAFEARRAY *pTypeMembersSA = NULL;
	hr = pGeoHelper->GetGeoTypeMembersOfType(pDataSet, _bstr_t(typeName.asChar()), interfaceName, bInstanceOnly, &pTypeMembersSA);
	if(FAILED(hr))
		DisplayCOMErrorMaya(hr);
	
	if(pTypeMembersSA)
	{
		for(long i=0; i < (long)pTypeMembersSA->rgsabound[0].cElements; i++)
		{
			IGeoTypeMemberBase* pMember = NULL;
			hr = SafeArrayGetElement(pTypeMembersSA, &i, &pMember);
			BSTR name;
			pMember->get_Name(&name);
			resultArr.append(MString((const char*)_bstr_t(name)));
			pMember->Release();
		}
	}

	setResult(resultArr);
	return MS::kSuccess;
}

MSyntax GeoGetTypeMembersOfTypeCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag("ins", "instance");
	syntax.addArg(MSyntax::kString);
	syntax.addArg(MSyntax::kString);
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoGetNameFromUUID--------------------------------------------------------

MStatus GeoGetNameFromUUIDCmd::doIt( const MArgList &args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);
	MString resultString;

	MString uuid;
	argData.getCommandArgument(0, uuid);

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetNameFromUUID, No GeoDataSet has been loaded.");
		setResult(resultString);
		return MS::kSuccess;
	}

	IGeoTypePtr pGeoType;
	hr = pDataSet->GetInstanceByUUID(_bstr_t(uuid.asChar()), &pGeoType);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultString);

	if(pGeoType.GetInterfacePtr())
	{
		_bstr_t name;
		pGeoType->get_Name(&name.GetBSTR());
		MString resultName((const char*)name);
		setResult(resultName);
	}
	
	setResult(resultString);
	return MS::kSuccess;
}

MSyntax GeoGetNameFromUUIDCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addArg(MSyntax::kString);
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoGetNameFromUUID--------------------------------------------------------

MStatus GeoGetUUIDFromNameCmd::doIt( const MArgList &args)
{
	MArgDatabase argData(syntax(), args);
	MString typeName;
	argData.getCommandArgument(0, typeName);
	
	setResult(rageGeoDataSetCommands::GetUUIDFromName(typeName));
	return MS::kSuccess;
}

MSyntax GeoGetUUIDFromNameCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addArg(MSyntax::kString);
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoGetBaseTypeName--------------------------------------------------------

MStatus GeoGetBaseTypeNameCmd::doIt( const MArgList &args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);
	MString resultStr;

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetBaseTypeName, No GeoDataSet has been loaded.");
		setResult(resultStr);
		return MS::kSuccess;
	}

	IGeoTypePtr pType;
	if(argData.isFlagSet("tnm"))
	{
		MString typeName;
		argData.getFlagArgument("tnm", 0, typeName);
		hr = pDataSet->GetTopLevelType(_bstr_t(typeName.asChar()), &pType);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultStr);
		
		if(!pType.GetInterfacePtr())
		{
			MayaPrintf(MAYA_ERROR, "GeoGetBaseTypeName, Failed to locate type of name '%s'", typeName.asChar());
			setResult(resultStr);
			return MS::kSuccess;
		}
	}
	else if(argData.isFlagSet("tid"))
	{
		MString typeUUID;
		argData.getFlagArgument("tid", 0, typeUUID);
		hr = pDataSet->GetInstanceByUUID(_bstr_t(typeUUID.asChar()), &pType);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultStr);
		
		if(!pType.GetInterfacePtr())
		{
			MayaPrintf(MAYA_ERROR, "GeoGetBaseTypeName, Failed to locate type with UUID '%s'", typeUUID.asChar());
			setResult(resultStr);
			return MS::kSuccess;
		}
	}
	else
	{
		MGlobal::displayError("GeoGetBaseTypeName, must specify either -tnm or -tid flags.");
		setResult(resultStr);
		return MS::kSuccess;
	}

	IGeoTypePtr pBaseType;
	hr = pType->get_BaseType(&pBaseType);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultStr);
	
	if(!pBaseType.GetInterfacePtr())
	{
		setResult(resultStr);
		return MS::kSuccess;
	}

	_bstr_t bstrBaseTypeName;
	pBaseType->get_Name(&bstrBaseTypeName.GetBSTR());

	resultStr = (const char*)bstrBaseTypeName;
	setResult(resultStr);
	return MS::kSuccess;
}

MSyntax GeoGetBaseTypeNameCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag("tnm", "typename", MSyntax::kString);
	syntax.addFlag("tid", "typeid", MSyntax::kString);
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoGetTemplateName--------------------------------------------------------

MStatus GeoGetTemplateNameCmd::doIt( const MArgList &args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);
	MString resultStr;

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetTemplateName, No GeoDataSet has been loaded.");
		setResult(resultStr);
		return MS::kSuccess;
	}

	IGeoTypePtr pType;
	if(argData.isFlagSet("tnm"))
	{
		MString typeName;
		argData.getFlagArgument("tnm", 0, typeName);
		hr = pDataSet->GetTopLevelType(_bstr_t(typeName.asChar()), &pType);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultStr);
		if(!pType.GetInterfacePtr())
		{
			MayaPrintf(MAYA_ERROR, "GeoGetTemplateName, Failed to locate type of name '%s'", typeName.asChar());
			setResult(resultStr);
			return MS::kSuccess;
		}
	}
	else if(argData.isFlagSet("tid"))
	{
		MString typeUUID;
		argData.getFlagArgument("tid", 0, typeUUID);
		hr = pDataSet->GetInstanceByUUID(_bstr_t(typeUUID.asChar()), &pType);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultStr);
		if(!pType.GetInterfacePtr())
		{
			MayaPrintf(MAYA_ERROR, "GeoGetTemplateName, Failed to locate type with UUID '%s'", typeUUID.asChar());
			setResult(resultStr);
			return MS::kSuccess;
		}
	}
	else
	{
		MGlobal::displayError("GeoGetTemplateName, must specify either -tnm or -tid flags.");
		setResult(resultStr);
		return MS::kSuccess;
	}

	IGeoTemplatePtr pTemplate;
	hr = pType->get_Template(&pTemplate);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultStr);

	if(!pTemplate.GetInterfacePtr())
	{
		MGlobal::displayError("GeoGetTemplateName, failed to retrieve template.");
		setResult(resultStr);
		return MS::kSuccess;
	}

	_bstr_t bstrTemplateName;
	pTemplate->get_Name(&bstrTemplateName.GetBSTR());
	resultStr = (const char*)bstrTemplateName;
	setResult(resultStr);
	return MS::kSuccess;
}

MSyntax GeoGetTemplateNameCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag("tnm", "typename", MSyntax::kString);
	syntax.addFlag("tid", "typeid", MSyntax::kString);
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoMarkAllInstancesCmd----------------------------------------------------

MStatus GeoMarkAllInstancesCmd::doIt( const MArgList &args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);
	MString resultStr;

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoMarkAllInstances, No GeoDataSet has been loaded.");
		setResult(1);
		return MS::kSuccess;
	}

	if(argData.numberOfFlagsUsed() == 0)
		return MS::kInvalidParameter;

	if(argData.isFlagSet("cln"))
	{
		hr = pDataSet->MarkAllInstancesAsClean();
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, 1);
	}
	
	if(argData.isFlagSet("prs"))
	{
		hr = pDataSet->MarkAllInstancesAsPersistant();
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, 1);
	}
	
	setResult(0);
	return MS::kSuccess;
}

MSyntax GeoMarkAllInstancesCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag("cln", "clean");
	syntax.addFlag("prs", "persistant");
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoDeleteInstanceCmd------------------------------------------------------

MStatus GeoDeleteInstanceCmd::doIt( const MArgList &args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);
	MString resultStr;

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoDeleteInstance, No GeoDataSet has been loaded.");
		setResult(1);
		return MS::kSuccess;
	}

	IGeoTypePtr pType;
	if(argData.isFlagSet("tnm"))
	{
		MString typeName;
		argData.getFlagArgument("tnm", 0, typeName);
		hr = pDataSet->GetTopLevelType(_bstr_t(typeName.asChar()), &pType);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, 1);
		if(!pType.GetInterfacePtr())
		{
			MayaPrintf(MAYA_ERROR, "GeoDeleteInstance, Failed to locate instance with name '%s'", typeName.asChar());
			setResult(1);
			return MS::kSuccess;
		}

		_bstr_t typeUUID;
		hr = pType->get_UUID(&typeUUID.GetBSTR());
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, 1);

		if(typeUUID.length() == 0)
		{
			MayaPrintf(MAYA_ERROR, "GeoDeleteInstance, Supplied type '%s', does not have a valid UUID assigned to it.", typeName.asChar());
			setResult(1);
			return MS::kSuccess;
		}
		else
		{
			VARIANT_BOOL bDelRes;
			hr = pDataSet->DeleteInstanceByUUID(typeUUID, &bDelRes);
			CHECK_COM_ERROR_SETRESULT_RETURN(hr, 1);
			if(bDelRes == VARIANT_FALSE)
			{
				MayaPrintf(MAYA_ERROR, "GeoDeleteInstance, Deletion of instance '%s' failed.", typeName.asChar());
				setResult(1);
				return MS::kSuccess;
			}
		}
	}
	else if(argData.isFlagSet("tid"))
	{
		MString typeId;
		argData.getFlagArgument("tid", 0, typeId);
		VARIANT_BOOL bDelRes;

		hr = pDataSet->DeleteInstanceByUUID(_bstr_t(typeId.asChar()), &bDelRes);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, 1);
		if(bDelRes == VARIANT_FALSE)
		{
			MayaPrintf(MAYA_ERROR, "GeoDeleteInstance, Deletion of instance '%s' failed.", typeId.asChar());
			setResult(1);
			return MS::kSuccess;
		}
	}

	setResult(0);
	return MS::kSuccess;
}

MSyntax GeoDeleteInstanceCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag("tnm", "typename", MSyntax::kString);
	syntax.addFlag("tid", "typeid", MSyntax::kString);
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoGetAvailableTypes------------------------------------------------------

MStatus GeoGetAvailableTypesCmd::doIt( const MArgList &args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);
	MStringArray resultArr;

	if(argData.isFlagSet("asc"))
		sortType = rageGEODataSet::GeoSortType_Ascending;
	else if(argData.isFlagSet("dec"))
		sortType = rageGEODataSet::GeoSortType_Descending;
	else
		sortType = rageGEODataSet::GeoSortType_None;

	bool bIncludeInst = false;
	if(argData.isFlagSet("ins"))
		bIncludeInst = true;

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetAvailableTypes, No GeoDataSet has been loaded.");
		setResult(resultArr);
		return MS::kSuccess;
	}

	SAFEARRAY *pTypesSA = NULL;
	hr = pDataSet->GetAllTopLevelTypes(sortType, &pTypesSA);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultArr);
	if(!pTypesSA)
	{	
		setResult(resultArr);
		return MS::kSuccess;
	}

	for(long i=0; i < (long)pTypesSA->rgsabound[0].cElements; i++)
	{
		IGeoType *pType;
		SafeArrayGetElement(pTypesSA, &i, &pType);
		_bstr_t typeName;
		pType->get_Name(typeName.GetAddress());
		resultArr.append(MString((const char*)typeName));
		pType->Release();
	}
		
	setResult(resultArr);
	return MS::kSuccess;
}

MSyntax GeoGetAvailableTypesCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag("asc", "ascending");
	syntax.addFlag("dec", "decending");
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoGetInstanceContainerNamesFromUUID--------------------------------------

MStatus GeoGetInstanceContainerNamesFromUUIDCmd::doIt( const MArgList &args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);
	MStringArray resultArr;

	MString uuid;
	argData.getCommandArgument(0, uuid);

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetInstanceContainerNamesFromUUID, No GeoDataSet has been loaded.");
		setResult(resultArr);
		return MS::kSuccess;
	}

	IGeoDataSetHelperPtr pGeoHelper;
	hr = pGeoHelper.CreateInstance(CLSID_GeoDataSetHelper);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultArr);


	SAFEARRAY *pContainerNamesSA = NULL;
	hr = pGeoHelper->GetContainerNamesFromInstanceUUID(pDataSet, _bstr_t(uuid.asChar()), &pContainerNamesSA);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultArr);
	if( !pContainerNamesSA )
	{
		setResult(resultArr);
		return MS::kSuccess;
	}

	for(long i=0; i < (long)pContainerNamesSA->rgsabound[0].cElements; i++)
	{
		_bstr_t	bstrContainerName;
		SafeArrayGetElement(pContainerNamesSA, &i, &bstrContainerName.GetBSTR());
		resultArr.append(MString((const char*)bstrContainerName));
	}

	setResult(resultArr);
	return MS::kSuccess;
}

MSyntax GeoGetInstanceContainerNamesFromUUIDCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addArg(MSyntax::kString);
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoGetInstanceContainerUUIDsFromUUID--------------------------------------

MStatus GeoGetInstanceContainerUUIDsFromUUIDCmd::doIt( const MArgList &args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);
	MStringArray resultArr;

	MString uuid;
	argData.getCommandArgument(0, uuid);

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetInstanceContainerUUIDsFromUUID, No GeoDataSet has been loaded.");
		setResult(resultArr);
		return MS::kSuccess;
	}

	IGeoDataSetHelperPtr pGeoHelper;
	hr = pGeoHelper.CreateInstance(CLSID_GeoDataSetHelper);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultArr);

	SAFEARRAY *pContainerUUIDsSA = NULL;
	hr = pGeoHelper->GetContainerUUIDsFromInstanceUUID(pDataSet, _bstr_t(uuid.asChar()), &pContainerUUIDsSA);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, resultArr);
	
	if( !pContainerUUIDsSA )
	{
		setResult(resultArr);
		return MS::kSuccess;
	}

	for(long i=0; i < (long)pContainerUUIDsSA->rgsabound[0].cElements; i++)
	{
		_bstr_t	bstrUUID;
		SafeArrayGetElement(pContainerUUIDsSA, &i, &bstrUUID.GetBSTR());
		resultArr.append(MString((const char*)bstrUUID));
	}

	setResult(resultArr);
	return MS::kSuccess;
}

MSyntax GeoGetInstanceContainerUUIDsFromUUIDCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addArg(MSyntax::kString);
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoCreateInstanceOfTypeCmd------------------------------------------------

MStatus GeoCreateInstanceOfTypeCmd::doIt(const MArgList &args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);
	
	MString typeName;
	argData.getCommandArgument(0, typeName);

	MStringArray retArray;
	
	bool bSetName = false;
	MString msName;

	if(argData.isFlagSet("nm"))
	{
		bSetName = true;
		argData.getFlagArgument("nm", 0, msName);
	}

	//Get the data set instance
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoCreateInstanceOfType, No GeoDataSet has been loaded.");
		setResult(retArray);
		return MS::kSuccess;
	}

	//Get the base type
	IGeoTypePtr pBaseType;
	hr = pDataSet->GetTopLevelType(_bstr_t(typeName.asChar()), &pBaseType);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, retArray);
	
	if(!pBaseType.GetInterfacePtr())
	{
		MayaPrintf(MAYA_ERROR, "GeoCreateInstanceOfType, Unable to find base type '%s'", typeName.asChar());
		setResult(retArray);
		return MS::kSuccess;
	}

	//Create the instance
	IGeoTypePtr pInstanceType = NULL;
	if(bSetName)
	{
		hr = pDataSet->CreateChildTypeFromTypeWithName(pBaseType, true, _bstr_t(msName.asChar()), &pInstanceType);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, retArray);
	}
	else
	{
		hr = pDataSet->CreateChildTypeFromType(pBaseType.GetInterfacePtr(), true, &pInstanceType);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, retArray);
	}

	if(!pInstanceType.GetInterfacePtr())
	{
		MayaPrintf(MAYA_ERROR, "GeoCreateInstanceOfType, failed to create an instance of type '%s'.", typeName.asChar());
		setResult(retArray);
		return MS::kSuccess;
	}

	MString retInstanceName;
	_bstr_t bstrInstanceName;
	pInstanceType->get_Name(bstrInstanceName.GetAddress());
	retInstanceName = (const char*)bstrInstanceName;

	MString retInstanceUUID;
	_bstr_t bstrInstanceUUID;
	pInstanceType->get_UUID(bstrInstanceUUID.GetAddress());
	retInstanceUUID = (const char*)bstrInstanceUUID;

	retArray.append(retInstanceName);
	retArray.append(retInstanceUUID);
	setResult(retArray);

	return MS::kSuccess;
}

MSyntax GeoCreateInstanceOfTypeCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addFlag("nm", "name", MSyntax::kString);
	syntax.addArg(MSyntax::kString);
	return syntax;
}

//-----------------------------------------------------------------------------
//----GeoSetInstanceMemberValueCmd---------------------------------------------

MStatus GeoSetInstanceMemberValueCmd::doIt(const MArgList &args)
{
	static pair<string, SetMemberPtr> melToSetMethodPairs[] =
	{
		pair<string, SetMemberPtr>("float",&GeoSetInstanceMemberValueCmd::SetMemberFloatValue),
		pair<string, SetMemberPtr>("int",&GeoSetInstanceMemberValueCmd::SetMemberIntValue),
		pair<string, SetMemberPtr>("bool",&GeoSetInstanceMemberValueCmd::SetMemberBoolValue),
		pair<string, SetMemberPtr>("string",&GeoSetInstanceMemberValueCmd::SetMemberStringValue),
		pair<string, SetMemberPtr>("matrix34",&GeoSetInstanceMemberValueCmd::SetMemberMatrix34Value),
		pair<string, SetMemberPtr>("matrix44",&GeoSetInstanceMemberValueCmd::SetMemberMatrix44Value),
		pair<string, SetMemberPtr>("vector3",&GeoSetInstanceMemberValueCmd::SetMemberVector3Value)
	};
	static map<string, SetMemberPtr> melToSetMethodTable(melToSetMethodPairs, melToSetMethodPairs + sizeof(melToSetMethodPairs) / sizeof(melToSetMethodPairs[0]));

	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);

	MString memberName;
	argData.getCommandArgument(0, memberName);

	MString memberType;
	argData.getCommandArgument(1, memberType);

	MString memberValue;
	argData.getCommandArgument(2, memberValue);

	//Get the data set instance
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoSetInstanceMemberValue, No GeoDataSet has been loaded.");
		setResult(1);
		return MS::kSuccess;
	}

	IGeoTypePtr pType;
	if(argData.isFlagSet("inm"))
	{
		MString typeName;
		argData.getFlagArgument("inm", 0, typeName);
		hr = pDataSet->GetTopLevelType(_bstr_t(typeName.asChar()), &pType);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, (int)1);
		if(!pType.GetInterfacePtr())
		{
			MayaPrintf(MAYA_ERROR, "GeoSetInstanceMemberValue, failed to locate GEO instance with the name '%s'", typeName.asChar());
			setResult(1);
			return MS::kSuccess;
		}
	}
	else if(argData.isFlagSet("iid"))
	{
		MString typeUUID;
		argData.getFlagArgument("iid", 0, typeUUID);
		hr = pDataSet->GetInstanceByUUID(_bstr_t(typeUUID.asChar()), &pType);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, (int)1);
		
		if(!pType.GetInterfacePtr())
		{
			MayaPrintf(MAYA_ERROR, "GeoSetInstanceMemberValue, failed to locate GEO instance with the UUID '%s'", typeUUID.asChar());
			setResult(1);
			return MS::kSuccess;
		}
	}
	else
	{
		MGlobal::displayError("GeoSetInstanceMemberValue, must supply either the -inm or -iid flags");
		return MS::kFailure;
	}

	map<string, SetMemberPtr>::iterator it = melToSetMethodTable.find(memberType.asChar());
	if(it != melToSetMethodTable.end())
	{
		//Get the base member interface
		IGeoTypeMemberBasePtr pMbrBase;
		hr = pType->GetMemberByName(_bstr_t(memberName.asChar()), &pMbrBase);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, (int)1);

		//Only set values on members with an instance level instantation
		IGeoTemplateMemberBasePtr pTmplMbrBase;
		hr = pMbrBase->get_TemplateMemberBase(&pTmplMbrBase);
		CHECK_COM_ERROR_SETRESULT_RETURN(hr, (int)1);

		rageGEODataSet::InstantiationType instType;
		hr = pTmplMbrBase->get_Instantiation(&instType);
		if(instType != rageGEODataSet::InstantiationType_Instance)
		{
			if(instType == rageGEODataSet::InstantiationType_Both)
				MayaPrintf(MAYA_ERROR, "Cannot set member value '%s', its instantiation type is set to 'Both'", memberName.asChar());
			else
				MayaPrintf(MAYA_ERROR, "Cannot set member value '%s', its instantiation type is set to 'Type'", memberName.asChar());
			setResult(1);
			return MS::kSuccess;
		}

		//Set the member value
		status = it->second(pMbrBase, memberValue);
		if(status == MS::kSuccess)
		{
			pType->put_Dirty(true);
			pMbrBase->put_UseInheritedValue(false);
		}
	}
	else
	{
		MayaPrintf(MAYA_ERROR, "GeoSetInstanceMemberValue, got unknown member type '%s'.", memberType.asChar());
		status = MS::kFailure;
	}

	if(status == MS::kSuccess)
		setResult(0);
	else
		setResult(1);

	return MS::kSuccess;
};

MSyntax GeoSetInstanceMemberValueCmd::newSyntax()
{
	MSyntax syntax;
	
	syntax.addFlag("inm", "instname", MSyntax::kString);
	syntax.addFlag("iid", "instid", MSyntax::kString);

	syntax.addArg(MSyntax::kString);	//Member Name
	syntax.addArg(MSyntax::kString);	//Member Type
	syntax.addArg(MSyntax::kString);	//Member Value
	return syntax;
}

MStatus GeoSetInstanceMemberValueCmd::SetMemberMatrix34Value(IGeoTypeMemberBasePtr pMbrBase, MString memberValue)
{
	HRESULT hr;

	//Make sure the string supplied for the matrix has the correct numer of values in it
	unsigned int count=0;
	unsigned int valLen = memberValue.length();
	const char*  valStr = memberValue.asChar();
	for(unsigned int i=0; i<valLen; i++)
	{
		if(valStr[i] == ' ')
			count++;
	}
	if(count != 11)
	{
		MGlobal::displayError("Supplied Matrix34 value doesn contain the correct number of values");
		return MS::kFailure;
	}

	IGeoTypeMemberMatrix34Ptr pMbrMat34;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberMatrix34, &pMbrMat34);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MS::kFailure;
	}
	
	rageGEODataSet::Matrix34 geoMatrix;

	sscanf_s(memberValue.asChar(), "%f %f %f %f %f %f %f %f %f %f %f %f",
		&geoMatrix.a.x, &geoMatrix.a.y, &geoMatrix.a.z,
		&geoMatrix.b.x, &geoMatrix.b.y, &geoMatrix.b.z,
		&geoMatrix.c.x, &geoMatrix.c.y, &geoMatrix.c.z,
		&geoMatrix.d.x, &geoMatrix.d.y, &geoMatrix.d.z);

	pMbrMat34->put_Value(geoMatrix);
	
	return MS::kSuccess;
}

MStatus GeoSetInstanceMemberValueCmd::SetMemberMatrix44Value(IGeoTypeMemberBasePtr pMbrBase, MString memberValue)
{
	HRESULT hr;

	//Make sure the string supplied for the matrix has the correct numer of values in it
	unsigned int count=0;
	unsigned int valLen = memberValue.length();
	const char*  valStr = memberValue.asChar();
	for(unsigned int i=0; i<valLen; i++)
	{
		if(valStr[i] == ' ')
			count++;
	}
	if(count != 15)
	{
		MGlobal::displayError("Supplied Matrix44 value doesn contain the correct number of values");
		return MS::kFailure;
	}
		
	IGeoTypeMemberMatrix44Ptr pMbrMat44;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberMatrix44, &pMbrMat44);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MS::kFailure;
	}
	
	rageGEODataSet::Matrix44 geoMatrix;

	sscanf_s(memberValue.asChar(), "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
		&geoMatrix.a.x, &geoMatrix.a.y, &geoMatrix.a.z, &geoMatrix.a.w,
		&geoMatrix.b.x, &geoMatrix.b.y, &geoMatrix.b.z, &geoMatrix.b.w,
		&geoMatrix.c.x, &geoMatrix.c.y, &geoMatrix.c.z, &geoMatrix.c.w,
		&geoMatrix.d.x, &geoMatrix.d.y, &geoMatrix.d.z, &geoMatrix.d.w);

	pMbrMat44->put_Value(geoMatrix);
	
	return MS::kSuccess;
}

MStatus	GeoSetInstanceMemberValueCmd::SetMemberBoolValue(IGeoTypeMemberBasePtr pMbrBase, MString memberValue)
{
	HRESULT hr;

	IGeoTypeMemberBoolPtr pMbrBool;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberBool, &pMbrBool);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MS::kFailure;
	}

	bool bValue = false;
	if( (_strcmpi(memberValue.asChar(), "1")==0) ||
		(_strcmpi(memberValue.asChar(), "true")==0) )
		bValue = true;

	pMbrBool->put_Value(bValue);
	
	return MS::kSuccess;
}

MStatus GeoSetInstanceMemberValueCmd::SetMemberFloatValue(IGeoTypeMemberBasePtr pMbrBase, MString memberValue)
{
	HRESULT hr;

	IGeoTypeMemberFloatPtr pMbrFloat;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberFloat, &pMbrFloat);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MS::kFailure;
	}

	double fValue = atof(memberValue.asChar());

	pMbrFloat->put_Value(fValue);
	
	return MS::kSuccess;
}

MStatus GeoSetInstanceMemberValueCmd::SetMemberIntValue(IGeoTypeMemberBasePtr pMbrBase, MString memberValue)
{
	HRESULT hr;

	IGeoTypeMemberIntPtr pMbrInt;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberInt, &pMbrInt);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MS::kFailure;
	}

	int iValue = atoi(memberValue.asChar());

	pMbrInt->put_Value(iValue);
	
	return MS::kSuccess;
}

MStatus GeoSetInstanceMemberValueCmd::SetMemberStringValue(IGeoTypeMemberBasePtr pMbrBase, MString memberValue)
{
	HRESULT hr;

	IGeoTypeMemberStringPtr pMbrString;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberString, &pMbrString);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MS::kFailure;
	}

	_bstr_t strValue(memberValue.asChar());
	pMbrString->put_Value(strValue);
	
	return MS::kSuccess;
}

MStatus GeoSetInstanceMemberValueCmd::SetMemberVector3Value(IGeoTypeMemberBasePtr pMbrBase, MString memberValue)
{
	HRESULT hr;

	IGeoTypeMemberVector3Ptr pMbrVec3;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberVector3, &pMbrVec3);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MS::kFailure;
	}

	rageGEODataSet::Vector3 geoVec3;
	sscanf_s(memberValue.asChar(), "%f %f %f",
		&geoVec3.x, &geoVec3.y, &geoVec3.z);

	pMbrVec3->put_Value(geoVec3);
	
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------
//---GeoCanContainerHoldGeoInstanceCmd-----------------------------------------

MStatus GeoCanContainerHoldGeoInstanceCmd::doIt(const MArgList& args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);

	MString rootInstanceUUID;
	argData.getCommandArgument(0, rootInstanceUUID);
	MString containerUUID;
	argData.getCommandArgument(1, containerUUID);
	MString instanceItemUUID;
	argData.getCommandArgument(2, instanceItemUUID);

	//Get the data set instance
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoCanContainerHoldGeoInstance, No GeoDataSet has been loaded.");
		setResult(false);
		return MS::kSuccess;
	}

	IGeoDataSetHelperPtr pGeoHelper;
	hr = pGeoHelper.CreateInstance(CLSID_GeoDataSetHelper);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, false);

	VARIANT_BOOL bCanHold;
	hr = pGeoHelper->CanContainerHoldGeoInstance(pDataSet,
		_bstr_t(instanceItemUUID.asChar()), 
		_bstr_t(rootInstanceUUID.asChar()),
		_bstr_t(containerUUID.asChar()),
		&bCanHold);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, false);

	if(bCanHold == VARIANT_FALSE)
		setResult(false);
	else
		setResult(true);

	return MS::kSuccess;

};

MSyntax GeoCanContainerHoldGeoInstanceCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addArg(MSyntax::kString);	//Root Instance UUID
	syntax.addArg(MSyntax::kString);	//Container UUID
	syntax.addArg(MSyntax::kString);	//Instance Item UUID
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoMoveInstanceCmd--------------------------------------------------------

MStatus GeoMoveInstanceCmd::doIt(const MArgList& args)
{
	HRESULT hr;
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);

	MString parentInstanceUUID;
	argData.getCommandArgument(0, parentInstanceUUID);
	MString containerUUID;
	argData.getCommandArgument(1, containerUUID);
	MString instanceItemUUID;
	argData.getCommandArgument(2, instanceItemUUID);

	//Get the data set instance
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoMoveInstance, No GeoDataSet has been loaded.");
		setResult(1);
		return MS::kSuccess;
	}

	IGeoDataSetHelperPtr pGeoHelper;
	hr = pGeoHelper.CreateInstance(CLSID_GeoDataSetHelper);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, 1);

	VARIANT_BOOL bMoveResult;
	hr = pGeoHelper->MoveGeoInstance(pDataSet,
		_bstr_t(instanceItemUUID.asChar()),
		_bstr_t(parentInstanceUUID.asChar()),
		_bstr_t(containerUUID.asChar()),
		&bMoveResult);
	CHECK_COM_ERROR_SETRESULT_RETURN(hr, 1);

	if(bMoveResult == VARIANT_FALSE)
	{
		MayaPrintf(MAYA_ERROR,"GeoMoveInstance, instance '%s', could not be added to the container '%s' of '%s'",
			instanceItemUUID.asChar(), containerUUID.asChar(), parentInstanceUUID.asChar());
		setResult(1);
		return MS::kSuccess;
	}

	setResult(0);
	return MS::kSuccess;
}

MSyntax GeoMoveInstanceCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addArg(MSyntax::kString);	//Parent Instance UUID
	syntax.addArg(MSyntax::kString);	//Container UUID
	syntax.addArg(MSyntax::kString);	//Instance Item UUID
	return syntax;
}

//-----------------------------------------------------------------------------
//---GeoEditInstanceCmd--------------------------------------------------------

MStatus GeoEditInstanceCmd::doIt(const MArgList& args)
{
	MStatus status(MS::kSuccess);
	MArgDatabase argData(syntax(), args);

	MString strInstanceUUID;
	argData.getCommandArgument(0, strInstanceUUID);

	// Edit it
	cout<<__FILE__<<":"<<__LINE__<<" : GeoEditInstanceCmd::doIt"<<endl;
	rageGeoDataSetCommands::EditGeoInstance(strInstanceUUID);
	cout<<__FILE__<<":"<<__LINE__<<" : GeoEditInstanceCmd::doIt"<<endl;

	setResult(0);
	return MS::kSuccess;
}

MSyntax GeoEditInstanceCmd::newSyntax()
{
	MSyntax syntax;
	syntax.addArg(MSyntax::kString);	//Instance UUID
	return syntax;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
