// 
// /rageMayaGeoDataSet.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#include "rageMayaGeoDataSet.h"

//-----------------------------------------------------------------------------

rageMayaGeoDataSet* rageMayaGeoDataSet::sm_Instance = NULL;

//-----------------------------------------------------------------------------

void DisplayCOMErrorMaya(HRESULT hr)
{
	LPTSTR lpMsgBuf;
	FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			hr,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
	);
	
	_bstr_t strMsg(lpMsgBuf);
	MGlobal::displayError((const char*)strMsg);
	LocalFree( lpMsgBuf );
}

//-----------------------------------------------------------------------------

rageMayaGeoDataSet::rageMayaGeoDataSet()
{
	
}

//-----------------------------------------------------------------------------

rageMayaGeoDataSet::~rageMayaGeoDataSet()
{
	if(m_pGeoDataSet.GetInterfacePtr())
		m_pGeoDataSet->Release();
}

//-----------------------------------------------------------------------------

bool rageMayaGeoDataSet::LoadDataSet(const char* dataSetRootPath, bool bShowSplash)
{
	HRESULT hr;

	if(m_pGeoDataSet.GetInterfacePtr())
	{
		m_pGeoDataSet->Release();
		m_pGeoDataSet.Detach();
	}
	
	hr = m_pGeoDataSet.CreateInstance(CLSID_GeoDataSet);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return false;
	}
	else if(!m_pGeoDataSet.GetInterfacePtr())
	{
		MGlobal::displayError("Failed to create instance of GeoDataSet object.");
		return false;
	}
	
	hr = m_pGeoDataSet->LoadFromDirectory(_bstr_t(dataSetRootPath), bShowSplash);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		m_pGeoDataSet->Release();
		return false;
	}
	else
		return true;
}

//-----------------------------------------------------------------------------

void rageMayaGeoDataSet::UnLoadDataSet()
{
	if(m_pGeoDataSet.GetInterfacePtr())
	{
		m_pGeoDataSet.Release();
		m_pGeoDataSet.Detach();
	}
}

//-----------------------------------------------------------------------------

bool rageMayaGeoDataSet::SaveDataSet()
{
	if(!m_pGeoDataSet.GetInterfacePtr())
		return false;
	else
		m_pGeoDataSet->SaveAll();
	return true;
}

//-----------------------------------------------------------------------------


