#include <maya/MMessage.h>
#include <maya/MSceneMessage.h>


class rageMayaGeoSceneCallbacks
{
public:
	// Initialize and uninitialize functions
	static	void		InitializeGlobalCallbacks();
	static	void		UninitializeGlobalCallbacks();

	// The actual callback functions
	static void MayaExitingCallback(void* clientData);
	static bool m_bPluginNeedsCleanUp;
	static void BeforeOpenCallback(void* clientData);
	static void BeforeNewCallback(void* clientData);

private:
	static MCallbackId				m_MayaExitingCallbackId;
	static MCallbackId				m_BeforeOpenCallbackId;
	static MCallbackId				m_BeforeNewCallbackId;
};
