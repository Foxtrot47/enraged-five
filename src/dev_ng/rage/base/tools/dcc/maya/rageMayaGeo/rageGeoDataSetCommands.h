
#include "rageMayaGeoDataSet.h"

#pragma once
#define REQUIRE_IOSTREAM
#pragma warning(disable: 4668)
#include "maya/MString.h"
#include "maya/MGlobal.h"
#include "maya/MDagModifier.h"
#include "maya/MPlug.h"
#include "maya/MFnDagNode.h"
#include "maya/MMatrix.h"
#pragma warning(error: 4668)

class rageGeoDataSetCommands
{
public:
	static MString GetDataSetVersion();

	// File based operations
	static MString GetBaseGeoFolder();

	// Useful converters
	static MString GetUUIDFromName(const MString& strName);
	static MString GetNameFromUUID(const MString& strUUID);

	// Hierarchy based accessors
	static MString GetTypeNameFromInstanceUUID(const MString& strInstanceName);
	static MString GetParentTypeNameFromTypeName(const MString& strTypeName);
	static MString GetTemplateNameFromTypeName(const MString& strTypeName);
	static MString GetParentTemplateNameFromTemplateName(const MString& strGeoTemplateName);

	// Container functions
	static vector<MString>	GetContainerUIDsFromInstanceUID(const MString& strUIDInstance);
	static vector<MString>	GetContainerNamesFromInstanceUID(const MString& strUIDInstance);
	static vector<MString>	GetUIDsOfContainedInstancesFromParentInstanceUID(const MString& strInstanceUID, const MString& strContainerUID);

	// General accessors functions
	static MMatrix GetWorldSpaceMatrix(const MString& strInstanceUUID);
	static void EditGeoInstance(const MString& strInstanceUUID);

private:
	static void DisplayCOMErrorMaya(HRESULT hr);
};
