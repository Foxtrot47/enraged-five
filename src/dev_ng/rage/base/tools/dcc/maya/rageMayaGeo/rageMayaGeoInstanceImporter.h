// 
// /rageMayaGeoInstanceImporter.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef rageMayaGeoInstanceImporter_H
#define rageMayaGeoInstanceImporter_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxFileTranslator.h>
#pragma warning(error: 4668)

///////////////////////////////////////////
// rageMayaGeoInstanceImporter Class //
///////////////////////////////////////////

class rageMayaGeoInstanceImporter : public MPxFileTranslator
{
public:
	rageMayaGeoInstanceImporter();
	virtual         ~rageMayaGeoInstanceImporter();

	static  void *	creator();

	virtual MStatus  reader ( const MFileObject & file, const MString & optionsString, FileAccessMode mode);

	virtual MPxFileTranslator::MFileKind  identifyFile ( const MFileObject & file, const char* buffer, short size) const;

	virtual bool  haveReadMethod () const {return true;}

	virtual bool  haveWriteMethod () const {return false;}

	virtual bool  haveNamespaceSupport () const {return false;}

	virtual bool  haveReferenceMethod () const {return false;}

	virtual MString  defaultExtension () const {return "geo";}

	virtual MString  filter () const {return "*.geo";}

	virtual bool  canBeOpened () const {return true;}

private:
	void	CreateGEOInstanceInMayaFromName(const MString& strGEOInstanceName);

};

#endif /* GEN_RAGE_SHADER_MATERIAL_MAYA65_H */

