#ifndef _rageMayaGeoInstance
#define _rageMayaGeoInstance
//
// Copyright (C) 
// 
// File: rageMayaGeoInstance.h
//
// Dependency Graph Node: rageMayaGeoInstance
//
// Author: Maya Plug-in Wizard 2.0
//

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MMessage.h>
#include <maya/MPxTransform.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MNodeMessage.h>
#include <maya/MFileIO.h>
#pragma warning(pop)

 
class rageMayaGeoInstance : public MPxTransform
{
public:
						rageMayaGeoInstance();
	virtual				~rageMayaGeoInstance(); 
	virtual  void		postConstructor();
	static  void*		creator();
	static  MStatus		initialize();

	// Connection callbacks
	MStatus				connectionMade(const MPlug& obPlug, const MPlug& obOtherPlug, bool bAsSource);
	MStatus				connectionBroken(const MPlug& obPlug, const MPlug& obOtherPlug, bool bAsSource);
	MStatus				legalConnection(const MPlug& obPlug, const MPlug& obOtherPlug, bool bAsSource, bool& bResult ) const;

	// Callbacks
	void				InitializeNodeCallbacks();
	void				UninitializeNodeCallbacks();
	static  void		AttributeChangedCallback(MNodeMessage::AttributeMessage eAttributeMessage, MPlug& obPlug, MPlug& obOtherPlug, void* clientData);

	static	void		InitializeGlobalCallbacks();
	static	void		UninitializeGlobalCallbacks();
	static	void		BeforeMayaSaveCallback(void* pobUserData);
	static	void		BeforeOpenCallback(void* pobUserData);
	static	void		AfterOpenCallback(void* pobUserData);
	static	void		AfterImportCallback(void* pobUserData);
	static	void		BeforeNewCallback(void* pobUserData);
	static	void		AfterNewCallback(void* pobUserData);
	static	void		NodeAddedCallback(MObject& obNode, void* pvClientData);
	static void			NodePreRemovalCallback(MObject& obNode, void* clientData);

	// ActDumb is used to turn off all the clever interfacing with the GEO Dataset, this is required to manipulate the nodes when loading scenes
	bool				ActDumb();
	static	bool		OldSceneIsBeingCleared() {return (m_bBetweenBeforeSceneOpenAndAfterSceneOpen && !MFileIO::isReadingFile());}
	static	bool		NewSceneIsBeingOpened() {return (m_bBetweenBeforeSceneOpenAndAfterSceneOpen && MFileIO::isReadingFile());}

	// Functions
	static	void		SetToNewInstanceOfType(const MObject& obNode, const MString& strGeoType);
	static	void		UpdateContainerParameters(const MObject& obNode);
	static	void		GiveGraphicalRepresentation(const MObject& obNode);
	static	void		HuntColladaFiles(const MString& obFolder, MStringArray& astrColladaFiles);
public:

	// Need a UID attribute
	static  MObject		m_obGeoInstanceUIDAttribute;	
	static  MObject		m_obGeoInstanceTypeAttribute;	
	static  MObject		m_obGeoInstanceNameAttribute;	
	static  MObject		m_obGeoInstanceContainerUIDAttribute;	

	// Callback to assign new UUID whenever a node is created
	static MCallbackId				m_NodeAddedCallbackId;
	static MCallbackId				m_BeforeMayaSaveCallbackId;
	static MCallbackId				m_BeforeOpenCallbackId;
	static MCallbackId				m_BeforeNewCallbackId;
	static MCallbackId				m_AfterNewCallbackId;

	static bool m_bBetweenBeforeSceneOpenAndAfterSceneOpen;

	bool m_bNodeWasCreatedDuringFileOpenSoKillMe;

	// Callbacks registered per node instance
	MCallbackId				m_NodePreRemovalCallbackId;
	MCallbackId				m_AfterOpenCallbackId;
	MCallbackId				m_AfterImportCallbackId;

	// The typeid is a unique 32bit indentifier that describes this node.
	// It is used to save and retrieve nodes of this type from the binary
	// file format.  If it is not unique, it will cause file IO problems.
	//
	static	MTypeId		id;
};

#endif

