#pragma once

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MCommandResult.h>
#include <maya/MDoubleArray.h>
#include <maya/MStringArray.h>
#pragma warning(pop)

class rageMayaGeoInstanceCmd : public MPxCommand
{
public:
	rageMayaGeoInstanceCmd() {};
	virtual ~rageMayaGeoInstanceCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new rageMayaGeoInstanceCmd(); }

	// Actual functions
	static MStatus			NewInstanceOfType(const MString& strDagPathToNode, const MString& strGeoType);
	static MStatus			CreateExistingInstanceFromUID(const MString& strGeoInstanceUID);

private:
	static MObject			CreateExistingInstanceFromUID_Inner(const MString& strGeoInstanceUID);
	static MObject			CreateParentTransformBasedOnGeoInstance(const MString& strGeoInstanceUID);
	static MObject			CreateParentTransformBasedOnGeoType(const MString& strGeoTypeName);
	static MObject			CreateParentTransformBasedOnGeoTemplate(const MString& strGeoTemplateName);
};
