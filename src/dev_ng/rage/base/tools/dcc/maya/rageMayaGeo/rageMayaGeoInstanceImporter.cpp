// 
// /rageMayaGeoInstanceImporter.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#include "rageMayaGeoDataSet.h"
#include "rageMayaGeoDataSetCmds.h"
#include "rageMayaGeoSceneCallbacks.h"
#include "rageMayaGeoInstanceImporter.h"
#include "rageGeoDataSetCommands.h"
#include "rageMayaGeoInstanceCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(disable: 4668)
#include "maya/MString.h"
#include "maya/MGlobal.h"
#include "maya/MDagModifier.h"
#include "maya/MPlug.h"
#include "maya/MFnDagNode.h"
#pragma warning(error: 4668)

#include <iostream>
using namespace std;

rageMayaGeoInstanceImporter::rageMayaGeoInstanceImporter()
{
}

rageMayaGeoInstanceImporter::~rageMayaGeoInstanceImporter()
{
}

void * rageMayaGeoInstanceImporter::creator()
{
	return new rageMayaGeoInstanceImporter();
}

MStatus  rageMayaGeoInstanceImporter::reader(const MFileObject & file, const MString & , FileAccessMode )
{
	// Create the instance within Maya, this is really a dodgy trick, but I don't actually open the file in question.
	// Instead I work out the name of the instance within the file and create it within Maya through the loaded dataset
	// So get instance's name
	MString strFilename = file.name();

	// Chop off extension and that is the Geo Instance's name
	MString strGeoInstanceName = strFilename.substring(0, strFilename.rindex('.') - 1);

	// Loading a file, so tell the system to act dumb
	MGlobal::executeCommand("rageGeoInstance.SetActDumb(true)", true);

	// Create node in Maya
	CreateGEOInstanceInMayaFromName(strGeoInstanceName);

	// Finished loading a file, so stop being dumb
	MGlobal::executeCommand("rageGeoInstance.SetActDumb(false)", true);
	return MStatus::kSuccess;
}

MPxFileTranslator::MFileKind  rageMayaGeoInstanceImporter::identifyFile(const MFileObject & file, const char* , short ) const
{
	// Get file extension
	MString strFilename = file.fullName().toLowerCase();
	MString strExtension = strFilename.substring(strFilename.rindex('.'), strFilename.length());
	if (strExtension == ".geo")
	{
		return MPxFileTranslator::kIsMyFileType;
	}
	else
	{
		return MPxFileTranslator::kNotMyFileType;
	}
}

void	rageMayaGeoInstanceImporter::CreateGEOInstanceInMayaFromName(const MString& strGeoInstanceName)
{
	MString strGEOInstanceUUID = rageGeoDataSetCommands::GetUUIDFromName(strGeoInstanceName);
	rageMayaGeoInstanceCmd::CreateExistingInstanceFromUID(strGEOInstanceUUID);
}
