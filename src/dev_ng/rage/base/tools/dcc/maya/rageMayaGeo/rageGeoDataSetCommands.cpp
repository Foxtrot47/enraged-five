#include "rageGeoDataSetCommands.h"

void rageGeoDataSetCommands::DisplayCOMErrorMaya(HRESULT hr)
{
	LPTSTR lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		hr,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
		(LPTSTR) &lpMsgBuf,
		0,
		NULL
		);

	_bstr_t strMsg(lpMsgBuf);
	MGlobal::displayError((const char*)strMsg);
	LocalFree( lpMsgBuf );
}


MString rageGeoDataSetCommands::GetDataSetVersion()
{
	IGeoDataSetHelperPtr pGeoHelper;
	HRESULT hr = pGeoHelper.CreateInstance(CLSID_GeoDataSetHelper);
	if(FAILED(hr))
	{
		return "";
	}
	else
	{
		_bstr_t bstrVersion;
		pGeoHelper->GetGeoDataSetAssemblyVersion(&bstrVersion.GetBSTR());
		return ((const char*)bstrVersion);
	}
}

MString rageGeoDataSetCommands::GetUUIDFromName(const MString& strName)
{
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return "";
	}

	IGeoTypePtr pGeoType;
	HRESULT hr = pDataSet->GetTopLevelType(_bstr_t(strName.asChar()), &pGeoType);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return "";
	}

	if(pGeoType.GetInterfacePtr())
	{
		_bstr_t uuid;
		pGeoType->get_UUID(&uuid.GetBSTR());

		return ((const char*)uuid);
	}

	return "";
}


MString rageGeoDataSetCommands::GetNameFromUUID(const MString& strUUID)
{
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return "";
	}

	IGeoTypePtr pGeoType;
	HRESULT hr = pDataSet->GetInstanceByUUID(_bstr_t(strUUID.asChar()), &pGeoType);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return "";
	}

	if(pGeoType.GetInterfacePtr())
	{
		_bstr_t name;
		pGeoType->get_Name(&name.GetBSTR());

		return ((const char*)name);
	}

	return "";
}

MString rageGeoDataSetCommands::GetTypeNameFromInstanceUUID(const MString& strInstanceUUID)
{
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return "";
	}

	IGeoTypePtr pGeoType;
	HRESULT hr = pDataSet->GetInstanceByUUID(_bstr_t(strInstanceUUID.asChar()), &pGeoType);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return "";
	}

	if(!pGeoType)
	{
		MGlobal::displayError("Unable to find instance "+ strInstanceUUID +" in the DataSet");
		return "";
	}

	IGeoTypePtr pBaseType;
	hr = pGeoType->get_BaseType(&pBaseType);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return "";
	}

	if(!pBaseType.GetInterfacePtr())
	{
		return "";
	}

	_bstr_t bstrBaseTypeName;
	pBaseType->get_Name(&bstrBaseTypeName.GetBSTR());

	return (const char*)bstrBaseTypeName;
}

MString rageGeoDataSetCommands::GetParentTypeNameFromTypeName(const MString& strTypeName)
{
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return "";
	}

	IGeoTypePtr pGeoType;
	const char* pcTypeName = strTypeName.asChar();
	HRESULT hr = pDataSet->GetTopLevelType(_bstr_t(pcTypeName), &pGeoType);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return "";
	}

	if(!pGeoType)
	{
		MGlobal::displayError("Unable to find instance "+ strTypeName +" in the DataSet");
		return "";
	}

	if(pGeoType.GetInterfacePtr())
	{
		_bstr_t uuid;
		pGeoType->get_UUID(&uuid.GetBSTR());

		return ((const char*)uuid);
	}

	IGeoTypePtr pBaseType;
	hr = pGeoType->get_BaseType(&pBaseType);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return "";
	}

	if(!pBaseType.GetInterfacePtr())
	{
		return "";
	}

	_bstr_t bstrBaseTypeName;
	pBaseType->get_Name(&bstrBaseTypeName.GetBSTR());

	return (const char*)bstrBaseTypeName;
}

MString rageGeoDataSetCommands::GetTemplateNameFromTypeName(const MString& strTypeName)
{
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return "";
	}

	IGeoTypePtr pGeoType;
	const char* pcTypeName = strTypeName.asChar();
	HRESULT hr = pDataSet->GetTopLevelType(_bstr_t(pcTypeName), &pGeoType);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return "";
	}

	if(!pGeoType)
	{
		MGlobal::displayError("Unable to find instance "+ strTypeName +" in the DataSet");
		return "";
	}

	IGeoTemplatePtr pTemplate;
	hr = pGeoType->get_Template(&pTemplate);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return "";
	}

	if(!pTemplate.GetInterfacePtr())
	{
		MGlobal::displayError("GeoGetTemplateName, failed to retrieve template.");
		return "";
	}

	_bstr_t bstrTemplateName;
	pTemplate->get_Name(&bstrTemplateName.GetBSTR());
	return (const char*)bstrTemplateName;
}

MString rageGeoDataSetCommands::GetParentTemplateNameFromTemplateName(const MString& strGeoTemplateName)
{
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return "";
	}

	IGeoTemplatePtr pGeoTemplate;
	HRESULT hr = pDataSet->GetTemplate(_bstr_t(strGeoTemplateName.asChar()), &pGeoTemplate);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return "";
	}

	if(!pGeoTemplate)
	{
		MGlobal::displayError("Unable to find template "+ strGeoTemplateName +" in the DataSet");
		return "";
	}

	IGeoTemplatePtr pParentTemplate;
	hr = pGeoTemplate->GetBaseTemplate(&pParentTemplate);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return "";
	}

	if(!pParentTemplate.GetInterfacePtr())
	{
		MGlobal::displayError("GeoGetTemplateName, failed to retrieve template.");
		return "";
	}

	_bstr_t bstrTemplateName;
	pParentTemplate->get_Name(&bstrTemplateName.GetBSTR());
	return (const char*)bstrTemplateName;}

vector<MString>	rageGeoDataSetCommands::GetContainerUIDsFromInstanceUID(const MString& strUIDInstance)
{
	vector<MString> obAStrContainerUIDs;

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return obAStrContainerUIDs;
	}

	IGeoDataSetHelperPtr pGeoHelper;
	HRESULT hr = pGeoHelper.CreateInstance(CLSID_GeoDataSetHelper);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return obAStrContainerUIDs;
	}

	SAFEARRAY *pContainerUUIDsSA = NULL;
	hr = pGeoHelper->GetContainerUUIDsFromInstanceUUID(pDataSet, _bstr_t(strUIDInstance.asChar()), &pContainerUUIDsSA);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return obAStrContainerUIDs;
	}

	if( !pContainerUUIDsSA )
	{
		return obAStrContainerUIDs;
	}

	for(long i=0; i < (long)pContainerUUIDsSA->rgsabound[0].cElements; i++)
	{
		_bstr_t	bstrUUID;
		SafeArrayGetElement(pContainerUUIDsSA, &i, &bstrUUID.GetBSTR());
		obAStrContainerUIDs.push_back(MString((const char*)bstrUUID));
	}

	return obAStrContainerUIDs;
}

vector<MString>	rageGeoDataSetCommands::GetContainerNamesFromInstanceUID(const MString& strUIDInstance)
{
	vector<MString> obAStrContainerNames;

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return obAStrContainerNames;
	}

	IGeoDataSetHelperPtr pGeoHelper;
	HRESULT hr = pGeoHelper.CreateInstance(CLSID_GeoDataSetHelper);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return obAStrContainerNames;
	}

	SAFEARRAY *pContainerNamesSA = NULL;
	hr = pGeoHelper->GetContainerNamesFromInstanceUUID(pDataSet, _bstr_t(strUIDInstance.asChar()), &pContainerNamesSA);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return obAStrContainerNames;
	}

	if( !pContainerNamesSA )
	{
		return obAStrContainerNames;
	}

	for(long i=0; i < (long)pContainerNamesSA->rgsabound[0].cElements; i++)
	{
		_bstr_t	bstrName;
		SafeArrayGetElement(pContainerNamesSA, &i, &bstrName.GetBSTR());
		obAStrContainerNames.push_back(MString((const char*)bstrName));
	}

	return obAStrContainerNames;
}

vector<MString>	rageGeoDataSetCommands::GetUIDsOfContainedInstancesFromParentInstanceUID(const MString& strInstanceUID, const MString& strContainerUID)
{
	vector<MString> obAStrContainedUIDs;

	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return obAStrContainedUIDs;
	}

	IGeoDataSetHelperPtr pGeoHelper;
	HRESULT hr = pGeoHelper.CreateInstance(CLSID_GeoDataSetHelper);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return obAStrContainedUIDs;
	}

	SAFEARRAY *pContainerUUIDsSA = NULL;
	hr = pGeoHelper->GetUIDsOfContainedInstancesFromParentInstanceUID(pDataSet, _bstr_t(strInstanceUID.asChar()), _bstr_t(strContainerUID.asChar()), &pContainerUUIDsSA);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return obAStrContainedUIDs;
	}

	if( !pContainerUUIDsSA )
	{
		return obAStrContainedUIDs;
	}

	for(long i=0; i < (long)pContainerUUIDsSA->rgsabound[0].cElements; i++)
	{
		_bstr_t	bstrUUID;
		SafeArrayGetElement(pContainerUUIDsSA, &i, &bstrUUID.GetBSTR());
		obAStrContainedUIDs.push_back(MString((const char*)bstrUUID));
	}

	return obAStrContainedUIDs;
}

MString rageGeoDataSetCommands::GetBaseGeoFolder()
{
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return "";
	}

	// Get the template folder
	_bstr_t strTemplateFolder;
	pDataSet->GetGeoTemplateDirectory(&strTemplateFolder.GetBSTR());

	// Get the parent folder
	MString strReturnMe((const char*)strTemplateFolder);
	strReturnMe = strReturnMe.substring(0, strReturnMe.rindex('/'));
	return strReturnMe;
}

MMatrix rageGeoDataSetCommands::GetWorldSpaceMatrix(const MString& strInstanceUUID)
{
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return MMatrix::identity;
	}

	IGeoTypePtr pGeoInstance;
	HRESULT hr = pDataSet->GetInstanceByUUID(_bstr_t(strInstanceUUID.asChar()), &pGeoInstance);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MMatrix::identity;
	}

	if(!pGeoInstance)
	{
		MGlobal::displayError("Unable to find instance "+ strInstanceUUID +" in the DataSet");
		return MMatrix::identity;
	}

	// Got the instance, so get the matrix from it
	IGeoTypeMemberBasePtr pMbrBase;
	hr = pGeoInstance->GetMemberByName(_bstr_t("Transform"), &pMbrBase);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MMatrix::identity;
	}

	if(!pMbrBase)
	{
		MGlobal::displayError("Unable to find Transform parameter on instance "+ strInstanceUUID +" in the DataSet");
		return MMatrix::identity;
	}

	IGeoTypeMemberMatrix34Ptr pMbrMat34;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberMatrix34, &pMbrMat34);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return MMatrix::identity;
	}

	rageGEODataSet::Matrix34 afMatrix34;
	pMbrMat34->get_Value(&afMatrix34);
	MMatrix obReturnMe;
	obReturnMe.setToIdentity();
	obReturnMe[0][0] = afMatrix34.a.x;
	obReturnMe[0][1] = afMatrix34.a.y;
	obReturnMe[0][2] = afMatrix34.a.z;

	obReturnMe[1][0] = afMatrix34.b.x;
	obReturnMe[1][1] = afMatrix34.b.y;
	obReturnMe[1][2] = afMatrix34.b.z;

	obReturnMe[2][0] = afMatrix34.c.x;
	obReturnMe[2][1] = afMatrix34.c.y;
	obReturnMe[2][2] = afMatrix34.c.z;

	obReturnMe[3][0] = afMatrix34.d.x;
	obReturnMe[3][1] = afMatrix34.d.y;
	obReturnMe[3][2] = afMatrix34.d.z;

	return obReturnMe;
}

void rageGeoDataSetCommands::EditGeoInstance(const MString& strInstanceUUID)
{
	cout<<__FILE__<<":"<<__LINE__<<" : rageGeoDataSetCommands::EditGeoInstance(\""<<strInstanceUUID.asChar()<<"\")"<<endl;
	IGeoDataSet* pDataSet = GEODATASET.GetCurrentDataSet();
	if(!pDataSet)
	{
		MGlobal::displayError("GeoGetUUIDFromName, No GeoDataSet has been loaded.");
		return;
	}
	cout<<__FILE__<<":"<<__LINE__<<" : rageGeoDataSetCommands::EditGeoInstance(\""<<strInstanceUUID.asChar()<<"\")"<<endl;

	IGeoTypePtr pGeoInstance;
	HRESULT hr = pDataSet->GetInstanceByUUID(_bstr_t(strInstanceUUID.asChar()), &pGeoInstance);
	if(FAILED(hr))
	{
		DisplayCOMErrorMaya(hr);
		return;
	}
	cout<<__FILE__<<":"<<__LINE__<<" : rageGeoDataSetCommands::EditGeoInstance(\""<<strInstanceUUID.asChar()<<"\")"<<endl;

	if(!pGeoInstance)
	{
		MGlobal::displayError("Unable to find instance "+ strInstanceUUID +" in the DataSet");
		return;
	}
	cout<<__FILE__<<":"<<__LINE__<<" : rageGeoDataSetCommands::EditGeoInstance(\""<<strInstanceUUID.asChar()<<"\")"<<endl;

	// Edit it
	pGeoInstance->OpenInEditor();
	cout<<__FILE__<<":"<<__LINE__<<" : rageGeoDataSetCommands::EditGeoInstance(\""<<strInstanceUUID.asChar()<<"\")"<<endl;
}

