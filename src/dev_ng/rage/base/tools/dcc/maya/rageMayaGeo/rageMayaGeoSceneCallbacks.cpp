#include "rageMayaGeoSceneCallbacks.h"
#include "rageMayaGeoDataSet.h"

bool				rageMayaGeoSceneCallbacks::m_bPluginNeedsCleanUp = true;
MCallbackId			rageMayaGeoSceneCallbacks::m_MayaExitingCallbackId;
MCallbackId			rageMayaGeoSceneCallbacks::m_BeforeOpenCallbackId;
MCallbackId			rageMayaGeoSceneCallbacks::m_BeforeNewCallbackId;

void		rageMayaGeoSceneCallbacks::InitializeGlobalCallbacks()
{
	MStatus obStatus;
	m_MayaExitingCallbackId = MSceneMessage::addCallback(MSceneMessage::kMayaExiting, rageMayaGeoSceneCallbacks::MayaExitingCallback, NULL, &obStatus);
	m_BeforeOpenCallbackId = MSceneMessage::addCallback(MSceneMessage::kBeforeOpen, rageMayaGeoSceneCallbacks::BeforeOpenCallback, NULL, &obStatus);
	m_BeforeNewCallbackId = MSceneMessage::addCallback(MSceneMessage::kBeforeNew, rageMayaGeoSceneCallbacks::BeforeNewCallback, NULL, &obStatus);
}


void		rageMayaGeoSceneCallbacks::UninitializeGlobalCallbacks()
{
	MMessage::removeCallback(m_MayaExitingCallbackId);
	MMessage::removeCallback(m_BeforeOpenCallbackId);
	MMessage::removeCallback(m_BeforeNewCallbackId);
}

void rageMayaGeoSceneCallbacks::MayaExitingCallback(void* /*clientData*/)
{
	//Maya doesn't seem to always call the unitializePlugin method when its exiting, so
	//use this method to make sure all resources are freed up.  This helps to eliminate
	//memory leak dumps that really shouldn't be memory leaks.
	if(m_bPluginNeedsCleanUp)
	{
		SHUTDOWN_GEODATASET;
		m_bPluginNeedsCleanUp = false;
		CoUninitialize();
	}
}

void rageMayaGeoSceneCallbacks::BeforeOpenCallback(void* /*clientData*/)
{
	// Make sure everything is initialized before the scene is loaded
	// Uninitialize the GEO system
	MGlobal::executeCommand("rageGeo.Uninitalize()", true);

	// Initialize the GEO system
	MGlobal::executeCommand("rageGeo.Initalize()", true);
}

void rageMayaGeoSceneCallbacks::BeforeNewCallback(void* /*clientData*/)
{
	// Make sure everything is initialized before the scene is loaded
	// Uninitialize the GEO system
	MGlobal::executeCommand("rageGeo.Uninitalize()", true);

	// Initialize the GEO system
	MGlobal::executeCommand("rageGeo.Initalize()", true);
}
