// 
// /rageMayaGeoDataSetCmds.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __RAGEMAYAGEODATASETCMDS_H_
#define __RAGEMAYAGEODATASETCMDS_H_

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include <maya/MGlobal.h>
#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MCommandResult.h>
#include <maya/MDoubleArray.h>
#include <maya/MStringArray.h>

#pragma warning(pop)

#include "rageMayaGeoDataSet.h"

//-----------------------------------------------------------------------------

class GeoGetDataSetVersionCmd : public MPxCommand
{
public:
	GeoGetDataSetVersionCmd() {};
	virtual ~GeoGetDataSetVersionCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoGetDataSetVersionCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoLoadDataSetCmd : public MPxCommand
{
public:
	GeoLoadDataSetCmd() {};
	virtual ~GeoLoadDataSetCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoLoadDataSetCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoUnLoadDataSetCmd : public MPxCommand
{
public:
	GeoUnLoadDataSetCmd() {};
	virtual ~GeoUnLoadDataSetCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoUnLoadDataSetCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoSaveDataSetCmd : public MPxCommand
{
public:
	GeoSaveDataSetCmd() {};
	virtual ~GeoSaveDataSetCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoSaveDataSetCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoGetTypeMembersOfTypeCmd : public MPxCommand
{
public:
	GeoGetTypeMembersOfTypeCmd() {};
	virtual ~GeoGetTypeMembersOfTypeCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoGetTypeMembersOfTypeCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoGetAvailableTypesCmd : public MPxCommand
{
public:
	GeoGetAvailableTypesCmd() {};
	virtual ~GeoGetAvailableTypesCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoGetAvailableTypesCmd(); }
	static MSyntax	newSyntax();

private:
	rageGEODataSet::GeoSortType	sortType;
};

//-----------------------------------------------------------------------------

class GeoGetNameFromUUIDCmd : public MPxCommand
{
public:
	GeoGetNameFromUUIDCmd() {};
	virtual ~GeoGetNameFromUUIDCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoGetNameFromUUIDCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoGetUUIDFromNameCmd : public MPxCommand
{
public:
	GeoGetUUIDFromNameCmd() {};
	virtual ~GeoGetUUIDFromNameCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoGetUUIDFromNameCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoGetBaseTypeNameCmd : public MPxCommand
{
public:
	GeoGetBaseTypeNameCmd() {};
	virtual ~GeoGetBaseTypeNameCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoGetBaseTypeNameCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoGetTemplateNameCmd : public MPxCommand
{
public:
	GeoGetTemplateNameCmd() {};
	virtual ~GeoGetTemplateNameCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoGetTemplateNameCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoMarkAllInstancesCmd : public MPxCommand
{
	public:
	GeoMarkAllInstancesCmd() {};
	virtual ~GeoMarkAllInstancesCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoMarkAllInstancesCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoDeleteInstanceCmd : public MPxCommand
{
	public:
	GeoDeleteInstanceCmd() {};
	virtual ~GeoDeleteInstanceCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoDeleteInstanceCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoGetInstanceContainerNamesFromUUIDCmd : public MPxCommand
{
public:
	GeoGetInstanceContainerNamesFromUUIDCmd() {};
	virtual ~GeoGetInstanceContainerNamesFromUUIDCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoGetInstanceContainerNamesFromUUIDCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoGetInstanceContainerUUIDsFromUUIDCmd : public MPxCommand
{
public:
	GeoGetInstanceContainerUUIDsFromUUIDCmd() {};
	virtual ~GeoGetInstanceContainerUUIDsFromUUIDCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoGetInstanceContainerUUIDsFromUUIDCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoCreateInstanceOfTypeCmd : public MPxCommand
{
public:
	GeoCreateInstanceOfTypeCmd() {};
	virtual ~GeoCreateInstanceOfTypeCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoCreateInstanceOfTypeCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoSetInstanceMemberValueCmd : public MPxCommand
{
public:
	GeoSetInstanceMemberValueCmd() {};
	virtual ~GeoSetInstanceMemberValueCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoSetInstanceMemberValueCmd(); }
	static MSyntax	newSyntax();

private:
	typedef MStatus (*SetMemberPtr)(IGeoTypeMemberBasePtr,MString);

	static MStatus	SetMemberMatrix34Value(IGeoTypeMemberBasePtr pMbrBase, MString memberValue);
	static MStatus	SetMemberMatrix44Value(IGeoTypeMemberBasePtr pMbrBase, MString memberValue);
	static MStatus	SetMemberBoolValue(IGeoTypeMemberBasePtr pMbrBase, MString memberValue);
	static MStatus	SetMemberFloatValue(IGeoTypeMemberBasePtr pMbrBase, MString memberValue);
	static MStatus	SetMemberIntValue(IGeoTypeMemberBasePtr pMbrBase, MString memberValue);
	static MStatus	SetMemberStringValue(IGeoTypeMemberBasePtr pMbrBase, MString memberValue);
	static MStatus	SetMemberVector3Value(IGeoTypeMemberBasePtr pMbrBase, MString memberValue);
};

//-----------------------------------------------------------------------------

class GeoCanContainerHoldGeoInstanceCmd : public MPxCommand
{
public:
	GeoCanContainerHoldGeoInstanceCmd() {};
	virtual ~GeoCanContainerHoldGeoInstanceCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoCanContainerHoldGeoInstanceCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoMoveInstanceCmd : public MPxCommand
{
public:
	GeoMoveInstanceCmd() {};
	virtual ~GeoMoveInstanceCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoMoveInstanceCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

class GeoEditInstanceCmd : public MPxCommand
{
public:
	GeoEditInstanceCmd() {};
	virtual ~GeoEditInstanceCmd() {};

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator() { return new GeoEditInstanceCmd(); }
	static MSyntax	newSyntax();
};

//-----------------------------------------------------------------------------

#endif //__RAGEMAYAGEODATASET_H_
