#ifndef _RAGE_HAIR_MANIP
#define _RAGE_HAIR_MANIP
// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
#include <maya/MPxManipContainer.h>
#include <maya/MStringArray.h>
// #include <maya/MPxSelectionContext.h>
// #include <maya/MPxContextCommand.h>
// #include <maya/MModelMessage.h>
// #include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>

// Manipulators
// #include <maya/MFnScaleManip.h>

#define DUMMY_ATTRIBUTE_NAME	"rageHairManip_dummyAttribute"
#define USE_BLIND_DATA

void OutputError(const MString& obStrErrorMessage);

/////////////////////////////////////////////////////////////
//
// rageHairManip
//
// This class implements the component scaling example.
//
/////////////////////////////////////////////////////////////

class rageHairManip : public MPxManipContainer
{
public:
	rageHairManip();
	virtual ~rageHairManip();
	
	static MStatus initialize();
	virtual MStatus connectToDependNode(const MObject &node);

	virtual void draw(M3dView &view, 
					  const MDagPath &path, 
					  M3dView::DisplayStyle style,
					  M3dView::DisplayStatus status);

	virtual void setMeshDagPath(const MDagPath& dagPath) {
		m_obMeshDagPath = dagPath;
	}

	virtual void setComponentObject(const MObject& obj) {
		m_obSelectedComponents = obj;
	}

	// Callback function
	virtual MManipData manipChangedCallback(unsigned index) = 0;
	virtual MManipData centerChangedCallback(unsigned index);

	MDagPath m_obManipDagPath;

	// Accessors
	MObject GetSelectedComponents() const {return m_obSelectedComponents;}
	MDagPath GetMeshDagPath() const {return m_obMeshDagPath;}
	const MVector&	GetInitialHairVector(int i) const {return m_aobInitialHairVectors[i];}

private:

	MDagPath m_obMeshDagPath;
	MObject m_obSelectedComponents;

	MPoint m_obCentroidOfAllSelectedComponents;

	MVector* m_aobInitialHairVectors;
	bool*	m_abVertexIsSelected;

	unsigned dummyPlugIndex;

protected:
	MStringArray m_astrBindDataLongNames;
	MIntArray m_obAISelectedComponentIndexes;
};
#endif
