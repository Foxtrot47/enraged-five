// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
// #include <maya/MPxManipContainer.h>
// #include <maya/MPxSelectionContext.h>
#include <maya/MPxContextCommand.h>
// #include <maya/MModelMessage.h>
// #include <maya/MFnPlugin.h>
// #include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>

// Manipulators
// #include <maya/MFnScaleManip.h>
#include "rageHairScaleCmd.h"
#include "rageHairScaleContext.h"

MPxContext *rageHairScaleCmd::makeObj()
{
	return new rageHairScaleContext();
}

void *rageHairScaleCmd::creator()
{
	return new rageHairScaleCmd;
}
