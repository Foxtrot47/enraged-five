#ifndef _RAGE_HAIR_SCALE_CONTEXT
#define _RAGE_HAIR_SCALE_CONTEXT
// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
// #include <maya/MPxManipContainer.h>
#include <maya/MPxSelectionContext.h>
// #include <maya/MPxContextCommand.h>
#include <maya/MModelMessage.h>
// #include <maya/MFnPlugin.h>
// #include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>

// Manipulators
// #include <maya/MFnRotateManip.h>
#include "rageHairManip.h"

/////////////////////////////////////////////////////////////
//
// rageHairRotateContext
//
// This class is a simple context for supporting a rotate manipulator.
//
/////////////////////////////////////////////////////////////

class rageHairRotateContext : public MPxSelectionContext
{
public:
	rageHairRotateContext();
	virtual void	toolOnSetup(MEvent &event);
	virtual void	toolOffCleanup();

	// Callback issued when selection list changes
	static void updateManipulators(void * data);

private:
	MCallbackId id1;
};
#endif
