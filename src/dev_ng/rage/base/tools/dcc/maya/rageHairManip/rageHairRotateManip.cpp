//#include <maya/MIOStream.h>
//#include <stdio.h>
//#include <stdlib.h>
//
//#include <maya/MFn.h>
//#include <maya/MPxNode.h>
//#include <maya/rageHairManip.h>
//#include <maya/MPxSelectionContext.h>
//#include <maya/MPxContextCommand.h>
//#include <maya/MModelMessage.h>
//#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
//#include <maya/MItSelectionList.h>
//#include <maya/MPoint.h>
#include <maya/MVector.h>
//#include <maya/MDagPath.h>
#include <maya/MManipData.h>
//#include <maya/MSelectionList.h>
#include <maya/MItMeshVertex.h>
#include <maya/MFnComponent.h>
//#include <maya/MStatus.h>
//#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MEulerRotation.h>

//
//Manipulators
#include <maya/MFnRotateManip.h>
#include <maya/MFnRotateManip.h>
//
//#include "rageHairContext.h"
#include "rageHairRotateManip.h"
//#include "rageHairRotateCmd.h"
// #include "rageHairRotateManip.h"
#define POINT_SIZE				20.0	
#define BLIND_DATA_ID			60

MTypeId rageHairRotateManip::id( 0x700C );

rageHairRotateManip::rageHairRotateManip() 
: rageHairManip()
{ 
	// The constructor must not call createChildren for user-defined
	// manipulators.
}


rageHairRotateManip::~rageHairRotateManip() 
{
}


void *rageHairRotateManip::creator()
{
	 return new rageHairRotateManip();
}


MStatus rageHairRotateManip::initialize()
{ 
	MStatus obStatus;
	obStatus = rageHairManip::initialize();
	return obStatus;
}


MStatus rageHairRotateManip::createChildren()
{
	MStatus obStatus = MStatus::kSuccess;
	m_obManipDagPath = addRotateManip("rotateManip", "rotate");
	return obStatus;
}


MStatus rageHairRotateManip::connectToDependNode(const MObject &obMeshNode)
{
	MStatus obStatus;
	rageHairManip::connectToDependNode(obMeshNode);
	
	MFnComponent obFnSelectedComponents(GetSelectedComponents());
	MFnMesh obFnMeshNode(obMeshNode);
	MPlug obDummyPlug = obFnMeshNode.findPlug( DUMMY_ATTRIBUTE_NAME, &obStatus );
	if((!obStatus) || (obDummyPlug.isNull()))
	{
		OutputError(MString("Could not find ")+ DUMMY_ATTRIBUTE_NAME +" attribute on "+ obFnMeshNode.fullPathName());
		return MS::kFailure;
	}

	int i = 0;
	for (MItMeshVertex iter(GetMeshDagPath(), GetSelectedComponents()); !iter.isDone(); iter.next(), i++)
	{
		// Add the callback for this vector
		unsigned plugIndex = addManipToPlugConversionCallback(obDummyPlug, (manipToPlugConversionCallback) &rageHairRotateManip::manipChangedCallback);

		// Plug indices should be allocated sequentially, starting at 0.  Each
		// manip container will have its own set of plug indices.  This code 
		// relies on the sequential allocation of indices, so trigger an error
		// if the indices are found to be different.
		//
		if (plugIndex != (unsigned)i)
		{
			OutputError("Unexpected plug index returned.");
			return MS::kFailure;
		}
	}

	MFnRotateManip rotateManip(m_obManipDagPath);

	// Create a plugToManip callback that places the manipulator at the 
	// m_obCentroidOfAllSelectedComponents of the CVs.
	addPlugToManipConversionCallback(rotateManip.rotationCenterIndex(), (plugToManipConversionCallback) &rageHairRotateManip::centerChangedCallback);

	finishAddingManips();
	return obStatus;
}

MManipData rageHairRotateManip::manipChangedCallback(unsigned iMagicIndex) 
{
	// cout<<"rageHairRotateManip::manipChangedCallback("<<iMagicIndex<<")"<<endl;
	MObject obj = MObject::kNullObj;

	// If we entered the callback with an invalid index, print an error and
	// return.  The callback should only be called for indices from 0 to
	// m_iNumComponents-1.
	//
	MFnRotateManip rotateManip(m_obManipDagPath);
	if (iMagicIndex >= m_obAISelectedComponentIndexes.length())
	{
		OutputError("Invalid index in rotate changed callback!");
		return MManipData(32.0f);
	}

	// Now we need to determine the rotated position of the CV specified by iMagicIndex.
	MVector rotateVal;
	getConverterManipValue(rotateManip.rotationIndex(), rotateVal);

	// rotateVal now contains X,Y,Z Eular rotations in radians.
	// cout<<"rotateVal = "<<rotateVal<<endl;

	// I need to rotate the hair vector by these rotations
	MEulerRotation obRotationAsEular(rotateVal);
	MVector obNewHairVector = GetInitialHairVector(m_obAISelectedComponentIndexes[iMagicIndex]).rotateBy(obRotationAsEular);
	for(int iAxis=0; iAxis<3; iAxis++)
	{
		// Set value for the blind data
		// polyBlindData -id 60 -associationType "vertex" -longDataName "bdFloat" -doubleData 234 pPlane1.vtx[0]
		float fDataIn = (float)obNewHairVector[iAxis];
		// cout<<"fDataIn = "<<fDataIn<<"\tobVertexNormal["<<iAxis<<"] = "<<obVertexNormal[iAxis]<<endl;
		MFnMesh obFnMeshNode(GetMeshDagPath());
		MStatus obStatus = obFnMeshNode.setFloatBlindData(m_obAISelectedComponentIndexes[iMagicIndex], MFn::kMeshVertComponent, BLIND_DATA_ID, m_astrBindDataLongNames[iAxis], fDataIn);
		if (!obStatus)
		{
			OutputError("Could not set blind data "+ m_astrBindDataLongNames[iAxis] +" X for vert on "+ obFnMeshNode.fullPathName());
//			return MS::kFailure;
		}
	}
	return MManipData(42.0f);
}
