#ifndef _RAGE_HAIR_SCALE_MANIP
#define _RAGE_HAIR_SCALE_MANIP
// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
// #include <maya/MPxSelectionContext.h>
// #include <maya/MPxContextCommand.h>
// #include <maya/MModelMessage.h>
// #include <maya/MFnPlugin.h>
// #include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>

// Manipulators
// #include <maya/MFnScaleManip.h>

#include "rageHairManip.h"


/////////////////////////////////////////////////////////////
//
// rageHairScaleManip
//
// This class implements the component scaling example.
//
/////////////////////////////////////////////////////////////

class rageHairScaleManip : public rageHairManip
{
public:
	rageHairScaleManip();
	virtual ~rageHairScaleManip();
	
	static void * creator();
	static MStatus initialize();
	virtual MStatus createChildren();
	virtual MStatus connectToDependNode(const MObject &node);

	// Callback function
	MManipData manipChangedCallback(unsigned index);

public:
	static MTypeId id;

private:
};
#endif
