// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
// #include <maya/MPxManipContainer.h>
// #include <maya/MPxSelectionContext.h>
// #include <maya/MPxContextCommand.h>
// #include <maya/MModelMessage.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>
#include <maya/MStatus.h>
#include <maya/MObject.h>

// Manipulators
// #include <maya/MFnScaleManip.h>

// #include "rageHairContext.h"
#include "rageHairScaleManip.h"
#include "rageHairScaleCmd.h"
#include "rageHairRotateManip.h"
#include "rageHairRotateCmd.h"

///////////////////////////////////////////////////////////////////////
//
// The following routines are used to register/unregister
// the context and manipulator
//
///////////////////////////////////////////////////////////////////////

MStatus initializePlugin(MObject obj)
{
	MStatus status;
	MFnPlugin plugin(obj, "Alias", "6.0", "Any");

	// Scale
    status = plugin.registerContextCommand("rageHairScaleCmd",
										   &rageHairScaleCmd::creator);
	if (!status) 
	{
		OutputError("Error registering rageHairScaleCmd command");
		cout<<status.errorString()<<endl;
		return status;
	}

	status = plugin.registerNode("rageHairScaleManip", rageHairScaleManip::id, 
								 &rageHairScaleManip::creator, &rageHairScaleManip::initialize,
								 MPxNode::kManipContainer);
	if (!status) 
	{
		OutputError("Error registering rageHairScaleManip node");
		cout<<status.errorString()<<endl;
		return status;
	}

	// Rotate
	status = plugin.registerContextCommand("rageHairRotateCmd",
		&rageHairRotateCmd::creator);
	if (!status) 
	{
		OutputError("Error registering rageHairRotateCmd command");
		cout<<status.errorString()<<endl;
		return status;
	}

	status = plugin.registerNode("rageHairRotateManip", rageHairRotateManip::id, 
		&rageHairRotateManip::creator, &rageHairRotateManip::initialize,
		MPxNode::kManipContainer);
	if (!status) 
	{
		OutputError("Error registering rageHairRotateManip node");
		cout<<status.errorString()<<endl;
		return status;
	}

	return status;
}


MStatus uninitializePlugin(MObject obj)
{
	MStatus status;
	MFnPlugin plugin(obj);

	// Scale
	status = plugin.deregisterContextCommand("rageHairScaleCmd");
	if (!status) {
		OutputError("Error deregistering rageHairScaleCmd command");
		return status;
	}

	status = plugin.deregisterNode(rageHairScaleManip::id);
	if (!status) {
		OutputError("Error deregistering rageHairScaleManip node");
		return status;
	}

	// Rotate
	status = plugin.deregisterContextCommand("rageHairRotateCmd");
	if (!status) {
		OutputError("Error deregistering rageHairRotateCmd command");
		return status;
	}

	status = plugin.deregisterNode(rageHairRotateManip::id);
	if (!status) {
		OutputError("Error deregistering rageHairRotateManip node");
		return status;
	}

	return status;
}
