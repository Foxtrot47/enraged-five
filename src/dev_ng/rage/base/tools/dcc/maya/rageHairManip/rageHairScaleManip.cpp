//#include <maya/MIOStream.h>
//#include <stdio.h>
//#include <stdlib.h>
//
//#include <maya/MFn.h>
//#include <maya/MPxNode.h>
//#include <maya/rageHairManip.h>
//#include <maya/MPxSelectionContext.h>
//#include <maya/MPxContextCommand.h>
//#include <maya/MModelMessage.h>
//#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
//#include <maya/MItSelectionList.h>
//#include <maya/MPoint.h>
#include <maya/MVector.h>
//#include <maya/MDagPath.h>
#include <maya/MManipData.h>
//#include <maya/MSelectionList.h>
#include <maya/MItMeshVertex.h>
#include <maya/MFnComponent.h>
//#include <maya/MStatus.h>
//#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>

//
//Manipulators
#include <maya/MFnScaleManip.h>
#include <maya/MFnRotateManip.h>
//
//#include "rageHairContext.h"
#include "rageHairScaleManip.h"
//#include "rageHairScaleCmd.h"
// #include "rageHairScaleManip.h"
#define POINT_SIZE				20.0	
#define BLIND_DATA_ID			60

MTypeId rageHairScaleManip::id( 0x700B );

rageHairScaleManip::rageHairScaleManip() 
: rageHairManip()
{ 
	// The constructor must not call createChildren for user-defined
	// manipulators.
}


rageHairScaleManip::~rageHairScaleManip() 
{
}


void *rageHairScaleManip::creator()
{
	 return new rageHairScaleManip();
}


MStatus rageHairScaleManip::initialize()
{ 
	MStatus obStatus;
	obStatus = rageHairManip::initialize();
	return obStatus;
}


MStatus rageHairScaleManip::createChildren()
{
	MStatus obStatus = MStatus::kSuccess;
	m_obManipDagPath = addScaleManip("scaleManip", "scale");
	return obStatus;
}


MStatus rageHairScaleManip::connectToDependNode(const MObject &obMeshNode)
{
	MStatus obStatus;
	rageHairManip::connectToDependNode(obMeshNode);
	
	MFnComponent obFnSelectedComponents(GetSelectedComponents());
	MFnMesh obFnMeshNode(obMeshNode);

	MPlug obDummyPlug = obFnMeshNode.findPlug( DUMMY_ATTRIBUTE_NAME, &obStatus );
	if((!obStatus) || (obDummyPlug.isNull()))
	{
		OutputError(MString("Could not find ")+ DUMMY_ATTRIBUTE_NAME +" attribute on "+ obFnMeshNode.fullPathName());
		return MS::kFailure;
	}

	int i = 0;
	for (MItMeshVertex iter(GetMeshDagPath(), GetSelectedComponents()); !iter.isDone(); iter.next(), i++)
	{
		// Add the callback for this vector
		unsigned plugIndex = addManipToPlugConversionCallback(obDummyPlug, (manipToPlugConversionCallback) &rageHairScaleManip::manipChangedCallback);

		// Plug indices should be allocated sequentially, starting at 0.  Each
		// manip container will have its own set of plug indices.  This code 
		// relies on the sequential allocation of indices, so trigger an error
		// if the indices are found to be different.
		//
		if (plugIndex != (unsigned)i)
		{
			OutputError("Unexpected plug index returned.");
			return MS::kFailure;
		}
	}

	MFnScaleManip scaleManip(m_obManipDagPath);

	// Create a plugToManip callback that places the manipulator at the 
	// m_obCentroidOfAllSelectedComponents of the CVs.
	addPlugToManipConversionCallback(scaleManip.scaleCenterIndex(), (plugToManipConversionCallback) &rageHairScaleManip::centerChangedCallback);

	finishAddingManips();
	return obStatus;
}

MManipData rageHairScaleManip::manipChangedCallback(unsigned iMagicIndex) 
{
	MObject obj = MObject::kNullObj;

	// If we entered the callback with an invalid iMagicIndex, print an error and
	// return.  The callback should only be called for indices from 0 to
	// m_iNumComponents-1.
	//
	MFnScaleManip scaleManip(m_obManipDagPath);
	if (iMagicIndex >= m_obAISelectedComponentIndexes.length())
	{
		OutputError("Invalid index in scale changed callback!");
		return MManipData(32.0f);
	}

	// Now we need to determine the scaled position of the CV specified by iMagicIndex.
	MVector scaleVal;
	getConverterManipValue(scaleManip.scaleIndex(), scaleVal);

	// Apply the scale
	MVector obInitialHairVector = GetInitialHairVector(m_obAISelectedComponentIndexes[iMagicIndex]);
	MVector obNewHairVector(obInitialHairVector.x * scaleVal.x, obInitialHairVector.y * scaleVal.y, obInitialHairVector.z * scaleVal.z);

	for(int iAxis=0; iAxis<3; iAxis++)
	{
		// Set value for the blind data
		// polyBlindData -id 60 -associationType "vertex" -longDataName "bdFloat" -doubleData 234 pPlane1.vtx[0]
		float fDataIn = (float)obNewHairVector[iAxis];
		// cout<<"fDataIn = "<<fDataIn<<"\tobVertexNormal["<<iAxis<<"] = "<<obVertexNormal[iAxis]<<endl;
		MFnMesh obFnMeshNode(GetMeshDagPath());
		MStatus obStatus = obFnMeshNode.setFloatBlindData(m_obAISelectedComponentIndexes[iMagicIndex], MFn::kMeshVertComponent, BLIND_DATA_ID, m_astrBindDataLongNames[iAxis], fDataIn);
		if (!obStatus)
		{
			OutputError("Could not set blind data "+ m_astrBindDataLongNames[iAxis] +" X for vert on "+ obFnMeshNode.fullPathName());
//			return MS::kFailure;
		}
	}
	return MManipData(42.0f);
}
