#ifndef _RAGE_HAIR_ROTATE_CMD
#define _RAGE_HAIR_ROTATE_CMD
// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
// #include <maya/MPxManipContainer.h>
// #include <maya/MPxSelectionContext.h>
#include <maya/MPxContextCommand.h>
// #include <maya/MModelMessage.h>
// #include <maya/MFnPlugin.h>
// #include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>

// Manipulators
// #include <maya/MFnRotateManip.h>

/////////////////////////////////////////////////////////////
//
// rageHairRotateCmd
//
// This is the command that will be used to create instances
// of our context.
//
/////////////////////////////////////////////////////////////

class rageHairRotateCmd : public MPxContextCommand
{
public:
	rageHairRotateCmd() {};
	virtual MPxContext * makeObj();

public:
	static void* creator();
};
#endif
