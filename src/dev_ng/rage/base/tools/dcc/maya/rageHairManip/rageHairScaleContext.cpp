// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
// #include <maya/MPxManipContainer.h>
// #include <maya/MPxSelectionContext.h>
// #include <maya/MPxContextCommand.h>
// #include <maya/MModelMessage.h>
// #include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
#include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>

// Manipulators
// #include <maya/MFnScaleManip.h>

#include "rageHairScaleContext.h"
#include "rageHairScaleManip.h"

rageHairScaleContext::rageHairScaleContext()
{
	MString str("Plugin component scaling manipulator");
	setTitleString(str);
	setImage("rageHairScaleContext.bmp", MPxContext::kImage1 );
}


void rageHairScaleContext::toolOnSetup(MEvent &)
{
	MString str("Scale the selected components");
	setHelpString(str);

	updateManipulators(this);
	MStatus status;
	id1 = MModelMessage::addCallback(MModelMessage::kActiveListModified,
		updateManipulators, 
		this, &status);
	if (!status) {
		OutputError("Model addCallback failed");
	}
}


void rageHairScaleContext::toolOffCleanup()
{
	MStatus status;
	status = MModelMessage::removeCallback(id1);
	if (!status) {
		OutputError("Model remove callback failed");
	}
	MPxContext::toolOffCleanup();
}


void rageHairScaleContext::updateManipulators(void * data)
{
	MStatus stat = MStatus::kSuccess;

	rageHairScaleContext * ctxPtr = (rageHairScaleContext *) data;
	ctxPtr->deleteManipulators(); 

	MSelectionList list;
	stat = MGlobal::getActiveSelectionList(list);
	MItSelectionList iter(list, MFn::kInvalid, &stat);

	if (MS::kSuccess == stat) {
		for (; !iter.isDone(); iter.next()) {

			// Make sure the selection list item is a dag path with components
			// before attaching the manipulator to it.
			//
			MDagPath dagPath;
			MObject components;
			iter.getDagPath(dagPath, components);

			if (components.isNull() || !components.hasFn(MFn::kComponent))
			{
				MGlobal::displayWarning("Object in selection list is not "
					"a component.");
				continue;
			}

			// Add manipulator to the selected object
			//
			MString manipName ("rageHairScaleManip");
			MObject manipObject;
			rageHairScaleManip* manipulator =
				(rageHairScaleManip *) rageHairScaleManip::newManipulator(manipName, manipObject);

			if (NULL != manipulator) {
				// Add the manipulator
				//
				ctxPtr->addManipulator(manipObject);

				// Connect the manipulator to the object in the selection list.
				//
				manipulator->setMeshDagPath(dagPath);
				manipulator->setComponentObject(components);
				if (!manipulator->connectToDependNode(dagPath.node()))
				{
					MGlobal::displayWarning("Error connecting manipulator to"
						" object");
				}
			} 
		}
	}
}
