//-
// ==========================================================================
// Copyright (C) 1995 - 2005 Alias Systems Corp. and/or its licensors.  All 
// rights reserved. 
// 
// The coded instructions, statements, computer programs, and/or related 
// material (collectively the "Data") in these files are provided by Alias 
// Systems Corp. ("Alias") and/or its licensors for the exclusive use of the 
// Customer (as defined in the Alias Software License Agreement that 
// accompanies this Alias software). Such Customer has the right to use, 
// modify, and incorporate the Data into other products and to distribute such 
// products for use by end-users.
//  
// THE DATA IS PROVIDED "AS IS".  ALIAS HEREBY DISCLAIMS ALL WARRANTIES 
// RELATING TO THE DATA, INCLUDING, WITHOUT LIMITATION, ANY AND ALL EXPRESS OR 
// IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND/OR FITNESS FOR A 
// PARTICULAR PURPOSE. IN NO EVENT SHALL ALIAS BE LIABLE FOR ANY DAMAGES 
// WHATSOEVER, WHETHER DIRECT, INDIRECT, SPECIAL, OR PUNITIVE, WHETHER IN AN 
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, OR IN EQUITY, 
// ARISING OUT OF ACCESS TO, USE OF, OR RELIANCE UPON THE DATA.
// ==========================================================================
//+

//#include <maya/MPxLocatorNode.h> 
//#include <maya/MString.h> 
//#include <maya/MTypeId.h> 
//#include <maya/MPlug.h>
//#include <maya/MMatrix.h>
//#include <maya/MPoint.h>
//#include <maya/MPointArray.h>
//#include <maya/MDataBlock.h>
//#include <maya/MDataHandle.h>
//#include <maya/MColor.h>
//#include <maya/M3dView.h>
#include <maya/MFnPlugin.h>
//#include <maya/MFnNumericAttribute.h>
//#include <maya/MFnTypedAttribute.h>
//#include <maya/MFnNurbsSurface.h>
//#include <maya/MFnNurbsSurfaceData.h>
//#include <maya/MFnPointArrayData.h>
//#include <maya/MFnMatrixData.h>
//#include <maya/MIOStream.h>

#include "rageBoolBlinddataViewerNode.h"

MStatus initializePlugin( MObject obj )
{ 
	MStatus   status;
	MFnPlugin plugin( obj, "Alias", "3.0", "Any");

	status = plugin.registerNode( "rageBoolBlindDataViewer", rageBoolBlindDataViewer::id, 
						 &rageBoolBlindDataViewer::creator, &rageBoolBlindDataViewer::initialize,
						 MPxNode::kLocatorNode );
	if (!status) 
	{
		status.perror("registerNode");
		return status;
	}
	return status;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   status;
	MFnPlugin plugin( obj );

	status = plugin.deregisterNode( rageBoolBlindDataViewer::id );
	if (!status) 
	{
		status.perror("deregisterNode");
		return status;
	}

	return status;
}
