//-
// ==========================================================================
// Copyright (C) 1995 - 2005 Alias Systems Corp. and/or its licensors.  All 
// rights reserved. 
// 
// The coded instructions, statements, computer programs, and/or related 
// material (collectively the "Data") in these files are provided by Alias 
// Systems Corp. ("Alias") and/or its licensors for the exclusive use of the 
// Customer (as defined in the Alias Software License Agreement that 
// accompanies this Alias software). Such Customer has the right to use, 
// modify, and incorporate the Data into other products and to distribute such 
// products for use by end-users.
//  
// THE DATA IS PROVIDED "AS IS".  ALIAS HEREBY DISCLAIMS ALL WARRANTIES 
// RELATING TO THE DATA, INCLUDING, WITHOUT LIMITATION, ANY AND ALL EXPRESS OR 
// IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND/OR FITNESS FOR A 
// PARTICULAR PURPOSE. IN NO EVENT SHALL ALIAS BE LIABLE FOR ANY DAMAGES 
// WHATSOEVER, WHETHER DIRECT, INDIRECT, SPECIAL, OR PUNITIVE, WHETHER IN AN 
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, OR IN EQUITY, 
// ARISING OUT OF ACCESS TO, USE OF, OR RELIANCE UPON THE DATA.
// ==========================================================================
//+

//#include <maya/MPxLocatorNode.h> 
//#include <maya/MString.h> 
//#include <maya/MTypeId.h> 
//#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MMatrix.h>
//#include <maya/MPoint.h>
#include <maya/MPointArray.h>
//#include <maya/MDataBlock.h>
//#include <maya/MDataHandle.h>
#include <maya/MColor.h>
//#include <maya/M3dView.h>
//#include <maya/MFnPlugin.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNurbsSurface.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnMatrixData.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshVertex.h>

#include "rageBoolBlinddataViewerNode.h"

MTypeId rageBoolBlindDataViewer::id( 0x700D );
MObject rageBoolBlindDataViewer::m_obInputMeshAttribute;
MObject rageBoolBlindDataViewer::m_obVertexLocationsAttribute;
MObject rageBoolBlindDataViewer::m_obPointSizeAttribute;
MObject rageBoolBlindDataViewer::m_obDrawingEnabledAttribute;

rageBoolBlindDataViewer::rageBoolBlindDataViewer() {}
rageBoolBlindDataViewer::~rageBoolBlindDataViewer() {}

MStatus rageBoolBlindDataViewer::compute( const MPlug& plug, MDataBlock& data )
{ 
	MStatus stat;

	// cout << "rageBoolBlindDataViewer::compute\n";

	if( plug == m_obVertexLocationsAttribute )
	{
		MDataHandle inputData = data.inputValue ( m_obInputMeshAttribute, &stat );
		if(!stat)
		{
			stat.perror("rageBoolBlindDataViewer::compute get m_obInputMeshAttribute");
			return stat;
		}

		MObject obMesh = inputData.asMesh();
		MFnMesh obFnMesh(obMesh, &stat);
		if(!stat)
		{
			stat.perror("rageBoolBlindDataViewer::compute mesh creator");
			return stat;
		}

		MDataHandle outputData = data.outputValue ( m_obVertexLocationsAttribute, &stat );
		if(!stat)
		{
			stat.perror("rageBoolBlindDataViewer::compute get m_obVertexLocationsAttribute");
			return stat;
		}

		MObject obVerts = outputData.data();
		MFnPointArrayData obVertData(obVerts, &stat);
		if(!stat)
		{
			stat.perror("rageBoolBlindDataViewer::compute point array data creator");
			return stat;
		}

		MPointArray obVertArray;
		stat = obFnMesh.getPoints( obVertArray, MSpace::kObject);
		if(!stat)
		{
			stat.perror("rageBoolBlindDataViewer::compute getPoints");
			return stat;
		}

		stat = obVertData.set ( obVertArray );
		if(!stat)
		{
			stat.perror("rageBoolBlindDataViewer::compute setVerts");
			return stat;
		}

		outputData.set ( obVerts );

		stat = data.setClean ( plug );
		if(!stat)
		{
			stat.perror("rageBoolBlindDataViewer::compute setClean");
			return stat;
		}
	}
	else
	{
		return MS::kUnknownParameter;
	}

	return MS::kSuccess;
}

void rageBoolBlindDataViewer::draw( M3dView & view, const MDagPath & obDagPath, 
								   M3dView::DisplayStyle style,
								   M3dView::DisplayStatus status )
{ 
	// cout << "rageBoolBlindDataViewer::draw\n";

	MStatus		stat;
	MObject		obThisNode = thisMObject();

	MPlug obDrawingEnabledPlug( obThisNode, m_obDrawingEnabledAttribute );
	bool bDoDrawing; 
	stat = obDrawingEnabledPlug.getValue ( bDoDrawing );
	if(!stat)
	{
		stat.perror("rageBoolBlindDataViewer::draw get m_obDrawingEnabledAttribute");
		return;
	}

	if(!bDoDrawing)
	{
		return;
	}

	MPlug obPointSizePlug( obThisNode, m_obPointSizeAttribute );
	float fPointSize; 
	stat = obPointSizePlug.getValue ( fPointSize );
	if(!stat)
	{
		stat.perror("rageBoolBlindDataViewer::draw get m_obPointSizeAttribute");
		fPointSize = 4.0;
	}

	MPlug obVertexLocationsPlug( obThisNode, m_obVertexLocationsAttribute );
	MObject obVertexLocations;
	stat = obVertexLocationsPlug.getValue(obVertexLocations);
	if(!stat)
	{
		stat.perror("rageBoolBlindDataViewer::draw get obVertexLocations");
		return;
	}

	MFnPointArrayData obVertexLocationsData(obVertexLocations, &stat);
	if(!stat)
	{
		stat.perror("rageBoolBlindDataViewer::draw get point array data");
		return;
	}

	MPointArray obAVertexLocationsArray = obVertexLocationsData.array( &stat );
	if(!stat)
	{
		stat.perror("rageBoolBlindDataViewer::draw get point array");
		return;
	}

	// Extract the 'worldMatrix' attribute that is inherited from 'dagNode'
	//
	MFnDependencyNode obFnThisNode( obThisNode );
	MObject obWorldMatrixObjectAttribute = obFnThisNode.attribute( "worldMatrix" );
	MPlug obWorldMatrixObjectPlug( obThisNode, obWorldMatrixObjectAttribute);

	// 'worldMatrix' is an array so we must specify which element the plug
	// refers to
	obWorldMatrixObjectPlug = obWorldMatrixObjectPlug.elementByLogicalIndex (0);

	// Get the value of the 'worldMatrix' attribute
	//
	MObject obWorldMatrixObject;
	stat = obWorldMatrixObjectPlug.getValue(obWorldMatrixObject);
	if(!stat)
	{
		stat.perror("rageBoolBlindDataViewer::draw get obWorldMatrixObject");
		return;
	}

	MFnMatrixData obFnWorldMatrixData(obWorldMatrixObject, &stat);
	if(!stat)
	{
		stat.perror("rageBoolBlindDataViewer::draw get world matrix data");
		return;
	}

	MMatrix obWorldMatrix = obFnWorldMatrixData.matrix( &stat );
	if(!stat)
	{
		stat.perror("rageBoolBlindDataViewer::draw get world matrix");
		return;
	}

	if( view.isColorIndexMode() )
	{
		cerr << "Can't update obVertPos colors in color index mode\n";
		return;
	}

	// The mesh should be on the other end of my inputMeshAttribute, so follow it and get mesh
	MPlug obInputMeshPlug =  obFnThisNode.findPlug("inputMeshAttribute");

	// Is it connected?
	if(!obInputMeshPlug.isConnected())
	{
		// Needs wiring up
		view.drawText("Connect my inputMeshAttribute to an output mesh attribute", MTransformationMatrix(obWorldMatrix).translation(MSpace::kWorld));
		return;
	}

	// Get connections
	MPlugArray obPlugsOnOtherEndOfConnectionArray;
	obInputMeshPlug.connectedTo(obPlugsOnOtherEndOfConnectionArray, true, false);
    if(obPlugsOnOtherEndOfConnectionArray.length() != 1)
	{
		// Wired up wrong
		view.drawText("inputMeshAttribute is connected wrong", MTransformationMatrix(obWorldMatrix).translation(MSpace::kWorld));
		return;
	}

	// I need to create a MFnMesh node, but if you create one from a MObject the blind data functions do not work
	// So do some evil hacks to get a MDagPath to create it out of instead
	MDagPath obDagPathMeshNode;
	MDagPath::getAPathTo(obPlugsOnOtherEndOfConnectionArray[0].node(), obDagPathMeshNode);
	MFnMesh obFnMeshNode(obDagPathMeshNode);

	// Does the mesh even have blind data?
	if(!obFnMeshNode.hasBlindData(MFn::kMeshVertComponent))
	{
		// No blind data
		view.drawText(obFnMeshNode.name() +" has no blinddata", MTransformationMatrix(obWorldMatrix).translation(MSpace::kWorld));
		return;
	}

	// Does it have bool blind data?
	MIntArray obAIBlindDataTypesOnMesh;
	obFnMeshNode.getBlindDataTypes(MFn::kMeshVertComponent, obAIBlindDataTypesOnMesh);

	// Look for boolean blind data
	MStringArray obAStrBlindDataAttributeLongNames;
	MStringArray obAStrBlindDataAttributeShortNames;
	MStringArray obAStrBlindDataAttributeTypeNames;
	int iBlindDataIdToUse = -1;
	MString obStrBlindDataAttributeLongName;
	MString obStrBlindDataAttributeShortName;
	for(unsigned i=0; i<obAIBlindDataTypesOnMesh.length(); i++)
	{
		obAStrBlindDataAttributeLongNames.clear();
		obAStrBlindDataAttributeShortNames.clear();
		obAStrBlindDataAttributeTypeNames.clear();
		obFnMeshNode.getBlindDataAttrNames(obAIBlindDataTypesOnMesh[i], obAStrBlindDataAttributeLongNames, obAStrBlindDataAttributeShortNames, obAStrBlindDataAttributeTypeNames);

		for(unsigned j=0; j<obAStrBlindDataAttributeTypeNames.length(); j++)
		{
			if(obAStrBlindDataAttributeTypeNames[j] == "boolean")
			{
				// Bingo, found something to draw
				if(iBlindDataIdToUse != -1)
				{
					// Too much blind data to draw, panic and die for now
					view.drawText(obFnMeshNode.name() +" has more than one set of boolean blinddata", MTransformationMatrix(obWorldMatrix).translation(MSpace::kWorld));
					return;
				}
				else
				{
					iBlindDataIdToUse = obAIBlindDataTypesOnMesh[i];
					obStrBlindDataAttributeLongName		=	obAStrBlindDataAttributeLongNames[j];
					obStrBlindDataAttributeShortName	=	obAStrBlindDataAttributeShortNames[j];
				}
			}
		}
	}

	// Ok, I know what blinddata to draw, now actually get the data
	MIntArray obAIVertNosWithBlindData;
	MIntArray obAITheBlindData;
	obFnMeshNode.getBoolBlindData(MFn::kMeshVertComponent, iBlindDataIdToUse, obStrBlindDataAttributeLongName, obAIVertNosWithBlindData, obAITheBlindData);
    
	// On with the drawing

	view.beginGL(); 

	// Push the color settings
	// 
	glPushAttrib( GL_CURRENT_BIT | GL_POINT_BIT );
	glPointSize( fPointSize );
	glDisable ( GL_POINT_SMOOTH );  // Draw square "points"

	glBegin( GL_POINTS );

	// Look for blinddata that needs drawing
	for(unsigned i=0; i<obAIVertNosWithBlindData.length(); i++)
	{
		// Go through verts, drawing as I go
		MPoint	obVertPos(obAVertexLocationsArray[obAIVertNosWithBlindData[i]]);
		int		bBlindData = obAITheBlindData[i];
		if(bBlindData)
		{
			view.setDrawColor(MColor(1.0f, 1.0f, 1.0f));
		}
		else
		{
			view.setDrawColor(MColor(0.0f, 0.0f, 0.0f));
		}
		glVertex3f( (float)obVertPos.x, (float)obVertPos.y, (float)obVertPos.z);
	}

	glEnd();

	glPopAttrib();

	view.endGL();
}

bool rageBoolBlindDataViewer::isBounded() const
{ 
	return false;
}

void* rageBoolBlindDataViewer::creator()
{
	return new rageBoolBlindDataViewer();
}

MStatus rageBoolBlindDataViewer::initialize()
{ 
	MStatus				stat;
	MFnNumericAttribute	numericAttr;
	MFnTypedAttribute	typedAttr;

	m_obDrawingEnabledAttribute = numericAttr.create( "drawingEnabledAttribute", "en", MFnNumericData::kBoolean, 1, &stat );
	if(!stat)
	{
		stat.perror("create m_obDrawingEnabledAttribute attribute");
		return stat;
	}

	m_obPointSizeAttribute = numericAttr.create( "pointSizeAttribute", "ps", MFnNumericData::kFloat, 4.0, &stat );
	if(!stat)
	{
		stat.perror("create m_obPointSizeAttribute attribute");
		return stat;
	}

	m_obInputMeshAttribute = typedAttr.create( "inputMeshAttribute", "is", MFnMeshData::kMesh, &stat);
	if(!stat)
	{
		stat.perror("create m_obInputMeshAttribute attribute");
		return stat;
	}

	m_obVertexLocationsAttribute = typedAttr.create( "vertexLocationsAttribute", "obVertPos", MFnPointArrayData::kPointArray, &stat);
	if(!stat)
	{
		stat.perror("create m_obVertexLocationsAttribute attribute");
		return stat;
	}

	MPointArray			defaultPoints;
	MFnPointArrayData	defaultArray;
	MObject				defaultAttr;

	defaultPoints.clear(); // Empty array
	defaultAttr = defaultArray.create (defaultPoints);
	stat = typedAttr.setDefault(defaultAttr);
	if(!stat)
	{
		stat.perror("could not create default output attribute");
		return stat;
	}

	stat = addAttribute (m_obDrawingEnabledAttribute);
	if(!stat) { stat.perror("addAttribute"); return stat;}
	stat = addAttribute (m_obPointSizeAttribute);
	if(!stat) { stat.perror("addAttribute"); return stat;}
	stat = addAttribute (m_obInputMeshAttribute);
	if(!stat) { stat.perror("addAttribute"); return stat;}
	stat = addAttribute (m_obVertexLocationsAttribute);
	if(!stat) { stat.perror("addAttribute"); return stat;}

	stat = attributeAffects( m_obInputMeshAttribute, m_obVertexLocationsAttribute );
	if(!stat) { stat.perror("attributeAffects"); return stat;}
	stat = attributeAffects( m_obDrawingEnabledAttribute, m_obVertexLocationsAttribute );
	if(!stat) { stat.perror("attributeAffects"); return stat;}
	stat = attributeAffects( m_obPointSizeAttribute, m_obVertexLocationsAttribute );
	if(!stat) { stat.perror("attributeAffects"); return stat;}

	return MS::kSuccess;
}
