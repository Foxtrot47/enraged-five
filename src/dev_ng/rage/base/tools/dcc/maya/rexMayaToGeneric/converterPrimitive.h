// 
// rexMayaToGeneric/converterPrimitive.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REX_MAYAROCKSTAR_CONVERTERPRIMITIVE_H__
#define __REX_MAYAROCKSTAR_CONVERTERPRIMITIVE_H__

#include "atl/array.h"
#include "rexBase/converter.h"
#include "rexGeneric/objectPrimitive.h"

class MDagPath;

namespace rage 
{

//-----------------------------------------------------------------------------

class rexConverterPrimitive : public rexConverter
{
public:
	rexConverterPrimitive() : rexConverter() 
	{};

	virtual rexResult		Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexConverter*   CreateNew() const  { return new rexConverterPrimitive; }

protected:
   
};

//-----------------------------------------------------------------------------

} //End namespace rage

#endif //__REX_MAYAROCKSTAR_CONVERTERPRIMITIVE_H__
