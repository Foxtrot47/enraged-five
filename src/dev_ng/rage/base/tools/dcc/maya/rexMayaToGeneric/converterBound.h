// 
// rexMayaToGeneric/converterBound.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterMeshHierarchyBound
//			 -- converts dag nodes to bound objects
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexGathererMayaRulesBased::Module
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTERBOUND_H__
#define __REX_MAYAROCKSTAR_CONVERTERBOUND_H__

#include "rexGeneric/objectBound.h"
#include "rexMayaToGeneric/converterMesh.h"
#include "vector/matrix34.h"

#include "rexMaya/mayaSetCollection.h"										//TODO: remove me after test

/////////////////////////////////////////////////////////////////////////////////////
namespace rage {

class rexConverterBoundHierarchy : public rexConverterMeshHierarchy
{
public:
	rexConverterBoundHierarchy() : rexConverterMeshHierarchy()  { m_DataRelativeToTaggedRexRoot = false;}

	virtual rexConverter *CreateNew() const  { return new rexConverterBoundHierarchy; }
	
	void	        SetPrefixName(const char *s)  { m_PrefixName = s; }
	const atString	&GetPrefixName() const	        { return m_PrefixName; }

	void SetDataRelativeToTaggedRexRoot(bool b)  { m_DataRelativeToTaggedRexRoot = b; }
	bool GetDataRelativeToTaggedRexRoot() const	        { return m_DataRelativeToTaggedRexRoot; }

protected:
    atString m_PrefixName;

	virtual rexObjectHierarchy *CreateHierarchyNode() const { return new rexObjectGenericBoundHierarchy; }
	virtual bool IsAcceptableHierarchyNode(const MDagPath &dp) const;
	virtual rexResult ConvertMayaNode( const MDagPath &dp, int boneIndex, const Matrix44& boneMatrix, int lodGroupIndex, int lodLevel, float lodThreshold, const atString* groupID, int groupIndex, rexObject *&newObject ) const;

	rexResult rexConverterBoundHierarchy::GetAiMaterialNames(const MDagPath &dp, char *nameBuffer, const int nameBufferSize) const;
	rexResult rexConverterBoundHierarchy::GetAllSetsContainingBound(const MDagPath &dp, rexMayaSetCollection::SetMemberGroup& resultingSets) const;

private:
	bool m_DataRelativeToTaggedRexRoot;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyPrefixBoundName : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyPrefixBoundName; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyDataRelativeToTaggedRexRootBound : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyDataRelativeToTaggedRexRootBound; }
};
} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
