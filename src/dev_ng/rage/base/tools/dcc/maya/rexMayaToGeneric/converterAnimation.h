// 
// rexMayaToGeneric/converterAnimation.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterAnimation
//			 -- gathers all of entity's animation information
//
//		rexConverterAnimationSegment
//			 -- specifies subset of animation data for export
//
//		rexPropertyAnimationSegmentMoverNode
//			 -- allows for setting up mover node
//
//		rexPropertyAnimationSegmentStartFrame
//			 -- allows for setting up animation start frame 
//
//		rexPropertyAnimationSegmentEndFrame
//			 -- allows for setting up animation end frame 
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTANIMATION_H__
#define __REX_MAYAROCKSTAR_CONVERTANIMATION_H__

#include "rexBase/converter.h"
#include "rexBase/property.h"
#include "rexBase/animExportCtrl.h"

#include "rexGeneric/objectAnimation.h"
#include "rexGeneric/objectSkeleton.h"  // contains generic info

#include "atl/array.h"

class MFnAnimCurve;
class MFnDagNode;

namespace rage {

	/////////////////////////////////////////////////////////////////////////////////////

	class rexConverterAnimation : public rexConverter

	{
	public:
		rexConverterAnimation() : rexConverter()  
		{ 
			m_KeyAllFrames = false; 
			m_UseChildrenOfLevelInstanceNodes = false; 
			m_SampleRate = 0.0f; 
			m_CurveFittingTolerance = 0.005f; 
			m_FramesPerSecond = 30.f; 
			m_ChannelGroupCount = 0; 
			m_UseTimeLine = false; 
			m_AnimTransScale = 1.0f;
			m_bAllowMultipleMovers = false;
			m_bUseAnimCtrlExportFile = false;
			m_bApplyMatrixToAnimation = false;
		}

		virtual rexConverter*	CreateNew() const  { rexConverterAnimation* c = new rexConverterAnimation; c->SetChunkNamePrefix( m_ChunkNamePrefix ); c->SetKeyAllFrames( m_KeyAllFrames ); return c; }
		
		void AppendMoverNode( MDagPath& dp) 	   { m_MoverNodes.PushAndGrow(dp); }
		void AppendBlendShapeNode( MDagPath& dp)   { m_BlendShapeNodes.PushAndGrow(dp); }
		void AppendAnimNormalMapNode( MDagPath& dp) { m_AnimNormalMapNodes.PushAndGrow(dp); }
		void AppendAnimExprControlNode( MDagPath& dp) { m_AnimExprCtrlNodes.PushAndGrow(dp); }

		void SetParentNode( MDagPath& dp )		   { m_ParentNode = dp; }

		void SetUseChildrenOfLevelInstanceNodes( bool b )				{ m_UseChildrenOfLevelInstanceNodes = b; }
		bool GetUseChildrenOfLevelInstanceNodes() const					{ return m_UseChildrenOfLevelInstanceNodes; }

		void SetKeyAllFrames( bool b )				{ m_KeyAllFrames = b; }

		void SetUseTimeLine( bool b )				{ m_UseTimeLine = b; }
		bool GetUseTimeLine() const					{ return m_UseTimeLine; }

		void SetChunkNamePrefix( const char * prefix )	{ m_ChunkNamePrefix = prefix; }
		const atString& GetChunkNamePrefix() const			{ return m_ChunkNamePrefix; }

		void	SetTranslateScale( float s )		{ m_AnimTransScale = s; }
		float	GetTranslateScale() const			{ return m_AnimTransScale; }

		void	SetApplyMatrixToAnimation(bool b)	{m_bApplyMatrixToAnimation = b;}
		bool	GetApplyMatrixToAnimation() const	{return m_bApplyMatrixToAnimation;}

		void	SetFramesPerSecond( float fps )		{ m_FramesPerSecond = fps; }
		float	GetFramesPerSecond() const			{ return m_FramesPerSecond; }

		void	SetAllowMultipleMovers( bool b )	{ m_bAllowMultipleMovers = b; }
		bool	GetAllowMultipleMovers() const		{ return m_bAllowMultipleMovers; }

		// For anim curve fitting.
		void	SetSampleRate( float v )			{ m_SampleRate = v; }
		void	SetCurveFittingTolerance( float v )	{ m_CurveFittingTolerance = v; }
		float	GetSampleRate() const				{ return m_SampleRate; }
		float	GetCurveFittingTolerance() const	{ return m_CurveFittingTolerance; }

		int		AddChannelGroup()					{ return m_ChannelGroupCount++; }
		void	AddChannelToWrite( const atString& channelInputName, const atString& channelOutputName, int channelID, bool applyOnlyToRootBone, int channelGroupIndex = -1 )  
		{ m_ChannelInputNames.PushAndGrow( channelInputName ); m_ChannelOutputNames.PushAndGrow( channelOutputName ); m_ChannelSupportedForRootBoneOnly.PushAndGrow( applyOnlyToRootBone ); m_ChannelID.PushAndGrow( channelID ); m_ChannelGroup.PushAndGrow( channelGroupIndex ); }

		bool	GetUseAnimCtrlExportFile() const { return m_bUseAnimCtrlExportFile; }
		void	SetUseAnimCtrlExportFile(bool bUse) { m_bUseAnimCtrlExportFile = bUse; }
		bool	LoadAnimExportCtrlFile(const char* filePath)
		{
			return m_AnimExportCtrlSpec.LoadFromXML(filePath);
		}

		virtual rexResult		Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;

	protected:
		void	GetTrackFrameData(const MDagPath& dp, atArray<rexObjectGenericAnimation::TrackInfo*>& trackArray ) const;
		bool	GetTrackFrameData(const MDagPath& dp, rexObjectGenericAnimation::TrackInfo& track ) const;
		bool	GetTrackFrameData(const MDagPath& dp, rexObjectGenericAnimation::TrackInfo& track, const char *attrName) const;
		
		void	InitTracksInfo(const MDagPath& dp, int nFrames, atArray<rexObjectGenericAnimation::TrackInfo*>& channels, const atArray<atString>& channelNames, const atArray<int>& channelIDs ) const;
		bool	InitTrackInfo(const MDagPath& dp, int nFrames, rexObjectGenericAnimation::TrackInfo& track, const char *attrName) const;
		bool	InitTrackInfo(const MDagPath& dp, int nFrames, rexObjectGenericAnimation::TrackInfo& track) const;

		void	ConvertFromAllFrames( const atArray<MDagPath> &dpList, int sf, int ef, atArray<rexObjectGenericAnimation::ChunkInfo*>& chunkList) const;
		void	ConvertFromFrames(const MDagPath& dp, int sf, int ef, atArray<rexObjectGenericAnimation::TrackInfo*>& channels) const;

		void	ConvertFromAllFrames( const atArray<MDagPath> &dpList, int frameCount, float start, double deltaTime, atArray<rexObjectGenericAnimation::ChunkInfo*>& boneList) const;
		void	ConvertFromFrames(const MDagPath& dp, int frameCount, float start, double deltaTime, atArray<rexObjectGenericAnimation::TrackInfo*>& tracks) const;

		void	ApplyTranslateScale(atArray<rexObjectGenericAnimation::TrackInfo*>& tracks) const;
		void	ApplyTranslateScale(float fScale, atArray<rexObjectGenericAnimation::TrackInfo*>& tracks) const;
		void	ApplyMatrix(const MMatrix& obMatrixToApply, atArray<rexObjectGenericAnimation::TrackInfo*>& tracks) const;

		rexResult	ConvertAnimatedNormalMaps(rexObjectGenericAnimation* pAnimation) const;
		rexResult	ConvertExpressionControlData(rexObjectGenericAnimation* pAnimation) const;

		MDagPath			m_ParentNode;
		atArray<MDagPath>	m_MoverNodes;
		atArray<MDagPath>	m_BlendShapeNodes;
		atArray<MDagPath>	m_AnimNormalMapNodes;
		atArray<MDagPath>	m_AnimExprCtrlNodes;

		bool				m_KeyAllFrames, m_UseChildrenOfLevelInstanceNodes;
		float				m_SampleRate;
		float				m_CurveFittingTolerance;
		atString			m_ChunkNamePrefix;
		bool				m_UseTimeLine;
		float				m_FramesPerSecond;
		float				m_AnimTransScale;
		bool				m_bAllowMultipleMovers;
		bool				m_bApplyMatrixToAnimation;

		atArray<atString>	m_ChannelInputNames, m_ChannelOutputNames;
		atArray<int>		m_ChannelGroup, m_ChannelID;
		int					m_ChannelGroupCount;
		atArray<bool>		m_ChannelSupportedForRootBoneOnly;

		bool				m_bUseAnimCtrlExportFile;
		AnimExportCtrlSpec	m_AnimExportCtrlSpec;
	};


	///////////////////////


	class rexConverterAnimationSegment : public rexConverter
	{
	public:
		rexConverterAnimationSegment() : 
		  m_PrefixSegNameWithFilename(false)
		  {};

		virtual rexConverter*	CreateNew() const  { rexConverterAnimationSegment* c = new rexConverterAnimationSegment; c->SetChunkNamePrefix( m_ChunkNamePrefix ); return c; }

		void SetChunkNamePrefix( const char *prefix )		{ m_ChunkNamePrefix = prefix; }
		const atString& GetChunkNamePrefix() const			{ return m_ChunkNamePrefix; }

		void	SetPrefixSegNameWithFilename( bool b ) { m_PrefixSegNameWithFilename = b; }
		bool	GetPrefixSegNameWithFilename() const	{ return m_PrefixSegNameWithFilename; }

	protected:
		virtual rexResult	Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;

		atString			m_ChunkNamePrefix;
		bool				m_PrefixSegNameWithFilename;
	};

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationMoverNode : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationMoverNode; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationBlendShapeNode : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationBlendShapeNode; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationAnimNormalMapNode : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationAnimNormalMapNode; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationExprControlNode : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationExprControlNode; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationAllowMultipleMovers : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationAllowMultipleMovers; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationParentNode : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationParentNode; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationSegmentStartFrame : public rexProperty
	{
	public:
		virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationSegmentStartFrame; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationSegmentEndFrame : public rexProperty
	{
	public:
		virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationSegmentEndFrame; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationSegmentAutoplay : public rexProperty
	{
	public:
		virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationSegmentAutoplay; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationSegmentLooping : public rexProperty
	{
	public:
		virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationSegmentLooping; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationSegmentDontExportRoot : public rexProperty
	{
	public:
		virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationSegmentDontExportRoot; }
	};
		
	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationSampleRate : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationSampleRate; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationCurveFittingTolerance : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationCurveFittingTolerance; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationTranslateScale : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationTranslateScale; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyApplyMatrixToAnimation : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyApplyMatrixToAnimation; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationPrefixSegNameWithFilename : public rexProperty
	{
		public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationPrefixSegNameWithFilename; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

	class rexPropertyAnimationFramesPerSecond : public rexProperty
	{
	public:
		virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
		virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationFramesPerSecond; }
	};

	/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
