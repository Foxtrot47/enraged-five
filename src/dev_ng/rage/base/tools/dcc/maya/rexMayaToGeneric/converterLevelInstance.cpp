#include "rexGeneric/objectLevelInstance.h"
#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"
#include "converterLevelInstance.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MFnDagNode.h>
#include <Maya/MItDag.h>
#include <Maya/MMatrix.h>
#include <Maya/MString.h>
#pragma warning(pop)

#include <string.h>

using namespace rage;

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterLevelInstance::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& /*defaultName*/ ) const
{
	rexResult result( rexResult::PERFECT );

	rexObjectLevelInstanceGroup* group = new rexObjectLevelInstanceGroup;

	int inputObjectCount = inputObjectArray.GetCount();
	for( int a = 0; a < inputObjectCount; a++  )
	{
		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( inputObjectArray[ a ] );
		if( dpObj )
		{
			MItDag it;
			it.reset( dpObj->m_DagPath );

			bool isObjRoot = true;

			int rootDepth = it.depth();
			int prevDepth = rootDepth;

			rexObjectLevelInstance* rootObject = NULL;
			atArray<rexObjectLevelInstance*> objectStack;

			objectStack.Push( NULL );

//			bool skipDescendants = false;

			rexObjectLevelInstance* currObject = NULL;

			while( (int)it.depth() >= rootDepth && !it.isDone() )
			{
				MDagPath dp; 
				it.getPath( dp );
				MObject obj = dp.node();

				int currDepth = it.depth();
				int depthDelta = currDepth - prevDepth;

				if( currObject && ( obj.apiType() == MFn::kMesh ))
				{
					MDagPath parent( dp );
					parent.pop();
					if( !strstr( dp.fullPathName().asChar(), "bound" ) && !strstr( dp.fullPathName().asChar(), "BOUND" ) && !strstr( dp.fullPathName().asChar(), "Bound" ) )
						currObject->m_MeshNames.Push( rexMayaUtility::DagPathToName( parent ) );
				}
				else //if( !skipDescendants )
				{
					rexValue propGroupTypeV = rexMayaUtility::GetAttributeValue( obj, "RsdPropGroupType" );

					currObject = objectStack.Pop();

					rexObjectLevelInstance* instance = NULL;

					rexObjectLevelInstance::SubType subType = rexObjectLevelInstance::INVALID;

					if( propGroupTypeV.toString() )
					{
						atString propGroupType(propGroupTypeV.toString());

						if( propGroupType == "BangerRootGroup" )
						{
							subType = rexObjectLevelInstance::BANGER_ROOT_GROUP;
						}
						else if( propGroupType == "BangerGroup" )
						{
							subType = rexObjectLevelInstance::BANGER_CHILD;
						}
						else if( propGroupType == "UndamagedGroup" )
						{
							subType = rexObjectLevelInstance::BANGER_UNDAMAGED_GROUP;
						}
						else if( propGroupType == "DamagedGroup" )
						{
							subType = rexObjectLevelInstance::BANGER_DAMAGED_GROUP;
						}
						else if( propGroupType == "BreakableGroup" )
						{
							subType = rexObjectLevelInstance::BANGER_BREAKABLE_GROUP;
						}
						else if( propGroupType == "Instance" )
						{
							subType = rexObjectLevelInstance::INSTANCED_OBJECT;
						}
					}
					else
					{
						// this is for compatibility with current library -- n8 01/29/2004
						if( isObjRoot )
						{
							if( rexMayaUtility::GetAttributeValue( obj, "IsBanger" ).toBool() )
								subType = rexObjectLevelInstance::BANGER_ROOT_GROUP; 
							else
								subType = rexObjectLevelInstance::INSTANCED_OBJECT;
							//skipDescendants = true;
						}
					}

					if( subType != rexObjectLevelInstance::INVALID )
					{
						rexValue origNodeName = rexMayaUtility::GetAttributeValue( obj, "OrigNode" );
						atString name;
						if( origNodeName.toString() )
						{
							name = origNodeName.toString();			
						}
						else
						{
							MFnDagNode fn( dp );
							name = fn.name().asChar(); //rexMayaUtility::DagPathToName( dp );
						}

						instance = new rexObjectLevelInstance( name, subType );

						Matrix44 m = rexMayaUtility::GetSkeletonWorldMatrix( dp );
						rexMayaUtility::GetMatrix34FromMayaMatrix( instance->m_Matrix, dp.inclusiveMatrix() );
						instance->m_Matrix.d.Set( m.d.x, m.d.y, m.d.z );
						instance->m_Matrix.Normalize();

						switch( subType )
						{
						case rexObjectLevelInstance::BANGER_ROOT_GROUP:
							{
								instance->m_Data.m_RootGroupData.m_MinMoveForce = rexMayaUtility::GetAttributeValue( obj, "MinMoveForce" ).toFloat( instance->m_Data.m_RootGroupData.m_MinMoveForce );
								break;
							}
						case rexObjectLevelInstance::BANGER_CHILD:
							{
								instance->m_Data.m_BangerChildData.m_DamagedMass = rexMayaUtility::GetAttributeValue( obj, "DamagedMass" ).toFloat( instance->m_Data.m_BangerChildData.m_DamagedMass );
								instance->m_Data.m_BangerChildData.m_DamageHealth = rexMayaUtility::GetAttributeValue( obj, "DamageHealth" ).toFloat( instance->m_Data.m_BangerChildData.m_DamageHealth );
								instance->m_Data.m_BangerChildData.m_MinDamageForce = rexMayaUtility::GetAttributeValue( obj, "MinDamageForce" ).toFloat( instance->m_Data.m_BangerChildData.m_MinDamageForce );
								instance->m_Data.m_BangerChildData.m_PristineMass = rexMayaUtility::GetAttributeValue( obj, "PristineMass" ).toFloat( instance->m_Data.m_BangerChildData.m_PristineMass );
								break;
							}
						case rexObjectLevelInstance::BANGER_UNDAMAGED_GROUP:
						case rexObjectLevelInstance::BANGER_DAMAGED_GROUP:
						case rexObjectLevelInstance::BANGER_BREAKABLE_GROUP:
							{
								instance->m_Data.m_BangerGroupData.m_BreakHealth = rexMayaUtility::GetAttributeValue( obj, "BreakHealth" ).toFloat( instance->m_Data.m_BangerGroupData.m_BreakHealth );
								instance->m_Data.m_BangerGroupData.m_ForceTransmissionScaleDown = rexMayaUtility::GetAttributeValue( obj, "ForceTransmissionScaleDown" ).toFloat( instance->m_Data.m_BangerGroupData.m_ForceTransmissionScaleDown );
								instance->m_Data.m_BangerGroupData.m_ForceTransmissionScaleUp = rexMayaUtility::GetAttributeValue( obj, "ForceTransmissionScaleUp" ).toFloat( instance->m_Data.m_BangerGroupData.m_ForceTransmissionScaleUp );
								instance->m_Data.m_BangerGroupData.m_MinHitForce = rexMayaUtility::GetAttributeValue( obj, "MinHitForce" ).toFloat( instance->m_Data.m_BangerGroupData.m_MinHitForce );
								instance->m_Data.m_BangerGroupData.m_NonPristineMaxFrameTicks = rexMayaUtility::GetAttributeValue( obj, "NonPristineMaxFrameTicks" ).toInt( instance->m_Data.m_BangerGroupData.m_NonPristineMaxFrameTicks );
								instance->m_Data.m_BangerGroupData.m_ShatterBreakChance = rexMayaUtility::GetAttributeValue( obj, "ShatterBreakChance" ).toFloat( instance->m_Data.m_BangerGroupData.m_ShatterBreakChance );
								break;
							}
						case rexObjectLevelInstance::INSTANCED_OBJECT:
							{
								break;
							}
						default:
							Assert(0);
							break;
						}
					}

					if( !rootObject )
						rootObject = instance;

					// adjust stacks based on traversal depth detla
					if( !depthDelta )
					{
						currObject = objectStack.GetCount() ? objectStack[ objectStack.GetCount() - 1 ] : NULL;
					}
					else if( depthDelta > 0 )
					{
						for( int b = 0; b < depthDelta; b++ )
						{
							objectStack.Push( currObject );
						}				
					}
					else if( depthDelta < 0 )
					{
						for( int b = 0; b < abs( depthDelta ); b++ )
						{
							currObject = objectStack.Pop();
						}
					}		

					rexObjectLevelInstance* parentObject = objectStack.GetCount() ? objectStack[ objectStack.GetCount() - 1 ] : NULL;;

					if( instance )
						currObject = instance;

					objectStack.Push( currObject );

					if( parentObject && instance )
					{
						parentObject->m_ContainedObjects.Push( instance );
					}

					prevDepth = currDepth;
					isObjRoot = false;
				}
				it.next();
			}

			if( rootObject )
				group->m_ContainedObjects.Push( rootObject );
		}
		else
		{
			result.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}		
	}

	outputObject = group;
	
	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////

