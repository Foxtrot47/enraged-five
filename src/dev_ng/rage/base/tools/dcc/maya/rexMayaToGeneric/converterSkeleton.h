// 
// rexMayaToGeneric/converterSkeleton.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterSkeleton
//			 -- collects skeleton information 
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexGathererMayaRulesBased::Module
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTERSKELETON_H__
#define __REX_MAYAROCKSTAR_CONVERTERSKELETON_H__

#include "atl/array.h"
#include "rexBase/converter.h"
#include "rexBase/property.h"
#include "rexGeneric/objectSkeleton.h"

namespace rage {

class rexModule;

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterSkeleton : public rexConverter
{
public:
	rexConverterSkeleton() : rexConverter() { m_UseChildrenOfLevelInstanceNodes = false; m_ErrorOnFoundJointOrientationData = false; m_SpewOnLockedRootBone = false;}

	virtual rexResult			Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexConverter*		CreateNew() const { return new rexConverterSkeleton; }

	void SetUseChildrenOfLevelInstanceNodes( bool b )						{ m_UseChildrenOfLevelInstanceNodes = b; }
	bool GetUseChildrenOfLevelInstanceNodes() const							{ return m_UseChildrenOfLevelInstanceNodes; }

	void SetErrorOnFoundJointOrientationData( bool b )						{ m_ErrorOnFoundJointOrientationData = b; }
	bool GetErrorOnFoundJointOrientationData() const						{ return m_ErrorOnFoundJointOrientationData; }

	void SetSpewOnLockedRootBone( bool b )									{ m_SpewOnLockedRootBone = b; }
	bool GetSpewOnLockedRootBone() const									{ return m_SpewOnLockedRootBone; }
	
protected:
	bool GetChannelInfo( rexObjectGenericSkeleton::Bone::ChannelInfo& channel, MDagPath& dp, const atString& attributeName, bool isRoot ) const;

	bool m_UseChildrenOfLevelInstanceNodes;
	bool m_ErrorOnFoundJointOrientationData;
	bool m_SpewOnLockedRootBone;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertySkeletonSetType : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& object, rexValue value, const atArray<rexObject*>& ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonSetType; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertySkeletonSetJointLimits : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& object, rexValue value, const atArray<rexObject*>& ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonSetJointLimits; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyErrorOnFoundJointOrientationData : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyErrorOnFoundJointOrientationData; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertySpewOnLockedRootBone : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySpewOnLockedRootBone; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
