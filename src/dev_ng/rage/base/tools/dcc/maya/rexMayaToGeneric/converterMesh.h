// 
// rexMayaToGeneric/converterMesh.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterMeshHierarchy
//			 -- base class for items using a mesh hierarchy (bounds, drawables, portals, etc)
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexConverter
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTERMESH_H__
#define __REX_MAYAROCKSTAR_CONVERTERMESH_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "atl/bitSet.h"
#include "rexMayaToGeneric/converterBasic.h"
#include "vector/matrix44.h"

class MObject;

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

class rexConverterMeshHierarchy : public rexConverterHierarchy
{
public:
	rexConverterMeshHierarchy() : rexConverterHierarchy() { m_DataRelativeToTaggedRexRoot = false; m_CheckVertexNumWeights=false; m_ExportBlendShapes=false; m_ExportTriangles = true; }

	void		SetCheckVertexNumWeights( bool b )						{ m_CheckVertexNumWeights = b; }
	bool		GetCheckVertexNumWeights() const						{ return m_CheckVertexNumWeights; }

	void		SetExportBlendShapes( bool b )							{ m_ExportBlendShapes = b; }
	bool		GetExportBlendShapes() const							{ return m_ExportBlendShapes; }

	void		SetExportTriangles(bool b)								{ m_ExportTriangles = b; }
	bool		GetExportTriangles() const								{ return m_ExportTriangles; }

	void SetDataRelativeToTaggedRexRoot(bool b)  { m_DataRelativeToTaggedRexRoot = b; }
	bool GetDataRelativeToTaggedRexRoot() const	        { return m_DataRelativeToTaggedRexRoot; }

	void		AddBlindDataExport(const atString& bdName, rexValue::valueTypes bdType,
									int bdId, const rexValue& bdDefault) 
					{ m_BlindDataNames.PushAndGrow(bdName); 
					  m_BlindDataTypes.PushAndGrow(bdType); 
					  m_BlindDataIds.PushAndGrow(bdId);
					  m_BlindDataDefaults.PushAndGrow(bdDefault); }



protected:
	bool				m_CheckVertexNumWeights;
	bool				m_ExportBlendShapes;
	bool				m_ExportTriangles;
	bool				m_DataRelativeToTaggedRexRoot;

	atArray<atString>				m_BlindDataNames;
	atArray<rexValue::valueTypes>	m_BlindDataTypes;
	atArray<int>					m_BlindDataIds;
	atArray<rexValue>				m_BlindDataDefaults;

	virtual rexResult ConvertSubObject( const rexObject& object, rexObject*& newObject ) const;

	virtual bool IsAcceptableHierarchyNode(const MDagPath &dp) const;
	virtual rexResult ConvertMayaNode( const MDagPath &dp, int boneIndex, const Matrix44& boneMatrix, int lodGroupIndex, int lodLevel, float lodThreshold, const atString* groupID, int groupIndex, rexObject *&newObject ) const;
	
	struct BlendTargetDeltaInfo
	{
		atBitSet	posBlend;
		atBitSet	nrmBlend;
		atBitSet	tanBlend;
		atBitSet	biNrmBlend;
		atBitSet	clrBlend;
	};

	
	rexResult	TagBlendShapeMaterials( const MDagPath &dp, atArray<MObject> &blendTargets, rexObjectGenericMesh *mesh) const;
	rexResult	CollectBlendTargetsFromBlendShape( const MObject &blendShapeMObj, atArray<MObject>& outBlendTargets) const;
	rexResult	CollectBlendShapeVtxDeltaInfo( const MDagPath& dp, atArray<MObject>& targetObjects, atArray<BlendTargetDeltaInfo>& outDeltaInfo ) const;
	bool		IsMayaMeshAllTriangles( const MDagPath &dp ) const;
};


/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyMeshCheckVertexNumWeights : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshCheckVertexNumWeights; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyMeshExportBlendShapes : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshExportBlendShapes; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyMeshBlindDataToExport : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyMeshBlindDataToExport; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyDataRelativeToTaggedRexRootMesh : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyDataRelativeToTaggedRexRootMesh; }
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif 
