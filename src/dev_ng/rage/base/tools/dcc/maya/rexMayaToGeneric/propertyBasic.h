// 
// rexMayaToGeneric/propertyBasic.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexPropertyName
//			 -- allows for setting name for items that are entity components
//
//		rexPropertyIsSingleton
//			 -- determines whether node is instance or parent of a group of instances
//
//		rexPropertyOutputPath
//			 -- allows for setting output path for items that are entity components
//
//		rexPropertyExportData
//			 -- allows for setting whether to export for items that are entity components
//
//		rexPropertyInputNodes
//			 -- allows for setting converters' input nodes 
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_PROPERTYBASIC_H__
#define __REX_MAYAROCKSTAR_PROPERTYBASIC_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "atl/array.h"

#include "rexBase/property.h"
#include "rexBase/result.h"
#include "rexBase/value.h"

/////////////////////

namespace rage {

class rexModule;

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyName : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyName; }
};

/////////////////////////////

class rexPropertyOutputPath : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyOutputPath; }
};

/////////////////////////////

class rexPropertyExportData : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyExportData; }
};

/////////////////////////////

class rexPropertyInputNodes : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyInputNodes; }
};

/////////////////////////////

class rexPropertyBoneTag : public rexProperty
{
public:
	virtual rexResult		SetupShell( const atString& objectName, rexShell& shell, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoneTag; }
};

/////////////////////////////

class rexPropertySpecialFlag : public rexProperty
{
public:
	virtual rexResult		SetupShell( const atString& objectName, rexShell& shell, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySpecialFlag; }
};

/////////////////////////////

class rexPropertyGroupTag : public rexProperty
{
public:
	virtual rexResult		SetupShell( const atString& objectName, rexShell& shell, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyGroupTag; }
};

/////////////////////////////

class rexPropertySkeletonUsesOnlyJoints : public rexProperty
{
public:
	virtual rexResult		SetupShell( const atString& objectName, rexShell& shell, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertySkeletonUsesOnlyJoints; }
};

/////////////////////////////

class rexPropertyUnitConversion : public rexProperty
{
public:
	virtual rexResult		SetupShell( const atString& objectName, rexShell& shell, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyUnitConversion; }
};

/////////////////////////////

class rexPropertyIgnoreNullJoints : public rexProperty
{
public:
	virtual rexResult		SetupShell( const atString& objectName, rexShell& shell, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyIgnoreNullJoints; }
};

/////////////////////////////

class rexPropertyUseBoneIDs : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyUseBoneIDs; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyUseBoundFiltering : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyUseBoundFiltering; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundFilteringBoundTypes : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundFilteringBoundTypes; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
