#include "rexMaya/mayahelp.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include "maya/MDagPath.h"
#include "maya/MFnDagNode.h"
#include "maya/MPxNode.h"
#include "maya/MMatrix.h"
#include "maya/MPlugArray.h"
#include "maya/MTransformationMatrix.h"
#pragma warning(pop)

#include "rexBase/module.h"
#include "rexMaya/mayaUtility.h"

#include "vector/matrix34.h"

#include "converterBound.h"

#include "rexMaya/mayaSetCollection.h"

namespace rage {

// virtual
bool rexConverterBoundHierarchy::IsAcceptableHierarchyNode(const MDagPath &dp) const
{
	MFn::Type nodeType = dp.apiType();

	if(nodeType == MFn::kMesh)
		return true;
	else if( (nodeType == MFn::kPluginLocatorNode) || (nodeType == MFn::kPluginShape) )
	{
		MObject nodeObj = dp.node();
		MFnDagNode node(nodeObj);
		MTypeId type = node.typeId();

		return (type == NODE_RAGE_PHBOUNDBOX || type == NODE_RAGE_PHBOUNDSPHERE || type == NODE_RAGE_PHBOUNDCAPSULE || type == NODE_RAGE_PHBOUNDCYLINDER);
	}
	else
		return false;
}

// virtual
rexResult rexConverterBoundHierarchy::ConvertMayaNode(const MDagPath &dp, int boneIndex, const Matrix44& boneMatrix, int lodGroupIndex, int lodLevel, float lodThreshold, const atString* groupID, int groupIndex, rexObject *&newObject ) const
{
	rexResult retval(rexResult::PERFECT);
	rexObjectGenericBound *bnd = new rexObjectGenericBound();

	if( m_ProgressBarTextCB )
	{
		static char message[1024];
		sprintf( message, "Converting bound data at %s", dp.partialPathName().asChar() );
		(*m_ProgressBarTextCB) ( message );
	}

	MFn::Type nodeType = dp.apiType();

	rexMayaSetCollection::SetMemberGroup setsContainingBound;
	GetAllSetsContainingBound(dp, setsContainingBound);

	switch(nodeType)
	{
	case(MFn::kPluginShape):
	case(MFn::kPluginLocatorNode):
	{
		MObject nodeObj = dp.node();
		MFnDagNode pluginNode(nodeObj);
		MTypeId type = pluginNode.typeId();

        atString meshName = GetPrefixName();
        meshName += rexMayaUtility::DagPathToName( dp );
		bnd->SetMeshName(meshName);

		bnd->m_FullPath = dp.fullPathName().asChar();
		if( groupID )
			bnd->m_GroupID = *groupID;
		bnd->m_GroupIndex = groupIndex;

		bnd->m_BoneIndex = boneIndex;
		bnd->m_ParentBoneMatrix.Identity();

		MDagPath boneDP( dp );
		boneDP.pop();

		MStatus popStatus = MStatus::kSuccess;
		if (GetDataRelativeToTaggedRexRoot())
		{
			MStatus status = MStatus::kSuccess;
			bool isTaggedRexRoot = false;
			while (!isTaggedRexRoot)
			{
				popStatus = boneDP.pop();
				if (popStatus != MStatus::kSuccess)
				{
					break;
				}

				MObject node = boneDP.node(&status);
				if (status == MStatus::kSuccess)
				{
					isTaggedRexRoot = rexMayaUtility::IsTaggedRootRageRexExportDataNode(node);
				}

				if (boneDP.apiType() == MFn::kJoint && popStatus == MStatus::kSuccess)
				{
					break;
				}
			}
		}
		else
		{
			while ((boneDP.apiType() != MFn::kJoint && popStatus == MStatus::kSuccess))
			{
				popStatus = boneDP.pop();
			}
		}

		if (popStatus == MStatus::kSuccess)
		{
			MMatrix boneMat = boneDP.inclusiveMatrix();
			rexMayaUtility::GetMatrix34FromMayaMatrix(bnd->m_ParentBoneMatrix, boneMat);
		}

		if(type == NODE_RAGE_PHBOUNDBOX)
		{
			bnd->m_Type = phBound::BOX;

			MDagPath parent( dp );
			parent.pop();
			Matrix44 m;

			MMatrix mayaParMat = parent.inclusiveMatrix();
			MTransformationMatrix mayaParTMat(mayaParMat);
			
			double scale[3];
			mayaParTMat.getScale(scale, MSpace::kWorld);

			//Check for negative scaling
			if(scale[0] < 0.0f)
			{
				scale[0] = fabs(scale[0]);
				retval.Combine(rexResult::WARNINGS);
				retval.AddMessage("Negative scale found on primitive bound node %s", (const char*)bnd->GetMeshName());
			}
			if(scale[1] < 0.0f)
			{
				scale[1] = fabs(scale[1]);
				retval.Combine(rexResult::WARNINGS);
				retval.AddMessage("Negative scale found on primitive bound node %s", (const char*)bnd->GetMeshName());
			}
			if(scale[2] < 0.0f)
			{
				scale[2] = fabs(scale[2]);
				retval.Combine(rexResult::WARNINGS);
				retval.AddMessage("Negative scale found on primitive bound node %s", (const char*)bnd->GetMeshName());
			}

			bnd->m_Width = (float)scale[0];
			bnd->m_Height = (float)scale[1];
			bnd->m_Depth  = (float)scale[2];

		}
		else if(type == NODE_RAGE_PHBOUNDSPHERE)
		{
			bnd->m_Type = phBound::SPHERE;

			MDagPath parent( dp );
			parent.pop();
			Matrix44 m;

			MMatrix mayaParMat = parent.inclusiveMatrix();
			MTransformationMatrix mayaParTMat(mayaParMat);
			
			double scale[3];
			mayaParTMat.getScale(scale, MSpace::kWorld);

			if(scale[0] < 0.0f)
			{
				scale[0] = fabs(scale[0]);
				retval.Combine(rexResult::WARNINGS);
				retval.AddMessage("Negative scale found on primitive bound node %s", (const char*)bnd->GetMeshName());
			}

			bnd->m_Radius = (float)scale[0];
		}
		else if(type == NODE_RAGE_PHBOUNDCAPSULE)
		{
			bnd->m_Type = phBound::CAPSULE;

			MDagPath parent( dp );
			parent.pop();
			Matrix44 m;

			MMatrix mayaParMat = parent.inclusiveMatrix();
			MTransformationMatrix mayaParTMat(mayaParMat);
			
			double scale[3];
			mayaParTMat.getScale(scale, MSpace::kWorld);

			if(scale[0] < 0.0f)
			{
				scale[0] = fabs(scale[0]);
				retval.Combine(rexResult::WARNINGS);
				retval.AddMessage("Negative scale found on primitive bound node %s", (const char*)bnd->GetMeshName());
			}
			if(scale[1] < 0.0f)
			{
				scale[1] = fabs(scale[1]);
				retval.Combine(rexResult::WARNINGS);
				retval.AddMessage("Negative scale found on primitive bound node %s", (const char*)bnd->GetMeshName());
			}

			bnd->m_Radius = (float)scale[0];
			bnd->m_Height = (float)scale[1];
		}
		else if(type == NODE_RAGE_PHBOUNDCYLINDER)
		{
			bnd->m_Type = phBound::GEOMETRY_CURVED;

			MDagPath parent( dp );
			parent.pop();
			Matrix44 m;

			MMatrix mayaParMat = parent.inclusiveMatrix();
			MTransformationMatrix mayaParTMat(mayaParMat);

			double scale[3];
			mayaParTMat.getScale(scale, MSpace::kWorld);

			if(scale[0] < 0.0f)
			{
				scale[0] = fabs(scale[0]);
				retval.Combine(rexResult::WARNINGS);
				retval.AddMessage("Negative scale found on primitive bound node %s", (const char*)bnd->GetMeshName());
			}
			if(scale[1] < 0.0f)
			{
				scale[1] = fabs(scale[1]);
				retval.Combine(rexResult::WARNINGS);
				retval.AddMessage("Negative scale found on primitive bound node %s", (const char*)bnd->GetMeshName());
			}

			rexValue height = rexMayaUtility::GetAttributeValue( pluginNode.object(), "height" );
			rexValue radius1 = rexMayaUtility::GetAttributeValue( pluginNode.object(), "radius1" );
			rexValue radius2 = rexMayaUtility::GetAttributeValue( pluginNode.object(), "radius2" );
			rexValue capCurve = rexMayaUtility::GetAttributeValue( pluginNode.object(), "capCurve" );
			rexValue sideCurve = rexMayaUtility::GetAttributeValue( pluginNode.object(), "sideCurve" );

			bnd->m_Height = height.toFloat() * (float)scale[1];
			bnd->m_Radius = radius1.toFloat() * (float)scale[0];
			bnd->m_Radius2 = radius2.toFloat() * (float)scale[0];
			bnd->m_CapCurve = capCurve.toFloat();
			bnd->m_SideCurve = sideCurve.toFloat();
		}
		else
		{
			// TODO: write error to log - wil
			delete bnd;
			bnd = NULL;
			retval.Set(rexResult::ERRORS, rexResult::TYPE_INCOMPATIBLE);
			break;
		}

		// set the matrix & center
		if(bnd)
		{
			if( (type == NODE_RAGE_PHBOUNDBOX) || (type == NODE_RAGE_PHBOUNDSPHERE) || (type == NODE_RAGE_PHBOUNDCAPSULE) || (type == NODE_RAGE_PHBOUNDCYLINDER) )
			{
				//New phBound types..
				MDagPath parentDP = dp;
				parentDP.pop();

				MMatrix mayaParMat = parentDP.inclusiveMatrix();
				MTransformationMatrix mayaParTMat(mayaParMat);

				//We need to remove the scale from the bound matrix since it will
				//be applied through the width,height,depth, and radius
				//members of the bound at run-time...
				double unitScale[3] = {1.0, 1.0, 1.0};
				mayaParTMat.setScale(unitScale, MSpace::kWorld);
				rexMayaUtility::GetMatrix34FromMayaMatrix( bnd->m_Matrix, mayaParTMat.asMatrix() );
				
				bnd->m_Center = bnd->m_Matrix.d;
			}

			//Check for extra instance attributes and deal with them accordingly
			//Set any extra attributes that should be exported with the locator node
			int nExtraAttrib = GetInstanceAttrCount();
			if(nExtraAttrib)
			{
				MDagPath parent( dp );
				parent.pop();

				for(int i=0; i<nExtraAttrib; i++)
				{
					const rexConverterInstanceAttr* pConvAttr = GetInstanceAttr(i);
					Assert(pConvAttr);
		
					//If there is a phmaterial attribute set to export on this bound instance,
					//create a material in the bound that has the same name as the attribute value
					if(pConvAttr->m_Name == "phmaterial")
					{
						rexValue attrVal = rexMayaUtility::GetAttributeValue(parent.node(), pConvAttr->m_Name);
						if(attrVal.GetType() != rexValue::UNKNOWN)
						{
							rexObjectGenericMesh::MaterialInfo &mtl = bnd->m_Materials.Grow();
							mtl.m_Name = attrVal.toString();
						}
					}
				}
			}

			//Build up a mapping for each polygon to any set is a member of that has been tagged to a REX export node.
			atMap<int, atString> polyIndexToSetPostfixName;
			atString setPostfixNames;
			rexMayaSetCollection	&setCollection = rexMayaSetCollectionSingleton::GetInstance();
			for(int setIdx=0; setIdx<setsContainingBound.GetCount(); setIdx++)
			{
				rexMayaSetCollection::SetMemberInfo &smi = (setsContainingBound)[setIdx];
			
				//TODO set the post fix name to a comma separated list of the names of the REX Export nodes
				//this set has been tagged to.
				const int		smisetIdx = smi.m_SetIndex;
				const int		rexNodeCount = setCollection.GetNumberOfConnectedREXNodes(smisetIdx);
				for(int rexIdx=0; rexIdx<rexNodeCount; rexIdx++)
				{
					setPostfixNames += ",";
					setPostfixNames += setCollection.GetConnectedREXNodeName(smisetIdx, rexIdx);
				}
			}

			// Check to see if it has a material on it
			MFnDependencyNode obFnMayaNode(dp.node());

			// Am I connected to a shaderEngine?
			MPlugArray aobConnectedPlugs;
			obFnMayaNode.getConnections(aobConnectedPlugs);

			for(unsigned c=0; c<aobConnectedPlugs.length(); c++)
			{
				// Get the plug on the other end of the connection
				MPlugArray aobOtherEndOfConnectedPlug;
				aobConnectedPlugs[c].connectedTo(aobOtherEndOfConnectedPlug, true, true);
				for(unsigned d=0; d<aobOtherEndOfConnectedPlug.length(); d++)
				{
					// Displayf("aobOtherEndOfConnectedPlug[%d].name() = %s", d, aobOtherEndOfConnectedPlug[d].name().asChar());

					// Are any of the nodes on the end of the connections, shader engines?
					MFnDependencyNode obFnConnectedNode(aobOtherEndOfConnectedPlug[d].node());
					// Displayf("obFnConnectedNode.typeId() = %d obFnConnectedNode.typeName() = %s", obFnConnectedNode.typeId(), obFnConnectedNode.typeName().asChar());

					if(obFnConnectedNode.typeName() == "shadingEngine")
					{
						// Bingo!
						// Got a shading engine, now get the material it uses
						MObject obShaderEngine = aobOtherEndOfConnectedPlug[d].node();
						MPlugArray aobConnectedShadingEnginePlugs;
						obFnConnectedNode.getConnections(aobConnectedShadingEnginePlugs);

						for(unsigned e=0; e<aobConnectedShadingEnginePlugs.length(); e++)
						{
							// Displayf("aobConnectedShadingEnginePlugs[%d].name() = %s", e, aobConnectedShadingEnginePlugs[e].name().asChar());

							// Get the plug on the other end of the connection
							MPlugArray aobOtherEndOfConnectedShadingEnginePlug;
							aobConnectedShadingEnginePlugs[e].connectedTo(aobOtherEndOfConnectedShadingEnginePlug, true, false);
							for(unsigned f=0; f<aobOtherEndOfConnectedShadingEnginePlug.length(); f++)
							{
								// Displayf("aobOtherEndOfConnectedShadingEnginePlug[%d].name() = %s", f, aobOtherEndOfConnectedShadingEnginePlug[f].name().asChar());

								// Are any of the nodes on the end of the connections, rageShaderMaya nodes?
								MFnDependencyNode obFnNodeConnectedToShadingEngine(aobOtherEndOfConnectedShadingEnginePlug[f].node());
								// Displayf("obFnNodeConnectedToShadingEngine.typeId() = %d obFnNodeConnectedToShadingEngine.typeName() = %s", obFnNodeConnectedToShadingEngine.typeId(), obFnNodeConnectedToShadingEngine.typeName().asChar());

								if(obFnNodeConnectedToShadingEngine.typeName() == "rageShaderMaya")
								{
									// Bingo!  Found a material!
									MFnDependencyNode obFnRageShaderMayaNode(aobOtherEndOfConnectedShadingEnginePlug[f].node());
									MPlug obPhysicsMaterialPlug = obFnRageShaderMayaNode.findPlug("PhysicsMaterial");
									if(!obPhysicsMaterialPlug.isNull())
									{
										int iMaterialId = 0;
										obPhysicsMaterialPlug.getValue(iMaterialId);
										rexObjectGenericMesh::MaterialInfo &matInfo = bnd->m_Materials.Grow();
										rexMayaUtility::GetMaterialInfo( matInfo, MFnSet(obShaderEngine), setPostfixNames.c_str());
									}
								}
							}

						}
					}
				}
			}
		}

		break;
	}
	case(MFn::kMesh):
	{
		//char	aiMaterialNames[1024];
		//retval = GetAiMaterialNames(dp, aiMaterialNames, sizeof(aiMaterialNames));
		
		bnd->m_Type = phBound::GEOMETRY;
		retval.Combine(rexMayaUtility::ConvertMayaMeshNodeIntoGenericMesh( bnd, dp, boneIndex, &boneMatrix, lodGroupIndex, lodLevel, lodThreshold, groupID, groupIndex, false, true, &setsContainingBound, GetDataRelativeToTaggedRexRoot()));

		//Get the bullet margin if one has been set for the bound
		MDagPath parent( dp );
		parent.pop();
		float margin;
		if(rexMayaUtility::GetBulletMargin(parent.node(), margin))
		{
			bnd->m_Margin = margin;
		}

//		Displayf("Mesh name: >%s<, first material: >%s<", (const char *)(bnd->GetMeshName()), (const char *)(bnd->m_Materials[0].m_Name));

		break;
	}
	default:
	{
		// TODO: write error to log - wil
		delete bnd;
		bnd = NULL;
		retval.Set(rexResult::ERRORS, rexResult::TYPE_INCOMPATIBLE);
		break;
	}
	} // switch(nodeType)

	newObject = bnd;

	return retval;
}

rexResult rexConverterBoundHierarchy::GetAllSetsContainingBound(const MDagPath &dp, rexMayaSetCollection::SetMemberGroup& resultingSets) const
{
	rexResult retVal(rexResult::PERFECT);
	rexMayaSetCollection							&setCollection= rexMayaSetCollectionSingleton::GetInstance();
	rexMayaSetCollection::SetSelectionGroup			filteredSetGroup;

	//const char									*rexNodeNamePrefix= setCollection.GetRexExportDataNodeNamePrefix();
	//atString										aiNamePrefix(rexNodeNamePrefix);
	//aiNamePrefix += "ai";

	filteredSetGroup.Reserve(setCollection.GetTotalNumberOfSets());
	setCollection.FilterSetsByRexExportDataNodeNamePrefix("", filteredSetGroup);
	resultingSets.Reserve(filteredSetGroup.GetCount());

	retVal = setCollection.FindAllSetsContainingObject(dp, filteredSetGroup, resultingSets);

	return retVal;
}

rexResult rexConverterBoundHierarchy::GetAiMaterialNames(const MDagPath &dp, char *nameBuffer, const int nameBufferSize) const
{
	rexResult										retval(rexResult::PERFECT);
	rexMayaSetCollection							&setCollection= rexMayaSetCollectionSingleton::GetInstance();
	rexMayaSetCollection::SetSelectionGroup			setFilter;
	rexMayaSetCollection::SetMemberGroup			resultingSets;
	const char										aiNameDelimiter[]= ",";
	int												delimiterLength= strlen(aiNameDelimiter);
	const char										*rexNodeNamePrefix= setCollection.GetRexExportDataNodeNamePrefix();
	atString										aiNamePrefix(rexNodeNamePrefix);
	aiNamePrefix += "ai";

	setFilter.Reserve(setCollection.GetTotalNumberOfSets());
	setCollection.FilterSetsByRexExportDataNodeNamePrefix(aiNamePrefix, setFilter);
	resultingSets.Reserve(setFilter.GetCount());
	retval= setCollection.FindAllSetsContainingObject(dp, setFilter, resultingSets);

	formatf(nameBuffer, nameBufferSize, "%s", aiNameDelimiter);				//Prefix the ai material name list with the delimiter. The assumption is that there is also a physics material and so a delimiter between the physics material name and the first ai name is needed.
	setCollection.ConcatenateRexExportDataNodeNames(resultingSets, nameBuffer+delimiterLength, nameBufferSize-delimiterLength, strlen(rexNodeNamePrefix), aiNameDelimiter, aiNamePrefix);
	if(nameBuffer[delimiterLength]=='\0')												//If ConcatenateRexExportDataNodeNames() returned an empty string also remove the initial delimiter.
		nameBuffer[0]= '\0';
/*
	//Debug printing (needs #include "maya/MItMeshPolygon.h" line):
	if(resultingSets.GetCount()>0)
	{
		Displayf("%s is (partial) member of:", dp.partialPathName().asChar());

		for(int i=0; i<resultingSets.GetCount(); i++)
		{
			const int			currentSetIndex= resultingSets[i].m_SetIndex;
			MObject				&currentComponentData= resultingSets[i].m_ComponentData;
			const MFnSet		&currentSet= setCollection.GetSet(currentSetIndex);

			Displayf("  %d-%s", currentSetIndex, currentSet.name().asChar());

			if(currentComponentData != MObject::kNullObj)
			{
				MStatus					status;
				MItMeshPolygon			it(dp, currentComponentData, &status);
				Assert(status == MStatus::kSuccess);										//Otherwise it's probably some other type of component we are not handling here.
				for( ; !it.isDone(); it.next())
					Displayf("    poly index: %d", it.index());
			}
		}

		Displayf("- Concatenated AI material names: >%s<", nameBuffer);
	}*/

	return retval;
}


rexResult rexPropertyPrefixBoundName::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& UNUSED_PARAM(objects) ) const
{
	rexConverterBoundHierarchy* conv = dynamic_cast<rexConverterBoundHierarchy*>( module.m_Converter );

	if( !conv )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

    conv->SetPrefixName(value.toString());
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyDataRelativeToTaggedRexRootBound::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& UNUSED_PARAM(objects) ) const
{
	rexResult retval( rexResult::PERFECT );

	rexConverterBoundHierarchy* conv = dynamic_cast<rexConverterBoundHierarchy*>( module.m_Converter );

	if( conv )
	{
		conv->SetDataRelativeToTaggedRexRoot( value.toBool( conv->GetDataRelativeToTaggedRexRoot() ) );
	}
	else
	{
		retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	return retval;
}



} // namespace rage

