// 
// rexMayaToGeneric/converterHair.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#define _BOOL

#include "rexBase/module.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"

#include "rexMayaToGeneric/converterHair.h"


namespace rage
{

rexResult rexConverterHairMeshGroup::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& /*defaultName*/ ) const
{
	rexResult result (rexResult::PERFECT);

	rexObjectGenericHairMeshGroup *pHairGroup = new rexObjectGenericHairMeshGroup();
	outputObject = pHairGroup;

	return result;
}

void rexConverterHairMeshInfo::SetHairMeshNode(MDagPath& dp)
{
	m_HairMesh = dp;
}

rexResult rexConverterHairMeshInfo::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );
	rexObjectGenericHairMeshInfo *pHairInfo = new rexObjectGenericHairMeshInfo();

	pHairInfo->SetName( defaultName );
	pHairInfo->m_HairMeshName = rexMayaUtility::DagPathToName(m_HairMesh);

	outputObject = pHairInfo;

	return result;
}


rexResult rexPropertyHairMeshInfoMesh::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexResult result( rexResult::PERFECT );

	rexConverterHairMeshInfo* conv = dynamic_cast<rexConverterHairMeshInfo*>( module.m_Converter );

	if( !conv )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	if( objects.GetCount() )
	{
		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[ 0 ] );
		if( dpObj )
		{
			atString dbg = rexMayaUtility::DagPathToName(dpObj->m_DagPath);
			conv->SetHairMeshNode( dpObj->m_DagPath );
		}
		else
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	return result;
}

}//namespace rage
