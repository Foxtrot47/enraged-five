#include "converterFragment.h"

#include "rexBase/module.h"
#include "rexGeneric/objectFragment.h"
#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MString.h>
#pragma warning(pop)


namespace rage {

///////////////////////////////////////////////////////////////////////////////////////////
// virtual
bool rexConverterFragmentHierarchy::IsAcceptableHierarchyNode(const MDagPath &UNUSED_PARAM(dp)) const
{
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////
// virtual
rexResult rexConverterFragmentHierarchy::ConvertSubObject(const rexObject& object, rexObject*& newObject) const
{
	rexObjectGenericFragmentHierarchy* fragmentHier = dynamic_cast<rexObjectGenericFragmentHierarchy*>(CreateHierarchyNode());

	const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &object );
	rexResult result( rexResult::PERFECT );

	if( !dpObj )
	{
		result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		return result;
	}		

	if( m_ProgressBarTextCB )
	{
		char message[1024];
		sprintf( message, "Converting fragment data at %s", dpObj->m_DagPath.partialPathName().asChar() );
		(*m_ProgressBarTextCB) ( message );
	}

	const MDagPath& dp = dpObj->m_DagPath;

	if(m_breakableGlassNodes.Find(dp) != -1)
	{
		fragmentHier->SetBreakableGlassFlag(true);
	}

	// Set the glass pane type (if the node is tagged with one)
	fragmentHier->m_GlassType = rexMayaUtility::GetAttributeValueAsString( dp.node(), "glasstype" );

	fragmentHier->SetName(rexMayaUtility::DagPathToName( dp )); // this way, name better matches what artists see in outliner with default settings
	fragmentHier->SetFullPath(dp.fullPathName().asChar()); // this way, name better matches what artists see in outliner with default settings

	newObject = fragmentHier;

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyFragmentBreakableGlassNode::SetupModuleInstance( rexModule& module, rexValue /*value*/, const atArray<rexObject*>& objects ) const
{
	rexConverterFragmentHierarchy* conv = dynamic_cast<rexConverterFragmentHierarchy*>( module.m_Converter );

	if( !conv )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	if( objects.GetCount() )
	{
		for(int i=0; i < objects.GetCount(); i++)
		{
			rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[i] );
			if(dpObj)
			{
				conv->AppendBreakableGlassNode( dpObj->m_DagPath );
			}
			else
			{
				return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
			}
		}
	}

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

} // namespace rage
