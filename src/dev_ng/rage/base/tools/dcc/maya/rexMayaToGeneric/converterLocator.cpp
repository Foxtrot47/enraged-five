#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"
#include "rexGeneric/objectLocator.h"
#include "rexMayaToGeneric/converterLocator.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MFnDagNode.h>
#include <Maya/MMatrix.h>
#include <Maya/MString.h>
#pragma warning(pop)

/////////////////////////
namespace rage {

rexResult rexConverterLocator::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& /*defaultName*/ ) const
{
	rexResult result( rexResult::PERFECT );

	// create object 
	rexObjectGenericLocatorGroup* locatorGroup = CreateNewLocatorGroup();

	int inputObjectCount = inputObjectArray.GetCount();
	
	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );

	for( int a = 0; a < inputObjectCount; a++ )
	{		
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)( a );

		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( inputObjectArray[ a ] );
		
		if( dpObj )
		{
			if( !m_UseChildrenOfLevelInstanceNodes && dpObj->m_ChildOfLevelInstanceNode )
				continue;

			MDagPath& dp = dpObj->m_DagPath;
			
			if( m_ProgressBarTextCB )
			{
				static char message[1024];
				sprintf( message, "Converting locator data at %s", dpObj->m_DagPath.partialPathName().asChar() );
				(*m_ProgressBarTextCB) ( message );
			}
		
			if( dp.apiType() == MFn::kLocator )
			{
				MDagPath parentDP( dp );
				parentDP.pop();
				MFnDagNode fn( parentDP );
				rexObjectGenericLocator* locator = new rexObjectGenericLocator;
				locator->m_BoneIndex = dpObj->m_BoneIndex;
				locator->m_GroupIndex = dpObj->m_GroupIndex;
				locator->SetName( rexMayaUtility::DagPathToName( parentDP ) );

				MStatus status;
				MObject locObj = dp.node();
				MFnDagNode loc( locObj );
				MObject attr;
				MPlug plug;
				Vector3 locPos;
				attr = loc.attribute( "localPositionX", &status );
				plug = loc.findPlug( attr, &status );
				status = plug.getValue( locPos.x );
				attr = loc.attribute( "localPositionY", &status );
				plug = loc.findPlug( attr, &status );
				status = plug.getValue( locPos.y );
				attr = loc.attribute( "localPositionZ", &status );
				plug = loc.findPlug( attr, &status );
				status = plug.getValue( locPos.z );

				Matrix34 m, tm, pm;
				rexMayaUtility::GetMatrix34FromMayaMatrix( m, dp.inclusiveMatrix() );
				m.d.Add( locPos );

				dpObj->m_BoneMatrix.ToMatrix34( pm );
				locator->m_Matrix.DotTranspose( m, pm );

				//Set any extra attributes that should be exported with the locator node
				int nExtraAttrib = GetInstanceAttrCount();
				if(nExtraAttrib)
				{
					for(int i=0; i<nExtraAttrib; i++)
					{
						const rexConverterInstanceAttr* pConvAttr = GetInstanceAttr(i);
						Assert(pConvAttr);

						rexValue attrVal = rexMayaUtility::GetAttributeValue(parentDP.node(), pConvAttr->m_Name);
						if(attrVal.GetType() != rexValue::UNKNOWN)
						{
							locator->m_AttributeNames.PushAndGrow(pConvAttr->m_Name);
							locator->m_AttributeValues.PushAndGrow(attrVal);
						}
					}
				}
				
				locatorGroup->m_ContainedObjects.PushAndGrow( locator,(u16) inputObjectCount );
			}
		}		
	}	

	// set up return values
	outputObject = locatorGroup;
	
	return result;
}

} // namespace rage

/////////////////////////

