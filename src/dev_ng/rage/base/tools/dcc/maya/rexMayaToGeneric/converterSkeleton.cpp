#define REQUIRE_IOSTREAM

#include "rexMaya/mayahelp.h"

#include "rexBase/module.h"
#include "rexGeneric/objectSkeleton.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"
#include "rexMayaToGeneric/converterSkeleton.h"
#include "rexRAGE/serializerSkeleton.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MFnDagNode.h>
#include <Maya/MFnTransform.h>
#include <Maya/MGlobal.h>
#include <Maya/MVector.h>
#pragma warning(pop)

namespace rage {

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterSkeleton::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult retval( rexResult::PERFECT );

	int inputObjectCount = inputObjectArray.GetCount();

	atArray<rexObjectMDagPath*>					boneDPObjArray;
	atArray<rexObjectGenericSkeleton::Bone*>	boneArray;

	rexObjectGenericSkeleton* newSkeleton = new rexObjectGenericSkeleton;
	newSkeleton->SetName( defaultName );
	newSkeleton->m_AuthoredOrientation = GetAuthoredOrientation();

	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );

	// Goto bind pose.
	int result = 0;

	MGlobal::executeCommand("ikSystem -q -sol", result, false);
	MGlobal::executeCommand("ikSystem -e -sol false", false);
	atString savePoseName;
	if( inputObjectCount > 0 )
	{
		rexObjectMDagPath* RJ=dynamic_cast<rexObjectMDagPath*>(inputObjectArray[0]);
		if( RJ && ( RJ->m_DagPath.apiType() == MFn::kJoint ) )
		{
			MString cmd("select -r ");
			cmd += RJ->m_DagPath.partialPathName();
			MGlobal::executeCommand(cmd, false);

			MStringArray hasPoseCmdRes;
			MGlobal::executeCommand("dagPose -q -bp", hasPoseCmdRes, false, true);
			if(hasPoseCmdRes.length() != 0)
			{
				MStringArray poseName;
				MGlobal::executeCommand("dagPose -save -name exportPoseSkel",poseName,false,true);
				savePoseName=poseName[0].asChar();
				MGlobal::executeCommand("gotoBindPose", false);
			}
		}
	}

	for( int a = 0; a < inputObjectCount; a++ )
	{		
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)( a );

		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( inputObjectArray[ a ] );
		if( dpObj )
		{			
			if( m_ProgressBarTextCB )
			{
				static char message[1024];
				sprintf( message, "Converting skeleton data at %s", dpObj->m_DagPath.partialPathName().asChar() );
				(*m_ProgressBarTextCB) ( message );
			}

			MFnDagNode fn( dpObj->m_DagPath );
			MFnTransform fnT( dpObj->m_DagPath );
			//MVector offset = fnT.translation(MSpace::kTransform);

			rexObjectGenericSkeleton::Bone* bone = NULL;

			if( newSkeleton->m_RootBone )
			{
				int boneCount = boneDPObjArray.GetCount();
				Assert( boneArray.GetCount() == boneCount );

				int minPositiveDepthDelta = 0x7FFFFFFF;
				int currDepth = rexMayaUtility::GetDPDepth( dpObj->m_DagPath );
				rexObjectGenericSkeleton::Bone* bestParentBone = NULL;

				for( int b = 0; b < boneCount; b++ )
				{
					if( rexMayaUtility::IsAncestorOf( boneDPObjArray[ b ]->m_DagPath, dpObj->m_DagPath ) )
					{
						int boneDepth = rexMayaUtility::GetDPDepth( boneDPObjArray[ b ]->m_DagPath );
						int depthDelta = currDepth - boneDepth;

						Assert( depthDelta != 0 );

						if( depthDelta > 0 )
						{
							if( depthDelta < minPositiveDepthDelta )
							{
								minPositiveDepthDelta = depthDelta;
								bestParentBone = boneArray[ b ];
							}
						}
					}
				}

				if( !bestParentBone )
					bestParentBone = newSkeleton->m_RootBone;

				Assert( bestParentBone );

				Matrix34 thisMatrix, transposed, ancestorMatrix( bestParentBone->GetBoneMatrix() );
				dpObj->m_BoneMatrix.ToMatrix34( thisMatrix );

				for( rexObjectGenericSkeleton::Bone* currAncestor = bestParentBone->m_Parent; currAncestor; currAncestor = currAncestor->m_Parent )
				{
					Matrix34 t( ancestorMatrix );
					ancestorMatrix.Dot( t, currAncestor->GetBoneMatrix() );
				}

				transposed.DotTranspose( thisMatrix, ancestorMatrix );

				bone = bestParentBone->AddChild( fn.name().asChar(), transposed );
			}
			else
			{				
				bone = new rexObjectGenericSkeleton::Bone;
				newSkeleton->m_RootBone = bone;
				bone->m_Name = fn.name().asChar(); 

				if (fn.isLocked() && GetSpewOnLockedRootBone())
				{
					Warningf("ART_PROBLEM: Adding bone {%s} as a root bone. It seems to be locked which seems to cause problems with animation export.", fn.fullPathName().asChar());
				}

				Matrix34 obTempCopyMatrix;
				dpObj->m_BoneMatrix.ToMatrix34( obTempCopyMatrix );

				// This removes potential scale that can come out of the Maya data
				obTempCopyMatrix.Normalize();
				bone->SetBoneMatrix(obTempCopyMatrix);
			}

			Assert( bone );
			bone->m_BoneTags = dpObj->m_BoneTags;
			bone->m_IsJoint = (dpObj->m_DagPath.apiType() == MFn::kJoint);

			MStatus bidStatus;
			MPlug bidPlug = fn.findPlug("boneID", bidStatus);
			if(!bidStatus.error())
			{
				MString s;
				bidStatus = bidPlug.getValue(s);
				if(!bidStatus.error())
				{
					bone->m_BoneID = s.asChar();
				}
				else
				{
					if(GetGenBoneIdFromFullPath() && (bone != newSkeleton->m_RootBone) )
					{
						//There isn't a boneID override attribute, but the property has been set to
						//use the full path name of the node to create the bone ID
						bone->m_BoneID = rexMayaUtility::FullPathNameToBoneIdString(dpObj->m_DagPath);
					}
				}
			}
			else
			{
				if(GetGenBoneIdFromFullPath() && (bone != newSkeleton->m_RootBone) )
				{
					//There isn't a boneID override attribute, but the property has been set to
					//use the full path name of the node to create the bone ID
					bone->m_BoneID = rexMayaUtility::FullPathNameToBoneIdString(dpObj->m_DagPath);
				}
			}

			// store away current bone's joint orientation:
			MDagPath dpSub(dpObj->m_DagPath);
			if (dpSub.apiType() == MFn::kJoint)
			{
				Matrix34 jointOrientMtx, scaleOrientMtx;
				rexMayaUtility::GetJointOrientMatrices(dpSub, jointOrientMtx, scaleOrientMtx);
				bone->m_JointOrient = jointOrientMtx;
				bone->m_ScaleOrient = scaleOrientMtx;

				if (GetErrorOnFoundJointOrientationData())
				{
					Matrix34 ident;
					ident.Identity();
					if (bone->m_JointOrient.IsNotEqual(ident))
					{
						Warningf("ART_PROBLEM: Joint [%s] has orientation information this has been found to cause display issues. Please remove this information.", dpSub.fullPathName().asChar());
					}

					if (bone->m_ScaleOrient.IsNotEqual(ident))
					{
						Warningf("ART_PROBLEM: Joint [%s] has scale orientation information this has been found to cause display issues. Please remove this information.", dpSub.fullPathName().asChar());
					}
				}
			}

			//bone->m_Offset.x = float(offset.x);
			//bone->m_Offset.y = float(offset.y);
			//bone->m_Offset.z = float(offset.z);

			bone->m_Offset.x = bone->GetBoneMatrix().d.x;
			bone->m_Offset.y = bone->GetBoneMatrix().d.y;
			bone->m_Offset.z = bone->GetBoneMatrix().d.z;

			MObject obj = dpObj->m_DagPath.node();

			atArray< atString > attributes;
			rexMayaUtility::GetAttributeList( obj, attributes );
			int attrCount = attributes.GetCount();

			bool isRoot = (bone == newSkeleton->m_RootBone)?true:false;

			for( int c = 0; c < attrCount; c++ )
			{
				rexObjectGenericSkeleton::Bone::ChannelInfo& channel = bone->m_Channels.Grow((u16) attrCount);
				GetChannelInfo( channel, dpObj->m_DagPath, attributes[ c ], isRoot );
			}

			boneDPObjArray.PushAndGrow( dpObj,(u16) inputObjectCount );
			boneArray.PushAndGrow( bone,(u16) inputObjectCount );
		}
	}

	// fix up bone matrices, offsets etc for joint / scale orients:
	for(int i=0; i<boneArray.GetCount(); i++)
	{
		rexObjectGenericSkeleton::Bone* bone = boneArray[i];

		// undo joint orientations for bones:
		if (bone->m_IsJoint)
		{
			if(GetAuthoredOrientation())
			{
				Matrix34 boneMtx = bone->GetBoneMatrix();

				boneMtx.Dot3x3FromLeft(bone->m_ScaleOrient);
				boneMtx.Dot3x3(bone->m_JointOrient);

				bone->SetBoneMatrix(boneMtx);
			}
			else
			{
				// get orientations from previous bones:
				Matrix34 rotationMatrixTotal;
				rotationMatrixTotal.Identity();

				rexObjectGenericSkeleton::Bone* parent = bone->m_Parent; // don't use the current bone's orientation
				while(parent)
				{
					if( parent->m_IsJoint )
					{
						rotationMatrixTotal.Dot(parent->m_ScaleOrient);
						rotationMatrixTotal.Dot(parent->m_JointOrient);
					}
					parent = parent->m_Parent;
				}

				// bake fixed orientations into offset:
				Vector3 oldOffset = bone->m_Offset;
				rotationMatrixTotal.Transform3x3(oldOffset, bone->m_Offset);

				// modify bone matrix rotation to deal with joint / scale orients:
				Matrix34 parentInverse(rotationMatrixTotal);
				parentInverse.Inverse();
				Matrix34 jointInverse(bone->m_JointOrient);
				jointInverse.Inverse();

				Matrix34 preDot(parentInverse);
				preDot.d.Zero();
				preDot.Dot(jointInverse);

				Matrix34 postDot(bone->m_JointOrient);
				postDot.d.Zero();
				postDot.Dot(rotationMatrixTotal);

				Matrix34 boneMatrix = bone->GetBoneMatrix();
				boneMatrix.Dot3x3FromLeft(preDot);
				boneMatrix.Dot3x3(postDot);
				bone->SetBoneMatrix(boneMatrix);
			}
		}
	}



	outputObject = newSkeleton;

	if( inputObjectCount > 0 && savePoseName.GetLength())
	{
		MString mayaCmd="dagPose -restore ";
		mayaCmd += savePoseName;
		MGlobal::executeCommand(mayaCmd, false);

		// Clean up the dagPose node that was created
		MString delDagPoseCmd;
		delDagPoseCmd = "delete ";
		delDagPoseCmd += savePoseName;
		MGlobal::executeCommand(delDagPoseCmd, false);
	}

	// Restore the IK.
	MString cmd("ikSystem -e -sol ");
	cmd += result;
	MGlobal::executeCommand(cmd, false);

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////

static bool GetChannelLimitType( const atString& attributeName, MFnTransform::LimitType& minType, MFnTransform::LimitType& maxType)
{
	if( attributeName == "translateX" )
	{
		minType = MFnTransform::kTranslateMinX;
		maxType = MFnTransform::kTranslateMaxX;
	}
	else if( attributeName == "translateY" )
	{
		minType = MFnTransform::kTranslateMinY;
		maxType = MFnTransform::kTranslateMaxY;
	}
	else if( attributeName == "translateZ" )
	{
		minType = MFnTransform::kTranslateMinZ;
		maxType = MFnTransform::kTranslateMaxZ;
	}
	else if( attributeName == "rotateX" )
	{
		minType = MFnTransform::kRotateMinX;
		maxType = MFnTransform::kRotateMaxX;
	}
	else if( attributeName == "rotateY" )
	{
		minType = MFnTransform::kRotateMinY;
		maxType = MFnTransform::kRotateMaxY;
	}
	else if( attributeName == "rotateZ" )
	{
		minType = MFnTransform::kRotateMinZ;
		maxType = MFnTransform::kRotateMaxZ;
	}
	else if( attributeName == "scaleX" )
	{
		minType = MFnTransform::kScaleMinX;
		maxType = MFnTransform::kScaleMaxX;
	}
	else if( attributeName == "scaleY" )
	{
		minType = MFnTransform::kScaleMinY;
		maxType = MFnTransform::kScaleMaxY;
	}
	else if( attributeName == "scaleZ" )
	{
		minType = MFnTransform::kScaleMinZ;
		maxType = MFnTransform::kScaleMaxZ;
	}
	else
		return false;

	return true;
}

bool rexConverterSkeleton::GetChannelInfo( rexObjectGenericSkeleton::Bone::ChannelInfo& channel, MDagPath& dp, const atString& attributeName, bool isRoot ) const
{
	MFnTransform fnT( dp );
	MStatus status;
	MPlug plug = fnT.findPlug( MString( attributeName ), &status );

	if( status.error() )
		return false;

	channel.m_IsLocked = ( plug.isLocked() || !plug.isKeyable() );
	if (channel.m_IsLocked && isRoot && GetSpewOnLockedRootBone())
	{
		Warningf("ART_PROBLEM: root bone {%s} has locked channel {%s}. Which seems to cause problems with animation application.", fnT.fullPathName().asChar(), attributeName.c_str());
	}

	channel.m_Name = attributeName;

	MFnTransform::LimitType minType, maxType;
	if( GetChannelLimitType( attributeName, minType, maxType ) )
	{
		channel.m_IsLimited = fnT.isLimited( minType ) || fnT.isLimited( maxType );
		if( channel.m_IsLocked )
		{
			status = plug.getValue( channel.m_Min );
			if( status.error() )
				channel.m_Min = 0.0f;
			channel.m_Max = channel.m_Min;
		}
		else
		{
			channel.m_Min = (float)fnT.limitValue( minType );
			channel.m_Max = (float)fnT.limitValue( maxType );
		}
	}
	else
	{
		channel.m_IsLimited = false;
		channel.m_Min = 0;
		channel.m_Max = 0;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult	rexPropertySkeletonSetType::ModifyObject( rexObject& object, rexValue value, const atArray<rexObject*>& ) const
{
	rexObjectGenericSkeleton* skel = dynamic_cast<rexObjectGenericSkeleton*>( &object );
	if( skel && value.toString() )
	{
		skel->m_SkeletonType = value.toString();
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertySkeletonSetJointLimits::ModifyObject( rexObject& object, rexValue value, const atArray<rexObject*>& ) const
{
	rexObjectGenericSkeleton* skel = dynamic_cast<rexObjectGenericSkeleton*>( &object );
	if( skel && value.toString() )
	{
		skel->m_JointLimits = value.toString();
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyErrorOnFoundJointOrientationData::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterSkeleton* conv = dynamic_cast<rexConverterSkeleton*>( module.m_Converter );
	if( conv )
	{
		conv->SetErrorOnFoundJointOrientationData( value.toBool( conv->GetErrorOnFoundJointOrientationData() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertySpewOnLockedRootBone::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterSkeleton* conv = dynamic_cast<rexConverterSkeleton*>( module.m_Converter );
	if( conv )
	{
		conv->SetSpewOnLockedRootBone( value.toBool( conv->GetSpewOnLockedRootBone() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////
} // namespace rage
