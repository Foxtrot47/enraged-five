// 
// rexMayaToGeneric/exportTexture.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REX_EXPORTER_EXPORT_TEXTURE_H__
#define __REX_EXPORTER_EXPORT_TEXTURE_H__

#include "atl/array.h"
#include "atl/map.h"
#include "atl/string.h"

struct ILinfo;

namespace rage {
class rexTextureExporter 
{
public:
	// PURPOSE: constructor
	// PARAMS: fileName - the file name of the directory to database table name
	rexTextureExporter(const char* fileName);
	virtual	~rexTextureExporter();

	// inlines
	void	SetDBHostName(const atString &name);
	void	SetDBName(const atString &name);
//	void	SetDBDTableName(const atString &name);
	void	SetDBLogin(const atString &name);
	void	SetDBPassword(const atString &name);
	void	SetAddNewTexturesToDatabase( bool b );
	
	void	AddDoubleExportDirectory( const atString& srcDir, const atString& destDir )
	{
		DoubleExportInfo& d = mDoubleExportInfo.Grow();
		d.input = srcDir;
		d.output = destDir;
	}

	bool	Export( const atString& input, const atString& output, bool hasAlpha, bool ps2LightMap );	
	bool	QueryDB( const atString& srcImg, atArray<atString>& fieldNames, atArray<atString>& fieldValues );

	void	SetDefaultColorDepth( int format )  { mDefaultColorDepth = format; }

	enum	Platform
	{
		PLAYSTATION,
		XBOX,
		NINTENDO,
		PC
	};
	enum  { NUM_PLATFORMS = 4 };
	
	void	SetPlatform(Platform platform)  { mPlatform = (int)platform; }
	

private:
	void	Reset();

	bool	LoadSourceImage(int& depth,int& width,int& height);

	bool	ModifyRow(const char* imgSrc, const char* imgDest, bool insertRow=false);
	bool	ModifyMDB();

	bool	SetupFromRecord();
	bool	SetupFromCaption( int defaultColorDepth );
	int		GenerateChecksum();

	bool	ExportNow( const atString& input, const atString& output, bool hasAlpha, bool ps2LightMap );	


	int		mPlatform;
	atString  mInputFilename, mOutputFilename;

	atString	mDBHostName, mDBName, mDBTableName, mDBLogin, mDBPassword;
	atString	mQuantizerType;
	int		mFrames;
	int		mSrcResolutionX, mSrcResolutionY;
	bool	mClampS;
	bool	mClampT;
	atString	mType;
	double  mTimestamp;
	int		mChecksum;

	struct DoubleExportInfo
	{
		atString input;
		atString output;
	};

	struct SourceDirectoryInfo
	{
		atString srcDir;
		atString tableName;
	};

	atArray<DoubleExportInfo> mDoubleExportInfo;
	static atArray<SourceDirectoryInfo> smSourceDirectoryInfo;
	
	struct PlatformInfo
	{
		int			mResolutionX, mResolutionY;
		atString	mFormatType;
		int			mFrameSkip;
		int			mMips;
	};

	PlatformInfo mPlatformInfo[NUM_PLATFORMS];

	bool	mAddNewTexturesToDatabase;
	bool	mIfSourceResolutionChangedAskToUpdate;
	bool	mUsePathValidationFiles;

	bool    mExportAsPS2LightMap;
	int		mDefaultColorDepth;
	
	struct DBTableMap
	{
		atString basePath;
		atString tableName;
	};

	atArray< DBTableMap > mDBTableMaps;
};

inline void rexTextureExporter::SetDBHostName(const atString &name)
{ mDBHostName = name; }
inline void rexTextureExporter::SetDBName(const atString &name)
{ mDBName = name; }
//inline void rexTextureExporter::SetDBTableName(const atString &name)
//{ mDBTableName = name; }
inline void rexTextureExporter::SetDBLogin(const atString &name)
{ mDBLogin = name; }
inline void rexTextureExporter::SetDBPassword(const atString &name)
{ mDBPassword = name; }
inline void rexTextureExporter::SetAddNewTexturesToDatabase(bool value)
{ mAddNewTexturesToDatabase = value; }

} // namespace rage

#endif

