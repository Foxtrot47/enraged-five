// 
// rexMayaToGeneric/filterBoundHierarchy.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexFilterBoundHierarchy -- filter that accepts meshes, transforms, LOD groups, and joints
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYA_ROCKSTAR_FILTERBOUNDHIERARCHY_H__
#define __REX_MAYA_ROCKSTAR_FILTERBOUNDHIERARCHY_H__

/////////////////////////////////////////////////////////////////////////////////////
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MFnDagNode.h>
#pragma warning(pop)

#include "rexBase/filter.h"
#include "rexMaya/objectMDagPath.h"



namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexFilterBoundHierarchy : public rexFilter
{
public:
	virtual bool AcceptObject( const rexObject& inputObject ) const 
	{ 
		const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &inputObject );
		if( !dpObj )
			return false;

		MFn::Type nodeType = dpObj->m_DagPath.apiType();

		bool good = ( nodeType == MFn::kMesh || nodeType == MFn::kLodGroup ||
				 nodeType == MFn::kTransform || nodeType == MFn::kJoint );

		if(!good && (nodeType == MFn::kPluginLocatorNode || nodeType == MFn::kPluginShape) )
		{
			MObject nodeObj = dpObj->m_DagPath.node();
			MFnDagNode node(nodeObj);
			MTypeId type = node.typeId();

			good = (type == NODE_RAGE_PHBOUNDBOX || type == NODE_RAGE_PHBOUNDSPHERE || type == NODE_RAGE_PHBOUNDCAPSULE || type == NODE_RAGE_PHBOUNDCYLINDER);
		}

		return good;
	}

	virtual rexFilter*		CreateNew() const { return new rexFilterBoundHierarchy; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // #ifndef __REX_MAYA_ROCKSTAR_FILTERBOUNDHIERARCHY_H__
