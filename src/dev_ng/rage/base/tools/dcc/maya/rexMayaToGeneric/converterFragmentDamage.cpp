#include "converterFragmentDamage.h"

#include "rexGeneric/objectFragment.h"
#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MString.h>
#pragma warning(pop)

///////////////////////////////////////////////////////////////////////////////////////////
namespace rage {

// virtual
bool rexConverterFragmentDamageHierarchy::IsAcceptableHierarchyNode(const MDagPath &UNUSED_PARAM(dp)) const
{
	return true;
}

// virtual
rexResult rexConverterFragmentDamageHierarchy::ConvertSubObject(const rexObject& object, rexObject*& newObject) const
{
	rexObjectGenericFragmentHierarchy* fragmentHier = dynamic_cast<rexObjectGenericFragmentHierarchy*>(CreateHierarchyNode());

	const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &object );
	rexResult result( rexResult::PERFECT );

	if( !dpObj )
	{
		result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		return result;
	}		

	if( m_ProgressBarTextCB )
	{
		char message[1024];
		sprintf( message, "Converting fragment data at %s", dpObj->m_DagPath.partialPathName().asChar() );
		(*m_ProgressBarTextCB) ( message );
	}

	const MDagPath& dp = dpObj->m_DagPath;

	// Set the glass pane type (if the node is tagged with one)
	fragmentHier->m_GlassType = rexMayaUtility::GetAttributeValueAsString( dp.node(), "glasstype" );

	fragmentHier->SetName(rexMayaUtility::DagPathToName( dp )); // this way, name better matches what artists see in outliner with default settings
	fragmentHier->SetFullPath(dp.fullPathName().asChar()); // this way, name better matches what artists see in outliner with default settings

	newObject = fragmentHier;

	return result;
}

} // namespace rage
