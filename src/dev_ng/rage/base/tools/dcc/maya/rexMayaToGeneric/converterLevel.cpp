#include "converterLevel.h"

#include "rexGeneric/objectLevel.h"
#include "rexMaya/mayahelp.h"
#include "rexMaya/objectMDagPath.h"
#include "rexMaya/mayaUtility.h"

using namespace rage;

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterRmworldDistrict::Convert( const atArray<rexObject*>&, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	rexObjectRmworldDistrict* level = new rexObjectRmworldDistrict;
	level->SetName( defaultName );

	outputObject = level;
	
	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterRoomTypes::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	rexObjectRoomType* roomType = new rexObjectRoomType;
	roomType->m_Name = defaultName;

	outputObject = roomType;
	
	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyRoomSetAmbientSoundName::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectRoomType* roomType = dynamic_cast<rexObjectRoomType*>( &obj );

	if( !roomType )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	roomType->m_AmbientSoundName = value.toString();

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyRoomSetAmbientSoundVolume::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectRoomType* roomType = dynamic_cast<rexObjectRoomType*>( &obj );

	if( !roomType )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	roomType->m_AmbientSoundVolume = value.toFloat( roomType->m_AmbientSoundVolume );

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyRoomSetReverbType::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectRoomType* roomType = dynamic_cast<rexObjectRoomType*>( &obj );

	if( !roomType )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	roomType->m_ReverbType = value.toInt( roomType->m_ReverbType );

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyRoomSetReverbVolume::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectRoomType* roomType = dynamic_cast<rexObjectRoomType*>( &obj );

	if( !roomType )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	roomType->m_ReverbVolume = value.toFloat( roomType->m_ReverbVolume );

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyRoomSetUserData0::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectRoomType* roomType = dynamic_cast<rexObjectRoomType*>( &obj );

	if( !roomType )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	roomType->m_UserData0 = value.toFloat( roomType->m_UserData0 );

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyRoomSetUserData1::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectRoomType* roomType = dynamic_cast<rexObjectRoomType*>( &obj );

	if( !roomType )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	roomType->m_UserData1 = value.toFloat( roomType->m_UserData1 );

	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterRmworldDistrictProxyMeshHierarchy::ConvertMayaNode(const MDagPath &dp, int boneIndex, const Matrix44& boneMatrix, int lodGroupIndex, int lodLevel, float lodThreshold, const atString* groupID, int groupIndex, rexObject *&newObject) const
{
	newObject = NULL;

	rexObjectGenericMesh *mesh = new rexObjectGenericMesh();
	if(!mesh)
		return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);

	rexResult retval;

	if( (retval = rexMayaUtility::ConvertMayaMeshNodeIntoGenericMesh( mesh, dp, boneIndex, &boneMatrix, lodGroupIndex, lodLevel, lodThreshold, groupID, groupIndex, false )).Errors() )
	{
		delete mesh;
		return retval;
	}

	newObject = mesh;

	return retval;
}

///////////////////////////////////////////////////////////////////////////////////////////

