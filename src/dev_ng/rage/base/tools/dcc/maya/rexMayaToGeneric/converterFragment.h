// 
// rexMayaToGeneric/converterFragment.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterFragment
//			 -- converts fragment type information
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTERFRAGMENT_H__
#define __REX_MAYAROCKSTAR_CONVERTERFRAGMENT_H__

#include "converterBasic.h"
#include "rexGeneric/objectFragment.h"

/////////////////////////////////////////////////////////////////////////////////////
namespace rage {

class rexConverterFragmentHierarchy : public rexConverterHierarchy
{
public:
	virtual rexConverter *CreateNew() const  { return new rexConverterFragmentHierarchy; }
	
	void AppendBreakableGlassNode( MDagPath& dp) { m_breakableGlassNodes.PushAndGrow(dp); }

protected:
	virtual rexObjectHierarchy *CreateHierarchyNode() const { return new rexObjectGenericFragmentHierarchy(false); }
	virtual rexResult			ConvertSubObject( const rexObject& object, rexObject*& newObject ) const;
	virtual bool IsAcceptableHierarchyNode(const MDagPath &dp) const;
	
protected:
	atArray<MDagPath>	m_breakableGlassNodes;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyFragmentBreakableGlassNode : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyFragmentBreakableGlassNode; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
