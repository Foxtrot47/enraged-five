// 
// rexMayaToGeneric/converterBasic.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterHierarchy
//			 -- base class for items using a hierarchy
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTERBASIC_H__
#define __REX_MAYAROCKSTAR_CONVERTERBASIC_H__

#include "atl/array.h"

#include "rexBase/converter.h"
#include "rexBase/property.h"
#include "rexBase/result.h"
#include "rexBase/value.h"

class MDagPath;

/////////////////
namespace rage {

class rexObjectHierarchy;

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterHierarchy : public rexConverter
{
protected:
	virtual rexResult			Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	
	virtual rexResult			ConvertSubObject( const rexObject& object, rexObject*& newObject ) const = 0;
	
	virtual bool				IsAcceptableHierarchyNode(const MDagPath &dp) const = 0;
	virtual rexObjectHierarchy *CreateHierarchyNode() const = 0;
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage


#endif
