#include "rexMaya/mayahelp.h"
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MGlobal.h>
#include <Maya/MString.h>
#include <Maya/MFnDagNode.h>
#include <Maya/MMatrix.h>
#include <Maya/MFnAttribute.h>
#include <Maya/MFnCompoundAttribute.h>
#include <Maya/MFnTypedAttribute.h>
#include <Maya/MFnMesh.h>
#include <Maya/MFnBlendShapeDeformer.h>
#include <Maya/MPointArray.h>
#include <Maya/MFloatVectorArray.h>
#include <Maya/MFnTransform.h>
#include <Maya/MItDependencyGraph.h>
#include <Maya/MFloatPoint.h>
#include <Maya/MItMeshPolygon.h>
#include <Maya/MFnSkinCluster.h>
#include <Maya/MDagPathArray.h>
#pragma warning(pop)

#include "rexBase/module.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"

#include "rexMayaToGeneric/converterMesh.h"

#include <string>

///////////////////////////////////////////////////////////////////////////////////////////

#define BLEND_DELTA_THRESHOLD 0.00001

///////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

// virtual
rexResult rexConverterMeshHierarchy::ConvertSubObject( const rexObject& object, rexObject*& newObject ) const
{
	rexResult result( rexResult::PERFECT );
	const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &object );

	if( !dpObj )
	{
		result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		return result;
	}		

	if( m_ProgressBarTextCB )
	{
		static char message[1024];
		sprintf( message, "Converting mesh data at %s", dpObj->m_DagPath.partialPathName().asChar() );
		(*m_ProgressBarTextCB) ( message );
	}

	const MDagPath& dp = dpObj->m_DagPath;
	MFnDagNode fnDag( dp );

	if( fnDag.isIntermediateObject() )
		return result;

	bool isLOD = ( dp.apiType() == MFn::kLodGroup );

	if( isLOD || ( dp.apiType() == MFn::kTransform ) || ( dp.apiType() == MFn::kJoint ) )
	{
		rexObjectGenericMeshHierarchy* meshHier = dynamic_cast<rexObjectGenericMeshHierarchy*>(CreateHierarchyNode());
		meshHier->SetName( dp.partialPathName().asChar() );
		meshHier->SetFullPath( dp.fullPathName().asChar() );
		rexMayaUtility::GetMatrix44FromMayaMatrix( meshHier->m_Matrix, dp.inclusiveMatrix() );
		if( isLOD )
		{
			MFnDagNode fnLod(dp);
			MStatus s;
			MPlug plug = fnLod.findPlug( "threshold", &s );
			
			if( !s.error() && plug.isArray() )
			{
				int thresholdCount = plug.evaluateNumElements();
				meshHier->m_IsLOD = true;
				for( int a = 0; a < thresholdCount; a++ )
				{
					MPlug childPlug = plug[ a ];

					double threshold = 9999.0;
					childPlug.getValue( threshold );
					if( threshold < 0.0f )
						threshold = fabs( threshold );
					meshHier->m_LODThresholds.PushAndGrow( (float)threshold, (u16) thresholdCount );
				}
			}

			atArray<MDagPath> pathStack;
            MDagPath ancestor( dp );
			ancestor.pop();
			while( ancestor.fullPathName().length() > 1 )
			{
				pathStack.PushAndGrow( ancestor );
				if( ancestor.hasFn( MFn::kLodGroup ) )
				{
					break;
				}
				ancestor.pop();
			}
			
			if( pathStack.GetCount() > 1 )
			{
				ancestor = pathStack.Pop();
				MDagPath child = pathStack.Pop();

				int childCount = ancestor.childCount();
				for( int c = 0; c < childCount; c++ )
				{				
					if( ancestor.child( c ) == child.node() )
					{
						meshHier->m_ChildIndex = c;
						break;
					}
				}
			}
			
			while( ancestor.fullPathName().length() > 1 )
			{
				if( ancestor.hasFn( MFn::kLodGroup ) )
					meshHier->m_LODDepth++;
				ancestor.pop();
			}
		}
		int childCount = object.m_ContainedObjects.GetCount();
		for( int a = 0; a < childCount; a++ )
		{
			rexObject* subObject;
			rexResult thisresult = ConvertSubObject( *object.m_ContainedObjects[ a ], subObject );
			result &= thisresult;
			if( thisresult.OK() && subObject )
			{
				meshHier->m_ContainedObjects.PushAndGrow( subObject, (u16) childCount );
			}		
		}
		newObject = meshHier;
	}
	else if( IsAcceptableHierarchyNode( dp ) )
	{
		result &= ConvertMayaNode( dp, dpObj->m_BoneIndex, dpObj->m_BoneMatrix, dpObj->m_LODGroupIndex, dpObj->m_LODLevel, dpObj->m_LODThreshold, &dpObj->m_GroupID, dpObj->m_GroupIndex, newObject );
		rexObjectGenericMesh* mesh = dynamic_cast< rexObjectGenericMesh* >( newObject );
		if( mesh )
		{
			if( dpObj->m_ChildOfLevelInstanceNode )
			{
				// so banger hierarchies don't cause fucked up huge filenames -- n8
				MFnDagNode fn( dpObj->m_DagPath );
				mesh->SetMeshName(fn.name().asChar());
			}

			mesh->m_SpecialFlags = dpObj->m_SpecialFlags;
			mesh->m_ChildOfLevelInstanceNode = dpObj->m_ChildOfLevelInstanceNode;
		}
	}
	else
	{
		result.Set( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////

// virtual
bool rexConverterMeshHierarchy::IsAcceptableHierarchyNode(const MDagPath &dp) const
{
	return (dp.apiType() == MFn::kMesh);
}

///////////////////////////////////////////////////////////////////////////////////////////

// virtual
rexResult rexConverterMeshHierarchy::ConvertMayaNode(const MDagPath &dp, int boneIndex, const Matrix44& boneMatrix, int lodGroupIndex, int lodLevel, float lodThreshold, const atString* groupID, int groupIndex, rexObject *&newObject) const
{
//	Displayf("rexResult rexConverterMeshHierarchy::ConvertMayaNode(\"%s\", %d, const Matrix44& boneMatrix, %d, %d, %f, \"%s\", %d, %p) const", dp.fullPathName().asChar(), boneIndex, lodGroupIndex, lodLevel, lodThreshold, *groupID, groupIndex, newObject);
	newObject = NULL;

	rexObjectGenericMesh *mesh = new rexObjectGenericMesh();
	if(!mesh)
		return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);

	rexResult retval;
	retval = rexMayaUtility::ConvertMayaMeshNodeIntoGenericMesh( mesh, dp, boneIndex, &boneMatrix, lodGroupIndex, lodLevel, lodThreshold, groupID, groupIndex, GetCheckVertexNumWeights(), GetExportTriangles() , NULL, GetDataRelativeToTaggedRexRoot());
	
	if( retval.Errors() )
	{
		delete mesh;
		return retval;
	}
	else if( mesh->m_Materials.GetCount() == 0 )
	{
		retval.Combine( rexResult::WARNINGS, rexResult::INVALID_INPUT );
		retval.AddMessage( "Mesh at %s has no materials. Will not be exported!", dp.partialPathName().asChar() );	
		delete mesh;
		return retval;
	}

	//BEGIN TEMPORARY TEST CODE
	rexValue volumeId = rexMayaUtility::GetAttributeValue(dp.node(), "roomvolumeidx");
	if(!volumeId.isNull() && !volumeId.isUnknown())
	{
		mesh->m_GroupIndex = volumeId.toInt();
	}
	//END - TEMPORARY TEST CODE...

	//the dp provides the shape we want the attribute on its parent so get it.
	//Displayf("%s",dp.fullPathName().asChar());
	MDagPath parent = dp;
	parent.pop();
	Displayf("%s",parent.fullPathName().asChar());
	rexValue rPri = rexMayaUtility::GetAttributeValue(parent.node(), "renderpriority");
	if(!rPri.isNull() && !rPri.isUnknown())
	{
		mesh->m_RenderPriority = rPri.toInt();
	}

	//Do we need to extract vertex based blind data from the mesh?
	MStatus status;
	MFnMesh mayaMesh(dp);

	// Check to see if we are in a bakeset and add the name of the bake set to the 
	char command[2048];
	memset(command, 0, 2048);

	MStringArray bakeSet;
	sprintf(command, "listConnections -type textureBakeSet %s", mayaMesh.fullPathName().asChar());
	MGlobal::executeCommand(command, bakeSet, false);

	if (bakeSet.length() > 0)
	{
		MString path;
		sprintf(command, "getAttr %s.LightMapPath", bakeSet[0].asChar());
		MGlobal::executeCommand(command, path, false);

		MString prefix;
		sprintf(command, "getAttr %s.prefix", bakeSet[0].asChar());
		MGlobal::executeCommand(command, prefix, false);

		for (int i=0; i<mesh->m_Materials.GetCount(); i++ )
		{
			mesh->m_Materials[i].SetLightmapFileNames(prefix.asChar(), path.asChar());
		}
	}

	bool hasBlindData = mayaMesh.hasBlindData(MFn::kMeshVertComponent, &status);
	int blindDataChnCount = m_BlindDataNames.GetCount();

	//If blind data channels have been specified for export, and the mesh actually has blind
	//data associated with it, export it..
	if( blindDataChnCount && hasBlindData )
	{
		//Store the blind data channel IDs and types in the generic mesh
		for(int i=0; i<m_BlindDataIds.GetCount(); i++)
		{
			mesh->m_BlindDataIds.PushAndGrow(m_BlindDataIds[i]);
			mesh->m_BlindDataTypes.PushAndGrow(m_BlindDataTypes[i]);
		}
	
		//Build up an array parallel to the blindDataIds array that maps
		//the maya blind data id to the specified rage blind data channel id. 
		//This is done by matching the long name of the maya attribute for the blind data
		//with the names specified in the rule file for each rage blind channel id.
		atArray<int> gnrcVertBlindIds;
		gnrcVertBlindIds.Resize(blindDataChnCount);

		for(int a=0; a<blindDataChnCount; a++)
			gnrcVertBlindIds[a] = -1;

		MIntArray blindDataIds;
		status = mayaMesh.getBlindDataTypes(MFn::kMeshVertComponent, blindDataIds);
		unsigned int mayaBlindIDCount = blindDataIds.length();

		for(unsigned int i=0; i<mayaBlindIDCount; i++)
		{
			MStringArray attrShortNames;
			MStringArray attrLongNames;
			MStringArray attrTypes;

			status = mayaMesh.getBlindDataAttrNames(blindDataIds[i], attrLongNames, attrShortNames, attrTypes);
			
			int attrCount = attrLongNames.length();
			for(int j=0; j<attrCount; j++)
			{
				atString mayaBlindDataName(attrLongNames[j].asChar());
				int findIdx = m_BlindDataNames.Find(mayaBlindDataName);
				if(findIdx != -1)
				{
					gnrcVertBlindIds[findIdx] = blindDataIds[i];	
				}
			}
		}
		
		//Move through each vertex in the mesh, and gather the blind data values.
		unsigned int numVerts = mayaMesh.numVertices();
		for(unsigned int i=0; i<numVerts;i++)
		{
			rexObjectGenericMesh::VertexInfo& gnrcVI = mesh->m_Vertices[i];
			gnrcVI.m_BlindData.Resize(blindDataChnCount);

			for(int blindIdx=0; blindIdx<blindDataChnCount; blindIdx++)
			{
				if( (gnrcVertBlindIds[blindIdx] != -1) &&
					(mayaMesh.hasBlindData(i, MFn::kMeshVertComponent, gnrcVertBlindIds[blindIdx])))
				{
					rexValue blindValue( m_BlindDataTypes[blindIdx] );

					//There is a blind data value specified for this vertex in maya...
					MStringArray attrShortNames;
					MStringArray attrLongNames;
					MStringArray attrTypes;
					status = mayaMesh.getBlindDataAttrNames(gnrcVertBlindIds[blindIdx], attrLongNames, attrShortNames, attrTypes);

					atString dbgType(attrTypes[0].asChar());
					if(attrTypes[0] == "double")
					{
						double mayaBlindVal;
						status = mayaMesh.getDoubleBlindData(i, MFn::kMeshVertComponent, gnrcVertBlindIds[blindIdx], 
															MString((const char*)m_BlindDataNames[blindIdx]), mayaBlindVal);
						blindValue = (float)mayaBlindVal;
					}
					else if(attrTypes[0] == "float")
					{
						float mayaBlindVal;
						status = mayaMesh.getFloatBlindData(i, MFn::kMeshVertComponent, gnrcVertBlindIds[blindIdx],
															MString((const char*)m_BlindDataNames[blindIdx]), mayaBlindVal);
						blindValue = mayaBlindVal;
					}
					else if(attrTypes[0] == "int")
					{
						int mayaBlindVal;
						status = mayaMesh.getIntBlindData(i, MFn::kMeshVertComponent, gnrcVertBlindIds[blindIdx], 
														MString((const char*)m_BlindDataNames[blindIdx]), mayaBlindVal);
						blindValue = mayaBlindVal;
					}
					else if(attrTypes[0] == "boolean")
					{
						//Convert boolean values from maya into integer values in the mesh blind data
						bool mayaBlindVal;
						status = mayaMesh.getBoolBlindData(i, MFn::kMeshVertComponent, gnrcVertBlindIds[blindIdx], 
														MString((const char*)m_BlindDataNames[blindIdx]), mayaBlindVal);
						if(mayaBlindVal)
							blindValue = (int)1;
						else
							blindValue = (int)0;
					}
					else
					{
						retval.Combine( rexResult::WARNINGS );
						retval.AddMessage( "The specified blind data channel '%s', applied to the mesh '%s', is of an unsupported type. The default value will be used in its place.", 
						(const char*)m_BlindDataNames[blindIdx], (const char*)mesh->GetMeshName() );

						blindValue = m_BlindDataDefaults[blindIdx];
					}

					//Set the blind data value
					gnrcVI.m_BlindData[blindIdx] = blindValue;
				}
				else
				{
					//There isn't a value specified in maya, so set the default value specified
					//by the rule file
					gnrcVI.m_BlindData[blindIdx] = m_BlindDataDefaults[blindIdx];
				}
			}
		}
	}

	//Do we need to populate the generic mesh object with blendshape/blendtarget information?
	if(m_ExportBlendShapes)
	{
		if( m_ProgressBarTextCB )
		{
			(*m_ProgressBarTextCB) ( "Collecting blend shape data" );
		}

		//Check to see if a blendshape node is attached to the mesh.  If so, we need to mark the materials
		//that are applied to faces whose vertices are blended.
		MObject blendShapeMObj;
		atArray<rexObjectGenericMesh::BlendTargetDelta>	targets;
		atArray<atString> targetNames;

		atArray<MObject> blendShapeNodes;
		rexMayaUtility::GetAllBlendShapeNodes(dp, blendShapeNodes);
		for( int i = 0; i < blendShapeNodes.GetCount(); i++ )
		{
			blendShapeMObj = blendShapeNodes[i];

			retval.Combine(rexMayaUtility::GetBlendTargetDeltas(blendShapeMObj, targets));
			if( retval.Errors() )
			{
				delete mesh;
				return retval;
			}

			retval.Combine(rexMayaUtility::GetBlendTargetNames(blendShapeMObj, targetNames));
			if( retval.Errors() )
			{
				delete mesh;
				return retval;
			}
		}

		// Copy the names for later
		for( int i = 0; i < targetNames.GetCount(); i++ )
		{
			rexObjectGenericMesh::BlendTargetInfo& btInfo = mesh->m_BlendTargets.Grow();
			btInfo.m_Name = targetNames[i];
		}

		// For each target, put delta information in each vertex in the mesh
		if( m_ProgressBarObjectCountCB )
			(*m_ProgressBarObjectCountCB )( mesh->m_Vertices.GetCount() );	

		for( int vert = 0; vert < mesh->m_Vertices.GetCount(); vert++ )
		{
			if( m_ProgressBarObjectCurrentIndexCB )
				(*m_ProgressBarObjectCurrentIndexCB)( vert );

			mesh->m_Vertices[vert].m_TargetDeltas.Resize(targets.GetCount());
			for( int target = 0; target < targets.GetCount(); target++ )
			{
				mesh->m_Vertices[vert].m_TargetDeltas[target] = targets[target].GetDelta(vert);
			}
		}

	}//End if(m_ExportBlendShapes)

	//If there are any instance attributes this converter should grab, populate the generic object 
	//with them
	if(m_InstanceAttributes.GetCount())
	{
		int instAttrCount = m_InstanceAttributes.GetCount();

		MDagPath curDP = dp;
		MFn::Type dpType = curDP.apiType();
		if( (dpType != MFn::kTransform) ||
			(dpType != MFn::kJoint) )
			curDP.pop();
		while(1)
		{
			for(int attrIdx=0; attrIdx<instAttrCount; attrIdx++)
			{
				rexValue attrVal;
				attrVal = rexMayaUtility::GetAttributeValue(curDP.node(), m_InstanceAttributes[attrIdx].m_Name);
				if(attrVal.GetType() != rexValue::UNKNOWN)
				{
					mesh->m_InstAttrNames.PushAndGrow(m_InstanceAttributes[attrIdx].m_Name);
					mesh->m_InstAttrValues.PushAndGrow(attrVal);
				}
			}
			if(curDP.pop() != MS::kSuccess)
				break;
		}
	}

	newObject = mesh;
/*
 	rexObjectAdapterPtr *ptr = new rexObjectAdapterPtr(mesh, rexObjectGenericMesh::CreateAdapter);
	if(!ptr)
	{
		delete mesh;
		return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);
	}

	// immediately dump the mesh to disk
	if( (retval = ptr->Save()).Errors() )
		return retval;
	if( (retval = ptr->Unload()).Errors() )
		return retval;
	newObject = ptr;
*/

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterMeshHierarchy::TagBlendShapeMaterials( const MDagPath &dp, atArray<MObject> &blendTargets, rexObjectGenericMesh *mesh ) const
{
	rexResult result(rexResult::PERFECT);
	
	int btCount = blendTargets.GetCount();
	if(!btCount)
		return result;

	//Get a list of which components are modified by each target
	atArray<BlendTargetDeltaInfo> btDeltaInfoArr;
	result.Combine(CollectBlendShapeVtxDeltaInfo ( dp, blendTargets, btDeltaInfoArr ));
	if(result.Errors())
		return result;

	//For each material, loop through all the primitives and determine which blend targets
	//(if any) affect the primitives the material is assigned to.
	int mtlCount = mesh->m_Materials.GetCount();
	for(int mtlIdx = 0; mtlIdx < mtlCount; mtlIdx++)
	{
		rexObjectGenericMesh::MaterialInfo& mtl = mesh->m_Materials[mtlIdx];

		for(int btIndex = 0; btIndex < btCount; btIndex++)
		{
			//For each blend target, check if any of its primitives are modified 
			//by the current blend target
			BlendTargetDeltaInfo& btDeltaInfo = btDeltaInfoArr[btIndex];

			int primCount = mtl.m_PrimitiveIndices.GetCount();
			for(int primIdx = 0; primIdx < primCount; primIdx++)
			{
				bool bPrimBreak = false;
				rexObjectGenericMesh::PrimitiveInfo& prim = mesh->m_Primitives[ mtl.m_PrimitiveIndices[ primIdx ] ];

				int adjCount = prim.m_Adjuncts.GetCount();
				for(int adjIdx = 0; adjIdx < adjCount; adjIdx++)
				{
					rexObjectGenericMesh::AdjunctInfo& adj = prim.m_Adjuncts[adjIdx];
					
					if( (btDeltaInfo.posBlend.IsSet(adj.m_VertexIndex)))
					{
						//One of the verts in this material is modified by the blend target,
						//so append the blend target index, and drop out of the prim loop.
						bPrimBreak = true;
						mtl.m_BlendTargetIndices.PushAndGrow(btIndex);
						break;
					}
				}
				//The material was tagged, so stop searching through the prims in this material
				if(bPrimBreak)
					break;
			}//End for(int primIdx...
		}//End for(int btIndex...
	}//End for(int mtlIdx ...
	
	return result;
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexConverterMeshHierarchy::CollectBlendShapeVtxDeltaInfo( const MDagPath& dp, atArray<MObject>& targetObjects, 
																  atArray<BlendTargetDeltaInfo>& outDeltaInfoArr ) const
{
	MStatus		status;
	rexResult	result( rexResult::PERFECT );
		
	MFnMesh baseMesh(dp, &status);
	Assertf( (status == MS::kSuccess), "Internal Maya error, failed to create MFnMesh from DAG object (%s)", dp.partialPathName().asChar());

	//Get the position, normal, bi-normal, tangent, and color arrays from the 
	//base mesh
	MPointArray	fBaseVertexArray;
	status = baseMesh.getPoints(fBaseVertexArray, MSpace::kObject);

	MFloatVectorArray fBaseNrmArray;
	status = baseMesh.getNormals(fBaseNrmArray, MSpace::kObject);

	/*
	MFloatVectorArray fBaseBiNrmArray;
	status = baseMesh.getBinormals(fBaseBiNrmArray, MSpace::kObject);

	MFloatVectorArray fBaseTanArray;
	status = baseMesh.getTangents(fBaseTanArray, MSpace::kObject);
	*/

	MColorArray fBaseColorArray;
	status = baseMesh.getVertexColors(fBaseColorArray);

	//Loop through each target, checking each component for changes against
	//the base object
	int targetObjCount = targetObjects.GetCount();
	for(int a = 0; a < targetObjCount; a++)
	{
		BlendTargetDeltaInfo& outDeltaInfo = outDeltaInfoArr.Grow();

		outDeltaInfo.posBlend.Init(fBaseVertexArray.length());
		outDeltaInfo.nrmBlend.Init(fBaseNrmArray.length());
		//outDeltaInfo.biNrmBlend.Init(fBaseBiNrmArray.length());
		//outDeltaInfo.tanBlend.Init(fBaseTanArray.length());
		outDeltaInfo.clrBlend.Init(fBaseColorArray.length());

		MFnMesh targetMesh(targetObjects[a], &status);

		//Get the position, normal, bi-normal, tangent, and color arrays from the 
		//target mesh
		MPointArray	fTargetVertexArray;
		status = targetMesh.getPoints(fTargetVertexArray, MSpace::kObject);
		unsigned int targetPosCount = fTargetVertexArray.length();

		MFloatVectorArray fTargetNrmArray;
		status = targetMesh.getNormals(fTargetNrmArray, MSpace::kObject);
		unsigned int targetNrmCount = fTargetNrmArray.length();

		//MFloatVectorArray fTargetBiNrmArray;
		//status = targetMesh.getBinormals(fTargetBiNrmArray, MSpace::kObject);
		//unsigned int targetBiNrmCount = fTargetBiNrmArray.length();

		//MFloatVectorArray fTargetTanArray;
		//status = targetMesh.getTangents(fTargetTanArray, MSpace::kObject);
		//unsigned int targetTanCount = fTargetTanArray.length();

		MColorArray fTargetColorArray;
		status = targetMesh.getVertexColors(fTargetColorArray);
		unsigned int targetColorCount = fTargetColorArray.length();

		//Check to make sure the component counts are the same between the base
		//object and the blend targets
		if(targetPosCount != fBaseVertexArray.length() )
		{
			result = rexResult::ERRORS;
			result.AddMessage("Base blendshape object (%s), and blend target (%s), have differing vertex counts",
				baseMesh.name().asChar(), targetMesh.name().asChar());
			return result;
		}
		if(targetNrmCount != fBaseNrmArray.length() )
		{
			result = rexResult::ERRORS;
			result.AddMessage("Base blendshape object (%s), and blend target (%s), have differing vertex normal counts",
				baseMesh.name().asChar(), targetMesh.name().asChar());
			return result;
		}

		/*
		if(targetBiNrmCount != fBaseBiNrmArray.length() )
		{
			result = rexResult::ERRORS;
			result.AddMessage("Base blendshape object (%s), and blend target (%s), have differing vertex bi-normal counts",
				baseMesh.name().asChar(), targetMesh.name().asChar());
			return result;
		}

		if(targetTanCount != fBaseTanArray.length() )
		{
			result = rexResult::ERRORS;
			result.AddMessage("Base blendshape object (%s), and blend target (%s), have differing vertex tangent counts",
				baseMesh.name().asChar(), targetMesh.name().asChar());
			return result;
		}
		*/

		if(targetColorCount != fBaseColorArray.length() )
		{
			result = rexResult::ERRORS;
			result.AddMessage("Base blendshape object (%s), and blend target (%s), have differing vertex color counts",
				baseMesh.name().asChar(), targetMesh.name().asChar());
			return result;
		}

		//Position check
		for(unsigned int i=0; i<targetPosCount; i++)
		{
			if(!fBaseVertexArray[i].isEquivalent(fTargetVertexArray[i], BLEND_DELTA_THRESHOLD))
				outDeltaInfo.posBlend.Set(i);
		}

		//Normal check
		for(unsigned int i=0; i<targetNrmCount; i++)
		{
			if(!fBaseNrmArray[i].isEquivalent(fTargetNrmArray[i], (float)BLEND_DELTA_THRESHOLD))
				outDeltaInfo.nrmBlend.Set(i);
		}

		/*
		//Bi-Normal check
		for(unsigned int i=0; i<targetBiNrmCount; i++)
		{
			if(!fBaseBiNrmArray[i].isEquivalent(fTargetBiNrmArray[i], (float)BLEND_DELTA_THRESHOLD))
				outDeltaInfo.biNrmBlend.Set(i);
		}

		//Tangent check
		for(unsigned int i=0; i<targetTanCount; i++)
		{
			if(!fBaseTanArray[i].isEquivalent(fTargetTanArray[i], (float)BLEND_DELTA_THRESHOLD))
				outDeltaInfo.tanBlend.Set(i);
		}
		*/

		//Color check
		for(unsigned int i=0; i<targetColorCount; i++)
		{
			MFloatPoint bsPoint(fBaseColorArray[i].r, fBaseColorArray[i].g, fBaseColorArray[i].b, fBaseColorArray[i].a);
			MFloatPoint tgPoint(fTargetColorArray[i].r, fTargetColorArray[i].g, fTargetColorArray[i].b, fTargetColorArray[i].a);
			if(!bsPoint.isEquivalent(tgPoint, (float)BLEND_DELTA_THRESHOLD))
				outDeltaInfo.clrBlend.Set(i);
		}
	}//for(int a = 0; a < targetObjCount; a++)
	
	return result;
}

/////////////////////////////////////////////////////////////////////////////////////
//CLEANUP : This could be moved to mayaUtility.cpp
rexResult rexConverterMeshHierarchy::CollectBlendTargetsFromBlendShape( const MObject& blendShapeMObj, atArray<MObject>& outBlendTargets) const
{
	MStatus		status;
	rexResult	result(rexResult::PERFECT);

	MFnBlendShapeDeformer fnBlendShapeDef(blendShapeMObj, &status);
	Assert(status == MS::kSuccess);
	atString dbgString(fnBlendShapeDef.name().asChar());

	bool bIsParallelBlender = false;
	
	MPlug isParallelPlug = fnBlendShapeDef.findPlug("parallelBlender", &status);
	if(status == MS::kSuccess)
	{
		bool bTmp;
		status = isParallelPlug.getValue(bTmp);
		if(status == MS::kSuccess)
			bIsParallelBlender = bTmp;
	}
	
	MObjectArray baseMObjArr;
	fnBlendShapeDef.getBaseObjects(baseMObjArr);
	Assertf( (baseMObjArr.length()), "Unable to locate base object attached to the blendshape deformer (%s)", fnBlendShapeDef.name().asChar());

	if(baseMObjArr.length() > 1)
	{
		result.Set(rexResult::ERRORS);
		result.AddMessage("Multiple base objects returned for the blendshape deformer (%s), this is not permitted", fnBlendShapeDef.name().asChar());
		return result;
	}

	MObject baseMObj = baseMObjArr[0];

	MIntArray indexArray;
	status = fnBlendShapeDef.weightIndexList(indexArray);
	unsigned int numTargets = indexArray.length();

	MFnDependencyNode base(baseMObj);
	MString name = fnBlendShapeDef.name();
	Printf("Collecting %d targets from blend shape: %s.  For base object: %s\n", numTargets, name.asChar(), base.name().asChar()); 

	for(unsigned int tIndex=0; tIndex<numTargets; tIndex++)
	{
		MObjectArray targetObjects;

		//MStatus s = fnBlendShapeDef.getTargets(baseMObj, indexArray[tIndex], targetObjects);
		MStatus s = fnBlendShapeDef.getTargets(baseMObj, tIndex, targetObjects);
		unsigned int targetCount = targetObjects.length();
		if(targetCount == 0)
			continue;

		MObject targetMObj;
		
		//We only allow one target per weight index. Maya does support multiple targets per weight index to allow for
		//"Serial" blending (from the Maya API docs) :
		//"It is also possible to chain together target shapes so that a base object will deform through each shape one at a time as 
		//the weight value increases. This is done by adding multiple targets to a base shape using the same weight index for all of them. 
		//When each target is added, a weight value is specified at which that target will be in full effect."
		if(targetCount == 1)
		{
			targetMObj = targetObjects[0];
		}
		else
		{
			result.Set(rexResult::ERRORS);
			result.AddMessage("Multiple targets (%d) specified for a single weight (%d), in the blend shape deformer (%s).  Serial blend targets are not allowed.",
				targetCount, tIndex, fnBlendShapeDef.name().asChar());
			return result;
		}

		MFnDependencyNode dntemp(targetMObj);
		Printf("Blend shape: %s. Has target: %s at index %d\n", name.asChar(), dntemp.name().asChar(), tIndex);

		//Note : Only append meshes that are targets of non-parallel blenders, since these will
		//have other blendshape nodes attached to them as targets, which will be connected to
		//the actual mesh targets we are looking for.
		MFn::Type tgType = targetMObj.apiType();
		if(tgType == MFn::kBlendShape)
		{
			rexResult r = CollectBlendTargetsFromBlendShape(targetMObj, outBlendTargets);
			if( !r.Perfect() )
				return r;
		}
		else if(tgType == MFn::kMesh && (!bIsParallelBlender))
		{
			MFnMesh dbgMesh(targetMObj);
			atString dbgName(dbgMesh.name().asChar());
			outBlendTargets.PushAndGrow(targetMObj);
		}
	}

	return result;
}

/////////////////////////////////////////////////////////////////////////////////////
//CLEANUP : This could be moved to mayaUtility.cpp
bool rexConverterMeshHierarchy::IsMayaMeshAllTriangles( const MDagPath &dp ) const
{
	MStatus status;
	
	MItMeshPolygon polyIt(dp.node(), &status);
	Assert(status == MS::kSuccess);

	while(!polyIt.isDone())
	{
		int nVerts = polyIt.polygonVertexCount();
		if(nVerts >= 4)
			return false;
		polyIt.next();
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyMeshCheckVertexNumWeights::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterMeshHierarchy* conv = dynamic_cast<rexConverterMeshHierarchy*>( module.m_Converter );

	if( conv )
	{
		conv->SetCheckVertexNumWeights( value.toBool( conv->GetCheckVertexNumWeights() ) );			
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyMeshExportBlendShapes::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterMeshHierarchy* conv = dynamic_cast<rexConverterMeshHierarchy*>( module.m_Converter );

	if( conv )
	{
		conv->SetExportBlendShapes( value.toBool( conv->GetExportBlendShapes() ) );			
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////
//
//The property specification in the rule file for this property is expected to be
//in the following format
//
//BlindDataToExport,HI_BlindDataToExport,string,(maya long attribute name|rage blind data id|type|default)
//e.g.
//BlindDataToExport,HI_BlindDataToExport,string,(rageClothPins|9|float|0.0)
//Note : float,double, and int are the only types currently supported for export.

//Multiple specifications should be seperated by ;'s.
//
rexResult rexPropertyMeshBlindDataToExport::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterMeshHierarchy* conv = dynamic_cast<rexConverterMeshHierarchy*>( module.m_Converter );

	if( conv )
	{
		if( value.toString() )
		{
			std::string bdNamesString = value.toString();
			unsigned int startParen = bdNamesString.find_first_of('(');
			unsigned int endParen = bdNamesString.find_last_of(')');
			if( (startParen == std::string::npos) ||
				(endParen == std::string::npos) )
			{
				return rexResult( rexResult::WARNINGS, rexResult::INVALID_INPUT );
			}
			unsigned int cIdx = startParen+1;
			while(cIdx < endParen)
			{
				atString bdNameStr;
				atString bdIdStr;
				atString bdTypeStr;
				atString bdDefaultStr;

				while( (bdNamesString[cIdx] != '|') &&
					   (bdNamesString[cIdx] != ')') )
				{
					bdNameStr += bdNamesString[cIdx++];
				}
				cIdx++;
				
				while( (bdNamesString[cIdx] != '|') &&
					   (bdNamesString[cIdx] != ')') )
				{
					bdIdStr += bdNamesString[cIdx++];
				}
				cIdx++;

				while( (bdNamesString[cIdx] != '|') &&
					   (bdNamesString[cIdx] != ')') )
				{
					bdTypeStr += bdNamesString[cIdx++];
				}
				cIdx++;
				
				while( (bdNamesString[cIdx] != ';') &&
					   (bdNamesString[cIdx] != ')') )
				{
					bdDefaultStr += bdNamesString[cIdx++];
				}
				cIdx++;

				//Get the ID 
				int bdID = (int)atoi((const char*)bdIdStr);

				//Get the type
				rexValue::valueTypes bdType;
				if( strcmpi( (const char*)bdTypeStr, "float" ) == 0)
					bdType = rexValue::FLOAT;
				else if( strcmpi( (const char*)bdTypeStr, "double" ) == 0)
					bdType = rexValue::FLOAT;
				else if( strcmpi( (const char*)bdTypeStr, "int" ) == 0)
					bdType = rexValue::INTEGER;
				else
				{
					rexResult result = rexResult( rexResult::ERRORS );
					result.AddMessage("Unknown type '%s' in BlindDataToExport property specification", (const char*)bdTypeStr);
					return result;
				}

				//Get the default value
				rexValue bdDefault( bdType );
				switch( bdType )
				{
				case rexValue::FLOAT:
					{
						float fTmp = (float)atof((const char*)bdDefaultStr);
						bdDefault = fTmp;
						break;
					}
				case rexValue::INTEGER:
					{
						int iTmp = (int)atoi((const char*)bdDefaultStr);
						bdDefault = iTmp;
						break;
					}
				default:
					{
						break;
					}
				};

				conv->AddBlindDataExport(bdNameStr, bdType, bdID, bdDefault);
			}
		}
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyDataRelativeToTaggedRexRootMesh::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterMeshHierarchy* conv = dynamic_cast<rexConverterMeshHierarchy*>( module.m_Converter );

	if( conv )
	{
		conv->SetDataRelativeToTaggedRexRoot( value.toBool( conv->GetDataRelativeToTaggedRexRoot() ) );			
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage
