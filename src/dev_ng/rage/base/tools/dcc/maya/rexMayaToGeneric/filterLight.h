// 
// rexMayaToGeneric/filterLight.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexFilterLights -- filter that accepts lights
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYA_ROCKSTAR_FILTERLIGHT_H__
#define __REX_MAYA_ROCKSTAR_FILTERLIGHT_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/filter.h"
#include "rexMaya/objectMDagPath.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexFilterLight : public rexFilter
{
public:
	virtual bool AcceptObject( const rexObject& inputObject ) const 
	{ 
		const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &inputObject );
		if( !dpObj )
			return false;

		return( ( dpObj->m_DagPath.apiType() == MFn::kDirectionalLight ) || ( dpObj->m_DagPath.apiType() == MFn::kSpotLight ) || ( dpObj->m_DagPath.apiType() == MFn::kAmbientLight ) || ( dpObj->m_DagPath.apiType() == MFn::kPointLight ));
	}
	
	virtual rexFilter*		CreateNew() const { return new rexFilterLight; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
