// 
// rexMayaToGeneric/converterHair.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REX_MAYAROCKSTAR_CONVERTERHAIR_H__
#define __REX_MAYAROCKSTAR_CONVERTERHAIR_H__

#define _BOOL
#include "atl/array.h"
#include "rexBase/converter.h"
#include "rexGeneric/objectHair.h"

namespace rage
{

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterHairMeshGroup : public rexConverter
{
public:
	virtual rexConverter* CreateNew() const  { return new rexConverterHairMeshGroup; }

protected:
	virtual rexResult Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterHairMeshInfo : public rexConverter
{
public:
	virtual rexConverter* CreateNew() const  { return new rexConverterHairMeshInfo; }

	void SetHairMeshNode(MDagPath& dp); 
	
protected:
	virtual rexResult Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;

	MDagPath				m_HairMesh;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyHairMeshInfoMesh : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyHairMeshInfoMesh; }
};

/////////////////////////////////////////////////////////////////////////////////////

}// namespace rage

#endif //__REX_MAYAROCKSTAR_CONVERTERHair_H__
