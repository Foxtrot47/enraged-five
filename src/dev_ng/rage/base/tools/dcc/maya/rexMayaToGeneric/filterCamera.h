// 
// rexMayaToGeneric/filterCamera.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexFilterLocator -- filter that accepts locators
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYA_ROCKSTAR_FILTERCAMERA_H__
#define __REX_MAYA_ROCKSTAR_FILTERCAMERA_H__ 

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/filter.h"
#include "rexMaya/objectMDagPath.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexFilterCamera : public rexFilter
{
public:
	virtual bool AcceptObject( const rexObject& inputObject ) const 
	{ 
		const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &inputObject );
		if( !dpObj )
			return false;

		return ( dpObj->m_DagPath.apiType() == MFn::kCamera );
	}
	
	virtual rexFilter*		CreateNew() const { return new rexFilterCamera; }
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
