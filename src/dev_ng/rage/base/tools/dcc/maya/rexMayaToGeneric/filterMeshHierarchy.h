// 
// rexMayaToGeneric/filterMeshHierarchy.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexFilterMeshHierarchy -- filter that accepts meshes, transforms, LOD groups, and joints
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYA_ROCKSTAR_FILTERMESHHIERARCHY_H__
#define __REX_MAYA_ROCKSTAR_FILTERMESHHIERARCHY_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/filter.h"
#include "rexMaya/objectMDagPath.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexFilterMeshHierarchy : public rexFilter
{
public:
	virtual bool AcceptObject( const rexObject& inputObject ) const 
	{ 
		const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &inputObject );
		if( !dpObj )
			return false;

		return ( dpObj->m_DagPath.apiType() == MFn::kMesh || dpObj->m_DagPath.apiType() == MFn::kLodGroup ||
				 dpObj->m_DagPath.apiType() == MFn::kTransform || dpObj->m_DagPath.apiType() == MFn::kJoint );
	}
	
	virtual rexFilter*		CreateNew() const { return new rexFilterMeshHierarchy; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
