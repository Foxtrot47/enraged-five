// 
// rexMayaToGeneric/converterCloth.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#define _BOOL

#include "rexBase/module.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"

#include "rexMayaToGeneric/converterCloth.h"


namespace rage
{

rexResult rexConverterClothMeshGroup::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& /*defaultName*/ ) const
{
	rexResult result (rexResult::PERFECT);

	rexObjectGenericClothMeshGroup *pClothGroup = new rexObjectGenericClothMeshGroup();
	outputObject = pClothGroup;

	return result;
}

rexResult rexConverterCharClothMeshGroup::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& /*defaultName*/ ) const
{
	rexResult result (rexResult::PERFECT);

	rexObjectGenericCharClothMesh *pCharClothGroup = new rexObjectGenericCharClothMesh();
	outputObject = pCharClothGroup;

	return result;
}

void rexConverterClothMeshInfo::SetClothMeshNode(MDagPath& dp)
{
	m_clothMesh = dp;
}

void rexConverterClothMeshInfo::AppendClothBound(MDagPath& dp)
{
	m_clothBounds.PushAndGrow(dp);
}

rexResult rexConverterClothMeshInfo::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );
	rexObjectGenericClothMeshInfo *pClothInfo = new rexObjectGenericClothMeshInfo();

	pClothInfo->SetName( defaultName );
	pClothInfo->m_ClothMeshName = rexMayaUtility::DagPathToName(m_clothMesh);

	int boundCount = m_clothBounds.GetCount();
	for(int i=0; i<boundCount; i++)
	{
		//Locate the joint the bound is parented to
		MDagPath boneDP( m_clothBounds[i] );
		boneDP.pop();
		MStatus popStatus = MStatus::kSuccess;
		while (boneDP.apiType() != MFn::kJoint && popStatus == MStatus::kSuccess)
		{
			popStatus = boneDP.pop();
		}

		//Did we find a joint?
		if (popStatus == MStatus::kSuccess)
		{
			atString boneName = rexMayaUtility::DagPathToName(boneDP);

			//Only add it if it isn't already included in the list
			if(pClothInfo->m_BoneNames.Find(boneName) == -1)
				pClothInfo->m_BoneNames.PushAndGrow(boneName);
		}
		else
		{
			result.Set(rexResult::ERRORS);
			result.AddMessage("The bound '%s' has been tagged as effecting a cloth piece, but isn't parented to a joint.",
				(const char*)(rexMayaUtility::DagPathToName(m_clothBounds[i])));
			delete pClothInfo;
			return result;
		}
	}

	outputObject = pClothInfo;

	return result;
}


rexResult rexPropertyClothMeshInfoMesh::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexResult result( rexResult::PERFECT );

	rexConverterClothMeshInfo* conv = dynamic_cast<rexConverterClothMeshInfo*>( module.m_Converter );

	if( !conv )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	if( objects.GetCount() )
	{
		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[ 0 ] );
		if( dpObj )
		{
			atString dbg = rexMayaUtility::DagPathToName(dpObj->m_DagPath);
			conv->SetClothMeshNode( dpObj->m_DagPath );
		}
		else
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	return result;
}

rexResult rexPropertyClothMeshInfoBounds::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexResult result( rexResult::PERFECT );

	rexConverterClothMeshInfo* conv = dynamic_cast<rexConverterClothMeshInfo*>( module.m_Converter );

	if( !conv )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	int objCount = objects.GetCount();
	if( objCount)
	{
		for(int i=0; i<objCount; i++)
		{
			rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[ i ] );
			if( dpObj )
			{
				atString dbg = rexMayaUtility::DagPathToName(dpObj->m_DagPath);
				conv->AppendClothBound( dpObj->m_DagPath );
			}
			else
				return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}
	}

	return result;
}

}//namespace rage
