// 
// rexMayaToGeneric/converterLevel.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Rmworld Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		AGE 
//			atl, core, data
//		REX
//			Base Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterRmworldDistrict
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_CONVERTERLEVEL_H__
#define __REX_CONVERTERLEVEL_H__

#include "rexBase/converter.h"
#include "rexBase/property.h"
#include "rexMayaToGeneric/propertyBasic.h"
#include "rexMayaToGeneric/converterBound.h"
#include "rexMayaToGeneric/converterMesh.h"
#include "rexGeneric/objectLevel.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterRmworldDistrict : public rexConverter
{
public:
	virtual rexResult			Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexConverter*		CreateNew() const { return new rexConverterRmworldDistrict; }
};

/////////////////////////////

class rexConverterMeshHierarchyPortals : public rexConverterMeshHierarchy
{
public:
	rexConverterMeshHierarchyPortals()
	{
		m_ExportTriangles = false;
	}

	virtual rexConverter *CreateNew() const  { return new rexConverterMeshHierarchyPortals; }
	
protected:
	virtual rexObjectHierarchy *CreateHierarchyNode() const { return new rexObjectGenericMeshHierarchyPortals; }
};

/////////////////////////////

class rexConverterMeshHierarchyWater : public rexConverterMeshHierarchy
{
public:
	virtual rexConverter *CreateNew() const  { return new rexConverterMeshHierarchyWater; }
	
protected:
	virtual rexObjectHierarchy *CreateHierarchyNode() const { return new rexObjectGenericMeshHierarchyWater; }
};

/////////////////////////////

class rexConverterMeshHierarchyRoomVolumes : public rexConverterMeshHierarchy
{
public:
	rexConverterMeshHierarchyRoomVolumes()
	{
		m_ExportTriangles = false;
	}

	virtual rexConverter *CreateNew() const  { return new rexConverterMeshHierarchyRoomVolumes; }

protected:
	virtual rexObjectHierarchy *CreateHierarchyNode() const { return new rexObjectGenericMeshHierarchyRoomVolumes; }
};

/////////////////////////////

class rexConverterMeshHierarchyOccluders : public rexConverterMeshHierarchy
{
public:
	virtual rexConverter *CreateNew() const  { return new rexConverterMeshHierarchyOccluders; }
	
protected:
	virtual rexObjectHierarchy *CreateHierarchyNode() const { return new rexObjectGenericMeshHierarchyOccluders; }
};

/////////////////////////////

class rexConverterRmworldDistrictProxyMeshHierarchy : public rexConverterMeshHierarchy
{
public:
	virtual rexConverter *CreateNew() const { return new rexConverterRmworldDistrictProxyMeshHierarchy; }

protected:
	virtual rexObjectHierarchy *CreateHierarchyNode() const { return new rexObjectRmworldDistrictProxyMeshHierarchy; }
	virtual rexResult ConvertMayaNode( const MDagPath &dp, int boneIndex, const Matrix44& boneMatrix, int lodGroupIndex, int lodLevel, float lodThreshold, const atString* groupID, int groupIndex, rexObject *&newObject ) const;
};

/////////////////////////////

class rexConverterRmworldDistrictNavMeshHierarchy : public rexConverterBoundHierarchy
{
public:
	virtual rexConverter *CreateNew() const { return new rexConverterRmworldDistrictNavMeshHierarchy; }

protected:
	virtual rexObjectHierarchy *CreateHierarchyNode() const { return new rexObjectRmworldDistrictNavMeshHierarchy; }
};

/////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

class rexConverterRoomTypes : public rexConverter
{
public:
	virtual rexResult		Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexConverter*	CreateNew() const  { return new rexConverterRoomTypes; }	
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyRoomSetAmbientSoundName : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const  { return new rexPropertyRoomSetAmbientSoundName; }	
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyRoomSetAmbientSoundVolume : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const  { return new rexPropertyRoomSetAmbientSoundVolume; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyRoomSetReverbType : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const  { return new rexPropertyRoomSetReverbType; }	
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyRoomSetReverbVolume : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const  { return new rexPropertyRoomSetReverbVolume; }	
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyRoomSetUserData0 : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const  { return new rexPropertyRoomSetUserData0; }	
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyRoomSetUserData1 : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const  { return new rexPropertyRoomSetUserData1; }	
};

}	// namespace rage

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

#endif
