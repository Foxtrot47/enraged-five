// 
// rexMayaToGeneric/filterPrimitive.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#ifndef __REX_MAYA_ROCKSTAR_FILTERPRIMITIVE_H__
#define __REX_MAYA_ROCKSTAR_FILTERPRIMITIVE_H__


#include "rexBase/filter.h"
#include "rexMaya/objectMDagPath.h"


namespace rage {

//-----------------------------------------------------------------------------

class rexFilterPrimitive : public rexFilter
{
public:
	virtual bool AcceptObject( const rexObject& inputObject ) const 
	{ 
		const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &inputObject );
		if( !dpObj )
			return false;
		
		MObject nodeObj = dpObj->m_DagPath.node();
		MFnDagNode node(nodeObj);
		MTypeId type = node.typeId();

		if( (type == NODE_RAGE_PHBOUNDBOX) ||
			(type == NODE_RAGE_PHBOUNDCAPSULE) ||
			(type == NODE_RAGE_PHBOUNDSPHERE ) ||
			(type == NODE_RAGE_PHBOUNDCYLINDER) )
		{
			return true;
		}
		else
			return false;
	}
	
	virtual rexFilter*		CreateNew() const { return new rexFilterPrimitive; }
};

//-----------------------------------------------------------------------------

} // End namespace rage

#endif //__REX_MAYA_ROCKSTAR_FILTERPRIMITIVE_H__
