// 
// rexMayaToGeneric/converterPrimitive.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"

#include "rexGeneric/objectPrimitive.h"
#include "rexMayaToGeneric/converterPrimitive.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MDagPath.h>
#include <Maya/MFnDagNode.h>
#include <Maya/MMatrix.h>
#include <Maya/MString.h>
#include <Maya/MTransformationMatrix.h>
#pragma warning(pop)


namespace rage 
{

//-----------------------------------------------------------------------------

rexResult rexConverterPrimitive::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& /*defaultName*/ ) const
{
	rexResult result( rexResult::PERFECT );

	rexObjectGenericPrimitiveGroup *pPrimGroup = new rexObjectGenericPrimitiveGroup();

	int inputObjectCount = inputObjectArray.GetCount();
	
	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );

	for( int a = 0; a < inputObjectCount; a++ )
	{		
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)( a );

		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( inputObjectArray[ a ] );
		
		if( dpObj )
		{
			MDagPath& dp = dpObj->m_DagPath;
			
			if( m_ProgressBarTextCB )
			{
				static char message[1024];
				sprintf( message, "Converting primitive data at %s", dpObj->m_DagPath.partialPathName().asChar() );
				(*m_ProgressBarTextCB) ( message );
			}

			MObject nodeObj = dp.node();
			MFnDagNode node(nodeObj);
			MTypeId type = node.typeId();

			if(type == NODE_RAGE_PHBOUNDBOX)
			{
				MDagPath parent( dp );
				parent.pop();
				Matrix44 m;

				MMatrix mayaParMat = parent.inclusiveMatrix();
				MTransformationMatrix mayaParTMat(mayaParMat);
				
				double scale[3];
				mayaParTMat.getScale(scale, MSpace::kWorld);

				rexObjectGenericPrimitiveBox* pPrimBox = new rexObjectGenericPrimitiveBox();
			
				pPrimBox->SetFullPath(parent.fullPathName().asChar());
				pPrimBox->SetName( rexMayaUtility::DagPathToName( parent ) );
				pPrimBox->m_Width = (float)scale[0];
				pPrimBox->m_Height = (float)scale[1];
				pPrimBox->m_Depth  = (float)scale[2];

				MMatrix incMat = parent.inclusiveMatrix();
				MTransformationMatrix tm(incMat);
				double unitVec[3] = {1.0,1.0,1.0};
				tm.setScale(unitVec, MSpace::kWorld);
				MMatrix unitScaleMat = tm.asMatrix();

				rexMayaUtility::GetMatrix34FromMayaMatrix( pPrimBox->m_Matrix, unitScaleMat );
				
				pPrimGroup->m_ContainedObjects.PushAndGrow(pPrimBox);
			}
			else if(type == NODE_RAGE_PHBOUNDSPHERE)
			{
				MDagPath parent( dp );
				parent.pop();
				Matrix44 m;

				MMatrix mayaParMat = parent.inclusiveMatrix();
				MTransformationMatrix mayaParTMat(mayaParMat);
				
				double scale[3];
				mayaParTMat.getScale(scale, MSpace::kWorld);

				rexObjectGenericPrimitiveSphere* pPrimSphere = new rexObjectGenericPrimitiveSphere();

				pPrimSphere->SetFullPath(parent.fullPathName().asChar());
				pPrimSphere->SetName( rexMayaUtility::DagPathToName( parent ) );
				pPrimSphere->m_Radius = (float)scale[0];
				
				MMatrix incMat = parent.inclusiveMatrix();
				MTransformationMatrix tm(incMat);
				double unitVec[3] = {1.0,1.0,1.0};
				tm.setScale(unitVec, MSpace::kWorld);
				MMatrix unitScaleMat = tm.asMatrix();

				rexMayaUtility::GetMatrix34FromMayaMatrix( pPrimSphere->m_Matrix, unitScaleMat );
				
				pPrimGroup->m_ContainedObjects.PushAndGrow(pPrimSphere);
			}
		}
	}

	outputObject = pPrimGroup;

	return result;
}

//-----------------------------------------------------------------------------

} //End namespace rage
