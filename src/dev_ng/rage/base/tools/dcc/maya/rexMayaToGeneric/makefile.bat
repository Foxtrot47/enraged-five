set ARCHIVE=rexMayaToGeneric
set ARCHIVE_BASE=mayalib
call ..\..\libs\rex\setmayapath.bat
set XPROJ=%RAGE_DIR%\base\src
set XINCLUDE=%XINCLUDE% %RAGE_DIR%\base\tools\deprecated
set FILES=exportTexture converterBasic converterEntity converterFragment converterFragmentDamage
set FILES=%FILES% converterSkeleton converterAnimation converterMesh 
set FILES=%FILES% converterBound converterLocator converterEdgeModel
set FILES=%FILES% converterCutscene propertyBasic converterLight converterNurbsCurve
set FILES=%FILES% converterFX converterLevel converterLevelInstance
set FILES=%FILES% converterCloth converterHair converterPrimitive
set HEADONLY=filterMesh filterMeshHierarchy filterBoundHierarchy filterBoneRoot filterPrimitive 
set HEADONLY=%HEADONLY% filterLocator filterLight filterNurbsCurve filterCamera 
set HEADONLY=%HEADONLY% converterDrawableMesh  filterLevelInstance
