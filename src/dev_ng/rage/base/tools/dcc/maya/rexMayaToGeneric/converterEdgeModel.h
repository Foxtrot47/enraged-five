// 
// rexMayaToGeneric/converterEdgeModel.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterMeshHierarchyEdgeModel
//			 -- gathers edge model mesh information
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexConverter
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTEREDGEMODEL_H__
#define __REX_MAYAROCKSTAR_CONVERTEREDGEMODEL_H__

#include "rexMayaToGeneric/converterMesh.h"

/////////////////////////////////////////////////////////////////////////////////////
namespace rage {

class rexConverterMeshHierarchyEdgeModel : public rexConverterMeshHierarchy
{
public:
	virtual rexConverter *CreateNew() const { return new rexConverterMeshHierarchyEdgeModel; }

protected:
	virtual rexObjectHierarchy *CreateHierarchyNode() const;
	virtual rexResult ConvertMayaNode( const MDagPath &dp, int boneIndex, const Matrix44& boneMatrix, int lodGroupIndex, int lodLevel, float lodThreshold, const atString* groupID, int groupIndex, rexObject *&newObject ) const;
};

class rexPropertyAllowMultipleEdgeModels : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyAllowMultipleEdgeModels; }
};

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////

#endif
