// 
// rexMayaToGeneric/converterEntity.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterEntity
//			 -- converts entity information
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTERENTITY_H__
#define __REX_MAYAROCKSTAR_CONVERTERENTITY_H__

#include "rexBase/converter.h"
#include "rexBase/property.h"

/////////////////////////////////////////////////////////////////////////////////////
namespace rage {

class rexConverterEntity : public rexConverter
{
public:
	virtual rexResult			Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexConverter*		CreateNew() const { return new rexConverterEntity; }
	
};

/////////////////////////////

class rexPropertyEntityTypeFileName : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityTypeFileName; }
};

class rexPropertyEntityReferenced : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityReferenced; }
};

class rexPropertyEntitySkipBoundSectionOfTypeFile : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntitySkipBoundSectionOfTypeFile; }
};

class rexPropertyEntityEntityClass : public rexProperty
{
public:
	virtual rexResult		ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEntityEntityClass; }
};

/////////////////////////////

} // namespace rage

#endif
