#include "rexGeneric/objectEntity.h"
#include "rexMaya/mayahelp.h"
#include "rexMayaToGeneric/converterEntity.h"

///////////////////////////////////////////////////////////////////////////////////////////
namespace rage {

rexResult rexConverterEntity::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	rexObjectGenericEntity* entity = new rexObjectGenericEntity;
	entity->SetName( defaultName );

	outputObject = entity;
	
	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyEntityTypeFileName::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectGenericEntity* entity = dynamic_cast<rexObjectGenericEntity*>( &obj );

	if( !entity )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	entity->m_EntityTypeFileName = value.toString();

	return rexResult( rexResult::PERFECT );
}

////////////////////////////////////////////////////////////////////////////////
rexResult rexPropertyEntityReferenced::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectGenericEntity* entity = dynamic_cast<rexObjectGenericEntity*>( &obj );

	if( !entity )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	entity->m_Referenced = value.toBool();
	return rexResult( rexResult::PERFECT );
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyEntitySkipBoundSectionOfTypeFile::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectGenericEntity* entity = dynamic_cast<rexObjectGenericEntity*>( &obj );

	if( !entity )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	entity->m_SkipBoundSectionOfTypeFile = value.toBool();

	return rexResult( rexResult::PERFECT );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyEntityEntityClass::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectGenericEntity* entity = dynamic_cast<rexObjectGenericEntity*>( &obj );

	if( !entity )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	entity->m_EntityClass = value.toString();

	return rexResult( rexResult::PERFECT );
}

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage
