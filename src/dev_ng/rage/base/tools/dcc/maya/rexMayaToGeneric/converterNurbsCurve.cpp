#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"
#include "rexGeneric/objectNurbsCurve.h"
#include "rexMayaToGeneric/converterNurbsCurve.h"
#include "rexBase/module.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MDoubleArray.h>
#include <Maya/MFnNurbsCurve.h>
#include <Maya/MPointArray.h>
#include <Maya/MString.h>
#pragma warning(pop)

/////////////////////////
namespace rage {

rexResult rexConverterNurbsCurve::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	printf("%d", defaultName);

	// create object 
	rexObjectGenericNurbsCurveGroup* curveGroup = new rexObjectGenericNurbsCurveGroup;
	curveGroup->SetName(defaultName);

	int inputObjectCount = inputObjectArray.GetCount();
	
	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );

	for( int a = 0; a < inputObjectCount; a++ )
	{		
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)( a );

		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( inputObjectArray[ a ] );
		
		if( dpObj )
		{
			if( !m_UseChildrenOfLevelInstanceNodes && dpObj->m_ChildOfLevelInstanceNode )
				continue;

			MDagPath& dp = dpObj->m_DagPath;
			
			if( m_ProgressBarTextCB )
			{
				static char message[1024];
				sprintf( message, "Converting curve data at %s", dpObj->m_DagPath.partialPathName().asChar() );
				(*m_ProgressBarTextCB) ( message );
			}
		
			if( dp.apiType() == MFn::kNurbsCurve )
			{
				rexObjectGenericNurbsCurve* curve = new rexObjectGenericNurbsCurve;

				MFnNurbsCurve fn( dp );

				MDagPath parentDP( dp );
				parentDP.pop();
				MFnDagNode fnParent( parentDP );
				curve->SetName( fnParent.name().asChar() );
				
				curve->m_BoneIndex = dpObj->m_BoneIndex;
				curve->m_GroupIndex = dpObj->m_GroupIndex;

				curve->m_Degree = fn.degree();

				switch( fn.form() )
				{
					case MFnNurbsCurve::kOpen:
						curve->m_Type = rexObjectGenericNurbsCurve::NC_OPEN;
						break;
					case MFnNurbsCurve::kClosed:
						curve->m_Type = rexObjectGenericNurbsCurve::NC_CLOSED;
						break;
					case MFnNurbsCurve::kPeriodic:
						curve->m_Type = rexObjectGenericNurbsCurve::NC_PERIODIC;
						break;
					default:
						Assertf(0,"I dont understand MFnNurbsCurve of type %d. Only MFnNurbsCurve::kOpen = 1, MFnNurbsCurve::kClosed = 2, and MFnNurbsCurve::kPeriodic = 3.",fn.form());
						break;
				};

				MPointArray		controlVerts;
				MDoubleArray	knotValues;
				fn.getKnots( knotValues );
				fn.getCVs( controlVerts, MSpace::kWorld );

				int knotCount = knotValues.length();
				for( int a = 0; a < knotCount; a++ )
				{
					curve->m_Knots.PushAndGrow( (float)knotValues[ a ],(u16) knotCount );
				}

				int cvCount = controlVerts.length();
				for( int a = 0; a < cvCount; a++ )
				{
					Vector3 pt( (float)controlVerts[ a ].x, (float)controlVerts[ a ].y, (float)controlVerts[ a ].z );
					if( dpObj->m_BoneIndex > 0 || GetCurvesRelativeToBone())
					{
						Matrix34 m;
						dpObj->m_BoneMatrix.ToMatrix34( m );
						m.UnTransform( pt, curve->m_ControlVertices.Grow((u16) cvCount) );
					}
					else
					{
						curve->m_ControlVertices.PushAndGrow( pt, (u16) cvCount );
					}
				}

				curveGroup->m_ContainedObjects.PushAndGrow( curve );
			}
		}		
	}	

	// set up return values
	outputObject = curveGroup;
	
	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyCurvesRelativeToBone::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterNurbsCurve* conv = dynamic_cast<rexConverterNurbsCurve*>( module.m_Converter );
	if( conv )
	{
		conv->SetCurvesRelativeToBone( value.toBool( conv->GetCurvesRelativeToBone() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

} // namespace rage
