// 
// rexMayaToGeneric/converterCloth.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __REX_MAYAROCKSTAR_CONVERTERCLOTH_H__
#define __REX_MAYAROCKSTAR_CONVERTERCLOTH_H__

#define _BOOL

#include "atl/array.h"
#include "rexBase/converter.h"
#include "rexGeneric/objectCloth.h"

namespace rage
{

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterClothMeshGroup : public rexConverter
{
public:
	virtual rexConverter* CreateNew() const  { return new rexConverterClothMeshGroup; }

protected:
	virtual rexResult Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterCharClothMeshGroup : public rexConverter
{
public:
	virtual rexConverter* CreateNew() const  { return new rexConverterCharClothMeshGroup; }

protected:
	virtual rexResult Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterClothMeshInfo : public rexConverter
{
public:
	virtual rexConverter* CreateNew() const  { return new rexConverterClothMeshInfo; }

	void SetClothMeshNode(MDagPath& dp); 
	void AppendClothBound(MDagPath& dp); 

protected:
	virtual rexResult Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;

	MDagPath				m_clothMesh;
	atArray<MDagPath>		m_clothBounds;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyClothMeshInfoMesh : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyClothMeshInfoMesh; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyClothMeshInfoBounds : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyClothMeshInfoBounds; }
};

/////////////////////////////////////////////////////////////////////////////////////

}// namespace rage

#endif //__REX_MAYAROCKSTAR_CONVERTERCLOTH_H__
