#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"
#include "rexGeneric/objectLight.h"
#include "rexMayaToGeneric/converterLight.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MColor.h>
#include <Maya/MFloatVector.h>
#include <Maya/MFnDagNode.h>
#include <Maya/MFnLight.h>
#include <Maya/MFnPointLight.h>
#include <Maya/MMatrix.h>
#include <Maya/MString.h>
#pragma warning(pop)



/////////////////////////
namespace rage {

rexResult rexConverterLight::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& /*defaultName*/ ) const
{
	rexResult result( rexResult::PERFECT );

	// create object 
	rexObjectGenericLightGroup* lightGroup = new rexObjectGenericLightGroup;

	int inputObjectCount = inputObjectArray.GetCount();

	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );

	for( int a = 0; a < inputObjectCount; a++ )
	{		
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)( a );

		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( inputObjectArray[ a ] );

		if( dpObj )
		{
			MDagPath& dp = dpObj->m_DagPath;

			if( m_ProgressBarTextCB )
			{
				static char message[1024];
				sprintf( message, "Converting light data at %s", dpObj->m_DagPath.partialPathName().asChar() );
				(*m_ProgressBarTextCB) ( message );
			}

			if( ( ( dp.apiType() == MFn::kDirectionalLight ) || ( dp.apiType() == MFn::kSpotLight ) || ( dp.apiType() == MFn::kAmbientLight ) || ( dp.apiType() == MFn::kPointLight )) )
			{
				MDagPath parent( dp );
				parent.pop();
				MFnDagNode fnParent( parent );
				MFnLight fnLight(dp);
				
				rexObjectGenericLight* light = new rexObjectGenericLight;
				light->m_BoneIndex = dpObj->m_BoneIndex;
				light->m_RoomIndex = dpObj->m_GroupIndex;
				light->SetName( fnParent.name().asChar() );

				Matrix34 m;
				rexMayaUtility::GetMatrix34FromMayaMatrix( m, dp.inclusiveMatrix() );
				light->m_Location.Set( m.d );
				
				MFloatVector dir = fnLight.lightDirection( 0, MSpace::kWorld );
				light->m_Direction.Set( (float)dir.x, (float)dir.y, (float)dir.z );

				short decay = 0;	// no decay
				if( dp.apiType() == MFn::kDirectionalLight )
					light->m_LightType = rexObjectGenericLight::LIGHT_DIRECTIONAL;
				else if( dp.apiType() == MFn::kPointLight ) {
					light->m_LightType = rexObjectGenericLight::LIGHT_POINT;
					MFnPointLight fnPointLight(dp);
					MStatus status;
					decay = fnPointLight.decayRate(&status);
					// printf("got a decay rate = %d",decay);
				}
				else if( dp.apiType() == MFn::kSpotLight )
					light->m_LightType = rexObjectGenericLight::LIGHT_SPOTLIGHT;
				else if( dp.apiType() == MFn::kAmbientLight )
					light->m_LightType = rexObjectGenericLight::LIGHT_AMBIENT;

				MColor color = fnLight.color();
				light->m_Color.Set( (float)color.r, (float)color.g, (float)color.b, (float)color.a );
				light->m_Intensity = fnLight.intensity();
				light->m_DecayRate = decay;

				MPlug plug;
				MStatus status;
				plug = fnLight.findPlug( "lightRadius", &status );
				if( status == MS::kSuccess )
					plug.getValue( light->m_CastShadowRange );

				plug = fnLight.findPlug( "coneAngle", &status );
				if( status == MS::kSuccess )
					plug.getValue( light->m_SpotAngle );

				lightGroup->m_ContainedObjects.PushAndGrow( light,(u16) inputObjectCount );
			}
		}		
	}	

	// set up return values
	outputObject = lightGroup;

	return result;
}

} // namespace rage

/////////////////////////

