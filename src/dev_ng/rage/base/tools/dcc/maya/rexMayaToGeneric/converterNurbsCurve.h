// 
// rexMayaToGeneric/converterNurbsCurve.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterNurbsCurve
//			 -- gathers all of entity's curves
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexConverter
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTERNURBSCURVE_H__
#define __REX_MAYAROCKSTAR_CONVERTERNURBSCURVE_H__

#include "atl/array.h"
#include "rexBase/converter.h"

class MDagPath;

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterNurbsCurve : public rexConverter
{
public:
	rexConverterNurbsCurve() : rexConverter() { m_UseChildrenOfLevelInstanceNodes = false; m_CurvesRelativeToBone = false; }

	virtual rexResult		Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexConverter*   CreateNew() const  { return new rexConverterNurbsCurve; }

	void SetUseChildrenOfLevelInstanceNodes( bool b )						{ m_UseChildrenOfLevelInstanceNodes = b; }
	bool GetUseChildrenOfLevelInstanceNodes() const							{ return m_UseChildrenOfLevelInstanceNodes; }

	void SetCurvesRelativeToBone( bool b )								{ m_CurvesRelativeToBone = b; }
	bool GetCurvesRelativeToBone() const								{ return m_CurvesRelativeToBone; }

protected:
    bool m_UseChildrenOfLevelInstanceNodes;
	bool m_CurvesRelativeToBone;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyCurvesRelativeToBone : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyCurvesRelativeToBone; }
};

} // namespace rage

#endif
