// 
// rexMayaToGeneric/converterFragmentDamage.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterFragmentDamage
//			 -- converts fragment type information damaged states
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTERFRAGMENTDAMAGE_H__
#define __REX_MAYAROCKSTAR_CONVERTERFRAGMENTDAMAGE_H__

#include "converterBasic.h"
#include "rexGeneric/objectFragment.h"

/////////////////////////////////////////////////////////////////////////////////////
namespace rage {

class rexConverterFragmentDamageHierarchy : public rexConverterHierarchy
{
public:
	virtual rexConverter *CreateNew() const  { return new rexConverterFragmentDamageHierarchy; }
	
protected:
	virtual rexObjectHierarchy *CreateHierarchyNode() const { return new rexObjectGenericFragmentHierarchy(true); }
	virtual rexResult			ConvertSubObject( const rexObject& object, rexObject*& newObject ) const;
	virtual bool IsAcceptableHierarchyNode(const MDagPath &dp) const;
};

/////////////////////////////

} // namespace rage

#endif // __REX_MAYAROCKSTAR_CONVERTERFRAGMENTDAMAGE_H__
