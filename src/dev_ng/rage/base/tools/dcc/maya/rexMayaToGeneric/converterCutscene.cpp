
#include "converterCutscene.h"

#include "rexGeneric/objectCutscene.h"

#include "rexBase/module.h"
#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"

#include "vector/matrix34.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include "maya/MDagPath.h"
#include "maya/MFnCamera.h"
#include "maya/MGlobal.h"
#include "maya/MMatrix.h"
#include "maya/MPlug.h"
#include "maya/MStatus.h"
#include "maya/MString.h"
#include "maya/MStringArray.h"
#pragma warning(pop)

namespace rage {

// rexConverterCutscene
rexResult rexConverterCutscene::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	rexObjectCutscene* cs = new rexObjectCutscene;
	cs->SetName( defaultName );

	outputObject = cs;

	return result;
}

// rexConverterCSCast
rexResult rexConverterCSCast::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	int inputObjectCount = inputObjectArray.GetCount();

	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );

	rexObjectCSCast* cast = new rexObjectCSCast;

	cast->mReferenced = false;
	cast->mCategory = defaultName;
	cast->mType = defaultName;
	cast->mName = defaultName;

	outputObject = cast;

	return result;
}

// rexConverterCSData
rexResult rexConverterCSData::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	int inputObjectCount = inputObjectArray.GetCount();

	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );

	rexObjectCSData* data = new rexObjectCSData;

	data->mType = defaultName;
	data->mFileName = defaultName;

	outputObject = data;

	return result;
}

// rexConverterCSEvent
rexResult rexConverterCSEvent::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	int inputObjectCount = inputObjectArray.GetCount();

	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );

	rexObjectCSEvent* event = new rexObjectCSEvent;
	event->SetName( defaultName );
	event->SetType( mEventType );
	outputObject = event;

	// If the event type is "EMITPARTICLES", get the emitter node.
	// Then find its parent bone node and compute offset
	if( mEventType != "EMITPARTICLES" )
		return result;

	// Find the emitter node.
	MDagPath dpEmitter;
	bool emitterFound = rexMayaUtility::FindNode(mEmitterName, dpEmitter);
	if( !emitterFound )
		return result;

	// Find the parent bone node of the emitter node.
	MDagPath dpBone(dpEmitter);
	dpBone.pop();
	while(1)
	{
		if( dpBone.apiType() == MFn::kTransform )
			break;

		if( dpBone.apiType() == MFn::kJoint )
			break;

		// The world has been reached.
		if( dpBone.apiType() == MFn::kWorld )
			break;

		// Next parent.
		dpBone.pop();
	}

	// The parent bone not found, return.
	if( dpBone.apiType() == MFn::kWorld )
		return result;

	// Compute emitter to parent bone offset.
	MMatrix M = dpEmitter.inclusiveMatrix() * dpBone.inclusiveMatrixInverse();

	// Save bone name and offset into rexObjectCSEvent node.
	event->SetParentBone( dpBone.partialPathName().asChar() );
	event->SetOffsetX( (float)(M(3, 0)) );
	event->SetOffsetY( (float)(M(3, 1)) );
	event->SetOffsetZ( (float)(M(3, 2)) );

	return result;
}

// rexConverterCSCamera
rexResult rexConverterCSCamera::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	int inputObjectCount = inputObjectArray.GetCount();
	if( inputObjectCount > 1 )
	{
		// Should be one camera only.
		// No more than one camera allowed
		return result;
	}

	// Get maya frames
/*
	int sf = -1;
	int ef = -1;
	rexMayaUtility::GetMayaSceneStartAndEndFrames(sf, ef);
	if( sf < 0 || ef < 0 || ef <= sf )
	{
		// frame numbers are wrong.
		return result;
	}
*/

	rexObjectCSCamera* rexObjCamera = new rexObjectCSCamera;
	rexObjCamera->SetName( defaultName );
	Assert( rexObjCamera );

/*
	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );
	if( m_ProgressBarObjectCurrentIndexCB )
		(*m_ProgressBarObjectCurrentIndexCB)( 0 );

	rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( inputObjectArray[0] );
	if( dpObj == NULL )
	{
		// Bad rex object.
		return result;
	}

	const MDagPath& dpCamera = dpObj->m_DagPath;
	if( dpCamera.apiType() != MFn::kCamera )
	{
		// This is not a maya camera node.
		return result;
	}

	if( m_ProgressBarTextCB )
	{
		static char msg[1024];
		sprintf( msg, "Converting animation at %s",dpCamera.fullPathName().asChar() );
		(*m_ProgressBarTextCB)( msg );
	}

	// Set header info
	rexObjCamera->SetMagic( MAKE_MAGIC_NUMBER('A','N','I','2') );
	rexObjCamera->SetFlags( 0 );
	rexObjCamera->SetNumFrames( (ef - sf + 1) );
	// Currently support 15 channels
	rexObjCamera->SetNumChannels( 15 );
	rexObjCamera->SetStride( 0.0f, 0.0f, 0.0f );

	// Get frame data
	rexObjCamera->Init();

	MFnCamera fnCamera( dpCamera );

	// Get mover matrix
	bool applyMover = false;
	Matrix34 MInvMover;
	MDagPath dpMover;
	atString moverName = "mover";
	bool foundMover = rexMayaUtility::FindNode(moverName, dpMover);
	if( foundMover )
	{
		rexMayaUtility::GetMatrix34FromMayaMatrix(MInvMover, dpMover.inclusiveMatrixInverse());
		applyMover = true;
	}

	// Aperture and FocalPoint plug
	MStatus apertureExist;
	MPlug plugAperture = fnCamera.findPlug( "csAperture", &apertureExist );
	MStatus fpExist;
	MPlug plugFocalPoint = fnCamera.findPlug( "csFocalPoint", &fpExist );

	float data[15];
	for( int i = sf; i <= ef; i++ )
	{
		rexMayaUtility::SetFrame(i);
		Matrix34 mtx;
		rexMayaUtility::GetMatrix34FromMayaMatrix(mtx, dpCamera.inclusiveMatrix());

		if( applyMover )
			mtx.Dot(MInvMover);

		// n8 hack -- takes care of letterbox not actually being viewport		
		// fnCamera.setFocalLength( fnCamera.focalLength() * 1.20f );		
		double fov = fnCamera.horizontalFieldOfView();
		fov *= ( 180.0f / PI );

		// Get aperture value.
		float aperture = 0.0f;
		if( apertureExist == MS::kSuccess )
			plugAperture.getValue(aperture);

		// Get focalPoint value.
		int focalPoint = 255;
		if( fpExist == MS::kSuccess )
			plugFocalPoint.getValue(focalPoint);

		data[ 0 ] = mtx.a.x;
		data[ 1 ] = mtx.a.y;
		data[ 2 ] = mtx.a.z;
		data[ 3 ] = mtx.b.x;
		data[ 4 ] = mtx.b.y;
		data[ 5 ] = mtx.b.z;
		data[ 6 ] = mtx.c.x;
		data[ 7 ] = mtx.c.y;
		data[ 8 ] = mtx.c.z;
		data[ 9 ] = mtx.d.x;
		data[ 10 ] = mtx.d.y;
		data[ 11 ] = mtx.d.z;
		data[ 12 ] = (float)fov;
		data[ 13 ] = aperture;
		data[ 14 ] = (float)focalPoint;

		// Store channel data into rexObjCamera
		rexObjCamera->SetFrameData(i-sf, 15, data);
	}
*/
	outputObject = rexObjCamera;

	return result;
}

////////////////////////////////////////////////////////////////////////////////
//		Property classes
////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyEventType::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterCSEvent* conv = dynamic_cast<rexConverterCSEvent*>(module.m_Converter);
	if( !conv )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	conv->SetEventType( value.toString() );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyCSStartFrame::ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectCSEvent* event = dynamic_cast<rexObjectCSEvent*>( &obj );
	if( !event )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	float sf = 0.0f;
	rexMayaUtility::GetMayaSceneStartFrame( sf );
	event->SetSceneStartFrame( (int)sf );
	event->SetStartFrame( value.toInt() );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyCSEndFrame::ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectCSEvent* event = dynamic_cast<rexObjectCSEvent*>( &obj );
	if( !event )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	event->SetEndFrame( value.toInt() );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyEventString::ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectCSEvent* event = dynamic_cast<rexObjectCSEvent*>( &obj );
	if( !event )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	event->SetEventString( value.toString() );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyEventFloat::ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectCSEvent* event = dynamic_cast<rexObjectCSEvent*>( &obj );
	if( !event )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	event->SetEventGenericFloat( value.toFloat() );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyAudioStream::ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectCSEvent* event = dynamic_cast<rexObjectCSEvent*>( &obj );
	if( !event )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	event->SetStreamName( value.toString() );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyEmitterName::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterCSEvent* conv = dynamic_cast<rexConverterCSEvent*>(module.m_Converter);
	if( !conv )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	conv->SetEmitterName( value.toString() );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyEmitterType::ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectCSEvent* event = dynamic_cast<rexObjectCSEvent*>( &obj );
	if( !event )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	event->SetEmitterType( value.toString() );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyParentActor::ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectCSEvent* event = dynamic_cast<rexObjectCSEvent*>( &obj );
	if( !event )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	event->SetParentActor( value.toString() );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyClipNear::ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectCSEvent* event = dynamic_cast<rexObjectCSEvent*>( &obj );
	if( !event )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	event->SetClipNear( value.toFloat(0.01f) );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyClipFar::ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectCSEvent* event = dynamic_cast<rexObjectCSEvent*>( &obj );
	if( !event )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	event->SetClipFar( value.toFloat(500.0f) );
	return rexResult( rexResult::PERFECT );
}

rexResult rexPropertyCSBookmarks::ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectCutscene* cs = dynamic_cast<rexObjectCutscene*>( &obj );
	if( !cs )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	MString mayaStr = (const char*)(value.toString());
	MStringArray bookmarks;
	mayaStr.split( ' ', bookmarks );
	cs->SetNumBookmarks( bookmarks.length() );
	cs->SetBookmarks( value.toString() );
	return rexResult( rexResult::PERFECT );
}

} // namespace rage



