// 
// rexMayaToGeneric/converterCutscene.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTER_CUTSCENE_H__
#define __REX_MAYAROCKSTAR_CONVERTER_CUTSCENE_H__

#include "rexBase/converter.h"
#include "rexBase/object.h"
#include "rexBase/property.h"

#include "atl/array.h"
#include "atl/string.h"

namespace rage {

////////////////////////////////////////////////////////////////////////////////
//	rexConverterCutscene - converts cutscene information
//
//	INHERITS FROM: rexConverter
////////////////////////////////////////////////////////////////////////////////
class rexConverterCutscene : public rexConverter
{
public:
	virtual rexConverter	*CreateNew() const { return new rexConverterCutscene; }
	virtual rexResult		Convert(const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName) const;
};

////////////////////////////////////////////////////////////////////////////////
//	rexConverterCSCast - converts CSCast information
//
//	INHERITS FROM: rexConverter
////////////////////////////////////////////////////////////////////////////////
class rexConverterCSCast : public rexConverter
{
public:
	virtual rexConverter	*CreateNew() const { return new rexConverterCSCast; }
	virtual rexResult		Convert(const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName) const;
};

////////////////////////////////////////////////////////////////////////////////
//	rexConverterCSData - converts CSData information
//
//	INHERITS FROM: rexConverter
////////////////////////////////////////////////////////////////////////////////

class rexConverterCSData : public rexConverter
{
public:
	virtual rexConverter	*CreateNew() const { return new rexConverterCSData; }
	virtual rexResult		Convert(const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName) const;
};

////////////////////////////////////////////////////////////////////////////////
//	rexConverterCSEvent - converts CSEvent information
//
//	INHERITS FROM: rexConverter
////////////////////////////////////////////////////////////////////////////////
class rexConverterCSEvent : public rexConverter
{
public:
	virtual rexConverter	*CreateNew() const { return new rexConverterCSEvent; }
	virtual rexResult		Convert(const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName) const;

	//
	void SetEventType(const char *type)	{ mEventType = type; }
	void SetEmitterName(const char *name)	{ mEmitterName = name; }

protected:
	atString	mEventType;
	atString	mEmitterName;
};

////////////////////////////////////////////////////////////////////////////////
//	rexConverterCSCamera - converts CSCamera information
//
//	INHERITS FROM: rexConverter
////////////////////////////////////////////////////////////////////////////////
class rexConverterCSCamera : public rexConverter
{
public:
	virtual rexConverter	*CreateNew() const { return new rexConverterCSCamera; }
	virtual rexResult		Convert(const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName) const;
};

////////////////////////////////////////////////////////////////////////////////
//	rexConverterCSLight - converts CSLight information
//
//	INHERITS FROM: rexConverter
////////////////////////////////////////////////////////////////////////////////
class rexConverterCSLight : public rexConverter
{
public:
	virtual rexConverter	*CreateNew() const { return new rexConverterCSLight; }
	virtual rexResult		Convert(const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName) const;
};

////////////////////////////////////////////////////////////////////////////////
//		Property classes
////////////////////////////////////////////////////////////////////////////////

class rexPropertyEventType : public rexProperty
{
public:
	virtual rexResult	SetupModuleInstance(rexModule& module, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEventType; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyCSStartFrame : public rexProperty
{
public:
	virtual rexResult	ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyCSStartFrame; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyCSEndFrame : public rexProperty
{
public:
	virtual rexResult	ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyCSEndFrame; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEventString : public rexProperty
{
public:
	virtual rexResult	ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEventString; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEventFloat : public rexProperty
{
public:
	virtual rexResult	ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEventFloat; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyAudioStream : public rexProperty
{
public:
	virtual rexResult	ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyAudioStream; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEmitterName : public rexProperty
{
public:
	virtual rexResult	SetupModuleInstance(rexModule& module, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEmitterName; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyEmitterType : public rexProperty
{
public:
	virtual rexResult	ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyEmitterType; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyParentActor : public rexProperty
{
public:
	virtual rexResult	ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyParentActor; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyClipNear : public rexProperty
{
public:
	virtual rexResult	ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyClipNear; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyClipFar : public rexProperty
{
public:
	virtual rexResult	ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyClipFar; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyCSBookmarks : public rexProperty
{
public:
	virtual rexResult	ModifyObject(rexObject& obj, rexValue value, const atArray<rexObject*>& objects) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyCSBookmarks; }
};

} // namespace rage

#endif
