#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"

#include "rexGeneric/objectEdgeModel.h"
#include "rexMayaToGeneric/converterEdgeModel.h"

/////////////////////////////////////////////////////////////////////
namespace rage {

rexObjectHierarchy*	rexConverterMeshHierarchyEdgeModel::CreateHierarchyNode() const
{
	return new rexObjectGenericEdgeModelHierarchy;
}

/////////////////////////////////////////////////////////////////////
// virtual
rexResult rexConverterMeshHierarchyEdgeModel::ConvertMayaNode(const MDagPath &dp, int boneIndex, const Matrix44& boneMatrix, int lodGroupIndex, int lodLevel, float lodThreshold, const atString* groupID, int groupIndex, rexObject *&newObject) const
{
	newObject = NULL;

	rexObjectGenericMesh *mesh = new rexObjectGenericMesh();
	if(!mesh)
		return rexResult(rexResult::ERRORS, rexResult::OUT_OF_MEMORY);

	rexResult retval;

	if( (retval = rexMayaUtility::ConvertMayaMeshNodeIntoGenericMesh( mesh, dp, boneIndex, &boneMatrix, lodGroupIndex, lodLevel, lodThreshold, groupID, groupIndex, false )).Errors() )
	{
		delete mesh;
		return retval;
	}

	newObject = mesh;

	return retval;
}

/////////////////////////////////////////////////////////////////////
// virtual
rexResult  rexPropertyAllowMultipleEdgeModels::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectGenericEdgeModelHierarchy* emh = dynamic_cast<rexObjectGenericEdgeModelHierarchy*>( &obj );

	if( !emh )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	emh->m_AllowMultipleEdgeModels = value.toBool();

	return rexResult( rexResult::PERFECT );
}

} // namespace rage

