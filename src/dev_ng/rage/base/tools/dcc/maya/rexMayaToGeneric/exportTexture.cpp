//
// rexMayaToGeneric/exportTexture.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "exportTexture.h"

#include "dataacc/mySQLDataAcc.h"
#include "devil/il.h"
#include "devil/ilu.h"
#include "file/stream.h"
#include "file/token.h"
#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"


#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MGlobal.h>
#include <Maya/MString.h>
#pragma warning(pop)


namespace rage {
static int MakePow2AtLeast16(int v) {
	// Rounds v up to next power of two if it isn't already
	while ((v<16) || (v & -v) != v)
		++v;
	return v;
}


// static variables
namespace
{

/*
PURPOSE
struct match the row of the table of the .MDB file.
*/

////////////////

class dbRow
{
protected:
	struct Entry
	{
	public:
//		Entry()  { Name = NULL; MaxLength = 0; Data = NULL; }
		Entry( const char* name, int length )  { Name = StringDuplicate( name ); MaxLength = length; Data = new char[ length ]; *Data = 0; }
		~Entry()  { if( Name ) delete [] Name; if( Data ) delete [] Data; }
//		Entry( Entry& e )  { Name = StringDuplicate( e.Name ); MaxLength = e.MaxLength; Data = new char[ MaxLength ]; strncpy( Data, e.Data, MaxLength );  }

		char*	Name;
		int		MaxLength;
		char*	Data;
	};

public:
	char* GetValue( const char* name ) const
	{
		int count = mEntries.GetCount();
		for( int a = 0; a < count; a++ )
		{
			if( !strcmp( mEntries[ a ]->Name, name ) )
			{
				return mEntries[ a ]->Data;
			}			
		}
		return NULL;			
	}

	int GetEntryCount() const
	{
		return mEntries.GetCount();
	}
	
	const char* GetName( int index ) const
	{
		if( index < 0 || index >= mEntries.GetCount() ) return NULL;
		return mEntries[ index ]->Name;
	}

	void AddEntry( const char* name, int length )
	{
		Entry* entry = new Entry( name, length );
		mEntries.PushAndGrow( entry );
	}

	void CopyFromCharPtrPtr( char** row )
	{
		int count = mEntries.GetCount();
		for( int a = 0; a < count; a++ )
			strncpy( mEntries[ a ]->Data, row[ a ], mEntries[ a ]->MaxLength );
	}

	virtual ~dbRow()
	{
		while( mEntries.GetCount() )
			delete mEntries.Pop();
	}

protected:
	atArray<Entry*> mEntries;
};

////////////////

class ourRow : public dbRow
{
public:
	ourRow()
	{
		AddEntry( "SrcImg", 256 );
		AddEntry( "DestImg", 256 );
		AddEntry( "Type", 256 );

		AddEntry( "SrcResX", 8 );
		AddEntry( "SrcResY", 8 );
		AddEntry( "SrcFrameCount", 8 );
		AddEntry( "DestQuantizer", 16 );
		
		AddEntry( "DestResX_PS", 8 );
		AddEntry( "DestResY_PS", 8 );
		AddEntry( "DestFormat_PS", 16 );
		AddEntry( "DestMips_PS", 8 );
		AddEntry( "DestFrameSkip_PS", 8 );
		AddEntry( "DestFlag1_PS", 16 );
		AddEntry( "DestFlag2_PS", 16 );
		
		AddEntry( "DestResX_XB", 8 );
		AddEntry( "DestResY_XB", 8 );
		AddEntry( "DestFormat_XB", 16 );
		AddEntry( "DestMips_XB", 8 );
		AddEntry( "DestFrameSkip_XB", 8 );
		AddEntry( "DestFlag1_XB", 16 );
		AddEntry( "DestFlag2_XB", 16 );

		AddEntry( "DestResX_NG", 8 );
		AddEntry( "DestResY_NG", 8 );
		AddEntry( "DestFormat_NG", 16 );
		AddEntry( "DestMips_NG", 8 );
		AddEntry( "DestFrameSkip_NG", 8 );
		AddEntry( "DestFlag1_NG", 16 );
		AddEntry( "DestFlag2_NG", 16 );

		AddEntry( "DestResX_PC", 8 );
		AddEntry( "DestResY_PC", 8 );
		AddEntry( "DestFormat_PC", 16 );
		AddEntry( "DestMips_PC", 8 );
		AddEntry( "DestFrameSkip_PC", 8 );
		AddEntry( "DestFlag1_PC", 16 );
		AddEntry( "DestFlag2_PC", 16 );

		AddEntry( "DestClampS", 8 );
		AddEntry( "DestClampT", 8 );
		AddEntry( "DestTransparent", 8 );
		AddEntry( "DestFlag1", 16 );
		AddEntry( "DestFlag2", 16 );
		AddEntry( "DestFlag3", 16 );
	
		AddEntry( "Checksum", 32 );
		AddEntry( "Timestamp", 64 );
	}
};

ourRow *TableRow = NULL;

mySQLDataAccess* sMySQLDataAccess = NULL;


/*
PURPOSE
	Release the MS Jet database.
PARAMS
	none.
RETURN
	none.
NOTES
*/
static bool dbInitted = false;
void FreeDBConnection()
{
	if( !dbInitted ) return;
	if(TableRow)
	{
		delete TableRow;
		TableRow = NULL;
	}
	if( sMySQLDataAccess )
	{
		delete sMySQLDataAccess;
		sMySQLDataAccess = NULL;
	}
	dbInitted = false;
}

/*
PURPOSE
	Initialize MS Jet database and TableRow struct.
PARAMS
	mdb - mdb file name.
	table - MDB table name.
	readWriteMode - false for read, true for write.
RETURN
	none.
NOTES
	This function is used for read only.
*/
static bool sCantConnectWarningPrinted = false;

bool InitDBConnection(const char* hostname, const char* dbname, const char* login, const char* password, bool /*readWriteMode*/)
{	
	if( !dbInitted )
	{
		sMySQLDataAccess = new mySQLDataAccess( hostname, login, password, dbname ); 
		if( !sMySQLDataAccess->IsConnected() )
		{
			if( !sCantConnectWarningPrinted )
			{
				Displayf( "Can't connect to texture DB! hostname = '%s', dbname = '%s'", hostname, dbname );
				fflush(stdout);
			}
			sCantConnectWarningPrinted = true;
			FreeDBConnection();
			return false;
		}			
		dbInitted = true;
	}

	if( TableRow ) delete TableRow;
	TableRow = new ourRow;
	return true;
}

/*
PURPOSE
	Read one row from database.
PARAMS
	none.
RETURN
	true if find the row.
NOTES
*/

void RowToTableRow( char** row, ourRow& dbrow )
{
	dbrow.CopyFromCharPtrPtr( row );
}

bool GetRow(ourRow& myRow, const char* tablename, const char* imgSrc)
{
	if( !sMySQLDataAccess->IsConnected() )
		return false;

	atString sql;
	sql = "SELECT ";
	int count = myRow.GetEntryCount();
	for( int a = 0; a < count; a++ )
	{
		if( a > 0 ) sql += ",";
		sql += myRow.GetName( a );		
	}
	sql += " FROM ";
	sql += tablename;
	sql += " WHERE SrcImg='";
	sql += imgSrc;
//	sql += "' AND DestImg='";
//	sql += imgDest;
	sql += "';";	
	int rowCount = sMySQLDataAccess->Query( sql );

	if( rowCount > 0 )
	{
		char** row = sMySQLDataAccess->GetQueryResultRow();
		Assert( sMySQLDataAccess->GetQueryResultFieldCount() == myRow.GetEntryCount() );
		RowToTableRow( row, myRow );		
	}	

	return ( rowCount > 0 );
}

}

/*
PURPOSE
	Constructor.
PARAMS
	none.
RETURN
	none.
NOTES
*/
//atArray<atString> rexTextureExporter::smValidSourcePaths, rexTextureExporter::smValidOutputPaths;

atArray<rexTextureExporter::SourceDirectoryInfo> rexTextureExporter::smSourceDirectoryInfo;

rexTextureExporter::rexTextureExporter(const char* fileName)
{
	Reset();

	if( !smSourceDirectoryInfo.GetCount() )
	{		
		fiStream* s = fiStream::Open(fileName);
		if( s )
		{
			fiAsciiTokenizer tok;
			tok.Init( fileName, s );
			char buffer[512];
			while( tok.GetToken( buffer, sizeof( buffer ) ) > 1 )
			{
				SourceDirectoryInfo& sdi = smSourceDirectoryInfo.Grow();
				atString t(buffer);
				
				rexUtility::ConvertBackslashesToSlashes( rexUtility::ToLowerCase( t ), sdi.srcDir );
				tok.GetToken( buffer, sizeof( buffer ) );
				sdi.tableName = buffer;
			}
			s->Close();
		}
	}
}

/*
PURPOSE
	Destructor
PARAMS
	none.
RETURN
	none.
NOTES
*/
rexTextureExporter::~rexTextureExporter()
{
	FreeDBConnection();
}

/*
PURPOSE
	Reset this exporter.
PARAMS
	none.
RETURN
	none.
NOTES
*/
void rexTextureExporter::Reset()
{
//	sCantConnectWarningPrinted = false;
	mPlatform = PLAYSTATION;
	mFrames = 0;
	mSrcResolutionX = 32;
	mSrcResolutionY = 32;
	mClampS = false;
	mClampT = false;
	mAddNewTexturesToDatabase = false;
	mIfSourceResolutionChangedAskToUpdate = true;
	mChecksum = 0;
	mTimestamp = 0;
	mType = "default";
	mUsePathValidationFiles = true;

	mQuantizerType = "PADIE";

	for( int a = 0; a < NUM_PLATFORMS; a++ )
	{
		mPlatformInfo[a].mMips = 1;
		mPlatformInfo[a].mFormatType = "P4";
		mPlatformInfo[a].mResolutionX = 32;
		mPlatformInfo[a].mResolutionY = 32;
		mPlatformInfo[a].mFrameSkip = 0;
	}
}

bool rexTextureExporter::QueryDB( const atString& srcImg, atArray<atString>& fieldNames, atArray<atString>& fieldValues )
{
	if( !InitDBConnection( mDBHostName, mDBName, mDBLogin, mDBPassword.GetLength() ? (const char*)mDBPassword : NULL, true ) )
	{
		MString err = "Could not establish connection with texture DB: host '";
		err += (const char*)mDBHostName;
		err += "', db '";
		err += (const char*)mDBName;
		err += "', login '";
		err += (const char*)mDBLogin;
		err += "', password '";
		err += ( mDBPassword.GetLength() ? (const char*)mDBPassword : "NULL" );
		err += "'";
		MGlobal::displayError( err );		
		return false;
	}

	if( !fieldNames.GetCount() )
	{
		fieldNames.Grow() = "DestResX_PS";
		fieldNames.Grow() = "DestResY_PS";
		fieldNames.Grow() = "DestQuantizer";
		fieldNames.Grow() = "DestClampS";
		fieldNames.Grow() = "DestClampT";
		fieldNames.Grow() = "DestFormat_PS";
		fieldNames.Grow() = "DestMips_PS";
		fieldNames.Grow() = "DestFrameSkip_PS";		
	}

	int a, pathCount;
	bool isInputValid = false; 

	pathCount = smSourceDirectoryInfo.GetCount();
	atString t;
	rexUtility::ConvertBackslashesToSlashes( rexUtility::ToLowerCase( srcImg ), t );
	for( a = 0; a < pathCount; a++ )
	{
		if( !strncmp( t, smSourceDirectoryInfo[ a ].srcDir, smSourceDirectoryInfo[ a ].srcDir.GetLength() ) )
		{
			isInputValid = true;
			mDBTableName = smSourceDirectoryInfo[ a ].tableName;
			break;
		}
	}

	if( !isInputValid )
		return false;

	bool setValues = ( fieldValues.GetCount() > 0 );

	if( setValues )
	{
		if( !sMySQLDataAccess ) return MS::kFailure;

		char buffer[512];
		int valueCount = fieldValues.GetCount();
		for( int a = 0; a < valueCount; a++ )
		{
			sprintf( buffer, "UPDATE %s SET %s = '%s' WHERE SrcImg = '%s'", (const char*)mDBTableName, (const char*)fieldNames[ a ], (const char*)fieldValues[ a ], (const char*)t );
			sMySQLDataAccess->Query( buffer );
		}		
	}
	else
	{
		ourRow myRow;
		bool found = GetRow( myRow, mDBTableName, t );
		if( !found )
			return false;
		int fieldCount = fieldNames.GetCount();
		for( int a = 0; a < fieldCount; a++ )
		{
			fieldValues.Grow((u16) fieldCount) = myRow.GetValue( fieldNames[ a ] );
		}
	}
	return true;
}
/*
PURPOSE
Wrapper to make sure devil is properly cleaned up for all exit paths of exportnow
 */
bool rexTextureExporter::Export( const atString& input, const atString& output, bool hasAlpha, bool ps2LightMap ) 
{
	rexUtility::BeginDevil();

	bool retVal = ExportNow(input, output, hasAlpha, ps2LightMap );	

	rexUtility::EndDevil();

	return retVal;
}

/*
PURPOSE
Export the texture.
PARAMS
none.
RETURN
MS::kSuccess if success. Otherwise, MS::kFailure.
NOTES
*/
bool rexTextureExporter::ExportNow( const atString& input, const atString& output, bool hasAlpha, bool ps2LightMap )
{
	// Open the MDB for writing.	

	mInputFilename = input;
	mOutputFilename = output;
	mExportAsPS2LightMap = ps2LightMap;

	if( !rexUtility::FileExists( mInputFilename ) )
	{
		MGlobal::displayError( MString( "MISSING SOURCE TEXTURE FILE: " ) + mInputFilename );		
		return false;
	}

	if( !InitDBConnection( mDBHostName, mDBName, mDBLogin, mDBPassword.GetLength() ? (const char*)mDBPassword : NULL, true ) )
	{
		MString err = "Could not establish connection with texture DB: host '";
		err += (const char*)mDBHostName;
		err += "', db '";
		err += (const char*)mDBName;
		err += "', login '";
		err += (const char*)mDBLogin;
		err += "', password '";
		err += ( mDBPassword.GetLength() ? (const char*)mDBPassword : "NULL" );
		err += "'";
		MGlobal::displayError( err );		
	}

	//WriteLogFile(0,"rexAgentTextureExporter::Export", this, "Exporting texture data begin.");

	bool modifyDB = false;
	bool isInputValid = true;

	bool exportIt = !rexUtility::FileExists( mOutputFilename );
	if( !exportIt )
	{
		exportIt = ( rexUtility::GetFileTimestamp( mInputFilename ) > rexUtility::GetFileTimestamp( mOutputFilename ) );
	}

	//	if( smSourceDirectoryInfo.GetCount() )
	{
		int a, pathCount;
		isInputValid = false; 

		pathCount = smSourceDirectoryInfo.GetCount();
		atString t = mInputFilename;
		rexUtility::ConvertBackslashesToSlashes( rexUtility::ToLowerCase( t ), mInputFilename );
		for( a = 0; a < pathCount; a++ )
		{
			if( !strncmp( mInputFilename, smSourceDirectoryInfo[ a ].srcDir, smSourceDirectoryInfo[ a ].srcDir.GetLength() ) )
			{
				isInputValid = true;
				mDBTableName = smSourceDirectoryInfo[ a ].tableName;
				break;
			}
		}
		if( !isInputValid )
		{
			atString messageString( "Input Texture Path '" );
			messageString += mInputFilename;
			messageString += "' is invalid.  Texture will not be exported nor added to texture database.  Allowable paths are:";
			for( a = 0; a < pathCount; a++ )
			{
				messageString += " '";
				messageString += smSourceDirectoryInfo[ a ].srcDir;
				messageString += "'";
			}
			Displayf( (const char*) messageString );
			//rexUtility::MayaMsgBoxOK( messageString, "Invalid Input Texture Path" );
		}
	}

	bool loadedImage=false;
	ILint depth=32,width=32,height=32;

	if( isInputValid && SetupFromRecord() )
	{		
		int checksum = GenerateChecksum();
		if( checksum != mChecksum )
		{
			modifyDB = true;
			exportIt = true;
		}			
	
		double fileTimestamp = rexUtility::GetFileTimestamp( mInputFilename );
		if( mTimestamp < fileTimestamp )
		{
			// load the image:
			loadedImage=LoadSourceImage(depth,width,height);
			if (!loadedImage)
			{
				return false;
			}

			modifyDB = true;
			exportIt = true;

			if( mIfSourceResolutionChangedAskToUpdate )
			{
				if(( width != mSrcResolutionX ) || ( height != mSrcResolutionY ))
				{
					atString message("The resolution of source image '");
					message += mInputFilename;
					message += "' has changed.  Would you like to resync the resolution of the output image?";

					//						if( rexMayaUtility::MessageBoxYesNo( message , "Source Image Resolution Change" ) )
					{
						for( int a = 0; a < NUM_PLATFORMS; a++ )
						{
							mPlatformInfo[a].mResolutionX = width;
							mPlatformInfo[a].mResolutionY = height;
						}
					}
				}
				mSrcResolutionX = width;
				mSrcResolutionY = height;
			}
		}
	}
	else
	{
		// load the image:
		mSrcResolutionX = width;
		mSrcResolutionY = height;
		loadedImage=LoadSourceImage(depth,mSrcResolutionX,mSrcResolutionY);
		if (!loadedImage)
		{
			return false;
		}

		// add it to the database if asked to do so:
		modifyDB = ( isInputValid && mAddNewTexturesToDatabase );
	}		

	// export our texture:
	if( exportIt )
	{
		if (!loadedImage)
		{
			loadedImage=LoadSourceImage(depth,width,height);
			if (!loadedImage)
			{
				return false;
			}
		}

		iluScale(mPlatformInfo[mPlatform].mResolutionX,mPlatformInfo[mPlatform].mResolutionY,depth);
		rexUtility::ConvertAndSaveDevilImage(mOutputFilename,hasAlpha);
	}

	if( modifyDB )
		ModifyMDB();

	// double export
	atString srcPath, destPath;
	rexUtility::ConvertBackslashesToSlashes( rexUtility::ToLowerCase( rexUtility::PathFromPath( input ) ), srcPath );
	int count = mDoubleExportInfo.GetCount();

	for( int c = 0; c < count; c++ )
	{
		while( srcPath.GetLength() > 4 )
		{
			if( mDoubleExportInfo[ c ].input == srcPath )
			{
				destPath = mDoubleExportInfo[ c ].output;
				break;
			}
			rexUtility::PopPath( srcPath );
		}
		if( destPath.GetLength() )
			break;
	}
	if( destPath.GetLength() )
	{
		atString texName = rexUtility::FilenameFromPath( output );
		atString output2 = destPath;
		output2 += "/";
		output2 += texName;
		rexUtility::FileCopy( output, output2 );
	}

	//WriteLogFile(0,"rexAgentTextureExporter::Export", this, "Exporting texture data done.");
	return true;
}

bool rexTextureExporter::LoadSourceImage(int& depth,int& width,int& height)
{
	if (rexUtility::LoadDevilImage(mInputFilename))
	{
		ILinfo info;
		iluGetImageInfo(&info);
		depth=info.Depth;
		width=info.Width;
		height=info.Height;
		return true;
	}
	else 
	{
		MString err = "Could not open texture file '";
		err += (const char*)mInputFilename;
		err += "'";
		MGlobal::displayError( err );		
		return false;
	}
}

bool rexTextureExporter::SetupFromRecord()
{
	ourRow myRow;
	bool found = GetRow( myRow, mDBTableName, mInputFilename );
	if( found )
	{		
		mType = myRow.GetValue( "Type" );
		mSrcResolutionX = atoi( myRow.GetValue( "SrcResX" ) );
		mSrcResolutionY = atoi( myRow.GetValue( "SrcResY" ) );
		mFrames = atoi( myRow.GetValue( "SrcFrameCount" ) );
		mQuantizerType = myRow.GetValue( "DestQuantizer" );

		mClampS = ( myRow.GetValue( "DestClampS" )[0] != '0' );
		mClampT = ( myRow.GetValue( "DestClampT" )[0] != '0' );

		mChecksum = atoi( myRow.GetValue( "Checksum" ) );
		mTimestamp = atof( myRow.GetValue( "Timestamp" ) );

		mPlatformInfo[PLAYSTATION].mResolutionX = MakePow2AtLeast16( atoi( myRow.GetValue( "DestResX_PS" ) ) );
		mPlatformInfo[PLAYSTATION].mResolutionY = MakePow2AtLeast16( atoi( myRow.GetValue( "DestResY_PS" ) ) );
		mPlatformInfo[PLAYSTATION].mFormatType = myRow.GetValue( "DestFormat_PS" );
		mPlatformInfo[PLAYSTATION].mMips = atoi( myRow.GetValue( "DestMips_PS" ) );
		mPlatformInfo[PLAYSTATION].mFrameSkip = atoi( myRow.GetValue( "DestFrameSkip_PS" ) );

		// playstation max size 64K, so make sure it's within limits
		bool is4Bit = ( mPlatformInfo[PLAYSTATION].mFormatType == "PA4" || mPlatformInfo[PLAYSTATION].mFormatType == "P4" );
		while( ( ( mPlatformInfo[PLAYSTATION].mResolutionX * mPlatformInfo[PLAYSTATION].mResolutionY ) ) > ( 65536 * ( is4Bit ? 2 : 1 )) )
		{
			mPlatformInfo[PLAYSTATION].mResolutionX /= 2;
			mPlatformInfo[PLAYSTATION].mResolutionY /= 2;
		}

		mPlatformInfo[XBOX].mResolutionX = MakePow2AtLeast16( atoi( myRow.GetValue( "DestResX_XB" ) ) );
		mPlatformInfo[XBOX].mResolutionY = MakePow2AtLeast16( atoi( myRow.GetValue( "DestResY_XB" ) ) );
		mPlatformInfo[XBOX].mFormatType = myRow.GetValue( "DestFormat_XB" );
		mPlatformInfo[XBOX].mMips = atoi( myRow.GetValue( "DestMips_XB" ) );
		mPlatformInfo[XBOX].mFrameSkip = atoi( myRow.GetValue( "DestFrameSkip_XB" ) );

		mPlatformInfo[NINTENDO].mResolutionX = MakePow2AtLeast16( atoi( myRow.GetValue( "DestResX_NG" ) ) );
		mPlatformInfo[NINTENDO].mResolutionY = MakePow2AtLeast16( atoi( myRow.GetValue( "DestResY_NG" ) ) );
		mPlatformInfo[NINTENDO].mFormatType = myRow.GetValue( "DestFormat_NG" );
		mPlatformInfo[NINTENDO].mMips = atoi( myRow.GetValue( "DestMips_NG" ) );
		mPlatformInfo[NINTENDO].mFrameSkip = atoi( myRow.GetValue( "DestFrameSkip_NG" ) );

		mPlatformInfo[PC].mResolutionX = MakePow2AtLeast16( atoi( myRow.GetValue( "DestResX_PC" ) ) );
		mPlatformInfo[PC].mResolutionY = MakePow2AtLeast16( atoi( myRow.GetValue( "DestResY_PC" ) ) );
		mPlatformInfo[PC].mFormatType = myRow.GetValue( "DestFormat_PC" );
		mPlatformInfo[PC].mMips = atoi( myRow.GetValue( "DestMips_PC" ) );
		mPlatformInfo[PC].mFrameSkip = atoi( myRow.GetValue( "DestFrameSkip_PC" ) );
	}
	return found;
}

int GetNumDefaultMips( int resX, int resY )
{
	int min = Min( resX, resY );
	if( min >= 128 )
		return 4;
	else if( min >= 64 )
		return 3;
	else if( min >= 32 )
		return 2;
	else 
		return 1;
}



/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

/*
PURPOSE
Modify or insert a row of the database table.
PARAMS
imgName - image file name for finding the row.
insertRow - if true, insert a row. if false, modify the row.
RETURN
bool.
NOTES
*/
bool rexTextureExporter::ModifyRow(const char* imgSrc, const char* /*imgDest*/, bool insertRow)
{
	if( !sMySQLDataAccess ) return MS::kFailure;

	// Modify the Row data with SQL command: UPDATE and SET with the filter of
	// WHERE.
	char sql[32768];

	if( !insertRow )
	{
		sprintf(sql,"DELETE FROM %s WHERE SrcImg='%s'", (const char*)mDBTableName, imgSrc );
		sMySQLDataAccess->Query( sql );			
	}

	atString columns;
	ourRow row;
	int eCount = row.GetEntryCount();
	for( int a = 0; a < eCount; a++ )
	{
		if( a != 0 ) columns += ",";
		columns += row.GetName( a );		
	}


	sprintf(sql,"INSERT INTO %s (%s) VALUES"
		"('%s','%s','%s',"
		"'%d','%d','%d','%s',"
		"'%d','%d','%s','%d','%d','0','0',"
		"'%d','%d','%s','%d','%d','0','0',"
		"'%d','%d','%s','%d','%d','0','0',"
		"'%d','%d','%s','%d','%d','0','0',"
		"'%d','%d','0','0','0','0',"
		"'%d','%f' );", 

		(const char*)mDBTableName, (const char*)columns, 
		(const char*)imgSrc, "", (const char*)mType, 
		mSrcResolutionX, mSrcResolutionY, mFrames, (const char*)mQuantizerType,
		mPlatformInfo[PLAYSTATION].mResolutionX, mPlatformInfo[PLAYSTATION].mResolutionY, (const char*)mPlatformInfo[PLAYSTATION].mFormatType, mPlatformInfo[PLAYSTATION].mMips,  mPlatformInfo[PLAYSTATION].mFrameSkip, 
		mPlatformInfo[XBOX].mResolutionX, mPlatformInfo[XBOX].mResolutionY, (const char*)mPlatformInfo[XBOX].mFormatType, mPlatformInfo[XBOX].mMips,  mPlatformInfo[XBOX].mFrameSkip, 
		mPlatformInfo[NINTENDO].mResolutionX, mPlatformInfo[NINTENDO].mResolutionY, (const char*)mPlatformInfo[NINTENDO].mFormatType, mPlatformInfo[NINTENDO].mMips,  mPlatformInfo[NINTENDO].mFrameSkip, 
		mPlatformInfo[PC].mResolutionX, mPlatformInfo[PC].mResolutionY, (const char*)mPlatformInfo[PC].mFormatType, mPlatformInfo[PC].mMips,  mPlatformInfo[PC].mFrameSkip, 
		mClampS, mClampT,
		GenerateChecksum(), rexUtility::GetTimestamp() );

	sMySQLDataAccess->Query( sql );
	return MS::kSuccess;
}

/*
PURPOSE
Modify database table.
PARAMS
none.
RETURN
bool.
NOTES
*/
bool rexTextureExporter::ModifyMDB()
{
	ourRow myRow;
	bool found = GetRow( myRow, mDBTableName, mInputFilename );
	ModifyRow( mInputFilename, mOutputFilename, !found);

	return MS::kSuccess;
}


int rexTextureExporter::GenerateChecksum()
{
	MString inputFilename( mInputFilename );
	MString type( mType );
	inputFilename.toLowerCase();
	type.toLowerCase();

	MString dataString =	inputFilename + type + 
		mSrcResolutionX + mSrcResolutionY + mFrames + MString( (const char*)mQuantizerType ) +
		mPlatformInfo[PLAYSTATION].mResolutionX + mPlatformInfo[PLAYSTATION].mResolutionY + MString( (const char*)mPlatformInfo[PLAYSTATION].mFormatType ) + mPlatformInfo[PLAYSTATION].mMips + mPlatformInfo[PLAYSTATION].mFrameSkip + "0" + "0" +
		mPlatformInfo[XBOX].mResolutionX + mPlatformInfo[XBOX].mResolutionY + MString( (const char*)mPlatformInfo[XBOX].mFormatType ) + mPlatformInfo[XBOX].mMips + mPlatformInfo[XBOX].mFrameSkip + "0" + "0" +
		mPlatformInfo[NINTENDO].mResolutionX + mPlatformInfo[NINTENDO].mResolutionY + MString( (const char*)mPlatformInfo[NINTENDO].mFormatType ) + mPlatformInfo[NINTENDO].mMips + mPlatformInfo[NINTENDO].mFrameSkip + "0" + "0" +
		mPlatformInfo[PC].mResolutionX + mPlatformInfo[PC].mResolutionY + MString( (const char*)mPlatformInfo[PC].mFormatType ) + mPlatformInfo[PC].mMips + mPlatformInfo[PC].mFrameSkip + "0" + "0" +
		mClampS + mClampT + "0" + "0" + "0" + "0" + "0" + "0";							

	return rexUtility::GetChecksum( atString( dataString.asChar() ) );
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////


} // namespace rage
