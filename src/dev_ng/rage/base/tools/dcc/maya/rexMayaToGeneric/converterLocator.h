// 
// rexMayaToGeneric/converterLocator.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterLocator
//			 -- gathers all of entity's locators
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexConverter
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_CONVERTERLOCATOR_H__
#define __REX_MAYAROCKSTAR_CONVERTERLOCATOR_H__

#include "atl/array.h"
#include "rexBase/converter.h"
#include "rexGeneric/objectLocator.h"

class MDagPath;

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterLocator : public rexConverter
{
public:
	rexConverterLocator() : rexConverter() { m_UseChildrenOfLevelInstanceNodes = false; }

	virtual rexResult		Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexConverter*   CreateNew() const  { return new rexConverterLocator; }
	
	virtual rexObjectGenericLocatorGroup* CreateNewLocatorGroup() const { return new rexObjectGenericLocatorGroup; }

	void SetUseChildrenOfLevelInstanceNodes( bool b )						{ m_UseChildrenOfLevelInstanceNodes = b; }
	bool GetUseChildrenOfLevelInstanceNodes() const							{ return m_UseChildrenOfLevelInstanceNodes; }

protected:
    bool m_UseChildrenOfLevelInstanceNodes;
};

/////////////////////////////////////////////////////////////////////////////////////
//Specialized locator type converters

class rexConverterSoundLocator : public rexConverterLocator
{
public:
	virtual rexConverter*   CreateNew() const  { return new rexConverterSoundLocator; }
	virtual rexObjectGenericLocatorGroup* CreateNewLocatorGroup() const { return new rexObjectGenericSoundLocatorGroup; }
};

class rexConverterCharacterInstanceLocator : public rexConverterLocator
{
public:
	virtual rexConverter*	CreateNew() const { return new rexConverterCharacterInstanceLocator; }
	virtual rexObjectGenericLocatorGroup* CreateNewLocatorGroup() const { return new rexObjectGenericCharacterInstanceLocatorGroup; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage


#endif
