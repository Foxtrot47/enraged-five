// 
// rexMayaToGeneric/converterFX.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		AGE 
//			atl, core, data
//		REX
//			Base Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexConverterFX
//			 -- gathers all of entity's fx
//
////////////////////////////////////////////////////////////////////////////////////////
//		
//		INHERITS FROM:
//				rexConverter
//
//		PUBLIC INTERFACE:
//				[Primary Functionality]
//			
/////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_CONVERTERFX_H__
#define __REX_CONVERTERFX_H__

#include "atl/array.h"
#include "rexBase/converter.h"
#include "rexBase/property.h"

#include "rexMaya/mayahelp.h"
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MDagPath.h>
#pragma warning(pop)

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexConverterRmworldFX : public rexConverter
{
public:
	virtual rexResult		Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const;
	virtual rexConverter*   CreateNew() const  { return new rexConverterRmworldFX; }

	void					AddShape( MDagPath& dp )  { m_Shapes.PushAndGrow( dp ); }

protected:
	atArray<MDagPath>		m_Shapes;
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyFXShapeNodes : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
	virtual rexProperty*	CreateNew() const  { return new rexPropertyFXShapeNodes; }
};

/////////////////////////////////////////////////////////////////////////////////////

}	// namespace rage

#endif
