
#include "rexBase/module.h"
#include "rexBase/utility.h"
#include "rexGeneric/objectAnimation.h"
#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"
#include "rexMayaToGeneric/converterAnimation.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MAnimControl.h>
#include <Maya/MFnNumericData.h>
#include <Maya/MEulerRotation.h>
#include <Maya/MFnDagNode.h>
#include <Maya/MGlobal.h>
#include <Maya/MMatrix.h>
#include <Maya/MFileIO.h>
#include <Maya/MFnMesh.h>
#include <Maya/MFnAttribute.h>
#include <Maya/MFnBlendShapeDeformer.h>
#pragma warning(pop)

#include "vector/matrix34.h"

#include <string>
#include <string.h>
#include <list>
#include <algorithm>

namespace rage 
{

	///////////////////////////////////////////////////////////////////////////////////////////

	// Helper functions for converting rotation from Maya to rex.

	bool GetVector3FromAttribute(const MFnDagNode &fn, const char *name, Vector3 &v)
	{
		MStatus status;
		MPlug plug = fn.findPlug(name, status);
		if(status.error())
			return false;

		MObject obj;
		plug.getValue(obj);
		MFnNumericData num(obj);

		double x, y, z;
		num.getData(x, y, z);
		v.x = float(x);
		v.y = float(y);
		v.z = float(z);

		return true;
	}

	bool GetRotationFromAttribute(const MFnDagNode &fn, Quaternion &q)
	{
		MStatus status;
		MPlug plug = fn.findPlug("rotate", status);
		if(status.error())
			return false;

		MObject obj;
		plug.getValue(obj);
		MFnNumericData num(obj);

		double x, y, z;
		num.getData(x, y, z);

		int order = MEulerRotation::kXYZ;
		MPlug plugOrder = fn.findPlug("rotateOrder");
		plugOrder.getValue(order);
		if(order != MEulerRotation::kXYZ)
		{
			MEulerRotation rot(x, y, z, MEulerRotation::RotationOrder(order));
			rot.reorderIt(MEulerRotation::kXYZ);
			x = rot.x; y = rot.y; z = rot.z;
		}

		q.FromEulers(Vector3(float(x), float(y), float(z)));

		return true;
	}

	bool GetValueFromAttribute(const MFnDagNode &fn, const char *name, float &f)
	{
		MStatus status;
		MPlug plug = rexMayaUtility::FindPlug(fn, name, &status);
		if(status.error())
			return false;

		status = plug.getValue(f);

		if(status.error())
			return false;

		return true;
	}

	static bool ExtractVector(const MPlug& plug, Vector3& v)
	{
		MObject obj;
		plug.getValue(obj);
		MFnNumericData num(obj);

		double x, y, z;
		num.getData(x, y, z);
		v.Set( (float)x, (float)y, (float)z );
		return true;
	}

	bool GetStringFromAttribute(const MFnDagNode &fn, const char *name, atString &outString)
	{
		MStatus status;
		MPlug plug = fn.findPlug((const char*)name, status);
		if(status.error())
		{
			return false;
		}

		MString s;
		status = plug.getValue(s);
		if(status.error())
		{
			return false;
		}

		outString = s.asChar();

		return true;
	}

	static const float TIME_TO_FRAME_TOLERANCE = 0.001f;

	rexResult rexConverterAnimation::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& /*defaultName*/ ) const
	{
		rexResult result( rexResult::PERFECT );

		int inputObjectCount = inputObjectArray.GetCount();

		rexObjectGenericAnimation* animation = new rexObjectGenericAnimation;
		animation->m_AuthoredOrientation = GetAuthoredOrientation();

		atArray<MDagPath>								boneDPArray;
		atArray<rexObjectGenericAnimation::ChunkInfo*>	boneArray;

		// Turn on IK, in case it is off.
		MGlobal::executeCommand("ikSystem -e -sol true", false);

		bool rootDone = false;

		for( int a = 0; a < inputObjectCount; a++ )
		{		
			rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( inputObjectArray[ a ] );
			if( dpObj )
			{			
				MFnDagNode fn( dpObj->m_DagPath );
				rexObjectGenericAnimation::ChunkInfo* chunk = NULL;

				if( rootDone )
				{
					int boneCount = boneDPArray.GetCount();
					Assert( boneArray.GetCount() == boneCount );

					int minPositiveDepthDelta = 0x7FFFFFFF;
					int currDepth = rexMayaUtility::GetDPDepth( dpObj->m_DagPath );

					rexObjectGenericAnimation::ChunkInfo* bestParentChunk = NULL;

					for( int b = 0; b < boneCount; b++ )
					{
						if( rexMayaUtility::IsAncestorOf( boneDPArray[b], dpObj->m_DagPath ) )
						{
							int boneDepth = rexMayaUtility::GetDPDepth( boneDPArray[b] );
							int depthDelta = currDepth - boneDepth;

							Assert( depthDelta != 0 );

							if( depthDelta > 0 )
							{
								if( depthDelta < minPositiveDepthDelta )
								{
									minPositiveDepthDelta = depthDelta;
									bestParentChunk = boneArray[ b ];
								}
							}
						}
					}

					if( bestParentChunk )
					{
						Matrix34 thisMatrix, transposed, ancestorMatrix;
						bestParentChunk->m_BoneMatrix.ToMatrix34( ancestorMatrix );
						dpObj->m_BoneMatrix.ToMatrix34( thisMatrix );

						for( rexObjectGenericAnimation::ChunkInfo* currAncestor = bestParentChunk->m_Parent; currAncestor; currAncestor = currAncestor->m_Parent )
						{
							Matrix34 t( ancestorMatrix ), u;
							currAncestor->m_BoneMatrix.ToMatrix34( u );
							ancestorMatrix.Dot( t, u );
						}

						transposed.DotTranspose( thisMatrix, ancestorMatrix );

						chunk = new rexObjectGenericAnimation::ChunkInfo;
						bestParentChunk->m_Children.PushAndGrow( chunk );
						chunk->m_BoneMatrix.FromMatrix34( transposed );
						chunk->m_Parent = bestParentChunk;
					}
					else
					{				
						chunk = new rexObjectGenericAnimation::ChunkInfo;
						animation->m_RootChunks.PushAndGrow( chunk );
						chunk->m_BoneMatrix = dpObj->m_BoneMatrix;
						chunk->m_Parent = NULL;
					}
				}
				else
				{				
					rootDone = true;
					chunk = new rexObjectGenericAnimation::ChunkInfo;
					animation->m_RootChunks.PushAndGrow( chunk );
					chunk->m_BoneMatrix = dpObj->m_BoneMatrix;
					chunk->m_Parent = NULL;
				}

				Assert( chunk );

				chunk->m_Name = m_ChunkNamePrefix;

				if(( dpObj->m_DagPath.apiType() == MFn::kTransform ) || ( dpObj->m_DagPath.apiType() == MFn::kJoint ))
				{
					chunk->m_Name += rexMayaUtility::DagPathToName( dpObj->m_DagPath );
					chunk->m_ShortName += rexMayaUtility::DagPathToNodeName( dpObj->m_DagPath );
				}
				else
				{
					MDagPath parentDP( dpObj->m_DagPath );
					parentDP.pop();

					chunk->m_Name += rexMayaUtility::DagPathToName( parentDP );
					chunk->m_ShortName += rexMayaUtility::DagPathToNodeName( dpObj->m_DagPath );
				}
				chunk->m_BoneIndex = dpObj->m_BoneIndex;

				rexMayaUtility::GetMatrix44FromMayaMatrix( chunk->m_InitialExclusiveMatrix, dpObj->m_DagPath.exclusiveMatrix() );

				MFnDagNode fn2( dpObj->m_DagPath );
				if(!GetStringFromAttribute(fn2, "boneID", chunk->m_BoneID))
				{
					if(GetGenBoneIdFromFullPath() && (chunk->m_Parent != NULL) )
					{
						//There isn't a boneID override attribute, but the property has been set to
						//use the full path name of the node to create the bone ID (but don't include the
						//root node name of the path
						chunk->m_BoneID = rexMayaUtility::FullPathNameToBoneIdString( dpObj->m_DagPath );
					}
				}

				// n8 -- gotta make my own translation matrix, cos maya fucks it up if there's an initial rotation
				//       (it applies the rotation to the initial translate vector, using its pivot point)

				//			rexMayaUtility::GetMatrix44FromMayaMatrix( chunk->m_InitialTransformationMatrix, fn2.transformationMatrix() );chunk->m_InitialTransformationMatrix.ToMatrix34( temp );MPlug tplug = fn2.findPlug("translate");

				Vector3 t;
				GetVector3FromAttribute(fn2, "translate", t);

				Quaternion r;
                GetRotationFromAttribute(fn2, r);

				chunk->m_Scale.Set( 1.0f, 1.0f, 1.0f );
				MDagPath currDP = dpObj->m_DagPath;

				while( currDP.isValid() && ( currDP.fullPathName().length() > 2 ))
				{
					MFnDagNode currFn( currDP );
					Vector3 s;
					if(GetVector3FromAttribute(currFn, "scale", s))
					{
						chunk->m_Scale.Multiply( s );
					}
					currDP.pop();
				}

				Matrix34 temp;
				temp.FromQuaternion(r);
				temp.d.Set(t);
				chunk->m_InitialTransformationMatrix.FromMatrix34(temp);

				chunk->m_IsJoint = ( dpObj->m_DagPath.apiType() == MFn::kJoint ) && ( !chunk->m_Parent || chunk->m_Parent->m_IsJoint );	

				boneDPArray.PushAndGrow( dpObj->m_DagPath );
				boneArray.PushAndGrow( chunk );
			}
		}

		if( m_ProgressBarTextCB && inputObjectCount )
		{
			static char message[1024];
			rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( inputObjectArray[0] );
			sprintf( message, "Converting animation data for entity %s", dpObj->m_DagPath.partialPathName().asChar() );
			(*m_ProgressBarTextCB) ( message );
		}

		// calculate animation length
		int sf = 0;
		int ef = 0;
		if(m_UseTimeLine)
		{
			rexMayaUtility::GetMayaSceneStartAndEndFrames(sf, ef);
		}
		else
		{
			rexMayaUtility::GetAnimationStartAndEndFrames(boneDPArray, sf, ef);
		}

		// use FPS
		animation->m_FramesPerSecond = m_FramesPerSecond;

		// convert via Maya's animation timebase 
		animation->m_StartTime = rexMayaUtility::ConvertTime(float(sf));
		animation->m_EndTime = rexMayaUtility::ConvertTime(float(ef));

		int nFrames = int((animation->m_EndTime - animation->m_StartTime + TIME_TO_FRAME_TOLERANCE) * animation->m_FramesPerSecond) + 1;

		double deltaTime = 1.0f / animation->m_FramesPerSecond;

		//Convert the movers
		int moverCount;

		if(GetAllowMultipleMovers())
		{
			animation->m_MultiMoverMode = true;		
			moverCount = m_MoverNodes.GetCount();
		}
		else
		{
			animation->m_MultiMoverMode = false;
			moverCount = m_MoverNodes.GetCount()?1:0;
		}

		//Check for the case where a mover has been tagged, without tagging a root node.
		if( moverCount && !boneDPArray.GetCount() )
		{
			result.Combine(rexResult::ERRORS);
			result.AddMessage("A Mover node has been tagged for export without tagging a skeleton root.");
			return result;
		}

		for(int moverIdx=0; moverIdx<moverCount; moverIdx++)
		{
			MDagPath moverNode = m_MoverNodes[moverIdx];
			if(moverNode.isValid())
			{
				atArray<atString>	moverTrackNames;
				atArray<int>		moverTrackIDs;

				moverTrackNames.PushAndGrow(atString("translate"));
				moverTrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_TRANSLATE);

				moverTrackNames.PushAndGrow(atString("rotate"));
				moverTrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_ROTATE);

				rexObjectGenericAnimation::MoverChunkInfo *pMoverChunkInfo = new rexObjectGenericAnimation::MoverChunkInfo();

				InitTracksInfo(boneDPArray[0], nFrames, pMoverChunkInfo->m_MoverTracks, moverTrackNames, moverTrackIDs);

				//Get the moverID, and name
				MFnDagNode fndn( moverNode );
				GetStringFromAttribute(fndn, "moverID", pMoverChunkInfo->m_MoverId);
				pMoverChunkInfo->m_MoverName = fndn.partialPathName().asChar();

				// use time-based function
				ConvertFromFrames(moverNode, 
									nFrames,
									animation->m_StartTime, 
									deltaTime, 
									pMoverChunkInfo->m_MoverTracks);

				animation->m_MoverChunks.PushAndGrow(pMoverChunkInfo);
			}
		}

		// Process blend shapes
		int blendShapeMeshCount = m_BlendShapeNodes.GetCount();
		for( int blendShapeIdx = 0; blendShapeIdx < blendShapeMeshCount; blendShapeIdx++ )
		{
			MDagPath meshWithBlendShapes = m_BlendShapeNodes[blendShapeIdx];
			if( meshWithBlendShapes.isValid() )
			{
				MDagPath shapeDp = meshWithBlendShapes;
				if( shapeDp.extendToShape() == MS::kSuccess )
				{
					if(shapeDp.apiType() != MFn::kMesh)
					{
						MString fullPath = shapeDp.fullPathName();
						result.Combine( rexResult::ERRORS );
						result.AddMessage("Node '%s' is tagged to export blendshape animation from, but is not a mesh node", fullPath.asChar());
						return result;
					}
				}
				else
				{
					MString fullPath = meshWithBlendShapes.fullPathName();
					result.Combine( rexResult::ERRORS );
					result.AddMessage("Node '%s' is tagged to export blendshape animation from, but is not a mesh node", fullPath.asChar());
					return result;
				}

				// Visit each blend shape node on this mesh
				atArray<MObject> blendShapeNodes;
				rexMayaUtility::GetAllBlendShapeNodes(meshWithBlendShapes, blendShapeNodes);
				for( int j = 0; j < blendShapeNodes.GetCount(); j++ )
				{
					MObject blendShapeMObj = blendShapeNodes[j];
					MFnBlendShapeDeformer fnBlend(blendShapeMObj);
					
					MPlug weightsPlug = fnBlend.findPlug("weight");

					MIntArray indexArray;
					fnBlend.weightIndexList(indexArray);
					
					// Visit each blend target on this blend shape
					int targetCount = indexArray.length();

					for( int k = 0; k < targetCount; k++ )
					{
						int logicalIdx = indexArray[k];
						MPlug targetWeightPlug = weightsPlug.elementByLogicalIndex( logicalIdx );

						// Get the name of the target
						MString aliasName;
						fnBlend.getPlugsAlias( targetWeightPlug, aliasName);

						atString targetName(aliasName.asChar());
						
						// Allocate an animation track for this blend target
						rexObjectGenericAnimation::TrackInfoFloat* pTrack = new rexObjectGenericAnimation::TrackInfoFloat();
						pTrack->m_TrackID = rexObjectGenericAnimation::TRACK_BLENDSHAPE;
						pTrack->m_Name = targetName;
						pTrack->m_IsLocked = false;
						pTrack->m_StartTime = animation->m_StartTime;
						pTrack->m_EndTime = animation->m_EndTime;
						pTrack->ResetKeys(nFrames);

						// Grab all the key frames for this target
						double start = (double)animation->m_StartTime;
						
						// Force animation data dirty.
						rexMayaUtility::SetFrame(-1);

						for(int i = 0; i < nFrames; i++)
						{
							double t = start + (deltaTime * (double)i);
							MAnimControl::setCurrentTime(MTime(t, MTime::kSeconds));
							
							float weight;
							targetWeightPlug.getValue(weight);
							pTrack->AppendFloatKey(weight);
						}

						MAnimControl::setCurrentTime(MTime(double(start), MTime::kSeconds));
						
						// Allocate the blend chunk if it hasn't been created yet, no sense in creating it until it
						// will contain data
						if( !animation->m_GenericChunk )
							animation->m_GenericChunk = new rexObjectGenericAnimation::ChunkInfo();

						// Add the track for this blend target to the blend chunk
						animation->m_GenericChunk->m_Tracks.PushAndGrow(pTrack);							
					}
				}
			}
		}

		//Convert any animated normal map data
		rexResult animNrmConvRes = ConvertAnimatedNormalMaps(animation);
		if(animNrmConvRes.Errors())
		{
			result.Combine(animNrmConvRes);
			result.AddMessage("An error has occured while exporting animated normal maps");
			return result;
		}

		//Convert any expression control data
		rexResult animExprCtrlRes = ConvertExpressionControlData(animation);
		if(animExprCtrlRes.Errors())
		{
			result.Combine(animExprCtrlRes);
			result.AddMessage("An error has occured while exporting the expression control data");
			return result;
		}

		// Convert for parent matrix.
		if( m_ParentNode.isValid() ) 
		{
			rexMayaUtility::GetMatrix34FromMayaMatrix( animation->m_ParentMatrix, m_ParentNode.inclusiveMatrix() );
		}

		// Convert all the bones
		ConvertFromAllFrames(boneDPArray, 
			nFrames, 
			animation->m_StartTime, 
			deltaTime, 
			boneArray);

		//Apply the translate scale
		if(GetTranslateScale() != 1.f)
		{
			for(int i=0; i<boneArray.GetCount(); i++)
			{
				ApplyTranslateScale(boneArray[i]->m_Tracks);
			}

			for(int i=0; i<animation->m_MoverChunks.GetCount(); i++)
			{
				ApplyTranslateScale(animation->m_MoverChunks[i]->m_MoverTracks);
			}
		}

		if(GetApplyMatrixToAnimation())
		{
			// Get the matrix to apply by getting the uninclusive matrix of the root node
			MMatrix obMatrixToApply = boneDPArray[0].exclusiveMatrix();
			MTransformationMatrix obTransformationMatrixToApply(obMatrixToApply);
			double adScale[3];
			obTransformationMatrixToApply.getScale(adScale, MSpace::kTransform);

			// Apply the matrix to the root....
			if(boneArray.GetCount() > 0)
			{
				ApplyMatrix(obMatrixToApply, boneArray[0]->m_Tracks);
			}

			if(animation->m_MoverChunks.GetCount() > 0)
			{
				ApplyMatrix(obMatrixToApply, animation->m_MoverChunks[0]->m_MoverTracks);
			}
		
			// And the scale from the matrix to everything else
			for(int i=1; i<boneArray.GetCount(); i++)
			{
				ApplyTranslateScale((float)adScale[0], boneArray[i]->m_Tracks);
			}

			for(int i=1; i<animation->m_MoverChunks.GetCount(); i++)
			{
				ApplyTranslateScale((float)adScale[0], animation->m_MoverChunks[i]->m_MoverTracks);
			}
		}
		

		// Joint orientation:
		for (int i=0;i<boneArray.GetCount();i++)
		{
			// undo joint orientations for bones:
			if (boneArray[i]->m_IsJoint)
			{
				// find tracks:
				atArray<rexObjectGenericAnimation::TrackInfo*>& trackInfo=boneArray[i]->m_Tracks;
				rexObjectGenericAnimation::TrackInfo *rotate=NULL;
				rexObjectGenericAnimation::TrackInfo *scale=NULL;
				rexObjectGenericAnimation::TrackInfo *translate=NULL;
				for (int t=0;t<trackInfo.GetCount();t++)
				{
					switch(trackInfo[t]->m_TrackID)
					{

					case rexObjectGenericAnimation::TRACK_TRANSLATE:
						translate=trackInfo[t];
						break;
					case rexObjectGenericAnimation::TRACK_ROTATE:
						rotate=trackInfo[t];
						break;
					case rexObjectGenericAnimation::TRACK_SCALE:
						scale=trackInfo[t];
						break;
					}
				}

				if(GetAuthoredOrientation())
				{
					// authored path to joint/scale orientation:

					Matrix34 jointMatrix,scaleOrientMatrix;
					rexMayaUtility::GetJointOrientMatrices(boneDPArray[i],jointMatrix,scaleOrientMatrix);					

					if(rotate)
					{
						int maxFrames=rotate->GetNumKeys();

						// loop through all frames, applying joint and scale orientations
						for (int f=0;f<maxFrames;f++)
						{
							// get current rotation matrix:
							Quaternion currentQuat;
							rotate->GetQuaternionKey(f, currentQuat);

							Matrix34 currentMatrix;
							currentMatrix.FromQuaternion(currentQuat);

							currentMatrix.Dot3x3(jointMatrix);
							currentMatrix.Dot3x3FromLeft(scaleOrientMatrix);

							// set our new quaternion:
							Quaternion newQuat;
							currentMatrix.ToQuaternion(newQuat);

							rotate->SetQuaternionKey(f, newQuat);
						}
					}
				}
				else
				{
					// non authored path to joint/scale orientation:

					// get orientations from previous bones in our hierarchy:
					Matrix34 parentMatrix;
					parentMatrix.Identity();

					MDagPath dpSub(boneDPArray[i]);
					dpSub.pop(); // don't include the current joint!

					// collect rotation matrix:
					while (dpSub.length())
					{
						if( dpSub.apiType() == MFn::kJoint )
						{
							Matrix34 jointMatrix,scaleOrientMatrix;
							rexMayaUtility::GetJointOrientMatrices(dpSub,jointMatrix,scaleOrientMatrix);					
							parentMatrix.Dot3x3(scaleOrientMatrix);
							parentMatrix.Dot3x3(jointMatrix);
						}
						dpSub.pop();
					}

					// get the current joint's matrix:
					Matrix34 jointMatrix,scaleOrientMatrix;;
					rexMayaUtility::GetJointOrientMatrices(boneDPArray[i],jointMatrix,scaleOrientMatrix);					

					// matrix inverses we'll need for the following loop:
					Matrix34 parentInverse(parentMatrix);
					parentInverse.Inverse();
					Matrix34 jointInverse(jointMatrix);
					jointInverse.Inverse();

					int maxFrames=rotate->GetNumKeys();

					// do rotate channels:
					if(rotate)
					{
						// loop through all frames, applying the joint orientations to the euler channels:
						Matrix34 preDot(parentInverse);
						preDot.d.Zero();
						preDot.Dot(jointInverse);

						Matrix34 postDot(jointMatrix);
						postDot.d.Zero();
						postDot.Dot(parentMatrix);

						for (int f=0; f<maxFrames; ++f)
						{
							// get current rotation matrix:
							Quaternion currentQuat;
							rotate->GetQuaternionKey(f, currentQuat);

							Matrix34 currentMatrix;
							currentMatrix.FromQuaternion(currentQuat);

							// untransform all of the joint orientations:
							Matrix34 finalMatrix;
							finalMatrix.Dot(preDot, currentMatrix);
							finalMatrix.Dot(postDot);

							// get and set our new quaternion:
							Quaternion newQuat;
							finalMatrix.ToQuaternion(newQuat);

							rotate->SetQuaternionKey(f, newQuat);
						}
					}


					if(translate)
					{
						for (int f=0; f<maxFrames; ++f)
						{
							// get current translation:
							Vector3 currentTrans, newTrans;
							translate->GetVector3Key(f, currentTrans);

							// apply the joint / scale orientations to the translation:
							parentMatrix.Transform3x3(currentTrans, newTrans);

							// set our new translation:
							translate->SetVector3Key(f, newTrans);
						}
					}

					// what follows is code for adjusting scaling based on the joint orientations.
					// scaling hasn't really been implemented in the real-time code, so this 
					// code is off and not thoroughly tested.
	/*					if (0)
					{
						// do scale channels:
						numFramesX=scaleX?scaleX->m_Keyframes.GetCount():0;
						numFramesY=scaleY?scaleY->m_Keyframes.GetCount():0;
						numFramesZ=scaleZ?scaleZ->m_Keyframes.GetCount():0;
						maxFrames=Max(numFramesX,numFramesY,numFramesZ);


						// loop through all frames, applying the joint orientations to the scale channels:
						for (int f=0;f<maxFrames;f++)
						{
							// get current scale matrix:
							Vector3 currentScale;
							currentScale.x=GetKeyframeValue(scaleX,f);
							currentScale.y=GetKeyframeValue(scaleY,f);
							currentScale.z=GetKeyframeValue(scaleZ,f);
							Matrix34 scaleMatrix;
							scaleMatrix.MakeScale(currentScale);
							scaleMatrix.d.Zero();

							// untransform scale:
							Matrix34 finalMatrix(parentInverse);
							finalMatrix.Dot(jointInverse);
							finalMatrix.Dot(scaleMatrix);
							finalMatrix.Dot(jointMatrix);
							finalMatrix.Dot(parentMatrix);

							// get and set our new scale values:
							SetKeyframeValue(scaleX,f,finalMatrix.a.Mag());
							SetKeyframeValue(scaleY,f,finalMatrix.b.Mag());
							SetKeyframeValue(scaleZ,f,finalMatrix.c.Mag());
						}

						// unlock all scale channels since we may be actually using channels that we weren't using before due to transformations:
						if (scaleX)
							scaleX->m_IsLocked=false;
						if (scaleY)
							scaleY->m_IsLocked=false;
						if (scaleZ)
							scaleZ->m_IsLocked=false;
					}*/
				}
			}
		}

		outputObject = animation;
		return result;
	}

	///////////////////////////////////////////////////////////////////////////////////////////

	struct ExpressionCtrlTrackData
	{
		MDagPath	m_dp;
		atArray< atString > m_attrNames;
		atArray< rexObjectGenericAnimation::TrackInfo* > m_tracks;
	};

	rexResult rexConverterAnimation::ConvertExpressionControlData(rexObjectGenericAnimation* pAnimation) const
	{
		MStatus status;
		rexResult result(rexResult::PERFECT);

		int exprCtrlNodeCount = m_AnimExprCtrlNodes.GetCount();
		if(!exprCtrlNodeCount)
			return rexResult::PERFECT;

		int nFrames = int((pAnimation->m_EndTime - pAnimation->m_StartTime + TIME_TO_FRAME_TOLERANCE) * pAnimation->m_FramesPerSecond) + 1;
		float deltaTime = 1.0f / pAnimation->m_FramesPerSecond;

		float st, et;

		if(m_UseTimeLine)
		{
			float ssf, sef;
			rexMayaUtility::GetMayaSceneStartFrame(ssf);
			st = rexMayaUtility::ConvertTime(ssf);
			rexMayaUtility::GetMayaSceneEndFrame(sef);
			et = rexMayaUtility::ConvertTime(sef);
		}
		else
		{
			result = rexResult::ERRORS;
			result.AddMessage("'UseTimeLine' property must be enabled when exporting expression control data.");
			return result;
		}
		
		//Create the generic chunk (if it doesn't already exist)
		if(!pAnimation->m_GenericChunk )
			pAnimation->m_GenericChunk = new rexObjectGenericAnimation::ChunkInfo();

		atArray<ExpressionCtrlTrackData*> nodeTrackData;

		for(int nodeIdx = 0; nodeIdx < exprCtrlNodeCount; nodeIdx++)
		{
			MDagPath nodeDp = m_AnimExprCtrlNodes[nodeIdx];
			MFnDependencyNode nodeFnDep(nodeDp.node());

			ExpressionCtrlTrackData* pData = new ExpressionCtrlTrackData();
			pData->m_dp = nodeDp;

			//Add the translation track
			rexObjectGenericAnimation::TrackInfo* pTransTrackInfo = new rexObjectGenericAnimation::TrackInfoVector3();
			pTransTrackInfo->m_TrackID = rexObjectGenericAnimation::TRACK_FACIAL_TRANSLATION;
			pTransTrackInfo->m_Name = rexMayaUtility::DagPathToName(nodeDp);
			pTransTrackInfo->m_StartTime = st;
			pTransTrackInfo->m_EndTime = et;
		
			pTransTrackInfo->ResetKeys(nFrames);
		
			pData->m_tracks.PushAndGrow( pTransTrackInfo );
			pData->m_attrNames.PushAndGrow(atString("translate"));

			pAnimation->m_GenericChunk->m_Tracks.PushAndGrow(pTransTrackInfo);		

			//Add the rotation track
			rexObjectGenericAnimation::TrackInfo* pRotTrackInfo = new rexObjectGenericAnimation::TrackInfoQuaternion();
			pRotTrackInfo->m_TrackID = rexObjectGenericAnimation::TRACK_FACIAL_ROTATION;
			pRotTrackInfo->m_Name = rexMayaUtility::DagPathToName(nodeDp);
			pRotTrackInfo->ResetKeys(nFrames);
			pRotTrackInfo->m_StartTime = st;
			pRotTrackInfo->m_EndTime = et;

			InitTrackInfo(nodeDp, nFrames, *pRotTrackInfo, "rotate");
			
			pData->m_tracks.PushAndGrow( pRotTrackInfo );
			pData->m_attrNames.PushAndGrow(atString("rotate"));
			
			pAnimation->m_GenericChunk->m_Tracks.PushAndGrow(pRotTrackInfo);		

			//Add the tracks for any of the dynamic attributes
			unsigned int nAttrs = nodeFnDep.attributeCount();
			for(unsigned int attrIdx = 0; attrIdx < nAttrs; attrIdx++)
			{
				MObject attrMObj = nodeFnDep.attribute( attrIdx, &status );
				if( !status.error() && (nodeFnDep.isNewAttribute(attrMObj, &status)) )
				{
					MFnAttribute fnAttr(attrMObj, &status);
					MFn::Type attrType = fnAttr.object().apiType();

					if(!status.error() && (attrType == MFn::kNumericAttribute) )
					{
						//Add a track for the attribute
						MString attrName = fnAttr.name();
						const char* szAttrName = attrName.asChar();

						atString nodeName = rexMayaUtility::DagPathToName(nodeDp);

						char exprCtrlTrackName[128];
						exprCtrlTrackName[0] = 0;

						sprintf(exprCtrlTrackName, "%s_%s", (const char*)nodeName, szAttrName);
						
						rexObjectGenericAnimation::TrackInfo* pAttrTrackInfo = new rexObjectGenericAnimation::TrackInfoFloat();
						pAttrTrackInfo->m_TrackID = rexObjectGenericAnimation::TRACK_FACIAL_CONTROL;
						pAttrTrackInfo->m_Name = exprCtrlTrackName;
						pAttrTrackInfo->m_StartTime = st;
						pAttrTrackInfo->m_EndTime = et;
						pAttrTrackInfo->ResetKeys(nFrames);

						InitTrackInfo(nodeDp, nFrames, *pAttrTrackInfo, szAttrName);

						pData->m_tracks.PushAndGrow(pAttrTrackInfo);
						pData->m_attrNames.PushAndGrow(atString(attrName.asChar()));
						
						pAnimation->m_GenericChunk->m_Tracks.PushAndGrow(pAttrTrackInfo);
					}
				}
			}

			Assert(pData->m_attrNames.GetCount() == pData->m_tracks.GetCount());
			nodeTrackData.PushAndGrow(pData);			
		}

		//Sample the control data..
		double start = pAnimation->m_StartTime;
		int nNodes = nodeTrackData.GetCount();

		// Force animation data dirty.
		rexMayaUtility::SetFrame(-1);

		for(int i = 0; i < nFrames; i++)
		{
			double t = start + (deltaTime * (double)i);
			MAnimControl::setCurrentTime(MTime(t, MTime::kSeconds));
			
			for(int nodeIdx = 0; nodeIdx < nNodes; nodeIdx++)
			{
				ExpressionCtrlTrackData* pData = nodeTrackData[nodeIdx];

				int nTracks = pData->m_tracks.GetCount();
				for( int trackIdx = 0; trackIdx < nTracks; trackIdx++ )
				{
					GetTrackFrameData(pData->m_dp, *pData->m_tracks[trackIdx], pData->m_attrNames[trackIdx]);
				}
			}
		}

		MAnimControl::setCurrentTime(MTime(start, MTime::kSeconds));
		
		return result;
	}

	///////////////////////////////////////////////////////////////////////////////////////////

	rexResult rexConverterAnimation::ConvertAnimatedNormalMaps(rexObjectGenericAnimation* pAnimation) const
	{
		rexResult result(rexResult::PERFECT);

		int nFrames = int((pAnimation->m_EndTime - pAnimation->m_StartTime + TIME_TO_FRAME_TOLERANCE) * pAnimation->m_FramesPerSecond) + 1;
		double deltaTime = 1.0f / pAnimation->m_FramesPerSecond;

		//Process Animated Normal Maps
		int animNormalMapNodeCount = m_AnimNormalMapNodes.GetCount();
		for( int animNormalMapNodeIdx = 0; animNormalMapNodeIdx < animNormalMapNodeCount; animNormalMapNodeIdx++ )
		{	
			//Make sure we have a valid node to work with
			MDagPath animNrmMapDp = m_AnimNormalMapNodes[animNormalMapNodeIdx];
			if(!animNrmMapDp.isValid())
			{
				continue;
			}

			MString animNrmMapNodeName = animNrmMapDp.partialPathName();

			//Make sure a mesh was tagged
			MDagPath shapeDp = animNrmMapDp;
			if( shapeDp.extendToShape() == MS::kSuccess )
			{
				if(shapeDp.apiType() != MFn::kMesh)
				{
					MString fullPath = shapeDp.fullPathName();
					result.Combine( rexResult::ERRORS );
					result.AddMessage("Node '%s' is tagged to export animated normal maps from, but is not a mesh node", fullPath.asChar());
					return result;
				}
			}
			else
			{
				MString fullPath = animNrmMapDp.fullPathName();
				result.Combine( rexResult::ERRORS );
				result.AddMessage("Node '%s' is tagged to export animated normal maps from, but is not a mesh node", fullPath.asChar());
				return result;
			}

			MStatus status;

			//Find the materials associated with the mesh
			MFnMesh fnMesh(shapeDp);
			MString fnMeshName = fnMesh.partialPathName();

			MObjectArray shaderObjArr;
			MIntArray	 faceIndexArr;

			fnMesh.getConnectedShaders(0, shaderObjArr, faceIndexArr);
			int shaderCount = shaderObjArr.length();
			for( int shaderIdx = 0; shaderIdx < shaderCount; shaderIdx++ )
			{
				MFnSet shaderSet(shaderObjArr[shaderIdx], &status);
				MPlug surfaceShader = shaderSet.findPlug("surfaceShader",&status);
				if(status.error())
				{
					result.Combine( rexResult::ERRORS );
					result.AddMessage( "Failed to find the 'surfaceShader' plug for the shader set applied to '%s'", fnMeshName.asChar());
					return result;
				}

				MPlugArray plugs;
				surfaceShader.connectedTo(plugs, true, false, &status);
				if(status.error()) 
				{
					result.Combine( rexResult::ERRORS );
					result.AddMessage( "Failed to find any shaders connected to the 'surfaceShader' plug of the shader assigned to '%s'", fnMeshName.asChar());
					return result;
				}
				if(plugs.length() != 1) 
				{
					if( plugs.length() > 0 )
					{
						result.Combine( rexResult::ERRORS );
						result.AddMessage( "Cannot process shader node '%s', multiple shaders are assigned", surfaceShader.name().asChar() );
					}
					return result;
				}

				//Get the shader object (should be a lambert or a blinn...)
				MObject shaderMObj = plugs[0].node();
				MFnDependencyNode shaderDn(shaderMObj, &status);
				
				if( (shaderDn.object().apiType() != MFn::kLambert ) &&
					(shaderDn.object().apiType() != MFn::kBlinn) )
				{
					result.Combine( rexResult::ERRORS );
					result.AddMessage( "Cannot export animated normal maps from shader '%s', shader isn't of type lambert or blinn", shaderDn.name().asChar() );
					return result;
				}

				//Look for the node connected to the normalCamera (bump map) plug
				MPlug normalCameraPlug = shaderDn.findPlug("normalCamera", &status);
				if(status.error())
				{
					result.Combine( rexResult::ERRORS );
					result.AddMessage( "Cannot access attribute 'normalCamera' of shader '%s'", shaderDn.name().asChar() );
					return result;
				}

				plugs.clear();
				normalCameraPlug.connectedTo(plugs, true, false, &status);
				if( status.error() || !plugs.length() )
				{
					result.Combine( rexResult::ERRORS );
					result.AddMessage( "Nothing is connected to the 'normalCamera' attribute of shader '%s', but model '%s' was tagged to export animated normal maps from.", 
						shaderDn.name().asChar(), animNrmMapNodeName.asChar() );
					continue;
				}

				MFnDependencyNode animNrmMapDn;
				
				//There are two ways this can be setup, one has a checker node connected to the normalCamera plug, and then to the
				//animated normal map node (legacy style), and the other has the animated normal map node connected directly to
				//the normalCamera plug (new style)... but check for both

				MObject normalCameraDstPlugMObj = plugs[0].node();
				MFnDependencyNode normalCameraDstFnDep( normalCameraDstPlugMObj, &status);

				if( normalCameraDstFnDep.object().apiType() == MFn::kChecker)
				{
					//The older style of using a checker node is in use...

					//Now the animated normal map node should be connected to both the color1 and color2 inputs 
					//of the checker object
					MPlug colorOnePlug = normalCameraDstFnDep.findPlug("color1", &status);
				
					plugs.clear();
					colorOnePlug.connectedTo(plugs, true, false, &status);
					if( status.error() || !plugs.length() )
					{
						result.Combine( rexResult::WARNINGS );
						result.AddMessage( "Nothing connected to 'color1' attribute of node '%s', no animated normal maps found for export, skipping.",
									normalCameraDstFnDep.name().asChar() );
						continue;
					}

					MObject animNrmMapMObj = plugs[0].node();

					status = animNrmMapDn.setObject(animNrmMapMObj);
	
					MString nodeTypeName = animNrmMapDn.typeName();
					if( (nodeTypeName != "AnimNormalMap") && (nodeTypeName != "AnimatedNormalMaps"))
					{
						result.Combine( rexResult::WARNINGS );
						result.AddMessage( "Node connected to 'color1' attribute of node '%s' is not of type 'AnimatedNormalMaps', no animated normal maps found for export, skipping.",
								normalCameraDstFnDep.name().asChar() );
						continue;
					}
				}
				else
				{
					//The new style of a direction connection is in use...
					status = animNrmMapDn.setObject(normalCameraDstPlugMObj);

					MString nodeTypeName = animNrmMapDn.typeName();
					const char* szDbgNodeTypeName = nodeTypeName.asChar();
					szDbgNodeTypeName;
					if(nodeTypeName != "AnimNormalMap")
					{
						result.Combine( rexResult::WARNINGS );
						result.AddMessage( "Node connected to 'normalCamera' attribute of node '%s' us not of type 'AnimatedNormalMaps', no animated normal maps found for export, skipping.",
							shaderDn.name().asChar());
						continue;
					}
				}
			
				//Get a list of all the multipliers that have been enabled on the animated normal map node,
				//and their associated expression names
				MPlugArray multiplierPlugArr;
				MStringArray multiplierExprNameArr;
			
				int unknownIdCounter = 1;
				char unknownIdBuf[32];
				char plugBuf[32];
			
				plugBuf[0] = 0;
				for(int i=1; i<=60; i++)
				{
					sprintf(plugBuf, "Enable%d", i);

					MPlug enablePlug = animNrmMapDn.findPlug(plugBuf, &status);
					if( !enablePlug.isNull() )
					{
						bool bEnable = false;
						status = enablePlug.getValue( bEnable );
						if( !status.error() && bEnable )
						{
							//Add the plug to the enabled multiplier plug array
							sprintf(plugBuf, "mu%d", i);
							MPlug multPlug = animNrmMapDn.findPlug(plugBuf, &status);
							if( !multPlug.isNull() )
							{
								multiplierPlugArr.append(multPlug);
							}

							//Grab the expression name for the multiplier, which will be hased to generate the track's bone id
							sprintf(plugBuf, "ExprName%d", i);
							MPlug exprNamePlug = animNrmMapDn.findPlug(plugBuf, &status);
							if( !exprNamePlug.isNull() )
							{
								MString exprName;
								status = exprNamePlug.getValue( exprName );

								if(exprName.length())
								{
									//There is a valid name set, so store this
									const char* szDbgExprName = exprName.asChar();
									szDbgExprName;
									multiplierExprNameArr.append(exprName);
								}
								else
								{
									//There isn't a valid name set, so try to put something sensible in for the track id
									sprintf(unknownIdBuf, "#%d", unknownIdCounter++);
									multiplierExprNameArr.append(MString(unknownIdBuf));

									result.Combine( rexResult::WARNINGS );
									result.AddMessage("While exporting animated normal map animation, found an enabled multiplier with no expression name set, resulting animation will not play back correctly");
								}
							}
							else
							{
								sprintf(unknownIdBuf, "#%d", unknownIdCounter++);
								multiplierExprNameArr.append(MString(unknownIdBuf));
							}
						}
					}
				}

				//Create generic animation tracks for all the enabled multiplers
				int enabledMultCount = multiplierPlugArr.length();
				vector< rexObjectGenericAnimation::TrackInfoFloat* > multiplierTracks;
				multiplierTracks.reserve(enabledMultCount);
				
				for( int trackIdx=0; trackIdx < enabledMultCount; trackIdx++)
				{
					//Create the generic track
					rexObjectGenericAnimation::TrackInfoFloat* pTrack = new rexObjectGenericAnimation::TrackInfoFloat();
					pTrack->m_TrackID = rexObjectGenericAnimation::TRACK_ANIMATED_NORMAL_MAPS;
					pTrack->m_Name = multiplierExprNameArr[trackIdx].asChar();
					pTrack->m_IsLocked = false;
					pTrack->m_StartTime = pAnimation->m_StartTime;
					pTrack->m_EndTime = pAnimation->m_EndTime;
					pTrack->ResetKeys(nFrames);

					multiplierTracks.push_back(pTrack);

					// Allocate the generic chunk if it hasn't been created yet, no sense in creating it until it
					// will contain data
					if( !pAnimation->m_GenericChunk )
						pAnimation->m_GenericChunk = new rexObjectGenericAnimation::ChunkInfo();

					// Add the track for this animated normal map to the generic chunk
					pAnimation->m_GenericChunk->m_Tracks.PushAndGrow(pTrack);			
				}

				//Sample the multipler values across the animation
				double start = (double)pAnimation->m_StartTime;

				rexMayaUtility::SetFrame(-1);

				for(int i = 0; i < nFrames; i++)
				{
					double t = start + (deltaTime * (double)i);
					MAnimControl::setCurrentTime(MTime(t, MTime::kSeconds));

					for( int trackIdx=0; trackIdx < enabledMultCount; trackIdx++)
					{
						rexObjectGenericAnimation::TrackInfoFloat* pTrack = multiplierTracks[trackIdx];
						float multValue;
						multiplierPlugArr[trackIdx].getValue(multValue);
						pTrack->AppendFloatKey(multValue);
					}
				}

				MAnimControl::setCurrentTime(MTime(start, MTime::kSeconds));

			}//End for( int shaderIdx = 0...

		}//End for( int animNormalMapNodeIdx = 0...

		return result;
	}

	///////////////////////////////////////////////////////////////////////////////////////////

	void rexConverterAnimation::ApplyTranslateScale(atArray<rexObjectGenericAnimation::TrackInfo*>& tracks) const
	{
		float translateScale = GetTranslateScale();
		ApplyTranslateScale(translateScale, tracks);
	}

	void rexConverterAnimation::ApplyTranslateScale(float translateScale, atArray<rexObjectGenericAnimation::TrackInfo*>& tracks) const
	{
		for (int t=0; t<tracks.GetCount(); t++)
		{
			if(tracks[t]->m_TrackID == rexObjectGenericAnimation::TRACK_TRANSLATE)
			{
				rexObjectGenericAnimation::TrackInfo *translate = tracks[t];
				int maxFrames=translate->GetNumKeys();

				for(int f=0; f<maxFrames; f++)
				{
					Vector3 v;
					translate->GetVector3Key(f, v);
					v *= translateScale;
					translate->SetVector3Key(f, v);
				}
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////

	void rexConverterAnimation::ApplyMatrix(const MMatrix& obMatrixToApply, atArray<rexObjectGenericAnimation::TrackInfo*>& tracks) const
	{
		Matrix44 obMatrix44ToApply;
		rexMayaUtility::GetMatrix44FromMayaMatrix( obMatrix44ToApply, obMatrixToApply );

		for (int t=0; t<tracks.GetCount(); t++)
		{
			if(tracks[t]->m_TrackID == rexObjectGenericAnimation::TRACK_TRANSLATE)
			{
				rexObjectGenericAnimation::TrackInfo *translate = tracks[t];
				int maxFrames=translate->GetNumKeys();

				for(int f=0; f<maxFrames; f++)
				{
					Vector3 v;
					translate->GetVector3Key(f, v);
					v = obMatrix44ToApply.FullTransform(v);
					translate->SetVector3Key(f, v);
				}
				return;
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////

	rexResult rexConverterAnimationSegment::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
	{
		rexResult result( rexResult::PERFECT );

		rexObjectGenericAnimationSegment* segment = new rexObjectGenericAnimationSegment;

		if(m_PrefixSegNameWithFilename)
		{
			std::string curFile = MFileIO::currentFile().asChar();
			
			//Strip off the path portion of the filename
			size_t idx = curFile.find_last_of('/');
			if(idx == std::string::npos)
				idx = curFile.find_last_of('\\');
			curFile = curFile.substr(idx+1);

			//Strip off the extension
			idx = curFile.find_last_of('.');
			if(idx != std::string::npos)
				curFile = curFile.substr(0,idx);

			//Look for "_SEQ" in the file name, and strip it off
			idx = curFile.find("_SEQ");
			if(idx != std::string::npos)
				curFile = curFile.substr(0, idx);

			std::string segmentName = curFile;
			segmentName += (const char*)defaultName;
			segment->SetName(segmentName.c_str());
		}
		else
			segment->SetName( defaultName );

		int inputObjectCount = inputObjectArray.GetCount();

		for( int a = 0; a < inputObjectCount; a++ )
		{		
			rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( inputObjectArray[ a ] );
			if( dpObj )
			{			
				atString s( m_ChunkNamePrefix );
				s += rexMayaUtility::DagPathToName( dpObj->m_DagPath );
				segment->m_ChunkNames.PushAndGrow( s, (u16) inputObjectCount );
			}
		}

		outputObject = segment;
		return result;
	}

	/*
	PURPOSE
	Get all channel data for the given node at the given frame.
	PARAMS
	f - the current frame index.
	dp - dagpath node from which the data comes.
	channelArray - the channel list.
	RETURN
	none.
	NOTES
	*/
	void rexConverterAnimation::GetTrackFrameData(const MDagPath& dp, atArray<rexObjectGenericAnimation::TrackInfo*>& trackArray ) const
	{
		int trackCount = trackArray.GetCount();
		for( int t = 0; t < trackCount; t++ )
			GetTrackFrameData(dp, *(trackArray[t]));

	}

	bool rexConverterAnimation::GetTrackFrameData(const MDagPath& dp, rexObjectGenericAnimation::TrackInfo& track ) const
	{
		return GetTrackFrameData(dp, track, track.m_Name);
	}

	bool rexConverterAnimation::GetTrackFrameData(const MDagPath& dp, rexObjectGenericAnimation::TrackInfo& track, const char *attrName) const
	{
		MFnDagNode fn(dp);

		switch(track.m_TrackID)
		{
		case rexObjectGenericAnimation::TRACK_TRANSLATE:
		case rexObjectGenericAnimation::TRACK_FACIAL_TRANSLATION:
		case rexObjectGenericAnimation::TRACK_SCALE:
			{
				Vector3 v;
				if(!GetVector3FromAttribute(fn, attrName, v))
				{
					return false;
				}
				track.AppendVector3Key(v);
			}
			break;

		case rexObjectGenericAnimation::TRACK_ROTATE:
		case rexObjectGenericAnimation::TRACK_FACIAL_ROTATION:
			{
				Quaternion q;
				if(!GetRotationFromAttribute(fn, q))
				{
					return false;
				}
				track.AppendQuaternionKey(q);
			}
			break;

		default:
			{
				float f;
				if(!GetValueFromAttribute(fn, attrName, f))
				{
					return false;
				}
				track.AppendFloatKey(f);
			}
		}
		return true;
	}

	/*
	PURPOSE
	Initialize ChannelInfo list for the given node.
	PARAMS
	dp - the node for which channel is to be initialized.
	frameCount - the number of frames.
	isCurve - the channel is using curve.
	channels - the channel list to be initialized.
	RETURN
	none.
	NOTES
	*/
	void rexConverterAnimation::InitTracksInfo(const MDagPath& dp, int nFrames, atArray<rexObjectGenericAnimation::TrackInfo*>& tracks, const atArray<atString>& trackNames, const atArray<int>& trackIDs) const
	{
		int trackCount = trackNames.GetCount();
		tracks.Reset();
		tracks.Reserve(trackCount);
		for( int c = 0; c < trackCount; c++ )
		{
			rexObjectGenericAnimation::TrackInfo* track = 0;
			switch(trackIDs[c])
			{
			case rexObjectGenericAnimation::TRACK_TRANSLATE:
			case rexObjectGenericAnimation::TRACK_SCALE:
				track = new rexObjectGenericAnimation::TrackInfoVector3();
				break;

			case rexObjectGenericAnimation::TRACK_ROTATE:
				track = new rexObjectGenericAnimation::TrackInfoQuaternion();
				break;

			default:
                track = new rexObjectGenericAnimation::TrackInfoFloat();
				break;
			}
			if(!track)
				continue;
			
			track->m_Name = trackNames[c];
			track->m_TrackID = trackIDs[c];

			if(InitTrackInfo(dp, nFrames, *track))
			{
				tracks.Append() = track;
			}
			else
			{
				//We weren't able to locate the attribute to export data from for the tack on the node
				//so don't add the track.
				delete track;
			}
		}
	}


	bool rexConverterAnimation::InitTrackInfo(const MDagPath& dp, int nFrames, rexObjectGenericAnimation::TrackInfo& track, const char *attrName) const
	{
		MStatus status;
		MPlug plug = rexMayaUtility::FindPlug(dp, attrName, &status);
		if(status.error())
			return false;

		if(m_UseTimeLine)
		{
			float sf, ef;
			rexMayaUtility::GetMayaSceneStartFrame(sf);
			track.m_StartTime = rexMayaUtility::ConvertTime(sf);
			rexMayaUtility::GetMayaSceneEndFrame(ef);
			track.m_EndTime = rexMayaUtility::ConvertTime(ef);
		}
		else
		{
			int sf, ef;
			rexMayaUtility::GetAnimationStartAndEndFrames(dp, sf, ef, false, false);
			track.m_StartTime = rexMayaUtility::ConvertTime(float(sf));
			track.m_EndTime = rexMayaUtility::ConvertTime(float(ef));
		}

		track.m_IsLocked = (plug.isLocked() || !plug.isKeyable() || (track.m_EndTime <= track.m_StartTime));
	
		track.ResetKeys(nFrames);

		return true;
	}

	bool rexConverterAnimation::InitTrackInfo(const MDagPath& dp, int nFrames, rexObjectGenericAnimation::TrackInfo& track) const
	{
		return InitTrackInfo(dp, nFrames, track, track.m_Name);
	}


	/*
	PURPOSE
		Get animation data from all frames of maya nodes and save it into
		generic nodes.
	PARAMS
		dpList - maya node list.
		boneList - generic node list.
	RETURN
		none.
	NOTES
	*/
	void rexConverterAnimation::ConvertFromAllFrames(const atArray<MDagPath> &dpList, int sf, int ef, atArray<rexObjectGenericAnimation::ChunkInfo*>& boneList) const
	{
		int dpCount = dpList.GetCount();
		int boneCount = boneList.GetCount();
		if(dpCount == 0 || boneCount != dpCount)
			return;

		atArray<atString>	TEMPTrackNames;
		atArray<int>		TEMPTrackIDs;

		TEMPTrackNames.PushAndGrow(atString("translate"));
		TEMPTrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_TRANSLATE);

		TEMPTrackNames.PushAndGrow(atString("rotate"));
		TEMPTrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_ROTATE);

		// find any user defined tracks, add them
		for(int i=0; i<m_ChannelInputNames.GetCount(); i++)
		{
			const atString& s = m_ChannelInputNames[i];

			if(s != "translateX" && s != "translateY" && s != "translateZ" &&
				s != "rotateX" && s != "rotateY" && s != "rotateZ" &&
				s != "scaleX" && s != "scaleY" && s != "scaleZ")
			{
				TEMPTrackNames.PushAndGrow(s);
				int trackID = rexObjectGenericAnimation::TRACK_UNKNOWN;
				if(s == "focusDistance")
					trackID = rexObjectGenericAnimation::TRACK_DISTANCE;
				TEMPTrackIDs.PushAndGrow(trackID);
			}
		}

		// Initialize channels.
		int frameCount = ef - sf + 1;
		for( int i = 0; i < boneCount; i++ )
		{
			InitTracksInfo(dpList[i], frameCount, boneList[i]->m_Tracks, TEMPTrackNames, TEMPTrackIDs);
		}

		// Force animation data dirty.
		rexMayaUtility::SetFrame(sf-1);

		// Convert animation data for all frames.
		for( int f = sf; f <= ef; f++ )
		{
			rexMayaUtility::SetFrame(f);

			for( int b = 0; b < boneCount; b++ )
			{
				GetTrackFrameData(dpList[b], boneList[b]->m_Tracks);
			}
		}

		rexMayaUtility::SetFrame(sf);
	}


	void rexConverterAnimation::ConvertFromFrames(const MDagPath& dp, int sf, int ef, atArray<rexObjectGenericAnimation::TrackInfo*>& tracks) const
	{
		// Force animation data dirty.
		rexMayaUtility::SetFrame(sf-1);

		for(int f=sf; f<=ef; f++)
		{
			rexMayaUtility::SetFrame(f);

			GetTrackFrameData(dp, tracks);
		}

		rexMayaUtility::SetFrame(sf);
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////
	// [RSV CLEMENS] new time-based functions
	///////////////////////////////////////////////////////////////////////////////////////////

	/*
	PURPOSE
	Get animation data from all frames of maya nodes and save it into
	generic nodes.
	PARAMS
	dpList - maya node list.
	boneList - generic node list.
	RETURN
	none.
	NOTES
	*/
	void rexConverterAnimation::ConvertFromAllFrames( const atArray<MDagPath> &dpList, int frameCount, float start, double deltaTime, atArray<rexObjectGenericAnimation::ChunkInfo*>& boneList) const
	{
		int dpCount = dpList.GetCount();
		int boneCount = boneList.GetCount();
		if(dpCount == 0 || boneCount != dpCount)
			return;

		atArray<atString>	TEMPTrackNames;
		atArray<int>		TEMPTrackIDs;

		TEMPTrackNames.PushAndGrow(atString("translate"));
		TEMPTrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_TRANSLATE);

		TEMPTrackNames.PushAndGrow(atString("rotate"));
		TEMPTrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_ROTATE);

		if(m_bUseAnimCtrlExportFile)
		{
			//Add tracks for any non translate/rotate/scale specs that may be in the animation control file
			std::list<string> unknownTracks;

			int nSpecs = m_AnimExportCtrlSpec.GetNumTrackSpecs();
			for(int specIdx=0; specIdx<nSpecs; specIdx++)
			{
				const AnimExportCtrlTrackSpec& spec = m_AnimExportCtrlSpec.GetTrackSpec(specIdx);
				int nTracks = spec.GetTrackCount();
				for(int trackIdx=0; trackIdx < nTracks; trackIdx++)
				{
					const AnimExportCtrlTrack& track = spec.GetTrack(trackIdx);
					const char* szInputName = track.GetInputName();
					if( (strcmpi(szInputName, "translate") !=0) &&
						(strcmpi(szInputName, "rotate") !=0) &&
						(strcmpi(szInputName, "scale") !=0) )
					{
						if(std::find(unknownTracks.begin(), unknownTracks.end(), szInputName) == unknownTracks.end())
						{
							TEMPTrackNames.PushAndGrow(atString(szInputName));
							
							if(strcmpi("focusDistance", szInputName))
								TEMPTrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_DISTANCE);
							else
								TEMPTrackIDs.PushAndGrow(rexObjectGenericAnimation::TRACK_UNKNOWN);

							//Store the track name we added to avoid duplicate entries
							unknownTracks.push_back(szInputName);
						}
					}
				}
			}
		}
		else
		{
			// find any user defined tracks, add them
			for(int i=0; i<m_ChannelInputNames.GetCount(); i++)
			{
				const atString& s = m_ChannelInputNames[i];

				if(s != "translateX" && s != "translateY" && s != "translateZ" &&
					s != "rotateX" && s != "rotateY" && s != "rotateZ" &&
					s != "scaleX" && s != "scaleY" && s != "scaleZ")
				{
					TEMPTrackNames.PushAndGrow(s);
					int trackID = rexObjectGenericAnimation::TRACK_UNKNOWN;
					if(s == "focusDistance")
						trackID = rexObjectGenericAnimation::TRACK_DISTANCE;
					TEMPTrackIDs.PushAndGrow(trackID);
				}
			}
		}

		// Initialize channels.
		for( int i = 0; i < boneCount; i++ )
		{
			InitTracksInfo(dpList[i], frameCount, boneList[i]->m_Tracks, TEMPTrackNames, TEMPTrackIDs);
		}

		// Force animation data dirty.
		rexMayaUtility::SetFrame(-1);

		for(int i = 0; i < frameCount; i++)
		{
			double t = (double)start + (deltaTime * (double)i);
			MAnimControl::setCurrentTime(MTime(t, MTime::kSeconds));
			for( int b = 0; b < boneCount; b++ )
			{
				GetTrackFrameData(dpList[b], boneList[b]->m_Tracks);
			}
		}

		MAnimControl::setCurrentTime(MTime(double(start), MTime::kSeconds));
	}

	void rexConverterAnimation::ConvertFromFrames(const MDagPath& dp, int frameCount, float start, double deltaTime, atArray<rexObjectGenericAnimation::TrackInfo*>& tracks) const
	{
		// Force animation data dirty.
		rexMayaUtility::SetFrame(-1);

		for(int i=0; i < frameCount; i++)
		{
			double t = (double)start + (deltaTime * (double)i);
			MAnimControl::setCurrentTime(MTime(double(t), MTime::kSeconds));
			GetTrackFrameData(dp, tracks);
		}

		MAnimControl::setCurrentTime(MTime(double(start), MTime::kSeconds));
	}

	///////////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationMoverNode::SetupModuleInstance( rexModule& module, rexValue /*value*/, const atArray<rexObject*>& objects ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );

		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		if( objects.GetCount() )
		{
			for(int i=0; i<objects.GetCount(); i++)
			{
				rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[ i ] );
				if( dpObj )
					conv->AppendMoverNode( dpObj->m_DagPath );
				else
					return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
			}
		}

		return rexResult( rexResult::PERFECT );

	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationBlendShapeNode::SetupModuleInstance( rexModule& module, rexValue /*value*/, const atArray<rexObject*>& objects ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );

		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		if( objects.GetCount() )
		{
			for(int i=0; i<objects.GetCount(); i++)
			{
				rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[ i ] );
				if( dpObj )
					conv->AppendBlendShapeNode( dpObj->m_DagPath );
				else
					return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
			}
		}

		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationAnimNormalMapNode::SetupModuleInstance( rexModule& module, rexValue /*value*/, const atArray<rexObject*>& objects ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );

		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		if( objects.GetCount() )
		{
			for(int i=0; i < objects.GetCount(); i++)
			{
				rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[i] );
				if(dpObj)
				{
					conv->AppendAnimNormalMapNode( dpObj->m_DagPath );
				}
				else
				{
					return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
				}
			}
		}

		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationExprControlNode::SetupModuleInstance( rexModule& module, rexValue /*value*/, const atArray<rexObject*>& objects ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );

		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		if( objects.GetCount() )
		{
			for(int i=0; i < objects.GetCount(); i++)
			{
				rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[i] );
				if(dpObj)
				{
					conv->AppendAnimExprControlNode( dpObj->m_DagPath );
				}
				else
				{
					return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
				}
			}
		}

		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationAllowMultipleMovers::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );

		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );


		conv->SetAllowMultipleMovers(value.toBool( conv->GetAllowMultipleMovers() ) );

		return rexResult( rexResult::PERFECT );
	}
		
	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationParentNode::SetupModuleInstance( rexModule& module, rexValue /*value*/, const atArray<rexObject*>& objects ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );

		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		if( objects.GetCount() )
		{
			rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[ 0 ] );
			if( dpObj )
				conv->SetParentNode( dpObj->m_DagPath );
			else
				return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}

		return rexResult( rexResult::PERFECT );

	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult  rexPropertyAnimationSegmentStartFrame::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexObjectGenericAnimationSegment* segment = dynamic_cast<rexObjectGenericAnimationSegment*>( &obj );

		if( !segment )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		float v = value.toFloat(-1.0f);
		segment->m_StartTime = ( v < 0 ) ? v : rexMayaUtility::ConvertTime(v);

		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult  rexPropertyAnimationSegmentEndFrame::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexObjectGenericAnimationSegment* segment = dynamic_cast<rexObjectGenericAnimationSegment*>( &obj );

		if( !segment )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		float v = value.toFloat(-1.0f);
		segment->m_EndTime = ( v < 0 ) ? v : rexMayaUtility::ConvertTime(v);

		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult  rexPropertyAnimationSegmentAutoplay ::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexObjectGenericAnimationSegment* segment = dynamic_cast<rexObjectGenericAnimationSegment*>( &obj );

		if( !segment )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		segment->m_Autoplay = value.toBool( segment->m_Autoplay );

		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult  rexPropertyAnimationSegmentLooping ::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexObjectGenericAnimationSegment* segment = dynamic_cast<rexObjectGenericAnimationSegment*>( &obj );

		if( !segment )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		segment->m_Looping = value.toBool( segment->m_Looping );

		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationSegmentDontExportRoot::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexObjectGenericAnimationSegment* segment = dynamic_cast<rexObjectGenericAnimationSegment*>( &obj );

		if( !segment )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		segment->m_DontExportRoot = value.toBool( segment->m_DontExportRoot );

		return rexResult( rexResult::PERFECT );
	}
	
	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationSampleRate::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );
		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		conv->SetSampleRate( value.toFloat( conv->GetSampleRate() ) );	
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationCurveFittingTolerance::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );
		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		conv->SetCurveFittingTolerance( value.toFloat( conv->GetCurveFittingTolerance() ) );	
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationTranslateScale::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );
		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		conv->SetTranslateScale( value.toFloat( conv->GetTranslateScale() ) );
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyApplyMatrixToAnimation::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );
		if( !conv )
		{
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		}

		conv->SetApplyMatrixToAnimation(true);
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationPrefixSegNameWithFilename::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexConverterAnimationSegment* conv = dynamic_cast<rexConverterAnimationSegment*>( module.m_Converter );
		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		conv->SetPrefixSegNameWithFilename( value.toBool( conv->GetPrefixSegNameWithFilename() ) );
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////

	rexResult rexPropertyAnimationFramesPerSecond::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
	{
		rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );
		if( !conv )
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

		conv->SetFramesPerSecond( value.toFloat( conv->GetFramesPerSecond() ) );
		return rexResult( rexResult::PERFECT );
	}

	/////////////////////////////////////////////////////////////////////////////////////
} // namespace rage

