#include "converterFX.h"
#include "rexGeneric/objectFX.h"

#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"
#include "rexBase/module.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <Maya/MFnDagNode.h>
#pragma warning(pop)

using namespace rage;

/////////////////////////

rexResult rexConverterRmworldFX::Convert( const atArray<rexObject*>& /*inputObjectArray*/, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	rexObjectRmworldFX* fx = new rexObjectRmworldFX;
	fx->SetName( defaultName );

	int shapeCount = m_Shapes.GetCount();
	for( int a = 0; a < shapeCount; a++ )
	{
		bool isNodeTransform = ( m_Shapes[ a ].apiType() == MFn::kTransform );
		int child = -1;
		if( isNodeTransform )
		{
			int childCount = m_Shapes[ a ].childCount();
			MFn::Type childType;
			for( int b = 0; b < childCount; b++ )
			{
				childType = m_Shapes[ a ].child( b ).apiType();
				if( ( childType == MFn::kNurbsCurve ) || ( childType == MFn::kLocator ) )
				{
					child = b;
					break;
				}
			}
		}
		MDagPath parentDP( m_Shapes[ a ] );
		parentDP.pop();
		MObject shapeObj = isNodeTransform ? m_Shapes[ a ].child( ( child >= 0 ) ? child : 0 ) : m_Shapes[ a ].node();
		MObject	transformObj = isNodeTransform ? m_Shapes[ a ].node() : parentDP.node();
		MFnDagNode fn( shapeObj );
		MFnDagNode fnTransform( transformObj );

		MFn::Type type = shapeObj.apiType();
		if( type == MFn::kNurbsCurve )
			fx->m_CurveNames.PushAndGrow( atString( fnTransform.name().asChar() ) );
		else if( type == MFn::kLocator )
			fx->m_LocatorNames.PushAndGrow( atString( fnTransform.name().asChar() ) );
		else
		{
			result.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
			result.AddMessage( "FX (%s) connected to node '%s', which is neither a curve nor a locator.  Ignored.", (const char*)defaultName, m_Shapes[ a ].fullPathName().asChar()  );
		}
	}

	outputObject = fx;

	return result;
}

/////////////////////////

rexResult rexPropertyFXShapeNodes::SetupModuleInstance( rexModule& module, rexValue /*value*/, const atArray<rexObject*>& objects ) const
{
	rexConverterRmworldFX* conv = dynamic_cast<rexConverterRmworldFX*>( module.m_Converter );

	if( !conv )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	int objectCount = objects.GetCount();
	for( int a = 0; a < objectCount; a++ )
	{
		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[ a ] );
		if( dpObj )
			conv->AddShape( dpObj->m_DagPath );
		else
			return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
	}

	return rexResult( rexResult::PERFECT );

}

/////////////////////////////////////////////////////////////////////////////////////


