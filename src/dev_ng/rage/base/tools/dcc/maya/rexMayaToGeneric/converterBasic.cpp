#include "rexGeneric/objectHierarchy.h"
#include "rexGeneric/objectEntity.h"
#include "rexMaya/mayahelp.h"
#include "rexMaya/objectMDagPath.h"
#include "rexMayaToGeneric/converterBasic.h"

/////////////////////////////////////////////////////////////////////////////////////
namespace rage {

rexResult rexConverterHierarchy::Convert( const atArray<rexObject*>& inputObjectArray, rexObject*& outputObject, const atString& defaultName ) const
{
	rexResult result( rexResult::PERFECT );

	// create object 
	rexObjectHierarchy* hier = CreateHierarchyNode();
	hier->SetName( defaultName );
	Assert( hier );

	// get module-specific scene data here and fill out data structures as necessary
	int inputObjectCount = inputObjectArray.GetCount();

	if( m_ProgressBarObjectCountCB )
		(*m_ProgressBarObjectCountCB)( inputObjectCount );

	for( int a = 0; a < inputObjectCount; a++ )
	{		
		if( m_ProgressBarObjectCurrentIndexCB )
			(*m_ProgressBarObjectCurrentIndexCB)( a );

		rexObject* subObject = NULL;
		rexResult thisResult = ConvertSubObject( *inputObjectArray[ a ], subObject );
		
		if( thisResult.Warnings() || thisResult.Errors() )
			result.Combine( thisResult );
		
		if( thisResult.OK() && subObject )
			hier->m_ContainedObjects.PushAndGrow( subObject,(u16) inputObjectCount );
	}

	// set up return values
	outputObject = hier;
	
	return result;
}

} // namespace rage

/////////////////////////////////////////////////////////////////////////////////////
