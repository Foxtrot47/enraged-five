#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"

#include "rexBase/module.h"
#include "rexGeneric/objectEntity.h"
#include "rexGeneric/objectMesh.h"
#include "rexMaya/objectMDagPath.h"
#include "rexMaya/shell.h"
#include "rexMayaToGeneric/propertyBasic.h"
#include <string.h>

namespace rage {

///////////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyName::ModifyObject( rexObject& obj, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexObjectGenericEntityComponent* comp = dynamic_cast<rexObjectGenericEntityComponent*>( &obj );

	if( !comp )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	comp->SetName( value.toString() );
	
	return rexResult( rexResult::PERFECT );
}

/////////////////////////////////////////////////////////////////////////////////////


rexResult  rexPropertyOutputPath::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	if( module.m_Serializer )
	{
		// concatenation lets us set up base paths and whatnot
		// if colon found in value, replace.  otherwise, concatenate
		atString valueString(value.toString());
		if( strstr( valueString, ":" ) )
		{
			module.m_Serializer->SetOutputPath( valueString );	
		}
		else
		{
			atString outputPath = module.m_Serializer->GetOutputPath();
			outputPath += valueString;
			module.m_Serializer->SetOutputPath( outputPath );	
		}
		
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyExportData::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	if( module.m_Serializer )
	{
		module.m_Serializer->SetExportData( value.toBool( module.m_Serializer->GetExportData() ) );	
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyInputNodes::SetupModuleInstance( rexModule& module, rexValue /*value*/, const atArray<rexObject*>& objects ) const
{
	while( module.m_InputObjects.GetCount() )
		delete module.m_InputObjects.Pop();
	module.m_InputObjects.Reset();

	int numInputObj = objects.GetCount();

	rexObjectMDagPath *obj = NULL;
	for(int i = 0; i < numInputObj; i++)
	{
		obj = dynamic_cast<rexObjectMDagPath*>(objects[i]);
		Assert(obj);
		if(obj)
			module.m_InputObjects.PushAndGrow(new rexObjectMDagPath(obj),(u16) numInputObj);
	}
	return rexResult( rexResult::PERFECT );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyBoneTag::SetupShell( const atString& /*objectName*/, rexShell& _shell, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexResult retval( rexResult::PERFECT );

	rexShellMaya* shell = dynamic_cast<rexShellMaya*>( &_shell );

	if( !shell )
	{
		retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		return retval;
	}		

	//int objectCount = objects.GetCount();
	//if( objectCount != 1 )
	//{
	//	// krose 5/26/2005 commented out the two lines below as they make no sense since REX has no idea whether this should be an error or not
	//	//retval.Combine( rexResult::WARNINGS );
	//	//retval.AddMessage( "BoneTag '%s' has %s connected nodes!", value.toString(), objectCount ? "multiple" : "no" );
	//	return retval;
	//}

	for(int i=0; i<objects.GetCount(); i++)
	{
		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[ i ] );
		Assert( dpObj );
		shell->RegisterBoneTag( atString( value.toString() ), dpObj->m_DagPath );
	}
	
	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertySpecialFlag::SetupShell( const atString& /*objectName*/, rexShell& _shell, rexValue value, const atArray<rexObject*>& objects ) const
{
	rexResult retval( rexResult::PERFECT );

	rexShellMaya* shell = dynamic_cast<rexShellMaya*>( &_shell );

	if( !shell )
	{
		retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		return retval;
	}		

	atString valueString(value.toString());
	atArray<atString> valueStrings;
	int len = valueString.GetLength();
	int lastSemiColon = -1;
	char buffer[64];
	int currCharCount = 0;
	for( int c = 0; c < len; c++ )
	{
		bool isSemiColon = ( valueString[ c ] == ';' );
		if( !isSemiColon )
			buffer[ currCharCount++ ] = valueString[ c ];
		
		if( isSemiColon || ( c == ( len - 1 ) ) )
		{
			buffer[ currCharCount++ ] = 0;
			valueStrings.Grow((u16) len) = buffer;
			lastSemiColon = c;
			currCharCount = 0;
		}		
	}

	int objectCount = objects.GetCount();

	for( int a = 0; a < objectCount; a++ )
	{
		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[ a ] );
		Assert( dpObj );
		int valueStringCount = valueStrings.GetCount();
		for( int v = 0; v < valueStringCount; v++ )
			shell->RegisterSpecialFlag( valueStrings[ v ], dpObj->m_DagPath );
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyGroupTag::SetupShell( const atString& objectName, rexShell& _shell, rexValue /*value*/, const atArray<rexObject*>& objects ) const
{
	rexResult retval( rexResult::PERFECT );

	rexShellMaya* shell = dynamic_cast<rexShellMaya*>( &_shell );

	if( !shell )
	{
		retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		return retval;
	}		

	int objectCount = objects.GetCount();
	for( int a = 0; a < objectCount; a++ )
	{
		rexObjectMDagPath* dpObj = dynamic_cast<rexObjectMDagPath*>( objects[ a ] );
		Assert( dpObj );
		shell->RegisterGlobalObject( objectName, dpObj->m_DagPath );
	}

	return retval;
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertySkeletonUsesOnlyJoints::SetupShell( const atString& /*objectName*/, rexShell& _shell, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexResult retval( rexResult::PERFECT );

	rexShellMaya* shell = dynamic_cast<rexShellMaya*>( &_shell );

	if( !shell )
	{
		retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		return retval;
	}		

	shell->SetSkeletonUsesOnlyJoints( value.toBool( shell->GetSkeletonUsesOnlyJoints() ) );

	return retval;
}


/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyUnitConversion::SetupShell( const atString& /*objectName*/, rexShell& _shell, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexResult retval( rexResult::PERFECT );

	rexShellMaya* shell = dynamic_cast<rexShellMaya*>( &_shell );

	if( !shell )
	{
		retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		return retval;
	}		

	rexSerializer::SetBaseUnits(rexSerializer::CENTIMETERS); // Maya ALWAYS reports units in centimeters through the API
	rexSerializer::SetUnitTypeToConvertTo(value.toString());

	return retval;
}


/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyUseBoneIDs::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	if( module.m_Serializer )
	{
		module.m_Serializer->SetUseBoneIDs( value.toBool( module.m_Serializer->GetUseBoneIDs() ) );	
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyIgnoreNullJoints::SetupShell( const atString& /*objectName*/, rexShell& _shell, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexResult retval( rexResult::PERFECT );

	rexShellMaya* shell = dynamic_cast<rexShellMaya*>( &_shell );

	if( !shell )
	{
		retval.Combine( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
		return retval;
	}		

	shell->SetIgnoreNullJoints( value.toBool( shell->GetIgnoreNullJoints() ) );

	return retval;
}
/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyUseBoundFiltering::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	if( module.m_Serializer )
	{
		module.m_Serializer->SetUseBoundFiltering( value.toBool( module.m_Serializer->GetUseBoundFiltering() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyBoundFilteringBoundTypes::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	if( module.m_Serializer )
	{
		if (value.toString())
		{
			std::string types = value.toString();
			unsigned int startParen = types.find_first_of('(');
			unsigned int endParen = types.find_last_of(')');
			if( (startParen == std::string::npos) ||
				(endParen == std::string::npos) )
			{
				return rexResult( rexResult::WARNINGS, rexResult::INVALID_INPUT );
			}
			unsigned int charIndex = startParen+1;
			while(charIndex < endParen)
			{
				atString atType;
				while( (types[charIndex] != '|') &&
					(types[charIndex] != ')') )
				{
					atType += types[charIndex++];
				}
				module.m_Serializer->AddBoundFilterType(atType);
				charIndex++;
			}
			return rexResult( rexResult::PERFECT );
		}
		else
		{
			return rexResult( rexResult::WARNINGS, rexResult::INVALID_INPUT );
		}		
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

} // namespace rage
