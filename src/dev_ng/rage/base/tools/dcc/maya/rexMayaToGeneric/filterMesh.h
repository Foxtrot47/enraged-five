// 
// rexMayaToGeneric/filterMesh.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya Rockstar Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		RTTI
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexFilterMesh -- filter that accepts meshes
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYA_ROCKSTAR_FILTERMESH_H__
#define __REX_MAYA_ROCKSTAR_FILTERMESH_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/filter.h"
#include "rexMaya/mayaUtility.h"
#include "rexMaya/objectMDagPath.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////////

class rexFilterMesh : public rexFilter
{
public:
	virtual bool AcceptObject( const rexObject& inputObject ) const 
	{ 
		const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &inputObject );
		if( !dpObj )
			return false;

		return ( dpObj->m_DagPath.apiType() == MFn::kMesh );
	}
	
	virtual rexFilter*		CreateNew() const { return new rexFilterMesh; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexFilterMeshWithTextureAnimation : public rexFilter
{
public:
	virtual bool AcceptObject( const rexObject& inputObject ) const 
	{ 
		const rexObjectMDagPath* dpObj = dynamic_cast<const rexObjectMDagPath*>( &inputObject );
		if( !dpObj )
			return false;

		return ( dpObj->m_DagPath.apiType() == MFn::kMesh ) && rexMayaUtility::HasTextureAnimation( dpObj->m_DagPath );
	}

	virtual rexFilter*		CreateNew() const { return new rexFilterMeshWithTextureAnimation; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
