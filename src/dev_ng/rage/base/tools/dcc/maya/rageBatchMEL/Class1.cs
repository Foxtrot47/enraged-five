using System;
using System.IO;
using System.Collections;
using rageUsefulCSharpToolClasses;
using System.Windows.Forms;

namespace rageBatchMEL
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			// Get a ball of strings from the user
//			string strSourceFolder = rageSimpleUIElements.FolderBrowser("Please choose folder containing source mocap Maya files", "P:\\pong\\MoCap\\!New\\Final Maya Files").Replace("\\", "/");
//			if(strSourceFolder == "") return;

			// Get MEL file to source
			OpenFileDialog openFileDialog0 = new OpenFileDialog();

			openFileDialog0.InitialDirectory = "T:\\temp\\" ;
			openFileDialog0.Filter = "MEL Scripts (*.mel)|*.mel";
			openFileDialog0.FilterIndex = 2 ;
			openFileDialog0.RestoreDirectory = true ;
			openFileDialog0.Multiselect = false;
			openFileDialog0.Title = ("Select MEL file to source for each file");

			if(openFileDialog0.ShowDialog() != DialogResult.OK)
			{
				// They quit
				return;
			}
			string strMelFile = openFileDialog0.FileName.Replace("\\", "/");

			// Get files to export
			OpenFileDialog openFileDialog1 = new OpenFileDialog();

			openFileDialog1.InitialDirectory = "T:\\pong\\art\\";
			openFileDialog1.Filter = "Maya binary files (*.mb)|*.mb|Maya ascii files (*.ma)|*.ma" ;
			openFileDialog1.FilterIndex = 2 ;
			openFileDialog1.RestoreDirectory = true ;
			openFileDialog1.Multiselect = true;
			openFileDialog1.Title = ("Select files to process");

			if(openFileDialog1.ShowDialog() != DialogResult.OK)
			{
				// They quit
				return;
			}
			string[] astrFilesToExport = openFileDialog1.FileNames;

			// Get environment to use
			OpenFileDialog openFileDialog2 = new OpenFileDialog();

			openFileDialog2.InitialDirectory = "T:\\rage\\tools\\Modules\\base\\rage\\exes\\" ;
			openFileDialog2.Filter = "Rage Environments (SetupEnvironmentFor*AndMaya*.bat)|SetupEnvironmentFor*AndMaya*.bat";
			openFileDialog2.FilterIndex = 2 ;
			openFileDialog2.RestoreDirectory = true ;
			openFileDialog2.Multiselect = false;
			openFileDialog2.Title = ("Select environment to use");

			if(openFileDialog2.ShowDialog() != DialogResult.OK)
			{
				// They quit
				return;
			}
			string strEnvironmentBatFile = openFileDialog2.FileName;

			// Logging folder
			string strLogFolder = rageSimpleUIElements.FolderBrowser("Please choose folder to log progess to", "T:\\rageLogs").Replace("\\", "/");
			if(strLogFolder == "") return;

			// Start logging
			rageHtmlLog obHtmlLog = new rageHtmlLog(strLogFolder + "index.html", "Rage Batch MEL");

			// Got everything I need to know, setup a Maya for the process
			rageMayaBatch obMayaBatch = new rageMayaBatch();
			obMayaBatch.SetBatFileToSetupEnvironment(strEnvironmentBatFile);

			// Go through files, exporting as I go
			foreach( string strFile in astrFilesToExport)
			{
				// strFile = strFile.Replace("\\", "/");
				System.Console.WriteLine(strFile);

				// Export it
				rageStatus obTempStatus;
				obHtmlLog.LogEvent("Processing "+ strFile.Replace("\\", "/"), out obTempStatus);
				rageStatus obStatus;
				obMayaBatch.BatchExecuteMel("file -force -open \\\""+ strFile.Replace("\\", "/") +"\\\"; print(\\\"***   "+ strMelFile +"   ***\\\\n\\\"); source \\\""+ strMelFile +"\\\"; ", out obStatus);
				string strLogFilename = rageFileUtilities.RemoveFileExtension(rageFileUtilities.GetFilenameFromFilePath(strFile)) +".log.html";
				rageFileUtilities.MoveFile(obMayaBatch.GetLogFilename(), strLogFolder + strLogFilename, out obTempStatus);
				if(!obStatus.Success())
				{
					// Errors occured
					Console.WriteLine("Errors occured");
					obHtmlLog.LogEvent("<font color=#FF0000>Errors occured</font> <a href=\""+ strLogFilename +"\">Complete log</a>", out obTempStatus);
				}
				else
				{
					Console.WriteLine("Successful");
					obHtmlLog.LogEvent("Success! <a href=\""+ strLogFilename +"\">Complete log</a>", out obTempStatus);
				}
			}
		}
	}
}

