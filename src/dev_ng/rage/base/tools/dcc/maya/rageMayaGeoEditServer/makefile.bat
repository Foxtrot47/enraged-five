call ..\..\libs\rex\setmayapath.bat
set XDEFINE=NT_PLUGIN
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\tools %RAGE_DIR%\base\tools\deprecated
set LIBS=%LIBS% %RAGE_CORE_LIBS% parser parsercore rageGEOEditCommands
set TESTER_BASE=maya_plugin
set TESTERS=rageMayaGeoEditServer
set RESOURCES=pluginMain.cpp pluginMain.h rageMayaGeoEditServer.h