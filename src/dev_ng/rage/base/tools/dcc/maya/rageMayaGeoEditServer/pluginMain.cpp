// 
// /pluginMain.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma warning(push)
#if _MSC_VER >= 1400
#pragma warning(disable: 4005) // warning C4005: 'strcasecmp' : macro redefinition
							   // rage\base\src\string\string.h declares strcasecmp, and so does MTypes.h
							   // but they are currently differenct
#endif

#include "rageMayaGeoEditServer.h"
#include "system/param.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MGlobal.h>
#include <maya/MFnPlugin.h>
#include <maya/MMessage.h>
#include <maya/MSceneMessage.h>

#pragma warning(pop)

//-----------------------------------------------------------------------------

static MCallbackId sBeforeNewCallbackId = 0;
static MCallbackId sAfterOpenCallbackId = 0;
static MCallbackId sMayaExitingCallbackId = 0;

static bool bPluginNeedsCleanup = true;

//-----------------------------------------------------------------------------

static void SceneMayaExitingCallback(void* /*clientData*/)
{
	//Maya doesn't seem to always call the unitializePlugin method when its exiting, so
	//use this method to make sure all resources are freed up.  This helps to eliminate
	//memory leak dumps that really shouldn't be memory leaks.
	if(bPluginNeedsCleanup)
	{
		SHUTDOWN_GEOEDITSVR;
		SHUTDOWN_PARSER;
		fiDeviceTcpIp::ShutdownClass();

		bPluginNeedsCleanup = false;
	}
}

//-----------------------------------------------------------------------------

MStatus initializePlugin( MObject obj )
{ 
	MStatus   status;
	MFnPlugin plugin( obj, "", "6.0", "Any");

	rage::sysParam::Init(0,NULL);

	fiDeviceTcpIp::InitClass(0, NULL);
	INIT_PARSER;
	INIT_GEOEDITSVR;

	//Register Commands
	status = plugin.registerCommand("rageMayaGeoEditServer", rageMayaGeoEditServerCmd::creator, rageMayaGeoEditServerCmd::newSyntax);
	if (!status) {
		status.perror("registerNode");
		return status;
	}

	//Register Scene Callbacks
	sBeforeNewCallbackId = MSceneMessage::addCallback(MSceneMessage::kBeforeNew, SceneBeforeNewCallback, NULL, &status);
	sAfterOpenCallbackId = MSceneMessage::addCallback(MSceneMessage::kAfterOpen, SceneAfterOpenCallback, NULL, &status);
	sMayaExitingCallbackId = MSceneMessage::addCallback(MSceneMessage::kMayaExiting, SceneMayaExitingCallback, NULL, &status);

	return status;
}

//-----------------------------------------------------------------------------

MStatus uninitializePlugin( MObject obj)
{
	MStatus   status;
	MFnPlugin plugin( obj );

	//Unregister Scene Callbacks
	MSceneMessage::removeCallback(sBeforeNewCallbackId);
	MSceneMessage::removeCallback(sAfterOpenCallbackId);
	MSceneMessage::removeCallback(sMayaExitingCallbackId);

	status = plugin.deregisterCommand("rageMayaGeoEditServer");
	if (!status) {
		status.perror("deregisterNode");
		return status;
	}
	
	if(bPluginNeedsCleanup)
	{
		SHUTDOWN_GEOEDITSVR;
		SHUTDOWN_PARSER;
		fiDeviceTcpIp::ShutdownClass();

		bPluginNeedsCleanup = false;
	}

	return status;
}

//-----------------------------------------------------------------------------
