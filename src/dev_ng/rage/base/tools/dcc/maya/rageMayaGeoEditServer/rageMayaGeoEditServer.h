// 
// /rageMayaGeoEditServer.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __RAGEMAYAGEOEDITSERVER_H_
#define __RAGEMAYAGEOEDITSERVER_H_

#pragma warning(disable : 4702) //Disable the 'unreachable code' warning that seems to appear in STLPort _tree.h
#include <map>

#include "atl/array.h"
#include "atl/queue.h"
#include "atl/singleton.h"
#include "file/tcpip.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include <maya/MGlobal.h>
#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MCommandResult.h>
#include <maya/MStringArray.h>

#pragma warning(pop)

#include "rageGEOEditCommands/rageGEOEditCommands.h"

using namespace rage;

//-----------------------------------------------------------------------------

void SceneBeforeNewCallback(void*);
void SceneAfterOpenCallback(void*); 

//-----------------------------------------------------------------------------

struct recGeoEditCmd
{
	int	cmdLength;
};

//-----------------------------------------------------------------------------

struct cmdListenerThreadData
{
	unsigned int cmdPort;
	unsigned int responsePort;
};

//-----------------------------------------------------------------------------

struct aSyncDCCCallback
{
	aSyncDCCCallback() {};
	aSyncDCCCallback(const char* success, const char* fail)
		: successCbkStr(success)
		, failCbkStr(fail) 
	{};

	string	successCbkStr;
	string	failCbkStr;
};

//-----------------------------------------------------------------------------

class rageMayaGeoEditServer
{
public:
	~rageMayaGeoEditServer();

	void			Start(unsigned int cmdPort, unsigned int responsePort);
	void			Stop();

	static void		CommandListenerThreadFn(void*);
	static void		DispatchCmdThreadFn(void*);

	void			SendCommand(const std::string &cmdResult);
	void			SendCommand(GeoEditCommand* pCmd);

	void			SendCommandAsync(GeoEditCommand* pCmd, const MString& successCbkStr, const MString& failCbkStr );
	void			SendCommandSync(GeoEditCommand* pCmd);

	void			HandleCreateTypeCommand(GeoEditCommand* pCmd);
	void			HandleConnectParamaterCommand(GeoEditCommand* pCmd);
	void			HandleDisconnectParameterCommand(GeoEditCommand* pCmd);
	void			HandleSyncToGeoEditorCommand(GeoEditCommand* pCmd);
	void			HandleSelectNodesCommand(GeoEditCommand* pCmd);
	void			HandleIsParameterConnectedCommand(GeoEditCommand* pCmd);
	void			HandleHandshakeCommand(GeoEditCommand* pCmd);
	void			HandleGetDCCFilePathCommand(GeoEditCommand* pCmd);
	void			HandleRenameTypeNodeCommand(GeoEditCommand* pCmd);
	void			HandleGeneralEditorCmdResult(GeoEditCommand* pCmd);

	void			SetGeoMELCommandResult(const MString& result);

	unsigned int	GetNewCmdId();
	bool			GetDCCResultFromString(const string& dccResultStr, GeoEditDCCResult** pDCCResultObj);

	bool			IsEditorConnected() { return (m_CmdSocketHandle == fiHandleInvalid) ? false : true; }

protected:
	rageMayaGeoEditServer();

private:
	fiHandle						m_CmdSocketHandle;
	fiHandle						m_CmdSocketListenerHandle;
	fiHandle						m_ResponseSocketHandle;
	
	bool							m_StopListener;
	bool							m_ResetListener;

	bool							m_StopDispatcher;

	atQueue<GeoEditCommand*, 64>	m_CommandQueue;

	std::map<int,aSyncDCCCallback>	m_AsyncDCCCallbacks;
	std::map<int,HANDLE>			m_SyncCmdEventMap;

	HANDLE							m_hCmdQueueMutex;
	HANDLE							m_hCmdExecFinishedEvent;
	HANDLE							m_hCmdIdMutex;
	CRITICAL_SECTION				m_csResponseSocketAccess;

	MString							m_geoMelCmdResult;

	HANDLE							m_listenerThreadId;
	HANDLE							m_dispatcherThreadId;

	unsigned int					m_CmdIdCounter;
};

typedef atSingleton<rageMayaGeoEditServer> rageMayaGeoEditServerSingleton;

#define GEOEDITSVR				::rageMayaGeoEditServerSingleton::InstanceRef()
#define INIT_GEOEDITSVR			::rageMayaGeoEditServerSingleton::Instantiate()
#define SHUTDOWN_GEOEDITSVR		::rageMayaGeoEditServerSingleton::Destroy()

//-----------------------------------------------------------------------------

class rageMayaGeoEditServerCmd : public MPxCommand
{
public:
					rageMayaGeoEditServerCmd();
	virtual			~rageMayaGeoEditServerCmd();

	MStatus			doIt( const MArgList &args);
	bool			isUndoable() const { return false; }

	static void*	creator();
	static MSyntax	newSyntax();

private:
#ifdef _DEBUG
	void			WriteCommands(const char* outputPath);
#endif //_DEBUG
};

//-----------------------------------------------------------------------------


#endif //__RAGEMAYAGEOEDITSERVER_H_
