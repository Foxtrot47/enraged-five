// 
// /rageMayaGeoEditServer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma warning(push)
#if _MSC_VER >= 1400
#pragma warning(disable: 4005) // warning C4005: 'strcasecmp' : macro redefinition
							   // rage\base\src\string\string.h declares strcasecmp, and so does MTypes.h
							   // but they are currently differenct
#endif

#include <process.h>

#include "file/asset.h"
#include "file/device.h"
#include "system/timer.h"

#include "rageGEOEditCommands/rageGEOEditCommands.h"

#include "rageMayaGeoEditServer.h"

#define kStartServer			"-st"
#define kStartServerLong		"-start"
#define kStopServer				"-sp"
#define kStopServerLong			"-stop"
#define kSetCmdRes				"-cr"
#define kSetCmdResLong			"-cmdres"
#define kIsEditorConnected		"-ec"
#define kIsEditorConnectedLong	"-editconnected"

#define kGeneralEditorCmdSync		"-ics"
#define kGeneralEditorCmdSyncLong	"-issuecmdsync"

#define kGeneralEditorCmdASync		"-ica"
#define kGeneralEditorCmdASyncLong	"-issuecmdasync"

#define LOCALHOST				"127.0.0.1"

#define LISTENER_SLEEP_TIME		100
#define DISPATCHER_SLEEP_TIME	100
#define WAIT_CMD_EXEC_TIME		10000
#define SYNC_CMD_TIMEOUT		100000

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------

void SceneBeforeNewCallback(void*)
{

}

//-----------------------------------------------------------------------------

void SceneAfterOpenCallback(void*)
{

}

//-----------------------------------------------------------------------------

void* rageMayaGeoEditServerCmd::creator()
{
	return new rageMayaGeoEditServerCmd();
}

//-----------------------------------------------------------------------------

MSyntax	rageMayaGeoEditServerCmd::newSyntax()
{
	MSyntax syntax;

	syntax.enableQuery();

	//No Args
	syntax.addFlag(kIsEditorConnected, kIsEditorConnectedLong);

	//{0} - Incoming cmd port, {1} - Response cmd port
	syntax.addFlag(kStartServer, kStartServerLong, MSyntax::kLong, MSyntax::kLong);

	//No Args
	syntax.addFlag(kStopServer, kStopServerLong);

	//{0} - Command Result
	syntax.addFlag(kSetCmdRes, kSetCmdResLong, MSyntax::kString);

	//{0} - GEO Editor Cmd String
	syntax.addFlag(kGeneralEditorCmdSync, kGeneralEditorCmdSyncLong, MSyntax::kString);

	//{0} - GEO Editor Cmd String, {1} - Success Callback, {2} - Failure Callback
	syntax.addFlag(kGeneralEditorCmdASync, kGeneralEditorCmdASyncLong, MSyntax::kString, MSyntax::kString, MSyntax::kString);

	return syntax;
}

//-----------------------------------------------------------------------------

rageMayaGeoEditServerCmd::rageMayaGeoEditServerCmd()
{

}

//-----------------------------------------------------------------------------

rageMayaGeoEditServerCmd::~rageMayaGeoEditServerCmd()
{

}

//-----------------------------------------------------------------------------

MStatus rageMayaGeoEditServerCmd::doIt( const MArgList &args)
{
	MStatus status;
	MArgDatabase argData(syntax(), args);

	if(argData.isFlagSet(kSetCmdRes, &status))
	{
		MString cmdRes;
		argData.getFlagArgument(kSetCmdRes, 0, cmdRes);
		GEOEDITSVR.SetGeoMELCommandResult(cmdRes);
	}
	else if(argData.isFlagSet(kStartServer, &status))
	{
		unsigned int cmdPort, responsePort;

		status = argData.getFlagArgument(kStartServer, 0, cmdPort);
		if(status != MS::kSuccess)
		{
			MGlobal::displayError("Invalid arguments, failed to retreive command port value.");
			return status;
		}

		status = argData.getFlagArgument(kStartServer, 1, responsePort);
		if(status != MS::kSuccess)
		{
			MGlobal::displayError("Invalid arguments, failed to retreive response port value.");
			return status;
		}
		GEOEDITSVR.Start(cmdPort, responsePort);
	}
	else if(argData.isFlagSet(kStopServer, &status))
	{
		GEOEDITSVR.Stop();
	}
	else if(argData.isFlagSet(kIsEditorConnected, &status))
	{
		if(argData.isQuery())
		{
			setResult(GEOEDITSVR.IsEditorConnected());
		}
	}
	else if(argData.isFlagSet(kGeneralEditorCmdSync, &status))
	{
		MString	geoEditCmdStr;

		if(!GEOEDITSVR.IsEditorConnected())
		{
			MGlobal::displayError("Cannot issue synchronous command, GEO Editor is not connected.");
			return MS::kFailure;
		}

		argData.getFlagArgument(kGeneralEditorCmdSync, 0, geoEditCmdStr);

		GeoEditGeneralEditorCommand gCmd;
		gCmd.SetCommandId(GEOEDITSVR.GetNewCmdId());
		gCmd.SetGeoEditorCmdStr(geoEditCmdStr.asChar());

		GEOEDITSVR.SendCommandSync(&gCmd);
	}
	else if(argData.isFlagSet(kGeneralEditorCmdASync, &status))
	{
		MString	geoEditCmdStr;
		MString successCbkStr;
		MString failCbkStr;

		if(!GEOEDITSVR.IsEditorConnected())
		{
			MGlobal::displayError("Cannot issue asynchronous command, GEO Editor is not connected.");
			return MS::kFailure;
		}

		argData.getFlagArgument(kGeneralEditorCmdASync, 0, geoEditCmdStr);
		argData.getFlagArgument(kGeneralEditorCmdASync, 1, successCbkStr);
		argData.getFlagArgument(kGeneralEditorCmdASync, 2, failCbkStr);

		GeoEditGeneralEditorCommand gCmd;
		gCmd.SetCommandId(GEOEDITSVR.GetNewCmdId());
		gCmd.SetGeoEditorCmdStr(geoEditCmdStr.asChar());
		
		GEOEDITSVR.SendCommandAsync(&gCmd, successCbkStr, failCbkStr);
	}

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

rageMayaGeoEditServer::rageMayaGeoEditServer()
	: m_StopListener(false)
	, m_ResetListener(false)
	, m_StopDispatcher(false)
	, m_CmdSocketHandle(fiHandleInvalid)
	, m_CmdSocketListenerHandle(fiHandleInvalid)
	, m_listenerThreadId(0)
	, m_dispatcherThreadId(0)
	, m_CmdIdCounter(0)
{
	m_hCmdQueueMutex = CreateMutex(NULL, FALSE, "CommandQueueMutex");
	m_hCmdExecFinishedEvent = CreateEvent(NULL, FALSE, FALSE, "ExecFinishedEvent");
	m_hCmdIdMutex = CreateMutex(NULL, FALSE, "CmdIdMutex");
	
	InitializeCriticalSection(&m_csResponseSocketAccess);
}

//-----------------------------------------------------------------------------

rageMayaGeoEditServer::~rageMayaGeoEditServer()
{
	if(m_CommandQueue.GetCount())
	{
		while(!m_CommandQueue.IsEmpty())
		{
			GeoEditCommand* pCmd = m_CommandQueue.Pop();
			delete pCmd;
		}
	}

	DeleteCriticalSection(&m_csResponseSocketAccess);
}

//-----------------------------------------------------------------------------

unsigned int rageMayaGeoEditServer::GetNewCmdId()
{
	unsigned int newId = 0;

	DWORD wRes = WaitForSingleObject( m_hCmdIdMutex, 200);
	if(wRes == WAIT_OBJECT_0)
	{
		newId = m_CmdIdCounter++;
		ReleaseMutex(m_hCmdIdMutex);
	}
	else if(wRes == WAIT_TIMEOUT)
	{
		MGlobal::displayError("MayaGEOEditServer -> Error, Timeout while waiting for command id counter mutex.");
	}

	return newId;
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::DispatchCmdThreadFn(void* /*data*/)
{
	GeoEditCommand* pCmd = NULL;
	
	while(1)
	{	
		//Attempt to retreive a command off the command queue.
		DWORD wRes = WaitForSingleObject(GEOEDITSVR.m_hCmdQueueMutex, 200);
		if(wRes == WAIT_OBJECT_0)
		{
			if(!GEOEDITSVR.m_CommandQueue.IsEmpty())
			{
				pCmd = GEOEDITSVR.m_CommandQueue.Pop();
			}
			ReleaseMutex( GEOEDITSVR.m_hCmdQueueMutex );
		}
		else if(wRes == WAIT_TIMEOUT)
		{
			MGlobal::displayError("MayaGEOEditServer -> Error, rageMayaGeoEditServer::DispatchCmdThreadFn timeod out while waiting for command queue mutex.");
		}

		//If there was a waiting command execute it.
		if(pCmd)
		{
			//First, call any command specific handlers
			unsigned int typeId = pCmd->GetTypeId();
			switch(typeId)
			{
			case kGeoEditCmdHandshake_TypeId:
				GEOEDITSVR.HandleHandshakeCommand(pCmd);
				break;
			case kGeoEditCmdConnectParam_TypeId:
				GEOEDITSVR.HandleConnectParamaterCommand(pCmd);
				break;
			case kGeoEditCmdDisconnectParam_TypeId:
				GEOEDITSVR.HandleDisconnectParameterCommand(pCmd);
				break;
			case kGeoEditCmdCreateTypeNode_TypeId:
				GEOEDITSVR.HandleCreateTypeCommand(pCmd);
				break;
			case kGeoEditCmdIsParameterConnected_TypeId:
				GEOEDITSVR.HandleIsParameterConnectedCommand(pCmd);
				break;
			case kGeoEditCmdSelectNodes_TypeId:
				GEOEDITSVR.HandleSelectNodesCommand(pCmd);
				break;
			case kGeoEditCmdGetDCCFilePath_TypeId:
				GEOEDITSVR.HandleGetDCCFilePathCommand(pCmd);
				break;
			case kGeoEditCmdGeneralEditorCmdResult_TypeId:
				GEOEDITSVR.HandleGeneralEditorCmdResult(pCmd);
				break;
			default:
				break;
			}

			delete pCmd;
			pCmd = NULL;
		}
		
		if(GEOEDITSVR.m_StopDispatcher)
		{
			GEOEDITSVR.m_StopDispatcher = false;
			MGlobal::displayInfo("MayaGEOEditServer -> Stopping dispatcher.");
			break;
		}

		Sleep(DISPATCHER_SLEEP_TIME);
	}
	_endthread();
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::CommandListenerThreadFn(void* data)
{
	char msg[256];
	cmdListenerThreadData* pCmdThreadData = static_cast<cmdListenerThreadData*>(data);

	GEOEDITSVR.m_CmdSocketListenerHandle = fiDeviceTcpIp::Listen(pCmdThreadData->cmdPort,1);
	if(GEOEDITSVR.m_CmdSocketListenerHandle == fiHandleInvalid)
	{
		sprintf(msg,"MayaGEOEditServer -> Error, fiDeviceTcpIp::Listen failed on port %d", pCmdThreadData->cmdPort);
		MGlobal::displayError(msg);
		return;
	}

	while (1) 
	{
		sprintf(msg,"MayaGEOEditServer -> Started listening for connection on port %d", pCmdThreadData->cmdPort);
		MGlobal::displayInfo(msg);

		GEOEDITSVR.m_CmdSocketHandle = fiDeviceTcpIp::Pickup(GEOEDITSVR.m_CmdSocketListenerHandle);
		if(GEOEDITSVR.m_CmdSocketHandle != fiHandleInvalid)
		{
			char buffer[8192];
			char memFileName[256];

			//Connect back to the client for sending response messages
			sprintf(msg, "MayaGEOEditServer -> Attempting to establish response connection on port %d", pCmdThreadData->responsePort);
			MGlobal::displayInfo(msg);

			GEOEDITSVR.m_ResponseSocketHandle  = fiDeviceTcpIp::Connect(LOCALHOST, pCmdThreadData->responsePort);
			if(GEOEDITSVR.m_ResponseSocketHandle != fiHandleInvalid)
			{
				MGlobal::displayInfo("MayaGEOEditServer -> Response connection successfully established.");
			}
			else
			{
				//Failed to establish a response connection, so reset and wait for a new connection..
				MGlobal::displayInfo("MayaGEOEditServer -> Failed to establish response connection.");
				GEOEDITSVR.m_ResetListener = true;
			}

			//A connection has been established, so loop checking for any data waiting to be read on the socket
			while(1)
			{
				unsigned int readCount = fiDeviceTcpIp::GetReadCount(GEOEDITSVR.m_CmdSocketHandle);
				if(readCount)
				{
					//Data is waiting to be read...
					recGeoEditCmd *geoCmd = (recGeoEditCmd*)buffer;
					if(fiDeviceTcpIp::GetInstance().SafeRead(GEOEDITSVR.m_CmdSocketHandle,buffer,sizeof(recGeoEditCmd)))
					{
						Assert( readCount == (sizeof(recGeoEditCmd) + geoCmd->cmdLength) );
						if (geoCmd->cmdLength)
						{
							//Read the command XML
							fiDeviceTcpIp::GetInstance().SafeRead(GEOEDITSVR.m_CmdSocketHandle,(geoCmd + 1),geoCmd->cmdLength);
							buffer[sizeof(recGeoEditCmd) + geoCmd->cmdLength] = 0x00;

							//Parse the command into a parTree and add it to the command queue
							fiDevice::MakeMemoryFileName(memFileName, 256, &buffer[sizeof(recGeoEditCmd)], geoCmd->cmdLength, false, "GeoCommandBuffer");
							fiStream *pCmdMemFile = ASSET.Open(memFileName, "");
							Assert(pCmdMemFile);

							//Convert from XML into a command object
							GeoEditCommand* pGeoCommand;
							if(PARSER.LoadObjectPtr(pCmdMemFile, pGeoCommand))
							{
								//Make sure the dispatcher isn't accessing the command queue
								DWORD wRes = WaitForSingleObject( GEOEDITSVR.m_hCmdQueueMutex, 200);
								if(wRes == WAIT_OBJECT_0)
								{
									//Push the command onto the queue of commands for the dispatcher to execute
									GEOEDITSVR.m_CommandQueue.Push(pGeoCommand);
									ReleaseMutex( GEOEDITSVR.m_hCmdQueueMutex );
								}
								else if(wRes == WAIT_TIMEOUT)
								{
									//Something went wrong and the dispatcher never let go of the command queue...
									MGlobal::displayError("MayaGEOEditServer -> Error, rageMayaGeoEditServer::CommandListenerThreadFn timed out waiting for command queue mutex, command dropped.");
									delete pGeoCommand;
								}
							}
							else
							{
								MGlobal::displayError("MayaGEOEditServer -> Error, failed to parse incoming command, command buffer dumped to the script window");
								MGlobal::displayInfo((const char*)(buffer+sizeof(recGeoEditCmd)));	
							}
							pCmdMemFile->Close();						
						}
					}
				}//End if(readCount)

				//Check to see if the GeoEditor is still actually connected...
				char ckBuffer[1] = "";
				int res = ::recv((SOCKET)GEOEDITSVR.m_CmdSocketHandle, ckBuffer, 1, MSG_PEEK);
				if( res == 0)
				{
					//The GeoEditor disconnected (or crashed), so reset the listener and wait for a new connection
					MGlobal::displayInfo("MayaGEOEditServer -> Client disconnected.");
					GEOEDITSVR.m_ResetListener = true;
				}

				//Close the socket we are reading from, and return to waiting for a connection
				if(GEOEDITSVR.m_ResetListener)
				{
					MGlobal::displayInfo("MayaGEOEditServer -> Resetting Listener");
					fiDeviceTcpIp::GetInstance().Close(GEOEDITSVR.m_CmdSocketHandle);
					fiDeviceTcpIp::GetInstance().Close(GEOEDITSVR.m_ResponseSocketHandle);

					GEOEDITSVR.m_CmdSocketHandle = fiHandleInvalid;
					GEOEDITSVR.m_ResponseSocketHandle = fiHandleInvalid;

					GEOEDITSVR.m_ResetListener = false;
					break;
				}
					
				//Shut down the listener thread altogether
				if(GEOEDITSVR.m_StopListener)
					break;
				
				Sleep(LISTENER_SLEEP_TIME);

			}//End while(1)
		}//End if(m_CmdSocketHandle != fiHandleInvalid)

		//Shut down the listener thread altogether
		if(GEOEDITSVR.m_StopListener)
		{
			MGlobal::displayInfo("MayaGEOEditServer -> Stopping listening for GeoEditor connections");
			GEOEDITSVR.m_StopListener = false;
			break;
		}

		Sleep(LISTENER_SLEEP_TIME);
	}

	//Thread is exiting, so cleanup any active connections..
	if(GEOEDITSVR.m_CmdSocketHandle != fiHandleInvalid)
	{
		MGlobal::displayInfo("MayaGEOEditServer -> Closing GeoEditor command connection.");
		fiDeviceTcpIp::GetInstance().Close(GEOEDITSVR.m_CmdSocketHandle);
	}
	GEOEDITSVR.m_CmdSocketHandle = fiHandleInvalid;

	
	if(GEOEDITSVR.m_ResponseSocketHandle != fiHandleInvalid)
	{
		MGlobal::displayInfo("MayaGEOEditServer -> Closing response connection.");
		fiDeviceTcpIp::GetInstance().Close(GEOEDITSVR.m_ResponseSocketHandle);
	}
	GEOEDITSVR.m_ResponseSocketHandle = fiHandleInvalid;

	if(GEOEDITSVR.m_CmdSocketListenerHandle != fiHandleInvalid)
	{
		fiDeviceTcpIp::GetInstance().Close(GEOEDITSVR.m_CmdSocketListenerHandle);
	}
	GEOEDITSVR.m_CmdSocketListenerHandle = fiHandleInvalid;

	MGlobal::displayInfo("MayaGEOEditServer -> Stopped.");

	delete pCmdThreadData;

	_endthread();
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::SendCommand(const string& cmd)
{
	EnterCriticalSection(&m_csResponseSocketAccess);
	if(m_ResponseSocketHandle != fiHandleInvalid)
	{
		int resultSize = cmd.size() + 4;
		char* resultBuffer = new char[resultSize];

		//Pack the size of the command being sent into the first 4 bytes of the buffer
		*((int*)(&resultBuffer[0])) = resultSize;
		strcpy(&resultBuffer[4], cmd.c_str());

		//Send the response out..
		fiDeviceTcpIp::GetInstance().Write(m_ResponseSocketHandle, resultBuffer, resultSize );
	}
	LeaveCriticalSection(&m_csResponseSocketAccess);
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::SendCommand(GeoEditCommand* pCmd)
{
	char memFileName[256];
	char commandBuffer[1024];

	EnterCriticalSection(&m_csResponseSocketAccess);
	if(m_ResponseSocketHandle != fiHandleInvalid)
	{
		memset(commandBuffer, 0, 1024);

		//Serialize the result object
		fiDevice::MakeMemoryFileName(memFileName, 256, &commandBuffer[4], 1020, false, "GeoCommandBuffer");
		fiStream *pCmdMemFile = ASSET.Open(memFileName, "");
		Assert(pCmdMemFile);

		PARSER.SaveObject(pCmdMemFile, pCmd, parManager::XML);

		//Set the first 4 bytes the buffer to contain the length of the response message
		int cmdLength = pCmdMemFile->Tell() + 4;
		*((int*)(&commandBuffer[0])) = cmdLength;

		pCmdMemFile->Close();

		//Send the response out..
		fiDeviceTcpIp::GetInstance().Write(m_ResponseSocketHandle, commandBuffer, cmdLength );
	}
	LeaveCriticalSection(&m_csResponseSocketAccess);
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::SendCommandAsync(GeoEditCommand* pCmd, const MString& successCbkStr, const MString& failCbkStr )
{
	m_AsyncDCCCallbacks[pCmd->GetCommandId()].successCbkStr = successCbkStr.asChar();
	m_AsyncDCCCallbacks[pCmd->GetCommandId()].failCbkStr = failCbkStr.asChar();
	SendCommand(pCmd);
	
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::SendCommandSync(GeoEditCommand* pCmd)
{
	HANDLE hCmdSyncEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	//Add the event to the synchronous event map, so it can be signaled when the response message comes back
	m_SyncCmdEventMap[pCmd->GetCommandId()] = hCmdSyncEvent;

	SendCommand(pCmd);

	//Wait for the result message handler to signal us that the command has completed executing...
	DWORD dwRes = WaitForSingleObject(hCmdSyncEvent, SYNC_CMD_TIMEOUT);
	
	if(dwRes == WAIT_TIMEOUT)
	{
		MGlobal::displayError("MayaGEOEditServer -> Error, rageMayaGeoEditServer::SendCommandSync timed out waiting for command result.");
	}

	CloseHandle(hCmdSyncEvent);
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::SetGeoMELCommandResult(const MString& result)
{
	//This method gets called through the MEL commands "rageMayaGeoEditServer -cr $cmdRes" that are added to the end of any MEL that needs to get executed
	//to perform an operation for the GEO Editor.  
	
	//Store the result string of the command and signal the dispatcher thread that we are done executing the command.
	m_geoMelCmdResult = result;
	SetEvent(m_hCmdExecFinishedEvent);
}

//-----------------------------------------------------------------------------

bool rageMayaGeoEditServer::GetDCCResultFromString(const string& dccResultStr, GeoEditDCCResult** pDCCResultObj)
{
	char memFileName[256];
	fiDevice::MakeMemoryFileName(memFileName, 256, dccResultStr.c_str(), dccResultStr.size(), false, "DCCResultBuffer");
	fiStream *pCmdMemFile = ASSET.Open(memFileName, "");
	Assert(pCmdMemFile);

	//Convert from XML into a command object
	bool bRes = PARSER.LoadObjectPtr(pCmdMemFile, *pDCCResultObj);
	pCmdMemFile->Close();
	
	return bRes;
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::HandleGeneralEditorCmdResult(GeoEditCommand* pCmd)
{
	GeoEditGeneralEditorCommandResult *pCmdResult = dynamic_cast<GeoEditGeneralEditorCommandResult*>(pCmd);
	if(!pCmdResult)
	{
		MGlobal::displayError("MayaGeoEditServer -> Error, rageMayaGeoEditServer::HandleGeneralEditorCmdResult received command that isn't a general editor command result");
		return;
	}

	int srcCmdId = pCmdResult->GetSrcCmdId();

	//Check the synchronous command event map to determine if there is a command blocking on this result.
	map<int,HANDLE>::iterator	syncIt = m_SyncCmdEventMap.find(srcCmdId);
	if(syncIt != m_SyncCmdEventMap.end())
	{
		HANDLE syncEvent = syncIt->second;
		SetEvent(syncEvent);
		m_SyncCmdEventMap.erase(syncIt);
	}

	//Check the asynchronous command event map to determine if there are any scripts callbacks that need to get
	//executed based on the result.
	map<int,aSyncDCCCallback>::iterator asyncIt = m_AsyncDCCCallbacks.find(srcCmdId);
	if(asyncIt != m_AsyncDCCCallbacks.end())
	{
		aSyncDCCCallback* pCbks = &asyncIt->second;
		if(pCmdResult->GetSuccess())
		{
			if(pCbks->successCbkStr.size())
			{
				MGlobal::executeCommandOnIdle(pCbks->successCbkStr.c_str(), false);
			}
		}
		else
		{
			if(pCbks->failCbkStr.size())
			{
				MGlobal::executeCommandOnIdle(pCbks->failCbkStr.c_str(), false);
			}
		}
		m_AsyncDCCCallbacks.erase(asyncIt);
	}
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::HandleHandshakeCommand(GeoEditCommand* pCmd)
{
	GeoEditCommandHandshake* pHandShakeCmd = dynamic_cast<GeoEditCommandHandshake*>(pCmd);
	if(!pHandShakeCmd)
	{
		MGlobal::displayError("MayaGEOEditServer -> Error, rageMayaGeoEditServer::HandleHandshakeCommand received command that isn't a handshake command");
		return;
	}

	GeoEditCommandHandshakeReply cmdHndReply;
	cmdHndReply.SetCommandId((unsigned int)GetNewCmdId());
	cmdHndReply.SetSrcCmdId(pCmd->GetCommandId());
	cmdHndReply.SetAppName("Maya");

	SendCommand(&cmdHndReply);
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::HandleCreateTypeCommand(GeoEditCommand* pCmd)
{
	GeoEditCommandCreateTypeNode* pCreateTypeNodeCmd = dynamic_cast<GeoEditCommandCreateTypeNode*>(pCmd);
	if(!pCreateTypeNodeCmd)
	{
		MGlobal::displayError("MayaGEOEditServer -> Error, rageMayaGeoEditServer::HandleCreateTypeCommand received command that isn't a create type node command");
		return;
	}

	ResetEvent(m_hCmdExecFinishedEvent);

	char cmdBuf[256];
	sprintf(cmdBuf, "string $cmdRes = rageCreateGeoTypeNodeOfType(\"%s\"); rageMayaGeoEditServer -cr $cmdRes;",
		pCreateTypeNodeCmd->GetGeoType().c_str());
	MGlobal::executeCommandOnIdle(cmdBuf, true);

	//Block until Maya has finished executing the command
	DWORD wRes = WaitForSingleObject(m_hCmdExecFinishedEvent, WAIT_CMD_EXEC_TIME);
	if(wRes == WAIT_TIMEOUT)
	{
		//Something went wrong and the command never finished...
		MGlobal::displayError("MayaGEOEditServer -> rageMayaGeoEditServer::HandleCreateTypeCommand timed out waiting for create type command to complete.");
		m_geoMelCmdResult = "<TIMEOUT>";
		GeoEditCommandCreateTypeNodeResult cmdResult(GetNewCmdId(), false, pCreateTypeNodeCmd->GetCommandId());
		SendCommand(&cmdResult);
	}
	else
	{
		//Get the result of the command
		string responseMsg = m_geoMelCmdResult.asChar();

		GeoEditDCCResult *pResult;
		bool bRes = GetDCCResultFromString(responseMsg, &pResult);
		if(bRes)
		{
			//Convert the DCC Result into a GeoEditor Result object
			GeoEditCommandCreateTypeNodeResult cmdResult(GetNewCmdId(), pResult->GetSuccess(), pCreateTypeNodeCmd->GetCommandId());
			cmdResult.SetTypeNodeDCCAppFilePath(pResult->GetDCCResult());
			SendCommand(&cmdResult);
		}
		else
		{
			//Something is wrong with the response message, so send a failed command response
			MGlobal::displayError("MayaGeoEditServer -> rageMayaGeoEditServer::HandleCreateTypeCommand, malformed DCC response.");
			GeoEditCommandCreateTypeNodeResult cmdResult(GetNewCmdId(), false, pCreateTypeNodeCmd->GetCommandId());
			SendCommand(&cmdResult);
		}
		delete pResult;
	}
}	

//-----------------------------------------------------------------------------

string TrimLeadTrailWhiteSpace(const string& in)
{
	size_t st = in.find_first_not_of("\t\r\n");
	size_t ed = in.find_last_not_of("\t\r\n");
	if(st != string::npos)
		return in.substr(st, ed-st+1);
	else
		return string();
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::HandleConnectParamaterCommand(GeoEditCommand* pCmd)
{
	GeoEditCommandConnectParameter* pConnectParamCmd = dynamic_cast<GeoEditCommandConnectParameter*>(pCmd);
	if(!pConnectParamCmd)
	{
		MGlobal::displayError("MayaGEOEditServer -> rageMayaGeoEditServer::HandleConnectParamaterCommand received command that isn't a connect parameter command");
		return;
	}

	ResetEvent(m_hCmdExecFinishedEvent);

		char cmdBuf[256];

		//MEL Signature
		//ConnectToParameter(string $strTypeName, string $strParameterName, string $strParameterExportType, string $strParameterValidationType)
		sprintf(cmdBuf, "string $cmdRes = rageGeoType.ConnectToParameter(\"%s\",\"%s\",\"%s\",\"%s\"); rageMayaGeoEditServer -cr $cmdRes;",
			pConnectParamCmd->GetGeoType().c_str(),
			pConnectParamCmd->GetParameterName().c_str(),
			pConnectParamCmd->GetExportType().c_str(),
			pConnectParamCmd->GetConnectValidator().c_str());
		MGlobal::executeCommandOnIdle(cmdBuf, true);

	DWORD wRes = WaitForSingleObject(m_hCmdExecFinishedEvent, WAIT_CMD_EXEC_TIME);
	if(wRes == WAIT_TIMEOUT)
	{
		MGlobal::displayError("MayaGEOEditServer -> rageMayaGeoEditServer::HandleConnectParamaterCommand timed out waiting for connect parameter command to complete.");
		m_geoMelCmdResult = "<TIMEOUT>";
		GeoEditCommandConnectParameterResult cmdResult(GetNewCmdId(),false, pConnectParamCmd->GetCommandId());
		SendCommand(&cmdResult);
	}
	else
	{
		//Get the results of the command
		string responseMsg = m_geoMelCmdResult.asChar();

		GeoEditDCCResult *pResult;
		bool bRes = GetDCCResultFromString(responseMsg, &pResult);
		if(bRes)
		{
			//Convert the DCC Result into a GeoEditor Result object
			GeoEditCommandConnectParameterResult cmdResult(GetNewCmdId(), pResult->GetSuccess(), pConnectParamCmd->GetCommandId());

			//Parse out the names of the nodes that were connected
			string nodeNames = pResult->GetDCCResult();
			if(nodeNames.length() != 0)
			{
				unsigned int st = 0;
				unsigned int ed = 0;
				
				while(st < (int)nodeNames.length())
				{
					ed = nodeNames.find_first_of(",",st);
					if(ed==st)
					{
						break;
					}
					else if(ed==string::npos)
					{
						string nodeName = TrimLeadTrailWhiteSpace(nodeNames);
						cmdResult.AppendNodeName(nodeName);
						break;
					}
					else
					{
						string nodeName = TrimLeadTrailWhiteSpace(nodeNames.substr(st, (ed-st)-1));
						cmdResult.AppendNodeName(nodeName);
						st = ed+1;
					}
				}
			}

			SendCommand(&cmdResult);
		}
		else
		{
			//Something is wrong with the response message, so send a failed command response
			MGlobal::displayError("MayaGeoEditServer -> rageMayaGeoEditServer::HandleConnectParamaterCommand, malformed DCC response.");
			GeoEditCommandConnectParameterResult cmdResult(GetNewCmdId(), false, pConnectParamCmd->GetCommandId());
			SendCommand(&cmdResult);
		}
		delete pResult;
	}
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::HandleDisconnectParameterCommand(GeoEditCommand* pCmd)
{
	GeoEditCommandDisconnectParameter* pDisconnectParamCmd = dynamic_cast<GeoEditCommandDisconnectParameter*>(pCmd);
	if(!pDisconnectParamCmd)
	{
		MGlobal::displayError("rageMayaGeoEditServer::HandleDisconnectParameterCommand -> Received command that isn't a disconnect parameter command");
		return;
	}

	ResetEvent(m_hCmdExecFinishedEvent);

		char cmdBuf[256];
		sprintf(cmdBuf, "string $cmdRes = rageGeoType.DisconnectParameter(\"%s\",\"%s\"); rageMayaGeoEditServer -cr $cmdRes;",
			pDisconnectParamCmd->GetGeoType().c_str(),
			pDisconnectParamCmd->GetParameterName().c_str());
		MGlobal::executeCommandOnIdle(cmdBuf, true);

	DWORD wRes = WaitForSingleObject(m_hCmdExecFinishedEvent, WAIT_CMD_EXEC_TIME);
	if(wRes == WAIT_TIMEOUT)
	{
		MGlobal::displayError("rageMayaGeoEditServer::HandleDisconnectParameterCommand -> Timed out waiting for disconnect parameter command to complete.");
		m_geoMelCmdResult = "<TIMEOUT>";
		GeoEditCommandDisconnectParameterResult cmdResult(GetNewCmdId(),false, pDisconnectParamCmd->GetCommandId());
		SendCommand(&cmdResult);
	}
	else
	{
		//Get the command response
		string responseMsg = m_geoMelCmdResult.asChar();

		GeoEditDCCResult *pResult;
		bool bRes = GetDCCResultFromString(responseMsg, &pResult);
		if(bRes)
		{
			//Convert the DCC Result into a GeoEditor Result object
			GeoEditCommandDisconnectParameterResult cmdResult(GetNewCmdId(), pResult->GetSuccess(), pDisconnectParamCmd->GetCommandId());
			SendCommand(&cmdResult);
		}
		else
		{
			//Something is wrong with the response message, so send a failed command response
			MGlobal::displayError("MayaGeoEditServer -> rageMayaGeoEditServer::HandleDisconnectParameterCommand, malformed DCC response.");
			GeoEditCommandDisconnectParameterResult cmdResult(GetNewCmdId(), false, pDisconnectParamCmd->GetCommandId());
			SendCommand(&cmdResult);
		}
		delete pResult;
	}
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::HandleIsParameterConnectedCommand(GeoEditCommand* pCmd)
{
	GeoEditCommandIsParameterConnected* pIsParamConnectedCmd = dynamic_cast<GeoEditCommandIsParameterConnected*>(pCmd);
	if(!pIsParamConnectedCmd)
	{
		MGlobal::displayError("rageMayaGeoEditServer::HandleIsParameterConnectedCommand -> Received command that isn't an IsParameterConnected command");
		return;
	}
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::HandleGetDCCFilePathCommand(GeoEditCommand* pCmd)
{
	GeoEditCommandGetDCCFilePath* pGetDCCFilePathCmd = dynamic_cast<GeoEditCommandGetDCCFilePath*>(pCmd);
	if(!pGetDCCFilePathCmd)
	{
		MGlobal::displayError("rageMayaGeoEditServer::HandleGetDCCFilePathCommand -> Received command that isn't a GetDCCFilePath command");
		return;
	}

	ResetEvent(m_hCmdExecFinishedEvent);

		char cmdBuf[256];
		sprintf(cmdBuf, "string $cmdRes = rageGeo.GetMayaFileName(); rageMayaGeoEditServer -cr $cmdRes;");
		MGlobal::executeCommandOnIdle(cmdBuf, true);

	DWORD wRes = WaitForSingleObject(m_hCmdExecFinishedEvent, WAIT_CMD_EXEC_TIME);
	if(wRes == WAIT_TIMEOUT)
	{
		MGlobal::displayError("rageMayaGeoEditServer::HandleGetDCCFilePathCommand -> Timed out waiting for GetDCCFilePath command to complete.");
		m_geoMelCmdResult = "<TIMEOUT>";
		GeoEditCommandGetDCCFilePathResult cmdResult(GetNewCmdId(),false, pGetDCCFilePathCmd->GetCommandId());
		SendCommand(&cmdResult);
	}
	else
	{
		//Get the command response
		string responseMsg = m_geoMelCmdResult.asChar();

		GeoEditDCCResult *pResult;
		bool bRes = GetDCCResultFromString(responseMsg, &pResult);
		if(bRes)
		{
			//Convert the DCC Result into a GeoEditor Result object
			GeoEditCommandGetDCCFilePathResult cmdResult(GetNewCmdId(), pResult->GetSuccess(), pGetDCCFilePathCmd->GetCommandId());
			cmdResult.SetDCCFilePath(pResult->GetDCCResult());
			SendCommand(&cmdResult);
		}
		else
		{
			//Something is wrong with the response message, so send a failed command response
			MGlobal::displayError("MayaGeoEditServer -> rageMayaGeoEditServer::HandleGetDCCFilePathCommand, malformed DCC response.");
			GeoEditCommandGetDCCFilePathResult cmdResult(GetNewCmdId(), false, pGetDCCFilePathCmd->GetCommandId());
			SendCommand(&cmdResult);
		}
		delete pResult;
	}
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::HandleSyncToGeoEditorCommand(GeoEditCommand* /*pCmd*/)
{
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::HandleSelectNodesCommand(GeoEditCommand* pCmd)
{
	GeoEditCommandSelectNodes* pSelectNodesCmd = dynamic_cast<GeoEditCommandSelectNodes*>(pCmd);
	if(!pSelectNodesCmd)
	{
		MGlobal::displayError("rageMayaGeoEditServer::HandleSelectNodesCommand -> Received command that isn't a select nodes command");
		return;
	}

	ResetEvent(m_hCmdExecFinishedEvent);

		char cmdBuf[256];
		sprintf(cmdBuf, "string $cmdRes = rageGeoType.SelectNodes(\"%s\",\"%s\"); rageMayaGeoEditServer -cr $cmdRes;",
			pSelectNodesCmd->GetGeoType().c_str(),
			pSelectNodesCmd->GetParameterName().c_str());
		MGlobal::executeCommandOnIdle(cmdBuf, true);

	//Block until Maya has finished executing the command
	DWORD wRes = WaitForSingleObject(m_hCmdExecFinishedEvent, WAIT_CMD_EXEC_TIME);
	if(wRes == WAIT_TIMEOUT)
	{
		MGlobal::displayError("rageMayaGeoEditServer::HandleSelectNodesCommand -> Timed out waiting for select nodes command to complete.");
		m_geoMelCmdResult = "<TIMEOUT>";
		GeoEditCommandSelectNodesResult cmdResult(GetNewCmdId(),false, pSelectNodesCmd->GetCommandId());
		SendCommand(&cmdResult);
	}
	else
	{
		//Get the command response
		string responseMsg = m_geoMelCmdResult.asChar();

		GeoEditDCCResult *pResult;
		bool bRes = GetDCCResultFromString(responseMsg, &pResult);
		if(bRes)
		{
			//Convert the DCC Result into a GeoEditor Result object
			GeoEditCommandSelectNodesResult cmdResult(GetNewCmdId(), pResult->GetSuccess(), pSelectNodesCmd->GetCommandId());
			SendCommand(&cmdResult);
		}
		else
		{
			//Something is wrong with the response message, so send a failed command response
			MGlobal::displayError("MayaGeoEditServer -> rageMayaGeoEditServer::HandleSelectNodesCommand, malformed DCC response.");
			GeoEditCommandSelectNodesResult cmdResult(GetNewCmdId(), false, pSelectNodesCmd->GetCommandId());
			SendCommand(&cmdResult);
		}
		delete pResult;
	}
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::HandleRenameTypeNodeCommand(GeoEditCommand* pCmd)
{
	GeoEditCommandRenameTypeNode* pRenameTypeNodeCmd = dynamic_cast<GeoEditCommandRenameTypeNode*>(pCmd);
	if(!pRenameTypeNodeCmd)
	{
		MGlobal::displayError("rageMayaGeoEditServer::HandleRenameTypeNodeCommand -> Received command that isn't a Rename Type Node command");
		return;
	}

	ResetEvent(m_hCmdExecFinishedEvent);

		char cmdBuf[256];
		sprintf(cmdBuf, "string $cmdRes = rageGeoType.RenameGEOType(\"%s\",\"%s\"); rageMayaGeoEditServer -cr $cmdRes;",
			pRenameTypeNodeCmd->GetOldTypeName().c_str(),
			pRenameTypeNodeCmd->GetNewTypeName().c_str());
		MGlobal::executeCommandOnIdle(cmdBuf, true);

	DWORD wRes = WaitForSingleObject(m_hCmdExecFinishedEvent, WAIT_CMD_EXEC_TIME);
	if(wRes == WAIT_TIMEOUT)
	{
		MGlobal::displayError("rageMayaGeoEditServer::HandleRenameTypeNodeCommand -> Timed out waiting for Rename Type Node command to complete.");
		m_geoMelCmdResult = "<TIMEOUT>";
		GeoEditCommandRenameTypeNodeResult cmdResult(GetNewCmdId(),false, pRenameTypeNodeCmd->GetCommandId());
		SendCommand(&cmdResult);
	}
	else
	{
		//Get the command response
		string responseMsg = m_geoMelCmdResult.asChar();

		GeoEditDCCResult *pResult;
		bool bRes = GetDCCResultFromString(responseMsg, &pResult);
		if(bRes)
		{
			//Convert the DCC Result into a GeoEditor Result object
			GeoEditCommandRenameTypeNodeResult cmdResult(GetNewCmdId(), pResult->GetSuccess(), pRenameTypeNodeCmd->GetCommandId());
			SendCommand(&cmdResult);
		}
		else
		{
			//Something is wrong with the response message, so send a failed command response
			MGlobal::displayError("MayaGeoEditServer -> rageMayaGeoEditServer::HandleRenameTypeNodeCommand, malformed DCC response.");
			GeoEditCommandRenameTypeNodeResult cmdResult(GetNewCmdId(), false, pRenameTypeNodeCmd->GetCommandId());
			SendCommand(&cmdResult);
		}
		delete pResult;
	}
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::Start(unsigned int cmdPort, unsigned int responsePort)
{
	cmdListenerThreadData* cmdThreadData = new cmdListenerThreadData();
	cmdThreadData->cmdPort = cmdPort;
	cmdThreadData->responsePort = responsePort;

	if(m_listenerThreadId == 0)
	{
		//Start the listener thread
		m_listenerThreadId = (HANDLE)_beginthread(&rageMayaGeoEditServer::CommandListenerThreadFn, 0, cmdThreadData);
	}
	else
		MGlobal::displayInfo("MayaGEOEditServer -> Listener thread already running...");

	if(m_dispatcherThreadId == 0)
	{
		//Start the dispatcher thread
		m_dispatcherThreadId = (HANDLE)_beginthread(&rageMayaGeoEditServer::DispatchCmdThreadFn, 0, NULL);
	}
	else
		MGlobal::displayInfo("MayaGEOEditServer -> Dispatcher thread already running...");
}

//-----------------------------------------------------------------------------

void rageMayaGeoEditServer::Stop()
{
	DWORD dwResult;

	//Signal the command listener thread to stop, and wait.  If it doesn't signal within the specified time manually kill the thread
	//since its likely blocking on the fiDeviceTcpIp::Pickup call.
	m_StopListener = true;
	dwResult = WaitForSingleObject(m_listenerThreadId, 200);
	if(dwResult == WAIT_TIMEOUT)
	{
		//The thread didn't exit when a stop was requested, so kill it..
		MGlobal::displayInfo("MayaGEOEditServer -> Manually killing listener thread...");
		TerminateThread(m_listenerThreadId,0);

		//Close down any of the socket handles that may have been left open by terminating the thread
		if(m_CmdSocketHandle != fiHandleInvalid)
		{
			MGlobal::displayInfo("MayaGEOEditServer -> Closing command socket handle.");
			fiDeviceTcpIp::GetInstance().Close(m_CmdSocketHandle);
		}
		m_CmdSocketHandle = fiHandleInvalid;
		
		if(m_ResponseSocketHandle != fiHandleInvalid)
		{
			MGlobal::displayInfo("MayaGEOEditServer -> Closing response socket handle.");
			fiDeviceTcpIp::GetInstance().Close(m_ResponseSocketHandle);
		}
		m_ResponseSocketHandle = fiHandleInvalid;

		if(m_CmdSocketListenerHandle != fiHandleInvalid)
		{
			MGlobal::displayInfo("MayaGEOEditServer -> Closing listener.");
			fiDeviceTcpIp::GetInstance().Close(m_CmdSocketListenerHandle);
		}
		m_CmdSocketListenerHandle = fiHandleInvalid;

		m_StopListener = false;

		MGlobal::displayInfo("MayaGEOEditServer -> Stopped.");
	}
	m_listenerThreadId = (HANDLE)0;

	m_StopDispatcher = true;
	dwResult = WaitForSingleObject(m_dispatcherThreadId, 200);
	if(dwResult == WAIT_TIMEOUT)
	{
		//The thread didn't exit when a stop was requested, so kill it...
		MGlobal::displayInfo("MayaGEOEditServer -> Manually killing the dispatcher thread...");
		TerminateThread(m_dispatcherThreadId, 200);
		MGlobal::displayInfo("MayaGEOEditServer -> Stopped dispatcher");
		
		m_StopDispatcher = false;
	}
	m_dispatcherThreadId = (HANDLE)0;
}

//-----------------------------------------------------------------------------
