// 
// rageRst/mayacmds.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma warning(push)
#if _MSC_VER >= 1400
#pragma warning(disable: 4005) // warning C4005: 'strcasecmp' : macro redefinition
							   // rage\base\src\string\string.h declares strcasecmp, and so does MTypes.h
							   // but they are currently differenct
#endif

#include "system/xtl.h"
#include <maya/MArgList.h>

#include "mayacmds.h"

#include "rst/shaderlist.h"
#include "rst/shaderplugin.h"

#include "file/token.h"
#include "diag/output.h"
#include "file/device.h"
#include "file/stream.h"
#include "grcore/device.h"
#include "grcore/texture.h"
#include "grmodel/shader.h"
#include "grmodel/shadervar.h"

using namespace rage;

// **********************************************
//  COMMAND BASE -- Handles thread safety issues
// **********************************************
rageRstCommand::rageRstCommand() : m_VerifyActive(true) {
	// EMPTY
}

MStatus rageRstCommand::doIt( const MArgList &args ) {
	if ( m_VerifyActive ) {
		if ( rageRstShaderPlugin::GetInstance()->GetRageLoop()->IsWindowActive() == false ) {
			Errorf( "RAGE not properly initialized -- need to call rageRstActivate" );
			return MS::kFailure;
		}
	}
	rageRstRageLoopCriticalSection cs;
	clearResult();
	return doItNow( args );
}

void rageRstCommand::MakeNameMayaFriendly(MString &str) {
	// I'm violating all kinds of rules here, but MStrings are pretty weak in terms of 
	//	string manipulations...
	char *buff = const_cast<char *>(str.asChar());
	while ( *buff != '\0' ) {
		bool isLegal = (*buff >= '0' && *buff <= '9') || (*buff >= 'A' && *buff <= 'Z') || (*buff >= 'a' && *buff <= 'z');
		if ( isLegal == false )
			*buff = '_';
		++buff;
	}
}

// ************************************
//  SANDBOX -- Experimentation command
// ************************************
rageRstSandbox::rageRstSandbox() {
	m_VerifyActive = false;
}

MStatus rageRstSandbox::doItNow( const MArgList &/*args*/ ) {
	return MS::kSuccess;
}

void *rageRstSandbox::creator() {
	return new rageRstSandbox;
}
// --------------

// ************************************
//  Activate
//		PARAMS: 
//			bool - Enable/Disable
//			string - shader path
//			string - shader database path
//			string - parent window title
//			string - target window title
// ************************************
bool rageRstActivate::sm_HiddenWindow = false;

rageRstActivate::rageRstActivate() {
	m_VerifyActive = false;
}

MStatus rageRstActivate::doItNow( const MArgList &args ) {
	bool activate = args.asBool(0);
	int result = true;

	rageRstShaderPlugin::GetInstance()->GetShaderList()->SetShaderPath(args.asString(1).asChar());
	rageRstShaderPlugin::GetInstance()->SetShaderDbPath(args.asString(2).asChar());

	if ( activate ) {
		bool wantHidden = (args.asString(3).length() == 0 && args.asString(4).length() == 0);
		result = false;
		HWND tgt = 0;
		if ( wantHidden ) {
			tgt = (HWND) rageRstShaderPlugin::GetInstance()->GetHiddenWindow();
		}
		HWND parent = FindWindow(0, args.asString(3).asChar());
		if ( !wantHidden && parent ) {
			tgt = FindWindowEx( parent, 0, 0, args.asString(3).asChar() );
			if ( !tgt )
				tgt = FindWindow(0, args.asString(4).asChar());
		}
		if ( tgt ) {
			result = true;
			rageRstShaderPlugin::GetInstance()->GetRageLoop()->EnableRender(!wantHidden);
			rageRstShaderPlugin::GetInstance()->GetRageLoop()->EnableUpdate(true);
			rageRstShaderPlugin::GetInstance()->GetRageLoop()->AttachToWindow((u32) tgt, 0);
		}
		sm_HiddenWindow = wantHidden;
	}
	else if ( !activate && rageRstShaderPlugin::GetInstance()->GetRageLoop()->IsWindowActive() == true ) {
		rageRstShaderPlugin::GetInstance()->GetRageLoop()->EnableRender(false);
		rageRstShaderPlugin::GetInstance()->GetRageLoop()->EnableUpdate(false);
		rageRstShaderPlugin::GetInstance()->GetRageLoop()->AttachToWindow(rageRstShaderPlugin::GetInstance()->GetHiddenWindow(), 0);
	}
	setResult( result );
	return MS::kSuccess;
}

void *rageRstActivate::creator() {
	return new rageRstActivate;
}
// --------------

// *****************
//  GET VERSION
//		RETURNS: string version number "m.n.o" of plugin
// *****************
rageRstGetVersion::rageRstGetVersion() {
	m_VerifyActive = false;
}

MStatus rageRstGetVersion::doItNow( const MArgList &/*args*/ ) {
	MString result;
	result = "1.6.0";
	setResult( result );
	return MS::kSuccess;
}

void *rageRstGetVersion::creator() {
	return new rageRstGetVersion;
}
// --------------


// *****************
//  SHADER LIST
//	PARAMS:
//	RETURNS: 
//		number of shaders in that path
// *****************
MStatus rageRstGetShaderCount::doItNow( const MArgList &/*args*/ ) {
	rageRstShaderList *lst = rageRstShaderPlugin::GetInstance()->GetShaderList();
	lst->BuildShaderList();
	int count = lst->GetShaderCount();
	setResult( count );
	return MS::kSuccess;
}

void *rageRstGetShaderCount::creator() {
	return new rageRstGetShaderCount;
}
// --------------

// *****************
//  GET SHADER NAME
// *****************
MStatus rageRstGetShaderName::doItNow( const MArgList &args ) {
	rageRstShaderList *lst = rageRstShaderPlugin::GetInstance()->GetShaderList();
	int idx = args.asInt(0);
	setResult( lst->GetShaderName(idx) );
	return MS::kSuccess;
}

void *rageRstGetShaderName::creator() {
	return new rageRstGetShaderName;
}
// --------------


// **********************************
//  SHADER DB COMMAND
// **********************************
const char *rageRstShaderDbCommand::CleanPath(const char *rawPath) {
	static char path[512];
	if ( strlen(rawPath) > (sizeof(path) - 1) ) {
		Errorf("ShaderDbItemList -- Path is too large for internal buffers");
		return 0;
	}
	else {
		const char *work = rawPath;
		char *out = path;
		*out = '\0';
		do {
			if ( *work != '[' && *work != ']' ) {
				*out = *work;
				out++;
			}
		} while ( *work++ != '\0' );
		return path;
	}
}
// --------------

// **********************************
//  GET SHADER DB ITEM LIST
// **********************************
MStatus rageRstGetShaderDbItemList::doItNow( const MArgList &args ) {
	// Pull out any "[" or "]" characters
	const char *path = CleanPath(args.asString(0).asChar());
	
	rageRstShaderDb *shaderDb = rageRstShaderPlugin::GetInstance()->GetShaderDb();
	const int itemCount = shaderDb->BuildEntryList(path);
	
	MString result = "";
	for (int i = 0; i < itemCount; ++i) {
		result += "'";
		bool grp = shaderDb->IsGroup( i );
		if ( grp ) {
			result += "[";
		}
		result += shaderDb->GetEntryName( i );
		if ( grp ) {
			result += "]";
		}
		result += "' ";
	}
	setResult( result );
	return MS::kSuccess;
}

void *rageRstGetShaderDbItemList::creator() {
	return new rageRstGetShaderDbItemList;
}
// --------------

// **********************************
//  CREATE SHADER DB GROUP
// **********************************
MStatus rageRstCreateShaderDbGroup::doItNow( const MArgList &args ) {
	const char *path = CleanPath(args.asString(0).asChar());
	if ( rageRstShaderPlugin::GetInstance()->GetShaderDb()->CreateGroup(path) )
		return MS::kSuccess;
	else
		return MS::kFailure;
}

void *rageRstCreateShaderDbGroup::creator() {
	return new rageRstCreateShaderDbGroup;
}
// --------------

// **********************************
//  DELETE SHADER DB GROUP
// **********************************
MStatus rageRstDeleteShaderDbGroup::doItNow( const MArgList &args ) {
	const char *path = CleanPath(args.asString(0).asChar());
	if ( rageRstShaderPlugin::GetInstance()->GetShaderDb()->DeleteGroup(path) )
		return MS::kSuccess;
	else
		return MS::kFailure;
}

void *rageRstDeleteShaderDbGroup::creator() {
	return new rageRstDeleteShaderDbGroup;
}
// --------------

// **********************************
//  CREATE SHADER DB ITEM
//		PARAMS:
//			string - Name of database entry (+ relative path)
//			string - Base shader name to use
// **********************************
MStatus rageRstCreateShaderDbItem::doItNow( const MArgList &args ) {
	const char *name = CleanPath(args.asString(0).asChar());
	if ( rageRstShaderPlugin::GetInstance()->GetShaderDb()->CreateEntry(name, args.asString(1).asChar()) )
		return MS::kSuccess;
	else
		return MS::kFailure;
}

void *rageRstCreateShaderDbItem::creator() {
	return new rageRstCreateShaderDbItem;
}
// --------------

// **********************************
//  DELETE SHADER DB ITEM
//		PARAMS:
//			string - Name of database entry (+ relative path)
// **********************************
MStatus rageRstDeleteShaderDbItem::doItNow( const MArgList &args ) {
	const char *name = CleanPath(args.asString(0).asChar());
	if ( rageRstShaderPlugin::GetInstance()->GetShaderDb()->DeleteEntry( name ) )
		return MS::kSuccess;
	else
		return MS::kFailure;
}

void *rageRstDeleteShaderDbItem::creator() {
	return new rageRstDeleteShaderDbItem;
}
// --------------

// **********************************
//  LOCK SHADER DB ITEM
//		PARAMS:
//			string - Name of database entry (+ relative path)
// **********************************
MStatus rageRstLockShaderDbItem::doItNow( const MArgList &args ) 
{
	// NOTE: For now, reload the settings
	const char *name = CleanPath(args.asString(0).asChar());
	rageRstShaderPlugin* pobShaderPlugin = rageRstShaderPlugin::GetInstance();
	if(pobShaderPlugin == NULL)
	{
		Displayf("Error getting shader plugin");
		return MS::kFailure;
	}

	rageRstShaderDb* pobShaderDb = pobShaderPlugin->GetShaderDb();
	if(pobShaderDb == NULL)
	{
		Displayf("Error getting shader database");
		return MS::kFailure;
	}

	if ( pobShaderDb->LockEntry(name) ) 
	{
		return MS::kSuccess;
	}
	else 
	{
		Displayf("Error locking %s in shader database", name);
		return MS::kFailure;
	}
}

void *rageRstLockShaderDbItem::creator() {
	return new rageRstLockShaderDbItem;
}
// --------------

// **********************************
//  UNLOCK SHADER DB ITEM
//		PARAMS:
//			string - Name of database entry (+ relative path)
//			bool - Set to true to accept changes
// **********************************
MStatus rageRstUnlockShaderDbItem::doItNow( const MArgList &args ) {
	// NOTE: For now, save the settings
	const char *name = CleanPath(args.asString(0).asChar());
	if ( rageRstShaderPlugin::GetInstance()->GetShaderDb()->UnlockEntry(name, args.asBool(1)) ) {
		return MS::kSuccess;
	}
	else {
		return MS::kFailure;
	}
}

void *rageRstUnlockShaderDbItem::creator() {
	return new rageRstUnlockShaderDbItem;
}
// --------------

// **********************************
//  GET SHADER DB ITEM INFO
//		PARAMS:
//			string - Name of database entry (+ relative path)
// **********************************
MStatus rageRstGetShaderDbItemInfo::doItNow( const MArgList &args ) {
	const char *name = CleanPath(args.asString(0).asChar());
	rageRstShaderDbEntry *entry = rageRstShaderPlugin::GetInstance()->GetShaderDb()->GetItem(name);
	if ( entry ) {
		MString result;
		result = "baseshader='";
		result += entry->m_Preset->GetShaderName();
		result += "' comment='";
		result += entry->m_Preset->GetComment();
		result += "' totalvarcount='";	
		result += entry->m_TotalVariableCount;
		result += "' dbonlyvarcount='";
		result += entry->m_DbOnlyCount;
		result += "'";

		setResult( result );
		return MS::kSuccess;
	}
	else {
		return MS::kFailure;
	}
}

void *rageRstGetShaderDbItemInfo::creator() {
	return new rageRstGetShaderDbItemInfo;
}
// --------------


// **********************************
//  GET SHADER DB ITEM COMMENT
//		PARAMS:
//			string - Name of database entry (+ relative path)
//			string - New comment string to use
// **********************************
MStatus rageRstGetShaderDbItemComment::doItNow( const MArgList &args ) {
	const char *name = CleanPath(args.asString(0).asChar());
	rageRstShaderDbEntry *entry = rageRstShaderPlugin::GetInstance()->GetShaderDb()->GetItem(name);
	if ( entry && entry->m_Preset ) {
		MString result;
		result = entry->m_Preset->GetComment();
		setResult(result);
	}
	else {
		MString result;
		result = name;
		result += " does not exist";
		setResult(result);
		return MS::kFailure;
	}
	return MS::kSuccess;
}

void *rageRstGetShaderDbItemComment::creator() {
	return new rageRstGetShaderDbItemComment;
}
// --------------


// **********************************
//  SET SHADER DB ITEM COMMENT
//		PARAMS:
//			string - Name of database entry (+ relative path)
//			string - New comment string to use
// **********************************
MStatus rageRstSetShaderDbItemComment::doItNow( const MArgList &args ) {
	const char *name = CleanPath(args.asString(0).asChar());
	rageRstShaderDbEntry *entry = rageRstShaderPlugin::GetInstance()->GetShaderDb()->GetItem(name);
	if ( entry && entry->m_Preset ) {
		if ( rageRstShaderPlugin::GetInstance()->GetShaderDb()->IsLocked(entry) == false ) {
			Warningf("Must lock item first");
			return MS::kFailure;
		}
		entry->m_Preset->SetComment(args.asString(1).asChar());
	}
	else {
		return MS::kFailure;
	}
	return MS::kSuccess;
}

void *rageRstSetShaderDbItemComment::creator() {
	return new rageRstSetShaderDbItemComment;
}
// --------------

// **********************************
//  GET SHADER DB VARIABLE INFO
//		PARAMS:
//			string - Name of database entry (+ relative path)
//			int - The variable index
// **********************************
MStatus rageRstGetShaderDbVariableInfo::doItNow( const MArgList &args ) {
	const char *name = CleanPath(args.asString(0).asChar());
	rageRstShaderDbEntry *entry = rageRstShaderPlugin::GetInstance()->GetShaderDb()->GetItem(name);
	if ( entry ) {
		const grcEffect *shader = &entry->m_Preset->GetBasis();
		if ( shader ) {
			int localIdx = args.asInt(1);
			grmVariableInfo info;
			shader->GetInstancedVariableInfo(localIdx, info);
			MString str;
			str = "name='";
			str += info.m_Name;
			str += "' uiname='";
			MString uiName = info.m_UiName;
			// Maya can't deal with spaces in the labels displayed to the user
			MakeNameMayaFriendly(uiName);
			str += uiName;
			str += "_' uihelp='";
			str += info.m_UiHelp ? info.m_UiHelp : "none";
			str += "' type='";
			str += info.m_TypeName;
			str += "' uimax='";
			str += info.m_UiMax;
			str += "' uimin='";
			str += info.m_UiMin;
			str += "' uistep='";
			str += info.m_UiStep;
			str += "' uihint='";
			str += info.m_UiHint ? info.m_UiHint : "none";
			str += "' uidatabase='";
			str += info.m_IsMaterial? "true" : "false";
			str += "'";
			setResult( str );
		}
	}
	else {
		return MS::kFailure;
	}
	return MS::kSuccess;
}

void *rageRstGetShaderDbVariableInfo::creator() {
	return new rageRstGetShaderDbVariableInfo;
}
// --------------

// **********************************
//  GET SHADER DB VARIABLE VALUE
//		PARAMS:
//			string - Name of database entry (+ relative path)
//			int - The variable index
// **********************************
MStatus rageRstGetShaderDbVariableValue::doItNow( const MArgList &args ) {
	const char *name = CleanPath(args.asString(0).asChar());
	rageRstShaderDbEntry *entry = rageRstShaderPlugin::GetInstance()->GetShaderDb()->GetItem(name);
	if ( entry ) {
		if ( entry->m_Preset ) {
			int localIdx = args.asInt(1);
			grmShaderVar val(*entry->m_Preset,(grcEffectVar)localIdx);
			char buff[512];
			val.Save(buff,sizeof(buff));
			MString str = buff;
			setResult( str );
			return MS::kSuccess;
		}
	}
	return MS::kFailure;
}

void *rageRstGetShaderDbVariableValue::creator() {
	return new rageRstGetShaderDbVariableValue;
}
// --------------

// **********************************
//  SET SHADER DB VARIABLE VALUE
// **********************************
MStatus rageRstSetShaderDbVariableValue::doItNow( const MArgList &args ) {
	const char *name = CleanPath(args.asString(0).asChar());
	rageRstShaderDbEntry *entry = rageRstShaderPlugin::GetInstance()->GetShaderDb()->GetItem(name);
	if ( entry ) {
		if ( rageRstShaderPlugin::GetInstance()->GetShaderDb()->IsLocked(entry) == false ) {
			Warningf("Must lock item first");
		}
		else if ( entry->m_Preset ) {
			int localIdx = args.asInt(1);
			grmShaderVar val (*entry->m_Preset,(grcEffectVar)localIdx );
			char buff[128];
			safecpy(buff, args.asString(2).asChar());
			val.Load(buff);
			MString str = buff;
			setResult( str );
			return MS::kSuccess;
		}
	}
	return MS::kFailure;
}

void *rageRstSetShaderDbVariableValue::creator() {
	return new rageRstSetShaderDbVariableValue;
}
// --------------
