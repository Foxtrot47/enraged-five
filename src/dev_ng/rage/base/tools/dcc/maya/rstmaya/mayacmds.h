// 
// rstmaya/mayacmds.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
// These are simple wrappers around rage code to expose various
// functionality to MEL

#ifndef RST_MAYACMDS_H
#define RST_MAYACMDS_H

#include <maya/MPxCommand.h>
class MArgList;

namespace rage {

// **********************************************
//  COMMAND BASE -- Handles thread safety issues
// **********************************************
class rageRstCommand : public MPxCommand {
public:
	rageRstCommand();
	MStatus			doIt( const MArgList &args );
	virtual MStatus	doItNow( const MArgList &args ) = 0;
protected:
	void MakeNameMayaFriendly(MString &str);
	
	// This is set to true to ensure that rage is properly enabled before proceeding
	bool			m_VerifyActive;
};

// ************************************
//  SANDBOX -- Experimentation command
// ************************************
class rageRstSandbox : public rageRstCommand {
public:
	rageRstSandbox();
	MStatus			doItNow( const MArgList &args );
	static void *	creator();

};

// ************************************
//  Activate
// ************************************
class rageRstActivate : public rageRstCommand {
public:
	rageRstActivate();
	MStatus			doIt( const MArgList &args ) { return doItNow(args); }
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
protected:
	static bool		sm_HiddenWindow;
};

// *****************
//  Version
// *****************
class rageRstGetVersion : public rageRstCommand {
public:
	rageRstGetVersion();
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// *****************
//  SHADER COUNT
// *****************
class rageRstGetShaderCount : public rageRstCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// *****************
//  SHADER NAME
// *****************
class rageRstGetShaderName : public rageRstCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  SHADER DB COMMAND
// **********************************
class rageRstShaderDbCommand : public rageRstCommand {
public:
	// NOTE: Contents of CleanPath only valid until next CleanPath call
	const char *CleanPath(const char *rawPath);
};

// **********************************
//  GET SHADER DB ITEM LIST
// **********************************
class rageRstGetShaderDbItemList : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  CREATE SHADER DB GROUP
// **********************************
class rageRstCreateShaderDbGroup : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  DELETE SHADER DB GROUP
// **********************************
class rageRstDeleteShaderDbGroup : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  CREATE SHADER DB ITEM
// **********************************
class rageRstCreateShaderDbItem : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  DELETE SHADER DB ITEM
// **********************************
class rageRstDeleteShaderDbItem : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  LOCK SHADER DB ITEM
// **********************************
class rageRstLockShaderDbItem : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  UNLOCK SHADER DB ITEM
// **********************************
class rageRstUnlockShaderDbItem : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  GET SHADER DB ITEM INFO
// **********************************
class rageRstGetShaderDbItemInfo : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  GET SHADER DB ITEM COMMENT
// **********************************
class rageRstGetShaderDbItemComment : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  SET SHADER DB ITEM COMMENT
// **********************************
class rageRstSetShaderDbItemComment : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  GET SHADER DB VARIABLE INFO
// **********************************
class rageRstGetShaderDbVariableInfo : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  GET SHADER DB VARIABLE VALUE
// **********************************
class rageRstGetShaderDbVariableValue : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

// **********************************
//  SET SHADER DB VARIABLE VALUE
// **********************************
class rageRstSetShaderDbVariableValue : public rageRstShaderDbCommand {
public:
	MStatus			doItNow( const MArgList &args );
	static void *	creator();
};

};	// NAMESPACE RAGE

#endif	// RST_MAYACMDS_H
