set ARCHIVE=rstmaya
call ..\..\libs\rex\setmayapath.bat
set XLIBS=$(MAYA_DEV_PATH_6_0)\lib\Foundation.lib $(MAYA_DEV_PATH_6_0)\lib\OpenMaya.lib $(MAYA_DEV_PATH_6_0)\lib\OpenMayaAnim.lib $(MAYA_DEV_PATH_6_0)\lib\OpenMayaUI.lib 
set XDEFINE=NT_PLUGIN
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\tools\dcc\libs\
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% rst parser parsercore init
set TESTER_BASE=rex_maya60
set FILES=mayacmds
set TESTERS=ragerstmaya

