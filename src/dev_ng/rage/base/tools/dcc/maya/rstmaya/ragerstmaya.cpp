// 
// rageRst/rageRstmaya.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma warning(push)
#if _MSC_VER >= 1400
#pragma warning(disable: 4005) // warning C4005: 'strcasecmp' : macro redefinition
							   // rage\base\src\string\string.h declares strcasecmp, and so does MTypes.h
							   // but they are currently differenct
#endif

// specify additional libraries to link to
#pragma comment(lib, "Foundation.lib")
#pragma comment(lib, "OpenMaya.lib")
#pragma comment(lib, "OpenMayaAnim.lib")
#pragma comment(lib, "OpenMayaUI.lib")
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "../../../libs/devil/libtiff.lib")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// this part allows us to register the MLL (maya plugin)
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "system/xtl.h"
#include "system/param.h"
#include "grmodel/shader.h"

#pragma warning(disable: 4668)
#pragma warning(push, 3)
#include <maya/MFnPlugin.h>
#include <maya/MObject.h>
#include <maya/MSceneMessage.h>
#include <Maya/MGlobal.h>
#pragma warning(pop)

#include "mayacmds.h"

#include "rst/rageloop.h"
#include "rst/shaderplugin.h"
#pragma warning(error: 4668)

#include "atl/array.h"

using namespace rage;

namespace rage
{
#if __ASSERT
	XPARAM(nopopups);
#endif
}

// ********************
//  Mel Function Mgr
// ********************
//	Automates process of registering/unregistering maya functions
//	Having to explicitly keep this coupled is bug prone, so I'm automating it

class rageRstMelFunctionList {
public:
	rageRstMelFunctionList();
	~rageRstMelFunctionList();

	void AddMelCommand( const char *name, void * (*factory)() );
	void RegisterCommands( MFnPlugin &plugin );
	void UnregisterCommands( MFnPlugin &plugin );
	void MayaAborting();

protected:
	struct rageRstCommandInfo {
		const char *m_Name;
		void * (*m_Factory)();
		bool m_Registered;
	};

	atArray<rageRstCommandInfo>	m_Commands;
	bool m_CommandsRegistered;
};

rageRstMelFunctionList::rageRstMelFunctionList() : m_CommandsRegistered(false) {
}

rageRstMelFunctionList::~rageRstMelFunctionList() {
	Assert("Forgot to unregister mel commands" && m_CommandsRegistered == false );
}

void rageRstMelFunctionList::AddMelCommand( const char *name, void * (*factory)() ) {
	rageRstCommandInfo *info = 0;
	if ( m_Commands.GetCount() == m_Commands.GetCapacity() ) {
		info = &m_Commands.Grow();
	}
	else {
		info = &m_Commands.Append();
	}
	info->m_Name = name;
	info->m_Factory = factory;
	info->m_Registered = false;
}

void rageRstMelFunctionList::RegisterCommands( MFnPlugin &plugin ) {
	m_CommandsRegistered = true;
	for (int i = 0; i < m_Commands.GetCount(); ++i) {
		if ( m_Commands[i].m_Registered == false ) {
			plugin.registerCommand( m_Commands[i].m_Name, m_Commands[i].m_Factory );
			m_Commands[i].m_Registered = true;
		}
		else {
			Warningf("Ignoring 2nd attempt at registering: %s", m_Commands[i].m_Name);
		}
	}
}

void rageRstMelFunctionList::UnregisterCommands( MFnPlugin &plugin ) {
	m_CommandsRegistered = false;
	for (int i = 0; i < m_Commands.GetCount(); ++i) {
		if ( m_Commands[i].m_Registered == true ) {
			plugin.deregisterCommand( m_Commands[i].m_Name );
			m_Commands[i].m_Registered = false;
		}
		else {
			Warningf("Ignoring 2nd attempt at deregistering: %s", m_Commands[i].m_Name);
		}
	}
}

void rageRstMelFunctionList::MayaAborting() {
	m_CommandsRegistered = false;
}
// --------------

static rageRstMelFunctionList *sMelList = 0;
static MCallbackId sCallbackId;

#define ADDMELCMD( cmd ) sMelList->AddMelCommand( #cmd, cmd::creator )

void killPlugin( void * ) {
	delete grcMaterialLibrary::SetCurrent(NULL);
	delete rageRstShaderPlugin::GetInstance();
	if ( sMelList )
		sMelList->MayaAborting();
	delete sMelList;
}

MStatus __declspec(dllexport) initializePlugin( MObject obj )
{
	rage::sysParam::Init(0,NULL);

#if __ASSERT
	if(MGlobal::mayaState() != MGlobal::kInteractive)
	{
		//The plugin has been loaded through mayabatch, so disable the RAGE popups for Asserts.
		rage::PARAM_nopopups.Set("");
	}
#endif //__ASSERT

	new rageRstShaderPlugin;

	MStatus   status;
	MFnPlugin plugin( obj, "RAGE Shader Editor", RAGE_RELEASE_STRING " - " __DATE__ " - " __TIME__, "Any");

	sMelList = new rageRstMelFunctionList;

	ADDMELCMD( rageRstSandbox );

	ADDMELCMD( rageRstActivate );
	ADDMELCMD( rageRstGetVersion );

	ADDMELCMD( rageRstGetShaderCount );
	ADDMELCMD( rageRstGetShaderName );

	ADDMELCMD( rageRstGetShaderDbItemList );
	ADDMELCMD( rageRstCreateShaderDbGroup );
	ADDMELCMD( rageRstDeleteShaderDbGroup );
	ADDMELCMD( rageRstCreateShaderDbItem );
	ADDMELCMD( rageRstDeleteShaderDbItem );
	ADDMELCMD( rageRstLockShaderDbItem );
	ADDMELCMD( rageRstUnlockShaderDbItem );
	ADDMELCMD( rageRstGetShaderDbItemInfo );
	ADDMELCMD( rageRstGetShaderDbItemComment );
	ADDMELCMD( rageRstSetShaderDbItemComment );
	ADDMELCMD( rageRstGetShaderDbVariableInfo );
	ADDMELCMD( rageRstGetShaderDbVariableValue );
	ADDMELCMD( rageRstSetShaderDbVariableValue );

	// Register the commands with the plugin
	sMelList->RegisterCommands( plugin );

	// Tell MAYA to let us know when it shuts down
	sCallbackId = MSceneMessage::addCallback( MSceneMessage::kMayaExiting, killPlugin );

	return status;
}

MStatus __declspec(dllexport) uninitializePlugin( MObject obj )
{
	MStatus   status;
	MFnPlugin plugin( obj );

	grmShaderFactory::ClearShaderLibPath();

	// Automatically remove all registered commands
	sMelList->UnregisterCommands( plugin );
	MSceneMessage::removeCallback(sCallbackId);
	killPlugin(&obj);

	return status;
}


