// 
// /meshPointNormalsAway.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include "maya/MGlobal.h"
#include "maya/MObject.h"
#include "maya/MArgDatabase.h"
#include "maya/MStringArray.h"
#include "maya/MFnDagNode.h"
#include "maya/MFnMesh.h"
#include "maya/MFloatPointArray.h"
#include "maya/MSelectionList.h"
#include "maya/MItSelectionList.h"
#include "maya/MDagPath.h"
#include "maya/MItMeshPolygon.h"
#include "maya/MPointArray.h"
#include "maya/MArgList.h"
#include "maya/MFnTransform.h"
#include "maya/MObjectArray.h"
#include "maya/MFnComponentListData.h"
#include "maya/MFnDoubleIndexedComponent.h"
#include "maya/MFnSingleIndexedComponent.h"
#include "maya/MItMeshVertex.h"
#include "maya/MItMeshFaceVertex.h"
#include "maya/MItMeshVertex.h"

#pragma warning(pop)

#include "float.h"
#include <vector>
#include <algorithm>

#include "meshPointNormalsAway.h"

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------

MSyntax	meshPointNormalsAwayCmd::newSyntax()
{
	MSyntax syntax;

	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus	meshPointNormalsAwayCmd::parseArguments( const MArgList& args )
{
	MStatus status(MS::kSuccess);
	return status;
}

//-----------------------------------------------------------------------------

MStatus	meshPointNormalsAwayCmd::doIt( const MArgList& args )
{
	MStatus status(MS::kSuccess);

	//Get the current selection
	MSelectionList activeSelList;
	MGlobal::getActiveSelectionList(activeSelList);
	if(activeSelList.isEmpty())
	{
		MGlobal::displayError("No vertices selected.");
		return MS::kFailure;
	}

	//If necessary convert the selection to a faceVertex selection set
	int nActiveSelItems = activeSelList.length();
	MSelectionList operationSelList;
	for(int selIdx=0; selIdx < nActiveSelItems; selIdx++)
	{
		MDagPath tmpDp;
		MObject	 tmpMObj;
		status = activeSelList.getDagPath(selIdx, tmpDp, tmpMObj);
		if(!tmpMObj.isNull())
		{
			MFn::Type tmpMObjType = tmpMObj.apiType();
			if(tmpMObjType == MFn::kMeshPolygonComponent)
			{
				//Polygons are selected so convert polygon component selection to a face vertex selection
				MIntArray polyIndexArray;
				MIntArray faceVertexIndexArray;

				MItMeshPolygon tmpPolyIter(tmpDp, tmpMObj);
				if(status == MS::kSuccess)
				{
					for(tmpPolyIter.reset(); !tmpPolyIter.isDone(); tmpPolyIter.next())
					{
						MIntArray polyVertIndices;
						tmpPolyIter.getVertices(polyVertIndices);
						for(unsigned int i=0; i<polyVertIndices.length(); i++)
						{
							polyIndexArray.append(tmpPolyIter.index());
							faceVertexIndexArray.append(polyVertIndices[i]);
						}
					}

					MFnDoubleIndexedComponent tmpDiCmp;
					tmpDiCmp.create(MFn::kMeshVtxFaceComponent, &status);
					status = tmpDiCmp.addElements(faceVertexIndexArray,polyIndexArray);
					operationSelList.add(tmpDp, tmpDiCmp.object());
				}
			}
			else if(tmpMObjType == MFn::kMeshVertComponent)
			{
				//Vertices are selected so convert the vertex component selection to a face vertex selection
				MIntArray polyIndexArray;
				MIntArray faceVertexIndexArray;

				MItMeshVertex tmpVertIter(tmpDp, tmpMObj);
				if(status == MS::kSuccess)
				{
					for(tmpVertIter.reset(); !tmpVertIter.isDone(); tmpVertIter.next())
					{
						int vertIdx = tmpVertIter.index();
						MIntArray connectedFaces;
						tmpVertIter.getConnectedFaces(connectedFaces);
						int nConnectedFaces = connectedFaces.length();
						for(int faceIdx=0; faceIdx<nConnectedFaces; faceIdx++)
						{
							polyIndexArray.append(connectedFaces[faceIdx]);
							faceVertexIndexArray.append(vertIdx);
						}
					}
			
					MFnDoubleIndexedComponent tmpDiCmp;
					tmpDiCmp.create(MFn::kMeshVtxFaceComponent, &status);
					status = tmpDiCmp.addElements(faceVertexIndexArray,polyIndexArray);
					operationSelList.add(tmpDp, tmpDiCmp.object());
				}
			}
		}//End if(!tmpMObj.isNull())
		else
		{
			//An entire object is selected so convert the selection to a face vertex selection for all the face vertices in the object
			MIntArray polyIndexArray;
			MIntArray faceVertexIndexArray;

			MItMeshFaceVertex tmpFaceVertIter(tmpDp.node(), &status);
			if(status == MS::kSuccess)
			{
				for(tmpFaceVertIter.reset(); !tmpFaceVertIter.isDone(); tmpFaceVertIter.next())
				{
					polyIndexArray.append( tmpFaceVertIter.faceId() );
					faceVertexIndexArray.append( tmpFaceVertIter.vertId() );
				}

				MFnDoubleIndexedComponent tmpDiCmp;
				tmpDiCmp.create(MFn::kMeshVtxFaceComponent, &status);
				status = tmpDiCmp.addElements(faceVertexIndexArray,polyIndexArray);
				operationSelList.add(tmpDp, tmpDiCmp.object());
			}
		}
	}//End for(int selIdx=0;...


	MItSelectionList faceVertexComponentIter(operationSelList, MFn::kMeshVtxFaceComponent, &status);
	for (faceVertexComponentIter.reset(); !faceVertexComponentIter.isDone(); faceVertexComponentIter.next())
	{
		MDagPath	meshDagPath;
		MObject		multiVertexComponent;

		faceVertexComponentIter.getDagPath(meshDagPath, multiVertexComponent);

		if (!multiVertexComponent.isNull())
		{
			MFnMesh fnMesh(meshDagPath);
		}

		meshVertexNormalInfo* pNrmInfo = new meshVertexNormalInfo();
		pNrmInfo->dagPath = meshDagPath;
		int count = 0;

		for (MItMeshFaceVertex faceVertexIter(meshDagPath, multiVertexComponent); !faceVertexIter.isDone(); faceVertexIter.next())
		{
			MPoint pos = faceVertexIter.position(MSpace::kWorld);
			pNrmInfo->avgPosX += pos.x;
			pNrmInfo->avgPosY += pos.y;
			pNrmInfo->avgPosZ += pos.z;
			count++;
		}
		pNrmInfo->avgPosX /= (double)count;
		pNrmInfo->avgPosY /= (double)count;
		pNrmInfo->avgPosZ /= (double)count;

		for (MItMeshFaceVertex faceVertexIter(meshDagPath, multiVertexComponent); !faceVertexIter.isDone(); faceVertexIter.next())
		{
			int faceId = faceVertexIter.faceId();
			int vertID = faceVertexIter.vertId();

			MVector normal;
			faceVertexIter.getNormal(normal);
			MPoint pos = faceVertexIter.position(MSpace::kWorld);

			pNrmInfo->faceIds.append(faceId);
			pNrmInfo->vertexIds.append(vertID);
			pNrmInfo->orgNormals.append(normal);

			MVector modNormal;
			modNormal.x = (pos.x - pNrmInfo->avgPosX);
			modNormal.y = (pos.y - pNrmInfo->avgPosY);
			modNormal.z = (pos.z - pNrmInfo->avgPosZ);
			modNormal.normalize();

			pNrmInfo->modNormals.append(modNormal);
		}

		m_modNrmList.push_back(pNrmInfo);

	}//End for (faceVertexComponentIter.reset()...

	return redoIt();
}

//-----------------------------------------------------------------------------

MStatus meshPointNormalsAwayCmd::redoIt()
{
	MStatus status;

	int nObjects = (int)m_modNrmList.size();
	for(int objIdx=0; objIdx<nObjects; objIdx++)
	{
		meshVertexNormalInfo* pNrmInfo = m_modNrmList[objIdx];

		MFnMesh fnMesh(pNrmInfo->dagPath);

		status = fnMesh.unlockFaceVertexNormals(pNrmInfo->faceIds, pNrmInfo->vertexIds);
		if(status != MS::kSuccess)
		{
			status.perror("unlockFaceVertexNormals failed.");
			return status;
		}

		status = fnMesh.setFaceVertexNormals(pNrmInfo->modNormals, pNrmInfo->faceIds, pNrmInfo->vertexIds, MSpace::kObject);
		if(status != MS::kSuccess)
		{
			status.perror("setFaceVertexNormals failed.");
			return status;
		}
	}
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus meshPointNormalsAwayCmd::undoIt()
{
	MStatus status;

	int nObjects = (int)m_modNrmList.size();
	for(int objIdx=0; objIdx<nObjects; objIdx++)
	{
		meshVertexNormalInfo* pNrmInfo = m_modNrmList[objIdx];

		MFnMesh fnMesh(pNrmInfo->dagPath);
		status = fnMesh.setFaceVertexNormals(pNrmInfo->orgNormals, pNrmInfo->faceIds, pNrmInfo->vertexIds, MSpace::kObject);
		if(status != MS::kSuccess)
		{
			status.perror("setFaceVertexNormals failed.");
			return status;
		}
	}

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

