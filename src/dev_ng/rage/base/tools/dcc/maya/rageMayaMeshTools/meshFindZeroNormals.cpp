// 
// /meshFindZeroNormals.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include "maya/MGlobal.h"
#include "maya/MObject.h"
#include "maya/MArgDatabase.h"
#include "maya/MStringArray.h"
#include "maya/MFnDagNode.h"
#include "maya/MFnMesh.h"
#include "maya/MFloatPointArray.h"
#include "maya/MSelectionList.h"
#include "maya/MItSelectionList.h"
#include "maya/MDagPath.h"
#include "maya/MItMeshPolygon.h"
#include "maya/MPointArray.h"
#include "maya/MArgList.h"
#include "maya/MFnTransform.h"
#include "maya/MObjectArray.h"
#include "maya/MFnComponentListData.h"
#include "maya/MFnSingleIndexedComponent.h"
#include "maya/MItMeshVertex.h"
#include "maya/MColorArray.h"
#include "maya/MColor.h"
#include "maya/MItMeshFaceVertex.h"
#include "maya/MItMeshVertex.h"

#pragma warning(pop)

#include "float.h"
#include <vector>
#include <algorithm>

#include "meshFindZeroNormals.h"

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------

meshFindZeroNormalsCmd::meshFindZeroNormalsCmd()
	: m_Select(false)
	, m_Repair(false)
	, m_FindFaces(false)
{

}

//-----------------------------------------------------------------------------

MSyntax meshFindZeroNormalsCmd::newSyntax()
{
	MSyntax syntax;

	syntax.enableQuery(false);

	syntax.addFlag("-sel", "-select");
	syntax.addFlag("-rep", "-repair");
	syntax.addFlag("-fc", "-faces");

	syntax.enableEdit(false);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus meshFindZeroNormalsCmd::parseArguments( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	MArgDatabase argDb(syntax(), args);

	if(argDb.isFlagSet("-select"))
		m_Select = true;

	if(argDb.isFlagSet("-repair"))
		m_Repair = true;

	if(argDb.isFlagSet("-faces"))
		m_FindFaces = true;

	if( m_Select && m_Repair )
	{
		MGlobal::displayError("Cannot use -select and -repair together");
		return MS::kInvalidParameter;
	}

	if( !m_Select && !m_Repair )
		m_Select = true;

	if(m_Repair && m_FindFaces)
	{
		MGlobal::displayError("Cannot repair face normals, -repair is only valid for verts");
		return MS::kInvalidParameter;
	}

	return status;
}

//-----------------------------------------------------------------------------

MStatus meshFindZeroNormalsCmd::doIt( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	//Parse the command arguments
	status = parseArguments( args );
	if(status != MS::kSuccess)
	{
		status.perror("parseArguments failed.");
		return status;
	}

	if(m_FindFaces)
		return DoFindFaces();
	else
		return DoFindVerts();
}

//-----------------------------------------------------------------------------

MStatus meshFindZeroNormalsCmd::DoFindFaces()
{
	MStatus status = MS::kSuccess;

	//Get the current selection
	MSelectionList activeSelList;
	MGlobal::getActiveSelectionList(activeSelList);
	if(activeSelList.isEmpty())
	{
		MGlobal::displayError("No objects selected to inspect.");
		return MS::kFailure;
	}

	MSelectionList	badNrmList;

	int nObjects = activeSelList.length();
	for( int objIdx=0; objIdx<nObjects; objIdx++)
	{
		MDagPath dp;
		activeSelList.getDagPath(objIdx, dp);

		status = dp.extendToShape();
		if(status != MS::kSuccess)
			continue;

		if( dp.apiType() != MFn::kMesh )
			continue;

		MFnMesh fnMesh( dp );

		MItMeshPolygon mIt( dp );

		for( mIt.reset(); !mIt.isDone(); mIt.next() )
		{
			MVector faceNrm;
			status = mIt.getNormal( faceNrm, MSpace::kWorld );
			
			double nrmLength = faceNrm.length();
			if(nrmLength < 0.001)
			{
				badNrmList.add( dp, mIt.currentItem() );
			}
		}
	}

	if(badNrmList.length() == 0)
	{
		MGlobal::displayInfo("No faces with zero length normals found");
	}
	else
	{
		if(m_Select)
			MGlobal::setActiveSelectionList( badNrmList );
		else
		{
			char msg[512];
			sprintf_s(msg, "Repaired %d zero-length face normals.", badNrmList.length() );
			MGlobal::displayInfo(msg);
		}
	}

	return status;
}

//-----------------------------------------------------------------------------

MStatus meshFindZeroNormalsCmd::DoFindVerts()
{
	MStatus status = MS::kSuccess;

	//Get the current selection
	MSelectionList activeSelList;
	MGlobal::getActiveSelectionList(activeSelList);
	if(activeSelList.isEmpty())
	{
		MGlobal::displayError("No objects selected to inspect.");
		return MS::kFailure;
	}

	MSelectionList	badNrmList;

	int nObjects = activeSelList.length();
	for( int objIdx=0; objIdx<nObjects; objIdx++)
	{
		MDagPath dp;
		activeSelList.getDagPath(objIdx, dp);

		status = dp.extendToShape();
		if(status != MS::kSuccess)
			continue;

		if( dp.apiType() != MFn::kMesh )
			continue;

		MFnMesh fnMesh( dp );
		MItMeshVertex mIt( dp );

		for( mIt.reset(); !mIt.isDone(); mIt.next() )
		{
			MIntArray conFaceIdxs;
			status = mIt.getConnectedFaces( conFaceIdxs );
			int nConFaces = conFaceIdxs.length();
			for(int faceIdx=0; faceIdx<nConFaces; faceIdx++)
			{
				MVector faceVertNrm;
				status = mIt.getNormal( faceVertNrm, conFaceIdxs[faceIdx], MSpace::kWorld );

				double nrmLength = faceVertNrm.length();
				if(nrmLength < 0.001)
				{
					status = badNrmList.add( dp, mIt.currentItem() );

					if(m_Repair)
					{
						MVector combFaceVertNrm(0.0,0.0,0.0);
						for(int i=0; i<nConFaces; i++)
						{
							MVector faceNormal;
							fnMesh.getPolygonNormal( conFaceIdxs[i], faceNormal, MSpace::kObject );
							combFaceVertNrm = combFaceVertNrm + faceNormal;
						}
						combFaceVertNrm.normalize();

						status = fnMesh.setVertexNormal( combFaceVertNrm, mIt.index(), MSpace::kObject );
						if(status != MS::kSuccess)
						{
							status.perror("Failed to set vertex normal");
						}
					}
				}
			}//End for(int faceIdx...
		}
	}


	if(badNrmList.length() == 0)
	{
		MGlobal::displayInfo("No verts with zero length normals found");
	}
	else
	{
		if(m_Select)
			MGlobal::setActiveSelectionList( badNrmList );
		else
		{
			char msg[512];
			sprintf_s(msg, "Repaired %d zero-length vertex normals.", badNrmList.length() );
			MGlobal::displayInfo(msg);
		}
	}

	return status;
}

//-----------------------------------------------------------------------------

