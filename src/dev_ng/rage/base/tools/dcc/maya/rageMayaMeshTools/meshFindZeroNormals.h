// 
// /meshFindZeroNormals.h
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MPxCommand.h>
#include <maya/MColor.h>
#pragma warning(pop)

//-----------------------------------------------------------------------------

namespace rage
{
	class meshFindZeroNormalsCmd : public MPxCommand
	{
	public:
		meshFindZeroNormalsCmd();
		virtual ~meshFindZeroNormalsCmd() {};

		static	void*	creator() { return new meshFindZeroNormalsCmd(); }
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual bool	isUndoable() const { return false; }

	public:
		MStatus	parseArguments( const MArgList& args );

	private:
		MStatus DoFindFaces();
		MStatus DoFindVerts();

	private:
		bool	m_Select;
		bool	m_Repair;
		bool	m_FindFaces;
	};

}//End namespace rage

//-----------------------------------------------------------------------------

