// 
// /pointNormalsAway.h
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MPxCommand.h>
#include <maya/MDagPath.h>
#include <maya/MVectorArray.h>
#include <maya/MIntArray.h>

#pragma warning(pop)

#include <vector>

using namespace std;

//-----------------------------------------------------------------------------

namespace rage
{
	
	struct meshVertexNormalInfo
	{
		MDagPath	dagPath;

		double		avgPosX;
		double		avgPosY;
		double		avgPosZ;

		MVectorArray	orgNormals;
		MVectorArray	modNormals;

		MIntArray		vertexIds;
		MIntArray		faceIds;

		meshVertexNormalInfo()
			: avgPosX(0.0)
			, avgPosY(0.0)
			, avgPosZ(0.0)
		{};
	};

	typedef std::vector< meshVertexNormalInfo* > MESHNRMLIST;

//-----------------------------------------------------------------------------

	class meshPointNormalsAwayCmd : public MPxCommand
	{
	public:
		meshPointNormalsAwayCmd() {};
		virtual ~meshPointNormalsAwayCmd() {};

		static	void*	creator() { return new meshPointNormalsAwayCmd(); }
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual MStatus redoIt();
		virtual MStatus undoIt();
		virtual bool	isUndoable() const { return true; }

	public:
		MStatus	parseArguments( const MArgList& args );

	private:
		MESHNRMLIST	m_modNrmList;
	};


}//End namespace rage

//-----------------------------------------------------------------------------

