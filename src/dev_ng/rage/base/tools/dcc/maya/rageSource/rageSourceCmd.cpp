/****************************************************************************
*	$Header$
*	Function to source MEL files
*
*	CHANGES
*	4.11.00	Kevin Rose	created
*
****************************************************************************/
//
// Copyright (C) 2000 Kevin's Plugin Factory 
// 
// File: rageSourceCmd.cpp
//
// MEL Command: rageSource
//
// Author: camkr
//
// Includes everything needed to register a simple MEL command with Maya.
// 
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <maya/MSimple.h>
#include <maya/MGlobal.h>
#pragma warning(pop)

// Use helper macro to register a command with Maya.  It creates and
// registers a command that does not support undo or redo.  The 
// created class derives off of MPxCommand.
//
DeclareSimpleCommand( rageSource, "rage", "6.0");

MStatus rageSource::doIt( const MArgList& args )
//
//	Description:
//		implements the MEL rageSource command.
//
//	Arguments:
//		args - the argument list that was passes to the command from MEL
//
//	Return Value:
//		MS::kSuccess - command succeeded
//		MS::kFailure - command failed (returning this value will cause the 
//                     MEL script that is being run to terminate unless the
//                     error is caught using a "catch" statement.
//
{
	MStatus stat = MS::kSuccess;
	MString obStrFilename;
	obStrFilename = args.asString(0, &stat);

	// Check it worked
	if((obStrFilename=="") || (!stat)){
		// They done something wrong, the fools
		MGlobal::displayInfo("Error :: Wrong parameters");
		MGlobal::displayInfo("Usage :: rageSource <filename>");
		return MStatus::kFailure;
	}

	// Do something dodgy, hidden in here.  Move along, nothing to see here....
//	MGlobal::executeCommand("optionVar -sv \"defaultFileSaveType\" \"mayaAscii\"");

	// source the file
	stat = MGlobal::sourceFile(obStrFilename);
	return stat;
}

