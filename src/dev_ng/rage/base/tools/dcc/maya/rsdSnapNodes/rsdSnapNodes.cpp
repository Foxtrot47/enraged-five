//
// Copyright (C) Rockstar San Diego
// 
// File: pluginMain.cpp
//
// Author: Steven Waller
//

#pragma comment(lib, "Foundation.lib")
#pragma comment(lib, "OpenMaya.lib")
#pragma comment(lib, "OpenMayaAnim.lib")
#pragma comment(lib, "OpenMayaUI.lib")
#pragma comment(lib, "OpenGL32.lib")

#include "rsdSnapNodesCmd.h"

#pragma warning(push)
#pragma warning(disable:4100)
#include <maya/MFnPlugin.h>
#pragma warning(pop)

MStatus __declspec(dllexport) initializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is loaded into Maya.  It 
//		registers all of the services that this plug-in provides with 
//		Maya.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{ 
	MStatus   status;
	MFnPlugin plugin( obj, "Rockstar San Diego", "6.5", "Any");

	status = plugin.registerCommand( "rsdSnapNodes", rsdSnapNodes::creator, rsdSnapNodes::addFlags );
	if (!status) {
		status.perror("registerCommand");
		return status;
	}

	return status;
}

MStatus __declspec(dllexport) uninitializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is unloaded from Maya. It 
//		deregisters all of the services that it was providing.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{
	MStatus   status;
	MFnPlugin plugin( obj );

	status = plugin.deregisterCommand( "rsdSnapNodes" );
	if (!status) {
		status.perror("deregisterCommand");
		return status;
	}

	return status;
}
