#ifndef _rsdSnapNodesCmd
#define _rsdSnapNodesCmd
//
// Copyright (C) Rockstar San Diego
// 
// File: rsdSnapNodesCmd.h
//
// MEL Command: rsdSnapNodes
//
// Author: Steven Waller
//

#include "system/xtl.h"

#include <maya/MPxCommand.h>

#include <maya/MDagPath.h>
#include <maya/MSelectionList.h>
#include <maya/MVectorArray.h>

//class MArgList;

class rsdSnapNodes : public MPxCommand
{

public:
				rsdSnapNodes();
	virtual		~rsdSnapNodes();

	MStatus		doIt( const MArgList& );
	MStatus		redoIt();
	MStatus		undoIt();
	bool		isUndoable() const;
	static		MSyntax addFlags();

	static		void* creator();

private:
	MDagPath		dpDestMesh;
	MSelectionList	editList;
	MVectorArray	vaOrigPos, vaNewPos;
};

#endif
