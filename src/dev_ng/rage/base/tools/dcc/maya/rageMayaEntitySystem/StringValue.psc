<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef base="CValue" type="CStringValue">
<!--
std::strings and std::vectors are no longer supported, and this causes errors when doing a rebuild_parser_files.rb pass
<string name="m_strValue" type="std::string"/>
-->
</structdef>

</ParserSchema>