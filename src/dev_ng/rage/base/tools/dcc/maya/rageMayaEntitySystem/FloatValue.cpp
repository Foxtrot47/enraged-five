// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "floatvalue.h"
#include "floatvalue_parser.h"

#include <iostream>
using namespace std;


CFloatValue::CFloatValue()
: CValue(), m_fValue(0.0f)
{
	SetValueType("float");
}

CFloatValue::~CFloatValue(void)
{
}

float CFloatValue::GetValue() const 
{
	if(ValueHasBeenProvided())
	{
		return m_fValue;
	}
	return GetParameter()->GetDefaultValue();
}

vector<string> CFloatValue::GetValueInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CValue::GetValueInfoAsStringVector();
	m_obReturnMe.push_back("value:value");
	char acBuffer[32];
	sprintf(acBuffer, "%f", m_fValue);
	m_obReturnMe.push_back(acBuffer);

	return m_obReturnMe;
}

