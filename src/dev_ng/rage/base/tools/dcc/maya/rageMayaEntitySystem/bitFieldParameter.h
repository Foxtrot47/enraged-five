// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parameter.h"
#include "parser/manager.h"

using namespace rage;

class CBitfieldParameter : public CParameter
{
public:
	CBitfieldParameter(void);
	virtual ~CBitfieldParameter(void);

	// Accessors/Modifers
	int GetDefaultValue() const { return m_iDefaultValue;	}
	void SetDefaultValue(const int iDefaultValue) { m_iDefaultValue = iDefaultValue;	}
	const vector<string>&	GetVectorOfBitLabels() const {return m_obVectorOfBitLabels;}
	vector<string>	GetVectorOfBitLabels() {return m_obVectorOfBitLabels;}
	const string&	GetBitLabel(unsigned iLabelNo) const {return m_obVectorOfBitLabels[iLabelNo];}
	void	SetBitLabel(unsigned iLabelNo, const string strLabel);
	void	AddBitLabel(const string strLabel) {m_obVectorOfBitLabels.push_back(strLabel);}
	virtual vector<string> GetParameterInfoAsStringVector() const;

private:
	int m_iDefaultValue;
	vector<string>	m_obVectorOfBitLabels;

	PAR_PARSABLE();
};
