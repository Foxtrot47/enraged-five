// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "value.h"
#include "value_parser.h"
#include "entitytypesdatabase.h"
#include "entitiesdatabase.h"

#include <iostream>
using namespace std;


CValue::CValue()
: m_bValueProvided(false), m_pobParentValue(NULL), m_pobParameterValueIsBasedOn(NULL), m_strParameterName(""), m_eUsesForValue(USE_ENTITY_TYPE_VALUE)
{
	SetValueType("invalid");
}

CValue::~CValue(void)
{
}
 
void	CValue::ConnectUpPointersAfterLoad(const CValue* pobParentValue)
{
	// First wire myself up
	SetParentValue(pobParentValue);
	if(GetParentValue())
	{
		// I have a parent value, so look at the children parameters it has to find my partner
		CEntityType* pobParentParameter = (CEntityType*)(GetParentValue()->GetParameter());

		// Go through parent's parameters looking for my partner
		for(unsigned i=0; i<pobParentParameter->GetParameters().size(); i++)
		{
			if(pobParentParameter->GetParameters()[i]->GetName() == GetParameterName())
			{
				// Bingo, I have found my mate, so point at it
				SetParameter(pobParentParameter->GetParameters()[i]);
				break;
			}
		}
	}
	else
	{
		// I do not have a parent value, so look at the entities in the root of the database to find my partner
		SetParameter(CEntitiesDatabase::GetEntitiesDatabase()->GetEntityTypesDatabase()->GetEntityType(GetParameterName()));
	}
}

vector<string> CValue::GetValueInfoAsStringVector() const
{
	vector<string> m_obReturnMe;
	m_obReturnMe.push_back("value:use");
	switch(GetUsesForValue())
	{
	case USE_ENTITY_TYPE_VALUE:
		{
			m_obReturnMe.push_back("value:use entity type value");
			break;
		}
	case USE_ENTITY_VALUE:
		{
			m_obReturnMe.push_back("value:use entity value");
			break;
		}
	case USE_ENTITY_PROXY_VALUE:
		{
			m_obReturnMe.push_back("value:use entity proxy value");
			break;
		}
	}
	m_obReturnMe.push_back("value:value provided");
	m_obReturnMe.push_back(m_bValueProvided ? "true" : "false");
	return m_obReturnMe;
}

