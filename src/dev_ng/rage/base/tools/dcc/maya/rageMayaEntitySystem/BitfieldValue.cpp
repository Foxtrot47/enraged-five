// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "bitfieldvalue.h"
#include "bitfieldvalue_parser.h"

#include <iostream>
using namespace std;


CBitfieldValue::CBitfieldValue()
: CValue()
{
	SetValueType("bitfield");
}

CBitfieldValue::~CBitfieldValue(void)
{
}

int CBitfieldValue::GetValue() const 
{
	if(ValueHasBeenProvided())
	{
		return m_iValue;
	}
	return GetParameter()->GetDefaultValue();
}

vector<string> CBitfieldValue::GetValueInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CValue::GetValueInfoAsStringVector();
	m_obReturnMe.push_back("value:value");
	char acBuffer[32];
	sprintf(acBuffer, "%d", m_iValue);
	m_obReturnMe.push_back(acBuffer);
	
	return m_obReturnMe;
}

