<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef base="CValue" onPostLoad="ConnectUpPointersAfterLoad_PostLoadCallback" type="CEntityValue">
<!--
std::strings and std::vectors are no longer supported, and this causes errors when doing a rebuild_parser_files.rb pass
<string name="m_strName" type="std::string"/>
<array name="m_obVectorOfParameterValues" type="std::vector">
	<pointer type="CValue" policy="owner"/>
</array>
-->
</structdef>

</ParserSchema>