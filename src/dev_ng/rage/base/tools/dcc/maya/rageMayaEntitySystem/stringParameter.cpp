// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "stringparameter.h"
#include "stringparameter_parser.h"

#include <iostream>
using namespace std;

CStringParameter::CStringParameter(void)
: CParameter()
{
	SetDefaultValue("");
	SetName("");
	SetDescription("");
	SetParameterType("string");
}

CStringParameter::~CStringParameter(void)
{
}


vector<string> CStringParameter::GetParameterInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CParameter::GetParameterInfoAsStringVector();

	m_obReturnMe.push_back("parameter:default");
	m_obReturnMe.push_back(GetDefaultValue());

	return m_obReturnMe;
}


