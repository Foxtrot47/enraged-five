// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parser/manager.h"
#include "value.h"
#include "bitfieldparameter.h"
#include <string>

using namespace std;
using namespace rage;

class CBitfieldValue : public CValue
{
public:
	CBitfieldValue();
	virtual ~CBitfieldValue(void);

	// Accessors/Modifers
	virtual const CBitfieldParameter* GetParameter() const {return (CBitfieldParameter*)CValue::GetParameter();}
	int GetValue() const;
	void SetValue(int iValue) {m_iValue = iValue; ProvidedValue();}
	virtual vector<string> GetValueInfoAsStringVector() const;

protected:

private:
	int		m_iValue;
	PAR_PARSABLE();
};
