// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "stringvalue.h"
#include "stringvalue_parser.h"

#include <iostream>
using namespace std;


CStringValue::CStringValue()
: CValue()
{
	SetValueType("string");
}

CStringValue::~CStringValue(void)
{
}

string CStringValue::GetValue() const 
{
	if(ValueHasBeenProvided())
	{
		return m_strValue;
	}
	return GetParameter()->GetDefaultValue();
}


vector<string> CStringValue::GetValueInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CValue::GetValueInfoAsStringVector();
	m_obReturnMe.push_back("value:value");
	m_obReturnMe.push_back(m_strValue);

	return m_obReturnMe;
}

