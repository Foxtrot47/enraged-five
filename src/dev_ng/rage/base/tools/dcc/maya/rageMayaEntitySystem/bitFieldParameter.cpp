// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "bitfieldparameter.h"
#include "bitfieldparameter_parser.h"

CBitfieldParameter::CBitfieldParameter(void)
: CParameter()
{
	SetDefaultValue(0);
	SetName("");
	SetDescription("");
	SetParameterType("bitfield");
}

CBitfieldParameter::~CBitfieldParameter(void)
{
}

void	CBitfieldParameter::SetBitLabel(unsigned iLabelNo, const string strLabel) 
{
	if(iLabelNo < m_obVectorOfBitLabels.size())
	{
		m_obVectorOfBitLabels[iLabelNo] = strLabel;
	}
	else
	{
		AddBitLabel(strLabel);
		SetBitLabel(iLabelNo, strLabel);
	}
}

vector<string> CBitfieldParameter::GetParameterInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CParameter::GetParameterInfoAsStringVector();
	m_obReturnMe.push_back("parameter:default");
	char acBuffer[32];
	sprintf(acBuffer, "%d", GetDefaultValue());
	m_obReturnMe.push_back(acBuffer);

	m_obReturnMe.push_back("parameter:noofbitlabels");
	sprintf(acBuffer, "%d", GetVectorOfBitLabels().size());
	m_obReturnMe.push_back(acBuffer);

	m_obReturnMe.push_back("parameter:bitlabels");
	for(unsigned i=0; i<GetVectorOfBitLabels().size(); i++) m_obReturnMe.push_back(GetBitLabel(i));
	return m_obReturnMe;
}


