// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "parameter.h"
#include "parameter_parser.h"

#include <iostream>
using namespace std;


CParameter::CParameter(void)
{
	SetName("");
	SetDescription("");
	SetParameterType("invalid");
}

CParameter::~CParameter(void)
{
}

vector<string> CParameter::GetParameterInfoAsStringVector() const
{
	vector<string> m_obReturnMe;
	m_obReturnMe.push_back("parameter:name");
	m_obReturnMe.push_back(GetName());
	m_obReturnMe.push_back("parameter:description");
	m_obReturnMe.push_back(GetDescription());
	m_obReturnMe.push_back("parameter:type");
	m_obReturnMe.push_back(GetParameterType());
	return m_obReturnMe;
}

