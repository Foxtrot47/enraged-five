// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parser/manager.h"
#include "value.h"
#include "nodeparameter.h"
#include <string>

using namespace std;
using namespace rage;

class CNodeValue : public CValue
{
public:
	CNodeValue();
	virtual ~CNodeValue(void);

	// Accessors/Modifers
	virtual const CNodeParameter* GetParameter() const {return (CNodeParameter*)CValue::GetParameter();}
	virtual vector<string> GetValueInfoAsStringVector() const;

protected:

private:
	PAR_PARSABLE();
};
