// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "meshparameter.h"
#include "meshparameter_parser.h"

#include <iostream>
using namespace std;

CMeshParameter::CMeshParameter(void)
: CParameter()
{
	SetParameterType("mesh");
}

CMeshParameter::~CMeshParameter(void)
{
}

vector<string> CMeshParameter::GetParameterInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CParameter::GetParameterInfoAsStringVector();
	return m_obReturnMe;
}


