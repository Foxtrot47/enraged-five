<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef type="CParameter">
<!--
std::strings and std::vectors are no longer supported, and this causes errors when doing a rebuild_parser_files.rb pass
	<string name="m_strName" type="std::string"/>
	<string name="m_strDescription" type="std::string"/>
	<string name="m_strParameterType" type="std::string"/>
-->
</structdef>

</ParserSchema>