// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "entitytype.h"
#include "entitytype_parser.h"
#include "StringParameter.h"
#include "bitfieldParameter.h"
#include "intParameter.h"
#include "floatParameter.h"
#include "nodeparameter.h"

#include <string>
#include <vector>
#include <iostream>
using namespace std;


CEntityType::CEntityType(void)
: CParameter()
{
	SetParameterType("entity");
}

CEntityType::~CEntityType(void)
{
	for(unsigned i=0; i<m_obVectorOfParameters.size(); i++)
	{
		delete m_obVectorOfParameters[i];
	}
}

const string& CEntityType::GetRandomString() const
{
	static vector<string> astrRandomBallOfStrings;
	astrRandomBallOfStrings.push_back("Hamster");
	astrRandomBallOfStrings.push_back("Fish");
	astrRandomBallOfStrings.push_back("Dinosaur");
	astrRandomBallOfStrings.push_back("Gerbil");
	astrRandomBallOfStrings.push_back("Starfish");
	astrRandomBallOfStrings.push_back("Cat");
	astrRandomBallOfStrings.push_back("Dog");

	return (astrRandomBallOfStrings[rand() % astrRandomBallOfStrings.size()]);
}


void CEntityType::RandomlyGenerateATestEntityType()
{
//	cout<<"GetRandomString() = "<<GetRandomString()<<endl;
	SetName(GetRandomString() +"EntityTypeName");
	SetDescription("This contains an entity type, it is a base type for an Entity, an example would be 'Fish', with an Entity being 'Shark' and an Entity Proxy being 'SimonTheShark'");

	// An entity type is just a collection of parameters, without values
	// Always start with the entity types name

	// Next throw in a random number of random params
	int iNoOfParams = rand() % 10;
	for(int i=0; i<iNoOfParams; i++)
	{
		// What type param shall I add?
		int iParamType = rand() % 6;
		switch(iParamType)
		{
		case 0:
			{
				// Bitfield
				CBitfieldParameter* pobBitfieldParam = new CBitfieldParameter;
				pobBitfieldParam->SetName(GetRandomString());
				pobBitfieldParam->SetDefaultValue(rand());
				pobBitfieldParam->SetDescription("Randomly generated bitfield");
				m_obVectorOfParameters.push_back(pobBitfieldParam);
				break;
			}
		case 1:
			{
				// Float
				CFloatParameter* pobFloatParam = new CFloatParameter;
				pobFloatParam->SetName(GetRandomString());
				pobFloatParam->SetDefaultValue(0.01f * rand());
				pobFloatParam->SetDescription("Randomly generated Float");
				m_obVectorOfParameters.push_back(pobFloatParam);
				break;
			}
		case 2:
			{
				// Int
				CIntParameter* pobIntParam = new CIntParameter;
				pobIntParam->SetName(GetRandomString());
				pobIntParam->SetDefaultValue(rand());
				pobIntParam->SetDescription("Randomly generated Int");
				m_obVectorOfParameters.push_back(pobIntParam);
				break;
			}
		case 3:
			{
				// String
				CStringParameter* pobStringParam = new CStringParameter;
				pobStringParam->SetName(GetRandomString());
				pobStringParam->SetDefaultValue(GetRandomString());
				pobStringParam->SetDescription("Randomly generated String");
				m_obVectorOfParameters.push_back(pobStringParam);
				break;
			}
		case 4:
			{
				// Entity Type
				CEntityType* pobEntityTypeParam = new CEntityType;
				pobEntityTypeParam->RandomlyGenerateATestEntityType();
				pobEntityTypeParam->SetDescription("Randomly generated entity type");
				m_obVectorOfParameters.push_back(pobEntityTypeParam);
				break;
			}
		case 5:
			{
				// Node
				CNodeParameter* pobNodeParam = new CNodeParameter;
				pobNodeParam->SetName(GetRandomString());
				pobNodeParam->SetDescription("Randomly generated Node");
				m_obVectorOfParameters.push_back(pobNodeParam);
				break;
			}
		}
	}
}

vector<string> CEntityType::GetParameterInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CParameter::GetParameterInfoAsStringVector();

	m_obReturnMe.push_back("parameter:noofparameters");
	char acBuffer[32];
	sprintf(acBuffer, "%d", GetParameters().size());
	m_obReturnMe.push_back(acBuffer);

	m_obReturnMe.push_back("parameter:parameternames");
	for(unsigned i=0; i<GetParameters().size(); i++) m_obReturnMe.push_back(GetParameters()[i]->GetName());

	return m_obReturnMe;
}


const CParameter* CEntityType::GetParameter(const string& strParameterName) const 
{
	for(unsigned i=0; i<GetParameters().size(); i++)
	{
		if(GetParameters()[i]->GetName() == strParameterName)
		{
			return GetParameters()[i];
		}
	}
	return NULL;
}

