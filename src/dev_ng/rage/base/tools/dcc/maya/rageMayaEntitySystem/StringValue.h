// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parser/manager.h"
#include "value.h"
#include "stringparameter.h"
#include <string>

using namespace std;
using namespace rage;

class CStringValue : public CValue
{
public:
	CStringValue();
	virtual ~CStringValue(void);

	// Accessors/Modifers
	virtual const CStringParameter* GetParameter() const {return (CStringParameter*)CValue::GetParameter();}

	string GetValue() const;
	void SetValue(string iValue) {m_strValue = iValue; ProvidedValue();}
	virtual vector<string> GetValueInfoAsStringVector() const;

protected:

private:
	string		m_strValue;
	PAR_PARSABLE();
};
