// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parameter.h"
#include "parser/manager.h"

using namespace rage;

class CIntParameter : public CParameter
{
public:
	CIntParameter(void);
	virtual ~CIntParameter(void);

	// Accessors/Modifers
	int GetDefaultValue() const { return m_iDefaultValue;	}
	void SetDefaultValue(const int iDefaultValue) { m_iDefaultValue = iDefaultValue;	}
	int GetMinValue() const { return m_iMinValue;	}
	void SetMinValue(const int iMinValue) { m_iMinValue = iMinValue;	}
	int GetMaxValue() const { return m_iMaxValue;	}
	void SetMaxValue(const int iMaxValue) { m_iMaxValue = iMaxValue;	}
	virtual vector<string> GetParameterInfoAsStringVector() const;

private:
	int m_iDefaultValue;
	int m_iMinValue;
	int m_iMaxValue;

	PAR_PARSABLE();
};
