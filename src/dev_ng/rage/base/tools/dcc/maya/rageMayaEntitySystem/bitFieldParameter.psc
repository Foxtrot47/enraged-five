<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef base="CParameter" type="CBitfieldParameter">
<int name="m_iDefaultValue"/>
<!--
std::strings and std::vectors are no longer supported, and this causes errors when doing a rebuild_parser_files.rb pass
<array name="m_obVectorOfBitLabels" type="std::vector">
	<string type="std::string"/>
</array>
-->
</structdef>

</ParserSchema>