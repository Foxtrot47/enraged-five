// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parser/manager.h"
#include "value.h"
#include "entitytype.h"
#include <string>

using namespace std;
using namespace rage;

class CEntityValue : public CValue
{
public:
	CEntityValue();
	virtual ~CEntityValue(void);

	// Accessors/Modifers
	string					GetName() const;
	void					SetName(const string& iValue) {m_strName = iValue; ProvidedValue();}
	void					ConnectUpPointersAfterLoad_PostLoadCallback();
	virtual void			ConnectUpPointersAfterLoad(const CValue* pobParentValue = NULL);
	virtual void			SetParameter(const CParameter* pobParameterValueIsBasedOn);
	virtual const CEntityType* GetParameter() const {return (CEntityType*)CValue::GetParameter();}
	virtual vector<string>	GetValueInfoAsStringVector() const;
	const	CValue*			GetValue(const string& strValueName) const;
	const vector<CValue*>&		GetValues() const	{return m_obVectorOfParameterValues;}
	vector<CValue*>&			GetValues()			{return m_obVectorOfParameterValues;}

protected:

private:
	string					m_strName;
	vector<CValue*>		m_obVectorOfParameterValues;
	PAR_PARSABLE();
};
