// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parser/manager.h"

#include <vector>
#include <string>

using namespace std;
using namespace rage;

class CParameter
{
public:
	CParameter(void);
	virtual ~CParameter(void);

	// Accessors/Modifers
	const string& GetName() const { return m_strName;	}
	void SetName(const string& strName) { m_strName = strName;	}
	const string& GetDescription() const { return m_strDescription;	}
	void SetDescription(const string& strDescription) { m_strDescription = strDescription;	}
	const string& GetParameterType() const { return m_strParameterType;	}
	virtual vector<string> GetParameterInfoAsStringVector() const;

protected:
	void SetParameterType(const string& strParameterType) { m_strParameterType = strParameterType;	}

private:
	string m_strName;
	string m_strDescription;
	string m_strParameterType;

	PAR_PARSABLE();
};
