// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "EntityType.h"
#include "vector"

using namespace std;

class CEntityTypesDatabase
{
public:
	CEntityTypesDatabase(void);
	~CEntityTypesDatabase(void);

	// Accessors and Modifiers
	void FillFromFolder(string strFolder);
	void AddEntityType(CEntityType* obEntityType) {m_obVectorOfAllPossibleEntityTypes.push_back(obEntityType);}
	bool IsEmpty() const {return (m_obVectorOfAllPossibleEntityTypes.size() == 0);}
	int	GetNumberOfEntityTypes() const {return m_obVectorOfAllPossibleEntityTypes.size();}
	const CEntityType* GetEntityType(int iTypeNo) const {return m_obVectorOfAllPossibleEntityTypes[iTypeNo];}
	const CEntityType* GetEntityType(const string& strTypeName) const;

	// Operators
	void Dump(const string& strPath = "C:/temp/") const;

private:
	// CEntityType* GetEntityType(int iTypeNo) {return m_obVectorOfAllPossibleEntityTypes[iTypeNo];}
	vector<CEntityType*> m_obVectorOfAllPossibleEntityTypes;
};
