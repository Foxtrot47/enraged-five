<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef base="CParameter" type="CEntityType">
<!-- 
std::strings and std::vectors are no longer supported, and this causes errors when doing a rebuild_parser_files.rb pass
<array name="m_obVectorOfParameters" type="std::vector">
	<pointer type="CParameter" policy="owner"/>
</array>
-->
</structdef>

</ParserSchema>