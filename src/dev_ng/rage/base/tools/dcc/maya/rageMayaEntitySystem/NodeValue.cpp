// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "nodevalue.h"
#include "nodevalue_parser.h"

#include <iostream>
using namespace std;


CNodeValue::CNodeValue()
: CValue()
{
	SetValueType("node");
}

CNodeValue::~CNodeValue(void)
{
}

vector<string> CNodeValue::GetValueInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CValue::GetValueInfoAsStringVector();
	return m_obReturnMe;
}

