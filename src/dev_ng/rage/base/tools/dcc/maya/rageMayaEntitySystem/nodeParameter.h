// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parameter.h"
#include "parser/manager.h"

using namespace rage;

class CNodeParameter : public CParameter
{
public:
	CNodeParameter(void);
	virtual ~CNodeParameter(void);

	// Accessors/Modifers
	const vector<string>&	GetVectorOfAttributesToExport() const {return m_obVectorOfAttributesToExport;}
	vector<string>	GetVectorOfAttributesToExport() {return m_obVectorOfAttributesToExport;}
	const string&	GetAttributeToExport(unsigned iAttrNo) const {return m_obVectorOfAttributesToExport[iAttrNo];}
	virtual vector<string> GetParameterInfoAsStringVector() const;

private:
	PAR_PARSABLE();
	vector<string>	m_obVectorOfAttributesToExport;
};
