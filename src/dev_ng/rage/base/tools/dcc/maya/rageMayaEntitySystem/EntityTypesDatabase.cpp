// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "file/device.h"
#include "entitytypesdatabase.h"
#include "parser/manager.h"

#include <string>
#include <iostream>
using namespace std;
using namespace rage;

CEntityTypesDatabase::CEntityTypesDatabase(void)
{
}

CEntityTypesDatabase::~CEntityTypesDatabase(void)
{
	for(unsigned i=0; i<m_obVectorOfAllPossibleEntityTypes.size(); i++)
	{
		delete m_obVectorOfAllPossibleEntityTypes[i];
	}
}

void CEntityTypesDatabase::FillFromFolder(string strFolder)
{
	// Get contents of folder
	const fiDevice* pobDevice = fiDevice::GetDevice(strFolder.c_str(),true);
	if (pobDevice) 
	{
		fiFindData data;
		fiHandle handle = pobDevice->FindFileBegin(strFolder.c_str(),data);
		int iNoOfEntityTypesBefore = m_obVectorOfAllPossibleEntityTypes.size();
		if (fiIsValidHandle(handle)) 
		{
			do 
			{
				const char *pcExtension = strrchr(data.m_Name, '_');
				if (pcExtension && stricmp(pcExtension, "_entitytype.xml") == 0) 
				{
					// Bingo, found an Entity Type file
					cout<<"About to load entity "<<data.m_Name<<endl;
					CEntityType* pobEntityType = NULL;
					PARSER.LoadObjectPtr((strFolder + data.m_Name).c_str(), "", pobEntityType);
					cout<<"Loaded entity "<<flush<<pobEntityType->GetName()<<endl;
					AddEntityType(pobEntityType);
				}
			} while (pobDevice->FindFileNext(handle,data));
			pobDevice->FindFileEnd(handle);
		}
		cout<<(m_obVectorOfAllPossibleEntityTypes.size() - iNoOfEntityTypesBefore)<<" entity types added"<<endl;
	}
	else
	{
		cout<<"Error, unable to find folder "<<strFolder<<endl;
	}
}

void CEntityTypesDatabase::Dump(const string& strPath) const
{
	for(unsigned i=0; i<m_obVectorOfAllPossibleEntityTypes.size(); i++)
	{
		string strFilename(strPath +"/"+ m_obVectorOfAllPossibleEntityTypes[i]->GetName());
		PARSER.SaveObject((strFilename +"_entitytype").c_str(), "xml", m_obVectorOfAllPossibleEntityTypes[i]);
	}
}

const CEntityType* CEntityTypesDatabase::GetEntityType(const string& strTypeName) const 
{
	for(unsigned i=0; i<m_obVectorOfAllPossibleEntityTypes.size(); i++)
	{
		if(m_obVectorOfAllPossibleEntityTypes[i]->GetName() == strTypeName)
		{
			return m_obVectorOfAllPossibleEntityTypes[i];
		}
	}
	return NULL;
}
