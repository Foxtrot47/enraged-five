// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parameter.h"
#include "parser/manager.h"

using namespace rage;

class CStringParameter : public CParameter
{
public:
	CStringParameter(void);
	virtual ~CStringParameter(void);

	// Accessors/Modifers
	string GetDefaultValue() const { return m_strDefaultValue;	}
	void SetDefaultValue(const string iDefaultValue) { m_strDefaultValue = iDefaultValue;	}
	virtual vector<string> GetParameterInfoAsStringVector() const;

private:
	string m_strDefaultValue;

	PAR_PARSABLE();
};
