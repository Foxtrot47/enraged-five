// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parser/manager.h"
#include "value.h"
#include "intparameter.h"
#include <string>

using namespace std;
using namespace rage;

class CIntValue : public CValue
{
public:
	CIntValue();
	virtual ~CIntValue(void);

	// Accessors/Modifers
	virtual const CIntParameter* GetParameter() const {return (CIntParameter*)CValue::GetParameter();}

	int GetValue() const;
	void SetValue(int iValue) {m_iValue = iValue; ProvidedValue();}
	virtual vector<string> GetValueInfoAsStringVector() const;

protected:

private:
	int		m_iValue;
	PAR_PARSABLE();
};
