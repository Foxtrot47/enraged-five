// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "file/device.h"
#include "entitytypesdatabase.h"
#include "entitiesdatabase.h"
#include "parser/manager.h"

#include <string>
#include <iostream>
using namespace std;
using namespace rage;

CEntitiesDatabase* CEntitiesDatabase::m_pobLastCreatedEntitiesDatabase = NULL;

CEntitiesDatabase::CEntitiesDatabase(CEntityTypesDatabase*	pobEntityTypesDatabase)
: m_pobEntityTypesDatabase(pobEntityTypesDatabase)
{
	m_pobLastCreatedEntitiesDatabase = this;
}

CEntitiesDatabase::~CEntitiesDatabase(void)
{
	for(unsigned i=0; i<m_obVectorOfAllAvailableEntities.size(); i++)
	{
		delete m_obVectorOfAllAvailableEntities[i];
	}
}

void CEntitiesDatabase::FillFromFolder(string strFolder)
{
	// Get contents of folder
	const fiDevice* pobDevice = fiDevice::GetDevice(strFolder.c_str(),true);
	if (pobDevice) 
	{
		fiFindData data;
		fiHandle handle = pobDevice->FindFileBegin(strFolder.c_str(),data);
		int iNoOfEntitiesBefore = m_obVectorOfAllAvailableEntities.size();
		if (fiIsValidHandle(handle)) 
		{
			do 
			{
				const char *pcExtension = strrchr(data.m_Name, '_');
				if (pcExtension && stricmp(pcExtension, "_entity.xml") == 0) 
				{
					// Bingo, found an Entity Type file
					cout<<"About to load entity "<<data.m_Name<<endl;
					CEntityValue* pobEntity = NULL;
					PARSER.LoadObjectPtr((strFolder + data.m_Name).c_str(), "", pobEntity);
					cout<<"Loaded entity "<<flush<<pobEntity->GetName()<<endl;
					AddEntity(pobEntity);
				}
			} while (pobDevice->FindFileNext(handle,data));
			pobDevice->FindFileEnd(handle);
		}
		cout<<(m_obVectorOfAllAvailableEntities.size() - iNoOfEntitiesBefore)<<" entity types added"<<endl;
	}
	else
	{
		cout<<"Error, unable to find folder "<<strFolder<<endl;
	}
}

void CEntitiesDatabase::Dump(const string& strPath) const
{
	for(unsigned i=0; i<m_obVectorOfAllAvailableEntities.size(); i++)
	{
		string strFilename(strPath +"/"+ m_obVectorOfAllAvailableEntities[i]->GetName());
		CEntityValue* pobEntityToDump = m_obVectorOfAllAvailableEntities[i];
		cout<<"About to dump "<<pobEntityToDump->GetName()<<endl;
		PARSER.SaveObject((strFilename +"_entity").c_str(), "xml", pobEntityToDump);
	}
}

void CEntitiesDatabase::CreateRandomEntity()
{
	int iEntityTypeToCreate = rand() % GetEntityTypesDatabase()->GetNumberOfEntityTypes();
	CEntityValue* pobEntityToAdd = new CEntityValue();
	pobEntityToAdd->SetParameter(GetEntityTypesDatabase()->GetEntityType(iEntityTypeToCreate));
	char acBuffer[32]; sprintf(acBuffer, "RandomEntity%d", rand() % 9);
	pobEntityToAdd->SetName(acBuffer);

	// Add to database
	AddEntity(pobEntityToAdd);
}

const CEntityValue* CEntitiesDatabase::GetEntity(const string& strTypeName) const 
{
	for(unsigned i=0; i<m_obVectorOfAllAvailableEntities.size(); i++)
	{
		if(m_obVectorOfAllAvailableEntities[i]->GetName() == strTypeName)
		{
			return m_obVectorOfAllAvailableEntities[i];
		}
	}
	return NULL;
}
