#pragma once

class CMayaAttributeValue
{
public:
	CMayaAttributeValue(void);
	virtual ~CMayaAttributeValue(void);

private:
	string		m_strAttributeName;
	string		m_strAttributeValue;

	// Useful stuff
	string		m_strNodeName;
	string		m_strScenePathAndFilename;
};
