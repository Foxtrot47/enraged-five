// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parameter.h"
#include "parser/manager.h"

using namespace rage;

class CMeshParameter : public CParameter
{
public:
	CMeshParameter(void);
	virtual ~CMeshParameter(void);

	// Accessors/Modifers
	virtual vector<string> GetParameterInfoAsStringVector() const;

private:
	PAR_PARSABLE();
};
