// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "floatparameter.h"
#include "floatparameter_parser.h"


CFloatParameter::CFloatParameter(void)
: CParameter()
{
	SetDefaultValue(0);
	SetMinValue(0);
	SetMaxValue(0);
	SetName("");
	SetDescription("");
	SetParameterType("float");
}

CFloatParameter::~CFloatParameter(void)
{
}


vector<string> CFloatParameter::GetParameterInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CParameter::GetParameterInfoAsStringVector();
	char acBuffer[32];

	m_obReturnMe.push_back("parameter:default");
	sprintf(acBuffer, "%f", GetDefaultValue());
	m_obReturnMe.push_back(acBuffer);

	m_obReturnMe.push_back("parameter:min");
	sprintf(acBuffer, "%f", GetMinValue());
	m_obReturnMe.push_back(acBuffer);

	m_obReturnMe.push_back("parameter:max");
	sprintf(acBuffer, "%f", GetMaxValue());
	m_obReturnMe.push_back(acBuffer);

	return m_obReturnMe;
}


