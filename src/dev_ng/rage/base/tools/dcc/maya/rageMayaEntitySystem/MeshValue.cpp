// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "meshvalue.h"
#include "meshvalue_parser.h"

#include <iostream>
using namespace std;


CMeshValue::CMeshValue()
: CValue()
{
	SetValueType("mesh");
}

CMeshValue::~CMeshValue(void)
{
}

vector<string> CMeshValue::GetValueInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CValue::GetValueInfoAsStringVector();
	return m_obReturnMe;
}

