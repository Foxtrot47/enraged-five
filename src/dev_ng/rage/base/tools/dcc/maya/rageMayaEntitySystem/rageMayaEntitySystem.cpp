// 
// /rageMayaEntitySystem.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#define _BOOL
#define TRUE_AND_FALSE_DEFINED

// specify additional libraries to link to
#pragma comment(lib, "Foundation.lib")
#pragma comment(lib, "OpenMaya.lib")
#pragma comment(lib, "OpenMayaAnim.lib")
#pragma comment(lib, "OpenMayaUI.lib")

// YOU NEED THESE TWO PRAGMAS OR RAGE AND MAYA DON'T GET ON!!!
#pragma warning(disable: 4668)
#pragma warning(disable: 4100)
#include <maya/MSimple.h> 
#include <maya/MGlobal.h> 

#include "parser/manager.h"
#include "Parameter.h"
#include "StringParameter.h"
#include "bitfieldParameter.h"
#include "intParameter.h"
#include "floatParameter.h"
#include "nodeParameter.h"
#include "EntityType.h"
  
#include "Value.h"
#include "StringValue.h"
#include "bitfieldValue.h"
#include "intValue.h"
#include "floatValue.h"
#include "nodeValue.h"
#include "Entity.h"

#include "EntityTypesDatabase.h"
#include "EntitiesDatabase.h"

#include <iostream>
using namespace std;
using namespace rage;

// Use helper macro to register a command with Maya.  It creates and
// registers a command that does not support undo or redo.  The 
// created class derives off of MPxCommand. 
// 
DeclareSimpleCommand( rageMayaEntitySystem, "rage", "6.0");

MStatus rageMayaEntitySystem::doIt( const MArgList& args )
//
//	Description:
//		implements the MEL rageMayaEntitySystem command.
//
//	Arguments:
//		args - the argument list that was passes to the command from MEL
//
//	Return Value:
//		MS::kSuccess - command succeeded
//		MS::kFailure - command failed (returning this value will cause the 
//                     MEL script that is being run to terminate unless the
//                     error is caught using a "catch" statement.
//
{
	MStatus stat = MS::kSuccess;
//	MString obStrFilename;
//	obStrFilename = args.asString(0, &stat);
//	cout<<"I am doing something "<<__FILE__<<":"<<__LINE__<<endl;

	INIT_PARSER;

	REGISTER_PARSABLE_CLASS(CParameter);
	REGISTER_PARSABLE_CLASS(CIntParameter);
	REGISTER_PARSABLE_CLASS(CFloatParameter);
	REGISTER_PARSABLE_CLASS(CStringParameter);
	REGISTER_PARSABLE_CLASS(CBitfieldParameter);
	REGISTER_PARSABLE_CLASS(CEntityType);
	REGISTER_PARSABLE_CLASS(CNodeParameter);

	REGISTER_PARSABLE_CLASS(CValue);
	REGISTER_PARSABLE_CLASS(CIntValue);
	REGISTER_PARSABLE_CLASS(CFloatValue);
	REGISTER_PARSABLE_CLASS(CStringValue);
	REGISTER_PARSABLE_CLASS(CBitfieldValue);
	REGISTER_PARSABLE_CLASS(CEntityValue);
	REGISTER_PARSABLE_CLASS(CNodeValue);

	CEntityTypesDatabase obEntityTypesDatabase;

	// Build the entity system
	if(obEntityTypesDatabase.IsEmpty())
	{
		// Fill in the vector of possible types
		obEntityTypesDatabase.FillFromFolder("T:/EntitySystem/EntityTypes/");
		obEntityTypesDatabase.Dump();
	}
 
	CEntitiesDatabase obEntitiesDatabase(&obEntityTypesDatabase);

	// Build the entity system
	if(obEntitiesDatabase.IsEmpty())
	{
		// Fill in the vector of possible types
		obEntitiesDatabase.FillFromFolder("T:/EntitySystem/Entities/");

		// Create an entity
		obEntitiesDatabase.CreateRandomEntity();

		obEntitiesDatabase.Dump();
	}

	// Parse the arguments
	if(args.length() > 0)
	{
		// Given an argument, so work out what to do about it
		if(args.asString(0) == "-getEntityTypes")
		{
			// Return a list of all the entity types
			MStringArray obAStrReturnMe;
			for(int i=0; i<obEntityTypesDatabase.GetNumberOfEntityTypes(); i++)
			{
				obAStrReturnMe.append(obEntityTypesDatabase.GetEntityType(i)->GetName().c_str());
			}
			setResult( obAStrReturnMe );
		}
		else if(args.asString(0) == "-getEntities")
		{
			// Return a list of all the entities
			MStringArray obAStrReturnMe;
			for(int i=0; i<obEntitiesDatabase.GetNumberOfEntities(); i++)
			{
				obAStrReturnMe.append(obEntitiesDatabase.GetEntity(i)->GetName().c_str());
			}
			setResult( obAStrReturnMe );
		}
		else if(args.asString(0) == "-getParameterInfo")
		{
			// Get all the info I can about a parameter
			// Next arg, should be parameters name (and path)
			MString obStrParameterNameAndPath = args.asString(1);

			// Tokenize into path parts
			MStringArray obAStrParameterNameAndPathParts;
			obStrParameterNameAndPath.split('/', obAStrParameterNameAndPathParts);

			// First part is always entity type
			MString obStrEntityType = obAStrParameterNameAndPathParts[0];

			// Find entity type
			const CParameter* pobCurrentParameter = obEntityTypesDatabase.GetEntityType(obStrEntityType.asChar());

			// Parse the rest of the path
			for(unsigned i=1; i<obAStrParameterNameAndPathParts.length(); i++)
			{
				const CEntityType* pobCurrentEntityType = (const CEntityType*)pobCurrentParameter;
				pobCurrentParameter = pobCurrentEntityType->GetParameter(obAStrParameterNameAndPathParts[i].asChar());
			}
			
			// Ok, go the parameter, so get its info
			vector<string> astrParameterInfo = pobCurrentParameter->GetParameterInfoAsStringVector();
			MStringArray obAStrReturnMe;
			for(unsigned i=0; i<astrParameterInfo.size(); i++)
			{
				obAStrReturnMe.append(astrParameterInfo[i].c_str());
			}
			setResult( obAStrReturnMe );
		}
		else if(args.asString(0) == "-getValueInfo")
		{
			// Get all the info I can about a Value
			// Next arg, should be Values name (and path)
			MString obStrValueNameAndPath = args.asString(1);

			// Tokenize into path parts
			MStringArray obAStrValueNameAndPathParts;
			obStrValueNameAndPath.split('/', obAStrValueNameAndPathParts);

			// First part is always entity value
			MString obStrEntityValue = obAStrValueNameAndPathParts[0];

			// Find entity type
			const CValue* pobCurrentValue = obEntitiesDatabase.GetEntity(obStrEntityValue.asChar());

			// Parse the rest of the path
			for(unsigned i=1; i<obAStrValueNameAndPathParts.length(); i++)
			{
				const CEntityValue* pobCurrentEntityType = (const CEntityValue*)pobCurrentValue;
				pobCurrentValue = pobCurrentEntityType->GetValue(obAStrValueNameAndPathParts[i].asChar());
			}

			// Ok, go the Value, so get its info
			vector<string> astrValueInfo = pobCurrentValue->GetValueInfoAsStringVector();
			MStringArray obAStrReturnMe;
			for(unsigned i=0; i<astrValueInfo.size(); i++)
			{
				obAStrReturnMe.append(astrValueInfo[i].c_str());
			}
			setResult( obAStrReturnMe );
		}
	}

	cout<<"About to shut down parser"<<endl;
	SHUTDOWN_PARSER;
	cout<<"Shut down parser"<<endl;

	return stat;
}

