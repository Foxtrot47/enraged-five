// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "intparameter.h"
#include "intparameter_parser.h"

CIntParameter::CIntParameter(void)
: CParameter()
{
	SetDefaultValue(0);
	SetMinValue(0);
	SetMaxValue(0);
	SetName("");
	SetDescription("");
	SetParameterType("int");
}

CIntParameter::~CIntParameter(void)
{
}

vector<string> CIntParameter::GetParameterInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CParameter::GetParameterInfoAsStringVector();
	char acBuffer[32];

	m_obReturnMe.push_back("parameter:default");
	sprintf(acBuffer, "%d", GetDefaultValue());
	m_obReturnMe.push_back(acBuffer);

	m_obReturnMe.push_back("parameter:min");
	sprintf(acBuffer, "%d", GetMinValue());
	m_obReturnMe.push_back(acBuffer);

	m_obReturnMe.push_back("parameter:max");
	sprintf(acBuffer, "%d", GetMaxValue());
	m_obReturnMe.push_back(acBuffer);

	return m_obReturnMe;
}


