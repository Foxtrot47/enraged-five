// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parser/manager.h"
#include "value.h"
#include "floatparameter.h"
#include <string>

using namespace std;
using namespace rage;

class CFloatValue : public CValue
{
public:
	CFloatValue();
	virtual ~CFloatValue(void);

	// Accessors/Modifers
	virtual const CFloatParameter* GetParameter() const {return (CFloatParameter*)CValue::GetParameter();}

	float GetValue() const;
	void SetValue(float fValue) {m_fValue = fValue; ProvidedValue();}
	virtual vector<string> GetValueInfoAsStringVector() const;

protected:

private:
	float		m_fValue;
	PAR_PARSABLE();
};
