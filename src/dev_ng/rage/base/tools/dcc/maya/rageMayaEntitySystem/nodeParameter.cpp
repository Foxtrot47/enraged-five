// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "nodeparameter.h"
#include "nodeparameter_parser.h"

#include <iostream>
using namespace std;


CNodeParameter::CNodeParameter(void)
: CParameter()
{
	SetParameterType("node");
}

CNodeParameter::~CNodeParameter(void)
{
}

vector<string> CNodeParameter::GetParameterInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CParameter::GetParameterInfoAsStringVector();
	return m_obReturnMe;
}


