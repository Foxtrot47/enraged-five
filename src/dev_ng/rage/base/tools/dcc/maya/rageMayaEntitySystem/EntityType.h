// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parameter.h"

class CEntityType : public CParameter
{
public:
	CEntityType(void);
	virtual ~CEntityType(void);

	// Modifiers and Accessors
	const vector<CParameter*>&		GetParameters() const	{return m_obVectorOfParameters;}
	vector<CParameter*>&			GetParameters()			{return m_obVectorOfParameters;}
	virtual vector<string> GetParameterInfoAsStringVector() const;
	const CParameter* GetParameter(const string& strParameterName) const;

	// Test stuff and nonsense
	const string& GetRandomString() const;
	void	RandomlyGenerateATestEntityType();

private:
	vector<CParameter*>		m_obVectorOfParameters;

	PAR_PARSABLE();
};
