// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "Entity.h"
#include <vector>

using namespace std;

class CEntityTypesDatabase;

class CEntitiesDatabase
{
public:
	CEntitiesDatabase(CEntityTypesDatabase*	pobEntityTypesDatabase);
	~CEntitiesDatabase(void);

	// Accessors and Modifiers
	void FillFromFolder(string strFolder);
	void AddEntity(CEntityValue* obEntity) {m_obVectorOfAllAvailableEntities.push_back(obEntity);}
	bool IsEmpty() const {return (m_obVectorOfAllAvailableEntities.size() == 0);}
	int	GetNumberOfEntities() const {return m_obVectorOfAllAvailableEntities.size();}
	const CEntityTypesDatabase* GetEntityTypesDatabase() const {return m_pobEntityTypesDatabase;}
	CEntityTypesDatabase* GetEntityTypesDatabase() {return m_pobEntityTypesDatabase;}
	const CEntityValue* GetEntity(int iTypeNo) const {return m_obVectorOfAllAvailableEntities[iTypeNo];}
	const CEntityValue* GetEntity(const string& strTypeName) const;

	static const CEntitiesDatabase* GetEntitiesDatabase() {return m_pobLastCreatedEntitiesDatabase;}

	// Operators
	void CreateRandomEntity();
	void Dump(const string& strPath = "C:/temp/") const;

private:
	CEntityTypesDatabase*	m_pobEntityTypesDatabase;
	vector<CEntityValue*> m_obVectorOfAllAvailableEntities;
	static CEntitiesDatabase* m_pobLastCreatedEntitiesDatabase;
};
