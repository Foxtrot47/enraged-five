// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "StringValue.h"
#include "bitfieldValue.h"
#include "intValue.h"
#include "floatValue.h"
#include "nodeValue.h"
#include "Entity.h"
#include "entity_parser.h"

#include <iostream>
using namespace std;


CEntityValue::CEntityValue()
: CValue()
{
	SetValueType("entity");
}

CEntityValue::~CEntityValue(void)
{
	for(unsigned i=0; i<m_obVectorOfParameterValues.size(); i++)
	{
		delete m_obVectorOfParameterValues[i];
	}
}

string CEntityValue::GetName() const 
{
	if(ValueHasBeenProvided())
	{
		return m_strName;
	}
	return "";
}

void	CEntityValue::ConnectUpPointersAfterLoad_PostLoadCallback()
{
	ConnectUpPointersAfterLoad(NULL);
}

void	CEntityValue::ConnectUpPointersAfterLoad(const CValue* pobParentValue)
{
	// First wire myself up
	CValue::ConnectUpPointersAfterLoad(pobParentValue);

	// Ok, so that is me wired up, now my children
	for(unsigned i=0; i<m_obVectorOfParameterValues.size(); i++)
	{
		m_obVectorOfParameterValues[i]->ConnectUpPointersAfterLoad(this);
	}
}

void CEntityValue::SetParameter(const CParameter* pobParameterValueIsBasedOn)
{
	// Deal with my parent class
	CValue::SetParameter(pobParameterValueIsBasedOn);

	// Next create all my parameters and wire them all up
	const CEntityType* pobEntityTypeValueIsBasedOn = (const CEntityType*)pobParameterValueIsBasedOn;

	for(unsigned i=0; i<pobEntityTypeValueIsBasedOn->GetParameters().size(); i++)
	{
		// What type is it?
		string strParameterType = pobEntityTypeValueIsBasedOn->GetParameters()[i]->GetParameterType();
		string strParameterName = pobEntityTypeValueIsBasedOn->GetParameters()[i]->GetName();

		// Do I already have a value for this parameter?
		CValue* pobParameterValue = NULL;
		for(unsigned j=0; j<m_obVectorOfParameterValues.size(); j++)
		{
			if((m_obVectorOfParameterValues[j]->GetParameterName() == strParameterName) && (m_obVectorOfParameterValues[j]->GetValueType() == strParameterType))
			{
				// Bingo, I have found what I am looking for
				pobParameterValue = m_obVectorOfParameterValues[j];
				break;
			}
		}
		if(!pobParameterValue)
		{
			// Parameter does not already exist, so create it
			if(strParameterType == "bitfield")
			{
				// Create a Bitfield value to go with this 
				pobParameterValue = new CBitfieldValue();
			}
			else if(strParameterType == "entity")
			{
				// Create a entity value to go with this 
				pobParameterValue = new CEntityValue();
			}
			else if(strParameterType == "float")
			{
				// Create a float value to go with this 
				pobParameterValue = new CFloatValue();
			}
			else if(strParameterType == "int")
			{
				// Create a int value to go with this 
				pobParameterValue = new CIntValue();
			}
			else if(strParameterType == "node")
			{
				// Create a node value to go with this 
				pobParameterValue = new CNodeValue();
			}
			else if(strParameterType == "string")
			{
				// Create a string value to go with this 
				pobParameterValue = new CStringValue();
			}
			else
			{
				// Error
				cout<<"Unknown parameter type found "<<strParameterType<<endl;
			}
			m_obVectorOfParameterValues.push_back(pobParameterValue);
		}
		pobParameterValue->SetParameter(pobEntityTypeValueIsBasedOn->GetParameters()[i]);
	}
}

vector<string> CEntityValue::GetValueInfoAsStringVector() const
{
	vector<string> m_obReturnMe = CValue::GetValueInfoAsStringVector();
	m_obReturnMe.push_back("value:name");
	m_obReturnMe.push_back(m_strName);

	return m_obReturnMe;
}

const CValue* CEntityValue::GetValue(const string& strValueName) const 
{
	for(unsigned i=0; i<GetValues().size(); i++)
	{
		if(GetValues()[i]->GetParameterName() == strValueName)
		{
			return GetValues()[i];
		}
	}
	return NULL;
}


