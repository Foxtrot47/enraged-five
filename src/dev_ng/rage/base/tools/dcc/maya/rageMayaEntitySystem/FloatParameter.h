// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parameter.h"
#include "parser/manager.h"

using namespace rage;

class CFloatParameter : public CParameter
{
public:
	CFloatParameter(void);
	virtual ~CFloatParameter(void);

	// Accessors/Modifers
	float GetDefaultValue() const { return m_fDefaultValue;	}
	void SetDefaultValue(const float iDefaultValue) { m_fDefaultValue = iDefaultValue;	}
	float GetMinValue() const { return m_fMinValue;	}
	void SetMinValue(const float iMinValue) { m_fMinValue = iMinValue;	}
	float GetMaxValue() const { return m_fMaxValue;	}
	void SetMaxValue(const float iMaxValue) { m_fMaxValue = iMaxValue;	}
	virtual vector<string> GetParameterInfoAsStringVector() const;

private:
	float m_fDefaultValue;
	float m_fMinValue;
	float m_fMaxValue;

	PAR_PARSABLE();
};
