// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parser/manager.h"
#include "parameter.h"
#include <string>

using namespace std;
using namespace rage;

class CValue
{
public:
	enum USES_FOR_VALUE
	{
		USE_ENTITY_TYPE_VALUE,
		USE_ENTITY_VALUE,
		USE_ENTITY_PROXY_VALUE,
	};

	CValue();
	virtual ~CValue(void);

	// Accessors/Modifers
	virtual const CParameter*	GetParameter()		const {return m_pobParameterValueIsBasedOn;}
	string						GetParameterName()	const {return m_strParameterName;}
	const CValue*				GetParentValue()	const {return m_pobParentValue;}

	bool						ValueHasBeenProvided() const {return m_bValueProvided;}

	virtual void ConnectUpPointersAfterLoad(const CValue* pobParentValue);
	virtual void SetParameter(const CParameter* pobParameterValueIsBasedOn) {m_pobParameterValueIsBasedOn = pobParameterValueIsBasedOn; m_strParameterName = pobParameterValueIsBasedOn->GetName();}
	const string& GetValueType() const { return m_strValueType;	}
	virtual vector<string> GetValueInfoAsStringVector() const;
	USES_FOR_VALUE	GetUsesForValue() const {return m_eUsesForValue;}


protected:
	void SetValueType(const string& strValueType) { m_strValueType = strValueType;	}
	void ProvidedValue() {m_bValueProvided = true;}
	void SetParentValue(const CValue* pobParentValue) {m_pobParentValue = pobParentValue;}

private:
	bool				m_bValueProvided;
	USES_FOR_VALUE		m_eUsesForValue;
	const CValue*		m_pobParentValue;
	const CParameter*	m_pobParameterValueIsBasedOn;
	string				m_strParameterName;
	string				m_strValueType;
	PAR_PARSABLE();
};
