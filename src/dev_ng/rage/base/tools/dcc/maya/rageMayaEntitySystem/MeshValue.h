// 
// rageMayaEntitySystemHelpers/entitiesdatabase.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#pragma once
#include "parser/manager.h"
#include "value.h"
#include "meshparameter.h"
#include <string>

using namespace std;
using namespace rage;

class CMeshValue : public CValue
{
public:
	CMeshValue();
	virtual ~CMeshValue(void);

	// Accessors/Modifers
	virtual const CMeshParameter* GetParameter() const {return (CMeshParameter*)CValue::GetParameter();}
	virtual vector<string> GetValueInfoAsStringVector() const;

protected:

private:
	PAR_PARSABLE();
};
