// 
// /rageMayaReduce.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __RAGE_MAYA_REDUCE_H_
#define __RAGE_MAYA_REDUCE_H_

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <maya/MVector.h>
#include <maya/MFnMesh.h>
#pragma warning(pop)

#include "rrapi/rrapi.h"

#include <list>
#include <vector>

#define MAX_VERTS_PER_POLY	16

class MVector;

namespace rage
{
//-----------------------------------------------------------------------------
	struct reducePolyAttr
	{
		MObject	shaderMObj;			//Reference to the shader assigned to a poly
		int		origFaceId;			//Index of the original poly
		int		origTriId;			//Index of the triangle within the original poly
	
		MVector origFaceNormal;		//Original face normal

		reducePolyAttr()
			: shaderMObj(MObject::kNullObj)
			, origFaceId(-1)
			, origTriId(-1)
		{
		};
	};
//-----------------------------------------------------------------------------
	class rageMayaReduce : public MPxCommand
	{
	public:
		rageMayaReduce();
		virtual         ~rageMayaReduce();

		static  void*	creator();
		static	MSyntax	newSyntax();

		virtual MStatus doIt ( const MArgList & args );
		virtual bool	isUndoable () const {return false;}

	public:
		//PURPOSE: Static callback for RR to retrieve mesh data from Maya
		static rr_bool_t extractPrimitives_Tris(void *rrHandle, void *userdata);
		static rr_bool_t extractPrimitives_Polys(void *rrHandle, void *userdata); 

		//PURPOSE: Static callback for RR to push reduced triangle data back into Maya.
		static rr_bool_t stuffPrimitives(void *rrHandle, void *userdata);

		//PURPOSE: Static callback for RR to determine if two triangles have the same surface parameters
		static float sameSurface(void *triangle1data, void *triangle2data, void *userdata);

		//PURPOSE: Static callback for RR to determine if two triangles have the same material
		static float sameMaterial(void *triangle1data, void *triangle2data, void *userdata);

		//PURPOSE: Static callback for RR to determine if two triangles have the same texture
		static float sameTexture(void *triangle1data, void *triangle2data, void *userdata);

		//PURPOSE: Static simple progress callback for RR
		static void	simpleProgress(float fraction, void* userdata);

		static void advProgress_newBlock(const char *const taskstring, const float fraction, void *const userdata);
		static void advProgress_progress(float fraction, void *userdata);

	private:
		//PURPOSE: Static method to thats checks Maya set membership for polys and verts from the mesh being reduced.
		//Output is two arrays of polygon and vertex indices sorted in ascending order.
		static int getComponentsMarkedForRetention(std::vector<int>& retPolyIdxArray, std::vector<int>& retVertexIdxArray);

		//static bool PerformMeshPostProcessing(MFnMesh& fnRdcMesh);
		static bool		postProcessEdgeSmoothing(MFnMesh& fnRdcMesh);
		static MStatus	postProcessVertexColors(MDagPath& rdcMeshDagPath);
		static MStatus	postProcessUvCoordinates(MDagPath& rdcMeshDagPath);
		
	private:
		static MDagPath	 m_DagPathToReduce;
		static MString	 m_DoNotReduceSetName;

		static bool		 m_bPreserveHierarchy;
		static bool		 m_bTransferVertexColors;

		static bool		 m_bPerformEdgeSmoothing;
		static float	 m_EdgeSmoothingAngle;

	};
//-----------------------------------------------------------------------------
} // end using namespace rage


#endif

