// 
// /pluginMain.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#pragma warning(pop)

#include "rageMayaReduce.h"

using namespace rage;

//-----------------------------------------------------------------------------

MStatus initializePlugin( MObject obj )
{
	MFnPlugin plugin( obj, "Alias|Wavefront", "6.5", "Any");
	plugin.registerCommand("rageMayaReduce", rageMayaReduce::creator, rageMayaReduce::newSyntax);
	return( MS::kSuccess );
}

//-----------------------------------------------------------------------------

MStatus uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );
	plugin.deregisterCommand("rageMayaReduce");
	return( MS::kSuccess );
}

//-----------------------------------------------------------------------------


