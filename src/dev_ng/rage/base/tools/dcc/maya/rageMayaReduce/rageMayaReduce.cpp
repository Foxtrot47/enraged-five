// 
// /rageMayaReduce.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>

#include <maya/MArgDatabase.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MDagModifier.h>
#include <maya/MDGModifier.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFnComponentListData.h>
#include <maya/MFnDagNode.h>
#include <maya/MFnMesh.h>
#include <maya/MFnSet.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MFnTransform.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MItMeshEdge.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MMatrix.h>
#include <maya/MObject.h>
#include <maya/MObjectArray.h>
#include <maya/MPlug.h>
#include <maya/MPointArray.h>
#include <maya/MProgressWindow.h>
#include <maya/MSelectionList.h>
#include <maya/MStatus.h>
#include <maya/MStringArray.h>
#pragma warning(pop)

#include <algorithm>
#include <assert.h>
#include <vector>
#include <list>
#include <set>

#include "float.h"
#include "math.h"

#include "rageMayaReduce.h"
#include "rmrDagHelper.h"

#ifdef _DEBUG
#pragma comment(lib, "..\\..\\..\\..\\..\\..\\rage\\base\\tools\\3rdParty\\rrapi-win32-1.2.4\\lib\\rrapi_mdd.lib")
#else
#pragma comment(lib, "..\\..\\..\\..\\..\\..\\rage\\base\\tools\\3rdParty\\rrapi-win32-1.2.4\\lib\\rrapi_md.lib")
#endif //_DEBUG

#ifndef PI
#define PI		(3.14159265358979323846264338327950288f)
#endif

#ifndef RtoD
#define RtoD	(180.0f/PI)
#endif

#ifndef DtoR
#define DtoR	(PI/180.0f)
#endif

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------

MStatus AttachMeshShader(const MObject& shadingGroup, const MObject& mesh, const MObject& components, uint instanceNumber);

//-----------------------------------------------------------------------------

template <class Type> inline Type Clamp(Type t,Type a,Type b)
{
	return (t < a) ? a : (t > b) ? b : t;
}

//-----------------------------------------------------------------------------

MDagPath	rageMayaReduce::m_DagPathToReduce;
MString		rageMayaReduce::m_DoNotReduceSetName("none");

bool		rageMayaReduce::m_bPreserveHierarchy(false);
bool		rageMayaReduce::m_bTransferVertexColors(false);

bool		rageMayaReduce::m_bPerformEdgeSmoothing(false);
float		rageMayaReduce::m_EdgeSmoothingAngle(60.0f);

//-----------------------------------------------------------------------------

rageMayaReduce::rageMayaReduce()
{
}

//-----------------------------------------------------------------------------

rageMayaReduce::~rageMayaReduce()
{
}

//-----------------------------------------------------------------------------

void* rageMayaReduce::creator()
{
	return new rageMayaReduce();
}

//-----------------------------------------------------------------------------

MSyntax rageMayaReduce::newSyntax()
{
	 MSyntax syntax;

	 syntax.addFlag("-pct", "-percent", MSyntax::kDouble);

	 syntax.addFlag("-bl", "-boundryLock");
	 syntax.addFlag("-fr", "-fastReduce");
	 syntax.addFlag("-eps", "-epsilon");
	 syntax.addFlag("-drs", "-dontReduceSet", MSyntax::kString);
	 syntax.addFlag("-adp", "-adaptive");
	 syntax.addFlag("-ph", "-preserveHier");
	 syntax.addFlag("-pv", "-preserveCpvs");
	 syntax.addFlag("-pes", "-ppEdgeSmoothing");

	 //Debug / development related flags
	 syntax.addFlag("-up", "-usepolys");
	
	 syntax.enableQuery(false);
	 syntax.enableEdit(false);

	 return syntax;
}

//-----------------------------------------------------------------------------

void rageMayaReduce::simpleProgress(float fraction, void* /*userdata*/)
{
	MProgressWindow::setProgress((int)(fraction*1000));
}

//-----------------------------------------------------------------------------

void rageMayaReduce::advProgress_newBlock(const char *const taskstring, const float /*fraction*/, void* /*const userdata*/)
{
	MProgressWindow::setProgressStatus(MString(taskstring));
}

//-----------------------------------------------------------------------------

void rageMayaReduce::advProgress_progress(float fraction, void* /*userdata*/)
{
	MProgressWindow::setProgress((int)(fraction*1000));
}

//-----------------------------------------------------------------------------

struct MeshReductionInfo
{
	MeshReductionInfo()
		: m_pctToReduce(0.0f)
		, m_triangleCount(-1)
	{};

	MeshReductionInfo(float pct, int triCount)
		: m_pctToReduce(pct)
		, m_triangleCount(triCount)
	{};

	float	m_pctToReduce;
	int		m_triangleCount;
};

MStatus rageMayaReduce::doIt ( const MArgList & args )
{
	MStatus status = MS::kSuccess;
	char msgBuf[512];
	MStringArray cmdResult;

	MSelectionList	reduceObjectList;
	double			pctReduction = 80.0f;

	//Parse the command arguments
	MArgDatabase argData(syntax(), args);

	//Get the amount to reduce the meshes by
	bool bPctSet = argData.isFlagSet("-percent", &status);
	if(bPctSet)
	{
		 argData.getFlagArgument("-percent", 0, pctReduction);
	}

	//Get the name of the set that contains polys marked for retention
	if(argData.isFlagSet("-dontReduceSet", &status))
	{
		MString drsName;
		argData.getFlagArgument("-dontReduceSet", 0, drsName);
		if(drsName.length())
			m_DoNotReduceSetName = drsName;
		else
			m_DoNotReduceSetName = "none";
	}
	else
		m_DoNotReduceSetName = "none";

	//Check if the adaptive reduction has been enabled
	bool bAdaptivePctReduction = argData.isFlagSet("-adaptive", &status);

	//Is boundary locking enabled?
	bool bBoundryLocking = argData.isFlagSet("-boundryLock");

	//Is Fast reduction enabled?
	bool bFastReduction = argData.isFlagSet("-fastReduce");

	//Did the user specify an epsilon value?
	double epsValue = 0.1f;
	bool bSetEpsilon = argData.isFlagSet("-epsilon");
	if(bSetEpsilon)
	{
		argData.getFlagArgument("-epsilon", 0, epsValue);
	}

	//Should we preserve the hierarchy?
	m_bPreserveHierarchy = argData.isFlagSet("-preserveHier");

	//Should we preserve the vertex colors of the original mesh?
	m_bTransferVertexColors = argData.isFlagSet("-preserveCpvs");

	//Shous we automatically adjust the edge hard/smooth flags?
	m_bPerformEdgeSmoothing = argData.isFlagSet("-ppEdgeSmoothing");
	double smoothingAngle;
	if(argData.getFlagArgument("-ppEdgeSmoothing",0,smoothingAngle) == MS::kSuccess)
	{
		m_EdgeSmoothingAngle = (float)smoothingAngle;
	}
	
	//If no objects were passed to the command then operate on the selected object set.
	argData.getObjects(reduceObjectList);
	if(!reduceObjectList.length())
	{
		MGlobal::getActiveSelectionList(reduceObjectList);
	}
	int nRdcObjects = reduceObjectList.length();


	//Establish the reduction percentages for each selected mesh
	vector<MeshReductionInfo*> meshReductionInfoArr;
	
	int totalSelectedTriangleCount = 0;
	int highMeshTriangleCount = -INT_MAX;
	int lowMeshTriangleCount = INT_MAX;

	for(int rdcObjectIdx=0; rdcObjectIdx<nRdcObjects; rdcObjectIdx++)
	{
		MDagPath rdcObjectMDagPath;
		reduceObjectList.getDagPath(rdcObjectIdx, rdcObjectMDagPath);

		//Count the number of triangles in the mesh (used for progress information)
		int triangleCount = 0;
		MItMeshPolygon polyIt(rdcObjectMDagPath);
		for( polyIt.reset() ; !polyIt.isDone(); polyIt.next() )
		{
			int polyTriCount;
			status = polyIt.numTriangles(polyTriCount);
			triangleCount += polyTriCount;
		}
		
		meshReductionInfoArr.push_back(new MeshReductionInfo((float)pctReduction, triangleCount));
		totalSelectedTriangleCount += triangleCount;

		if(triangleCount > highMeshTriangleCount)
			highMeshTriangleCount = triangleCount;
		
		if(triangleCount < lowMeshTriangleCount)
			lowMeshTriangleCount = triangleCount;
	}

	//If adaptive reduction percentages have been enabled, update the per mesh 
	//percentages to reflect this
	if(bAdaptivePctReduction)
	{
		float upperPctLimit = (float)pctReduction;
		float lowerPctLimit = ((float)pctReduction - 20.0f);

		for(int rdcObjectIdx=0; rdcObjectIdx<nRdcObjects; rdcObjectIdx++)
		{
			float lambda = ((float)(meshReductionInfoArr[rdcObjectIdx]->m_triangleCount - lowMeshTriangleCount) / (float)(highMeshTriangleCount - lowMeshTriangleCount));
			lambda = pow(lambda, 0.25f);
			float adaptReducePct = lowerPctLimit + (lambda * (upperPctLimit - lowerPctLimit));
			
			meshReductionInfoArr[rdcObjectIdx]->m_pctToReduce = adaptReducePct;

#ifdef _DEBUG
			char msgBuf[512];
			MDagPath rdcObjectMDagPath;
			reduceObjectList.getDagPath(rdcObjectIdx, rdcObjectMDagPath);
			MString objectName = rdcObjectMDagPath.partialPathName();
			sprintf_s(msgBuf, "Object '%s' will be reduced by %f percent.", objectName.asChar(), adaptReducePct);
			MGlobal::displayInfo(msgBuf);
#endif //_DEBUG
		}
	}
	
	bool bShowProgress = true;

	MGlobal::MMayaState curState = MGlobal::mayaState();
	if(curState == MGlobal::kBatch)
		bShowProgress = false;

	//Setup the maya progress window
	if(bShowProgress)
	{
		MProgressWindow::reserve();
		MProgressWindow::setTitle("Reducing Meshes...");
		MProgressWindow::setProgressMin(0);
		MProgressWindow::setProgressMax(1000);
		MProgressWindow::startProgress();
	}

	//Reduce the objects
	for(int rdcObjectIdx=0; rdcObjectIdx<nRdcObjects; rdcObjectIdx++)
	{
		MDagPath rdcObjectMDagPath;
		reduceObjectList.getDagPath(rdcObjectIdx, rdcObjectMDagPath);

		rdcObjectMDagPath.extendToShape();

		if(rdcObjectMDagPath.apiType() != MFn::kMesh)
		{
			MGlobal::displayError("Only able to reduce mesh objects.");
			return MS::kFailure;
		}

		m_DagPathToReduce = rdcObjectMDagPath;
	
		if(bShowProgress)
		{
			sprintf_s(msgBuf, "Reducing : %s", m_DagPathToReduce.partialPathName());
			MProgressWindow::setTitle(MString(msgBuf));
			MProgressWindow::setProgress(0);
		}
		
		//Initialze Rational Reducer
		void* rrHandle = rr_init(TRUE);

		//Establish the Rational Reducer callbacks...

		//We need to externally store the pointers to the triangle attribute structures since they are allocated
		//in the extract callback and used in the stuff callback.
		std::vector<reducePolyAttr*> m_polyAttrOwnerList;
		m_polyAttrOwnerList.reserve(meshReductionInfoArr[rdcObjectIdx]->m_triangleCount);
		
		if(argData.isFlagSet("-usepolys"))
		{
			rr_set_extract_primitives(rrHandle, rageMayaReduce::extractPrimitives_Polys, (void*)(&m_polyAttrOwnerList));
		}
		else
		{
			rr_set_extract_primitives(rrHandle, rageMayaReduce::extractPrimitives_Tris, (void*)(&m_polyAttrOwnerList));
		}
		
		MDagPath reducedDagPath;
		rr_set_stuff_primitives(rrHandle, rageMayaReduce::stuffPrimitives, (void*)(&reducedDagPath));

		//Set the triangle comparison callbacks
		rr_set_same_surfaceappearance(rrHandle,rageMayaReduce::sameSurface,NULL);
		rr_set_same_texture(rrHandle,rageMayaReduce::sameTexture,NULL);
		rr_set_same_material(rrHandle,rageMayaReduce::sameMaterial,NULL);

		if(bShowProgress)
		{
			//Set the progress callback
			rr_set_advanced_progress(rrHandle, advProgress_newBlock, advProgress_progress, NULL, NULL);
		}

		//Set the number of triangles in the source mesh
		rr_set_num_native_triangles(rrHandle, meshReductionInfoArr[rdcObjectIdx]->m_triangleCount);

		//Set the percentage to reduce
		rr_set_percentage(rrHandle, meshReductionInfoArr[rdcObjectIdx]->m_pctToReduce);

		//Set other misc options
		rr_set_lock_boundary(rrHandle, bBoundryLocking);
		rr_set_fast_reduction(rrHandle, bFastReduction);
		
		//rr_set_epsilon(rrHandle, (float)epsValue);

		//Reduce the model
		int ret = rr_reduce(rrHandle);
		if (ret!=RR_OK) 
		{
			MGlobal::displayError("Reduction error");
			status = MS::kFailure;
		}

		//Free the triangle attributes
		for(std::vector<reducePolyAttr*>::iterator it = m_polyAttrOwnerList.begin(); it != m_polyAttrOwnerList.end(); ++it)
			delete (*it);
		m_polyAttrOwnerList.clear();

		//Unitialize Rational Reducer
		rr_end(rrHandle);

		//Add the name of the reduced mesh to the list of reduced mesh which will be set as the result of the command
		cmdResult.append(reducedDagPath.fullPathName());
	}

	if(bShowProgress)
	{
		MProgressWindow::endProgress();
	}

	//General Cleanup..
	for(vector<MeshReductionInfo*>::iterator it=meshReductionInfoArr.begin();
		it != meshReductionInfoArr.end();
		++it)
	{
		delete (*it);
	}

	//If the reduction succeeded, set the result to the list of names of the reduced mesh transforms, and
	//select all of the reduced transforms
	if(status == MS::kSuccess)
	{
		setResult(cmdResult);
		
		MSelectionList resultSelection;
		for(unsigned i=0; i<cmdResult.length(); i++)
			resultSelection.add(cmdResult[i]);
		MGlobal::setActiveSelectionList( resultSelection, MGlobal::kReplaceList );
	}

	return status;
}

//-----------------------------------------------------------------------------

int rageMayaReduce::getComponentsMarkedForRetention(std::vector<int>& retPolyIdxArray, std::vector<int>& retVertexIdxArray)
{
	MStatus status;

	//Check for a set that contains polygons marked for preservation during reduction.
	MFnSet dontReduceFnSet;
	MSelectionList setSelList;
	status = setSelList.add(m_DoNotReduceSetName);
	if(status != MS::kSuccess)
	{
		//Failed to locate a set in the scene that specifies polys which should be retained during reduction
		return 0;
	}

	MObject dontReduceSetMObj;
	setSelList.getDependNode( 0, dontReduceSetMObj );
	status = dontReduceFnSet.setObject(dontReduceSetMObj);

	//Look through all the members of the set and see if the mesh we are currently reducing
	//is a member
	MObject dontReducePolysMObj(MObject::kNullObj);
	MObject dontReduceVertsMObj(MObject::kNullObj);

	MSelectionList setMembers;
	dontReduceFnSet.getMembers( setMembers, true );
	int nSetMembers = setMembers.length();
	int reduceMeshSetMemberIdx = -1;
	for(int i=0; i<nSetMembers; i++)
	{
		MDagPath memberDagPath;
		MObject	 memberComponentMObj;

		setMembers.getDagPath(i, memberDagPath, memberComponentMObj);
		if( !memberComponentMObj.isNull() )
		{
			if( memberComponentMObj.apiType()==MFn::kMeshPolygonComponent )
			{
				if(memberDagPath == m_DagPathToReduce)
				{
					dontReducePolysMObj = memberComponentMObj;
				}
			}
			else if( memberComponentMObj.apiType()==MFn::kMeshVertComponent )
			{
				if(memberDagPath == m_DagPathToReduce)
				{
					dontReduceVertsMObj = memberComponentMObj;
				}
			}
			else
			{
				char msgBuf[1024];
				sprintf_s(msgBuf,"Found components other than polygon components in set '%s', these will be ignored", m_DoNotReduceSetName.asChar());
				MGlobal::displayWarning(msgBuf);
			}
		}
		else
		{
			memberDagPath.extendToShape();
			if(memberDagPath == m_DagPathToReduce)
			{
				//The entire object is marked in the set
				return -1;
			}
		}
	}

	int retComponentCount = 0;
	
	if(!dontReducePolysMObj.isNull())
	{
		//The mesh is a member, so get a list of all the indices of polys that are part of the set
		MFnSingleIndexedComponent setIndexedComp( dontReducePolysMObj, &status );
		if(status != MS::kSuccess)
		{
			status.perror("In 'getComponentsMarkedForRetention', failed to create MFnSingleIndexedComponent object for polys");
			return 0;
		}

		//Move all the indices of the polygons marked for retention into the return vector of integers
		//and sort the list so we can do quick lookups in it during the process of adding polys/tris 
		//to the RR database.
		MIntArray setCompElementArray;
		setIndexedComp.getElements( setCompElementArray );
		int nComponents = setCompElementArray.length();
		retPolyIdxArray.reserve( nComponents );
		for(int i=0; i<nComponents; i++)
		{
			retPolyIdxArray.push_back( setCompElementArray[i] );
		}
		sort(retPolyIdxArray.begin(),retPolyIdxArray.end());

		//Return the number of poly indices marked in the set
		retComponentCount += (int)retPolyIdxArray.size();
	}

	if(!dontReduceVertsMObj.isNull())
	{
		MFnSingleIndexedComponent setIndexedComp( dontReduceVertsMObj, &status );
		if(status != MS::kSuccess)
		{
			status.perror("In 'getComponentsMarkedForRetention', failed to create MFnSingleIndexedComponent object for vertices");
			return 0;
		}

		//Move all the indices of the polygons marked for retention into the return vector of integers
		//and sort the list so we can do quick lookups in it during the process of adding polys/tris 
		//to the RR database.
		MIntArray setCompElementArray;
		setIndexedComp.getElements( setCompElementArray );
		int nComponents = setCompElementArray.length();
		retVertexIdxArray.reserve( nComponents );
		for(int i=0; i<nComponents; i++)
		{
			retVertexIdxArray.push_back( setCompElementArray[i] );
		}
		sort(retVertexIdxArray.begin(),retVertexIdxArray.end());

		//Return the number of poly indices marked in the set
		retComponentCount += (int)retVertexIdxArray.size();
	}

	return retComponentCount;
}

//-----------------------------------------------------------------------------
//This extraction callback extracts the primitive data from Maya passing the 
//polygons into the Rational Reduce polygon store (as opposed to passing in the
//triangulated poly information)
rr_bool_t rageMayaReduce::extractPrimitives_Polys(void *rrHandle, void* userdata)
{
	MStatus status;

	std::vector<reducePolyAttr*>& polyAttrOwnerList = *(static_cast<std::vector<reducePolyAttr*>*>(userdata));

	//Check for any polygons or vertices that have been marked for retention
	bool bCheckPolysForSetMembership = false;
	vector<int>	setPolyIdxMembers;

	bool bCheckVerticesForSetMembership = false;
	vector<int> setVertIdxMembers;

	int iRes = getComponentsMarkedForRetention(setPolyIdxMembers, setVertIdxMembers);
	if(iRes == -1)
	{
		//The entire mesh was marked for exclusion from reduction.
		return true;
	}
	else
	{
		if(setPolyIdxMembers.size())
			bCheckPolysForSetMembership = true;

#if 0	//Disable until support for this comes online from Systems in Motion
		if(setVertIdxMembers.size())
			bCheckVerticesForSetMembership = true;
#endif
	}

	MFnMesh fnMesh( m_DagPathToReduce, &status );
	if(status != MS::kSuccess)
	{
		status.perror("rageMayaReduce::extractPrimitives");
		return false;
	}

	MFloatPointArray vList;
	status = fnMesh.getPoints(vList, MSpace::kObject);
	int nVerts = vList.length();

	//Add the vertices
	rr_vertex_t **rrVertexArray = new rr_vertex_t*[nVerts];
	for (int i=0; i<nVerts; i++) 
	{
		rrVertexArray[i] = rr_add_vertex(rrHandle,
			vList[i].x,
			vList[i].y,
			vList[i].z);
	}

	//Collect information about the shaders assigned to the mesh
	int instanceNum = 0;
	if( m_DagPathToReduce.isInstanced() )
		instanceNum = m_DagPathToReduce.instanceNumber();

	MObjectArray	shaders;
	MIntArray		shaderIndices;
	fnMesh.getConnectedShaders(instanceNum, shaders, shaderIndices);

	int shaderSize = shaders.length();
	int shaderIndicesSize = shaderIndices.length();

	//Add the polys
	MItMeshPolygon polyIt(m_DagPathToReduce);

	int		flatIdxArr[MAX_VERTS_PER_POLY];
	float	flatTexCoordArr[MAX_VERTS_PER_POLY * 2];

	for( polyIt.reset() ; !polyIt.isDone(); polyIt.next() )
	{
		MIntArray vertIdxs;

		int polyIndex = polyIt.index();

		status = polyIt.getVertices(vertIdxs);
		vertIdxs.get(flatIdxArr);

		int polyVertCount = polyIt.polygonVertexCount();
		assert( polyVertCount < MAX_VERTS_PER_POLY );

		//Get the texture coordinates (Rational Reduce only supports a single UV set)
		bool bHasUVs = polyIt.hasUVs();
		if(bHasUVs)
		{
			MFloatArray UValArr, VValArr;
			polyIt.getUVs( UValArr, VValArr );
			int flatIdx = 0;
			for(int i=0; i<polyVertCount; i++)
			{
				flatTexCoordArr[flatIdx++] = UValArr[i];
				flatTexCoordArr[flatIdx++] = VValArr[i];
			}
		}
		else
		{
			memset(flatTexCoordArr, 0, (MAX_VERTS_PER_POLY*2*sizeof(float)));
		}

		//Build the polygon attribute structure
		reducePolyAttr* pAttr = new reducePolyAttr();
		polyAttrOwnerList.push_back(pAttr);

		//Store the original face id
		pAttr->origFaceId = polyIndex;

		//Store the original face normal
		polyIt.getNormal( pAttr->origFaceNormal, MSpace::kWorld );
		pAttr->origFaceNormal.normalize();

		//Set the shader
		if(shaderIndices[polyIndex] != -1)
		{
			int shaderIdx = shaderIndices[polyIndex];
			MObject shaderMObj = shaders[shaderIdx];
			pAttr->shaderMObj = shaderMObj;
		}
		else
		{
			pAttr->shaderMObj = MObject::kNullObj;
		}

		short rrFlags = 0;
		if(bCheckPolysForSetMembership)
		{
			//Only mark the triangle as reducable if the polygon it is part of is not included
			//in the list of polygon indices to keep from reducing.
			if( !binary_search(setPolyIdxMembers.begin(), setPolyIdxMembers.end(), polyIndex) )
				rrFlags = RR_REDUCABLE;
		}
		else
		{
			rrFlags = RR_REDUCABLE;
		}

		rr_add_indexed_polygon(rrHandle, 
			polyVertCount,
			flatIdxArr, 
			rrVertexArray, 
			pAttr, 
			flatTexCoordArr,
			rrFlags);
	}

	delete [] rrVertexArray;

	return true;
}

//-----------------------------------------------------------------------------
//This extraction callback extracts the primitive data from Maya passing 
//triangles into the Rational Reduce polygon store (as opposed to passing
//in the non-triangulated polygons).
rr_bool_t rageMayaReduce::extractPrimitives_Tris(void *rrHandle, void* userdata)
{
	MStatus status;

	std::vector<reducePolyAttr*>& polyAttrOwnerList = *(static_cast<std::vector<reducePolyAttr*>*>(userdata));

	//Check for any polygons or vertices that have been marked for retention
	bool bCheckPolysForSetMembership = false;
	vector<int>	setPolyIdxMembers;

	bool bCheckVerticesForSetMembership = false;
	vector<int> setVertIdxMembers;

	int iRes = getComponentsMarkedForRetention(setPolyIdxMembers, setVertIdxMembers);
	if(iRes == -1)
	{
		//The entire mesh was marked for exclusion from reduction
		return true;
	}
	else
	{
		if(setPolyIdxMembers.size())
			bCheckPolysForSetMembership = true;

#if 0	//Disable until support for this comes online from Systems in Motion
		if(setVertIdxMembers.size())
			bCheckVerticesForSetMembership = true;
#endif

	}
	
	MFnMesh fnMesh( m_DagPathToReduce, &status );
	if(status != MS::kSuccess)
	{
		status.perror("rageMayaReduce::extractPrimitives");
		return false;
	}

	MFloatPointArray vList;
	status = fnMesh.getPoints(vList, MSpace::kObject);
	int nVerts = vList.length();

	//Add the vertices
	rr_vertex_t **rrVertexArray = new rr_vertex_t*[nVerts];
	if(!bCheckVerticesForSetMembership)
	{
		for(int i=0; i<nVerts; i++) 
		{
			rrVertexArray[i] = rr_add_vertex(rrHandle,
				vList[i].x,
				vList[i].y,
				vList[i].z);
		}
	}
	else
	{
		for(int i=0; i<nVerts; i++)
		{
			float vertWeight = 0;
			if( binary_search(setVertIdxMembers.begin(), setVertIdxMembers.end(), i) )
				vertWeight = 255.0f;

			rrVertexArray[i] = rr_add_weighted_vertex(rrHandle,
				vList[i].x,
				vList[i].y,
				vList[i].z,
				vertWeight);
		}
	}

	//Collect information about the shaders assigned to the mesh
	int instanceNum = 0;
	if( m_DagPathToReduce.isInstanced() )
		instanceNum = m_DagPathToReduce.instanceNumber();

	MObjectArray	shaders;
	MIntArray		shaderIndices;
	fnMesh.getConnectedShaders(instanceNum, shaders, shaderIndices);

	int shaderSize = shaders.length();
	int shaderIndicesSize = shaderIndices.length();

	//Add the polys (using Maya's triangulation)
	MItMeshPolygon polyIt(m_DagPathToReduce);

	for( polyIt.reset() ; !polyIt.isDone(); polyIt.next() )
	{
		MIntArray vertIdxs;

		int polyIndex = polyIt.index();
		bool bPolyHasUVs = polyIt.hasUVs();

		MIntArray polygonVertexIndices;
		polyIt.getVertices(polygonVertexIndices);
		
		MPointArray dummyPointArr;
		MIntArray   triangleVertexIndices;
		polyIt.getTriangles( dummyPointArr, triangleVertexIndices, MSpace::kWorld);

		// Because getTriangles() returns the triangle-vertex-indices in local-triangle space
		// we need to convert triangle-vertex-indices to polygon-vertex-indices
		for (unsigned i = 0; i < triangleVertexIndices.length(); ++i)
		{
			unsigned n;
			for (n = 0; n < polygonVertexIndices.length(); ++n)
			{
				if (triangleVertexIndices[i] == polygonVertexIndices[n])
				{
					triangleVertexIndices[i] = n;
					break;
				}
			}
		}

		//Add each triangle to the RR triangle list
		int numPolyTris;
		status = polyIt.numTriangles(numPolyTris);

		for( int triIdx = 0; triIdx < numPolyTris; triIdx++)
		{
			int triIdxs[3];
			triIdxs[0] = polygonVertexIndices[ triangleVertexIndices[ (triIdx*3) + 0 ] ];
			triIdxs[1] = polygonVertexIndices[ triangleVertexIndices[ (triIdx*3) + 1 ] ];
			triIdxs[2] = polygonVertexIndices[ triangleVertexIndices[ (triIdx*3) + 2 ] ];

			float flatTexCoordArr[6];
			if(bPolyHasUVs)
			{
				float2 UV_V0, UV_V1, UV_V2;
				
				fnMesh.getPolygonUV( polyIndex, triangleVertexIndices[ (triIdx*3) + 0 ],  UV_V0[0],  UV_V0[1] );
				fnMesh.getPolygonUV( polyIndex, triangleVertexIndices[ (triIdx*3) + 1 ],  UV_V1[0],  UV_V1[1] );
				fnMesh.getPolygonUV( polyIndex, triangleVertexIndices[ (triIdx*3) + 2 ],  UV_V2[0],  UV_V2[1] );

				flatTexCoordArr[0] = UV_V0[0];
				flatTexCoordArr[1] = UV_V0[1];

				flatTexCoordArr[2] = UV_V1[0];
				flatTexCoordArr[3] = UV_V1[1];

				flatTexCoordArr[4] = UV_V2[0];
				flatTexCoordArr[5] = UV_V2[1];
				
			}
			else
			{
				memset(flatTexCoordArr, 0, (6 * sizeof(float)));
			}

			//Build the polygon attribute structure
			reducePolyAttr* pAttr = new reducePolyAttr();
			polyAttrOwnerList.push_back(pAttr);
			
			//Store the original face and triangle indices
			pAttr->origFaceId = polyIndex;
			pAttr->origTriId = triIdx;

			//Set the shader used by the poly
			if(shaderIndices[polyIndex] != -1)
			{
				int shaderIdx = shaderIndices[polyIndex];
				MObject shaderMObj = shaders[shaderIdx];
				pAttr->shaderMObj = shaderMObj;
			}
			else
			{
				pAttr->shaderMObj = MObject::kNullObj;
			}

			//Store the original face normal
			polyIt.getNormal( pAttr->origFaceNormal, MSpace::kWorld );
			pAttr->origFaceNormal.normalize();

			//Set the RR flags on the triangle
			short rrFlags = 0;
			if(bCheckPolysForSetMembership)
			{
				//Only mark the triangle as reducable if the polygon it is part of is not included
				//in the list of polygon indices to keep from reducing.
				if( !binary_search(setPolyIdxMembers.begin(), setPolyIdxMembers.end(), polyIndex) )
					rrFlags = RR_REDUCABLE;
			}
			else
				rrFlags = RR_REDUCABLE;

			//Add the triangle to the RR triangle list
			rr_add_indexed_polygon(rrHandle, 
				3,
				triIdxs, 
				rrVertexArray, 
				pAttr, 
				flatTexCoordArr,
				rrFlags);

		}//End for( int triIdx = 0...
	}//End for( polyIt.reset()...

	delete [] rrVertexArray;

	return true;
}

//-----------------------------------------------------------------------------

struct rmrShadedComponent
{
	MObject	shader;
	MObject	components;
	
	rmrShadedComponent()
		: shader(MObject::kNullObj)
		, components(MObject::kNullObj)
	{};
};

//-----------------------------------------------------------------------------

MStatus buildShadedComponentList(reducePolyAttr**& polyAttrArr, int polyCount, vector<rmrShadedComponent*>& shadedCompList)
{
	MStatus status = MS::kSuccess;
	MFnSingleIndexedComponent componentFn;

	for(int polyIdx=0; polyIdx<polyCount; polyIdx++)
	{
		//Look for an existing entry for the shader assigned to the current poly
		vector<rmrShadedComponent*>::iterator findIt;
		for( findIt = shadedCompList.begin(); findIt != shadedCompList.end(); ++findIt)
		{
			if( (*findIt)->shader == polyAttrArr[polyIdx]->shaderMObj)
				break;
		}
			
		if(findIt != shadedCompList.end())
		{
			//Found an entry for the shader
			rmrShadedComponent* pShCmp = *findIt;
			componentFn.setObject(pShCmp->components);
			componentFn.addElement(polyIdx);
		}
		else
		{
			//No entry exists for the given shader, so add one
			rmrShadedComponent* pShCmp = new rmrShadedComponent();
			pShCmp->shader = polyAttrArr[polyIdx]->shaderMObj;
			pShCmp->components = componentFn.create(MFn::kMeshPolygonComponent);
			componentFn.addElement(polyIdx);

			shadedCompList.push_back(pShCmp);
		}
	}

	return status;
}

//-----------------------------------------------------------------------------

MStatus assignPolyShaders(MObject rdcMeshMObj, reducePolyAttr**& polyAttrArr, int polyCount)
{
	MStatus status = MS::kSuccess;

	vector<rmrShadedComponent*> shadedCompList;
	status = buildShadedComponentList(polyAttrArr, polyCount, shadedCompList);

	MFnMesh rdcFnMesh(rdcMeshMObj);

	for(vector<rmrShadedComponent*>::iterator it = shadedCompList.begin();
		it != shadedCompList.end();
		++it)
	{
		rmrShadedComponent* pShCmp = (*it);
		AttachMeshShader( pShCmp->shader, rdcMeshMObj, pShCmp->components, 0);
	}

	//Clean-up
	for(vector<rmrShadedComponent*>::iterator it = shadedCompList.begin();
		it != shadedCompList.end();
		++it)
	{
		delete (*it);
	}

	return status;
}

//-----------------------------------------------------------------------------

rr_bool_t rageMayaReduce::stuffPrimitives(void *rrHandle, void* userdata)
{
	MStatus status;
	
	MDagPath *pResultDagPath = static_cast<MDagPath*>(userdata);

	//Create the Maya array of the reduced vertex positions
	int reducedVertCount = rr_get_num_vertices(rrHandle);
	MFloatPointArray mayaVertArray;
	mayaVertArray.setLength(reducedVertCount);
	for(int rvIdx=0; rvIdx < reducedVertCount; rvIdx++)
	{
		rr_vertex_t *const pReducedVert = rr_get_vertex(rrHandle, rvIdx);
		float *pVertPos = rr_vertex_get_coord(pReducedVert);
		mayaVertArray.set(rvIdx, pVertPos[0], pVertPos[1], pVertPos[2]);
	}
	
	int reducedTriCount = rr_get_num_triangles(rrHandle);
	if(reducedTriCount == 0)
	{
		//No data to build a mesh from...
		return true;
	}
	
	MIntArray mayaPolyCountArray;
	MIntArray mayaPolyConnectsArray;
	
	MFloatArray mayaTexCoordUArray;
	MFloatArray mayaTexCoordVArray;
	MIntArray   mayaUvIdsArray;
	MIntArray	mayaUvCountsArray;

	//Allocate an array to hold the poly attributes for each poly
	reducePolyAttr** polyAttrArr = new reducePolyAttr*[reducedTriCount];

	for(int rtIdx=0; rtIdx<reducedTriCount; rtIdx++) 
	{
		//Rational Reduced always returns triangles
		mayaPolyCountArray.append(3);
		mayaUvCountsArray.append(3);

		//Get the reduced triangle vertex indicies
		rr_triangle_t *const pReducedTri = rr_get_triangle(rrHandle,rtIdx);
		assert(pReducedTri);

		int v0 = rr_triangle_get_vertex_index( pReducedTri, 0);
		assert( v0 < reducedVertCount );
		int v1 = rr_triangle_get_vertex_index( pReducedTri, 1);
		assert( v1 < reducedVertCount );
		int v2 = rr_triangle_get_vertex_index( pReducedTri, 2);
		assert( v1 < reducedVertCount );
		
		mayaPolyConnectsArray.append( v0 );
		mayaPolyConnectsArray.append( v1 );
		mayaPolyConnectsArray.append( v2 );

		//Get the reduced triangle UV coordinates
		float* rdcUVCoords = rr_triangle_get_texcoords( pReducedTri );
		if(rdcUVCoords)
		{
			mayaTexCoordUArray.append( rdcUVCoords[0] );
			mayaTexCoordVArray.append( rdcUVCoords[1] );

			mayaTexCoordUArray.append( rdcUVCoords[2] );
			mayaTexCoordVArray.append( rdcUVCoords[3] );

			mayaTexCoordUArray.append( rdcUVCoords[4] );
			mayaTexCoordVArray.append( rdcUVCoords[5] );

			mayaUvIdsArray.append( (rtIdx*3) + 0 );
			mayaUvIdsArray.append( (rtIdx*3) + 1 );
			mayaUvIdsArray.append( (rtIdx*3) + 2 );
		}

		//Cache the attribute data for the triangle to be used later in the Maya mesh creation process
		polyAttrArr[rtIdx] = static_cast<reducePolyAttr*>( rr_triangle_get_data( pReducedTri ) );
	}

	MObject origMeshTransformMObj = m_DagPathToReduce.transform(&status);
	if(status != MS::kSuccess)
		status.perror("MDagPath::transform() failed.");
	
	MDagPath origMeshTransformMDagPath;
	status = MDagPath::getAPathTo(origMeshTransformMObj, origMeshTransformMDagPath);
	
	MFnTransform origMeshFnTransform(origMeshTransformMDagPath);

	MFnDependencyNode origMeshTransformFnDep(origMeshTransformMObj);
	MFnDependencyNode origMeshFnDep(m_DagPathToReduce.node());

	//Determine the name of the reduced mesh transform node
	MString rdcMeshTransformName = "reduced_";
	rdcMeshTransformName += origMeshTransformFnDep.name();

	//Create the reduced mesh transform node
	MFnTransform dummyFnTransform;
	MObject reducedMeshTransformMObj = dummyFnTransform.create(MObject::kNullObj);
	MFnTransform reducedMeshTransform(reducedMeshTransformMObj);
	reducedMeshTransform.setName(rdcMeshTransformName);

	MTransformationMatrix origMeshTransformationMatrix = origMeshFnTransform.transformation();
	MTransformationMatrix reducedMeshTransformationMatrix = reducedMeshTransform.transformation();

	//Create the reduced mesh 
	MFnMesh reducedFnMesh;
	reducedFnMesh.create( reducedVertCount, reducedTriCount, mayaVertArray, mayaPolyCountArray, mayaPolyConnectsArray, 
							mayaTexCoordUArray, mayaTexCoordVArray, reducedMeshTransformMObj , &status);
	if(status == MS::kSuccess)
	{
		MString rcdMeshName = "reduced_";
		rcdMeshName += origMeshFnDep.name();
		reducedFnMesh.setName(rcdMeshName);

		//Assign the UV coordinates to the mesh
		status = reducedFnMesh.assignUVs( mayaUvCountsArray, mayaUvIdsArray);

		//Assign the shaders from the original mesh to the reduced mesh
		status = assignPolyShaders( reducedFnMesh.object(), polyAttrArr, reducedTriCount );

		//Set the transformation matrix of the mesh transform to match the original mesh transform
		reducedMeshTransform.set( origMeshTransformationMatrix );

		//Set the resulting reduced mesh DAG Path
		status = reducedMeshTransform.getPath((*pResultDagPath));

		if(m_bPreserveHierarchy)
		{
			MDagPath newParentDagPath = m_DagPathToReduce;
			newParentDagPath.pop(); //Set the Dag path to the shape's transform
			newParentDagPath.pop(); //Set the Dag path to the parent node

			//If the parent dag path is valid then reparent the reduced mesh to this node
			if(newParentDagPath.isValid() && 
			   ( (newParentDagPath.apiType() == MFn::kJoint) ||
			     (newParentDagPath.apiType() == MFn::kTransform) ))
			{
				MDagModifier dagMod;
				dagMod.reparentNode( reducedMeshTransform.object(), newParentDagPath.node() );
				if(dagMod.doIt() != MS::kSuccess)
				{
					status.perror("Failed to reparent reduced mesh");
				}
			}
		}

		MDagPath rdcMeshDagPath;
		rdcMeshDagPath= MDagPath::getAPathTo(reducedFnMesh.object(), &status);
		if(status != MS::kSuccess)
		{
			status.perror("Failed to get a path to the reduced mesh");
		}
		else
		{
			//postProcessUvCoordinates(rdcMeshDagPath);

			//Copy vertex colors over to the reduced mesh
			if(m_bTransferVertexColors)
				postProcessVertexColors(rdcMeshDagPath);

			//Peform an automatic soft/hard edge adjustmen of the reduced model
			if(m_bPerformEdgeSmoothing)
				postProcessEdgeSmoothing(reducedFnMesh);
		}

		//Clean up
		delete [] polyAttrArr;

		return true;
	}
	else
	{
		if(status == MS::kInvalidParameter)
		{
			status.perror("MFnMesh::create");
			MGlobal::displayError("MFnMesh::create() returned invalidParameter error.");
		}
		else if(status == MS::kInsufficientMemory)
		{
			status.perror("MFnMesh::create");
			MGlobal::displayError("MFnMesh::create() returned an insufficient memory error.");
		}
		else if(status == MS::kFailure)
		{
			status.perror("MFnMesh::create");
			MGlobal::displayError("MFnMesh::create() returned a general failure error.");
		}
		else
		{
			status.perror("MFnMesh::create");
			MGlobal::displayError("MFnMesh::create() returned an unknown error.");
		}

		//Clean up
		delete [] polyAttrArr;

		return false;
	}
}

//-----------------------------------------------------------------------------

float rageMayaReduce::sameSurface(void* triangle1data, void* triangle2data, void* userdata)
{
	return 1.0f;
}

//-----------------------------------------------------------------------------

float rageMayaReduce::sameMaterial(void* triangle1data, void* triangle2data, void* /*userdata*/)
{
	reducePolyAttr *pTri1Attr = static_cast<reducePolyAttr*>(triangle1data);
	reducePolyAttr *pTri2Attr = static_cast<reducePolyAttr*>(triangle2data);

	if( !pTri1Attr || !pTri2Attr )
		return 0;

	if(pTri1Attr->shaderMObj == pTri2Attr->shaderMObj)
		return 0;
	else
		return 1;
}

//-----------------------------------------------------------------------------

float rageMayaReduce::sameTexture(void* triangle1data, void* triangle2data, void* userdata)
{
	return sameMaterial(triangle1data, triangle2data, userdata);
}

//-----------------------------------------------------------------------------

MStatus AttachMeshShader(const MObject& shadingGroup, const MObject& mesh, const MObject& components, uint instanceNumber)
{
	MFnMesh meshFn(mesh);
	MFnDependencyNode sgFn(shadingGroup);
	MFnComponentListData componentListFn;

	// Retrieve the object group plug
	MPlug objectGroupsInstancesPlug = meshFn.findPlug("iog");
	MPlug objectGroupsInstancePlug = objectGroupsInstancesPlug.elementByLogicalIndex(instanceNumber);
	MPlug objectGroupsPlug = rmrDagHelper::GetChildPlug(objectGroupsInstancePlug, "og");

	// Check if a group already exists between this mesh and the shading group.
	uint numGroups = objectGroupsPlug.numElements();
	uint groupIndex = 0;
	for (; groupIndex < numGroups; ++groupIndex)
	{
		MPlug headPlug = objectGroupsPlug.elementByPhysicalIndex(groupIndex);
		MObject connection = rmrDagHelper::GetNodeConnectedTo(headPlug);

		// Retrieve the component list for this group.
		MObject componentList;
		MPlug componentListPlug = rmrDagHelper::GetChildPlug(headPlug, "gcl");
		componentListPlug.getValue(componentList);
		componentListFn.setObject(componentList);

		if (connection != shadingGroup)
		{
			// Check if the wanted components are included in this group.
			// Strangely, that happens often with materials.
			// Also: this could break skinning, need testing!!
			if (componentListFn.has(components)) break;
		}
		else
		{
			// Re-use this group, since the faces end up on the same material.
			componentListFn.add(const_cast<MObject&>(components));
			componentListPlug.setValue(componentList);
			return MStatus::kSuccess;
		}
	}

	MPlug objectGroupPlug = (groupIndex >= numGroups) ? objectGroupsPlug.elementByLogicalIndex(groupIndex) : objectGroupsPlug.elementByPhysicalIndex(groupIndex);

	MFnDependencyNode groupIdFn;
	if (groupIndex >= numGroups)
	{

		// Create the group id node
		groupIdFn.create("groupId");
		rmrDagHelper::SetPlugValue(groupIdFn.object(), "isHistoricallyInteresting", 0);

		// Assign the components
		MPlug componentListPlug = rmrDagHelper::GetChildPlug(objectGroupPlug, "gcl");
		MObject componentList = componentListFn.create();
		componentListFn.add(const_cast<MObject&>(components));
		componentListPlug.setValue(componentList);

		// Attach the group id node to the mesh
		MPlug groupIdPlug = rmrDagHelper::GetChildPlug(objectGroupPlug, "objectGroupId");
		rmrDagHelper::Connect(groupIdFn.object(), "groupId", groupIdPlug);

		MPlug inMeshPlug = meshFn.findPlug("inMesh");
		if (rmrDagHelper::HasConnection(inMeshPlug, false, true))
		{
			// Need to add group parts node
			MPlug oldConnection;
			rmrDagHelper::GetPlugConnectedTo(inMeshPlug, oldConnection);
			MFnDependencyNode groupPartsFn;
			groupPartsFn.create("groupParts");
			MPlug groupPartsComponentPlug = groupPartsFn.findPlug("ic");
			groupPartsComponentPlug.setValue(componentList);
			rmrDagHelper::SetPlugValue(groupPartsFn.object(), "isHistoricallyInterested", 0);
			rmrDagHelper::Connect(oldConnection, groupPartsFn.object(), "inputGeometry");
			rmrDagHelper::Connect(groupIdFn.object(), "groupId", groupPartsFn.object(), "groupId");
			rmrDagHelper::Disconnect(oldConnection, inMeshPlug);
			rmrDagHelper::Connect(groupPartsFn.object(), "outputGeometry", inMeshPlug);
		}
	}
	else
	{
		// Retrieve the group id node.
		MPlug groupIdPlug = rmrDagHelper::GetChildPlug(objectGroupPlug, "objectGroupId");
		groupIdFn.setObject(rmrDagHelper::GetNodeConnectedTo(groupIdPlug));
	}

	// Attach the shading group's color to the object group
	MPlug colorPlug = rmrDagHelper::GetChildPlug(objectGroupPlug, "objectGrpColor");
	rmrDagHelper::Disconnect(colorPlug);
	rmrDagHelper::Connect(sgFn.object(), "mwc", colorPlug);

	// Attach the group id node and the object group to the shading group.
	MPlug messagePlug = groupIdFn.findPlug("message");
	rmrDagHelper::Disconnect(messagePlug);
	rmrDagHelper::Disconnect(objectGroupPlug);

	// Figure out the first available indices in the two array plugs.
	int index = -1;
	index = rmrDagHelper::GetNextAvailableIndex(shadingGroup, "groupNodes", index);
	index = rmrDagHelper::GetNextAvailableIndex(shadingGroup, "dagSetMembers", index);

	if (index > -1)
	{
		rmrDagHelper::ConnectToList(messagePlug, shadingGroup, "groupNodes", &index);
		rmrDagHelper::ConnectToList(objectGroupPlug, shadingGroup, "dagSetMembers", &index);
	}

	return MStatus::kSuccess;
}

//-----------------------------------------------------------------------------

bool rageMayaReduce::postProcessEdgeSmoothing(MFnMesh& fnRdcMesh)
{
	MStatus status;
	char	msgBuf[1024];

	MDagPath rdcMeshDagPath;
	MDagPath::getAPathTo(fnRdcMesh.object(), rdcMeshDagPath);

	double dpCompare = cos(DtoR * m_EdgeSmoothingAngle);

	MItMeshEdge rdcMeshEdgeIt(rdcMeshDagPath);
	for(rdcMeshEdgeIt.reset(); !rdcMeshEdgeIt.isDone(); rdcMeshEdgeIt.next())
	{
		if(!rdcMeshEdgeIt.onBoundary())
		{
			MIntArray connectedFaces;
			int nConnFaces = rdcMeshEdgeIt.getConnectedFaces(connectedFaces);
			if(nConnFaces < 2 )
			{
				//We have an edge that isn't on a boundary, but only has 1 face connected to it..?
				sprintf_s(msgBuf,"Found edge (%d), that isn't a boundary edge, but only has one face connected to it", rdcMeshEdgeIt.index());
				MGlobal::displayError(msgBuf);
				return false;
			}
			else if(nConnFaces == 2)
			{
				//Compare the face nomals of the polys connected to this edge and set the hard/smooth
				//flag based on the difference between them
				MFnSingleIndexedComponent fnRdcMeshConnPolysComp;
				fnRdcMeshConnPolysComp.create(MFn::kMeshPolygonComponent);
				fnRdcMeshConnPolysComp.addElements(connectedFaces);

				MItMeshPolygon connFacesPolyIt(rdcMeshDagPath, fnRdcMeshConnPolysComp.object(), &status);

				MVector normalA;
				connFacesPolyIt.getNormal(normalA, MSpace::kWorld);
				normalA.normalize();
				connFacesPolyIt.next();
				MVector normalB;
				connFacesPolyIt.getNormal(normalB, MSpace::kWorld);
				normalB.normalize();

				double dp = normalA * normalB;
				if(dp < dpCompare)
					rdcMeshEdgeIt.setSmoothing(false);
				else
					rdcMeshEdgeIt.setSmoothing(true);
			}
			else
			{
				//If the edge is connected to more than two faces then just mark it as smooth
				rdcMeshEdgeIt.setSmoothing(true);
			}
		}
		else
		{
			//The edge is a boundary edge so make it a hard edge
			rdcMeshEdgeIt.setSmoothing(false);
		}
	}
	fnRdcMesh.updateSurface();
	return true;
}

//-----------------------------------------------------------------------------

struct meshProximityInfo
{
	MPoint		closePoint;

	int			closePolyIndex;				//Object Relative
	MIntArray	closePolyVertIndices;		//Object Relative

	int			closePolyTriIndex;			//Polygon Relative
	MIntArray	closePolyTriVertIndices;	//Polygon Relative
	MPoint		closePointTriBary;
};

//-----------------------------------------------------------------------------

bool isPointInTriangle(MPoint point, MPointArray& allMeshPoints, int triIndices[3], MPoint* outBary = NULL)
{
	double u,v;

	MVector P(point);

	MVector A(allMeshPoints[triIndices[0]]);
	MVector B(allMeshPoints[triIndices[1]]);
	MVector C(allMeshPoints[triIndices[2]]);

	bool bOnPoint = false;
	if(point.isEquivalent(A, 0.000001))
	{
		u = 1.0;
		v = 0.0;
		bOnPoint = true;
	}
	else if(point.isEquivalent(B, 0.000001))
	{
		u = 0.0;
		v = 1.0;
		bOnPoint = true;
	}
	else if(point.isEquivalent(C, 0.000001))
	{
		u = 0.0;
		v = 0.0;
		bOnPoint = true;
	}

	if(!bOnPoint)
	{
		MVector v0,v1, vCross;

		//Compute the total triangle area
		v0 = C - A;
		v1 = B - A;
		vCross = v0 ^ v1;
		double triArea = vCross.length() / 2.0;

		//Compute U val
		v0 = C - A;
		v1 = P - A;	
		vCross = v0 ^ v1;
		u = (vCross.length() / 2.0) / triArea;

		//Compute V val
		v0 = B - A;
		v1 = P - A;
		vCross = v0 ^ v1;
		v = (vCross.length() / 2.0) / triArea;
	}

	if(outBary)
	{
		outBary->x = Clamp(u,0.0,1.0);
		outBary->y = Clamp(v,0.0,1.0);
		outBary->z = 1.0 - outBary->x - outBary->y;
	}

	if(bOnPoint)
		return true;
	else
		return (u > -0.000001) && (v > -0.000001) && (u + v < 1.00001);
}

//-----------------------------------------------------------------------------

MStatus generateVertexProximityInfo(MDagPath& origMeshDp, MDagPath& rdcMeshDp, vector<meshProximityInfo*>& rdcMeshProximityInfoArr)
{
	MStatus status = MS::kSuccess;

	MFnMesh fnRdcMesh(rdcMeshDp);
	MFnMesh fnOrigMesh(origMeshDp);

	MPointArray origMeshVertPositions;
	status = fnOrigMesh.getPoints(origMeshVertPositions, MSpace::kWorld);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	MIntArray origMeshTriangleCounts;
	MIntArray origMeshTriangleVertices;
	status = fnOrigMesh.getTriangles(origMeshTriangleCounts, origMeshTriangleVertices);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	MPointArray rdcMeshVertPositions;
	status = fnRdcMesh.getPoints(rdcMeshVertPositions, MSpace::kWorld);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	int nRdcVerts = rdcMeshVertPositions.length();
	rdcMeshProximityInfoArr.reserve(nRdcVerts);

	for(int vIdx=0; vIdx<nRdcVerts; vIdx++)
	{
		meshProximityInfo* pProxInfo = new meshProximityInfo();
		MPoint rdcMeshPoint = rdcMeshVertPositions[vIdx];

		//Get the closest point
		status = fnOrigMesh.getClosestPoint(rdcMeshPoint, 
							pProxInfo->closePoint, 
							MSpace::kWorld, 
							&pProxInfo->closePolyIndex);
		CHECK_MSTATUS_AND_RETURN_IT(status);

		//Get the vertex indices of the polygon that point lies in
		fnOrigMesh.getPolygonVertices(pProxInfo->closePolyIndex, pProxInfo->closePolyVertIndices);

		//Look through all the the triangles which make up the polygon and locate the triangle
		//that the closest point lies in
		int nPolyTris = origMeshTriangleCounts[pProxInfo->closePolyIndex];
		for(int tIdx=0; tIdx<nPolyTris; tIdx++)
		{
			int		triVertIndices[3];
			MPoint	baryCoord;

			fnOrigMesh.getPolygonTriangleVertices(pProxInfo->closePolyIndex, tIdx, triVertIndices);
			if(isPointInTriangle(pProxInfo->closePoint, origMeshVertPositions, triVertIndices, &baryCoord))
			{
				//We've got the triangle, so cache the triangle index, as well as the barycentric corrdinates
				//of the point withing that triangle
				pProxInfo->closePolyTriIndex = tIdx;
				pProxInfo->closePointTriBary = baryCoord;

				//triVertIndices currently contains mesh relative vertex indices, what we will need is polygon relative
				//vertex indices for each point in the triangle, since most of the MFnMesh methods require these,
				//so do the conversion from mesh relative to polygon relative 
				for(unsigned int i=0; i<3; i++)
				{
					int polyRelativeIdx = -1;
					for(unsigned int j=0; j<pProxInfo->closePolyVertIndices.length(); j++)
					{
						if(triVertIndices[i] == pProxInfo->closePolyVertIndices[j])
						{
							polyRelativeIdx = j;
							break;
						}
					}
					assert(polyRelativeIdx != -1);
					pProxInfo->closePolyTriVertIndices.append(polyRelativeIdx);
				}
				break;
			}//End if(isPointInTriangle...				 
		}//End for(int tIdx...
		
		rdcMeshProximityInfoArr.push_back(pProxInfo);
	}

	return status;
}

MStatus	rageMayaReduce::postProcessVertexColors(MDagPath& rdcMeshDagPath)
{
	MStatus status = MS::kSuccess;

	MFnMesh fnRdcMesh(rdcMeshDagPath);
	MFnMesh fnOrigMesh(m_DagPathToReduce);

	//Cache some data from the original mesh to be used for processing the vertex colors later...
	MStringArray origMeshColorSetNames;
	status = fnOrigMesh.getColorSetNames(origMeshColorSetNames);
	CHECK_MSTATUS_AND_RETURN_IT(status);
	int nColorSets = origMeshColorSetNames.length();
	if(nColorSets == 0)
		return MS::kSuccess;	//Nothing to do...

	vector<MColorArray>	origMeshFaceVertexColors;
	origMeshFaceVertexColors.assign(nColorSets, MColorArray());

	for(int clrSetIdx=0; clrSetIdx<nColorSets; clrSetIdx++)
	{
		//Store the original mesh face vertex color
		status = fnOrigMesh.getFaceVertexColors(origMeshFaceVertexColors.at(clrSetIdx), &origMeshColorSetNames[clrSetIdx]);
		CHECK_MSTATUS_AND_RETURN_IT(status);
	}

	//Create a default mapping of face vertices to colors.
	MColorArray tmpColorArray;
	MIntArray	tmpColorIndexArray;
	MItMeshPolygon tmpPolyIt(rdcMeshDagPath);
	int tmpClrIdx=0;
	for(tmpPolyIt.reset(); !tmpPolyIt.isDone(); tmpPolyIt.next())
	{
		for(unsigned int i=0; i<tmpPolyIt.polygonVertexCount(); i++)
		{
			tmpColorArray.append(MColor(1.0f, 1.0f, 1.0f, 1.0f));
			tmpColorIndexArray.append(tmpClrIdx++);
		}
	}

	//Create the color sets on the reduced mesh to match the color sets
	//on the original, as well as assign the default face vertex color mappings
	for(int clrSetIdx=0; clrSetIdx<nColorSets; clrSetIdx++)
	{
		status = fnRdcMesh.createColorSet(origMeshColorSetNames[clrSetIdx]);
		CHECK_MSTATUS_AND_RETURN_IT(status);
		
		status = fnRdcMesh.setColors(tmpColorArray, &origMeshColorSetNames[clrSetIdx]);
		CHECK_MSTATUS_AND_RETURN_IT(status);
		status = fnRdcMesh.assignColors(tmpColorIndexArray, &origMeshColorSetNames[clrSetIdx]);
		CHECK_MSTATUS_AND_RETURN_IT(status);
	}

	//For each vertex in the reduced mesh determine the closest point to that vertex
	//on the original mesh, as well as what polygon and triangle that point lies in
	vector<meshProximityInfo*> rdcMeshProximityInfoArr;
	status = generateVertexProximityInfo( m_DagPathToReduce, rdcMeshDagPath, rdcMeshProximityInfoArr );
	if(status != MS::kSuccess)
	{
		MGlobal::displayError("postProcessVertexColors, failed to generate vertex proximity info table.");
		return status;
	}

	//Iterate through each color set 
	for(int clrSetIdx=0; clrSetIdx<nColorSets; clrSetIdx++)
	{
		MColorArray rdcMeshVertexColors;
		MIntArray	rdcMeshVertexColorIndicies;
		int rdcMeshPolyVertexCount = 0;

		MString curColorSetName = origMeshColorSetNames[clrSetIdx];

		//Get the color array from the original mesh for the current color set
		MColorArray& curOrigMeshFaceVertexColorArr = origMeshFaceVertexColors.at(clrSetIdx);

		MItMeshPolygon rdcMeshPolyIt(rdcMeshDagPath);
		for(rdcMeshPolyIt.reset(); !rdcMeshPolyIt.isDone(); rdcMeshPolyIt.next())
		{
			int rdcPolyIndex = rdcMeshPolyIt.index();

			int nPolyVerts = rdcMeshPolyIt.polygonVertexCount();
			rdcMeshPolyVertexCount += nPolyVerts;

			//Get the color indicies for each vertex in the current reduced mesh poly
			MIntArray rdcMeshPolyColorIndices;
			status = rdcMeshPolyIt.getColorIndices(rdcMeshPolyColorIndices, &curColorSetName);
			CHECK_MSTATUS_AND_RETURN_IT(status);
		
			//Iterate over each of the vertices in the current reduced mesh poly, updating its
			//color based on the color information in the original mesh
			MIntArray polyVertIndices;
			rdcMeshPolyIt.getVertices(polyVertIndices);
			for(unsigned pvIdx=0; pvIdx<polyVertIndices.length(); pvIdx++)
			{
				meshProximityInfo* pProxInfo = rdcMeshProximityInfoArr[polyVertIndices[pvIdx]];
				
				//Get the colors for each face vertex of the orignal mesh for the triangle that
				//the closest point is inside of.
				MColorArray polyTriColors;
				for(unsigned int i=0; i<3; i++)
				{
					int clrIndex;
					fnOrigMesh.getFaceVertexColorIndex(pProxInfo->closePolyIndex,
														pProxInfo->closePolyTriVertIndices[i], 
														clrIndex, 
														&curColorSetName);
					polyTriColors.append(curOrigMeshFaceVertexColorArr[clrIndex]);

				}
				
				//Generate the vertex color by summing the weighted colors of each of the colors
				//applied to the vertices of the triangle
				MColor newColor(0.0f,0.0f,0.0f,0.0f);
				newColor.r = (polyTriColors[0].r * (float)pProxInfo->closePointTriBary.x) +
					(polyTriColors[1].r * (float)pProxInfo->closePointTriBary.y) +
					(polyTriColors[2].r * (float)pProxInfo->closePointTriBary.z);

				newColor.g = (polyTriColors[0].g * (float)pProxInfo->closePointTriBary.x) +
					(polyTriColors[1].g * (float)pProxInfo->closePointTriBary.y) +
					(polyTriColors[2].g * (float)pProxInfo->closePointTriBary.z);

				newColor.b = (polyTriColors[0].b * (float)pProxInfo->closePointTriBary.x) +
					(polyTriColors[1].b * (float)pProxInfo->closePointTriBary.y) +
					(polyTriColors[2].b * (float)pProxInfo->closePointTriBary.z);

				newColor.a = (polyTriColors[0].a * (float)pProxInfo->closePointTriBary.x) +
					(polyTriColors[1].a * (float)pProxInfo->closePointTriBary.y) +
					(polyTriColors[2].a * (float)pProxInfo->closePointTriBary.z);

				//Store the color in the array of colors to apply to the reduced mesh
				rdcMeshVertexColors.append(newColor);
				int clrIdx = rdcMeshPolyColorIndices[pvIdx];
				rdcMeshVertexColorIndicies.append(clrIdx);
			}
		}//End for(rdcMeshPolyIt...

		//Make sure we ended up with the right number of indices
		assert(rdcMeshVertexColorIndicies.length() == rdcMeshPolyVertexCount);

		//Apply the colors
		status = fnRdcMesh.setSomeColors(rdcMeshVertexColorIndicies, rdcMeshVertexColors, &curColorSetName);
		CHECK_MSTATUS_AND_RETURN_IT(status);
	
	}//End for(int clrSetIdx...

	//Update the mesh
	status = fnRdcMesh.updateSurface();
	CHECK_MSTATUS_AND_RETURN_IT(status);

	//Cleanup
	for(vector<meshProximityInfo*>::iterator it = rdcMeshProximityInfoArr.begin();
		it != rdcMeshProximityInfoArr.end();
		++it)
	{
		delete (*it);
	}

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus rageMayaReduce::postProcessUvCoordinates(MDagPath& rdcMeshDagPath)
{
	MStatus status = MS::kSuccess;

	MFnMesh fnRdcMesh(rdcMeshDagPath);
	MFnMesh fnOrigMesh(m_DagPathToReduce);
	
	MStringArray origUvSetNames;
	fnOrigMesh.getUVSetNames(origUvSetNames);
	unsigned int nOrigUvSets = origUvSetNames.length();
	if(nOrigUvSets == 1)
	{
		//If there is only one UV set then this was already transferred as part of the reduction process...
		return MS::kSuccess;
	}

	MString origCurrentUvSetName;
	fnOrigMesh.getCurrentUVSetName(origCurrentUvSetName);

	MStringArray uvSetsToTransfer;
	for(unsigned int setIdx=0; setIdx < nOrigUvSets; setIdx++)
	{
		if(origUvSetNames[setIdx] == origCurrentUvSetName)
			continue;
		else
		{
			//Store the name of the set to transfer
			uvSetsToTransfer.append(origUvSetNames[setIdx]);

			//Create a new UV set on the reduced mesh
			status = fnRdcMesh.createUVSet(origUvSetNames[setIdx]);
			CHECK_MSTATUS_AND_RETURN_IT(status);
		}
	}
	unsigned int nUvSetsToTransfer = uvSetsToTransfer.length();

	//Create a default UV set mapping
	MIntArray defaultUvCounts;
	MIntArray defaultUvIds;

	MFloatArray defaultUVals;
	MFloatArray defaultVVals;

	MItMeshPolygon rdcMeshPolyIt(rdcMeshDagPath);

	for(rdcMeshPolyIt.reset(); !rdcMeshPolyIt.isDone(); rdcMeshPolyIt.next())
	{
		int nPolyVerts = rdcMeshPolyIt.polygonVertexCount();

		defaultUvCounts.append(nPolyVerts);

		int uvIdx = 0;
		for(int vtxIdx=0; vtxIdx<nPolyVerts; vtxIdx++)
		{
			defaultUvIds.append(uvIdx++);
			defaultUVals.append(0.0f);
			defaultVVals.append(0.0f);
		}
	}

	//For each vertex in the reduced mesh determine the closest point to that vertex
	//on the original mesh, as well as what polygon and triangle that point lies in
	vector<meshProximityInfo*> rdcMeshProximityInfoArr;
	status = generateVertexProximityInfo( m_DagPathToReduce, rdcMeshDagPath, rdcMeshProximityInfoArr );
	if(status != MS::kSuccess)
	{
		MGlobal::displayError("postProcessUvCoordinates, failed to generate vertex proximity info table.");
		return status;
	}

	for(unsigned int setIdx = 0; setIdx < nUvSetsToTransfer; setIdx++)
	{
		MString curUvSetName = uvSetsToTransfer[setIdx];

		//First setup the default UV mapping
		fnRdcMesh.assignUVs(defaultUvCounts, defaultUvIds, &curUvSetName);
		fnRdcMesh.setUVs(defaultUVals, defaultVVals, &curUvSetName);

		//Now iterate over each face vertex and interpolate the UV corrdinates
		//from the original mesh
		for(rdcMeshPolyIt.reset(); !rdcMeshPolyIt.isDone(); rdcMeshPolyIt.next())
		{
			int nPolyVerts = rdcMeshPolyIt.polygonVertexCount();
			
		}
	}

	//Cleanup
	for(vector<meshProximityInfo*>::iterator it = rdcMeshProximityInfoArr.begin();
		it != rdcMeshProximityInfoArr.end();
		++it)
	{
		delete (*it);
	}

	return MS::kSuccess;
}


//-----------------------------------------------------------------------------

#if 0
bool rageMayaReduce::PerformMeshPostProcessing(MFnMesh& fnRdcMesh)
{
	MStatus status;
	char	msgBuf[1024];

	MFnMesh fnOrigMesh(m_DagPathToReduce);
	MMeshIsectAccelParams orgMeshAccel = fnOrigMesh.autoUniformGridParams();
	
	MFloatVectorArray origMeshNormals;
	fnOrigMesh.getNormals(origMeshNormals);

	MVectorArray rdcMeshAdjNormals;
	MIntArray	 rdcMeshAdjFaceIds;
	MIntArray	 rdcMeshAdjVertexIds;

	MDagPath rdcMeshDagPath;
	MDagPath::getAPathTo(fnRdcMesh.object(), rdcMeshDagPath);

	MItMeshPolygon rdcMeshPolyIt(rdcMeshDagPath);
	for(rdcMeshPolyIt.reset(); !rdcMeshPolyIt.isDone(); rdcMeshPolyIt.next())
	{
		//Get the polygon normal
		MVector rdcMeshPolyNrm;
		rdcMeshPolyIt.getNormal(rdcMeshPolyNrm, MSpace::kWorld);
		rdcMeshPolyNrm.normalize();

		//Get the polygon area
		double rdcMeshPolyArea;
		rdcMeshPolyIt.getArea(rdcMeshPolyArea, MSpace::kWorld);

		//Get a list of the vertex positions
		MPointArray rdcMeshPolyVerts;
		rdcMeshPolyIt.getPoints(rdcMeshPolyVerts, MSpace::kWorld);
		int nRdcMeshPolyVerts = rdcMeshPolyVerts.length();

		for(int i=0;i<nRdcMeshPolyVerts;i++)
		{
			//Store the object relative index of this vertex
			int objectRelativeVertexIndex = rdcMeshPolyIt.vertexIndex(i);

			MPoint	origMshClosestPoint;
			MVector	origMshClosestNormal;
			int		origMshClosestPolyIdx;

			//Find the closest point, and poly on the original mesh
			status = fnOrigMesh.getClosestPointAndNormal(rdcMeshPolyVerts[i], origMshClosestPoint, origMshClosestNormal, MSpace::kWorld, &origMshClosestPolyIdx);
			if(status == MS::kSuccess)
			{
				//Create an iterator for the closest polygon to this point in the original mesh
				MFnSingleIndexedComponent fnOrigMshSiCmp;
				fnOrigMshSiCmp.create(MFn::kMeshPolygonComponent);
				fnOrigMshSiCmp.addElement(origMshClosestPolyIdx);
				MItMeshPolygon origMeshPolyIt(m_DagPathToReduce, fnOrigMshSiCmp.object(), &status);
				if(status != MS::kSuccess)
				{
					status.perror("rageMayaReduce::PerformMeshPostProcessing, failed to create poly iterator.");
					return false;
				}

				//Get the face normal of the closest face on the original mesh
				MVector origMeshPolyNrm;
				origMeshPolyIt.getNormal(origMeshPolyNrm, MSpace::kWorld);
				origMeshPolyNrm.normalize();

				//If the closest poly normal is facing relatively the same direction as the face normal
				//of the reduced face this vert is a part of then apply that norml to this vertex,
				//otherwise look through the adjacent faces in the original mesh trying to find a 
				//good normal value to use
				if(rdcMeshPolyNrm.isParallel(origMeshPolyNrm, 0.2))
				{
					rdcMeshAdjNormals.append(origMeshPolyNrm);
					rdcMeshAdjFaceIds.append(rdcMeshPolyIt.index());
					rdcMeshAdjVertexIds.append(objectRelativeVertexIndex);
				}
				else
				{
					MVector bestVertexNormal;

					set<int> searchedOrigMshPolyIdxs;
					searchedOrigMshPolyIdxs.insert(origMshClosestPolyIdx);

					list<MObject> adjElementQueue;

					//Seed the queue with the list of faces adjacent to the closest face on the original mesh
					MIntArray origMeshPolyConFaces;
					origMeshPolyIt.getConnectedFaces(origMeshPolyConFaces);
					MFnSingleIndexedComponent fnOrigMshAdjSiCmp;
					fnOrigMshAdjSiCmp.create(MFn::kMeshPolygonComponent);
					fnOrigMshAdjSiCmp.addElements(origMeshPolyConFaces);
					adjElementQueue.push_back( fnOrigMshAdjSiCmp.object() );

					//Keep searching the mesh faces until we find a normal that is close to the current face normal
					while(!adjElementQueue.empty())
					{
						MObject adjElementMObj = adjElementQueue.front();
						adjElementQueue.pop_front();

						MItMeshPolygon origMeshAdjPolyIt(m_DagPathToReduce, fnOrigMshAdjSiCmp.object(), &status);

						double	minDiff = 1.0f;
						MVector	closestNormal;

						for(origMeshAdjPolyIt.reset(); !origMeshAdjPolyIt.isDone(); origMeshAdjPolyIt.next())
						{
							if(searchedOrigMshPolyIdxs.find(origMeshAdjPolyIt.index()) != searchedOrigMshPolyIdxs.end())
								continue;
						
							//Add the poly we just checked to the set of polys that have been searched (so we don't
							//end up searching the same polys multiple times due to the adjancency testing)
							searchedOrigMshPolyIdxs.insert(origMeshAdjPolyIt.index());

							//Get the area
							double origMeshAdjPolyArea;
							origMeshAdjPolyIt.getArea(origMeshAdjPolyArea, MSpace::kWorld);

							//Get the normal
							MVector origMeshAdjPolyNrm;
							origMeshAdjPolyIt.getNormal(origMeshAdjPolyNrm, MSpace::kWorld);
							origMeshAdjPolyNrm.normalize();

							double dp = rdcMeshPolyNrm * origMeshAdjPolyNrm;
							if(dp > 0.0)
							{
								double diff = 1.0 - dp;
								if(diff < minDiff)
								{
									minDiff = diff;
									closestNormal = origMeshAdjPolyNrm;
								}
							}

							//Add the faces adjacent to this 
							MIntArray origMeshAdjPolyConFaces;
							origMeshAdjPolyIt.getConnectedFaces(origMeshAdjPolyConFaces);
							MFnSingleIndexedComponent fnOrigMshAdjAdjSiCmp;
							fnOrigMshAdjAdjSiCmp.create(MFn::kMeshPolygonComponent);
							fnOrigMshAdjAdjSiCmp.addElements(origMeshAdjPolyConFaces);
							adjElementQueue.push_back( fnOrigMshAdjAdjSiCmp.object() );
						}

						if(minDiff < 0.2)
						{
							bestVertexNormal = closestNormal;
							break;
						}
						else if(searchedOrigMshPolyIdxs.size() > 32)
						{
							bestVertexNormal = closestNormal;
							sprintf_s(msgBuf, "Halting normal scan for poly/vert (%d,%d), closest normal to (%f,%f,%f) found was (%f,%f,%f)",
								rdcMeshPolyIt.index(), i, 
								rdcMeshPolyNrm.x, rdcMeshPolyNrm.y, rdcMeshPolyNrm.z,
								bestVertexNormal.x, bestVertexNormal.y, bestVertexNormal.z);
							MGlobal::displayInfo(msgBuf);
							break;
						}
					}

					rdcMeshAdjNormals.append(bestVertexNormal);
					rdcMeshAdjFaceIds.append(rdcMeshPolyIt.index());
					rdcMeshAdjVertexIds.append(objectRelativeVertexIndex);
				}
			}
			else
			{
				//Add the existing face-vertex normal to the adjusted list of normals
				MVector vertNorm;
				rdcMeshPolyIt.getNormal(vertNorm, MSpace::kWorld);
				
				rdcMeshAdjNormals.append(vertNorm);
				rdcMeshAdjFaceIds.append(rdcMeshPolyIt.index());
				rdcMeshAdjVertexIds.append(objectRelativeVertexIndex);
			}
		}
	}

	status = fnRdcMesh.setFaceVertexNormals(rdcMeshAdjNormals, rdcMeshAdjFaceIds, rdcMeshAdjVertexIds, MSpace::kWorld);
	if(status != MS::kSuccess)
	{
		status.perror("PerformMeshPostProcessing, 'setFaceVertexNormals' failed.");
		return false;
	}
	fnRdcMesh.updateSurface();

	/*int runCount = 0;
	for (MItMeshVertex vertexIter(fnRdcMesh.object()); !vertexIter.isDone() && runCount < 10; vertexIter.next())
	{
		MPoint curVertPos = vertexIter.position(MSpace::kWorld);

		MPoint	origClosestPoint;
		MVector	origClosestNormal;
		int		origClosestPoly;

		status = fnOrigMesh.getClosestPointAndNormal(curVertPos, origClosestPoint, origClosestNormal, MSpace::kWorld, &origClosestPoly);
		if(status == MS::kSuccess)
		{
			sprintf_s(msgBuf, "Src VIndx (%d), ClPoint (%f,%f,%f), ClPoly (%d)", vertexIter.index(),
					origClosestPoint.x, origClosestPoint.y, origClosestPoint.z, origClosestPoly);
			MGlobal::displayInfo(msgBuf);
			runCount++;
		}
	}*/

	return true;
}
#endif

//-----------------------------------------------------------------------------

