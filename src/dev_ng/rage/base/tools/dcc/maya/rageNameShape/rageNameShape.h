#include <maya/MPxLocatorNode.h> 

class rageNameShape : public MPxLocatorNode
{
public:
	rageNameShape();
	virtual ~rageNameShape();
	static  void*        creator();
	static MStatus initialize();
	virtual void draw(
		M3dView & view,
		const MDagPath & path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status );

	static  MTypeId id;
	MStatus ComputeUnitSize(void);
protected:
	static MObject m_gobInputText;
	static MObject m_gobTextColor;
private:
	float m_UnitSizeInCentimeters;
};
