#include <maya/MColor.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MGlobal.h>

#include "rageNameShape.h"

MTypeId rageNameShape::id(0x7014);
MObject rageNameShape::m_gobInputText;
MObject rageNameShape::m_gobTextColor;

rageNameShape::rageNameShape()
{
	ComputeUnitSize();
}

rageNameShape::~rageNameShape()
{
}

void *rageNameShape::creator()
{
	return new rageNameShape;
}

MStatus rageNameShape::initialize()
{
	MStatus obStatus;
	MFnNumericAttribute obNAttrFn;
	MFnTypedAttribute obTAttrFn;

	m_gobInputText = obTAttrFn.create(MString("InputText"), MString("it"), MFnData::kString, MObject::kNullObj, &obStatus);
	obStatus = addAttribute(m_gobInputText);

	// color attribute
	m_gobTextColor = obNAttrFn.create( "TextColor", "tc",MFnNumericData::k3Float);
	obNAttrFn.setStorable(true);
	obNAttrFn.setHidden(false);
	obNAttrFn.setDefault(0.0f);
	obNAttrFn.setUsedAsColor(true);
	obStatus = addAttribute(m_gobTextColor);

	return obStatus;
}


void rageNameShape::draw(M3dView & view,
						 const MDagPath & path, 
						 M3dView::DisplayStyle style,
						 M3dView::DisplayStatus DisplayStatus )
{


	MStatus obStatus;

	// Get the color of the Text
	MObject thisNode = thisMObject();

	// Get the name of the map Object
	MString strStringToDisplay;
	MPlug obInputTextPlug( thisNode, m_gobInputText);
	obStatus = obInputTextPlug.getValue(strStringToDisplay);

	if(strStringToDisplay.length() == 0)
	{
		return; // nothing to do...
	}

	MPlug colorPlug( thisNode, m_gobTextColor );
	// colorPlug is a compound plug with 3 children (k3Float) 
	MPlug colorComponentPlug;
	float afTextColor[3];

	switch(DisplayStatus)
	{
	case M3dView::kActive: // ok - Selected 
	case M3dView::kLead:	// ok - Leader of selected group
		afTextColor[0] = 1.0;
		afTextColor[1] = 1.0;
		afTextColor[2] = 1.0;
		break;
	case M3dView::kDormant: // same as other locators - could change for map object
	case M3dView::kHilite: // Has selected componets
	case M3dView::kTemplate : // object is templated
	case M3dView::kActiveTemplate  :	// object is active and templated
	default:
		colorComponentPlug = colorPlug.child((unsigned int)0);
		colorComponentPlug.getValue(afTextColor[0]);
		colorComponentPlug = colorPlug.child((unsigned int)1);
		colorComponentPlug.getValue(afTextColor[1]);
		colorComponentPlug = colorPlug.child((unsigned int)2);
		colorComponentPlug.getValue(afTextColor[2]);
	}
	MColor obTextColor(afTextColor[0], afTextColor[1], afTextColor[2], 1.0f);
	view.setDrawColor( obTextColor );

	// Get the position of the text
	//	MMatrix obLocToWorld = path.inclusiveMatrix();

	MPoint obLocalPos;
	MPlug obLocPosXPlug ( thisNode, localPositionX );
	obLocPosXPlug.getValue( obLocalPos.x );
	MPlug obLocPosYPlug ( thisNode, localPositionY );
	obLocPosYPlug.getValue( obLocalPos.y );
	MPlug obLocPosZPlug ( thisNode, localPositionZ );
	obLocPosZPlug.getValue( obLocalPos.z );

	//	strStringToDisplayPos = obLocalPos * obLocToWorld;

	view.drawText(strStringToDisplay, obLocalPos, M3dView::kCenter);
}


MStatus rageNameShape::ComputeUnitSize(void)
{

	MString Result;
	MGlobal::executeCommand("convertUnit -t \"cm\" \"1\"", Result);
	// The result is a string like "100cm" EXCEPT when the current units are cm :
	// in this case it returns the string "1" (Funny, isn't it ? ;-()
	if(Result.length()==1)
	{ // no room for "cm" so the current units are centimeters
		m_UnitSizeInCentimeters=1.0f;
		return(MS::kSuccess);
	}
	char * NumberString; // The string in which I will put the number only
	NumberString = new char[Result.length()+1];
	strcpy(NumberString,Result.asChar());
	NumberString[Result.length()-2]='\0'; // suppress the two last characters (cm)
	m_UnitSizeInCentimeters=1.0f;
	int NumOfFieldsRead=sscanf(NumberString, "%f", &m_UnitSizeInCentimeters);
	delete [] NumberString;
	if(NumOfFieldsRead != 1)
		return(MS::kFailure);
	else
		return(MS::kSuccess);
}


