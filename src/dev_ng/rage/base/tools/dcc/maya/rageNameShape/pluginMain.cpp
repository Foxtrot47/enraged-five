//
// Copyright (C) Rage
// 
// File: pluginMain.cpp
//
// Author: Maya Plug-in Wizard 2.0
//
#include "rageIntShape.h"
#include "rageNameShape.h"

#include <maya/MFnPlugin.h>

MStatus initializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is loaded into Maya.  It 
//		registers all of the services that this plug-in provides with 
//		Maya.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{ 
	MStatus   status;
	MFnPlugin plugin( obj, "Rage", "6.0", "Any");

	status = plugin.registerNode( "rageNameShape", rageNameShape::id, rageNameShape::creator,
								  rageNameShape::initialize, MPxNode::kLocatorNode );
	if (!status) {
		status.perror("rageNameShape : registerNode");
		return status;
	}

	status = plugin.registerNode( "rageIntShape", rageIntShape::id, rageIntShape::creator,
								  rageIntShape::initialize, MPxNode::kLocatorNode );
	if (!status) {
		status.perror("rageIntShape : registerNode");
		return status;
	}

	return status;
}

MStatus uninitializePlugin( MObject obj)
//
//	Description:
//		this method is called when the plug-in is unloaded from Maya. It 
//		deregisters all of the services that it was providing.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{
	MStatus   status;
	MFnPlugin plugin( obj );

	status = plugin.deregisterNode( rageNameShape::id );
	if (!status) {
		status.perror("deregisterNode");
		return status;
	}
	status = plugin.deregisterNode( rageIntShape::id );
	if (!status) {
		status.perror("deregisterNode");
		return status;
	}

	return status;
}
