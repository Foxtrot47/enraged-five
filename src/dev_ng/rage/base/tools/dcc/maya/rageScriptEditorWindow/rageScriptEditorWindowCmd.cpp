// polySurfaceShape1277694268
// Copyright (C) 2000 Kevin's Plugin Factory 
// 
// File: rageScriptEditorWindowCmd.cpp
//
// MEL Command: rageScriptEditorWindow
//
// Author: Maya SDK Wizard
//
// Includes everything needed to register a simple MEL command with Maya.
// male_medium_shared_low_peek_right_start_shotgun_weapon_m16_materialInfo1
#include <Afxcmn.h> 
#include <Afxwin.h> 
#include <maya/MSimple.h>
#include <maya/MGlobal.h>
#include <iostream>
using namespace std;

#define MAX_LINE_LENGTH	512

// Use helper macro to register a command with Maya.  It creates and
// registers a command that does not support undo or redo.  The 
// created class derives off of MPxCommand.
DeclareSimpleCommand( rageScriptEditorWindow, "Kevin's Plugin Factory", "6.0");

void OutputUsageInfo()
{
	MGlobal::displayInfo("Error: Usage : rageScriptEditorWindow [-clear] [-grab]");
}

void RecursivelyFindWindow(HWND obParentWindow = NULL)
{
	if(!obParentWindow)
	{
		obParentWindow = GetDesktopWindow();
	}
	static int iDepth = 0;
	for(int i=0; i<iDepth; i++) cout<<"\t";
	char acBuffer[255];
	GetWindowText(obParentWindow, acBuffer, 255);
	cout<<"Found window : "<<acBuffer<<flush;
	GetClassName(obParentWindow, acBuffer, 255);
	cout<<"("<<acBuffer<<")"<<endl;
	for(int i=0; i<iDepth; i++) cout<<"\t";
	cout<<"{"<<endl;


	// Go through children
	iDepth++;
	HWND obChildWindow = GetWindow(obParentWindow, GW_CHILD);
	while(obChildWindow)
	{
		RecursivelyFindWindow(obChildWindow);
		obChildWindow = GetWindow(obChildWindow, GW_HWNDNEXT);
	}
	iDepth--;
	for(int i=0; i<iDepth; i++) cout<<"\t";
	cout<<"}"<<endl;
}

MStatus rageScriptEditorWindow::doIt( const MArgList& args )
{
	//if(args.length() == 0)
	//{
	//	OutputUsageInfo();
	//	return MS::kFailure;
	//}
	MGlobal::executeCommand("rageOutputWindow -clear");
	MStatus stat = MS::kSuccess;

	// Get the output window
	HWND obScriptEditorWindow;

	if((obScriptEditorWindow=FindWindow(  NULL, "Script Editor")) ==NULL) 
	{
		// Unable to find script editor, bail
		cout<<"Unable to find script editor"<<endl;
		return MS::kFailure;
	}

	// I've got the script editor, wohoo!
	// RecursivelyFindWindow(obScriptEditorWindow);

	// The hieracrchy under the script editor, looks like this:
	//Found window : Script Editor(AfxFrameOrView70)
	//{
	//	Found window : CommandWndLayout(AfxWnd70)
	//	{
	//		Found window : (AfxMDIFrame70)
	//		{
	//			Found window : (RichEdit20A)  <-- Command Window
	//			{
	//			}
	//			Found window : (RichEdit20A)  <-- History Window
	//			{
	//			}
	//		}
	//	}
	//}
	// I want to get at the second RichEdit box
	HWND obCommandWndLayout = GetWindow(obScriptEditorWindow, GW_CHILD);
	if(!obCommandWndLayout)
	{
		// Oh dear, I can't get the CommandWndLayout
		cout<<"Unable to find the CommandWndLayout child of the script editor"<<endl;
		return MS::kFailure;
	}

	HWND obMDIFrame = GetWindow(obCommandWndLayout, GW_CHILD);
	if(!obMDIFrame)
	{
		// Oh dear, I can't get the MDIFrame
		cout<<"Unable to find the MDIFrame child of the script editor"<<endl;
		return MS::kFailure;
	}

	HWND obRichEdit = GetWindow(obMDIFrame, GW_CHILD);
	if(!obRichEdit)
	{
		// Oh dear, I can't get the RichEdit
		cout<<"Unable to find the command window RichEdit child of the script editor"<<endl;
		return MS::kFailure;
	}

	obRichEdit = GetWindow(obRichEdit, GW_HWNDNEXT);
	if(!obRichEdit)
	{
		// Oh dear, I can't get the RichEdit
		cout<<"Unable to find the history window RichEdit child of the script editor"<<endl;
		return MS::kFailure;
	}

	// Ok, I've got the richtext box, now get the text from it
	CRichEditCtrl obRichEditCtrl;
	if(!obRichEditCtrl.Attach(obRichEdit))
	{
		cout<<"Unable to attach to script editor rich text box"<<endl;
		return MS::kFailure;
	}

	// Ok, got output window, so parse args
	for(unsigned i=0; i<args.length(); i++)
	{
		if(args.asString(i) == "-clear")
		{
			// Clear the output window
			obRichEditCtrl.SetSel(0,-1); 
			obRichEditCtrl.ReplaceSel("");
		}
		else if(args.asString(i) == "-grab")
		{
			// Print the output window's contents
			MStringArray obAStrScriptEditorWindowsText;
			char acBuffer[MAX_LINE_LENGTH];
			for(int j=0; j<obRichEditCtrl.GetLineCount(); j++)
			{
				acBuffer[obRichEditCtrl.GetLine(j, acBuffer, MAX_LINE_LENGTH)] = '\0';
				obAStrScriptEditorWindowsText.append(acBuffer);
			}
			setResult( obAStrScriptEditorWindowsText );
		}
		else
		{
			// I've been given rubbish, bail
			OutputUsageInfo();
			obRichEditCtrl.Detach();
			return MS::kFailure;
		}
	}

	obRichEditCtrl.Detach();

	return stat;
}
