#include "system/xtl.h"

#include "rexMayaRAGE/wrapper.h"

#include "rexMaya/objectMDagPath.h"

#include "rexBase/result.h"
#include "rexBase/object.h"
#include "rexBase/objectAdapter.h"

#include "rexMaya/shell.h"
#include "rexMaya/mayaUtility.h"


#include "rexMayaToGeneric/filterCamera.h"
#include "rexMayaToGeneric/filterLight.h"
#include "rexMayaToGeneric/filterLocator.h"
#include "rexMayaToGeneric/filterLevelInstance.h"
#include "rexMayaToGeneric/filterMeshHierarchy.h"
#include "rexMayaToGeneric/filterMesh.h"
#include "rexMayaToGeneric/filterBoundHierarchy.h"
#include "rexMayaToGeneric/filterBoneRoot.h"
#include "rexMayaToGeneric/filterNurbsCurve.h"
#include "rexMayaToGeneric/filterPrimitive.h"

#include "rexMayaToGeneric/converterAnimation.h"
#include "rexMayaToGeneric/converterBasic.h"
#include "rexMayaToGeneric/converterBound.h"
#include "rexMayaToGeneric/converterLevel.h"
#include "rexMayaToGeneric/converterLevelInstance.h"
#include "rexMayaToGeneric/converterDrawableMesh.h"
#include "rexMayaToGeneric/converterEdgeModel.h"
#include "rexMayaToGeneric/converterEntity.h"
#include "rexMayaToGeneric/converterFragment.h"
#include "rexMayaToGeneric/converterFragmentDamage.h"
#include "rexMayaToGeneric/converterLight.h"
#include "rexMayaToGeneric/converterLocator.h"
#include "rexMayaToGeneric/converterNurbsCurve.h"
#include "rexMayaToGeneric/converterSkeleton.h"
#include "rexMayaToGeneric/converterCutscene.h"
#include "rexMayaToGeneric/converterCloth.h"
#include "rexMayaToGeneric/converterHair.h"
#include "rexMayaToGeneric/converterPrimitive.h"

#include "rexMayaToGeneric/propertyBasic.h"

#include "rexGeneric/processorMeshCombiner.h"
#include "rexGeneric/processorPurgeDegenerateSkeleton.h"

#include "rexMayaRAGE/propertyBasic.h"
#include "rexRAGE/serializerBound.h"
#include "rexRAGE/serializerLevel.h"
#include "rexRAGE/serializerLevelInstance.h"
#include "rexRAGE/serializerEdgeModel.h"
#include "rexRAGE/serializerEntity.h"
#include "rexRAGE/serializerMesh.h"
#include "rexRAGE/serializerSkeleton.h"
#include "rexRAGE/serializerAnimation.h"
#include "rexRAGE/serializerCutscene.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable: 4100)
#pragma warning(disable: 4668)
#include <maya/MDagPath.h>
#pragma warning(pop)

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// This is the main registration function.  It sets up the shell with all the modules that are compatible
// with RAGE.  Projects can override this function locally and call this function to set up basics, then
// add new modules and/or override existing ones.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace rage {

void rexWrapperMayaRAGE::RegisterModules( rexShellMaya& shell )
{
	rexWrapperMaya::RegisterModules( shell );
	shell.SetExportDataNamespace( "_ExData_:" );

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// filter declarations.  filters look at an object and return a boolean which indicates whether the
	// node meets filter specifications.  for instance, the mesh filter returns true when the object 
	// (in Maya's case, DAG node) is a mesh, LOD group, transform, or joint
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	rexFilterCamera											filterCamera;
	rexFilterLocator										filterLocator;
	rexFilterLight											filterLight;
	rexFilterMeshHierarchy									filterMeshHierarchy;
	rexFilterBoundHierarchy									filterBoundHierarchy;
	rexFilterBoneRoot										filterBoneRoot;
	rexFilterNurbsCurve										filterNurbsCurve;
	rexFilterMeshWithTextureAnimation						filterMeshWithTextureAnimation;
	rexFilterLevelInstance                                  filterLevelInstance;
	rexFilterPrimitive										filterPrimitive;
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// converter declarations.  converters take input objects (in Maya's case, DAG nodes) and create
	// generic objects (meshes, skeletons, etc) that later get serialized.
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	// RAGE compatible basic converters
	rexConverterDrawableMeshHierarchy						converterMeshHierarchy;
	rexConverterBoundHierarchy								converterBoundHierarchy;
	rexConverterMeshHierarchyEdgeModel						converterEdgeModel;
	rexConverterEntity										converterEntity;
	rexConverterRmworldDistrict								converterLevel;
	rexConverterLevelInstance								converterLevelInstance;
	rexConverterRoomTypes									converterRoomTypes;
	rexConverterMeshHierarchyRoomVolumes					converterRoomVolumes;
	rexConverterMeshHierarchyPortals						converterPortals;
	rexConverterMeshHierarchyWater							converterWater;
	rexConverterMeshHierarchyOccluders						converterOccluders;
	rexConverterFragmentHierarchy							converterFragment;
	rexConverterFragmentDamageHierarchy						converterFragmentDamage;
	rexConverterSkeleton									converterSkeleton;
	rexConverterLocator										converterLocator;
	rexConverterSoundLocator								converterSoundLocator;
	rexConverterCharacterInstanceLocator					converterCharacterInstanceLocator;
	rexConverterLight										converterLight;
	rexConverterNurbsCurve									converterNurbsCurve;
	rexConverterAnimation									converterAnimation;
	rexConverterAnimation									converterTextureAnimation;
	rexConverterAnimationSegment							converterAnimationSegment;
	rexConverterAnimationSegment							converterTextureAnimationSegment;

	rexConverterClothMeshGroup								converterClothMeshGroup;
	rexConverterCharClothMeshGroup							converterCharClothMeshGroup;
	rexConverterClothMeshInfo								converterClothMeshInfo;

	rexConverterHairMeshGroup								converterHairMeshGroup;
	rexConverterHairMeshInfo								converterHairMeshInfo;

	rexConverterPrimitive									converterPrimitive;

	converterTextureAnimation.SetChunkNamePrefix( "tex_" );
	converterTextureAnimationSegment.SetChunkNamePrefix( "tex_" );


	// RAGE compatible cutscene converters
	rexConverterCutscene									converterCutscene;
	rexConverterCSCamera									converterCSCamera;
	rexConverterCSEvent										converterCSEvent;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// processor declarations.  processors take generic objects (created by converters) and allow for 
	// generic processing of the data.
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	rexProcessorMeshCombiner								processorMeshCombiner;
	rexProcessorMeshCombiner								processorBoundCombiner;
	processorBoundCombiner.SetBoundMode( true );

	rexProcessorPurgeDegenerateSkeleton						processorPurgeDegenerateSkeleton;
	processorPurgeDegenerateSkeleton.SetPurgeEnabled( false );

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// serializer declarations.  serializers take generic objects (created by converters) and write out
	// to a particular destination file format.  this allows us to change file formats by simply replacing
	// the serializer used by the module
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	// RAGE compatible basic serializers
	rexSerializerRAGEMesh									serializerMesh;
	rexSerializerRAGEPhysicsBound							serializerBound;
	rexSerializerRAGEEntity									serializerEntity;
	rexSerializerRmworldDistrict							serializerLevel;
	rexSerializerLevelInstance								serializerLevelInstance;
	rexSerializerRmworldRoomType							serializerRoomTypes;
	rexSerializerRAGESkeleton								serializerSkeleton;
	rexSerializerRAGEAnimation								serializerAnimation;
	rexSerializerRAGEEdgeModel								serializerEdgeModel;

	rexSerializerRAGEMesh									serializerMeshUnprocessed;
	serializerMeshUnprocessed.SetPolyMode( rexSerializerRAGEMesh::POLYGON );
	serializerMeshUnprocessed.SetExportRootBoneMeshesRelativeToMeshCenter( true );

	
	// RAGE compatible cutscene serializers
	rexSerializerCutscene 									serializerCutscene;
	rexSerializerCSCamera 									serializerCSCamera;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// properties declarations.  properties allow for modifying modules, objects, or shell behaviors.  
	// in maya, attributes on the RexExportData nodes define these properties and values.
	// also note that a property can have connected objects as well as values, which allows us to use this
	// to implement our tagging system.
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	// attributes to ignore because they are handled externally by UI scripts
	shell.RegisterProperty( "BaseRule" );

	// basic properties applicable to all modules
	rexPropertyInputNodes									propertyInputNodes;  // this is where most tagged nodes are handled
	rexPropertyName											propertyName;		
	rexPropertyOutputPath									propertyOutputPath;
	rexPropertyExportData									propertyExportData;  
	rexPropertyBoneTag										propertyBoneTag;	 // this modifies shell to create bone when no joint/animation found (i.e. car wheels)
	rexPropertySpecialFlag									propertySpecialFlag;  
	rexPropertyUseChildrenOfLevelInstanceNodes				propertyUseChildrenOfLevelInstanceNodes;
	rexPropertyUnitConversion								propertyUnitConversion;
	rexPropertyUseBoneIDs									propertyUseBoneIDs;
	rexPropertyIgnoreNullJoints								propertyIgnoreNullJoints;
	rexPropertyUseBoundFiltering							propertyUseBoundFiltering;
	rexPropertyBoundFilteringBoundTypes						propertyBoundFilteringBoundTypes;

	// basic converter properties
	rexPropertyConverterInstanceAttributes					propertyConverterInstanceAttributes;
	rexPropertyAuthoredOrientation							propertyAuthoredOrientation;
	rexPropertyGenBoneIdFromFullPath						propertyGenBoneIdFromFullPath;

	// drawable mesh properties
	rexPropertyTextureOutputPath							propertyTextureOutputPath;
	rexPropertyMeshMaxMemorySize							propertyMeshMaxMemorySize;
	rexPropertySetExportRootBoneMeshesRelativeToMeshCenter	propertyMeshExportRootBoneMeshesCentered;
	rexPropertyMeshCombineMaxPolyCountToCombine				propertyMeshMaxPolyCountToCombine;
	rexPropertyMeshCombineMaxPolyCountOfCombinedMesh		propertyMeshMaxPolyCountOfCombinedMesh;
	rexPropertyMeshCombineMaxDistance						propertyMeshCombineMaxDistance;
	rexPropertyMeshCombineMaxLODThresholdRatio				propertyMeshCombineMaxLODThresholdRatio;
	rexPropertyMeshCombineAcrossBones						propertyMeshCombineAcrossBones;
	rexPropertyMeshCombineMergeMaterials					propertyMeshCombineMergeMaterials;
	rexPropertyMeshSetExportTextures						propertyMeshSetExportTextures;
	rexPropertyMeshSetExportEnityTextureList				propertyMeshSetExportEntityTextureList;
	rexPropertyMeshEntityTextureListKeepExtensions			propertyMeshEntityTextureListKeepExtensions;
	rexPropertyMeshCheckVertexNumWeights					propertyMeshCheckVertexNumWeights;
	rexPropertyMeshExportBlendShapes						propertyMeshExportBlendShapes;
	rexPropertyMeshBlindDataToExport						propertyMeshBlindDataToExport;
	rexPropertyDataRelativeToTaggedRexRootMesh				propertyDataRelativeToTaggedRexRootMesh;
	rexPropertyDisableTriStripping							propertyDisableTriStripping;
	rexPropertySetMaxMatricesPerMaterial					propertySetMaxMatricesPerMaterial;
	rexPropertyUseNonOptimizedMTLCompareMesh				propertyUseNonOptimizedMTLCompareMesh;
	
	rexPropertyClothMeshInfoMesh							propertyClothMeshInfoMesh;
	rexPropertyClothMeshInfoBounds							propertyClothMeshInfoBounds;

	rexPropertyHairMeshInfoMesh								propertyHairMeshInfoMesh;
	rexPropertyLODMeshFilterMappings						propertyLODMeshFilterMappings;

	// physics bounds properties
	rexPropertyBoundSetGeometryBoundsExportAsBvhGrid		propertyBoundSetGeometryBoundsExportAsBvhGrid;
	rexPropertyBoundSetGeometryBoundsExportAsBvhGeom		propertyBoundSetGeometryBoundsExportAsBvhGeom;
	rexPropertyBoundSetGridCellSize							propertyBoundSetGridCellSize;
	rexPropertyZeroPrimitiveCentroids						propertyZeroPrimitiveCentroids;
	rexPropertyBoundSetPrimitiveBoundCompositeMaxDistance					propertyBoundSetPrimBoundCompMaxDist;
	rexPropertyBoundSetPrimitiveBoundExportPrimitivesInBvh	propertyBoundExportPrimsInBvh;
	rexPropertyBoundSetPrimitiveBoundCompositeMaxDistanceToBoundRadiusRatio	propertyBoundSetPrimBoundCompMaxDistToRadRatio;
	rexPropertyCheckBoundVertexCounts						propertyCheckBoundVertexCounts;
	rexPropertyPrefixBoundName              				propertyPrefixBoundName;
	rexPropertyDataRelativeToTaggedRexRootBound				propertyDataRelativeToTaggedRexRootBound;
	rexPropertyQuadrifyGeometryBounds						propertyQuadrifyGeometryBounds;
	rexPropertyApplyBulletShrinkMargin						propertyApplyBulletShrinkMargin;

	// fragment properties
	rexPropertyFragmentBreakableGlassNode					propertyBreakableGlassNode;

	// skeleton properties
	rexPropertyIgnoreSkeletonRootXform						propertyIgnoreSkeletonRootXform;
	rexPropertySkeletonSetType								propertySkeletonSetType;
	rexPropertySkeletonExportEulers							propertyExportEulers;
	rexPropertySkeletonUsesOnlyJoints						propertySkeletonUsesOnlyJoints;
	rexPropertySkeletonWriteLimitAndLockInfo				propertySkeletonWriteLimitAndLockInfo;
	rexPropertyEnablePurgeDegenerateSkeleton				propertyEnablePurgeDegenerateSkeleton;
	rexPropertySkeletonSetJointLimits						propertySkeletonSetJointLimits;
	rexPropertyBoundSpecialFlagMappings						propertyBoundSpecialFlagMappings;
	rexPropertyErrorOnFoundJointOrientationData				propertyErrorOnFoundJointOrientationData;
	rexPropertySpewOnLockedRootBone							propertySpewOnLockedRootBone;

	// animation properties
	rexPropertyAnimationMoverNode							 propertyAnimMover;
	rexPropertyAnimationBlendShapeNode						 propertyAnimBlendShape;
	rexPropertyAnimationParentNode							 propertyAnimParent;
	rexPropertyAnimationSegmentStartFrame					 propertyAnimStartFrame;
	rexPropertyAnimationSegmentEndFrame						 propertyAnimEndFrame;
	rexPropertyAnimationSegmentAutoplay						 propertyAnimAutoplay;
	rexPropertyAnimationSegmentLooping						 propertyAnimLooping;
	rexPropertyAnimationSegmentDontExportRoot				 propertyDontExportRoot;
	rexPropertyAnimationSkipLockedChannel					 propertySkipLockedChannel;
	rexPropertyAnimationSerializeAnimCurves					 propertySerializeAnimCurves;
	rexPropertyAnimationSetExportAllUntaggedChunksSeperately propertyAnimExportAllUntaggedChunksSeperately;
	rexPropertyAnimationUseTimeLine							 propertyAnimationUseTimeLine;
	rexPropertyAnimationExportMoverChannels					 propertyExportMoverChannels;
	rexPropertyAnimationGimbelLockFix						 propertyGimbelLockFix;
	rexPropertyAnimationCompressionErrorTolerance			 propertyAnimationCompressionErrorTolerance;
	rexPropertyAnimationTranslateScale						 propertyAnimationTranslateScale;
	rexPropertyApplyMatrixToAnimation						 propertyApplyMatrixToAnimation;
	rexPropertyAnimationNormalizeRoot						 propertyAnimationNormalizeRoot;
	rexPropertyAnimationPrefixSegNameWithFilename			 propertyAnimationPrefixSegNameWithFilename;
	rexPropertyAnimationFramesPerSecond						 propertyAnimationFramesPerSecond;
	rexPropertyAnimationAllowMultipleMovers					 propertyAnimationAllowMultipleMovers;
	rexPropertyAnimationCompressionCost						 propertyAnimationCompressionCost;
	rexPropertyAnimationDecompressionCost					 propertyAnimationDecompressionCost;
	rexPropertyAnimationMaximumBlockSize					 propertyAnimationMaximumBlockSize;
	rexPropertyAnimationAnimNormalMapNode					 propertyAnimNormalMap;
	rexPropertyAnimationExprControlNode						 propertyAnimationExprControlNode;

	// animation properties: for anim curve fitting
	rexPropertyAnimationSampleRate							propertyAnimSampleRate;
	rexPropertyAnimationCurveFittingTolerance				propertyAnimCurveFittingTolerance;
	rexPropertyAnimationProcessNonJointTransformAndRotates	propertyProcessNonJointTransformAndRotates;

	// properties applicable to both skeleton & animation
	rexPropertyChannelsToWrite								propertyChannelsToWrite;
	rexPropertyAnimExportCtrlFile							propertyAnimExportCtrlFile;

	// entity properties
	rexPropertyEntityTypeFileName							propertyEntityTypeFileName;
	rexPropertyEntityAddTextureSet							propertyEntityAddTextureSet;
	rexPropertyEntityComposeBounds							propertyEntityComposeBounds;
	rexPropertyEntityFragmentHasDamage						propertyFragmentHasDamage;
	rexPropertyEntityFragmentWriteMeshes					propertyFragmentWriteMeshes;
	rexPropertyEntityWriteBoundSituations					propertyEntityWriteBoundSituations;
	rexPropertyEntityWriteBoundFlags						propertyEntityWriteBoundFlags;
	rexPropertyEntitySkipBoundSectionOfTypeFile				propertyEntitySkipBoundSectionOfTypeFile;
	rexPropertyEntityEntityClass							propertyEntityEntityClass;
	rexPropertyEntityWriteLocatorsAsSeparateFiles			propertyEntityWriteLocatorsAsSeparateFiles;
	rexPropertyEntityWriteCurvesAsSeparateFiles				propertyEntityWriteCurvesAsSeparateFiles;
	rexPropertyEntityWriteMeshGroupIndex					propertyWriteMeshGroupIndex;
	rexPropertyEntityWriteSeparateShadingGroupFile			propertyWriteSeparateShadingGroupFile;
	rexPropertySplitLODGroupsByFlags						propertySplitLODGroupsByFlags;
	rexPropertyBoundGeometryExportType						propertyBoundGeometryExportType;
	rexPropertyUseNonOptimizedMTLCompare					propertyUseNonOptimizedMTLCompare;
	rexPropertyUseLODMeshTypes								propertyUseLODMeshTypes;
	rexPropertySerializeVersionTwoNurbsCurves				propertySerializeVersionTwoNurbsCurves;
	rexPropertyErrorOnClothWithoutVertexTuning				propertyErrorOnClothWithoutVertexTuning;

	// edgemodel properties
	rexPropertyAllowMultipleEdgeModels						propertyAllowMultipleEdgeModels;

	// cutscene properties
	rexPropertyEventType									propertyEventType;
	rexPropertyCSStartFrame									propertyCSStartFrame;
	rexPropertyCSEndFrame									propertyCSEndFrame;
	rexPropertyEventString									propertyEventString;
	rexPropertyEventFloat									propertyEventFloat;
	rexPropertyAudioStream									propertyAudioStream;
	rexPropertyEmitterName									propertyEmitterName;
	rexPropertyEmitterType									propertyEmitterType;
	rexPropertyParentActor									propertyParentActor;
	rexPropertyClipNear										propertyClipNear;
	rexPropertyClipFar										propertyClipFar;
	rexPropertyEntityReferenced								propertyEntityReferenced;
	rexPropertyCSBookmarks									propertyCSBookmarks;	

	// Curve properties
	rexPropertyCurvesRelativeToBone							propertyCurvesRelativeToBone;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// module registration.  modules are simply combinations of a filter, a converter, and/or a serializer.
	// note that not all modules have their own serializers (i.e. locator).  This is because locators 
	// themselves do not have a file format, but are rather included in parent object file formats.
	// entities and districts do not contain filters, because they are strictly dependent on their child 
	// modules/objects and do not directly process input objects on their own.
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	// RAGE base modules
	shell.RegisterModule( "Entity", converterEntity, serializerEntity, true, 1 ); // entities can only have one object parented!
	shell.RegisterModule( "District", converterLevel, serializerLevel, true );
	shell.RegisterModule( "LevelInstance", filterLevelInstance, converterLevelInstance, true );
	shell.RegisterModule( "RoomType", converterRoomTypes, serializerRoomTypes, true );
	// shell.RegisterModule( "FX", converterFX );
	shell.RegisterModule( "FragmentDamage", converterFragmentDamage, true );
	shell.RegisterModule( "Fragment", converterFragment, true );
	shell.RegisterModule( "Skeleton", filterBoneRoot, converterSkeleton, processorPurgeDegenerateSkeleton, serializerSkeleton, true );
	shell.RegisterModule( "Animation", filterBoneRoot, converterAnimation, serializerAnimation, true );  // animation looks at all animation in (sub-)scene
	shell.RegisterModule( "TextureAnimation", filterMeshWithTextureAnimation, converterTextureAnimation, serializerAnimation, true );  
	shell.RegisterModule( "AnimationSegment", /*filterBoneRoot,*/ converterAnimationSegment, false );  // animation segments contains references to part(s) of animation
	shell.RegisterModule( "TextureAnimationSegment", converterTextureAnimationSegment, false );  // animation segments contains references to part(s) of animation
	shell.RegisterModule( "Locator", filterLocator, converterLocator, true );
	shell.RegisterModule( "SoundLocator", filterLocator, converterSoundLocator, true );
	shell.RegisterModule( "CharacterInstanceLocator", filterLocator, converterCharacterInstanceLocator, true );
	shell.RegisterModule( "Curve", filterNurbsCurve, converterNurbsCurve, true );
	shell.RegisterModule( "Lights", filterLight, converterLight, true );

	shell.RegisterModule( "Cloth", converterClothMeshGroup, true);
	shell.RegisterModule( "CharCloth", converterCharClothMeshGroup, true);
	shell.RegisterModule( "ClothPiece", converterClothMeshInfo, false);
	
	shell.RegisterModule( "Hair", converterHairMeshGroup, true);
	shell.RegisterModule( "HairPiece", converterHairMeshInfo, false);

	shell.RegisterModule( "Primitive", filterPrimitive, converterPrimitive, false );

	// RAGE cutscene modules
	shell.RegisterModule( "Cutscene", converterCutscene, serializerCutscene, true );
	shell.RegisterModule( "CSCamera", filterCamera, converterCSCamera, serializerCSCamera, true );
	shell.RegisterModule( "CSEvent", converterCSEvent, true );

	shell.RegisterModule( "Bound", filterBoundHierarchy, converterBoundHierarchy, processorBoundCombiner, serializerBound, true );
	shell.RegisterModule( "Mesh", filterMeshHierarchy, converterMeshHierarchy, processorMeshCombiner, serializerMesh, true );
	shell.RegisterModule( "EdgeModel", filterMeshHierarchy, converterEdgeModel, serializerEdgeModel, true );
	shell.RegisterModule( "Occluder", filterMeshHierarchy, converterOccluders, serializerMeshUnprocessed, true );
	shell.RegisterModule( "Portal",  filterMeshHierarchy, converterPortals, serializerMeshUnprocessed, true );
	shell.RegisterModule( "RoomVolume", filterMeshHierarchy, converterRoomVolumes, serializerMeshUnprocessed, true );
	shell.RegisterModule( "Water",  filterMeshHierarchy, converterWater, serializerMeshUnprocessed, true );
	// shell.RegisterModule( "Proxy", filterMeshHierarchy, converterProxyMesh, serializerMesh, true );

	shell.AddToModuleFilter("Mesh", "Bound");
	shell.AddToModuleFilter("Mesh", "EdgeModel");
	shell.AddToModuleFilter("Mesh", "Occluder");
	shell.AddToModuleFilter("Mesh", "Portal");
	shell.AddToModuleFilter("Mesh", "RoomVolume");
	shell.AddToModuleFilter("Mesh", "Water");

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// properties registration.  these basically just connect a string to a property previously declared.  
	// in maya, RexExportData nodes' attributes' names are compared to this string, and the property applied
	// (using attribute's values and/or connected nodes) when there is a match.
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

	// RAGE basic
	shell.RegisterProperty( "Nodes", propertyInputNodes ); 	
	shell.RegisterProperty( "Name", propertyName );
	shell.RegisterProperty( "OutputPath", propertyOutputPath );
	shell.RegisterProperty( "ExportData", propertyExportData );
	shell.RegisterProperty( "BoneTag", propertyBoneTag );
	shell.RegisterProperty( "Flags", propertySpecialFlag );
	shell.RegisterProperty( "UseChildrenOfLevelInstanceNodes", propertyUseChildrenOfLevelInstanceNodes );
	shell.RegisterProperty( "UnitConversion", propertyUnitConversion );
	shell.RegisterProperty( "UseBoneIDs", propertyUseBoneIDs );
	shell.RegisterProperty( "IgnoreNullJoints", propertyIgnoreNullJoints );

	shell.RegisterProperty( "ConvInstanceAttributes", propertyConverterInstanceAttributes );
	shell.RegisterProperty( "AuthoredOrientation", propertyAuthoredOrientation);
	shell.RegisterProperty( "GenBoneIdFromFullPath", propertyGenBoneIdFromFullPath);

	shell.RegisterProperty( "ClothBounds", propertyClothMeshInfoBounds);
	shell.RegisterProperty( "ClothMesh", propertyClothMeshInfoMesh);

	shell.RegisterProperty( "HairMesh", propertyHairMeshInfoMesh);

	// RAGE drawable mesh
	shell.RegisterProperty( "TextureOutputPath", propertyTextureOutputPath );	
	shell.RegisterProperty( "MaxMemorySize", propertyMeshMaxMemorySize );
	shell.RegisterProperty( "ExportRootBoneMeshesCentered", propertyMeshExportRootBoneMeshesCentered );
	shell.RegisterProperty( "MaxPolyCountToCombine", propertyMeshMaxPolyCountToCombine );
	shell.RegisterProperty( "MaxPolyCountOfCombined", propertyMeshMaxPolyCountOfCombinedMesh );
	shell.RegisterProperty( "MaxDistanceToCombine", propertyMeshCombineMaxDistance );	
	shell.RegisterProperty( "MaxLODThresholdRatio", propertyMeshCombineMaxLODThresholdRatio );		
	shell.RegisterProperty( "CombineAcrossBones", propertyMeshCombineAcrossBones );	
	shell.RegisterProperty( "CombineMergeMaterials", propertyMeshCombineMergeMaterials );		
	shell.RegisterProperty( "ExportTextures", propertyMeshSetExportTextures );
	shell.RegisterProperty( "ExportEntityTextureList", propertyMeshSetExportEntityTextureList);
	shell.RegisterProperty( "EntityTextureListKeepExtensions", propertyMeshEntityTextureListKeepExtensions);
	shell.RegisterProperty( "CheckVertexNumWeights", propertyMeshCheckVertexNumWeights );
	shell.RegisterProperty( "ExportBlendShapes", propertyMeshExportBlendShapes );
	shell.RegisterProperty( "BlindDataToExport", propertyMeshBlindDataToExport );
	shell.RegisterProperty( "DataRelativeToTaggedRexRootMesh", propertyDataRelativeToTaggedRexRootMesh );
	shell.RegisterProperty( "DisableTriStripping", propertyDisableTriStripping );
	shell.RegisterProperty( "MaxMatricesPerMaterial", propertySetMaxMatricesPerMaterial );
	shell.RegisterProperty( "UseNonOptimizedMTLCompareMesh", propertyUseNonOptimizedMTLCompareMesh );
	
	// RAGE skeleton
	shell.RegisterProperty( "IgnoreSkeletonRootXform", propertyIgnoreSkeletonRootXform);
	shell.RegisterProperty( "ExportEulers", propertyExportEulers );
	shell.RegisterProperty( "SkeletonUsesOnlyJoints", propertySkeletonUsesOnlyJoints );
	shell.RegisterProperty( "WriteLimitAndLockInfo", propertySkeletonWriteLimitAndLockInfo );
	shell.RegisterProperty( "SkeletonType", propertySkeletonSetType );
	shell.RegisterProperty( "EnablePurgeDegenerateSkeleton", propertyEnablePurgeDegenerateSkeleton );
	shell.RegisterProperty( "JointLimits", propertySkeletonSetJointLimits );
	shell.RegisterProperty( "BoundSpecialFlagMappings", propertyBoundSpecialFlagMappings );
	shell.RegisterProperty( "ErrorOnFoundJointOrientationData", propertyErrorOnFoundJointOrientationData );
	shell.RegisterProperty( "SpewOnLockedRootBone", propertySpewOnLockedRootBone );

	// RAGE animation
	shell.RegisterProperty( "StartFrame", propertyAnimStartFrame );
	shell.RegisterProperty( "EndFrame", propertyAnimEndFrame );
	shell.RegisterProperty( "Mover", propertyAnimMover );
	shell.RegisterProperty( "BlendShape", propertyAnimBlendShape );
	shell.RegisterProperty( "Parent", propertyAnimParent );	
	shell.RegisterProperty( "Autoplay", propertyAnimAutoplay );
	shell.RegisterProperty( "Looping", propertyAnimLooping );
	shell.RegisterProperty( "DontExportRoot", propertyDontExportRoot );
	shell.RegisterProperty( "SkipLockedChannel", propertySkipLockedChannel );
	shell.RegisterProperty( "SerializeAnimCurves", propertySerializeAnimCurves );
	shell.RegisterProperty( "ExportAllUntaggedChunksSeperately", propertyAnimExportAllUntaggedChunksSeperately );
	shell.RegisterProperty( "UseTimeLine", propertyAnimationUseTimeLine );
	shell.RegisterProperty( "CompressionErrorTolerance", propertyAnimationCompressionErrorTolerance );
	shell.RegisterProperty( "AnimTranslateScale", propertyAnimationTranslateScale );
	shell.RegisterProperty( "ApplyMatrixToAnimation", propertyApplyMatrixToAnimation );
	shell.RegisterProperty( "NormalizeRoot", propertyAnimationNormalizeRoot );
	shell.RegisterProperty( "PrefixSegmentNameWithFilename", propertyAnimationPrefixSegNameWithFilename );
	shell.RegisterProperty( "FramesPerSecond", propertyAnimationFramesPerSecond );
	shell.RegisterProperty( "AllowMultipleMovers", propertyAnimationAllowMultipleMovers );
	shell.RegisterProperty( "CompressionCost", propertyAnimationCompressionCost );
	shell.RegisterProperty( "DecompressionCost", propertyAnimationDecompressionCost );
	shell.RegisterProperty( "MaximumBlockSize", propertyAnimationMaximumBlockSize );
	shell.RegisterProperty( "AnimNormalMap", propertyAnimNormalMap );
	shell.RegisterProperty( "ExpressionControlNodes", propertyAnimationExprControlNode );

	// RAGE animation: for anim curve fitting
	shell.RegisterProperty( "SampleRate", propertyAnimSampleRate );
	shell.RegisterProperty( "CurveFittingTolerance", propertyAnimCurveFittingTolerance );

	// RAGE skeleton & animation
	shell.RegisterProperty( "ChannelsToExport", propertyChannelsToWrite );
	shell.RegisterProperty( "AnimExportCtrlFile", propertyAnimExportCtrlFile);
	shell.RegisterProperty( "DoMatrixProcessing", propertyProcessNonJointTransformAndRotates );
	shell.RegisterProperty( "ExportMoverChannels", propertyExportMoverChannels );
	shell.RegisterProperty( "GimbelLockFix", propertyGimbelLockFix );

	// RAGE physics bounds
	shell.RegisterProperty(	"ExportAsBvhGrid", propertyBoundSetGeometryBoundsExportAsBvhGrid );
	shell.RegisterProperty( "ExportAsBvhGeom", propertyBoundSetGeometryBoundsExportAsBvhGeom );
	shell.RegisterProperty( "GridCellSize", propertyBoundSetGridCellSize );
	shell.RegisterProperty( "ZeroPrimitiveCentroids", propertyZeroPrimitiveCentroids );
	shell.RegisterProperty( "PrimitiveBoundCompositionMaxDistance", propertyBoundSetPrimBoundCompMaxDist );
	shell.RegisterProperty( "ExportPrimitivesInBvh", propertyBoundExportPrimsInBvh );
	shell.RegisterProperty( "PrimitiveBoundCompositionMaxDistanceToRadiusRatio", propertyBoundSetPrimBoundCompMaxDistToRadRatio );
	shell.RegisterProperty( "CheckBoundVertexCounts", propertyCheckBoundVertexCounts );
	shell.RegisterProperty( "PrefixBoundName", propertyPrefixBoundName );
	shell.RegisterProperty( "DataRelativeToTaggedRexRootBound", propertyDataRelativeToTaggedRexRootBound );
	shell.RegisterProperty( "QuadrifyGeometryBounds", propertyQuadrifyGeometryBounds );
	shell.RegisterProperty( "ApplyBulletShrinkMargin", propertyApplyBulletShrinkMargin);
	shell.RegisterProperty( "UseBoundFiltering", propertyUseBoundFiltering);
	shell.RegisterProperty( "BoundFilteringBoundTypes", propertyBoundFilteringBoundTypes);
	
	// RAGE fragments
	shell.RegisterProperty( "BreakableGlassNode", propertyBreakableGlassNode);

	// RAGE entity
	shell.RegisterProperty( "EntityTypeFileName", propertyEntityTypeFileName );
	shell.RegisterProperty( "TextureSet", propertyEntityAddTextureSet );
	shell.RegisterProperty( "ComposeBounds", propertyEntityComposeBounds );
	shell.RegisterProperty( "FragmentHasDamage", propertyFragmentHasDamage );
	shell.RegisterProperty( "FragmentWriteMeshes", propertyFragmentWriteMeshes );
	shell.RegisterProperty( "WriteBoundSituations", propertyEntityWriteBoundSituations );
	shell.RegisterProperty( "WriteBoundFlags", propertyEntityWriteBoundFlags);
	shell.RegisterProperty( "SkipBoundSectionOfTypeFile", propertyEntitySkipBoundSectionOfTypeFile );
	shell.RegisterProperty( "EntityClass", propertyEntityEntityClass );
	shell.RegisterProperty( "WriteLocatorsAsSeparateFiles", propertyEntityWriteLocatorsAsSeparateFiles);
	shell.RegisterProperty( "WriteCurvesAsSeparateFiles", propertyEntityWriteCurvesAsSeparateFiles);
	shell.RegisterProperty( "WriteMeshGroupIndex", propertyWriteMeshGroupIndex);
	shell.RegisterProperty( "WriteSeparateShadingGroupFile", propertyWriteSeparateShadingGroupFile);
	shell.RegisterProperty( "SplitLODGroupsByFlags", propertySplitLODGroupsByFlags);
	shell.RegisterProperty( "BoundGeometryExportType", propertyBoundGeometryExportType);
	shell.RegisterProperty( "UseNonOptimizedMTLCompare", propertyUseNonOptimizedMTLCompare);
	shell.RegisterProperty( "LODMeshFilterMappings", propertyLODMeshFilterMappings);
	shell.RegisterProperty( "UseLODMeshTypes", propertyUseLODMeshTypes);
	shell.RegisterProperty( "SerializeVersionTwoNurbsCurves", propertySerializeVersionTwoNurbsCurves);
	shell.RegisterProperty( "ErrorOnClothWithoutVertexTuning", propertyErrorOnClothWithoutVertexTuning );

	// RAGE edgemodels
	shell.RegisterProperty( "MultipleEdgeModels", propertyAllowMultipleEdgeModels );

	// RAGE cutscene
	shell.RegisterProperty( "EventType", propertyEventType );
	shell.RegisterProperty( "CSStartFrame", propertyCSStartFrame );
	shell.RegisterProperty( "CSEndFrame", propertyCSEndFrame );
	shell.RegisterProperty( "EventString", propertyEventString );
	shell.RegisterProperty( "EventFloat", propertyEventFloat );
	shell.RegisterProperty( "AudioStream", propertyAudioStream );
	shell.RegisterProperty( "EmitterName", propertyEmitterName );
	shell.RegisterProperty( "EmitterType", propertyEmitterType );
	shell.RegisterProperty( "ParentActor", propertyParentActor );
	shell.RegisterProperty( "ClipNear", propertyClipNear );
	shell.RegisterProperty( "ClipFar", propertyClipFar );
	shell.RegisterProperty( "Referenced", propertyEntityReferenced );
	shell.RegisterProperty( "CSBookmarks", propertyCSBookmarks );

	// Curve properties
	shell.RegisterProperty( "CurvesRelativeToBone", propertyCurvesRelativeToBone );
}

} // namespace rage

