#define REQUIRE_IOSTREAM

#include "rexMaya/mayahelp.h"
#include "rexMaya/mayaUtility.h"

#include "rexRAGE/serializerBound.h"
#include "rexRAGE/serializerEdgeModel.h"
#include "rexRAGE/serializerEntity.h"
#include "rexRAGE/serializerMesh.h"
#include "rexRAGE/serializerAnimation.h"
#include "rexRAGE/serializerSkeleton.h"
#include "rexBase/module.h"
#include "rexGeneric/objectEntity.h"
#include "rexGeneric/objectMesh.h"
#include "rexGeneric/processorMeshCombiner.h"
#include "rexMaya/objectMDagPath.h"
#include "rexMaya/shell.h"
#include "rexMayaToGeneric/converterAnimation.h"
#include "rexMayaToGeneric/converterLocator.h"
#include "rexMayaToGeneric/converterMesh.h"
#include "rexMayaToGeneric/converterSkeleton.h"
#include "rexMayaRAGE/propertyBasic.h"

#include <string>

/////////////////////////////////////////////////////////////////////////////////////

namespace rage {

rexResult  rexPropertyUseChildrenOfLevelInstanceNodes ::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexSerializerRAGEEntity* serEntity = dynamic_cast<rexSerializerRAGEEntity*>( module.m_Serializer );
	if( serEntity )
	{
		serEntity->SetUseChildrenOfLevelInstanceNodes( value.toBool( serEntity->GetUseChildrenOfLevelInstanceNodes() ) );
		return rexResult( rexResult::PERFECT );
	}
	rexSerializerRAGEMesh* serMesh = dynamic_cast<rexSerializerRAGEMesh*>( module.m_Serializer );
	if( serMesh )
	{
		serMesh->SetUseChildrenOfLevelInstanceNodes( value.toBool( serMesh->GetUseChildrenOfLevelInstanceNodes() ) );
		return rexResult( rexResult::PERFECT );
	}
	rexSerializerRAGEPhysicsBound* serBound = dynamic_cast<rexSerializerRAGEPhysicsBound*>( module.m_Serializer );
	if( serBound )
	{
		serBound->SetUseChildrenOfLevelInstanceNodes( value.toBool( serBound->GetUseChildrenOfLevelInstanceNodes() ) );
		return rexResult( rexResult::PERFECT );
	}
	rexSerializerRAGEEdgeModel* serEM = dynamic_cast<rexSerializerRAGEEdgeModel*>( module.m_Serializer );
	if( serEM )
	{
		serEM->SetUseChildrenOfLevelInstanceNodes( value.toBool( serEM->GetUseChildrenOfLevelInstanceNodes() ) );
		return rexResult( rexResult::PERFECT );
	}
	rexConverterAnimation* _rexConverterAnimation = dynamic_cast<rexConverterAnimation*>( module.m_Converter );
	if( _rexConverterAnimation )
	{
		_rexConverterAnimation->SetUseChildrenOfLevelInstanceNodes( value.toBool( _rexConverterAnimation->GetUseChildrenOfLevelInstanceNodes() ) );
		return rexResult( rexResult::PERFECT );
	}
	rexConverterLocator* _rexConverterLocator = dynamic_cast<rexConverterLocator*>( module.m_Converter );
	if( _rexConverterLocator )
	{
		_rexConverterLocator->SetUseChildrenOfLevelInstanceNodes( value.toBool( _rexConverterLocator->GetUseChildrenOfLevelInstanceNodes() ) );
		return rexResult( rexResult::PERFECT );
	}
	rexConverterSkeleton* _rexConverterSkeleton = dynamic_cast<rexConverterSkeleton*>( module.m_Converter );
	if( _rexConverterSkeleton )
	{
		_rexConverterSkeleton->SetUseChildrenOfLevelInstanceNodes( value.toBool( _rexConverterSkeleton->GetUseChildrenOfLevelInstanceNodes() ) );
		return rexResult( rexResult::PERFECT );
	}
	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult  rexPropertyChannelsToWrite::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterAnimation* animConv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );
	rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
	rexSerializerRAGESkeleton* skelSer = dynamic_cast<rexSerializerRAGESkeleton*>( module.m_Serializer );

	if( value.toString() )
	{
		atString channelsString(value.toString());
		int charCount = channelsString.GetLength();
		atString currChannelInputString, currChannelOutputString;
		bool readingInputString = true, readingChannelID = false, appliesToRootsOnly = false;
		int currChannelGroup = -1, currChannelID = 0;
		atString channelIDBuffer;

		for( int a = 0; a < charCount; a++ )
		{
			if( channelsString[ a ] == '(' )
			{
				if( animSer )
					currChannelGroup = animSer->AddChannelGroup();
				if( skelSer )
					currChannelGroup = skelSer->AddChannelGroup();
				if( animConv )
					currChannelGroup = animConv->AddChannelGroup();
			}
			else if( channelsString[ a ] == ')' )
			{
				currChannelGroup = -1;
			}
			else if( channelsString[ a ] == '[' )
			{
				readingChannelID = true;
			}
			else if( channelsString[ a ] == ']' )
			{
				readingChannelID = false;
				currChannelID = atoi( channelIDBuffer );
				channelIDBuffer.Reset();
			}
			else if( readingChannelID )
			{
				channelIDBuffer += channelsString[ a ];
			}
			else if( readingInputString )
			{
				if(( channelsString[ a ] == ';' ) || ( a == ( charCount - 1 ) ))
				{					
					if( animSer )
						animSer->AddChannelToWrite( currChannelInputString, currChannelInputString, currChannelID, appliesToRootsOnly, currChannelGroup ); // input and output name same
					if( skelSer )
						skelSer->AddChannelToWrite( currChannelInputString, currChannelInputString, currChannelID, appliesToRootsOnly, currChannelGroup ); // input and output name same
					if( animConv )
						animConv->AddChannelToWrite( currChannelInputString, currChannelInputString, currChannelID, appliesToRootsOnly, currChannelGroup ); // input and output name same
					currChannelID++;
					currChannelInputString.Reset();
					appliesToRootsOnly = false;
				}
				else if( channelsString[ a ] == '|' )
				{
					readingInputString = false;
				}
				else if( channelsString[ a ] == '*' )
				{
					appliesToRootsOnly = true;
				}
				else
				{
					currChannelInputString += channelsString[ a ];
				}
			}
			else
			{
				if( channelsString[ a ] == ';' || ( a == ( charCount - 1 ) ))
				{					
					if( animSer )
						animSer->AddChannelToWrite( currChannelInputString, currChannelOutputString, currChannelID, appliesToRootsOnly, currChannelGroup ); 
					if( skelSer )
						skelSer->AddChannelToWrite( currChannelInputString, currChannelOutputString, currChannelID, appliesToRootsOnly, currChannelGroup ); 
					if( animConv )
						animConv->AddChannelToWrite( currChannelInputString, currChannelOutputString, currChannelID, appliesToRootsOnly, currChannelGroup ); 
					currChannelID++;
					currChannelInputString.Reset();
					currChannelOutputString.Reset();
					readingInputString = true;
					appliesToRootsOnly = false;
				}
				else if( channelsString[ a ] == '*' )
				{
					appliesToRootsOnly = true;
				}
				else
				{
					currChannelOutputString += channelsString[ a ];
				}
			}
		}			
	}		
	return rexResult( rexResult::PERFECT );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyAnimExportCtrlFile::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexResult result = rexResult::PERFECT;

	rexConverterAnimation* animConv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );
	rexSerializerRAGEAnimation* animSer = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
	rexSerializerRAGESkeleton* skelSer = dynamic_cast<rexSerializerRAGESkeleton*>( module.m_Serializer );

	if( value.toString() )
	{
		if(animConv)
		{
			bool bRes = animConv->LoadAnimExportCtrlFile( (const char*)(value.toString()) );
			if(!bRes)
			{
				result = rexResult::ERRORS;
				result.AddMessage("Failed to load animation export control file : '%s'", (const char*)(value.toString()));
				return result;
			}
			animConv->SetUseAnimCtrlExportFile(true);
		}

		if(animSer)
		{
			bool bRes = animSer->LoadAnimExportCtrlFile( (const char*)(value.toString()) );
			if(!bRes)
			{
				result = rexResult::ERRORS;
				result.AddMessage("Failed to load animation export control file : '%s'", (const char*)(value.toString()));
				return result;
			}
			animSer->SetUseAnimCtrlExportFile(true);
		}

		if(skelSer)
		{
			bool bRes = skelSer->LoadAnimExportCtrlFile( (const char*)(value.toString()) );
			if(!bRes)
			{
				result = rexResult::ERRORS;
				result.AddMessage("Failed to load animation export control file : '%s'", (const char*)(value.toString()));
				return result;
			}
			skelSer->SetUseAnimCtrlExportFile(true);
		}
	}

	return result;
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyAnimationUseTimeLine::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverterAnimation* conv = dynamic_cast<rexConverterAnimation*>( module.m_Converter );
	rexSerializerRAGEAnimation* ser = dynamic_cast<rexSerializerRAGEAnimation*>( module.m_Serializer );
	if( ! ( conv || ser ) )
		return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );

	if( conv )
		conv->SetUseTimeLine( value.toBool( conv->GetUseTimeLine() ) );	
	if( ser )
		ser->SetUseTimeLine( value.toBool( ser->GetUseTimeLine() ) );	

	return rexResult( rexResult::PERFECT );
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyConverterInstanceAttributes::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverter* _rexConv = dynamic_cast<rexConverter*>( module.m_Converter );
	if( _rexConv )
	{
		if( value.toString() )
		{
			std::string instAttrStr = value.toString();
			unsigned int startParen = instAttrStr.find_first_of('(');
			unsigned int endParen = instAttrStr.find_last_of(')');
			if( (startParen == std::string::npos) ||
				(endParen == std::string::npos) )
			{
				return rexResult( rexResult::WARNINGS, rexResult::INVALID_INPUT );
			}
			unsigned int cIdx = startParen+1;
			while(cIdx < endParen)
			{
				atString attrName;
				atString attrType;

				while( (instAttrStr[cIdx] != '|') &&
					   (instAttrStr[cIdx] != ')') )
				{
					attrName += instAttrStr[cIdx++];
				}
				cIdx++;
				while( (instAttrStr[cIdx] != ';') &&
					   (instAttrStr[cIdx] != ')') )
				{
					attrType += instAttrStr[cIdx++];
				}
				cIdx++;

				_rexConv->AppendInstanceAttr(attrName, attrType);
			}
		}
	}
	return rexResult::PERFECT;
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyAuthoredOrientation::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverter* _rexConv = dynamic_cast<rexConverter*>( module.m_Converter );
	if( _rexConv )
	{
		_rexConv->SetAuthoredOrientation( value.toBool( _rexConv->GetAuthoredOrientation() ) );	
	}
	return rexResult::PERFECT;
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyGenBoneIdFromFullPath::SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& /*objects*/ ) const
{
	rexConverter* _rexConv = dynamic_cast<rexConverter*>( module.m_Converter );
	if( _rexConv )
	{
		_rexConv->SetGenBoneIdFromFullPath( value.toBool( _rexConv->GetGenBoneIdFromFullPath() ) );	
	}
	return rexResult::PERFECT;
}

/////////////////////////////////////////////////////////////////////////////////////

rexResult rexPropertyBoundSetPrimitiveBoundExportPrimitivesInBvh::SetupModuleInstance( rexModule &module, rexValue value, const atArray<rexObject*>& /*objects*/) const
{
	int processorCount = module.m_Processors.GetCount();
	for ( int i = 0; i < processorCount; ++i )
	{
		rexProcessorMeshCombiner* processor = dynamic_cast< rexProcessorMeshCombiner* >( module.m_Processors[ i ] );
		if ( processor )
		{
			processor->SetPrimitiveBoundExportPrimitivesInBvh( value.toBool( processor->GetPrimitiveBoundExportPrimitivesInBvh() ) );
		}
	}

	rexSerializerRAGEPhysicsBound* ser = dynamic_cast< rexSerializerRAGEPhysicsBound* >( module.m_Serializer );
	if ( ser )
	{
		ser->SetPrimitiveBoundExportPrimitivesInBvh( value.toBool( ser->GetPrimitiveBoundExportPrimitivesInBvh() ) );
	}

	return rexResult( rexResult::WARNINGS, rexResult::TYPE_INCOMPATIBLE );
} 

} // namespace rage
