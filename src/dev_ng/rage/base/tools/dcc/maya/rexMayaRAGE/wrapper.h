// 
// rexMayaRAGE/wrapper.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Maya RAGE REX Wrapper
//
// This file provides hooks to Maya and registers all modules and properties required by RAGE base 
// functionality
// 
//////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAROCKSTAR_WRAPPER_H__
#define __REX_MAYAROCKSTAR_WRAPPER_H__

#include "rexMaya/wrapper.h"

namespace rage {

class rexWrapperMayaRAGE : public rexWrapperMaya
{
public:
	static void	*creator()			{ return new rexWrapperMayaRAGE; }
	
	virtual	void RegisterModules( rexShellMaya& shell );
};

} // namespace rage


#endif
