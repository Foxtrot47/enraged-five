// 
// rexMayaRAGE/propertyBasic.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

/////////////////////////////////////////////////////////////////////////////////////
//
//  REX Maya RAGE Library
//
/////////////////////////////////////////////////////////////////////////////////////
//
//	DEPENDENCIES:
//		RAGE 
//			atl, core, data
//		REX
//			datBase Lib, Generic Lib, Maya Lib
//		MAYA 4.5 API
//
/////////////////////////////////////////////////////////////////////////////////////
//  CLASSES:
/////////////////////////////////////////////////////////////////////////////////////
//
//		rexPropertyUseChildrenOfLevelInstanceNodes
//			 -- allows for setting whether to consider level instance objects for export
//
////////////////////////////////////////////////////////////////////////////////////////

#ifndef __REX_MAYAAGE_PROPERTYBASIC_H__
#define __REX_MAYAAGE_PROPERTYBASIC_H__

/////////////////////////////////////////////////////////////////////////////////////

#include "rexBase/property.h"

namespace rage {

class rexModule;

/////////////////////////////////////////////////////////////////////////////////////


class rexPropertyUseChildrenOfLevelInstanceNodes : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyUseChildrenOfLevelInstanceNodes; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyChannelsToWrite : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyChannelsToWrite; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyAnimExportCtrlFile : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyAnimExportCtrlFile; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyAnimationUseTimeLine : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyAnimationUseTimeLine; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyConverterInstanceAttributes : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyConverterInstanceAttributes; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyAuthoredOrientation : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyAuthoredOrientation; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyGenBoneIdFromFullPath : public rexProperty
{
public:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects  ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyGenBoneIdFromFullPath; }
};

/////////////////////////////////////////////////////////////////////////////////////

class rexPropertyBoundSetPrimitiveBoundExportPrimitivesInBvh : public rexProperty
{
protected:
	virtual rexResult		SetupModuleInstance( rexModule& module, rexValue value, const atArray<rexObject*>& objects ) const;
	virtual rexProperty*	CreateNew() const { return new rexPropertyBoundSetPrimitiveBoundExportPrimitivesInBvh; }
};

/////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif
