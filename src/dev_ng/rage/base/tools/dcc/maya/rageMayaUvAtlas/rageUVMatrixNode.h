// -----------------------------------------------------------------------------
// $Workfile: rageUVMatrixNode.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This plugin uses the directx atlas UV creation libraries
// -----------------------------------------------------------------------------

#ifndef RSV_UVMATRIX_NODE_H
#define RSV_UVMATRIX_NODE_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxNode.h>
#include <maya/MFnStringData.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnMeshData.h>

#include <d3dx9mesh.h>
#include <WinDef.h>

class MArgList;
#pragma warning(pop)

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class rageUVMatrixNode : public MPxNode
// -----------------------------------------------------------------------------
{
public:
    rageUVMatrixNode();
    virtual
    ~rageUVMatrixNode();

    static
    void* creator();

    static
    MStatus	initialize();

    virtual 
    MStatus	compute( const MPlug& plug, MDataBlock& data );

    static MStatus setSettings(MObject modifierNode, const double* matrix, MString UVSetName);


private:

    static MObject mInMesh;
    static MObject mOutMesh;
    static MObject mUiMatrixElement0;
    static MObject mUiMatrixElement1;
    static MObject mUiMatrixElement2;
    static MObject mUiMatrixElement3;
    static MObject mUiMatrixElement4;
    static MObject mUiMatrixElement5;
    static MObject mUiUVSetName;
};

} // namespace rage


#endif // RSV_UVMATRIX_NODE_H
