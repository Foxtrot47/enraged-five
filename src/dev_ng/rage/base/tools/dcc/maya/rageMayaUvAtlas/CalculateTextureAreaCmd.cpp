// -----------------------------------------------------------------------------
// $Workfile: CalculateTextureAreaCmd.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\CalculateTextureAreaCmd\CalculateTextureAreaCmd.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This command calculates the area by summing up the poly`s areas.
// -----------------------------------------------------------------------------

#include "CalculateTextureAreaCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#pragma warning(pop)


namespace rage
{


// -----------------------------------------------------------------------------
void* CalculateTextureAreaCmd::creator()
// -----------------------------------------------------------------------------
{
    return new CalculateTextureAreaCmd();
}


//-----------------------------------------------------------------------------
CalculateTextureAreaCmd::CalculateTextureAreaCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
CalculateTextureAreaCmd::~CalculateTextureAreaCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
MStatus CalculateTextureAreaCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
    MDoubleArray result;
    MString UVSetName;
    MString* currentUVSet=NULL;

    for (unsigned int i=0; i< args.length(); i++)
    {
        if (args.asString(i)==MString("-us"))
        {
            i++;
            if (i< args.length())
            {
                UVSetName = args.asString(i);
                currentUVSet = &UVSetName;
            }
        }
        else
        {
            MSelectionList list;
            MDagPath dagPath;
            list.add(args.asString(i));
            list.getDagPath(0, dagPath);
            
            double area = 0;
            MItMeshPolygon polyIter(dagPath, MObject::kNullObj, &status );
            if ( MS::kSuccess != status) 
            {
                MString errorMsg("Failure in MItMeshPolygon initialization.");
                errorMsg+="('";
                errorMsg+=args.asString(i);
                errorMsg+="')";
                status.perror(errorMsg);
                return status;
            }

            for ( ; !polyIter.isDone() && (MS::kSuccess == status); polyIter.next() ) 
            {
                double thePolyArea=0;
                polyIter.getUVArea(thePolyArea, currentUVSet);
                area+=thePolyArea;
            }
            result.append(area);
        }
    }

    setResult(result);
    return status;
}


} // namespace rage
