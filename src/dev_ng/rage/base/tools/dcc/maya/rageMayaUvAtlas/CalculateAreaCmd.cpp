// -----------------------------------------------------------------------------
// $Workfile: CalculateAreaCmd.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\CalculateAreaCmd\CalculateAreaCmd.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This command calculates the area by summing up the poly`s areas.
// -----------------------------------------------------------------------------

#include "CalculateAreaCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#pragma warning(pop)


namespace rage
{


// -----------------------------------------------------------------------------
void* CalculateAreaCmd::creator()
// -----------------------------------------------------------------------------
{
    return new CalculateAreaCmd();
}


//-----------------------------------------------------------------------------
CalculateAreaCmd::CalculateAreaCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
CalculateAreaCmd::~CalculateAreaCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
MStatus CalculateAreaCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
    MDoubleArray result;

    for (unsigned int i=0; i< args.length(); i++)
    {
        MSelectionList list;
        MDagPath dagPath;
        list.add(args.asString(i));
        list.getDagPath(0, dagPath);
        
        double area = 0;
        MItMeshPolygon polyIter(dagPath, MObject::kNullObj, &status );
        if ( MS::kSuccess != status) 
        {
            MString errorMsg("Failure in MItMeshPolygon initialization.");
            errorMsg+="('";
            errorMsg+=args.asString(i);
            errorMsg+="')";
            status.perror(errorMsg);
            return status;
        }

        for ( ; !polyIter.isDone() && (MS::kSuccess == status); polyIter.next() ) 
        {
            double thePolyArea=0;
            polyIter.getArea(thePolyArea, MSpace::kWorld);
            area+=thePolyArea;
        }
        result.append(area);
    }

    setResult(result);
    return status;
}


} // namespace rage
