// -----------------------------------------------------------------------------
// $Workfile: GetPolygonCenter.h  $
//   Creator: Roman Rath
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: This comand returns the center point of the input object
// -----------------------------------------------------------------------------

#ifndef RSV_GET_POLYGON_CENTER_H
#define RSV_GET_POLYGON_CENTER_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#pragma warning(pop)

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class GetPolygonCenterCmd : public MPxCommand
// -----------------------------------------------------------------------------
{
public:
    GetPolygonCenterCmd();
    virtual
    ~GetPolygonCenterCmd();

    MStatus doIt(const MArgList& args);

    static
    void* creator();
};

} // namespace rage


#endif // RSV_GET_POLYGON_CENTER_H
