// -----------------------------------------------------------------------------
// $Workfile: GetNumUVsCmd.h  $
//   Creator: Erik Pojar
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This command returns how many UV's are in a UV set.
// -----------------------------------------------------------------------------

#ifndef RSV_GET_NUM_UVS_CMD_H
#define RSV_GET_NUM_UVS_CMD_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#pragma warning(pop)

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class GetNumUVsCmd : public MPxCommand
// -----------------------------------------------------------------------------
{
public:
    GetNumUVsCmd();
    virtual
    ~GetNumUVsCmd();

    MStatus doIt(const MArgList& args);

    static
    void* creator();

};

} // namespace rage


#endif // RSV_GET_NUM_UVS_CMD_H
