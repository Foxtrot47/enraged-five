// -----------------------------------------------------------------------------
// $Workfile: rageMayaUVAtlas.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\rageMayaUVAtlas\rageMayaUVAtlas.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This plugin uses the directx atlas UV creation libraries
// -----------------------------------------------------------------------------

#include "rageMayaUVAtlasMain.h"
#include "rageMayaUVAtlasCmd.h"
#include "rageMayaUVAtlasNode.h"
#include "rageUVMatrixNode.h"
#include "rageUVMatrixCmd.h"
#include "CalculateAreaCmd.h"
#include "CalculateTextureAreaCmd.h"
#include "GetNumUVsCmd.h"
#include "CheckUVAtlasCompatibilityCmd.h"
#include "ragePlayWavCmd.h"
#include "UVCache.h"
#include "ClosestPointAndFaceOnMeshCmd.h"
#include "GetUVOnMeshCmd.h"
#include "GetPolygonCenterCmd.h"
#include "IsConvexCmd.h"
#include "IsPlanarCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MFnPlugin.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MDistance.h>
#include <maya/MPointArray.h>

// #include <crtdbg.h>

// #include <assert.h>
#pragma warning(pop)

// #define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__)
// #define new DEBUG_NEW


namespace rage
{

//-----------------------------------------------------------------------------
void error(const MString& message, unsigned line, MStatus& status)
//-----------------------------------------------------------------------------
{
    MString m(message);
    m += line;
    m += ") ";
    status.perror (m);
    MGlobal::displayError (m);
}

//-----------------------------------------------------------------------------
MStatus initializePlugin(MObject obj)
//-----------------------------------------------------------------------------
{
    // enable leak tracking
    //int tmpFlag = _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG );
    //tmpFlag |= _CRTDBG_LEAK_CHECK_DF;
    //_CrtSetDbgFlag( tmpFlag );

    MStatus status=MStatus::kSuccess;

    MFnPlugin plugin(obj, "Rockstar Vienna", "0.1", "Any", &status);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageMayaUVAtlasCmd failed.");
        return status;
    }

    status = plugin.registerCommand("rageMayaUVAtlas", 
                                    rageMayaUVAtlasCmd::creator);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageMayaUVAtlasCmd failed.");
        return status;
    }

    status = plugin.registerNode("rageMayaUVAtlasNode", 
        ID_UVATLAS_NODE,
        rageMayaUVAtlasNode::creator,
        rageMayaUVAtlasNode::initialize);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageMayaUVAtlasNode failed.");
        plugin.deregisterCommand("rageMayaUVAtlas");
        return status;
    }


    status = plugin.registerCommand("rageUVMatrix", 
        rageUVMatrixCmd::creator);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageUVMatrixCmd failed.");
        return status;
    }

    status = plugin.registerNode("rageUVMatrixNode", 
        ID_UVMATRIX_NODE,
        rageUVMatrixNode::creator,
        rageUVMatrixNode::initialize);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageUVMatrixNode failed.");
        plugin.deregisterCommand("rageMayaUVAtlas");
        return status;
    }

    status = plugin.registerCommand("rageCalculateMeshArea", 
        CalculateAreaCmd::creator);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageCalculateMeshArea failed.");
        return status;
    }


    status = plugin.registerCommand("rageCalculateMeshTextureArea", 
        CalculateTextureAreaCmd::creator);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageCalculateMeshTextureArea failed.");
        return status;
    }

    status = plugin.registerCommand("rageGetNumUVs", 
        GetNumUVsCmd::creator);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageGetNumUVs failed.");
        return status;
    }

    status = plugin.registerCommand("rageCheckUVAtlasCompatibility", 
        CheckUVAtlasCompatibilityCmd::creator);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageCheckUVAtlasCompatibility failed.");
        return status;
    }


    status = plugin.registerCommand("ragePlayWav", 
        ragePlayWavCmd::creator);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for ragePlayWav failed.");
        return status;
    }

    status = plugin.registerData (UVCacheData::typeName, UVCacheData::id, UVCacheData::creator);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageUVCacheData failed.");
        return status;
    }

    status = plugin.registerCommand("rageClosestPointAndFaceOnMesh", 
		ClosestPointAndFaceOnMeshCmd::creator);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageClosestPointAndFaceOnMesh failed.");
	return status;
    }

    status = plugin.registerCommand("rageGetUVOnMesh",  GetUVOnMeshCmd::creator);
    if (status!= MStatus::kSuccess)
    {
	status.perror("initializePlugin() for rageGetUVOnMesh failed.");
	return status;
    }

    status = plugin.registerCommand("rageGetPolygonCenter", 
		GetPolygonCenterCmd::creator);
    if (status!= MStatus::kSuccess)
    {
        status.perror("initializePlugin() for rageGetPolygonCenter failed.");
	return status;
    }

	status = plugin.registerCommand("isConvex", IsConvexCmd::creator);
	if (status!= MStatus::kSuccess)
	{
		status.perror("initializePlugin() for isConvex failed.");
		return status;
	}

	status = plugin.registerCommand("isPlanar", IsPlanarCmd::creator);
	if (status!= MStatus::kSuccess)
	{
		status.perror("initializePlugin() for isPlanar failed.");
		return status;
	}

	return status;
}


//-----------------------------------------------------------------------------
MStatus uninitializePlugin(MObject obj)
//-----------------------------------------------------------------------------
{
    MStatus status=MStatus::kSuccess;
    MFnPlugin plugin(obj);

    status=plugin.deregisterNode(ID_UVATLAS_NODE);
    if (!status) 
    {
        status.perror("deregisterNode(ID_UVATLAS_NODE)");
    }

    status=plugin.deregisterNode(ID_UVMATRIX_NODE);
    if (!status) 
    {
        status.perror("deregisterNode(ID_UVMATRIX_NODE)");
    }

#define DEREGISTER_COMMAND(command) \
    status=plugin.deregisterCommand(command);\
    if (!status)\
    {\
        status.perror("deregisterCommand "\
        command\
        );\
    }\

    DEREGISTER_COMMAND("rageMayaUVAtlas");
    DEREGISTER_COMMAND("rageUVMatrix");
    DEREGISTER_COMMAND("rageCalculateMeshArea");
    DEREGISTER_COMMAND("rageCalculateMeshTextureArea");
    DEREGISTER_COMMAND("rageGetNumUVs");
    DEREGISTER_COMMAND("rageCheckUVAtlasCompatibility");
    DEREGISTER_COMMAND("ragePlayWav");
    DEREGISTER_COMMAND("rageClosestPointAndFaceOnMesh");
    DEREGISTER_COMMAND("rageGetUVOnMesh");
    DEREGISTER_COMMAND("rageGetPolygonCenter");
	DEREGISTER_COMMAND("isConvex");
	DEREGISTER_COMMAND("isPlanar");

#undef DEREGISTER_COMMAND

    return status;
}


//-----------------------------------------------------------------------------
bool createNULLRefDevice(LPDIRECT3D9 &spD3D, LPDIRECT3DDEVICE9& spd3dDevice)
//-----------------------------------------------------------------------------
{
    HRESULT hr = 0;
    if (spD3D==NULL)
    {
        spD3D = Direct3DCreate9( D3D_SDK_VERSION );
    }
    if( NULL == spD3D )
        return false;

    if (spd3dDevice==NULL)
    {
        D3DDISPLAYMODE Mode;
        spD3D->GetAdapterDisplayMode(0, &Mode);

        D3DPRESENT_PARAMETERS pp;
        ZeroMemory( &pp, sizeof(D3DPRESENT_PARAMETERS) ); 
        pp.BackBufferWidth  = 1;
        pp.BackBufferHeight = 1;
        pp.BackBufferFormat = Mode.Format;
        pp.BackBufferCount  = 1;
        pp.SwapEffect       = D3DSWAPEFFECT_COPY;
        pp.Windowed         = TRUE;

        hr = spD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_NULLREF, GetDesktopWindow(), 
            D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_FPU_PRESERVE | D3DCREATE_MULTITHREADED, &pp, &spd3dDevice );

        if( FAILED(hr) || spd3dDevice == NULL )
        {
            releaseNULLRefDevice(spD3D, spd3dDevice);
            return false;
        }
    }
    
    return true;
}

//-----------------------------------------------------------------------------
void releaseNULLRefDevice(LPDIRECT3D9 &spD3D, LPDIRECT3DDEVICE9& spd3dDevice)
//-----------------------------------------------------------------------------
{
    if( spd3dDevice != NULL) 
    {
        spd3dDevice->Release();
        spd3dDevice = NULL;
    }

    if( spD3D != NULL)
    {
        spD3D->Release();
        spD3D = NULL;
    }
}

// Vertex declaration
D3DVERTEXELEMENT9 VERTEX_DECL[] =
{
    { 0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,  D3DDECLUSAGE_POSITION, 0}, 
    { 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,  D3DDECLUSAGE_NORMAL,   0}, 
    { 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT,  D3DDECLUSAGE_TEXCOORD, 0},
    { 0, 32, D3DDECLTYPE_UBYTE4, D3DDECLMETHOD_DEFAULT,  D3DDECLUSAGE_BLENDINDICES, 0},    
    D3DDECL_END()
};


// Vertex format
struct VERTEX
{
    D3DXVECTOR3 position;
    D3DXVECTOR3 normal;
    D3DXVECTOR2 texcoord;
    unsigned char blendIndices[4];
};

typedef VERTEX* VERTEXPTR;

//-----------------------------------------------------------------------------
void setVertexData(VERTEXPTR& pVertex, MMatrix& matrix, MPoint& point, 
                   unsigned int meshIdx, unsigned int index, 
                   MFloatVector& normal)
//-----------------------------------------------------------------------------
{
    MPoint pointTransformed = point * matrix;
    pVertex->position.x = static_cast<float>(pointTransformed.x);
    pVertex->position.y = static_cast<float>(pointTransformed.y);
    pVertex->position.z = static_cast<float>(pointTransformed.z);

    MVector transformedNormal = normal;
    transformedNormal.transformAsNormal(matrix);
    transformedNormal.normalize();
    pVertex->normal.x = static_cast<float>(transformedNormal.x);
    pVertex->normal.y = static_cast<float>(transformedNormal.y);
    pVertex->normal.z = static_cast<float>(transformedNormal.z);

    pVertex->texcoord.x = 0.0f;
    pVertex->texcoord.y = 0.0f;
    pVertex->blendIndices[0] = (unsigned char)(index & 0xff);
    pVertex->blendIndices[1] = (unsigned char)((index >> 8 )& 0xff);
    pVertex->blendIndices[2] = (unsigned char)(meshIdx) & 0xff;
    pVertex->blendIndices[3] = (unsigned char)(meshIdx >>8) & 0xff;
    pVertex++;
}


//-----------------------------------------------------------------------------
int getNormalIdFromVertex(MItMeshPolygon& polyIter, unsigned int vertexIdx)
//-----------------------------------------------------------------------------
{
    int retIdx=-1;
    for (unsigned i=0; (i<polyIter.polygonVertexCount()) && (retIdx<0);i++)
    {
        if (polyIter.vertexIndex(i)==vertexIdx)
        {
            retIdx = polyIter.normalIndex(i);
        }
    }
    return retIdx;
}


struct MeshVertexFaceElement
{
    int mMeshId;
    int mVertexId;
    int mNormalId;

    MeshVertexFaceElement(int meshId, int vertexId, int normalId)
    {
        mMeshId = meshId;
        mVertexId = vertexId;
        mNormalId = normalId;
    }

    bool less (const MeshVertexFaceElement& rhs) const
    {
        if (mMeshId == rhs.mMeshId)
        {
            if (mVertexId == rhs.mVertexId)
            {
                return mNormalId < rhs.mNormalId;
            }
            else
            {
                return mVertexId < rhs.mVertexId;
            }
        }
        else
        {
            return mMeshId < rhs.mMeshId;
        }
    }
};

bool operator < (const MeshVertexFaceElement& lhs, const MeshVertexFaceElement& rhs)
{
    return lhs.less(rhs);
}

typedef std::map<MeshVertexFaceElement, int> MeshVertexFaceElementMap;
typedef std::vector<MeshVertexFaceElement> MeshVertexFaceElementVector;

//-----------------------------------------------------------------------------
int getElementId(MeshVertexFaceElementMap& elements, MeshVertexFaceElement& findElement )
//-----------------------------------------------------------------------------
{   
    MeshVertexFaceElementMap::iterator it;
    it = elements.find(findElement);

    if (it == elements.end())
    {
        return -1;
    }
    else
    {
        return it->second;
    }
}

//-----------------------------------------------------------------------------
void AddToElementList(MeshVertexFaceElementVector& elementVector,
                      MeshVertexFaceElementMap& elements,
                      MeshVertexFaceElement& newElement )
//-----------------------------------------------------------------------------
{
    int index = getElementId(elements, newElement);
    if (index == -1)
    {
        elements[newElement] = elements.size();
        elementVector.push_back(newElement);
        // assert(elements.size()==elementVector.size());
    }
}


//-----------------------------------------------------------------------------
MStatus createD3DxMeshFromMaya(MObjectArray& meshNodes, 
                               MMatrixArray& matrices,
                               const LPDIRECT3DDEVICE9& spd3dDevice,
                               LPD3DXMESH *ppMesh)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    HRESULT hr = S_OK;
    int numVertices = 0;
    // int numVerticesVertexElements=0;
    int numFaces = 0;
    MeshVertexFaceElementMap elements;
    MeshVertexFaceElementVector elementVector;

    for (unsigned int idx = 0; idx<meshNodes.length(); idx++)
    {
        MFnMesh mesh(meshNodes[idx], &status);

        MPointArray points;
        mesh.getPoints(points, MSpace::kWorld);

        MFloatVectorArray normals;
        mesh.getNormals(normals, MSpace::kWorld);

        MItMeshPolygon polyIter( meshNodes[idx], &status );
        if ( MS::kSuccess != status) 
        {
            ReportError("Failure in MItMeshPolygon initialization.");
            return status;
        }

        for ( ; !polyIter.isDone(); polyIter.next() ) 
        {
            if (!polyIter.hasValidTriangulation())
            {
                status = MS::kFailure;
                ReportError("Failure in MItMeshPolygon, mesh has no valid triangulation.");
                return status;
            }

            int numTris=0;
            polyIter.numTriangles(numTris);
            numFaces +=numTris;
            for (int i=0;i<numTris; i++)
            {
                int vertexIndices[3];
                mesh.getPolygonTriangleVertices(polyIter.index(NULL), i, vertexIndices);
                for (int j=0; j<3; j++)
                {
                    int normalIdx = getNormalIdFromVertex(polyIter, vertexIndices[j]); 
					MeshVertexFaceElement obMeshVertexFaceElement(idx, vertexIndices[j], normalIdx);
                    AddToElementList(elementVector, elements, obMeshVertexFaceElement);
                }
            }
        }
    }            
    numVertices= elements.size();


    hr = D3DXCreateMesh( numFaces, 
        numVertices, 
        D3DXMESH_MANAGED | D3DXMESH_32BIT, 
        VERTEX_DECL, 
        spd3dDevice, ppMesh );
    if (FAILED(hr))
    {
        status = MS::kFailure;
        return status;
    }

    //do the verteces...
    VERTEX* pVertex;
    (*ppMesh)->LockVertexBuffer( 0, (void**) &pVertex );
    MeshVertexFaceElementVector::iterator it = elementVector.begin();
    for (unsigned int idx = 0; idx<meshNodes.length(); idx++)
    {
        MFnMesh mesh(meshNodes[idx], &status);
        MMatrix matrix = matrices[idx];

        MPointArray points;
        mesh.getPoints(points, MSpace::kWorld);

        MFloatVectorArray normals;
        mesh.getNormals(normals, MSpace::kWorld);

        while (it!=elementVector.end() && (it->mMeshId == (int)idx))
        {
            setVertexData(pVertex, matrix, points[it->mVertexId], idx, it->mVertexId, normals[it->mNormalId]);
            it++;
        }
    }
    (*ppMesh)->UnlockVertexBuffer();
           

    //do the faces
    DWORD* pIndex;
    (*ppMesh)->LockIndexBuffer( 0, (void**) &pIndex );
    for (unsigned int idx = 0; idx<meshNodes.length(); idx++)
    {
        MFnMesh mesh(meshNodes[idx], &status);

        MItMeshPolygon polyIter( meshNodes[idx], &status );
        if ( MS::kSuccess != status) 
        {
            ReportError("Failure in MItMeshPolygon initialization.");
            return status;
        }

        for ( ; !polyIter.isDone(); polyIter.next() ) 
        {
            if (!polyIter.hasValidTriangulation())
            {
                status = MS::kFailure;
                ReportError("Failure in MItMeshPolygon, mesh has no valid triangulation.");
                return status;
            }

            int numTris=0;
            polyIter.numTriangles(numTris);
            numFaces +=numTris;
            for (int i=0;i<numTris; i++)
            {
                int vertexIndices[3];
                mesh.getPolygonTriangleVertices(polyIter.index(NULL), i, vertexIndices);
                for (int j=0; j<3; j++)
                {
                    int normalIdx = getNormalIdFromVertex(polyIter, vertexIndices[j]); 
					MeshVertexFaceElement obMeshVertexFaceElement(idx, vertexIndices[j], normalIdx);
                    int elementIdx = getElementId( elements, obMeshVertexFaceElement);
                    if (elementIdx<0)
                    {
                        status = MS::kFailure;
                        ReportError("internal failure, mesh vertex not found.");
                        return status;                        
                    }
                    else
                    {
                        *pIndex =elementIdx;
                        pIndex++;
                    }
                }
            }
        }
    }
    (*ppMesh)->UnlockIndexBuffer(); 

    return status;
}


//-----------------------------------------------------------------------------
MStatus checkD3DxMeshForConsistency(LPD3DXMESH pMesh)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    VERTEX* pVertex;
    DWORD* pIndex;
    // int numVerteces = pMesh->GetNumVertices();   
    int numFaces = pMesh->GetNumFaces();
    pMesh->LockVertexBuffer( 0, (void**) &pVertex );
    pMesh->LockIndexBuffer( 0, (void**) &pIndex );

    for (int j=0; j<numFaces && (MS::kSuccess == status) ; j++)
    {   
        int dxIdx1 = pIndex[j*3+0];
        int dxIdx2 = pIndex[j*3+1];
        int dxIdx3 = pIndex[j*3+2];

        int mayaMeshIdx1 = (pVertex[dxIdx1].blendIndices[3]<<8) | (pVertex[dxIdx1].blendIndices[2]);
        int mayaMeshIdx2 = (pVertex[dxIdx2].blendIndices[3]<<8) | (pVertex[dxIdx2].blendIndices[2]);
        int mayaMeshIdx3 = (pVertex[dxIdx3].blendIndices[3]<<8) | (pVertex[dxIdx3].blendIndices[2]);

        if ((mayaMeshIdx1!=mayaMeshIdx2) || (mayaMeshIdx1!=mayaMeshIdx3))
        {
            //internal error - what should we do here??
            ReportError("Failure in D3Dx Consistency.");
            status = MS::kFailure;
        }
    }
    pMesh->UnlockVertexBuffer();
    pMesh->UnlockIndexBuffer();

    return status;
}

//-----------------------------------------------------------------------------
MStatus mergeBackD3DxMeshIntoMaya(MObjectArray& meshNodes, 
                                  LPD3DXMESH pMesh, 
                                  MString *UVSet)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    for (unsigned int idx = 0; (idx<meshNodes.length()) && (MS::kSuccess == status) ; idx++)
    {
        MFnMesh mesh(meshNodes[idx], &status);
        if ( MS::kSuccess != status) 
        {
            ReportError("Failure in MFnMesh initialization.");
            return status;
        }

        if (UVSet!=NULL)
        {
            MStringArray setNames;
            mesh.getUVSetNames(setNames);
            bool found = false;
            for (unsigned int i=0; (i<setNames.length()) && (!found);i++)
            {
                found = (setNames[i]==*UVSet);
            }
            if (!found)
            {
                MString UVSetName = *UVSet;
                status = mesh.createUVSet(UVSetName);
                MString err = status.errorString();
                if ( MS::kSuccess != status) 
                {
                    MString msg("Could not create UVset");
                    if (UVSet!=NULL)
                    {
                        msg+=MString(" ");
                        msg+=*UVSet;
                    }
                    msg+=" ( ";
                    msg+=err.asChar();
                    msg+=" ) ";
                    MGlobal::displayError(msg);
                    return status;
                }
            }
        }
        
        //delete the current UV set....
        status = mesh.clearUVs(UVSet);
        if ( MS::kSuccess != status) 
        {
            MString msg("Could not clear UVset");
            if (UVSet!=NULL)
            {
                msg+=MString(" ");
                msg+=*UVSet;
            }
            MGlobal::displayError(msg);
            return status;
        }


        MPointArray points;
        mesh.getPoints(points, MSpace::kWorld);

        MItMeshPolygon polyIter(meshNodes[idx], &status );
        if ( MS::kSuccess != status) 
        {
            ReportError("Failure in MItMeshPolygon initialization.");
            return status;
        }

        //get the UVs from the dx vertex buffer....
        VERTEX* pVertex;
        DWORD* pIndex;
//        int numVerteces = pMesh->GetNumVertices();   
        int numFaces = pMesh->GetNumFaces();
        pMesh->LockVertexBuffer( 0, (void**) &pVertex );
        pMesh->LockIndexBuffer( 0, (void**) &pIndex );
        int mayaNumVerteces = mesh.numVertices(NULL);

        MFloatArray uArray;
        MFloatArray vArray;
        MIntArray UVCounts;
        MIntArray UVIds;

        //initialize the structures...
        polyIter.reset();
        for ( ; !polyIter.isDone() && (MS::kSuccess == status); polyIter.next() ) 
        {
            int numVertecesPerPoly = polyIter.polygonVertexCount(&status);
            UVCounts.append(numVertecesPerPoly);
            for (int i=0;i<numVertecesPerPoly; i++)
            {
                UVIds.append(-1);
            }        
        }

        int num=0;
        for ( int vert=0; (vert<mayaNumVerteces) && (MS::kSuccess == status) ; vert++ ) 
        {
            polyIter.reset();
            int polyUVIdx = 0;
            for ( ; !polyIter.isDone() && (MS::kSuccess == status); polyIter.next() ) 
            {
                int numTris=0;
                polyIter.numTriangles(numTris);
//                bool hasUVs = polyIter.hasUVs(&status);
                MIntArray vertexIndeces;
                polyIter.getVertices (vertexIndeces);

                int numVertecesPerPoly = polyIter.polygonVertexCount(&status);

                for (int i=0;i<numTris && (MS::kSuccess == status); i++)
                {
                    int vertexList[3];
                    mesh.getPolygonTriangleVertices(polyIter.index(NULL), i, vertexList);
                    if ( (vertexList[0]==vert) || 
                        (vertexList[1]==vert) || 
                        (vertexList[2]==vert) )
                    {

                        int foundFace = false;
                        int dxIdx = -1;
                        int dxIdx1 = -1;
                        int dxIdx2 = -1;
                        int dxIdx3 = -1;
                        //search the triangles in the dx mesh and find the correct face....
                        for (int j=0; j<numFaces && (MS::kSuccess == status) && (!foundFace); j++)
                        {   
                            dxIdx1 = pIndex[j*3+0];
                            dxIdx2 = pIndex[j*3+1];
                            dxIdx3 = pIndex[j*3+2];

                            unsigned int mayaMeshIdx1 = (pVertex[dxIdx1].blendIndices[3]<<8) | (pVertex[dxIdx1].blendIndices[2]);
                            unsigned int mayaMeshIdx2 = (pVertex[dxIdx2].blendIndices[3]<<8) | (pVertex[dxIdx2].blendIndices[2]);
                            unsigned int mayaMeshIdx3 = (pVertex[dxIdx3].blendIndices[3]<<8) | (pVertex[dxIdx3].blendIndices[2]);

                            if ((mayaMeshIdx1 ==idx) || (mayaMeshIdx2 ==idx) || (mayaMeshIdx3 ==idx))
                            {
                                if ((mayaMeshIdx1!=mayaMeshIdx2) || (mayaMeshIdx1!=mayaMeshIdx3))
                                {
                                    //internal error - what should we do here??
                                    status = MS::kFailure;
                                }
                            }

                            if ((mayaMeshIdx1 ==idx) || (mayaMeshIdx2 ==idx) || (mayaMeshIdx3 ==idx))
                            {
                                int mayaIdx1 = ((int)pVertex[dxIdx1].blendIndices[0])+((int)pVertex[dxIdx1].blendIndices[1])*256;
                                int mayaIdx2 = ((int)pVertex[dxIdx2].blendIndices[0])+((int)pVertex[dxIdx2].blendIndices[1])*256;
                                int mayaIdx3 = ((int)pVertex[dxIdx3].blendIndices[0])+((int)pVertex[dxIdx3].blendIndices[1])*256;
                                foundFace = ( (vertexList[0]==mayaIdx1) && (vertexList[1]==mayaIdx2) && (vertexList[2]==mayaIdx3)) ||
                                    ( (vertexList[0]==mayaIdx1) && (vertexList[1]==mayaIdx3) && (vertexList[2]==mayaIdx2)) ||
                                    ( (vertexList[0]==mayaIdx2) && (vertexList[1]==mayaIdx3) && (vertexList[2]==mayaIdx1)) ||
                                    ( (vertexList[0]==mayaIdx2) && (vertexList[1]==mayaIdx1) && (vertexList[2]==mayaIdx3)) ||
                                    ( (vertexList[0]==mayaIdx3) && (vertexList[1]==mayaIdx1) && (vertexList[2]==mayaIdx2)) ||
                                    ( (vertexList[0]==mayaIdx3) && (vertexList[1]==mayaIdx2) && (vertexList[2]==mayaIdx1)) ;
                                if (mayaIdx1==vert)
                                {
                                    dxIdx = dxIdx1;
                                }
                                else if (mayaIdx2==vert)
                                {
                                    dxIdx = dxIdx2;
                                }
                                else if (mayaIdx3==vert)
                                {
                                    dxIdx = dxIdx3;
                                }
                            }
                        }
                        if ((!foundFace) || (dxIdx==-1))
                        {
                            //internal error - what should we do here??
                            status = MS::kFailure;
                        }
                        else
                        {
                            float2 UVPoint;
                            UVPoint[0] = pVertex[dxIdx].texcoord.x;
                            UVPoint[1] = pVertex[dxIdx].texcoord.y;

                            unsigned int findIdx=0;
                            while (findIdx<vertexIndeces.length() && vertexIndeces[findIdx]!=vert)
                            {
                                findIdx++;
                            }
                            if (findIdx<vertexIndeces.length())
                            {                                             
                                int UVId=0;
                                int numCurrentUVs= uArray.length();
                                while ((UVId<numCurrentUVs) && !((uArray[UVId]==UVPoint[0]) && (vArray[UVId]==UVPoint[1])))
                                {
                                    UVId++;
                                }
                                if (UVId==numCurrentUVs)
                                {
                                    uArray.append(UVPoint[0]);
                                    vArray.append(UVPoint[1]);
                                }
                                if ( (UVIds[polyUVIdx+findIdx]!=-1) && (UVIds[polyUVIdx+findIdx]!=UVId) )
                                {
                                    //internal error - what should we do here??
                                    //stat = MS::kFailure;  
                                    //UVId = UVIds[polyUVIdx+findIdx];
                                }
                                UVIds[polyUVIdx+findIdx] = UVId;
                                /*
                                char buf[4096];
                                sprintf(buf,"%d : %d (%f %f %f) %d %d %d %d: %d %d %d (%f,%f,%f) (%f,%f,%f) (%f,%f,%f)   %d (%d %d %d) %f %f  (%f,%f) (%f,%f) (%f,%f)\n",
                                num,
                                vert, points[vert].x, points[vert].y, points[vert].z, 
                                UVId, polyUVIdx, findIdx, polyUVIdx+findIdx,
                                vertexList[0],vertexList[1],vertexList[2], 
                                pVertex[dxIdx1].position.x,pVertex[dxIdx1].position.y, pVertex[dxIdx1].position.z, 
                                pVertex[dxIdx2].position.x,pVertex[dxIdx2].position.y, pVertex[dxIdx2].position.z, 
                                pVertex[dxIdx3].position.x,pVertex[dxIdx3].position.y, pVertex[dxIdx3].position.z,                                           
                                dxIdx, dxIdx1,dxIdx2,dxIdx3,
                                UVPoint[0],UVPoint[1],
                                pVertex[dxIdx1].texcoord.x,pVertex[dxIdx1].texcoord.y,pVertex[dxIdx2].texcoord.x,pVertex[dxIdx2].texcoord.y,pVertex[dxIdx3].texcoord.x,pVertex[dxIdx3].texcoord.y);

                                OutputDebugString(buf);
                                */
                            }
                            else
                            {
                                //internal error - what should we do here??
                                status = MS::kFailure;
                            }
                        }
                        num++;
                    }
                }

                polyUVIdx+=numVertecesPerPoly;
            }
        }
        pMesh->UnlockVertexBuffer();
        pMesh->UnlockIndexBuffer();       

        polyIter.reset();
        int idx=0;
        for ( ; !polyIter.isDone() && (MS::kSuccess == status); polyIter.next() ) 
        {
            int numVertecesPerPoly = polyIter.polygonVertexCount(&status);
            for (int i=0;i<numVertecesPerPoly; i++)
            {
                if (UVIds[idx]==-1)
                {
                    status = MS::kFailure;
                }
                idx++;
            }        
        }

        status = mesh.setUVs(uArray, vArray, UVSet);

        if ( MS::kSuccess == status) 
        {
            mesh.assignUVs(UVCounts, UVIds, UVSet);
        }      
    }

    return status;
}


//-----------------------------------------------------------------------------
MStatus CheckMeshValidation(LPD3DXMESH pMesh, 
                            LPD3DXMESH *ppMeshOut, 
                            DWORD **ppAdjacency)
//-----------------------------------------------------------------------------
{
    HRESULT hr = S_OK;
    MStatus status = MStatus::kSuccess;
    DWORD *pAdjacencyIn = NULL;
    LPD3DXBUFFER pErrorsAndWarnings = NULL;

    if (!(pMesh && ppAdjacency)) 
    {
        status = MStatus::kFailure;
        ReportError("Unable to validate mesh.");
    }

    if (status==MStatus::kSuccess)
    {
        pAdjacencyIn = new DWORD[pMesh->GetNumFaces() * sizeof(DWORD)*3];
        hr = pMesh->ConvertPointRepsToAdjacency(NULL, pAdjacencyIn);
        if (FAILED(hr))
        {
            status = MStatus::kFailure;
            ReportError("ConvertPointRepsToAdjacency() failed!");
        }
    }

    if (status==MStatus::kSuccess)
    {

        hr = D3DXValidMesh(pMesh, pAdjacencyIn, &pErrorsAndWarnings);
        if (NULL != pErrorsAndWarnings) {
            char* s = (char*)pErrorsAndWarnings->GetBufferPointer(); 
            MGlobal::displayWarning(s);
        }

        if (FAILED(hr)) 
        {
            MGlobal::displayWarning("D3DXValidMesh() failed: %x.  Attempting D3DXCleanMesh()...\n");

            hr = D3DXCleanMesh(D3DXCLEAN_SIMPLIFICATION, pMesh, pAdjacencyIn, ppMeshOut, pAdjacencyIn, &pErrorsAndWarnings);

            if (NULL != pErrorsAndWarnings) 
            {
                char* s = (char*)pErrorsAndWarnings->GetBufferPointer(); 
                MGlobal::displayWarning(s);
            }

            if (FAILED(hr)) 
            {
                status = MStatus::kFailure;
                ReportError("D3DXCleanMesh() failed!");
            } 
            else
            {
                MGlobal::displayInfo("D3DXCleanMesh() succeeded: %x\n");
            }
        } 
        else
        {
            *ppMeshOut = pMesh;
            (*ppMeshOut)->AddRef();
        }
    }

    if (pErrorsAndWarnings) 
    { 
        pErrorsAndWarnings->Release(); 
    } 

    if (status!=MStatus::kSuccess)
    {
        if (pAdjacencyIn)
        {
            delete (pAdjacencyIn);
			pAdjacencyIn = NULL;
        }
        if (*ppMeshOut)
        {
            delete (*ppMeshOut);
			*ppMeshOut = NULL;
        }
    }
    *ppAdjacency = pAdjacencyIn;
    return status;
}


//-----------------------------------------------------------------------------
MStatus processUVAtlasCreation(LPD3DXMESH pMesh, 
                               LPD3DXMESH *ppMeshOut, 
                               Settings &settings)
//-----------------------------------------------------------------------------
{
    MStatus status = MStatus::kSuccess;
    ID3DXMesh *pMeshValid = NULL;
    LPD3DXBUFFER pFacePartitioning = NULL;
    FLOAT stretchOut;
    UINT numchartsOut = 0;
    DWORD *pAdjacency= 0;

    // [ERIK] handy for debugging: uncomment to have the mesh saved as a direct x mesh
    D3DXMATERIAL material;
    memset(&material,0,sizeof(D3DXMATERIAL));
    material.MatD3D.Diffuse.r = 1.0;
    material.MatD3D.Diffuse.g = 1.0;
    material.MatD3D.Diffuse.b = 1.0;

    HRESULT hr = D3DXSaveMeshToX("C:\\debug.x", 
                                 pMesh,
                                 NULL,
                                 &material,
                                 NULL,
                                 1,
                                 D3DXF_FILEFORMAT_TEXT);

    if (FAILED(hr)) 
    {
        status.perror("UV Atlas: could not save debug mesh.");        
    }



    if ((status==MStatus::kSuccess) && (!CheckMeshValidation(pMesh, &pMeshValid, &pAdjacency) ))
    {
        status = MStatus::kFailure;
        ReportError("Unable to clean mesh.");
    }

    /*
    if (status==MStatus::kSuccess)
    {
        status = checkD3DxMeshForConsistency(pMesh);
        if (status!=MStatus::kSuccess)
        {
            status = MStatus::kFailure;
            status.perror("D3Dx mesh not consistent after clean.");
            MGlobal::displayError("D3Dx mesh not consistent after clean.");
        }
    }
    */

    if (status==MStatus::kSuccess)
    {
        HRESULT hr = D3DXUVAtlasCreate(pMeshValid,	// pMesh
                        settings.maxcharts,			// dwMaxChartNumber
                        settings.maxstretch,		// fMaxStretch
                        settings.width,				// dwWidth
                        settings.height,			// dwHeight
                        settings.gutter,			// fGutter
                        0,							// dwTextureIndex
                        pAdjacency,					// pdwAdjacency
						NULL,						// pcFalseEdges
                        NULL,						// pfIMTArray
                        NULL,						// pCallback
                        0,							// fCallbackFrequency
                        NULL,						// pUserContent
						D3DXUVATLAS_DEFAULT,		// dwOptions
                        ppMeshOut,					// ppMeshOut
                        &pFacePartitioning,			// ppFacePartitioning
                        NULL,						// ppVertexRemapArray
                        &stretchOut,				// pfMaxStretchOut
                        &numchartsOut				// pdwNumChartsOut
						);
        if (FAILED(hr)) 
        {
            status = MStatus::kFailure;
            ReportError("UV Atlas creation failed.");
        }
    }
    if (pMeshValid)
    {
        pMeshValid->Release();
    }
    if (pFacePartitioning)
    {
        pFacePartitioning->Release();
    }
    if (pAdjacency)
    {
        delete pAdjacency;
    }    


    return status;
}


//-----------------------------------------------------------------------------
MStatus processUVAtlasCreationOnMayaMesh(MObjectArray& meshNodes, 
                                         MMatrixArray& matrices, 
                                         LPDIRECT3DDEVICE9& spd3dDevice, 
                                         Settings &settings)
//-----------------------------------------------------------------------------
{
    MStatus status=MStatus::kSuccess;

    if (spd3dDevice==NULL)
    {
        status = MStatus::kFailure;
        ReportError("DirectX9 initialization failed.");
        return status;
    }


    ID3DXMesh *pMesh = NULL;
    ID3DXMesh *pMeshResult = NULL;

    status = createD3DxMeshFromMaya(meshNodes, matrices, spd3dDevice, &pMesh);
    if (status!=MStatus::kSuccess)
    {
        status = MStatus::kFailure;
        ReportError("Error creating directx mesh from maya mesh.");
    }

    /*
    status = checkD3DxMeshForConsistency(pMesh);
    if (status!=MStatus::kSuccess)
    {
        status = MStatus::kFailure;
        status.perror("Error in d3dx mesh.");
        MGlobal::displayError("Error in d3dx mesh.");
    }
    */

    status = processUVAtlasCreation(pMesh, &pMeshResult, settings);

    MString* UVSetName=NULL;
    if (settings.UVSetName.length()>0)
    {
        UVSetName = &settings.UVSetName;
    }
    if ((status==MStatus::kSuccess) && 
        (!mergeBackD3DxMeshIntoMaya(meshNodes, pMeshResult, UVSetName)) )
    {
        status = MStatus::kFailure;
        ReportError("error merging generated UV set back into maya.");
    }    

    if (pMesh)
    {
        pMesh->Release();
    }
    if (pMeshResult)
    {
        pMeshResult->Release();
    }

    return status;
}


//-----------------------------------------------------------------------------
MStatus processMatrixOnUVSet(MObject& meshNode, 
                             double *matrix,
                             MString* UVSet)
//-----------------------------------------------------------------------------
{
    MStatus status=MStatus::kSuccess;

    MFnMesh mesh(meshNode, &status);
    if ( MS::kSuccess != status) 
    {
        ReportError("Failure in MFnMesh initialization.");
    }

    MFloatArray uArray;
    MFloatArray vArray;
  	status = mesh.getUVs(uArray, vArray, UVSet);
    for(unsigned int i=0; (i<uArray.length()) && (i<vArray.length());i++ )
    {
        uArray[i]= static_cast<float>(uArray[i] * matrix[0] + vArray[i] * matrix[2] + matrix[4]);
        vArray[i]= static_cast<float>(uArray[i] * matrix[1] + vArray[i] * matrix[3] + matrix[5]);
    }
  	
    status = mesh.setUVs(uArray, vArray, UVSet);

    return status;
}

} // namespace rage
