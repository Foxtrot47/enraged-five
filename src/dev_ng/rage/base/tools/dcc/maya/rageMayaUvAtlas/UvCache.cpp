// -----------------------------------------------------------------------------
// $Workfile: rageUVMatrixCmd.h  $
//   Creator: Erik Pojar
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
//   Custom Data attribute to hold the cached result of the UV atlas calculation
// -----------------------------------------------------------------------------

#include "UVCache.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MArgList.h>
#include <maya/MObjectArray.h>
#include <maya/MFnMesh.h>
#include <maya/MGlobal.h>
#pragma warning(pop)

#include "rageMayaUVAtlasMain.h"
// #include <crtdbg.h>


namespace rage
{

const MTypeId UVCacheData::id( 0x80666 );
const MString UVCacheData::typeName( "rageUVCacheData" );

//------------------------------------------------------------------------------
UVCacheData::UVCacheData()
//------------------------------------------------------------------------------
{
}


//------------------------------------------------------------------------------
UVCacheData::~UVCacheData()
//------------------------------------------------------------------------------
{
}


//------------------------------------------------------------------------------
void* UVCacheData::creator()
//------------------------------------------------------------------------------
{
    return new UVCacheData;
}


//------------------------------------------------------------------------------
void UVCacheData::copy(const MPxData& other)
//------------------------------------------------------------------------------
{ 
    if( other.typeId() == UVCacheData::id ) 
    {
        const UVCacheData* otherData = (const UVCacheData*)&other;
        mCache = otherData->mCache;
    } 
    else 
    {
        //  we need to convert to the other type based on its iff Tag
        cerr << "wrong data format!" << endl;
    }
    return;
}

//------------------------------------------------------------------------------
bool UVCacheData::get(unsigned int index, cacheEntry& entry)
//------------------------------------------------------------------------------
{
    if (index < mCache.size())
    {
        entry = mCache[index];
        return true;
    }
    else
    {
        return false;
    }
}


//------------------------------------------------------------------------------
MTypeId UVCacheData::typeId() const
//------------------------------------------------------------------------------
{
    return UVCacheData::id;
}


//------------------------------------------------------------------------------
MString UVCacheData::name() const
//------------------------------------------------------------------------------
{ 
    return UVCacheData::typeName; 
}


//------------------------------------------------------------------------------
void UVCacheData::clearCache ()
//------------------------------------------------------------------------------
{
    cacheT empty;
    mCache.clear();
    std::swap(mCache, empty);
}


//------------------------------------------------------------------------------
MStatus UVCacheData::storeIntoCache (MObjectArray& meshes, MString& UVSetName)
//------------------------------------------------------------------------------
{
    MStatus status;    

    // empty the cache
    clearCache();
    
    // store into the cache
    for (unsigned int index = 0; (index<meshes.length() && status == MS::kSuccess); ++index)
    {
        MFnMesh mesh(meshes[index], &status);
        if (status == MS::kSuccess )
        {
            mCache.push_back(cacheEntry());
            cacheEntry& entry = mCache[index];

            status = mesh.getUVs(entry.uArray, entry.vArray, &UVSetName);
            if ( MS::kSuccess == status) 
            {
                status = mesh.getAssignedUVs(entry.UVCounts, entry.UVIds, &UVSetName);
                if (status != MS::kSuccess)
                {
                    ReportError("getAssignedUVs failed");
                }
            }
            else
            {
                ReportError("getUVs failed");
            }
        }
        else
        {
            ReportError("Could not create functionset for mesh");
        }
    }

    return status;
}   

//------------------------------------------------------------------------------
MStatus UVCacheData::readASCII(const MArgList& args, unsigned& lastParsedElement)
//------------------------------------------------------------------------------
{
    MStatus status;
    int argLength = args.length();
    if( argLength > 0 ) 
    {
        // int numDataRecord = (argLength - lastParsedElement); 

        unsigned int recordCount;

        recordCount = args.asInt( lastParsedElement++, &status );
        if ( status == MS::kSuccess ) 
        {
            for (unsigned int i = 0; i < recordCount; ++i)
            {
                float d;
                d = (float)args.asDouble( lastParsedElement++, &status );
                cout << "readA: " << d << endl;
                if (status != MS::kSuccess)
                {
                    return status;
                }
            }
        } 
        return status;
    } 
    return MS::kFailure;
}


//------------------------------------------------------------------------------
MStatus UVCacheData::writeASCII( ostream& out )
//------------------------------------------------------------------------------
{
    //MStatus status;
    unsigned int recordsCount = 10;
    out << recordsCount << " ";
    if (!out.fail())
    {
        float d = 1.0f;
        for (unsigned int i=0; i < recordsCount; ++i) 
        {
            out << d << " ";
            if (out.fail()) 
            {
                return MS::kFailure;
            }
            d++;
        }
    }
    else
    {
        return MS::kFailure;
    }    
    return MS::kSuccess;
}

//------------------------------------------------------------------------------
MStatus UVCacheData::readBinary( istream& in, unsigned /*totalLength*/ )
//------------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
    unsigned int length = mCache.size();

    clearCache();

    in.read((char*) &length, sizeof(length));
    if (!in.fail()) 
    {
        mCache.resize(length);

        cacheT::iterator it;
        for (it = mCache.begin(); (it != mCache.end()) && (status == MS::kSuccess); ++it)
        {
            status = it->readBinary(in);
        }        
    }
    else 
    {
        status = MS::kFailure;
    }

    if (status == MS::kFailure)
    {
        clearCache();
    }
    return status;

}

//------------------------------------------------------------------------------
void UVCacheData::debugOutput() const
//------------------------------------------------------------------------------
{
    cacheT::const_iterator it;
    unsigned int index = 0;

    MGlobal::displayInfo("Full cache contents");

    for (it = mCache.begin(); it != mCache.end(); ++it, ++index)
    {
        MString message("  Index: ");
        message += index;
        MGlobal::displayInfo(message);
        it->debugOutput();
    }        
}


//------------------------------------------------------------------------------
MStatus UVCacheData::writeBinary(ostream& out)
//------------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
    unsigned int length = mCache.size();

#ifdef MUV_VERBOSE
    debugOutput();
#endif

    out.write((char*) &length, sizeof(length));
    if (!out.fail()) 
    {
        cacheT::iterator it;
        for (it = mCache.begin(); (it != mCache.end()) && (status == MS::kSuccess); ++it)
        {
            status = it->writeBinary(out);
        }        
    }
    else 
    {
        status = MS::kFailure;
    }

    return status;
}

//------------------------------------------------------------------------------
static MStatus writeFloatArrayBinary(ostream& out, MFloatArray& array)
//------------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    unsigned int length = array.length();
    out.write((char*) &length, sizeof(length));   

    if (!out.fail())
    {        
        if (length > 0)
        {
            float* fp = new float[length];
            status = array.get(fp);
            if (status)
            {
                out.write((char*)fp,sizeof(float)*length);
                if (out.fail())
                {
                    status == MS::kFailure;
                }
            }
            delete[] fp;
        }       
    }
    else
    {
        status == MS::kFailure;
    }

    return status;
}


//------------------------------------------------------------------------------
static MStatus readFloatArrayBinary(istream& in, MFloatArray& array)
//------------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    unsigned int length;
    in.read((char*) &length, sizeof(length));   

    if (!in.fail())
    {        
        if (length > 0)
        {
            float* fp = new float[length];
            in.read((char*)fp,sizeof(float)*length);
            if (!in.fail())
            {
                status = array.setLength(length);
                if (status)
                {
                    for (unsigned i = 0; (i < length) && (status == MS::kSuccess); ++i)
                    {
                        status = array.set(fp[i],i);
                    }
                }
            }
            else
            {
                status == MS::kFailure;
            }
            delete[] fp;
        }       
    }
    else
    {
        status == MS::kFailure;
    }

    return status;
}


//------------------------------------------------------------------------------
static MStatus writeIntArrayBinary(ostream& out, MIntArray& array)
//------------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    unsigned int length = array.length();
    out.write((char*) &length, sizeof(length));   

    if (!out.fail())
    {        
        if (length > 0)
        {
            int* ip = new int[length];
            status = array.get(ip);
            if (status)
            {
                out.write((char*)ip,sizeof(int)*length);
                if (out.fail())
                {
                    status == MS::kFailure;
                }
            }
            delete[] ip;
        }       
    }
    else
    {
        status == MS::kFailure;
    }

    return status;
}


//------------------------------------------------------------------------------
static MStatus readIntArrayBinary(istream& in, MIntArray& array)
//------------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    unsigned int length;
    in.read((char*) &length, sizeof(length));   

    if (!in.fail())
    {        
        if (length > 0)
        {
            int* ip = new int[length];
            in.read((char*)ip,sizeof(int)*length);
            if (!in.fail())
            {
                status = array.setLength(length);
                if (status)
                {
                    for (unsigned i = 0; (i < length) && (status == MS::kSuccess); ++i)
                    {
                        status = array.set(ip[i],i);
                    }
                }
            }
            else
            {
                status == MS::kFailure;
            }
            delete[] ip;
        }       
    }
    else
    {
        status == MS::kFailure;
    }

    return status;
}


//------------------------------------------------------------------------------
MStatus UVCacheData::cacheEntry::writeBinary(ostream& out)
//------------------------------------------------------------------------------
{
    MStatus status;
    status = writeFloatArrayBinary(out, uArray);
    if (status)
        status = writeFloatArrayBinary(out, vArray);
    if (status)
        status = writeIntArrayBinary(out, UVCounts);
    if (status)
        status = writeIntArrayBinary(out, UVIds);

    return status;
}


//------------------------------------------------------------------------------
MStatus UVCacheData::cacheEntry::readBinary(istream& in)
//------------------------------------------------------------------------------
{
    MStatus status;
    status = readFloatArrayBinary(in, uArray);
    if (status)
        status = readFloatArrayBinary(in, vArray);
    if (status)
        status = readIntArrayBinary(in, UVCounts);
    if (status)
        status = readIntArrayBinary(in, UVIds);

    return status;
}

template <class T>
static void printMayaArray(T& /*array*/, char* /*name*/)
{
    //std::stringstream stream;
    //stream << "    " << name << ": (" << array.length() << ") " << array;
    //MGlobal::displayInfo(stream.str().c_str());
}


//------------------------------------------------------------------------------
void UVCacheData::cacheEntry::debugOutput() const
//------------------------------------------------------------------------------
{
    printMayaArray(uArray, "cache uArray");
    printMayaArray(vArray, "cache vArray");

    bool allTriangles = true;
    for (unsigned int index = 0; index < UVCounts.length(); ++index)
    {
        if (UVCounts[index] != 3)
        {
            allTriangles = false;
        }
    }
    if (allTriangles)
    {
        MString message = "    cache UVCounts: (";
        message += UVCounts.length();
        message += ") all triangles";
        MGlobal::displayInfo(message);
    }
    else
    {
        printMayaArray(UVCounts, "cache UVCounts");
    }    
    printMayaArray(UVIds, "cache UVIds");
}


} // namespace rage
