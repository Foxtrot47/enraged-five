// -----------------------------------------------------------------------------
// $Workfile: ClosestUVAndFaceOnMesh.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\ClosestUVAndFaceOnMesh\ClosestUVAndFaceOnMesh.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// Plays a wav file asyncron
// -----------------------------------------------------------------------------

#include "ClosestUVAndFaceOnMesh.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MDistance.h>

#include <shlobj.h>
#pragma warning(pop)


namespace rage
{


// -----------------------------------------------------------------------------
void* ClosestUVAndFaceOnMesh::creator()
// -----------------------------------------------------------------------------
{
    return new ClosestUVAndFaceOnMesh();
}


//-----------------------------------------------------------------------------
ClosestUVAndFaceOnMesh::ClosestUVAndFaceOnMesh( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
ClosestUVAndFaceOnMesh::~ClosestUVAndFaceOnMesh( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
MStatus ClosestUVAndFaceOnMesh::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
	MDoubleArray result;

	if ((args.length()<4) || (args.length()>6))
	{
		MGlobal::displayInfo("USAGE: ClosestUVAndFaceOnMesh x y z mesh [UVset]");
		status = MS::kInvalidParameter;
		return status;
	}
	
	MVector inPosition;

	inPosition.x = args.asDouble(0);
	inPosition.y = args.asDouble(1);
	inPosition.z = args.asDouble(2);

	inPosition.x = MDistance::uiToInternal(inPosition.x);
	inPosition.y = MDistance::uiToInternal(inPosition.y);
	inPosition.z = MDistance::uiToInternal(inPosition.z);

	MString UVSet("");
	MString *UVSetPtr=NULL;
	if (args.length()>4)
	{
		UVSet= args.asString(4);
		UVSetPtr = &UVSet;
	}

	MSelectionList list;
	MDagPath dagPath;
	list.add(args.asString(3));
	list.getDagPath(0, dagPath);
		
	MFnMesh meshFn(dagPath);

	MPoint meshPosition;
	MVector meshNormal;
	int closestFaceIndex;
	status = meshFn.getClosestPointAndNormal(inPosition, meshPosition, meshNormal, MSpace::kWorld, &closestFaceIndex);
	if (!status)
	{
		return status;
	}

	// CREATE FACE ITERATOR, AND SET ITS INDEX TO THAT OF THE CLOSEST FACE, AND USE ITERATOR TO FIND CLOSEST UV:
	MItMeshPolygon faceIter(dagPath);

	int dummyIndex;
	faceIter.setIndex(closestFaceIndex, dummyIndex);
	float2 UVPoint;
	status = faceIter.getUVAtPoint(meshPosition, UVPoint, MSpace::kWorld, UVSetPtr);
	if (!status)
	{
		return status;
	}
	
	result.append(closestFaceIndex);
	result.append(UVPoint[0]);
	result.append(UVPoint[1]);

	setResult(result);
    return status;
}


} // namespace rage
