// -----------------------------------------------------------------------------
// $Workfile: GetUVOnMeshCmd.cpp
//   Creator: Roman Rath
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: This command returns the UV coordinates for a UVSet at a given
//              point, mesh and face index.
// -----------------------------------------------------------------------------

#include "GetUVOnMeshCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MDistance.h>
#pragma warning(pop)


namespace rage
{


// -----------------------------------------------------------------------------
void* GetUVOnMeshCmd::creator()
// -----------------------------------------------------------------------------
{
    return new GetUVOnMeshCmd();
}


//-----------------------------------------------------------------------------
GetUVOnMeshCmd::GetUVOnMeshCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
GetUVOnMeshCmd::~GetUVOnMeshCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
void GetUVOnMeshCmd::computeFaceUVParameters(const MDagPath& dagPath,
											 const int currFace,
											 MString *UVSetPtr)
//-----------------------------------------------------------------------------
{
	MVector	mPlaneNormal;
	double	mPlaneOffset;
	MString	mCurrentObjectName;
	MIntArray mCurrentObjectFaceEdges;
	MPoint	mFaceCenter;

	MFnMesh mesh(dagPath);
	mCurrentObjectName = mesh.fullPathName();

	// determine the faces edges
	MItMeshPolygon faceIt(dagPath);

	int prevIndex;
	faceIt.setIndex(currFace, prevIndex);
	faceIt.getEdges(mCurrentObjectFaceEdges);

	MStatus status = MS::kSuccess;
	mFaceCenter = faceIt.center(MSpace::kWorld, &status);

	// create a plane from that face
	mesh.getPolygonNormal(currFace, mPlaneNormal, MSpace::kWorld);
	// for some reasons we need to negate it, double-sided?
	mPlaneNormal = -mPlaneNormal;
	mPlaneOffset = -(mPlaneNormal * mFaceCenter);

	MPointArray faceVertices;
	faceIt.getPoints(faceVertices, MSpace::kWorld);

	float2 faceUVs[3];
	faceIt.getUV(0, faceUVs[0], UVSetPtr);
	faceIt.getUV(1, faceUVs[1], UVSetPtr);
	faceIt.getUV(2, faceUVs[2], UVSetPtr);

	double m[4][4] =
		{{faceVertices[0].x, faceVertices[0].y, faceVertices[0].z, 1},
	     {faceVertices[1].x, faceVertices[1].y, faceVertices[1].z, 1},
	     {faceVertices[2].x, faceVertices[2].y, faceVertices[2].z, 1},
         {mPlaneNormal.x, mPlaneNormal.y, mPlaneNormal.z, 0}};
    MMatrix matrix(m);

	matrix = matrix.inverse();
	matrix = matrix.transpose();

	MPoint u(faceUVs[0][0], faceUVs[1][0], faceUVs[2][0], 0);
	MPoint v(faceUVs[0][1], faceUVs[1][1], faceUVs[2][1], 0);

	MPoint gU = u*matrix;
	MPoint gV = v*matrix;

	mGu = MVector(gU.x, gU.y, gU.z);
	mDu = gU.w;
	mGv = MVector(gV.x, gV.y, gV.z);
	mDv = gV.w;
}


//-----------------------------------------------------------------------------
MStatus GetUVOnMeshCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
	MDoubleArray result;

	if ((args.length()<5) || (args.length()>7))
	{
		MGlobal::displayInfo("USAGE: rageGetUVOnMesh x y z mesh faceindex [UVset]");
		status = MS::kInvalidParameter;
		return status;
	}

	MVector inPosition;
	inPosition.x = MDistance::uiToInternal(args.asDouble(0));
	inPosition.y = MDistance::uiToInternal(args.asDouble(1));
	inPosition.z = MDistance::uiToInternal(args.asDouble(2));

	MString UVSet("");
	MString *UVSetPtr=NULL;
	if (args.length()>5)
	{
		UVSet= args.asString(5);
		UVSetPtr = &UVSet;
	}

	MSelectionList list;
	MDagPath dagPath;
	list.add(args.asString(3));
	list.getDagPath(0, dagPath);
		
	int closestFaceIndex = args.asInt(4);

	computeFaceUVParameters(dagPath, closestFaceIndex, UVSetPtr);

	result.append(float(mGu*inPosition + mDu));
	result.append(float(mGv*inPosition + mDv));

	setResult(result);
    return status;
}


} // namespace rage
