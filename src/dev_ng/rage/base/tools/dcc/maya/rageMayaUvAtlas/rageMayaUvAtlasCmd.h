// -----------------------------------------------------------------------------
// $Workfile: rageMayaUVAtlasCmd.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This plugin uses the directx atlas UV creation libraries
// -----------------------------------------------------------------------------

#ifndef RSV_MAYA_UVATLAS_CMD_H
#define RSV_MAYA_UVATLAS_CMD_H

#include "rageMayaUVAtlasMain.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#pragma warning(pop)
#include "polyModifierCmd.h"


#include <string>
#include <vector>
using namespace std;

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class rageMayaUVAtlasCmd : public polyModifierCmd
// -----------------------------------------------------------------------------
{
public:
    rageMayaUVAtlasCmd();
    virtual
    ~rageMayaUVAtlasCmd();

    MStatus doIt(const MArgList& args);
    MStatus redoIt();
    MStatus undoIt();
    bool isUndoable() const;

    static
    void* creator();

    MStatus	initModifierNode( MObject modifierNode );
    MStatus	directModifier( MObjectArray meshes );

protected:
    virtual MStatus doAdditionalyConnects(MDGModifier& dgModifier, MObject& modifierNode, int idx, const modifyPolyData& data);

private:
    static bool ParseCommandLine( Settings& settings, 
                           std::vector<std::string>& meshes,
                           MString& nodeName,
                           const MArgList& args);

    void DisplayUsage();

    static LPDIRECT3D9             spD3D;
    static LPDIRECT3DDEVICE9       spd3dDevice;
    
    Settings mSettings;
    MMatrixArray mMatrixArray;
};

} // namespace rage


#endif // RSV_MAYA_UVATLAS_CMD_H
