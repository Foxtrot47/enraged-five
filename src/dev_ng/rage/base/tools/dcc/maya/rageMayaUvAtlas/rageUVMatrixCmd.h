// -----------------------------------------------------------------------------
// $Workfile: rageUVMatrixCmd.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This plugin uses the directx atlas UV creation libraries
// -----------------------------------------------------------------------------

#ifndef RSV_UVMATRIX_CMD_H
#define RSV_UVMATRIX_CMD_H

#include "rageMayaUVAtlasMain.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#include <maya/MObject.h>
#include <maya/MStatus.h>
#pragma warning(pop)
#include "polyModifierCmd.h"


#include <string>
#include <vector>

using namespace std;


namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class rageUVMatrixCmd : public polyModifierCmd
// -----------------------------------------------------------------------------
{
public:
    rageUVMatrixCmd();
    virtual
    ~rageUVMatrixCmd();

    MStatus doIt(const MArgList& args);
    MStatus redoIt();
    MStatus undoIt();
    bool isUndoable() const;

    static
    void* creator();

    MStatus	initModifierNode( MObject modifierNode );
    MStatus	directModifierOne( MObject mesh );


private:
    bool ParseCommandLine( double* matrix, 
        MString &UVSetName,
        std::vector<std::string>& meshes,
        const MArgList& arguments);

    void DisplayUsage();
    
    double mMatrix[6];
    MString mUVSetName;

};

} // namespace rage


#endif // RSV_UVMATRIX_CMD_H
