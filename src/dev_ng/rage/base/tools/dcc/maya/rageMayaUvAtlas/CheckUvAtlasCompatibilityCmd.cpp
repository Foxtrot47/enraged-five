// -----------------------------------------------------------------------------
// $Workfile: CheckUVAtlasCompatibilityCmd.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\CheckUVAtlasCompatibilityCmd\CheckUVAtlasCompatibilityCmd.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This command calculates the area by summing up the poly`s areas.
// -----------------------------------------------------------------------------

#include "CheckUVAtlasCompatibilityCmd.h"
#include "rageMayaUVAtlasMain.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#pragma warning(pop)



namespace rage
{


// -----------------------------------------------------------------------------
void* CheckUVAtlasCompatibilityCmd::creator()
// -----------------------------------------------------------------------------
{
    return new CheckUVAtlasCompatibilityCmd();
}


//-----------------------------------------------------------------------------
CheckUVAtlasCompatibilityCmd::CheckUVAtlasCompatibilityCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
CheckUVAtlasCompatibilityCmd::~CheckUVAtlasCompatibilityCmd( )
//-----------------------------------------------------------------------------
{
}


#define CHECK_STATUS(message)\
if (MS::kSuccess != status)\
{\
    ReportError(#message " failed" );\
    setResult(false);\
    return status;\
}\


//-----------------------------------------------------------------------------
MStatus CheckUVAtlasCompatibilityCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
    bool result = false;
    MString UVSetName;
//    MString* currentUVSet=NULL;

    if (args.length() > 0)    
    {
        MSelectionList list;
        MDagPath dagPath;
        status = list.add(args.asString(0));
        CHECK_STATUS("select object");

        status = list.getDagPath(0, dagPath);
        CHECK_STATUS("getDagPath");
        
        MObject object = dagPath.node(&status);
        CHECK_STATUS("get node object");

        if (!object.isNull())
        {
            if (object.apiType() == MFn::kMesh)
            {
            

                MObjectArray objArray;
                MMatrixArray mtxArray;
                MMatrix matrix;
                LPDIRECT3D9 pD3D = NULL;                // Used to create the D3DDevice
                LPDIRECT3DDEVICE9 pd3dDevice = NULL;    // Our rendering device

                matrix.setToIdentity();
                mtxArray.push_back(matrix);

                status = objArray.append(object);
                CHECK_STATUS("append object");

                if (!createNULLRefDevice(pD3D, pd3dDevice))
                {
                    status = MS::kFailure;
                    ReportError ("Could not create d3d device");
                    return status;
                }

				Settings obSettings;
                status = processUVAtlasCreationOnMayaMesh(
                    objArray, 
                    mtxArray, 
                    pd3dDevice, 
                    obSettings);

                releaseNULLRefDevice(pD3D, pd3dDevice);

                if (status == MS::kSuccess)
                {
                    result = true;
                }
                status = MS::kSuccess;
            }
            else
            {
                status = MS::kFailure;
                ReportError("Object is no mesh");
                return status;
            }
        }
        else
        {
            status = MS::kFailure;
            ReportError("Object is NULL");
            return status;
        }
    }

    setResult(result);
    return status;
}


} // namespace rage
