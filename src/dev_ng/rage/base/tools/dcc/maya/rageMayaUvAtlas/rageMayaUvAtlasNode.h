// -----------------------------------------------------------------------------
// $Workfile: rageMayaUVAtlasNode.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This plugin uses the directx atlas UV creation libraries
// -----------------------------------------------------------------------------

#ifndef RSV_MAYA_UVATLAS_NODE_H
#define RSV_MAYA_UVATLAS_NODE_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxNode.h>
#include <maya/MFnStringData.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnMeshData.h>

#include <d3dx9mesh.h>
#include <WinDef.h>
#pragma warning(pop)

#include "UVCache.h"

class MArgList;

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class rageMayaUVAtlasNode : public MPxNode
// -----------------------------------------------------------------------------
{
public:
    rageMayaUVAtlasNode();
    virtual
    ~rageMayaUVAtlasNode();

    static
    void* creator();

    static
    MStatus	initialize();

    virtual
    void postConstructor();

    virtual 
    MStatus	compute( const MPlug& plug, MDataBlock& data );

    static MStatus setSettings(MObject modifierNode, const Settings& settings);
 
    MStatus shouldSave ( const  MPlug  & plug, bool & ret );

private:
    // helper routines
    MStatus computeQualityScaleMatrix(MMatrix& matrix, unsigned index, MDataBlock&);
    void initSettings(Settings& settings, MDataBlock&);
    MStatus getUVSetName(MString& UVSetNameStr, MString*& UVSetNameStrPtr, MDataBlock&);
    unsigned int getCacheSizeAndHandle(UVCacheData*& UVCache, MDataBlock&);


    static MObject mInMeshes;
    static MObject mOutMeshes;
    static MObject mOutMeshesCache;
    static MObject mIsLoaded;
    static MObject mWorldMatrices;
    static MObject mQuality;
    static MObject mUiMaxCharts;
    static MObject mUiWidth;
    static MObject mUiHeight;
    static MObject mUiMaxStretch;
    static MObject mUiGutter;
    static MObject mUiUVSetName;
    static MObject mUVCache;

    static LPDIRECT3D9             spD3D;
    static LPDIRECT3DDEVICE9       spd3dDevice;

    class UVCacheData* mCacheAttribute;
};

} // namespace rage


#endif // RSV_MAYA_UVATLAS_NODE_H
