// -----------------------------------------------------------------------------
// $Workfile: IsConvex.h  $
//   Creator: Roman Rath
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: This comand returns whether a face is convex or not
// -----------------------------------------------------------------------------

#ifndef RSV_IS_CONVEX_H
#define RSV_IS_CONVEX_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#pragma warning(pop)

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class IsConvexCmd : public MPxCommand
// -----------------------------------------------------------------------------
{
public:
    IsConvexCmd();
    virtual
    ~IsConvexCmd();

    MStatus doIt(const MArgList& args);

    static
    void* creator();
};

} // namespace rage


#endif // RSV_IS_CONVEX_H
