// -----------------------------------------------------------------------------
// $Workfile: ClosestPointAndFaceOnMeshCmd.cpp $
//   Creator: Roman Rath
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: This command returns the closest point and its face index of
//			    the input mesh and point.
// -----------------------------------------------------------------------------

#include "ClosestPointAndFaceOnMeshCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MDistance.h>

#include <shlobj.h>
#pragma warning(pop)


namespace rage
{


// -----------------------------------------------------------------------------
void* ClosestPointAndFaceOnMeshCmd::creator()
// -----------------------------------------------------------------------------
{
    return new ClosestPointAndFaceOnMeshCmd();
}


//-----------------------------------------------------------------------------
ClosestPointAndFaceOnMeshCmd::ClosestPointAndFaceOnMeshCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
ClosestPointAndFaceOnMeshCmd::~ClosestPointAndFaceOnMeshCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
MStatus ClosestPointAndFaceOnMeshCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
	MDoubleArray result;

	if (args.length() != 4)
	{
		MGlobal::displayInfo("USAGE: rageClosestPointAndFaceOnMesh x y z mesh");
		status = MS::kInvalidParameter;
		return status;
	}
	
	MVector inPosition;
	inPosition.x = args.asDouble(0);
	inPosition.y = args.asDouble(1);
	inPosition.z = args.asDouble(2);

	inPosition.x = MDistance::uiToInternal(inPosition.x);
	inPosition.y = MDistance::uiToInternal(inPosition.y);
	inPosition.z = MDistance::uiToInternal(inPosition.z);

	MSelectionList list;
	MDagPath dagPath;
	list.add(args.asString(3));
	list.getDagPath(0, dagPath);
		
	MFnMesh meshFn(dagPath);

	MPoint meshPosition;
	MVector meshNormal;
	int closestFaceIndex;
	status = meshFn.getClosestPointAndNormal(inPosition, meshPosition, meshNormal, MSpace::kWorld, &closestFaceIndex);
	if (!status)
	{
		return status;
	}

	result.append(MDistance::internalToUI(meshPosition.x));
	result.append(MDistance::internalToUI(meshPosition.y));
	result.append(MDistance::internalToUI(meshPosition.z));
	result.append(closestFaceIndex);

	setResult(result);
    return status;
}


} // namespace rage
