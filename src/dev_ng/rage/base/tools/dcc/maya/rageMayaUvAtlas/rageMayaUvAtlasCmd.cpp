// -----------------------------------------------------------------------------
// $Workfile: rageMayaUVAtlasCmd.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\rageMayaUVAtlasCmd\rageMayaUVAtlasCmd.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This plugin uses the directx atlas UV creation libraries
// -----------------------------------------------------------------------------

#include "rageMayaUVAtlasCmd.h"
#include "rageMayaUVAtlasMain.h"
#include "rageMayaUVAtlasNode.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>

#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MDistance.h>
#include <maya/MPointArray.h>
#pragma warning(pop)



namespace rage
{

using namespace std;



//-----------------------------------------------------------------------------
// Global variables
//-----------------------------------------------------------------------------
LPDIRECT3D9             rageMayaUVAtlasCmd::spD3D       = NULL; // Used to create the D3DDevice
LPDIRECT3DDEVICE9       rageMayaUVAtlasCmd::spd3dDevice = NULL; // Our rendering device




// -----------------------------------------------------------------------------
void* rageMayaUVAtlasCmd::creator()
// -----------------------------------------------------------------------------
{
    return new rageMayaUVAtlasCmd();
}


//-----------------------------------------------------------------------------
rageMayaUVAtlasCmd::rageMayaUVAtlasCmd( )
//-----------------------------------------------------------------------------
{
    createNULLRefDevice(spD3D, spd3dDevice);
}


//-----------------------------------------------------------------------------
rageMayaUVAtlasCmd::~rageMayaUVAtlasCmd( )
//-----------------------------------------------------------------------------
{
    releaseNULLRefDevice(spD3D, spd3dDevice);
}


//-----------------------------------------------------------------------------
bool rageMayaUVAtlasCmd::ParseCommandLine( Settings& settings, 
                                      vector<std::string>& meshes,
                                      MString& nodeName,
                                      const MArgList& arguments)
//-----------------------------------------------------------------------------
{
    MStatus status=MStatus::kSuccess;
    int numParameters = arguments.length(&status);

    int currentParam=0;
    bool paramOk=true;

    while ((currentParam<numParameters) && (paramOk))
    {
        if (arguments.asString(currentParam)==MString("-n"))
        {
            currentParam++;
            if (currentParam<numParameters)
            {
                settings.maxcharts = atoi(arguments.asString(currentParam).asChar());
            }
            else
            {
                paramOk=false;
            }
        }
        else if (arguments.asString(currentParam)==MString("-st"))
        {
            currentParam++;
            if (currentParam<numParameters)
            {
                settings.maxstretch = (float)atof(arguments.asString(currentParam).asChar());
            }
            else
            {
                paramOk=false;
            }
        }
        else if (arguments.asString(currentParam)==MString("-g"))
        {
            currentParam++;
            if (currentParam<numParameters)
            {
                settings.gutter = (float)atof(arguments.asString(currentParam).asChar());
            }
            else
            {
                paramOk=false;
            }
        }
        else if (arguments.asString(currentParam)==MString("-w"))
        {
            currentParam++;
            if (currentParam<numParameters)
            {
                settings.width = atoi(arguments.asString(currentParam).asChar());
            }
            else
            {
                paramOk=false;
            }
        }
        else if (arguments.asString(currentParam)==MString("-h"))
        {
            currentParam++;
            if (currentParam<numParameters)
            {
                settings.height = atoi(arguments.asString(currentParam).asChar());
            }
            else
            {
                paramOk=false;
            }
        }
        else if (arguments.asString(currentParam)==MString("-us"))
        {
            currentParam++;
            if (currentParam<numParameters)
            {
                settings.UVSetName = arguments.asString(currentParam);
            }
            else
            {
                paramOk=false;
            }
        }
        else if (arguments.asString(currentParam)==MString("-name"))
        {
            currentParam++;
            if (currentParam<numParameters)
            {
                nodeName = arguments.asString(currentParam);
            }
            else
            {
                paramOk=false;
            }
        }
        else
        {
            meshes.push_back(std::string(arguments.asString(currentParam).asChar()));
        }
        currentParam++;
    }

    return (paramOk && (meshes.size()>0));
}


//-----------------------------------------------------------------------------
void rageMayaUVAtlasCmd::DisplayUsage()
//-----------------------------------------------------------------------------
{
    MGlobal::displayInfo( "rageMayaUVAtlasCmd - a command line tool for generating UV Atlases" );
    MGlobal::displayInfo( "Usage: rageMayaUVAtlasCmd [-n #] [-nt] [-nc] [-st #.##] [-w #] [-h #] [-us UVsetname] [-g #.##] [meshname1] [meshname2] ..." );
    MGlobal::displayInfo( "where:" );
    MGlobal::displayInfo( "" );
    MGlobal::displayInfo( "  [-n #]\tSpecifies the maximum number of charts to generate (default 0, partition by stretch)." );
    MGlobal::displayInfo( "  [-st #.##]\tSpecifies the maximum amount of stretch, valid range is [0-1] (default 0.16667)." );
    MGlobal::displayInfo( "  [-g #.##]\tSpecifies the gutter width (default 2)." );
    MGlobal::displayInfo( "  [-w #]\tSpecifies the texture width (default 512)." );
    MGlobal::displayInfo( "  [-h #]\tSpecifies the texture height (default 512)." );
    MGlobal::displayInfo( "  [-us UVsetname]\tSpecifies the name of the UV set (default is active UV set)." );
    MGlobal::displayInfo( "  [meshname*]\tSpecifies the meshes to generate Atlases for." );
}


//-----------------------------------------------------------------------------
MStatus rageMayaUVAtlasCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status=MStatus::kSuccess;
    vector<std::string> meshes;
    MString nodeName;

    if ( (spd3dDevice==NULL) || (spD3D==NULL))
    {
        status = MStatus::kFailure;
        status.perror("DirectX9 initialization failed.");
        MGlobal::displayError("DirectX9 initialization failed.");
        return status;
    }

    //IDirect3DDevice9* pd3dDevice = NULL;
    
    if (!ParseCommandLine(mSettings, meshes, nodeName, args))
    {
        DisplayUsage();
        status = MStatus::kFailure;
        status.perror("wrong arguments");
        MGlobal::displayError("wrong arguments.");
        return status;
    }

    setModifierNodeType( ID_UVATLAS_NODE );
    setModifierNodeName(nodeName);
    for (std::vector<std::string>::const_iterator it = meshes.begin(); it != meshes.end(); ++it)
    {
        MSelectionList list;
        MDagPath dagPath;
        list.add(MString(it->c_str()));
        list.getDagPath(0, dagPath);

        dagPath.extendToShape();
        addMeshNode(dagPath);
    }
        
    status = doModifyPoly();

    return status;
}


//-----------------------------------------------------------------------------
MStatus rageMayaUVAtlasCmd::undoIt()
//-----------------------------------------------------------------------------
{
    MStatus status;

    status = undoModifyPoly();

    if( status == MS::kSuccess )
    {
        setResult( "splitUV undo succeeded!" );
    }
    else
    {
        setResult( "splitUV undo failed!" );
    }

    return status;
}


//-----------------------------------------------------------------------------
MStatus rageMayaUVAtlasCmd::redoIt()
//-----------------------------------------------------------------------------
{
    MStatus status;

    // Process the polyModifierCmd
    //
    status = redoModifyPoly();

    if( status == MS::kSuccess )
    {
        setResult( "splitUV command succeeded!" );
    }
    else
    {
        displayError( "splitUV command failed!" );
    }

    return status;
}


//-----------------------------------------------------------------------------
bool rageMayaUVAtlasCmd::isUndoable() const
//-----------------------------------------------------------------------------
{
    return true;
}


//-----------------------------------------------------------------------------
MStatus	rageMayaUVAtlasCmd::initModifierNode( MObject modifierNode )
//-----------------------------------------------------------------------------
{
    return rageMayaUVAtlasNode::setSettings(modifierNode, mSettings);
}


//-----------------------------------------------------------------------------
MStatus	rageMayaUVAtlasCmd::directModifier( MObjectArray meshObjects )
//-----------------------------------------------------------------------------
{
    MStatus status;

    status = processUVAtlasCreationOnMayaMesh(meshObjects, mMatrixArray, spd3dDevice, mSettings);

    return status;
}


//-----------------------------------------------------------------------------
MStatus rageMayaUVAtlasCmd::doAdditionalyConnects(MDGModifier& dgModifier, 
                                              MObject& modifierNode, 
                                              int idx, 
                                              const modifyPolyData& data)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    MFnDependencyNode depNodeFn;
    depNodeFn.setObject(modifierNode);
    MObject inMatrixAttr= depNodeFn.attribute( "WorldMatrices" );        
    MPlug dstPlug = depNodeFn.findPlug(inMatrixAttr);
    if (dstPlug.isArray())
    {
        dstPlug = dstPlug.elementByLogicalIndex(idx);
    }
    else
    {
        status = MStatus::kFailure;
        status.perror("WorldMatrices attribute is not an array.");
        return status;
    }

    depNodeFn.setObject(data.meshNodeTransform);
    MObject outMatrixAttr= depNodeFn.attribute("worldMatrix" ); 
    MPlug srcPlug = depNodeFn.findPlug(outMatrixAttr);
    if (srcPlug.isArray())
    {
        srcPlug = srcPlug.elementByLogicalIndex(0);
    }
    status = dgModifier.connect( srcPlug, dstPlug);


    depNodeFn.setObject(data.meshNodeTransform);
    MObject outQualityAttr= depNodeFn.attribute("UVQuality" ); 
    if (!outQualityAttr.isNull())
    {
        MPlug srcPlug = depNodeFn.findPlug(outQualityAttr);

        MFnDependencyNode depNodeFn;
        depNodeFn.setObject(modifierNode);
        MObject qualityAttr= depNodeFn.attribute( "Quality" );        
        MPlug dstPlug = depNodeFn.findPlug(qualityAttr);
        if (dstPlug.isArray())
        {
            dstPlug = dstPlug.elementByLogicalIndex(idx);
        }
        else
        {
            status = MStatus::kFailure;
            status.perror("quality attribute is not an array.");
            return status;
        }

        status = dgModifier.connect( srcPlug, dstPlug);
    }


    return status;
}


} // namespace rage
