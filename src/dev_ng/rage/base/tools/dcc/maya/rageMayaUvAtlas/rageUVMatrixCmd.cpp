// -----------------------------------------------------------------------------
// $Workfile: rageUVMatrixCmd.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\rageUVMatrixCmd\rageUVMatrixCmd.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This plugin uses the directx atlas UV creation libraries
// -----------------------------------------------------------------------------

#include "rageUVMatrixCmd.h"
#include "rageMayaUVAtlasMain.h"
#include "rageUVMatrixNode.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MDistance.h>
#include <maya/MPointArray.h>
#pragma warning(pop)



namespace rage
{

using namespace std;


// -----------------------------------------------------------------------------
void* rageUVMatrixCmd::creator()
// -----------------------------------------------------------------------------
{
    return new rageUVMatrixCmd();
}


//-----------------------------------------------------------------------------
rageUVMatrixCmd::rageUVMatrixCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
rageUVMatrixCmd::~rageUVMatrixCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
bool rageUVMatrixCmd::ParseCommandLine( double* matrix, 
                                       MString &UVSetName,
                                       vector<std::string>& meshes,
                                       const MArgList& arguments)
//-----------------------------------------------------------------------------
{
    MStatus status=MStatus::kSuccess;
    int numParameters = arguments.length(&status);

    int currentParam=0;
    bool paramOk=true;

    matrix[0]=1.0;
    matrix[1]=0.0;
    matrix[2]=0.0;
    matrix[3]=1.0;
    matrix[4]=0.0;
    matrix[5]=0.0;

    while ((currentParam<numParameters) && (paramOk))
    {
        if (arguments.asString(currentParam)==MString("-m"))
        {
            currentParam++;
            for (int i=0; (i<6) && (currentParam<numParameters); i++)
            {
                matrix[i]= (float)atof(arguments.asString(currentParam).asChar());
            }
            if (currentParam>=numParameters)
            {
                paramOk=false;
            }
        }
        else if (arguments.asString(currentParam)==MString("-us"))
        {
            currentParam++;
            if (currentParam<numParameters)
            {
                UVSetName = arguments.asString(currentParam);
            }
            else
            {
                paramOk=false;
            }
        }
        else
        {
            meshes.push_back(std::string(arguments.asString(currentParam).asChar()));
        }
        currentParam++;
    }

    return (paramOk && (meshes.size()>0));
}


//-----------------------------------------------------------------------------
void rageUVMatrixCmd::DisplayUsage()
//-----------------------------------------------------------------------------
{
    MGlobal::displayInfo( "rageUVMatrixCmd - a command line tool for modifying a UVset with a 2x3 matrix." );
    MGlobal::displayInfo( "Usage: rageUVMatrixCmd [-m # # # # # #] [-us UVsetname] [-g #.##] [meshname1] [meshname2] ..." );
}


//-----------------------------------------------------------------------------
MStatus rageUVMatrixCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status=MStatus::kSuccess;
    vector<std::string> meshes;
    
    if (!ParseCommandLine(mMatrix, mUVSetName, meshes, args))
    {
        DisplayUsage();
        status = MStatus::kFailure;
        status.perror("wrong arguments");
        MGlobal::displayError("wrong arguments.");
        return status;
    }

    for (std::vector<std::string>::const_iterator it = meshes.begin(); it != meshes.end(); ++it)
    {
        MSelectionList list;
        MDagPath dagPath;
        list.add(MString(it->c_str()));
        list.getDagPath(0, dagPath);

        setModifierNodeType( ID_UVMATRIX_NODE );
        dagPath.extendToShape();
        addMeshNode(dagPath);
        status = doModifyPoly();
    }
    return status;
}


//-----------------------------------------------------------------------------
MStatus rageUVMatrixCmd::undoIt()
//-----------------------------------------------------------------------------
{
    MStatus status;

    status = undoModifyPoly();

    if( status == MS::kSuccess )
    {
        setResult( "rageUVMatrixCmd undo succeeded!" );
    }
    else
    {
        setResult( "rageUVMatrixCmd undo failed!" );
    }

    return status;
}


//-----------------------------------------------------------------------------
MStatus rageUVMatrixCmd::redoIt()
//-----------------------------------------------------------------------------
{
    MStatus status;

    // Process the polyModifierCmd
    status = redoModifyPoly();

    if( status == MS::kSuccess )
    {
        setResult( "rageUVMatrixCmd command succeeded!" );
    }
    else
    {
        displayError( "rageUVMatrixCmd command failed!" );
    }

    return status;
}


//-----------------------------------------------------------------------------
bool rageUVMatrixCmd::isUndoable() const
//-----------------------------------------------------------------------------
{
    return true;
}


//-----------------------------------------------------------------------------
MStatus	rageUVMatrixCmd::initModifierNode( MObject modifierNode )
//-----------------------------------------------------------------------------
{
    return rageUVMatrixNode::setSettings(modifierNode, mMatrix, mUVSetName);
}


//-----------------------------------------------------------------------------
MStatus	rageUVMatrixCmd::directModifierOne( MObject meshObj )
//-----------------------------------------------------------------------------
{
    MStatus status;

    MString* pUVSet=NULL;
    if (mUVSetName.length()>0)
    {
        pUVSet = &mUVSetName;
    }
    status = processMatrixOnUVSet(meshObj, mMatrix, pUVSet);

    return status;
}

} // namespace rage
