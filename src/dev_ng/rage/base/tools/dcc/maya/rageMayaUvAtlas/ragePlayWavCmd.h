// -----------------------------------------------------------------------------
// $Workfile: ragePlayWavCmd.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This command converts a given path to the old 8.3 style path
// -----------------------------------------------------------------------------

#ifndef RSV_PLAYWAV_CMD_H
#define RSV_PLAYWAV_CMD_H


#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#pragma warning(pop)

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class ragePlayWavCmd : public MPxCommand
// -----------------------------------------------------------------------------
{
public:
    ragePlayWavCmd();
    virtual
    ~ragePlayWavCmd();

    MStatus doIt(const MArgList& args);

    static
    void* creator();

};

} // namespace rage


#endif // RSV_PLAYWAV_CMD_H
