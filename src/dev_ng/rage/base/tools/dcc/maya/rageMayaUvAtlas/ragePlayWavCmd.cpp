// -----------------------------------------------------------------------------
// $Workfile: ragePlayWavCmd.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\ragePlayWavCmd\ragePlayWavCmd.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// Plays a wav file asyncron
// -----------------------------------------------------------------------------

#include "ragePlayWavCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#include <shlobj.h>
#pragma warning(pop)


namespace rage
{


// -----------------------------------------------------------------------------
void* ragePlayWavCmd::creator()
// -----------------------------------------------------------------------------
{
    return new ragePlayWavCmd();
}


//-----------------------------------------------------------------------------
ragePlayWavCmd::ragePlayWavCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
ragePlayWavCmd::~ragePlayWavCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
MStatus ragePlayWavCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    if (args.length()==1)
    {
        MString param = args.asString(0);
        if (!sndPlaySound(param.asChar(),SND_ASYNC))
        {
            status = MS::kFailure;
        }
    }
    else
    {
        status = MS::kInvalidParameter;
    }

    return status;
}


} // namespace rage
