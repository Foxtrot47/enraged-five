// -----------------------------------------------------------------------------
// $Workfile: IsConvex.cpp $
//   Creator: Roman Rath
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: This comand returns whether a face is convex or not
// -----------------------------------------------------------------------------

#include "IsConvexCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MDistance.h>

#include <shlobj.h>
#pragma warning(pop)


namespace rage
{


// -----------------------------------------------------------------------------
void* IsConvexCmd::creator()
// -----------------------------------------------------------------------------
{
    return new IsConvexCmd();
}


//-----------------------------------------------------------------------------
IsConvexCmd::IsConvexCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
IsConvexCmd::~IsConvexCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
MStatus IsConvexCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
	MDoubleArray result;

	if (args.length() != 1)
	{
		MGlobal::displayInfo("USAGE: isConvex face");
		status = MS::kInvalidParameter;
		return status;
	}

	MSelectionList list;
	MDagPath dagPath;
	MObject component;
	list.add(args.asString(0));
	list.getDagPath(0, dagPath, component);
			
	MItMeshPolygon faceIt(dagPath, component);

	bool convex = faceIt.isConvex(&status);
	if (!status)
	{
		return status;
	}

	result.append(convex);

	setResult(result);
    return status;
}


} // namespace rage
