// -----------------------------------------------------------------------------
// $Workfile: ClosestPointAndFaceOnMeshCmd.h  $
//   Creator: Roman Rath
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: This command returns the closest point and its face index of
//			    the input mesh and point.
// -----------------------------------------------------------------------------

#ifndef RSV_CLOSEST_POINT_AND_FACE_ON_MESH_CMD_H
#define RSV_CLOSEST_POINT_AND_FACE_ON_MESH_CMD_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#pragma warning(pop)

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class ClosestPointAndFaceOnMeshCmd : public MPxCommand
// -----------------------------------------------------------------------------
{
public:
    ClosestPointAndFaceOnMeshCmd();
    virtual
    ~ClosestPointAndFaceOnMeshCmd();

    MStatus doIt(const MArgList& args);

    static
    void* creator();
};

} // namespace rage


#endif // RSV_CLOSEST_POINT_AND_FACE_ON_MESH_CMD_H
