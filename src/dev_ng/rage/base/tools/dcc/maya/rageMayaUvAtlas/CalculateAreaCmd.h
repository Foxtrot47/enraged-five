// -----------------------------------------------------------------------------
// $Workfile: CalculateAreaCmd.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This command calculates the area by summing up the poly`s areas.
// -----------------------------------------------------------------------------

#ifndef RSV_CALCULATE_AREA_CMD_H
#define RSV_CALCULATE_AREA_CMD_H

#include "CalculateAreaCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#pragma warning(pop)

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class CalculateAreaCmd : public MPxCommand
// -----------------------------------------------------------------------------
{
public:
    CalculateAreaCmd();
    virtual
    ~CalculateAreaCmd();

    MStatus doIt(const MArgList& args);

    static
    void* creator();

};

} // namespace rage


#endif // RSV_CALCULATE_AREA_CMD_H
