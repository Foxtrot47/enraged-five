// -----------------------------------------------------------------------------
// $Workfile: GetUVOnMeshCmd.h  $
//   Creator: Roman Rath
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: This command returns the UV coordinates for a UVSet at a given
//              point, mesh and face index.
// -----------------------------------------------------------------------------

#ifndef RSV_GET_UV_ON_MESH_CMD_H
#define RSV_GET_UV_ON_MESH_CMD_H


#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MDagPath.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MMatrix.h>
#include <maya/MStatus.h>
#include <maya/MVector.h>
#include <maya/MArgList.h>
#pragma warning(pop)


namespace rage
{

struct Settings;

// -----------------------------------------------------------------------------
class GetUVOnMeshCmd : public MPxCommand
// -----------------------------------------------------------------------------
{
public:
    GetUVOnMeshCmd();
    virtual
    ~GetUVOnMeshCmd();

    MStatus doIt(const MArgList& args);

    static
    void* creator();

private:
	void computeFaceUVParameters(const MDagPath& dagPath, const int currFace, MString *UVSetPtr);

	MVector	mGu;
	MVector	mGv;
	double	mDu;
	double	mDv;
};

} // namespace rage


#endif // RSV_GET_UV_ON_MESH_CMD_H
