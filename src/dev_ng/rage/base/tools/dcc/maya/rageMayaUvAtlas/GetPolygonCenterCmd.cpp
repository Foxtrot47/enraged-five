// -----------------------------------------------------------------------------
// $Workfile: GetPolygonCenter.cpp $
//   Creator: Roman Rath
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: This comand returns the center point of the input object
// -----------------------------------------------------------------------------

#include "GetPolygonCenterCmd.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MDistance.h>

#include <shlobj.h>
#pragma warning(pop)


namespace rage
{


// -----------------------------------------------------------------------------
void* GetPolygonCenterCmd::creator()
// -----------------------------------------------------------------------------
{
    return new GetPolygonCenterCmd();
}


//-----------------------------------------------------------------------------
GetPolygonCenterCmd::GetPolygonCenterCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
GetPolygonCenterCmd::~GetPolygonCenterCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
MStatus GetPolygonCenterCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
	MDoubleArray result;

	if (args.length() != 1)
	{
		MGlobal::displayInfo("USAGE: rageGetPolygonCenter face");
		status = MS::kInvalidParameter;
		return status;
	}

	MSelectionList list;
	MDagPath dagPath;
	MObject component;
	list.add(args.asString(0));
	list.getDagPath(0, dagPath, component);
			
	MItMeshPolygon faceIt(dagPath, component);
	MPoint polygonCenter;

	polygonCenter = faceIt.center(MSpace::kWorld, &status);
	if (!status)
	{
		return status;
	}

	result.append(MDistance::internalToUI(polygonCenter.x));
	result.append(MDistance::internalToUI(polygonCenter.y));
	result.append(MDistance::internalToUI(polygonCenter.z));

	setResult(result);
    return status;
}


} // namespace rage
