// -----------------------------------------------------------------------------
// $Workfile: rageMayaUVAtlas.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This plugin uses the directx atlas UV creation libraries
// -----------------------------------------------------------------------------

#ifndef RSV_MAYA_UVATLAS_H
#define RSV_MAYA_UVATLAS_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>

#include <d3dx9mesh.h>
#include <WinDef.h>
#pragma warning(pop)

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class rageMayaUVAtlas : public MPxCommand
// -----------------------------------------------------------------------------
{
public:
    rageMayaUVAtlas();
    virtual
    ~rageMayaUVAtlas();

    MStatus doIt(const MArgList& args);
    //MStatus redoIt();
    //MStatus undoIt();
    //bool isUndoable() const;

    static
    void* creator();

private:
    bool ParseCommandLine( Settings* pSettings, const MArgList& args);
    void DisplayUsage();

    bool CheckMeshValidation(LPD3DXMESH pMesh, LPD3DXMESH *pMeshOut, DWORD **ppAdjacency);
    bool createD3DxMeshFromMaya(std::string meshName, LPD3DXMESH *ppMesh);
    bool mergeBackD3DxMeshIntoMaya(std::string meshName, LPD3DXMESH pMesh);   

    bool CreateNULLRefDevice();

    static LPDIRECT3D9             spD3D;
    static LPDIRECT3DDEVICE9       spd3dDevice;

};

} // namespace rage


#endif // RSV_MAYA_UVATLAS_H
