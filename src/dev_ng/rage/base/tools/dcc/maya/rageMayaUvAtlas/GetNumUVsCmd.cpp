// -----------------------------------------------------------------------------
// $Workfile: GetNumUVsCmd.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\GetNumUVsCmd\GetNumUVsCmd.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This command calculates the area by summing up the poly`s areas.
// -----------------------------------------------------------------------------

#include "GetNumUVsCmd.h"
#include "rageMayaUVAtlasMain.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MGlobal.h>
#include <maya/MFnMesh.h>
#include <maya/MSelectionList.h>
#include <maya/MArgList.h>
#include <maya/MDagPath.h>
#pragma warning(pop)

//#include <maya/MItMeshPolygon.h>


namespace rage
{


// -----------------------------------------------------------------------------
void* GetNumUVsCmd::creator()
// -----------------------------------------------------------------------------
{
    return new GetNumUVsCmd();
}


//-----------------------------------------------------------------------------
GetNumUVsCmd::GetNumUVsCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
GetNumUVsCmd::~GetNumUVsCmd( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
MStatus GetNumUVsCmd::doIt(const MArgList& args)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
    MDoubleArray result;
    MString UVSetName;
    MString* currentUVSet=NULL;

    for (unsigned int i=0; (i< args.length()) && (MS::kSuccess == status); i++)
    {
        if (args.asString(i)==MString("-us"))
        {
            i++;
            if (i< args.length())
            {
                UVSetName = args.asString(i);
                currentUVSet = &UVSetName;
            }
        }
        else
        {
            double numUVs = 0;

            MSelectionList list;
            MDagPath dagPath;
            list.add(args.asString(i));
            list.getDagPath(0, dagPath);
            
            MFnMesh mesh(dagPath, &status);

            if (MS::kSuccess == status)
            {
                if (currentUVSet)
                {
                    numUVs = mesh.numUVs(*currentUVSet, &status);
                    if (MS::kSuccess != status)
                    {
                        ReportError("couldn't get numUVs for: " + args.asString(i));
                    }
                }
                else
                {
                    numUVs = mesh.numUVs(&status);
                    if (MS::kSuccess != status)
                    {
                        ReportError("couldn't get numUVs for: " + args.asString(i));
                    }
                }
            }
            else
            {
                ReportError("couldn't create MFnMesh for: " + args.asString(i));
            }

            result.append(numUVs);
        }
    }

    setResult(result);
    return status;
}


} // namespace rage
