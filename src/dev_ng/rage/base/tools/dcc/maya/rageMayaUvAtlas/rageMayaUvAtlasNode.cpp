// -----------------------------------------------------------------------------
// $Workfile: rageMayaUVAtlasNode.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\rageMayaUVAtlasCmd\rageMayaUVAtlasCmd.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// -----------------------------------------------------------------------------

#include "rageMayaUVAtlasNode.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnTypedAttribute.h>
#include <maya/MObject.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnStringData.h>
#include <maya/MFnMeshData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnComponentListData.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MMatrix.h>
#include <maya/MFnMatrixData.h>
#include <maya/MFloatArray.h>
#include <maya/MIntArray.h>
#include <maya/MGlobal.h>
#pragma warning(pop)


#include "rageMayaUVAtlasMain.h"



namespace rage
{

using namespace std;

MObject	rageMayaUVAtlasNode::mInMeshes;
MObject	rageMayaUVAtlasNode::mOutMeshes;
MObject	rageMayaUVAtlasNode::mIsLoaded;
MObject	rageMayaUVAtlasNode::mOutMeshesCache;
MObject	rageMayaUVAtlasNode::mUVCache;
MObject rageMayaUVAtlasNode::mWorldMatrices;
MObject rageMayaUVAtlasNode::mQuality;

MObject rageMayaUVAtlasNode::mUiMaxCharts;
MObject rageMayaUVAtlasNode::mUiWidth;
MObject rageMayaUVAtlasNode::mUiHeight;
MObject rageMayaUVAtlasNode::mUiMaxStretch;
MObject rageMayaUVAtlasNode::mUiGutter;
MObject rageMayaUVAtlasNode::mUiUVSetName;

LPDIRECT3D9             rageMayaUVAtlasNode::spD3D       = NULL; // Used to create the D3DDevice
LPDIRECT3DDEVICE9       rageMayaUVAtlasNode::spd3dDevice = NULL; // Our rendering device


// -----------------------------------------------------------------------------
void* rageMayaUVAtlasNode::creator()
// -----------------------------------------------------------------------------
{
    return new rageMayaUVAtlasNode();
}


//-----------------------------------------------------------------------------
rageMayaUVAtlasNode::rageMayaUVAtlasNode( ):
//-----------------------------------------------------------------------------
mCacheAttribute(NULL)
{
    createNULLRefDevice(spD3D, spd3dDevice);
}


//-----------------------------------------------------------------------------
rageMayaUVAtlasNode::~rageMayaUVAtlasNode( )
//-----------------------------------------------------------------------------
{
    releaseNULLRefDevice(spD3D, spd3dDevice);
    //delete mCacheAttribute;
    mCacheAttribute = NULL;
}

#define CHECK_STATUS(message)\
    if (!status)\
    {\
        ReportError(message);\
        return status;\
    }

#define CHECK_STATUS_NR(message)\
    if (!status)\
    {\
        ReportError(message);\
    }

#define ACCUMULATE_FAILURE\
    failure = failure | (!status);

//-----------------------------------------------------------------------------
MStatus rageMayaUVAtlasNode::computeQualityScaleMatrix(MMatrix& matrix, 
                                                   unsigned index,
                                                   MDataBlock& data)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
    matrix = MMatrix::identity;

    MPlug matrixPlug( thisMObject(), mWorldMatrices );
    matrixPlug = matrixPlug.elementByLogicalIndex(index, &status);              CHECK_STATUS ("Couldn't get matrix plug");
    if (!matrixPlug.isNull())
    {
        if (matrixPlug.isConnected())
        {
            MDataHandle hWMatrix = data.inputValue( matrixPlug, &status);       CHECK_STATUS ("Couldn't create matrix handle");
            MFnMatrixData matrixData(hWMatrix.data(), &status);                 CHECK_STATUS ("Couldn't create matrix fn set");
            matrix = matrixData.matrix(&status);                                CHECK_STATUS ("Couldn't read matrix data");
        }
    }

    MPlug qualityPlug( thisMObject(), mQuality);
    qualityPlug = qualityPlug.elementByLogicalIndex(index, &status);            CHECK_STATUS ("Couldn't get quality plug");
    if (!qualityPlug.isNull())
    {
        if (qualityPlug.isConnected())
        {
            MDataHandle hQuality = data.inputValue( qualityPlug, &status );     CHECK_STATUS ("Couldn't create quality handle");
            double quality = hQuality.asDouble();                               CHECK_STATUS ("Couldn't read quality data");
            matrix *=quality;
        }
    }

    return status;
}


//-----------------------------------------------------------------------------
void rageMayaUVAtlasNode::initSettings(Settings& settings, MDataBlock& data)
//-----------------------------------------------------------------------------
{
    settings.maxcharts = data.inputValue(mUiMaxCharts).asInt();
    settings.width = data.inputValue(mUiWidth).asInt();
    settings.height = data.inputValue(mUiHeight).asInt();
    settings.maxstretch = data.inputValue(mUiMaxStretch).asFloat();
    settings.gutter= data.inputValue(mUiGutter).asFloat();
}


//-----------------------------------------------------------------------------
MStatus rageMayaUVAtlasNode::getUVSetName(MString& UVSetNameStr, 
                                      MString*& UVSetNameStrPtr,
                                      MDataBlock& data)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    MDataHandle UVSetNameHandle = data.inputValue(mUiUVSetName, &status); CHECK_STATUS("get UVSetName failed");
    MFnStringData UVSetName(UVSetNameHandle.data());
    
    UVSetNameStr = UVSetName.string();
    UVSetNameStrPtr =NULL;
    if (UVSetNameStr.length()>0)
    {
        UVSetNameStrPtr = &UVSetNameStr; 
    }

    return status;
}

//-----------------------------------------------------------------------------
unsigned int rageMayaUVAtlasNode::getCacheSizeAndHandle(UVCacheData*& UVCache, 
                                                    MDataBlock& data)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    UVCache = NULL;
    MDataHandle UVCacheHandle = data.inputValue(mUVCache, &status);
    if (status)
    {                 
        UVCache = (UVCacheData*)UVCacheHandle.asPluginData();
    }
    unsigned int numCacheElements = 0;
    if (UVCache)
    {
        numCacheElements  = UVCache->size();
    }

    return numCacheElements;
}



//-----------------------------------------------------------------------------
MStatus rageMayaUVAtlasNode::compute(const MPlug& plug, MDataBlock& data)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    MObject thisNode = thisMObject();
    MFnDependencyNode depNodeFn(thisNode, &status); CHECK_STATUS("construction of depNodeFn failed");

#ifdef MUV_VERBOSE
    MGlobal::displayInfo(MString("Processing: ") + depNodeFn.name());
#endif

    MDataHandle stateData = data.outputValue( state, &status );
    CHECK_STATUS ("could not get stateData");

    // [ERIK] setup some common variables (and do error checking)
    MArrayDataHandle inputMeshListObj = data.inputArrayValue( mInMeshes, &status );  CHECK_STATUS("get ArrayDataHandle on mInMeshes failed");
    MArrayDataHandle outputMeshListObj = data.outputArrayValue( mOutMeshes, &status );  CHECK_STATUS("get ArrayDataHandle on mOutMeshes failed");
    MArrayDataHandle outputMeshListObjCached = data.inputArrayValue( mOutMeshesCache, &status );  CHECK_STATUS("get ArrayDataHandle on mOutMeshesCache failed");
    unsigned int numInputMeshes = inputMeshListObj.elementCount();
    unsigned int numOutputMeshes = outputMeshListObj.elementCount();
    unsigned int numCacheElementsOldMethod = outputMeshListObjCached.elementCount();

    // check in / out consistency
    if ((numInputMeshes < 1) || (numOutputMeshes < 1))
    {
        status = MS::kInvalidParameter;
        ReportError ("Suspicious elementCounts on input/output meshes");
        return status;
    }
    
    // Check for the HasNoEffect/PassThrough flag on the node.
    // (stateData is an enumeration standard in all depend nodes - stored as short)
    // 
    // (0 = Normal)
    // (1 = HasNoEffect/PassThrough)
    // (2 = Blocking)
    // ...
    short mode = stateData.asShort();
    if(mode == 1 )
    {                
        for (unsigned int idx = 0; idx < numInputMeshes; idx++ )
        {
            MDataHandle inputData = inputMeshListObj.inputValue( &status );         CHECK_STATUS("get MDataHandle on inputMeshListObj failed");
            MDataHandle outputData = outputMeshListObj.outputValue( &status );      CHECK_STATUS("get MDataHandle on outputMeshListObj failed");

            status = outputData.set(inputData.asMesh());
            status = inputMeshListObj.next();
            status = outputMeshListObj.next();
        }

        outputMeshListObj.setClean();
        outputMeshListObj.setAllClean();

    } 
    else if (mode == 2)
    {
        outputMeshListObj.setClean();
        outputMeshListObj.setAllClean();
    }
    else
    {
        if (plug == mOutMeshes)
        {
            Settings settings;
            MObjectArray outputMeshes;
            MMatrixArray matrixArray;
            MString UVSetNameStr, *UVSetNameStrPtr;
            getUVSetName(UVSetNameStr, UVSetNameStrPtr, data);      CHECK_STATUS("Failure in getUVSetName");

            UVCacheData* UVCache = NULL;
            unsigned int numCacheElements = getCacheSizeAndHandle(UVCache, data); 

            bool isLoaded = data.inputValue(mIsLoaded).asBool();
            bool isReference = depNodeFn.isFromReferencedFile();

            // [ERIK] there are two forms of UV cache supported:
            // the "old" method, which stores a full mesh  and
            // the "new" method, which stores only the UV's in the UVCacheData class
            // old cache can still be read, but only the new cache will be saved..
            bool newCacheInitialized = UVCache && (numCacheElements>0) && (numOutputMeshes == numCacheElements);
            bool oldCacheInitialized = (numCacheElementsOldMethod>0) && (numOutputMeshes == numCacheElementsOldMethod);

            bool copyFromCache= ((!isLoaded) && (newCacheInitialized || oldCacheInitialized));

            if (!copyFromCache)
            {
                initSettings(settings, data); 
                settings.UVSetName = UVSetNameStr;

                //just to be sure the device is initialized - should return if it is already initialized
                createNULLRefDevice(spD3D, spd3dDevice);
            }

            unsigned int numElements = max(numInputMeshes, numOutputMeshes);
            unsigned int cacheIndex = 0;
            for (unsigned int idx = 0; idx < numElements; idx++ )
            {        
                MPlug inMeshPlug( thisNode, mInMeshes);
                inMeshPlug = inMeshPlug.elementByLogicalIndex(idx);
                MPlug outMeshPlug( thisNode, mOutMeshes);
                outMeshPlug = outMeshPlug.elementByLogicalIndex(idx);

#ifdef MUV_VERBOSE             
                {                
                    MString message("  Index: ");   
                    message += idx;
                    MGlobal::displayInfo(message);
                }
#endif

                //[ERIK] to make the code more debug-able, pull values out to booleans.
                bool inMeshPlugIsNull = inMeshPlug.isNull();
                bool inMeshPlugIsConnected = inMeshPlug.isConnected();
                bool outMeshPlugIsNull = outMeshPlug.isNull();
                bool outMeshPlugIsConnected = outMeshPlug.isConnected();

                if ((inMeshPlugIsNull != outMeshPlugIsNull) || (inMeshPlugIsConnected != outMeshPlugIsConnected))
                {
                    MGlobal::displayWarning(MString(__FUNCTION__) + MString(": in/out mesh plug configuration suspicios!"));
                }

                if (!inMeshPlugIsNull && inMeshPlugIsConnected && !outMeshPlugIsNull && outMeshPlugIsConnected)                    
                {
                    // go to the input and output mesh
                    status = outputMeshListObj.jumpToElement(idx);                      CHECK_STATUS("Could not jump to indexed outputMesh");
                    status = inputMeshListObj.jumpToElement(idx);                       CHECK_STATUS("Could not jump to indexed inputMesh");
                    MDataHandle inputData = inputMeshListObj.inputValue( &status );     CHECK_STATUS("get MDataHandle on inputMeshListObj failed");
                    MDataHandle outputData = outputMeshListObj.outputValue( &status );  CHECK_STATUS("get MDataHandle on outputMeshListObj failed");

                    // Copy the inMesh to the outMesh, and now you can perform operations in-place on the outMesh
                    outputData.set(inputData.asMesh());

                    if (copyFromCache)
                    {
                        // just copy the cache
                        MFnMesh outputMesh(outputData.asMesh(), &status);                                           CHECK_STATUS("could not create function set for output mesh");
#ifdef MUV_VERBOSE
                        MGlobal::displayInfo("    outmesh information:");
                        MStringArray UVSetNames;
                        if (outputMesh.getUVSetNames(UVSetNames))
                        {
                            for (unsigned UVsIndex = 0; UVsIndex < UVSetNames.length(); ++UVsIndex)
                            {
                                MString message("      UVset Index: ");   
                                message += UVsIndex;
                                MGlobal::displayInfo(message);
                                MGlobal::displayInfo(MString("        ")+UVSetNames[UVsIndex]);
                            }
                        }

                        int numPolygons = outputMesh.numPolygons(&status);
                        if (status)
                        {
                            MString message("      numPolygons: ");   
                            message += numPolygons;
                            MGlobal::displayInfo(message);
                        }

                        for (int faceIndex = 0; faceIndex < numPolygons; faceIndex++)
                        {
                            MIntArray vertexList;
                            if (outputMesh.getPolygonVertices(faceIndex, vertexList))
                            {
                                if (vertexList.length() != 3)
                                {
                                    MString message("      face index# ");   
                                    message += faceIndex;
                                    message += " has not 3 vertices: ";
                                    message += vertexList.length();
                                    MGlobal::displayWarning(message);
                                }
                            }
                            else
                            {
                                MGlobal::displayError("Could not getPolygonVertices");
                            }
                        }
#endif
                        // clear the UVs so that we can assign them from the cache for sure
                        outputMesh.clearUVs(UVSetNameStrPtr);

                        bool failure = false;

                        if (newCacheInitialized)
                        {
                            UVCacheData::cacheEntry cacheEntry;
                            if (!UVCache->get(cacheIndex, cacheEntry))
                            {
                                MGlobal::displayWarning("Wrong cache index");
                            }
#ifdef MUV_VERBOSE
                            cacheEntry.debugOutput();
#endif
                            status = outputMesh.setUVs(cacheEntry.uArray, cacheEntry.vArray, UVSetNameStrPtr); CHECK_STATUS_NR("could not set UVs for output mesh (from new cache)"); ACCUMULATE_FAILURE;
                            status = outputMesh.assignUVs(cacheEntry.UVCounts, cacheEntry.UVIds, UVSetNameStrPtr); CHECK_STATUS_NR("could not assign UVs for output mesh (from new cache)"); ACCUMULATE_FAILURE;
                        }
                        else
                        {
                            status = outputMeshListObjCached.jumpToElement(idx);                        CHECK_STATUS("Could not jump to indexed outputMeshCache");
                            MDataHandle cachedData =outputMeshListObjCached.inputValue( &status );      CHECK_STATUS("get MDataHandle on outputMeshListObjCached failed");

                            MFnMesh cachedMesh(cachedData.asMesh(), &status);  CHECK_STATUS("could not create function set for cached mesh");
                            MFloatArray uArray;
                            MFloatArray vArray;
                            MIntArray UVCounts;
                            MIntArray UVIds;

                            status = cachedMesh.getUVs(uArray, vArray, UVSetNameStrPtr);  CHECK_STATUS_NR("could not getUVs from cached mesh"); ACCUMULATE_FAILURE;
                            status = cachedMesh.getAssignedUVs(UVCounts, UVIds, UVSetNameStrPtr);  CHECK_STATUS_NR("could not getAssignedUVs from cached mesh"); ACCUMULATE_FAILURE;
                            status = outputMesh.setUVs(uArray, vArray, UVSetNameStrPtr); CHECK_STATUS_NR("could not set UVs for output mesh (from old cache)"); ACCUMULATE_FAILURE;
                            status = outputMesh.assignUVs(UVCounts, UVIds, UVSetNameStrPtr); CHECK_STATUS_NR("could not assign UVs for output mesh (from old cache)"); ACCUMULATE_FAILURE;
                        }

                        if (failure)
                        {
                            // [ERIK] here we have an error case with the UV Sets - just clear the UV set 
                            status == MS::kFailure;
                            ReportError("Failed to assign the cached UVSet - no Lightmap UV set assigned!");
                            status == MS::kSuccess;
                            outputMesh.clearUVs(UVSetNameStrPtr);
                        }
                    }
                    else
                    {
                        // build data structures for calculating the UVSet
                        outputMeshes.append(outputData.asMesh());                    
                        MMatrix matrix;
                        status = computeQualityScaleMatrix(matrix, idx, data);              CHECK_STATUS("failure in computeQualityScaleMatrix");
                        matrixArray.push_back(matrix);
                    }

                    cacheIndex++;
                }
            }

            if (!copyFromCache)
            {
                MGlobal::displayInfo("  !!Re-calculating the lightmap UV set");
                //do the calculation here...
                status = processUVAtlasCreationOnMayaMesh(outputMeshes, matrixArray, spd3dDevice, settings);

                // copy over to the cache                
                if (UVCache != NULL && !isReference)
                {
                    UVCache->storeIntoCache(outputMeshes, UVSetNameStr);
                }

            }

            isLoaded = true;
            data.inputValue(mIsLoaded).set(isLoaded);

            if (isReference)
            {
                // references are switched to blocking mode, and the cache is freed
                stateData.set(2);
                if (UVCache != NULL)
                {
                    UVCache->clearCache();
                }
            }

            if (!status)
            {
                MGlobal::displayWarning("UVAtlas::compute failed");
                status = MS::kSuccess;
            }
            outputMeshListObj.setClean();
            outputMeshListObj.setAllClean();
        }
        else
        {
            status = MS::kUnknownParameter;
        }
    }

    return status;
}

#undef CHECK_STATUS

//-----------------------------------------------------------------------------
MStatus rageMayaUVAtlasNode::initialize()
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
    MFnNumericAttribute nAttr; 
	MFnTypedAttribute attrFn;
    MFnCompoundAttribute cmpAttr;
    Settings settings;


    mUiMaxCharts = nAttr.create("MaxCharts", "mc", MFnNumericData::kInt, settings.maxcharts, &status);
    nAttr.setStorable(true);
    nAttr.setMin(0);

    mUiWidth = nAttr.create("Width", "w", MFnNumericData::kInt, settings.width);
    nAttr.setStorable(true);
    nAttr.setMin(0);

    mUiHeight = nAttr.create("Height", "h", MFnNumericData::kInt, settings.height);
    nAttr.setStorable(true);
    nAttr.setMin(0);

    mUiMaxStretch = nAttr.create("MaxStretch", "ms", MFnNumericData::kFloat, settings.maxstretch);
    nAttr.setStorable(true);
    nAttr.setMin(0);
    nAttr.setMax(1);

    mUiGutter = nAttr.create("Gutter", "g", MFnNumericData::kFloat, settings.gutter);
    nAttr.setStorable(true);
    nAttr.setMin(0);

    mUiUVSetName = attrFn.create("UVSetName", "us", MFnData::kString);
    attrFn.setStorable(true);

    mInMeshes = attrFn.create("inMeshes", "im", MFnMeshData::kMesh);
    status = attrFn.setStorable(true);
    status = attrFn.setArray(true);
    status = attrFn.setUsesArrayDataBuilder(true);

    mWorldMatrices = attrFn.create("WorldMatrices", "wm", MFnData::kMatrix);
    status = attrFn.setStorable(true);
    status = attrFn.setArray(true);
    status = attrFn.setUsesArrayDataBuilder(true);

    mQuality = nAttr.create("Quality", "qy", MFnNumericData::kDouble, 1.0);
    nAttr.setStorable(true);
    nAttr.setArray(true);
    nAttr.setUsesArrayDataBuilder(true);

    // Attribute is read-only because it is an output attribute
    mOutMeshes = attrFn.create("outMeshes", "om", MFnMeshData::kMesh);
    attrFn.setStorable(true); //write out the data so that the scene doesn`t need to recreate the UVs on loading.
    attrFn.setWritable(false);
    attrFn.setArray(true);
    attrFn.setUsesArrayDataBuilder(true);

    mOutMeshesCache = attrFn.create("mOutMeshesCache", "omc", MFnMeshData::kMesh);
    status = attrFn.setStorable(true);
    status = attrFn.setWritable(true);
    status = attrFn.setReadable(false);
    status = attrFn.setHidden(true);
    status = attrFn.setArray(true);
    status = attrFn.setCached(false);
    status = attrFn.setKeyable(false);
    status = attrFn.setChannelBox(false);
    status = attrFn.setConnectable(false);
    status = attrFn.setUsesArrayDataBuilder(true);

    mIsLoaded = nAttr.create("IsLoaded", "il", MFnNumericData::kBoolean, false);
    nAttr.setStorable(false);
    nAttr.setHidden(true);

    // Add the attributes we have created to the node
    status = addAttribute( mUiMaxCharts );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiWidth );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiHeight );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiMaxStretch );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiGutter );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiUVSetName );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mInMeshes );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }

    status = addAttribute( mOutMeshes);
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }

    status = addAttribute( mOutMeshesCache);
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }

    status = addAttribute( mIsLoaded);
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }

    status = addAttribute( mWorldMatrices );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }

    status = addAttribute( mQuality);
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }

    // Set up a dependency between the input and the output.  This will cause
    // the output to be marked dirty when the input changes.  The output will
    // then be recomputed the next time the value of the output is requested.
    status = attributeAffects( mInMeshes, mOutMeshes );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }

    status = attributeAffects( mUiMaxCharts, mOutMeshes );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiWidth, mOutMeshes );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiHeight, mOutMeshes );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiMaxStretch, mOutMeshes );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiGutter, mOutMeshes );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiUVSetName, mOutMeshes );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mWorldMatrices, mOutMeshes );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mQuality, mOutMeshes );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    return status;
}

//-----------------------------------------------------------------------------
void rageMayaUVAtlasNode::postConstructor()
//-----------------------------------------------------------------------------
{   
    MStatus status;
    MFnTypedAttribute attrFn;

    mUVCache = attrFn.create("mUVCache", "UVc", UVCacheData::id, MObject::kNullObj, &status);
    if (status == MS::kSuccess)
    {
        status = attrFn.setStorable(true);
        status = attrFn.setWritable(true);
        status = attrFn.setReadable(false);
        status = attrFn.setHidden(true);
        status = attrFn.setArray(false);
        status = attrFn.setCached(false);
        status = attrFn.setKeyable(false);
    }
    else
    {
        status.perror("Could not create the UVCache attribute");
    }


    MFnDependencyNode MFnDn(thisMObject(), &status);
    if (status == MS::kSuccess)
    {
        status = MFnDn.addAttribute(mUVCache);    
    }
    if (!status)
    {
        status.perror("addAttribute for mUVCache failed"); 
    }

    MPlug plug(thisMObject(), mUVCache);

    mCacheAttribute = new UVCacheData();
    status = plug.setValue(mCacheAttribute);

}

//-----------------------------------------------------------------------------
MStatus rageMayaUVAtlasNode::shouldSave( const MPlug& plug, bool &ret)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    if (plug==mOutMeshesCache)
    {
        ret=false;
    }
    else if (plug==mIsLoaded)
    {
        ret = false;
    }
    else if (plug==mUVCache)
    {
        ret = true;
    }
    else
    {
        status = MPxNode::shouldSave( plug, ret );
    }
    return status;
}


//-----------------------------------------------------------------------------
MStatus rageMayaUVAtlasNode::setSettings(MObject modifierNode, const Settings& settings)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    MFnDependencyNode depNodeFn( modifierNode );
    MObject maxChartAttr = depNodeFn.attribute( "MaxCharts" );
    MPlug maxChartPlug( modifierNode, maxChartAttr );
    status = maxChartPlug.setValue(settings.maxcharts);
    if (status!= MS::kSuccess)
    {
        return status;
    }

    MObject widthAttr = depNodeFn.attribute( "Width" );
    MPlug widthPlug( modifierNode, widthAttr );
    status = widthPlug.setValue(settings.width);
    if (status!= MS::kSuccess)
    {
        return status;
    }

    MObject heightAttr = depNodeFn.attribute( "Height" );
    MPlug heightPlug( modifierNode, heightAttr );
    status = heightPlug.setValue(settings.height);
    if (status!= MS::kSuccess)
    {
        return status;
    }

    MObject maxStretchAttr = depNodeFn.attribute( "MaxStretch" );
    MPlug maxStretchPlug( modifierNode, maxStretchAttr );
    status = maxStretchPlug.setValue(settings.maxstretch);
    if (status!= MS::kSuccess)
    {
        return status;
    }

    MObject gutterAttr = depNodeFn.attribute( "Gutter" );
    MPlug gutterPlug( modifierNode, gutterAttr );
    status = gutterPlug.setValue(settings.gutter);
    if (status!= MS::kSuccess)
    {
        return status;
    }

    MObject UVSetNameAttr = depNodeFn.attribute( "UVSetName" );
    MPlug UVSetNamePlug( modifierNode, UVSetNameAttr );
    MString UVsetName(settings.UVSetName);
    status = UVSetNamePlug.setValue(UVsetName);

    return status;
}


} // namespace rage
