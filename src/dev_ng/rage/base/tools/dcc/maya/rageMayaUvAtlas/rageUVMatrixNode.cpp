// -----------------------------------------------------------------------------
// $Workfile: rageUVMatrixNode.cpp (\Workspace\PRJ_LH\Development\Prototypes\Tools\rageMayaUVAtlasCmd\rageMayaUVAtlasCmd.cpp) $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// -----------------------------------------------------------------------------

#include "rageUVMatrixNode.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MFnTypedAttribute.h>
#include <maya/MObject.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnStringData.h>
#include <maya/MFnMeshData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>
#pragma warning(pop)

#include "rageMayaUVAtlasMain.h"

namespace rage
{

using namespace std;

MObject	rageUVMatrixNode::mInMesh;
MObject	rageUVMatrixNode::mOutMesh;
MObject rageUVMatrixNode::mUiMatrixElement0;
MObject rageUVMatrixNode::mUiMatrixElement1;
MObject rageUVMatrixNode::mUiMatrixElement2;
MObject rageUVMatrixNode::mUiMatrixElement3;
MObject rageUVMatrixNode::mUiMatrixElement4;
MObject rageUVMatrixNode::mUiMatrixElement5;
MObject rageUVMatrixNode::mUiUVSetName;



// -----------------------------------------------------------------------------
void* rageUVMatrixNode::creator()
// -----------------------------------------------------------------------------
{
    return new rageUVMatrixNode();
}


//-----------------------------------------------------------------------------
rageUVMatrixNode::rageUVMatrixNode( )
//-----------------------------------------------------------------------------
{
}


//-----------------------------------------------------------------------------
rageUVMatrixNode::~rageUVMatrixNode( )
//-----------------------------------------------------------------------------
{    
}


//-----------------------------------------------------------------------------
MStatus rageUVMatrixNode::compute( const MPlug& plug, MDataBlock& data )
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;

    MDataHandle stateData = data.outputValue( state, &status );
    if (status!=MS::kSuccess)
    {
        return status;
    }
    
    // Check for the HasNoEffect/PassThrough flag on the node.
    //
    // (stateData is an enumeration standard in all depend nodes - stored as short)
    // 
    // (0 = Normal)
    // (1 = HasNoEffect/PassThrough)
    // (2 = Blocking)
    // ...
    if( stateData.asShort() == 1 )
    {
        MDataHandle inputData = data.inputValue( mInMesh, &status );
        if (status!=MS::kSuccess)
        {
            return status;
        }

        MDataHandle outputData = data.outputValue( mOutMesh, &status );
        if (status!=MS::kSuccess)
        {
            return status;
        }

        // Simply redirect the inMesh to the outMesh for the PassThrough effect
        //
        outputData.set(inputData.asMesh());
    }
    else
    {
        // Check which output attribute we have been asked to 
        // compute. If this node doesn't know how to compute it, 
        // we must return MS::kUnknownParameter
        if (plug == mOutMesh)
        {
            MDataHandle inputMesh = data.inputValue( mInMesh, &status );
            if (status!=MS::kSuccess)
            {
                return status;
            }

            MDataHandle outputData = data.outputValue( mOutMesh, &status );
            if (status!=MS::kSuccess)
            {
                return status;
            }            

            // Copy the inMesh to the outMesh, and now you can
            // perform operations in-place on the outMesh
            outputData.set(inputMesh.asMesh());

            double matrix[6];
            MString* pUVSet=NULL;
            matrix[0]=data.inputValue(mUiMatrixElement0).asFloat();
            matrix[1]=data.inputValue(mUiMatrixElement1).asFloat();
            matrix[2]=data.inputValue(mUiMatrixElement2).asFloat();
            matrix[3]=data.inputValue(mUiMatrixElement3).asFloat();
            matrix[4]=data.inputValue(mUiMatrixElement4).asFloat();
            matrix[5]=data.inputValue(mUiMatrixElement5).asFloat();            
            MDataHandle UVSetNameHandle = data.inputValue(mUiUVSetName);
            MFnStringData UVSetNameData(UVSetNameHandle.data());
            MString UVSetName = UVSetNameData.string();
            if (UVSetName.length()>0)
            {
                pUVSet = &UVSetName;
            }
			MObject obMeshAsMObject = outputData.asMesh();
            status = processMatrixOnUVSet(obMeshAsMObject, matrix, pUVSet);
        }
        else
        {
            status = MS::kUnknownParameter;
        }        
    }

    return status;
}


//-----------------------------------------------------------------------------
MStatus rageUVMatrixNode::initialize()
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
    MFnNumericAttribute nAttr; 
	MFnTypedAttribute attrFn;

    mUiMatrixElement0 = nAttr.create("mUiMatrixElement0", "m0", MFnNumericData::kFloat, 1.0f);
    nAttr.setStorable(true);
    mUiMatrixElement1 = nAttr.create("mUiMatrixElement1", "m1", MFnNumericData::kFloat, 0.0f);
    nAttr.setStorable(true);
    mUiMatrixElement2 = nAttr.create("mUiMatrixElement2", "m2", MFnNumericData::kFloat, 0.0f);
    nAttr.setStorable(true);
    mUiMatrixElement3 = nAttr.create("mUiMatrixElement3", "m3", MFnNumericData::kFloat, 1.0f);
    nAttr.setStorable(true);
    mUiMatrixElement4 = nAttr.create("mUiMatrixElement4", "m4", MFnNumericData::kFloat, 0.0f);
    nAttr.setStorable(true);
    mUiMatrixElement5 = nAttr.create("mUiMatrixElement5", "m5", MFnNumericData::kFloat, 0.0f);
    nAttr.setStorable(true);

    mUiUVSetName = attrFn.create("UVSetName", "us", MFnData::kString);
    attrFn.setStorable(true);

    mInMesh = attrFn.create("inMesh", "im", MFnMeshData::kMesh);
    attrFn.setStorable(true);

    // Attribute is read-only because it is an output attribute
    mOutMesh = attrFn.create("outMesh", "om", MFnMeshData::kMesh);
    attrFn.setStorable(false);
    attrFn.setWritable(false);

    
    // Add the attributes we have created to the node
    status = addAttribute( mUiMatrixElement0 );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiMatrixElement1 );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiMatrixElement2 );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiMatrixElement3 );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiMatrixElement4 );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiMatrixElement5 );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mUiUVSetName );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mInMesh );
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }
    status = addAttribute( mOutMesh);
    if (!status)
    {
        status.perror("addAttribute");
        return status;
    }


    // Set up a dependency between the input and the output.  This will cause
    // the output to be marked dirty when the input changes.  The output will
    // then be recomputed the next time the value of the output is requested.
    status = attributeAffects( mInMesh, mOutMesh );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }

    status = attributeAffects( mUiMatrixElement0, mOutMesh );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiMatrixElement1, mOutMesh );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiMatrixElement2, mOutMesh );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiMatrixElement3, mOutMesh );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiMatrixElement4, mOutMesh );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiMatrixElement5, mOutMesh );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }
    status = attributeAffects( mUiUVSetName, mOutMesh );
    if (!status)
    {
        status.perror("attributeAffects");
        return status;
    }

    return status;
}


//-----------------------------------------------------------------------------
MStatus rageUVMatrixNode::setSettings(MObject modifierNode, 
                                  const double* matrix, 
                                  MString UVSetName)
//-----------------------------------------------------------------------------
{
    MStatus status = MS::kSuccess;
    MFnDependencyNode depNodeFn( modifierNode );

    MObject matrixElement0Attr = depNodeFn.attribute( "mUiMatrixElement0" );
    MPlug matrixElement0Plug( modifierNode, matrixElement0Attr );
    status = matrixElement0Plug.setValue(matrix[0]);
    if (status!= MS::kSuccess)
    {
        return status;
    }
    MObject matrixElement1Attr = depNodeFn.attribute( "mUiMatrixElement1" );
    MPlug matrixElement1Plug( modifierNode, matrixElement1Attr );
    status = matrixElement1Plug.setValue(matrix[1]);
    if (status!= MS::kSuccess)
    {
        return status;
    }
    MObject matrixElement2Attr = depNodeFn.attribute( "mUiMatrixElement2" );
    MPlug matrixElement2Plug( modifierNode, matrixElement2Attr );
    status = matrixElement2Plug.setValue(matrix[2]);
    if (status!= MS::kSuccess)
    {
        return status;
    }
    MObject matrixElement3Attr = depNodeFn.attribute( "mUiMatrixElement3" );
    MPlug matrixElement3Plug( modifierNode, matrixElement3Attr );
    status = matrixElement3Plug.setValue(matrix[3]);
    if (status!= MS::kSuccess)
    {
        return status;
    }
    MObject matrixElement4Attr = depNodeFn.attribute( "mUiMatrixElement4" );
    MPlug matrixElement4Plug( modifierNode, matrixElement4Attr );
    status = matrixElement4Plug.setValue(matrix[4]);
    if (status!= MS::kSuccess)
    {
        return status;
    }
    MObject matrixElement5Attr = depNodeFn.attribute( "mUiMatrixElement5" );
    MPlug matrixElement5Plug( modifierNode, matrixElement5Attr );
    status = matrixElement5Plug.setValue(matrix[5]);
    if (status!= MS::kSuccess)
    {
        return status;
    }

    MObject UVSetNameAttr = depNodeFn.attribute( "UVSetName" );
    MPlug UVSetNamePlug( modifierNode, UVSetNameAttr );
    status = UVSetNamePlug.setValue(UVSetName);

    return status;
}


} // namespace rage
