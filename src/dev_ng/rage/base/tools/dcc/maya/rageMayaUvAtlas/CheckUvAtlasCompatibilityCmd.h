// -----------------------------------------------------------------------------
// $Workfile: CheckUVAtlasCompatibilityCmd.h  $
//   Creator: Erik Pojar
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This command returns false if UV atlas is unable to create a UV set for a mesh
// -----------------------------------------------------------------------------

#ifndef RSV_CHECK_UV_ATLAS_COMPATIBILITY_CMD_H
#define RSV_CHECK_UV_ATLAS_COMPATIBILITY_CMD_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#pragma warning(pop)

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class CheckUVAtlasCompatibilityCmd : public MPxCommand
// -----------------------------------------------------------------------------
{
public:
    CheckUVAtlasCompatibilityCmd();
    virtual
    ~CheckUVAtlasCompatibilityCmd();

    MStatus doIt(const MArgList& args);

    static
    void* creator();

};

} // namespace rage


#endif // RSV_CHECK_UV_ATLAS_COMPATIBILITY_CMD_H
