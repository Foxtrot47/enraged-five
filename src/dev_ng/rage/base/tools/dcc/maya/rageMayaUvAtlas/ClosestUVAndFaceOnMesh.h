// -----------------------------------------------------------------------------
// $Workfile: ClosestUVAndFaceOnMesh.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This command converts a given path to the old 8.3 style path
// -----------------------------------------------------------------------------

#ifndef RSV_CLOSEST_UV_AND_FACE_ON_MESH_H
#define RSV_CLOSEST_UV_AND_FACE_ON_MESH_H


#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#include <maya/MArgList.h>
#pragma warning(pop)

namespace rage
{

struct Settings;


// -----------------------------------------------------------------------------
class ClosestUVAndFaceOnMesh : public MPxCommand
// -----------------------------------------------------------------------------
{
public:
    ClosestUVAndFaceOnMesh();
    virtual
    ~ClosestUVAndFaceOnMesh();

    MStatus doIt(const MArgList& args);

    static
    void* creator();

};

} // namespace rage


#endif // RSV_CLOSEST_UV_AND_FACE_ON_MESH_H
