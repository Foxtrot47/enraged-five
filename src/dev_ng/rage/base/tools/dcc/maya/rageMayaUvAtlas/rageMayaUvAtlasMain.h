// -----------------------------------------------------------------------------
// $Workfile: rageMayaUVAtlasMain.h  $
//   Creator: Clemens Pecinovsky
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
// This plugin uses the directx atlas UV creation libraries
// -----------------------------------------------------------------------------

#ifndef RSV_MAYA_UVATLAS_H
#define RSV_MAYA_UVATLAS_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MString.h>
#include <maya/MObject.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MStatus.h>

#include <d3dx9mesh.h>
#include <WinDef.h>
#pragma warning(pop)


#include <string>
#include <vector>

#define ID_UVATLAS_NODE 0x7018
#define ID_UVMATRIX_NODE 0x7019

typedef std::vector<MMatrix> MMatrixArray;

// [erik] define this if you want verbose debug output for the maya UV atlas node
//#define MUV_VERBOSE

namespace rage
{


struct Settings
{
    int maxcharts, width, height;
    float maxstretch, gutter;
    MString UVSetName;

    Settings()
    {
        maxcharts = 0;
        maxstretch = 1.0f / 6.0f;
        gutter = 2.0f;
        width = 512;
        height = 512;
    }
};

void error(const MString& message, unsigned line, MStatus& status);

#define ReportError(message) error(MString(__FUNCTION__)+ MString(": ") + MString(message) + MString(" (") + MString(__FILE__) + MString (" Line: ") , __LINE__, status)
//#define ReportError(message) error((message), status)

MStatus initializePlugin( MObject obj );
MStatus uninitializePlugin( MObject obj );

bool createNULLRefDevice(LPDIRECT3D9 &spD3D, LPDIRECT3DDEVICE9& spd3dDevice);
void releaseNULLRefDevice(LPDIRECT3D9 &spD3D, LPDIRECT3DDEVICE9& spd3dDevice);

MStatus createD3DxMeshFromMaya(MObjectArray& meshNodes, 
                               MMatrixArray& matrices,
                               const LPDIRECT3DDEVICE9& spd3dDevice,
                               LPD3DXMESH *ppMesh);

MStatus mergeBackD3DxMeshIntoMaya(MObjectArray& meshNodes, 
                               LPD3DXMESH pMesh, 
                               MString *UVSet);

MStatus checkD3DxMeshForConsistency(LPD3DXMESH pMesh);

MStatus CheckMeshValidation(LPD3DXMESH pMesh, 
                            LPD3DXMESH *ppMeshOut, 
                            DWORD **ppAdjacency);

MStatus processUVAtlasCreation(LPD3DXMESH pMesh, 
                               LPD3DXMESH *ppMeshOut, 
                               Settings &settings);


MStatus processUVAtlasCreationOnMayaMesh(MObjectArray& meshes, 
                                         MMatrixArray& matrices, 
                                         LPDIRECT3DDEVICE9& spd3dDevice, 
                                         Settings &settings);

MStatus processMatrixOnUVSet(MObject& meshNode, 
                             double* matrix,
                             MString* UVSet);


} // namespace rage


#endif // RSV_MAYA_UVATLAS_H
