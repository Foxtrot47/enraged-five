// -----------------------------------------------------------------------------
// $Workfile: rageUVMatrixCmd.h  $
//   Creator: Erik Pojar
// Copyright: 2005 Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
// -----------------------------------------------------------------------------
// Description: 
//   Custom Data attribute to hold the cached result of the UV atlas calculation
// -----------------------------------------------------------------------------

#ifndef RSV_UVCACHE_DATA_H
#define RSV_UVCACHE_DATA_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MFloatArray.h>
#include <maya/MIntArray.h>
#include <maya/MObjectArray.h>
#pragma warning(pop)

// #include <sstream>
#include <vector>
using namespace std;

namespace rage
{

class UVCacheData : public MPxData
{
public:
    struct cacheEntry
    {
        MFloatArray uArray;
        MFloatArray vArray;
        MIntArray UVCounts;
        MIntArray UVIds;

        MStatus writeBinary(ostream& out);
        MStatus readBinary(istream& out);
        void debugOutput() const;
    };

    typedef std::vector<cacheEntry> cacheT;

    UVCacheData();
    virtual ~UVCacheData();

    //
    // Override methods in MPxData.
    virtual MStatus         readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus         readBinary( istream& in, unsigned length );
    virtual MStatus         writeASCII( ostream& out );
    virtual MStatus         writeBinary( ostream& out );

    //
    // Custom methods.
    virtual void			copy( const MPxData& ); 
    MTypeId                 typeId() const; 
    MString					name() const;

    void clearCache ();
    bool get (unsigned int index, cacheEntry&);
    unsigned size() {return mCache.size();}
    void debugOutput() const;
    MStatus storeIntoCache (MObjectArray& arrayData, MString& UVSetName);    

    //
    // static methods and data.
    static const MString    typeName;
    static const MTypeId    id;
    static void*            creator();
private:    
    cacheT mCache;
};

} // namespace rage


#endif // RSV_UVCACHE_DATA_H
