// polySurfaceShape1277694268
// Copyright (C) 2000 Kevin's Plugin Factory 
// 
// File: rageOutputWindowCmd.cpp
//
// MEL Command: rageOutputWindow
//
// Author: Maya SDK Wizard
//
// Includes everything needed to register a simple MEL command with Maya.
// male_medium_shared_low_peek_right_start_shotgun_weapon_m16_materialInfo1
#include <Afxwin.h> 
#include <maya/MSimple.h>
#include <maya/MGlobal.h>
#include <iostream>
using namespace std;

#define MAX_LINE_LENGTH	512

// Use helper macro to register a command with Maya.  It creates and
// registers a command that does not support undo or redo.  The 
// created class derives off of MPxCommand.
DeclareSimpleCommand( rageOutputWindow, "Kevin's Plugin Factory", "6.0");

void OutputUsageInfo()
{
	MGlobal::displayInfo("Error: Usage : rageOutputWindow [-clear] [-grab]");
}

MStatus rageOutputWindow::doIt( const MArgList& args )
{
	if(args.length() == 0)
	{
		OutputUsageInfo();
		return MS::kFailure;
	}

	MStatus stat = MS::kSuccess;

	// Get the output window
	HWND wh;
	CWnd theWindow, *h ;
	CEdit editCtrl;

	if((wh=FindWindow(  NULL, "Output Window")) ==NULL) 
	{
		// Hmmm, unable to find the Output Window, just try dumping to it incase it has been closed
		cout<<"Forcing the Output Window to display"<<endl;
		if((wh=FindWindow(  NULL, "Output Window")) ==NULL) 
		{
			// That didn't help, bail
			cout<<"Unable to find output window"<<endl;
			return MS::kFailure;
		}
	}

	if(!theWindow.Attach(wh))
	{
		cout<<"Unable to attach to output window"<<endl;
		return MS::kFailure;
	}

	if((h=theWindow.GetTopWindow( ))!=NULL) 
	{
		editCtrl.Attach(h->GetSafeHwnd());

		// Ok, got output window, so parse args
		for(unsigned i=0; i<args.length(); i++)
		{
			if(args.asString(i) == "-clear")
			{
				// Clear the output window
				editCtrl.SetSel(0,-1); 
				editCtrl.ReplaceSel("");
			}
			else if(args.asString(i) == "-grab")
			{
				// Print the output window's contents
				MStringArray obAStrOutputWindowsText;
				char acBuffer[MAX_LINE_LENGTH];
				for(int j=0; j<editCtrl.GetLineCount(); j++)
				{
					acBuffer[editCtrl.GetLine(j, acBuffer, MAX_LINE_LENGTH)] = '\0';
					obAStrOutputWindowsText.append(acBuffer);
				}
				setResult( obAStrOutputWindowsText );
			}
			else
			{
				// I've been given rubbish, bail
				OutputUsageInfo();
				theWindow.Detach();
				editCtrl.Detach();
				return MS::kFailure;
			}
		}
	}
	else 
	{
		return MS::kFailure;
	}

	theWindow.Detach();
	editCtrl.Detach();

	return stat;
}
