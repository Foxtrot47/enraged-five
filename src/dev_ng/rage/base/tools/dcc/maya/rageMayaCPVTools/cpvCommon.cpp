// 
// /cpvHeightGradient.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include "maya/MGlobal.h"
#include "maya/MObject.h"
#include "maya/MArgDatabase.h"
#include "maya/MStringArray.h"
#include "maya/MFnDagNode.h"
#include "maya/MFnMesh.h"
#include "maya/MFloatPointArray.h"
#include "maya/MSelectionList.h"
#include "maya/MItSelectionList.h"
#include "maya/MDagPath.h"
#include "maya/MItMeshPolygon.h"
#include "maya/MPointArray.h"
#include "maya/MArgList.h"
#include "maya/MFnTransform.h"
#include "maya/MObjectArray.h"
#include "maya/MFnComponentListData.h"
#include "maya/MFnDoubleIndexedComponent.h"
#include "maya/MFnSingleIndexedComponent.h"
#include "maya/MItMeshVertex.h"
#include "maya/MItMeshFaceVertex.h"
#include "maya/MColorArray.h"
#include "maya/MColor.h"

#pragma warning(pop)

#include <algorithm>

#include "float.h"
#include "math.h"

#include "cpvCommon.h"

using namespace std;
using namespace rage;

//-----------------------------------------------------------------------------

SceneCpvInfo::SceneCpvInfo()
	: m_maxValuePerChn(-FLT_MAX,-FLT_MAX,-FLT_MAX,-FLT_MAX)
	, m_minValuePerChn(FLT_MAX,FLT_MAX,FLT_MAX,FLT_MAX)
	, m_maxHsvValue(-FLT_MAX)
	, m_minHsvValue(FLT_MAX)
{
	m_bbMax[0] = m_bbMax[1] = m_bbMax[2] = -FLT_MAX;
	m_bbMin[0] = m_bbMin[1] = m_bbMin[2] = FLT_MAX;
}

//-----------------------------------------------------------------------------

SceneCpvInfo::~SceneCpvInfo()
{
	for(VERTINFO_ARRAY::iterator viIt = m_vertInfoArr.begin(); viIt != m_vertInfoArr.end(); ++viIt)
	{
		delete (*viIt);
	}
}

//-----------------------------------------------------------------------------

MStatus SceneCpvInfo::InitializeFromSelection()
{
	MStatus status = MS::kSuccess;
	MObject multiVertexComponent, singleVertexComponent;

	m_vertInfoArr.reserve(32);

	m_bbMax[0] = m_bbMax[1] = m_bbMax[2] = -FLT_MAX;
	m_bbMin[0] = m_bbMin[1] = m_bbMin[2] = FLT_MAX;

	//Get the current selection
	MSelectionList activeSelList;
	MGlobal::getActiveSelectionList(activeSelList);
	if(activeSelList.isEmpty())
	{
		MGlobal::displayError("No vertices selected to apply color gradient to.");
		return MS::kFailure;
	}
	
	//Construct the appropriate selection list to apply the operation to. 
	int nActiveSelItems = activeSelList.length();
	MSelectionList operationSelList;
	for(int selIdx=0; selIdx < nActiveSelItems; selIdx++)
	{
		MDagPath tmpDp;
		MObject  tmpMObj;
		status = activeSelList.getDagPath(selIdx, tmpDp, tmpMObj);
		if(!tmpMObj.isNull())
		{
			MFn::Type tmpMObjType = tmpMObj.apiType();
			if(tmpMObjType == MFn::kMeshPolygonComponent)
			{
				//Polygons are selected to convert the polygon component selection to a face vertex selection
				MIntArray polyIndexArray;
				MIntArray faceVertexIndexArray;

				MItMeshPolygon tmpPolyIter(tmpDp, tmpMObj);
				if(status == MS::kSuccess)
				{
					for(tmpPolyIter.reset(); !tmpPolyIter.isDone(); tmpPolyIter.next())
					{
						MIntArray polyVertIndices;
						tmpPolyIter.getVertices(polyVertIndices);
						for(unsigned int i=0; i<polyVertIndices.length(); i++)
						{
							polyIndexArray.append(tmpPolyIter.index());
							faceVertexIndexArray.append(polyVertIndices[i]);
						}
					}

					MFnDoubleIndexedComponent tmpDiCmp;
					tmpDiCmp.create(MFn::kMeshVtxFaceComponent, &status);
					status = tmpDiCmp.addElements(faceVertexIndexArray,polyIndexArray);
					operationSelList.add(tmpDp, tmpDiCmp.object());
				}
			}
			else
			{
				//If the component object type is vertices, or face vertices it will be handled
				//correctly during the vertex info collection process below
				operationSelList.add(tmpDp, tmpMObj);
			}
		}
		else
		{
			//An entire object is selected, so convert the object selection to the selection set
			//of all the vertices of the object
			MFnSingleIndexedComponent tmpSiCmp;
			tmpSiCmp.create(MFn::kMeshVertComponent, &status);
			MItMeshVertex tmpVtxIter(tmpDp.node(), &status);
			if(status == MS::kSuccess)
			{
				for(tmpVtxIter.reset(); !tmpVtxIter.isDone(); tmpVtxIter.next())
					tmpSiCmp.addElement(tmpVtxIter.index());

				operationSelList.add(tmpDp, tmpSiCmp.object());
			}
		}
	}
	
	//Gather information from meshes that have -Vertex Face- components selected
	MItSelectionList faceVertexComponentIter(operationSelList, MFn::kMeshVtxFaceComponent, &status);
	for (faceVertexComponentIter.reset(); !faceVertexComponentIter.isDone(); faceVertexComponentIter.next())
	{
		MDagPath meshDagPath;
		faceVertexComponentIter.getDagPath(meshDagPath, multiVertexComponent);
		
		//Check if we already have a node for this object in the stored list, if not create a new one
		meshVertexInfo* pInfoNode = NULL;
		VERTINFO_ARRAY::iterator findIt = find(m_vertInfoArr.begin(), m_vertInfoArr.end(), meshDagPath);
		if(findIt != m_vertInfoArr.end())
			pInfoNode = (*findIt);
		else
		{
			pInfoNode = new meshVertexInfo(meshDagPath);
			m_vertInfoArr.push_back(pInfoNode);
		}
		
		pInfoNode->m_meshBbMax[0] = pInfoNode->m_meshBbMax[1] = pInfoNode->m_meshBbMax[2] = -FLT_MAX;
		pInfoNode->m_meshBbMin[0] = pInfoNode->m_meshBbMin[1] = pInfoNode->m_meshBbMin[2] = FLT_MAX;

		if (!multiVertexComponent.isNull())
		{
			MFn::Type dbgType = multiVertexComponent.apiType();

			MFnMesh fnMesh(meshDagPath);

			for (MItMeshFaceVertex faceVertexIter(meshDagPath, multiVertexComponent); !faceVertexIter.isDone(); faceVertexIter.next())
			{
				//Store the vertex index
				pInfoNode->vertindices.append( faceVertexIter.vertId() );

				//Store the position
				MPoint pos = faceVertexIter.position(MSpace::kWorld);
				pInfoNode->vertpositions.append( pos );

				//Determine the AABB values of -all- the selected verts (across objects, and locally) while we are pulling out this information
				if(pos.x > m_bbMax[0]) m_bbMax[0] = (float)pos.x;
				if(pos.y > m_bbMax[1]) m_bbMax[1] = (float)pos.y;
				if(pos.z > m_bbMax[2]) m_bbMax[2] = (float)pos.z;

				if(pos.x < m_bbMin[0]) m_bbMin[0] = (float)pos.x;
				if(pos.y < m_bbMin[1]) m_bbMin[1] = (float)pos.y;
				if(pos.z < m_bbMin[2]) m_bbMin[2] = (float)pos.z;

				if(pos.x > pInfoNode->m_meshBbMax[0]) pInfoNode->m_meshBbMax[0] = (float)pos.x;
				if(pos.y > pInfoNode->m_meshBbMax[1]) pInfoNode->m_meshBbMax[1] = (float)pos.y;
				if(pos.z > pInfoNode->m_meshBbMax[2]) pInfoNode->m_meshBbMax[2] = (float)pos.z;

				if(pos.x < pInfoNode->m_meshBbMin[0]) pInfoNode->m_meshBbMin[0] = (float)pos.x;
				if(pos.y < pInfoNode->m_meshBbMin[1]) pInfoNode->m_meshBbMin[1] = (float)pos.y;
				if(pos.z < pInfoNode->m_meshBbMin[2]) pInfoNode->m_meshBbMin[2] = (float)pos.z;


				//Store the vertex color data associated with the vertex
				meshVertexColorInfo* pColorInfo = new meshVertexColorInfo();
				
				int colorIndex;
				faceVertexIter.getColorIndex( colorIndex );
				pColorInfo->colorIndicies.append( colorIndex );

				MColor faceVertColor;
				faceVertexIter.getColor( faceVertColor );
				pColorInfo->colorValues.append( faceVertColor );

				//Store the min/max color values per channel values
				if(faceVertColor.r > m_maxValuePerChn.r)
					m_maxValuePerChn.r = faceVertColor.r;
				if(faceVertColor.r < m_minValuePerChn.r)
					m_minValuePerChn.r = faceVertColor.r;

				if(faceVertColor.g > m_maxValuePerChn.g)
					m_maxValuePerChn.g = faceVertColor.g;
				if(faceVertColor.g < m_minValuePerChn.g)
					m_minValuePerChn.g = faceVertColor.g;

				if(faceVertColor.b > m_maxValuePerChn.b)
					m_maxValuePerChn.b = faceVertColor.b;
				if(faceVertColor.b < m_minValuePerChn.b)
					m_minValuePerChn.b = faceVertColor.b;

				//Store the min/max HSV value values
				float h,s,v;
				faceVertColor.get(MColor::kHSV, h, s, v);
				if(v > m_maxHsvValue)
					m_maxHsvValue = v;
				if(v < m_minHsvValue)
					m_minHsvValue = v;
			
				pInfoNode->vertColors.push_back(pColorInfo);
			}
		}
	}

	//Gather information from meshes that have -Vertex- components selected
	for (MItSelectionList vertexComponentIter(operationSelList, MFn::kMeshVertComponent); !vertexComponentIter.isDone(); vertexComponentIter.next())
	{
		MDagPath meshDagPath;
		vertexComponentIter.getDagPath(meshDagPath, multiVertexComponent);
		
		//Check if we already have a node for this object in the stored list, if not create a new one
		meshVertexInfo* pInfoNode = NULL;
		VERTINFO_ARRAY::iterator findIt = find(m_vertInfoArr.begin(), m_vertInfoArr.end(), meshDagPath);
		if(findIt != m_vertInfoArr.end())
			pInfoNode = (*findIt);
		else
		{
			pInfoNode = new meshVertexInfo(meshDagPath);
			m_vertInfoArr.push_back(pInfoNode);
		}
		
		pInfoNode->m_meshBbMax[0] = pInfoNode->m_meshBbMax[1] = pInfoNode->m_meshBbMax[2] = -FLT_MAX;
		pInfoNode->m_meshBbMin[0] = pInfoNode->m_meshBbMin[1] = pInfoNode->m_meshBbMin[2] = FLT_MAX;

		if (!multiVertexComponent.isNull())
		{
			MFnMesh fnMesh(meshDagPath);

			for (MItMeshVertex vertexIter(meshDagPath, multiVertexComponent); !vertexIter.isDone(); vertexIter.next())
			{
				//Store the vertex index
				pInfoNode->vertindices.append( vertexIter.index() );

				//Store the position
				MPoint pos = vertexIter.position(MSpace::kWorld);
				pInfoNode->vertpositions.append( pos );

				//Determine the AABB values of -all- the selected verts (across objects, and locally) while we are pulling out this information
				if(pos.x > m_bbMax[0]) m_bbMax[0] = (float)pos.x;
				if(pos.y > m_bbMax[1]) m_bbMax[1] = (float)pos.y;
				if(pos.z > m_bbMax[2]) m_bbMax[2] = (float)pos.z;

				if(pos.x < m_bbMin[0]) m_bbMin[0] = (float)pos.x;
				if(pos.y < m_bbMin[1]) m_bbMin[1] = (float)pos.y;
				if(pos.z < m_bbMin[2]) m_bbMin[2] = (float)pos.z;

				if(pos.x > pInfoNode->m_meshBbMax[0]) pInfoNode->m_meshBbMax[0] = (float)pos.x;
				if(pos.y > pInfoNode->m_meshBbMax[1]) pInfoNode->m_meshBbMax[1] = (float)pos.y;
				if(pos.z > pInfoNode->m_meshBbMax[2]) pInfoNode->m_meshBbMax[2] = (float)pos.z;

				if(pos.x < pInfoNode->m_meshBbMin[0]) pInfoNode->m_meshBbMin[0] = (float)pos.x;
				if(pos.y < pInfoNode->m_meshBbMin[1]) pInfoNode->m_meshBbMin[1] = (float)pos.y;
				if(pos.z < pInfoNode->m_meshBbMin[2]) pInfoNode->m_meshBbMin[2] = (float)pos.z;


				//Store the vertex color data associated with the vertex
				meshVertexColorInfo* pColorInfo = new meshVertexColorInfo();
				
				vertexIter.getColorIndices( pColorInfo->colorIndicies );
				vertexIter.getColors( pColorInfo->colorValues );

				int nColors = pColorInfo->colorValues.length();
				for(int clrIdx=0; clrIdx<nColors; clrIdx++)
				{					
					const MColor& rgbColor = pColorInfo->colorValues[clrIdx];
					
					//Store the min/max color values per channel values
					if(rgbColor.r > m_maxValuePerChn.r)
						m_maxValuePerChn.r = rgbColor.r;
					if(rgbColor.r < m_minValuePerChn.r)
						m_minValuePerChn.r = rgbColor.r;

					if(rgbColor.g > m_maxValuePerChn.g)
						m_maxValuePerChn.g = rgbColor.g;
					if(rgbColor.g < m_minValuePerChn.g)
						m_minValuePerChn.g = rgbColor.g;

					if(rgbColor.b > m_maxValuePerChn.b)
						m_maxValuePerChn.b = rgbColor.b;
					if(rgbColor.b < m_minValuePerChn.b)
						m_minValuePerChn.b = rgbColor.b;

					//Store the min/max HSV value values
					float h,s,v;
					rgbColor.get(MColor::kHSV, h, s, v);
					if(v > m_maxHsvValue)
						m_maxHsvValue = v;
					if(v < m_minHsvValue)
						m_minHsvValue = v;
				}
							
				pInfoNode->vertColors.push_back(pColorInfo);
			}
		}
	}//End for (MItSelectionList...

	return status;
}

//-----------------------------------------------------------------------------

void GradientEval::AddPoint(const MColor &color, float position)
{
	m_points.push_back( GradientPoint() );
	GradientPoint& newPoint = m_points.back();
	newPoint.m_color = color;
	newPoint.m_position = position;
	sort(m_points.begin(), m_points.end());
}

//-----------------------------------------------------------------------------

MColor GradientEval::EvaluateAt(float t)
{
	int AIdx = 0;
	int BIdx = 0;

	if(!m_points.size())
		return MColor(0.0f,0.0f,0.0f,1.0f);

	for(vector<GradientPoint>::iterator it = m_points.begin(); it != m_points.end(); ++it)
	{	
		GradientPoint& point = (*it);
		if( point.m_position == t )
			return (point.m_color);
		else if( point.m_position > t )
		{
			AIdx = (BIdx - 1);
			break;
		}
		BIdx++;
	}

	if(BIdx >= (int)m_points.size())
	{
		//If the tVal is greater than the last gradent point position, the color is simply
		//the color of the last gradient point
		return m_points[m_points.size()-1].m_color;
	}
	else if(AIdx < 0)
	{
		//If the tVal is less than the first gradient point position, the color is simply the 
		//color of the first gradient point
		return m_points[0].m_color;
	}
	else
	{
		MColor retColor;
	
		float localTVal;
		if(m_falloff == 0.0)
			localTVal = 0.0f;
		else
			localTVal = pow((t - m_points[AIdx].m_position) / (m_points[BIdx].m_position - m_points[AIdx].m_position), m_falloff);

		retColor.r = m_points[AIdx].m_color.r + (localTVal * (m_points[BIdx].m_color.r - m_points[AIdx].m_color.r));
		retColor.g = m_points[AIdx].m_color.g + (localTVal * (m_points[BIdx].m_color.g - m_points[AIdx].m_color.g));
		retColor.b = m_points[AIdx].m_color.b + (localTVal * (m_points[BIdx].m_color.b - m_points[AIdx].m_color.b));

		return retColor;
	}
}

//-----------------------------------------------------------------------------

