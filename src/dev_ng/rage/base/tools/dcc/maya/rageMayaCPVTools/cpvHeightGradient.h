// 
// /cpvHeightGradient.h
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __CPV_HEIGHT_GRADIENT_H__
#define __CPV_HEIGHT_GRADIENT_H__

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MPxCommand.h>
#include <maya/MColor.h>
#include <maya/MVector.h>
#pragma warning(pop)

#include "cpvCommon.h"

//-----------------------------------------------------------------------------

namespace rage
{
	class cpvHeightGradientCmd : public MPxCommand
	{
	public:
		enum GradientAxis
		{
			GRAD_X = 0,
			GRAD_Y = 1,
			GRAD_Z = 2,
		};

		enum ApplicationMode
		{
			REPLACE = 0,
			ADDITIVE,
			SUBTRACTIVE
		};

	public:
		cpvHeightGradientCmd();
		virtual ~cpvHeightGradientCmd();

		static	void*	creator() { return new cpvHeightGradientCmd(); }
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual MStatus redoIt();
		virtual MStatus undoIt();

		virtual bool	isUndoable() const { return true; }

	public:
		MStatus	parseArguments( const MArgList& args );

	private:
		MColor			m_startColor;
		MColor			m_endColor;
		
		GradientAxis	m_axis;
		
		float			m_fallOff;
		
		bool			m_objectBBoxRelative;
		
		bool			m_pointRelative;
		float			m_pointRelMin;
		float			m_pointRelMax;

		ApplicationMode	m_applicationMode;

		GradientEval	m_gradientEvaluator;

		CPVADJUST_ARRAY	m_adjustments;
	};

}//End namespace rage

//-----------------------------------------------------------------------------

#endif //__CPV_HEIGHT_GRADIENT_H__

