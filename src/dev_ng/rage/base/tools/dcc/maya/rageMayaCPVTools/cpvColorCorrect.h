// 
// /cpvColorCorrect.h
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __CPV_COLOR_CORRECT_H__
#define __CPV_COLOR_CORRECT_H__

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MPxCommand.h>
#include <maya/MColor.h>
#include <maya/MVector.h>
#include <maya/MDagPath.h>

#pragma warning(pop)

#include <vector>

#include "cpvCommon.h"

//-----------------------------------------------------------------------------

namespace rage
{
	//-------------------------------------------------------------------------
	
	class IColorOperator
	{
	public:
		virtual MStatus DoOperation(MColor& oldColor, MColor& newColor) const = 0;
	};
	
	//-------------------------------------------------------------------------
	
	class TintOperator : public IColorOperator
	{
	public:
		TintOperator()
			: m_tintValue(0)
			, m_colorize(false)
		{};

		virtual MStatus DoOperation(MColor& oldColor, MColor& newColor) const;
		float	m_tintValue;
		bool	m_colorize;
	};

	//-------------------------------------------------------------------------
	
	class LightenOperator : public IColorOperator
	{
	public:
		LightenOperator()
			: m_amount(0)
		{};

		virtual MStatus DoOperation(MColor& oldColor, MColor& newColor) const;
		float	m_amount;
	};

	//-------------------------------------------------------------------------
	
	class DarkenOperator : public IColorOperator
	{
	public:
		DarkenOperator()
			: m_amount(0)
		{};

		virtual MStatus DoOperation(MColor& oldColor, MColor& newColor) const;
		float	m_amount;
	};

	//-------------------------------------------------------------------------

	class ScaleUpOperator : public IColorOperator
	{
	public:
		ScaleUpOperator()
			: m_amount(0)
		{};
		virtual MStatus DoOperation(MColor& oldColor, MColor& newColor) const;
		float	m_amount;
	};

	//-------------------------------------------------------------------------

	class ScaleDownOperator : public IColorOperator
	{
	public:
		ScaleDownOperator()
			: m_amount(0)
		{};

		virtual MStatus DoOperation(MColor& oldColor, MColor& newColor) const;
		float	m_amount;
	};

	//-------------------------------------------------------------------------
	
	class cpvColorCorrectCmd : public MPxCommand
	{
	public:
		cpvColorCorrectCmd();
		virtual ~cpvColorCorrectCmd();

		static	void*	creator();
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual MStatus redoIt();
		virtual MStatus undoIt();

		virtual bool	isUndoable() const { return true; }

	public:
		MStatus	parseArguments( const MArgList& args );

	private:
		IColorOperator*	m_activeOperator;

		CPVADJUST_ARRAY	m_adjustments;
	};

}//End namespace rage

//-----------------------------------------------------------------------------

#endif //__CPV_COLOR_CORRECT_H__

