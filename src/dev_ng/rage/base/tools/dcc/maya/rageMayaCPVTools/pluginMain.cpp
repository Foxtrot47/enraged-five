// 
// /pluginMain.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#pragma warning(pop)

#include "cpvHeightGradient.h"
#include "cpvRadialGradient.h"
#include "cpvColorCorrect.h"
#include "cpvBlur.h"

using namespace rage;

//-----------------------------------------------------------------------------

MStatus initializePlugin( MObject obj )
{
	MFnPlugin plugin( obj, "Alias|Wavefront", "6.5", "Any");
	
	plugin.registerCommand("cpvHeightGradient", cpvHeightGradientCmd::creator, cpvHeightGradientCmd::newSyntax);
	plugin.registerCommand("cpvRadialGradient", cpvRadialGradientCmd::creator, cpvRadialGradientCmd::newSyntax);
	plugin.registerCommand("cpvColorCorrect", cpvColorCorrectCmd::creator, cpvColorCorrectCmd::newSyntax);
	plugin.registerCommand("cpvBlur", cpvBlurCmd::creator, cpvBlurCmd::newSyntax);
	
	return( MS::kSuccess );
}

//-----------------------------------------------------------------------------

MStatus uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );
	
	plugin.deregisterCommand("cpvHeightGradient");
	plugin.deregisterCommand("cpvRadialGradient");
	plugin.deregisterCommand("cpvColorCorrect");
	plugin.deregisterCommand("cpvBlur");

	return( MS::kSuccess );
}

//-----------------------------------------------------------------------------

