// 
// /cpvRadialGradient.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include "maya/MGlobal.h"
#include "maya/MObject.h"
#include "maya/MArgDatabase.h"
#include "maya/MStringArray.h"
#include "maya/MFnDagNode.h"
#include "maya/MFnMesh.h"
#include "maya/MFloatPointArray.h"
#include "maya/MSelectionList.h"
#include "maya/MItSelectionList.h"
#include "maya/MDagPath.h"
#include "maya/MItMeshPolygon.h"
#include "maya/MPointArray.h"
#include "maya/MArgList.h"
#include "maya/MFnTransform.h"
#include "maya/MObjectArray.h"
#include "maya/MFnComponentListData.h"
#include "maya/MFnSingleIndexedComponent.h"
#include "maya/MItMeshVertex.h"
#include "maya/MColorArray.h"
#include "maya/MColor.h"
#include "maya/MMatrix.h"
#include "maya/MTransformationMatrix.h"
#include "maya/MVector.h"
#pragma warning(pop)

#include "math.h"
#include "float.h"

#include <vector>
#include <algorithm>

#include "cpvCommon.h"
#include "cpvRadialGradient.h"

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------

cpvRadialGradientCmd::cpvRadialGradientCmd()
	: m_startColor(1.0f,1.0f,1.0f,1.0f)
	, m_endColor(0.0f,0.0f,0.0f,1.0f)
	, m_fallOff(1.0)
	, m_objectBBoxRelative(false)
	, m_applicationMode(ADDITIVE)
	, m_pointRelative(false)
	, m_pointRelMin(0.0f)
	, m_pointRelMax(0.0f)
{
}

//-----------------------------------------------------------------------------

cpvRadialGradientCmd::~cpvRadialGradientCmd()
{
	for(CPVADJUST_ARRAY::iterator it = m_adjustments.begin(); it != m_adjustments.end(); ++it)
	{
		delete (*it);
	}
}

//-----------------------------------------------------------------------------

MSyntax	cpvRadialGradientCmd::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag("-sr", "-startred", MSyntax::kDouble);
	syntax.addFlag("-sg", "-startgreen", MSyntax::kDouble);
	syntax.addFlag("-sb", "-startblue", MSyntax::kDouble);

	syntax.addFlag("-er", "-endred", MSyntax::kDouble);
	syntax.addFlag("-eg", "-endgreen", MSyntax::kDouble);
	syntax.addFlag("-eb", "-endblue", MSyntax::kDouble);
	
	syntax.addFlag("-fo", "-falloff", MSyntax::kDouble);

	syntax.addFlag("-cn", "-centerNode", MSyntax::kString);

	syntax.addFlag("-am", "-applicationMode", MSyntax::kString);

	syntax.addFlag("-or", "-objectRelative");
	syntax.addFlag("-pr", "-pointRelative", MSyntax::kDouble, MSyntax::kDouble);
	
	syntax.addFlag("-gp", "-gradientPoint", MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble, MSyntax::kDouble);
	syntax.makeFlagMultiUse("-gradientPoint");

	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus cpvRadialGradientCmd::parseArguments( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	MArgDatabase argDb(syntax(), args);

	//Parse the starting color
	if(argDb.isFlagSet("-startred", &status))
	{
		double red;
		argDb.getFlagArgument("-startred", 0, red);
		m_startColor.r = (float)red;
	}
	if(argDb.isFlagSet("-startgreen", &status))
	{
		double green;
		argDb.getFlagArgument("-startgreen", 0, green);
		m_startColor.g = (float)green;
	}
	if(argDb.isFlagSet("-startblue", &status))
	{
		double blue;
		argDb.getFlagArgument("-startblue", 0, blue);
		m_startColor.b = (float)blue;
	}

	//Parse the ending color
	if(argDb.isFlagSet("-endred", &status))
	{
		double red;
		argDb.getFlagArgument("-endred", 0, red);
		m_endColor.r = (float)red;
	}
	if(argDb.isFlagSet("-endgreen", &status))
	{
		double green;
		argDb.getFlagArgument("-endgreen", 0, green);
		m_endColor.g = (float)green;
	}
	if(argDb.isFlagSet("-endblue", &status))
	{
		double blue;
		argDb.getFlagArgument("-endblue", 0, blue);
		m_endColor.b = (float)blue;
	}
	
	//Parse the falloff
	if(argDb.isFlagSet("-falloff", &status))
	{
		double falloff;
		argDb.getFlagArgument("-falloff", 0, falloff);
		m_fallOff = (float)falloff;

		m_gradientEvaluator.SetFalloff(m_fallOff);
	}

	//Parse the optional radial center
	if(argDb.isFlagSet("-centerNode", &status))
	{
		MString centerNodeName;
		argDb.getFlagArgument("-centerNode", 0, centerNodeName);
		if(centerNodeName.length())
		{
			MSelectionList centerNodeSelList;
			status = centerNodeSelList.add(centerNodeName);
			if(status == MS::kSuccess)
			{
				status = centerNodeSelList.getDagPath(0, m_centerNodeDagPath);
			}
		}
	}

	//Parse the application mode
	if(argDb.isFlagSet("-applicationMode", &status))
	{
		MString strAppMode;
		argDb.getFlagArgument("-applicationMode", 0, strAppMode);
		if(_strcmpi("replace", strAppMode.asChar()) == 0)
			m_applicationMode = REPLACE;
		else if(_strcmpi("additive", strAppMode.asChar()) == 0)
			m_applicationMode = ADDITIVE;
		else if(_strcmpi("subtractive", strAppMode.asChar()) == 0)
			m_applicationMode = SUBTRACTIVE;
		else
		{
			char msgBuf[512];
			sprintf_s(msgBuf,"Unknown color application mode specification '%s' supplied for -applicationMode argument, valid values are \"replace\", \"additive\", or \"subtractive\"", strAppMode.asChar());
			MGlobal::displayError(msgBuf);
			return MS::kInvalidParameter;
		}
	}

	//Parse the gradient points
	unsigned int nGradPoints = argDb.numberOfFlagUses("-gradientPoint");
	for(unsigned int ptIdx=0; ptIdx<nGradPoints; ptIdx++)
	{
		MArgList gpArgs;
		status = argDb.getFlagArgumentList("-gradientPoint", ptIdx, gpArgs);
		
		double pos, r, g, b;
		status = gpArgs.get(0, pos);
		status = gpArgs.get(1, r);
		status = gpArgs.get(2, g);
		status = gpArgs.get(3, b);
		MColor pointColor((float)r, (float)g, (float)b, 1.0f);
		m_gradientEvaluator.AddPoint(pointColor, (float)pos);
	}

	//Parse the object/selection relative flag
	m_objectBBoxRelative = argDb.isFlagSet("-objectRelative");

	//Parse the point relative flag
	m_pointRelative = argDb.isFlagSet("-pointRelative");
	if(m_pointRelative)
	{
		double val;
		argDb.getFlagArgument("-pointRelative", 0, val);
		m_pointRelMin = (val < 0.0) ? 0.0f : (float) val;
		argDb.getFlagArgument("-pointRelative", 1, val);
		m_pointRelMax = (float)val;
	}

	//Command error checking...
	if(m_objectBBoxRelative && m_pointRelative)
	{
		MGlobal::displayError("Arguments '-objectRelative' and '-pointRelative' cannot be used simultaneously");
		return MS::kInvalidParameter;
	}

	return status;
}

//-----------------------------------------------------------------------------

MStatus cpvRadialGradientCmd::doIt( const MArgList& args )
{
	MStatus status = MS::kSuccess;
	
	//Parse the command arguments
	status = parseArguments( args );
	if(status != MS::kSuccess)
	{
		status.perror("parseArguments failed.");
		return status;
	}

	//Collect the CPV info from the elements selected in the scene
	SceneCpvInfo cpvInfo;
	status = cpvInfo.InitializeFromSelection();
	if(status != MS::kSuccess)
	{
		status.perror("SceneCpvInfo::InitializeFromSelection failed.");
		return status;
	}

	MPoint sceneRadialCenter;

	if(m_centerNodeDagPath.isValid())
	{
		MTransformationMatrix centerNodeTM(m_centerNodeDagPath.inclusiveMatrix());
		MVector centerNodeTrans = centerNodeTM.translation(MSpace::kWorld);
		sceneRadialCenter.x = centerNodeTrans.x;
		sceneRadialCenter.y = centerNodeTrans.y;
		sceneRadialCenter.z = centerNodeTrans.z;
	}
	else
	{	
		sceneRadialCenter.x = cpvInfo.m_bbMin[0] + ((cpvInfo.m_bbMax[0] - cpvInfo.m_bbMin[0]) / 2.0f);
		sceneRadialCenter.y = cpvInfo.m_bbMin[1] + ((cpvInfo.m_bbMax[1] - cpvInfo.m_bbMin[1]) / 2.0f);
		sceneRadialCenter.z = cpvInfo.m_bbMin[2] + ((cpvInfo.m_bbMax[2] - cpvInfo.m_bbMin[2]) / 2.0f);
	}

	float distR = m_endColor.r - m_startColor.r;
	float distG = m_endColor.g - m_startColor.g;
	float distB = m_endColor.b - m_startColor.b;

	vector<MPoint>	objectRadialCenters;
	vector<float>	objectMaxDistances;
	
	//Find the point in the set that is the furthest away from the center of the radial gradient
	float sceneMaxDist = -FLT_MAX;
	for(VERTINFO_ARRAY::iterator viIt = cpvInfo.m_vertInfoArr.begin(); viIt != cpvInfo.m_vertInfoArr.end(); ++viIt)
	{
		MPoint objectRadialCenter;

		meshVertexInfo* pMvi = (*viIt);

		if(m_objectBBoxRelative)
		{
			objectRadialCenter.x = pMvi->m_meshBbMin[0] + ((pMvi->m_meshBbMax[0] - pMvi->m_meshBbMin[0]) / 2.0f);
			objectRadialCenter.y = pMvi->m_meshBbMin[1] + ((pMvi->m_meshBbMax[1] - pMvi->m_meshBbMin[1]) / 2.0f);
			objectRadialCenter.z = pMvi->m_meshBbMin[2] + ((pMvi->m_meshBbMax[2] - pMvi->m_meshBbMin[2]) / 2.0f);
		}
		else
		{
			objectRadialCenter = sceneRadialCenter;
		}

		objectRadialCenters.push_back(objectRadialCenter);

		float objectMaxDist = -FLT_MAX;

		int vCount = pMvi->vertindices.length();
		for(int i=0; i<vCount; i++)
		{
			meshVertexColorInfo* pClrInfo = pMvi->vertColors[i];
			int cCount = pClrInfo->colorValues.length();
			for( int j=0; j<cCount; j++)
			{
				float dist = (float)objectRadialCenter.distanceTo(pMvi->vertpositions[i]);
				if(dist > sceneMaxDist)
				{
					sceneMaxDist = dist;
				}
				if(dist > objectMaxDist)
				{
					objectMaxDist = dist;
				}
			}
		}
		objectMaxDistances.push_back(objectMaxDist);
	}

	//Apply the radial gradient
	int objectIdx = 0;
	for(VERTINFO_ARRAY::iterator viIt = cpvInfo.m_vertInfoArr.begin(); viIt != cpvInfo.m_vertInfoArr.end(); ++viIt)
	{
		meshVertexInfo* pMvi = (*viIt);
		MFnMesh fnMesh(pMvi->dagPath);

		int vCount = pMvi->vertindices.length();
		MPoint& radialCenter = objectRadialCenters[objectIdx];

		CPVAdjustInfo* pAdjustment = new CPVAdjustInfo();
		pAdjustment->m_dagPath = pMvi->dagPath;

		float maxDist;
		float minDist = 0.0f;

		if(!m_objectBBoxRelative)
		{
			if(!m_pointRelative)
				maxDist = sceneMaxDist;
			else
			{
				maxDist = m_pointRelMax;
				minDist = m_pointRelMin;
			}
		}
		else
			maxDist = objectMaxDistances[objectIdx];

		for(int i=0; i<vCount; i++)
		{
			meshVertexColorInfo* pClrInfo = pMvi->vertColors[i];
			
			int cCount = pClrInfo->colorValues.length();
			for( int j=0; j<cCount; j++)
			{
				MColor newColor(pClrInfo->colorValues[j].r, pClrInfo->colorValues[j].g, pClrInfo->colorValues[j].b);
			
				float vDist = (float)radialCenter.distanceTo( pMvi->vertpositions[i] );

				float redAdj, greenAdj, blueAdj;
				
				float lambda = (vDist - minDist) / (maxDist - minDist);
				
				if( (lambda >= 0.0f) && (lambda <= 1.0f) )
				{
					if(m_gradientEvaluator.NumPoints() == 0)
					{
						redAdj = m_startColor.r + pow(lambda, m_fallOff) * distR;
						greenAdj = m_startColor.g + pow(lambda, m_fallOff) * distG;
						blueAdj = m_startColor.b + pow(lambda, m_fallOff) * distB;
					}
					else
					{
						//The advanced gradient arguments were used
						MColor gradColor = m_gradientEvaluator.EvaluateAt(lambda);
						redAdj = gradColor.r;
						greenAdj = gradColor.g;
						blueAdj = gradColor.b;
					}

					if(m_applicationMode == REPLACE)
					{
						newColor.r = redAdj;
						newColor.g = greenAdj;
						newColor.b = blueAdj;
					}
					else if(m_applicationMode == ADDITIVE)
					{
						newColor.r = Clamp(pClrInfo->colorValues[j].r + redAdj, 0.0f, 1.0f);
						newColor.g = Clamp(pClrInfo->colorValues[j].g + greenAdj, 0.0f, 1.0f);
						newColor.b = Clamp(pClrInfo->colorValues[j].b + blueAdj, 0.0f, 1.0f);
					}
					else if(m_applicationMode == SUBTRACTIVE)
					{
						newColor.r = Clamp(pClrInfo->colorValues[j].r - redAdj, 0.0f, 1.0f);
						newColor.g = Clamp(pClrInfo->colorValues[j].g - greenAdj, 0.0f, 1.0f);
						newColor.b = Clamp(pClrInfo->colorValues[j].b - blueAdj, 0.0f, 1.0f);
					}
				}

				//Store the old and new color values
				pAdjustment->m_ClrIndicies.append( pClrInfo->colorIndicies[j] );
				
				pAdjustment->m_oldClrValues.append( pClrInfo->colorValues[j] );
				pAdjustment->m_newClrValues.append( newColor );
			}
		}
		
		objectIdx++;
		m_adjustments.push_back(pAdjustment);
	}

	return redoIt();
}

//-----------------------------------------------------------------------------

MStatus cpvRadialGradientCmd::redoIt()
{
	MStatus status;

	for(CPVADJUST_ARRAY::iterator it = m_adjustments.begin(); it != m_adjustments.end(); ++it)
	{
		CPVAdjustInfo* pAdjustment = (*it);
		if(pAdjustment->m_dagPath.isValid())
		{
			MFnMesh fnMesh(pAdjustment->m_dagPath);
			status = fnMesh.setSomeColors( pAdjustment->m_ClrIndicies, pAdjustment->m_newClrValues );
			if(status != MS::kSuccess)
			{
				status.perror("cpvRadialGradientCmd::redoIt, 'setSomeColors' failed");
				return status;
			}
			fnMesh.updateSurface();
		}
		else
		{
			char msgBuf[512];
			MString fullPath = pAdjustment->m_dagPath.fullPathName();
			sprintf_s(msgBuf,"DAG Path '%s' is no longer a valid path, unable to perform operation", fullPath.asChar());
			MGlobal::displayError(msgBuf);
			return MS::kFailure;
		}
	}
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus cpvRadialGradientCmd::undoIt()
{
	MStatus status;

	for(CPVADJUST_ARRAY::iterator it = m_adjustments.begin(); it != m_adjustments.end(); ++it)
	{
		CPVAdjustInfo* pAdjustment = (*it);
		if(pAdjustment->m_dagPath.isValid())
		{
			MFnMesh fnMesh(pAdjustment->m_dagPath);
			status = fnMesh.setSomeColors( pAdjustment->m_ClrIndicies, pAdjustment->m_oldClrValues );
			if(status != MS::kSuccess)
			{
				status.perror("cpvRadialGradientCmd::undoIt, 'setSomeColors' failed");
				return status;
			}
			fnMesh.updateSurface();
		}
		else
		{
			char msgBuf[512];
			MString fullPath = pAdjustment->m_dagPath.fullPathName();
			sprintf_s(msgBuf,"DAG Path '%s' is no longer a valid path, unable to perform operation", fullPath.asChar());
			MGlobal::displayError(msgBuf);
			return MS::kFailure;
		}
	}
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------



