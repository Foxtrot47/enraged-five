// 
// /cpvHeightGradient.h
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __CPV_COMMON_H__
#define __CPV_COMMON_H__

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MGlobal.h>
#include <maya/MDagPath.h>
#include <maya/MIntArray.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MColorArray.h>
#include <maya/MColor.h>
#pragma warning(pop)

#include <vector>

using namespace std;

namespace rage
{

//-----------------------------------------------------------------------------

template <class Type> inline Type Clamp(Type t,Type a,Type b)
{
	return (t < a) ? a : (t > b) ? b : t;
}

//-------------------------------------------------------------------------

//Structure that can be used to support undo/redo operations
struct CPVAdjustInfo
{
	CPVAdjustInfo()
	{
		m_ClrIndicies.setSizeIncrement( 128 );
		m_newClrValues.setSizeIncrement( 128 );
		m_oldClrValues.setSizeIncrement( 128 );
	}

	MDagPath		m_dagPath;

	MIntArray		m_ClrIndicies;
	
	MColorArray		m_newClrValues;
	MColorArray		m_oldClrValues;
};

typedef std::vector<CPVAdjustInfo*>	CPVADJUST_ARRAY;

//-----------------------------------------------------------------------------

struct meshVertexColorInfo
{
	MIntArray		colorIndicies;
	MColorArray		colorValues;
};

//-----------------------------------------------------------------------------

struct meshVertexInfo
{
	meshVertexInfo(const MDagPath& dp)
		: dagPath(dp)
	{};
	~meshVertexInfo()
	{
		for(vector<meshVertexColorInfo*>::iterator it = vertColors.begin(); it != vertColors.end(); ++it)
			delete (*it);
	}

	MDagPath						dagPath;

	float							m_meshBbMax[3];
	float							m_meshBbMin[3];

	MIntArray						vertindices;
	MPointArray						vertpositions;

	vector<meshVertexColorInfo*>	vertColors;
};

__inline int operator==(meshVertexInfo *pA, const MDagPath& other)
{
	return (pA->dagPath == other);
}

typedef vector< meshVertexInfo* > VERTINFO_ARRAY;

//-----------------------------------------------------------------------------

struct SceneCpvInfo
{
	SceneCpvInfo();
	~SceneCpvInfo();

	MStatus			InitializeFromSelection();

	float			m_bbMax[3];
	float			m_bbMin[3];

	MColor			m_maxValuePerChn;
	MColor			m_minValuePerChn;

	float			m_maxHsvValue;
	float			m_minHsvValue;

	VERTINFO_ARRAY	m_vertInfoArr;
};

//-----------------------------------------------------------------------------

class GradientPoint
{
public:
	GradientPoint& operator= (const GradientPoint& other)
	{
		m_color = other.m_color;
		m_position = other.m_position;
		return (*this);
	}

	bool operator== (const GradientPoint& other)
	{
		return ( (m_color == other.m_color) && ( m_position == other.m_position) );
	}

	bool operator< (const GradientPoint& other)
	{
		return ( m_position < other.m_position );
	}

	bool operator< (float tVal)
	{
		return ( m_position < tVal );
	}

public:
	MColor	m_color;
	float	m_position;
};

class GradientEval
{
public:
	GradientEval()
		: m_falloff(1.0f)
	{};

	void	AddPoint(const MColor &color, float position);
	int		NumPoints() const { return (int)m_points.size(); }
	
	void	SetFalloff(float falloff) { m_falloff = falloff; }

	MColor	EvaluateAt(float t);


private:
	float					m_falloff;
	vector<GradientPoint>	m_points;

};

//-----------------------------------------------------------------------------

}//End namespace rage

#endif //__CPV_COMMON_H__

