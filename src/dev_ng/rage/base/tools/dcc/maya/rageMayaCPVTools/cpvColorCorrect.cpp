// 
// /cpvColorCorrect.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include "maya/MGlobal.h"
#include "maya/MObject.h"
#include "maya/MArgDatabase.h"
#include "maya/MStringArray.h"
#include "maya/MFnDagNode.h"
#include "maya/MFnMesh.h"
#include "maya/MFloatPointArray.h"
#include "maya/MSelectionList.h"
#include "maya/MItSelectionList.h"
#include "maya/MDagPath.h"
#include "maya/MItMeshPolygon.h"
#include "maya/MPointArray.h"
#include "maya/MArgList.h"
#include "maya/MFnTransform.h"
#include "maya/MObjectArray.h"
#include "maya/MFnComponentListData.h"
#include "maya/MFnSingleIndexedComponent.h"
#include "maya/MItMeshVertex.h"
#include "maya/MColorArray.h"
#include "maya/MColor.h"
#include "maya/MMatrix.h"
#include "maya/MTransformationMatrix.h"
#include "maya/MVector.h"
#pragma warning(pop)

#include "math.h"
#include "float.h"

#include <vector>
#include <algorithm>

#include "cpvCommon.h"
#include "cpvColorCorrect.h"

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------

cpvColorCorrectCmd::cpvColorCorrectCmd()
{
}

//-----------------------------------------------------------------------------

cpvColorCorrectCmd::~cpvColorCorrectCmd()
{
	for(CPVADJUST_ARRAY::iterator it = m_adjustments.begin(); it != m_adjustments.end(); ++it)
	{
		delete (*it);
	}

	if(m_activeOperator)
		delete m_activeOperator;
}

//-----------------------------------------------------------------------------

void* cpvColorCorrectCmd::creator()
{
	return new cpvColorCorrectCmd();
}

//-----------------------------------------------------------------------------

MSyntax cpvColorCorrectCmd::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag("-ti", "-tint", MSyntax::kDouble);
	syntax.addFlag("-tic", "-tintColorize");
	syntax.addFlag("-li", "-lighten", MSyntax::kDouble);
	syntax.addFlag("-dk", "-darken", MSyntax::kDouble);
	syntax.addFlag("-scu", "-scaleUp", MSyntax::kDouble);
	syntax.addFlag("-scd", "-scaleDown", MSyntax::kDouble);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus	cpvColorCorrectCmd::parseArguments( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	MArgDatabase argDb(syntax(), args);
	
	if(argDb.isFlagSet("-tint", &status))
	{
		double tintVal;
		argDb.getFlagArgument("-tint", 0, tintVal);

		m_activeOperator = new TintOperator();
		((TintOperator*)m_activeOperator)->m_tintValue = (float) tintVal;

		if(argDb.isFlagSet("-tintColorize"))
			((TintOperator*)m_activeOperator)->m_colorize = true;
		else
			((TintOperator*)m_activeOperator)->m_colorize = false;
	}
	else if(argDb.isFlagSet("-lighten", &status))
	{
		double amt;
		argDb.getFlagArgument("-lighten", 0, amt);

		m_activeOperator = new LightenOperator();
		((LightenOperator*)m_activeOperator)->m_amount = (float)amt;
	}
	else if(argDb.isFlagSet("-darken", &status))
	{
		double amt;
		argDb.getFlagArgument("-darken", 0, amt);

		m_activeOperator = new DarkenOperator();
		((DarkenOperator*)m_activeOperator)->m_amount = (float)amt;
	}
	else if(argDb.isFlagSet("-scaleUp", &status))
	{
		double amt;
		argDb.getFlagArgument("-scaleUp", 0, amt);

		m_activeOperator = new ScaleUpOperator();
		((ScaleUpOperator*)m_activeOperator)->m_amount = (float)amt;
	}
	else if(argDb.isFlagSet("-scaleDown", &status))
	{
		double amt;
		argDb.getFlagArgument("-scaleDown", 0, amt);

		m_activeOperator = new ScaleDownOperator();
		((ScaleDownOperator*)m_activeOperator)->m_amount = (float)amt;
	}

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus cpvColorCorrectCmd::doIt( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	//Parse the command arguments
	status = parseArguments( args );
	if(status != MS::kSuccess)
	{
		status.perror("parseArguments failed.");
		return status;
	}

	//Collect the CPV info from the elements selected in the scene
	SceneCpvInfo cpvInfo;
	status = cpvInfo.InitializeFromSelection();
	if(status != MS::kSuccess)
	{
		status.perror("SceneCpvInfo::InitializeFromSelection failed.");
		return status;
	}

	for(VERTINFO_ARRAY::iterator viIt = cpvInfo.m_vertInfoArr.begin(); viIt != cpvInfo.m_vertInfoArr.end(); ++viIt)
	{
		meshVertexInfo* pMvi = (*viIt);
		MFnMesh fnMesh(pMvi->dagPath);

		int vCount = pMvi->vertindices.length();

		CPVAdjustInfo* pAdjustment = new CPVAdjustInfo();
		pAdjustment->m_dagPath = pMvi->dagPath;

		for(int i=0; i<vCount; i++)
		{
			meshVertexColorInfo* pClrInfo = pMvi->vertColors[i];
			
			int cCount = pClrInfo->colorValues.length();
			for( int j=0; j<cCount; j++)
			{
				MColor newColor;
			
				//Compute the adjusted color
				m_activeOperator->DoOperation(pClrInfo->colorValues[j], newColor);

				//Store the old and new color values
				pAdjustment->m_ClrIndicies.append( pClrInfo->colorIndicies[j] );
				
				pAdjustment->m_oldClrValues.append( pClrInfo->colorValues[j] );
				pAdjustment->m_newClrValues.append( newColor );
			}
		}

		m_adjustments.push_back(pAdjustment);
	}

	//Perform the actual adjustment
	return redoIt();
}

//-----------------------------------------------------------------------------

MStatus cpvColorCorrectCmd::redoIt()
{
	MStatus status;

	for(CPVADJUST_ARRAY::iterator it = m_adjustments.begin(); it != m_adjustments.end(); ++it)
	{
		CPVAdjustInfo* pAdjustment = (*it);
		if(pAdjustment->m_dagPath.isValid())
		{
			MFnMesh fnMesh(pAdjustment->m_dagPath);
			status = fnMesh.setSomeColors( pAdjustment->m_ClrIndicies, pAdjustment->m_newClrValues );
			if(status != MS::kSuccess)
			{
				status.perror("cpvColorCorrectCmd::redoIt, 'setSomeColors' failed");
				return status;
			}
			fnMesh.updateSurface();
		}
		else
		{
			char msgBuf[512];
			MString fullPath = pAdjustment->m_dagPath.fullPathName();
			sprintf_s(msgBuf,"DAG Path '%s' is no longer a valid path, unable to perform operation", fullPath.asChar());
			MGlobal::displayError(msgBuf);
			return MS::kFailure;
		}
	}
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus cpvColorCorrectCmd::undoIt()
{
	MStatus status;

	for(CPVADJUST_ARRAY::iterator it = m_adjustments.begin(); it != m_adjustments.end(); ++it)
	{
		CPVAdjustInfo* pAdjustment = (*it);
		if(pAdjustment->m_dagPath.isValid())
		{
			MFnMesh fnMesh(pAdjustment->m_dagPath);
			status = fnMesh.setSomeColors( pAdjustment->m_ClrIndicies, pAdjustment->m_oldClrValues );
			if(status != MS::kSuccess)
			{
				status.perror("cpvColorCorrectCmd::undoIt, 'setSomeColors' failed");
				return status;
			}
			fnMesh.updateSurface();
		}
		else
		{
			char msgBuf[512];
			MString fullPath = pAdjustment->m_dagPath.fullPathName();
			sprintf_s(msgBuf,"DAG Path '%s' is no longer a valid path, unable to perform operation", fullPath.asChar());
			MGlobal::displayError(msgBuf);
			return MS::kFailure;
		}
	}
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus TintOperator::DoOperation(MColor& oldColor, MColor& newColor) const
{
	float h,s,v;
	oldColor.get(MColor::kHSV, h, s, v);

	if(m_colorize)
	{
		if(s == 0.0)
			s = 0.25f;
	}

	newColor.set(MColor::kHSV, m_tintValue, s, v);
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus LightenOperator::DoOperation(MColor& oldColor, MColor& newColor) const
{
	float h,s,v;
	oldColor.get(MColor::kHSV, h, s, v);
	v += m_amount;;
	Clamp(v, 0.0f, 1.0f);
	newColor.set(MColor::kHSV, h, s, v);
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus DarkenOperator::DoOperation(MColor& oldColor, MColor& newColor) const
{
	float h,s,v;
	oldColor.get(MColor::kHSV, h, s, v);
	v -= m_amount;
	Clamp(v, 0.0f, 1.0f);
	newColor.set(MColor::kHSV, h, s, v);
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus ScaleUpOperator::DoOperation(MColor& oldColor, MColor& newColor) const
{
	float h,s,v;
	oldColor.get(MColor::kHSV, h, s, v);
	v = v + (v * m_amount);
	Clamp(v, 0.0f, 1.0f);
	newColor.set(MColor::kHSV, h, s, v);
	return MS::kSuccess;
}
//-----------------------------------------------------------------------------

MStatus ScaleDownOperator::DoOperation(MColor& oldColor, MColor& newColor) const
{
	float h,s,v;
	oldColor.get(MColor::kHSV, h, s, v);
	v = v - (v * m_amount);
	Clamp(v, 0.0f, 1.0f);
	newColor.set(MColor::kHSV, h, s, v);
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------


