// 
// /cpvBlur.cpp
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)

#include "maya/MGlobal.h"
#include "maya/MObject.h"
#include "maya/MArgDatabase.h"
#include "maya/MStringArray.h"
#include "maya/MFnDagNode.h"
#include "maya/MFnMesh.h"
#include "maya/MFloatPointArray.h"
#include "maya/MSelectionList.h"
#include "maya/MItSelectionList.h"
#include "maya/MDagPath.h"
#include "maya/MItMeshPolygon.h"
#include "maya/MPointArray.h"
#include "maya/MArgList.h"
#include "maya/MFnTransform.h"
#include "maya/MObjectArray.h"
#include "maya/MFnComponentListData.h"
#include "maya/MFnSingleIndexedComponent.h"
#include "maya/MItMeshVertex.h"
#include "maya/MColorArray.h"
#include "maya/MColor.h"
#include "maya/MMatrix.h"
#include "maya/MTransformationMatrix.h"
#include "maya/MVector.h"
#include "maya/MProgressWindow.h"
#pragma warning(pop)

#include "math.h"
#include "float.h"

#include <vector>
#include <list>
#include <algorithm>
#include <set>

#include "cpvCommon.h"
#include "cpvBlur.h"

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------

cpvBlurCmd::cpvBlurCmd()
	: m_bDistanceWeighting(false)
	, m_adjIterations(1)
{
}

//-----------------------------------------------------------------------------

cpvBlurCmd::~cpvBlurCmd()
{
	for(CPVADJUST_ARRAY::iterator it = m_adjustments.begin(); it != m_adjustments.end(); ++it)
	{
		delete (*it);
	}
}

//-----------------------------------------------------------------------------

MSyntax cpvBlurCmd::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag("-dw", "-distanceWeight");
	syntax.addFlag("-adi", "-adjIterations", MSyntax::kLong);

	syntax.enableQuery(false);
	syntax.enableEdit(false);

	return syntax;
}

//-----------------------------------------------------------------------------

MStatus	cpvBlurCmd::parseArguments( const MArgList& args )
{
	MStatus status = MS::kSuccess;

	MArgDatabase argDb(syntax(), args);

	m_bDistanceWeighting = argDb.isFlagSet("-distanceWeight");

	if(argDb.isFlagSet("-adjIterations"))
	{
		status = argDb.getFlagArgument("-adjIterations", 0, m_adjIterations);
		if(status != MS::kSuccess)
			return MS::kInvalidParameter;
	}

	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MColor cpvBlurCmd::computeBlurColor(MFnMesh &fnMesh, MDagPath &dp, MPointArray& meshPoints, int srcVtxIndex)
{
	MPoint srcVtxPos = meshPoints[srcVtxIndex];

	//Parallel arrays of colors and distances from the "source" vertex
	MColorArray	adjVtxColors;
	MFloatArray adjVtxDistances;
	
	//Set to keep track of vertex indices that have already been checked/added to the list
	//of colors to consider in the blur operation.
	set<int>	checkedVtxIndices;

	checkedVtxIndices.insert(srcVtxIndex);
	
	//Create a vertex iterator set to the "source" vertex
	MFnSingleIndexedComponent fnVtxSiCmp;
	fnVtxSiCmp.create(MFn::kMeshVertComponent);
	fnVtxSiCmp.addElement(srcVtxIndex);
	MItMeshVertex meshVtxIt(dp, fnVtxSiCmp.object());

	//Add the source vertex color to the color and distance arrays
	MColor sourceVtxColor;
	meshVtxIt.getColor(sourceVtxColor);
	adjVtxColors.append(sourceVtxColor);
	adjVtxDistances.append(0.0f);
	
	//Create an queue that will be used during the process of gathering
	//adjacent vertices that should be considered in the blur process
	list<MIntArray> adjacencyArray;
	
	//Seed the adjacency queue with the vertices that are used by the 
	//faces which connected to the source vertex
	adjacencyArray.push_back(MIntArray());
	MIntArray& vtxAdjArray = adjacencyArray.front();
	MIntArray seedFaces;
	meshVtxIt.getConnectedFaces(seedFaces);
	unsigned int nSeedFaces = seedFaces.length();
	for(unsigned int i=0; i<nSeedFaces; i++)
	{
		MIntArray conPolyVertIndices;
		fnMesh.getPolygonVertices(seedFaces[i], conPolyVertIndices);
		unsigned int nConPolyVertIndicies = conPolyVertIndices.length();
		for(unsigned int j=0; j<nConPolyVertIndicies; j++)
		{
			if(conPolyVertIndices[j] != srcVtxIndex)
				vtxAdjArray.append(conPolyVertIndices[j]);
		}
	}
	
	float maxDist = -FLT_MAX;

	int iterCount = 0;
	while(adjacencyArray.size() && iterCount < m_adjIterations)
	{
		MIntArray& adjVertIndicies = adjacencyArray.front();

		//Create a vertex iterator for the adjacent vertices, allowing us access to
		//their color information as well as their adjcent faces/vertices
		MFnSingleIndexedComponent adjVtxSiCmp;
		adjVtxSiCmp.create(MFn::kMeshVertComponent);
		adjVtxSiCmp.addElements(adjVertIndicies);
		MItMeshVertex adjMeshVtxIt(dp, adjVtxSiCmp.object());

		set<int> nextIterIndicies;

		//Iterate over the vertices collecting their vertex colors and distance from the source vertex.
		//Also add their adjacent vertex indicies to the queue
		for(adjMeshVtxIt.reset(); !adjMeshVtxIt.isDone(); adjMeshVtxIt.next())
		{
			//First check if we have already come across this vertex index
			int vtxIndex = adjMeshVtxIt.index();
			if(checkedVtxIndices.find(vtxIndex) != checkedVtxIndices.end())
				continue;
			else
				checkedVtxIndices.insert(vtxIndex);
			
			//Get the color of the vertex
			MColor adjVtxColor;
			adjMeshVtxIt.getColor(adjVtxColor);
			adjVtxColors.append(adjVtxColor);

			//Compute the distance from the source vertex
			MPoint adjVtxPos;
			adjMeshVtxIt.position(MSpace::kWorld);
			float adjVtxDistance = (float)(srcVtxPos.distanceTo(adjVtxPos));
			adjVtxDistances.append( adjVtxDistance );
			if(adjVtxDistance > maxDist)
				maxDist = adjVtxDistance;

			//Add the adjacent vertices 
			MIntArray adjVtxConFaces;
			adjMeshVtxIt.getConnectedFaces(adjVtxConFaces);
			unsigned int nAdjVtxConFaces = adjVtxConFaces.length();
			for(unsigned int i=0; i<nAdjVtxConFaces; i++)
			{
				MIntArray conPolyVertIndices;
				fnMesh.getPolygonVertices(adjVtxConFaces[i], conPolyVertIndices);
				unsigned int nConPolyVertIndicies = conPolyVertIndices.length();
				for(unsigned int j=0; j<nConPolyVertIndicies; j++)
				{
					if(nextIterIndicies.find(conPolyVertIndices[j]) == nextIterIndicies.end())
						nextIterIndicies.insert(conPolyVertIndices[j]);
				}
			}

		}

		//Add the next ring of adjacent vertices to the adjacency queue
		adjacencyArray.push_back(MIntArray());
		MIntArray& nextStepAdjArray = adjacencyArray.back();
		for(set<int>::iterator it = nextIterIndicies.begin(); it != nextIterIndicies.end(); ++it)
			nextStepAdjArray.append(*it);

		//Dequeue the array of vertices we just checked and increase the iteration counter
		adjacencyArray.pop_front();
		iterCount++;
	}

	int nColors = adjVtxColors.length();

	//Calculate the vertex weights
	float weightTotal = 1.0f;
	MFloatArray adjVtxWeights;
	adjVtxWeights.append(1.0f); // Source color gets full weight;
	for(int clrIdx = 1; clrIdx<nColors; clrIdx++)
	{
		float lambda = adjVtxDistances[clrIdx] / maxDist;
		float weight = 1.0f + (lambda * (0.05f - 1));
		adjVtxWeights.append(weight);
		weightTotal += weight;
	}
	
	//Compute the averaged color value
	MColor combinedColor(0.0f,0.0f,0.0f,0.0f);
	
	if(m_bDistanceWeighting)
	{
		for(int clrIdx = 0; clrIdx < nColors; clrIdx++)
			combinedColor += (adjVtxColors[clrIdx] * adjVtxWeights[clrIdx]);
		combinedColor /= weightTotal;
	}
	else
	{
		for(int clrIdx = 0; clrIdx < nColors; clrIdx++)
			combinedColor += (adjVtxColors[clrIdx]);
		combinedColor /= (float)nColors;
	}

	return combinedColor;
}

//-----------------------------------------------------------------------------

MStatus cpvBlurCmd::doIt( const MArgList& args )
{
	char titleMsg[256];

	MStatus status = MS::kSuccess;

	//Parse the command arguments
	status = parseArguments( args );
	if(status != MS::kSuccess)
	{
		status.perror("parseArguments failed.");
		return status;
	}

	//Collect the CPV info from the elements selected in the scene
	SceneCpvInfo cpvInfo;
	status = cpvInfo.InitializeFromSelection();
	if(status != MS::kSuccess)
	{
		status.perror("SceneCpvInfo::InitializeFromSelection failed.");
		return status;
	}

	MProgressWindow::reserve();
	MProgressWindow::setProgressMin(0);
	MProgressWindow::setProgressMax(100);
	MProgressWindow::startProgress();

	int totalMeshCount = (int)cpvInfo.m_vertInfoArr.size();
	int currentMesh = 1;

	for(VERTINFO_ARRAY::iterator viIt = cpvInfo.m_vertInfoArr.begin(); viIt != cpvInfo.m_vertInfoArr.end(); ++viIt)
	{
		sprintf_s(titleMsg,"Softening CPVs (%d of %d)", currentMesh, totalMeshCount);
		MProgressWindow::setTitle(titleMsg);

		meshVertexInfo* pMvi = (*viIt);
		MFnMesh fnMesh(pMvi->dagPath);

		MPointArray meshPoints;
		fnMesh.getPoints(meshPoints, MSpace::kWorld);

		int vCount = pMvi->vertindices.length();

		CPVAdjustInfo* pAdjustment = new CPVAdjustInfo();
		pAdjustment->m_dagPath = pMvi->dagPath;
		
		for(int i=0; i<vCount; i++)
		{
			MProgressWindow::setProgress( (int)( ((float)i / (float) vCount) * 100.0 ) );

			meshVertexColorInfo* pClrInfo = pMvi->vertColors[i];

			MColor newColor = computeBlurColor(fnMesh, pMvi->dagPath, meshPoints, pMvi->vertindices[i]);
			newColor.a = 1.0f;

			int cCount = pClrInfo->colorValues.length();
			for( int j=0; j<cCount; j++)
			{
				//Store the old and new color values
				pAdjustment->m_ClrIndicies.append( pClrInfo->colorIndicies[j] );
				
				pAdjustment->m_oldClrValues.append( pClrInfo->colorValues[j] );
				pAdjustment->m_newClrValues.append( newColor );
			}
		}

		m_adjustments.push_back(pAdjustment);
		currentMesh++;
	}

	MProgressWindow::endProgress();

	return redoIt();
}

//-----------------------------------------------------------------------------

MStatus	cpvBlurCmd::redoIt()
{
	MStatus status;

	for(CPVADJUST_ARRAY::iterator it = m_adjustments.begin(); it != m_adjustments.end(); ++it)
	{
		CPVAdjustInfo* pAdjustment = (*it);
		if(pAdjustment->m_dagPath.isValid())
		{
			MFnMesh fnMesh(pAdjustment->m_dagPath);
			status = fnMesh.setSomeColors( pAdjustment->m_ClrIndicies, pAdjustment->m_newClrValues );
			if(status != MS::kSuccess)
			{
				status.perror("cpvBlurCmd::redoIt, 'setSomeColors' failed");
				return status;
			}
			fnMesh.updateSurface();
		}
		else
		{
			char msgBuf[512];
			MString fullPath = pAdjustment->m_dagPath.fullPathName();
			sprintf_s(msgBuf,"DAG Path '%s' is no longer a valid path, unable to perform operation", fullPath.asChar());
			MGlobal::displayError(msgBuf);
			return MS::kFailure;
		}
	}
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------

MStatus cpvBlurCmd::undoIt()
{
	MStatus status;

	for(CPVADJUST_ARRAY::iterator it = m_adjustments.begin(); it != m_adjustments.end(); ++it)
	{
		CPVAdjustInfo* pAdjustment = (*it);
		if(pAdjustment->m_dagPath.isValid())
		{
			MFnMesh fnMesh(pAdjustment->m_dagPath);
			status = fnMesh.setSomeColors( pAdjustment->m_ClrIndicies, pAdjustment->m_oldClrValues );
			if(status != MS::kSuccess)
			{
				status.perror("cpvBlurCmd::undoIt, 'setSomeColors' failed");
				return status;
			}
			fnMesh.updateSurface();
		}
		else
		{
			char msgBuf[512];
			MString fullPath = pAdjustment->m_dagPath.fullPathName();
			sprintf_s(msgBuf,"DAG Path '%s' is no longer a valid path, unable to perform operation", fullPath.asChar());
			MGlobal::displayError(msgBuf);
			return MS::kFailure;
		}
	}
	return MS::kSuccess;
}

//-----------------------------------------------------------------------------





