// 
// /cpvBlur.h
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __CPV_BLUR_H__
#define __CPV_BLUR_H__

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MPxCommand.h>
#include <maya/MColor.h>
#include <maya/MVector.h>
#include <maya/MDagPath.h>
#include <maya/MFnMesh.h>
#pragma warning(pop)

//-----------------------------------------------------------------------------

namespace rage
{
	class cpvBlurCmd : public MPxCommand
	{
	public:
		cpvBlurCmd();
		virtual ~cpvBlurCmd();

		static	void*	creator() { return new cpvBlurCmd(); }
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual MStatus	redoIt();
		virtual MStatus undoIt();

		virtual bool	isUndoable() const { return true; }

	public:
		MStatus	parseArguments( const MArgList& args );

	private:
		MColor computeBlurColor(MFnMesh &fnMesh, MDagPath &dp, MPointArray& meshPoints, int srcVtxIndex);

	private:
		bool			m_bDistanceWeighting;
		int				m_adjIterations;
		CPVADJUST_ARRAY	m_adjustments;
	};

}//End namespace rage

//-----------------------------------------------------------------------------

#endif //__CPV_BLUR_H__

