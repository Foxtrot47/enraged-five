// 
// /cpvRadialGradient.h
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef __CPV_RADIAL_GRADIENT_H__
#define __CPV_RADIAL_GRADIENT_H__

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)

#include <maya/MStatus.h>
#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MPxCommand.h>
#include <maya/MColor.h>
#include <maya/MVector.h>
#include <maya/MDagPath.h>

#pragma warning(pop)

#include "cpvCommon.h"

//-----------------------------------------------------------------------------

namespace rage
{
	class cpvRadialGradientCmd : public MPxCommand
	{
	public:
		enum ApplicationMode
		{
			REPLACE = 0,
			ADDITIVE,
			SUBTRACTIVE
		};

	public:
		cpvRadialGradientCmd();
		virtual ~cpvRadialGradientCmd();

		static	void*	creator() { return new cpvRadialGradientCmd(); }
		static	MSyntax	newSyntax();

		virtual MStatus	doIt( const MArgList& args );
		virtual MStatus redoIt();
		virtual MStatus undoIt();

		virtual bool	isUndoable() const { return true; }

	public:
		MStatus	parseArguments( const MArgList& args );

	private:
		MColor			m_startColor;
		MColor			m_endColor;
		float			m_fallOff;
		MDagPath		m_centerNodeDagPath;
		bool			m_objectBBoxRelative;
		bool			m_pointRelative;
		float			m_pointRelMin;
		float			m_pointRelMax;
		ApplicationMode	m_applicationMode;
		GradientEval	m_gradientEvaluator;
		CPVADJUST_ARRAY	m_adjustments;
	};

}//End namespace rage

//-----------------------------------------------------------------------------

#endif //__CPV_RADIAL_GRADIENT_H__

