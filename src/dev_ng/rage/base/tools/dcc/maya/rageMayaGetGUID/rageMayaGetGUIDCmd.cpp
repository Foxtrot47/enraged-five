/****************************************************************************
*	$Header$
*	Function to source MEL files
*
*	CHANGES
*	4.11.00	Kevin Rose	created
*
****************************************************************************/
//
// Copyright (C) 2000 Kevin's Plugin Factory 
// 
// File: rageMayaGetGUIDCmd.cpp
//
// MEL Command: rageMayaGetGUID
//
// Author: camkr
//
// Includes everything needed to register a simple MEL command with Maya.
// 
#include <maya/MSimple.h>
#include <maya/MGlobal.h>
#include <ATLComTime.h>


// Use helper macro to register a command with Maya.  It creates and
// registers a command that does not support undo or redo.  The 
// created class derives off of MPxCommand.
//
DeclareSimpleCommand( rageMayaGetGUID, "rage", "6.0");

MStatus rageMayaGetGUID::doIt( const MArgList& args )
//
//	DescrMACtion:
//		implements the MEL rageMayaGetGUID command.
//
//	Arguments:
//		args - the argument list that was passes to the command from MEL
//
//	Return Value:
//		MS::kSuccess - command succeeded
//		MS::kFailure - command failed (returning this value will cause the 
//                     MEL scrMACt that is being run to terminate unless the
//                     error is caught using a "catch" statement.
//
{
	MStatus stat = MS::kSuccess;

	bool bReturnComputerMACFromGUID = false;
	bool bReturnCreationTimeFromGUID = false;
	MString strGivenGUID = "";

	for(unsigned i=0; i<args.length(); i++)
	{
		if(args.asString(i) == "-getMAC")
		{
			strGivenGUID = args.asString(++i);
			bReturnComputerMACFromGUID = true;
		}
		else if(args.asString(i) == "-getTime")
		{
			strGivenGUID = args.asString(++i);
			bReturnCreationTimeFromGUID = true;
		}
	}

	if(bReturnComputerMACFromGUID || bReturnCreationTimeFromGUID)
	{
		// Process given GUID
		UUID obUUID;
		unsigned char* sTemp = NULL;
		UuidFromString((unsigned char*)strGivenGUID.asChar(), &obUUID);

		// What data should I extract
		if(bReturnComputerMACFromGUID)
		{
			unsigned char MACData[6];
			for (int i=2; i<8; i++)
			{
				// Bytes 2 through 7 inclusive are MAC address
				MACData[i - 2] = obUUID.Data4[i];
			}

			char acBuffer[255];
			sprintf(acBuffer, "%02X-%02X-%02X-%02X-%02X-%02X", MACData[0], MACData[1], MACData[2], MACData[3], MACData[4], MACData[5]);
			MString strMACAsMayaString(acBuffer);
			setResult( strMACAsMayaString );
			return stat;
		}

		if(bReturnCreationTimeFromGUID)
		{
			// Extract the time
			//		printf("%08x-%04x-%04x\n", obUUID.Data1, obUUID.Data2, obUUID.Data3);
			unsigned __int64 i64NoOfNanoSeconds1582 = obUUID.Data1;
			unsigned short* psNoOfNanoSeconds1582 = (unsigned short*)&i64NoOfNanoSeconds1582;
			*(psNoOfNanoSeconds1582 + 2) = obUUID.Data2;
			*(psNoOfNanoSeconds1582 + 3) = (obUUID.Data3 & 0xFFF);
			// printf("i64NoOfNanoSeconds1582 = %016I64x\n", i64NoOfNanoSeconds1582);
			// printf("i64NoOfNanoSeconds1582 = %I64d\n", i64NoOfNanoSeconds1582);

			// Ok I have the number of nano seconds since Friday, October 15, 1582 12:00:00 AM GMT
			// but this is no use to me because I can only handle dates since 1/1/1970
			// But 1/1/1970 is 122192928000000000 nano seconds after Friday, October 15, 1582 12:00:00 AM
			unsigned __int64 i64NoOfNanoSeconds1970 = (i64NoOfNanoSeconds1582 - 122192928000000000);

			// Convert to seconds
			unsigned __int64 i64NoOfSecondsSince1970 = i64NoOfNanoSeconds1970 / 10000000;
			// printf("i64NoOfSecondsSince1970 = %I64d\n", i64NoOfSecondsSince1970);

			// Convert into a real time
			CTime obTimeOfUUID(i64NoOfSecondsSince1970);
			CString strTimeAsMSString = obTimeOfUUID.Format( "%A, %d %B %Y at %H:%M:%S" );
			MString strTimeAsMayaString(strTimeAsMSString.GetString());
			setResult( strTimeAsMayaString );
			return stat;
		}
	}
	else
	{
		// Get New UUID
		UUID obUUID;
		unsigned char* sTemp = NULL;
		UuidCreateSequential( &obUUID );
		UuidToString(&obUUID, &sTemp);
		MString strUUIDAsMayaString((const char*)sTemp);
		RpcStringFree(&sTemp);

		setResult( strUUIDAsMayaString );
	}
	return stat;
}

