// 
// rageMayaFindNonPlanarQuads/pluginMain.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#pragma warning(pop)

#include	"system/param.h"

#include "rageMayaFindNonPlanarQuads.h"

#pragma warning(error: 4668)

using namespace rage;

namespace rage
{
	XPARAM(noquits);
}

//////////////////////////////////////////////////////////////

MStatus initializePlugin( MObject obj )
{
//	MGlobal::displayInfo("initializePlugin( MObject obj )");

	// Make it so asserts do not kill Maya
	static const char* gTrueVal = {"true"};
	rage::PARAM_noquits.Set(gTrueVal);

	MFnPlugin plugin( obj, "Alias|Wavefront", "6.5", "Any");

	// Register the command with the system
	plugin.registerCommand("rageMayaFindNonPlanarQuads", rageMayaFindNonPlanarQuads::creator);

	return( MS::kSuccess );
}
//////////////////////////////////////////////////////////////

MStatus uninitializePlugin( MObject obj )
{
	MFnPlugin plugin( obj );
	plugin.deregisterCommand("rageMayaFindNonPlanarQuads");
	return( MS::kSuccess );
}
