// 
// /rageMayaFindNonPlanarQuads.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable stupid STL warnings.
#endif

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include "maya/MObject.h"
#include "maya/MStringArray.h"
#include "maya/MItDependencyNodes.h"
#include "maya/MFnDagNode.h"
#include "maya/MSelectionList.h"
#include "maya/MDagPath.h"
#include "maya/MItMeshPolygon.h"
#include "maya/MPointArray.h"
#include "maya/MGlobal.h"
#include "maya/MArgList.h"
#pragma warning(pop)

#include "vector/vector3.h"
#include "phbound/BoundPolyhedron.h"


#include "rageMayaFindNonPlanarQuads.h"

using namespace rage;

rageMayaFindNonPlanarQuads::rageMayaFindNonPlanarQuads()
{
}

rageMayaFindNonPlanarQuads::~rageMayaFindNonPlanarQuads()
{
}

void * rageMayaFindNonPlanarQuads::creator()
{
	return new rageMayaFindNonPlanarQuads();
}

MStatus rageMayaFindNonPlanarQuads::doIt ( const MArgList & args )
//
//	Description:
//		implements the MEL rageVertexColourDetector command.
//
//	Arguments:
//		args - the argument list that was passes to the command from MEL
//
//	Return Value:
//		MS::kSuccess - command succeeded
//		MS::kFailure - command failed (returning this value will cause the 
//                     MEL script that is being run to terminate unless the
//                     error is caught using a "catch" statement.
//
{
	// printf("Doing it\n");
	MStatus stat = MS::kSuccess;

	//MColor obSearchColor(0.0f,0.0f,0.0f,1.0f);
	vector<MObject> obVectorOfMeshNodeObjects;
	MStringArray astrMeshNames;
	float quadBadness = 0.02f;
	bool bSelectVerts = true;
	//bool bBlendVerts = true;

	for(unsigned a=0; a<args.length(); a++)
	{
		if(args.asString(a) == "-selectFaces")
		{
			bSelectVerts = args.asBool(++a);
		}
		//else if(args.asString(a) == "-blendVerts")
		//{
		//	bBlendVerts = args.asBool(++a);
		//}
		else if(args.asString(a) == "-meshes")
		{
			args.get(++a, astrMeshNames);
		}
		else if(args.asString(a) == "-quadBadness")
		{
			quadBadness = (float)args.asDouble(++a);
		}
		//else if(args.asString(a) == "-g")
		//{
		//	obSearchColor.g = (float)args.asDouble(++a);
		//}
		//else if(args.asString(a) == "-b")
		//{
		//	obSearchColor.b = (float)args.asDouble(++a);
		//}
	}

	Displayf("quadBadness = %f", quadBadness);

	if(astrMeshNames.length() == 0)
	{
		// Not given any meshes, so use every mesh in the scene
		// Displayf("Not given meshes, so using all of them");
		MItDependencyNodes depIterator( MFn::kMesh);
		for ( ; !depIterator.isDone(); depIterator.next() )
		{
			MObject obMeshNode = depIterator.thisNode();
			MFnDagNode obFnMeshNode(obMeshNode);

			// Is it an intermediate node?  Ignore if it is.
			if(!obFnMeshNode.isIntermediateObject())
			{
				// Not an intermediate node, so add it
				obVectorOfMeshNodeObjects.push_back(obMeshNode);
				astrMeshNames.append(obFnMeshNode.fullPathName());
			}
		}
	}
	else
	{
		// Given mesh names to use, so use them
		// Displayf("Given meshes, so using them");
		for(unsigned m=0; m<astrMeshNames.length(); m++)
		{
			// Displayf(astrMeshNames[m].asChar());
			MSelectionList obSelectionListForStringToApiConversions;
			obSelectionListForStringToApiConversions.add(astrMeshNames[m]);
			MDagPath obMeshDagPath;
			obSelectionListForStringToApiConversions.getDagPath(0, obMeshDagPath);
			obVectorOfMeshNodeObjects.push_back(obMeshDagPath.node());
		}
	}

	// Clear selection
	if(bSelectVerts)
	{
		MGlobal::executeCommand("select -clear", false);
	}


	// Got meshes, as MObject, next go through the faces
	for(unsigned m=0; m<obVectorOfMeshNodeObjects.size(); m++)
	{
		// For every face in the mesh
		for(MItMeshPolygon faceIt(obVectorOfMeshNodeObjects[m]);	!faceIt.isDone(); faceIt.next() )
		{
			// Get vertex positions
			MPointArray obAVertexPositions;
			faceIt.getPoints(obAVertexPositions);

			// I only care about Quads, so if I'm not a quad, skip it
			if(obAVertexPositions.length() == 4)
			{
				// Covert obAVertexPositions to an array of Vector3
				Vector3 aobVertexPositions[4];
				for(int v=0; v<4; v++)
				{
					aobVertexPositions[v] = Vector3((float)obAVertexPositions[v].x, (float)obAVertexPositions[v].y, (float)obAVertexPositions[v].z);
				}
				// Test to see if it is planar
				bool bBadQuad = phBoundPolyhedron::TestForBadQuad(0, 1, 2, 3, aobVertexPositions[0], aobVertexPositions[1], aobVertexPositions[2], aobVertexPositions[3], quadBadness);
				if(bBadQuad && bSelectVerts)
				{
					// Select it
					MGlobal::executeCommand("select -add "+ astrMeshNames[m] +".f["+ faceIt.index() +"]", false);
				}
			}
		}
		
	//	// For every face vertex in the mesh
	//	for(MItMeshVertex vertIt(obVectorOfMeshNodeObjects[m]);	!vertIt.isDone(); vertIt.next() )
	//	{
	//		// Does the vertex have a colour?
	//		if(vertIt.hasColor())
	//		{
	//			// I have a colour, get it
	//			MColor obMyColour;
	//			vertIt.getColor(obMyColour);

	//			// Is it the colour I am looking for?
	//			// printf("Comparing [%f %f %f %f] to [%f %f %f %f]\n", obMyColour.r, obMyColour.g, obMyColour.b, obMyColour.a, obSearchColor.r, obSearchColor.g, obSearchColor.b, obSearchColor.a);
	//			if(obMyColour == obSearchColor)
	//			{
	//				// Bingo, found a colour I am looking for
	//				// printf("Bingo, found a colour I am looking for\n");
	//				obVectorOfVerticesMatchingSearchColour.push_back(vertIt.vertex());
	//				MString strSelectionString = MFnDependencyNode(obVectorOfMeshNodeObjects[m]).name();
	//				strSelectionString += ".vtx[";
	//				strSelectionString += vertIt.index();
	//				strSelectionString += "]";
	//				obVectorOfVerticesMatchingSearchColourSelectionStrings.push_back(strSelectionString);
	//			}
	//		}
	//	}
	}

	//if(bSelectVerts)
	//{
	//	// Select the vertices
	//	// printf("Select the vertices\n");
	//	MStatus obStatus;
	//	MSelectionList obSelectionListOfVerts;
	//	for(unsigned v=0; v<obVectorOfVerticesMatchingSearchColourSelectionStrings.size(); v++)
	//	{
	//		// printf("Adding vertex\n");
	//		obStatus = obSelectionListOfVerts.add(obVectorOfVerticesMatchingSearchColourSelectionStrings[v]);
	//		if(obStatus != MS::kSuccess)
	//		{
	//			// Oh dear, unable to add face vert to selection list
	//			printf("Unable to add vertex %s to selection list %s\n", obVectorOfVerticesMatchingSearchColourSelectionStrings[v].asChar(), obStatus.errorString().asChar());
	//		}
	//	}

	//	// Select them
	//	// printf("Selecting\n");
	//	MGlobal::setActiveSelectionList(obSelectionListOfVerts);
	//}

	//if(bBlendVerts)
	//{
	//	// Set the colour of the verts to be the average of those connected to it
	//	vector<MString>  obSelectionStringsOfVertsLeftToBeBlended = obVectorOfVerticesMatchingSearchColourSelectionStrings;

	//	// When blending vertex colours I need to consider the colour of all my surrounding vertices that 
	//	// are NOT the search colour.  This is complicated by the fact that as I change one vertex's colour
	//	// to no longer be the search colour, it will effect the surrounding vertices.
	//	//
	//	// So, the solution is to only consider vertices with 0 surrounding vertices of the search colour,
	//	// then when there are no more of those left, consider vertices with 1 surrounding vertex of the 
	//	// search colour, then when there are no more of those left, consider 2, 3, etc.... until there are
	//	// no vertices of the search colour left.
	//	//
	//	// Of course whenever I fix the colour of one vert, I have to start again as it might effect all the other
	//	// verts
	//	int iNumberOfBadVertsConnectedToBeConsidered = 0;
	//	while(obSelectionStringsOfVertsLeftToBeBlended.size() > 0)
	//	{
	//		// Still verts to fixup, so consider them in order of the number of relationships they have
	//		int iNumberOfVertsFixedInThisPass = 0;
	//		for(unsigned v=0; v<obSelectionStringsOfVertsLeftToBeBlended.size(); v++)
	//		{
	//			// Get the vert as something I can use in the API
	//			MSelectionList obSelectionListForStringToApiConversions;
	//			obSelectionListForStringToApiConversions.add(obSelectionStringsOfVertsLeftToBeBlended[v]);
	//			MDagPath obMeshDagPath;
	//			MObject obMeshVertexComponent;
	//			obSelectionListForStringToApiConversions.getDagPath(0, obMeshDagPath, obMeshVertexComponent);

	//			MItMeshVertex obVertIteratorForThisVert(obMeshDagPath, obMeshVertexComponent);
	//			// printf("Face = %d Vert = %d\n", obFaceVertIteratorForThisFaceVert.faceId(), obFaceVertIteratorForThisFaceVert.vertId());
	//			int iVertId = obVertIteratorForThisVert.index();

	//			// Get the connected verts for this vert
	//			MIntArray obAIConnectedVertIds;
	//			obVertIteratorForThisVert.getConnectedVertices(obAIConnectedVertIds);

	//			// Get all the colours for all this mesh (annoyingly, you can't just get the color of individual verts)
	//			MColorArray obMeshColours;
	//			MFnMesh obFnMeshNode(obMeshDagPath);
	//			obFnMeshNode.getVertexColors(obMeshColours);

	//			// How many of the connected verts are useless colours?
	//			int iNoOfSearchColouredVertsConnected = 0;
	//			float fAverageR = 0.0f;
	//			float fAverageG = 0.0f;
	//			float fAverageB = 0.0f;
	//			float fAverageA = 0.0f;
	//			for(unsigned cv=0; cv<obAIConnectedVertIds.length(); cv++)
	//			{
	//				if(obMeshColours[obAIConnectedVertIds[cv]] == obSearchColor)
	//				{
	//					// Useless
	//					iNoOfSearchColouredVertsConnected++;
	//				}
	//				else
	//				{
	//					// Worth considering
	//					MColor obConnectedColor = obMeshColours[obAIConnectedVertIds[cv]];
	//					fAverageR += obConnectedColor.r;
	//					fAverageG += obConnectedColor.g;
	//					fAverageB += obConnectedColor.b;
	//					fAverageA += obConnectedColor.a;
	//				}
	//			}
	//			// printf("Vertex %d is connected to %d useless verts\n", iVertId, iNoOfSearchColouredVertsConnected);
	//			if(iNoOfSearchColouredVertsConnected <= iNumberOfBadVertsConnectedToBeConsidered)
	//			{
	//				// There are not too many bad verts, so set my colour to be the average of my good connected verts
	//				float fNoOfGoodColouredVertsConnected = (float)obAIConnectedVertIds.length() - (float)iNoOfSearchColouredVertsConnected;
	//				MColor obAverageColour(fAverageR / fNoOfGoodColouredVertsConnected, fAverageG / fNoOfGoodColouredVertsConnected, fAverageB / fNoOfGoodColouredVertsConnected, fAverageA / fNoOfGoodColouredVertsConnected);
	//				obFnMeshNode.setVertexColor(obAverageColour, iVertId);

	//				// I've fixed that vertex, so I can remove it from the list of verts I need to consider
	//				obSelectionStringsOfVertsLeftToBeBlended.erase(obSelectionStringsOfVertsLeftToBeBlended.begin() + v);
	//				iNumberOfVertsFixedInThisPass++;
	//				v--;
	//			}
	//		}
	//		if(iNumberOfVertsFixedInThisPass == 0)
	//		{
	//			iNumberOfBadVertsConnectedToBeConsidered++;
	//		}
	//		else
	//		{
	//			iNumberOfBadVertsConnectedToBeConsidered = 0;
	//		}
	//	}

	//}

	// Since this class is derived off of MPxCommand, you can use the 
	// inherited methods to return values and set error messages
	//
	setResult( "rageMayaFindNonPlanarQuads command executed!\n" );
	_flushall();

	return stat;
}
