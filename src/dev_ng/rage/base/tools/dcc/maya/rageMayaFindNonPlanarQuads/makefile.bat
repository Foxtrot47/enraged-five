call ..\..\libs\rex\setmayapath.bat
set XDEFINE=NT_PLUGIN
set XPROJ=%RAGE_DIR%\base\src
set LIBS=%LIBS% %RAGE_CORE_LIBS% %RAGE_GFX_LIBS% phcore phbound phbullet curve
set TESTER_BASE=maya_plugin
set TESTERS=rageMayaFindNonPlanarQuads
set RESOURCES=pluginMain.cpp rageMayaFindNonPlanarQuads.h
