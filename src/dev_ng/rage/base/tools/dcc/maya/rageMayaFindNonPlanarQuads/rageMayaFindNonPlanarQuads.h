// 
// /rageMayaFindNonPlanarQuads.h
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RAGEMAYAFINDNONPLANARQUADS_H
#define RAGEMAYAFINDNONPLANARQUADS_H

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxCommand.h>
#pragma warning(pop)

#include <vector>

using namespace std;

///////////////////////////////////////////
// rageMayaFindNonPlanarQuads Class //
///////////////////////////////////////////

namespace rage {

class Vector3;

class rageMayaFindNonPlanarQuads : public MPxCommand
{
public:
	rageMayaFindNonPlanarQuads();
	virtual         ~rageMayaFindNonPlanarQuads();

	static  void *	creator();
	virtual MStatus doIt ( const MArgList & args );
	virtual bool  isUndoable () const {return false;}

private:

};

} // end using namespace rage

#endif /* RAGEMAYAFINDNONPLANARQUADS_H */
