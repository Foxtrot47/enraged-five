//
// Copyright (C) 
// 
// File: pluginMain.cpp
//
// Author: Maya Plug-in Wizard 2.0
//

// specify additional libraries to link to
#pragma comment(lib, "Foundation.lib")
#pragma comment(lib, "OpenMaya.lib")
#pragma comment(lib, "OpenMayaUI.lib")
#pragma comment(lib, "OpenGL32.lib")

#include "rageMayaGeoType.h"

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#pragma warning(disable : 4100)
#include <maya/MPxTransformationMatrix.h>
#include <maya/MFnPlugin.h>
#pragma warning(pop)

MStatus initializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is loaded into Maya.  It 
//		registers all of the services that this plug-in provides with 
//		Maya.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{ 
	MStatus   status;
	MFnPlugin plugin( obj, "", "6.0", "Any");
	status = plugin.registerTransform( "rageMayaGeoType", rageMayaGeoType::id, rageMayaGeoType::creator, rageMayaGeoType::initialize, MPxTransformationMatrix::creator, MPxTransformationMatrix::baseTransformationMatrixId );
	if (!status) {
		status.perror("registerNode");
		return status;
	}

	return status;
}

MStatus uninitializePlugin( MObject obj)
//
//	Description:
//		this method is called when the plug-in is unloaded from Maya. It 
//		deregisters all of the services that it was providing.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{
	MStatus   status;
	MFnPlugin plugin( obj );

	status = plugin.deregisterNode( rageMayaGeoType::id );
	if (!status) {
		status.perror("deregisterNode");
		return status;
	}

	return status;
}
