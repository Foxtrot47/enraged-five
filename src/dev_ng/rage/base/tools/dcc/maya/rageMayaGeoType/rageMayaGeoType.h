#ifndef _rageMayaGeoType
#define _rageMayaGeoType
//
// Copyright (C) 
// 
// File: rageMayaGeoType.h
//
// Dependency Graph Node: rageMayaGeoType
//
// Author: Maya Plug-in Wizard 2.0
//

#define REQUIRE_IOSTREAM
#pragma warning(push)
#pragma warning(disable : 4668)
#include <maya/MPxTransform.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#pragma warning(pop)

 
class rageMayaGeoType : public MPxTransform
{
public:
						rageMayaGeoType();
	virtual				~rageMayaGeoType(); 

	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );

	static  void*		creator();
	static  MStatus		initialize();

public:

	// There needs to be a MObject handle declared for each attribute that
	// the node will have.  These handles are needed for getting and setting
	// the values later.
	//
	static  MObject		input;		// Example input attribute
	static  MObject		output;		// Example output attribute


	// The typeid is a unique 32bit indentifier that describes this node.
	// It is used to save and retrieve nodes of this type from the binary
	// file format.  If it is not unique, it will cause file IO problems.
	//
	static	MTypeId		id;
};

#endif
