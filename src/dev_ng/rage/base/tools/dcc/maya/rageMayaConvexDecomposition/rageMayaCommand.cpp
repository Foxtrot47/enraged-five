#include "rageMayaCommand.h"

#include "rageMayaConvexDecomp.h"
#include "rageMayaIntersect.h"

#include "ConvexDecomposition.h"

#include <assert.h>

#pragma warning( push )
#pragma warning( disable: 4668 )
#pragma warning( disable: 4100 )

#include <Maya/MArgDatabase.h>
#include <Maya/MDagPath.h>
#include <Maya/MFloatPointArray.h>
#include <Maya/MGlobal.h>
#include <Maya/MPointArray.h>

#include <Maya/MFnDagNode.h>
#include <Maya/MFnDoubleIndexedComponent.h>
#include <Maya/MFnMesh.h>
#include <Maya/MFnSingleIndexedComponent.h>
#include <Maya/MFnTransform.h>

#include <Maya/MItDependencyNodes.h>
#include <Maya/MItMeshFaceVertex.h>
#include <Maya/MItMeshPolygon.h>
#include <Maya/MItMeshVertex.h>
#include <Maya/MItSelectionList.h>

#pragma warning( pop )

#include <algorithm>
#include <vector>
#include <string>
#include <map>

using namespace ConvexDecomposition;
using namespace rage;

static void SetBoundsMaterials( CreatedList& bounds, const MString& material )
{
	int length = int( bounds.size() );
	for ( int i = 0; i < length; ++i )
	{
		MObject& object = bounds[ i ];

		MDagPath path;
		MDagPath::getAPathTo( object, path );

		MString name = path.fullPathName();

		if ( material != "" )
		{
			char command[ 256 ];
			sprintf( command, "select -r %s; addAttr -longName phbound -dt \"string\"; setAttr \"%s.phbound\" -type \"string\" %s", 
				name.asChar(), name.asChar(), material.asChar() );

			MGlobal::executeCommand( command, false );
		}
	}
}

static void SetHullsMaterials( CreatedList& hulls, const MString& material )
{
	int length = int( hulls.size() );
	for ( int i = 0; i < length; ++i )
	{
		MObject& object = hulls[ i ];

		MDagPath path;
		MDagPath::getAPathTo( object, path );

		MString name = path.fullPathName();

		if ( material != "" )
		{
			char command[ 256 ];
			sprintf( command, "select -r %s; hyperShade -assign %s;", name.asChar(), material.asChar() );

			MGlobal::executeCommand( command, false );
		}
	}
}

ConvexDecompCommand::ConvexDecompCommand()
:	m_Modifier ( NULL )
{
}

ConvexDecompCommand::~ConvexDecompCommand()
{
	delete m_Modifier;
}

MStatus ConvexDecompCommand::redoIt()
{
	if ( m_Modifier )
	{
		delete m_Modifier;
	}

	m_Modifier = new MDagModifier;

	ParentList parents;

	CreatedList bounds;
	CreatedList hulls;

	int selectionCount = int( m_VertInfoList.size() );

	if ( m_Rule.m_AsOneGroup )
	{
		// count the amount of triangles in all the nodes

		MPointArray positions;
		MIntArray indices;

		unsigned int currMaxIndex = 0;
		unsigned int maxIndex = 0;

		for ( int i = 0; i < selectionCount; ++i )
		{
			MPointArray& vertPositions = m_VertInfoList[ i ]->VertPositions;

			int positionCount = int( vertPositions.length() );
			for ( int positionIndex = 0; positionIndex < positionCount; ++positionIndex )
			{
				positions.append( vertPositions[ positionIndex ] );
			}
	
			MIntArray& vertIndices = m_VertInfoList[ i ]->VertIndices;

			int indexCount = int( vertIndices.length() );
			for ( int indexIndex = 0; indexIndex < indexCount; ++indexIndex )
			{
				unsigned int index = maxIndex + vertIndices[ i ];

				indices.append( index );

				currMaxIndex = max( index, maxIndex );
			}

			maxIndex = currMaxIndex;
		}


		// add all the triangles in the nodes to the lists,
		// making sure that the indices are correctly represented.

		DecompDesc desc;
		desc.mVcount = int( positions.length() );
		desc.mVertices = new double[ positions.length() * 3 ];

		double* vertIt = const_cast< double* >( desc.mVertices );
		for ( unsigned int i = 0; i < positions.length(); ++i )
		{
			*vertIt++ = positions[ i ].x;
			*vertIt++ = positions[ i ].y;
			*vertIt++ = positions[ i ].z;
		}

		desc.mTcount = int( indices.length() / 3 );
		desc.mIndices = new unsigned int[ indices.length() ];
		for ( unsigned int i = 0; i < indices.length(); ++i )
		{
			desc.mIndices[ i ] = indices[ i ];
		}

		desc.mDepth = m_Rule.m_DecompositionDepth;
		desc.mCpercent = m_Rule.m_ConcaveThreshold;
		desc.mPpercent = m_Rule.m_MergeThreshold;
		desc.mMaxVertices = m_Rule.m_MaxVertices;
		desc.mSkinWidth = m_Rule.m_SkinWidth;

		//MObject parent = m_Modifier->createNode( "transform" );
		//MFnTransform transform( parent );
		//transform.setName( "group_bound" );

		//MDagPath path;
		//MDagPath::getAPathTo( transform.object(), path );
		//path.pop();

		//MFnDagNode parentdn ( path );

		//MTransformationMatrix tm;

		ConvexDecomp cb ( "bounds", m_Rule, m_Modifier );
		desc.mCallback = &cb;

		// send off the whole group to be processed.

		performConvexDecomposition( desc );
	}
	else // seperate groups
	{
		typedef std::map< std::string, int > BoundNameList;
		BoundNameList names;

		for ( int selectionIndex = 0; selectionIndex < selectionCount; ++selectionIndex )
		{
			MPointArray& positions = m_VertInfoList[ selectionIndex ]->VertPositions;

			DecompDesc desc;
			desc.mVcount = positions.length();
			desc.mVertices = new double[ positions.length() * 3 ];
			
			double* vertIt = const_cast< double* >( desc.mVertices );

			for ( unsigned int i = 0; i < desc.mVcount; ++i )
			{
				*vertIt++ = positions[ i ].x;
				*vertIt++ = positions[ i ].y;
				*vertIt++ = positions[ i ].z;
			}

			MIntArray& indices = m_VertInfoList[ selectionIndex ]->VertIndices;

			desc.mTcount = indices.length() / 3;
			desc.mIndices = new unsigned int[ indices.length() ];

			unsigned int* indicesIt = desc.mIndices;

			for ( unsigned int i = 0; i < indices.length(); ++i )
			{
				*indicesIt++ = indices[ i ];
			}

			desc.mDepth = m_Rule.m_DecompositionDepth;
			desc.mCpercent = m_Rule.m_ConcaveThreshold;
			desc.mPpercent = m_Rule.m_MergeThreshold;
			desc.mMaxVertices = m_Rule.m_MaxVertices;
			desc.mSkinWidth = m_Rule.m_SkinWidth;

			// preserve the nodes parent
			
			/*
			MDagPath path;
			MDagPath::getAPathTo( node.node(), path );
			path.pop();

			// this is the parent of the current node...
			// if we want to group it, create a mesh and 
			// and bounds node
			MFnDagNode parentdn ( path );

			MObject& parentNode = MObject::kNullObj;

			if ( m_Rule.m_GroupBoundsAndMesh )
			{
				MObject meshobj = m_Modifier->createNode( "transform", path.node() );
				MFnTransform mgroup ( meshobj );
				mgroup.setName( MString( "mesh_" ) + mesh.name() );

				m_Modifier->reparentNode( node.node(), meshobj );

				if ( m_Rule.m_GroupBounds )
				{
					MObject boundsobj = m_Modifier->createNode( "transform", meshobj );
					MFnTransform bgroup ( boundsobj );
					bgroup.setName( MString( "bounds_" ) + mesh.name() );

					parentNode = boundsobj;
				}
				else
				{
					parentNode = meshobj;
				}
			}
			else
			if ( m_Rule.m_GroupBounds )
			{
				MObject obj = m_Modifier->createNode( "transform", path.node() );
				MFnTransform bgroup ( obj );
				bgroup.setName( MString( "bounds_" ) + mesh.name() );

				parentNode = obj;
			}

			*/

			// find a name for this bound

			MFnMesh node ( m_VertInfoList[ selectionIndex ]->DagPath.node() );
	
			BoundNameList::iterator findIt = names.find( node.name().asChar() );

			MString name = "";

			if ( findIt != names.end() )
			{
				findIt->second += 1;

				char buf[ 128 ];
				sprintf( buf, "%s_%d", node.name().asChar(), findIt->second );

				name = MString( buf );
			}
			else
			{
				names.insert( std::make_pair( node.name().asChar(), 0 ) );

				char buf[ 128 ];
				sprintf( buf, "%s_%d", node.name().asChar(), 0 );

				name = MString( buf );
			}

			ConvexDecomp cb ( name, m_Rule, m_Modifier );
			desc.mCallback = &cb;

			performConvexDecomposition( desc );

			delete[] desc.mIndices;
			delete[] desc.mVertices;
		}
	}	

	m_Modifier->doIt();

	SetBoundsMaterials( bounds, m_Rule.m_MaterialName );
	SetHullsMaterials( hulls, m_Rule.m_MaterialName );
/*
	int numParents = int( parents.size() );
	for ( int i = 0; i < numParents; ++i )
	{
		MObject& child = parents[ i ].first;
		MObject& parent = parents[ i ].second;

		MDagPath childPath;
		MDagPath::getAPathTo( child, childPath );

		MString childName = childPath.fullPathName();

		MDagPath parentPath;
		MDagPath::getAPathTo( parent, parentPath );

		MString parentName = parentPath.fullPathName();

		char buf[ 128 ];
		sprintf( buf, "parent %s %s;", childName.asChar(), parentName.asChar() );

		MGlobal::executeCommand( buf, false );
	}
*/
	return MS::kSuccess;
}

MStatus ConvexDecompCommand::undoIt()
{
	m_Modifier->undoIt();

	return MS::kSuccess;
}

MStatus	ConvexDecompCommand::doIt( const MArgList& args ) 
{ 
	// save the rules and the selection, that's all i need...

	MStatus status;
	MArgDatabase db ( syntax(), args, &status );

	m_Rule.SetFromDatabase( db );

	InitializeFromSelection();

	// rather than just go through the selection list,
	// we go through the 
	//MGlobal::getActiveSelectionList( m_SelectionList );

	return redoIt();
}

MSyntax ConvexDecompCommand::createSyntax()
{
	MSyntax syntax;
	syntax.addFlag( "-d", "-split_depth", MSyntax::kUnsigned );
	syntax.addFlag( "-c", "-concave_theshold", MSyntax::kDouble );
	syntax.addFlag( "-p", "-merge_threshold", MSyntax::kDouble );
	syntax.addFlag( "-v", "-max_vertices", MSyntax::kUnsigned );
	syntax.addFlag( "-s", "-shrink_ratio", MSyntax::kDouble );
	syntax.addFlag( "-fb", "-fit_box" );
	syntax.addFlag( "-obb", "-use_obb" );
	syntax.addFlag( "-fs", "-fit_sphere" );
	syntax.addFlag( "-fc", "-fit_capsule" );
	syntax.addFlag( "-fch", "-fit_convex" );
	syntax.addFlag( "-bp", "-box_percent", MSyntax::kDouble );
	syntax.addFlag( "-sp", "-sphere_percent", MSyntax::kDouble );
	syntax.addFlag( "-cp", "-capsule_percent", MSyntax::kDouble );
	syntax.addFlag( "-m", "-material", MSyntax::kString );
	syntax.addFlag( "-gbm", "-group_bounds_mesh" );
	syntax.addFlag( "-gb", "-group_bounds" );
	syntax.addFlag( "-h", "-keep_hierarchy" );
	syntax.addFlag( "-g", "-as_one_group" );

	syntax.enableQuery( false );
	syntax.enableEdit( false );

	return syntax;
}

void ConvexDecompCommand::InitializeFromSelection()
{
	MStatus status = MS::kSuccess;

	MObject multiVertexComponent;
	MObject singleVertexComponent;

	MSelectionList activeSelectionList;
	MGlobal::getActiveSelectionList( activeSelectionList );

	if ( activeSelectionList.isEmpty() )
	{
		return;
	}

	int activeSelectedItemCount = activeSelectionList.length();
	MSelectionList operationSelectionList;
	for ( int selectionIndex = 0; selectionIndex < activeSelectedItemCount; ++selectionIndex )
	{
		MDagPath tempdp;
		MObject tempobj;
		
		status = activeSelectionList.getDagPath( selectionIndex, tempdp, tempobj );

		if ( !tempobj.isNull() )
		{
			MFn::Type type = tempobj.apiType();

			if ( MFn::kMeshPolygonComponent == type )
			{
				// polygons are selected to convert the polygon component 
				// selection to a face vertex selection

				MIntArray polyIndexArray;
				MIntArray faceVertexIndexArray;

				MItMeshPolygon polyIt ( tempdp, tempobj );

				if ( MS::kSuccess == status )
				{
					for ( polyIt.reset(); !polyIt.isDone(); polyIt.next() )
					{
						MIntArray polyVertIndices;
						polyIt.getVertices( polyVertIndices );

						for ( unsigned int i = 0; i < polyVertIndices.length(); ++i )
						{
							polyIndexArray.append( polyIt.index() );
							faceVertexIndexArray.append( polyVertIndices[ i ] );
						}
					}

					MFnDoubleIndexedComponent dicmp;
					dicmp.create( MFn::kMeshVtxFaceComponent, &status );
					
					status = dicmp.addElements( faceVertexIndexArray, polyIndexArray );

					operationSelectionList.add( tempdp, dicmp.object() );
				}
			}
			else
			{
				operationSelectionList.add( tempdp, tempobj );
			}
		}
		else
		{
			MFnSingleIndexedComponent sicmp;
			sicmp.create( MFn::kMeshVertComponent, &status );

			MObject tempnode = tempdp.node();
			MItMeshVertex vertIt ( tempnode, &status );

			if ( MS::kSuccess == status )
			{
				 for ( vertIt.reset(); !vertIt.isDone(); vertIt.next() )
				 {
					 sicmp.addElement( vertIt.index() );
				 }

				 operationSelectionList.add( tempdp, sicmp.object() );
			}
		}
}	

	// gather information from meshes that have vertex 
	// face components selected

	MItSelectionList faceVertexComponentIt ( operationSelectionList, MFn::kMeshVtxFaceComponent, &status );
	for ( faceVertexComponentIt.reset(); !faceVertexComponentIt.isDone(); faceVertexComponentIt.next() )
	{
		MDagPath meshdp;
		faceVertexComponentIt.getDagPath( meshdp, multiVertexComponent );

		VertexInfo* infoNode = NULL;
		
		VertexInfoList::iterator findIt = std::find( m_VertInfoList.begin(), m_VertInfoList.end(), meshdp );
		if ( findIt != m_VertInfoList.end() )
		{
			infoNode = (*findIt);
		}
		else
		{
			infoNode = new VertexInfo( meshdp );
			m_VertInfoList.push_back( infoNode );
		}

		if ( !multiVertexComponent.isNull() )
		{
			MFnMesh mesh ( meshdp );

			for ( MItMeshFaceVertex faceVertIt ( meshdp, multiVertexComponent ); !faceVertIt.isDone(); faceVertIt.next() )
			{
				infoNode->VertIndices.append( faceVertIt.vertId() );

				MPoint position = faceVertIt.position( MSpace::kWorld );
				infoNode->VertPositions.append( position );
			}
		}
	}

	for ( MItSelectionList vertCompIt ( operationSelectionList, MFn::kMeshVertComponent ); !vertCompIt.isDone(); vertCompIt.next() )
	{
		MDagPath meshdp;
		vertCompIt.getDagPath( meshdp, multiVertexComponent );

		VertexInfo* infoNode = NULL;
		
		VertexInfoList::iterator findIt = std::find( m_VertInfoList.begin(), m_VertInfoList.end(), meshdp );
		if ( findIt != m_VertInfoList.end() )
		{
			infoNode = (*findIt);
		}
		else
		{
			infoNode = new VertexInfo( meshdp );
			m_VertInfoList.push_back( infoNode );
		}

		if ( !multiVertexComponent.isNull() )
		{
			MFnMesh mesh ( meshdp );

			for ( MItMeshVertex vertIt ( meshdp, multiVertexComponent); !vertIt.isDone(); vertIt.next() )
			{
				infoNode->VertIndices.append( vertIt.index() );

				MPoint position = vertIt.position( MSpace::kWorld );
				infoNode->VertPositions.append( position );
			}
		}
	}
}

SelectIntersectBoundVolumeCommand::SelectIntersectBoundVolumeCommand()
{

}

SelectIntersectBoundVolumeCommand::~SelectIntersectBoundVolumeCommand()
{

}

MStatus SelectIntersectBoundVolumeCommand::doIt( const MArgList& )
{
	MFnDependencyNodeList bounds;

	// get all the rage bounding volumes... this doesn't include the bounding meshes

	MItDependencyNodes dnIt;

	while ( !dnIt.isDone() )
	{
		MFnDependencyNode* node = new MFnDependencyNode( dnIt.thisNode() );

		MTypeId typeId = node->typeId();
		unsigned int id = typeId.id();

		if ( id == PH_BOUND_BOX_NODE_ID.id() ||
			 id == PH_BOUND_CAPSULE_NODE_ID.id() ||
			 id == PH_BOUND_SPHERE_NODE_ID.id() )
		{
			bounds.push_back( node );
		}

		dnIt.next();
	}

	// send the bounding meshes off the be tested...

	MFnDependencyNodeList intersects;
	FindIntersectingBounds( intersects, bounds );

	// and select them...


	MGlobal::executeCommand( "select -clear;", false );

	for ( MFnDependencyNodeList::iterator it = intersects.begin(); it != intersects.end(); ++it )
	{
		MObject object = ( *it )->object();
		MFnDagNode dnode ( object );

		MDagPath dpath;
		MDagPath::getAPathTo( dnode.object(), dpath );
		//dnode.getPath( dpath );

		dpath.pop();

		MFnTransform parent ( dpath.node() );

		MString name = parent.name();

		MString command = "select -add ";
		command += name;

		MGlobal::executeCommand( command, false );
	}


	return MS::kSuccess;
}
