#pragma warning( push )
#pragma warning( disable: 4668 )
#pragma warning( disable: 4100 )

#include <Maya/MSceneMessage.h>

#include <Maya/MFnPlugin.h>

#pragma warning( pop )

#include "rageMayaCommand.h"

#pragma comment( lib, "Foundation.lib" )
#pragma comment( lib, "OpenMaya.lib" )
#pragma comment( lib, "OpenMayaUI.lib" )

void killPlugin( void* )
{

} 

MStatus __declspec( dllexport ) initializePlugin( MObject obj )
{
	MFnPlugin plugin( obj, "RAGE Convex Decomposition", RAGE_RELEASE_STRING " - " __DATE__ " - " __TIME__, "Any" );

	//rage::sysParam::Init( 0, NULL );

	MStatus status = plugin.registerCommand( 
		"rageConvexDecomposition", 
		&rage::ConvexDecompCommand::creator,
		&rage::ConvexDecompCommand::createSyntax  );

	if ( MS::kSuccess != status )
	{
		return status;
	}

	status = plugin.registerCommand(
		"rageSelectIntersectingBounds", 
		&rage::SelectIntersectBoundVolumeCommand::creator );
	
	if ( MS::kSuccess != status )
	{
		return status;
	}

	MSceneMessage::addCallback( MSceneMessage::kMayaExiting, killPlugin );

	return status;

}

MStatus __declspec( dllexport ) uninitializePlugin( MObject obj )
{
	MFnPlugin plugin ( obj );

	MStatus status = plugin.deregisterCommand( "rageConvexDecomposition" );
	if ( !status )
	{
		status.perror( "deregisterCommand" );
	}

	return status;
}
