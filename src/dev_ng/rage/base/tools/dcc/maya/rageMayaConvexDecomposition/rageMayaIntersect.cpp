#include "rageMayaIntersect.h"

#include "rageMayaConvexDecomp.h"

#include "vector/matrix34.h"

#pragma warning( push )
#pragma warning( disable: 4668 )
#pragma warning( disable: 4100 )

#include <Maya/MDagPath.h>
#include <Maya/MMatrix.h>
#include <Maya/MObject.h>
#include <Maya/MTransformationMatrix.h>
#include <Maya/MTypeId.h>
#include <Maya/MVector.h>

#include <Maya/MFnDagNode.h>

#pragma warning( pop )

#include "float_math.h"

#include <math.h>

#include <algorithm>

using namespace ConvexDecomposition;
using namespace rage;

static void GetSphere( MFnDagNode& sphere, MVector& center, double& radius )
{
	MDagPath dp;
	MDagPath::getAPathTo( sphere.object(), dp );
	//sphere.getPath( dp );
	dp.pop(); // get the parent

	MTransformationMatrix tm ( dp.inclusiveMatrix() );

	double scale[ 3 ];
	tm.getScale( scale, MSpace::kWorld );

	radius = scale[ 0 ];

	center = tm.getTranslation( MSpace::kWorld );
}

static void GetBox( MFnDagNode& box, MVector& center, MVector* axis, double* extents )
{
	MDagPath dp;
	MDagPath::getAPathTo( box.object(), dp );
	//box.getPath( dp );
	dp.pop(); // get the parent
 
 	MTransformationMatrix tm ( dp.inclusiveMatrix() );
 
 	double scale[ 3 ];
 	tm.getScale( scale, MSpace::kWorld );
 
 	extents[ 0 ] = scale[ 0 ];
 	extents[ 1 ] = scale[ 1 ];
 	extents[ 2 ] = scale[ 2 ];
 
 	center = tm.getTranslation( MSpace::kWorld );
 
 	MMatrix mat = tm.asMatrix();
 
 	axis[ 0 ] = MVector( mat( 0, 0 ), mat( 0, 1 ), mat( 0, 2 ) );
 	axis[ 1 ] = MVector( mat( 1, 0 ), mat( 1, 1 ), mat( 1, 2 ) );
 	axis[ 2 ] = MVector( mat( 2, 0 ), mat( 2, 1 ), mat( 2, 2 ) );
}

static void GetCapsule( MFnDagNode& capsule, MVector& center, MVector& direction, double& extent, double& radius )
{
	MDagPath dp;
	MDagPath::getAPathTo( capsule.object(), dp );
	//capsule.getPath( dp );
	dp.pop(); // get the parent

	MTransformationMatrix tm ( dp.inclusiveMatrix() );

	double scale[ 3 ];
	tm.getScale( scale, MSpace::kWorld );

	radius = scale[ 0 ];
	extent = scale[ 2 ];

	center = tm.getTranslation( MSpace::kWorld );

	MMatrix mat = tm.asMatrix();

	MVector dir ( 1, 0, 0 );
	direction = dir.transformAsNormal( mat );
}

static double Dot( const MVector& v0, const MVector& v1 )
{
	return v0.x * v1.x + v0.y * v1.y + v0.z * v1.z;
}

static bool IntersectBoxBox( MFnDagNode& box1, MFnDagNode& box2 )
{
	MVector center1;
	MVector axis1[ 3 ];
	double extents1[ 3 ];
	GetBox( box1, center1, axis1, extents1 );
	extents1[ 0 ] *= 0.5f;
	extents1[ 1 ] *= 0.5f;
	extents1[ 2 ] *= 0.5f;

	axis1[ 0 ].normalize();
	axis1[ 1 ].normalize();
	axis1[ 2 ].normalize();

	MVector center2;
	MVector axis2[ 3 ];
	double extents2[ 3 ];
	GetBox( box2, center2, axis2, extents2 );
	extents2[ 0 ] *= 0.5f;
	extents2[ 1 ] *= 0.5f;
	extents2[ 2 ] *= 0.5f;

	axis2[ 0 ].normalize();
	axis2[ 1 ].normalize();
	axis2[ 2 ].normalize();

	const double cutoff = 1.0 - 1e-08;
	bool existsParallelPair = false;
	int i;

	// convenience variables
	const MVector* a = axis1;
	const MVector* b = axis2; 
	const double* ea = extents1;
	const double* eb = extents2;

	// compute difference of box centers, D = C1-C0
	MVector d = center2 - center1;

	double c[3][3];     // matrix C = A^T B, c_{ij} = Dot(A_i,B_j)
	double absc[3][3];  // |c_{ij}|
	double ad[3];        // Dot(A_i,D)
	double r0, r1, r;   // interval radii and distance between centers
	double r01;           // = R0 + R1

	// axis C0+t*A0
	for (i = 0; i < 3; i++)
	{
		c[0][i] = Dot( a[0], b[i]);
		absc[0][i] = abs(c[0][i]);
		if (absc[0][i] > cutoff)
		{
			existsParallelPair = true;
		}
	}
	ad[0] = Dot( a[0], d );
	r = abs(ad[0]);
	r1 = eb[0]*absc[0][0]+eb[1]*absc[0][1]+eb[2]*absc[0][2];
	r01 = ea[0] + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*A1
	for (i = 0; i < 3; i++)
	{
		c[1][i] = Dot( a[1], b[i]);
		absc[1][i] = abs(c[1][i]);
		if (absc[1][i] > cutoff)
		{
			existsParallelPair = true;
		}
	}
	ad[1] = Dot( a[1], d);
	r = abs(ad[1]);
	r1 = eb[0]*absc[1][0]+eb[1]*absc[1][1]+eb[2]*absc[1][2];
	r01 = ea[1] + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*A2
	for (i = 0; i < 3; i++)
	{
		c[2][i] = Dot( a[2], b[i]);
		absc[2][i] = abs(c[2][i]);
		if (absc[2][i] > cutoff)
		{
			existsParallelPair = true;
		}
	}
	ad[2] = Dot( a[2], d);
	r = abs(ad[2]);
	r1 = eb[0]*absc[2][0]+eb[1]*absc[2][1]+eb[2]*absc[2][2];
	r01 = ea[2] + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*B0
	r = abs(Dot( b[0], d));
	r0 = ea[0]*absc[0][0]+ea[1]*absc[1][0]+ea[2]*absc[2][0];
	r01 = r0 + eb[0];
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*B1
	r = abs(Dot( b[1], d));
	r0 = ea[0]*absc[0][1]+ea[1]*absc[1][1]+ea[2]*absc[2][1];
	r01 = r0 + eb[1];
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*B2
	r = abs(Dot( b[2], d));
	r0 = ea[0]*absc[0][2]+ea[1]*absc[1][2]+ea[2]*absc[2][2];
	r01 = r0 + eb[2];
	if (r > r01)
	{
		return false;
	}

	// At least one pair of box axes was parallel, so the separation is
	// effectively in 2D where checking the "edge" normals is sufficient for
	// the separation of the boxes.
	if (existsParallelPair)
	{
		return true;
	}

	// axis C0+t*A0xB0
	r = abs(ad[2]*c[1][0]-ad[1]*c[2][0]);
	r0 = ea[1]*absc[2][0] + ea[2]*absc[1][0];
	r1 = eb[1]*absc[0][2] + eb[2]*absc[0][1];
	r01 = r0 + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*A0xB1
	r = abs(ad[2]*c[1][1]-ad[1]*c[2][1]);
	r0 = ea[1]*absc[2][1] + ea[2]*absc[1][1];
	r1 = eb[0]*absc[0][2] + eb[2]*absc[0][0];
	r01 = r0 + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*A0xB2
	r = abs(ad[2]*c[1][2]-ad[1]*c[2][2]);
	r0 = ea[1]*absc[2][2] + ea[2]*absc[1][2];
	r1 = eb[0]*absc[0][1] + eb[1]*absc[0][0];
	r01 = r0 + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*A1xB0
	r = abs(ad[0]*c[2][0]-ad[2]*c[0][0]);
	r0 = ea[0]*absc[2][0] + ea[2]*absc[0][0];
	r1 = eb[1]*absc[1][2] + eb[2]*absc[1][1];
	r01 = r0 + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*A1xB1
	r = abs(ad[0]*c[2][1]-ad[2]*c[0][1]);
	r0 = ea[0]*absc[2][1] + ea[2]*absc[0][1];
	r1 = eb[0]*absc[1][2] + eb[2]*absc[1][0];
	r01 = r0 + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*A1xB2
	r = abs(ad[0]*c[2][2]-ad[2]*c[0][2]);
	r0 = ea[0]*absc[2][2] + ea[2]*absc[0][2];
	r1 = eb[0]*absc[1][1] + eb[1]*absc[1][0];
	r01 = r0 + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*A2xB0
	r = abs(ad[1]*c[0][0]-ad[0]*c[1][0]);
	r0 = ea[0]*absc[1][0] + ea[1]*absc[0][0];
	r1 = eb[1]*absc[2][2] + eb[2]*absc[2][1];
	r01 = r0 + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*A2xB1
	r = abs(ad[1]*c[0][1]-ad[0]*c[1][1]);
	r0 = ea[0]*absc[1][1] + ea[1]*absc[0][1];
	r1 = eb[0]*absc[2][2] + eb[2]*absc[2][0];
	r01 = r0 + r1;
	if (r > r01)
	{
		return false;
	}

	// axis C0+t*A2xB2
	r = abs(ad[1]*c[0][2]-ad[0]*c[1][2]);
	r0 = ea[0]*absc[1][2] + ea[1]*absc[0][2];
	r1 = eb[0]*absc[2][1] + eb[1]*absc[2][0];
	r01 = r0 + r1;
	if (r > r01)
	{
		return false;
	}

	return true;
}

static bool IntersectBoxCapsule( MFnDagNode& box, MFnDagNode& capsule )
{
	MVector boxCenter;
	MVector boxAxis[ 3 ];
	double boxExtents[ 3 ];
	GetBox( box, boxCenter, boxAxis, boxExtents );
	boxAxis[ 0 ].normalize();
	boxAxis[ 1 ].normalize();
	boxAxis[ 2 ].normalize();

	// check up on the box extents
	boxExtents[ 0 ] *= 0.5;
	boxExtents[ 1 ] *= 0.5;
	boxExtents[ 2 ] *= 0.5;

	MVector capsuleCenter, capsuleDirection;
	double capsuleExtent, capsuleRadius;
	GetCapsule( capsule, capsuleCenter, capsuleDirection, capsuleExtent, capsuleRadius );

	MVector diff = capsuleCenter - boxCenter;

	double ax = abs(Dot(diff, boxAxis[0]));
	double ay = abs(Dot(diff, boxAxis[1]));
	double az = abs(Dot(diff, boxAxis[2]));
	double dx = ax - boxExtents[0];
	double dy = ay - boxExtents[1];
	double dz = az - boxExtents[2];

	if (ax <= boxExtents[0])
	{
		if (ay <= boxExtents[1])
		{
			if (az <= boxExtents[2])
			{
				// sphere center inside box
				return true;
			}
			else
			{
				// potential sphere-face intersection with face z
				return dz <= capsuleRadius;
			}
		}
		else
		{
			if (az <= boxExtents[2])
			{
				// potential sphere-face intersection with face y
				return dy <= capsuleRadius;
			}
			else
			{
				// potential sphere-edge intersection with edge formed
				// by faces y and z
				double sqr = capsuleRadius*capsuleRadius;
				return dy*dy + dz*dz <= sqr;
			}
		}
	}
	else
	{
		if (ay <= boxExtents[1])
		{
			if (az <= boxExtents[2])
			{
				// potential sphere-face intersection with face x
				return dx <= capsuleRadius;
			}
			else
			{
				// potential sphere-edge intersection with edge formed
				// by faces x and z
				double sqr = capsuleRadius*capsuleRadius;
				return dx*dx + dz*dz <= sqr;
			}
		}
		else
		{
			if (az <= boxExtents[2])
			{
				// potential sphere-edge intersection with edge formed
				// by faces x and y
				double sqr = capsuleRadius*capsuleRadius;
				return dx*dx + dy*dy <= sqr;
			}
			else
			{
				// potential sphere-vertex intersection at corner formed
				// by faces x,y,z
				double sqr = capsuleRadius*capsuleRadius;
				return dx*dx + dy*dy + dz*dz <= sqr;
			}
		}
	}
}

static bool IntersectBoxSphere( MFnDagNode& box, MFnDagNode& sphere )
{
	MVector boxCenter;
	MVector boxAxis[ 3 ];
	double boxExtents[ 3 ];
	GetBox( box, boxCenter, boxAxis, boxExtents );
	boxAxis[ 0 ].normalize();
	boxAxis[ 1 ].normalize();
	boxAxis[ 2 ].normalize();

	// check up on the box extents
	boxExtents[ 0 ] *= 0.5;
	boxExtents[ 1 ] *= 0.5;
	boxExtents[ 2 ] *= 0.5;

	MVector sphereCenter;
	double sphereRadius;
	GetSphere( sphere, sphereCenter, sphereRadius );

	MVector diff = sphereCenter - boxCenter;

	double ax = abs(Dot(diff, boxAxis[0]));
	double ay = abs(Dot(diff, boxAxis[1]));
	double az = abs(Dot(diff, boxAxis[2]));
	double dx = ax - boxExtents[0];
	double dy = ay - boxExtents[1];
	double dz = az - boxExtents[2];

	if (ax <= boxExtents[0])
	{
		if (ay <= boxExtents[1])
		{
			if (az <= boxExtents[2])
			{
				// sphere center inside box
				return true;
			}
			else
			{
				// potential sphere-face intersection with face z
				return dz <= sphereRadius;
			}
		}
		else
		{
			if (az <= boxExtents[2])
			{
				// potential sphere-face intersection with face y
				return dy <= sphereRadius;
			}
			else
			{
				// potential sphere-edge intersection with edge formed
				// by faces y and z
				double sqr = sphereRadius*sphereRadius;
				return dy*dy + dz*dz <= sqr;
			}
		}
	}
	else
	{
		if (ay <= boxExtents[1])
		{
			if (az <= boxExtents[2])
			{
				// potential sphere-face intersection with face x
				return dx <= sphereRadius;
			}
			else
			{
				// potential sphere-edge intersection with edge formed
				// by faces x and z
				double sqr = sphereRadius*sphereRadius;
				return dx*dx + dz*dz <= sqr;
			}
		}
		else
		{
			if (az <= boxExtents[2])
			{
				// potential sphere-edge intersection with edge formed
				// by faces x and y
				double sqr = sphereRadius*sphereRadius;
				return dx*dx + dy*dy <= sqr;
			}
			else
			{
				// potential sphere-vertex intersection at corner formed
				// by faces x,y,z
				double sqr = sphereRadius*sphereRadius;
				return dx*dx + dy*dy + dz*dz <= sqr;
			}
		}
	}
}

static bool IntersectCapsuleCapsule( MFnDagNode& capsule1, MFnDagNode& capsule2 )
{
	MVector center1, direction1;
	double extent1, radius1;
	GetCapsule( capsule1, center1, direction1, extent1, radius1 );

	MVector center2, direction2;
	double extent2, radius2;
	GetCapsule( capsule2, center2, direction2, extent2, radius2 );


	MVector diff = center1 - center2;
	double a01 = - Dot( direction1,  direction2 );
	double b0 = Dot( diff, direction1 );
	double b1 = -Dot( diff, direction2 );
	double c = diff.length() * diff.length();
	double det = abs( 1.0 - a01 * a01 );
	double s0, s1, sqrDist, extDet0, extDet1, tmpS0, tmpS1;

	if ( det >= 1e-08 )
	{
		// segments are not parallel
		s0 = a01*b1-b0;
		s1 = a01*b0-b1;
		extDet0 = extent1*det;
		extDet1 = extent2*det;

		if (s0 >= -extDet0)
		{
			if (s0 <= extDet0)
			{
				if (s1 >= -extDet1)
				{
					if (s1 <= extDet1)  // region 0 (interior)
					{
						// minimum at two interior points of 3D lines
						double invDet = (1.0)/det;
						s0 *= invDet;
						s1 *= invDet;
						sqrDist = s0*(s0+a01*s1+(2.0)*b0) +
							s1*(a01*s0+s1+(2.0)*b1)+c;
					}
					else  // region 3 (side)
					{
						s1 = extent2;
						tmpS0 = -(a01*s1+b0);
						if (tmpS0 < -extent1)
						{
							s0 = -extent1;
							sqrDist = s0*(s0-(2.0)*tmpS0) +
								s1*(s1+(2.0)*b1)+c;
						}
						else if (tmpS0 <= extent1)
						{
							s0 = tmpS0;
							sqrDist = -s0*s0+s1*(s1+(2.0)*b1)+c;
						}
						else
						{
							s0 = extent1;
							sqrDist = s0*(s0-(2.0)*tmpS0) +
								s1*(s1+(2.0)*b1)+c;
						}
					}
				}
				else  // region 7 (side)
				{
					s1 = -extent2;
					tmpS0 = -(a01*s1+b0);
					if (tmpS0 < -extent1)
					{
						s0 = -extent1;
						sqrDist = s0*(s0-(2.0)*tmpS0) +
							s1*(s1+(2.0)*b1)+c;
					}
					else if (tmpS0 <= extent1)
					{
						s0 = tmpS0;
						sqrDist = -s0*s0+s1*(s1+(2.0)*b1)+c;
					}
					else
					{
						s0 = extent1;
						sqrDist = s0*(s0-(2.0)*tmpS0) +
							s1*(s1+(2.0)*b1)+c;
					}
				}
			}
			else
			{
				if (s1 >= -extDet1)
				{
					if (s1 <= extDet1)  // region 1 (side)
					{
						s0 = extent1;
						tmpS1 = -(a01*s0+b1);
						if (tmpS1 < -extent2)
						{
							s1 = -extent2;
							sqrDist = s1*(s1-(2.0)*tmpS1) +
								s0*(s0+(2.0)*b0)+c;
						}
						else if (tmpS1 <= extent2)
						{
							s1 = tmpS1;
							sqrDist = -s1*s1+s0*(s0+(2.0)*b0)+c;
						}
						else
						{
							s1 = extent2;
							sqrDist = s1*(s1-(2.0)*tmpS1) +
								s0*(s0+(2.0)*b0)+c;
						}
					}
					else  // region 2 (corner)
					{
						s1 = extent2;
						tmpS0 = -(a01*s1+b0);
						if (tmpS0 < -extent1)
						{
							s0 = -extent1;
							sqrDist = s0*(s0-(2.0)*tmpS0) +
								s1*(s1+(2.0)*b1)+c;
						}
						else if (tmpS0 <= extent1)
						{
							s0 = tmpS0;
							sqrDist = -s0*s0+s1*(s1+(2.0)*b1)+c;
						}
						else
						{
							s0 = extent1;
							tmpS1 = -(a01*s0+b1);
							if (tmpS1 < -extent2)
							{
								s1 = -extent2;
								sqrDist = s1*(s1-(2.0)*tmpS1) +
									s0*(s0+(2.0)*b0)+c;
							}
							else if (tmpS1 <= extent2)
							{
								s1 = tmpS1;
								sqrDist = -s1*s1+s0*(s0+(2.0)*b0)
									+ c;
							}
							else
							{
								s1 = extent2;
								sqrDist = s1*(s1-(2.0)*tmpS1) +
									s0*(s0+(2.0)*b0)+c;
							}
						}
					}
				}
				else  // region 8 (corner)
				{
					s1 = -extent2;
					tmpS0 = -(a01*s1+b0);
					if (tmpS0 < -extent1)
					{
						s0 = -extent1;
						sqrDist = s0*(s0-(2.0)*tmpS0) +
							s1*(s1+(2.0)*b1)+c;
					}
					else if (tmpS0 <= extent1)
					{
						s0 = tmpS0;
						sqrDist = -s0*s0+s1*(s1+(2.0)*b1)+c;
					}
					else
					{
						s0 = extent1;
						tmpS1 = -(a01*s0+b1);
						if (tmpS1 > extent2)
						{
							s1 = extent2;
							sqrDist = s1*(s1-(2.0)*tmpS1) +
								s0*(s0+(2.0)*b0)+c;
						}
						else if (tmpS1 >= -extent2)
						{
							s1 = tmpS1;
							sqrDist = -s1*s1+s0*(s0+(2.0)*b0)
								+ c;
						}
						else
						{
							s1 = -extent2;
							sqrDist = s1*(s1-(2.0)*tmpS1) +
								s0*(s0+(2.0)*b0)+c;
						}
					}
				}
			}
		}
		else 
		{
			if (s1 >= -extDet1)
			{
				if (s1 <= extDet1)  // region 5 (side)
				{
					s0 = -extent1;
					tmpS1 = -(a01*s0+b1);
					if (tmpS1 < -extent2)
					{
						s1 = -extent2;
						sqrDist = s1*(s1-(2.0)*tmpS1) +
							s0*(s0+(2.0)*b0)+c;
					}
					else if (tmpS1 <= extent2)
					{
						s1 = tmpS1;
						sqrDist = -s1*s1+s0*(s0+(2.0)*b0)+c;
					}
					else
					{
						s1 = extent2;
						sqrDist = s1*(s1-(2.0)*tmpS1) +
							s0*(s0+(2.0)*b0)+c;
					}
				}
				else  // region 4 (corner)
				{
					s1 = extent2;
					tmpS0 = -(a01*s1+b0);
					if (tmpS0 > extent1)
					{
						s0 = extent1;
						sqrDist = s0*(s0-(2.0)*tmpS0) +
							s1*(s1+(2.0)*b1)+c;
					}
					else if (tmpS0 >= -extent1)
					{
						s0 = tmpS0;
						sqrDist = -s0*s0+s1*(s1+(2.0)*b1)+c;
					}
					else
					{
						s0 = -extent1;
						tmpS1 = -(a01*s0+b1);
						if (tmpS1 < -extent2)
						{
							s1 = -extent2;
							sqrDist = s1*(s1-(2.0)*tmpS1) +
								s0*(s0+(2.0)*b0)+c;
						}
						else if (tmpS1 <= extent2)
						{
							s1 = tmpS1;
							sqrDist = -s1*s1+s0*(s0+(2.0)*b0)
								+ c;
						}
						else
						{
							s1 = extent2;
							sqrDist = s1*(s1-(2.0)*tmpS1) +
								s0*(s0+(2.0)*b0)+c;
						}
					}
				}
			}
			else   // region 6 (corner)
			{
				s1 = -extent2;
				tmpS0 = -(a01*s1+b0);
				if (tmpS0 > extent1)
				{
					s0 = extent1;
					sqrDist = s0*(s0-(2.0)*tmpS0) +
						s1*(s1+(2.0)*b1)+c;
				}
				else if (tmpS0 >= -extent1)
				{
					s0 = tmpS0;
					sqrDist = -s0*s0+s1*(s1+(2.0)*b1)+c;
				}
				else
				{
					s0 = -extent1;
					tmpS1 = -(a01*s0+b1);
					if (tmpS1 < -extent2)
					{
						s1 = -extent2;
						sqrDist = s1*(s1-(2.0)*tmpS1) +
							s0*(s0+(2.0)*b0)+c;
					}
					else if (tmpS1 <= extent2)
					{
						s1 = tmpS1;
						sqrDist = -s1*s1+s0*(s0+(2.0)*b0)
							+ c;
					}
					else
					{
						s1 = extent2;
						sqrDist = s1*(s1-(2.0)*tmpS1) +
							s0*(s0+(2.0)*b0)+c;
					}
				}
			}
		}
	}
	else
	{
		// segments are parallel
		double fE0pE1 = extent1 + extent2;
		double fSign = (a01 > 0.0 ? -1.0 : 1.0);
		double fLambda = -b0;
		if (fLambda < -fE0pE1)
		{
			fLambda = -fE0pE1;
		}
		else if (fLambda > fE0pE1)
		{
			fLambda = fE0pE1;
		}

		s1 = fSign*b0*extent2/fE0pE1;
		s0 = fLambda + fSign*s1;
		sqrDist = fLambda*(fLambda + (2.0)*b0) + c;
	}

	double dist = sqrt( abs( sqrDist ) );

	double sum = radius1 + radius2;

	return dist <= sum;
}

static bool IntersectCapsuleSphere( MFnDagNode& capsule, MFnDagNode& sphere )
{
	MVector capsuleCenter, capsuleDirection;
	double capsuleExtent, capsuleRadius;
	GetCapsule( capsule, capsuleCenter, capsuleDirection, capsuleExtent, capsuleRadius );

	MVector sphereCenter;
	double sphereRadius;
	GetSphere( sphere, sphereCenter, sphereRadius );

	// distance point to segment

	MVector diff = sphereCenter - capsuleCenter;
	double param = Dot( capsuleDirection, diff );

	MVector closestPoint0;
	MVector closestPoint1;

	if (-capsuleExtent < param)
	{
		if (param < capsuleExtent)
		{
			closestPoint1 = capsuleCenter +
				param*capsuleDirection;
		}
		else
		{
			closestPoint1 = capsuleCenter +
				capsuleExtent*capsuleDirection;
		}
	}
	else
	{
		closestPoint1 = capsuleCenter -
			capsuleExtent*capsuleDirection;
	}

	closestPoint0 = sphereCenter;
	diff = closestPoint1 - closestPoint0;
	
	double dist = diff.length();

	double radius = sphereRadius + capsuleRadius;

	return dist <= radius;
}

static bool IntersectSphereSphere( MFnDagNode& sphere1, MFnDagNode& sphere2 )
{
	MVector center1;
	double radius1;
	GetSphere( sphere1, center1, radius1 );

	MVector center2;
	double radius2;
	GetSphere( sphere2, center2, radius2 );

	MVector dist = center2 - center1;

	return dist.length() <= ( radius1 + radius2 );
}

static bool Intersect( MFnDependencyNode* n1, MFnDependencyNode* n2 )
{
	MObject obj1 = n1->object();
	MFnDagNode plugin1 ( obj1 );
	MTypeId type1 = plugin1.typeId();

	MObject obj2 = n2->object();
	MFnDagNode plugin2 ( obj2 );
	MTypeId type2 = plugin2.typeId();

	if ( type1 == PH_BOUND_BOX_NODE_ID && type2 == PH_BOUND_BOX_NODE_ID )
	{
		return IntersectBoxBox( plugin1, plugin2 );
	}
	else
	if ( type1 == PH_BOUND_BOX_NODE_ID && type2 == PH_BOUND_CAPSULE_NODE_ID )
	{
		return IntersectBoxCapsule( plugin1, plugin2 );
	}
	else
	if ( type1 == PH_BOUND_BOX_NODE_ID && type2 == PH_BOUND_SPHERE_NODE_ID )
	{
		return IntersectBoxSphere( plugin1, plugin2 );
	}
	else
	if( type1 == PH_BOUND_CAPSULE_NODE_ID && type2 == PH_BOUND_CAPSULE_NODE_ID )
	{
		return IntersectCapsuleCapsule( plugin1, plugin2 );
	}
	else
	if( type1 == PH_BOUND_CAPSULE_NODE_ID && type2 == PH_BOUND_BOX_NODE_ID )
	{
		return IntersectBoxCapsule( plugin2, plugin1 );
	}
	else
	if( type1 == PH_BOUND_CAPSULE_NODE_ID && type2 == PH_BOUND_SPHERE_NODE_ID )
	{
		return IntersectCapsuleSphere( plugin1, plugin2 );
	}
	else
	if( type1 == PH_BOUND_SPHERE_NODE_ID && type2 == PH_BOUND_SPHERE_NODE_ID )
	{
		return IntersectSphereSphere( plugin1, plugin2 );
	}
	else
	if( type1 == PH_BOUND_SPHERE_NODE_ID && type2 == PH_BOUND_BOX_NODE_ID )
	{
		return IntersectBoxSphere( plugin2, plugin1 );
	}
	if( type1 == PH_BOUND_SPHERE_NODE_ID && type2 == PH_BOUND_CAPSULE_NODE_ID )
	{
		return IntersectCapsuleSphere( plugin2, plugin1 );
	}

	return false;
}

static void AddNode( MFnDependencyNodeList& intersects, MFnDependencyNode* node )
{
	MFnDependencyNodeList::iterator it = std::find( intersects.begin(), intersects.end(), node );
	if ( it == intersects.end() )
	{
		intersects.push_back( node );
	}
}

namespace rage {

void FindIntersectingBounds( MFnDependencyNodeList& intersects, const MFnDependencyNodeList& bounds )
{
	MFnDependencyNodeList::const_iterator it = bounds.begin();
	while ( it != bounds.end() )
	{
		MFnDependencyNode* node = *it;

		MFnDependencyNodeList::const_iterator cmpIt = it + 1;
		while ( cmpIt != bounds.end() )
		{
			MFnDependencyNode* cmp = *cmpIt;

			if ( Intersect( node, cmp ) )
			{
				AddNode( intersects, node );
				AddNode( intersects, cmp );
			}

			++cmpIt;
		}

		++it;
	}
}

} // rage
