#include "rageMayaMeshRule.h"

using namespace rage;

MeshRule::MeshRule()
:	m_ShapeFlags ( 0 )
,	m_ShrinkRatio ( 0.0 )
,	m_DecompositionDepth ( 0 )
,	m_ConcaveThreshold ( 0.0 )
,	m_MergeThreshold ( 0.0 )
,	m_MaxVertices ( 0 )
,	m_SkinWidth ( 0.0 )
,	m_BoxPercent ( 0.0 )
,	m_SpherePercent ( 0.0 )
,	m_CapsulePercent ( 0.0 )
,	m_CapsuleShrink ( 0.0 ) 
,	m_MaterialName( "" )
,	m_GroupBoundsAndMesh ( false )
,	m_GroupBounds ( false )
,	m_KeepHierarchy ( false )
,	m_AsOneGroup ( false ) { }

MeshRule::MeshRule( MArgDatabase& db )
:	m_ShapeFlags ( 0 )
,	m_ShrinkRatio ( 0.0 )
,	m_DecompositionDepth ( 0 )
,	m_ConcaveThreshold ( 0.0 )
,	m_MergeThreshold ( 0.0 )
,	m_MaxVertices ( 0 )
,	m_SkinWidth ( 0.0 )
,	m_BoxPercent ( 0.0 )
,	m_SpherePercent ( 0.0 )
,	m_CapsulePercent ( 0.0 )
,	m_CapsuleShrink ( 0.0 ) 
,	m_MaterialName( "" )
,	m_GroupBoundsAndMesh ( false )
,	m_GroupBounds ( false )
,	m_KeepHierarchy ( false )
,	m_AsOneGroup ( false )
{ 
	SetFromDatabase( db );
}

void MeshRule::SetFromDatabase( MArgDatabase& db )
{
	if ( db.isFlagSet( "-split_depth" ) )
	{
		db.getFlagArgument( "-split_depth", 0, m_DecompositionDepth );
	}

	if ( db.isFlagSet( "-concave_theshold" ) )
	{
		db.getFlagArgument( "-concave_theshold", 0, m_ConcaveThreshold );
	}

	if ( db.isFlagSet( "-merge_threshold" ) )
	{
		db.getFlagArgument( "-merge_threshold", 0, m_MergeThreshold );
	}

	if ( db.isFlagSet( "-max_vertices" ) )
	{
		db.getFlagArgument( "-max_vertices", 0, m_MaxVertices );
	}

	if ( db.isFlagSet( "-shrink_ratio" ) )
	{
		db.getFlagArgument( "-shrink_ratio", 0, m_ShrinkRatio );
	}

	if ( db.isFlagSet( "-fit_box" ) )
	{
		SetShapeFlag( MeshRule::SF_BOX );
	}

	if ( db.isFlagSet( "-use_obb" ) )
	{
		SetShapeFlag( MeshRule::SF_OBB );
	}

	if ( db.isFlagSet( "-fit_sphere" ) )
	{
		SetShapeFlag( MeshRule::SF_SPHERE );
	}

	if ( db.isFlagSet( "-fit_capsule" ) )
	{
		SetShapeFlag( MeshRule::SF_CAPSULE );
	}

	if ( db.isFlagSet( "-fit_convex" ) )
	{
		SetShapeFlag( MeshRule::SF_CONVEX );
	}

	if ( db.isFlagSet( "-box_percent" ) )
	{
		db.getFlagArgument( "-box_percent", 0, m_BoxPercent );
	}

	if ( db.isFlagSet( "-sphere_percent" ) )
	{
		db.getFlagArgument( "-sphere_percent", 0, m_SpherePercent );
	}

	if ( db.isFlagSet( "-capsule_percent" ) )
	{
		db.getFlagArgument( "-capsule_percent", 0, m_CapsulePercent );
	}

	if ( db.isFlagSet( "-material" ) )
	{
		db.getFlagArgument( "-material", 0, m_MaterialName );
	}

	if ( db.isFlagSet( "-group_bounds_mesh" ) )
	{
		m_GroupBoundsAndMesh = true;
	}

	if ( db.isFlagSet( "-group_bounds" ) )
	{
		m_GroupBounds = true;
	}

	if ( db.isFlagSet( "-keep_hierarchy" ) )
	{
		m_KeepHierarchy = true;
	}

	if ( db.isFlagSet( "-as_one_group" ) )
	{
		m_AsOneGroup = true;
	}
}

void MeshRule::SetShapeFlag( ShapeFlag flag )		
{ 
	m_ShapeFlags |= flag; 
}

void MeshRule::ClearShapeFlag( ShapeFlag flag )	
{ 
	m_ShapeFlags &= ~flag; 
}

bool MeshRule::HasShapeFlag( ShapeFlag flag )		
{ 
	if ( m_ShapeFlags & flag )
	{
		return true; 
	}
	return false; 
}
