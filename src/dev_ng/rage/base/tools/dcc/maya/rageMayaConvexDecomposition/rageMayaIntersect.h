#ifndef __RAGE_MAYA_INTERSECT_H__
#define __RAGE_MAYA_INTERSECT_H__

#pragma warning( push )
#pragma warning( disable: 4668 )
#pragma warning( disable: 4100 )

#include <Maya/MFnDependencyNode.h>

#pragma warning( pop )

#include <vector>

namespace rage {

	typedef std::vector< MFnDependencyNode* > MFnDependencyNodeList;

	void FindIntersectingBounds( MFnDependencyNodeList& intersects, const MFnDependencyNodeList& bounds );

} // rage

#endif // __RAGE_MAYA_INTERSECT_H__
