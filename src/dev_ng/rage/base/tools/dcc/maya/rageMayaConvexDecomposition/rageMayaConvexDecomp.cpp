#include "rageMayaConvexDecomp.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <float.h>
#include <math.h>

#include "bestfitobb.h"
#include "fitsphere.h"
#include "float_math.h"

#pragma warning( push )
#pragma warning( disable: 4668 )
#pragma warning( disable: 4100 )

#include <Maya/MDagPath.h>
#include <Maya/MFloatPointArray.h>
#include <Maya/MGlobal.h>
#include <Maya/MIntArray.h>
#include <Maya/MQuaternion.h>
#include <Maya/MVector.h>

#include <Maya/MFnDependencyNode.h>
#include <Maya/MFnMesh.h>
#include <maya/MFnTransform.h>

#pragma warning( pop )

using namespace ConvexDecomposition;
using namespace rage;

namespace rage
{
	const MTypeId PH_BOUND_BOX_NODE_ID = 0x7010;
	const MTypeId PH_BOUND_CAPSULE_NODE_ID = 0x7011;
	const MTypeId PH_BOUND_SPHERE_NODE_ID =0x7012;
}

static void SetTransformName( MFnTransform& transform, const MString& name, unsigned int index )
{
	MString bname = "bounds_";
	bname += name;

	char str[ 128 ];
	sprintf( str, "_%03d", index );

	bname += MString( str );

	transform.setName( bname );
}

static int FindLongestSide( const double sides[ 3 ] )
{
	if ( sides[ 0 ] < sides[ 1 ] )
	{
		if ( sides[ 1 ] < sides[ 2 ] )
		{
			return 2;
		}
		
		return 1;
	}

	if ( sides[ 0 ] < sides[ 2 ] )
	{
		return 2;
	}

	return 0;
}

static void GetOtherSides( const int side, int& side0, int& side1 )
{
	switch ( side )
	{
	case 0:
		side0 = 1;
		side1 = 2;
		break;
	case 1:
		side0 = 0;
		side1 = 2;
		break;
	case 2:
		side0 = 0;
		side1 = 1;
		break;
	default:
		assert( false );
	}
}

static void MultQuat( double* r, const double* a, const double* b )
{
	r[ 0 ] = a[ 3 ] * b[ 0 ] + a[ 0 ] * b[ 3 ] 
		+ a[ 1 ] * b[ 2 ] - a[ 2 ] * b[ 1 ];
	r[ 1 ] = a[ 3 ] * b[ 1 ] + a[ 1 ] * b[ 3 ] 
		+ a[ 2 ] * b[ 0 ] - a[ 0 ] * b[ 2 ];
	r[ 2 ] = a[ 3 ] * b[ 2 ] + a[ 2 ] * b[ 3 ] 
		+ a[ 0 ] * b[ 1 ] - a[ 1 ] * b[ 0 ];
	r[ 3 ] = a[ 3 ] * b[ 3 ] - a[ 0 ] * b[ 0 ] 
		- a[ 1 ] * b[ 1 ] - a[ 2 ] * b[ 2 ];
}

ConvexDecomp::ConvexDecomp( const MString& name, const MeshRule& rule, MDagModifier* modifier )
:	m_HullCount ( 0 )
,	m_Rule ( rule )
,	m_Name ( name )
,	m_Modifier ( modifier )
{
}

ConvexDecomp::~ConvexDecomp()
{
}

void ConvexDecomp::ConvexDecompResult( ConvexResult& result )
{
	// we have a hull...

	double boxSides[ 3 ];
	double boxPos[ 3 ];
	double boxOrientation[ 4 ] = { 0.0, 0.0, 0.0, 1.0 };

	if ( m_Rule.HasShapeFlag( MeshRule::SF_OBB ) )
	{
		computeBestFitOBB( result.mHullVcount, result.mHullVertices, sizeof( double ) * 3, boxSides, boxPos, boxOrientation );
	}
	else
	{
		computeBestFitABB( result.mHullVcount, result.mHullVertices, sizeof( double ) * 3, boxSides, boxPos );
	}

	boxSides[ 0 ] -= boxSides[ 0 ] * m_Rule.m_ShrinkRatio;
	boxSides[ 1 ] -= boxSides[ 1 ] * m_Rule.m_ShrinkRatio;
	boxSides[ 2 ] -= boxSides[ 2 ] * m_Rule.m_ShrinkRatio;

	double boxVolume = boxSides[ 0 ] * boxSides[ 1 ] *  boxSides[ 2 ];

	double sphereCenter[ 3 ];
	double sphereRadius = computeBoundingSphere( result.mHullVcount, result.mHullVertices, sphereCenter );

	sphereRadius -= sphereRadius * m_Rule.m_ShrinkRatio;

	double sphereVolume = fm_sphereVolume( sphereRadius );

	double capsuleSides[ 3 ];
	double capsuleCenter[ 3 ];
	double capsuleOrientation[ 4 ];

	computeBestFitOBB( result.mHullVcount, result.mHullVertices, sizeof( double ) * 3, capsuleSides, capsuleCenter, capsuleOrientation );

	// quaternion needs be directed along the longest side of the box

	double cpercent = 0.0;
	double bpercent = ( result.mHullVolume * 100.0 ) / boxVolume;
	double spercent = ( result.mHullVolume * 100.0 ) / sphereVolume;

	int longestSide = FindLongestSide( capsuleSides );
	int side0, side1;
	GetOtherSides( longestSide, side0, side1 );

	double capsuleRadius = sqrt( capsuleSides[ side0 ] * capsuleSides[ side0 ] + capsuleSides[ side1 ] * capsuleSides[ side1 ] );

	double capsuleHeight = 0.0;
	double capsuleVolume = 0.0;

	// a vector straight up in y is transformed by our quaternion, if it's in 
	// the "wrong" direction we need to rotate our resulting sphere

	double v[ 3 ] = { 0.0, 1.0, 0.0 };
	double r[ 3 ];
	fm_quatRotate( capsuleOrientation, v, r );


	if ( 2 == longestSide )
	{
		double quat[ 4 ];
		double r[ 4 ];
		fm_eulerToQuat( FM_DEG_TO_RAD * 90.0f, 0.f, 0.f, quat );

		MultQuat( r, capsuleOrientation, quat );

		capsuleOrientation[ 0 ] = r[ 0 ];
		capsuleOrientation[ 1 ] = r[ 1 ];
		capsuleOrientation[ 2 ] = r[ 2 ];
		capsuleOrientation[ 3 ] = r[ 3 ];
	}
	else
	if ( 0 == longestSide )
	{
		double quat[ 4 ];
		double r[ 4 ];
		fm_eulerToQuat( 0.f, 0.f, FM_DEG_TO_RAD * 90.0f, quat );

		MultQuat( r, capsuleOrientation, quat );

		capsuleOrientation[ 0 ] = r[ 0 ];
		capsuleOrientation[ 1 ] = r[ 1 ];
		capsuleOrientation[ 2 ] = r[ 2 ];
		capsuleOrientation[ 3 ] = r[ 3 ];
	}

	//if ( capsuleSides[ 0 ] > ( capsuleRadius * 1.01f ) )
	{
		capsuleHeight = capsuleSides[ longestSide ];
		capsuleRadius = capsuleRadius * 0.5; //* m_Rule.m_CapsuleShrink;
		capsuleVolume = fm_capsuleVolume( capsuleRadius, capsuleHeight * 2.0 );

		cpercent = ( result.mHullVolume * 100.0 ) / capsuleVolume;
	}

	capsuleRadius -= capsuleRadius * m_Rule.m_ShrinkRatio;
	capsuleHeight -= capsuleHeight * m_Rule.m_ShrinkRatio;

	// chose type
	ShapeType type = ST_INVALID;

	if ( m_Rule.HasShapeFlag( MeshRule::SF_CONVEX ) )
	{
		type = ST_CONVEX;
	}

	if ( bpercent >= m_Rule.m_BoxPercent && m_Rule.HasShapeFlag( MeshRule::SF_BOX ) )
	{
		type = ST_BOX;
	}

	if ( spercent >= m_Rule.m_SpherePercent && m_Rule.HasShapeFlag( MeshRule::SF_SPHERE ) )
	{
		type = ST_SPHERE;
	}

	if ( cpercent >= m_Rule.m_CapsulePercent && m_Rule.HasShapeFlag( MeshRule::SF_CAPSULE ) )
	{
		type = ST_CAPSULE;
	}

	if ( ST_INVALID == type )
	{
		type = ST_BOX;

		if ( m_Rule.HasShapeFlag( MeshRule::SF_SPHERE ) )
		{
			type = ST_SPHERE;
		}
		if ( m_Rule.HasShapeFlag( MeshRule::SF_BOX ) )
		{
			type = ST_BOX;
		}
		if ( m_Rule.HasShapeFlag( MeshRule::SF_CAPSULE ) )
		{
			type = ST_CAPSULE;
		}
	}

	switch ( type )
	{
	case ST_BOX:
		{
			MStatus status;

			MFnDependencyNode node;
			MObject temp = m_Modifier->createNode( "transform" );
			MObject obj = m_Modifier->createNode( PH_BOUND_BOX_NODE_ID, temp );

			MFnTransform transform ( temp, &status );

			transform.setTranslation( MVector( boxPos ), MSpace::kTransform );
			transform.setScale( boxSides );
			transform.setRotation( MQuaternion( boxOrientation ), MSpace::kTransform );

			SetTransformName( transform, m_Name, m_HullCount );
		}
		break;
	case ST_SPHERE:
		{
			MStatus status;

			MFnDependencyNode node;
			MObject temp = m_Modifier->createNode( "transform" );
			MObject obj = m_Modifier->createNode( PH_BOUND_SPHERE_NODE_ID, temp );

			double scale[ 3 ] = 
				{ sphereRadius, sphereRadius, sphereRadius };

			MFnTransform transform ( temp, &status );
			transform.setScale( scale );
			transform.setTranslation( MVector( sphereCenter ), MSpace::kTransform );

			SetTransformName( transform, m_Name, m_HullCount );
		}
		break;
	case ST_CAPSULE:
		{
			MStatus status;

			MFnDependencyNode node;
			MObject temp = m_Modifier->createNode( "transform" );
			MObject obj = m_Modifier->createNode( PH_BOUND_CAPSULE_NODE_ID, temp );

			double scale[ 3 ] =
				{ capsuleRadius, capsuleHeight, capsuleHeight };

			MFnTransform transform ( temp, &status );
			transform.setScale( scale );
			transform.setTranslation( MVector( capsuleCenter ), MSpace::kTransform );
			transform.setRotation( MQuaternion( capsuleOrientation ), MSpace::kTransform );

			SetTransformName( transform, m_Name, m_HullCount );
		}
		break;
	case ST_CONVEX:
		{
			MFloatPointArray vertices;
			vertices.setLength( result.mHullVcount );

			double* p = result.mHullVertices; 
			for ( unsigned int i = 0; i < result.mHullVcount; ++i )
			{
				vertices.set( i, float( p[ 0 ] ), float( p[ 1 ] ), float( p[ 2 ] ) );
				p += 3;
			}

			MIntArray indices;
			MIntArray counts;

			unsigned int* index = result.mHullIndices;

			for ( unsigned int i = 0; i < result.mHullTcount; ++i )
			{
				indices.append( *index++ );
				indices.append( *index++ );
				indices.append( *index++ );

				counts.append( 3 );
			}

			MStatus status;

			MObject temp = m_Modifier->createNode( "transform" );
			
			MFnMesh mesh;
			mesh.create( result.mHullVcount, result.mHullTcount, vertices, counts, indices, temp, &status );
			if ( MS::kSuccess == status )
			{
				MFnTransform transform ( temp );
				
				SetTransformName( transform, m_Name, m_HullCount );
			}
		}
		break;
	case ST_INVALID:
		break;
	}

	m_HullCount++;
}

void ConvexDecomp::ConvexDebugTri( const double* p1, const double* p2, const double* p3, unsigned int )
{
	MFloatPointArray vertices;
	vertices.setLength( 3 );

	vertices.set( 0, float( p1[ 0 ] ), float( p1[ 1 ] ), float( p1[ 2 ] ) );
	vertices.set( 1, float( p2[ 0 ] ), float( p2[ 1 ] ), float( p2[ 2 ] ) );
	vertices.set( 2, float( p3[ 0 ] ), float( p3[ 1 ] ), float( p3[ 2 ] ) );

	MIntArray indices;
	MIntArray counts;

	indices.append( 0 );
	indices.append( 1 );
	indices.append( 2 );

	counts.append( 3 );
	

	MStatus status;

	MFnMesh mesh;
	mesh.create( 3, 1, vertices, counts, indices, MObject::kNullObj, &status );
}
