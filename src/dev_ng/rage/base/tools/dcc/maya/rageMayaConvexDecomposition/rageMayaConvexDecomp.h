#ifndef __RAGE_MAYA_CONVEX_DECOMP_H__
#define __RAGE_MAYA_CONVEX_DECOMP_H__

#include "rageMayaMeshRule.h"

#include "ConvexDecomposition.h"

#include <vector>

#pragma warning( push )
#pragma warning( disable: 4668 )
#pragma warning( disable: 4100 )

#include <Maya/MDagModifier.h>
#include <Maya/MString.h>
#include <Maya/MTypeId.h>

#include "Maya/MObject.h"
#include "Maya/MTransformationMatrix.h"

#pragma warning( pop )

namespace rage {

extern const MTypeId PH_BOUND_BOX_NODE_ID;
extern const MTypeId PH_BOUND_CAPSULE_NODE_ID;
extern const MTypeId PH_BOUND_SPHERE_NODE_ID;

typedef std::vector< std::pair< MObject, MObject > > ParentList; // object, parent
typedef std::vector< MObject > CreatedList;

class ConvexDecomp : public ConvexDecomposition::ConvexDecompInterface
{
public:
	enum ShapeType
	{
		ST_BOX,
		ST_SPHERE,
		ST_CAPSULE,
		ST_CONVEX,

		ST_INVALID,
	};
public:
	virtual void ConvexDecompResult( ConvexDecomposition::ConvexResult& result );

	virtual void ConvexDebugTri( const double* p1, const double* p2, const double* p3, unsigned int color );
public:
	ConvexDecomp( const MString& name, const MeshRule& rule, MDagModifier* modifier );
	virtual ~ConvexDecomp();
private:
	ConvexDecomp& operator=( const ConvexDecomp& );
private:
	unsigned int m_HullCount;

	MeshRule m_Rule;

	MString m_Name;

	MDagModifier* m_Modifier;
};

} // rage

#endif // __RAGE_MAYA_CONVEX_DECOMP_H__
