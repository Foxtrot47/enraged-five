#ifndef __RAGE_MAYA_MESH_RULE_H__
#define __RAGE_MAYA_MESH_RULE_H__

#pragma warning( push )
#pragma warning( disable: 4668 )
#pragma warning( disable: 4100 )

#include <Maya/MArgDatabase.h>
#include <Maya/MString.h>

#pragma warning( pop )

namespace rage {

class MeshRule
{
public:
	enum ShapeFlag 
	{
		SF_BOX			= ( 1 << 0 ),
		SF_SPHERE		= ( 1 << 1 ),
		SF_CAPSULE		= ( 1 << 2 ),
		SF_CONVEX		= ( 1 << 3 ),
		SF_OBB			= ( 1 << 4 ),
	};
public:
	void SetShapeFlag( ShapeFlag flag );
	void ClearShapeFlag( ShapeFlag flag );
	bool HasShapeFlag( ShapeFlag flag );

	void SetFromDatabase( MArgDatabase& db );
public:
	MeshRule( MArgDatabase& db );
	MeshRule();
public:
	int m_ShapeFlags;

	double m_ShrinkRatio;

	int m_DecompositionDepth;
	double m_ConcaveThreshold;
	double m_MergeThreshold;
	int m_MaxVertices;
	double m_SkinWidth;

	double m_BoxPercent;
	double m_SpherePercent;
	double m_CapsulePercent;
	double m_CapsuleShrink;

	MString m_MaterialName;

	bool m_GroupBoundsAndMesh;	// bounding volumes are grouped with mesh
	bool m_GroupBounds;			// bounding volumes are grouped together

	bool m_KeepHierarchy;

	bool m_AsOneGroup;
};

} // rage

#endif // __RAGE_MAYA_MESH_RULE_H__
