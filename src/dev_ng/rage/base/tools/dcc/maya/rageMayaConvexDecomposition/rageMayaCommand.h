#ifndef __RAGE_MAYA_COMMAND_H__
#define __RAGE_MAYA_COMMAND_H__

#include "rageMayaMeshRule.h"

#pragma warning( push )
#pragma warning( disable: 4668 )
#pragma warning( disable: 4100 )

#include <Maya/MDagModifier.h>
#include <Maya/MDagPath.h>
#include <Maya/MIntArray.h>
#include <Maya/MPointArray.h>
#include <Maya/MSelectionList.h>
#include <Maya/MSyntax.h>

#include <Maya/MPxCommand.h>

#pragma warning ( pop )

#include <vector>

namespace rage {

struct VertexInfo
{
	VertexInfo( const MDagPath& dp )
		:	DagPath ( dp ) { }

	MDagPath DagPath;

	MIntArray VertIndices;
	MPointArray VertPositions;
};

__inline bool operator ==( VertexInfo* a, const MDagPath& other )
{
	return ( a->DagPath == other );
}

typedef std::vector< VertexInfo* > VertexInfoList;

class ConvexDecompCommand : public MPxCommand
{
public:
	bool			isUndoable() const { return true; }

	MStatus			redoIt();//		{ clearResult(); setResult( int( 1 ) ); return MS::kSuccess; }
	MStatus			undoIt();//		{ return MS::kSuccess; }

	MStatus			doIt( const MArgList& args );

	static void*	creator()		{ return new ConvexDecompCommand; }
	static MSyntax	createSyntax();
public:
	ConvexDecompCommand();
	virtual ~ConvexDecompCommand();
private:
	void InitializeFromSelection();
private:
	//MSelectionList m_SelectionList;

	MeshRule m_Rule;

	MDagModifier* m_Modifier;

	VertexInfoList m_VertInfoList;
};

class SelectIntersectBoundVolumeCommand : public MPxCommand
{
public:
	bool			isUndoable() const { return false; }

	MStatus			redoIt()		{ clearResult(); setResult( int( 1 ) ); return MS::kSuccess; }
	MStatus			undoIt()		{ return MS::kSuccess; }

	MStatus			doIt( const MArgList& args );

	static void*	creator()		{ return new SelectIntersectBoundVolumeCommand; }
public:
	SelectIntersectBoundVolumeCommand();
	virtual ~SelectIntersectBoundVolumeCommand();
};

} // rage

#endif // __RAGE_MAYA_COMMAND_H__
