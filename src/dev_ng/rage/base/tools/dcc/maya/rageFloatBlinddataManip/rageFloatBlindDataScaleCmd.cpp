// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
// #include <maya/MPxManipContainer.h>
// #include <maya/MPxSelectionContext.h>
#include <maya/MPxContextCommand.h>
// #include <maya/MModelMessage.h>
// #include <maya/MFnPlugin.h>
// #include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>
#include <maya/MArgParser.h>

// Manipulators
// #include <maya/MFnScaleManip.h>
#include "rageFloatBlindDataScaleCmd.h"
#include "rageFloatBlindDataScaleContext.h"

// Args
#define kBlindDataFlag		"-bd"
#define kBlindDataFlagLong	"-blindData"
#define kBlindDataIDFlag		"-bdi"
#define kBlindDataIDFlagLong	"-blindDataID"
#define kBlindDataAttributeNameFlag		"-at"
#define kBlindDataAttributeNameFlagLong	"-blindDataAttributeName"
#define kDrawValuesFlag		"-val"
#define kDrawValuesFlagLong	"-drawValues"
#define kDrawNormalsFlag		"-nor"
#define kDrawNormalsFlagLong	"-drawNormals"
#define kMinFlag		"-min"
#define kMinFlagLong	"-minValue"
#define kMaxFlag		"-max"
#define kMaxFlagLong	"-maxValue"
#define kDefaultFlag	"-def"
#define kDefaultFlagLong	"-defaultValue"


MPxContext *rageFloatBlindDataScaleCmd::makeObj()
{
	m_pobRageFloatBlindDataScaleContext = new rageFloatBlindDataScaleContext();
	return m_pobRageFloatBlindDataScaleContext;
}

void *rageFloatBlindDataScaleCmd::creator()
{
	return new rageFloatBlindDataScaleCmd;
}

MStatus rageFloatBlindDataScaleCmd::doEditFlags()
{
	MStatus status = MS::kSuccess;
	
	MArgParser argData = parser();
	if (argData.isFlagSet(kBlindDataFlag)) 
	{
		// Store the BlindData ID
		unsigned uBlindDataID;
		status = argData.getFlagArgument(kBlindDataFlag, 0, uBlindDataID);
		if (!status) 
		{
			status.perror(MString(kBlindDataFlagLong) +" flag parsing failed.");
			return status;
		}
		m_pobRageFloatBlindDataScaleContext->SetBlindDataID(uBlindDataID);

		// Store the BlindData Attribute name
		MString strBlindDataAttributeName;
		status = argData.getFlagArgument(kBlindDataFlag, 1, strBlindDataAttributeName);
		if (!status) 
		{
			status.perror(MString(kBlindDataFlagLong) +" flag parsing failed.");
			return status;
		}
		m_pobRageFloatBlindDataScaleContext->SetAttributeName(strBlindDataAttributeName);
		rageFloatBlindDataScaleContext::updateManipulators(m_pobRageFloatBlindDataScaleContext);
	}
	else if (argData.isFlagSet(kBlindDataIDFlag)) 
	{
		status.perror(MString(kBlindDataIDFlagLong) +" is a query only flag.");
		return status;
	}
	else if (argData.isFlagSet(kBlindDataAttributeNameFlag)) 
	{
		status.perror(MString(kBlindDataAttributeNameFlagLong) +" is a query only flag.");
		return status;
	}
	else if (argData.isFlagSet(kDrawValuesFlag)) 
	{
		// Store the BlindDataID
		bool bDrawValues;
		status = argData.getFlagArgument(kDrawValuesFlag, 0, bDrawValues);
		if (!status) 
		{
			status.perror(MString(kDrawValuesFlag) +" flag parsing failed.");
			return status;
		}
		m_pobRageFloatBlindDataScaleContext->SetDrawValues(bDrawValues);
	}
	else if (argData.isFlagSet(kDrawNormalsFlag)) 
	{
		// Store the BlindDataID
		bool bDrawNormals;
		status = argData.getFlagArgument(kDrawNormalsFlag, 0, bDrawNormals);
		if (!status) 
		{
			status.perror(MString(kDrawNormalsFlag) +" flag parsing failed.");
			return status;
		}
		m_pobRageFloatBlindDataScaleContext->SetDrawNormals(bDrawNormals);
	}
	else if (argData.isFlagSet(kMinFlag)) 
	{
		// Store the BlindDataID
		double fMin;
		status = argData.getFlagArgument(kMinFlag, 0, fMin);
		if (!status) 
		{
			status.perror(MString(kMinFlag) +" flag parsing failed.");
			return status;
		}
		m_pobRageFloatBlindDataScaleContext->SetMin((float)fMin);
	}
	else if (argData.isFlagSet(kMaxFlag)) 
	{
		// Store the BlindDataID
		double fMax;
		status = argData.getFlagArgument(kMaxFlag, 0, fMax);
		if (!status) 
		{
			status.perror(MString(kMaxFlag) +" flag parsing failed.");
			return status;
		}
		m_pobRageFloatBlindDataScaleContext->SetMax((float)fMax);
	}
	else if (argData.isFlagSet(kDefaultFlag)) 
	{
		// Store the BlindDataID
		double fDefault;
		status = argData.getFlagArgument(kDefaultFlag, 0, fDefault);
		if (!status) 
		{
			status.perror(MString(kDefaultFlag) +" flag parsing failed.");
			return status;
		}
		m_pobRageFloatBlindDataScaleContext->SetDefault((float)fDefault);
	}
	
	return MS::kSuccess;
}

MStatus rageFloatBlindDataScaleCmd::doQueryFlags()
{
	MArgParser argData = parser();
	
	if (argData.isFlagSet(kBlindDataIDFlag)) 
	{
		setResult((int)m_pobRageFloatBlindDataScaleContext->GetBlindDataID());
	}
	else if (argData.isFlagSet(kBlindDataAttributeNameFlag)) 
	{
		setResult(m_pobRageFloatBlindDataScaleContext->GetAttributeName());
	}
	else if (argData.isFlagSet(kDrawValuesFlag)) 
	{
		setResult(m_pobRageFloatBlindDataScaleContext->GetDrawValues());
	}
	else if (argData.isFlagSet(kDrawNormalsFlag)) 
	{
		setResult(m_pobRageFloatBlindDataScaleContext->GetDrawNormals());
	}
	else if (argData.isFlagSet(kMinFlag)) 
	{
		setResult(m_pobRageFloatBlindDataScaleContext->GetMin());
	}
	else if (argData.isFlagSet(kMaxFlag)) 
	{
		setResult(m_pobRageFloatBlindDataScaleContext->GetMax());
	}
	else if (argData.isFlagSet(kDefaultFlag)) 
	{
		setResult(m_pobRageFloatBlindDataScaleContext->GetDefault());
	}
	
	return MS::kSuccess;
}

MStatus rageFloatBlindDataScaleCmd::appendSyntax()
{
	MSyntax mySyntax = syntax();
	
	if (MS::kSuccess != mySyntax.addFlag(kBlindDataFlag, kBlindDataFlagLong,MSyntax::kUnsigned,MSyntax::kString)) 
	{
		return MS::kFailure;
	}
	if (MS::kSuccess != mySyntax.addFlag(kBlindDataIDFlag, kBlindDataIDFlagLong,MSyntax::kUnsigned)) 
	{
		return MS::kFailure;
	}
	if (MS::kSuccess != mySyntax.addFlag(kBlindDataAttributeNameFlag, kBlindDataAttributeNameFlagLong,MSyntax::kString)) 
	{
		return MS::kFailure;
	}
	if (MS::kSuccess != mySyntax.addFlag(kDrawValuesFlag, kDrawValuesFlagLong,MSyntax::kBoolean)) 
	{
		return MS::kFailure;
	}
	if (MS::kSuccess != mySyntax.addFlag(kDrawNormalsFlag, kDrawNormalsFlagLong,MSyntax::kBoolean)) 
	{
		return MS::kFailure;
	}
	if (MS::kSuccess != mySyntax.addFlag(kMinFlag, kMinFlagLong,MSyntax::kDouble)) 
	{
		return MS::kFailure;
	}
	MStatus obStatus = mySyntax.addFlag(kDefaultFlag, kDefaultFlagLong,MSyntax::kDouble);
	if (MS::kSuccess != obStatus) 
	{
		char acError[512];
		strcpy(acError, obStatus.errorString().asChar());
		cout<<acError<<endl;
		return MS::kFailure;
	}
	obStatus = mySyntax.addFlag(kMaxFlag, kMaxFlagLong,MSyntax::kDouble);
	if (MS::kSuccess != obStatus) 
	{
		char acError[512];
		strcpy(acError, obStatus.errorString().asChar());
		cout<<acError<<endl;
		return MS::kFailure;
	}

	return MS::kSuccess;
}
