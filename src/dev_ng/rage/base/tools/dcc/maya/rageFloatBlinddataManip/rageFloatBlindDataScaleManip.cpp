//#include <maya/MIOStream.h>
//#include <stdio.h>
//#include <stdlib.h>
//
//#include <maya/MFn.h>
//#include <maya/MPxNode.h>
//#include <maya/rageFloatBlindDataManip.h>
//#include <maya/MPxSelectionContext.h>
//#include <maya/MPxContextCommand.h>
//#include <maya/MModelMessage.h>
//#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
//#include <maya/MItSelectionList.h>
//#include <maya/MPoint.h>
#include <maya/MVector.h>
//#include <maya/MDagPath.h>
#include <maya/MManipData.h>
//#include <maya/MSelectionList.h>
#include <maya/MItMeshVertex.h>
#include <maya/MFnComponent.h>
//#include <maya/MStatus.h>
//#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>

//
//Manipulators
#include <maya/MFnScaleManip.h>
#include <maya/MFnRotateManip.h>
//
//#include "rageFloatBlindDataContext.h"
#include "rageFloatBlindDataScaleManip.h"
//#include "rageFloatBlindDataScaleCmd.h"
// #include "rageFloatBlindDataScaleManip.h"
#define POINT_SIZE				20.0

MTypeId rageFloatBlindDataScaleManip::id( 0x700E );

rageFloatBlindDataScaleManip::rageFloatBlindDataScaleManip() 
: rageFloatBlindDataManip()
{ 
	// The constructor must not call createChildren for user-defined
	// manipulators.
}


rageFloatBlindDataScaleManip::~rageFloatBlindDataScaleManip() 
{
}


void *rageFloatBlindDataScaleManip::creator()
{
	 return new rageFloatBlindDataScaleManip();
}


MStatus rageFloatBlindDataScaleManip::initialize()
{ 
	MStatus obStatus;
	obStatus = rageFloatBlindDataManip::initialize();
	return obStatus;
}


MStatus rageFloatBlindDataScaleManip::createChildren()
{
	MStatus obStatus = MStatus::kSuccess;
	m_obManipDagPath = addScaleManip("scaleManip", "scale");
	return obStatus;
}


MStatus rageFloatBlindDataScaleManip::connectToDependNode(const MObject &obMeshNode)
{
	MStatus obStatus;
	rageFloatBlindDataManip::connectToDependNode(obMeshNode);
	
	MFnComponent obFnSelectedComponents(GetSelectedComponents());
	MFnMesh obFnMeshNode(obMeshNode);

	MPlug obDummyPlug = obFnMeshNode.findPlug( DUMMY_ATTRIBUTE_NAME, &obStatus );
	if((!obStatus) || (obDummyPlug.isNull()))
	{
		OutputError(MString("Could not find ")+ DUMMY_ATTRIBUTE_NAME +" attribute on "+ obFnMeshNode.fullPathName());
		return MS::kFailure;
	}

	int i = 0;
	for (MItMeshVertex iter(GetMeshDagPath(), GetSelectedComponents()); !iter.isDone(); iter.next(), i++)
	{
		// Add the callback for this vector
		unsigned plugIndex = addManipToPlugConversionCallback(obDummyPlug, (manipToPlugConversionCallback) &rageFloatBlindDataScaleManip::manipChangedCallback);

		// Plug indices should be allocated sequentially, starting at 0.  Each
		// manip container will have its own set of plug indices.  This code 
		// relies on the sequential allocation of indices, so trigger an error
		// if the indices are found to be different.
		//
		if (plugIndex != (unsigned)i)
		{
			OutputError("Unexpected plug index returned.");
			return MS::kFailure;
		}
	}

	MFnScaleManip scaleManip(m_obManipDagPath);

	// Create a plugToManip callback that places the manipulator at the 
	// m_obCentroidOfAllSelectedComponents of the CVs.
	addPlugToManipConversionCallback(scaleManip.scaleCenterIndex(), (plugToManipConversionCallback) &rageFloatBlindDataScaleManip::centerChangedCallback);

	finishAddingManips();
	return obStatus;
}

MManipData rageFloatBlindDataScaleManip::manipChangedCallback(unsigned iMagicIndex)
{
	// cout<<"m_obAISelectedComponentIndexes["<<iMagicIndex<<"] = "<<m_obAISelectedComponentIndexes[iMagicIndex]<<endl;

	// If we entered the callback with an invalid iMagicIndex, print an error and
	// return.  The callback should only be called for indices from 0 to
	// m_iNumComponents-1.
	//
	MFnScaleManip scaleManip(m_obManipDagPath);
	if (iMagicIndex >= m_obAISelectedComponentIndexes.length())
	{
		OutputError("Invalid index in scale changed callback!");
		return MManipData(32.0f);
	}

	// Now we need to determine the scaled position of the CV specified by iMagicIndex.
	MVector scaleVal;
	getConverterManipValue(scaleManip.scaleIndex(), scaleVal);

	// Apply the scale
	float fInitialFloatBlindDataValue = GetInitialFloatBlindDataValue(m_obAISelectedComponentIndexes[iMagicIndex]);
	float fScaleFactor = (float)(scaleVal.x * scaleVal.y * scaleVal.z);
	
	// If fInitialFloatBlindDataValue == 0 and fScaleFactor > 1, then hack the fInitialFloatBlindDataValue or you get stuck at 0
	if((fInitialFloatBlindDataValue == 0.0f)  && (fScaleFactor > 1.0f)) fInitialFloatBlindDataValue = 0.01f;

	// Apply the scale
	float fNewFloatBlindDataValue = fInitialFloatBlindDataValue * fScaleFactor;

	// Clamp it
	if(fNewFloatBlindDataValue > GetMax())
	{
		fNewFloatBlindDataValue = GetMax();
	}
	if(fNewFloatBlindDataValue < GetMin())
	{
		fNewFloatBlindDataValue = GetMin();
	}

	// Apply new value
	MFnMesh obFnMeshNode(GetMeshDagPath());
	// cout<<"obFnMeshNode.setFloatBlindData("<<m_obAISelectedComponentIndexes[iMagicIndex]<<", MFn::kMeshVertComponent, "<<GetBlindDataID()<<", \""<<GetAttributeName()<<"\", "<<fNewFloatBlindDataValue<<");"<<endl;
	MStatus obStatus = obFnMeshNode.setFloatBlindData(m_obAISelectedComponentIndexes[iMagicIndex], MFn::kMeshVertComponent, GetBlindDataID(), GetAttributeName(), fNewFloatBlindDataValue);
	if (!obStatus)
	{
		OutputError("Could not set blind data "+ GetAttributeName() +" for vert on "+ obFnMeshNode.fullPathName() +" because :"+ obStatus.errorString());
//		return MS::kFailure;
	}
	return MManipData(42.0f);
}
