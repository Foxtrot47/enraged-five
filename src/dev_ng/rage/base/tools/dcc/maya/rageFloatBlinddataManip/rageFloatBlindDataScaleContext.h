#ifndef _RAGE_FLOATBLINDDATA_SCALE_CONTEXT
#define _RAGE_FLOATBLINDDATA_SCALE_CONTEXT
// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
// #include <maya/MPxManipContainer.h>
#include <maya/MPxSelectionContext.h>
// #include <maya/MPxContextCommand.h>
#include <maya/MModelMessage.h>
// #include <maya/MFnPlugin.h>
// #include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>

// Manipulators
// #include <maya/MFnScaleManip.h>


/////////////////////////////////////////////////////////////
//
// rageFloatBlindDataScaleContext
//
// This class is a simple context for supporting a scale manipulator.
//
/////////////////////////////////////////////////////////////

class rageFloatBlindDataScaleContext : public MPxSelectionContext
{
public:
	rageFloatBlindDataScaleContext();
	virtual void	toolOnSetup(MEvent &event);
	virtual void	toolOffCleanup();

	// Callback issued when selection list changes
	static void updateManipulators(void * data);

	// Accessors and modifiers
	void SetBlindDataID(unsigned uBlindDataID) 
	{
		m_uBlindDataID = uBlindDataID; /* updateManipulators(this); */ 
		m_bDefaultGiven = false;
	}
	unsigned GetBlindDataID() const { return m_uBlindDataID; }
	void SetAttributeName(const MString& strAttributeName)
	{
		m_strBindDataLongName = strAttributeName; /* updateManipulators(this); */
		m_bDefaultGiven = false;
	}
	const MString& GetAttributeName() const { return m_strBindDataLongName; }
	void SetDrawValues(bool bDrawValues) { m_bDrawValues = bDrawValues; /* updateManipulators(this); */ }
	bool GetDrawValues() const { return m_bDrawValues; }
	void SetDrawNormals(bool bDrawNormals) { m_bDrawNormals = bDrawNormals; /* updateManipulators(this); */ }
	bool GetDrawNormals() const { return m_bDrawNormals; }
	void SetMin(float fMin) { m_fMin = fMin; /* updateManipulators(this); */ }
	float GetMin() const { return m_fMin; }
	void SetMax(float fMax) { m_fMax = fMax; /* updateManipulators(this); */ }
	float GetMax() const { return m_fMax; }
	void SetDefault(float fDefault) 
	{ 
		m_fDefault = fDefault; /* updateManipulators(this); */ 
		m_bDefaultGiven = true;
	}
	float GetDefault() const { return m_fDefault; }
	bool GetDefaultGiven() const { return m_bDefaultGiven; }

private:
	MCallbackId id1;

	unsigned	m_uBlindDataID;
	MString		m_strBindDataLongName;
	bool		m_bDrawValues;
	bool		m_bDrawNormals;
	float		m_fMin;
	float		m_fMax;
	float		m_fDefault;
	bool		m_bDefaultGiven;
};
#endif
