// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
// #include <maya/MPxManipContainer.h>
// #include <maya/MPxSelectionContext.h>
// #include <maya/MPxContextCommand.h>
// #include <maya/MModelMessage.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>
#include <maya/MStatus.h>
#include <maya/MObject.h>

// Manipulators
// #include <maya/MFnScaleManip.h>

// #include "rageFloatBlindDataContext.h"
#include "rageFloatBlindDataScaleManip.h"
#include "rageFloatBlindDataScaleCmd.h"

///////////////////////////////////////////////////////////////////////
//
// The following routines are used to register/unregister
// the context and manipulator
//
///////////////////////////////////////////////////////////////////////

MStatus initializePlugin(MObject obj)
{
	MStatus status;
	MFnPlugin plugin(obj, "Alias", "6.0", "Any");

	// Scale
    status = plugin.registerContextCommand("rageFloatBlindDataScaleCmd",
										   &rageFloatBlindDataScaleCmd::creator);
	if (!status) 
	{
		OutputError("Error registering rageFloatBlindDataScaleCmd command");
		cout<<status.errorString()<<endl;
		return status;
	}

	status = plugin.registerNode("rageFloatBlindDataScaleManip", rageFloatBlindDataScaleManip::id, 
								 &rageFloatBlindDataScaleManip::creator, &rageFloatBlindDataScaleManip::initialize,
								 MPxNode::kManipContainer);
	if (!status) 
	{
		OutputError("Error registering rageFloatBlindDataScaleManip node");
		cout<<status.errorString()<<endl;
		return status;
	}

	return status;
}


MStatus uninitializePlugin(MObject obj)
{
	MStatus status;
	MFnPlugin plugin(obj);

	// Scale
	status = plugin.deregisterContextCommand("rageFloatBlindDataScaleCmd");
	if (!status) {
		OutputError("Error deregistering rageFloatBlindDataScaleCmd command");
		return status;
	}

	status = plugin.deregisterNode(rageFloatBlindDataScaleManip::id);
	if (!status) {
		OutputError("Error deregistering rageFloatBlindDataScaleManip node");
		return status;
	}

	return status;
}
