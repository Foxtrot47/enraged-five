//#include <maya/MIOStream.h>
//#include <stdio.h>
//#include <stdlib.h>
//
//#include <maya/MFn.h>
//#include <maya/MPxNode.h>
//#include <maya/MPxManipContainer.h>
//#include <maya/MPxSelectionContext.h>
//#include <maya/MPxContextCommand.h>
//#include <maya/MModelMessage.h>
//#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
//#include <maya/MItSelectionList.h>
//#include <maya/MPoint.h>
#include <maya/MVector.h>
//#include <maya/MDagPath.h>
#include <maya/MManipData.h>
//#include <maya/MSelectionList.h>
#include <maya/MItMeshVertex.h>
#include <maya/MFnComponent.h>
//#include <maya/MStatus.h>
//#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>

//
//Manipulators
#include <maya/MFnScaleManip.h>
#include <maya/MFnRotateManip.h>
//
//#include "rageFloatBlindDataContext.h"
#include "rageFloatBlindDataManip.h"
//#include "rageFloatBlindDataScaleCmd.h"
// #include "rageFloatBlindDataManip.h"
#define POINT_SIZE				20.0	

void OutputError(const MString& obStrErrorMessage)
{
	cout<<obStrErrorMessage<<endl;
	MGlobal::displayError(obStrErrorMessage);
}


MStatus addArrayAttribute(MString strAttributeName, MObject obNode)
{
	MFnDependencyNode obFnNode(obNode);

	// Add an array atrribute to store all the floatblinddata vectors in
	MStatus obStatus;
	MPlug obPlug = obFnNode.findPlug(strAttributeName, &obStatus);
	if (obPlug.isNull())
	{
		// Attribute does not exist, so create it
		MFnNumericAttribute attributeFn;
		MObject attr = attributeFn.create(strAttributeName, strAttributeName, MFnNumericData::k3Double);
		attributeFn.setArray(true);
		obFnNode.addAttribute(attr, MFnDependencyNode::kLocalDynamicAttr);
		MPlug obPlug = obFnNode.findPlug(strAttributeName, &obStatus);
		if (obPlug.isNull())
		{
			OutputError("Could not add "+ strAttributeName +" attribute.");
			return MS::kFailure;
		}
	}
	return MS::kSuccess;
}

rageFloatBlindDataManip::rageFloatBlindDataManip() : 
m_afInitialFloatBlindDataValues(NULL),
m_abVertexIsSelected(NULL),
m_uBlindDataID(0),
m_strBindDataLongName(""),
m_bDrawValues(true),
m_bDrawNormals(true),
m_fMin(0.0f),
m_fMax(1.0f),
m_fDefault(1.0f),
m_bDefaultGiven(false)
{ 
	// The constructor must not call createChildren for user-defined
	// manipulators.
}


rageFloatBlindDataManip::~rageFloatBlindDataManip() 
{
	// m_afInitialFloatBlindDataValues should always be either NULL or an allocated array
	// of MPoint.
	//
	delete [] m_afInitialFloatBlindDataValues;
	delete [] m_abVertexIsSelected;
}

MStatus rageFloatBlindDataManip::initialize()
{ 
	MStatus obStatus;
	obStatus = MPxManipContainer::initialize();
	return obStatus;
}

MStatus rageFloatBlindDataManip::connectToDependNode(const MObject &obMeshNode)
{
	MStatus obStatus;

	// Set up some stuff
	delete [] m_afInitialFloatBlindDataValues;
	delete [] m_abVertexIsSelected;
	int iTotalNoOfVertsInMesh = MFnMesh(obMeshNode).numVertices();

	// Fill in vector array
	m_afInitialFloatBlindDataValues = new float[iTotalNoOfVertsInMesh];
	for(int i=0; i<iTotalNoOfVertsInMesh; i++) m_afInitialFloatBlindDataValues[i] = m_fDefault;
	
	// Fill in the bool array
	m_abVertexIsSelected = new bool[iTotalNoOfVertsInMesh];
	for(int i=0; i<iTotalNoOfVertsInMesh; i++) m_abVertexIsSelected[i] = false;

	// I need to create a MFnMesh node, but if you create one from a MObject the blind data functions do not work
	// So do some evil hacks to get a MDagPath to create it out of instead
	MDagPath obDagPathMeshNode;
	MDagPath::getAPathTo(obMeshNode, obDagPathMeshNode);
	MFnMesh obFnMeshNode(obDagPathMeshNode);

	// Throw on a dummy attribute, just so the manipulator has something to hang off
	MPlug obDummyPlug = obFnMeshNode.findPlug( DUMMY_ATTRIBUTE_NAME, &obStatus );
	if((!obStatus) || (obDummyPlug.isNull()))
	{
		// Unable to find dummy plug, so add it
		MFnNumericAttribute obFnNumericAttribute;
		MObject obAttr = obFnNumericAttribute.create(DUMMY_ATTRIBUTE_NAME, "rhd", MFnNumericData::kFloat , 0, &obStatus);
		if(!obStatus)
		{
			OutputError(MString("Unable to create dummy attribute ")+ DUMMY_ATTRIBUTE_NAME);
			return MS::kFailure;
		}
		obFnNumericAttribute.setHidden(true);

		obStatus = obFnMeshNode.addAttribute(obAttr, MFnDependencyNode::kLocalDynamicAttr);
		if(!obStatus)
		{
			OutputError(MString("Unable to add dummy attribute ")+ DUMMY_ATTRIBUTE_NAME +" to "+ obFnMeshNode.fullPathName());
			return MS::kFailure;
		}
	}

	// Do I have everything I need to do blind data stuff?
	if(GetBlindDataID() == 0)
	{
		// No blinddata id given
		return MStatus::kSuccess;
	}
	if(GetAttributeName() == 0)
	{
		// No blinddata id given
		return MStatus::kSuccess;
	}
	if(!m_bDefaultGiven)
	{
		// No default value given
		return MStatus::kSuccess;
	}

	// If mesh currently does not have blinddata, add it
	// See if blinddata id already used in scene
	if(!obFnMeshNode.isBlindDataTypeUsed(GetBlindDataID(),&obStatus))
	{
		// cout<<"Creating blind data"<<endl;
		// Not already used so create
		// Create blind data type
		MStringArray astrBindDataNames;
		astrBindDataNames.append(GetAttributeName());
		MStringArray formatNames;
		formatNames.append("float");

		// Add it
		obStatus = obFnMeshNode.createBlindDataType(GetBlindDataID(), astrBindDataNames, astrBindDataNames, formatNames);
		if(!obStatus)
		{
			OutputError("Error creating blind data");
			return MS::kFailure;
		}
	}

	// Iterate through the components, storing initial positions and find the
	// m_obCentroidOfAllSelectedComponents.  Add manipToPlug callbacks for each component.
	//
	m_obAISelectedComponentIndexes.clear();
	MItMeshVertex iter(m_obMeshDagPath, GetSelectedComponents());
	for (; !iter.isDone(); iter.next())
	{
		MString obStrVertNoAsString;
		obStrVertNoAsString += iter.index();
		m_obAISelectedComponentIndexes.append(iter.index());
		m_abVertexIsSelected[iter.index()] = true;
		m_obCentroidOfAllSelectedComponents += iter.position(MSpace::kWorld);

		if(!obFnMeshNode.hasBlindData(iter.index(), MFn::kMeshVertComponent, GetBlindDataID()))
		{
			// I don't have a floatblinddata value, so create one
			// Set value for the blind data
			obStatus = obFnMeshNode.setFloatBlindData(iter.index(), MFn::kMeshVertComponent, GetBlindDataID(), GetAttributeName(), GetDefault());
			if (!obStatus)
			{
				OutputError("Could not set blind data "+ GetAttributeName() +" for vert "+ obStrVertNoAsString +" on "+ obFnMeshNode.fullPathName());
				return MS::kFailure;
			}
		}

		// Store for future reference
		float fDataOut;
		obStatus = obFnMeshNode.getFloatBlindData(iter.index(), MFn::kMeshVertComponent, GetBlindDataID(), GetAttributeName(), fDataOut);
		if (!obStatus)
		{
			OutputError("Could not get "+ GetAttributeName() +" for vert "+ obStrVertNoAsString +" on "+ obFnMeshNode.fullPathName());
			return MS::kFailure;
		}
		m_afInitialFloatBlindDataValues[iter.index()] = fDataOut;
	}
	m_obCentroidOfAllSelectedComponents = m_obCentroidOfAllSelectedComponents * (1.0f/(float)iter.count());

	MPxManipContainer::connectToDependNode(obMeshNode);

	return obStatus;
}


void rageFloatBlindDataManip::draw(M3dView & view, 
					 const MDagPath & path, 
					 M3dView::DisplayStyle style,
					 M3dView::DisplayStatus obStatus)
{
	MPxManipContainer::draw(view, path, style, obStatus);

	// Do I have everything I need to draw stuff?
	if(GetBlindDataID() == 0)
	{
		// No blinddata id given
		view.drawText("No blind data id given", m_obCentroidOfAllSelectedComponents, M3dView::kLeft);
		return;
	}
	if(GetAttributeName() == 0)
	{
		// No blinddata id given
		view.drawText("No attribute name given", m_obCentroidOfAllSelectedComponents, M3dView::kLeft);
		return;
	}

	// Draw values
	if(GetDrawValues())
	{
		// Draw them
		float fOneOverRange = 1.0f / (GetMax() - GetMin());
		char acBuffer[10];
		MFnMesh obFnMeshNode(m_obMeshDagPath.node());
		for(MItMeshVertex iter(m_obMeshDagPath); !iter.isDone(); iter.next())
		{
			if(obFnMeshNode.hasBlindData(iter.index(), MFn::kMeshVertComponent, GetBlindDataID()))
			{
				// Go through verts, drawing as I go
				MPoint vertex = iter.position(MSpace::kWorld);
				float fFloatBlindData;
				obFnMeshNode.getFloatBlindData(iter.index(), MFn::kMeshVertComponent, GetBlindDataID(), GetAttributeName(), fFloatBlindData);
				_snprintf(acBuffer, 9, "%#.2f", fFloatBlindData);				

				// Select colour
				if(m_abVertexIsSelected[iter.index()])
				{
					glColor4f( 1.0f, 0.0f, (fFloatBlindData - GetMin()) * fOneOverRange, 1.0f );
				}
				else
				{
					glColor4f( 0.0f, 1.0f, (fFloatBlindData - GetMin()) * fOneOverRange, 1.0f );
				}

				// Draw it
				view.drawText(acBuffer, vertex, M3dView::kCenter);
			}
		}
	}

	// Draw the normals
	if(GetDrawNormals())
	{
		view.beginGL(); 

		// Query current obStatuse so it can be restored
		//
		bool lightingWasOn = glIsEnabled( GL_LIGHTING ) ? true : false;
		if ( lightingWasOn ) 
		{
			glDisable( GL_LIGHTING );
		}

		float lastPointSize;
		glGetFloatv( GL_POINT_SIZE, &lastPointSize );

		// Set the point size of the vertices
		glPointSize( POINT_SIZE );

		// Get verts
		// First the unselected one
		glColor4f( 0.0f, 1.0f, 0.0f, 1.0f );
		MFnMesh obFnMeshNode(m_obMeshDagPath.node());
		for(MItMeshVertex iter(m_obMeshDagPath); !iter.isDone(); iter.next())
		{
			if((!m_abVertexIsSelected[iter.index()]) && (obFnMeshNode.hasBlindData(iter.index(), MFn::kMeshVertComponent, GetBlindDataID())))
			{
				// Go through verts, drawing as I go
				MPoint vertex = iter.position(MSpace::kWorld);
				float fFloatBlindData;
				obFnMeshNode.getFloatBlindData(iter.index(), MFn::kMeshVertComponent, GetBlindDataID(), GetAttributeName(), fFloatBlindData);

				MVector obNormal;
				iter.getNormal(obNormal, MSpace::kWorld);
				obNormal = obNormal * fFloatBlindData;

				glBegin( GL_LINES );
				glVertex3f( (float)vertex[0], (float)vertex[1], (float)vertex[2] );
				glVertex3f( (float)obNormal.x + (float)vertex[0], (float)obNormal.y + (float)vertex[1], (float)obNormal.z + (float)vertex[2] );
				glEnd();
			}
		}

		// Then the selected one
		glColor4f( 1.0f, 0.0f, 0.0f, 1.0f );
		for(MItMeshVertex iter(m_obMeshDagPath); !iter.isDone(); iter.next())
		{
			if((m_abVertexIsSelected[iter.index()]) && (obFnMeshNode.hasBlindData(iter.index(), MFn::kMeshVertComponent, GetBlindDataID())))
			{
				// Go through verts, drawing as I go
				MPoint vertex = iter.position(MSpace::kWorld);
				float fFloatBlindData;
				obFnMeshNode.getFloatBlindData(iter.index(), MFn::kMeshVertComponent, GetBlindDataID(), GetAttributeName(), fFloatBlindData);

				MVector obNormal;
				iter.getNormal(obNormal, MSpace::kWorld);
				obNormal = obNormal * fFloatBlindData;

				glBegin( GL_LINES );
				glVertex3f( (float)vertex[0], (float)vertex[1], (float)vertex[2] );
				glVertex3f( (float)obNormal.x + (float)vertex[0], (float)obNormal.y + (float)vertex[1], (float)obNormal.z + (float)vertex[2] );
				glEnd();
			}
		}

		// Restore the obStatuse
		if ( lightingWasOn ) 
		{
			glEnable( GL_LIGHTING );
		}
		glPointSize( lastPointSize );

		view.endGL(); 
	}
}

MManipData rageFloatBlindDataManip::centerChangedCallback(unsigned index) 
{
	MObject obj = MObject::kNullObj;

	// If we entered the callback with an invalid index, print an error and
	// return.  Since we registered the callback only for one index, all 
	// invocations of the callback should be for that index.
	//
	//MFnScaleManip scaleManip(m_obManipDagPath);
	//if (index != scaleManip.scaleCenterIndex())
	//{
	//	OutputError("Invalid index in center changed callback!");

	//	// For invalid indices, return vector of 0's
	//	MFnNumericData numericData;
	//	obj = numericData.create( MFnNumericData::k3Double );
	//	numericData.setData(0.0,0.0,0.0);

	//	return obj;
	//}

	// Set the center point for scaling to the m_obCentroidOfAllSelectedComponents of the CV's.
	//
	MFnNumericData numericData;
	obj = numericData.create( MFnNumericData::k3Double );
	numericData.setData(m_obCentroidOfAllSelectedComponents.x,m_obCentroidOfAllSelectedComponents.y,m_obCentroidOfAllSelectedComponents.z);

	return MManipData(obj);
}
