#ifndef _RAGE_FLOATBLINDDATA_SCALE_CMD
#define _RAGE_FLOATBLINDDATA_SCALE_CMD
// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
// #include <maya/MPxManipContainer.h>
// #include <maya/MPxSelectionContext.h>
#include <maya/MPxContextCommand.h>
// #include <maya/MModelMessage.h>
// #include <maya/MFnPlugin.h>
// #include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>

// Manipulators
// #include <maya/MFnScaleManip.h>
#include "rageFloatBlindDataScaleContext.h"

/////////////////////////////////////////////////////////////
//
// rageFloatBlindDataScaleCmd
//
// This is the command that will be used to create instances
// of our context.
//
/////////////////////////////////////////////////////////////

class rageFloatBlindDataScaleCmd : public MPxContextCommand
{
public:
	rageFloatBlindDataScaleCmd() {}
	virtual ~rageFloatBlindDataScaleCmd() { delete m_pobRageFloatBlindDataScaleContext; }
	virtual MPxContext * makeObj();
	static void* creator();

	// For MELing
	virtual	MStatus		doEditFlags();
	virtual MStatus		doQueryFlags();
	virtual MStatus		appendSyntax();

protected:
    rageFloatBlindDataScaleContext*		m_pobRageFloatBlindDataScaleContext;
};
#endif
