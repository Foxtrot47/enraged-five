#ifndef _RAGE_FLOATBLINDDATA_MANIP
#define _RAGE_FLOATBLINDDATA_MANIP
// #include <maya/MIOStream.h>
// #include <stdio.h>
// #include <stdlib.h>

// #include <maya/MFn.h>
// #include <maya/MPxNode.h>
#include <maya/MPxManipContainer.h>
#include <maya/MStringArray.h>
// #include <maya/MPxSelectionContext.h>
// #include <maya/MPxContextCommand.h>
// #include <maya/MModelMessage.h>
// #include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
// #include <maya/MItSelectionList.h>
// #include <maya/MPoint.h>
// #include <maya/MVector.h>
// #include <maya/MDagPath.h>
// #include <maya/MManipData.h>
// #include <maya/MSelectionList.h>
// #include <maya/MItSurfaceCV.h>
// #include <maya/MFnComponent.h>

// Manipulators
// #include <maya/MFnScaleManip.h>

#define DUMMY_ATTRIBUTE_NAME	"rageFloatBlindDataManip_dummyAttribute"

void OutputError(const MString& obStrErrorMessage);

/////////////////////////////////////////////////////////////
//
// rageFloatBlindDataManip
//
// This class implements the component scaling example.
//
/////////////////////////////////////////////////////////////

class rageFloatBlindDataManip : public MPxManipContainer
{
public:
	rageFloatBlindDataManip();
	virtual ~rageFloatBlindDataManip();
	
	static MStatus initialize();
	virtual MStatus connectToDependNode(const MObject &node);

	virtual void draw(M3dView &view, 
					  const MDagPath &path, 
					  M3dView::DisplayStyle style,
					  M3dView::DisplayStatus status);

	virtual void setMeshDagPath(const MDagPath& dagPath) 
	{
		m_obMeshDagPath = dagPath;
	}

	virtual void setComponentObject(const MObject& obj) 
	{
		m_obSelectedComponents = obj;
	}

	// Callback function
	virtual MManipData manipChangedCallback(unsigned index) = 0;
	virtual MManipData centerChangedCallback(unsigned index);

	MDagPath m_obManipDagPath;

	// Accessors
	MObject GetSelectedComponents() const {return m_obSelectedComponents;}
	MDagPath GetMeshDagPath() const {return m_obMeshDagPath;}
	float	GetInitialFloatBlindDataValue(int i) const {return m_afInitialFloatBlindDataValues[i];}

	// Accessors and modifiers
	void SetBlindDataID(unsigned uBlindDataID) 
	{
		m_uBlindDataID = uBlindDataID;
		m_bDefaultGiven = false;
	}
	unsigned GetBlindDataID() const { return m_uBlindDataID; }
	void SetAttributeName(const MString& strAttributeName) 
	{
		m_strBindDataLongName = strAttributeName; 
		m_bDefaultGiven = false;
	}
	const MString& GetAttributeName() const { return m_strBindDataLongName; }
	void SetDrawValues(bool bDrawValues) { m_bDrawValues = bDrawValues;}
	bool GetDrawValues() const { return m_bDrawValues; }
	void SetDrawNormals(bool bDrawNormals) { m_bDrawNormals = bDrawNormals;}
	bool GetDrawNormals() const { return m_bDrawNormals; }
	void SetMin(float fMin) { m_fMin = fMin;}
	float GetMin() const { return m_fMin; }
	void SetMax(float fMax) { m_fMax = fMax;}
	float GetMax() const { return m_fMax; }
	void SetDefault(float fDefault) 
	{
		m_fDefault = fDefault;
		m_bDefaultGiven = true;
	}
	float GetDefault() const { return m_fDefault; }

private:

	MDagPath m_obMeshDagPath;
	MObject m_obSelectedComponents;

	MPoint		m_obCentroidOfAllSelectedComponents;

	float*		m_afInitialFloatBlindDataValues;
	bool*		m_abVertexIsSelected;

	unsigned dummyPlugIndex;

private:
	unsigned	m_uBlindDataID;
	MString		m_strBindDataLongName;
	bool		m_bDrawValues;
	bool		m_bDrawNormals;
	float		m_fMin;
	float		m_fMax;
	float		m_fDefault;
	bool		m_bDefaultGiven;

protected:
	MIntArray	m_obAISelectedComponentIndexes;
};
#endif
