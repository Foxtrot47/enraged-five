// 
// uvatlas/uvatlas.cpp
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "uvatlas.h"

#include "convolver.h"

#define _WIN32_WINNT 0x500

#include "atl/array.h"
#include "grcore/device.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#include "system/xtl.h"


#if __D3D
#include <d3d9.h>
#include <dxerr9.h>
#include <d3dx9mesh.h>
#if !__OPTIMIZED
#pragma comment(lib,"d3dx9d.lib")
#pragma comment(lib,"d3d9.lib")
#else
#pragma comment(lib,"d3dx9.lib")
#pragma comment(lib,"d3d9.lib")
#endif
#endif

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if(p) { (p)->Release(); (p)=NULL; } }
#endif

namespace rage {

uvaUvAtlasHelper::uvaUvAtlasHelper(int width,int height)
{
	m_DxMesh=m_NewDxMesh	=NULL;
	m_Adjacency				=NULL;
	m_RemapArray			=NULL;
	m_TextureRenderTarget	=NULL;
	m_RenderTarget			=NULL;
	m_TextureImage			=NULL;
	m_TexWidth				=width;
	m_TexHeight				=height;
}

bool uvaUvAtlasHelper::InitAtlas(ID3DXMesh& mesh,float epsilon,unsigned long* adjacency)
{
	if (adjacency==NULL)
	{
		HRESULT hr = mesh.GenerateAdjacency( epsilon, adjacency );
		if (FAILED(hr))
		{
			Errorf("failed to generate adjacency");
			return false;
		}
	}
	else 
		m_Adjacency=adjacency;

	// RAY - had to add bowties for the saloon  used to be D3DXCLEAN_OPTIMIZATION
	ID3DXMesh*	ptrMesh = &mesh;
	D3DXCleanMesh( D3DXCLEAN_SIMPLIFICATION, ptrMesh,adjacency, &ptrMesh,adjacency,NULL);

	// make sure it's a valid mesh:
	HRESULT hr = D3DXValidMesh(ptrMesh, adjacency, NULL);
	m_DxMesh=ptrMesh;
	if (S_OK != hr) {
		Warningf("couldn't validate mesh");
	}
	
	return true;
}

HRESULT WINAPI sProgress(FLOAT UNUSED_PARAM(percentDone),LPVOID)
{
	return S_OK;
}


ID3DXMesh* uvaUvAtlasHelper::Create(float maxStretch,u32 width,u32 height,float	gutter,int textureIndex)
{
	m_TexWidth=width;
	m_TexHeight=height;
	m_GutterSize=gutter;

	if (!m_DxMesh)
		return NULL;

	HRESULT hr;
	UINT numCharts;
	
	// hack to get around the fact that the first set of texture coordinates are screwed up by the uvatlas creator:
	u32* indicesOld;
	m_DxMesh->LockIndexBuffer(0x0,(LPVOID*)&indicesOld);
	
	uvaUvAtlasHelper::Vertex* verticesOld;
	m_DxMesh->LockVertexBuffer(0x0,(LPVOID*)&verticesOld);	

	hr = D3DXUVAtlasCreate(m_DxMesh,
									  0, // parameterized based on stretch
									  maxStretch,
									  m_TexWidth,
									  m_TexHeight,
									  m_GutterSize,
									  textureIndex,
									  m_Adjacency,
#if D3DX_SDK_VERSION >= 29
									  NULL,	// pcFalseEdges
#endif
									  NULL,  // pfIMTArray
									  sProgress,
									  0.0001f,
									  NULL,
									   0,
									  &m_NewDxMesh,
									  NULL,
									  &m_RemapArray,
									  &maxStretch,
									  &numCharts);
    if (FAILED(hr))
	{
		switch(hr)
		{
		case D3DXERR_INVALIDMESH: Errorf("UV Atlas creation failed: Non-manifold mesh"); break;
		default:
			Errorf("UV Atlas creation failed: \n"
				   "Try increasing the max number of charts or max stretch"); 
			break;
		}
		return NULL;
	}
	
	// create a brand new mesh.  After reviewing the previous data and the data with the uv mapping applied,
	// it seems that the uv atlasing is welding verts where it shouldn't!
	D3DVERTEXELEMENT9 declaration[MAX_FVF_DECL_SIZE];
	m_NewDxMesh->GetDeclaration(declaration);
	ID3DXMesh* finalMesh;
	DWORD options=D3DXMESH_SYSTEMMEM|D3DXMESH_32BIT;
	D3DXCreateMesh(m_NewDxMesh->GetNumFaces(),m_NewDxMesh->GetNumVertices()*3,options,declaration,GRCDEVICE.GetCurrent(),&finalMesh);
	
	u32* indicesNew;
	m_NewDxMesh->LockIndexBuffer(0x0,(LPVOID*)&indicesNew);
	uvaUvAtlasHelper::Vertex* verticesNew;
	m_NewDxMesh->LockVertexBuffer(0x0,(LPVOID*)&verticesNew);

	u32* indicesFinal;
	finalMesh->LockIndexBuffer(0x0,(LPVOID*)&indicesFinal);
	uvaUvAtlasHelper::Vertex* verticesFinal;
	finalMesh->LockVertexBuffer(0x0,(LPVOID*)&verticesFinal);

	// copy old vertex data *EXCEPT* for the second texture coordinate set:
	bool* vertexAlreadySet= new bool[finalMesh->GetNumVertices()];
	for (u32 idx=0;idx<(u32)finalMesh->GetNumVertices();idx++)
	{
		vertexAlreadySet[idx]=false;
	}

	DWORD addedVert=m_DxMesh->GetNumVertices();
	DWORD numNewVerts=m_NewDxMesh->GetNumVertices();
	DWORD numFaces=m_NewDxMesh->GetNumFaces();
	numFaces=numFaces;
	numNewVerts=numNewVerts;
	for (u32 idx=0;idx<(u32)m_NewDxMesh->GetNumFaces()*3;idx++)
	{
		verticesFinal[indicesOld[idx]].m_Position		= verticesOld[indicesOld[idx]].m_Position;
		verticesFinal[indicesOld[idx]].m_Normal			= verticesOld[indicesOld[idx]].m_Normal;
		verticesFinal[indicesOld[idx]].m_Color			= verticesOld[indicesOld[idx]].m_Color;
		verticesFinal[indicesOld[idx]].m_Texcoord		= verticesOld[indicesOld[idx]].m_Texcoord;
		verticesFinal[indicesOld[idx]].m_BlendWeight0	= verticesOld[indicesOld[idx]].m_BlendWeight0;
		for (int i=0;i<NUM_BLEND_WEIGHTS;i++)
			verticesFinal[indicesOld[idx]].m_BlendWeight[i] = verticesOld[indicesOld[idx]].m_BlendWeight[i];
		
		// if the vert hasn't already been set, set it...
		if (!vertexAlreadySet[indicesOld[idx]])
		{
			verticesFinal[indicesOld[idx]].m_Texcoord2=verticesNew[indicesNew[idx]].m_Texcoord2;
			vertexAlreadySet[indicesOld[idx]]=true;
			
			indicesFinal[idx]=indicesOld[idx];
		}
		// ...otherwise, add it to the end if it's not the equal:
		else if (!verticesFinal[indicesOld[idx]].m_Texcoord2.IsClose(verticesNew[indicesNew[idx]].m_Texcoord2,10e-5f))
		{
			Assert(addedVert<finalMesh->GetNumVertices());
			verticesFinal[addedVert].m_Position		= verticesOld[indicesOld[idx]].m_Position;
			verticesFinal[addedVert].m_Normal		= verticesOld[indicesOld[idx]].m_Normal;
			verticesFinal[addedVert].m_Color		= verticesOld[indicesOld[idx]].m_Color;
			verticesFinal[addedVert].m_Texcoord		= verticesOld[indicesOld[idx]].m_Texcoord;
			verticesFinal[addedVert].m_BlendWeight0	= verticesOld[indicesOld[idx]].m_BlendWeight0;
			for (int i=0;i<NUM_BLEND_WEIGHTS;i++)
				verticesFinal[addedVert].m_BlendWeight[i] = verticesOld[indicesOld[idx]].m_BlendWeight[i];
	
			// not a typo, this has to be indicesNew:
			verticesFinal[addedVert].m_Texcoord2	= verticesNew[indicesNew[idx]].m_Texcoord2;
			
			indicesFinal[idx]=addedVert++;
			vertexAlreadySet[indicesFinal[idx]]=true;
		}
		else
			indicesFinal[idx]=indicesOld[idx];
	}

	delete [] vertexAlreadySet;

	m_NewDxMesh->UnlockVertexBuffer();
	m_DxMesh->UnlockVertexBuffer();

	m_NewDxMesh->UnlockIndexBuffer();
	m_DxMesh->UnlockIndexBuffer();

	
	m_NewDxMesh->Release();

	// create and copy the final mesh:
	D3DXCreateMesh(finalMesh->GetNumFaces(),addedVert,options,declaration,GRCDEVICE.GetCurrent(),&m_NewDxMesh);
	u32* indicesCopyLast;
	m_NewDxMesh->LockIndexBuffer(0x0,(LPVOID*)&indicesCopyLast);
	uvaUvAtlasHelper::Vertex* verticesCopyLast;
	m_NewDxMesh->LockVertexBuffer(0x0,(LPVOID*)&verticesCopyLast);

	for (u32 idx=0;idx<(u32)m_NewDxMesh->GetNumFaces()*3;idx++)
	{
		indicesCopyLast[idx]=indicesFinal[idx];
	}

	for (u32 idx=0;idx<(u32)m_NewDxMesh->GetNumVertices();idx++)
	{
		verticesCopyLast[idx]=verticesFinal[idx];
	}
	
	m_NewDxMesh->UnlockVertexBuffer();
	m_NewDxMesh->UnlockIndexBuffer();

	finalMesh->UnlockVertexBuffer();
	finalMesh->UnlockIndexBuffer();
	finalMesh->Release();

	// set attributes:
	DWORD* attribsOld;
	m_DxMesh->LockAttributeBuffer(0x0,&attribsOld);
	DWORD* attribsNew;
	m_NewDxMesh->LockAttributeBuffer(0x0,&attribsNew);
	for (DWORD i=0;i<m_NewDxMesh->GetNumFaces();i++)
		attribsNew[i]=attribsOld[i];
	m_NewDxMesh->UnlockAttributeBuffer();
	m_DxMesh->UnlockAttributeBuffer();

	//DWORD* aAdjacencyIn = new DWORD[m_NewDxMesh->GetNumFaces() * 3]; 
	//m_NewDxMesh->GenerateAdjacency(1e-6f,aAdjacencyIn);
	//D3DXValidMesh(m_NewDxMesh,aAdjacencyIn,NULL);

	return m_NewDxMesh;
}

IDirect3DSurface9* uvaUvAtlasHelper::CreateOffscreenSurfaceStatic(u32 width,u32 height)
{
	IDirect3DSurface9* target;
	GRCDEVICE.GetCurrent()->CreateOffscreenPlainSurface(width,height,D3DFMT_A8R8G8B8,D3DPOOL_SYSTEMMEM,&target,NULL);
	return target;
}

bool uvaUvAtlasHelper::Render()
{
	// create dest texture to copy render texture to:
	if (m_TextureRenderTarget==NULL)
	{
		m_TextureRenderTarget=CreateOffscreenSurface();
	}

	if (m_RenderTarget==NULL)
	{
		// create a render target to handle the texture:
		m_RenderTarget=grcTextureFactory::GetInstance().CreateRenderTarget("lightmapbuffer",grcrtPermanent,m_TexWidth,m_TexHeight,32);
	}
	
	// render the texture:
	grcTextureFactory::GetInstance().LockRenderTarget(0, m_RenderTarget, NULL);
	// make sure there's a viewport:
	grcViewport viewport;
	grcViewport::SetCurrent(&viewport);

	Color32 clearColor;
	clearColor.Set(ILLEGAL_TEXEL_COLOR);
	GRCDEVICE.Clear(true,clearColor,false,grcDepthFarthest,0);
	grcViewport::GetCurrent()->Screen();

	// render triangles into render target:
	RenderToTexture();

	// we're done rendering into the render target:
	grcTextureFactory::GetInstance().UnlockRenderTarget(0);

	// copy the render target back:
	IDirect3DSurface9* surfaceSource=NULL;
	static_cast<IDirect3DTexture9*>(m_RenderTarget->GetTexturePtr())->GetSurfaceLevel(0,&surfaceSource);
	GRCDEVICE.GetCurrent()->GetRenderTargetData(surfaceSource, m_TextureRenderTarget);

	surfaceSource->Release();

	grcViewport::SetCurrent(NULL);
	
	return true;
}

void uvaUvAtlasHelper::CopyTextureUvSet2To1()
{	
	Assert(m_NewDxMesh!=NULL);

	// get face vertices:
	uvaUvAtlasHelper::Vertex* vertices;
	m_NewDxMesh->LockVertexBuffer(0x0,(LPVOID*)&vertices);
	for (u32 vert=0;vert<(u32)m_NewDxMesh->GetNumVertices();vert++)
	{
		vertices[vert].m_Texcoord=vertices[vert].m_Texcoord2;
	}
	m_NewDxMesh->UnlockVertexBuffer();
}

Color32* uvaUvAtlasHelper::LockImageStatic(IDirect3DSurface9& surface)
{
	D3DLOCKED_RECT rect;
	HRESULT hr=surface.LockRect(&rect, NULL, D3DLOCK_READONLY);
	if (hr!=S_OK)
		return NULL;

	return (Color32*)rect.pBits;
}

void uvaUvAtlasHelper::UnlockImageStatic(IDirect3DSurface9& surface)
{
	surface.UnlockRect();
}

bool uvaUvAtlasHelper::LoadTexture(const char* fileName)
{
	// allocate it if we don't already have it:
	if (m_TextureRenderTarget==NULL)
	{
		m_TextureRenderTarget=CreateOffscreenSurface();
	}

	// load the surface:
	D3DXIMAGE_INFO imageInfo;
	if (FAILED(D3DXLoadSurfaceFromFile(m_TextureRenderTarget,
									   NULL,
									   NULL,
									   fileName,
									   NULL,
									   D3DX_FILTER_NONE,	
									   0x0,
									   &imageInfo)))
		return false;

	// make sure it matches the current lightmap, if there was one:
	if (imageInfo.Width!=(UINT)GetTexWidth() && imageInfo.Height!=(UINT)GetTexHeight())
	{
		return false;
	}

	return true;
}

bool uvaUvAtlasHelper::SaveTexture(const char* fileName,grcImage::Format UNUSED_PARAM(outputFormat),bool applyGutter,int blurSize)
{
	bool result=false;

	grcImage* image=m_TextureImage;
	LPD3DXTEXTUREGUTTERHELPER gutterHelper=NULL;

	// read back from the texture (this will probably only work on the PC):
	Color32* pixels=LockImage();
	if (pixels==NULL)
		goto FAIL;

	if (m_TextureImage==NULL)
	{
		// separate this out for testing:
		Color32 illegalColor(0xffff0000);

		// create the final image that we'll use to save the texture as:
		image=grcImage::Create(m_TexWidth,m_TexHeight,1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
		if (image==NULL)
		{
			Errorf("couldn't create image (%dx%d)",m_TexWidth,m_TexHeight);
			goto FAIL;
		}

		// read from surface into image:
		for (int y=0;y<m_TexHeight;y++)
		{
			for (int x=0;x<m_TexWidth;x++)
			{
				if (pixels[x+(y*m_TexWidth)].GetColor()==ILLEGAL_TEXEL_COLOR)
					image->SetPixel(x,y,illegalColor.GetColor());
				else 
					image->SetPixel(x,y,pixels[x+(y*m_TexWidth)].GetColor());
			}
		}

		// blur the image if told to do so:
		if (blurSize>0)
		{
			uvaConvolver convolver(blurSize);
			convolver.Blur(*image,illegalColor);
		}
	}

	
#ifdef CRAP
	// reformat the image:
	if (outputFormat!=grcImage::A8R8G8B8)
	{
		image=image->Reformat(outputFormat);
		if (image==NULL)
		{
			Errorf("couldn't create image with output format %d",outputFormat);
			goto FAIL;
		}
	}
#endif

	// save the image:
	result=image->SaveDDS(fileName);
	if (result==false)
	{
		Errorf("couldn't save image '%s'",fileName);
		goto FAIL;
	}

	result=true;

	// apply the texture gutter:
	if (applyGutter)
	{
		CopyTextureUvSet2To1();

		// create gutter helper:
		HRESULT hr=D3DXCreateTextureGutterHelper(m_TexWidth,m_TexHeight,m_NewDxMesh,m_GutterSize,&gutterHelper);
		if (hr!=S_OK)
		{
			Errorf("Couldn't create the texture gutter helper.");
			goto FAIL;
		}

		//image->SaveDDS("c:\\good.dds");

		D3DXVECTOR2*	texelData=new D3DXVECTOR2[m_TexHeight*m_TexWidth];
		BYTE*			classData=new BYTE[m_TexHeight*m_TexWidth];
		gutterHelper->GetTexelMap(texelData);
		gutterHelper->GetGutterMap(classData);

		// apply the gutter:
		grcTexture* texture=grcTextureFactory::GetInstance().Create(image);
		LPDIRECT3DTEXTURE9 texturePtr=(LPDIRECT3DTEXTURE9)texture->GetTexturePtr();
		gutterHelper->ApplyGuttersTex(texturePtr);

		// save the texture:
		D3DXSaveTextureToFile(fileName,D3DXIFF_DDS,texturePtr,NULL);

		delete [] texelData;
		delete [] classData;
	}

FAIL:
	if (gutterHelper)
		gutterHelper->Release();
	if (image)
		image->Release();
	m_TextureImage=NULL;
	if (m_TextureRenderTarget)
		UnlockImage();

	return result;
}

void uvaUvAtlasHelper::AddSmearedBorder(bool addedTexels[],Color32 pixelsRead[],Color32 pixelsWrite[],float borderThickness)
{
	// copy out pixels to illegal values from legal values to create a one pixel border around each chart:

	for (int y=0;y<GetTexHeight();y++)
	{
		for (int x=0;x<GetTexWidth();x++)
		{	
#define CHECK_AND_SET_TEXEL(__xOffset,__yOffset,__xLimit,__yLimit)											\
			if ((__xLimit) && (__yLimit) && GetTexel(addedTexels,x+(__xOffset),y+(__yOffset))!=true)		\
			{																								\
				Color32 color=GetTexel(pixelsWrite,x+(__xOffset),y+(__yOffset));							\
				if (color.GetColor()!=ILLEGAL_TEXEL_COLOR)													\
				{																							\
					SetTexel(pixelsWrite,x,y,color);														\
					SetTexel(addedTexels,x,y,true);															\
					goto foundPixel;																		\
				}																							\
			}

			Color32 color=GetTexel(pixelsRead,x,y);
			if (color.GetColor()==ILLEGAL_TEXEL_COLOR)
			{
				for (int i=1;i<=borderThickness;i++)
				{
					// left and right:
					CHECK_AND_SET_TEXEL(-i,0,x>=i,true);
					CHECK_AND_SET_TEXEL(i,0,x<(GetTexWidth()-i),true);

					// top and bottom:
					CHECK_AND_SET_TEXEL(0,-i,true,y>=i);
					CHECK_AND_SET_TEXEL(0,i,true,y<(GetTexHeight()-i));

					for (int j=1;j<=borderThickness;j++)
					{
						// diagonal left and right upper:
						CHECK_AND_SET_TEXEL(-i,-j,x>=i,y>=j);
						CHECK_AND_SET_TEXEL(i,-j,x<(GetTexWidth()-i),y>=j);

						// diagonal left and right lower:
						CHECK_AND_SET_TEXEL(-i,j,x>=i,y<(GetTexHeight()-j));
						CHECK_AND_SET_TEXEL(i,j,x<(GetTexWidth()-i),y<(GetTexHeight()-j));
					}
				}
			}
foundPixel: 
			continue;
		}
	}
}


} // namespace rage
