// 
// uvatlas/convolver.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "convolver.h"

#include "grcore/image.h"
#include "math/amath.h"

namespace rage {

void uvaConvolver::SetRadius(int sz)
{
	sz=Min(Max(1,sz),248);
	if (m_Radius==sz) 
		return;
	m_KernelSize=1+sz*2;
	m_Radius=sz;
	m_Kernel.Reset();
	m_Kernel.Resize(m_KernelSize);
	for (int i=0;i<m_KernelSize;i++)
		m_Kernel[i]=0;
	m_Mult.Reset();
	m_Mult.Resize(m_KernelSize);
	for (int i=0;i<m_KernelSize;i++)
	{
		m_Mult[i].Resize(256);
		for (int j=0;j<256;j++)
			m_Mult[i][j]=0;
	}

	int sum=0;
	for (int i=1;i<sz;i++){
		int szi=sz-i;
		m_Kernel[sz+i]=m_Kernel[szi]=szi*szi;
		sum+=m_Kernel[szi]+m_Kernel[szi];
		for (int j=0;j<256;j++){
			m_Mult[sz+i][j]=m_Mult[szi][j]=m_Kernel[szi]*j;
		}
	}
	m_Kernel[sz]=sz*sz;
	sum+=m_Kernel[sz];
	for (int j=0;j<256;j++){
		m_Mult[sz][j]=m_Kernel[sz]*j;
	}
}

void uvaConvolver::Blur(grcImage& image,Color32 illegalColor)
{
	int sum,cr,cg,cb;
	int read,yi,ym,riw;
	int ri;
	
	int iw=image.GetWidth();
	int ih=image.GetHeight();
	int wh=iw*ih;

	int* r=new int[wh];
	int* g=new int[wh];
	int* b=new int[wh];
	int* a=new int[wh];

	Color32* colorArray=image.GetColor32();
	for (int i=0;i<wh;i++){
		r[i]=colorArray[i].GetRed();
		g[i]=colorArray[i].GetGreen();
		b[i]=colorArray[i].GetBlue();
		a[i]=colorArray[i].GetAlpha();
	}

	int* r2=new int[wh];
	int* g2=new int[wh];
	int* b2=new int[wh];

	yi=0;
	for (int yl=0;yl<ih;yl++){
		for (int xl=0;xl<iw;xl++){
			cb=cg=cr=sum=0;
			int ri=xl-m_Radius;
			for (int i=0;i<m_KernelSize;i++){
				read=ri+i;
				if (read>=0 && read<iw)
				{
					read+=yi;

					if (r[read]!=illegalColor.GetRed()   ||
						g[read]!=illegalColor.GetGreen() ||
						b[read]!=illegalColor.GetBlue()  ||
						a[read]!=illegalColor.GetAlpha())
					{
						cr+=m_Mult[i][r[read]];
						cg+=m_Mult[i][g[read]];
						cb+=m_Mult[i][b[read]];
						sum+=m_Kernel[i];
					}

				}
			}
			ri=yi+xl;
			if (sum==0)
			{
				r2[ri]=r[ri];
				g2[ri]=g[ri];
				b2[ri]=b[ri];
			}
			else
			{
				r2[ri]=cr/sum;
				g2[ri]=cg/sum;
				b2[ri]=cb/sum;
			}
		
		}
		yi+=iw;
	}

	yi=0;
	for (int yl=0;yl<ih;yl++){
		ym=yl-m_Radius;
		riw=ym*iw;
		for (int xl=0;xl<iw;xl++){
			cb=cg=cr=sum=0;
			ri=ym;
			read=xl+riw;
			for (int i=0;i<m_KernelSize;i++){
				if (ri<ih && ri>=0 && (
					(r2[read]!=illegalColor.GetRed()   ||
					 g2[read]!=illegalColor.GetGreen() ||
				 	 b2[read]!=illegalColor.GetBlue() )))
				{
					cr+=m_Mult[i][r2[read]];
					cg+=m_Mult[i][g2[read]];
					cb+=m_Mult[i][b2[read]];
					sum+=m_Kernel[i];
				}
				ri++;
				read+=iw;
			}

			if (sum!=0)
				colorArray[xl+yi].Set(cr/sum,cg/sum,cb/sum);
		}
		yi+=iw;
	}

	delete [] r;
	delete [] g;
	delete [] b;

	delete [] r2;
	delete [] g2;
	delete [] b2;
}

} // namespace rage
