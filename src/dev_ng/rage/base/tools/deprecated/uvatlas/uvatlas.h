// 
// uvatlas/uvatlas.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef UVATLAS_UVATLAS_H
#define UVATLAS_UVATLAS_H

#include "grcore/config.h"
#include "grcore/image.h"
#include "system/noncopyable.h"
#include "system/xtl.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

#include <d3d9types.h>
#include <d3dx9mesh.h>

struct ID3DXMesh;
struct ID3DXBuffer;

#if !__D3D
#error "This code only compiles with Direct3D."
#endif

namespace rage {

class grcRenderTarget;

// PURPOSE:
//	This class is a wrapper around the DirectX UVatlas functions.
//	Given a mesh in the directx format, it will produce a unique UV mapping 
//	for a single texture for a mesh based one of the mesh's texture coordinate sets.
//	This will be used by the lightmap generation code, and it's possible this code 
//	could also be used by LOD generation code for terrain or other objects (basically 
//	low level of details would contain only one texture wrapped around an object for 
//	speed and performance reasons.) The class is intended to be subclassed so that 
//	you can override the texture generation function.
class uvaUvAtlasHelper
{
public:
	enum
	{
		NUM_BLEND_WEIGHTS=6
	};

	// Vertex format
	struct Vertex
	{
		Vector3		m_Position;
		Vector3		m_Normal;
		Vector4		m_BlendWeight[NUM_BLEND_WEIGHTS];
		D3DCOLOR	m_Color;
		Vector2		m_Texcoord;
		Vector2		m_Texcoord2;
		int			m_BlendWeight0; // yes, this shouldn't be an array like the other blendweights
	};

	uvaUvAtlasHelper(int texWidth,int texHeight);

	bool InitAtlas(ID3DXMesh& mesh,float epsilon,unsigned long* adjacency);
	ID3DXMesh* Create(float	maxStretch,
					  u32	width,
					  u32	height,
					  float	gutter,
					  int	textureIndex);
	
	bool Render();
	bool SaveTexture(const char* fileName,grcImage::Format outputFormat,bool applyGutter=true,int blurSize=0);
	bool LoadTexture(const char* fileName);

	void AddSmearedBorder(bool addedTexels[],Color32 pixelsRead[],Color32 pixelsWrite[],float borderThickness);

	template<class _Type> _Type GetTexel(_Type* array,int x,int y)
	{
		return array[(x)+((y)*m_TexWidth)];
	}

	template<class _Type> static _Type GetTexel(_Type* array,int x,int y,int width)
	{
		return array[(x)+((y)*width)];
	}

	template<class _Type>  void SetTexel(_Type* array,int x,int y,_Type value)
	{
		array[(x)+((y)*m_TexWidth)]=value;
	}

	template<class _Type> static void SetTexel(_Type* array,int x,int y,int width,_Type value)
	{
		array[(x)+((y)*width)]=value;
	}

	Color32* LockImage()
	{
		Assert(m_TextureRenderTarget);
		return LockImageStatic(*m_TextureRenderTarget);
	}

	void UnlockImage()
	{
		Assert(m_TextureRenderTarget);
		UnlockImageStatic(*m_TextureRenderTarget);
	}

	Color32* LockImageStatic(IDirect3DSurface9& surface);
	void UnlockImageStatic(IDirect3DSurface9& surface);


	int GetTexWidth() const;
	int GetTexHeight() const;
	float GetGutterSize() const
	{
		return m_GutterSize;
	}

	
protected:
	enum {ILLEGAL_TEXEL_COLOR=0x00000000};

	IDirect3DSurface9* CreateOffscreenSurface()
	{
		return CreateOffscreenSurfaceStatic(m_TexWidth,m_TexHeight);
	}

	IDirect3DSurface9* CreateOffscreenSurfaceStatic(u32 width,u32 height);

	void CopyTextureUvSet2To1();
	virtual void RenderToTexture() = 0;

	IDirect3DSurface9*	m_TextureRenderTarget;
	grcImage*			m_TextureImage;
	ID3DXMesh*			m_NewDxMesh;
	float				m_GutterSize;
	grcRenderTarget*	m_RenderTarget;

private:
	int					m_TexWidth;
	int					m_TexHeight;
	ID3DXMesh*			m_DxMesh;

	ID3DXBuffer*		m_RemapArray;
	unsigned long*		m_Adjacency;

};

inline int uvaUvAtlasHelper::GetTexWidth() const
{
	return m_TexWidth;
}

inline int uvaUvAtlasHelper::GetTexHeight() const
{
	return m_TexHeight;
}

} // namespace rage

#endif // UVATLAS_UVATLAS_H
