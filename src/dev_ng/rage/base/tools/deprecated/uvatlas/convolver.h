// 
// uvatlas/convolver.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef UVATLAS_CONVOLVER_H
#define UVATLAS_CONVOLVER_H

#include "atl/array.h"
#include "vector/color32.h"

namespace rage {

	class grcImage;

// PURPOSE:
// One of my first steps with Processing. I am a fan
// of blurring. Especially as you can use blurred images
// as a base for other effects. So this is something I
// might get back to in later experiments.
//
// What you see is an attempt to implement a Gaussian Blur algorithm
// which is exact but fast. I think that this one should be
// relatively fast because it uses a special trick by first
// making a horizontal blur on the original image and afterwards
// making a vertical blur on the pre-processed image. This
// is a mathematical correct thing to do and reduces the
// calculation a lot.
//
// In order to avoid the overhead of function calls I unrolled
// the whole convolution routine in one method. This may not
// look nice, but brings a huge performance boost.
//
// NOTES:
// Fast Gaussian Blur v1.3
// by Mario Klingemann http://incubator.quasimondo.com
// v1.1: I replaced some multiplications by additions
//       and added aome minor pre-caclulations.
//       Also add correct rounding for float->int conversion
//
// v1.2: I completely got rid of all floating point calculations
//       and speeded up the whole process by using a
//       precalculated multiplication table. Unfortunately
//       a precalculated division table was becoming too
//       huge. But maybe there is some way to even speed
//       up the divisions.
//
// v1.3: Fixed a bug that caused blurs that start at y>0
//	 to go wrong. Thanks to Jeroen Schellekens for 
//       finding it!
class uvaConvolver
{
public:
	uvaConvolver(int sz)
	{
		m_Radius=m_KernelSize=0;
		SetRadius(sz);
	}

	void SetRadius(int sz);
	void Blur(grcImage& image,Color32 illegalColor);

private:
	int						m_Radius;
	int						m_KernelSize;
	atArray<int>			m_Kernel;
	atArray< atArray<int> > m_Mult;
};

} // namespace rage

#endif // PRTTOOLS_CONVOLVER_H
