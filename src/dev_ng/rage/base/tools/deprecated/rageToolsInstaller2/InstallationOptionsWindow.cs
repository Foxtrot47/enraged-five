using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace rageToolsInstaller2
{
	public partial class InstallationOptionsWindow : rageToolsInstaller2.BaseInstallWindow
	{
		public InstallationOptionsWindow()
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Installation Options";
			this.infoLabel.Text = "Select items to install.";
		}

		public bool InstallTools
		{
			get	
			{	
				return ToolsCheckBox.Checked;
			}
			set
			{
				ToolsCheckBox.Checked = value;
			}
		}

		public bool InstallShaders
		{
			get
			{
				return ShadersCheckBox.Checked;
			}
			set
			{
				ShadersCheckBox.Checked = value;
			}
		}

		public bool InstallAddToStartMenu
		{
			get
			{
				return AddToStartMenuCheckBox.Checked;
			}
			set
			{
				AddToStartMenuCheckBox.Checked = value;
			}
		}

		public bool InstallExplorerExtensions
		{
			get
			{
				return ExplorerExtensionsCheckBox.Checked;
			}
			set
			{
				ExplorerExtensionsCheckBox.Checked = value;
			}
		}

		public bool InstallPowerShell
		{
			get
			{
				return PowerShellCheckBox.Checked;
			}
			set
			{
				PowerShellCheckBox.Checked = value;
			}
		}

		public bool InstallExplorerMtlExtensions
		{
			get
			{
				return ExplorerMtlExtensionsCheckBox.Checked;
			}
			set
			{
				ExplorerMtlExtensionsCheckBox.Checked = value;
			}
		}

		public bool InstallGEOs
		{
			get
			{
				return GEOsCheckBox.Checked;
			}
			set
			{
				GEOsCheckBox.Checked = value;
			}
		}

		public bool EnableInstallTools
		{
			set
			{
				ToolsCheckBox.Enabled = value;
			}
		}

		public bool EnableInstallShaders
		{
			set
			{
				ShadersCheckBox.Enabled = value;
			}
        }
	}
}

