namespace rageToolsInstaller2
{
	partial class ProjectAndDCCWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label3 = new System.Windows.Forms.Label();
			this.ProjectComboBox = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.DCCComboBox = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.DCCComboBox);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.SetChildIndex(this.label1, 0);
			this.groupBox1.Controls.SetChildIndex(this.infoLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.label4, 0);
			this.groupBox1.Controls.SetChildIndex(this.DCCComboBox, 0);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(18, 170);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(469, 24);
			this.label3.TabIndex = 20;
			this.label3.Text = "(If your Project is not listed, select Rage.)";
			this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// ProjectComboBox
			// 
			this.ProjectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.ProjectComboBox.Location = new System.Drawing.Point(141, 146);
			this.ProjectComboBox.Name = "ProjectComboBox";
			this.ProjectComboBox.Size = new System.Drawing.Size(346, 21);
			this.ProjectComboBox.TabIndex = 19;
			this.ProjectComboBox.SelectedIndexChanged += new System.EventHandler(this.ProjectComboBox_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(48, 82);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 16);
			this.label1.TabIndex = 18;
			this.label1.Text = "Project";
			// 
			// DCCComboBox
			// 
			this.DCCComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.DCCComboBox.Location = new System.Drawing.Point(174, 140);
			this.DCCComboBox.Name = "DCCComboBox";
			this.DCCComboBox.Size = new System.Drawing.Size(346, 21);
			this.DCCComboBox.TabIndex = 22;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(48, 143);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(161, 16);
			this.label4.TabIndex = 21;
			this.label4.Text = "Digital Content Creator";
			// 
			// ProjectAndDCCWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.ProjectComboBox);
			this.Name = "ProjectAndDCCWindow";
			this.Controls.SetChildIndex(this.groupBox1, 0);
			this.Controls.SetChildIndex(this.cancelButton, 0);
			this.Controls.SetChildIndex(this.titleLabel, 0);
			this.Controls.SetChildIndex(this.nextButton, 0);
			this.Controls.SetChildIndex(this.backButton, 0);
			this.Controls.SetChildIndex(this.ProjectComboBox, 0);
			this.Controls.SetChildIndex(this.label3, 0);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox ProjectComboBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox DCCComboBox;
		private System.Windows.Forms.Label label4;
	}
}
