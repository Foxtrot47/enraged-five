namespace rageToolsInstaller2
{
	partial class UnZipWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.ToolsProgressBar = new System.Windows.Forms.ProgressBar();
			this.ToolsLabel = new System.Windows.Forms.Label();
			this.ShadersLabel = new System.Windows.Forms.Label();
			this.ShadersProgressBar = new System.Windows.Forms.ProgressBar();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.ShadersLabel);
			this.groupBox1.Controls.Add(this.ShadersProgressBar);
			this.groupBox1.Controls.Add(this.ToolsLabel);
			this.groupBox1.Controls.Add(this.ToolsProgressBar);
			this.groupBox1.Controls.SetChildIndex(this.ToolsProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.ToolsLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.infoLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.ShadersProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.ShadersLabel, 0);
			// 
			// ToolsProgressBar
			// 
			this.ToolsProgressBar.Location = new System.Drawing.Point(48, 83);
			this.ToolsProgressBar.Name = "ToolsProgressBar";
			this.ToolsProgressBar.Size = new System.Drawing.Size(469, 23);
			this.ToolsProgressBar.TabIndex = 1;
			// 
			// ToolsLabel
			// 
			this.ToolsLabel.AutoSize = true;
			this.ToolsLabel.Location = new System.Drawing.Point(51, 64);
			this.ToolsLabel.Name = "ToolsLabel";
			this.ToolsLabel.Size = new System.Drawing.Size(86, 13);
			this.ToolsLabel.TabIndex = 2;
			this.ToolsLabel.Text = "Expanding Tools";
			// 
			// ShadersLabel
			// 
			this.ShadersLabel.AutoSize = true;
			this.ShadersLabel.Location = new System.Drawing.Point(51, 120);
			this.ShadersLabel.Name = "ShadersLabel";
			this.ShadersLabel.Size = new System.Drawing.Size(99, 13);
			this.ShadersLabel.TabIndex = 4;
			this.ShadersLabel.Text = "Expanding Shaders";
			// 
			// ShadersProgressBar
			// 
			this.ShadersProgressBar.Location = new System.Drawing.Point(48, 139);
			this.ShadersProgressBar.Name = "ShadersProgressBar";
			this.ShadersProgressBar.Size = new System.Drawing.Size(469, 23);
			this.ShadersProgressBar.TabIndex = 3;
			// 
			// UnZipWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Name = "UnZipWindow";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ProgressBar ToolsProgressBar;
		private System.Windows.Forms.Label ToolsLabel;
		private System.Windows.Forms.Label ShadersLabel;
		private System.Windows.Forms.ProgressBar ShadersProgressBar;
	}
}
