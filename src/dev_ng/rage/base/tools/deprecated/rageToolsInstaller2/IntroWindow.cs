using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace rageToolsInstaller2
{
	public partial class IntroWindow : rageToolsInstaller2.BaseInstallWindow
	{
		public IntroWindow()
		{
			InitializeComponent();

			// Override stuff for this window
			this.backButton.Enabled = false;
			this.infoLabel.Text = "Welcome to the Rage Tools Installer.\n\nWork through this installer to install the correct version of the Rage tools for your project.";

			string strLastFileModifiedTime = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).LastWriteTime.ToString();
			InstallerInfoLabel.Text = "Installer Build Time : " + strLastFileModifiedTime;
		}
	}
}

