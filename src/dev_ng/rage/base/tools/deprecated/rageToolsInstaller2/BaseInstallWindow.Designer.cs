namespace rageToolsInstaller2
{
	public partial class BaseInstallWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseInstallWindow));
			this.infoLabel = new System.Windows.Forms.Label();
			this.backButton = new System.Windows.Forms.Button();
			this.nextButton = new System.Windows.Forms.Button();
			this.titleLabel = new System.Windows.Forms.Label();
			this.cancelButton = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// infoLabel
			// 
			this.infoLabel.Location = new System.Drawing.Point(48, 24);
			this.infoLabel.Name = "infoLabel";
			this.infoLabel.Size = new System.Drawing.Size(472, 40);
			this.infoLabel.TabIndex = 0;
			this.infoLabel.Text = "Please provide the following information about where you are storing the Rage Too" +
				"l modules in Local.";
			// 
			// backButton
			// 
			this.backButton.Enabled = false;
			this.backButton.Location = new System.Drawing.Point(303, 351);
			this.backButton.Name = "backButton";
			this.backButton.Size = new System.Drawing.Size(88, 24);
			this.backButton.TabIndex = 17;
			this.backButton.Text = "< Back";
			// 
			// nextButton
			// 
			this.nextButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.nextButton.Location = new System.Drawing.Point(399, 351);
			this.nextButton.Name = "nextButton";
			this.nextButton.Size = new System.Drawing.Size(88, 24);
			this.nextButton.TabIndex = 11;
			this.nextButton.Text = "Next >";
			this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
			// 
			// titleLabel
			// 
			this.titleLabel.BackColor = System.Drawing.Color.Transparent;
			this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
			this.titleLabel.Image = ((System.Drawing.Image)(resources.GetObject("titleLabel.Image")));
			this.titleLabel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.titleLabel.Location = new System.Drawing.Point(-1, -1);
			this.titleLabel.Name = "titleLabel";
			this.titleLabel.Size = new System.Drawing.Size(504, 72);
			this.titleLabel.TabIndex = 15;
			this.titleLabel.Text = "\r\n     Rage Tools Installer\r\n";
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(207, 351);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(88, 24);
			this.cancelButton.TabIndex = 14;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.BackColor = System.Drawing.Color.Transparent;
			this.groupBox1.Controls.Add(this.infoLabel);
			this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.groupBox1.Location = new System.Drawing.Point(-33, 67);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(768, 270);
			this.groupBox1.TabIndex = 16;
			this.groupBox1.TabStop = false;
			// 
			// BaseInstallWindow
			// 
			this.AcceptButton = this.nextButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Controls.Add(this.backButton);
			this.Controls.Add(this.nextButton);
			this.Controls.Add(this.titleLabel);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "BaseInstallWindow";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Rage Tools Installer";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		protected System.Windows.Forms.Label infoLabel;
		protected System.Windows.Forms.Button backButton;
		protected System.Windows.Forms.Button nextButton;
		protected System.Windows.Forms.Label titleLabel;
		protected System.Windows.Forms.Button cancelButton;
		public System.Windows.Forms.GroupBox groupBox1;
	}
}