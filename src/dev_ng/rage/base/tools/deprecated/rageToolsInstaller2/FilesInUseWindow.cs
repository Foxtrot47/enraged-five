using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using rageUsefulCSharpToolClasses;

namespace rageToolsInstaller2
{
	public partial class FilesInUseWindow : rageToolsInstaller2.BaseInstallWindow
	{
		private class ApplicationThatNeedsKilling
		{
			public ApplicationThatNeedsKilling(string strName, int iPid)
			{
				m_strApplicationName = strName;
				m_iApplicationPid = iPid;
			}
			private string m_strApplicationName = "";
			public string ApplicationName
			{
				get 
				{
					return m_strApplicationName;
				}
			}
			private int m_iApplicationPid;
			public int ApplicationPid
			{
				get
				{
					return m_iApplicationPid;
				}
			}
		}

		public FilesInUseWindow(List<string> obAStrFilesThatShouldNotBeInUse)
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Files In Use";
			this.infoLabel.Text = "The applications listed below are holding onto files I need to update.";
			this.nextButton.Text = "Ignore";

			m_obAStrFilesThatShouldNotBeInUse = obAStrFilesThatShouldNotBeInUse;

			// Fill out the list of applications
			RecheckList();
		}

		void RecheckList()
		{
			rageStatus obStatus = new rageStatus();
			ApplicationsToKillListView.Items.Clear();
			// ApplicationsToKillListView.Clear();
			m_obApplicationsThatNeedKilling.Clear();
			foreach(string strFileThatShouldNotBeInUse in m_obAStrFilesThatShouldNotBeInUse)
			{
				rageExecuteCommand obCommand = new rageExecuteCommand();
				obCommand.Command = "handle.exe";
				obCommand.Arguments = strFileThatShouldNotBeInUse.Replace("/", "\\");
				obCommand.UpdateLogFileInRealTime = false;
				obCommand.RemoveLog = false;
				obCommand.EchoToConsole = true;
				obCommand.UseBusySpinner = false;
				// ApplicationsToKillTextBox.Text += obCommand.GetDosCommand() + "\n";
				obCommand.Execute(out obStatus);

				// Parse the output of handle
				List<string> obHandleLog = obCommand.GetLogAsArray();
				foreach(string strHandleOutput in obHandleLog)
				{
					if(strHandleOutput.Contains("pid:"))
					{
						// Found an application that needs killing
						int iPosOfPidString = strHandleOutput.IndexOf("pid:");
						string strApplicationName = strHandleOutput.Substring(0, iPosOfPidString - 1).TrimEnd();
						string strPid = strHandleOutput.Substring(iPosOfPidString + 5, 5).TrimStart().TrimEnd();
						int iPid = Int32.Parse(strPid);

						// Does it already exist in the list of applications?
						bool bAlreadyInList = false;
						foreach (ApplicationThatNeedsKilling obApp in m_obApplicationsThatNeedKilling)
						{
							if(obApp.ApplicationPid == iPid)
							{
								bAlreadyInList = true;
								break;
							}
						}

						if (!bAlreadyInList)
						{
							m_obApplicationsThatNeedKilling.Add(new ApplicationThatNeedsKilling(strApplicationName, iPid));
						}
					}
				}
			}

			// Add all the applications to the list
			foreach(ApplicationThatNeedsKilling obApp in m_obApplicationsThatNeedKilling)
			{
				ListViewItem obListViewItem = new ListViewItem(obApp.ApplicationName);
				obListViewItem.SubItems.Add(new ListViewItem.ListViewSubItem(obListViewItem, obApp.ApplicationPid.ToString()));
				ApplicationsToKillListView.Items.Add(obListViewItem);
			}

			if (m_obApplicationsThatNeedKilling.Count == 0)
			{
				this.nextButton.Text = "Next >";
			}
		}

		List<string> m_obAStrFilesThatShouldNotBeInUse = new List<string>();
		List<ApplicationThatNeedsKilling> m_obApplicationsThatNeedKilling = new List<ApplicationThatNeedsKilling>();

		private void RefreshButton_Click(object sender, EventArgs e)
		{
			// Make sure the UI editing is thread safe
			// For more info see http://weblogs.asp.net/justin_rogers/articles/126345.aspx
			if (this.InvokeRequired)
			{
				this.BeginInvoke(new MethodInvoker(RecheckList));
			}
			else
			{
				RecheckList();
			}
		}

		private void ShutdownButton_Click(object sender, EventArgs e)
		{
			KillApplications();
			RefreshButton_Click(sender, e);
		}

		private void KillApplications()
		{
			foreach (ApplicationThatNeedsKilling obApp in m_obApplicationsThatNeedKilling)
			{
				// Get application
				Process obProcessToKill = Process.GetProcessById(obApp.ApplicationPid);
				obProcessToKill.Kill();
			}
		}
	}
}

