using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;
using rageUsefulCSharpToolClasses;

namespace rageToolsInstaller2
{
	public partial class UpdateEnvironmentProgressWindow : rageToolsInstaller2.ProgressWindow
	{
		public UpdateEnvironmentProgressWindow(bool bBatchMode)
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Update Progress";
			this.infoLabel.Text = "Updating Environment";
			this.infoLabel.Visible = false;

			// Open the window in the background
			if (!bBatchMode)
			{
				EnableButton(cancelButton, false);
				EnableButton(nextButton, false);
				EnableButton(backButton, false);
				//Thread obWindowThread = new Thread(new ThreadStart(ShowWindow));
				//obWindowThread.Start();
			}
		}

		private DialogResult ShowSetupEnvironmentErrorDialog( string strErrorString, List<string> astrLogLines, string strDefaultResponseToFileProblems)
		{
			// Log it
			Console.WriteLine( "Error : " + strErrorString );

			// Handle error
			switch ( strDefaultResponseToFileProblems.ToLower() )
			{
				case "abort":
					{
						return DialogResult.Cancel;
					}
				case "retry":
					{
						return DialogResult.Retry;
					}
				case "ignore":
					{
						return DialogResult.Ignore;
					}
				default:
					{
						// No default given, so ask user what to do
                        StringBuilder msg = new StringBuilder( strErrorString );
                        msg.Append( Environment.NewLine );

                        foreach ( string strLogLine in astrLogLines )
                        {
                            msg.Append( Environment.NewLine );
                            msg.Append( strLogLine );
                        }

						string caption = "Setup Environment Error";
						MessageBoxButtons buttons = MessageBoxButtons.AbortRetryIgnore;

						// Displays the MessageBox.
                        return MessageBox.Show( msg.ToString(), caption, buttons );
					}
			}
		}


		public void AddStartMenuItems(string strLocalPath, string strCodeLine, string strNetworkPath)
		{
			// Console.WriteLine("AddStartMenuItems(\""+ strLocalPath +"\", \""+ strCodeLine +"\", \""+ strNetworkPath +"\")");
			SetProgressBarMin(StartMenuProgressBar, 0);
			SetProgressBarMax(StartMenuProgressBar, 6);
			SetProgressBarValue(StartMenuProgressBar, 1);
			SetLabelText(StartMenuLabel, "Start Menu : Retrieving Start Menu");

			// Use the Environment class to retrieve the Start Menu on the system
			string strStartMenuFolder = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu);
			// Use the name of the company to create the Start Menu folder for your application
			string strRageStartMenuDirectoryPath = strStartMenuFolder + "\\Rage";
			rageFileUtilities.MakeDir(strRageStartMenuDirectoryPath);

			// Clean out old crap
			//rageStatus obStatus;
			//foreach (string strSubDir in Directory.GetDirectories(strRageStartMenuDirectoryPath, "*"))
			//{
			//    // Does it deserve to live?
			//    if (!strSubDir.Contains("�")) rageFileUtilities.DeleteLocalFolder(strSubDir, out obStatus);
			//}
			//foreach (string strFile in Directory.GetFiles(strRageStartMenuDirectoryPath, "*"))
			//{
			//    // Does it deserve to live?
			//    if (!strFile.Contains("�")) rageFileUtilities.DeleteLocalFile(strFile, out obStatus);
			//}

			// Setup folder for the code line
			strRageStartMenuDirectoryPath += "\\Codeline " + strCodeLine;
			rageFileUtilities.MakeDir(strRageStartMenuDirectoryPath);

			// Add shortcuts
			SetProgressBarValue(StartMenuProgressBar, 2);
			SetLabelText(StartMenuLabel, "Start Menu : Adding Shortcuts to Common Tools");
            string[] astrShortCuts = { (strLocalPath + "tools/bin/geoEdit.exe"), (strLocalPath + "tools/bin/rageBatchExporter.exe"), (strLocalPath + "tools/bin/rageBatchFolderExporter.exe"), (strLocalPath + "tools/bin/rageConvertMBToMA.exe"), (strLocalPath + "tools/bin/rag/rag.exe"), (strLocalPath + "tools/bin/rageViewer.exe"), (strLocalPath + "tools/bin/rageLightMapper.exe"), (strLocalPath + "tools/bin/systrayrfs.exe") };
			string[] astrShortCutArgs = { "", "", "", "", "", "-rag", "", "" };
			string[] astrShortNames = { "Rage GEO Editor", "Batch Maya Exporter", "Batch Maya Folder Exporter", "MB to MA", "RAG", "Rage Viewer", "Rage Light Mapper", "Sys Tray RFS" };
			string[] astrDescriptions = { "The Generic Engine Object Editor", "Export many maya files", "Export all the maya files in one folder", "Convert a Maya Binary file to a Maya Ascii file", "Rage Application GUI", "The Rage Viewer", "Generate Light Maps For a Maya File", "Sys Tray RFS" };
			for (int i = 0; i < astrShortCuts.Length; i++)
			{
				IWshRuntimeLibrary.IWshShell_Class TheShell = new IWshRuntimeLibrary.IWshShell_Class();
				IWshRuntimeLibrary.IWshShortcut_Class shellLink = (IWshRuntimeLibrary.IWshShortcut_Class)TheShell.CreateShortcut(strRageStartMenuDirectoryPath + System.IO.Path.DirectorySeparatorChar.ToString() + astrShortNames[i] + ".lnk");
				shellLink.TargetPath = astrShortCuts[i].Replace("/", System.IO.Path.DirectorySeparatorChar.ToString());
				shellLink.Description = astrDescriptions[i];
				shellLink.Arguments = astrShortCutArgs[i];
				shellLink.Save();
			}

			// Add environments
			SetProgressBarValue(StartMenuProgressBar, 3);
			SetLabelText(StartMenuLabel, "Start Menu : Searching for valid Rage environments");
			string strRageEnvironmentsStartMenuDirectoryPath = strRageStartMenuDirectoryPath + "\\Environments";
			rageFileUtilities.MakeDir(strRageEnvironmentsStartMenuDirectoryPath);

			// Get possibilities
			List<string> obAStrListOfProjectSettingBatFiles = rageFileUtilities.GetFilesAsStringList(strLocalPath + "tools/bin/setup", "SetupEnvironmentFor*And*.bat");

			// Add a menu for each bat file
			SetProgressBarValue(StartMenuProgressBar, 4);
			SetLabelText(StartMenuLabel, "Start Menu : Adding Rage environments");
			foreach (string strProjectSettingBatFile in obAStrListOfProjectSettingBatFiles)
			{
				// Split up the file into the important parts
				int iPosOfProjectStart = strProjectSettingBatFile.ToLower().IndexOf("setupenvironmentfor") + "setupenvironmentfor".Length;
				int iPosOfProjectEnd = strProjectSettingBatFile.ToLower().Substring(iPosOfProjectStart, strProjectSettingBatFile.Length - iPosOfProjectStart).LastIndexOf("and");
				string strProjectPart = strProjectSettingBatFile.ToLower().Substring(iPosOfProjectStart, iPosOfProjectEnd);
				int iPosOfDCCStart = iPosOfProjectStart + iPosOfProjectEnd + 3;
				int iPosOfDCCEnd = strProjectSettingBatFile.Length - 4;
				string strDCC = strProjectSettingBatFile.ToLower().Substring(iPosOfDCCStart, iPosOfDCCEnd - iPosOfDCCStart);

				// Add shortcut
				IWshRuntimeLibrary.IWshShell_Class TheShell = new IWshRuntimeLibrary.IWshShell_Class();
				IWshRuntimeLibrary.IWshShortcut_Class shellLink = (IWshRuntimeLibrary.IWshShortcut_Class)TheShell.CreateShortcut(strRageEnvironmentsStartMenuDirectoryPath + "\\" + strProjectPart + " and " + strDCC + ".lnk");
				shellLink.TargetPath = strProjectSettingBatFile.Replace("/", "\\");
				shellLink.Description = "Set up environment for " + strProjectPart + " and " + strDCC;
				shellLink.Save();
			}

			// Add A link to this installer
			{
				SetProgressBarValue(StartMenuProgressBar, 5);
				SetLabelText(StartMenuLabel, "Start Menu : Adding install shortcuts");
				string strRageUpdateStartMenuDirectoryPath = strRageStartMenuDirectoryPath + "\\Update";
				rageFileUtilities.MakeDir(strRageUpdateStartMenuDirectoryPath);

				// Get myself
				string[] astrPathParts = strNetworkPath.Split("/".ToCharArray());
				string strGetMyTag = "";
				for (int i = astrPathParts.Length - 1; i > -1; i--)
				{
					if (astrPathParts[i] != "")
					{
						strGetMyTag = astrPathParts[i];
						break;
					}
				}

				// Add shortcut
				IWshRuntimeLibrary.IWshShell_Class TheShell = new IWshRuntimeLibrary.IWshShell_Class();
				IWshRuntimeLibrary.IWshShortcut_Class shellLink = (IWshRuntimeLibrary.IWshShortcut_Class)TheShell.CreateShortcut(strRageUpdateStartMenuDirectoryPath + "\\Update tools to " + strGetMyTag + ".lnk");
				shellLink.TargetPath = (strNetworkPath + "/SetUp.bat").Replace("/", "\\");
				shellLink.Description = "Run the Rage Tool Installer To Install the " + strGetMyTag + " tools.";
				shellLink.Save();
			}

			// Done
			SetProgressBarValue(StartMenuProgressBar, 6);
			SetLabelText(StartMenuLabel, "Start Menu : Done");
		}

		public void SetupEnvironment(string strLocalPath, string strProject, string strDCC, string strDefaultResponseToFileProblems, out rageStatus obStatus)
		{
			SetProgressBarMin(EnvironmentProgressBar, 0);
			SetProgressBarMax(EnvironmentProgressBar, 4);
			SetProgressBarValue(EnvironmentProgressBar, 1);
			SetLabelText(EnvironmentLabel, "Environment : Searching for environment file");

			// Work out stuff
			string strBatFileToSetupEnvironment = strLocalPath + "bin/setup/SetupEnvironmentFor" + strProject + "And" + strDCC + ".bat";

			// Setup a bat file to call the bat file, crappy I know
			SetProgressBarValue(EnvironmentProgressBar, 2);
			SetLabelText(EnvironmentLabel, "Environment : Creating bat file");
			string strBatFileToCallBatFileToSetupEnvironment = Environment.GetEnvironmentVariable("TEMP") + "/BatFileToCallBatFileToSetupEnvironment.bat";
			StreamWriter batWriter = File.CreateText(strBatFileToCallBatFileToSetupEnvironment);
			batWriter.WriteLine(strBatFileToSetupEnvironment.Substring(0, strBatFileToSetupEnvironment.IndexOf("/")));
			batWriter.WriteLine("cd " + rageFileUtilities.GetLocationFromPath(strBatFileToSetupEnvironment).Replace('/', '\\'));
			batWriter.WriteLine(strBatFileToSetupEnvironment.Replace('/', '\\'));
			batWriter.Close();

			// Do it
			SetProgressBarValue(EnvironmentProgressBar, 3);
			SetLabelText(EnvironmentLabel, "Environment : " + strBatFileToSetupEnvironment);
			rageExecuteCommand obExecuteCommand = new rageExecuteCommand();
			obExecuteCommand.Command = strBatFileToCallBatFileToSetupEnvironment;
			obExecuteCommand.WorkingDirectory = rageFileUtilities.GetLocationFromPath(strBatFileToSetupEnvironment);
			obExecuteCommand.EchoToConsole = true;
			obExecuteCommand.UseBusySpinner = false;
            obExecuteCommand.TimeOutInSeconds = 5.0f * 60.0f;
            obExecuteCommand.TimeOutWithoutOutputInSeconds = 5.0f * 60.0f;
			obExecuteCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened
                DialogResult obResult = ShowSetupEnvironmentErrorDialog( obStatus.GetErrorString(), obExecuteCommand.GetLogAsArray(), 
                    strDefaultResponseToFileProblems );
				if (obResult == DialogResult.Abort)
				{
					// It is all bad, abandon ship
					return;
				}
				else if (obResult == DialogResult.Retry)
				{
					// Try again
					SetupEnvironment(strLocalPath, strProject, strDCC, strDefaultResponseToFileProblems, out obStatus);
				}
				else if (obResult == DialogResult.Ignore)
				{
					// Ignore
				}
			}

			// Done
			SetProgressBarValue(EnvironmentProgressBar, 4);
			SetLabelText(EnvironmentLabel, "Environment : Done");
		}

		private void Setup3DSMaxVersion(string strLocalPath, string strVersion, string strPlatform, out rageStatus obStatus)
		{
			// Is there a plugin.ini
            string strPluginIniPathAndFilename = Environment.GetEnvironmentVariable("USERPROFILE") + "/Local Settings/Application Data/Autodesk/3dsmax/" + strVersion + " - " + strPlatform + "/enu/plugin.ini";

            if ((File.Exists(strPluginIniPathAndFilename)) && (Directory.Exists(strLocalPath + "max/3dsmax" + strVersion)))
			{
				// The plugin.ini file exists, so I need to hack it.
                string strRagePluginIniPathAndFilename;

                if (strPlatform == "64bit")
                {
                    strRagePluginIniPathAndFilename = strLocalPath + "max/3dsmax" + strVersion + "/plugin64.ini";
                }
                else
                {
                    strRagePluginIniPathAndFilename = strLocalPath + "max/3dsmax" + strVersion + "/plugin.ini";
                }

				// Read in the current plugin.ini
				StreamReader obStreamReader = File.OpenText(strPluginIniPathAndFilename);

				// And write it out again to a temporary file
				string strTempPluginIniPathAndFilename = Environment.GetEnvironmentVariable("TEMP") + "/rageToolsInstaller2_3DSMax_plugin.ini";
				StreamWriter obStreamWriter = File.CreateText(strTempPluginIniPathAndFilename);

				// Go through the file
				string strInput;
				bool bRageIsAlreadyInPluginIni = false;
				while ((strInput = obStreamReader.ReadLine()) != null)
				{
					if (strInput.Contains("RagesMaxIniFile"))
					{
						bRageIsAlreadyInPluginIni = true;
						strInput = ("RagesMaxIniFile=" + strRagePluginIniPathAndFilename.Replace('/', '\\'));
					}
					obStreamWriter.WriteLine(strInput);
				}

				if (!bRageIsAlreadyInPluginIni)
				{
					obStreamWriter.WriteLine("[Include]");
					obStreamWriter.WriteLine("RagesMaxIniFile=" + strRagePluginIniPathAndFilename.Replace('/', '\\'));
				}
				obStreamReader.Close();
				obStreamWriter.Close();

				// Copy over plugin.ini with the temp version
				rageFileUtilities.CopyFile(strTempPluginIniPathAndFilename, strPluginIniPathAndFilename, out obStatus);
				if (!obStatus.Success())
				{
					return;
				}

				// Write out a rage specific plugin.ini
				if ((!File.Exists(strRagePluginIniPathAndFilename)) || ((File.GetAttributes(strRagePluginIniPathAndFilename) & FileAttributes.ReadOnly) == 0))
				{
					obStreamWriter = File.CreateText(strRagePluginIniPathAndFilename);
					obStreamWriter.WriteLine("[Directories]");

					if (strPlatform == "64bit")
					{
						obStreamWriter.WriteLine("Rage plug-ins=" + rageFileUtilities.GetLocationFromPath(strRagePluginIniPathAndFilename).Replace('/', '\\') + "\\plugins\\x64");
					}
					else
					{
						obStreamWriter.WriteLine("Rage plug-ins=" + rageFileUtilities.GetLocationFromPath(strRagePluginIniPathAndFilename).Replace('/', '\\') + "\\plugins");
					}
					obStreamWriter.Close();
				}
			}
			obStatus = new rageStatus();
		}

		public void Setup3DSMax(string strLocalPath, out rageStatus obStatus)
		{
			SetProgressBarMin(MaxProgressBar, 0);
			SetProgressBarMax(MaxProgressBar, 4);
			SetProgressBarValue(MaxProgressBar, 1);
			SetLabelText(MaxLabel, "Adding 3DS Max Support :");

			Setup3DSMaxVersion(strLocalPath, "9", "32bit", out obStatus);
			Setup3DSMaxVersion(strLocalPath, "2009", "32bit", out obStatus);
            Setup3DSMaxVersion(strLocalPath, "2009", "64bit", out obStatus);

			// Done
			SetProgressBarValue(MaxProgressBar, 4);
			SetLabelText(MaxLabel, "Adding 3DS Max Support : Done");
			obStatus = new rageStatus();
		}

		private void RemoveFileExtFromRegistry(string strFileExt)
		{
			RegistryKey obRootRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT");
			RegistryKey obSubRegistryKey = obRootRegistryKey.OpenSubKey(strFileExt);
			if (obSubRegistryKey != null)
			{
				obSubRegistryKey.Close();
				obRootRegistryKey.DeleteSubKeyTree(strFileExt);
			}

			obRootRegistryKey = rageRegistry.GetRegistryKey("HKEY_LOCAL_MACHINE/SOFTWARE/Classes");
			obSubRegistryKey = obRootRegistryKey.OpenSubKey(strFileExt);
			if (obSubRegistryKey != null)
			{
				obSubRegistryKey.Close();
				obRootRegistryKey.DeleteSubKeyTree(strFileExt);
			}
			obRootRegistryKey = rageRegistry.GetRegistryKey("HKEY_CURRENT_USER/Software/Microsoft/Windows/CurrentVersion/Explorer/FileExts");
			obSubRegistryKey = obRootRegistryKey.OpenSubKey(strFileExt);
			if (obSubRegistryKey != null)
			{
				obSubRegistryKey.Close();
				obRootRegistryKey.DeleteSubKeyTree(strFileExt);
			}
		}

		public void AddExplorerExtensions(string strLocalPath)
		{
			SetProgressBarMin(ExplorerExtensionsProgressBar, 0);
			SetProgressBarMax(ExplorerExtensionsProgressBar, 3);
			SetProgressBarValue(ExplorerExtensionsProgressBar, 1);

			// Register filetypes
			string[] astrTextureExtensions = { ".tif", "ACDSee 8.0.tif", "ACDSee 8.0.tiff", "ACDSee.tif", "ACDSee.tiff", ".TIF", "ACDSee 8.0.TIF", "ACDSee 8.0.TIFF", "ACDSee.TIF", "ACDSee.TIFF", "TIFImage.Document" };
			foreach (string strTextureExtension in astrTextureExtensions)
			{
				RegistryKey obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTextureExtension + "/shell/GeoEdit");
				obRegistryKey.SetValue("", "Edit GEO Export Texture Options");
				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTextureExtension + "/shell/GeoEdit/command");
				obRegistryKey.SetValue("", strLocalPath.Replace('/','\\') +"tools\\bin\\rageEditTextureGeos.bat \"%1\"");
			}

			string[] astrGeoExtensions = { ".geo" };
			foreach (string strGeoExtension in astrGeoExtensions)
			{
				RemoveFileExtFromRegistry(strGeoExtension);
				RegistryKey obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strGeoExtension + "/shell/GeoEdit");
				obRegistryKey.SetValue("", "Edit GEO in GeoEdit");
				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strGeoExtension + "/shell/GeoEdit/command");
				obRegistryKey.SetValue("", strLocalPath.Replace('/','\\') +"tools\\bin\\geoedit \"%1\"");

				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strGeoExtension + "/shell/Notepad");
				obRegistryKey.SetValue("", "Edit GEO in Notepad");
				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strGeoExtension + "/shell/Notepad/command");
				obRegistryKey.SetValue("", "notepad \"%1\"");
			}

			string[] astrTypeExtensions = { ".type", "type_auto_file" };
			foreach (string strTypeExtension in astrTypeExtensions)
			{
				RemoveFileExtFromRegistry(strTypeExtension);

				RegistryKey obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTypeExtension + "/shell/RageOpenOnPCAsEntity");
				obRegistryKey.SetValue("", "Rage : Open on PC as entity");
				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTypeExtension + "/shell/RageOpenOnPCAsEntity/command");
				obRegistryKey.SetValue("", strLocalPath.Replace('/', '\\') + "tools\\bin\\rageViewer.exe -autoframe -nodrawablecontainer -file=\"%1\"");

				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTypeExtension + "/shell/RageOpenOnPCAsFragment");
				obRegistryKey.SetValue("", "Rage : Open on PC as fragment");
				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTypeExtension + "/shell/RageOpenOnPCAsFragment/command");
                obRegistryKey.SetValue("", strLocalPath.Replace('/', '\\') + "tools\\bin\\rageViewer.exe -autoframe -nodrawablecontainer -fragfile=\"%1\"");

				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTypeExtension + "/shell/RageOpenOn360AsEntity");
				obRegistryKey.SetValue("", "Rage : Open on 360 as entity");
				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTypeExtension + "/shell/RageOpenOn360AsEntity/command");
				obRegistryKey.SetValue("", strLocalPath.Replace('/', '\\') + "base\\xenon\\rageViewer.bat -autoframe -nodrawablecontainer -file=\"%1\"");

				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTypeExtension + "/shell/RageOpenOn360AsFragment");
				obRegistryKey.SetValue("", "Rage : Open on 360 as fragment");
				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTypeExtension + "/shell/RageOpenOn360AsFragment/command");
				obRegistryKey.SetValue("", strLocalPath.Replace('/', '\\') + "base\\xenon\\rageViewer.bat -autoframe -nodrawablecontainer -fragfile=\"%1\"");

				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTypeExtension + "/shell/Notepad");
				obRegistryKey.SetValue("", "Open in Notepad");
				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strTypeExtension + "/shell/Notepad/command");
				obRegistryKey.SetValue("", "notepad \"%1\"");
			}

			string[] astrWdrExtensions = { ".wdr", "wdr_auto_file" };
			foreach (string strWdrExtension in astrWdrExtensions)
			{
				RemoveFileExtFromRegistry(strWdrExtension);
				RegistryKey obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strWdrExtension + "/shell/RageOpenAsEntity");
				obRegistryKey.SetValue("", "Rage : Open as entity");
				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strWdrExtension + "/shell/RageOpenAsEntity/command");
                obRegistryKey.SetValue("", strLocalPath.Replace('/', '\\') + "tools\\bin\\rageViewer.exe -autoframe -nodrawablecontainer -file=\"%1\"");

				// obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strWdrExtension + "/shell/RageOpenAsFragment");
				// obRegistryKey.SetValue("", "Rage : Open as fragment");
				// obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strWdrExtension + "/shell/RageOpenAsFragment/command");
                // obRegistryKey.SetValue("", strLocalPath.Replace('/', '\\') + "tools\\bin\\rageViewer.exe -autoframe -nodrawablecontainer -fragfile=\"%1\"");
			}

			string[] astrXdrExtensions = { ".xdr", "xdr_auto_file" };
			foreach (string strXdrExtension in astrXdrExtensions)
			{
				RemoveFileExtFromRegistry(strXdrExtension);
				RegistryKey obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strXdrExtension + "/shell/RageOpenAsEntity");
				obRegistryKey.SetValue("", "Rage : Open as entity");
				obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strXdrExtension + "/shell/RageOpenAsEntity/command");
				obRegistryKey.SetValue("", strLocalPath.Replace('/', '\\') + "base\\xenon\\rageViewer.bat -autoframe -nodrawablecontainer -file=\"%1\"");

				// obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strXdrExtension + "/shell/RageOpenAsFragment");
				// obRegistryKey.SetValue("", "Rage : Open as fragment");
				// obRegistryKey = rageRegistry.GetRegistryKey("HKEY_CLASSES_ROOT/" + strXdrExtension + "/shell/RageOpenAsFragment/command");
				// obRegistryKey.SetValue("", strLocalPath.Replace('/', '\\') + "base\\xenon\\rageViewer.bat -autoframe -nodrawablecontainer -fragfile=\"%1\"");
			}

			// Done
			SetProgressBarValue(ExplorerExtensionsProgressBar, 3);
			SetLabelText(StartMenuLabel, "Explorer Extensions : Done");
		}

		public void RegisterAssemblies(string strLocalPath, out rageStatus obStatus)
		{
			obStatus = new rageStatus();
            /*
            // Console.WriteLine("AddStartMenuItems(\""+ strLocalPath +"\", \""+ strCodeLine +"\", \""+ strNetworkPath +"\")");
            SetProgressBarMin(AssembliesProgressBar, 0);
            SetProgressBarMax(AssembliesProgressBar, 6);
            SetLabelText(AssembliesLabel, "Registering Assemblies : Seeking .Net Framework");
            SetProgressBarValue(AssembliesProgressBar, 1);

            //NOTE: We get the .NET runtime directory here so that we can call regasm.exe to register the
            //assemblies. The call to GetRuntimeDirectory() returns the .NET runtime directory for the
            //version of .NET being used by this project, NOT the latest installed version of .NET. If we
            //want to register assemblies that use later versions of .NET, this will have to be updated to
            //look into the registry and find the last .NET version's directory.
            String strPathToRegAsm = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();

            // Register GEOs
            SetLabelText(AssembliesLabel, "Registering Assemblies : GEOs");
            SetProgressBarValue(AssembliesProgressBar, 2);

            // Execute the command
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = strPathToRegAsm.Replace('/', '\\') + "RegAsm.exe";
            obCommand.Arguments = " /verbose /codebase " + strLocalPath.Replace('/', '\\') + "\\tools\\bin\\rageGEODataSet.dll";
            obCommand.WorkingDirectory = strPathToRegAsm;
            obCommand.UpdateLogFileInRealTime = false;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.EchoToConsole = true;
            obCommand.UseBusySpinner = false;
            Console.WriteLine(obCommand.GetDosCommand());
            obCommand.Execute(out obStatus);
            if (!obStatus.Success())
            {
                // Badness happened
                return;
            }

            // Register rageToolCommandCenter.exe
            SetLabelText(AssembliesLabel, "Registering Assemblies : rageToolCommandCenter");
            SetProgressBarValue(AssembliesProgressBar, 3);

            // Execute the command
            obCommand = new rageExecuteCommand();
            obCommand.Command = strPathToRegAsm.Replace('/', '\\') + "RegAsm.exe";
            obCommand.Arguments = " /verbose " + strLocalPath.Replace('/', '\\') + "\\tools\\bin\\rageToolCommandCenter.exe";
            obCommand.WorkingDirectory = strPathToRegAsm;
            obCommand.UpdateLogFileInRealTime = false;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.EchoToConsole = true;
            obCommand.UseBusySpinner = false;
            Console.WriteLine(obCommand.GetDosCommand());
            obCommand.Execute(out obStatus);
            if (!obStatus.Success())
            {
                // Badness happened
                return;
            }

            // Launch it now
            SetLabelText(AssembliesLabel, "Registering Assemblies : Launching rageToolCommandCenter");
            SetProgressBarValue(AssembliesProgressBar, 4);

            // Actually do not launch it directly, instead create a .bat file to launch it
            // and then launch the bat file, this way it is easy for the user to repeat launches
            // Batfile 1 to launch the viewer
            string strBatFilename = Environment.GetEnvironmentVariable("TEMP") + "/rageToolCommandCenterLauncher.bat";
            StreamWriter batWriter = File.CreateText(strBatFilename);
            batWriter.WriteLine((strLocalPath + "tools/bin/rageToolCommandCenter.exe").Replace("/", "\\"));
            batWriter.Write("exit\n");
            batWriter.Close();

            // Batfile 2 to launch Batfile 1!  Madness!
            string strBat2Filename = Environment.GetEnvironmentVariable("TEMP") + "/rageToolCommandCenterLauncherLauncher.bat";
            StreamWriter bat2Writer = File.CreateText(strBat2Filename);
            bat2Writer.WriteLine("start /MIN " + strBatFilename.Replace("/", "\\"));
            bat2Writer.Close();

            // Execute it
            obCommand = new rageExecuteCommand();
            obCommand.Command = strBat2Filename;
            obCommand.EchoToConsole = false;
            obCommand.Execute(out obStatus);

            // Done
            SetLabelText(AssembliesLabel, "Registering Assemblies : Done");
            SetProgressBarValue(AssembliesProgressBar, 6);
            */
        }
	}
}

