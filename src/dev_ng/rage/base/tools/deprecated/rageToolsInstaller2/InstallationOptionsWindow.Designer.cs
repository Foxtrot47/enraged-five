namespace rageToolsInstaller2
{
	partial class InstallationOptionsWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.ToolsCheckBox = new System.Windows.Forms.CheckBox();
            this.ShadersCheckBox = new System.Windows.Forms.CheckBox();
            this.AddToStartMenuCheckBox = new System.Windows.Forms.CheckBox();
            this.ExplorerExtensionsCheckBox = new System.Windows.Forms.CheckBox();
            this.ExplorerMtlExtensionsCheckBox = new System.Windows.Forms.CheckBox();
            this.GEOsCheckBox = new System.Windows.Forms.CheckBox();
            this.PowerShellCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add( this.PowerShellCheckBox );
            this.groupBox1.Controls.Add( this.GEOsCheckBox );
            this.groupBox1.Controls.Add( this.ExplorerMtlExtensionsCheckBox );
            this.groupBox1.Controls.Add( this.ExplorerExtensionsCheckBox );
            this.groupBox1.Controls.Add( this.AddToStartMenuCheckBox );
            this.groupBox1.Controls.Add( this.ShadersCheckBox );
            this.groupBox1.Controls.Add( this.ToolsCheckBox );
            this.groupBox1.Controls.SetChildIndex( this.ToolsCheckBox, 0 );
            this.groupBox1.Controls.SetChildIndex( this.infoLabel, 0 );
            this.groupBox1.Controls.SetChildIndex( this.ShadersCheckBox, 0 );
            this.groupBox1.Controls.SetChildIndex( this.AddToStartMenuCheckBox, 0 );
            this.groupBox1.Controls.SetChildIndex( this.ExplorerExtensionsCheckBox, 0 );
            this.groupBox1.Controls.SetChildIndex( this.ExplorerMtlExtensionsCheckBox, 0 );
            this.groupBox1.Controls.SetChildIndex( this.GEOsCheckBox, 0 );
            this.groupBox1.Controls.SetChildIndex( this.PowerShellCheckBox, 0 );
            // 
            // ToolsCheckBox
            // 
            this.ToolsCheckBox.AutoSize = true;
            this.ToolsCheckBox.Location = new System.Drawing.Point( 115, 67 );
            this.ToolsCheckBox.Name = "ToolsCheckBox";
            this.ToolsCheckBox.Size = new System.Drawing.Size( 52, 17 );
            this.ToolsCheckBox.TabIndex = 1;
            this.ToolsCheckBox.Text = "Tools";
            this.ToolsCheckBox.UseVisualStyleBackColor = true;
            // 
            // ShadersCheckBox
            // 
            this.ShadersCheckBox.AutoSize = true;
            this.ShadersCheckBox.Location = new System.Drawing.Point( 115, 90 );
            this.ShadersCheckBox.Name = "ShadersCheckBox";
            this.ShadersCheckBox.Size = new System.Drawing.Size( 65, 17 );
            this.ShadersCheckBox.TabIndex = 2;
            this.ShadersCheckBox.Text = "Shaders";
            this.ShadersCheckBox.UseVisualStyleBackColor = true;
            // 
            // AddToStartMenuCheckBox
            // 
            this.AddToStartMenuCheckBox.AutoSize = true;
            this.AddToStartMenuCheckBox.Location = new System.Drawing.Point( 115, 113 );
            this.AddToStartMenuCheckBox.Name = "AddToStartMenuCheckBox";
            this.AddToStartMenuCheckBox.Size = new System.Drawing.Size( 112, 17 );
            this.AddToStartMenuCheckBox.TabIndex = 3;
            this.AddToStartMenuCheckBox.Text = "Add to Start Menu";
            this.AddToStartMenuCheckBox.UseVisualStyleBackColor = true;
            // 
            // ExplorerExtensionsCheckBox
            // 
            this.ExplorerExtensionsCheckBox.AutoSize = true;
            this.ExplorerExtensionsCheckBox.Location = new System.Drawing.Point( 115, 136 );
            this.ExplorerExtensionsCheckBox.Name = "ExplorerExtensionsCheckBox";
            this.ExplorerExtensionsCheckBox.Size = new System.Drawing.Size( 118, 17 );
            this.ExplorerExtensionsCheckBox.TabIndex = 4;
            this.ExplorerExtensionsCheckBox.Text = "Explorer Extensions";
            this.ExplorerExtensionsCheckBox.UseVisualStyleBackColor = true;
            // 
            // ExplorerMtlExtensionsCheckBox
            // 
            this.ExplorerMtlExtensionsCheckBox.AutoSize = true;
            this.ExplorerMtlExtensionsCheckBox.Location = new System.Drawing.Point( 115, 182 );
            this.ExplorerMtlExtensionsCheckBox.Name = "ExplorerMtlExtensionsCheckBox";
            this.ExplorerMtlExtensionsCheckBox.Size = new System.Drawing.Size( 135, 17 );
            this.ExplorerMtlExtensionsCheckBox.TabIndex = 5;
            this.ExplorerMtlExtensionsCheckBox.Text = "Explorer Mtl Extensions";
            this.ExplorerMtlExtensionsCheckBox.UseVisualStyleBackColor = true;
            // 
            // GEOsCheckBox
            // 
            this.GEOsCheckBox.AutoSize = true;
            this.GEOsCheckBox.Location = new System.Drawing.Point( 115, 205 );
            this.GEOsCheckBox.Name = "GEOsCheckBox";
            this.GEOsCheckBox.Size = new System.Drawing.Size( 54, 17 );
            this.GEOsCheckBox.TabIndex = 6;
            this.GEOsCheckBox.Text = "GEOs";
            this.GEOsCheckBox.UseVisualStyleBackColor = true;
            // 
            // PowerShellCheckBox
            // 
            this.PowerShellCheckBox.AutoSize = true;
            this.PowerShellCheckBox.Location = new System.Drawing.Point( 115, 159 );
            this.PowerShellCheckBox.Name = "PowerShellCheckBox";
            this.PowerShellCheckBox.Size = new System.Drawing.Size( 98, 17 );
            this.PowerShellCheckBox.TabIndex = 7;
            this.PowerShellCheckBox.Text = "MS PowerShell";
            this.PowerShellCheckBox.UseVisualStyleBackColor = true;
            // 
            // InstallationOptionsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.ClientSize = new System.Drawing.Size( 496, 381 );
            this.Name = "InstallationOptionsWindow";
            this.groupBox1.ResumeLayout( false );
            this.groupBox1.PerformLayout();
            this.ResumeLayout( false );

		}

		#endregion

		private System.Windows.Forms.CheckBox ToolsCheckBox;
		private System.Windows.Forms.CheckBox ExplorerMtlExtensionsCheckBox;
		private System.Windows.Forms.CheckBox ExplorerExtensionsCheckBox;
		private System.Windows.Forms.CheckBox AddToStartMenuCheckBox;
		private System.Windows.Forms.CheckBox ShadersCheckBox;
		private System.Windows.Forms.CheckBox GEOsCheckBox;
        private System.Windows.Forms.CheckBox PowerShellCheckBox;
	}
}
