using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace rageToolsInstaller2
{
	public partial class UpdateReviewWindow : rageToolsInstaller2.BaseInstallWindow
	{
		public UpdateReviewWindow(List<string> obAStrFilesToDelete, List<string> obAStrFilesToUpdate, List<string> obAStrFilesToAdd)
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Update Review";
			this.infoLabel.Visible = false;

			// Fill out the lists of files
			FilesToDeleteTextBox.Text = "";
			foreach(string strFilesToDelete in obAStrFilesToDelete)
			{
				FilesToDeleteTextBox.Text += strFilesToDelete + "\n";
			}
			FilesToUpdateTextBox.Text = "";
			foreach (string strFilesToUpdate in obAStrFilesToUpdate)
			{
				FilesToUpdateTextBox.Text += strFilesToUpdate + "\n";
			}
			FilesToAddTextBox.Text = "";
			foreach (string strFilesToAdd in obAStrFilesToAdd)
			{
				FilesToAddTextBox.Text += strFilesToAdd + "\n";
			}
		}
	}
}

