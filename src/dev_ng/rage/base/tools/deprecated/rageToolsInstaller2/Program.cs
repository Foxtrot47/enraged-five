using System;
using System.IO;
using System.Xml;
using Microsoft.Win32;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using rageUsefulCSharpToolClasses;

namespace rageToolsInstaller2
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static int Main(string[] args)
		{
			//try
			{
				// Kick off
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				//			Application.Run(new Form1());

				// Parse args
				string strProject = "rage";// rageModuleManagement.GuessProjectName(out obStatus);
				string strDCC = "maya8.0";
				string strDefaultResponseToFileProblems = "";
				bool bInstallTools = true;
				bool bInstallShaders = true;
				bool bInstallAddToStartMenu = true;
				bool bInstallExplorerExtensions = true;
				bool bInstallExplorerMtlExtensions = true;
				bool bInstallPowerShell = true;
				bool bIgnorePerforce = false;
				string strUseExistingTools = null;
				bool bBatchMode = false;
                bool bNoDeleteMode = false;
				string strZipFolder = "//sanrage/RageTools/ToolReleases/dev/Latest/";// rageFileUtilities.GetLocationFromPath(System.Reflection.Assembly.GetExecutingAssembly().Location);
				string strSourceCodeLine = "";
				string strDestinationToolFolder = "";
				string strDestinationShaderFolder = "T:/";
				string strLogLocation = Environment.GetEnvironmentVariable("TEMP");

				for (int i = 0; i < args.Length; i++)
				{
					if (args[i] == "-batch")
					{
						// Batch mode
						bBatchMode = true;
					}
					else if (args[i] == "-noExplorerExtensions")
					{
						// Batch mode
						bInstallExplorerExtensions = false;
					}
					else if (args[i] == "-ignorePerforce")
					{
						// Batch mode
						bIgnorePerforce = true;
					}
					else if (args[i] == "-useExistingTools")
					{
						// Batch mode
						strUseExistingTools = args[++i];
					}
					else if (args[i] == "-zips")
					{
						// Batch mode
						strZipFolder = args[++i];
					}
					else if (args[i] == "-codeLine")
					{
						// Given a code line, so use it
						strSourceCodeLine = args[++i];
					}
					else if (args[i] == "-project")
					{
						// Given a code line, so use it
						strProject = args[++i];
					}
					else if (args[i] == "-dcc")
					{
						// Given a code line, so use it
						strDCC = args[++i];
					}
					else if (args[i] == "-logLocation")
					{
						// Given a log location, so use it
						strLogLocation = args[++i];
					}
                    else if ( args[i] == "-defaultErrorResponse" )
                    {
                        strDefaultResponseToFileProblems = args[++i];
                    }
					else if (args[i] == "-destinationToolFolder")
					{
						strDestinationToolFolder = args[++i];
					}
					else if (args[i] == "-nodelete")
                    {
                        bNoDeleteMode = true;
                    }
                    else
                    {
                        // They given me junk
                        Console.WriteLine("Usage: rageToolsInstaller2 [-batch] [-zips <zip folder>]");
                        return -1;
                    }
				}

				// Using the source dir, take a guess at the code line I am installing tools from.
				if (strSourceCodeLine == "")
				{
					strSourceCodeLine = rageFileUtilities.GetFilenameFromFilePath(rageFileUtilities.GetLocationFromPath(strZipFolder));
				}

				// Open a log
				// Start logging
				Console.ResetColor();
				//			rageHtmlLogII obRedirectedConsoleOut = new rageHtmlLogII(Environment.GetEnvironmentVariable("TEMP") + "/rageToolSetupExtension_" + rageFileUtilities.GenerateTimeBasedFilename() + ".html", false, true);
				rageHtmlLogII obRedirectedConsoleOut = new rageHtmlLogII(strLogLocation + "/rageToolSetupExtension_" + rageFileUtilities.GenerateTimeBasedFilename() + ".html", true, true);
				Console.SetOut(obRedirectedConsoleOut);
				string strLastFileModifiedTime = new System.IO.FileInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).LastWriteTime.ToString();
				Console.WriteLine("Build time : " + strLastFileModifiedTime);

				// Standardise slashes
				strZipFolder = strZipFolder.Replace('\\', '/');
				if (!strZipFolder.EndsWith("/")) strZipFolder += "/";

				if ((strDestinationToolFolder == "") || (strDestinationToolFolder == null)) strDestinationToolFolder = "c:/ragetools/" + strSourceCodeLine + "/tools";
				strDestinationToolFolder = strDestinationToolFolder.Replace('\\', '/');
				if (!strDestinationToolFolder.EndsWith("/")) strDestinationToolFolder += "/";

				// Using ExistingTools?
				if (!String.IsNullOrEmpty(strUseExistingTools))
				{
					strDestinationToolFolder = strUseExistingTools;
				}

				rageStatus obStatus;

				// Log Initial data
				strZipFolder = strZipFolder.Replace("\\", "/");
				Console.WriteLine("strDestinationShaderFolder = " + strDestinationShaderFolder);
				Console.WriteLine("strDestinationToolFolder = " + strDestinationToolFolder);
				Console.WriteLine("strProject = " + strProject);
				Console.WriteLine("strDCC = " + strDCC);
				Console.WriteLine("strDefaultResponseToFileProblems = " + strDefaultResponseToFileProblems);
				Console.WriteLine("bBatchMode = " + bBatchMode);
				Console.WriteLine("strZipFolder = " + strZipFolder);
				Console.WriteLine("strUseExistingTools = " + strUseExistingTools);

				// If not batch, then allow the user to change any and all options
				if (!bBatchMode)
				{
					// Open first window
					Console.WriteLine("Opening IntroWindow");
					IntroWindow obIntroWindow = new IntroWindow();
					using (obIntroWindow)
					{
						if (obIntroWindow.ShowDialog() != DialogResult.OK)
						{
							return 1;
						}
					}
					Console.WriteLine("Closed IntroWindow");
				}

				// Second window - Local info
				LocalInfoWindow obLocalInfoWindow = new LocalInfoWindow();
				using (obLocalInfoWindow)
				{
					Console.WriteLine("Opening LocalInfoWindow");
					obLocalInfoWindow.SourceCodeLine = strSourceCodeLine;
					obLocalInfoWindow.DestinationToolPathWhenNoWorkspace = strDestinationToolFolder;
					obLocalInfoWindow.DestinationToolPath = strDestinationToolFolder;
					obLocalInfoWindow.DestinationShaderPath = strDestinationShaderFolder;
					obLocalInfoWindow.NetworkRagePath = strZipFolder;

					if (!bIgnorePerforce)
					{
						obLocalInfoWindow.FillOutPerForceWorkSpaceComboBox();
					}

					if (!bBatchMode)
					{
						if (obLocalInfoWindow.ShowDialog() != DialogResult.OK)
						{
							return 1;
						}
					}

					strDestinationToolFolder = obLocalInfoWindow.DestinationToolPath;
					strDestinationShaderFolder = obLocalInfoWindow.DestinationShaderPath;
					strZipFolder = obLocalInfoWindow.NetworkRagePath;
					Console.WriteLine("Closed LocalInfoWindow");
				}

				if (String.IsNullOrEmpty(strUseExistingTools))
				{
					if (!File.Exists(strZipFolder + "/ToolsFromPerforce.zip"))
					{
						// To tools zip file is missing, give up
						rageEmailUtilities.SendEmail("krose@rockstarsandiego.com", "rageToolInstaller2@rockstarsandiego.com", "No tools zip file found", strZipFolder + "/ToolsFromPerforce.zip is missing for user " + Environment.GetEnvironmentVariable("USERNAME") + " on " + Environment.GetEnvironmentVariable("COMPUTERNAME") + ".", rageEmailUtilities.MailFormat.Text);
						return -2;
					}
				}

				if (!bBatchMode)
				{
					// Third window - Installation parts
					Console.WriteLine("Opening InstallationOptionsWindow");
					InstallationOptionsWindow obInstallationOptionsWindow = new InstallationOptionsWindow();
					using (obInstallationOptionsWindow)
					{
						obInstallationOptionsWindow.EnableInstallTools = obInstallationOptionsWindow.InstallTools = true;
						obInstallationOptionsWindow.EnableInstallShaders = obInstallationOptionsWindow.InstallShaders = (new FileInfo(strZipFolder + "/Shaders.zip")).Exists;
						obInstallationOptionsWindow.InstallAddToStartMenu = true;
						obInstallationOptionsWindow.InstallExplorerExtensions = bInstallExplorerExtensions;
						obInstallationOptionsWindow.InstallExplorerMtlExtensions = false;
						obInstallationOptionsWindow.InstallPowerShell = bInstallPowerShell;

						if (obInstallationOptionsWindow.ShowDialog() != DialogResult.OK)
						{
							return 1;
						}

						bInstallTools = obInstallationOptionsWindow.InstallTools;
						bInstallShaders = obInstallationOptionsWindow.InstallShaders;
						bInstallAddToStartMenu = obInstallationOptionsWindow.InstallAddToStartMenu;
						bInstallExplorerExtensions = obInstallationOptionsWindow.InstallExplorerExtensions;
						bInstallPowerShell = obInstallationOptionsWindow.InstallPowerShell;
						bInstallExplorerMtlExtensions = obInstallationOptionsWindow.InstallExplorerMtlExtensions;
					}
					Console.WriteLine("Closed InstallationOptionsWindow");
				}

				string strTempFolder = null;
				List<String> obAStrFilesOnlyExistInLocalFolder;
				List<String> obAStrFilesOnlyExistInTempFolder;
				List<String> obAStrFilesExistInBothButNewerInLocalFolder;
				List<String> obAStrFilesExistInBothButNewerInTempFolder;
				List<String> obAStrFilesSameInBoth;
				List<String> obAStrToolFilesToDelete = new List<string>();
				List<String> obAStrToolFilesToUpdate = new List<string>();
				List<String> obAStrToolFilesToAdd = new List<string>();
				List<String> obAStrShaderFilesToDelete = new List<string>();
				List<String> obAStrShaderFilesToUpdate = new List<string>();
				List<String> obAStrShaderFilesToAdd = new List<string>();
				if (String.IsNullOrEmpty(strUseExistingTools))
				{
					// Forth window - Grab and unzip
					strTempFolder = Environment.GetEnvironmentVariable("TEMP") + "/rageToolsInstaller_" + rageFileUtilities.GenerateTimeBasedFilename() + "/";
					rageFileUtilities.MakeDir(strTempFolder);
					UnZipWindow obUnZipWindow = new UnZipWindow(bBatchMode);
					using (obUnZipWindow)
					{
						obUnZipWindow.OpenWindow();
						if (bInstallTools) obUnZipWindow.UnZipTools(strZipFolder + "/ToolsFromPerforce.zip", strTempFolder);
						if (bInstallShaders) obUnZipWindow.UnZipShaders(strZipFolder + "/Shaders.zip", strTempFolder);
						obUnZipWindow.Done();
					}
					if (!Directory.Exists(strTempFolder + "rage/assets/tune/shaders") && Directory.Exists(strTempFolder + "assets/dev/rage/assets/tune/shaders"))
					{
						Directory.CreateDirectory(strTempFolder + "rage/assets/tune");
						Directory.Move(strTempFolder + "assets/dev/rage/assets/tune/shaders", strTempFolder + "rage/assets/tune/shaders");
					}

					// Get lists of tool files to change
					if (bInstallTools)
					{
						rageFileUtilities.CompareFolders(strDestinationToolFolder, strTempFolder + "tools/", out obAStrFilesOnlyExistInLocalFolder, out obAStrFilesOnlyExistInTempFolder, out obAStrFilesExistInBothButNewerInLocalFolder, out obAStrFilesExistInBothButNewerInTempFolder, out obAStrFilesSameInBoth);
						foreach (string strRelativeFileToDelete in obAStrFilesOnlyExistInLocalFolder)
						{
							obAStrToolFilesToDelete.Add(strRelativeFileToDelete.Replace("\\", "/"));
						}
						foreach (string strRelativeFileToUpdate in obAStrFilesExistInBothButNewerInLocalFolder)
						{
							obAStrToolFilesToUpdate.Add(strRelativeFileToUpdate.Replace("\\", "/"));
						}
						foreach (string strRelativeFileToUpdate in obAStrFilesExistInBothButNewerInTempFolder)
						{
							obAStrToolFilesToUpdate.Add(strRelativeFileToUpdate.Replace("\\", "/"));
						}
						foreach (string strRelativeFileToAdd in obAStrFilesOnlyExistInTempFolder)
						{
							obAStrToolFilesToAdd.Add(strRelativeFileToAdd.Replace("\\", "/"));
						}
					}

					// Get lists of Shader files to change
					if (bInstallShaders)
					{
						rageFileUtilities.CompareFolders(strDestinationShaderFolder + "rage/assets/tune/shaders/", strTempFolder + "rage/assets/tune/shaders/", out obAStrFilesOnlyExistInLocalFolder, out obAStrFilesOnlyExistInTempFolder, out obAStrFilesExistInBothButNewerInLocalFolder, out obAStrFilesExistInBothButNewerInTempFolder, out obAStrFilesSameInBoth);
						foreach (string strRelativeFileToDelete in obAStrFilesOnlyExistInLocalFolder)
						{
							if (!strRelativeFileToDelete.EndsWith(".sps"))
							{
								obAStrShaderFilesToDelete.Add("rage/assets/tune/shaders/" + strRelativeFileToDelete.Replace("\\", "/"));
							}
						}
						foreach (string strRelativeFileToUpdate in obAStrFilesExistInBothButNewerInLocalFolder)
						{
							obAStrShaderFilesToUpdate.Add("rage/assets/tune/shaders/" + strRelativeFileToUpdate.Replace("\\", "/"));
						}
						foreach (string strRelativeFileToUpdate in obAStrFilesExistInBothButNewerInTempFolder)
						{
							obAStrShaderFilesToUpdate.Add("rage/assets/tune/shaders/" + strRelativeFileToUpdate.Replace("\\", "/"));
						}
						foreach (string strRelativeFileToAdd in obAStrFilesOnlyExistInTempFolder)
						{
							obAStrShaderFilesToAdd.Add("rage/assets/tune/shaders/" + strRelativeFileToAdd.Replace("\\", "/"));
						}
					}
				}

				// If not batch, then allow the user to change any and all options
				if (!bBatchMode)
				{
					// Fifth window - Project and DCC
					Console.WriteLine("Opening ProjectAndDCCWindow");
					ProjectAndDCCWindow obProjectAndDCCWindow = new ProjectAndDCCWindow(rageFileUtilities.GetFilesAsStringList(strTempFolder + "framework\\tools\\bin\\setup", "SetupEnvironmentFor*And*.bat"));
					using (obProjectAndDCCWindow)
					{
						obProjectAndDCCWindow.Project = strProject;
						obProjectAndDCCWindow.DCC = strDCC;

						if (obProjectAndDCCWindow.ShowDialog() != DialogResult.OK)
						{
							return 1;
						}

                        if ( !String.IsNullOrEmpty( obProjectAndDCCWindow.Project ) )
                        {
                            strProject = obProjectAndDCCWindow.Project;
                        }

                        if ( !String.IsNullOrEmpty( obProjectAndDCCWindow.DCC ) )
                        {
                            strDCC = obProjectAndDCCWindow.DCC;
                        }
					}
					Console.WriteLine("Closed ProjectAndDCCWindow");

					// Sixth window - File in use check
					if (String.IsNullOrEmpty(strUseExistingTools))
					{
						Console.WriteLine("Opening FilesInUseWindow");
						List<string> obAStrFilesThatShouldNotBeInUse = new List<string>();
                        obAStrFilesThatShouldNotBeInUse.Add(strDestinationToolFolder.Substring(2) + "rage/framework/");
						obAStrFilesThatShouldNotBeInUse.Add("/rage/assets/tune/shaders/");
						FilesInUseWindow obFilesInUseWindow = new FilesInUseWindow(obAStrFilesThatShouldNotBeInUse);
						using (obFilesInUseWindow)
						{
							if (obFilesInUseWindow.ShowDialog() != DialogResult.OK)
							{
								return 1;
							}
						}
						Console.WriteLine("Closed FilesInUseWindow");

						// Sixth window - Update review
						Console.WriteLine("Opening UpdateReviewWindow");
						UpdateReviewWindow obUpdateReviewWindow = new UpdateReviewWindow(obAStrToolFilesToDelete, obAStrToolFilesToUpdate, obAStrToolFilesToAdd);
						using (obUpdateReviewWindow)
						{
							if (obUpdateReviewWindow.ShowDialog() != DialogResult.OK)
							{
								return 1;
							}
						}
						Console.WriteLine("Closed UpdateReviewWindow");
					}
				}

				// Uninstall window
				Console.WriteLine("Opening UninstallWindow");
				UninstallWindow obUninstallWindow = new UninstallWindow(bBatchMode);
				using (obUninstallWindow)
				{
					obUninstallWindow.OpenWindow();
					obUninstallWindow.UninstallAssemblies(out obStatus);
					if (!obStatus.Success())
					{
                        obUninstallWindow.Done();
						Console.WriteLine(obStatus.GetErrorString());
						return -3;
					}
					obUninstallWindow.Done();
				}
				Console.WriteLine("Closed UninstallWindow");

				// Seventh window - tool Update files
				Console.WriteLine("Opening UpdateFilesProgressWindow");
				UpdateFilesProgressWindow obUpdateFilesProgressWindow = new UpdateFilesProgressWindow(bBatchMode);
				using (obUpdateFilesProgressWindow)
				{
					obUpdateFilesProgressWindow.OpenWindow();
                    if (!bNoDeleteMode)
                    {
                        obUpdateFilesProgressWindow.DeleteFiles(strDestinationToolFolder, obAStrToolFilesToDelete, strDefaultResponseToFileProblems, out obStatus);
                        if (!obStatus.Success())
                        {
                            obUpdateFilesProgressWindow.Done();
                            Console.WriteLine(obStatus.GetErrorString());
                            return -3;
                        }
                    }
					obUpdateFilesProgressWindow.UpdateFiles(strDestinationToolFolder, strTempFolder + "/tools/", obAStrToolFilesToUpdate, strDefaultResponseToFileProblems, out obStatus);
					if (!obStatus.Success())
					{
                        obUpdateFilesProgressWindow.Done();
						Console.WriteLine(obStatus.GetErrorString());
						return -3;
					}
					obUpdateFilesProgressWindow.AddFiles(strDestinationToolFolder, strTempFolder + "/tools/", obAStrToolFilesToAdd, strDefaultResponseToFileProblems, out obStatus);
					if (!obStatus.Success())
					{
                        obUpdateFilesProgressWindow.Done();
						Console.WriteLine(obStatus.GetErrorString());
						return -3;
					}
					obUpdateFilesProgressWindow.Done();
				}
				Console.WriteLine("Closed UpdateFilesProgressWindow");

				// Seventh window - Shader Update files
				Console.WriteLine("Opening UpdateFilesProgressWindow");
				UpdateFilesProgressWindow obUpdateFilesProgressWindow2 = new UpdateFilesProgressWindow(bBatchMode);
				using (obUpdateFilesProgressWindow)
				{
					obUpdateFilesProgressWindow2.OpenWindow();
                    if (!bNoDeleteMode)
                    {
                        obUpdateFilesProgressWindow2.DeleteFiles(strDestinationShaderFolder, obAStrShaderFilesToDelete, strDefaultResponseToFileProblems, out obStatus);
                        if (!obStatus.Success())
                        {
                            obUpdateFilesProgressWindow2.Done();
                            Console.WriteLine(obStatus.GetErrorString());
                            return -3;
                        }
                    }
					obUpdateFilesProgressWindow2.UpdateFiles(strDestinationShaderFolder, strTempFolder, obAStrShaderFilesToUpdate, strDefaultResponseToFileProblems, out obStatus);
					if (!obStatus.Success())
					{
                        obUpdateFilesProgressWindow2.Done();
						Console.WriteLine(obStatus.GetErrorString());
						return -3;
					}
					obUpdateFilesProgressWindow2.AddFiles(strDestinationShaderFolder, strTempFolder, obAStrShaderFilesToAdd, strDefaultResponseToFileProblems, out obStatus);
					if (!obStatus.Success())
					{
                        obUpdateFilesProgressWindow2.Done();
						Console.WriteLine(obStatus.GetErrorString());
						return -3;
					}
					obUpdateFilesProgressWindow2.Done();
				}
				Console.WriteLine("Closed UpdateFilesProgressWindow");

				// Make all the files in the shader folder writable
				rageFileUtilities.SetAttributes(strDestinationShaderFolder + "/rage/assets/tune/shaders/", FileAttributes.Normal);

				// Eighth window - Third Party
				Console.WriteLine("Opening ThirdPartyProgressWindow");
				ThirdPartyProgressWindow obThirdPartyProgressWindow = new ThirdPartyProgressWindow(bBatchMode);
				using (obThirdPartyProgressWindow)
				{
					obThirdPartyProgressWindow.OpenWindow();
					if (bInstallPowerShell) obThirdPartyProgressWindow.InstallPowerShell(strDestinationToolFolder);
					obThirdPartyProgressWindow.Done();
				}
				Console.WriteLine("Closed ThirdPartyProgressWindow");

				// Eighth window - Update environment
				Console.WriteLine("Opening UpdateEnvironmentProgressWindow");
				UpdateEnvironmentProgressWindow obUpdateEnvironmentProgressWindow = new UpdateEnvironmentProgressWindow(bBatchMode);
				using (obUpdateEnvironmentProgressWindow)
				{
					obUpdateEnvironmentProgressWindow.OpenWindow();
					if (bInstallAddToStartMenu) obUpdateEnvironmentProgressWindow.AddStartMenuItems(strDestinationToolFolder, strSourceCodeLine, strZipFolder);
					if (bInstallExplorerExtensions)
					{
						obUpdateEnvironmentProgressWindow.AddExplorerExtensions(strDestinationToolFolder);
					}
					obUpdateEnvironmentProgressWindow.SetupEnvironment(strDestinationToolFolder, strProject, strDCC, strDefaultResponseToFileProblems, out obStatus);
					if (!obStatus.Success())
					{
                        obUpdateEnvironmentProgressWindow.Done();
						Console.WriteLine(obStatus.GetErrorString());
						return -3;
					}
					obUpdateEnvironmentProgressWindow.Setup3DSMax(strDestinationToolFolder, out obStatus);
					if (!obStatus.Success())
					{
                        obUpdateEnvironmentProgressWindow.Done();
						Console.WriteLine(obStatus.GetErrorString());
						return -3;
					}

					obUpdateEnvironmentProgressWindow.RegisterAssemblies(strDestinationToolFolder, out obStatus);
					if (!obStatus.Success())
					{
                        obUpdateEnvironmentProgressWindow.Done();
						Console.WriteLine(obStatus.GetErrorString());
						return -3;
					}

					obUpdateEnvironmentProgressWindow.Done();
				}
				Console.WriteLine("Closed UpdateEnvironmentProgressWindow");

				// Clean up
				if (String.IsNullOrEmpty(strUseExistingTools))
				{
					rageFileUtilities.DeleteLocalFolder(strTempFolder, out obStatus);
					Console.WriteLine("Deleted temp folder");
				}

				// If not batch, then allow the user to change any and all options
				if (!bBatchMode)
				{
					// Final window - Success
					Console.WriteLine("Opening InstallationCompleteWindow");
					InstallationCompleteWindow obInstallationCompleteWindow = new InstallationCompleteWindow();
					using (obInstallationCompleteWindow)
					{
						if (obInstallationCompleteWindow.ShowDialog() != DialogResult.OK)
						{
							return 1;
						}
					}
					Console.WriteLine("Closed InstallationCompleteWindow");
				}
				Console.WriteLine("All done, leaving installer");
			}
			//catch (Exception ex)
			//{
			//    // Something bad happened somewhere
			//    String strErrorMessage = "Something really bad happened during installation :\n\n";

			//    // Outer exception
			//    strErrorMessage += "Outer exception Type : " + ex.GetType() + "\n";
			//    strErrorMessage += "Outer exception Message : " + ex.Message + "\n";
			//    strErrorMessage += "Outer exception StackTrace : " + ex.StackTrace + "\n";
			//    strErrorMessage += "Outer exception String : " + ex.ToString() + "\n";

			//    // Inner exceptions
			//    ex = ex.InnerException;
			//    while (ex != null)
			//    {
			//        System.Console.WriteLine("--------------------------------");
			//        strErrorMessage += "Inner exception Type : " + ex.GetType() + "\n";
			//        strErrorMessage += "Inner exception Message : " + ex.Message + "\n";
			//        strErrorMessage += "Inner exception StackTrace : " + ex.StackTrace + "\n";
			//        strErrorMessage += "Inner exception String : " + ex.ToString() + "\n";
			//        ex = ex.InnerException;
			//    }

			//    // Log error
			//    Console.WriteLine(strErrorMessage);

			//    // Email error
			//    rageEmailUtilities.SendEmail("krose@rockstarsandiego.com", "rageToolInstaller2@rockstarsandiego.com", "rageToolInstaller2 Exception Handler", "Computer : " + Environment.GetEnvironmentVariable("COMPUTERNAME") + "\nUser : " + Environment.GetEnvironmentVariable("USERNAME") + "\nCommandline : " + Environment.CommandLine + "\n\n" + strErrorMessage, rageEmailUtilities.MailFormat.Text);

			//    // Show error window
			//    MessageBox.Show(strErrorMessage, "rageToolInstaller2 Exception Handler", MessageBoxButtons.OK);
			//}
			return 0;
		}
	}
}
