namespace rageToolsInstaller2
{
	partial class UpdateFilesProgressWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.DeleteProgressBar = new System.Windows.Forms.ProgressBar();
			this.UpdateLabel = new System.Windows.Forms.Label();
			this.UpdateProgressBar = new System.Windows.Forms.ProgressBar();
			this.AddLabel = new System.Windows.Forms.Label();
			this.AddProgressBar = new System.Windows.Forms.ProgressBar();
			this.DeleteLabel = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// infoLabel
			// 
			this.infoLabel.Location = new System.Drawing.Point(48, 69);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.DeleteLabel);
			this.groupBox1.Controls.Add(this.DeleteProgressBar);
			this.groupBox1.Controls.Add(this.UpdateLabel);
			this.groupBox1.Controls.Add(this.UpdateProgressBar);
			this.groupBox1.Controls.Add(this.AddLabel);
			this.groupBox1.Controls.Add(this.AddProgressBar);
			this.groupBox1.Controls.SetChildIndex(this.AddProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.AddLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.UpdateProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.UpdateLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.DeleteProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.DeleteLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.infoLabel, 0);
			// 
			// DeleteProgressBar
			// 
			this.DeleteProgressBar.Location = new System.Drawing.Point(40, 35);
			this.DeleteProgressBar.Name = "DeleteProgressBar";
			this.DeleteProgressBar.Size = new System.Drawing.Size(480, 24);
			this.DeleteProgressBar.TabIndex = 26;
			// 
			// UpdateLabel
			// 
			this.UpdateLabel.Location = new System.Drawing.Point(40, 62);
			this.UpdateLabel.Name = "UpdateLabel";
			this.UpdateLabel.Size = new System.Drawing.Size(480, 16);
			this.UpdateLabel.TabIndex = 25;
			this.UpdateLabel.Text = "Update Progress:";
			// 
			// UpdateProgressBar
			// 
			this.UpdateProgressBar.Location = new System.Drawing.Point(40, 81);
			this.UpdateProgressBar.Name = "UpdateProgressBar";
			this.UpdateProgressBar.Size = new System.Drawing.Size(480, 24);
			this.UpdateProgressBar.TabIndex = 24;
			// 
			// AddLabel
			// 
			this.AddLabel.Location = new System.Drawing.Point(40, 108);
			this.AddLabel.Name = "AddLabel";
			this.AddLabel.Size = new System.Drawing.Size(480, 16);
			this.AddLabel.TabIndex = 23;
			this.AddLabel.Text = "Addition Progress:";
			// 
			// AddProgressBar
			// 
			this.AddProgressBar.Location = new System.Drawing.Point(40, 127);
			this.AddProgressBar.Name = "AddProgressBar";
			this.AddProgressBar.Size = new System.Drawing.Size(480, 24);
			this.AddProgressBar.TabIndex = 22;
			// 
			// DeleteLabel
			// 
			this.DeleteLabel.Location = new System.Drawing.Point(40, 16);
			this.DeleteLabel.Name = "DeleteLabel";
			this.DeleteLabel.Size = new System.Drawing.Size(480, 16);
			this.DeleteLabel.TabIndex = 27;
			this.DeleteLabel.Text = "Delete Progress:";
			// 
			// UpdateFilesProgressWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Name = "UpdateFilesProgressWindow";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ProgressBar DeleteProgressBar;
		private System.Windows.Forms.Label UpdateLabel;
		private System.Windows.Forms.ProgressBar UpdateProgressBar;
		private System.Windows.Forms.Label AddLabel;
		private System.Windows.Forms.ProgressBar AddProgressBar;
		private System.Windows.Forms.Label DeleteLabel;
	}
}
