using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using rageUsefulCSharpToolClasses;

namespace rageToolsInstaller2
{
	public partial class LocalInfoWindow : rageToolsInstaller2.BaseInstallWindow
	{
		public LocalInfoWindow()
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Local Installation Information";
			this.infoLabel.Text = "Please provide the following information about where you are storing the Rage Tool modules.";
		}

		public void FillOutPerForceWorkSpaceComboBox()
		{
			// Add none to the list of available workspaces
			PerforceWorkspaceComboBox.Items.Clear();
			PerforceWorkspaceComboBox.Items.Add("[none]");

			// Do they have perforce?
			Console.WriteLine("ragePerforce.IsPerforceInstalled() = " + ragePerforce.IsPerforceInstalled());
			if (ragePerforce.IsPerforceInstalled())
			{
				// We have perforce, so use it to work out where I should install this stuff
				// Get workspace to use
				rageStatus obStatus;
				string[] astrWorkspaces = ragePerforce.GetWorkspacesForCurrentUserOnCurrentHost();
				Console.WriteLine("ragePerforce.GetWorkspacesForCurrentUserOnCurrentHost(); = ");
				foreach (string strWorkspace in astrWorkspaces)
				{
					Console.WriteLine(strWorkspace);
				}
				foreach (string strWorkspace in astrWorkspaces)
				{
					// Does this workspace offer a path on this code line?
					ragePerforce.WorkspaceName = strWorkspace;

					ragePerforce.EchoToConsole = true;
					string strDestinationToolPath = ragePerforce.GetLocalPathFromPerforcePath("//rage/", SourceCodeLine, "rage/framework/tools/", out obStatus);
					ragePerforce.EchoToConsole = false;
                    Console.WriteLine("ragePerforce.GetLocalPathFromPerforcePath(\"//rage/\", \"" + SourceCodeLine + "\", \"rage/framework/tools/\", out obStatus) = " + strDestinationToolPath);
					if ((strDestinationToolPath != "") && (strDestinationToolPath != null))
					{
						PerforceWorkspaceComboBox.Items.Add(strWorkspace);
					}
				}
			}
			PerforceWorkspaceComboBox.SelectedIndex = PerforceWorkspaceComboBox.Items.Count - 1;
		}

		private string m_strDestinationToolPathWhenNoWorkspace = "";
		public string DestinationToolPathWhenNoWorkspace
		{
			get
			{
				return m_strDestinationToolPathWhenNoWorkspace;
			}
			set
			{
				m_strDestinationToolPathWhenNoWorkspace = value.Replace("\\", "/");
			}
		}

		private string m_strSourceCodeLine = "";
		public string SourceCodeLine
		{
			get
			{
				return m_strSourceCodeLine;
			}
			set
			{
				m_strSourceCodeLine = value.Replace("\\", "/");
			}
		}


		public string DestinationToolPath
		{
			get
			{
				return DestinationToolPathTextBox.Text;
			}
			set
			{
				DestinationToolPathTextBox.Text = value.Replace("\\", "/");
				if (!DestinationToolPathTextBox.Text.EndsWith("/")) DestinationToolPathTextBox.Text += "/";
			}
		}

		public string DestinationShaderPath
		{
			get
			{
				return DestinationShaderPathTextBox.Text;
			}
			set
			{
				DestinationShaderPathTextBox.Text = value.Replace("\\", "/");
			}
		}

		public string NetworkRagePath
		{
			get
			{
				return NetworkPathTextBox.Text;
			}
			set
			{
				NetworkPathTextBox.Text = value.Replace("\\", "/");
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			// Browse for a new destination tool path
			DestinationToolPath = rageSimpleUIElements.FolderBrowser("Destination Tool Path", DestinationToolPath);
		}

		private void button4_Click(object sender, EventArgs e)
		{
			// Browse for a new destination Shader path
			DestinationShaderPath = rageSimpleUIElements.FolderBrowser("Destination Shader Path", DestinationShaderPath);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			// Browse for a new source path
			NetworkRagePath = rageSimpleUIElements.FolderBrowser("Network Rage Path", NetworkRagePath);
		}

		private void PerforceWorkspaceComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			// They have changed the perforce workspace, so update the other fields
			string strWorkspace = PerforceWorkspaceComboBox.Text;
			if (strWorkspace == "[none]")
			{
				DestinationToolPath = DestinationToolPathWhenNoWorkspace;
				DestinationToolPathTextBox.ReadOnly = false;
				button2.Enabled = true;
			}
			else
			{
				rageStatus obStatus;
				ragePerforce.WorkspaceName = strWorkspace;
                string strToolPath = ragePerforce.GetLocalPathFromPerforcePath("//rage/", SourceCodeLine, "rage/framework/tools/", out obStatus);
				DestinationToolPath = strToolPath;
				DestinationToolPathTextBox.ReadOnly = true;
				button2.Enabled = false;
			}
		}
	}
}

