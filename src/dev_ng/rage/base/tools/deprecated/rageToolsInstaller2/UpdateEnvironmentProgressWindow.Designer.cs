namespace rageToolsInstaller2
{
	partial class UpdateEnvironmentProgressWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.StartMenuLabel = new System.Windows.Forms.Label();
			this.StartMenuProgressBar = new System.Windows.Forms.ProgressBar();
			this.EnvironmentProgressBar = new System.Windows.Forms.ProgressBar();
			this.EnvironmentLabel = new System.Windows.Forms.Label();
			this.ExplorerExtensionsLabel = new System.Windows.Forms.Label();
			this.ExplorerExtensionsProgressBar = new System.Windows.Forms.ProgressBar();
			this.AssembliesLabel = new System.Windows.Forms.Label();
			this.AssembliesProgressBar = new System.Windows.Forms.ProgressBar();
			this.MaxLabel = new System.Windows.Forms.Label();
			this.MaxProgressBar = new System.Windows.Forms.ProgressBar();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// infoLabel
			// 
			this.infoLabel.Location = new System.Drawing.Point(48, 69);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.MaxLabel);
			this.groupBox1.Controls.Add(this.MaxProgressBar);
			this.groupBox1.Controls.Add(this.AssembliesLabel);
			this.groupBox1.Controls.Add(this.AssembliesProgressBar);
			this.groupBox1.Controls.Add(this.ExplorerExtensionsLabel);
			this.groupBox1.Controls.Add(this.ExplorerExtensionsProgressBar);
			this.groupBox1.Controls.Add(this.EnvironmentLabel);
			this.groupBox1.Controls.Add(this.EnvironmentProgressBar);
			this.groupBox1.Controls.Add(this.StartMenuProgressBar);
			this.groupBox1.Controls.Add(this.StartMenuLabel);
			this.groupBox1.Controls.SetChildIndex(this.infoLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.StartMenuLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.StartMenuProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.EnvironmentProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.EnvironmentLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.ExplorerExtensionsProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.ExplorerExtensionsLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.AssembliesProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.AssembliesLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.MaxProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.MaxLabel, 0);
			// 
			// StartMenuLabel
			// 
			this.StartMenuLabel.Location = new System.Drawing.Point(40, 16);
			this.StartMenuLabel.Name = "StartMenuLabel";
			this.StartMenuLabel.Size = new System.Drawing.Size(480, 16);
			this.StartMenuLabel.TabIndex = 21;
			this.StartMenuLabel.Text = "Start Menu Progress:";
			// 
			// StartMenuProgressBar
			// 
			this.StartMenuProgressBar.Location = new System.Drawing.Point(40, 35);
			this.StartMenuProgressBar.Name = "StartMenuProgressBar";
			this.StartMenuProgressBar.Size = new System.Drawing.Size(480, 24);
			this.StartMenuProgressBar.TabIndex = 20;
			// 
			// EnvironmentProgressBar
			// 
			this.EnvironmentProgressBar.Location = new System.Drawing.Point(40, 128);
			this.EnvironmentProgressBar.Name = "EnvironmentProgressBar";
			this.EnvironmentProgressBar.Size = new System.Drawing.Size(480, 24);
			this.EnvironmentProgressBar.TabIndex = 28;
			// 
			// EnvironmentLabel
			// 
			this.EnvironmentLabel.Location = new System.Drawing.Point(40, 109);
			this.EnvironmentLabel.Name = "EnvironmentLabel";
			this.EnvironmentLabel.Size = new System.Drawing.Size(480, 16);
			this.EnvironmentLabel.TabIndex = 29;
			this.EnvironmentLabel.Text = "Environment Progress:";
			// 
			// ExplorerExtensionsLabel
			// 
			this.ExplorerExtensionsLabel.Location = new System.Drawing.Point(40, 62);
			this.ExplorerExtensionsLabel.Name = "ExplorerExtensionsLabel";
			this.ExplorerExtensionsLabel.Size = new System.Drawing.Size(480, 16);
			this.ExplorerExtensionsLabel.TabIndex = 31;
			this.ExplorerExtensionsLabel.Text = "Environment Progress:";
			// 
			// ExplorerExtensionsProgressBar
			// 
			this.ExplorerExtensionsProgressBar.Location = new System.Drawing.Point(40, 81);
			this.ExplorerExtensionsProgressBar.Name = "ExplorerExtensionsProgressBar";
			this.ExplorerExtensionsProgressBar.Size = new System.Drawing.Size(480, 24);
			this.ExplorerExtensionsProgressBar.TabIndex = 30;
			// 
			// AssembliesLabel
			// 
			this.AssembliesLabel.Location = new System.Drawing.Point(40, 201);
			this.AssembliesLabel.Name = "GEOsLabel";
			this.AssembliesLabel.Size = new System.Drawing.Size(480, 16);
			this.AssembliesLabel.TabIndex = 33;
			this.AssembliesLabel.Text = "Registering GEOs Progress:";
			// 
			// AssembliesProgressBar
			// 
			this.AssembliesProgressBar.Location = new System.Drawing.Point(40, 220);
			this.AssembliesProgressBar.Name = "GEOsProgressBar";
			this.AssembliesProgressBar.Size = new System.Drawing.Size(480, 24);
			this.AssembliesProgressBar.TabIndex = 32;
			// 
			// MaxLabel
			// 
			this.MaxLabel.Location = new System.Drawing.Point(40, 155);
			this.MaxLabel.Name = "MaxLabel";
			this.MaxLabel.Size = new System.Drawing.Size(480, 16);
			this.MaxLabel.TabIndex = 35;
			this.MaxLabel.Text = "Adding 3DS Max Support:";
			// 
			// MaxProgressBar
			// 
			this.MaxProgressBar.Location = new System.Drawing.Point(40, 174);
			this.MaxProgressBar.Name = "MaxProgressBar";
			this.MaxProgressBar.Size = new System.Drawing.Size(480, 24);
			this.MaxProgressBar.TabIndex = 34;
			// 
			// UpdateEnvironmentProgressWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Name = "UpdateEnvironmentProgressWindow";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label StartMenuLabel;
		private System.Windows.Forms.ProgressBar StartMenuProgressBar;
		private System.Windows.Forms.ProgressBar EnvironmentProgressBar;
		private System.Windows.Forms.Label EnvironmentLabel;
		private System.Windows.Forms.Label ExplorerExtensionsLabel;
		private System.Windows.Forms.ProgressBar ExplorerExtensionsProgressBar;
		private System.Windows.Forms.Label AssembliesLabel;
		private System.Windows.Forms.ProgressBar AssembliesProgressBar;
		private System.Windows.Forms.Label MaxLabel;
		private System.Windows.Forms.ProgressBar MaxProgressBar;
	}
}
