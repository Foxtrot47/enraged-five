using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using OrganicBit.Zip;
using rageUsefulCSharpToolClasses;

namespace rageToolsInstaller2
{
	public partial class UnZipWindow : rageToolsInstaller2.ProgressWindow
	{
		public UnZipWindow(bool bBatchMode)
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Expanding data";
			this.infoLabel.Text = "Expanding tools data.";

			// Open the window in the background
			if (!bBatchMode)
			{
				EnableButton(cancelButton, false);
				EnableButton(nextButton, false);
				EnableButton(backButton, false);
				// this.Show();
				//Thread obWindowThread = new Thread(new ThreadStart(ShowWindow));
				//obWindowThread.Start();
			}
		}

		public void UnZipTools(string strSourceZip, string strDestinationFolder)
		{
			UnZip(strSourceZip, strDestinationFolder, ToolsProgressBar, ToolsLabel);
		}

		public void UnZipShaders(string strSourceZip, string strDestinationFolder)
		{
			UnZip(strSourceZip, strDestinationFolder, ShadersProgressBar, ShadersLabel);
		}

		private void UnZip(string strSourceZip, string strDestinationFolder, ProgressBar obProgressBar, Label obTextLabel)
		{
			// Make sure the zip file is there
			while(!File.Exists(strSourceZip))
			{
				// File doesn't exist, wait for it to appear
				Console.WriteLine(strSourceZip +" does not exist :(  I am waiting in the hope it appears");
				Thread.Sleep(new TimeSpan(0, 0, 2));
			}
			// Ok, the file exists, annoyingly there is a special case where between the line about and the file being opened on 
			// the line below the file might vanish!
			// So watch for that

			ZipReader reader = null;
			while (reader == null)
			{
				try
				{
					reader = new ZipReader(strSourceZip);
				}
				catch (Exception e)
				{
					// Something bad happened
					Console.WriteLine("Failed to open zip file " + strSourceZip + " : " + e.ToString());
					// rageEmailUtilities.SendEmail("krose@rockstarsandiego.com", "rageToolInstaller2@rockstarsandiego.com", "Failed to open zip file", "Failed to open zip file " + strSourceZip + " : " + e.ToString() + "\n\n User " + Environment.GetEnvironmentVariable("USERNAME") + " on " + Environment.GetEnvironmentVariable("COMPUTERNAME") + ".", rageEmailUtilities.MailFormat.Text);
				}
			}
			Console.WriteLine("Archive: " + strSourceZip);
			Console.WriteLine(reader.Comment);
			int iNoOfEntries = reader.Entries.Count;
			SetProgressBarMin(obProgressBar, 0);
			SetProgressBarMax(obProgressBar, iNoOfEntries);

			// buffer to hold temp bytes
			byte[] buffer = new byte[4096];
			int byteCount;

			// Get the zipped entries
			while (reader.MoveNext())
			{
				ZipEntry entry = reader.Current;
				IncProgressBarValue(obProgressBar);
				SetLabelText(obTextLabel, "Expanding : " + entry.Name);

				if (entry.IsDirectory)
				{
					Directory.CreateDirectory(strDestinationFolder +"/"+ entry.Name);
				}
				else
				{
					// create output stream
					string strDestinationPathAndFileName = strDestinationFolder +"/"+ entry.Name;
					FileStream writer = File.Open(strDestinationPathAndFileName, FileMode.Create);

					// write uncompressed data
					while ((byteCount = reader.Read(buffer, 0, buffer.Length)) > 0)
					{
						// Console.Write(".");
						writer.Write(buffer, 0, byteCount);
					}
					writer.Close();
					// Console.WriteLine();

					// Set the time on the created file to be the same as the time in the zip file
					File.SetLastWriteTime(strDestinationPathAndFileName, entry.ModifiedTime);
				}
			}
			reader.Close();
			SetLabelText(obTextLabel, "Done");
		}
	}
}

