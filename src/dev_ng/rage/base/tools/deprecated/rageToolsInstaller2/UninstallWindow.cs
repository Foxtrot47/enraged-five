using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using rageUsefulCSharpToolClasses;

namespace rageToolsInstaller2
{
	public partial class UninstallWindow : rageToolsInstaller2.ProgressWindow
	{
		public UninstallWindow(bool bBatchMode)
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Update Progress";
			this.infoLabel.Text = "Uninstalling";
			this.infoLabel.Visible = false;

			// Open the window in the background
			if (!bBatchMode)
			{
				EnableButton(cancelButton, false);
				EnableButton(nextButton, false);
				EnableButton(backButton, false);
				//Thread obWindowThread = new Thread(new ThreadStart(ShowWindow));
				//obWindowThread.Start();
			}
		}

		public void UninstallAssemblies(out rageStatus obStatus)
		{
			obStatus = new rageStatus();
			/*
			// Console.WriteLine("AddStartMenuItems(\""+ strLocalPath +"\", \""+ strCodeLine +"\", \""+ strNetworkPath +"\")");
			SetProgressBarMin(AssembliesProgressBar, 0);
			SetProgressBarMax(AssembliesProgressBar, 6);
			SetLabelText(AssembliesLabel, "Uninstalling Assemblies : Seeking .Net Framework");
			SetProgressBarValue(AssembliesProgressBar, 1);

            //NOTE: We get the .NET runtime directory here so that we can call regasm.exe to register the
            //assemblies. The call to GetRuntimeDirectory() returns the .NET runtime directory for the
            //version of .NET being used by this project, NOT the latest installed version of .NET. If we
            //want to register assemblies that use later versions of .NET, this will have to be updated to
            //look into the registry and find the last .NET version's directory.
            String strPathToRegAsm = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();

			// Unregister GEOs
			SetLabelText(AssembliesLabel, "Uninstalling Assemblies : GEOs");
			SetProgressBarValue(AssembliesProgressBar, 2);

			// Execute the command
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = strPathToRegAsm.Replace('/','\\') + "RegAsm.exe";
			obCommand.Arguments = " /verbose /unregister rageGEODataSet.dll";
			obCommand.WorkingDirectory = strPathToRegAsm;
			obCommand.UpdateLogFileInRealTime = false;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.EchoToConsole = true;
			obCommand.UseBusySpinner = false;
			Console.WriteLine(obCommand.GetDosCommand());
			obCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened, but probably just means the GEO stuff was never even installed on this computer, so ignore.
				// return;
			}

			// Nuke rageExplorerToolTips as it is evil and kills things
			// rageFileUtilities.DeleteFilesFromLocalFolder(Environment.GetEnvironmentVariable("SystemRoot"), "*rageExplorerToolTip*", out obStatus);

			// Done
			SetProgressBarValue(AssembliesProgressBar, 6);
			obStatus = new rageStatus();
			*/
		}
	}
}

