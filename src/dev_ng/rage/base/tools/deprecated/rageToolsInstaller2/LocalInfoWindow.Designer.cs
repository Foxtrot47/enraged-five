namespace rageToolsInstaller2
{
	partial class LocalInfoWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.NetworkPathTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.DestinationShaderPathTextBox = new System.Windows.Forms.TextBox();
			this.button4 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.DestinationToolPathTextBox = new System.Windows.Forms.TextBox();
			this.PerforceWorkspaceComboBox = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.PerforceWorkspaceComboBox);
			this.groupBox1.Controls.Add(this.button2);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.DestinationToolPathTextBox);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.NetworkPathTextBox);
			this.groupBox1.Controls.Add(this.button4);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.DestinationShaderPathTextBox);
			this.groupBox1.Controls.SetChildIndex(this.DestinationShaderPathTextBox, 0);
			this.groupBox1.Controls.SetChildIndex(this.label1, 0);
			this.groupBox1.Controls.SetChildIndex(this.button4, 0);
			this.groupBox1.Controls.SetChildIndex(this.NetworkPathTextBox, 0);
			this.groupBox1.Controls.SetChildIndex(this.label2, 0);
			this.groupBox1.Controls.SetChildIndex(this.infoLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.button1, 0);
			this.groupBox1.Controls.SetChildIndex(this.DestinationToolPathTextBox, 0);
			this.groupBox1.Controls.SetChildIndex(this.label3, 0);
			this.groupBox1.Controls.SetChildIndex(this.button2, 0);
			this.groupBox1.Controls.SetChildIndex(this.PerforceWorkspaceComboBox, 0);
			this.groupBox1.Controls.SetChildIndex(this.label4, 0);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(392, 144);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(120, 24);
			this.button1.TabIndex = 23;
			this.button1.Text = "Browse";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(48, 147);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(136, 16);
			this.label2.TabIndex = 22;
			this.label2.Text = "Network Rage Path";
			// 
			// NetworkPathTextBox
			// 
			this.NetworkPathTextBox.Location = new System.Drawing.Point(190, 144);
			this.NetworkPathTextBox.Name = "NetworkPathTextBox";
			this.NetworkPathTextBox.Size = new System.Drawing.Size(186, 20);
			this.NetworkPathTextBox.TabIndex = 21;
			this.NetworkPathTextBox.Text = "NetworkCachePathTextBox";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(48, 117);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(136, 16);
			this.label1.TabIndex = 18;
			this.label1.Text = "Destination Shader Path";
			// 
			// DestinationShaderPathTextBox
			// 
			this.DestinationShaderPathTextBox.Location = new System.Drawing.Point(190, 114);
			this.DestinationShaderPathTextBox.Name = "DestinationShaderPathTextBox";
			this.DestinationShaderPathTextBox.Size = new System.Drawing.Size(186, 20);
			this.DestinationShaderPathTextBox.TabIndex = 19;
			this.DestinationShaderPathTextBox.Text = "NetworkCachePathTextBox";
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(392, 114);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(120, 24);
			this.button4.TabIndex = 20;
			this.button4.Text = "Browse";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(392, 84);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(120, 24);
			this.button2.TabIndex = 26;
			this.button2.Text = "Browse";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(48, 90);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(136, 16);
			this.label3.TabIndex = 24;
			this.label3.Text = "Destination Tool Path";
			// 
			// DestinationToolPathTextBox
			// 
			this.DestinationToolPathTextBox.Location = new System.Drawing.Point(190, 88);
			this.DestinationToolPathTextBox.Name = "DestinationToolPathTextBox";
			this.DestinationToolPathTextBox.Size = new System.Drawing.Size(186, 20);
			this.DestinationToolPathTextBox.TabIndex = 25;
			this.DestinationToolPathTextBox.Text = "NetworkCachePathTextBox";
			// 
			// PerforceWorkspaceComboBox
			// 
			this.PerforceWorkspaceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.PerforceWorkspaceComboBox.FormattingEnabled = true;
			this.PerforceWorkspaceComboBox.Location = new System.Drawing.Point(190, 61);
			this.PerforceWorkspaceComboBox.Name = "PerforceWorkspaceComboBox";
			this.PerforceWorkspaceComboBox.Size = new System.Drawing.Size(322, 21);
			this.PerforceWorkspaceComboBox.TabIndex = 27;
			this.PerforceWorkspaceComboBox.SelectedIndexChanged += new System.EventHandler(this.PerforceWorkspaceComboBox_SelectedIndexChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(48, 64);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(136, 16);
			this.label4.TabIndex = 28;
			this.label4.Text = "Perforce Workspace";
			// 
			// LocalInfoWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Name = "LocalInfoWindow";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox NetworkPathTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox DestinationShaderPathTextBox;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox DestinationToolPathTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ComboBox PerforceWorkspaceComboBox;
	}
}
