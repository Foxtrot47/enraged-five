using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace rageToolsInstaller2
{
	public partial class InstallationCompleteWindow : rageToolsInstaller2.BaseInstallWindow
	{
		public InstallationCompleteWindow()
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Installation Complete";
			this.infoLabel.Text = "Rage Tools have been successfully installed.\n\nClick \"Close\" to exit.";

			EnableButton(cancelButton, false);
			EnableButton(nextButton, true);
			EnableButton(backButton, false);
			nextButton.Text = "Close";
		}
	}
}

