using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using rageUsefulCSharpToolClasses;

namespace rageToolsInstaller2
{
	public partial class UpdateProgressWindow : rageToolsInstaller2.ProgressWindow
	{
		public UpdateProgressWindow(bool bBatchMode)
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Update Progress";
			this.infoLabel.Visible = false;

			// Open the window in the background
			if (!bBatchMode)
			{
				EnableButton(cancelButton, false);
				EnableButton(nextButton, false);
				EnableButton(backButton, false);
				Thread obWindowThread = new Thread(new ThreadStart(ShowWindow));
				obWindowThread.Start();
			}
		}

		public void DeleteFiles(string strLocalPath, List<string> obAStrFilesToDelete, string strDefaultResponseToFileProblems, out rageStatus obStatus)
		{
			SetProgressBarMin(DeleteProgressBar, 0);
			SetProgressBarMax(DeleteProgressBar, obAStrFilesToDelete.Count + 1);

			// Delete everything
			for(int i=0; i<obAStrFilesToDelete.Count; i++)
			{
				string strFileToDelete = obAStrFilesToDelete[i];
				SetProgressBarValue(DeleteProgressBar, i);
				SetLabelText(DeleteLabel, "Deleting : " + strFileToDelete);
				rageFileUtilities.DeleteLocalFile(strLocalPath + strFileToDelete, out obStatus);

				if (!obStatus.Success())
				{
					// Badness happened
					DialogResult obResult = FileInUseDialogWindow(obStatus.GetErrorString(), strDefaultResponseToFileProblems);
					if (obResult == DialogResult.Abort)
					{
						// It is all bad, abandon ship
						return;
					}
					else if (obResult == DialogResult.Retry)
					{
						// Try again
						i--;
					}
					else if (obResult == DialogResult.Ignore)
					{
						// Ignore
					}
				}
			}

			SetLabelText(DeleteLabel, "Deleting : Done");
			SetProgressBarValue(DeleteProgressBar, obAStrFilesToDelete.Count + 1);

			// Make sure everything is ok
			obStatus = new rageStatus();
		}


		public void UpdateFiles(string strLocalPath, string strSourcePath, List<string> obAStrFilesToUpdate, string strDefaultResponseToFileProblems, out rageStatus obStatus)
		{
			SetProgressBarMin(UpdateProgressBar, 0);
			SetProgressBarMax(UpdateProgressBar, obAStrFilesToUpdate.Count + 1);

			// Update everything
			for (int i = 0; i < obAStrFilesToUpdate.Count; i++)
			{
				string strFileToUpdate = obAStrFilesToUpdate[i];
				SetProgressBarValue(UpdateProgressBar, i);
				SetLabelText(UpdateLabel, "Updating : " + strFileToUpdate);
				string strDestinationPathAndFileName = strLocalPath + strFileToUpdate;
				rageFileUtilities.DeleteLocalFile(strDestinationPathAndFileName, out obStatus);

				if (!obStatus.Success())
				{
					// Badness happened
					DialogResult obResult = FileInUseDialogWindow(obStatus.GetErrorString(), strDefaultResponseToFileProblems);
					if (obResult == DialogResult.Abort)
					{
						// It is all bad, abandon ship
						return;
					}
					else if (obResult == DialogResult.Retry)
					{
						// Try again
						i--;
					}
					else if (obResult == DialogResult.Ignore)
					{
						// Ignore
					}
				}
				else
				{
					// The delete worked, so update
					rageFileUtilities.CopyFile(strSourcePath + strFileToUpdate, strDestinationPathAndFileName, out obStatus);

					if (!obStatus.Success())
					{
						// Badness happened
						DialogResult obResult = FileInUseDialogWindow(obStatus.GetErrorString(), strDefaultResponseToFileProblems);
						if (obResult == DialogResult.Abort)
						{
							// It is all bad, abandon ship
							return;
						}
						else if (obResult == DialogResult.Retry)
						{
							// Try again
							i--;
						}
						else if (obResult == DialogResult.Ignore)
						{
							// Ignore
						}
					}
					else
					{
						// Make readonly
						File.SetAttributes(strDestinationPathAndFileName, File.GetAttributes(strDestinationPathAndFileName) | FileAttributes.ReadOnly);
					}
				}
			}

			SetLabelText(UpdateLabel, "Updating : Done");
			SetProgressBarValue(UpdateProgressBar, obAStrFilesToUpdate.Count + 1);

			// Make sure everything is ok
			obStatus = new rageStatus();
		}


		public void AddFiles(string strLocalPath, string strSourcePath, List<string> obAStrFilesToAdd, string strDefaultResponseToFileProblems, out rageStatus obStatus)
		{
			SetProgressBarMin(AddProgressBar, 0);
			SetProgressBarMax(AddProgressBar, obAStrFilesToAdd.Count + 1);

			// Add everything
			for (int i = 0; i < obAStrFilesToAdd.Count; i++)
			{
				string strFileToAdd = obAStrFilesToAdd[i];
				SetProgressBarValue(AddProgressBar, i);
				SetLabelText(AddLabel, "Adding : " + strFileToAdd);
				string strDestinationPathAndFileName = strLocalPath + strFileToAdd;
				// The delete worked, so Add
				rageFileUtilities.CopyFile(strSourcePath + strFileToAdd, strDestinationPathAndFileName, out obStatus);

				if (!obStatus.Success())
				{
					// Badness happened
					DialogResult obResult = FileInUseDialogWindow(obStatus.GetErrorString(), strDefaultResponseToFileProblems);
					if (obResult == DialogResult.Abort)
					{
						// It is all bad, abandon ship
						return;
					}
					else if (obResult == DialogResult.Retry)
					{
						// Try again
						i--;
					}
					else if (obResult == DialogResult.Ignore)
					{
						// Ignore
					}
				}
				else
				{
					// Make readonly
					File.SetAttributes(strDestinationPathAndFileName, File.GetAttributes(strDestinationPathAndFileName) | FileAttributes.ReadOnly);
				}
			}

			SetLabelText(AddLabel, "Adding : Done");
			SetProgressBarValue(AddProgressBar, obAStrFilesToAdd.Count + 1);

			// Make sure everything is ok
			obStatus = new rageStatus();
		}


		private DialogResult FileInUseDialogWindow(string strErrorString, string strDefaultResponseToFileProblems)
		{
			// Log it
			Console.WriteLine("Error : " + strErrorString);

			// Handle error
			switch (strDefaultResponseToFileProblems.ToLower())
			{
				case "abort":
					{
						return DialogResult.Cancel;
					}
				case "retry":
					{
						return DialogResult.Retry;
					}
				case "ignore":
					{
						return DialogResult.Ignore;
					}
				default:
					{
						// No default given, so ask user what to do
						string message = strErrorString;
						string caption = "File in use";
						MessageBoxButtons buttons = MessageBoxButtons.AbortRetryIgnore;

						// Displays the MessageBox.
						return MessageBox.Show(message, caption, buttons);
					}

			}
		}


		public void AddStartMenuItems(string strLocalPath, string strNetworkPath)
		{
			SetProgressBarMin(StartMenuProgressBar, 0);
			SetProgressBarMax(StartMenuProgressBar, 6);
			SetProgressBarValue(StartMenuProgressBar, 1);
			SetLabelText(StartMenuLabel, "Start Menu : Retrieving Start Menu");

			// Use the Environment class to retrieve the Start Menu on the system
			string strStartMenuFolder = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu);
			// Use the name of the company to create the Start Menu folder for your application
			string strRageStartMenuDirectoryPath = strStartMenuFolder + "\\Rage";
			rageFileUtilities.MakeDir(strRageStartMenuDirectoryPath);

			// Add shortcuts
			SetProgressBarValue(StartMenuProgressBar, 2);
			SetLabelText(StartMenuLabel, "Start Menu : Adding Shortcuts to Common Tools");
			string[] astrShortCuts = { (strLocalPath + "/tools/Modules/base/rage/exes/rageToolCommandCenter.exe"), (strLocalPath + "/tools/Modules/base/rage/exes/rageBatchExporter.exe"), (strLocalPath + "/tools/Modules/base/rage/exes/rageBatchFolderExporter.exe"), (strLocalPath + "/tools/Modules/base/rage/exes/rageConvertMBToMA.exe"), (strLocalPath + "/tools/Modules/base/rage/exes/rag.exe"), (strLocalPath + "/tools/Modules/base/rage/exes/sample_simpleworld.exe"), (strLocalPath + "/tools/Modules/base/rage/exes/rageLightMapper.exe") };
			string[] astrShortCutArgs = { "", "", "", "", "", "-rag", "" };
			string[] astrShortNames = { "Rage Tool Command Center", "Batch Maya Exporter", "Batch Maya Folder Exporter", "MB to MA", "RAG", "Simple World Viewer", "Rage Light Mapper" };
			string[] astrDescriptions = { "A tool tray application that adds right click options to some filetypes", "Export many maya files", "Export all the maya files in one folder", "Convert a Maya Binary file to a Maya Ascii file", "Rage Application GUI", "The Rage Viewer", "Generate Light Maps For a Maya File" };
			for (int i = 0; i < astrShortCuts.Length; i++)
			{
				IWshRuntimeLibrary.IWshShell_Class TheShell = new IWshRuntimeLibrary.IWshShell_Class();
				IWshRuntimeLibrary.IWshShortcut_Class shellLink = (IWshRuntimeLibrary.IWshShortcut_Class)TheShell.CreateShortcut(strRageStartMenuDirectoryPath + "\\" + astrShortNames[i] + ".lnk");
				shellLink.TargetPath = astrShortCuts[i].Replace("/", "\\");
				shellLink.Description = astrDescriptions[i];
				shellLink.Arguments = astrShortCutArgs[i];
				//				shellLink.IconLocation = listBox1.SelectedItems[i] + ", 0";
				shellLink.Save();
			}

			// Add environments
			SetProgressBarValue(StartMenuProgressBar, 3);
			SetLabelText(StartMenuLabel, "Start Menu : Searching for valid Rage environments");
			string strRageEnvironmentsStartMenuDirectoryPath = strRageStartMenuDirectoryPath + "\\Environments";
			rageFileUtilities.MakeDir(strRageEnvironmentsStartMenuDirectoryPath);

			// Get possibilities
			List<string> obAStrListOfProjectSettingBatFiles = rageFileUtilities.GetFilesAsStringList(strLocalPath + "rage/tools/modules/base/rage/exes", "SetupEnvironmentFor*And*.bat");

			// Add a menu for each bat file
			SetProgressBarValue(StartMenuProgressBar, 4);
			SetLabelText(StartMenuLabel, "Start Menu : Adding Rage environments");
			foreach (string strProjectSettingBatFile in obAStrListOfProjectSettingBatFiles)
			{
				// Split up the file into the important parts
				int iPosOfProjectStart = strProjectSettingBatFile.ToLower().IndexOf("setupenvironmentfor") + "setupenvironmentfor".Length;
				int iPosOfProjectEnd = strProjectSettingBatFile.ToLower().Substring(iPosOfProjectStart, strProjectSettingBatFile.Length - iPosOfProjectStart).LastIndexOf("and");
				string strProjectPart = strProjectSettingBatFile.ToLower().Substring(iPosOfProjectStart, iPosOfProjectEnd);
				int iPosOfDCCStart = iPosOfProjectStart + iPosOfProjectEnd + 3;
				int iPosOfDCCEnd = strProjectSettingBatFile.Length - 4;
				string strDCC = strProjectSettingBatFile.ToLower().Substring(iPosOfDCCStart, iPosOfDCCEnd - iPosOfDCCStart);

				// Add shortcut
				IWshRuntimeLibrary.IWshShell_Class TheShell = new IWshRuntimeLibrary.IWshShell_Class();
				IWshRuntimeLibrary.IWshShortcut_Class shellLink = (IWshRuntimeLibrary.IWshShortcut_Class)TheShell.CreateShortcut(strRageEnvironmentsStartMenuDirectoryPath + "\\" + strProjectPart + " and " + strDCC + ".lnk");
				shellLink.TargetPath = strProjectSettingBatFile.Replace("/", "\\");
				shellLink.Description = "Set up environment for " + strProjectPart + " and " + strDCC;
				shellLink.Save();
			}

			// Add A link to this installer
			{
				SetProgressBarValue(StartMenuProgressBar, 5);
				SetLabelText(StartMenuLabel, "Start Menu : Adding install shortcuts");
				string strRageUpdateStartMenuDirectoryPath = strRageStartMenuDirectoryPath + "\\Update";
				rageFileUtilities.MakeDir(strRageUpdateStartMenuDirectoryPath);

				// Get myself
				string strGetMyTag = rageFileUtilities.GetFilenameFromFilePath(strNetworkPath);

				// Add shortcut
				IWshRuntimeLibrary.IWshShell_Class TheShell = new IWshRuntimeLibrary.IWshShell_Class();
				IWshRuntimeLibrary.IWshShortcut_Class shellLink = (IWshRuntimeLibrary.IWshShortcut_Class)TheShell.CreateShortcut(strRageUpdateStartMenuDirectoryPath + "\\Update tools to " + strGetMyTag + ".lnk");
				shellLink.TargetPath = (strNetworkPath +"/SetUp.bat").Replace("/", "\\");
				shellLink.Description = "Run the Rage Tool Installer To Install the " + strGetMyTag + " tools.";
				shellLink.Save();
			}

			// Done
			SetProgressBarValue(StartMenuProgressBar, 6);
			SetLabelText(StartMenuLabel, "Start Menu : Done");
		}

		public void SetupEnvironment(string strLocalPath, string strProject, string strDCC, string strDefaultResponseToFileProblems, out rageStatus obStatus)
		{
			SetProgressBarMin(EnvironmentProgressBar, 0);
			SetProgressBarMax(EnvironmentProgressBar, 3);
			SetProgressBarValue(EnvironmentProgressBar, 1);
			SetLabelText(EnvironmentLabel, "Environment : Searching for environment file");

			// Work out stuff
			string strBatFile = strLocalPath + "rage/tools/modules/base/rage/exes/SetupEnvironmentFor" + strProject + "And" + strDCC + ".bat";

			// Do it
			SetProgressBarValue(EnvironmentProgressBar, 2);
			SetLabelText(EnvironmentLabel, "Environment : " + strBatFile);
			rageExecuteCommand obExecuteCommand = new rageExecuteCommand();
			obExecuteCommand.Command = strBatFile;
			obExecuteCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened
				DialogResult obResult = FileInUseDialogWindow(obStatus.GetErrorString(), strDefaultResponseToFileProblems);
				if (obResult == DialogResult.Abort)
				{
					// It is all bad, abandon ship
					return;
				}
				else if (obResult == DialogResult.Retry)
				{
					// Try again
					SetupEnvironment(strLocalPath, strProject, strDCC, strDefaultResponseToFileProblems, out obStatus);
				}
				else if (obResult == DialogResult.Ignore)
				{
					// Ignore
				}
			}

			// Done
			SetProgressBarValue(EnvironmentProgressBar, 3);
			SetLabelText(EnvironmentLabel, "Environment : Done");
		}

	}
}

