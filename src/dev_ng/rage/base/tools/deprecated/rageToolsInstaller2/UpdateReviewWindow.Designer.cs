namespace rageToolsInstaller2
{
	partial class UpdateReviewWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.FilesToAddTextBox = new System.Windows.Forms.RichTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.FilesToUpdateTextBox = new System.Windows.Forms.RichTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.FilesToDeleteTextBox = new System.Windows.Forms.RichTextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.FilesToAddTextBox);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.FilesToDeleteTextBox);
			this.groupBox1.Controls.Add(this.FilesToUpdateTextBox);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.SetChildIndex(this.label1, 0);
			this.groupBox1.Controls.SetChildIndex(this.label5, 0);
			this.groupBox1.Controls.SetChildIndex(this.FilesToUpdateTextBox, 0);
			this.groupBox1.Controls.SetChildIndex(this.FilesToDeleteTextBox, 0);
			this.groupBox1.Controls.SetChildIndex(this.label2, 0);
			this.groupBox1.Controls.SetChildIndex(this.infoLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.FilesToAddTextBox, 0);
			// 
			// FilesToAddTextBox
			// 
			this.FilesToAddTextBox.Location = new System.Drawing.Point(40, 200);
			this.FilesToAddTextBox.Name = "FilesToAddTextBox";
			this.FilesToAddTextBox.Size = new System.Drawing.Size(480, 56);
			this.FilesToAddTextBox.TabIndex = 22;
			this.FilesToAddTextBox.Text = "richTextBox3";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(40, 184);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(480, 16);
			this.label2.TabIndex = 23;
			this.label2.Text = "Files to be added:";
			// 
			// FilesToUpdateTextBox
			// 
			this.FilesToUpdateTextBox.Location = new System.Drawing.Point(40, 112);
			this.FilesToUpdateTextBox.Name = "FilesToUpdateTextBox";
			this.FilesToUpdateTextBox.Size = new System.Drawing.Size(480, 56);
			this.FilesToUpdateTextBox.TabIndex = 21;
			this.FilesToUpdateTextBox.Text = "richTextBox2";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(40, 96);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(480, 16);
			this.label1.TabIndex = 20;
			this.label1.Text = "Files to be updated:";
			// 
			// FilesToDeleteTextBox
			// 
			this.FilesToDeleteTextBox.Location = new System.Drawing.Point(40, 24);
			this.FilesToDeleteTextBox.Name = "FilesToDeleteTextBox";
			this.FilesToDeleteTextBox.Size = new System.Drawing.Size(480, 56);
			this.FilesToDeleteTextBox.TabIndex = 19;
			this.FilesToDeleteTextBox.Text = "richTextBox1";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(40, 8);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(480, 16);
			this.label5.TabIndex = 18;
			this.label5.Text = "Files to be deleted:";
			// 
			// UpdateReviewWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Name = "UpdateReviewWindow";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.RichTextBox FilesToAddTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.RichTextBox FilesToUpdateTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.RichTextBox FilesToDeleteTextBox;
		private System.Windows.Forms.Label label5;
	}
}
