using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace rageToolsInstaller2
{
	public partial class BaseInstallWindow : Form
	{
		public BaseInstallWindow()
		{
			InitializeComponent();
		}

		private void Canceled()
		{
			Close();
		}

		private void cancelButton_Click(object sender, EventArgs e)
		{
			// Close everything
			Canceled();
		}

		private void nextButton_Click(object sender, EventArgs e)
		{
			// Close everything, but don't cancel
			Hide();
		}

		private delegate void EnableButtonCallback(Button obButton, bool bValue);
		public void EnableButton(Button obButton, bool bValue)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (obButton.InvokeRequired)
			{
				EnableButtonCallback d = new EnableButtonCallback(EnableButton);
				this.Invoke(d, new object[] { obButton, bValue });
			}
			else
			{
				obButton.Enabled = bValue;
			}
		}
	}
}