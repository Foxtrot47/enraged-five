namespace rageToolsInstaller2
{
	partial class ThirdPartyProgressWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.InstallationProgressLabel = new System.Windows.Forms.Label();
			this.InstallationProgressBar = new System.Windows.Forms.ProgressBar();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// infoLabel
			// 
			this.infoLabel.Location = new System.Drawing.Point(48, 69);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.InstallationProgressBar);
			this.groupBox1.Controls.Add(this.InstallationProgressLabel);
			this.groupBox1.Controls.SetChildIndex(this.infoLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.InstallationProgressLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.InstallationProgressBar, 0);
			// 
			// InstallationProgressLabel
			// 
			this.InstallationProgressLabel.Location = new System.Drawing.Point(40, 16);
			this.InstallationProgressLabel.Name = "InstallationProgressLabel";
			this.InstallationProgressLabel.Size = new System.Drawing.Size(480, 16);
			this.InstallationProgressLabel.TabIndex = 21;
			this.InstallationProgressLabel.Text = "Installation Progress:";
			// 
			// InstallationProgressBar
			// 
			this.InstallationProgressBar.Location = new System.Drawing.Point(40, 35);
			this.InstallationProgressBar.Name = "InstallationProgressBar";
			this.InstallationProgressBar.Size = new System.Drawing.Size(480, 24);
			this.InstallationProgressBar.TabIndex = 20;
			// 
			// ThirdPartyProgressWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Name = "ThirdPartyProgressWindow";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label InstallationProgressLabel;
		private System.Windows.Forms.ProgressBar InstallationProgressBar;
	}
}
