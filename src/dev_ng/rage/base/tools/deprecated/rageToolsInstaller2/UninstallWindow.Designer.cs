namespace rageToolsInstaller2
{
	partial class UninstallWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.AssembliesLabel = new System.Windows.Forms.Label();
			this.AssembliesProgressBar = new System.Windows.Forms.ProgressBar();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.AssembliesLabel);
			this.groupBox1.Controls.Add(this.AssembliesProgressBar);
			this.groupBox1.Controls.SetChildIndex(this.infoLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.AssembliesProgressBar, 0);
			this.groupBox1.Controls.SetChildIndex(this.AssembliesLabel, 0);
			// 
			// AssembliesLabel
			// 
			this.AssembliesLabel.Location = new System.Drawing.Point(40, 16);
			this.AssembliesLabel.Name = "GEOsLabel";
			this.AssembliesLabel.Size = new System.Drawing.Size(480, 16);
			this.AssembliesLabel.TabIndex = 29;
			this.AssembliesLabel.Text = "Removing GEOs:";
			// 
			// AssembliesProgressBar
			// 
			this.AssembliesProgressBar.Location = new System.Drawing.Point(40, 35);
			this.AssembliesProgressBar.Name = "GEOsProgressBar";
			this.AssembliesProgressBar.Size = new System.Drawing.Size(480, 24);
			this.AssembliesProgressBar.TabIndex = 28;
			// 
			// UninstallWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Name = "UninstallWindow";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label AssembliesLabel;
		private System.Windows.Forms.ProgressBar AssembliesProgressBar;
	}
}
