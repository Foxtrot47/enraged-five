namespace rageToolsInstaller2
{
	partial class IntroWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.InstallerInfoLabel = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.InstallerInfoLabel);
			this.groupBox1.Controls.SetChildIndex(this.InstallerInfoLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.label1, 0);
			this.groupBox1.Controls.SetChildIndex(this.infoLabel, 0);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(396, 242);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(124, 13);
			this.label1.TabIndex = 18;
			this.label1.Text = "Click \"Next\" to continue.";
			// 
			// InstallerInfoLabel
			// 
			this.InstallerInfoLabel.AutoSize = true;
			this.InstallerInfoLabel.Enabled = false;
			this.InstallerInfoLabel.Location = new System.Drawing.Point(48, 242);
			this.InstallerInfoLabel.Name = "InstallerInfoLabel";
			this.InstallerInfoLabel.Size = new System.Drawing.Size(35, 13);
			this.InstallerInfoLabel.TabIndex = 19;
			this.InstallerInfoLabel.Text = "label2";
			// 
			// IntroWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Name = "IntroWindow";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label InstallerInfoLabel;
	}
}
