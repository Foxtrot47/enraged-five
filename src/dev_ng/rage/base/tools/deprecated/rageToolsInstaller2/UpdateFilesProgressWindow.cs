using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using rageUsefulCSharpToolClasses;

namespace rageToolsInstaller2
{
	public partial class UpdateFilesProgressWindow : rageToolsInstaller2.ProgressWindow
	{
		public UpdateFilesProgressWindow(bool bBatchMode)
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Update Progress";
			this.infoLabel.Text = "Updating files";
			this.infoLabel.Visible = false;

			// Open the window in the background
			if (!bBatchMode)
			{
				EnableButton(cancelButton, false);
				EnableButton(nextButton, false);
				EnableButton(backButton, false);
				//Thread obWindowThread = new Thread(new ThreadStart(ShowWindow));
				//obWindowThread.Start();
			}
		}

		public void DeleteFiles(string strLocalPath, List<string> obAStrFilesToDelete, string strDefaultResponseToFileProblems, out rageStatus obStatus)
		{
			SetProgressBarMin(DeleteProgressBar, 0);
			SetProgressBarMax(DeleteProgressBar, obAStrFilesToDelete.Count + 1);

			// Delete everything
			for(int i=0; i<obAStrFilesToDelete.Count; i++)
			{
				string strFileToDelete = obAStrFilesToDelete[i];
				SetProgressBarValue(DeleteProgressBar, i);
				SetLabelText(DeleteLabel, "Deleting : " + strFileToDelete);
				rageFileUtilities.DeleteLocalFile(strLocalPath + strFileToDelete, out obStatus);

				if (!obStatus.Success())
				{
					// Badness happened
					DialogResult obResult = FileInUseDialogWindow(obStatus.GetErrorString(), strDefaultResponseToFileProblems);
					if (obResult == DialogResult.Abort)
					{
						// It is all bad, abandon ship
						return;
					}
					else if (obResult == DialogResult.Retry)
					{
						// Try again
						i--;
					}
					else if (obResult == DialogResult.Ignore)
					{
						// Ignore
					}
				}
			}

			SetLabelText(DeleteLabel, "Deleting : Done");
			SetProgressBarValue(DeleteProgressBar, obAStrFilesToDelete.Count + 1);

			// Make sure everything is ok
			obStatus = new rageStatus();
		}


		public void UpdateFiles(string strLocalPath, string strSourcePath, List<string> obAStrFilesToUpdate, string strDefaultResponseToFileProblems, out rageStatus obStatus)
		{
			SetProgressBarMin(UpdateProgressBar, 0);
			SetProgressBarMax(UpdateProgressBar, obAStrFilesToUpdate.Count + 1);

			// Update everything
			for (int i = 0; i < obAStrFilesToUpdate.Count; i++)
			{
				string strFileToUpdate = obAStrFilesToUpdate[i];
				SetProgressBarValue(UpdateProgressBar, i);
				SetLabelText(UpdateLabel, "Updating : " + strFileToUpdate);
				string strDestinationPathAndFileName = strLocalPath + strFileToUpdate;
				rageFileUtilities.DeleteLocalFile(strDestinationPathAndFileName, out obStatus);

				if (!obStatus.Success())
				{
					// Badness happened
					DialogResult obResult = FileInUseDialogWindow(obStatus.GetErrorString(), strDefaultResponseToFileProblems);
					if (obResult == DialogResult.Abort)
					{
						// It is all bad, abandon ship
						return;
					}
					else if (obResult == DialogResult.Retry)
					{
						// Try again
						i--;
					}
					else if (obResult == DialogResult.Ignore)
					{
						// Ignore
					}
				}
				else
				{
					// The delete worked, so update
					rageFileUtilities.CopyFile(strSourcePath + strFileToUpdate, strDestinationPathAndFileName, out obStatus);

					if (!obStatus.Success())
					{
						// Badness happened
						DialogResult obResult = FileInUseDialogWindow(obStatus.GetErrorString(), strDefaultResponseToFileProblems);
						if (obResult == DialogResult.Abort)
						{
							// It is all bad, abandon ship
							return;
						}
						else if (obResult == DialogResult.Retry)
						{
							// Try again
							i--;
						}
						else if (obResult == DialogResult.Ignore)
						{
							// Ignore
						}
					}
					else
					{
						// Make readonly
						File.SetAttributes(strDestinationPathAndFileName, File.GetAttributes(strDestinationPathAndFileName) | FileAttributes.ReadOnly);
					}
				}
			}

			SetLabelText(UpdateLabel, "Updating : Done");
			SetProgressBarValue(UpdateProgressBar, obAStrFilesToUpdate.Count + 1);

			// Make sure everything is ok
			obStatus = new rageStatus();
		}


		public void AddFiles(string strLocalPath, string strSourcePath, List<string> obAStrFilesToAdd, string strDefaultResponseToFileProblems, out rageStatus obStatus)
		{
			SetProgressBarMin(AddProgressBar, 0);
			SetProgressBarMax(AddProgressBar, obAStrFilesToAdd.Count + 1);

			// Add everything
			for (int i = 0; i < obAStrFilesToAdd.Count; i++)
			{
				string strFileToAdd = obAStrFilesToAdd[i];
				SetProgressBarValue(AddProgressBar, i);
				SetLabelText(AddLabel, "Adding : " + strFileToAdd);
				string strDestinationPathAndFileName = strLocalPath + strFileToAdd;
				// The delete worked, so Add
				rageFileUtilities.CopyFile(strSourcePath + strFileToAdd, strDestinationPathAndFileName, out obStatus);

				if (!obStatus.Success())
				{
					// Badness happened
					DialogResult obResult = FileInUseDialogWindow(obStatus.GetErrorString(), strDefaultResponseToFileProblems);
					if (obResult == DialogResult.Abort)
					{
						// It is all bad, abandon ship
						return;
					}
					else if (obResult == DialogResult.Retry)
					{
						// Try again
						i--;
					}
					else if (obResult == DialogResult.Ignore)
					{
						// Ignore
					}
				}
				else
				{
					// Make readonly
					File.SetAttributes(strDestinationPathAndFileName, File.GetAttributes(strDestinationPathAndFileName) | FileAttributes.ReadOnly);
				}
			}

			SetLabelText(AddLabel, "Adding : Done");
			SetProgressBarValue(AddProgressBar, obAStrFilesToAdd.Count + 1);

			// Make sure everything is ok
			obStatus = new rageStatus();
		}


		private DialogResult FileInUseDialogWindow(string strErrorString, string strDefaultResponseToFileProblems)
		{
			// Log it
			Console.WriteLine("Error : " + strErrorString);

			// Handle error
			switch (strDefaultResponseToFileProblems.ToLower())
			{
				case "abort":
					{
						return DialogResult.Cancel;
					}
				case "retry":
					{
						return DialogResult.Retry;
					}
				case "ignore":
					{
						return DialogResult.Ignore;
					}
				default:
					{
						// No default given, so ask user what to do
						string message = strErrorString;
						string caption = "File in use";
						MessageBoxButtons buttons = MessageBoxButtons.AbortRetryIgnore;

						// Displays the MessageBox.
						return MessageBox.Show(message, caption, buttons);
					}

			}
		}
	}
}

