using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace rageToolsInstaller2
{
	public partial class ProgressWindow : rageToolsInstaller2.BaseInstallWindow
	{
		public ProgressWindow()
		{
			InitializeComponent();

			// EVIL HACK!!!!!!
			Form.CheckForIllegalCrossThreadCalls = false;
			// EVIL HACK!!!!!!
			this.nextButton.Enabled = false;
			this.backButton.Enabled = false;
			this.cancelButton.Enabled = false;
		}

		public void OpenWindow()
		{
			Thread obShowWindowThread = new Thread(delegate()
				{
					Console.WriteLine(this.infoLabel.Text +" ProgressWindow::ShowWindow() 0");
					if((!this.IsDisposed) && (!this.Disposing))
					{
						Console.WriteLine(this.infoLabel.Text +" ProgressWindow::ShowWindow() Creating control");
						CreateControl();
						Console.WriteLine(this.infoLabel.Text +" ProgressWindow::ShowWindow() Created control");
						this.ShowDialog();
					}
					else
					{
						Console.WriteLine(this.infoLabel.Text +" Not showing the progress window as it has already been disposed of");
					}
					Console.WriteLine(this.infoLabel.Text +" ProgressWindow::ShowWindow() 1");
				});
			obShowWindowThread.Name = "ProgressWindow.ShowWindow Thread";
			obShowWindowThread.Start();
		}

		public void Done()
		{
			// Wait for it to open, before closing it
			do
			{
				Console.WriteLine(this.infoLabel.Text + " Waiting to close");
				Thread.Sleep(100);
			} while (!this.Visible || !this.Created || (this.Handle == IntPtr.Zero));

			//Close dialogue
			this.Invoke(new System.Windows.Forms.MethodInvoker(
				delegate()
				{
					do
					{
						Console.WriteLine(this.infoLabel.Text +" Waiting to close");
						Thread.Sleep(100);
					} while (!this.Visible || !this.Created || (this.Handle == IntPtr.Zero));
					Console.WriteLine("Closing");
					this.Close();
					Console.WriteLine("Closed");
				}
				));
			do
			{
				Console.WriteLine("Waiting to be closed");
				Thread.Sleep(100);
			} while (this.Visible);
		}

		private delegate void SetProgressBarMaxCallback(ProgressBar obProgressBar, int iMax);
		protected void SetProgressBarMax(ProgressBar obProgressBar, int iMax)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (obProgressBar.InvokeRequired)
			{
				SetProgressBarMaxCallback d = new SetProgressBarMaxCallback(SetProgressBarMax);
				this.Invoke(d, new object[] { obProgressBar, iMax });
			}
			else
			{
				obProgressBar.Maximum = iMax;
			}
		}


		private delegate void SetProgressBarMinCallback(ProgressBar obProgressBar, int iMin);
		protected void SetProgressBarMin(ProgressBar obProgressBar, int iMin)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (obProgressBar.InvokeRequired)
			{
				SetProgressBarMinCallback d = new SetProgressBarMinCallback(SetProgressBarMin);
				this.Invoke(d, new object[] { obProgressBar, iMin });
			}
			else
			{
				obProgressBar.Minimum = iMin;
			}
		}


		private delegate void SetProgressBarValueCallback(ProgressBar obProgressBar, int iValue);
		protected void SetProgressBarValue(ProgressBar obProgressBar, int iValue)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (obProgressBar.InvokeRequired)
			{
				SetProgressBarValueCallback d = new SetProgressBarValueCallback(SetProgressBarValue);
				this.Invoke(d, new object[] { obProgressBar, iValue });
			}
			else
			{
				obProgressBar.Value = iValue;
			}
		}

		private delegate void IncProgressBarValueCallback(ProgressBar obProgressBar);
		protected void IncProgressBarValue(ProgressBar obProgressBar)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (obProgressBar.InvokeRequired)
			{
				IncProgressBarValueCallback d = new IncProgressBarValueCallback(IncProgressBarValue);
				this.Invoke(d, new object[] { obProgressBar });
			}
			else
			{
				obProgressBar.Value++;
			}
		}


		private delegate void SetLabelTextCallback(Label obLabel, string strText);
		protected void SetLabelText(Label obLabel, string strText)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (obLabel.InvokeRequired)
			{
				SetLabelTextCallback d = new SetLabelTextCallback(SetLabelText);
				this.Invoke(d, new object[] { obLabel, strText });
			}
			else
			{
				obLabel.Text = strText;
			}
		}
	}
}

