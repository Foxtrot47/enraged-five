﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("rageToolsInstaller2")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
// Now in CommonAssemblyInfo.cs: [assembly: AssemblyCompany("Rockstar San Diego")]
[assembly: AssemblyProduct("rageToolsInstaller2")]
// Now in CommonAssemblyInfo.cs: [assembly: AssemblyCopyright("Copyright © Rockstar San Diego 2006")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e2289c56-4db6-46ac-8d0b-be86a947fa8f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// Now in CommonAssemblyInfo.cs: [assembly: AssemblyVersion("1.0.0.0")]
// Now in CommonAssemblyInfo.cs: [assembly: AssemblyFileVersion("1.0.0.0")]
