using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace rageToolsInstaller2
{
	public partial class ProjectAndDCCWindow : rageToolsInstaller2.BaseInstallWindow
	{
		private class ProjectWithDCCs
		{
			private string m_strProjectName = "";
			public string ProjectName
			{
				get
				{
					return m_strProjectName;
				}
				set
				{
					m_strProjectName = value.ToLower();
				}
			}

			private List<string> m_obAStrDCCs = new List<string>();
			public List<string> DCCs
			{
				get
				{
					return m_obAStrDCCs;
				}
			}

			public void AddDCC(string strDCC)
			{
				m_obAStrDCCs.Add(strDCC);
				m_obAStrDCCs.Sort();
			}
		}

		public ProjectAndDCCWindow(List<string> obAStrProjectEnvironmentFiles)
		{
			InitializeComponent();

			// Fill out the List
			obAStrProjectEnvironmentFiles.Sort();
			foreach (string strProjectSettingBatFile in obAStrProjectEnvironmentFiles)
			{
				// Split up the file into the important parts
				int iPosOfProjectStart = strProjectSettingBatFile.ToLower().IndexOf("setupenvironmentfor") + "setupenvironmentfor".Length;
				int iPosOfProjectEnd = strProjectSettingBatFile.ToLower().Substring(iPosOfProjectStart, strProjectSettingBatFile.Length - iPosOfProjectStart).LastIndexOf("and");
				string strProjectPart = strProjectSettingBatFile.ToLower().Substring(iPosOfProjectStart, iPosOfProjectEnd);
				int iPosOfDCCStart = iPosOfProjectStart + iPosOfProjectEnd + 3;
				int iPosOfDCCEnd = strProjectSettingBatFile.Length - 4;
				string strDCC = strProjectSettingBatFile.ToLower().Substring(iPosOfDCCStart, iPosOfDCCEnd - iPosOfDCCStart);

				// Add
				AddProjectDCCPair(strProjectPart, strDCC);
			}

			// Fill in the Project Combo Box
			foreach(ProjectWithDCCs obProjectWithDCCs in m_AObProjectsWithDCCs)
			{
				ProjectComboBox.Items.Add(obProjectWithDCCs.ProjectName);
			}
		}

		private void AddProjectDCCPair(string strProject, string strDCC)
		{
			for(int i=0; i<m_AObProjectsWithDCCs.Count; i++)
			{
				if(m_AObProjectsWithDCCs[i].ProjectName == strProject)
				{
					// Bingo, already have the project
					m_AObProjectsWithDCCs[i].AddDCC(strDCC);
					return;
				}
			}

			// I don't already have the project, so add it
			ProjectWithDCCs obProjectWithDCC = new ProjectWithDCCs();
			obProjectWithDCC.ProjectName = strProject;
			obProjectWithDCC.AddDCC(strDCC);
			m_AObProjectsWithDCCs.Add(obProjectWithDCC);
		}

		private List<ProjectWithDCCs> m_AObProjectsWithDCCs = new List<ProjectWithDCCs>();

		public string Project
		{
			get
			{
				return (string)ProjectComboBox.SelectedItem;
			}
			set
			{
				for (int i = 0; i < m_AObProjectsWithDCCs.Count; i++)
				{
					if (m_AObProjectsWithDCCs[i].ProjectName == value.ToLower())
					{
						// Select it
						ProjectComboBox.SelectedIndex = i;
						UpdateDCCComboBox(i);
						break;
					}
				}
			}
		}

		public string DCC
		{
			get
			{
				return (string)DCCComboBox.SelectedItem;
			}
			set
			{
				int iProjectNumber = ProjectComboBox.SelectedIndex;
                if ( (iProjectNumber > -1) && (iProjectNumber < m_AObProjectsWithDCCs.Count) )
                {
                    for ( int i = 0; i < m_AObProjectsWithDCCs[iProjectNumber].DCCs.Count; i++ )
                    {
                        if ( m_AObProjectsWithDCCs[iProjectNumber].DCCs[i] == value.ToLower() )
                        {
                            // Select it
                            DCCComboBox.SelectedIndex = i;
                            break;
                        }
                    }
                }
			}
		}

		private void UpdateDCCComboBox(int iProjectNumber)
		{
			// Update the DCC Combo box
			DCCComboBox.Items.Clear();
			foreach (string strDCC in m_AObProjectsWithDCCs[iProjectNumber].DCCs)
			{
				DCCComboBox.Items.Add(strDCC);
			}
			DCCComboBox.SelectedIndex = DCCComboBox.Items.Count - 1;
		}
		
		private void ProjectComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			// Update DCC combo box
			UpdateDCCComboBox(ProjectComboBox.SelectedIndex);
		}
	}
}

