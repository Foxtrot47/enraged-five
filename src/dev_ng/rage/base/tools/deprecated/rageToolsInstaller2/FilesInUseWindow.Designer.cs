namespace rageToolsInstaller2
{
	partial class FilesInUseWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "Rabbits",
            "Fish"}, -1);
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Sharks");
			this.filesInUseWindowBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.ApplicationsToKillListView = new System.Windows.Forms.ListView();
			this.ApplicationName = new System.Windows.Forms.ColumnHeader();
			this.ApplicationPid = new System.Windows.Forms.ColumnHeader();
			this.ShutdownButton = new System.Windows.Forms.Button();
			this.RefreshButton = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.filesInUseWindowBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.RefreshButton);
			this.groupBox1.Controls.Add(this.ShutdownButton);
			this.groupBox1.Controls.Add(this.ApplicationsToKillListView);
			this.groupBox1.Controls.SetChildIndex(this.infoLabel, 0);
			this.groupBox1.Controls.SetChildIndex(this.ApplicationsToKillListView, 0);
			this.groupBox1.Controls.SetChildIndex(this.ShutdownButton, 0);
			this.groupBox1.Controls.SetChildIndex(this.RefreshButton, 0);
			// 
			// filesInUseWindowBindingSource
			// 
			this.filesInUseWindowBindingSource.DataSource = typeof(rageToolsInstaller2.FilesInUseWindow);
			// 
			// ApplicationsToKillListView
			// 
			this.ApplicationsToKillListView.Activation = System.Windows.Forms.ItemActivation.OneClick;
			this.ApplicationsToKillListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ApplicationName,
            this.ApplicationPid});
			this.ApplicationsToKillListView.FullRowSelect = true;
			listViewItem2.IndentCount = 1;
			this.ApplicationsToKillListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2});
			this.ApplicationsToKillListView.Location = new System.Drawing.Point(51, 57);
			this.ApplicationsToKillListView.Name = "ApplicationsToKillListView";
			this.ApplicationsToKillListView.ShowGroups = false;
			this.ApplicationsToKillListView.Size = new System.Drawing.Size(277, 197);
			this.ApplicationsToKillListView.TabIndex = 23;
			this.ApplicationsToKillListView.UseCompatibleStateImageBehavior = false;
			this.ApplicationsToKillListView.View = System.Windows.Forms.View.Details;
			// 
			// ApplicationName
			// 
			this.ApplicationName.Text = "Name";
			this.ApplicationName.Width = 213;
			// 
			// ApplicationPid
			// 
			this.ApplicationPid.Text = "Pid";
			// 
			// ShutdownButton
			// 
			this.ShutdownButton.Location = new System.Drawing.Point(350, 57);
			this.ShutdownButton.Name = "ShutdownButton";
			this.ShutdownButton.Size = new System.Drawing.Size(167, 23);
			this.ShutdownButton.TabIndex = 24;
			this.ShutdownButton.Text = "Shutdown Applications";
			this.ShutdownButton.UseVisualStyleBackColor = true;
			this.ShutdownButton.Click += new System.EventHandler(this.ShutdownButton_Click);
			// 
			// RefreshButton
			// 
			this.RefreshButton.Location = new System.Drawing.Point(350, 86);
			this.RefreshButton.Name = "RefreshButton";
			this.RefreshButton.Size = new System.Drawing.Size(167, 23);
			this.RefreshButton.TabIndex = 25;
			this.RefreshButton.Text = "Refresh List";
			this.RefreshButton.UseVisualStyleBackColor = true;
			this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
			// 
			// FilesInUseWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.ClientSize = new System.Drawing.Size(496, 381);
			this.Name = "FilesInUseWindow";
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.filesInUseWindowBindingSource)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.BindingSource filesInUseWindowBindingSource;
		private System.Windows.Forms.ListView ApplicationsToKillListView;
		private System.Windows.Forms.ColumnHeader ApplicationName;
		private System.Windows.Forms.ColumnHeader ApplicationPid;
		private System.Windows.Forms.Button ShutdownButton;
		private System.Windows.Forms.Button RefreshButton;

	}
}
