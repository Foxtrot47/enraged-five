using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;
using rageUsefulCSharpToolClasses;

namespace rageToolsInstaller2
{
	public partial class ThirdPartyProgressWindow : rageToolsInstaller2.ProgressWindow
	{
		public ThirdPartyProgressWindow(bool bBatchMode)
		{
			InitializeComponent();

			// Override stuff for this window
			this.titleLabel.Text = "\n     Update Progress";
			this.infoLabel.Text = "Updating Environment";
			this.infoLabel.Visible = false;

			// Open the window in the background
			if (!bBatchMode)
			{
				EnableButton(cancelButton, false);
				EnableButton(nextButton, false);
				EnableButton(backButton, false);
				//Thread obWindowThread = new Thread(new ThreadStart(ShowWindow));
				//obWindowThread.Start();
			}
		}

		public void InstallPowerShell(string strDestinationToolFolder)
		{
			// Output details
			Console.WriteLine("InstallPowerShell(\""+ strDestinationToolFolder +"\"))");

			if(File.Exists(strDestinationToolFolder +"3rdParty/WindowsXP-KB926139-x86-ENU.exe"))
			{
				// Bingo!  I've got powershell, install it
				rageStatus obStatus;
				rageExecuteCommand obExecuteCommand = new rageExecuteCommand();
				obExecuteCommand.Command = strDestinationToolFolder + "3rdParty/WindowsXP-KB926139-x86-ENU.exe";
				obExecuteCommand.Arguments = "/passive";
				obExecuteCommand.EchoToConsole = true;
				obExecuteCommand.UseBusySpinner = false;
				obExecuteCommand.Execute(out obStatus);
				if (!obStatus.Success())
				{
					Console.WriteLine(obStatus.GetErrorString());
				}
			}
			else
			{
				Console.WriteLine("User wants to install Power Shell, but " + strDestinationToolFolder + "3rdParty/WindowsXP-KB926139-x86-ENU.exe does not exist so skipping");
			}
		}
	}
}

