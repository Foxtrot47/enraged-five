using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data;
using System.IO;
using System.Web.Mail;
using System.Web.UI;
using Microsoft.Win32;
using System.Xml;
using System.Threading;
using System.Text.RegularExpressions;

namespace commitEmailComposer
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Splitter splitter2;
		private System.Windows.Forms.Splitter splitter3;
		private System.Windows.Forms.Splitter splitter4;
		private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.Splitter splitter6;

		private String m_CommitFilePath=null;
		private ArrayList m_GroupBoxes=new ArrayList();
		private System.Windows.Forms.Timer m_HtmlTimer;
		private Hashtable m_GroupToTextBoxes=new Hashtable();
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Splitter splitter7;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.TextBox m_TextBox_MailSubject;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox m_TextBox_MailBcc;
		private System.Windows.Forms.TextBox m_TextBox_MailCc;
		private System.Windows.Forms.TextBox m_TextBox_MailTo;
		
		private bool m_Dirty=false;

		private Hashtable m_GroupToPreviousFile=new Hashtable();
		private String m_CVSCommittingTempFile;

		private string m_FileBlockModified;
		private string m_FileBlockAdded;
		private string m_FileBlockRemoved;
		
		private ArrayList m_FileListModified=new ArrayList();
		private ArrayList m_FileListAdded=new ArrayList();
		private ArrayList m_FileListRemoved=new ArrayList();

		private ArrayList m_FileListModifiedRevision=new ArrayList();
		private ArrayList m_FileListAddedRevision=new ArrayList();
		private ArrayList m_FileListRemovedRevision=new ArrayList();

		private DiffList m_DiffList=new DiffList();

		private int m_ExitCode=0;
		private bool m_GotFileStatus=false;
		private ArrayList m_ListOfFilesString=new ArrayList();
		private string m_QaDirectory;

		#region Values Read From The Configuration File
		private string m_SmtpServer;
		private string m_DomainName;
		private string m_ToList;
		private string m_CcList;
		private string m_BccList;
		private string m_SubjectPrefix;
		private string m_ReleaseFileName = "current-release.html";
		private bool m_CVSTreeHasSeparateRoot;
		#endregion

		private System.Windows.Forms.GroupBox m_GroupBox_PerformanceChanges;
		private System.Windows.Forms.GroupBox m_GroupBox_APIChanges;
		private System.Windows.Forms.GroupBox m_GroupBox_TasksCompleted;
		private System.Windows.Forms.GroupBox m_GroupBox_BugsFixed;
		private System.Windows.Forms.Button m_Button_ClearPreviousLog;
		private System.Windows.Forms.Button m_Button_Cancel;
		private System.Windows.Forms.GroupBox m_GroupBox_Testables;
		private System.Windows.Forms.Splitter splitter8;
		private System.Windows.Forms.GroupBox m_GroupBox_Files;
		private System.Windows.Forms.Button m_Button_CommitDontSend;
		private System.Windows.Forms.Button m_Button_CommitAndSend;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem m_MenuItemDiffList;
        private WebBrowser webBrowser1;
		private System.Windows.Forms.GroupBox m_GroupBox_GeneralComments;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

			m_GroupBoxes.Add(m_GroupBox_GeneralComments);
			m_GroupBoxes.Add(m_GroupBox_Testables);
			m_GroupBoxes.Add(m_GroupBox_BugsFixed);
			m_GroupBoxes.Add(m_GroupBox_TasksCompleted);
			m_GroupBoxes.Add(m_GroupBox_APIChanges);
			m_GroupBoxes.Add(m_GroupBox_PerformanceChanges);
			m_GroupBoxes.Add(m_GroupBox_Files);

			CreateTextBoxes();

			// we do this so that the group box for performance changes has no
			// empty space below it (because of the deactivated group box for files:
			m_GroupBox_PerformanceChanges.Dock=DockStyle.Fill;
			splitter8.Enabled=false;
			splitter8.Visible=false;
		}

		private Thread m_ReadThread;
		private bool Init()
		{
			this.Handle.ToInt32(); //evil voodoo!

			String logname = Environment.GetEnvironmentVariable("LOGNAME");
			if (logname == null) 
			{
				MessageBox.Show("The LOGNAME variable is not defined in your environment.  Please set it appropriately.","No LOGNAME Defined",MessageBoxButtons.OK,MessageBoxIcon.Error);
				ReallyExit();
				return false;
			}
			String editor = Environment.GetEnvironmentVariable("EDITOR");
			if (editor==null || editor=="")
			{
				MessageBox.Show("The EDITOR variable needs to be set to the location of the commitEmailComposerScript.bat.","EDITOR Not Defined",MessageBoxButtons.OK,MessageBoxIcon.Error);
				ReallyExit();
				return false;
			}
			
			if (!editor.EndsWith(".bat"))
			{
				MessageBox.Show("The EDITOR variable should point at commitEmailComposerScript.bat, not commitEmailComposer.exe.","Bad EDITOR Defined",MessageBoxButtons.OK,MessageBoxIcon.Error);
				ReallyExit();
				return false;
			}

			if (LoadConfigFile())
			{
				ReadPreviousMessage();
	
				if (ReadTempCommitFile())
				{
					CreateAndShowPage();				
				
					this.m_HtmlTimer.Enabled = true;
					return true;
				}
			}
	
			return false;
		}

		private bool LoadConfigFile()
		{
			// find the nearest qa\ directory:
			string currentDirectory=Environment.CurrentDirectory;
			m_QaDirectory=null;
			do 
			{
				// see if it's a subdirectory:
				string[] subDirs=Directory.GetDirectories(currentDirectory,"qa");
				Debug.Assert(subDirs.Length<2);
				// found it!
				if (subDirs.Length>0)
				{
					// make sure we have the right qa directory:
					string fileName=subDirs[0] + "\\" + "commit-email-config.xml";
					if (File.Exists(fileName)==true)
					{
						m_QaDirectory=subDirs[0];
						break;
					}
				}

				// go up a if we haven't found the directory:
				DirectoryInfo info=Directory.GetParent(currentDirectory);
				
				// didn't find the directory:
				if (info==null)
				{
					MessageBox.Show("Couldn't find the qa directory in the ancestors of '" + Environment.CurrentDirectory + "'" ,"Couldn't Find Directory",MessageBoxButtons.OK,MessageBoxIcon.Error);
					ReallyExit();
					return false;
				}

				currentDirectory=info.FullName;
				
			} while (m_QaDirectory==null);

			// read the configuration file:
			if (m_QaDirectory!=null)
			{
				
				string fileName=m_QaDirectory + "\\" + "commit-email-config.xml";

				XmlTextReader reader = new XmlTextReader(fileName);

				while (reader.Read())
				{
					if (reader.NodeType == XmlNodeType.Element && reader.NodeType != XmlNodeType.EndElement)
					{
						switch (reader.Name)
						{
							case "Server":
								m_SmtpServer=reader["value"];
								break;

							case "DomainName":
								m_DomainName=reader["value"];
								break;

							case "ToList":
								m_ToList=reader["value"];
								if (m_TextBox_MailTo.Text.IndexOf(m_ToList)<0)
								{
									m_TextBox_MailTo.Text+=";" + m_ToList;
								}
								break;

							case "CcList":
								m_CcList=reader["value"];
								if (m_TextBox_MailCc.Text.IndexOf(m_CcList)<0)
								{
									m_TextBox_MailCc.Text+=";" + m_CcList;
								}
								break;

							case "BccList":
								m_BccList=reader["value"];
								if (m_TextBox_MailBcc.Text.IndexOf(m_BccList)<0)
								{
									m_TextBox_MailBcc.Text+=";" + m_BccList;
								}
								break;
							case "SubjectPrefix":
								m_SubjectPrefix=reader["value"];
								break;

							case "ReleaseFileName":
								m_ReleaseFileName=reader["value"];
								break;

							case "QaDirectory":
								m_QaDirectory=reader["value"];
								break;

							case "CVSTreeHasSeparateRoot":
								m_CVSTreeHasSeparateRoot=Convert.ToBoolean(reader["value"]);
								break;

							case "BadFileTypes":
							{
								string tmp=reader["value"];
								if (tmp!=null)
								{
									tmp=tmp.Replace(" ","");
									tmp=tmp.Replace("\t","");
									tmp=tmp.Replace("\r","");
									tmp=tmp.Replace("\n","");
									m_BadFiles=tmp.Split(new char[]{';'});
								}
							}
								break;

						}
					}

					m_CommitFilePath=m_QaDirectory + @"\." + Environment.GetEnvironmentVariable("LOGNAME") + "commit-email-body.html";
				}
				reader.Close();
			}
	
			return true;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_GroupBox_Files = new System.Windows.Forms.GroupBox();
            this.splitter8 = new System.Windows.Forms.Splitter();
            this.m_GroupBox_PerformanceChanges = new System.Windows.Forms.GroupBox();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.m_GroupBox_APIChanges = new System.Windows.Forms.GroupBox();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.m_GroupBox_TasksCompleted = new System.Windows.Forms.GroupBox();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.m_GroupBox_BugsFixed = new System.Windows.Forms.GroupBox();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.m_GroupBox_Testables = new System.Windows.Forms.GroupBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.m_GroupBox_GeneralComments = new System.Windows.Forms.GroupBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.m_Button_Cancel = new System.Windows.Forms.Button();
            this.m_Button_ClearPreviousLog = new System.Windows.Forms.Button();
            this.m_Button_CommitDontSend = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.m_Button_CommitAndSend = new System.Windows.Forms.Button();
            this.m_TextBox_MailSubject = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_TextBox_MailBcc = new System.Windows.Forms.TextBox();
            this.m_TextBox_MailCc = new System.Windows.Forms.TextBox();
            this.m_TextBox_MailTo = new System.Windows.Forms.TextBox();
            this.m_HtmlTimer = new System.Windows.Forms.Timer(this.components);
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.m_MenuItemDiffList = new System.Windows.Forms.MenuItem();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.m_GroupBox_Files);
            this.panel1.Controls.Add(this.splitter8);
            this.panel1.Controls.Add(this.m_GroupBox_PerformanceChanges);
            this.panel1.Controls.Add(this.splitter6);
            this.panel1.Controls.Add(this.m_GroupBox_APIChanges);
            this.panel1.Controls.Add(this.splitter5);
            this.panel1.Controls.Add(this.m_GroupBox_TasksCompleted);
            this.panel1.Controls.Add(this.splitter4);
            this.panel1.Controls.Add(this.m_GroupBox_BugsFixed);
            this.panel1.Controls.Add(this.splitter3);
            this.panel1.Controls.Add(this.m_GroupBox_Testables);
            this.panel1.Controls.Add(this.splitter2);
            this.panel1.Controls.Add(this.m_GroupBox_GeneralComments);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(432, 741);
            this.panel1.TabIndex = 0;
            // 
            // m_GroupBox_Files
            // 
            this.m_GroupBox_Files.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_GroupBox_Files.Location = new System.Drawing.Point(0, 715);
            this.m_GroupBox_Files.Name = "m_GroupBox_Files";
            this.m_GroupBox_Files.Size = new System.Drawing.Size(432, 26);
            this.m_GroupBox_Files.TabIndex = 11;
            this.m_GroupBox_Files.TabStop = false;
            this.m_GroupBox_Files.Text = "Files";
            // 
            // splitter8
            // 
            this.splitter8.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter8.Location = new System.Drawing.Point(0, 712);
            this.splitter8.Name = "splitter8";
            this.splitter8.Size = new System.Drawing.Size(432, 3);
            this.splitter8.TabIndex = 10;
            this.splitter8.TabStop = false;
            // 
            // m_GroupBox_PerformanceChanges
            // 
            this.m_GroupBox_PerformanceChanges.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_GroupBox_PerformanceChanges.Location = new System.Drawing.Point(0, 627);
            this.m_GroupBox_PerformanceChanges.Name = "m_GroupBox_PerformanceChanges";
            this.m_GroupBox_PerformanceChanges.Size = new System.Drawing.Size(432, 85);
            this.m_GroupBox_PerformanceChanges.TabIndex = 5;
            this.m_GroupBox_PerformanceChanges.TabStop = false;
            this.m_GroupBox_PerformanceChanges.Text = "Performance Changes";
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter6.Location = new System.Drawing.Point(0, 624);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(432, 3);
            this.splitter6.TabIndex = 9;
            this.splitter6.TabStop = false;
            // 
            // m_GroupBox_APIChanges
            // 
            this.m_GroupBox_APIChanges.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_GroupBox_APIChanges.Location = new System.Drawing.Point(0, 491);
            this.m_GroupBox_APIChanges.Name = "m_GroupBox_APIChanges";
            this.m_GroupBox_APIChanges.Size = new System.Drawing.Size(432, 133);
            this.m_GroupBox_APIChanges.TabIndex = 4;
            this.m_GroupBox_APIChanges.TabStop = false;
            this.m_GroupBox_APIChanges.Text = "API Changes";
            // 
            // splitter5
            // 
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter5.Location = new System.Drawing.Point(0, 488);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(432, 3);
            this.splitter5.TabIndex = 7;
            this.splitter5.TabStop = false;
            // 
            // m_GroupBox_TasksCompleted
            // 
            this.m_GroupBox_TasksCompleted.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_GroupBox_TasksCompleted.Location = new System.Drawing.Point(0, 395);
            this.m_GroupBox_TasksCompleted.Name = "m_GroupBox_TasksCompleted";
            this.m_GroupBox_TasksCompleted.Size = new System.Drawing.Size(432, 93);
            this.m_GroupBox_TasksCompleted.TabIndex = 3;
            this.m_GroupBox_TasksCompleted.TabStop = false;
            this.m_GroupBox_TasksCompleted.Text = "Tasks Completed";
            // 
            // splitter4
            // 
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter4.Location = new System.Drawing.Point(0, 392);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(432, 3);
            this.splitter4.TabIndex = 5;
            this.splitter4.TabStop = false;
            // 
            // m_GroupBox_BugsFixed
            // 
            this.m_GroupBox_BugsFixed.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_GroupBox_BugsFixed.Location = new System.Drawing.Point(0, 315);
            this.m_GroupBox_BugsFixed.Name = "m_GroupBox_BugsFixed";
            this.m_GroupBox_BugsFixed.Size = new System.Drawing.Size(432, 77);
            this.m_GroupBox_BugsFixed.TabIndex = 2;
            this.m_GroupBox_BugsFixed.TabStop = false;
            this.m_GroupBox_BugsFixed.Text = "Bugs Fixed";
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 312);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(432, 3);
            this.splitter3.TabIndex = 3;
            this.splitter3.TabStop = false;
            // 
            // m_GroupBox_Testables
            // 
            this.m_GroupBox_Testables.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_GroupBox_Testables.Location = new System.Drawing.Point(0, 187);
            this.m_GroupBox_Testables.Name = "m_GroupBox_Testables";
            this.m_GroupBox_Testables.Size = new System.Drawing.Size(432, 125);
            this.m_GroupBox_Testables.TabIndex = 1;
            this.m_GroupBox_Testables.TabStop = false;
            this.m_GroupBox_Testables.Text = "Testables";
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 184);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(432, 3);
            this.splitter2.TabIndex = 1;
            this.splitter2.TabStop = false;
            // 
            // m_GroupBox_GeneralComments
            // 
            this.m_GroupBox_GeneralComments.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_GroupBox_GeneralComments.Location = new System.Drawing.Point(0, 0);
            this.m_GroupBox_GeneralComments.Name = "m_GroupBox_GeneralComments";
            this.m_GroupBox_GeneralComments.Size = new System.Drawing.Size(432, 184);
            this.m_GroupBox_GeneralComments.TabIndex = 0;
            this.m_GroupBox_GeneralComments.TabStop = false;
            this.m_GroupBox_GeneralComments.Text = "General Comments";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(432, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 741);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox7);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(435, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(597, 741);
            this.panel2.TabIndex = 2;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.panel4);
            this.groupBox7.Controls.Add(this.splitter7);
            this.groupBox7.Controls.Add(this.panel3);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Location = new System.Drawing.Point(0, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(597, 741);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Email Message";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.webBrowser1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 171);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(591, 567);
            this.panel4.TabIndex = 3;
            // 
            // splitter7
            // 
            this.splitter7.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter7.Location = new System.Drawing.Point(3, 168);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(591, 3);
            this.splitter7.TabIndex = 2;
            this.splitter7.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.m_Button_Cancel);
            this.panel3.Controls.Add(this.m_Button_ClearPreviousLog);
            this.panel3.Controls.Add(this.m_Button_CommitDontSend);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.m_Button_CommitAndSend);
            this.panel3.Controls.Add(this.m_TextBox_MailSubject);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.m_TextBox_MailBcc);
            this.panel3.Controls.Add(this.m_TextBox_MailCc);
            this.panel3.Controls.Add(this.m_TextBox_MailTo);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 16);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(591, 152);
            this.panel3.TabIndex = 1;
            // 
            // m_Button_Cancel
            // 
            this.m_Button_Cancel.Image = ((System.Drawing.Image)(resources.GetObject("m_Button_Cancel.Image")));
            this.m_Button_Cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_Button_Cancel.Location = new System.Drawing.Point(487, 120);
            this.m_Button_Cancel.Name = "m_Button_Cancel";
            this.m_Button_Cancel.Size = new System.Drawing.Size(96, 23);
            this.m_Button_Cancel.TabIndex = 8;
            this.m_Button_Cancel.Text = "Cancel";
            this.m_Button_Cancel.Click += new System.EventHandler(this.m_Button_Cancel_Click);
            // 
            // m_Button_ClearPreviousLog
            // 
            this.m_Button_ClearPreviousLog.Enabled = false;
            this.m_Button_ClearPreviousLog.Image = ((System.Drawing.Image)(resources.GetObject("m_Button_ClearPreviousLog.Image")));
            this.m_Button_ClearPreviousLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_Button_ClearPreviousLog.Location = new System.Drawing.Point(330, 120);
            this.m_Button_ClearPreviousLog.Name = "m_Button_ClearPreviousLog";
            this.m_Button_ClearPreviousLog.Size = new System.Drawing.Size(136, 23);
            this.m_Button_ClearPreviousLog.TabIndex = 7;
            this.m_Button_ClearPreviousLog.Text = "Clear Previous Log";
            this.m_Button_ClearPreviousLog.Click += new System.EventHandler(this.m_Button_ClearPreviousLog_Click);
            // 
            // m_Button_CommitDontSend
            // 
            this.m_Button_CommitDontSend.Enabled = false;
            this.m_Button_CommitDontSend.Image = ((System.Drawing.Image)(resources.GetObject("m_Button_CommitDontSend.Image")));
            this.m_Button_CommitDontSend.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_Button_CommitDontSend.Location = new System.Drawing.Point(8, 120);
            this.m_Button_CommitDontSend.Name = "m_Button_CommitDontSend";
            this.m_Button_CommitDontSend.Size = new System.Drawing.Size(152, 23);
            this.m_Button_CommitDontSend.TabIndex = 5;
            this.m_Button_CommitDontSend.Text = "Commit (Don\'t Send)";
            this.m_Button_CommitDontSend.Click += new System.EventHandler(this.Commit_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "To:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m_Button_CommitAndSend
            // 
            this.m_Button_CommitAndSend.Enabled = false;
            this.m_Button_CommitAndSend.Image = ((System.Drawing.Image)(resources.GetObject("m_Button_CommitAndSend.Image")));
            this.m_Button_CommitAndSend.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_Button_CommitAndSend.Location = new System.Drawing.Point(181, 120);
            this.m_Button_CommitAndSend.Name = "m_Button_CommitAndSend";
            this.m_Button_CommitAndSend.Size = new System.Drawing.Size(128, 23);
            this.m_Button_CommitAndSend.TabIndex = 6;
            this.m_Button_CommitAndSend.Text = "Commit And Send";
            this.m_Button_CommitAndSend.Click += new System.EventHandler(this.CommitAndSend_Click);
            // 
            // m_TextBox_MailSubject
            // 
            this.m_TextBox_MailSubject.Location = new System.Drawing.Point(72, 88);
            this.m_TextBox_MailSubject.Name = "m_TextBox_MailSubject";
            this.m_TextBox_MailSubject.Size = new System.Drawing.Size(512, 20);
            this.m_TextBox_MailSubject.TabIndex = 4;
            this.m_TextBox_MailSubject.TextChanged += new System.EventHandler(this.Email_TextChanged);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cc:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Bcc:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Subject:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m_TextBox_MailBcc
            // 
            this.m_TextBox_MailBcc.Location = new System.Drawing.Point(72, 64);
            this.m_TextBox_MailBcc.Name = "m_TextBox_MailBcc";
            this.m_TextBox_MailBcc.Size = new System.Drawing.Size(512, 20);
            this.m_TextBox_MailBcc.TabIndex = 3;
            this.m_TextBox_MailBcc.TextChanged += new System.EventHandler(this.Email_TextChanged);
            // 
            // m_TextBox_MailCc
            // 
            this.m_TextBox_MailCc.Location = new System.Drawing.Point(72, 40);
            this.m_TextBox_MailCc.Name = "m_TextBox_MailCc";
            this.m_TextBox_MailCc.Size = new System.Drawing.Size(512, 20);
            this.m_TextBox_MailCc.TabIndex = 2;
            this.m_TextBox_MailCc.TextChanged += new System.EventHandler(this.Email_TextChanged);
            // 
            // m_TextBox_MailTo
            // 
            this.m_TextBox_MailTo.Location = new System.Drawing.Point(72, 16);
            this.m_TextBox_MailTo.Name = "m_TextBox_MailTo";
            this.m_TextBox_MailTo.Size = new System.Drawing.Size(512, 20);
            this.m_TextBox_MailTo.TabIndex = 1;
            this.m_TextBox_MailTo.TextChanged += new System.EventHandler(this.Email_TextChanged);
            // 
            // m_HtmlTimer
            // 
            this.m_HtmlTimer.Interval = 500;
            this.m_HtmlTimer.Tick += new System.EventHandler(this.m_HtmlTimer_Tick);
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.m_MenuItemDiffList});
            // 
            // m_MenuItemDiffList
            // 
            this.m_MenuItemDiffList.Enabled = false;
            this.m_MenuItemDiffList.Index = 0;
            this.m_MenuItemDiffList.Text = "Diff List...";
            this.m_MenuItemDiffList.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.AllowWebBrowserDrop = false;
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.Size = new System.Drawing.Size(591, 567);
            this.webBrowser1.TabIndex = 0;
            this.webBrowser1.WebBrowserShortcutsEnabled = false;
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1032, 741);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.Text = "Commit Composer";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CommitAndSend_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static int Main(string[] args) 
		{
			bool createdNew;

			Mutex m = new Mutex(true, "commitEmailComposer", out createdNew);

			if (! createdNew)
			{
				// app is already running...
				MessageBox.Show("Only one instance of commitEmailComposer.exe is allowed at a time.");
				return -1;
			}

			Form1 form=new Form1();
			if (args.Length>=1)
			{
				form.m_CVSCommittingTempFile=args[0];

			}

			if (form.Init())
			{
				Application.Run(form);
				// keep the mutex reference alive until the normal termination of the program
				GC.KeepAlive(m);

				return form.m_ExitCode;
			}
			else 
			{
				// keep the mutex reference alive until the normal termination of the program
				GC.KeepAlive(m);

				return -1;
			}
			
		}

		private void CreateTextBoxes()
		{
			int num=1;
			foreach (System.Windows.Forms.GroupBox groupBox in m_GroupBoxes)
			{
				System.Windows.Forms.TextBox textBox=new System.Windows.Forms.TextBox();

				textBox.Dock = System.Windows.Forms.DockStyle.Fill;
				textBox.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
				textBox.Location = new System.Drawing.Point(3, 16);
				textBox.Multiline = true;
				textBox.Name = "textBox" + (num++);
				textBox.Size = new System.Drawing.Size(426, 141);
				textBox.TabIndex = 0;
				textBox.Text = "";	
				textBox.TextChanged +=new EventHandler(textBox_TextChanged);

				if (groupBox.Text=="Files")
				{
					groupBox.Enabled=false;
					groupBox.Visible=false;

				}

				groupBox.Controls.Add(textBox);
				m_GroupToTextBoxes.Add(groupBox,textBox);
			}
		}

		private string GetPrevFileName()
		{
			return Path.GetDirectoryName(m_CommitFilePath) + "\\" +
				Path.GetFileNameWithoutExtension(m_CommitFilePath) + ".prev" + Path.GetExtension(m_CommitFilePath);
		}

		private string GetDiffListPrevFileName()
		{
			return Path.GetDirectoryName(m_CommitFilePath) + "\\" +
				"cvsxdiff-list.prev.txt";
		}

		private void GetListOfFiles(ref string listOfFilesString,ref ArrayList listOfFiles,ref ArrayList listOfRevisions)
		{
			if (listOfFilesString!=null)
			{
				listOfFilesString=listOfFilesString.Replace("\t","");
				listOfFilesString=listOfFilesString.Replace("\r\n","");
				listOfFilesString=listOfFilesString.Trim();
			}

			string[] filesArray=listOfFilesString!=null?listOfFilesString.Split():null;

			if (filesArray!=null)
			{
				foreach (string file in filesArray)
					if (file!= "")
					{
						string fileTrimmed=file.Trim();
						listOfFiles.Add(fileTrimmed);
						string revision;
						GetFileStatus(Environment.CurrentDirectory,fileTrimmed,out revision);
						if (revision!="")
							listOfRevisions.Add(revision);
						else 
							listOfRevisions.Add("UNKNOWN");
					}
			}
		}

		// TODO: add "needs merge" or "conflict" checking
		private void GetFileStatus(string directory,string file,out string revisionStr)
		{
			revisionStr=null;

			// set up process:
			ProcessStartInfo info=new ProcessStartInfo();
			info.FileName="cvs.exe";
			info.WorkingDirectory=directory;
			info.Arguments="status \"" + file + "\" ";
			info.RedirectStandardOutput=true;
			info.UseShellExecute=false;
			info.CreateNoWindow=true;
			

			Process process=Process.Start(info);
				
			// read output:
			StreamReader streamRead=process.StandardOutput;
			while (streamRead.Peek()!=-1)
			{
				string line=streamRead.ReadLine();
				line=line.Trim();
				if (line.StartsWith("Repository revision:"))
				{
					line=line.Replace("\t"," ");
					string[] lineArray=line.Split();

					bool getRevision=false;
					foreach (string str in lineArray)
					{
						if (str.Trim()=="revision:")
						{
							getRevision=true;
						}
						else if (getRevision && str.Length!=0)
						{
							revisionStr=str;
							break;
						}
					}
				}
				else if (line.StartsWith("File:"))
				{
					// Specifically do not check for "Status: File had conflicts on merge"
					// because there's no way to clear that without actually trying to commit.
					if (line.IndexOf("Status: Needs Merge") != -1)
					{
						m_NeedsMerge = true;
						m_FilesNeedingMerge = m_FilesNeedingMerge + " " + file;
					}
				}

				// see if we're done:
				if (revisionStr!=null)
					break;
			}	

			// check for EOL on EOF for the file
			String sourceFile = directory + "\\" + file;
			String ext = Path.GetExtension(file);
			if (ext == ".h" || ext == ".cpp" || ext == ".c")
			{
				try 
				{
					StreamReader checker = new StreamReader(sourceFile);
					String everything = checker.ReadToEnd();
					checker.Close();
					if (!everything.EndsWith("\r\n")) 
					{
						try
						{
							StreamWriter fixer = new StreamWriter(sourceFile,true);
							fixer.Write("\r\n");
							fixer.Close();
						}
						catch
						{
							MessageBox.Show("Unable to repair file with missing EOF at EOL",sourceFile);
						}
					}
				}
				catch
				{
				}
			}
		}

		private string GetCVSDirectoryName(string path)
		{
			path=Path.GetDirectoryName(path);
			
			// read root:
			string rootFileName=path + @"\CVS\Root";
			StreamReader reader=new StreamReader(rootFileName);
			string rootLine=reader.ReadLine();
			string[] rootSplit=rootLine.Split(':');
			if (rootSplit.Length<=0)
			{
				return "UNKNOWN_DIR";
			}
			string rootPath=rootSplit[rootSplit.Length-1];
			reader.Close();

			// read repository:
			string repositoryFileName=path + @"\CVS\Repository";
			reader=new StreamReader(repositoryFileName);
			string repository=reader.ReadLine();
			reader.Close();
	
			// remove the cvs repository to get the correct directory:
			return repository.Replace(rootPath,"") + "/";
		}

		private string GetRevisionString(string revision,int addition)
		{
			string result="";

			// compute numeric revision that will be created when the file is committed:
			string[] splitRevision=revision.Split('.');
			if (splitRevision.Length<=0)
				return "ERROR";

			Int32 revisionNum=Convert.ToInt32(splitRevision[splitRevision.Length-1]);
			revisionNum+=addition;
			splitRevision[splitRevision.Length-1]=revisionNum.ToString();

			for (int i=0;i<splitRevision.Length;i++)
			{
				if (i!=0)
					result+=".";
				result+=splitRevision[i];
			}

			return result;
		}

		private string ComputeRevisionUrl(string path,string revision,int revDelta)
		{
			//string result="<a href=\"http://cvs/viewcvs/viewcvs.cgi/";
			string resultFull="<a href=\"http://viewcvs.rockstarsd.com/full/";
			string resultSeparateRoot="<a href=\"http://viewcvs.rockstarsd.com/";
			string result;
			string viewcvs_cgi=@"viewcvs.cgi";

			string dirName=Path.GetDirectoryName(path);
			dirName=dirName.Replace(@"\","/");
			if (m_CVSTreeHasSeparateRoot)
			{
				result=resultSeparateRoot;
				string[] split=dirName.Split('/');
				int i=0;
				while (split[i]=="")
				{
					i++;
				}
				result+=split[i++] + "/";
				result+=viewcvs_cgi + "/";
				for (;i<split.Length;i++)
				{
					result+=split[i] + "/";
				}
			}
			else
			{
				result=resultFull;
				result+=viewcvs_cgi + "/";
				result+=dirName;
			}
			result += "/";
			string fileName=Path.GetFileName(path);
			result+=fileName + ".diff?";
	
			result+="r1=text&tr1=";
	
			result+=GetRevisionString(revision,revDelta);

			result+="&";

			result+="r2=text&tr2=" + revision;
			
			result+="&";

			result+="diff_format=h";

			result+="\">";

			return result;
		}

		private void AddFiles(ref ArrayList listOfFiles,ref ArrayList listOfFileRevisions,string header,bool addDiffUrl)
		{
			if (listOfFiles.Count<=0)
				return;

			// find the longest string:
			int maxLength=-1;
			foreach (string str in listOfFiles)
			{
				if (str.Length>maxLength)
					maxLength=str.Length;
			}
	
			m_ListOfFilesString.Add(header);
			for (int i=0;i<listOfFiles.Count;i++)
			{
				string file=listOfFiles[i] as string;
				if (file != "")
				{
					string lineString = "&nbsp&nbsp "+ file;

					// add spaces to line up text:
					for (int j=0;j<(maxLength+2)-file.Length;j++)
					{
						lineString += "&nbsp";
					}

					if (addDiffUrl)
					{
						string oldVersion=listOfFileRevisions[i] as string;
						if (oldVersion!=null)
						{
							string newVersion=GetRevisionString(oldVersion,1);
		
							// compare url:
							lineString += ComputeRevisionUrl(GetCVSDirectoryName(Environment.CurrentDirectory + "\\" + file) + "\\" + Path.GetFileName(file), 
								newVersion,-1) + oldVersion + " -> " + newVersion + "</a>";
						}
					
						lineString +="<BR>";
					}

					m_ListOfFilesString.Add(lineString);

				}
			}
			m_ListOfFilesString.Add("<BR>");
		}

		private bool m_HadBadFiles = false;

		private bool m_NeedsMerge = false;

		private string [] m_BadFiles;

		private string m_BadFilePretty;

		private string m_FilesNeedingMerge;

		private string m_CommitTag;

		private void ConfigureBadFileList()
		{
			if (m_BadFiles!=null)
			{
				// KELLEY MAKE THIS DATA-DRIVEN
				//m_BadFiles = new String[3]{"_parser.h",".suo",".ncb"};
	
				// ...but leave this alone
				for (int i=0;i<m_BadFiles.Length; i++)
				{
					m_BadFilePretty += "*";
					m_BadFilePretty += m_BadFiles[i];
					if (i == m_BadFiles.Length-2)
						m_BadFilePretty += ", or ";
					else if (i < m_BadFiles.Length-2)
						m_BadFilePretty += ", ";
				}
			}
		}

		private void CheckForBadFiles(String block)
		{
			if (m_BadFiles!=null)
			{
				for (int i=0;i<m_BadFiles.Length; i++)
					if (block.IndexOf(m_BadFiles[i]) != -1)
						m_HadBadFiles = true;
			}
		}

		private bool ReadTempCommitFile()
		{
			if (m_CVSCommittingTempFile!=null)
			{
				ConfigureBadFileList();

				// open file:
				StreamReader reader=null;
				try 
				{
					reader=File.OpenText(m_CVSCommittingTempFile);
				}
				catch
				{
					MessageBox.Show("Failed to open cvs temp file '" + m_CommitFilePath + "'" ,"Couldn't Open File",MessageBoxButtons.OK,MessageBoxIcon.Error);
					ReallyExit();
					return false;
				}

				// read all lines:
			

				bool readModified=false;
				bool readAdded=false;
				bool readRemoved=false;

				string currentBlock=null;

				// find files in commit message:
				while (reader.Peek()!=-1)
				{
					string line=reader.ReadLine();
					// skip tag line:
					if (line.Trim().StartsWith("CVS:  Tag:")) 
					{
						m_CommitTag = line.Trim().Substring(11);
						DialogResult resultDiag=MessageBox.Show("You're trying to commit code on a tag - are you sure you want to commit this code?",
							"[TAG: " + m_CommitTag + "]",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
						switch(resultDiag) 
						{
							case DialogResult.Yes:
								continue;
							case DialogResult.No:
								ReallyExit();
								return false;
						}
					}
					else if (line.Trim().ToLower().StartsWith("CVS: no tag"))
					{
						continue;
					}
					else if (line.StartsWith("CVS: Modified Files:"))
					{
						if (readAdded)
						{
							m_FileBlockAdded=currentBlock;
							CheckForBadFiles(m_FileBlockAdded);
							readAdded=false;
						}
						else if (readRemoved)
						{
							m_FileBlockRemoved=currentBlock;
							readRemoved=false;					
						}
						else if (readModified)
						{
							m_FileBlockModified=currentBlock;
							readModified=false;
						}
						readModified=true;
						currentBlock=null;
					}
					else if (line.StartsWith("CVS: Added Files:"))
					{
						if (readAdded)
						{
							m_FileBlockAdded=currentBlock;
							CheckForBadFiles(m_FileBlockAdded);
							readAdded=false;
						}
						else if (readRemoved)
						{
							m_FileBlockRemoved=currentBlock;
							readRemoved=false;					
						}
						else if (readModified)
						{
							m_FileBlockModified=currentBlock;
						}
						readAdded=true;
						currentBlock=null;
					}
					else if (line.StartsWith("CVS: Removed Files:"))
					{
						if (readAdded)
						{
							m_FileBlockAdded=currentBlock;
							CheckForBadFiles(m_FileBlockAdded);
							readAdded=false;
						}
						else if (readRemoved)
						{
							m_FileBlockRemoved=currentBlock;
							readRemoved=false;					
						}
						else if (readModified)
						{
							m_FileBlockModified=currentBlock;
							readModified=false;
						}
						readRemoved=true;
						currentBlock=null;
					}
					else if (line.StartsWith("CVS: ------------"))
					{
						if (readAdded)
						{
							m_FileBlockAdded=currentBlock;
							CheckForBadFiles(m_FileBlockAdded);
							readAdded=false;
						}
						else if (readRemoved)
						{
							m_FileBlockRemoved=currentBlock;
							readRemoved=false;					
						}
						else if (readModified)
						{
							m_FileBlockModified=currentBlock;
							readModified=false;
						}
						currentBlock=null;
					}
					else if (readAdded || readRemoved || readModified)
					{
						
						currentBlock+= " " + line.Replace("CVS:","");
					}
				}

				reader.Close();

				// start the thread:
				m_ReadThread = new Thread(new ThreadStart(GetStatusProc));
				m_ReadThread.Name = "Read Temp Commit File Thread";
				m_ReadThread.Start();	
			}
			else 
				GotFileStatus();

			return true;
		}

		void GetStatusProc()
		{
			m_ListOfFilesString.Clear();
			m_ListOfFilesString.Add("<BR><B>" + Environment.CurrentDirectory + "</B><BR><BR>");

			GetListOfFiles(ref m_FileBlockModified,ref m_FileListModified,ref m_FileListModifiedRevision);
			GetListOfFiles(ref m_FileBlockAdded,ref m_FileListAdded,ref m_FileListAddedRevision);
			GetListOfFiles(ref m_FileBlockRemoved,ref m_FileListRemoved,ref m_FileListRemovedRevision);

			this.Invoke(new MethodInvoker(FinishGetStatus));
		}

		private void FinishGetStatus()
		{
			// figure out the subject line:
			ArrayList subjectNames=new ArrayList();
			ArrayList subjectNumItems=new ArrayList();

			//Debugger.Break();

			CollectSubdirNames(m_FileListModified,ref subjectNames,ref subjectNumItems);
			CollectSubdirNames(m_FileListAdded,ref subjectNames,ref subjectNumItems);
			CollectSubdirNames(m_FileListRemoved,ref subjectNames,ref subjectNumItems);

			object[] subjectNumItemsArray=subjectNumItems.ToArray();
			object[] subjectNamesArray=subjectNames.ToArray();

			Array.Sort(subjectNumItemsArray,subjectNamesArray);
			m_TextBox_MailSubject.Text="";
			for (int i=subjectNamesArray.Length-1;i>=0 && i>=subjectNamesArray.Length-5;i--)
			{
				m_TextBox_MailSubject.Text+=subjectNamesArray[i];
				if (i>0 && i>subjectNamesArray.Length-5)
				{
					m_TextBox_MailSubject.Text+=", ";
				}
			}

			if (m_CommitTag != null)
				m_TextBox_MailSubject.Text += " [TAG: " + m_CommitTag + "]";
		
			AddFiles(ref m_FileListModified,ref m_FileListModifiedRevision,"<B>Modified Files:</B><BR>",true);
			AddFiles(ref m_FileListAdded,ref m_FileListAddedRevision,"<B>Added Files:</B><BR>",false);
			AddFiles(ref m_FileListRemoved,ref m_FileListRemovedRevision,"<B>Removed Files:</B><BR>",false);

			// add files to a diff list:
			//m_DiffList.LoadDiffList(GetDiffListPrevFileName());
			m_DiffList.AddFiles(m_FileListModified,Environment.CurrentDirectory);

			// put it at the bottom of the previous files list:
			ArrayList previousFilesStrings=m_GroupToPreviousFile[m_GroupBox_Files] as ArrayList;
			if (previousFilesStrings==null)
				m_GroupToPreviousFile[m_GroupBox_Files]=m_ListOfFilesString;
			else
			{
				previousFilesStrings.AddRange(m_ListOfFilesString);
			}
		   
			GotFileStatus();
			m_Dirty=true;
		}

		private void GotFileStatus()
		{
			if (m_CVSCommittingTempFile!=null)
				m_Button_CommitDontSend.Enabled=true;
			else
			{
				m_Button_CommitDontSend.Enabled=false;
				m_Button_CommitAndSend.Text="Send Email";
			}
			m_Button_CommitAndSend.Enabled=true;
			m_Button_ClearPreviousLog.Enabled=true;
			m_MenuItemDiffList.Enabled=true;
			m_GotFileStatus=true;
			if (m_HadBadFiles)
			{
				m_Button_CommitDontSend.Enabled=false;
				m_Button_CommitAndSend.Enabled=false;
				MessageBox.Show("Detected invalid files (one of "+m_BadFilePretty+
					"), please remove them!\n\nMake sure you should 'a' (for abort) after quitting this application.",
					"Intruder Alert!",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
			else if (m_NeedsMerge)
			{
				m_Button_CommitDontSend.Enabled=false;
				m_Button_CommitAndSend.Enabled=false;
				MessageBox.Show("Detected files needing merge ("+m_FilesNeedingMerge+
					" ), please fix them!\n\nMake sure you should 'a' (for abort) after quitting this application.",
					"Files Needing Merge!",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}

		private void CollectSubdirNames(ArrayList fileList,ref ArrayList subjectNames,ref ArrayList subjectNumItems)
		{
			foreach (string file in fileList)
			{
				string fullPath=Environment.CurrentDirectory + "\\" + file;
				fullPath=fullPath.Replace("/","\\");
				fullPath=fullPath.Replace("\\\\","\\");

				// find the last "\":
				int indexOf=0;
				int prevIndexOf=0;
				int prevPrevIndexOf=-1;
				do 
				{
					indexOf=fullPath.IndexOf("\\",indexOf+1);
					if (indexOf<0)
					{
						break;
					}
					else
					{
						prevPrevIndexOf=prevIndexOf;
						prevIndexOf=indexOf;
					}
				} while(true);

				// find subpath:
				if (prevPrevIndexOf!=-1)
				{
					string pathName=Path.GetDirectoryName(fullPath.Substring(prevPrevIndexOf+1));
					int indexOfSlash=subjectNames.IndexOf(pathName);
					if (indexOfSlash<0)
					{
						subjectNames.Add(pathName);
						subjectNumItems.Add(1);
					}
					else 
					{
						subjectNumItems[indexOfSlash]=((int)subjectNumItems[indexOfSlash])+1;
					}

				}
			}
		}

		private void ReadPreviousMessage()
		{
			string fileName=GetPrevFileName();
			StreamReader streamReader=null;
			if (File.Exists(fileName))
			{
				// try opening the file:
				try
				{
					streamReader=File.OpenText(fileName);
				}
				catch
				{
				}

				if (streamReader!=null)
				{
					ArrayList stringBlock=null;
					System.Windows.Forms.GroupBox groupBox=null;
					while (streamReader.Peek()!=-1)
					{
						string line=streamReader.ReadLine();
						string lineTrimmed=line.Trim();
						if (lineTrimmed.StartsWith("<!--BEGIN_BLOCK:"))
						{
							foreach (System.Windows.Forms.GroupBox groupBoxIter in m_GroupBoxes)
							{
								String groupName=groupBoxIter.Text;
								groupName=groupName.ToUpper();
								if (lineTrimmed=="<!--BEGIN_BLOCK:" + groupName + "-->")
								{
									groupBox=groupBoxIter;
									stringBlock=null;
									break;
								}
								else 
									groupBox=null;
							}
						}
						else if (groupBox!=null && lineTrimmed.Trim()=="<!--END_BLOCK:" + groupBox.Text.ToUpper() + "-->")
						{
							if (stringBlock!=null)
								Console.WriteLine("' : End");
							stringBlock=null;
							groupBox=null;
							continue; 
						}
						else if (groupBox!=null)
						{
							if (line.Trim()!="")
							{
								if (stringBlock==null)
								{
									Console.Write("Begin" + groupBox.Text.ToUpper() + ": '" );
									stringBlock=new ArrayList();
									m_GroupToPreviousFile.Add(groupBox,stringBlock);
									
								}
				
								Console.WriteLine(line);
								stringBlock.Add(line);
							}
						}
					}

				}
				streamReader.Close();
			}
		}

	private void CreateAndShowPage()
	{
		CreateWebPage();
		try 
		{
            using (StreamReader sr = new StreamReader(m_CommitFilePath))
            {
                string data = sr.ReadToEnd();
                webBrowser1.DocumentText = data;
            }
		}
		catch
		{
			m_Dirty=true;
		}
	}

	private void WriteSection(HtmlTextWriter writer,string title,string text,ArrayList previousText)
	{
		// make it upper case so it shows better in plain text:
		title=title.ToUpper();

		if (text.Trim()=="" && previousText==null)
			return;

		// change < and > into &lt; and &lt; unless they're part of
		// <code>, </code>, <i>, </i>, <b>, </b>, etc.
		text = Regex.Replace(text, @"<(?!/?(a(\s+(href|HREF).*)?|A(\s+(href|HREF).*)?|code|CODE|i|I|b|B|tt|TT|sub|SUB|sup|SUP)>)", "&lt;");
		text = Regex.Replace(text, @"(?<!</?(a(\s+(href|HREF).*)?|A(\s+(href|HREF).*)?|code|CODE|i|I|b|B|tt|TT|sub|SUB|sup|SUP))>", "&gt;");
	
		// write header:
		writer.WriteFullBeginTag("h5");
		writer.Write(title);
		writer.WriteEndTag("h5");
		writer.WriteLine();

		writer.WriteLine("<!--BEGIN_BLOCK:" + title + "-->");
		
		writer.WriteFullBeginTag("p");
		if (previousText!=null)
		{
			foreach (string line in previousText)
			{
				string lineActual=line.Replace("<p>","");
				lineActual=lineActual.Replace("</p>","<br>");
				writer.Write(lineActual);
				writer.WriteLine();
			}
		}
		
		text=text.Replace("\r\n","<br>");
		writer.Write(text);
		writer.WriteEndTag("p");
		writer.WriteLine();	

		writer.WriteLine("<!--END_BLOCK:" + title + "-->");
		writer.WriteLine();		
	}

	private string AddDomains(string addresses)
	{
		addresses=addresses.Trim(' ');
		string[] split=addresses.Split(';');
		addresses="";
		for (int i=0;i<split.Length;i++)
		{
			if (split[i]!="")
			{
				if (split[i].IndexOf('@')<0)
					split[i]+="@rockstarsandiego.com";
				addresses+=split[i];
				addresses+=";";
			}
		}

		return addresses;
	}

		#region Outlook Integration
#if CRAP
		private bool m_AllowToClose = true;
		private void SendEmailThroughOutlook()
		{
			m_AllowToClose=false;

			string body=null;
			// open the email body contents
			TextReader textReader;
			try
			{
				textReader=File.OpenText(m_CommitFilePath);
			}
			catch
			{
				MessageBox.Show("Failed to open file '" + m_CommitFilePath + "' - email not sent" ,"Couldn't Open File",MessageBoxButtons.OK,MessageBoxIcon.Error);
				return;
			}

			body = textReader.ReadToEnd();
			textReader.Close();

			//Initialize the envelope values.
			string strTo = m_TextBox_MailTo.Text;
			string strBCC = m_TextBox_MailBcc.Text;
			string strCC = m_TextBox_MailCc.Text;
			//string strSubject = "rage-commit:: " + m_TextBox_MailSubject.Text;
			string strSubject = m_SubjectPrefix + m_TextBox_MailSubject.Text;

			//Automate the Word document.
			wApp = new Word.Application();
			wApp.Visible = false;
			object template = System.Reflection.Missing.Value;
			object newTemplate = System.Reflection.Missing.Value;
			object documentType = Word.WdNewDocumentType.wdNewEmailMessage;
			object visible = false;
			wApp.Visible = false;
			Word._Document doc = wApp.Documents.Add(
				ref template,
				ref newTemplate,
				ref documentType,
				ref visible);

			//Automate the Outlook mail item.
			Outlook.MailItemClass mItem = (Outlook.MailItemClass)doc.MailEnvelope.Item;
			mItem.To = strTo;
			mItem.BCC = strBCC;
			mItem.CC = strCC;
			mItem.Subject = strSubject;
			mItem.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;
			mItem.HTMLBody = body;

			mItem.ItemEvents_Event_Close += new Outlook.ItemEvents_CloseEventHandler(this.wApp_Close);

			wApp.Visible = true;

			// Loop until there are no more references to release.
			while (Marshal.ReleaseComObject(mItem) > 0);
			mItem = null;

			// Invoke the .NET garbage collector.
			GC.Collect();
			GC.WaitForPendingFinalizers();
		}

		// Close the Word application after the message has been sent.
		private void wApp_Close(ref bool e)
		{
			object oMissing = System.Reflection.Missing.Value;
			wApp.Quit(ref oMissing,ref oMissing,ref oMissing);
			m_AllowToClose=true;
		}

		private Word.Application wApp;
		public class MyApi
		{
			[DllImport("user32.dll")]
			public static extern int FindWindow(string strclassName, string strWindowName);
		};
#endif
		#endregion

	private bool SendEmail()
	{
        System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient(m_SmtpServer);

        // added DOMAIN_NAME if the email doesn't have a domain name:
        string to = AddDomains(m_TextBox_MailTo.Text);
        to = to.Replace(';', ',');

        string from = Environment.GetEnvironmentVariable("LOGNAME") + "@" + m_DomainName;

        string cc = AddDomains(m_TextBox_MailCc.Text);
        cc = cc.Replace(';', ',');

        string bcc = AddDomains(m_TextBox_MailBcc.Text);
        bcc = bcc.Replace(';', ',');

        System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(from, to);
        mm.IsBodyHtml = true;

		if (cc != String.Empty)
	        mm.CC.Add(cc);
	
		if (bcc != String.Empty)
	        mm.Bcc.Add(bcc);
	
		mm.Subject = m_SubjectPrefix + m_TextBox_MailSubject.Text;

		// open the email body contents
		TextReader textReader;
		try
		{
			textReader=File.OpenText(m_CommitFilePath);
		}
		catch
		{
			MessageBox.Show("Failed to open file '" + m_CommitFilePath + "' - email not sent" ,"Couldn't Open File",MessageBoxButtons.OK,MessageBoxIcon.Error);
			return false;
		}

		mm.Body = textReader.ReadToEnd();
		textReader.Close();

		TextWriter textWriter;
		try
		{
			String commitlog = System.Environment.GetEnvironmentVariable("TEMP") + "\\commitlog.html";
			textWriter=File.AppendText(commitlog);
			textWriter.Write(mm.Body);
			textWriter.Close();
		}
		catch
		{
			// silently fail for now.
		}

		try 
		{
            mailClient.Send(mm);
		}
		catch (Exception e)
		{
			DialogResult result=MessageBox.Show("Send Mail error: " + e.Message + "\n\nPress 'Ok' to see the exception and then retry send\nPress 'Cancel' to retry send.","Mail Send Error",MessageBoxButtons.OKCancel,MessageBoxIcon.Error);
			if (result==DialogResult.OK)
			{
				int i=1;
				while( e.InnerException != null )
				{
					e = e.InnerException;
					DialogResult result2=MessageBox.Show(e.ToString(),"Inner Exception #" + i,MessageBoxButtons.OKCancel,MessageBoxIcon.Error);
					if (result2==DialogResult.Cancel)
						break;
					i++;
				}
			}
			return false;
		}

		return true;
	}

		private void CreateWebPage()
		{
			Console.WriteLine("creating web page...");
			TextWriter textWriter;
			try
			{
				textWriter=File.CreateText(m_CommitFilePath);
			}
			catch
			{
				MessageBox.Show("Failed to open file '" + m_CommitFilePath + "'" ,"Couldn't Open File",MessageBoxButtons.OK,MessageBoxIcon.Error);
				m_ExitCode=-1;
				return;
			}

			HtmlTextWriter writer=new HtmlTextWriter(textWriter);
			WriteWebPage(writer,true);
		}

		private void WriteWebPage(HtmlTextWriter writer,bool writeHeader)
		{
			if (writeHeader)
			{
				writer.WriteFullBeginTag("html");
				writer.WriteLine();

				writer.WriteFullBeginTag("head");
				writer.WriteLine();
				writer.WriteBeginTag("style");
				writer.WriteAttribute("Type","text/css");
				writer.Write(HtmlTextWriter.TagRightChar);
				writer.WriteLine();
				
				writer.WriteLine("body {");	
				writer.WriteLine("font-family: tahoma;");
				writer.WriteLine("font-size: 10pt;");
				writer.WriteLine("}");

				writer.WriteLine("address {");	
				writer.WriteLine("border-top: thin inset gray;");
				writer.WriteLine("border-bottom: thin inset gray;");
				writer.WriteLine("}");

				writer.WriteLine("p {");	
				//writer.WriteLine("border-bottom: thin inset gray;");
				writer.WriteLine("margin: 5px; border: 1px solid gray; padding: 10px;");
				writer.WriteLine("}");

				//writer.WriteLine("h3 {");	
				//writer.WriteLine("border-top: inset thin gray;");
				//writer.WriteLine("}");

				writer.WriteEndTag("style");
				writer.WriteEndTag("head");
				
				writer.WriteFullBeginTag("body");
			}
			writer.WriteLine();
		
			// write the email sections:
			foreach (System.Windows.Forms.GroupBox box in m_GroupBoxes)
			{
				System.Windows.Forms.TextBox textBox=m_GroupToTextBoxes[box] as System.Windows.Forms.TextBox;
				ArrayList previousFiles=m_GroupToPreviousFile[box] as ArrayList;
				if (box==m_GroupBox_Files && m_GotFileStatus==false)
				{
					ArrayList tempPreviousFiles=new ArrayList();
					tempPreviousFiles.Add("<i><b>reading file status...</b></i>");
					if (previousFiles!=null)
						tempPreviousFiles.AddRange(previousFiles);
					WriteSection(writer,box.Text,textBox.Text,tempPreviousFiles);
				}
				else 
					WriteSection(writer,box.Text,textBox.Text,previousFiles);

			}

			writer.WriteLine("<br>");

			if (writeHeader)
			{
			
				writer.WriteFullBeginTag("address");
				writer.WriteLine();
				writer.WriteLine("generated by " + Environment.GetEnvironmentVariable("LOGNAME") + " at " + DateTime.Now.ToString());
				writer.WriteEndTag("address");


				writer.WriteEndTag("body");
				writer.WriteLine();

				writer.WriteEndTag("html");
			}

			writer.Close();
		
			Console.WriteLine("done creating web page");
		}

		private void textBox_TextChanged(object sender, EventArgs e)
		{
			m_Dirty=true;
			// reset the timer:
			m_HtmlTimer.Stop();
			m_HtmlTimer.Start();
		}

		private void m_HtmlTimer_Tick(object sender, System.EventArgs e)
		{
			if (m_Dirty)
			{
				m_Dirty=false;
				Invoke(new MethodInvoker(CreateAndShowPage));
			
			}
		}

		private void Email_TextChanged(object sender, System.EventArgs e)
		{
		}

		private bool AppendToReleaseFile()
		{
			string exePath=Path.GetDirectoryName(Application.ExecutablePath);
			if (exePath!=null && exePath.Length>0)
			{
				string releaseFile=m_QaDirectory + "\\" + m_ReleaseFileName;
				string releaseFileTemp=releaseFile + ".temp";

				// open release file and append our notes:
				StreamWriter writer=null;
				bool addHeader=File.Exists(releaseFileTemp)==false;
				try
				{
					writer=new StreamWriter(releaseFileTemp,false);
				}
				catch
				{
					MessageBox.Show("Couldn't open the release file '" + releaseFileTemp + "'!",
						"Couldn't Open CVS Release File",MessageBoxButtons.OK,MessageBoxIcon.Error);
					return false;
				}

				WriteWebPage(new HtmlTextWriter(writer),addHeader);
				writer.Close();

			
				string batchScriptName=Environment.CurrentDirectory + "\\" + Environment.GetEnvironmentVariable("LOGNAME") + ".commit-release-file.bat";
				// open script that the outside client can run in order to commit changes:
				try
				{
					writer=new StreamWriter(batchScriptName,false);
				}
				catch
				{
					MessageBox.Show("Couldn't open batch script for committing the release file '" + m_CVSCommittingTempFile + "'!",
						"Couldn't Open CVS Log File",MessageBoxButtons.OK,MessageBoxIcon.Error);
					return false;
				}

				writer.WriteLine("pushd " + Path.GetDirectoryName(releaseFile));
				writer.WriteLine("cvs update -A " + Path.GetFileName(releaseFile));
				writer.WriteLine("type " + " " + releaseFileTemp + " >> " + releaseFile);
				writer.WriteLine("cvs commit -m \"\" " + Path.GetFileName(releaseFile));
				writer.WriteLine("popd");
				writer.Close();
				
				return true;
			}

			return false;
		}

		static String WrapText(String input,int lineLength)
		{
			String result = "";
			int offset = 0;
			while (offset + lineLength < input.Length) 
			{
				int scanner = offset + lineLength;
				int lineBreakOffset = -1;
				for (int i=offset; i<scanner-1; i++)
				{
					if (input[i]=='\r' && input[i+1]=='\n') 
					{
						lineBreakOffset = i;
						break;
					}
				}
				if (lineBreakOffset != -1)
				{
					result += input.Substring(offset,lineBreakOffset + 2 - offset);
					offset = lineBreakOffset + 2;
				}
				else
				{
					while (input[scanner] != 32 && scanner > offset+10)
						--scanner;
					result += input.Substring(offset,scanner - offset) + "\r\n";
					offset = scanner + 1;
				}
			}
			result += input.Substring(offset);

			// Debug code:
			/* for (int i=0; i<lineLength; i++)
				Console.Write("=");
			Console.WriteLine();
			Console.WriteLine(result); */

			return result;
		}

		private bool ModifyCVSLog()
		{
			// Do some sanity checks on the text wrapper.
			/* String test1 = WrapText("This is a basic test",40);
			String test2 = WrapText("This is a much\r\nlonger test that will hopefully do something useful enough to warrant the headache of learning a new language just to fix a problem.",30);
			String test3 = WrapText("This is a much longer test that will hopefully do something useful enough to warrant the headache of learning a new language just to fix a problem.",29);
			String test4 = WrapText("This is a much longer test that will hopefully do something useful enough to warrant the headache of learning a new language just to fix a problem.",28);
			String test5 = WrapText("This is a much longer test that will hopefully do something useful enough to warrant the headache of learning a new language just to fix a problem.",27);
			String test6 = WrapText("This is a much longer test that will hopefully do something useful enough to warrant the headache of learning a new language just to fix a problem.",26);
			String test7 = WrapText("This is a much longer test that will hopefully do something useful enough to warrant the headache of learning a new language just to fix a problem.",25); */

			// read current file contents:
			string currentFileContents=null;
			if (m_CVSCommittingTempFile!=null && File.Exists(m_CVSCommittingTempFile))
			{
				StreamReader reader;
				try 
				{
					reader=File.OpenText(m_CVSCommittingTempFile);
				}
				catch
				{
					MessageBox.Show("Couldn't open the temporary CVS log file '" + m_CVSCommittingTempFile + "'!",
						"Couldn't Open CVS Log File",MessageBoxButtons.OK,MessageBoxIcon.Error);
					return false;
				}

				currentFileContents=reader.ReadToEnd();
				reader.Close();
			}


			if (m_CVSCommittingTempFile!=null && File.Exists(m_CVSCommittingTempFile))
			{
				StreamWriter writer;
				try 
				{
					File.Delete(m_CVSCommittingTempFile);
					writer=File.CreateText(m_CVSCommittingTempFile);
				}
				catch
				{
					MessageBox.Show("Couldn't modify the temporary CVS log file '" + m_CVSCommittingTempFile + "'!",
						"Couldn't Modify CVS Log File",MessageBoxButtons.OK,MessageBoxIcon.Error);
					m_ExitCode=-1;
					return false;
				}
				
				System.Windows.Forms.TextBox textBox=m_GroupToTextBoxes[m_GroupBox_GeneralComments] as System.Windows.Forms.TextBox;
				
				String wrappedText = WrapText(textBox.Text, 76);
				writer.Write(wrappedText);
				writer.WriteLine();
				writer.Write(currentFileContents);
				writer.Close();
				return true;
			}

			// return true so that commit will work
			return true;
		}

		private void CommitAndSend_Click(object sender, System.EventArgs e)
		{
			CommitAndSend();
		}

		private DialogResult SendEmailFailureMessageBox()
		{
			return MessageBox.Show("Sending email failed!\r\nWould you like to retry send (I'll wait 5 seconds and then I try again)?\r\nif retries fail, the smtp server may be down. \r\n(Click \"No\" to save your message, and run the CommitEmailComposer by itself in this directory to send your commit email when you know that the SMTP server is working.)",
								   "Send email failed",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
		}

		private void ReallyExit()
		{
			// wait for outlook to exit:
			Application.Exit();

			// make really sure we absolutely exit:
			Environment.Exit(0);
		}

		private void CommitAndSend()
		{
			CreateWebPage();

			// make sure they have a subject for the email:
			if (m_TextBox_MailSubject.Text==null || m_TextBox_MailSubject.Text=="")
			{
				DialogResult resultDiag=MessageBox.Show("Your email doesn't have a subject line - do you want to add one?",
					"No Subject Line",MessageBoxButtons.YesNoCancel,MessageBoxIcon.Warning);
				switch(resultDiag) {
				case DialogResult.Yes:
					m_TextBox_MailSubject.Focus();
					return;
				case DialogResult.No:
					break;
				case DialogResult.Cancel:
					return;
				}
			}

			bool result=ModifyCVSLog();
		
			if (result==true)
			{
				result=SendEmail();
				if (!result)
				{

					DialogResult resultDiag = SendEmailFailureMessageBox();
					// keep looping until we're successful:
					while (resultDiag==DialogResult.Yes && result==false)
					{
						Thread.Sleep(5000);
						result=SendEmail();
						
						if (!result)
							resultDiag = SendEmailFailureMessageBox();
					}
				}

				if (result)
				{
					AppendToReleaseFile();
					//SendEmailThroughOutlook();
					File.Delete(GetPrevFileName());
					File.Delete(GetDiffListPrevFileName());

				File.Delete(m_CommitFilePath);
				}
				else
				{
					string fileNameTo=GetPrevFileName();
					try
					{
						if (File.Exists(fileNameTo))
							File.Delete(fileNameTo);
						File.Move(m_CommitFilePath,fileNameTo);
					}
					catch
					{
					}
				}

				Thread.Sleep(2000);

				ReallyExit();
			}
		}

		private void Commit_Click(object sender, System.EventArgs e)
		{
			bool result=ModifyCVSLog();

			if (result==true)
			{
				string fileNameTo=GetPrevFileName();
				try 
				{
					if (File.Exists(fileNameTo))
						File.Delete(fileNameTo);
					File.Move(m_CommitFilePath,fileNameTo);
				}
				catch 
				{
			
					MessageBox.Show("Couldn't move '" + m_CommitFilePath + "' to '" + fileNameTo,
						"Couldn't Move File",MessageBoxButtons.OK,MessageBoxIcon.Warning);
				}


				//m_DiffList.SaveDiffList(GetDiffListPrevFileName());

				Thread.Sleep(1000);
				ReallyExit();
			}
		}

		private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (m_ReadThread!=null)
				m_ReadThread.Abort();
			File.Delete(m_CommitFilePath);
		}

		private void m_Button_ClearPreviousLog_Click(object sender, System.EventArgs e)
		{
			DialogResult result=MessageBox.Show("Are you sure you want to clear the previous log text?\n\n" +
												"(This will clear everything from the email, including the \"FILES\" section.)","Clear Previous Log Text?",MessageBoxButtons.YesNo);
			if (result!=DialogResult.Yes)
			{
				return;
			}

			// clear out previous file text:
			foreach (GroupBox groupBox in m_GroupBoxes)
			{
				m_GroupToPreviousFile[groupBox]=null;
			}

			m_GroupToPreviousFile[m_GroupBox_Files]=m_ListOfFilesString;
			
			File.Delete(GetPrevFileName());
			CreateAndShowPage();
		}

		private void m_Button_Cancel_Click(object sender, System.EventArgs e)
		{
			if (m_ReadThread!=null)
				m_ReadThread.Abort();
			ReallyExit();
		}

		private void CommitAndSend_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return && e.Control)
			{
				CommitAndSend();
			}
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			if (m_DiffList.Visible==true)
			{
				m_DiffList.Focus();
			}
			else
				m_DiffList.Show();
		}
	}
}
