using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace commitEmailComposer
{
	/// <summary>
	/// Summary description for Form2.
	/// </summary>
	public class DiffList : System.Windows.Forms.Form
	{
		private System.Windows.Forms.CheckedListBox listBox1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public DiffList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(DiffList));
			this.listBox1 = new System.Windows.Forms.CheckedListBox();
			this.SuspendLayout();
			// 
			// listBox1
			// 
			this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listBox1.Location = new System.Drawing.Point(0, 0);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(536, 364);
			this.listBox1.Sorted = true;
			this.listBox1.TabIndex = 0;
			this.listBox1.Click += new System.EventHandler(this.listBox1_Click);
			this.listBox1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.listBox1_ItemCheck);
			// 
			// DiffList
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(536, 368);
			this.Controls.Add(this.listBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "DiffList";
			this.ShowInTaskbar = false;
			this.Text = "cvsxdiff list";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.DiffList_Closing);
			this.ResumeLayout(false);

		}
		#endregion

		public void LoadDiffList(string fileName)
		{
			StreamReader reader=null;

			try
			{
				reader=new StreamReader(fileName);
			}
			catch
			{
			}

			if (reader!=null)
			{
				while (reader.Peek()!=-1)
				{
					string fileNameLine=reader.ReadLine().ToLower();
					FileInfo info=new FileInfo();
					info.m_FileDir=Path.GetDirectoryName(fileNameLine);
					info.m_FileName=Path.GetFileName(fileNameLine);
					m_FileList.Add(fileNameLine,info);
				}

				reader.Close();
			}

			LoadListBox();
		}

		public void SaveDiffList(string fileName)
		{
			StreamWriter writer=null;

			try
			{
				writer=new StreamWriter(fileName,true);
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message ,"Mail Send Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
				while( e.InnerException != null )
				{
					e = e.InnerException;
					MessageBox.Show(e.ToString(),"Inner Exception",MessageBoxButtons.OK,MessageBoxIcon.Error);
				}
			}

			if (writer!=null)
			{
				IDictionaryEnumerator enumerator=m_FileList.GetEnumerator();
				while (enumerator.MoveNext())
				{
					FileInfo info=enumerator.Value as FileInfo;
					writer.WriteLine(info.ToString());
				}

				writer.Close();
			}
		}

		public void AddFiles(ArrayList fileList,string currentDirectory)
		{
			//Debugger.Break();

			foreach (string file in fileList)
			{
				string fileName=currentDirectory + "/" + file;
				fileName=fileName.ToLower();
				if (!m_FileList.ContainsKey(fileName))
				{
					FileInfo info=new FileInfo();
					info.m_FileDir=Path.GetDirectoryName(fileName);
					info.m_FileName=Path.GetFileName(fileName);
					m_FileList.Add(fileName,info);
				}
			}

			LoadListBox();
		}

	
		private void LoadListBox()
		{
			listBox1.Items.Clear();
			IDictionaryEnumerator enumerator=m_FileList.GetEnumerator();
			while (enumerator.MoveNext())
			{
				FileInfo info=enumerator.Value as FileInfo;
				listBox1.Items.Add(info);
			}
		}

		Hashtable	m_FileList=new Hashtable();

		private void DiffList_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// just hide it
			e.Cancel=true;
			Hide();	
		}

		private bool m_AllowSet=false;
		private void listBox1_Click(object sender, System.EventArgs e)
		{
			if (listBox1.SelectedIndex>=0)
			{
				m_AllowSet=true;
				listBox1.SetItemCheckState(listBox1.SelectedIndex,CheckState.Checked);
				m_AllowSet=false;

				FileInfo info=listBox1.Items[listBox1.SelectedIndex] as FileInfo;
				
				if (info!=null)
				{
					// set up process:
					ProcessStartInfo processInfo=new ProcessStartInfo();
					processInfo.FileName="cvsxdiff.bat";
					processInfo.WorkingDirectory=info.m_FileDir;
					processInfo.Arguments="\"" + info.m_FileName + "\" ";
					processInfo.RedirectStandardOutput=true;
					processInfo.UseShellExecute=false;
					processInfo.CreateNoWindow=true;
				
					try
					{
						Process process=Process.Start(processInfo);
					}
					catch
					{
					}
				}
			}
		}

		private void listBox1_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			// only click should change the checked status:
			if (m_AllowSet==false)
				e.NewValue=e.CurrentValue;
		}
	}

	class FileInfo
	{
		public String m_FileDir;
		public String m_FileName;

		public override string ToString()
		{
			return m_FileDir + "\\" + m_FileName;
		}

	}
}
