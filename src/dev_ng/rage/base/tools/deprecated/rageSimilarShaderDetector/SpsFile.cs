using System;
using System.IO;
using System.Collections;

namespace rageSimilarShaderDetector
{
	/// <summary>
	/// Summary description for SpsFile.
	/// </summary>
	public class SpsFile
	{
		public SpsFile(string strFilename)
		{
			// Open file
			// System.Console.WriteLine("Parsing : "+ strTypeFile);
			Filename = strFilename;
			m_aobFileWords = new ArrayList();
			StreamReader sr = File.OpenText(strFilename);

			// Parse file
			string strInput;
			while ((strInput=sr.ReadLine())!=null) 
			{
				// Split line on white space
				string[] astrWords = strInput.Split(" \t\r\n".ToCharArray());

				foreach(string strWord in astrWords)
				{
					m_aobFileWords.Add(strWord);
				}
			}

			// Ok, got a list of my words, end
			WorthChecking = true;
		}

		public ArrayList	GetFileWords()
		{
			return m_aobFileWords;
		}

		private ArrayList	m_aobFileWords;

		private string		m_strFilename;
		// Declare a Filename property of type string:
		public string Filename
		{
			get 
			{
				return m_strFilename; 
			}
			set 
			{
				m_strFilename = value.Replace("/", "\\"); 
			}
		}

		private bool		m_bWorthChecking;	//	It is "WorthChecking" if it has not already been compared against all other SPS files OR it is not already a known duplicate
		// Declare a WorthChecking property of type bool:
		public bool WorthChecking
		{
			get 
			{
				return m_bWorthChecking; 
			}
			set 
			{
				m_bWorthChecking = value; 
			}
		}
	}
}
