using System;
using System.IO;
using System.Collections;
using rageUsefulCSharpToolClasses;

namespace rageSimilarShaderDetector
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		//		public const string RAGE_MODULE_PATH_ENVIRONMENT_VARIABLE	= "RAGE_MODULE_PATH";
		public const string DEFAULT_SOURCE_LOCATION					= "t:/pong/assets/";
		public const string FILE_EXTENSION_TO_SEARCH_FOR			= ".sps";

		static void OutputUsageInfo()
		{
			System.Console.WriteLine("Usage : rageSimilarShaderDetector [-source <source folder>]");
			System.Console.WriteLine("[-source <source folder>]\tFolder to look for usage in, defaults to {0}", DEFAULT_SOURCE_LOCATION);
			System.Console.WriteLine("[-ext <extension to report on>]\tExtension to report on, defaults to {0}", FILE_EXTENSION_TO_SEARCH_FOR);
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static int Main(string[] args)
		{
			string strSourceLocation = DEFAULT_SOURCE_LOCATION;
			string strFileExtensionToSearchFor = FILE_EXTENSION_TO_SEARCH_FOR;
			for(int i=0; i<args.GetLength(0); i++)
			{
				if(args[i] == "-source")
				{
					strSourceLocation = args[++i];
				}
				else if(args[i] == "-ext")
				{
					strFileExtensionToSearchFor = args[++i];
				}
				else
				{
					System.Console.WriteLine("Unknown argument {0}", args[i]);
					OutputUsageInfo();
					return -1;
				}
			}

			// Convert all slashes to nice friendly '/'
			strSourceLocation = strSourceLocation.Replace("\\", "/");

			// Make sure the given paths end in "/"
			if(strSourceLocation[strSourceLocation.Length - 1] != '/') strSourceLocation += "/";
			strFileExtensionToSearchFor = strFileExtensionToSearchFor.ToLower();

			// Get a list of type files
			ArrayList obAStrSpsFiles = rageFileUtilities.GetFileList(strSourceLocation, "*"+ strFileExtensionToSearchFor);

			// Create structures to record the information
			ArrayList obAAStrContentsOfSpsFiles = new ArrayList();

			// Got a list of type files, now look for references to the given file extension in them
			foreach(string strSpsFile in obAStrSpsFiles)
			{
				// Create structure to record the information
				SpsFile obSpsFile = new SpsFile(strSpsFile);

				// Add the file to the list of files
				obAAStrContentsOfSpsFiles.Add(obSpsFile);
			}

			// Ok, got all the files, now look for matches
			// Compare each file to other files
			int iNoOfDuplicateFiles = 0;
			foreach(SpsFile obSpsFile in obAAStrContentsOfSpsFiles)
			{
				if(obSpsFile.WorthChecking)	//	It is "WorthChecking" if it has not already been compared against all other SPS files OR it is not already a known duplicate
				{
					// Create structures to record the duplicates
					ArrayList obDuplicatesOfSpsFile = new ArrayList();

					foreach(SpsFile obPossiblySimilarSpsFile in obAAStrContentsOfSpsFiles)
					{
						// Compare the contents of the two files
						if(obPossiblySimilarSpsFile.GetFileWords().Count == obSpsFile.GetFileWords().Count)
						{
							// Same length, so it is worth comparing the contents.
							bool bFilesAreIdentical = true;
							for(int i=0; i<obSpsFile.GetFileWords().Count; i++)
							{
								string strMyWord				= obSpsFile.GetFileWords()[i] as string;
								string strPossiblySimilarWord	= obPossiblySimilarSpsFile.GetFileWords()[i] as string;
								if(strMyWord != strPossiblySimilarWord)
								{
									// Not the same, but are they numbers?
									bool bIsWordAValidDouble = true;
									double dMyWord = 0.0;
									try 
									{
										dMyWord = Convert.ToDouble(strMyWord);
									} 
									catch
									{
										// Not a valid double
										bIsWordAValidDouble = false;
									}
									if(bIsWordAValidDouble)
									{
										double dPossiblySimilarWord = 0.0;
										try 
										{
											dPossiblySimilarWord = Convert.ToDouble(strPossiblySimilarWord);
										} 
										catch
										{
											// Not a valid double
											bIsWordAValidDouble = false;
										}
										if(bIsWordAValidDouble)
										{
											// Both words are valid doubles, so do compare them
											// System.Console.WriteLine("Are "+ dMyWord +" and "+ dPossiblySimilarWord +" similar enough?");
										}
									}
									bFilesAreIdentical = false;
									break;
								}
							}
							if(bFilesAreIdentical)
							{
								// Bingo!
								// I've found a duplicate so log it
								obDuplicatesOfSpsFile.Add(obPossiblySimilarSpsFile);

								// And make it so I don't bother checking it again
								obPossiblySimilarSpsFile.WorthChecking = false;
								// System.Console.WriteLine(obSpsFile.Filename +" is the same as "+ obPossiblySimilarSpsFile.Filename);
							}
						}
					}

					// Are there duplicates?
					if(obDuplicatesOfSpsFile.Count > 1)
					{
						// Found duplicates, so list them
						System.Console.WriteLine("The following files are identical:");
						foreach(SpsFile obDuplicateOfSpsFile in obDuplicatesOfSpsFile)
						{
							System.Console.WriteLine(obDuplicateOfSpsFile.Filename);
						}
						iNoOfDuplicateFiles += obDuplicatesOfSpsFile.Count - 1;
					}
					// Make it so I don't bother checking it again
					obSpsFile.WorthChecking = false;
				}
			}

			System.Console.WriteLine(obAAStrContentsOfSpsFiles.Count +" sps files scanned.  This could be reduced to "+ (obAAStrContentsOfSpsFiles.Count - iNoOfDuplicateFiles) +" if duplicates are removed.");
			return 0;
		}
	}
}
