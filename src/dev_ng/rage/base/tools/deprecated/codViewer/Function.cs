using System;

namespace codViewer
{
	/// <summary>
	/// Summary description for Function.
	/// </summary>
	public class Function : Object
	{
		public string m_szFullName;
		public string m_szDisplayName;
		public string m_szIPC;
		public System.Collections.ArrayList m_szInstructions;
		
		public Function()
		{
			m_szInstructions = new System.Collections.ArrayList();
		}

		public override string ToString()
		{
			return m_szDisplayName;
		}

		public void AddInstruction(Instruction inst)
		{
			m_szInstructions.Add(inst);
		}
	}
}
