using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;

namespace codViewer
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ColumnHeader VQFStall;
		private System.Windows.Forms.ColumnHeader BranchFlush;
		private System.Windows.Forms.ColumnHeader Microcode;
		private System.Windows.Forms.ColumnHeader StructuralHazard;
		private System.Windows.Forms.ColumnHeader DecoupledStore;
		private System.Windows.Forms.ColumnHeader LoadHitStore;
		private System.Windows.Forms.Label FunctionName;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.ColumnHeader LoadHitStoreUse;
		private System.Windows.Forms.ListBox Functions;
		private System.Windows.Forms.Label IPC;
		private System.Windows.Forms.ColumnHeader AddressDependencyStall;
		private System.Windows.Forms.MenuItem FileExit;
		private System.Windows.Forms.ListView Instructions;
		private System.Windows.Forms.ColumnHeader Instruction;
		private System.Windows.Forms.ColumnHeader Params;
		private System.Windows.Forms.ColumnHeader IS2Exit;
		private System.Windows.Forms.ColumnHeader DispatchSlot;
		private System.Windows.Forms.ColumnHeader VQ8Exit;
		private System.Windows.Forms.ColumnHeader DependencyStall;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem File;
		private System.Windows.Forms.MenuItem FileOpen;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private string m_szCodFileName;
		private DateTime m_lastUpdateTime;
		private System.Timers.Timer timer1;
		public int m_nCurrentFunction;

		public Form1(string szCodFile)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			if( szCodFile != null )
				LoadCodFile(szCodFile);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.VQFStall = new System.Windows.Forms.ColumnHeader();
			this.BranchFlush = new System.Windows.Forms.ColumnHeader();
			this.Microcode = new System.Windows.Forms.ColumnHeader();
			this.StructuralHazard = new System.Windows.Forms.ColumnHeader();
			this.DecoupledStore = new System.Windows.Forms.ColumnHeader();
			this.LoadHitStore = new System.Windows.Forms.ColumnHeader();
			this.FunctionName = new System.Windows.Forms.Label();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.LoadHitStoreUse = new System.Windows.Forms.ColumnHeader();
			this.Functions = new System.Windows.Forms.ListBox();
			this.IPC = new System.Windows.Forms.Label();
			this.AddressDependencyStall = new System.Windows.Forms.ColumnHeader();
			this.FileExit = new System.Windows.Forms.MenuItem();
			this.Instructions = new System.Windows.Forms.ListView();
			this.Instruction = new System.Windows.Forms.ColumnHeader();
			this.Params = new System.Windows.Forms.ColumnHeader();
			this.IS2Exit = new System.Windows.Forms.ColumnHeader();
			this.DispatchSlot = new System.Windows.Forms.ColumnHeader();
			this.VQ8Exit = new System.Windows.Forms.ColumnHeader();
			this.DependencyStall = new System.Windows.Forms.ColumnHeader();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.File = new System.Windows.Forms.MenuItem();
			this.FileOpen = new System.Windows.Forms.MenuItem();
			this.timer1 = new System.Timers.Timer();
			((System.ComponentModel.ISupportInitialize)(this.timer1)).BeginInit();
			this.SuspendLayout();
			// 
			// VQFStall
			// 
			this.VQFStall.Text = "VQ Full Stall";
			// 
			// BranchFlush
			// 
			this.BranchFlush.Text = "Branch Flush";
			// 
			// Microcode
			// 
			this.Microcode.Text = "Microcode";
			// 
			// StructuralHazard
			// 
			this.StructuralHazard.Text = "Structural Hazard";
			// 
			// DecoupledStore
			// 
			this.DecoupledStore.Text = "Decoupled Store";
			// 
			// LoadHitStore
			// 
			this.LoadHitStore.Text = "Load Hit Store";
			// 
			// FunctionName
			// 
			this.FunctionName.Location = new System.Drawing.Point(146, 32);
			this.FunctionName.Name = "FunctionName";
			this.FunctionName.Size = new System.Drawing.Size(100, 16);
			this.FunctionName.TabIndex = 5;
			this.FunctionName.Text = "label1";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 1;
			this.menuItem4.Text = "-";
			// 
			// LoadHitStoreUse
			// 
			this.LoadHitStoreUse.Text = "Load Hit Store Use";
			// 
			// Functions
			// 
			this.Functions.Location = new System.Drawing.Point(34, 32);
			this.Functions.Name = "Functions";
			this.Functions.Size = new System.Drawing.Size(96, 173);
			this.Functions.TabIndex = 4;
			this.Functions.SelectedIndexChanged += new System.EventHandler(this.Functions_SelectedIndexChanged);
			// 
			// IPC
			// 
			this.IPC.Location = new System.Drawing.Point(146, 56);
			this.IPC.Name = "IPC";
			this.IPC.Size = new System.Drawing.Size(100, 16);
			this.IPC.TabIndex = 6;
			this.IPC.Text = "label1";
			// 
			// AddressDependencyStall
			// 
			this.AddressDependencyStall.Text = "Address Dependency Stall";
			// 
			// FileExit
			// 
			this.FileExit.Index = 2;
			this.FileExit.Text = "Exit";
			this.FileExit.Click += new System.EventHandler(this.FileExit_Click);
			// 
			// Instructions
			// 
			this.Instructions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						   this.Instruction,
																						   this.Params,
																						   this.IS2Exit,
																						   this.DispatchSlot,
																						   this.VQ8Exit,
																						   this.DependencyStall,
																						   this.AddressDependencyStall,
																						   this.StructuralHazard,
																						   this.DecoupledStore,
																						   this.Microcode,
																						   this.VQFStall,
																						   this.BranchFlush,
																						   this.LoadHitStore,
																						   this.LoadHitStoreUse});
			this.Instructions.FullRowSelect = true;
			this.Instructions.GridLines = true;
			this.Instructions.Location = new System.Drawing.Point(144, 144);
			this.Instructions.Name = "Instructions";
			this.Instructions.TabIndex = 3;
			this.Instructions.View = System.Windows.Forms.View.Details;
			// 
			// Instruction
			// 
			this.Instruction.Text = "Instruction";
			this.Instruction.Width = 80;
			// 
			// Params
			// 
			this.Params.Text = "Parameters";
			this.Params.Width = 140;
			// 
			// IS2Exit
			// 
			this.IS2Exit.Text = "IS2 Exit";
			// 
			// DispatchSlot
			// 
			this.DispatchSlot.Text = "Dispatch Slot";
			// 
			// VQ8Exit
			// 
			this.VQ8Exit.Text = "VQ8 Exit";
			// 
			// DependencyStall
			// 
			this.DependencyStall.Text = "Dependency Stall";
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.File});
			// 
			// File
			// 
			this.File.Index = 0;
			this.File.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																				 this.FileOpen,
																				 this.menuItem4,
																				 this.FileExit});
			this.File.Text = "File";
			// 
			// FileOpen
			// 
			this.FileOpen.Index = 0;
			this.FileOpen.Text = "Open";
			this.FileOpen.Click += new System.EventHandler(this.FileOpen_Click);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 1000;
			this.timer1.SynchronizingObject = this;
			this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Elapsed);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.Functions);
			this.Controls.Add(this.IPC);
			this.Controls.Add(this.Instructions);
			this.Controls.Add(this.FunctionName);
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
			this.Text = "Cod Viewer";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Layout += new System.Windows.Forms.LayoutEventHandler(this.Form1_Layout);
			((System.ComponentModel.ISupportInitialize)(this.timer1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args) 
		{
			string szCodFile = null;
			if( args.Length > 0 )
				szCodFile = args[0];
				
			Application.Run(new Form1(szCodFile));
		}

		private void Form1_Layout(object sender, System.Windows.Forms.LayoutEventArgs e)
		{
			// Layout Funciton List
			Functions.Location = new System.Drawing.Point(8, 8);
			Functions.Size = new System.Drawing.Size((ClientSize.Width / 3) -8, ClientSize.Height - 16);

			// Layout Function Name
			int left = (ClientSize.Width / 3) + 8;
			FunctionName.Location = new System.Drawing.Point(left, 8);
			FunctionName.Size = new System.Drawing.Size((ClientSize.Width / 2), FunctionName.Height);
			FunctionName.Text = "Function:";

			// Layout IPC
			IPC.Location = new System.Drawing.Point(left, 12 + FunctionName.Height);
			IPC.Size = new System.Drawing.Size((ClientSize.Width / 2), IPC.Height);
			IPC.Text = "IPC: ";

			// Layout Instruction List
			int top = 16 + FunctionName.Height + IPC.Height;
			Instructions.Location = new System.Drawing.Point(left, top);
			Instructions.Size = new System.Drawing.Size(ClientSize.Width - 8 - left, ClientSize.Height - 8 - top);
		}

		private void FileOpen_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "cod files|*.cod";
			if( dlg.ShowDialog() == DialogResult.OK )
			{
				LoadCodFile(dlg.FileName);
			}
		}

		private void FileExit_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void LoadCodFile(string szCodFileName)
		{
			// Remember the file name for later
			m_szCodFileName = szCodFileName;
			m_lastUpdateTime = System.IO.File.GetLastWriteTime(m_szCodFileName);

			// Clear any exisiting data
			int nFunc = m_nCurrentFunction;
			Functions.Items.Clear();			

			// Open the file
			StreamReader sr = new StreamReader(szCodFileName);

			// Parse the file
			string line;
			Function currentFunction = null;
			while ((line = sr.ReadLine()) != null) 
			{
				const string functionStart = "; Begin code for function: ";
				const string functionEnd = "Estimated PX IPC = ";
				const string functionLine = "  ";
				// Check for a function
				int index = line.IndexOf(functionStart);
				if( index != -1 )
				{
					// Found a function
					if( currentFunction != null )
					{
						// Close the current function, guess it doesnt have an IPC count
						currentFunction.m_szIPC = "Unknown";
						Functions.Items.Add(currentFunction);
						currentFunction = null;
					}
					
					// Get the full function name
					string szFullFunctionName = line.Remove(0, functionStart.Length);

					// Get the display name
					string szDisplayName = szFullFunctionName.Remove(0, 1);

					index = szDisplayName.IndexOf("@");
					if( index != -1 )
						szDisplayName = szDisplayName.Remove(index, szDisplayName.Length - index);

					// Start parsing the function's instructions
					currentFunction = new Function();
					currentFunction.m_szFullName = szFullFunctionName;
					currentFunction.m_szDisplayName = szDisplayName;
				}

				// Check for function contents
				if( currentFunction != null )
				{
					// Check for an instruction in the function
					if( line.StartsWith(functionLine) )
					{
						index = line.IndexOf("\t ");
						string szInstructionName = line.Remove(0, index + 2);
						index = szInstructionName.IndexOf(" ");
						string szParams = "";
						if( index != -1 )
						{
							szParams = szInstructionName.Remove(0, index);
							szParams = szParams.Trim();
							szInstructionName = szInstructionName.Remove(index, szInstructionName.Length - index);
						}

						string szFlagsLine = sr.ReadLine();
						index = szFlagsLine.IndexOf("[");
						szFlagsLine = szFlagsLine.Remove(0, index + 1);
						index = szFlagsLine.IndexOf("|");
						string szIS2Exit = szFlagsLine.Remove(index, szFlagsLine.Length - index).Trim();
						szFlagsLine = szFlagsLine.Remove(0, index + 1);
						index = szFlagsLine.IndexOf("]");
						string szDispatchSlot = szFlagsLine.Remove(index, szFlagsLine.Length - index).Trim();
						szFlagsLine = szFlagsLine.Remove(0, index + 1);
						szFlagsLine = szFlagsLine.Trim();

						Instruction inst = new Instruction();
						inst.m_szName = szInstructionName;
						inst.m_szParams = szParams;
						inst.m_szIS2Exit = szIS2Exit;
						inst.m_szDispatchSlot = szDispatchSlot;

						while( szFlagsLine.Length > 0 )
						{
							string szFlag;
							index = szFlagsLine.IndexOf(" ");
							if( index != -1 )
							{
								szFlag = szFlagsLine.Remove(index, szFlagsLine.Length - index);
							}
							else
							{
								szFlag = szFlagsLine;
							}
							szFlagsLine = szFlagsLine.Remove(0, szFlag.Length);
							szFlagsLine = szFlagsLine.Trim();

							if( szFlag.StartsWith("VQF") )
							{
								inst.m_szVQFStall = szFlag.Remove(0, 3);
							}
							else if( szFlag.StartsWith("V") )
							{
								inst.m_szVQ8Exit = szFlag.Remove(0, 1);
							}
							else if( szFlag.StartsWith("DA") )
							{
								inst.m_szAddressDependencyStall = szFlag.Remove(0, 2);
							}
							else if( szFlag.StartsWith("DC") )
							{
								inst.m_szDecoupledStore = szFlag;
							}
							else if( szFlag.StartsWith("D") )
							{
								inst.m_szDependencyStall = szFlag.Remove(0, 1);
							}
							else if( szFlag.StartsWith("S") )
							{
								inst.m_szStructuralHazard = szFlag;
							}
							else if( szFlag.StartsWith("MC") )
							{
								inst.m_szMicrocode = szFlag;
							}
							else if( szFlag.StartsWith("BF") )
							{
								inst.m_szBranchFlush = szFlag;
							}
							else if( szFlag.StartsWith("LHSUSE") )
							{
								inst.m_szLoadHitStoreUse = szFlag;
							}
							else if( szFlag.StartsWith("LHS") )
							{
								inst.m_szLoadHitStore = szFlag;
							}
							else
							{
								Console.WriteLine("Unknown Flag: %s\n", szFlag);
							}
						}

						currentFunction.AddInstruction(inst);
					}

					// Check for the end of the function
					index = line.IndexOf(functionEnd);
					if( index != -1 )
					{
						// Grab the IPC
						currentFunction.m_szIPC = line.Remove(0, functionEnd.Length);
					
						// Add the function to the list
						Functions.Items.Add(currentFunction);

						// Clear the function pointer
						currentFunction = null;
					}
				}
			}

			// Close the file
			sr.Close();

			// Set the Initial function
			Functions.SelectedIndex = nFunc;
		}
		
		public void SetFunction(int nFunctionIndex)
		{
			if( Functions.Items.Count <= 0 )
				return;

			// Set the index
			int nFunc = nFunctionIndex;
			if( nFunc > Functions.Items.Count )
				nFunc = 0;
			m_nCurrentFunction = nFunc;

			// Select the function in the function list
			Function func = (Function)Functions.Items[nFunc];

			// Set the name of the function
			FunctionName.Text = func.m_szFullName;

			// Set the ipc of the function
			IPC.Text = func.m_szIPC;

			// Set the instructions
			Instructions.Items.Clear();
			for( int i = 0; i < func.m_szInstructions.Count; i++ )
			{
				Instruction inst = (Instruction)func.m_szInstructions[i];
				ListViewItem instItem = Instructions.Items.Add(inst.m_szName);
				instItem.SubItems.Add(inst.m_szParams);
				instItem.SubItems.Add(inst.m_szIS2Exit);
				instItem.SubItems.Add(inst.m_szDispatchSlot);
				instItem.SubItems.Add(inst.m_szVQ8Exit);
				instItem.SubItems.Add(inst.m_szDependencyStall);
				instItem.SubItems.Add(inst.m_szAddressDependencyStall);
				instItem.SubItems.Add(inst.m_szStructuralHazard);
				instItem.SubItems.Add(inst.m_szDecoupledStore);
				instItem.SubItems.Add(inst.m_szMicrocode);
				instItem.SubItems.Add(inst.m_szVQFStall);
				instItem.SubItems.Add(inst.m_szBranchFlush);	
				instItem.SubItems.Add(inst.m_szLoadHitStore);
				instItem.SubItems.Add(inst.m_szLoadHitStoreUse);
			}
		}

		private void Functions_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SetFunction(Functions.SelectedIndex);
		}

		private void timer1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			if( m_szCodFileName != null )
			{
				DateTime time = System.IO.File.GetLastWriteTime(m_szCodFileName);
				if( time.Ticks > m_lastUpdateTime.Ticks )
				{
					// File was modified, reload it
					DateTime originalTime = m_lastUpdateTime;
					try
					{
						LoadCodFile(m_szCodFileName);
					}
					catch( Exception  )
					{
						m_lastUpdateTime = originalTime;
					}
				}
			}
		}
	}
}
