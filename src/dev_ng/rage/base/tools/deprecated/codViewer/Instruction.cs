using System;

namespace codViewer
{
	/// <summary>
	/// Summary description for Instruction.
	/// </summary>
	public class Instruction : Object
	{
		public string m_szName;
		public string m_szParams;
		public string m_szIS2Exit;
		public string m_szDispatchSlot;
		public string m_szVQ8Exit;
		public string m_szDependencyStall;
		public string m_szAddressDependencyStall;
		public string m_szStructuralHazard;
		public string m_szDecoupledStore;
		public string m_szMicrocode;
		public string m_szVQFStall;
		public string m_szBranchFlush;	
		public string m_szLoadHitStore;
		public string m_szLoadHitStoreUse;

		public Instruction()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
