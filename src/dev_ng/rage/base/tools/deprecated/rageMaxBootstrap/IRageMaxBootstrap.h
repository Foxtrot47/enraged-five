// ./IRageMaxBootStrap.h

#ifndef __IRAGE_MAX_BOOTSTRAP_H__
#define __IRAGE_MAX_BOOTSTRAP_H__

//-----------------------------------------------------------------------------

#define RAGEMAXBOOTSTRAP_INTERFACE Interface_ID(0x467c3442, 0x64962b17)

#define GetRageMaxBootstrapInterface(cd) \
	(IRageMaxBootStrap*)(cd)->GetFPInterface(RAGEMAXBOOTSTRAP_INTERFACE)

//-----------------------------------------------------------------------------

class IRageMaxBootStrap : public FPStaticInterface
{
public:
	virtual ~IRageMaxBootStrap() {};
	void	DoBootstap(BOOL bEvalAll, BOOL bQuiet);
};

//-----------------------------------------------------------------------------

#endif //__IRAGE_MAX_BOOTSTRAP_H__