// /rageMaxBootstrap.h

#ifndef __RAGE_MAX_BOOTSTRAP_H__
#define __RAGE_MAX_BOOTSTRAP_H__

#include <list>
#include <string>

#include "IRageMaxBootstrap.h"

using namespace std;

//-----------------------------------------------------------------------------

#define RAGE_MAX_BOOTSTRAP_CLASS_ID	Class_ID(0x398c5c28, 0x2ccf091a)

class CharStream;

//-----------------------------------------------------------------------------

ClassDesc* GetRageMaxBootstrapGupDesc();

//-----------------------------------------------------------------------------

class RageMaxBootstrapGUP : public GUP, public IRageMaxBootStrap
{
public:
	DWORD	Start();
	void	Stop();

	BOOL	DoBootstrap(BOOL bEvalAll, BOOL bQuiet);

private:
	bool	EvalScripts();
	
	void	EvalScript(const char* szScriptPath, bool quietMode = true);
	
	void	GetScriptLists(string& rageScriptRoot, list<string>& startupScripts, list<string>& standardScripts);

	static Value* FileIn(CharStream* source, bool quiet);

public:
	enum
	{
		em_DoBootstrap
	};

	DECLARE_DESCRIPTOR(RageMaxBootstrapGUP)

	BEGIN_FUNCTION_MAP
		FN_2(em_DoBootstrap, TYPE_BOOL, DoBootstrap, TYPE_BOOL, TYPE_BOOL);
	END_FUNCTION_MAP
};

//-----------------------------------------------------------------------------

#endif //__RAGE_MAX_BOOTSTRAP_H__