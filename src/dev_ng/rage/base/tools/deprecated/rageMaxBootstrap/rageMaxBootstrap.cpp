// /rageMaxBootstrap.cpp
#include <algorithm>
#include <string>
#include <list>
#include <queue>

#include "atl/string.h"
#include "file/asset.h"
#include "file/device.h"
#include "init/moduleSettings.h"

#include "maxscrpt/maxscrpt.h"
#include "maxscrpt/numbers.h"
#include "maxscrpt/arrays.h"
#include "maxscrpt/strings.h"
#include "maxscrpt/name.h"
#include "maxscrpt/parser.h"
#include "maxscrpt/definsfn.h"

#include "resource.h"
#include "rageMaxBootstrap.h"

using namespace std;
using namespace rage;

#define RAGE_SCRIPT_PATH_TAG	"rageMaxScriptRoot"

//-----------------------------------------------------------------------------

RageMaxBootstrapGUP	sRageMaxBootstrapGUP (
	RAGEMAXBOOTSTRAP_INTERFACE, _T("rageBootstrap"), 0, NULL, FP_CORE,

	RageMaxBootstrapGUP::em_DoBootstrap, _T("bootstrap"), 0, TYPE_BOOL, 0, 2,
		_T("evalAll"), 0, TYPE_BOOL,
		_T("quiet"), 0, TYPE_BOOL,

	end
	);

TCHAR *GetString(int id);
//-----------------------------------------------------------------------------

class RageMaxBootstrapGUPClassDesc : public ClassDesc2
{
public:
	int				IsPublic()						{ return 1; }
	void*			Create(BOOL loading = FALSE)	{ return &sRageMaxBootstrapGUP; } 
	const TCHAR*	ClassName()						{ return GetString(IDS_CLASSNAME); }
	SClass_ID		SuperClassID()					{ return GUP_CLASS_ID; } 
	Class_ID		ClassID()						{ return RAGE_MAX_BOOTSTRAP_CLASS_ID; }
	const TCHAR*	Category()						{ return GetString(IDS_CATEGORY); }
};

static RageMaxBootstrapGUPClassDesc sRageMaxBootStrapGUPDesc;

//-----------------------------------------------------------------------------

struct ScriptDirFileData
{
	string		   root;
	list< string > directories;
	list< string > scripts;
};

//-----------------------------------------------------------------------------

void ReplaceFwdSlashWithBackSlash(string& str, bool bTermWithBackslash)
{
	std::string::iterator it;
	for(it = str.begin(); 
		it != str.end();
		++it)
	{
		if(*it == '/')
			*it = '\\';
	}

	if(bTermWithBackslash)
	{
		it--;

		//Make sure the last character is a backslash
		if(*it != '\\')
			str.append("\\");
	}
}

//-----------------------------------------------------------------------------

ClassDesc* GetRageMaxBootstrapGupDesc()
{
	return &sRageMaxBootStrapGUPDesc;
}

//-----------------------------------------------------------------------------

DWORD RageMaxBootstrapGUP::Start()
{
	return GUPRESULT_KEEP;
}

//-----------------------------------------------------------------------------

void RageMaxBootstrapGUP::Stop()
{
}

//-----------------------------------------------------------------------------

BOOL RageMaxBootstrapGUP::DoBootstrap(BOOL bEvalAll, BOOL bQuiet)
{
	string rageScriptRoot;

	atString atRageScriptPathRoot;
	if(initModuleSettings::GetModuleSetting(RAGE_SCRIPT_PATH_TAG, atRageScriptPathRoot))
	{
		rageScriptRoot = (const char*)atRageScriptPathRoot;

		//Flip all forward slashes to backslashes
		ReplaceFwdSlashWithBackSlash(rageScriptRoot, true);
	}
	else
	{
		return false;
	}

	//Check to see if the RAGE startup script path is already set as either the standard startup
	//script path or the user startup script path, if it is then we don't need to manually evaluate
	//those scripts
	bool bEvalStartupScripts = true;
	transform(rageScriptRoot.begin(), rageScriptRoot.end(), rageScriptRoot.begin(), tolower);

	string maxStdStartpScriptPath(IPathConfigMgr::GetPathConfigMgr()->GetDir(APP_STARTUPSCRIPTS_DIR));
	transform(maxStdStartpScriptPath.begin(), maxStdStartpScriptPath.end(), maxStdStartpScriptPath.begin(), tolower);
	ReplaceFwdSlashWithBackSlash(maxStdStartpScriptPath, true);

	string maxUserStartupScriptPath(IPathConfigMgr::GetPathConfigMgr()->GetDir(APP_USER_STARTUPSCRIPTS_DIR));
	transform(maxUserStartupScriptPath.begin(), maxUserStartupScriptPath.end(), maxUserStartupScriptPath.begin(), tolower);
	ReplaceFwdSlashWithBackSlash(maxUserStartupScriptPath, true);

	if( (rageScriptRoot == maxStdStartpScriptPath) ||
		(rageScriptRoot == maxUserStartupScriptPath) )
	{
		bEvalStartupScripts = false;
	}

	//Get a list of all the RAGE scripts that may need to be evaluated
	list< string > startupScripts;
	list< string > standardScripts;

	GetScriptLists( rageScriptRoot, startupScripts, standardScripts );

	if(bEvalStartupScripts)
	{
		//Evaluate the startup scripts
		for(list<string>::iterator it = startupScripts.begin(); 
			it != startupScripts.end();
			++it)
		{
			EvalScript( (*it).c_str(), bQuiet );
		}
	}
	
	if(bEvalAll)
	{
		for(list<string>::iterator it = standardScripts.begin();
			it != standardScripts.end();
			++it)
		{
			EvalScript( (*it).c_str(), bQuiet );
		}
	}

	return true;
}

//-----------------------------------------------------------------------------

void FileEnumCbk(const fiFindData &data,void *userArg)
{
	 ScriptDirFileData* scriptData = static_cast<ScriptDirFileData*>(userArg);

	 if(data.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)
	 {
		 string fullDirPath(scriptData->root);
		 fullDirPath.append( data.m_Name );
		 fullDirPath.append("\\");
		 transform(fullDirPath.begin(), fullDirPath.end(), fullDirPath.begin(), tolower);
		 scriptData->directories.push_back( fullDirPath );
	 }
	 else
	 {
		 if(strcmpi(ASSET.FindExtensionInPath( data.m_Name ), ".ms") == 0)
		 {
			string fullFilePath(scriptData->root);
			fullFilePath.append( data.m_Name );
			transform(fullFilePath.begin(), fullFilePath.end(), fullFilePath.begin(), tolower);
			scriptData->scripts.push_back( fullFilePath );
		 }
	 }
}

//-----------------------------------------------------------------------------

void RageMaxBootstrapGUP::GetScriptLists(string& rageScriptRoot, list<string>& startupScripts, list<string>& standardScripts)
{
	//Get a list of all the startup scripts
	string startupScriptPath = rageScriptRoot;
	startupScriptPath.append("startup\\");
	transform(startupScriptPath.begin(), startupScriptPath.end(), startupScriptPath.begin(), tolower);

	ScriptDirFileData startupDirFileData;
	startupDirFileData.root = startupScriptPath;
	ASSET.EnumFiles( startupScriptPath.c_str(), &FileEnumCbk, &startupDirFileData);

	for(list<string>::iterator it = startupDirFileData.scripts.begin();
		it != startupDirFileData.scripts.end();
		++it)
	{
		startupScripts.push_back( *it );
	}

	//Get a list of all the other scripts
	ScriptDirFileData sdfd;
	sdfd.root = rageScriptRoot;
	ASSET.EnumFiles( rageScriptRoot.c_str(), &FileEnumCbk, &sdfd);

	//Remove the startup script directory from the list of directories
	list<string>::iterator findIt = find( sdfd.directories.begin(), sdfd.directories.end(), startupScriptPath);
	if(findIt != sdfd.directories.end())
	{
		sdfd.directories.erase( findIt );
	}

	for(list<string>::iterator it = sdfd.directories.begin();
		it != sdfd.directories.end();
		++it)
	{
		Displayf("%s", (*it).c_str());
	}

	queue< ScriptDirFileData > searchQueue;
	searchQueue.push( sdfd );

	while( !searchQueue.empty() )
	{
		ScriptDirFileData curSdFd = searchQueue.front();
		searchQueue.pop();

		for(list<string>::iterator it = curSdFd.scripts.begin();
			it != curSdFd.scripts.end();
			++it)
		{
			standardScripts.push_back( *it );
		}

		for(list<string>::iterator it = curSdFd.directories.begin();
			it != curSdFd.directories.end();
			++it)
		{
			ScriptDirFileData newSdFd;
			newSdFd.root = *it;
			ASSET.EnumFiles( (*it).c_str(), &FileEnumCbk, &newSdFd );
			searchQueue.push( newSdFd );
		}
	}

#if 0
	Displayf("=========== Startup Scripts ===========");
	for(list<string>::iterator it = startupScripts.begin();
		it != startupScripts.end();
		++it)
	{
		Displayf("%s", (*it).c_str());
	}

	Displayf("=========== Standard Scripts ===========");
	for(list<string>::iterator it = standardScripts.begin();
		it != standardScripts.end();
		++it)
	{
		Displayf("%s", (*it).c_str());
	}
#endif 

}

//-----------------------------------------------------------------------------

bool RageMaxBootstrapGUP::EvalScripts()
{

	return true;
}

//-----------------------------------------------------------------------------

void RageMaxBootstrapGUP::EvalScript(const char* szScriptPath, bool quietMode)
{
	two_typed_value_locals(FileStream* file, Value* result);

	vl.file = (new FileStream)->open( const_cast<char*>(szScriptPath), "rt");
	if (vl.file == (FileStream*)&undefined)
		throw RuntimeError( "rageMaxBootstrap cannot open file : ",  const_cast<char*>(szScriptPath) );

	try
	{
		vl.result = FileIn( vl.file, (quietMode == true) );
	}
	catch (...)
	{
		vl.file->close();
		throw;
	}
}

//-----------------------------------------------------------------------------

Value* RageMaxBootstrapGUP::FileIn(CharStream* source, bool quietMode)
{
	init_thread_locals();
	push_alloc_frame();
	three_typed_value_locals(Parser* parser, Value* code, Value* result);
	save_current_frames();
	trace_back_active = FALSE;

	CharStream* out = thread_local(current_stdout);
	if (!quietMode)
		source->log_to(out);

	// loop through stream compiling & evaluating all expressions
	try
	{
		// make a new compiler instance
		vl.parser = new Parser (out);
		source->flush_whitespace();
		while (!source->at_eos() || vl.parser->back_tracked)
		{
			vl.code = vl.parser->compile(source);
			vl.result = vl.code->eval();
			if (!quietMode)
				vl.result->print();
			source->flush_whitespace();
		}
		if (vl.parser->expr_level != 0 || vl.parser->back_tracked && vl.parser->token != t_end)
			throw RuntimeError ("Unexpected end of file during fileIn");
		source->close();
	}
	catch (...)
	{
		// catch any errors and tell what file we are in if any
		out->puts("Error occurred during fileIn: ");
		source->sprin1(out);
		out->puts("\n");
		source->close();
		pop_alloc_frame();
		throw;
	}

	// if there was one, return last expression in stream as result
	if (vl.result == NULL)
		vl.result = &ok;
	else
		vl.result = vl.result->get_heap_ptr();
	pop_alloc_frame();
	return_value(vl.result);
}


//-----------------------------------------------------------------------------