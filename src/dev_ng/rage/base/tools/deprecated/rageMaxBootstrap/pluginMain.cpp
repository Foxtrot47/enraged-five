// /pluginMain.cpp
#include <windows.h>

#include "parser/manager.h"
#include "system/param.h"

#include "resource.h"
#include "rageMaxBootstrap.h"

using namespace rage;

//-----------------------------------------------------------------------------

HINSTANCE	ghInstance;
BOOL		gbLibInitialized = FALSE;
BOOL		gbLibShutdown = FALSE;

//-----------------------------------------------------------------------------

BOOL WINAPI DllMain(HINSTANCE hInstDll, ULONG fdwReason, LPVOID lpvReserved)
{
	ghInstance = hInstDll;
	return TRUE;
}

//-----------------------------------------------------------------------------

TCHAR *GetString(int id)
{
	static TCHAR buf[2048];

	if (ghInstance)
		return LoadString(ghInstance, id, buf, sizeof(buf)) ? buf : NULL;

	return NULL;
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) const TCHAR* LibDescription()
{
	return GetString(IDS_DESCRIPTION);
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) int LibNumberClasses() 
{
	return 1;
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) ClassDesc* LibClassDesc(int i) 
{
	switch(i) {
		case 0: 
			return GetRageMaxBootstrapGupDesc();
		default: 
			return 0;
	}
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) ULONG LibVersion() 
{
	return VERSION_3DSMAX;
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) ULONG CanAutoDefer()
{
	return 0;
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) int LibInitialize(void)
{
	if(!gbLibInitialized)
	{
		char appName[] = "rageMaxBootstrap.gup";
		char* p_Argv[1];

		p_Argv[0] = appName;

		sysParam::Init(1,p_Argv);

		INIT_PARSER;

		gbLibInitialized = TRUE;
	}

	return 1;
}

//-----------------------------------------------------------------------------

__declspec( dllexport ) int LibShutdown(void)
{
	if(!gbLibShutdown)
	{
		SHUTDOWN_PARSER;
		gbLibShutdown = TRUE;
	}

	return 1;
}

//-----------------------------------------------------------------------------
