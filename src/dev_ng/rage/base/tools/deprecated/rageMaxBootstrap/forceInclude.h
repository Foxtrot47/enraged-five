// /forceInclude.h

#ifndef __MAX_FORCEINCLUDE_H__
#define __MAX_FORCEINCLUDE_H__

//this file is included through the forceinclude setting
//in the project so is included in every file

//we have to include max first because it defines some functions
//that are macros in the rage headers (Assert)

#include <max.h>
#include <bmmlib.h>
#include <guplib.h>
#include <istdplug.h>
#include <iparamb2.h>
#include <iparamm2.h>
#include <istdplug.h>
#include <modstack.h>
#include <inode.h>
#include <IPathConfigMgr.h>

#if defined(_DEBUG)

#include "forceinclude/win32_tooldebug.h"

#elif defined(NDEBUG)

#include "forceinclude/win32_toolrelease.h"

#else

#include "forceinclude/win32_toolbeta.h"

#endif

#pragma warning( disable: 4800 )
#pragma warning( disable: 4265 )
#pragma warning( disable: 4263 )

#endif //__MAX_FORCEINCLUDE_H__

