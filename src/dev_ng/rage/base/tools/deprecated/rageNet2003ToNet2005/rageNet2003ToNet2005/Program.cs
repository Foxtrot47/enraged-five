using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using rageUsefulCSharpToolClasses;

namespace rageNet2003ToNet2005
{
    class Program
    {
        static void Main(string[] args)
        {
            rageStatus obStatus;

            // Get list of .vcproj files
            ArrayList obVcprojFiles = rageFileUtilities.GetFileList("c:/soft/rage/base", "*.vcproj");

            // Make copies of them
            foreach (string strVcprojFile in obVcprojFiles)
            {
                // Does it contain the string ".Net2005."?
                if (strVcprojFile.IndexOf(".Net2005.") < 1)
                {
                    // It does not contain ".Net2005.", so take a copy of it
                    string strNewName = rageFileUtilities.RemoveFileExtension(strVcprojFile) + ".Net2005.vcproj";

                    // Does the new version already exist?
                    if(!File.Exists(strNewName))
                    {
                        Console.WriteLine("Copying "+ rageFileUtilities.GetFilenameFromFilePath(strVcprojFile) +" to "+ rageFileUtilities.GetFilenameFromFilePath(strNewName));
                        rageFileUtilities.CopyFile(strVcprojFile, strNewName, out obStatus);
                        if (!obStatus.Success())
                        {
                            // Error occured
                            System.Console.WriteLine(obStatus.GetErrorString());
                            return;
                        }
                    }
                }
            }

            // Done vcproj files, now do sln files
            // Get list of .sln files
            ArrayList obSlnFiles = rageFileUtilities.GetFileList("c:/soft/rage/base", "*.sln");

            // Make copies of them
            foreach (string strSlnFile in obSlnFiles)
            {
                // Does it contain the string ".Net2005."?
                if (strSlnFile.IndexOf(".Net2005.") < 1)
                {
                    // It does not contain ".Net2005.", so take a copy of it
                    string strNewName = rageFileUtilities.RemoveFileExtension(strSlnFile) + ".Net2005.sln";

                    // Does the new version already exist?
                    if (!File.Exists(strNewName))
                    {
                        Console.WriteLine("Copying " + rageFileUtilities.GetFilenameFromFilePath(strSlnFile) + " to " + rageFileUtilities.GetFilenameFromFilePath(strNewName));

                        // Create new sln file
                        StreamWriter slnWriter;
                        try
                        {
                            slnWriter = File.CreateText(strNewName);
                        }
                        catch (Exception e)
                        {
                            // Something bad happened
                            Console.WriteLine("Failed to create file " + strNewName + " : " + e.ToString());
                            return;
                        }
                        StreamReader slnReader = File.OpenText(strSlnFile);

                        // Read a line
                        string strInput;
                        while ((strInput = slnReader.ReadLine()) != null)
                        {
                            string strOutput = strInput;
                            if((strOutput.IndexOf(".vcproj") > 0) && (strOutput.IndexOf(".Net2005.") < 1))
                            {
                                // Ok, convert
                                int iPosOfVcproj = strOutput.IndexOf(".vcproj");
                                strOutput = strOutput.Substring(0, iPosOfVcproj) + ".Net2005" + strOutput.Substring(iPosOfVcproj);
                                //Console.WriteLine("Input  : " + strInput);
                                //Console.WriteLine("Output : " + strOutput);
                            }
                            
                            // Write it out
                            slnWriter.WriteLine(strOutput);
                        }
                        // Close files
                        slnReader.Close();
                        slnWriter.Close();
                    }
                }
            }

        }
    }
}
