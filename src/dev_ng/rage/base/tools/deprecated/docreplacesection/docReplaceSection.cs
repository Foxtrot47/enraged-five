using System;
using System.IO;
using System.Text.RegularExpressions;

namespace docreplacesection
{
	/// <summary>
	/// Summary description for docReplaceSection.
	/// </summary>
	class docReplaceSection
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if (args.Length<3)
			{
				PrintUsage();
				return;
			}

			using (StreamReader srIn = File.OpenText(args[0]))
			{
				Regex regExSectionBegin = new Regex(@"\A\s*\##BEGIN\s+" + args[2]);
				Regex regExSectionEnd = new Regex(@"\A\s*\##END\s+" + args[2]);

				String line;
				bool writeLines=true;

				// read through file:
				while ((line=srIn.ReadLine())!=null)
				{
					if (writeLines)
						Console.WriteLine(line);

					// find the beginning of the section:
					if (regExSectionBegin.Match(line).Success==true)
					{
						using (StreamReader srReplace = File.OpenText(args[1]))
						{
							String line2;
							while ((line2=srReplace.ReadLine())!=null)
							{
								Console.WriteLine(line2);
							}
						}
						writeLines=false;
					}		
			
					// find the end of the section:
					if (regExSectionEnd.Match(line).Success==true)
					{
						Console.WriteLine(line);
						writeLines=true;
					}

				}
				
			}
		}

		static void PrintUsage()
		{
			Console.Error.WriteLine("usage: docreplacesection.exe <in file> <replace file> <section to replace>");
		}
	}
}
