using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace rageGEODataSet
{
    public partial class LoadingSplash : Form
    {
        public LoadingSplash()
        {
            InitializeComponent();
        }

        private void OnLoad(object sender, EventArgs e)
        {
           
            //Center the form
            System.Windows.Forms.Screen src = System.Windows.Forms.Screen.PrimaryScreen;
            int src_height = src.Bounds.Height;
            int src_width = src.Bounds.Width;

            //put the form in the center
            this.Left = (src_width - this.Width) / 2;
            this.Top = (src_height - this.Height) / 2;

            m_loadingCircle.Active = true;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            Pen myPen = new Pen(System.Drawing.Color.DarkGray, 1.0F);
            g.DrawRectangle(myPen, 0, 0, this.ClientRectangle.Width-1, this.ClientRectangle.Height-1);
            myPen.Dispose();
            base.OnPaint(e);
        }
    }
}