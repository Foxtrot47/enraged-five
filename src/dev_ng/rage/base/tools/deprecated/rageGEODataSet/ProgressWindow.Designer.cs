namespace rageGEODataSet
{
	partial class ProgressWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.infoLabel = new System.Windows.Forms.Label();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.SuspendLayout();
			// 
			// infoLabel
			// 
			this.infoLabel.AutoSize = true;
			this.infoLabel.Location = new System.Drawing.Point(12, 9);
			this.infoLabel.Name = "infoLabel";
			this.infoLabel.Size = new System.Drawing.Size(37, 13);
			this.infoLabel.TabIndex = 0;
			this.infoLabel.Text = "Status";
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(12, 25);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(608, 23);
			this.progressBar.TabIndex = 1;
			// 
			// ProgressWindow
			// 
			this.ClientSize = new System.Drawing.Size(632, 58);
			this.ControlBox = false;
			this.Controls.Add(this.progressBar);
			this.Controls.Add(this.infoLabel);
			this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "ProgressWindow";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.UseWaitCursor = true;
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label infoLabel;
		private System.Windows.Forms.ProgressBar progressBar;
	}
}
