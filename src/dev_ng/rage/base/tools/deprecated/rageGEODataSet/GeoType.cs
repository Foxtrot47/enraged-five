using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using rageUsefulCSharpToolClasses;

namespace rageGEODataSet
{
	[ComVisible(true)]
	[Guid("4010FFF7-29DD-4e45-92A8-A3B70F8AFFA1")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoType
	{
		string Name { get; set; }
		string FileName { get; set; }
		bool IsInstance { get; set; }
		bool Dirty { get; set; }
		bool Deleted { get; set; }
		string UUID { get; set; }

		IGeoType BaseType { get; set; }
		string BaseTypeName { get; set; }
		IGeoTemplate Template { get; }
		IGeoTypeMemberGeo ContainingMember { get; set; }

		Int32 GetChildTypeCount();
		IGeoType GetChildType(Int32 idx);
		void AddChildType(IGeoType type);

		Int32 GetMemberCount();
		IGeoTypeMemberBase GetMemberByIndex(Int32 idx);
		IGeoTypeMemberBase GetMemberByName(string name);
		IGeoTypeMemberBase GetMemberByUUID(string uuid);
		void AddMember(IGeoTypeMemberBase member);

		void Save();
		void SaveAs(String fileName);

		void SetAllMembersAsInherited();

		void OpenInEditor();
	}

	[ComVisible(true)]
	[Guid("8BC93C0B-EF55-48de-B1F3-D74CF80345D6")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeFileIO
	{
		IGeoType Load(String filename);
	}

	[ComVisible(true)]
	[Guid("FCAE2B1C-25BB-448c-81CD-B6F6A8175FF8")]
	[ProgId("rageGEODataSet.GeoTypeFileIO")]
	public class GeoTypeFileIO : IGeoTypeFileIO
	{
		public GeoTypeFileIO()
		{
		}

		public IGeoType Load(String filename)
		{
			return (IGeoType)(GeoType.Load(filename));
		}
	}

	[ComVisible(true)]
	[Guid("2654086F-C16C-43fd-9E56-254652E532D7")]
	[ProgId("rageGEODataSet.GeoType")]
	public class GeoType : GeoItem, IGeoType
	{
		#region Data Members

		private string m_Name;
		private string m_FileName;
		private string m_UUID; //Only valid for instance types   
		private bool m_IsInstance;
		private bool m_Dirty = false;
		private bool m_Deleted = false;

		private string m_BaseTypeName; // Only valid during loading
		public string m_DccFile = String.Empty;

		public GeoType m_BaseType;
		public GeoTypeMemberGeo m_ContainingMember;
		public GeoTemplate m_Template;

		public List<GeoType> m_ChildTypes = new List<GeoType>(); //Child Type Access
		public List<GeoTypeMemberBase> m_Members = new List<GeoTypeMemberBase>(); //Member Access

		#endregion

		#region Constructors

		public GeoType()
		{
			m_UUID = String.Empty;
		}

		#endregion

		#region IGeoType Interface

		public string Name
		{
			get { return m_Name; }
			set { m_Name = value.ToLower(); }
		}

		public string FileName
		{
			get { return m_FileName; }
			set { m_FileName = value.ToLower(); }
		}

		public string GetFileName()
		{
			if (FileName == null)
			{
				if ((m_ContainingMember != null) && (m_ContainingMember.GeoTypeThatContainsMe != null))
				{
					return (m_ContainingMember.GeoTypeThatContainsMe as GeoType).GetFileName().ToLower();
				}
			}
			return FileName;
		}

		public virtual bool IsInstance
		{
			get { return m_IsInstance; }
			set { m_IsInstance = value; }
		}

		public bool Dirty
		{
			get { return m_Dirty; }
			set
			{
				m_Dirty = value;

				// If I am dirty, so is my container and the type that contains that, all the way up the chain
				if (m_Dirty)
				{
					if (m_ContainingMember != null)
					{
						// m_ContainingMember.Dirty = true;
						if (m_ContainingMember.GeoTypeThatContainsMe != null)
						{
							m_ContainingMember.GeoTypeThatContainsMe.Dirty = true;
						}
					}
				}
			}
		}

		public bool Deleted
		{
			get { return m_Deleted; }
			set { m_Deleted = value; }
		}

		public string UUID
		{
			get { return m_UUID; }
			set { m_UUID = value; }
		}

		public IGeoType BaseType
		{
			get { return (IGeoType)m_BaseType; }
			set { m_BaseType = (GeoType)value; }
		}

		public String BaseTypeName
		{
			get { return m_BaseTypeName; }
			set { m_BaseTypeName = value.ToLower(); }
		}

		public IGeoTemplate Template
		{
			get { return (IGeoTemplate)m_Template; }
		}

		public Int32 GetChildTypeCount()
		{
			return m_ChildTypes.Count;
		}

		public IGeoType GetChildType(Int32 idx)
		{
			return (IGeoType)m_ChildTypes[idx];
		}

		public void AddChildType(IGeoType type)
		{
			m_ChildTypes.Add((GeoType)type);
		}

		public Int32 GetMemberCount()
		{
			return m_Members.Count;
		}

		public List<GeoTypeMemberBase> Members
		{
			get
			{
				return m_Members;
			}
		}

		public IGeoTypeMemberBase GetMemberByIndex(Int32 idx)
		{
			return (IGeoTypeMemberBase)m_Members[idx];
		}

		public GeoTypeMemberBase GetIMemberByIndex(Int32 idx)
		{
			return m_Members[idx];
		}

		public IGeoTypeMemberBase GetMemberByName(string name)
		{
			foreach (GeoTypeMemberBase m in m_Members)
			{
				if (m.Name == name)
					return (IGeoTypeMemberBase)m;
			}
			return null;
		}

		public IGeoTypeMemberBase GetMemberByDisplayName(string name)
		{
			foreach (GeoTypeMemberBase m in m_Members)
			{
                if (m.GetType() == typeof(GeoTypeMemberGeo))
                {
                    GeoTypeMemberGeo geoMember = m as GeoTypeMemberGeo;
                    if (geoMember.GeoTypeThatIContain != null)
                    {
                        IGeoTypeMemberBase result = (geoMember.GeoTypeThatIContain as GeoType).GetMemberByDisplayName(name);
                        if (null != result)
                        {
                            return result;
                        }                        
                    }
                }

                if (m.DisplayName == name)
                {
                    return (IGeoTypeMemberBase)m;
                }
			}
			return null;
		}

		public IGeoTypeMemberBase GetMemberByUUID(string uuid)
		{
			if (uuid == String.Empty)
				return null;

			foreach (GeoTypeMemberBase m in m_Members)
			{
				if (m.UUID == uuid)
					return m;
			}
			return null;
		}

		public void AddMember(IGeoTypeMemberBase member)
		{
            if (member != null)
            {
                m_Members.Add((GeoTypeMemberBase)member);
            }
		}

		public IGeoTypeMemberGeo ContainingMember
		{
			get { return m_ContainingMember; }
			set { m_ContainingMember = (GeoTypeMemberGeo)value; }
		}

		public void Save()
		{
            if (null == m_FileName)
            {
                FolderBrowserDialog browser = new FolderBrowserDialog();
                DialogResult result = browser.ShowDialog();
                if (result == DialogResult.OK)
                {
                    m_FileName = browser.SelectedPath + "\\" + Name;
                }
                else
                {
                    return;
                }
            }

			SaveAs(m_FileName);
		}

		public void SaveAs(string filename)
		{
			XmlDocument doc = new XmlDocument();

			string rootName;
			if (IsInstance)
			{
				rootName = m_Template.InstanceElementName;
			}
			else
			{
				rootName = m_Template.TypeElementName;
			}

			XmlElement root = doc.CreateElement(rootName);
			doc.AppendChild(root);

			root.SetAttribute("template", m_Template.FullName);

			if (m_BaseType != null)
			{
				root.SetAttribute("base", m_BaseType.Name);
			}
			root.SetAttribute("name", Name);

			if (m_UUID != String.Empty)
			{
				root.SetAttribute("uuid", m_UUID);
			}

			Save(doc, root);

			doc.Save(filename);

			// Just saved, so no longer dirty
			Dirty = false;
		}

		public class SetInheritedVisitor : GeoTypeVisitor
		{
			public override void VisitMember(GeoTypeMemberBase m)
			{
				m.m_UseInheritedValue = true;
				base.VisitMember(m);
			}
		}

		public void SetAllMembersAsInherited()
		{
			SetInheritedVisitor visitor = new SetInheritedVisitor();
			visitor.Visit(this);
		}

		public void OpenInEditor()
		{
			// Write out all the file associated with this Geo to a temporary location....
			/*            List<GeoTemplate> templates;
						List<GeoType> types;
						GeoDataSet.CurrentDataSet.FindMinimalDataSet(this, out templates, out types);
            
						// Make a temp directory (hardcoded to C:\temp for the moment)
						string tempDataSetName = (Environment.GetEnvironmentVariable("TEMP") + "/GeoDataSet_" + rageFileUtilities.GenerateTimeBasedFilename());
						rageFileUtilities.MakeDir(tempDataSetName);

						string templateDirName = tempDataSetName + "/GeoTemplates";
						rageFileUtilities.MakeDir(templateDirName);
						foreach(GeoTemplate template in templates)
						{
							string fileName = System.IO.Path.GetFileName(template.FileName);
							template.Save(templateDirName + "/" + fileName);
						}

						string typeDirName = tempDataSetName + "/GeoTypes";
						rageFileUtilities.MakeDir(typeDirName);
						foreach(GeoType type in types)
						{
							if (type != this)
							{
								string fileName = System.IO.Path.GetFileName(type.FileName);
								type.SaveAs(typeDirName + "/" + fileName);
							}
						}

						rageFileUtilities.MakeDir(tempDataSetName + "/GeoInstances");
						string instanceFileName = tempDataSetName + "/GeoInstances/instance.geo";
						this.SaveAs(instanceFileName);
						*/
			string instanceFileName = (Environment.GetEnvironmentVariable("TEMP") + "/GeoInstance_" + rageFileUtilities.GenerateTimeBasedFilename() + ".geo");
			this.SaveAs(instanceFileName);

			// Load the GEO editor, using these temp files and open with this instance shown....
			// Execute the command
			rageExecuteCommand obCommand = new rageExecuteCommand();
			rageStatus obStatus;
			obCommand.Command = "GeoEdit";
			obCommand.Arguments += " -notemplateview";
			obCommand.Arguments += " -showinstances=" + instanceFileName;
			//        obCommand.Arguments += " -typedir=" + typeDirName;
			//      obCommand.Arguments += " -templatedir=" + templateDirName;
			obCommand.Arguments += " " + instanceFileName;
			obCommand.UpdateLogFileInRealTime = false;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = false;
			obCommand.EchoToConsole = true;
			obCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened
				return;
			}

			// Reload the temp files back into my data set....

			XmlDocument doc = new XmlDocument();
			try
			{
				doc.Load(instanceFileName);
			}
			catch (XmlException ex)
			{
				MessageBox.Show("Error loading XML data from " + instanceFileName + "\nError was " + ex.Message, "XML read error");
				return;
			}

			if (doc.DocumentElement != null)
			{
				this.Load(doc.DocumentElement);
			}

			// Mark it all dirty, so any changes are saved out
			this.Dirty = true;

			//            rageFileUtilities.DeleteLocalFolder(tempDataSetName, out obStatus);
			rageFileUtilities.DeleteLocalFile(instanceFileName, out obStatus);
		}

		#endregion

        public delegate string MissingTemplateHandler( string filename, string template, Dictionary<string, GeoTemplate > kvps );
        static public MissingTemplateHandler OnMissingTemplate = null;

        public delegate string MissingBaseHandler(string filename, string template, Dictionary<string, GeoType>.KeyCollection keys);
        static public MissingBaseHandler OnMissingBase = null;

		static public GeoType Load(String filename)
		{
			XmlDocument doc = new XmlDocument();

			try
			{
				doc.Load(filename);
			}
			catch (XmlException ex)
			{
				MessageBox.Show(ex.Message, "Error loading " + filename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}

			XmlElement root = doc.DocumentElement;

			if (root == null)
			{
				MessageBox.Show("Type file " + filename + " was empty", "Error loading " + filename, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return null;
			}

            String templateName = root.GetAttribute("template").ToLower();


			// make sure this template exists (so we can create members)
			GeoTemplate template = null;
			GeoDataSet.CurrentDataSet.m_GeoTemplates.TryGetValue(templateName, out template);
			if (template == null)
			{
                if (OnMissingTemplate != null)
                {
                    templateName = OnMissingTemplate( filename, templateName, GeoDataSet.CurrentDataSet.m_GeoTemplates );
                    GeoDataSet.CurrentDataSet.m_GeoTemplates.TryGetValue(templateName, out template);
                }

                if (template == null)
                {
                    MessageBox.Show(
                        "Could not find GeoTemplate named " + templateName + " used in GeoType/GeoInstance " + filename, 
                        "Error loading " + filename, 
                        MessageBoxButtons.OK, 
                        MessageBoxIcon.Error
                    );
                    return null;
                }
			}

			GeoType t = template.CreateTypeInternal(true);
			t.Name = root.GetAttribute("name");
			t.m_UUID = root.GetAttribute("uuid");
			t.m_BaseTypeName = root.GetAttribute("base");
			t.m_DccFile = root.GetAttribute("dccFile");
			//t.SortMembersByTypeThenName();

			// we'll add the t.BaseType reference once all types are loaded.
			t.Load(root);

			return t;
		}

		public void Load(XmlElement root)
		{
			foreach (GeoTypeMemberBase member in m_Members)
			{
                if (member != null)
                {
                    member.Load(root);
                }
			}
		}

		public void FinishLoad()
		{
			UpdateBaseTypeFromName();
			foreach (GeoTypeMemberBase member in m_Members)
			{
                if (member != null)
                {
                    member.FinishLoad();
                }
			}
		}

		private void UpdateBaseTypeFromName()
		{
			DisconnectBaseType();

			if (m_BaseTypeName != null && m_BaseTypeName != "")
			{
				GeoDataSet.CurrentDataSet.m_GeoTypes.TryGetValue(m_BaseTypeName, out m_BaseType);
				if (m_BaseType == null)
				{
                    if (OnMissingBase != null)
                    {
                        m_BaseTypeName = OnMissingBase(this.FileName, m_BaseTypeName, GeoDataSet.CurrentDataSet.m_GeoTypes.Keys);
                        GeoDataSet.CurrentDataSet.m_GeoTypes.TryGetValue(m_BaseTypeName, out m_BaseType);
                    }

                    if (m_BaseType == null)
                    {
                        MessageBox.Show("Could not find GeoType named " + m_BaseType + " used in GeoType/GeoInstance " + Name, "Error loading", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
				}
			}
		}

		private void DisconnectBaseType()
		{
			if (m_BaseType != null)
			{
				m_BaseType.m_ChildTypes.Remove(this);
				m_BaseType = null;
			}
		}

		public void DeepCopyFrom(GeoType t)
		{
			// Note: Doesn't copy the ChildTypes list
			m_IsInstance = t.m_IsInstance;
			m_Template = t.m_Template;
			m_BaseType = t.m_BaseType;
			m_UUID = t.m_UUID;

			m_Members.Clear();
			foreach (GeoTypeMemberBase member in t.m_Members)
			{
				m_Members.Add(member.Clone());
			}
		}

		public void Save(XmlDocument doc, XmlElement root)
		{
			foreach (GeoTypeMemberBase m in m_Members)
			{
				if ((m.GetType() == typeof(GeoTypeMemberArray)) ||
					(m.GetType() == typeof(GeoTypeMemberGeo)) ||
					(!m.m_UseInheritedValue))
				{
					m.Save(doc, root);
				}
			}
		}

		public GeoType FindDefiningType(GeoTypeMemberBase m)
		{
			if (m.m_UseInheritedValue)
			{
				if (m_BaseType == null)
				{
					//					Debug.WriteLine(String.Format("Could not find definer for {0}. Creating new value.", m.Name));
					//					m.UseInheritedValue = false;
					//					return this;
					return null;
				}
				foreach (GeoTypeMemberBase baseMember in m_BaseType.m_Members)
				{
					if (baseMember.m_TemplateMemberBase == m.m_TemplateMemberBase)
					{
						return m_BaseType.FindDefiningType(baseMember);
					}
				}
			}
			return this;
		}

		public GeoTypeMemberBase FindDefiningMember(GeoTypeMemberBase m)
		{
			if (m.m_UseInheritedValue)
			{
				GeoType topMost = UltimateContainerType(true);
				Debug.Assert(topMost != null);

				if (topMost.m_BaseType != null)
				{
					string path = m.FindPathToMember(false, true);
					path = path.Substring(path.IndexOf('/') + 1);
					GeoTypeMemberBase memberInBaseType = SearchForMemberVisitor.Search(topMost.m_BaseType, path);

					Debug.Assert(memberInBaseType != null, String.Format("GeoType::FindDefiningMember 'memberInBaseType == null', GeoType {0}", this.Name));

					return memberInBaseType;
				}
			}
			return m;
		}

		public GeoTypeMemberBase FindMember(String path)
		{
			// TODO: Make this work with nested members
			foreach (GeoTypeMemberBase member in m_Members)
			{
				if (member.m_TemplateMemberBase.Name == path)
				{
					return member;
				}
			}
			return null;
		}

		public int GetNumberOfChildInstances()
		{
			int iReturnMe = 0;
			foreach (GeoType obChildType in m_ChildTypes)
			{
				if (obChildType.IsInstance)
				{
					iReturnMe++;
				}
				iReturnMe += obChildType.GetNumberOfChildInstances();
			}
			return iReturnMe;
		}

		public int GetNumberOfChildTypes()
		{
			int iReturnMe = 0;
			foreach (GeoType obChildType in m_ChildTypes)
			{
				if (!obChildType.IsInstance)
				{
					iReturnMe++;
				}
				iReturnMe += obChildType.GetNumberOfChildTypes();
			}
			return iReturnMe;
		}

		public class SearchForMemberVisitor : GeoTypeVisitor
		{
			public static GeoTypeMemberBase Search(GeoType t, string path)
			{
				SearchForMemberVisitor visitor = new SearchForMemberVisitor();
				String delim = "/";
				visitor.m_PathComponents = new List<String>(path.Split(delim.ToCharArray()));
				visitor.Visit(t);
				return visitor.m_FoundMember;
			}

			public override bool BeginGeoMember(GeoTypeMemberGeo member)
			{
				if (member.Name == CurrentPathComponent)
				{
					if (HasNextPathComponent)
					{
						// move to next path component
						m_PathIndex++;
						return true;
					}
				}
				return false;
			}

			public override void VisitMember(GeoTypeMemberBase m)
			{
				base.VisitMember(m);

				if (m.Name == CurrentPathComponent && !HasNextPathComponent)
				{
					m_FoundMember = m;
					return;
				}

			}

			public override bool BeginArrayMember(GeoTypeMemberArray member)
			{
				String delim = "[]";
				List<String> itemComponents = new List<String>(CurrentPathComponent.Split(delim.ToCharArray()));
				if (itemComponents.Count > 1)
				{
					String name = itemComponents[0];
					Int32 index = Int32.Parse(itemComponents[1]);
					if (member.Name == name)
					{
						if (index < member.ChildMembers.Count)
						{
							m_PathIndex++;
							VisitMember(member.ChildMembers[index]);
						}
					}
				}
				return false;
			}

			private GeoTypeMemberBase m_FoundMember;
			private List<string> m_PathComponents;
			private int m_PathIndex;
			string CurrentPathComponent
			{
				get
				{
					return m_PathComponents[m_PathIndex];
				}
			}
			bool HasNextPathComponent
			{
				get
				{
					return m_PathIndex < m_PathComponents.Count - 1;
				}
			}
		}

		public GeoType UltimateContainerType(bool bStopAtFirstArray)
		{
			if (m_ContainingMember == null || m_ContainingMember.GeoTypeThatContainsMe == null)
			{
				return this;
			}
			else
			{
				if (bStopAtFirstArray)
				{
					if (m_ContainingMember.ContainingTypeMember is GeoTypeMemberArray)
					{
						return this;
					}
					else
					{
						return (m_ContainingMember.GeoTypeThatContainsMe as GeoType).UltimateContainerType(bStopAtFirstArray);
					}
				}
				else
					return (m_ContainingMember.GeoTypeThatContainsMe as GeoType).UltimateContainerType(bStopAtFirstArray);
			}
		}

		protected class MemberAlphaComparer : IComparer<GeoTypeMemberBase>
		{
			public int Compare(GeoTypeMemberBase x, GeoTypeMemberBase y)
			{
				return x.m_TemplateMemberBase.Name.CompareTo(y.m_TemplateMemberBase.Name);
			}
		}

		protected class MemberTypeComparer : IComparer<GeoTypeMemberBase>
		{
			public int Compare(GeoTypeMemberBase x, GeoTypeMemberBase y)
			{
				int result = x.m_TemplateMemberBase.GetType().Name.CompareTo(y.m_TemplateMemberBase.GetType().Name);
				if (result != 0)
				{
					return result;
				}
				else
				{
					return x.m_TemplateMemberBase.Name.CompareTo(y.m_TemplateMemberBase.Name);
				}
			}
		}

		protected class MemberTemplateComparer : IComparer<GeoTypeMemberBase>
		{
			public int Compare(GeoTypeMemberBase x, GeoTypeMemberBase y)
			{
				int result = x.m_TemplateMemberBase.m_Template.Name.CompareTo(y.m_TemplateMemberBase.m_Template.Name);
				if (result != 0)
				{
					return result;
				}
				else
				{
					return x.m_TemplateMemberBase.Name.CompareTo(y.m_TemplateMemberBase.Name);
				}
			}
		}

		public void SortMembersByName()
		{
			m_Members.Sort(new MemberAlphaComparer());
		}

		public void SortMembersByTypeThenName()
		{
			m_Members.Sort(new MemberTypeComparer());
		}

		public void SortMembersByTemplateThenName()
		{
			m_Members.Sort(new MemberTemplateComparer());
		}
	}
}
