using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace rageGEODataSet
{
    #region GeoTemplateMemberBool
    [ComVisible(true)]
    [Guid("89B24566-2FD0-4adb-B83A-AD8992B8F0AC")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberBool
    {
        bool Default { get; set; }
    }

    [ComVisible(true)]
    [Guid("5BB0253F-FF33-48c8-AE5E-32760CA3A368")]
    [ProgId("rageGEODataSet.GeoTemplateMemberBool")]
    [GeoMemberMetadata(ValueType = true, Numeric = false, Composite = false)]
    public class GeoTemplateMemberBool : GeoTemplateMemberBase, IGeoTemplateMemberBool
    {
        #region Data Members

        private bool m_Default = false;

        #endregion

        #region Properties

        [XmlAttribute("init"), DefaultValue(false)]
        public bool Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        #endregion

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberBool(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Bool;
        }
    }
    #endregion

    #region GeoTemplateMemberInt
    [ComVisible(true)]
    [Guid("1D9EF7AC-ACB2-4c97-B90E-9315BD8D1458")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberInt
    {
        Int32 Default { get; set;}
        Int32 Min { get; set;}
        Int32 Max { get; set;}
        Int32 Step { get; set;} 
    }

    [ComVisible(true)]
    [Guid("2F31D195-DBBE-476c-8BA9-4B8D5B2D5E9F")]
    [ProgId("rageGEODataSet.GeoTemplateMemberInt")]
    [GeoMemberMetadata(ValueType = true, Numeric = true, Composite = false)]
    public class GeoTemplateMemberInt : GeoTemplateMemberBase, IGeoTemplateMemberInt
    {
        [XmlType("GeoTemplateMemberInt_Storage")]
        public enum StorageType
        {
            [XmlEnum("u8")]
            U8,
            [XmlEnum("s8")]
            S8,
            [XmlEnum("u16")]
            U16,
            [XmlEnum("s16")]
            S16,
            [XmlEnum("u32")]
            U32,
            [XmlEnum("s32")]
            S32
        };

        #region Data Members

        [XmlAttribute("storage"), DefaultValue(StorageType.S32)]
        public StorageType Storage = StorageType.S32;
        private Int32 m_Default = 0;
        private Int32 m_Min = Int32.MinValue;
        private Int32 m_Max = Int32.MaxValue;
        private Int32 m_Step = 1;

        #endregion

        #region Properties

        [XmlAttribute("init"), DefaultValue(0)]
        public Int32 Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        [XmlAttribute("min"), DefaultValue(Int32.MinValue)]
        public Int32 Min
        {
            get { return m_Min; }
            set { m_Min = value; }
        }

        [XmlAttribute("max"), DefaultValue(Int32.MaxValue)]
        public Int32 Max
        {
            get { return m_Max; }
            set { m_Max = value; }
        }

        [XmlAttribute("step"), DefaultValue(1)]
        public Int32 Step
        {
            get { return m_Step; }
            set { m_Step = value; }
        }

        #endregion

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberInt(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Int;
        }
    }
    #endregion

    #region GeoTemplateMemberFloat

    [ComVisible(true)]
    [Guid("49638396-859E-42c8-883F-2A95E34373C9")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberFloat
    {
        double Default { get; set; }
        double Min { get; set; }
        double Max { get; set; }
        double Step { get; set; }
    }

    [ComVisible(true)]
    [Guid("B27CBE50-4E8D-43d0-9423-C721A144807C")]
    [ProgId("rageGEODataSet.GeoTemplateMemberFloat")]
    [GeoMemberMetadata(ValueType = true, Numeric = true, Composite = false)]
    public class GeoTemplateMemberFloat : GeoTemplateMemberBase, IGeoTemplateMemberFloat
    {
        [XmlType("GeoTemplateMemberFloat_Storage")]
        public enum StorageType
        {
            [XmlEnum("float")]
            Float32,
            [XmlEnum("double")]
            Float64,
            // add fixed point formats here?
        };

        #region Data Members
        
        [XmlAttribute("storage"), DefaultValue(StorageType.Float32)]
        public StorageType Storage = StorageType.Float32;
        
        private double m_Default = 0.0f;
        private double m_Min = Single.MinValue;
        private double m_Max = Single.MaxValue;
        private double m_Step = 0.01f;

        #endregion

        #region Properties

        [XmlAttribute("init"), DefaultValue(0.0f)]
        public double Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        [XmlAttribute("min"), DefaultValue(Single.MinValue)]
        public double Min
        {
            get { return m_Min; }
            set { m_Min = value; }
        }

        [XmlAttribute("max"), DefaultValue(Single.MaxValue)]
        public double Max
        {
            get { return m_Max; }
            set { m_Max = value; }
        }

        [XmlAttribute("step"), DefaultValue(0.01f)]
        public double Step
        {
            get { return m_Step; }
            set { m_Step = value; }
        }

        #endregion

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberFloat(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Float;
        }
    }

    #endregion

    #region GeoTemplateMemberVector2

    [ComVisible(true)]
    [Guid("0257143A-687F-4c94-A4AA-4D4B42F03461")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberVector2
    {
        Vector2 Default { get; set; }
        float   Min { get; set; }
        float   Max { get; set; }
        float   Step { get; set; }
        bool    Normalized { get; }
    }

    [ComVisible(true)]
    [Guid("9E3C00A9-121F-49e5-A0B7-89E0E16B622F")]
    [ProgId("rageGEODataSet.GeoTemplateMemberVector2")]
    [GeoMemberMetadata(ValueType = true, Numeric = true, Composite = true)]
    public class GeoTemplateMemberVector2 : GeoTemplateMemberBase, IGeoTemplateMemberVector2
    {
        [XmlIgnore]
        public Vector2 m_Default = new Vector2(0.0f);
        [XmlIgnore]
        public Vector2 Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        [XmlAttribute("min")]
        public float m_Min = Single.MinValue;
        [XmlIgnore]
        public float Min
        {
            get { return m_Min; }
            set { m_Min = value; }
        }

        [XmlAttribute("max")]
        public float m_Max = Single.MaxValue;
        [XmlIgnore]
        public float Max
        {
            get { return m_Max; }
            set { m_Max = value; }
        }

        [XmlAttribute("step")]
        public float m_Step = 0.01f;
        [XmlIgnore]
        public float Step
        {
            get { return m_Step; }
            set { m_Step = value; }
        }

        [XmlAttribute("normalized"), DefaultValue(false)]
        public bool m_Normalized = false;
        [XmlIgnore]
        public bool Normalized
        {
            get { return m_Normalized; }
        }

        public string[] MemberNames
        {
            get
            {
                return new string[] { "X", "Y" };
            }
        }

        public override void PostLoad()
        {
            // unpack vector default string here.
            base.PostLoad();
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberVector2(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Vector2;
        }
    }

    #endregion

    #region GeoTemplateMemberVector3

    [ComVisible(true)]
    [Guid("F0284323-AAB7-4353-A400-9945A2795230")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberVector3
    {
        Vector3 Default { get; set; }
        float Min { get; set; }
        float Max { get; set; }
        float Step { get; set; }
        bool Normalized { get; }
    }

    [ComVisible(true)]
    [Guid("63347161-4DCD-485e-9DDC-0CC2F743B9A2")]
    [ProgId("rageGEODataSet.GeoTemplateMemberVector3")]
    [GeoMemberMetadata(ValueType = true, Numeric = true, Composite = true)]
    public class GeoTemplateMemberVector3 : GeoTemplateMemberBase, IGeoTemplateMemberVector3
    {
        [XmlElement("init")]
        public Vector3 m_Default = new Vector3(0.0f);
        [XmlIgnore]
        public Vector3 Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        [XmlIgnore]
        public bool DefaultSpecified = false;

        [XmlAttribute("min")]
        public float m_Min = -1000.0f; //Single.MinValue;
        [XmlIgnore]
        public float Min
        {
            get { return m_Min; }
            set { m_Min = value; }
        }

        [XmlAttribute("max")]
        public float m_Max = 1000.0f; //Single.MaxValue;
        [XmlIgnore]
        public float Max
        {
            get { return m_Max; }
            set { m_Max = value; }
        }

        [XmlAttribute("step")]
        public float m_Step = 0.01f;
        [XmlIgnore]
        public float Step
        {
            get { return m_Step; }
            set { m_Step = value; }
        }

        [XmlAttribute("normalized"), DefaultValue(false)]
        public bool m_Normalized = false;
        [XmlIgnore]
        public bool Normalized
        {
            get { return m_Normalized; }
            set { m_Normalized = value; }
        }

        public string[] MemberNames
        {
            get
            {
                return new string[] { "X", "Y", "Z" };
            }
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberVector3(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Vector3;
        }
    }

    #endregion

    #region GeoTemplateMemberVector4

    [ComVisible(true)]
    [Guid("2209B633-7046-4996-9C11-6156F11F7A55")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberVector4
    {
        Vector4 Default { get; set; }
        float Min { get; set; }
        float Max { get; set; }
        float Step { get; set; }
        bool Normalized { get; }
    }

    [ComVisible(true)]
    [Guid("477CB1E8-79E0-4504-B099-C9873F6A2EC5")]
    [ProgId("rageGEODataSet.GeoTemplateMemberVector4")]
    [GeoMemberMetadata(ValueType = true, Numeric = true, Composite = true)]
    public class GeoTemplateMemberVector4 : GeoTemplateMemberBase, IGeoTemplateMemberVector4
    {
        [XmlElement("init")]
        public Vector4 m_Default = new Vector4(0.0f);
        [XmlIgnore]
        public Vector4 Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        [XmlAttribute("min")]
        public float m_Min = Single.MinValue;
        [XmlIgnore]
        public float Min
        {
            get { return m_Min; }
            set { m_Min = value; }
        }

        [XmlAttribute("max")]
        public float m_Max = Single.MaxValue;
        [XmlIgnore]
        public float Max
        {
            get { return m_Max; }
            set { m_Max = value; }
        }

        [XmlAttribute("step")]
        public float m_Step = 0.01f;
        [XmlIgnore]
        public float Step
        {
            get { return m_Step; }
            set { m_Step = value; }
        }

        [XmlAttribute("normalized"), DefaultValue(false)]
        public bool m_Normalized = false;
        [XmlIgnore]
        public bool Normalized
        {
            get { return m_Normalized; }
            set { m_Normalized = value; }
        }

        public string[] MemberNames
        {
            get
            {
                return new string[] { "X", "Y", "Z", "W" };
            }
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberVector4(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Vector4;
        }
    }

    #endregion

    #region GeoTemplateMemberMatrix34

    [ComVisible(true)]
    [Guid("1101BF4E-49D3-443f-82BB-9040E613BA53")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberMatrix34
    {
        Matrix34 Default { get; set; }
    }

    [ComVisible(true)]
    [Guid("F7AA8149-B06A-494b-8E0B-7B5C9EF6905D")]
    [ProgId("rageGEODataSet.GeoTemplateMemberMatrix34")]
    [GeoMemberMetadata(ValueType = true, Numeric = true, Composite = true)]
    public class GeoTemplateMemberMatrix34 : GeoTemplateMemberBase, IGeoTemplateMemberMatrix34
    {
        [XmlElement("init")]
        public Matrix34 m_Default = Matrix34.Identity;
        [XmlIgnore]
        public Matrix34 Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        public string[] MemberNames
        {
            get
            {
                return new string[] { "AX", "AY", "AZ", "BX", "BY", "BZ", "CX", "CY", "CZ", "DX", "DY", "DZ" };
            }
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberMatrix34(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Matrix34;
        }
    }

    #endregion

    #region GeoTemplateMemberMatrix44

    [ComVisible(true)]
    [Guid("4F5F5C21-3299-4cc4-8C49-985BBFDBE681")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberMatrix44 
    {
        Matrix44 Default { get; set; }
    }

    [ComVisible(true)]
    [Guid("98C8308E-9A61-4f4a-8432-4443296364BB")]
    [ProgId("rageGEODataSet.GeoTemplateMemberMatrix44")]
    [GeoMemberMetadata(ValueType = true, Numeric = true, Composite = true)]
    public class GeoTemplateMemberMatrix44 : GeoTemplateMemberBase, IGeoTemplateMemberMatrix44
    {
        [XmlElement]
        public Matrix44 m_Default = Matrix44.Identity;
        [XmlIgnore]
        public Matrix44 Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        public string[] MemberNames
        {
            get
            {
                return new string[] { "AX", "AY", "AZ", "AW", "BX", "BY", "BZ", "BW", "CX", "CY", "CZ", "CW", "DX", "DY", "DZ", "DW" };
            }
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberMatrix44(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Matrix44;
        }
    }

    #endregion

    #region GeoTemplateMemberContainer
   
    [ComVisible(true)]
    [Guid("4BBA99B0-A933-4649-822D-243207CAE1BC")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberContainer
    {
        Int32 GetChildCount();
        IGeoTemplateMemberBase GetChild(Int32 idx);
    }
      
    [ComVisible(false)]
    [GeoMemberMetadata(ValueType = true, Numeric = false, Composite = false)]
    public abstract class GeoTemplateMemberContainer : GeoTemplateMemberBase
    {
        [
        XmlElement("bool", typeof(GeoTemplateMemberBool)),
        XmlElement("int", typeof(GeoTemplateMemberInt)),
        XmlElement("float", typeof(GeoTemplateMemberFloat)),
        XmlElement("Vector2", typeof(GeoTemplateMemberVector2)),
        XmlElement("Vector3", typeof(GeoTemplateMemberVector3)),
        XmlElement("Vector4", typeof(GeoTemplateMemberVector4)),
        XmlElement("Matrix34", typeof(GeoTemplateMemberMatrix34)),
        XmlElement("Matrix44", typeof(GeoTemplateMemberMatrix44)),
        XmlElement("string", typeof(GeoTemplateMemberString)),
        XmlElement("geo", typeof(GeoTemplateMemberGeo)),
        XmlElement("file", typeof(GeoTemplateMemberFile)),
        XmlElement("node", typeof(GeoTemplateMemberNode)),
        XmlElement("array", typeof(GeoTemplateMemberArray)),
        XmlElement("group", typeof(GeoTemplateMemberGroup)),
        XmlElement("enum", typeof(GeoTemplateMemberEnum)),
        ]
        public List<GeoTemplateMemberBase> Children = new List<GeoTemplateMemberBase>();

        public string[] MemberNames
        {
            get
            {
                string[] strings = new string[Children.Count];
                for (int i = 0; i < Children.Count; i++)
                {
                    strings[i] = (Children[i] as GeoTemplateMemberBase).Name;
                }
                return strings;
            }
        }

        public override void PostLoad()
        {
            base.PostLoad();

            foreach (GeoTemplateMemberBase member in Children)
            {
                member.PostLoad();
            }
        }

        public override void PreSave()
        {
            base.PreSave();

            foreach (GeoTemplateMemberBase member in Children)
            {
                member.PreSave();
            }
        }
    }

    #endregion

    #region GeoTemplateMemberArray

    [ComVisible(true)]
    [Guid("82AD8E85-575B-4480-A917-1D33F5A634B1")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberArray
    {
        int MinCount { get; set; }
        int MaxCount { get; set; }
        IGeoTemplateMemberBase ChildTemplate { get; }
    }

    [ComVisible(true)]
    [Guid("82309737-CA85-41ff-9A94-EE90CF939F10")]
    [ProgId("rageGEODataSet.GeoTemplateMemberArray")]
    [GeoMemberMetadata(ValueType = false, Numeric = false, Composite = true)]
    public class GeoTemplateMemberArray : GeoTemplateMemberContainer, IGeoTemplateMemberContainer, IGeoTemplateMemberArray
    {
        public GeoTemplateMemberArray()
        {
        }

        public Int32 GetChildCount()
        {
            return Children.Count;
        }

        public IGeoTemplateMemberBase GetChild(Int32 idx)
        {
            return (IGeoTemplateMemberBase)Children[idx];
        }

        [XmlAttribute("minCount"), DefaultValue(0)]
        public int m_MinCount = 0;
        [XmlIgnore]
        public int MinCount
        {
            get { return m_MinCount; }
            set { m_MinCount = value; }
        }

        [XmlAttribute("maxCount"), DefaultValue(Int32.MaxValue)]
        public int m_MaxCount = Int32.MaxValue;
        [XmlIgnore]
        public int MaxCount
        {
            get { return m_MaxCount; }
            set { m_MaxCount = value; }
        }

        public override void PostLoad()
        {
            base.PostLoad();
            Debug.Assert(Children.Count == 1);
        }

        public override void PreSave()
        {
            base.PreSave();
            Debug.Assert(Children.Count == 1);
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberArray();
        }

        public IGeoTemplateMemberBase ChildTemplate
        {
            get
            {
                Debug.Assert(Children.Count == 1);
                return Children[0] as IGeoTemplateMemberBase;
            }
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Array;
        }
    }

    #endregion

    #region GeoTemplateMemberString

    [ComVisible(true)]
    [Guid("E960D4BC-2DB6-4992-BE7F-727E22217B55")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberString
    {
       string Default { get; set; }
    }

    [ComVisible(true)]
    [Guid("8CBEE78F-3083-4e16-9ED3-BE1CF5CF9B67")]
    [ProgId("rageGEODataSet.GeoTemplateMemberString")]
    [GeoMemberMetadata(ValueType = false, Numeric = false, Composite = false)]
    public class GeoTemplateMemberString : GeoTemplateMemberBase, IGeoTemplateMemberString
    {
		[XmlAttribute("init"), DefaultValue("")]
        public string m_Default = "";
        [XmlIgnore]
        public string Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberString(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.String;
        }
    }

    #endregion

    #region GeoTemplateMemberGeo

    [ComVisible(true)]
    [Guid("CD34FFD7-2912-45d0-A19C-D3873AAB0E73")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberGeo
    {
        string TemplateName { get; set; }
        bool CanDerive { get; set; }
        bool Required { get; set; }
        GeoTemplate ChildTemplate { get; set; }
    }

    [ComVisible(true)]
    [Guid("D7719362-9345-4930-AF52-B740F296EA8B")]
    [ProgId("rageGEODataSet.GeoTemplateMemberGeo")]
    [GeoMemberMetadata(ValueType = false, Numeric = false, Composite = false)]
    public class GeoTemplateMemberGeo : GeoTemplateMemberBase, IGeoTemplateMemberGeo
    {
        public GeoTemplateMemberGeo()
        {
        }

        [XmlAttribute("template")]
        public string m_TemplateName = "";
        [XmlIgnore]
        public string TemplateName
        {
            get { return m_TemplateName; }
            set { m_TemplateName = value.ToLower(); }
        }

        [XmlAttribute("canDerive")]
        public bool m_CanDerive = true;
        [XmlIgnore]
        public bool CanDerive
        {
            get { return m_CanDerive; }
            set { m_CanDerive = value; }
        }

        [XmlAttribute("required")]
        public bool m_Required = true;
        [XmlIgnore]
        public bool Required
        {
            get { return m_Required; }
            set { m_Required = value; }
        }

        [XmlIgnore]
        public GeoTemplate ChildTemplate
        {
            get
            {
                if (m_ChildTemplate == null)
                {
					GeoDataSet.CurrentDataSet.m_GeoTemplates.TryGetValue(TemplateName, out m_ChildTemplate);
					if (m_ChildTemplate == null)
					{
						MessageBox.Show("Could not find GeoTemplate named " + TemplateName + " required by used in GeoTemplateMemberGeo " + Name, "Error loading", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return null;
					}
                }
                return m_ChildTemplate;
            }
            set
            {
                m_ChildTemplate = value;
            }
        }

        private GeoTemplate m_ChildTemplate;

        public override void PostLoad()
        {
            base.PostLoad();

            m_TemplateName = m_TemplateName.ToLower();

            GeoTemplate cTmpl = null;
            if (GeoDataSet.CurrentDataSet.m_GeoTemplates.TryGetValue(TemplateName, out cTmpl))
                ChildTemplate = cTmpl;
        }

        public override void PreSave()
        {
            base.PreSave();

            TemplateName = ChildTemplate.FullName;
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            GeoTypeMemberGeo member = new GeoTypeMemberGeo();

            if (Required && !CanDerive)
            {
                if (ChildTemplate == null)
                {
                    System.Windows.Forms.MessageBox.Show(
                        String.Format("Warning: Could not find a geotemplate named '{0}' while loading {1}", TemplateName, t.FileName),
                        "Could not find geotemplate",
                        System.Windows.Forms.MessageBoxButtons.OK,
                        System.Windows.Forms.MessageBoxIcon.Warning);
                    return null;
                }
                member.GeoTypeThatIContain = ChildTemplate.CreateTypeInternal(false);
                Debug.Assert(member.GeoTypeThatIContain != null);
                member.GeoTypeThatIContain.ContainingMember = member;
                //TODO : Set UUIDs for the reference type members...
            }

            return member;
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Geo;
        }
    }

    #endregion

    #region GeoTemplateMemberNode

    [ComVisible(true)]
    [Guid("472E4F1D-6971-4e8c-86A0-FE688E14BA95")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberNode
    {
        string NodeType { get; set; }
    }

    [ComVisible(true)]
    [Guid("206AC180-09C0-4129-B148-2CBEFA7578C5")]
    [ProgId("rageGEODataSet.GeoTemplateMemberNode")]
    [GeoMemberMetadata(ValueType = false, Numeric = false, Composite = true)]
    public class GeoTemplateMemberNode : GeoTemplateMemberContainer, IGeoTemplateMemberNode, IGeoTemplateMemberContainer
    {
        [XmlAttribute("type")]
        public string m_NodeType = "";
        [XmlIgnore]
        public string NodeType
        {
            get { return m_NodeType; }
            set { m_NodeType = value; }
        }

        public Int32 GetChildCount()
        {
            return Children.Count;
        }

        public IGeoTemplateMemberBase GetChild(Int32 idx)
        {
            return (IGeoTemplateMemberBase)Children[idx];
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            GeoTypeMemberNode node = new GeoTypeMemberNode();
            foreach (GeoTemplateMemberBase child in Children)
            {
                GeoTypeMemberBase childMember = (GeoTypeMemberBase)child.CreateTypeMember(t);
                childMember.m_ContainingTypeMember = node;
                node.Contents.Add(childMember);
            }
            return node;
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Node;
        }
    }

    #endregion

    #region GeoTemplateMemberFile

    [ComVisible(true)]
    [Guid("E6E009E5-CEC5-41e3-AB5B-A2868E327EDC")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberFile
    {
        string  Default { get; set; }
        string  Extension { get; set; }
        bool    MustExist { get; set; }
        string  Filter { get; set; }
    }

    [ComVisible(true)]
    [Guid("1A8DDCCB-53EE-44a0-955B-7253BB27BE76")]
    [ProgId("rageGEODataSet.GeoTemplateMemberFile")]
    [GeoMemberMetadata(ValueType = false, Numeric = false, Composite = false)]
    public class GeoTemplateMemberFile : GeoTemplateMemberBase, IGeoTemplateMemberFile
    {
        [XmlAttribute("init"), DefaultValue("")]
        public string m_Default = "";
        [XmlIgnore]
        public string Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        [XmlAttribute("extension")]
        public string m_Extension = "";
        [XmlIgnore]
        public string Extension
        {
            get { return m_Extension; }
            set { m_Extension = value; }
        }

        [XmlAttribute("mustExist"), DefaultValue(false)]
        public bool m_MustExist = false;
        [XmlIgnore]
        public bool MustExist
        {
            get { return m_MustExist; }
            set { m_MustExist = value; }
        }

        [XmlAttribute("filter"), DefaultValue("All Files (*.*)|*.*")]
        public string m_Filter;
        [XmlIgnore]
        public string Filter
        {
            get { return m_Filter; }
            set { m_Filter = value; }
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberFile(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.File;
        }
    }

    #endregion
	
	#region GeoTemplateMemberBinary

	[ComVisible(true)]
	[Guid("77F13EE1-AD7F-4d3b-A20C-3A352B0F90F7")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTemplateMemberBinary
	{
		byte[] Default { get; set; }
	}

	[ComVisible(true)]
	[Guid("E85190D8-77DD-4b44-841C-1D97EBE796FB")]
	[ProgId("rageGEODataSet.GeoTemplateMemberBinary")]
	[GeoMemberMetadata(ValueType = false, Numeric = false, Composite = false)]
	public class GeoTemplateMemberBinary : GeoTemplateMemberBase, IGeoTemplateMemberBinary
	{
		[XmlAttribute("init"), DefaultValue("")]
		public byte[] m_Default = null;
		[XmlIgnore]
		public byte[] Default
		{
			get { return m_Default; }
			set { m_Default = value; }
		}

		public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
		{
			return new GeoTypeMemberBinary(Default);
		}

		public override GeoMemberType GetMemberType()
		{
			return GeoMemberType.Binary;
		}
	}

	#endregion

	#region GeoTemplateMemberColor

    [ComVisible(true)]
    [Guid("22135C68-C786-416f-9FD9-7DEAE49D3B42")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberColor
    {
        System.Drawing.Color Default { get; set; }
        System.Drawing.Color Min { get; set; }
        System.Drawing.Color Max { get; set; }
        bool UseAlpha { get; set; }
    }

    [ComVisible(true)]
    [Guid("DCFB1ED5-1CED-4529-A50F-60385CF96BAC")]
    [ProgId("rageGEODataSet.GeoTemplateMemberColor")]
    [GeoMemberMetadata(ValueType = true, Numeric = true, Composite = true)]
    public class GeoTemplateMemberColor : GeoTemplateMemberBase, IGeoTemplateMemberColor
    {
        [XmlType("GeoTemplateMemberColor_Storage")]
        public enum StorageType
        {
            [XmlEnum("Color32")]
            Color32,
            [XmlEnum("Vector3")]
            Vector3,
            [XmlEnum("Vector4")]
            Vector4
        };

        public GeoTemplateMemberColor()
        {
            m_Default = System.Drawing.Color.FromArgb( 255, 0, 0, 0);
            m_Min = System.Drawing.Color.FromArgb(0, 0, 0, 0);
            m_Max = System.Drawing.Color.FromArgb(255, 255, 255, 255);
        }

        [XmlIgnore]
        public System.Drawing.Color m_Default = System.Drawing.Color.Empty;//(0.0f, 0.0f, 0.0f, 1.0f);
        [XmlIgnore]
        public System.Drawing.Color Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        [XmlIgnore]
        public System.Drawing.Color m_Min = System.Drawing.Color.Empty;//(0.0f, 0.0f, 0.0f, 0.0f);
        [XmlIgnore]
        public System.Drawing.Color Min
        {
            get { return m_Min; }
            set { m_Min = value; }
        }

        [XmlIgnore]
        public System.Drawing.Color m_Max = System.Drawing.Color.Empty;//(1.0f, 1.0f, 1.0f, 1.0f);
        [XmlIgnore]
        public System.Drawing.Color Max
        {
            get { return m_Max; }
            set { m_Max = value; }
        }

        [XmlAttribute("storage")]
        public StorageType Storage = StorageType.Color32;

        [XmlAttribute("useAlpha")]
        public bool m_UseAlpha = false;
        [XmlIgnore]
        public bool UseAlpha
        {
            get { return m_UseAlpha; }
            set { m_UseAlpha = value; }
        }

        public string[] MemberNames
        {
            get
            {
                if (UseAlpha)
                {
                    return new string[] { "R", "G", "B", "A" };
                }
                else
                {
                    return new string[] { "R", "G", "B" };
                }
            }
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberColor(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Color;
        }
    }

    #endregion

    #region GeoTemplateMemberGroup

    [ComVisible(true)]
    [Guid("A027F2A1-7F87-4705-9E99-0154D402B959")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberGroup
    {
    }

    [ComVisible(true)]
    [Guid("47CB49BE-0189-4092-8F3D-23B159C102F8")]
    [ProgId("rageGEODataSet.GeoTemplateMemberGroup")]
    [GeoMemberMetadata(ValueType = false, Numeric = false, Composite = true)]
    public class GeoTemplateMemberGroup : GeoTemplateMemberContainer, IGeoTemplateMemberGroup
    {
        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            GeoTypeMemberGroup str = new GeoTypeMemberGroup();
            foreach (GeoTemplateMemberBase child in Children)
            {
                GeoTypeMemberBase childMember = (GeoTypeMemberBase)child.CreateTypeMember(t);
                childMember.m_ContainingTypeMember = str;
                str.Contents.Add(childMember);
            }
            return str;
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Group;
        }
    }

    #endregion

    #region GeoTemplateMemberEnum

    [ComVisible(true)]
    [Guid("2CD56E3A-C9BB-4844-9C7B-D4DB7F32E451")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberEnum
    {
        string Choices { get; set; }
        string Default { get; set; }
        int GetNumChoices();
        string GetChoice(int idx);
    }

    [ComVisible(true)]
    [Guid("74879EF6-83B4-4b28-A134-0AF1087008E8")]
    [ProgId("rageGEODataSet.GeoTemplateMemberEnum")]
    [GeoMemberMetadata(ValueType = true, Numeric = false, Composite = false)]
    public class GeoTemplateMemberEnum : GeoTemplateMemberBase, IGeoTemplateMemberEnum
    {
        [XmlElement("Choices")]
        [XmlText()]
        public string m_Choices;
        [XmlIgnore]
        public string Choices
        {
            get { return m_Choices; }
            set { m_Choices = value; }
        }

        [XmlType("GeoTemplateMemberEnum_Storage")]
        public enum StorageType
        {
            [XmlEnum("int")]
            Int,
            [XmlEnum("string")]
            String
        };

        [XmlAttribute("init")]
        public string m_Default;
        [XmlIgnore]
        public string Default
        {
            get { return m_Default; }
            set { m_Default = value; }
        }

        [XmlAttribute("storage")]
        public StorageType Storage = StorageType.String;

        [XmlIgnore]
        public List<String> m_ChoiceArray = new List<String>();

        public int GetNumChoices()
        {
            return m_ChoiceArray.Count;
        }

        public string GetChoice(int idx)
        {
            return m_ChoiceArray[idx].ToString();
        }

        public override void PostLoad()
        {
            base.PostLoad();

            m_ChoiceArray.AddRange(m_Choices.Split(new char[] { '\r', '\n', ',' }));

            // C# needs a String.RemoveAll()
            int i = 0;
            while (i < m_ChoiceArray.Count)
            {
                if ((m_ChoiceArray[i] as string) == String.Empty)
                {
                    m_ChoiceArray.RemoveAt(i);
                }
                else
                {
                    m_ChoiceArray[i].TrimStart(new char[] { '\t', ' ' });
                    m_ChoiceArray[i].TrimEnd(new char[] { '\t', ' ' });
                    i++;
                }
            }

            if (!m_ChoiceArray.Contains(m_Default) && m_ChoiceArray.Count > 0)
            {
                m_Default = m_ChoiceArray[0] as string;
            }
        }

        public override GeoTypeMemberBase CreateTypeMemberInternal(GeoType t)
        {
            return new GeoTypeMemberEnum(Default);
        }

        public override GeoMemberType GetMemberType()
        {
            return GeoMemberType.Enum;
        }
    }

    #endregion

}
