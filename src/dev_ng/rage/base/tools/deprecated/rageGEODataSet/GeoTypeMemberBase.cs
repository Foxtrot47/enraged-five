
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Runtime.InteropServices;

namespace rageGEODataSet
{
	[ComVisible(true)]
	[Guid("79722007-7D77-470a-84A6-C0E3C99C4CB2")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberBase
	{
		string Name { get; set; }
		string DisplayName { get; set; }
		string UUID { get; set; }
		IGeoTemplateMemberBase TemplateMemberBase { get; }
		IGeoTypeMemberBase ContainingTypeMember { get; set; }
		IGeoType GeoTypeThatContainsMe { get; set; }
		bool UseInheritedValue { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("88DC60FB-9E48-487a-B4B5-09AC3DD98C01")]
	[ProgId("rageGEODataSet.GeoTypeMemberBase")]
	public class GeoTypeMemberBase : IGeoTypeMemberBase
	{
		#region Data Members

		public GeoTemplateMemberBase m_TemplateMemberBase;
		public GeoTypeMemberBase m_ContainingTypeMember;
		public GeoType m_GeoTypeThatContainsMe;
		public bool m_UseInheritedValue;

		#endregion

		#region Constructors

		public GeoTypeMemberBase()
		{
		}

		#endregion

		#region IGeoTypeMemberBase Interface

		public string Name
		{
			get
			{
				return m_TemplateMemberBase.Name;
			}
			set
			{
				m_TemplateMemberBase.Name = value.ToLower();
			}
		}

		public string DisplayName
		{
			get
			{
				return m_TemplateMemberBase.DisplayName;
			}
			set
			{
				m_TemplateMemberBase.DisplayName = value;
			}
		}

		public virtual string ValueAsString
		{
			get
			{
				return "ValueAsStringUnavailable";
			}
		}

		public virtual string UUID
		{
			get { return String.Empty; }
			set { }
		}

		public IGeoTemplateMemberBase TemplateMemberBase
		{
			get { return (IGeoTemplateMemberBase)m_TemplateMemberBase; }
			set { m_TemplateMemberBase = (GeoTemplateMemberBase)value; }
		}


		public IGeoTypeMemberBase ContainingTypeMember
		{
			get { return (IGeoTypeMemberBase)m_ContainingTypeMember; }
			set { m_ContainingTypeMember = (GeoTypeMemberBase)value; }
		}

		public IGeoType GeoTypeThatContainsMe
		{
			get { return (IGeoType)m_GeoTypeThatContainsMe; }
			set { m_GeoTypeThatContainsMe = (GeoType)value; }
		}

		public bool UseInheritedValue
		{
			get { return m_UseInheritedValue; }
			set { m_UseInheritedValue = value; }
		}

		#endregion

		public virtual System.Windows.Forms.Control CreateControl()
		{
			return null;
		}

		public virtual void Save(XmlDocument doc, XmlElement parent)
		{
			// do nothing
		}

		public virtual List<GeoTypeMemberBase> ChildMembers
		{
			// Only valid for composite types
			get
			{
				return null;
			}
		}

		public void Load(XmlElement parent)
		{
			m_UseInheritedValue = true;
			foreach (XmlElement elt in parent.ChildNodes)
			{
				if (elt.Name == Name)
				{
					m_UseInheritedValue = false;
					LoadData(elt);
					return;
				}
			}
		}

		public virtual void FinishLoad()
		{
		}

		public virtual void LoadData(XmlElement elt)
		{
		}

		public XmlElement CreateElement(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = doc.CreateElement(Name);
			parent.AppendChild(elt);
			return elt;
		}

		public GeoTypeMemberBase Clone()
		{
			GeoTypeMemberBase newObj = this.MemberwiseClone() as GeoTypeMemberBase;
			DeepCopy(newObj);
			return newObj;
		}

		protected virtual void DeepCopy(GeoTypeMemberBase dest)
		{
		}


		public string FindPathToMember(bool stopAtFirstGeo, bool stopAtFirstArray)
		{
			if (this is GeoTypeMemberGeo && stopAtFirstGeo)
			{
				return String.Empty;
			}
			else if (this is GeoTypeMemberArray && stopAtFirstArray)
			{
				return String.Empty;
			}
			else if (stopAtFirstGeo && GeoTypeThatContainsMe.ContainingMember == null)
			{
				return Name;
			}
			else if (ContainingTypeMember != null)
			{
				if (ContainingTypeMember is GeoTypeMemberArray)
				{
					// find which array element this is
					GeoTypeMemberArray arr = ContainingTypeMember as GeoTypeMemberArray;
					for (int i = 0; i < arr.ChildMembers.Count; i++)
					{
						if (arr.ChildMembers[i] == this)
						{
							return String.Format("{0}[{1}]", arr.FindPathToMember(stopAtFirstGeo, stopAtFirstArray), i);
						}
					}
				}
				return m_ContainingTypeMember.FindPathToMember(stopAtFirstGeo, stopAtFirstArray) + "/" + Name;
			}
			else if (GeoTypeThatContainsMe != null)
			{
				if (GeoTypeThatContainsMe.ContainingMember != null)
				{
					return m_GeoTypeThatContainsMe.m_ContainingMember.FindPathToMember(stopAtFirstGeo, stopAtFirstArray) + "/" + Name;
				}
				else
				{
					return m_GeoTypeThatContainsMe.Name + "/" + Name;
				}
			}
			else
			{
				return Name;
			}
		}

		// How do we figure out which array index to use?
		public void GetTwoPartPath(out string pathToType, out string pathToMember)
		{
			/* Given
			 * <A>
			 *   <B>
			 *     <C type="SomeType">
			 *       <D>
			 *         <E/>
			 * ...
			 * E.GetTwoPartPath should return "A/B/C/D" and "E"
			 */

			pathToType = FindPathToMember(false, false);

			pathToMember = Name;

			pathToType = pathToType.Remove((pathToType.Length - pathToMember.Length) - 1, pathToMember.Length + 1);
		}

		public void MakeDirty()
		{
			GeoTypeThatContainsMe.Dirty = true;
		}
	}
}
