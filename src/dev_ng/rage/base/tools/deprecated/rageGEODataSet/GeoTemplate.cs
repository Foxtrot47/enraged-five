using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.InteropServices;

namespace rageGEODataSet
{
    [ComVisible(true)]
    [Guid("417BED3D-A290-4ddb-8353-AB7A129A1803")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGEOTemplateFileIO
    {
        IGeoTemplate Load(string fileName);
    }

    [ComVisible(true)]
    [Guid("D65770F2-08F5-41d8-A725-4D8F637E05CA")]
    [ProgId("rageGEODataSet.GeoTemplateFileIO")]
    public class GeoTemplateFileIO : IGEOTemplateFileIO
    {
        public GeoTemplateFileIO() { }

        public IGeoTemplate Load(string fileName)
        {
            return (IGeoTemplate)GeoTemplate.Load(fileName);
        }
    }

    [ComVisible(true)]
    [Guid("C85272B3-E63A-45c4-94F0-B10700981F6F")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplate
    {
        string Name { get; set; }
        string FileName { get; set; }
        string FullName { get; }
        string InstanceElementName { get; set; }
		string InstanceFileExtension { get; set; }
		string BaseTemplateName { get; set; }
        string TypeElementName { get; set; }

        Int32 MajorVersion { get; set; }
        Int32 MinorVersion { get; set; }

        Boolean ContainedOnly { get; set; }

        IGeoTemplate GetBaseTemplate();

        Int32                   GetMemberCount();
        IGeoTemplateMemberBase  GetMember(int idx);
        IGeoTemplateMemberBase  GetMemberByName(String name);

        Int32           GetContainedTemplateCount();
        IGeoTemplate    GetContainedTemplate(int idx);

        Int32           GetChildTemplateCount();
        IGeoTemplate    GetChildTemplate(int idx);

        IGeoTemplateMemberBase[] GetInstanceOnlyMembers();

        IGeoType CreateType(bool addToChildList);
    }

    [ComVisible(true)]
    [Guid("9F30C9A2-C17E-4b32-8A13-949229EE8360")]
    [ProgId("rageGEODataSet.GeoTemplate")]
    public class GeoTemplate : GeoItem, IGeoTemplate
    {
        #region Constructors

        public GeoTemplate()
        {
        }

        #endregion

        #region Data Members

        private string      m_Name;
        private string      m_FileName;
        private string      m_BaseTemplateName;
        private string      m_InstanceElementName;
		private string		m_InstanceFileExtension;
        private string      m_TypeElementName;

        private Int32       m_MajorVersion;
        private Int32       m_MinorVersion;

        private Boolean     m_ContainedOnly = false;

        public GeoTemplate  m_BaseTemplate;
        public GeoTemplate  m_ContainingTemplate = null;
        
        [XmlIgnore]
        public List<GeoTemplate>    m_ChildTemplates = new List<GeoTemplate>();
        [XmlIgnore]
        public List<GeoType>        m_ChildTypes = new List<GeoType>();

        [
        XmlArrayItem("GeoTemplate", typeof(GeoTemplate)),
        XmlArray("GeoTemplates")
        ]
        public List<GeoTemplate> m_ContainedTemplates = new List<GeoTemplate>();

        [
        XmlArrayItem("bool", typeof(GeoTemplateMemberBool)),
        XmlArrayItem("int", typeof(GeoTemplateMemberInt)),
        XmlArrayItem("float", typeof(GeoTemplateMemberFloat)),
        XmlArrayItem("Vector2", typeof(GeoTemplateMemberVector2)),
        XmlArrayItem("Vector3", typeof(GeoTemplateMemberVector3)),
        XmlArrayItem("Vector4", typeof(GeoTemplateMemberVector4)),
        XmlArrayItem("Matrix34", typeof(GeoTemplateMemberMatrix34)),
        XmlArrayItem("Matrix44", typeof(GeoTemplateMemberMatrix44)),
        XmlArrayItem("string", typeof(GeoTemplateMemberString)),
        XmlArrayItem("geo", typeof(GeoTemplateMemberGeo)),
        XmlArrayItem("file", typeof(GeoTemplateMemberFile)),
        XmlArrayItem("node", typeof(GeoTemplateMemberNode)),
        XmlArrayItem("array", typeof(GeoTemplateMemberArray)),
        XmlArrayItem("group", typeof(GeoTemplateMemberGroup)),
        XmlArrayItem("enum", typeof(GeoTemplateMemberEnum)),
        XmlArray("Members")
        ]
        public List<GeoTemplateMemberBase> m_Members;

        #endregion

        #region IGeoTemplate Interface

        [XmlAttribute("name")]
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value.ToLower(); }
        }

        [XmlIgnore]
        public string FileName
        {
            get { return m_FileName; }
            set { m_FileName = value.ToLower(); }
        }

        [XmlIgnore]
        public string FullName
        {
            get
            {
                if (m_ContainingTemplate == null)
                {
                    return m_Name;
                }
                else
                {
                    return m_ContainingTemplate.FullName + "/" + m_Name;
                }
            }
        }

        [XmlAttribute("instanceElement")]
        public string InstanceElementName
        {
            get { return m_InstanceElementName; }
            set { m_InstanceElementName = value.ToLower(); }
        }

		[XmlAttribute("instanceFileExtension")]
		public string InstanceFileExtension
		{
			get 
			{
				if ((m_InstanceFileExtension == null) || (m_InstanceFileExtension == ""))
				{
					// Should I recurse?
					if (m_BaseTemplate != null)
					{
						return m_BaseTemplate.InstanceFileExtension;
					}
					else
					{
						return ".geo";
					}
				}
				else
				{
					return m_InstanceFileExtension;
				}
			}
			set { m_InstanceFileExtension = value.ToLower(); }
		}

		[XmlAttribute("base")]
        public string BaseTemplateName
        {
            get { return m_BaseTemplateName; }
            set { m_BaseTemplateName = value.ToLower(); }
        }

        [XmlAttribute("typeElement")]
        public string TypeElementName
        {
            get { return m_TypeElementName; }
            set { m_TypeElementName = value.ToLower(); }
        }

        [XmlAttribute("containedOnly")]
        public Boolean ContainedOnly
        {
            get { return m_ContainedOnly; }
            set { m_ContainedOnly = value; }
        }

        [XmlIgnore]
        public Int32 MajorVersion
        {
            get { return m_MajorVersion; }
            set { m_MajorVersion = value; }
        }

        [XmlIgnore]
        public Int32 MinorVersion
        {
            get { return m_MinorVersion; }
            set { m_MinorVersion = value; }
        }

        public IGeoTemplate GetBaseTemplate()
        {
            return (IGeoTemplate)m_BaseTemplate;
        }

        public int GetMemberCount()
        {
            return m_Members.Count;
        }

        public IGeoTemplateMemberBase GetMember(int idx)
        {
            return (IGeoTemplateMemberBase)m_Members[idx];
        }

        public IGeoTemplateMemberBase GetMemberByName(String name)
        {
            foreach (GeoTemplateMemberBase tmplBase in m_Members)
            {
                if (tmplBase.Name == name)
                    return (IGeoTemplateMemberBase)tmplBase;
            }
            return null;
        }

        public Int32 GetContainedTemplateCount()
        {
            return m_ContainedTemplates.Count;
        }

        public IGeoTemplate GetContainedTemplate(int idx)
        {
            return (IGeoTemplate)m_ContainedTemplates[idx];
        }

        public Int32 GetChildTemplateCount()
        {
            return m_ChildTemplates.Count;
        }

        public IGeoTemplate GetChildTemplate(int idx)
        {
            return (IGeoTemplate)m_ChildTemplates[idx];
        }

        public IGeoTemplateMemberBase[] GetInstanceOnlyMembers()
        {
            List<GeoTemplateMemberBase> instMembersAL = new List<GeoTemplateMemberBase>();

            GeoTemplate curTemplate = this;
            while (curTemplate != null)
            {
                foreach (GeoTemplateMemberBase m in curTemplate.m_Members)
                {
                    if (m.Instantiation == InstantiationType.Instance)
                    {
                        instMembersAL.Add(m);
                    }
                }
                curTemplate = curTemplate.m_BaseTemplate;
            }

            IGeoTemplateMemberBase[] retArray = new IGeoTemplateMemberBase[instMembersAL.Count];
            Int32 count = 0;
            foreach (GeoTemplateMemberBase m in instMembersAL)
            {
                retArray.SetValue((IGeoTemplateMemberBase)m, count++);
            }

            return retArray;
        }

        public IGeoType CreateType(bool addToChildList)
        {
            return (IGeoType)CreateTypeInternal(addToChildList);
        }

        #endregion

        #region GeoTemplate Methods

        static public GeoTemplate Load(String filename)
        {
            XmlSerializer s = new XmlSerializer(typeof(GeoTemplate));
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            XmlReader reader = new XmlTextReader(fs);

            GeoTemplate tmpl = null;
            try
            {
                tmpl = s.Deserialize(reader) as GeoTemplate;
                tmpl.Name = System.IO.Path.GetFileNameWithoutExtension(filename);
                tmpl.PostLoad();
            }
            catch (System.InvalidOperationException ex)
            {
				if (ex.InnerException is System.Xml.XmlException)
				{
					System.Windows.Forms.MessageBox.Show("Error reading " + filename + "\n" + ex.InnerException.Message, "XML Reading Error");
				}
				else
				{
					System.Windows.Forms.MessageBox.Show("Error reading " + filename + "\n" + ex.InnerException.Message, "XML Reading Error");
				}
            }

            reader.Close();
            return tmpl;
        }

        public void PostLoad()
        {
            GeoDataSet.CurrentDataSet.AddTemplateToProject(this);

            if (m_TypeElementName == null || m_TypeElementName == String.Empty)
            {
                m_TypeElementName = m_Name;
            }

            if (m_InstanceElementName == null || m_InstanceElementName == String.Empty)
            {
                m_InstanceElementName = m_Name;
            }

            // recurse into contained templates
            foreach (GeoTemplate tmpl in m_ContainedTemplates)
            {
                tmpl.m_ContainingTemplate = this;
                tmpl.PostLoad();
            }

            foreach (GeoTemplateMemberBase member in m_Members)
            {
                member.PostLoad();
            }
        }

        public void Save(string filename)
        {
            PreSave();

            XmlSerializer s = new XmlSerializer(typeof(GeoTemplate));
            FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write);
            XmlTextWriter writer = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            writer.Formatting = Formatting.Indented;
            writer.WriteRaw(String.Empty);
            writer.Indentation = 1;
            writer.IndentChar = '\t';

            // This removes the namespaces from the root level element
            XmlSerializerNamespaces xsn = new XmlSerializerNamespaces();
            xsn.Add(String.Empty, String.Empty);

            s.Serialize(writer, this, xsn);

            writer.Close();
        }

        public void PreSave()
        {
            foreach (GeoTemplateMemberBase member in m_Members)
            {
                member.PreSave();
            }

            foreach (GeoTemplate tmpl in m_ContainedTemplates)
            {
                tmpl.PreSave();
            }
        }

        [ComVisible(false)]
        public GeoType CreateTypeInternal(bool addToChildList)
        {
            GeoType t = new GeoType();
            t.m_Template = this;

            t.m_BaseType = null;

            AddMembersToType(t);

            if (addToChildList)
            {
                this.m_ChildTypes.Add(t);
            }

            return t;
        }

        public void AddMembersToType(GeoType t)
        {
            if (m_BaseTemplate != null)
            {
                m_BaseTemplate.AddMembersToType(t);
            }

            foreach (GeoTemplateMemberBase m in m_Members)
            {
                GeoTypeMemberBase typeMember = (GeoTypeMemberBase)m.CreateTypeMember(t);
                t.m_Members.Add(typeMember);
            }
        }

        public bool IsSubclassOf(GeoTemplate t)
        {
            if (t == this)
            {
                return true;
            }
            if (this.m_BaseTemplate == null)
            {
                return false;
            }
            return this.m_BaseTemplate.IsSubclassOf(t);
        }

		public int GetNumberOfChildInstances()
		{
			int iReturnMe = 0;
			foreach (GeoTemplate obChildTemplate in m_ChildTemplates)
			{
				iReturnMe += obChildTemplate.GetNumberOfChildInstances();
			}
			foreach (GeoType obChildType in m_ChildTypes)
			{
				iReturnMe += obChildType.GetNumberOfChildInstances();
			}
			return iReturnMe;
		}

		public int GetNumberOfChildTypes()
		{
			int iReturnMe = 0;
			foreach (GeoTemplate obChildTemplate in m_ChildTemplates)
			{
				iReturnMe += obChildTemplate.GetNumberOfChildTypes();
			}
			foreach (GeoType obChildType in m_ChildTypes)
			{
				if (!obChildType.IsInstance)
				{
					iReturnMe++;
				}
				iReturnMe += obChildType.GetNumberOfChildTypes();
			}
			return iReturnMe;
		}

		public int GetNumberOfChildTemplates()
		{
			int iReturnMe = 0;
			foreach (GeoTemplate obChildTemplate in m_ChildTemplates)
			{
				iReturnMe++;
				iReturnMe += obChildTemplate.GetNumberOfChildTemplates();
			}
			return iReturnMe;
		}

		#endregion
    }
}
