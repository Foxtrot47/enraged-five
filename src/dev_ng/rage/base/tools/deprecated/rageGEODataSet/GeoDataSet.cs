using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;

namespace rageGEODataSet
{
	[ComVisible(true)]
	[Guid("85705FD5-667F-4969-8D65-346B453393F6")]
	public enum GeoSortType
	{
		Ascending,
		Descending,
		None
	}

	[ComVisible(true)]
	[Guid("BD8AD840-FF00-4ae6-B757-C1B64F37F468")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoDataSet
	{
		string GetGeoTemplateDirectory();
		string GetGeoTypeDirectory();
		string GetGeoInstanceDirectory();

		void LoadFromDirectory(string dirName, bool bShowLoadingSplash);
		void LoadFromFileLists(String[] templates, String[] types, String[] instances);

		IGeoTemplate LoadTemplateFromFile(String templatePath);
		IGeoType LoadTypeFromFile(String typePath);
		IGeoType LoadInstanceFromFile(String instancePath);

		Int32 GetTemplateCount();
		IGeoTemplate GetTemplate(String templateName);

		Int32 GetTopLevelTypeCount();
		IGeoType GetTopLevelType(String typeName);
		IGeoType[] GetAllTopLevelTypes(GeoSortType nameSort);

		bool DeleteInstanceByUUID(String uuid);
		IGeoType GetInstanceByUUID(String uuid);

		void AddGeoType(IGeoType gType);

		IGeoType CreateTypeFromTemplate(IGeoTemplate tmpl);
		IGeoType CreateTypeFromTemplateWithName(IGeoTemplate tmpl, String name);

		IGeoType CreateChildTypeFromType(IGeoType type, bool isInstance);
		IGeoType CreateChildTypeFromTypeWithName(IGeoType type, bool isInstance, String name);

		String FindNewTypeName(String baseName);

		void SaveAll();

		void MarkAllInstancesAsClean();
		void MarkAllInstancesAsPersistant();

		IEnumerator GetGeoTypeEnumerator();
		IEnumerator GetTopLevelGeoTypeEnumerator();
		IEnumerator GetGeoInstanceEnumerator();
		IEnumerator GetTopLevelGeoInstanceEnumerator();
		IEnumerator GetGeoTypeAndInstanceEnumerator();
		IEnumerator GetTopLevelGeoTypeAndInstanceEnumerator();
	}

	[ComVisible(true)]
	[Guid("F9C8476A-6051-4192-A6D5-82531C576B88")]
	[ProgId("rageGEODataSet.GEODataSet")]
	public class GeoDataSet : IGeoDataSet
	{
		private enum GEOFilterType
		{
			GEO_INSTANCE,
			GEO_TYPE,
			GEO_BOTH
		};

		#region Data Members

		public Dictionary<String, GeoTemplate> m_GeoTemplates;
		public Dictionary<String, GeoType> m_GeoTypes;
		public List<GeoTemplate> m_RootTemplates;

		public static GeoDataSet CurrentDataSet;

		private string m_ProjectDirectory;

		private IWin32Window m_obFrontMostWindowHandle = null;
		public IWin32Window FrontMostWindowHandle
		{
			get 
			{
				return m_obFrontMostWindowHandle;
			}
		}

		#endregion

		#region Constructors

		public GeoDataSet()
		{
			m_GeoTemplates = new Dictionary<String, GeoTemplate>();
			m_GeoTypes = new Dictionary<String, GeoType>();
			m_RootTemplates = new List<GeoTemplate>();
			CurrentDataSet = this;
		}

		#endregion

		#region IGeoDataSet Interface

		public string GetGeoTemplateDirectory()
		{
			return GetGeoDirectoryVariable("BaseGEOTemplatePath", "GeoTemplates");
		}

		public string GetGeoTypeDirectory()
		{
			return GetGeoDirectoryVariable("BaseGEOTypePath", "GeoTypes");
		}

		public string GetGeoInstanceDirectory()
		{
			return GetGeoDirectoryVariable("BaseGEOInstancePath", "GeoInstances");
		}

		public void LoadFromDirectory(String dirName, bool bShowLoadingSplash)
		{
			LoadingSplash ls = null;
			IWin32Window obPrevFrontMostWindowHandle = FrontMostWindowHandle;

			m_ProjectDirectory = dirName;

			if (bShowLoadingSplash)
			{
				//Display the loading GEOs splash screen...
				ls = new LoadingSplash();
				m_obFrontMostWindowHandle = ls;

				Thread lsThread = new Thread(delegate()
				{
					ls.ShowDialog();
				});
				lsThread.Name = "LoadingSplashThread";
				lsThread.Start();
			}

			try
			{
				LoadTemplates();
				LoadTypes();
				LoadInstances();
			}
			catch (Exception ex)
			{
				throw new ApplicationException(String.Format("GeoDataSet.LoadFromDirectory({0}) Exception", dirName), ex);
			}
			finally
			{
				if (bShowLoadingSplash && (ls != null))
				{
					//In the event that the loading thread got here faster than the splash screen
					//thread could create the control check to make sure its available to respond
					//to the close call, otherwise it will open and stay visible forever
					m_obFrontMostWindowHandle = obPrevFrontMostWindowHandle;
					while (true)
					{
						if (ls.Created && ls.Visible)
							break;
						Thread.Sleep(10);
					}

					//Close the loading GEOs splash...
					ls.Invoke(new System.Windows.Forms.MethodInvoker(
						delegate()
						{
							ls.Close();
						}));
				}
			}
		}

		public void LoadFromFileLists(String[] templates, String[] types, String[] instances)
		{
			LoadFromFileLists(templates, types, instances, false);
		}

		public void LoadFromFileLists(String[] templates, String[] types, String[] instances, bool bShowLoadingSplash)
		{
			// Open UI
			LoadingSplash ls = null;
			IWin32Window obPrevFrontMostWindowHandle = FrontMostWindowHandle;

			if (bShowLoadingSplash)
			{
				//Display the loading GEOs splash screen...
				ls = new LoadingSplash();
				m_obFrontMostWindowHandle = ls;
				Thread lsThread = new Thread(delegate()
				{
					ls.ShowDialog();
				});
				lsThread.Name = "LoadingSplashThread";
				lsThread.Start();
			}

			// Load everything
			LoadFromFileListsInternal(templates, types, instances);

			// Close UI
			if (bShowLoadingSplash && (ls != null))
			{
				//In the event that the loading thread got here faster than the splash screen
				//thread could create the control check to make sure its available to respond
				//to the close call, otherwise it will open and stay visible forever
				m_obFrontMostWindowHandle = obPrevFrontMostWindowHandle;
				while (true)
				{
					if (ls.Created && ls.Visible)
						break;
					Thread.Sleep(10);
				}

				//Close the loading GEOs splash...
				ls.Invoke(new System.Windows.Forms.MethodInvoker(
					delegate()
					{
						ls.Close();
					}));
			}
		}

		private void LoadFromFileListsInternal(string[] templates, string[] types, string[] instances)
		{
            if (templates != null && templates.Length > 0)
			{
				// Progress window
				ProgressWindow obProgressWindow = new ProgressWindow();
				obProgressWindow.SetProgressBarMin(0);
				obProgressWindow.SetProgressBarMax(templates.Length +1);
				obProgressWindow.OpenWindow();

				// Load templates
				foreach (string s in templates)
				{
					// Increase progress window
					obProgressWindow.IncProgressBarValue();
					obProgressWindow.SetLabelText(s);

					// Load template
					LoadTemplateFromFile(s);
				}

				// Increase progress window
				obProgressWindow.IncProgressBarValue();
				obProgressWindow.SetLabelText("Rebuilding Tree....");

				// Rebuild internal representation
				RebuildProjectTemplateTree();

				// Close progress window
				obProgressWindow.Done();
			}

			if ((types != null && types.Length > 0) || (instances != null && instances.Length > 0))
			{
				// Progress window
				ProgressWindow obProgressWindow = new ProgressWindow();
				obProgressWindow.SetProgressBarMin(0);
                if (types != null && instances == null)
                {
                    obProgressWindow.SetProgressBarMax(types.Length + 1);
                }
                else if (types == null && instances != null)
                {
                    obProgressWindow.SetProgressBarMax(instances.Length + 1);
                }
                else
                {
                    obProgressWindow.SetProgressBarMax(types.Length + instances.Length + 1);
                }
				obProgressWindow.OpenWindow();

                if (types != null)
                {
                    // Load types
                    foreach (string s in types)
                    {
                        // Increase progress window
                        obProgressWindow.IncProgressBarValue();
                        obProgressWindow.SetLabelText(s);

                        // Load type
                        LoadTypeFromFile(s);
                    }
                }

                if (instances != null)
                {
                    bool bShowDuplicateInstanceWarning = true;
                    
                    // Load instances
                    for (int instIdx = 0; instIdx < instances.Length; instIdx++)
                    {
                        String s = instances[instIdx];

                        // Increase progress window
                        obProgressWindow.IncProgressBarValue();
                        obProgressWindow.SetLabelText(s);

                        // Load instance
                        try
                        {
                            LoadInstanceFromFile(s);
                        }
                        catch (System.ArgumentException ex)
                        {
                            if (bShowDuplicateInstanceWarning)
                            {
                                DialogResult dr = System.Windows.Forms.MessageBox.Show(
                                        String.Format("Found one or more instances named '{0}'.\n Continue showing warning for other instances?", ex.ParamName),
                                        "Duplictate Instance Error",
                                        MessageBoxButtons.YesNoCancel,
                                        MessageBoxIcon.Warning);
                                if (dr == DialogResult.Cancel)
                                    break;
                                else if (dr == DialogResult.No)
                                    bShowDuplicateInstanceWarning = false;
                            }
                        }
                    }
                }
				
				// Increase progress window
				obProgressWindow.IncProgressBarValue();
				obProgressWindow.SetLabelText("Rebuilding Tree....");

				// Rebuild internal representation
				RebuildProjectTypeTree();

				// Close progress window
				obProgressWindow.Done();
			}
		}

		public IGeoTemplate LoadTemplateFromFile(string filename)
		{
			GeoTemplate tmpl = GeoTemplate.Load(filename);
			if (tmpl != null)
			{
				tmpl.FileName = filename;
			}
			return tmpl;
		}

		public IGeoType LoadTypeFromFile(string filename)
		{
			GeoType t = GeoType.Load(filename);
			if (t == null)
			{
				return null;
			}
			t.FileName = filename;
			if (m_GeoTypes.ContainsKey(t.Name))
			{
				MessageBox.Show("Current Geo Data Set already contains a GEO Type called " + t.Name + ".\nSo ignoring new type with the same name", "rageWarning", MessageBoxButtons.OK);
			}
			else
			{
				m_GeoTypes.Add(t.Name, t);
			}

			return m_GeoTypes[t.Name];
		}
        
		public IGeoType LoadInstanceFromFile(string filename)
		{
            GeoType t = GeoType.Load(filename);
            if (t == null)
                return null;
     
            t.FileName = filename;
            t.IsInstance = true;

			try
			{
				m_GeoTypes.Add(t.Name, t);
			}
			catch (System.ArgumentException ex)
			{
                throw new System.ArgumentException(String.Format("Found more than one instance or type named '{0}'", t.Name), t.Name, ex);
			}
			return t;
		}

		public Int32 GetTemplateCount()
		{
			return m_GeoTemplates.Count;
		}

		public IGeoTemplate GetTemplate(string templateName)
		{
			GeoTemplate template = null;
			if (m_GeoTemplates.TryGetValue(templateName.ToLower(), out template))
				return (IGeoTemplate)template;
			else
				return null;
		}

		public Int32 GetTopLevelTypeCount()
		{
			return m_GeoTypes.Count;
		}

		public IGeoType GetTopLevelType(string typeName)
		{
			GeoType gType = null;
			if (m_GeoTypes.TryGetValue(typeName.ToLower(), out gType))
			{
				return (IGeoType)gType;
			}
			else
				return null;
		}

		public IGeoType[] GetAllTopLevelTypes(GeoSortType nameSort)
		{
			List<IGeoType> geoTypeList = new List<IGeoType>();

			foreach (IGeoType gType in this.TopLevelGeoTypes)
			{
				Debug.Assert((gType.Name != null) && (gType.Name != String.Empty),
					String.Format("GetAllTopLevelTypes, top-level GeoType found with no name assigned. [{0}]", gType.FileName));
				geoTypeList.Add(gType);
			}

			IGeoType[] retTypes = geoTypeList.ToArray();

			if (nameSort != GeoSortType.None)
			{
				GeoTypeNameComparer tComp = new GeoTypeNameComparer();
				tComp.SortType = nameSort;
				Array.Sort(retTypes, tComp);
			}

			return retTypes;
		}

		public bool DeleteInstanceByUUID(String uuid)
		{
			//Locate the instance...
			IGeoType type = GetInstanceByUUID(uuid);
			if (type == null)
				return false;

			//If the instance is contained then remove it from the container
			if (type.ContainingMember != null)
			{
				IGeoTypeMemberGeo containingMemberGeo = type.ContainingMember;
				if (((IGeoTypeMemberBase)containingMemberGeo).ContainingTypeMember != null)
				{
					IGeoTypeMemberBase outerMemberBase = ((IGeoTypeMemberBase)containingMemberGeo).ContainingTypeMember;
					if (outerMemberBase.GetType().GetInterface("IGeoTypeMemberArray") != null)
					{
						//The instance is held by a GeoMember in an array, so remove the GeoMember item from the array
						IGeoTypeMemberArray outerMemberArr = (IGeoTypeMemberArray)outerMemberBase;
						IGeoTypeMemberBase remItem = outerMemberArr.RemoveElementByUUID(containingMemberGeo.UUID);
						Debug.Assert(remItem == containingMemberGeo, "DeleteInstanceByUUID, removed item doesn't match container item.");

						outerMemberBase.GeoTypeThatContainsMe.Dirty = true;

						return true;
					}
					else
					{
						//TODO : Handle the GeoTypeMemberGeo case
						Debug.Assert(false, "DeleteInstanceByUUID, operation option isn't implemented");
						return false;
					}
				}
			}
			else
			{
				//The instance isn't contained, so mark it as dirty and deleted
				type.Deleted = true;
				type.Dirty = true;
			}

			return true;
		}

		public IGeoType GetInstanceByUUID(string uuid)
		{
			foreach (IGeoType gType in this.GeoInstances)
			{
				if (String.Compare(gType.UUID, uuid) == 0)
					return gType;
			}
			return null;
		}

		public void AddGeoType(IGeoType gType)
		{
			String geoName = gType.Name;
			if ((geoName == String.Empty) || (geoName == null))
			{
				geoName = FindNewTypeName(gType.BaseType.Name);
			}
			else
			{
				if (m_GeoTypes.ContainsKey(geoName))
				{
					geoName = FindNewTypeName(geoName);
				}
			}
			gType.Name = geoName;
			gType.Dirty = true;
			m_GeoTypes.Add(gType.Name, (GeoType)gType);
		}

		public IGeoType CreateTypeFromTemplate(IGeoTemplate tmpl)
		{
			string newName = FindNewTypeName(tmpl.FullName);
			if (newName == String.Empty)
			{
				return null;
			}
			return CreateTypeFromTemplateWithName(tmpl, newName);
		}

		public IGeoType CreateTypeFromTemplateWithName(IGeoTemplate tmpl, string name)
		{
			if (m_GeoTypes.ContainsKey(name))
			{
				return null;
			}

			IGeoType newType = tmpl.CreateType(true);
			newType.Name = name;
			newType.SetAllMembersAsInherited();
			newType.Dirty = true;

			m_GeoTypes.Add(newType.Name, (GeoType)newType);

			return (IGeoType)newType;
		}

		public IGeoType CreateChildTypeFromType(IGeoType type, bool isInstance)
		{
			string newName = FindNewTypeName(type.Name);

			if (newName == String.Empty)
			{
				return null;
			}
			return CreateChildTypeFromTypeWithName(type, isInstance, newName);
		}

		public IGeoType CreateChildTypeFromTypeWithName(IGeoType type, bool isInstance, string name)
		{
			if (m_GeoTypes.ContainsKey(name))
			{
				return null;
			}

			GeoTemplate tmpl = (GeoTemplate)type.Template;
			GeoType newType = tmpl.CreateTypeInternal(true);
			newType.DeepCopyFrom((GeoType)type);

			//TODO : The containing type of each member at this point is set to the "type", not
			//"newtype", this seems wrong???
			foreach (GeoTypeMemberBase member in newType.m_Members)
			{
				member.GeoTypeThatContainsMe = newType;
			}

			newType.m_BaseType = (GeoType)type;
			newType.IsInstance = isInstance;
			newType.SetAllMembersAsInherited();
			newType.Name = name;
			newType.Dirty = true;

			if (isInstance)
			{
				newType.UUID = GeoUUID.CreateUUID();
			}

			type.AddChildType(newType);

			m_GeoTypes.Add(newType.Name, newType);

			return newType;
		}

		public String FindNewTypeName(String baseName)
		{
			string newName = baseName;

			if (m_GeoTypes.ContainsKey(newName))
			{
				Regex re = new Regex("_[0-9]+$");
				string truncBaseName = re.Replace(newName, "");

				int count = 1;
				newName = truncBaseName + "_1";
				while (m_GeoTypes.ContainsKey(newName))
				{
					count++;
					newName = String.Format("{0}_{1}", truncBaseName, count);
				}
			}
			return newName;
		}

		public class GeoDirtyCheckVisitor : GeoTypeVisitor
		{
			public bool HasDirtyData = false;

			public override void VisitMember(GeoTypeMemberBase m)
			{
				if (m.GeoTypeThatContainsMe != null)
				{
					if (m.GeoTypeThatContainsMe.Dirty)
					{
						HasDirtyData = true;
						return;
					}
				}
				base.VisitMember(m);
			}
		}

		public void SaveAll()
		{
			List<String> errorFileList = new List<String>();
			List<GeoType> deletedTypeList = new List<GeoType>();

			foreach (GeoType t in m_GeoTypes.Values)
			{
				bool isWritable = true;
				bool isDirty = t.Dirty;

				//If the top level type is already dirty then we don't need to seach inside of it
				//since it will be saved, but if not then look at any contained types to see if they 
				//have been marked as dirty
				if (!isDirty)
				{
					GeoDirtyCheckVisitor dcv = new GeoDirtyCheckVisitor();
					dcv.Visit(t);
					isDirty = dcv.HasDirtyData;
				}

				if (t.FileName == null)
				{
					if (t.IsInstance)
					{
						t.FileName = String.Format("{0}\\GeoInstances\\{1}\\{2}\\{3}.geo",
							m_ProjectDirectory, t.m_Template.Name, t.m_BaseType.Name, t.Name);
					}
					else
					{
						t.FileName = String.Format("{0}\\GeoTypes\\{1}.geotype", m_ProjectDirectory, t.Name);
					}
					isWritable = true;
				}
				else
				{
					FileInfo info = new FileInfo(t.FileName);
					if (info.Exists)
					{
						if (info.IsReadOnly && isDirty)
						{
							DialogResult dlgRes = DialogResult.Retry;
							while ((dlgRes == DialogResult.Retry) && info.IsReadOnly)
							{
								dlgRes = MessageBox.Show(String.Format("Data has changed in the GEO File \"{0}\", but the file is not writable", t.FileName),
									"GeoDataSet File Error",
									System.Windows.Forms.MessageBoxButtons.AbortRetryIgnore);
								info.Refresh();
							}

							if (dlgRes == DialogResult.Ignore)
							{
								continue;
							}
							else if (dlgRes == DialogResult.Abort)
							{
								return;
							}
						}
						isWritable = (!info.IsReadOnly);
					}
					else
					{
						isWritable = true;
					}
				}

				if (isWritable && t.Deleted && t.IsInstance)
				{
					//If the instance as been marked as deleted, and a file exists for it, delete the instance file.
					FileInfo info = new FileInfo(t.FileName);
					if (info.Exists)
					{
						File.Delete(t.FileName);
					}
					deletedTypeList.Add(t);
				}
				else if (isWritable && isDirty)
				{
					//if (t.Template.ContainedOnly)
					//{
					//    errorFileList.Add(t.FileName);
					//}
					//else
					//{
					//    // only save if it's writable and dirty
					//    FileInfo fi = new FileInfo(t.FileName);
					//    DirectoryInfo di = new DirectoryInfo(fi.DirectoryName);
					//    if(!di.Exists)
					//        Directory.CreateDirectory(fi.DirectoryName);

					//    t.Save();
					//}

					// only save if it's writable and dirty
					FileInfo fi = new FileInfo(t.FileName);
					DirectoryInfo di = new DirectoryInfo(fi.DirectoryName);
					if (!di.Exists)
						Directory.CreateDirectory(fi.DirectoryName);
					t.Save();
				}
			}

			//Remove all the instances from the top level list that were marked as deleted
			foreach (GeoType t in deletedTypeList)
			{
				m_GeoTypes.Remove(t.Name);
			}

			if (errorFileList.Count > 0)
			{
				String errMsg = "The following files were not saved because they are marked as 'containOnly', and cannot\nbe saved as standalone geo instances.\n\n";
				foreach (String errorFileName in errorFileList)
					errMsg += (errorFileName + "\n");
				System.Windows.Forms.MessageBox.Show(errMsg, "GeoDataSet Errors.");
			}
		}

		public void MarkAllInstancesAsClean()
		{
			foreach (IGeoType gType in this.GeoInstances)
				gType.Dirty = false;
		}

		public void MarkAllInstancesAsPersistant()
		{
			foreach (IGeoType gType in this.GeoInstances)
				gType.Deleted = false;
		}

		public IEnumerator GetGeoTypeEnumerator()
		{
			return (PrivateGeoTypeIterator(null, GEOFilterType.GEO_TYPE, true)).GetEnumerator();
		}

		public IEnumerator GetTopLevelGeoTypeEnumerator()
		{
			return (PrivateGeoTypeIterator(null, GEOFilterType.GEO_TYPE, false)).GetEnumerator();
		}

		public IEnumerator GetGeoInstanceEnumerator()
		{
			return (PrivateGeoTypeIterator(null, GEOFilterType.GEO_INSTANCE, true)).GetEnumerator();
		}

		public IEnumerator GetTopLevelGeoInstanceEnumerator()
		{
			return (PrivateGeoTypeIterator(null, GEOFilterType.GEO_INSTANCE, false)).GetEnumerator();
		}

		public IEnumerator GetGeoTypeAndInstanceEnumerator()
		{
			return (PrivateGeoTypeIterator(null, GEOFilterType.GEO_BOTH, true)).GetEnumerator();
		}

		public IEnumerator GetTopLevelGeoTypeAndInstanceEnumerator()
		{
			return (PrivateGeoTypeIterator(null, GEOFilterType.GEO_BOTH, false)).GetEnumerator();
		}

		#endregion

		#region GeoDataSet Methods

		private void LoadTemplates()
		{
			//Load the templates
			try
			{
				if (Directory.Exists(GetGeoTemplateDirectory()))
				{
					LoadFromFileListsInternal(Directory.GetFiles(GetGeoTemplateDirectory(), "*.geotemplate"), new string[0], new string[0]);
				}
				else
				{
					throw new System.IO.DirectoryNotFoundException(String.Format("Failed to locate GEO Template directory : {0}", GetGeoTemplateDirectory()));
				}
			}
			catch (Exception ex)
			{
				throw new ApplicationException("GeoDataSet.LoadTemplates Exception", ex);
			}
		}

		private void LoadTypes()
		{
			//Load the types
			try
			{
				if (Directory.Exists(GetGeoTypeDirectory()))
				{
					LoadFromFileListsInternal(new string[0], Directory.GetFiles(GetGeoTypeDirectory(), "*.geotype"), new string[0]);
				}
				else
				{
					throw new System.IO.DirectoryNotFoundException(String.Format("Failed to locate GEO Type directory : {0}", GetGeoTypeDirectory()));
				}
			}
			catch (Exception ex)
			{
				throw new ApplicationException("GeoDataSet.LoadTypes Exception", ex);
			}
		}

		private void LoadInstances()
		{
			//Iterate over all the directories including and under the root GEO instances directory and
			//load each geo file found.
			if (Directory.Exists(GetGeoInstanceDirectory()))
			{
				// Get a list of valid instance extensions
				List<string> obAStrValidGeoInstanceFileExtensions = new List<string>();
				foreach (GeoTemplate t in m_GeoTemplates.Values)
				{
					if (!obAStrValidGeoInstanceFileExtensions.Contains(t.InstanceFileExtension))
					{
						obAStrValidGeoInstanceFileExtensions.Add(t.InstanceFileExtension);
					}
				}

				// Get instance files
				string[] astrAllFiles = Directory.GetFiles(GetGeoInstanceDirectory(), "*.*", SearchOption.AllDirectories);
				List<string> obAStrGeoInstanceFiles = new List<string>();
				foreach (string strFile in astrAllFiles)
				{
					// Is it one of the extensions we care about?
					foreach (string strExtension in obAStrValidGeoInstanceFileExtensions)
					{
						if (strFile.ToLower().EndsWith(strExtension))
						{
							obAStrGeoInstanceFiles.Add(strFile);
						}
					}
				}

				LoadFromFileListsInternal(new string[0], new string[0], obAStrGeoInstanceFiles.ToArray());
			}
		}

		public void AddTemplateToProject(GeoTemplate tmpl)
		{
			// Does it already exist?
			if (m_GeoTemplates.ContainsKey(tmpl.FullName))
			{
				MessageBox.Show("Current Geo Data Set already contains a GEO Template called " + tmpl.FullName + ".\nSo ignoring new template with the same name", "rageWarning", MessageBoxButtons.OK);
			}
			else
			{
				m_GeoTemplates.Add(tmpl.FullName, tmpl);
			}
		}

		protected string GetGeoDirectoryVariable(string var, string def)
		{
			//rageUsefulCSharpToolClasses.rageStatus status;
			//string dirLoc = rageUsefulCSharpToolClasses.rageModuleManagement.GetSettingsValue(var, out status);
			//if (status.Success())
			//{
			//    return dirLoc;
			//}
			return m_ProjectDirectory + "\\" + def;
		}

		public void RebuildProjectTemplateTree()
		{
			m_RootTemplates.Clear();

			//foreach (GeoTemplate t in m_GeoTemplates.Values)
			//{
			//    t.m_ChildTemplates.Clear();
			//    t.m_ChildTypes.Clear();
			//}

			foreach (GeoTemplate t in m_GeoTemplates.Values)
			{
				if (t.BaseTemplateName != null && t.BaseTemplateName != "")
				{
					try
					{
						GeoTemplate parent = m_GeoTemplates[t.BaseTemplateName] as GeoTemplate;
						parent.m_ChildTemplates.Add(t);
						t.m_BaseTemplate = parent;
					}
					catch (KeyNotFoundException ex)
					{
						throw new ApplicationException(String.Format("GeoDataSet::RebuildProjectTemplateTree, Base template '{0}' of template '{1}' is not loaded", t.BaseTemplateName, t.Name), ex);
					}
				}
				else if(t.FileName != null && t.FileName != "")
				{
					m_RootTemplates.Add(t);
				}
			}
		}

		public void RebuildProjectTypeTree()
		{

			//foreach (GeoType t in m_GeoTypes.Values)
			//{
			//    t.m_ChildTypes.Clear();
			//}

			foreach (GeoType t in m_GeoTypes.Values)
			{
				t.FinishLoad();
				if (t.m_BaseType != null)
				{
					try
					{
						GeoType parent = m_GeoTypes[t.m_BaseType.Name] as GeoType;
						parent.m_ChildTypes.Add(t);
						t.m_BaseType = parent;
					}
					catch (KeyNotFoundException ex)
					{
						throw new ApplicationException(String.Format("GeoDataSet::RebuildProjectTypeTree, Base type '{0}' of type '{1}' is not loaded",
							t.m_BaseType.Name, t.Name), ex);
					}
				}
			}
		}

		public IEnumerable<IGeoType> GeoTypes
		{
			get { return PrivateGeoTypeIterator(null, GEOFilterType.GEO_TYPE, true); }
		}

		public IEnumerable<IGeoType> TopLevelGeoTypes
		{
			get { return PrivateGeoTypeIterator(null, GEOFilterType.GEO_TYPE, false); }
		}

		public IEnumerable<IGeoType> GeoInstances
		{
			get { return PrivateGeoTypeIterator(null, GEOFilterType.GEO_INSTANCE, true); }
		}

		public IEnumerable<IGeoType> TopLevelGeoInstances
		{
			get { return PrivateGeoTypeIterator(null, GEOFilterType.GEO_INSTANCE, false); }
		}

		public IEnumerable<IGeoType> GeoTypesAndInstances
		{
			get { return PrivateGeoTypeIterator(null, GEOFilterType.GEO_BOTH, true); }
		}

		public IEnumerable<IGeoType> TopLevelGeoTypesAndInstances
		{
			get { return PrivateGeoTypeIterator(null, GEOFilterType.GEO_BOTH, false); }
		}

		private IEnumerable<IGeoType> PrivateGeoTypeIterator(GeoTypeMemberBase rootMember, GEOFilterType filter, Boolean bIncludeContainedTypes)
		{
			//If rootMember is null, then start iterating over the top level types/instances
			if (rootMember == null)
			{
				//Iterate over all the toplevel types, using the filter to determine which types to return
				//Note that types which are marked as deleted are ignored.
				Dictionary<String, GeoType>.Enumerator typeEnum = m_GeoTypes.GetEnumerator();
				while (typeEnum.MoveNext())
				{
					GeoType curType = typeEnum.Current.Value;
					if (filter == GEOFilterType.GEO_BOTH)
						yield return ((IGeoType)curType);
					else
					{
						if ((filter == GEOFilterType.GEO_TYPE) && (!curType.IsInstance) && (!curType.Deleted))
							yield return ((IGeoType)curType);
						else if ((filter == GEOFilterType.GEO_INSTANCE) && (curType.IsInstance) && (!curType.Deleted))
							yield return ((IGeoType)curType);
					}

					//If the iterator should include contained types, then move the iteration into the members of the type
					//which will allow the iterator to return types held by GeoType members.
					if (bIncludeContainedTypes)
					{
						List<GeoTypeMemberBase>.Enumerator mbrEnum = curType.m_Members.GetEnumerator();
						while (mbrEnum.MoveNext())
						{
							foreach (IGeoType type in PrivateGeoTypeIterator(mbrEnum.Current, filter, bIncludeContainedTypes))
								yield return type;
						}
					}
				}
			}
			else
			{
				Type rootMemberBaseType = rootMember.GetType();

				//If the current type member has child members then move the iterator into them.
				List<GeoTypeMemberBase> memberContents = rootMember.ChildMembers;
				if (memberContents != null)
				{
					List<GeoTypeMemberBase>.Enumerator contentEnum = memberContents.GetEnumerator();
					while (contentEnum.MoveNext())
					{
						foreach (IGeoType type in PrivateGeoTypeIterator(contentEnum.Current, filter, bIncludeContainedTypes))
							yield return type;
					}
				}
				else if (rootMemberBaseType == typeof(GeoTypeMemberGeo))
				{
					GeoTypeMemberGeo mbrGeo = (GeoTypeMemberGeo)rootMember;
					if (mbrGeo.GeoTypeThatIContain != null)
					{
						if (filter == GEOFilterType.GEO_BOTH)
							yield return ((IGeoType)mbrGeo.GeoTypeThatIContain);
						else
						{
							if ((filter == GEOFilterType.GEO_TYPE) && (!mbrGeo.GeoTypeThatIContain.IsInstance) && (!mbrGeo.GeoTypeThatIContain.Deleted))
								yield return ((IGeoType)mbrGeo.GeoTypeThatIContain);
							else if ((filter == GEOFilterType.GEO_INSTANCE) && (mbrGeo.GeoTypeThatIContain.IsInstance) && (!mbrGeo.GeoTypeThatIContain.Deleted))
								yield return ((IGeoType)mbrGeo.GeoTypeThatIContain);
						}
					}
				}
			}
		}

		// Finds the minimal list of files that would need to be loaded to be able to fully describe "type"
		public void FindMinimalDataSet(GeoType type, out List<GeoTemplate> templates, out List<GeoType> types)
		{

			Dictionary<GeoTemplate, object> templateSet = new Dictionary<GeoTemplate, object>();
			Dictionary<GeoType, object> typeSet = new Dictionary<GeoType, object>();

			AddTypeToDataSet(type, ref templateSet, ref typeSet);

			templates = new List<GeoTemplate>();
			templates.AddRange(templateSet.Keys);

			types = new List<GeoType>();
			types.AddRange(typeSet.Keys);
		}

		private void AddTypeToDataSet(GeoType type, ref Dictionary<GeoTemplate, object> templateSet, ref Dictionary<GeoType, object> typeSet)
		{
			// early out. assume that if type is in the set already, all its ancestors are there too
			if (typeSet.ContainsKey(type))
			{
				return;
			}

			// basically need to recursively follow a bunch of parent pointers back to their origins.
			// Also need to do the same for any contained geos.

			// pointers to follow:
			// type.BaseType
			// type.Template
			// type.Template.GetBaseTemplate
			// use visitor to find all geo members of type, do the same for their reference member.

			AddParentTemplates(type.m_Template, ref templateSet);

			GeoType t = type;
			while (t != null)
			{
				typeSet.Add(t, null);
				MinimalDataSetBuilder builder = new MinimalDataSetBuilder();
				builder.m_DataSet = this;
				builder.m_TemplateSet = templateSet;
				builder.m_TypeSet = typeSet;

				builder.Visit(t);

				t = t.m_BaseType;
			}
		}

		class MinimalDataSetBuilder : GeoTypeVisitor
		{
			public override bool BeginGeoMember(GeoTypeMemberGeo member)
			{
				if (member.GeoTypeThatIContain != null)
				{
					m_DataSet.AddTypeToDataSet(member.GeoTypeThatIContain as GeoType, ref m_TemplateSet, ref m_TypeSet);
				}

				return true;
			}

			public GeoDataSet m_DataSet;
			public Dictionary<GeoTemplate, object> m_TemplateSet;
			public Dictionary<GeoType, object> m_TypeSet;
		};

		private void AddParentTemplates(GeoTemplate tmpl, ref Dictionary<GeoTemplate, object> templates)
		{
			// early out, assume that if tmpl is in the set already, all its ancestors are there too.
			if (templates.ContainsKey(tmpl))
			{
				return;
			}

			GeoTemplate t = tmpl;
			// Walk up the template inheritance tree
			while (t != null)
			{
				templates.Add(t, null);
				// If this template is contained within another template, walk up that template's 
				// inheritance tree too
				if (t.m_ContainingTemplate != null)
				{
					AddParentTemplates(t.m_ContainingTemplate, ref templates);
				}
				t = t.m_BaseTemplate;
			}
		}

		public void RemoveType(GeoType obGeoType)
		{
			// Remove it
			RemoveTypeInternal(obGeoType);

			// Tidy up
			RebuildProjectTypeTree();
		}

		private void RemoveTypeInternal(GeoType obGeoType)
		{
			// First remove any types/instances that use this type
			bool bCheckForTypes = true;
			while (bCheckForTypes)
			{
				bCheckForTypes = false;
				foreach (GeoType obType in m_GeoTypes.Values)
				{
					if (obType.BaseTypeName == obGeoType.Name)
					{
						// Found a type to remove
						RemoveType(obType);

						// It sucks, but m_GeoTemplates.Values could have all changed now, so I need to restart
						bCheckForTypes = true;
						break;
					}
				}
			}

			// Finally actually remove myself
			m_GeoTypes.Remove(obGeoType.Name);
		}

		public void RemoveTemplate(GeoTemplate obGeoTemplate)
		{
			// Remove it
			RemoveTemplateInternal(obGeoTemplate);

			// Tidy up
			RebuildProjectTemplateTree();
		}

		private void RemoveTemplateInternal(GeoTemplate obGeoTemplate)
		{
			// First remove any types that use this template
			bool bCheckForTypes = true;
			while (bCheckForTypes)
			{
				bCheckForTypes = false;
				foreach (GeoType obGeoType in m_GeoTypes.Values)
				{
					if (obGeoType.Template.FileName == obGeoTemplate.FileName)
					{
						// Found a type to remove
						RemoveTypeInternal(obGeoType);

						// It sucks, but m_GeoTemplates.Values could have all changed now, so I need to restart
						bCheckForTypes = true;
						break;
					}
				}
			}

			// Next remove any templates that use this template
			bool bCheckForTemplates = true;
			while (bCheckForTemplates)
			{
				bCheckForTemplates = false;
				foreach (GeoTemplate obTemplate in m_GeoTemplates.Values)
				{
					if (obTemplate.BaseTemplateName == obGeoTemplate.Name)
					{
						// Found a Template to remove
						RemoveTemplateInternal(obTemplate);

						// It sucks, but m_GeoTemplates.Values could have all changed now, so I need to restart
						bCheckForTemplates = true;
						break;
					}
				}
			}

			// Finally actually remove the template
			m_GeoTemplates.Remove(obGeoTemplate.Name);
		}

		#endregion
	}

	[ComVisible(false)]
	public class GeoTypeNameComparer : IComparer
	{
		public GeoSortType SortType = GeoSortType.None;

		public int Compare(object x, object y)
		{
			IGeoType memX = x as IGeoType;
			IGeoType memY = y as IGeoType;
			if ((SortType == GeoSortType.None) || (SortType == GeoSortType.Ascending))
				return memX.Name.CompareTo(memY.Name);
			else
				return memY.Name.CompareTo(memX.Name);
		}
	}
}
