using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Xml.Serialization;
using System.ComponentModel;

namespace rageGEODataSet
{
    [ComVisible(false)]
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class GeoMemberMetadataAttribute : Attribute
    {
        public GeoMemberMetadataAttribute()
        {
            ValueType = true;
            Numeric = true;
            Composite = false;
        }

        // Does this member have a value that can be copied?
        public bool ValueType;

        // Does this member represent a number?
        public bool Numeric;

        // Does this member have child members?
        public bool Composite;
    }

    [ComVisible(true)]
    [Guid("AA7C6A07-AC64-4960-ABC9-CACE15665377")]
    public enum InstantiationType
    {
        [XmlEnum("type")]
        Type,
        [XmlEnum("instance")]
        Instance,
        [XmlEnum("both")]
        Both
    }

    [ComVisible(true)]
    [Guid("F1C42F29-655C-42a3-B3BE-29E4EA25114F")]
    public enum GeoMemberType
    {
        Bool,
        Int,
        Float,
        Vector2,
        Vector3,
        Vector4,
        Matrix34,
        Matrix44,
        String,
        Geo,
        File,
        Node,
        Array,
        Group,
        Color,
        Enum,
		Binary,
        Unknown
    }

    [ComVisible(true)]
    [Guid("EA53025F-00CA-43f6-A7DF-EFDE348B765E")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoTemplateMemberBase
    {
        string              Name { get; set; }
        bool                Locked { get; set; }
        InstantiationType   Instantiation { get; set; }
        GeoMemberType       GetMemberType();
        IGeoTypeMemberBase  CreateTypeMember(IGeoType t);
    }

    [GeoMemberMetadata()]
    public abstract class GeoTemplateMemberBase : IGeoTemplateMemberBase
    {

        #region Data Members

        [XmlAttribute("name")]
        public string m_Name;

        [XmlAttribute("displayName"), DefaultValue("")]
        public string m_InternalDisplayName = "";

        [XmlElement("Description"), DefaultValue("")]
        public string m_Description = "";

        [XmlAttribute("instantiation"), DefaultValue(InstantiationType.Type)]
        public InstantiationType m_Instantiation = InstantiationType.Type;

        [XmlAttribute("locked"), DefaultValue(false)]
        public bool m_Locked = false;

        [XmlIgnore]
        public GeoTemplate m_Template;

        [XmlAttribute("viewStages"), DefaultValue("0-31")]
        public string m_ViewStages = "0-31";

        [XmlAttribute("editStages"), DefaultValue("0-31")]
        public string m_EditStages = "0-31";

        private uint m_ViewBitmask = 0xFFFF;
        private uint m_EditBitmask = 0xFFFF;

        #endregion

        #region IGeoTemplateMemberBase

        [XmlIgnore]
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value.ToLower(); }
        }

        [XmlIgnore]
        public bool Locked
        {
            get { return m_Locked; }
            set { m_Locked = value; }
        }

        [XmlIgnore]
        public InstantiationType Instantiation
        {
            get { return m_Instantiation; }
            set { m_Instantiation = value; }
        }

        #endregion

        #region Properties
        
        [XmlIgnore]
        public string InternalDisplayName
        {
            get { return m_InternalDisplayName; }
            set { m_InternalDisplayName = value; }
        }

        [XmlIgnore]
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }

        [XmlIgnore]
        public string DisplayName
        {
            get
            {
                if (m_InternalDisplayName != null && m_InternalDisplayName != "")
                {
                    return m_InternalDisplayName;
                }
                return m_Name;
            }
            set
            {
                DisplayName = value;
            }
        }

        [XmlIgnore]
        public string ViewStages
        {
            get { return m_ViewStages; }
            set { m_ViewStages = value; }
        }

        [XmlIgnore]
        public string EditStages
        {
            get { return m_EditStages; }
            set { m_EditStages = value; }
        }

        #endregion

        #region Methods

        public bool IsViewableAtLevel(int level)
        {
            return TestBit(m_ViewBitmask, level);
        }

        public bool IsEditableAtLevel(int level)
        {
            return TestBit(m_EditBitmask, level);
        }

        public IGeoTypeMemberBase CreateTypeMember(IGeoType t)
        {
            GeoTypeMemberBase m = CreateTypeMemberInternal((GeoType)t);
            if (m != null)
            {
                m.m_TemplateMemberBase = this;
                m.GeoTypeThatContainsMe = t;
            }            
            return m;
        }

        public bool IsComposite()
        {
            Object[] attributes = this.GetType().GetCustomAttributes(true);
            foreach (Object attr in attributes)
            {
                GeoMemberMetadataAttribute geoMeta = attr as GeoMemberMetadataAttribute;
                if (geoMeta != null)
                {
                    return geoMeta.Composite;
                }
            }
            return false;
        }

        public virtual void PostLoad()
        {
            // turn editstages, viewstages into bitmasks
            m_ViewBitmask = StageListToBitmask(ViewStages);
            m_EditBitmask = StageListToBitmask(EditStages);

            if (DisplayName == "")
            {
                DisplayName = Name;
            }

            m_Name = m_Name.ToLower();
        }

        public virtual void PreSave()
        {
            // turn bitmasks into editstages, viewstages strings
            ViewStages = BitmaskToStageList(m_ViewBitmask);
            EditStages = BitmaskToStageList(m_EditBitmask);
        }

        protected static uint StageListToBitmask(string list)
        {
            uint result = 0;

            string[] ranges = list.Split(null);
            foreach (string range in ranges)
            {
                if (range == "")
                {
                    continue;
                }

                if (range.IndexOf('-') == -1)
                {
                    result |= 1u << Int32.Parse(range);
                }
                else
                {
                    int start = 0;
                    int end = 31;
                    string[] rangeEnds = range.Split(new char[] { '-' });
                    if (rangeEnds.Length != 2)
                    {
                        System.Diagnostics.Debug.WriteLine(String.Format("Misformed range string: {0}", range));
                    }
                    else
                    {
                        if (rangeEnds[0] != "")
                        { // check for -6
                            start = Int32.Parse(rangeEnds[0]);
                        }
                        if (rangeEnds[1] != "")
                        { // check for 6-
                            end = Int32.Parse(rangeEnds[1]);
                        }
                    }

                    for (int i = start; i <= end; i++)
                    {
                        result |= 1u << i;
                    }
                }
            }

            return result;
        }

        protected static bool TestBit(uint val, int bit)
        {
            return (val & (1 << bit)) != 0;
        }

        protected static string BitmaskToStageList(uint mask)
        {
            string result = "";

            for (int i = 0; i < 32; i++)
            {
                if (TestBit(mask >> i, 0))
                {
                    // if we have to write out this value...
                    // see if we have a span of 1, 2, or more
                    if (TestBit(mask >> i, 1))
                    {
                        // span of 2 or more
                        if (TestBit(mask >> i, 2))
                        {
                            // span of 3 or more
                            int start = i;
                            while (i < 32 && TestBit(mask >> i, 0))
                            {
                                i++;
                            }
                            int end = i;
                            result += String.Format("{0}-{1} ", start, end - 1);
                            continue;
                        }
                    }

                    // write this single value out
                    result += String.Format("{0} ", i);
                }
            }

            return result.TrimEnd(null);
        }

        #endregion

        #region Abstract Methods

        public abstract GeoTypeMemberBase   CreateTypeMemberInternal(GeoType t);
        public abstract GeoMemberType       GetMemberType();

        #endregion

    }
}
