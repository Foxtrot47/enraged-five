using System;
using System.Collections.Generic;
using System.Text;

namespace rageGEODataSet
{
    public class GeoTypeVisitor
    {

        public virtual void FloatMember(GeoTypeMemberFloat member) {}
        public virtual void BoolMember(GeoTypeMemberBool member) { }
        public virtual void IntMember(GeoTypeMemberInt member) { }
        public virtual void Vector2Member(GeoTypeMemberVector2 member) { }
        public virtual void Vector3Member(GeoTypeMemberVector3 member) { }
        public virtual void Vector4Member(GeoTypeMemberVector4 member) { }
        public virtual void Matrix34Member(GeoTypeMemberMatrix34 member) { }
        public virtual void Matrix44Member(GeoTypeMemberMatrix44 member) { }
        public virtual void StringMember(GeoTypeMemberString member) { }
        public virtual void FileMember(GeoTypeMemberFile member) { }
        public virtual void ColorMember(GeoTypeMemberColor member) { }
        public virtual void EnumMember(GeoTypeMemberEnum member) { }

        public virtual bool BeginGroupMember(GeoTypeMemberGroup member) { return true; }
        public virtual void EndGroupMember(GeoTypeMemberGroup member) { }

        public virtual bool BeginNodeMember(GeoTypeMemberNode member) { return true; }
        public virtual void EndNodeMember(GeoTypeMemberNode member) { }

        public virtual bool BeginGeoMember(GeoTypeMemberGeo member) { return true; }
        public virtual void EndGeoMember(GeoTypeMemberGeo member) { }

        public virtual bool BeginArrayMember(GeoTypeMemberArray member) { return true; }
        public virtual void EndArrayMember(GeoTypeMemberArray member) { }

        public virtual bool BeginType(GeoType t) { return true; }
        public virtual void EndType(GeoType t) {}

        public virtual void Visit(GeoType type)
        {
            if (BeginType(type))
            {
                foreach (GeoTypeMemberBase m in type.m_Members)
                {
                    if (m != null)
                    {
                        VisitMember(m);
                    }                    
                }
            }
            EndType(type);
        }

        public virtual void VisitMember(GeoTypeMemberBase m)
        {
            if (m is GeoTypeMemberFloat)
            {
                FloatMember(m as GeoTypeMemberFloat);
            }
            else if (m is GeoTypeMemberBool)
            {
                BoolMember(m as GeoTypeMemberBool);
            }
            else if (m is GeoTypeMemberInt)
            {
                IntMember(m as GeoTypeMemberInt);
            }
            else if (m is GeoTypeMemberVector2)
            {
                Vector2Member(m as GeoTypeMemberVector2);
            }
            else if (m is GeoTypeMemberVector3)
            {
                Vector3Member(m as GeoTypeMemberVector3);
            }
            else if (m is GeoTypeMemberVector4)
            {
                Vector4Member(m as GeoTypeMemberVector4);
            }
            else if (m is GeoTypeMemberMatrix34)
            {
                Matrix34Member(m as GeoTypeMemberMatrix34);
            }
            else if (m is GeoTypeMemberMatrix44)
            {
                Matrix44Member(m as GeoTypeMemberMatrix44);
            }
            else if (m is GeoTypeMemberString)
            {
                StringMember(m as GeoTypeMemberString);
            }
            else if (m is GeoTypeMemberFile)
            {
                FileMember(m as GeoTypeMemberFile);
            }
            else if (m is GeoTypeMemberColor)
            {
                ColorMember(m as GeoTypeMemberColor);
            }
            else if (m is GeoTypeMemberEnum)
            {
                EnumMember(m as GeoTypeMemberEnum);
            }
            else if (m is GeoTypeMemberGroup)
            {
                GeoTypeMemberGroup g = m as GeoTypeMemberGroup;
                if (BeginGroupMember(g))
                {
                    foreach(GeoTypeMemberBase submember in g.Contents)
                    {
                        VisitMember(submember);
                    }
                }
                EndGroupMember(g);
            }
            else if (m is GeoTypeMemberNode)
            {
                GeoTypeMemberNode n = m as GeoTypeMemberNode;
                if (BeginNodeMember(n))
                {
                    foreach(GeoTypeMemberBase submember in n.Contents)
                    {
                        VisitMember(submember);
                    }
                }
                EndNodeMember(n);
            }	
            else if (m is GeoTypeMemberGeo)
            {
                GeoTypeMemberGeo g = m as GeoTypeMemberGeo;
                if (BeginGeoMember(g))
                {
                    if (g.GeoTypeThatIContain != null)
                    {
						Visit(g.GeoTypeThatIContain as GeoType);
                    }
                }
                EndGeoMember(g);

            }
            else if (m is GeoTypeMemberArray)
            {
				GeoTypeMemberArray a = m as GeoTypeMemberArray;
                if (BeginArrayMember(a))
                {
                    foreach (GeoTypeMemberBase submember in a)
                    {
                        VisitMember(submember);
                    }
                }
                EndArrayMember(a);
            }
        }
    }
}
