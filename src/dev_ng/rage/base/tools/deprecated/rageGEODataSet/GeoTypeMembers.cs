using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace rageGEODataSet
{

	#region GeoTypeMemberBool

	[ComVisible(true)]
	[Guid("1D572C30-6D1B-4648-B783-6DCDB46BD2E1")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberBool
	{
		bool Value { get; set; }
		string ValueAsString { get; }
		IGeoTemplateMemberBool TemplateMember { get; }
	}

	[ComVisible(true)]
	[Guid("A5D2D7B1-27F0-48b8-8A88-BAB3DAD71899")]
	[ProgId("rageGEODataSet.GeoTypeMemberBool")]
	public class GeoTypeMemberBool : GeoTypeMemberBase, IGeoTypeMemberBool
	{
		public GeoTypeMemberBool(bool v) { Value = v; }

		public event EventHandler ValueChanged;

		[XmlAttribute("value")]
		public bool Value
		{
			get
			{
				return m_Value;
			}
			set
			{
				if (value != m_Value)
				{
					m_Value = value;
					if (ValueChanged != null)
					{
						ValueChanged(this, System.EventArgs.Empty);
					}
				}
			}
		}

		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		private bool m_Value;

		public IGeoTemplateMemberBool TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as IGeoTemplateMemberBool;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.SetAttribute("value", Value.ToString());
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			Value = Boolean.Parse(elt.GetAttribute("value"));
		}
	}

	#endregion

	#region GeoTypeMemberInt

	[ComVisible(true)]
	[Guid("5B47138E-E93E-4945-A329-D1234175AA43")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberInt
	{
		Int32 Value { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("633C54AF-D5F4-47ff-9606-08681FDD9EF3")]
	[ProgId("rageGEODataSet.GeoTypeMemberInt")]
	public class GeoTypeMemberInt : GeoTypeMemberBase, IGeoTypeMemberInt
	{
		public GeoTypeMemberInt(Int32 v) { Value = v; }

		public event EventHandler ValueChanged;

		[XmlAttribute("value")]
		public Int32 Value
		{
			get
			{
				return m_Value;
			}
			set
			{
				if (m_Value != value)
				{
					m_Value = value;
					if (ValueChanged != null)
					{
						ValueChanged(this, EventArgs.Empty);
					}
				}
			}
		}

		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		private Int32 m_Value;

		public GeoTemplateMemberInt TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberInt;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.SetAttribute("value", Value.ToString());
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			Value = Int32.Parse(elt.GetAttribute("value"));
		}
	}

	#endregion

	#region GeoTypeMemberFloat

	[ComVisible(true)]
	[Guid("F992B80C-21CB-4926-A6EE-2684FD59E91D")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberFloat
	{
		double Value { get; set; }
		string ValueAsString { get; }
		IGeoTemplateMemberFloat TemplateMember { get; }
	}

	[ComVisible(true)]
	[Guid("1E22FC49-23E1-4d81-848B-84A01B6CB637")]
	[ProgId("rageGEODataSet.GeoTypeMemberFloat")]
	public class GeoTypeMemberFloat : GeoTypeMemberBase, IGeoTypeMemberFloat
	{
		public GeoTypeMemberFloat(double v) { Value = v; }

		public event EventHandler ValueChanged;

		[XmlAttribute("value")]
		public double Value
		{
			get
			{
				return m_Value;
			}
			set
			{
				if (m_Value != value)
				{
					m_Value = value;
					if (ValueChanged != null)
					{
						ValueChanged(this, EventArgs.Empty);
					}
				}
			}
		}

		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		private double m_Value;

		public IGeoTemplateMemberFloat TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as IGeoTemplateMemberFloat;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.SetAttribute("value", Value.ToString());
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			Value = Double.Parse(elt.GetAttribute("value"));
		}
	}

	#endregion

	#region GeoTypeMemberVector2

	[ComVisible(true)]
	[Guid("249FC133-626C-4e0c-8773-1191B683CC7B")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberVector2
	{
		Vector2 Value { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("DD0D9C5B-7158-4a75-880A-453A9C01E3FC")]
	[ProgId("rageGEODataSet.GeoTypeMemberVector2")]
	public class GeoTypeMemberVector2 : GeoTypeMemberBase, IGeoTypeMemberVector2
	{
		public GeoTypeMemberVector2(Vector2 v) { Value = v; }

		public GeoTemplateMemberVector2 TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberVector2;
			}
		}

		public event EventHandler ValueChanged;

		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		private Vector2 m_Value;

		public Vector2 Value
		{
			get
			{
				return m_Value;
			}
			set
			{
				if (m_Value != value)
				{
					m_Value = value;
					if (ValueChanged != null)
					{
						ValueChanged(this, EventArgs.Empty);
					}
				}
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.SetAttribute("x", Value.x.ToString());
			elt.SetAttribute("y", Value.y.ToString());
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			Value = new Vector2(
				Single.Parse(elt.GetAttribute("x")),
				Single.Parse(elt.GetAttribute("y"))
				);
		}
	}

	#endregion

	#region GeoTypeMemberVector3

	[ComVisible(true)]
	[Guid("C24AE772-6D3F-47a7-8144-5144EF221FC8")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberVector3
	{
		Vector3 Value { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("BA604283-3917-4eb9-9E00-35C085C47892")]
	[ProgId("rageGEODataSet.GeoTypeMemberVector3")]
	public class GeoTypeMemberVector3 : GeoTypeMemberBase, IGeoTypeMemberVector3
	{
		public GeoTypeMemberVector3(Vector3 v) { Value = v; }

		public event EventHandler ValueChanged;

		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		private Vector3 m_Value;

		public Vector3 Value
		{
			get
			{
				return m_Value;
			}
			set
			{
				if (m_Value != value)
				{
					m_Value = value;
					if (ValueChanged != null)
					{
						ValueChanged(this, EventArgs.Empty);
					}
				}
			}
		}

		public GeoTemplateMemberVector3 TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberVector3;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.SetAttribute("x", Value.x.ToString());
			elt.SetAttribute("y", Value.y.ToString());
			elt.SetAttribute("z", Value.z.ToString());
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			Value = new Vector3(
				Single.Parse(elt.GetAttribute("x")),
				Single.Parse(elt.GetAttribute("y")),
				Single.Parse(elt.GetAttribute("z"))
			);
		}
	}

	#endregion

	#region GeoTypeMemberVector4

	[ComVisible(true)]
	[Guid("F7FF57D5-0AA7-4a0f-B598-88B5F92363AA")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberVector4
	{
		Vector4 Value { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("37BDE452-FAB2-4917-A496-FA30BBFA2223")]
	[ProgId("rageGEODataSet.GeoTypeMemberVector4")]
	public class GeoTypeMemberVector4 : GeoTypeMemberBase, IGeoTypeMemberVector4
	{
		public GeoTypeMemberVector4(Vector4 v) { Value = v; }

		public event EventHandler ValueChanged;

		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		private Vector4 m_Value;

		public Vector4 Value
		{
			get
			{
				return m_Value;
			}
			set
			{
				if (m_Value != value)
				{
					m_Value = value;
					if (ValueChanged != null)
					{
						ValueChanged(this, EventArgs.Empty);
					}
				}
			}
		}

		public GeoTemplateMemberVector4 TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberVector4;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.SetAttribute("x", Value.x.ToString());
			elt.SetAttribute("y", Value.y.ToString());
			elt.SetAttribute("z", Value.z.ToString());
			elt.SetAttribute("w", Value.w.ToString());
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			Value = new Vector4(
				Single.Parse(elt.GetAttribute("x")),
				Single.Parse(elt.GetAttribute("y")),
				Single.Parse(elt.GetAttribute("z")),
				Single.Parse(elt.GetAttribute("w"))
			);
		}
	}

	#endregion

	#region GeoTypeMemberMatrix34

	[ComVisible(true)]
	[Guid("485DB80D-7F38-4905-83AE-DCB0B970E4A4")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberMatrix34
	{
		Matrix34 Value { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("CF82B149-5587-4a55-A32E-8E6B6608479B")]
	[ProgId("rageGEODataSet.GeoTypeMemberMatrix34")]
	public class GeoTypeMemberMatrix34 : GeoTypeMemberBase, IGeoTypeMemberMatrix34
	{
		public GeoTypeMemberMatrix34(Matrix34 v) { Value = v; }

		public event EventHandler ValueChanged;

		public Matrix34 Value
		{
			get { return m_Value; }
			set
			{
				if (m_Value != value)
				{
					m_Value = value;
					if (ValueChanged != null)
					{
						ValueChanged(this, EventArgs.Empty);
					}
				}
			}
		}

		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		private Matrix34 m_Value;

		public GeoTemplateMemberMatrix34 TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberMatrix34;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.SetAttribute("content", "vector3_array");
			System.Text.StringBuilder str = new System.Text.StringBuilder();
			str.Append(String.Format("{0} {1} {2}\n", Value.a.x, Value.a.y, Value.a.z));
			str.Append(String.Format("{0} {1} {2}\n", Value.b.x, Value.b.y, Value.b.z));
			str.Append(String.Format("{0} {1} {2}\n", Value.c.x, Value.c.y, Value.c.z));
			str.Append(String.Format("{0} {1} {2}\n", Value.d.x, Value.d.y, Value.d.z));
			elt.AppendChild(doc.CreateTextNode(str.ToString()));
		}

		// Don't need to do this in .Net 2.0
		public static string[] RemoveEmpties(string[] list)
		{
			// count empties
			int empties = 0;
			foreach (string i in list)
			{
				if (i == String.Empty)
				{
					empties++;
				}
			}

			string[] ret = new string[list.Length - empties];
			int retIndex = 0;
			foreach (string i in list)
			{
				if (i != String.Empty)
				{
					ret[retIndex] = i;
					retIndex++;
				}
			}

			return ret;
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);

			Debug.Assert(String.Compare(elt.GetAttribute("content"), "vector3_array", true) == 0);

			string str = elt.InnerText;

			string[] numbers = RemoveEmpties(str.Split(null));

			Debug.Assert(numbers.Length == 12);

			Value = new Matrix34(
				new Vector3(Single.Parse(numbers[0]), Single.Parse(numbers[1]), Single.Parse(numbers[2])),
				new Vector3(Single.Parse(numbers[3]), Single.Parse(numbers[4]), Single.Parse(numbers[5])),
				new Vector3(Single.Parse(numbers[6]), Single.Parse(numbers[7]), Single.Parse(numbers[8])),
				new Vector3(Single.Parse(numbers[9]), Single.Parse(numbers[10]), Single.Parse(numbers[11]))
				);
		}
	}

	#endregion

	#region GeoTypeMemberMatrix44

	[ComVisible(true)]
	[Guid("CA5818FA-C7A3-4ed2-95BA-8E2F82D2B459")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberMatrix44
	{
		Matrix44 Value { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("0C84DB5B-793F-495f-A6CD-C6F603C98026")]
	[ProgId("rageGEODataSet.GeoTypeMemberMatrix44")]
	public class GeoTypeMemberMatrix44 : GeoTypeMemberBase
	{
		public GeoTypeMemberMatrix44(Matrix44 v) { Value = v; }

		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		public Matrix44 m_Value;
		public Matrix44 Value
		{
			get { return m_Value; }
			set { m_Value = value; }
		}

		public GeoTemplateMemberMatrix44 TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberMatrix44;
			}
		}

	}

	#endregion

	#region GeoTypeMemberArray

	[ComVisible(true)]
	[Guid("631030BA-354C-4581-BB5F-19FB3FD09281")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberArray
	{
		Int32 GetElementCount();
		IGeoTypeMemberBase GetElement(Int32 idx);
		void AddElement(IGeoTypeMemberBase mbr);
		IGeoTypeMemberBase RemoveElementAt(Int32 idx);
		IGeoTypeMemberBase RemoveElementByUUID(String uuid);

		IGeoTemplateMemberArray TemplateMember { get; }
		string UUID { get; set; }
	}

	[ComVisible(true)]
	[Guid("B78F4ABB-8212-4d58-BC2C-EFFB4144AE3A")]
	[ProgId("rageGEODataSet.GeoTypeMemberArray")]
	public class GeoTypeMemberArray : GeoTypeMemberBase, IEnumerable<GeoTypeMemberBase>, IGeoTypeMemberArray
	{
		private string m_UUID;
		private List<GeoTypeMemberBase> m_Contents;

		public GeoTypeMemberArray()
		{
			m_Contents = new List<GeoTypeMemberBase>();
			m_UUID = GeoUUID.CreateUUID();
		}

		public override string UUID
		{
			get { return m_UUID; }
			set { m_UUID = value; }
		}

		public Int32 GetElementCount()
		{
			return m_Contents.Count;
		}

		public IGeoTypeMemberBase GetElement(Int32 idx)
		{
			return (IGeoTypeMemberBase)m_Contents[idx];
		}

		public List<GeoTypeMemberBase> Elements
		{
			get
			{
				return m_Contents;
			}
		}

		public void AddElement(IGeoTypeMemberBase mbr)
		{
			// Wire it up
			mbr.ContainingTypeMember = this;

			// Add it
			m_Contents.Add((GeoTypeMemberBase)mbr);
		}

		public IGeoTypeMemberBase RemoveElement(GeoTypeMemberBase mbr)
		{
			m_Contents.Remove(mbr);
			return (mbr as IGeoTypeMemberBase);
		}

		public IGeoTypeMemberBase RemoveElementAt(Int32 idx)
		{
			IGeoTypeMemberBase elm = (IGeoTypeMemberBase)m_Contents[idx];
			m_Contents.RemoveAt(idx);
			return elm;
		}

		public IGeoTypeMemberBase RemoveElementByUUID(String uuid)
		{
			IGeoTypeMemberBase elm = null;
			Int32 elmIdx = -1;
			Int32 count = 0;
			foreach (GeoTypeMemberBase mbr in m_Contents)
			{
				if (mbr.UUID == uuid)
				{
					elmIdx = count;
					elm = m_Contents[elmIdx];
					break;
				}
				count++;
			}
			if (elmIdx != -1)
			{
				m_Contents.RemoveAt(elmIdx);
				return elm;
			}
			else
				return null;
		}

		public IGeoTemplateMemberArray TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as IGeoTemplateMemberArray;
			}
		}

		public override List<GeoTypeMemberBase> ChildMembers
		{
			get
			{
				return m_Contents;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);

			if (m_UUID != String.Empty)
				elt.SetAttribute("uuid", m_UUID);

			foreach (GeoTypeMemberBase m in m_Contents)
			{
				m.Save(doc, elt);
			}
		}

		public override void FinishLoad()
		{
			foreach (GeoTypeMemberBase m in m_Contents)
			{
				m.FinishLoad();
			}
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			m_UUID = elt.GetAttribute("uuid");
			foreach (XmlElement child in elt.ChildNodes)
			{
				// create child of the appropriate type
				GeoTypeMemberBase member = (GeoTypeMemberBase)(TemplateMember.ChildTemplate.CreateTypeMember(GeoTypeThatContainsMe));
				member.LoadData(child);
				member.m_ContainingTypeMember = this;
				m_Contents.Add(member);
			}
		}

		protected override void DeepCopy(GeoTypeMemberBase dest)
		{
			base.DeepCopy(dest);

			GeoTypeMemberArray realDest = dest as GeoTypeMemberArray;
			realDest.m_Contents = new List<GeoTypeMemberBase>();
			foreach (GeoTypeMemberBase m in m_Contents)
			{
				GeoTypeMemberBase clone = m.Clone();
				clone.GeoTypeThatContainsMe = realDest.GeoTypeThatContainsMe;
				clone.m_ContainingTypeMember = realDest;
				realDest.m_Contents.Add(clone);
			}
		}

		#region IEnumerable<GeoTypeMemberBase> Interface

		public IEnumerator<GeoTypeMemberBase> GetEnumerator()
		{
			return m_Contents.GetEnumerator();
		}
		IEnumerator IEnumerable.GetEnumerator()
		{
			return m_Contents.GetEnumerator();
		}

		#endregion
	}

	#endregion

	#region GeoTypeMemberString

	[ComVisible(true)]
	[Guid("F9356339-E9F3-49c2-A004-D3F5517193D6")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberString
	{
		string Value { get; set; }
		string ValueAsString { get; }
		IGeoTemplateMemberString TemplateMember { get; }
	}

	[ComVisible(true)]
	[Guid("EA4558E8-363E-451d-B5E7-788F5087049D")]
	[ProgId("rageGEODataSet.GeoTypeMemberString")]
	public class GeoTypeMemberString : GeoTypeMemberBase, IGeoTypeMemberString
	{
		public GeoTypeMemberString() { }
		public GeoTypeMemberString(string v) { Value = v; }

		public event EventHandler ValueChanged;

		[XmlText]
		public string Value
		{
			get
			{
				return m_Value;
			}
			set
			{
				if (m_Value != value)
				{
					m_Value = value;
					if (ValueChanged != null)
					{
						ValueChanged(this, EventArgs.Empty);
					}
				}
			}
		}

		private string m_Value = "";

        public override string ValueAsString
        {
            get
            {
                return m_Value;
            }
        }

		public IGeoTemplateMemberString TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as IGeoTemplateMemberString;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.AppendChild(doc.CreateTextNode(Value));
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			Value = elt.InnerText;
		}
	}

	#endregion

	#region GeoTypeMemberGeo

	[ComVisible(true)]
	[Guid("639A977B-5DAE-411d-9AC7-76B23645DF8F")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberGeo
	{
		IGeoType GeoTypeThatIContain { get; set; }
		IGeoTemplateMemberGeo TemplateMember { get; }
		string UUID { get; set; }
	}

	[ComVisible(true)]
	[Guid("548A6814-DAB1-41fb-8BCB-EC7D7BEE6591")]
	[ProgId("rageGEODataSet.GeoTypeMemberGeo")]
	public class GeoTypeMemberGeo : GeoTypeMemberBase, IGeoTypeMemberGeo
	{
		public GeoTypeMemberGeo()
		{
			m_UUID = GeoUUID.CreateUUID();
		}

		public string m_UUID;
		public override string UUID
		{
			get { return m_UUID; }
			set { m_UUID = value; }
		}

		private GeoType m_GeoTypeIContain;
		public IGeoType GeoTypeThatIContain
		{
			get { return m_GeoTypeIContain; }
			set
			{
				m_GeoTypeIContain = (GeoType)value;
			}
		}

		public IGeoTemplateMemberGeo TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as IGeoTemplateMemberGeo;
			}
		}

		public override void FinishLoad()
		{
			if (m_GeoTypeIContain != null)
			{
				if ((m_GeoTypeIContain.BaseTypeName != String.Empty) &&
					(m_GeoTypeIContain.BaseTypeName != null))
				{
					try
					{
						m_GeoTypeIContain.BaseType = GeoDataSet.CurrentDataSet.m_GeoTypes[m_GeoTypeIContain.BaseTypeName];
					}
					catch (Exception ex)
					{
						throw new ApplicationException("An exception occured trying to find \"" + m_GeoTypeIContain.BaseTypeName + "\" in " + GeoDataSet.CurrentDataSet.m_GeoTypes, ex);
					}
				}
				else
				{
					if (GeoTypeThatContainsMe.BaseType != null)
					{
						string memberPath = FindPathToMember(false, false);
						memberPath = memberPath.Substring(memberPath.IndexOf('/') + 1);
						GeoTypeMemberBase memberInBaseType = GeoType.SearchForMemberVisitor.Search(m_GeoTypeThatContainsMe.m_BaseType, memberPath);

						// GeoTypeMemberBase mbrBase = m_GeoTypeThatContainsMe.m_BaseType.FindMember(memberPath);
						GeoTypeMemberGeo mbrGeo = memberInBaseType as GeoTypeMemberGeo;
						if (mbrGeo != null)
						{
							m_GeoTypeIContain.BaseType = mbrGeo.m_GeoTypeIContain;
						}
					}
				}

                m_GeoTypeIContain.FinishLoad();
			}
		}

		protected override void DeepCopy(GeoTypeMemberBase dest)
		{
			base.DeepCopy(dest);
			GeoTypeMemberGeo destGeo = dest as GeoTypeMemberGeo;
			if (m_GeoTypeIContain != null)
			{
				destGeo.m_GeoTypeIContain = m_GeoTypeIContain.m_Template.CreateTypeInternal(false);
				Debug.Assert(destGeo.m_GeoTypeIContain != null);
				destGeo.m_GeoTypeIContain.DeepCopyFrom(m_GeoTypeIContain);
				destGeo.m_GeoTypeIContain.m_ContainingMember = destGeo;
			}
		}


		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);

			if (m_UUID != String.Empty)
				elt.SetAttribute("mbruuid", m_UUID);

			if (m_GeoTypeIContain != null)
			{
				if (m_GeoTypeIContain.IsInstance)
				{
					elt.SetAttribute("type", m_GeoTypeIContain.m_Template.InstanceElementName);

					Debug.Assert(m_GeoTypeIContain.m_BaseType != null, "m_ReferenceType.m_BaseType != null");
					elt.SetAttribute("base", m_GeoTypeIContain.m_BaseType.Name);

					elt.SetAttribute("instuuid", m_GeoTypeIContain.UUID);
				}
				else
				{
					elt.SetAttribute("type", m_GeoTypeIContain.m_Template.TypeElementName);
					if (m_GeoTypeIContain.m_BaseType != null)
					{
						elt.SetAttribute("base", m_GeoTypeIContain.m_BaseType.Name);
					}
				}
				elt.SetAttribute("template", m_GeoTypeIContain.m_Template.FullName);
				m_GeoTypeIContain.Save(doc, elt);
			}
			else
			{
				elt.SetAttribute("type", "NULL");
				elt.SetAttribute("template", "NULL");
			}
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);

			this.UUID = elt.GetAttribute("mbruuid");

			string typeName = elt.GetAttribute("template").ToLower();

            // TODO: LOG WARNINGS OR ERRORS HERE IF SOMEONE TRIES TO CREATE A NODE WITHOUT A TEMPLATE
			if (typeName == "NULL")
			{
				m_GeoTypeIContain = null;
			}
			else
			{
				GeoTemplate template = null;
				GeoDataSet.CurrentDataSet.m_GeoTemplates.TryGetValue(typeName, out template);
				if (template != null)
				{
					m_GeoTypeIContain = template.CreateTypeInternal(false);
					Debug.Assert(m_GeoTypeIContain != null);
					m_GeoTypeIContain.m_ContainingMember = this;
					m_GeoTypeIContain.Load(elt);
					m_GeoTypeIContain.UUID = elt.GetAttribute("instuuid");
					m_GeoTypeIContain.BaseTypeName = elt.GetAttribute("base");

					//HACK : If the UUID is available for the reference type, then this is an instance
					//There is no other way to get this information because the containing type's instance
					//flag isn't set until after this LoadData method has been called.
					if (m_GeoTypeIContain.UUID != String.Empty)
						m_GeoTypeIContain.IsInstance = true;
				}
			}
		}
	}

	#endregion

	#region GeoTypeMemberNode

	[ComVisible(true)]
	[Guid("D344559E-1DF0-46fc-94E7-513C6559856C")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberNode
	{
		Int32 GetElementCount();
		IGeoTypeMemberBase GetElement(Int32 idx);
		IGeoTypeMemberBase GetElementByName(String name);
		bool IsConnected { get; set; }
	}

	[ComVisible(true)]
	[Guid("81DB8200-BDB6-41c7-B65F-45023EC9F2F9")]
	[ProgId("rageGEODataSet.GeoTypeMemberNode")]
	public class GeoTypeMemberNode : GeoTypeMemberBase, IGeoTypeMemberNode
	{
		public List<GeoTypeMemberBase> Contents = new List<GeoTypeMemberBase>();

		public Int32 GetElementCount()
		{
			return Contents.Count;
		}
		public IGeoTypeMemberBase GetElement(Int32 idx)
		{
			return (IGeoTypeMemberBase)Contents[idx];
		}
		public IGeoTypeMemberBase GetElementByName(String name)
		{
			foreach (GeoTypeMemberBase mbrBase in Contents)
			{
				if (mbrBase.Name == name)
					return mbrBase;
			}
			return null;
		}

		public GeoTypeMemberNode() { }

		private bool m_IsConnected;
		public bool IsConnected
		{
			get
			{
				return m_IsConnected;
			}
			set
			{
				m_IsConnected = value;
			}
		}

		public GeoTemplateMemberNode TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberNode;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.SetAttribute("isConnected", IsConnected ? "true" : "false");
		}
		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			IsConnected = (elt.GetAttribute("isConnected") == "true");
		}
	}

	#endregion

	#region GeoTypeMemberFile

	[ComVisible(true)]
	[Guid("D1F1C446-676E-4eef-A31F-36C54237F856")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberFile
	{
		string Value { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("D7A841FF-4826-41a6-9F2A-827277E731FF")]
	[ProgId("rageGEODataSet.GeoTypeMemberFile")]
	public class GeoTypeMemberFile : GeoTypeMemberBase, IGeoTypeMemberFile
	{
		public GeoTypeMemberFile(string v) { Value = v; }
		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		private string m_Value;
		public string Value
		{
			get { return m_Value; }
			set { m_Value = value; }
		}

		public GeoTemplateMemberFile TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberFile;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.AppendChild(doc.CreateTextNode(m_Value));
		}
		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			m_Value = elt.InnerText;
		}
	}

	#endregion

	#region GeoTypeMemberBinary

	[ComVisible(true)]
	[Guid("14320213-4302-46a3-814A-F7A4FF69EE28")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberBinary
	{
		byte[] Value { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("134D791A-4674-49e4-9CE6-C468F85B105E")]
	[ProgId("rageGEODataSet.GeoTypeMemberBinary")]
	public class GeoTypeMemberBinary : GeoTypeMemberBase, IGeoTypeMemberBinary
	{
		public GeoTypeMemberBinary(byte[] v) { Value = v; }
		public override string ValueAsString
		{
			get
			{
				return "BinaryData";
			}
		}

		private byte[] m_Value;
		public byte[] Value
		{
			get { return m_Value; }
			set { m_Value = value; }
		}

		public GeoTemplateMemberBinary TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberBinary;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.AppendChild(doc.CreateTextNode(System.Convert.ToBase64String(m_Value)));
			elt.SetAttribute("content", "binary");
		}
		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			m_Value = System.Convert.FromBase64String(elt.InnerText);
		}
	}

	#endregion

	#region GeoTypeMemberColor

	[ComVisible(true)]
	[Guid("F7F0CB6D-96DE-42bb-90E3-A5410029D300")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberColor
	{
		System.Drawing.Color Value { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("5BF4EDCB-35E1-4f91-A619-A0BEB74A416D")]
	[ProgId("rageGEODataSet.GeoTypeMemberColor")]
	public class GeoTypeMemberColor : GeoTypeMemberBase, IGeoTypeMemberColor
	{
		public GeoTypeMemberColor(System.Drawing.Color v) { Value = v; }
		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		private System.Drawing.Color m_Value;
		public System.Drawing.Color Value
		{
			get { return m_Value; }
			set { m_Value = value; }
		}

		public GeoTemplateMemberColor TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberColor;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);

			switch (TemplateMember.Storage)
			{
				case GeoTemplateMemberColor.StorageType.Color32:
					uint a = (uint)(m_Value.A * 255.0f);
					uint r = (uint)(m_Value.R * 255.0f);
					uint g = (uint)(m_Value.G * 255.0f);
					uint b = (uint)(m_Value.B * 255.0f);
					uint color = ((a << 24) | (r << 16) | (g << 8) | b);
					elt.SetAttribute("value", color.ToString());
					break;
				case GeoTemplateMemberColor.StorageType.Vector3:
					elt.SetAttribute("x", m_Value.R.ToString());
					elt.SetAttribute("y", m_Value.G.ToString());
					elt.SetAttribute("z", m_Value.B.ToString());
					break;
				case GeoTemplateMemberColor.StorageType.Vector4:
					elt.SetAttribute("x", m_Value.R.ToString());
					elt.SetAttribute("y", m_Value.G.ToString());
					elt.SetAttribute("z", m_Value.B.ToString());
					elt.SetAttribute("w", m_Value.A.ToString());
					break;
			}
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			switch (TemplateMember.Storage)
			{
				case GeoTemplateMemberColor.StorageType.Color32:
					uint color = UInt32.Parse(elt.GetAttribute("value"));
                    m_Value = System.Drawing.Color.FromArgb(
                        (int)((color >> 24) & 0xFF),
                        (int)((color >> 16) & 0xFF),
                        (int)((color >> 8) & 0xFF),
                        (int)((color >> 0) & 0xFF));
					break;
				case GeoTemplateMemberColor.StorageType.Vector3:
                    m_Value = System.Drawing.Color.FromArgb(
                        255,
                        Convert.ToInt32( 255 * Single.Parse(elt.GetAttribute("x"))),
                        Convert.ToInt32( 255 * Single.Parse(elt.GetAttribute("y"))),
                        Convert.ToInt32( 255 * Single.Parse(elt.GetAttribute("z"))));
					break;
				case GeoTemplateMemberColor.StorageType.Vector4:
                    m_Value = System.Drawing.Color.FromArgb( 
					    Convert.ToInt32( 255 * Single.Parse(elt.GetAttribute("x"))),
					    Convert.ToInt32( 255 * Single.Parse(elt.GetAttribute("y"))),
					    Convert.ToInt32( 255 * Single.Parse(elt.GetAttribute("z"))),
                        Convert.ToInt32(255 * Single.Parse(elt.GetAttribute("w"))));
					break;
			}
		}
	}

	#endregion

	#region GeoTypeMemberGroup

	[ComVisible(true)]
	[Guid("365E9EA4-33C9-46a0-B849-DE1B0D86AE6D")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberGroup
	{
		Int32 GetElementCount();
		IGeoTypeMemberBase GetElement(Int32 idx);
	}

	[ComVisible(true)]
	[Guid("30C39570-C249-496b-8726-7D56B8887FA1")]
	[ProgId("rageGEODataSet.GeoTypeMemberGroup")]
	public class GeoTypeMemberGroup : GeoTypeMemberBase, IGeoTypeMemberGroup
	{
		public List<GeoTypeMemberBase> Contents = new List<GeoTypeMemberBase>();

		public Int32 GetElementCount()
		{
			return Contents.Count;
		}
		public IGeoTypeMemberBase GetElement(Int32 idx)
		{
			return (IGeoTypeMemberBase)Contents[idx];
		}

		public GeoTemplateMemberGroup TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberGroup;
			}
		}

		public override List<GeoTypeMemberBase> ChildMembers
		{
			get
			{
				return Contents;
			}
		}


		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);

			foreach (GeoTypeMemberBase m in Contents)
			{
				// only non-overridden members go here
				if (!m.m_UseInheritedValue)
				{
					m.Save(doc, elt);
				}
			}
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);

			foreach (GeoTypeMemberBase m in Contents)
			{
				m.Load(elt);
				m.m_ContainingTypeMember = this;
				m.GeoTypeThatContainsMe = this.GeoTypeThatContainsMe;
			}
		}


		protected override void DeepCopy(GeoTypeMemberBase dest)
		{
			base.DeepCopy(dest);
			GeoTypeMemberGroup realDest = dest as GeoTypeMemberGroup;
			realDest.Contents = new List<GeoTypeMemberBase>();
			foreach (GeoTypeMemberBase m in Contents)
			{
				GeoTypeMemberBase clone = m.Clone();
				clone.GeoTypeThatContainsMe = realDest.GeoTypeThatContainsMe;
				clone.m_ContainingTypeMember = realDest;
				realDest.Contents.Add(clone);
			}
		}
	}
	#endregion

	#region GeoTypeMemberEnum

	[ComVisible(true)]
	[Guid("5B7F3C18-7C9F-48d1-8ABD-2C30DF59E621")]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	public interface IGeoTypeMemberEnum
	{
		string Value { get; set; }
		string ValueAsString { get; }
	}

	[ComVisible(true)]
	[Guid("CC1E2585-6707-4a6c-8C07-3913CF727B5F")]
	[ProgId("rageGEODataSet.GeoTypeMemberEnum")]
	public class GeoTypeMemberEnum : GeoTypeMemberBase, IGeoTypeMemberEnum
	{
		public GeoTypeMemberEnum(string s)
		{
			Value = s;
		}

		public event EventHandler ValueChanged;

		[XmlText]
		public string Value
		{
			get { return m_Value; }
			set
			{
				if (m_Value != value)
				{
					m_Value = value;
					if (ValueChanged != null)
					{
						ValueChanged(this, EventArgs.Empty);
					}
				}
			}
		}

		public override string ValueAsString
		{
			get
			{
				return Value.ToString();
			}
		}

		private string m_Value;

		public GeoTemplateMemberEnum TemplateMember
		{
			get
			{
				return m_TemplateMemberBase as GeoTemplateMemberEnum;
			}
		}

		public override void Save(XmlDocument doc, XmlElement parent)
		{
			XmlElement elt = CreateElement(doc, parent);
			elt.AppendChild(doc.CreateTextNode(Value));
		}

		public override void LoadData(XmlElement elt)
		{
			base.LoadData(elt);
			Value = elt.InnerText;
		}
	}

	#endregion

}
