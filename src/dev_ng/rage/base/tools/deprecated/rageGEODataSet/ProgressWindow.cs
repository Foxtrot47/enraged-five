using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace rageGEODataSet
{
	public partial class ProgressWindow : Form
	{
		public ProgressWindow()
		{
			InitializeComponent();

			// EVIL HACK!!!!!!
			Form.CheckForIllegalCrossThreadCalls = false;
			// EVIL HACK!!!!!!
		}

		public void OpenWindow()
		{
			Thread obShowWindowThread = new Thread(delegate()
				{
					if((!this.IsDisposed) && (!this.Disposing))
					{
						CreateControl();
						this.ShowDialog();
					}
					else
					{
						// Console.WriteLine(this.infoLabel.Text +" Not showing the progress window as it has already been disposed of");
					}
				});
			obShowWindowThread.Name = "ProgressWindow.ShowWindow Thread";
			obShowWindowThread.Start();
		}

		public void Done()
		{
			// Wait for it to open, before closing it
			do
			{
				Thread.Sleep(100);
			} while (!this.Visible || !this.Created || (this.Handle == IntPtr.Zero));

			//Close dialogue
			this.Invoke(new System.Windows.Forms.MethodInvoker(
				delegate()
				{
					do
					{
						Thread.Sleep(100);
					} while (!this.Visible || !this.Created || (this.Handle == IntPtr.Zero));
					this.Close();
				}
				));
			do
			{
				Thread.Sleep(100);
			} while (this.Visible);
		}

		private delegate void SetProgressBarMaxCallback(int iMax);
		public void SetProgressBarMax(int iMax)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (progressBar.InvokeRequired)
			{
				SetProgressBarMaxCallback d = new SetProgressBarMaxCallback(SetProgressBarMax);
				this.Invoke(d, new object[] { iMax });
			}
			else
			{
				progressBar.Maximum = iMax;
			}
		}


		private delegate void SetProgressBarMinCallback(int iMin);
		public void SetProgressBarMin(int iMin)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (progressBar.InvokeRequired)
			{
				SetProgressBarMinCallback d = new SetProgressBarMinCallback(SetProgressBarMin);
				this.Invoke(d, new object[] { iMin });
			}
			else
			{
				progressBar.Minimum = iMin;
			}
		}


		private delegate void SetProgressBarValueCallback(int iValue);
		public void SetProgressBarValue(int iValue)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (progressBar.InvokeRequired)
			{
				SetProgressBarValueCallback d = new SetProgressBarValueCallback(SetProgressBarValue);
				this.Invoke(d, new object[] { iValue });
			}
			else
			{
				progressBar.Value = iValue;
			}
		}

		private delegate void IncProgressBarValueCallback();
		public void IncProgressBarValue()
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (progressBar.InvokeRequired)
			{
				IncProgressBarValueCallback d = new IncProgressBarValueCallback(IncProgressBarValue);
				this.Invoke(d, new object[] {});
			}
			else
			{
				progressBar.Value++;
			}
		}


		private delegate void SetLabelTextCallback(string strText);
		public void SetLabelText(string strText)
		{
			// InvokeRequired required compares the thread ID of the
			// calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (infoLabel.InvokeRequired)
			{
				SetLabelTextCallback d = new SetLabelTextCallback(SetLabelText);
				this.Invoke(d, new object[] { strText });
			}
			else
			{
				infoLabel.Text = strText;
			}
		}
	}
}

