namespace rageGEODataSet
{
    partial class LoadingSplash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.m_loadingCircle = new MRG.Controls.UI.LoadingCircle();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label1.Location = new System.Drawing.Point(19, 95);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(149, 24);
			this.label1.TabIndex = 1;
			this.label1.Text = "Loading GEOs...";
			// 
			// m_loadingCircle
			// 
			this.m_loadingCircle.Active = false;
			this.m_loadingCircle.Color = System.Drawing.Color.DarkGray;
			this.m_loadingCircle.InnerCircleRadius = 15;
			this.m_loadingCircle.Location = new System.Drawing.Point(15, 1);
			this.m_loadingCircle.Name = "m_loadingCircle";
			this.m_loadingCircle.NumberSpoke = 10;
			this.m_loadingCircle.OuterCircleRadius = 35;
			this.m_loadingCircle.RotationSpeed = 100;
			this.m_loadingCircle.Size = new System.Drawing.Size(160, 103);
			this.m_loadingCircle.SpokeThickness = 8;
			this.m_loadingCircle.TabIndex = 0;
			this.m_loadingCircle.Text = "loadingCircle1";
			// 
			// LoadingSplash
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(191, 132);
			this.ControlBox = false;
			this.Controls.Add(this.label1);
			this.Controls.Add(this.m_loadingCircle);
			this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "LoadingSplash";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "LoadingSplash";
			this.UseWaitCursor = true;
			this.Load += new System.EventHandler(this.OnLoad);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private MRG.Controls.UI.LoadingCircle m_loadingCircle;
        private System.Windows.Forms.Label label1;
    }
}