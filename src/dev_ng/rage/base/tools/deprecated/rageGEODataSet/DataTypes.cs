using System;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.InteropServices;

namespace rageGEODataSet
{
    [ComVisible(true)]
    [Guid("F952FDFB-7001-4f9a-AF71-E08CCF65D9DF")]
    public struct Vector2
    {
        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public Vector2(float v)
        {
            x = y = v;
        }

        public static bool operator ==(Vector2 vecA, Vector2 vecB)
        {
            return (
                vecA.x == vecB.x &&
                vecA.y == vecB.y);
        }

        public static bool operator !=(Vector2 vecA, Vector2 vecB)
        {
            return !(vecA == vecB);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj); // Prevents compiler warning
        }

        public override int GetHashCode()
        {
            return base.GetHashCode(); // Prevents compiler warning
        }

        public override string ToString()
        {
            return String.Format("{0}, {1}", x, y);
        }

        public float X
        {
            get { return x; }
            set { x = value; }
        }

        public float Y
        {
            get { return y; }
            set { y = value; }
        }

        [XmlAttribute]
        public float x, y;

        public static readonly Vector2 Origin = new Vector2(0.0f, 0.0f);
    }

    [ComVisible(true)]
    [Guid("5225BB68-EB4B-4d16-BEDF-71C54C078635")]
    public struct Vector3
    {
        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3(float v)
        {
            x = y = z = v;
        }

        public static bool operator ==(Vector3 vecA, Vector3 vecB)
        {
            return (
                vecA.x == vecB.x &&
                vecA.y == vecB.y &&
                vecA.z == vecB.z);
        }

        public static bool operator !=(Vector3 vecA, Vector3 vecB)
        {
            return !(vecA == vecB);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj); // Prevents compiler warning
        }

        public override int GetHashCode()
        {
            return base.GetHashCode(); // Prevents compiler warning
        }

        public override string ToString()
        {
            return String.Format("{0}, {1}, {2}", x, y, z);
        }

        public float X
        {
            get { return x; }
            set { x = value; }
        }

        public float Y
        {
            get { return y; }
            set { y = value; }
        }

        public float Z
        {
            get { return z; }
            set { z = value; }
        }

        [XmlAttribute]
        public float x, y, z;

        public static readonly Vector3 Origin = new Vector3(0.0f, 0.0f, 0.0f);
    }

    [ComVisible(true)]
    [Guid("2CE205B5-2CAF-4af5-A258-868F8DC3652B")]
    public struct Vector4
    {
        public Vector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public Vector4(float v)
        {
            x = y = z = w = v;
        }

        public static bool operator ==(Vector4 vecA, Vector4 vecB)
        {
            return (
                vecA.x == vecB.x &&
                vecA.y == vecB.y &&
                vecA.z == vecB.z &&
                vecA.w == vecB.w);
        }

        public static bool operator !=(Vector4 vecA, Vector4 vecB)
        {
            return !(vecA == vecB);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj); // Prevents compiler warning
        }

        public override int GetHashCode()
        {
            return base.GetHashCode(); // Prevents compiler warning
        }

        public override string ToString()
        {
            return String.Format("{0}, {1}, {2}, {3}", x, y, z, w);
        }

        public float X
        {
            get { return x; }
            set { x = value; }
        }

        public float Y
        {
            get { return y; }
            set { y = value; }
        }

        public float Z
        {
            get { return z; }
            set { z = value; }
        }

        public float W
        {
            get { return w; }
            set { w = value; }
        }

        [XmlAttribute]
        public float x, y, z, w;

        public static readonly Vector4 Origin = new Vector4(0.0f, 0.0f, 0.0f, 0.0f);
    }

    [ComVisible(true)]
    [Guid("42B19E24-234D-4601-9D96-04B3E872BF59")]
    public struct Matrix34
    {
        public Matrix34(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }

        public Matrix34(float v)
        {
            this.a = new Vector3(v);
            this.b = new Vector3(v);
            this.c = new Vector3(v);
            this.d = new Vector3(v);
        }

        public static bool operator ==(Matrix34 matA, Matrix34 matB)
        {
            return (
                matA.a == matB.a &&
                matA.b == matB.b &&
                matA.c == matB.c &&
                matA.d == matB.d);
        }

        public static bool operator !=(Matrix34 matA, Matrix34 matB)
        {
            return !(matA == matB);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj); // Prevents compiler warning
        }

        public override int GetHashCode()
        {
            return base.GetHashCode(); // Prevents compiler warning
        }

        public override string ToString()
        {
            return String.Format("{{0}}, {{1}}, {{2}}, {{3}}", a.ToString(), b.ToString(), c.ToString(), d.ToString());
        }

        public Vector3 A
        {
            get { return a; }
            set { a = value; }
        }

        public Vector3 B
        {
            get { return b; }
            set { b = value; }
        }

        public Vector3 C
        {
            get { return c; }
            set { c = value; }
        }

        public Vector3 D
        {
            get { return d; }
            set { d = value; }
        }

        [XmlElement]
        public Vector3 a, b, c, d;

        public static readonly Matrix34 Identity = new Matrix34(
            new Vector3(1.0f, 0.0f, 0.0f),
            new Vector3(0.0f, 1.0f, 0.0f),
            new Vector3(0.0f, 0.0f, 1.0f),
            new Vector3(0.0f, 0.0f, 0.0f));

        public static Matrix34 FromTransAndRot(Vector3 trans, Vector3 rot)
        {
            Matrix34 ret;
            ret.d = trans;

            float sx, sy, sz, cx, cy, cz;
            if (rot.x == 0.0f) { sx = 0.0f; cx = 1.0f; }
            else { cx = (float)Math.Cos(rot.x); sx = (float)Math.Sin(rot.x); }
            if (rot.y == 0.0f) { sy = 0.0f; cy = 1.0f; }
            else { cy = (float)Math.Cos(rot.y); sy = (float)Math.Sin(rot.y); }
            if (rot.z == 0.0f) { sz = 0.0f; cz = 1.0f; }
            else { cz = (float)Math.Cos(rot.z); sz = (float)Math.Sin(rot.z); }

            ret.a = new Vector3(cy * cz, cy * sz, -sy);
            ret.b = new Vector3(sx * sy * cz - cx * sz, sx * sy * sz + cx * cz, sx * cy);
            ret.c = new Vector3(cx * sy * cz + sx * sz, cx * sy * sz - sx * cz, cx * cy);

            return ret;
        }

        public void ToTransAndRot(out Vector3 trans, out Vector3 rot)
        {
            trans = d;

            rot = new Vector3(
                (float)Math.Atan2(b.z, c.z),
                (float)Math.Asin(-a.z),
                (float)Math.Atan2(a.y, a.x)
                );
        }

    }

    [ComVisible(true)]
    [Guid("EDA8D2B7-171E-4fb2-9DB9-B3AA56CB5F02")]
    public struct Matrix44
    {
        public Matrix44(Vector4 a, Vector4 b, Vector4 c, Vector4 d)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }

        public Matrix44(float v)
        {
            this.a = new Vector4(v);
            this.b = new Vector4(v);
            this.c = new Vector4(v);
            this.d = new Vector4(v);
        }

        public Vector4 A
        {
            get { return a; }
            set { a = value; }
        }

        public Vector4 B
        {
            get { return b; }
            set { b = value; }
        }

        public Vector4 C
        {
            get { return c; }
            set { c = value; }
        }

        public Vector4 D
        {
            get { return d; }
            set { d = value; }
        }

        [XmlElement]
        public Vector4 a, b, c, d;

        public static readonly Matrix44 Identity = new Matrix44(
            new Vector4(1.0f, 0.0f, 0.0f, 0.0f),
            new Vector4(0.0f, 1.0f, 0.0f, 0.0f),
            new Vector4(0.0f, 0.0f, 1.0f, 0.0f),
            new Vector4(0.0f, 0.0f, 0.0f, 1.0f));
    }

    [ComVisible(true)]
    [Guid("CF2F8DC0-5F4F-4621-9247-D34CA265D89F")]
    public struct Color
    {
        public Color(float r, float g, float b, float a)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        [XmlAttribute]
        public float r, g, b, a;
    }
}
