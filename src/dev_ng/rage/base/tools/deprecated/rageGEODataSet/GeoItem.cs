using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.InteropServices;

namespace rageGEODataSet
{
    public class GeoItem
    {
        public GeoItem()
        {
        }
        [XmlIgnore]
        public bool InheritViewExpanded = true;
    }
}
