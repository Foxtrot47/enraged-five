using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Runtime.InteropServices;

namespace rageGEODataSet
{
    [ComVisible(false)] 
    public class GeoPath
    {
        public enum ElementNodeType
        {
            Node,
            Array,
            Attribute
        };

        internal class GeoPathElement
        {
            public GeoPathElement()
            {
                m_NodeType = ElementNodeType.Node;
                m_ArrIndex = -1;
                m_Name = String.Empty;
            }
        
            public string Name
            {
                get { return m_Name; }
                set { m_Name = value.ToLower(); }
            }

            public string AttributeName
            {
                get { return m_AttributeName; }
                set { m_AttributeName = value; }
            }

            public Int32 ArrayIndex
            {
                get { return m_ArrIndex; }
                set { m_ArrIndex = value; }
            }

            public ElementNodeType NodeType
            {
                get { return m_NodeType; }
                set { m_NodeType = value; }
            }

            private string              m_Name;
            private string              m_AttributeName;
            private Int32               m_ArrIndex;
            private ElementNodeType     m_NodeType;
        }

        public GeoPath()
        {
            m_PathElements = new List<GeoPathElement>();
        }

        public bool Initialize(string gP)
        {
            m_PathString = gP;

            string[] elements = gP.Split(new char[] { '/' });

            if (elements.Length == 0)
                return false;
            m_RootName = elements[0];

            for (Int32 elmIdx = 1; elmIdx < elements.Length; elmIdx++)
            {
                string elm = elements[elmIdx];
          
                if (elm.IndexOf('[') != -1)
                {
                    //This is an array element
                    GeoPathElement pathElm = new GeoPathElement();
                    pathElm.NodeType = ElementNodeType.Array;

                    string[] splitName = elm.Split(new char[] { '[', ']' });
                    pathElm.Name = splitName[0];
                    pathElm.ArrayIndex = Int32.Parse(splitName[1]);

                    m_PathElements.Add(pathElm);
                }
                else
                {
                    if (elm.Contains("@"))
                    {
                        //This is an attribute element
                        GeoPathElement pathElm = new GeoPathElement();
                        pathElm.NodeType = ElementNodeType.Attribute;

                        string[] splitName = elm.Split(new char[] { '@' });
                        pathElm.Name = splitName[0];
                        pathElm.AttributeName = splitName[1];
                        m_PathElements.Add(pathElm);
                    }
                    else
                    {
                        //This is a node element
                        GeoPathElement pathElm = new GeoPathElement();
                        pathElm.NodeType = ElementNodeType.Node;
                        pathElm.Name = elm;
                        m_PathElements.Add(pathElm);
                    }
                }
            }
          
            return true;
        }

        public string RootName
        {
            get { return m_RootName; }
        }

        public string PathString
        {
            get { return m_PathString; }
        }

        public Int32 Count
        {
            get { return m_PathElements.Count; }
        }

        public String GetElementName(Int32 idx)
        {
            return m_PathElements[idx].Name;
        }

        public bool IsArrayElement(Int32 idx)
        {
            return (m_PathElements[idx].NodeType == ElementNodeType.Array);
        }

        public Int32 GetElementArrayIndex(Int32 idx)
        {
            return (m_PathElements[idx].ArrayIndex);
        }

        public bool IsAttributeElement(Int32 idx)
        {
            return (m_PathElements[idx].NodeType == ElementNodeType.Attribute);
        }

        public String GetElementAttributeName(Int32 idx)
        {
            return (m_PathElements[idx].AttributeName);
        }

        private string          m_PathString;
        private string          m_RootName;
        List<GeoPathElement>    m_PathElements;
    }
}
