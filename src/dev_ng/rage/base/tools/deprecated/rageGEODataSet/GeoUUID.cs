using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace rageGEODataSet
{
    public class GeoUUID
    {
        [DllImport("rpcrt4.dll", SetLastError = true)]
        static extern int UuidCreateSequential(out Guid guid);

        static public string CreateUUID()
        {
           const int RPC_S_OK = 0;
           Guid g;
           int hr = UuidCreateSequential(out g);
           if (hr != RPC_S_OK)
           {
             throw new ApplicationException("UuidCreateSequential failed: " + hr);
           }
           return g.ToString();
        }
    };
}
