using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;

namespace rageGEODataSet
{
    [ComVisible(true)]
    [Guid("90F79D92-22AA-4887-B0F2-DD7FCBFC8C7D")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGeoDataSetHelper
    {
        String GetGeoDataSetAssemblyVersion();

        IGeoTypeMemberBase[] GetGeoTypeMembersOfType(IGeoDataSet dataSet, string typeName, string memberInterfaceTypeStr, bool bInstanceOnly);
        
        String[] GetContainerNamesFromInstanceUUID(IGeoDataSet dataSet, String instanceUUID);
        String[] GetContainerUUIDsFromInstanceUUID(IGeoDataSet dataSet, String instanceUUID);

        bool CanContainerHoldGeoInstance(IGeoDataSet dataSet, String containerParentUUID, String containerUUID, String itemUUID);
        bool MoveGeoInstance(IGeoDataSet dataSet, String srcItemUUID, String dstContainerParentUUID, String dstContainerUUID);

        void BuildDataSetFileListsFromFiles(String templateDir, String typeDir, 
                                            [In]String[] inputFiles, 
                                            [In,Out] ref String[] templatePaths,
                                            [In,Out] ref String[] typePaths,
                                            [In,Out] ref String[] instancePaths);

		String[] GetUIDsOfContainedInstancesFromParentInstanceUID(IGeoDataSet dataSet, String dstContainerParentUUID, String dstContainerUUID);
    }

    [ComVisible(true)]
    [Guid("9BF067F8-BD2D-403d-80AE-8CDD33C01F34")]
    [ProgId("rageGEODataSet.GeoDataSetHelper")]
    public class GeoDataSetHelper : IGeoDataSetHelper
    {
        public GeoDataSetHelper()
        {
        }

        #region IGeoDataSetHelper Interface

        public String GetGeoDataSetAssemblyVersion()
        {
            Version vs = Assembly.GetExecutingAssembly().GetName().Version;
            return String.Format("{0}.{1}", vs.Major, vs.Minor);
        }

        public IGeoTypeMemberBase[] GetGeoTypeMembersOfType(IGeoDataSet dataSet, string typeName, string memberInterfaceTypeStr, bool bInstanceOnly)
        {
            List<IGeoTypeMemberBase> alMembers = new List<IGeoTypeMemberBase>();

            IGeoType type = dataSet.GetTopLevelType(typeName);
            if (type == null)
                return null;

            while (type != null)
            {
                int mC = type.GetMemberCount();
                for (int i = 0; i < mC; i++)
                {
                    IGeoTypeMemberBase member = type.GetMemberByIndex(i);
                    Type tp = member.GetType();
                    if (tp.GetInterface(memberInterfaceTypeStr) != null)
                    {
                        if (!bInstanceOnly)
                            alMembers.Add(member);
                        else
                        {
                            if (member.TemplateMemberBase.Instantiation == InstantiationType.Instance)
                                alMembers.Add(member);
                        }
                    }
                }
                type = type.BaseType;
            }

            IGeoTypeMemberBase[] arrMembers = alMembers.ToArray();

            return arrMembers;
        }

        public String[] GetContainerNamesFromInstanceUUID(IGeoDataSet dataSet, String instanceUUID)
        {
            List<String> containerNameList = new List<String>();

            IGeoType gType = dataSet.GetInstanceByUUID(instanceUUID);
            if (gType == null)
                return null;

            int mC = gType.GetMemberCount();
            for (int i = 0; i < mC; i++)
            {
                IGeoTypeMemberBase member = gType.GetMemberByIndex(i);
                if (member.TemplateMemberBase.Instantiation != InstantiationType.Instance)
                    continue;

                Type tp = member.GetType();
                if ((tp.GetInterface("IGeoTypeMemberArray") != null) ||
                     (tp.GetInterface("IGeoTypeMemberGeo") != null))
                {
                    containerNameList.Add(member.Name);
                }
            }

            String[] containerNameArr = containerNameList.ToArray();

            return containerNameArr;
        }

        public String[] GetContainerUUIDsFromInstanceUUID(IGeoDataSet dataSet, String instanceUUID)
        {
            List<String> containerUUIDList = new List<String>();

            IGeoType gType = dataSet.GetInstanceByUUID(instanceUUID);
            if (gType == null)
                return null;

            int mC = gType.GetMemberCount();
            for (int i = 0; i < mC; i++)
            {
                IGeoTypeMemberBase member = gType.GetMemberByIndex(i);
                if (member.TemplateMemberBase.Instantiation != InstantiationType.Instance)
                    continue;

                if (member.UUID != String.Empty)
                    containerUUIDList.Add(member.UUID);
            }

            String[] containerUUIDArr = containerUUIDList.ToArray();

            return containerUUIDArr;
        }

        public bool CanContainerHoldGeoInstance(IGeoDataSet dataSet, String srcItemUUID,
                                                String dstContainerParentUUID, String dstContainerUUID)
        {
            Debug.Assert(dstContainerParentUUID != String.Empty, "CanContainerHoldGeoInstance, dstContainerParentUUID is empty string");
            Debug.Assert(dstContainerUUID != String.Empty, "CanContainerHoldGeoInstance, dstContainerUUID is empty string");

            //First find the item...
            IGeoType srcItemGeo = dataSet.GetInstanceByUUID(srcItemUUID);
            if (srcItemGeo == null)
                return false;

            IGeoType dstContainerParentGeo = dataSet.GetInstanceByUUID(dstContainerParentUUID);
            if (dstContainerParentGeo == null)
                return false;

            IGeoTypeMemberBase dstContainerMemberBase = dstContainerParentGeo.GetMemberByUUID(dstContainerUUID);
            if (dstContainerMemberBase == null)
                return false;

            return CanContainerHoldGeoInstance(dataSet, srcItemGeo, dstContainerParentGeo, dstContainerMemberBase);
        }

        public bool MoveGeoInstance(IGeoDataSet dataSet, String srcItemUUID,
                                    String dstContainerParentUUID, String dstContainerUUID)
        {
            //First find the item...
            IGeoType srcItemGeo = dataSet.GetInstanceByUUID(srcItemUUID);
            String srcItemGeoTemplateName = srcItemGeo.Template.Name;

            //If the destination is a container get the parent and container references
            IGeoType dstContainerParentGeo = null;
            IGeoTypeMemberBase dstContainerMemberBase = null;
            Boolean bDstIsInContainer = false;
            if ((dstContainerParentUUID != String.Empty) &&
                (dstContainerUUID != String.Empty))
            {
                dstContainerParentGeo = dataSet.GetInstanceByUUID(dstContainerParentUUID);
                if (dstContainerParentGeo == null)
                    return false;

                dstContainerMemberBase = dstContainerParentGeo.GetMemberByUUID(dstContainerUUID);
                if (dstContainerMemberBase == null)
                    return false;

                //Ensure the container can hold the GEO
                Debug.Assert(CanContainerHoldGeoInstance(dataSet, srcItemGeo, dstContainerParentGeo, dstContainerMemberBase));

                bDstIsInContainer = true;
            }

            if (srcItemGeo.ContainingMember != null)
            {
                //The source item is contained in a geo (which may or may not be in an array), so remove the source type from it.
                IGeoTypeMemberGeo srcItemContaingMemberGeo = (IGeoTypeMemberGeo)srcItemGeo.ContainingMember;
                srcItemContaingMemberGeo.GeoTypeThatIContain = null;

                //IGeoTypeMemberBase outerContainerBase = ((IGeoTypeMemberBase)srcItemGeo).ContainingTypeMember;
                IGeoTypeMemberBase outerContainerBase = (IGeoTypeMemberBase)(((GeoType)srcItemGeo).m_ContainingMember.ContainingTypeMember);
                if ((outerContainerBase != null) &&
                    (outerContainerBase.GetType().GetInterface("IGeoTypeMemberArray") != null))
                {
                    IGeoTypeMemberArray outerContainerArr = (IGeoTypeMemberArray)outerContainerBase;
                    IGeoTypeMemberGeo remGeoTypeMember = (IGeoTypeMemberGeo)outerContainerArr.RemoveElementByUUID(srcItemContaingMemberGeo.UUID);
                    Debug.Assert(remGeoTypeMember == srcItemContaingMemberGeo);
                }
            }
            else
            {
                //The source GEO Type is currently held in the top level type list.  In this case we need to copy the type
                //then mark the top level type as deleted

                GeoType clonedType = new GeoType();
                ((GeoType)clonedType).DeepCopyFrom((GeoType)srcItemGeo);

                srcItemGeo.Deleted = true;
                srcItemGeo.Dirty = true;

                srcItemGeo = clonedType;
            }

            if (bDstIsInContainer)
            {
                //Add the item to the destination container
                if (dstContainerMemberBase.GetType().GetInterface("IGeoTypeMemberArray") != null)
                {
                    IGeoTypeMemberArray dstContainerMemberArr = (IGeoTypeMemberArray)dstContainerMemberBase;

                    //Create the new GEO Type member to hold the instance
                    GeoTypeMemberGeo newGeoElement = new GeoTypeMemberGeo();
                    newGeoElement.UUID = GeoUUID.CreateUUID();
                    newGeoElement.m_TemplateMemberBase = (GeoTemplateMemberBase)dstContainerMemberArr.TemplateMember.ChildTemplate;
                    newGeoElement.GeoTypeThatIContain = (GeoType)srcItemGeo;
                    newGeoElement.GeoTypeThatIContain.ContainingMember = newGeoElement;
                    newGeoElement.ContainingTypeMember = (IGeoTypeMemberBase)dstContainerMemberArr;

                    //Add the new GEO Type member to the array
                    dstContainerMemberArr.AddElement(newGeoElement);

                    //Mark the root type (which holds the container) as dirty
                    dstContainerParentGeo.Dirty = true;
                }
                else if (dstContainerMemberBase.GetType().GetInterface("IGeoTypeMemberGeo") != null)
                {
                    IGeoTypeMemberGeo dstContainerMemberGeo = (IGeoTypeMemberGeo)dstContainerMemberBase;
                    srcItemGeo.ContainingMember = dstContainerMemberGeo;
                    dstContainerMemberGeo.GeoTypeThatIContain = srcItemGeo;
                }
                else
                    Debug.Assert(false, "Destination container for move is not an array or GEO");
            }
            else
            {
                //The destination isn't a contianer so move the GEO into the top level GEOs
                srcItemGeo.ContainingMember = null;
                dataSet.AddGeoType(srcItemGeo);
            }

            return true;
        }

		public String[] GetUIDsOfContainedInstancesFromParentInstanceUID(IGeoDataSet dataSet, String dstContainerParentUUID, String dstContainerUUID)
		{
			List<String> containedInstancesUIDList = new List<String>();

			// Get the parent instance
			IGeoType dstContainerParentGeo = dataSet.GetInstanceByUUID(dstContainerParentUUID);
			if (dstContainerParentGeo == null)
				return containedInstancesUIDList.ToArray();

			// Get the container in the instance
			IGeoTypeMemberBase dstContainerMemberBase = dstContainerParentGeo.GetMemberByUUID(dstContainerUUID);
			if (dstContainerMemberBase == null)
				return containedInstancesUIDList.ToArray();

			// Is it an array member, if it isn't than I can't have children
			if (dstContainerMemberBase.GetType().GetInterface("IGeoTypeMemberArray") != null)
			{
				// Go through my contained stuff
				IGeoTypeMemberArray dstContainerMemberArr = (IGeoTypeMemberArray)dstContainerMemberBase;
				for(int i=0; i<dstContainerMemberArr.GetElementCount(); i++)
				{
					IGeoTypeMemberBase dstContainedElement = dstContainerMemberArr.GetElement(i);

					if(dstContainedElement.GetType().GetInterface("IGeoTypeMemberGeo") != null)
					{
						// It is a Geo Instance
						IGeoTypeMemberGeo dstContainedGeoInstance = (IGeoTypeMemberGeo)dstContainedElement;

						// Add the UID
						containedInstancesUIDList.Add(dstContainedGeoInstance.GeoTypeThatIContain.UUID);
					}
				}
			}

			return containedInstancesUIDList.ToArray();
		}
		#endregion

        public void BuildDataSetFileListsFromFiles(String templateDir, String typeDir,
                                                   [In]String[] inputFiles,
                                                   [In, Out] ref String[] templatePaths,
                                                   [In, Out] ref String[] typePaths,
                                                   [In, Out] ref String[] instancePaths)
        {
            if (!templateDir.EndsWith("\\") && (!templateDir.EndsWith("/")))
                templateDir += "/";

            if (!typeDir.EndsWith("\\") && (!typeDir.EndsWith("/")))
                typeDir += "/";

            List<String> templateList = new List<String>();
            List<String> typeList = new List<String>();
            List<String> instanceList = new List<String>();

            Queue<String> filesToSearch = new Queue<String>();
            List<String> filesSearched = new List<String>();

            //Add all of the incoming files to the files to search queue
            foreach (String file in inputFiles)
            {
                FileInfo fi = new FileInfo(file);
                if (!fi.Exists)
                    continue;

                //For instances add the full path, since they may not all be under a GEOInstances folder so
                //it would be impossible to rebuild their path later. For templates and types, just add the
                //name since it will reduce the length of the strings to compare when determining if we have
                //already added the template when we are scanning their contents below.
                if (String.Compare(fi.Extension, ".geo", true) == 0)
                    instanceList.Add(file);
                else if (String.Compare(fi.Extension, ".geotemplate", true) == 0)
                {
                    templateList.Add(fi.Name.Substring(0, fi.Name.LastIndexOf('.')));
                }
                else if (String.Compare(fi.Extension, ".geotype", true) == 0)
                    typeList.Add(fi.Name.Substring(0, fi.Name.LastIndexOf('.')));

                if (!filesToSearch.Contains(file))
                    filesToSearch.Enqueue(file);
            }

            Boolean bIsTemplate = false;
            char[] delims = {'/'};

            while (filesToSearch.Count != 0)
            {
                //Grab the next file to search
                String file = filesToSearch.Dequeue();
                if (filesSearched.Contains(file))
                    continue;

                //Determine if the file we are searching is a template file or a type file (instances are type files)
                FileInfo fi = new FileInfo(file);
                if (!fi.Exists)
                    continue;

                if (String.Compare(fi.Extension, ".geotemplate", true) == 0)
                    bIsTemplate = true;

                //Load the file as an XmlDocument
                XmlDocument xd = new XmlDocument();

                try { xd.Load(file); }
                catch (XmlException) { continue; }

                //Look for all the "template" attributes, since they specify a template we may need to load.
                XmlNodeList templateAttrList = xd.DocumentElement.SelectNodes("//@template");
                foreach (XmlNode templateNode in templateAttrList)
                {
                    String[] templateParts = templateNode.Value.Split(delims);
                    Debug.Assert(templateParts.Length != 0);
                    String baseTemplateName = templateParts[0];

                    if (!templateList.Contains(baseTemplateName))
                        templateList.Add(baseTemplateName);

                    String baseTemplatePath = String.Format("{0}{1}.geotemplate", templateDir, baseTemplateName);
                    if (!filesToSearch.Contains(baseTemplatePath))
                        filesToSearch.Enqueue(baseTemplatePath);
                }

                //Look for all the nodes that might be pointing to another type or a template
                XmlNodeList searchAttrList = xd.DocumentElement.SelectNodes("//@base");
                foreach (XmlNode searchNode in searchAttrList)
                {
                    //Special case.  This needs to be fixed, but the "Node" GEO member uses the "type" attribute
                    //to reference something other than another GEO type, so skip over it
                    
                    //if (String.Compare(searchNode.Name, "Node", true)==0)
                    //    continue;

                    if (bIsTemplate)
                    {
                        //The current file is a template file, in which case the base attribute will be referring to a geo template file
                        String baseTemplateName = searchNode.Value;
                        if (!templateList.Contains(baseTemplateName))
                            templateList.Add(baseTemplateName);

                        String baseTemplatePath = String.Format("{0}{1}.geotemplate", templateDir, baseTemplateName);
                        if (!filesToSearch.Contains(baseTemplatePath))
                            filesToSearch.Enqueue(baseTemplatePath);
                    }
                    else
                    {
                        //The current file is a type or instance file, in which case the base attribute will be referring to a geo type file
                        String baseTypeName = searchNode.Value;
                        if (baseTypeName.Length != 0)
                        {
                            if (!typeList.Contains(baseTypeName))
                                typeList.Add(baseTypeName);

                            String baseTypePath = String.Format("{0}{1}.geotype", typeDir, baseTypeName);
                            if (!filesToSearch.Contains(baseTypePath))
                                filesToSearch.Enqueue(baseTypePath);
                        }
                    }  
                }

                filesSearched.Add(file);
            }

            templatePaths = new String[templateList.Count];
            for (int i = 0; i < templateList.Count; i++)
                templatePaths[i] = String.Format("{0}{1}.geotemplate", templateDir, templateList[i]);

            typePaths = new String[typeList.Count];
            for (int i = 0; i < typeList.Count; i++)
                typePaths[i] = String.Format("{0}{1}.geotype", typeDir, typeList[i]);

            instancePaths = instanceList.ToArray(); 
        }

        public void BuildDataSetFileListsFromFilesOld(String templateDir, String typeDir, 
                                                    [In]String[] inputFiles, 
                                                    [In,Out] ref String[] templatePaths,
                                                    [In,Out] ref String[] typePaths,
                                                    [In,Out] ref String[] instancePaths)
        {
            //This is a pretty slow, brute force method of uncovering all the templates and types that need to be loaded,
            //but it should work for the time being....
            if (!templateDir.EndsWith("\\") && (!templateDir.EndsWith("/")))
                templateDir += "/";

            if (!typeDir.EndsWith("\\") && (!typeDir.EndsWith("/")))
                typeDir += "/";

            List<String> templateList = new List<String>();
            List<String> typeList = new List<String>();
            List<String> instanceList = new List<String>();

            Queue<String> filesToSearch = new Queue<String>();
            List<String> filesSearched = new List<String>();

            //Add all of the incoming files to the files to search queue
            foreach (String file in inputFiles)
            {
                FileInfo fi = new FileInfo(file);
                if (!fi.Exists)
                    continue;

                //For instances add the full path, since they may not all be under a GEOInstances folder so
                //it would be impossible to rebuild their path later. For templates and types, just add the
                //name since it will reduce the length of the strings to compare when determining if we have
                //already added the template when we are scanning their contents below.
                if (String.Compare(fi.Extension, ".geo", true) == 0)
                    instanceList.Add(file);
                else if (String.Compare(fi.Extension, ".geotemplate", true) == 0)
                {
                    templateList.Add(fi.Name.Substring(0, fi.Name.LastIndexOf('.')));
                }
                else if (String.Compare(fi.Extension, ".geotype", true) == 0)
                    typeList.Add(fi.Name.Substring(0, fi.Name.LastIndexOf('.')));

                if(!filesToSearch.Contains(file))
                    filesToSearch.Enqueue(file);
            }

            //Go through each of the files in the queue looking for template and type references.
            while (filesToSearch.Count != 0)
            {
                String file = filesToSearch.Dequeue();
                if (filesSearched.Contains(file))
                    continue;

                FileInfo fi = new FileInfo(file);

                XmlDocument xd = new XmlDocument();

                try { xd.Load(file); }
                catch (XmlException ) { continue; }

                IEnumerator nodeEnum = xd.GetEnumerator();
                
                XmlNode curNode;
                XmlAttribute itemAttr;
                char[] delims = {'/'};
                while (nodeEnum.MoveNext())
                {
                    curNode = (XmlNode)nodeEnum.Current;
                    if (curNode.Attributes != null && curNode.Attributes.Count != 0)
                    {
                        itemAttr = (XmlAttribute)curNode.Attributes.GetNamedItem("template");
                        if (itemAttr != null)
                        {
                            String[] templateParts = itemAttr.Value.Split(delims);
                            Debug.Assert(templateParts.Length != 0);
                            String baseTemplateName = templateParts[0];

                            if (!templateList.Contains(baseTemplateName))
                                templateList.Add(baseTemplateName);

                            String baseTemplatePath = String.Format("{0}{1}.geotemplate", templateDir, baseTemplateName);
                            if (!filesToSearch.Contains(baseTemplatePath))
                                filesToSearch.Enqueue(baseTemplatePath);
                        }

                        itemAttr = (XmlAttribute)curNode.Attributes.GetNamedItem("base");
                        if (itemAttr != null)
                        {
                            if (String.Compare(fi.Extension, ".geotemplate")==0)
                            {
                                //The current file is a template file, in which case the base attribute will be referring to a geo template file
                                String baseTemplateName = itemAttr.Value;
                                if (!templateList.Contains(baseTemplateName))
                                    templateList.Add(baseTemplateName);

                                String baseTemplatePath = String.Format("{0}{1}.geotemplate", templateDir, baseTemplateName);
                                if (!filesToSearch.Contains(baseTemplatePath))
                                    filesToSearch.Enqueue(baseTemplatePath);
                            }
                            else
                            {
                                //The current file is a type or instance file, in which case the base attribute will be referring to a geo type file
                                String baseTypeName = itemAttr.Value;
                                if (!typeList.Contains(baseTypeName))
                                    typeList.Add(baseTypeName);

                                String baseTypePath = String.Format("{0}{1}.geotype", typeDir, baseTypeName);
                                if (!filesToSearch.Contains(baseTypePath))
                                    filesToSearch.Enqueue(baseTypePath);
                            }
                        }
                    }
                }

                filesSearched.Add(file);
            }

            templatePaths = new String[templateList.Count];
            for (int i = 0; i < templateList.Count; i++)
                templatePaths[i] = String.Format("{0}{1}.geotemplate", templateDir, templateList[i]);

            typePaths = new String[typeList.Count];
            for (int i = 0; i < typeList.Count; i++)
                typePaths[i] = String.Format("{0}{1}.geotype", typeDir, typeList[i]);

            instancePaths = instanceList.ToArray(); 
        }

        private bool CanContainerHoldGeoInstance(IGeoDataSet dataSet, IGeoType srcItemGeo,
                                                IGeoType dstContainerParentGeo, IGeoTypeMemberBase dstContainerMemberBase)
        {
            String srcItemGeoTemplateName = srcItemGeo.Template.Name;

            //Ensure that the destination container can hold the item being moved into it
            if (dstContainerMemberBase.GetType().GetInterface("IGeoTypeMemberArray") != null)
            {
                IGeoTypeMemberArray dstContainerMemberArr = (IGeoTypeMemberArray)dstContainerMemberBase;
                IGeoTemplateMemberBase dstContainerChildTemplateBase = dstContainerMemberArr.TemplateMember.ChildTemplate;
                if (dstContainerChildTemplateBase.GetType().GetInterface("IGeoTemplateMemberGeo") != null)
                {
                    IGeoTemplateMemberGeo dstContainerChildTemplateGeo = (IGeoTemplateMemberGeo)dstContainerChildTemplateBase;
                    if (srcItemGeoTemplateName == dstContainerChildTemplateGeo.TemplateName)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            else if (dstContainerMemberBase.GetType().GetInterface("IGeoTypeMemberGeo") != null)
            {
                IGeoTypeMemberGeo dstContainerMemberGeo = (IGeoTypeMemberGeo)dstContainerMemberBase;
                if (srcItemGeoTemplateName == dstContainerMemberGeo.TemplateMember.TemplateName)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        private PropertyInfo FindTypePropertyCI(Object obj, String propertyName)
        {
            PropertyInfo[] piArr = obj.GetType().GetProperties();
            foreach (PropertyInfo pi in piArr)
            {
                if (String.Compare(pi.Name, propertyName, true) == 0)
                    return pi;
            }
            return null;
        }

        private Object GetPropertyValueCI(Object obj, String propertyName)
        {
            PropertyInfo pi = FindTypePropertyCI(obj, propertyName);
            if (pi != null)
            {
                return pi.GetValue(obj, null);
            }
            return null;
        }

        public String GetGeoValueFromPath(IGeoDataSet dataSet, string geoPath)
        {
            GeoPath gP = new GeoPath();
            if (!gP.Initialize(geoPath))
                return String.Empty;

            IGeoType rootType = dataSet.GetTopLevelType(gP.RootName);
            if (rootType == null)
                return String.Empty;

            Object pathObj = rootType;
            Int32 pathDepth = 0;
            
            while(pathDepth <= gP.Count)
            {
                Type pathObjType = pathObj.GetType();
                if (pathObjType.GetInterface("IGeoType") != null)
                {
                    //We are in a type, so this level of the path should be specifying a member of the type
                    IGeoType gType = (IGeoType)pathObj;
                    IGeoTypeMemberBase gTypeMemberBase = gType.GetMemberByName(gP.GetElementName(pathDepth));
                    if (gTypeMemberBase == null)
                    {
                        //The element doesn't exist in the type so move to the template
                        IGeoTemplate gTemplate = gType.Template;
                        pathObj = gTemplate;
                        continue;
                    }
                    else
                    {
                        pathObj = gTypeMemberBase;
                        if (!gP.IsArrayElement(pathDepth) && !gP.IsAttributeElement(pathDepth))
                            pathDepth++;
                    }
                }
                else if (pathObjType.GetInterface("IGeoTemplate") != null)
                {
                    IGeoTemplate pathObjTemplate = (IGeoTemplate)pathObj;
                    
                    IGeoTemplateMemberBase pathObjTmplMbrBase = pathObjTemplate.GetMemberByName(gP.GetElementName(pathDepth));
                    if (pathObjTmplMbrBase != null)
                    {
                        pathObj = pathObjTmplMbrBase;
                    }
                    else
                        return String.Empty;
                }
                else if (pathObjType.GetInterface("IGeoTemplateMemberBase") != null)
                {
                    if (gP.IsAttributeElement(pathDepth))
                    {
                        Object attrVal = GetPropertyValueCI(pathObj, gP.GetElementAttributeName(pathDepth));
                        if (attrVal != null)
                            return attrVal.ToString();
                        else
                            return String.Empty;
                    }
                    else
                    {
                        if (pathObjType.GetInterface("IGeoTemplateMemberArray") != null)
                        {
                            throw new ApplicationException("Not Implemented.");
                        }
                        else if (pathObjType.GetInterface("IGeoTemplateMemberString") != null)
                        {
                            throw new ApplicationException("Not Implemented.");
                        }
                        else
                        {
                            throw new ApplicationException("Not Implemented.");
                        }
                    }
                }
                else if (pathObjType.GetInterface("IGeoTypeMemberBase") != null)
                {
                    if (pathObjType.GetInterface("IGeoTypeMemberArray") != null)
                    {
                        IGeoTypeMemberArray pathObjArr = (IGeoTypeMemberArray)pathObj;
                        if (gP.IsArrayElement(pathDepth))
                        {
                            int idx = gP.GetElementArrayIndex(pathDepth);
                            if (idx < pathObjArr.GetElementCount())
                            {
                                pathObj = pathObjArr.GetElement(idx);
                                pathDepth++;
                            }
                            else
                            {
                                IGeoTemplateMemberArray tmplMemberArr = pathObjArr.TemplateMember;
                                pathObj = tmplMemberArr.ChildTemplate;
                            }
                        }
                        else if (gP.IsAttributeElement(pathDepth))
                        {
                            //Try to get the attribute value from the type 
                            Object attrVal = GetPropertyValueCI(pathObj, gP.GetElementAttributeName(pathDepth));
                            if (attrVal != null)
                            {
                                return attrVal.ToString();
                            }
                            else
                            {
                                IGeoTemplateMemberBase tmplMbrBase = (IGeoTemplateMemberBase)pathObjArr.TemplateMember;
                                pathObj = tmplMbrBase;
                            }
                        }
                        else
                        {
                            throw new ApplicationException("Not Implemented.");
                        }
                    }
                    else if (pathObjType.GetInterface("IGeoTypeMemberBool") != null)
                    {
                        IGeoTypeMemberBool pathObjBool = (IGeoTypeMemberBool)pathObj;
                        if (pathDepth == gP.Count)
                        {
                            return pathObjBool.Value.ToString();
                        }
                        else if (gP.IsAttributeElement(pathDepth))
                        {
                            Object attrVal = GetPropertyValueCI(pathObj, gP.GetElementAttributeName(pathDepth));
                            if (attrVal != null)
                                return attrVal.ToString();
                            else
                            {
                                IGeoTemplateMemberBase tmplMbrBase = (IGeoTemplateMemberBase)pathObjBool.TemplateMember;
                                pathObj = tmplMbrBase;
                            }
                        }
                        else
                            throw new ApplicationException(String.Format("GEOPath Error"));

                    }
                    else if (pathObjType.GetInterface("IGeoTypeMemberFloat") != null)
                    {
                        IGeoTypeMemberFloat pathObjFloat = (IGeoTypeMemberFloat)pathObj;
                        if (pathDepth == gP.Count)
                        {
                            return pathObjFloat.Value.ToString();
                        }
                        else if (gP.IsAttributeElement(pathDepth))
                        {
                            Object attrVal = GetPropertyValueCI(pathObj, gP.GetElementAttributeName(pathDepth));
                            if (attrVal != null)
                                return attrVal.ToString();
                            else
                            {
                                IGeoTemplateMemberBase tmplMbrBase = (IGeoTemplateMemberBase)pathObjFloat.TemplateMember;
                                pathObj = tmplMbrBase;
                            }
                        }
                        else
                            throw new ApplicationException(String.Format("GEOPath Error"));
                    }
                    else if (pathObjType.GetInterface("IGeoTypeMemberString") != null)
                    {
                        IGeoTypeMemberString pathObjString = (IGeoTypeMemberString)pathObj;
                        if (pathDepth == gP.Count)
                        {
                            return pathObjString.Value;
                        }
                        else if (gP.IsAttributeElement(pathDepth))
                        {
                            Object attrVal = GetPropertyValueCI(pathObj, gP.GetElementAttributeName(pathDepth));
                            if (attrVal != null)
                                return attrVal.ToString();
                            else
                            {
                                IGeoTemplateMemberBase tmplMbrBase = (IGeoTemplateMemberBase)pathObjString.TemplateMember;
                                pathObj = tmplMbrBase;
                            }
                        }
                        else
                            throw new ApplicationException(String.Format("GEOPath Error"));
                    }
                    else if (pathObjType.GetInterface("IGeoTypeMemberGeo") != null)
                    {
                        IGeoTypeMemberGeo pathObjGeo = (IGeoTypeMemberGeo)pathObj;

                        IGeoTypeMemberBase memberBase = pathObjGeo.GeoTypeThatIContain.GetMemberByName(gP.GetElementName(pathDepth));
                        if (memberBase != null)
                        {
                            pathObj = memberBase;
                            if (!gP.IsAttributeElement(pathDepth))
                                pathDepth++;
                        }
                        else
                        {
                            pathObj = pathObjGeo.GeoTypeThatIContain.Template;
                        }
                    }
                    else if (pathObjType.GetInterface("IGeoTypeMemberNode") != null)
                    {
                        IGeoTypeMemberNode pathObjNode = (IGeoTypeMemberNode)pathObj;
                        if (gP.IsAttributeElement(pathDepth))
                        {
                            Object propVal = GetPropertyValueCI(pathObj, gP.GetElementAttributeName(pathDepth));
                            return propVal.ToString();
                        }
                        else if (pathDepth < gP.Count)
                        {
                            IGeoTypeMemberBase memberBase = pathObjNode.GetElementByName(gP.GetElementName(pathDepth));
                            if (memberBase != null)
                            {
                                pathObj = memberBase;
                                if (!gP.IsAttributeElement(pathDepth))
                                    pathDepth++;
                            }
                            else
                            {
                                pathObj = ((IGeoTypeMemberBase)pathObj).TemplateMemberBase;
                            }
                        }
                    }
                    else
                    {
                        throw new ApplicationException("Not Implemented.");
                    }
                }
                else
                {
                    throw new ApplicationException("Not Implemented.");
                }
            }
            return String.Empty;
        }
	}


    public class GeoDataSetXPathNavigator : XPathNavigator
    {
        public override XPathNodeType NodeType
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string LocalName
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string Name
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string Prefix
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string Value
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string BaseURI
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override string NamespaceURI
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override bool IsEmptyElement
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override XmlNameTable NameTable
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public override XPathNavigator Clone()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool MoveToFirstAttribute()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool MoveToNextAttribute()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool MoveToFirstNamespace(XPathNamespaceScope namespaceScope)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool MoveToNextNamespace(XPathNamespaceScope namespaceScope)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool MoveTo(XPathNavigator other)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool MoveToNext()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool MoveToPrevious()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool MoveToFirstChild()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool MoveToParent()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool MoveToId(string id)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool IsSamePosition(XPathNavigator other)
        {
            throw new Exception("The method or operation is not implemented.");
        }

    };

}

