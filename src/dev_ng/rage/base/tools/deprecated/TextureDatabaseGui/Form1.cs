using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

using DotNet.KDNGrid;

namespace TextureDatabaseGui
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private ArrayList Columns = new ArrayList();
		//private DataSet MyDataSet;
		private DataTable m_DataTable;
		private Int32 MyLastRowNumber = -1;
		private Bitmap MyImage = null;

		private System.Windows.Forms.DataGrid dataGrid1;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.PictureBox pictureBox1;

		private MySqlConnection		m_Connection;
		private MySqlDataAdapter	m_DataAdapter;
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			LoadTable();
	

		}

		private static string SERVER		="roogle";
		private static string USER_ID		="root";
		private static string DATABASE		="texdb";
		private static string TABLE			="texdb";
		//private static string DATASET_NAME	="Textures";

		//private void Form1_Load(object sender, System.EventArgs e)
		private void LoadTable()
		{ 
			// connect to the database:
			string connectionString = String.Format("server={0};user id={1}; database=mysql; pooling=false",
				SERVER, USER_ID );
			try 
			{
				m_Connection = new MySqlConnection( connectionString );
				m_Connection.Open();
			}
			catch (MySqlException e) 
			{
				MessageBox.Show( "Error connecting to the server: " + e.Message );
				return;
			}

			m_Connection.ChangeDatabase(DATABASE);


			ColumnInfo[] columnInfos =
			{
				new ColumnInfo("id",				"id",				Column.Type.INT),
				new ColumnInfo("sourcefile",		"Source Path",		Column.Type.TEXT),
				new ColumnInfo("maxsize",			"Size",				Column.Type.TEXT)
			};

			String sqlColumns="";
			foreach (ColumnInfo colInfo in columnInfos)
			{
				sqlColumns += colInfo.m_SQLName;
				sqlColumns += ",";
			}
			sqlColumns=sqlColumns.Trim(',');

			m_DataAdapter = new MySqlDataAdapter( "SELECT " + sqlColumns + " FROM " + TABLE, m_Connection );
			MySqlCommandBuilder cb = new MySqlCommandBuilder(m_DataAdapter);
		
			foreach (ColumnInfo colInfo in columnInfos)
			{
				Column col=null;
				if (colInfo.m_Type==Column.Type.INT)
				{
					col=new Column(m_DataAdapter,colInfo.m_Type,colInfo.m_SQLName,colInfo.m_EditorName,50,0,1000000);
				}
				else if (colInfo.m_Type==Column.Type.TEXT)
				{
					col=new Column(m_DataAdapter,colInfo.m_Type,colInfo.m_SQLName,"id",400);
				}
				else 
					Debug.Fail("Column type '" + colInfo.m_Type.ToString() + "' not support yet.");

				Columns.Add(col);
			}

			// add columns:
			//Columns.Add(new Column(m_DataAdapter,Column.Type.INT,"SrcResY","",50,1,4096));
	

#if CRAP
			MyDataSet = new DataSet(DATASET_NAME);     
			m_DataAdapter.Fill(MyDataSet, DATASET_NAME);

			// figure out column data type:
			foreach (Column col in Columns)
				col.GetColumnType(MyDataSet);

			//CreateUpdateCommand(primaryKey,m_Connection);

			// bind the data grid to the data source:
			dataGrid1.DataSource = MyDataSet.DefaultViewManager;
			dataGrid1.SetDataBinding(MyDataSet,DATASET_NAME); 
			DataGridTableStyle myGridTableStyle = new DataGridTableStyle();
			myGridTableStyle.MappingName=DATASET_NAME;
			myGridTableStyle.AlternatingBackColor = Color.LightGray;

			foreach (Column col in Columns)
			{
				col.CreateStyle(myGridTableStyle,dataGrid1);
			}

			dataGrid1.TableStyles.Add(myGridTableStyle);
#else
			m_DataTable = new DataTable();
			m_DataAdapter.Fill(m_DataTable);
			dataGrid1.DataSource=m_DataTable;
#endif
		} 

#if CRAP
		private void CreateUpdateCommand(Column primaryKey,MySqlConnection conn)
		{
			OdbcCommand cmd;

			// Create the UpdateCommand.
			String updateCommandText="";
			int colCount=0;
			foreach (Column col in Columns)
			{
				updateCommandText += col.ActualName + " = ?";
				if (++colCount<Columns.Count)
					updateCommandText += ",";
			}

			cmd = new OdbcCommand("UPDATE texdb SET " + 
			updateCommandText + 
			" WHERE " + primaryKey.ActualName + " = ?", conn);

			foreach (Column col in Columns)
				cmd.Parameters.Add(col.ActualName, col.SQLType);
				
			// this is for the original primaryKey:
			Column colWhere=(Column)Columns[0];
			OdbcParameter param = new OdbcParameter(colWhere.ActualName,colWhere.SQLType);
			cmd.Parameters.Add(param);
			param.SourceVersion = DataRowVersion.Original;

			m_DataAdapter.UpdateCommand = cmd;
		}
#endif


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGrid1
			// 
			this.dataGrid1.DataMember = "";
			this.dataGrid1.Dock = System.Windows.Forms.DockStyle.Top;
			this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid1.Location = new System.Drawing.Point(0, 0);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.Size = new System.Drawing.Size(816, 273);
			this.dataGrid1.TabIndex = 0;
			this.dataGrid1.CurrentCellChanged += new System.EventHandler(this.dataGrid1_CurrentCellChanged);
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
			this.splitter1.Location = new System.Drawing.Point(0, 273);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(816, 3);
			this.splitter1.TabIndex = 1;
			this.splitter1.TabStop = false;
			// 
			// pictureBox1
			// 
			this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox1.Location = new System.Drawing.Point(0, 276);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(816, 185);
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(816, 461);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.dataGrid1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void LoadImage()
		{
#if CRAP
			if (MyLastRowNumber>=0)
			{
				// Sets up an image object to be displayed.
				if (MyImage != null)
				{
					MyImage.Dispose();
				}

				pictureBox1.SizeMode = PictureBoxSizeMode.Normal;
				try 
				{
					string imageSource=MyDataSet.Tables[0].Rows[MyLastRowNumber]["sourcefile"].ToString();
					if (imageSource!=null)
					{
						MyImage = new Bitmap(imageSource);
						pictureBox1.ClientSize = new Size(MyImage.Width, MyImage.Height);
					}
				}
				catch
				{
					MyImage = null;
				}
				
				pictureBox1.Image = (Image) MyImage ;
	
			}
#else
			if (MyLastRowNumber>=0)
			{
				// Sets up an image object to be displayed.
				if (MyImage != null)
				{
					MyImage.Dispose();
				}

				pictureBox1.SizeMode = PictureBoxSizeMode.Normal;
				try 
				{
					string imageSource=m_DataTable.Rows[MyLastRowNumber]["sourcefile"].ToString();
					if (imageSource!=null)
					{
						MyImage = new Bitmap(imageSource);
						pictureBox1.ClientSize = new Size(MyImage.Width, MyImage.Height);
					}
				}
				catch
				{
					MyImage = null;
				}
				
				pictureBox1.Image = (Image) MyImage ;
	
			}
#endif
		}

		private void UpdateTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
		
		}

		private void dataGrid1_CurrentCellChanged(object sender, System.EventArgs eventA)
		{
			if (dataGrid1.CurrentCell.RowNumber!=MyLastRowNumber)
			{	
				MyLastRowNumber=dataGrid1.CurrentCell.RowNumber;
				LoadImage();
			}

#if CRAP
			// Check for changes with the HasChanges method first.
			if(!MyDataSet.HasChanges(DataRowState.Modified)) return;

			// Create temporary DataSet variable.
			DataSet xDataSet;
			// GetChanges for modified rows only.
			xDataSet = MyDataSet.GetChanges(DataRowState.Modified);
			// Check the DataSet for errors.
			if(xDataSet.HasErrors)
			{
			}

			
			// After fixing errors, update the data source with the m_DataAdapter 
			// used to create the DataSet.
			try 
			{
				m_DataAdapter.Update(xDataSet,DATASET_NAME);	
			}
			catch (MySqlException e) 
			{
				MessageBox.Show( "Error updating the database: " + e.Message );
				return;
			}

			MyDataSet.AcceptChanges();
#else			// Create temporary DataSet variable.
			DataTable dataTable;
			// GetChanges for modified rows only.
			dataTable = m_DataTable.GetChanges(DataRowState.Modified);
			if (dataTable==null)
				return;

			// After fixing errors, update the data source with the m_DataAdapter 
			// used to create the DataSet.
			try 
			{
				m_DataAdapter.Update(dataTable);	
			}
			catch (MySqlException e) 
			{
				MessageBox.Show( "Error updating the database: " + e.Message );
				return;
			}

			m_DataTable.AcceptChanges();
#endif
		}
	}

	public class Column 
	{
		public enum Type {TEXT,BOOLEAN,INT,COMBO};

		public Column(MySqlDataAdapter adapter,Type type,String actual,String display,int width)
		{
			m_Type=type;
			m_ActualName=actual;
			m_DisplayName=display;
			m_ColumnWidth=width;
			adapter.TableMappings.Add(m_ActualName,m_DisplayName);
		}

		public Column(MySqlDataAdapter adapter,Type type,String actual,String display,int width,String[] comboItems) : this(adapter,type,actual,display,width)
		{
			Debug.Assert(type==Type.COMBO);
			
			m_ComboItems=comboItems;
		}

		public Column(MySqlDataAdapter adapter,Type type,String actual,String display,int width,int min,int max) : this(adapter,type,actual,display,width)
		{
			Debug.Assert(type==Type.INT);
			m_Min=min;
			m_Max=max;
		}

		public void CreateStyle(DataGridTableStyle tableStyle,DataGrid dataGrid)
		{
			DataGridColumnStyle style=null;
			switch (m_Type)
			{
				case Type.BOOLEAN:
					style = new CGridCheckBoxStyle(m_ActualName,m_ColumnWidth,HorizontalAlignment.Center,false,m_DisplayName,"","N","Y",false,"");
					break;
				case Type.INT:
					style = new CGridNumericUpDownStyle(m_ActualName,m_ColumnWidth,m_DisplayName,m_Min,m_Max,0,1,LeftRightAlignment.Left,0,"0");
					break;
				case Type.COMBO:
					style = new CGridComboBoxStyle(m_ActualName,m_ColumnWidth,HorizontalAlignment.Left,m_DisplayName,"(null)",m_ComboItems,ComboBoxStyle.DropDownList);
					break;
				case Type.TEXT:
				default:
					style = new CGridTextBoxStyle(m_ActualName,m_ColumnWidth,HorizontalAlignment.Left,false,m_DisplayName,"","");
					break;
			}
		
			if (style!=null)
				tableStyle.GridColumnStyles.Add(style);
		}

		public void GetColumnType(DataSet dataSet)
		{
			System.Type type = dataSet.Tables[0].Columns[m_ActualName].DataType;
	
			if (type==System.Type.GetType("System.String"))
			{
				m_SQLType = MySqlDbType.VarChar;
			}
			else if (type==System.Type.GetType("System.Boolean"))
			{
				m_SQLType = MySqlDbType.Byte;
			}
			else if (type==System.Type.GetType("System.Byte"))
			{
				m_SQLType = MySqlDbType.Byte;
			}
			else if (type==System.Type.GetType("System.Char"))
			{
				m_SQLType = MySqlDbType.VarChar;
			}
			else if (type==System.Type.GetType("System.DateTime"))
			{
				m_SQLType = MySqlDbType.Datetime;
			}
			else if (type==System.Type.GetType("System.Decimal"))
			{
				m_SQLType = MySqlDbType.Decimal;
			}
			else if (type==System.Type.GetType("System.Double"))
			{
				m_SQLType = MySqlDbType.Double;
			}
			else if (type==System.Type.GetType("System.Int16"))
			{
				m_SQLType = MySqlDbType.Int16;
			}
			else if (type==System.Type.GetType("System.Int32"))
			{
				m_SQLType = MySqlDbType.Int32;
			}
			else if (type==System.Type.GetType("System.Int64"))
			{
				m_SQLType = MySqlDbType.Int64;
			}
			else if (type==System.Type.GetType("System.UInt64"))
			{
				m_SQLType = MySqlDbType.Int64;
			}
			else if (type==System.Type.GetType("System.Single"))
			{
				m_SQLType = MySqlDbType.Float;
			}
			else 
			{
				string errorMessage = "Unsupported Column type " + type;
				string caption = "Unsupported Column Type!";
	
				// Displays the MessageBox.

				MessageBox.Show(errorMessage, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	
		public String ActualName { get {return m_ActualName;} }
		public MySqlDbType SQLType { get {return m_SQLType;} }

		MySqlDbType	m_SQLType;
		Type		m_Type;
		String		m_ActualName;
		String		m_DisplayName;
		int			m_ColumnWidth;
		String[]	m_ComboItems;
		int			m_Min;
		int			m_Max;
	}

	struct ColumnInfo
	{
		public ColumnInfo(string sqlName,string editorName,Column.Type type)
		{
			m_SQLName=sqlName;
			m_EditorName=editorName;
			m_Type=type;
		}

		public string m_SQLName;
		public string m_EditorName;
		public Column.Type m_Type;
	}

}
