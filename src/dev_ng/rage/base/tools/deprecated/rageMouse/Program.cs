using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using RSG.Base.Win32;

namespace rageMouse
{
	class Program
	{
		//Constants
		private const UInt32 MOUSEEVENTF_ABSOLUTE = 0x8000;
		private const UInt32 MOUSEEVENTF_MOVE = 0x0001;

		//Screen coord conversion factors.
		private const double SCREEN_X_CONV = 64.0; //65535 / 1024
		private const double SCREEN_Y_CONV = 85.3; //65535 / 768

		static public void moveMouseTo(int x, int y) 
		{
			x = (int) (x * SCREEN_X_CONV);
			y = (int) (y * SCREEN_Y_CONV);
			// Debug.WriteLine("mouse to: " + x.ToString() + "," + y.ToString());
			API.mouse_event(MOUSEEVENTF_ABSOLUTE|MOUSEEVENTF_MOVE, x, y, 0,(System.IntPtr)0);
		}

		static void Main(string[] args)
		{
			int iSecondsBetweenRepeats = 60;
			for (int i = 0; i < args.Length; i++)
			{
				if (args[i] == "-repeatEveryNSeconds")
				{
					iSecondsBetweenRepeats = Int32.Parse(args[++i]);
				}
			}

			Random obRandomNumberGenerator = new Random();
			do
			{
				moveMouseTo(obRandomNumberGenerator.Next(1024), obRandomNumberGenerator.Next(1024));
				if (iSecondsBetweenRepeats == 0)
				{
					// Bail
					break;
				}
				else
				{
					// Wait a while
					Thread.Sleep(iSecondsBetweenRepeats * 1000);
				}
			} while (true);
		}
	}
}



