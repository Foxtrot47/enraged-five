using System;
using ragCore;
using ragWidgets;


namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for PlayControl.
	/// </summary>
	public class PlayControl : ragWidgetPage.WidgetPage
	{
		public class StateData
		{
			public string m_CurrSelection;
		}

		public PlayControl()
		{
			InitializeComponent();
			//
			// TODO: Add constructor logic here
			//
		}

		private void InitializeComponent()
		{
            this.m_TimeScaler = new ragUi.ControlSlider();
            this.m_ButtonReset = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_CheckBoxStartFromCamera = new System.Windows.Forms.CheckBox();
            this.m_ButtonStartEffect = new System.Windows.Forms.Button();
            this.m_SliderStartDist = new ragUi.ControlSlider();
            this.m_SliderPosZ = new ragUi.ControlSlider();
            this.m_SliderPosY = new ragUi.ControlSlider();
            this.m_SliderPosX = new ragUi.ControlSlider();
            this.label1 = new System.Windows.Forms.Label();
            this.m_ButtonPauseEffect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_SliderTargetFPS = new ragUi.ControlSlider();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_TimeScaler
            // 
            this.m_TimeScaler.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_TimeScaler.IsFloat = true;
            this.m_TimeScaler.Location = new System.Drawing.Point(8, 16);
            this.m_TimeScaler.Maximum = 100F;
            this.m_TimeScaler.Minimum = 0F;
            this.m_TimeScaler.Name = "m_TimeScaler";
            this.m_TimeScaler.Size = new System.Drawing.Size(264, 24);
            this.m_TimeScaler.Step = 0.01F;
            this.m_TimeScaler.TabIndex = 13;
            this.m_TimeScaler.Text = "Time Scaler";
            this.m_TimeScaler.UseTrackbar = false;
            this.m_TimeScaler.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_TimeScaler.ValueXPath = null;
            // 
            // m_ButtonReset
            // 
            this.m_ButtonReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonReset.Location = new System.Drawing.Point(278, 51);
            this.m_ButtonReset.Name = "m_ButtonReset";
            this.m_ButtonReset.Size = new System.Drawing.Size(120, 23);
            this.m_ButtonReset.TabIndex = 14;
            this.m_ButtonReset.Text = "Reset";
            this.m_ButtonReset.Click += new System.EventHandler(this.m_ButtonReset_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.m_CheckBoxStartFromCamera);
            this.groupBox2.Controls.Add(this.m_ButtonStartEffect);
            this.groupBox2.Controls.Add(this.m_SliderStartDist);
            this.groupBox2.Controls.Add(this.m_SliderPosZ);
            this.groupBox2.Controls.Add(this.m_SliderPosY);
            this.groupBox2.Controls.Add(this.m_SliderPosX);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(8, 88);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(438, 144);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Effect:";
            // 
            // m_CheckBoxStartFromCamera
            // 
            this.m_CheckBoxStartFromCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CheckBoxStartFromCamera.Location = new System.Drawing.Point(16, 16);
            this.m_CheckBoxStartFromCamera.Name = "m_CheckBoxStartFromCamera";
            this.m_CheckBoxStartFromCamera.Size = new System.Drawing.Size(144, 40);
            this.m_CheckBoxStartFromCamera.TabIndex = 14;
            this.m_CheckBoxStartFromCamera.Text = "Start Infront of Camera";
            this.m_CheckBoxStartFromCamera.CheckedChanged += new System.EventHandler(this.m_CheckBoxStartFromCamera_CheckedChanged);
            // 
            // m_ButtonStartEffect
            // 
            this.m_ButtonStartEffect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonStartEffect.Location = new System.Drawing.Point(160, 104);
            this.m_ButtonStartEffect.Name = "m_ButtonStartEffect";
            this.m_ButtonStartEffect.Size = new System.Drawing.Size(120, 23);
            this.m_ButtonStartEffect.TabIndex = 3;
            this.m_ButtonStartEffect.Text = "Start";
            this.m_ButtonStartEffect.Click += new System.EventHandler(this.m_ButtonStartEffect_Click);
            // 
            // m_SliderStartDist
            // 
            this.m_SliderStartDist.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderStartDist.IsFloat = true;
            this.m_SliderStartDist.Location = new System.Drawing.Point(176, 24);
            this.m_SliderStartDist.Maximum = 100F;
            this.m_SliderStartDist.Minimum = 0F;
            this.m_SliderStartDist.Name = "m_SliderStartDist";
            this.m_SliderStartDist.Size = new System.Drawing.Size(240, 24);
            this.m_SliderStartDist.Step = 0.01F;
            this.m_SliderStartDist.TabIndex = 13;
            this.m_SliderStartDist.Text = "Start Distance";
            this.m_SliderStartDist.UseTrackbar = false;
            this.m_SliderStartDist.Value = new decimal(new int[] {
            0,
            0,
            0,
            -2147352576});
            this.m_SliderStartDist.ValueXPath = null;
            // 
            // m_SliderPosZ
            // 
            this.m_SliderPosZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderPosZ.IsFloat = true;
            this.m_SliderPosZ.Location = new System.Drawing.Point(306, 56);
            this.m_SliderPosZ.Maximum = 9000F;
            this.m_SliderPosZ.Minimum = -9000F;
            this.m_SliderPosZ.Name = "m_SliderPosZ";
            this.m_SliderPosZ.Size = new System.Drawing.Size(112, 24);
            this.m_SliderPosZ.Step = 0.01F;
            this.m_SliderPosZ.TabIndex = 25;
            this.m_SliderPosZ.Text = "Z";
            this.m_SliderPosZ.UseTrackbar = false;
            this.m_SliderPosZ.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderPosZ.ValueXPath = null;
            // 
            // m_SliderPosY
            // 
            this.m_SliderPosY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderPosY.IsFloat = true;
            this.m_SliderPosY.Location = new System.Drawing.Point(178, 56);
            this.m_SliderPosY.Maximum = 9000F;
            this.m_SliderPosY.Minimum = -9000F;
            this.m_SliderPosY.Name = "m_SliderPosY";
            this.m_SliderPosY.Size = new System.Drawing.Size(112, 24);
            this.m_SliderPosY.Step = 0.01F;
            this.m_SliderPosY.TabIndex = 24;
            this.m_SliderPosY.Text = "Y";
            this.m_SliderPosY.UseTrackbar = false;
            this.m_SliderPosY.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderPosY.ValueXPath = null;
            // 
            // m_SliderPosX
            // 
            this.m_SliderPosX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderPosX.IsFloat = true;
            this.m_SliderPosX.Location = new System.Drawing.Point(58, 56);
            this.m_SliderPosX.Maximum = 9000F;
            this.m_SliderPosX.Minimum = -9000F;
            this.m_SliderPosX.Name = "m_SliderPosX";
            this.m_SliderPosX.Size = new System.Drawing.Size(112, 24);
            this.m_SliderPosX.Step = 0.01F;
            this.m_SliderPosX.TabIndex = 23;
            this.m_SliderPosX.Text = "X";
            this.m_SliderPosX.UseTrackbar = false;
            this.m_SliderPosX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderPosX.ValueXPath = null;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 26;
            this.label1.Text = "Position:";
            // 
            // m_ButtonPauseEffect
            // 
            this.m_ButtonPauseEffect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonPauseEffect.Location = new System.Drawing.Point(278, 16);
            this.m_ButtonPauseEffect.Name = "m_ButtonPauseEffect";
            this.m_ButtonPauseEffect.Size = new System.Drawing.Size(120, 23);
            this.m_ButtonPauseEffect.TabIndex = 4;
            this.m_ButtonPauseEffect.Text = "Pause";
            this.m_ButtonPauseEffect.Click += new System.EventHandler(this.m_ButtonPauseEffect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.m_SliderTargetFPS);
            this.groupBox1.Controls.Add(this.m_TimeScaler);
            this.groupBox1.Controls.Add(this.m_ButtonPauseEffect);
            this.groupBox1.Controls.Add(this.m_ButtonReset);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(438, 80);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Global";
            // 
            // m_SliderTargetFPS
            // 
            this.m_SliderTargetFPS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderTargetFPS.IsFloat = true;
            this.m_SliderTargetFPS.Location = new System.Drawing.Point(8, 46);
            this.m_SliderTargetFPS.Maximum = 60F;
            this.m_SliderTargetFPS.Minimum = 0F;
            this.m_SliderTargetFPS.Name = "m_SliderTargetFPS";
            this.m_SliderTargetFPS.Size = new System.Drawing.Size(193, 24);
            this.m_SliderTargetFPS.Step = 0.01F;
            this.m_SliderTargetFPS.TabIndex = 27;
            this.m_SliderTargetFPS.Text = "Target  FPS";
            this.m_SliderTargetFPS.UseTrackbar = false;
            this.m_SliderTargetFPS.Value = new decimal(new int[] {
            0,
            0,
            0,
            -2147352576});
            this.m_SliderTargetFPS.ValueXPath = null;
            // 
            // PlayControl
            // 
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "PlayControl";
            this.Size = new System.Drawing.Size(455, 243);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

		}

        private ragUi.ControlSlider m_SliderTargetFPS;

        public static string MyName = "Play Controls";

        public static ragWidgetPage.WidgetPage CreatePage( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
        {
            if ( sm_Instance == null )
            {
                sm_Instance = new PlayControl( appName, bankMgr, dockControl );

                if ( ViewPlugIn.Instance.CheckPluginStatus() )
                {
                    Init();
                }

                return sm_Instance;
            }
            else
            {
                return sm_Instance;
            }
        }

        public static void Init()
        {
            if ( sm_Instance != null )
            {
                sm_Instance.FindWidgetHandles();
            }
        }

		public static void Shutdown()
		{
            if ( sm_Instance != null )
            {
                sm_Instance.ClearView();
                sm_Instance = null;
            }
		}

		public PlayControl(String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl)
			: base(appName, bankMgr, dockControl)
	    {
		    InitializeComponent();		    
	    }

		public override object GetStateData()
		{
			StateData ret = new StateData();
			ret.m_CurrSelection = m_SelectedEffect;
			return ret;
		}

		public override void SetStateData(object d)
		{
			StateData data = d as StateData;
			m_WidgetRoot=null;
			m_SelectedEffect = data.m_CurrSelection;
			FindWidgetHandles();
		}
		


		void NullPointers()
		{
			m_WidgetResetEffects = null;
			m_WidgetPauseEffects = null;
			m_WidgetStartEffect  = null;
			m_WidgetSelectedEffect = null;
		}

		void FindWidgetHandles()
		{
			RemoveBindings();
			NullPointers();

			string rmptfxRoot = "rage - Rmptfx";
			m_WidgetRoot = m_BankManager.FindFirstWidgetFromPath(rmptfxRoot) as WidgetGroupBase;

			if(m_WidgetRoot == null)
				return;

			//Play control
			ReferenceGroup("Play Control");
			WidgetGroup playcontrol = m_WidgetRoot.FindFirstWidgetFromPath("Play Control") as WidgetGroup;
			if(playcontrol != null)
			{

				m_WidgetResetEffects = playcontrol.FindFirstWidgetFromPath("Reset") as WidgetButton;
				m_WidgetPauseEffects = playcontrol.FindFirstWidgetFromPath("Pause") as WidgetButton;
				m_WidgetStartEffect= playcontrol.FindFirstWidgetFromPath("Start") as WidgetButton;
				m_WidgetSelectedEffect= playcontrol.FindFirstWidgetFromPath("CurEffect") as WidgetText;
				
				Bind(playcontrol,"FrameTimeScaler", m_TimeScaler);				
				Bind(playcontrol,"Distance from Camera", m_SliderStartDist);
				Bind(playcontrol,"StartFromCamera", m_CheckBoxStartFromCamera, "Checked");

				Bind(playcontrol,"StartPosition X", m_SliderPosX);
				Bind(playcontrol,"StartPosition Y", m_SliderPosY);
				Bind(playcontrol,"StartPosition Z", m_SliderPosZ);
			}
            
            //Time Control 
            ReferenceGroup("Time Control");
            WidgetGroup timecontrol = m_WidgetRoot.FindFirstWidgetFromPath("Time Control") as WidgetGroup;
            if(timecontrol != null)
                Bind(timecontrol, "TargetFPS", m_SliderTargetFPS);

			UpdateInterface();
		}

        public override string GetMyName()
        {
            return MyName;
        }

		public override string GetTabText() 
		{
            if ( this.Enabled && (m_SelectedEffect != null) )
            {
                return "Play Controls (" + m_SelectedEffect + ")";
            }
            else
            {
                return "Play Controls";
            }
		}

        public override bool IsSingleInstance
        {
            get
            {
                return true;
            }
        }

		public void SelectEffect(string name)
		{
			m_SelectedEffect = name;
			m_DockControl.Text = m_DockControl.TabText = GetTabText();
			m_WidgetSelectedEffect.String = name;
			groupBox2.Text = "Effect: "+m_SelectedEffect;
		}

		public void UpdateInterface()
		{
			if(m_CheckBoxStartFromCamera.Checked)
			{
				m_SliderPosX.Enabled = false;
				m_SliderPosY.Enabled = false;
				m_SliderPosZ.Enabled = false;
				m_SliderStartDist.Enabled =true;
			}
			else
			{
				m_SliderPosX.Enabled = true;
				m_SliderPosY.Enabled = true;
				m_SliderPosZ.Enabled = true;
				m_SliderStartDist.Enabled =false;
			}
		}

		#region Variables
		public static PlayControl sm_Instance;
		public string m_SelectedEffect;
		private ragUi.ControlSlider m_TimeScaler;
		private System.Windows.Forms.Button m_ButtonReset;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button m_ButtonPauseEffect;
		private System.Windows.Forms.Button m_ButtonStartEffect;
		private ragUi.ControlSlider m_SliderStartDist;
		WidgetButton m_WidgetPauseEffects=null;
		WidgetButton m_WidgetResetEffects=null;
		WidgetButton m_WidgetStartEffect = null;
		private System.Windows.Forms.CheckBox m_CheckBoxStartFromCamera;
		private System.Windows.Forms.Label label1;
		private ragUi.ControlSlider m_SliderPosZ;
		private ragUi.ControlSlider m_SliderPosY;
		private ragUi.ControlSlider m_SliderPosX;
		private System.Windows.Forms.GroupBox groupBox1;
		WidgetText   m_WidgetSelectedEffect = null;
		#endregion

		private void m_ButtonStartEffect_Click(object sender, System.EventArgs e)
		{
			m_WidgetStartEffect.Activate();
		}

		private void m_ButtonReset_Click(object sender, System.EventArgs e)
		{
			m_WidgetResetEffects.Activate();
		}

		private void m_ButtonPauseEffect_Click(object sender, System.EventArgs e)
		{
			m_WidgetPauseEffects.Activate();
		}

		private void m_CheckBoxStartFromCamera_CheckedChanged(object sender, System.EventArgs e)
		{
			UpdateInterface();
		}
	}
}
