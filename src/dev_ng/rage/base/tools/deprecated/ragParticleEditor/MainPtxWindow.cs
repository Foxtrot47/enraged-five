using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using ragCore;
using ragWidgets;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for MainPtxWindow.
	/// </summary>
	public class MainPtxWindow : ragWidgetPage.WidgetPage
    {
		private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label m_LabelDrawTime;
		private System.Windows.Forms.Label m_LabelUpdateTime;
		private System.Windows.Forms.Label m_LabelTotalPoints;
		private System.Windows.Forms.ListBox m_EffectsList;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Label m_LabelCodeVersion;
		private System.Windows.Forms.ListBox m_EmittersList;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ListBox m_PtxRulesList;
        private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ContextMenu m_MenuEffect;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.ContextMenu m_MenuEmitter;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.ContextMenu m_MenuPtxRule;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.MenuItem m_MenuEffectSaveAs;
		private System.Windows.Forms.MenuItem m_MenuEmitterSaveAs;
        private System.Windows.Forms.MenuItem m_MenuPtxRuleSaveAs;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem newToolStripMenuItem;
        private ToolStripMenuItem loadToolStripMenuItem;
        private ToolStripMenuItem saveAllToolStripMenuItem;
        private ToolStripMenuItem editToolStripMenuItem;
        private ToolStripMenuItem propertiesToolStripMenuItem;
        private ToolStripMenuItem newEffectRuleToolStripMenuItem;
        private ToolStripMenuItem newEmitterRuleToolStripMenuItem;
        private ToolStripMenuItem newPtxRuleToolStripMenuItem;
        private ToolStripMenuItem spriteRuleToolStripMenuItem;
        private ToolStripMenuItem modelRuleToolStripMenuItem;
        private ToolStripMenuItem loadEffectRuleToolStripMenuItem;
        private ToolStripMenuItem loadEmitterRuleToolStripMenuItem;
        private ToolStripMenuItem ptxRuleToolStripMenuItem;
        private ToolStripMenuItem oldEmitterEffectRuleToolStripMenuItem;
        private ToolStripMenuItem batchConvertToolStripMenuItem;
        private MenuItem m_RemoveEffectButton;
        private CheckBox doubleBufferCheck;
        private GroupBox groupBox2;
		private System.ComponentModel.IContainer components;

		public MainPtxWindow()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}

        public override string GetMyName()
        {
            return MyName;
        }

        private float m_Version = 2.0f;
		public override string GetTabText() 
		{
			return String.Format( "Particle Editor Ver {0:#0.0}", m_Version );
		}

        public override bool IsSingleInstance
        {
            get
            {
                return true;
            }
        }

		public static MainPtxWindow sm_Instance;

        public static string MyName = "Particle Editor";

        public static ragWidgetPage.WidgetPage CreatePage( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
        {
            if ( sm_Instance == null )
            {
                sm_Instance = new MainPtxWindow( appName, bankMgr, dockControl );

                if ( ViewPlugIn.Instance.CheckPluginStatus() )
                {
                    Init();
                }

                return sm_Instance;
            }
            else
            {
                return sm_Instance;
            }
        }

        public static void Init()
        {
            if ( sm_Instance != null )
            {
                sm_Instance.FindWidgetHandles();
                sm_Instance.BuildLists( true );
                sm_Instance.timer1.Enabled = true;
            }
        }

		public static void Shutdown()
		{
            if ( sm_Instance != null )
            {
                sm_Instance.ClearView();
                sm_Instance = null;
            }
		}

		public MainPtxWindow(String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl)
			: base(appName, bankMgr, dockControl)
		{
			InitializeComponent();
		}

		// All the widget handles we'll need

		//private bool	m_ToolTipsOn=true;
		private WidgetSliderFloat m_WidgetVersion=null;

		private WidgetButton m_WidgetSaveAll=null;
		private WidgetButton m_WidgetLoadEffectRule=null;
        private WidgetButton m_WidgetRemoveEffectRule = null;
        private WidgetButton m_WidgetNewEffectRule = null;
        private WidgetText m_WidgetNewEffectRuleName = null;
		
		private WidgetButton m_WidgetLoadEmitRule=null;
		private WidgetButton m_WidgetNewEmitRule=null;
		private WidgetText m_WidgetNewEmitRuleName=null;
		private WidgetButton m_WidgetOldEmitRule=null;
		private WidgetButton m_WidgetBatchOldEmitRule=null; 
		private WidgetText m_WidgetOldEmitRuleName=null;

		private WidgetButton m_WidgetLoadPtxRule=null;
		private WidgetButton m_WidgetNewPtxRule=null;
		private WidgetText m_WidgetNewPtxRuleName=null;
		private WidgetCombo m_WidgetNewPtxRuleType=null;
		private WidgetButton m_WidgetOldPtxRule=null;
		private WidgetText m_WidgetOldPtxRuleName=null;

		public WidgetGroup m_WidgetEffectList=null;
		public WidgetGroup m_WidgetEmitterList=null;
		public WidgetGroup m_WidgetPtxRuleList=null;
		private WidgetSliderInt m_WidgetPointsActive=null;
		private WidgetSliderFloat m_WidgetUpdateTime=null;
		private WidgetSliderFloat m_WidgetDrawTime=null;

		public void NullPointers()
		{
			m_WidgetVersion=null;

			m_WidgetNewEffectRule=null;
			m_WidgetNewEffectRuleName=null;
			m_WidgetNewEmitRule=null;
			m_WidgetNewEmitRuleName=null;
			m_WidgetNewPtxRule=null;
			m_WidgetNewPtxRuleName=null;
			m_WidgetNewPtxRuleType=null;
            m_WidgetRemoveEffectRule = null;

			m_WidgetEffectList=null;
			m_WidgetEmitterList=null;
			m_WidgetPtxRuleList=null;
			m_WidgetPointsActive=null;
			m_WidgetUpdateTime=null;
			m_WidgetDrawTime=null;
		}
		void FindWidgetHandles()
		{
			RemoveBindings();
			NullPointers();
			string rmptfxRoot = "rage - Rmptfx";
			m_WidgetRoot = m_BankManager.FindFirstWidgetFromPath(rmptfxRoot) as WidgetGroupBase;

			if(m_WidgetRoot == null)
				return;
			
			m_WidgetVersion = m_WidgetRoot.FindFirstWidgetFromPath("Version") as WidgetSliderFloat;
			
			//Profiling
			this.ReferenceGroup("Profiling");
			WidgetGroup profiling = m_WidgetRoot.FindFirstWidgetFromPath("Profiling") as WidgetGroup;
			if(profiling != null)
			{
				m_WidgetPointsActive = profiling.FindFirstWidgetFromPath("Total Points Active") as WidgetSliderInt;
				m_WidgetUpdateTime = profiling.FindFirstWidgetFromPath("Total CPU Update Time (ms)") as WidgetSliderFloat;
				m_WidgetDrawTime = profiling.FindFirstWidgetFromPath("Total CPU Draw Time (ms)") as WidgetSliderFloat;
			}
			
			//Debug
			WidgetGroup debug = m_WidgetRoot.FindFirstWidgetFromPath("Debug") as WidgetGroup;
			if(debug != null)
			{
                Bind(debug, "Double Buffer", doubleBufferCheck, "Checked");

				m_WidgetSaveAll = debug.FindFirstWidgetFromPath("SaveAll") as WidgetButton;
			}
			
			//PtxRules
			this.ReferenceGroup("PtxRules");
			WidgetGroup ptxrules = m_WidgetRoot.FindFirstWidgetFromPath("PtxRules") as WidgetGroup;
			if(ptxrules != null)
			{
				m_WidgetLoadPtxRule = ptxrules.FindFirstWidgetFromPath("Load PtxRule") as WidgetButton;
				m_WidgetNewPtxRule = ptxrules.FindFirstWidgetFromPath("New PtxRule") as WidgetButton;
				m_WidgetNewPtxRuleType = ptxrules.FindFirstWidgetFromPath("Rule Type") as WidgetCombo;
				m_WidgetNewPtxRuleName = ptxrules.FindFirstWidgetFromPath("ptxRule Name") as WidgetText;
				m_WidgetOldPtxRule = ptxrules.FindFirstWidgetFromPath("Old PtxRule") as WidgetButton;
				m_WidgetOldPtxRuleName = ptxrules.FindFirstWidgetFromPath("Old PtxRule Name") as WidgetText;

			}

			//EmitRules
			this.ReferenceGroup("EmitRules");
			WidgetGroup emitrules = m_WidgetRoot.FindFirstWidgetFromPath("EmitRules") as WidgetGroup;
			if(emitrules != null)
			{
				m_WidgetOldEmitRule = emitrules.FindFirstWidgetFromPath("Old Emitter") as WidgetButton;
				m_WidgetBatchOldEmitRule = emitrules.FindFirstWidgetFromPath("Batch Old Emitter") as WidgetButton;
				m_WidgetOldEmitRuleName = emitrules.FindFirstWidgetFromPath("Old Emitter Name") as WidgetText;
				m_WidgetLoadEmitRule = emitrules.FindFirstWidgetFromPath("Load Emitter") as WidgetButton;
				m_WidgetNewEmitRule = emitrules.FindFirstWidgetFromPath("New Emitter") as WidgetButton;
				m_WidgetNewEmitRuleName = emitrules.FindFirstWidgetFromPath("Emitter Name") as WidgetText;
			}
			
			//EffectRules
			this.ReferenceGroup("EffectRules");
			WidgetGroup effectrules= m_WidgetRoot.FindFirstWidgetFromPath("EffectRules") as WidgetGroup;
			if(effectrules != null)
			{
				m_WidgetLoadEffectRule = effectrules.FindFirstWidgetFromPath("Load Effect") as WidgetButton;
				m_WidgetNewEffectRule = effectrules.FindFirstWidgetFromPath("New Effect") as WidgetButton;
				m_WidgetNewEffectRuleName = effectrules.FindFirstWidgetFromPath("Effect Name") as WidgetText;
                m_WidgetRemoveEffectRule = effectrules.FindFirstWidgetFromPath("Remove Effect") as WidgetButton;
			}
			
			m_WidgetEffectList = m_WidgetRoot.FindFirstWidgetFromPath("EffectRules/Rules") as WidgetGroup;
			m_WidgetEmitterList = m_WidgetRoot.FindFirstWidgetFromPath("EmitRules/Rules") as WidgetGroup;
			m_WidgetPtxRuleList = m_WidgetRoot.FindFirstWidgetFromPath("PtxRules/Rules") as WidgetGroup;

			
			if(m_WidgetVersion!=null)
			{
				if(m_Version != m_WidgetVersion.Value)
					SetLabel(m_LabelCodeVersion, "Code Version ({0:#0.0}) -Out of synch!", m_WidgetVersion);
				else
					SetLabel(m_LabelCodeVersion, "Code Version ({0:#0.0})", m_WidgetVersion);
			}
			else
				m_LabelCodeVersion.Text = "Code Version (0.0) -Out of synch!";
			
		}

		public void SelectEffect(string name)
		{
			m_EffectsList.SelectedIndex=-1;
			for(int i=0;i<m_EffectsList.Items.Count;i++)
			{
				if(m_EffectsList.Items[i].ToString() == name)
				{
					m_EffectsList.SelectedIndex=i;
					PlayControl.sm_Instance.SelectEffect(name);
					return;
				}
			}
		}
		public void SelectEmitter(string name)
		{
			m_EmittersList.SelectedIndex=-1;
			for(int i=0;i<m_EmittersList.Items.Count;i++)
			{
				if(m_EmittersList.Items[i].ToString() == name)
				{ 
					m_EmittersList.SelectedIndex=i;
					return;
				}
			}
		}
		public void SelectPtxRule(string name)
		{
			m_PtxRulesList.SelectedIndex =-1;
			for(int i=0;i<m_PtxRulesList.Items.Count;i++)
			{
				if(m_PtxRulesList.Items[i].ToString() == name)
				{ 
					m_PtxRulesList.SelectedIndex =i;
					return;
				}
			}
		}
		void BuildLists(bool preserveSelection)
		{
			BuildEffectList(preserveSelection);
			BuildEmitterList(preserveSelection);
			BuildPtxRuleList(preserveSelection);
		}
		void BuildEffectList(bool preserveSelection)
		{
			string item = "";
			if(m_EffectsList.SelectedItem!=null)
				item = m_EffectsList.SelectedItem.ToString();
			m_EffectsList.BeginUpdate();
			m_EffectsList.Sorted=false;
			m_EffectsList.Items.Clear();
			m_EffectsList.ClearSelected();

			if (m_WidgetEffectList != null) {
				foreach(Widget w in m_WidgetEffectList.List)
				{
					if (w is WidgetGroup) 
					{
						m_EffectsList.Items.Add(w);
					}
				}
			}
			m_EffectsList.Sorted=true;
			m_EffectsList.EndUpdate();
			if(preserveSelection)
				SelectEffect(item);
		}

		void BuildEmitterList(bool preserveSelection)
		{
			string item = "";
			if(m_EmittersList.SelectedItem!=null)
				item = m_EmittersList.SelectedItem.ToString();
			m_EmittersList.BeginUpdate();
			m_EmittersList.Sorted=false;
			m_EmittersList.Items.Clear();
			m_EmittersList.ClearSelected();


			if (m_WidgetEmitterList != null) 
			{
				foreach(Widget w in m_WidgetEmitterList.List)
				{
					if (w is WidgetGroup) 
					{
						m_EmittersList.Items.Add(w);
					}
				}
			}
			m_EmittersList.Sorted=true;
			m_EmittersList.EndUpdate();
			if(preserveSelection)
				SelectEmitter(item);
		}

		void BuildPtxRuleList(bool preserveSelection)
		{
			string item = "";
			if(m_PtxRulesList.SelectedItem!=null)
				item=m_PtxRulesList.SelectedItem.ToString();
			m_PtxRulesList.BeginUpdate();
			m_PtxRulesList.Sorted=false;
			m_PtxRulesList.Items.Clear();
			m_PtxRulesList.ClearSelected();

			if (m_WidgetPtxRuleList != null) 
			{
				foreach(Widget w in m_WidgetPtxRuleList.List)
				{
					if (w is WidgetGroup) 
					{
						m_PtxRulesList.Items.Add(w);
					}
				}
			}

			m_PtxRulesList.Sorted=true;
			
			if(preserveSelection)
				SelectPtxRule(item);
			m_PtxRulesList.EndUpdate();
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_LabelDrawTime = new System.Windows.Forms.Label();
            this.m_LabelUpdateTime = new System.Windows.Forms.Label();
            this.m_LabelTotalPoints = new System.Windows.Forms.Label();
            this.m_EffectsList = new System.Windows.Forms.ListBox();
            this.m_MenuEffect = new System.Windows.Forms.ContextMenu();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.m_RemoveEffectButton = new System.Windows.Forms.MenuItem();
            this.m_MenuEffectSaveAs = new System.Windows.Forms.MenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.m_LabelCodeVersion = new System.Windows.Forms.Label();
            this.m_EmittersList = new System.Windows.Forms.ListBox();
            this.m_MenuEmitter = new System.Windows.Forms.ContextMenu();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.m_MenuEmitterSaveAs = new System.Windows.Forms.MenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.m_PtxRulesList = new System.Windows.Forms.ListBox();
            this.m_MenuPtxRule = new System.Windows.Forms.ContextMenu();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.m_MenuPtxRuleSaveAs = new System.Windows.Forms.MenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newEffectRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newEmitterRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newPtxRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spriteRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modelRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadEffectRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadEmitterRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ptxRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oldEmitterEffectRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.batchConvertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doubleBufferCheck = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.m_LabelDrawTime);
            this.groupBox1.Controls.Add(this.m_LabelUpdateTime);
            this.groupBox1.Controls.Add(this.m_LabelTotalPoints);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(8, 56);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(280, 72);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Profiling";
            // 
            // m_LabelDrawTime
            // 
            this.m_LabelDrawTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelDrawTime.Location = new System.Drawing.Point(8, 48);
            this.m_LabelDrawTime.Name = "m_LabelDrawTime";
            this.m_LabelDrawTime.Size = new System.Drawing.Size(240, 16);
            this.m_LabelDrawTime.TabIndex = 4;
            this.m_LabelDrawTime.Text = "DrawTime (cpu)";
            // 
            // m_LabelUpdateTime
            // 
            this.m_LabelUpdateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelUpdateTime.Location = new System.Drawing.Point(8, 32);
            this.m_LabelUpdateTime.Name = "m_LabelUpdateTime";
            this.m_LabelUpdateTime.Size = new System.Drawing.Size(248, 16);
            this.m_LabelUpdateTime.TabIndex = 3;
            this.m_LabelUpdateTime.Text = "Update Time (cpu)";
            // 
            // m_LabelTotalPoints
            // 
            this.m_LabelTotalPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelTotalPoints.Location = new System.Drawing.Point(8, 18);
            this.m_LabelTotalPoints.Name = "m_LabelTotalPoints";
            this.m_LabelTotalPoints.Size = new System.Drawing.Size(100, 16);
            this.m_LabelTotalPoints.TabIndex = 0;
            this.m_LabelTotalPoints.Text = "Total Points: ";
            // 
            // m_EffectsList
            // 
            this.m_EffectsList.ContextMenu = this.m_MenuEffect;
            this.m_EffectsList.Location = new System.Drawing.Point(16, 160);
            this.m_EffectsList.Name = "m_EffectsList";
            this.m_EffectsList.Size = new System.Drawing.Size(272, 160);
            this.m_EffectsList.Sorted = true;
            this.m_EffectsList.TabIndex = 8;
            this.m_EffectsList.DoubleClick += new System.EventHandler(this.StartSelectedWidget);
            this.m_EffectsList.SelectedIndexChanged += new System.EventHandler(this.m_EffectsList_SelectedIndexChanged);
            this.m_EffectsList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.m_EffectsList_MouseDown);
            // 
            // m_MenuEffect
            // 
            this.m_MenuEffect.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem2,
            this.menuItem1,
            this.m_RemoveEffectButton,
            this.m_MenuEffectSaveAs});
            this.m_MenuEffect.Popup += new System.EventHandler(this.m_MenuEffect_Popup);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "New Effect";
            this.menuItem2.Click += new System.EventHandler(this.m_ButtonNewEffect_Activate);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 1;
            this.menuItem1.Text = "Load Effect";
            this.menuItem1.Click += new System.EventHandler(this.m_ButtonLoadEffect_Activate);
            // 
            // m_RemoveEffectButton
            // 
            this.m_RemoveEffectButton.Index = 2;
            this.m_RemoveEffectButton.Text = "Remove Effect";
            this.m_RemoveEffectButton.Click += new System.EventHandler(this.m_ButtonRemoveEffect_Activate);
            // 
            // m_MenuEffectSaveAs
            // 
            this.m_MenuEffectSaveAs.Index = 3;
            this.m_MenuEffectSaveAs.Text = "Save {} As...";
            this.m_MenuEffectSaveAs.Click += new System.EventHandler(this.m_MenuEffectSaveAs_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Effects";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.UpdateProfiling);
            // 
            // m_LabelCodeVersion
            // 
            this.m_LabelCodeVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelCodeVersion.Location = new System.Drawing.Point(16, 32);
            this.m_LabelCodeVersion.Name = "m_LabelCodeVersion";
            this.m_LabelCodeVersion.Size = new System.Drawing.Size(264, 15);
            this.m_LabelCodeVersion.TabIndex = 13;
            this.m_LabelCodeVersion.Text = "Code Version";
            // 
            // m_EmittersList
            // 
            this.m_EmittersList.ContextMenu = this.m_MenuEmitter;
            this.m_EmittersList.Location = new System.Drawing.Point(301, 160);
            this.m_EmittersList.Name = "m_EmittersList";
            this.m_EmittersList.Size = new System.Drawing.Size(272, 160);
            this.m_EmittersList.Sorted = true;
            this.m_EmittersList.TabIndex = 15;
            this.m_EmittersList.SelectedIndexChanged += new System.EventHandler(this.m_EmittersList_SelectedIndexChanged);
            // 
            // m_MenuEmitter
            // 
            this.m_MenuEmitter.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem4,
            this.menuItem3,
            this.m_MenuEmitterSaveAs});
            this.m_MenuEmitter.Popup += new System.EventHandler(this.m_MenuEmitter_Popup);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 0;
            this.menuItem4.Text = "New Emitter";
            this.menuItem4.Click += new System.EventHandler(this.m_ButtonNewEmitter_Activate);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "Load Emitter";
            this.menuItem3.Click += new System.EventHandler(this.m_ButtonLoadEmitter_Activate);
            // 
            // m_MenuEmitterSaveAs
            // 
            this.m_MenuEmitterSaveAs.Index = 2;
            this.m_MenuEmitterSaveAs.Text = "Save {} As...";
            this.m_MenuEmitterSaveAs.Click += new System.EventHandler(this.m_MenuSaveEmitterAs_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(301, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Emitters";
            // 
            // m_PtxRulesList
            // 
            this.m_PtxRulesList.ContextMenu = this.m_MenuPtxRule;
            this.m_PtxRulesList.Location = new System.Drawing.Point(584, 160);
            this.m_PtxRulesList.Name = "m_PtxRulesList";
            this.m_PtxRulesList.Size = new System.Drawing.Size(272, 160);
            this.m_PtxRulesList.Sorted = true;
            this.m_PtxRulesList.TabIndex = 17;
            this.m_PtxRulesList.SelectedIndexChanged += new System.EventHandler(this.m_PtxRulesList_SelectedIndexChanged);
            // 
            // m_MenuPtxRule
            // 
            this.m_MenuPtxRule.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem6,
            this.menuItem5,
            this.m_MenuPtxRuleSaveAs});
            this.m_MenuPtxRule.Popup += new System.EventHandler(this.m_MenuPtxRule_Popup);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 0;
            this.menuItem6.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem7,
            this.menuItem8});
            this.menuItem6.Text = "New Ptx Rule";
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 0;
            this.menuItem7.Text = "New Sprite Rule";
            this.menuItem7.Click += new System.EventHandler(this.m_ButtonNewPtxSprite_Activate);
            // 
            // menuItem8
            // 
            this.menuItem8.Index = 1;
            this.menuItem8.Text = "New Model Rule";
            this.menuItem8.Click += new System.EventHandler(this.m_ButtonNewPtxModel_Activate);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 1;
            this.menuItem5.Text = "Load Ptx Rule";
            this.menuItem5.Click += new System.EventHandler(this.m_ButtonLoadPtxRule_Activate);
            // 
            // m_MenuPtxRuleSaveAs
            // 
            this.m_MenuPtxRuleSaveAs.Index = 2;
            this.m_MenuPtxRuleSaveAs.Text = "Save {} As...";
            this.m_MenuPtxRuleSaveAs.Click += new System.EventHandler(this.m_MenuPtxRuleSaveAs_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(584, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 16);
            this.label3.TabIndex = 16;
            this.label3.Text = "Ptx Rules";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(952, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.saveAllToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newEffectRuleToolStripMenuItem,
            this.newEmitterRuleToolStripMenuItem,
            this.newPtxRuleToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.newToolStripMenuItem.Text = "&New";
            // 
            // newEffectRuleToolStripMenuItem
            // 
            this.newEffectRuleToolStripMenuItem.Name = "newEffectRuleToolStripMenuItem";
            this.newEffectRuleToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.newEffectRuleToolStripMenuItem.Text = "E&ffect Rule";
            this.newEffectRuleToolStripMenuItem.Click += new System.EventHandler(this.m_ButtonNewEffect_Activate);
            // 
            // newEmitterRuleToolStripMenuItem
            // 
            this.newEmitterRuleToolStripMenuItem.Name = "newEmitterRuleToolStripMenuItem";
            this.newEmitterRuleToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.newEmitterRuleToolStripMenuItem.Text = "&Emitter Rule";
            this.newEmitterRuleToolStripMenuItem.Click += new System.EventHandler(this.m_ButtonNewEmitter_Activate);
            // 
            // newPtxRuleToolStripMenuItem
            // 
            this.newPtxRuleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spriteRuleToolStripMenuItem,
            this.modelRuleToolStripMenuItem});
            this.newPtxRuleToolStripMenuItem.Name = "newPtxRuleToolStripMenuItem";
            this.newPtxRuleToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.newPtxRuleToolStripMenuItem.Text = "New &Ptx Rule";
            // 
            // spriteRuleToolStripMenuItem
            // 
            this.spriteRuleToolStripMenuItem.Name = "spriteRuleToolStripMenuItem";
            this.spriteRuleToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.spriteRuleToolStripMenuItem.Text = "Sp&rite Rule";
            this.spriteRuleToolStripMenuItem.Click += new System.EventHandler(this.m_ButtonNewPtxSprite_Activate);
            // 
            // modelRuleToolStripMenuItem
            // 
            this.modelRuleToolStripMenuItem.Name = "modelRuleToolStripMenuItem";
            this.modelRuleToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.modelRuleToolStripMenuItem.Text = "&Model Rule";
            this.modelRuleToolStripMenuItem.Click += new System.EventHandler(this.m_ButtonNewPtxModel_Activate);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadEffectRuleToolStripMenuItem,
            this.loadEmitterRuleToolStripMenuItem,
            this.ptxRuleToolStripMenuItem,
            this.oldEmitterEffectRuleToolStripMenuItem,
            this.batchConvertToolStripMenuItem});
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.loadToolStripMenuItem.Text = "&Load";
            // 
            // loadEffectRuleToolStripMenuItem
            // 
            this.loadEffectRuleToolStripMenuItem.Name = "loadEffectRuleToolStripMenuItem";
            this.loadEffectRuleToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.loadEffectRuleToolStripMenuItem.Text = "E&ffect Rule...";
            this.loadEffectRuleToolStripMenuItem.Click += new System.EventHandler(this.m_ButtonLoadEffect_Activate);
            // 
            // loadEmitterRuleToolStripMenuItem
            // 
            this.loadEmitterRuleToolStripMenuItem.Name = "loadEmitterRuleToolStripMenuItem";
            this.loadEmitterRuleToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.loadEmitterRuleToolStripMenuItem.Text = "&Emitter Rule...";
            this.loadEmitterRuleToolStripMenuItem.Click += new System.EventHandler(this.m_ButtonLoadEmitter_Activate);
            // 
            // ptxRuleToolStripMenuItem
            // 
            this.ptxRuleToolStripMenuItem.Name = "ptxRuleToolStripMenuItem";
            this.ptxRuleToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ptxRuleToolStripMenuItem.Text = "&Ptx Rule...";
            this.ptxRuleToolStripMenuItem.Click += new System.EventHandler(this.m_ButtonLoadPtxRule_Activate);
            // 
            // oldEmitterEffectRuleToolStripMenuItem
            // 
            this.oldEmitterEffectRuleToolStripMenuItem.Name = "oldEmitterEffectRuleToolStripMenuItem";
            this.oldEmitterEffectRuleToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.oldEmitterEffectRuleToolStripMenuItem.Text = "&Old Emitter/Effect Rule...";
            this.oldEmitterEffectRuleToolStripMenuItem.Visible = false;
            this.oldEmitterEffectRuleToolStripMenuItem.Click += new System.EventHandler(this.m_MenuConvertOld_Activate);
            // 
            // batchConvertToolStripMenuItem
            // 
            this.batchConvertToolStripMenuItem.Name = "batchConvertToolStripMenuItem";
            this.batchConvertToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.batchConvertToolStripMenuItem.Text = "&Batch Convert...";
            this.batchConvertToolStripMenuItem.Visible = false;
            this.batchConvertToolStripMenuItem.Click += new System.EventHandler(this.m_MenuBatchConvert_Activate);
            // 
            // saveAllToolStripMenuItem
            // 
            this.saveAllToolStripMenuItem.Name = "saveAllToolStripMenuItem";
            this.saveAllToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.saveAllToolStripMenuItem.Text = "Save Al&l";
            this.saveAllToolStripMenuItem.Click += new System.EventHandler(this.m_MenuSaveAll_Activate);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.propertiesToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            this.propertiesToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.propertiesToolStripMenuItem.Text = "&Properties...";
            // 
            // doubleBufferCheck
            // 
            this.doubleBufferCheck.AutoSize = true;
            this.doubleBufferCheck.Location = new System.Drawing.Point(6, 19);
            this.doubleBufferCheck.Name = "doubleBufferCheck";
            this.doubleBufferCheck.Size = new System.Drawing.Size(105, 17);
            this.doubleBufferCheck.TabIndex = 19;
            this.doubleBufferCheck.Text = "Double Buffering";
            this.doubleBufferCheck.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.doubleBufferCheck);
            this.groupBox2.Location = new System.Drawing.Point(301, 56);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(272, 72);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Debug";
            // 
            // MainPtxWindow
            // 
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.m_PtxRulesList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_EmittersList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_LabelCodeVersion);
            this.Controls.Add(this.m_EffectsList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "MainPtxWindow";
            this.Size = new System.Drawing.Size(952, 608);
            this.groupBox1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region Properties
		public static MainPtxWindow Instance
		{
			get
			{
				if(MainPtxWindow.sm_Instance==null)
				{
                    ViewPlugIn.Instance.CreateAndOpenMainWindowView();

					if(EffectProperties.sm_Instance !=null)
						EffectProperties.sm_Instance.m_WidgetRoot=null;
					if(EmitterProperties.sm_Instance!=null)
						EmitterProperties.sm_Instance.m_WidgetRoot=null;
					if(PtxRuleProperties.sm_Instance!=null)
						PtxRuleProperties.sm_Instance.m_WidgetRoot=null;
					ViewPlugIn.Instance.RebuildAllPanels();
				}
				return MainPtxWindow.sm_Instance;
			}
		}
		#endregion


		public void InitHelpProvider()
		{

		}
		public string NewPtxSpriteRule()
		{
			m_WidgetNewPtxRuleType.Value=0;
			return NewPtxRule("New PtxSprite Rule Name");
		}
		public string NewPtxModelRule()
		{
			m_WidgetNewPtxRuleType.Value=1;
			return NewPtxRule("New PtxModel Rule Name");
		}
		public string NewEffectRule(string title)
		{
			SaveFileDialog sfd = new SaveFileDialog();
			if(UserData.LastEffectPath.Length>0)
				sfd.InitialDirectory = UserData.LastEffectPath;
			sfd.AddExtension=true;
			sfd.Filter="PtxEffect (*.effectrule)|*.effectrule";
			sfd.ValidateNames=true;
			sfd.OverwritePrompt=true;
			sfd.Title=title;
			if (sfd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastEffectPath = System.IO.Path.GetDirectoryName(sfd.FileName);
				m_WidgetNewEffectRuleName.String = System.IO.Path.GetFileNameWithoutExtension(sfd.FileName);
				string path=m_WidgetNewEffectRule.FindPath();
                EffectProperties.sm_Instance.RemoveBindings();
                EmitterProperties.sm_Instance.RemoveBindings();
                PtxRuleProperties.sm_Instance.RemoveBindings();

                RemoveBindings();
                WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
                if (button != null)
                    button.Activate();
                //m_WidgetNewEffectRule.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
				return m_WidgetNewEffectRuleName.String;
			}
			return "";
		}
		public string NewEmitRule(string title)
		{
			SaveFileDialog sfd = new SaveFileDialog();
			if(UserData.LastEmitterPath.Length>0)
				sfd.InitialDirectory = UserData.LastEmitterPath;
			sfd.AddExtension=true;
			sfd.Filter="PtxEmitter (*.emitrule)|*.emitrule";
			sfd.ValidateNames=true;
			sfd.OverwritePrompt=true;
			sfd.Title=title;
			if (sfd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastEmitterPath = System.IO.Path.GetDirectoryName(sfd.FileName);
				m_WidgetNewEmitRuleName.String = System.IO.Path.GetFileNameWithoutExtension(sfd.FileName);;
                string path = m_WidgetNewEmitRule.FindPath();
                EffectProperties.sm_Instance.RemoveBindings();
                EmitterProperties.sm_Instance.RemoveBindings();
                PtxRuleProperties.sm_Instance.RemoveBindings();

                RemoveBindings();
                WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
                if (button != null)
                    button.Activate();
                //m_WidgetNewEmitRule.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
				return m_WidgetNewEmitRuleName.String;
			}
			return "";
		}

		public string NewPtxRule(string title)
		{
			SaveFileDialog sfd = new SaveFileDialog();
			if(UserData.LastPtxRulePath.Length>0)
				sfd.InitialDirectory = UserData.LastPtxRulePath;
			sfd.AddExtension=true;
			sfd.Filter="PtxRule (*.ptxrule)|*.ptxrule";
			sfd.ValidateNames=true;
			sfd.OverwritePrompt=true;
			sfd.Title=title;
			if (sfd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastPtxRulePath = System.IO.Path.GetDirectoryName(sfd.FileName);
				m_WidgetNewPtxRuleName.String = System.IO.Path.GetFileNameWithoutExtension(sfd.FileName);;
                string path = m_WidgetNewPtxRule.FindPath();
                EffectProperties.sm_Instance.RemoveBindings();
                EmitterProperties.sm_Instance.RemoveBindings();
                PtxRuleProperties.sm_Instance.RemoveBindings();

                RemoveBindings();
                WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
                if (button != null)
                    button.Activate();
                //m_WidgetNewPtxRule.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
				return m_WidgetNewPtxRuleName.String;
			}
			return "";
		}
		public string LoadEffectRule()
		{
			return LoadEffectRule("Select an Effect");
		}
        public string LoadEffectRule( string title )
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if ( UserData.LastEffectPath.Length > 0 )
            {
                ofd.InitialDirectory = UserData.LastEffectPath;
            }
            
            ofd.Filter = "PtxEffect (*.effectrule)|*.effectrule|Resourced Effect (*.*)|*.*";
            ofd.CheckFileExists = true;
            ofd.Title = title;
            ofd.Multiselect = true;
            
            if ( ofd.ShowDialog() == DialogResult.OK )
            {
                foreach ( string filename in ofd.FileNames )
                {
                    UserData.LastEffectPath = System.IO.Path.GetDirectoryName( filename );
                    m_WidgetNewEffectRuleName.String = System.IO.Path.GetFileNameWithoutExtension( filename );

                    string path = m_WidgetLoadEffectRule.FindPath();
                    EffectProperties.sm_Instance.RemoveBindings();
                    EmitterProperties.sm_Instance.RemoveBindings();
                    PtxRuleProperties.sm_Instance.RemoveBindings();
                    ptxShader.RemoveAllBindings();
                    RemoveBindings();

                    WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
                    if (button != null)
                        button.Activate();
                    ViewPlugIn.Instance.RebuildAllPanels();

                    //m_WidgetLoadEffectRule.Activate();
                }

                if ( ofd.FileNames.Length > 0 )
                {
                    return m_WidgetNewEffectRuleName.String;
                }
            }
            return "";
        }

		public string LoadEmitRule()
		{
			return LoadEmitRule("Select an Emitter");
		}
		public string LoadEmitRule(string title)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			if(UserData.LastEmitterPath.Length>0)
				ofd.InitialDirectory = UserData.LastEmitterPath;
			ofd.Filter="PtxEmitter (*.emitrule)|*.emitrule|Resourced Emitter (*.*)|*.*";
			ofd.CheckFileExists=true;
			ofd.Title= title;
			if (ofd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastEmitterPath = System.IO.Path.GetDirectoryName(ofd.FileName);
				m_WidgetNewEmitRuleName.String = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);

                string path = m_WidgetLoadEmitRule.FindPath();
                EffectProperties.sm_Instance.RemoveBindings();
                EmitterProperties.sm_Instance.RemoveBindings();
                PtxRuleProperties.sm_Instance.RemoveBindings();
                RemoveBindings();
                WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
                if (button != null)
                    button.Activate();
                ViewPlugIn.Instance.RebuildAllPanels();

				return m_WidgetNewEmitRuleName.String;
			}
			return "";
		}
		public string LoadPtxRule()
		{
			return LoadPtxRule("Select a ptxrule");
		}

		public string LoadPtxRule(string title)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			if(UserData.LastPtxRulePath.Length>0)
				ofd.InitialDirectory = UserData.LastPtxRulePath;
			ofd.Filter="PtxRule (*.ptxrule)|*.ptxrule|Resourced PtxRule (*.*)|*.*";
			ofd.CheckFileExists=true;
			ofd.Title=title;
			if (ofd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastPtxRulePath = System.IO.Path.GetDirectoryName(ofd.FileName);
				m_WidgetNewPtxRuleName.String = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);

                string path = m_WidgetLoadPtxRule.FindPath();
                EffectProperties.sm_Instance.RemoveBindings();
                EmitterProperties.sm_Instance.RemoveBindings();
                PtxRuleProperties.sm_Instance.RemoveBindings();
                RemoveBindings();
                WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
                if (button != null)
                    button.Activate();
                ViewPlugIn.Instance.RebuildAllPanels();

				return m_WidgetNewPtxRuleName.String;
			}
			return "";
		}
		private void m_MenuItemAddEmitter_Activate(object sender, System.EventArgs e)
		{
			/*
			OpenFileDialog ofd = new OpenFileDialog();
			if(UserData.LastEffectPath.Length>0)
				ofd.InitialDirectory = UserData.LastEffectPath;

			if (ofd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastEffectPath = System.IO.Path.GetDirectoryName(ofd.FileName);
				m_WidgetNewRuleName.String = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);
				
				m_WidgetNewRule.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
			}
			*/
		}


		private void StartSelectedWidget(object sender, System.EventArgs e)
		{
			WidgetGroup selectedEffect = m_EffectsList.SelectedItem as WidgetGroup;
			if(selectedEffect !=null)
			{
				WidgetButton startButton = selectedEffect.FindFirstWidgetFromPath("Start") as WidgetButton;
				if(startButton != null)
					startButton.Activate();
			}
		}

		private void m_MenuItemNew_Activate(object sender, System.EventArgs e)
		{
			/*
			TextPromptDialog tpd = new TextPromptDialog();
			tpd.Text = "New effect name";
			tpd.PromptText = "Name of the new effect";
			tpd.Value = "";
			if (tpd.ShowDialog() == DialogResult.OK)
			{
				m_WidgetNewRuleName.String = tpd.Value;
				m_WidgetNewRule.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
			}
			*/
		}

		private void UpdateProfiling(object sender, System.EventArgs e)
		{
			m_LabelTotalPoints.Text = "Total Points: "+m_WidgetPointsActive.Value.ToString("0");
			m_LabelUpdateTime.Text = "CPU Update Time (ms): "+m_WidgetUpdateTime.Value.ToString("0.000");
			m_LabelDrawTime.Text = "CPU Draw Time (ms): "+m_WidgetDrawTime.Value.ToString("0.000");
		}

		private void m_EffectsList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(m_EffectsList.SelectedItem== null)
				return;
			if (EffectProperties.sm_Instance != null) 
				EffectProperties.sm_Instance.SelectEffect(m_EffectsList.SelectedItem.ToString());
			if(PlayControl.sm_Instance != null)
				PlayControl.sm_Instance.SelectEffect(m_EffectsList.SelectedItem.ToString());
		}

		public class StateData
		{
			public string m_CurrSelection;
		}

		public override object GetStateData()
		{
			StateData ret = new StateData();
			/*if (m_EffectsList.SelectedItem != null)
			{
				ret.m_CurrSelection = m_EffectsList.SelectedItem.ToString();
			}*/
			return ret;
		}

		public override void SetStateData(object d)
		{
			StateData data = d as StateData;
			m_WidgetRoot=null;
			FindWidgetHandles();
			BuildLists(true);
			/*for(int i = 0; i < m_EffectsList.Items.Count; i++) {
				if (m_EffectsList.Items[i].ToString() == data.m_CurrSelection) {
					m_EffectsList.SelectedIndex = i;
					break;
				}
			}*/
		}
		
		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			/*
			TextPromptDialog tpd = new TextPromptDialog();
			tpd.Text = "New effect name";
			tpd.PromptText = "Name of the new effect";
			tpd.Value = "";
			if (tpd.ShowDialog() == DialogResult.OK)
			{
				m_WidgetNewRuleName.String = tpd.Value;
				m_WidgetNewRule.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
			}
			*/
		}

		private void m_ContextOpen_Click(object sender, System.EventArgs e)
		{
			/*
			OpenFileDialog ofd = new OpenFileDialog();
			if(UserData.LastEffectPath.Length>0)
				ofd.InitialDirectory = UserData.LastEffectPath;

			if (ofd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastEffectPath = System.IO.Path.GetDirectoryName(ofd.FileName);
				m_WidgetNewRuleName.String = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);
				
				m_WidgetNewRule.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
			}
			*/

		}

		private void m_EmittersList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(m_EmittersList.SelectedItem== null)
				return;
			if (EmitterProperties.sm_Instance != null) 
				EmitterProperties.sm_Instance.SelectEmitter(m_EmittersList.SelectedItem.ToString());
		}

		private void m_PtxRulesList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(m_PtxRulesList.SelectedItem== null)
				return;
			if (PtxRuleProperties.sm_Instance != null) 
				PtxRuleProperties.sm_Instance.SelectPtxRule(m_PtxRulesList.SelectedItem.ToString());
		}

		private void m_ButtonLoadPtxRule_Activate(object sender, System.EventArgs e)
		{
			LoadPtxRule();
		}

		private void m_ButtonLoadEmitter_Activate(object sender, System.EventArgs e)
		{
			LoadEmitRule();
		}

		private void m_ButtonLoadEffect_Activate(object sender, System.EventArgs e)
		{
			LoadEffectRule();
		}

		private void m_ButtonNewEffect_Activate(object sender, System.EventArgs e)
		{
			NewEffectRule("New Effect Rule Name");
		}

		private void m_ButtonNewEmitter_Activate(object sender, System.EventArgs e)
		{
			NewEmitRule("New Emit Rule Name");
		}

		private void m_ButtonNewPtxSprite_Activate(object sender, System.EventArgs e)
		{
			NewPtxSpriteRule();
		}

		private void m_ButtonNewPtxModel_Activate(object sender, System.EventArgs e)
		{
			NewPtxModelRule();
		}

		private void m_MenuEffectSaveAs_Click(object sender, System.EventArgs e)
		{
			EffectProperties.sm_Instance.SelectEffect(m_EffectsList.SelectedItem.ToString());
			EffectProperties.sm_Instance.SaveEffectAs();

		}

		private void m_MenuEffect_Popup(object sender, System.EventArgs e)
		{
            if (m_EffectsList.SelectedIndex != -1)
            {
                m_MenuEffectSaveAs.Enabled = true;
                m_MenuEffectSaveAs.Text = "Save: " + m_EffectsList.Items[m_EffectsList.SelectedIndex].ToString() + " As...";
                m_RemoveEffectButton.Enabled = true;
                m_RemoveEffectButton.Text = "Remove " + m_EffectsList.SelectedItem.ToString();
            }
            else
            {
                m_MenuEffectSaveAs.Enabled = false;
                m_MenuEffectSaveAs.Text = "Save as...";
                m_RemoveEffectButton.Enabled = false;
                m_RemoveEffectButton.Text = "Remove effect";
            }
		}

		private void m_MenuSaveEmitterAs_Click(object sender, System.EventArgs e)
		{
			EmitterProperties.sm_Instance.SelectEmitter(m_EmittersList.SelectedItem.ToString());
			EmitterProperties.sm_Instance.SaveEmitterAs();
		
		}

		private void m_MenuEmitter_Popup(object sender, System.EventArgs e)
		{
			if(m_EmittersList.SelectedIndex!=-1)
			{
				m_MenuEmitterSaveAs.Visible=true;
				m_MenuEmitterSaveAs.Text = "Save: "+m_EmittersList.Items[m_EmittersList.SelectedIndex].ToString()+" As...";
			}
			else
				m_MenuEmitterSaveAs.Visible=false;

		}

		private void m_MenuPtxRule_Popup(object sender, System.EventArgs e)
		{
			if(m_PtxRulesList.SelectedIndex!=-1)
			{
				m_MenuPtxRuleSaveAs.Visible=true;
				m_MenuPtxRuleSaveAs.Text = "Save: "+m_PtxRulesList.Items[m_PtxRulesList.SelectedIndex].ToString()+" As...";
			}
			else
				m_MenuPtxRuleSaveAs.Visible=false;
		
		}

		private void m_MenuPtxRuleSaveAs_Click(object sender, System.EventArgs e)
		{
			PtxRuleProperties.sm_Instance.SelectPtxRule(m_PtxRulesList.SelectedItem.ToString());
			PtxRuleProperties.sm_Instance.SavePtxRuleAs();
		}

		private void m_MenuConvertOld_Activate(object sender, System.EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			if(UserData.LastEmitterPath.Length>0)
				ofd.InitialDirectory = UserData.LastEmitterPath;
			ofd.Filter="PtxEmitter (*.emitrule)|*.emitrule";
			ofd.CheckFileExists=true;
			ofd.Title= "Select an old emitter to convert and Load:";
			if (ofd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastEmitterPath = System.IO.Path.GetDirectoryName(ofd.FileName);
				m_WidgetOldEmitRuleName.String = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);
				m_WidgetOldEmitRule.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
			}
		}

		private void m_MenuConvertPtxRule_Activate(object sender, System.EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			if(UserData.LastPtxRulePath.Length>0)
				ofd.InitialDirectory = UserData.LastPtxRulePath;
			ofd.Filter="PtxRule (*.ptxrule)|*.ptxrule";
			ofd.CheckFileExists=true;
			ofd.Title= "Select an old ptxRule to convert and Load:";
			if (ofd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastPtxRulePath= System.IO.Path.GetDirectoryName(ofd.FileName);
				m_WidgetOldPtxRuleName.String = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);
				m_WidgetOldPtxRule.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
			}
	
		}

		private void m_MenuBatchConvert_Activate(object sender, System.EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			if(UserData.LastEmitterPath.Length>0)
				ofd.InitialDirectory = UserData.LastEmitterPath;
			ofd.Filter="PtxEmitter (*.emitrule)|*.emitrule";
			ofd.CheckFileExists=true;
			ofd.Multiselect = true;
			ofd.Title= "Select a group of old emitters to convert:";
			if (ofd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastEmitterPath = System.IO.Path.GetDirectoryName(ofd.FileName);
				
				foreach(string s in ofd.FileNames) 
				{
					m_WidgetOldEmitRuleName.String = System.IO.Path.GetFileNameWithoutExtension(s);
					m_WidgetOldEmitRuleName.UpdateRemote();
					m_WidgetBatchOldEmitRule.Activate();
				}
				ViewPlugIn.Instance.RebuildAllPanels();
			}
		}

		private void m_MenuSaveAll_Activate(object sender, System.EventArgs e)
		{
			m_WidgetSaveAll.Activate();
        }

        private void m_ButtonRemoveEffect_Activate(object sender, EventArgs e)
        {
            // get text of selected item
            string effectName = m_EffectsList.SelectedItem.ToString();
            m_WidgetNewEffectRuleName.String = effectName;
            m_WidgetRemoveEffectRule.Activate();
            SelectEffect("");
            ViewPlugIn.Instance.RebuildAllPanels();
        }

        private void m_EffectsList_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point pt = new Point(e.X, e.Y);
                int idx = m_EffectsList.IndexFromPoint(pt);
                m_EffectsList.SelectedIndex = idx;
            }
        }
	}
}
