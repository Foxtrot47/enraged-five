using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using ragCore;
using ragWidgets;


namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for EmitDomainProperties.
	/// </summary>
	public class ptxDomainProperties : System.Windows.Forms.UserControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ptxDomainProperties()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_CheckBoxShowDomain = new System.Windows.Forms.CheckBox();
			this.m_ListDomainKeyframes = new System.Windows.Forms.ListBox();
			this.m_CheckWorldSpace = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.m_ComboShape = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.m_CheckPointRel = new System.Windows.Forms.CheckBox();
			this.MenuDomainIO = new System.Windows.Forms.ContextMenu();
			this.MenuSaveDomain = new System.Windows.Forms.MenuItem();
			this.MenuLoadDomain = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// m_CheckBoxShowDomain
			// 
			this.m_CheckBoxShowDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_CheckBoxShowDomain.Location = new System.Drawing.Point(16, 8);
			this.m_CheckBoxShowDomain.Name = "m_CheckBoxShowDomain";
			this.m_CheckBoxShowDomain.Size = new System.Drawing.Size(96, 24);
			this.m_CheckBoxShowDomain.TabIndex = 1;
			this.m_CheckBoxShowDomain.Text = "Show Domain";
			// 
			// m_ListDomainKeyframes
			// 
			this.m_ListDomainKeyframes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.m_ListDomainKeyframes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_ListDomainKeyframes.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(100)), ((System.Byte)(0)));
			this.m_ListDomainKeyframes.Location = new System.Drawing.Point(8, 86);
			this.m_ListDomainKeyframes.Name = "m_ListDomainKeyframes";
			this.m_ListDomainKeyframes.Size = new System.Drawing.Size(328, 147);
			this.m_ListDomainKeyframes.TabIndex = 2;
			this.m_ListDomainKeyframes.DoubleClick += new System.EventHandler(this.m_ListDomainKeyframes_DoubleClick);
			// 
			// m_CheckWorldSpace
			// 
			this.m_CheckWorldSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_CheckWorldSpace.Location = new System.Drawing.Point(16, 34);
			this.m_CheckWorldSpace.Name = "m_CheckWorldSpace";
			this.m_CheckWorldSpace.TabIndex = 3;
			this.m_CheckWorldSpace.Text = "World Space";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(7, 68);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Properties:";
			// 
			// m_ComboShape
			// 
			this.m_ComboShape.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_ComboShape.Location = new System.Drawing.Point(215, 10);
			this.m_ComboShape.Name = "m_ComboShape";
			this.m_ComboShape.Size = new System.Drawing.Size(121, 21);
			this.m_ComboShape.TabIndex = 6;
			this.m_ComboShape.SelectedIndexChanged += new System.EventHandler(this.m_ComboShape_SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(129, 12);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(93, 23);
			this.label2.TabIndex = 7;
			this.label2.Text = "Domain Shape";
			// 
			// m_CheckPointRel
			// 
			this.m_CheckPointRel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_CheckPointRel.Location = new System.Drawing.Point(129, 30);
			this.m_CheckPointRel.Name = "m_CheckPointRel";
			this.m_CheckPointRel.Size = new System.Drawing.Size(125, 32);
			this.m_CheckPointRel.TabIndex = 12;
			this.m_CheckPointRel.Text = "Point Relative";
			// 
			// MenuDomainIO
			// 
			this.MenuDomainIO.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.MenuSaveDomain,
																						 this.MenuLoadDomain});
			// 
			// MenuSaveDomain
			// 
			this.MenuSaveDomain.Index = 0;
			this.MenuSaveDomain.Text = "Save Domain";
			this.MenuSaveDomain.Click += new System.EventHandler(this.MenuSaveDomain_Click);
			// 
			// MenuLoadDomain
			// 
			this.MenuLoadDomain.Index = 1;
			this.MenuLoadDomain.Text = "Load Domain";
			this.MenuLoadDomain.Click += new System.EventHandler(this.MenuLoadDomain_Click);
			// 
			// ptxDomainProperties
			// 
			this.ContextMenu = this.MenuDomainIO;
			this.Controls.Add(this.m_ComboShape);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.m_CheckWorldSpace);
			this.Controls.Add(this.m_ListDomainKeyframes);
			this.Controls.Add(this.m_CheckBoxShowDomain);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.m_CheckPointRel);
			this.Name = "ptxDomainProperties";
			this.Size = new System.Drawing.Size(344, 248);
			this.ResumeLayout(false);

		}
		#endregion

		public System.Windows.Forms.CheckBox m_CheckBoxShowDomain;
		public System.Windows.Forms.ListBox m_ListDomainKeyframes;
		public System.Windows.Forms.CheckBox m_CheckWorldSpace;
		public System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		public System.Windows.Forms.ComboBox m_ComboShape;

		WidgetGroup					m_WidgetRoot=null;
		ragWidgets.WidgetBinding	m_BindShowDomain=null;
		ragWidgets.WidgetBinding	m_BindWorldSpace=null;
		ragWidgets.WidgetBinding	m_BindPointRel=null;
		ragWidgets.WidgetBinding	m_BindComboShape=null;
		WidgetButton				m_WidgetSetDomain=null;
		WidgetSliderInt				m_WidgetDomainType=null;
		WidgetText					m_WidgetIOName=null;
		WidgetButton				m_WidgetSaveDomain=null;
		WidgetButton				m_WidgetLoadDomain=null;


		private System.Windows.Forms.CheckBox m_CheckPointRel;
		private System.Windows.Forms.ContextMenu MenuDomainIO;
		private System.Windows.Forms.MenuItem MenuSaveDomain;
		private System.Windows.Forms.MenuItem MenuLoadDomain;
		string						m_EmitterName="";
		

		public void BindToData(WidgetGroup rootPath,string emitterName)
		{
			m_EmitterName=emitterName;
			if(rootPath==null)
			{
				this.Enabled=false;
				return;
			}
			else
				this.Enabled = true;

			SetWidgetRoot(rootPath);
			FindWidgetHandles();
		}
		protected void SetWidgetRoot(WidgetGroup newRoot)
		{
			m_WidgetRoot = newRoot;
		}

		public void LoadDomain()
		{
			LoadDomain("Select a domain file");
		}
		public void LoadDomain(string title)
		{
			if(m_WidgetIOName == null || m_WidgetLoadDomain == null)
				return;
			OpenFileDialog ofd = new OpenFileDialog();
			if(UserData.LastDomainPath.Length>0)
				ofd.InitialDirectory = UserData.LastDomainPath;
			ofd.Filter="Domain (*.domain)|*.domain";
			ofd.CheckFileExists=true;
			ofd.Title=title;
			if (ofd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastDomainPath = System.IO.Path.GetDirectoryName(ofd.FileName);
				m_WidgetIOName.String = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);
				m_WidgetLoadDomain.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
			}
		}
		
		public void SaveDomain()
		{
			SaveDomain("Enter domain file name");
		}

		public void SaveDomain(string title)
		{
			if(m_WidgetIOName == null || m_WidgetSaveDomain == null)
				return;
			SaveFileDialog sfd = new SaveFileDialog();
			if(UserData.LastDomainPath.Length>0)
				sfd.InitialDirectory = UserData.LastDomainPath;
			sfd.AddExtension=true;
			sfd.Filter="Domain (*.domain)|*.domain";
			sfd.ValidateNames=true;
			sfd.OverwritePrompt=true;
			sfd.Title=title;
			if (sfd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastDomainPath = System.IO.Path.GetDirectoryName(sfd.FileName);
				m_WidgetIOName.String = System.IO.Path.GetFileNameWithoutExtension(sfd.FileName);
				m_WidgetSaveDomain.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
			}
		}

		protected void RemoveBindings()
		{
			if(m_BindShowDomain != null) 
				m_BindShowDomain.DisconnectEvents();
			if(m_BindWorldSpace != null) 
				m_BindWorldSpace.DisconnectEvents();
			if(m_BindComboShape != null)
				m_BindComboShape.DisconnectEvents();
			if(m_BindPointRel != null)
				m_BindPointRel.DisconnectEvents();
		}

		protected void FindWidgetHandles()
		{
			RemoveBindings();
			if(m_WidgetRoot==null)
				return;
			
			m_WidgetDomainType = m_WidgetRoot.FindFirstWidgetFromPath("TuneType") as WidgetSliderInt;
			m_WidgetSetDomain = m_WidgetRoot.FindFirstWidgetFromPath("SetDomain") as WidgetButton;

			//Types
			m_ComboShape.SelectedIndexChanged -= new System.EventHandler(this.m_ComboShape_SelectedIndexChanged);
			m_ComboShape.BeginUpdate();
			m_ComboShape.Items.Clear();
			WidgetGroup types = m_WidgetRoot.FindFirstWidgetFromPath("Types") as WidgetGroup;
			if (types != null)
			{
				foreach(Widget k in types.List)
				{
					m_ComboShape.Items.Add(k);
				}
			}
			m_ComboShape.EndUpdate();
			//m_BindComboShape=ragWidgets.WidgetBinding.Bind(m_WidgetRoot.FindFirstWidgetFromPath("TuneType"),m_ComboShape,"SelectedIndex");	
			m_ComboShape.SelectedIndex=(int)m_WidgetDomainType.Value;
			m_ComboShape.SelectedIndexChanged += new System.EventHandler(this.m_ComboShape_SelectedIndexChanged);
			
			//IOname
			m_WidgetIOName = m_WidgetRoot.FindFirstWidgetFromPath("IOName") as WidgetText;

			//Save domain
			m_WidgetSaveDomain = m_WidgetRoot.FindFirstWidgetFromPath("Save Domain") as WidgetButton;
			//Load Domain
			m_WidgetLoadDomain = m_WidgetRoot.FindFirstWidgetFromPath("Load Domain") as WidgetButton;


			//Show Domain
			Widget widget = m_WidgetRoot.FindFirstWidgetFromPath("ShowDomain");
			if(widget!= null)
			{
				m_CheckBoxShowDomain.Visible=true;
				m_CheckBoxShowDomain.Enabled=true;
				m_BindShowDomain= ragWidgets.WidgetBinding.Bind(widget,m_CheckBoxShowDomain,"Checked");
			}
			else
			{
				m_CheckBoxShowDomain.Enabled=false;
				m_CheckBoxShowDomain.Visible=false;
			}

			//WorldSpace
			widget = m_WidgetRoot.FindFirstWidgetFromPath("WorldSpace");
			if(widget!= null)
			{
				m_CheckWorldSpace.Visible=true;
				m_CheckWorldSpace.Enabled=true;
				m_BindWorldSpace= ragWidgets.WidgetBinding.Bind(widget,m_CheckWorldSpace,"Checked");
			}
			else
			{
				m_CheckWorldSpace.Enabled=false;
				m_CheckWorldSpace.Visible=false;
			}

			//PointRelative
			widget = m_WidgetRoot.FindFirstWidgetFromPath("PointRelative");
			if(widget!= null)
			{
				m_CheckPointRel.Visible=true;
				m_CheckPointRel.Enabled=true;
				m_BindPointRel= ragWidgets.WidgetBinding.Bind(widget,m_CheckPointRel,"Checked");
			}
			else
			{
				m_CheckPointRel.Enabled=false;
				m_CheckPointRel.Visible=false;
			}
			

			//Properties
			m_ListDomainKeyframes.BeginUpdate();
			m_ListDomainKeyframes.Items.Clear();
			WidgetGroup keys = m_WidgetRoot.FindFirstWidgetFromPath("DomainKeyframes") as WidgetGroup;
			if (keys != null)
			{
				foreach(Widget k in keys.List)
				{
					m_ListDomainKeyframes.Items.Add(k);
				}
			}
			m_ListDomainKeyframes.EndUpdate();


		}

		private void m_ListDomainKeyframes_DoubleClick(object sender, System.EventArgs e)
		{
			if (m_ListDomainKeyframes.SelectedItem is WidgetGroup)
			{
                ViewPlugIn.Instance.CreateAndOpenKeyframeEditorView( m_ListDomainKeyframes.SelectedItem as WidgetGroup, m_WidgetRoot.ToString() + ": " + m_EmitterName + "/" );
			}
		}

		private void m_ComboShape_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			m_WidgetDomainType.Value=m_ComboShape.SelectedIndex;
			m_WidgetSetDomain.Activate();
			ViewPlugIn.Instance.RebuildAllPanels();
		}

		private void MenuSaveDomain_Click(object sender, System.EventArgs e)
		{
			SaveDomain();
		}

		private void MenuLoadDomain_Click(object sender, System.EventArgs e)
		{
			LoadDomain();
		}
	}
}
