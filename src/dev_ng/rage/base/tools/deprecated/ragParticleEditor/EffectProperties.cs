using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using ragCore;
using ragWidgets;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for EffectProperties.
	/// </summary>
	public class EffectProperties : ragWidgetPage.WidgetPage
	{
		private System.ComponentModel.IContainer components;

		public EffectProperties(String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl) : base(appName, bankMgr, dockControl)
		{
			InitializeComponent();
			InitTimeLine();
		}

		public EffectProperties()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			InitTimeLine();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.m_SliderFZoom = new ragUi.ControlSlider();
            this.m_CheckboxShowInstances = new System.Windows.Forms.CheckBox();
            this.m_CheckboxShowCullspheres = new System.Windows.Forms.CheckBox();
            this.m_ButtonBakeZoom = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_LabelDrawTime = new System.Windows.Forms.Label();
            this.m_LabelUpdateTime = new System.Windows.Forms.Label();
            this.m_LabelCulledInstances = new System.Windows.Forms.Label();
            this.m_LabelActiveInstances = new System.Windows.Forms.Label();
            this.m_LabelTotalPoints = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.m_SliderPreUpdate = new ragUi.ControlSlider();
            this.m_SliderNumLoops = new ragUi.ControlSlider();
            this.m_SliderRateMax = new ragUi.ControlSlider();
            this.m_SliderRateMin = new ragUi.ControlSlider();
            this.m_SliderDurationMax = new ragUi.ControlSlider();
            this.m_SliderDurationMin = new ragUi.ControlSlider();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.m_SliderOffsetX = new ragUi.ControlSlider();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.m_CheckCullNoDraw = new System.Windows.Forms.CheckBox();
            this.m_CheckCullNoUpdate = new System.Windows.Forms.CheckBox();
            this.m_CheckCullNoEmit = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.m_SliderOffsetZ = new ragUi.ControlSlider();
            this.m_SliderOffsetY = new ragUi.ControlSlider();
            this.m_SliderRadius = new ragUi.ControlSlider();
            this.m_CheckboxUseCullsphere = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.m_SliderDistance = new ragUi.ControlSlider();
            this.m_ZoomCullDistCheckbox = new System.Windows.Forms.CheckBox();
            this.TimeLine1 = new ragPtxTimeLine.ptxTimeLine();
            this.m_GroupEventData = new System.Windows.Forms.GroupBox();
            this.m_EventPanel = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer( this.components );
            this.m_Title = new System.Windows.Forms.Label();
            this.m_KeyframeList = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timeLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewEmitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewActionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addEmitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addEffectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addActionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_EvolutionList = new System.Windows.Forms.ListBox();
            this.m_ContextMenuEvo = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.m_MenuAddEvo = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuDelEvo = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_EvoProps = new ragParticleEditor.EvoProperties();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.m_GroupEventData.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.m_ContextMenuEvo.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add( this.m_SliderFZoom );
            this.groupBox2.Controls.Add( this.m_CheckboxShowInstances );
            this.groupBox2.Controls.Add( this.m_CheckboxShowCullspheres );
            this.groupBox2.Controls.Add( this.m_ButtonBakeZoom );
            this.groupBox2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.groupBox2.Location = new System.Drawing.Point( 243, 25 );
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size( 218, 143 );
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Debugging";
            // 
            // m_SliderFZoom
            // 
            this.m_SliderFZoom.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderFZoom.IsFloat = true;
            this.m_SliderFZoom.Location = new System.Drawing.Point( 8, 72 );
            this.m_SliderFZoom.Maximum = 500F;
            this.m_SliderFZoom.Minimum = 0F;
            this.m_SliderFZoom.Name = "m_SliderFZoom";
            this.m_SliderFZoom.Size = new System.Drawing.Size( 192, 24 );
            this.m_SliderFZoom.Step = 1F;
            this.m_SliderFZoom.TabIndex = 20;
            this.m_SliderFZoom.Text = "Scale";
            this.m_SliderFZoom.UseTrackbar = true;
            this.m_SliderFZoom.Value = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.m_SliderFZoom.ValueXPath = null;
            // 
            // m_CheckboxShowInstances
            // 
            this.m_CheckboxShowInstances.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_CheckboxShowInstances.Location = new System.Drawing.Point( 8, 16 );
            this.m_CheckboxShowInstances.Name = "m_CheckboxShowInstances";
            this.m_CheckboxShowInstances.Size = new System.Drawing.Size( 104, 24 );
            this.m_CheckboxShowInstances.TabIndex = 0;
            this.m_CheckboxShowInstances.Text = "Show Instances";
            // 
            // m_CheckboxShowCullspheres
            // 
            this.m_CheckboxShowCullspheres.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_CheckboxShowCullspheres.Location = new System.Drawing.Point( 8, 40 );
            this.m_CheckboxShowCullspheres.Name = "m_CheckboxShowCullspheres";
            this.m_CheckboxShowCullspheres.Size = new System.Drawing.Size( 136, 24 );
            this.m_CheckboxShowCullspheres.TabIndex = 1;
            this.m_CheckboxShowCullspheres.Text = "Show CullSpheres";
            // 
            // m_ButtonBakeZoom
            // 
            this.m_ButtonBakeZoom.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_ButtonBakeZoom.Location = new System.Drawing.Point( 8, 104 );
            this.m_ButtonBakeZoom.Name = "m_ButtonBakeZoom";
            this.m_ButtonBakeZoom.Size = new System.Drawing.Size( 112, 24 );
            this.m_ButtonBakeZoom.TabIndex = 25;
            this.m_ButtonBakeZoom.Text = "Freeze Scale";
            this.m_ButtonBakeZoom.Click += new System.EventHandler( this.m_ButtonBakeZoom_Click );
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add( this.m_LabelDrawTime );
            this.groupBox1.Controls.Add( this.m_LabelUpdateTime );
            this.groupBox1.Controls.Add( this.m_LabelCulledInstances );
            this.groupBox1.Controls.Add( this.m_LabelActiveInstances );
            this.groupBox1.Controls.Add( this.m_LabelTotalPoints );
            this.groupBox1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.groupBox1.Location = new System.Drawing.Point( 7, 25 );
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size( 222, 143 );
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Profiling";
            // 
            // m_LabelDrawTime
            // 
            this.m_LabelDrawTime.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_LabelDrawTime.Location = new System.Drawing.Point( 8, 88 );
            this.m_LabelDrawTime.Name = "m_LabelDrawTime";
            this.m_LabelDrawTime.Size = new System.Drawing.Size( 144, 16 );
            this.m_LabelDrawTime.TabIndex = 4;
            this.m_LabelDrawTime.Text = "DrawTime (cpu)";
            // 
            // m_LabelUpdateTime
            // 
            this.m_LabelUpdateTime.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_LabelUpdateTime.Location = new System.Drawing.Point( 8, 69 );
            this.m_LabelUpdateTime.Name = "m_LabelUpdateTime";
            this.m_LabelUpdateTime.Size = new System.Drawing.Size( 160, 16 );
            this.m_LabelUpdateTime.TabIndex = 3;
            this.m_LabelUpdateTime.Text = "Update Time (cpu)";
            // 
            // m_LabelCulledInstances
            // 
            this.m_LabelCulledInstances.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_LabelCulledInstances.Location = new System.Drawing.Point( 8, 50 );
            this.m_LabelCulledInstances.Name = "m_LabelCulledInstances";
            this.m_LabelCulledInstances.Size = new System.Drawing.Size( 144, 16 );
            this.m_LabelCulledInstances.TabIndex = 2;
            this.m_LabelCulledInstances.Text = "Culled Instances";
            // 
            // m_LabelActiveInstances
            // 
            this.m_LabelActiveInstances.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_LabelActiveInstances.Location = new System.Drawing.Point( 8, 34 );
            this.m_LabelActiveInstances.Name = "m_LabelActiveInstances";
            this.m_LabelActiveInstances.Size = new System.Drawing.Size( 144, 16 );
            this.m_LabelActiveInstances.TabIndex = 1;
            this.m_LabelActiveInstances.Text = "Active Instances";
            // 
            // m_LabelTotalPoints
            // 
            this.m_LabelTotalPoints.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_LabelTotalPoints.Location = new System.Drawing.Point( 8, 18 );
            this.m_LabelTotalPoints.Name = "m_LabelTotalPoints";
            this.m_LabelTotalPoints.Size = new System.Drawing.Size( 136, 16 );
            this.m_LabelTotalPoints.TabIndex = 0;
            this.m_LabelTotalPoints.Text = "Total Points: ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add( this.m_SliderPreUpdate );
            this.groupBox3.Controls.Add( this.m_SliderNumLoops );
            this.groupBox3.Controls.Add( this.m_SliderRateMax );
            this.groupBox3.Controls.Add( this.m_SliderRateMin );
            this.groupBox3.Controls.Add( this.m_SliderDurationMax );
            this.groupBox3.Controls.Add( this.m_SliderDurationMin );
            this.groupBox3.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.groupBox3.Location = new System.Drawing.Point( 8, 399 );
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size( 454, 128 );
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Info";
            // 
            // m_SliderPreUpdate
            // 
            this.m_SliderPreUpdate.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderPreUpdate.IsFloat = true;
            this.m_SliderPreUpdate.Location = new System.Drawing.Point( 240, 85 );
            this.m_SliderPreUpdate.Maximum = 100F;
            this.m_SliderPreUpdate.Minimum = 0F;
            this.m_SliderPreUpdate.Name = "m_SliderPreUpdate";
            this.m_SliderPreUpdate.Size = new System.Drawing.Size( 208, 24 );
            this.m_SliderPreUpdate.Step = 0.01F;
            this.m_SliderPreUpdate.TabIndex = 16;
            this.m_SliderPreUpdate.Text = "Start Time";
            this.m_SliderPreUpdate.UseTrackbar = false;
            this.m_SliderPreUpdate.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderPreUpdate.ValueXPath = null;
            // 
            // m_SliderNumLoops
            // 
            this.m_SliderNumLoops.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderNumLoops.IsFloat = false;
            this.m_SliderNumLoops.Location = new System.Drawing.Point( 8, 85 );
            this.m_SliderNumLoops.Maximum = 100F;
            this.m_SliderNumLoops.Minimum = -1F;
            this.m_SliderNumLoops.Name = "m_SliderNumLoops";
            this.m_SliderNumLoops.Size = new System.Drawing.Size( 208, 24 );
            this.m_SliderNumLoops.Step = 1F;
            this.m_SliderNumLoops.TabIndex = 14;
            this.m_SliderNumLoops.Text = "Num Loops";
            this.m_SliderNumLoops.UseTrackbar = false;
            this.m_SliderNumLoops.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderNumLoops.ValueXPath = null;
            // 
            // m_SliderRateMax
            // 
            this.m_SliderRateMax.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderRateMax.IsFloat = true;
            this.m_SliderRateMax.Location = new System.Drawing.Point( 240, 53 );
            this.m_SliderRateMax.Maximum = 100F;
            this.m_SliderRateMax.Minimum = 0F;
            this.m_SliderRateMax.Name = "m_SliderRateMax";
            this.m_SliderRateMax.Size = new System.Drawing.Size( 208, 24 );
            this.m_SliderRateMax.Step = 0.01F;
            this.m_SliderRateMax.TabIndex = 13;
            this.m_SliderRateMax.Text = "Playback Rate Max";
            this.m_SliderRateMax.UseTrackbar = false;
            this.m_SliderRateMax.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderRateMax.ValueXPath = null;
            // 
            // m_SliderRateMin
            // 
            this.m_SliderRateMin.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderRateMin.IsFloat = true;
            this.m_SliderRateMin.Location = new System.Drawing.Point( 8, 53 );
            this.m_SliderRateMin.Maximum = 100F;
            this.m_SliderRateMin.Minimum = 0F;
            this.m_SliderRateMin.Name = "m_SliderRateMin";
            this.m_SliderRateMin.Size = new System.Drawing.Size( 208, 24 );
            this.m_SliderRateMin.Step = 0.01F;
            this.m_SliderRateMin.TabIndex = 12;
            this.m_SliderRateMin.Text = "Playback Rate Min";
            this.m_SliderRateMin.UseTrackbar = false;
            this.m_SliderRateMin.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderRateMin.ValueXPath = null;
            // 
            // m_SliderDurationMax
            // 
            this.m_SliderDurationMax.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderDurationMax.IsFloat = true;
            this.m_SliderDurationMax.Location = new System.Drawing.Point( 240, 21 );
            this.m_SliderDurationMax.Maximum = 9000F;
            this.m_SliderDurationMax.Minimum = 0F;
            this.m_SliderDurationMax.Name = "m_SliderDurationMax";
            this.m_SliderDurationMax.Size = new System.Drawing.Size( 208, 24 );
            this.m_SliderDurationMax.Step = 0.01F;
            this.m_SliderDurationMax.TabIndex = 11;
            this.m_SliderDurationMax.Text = "Duration Max";
            this.m_SliderDurationMax.UseTrackbar = false;
            this.m_SliderDurationMax.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderDurationMax.ValueXPath = null;
            this.m_SliderDurationMax.ValueChanged += new System.EventHandler( this.m_SliderDurationMax_ValueChanged );
            // 
            // m_SliderDurationMin
            // 
            this.m_SliderDurationMin.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderDurationMin.IsFloat = true;
            this.m_SliderDurationMin.Location = new System.Drawing.Point( 8, 21 );
            this.m_SliderDurationMin.Maximum = 9000F;
            this.m_SliderDurationMin.Minimum = 0F;
            this.m_SliderDurationMin.Name = "m_SliderDurationMin";
            this.m_SliderDurationMin.Size = new System.Drawing.Size( 208, 24 );
            this.m_SliderDurationMin.Step = 0.01F;
            this.m_SliderDurationMin.TabIndex = 10;
            this.m_SliderDurationMin.Text = "Duration Min";
            this.m_SliderDurationMin.UseTrackbar = false;
            this.m_SliderDurationMin.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderDurationMin.ValueXPath = null;
            this.m_SliderDurationMin.ValueChanged += new System.EventHandler( this.m_SliderDuration_ValueChanged );
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add( this.m_SliderOffsetX );
            this.groupBox4.Controls.Add( this.groupBox5 );
            this.groupBox4.Controls.Add( this.label1 );
            this.groupBox4.Controls.Add( this.m_SliderOffsetZ );
            this.groupBox4.Controls.Add( this.m_SliderOffsetY );
            this.groupBox4.Controls.Add( this.m_SliderRadius );
            this.groupBox4.Controls.Add( this.m_CheckboxUseCullsphere );
            this.groupBox4.Controls.Add( this.groupBox6 );
            this.groupBox4.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.groupBox4.Location = new System.Drawing.Point( 7, 175 );
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size( 454, 217 );
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Cull Sphere";
            // 
            // m_SliderOffsetX
            // 
            this.m_SliderOffsetX.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderOffsetX.IsFloat = true;
            this.m_SliderOffsetX.Location = new System.Drawing.Point( 97, 80 );
            this.m_SliderOffsetX.Maximum = 1000F;
            this.m_SliderOffsetX.Minimum = -1000F;
            this.m_SliderOffsetX.Name = "m_SliderOffsetX";
            this.m_SliderOffsetX.Size = new System.Drawing.Size( 112, 24 );
            this.m_SliderOffsetX.Step = 0.01F;
            this.m_SliderOffsetX.TabIndex = 17;
            this.m_SliderOffsetX.Text = "X";
            this.m_SliderOffsetX.UseTrackbar = false;
            this.m_SliderOffsetX.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderOffsetX.ValueXPath = null;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add( this.m_CheckCullNoDraw );
            this.groupBox5.Controls.Add( this.m_CheckCullNoUpdate );
            this.groupBox5.Controls.Add( this.m_CheckCullNoEmit );
            this.groupBox5.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.groupBox5.Location = new System.Drawing.Point( 11, 120 );
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size( 141, 88 );
            this.groupBox5.TabIndex = 28;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "When Culled:";
            // 
            // m_CheckCullNoDraw
            // 
            this.m_CheckCullNoDraw.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_CheckCullNoDraw.Location = new System.Drawing.Point( 17, 59 );
            this.m_CheckCullNoDraw.Name = "m_CheckCullNoDraw";
            this.m_CheckCullNoDraw.Size = new System.Drawing.Size( 103, 16 );
            this.m_CheckCullNoDraw.TabIndex = 30;
            this.m_CheckCullNoDraw.Text = "Stop Drawing";
            // 
            // m_CheckCullNoUpdate
            // 
            this.m_CheckCullNoUpdate.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_CheckCullNoUpdate.Location = new System.Drawing.Point( 17, 41 );
            this.m_CheckCullNoUpdate.Name = "m_CheckCullNoUpdate";
            this.m_CheckCullNoUpdate.Size = new System.Drawing.Size( 103, 16 );
            this.m_CheckCullNoUpdate.TabIndex = 29;
            this.m_CheckCullNoUpdate.Text = "Stop Updating";
            // 
            // m_CheckCullNoEmit
            // 
            this.m_CheckCullNoEmit.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_CheckCullNoEmit.Location = new System.Drawing.Point( 17, 20 );
            this.m_CheckCullNoEmit.Name = "m_CheckCullNoEmit";
            this.m_CheckCullNoEmit.Size = new System.Drawing.Size( 103, 24 );
            this.m_CheckCullNoEmit.TabIndex = 28;
            this.m_CheckCullNoEmit.Text = "Stop Emitting";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.label1.Location = new System.Drawing.Point( 16, 85 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size( 80, 17 );
            this.label1.TabIndex = 22;
            this.label1.Text = "Sphere Offset:";
            // 
            // m_SliderOffsetZ
            // 
            this.m_SliderOffsetZ.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderOffsetZ.IsFloat = true;
            this.m_SliderOffsetZ.Location = new System.Drawing.Point( 329, 80 );
            this.m_SliderOffsetZ.Maximum = 1000F;
            this.m_SliderOffsetZ.Minimum = -1000F;
            this.m_SliderOffsetZ.Name = "m_SliderOffsetZ";
            this.m_SliderOffsetZ.Size = new System.Drawing.Size( 112, 24 );
            this.m_SliderOffsetZ.Step = 0.01F;
            this.m_SliderOffsetZ.TabIndex = 19;
            this.m_SliderOffsetZ.Text = "Z";
            this.m_SliderOffsetZ.UseTrackbar = false;
            this.m_SliderOffsetZ.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderOffsetZ.ValueXPath = null;
            // 
            // m_SliderOffsetY
            // 
            this.m_SliderOffsetY.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderOffsetY.IsFloat = true;
            this.m_SliderOffsetY.Location = new System.Drawing.Point( 213, 80 );
            this.m_SliderOffsetY.Maximum = 1000F;
            this.m_SliderOffsetY.Minimum = -1000F;
            this.m_SliderOffsetY.Name = "m_SliderOffsetY";
            this.m_SliderOffsetY.Size = new System.Drawing.Size( 112, 24 );
            this.m_SliderOffsetY.Step = 0.01F;
            this.m_SliderOffsetY.TabIndex = 18;
            this.m_SliderOffsetY.Text = "Y";
            this.m_SliderOffsetY.UseTrackbar = false;
            this.m_SliderOffsetY.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderOffsetY.ValueXPath = null;
            // 
            // m_SliderRadius
            // 
            this.m_SliderRadius.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderRadius.IsFloat = true;
            this.m_SliderRadius.Location = new System.Drawing.Point( 16, 48 );
            this.m_SliderRadius.Maximum = 100F;
            this.m_SliderRadius.Minimum = 0F;
            this.m_SliderRadius.Name = "m_SliderRadius";
            this.m_SliderRadius.Size = new System.Drawing.Size( 192, 24 );
            this.m_SliderRadius.Step = 0.01F;
            this.m_SliderRadius.TabIndex = 21;
            this.m_SliderRadius.Text = "Sphere Radius";
            this.m_SliderRadius.UseTrackbar = false;
            this.m_SliderRadius.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderRadius.ValueXPath = null;
            // 
            // m_CheckboxUseCullsphere
            // 
            this.m_CheckboxUseCullsphere.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_CheckboxUseCullsphere.Location = new System.Drawing.Point( 8, 20 );
            this.m_CheckboxUseCullsphere.Name = "m_CheckboxUseCullsphere";
            this.m_CheckboxUseCullsphere.Size = new System.Drawing.Size( 120, 24 );
            this.m_CheckboxUseCullsphere.TabIndex = 16;
            this.m_CheckboxUseCullsphere.Text = "Use Culling";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add( this.m_SliderDistance );
            this.groupBox6.Controls.Add( this.m_ZoomCullDistCheckbox );
            this.groupBox6.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.groupBox6.Location = new System.Drawing.Point( 168, 120 );
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size( 272, 88 );
            this.groupBox6.TabIndex = 29;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Distance Culling";
            // 
            // m_SliderDistance
            // 
            this.m_SliderDistance.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderDistance.IsFloat = true;
            this.m_SliderDistance.Location = new System.Drawing.Point( 16, 24 );
            this.m_SliderDistance.Maximum = 100000F;
            this.m_SliderDistance.Minimum = 0F;
            this.m_SliderDistance.Name = "m_SliderDistance";
            this.m_SliderDistance.Size = new System.Drawing.Size( 184, 24 );
            this.m_SliderDistance.Step = 0.01F;
            this.m_SliderDistance.TabIndex = 25;
            this.m_SliderDistance.Text = "Cull Distance";
            this.m_SliderDistance.UseTrackbar = true;
            this.m_SliderDistance.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderDistance.ValueXPath = null;
            // 
            // m_ZoomCullDistCheckbox
            // 
            this.m_ZoomCullDistCheckbox.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_ZoomCullDistCheckbox.Location = new System.Drawing.Point( 16, 48 );
            this.m_ZoomCullDistCheckbox.Name = "m_ZoomCullDistCheckbox";
            this.m_ZoomCullDistCheckbox.Size = new System.Drawing.Size( 184, 24 );
            this.m_ZoomCullDistCheckbox.TabIndex = 26;
            this.m_ZoomCullDistCheckbox.Text = "Zoom Affects Cull Distance?";
            // 
            // TimeLine1
            // 
            this.TimeLine1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.TimeLine1.ChanelLabelWidth = 150;
            this.TimeLine1.ChannelHeight = 20;
            this.TimeLine1.Duration = 4F;
            this.TimeLine1.Granularity = 3;
            this.TimeLine1.Location = new System.Drawing.Point( 8, 543 );
            this.TimeLine1.Name = "TimeLine1";
            this.TimeLine1.Right_Padding = 40;
            this.TimeLine1.Size = new System.Drawing.Size( 856, 208 );
            this.TimeLine1.TabIndex = 27;
            this.TimeLine1.OnDeleteChannel += new ragPtxTimeLine.ptxTimeLine.ChannelEventDelegate( this.TimeLine1_OnDeleteChannel );
            this.TimeLine1.OnChannelInfoChanged += new ragPtxTimeLine.ptxTimeLine.ChannelEventDelegate( this.TimeLine1_OnChannelInfoChanged );
            this.TimeLine1.OnReOrderChannels += new System.EventHandler( this.TimeLine1_OnReOrderChannels );
            this.TimeLine1.OnSelectChannel += new ragPtxTimeLine.ptxTimeLine.ChannelEventDelegate( this.TimeLine1_OnSelectChannel );
            // 
            // m_GroupEventData
            // 
            this.m_GroupEventData.Controls.Add( this.m_EventPanel );
            this.m_GroupEventData.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_GroupEventData.Location = new System.Drawing.Point( 468, 26 );
            this.m_GroupEventData.Name = "m_GroupEventData";
            this.m_GroupEventData.Size = new System.Drawing.Size( 420, 366 );
            this.m_GroupEventData.TabIndex = 28;
            this.m_GroupEventData.TabStop = false;
            this.m_GroupEventData.Text = "TimeLine Element";
            // 
            // m_EventPanel
            // 
            this.m_EventPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_EventPanel.Location = new System.Drawing.Point( 3, 16 );
            this.m_EventPanel.Name = "m_EventPanel";
            this.m_EventPanel.Size = new System.Drawing.Size( 414, 347 );
            this.m_EventPanel.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler( this.UpdateProfiling );
            // 
            // m_Title
            // 
            this.m_Title.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_Title.Location = new System.Drawing.Point( 8, 535 );
            this.m_Title.Name = "m_Title";
            this.m_Title.Size = new System.Drawing.Size( 100, 16 );
            this.m_Title.TabIndex = 29;
            this.m_Title.Text = "Event Timeline";
            // 
            // m_KeyframeList
            // 
            this.m_KeyframeList.Location = new System.Drawing.Point( 467, 431 );
            this.m_KeyframeList.Name = "m_KeyframeList";
            this.m_KeyframeList.Size = new System.Drawing.Size( 129, 95 );
            this.m_KeyframeList.TabIndex = 30;
            this.m_KeyframeList.DoubleClick += new System.EventHandler( this.m_KeyframeList_DoubleClick );
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem} );
            this.menuStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size( 1294, 24 );
            this.menuStrip1.TabIndex = 31;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.saveAllToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.timeLineToolStripMenuItem} );
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size( 35, 20 );
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler( this.Menu_Save_Activate );
            // 
            // saveAllToolStripMenuItem
            // 
            this.saveAllToolStripMenuItem.Name = "saveAllToolStripMenuItem";
            this.saveAllToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.saveAllToolStripMenuItem.Text = "Save Al&l";
            this.saveAllToolStripMenuItem.Click += new System.EventHandler( this.Menu_SaveAll_Activate );
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.saveAsToolStripMenuItem.Text = "Save &As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler( this.m_MenuSaveAs_Activate );
            // 
            // timeLineToolStripMenuItem
            // 
            this.timeLineToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.addNewToolStripMenuItem,
            this.addToolStripMenuItem} );
            this.timeLineToolStripMenuItem.Name = "timeLineToolStripMenuItem";
            this.timeLineToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.timeLineToolStripMenuItem.Text = "TimeLine";
            // 
            // addNewToolStripMenuItem
            // 
            this.addNewToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.addNewEmitterToolStripMenuItem,
            this.addNewEffectToolStripMenuItem,
            this.addNewActionToolStripMenuItem} );
            this.addNewToolStripMenuItem.Name = "addNewToolStripMenuItem";
            this.addNewToolStripMenuItem.Size = new System.Drawing.Size( 121, 22 );
            this.addNewToolStripMenuItem.Text = "Add &New";
            // 
            // addNewEmitterToolStripMenuItem
            // 
            this.addNewEmitterToolStripMenuItem.Name = "addNewEmitterToolStripMenuItem";
            this.addNewEmitterToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.addNewEmitterToolStripMenuItem.Text = "&Emitter";
            this.addNewEmitterToolStripMenuItem.Click += new System.EventHandler( this.TimeLine_Menu_NewEmitter_Click );
            // 
            // addNewEffectToolStripMenuItem
            // 
            this.addNewEffectToolStripMenuItem.Enabled = false;
            this.addNewEffectToolStripMenuItem.Name = "addNewEffectToolStripMenuItem";
            this.addNewEffectToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.addNewEffectToolStripMenuItem.Text = "E&ffect";
            this.addNewEffectToolStripMenuItem.Click += new System.EventHandler( this.TimeLine_Menu_NewEffect_Click );
            // 
            // addNewActionToolStripMenuItem
            // 
            this.addNewActionToolStripMenuItem.Enabled = false;
            this.addNewActionToolStripMenuItem.Name = "addNewActionToolStripMenuItem";
            this.addNewActionToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.addNewActionToolStripMenuItem.Text = "A&ction";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.addEmitterToolStripMenuItem,
            this.addEffectToolStripMenuItem,
            this.addActionToolStripMenuItem} );
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size( 121, 22 );
            this.addToolStripMenuItem.Text = "Ad&d";
            // 
            // addEmitterToolStripMenuItem
            // 
            this.addEmitterToolStripMenuItem.Name = "addEmitterToolStripMenuItem";
            this.addEmitterToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.addEmitterToolStripMenuItem.Text = "&Emitter";
            this.addEmitterToolStripMenuItem.Click += new System.EventHandler( this.TimeLine_Menu_AddEmitter_Click );
            // 
            // addEffectToolStripMenuItem
            // 
            this.addEffectToolStripMenuItem.Enabled = false;
            this.addEffectToolStripMenuItem.Name = "addEffectToolStripMenuItem";
            this.addEffectToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.addEffectToolStripMenuItem.Text = "E&ffect";
            this.addEffectToolStripMenuItem.Click += new System.EventHandler( this.TimeLine_Menu_AddEffect_Click );
            // 
            // addActionToolStripMenuItem
            // 
            this.addActionToolStripMenuItem.Enabled = false;
            this.addActionToolStripMenuItem.Name = "addActionToolStripMenuItem";
            this.addActionToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.addActionToolStripMenuItem.Text = "A&ction";
            this.addActionToolStripMenuItem.Click += new System.EventHandler( this.TimeLine_Menu_AddAction_Click );
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size( 37, 20 );
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // m_EvolutionList
            // 
            this.m_EvolutionList.ContextMenuStrip = this.m_ContextMenuEvo;
            this.m_EvolutionList.Location = new System.Drawing.Point( 602, 431 );
            this.m_EvolutionList.Name = "m_EvolutionList";
            this.m_EvolutionList.Size = new System.Drawing.Size( 286, 95 );
            this.m_EvolutionList.TabIndex = 32;
            this.m_EvolutionList.SelectedIndexChanged += new System.EventHandler( this.m_EvolutionList_SelectedIndexChanged );
            // 
            // m_ContextMenuEvo
            // 
            this.m_ContextMenuEvo.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.m_MenuAddEvo,
            this.m_MenuDelEvo} );
            this.m_ContextMenuEvo.Name = "m_ContextMenuEvo";
            this.m_ContextMenuEvo.Size = new System.Drawing.Size( 169, 48 );
            this.m_ContextMenuEvo.Opening += new System.ComponentModel.CancelEventHandler( this.m_ContextMenuEvo_Opening );
            // 
            // m_MenuAddEvo
            // 
            this.m_MenuAddEvo.Name = "m_MenuAddEvo";
            this.m_MenuAddEvo.Size = new System.Drawing.Size( 168, 22 );
            this.m_MenuAddEvo.Text = "Add New Evolution";
            this.m_MenuAddEvo.Click += new System.EventHandler( this.m_MenuAddEvo_Click );
            // 
            // m_MenuDelEvo
            // 
            this.m_MenuDelEvo.Name = "m_MenuDelEvo";
            this.m_MenuDelEvo.Size = new System.Drawing.Size( 168, 22 );
            this.m_MenuDelEvo.Text = "Remove Evolution";
            this.m_MenuDelEvo.Click += new System.EventHandler( this.m_MenuDelEvo_Click );
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.label2.Location = new System.Drawing.Point( 464, 412 );
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size( 100, 16 );
            this.label2.TabIndex = 33;
            this.label2.Text = "Globals";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.label3.Location = new System.Drawing.Point( 599, 412 );
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size( 100, 16 );
            this.label3.TabIndex = 34;
            this.label3.Text = "Evolution Group";
            // 
            // m_EvoProps
            // 
            this.m_EvoProps.Location = new System.Drawing.Point( 894, 25 );
            this.m_EvoProps.Name = "m_EvoProps";
            this.m_EvoProps.Size = new System.Drawing.Size( 388, 808 );
            this.m_EvoProps.TabIndex = 35;
            // 
            // EffectProperties
            // 
            this.Controls.Add( this.label3 );
            this.Controls.Add( this.label2 );
            this.Controls.Add( this.m_EvolutionList );
            this.Controls.Add( this.m_EvoProps );
            this.Controls.Add( this.m_KeyframeList );
            this.Controls.Add( this.m_Title );
            this.Controls.Add( this.m_GroupEventData );
            this.Controls.Add( this.TimeLine1 );
            this.Controls.Add( this.groupBox3 );
            this.Controls.Add( this.groupBox4 );
            this.Controls.Add( this.groupBox2 );
            this.Controls.Add( this.groupBox1 );
            this.Controls.Add( this.menuStrip1 );
            this.Name = "EffectProperties";
            this.Size = new System.Drawing.Size( 1294, 808 );
            this.groupBox2.ResumeLayout( false );
            this.groupBox1.ResumeLayout( false );
            this.groupBox3.ResumeLayout( false );
            this.groupBox4.ResumeLayout( false );
            this.groupBox5.ResumeLayout( false );
            this.groupBox6.ResumeLayout( false );
            this.m_GroupEventData.ResumeLayout( false );
            this.menuStrip1.ResumeLayout( false );
            this.menuStrip1.PerformLayout();
            this.m_ContextMenuEvo.ResumeLayout( false );
            this.ResumeLayout( false );
            this.PerformLayout();

		}
		#endregion

		public static EffectProperties  sm_Instance;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.CheckBox m_CheckboxShowInstances;
		private System.Windows.Forms.CheckBox m_CheckboxShowCullspheres;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label m_LabelDrawTime;
		private System.Windows.Forms.Label m_LabelUpdateTime;
		private System.Windows.Forms.Label m_LabelCulledInstances;
		private System.Windows.Forms.Label m_LabelActiveInstances;
        private System.Windows.Forms.Label m_LabelTotalPoints;
		private System.Windows.Forms.GroupBox groupBox3;
		private ragUi.ControlSlider m_SliderPreUpdate;
		private ragUi.ControlSlider m_SliderNumLoops;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label1;
		private ragUi.ControlSlider m_SliderOffsetZ;
		private ragUi.ControlSlider m_SliderOffsetY;
		private ragUi.ControlSlider m_SliderRadius;
		private ragUi.ControlSlider m_SliderOffsetX;
        private System.Windows.Forms.CheckBox m_CheckboxUseCullsphere;
		private ragPtxTimeLine.ptxTimeLine TimeLine1;

		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.GroupBox m_GroupEventData;
		private System.Windows.Forms.Panel m_EventPanel;
		private System.Windows.Forms.Label m_Title;
		
		private WidgetSliderInt		m_WidgetTotalPoints;
		private WidgetSliderInt		m_WidgetActiveInstances;
		private WidgetSliderInt		m_WidgetCulledInstances;
		private WidgetSliderFloat	m_WidgetUpdateCpu;
		private WidgetButton		m_WidgetAddEmitterEvent=null;
		private WidgetText			m_WidgetEmitterName=null;
		private WidgetButton		m_WidgetAddEffectEvent=null;
		private WidgetButton		m_WidgetDelEvent=null;
		private WidgetSliderInt		m_WidgetSelectedEvent=null;
		private WidgetSliderFloat	m_WidgetDrawCpu;
		private ArrayList		    m_TimeLineEventObjs;
		private WidgetGroup			m_TimeLineWidgetGroup;
        private WidgetGroup         m_EvolutionGroupWidgetGroup = null;
		private WidgetButton		m_WidgetSave=null;
		private WidgetButton		m_WidgetSaveAll=null;
		private WidgetText			m_WidgetEffectName=null;
		private WidgetText			m_WidgetAddEffectName=null;
		private WidgetButton		m_WidgetReorderChannels=null;
		private WidgetButton		m_WidgetBakeZoom=null;
        private WidgetText          m_WidgetAddEvoName = null;
        private WidgetButton        m_WidgetAddEvo = null;
        private WidgetButton        m_WidgetDelEvo = null;
        private int m_CurSelectedChannel = -1;
        private String              m_SelectedEvo = "";

        string m_EffectName = "";
		private ragUi.ControlSlider m_SliderDurationMax;
		private ragUi.ControlSlider m_SliderDurationMin;
		private ragUi.ControlSlider m_SliderRateMax;
		private ragUi.ControlSlider m_SliderRateMin;
		private System.Windows.Forms.ListBox m_KeyframeList;
		private ragUi.ControlSlider m_SliderFZoom;
		private System.Windows.Forms.Button m_ButtonBakeZoom;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.CheckBox m_CheckCullNoDraw;
		private System.Windows.Forms.CheckBox m_CheckCullNoUpdate;
		private System.Windows.Forms.CheckBox m_CheckCullNoEmit;
		private System.Windows.Forms.GroupBox groupBox6;
		private ragUi.ControlSlider m_SliderDistance;
		private System.Windows.Forms.CheckBox m_ZoomCullDistCheckbox;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem saveToolStripMenuItem;
        private ToolStripMenuItem saveAllToolStripMenuItem;
        private ToolStripMenuItem saveAsToolStripMenuItem;
        private ToolStripMenuItem timeLineToolStripMenuItem;
        private ToolStripMenuItem addNewToolStripMenuItem;
        private ToolStripMenuItem addNewEmitterToolStripMenuItem;
        private ToolStripMenuItem addNewEffectToolStripMenuItem;
        private ToolStripMenuItem addNewActionToolStripMenuItem;
        private ToolStripMenuItem addToolStripMenuItem;
        private ToolStripMenuItem editToolStripMenuItem;
        private ToolStripMenuItem addEmitterToolStripMenuItem;
        private ToolStripMenuItem addEffectToolStripMenuItem;
        private ToolStripMenuItem addActionToolStripMenuItem;
        private ListBox m_EvolutionList;
        private Label label2;
        private Label label3;
        private ContextMenuStrip m_ContextMenuEvo;
        private ToolStripMenuItem m_MenuAddEvo;
        private ToolStripMenuItem m_MenuDelEvo;
        private EvoProperties m_EvoProps;
		
		public class StateData 
		{
			public string m_WidgetRootPath;
			public string m_CurrRule;
		}
		public static StateData		sm_SavedStateData;


		public void InitTimeLine()
		{
			MenuItem New =new MenuItem("New..."); 
			MenuItem Load =new MenuItem("Load...");
			MenuItem AddEmitter = new MenuItem("Emitter");
			MenuItem AddEffect = new MenuItem("Effect");
			MenuItem AddAction = new MenuItem("Action");
			MenuItem NewEmitter = new MenuItem("New Emitter");
			MenuItem NewEffect = new MenuItem("New Effect");
			TimeLine1.Menu_Context.MenuItems.Add(0,New);
			TimeLine1.Menu_Context.MenuItems.Add(1,Load);
			Load.MenuItems.Add(AddEmitter);
			Load.MenuItems.Add(AddEffect);
			Load.MenuItems.Add(AddAction);
			New.MenuItems.Add(NewEmitter);
			New.MenuItems.Add(NewEffect);
			NewEmitter.Click += new System.EventHandler(this.TimeLine_Menu_NewEmitter_Click);
			AddEmitter.Click += new System.EventHandler(this.TimeLine_Menu_AddEmitter_Click);
			NewEffect.Click += new System.EventHandler(this.TimeLine_Menu_NewEffect_Click);
			AddEffect.Click += new System.EventHandler(this.TimeLine_Menu_AddEffect_Click);
			AddAction.Click += new System.EventHandler(this.TimeLine_Menu_AddAction_Click);
			m_TimeLineEventObjs = new ArrayList();
			m_TimeLineWidgetGroup = null;
			AddAction.Enabled=false;
		}

        public static string MyName = "Effect Properties";

		public static ragWidgetPage.WidgetPage CreatePage(String appName,IBankManager bankMgr,TD.SandDock.DockControl dockControl)
		{
			if (sm_Instance == null) 
			{	
				sm_Instance = new EffectProperties(appName, bankMgr, dockControl);

                if ( ViewPlugIn.Instance.CheckPluginStatus() )
                {
                    Init();
                }

				return sm_Instance;
			}
			else
			{
				return sm_Instance;
			}
		}

		public void SelectEffect(string effectName)
		{
            m_CurSelectedChannel = -1;
            //m_SelectedEvo = "";
			if(effectName == m_EffectName)
				return;
			m_EffectName=effectName;
            m_SelectedEvo = "";
			FindWidgetHandles();
		}
		protected void FindWidgetHandles()
		{
			RemoveBindings();
			ClearTimeLine();
            m_EvoProps.ClearInterface();

			WidgetGroup effectGroup =null;
			m_WidgetRoot = MainPtxWindow.Instance.m_WidgetEffectList;

			if(m_WidgetRoot!=null)
				effectGroup = m_WidgetRoot.FindFirstWidgetFromPath(m_EffectName) as WidgetGroup;
			
			if(effectGroup == null)
			{
				this.Enabled=false;
				m_DockControl.Text = m_DockControl.TabText = GetTabText();
				return;
			}

			this.Enabled=true;
            m_DockControl.Text = m_DockControl.TabText = GetTabText();

			//Hook up to all the rage widgets
			m_WidgetEffectName = effectGroup.FindFirstWidgetFromPath("Name") as WidgetText;
            Bind(effectGroup, "Show instances", m_CheckboxShowInstances, "Checked");
			Bind(effectGroup,"Show CullSpheres",m_CheckboxShowCullspheres,"Checked");
			m_WidgetSave = effectGroup.FindFirstWidgetFromPath("Save") as WidgetButton;
			m_WidgetSaveAll = effectGroup.FindFirstWidgetFromPath("SaveAll") as WidgetButton;


			//Profiling info
            this.ReferenceGroup(m_EffectName+"/Profiling");
			WidgetGroup profile = effectGroup.FindFirstWidgetFromPath("Profiling") as WidgetGroup;
			if(profile != null)
			{
				m_WidgetTotalPoints =		profile.FindFirstWidgetFromPath("Total Points") as WidgetSliderInt;
				m_WidgetActiveInstances =	profile.FindFirstWidgetFromPath("Active Instances") as WidgetSliderInt;
				m_WidgetCulledInstances =	profile.FindFirstWidgetFromPath("Culled Instances") as WidgetSliderInt;
				m_WidgetUpdateCpu =			profile.FindFirstWidgetFromPath("CPU Update Time (ms)") as WidgetSliderFloat;
				m_WidgetDrawCpu =			profile.FindFirstWidgetFromPath("CPU Draw Time (ms)") as WidgetSliderFloat;
			}

			//Info
			WidgetGroup info = effectGroup.FindFirstWidgetFromPath("Info") as WidgetGroup;
			if(info != null)
			{
				this.ReferenceGroup(m_EffectName+"/Info");
				Bind(info,"UseCulling", m_CheckboxUseCullsphere, "Checked");
				Bind(info,"CullSphere Offset X", m_SliderOffsetX);
				Bind(info,"CullSphere Offset Y", m_SliderOffsetY);
				Bind(info,"CullSphere Offset Z", m_SliderOffsetZ);
				Bind(info,"CullSphere Radius", m_SliderRadius);
				Bind(info,"Cull Distance", m_SliderDistance);
				Bind(info,"ZoomCullDist",m_ZoomCullDistCheckbox,"Checked");
				Bind(info,"NoEmit",m_CheckCullNoEmit,"Checked");
				Bind(info,"NoUpdate",m_CheckCullNoUpdate,"Checked");
				Bind(info,"NoDraw",m_CheckCullNoDraw,"Checked");
				Bind(info,"FreezedZoom",m_SliderFZoom);
				m_WidgetBakeZoom = info.FindFirstWidgetFromPath("BakeZoom") as WidgetButton;
			}

			//TimeLine
			WidgetGroup timeline = effectGroup.FindFirstWidgetFromPath("TimeLine") as WidgetGroup;
			if(timeline != null)
			{
				Bind(timeline,"DurationMin", m_SliderDurationMin);
				Bind(timeline,"DurationMax", m_SliderDurationMax);
				Bind(timeline,"TimeScalarMin", m_SliderRateMin);
				Bind(timeline,"TimeScalarMax", m_SliderRateMax);
				Bind(timeline,"Num Loops", m_SliderNumLoops);
				Bind(timeline,"Pre Update", m_SliderPreUpdate);
				m_WidgetReorderChannels = timeline.FindFirstWidgetFromPath("ReorderChannels") as WidgetButton;
				m_WidgetSelectedEvent = timeline.FindFirstWidgetFromPath("SelectedEvent") as WidgetSliderInt;
				//Buttons	
				m_WidgetDelEvent= timeline.FindFirstWidgetFromPath("RemoveEvent") as WidgetButton;
				m_WidgetAddEmitterEvent = timeline.FindFirstWidgetFromPath("AddEmitterEvent") as WidgetButton;
				m_WidgetEmitterName = timeline.FindFirstWidgetFromPath("Emitter Name") as WidgetText;
				m_WidgetAddEffectEvent = timeline.FindFirstWidgetFromPath("AddEffectEvent") as WidgetButton;
				m_WidgetAddEffectName = timeline.FindFirstWidgetFromPath("Effect Name") as WidgetText;

				//TimeLine Events
				m_TimeLineWidgetGroup = timeline.FindFirstWidgetFromPath("Events") as WidgetGroup;
				BuildEventArray();
			}

            m_EvoProps.ClearInterface();
            m_EvolutionGroupWidgetGroup = effectGroup.FindFirstWidgetFromPath("EvolutionGroup") as WidgetGroup;
            if (m_EvolutionGroupWidgetGroup != null)
            {
                m_WidgetAddEvoName = m_EvolutionGroupWidgetGroup.FindFirstWidgetFromPath("NewEvolutionName") as WidgetText;
                m_WidgetAddEvo = m_EvolutionGroupWidgetGroup.FindFirstWidgetFromPath("CreateNewEvolution") as WidgetButton;
                m_WidgetDelEvo = m_EvolutionGroupWidgetGroup.FindFirstWidgetFromPath("DeleteEvolution") as WidgetButton;
                m_EvolutionList.Items.Clear();
                
                foreach (Widget w in m_EvolutionGroupWidgetGroup.List)
                {
                    if (w.IsGroup())
                        m_EvolutionList.Items.Add(w);
                }
            }
            
            BuildKeyframeList(effectGroup);

            //if(m_CurSelectedChannel>0 && m_CurSelectedChannel < m_TimeLineEventObjs.Count)
              //  TimeLineSelectChannel(m_TimeLineEventObjs[m_CurSelectedChannel] as ptxEvtData);
            //SelectEvolution(m_SelectedEvo);

		}
		
		protected void BuildKeyframeList(WidgetGroup group)
		{
			m_KeyframeList.BeginUpdate();
			m_KeyframeList.Items.Clear();
			WidgetGroup keys = group.FindFirstWidgetFromPath("Keyframes") as WidgetGroup;
			
			if (keys != null)
			{
				foreach(Widget w in keys.List) 
				{
					m_KeyframeList.Items.Add(w);
					//ListItemData d = new ListItemData();
					//d.m_Name = w.ToString();
					//d.m_Color = Color.FromArgb(255,0,100,255);
					//m_ListItems.Add(d);
				}
			}
			m_KeyframeList.EndUpdate();
		}

		private void ClearTimeLine()
		{
			TimeLine1.Clear();
			m_TimeLineEventObjs.Clear();
			m_EventPanel.Controls.Clear();
		}

		private void AddTimeLineEvent(ptxEvtData d,ragPtxTimeLine.ptxTimeLine.ptxChannel c)
		{
			c.m_Data = d;
			TimeLine1.AddChannel(c);
			m_TimeLineEventObjs.Add(d);
			m_EventPanel.SuspendLayout();
			d.Left = 0;
			d.Top = 0;
			d.Width = m_EventPanel.Width;
			d.Height = m_EventPanel.Height;
			d.Visible=false;
			m_EventPanel.Controls.Add(d);
			m_EventPanel.ResumeLayout();
			d.m_Index =c.m_Index;
		}
		private void BuildEventArray()
		{
			if(m_TimeLineWidgetGroup == null)
				return;
			foreach( Widget w in m_TimeLineWidgetGroup.List)
			{
				if(w is WidgetGroup)
				{
					WidgetGroup g = w as WidgetGroup;
					//Determine Type
					if(g.Title == "Emitter")
					{
						//find the attached emitter's duration widget
						float duration=1.0f;
						WidgetText name = g.FindFirstWidgetFromPath("Name") as WidgetText;
						WidgetSliderFloat startTime = g.FindFirstWidgetFromPath("TriggerTime") as WidgetSliderFloat;
						WidgetGroup emitterg = MainPtxWindow.sm_Instance.m_WidgetEmitterList.FindFirstWidgetFromPath(name.String) as WidgetGroup;
						WidgetGroup infog=null;
						WidgetSliderFloat sliderDuration = null;
						if(emitterg!=null)
						{
							infog = emitterg.FindFirstWidgetFromPath("Info") as WidgetGroup;
							if(infog!=null)
							{
								sliderDuration = infog.FindFirstWidgetFromPath("Duration") as WidgetSliderFloat;
								duration = sliderDuration.Value;
							}

						}
						WidgetSliderFloat timeScalarMin = g.FindFirstWidgetFromPath("Time Scalar Min") as WidgetSliderFloat;
						WidgetSliderFloat timeScalarMax = g.FindFirstWidgetFromPath("Time Scalar Max") as WidgetSliderFloat;
						Color color = Color.FromArgb(255,127,177,255);

						ragPtxTimeLine.ptxTimeLine.ptxChannel c = new ragPtxTimeLine.ptxTimeLine.ptxChannel(name.String,duration,startTime.Value,color);
						ptxEvtDataEmitter data = new ptxEvtDataEmitter(name.String,c);

						if(infog!=null)
							data.m_EmitterDuration = infog.FindFirstWidgetFromPath("Duration") as WidgetSliderFloat;

                        data.m_CheckEvoProps = g.FindFirstWidgetFromPath("CheckEvoData") as WidgetButton;
                        data.m_EvoProps = g.FindFirstWidgetFromPath("EventEvoData") as WidgetGroup;

						Bind(g,"DurationScalarMin",data.m_SliderDurationScalarMin);
						Bind(g,"DurationScalarMax",data.m_SliderDurationScalarMax);
						Bind(g,"Time Scalar Min",data.m_SliderPlaybackRateMin);
						Bind(g,"Time Scalar Max",data.m_Slider_PlaybackRateMax);
						Bind(g,"TriggerTime",data.m_SliderStartTime);
						Bind(g,"Index",data.m_SliderIndex);
						Bind(g,"ActiveToggle",data.m_ActiveToggle,"Checked");
						AddTimeLineEvent(data,c);
					}
					if(g.Title == "Effect")
					{
						WidgetText name = g.FindFirstWidgetFromPath("Name") as WidgetText;
						WidgetSliderFloat startTime = g.FindFirstWidgetFromPath("TriggerTime") as WidgetSliderFloat;

						Color color = Color.FromArgb(255,127,255,177);
						ragPtxTimeLine.ptxTimeLine.ptxChannel c = new ragPtxTimeLine.ptxTimeLine.ptxChannel(name.String,0.0f,startTime.Value,color);
						ptxEvtDataEffect data = new ptxEvtDataEffect(name.String,c);

						Bind(g,"TriggerTime",data.m_SliderStartTime);
						Bind(g,"TriggerCap",data.m_SliderTriggerMax);
						Bind(g,"Index",data.m_SliderIndex);
						Bind(g,"ActiveToggle",data.m_ActiveToggle,"Checked");
						Bind(g,"ShowAxis",data.m_CheckShowAxis,"Checked");
						Bind(g,"RotationMin X",data.m_SliderRotationX);
						Bind(g,"RotationMin Y",data.m_SliderRotationY);
						Bind(g,"RotationMin Z",data.m_SliderRotationZ);

						WidgetGroup domaing = g.FindFirstWidgetFromPath("Emitter Domain") as WidgetGroup;
						if(domaing != null)
						{
							data.m_DomainProperties.BindToData(domaing,"");
							AddTimeLineEvent(data,c);
						}
						data.m_DomainProperties.m_CheckWorldSpace.Visible=false;
					}
				}
			}
		}

        public static void Init()
        {
            if ( sm_Instance != null )
            {
                sm_Instance.FindWidgetHandles();
                sm_Instance.timer1.Enabled = true;
            }
        }

		public static void Shutdown()
		{
            if ( sm_Instance != null )
            {
                sm_Instance.ClearView();
                sm_Instance = null;
            }
		}

        public override string GetMyName()
        {
            return MyName;
        }

		public override string GetTabText() 
		{
			if ( this.Enabled && (m_EffectName != null) )
            {
                return "Effect: " + m_EffectName;
            }
            else
            {
                return "Effect: (none)";
            }
		}

        public override bool IsSingleInstance
        {
            get
            {
                return true;
            }
        }

		public override object GetStateData()
		{
			StateData ret = new StateData();
			return ret;
		}
		public override void SetStateData(object d)
		{
			//Need to do this because we've reloaded the widgets:
			m_WidgetRoot=null;
            m_EffectName = null;
			FindWidgetHandles();
		}


		public void SaveEffectAs()
		{
			SaveFileDialog sfd = new SaveFileDialog();
			if(UserData.LastEffectPath.Length>0)
				sfd.InitialDirectory = UserData.LastEffectPath;
			sfd.AddExtension=true;
			sfd.Filter="PtxEffect (*.effectrule)|*.effectrule";
			sfd.ValidateNames=true;
			sfd.OverwritePrompt=true;
			sfd.Title="Save As:";
			if (sfd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastEffectPath = System.IO.Path.GetDirectoryName(sfd.FileName);
				string oldname = m_WidgetEffectName.String;
				string newname = System.IO.Path.GetFileNameWithoutExtension(sfd.FileName);
				m_WidgetEffectName.String = newname;
				m_WidgetSaveAll.Activate();
				m_WidgetEffectName.String=oldname;
			}
		}
		
        private void SelectEvolution(string evoName)
        {
            int id = m_EvolutionList.FindStringExact(evoName);
            m_EvolutionList.SelectedIndex = id;

        }
        private void UpdateTimeLine()
		{
			float duration = (float)this.m_SliderDurationMin.Value;
			duration +=(float)this.m_SliderDurationMax.Value;
			duration*=0.5f;
			TimeLine1.Duration = duration;
		}
		private void UpdateProfiling(object sender, System.EventArgs e)
		{
			SetLabel(m_LabelTotalPoints, "Total Points: {0}", m_WidgetTotalPoints);
			SetLabel(m_LabelActiveInstances, "Active Instances: {0}", m_WidgetActiveInstances);
			SetLabel(m_LabelCulledInstances, "Culled Instances: {0}", m_WidgetCulledInstances);
			SetLabel(m_LabelUpdateTime, "Update Time (cpu): {0:#0.000}", m_WidgetUpdateCpu);
			SetLabel(m_LabelDrawTime, "Draw Time (cpu): {0:#0.000}", m_WidgetDrawCpu);
		}

		private void TimeLine_Menu_AddEmitter_Click(object sender, System.EventArgs e)
		{
			string emitname = MainPtxWindow.Instance.LoadEmitRule("Select an Emitter");
			if(emitname.Length<=0)
				return;
			m_WidgetEmitterName.String= emitname;
            string path = m_WidgetAddEmitterEvent.FindPath();
            EmitterProperties.sm_Instance.RemoveBindings();
            MainPtxWindow.Instance.RemoveBindings();
            PtxRuleProperties.sm_Instance.RemoveBindings();
            RemoveBindings();
            WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
            if (button != null)
                button.Activate();
            //m_WidgetAddEmitterEvent.Activate();
			ViewPlugIn.Instance.RebuildAllPanels();
		}
		private void TimeLine_Menu_NewEmitter_Click(object sender, System.EventArgs e)
		{
			string emitname = MainPtxWindow.Instance.NewEmitRule("Select an Emit Rule Name:");
			if(emitname.Length<=0)
				return;
			m_WidgetEmitterName.String= emitname;
            string path = m_WidgetAddEmitterEvent.FindPath();
            EmitterProperties.sm_Instance.RemoveBindings();
            MainPtxWindow.Instance.RemoveBindings();
            PtxRuleProperties.sm_Instance.RemoveBindings();

            RemoveBindings();
            WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
            if (button != null)
                button.Activate();
            //m_WidgetAddEmitterEvent.Activate();
			ViewPlugIn.Instance.RebuildAllPanels();
		}

		private void TimeLine_Menu_AddEffect_Click(object sender, System.EventArgs e)
		{
			string effectname = MainPtxWindow.Instance.LoadEffectRule("Select an Effect");
			if(effectname.Length<=0)
				return;
			m_WidgetAddEffectName.String= effectname;
			string path = m_WidgetAddEffectEvent.FindPath();
            EmitterProperties.sm_Instance.RemoveBindings();
            MainPtxWindow.Instance.RemoveBindings();
            PtxRuleProperties.sm_Instance.RemoveBindings();

            RemoveBindings();
            WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
            if(button != null)
                button.Activate();
            //m_WidgetAddEffectEvent.Activate();
			ViewPlugIn.Instance.RebuildAllPanels();
		}
		private void TimeLine_Menu_NewEffect_Click(object sender, System.EventArgs e)
		{
			string effect = MainPtxWindow.Instance.NewEffectRule("Select an Effect Rule Name:");
			if(effect.Length<=0)
				return;
			m_WidgetAddEffectName.String= effect;
            string path = m_WidgetAddEffectEvent.FindPath();
            EmitterProperties.sm_Instance.RemoveBindings();
            MainPtxWindow.Instance.RemoveBindings();
            PtxRuleProperties.sm_Instance.RemoveBindings();

            RemoveBindings();
            WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
            if (button != null)
                button.Activate();
            //m_WidgetAddEffectEvent.Activate();
			ViewPlugIn.Instance.RebuildAllPanels();
		}
		private void TimeLine_Menu_AddAction_Click(object sender, System.EventArgs e)
		{
			Color actionColor = Color.FromArgb(255,255,127,177);
			ragPtxTimeLine.ptxTimeLine.ptxChannel channel = new ragPtxTimeLine.ptxTimeLine.ptxChannel("Action 1",2.0f,0.0f,actionColor);
			TimeLine1.AddChannel(channel);
		}

		private void m_SliderDuration_ValueChanged(object sender, System.EventArgs e)
		{
			UpdateTimeLine();
		}

        private void TimeLineSelectChannel(ptxEvtData data)
        {
            foreach (ptxEvtData d in m_TimeLineEventObjs)
            {
                if (d != data)
                    d.Visible = false;
            }

            data.Visible = true;
            data.OnSelected();
            if (data.m_EvoProps != null)
            {
                m_EvoProps.m_CurSelEventData = data;
                if (m_EvolutionList.SelectedItem != null)
                {
                    m_EvoProps.SelectEvolution(m_EvolutionGroupWidgetGroup, m_EvolutionList.SelectedItem.ToString(), m_EffectName);
                    m_EvoProps.SelectEvent(m_EvolutionList.SelectedItem.ToString(),data);
                }
            }
            m_CurSelectedChannel = data.m_PtxChannel.m_Index;
        }
		private void TimeLine1_OnSelectChannel(ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
		{
            TimeLineSelectChannel(channel.m_Data as ptxEvtData);
		}

		private void TimeLine1_OnChannelInfoChanged(ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
		{
			ptxEvtData data = channel.m_Data as ptxEvtData;
			data.UpdateInterface();
		}

		private void TimeLine1_OnDeleteChannel(ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
		{
			m_WidgetSelectedEvent.Value = channel.m_Index;
            string path = m_WidgetDelEvent.FindPath();
            MainPtxWindow.Instance.RemoveBindings();
            EmitterProperties.sm_Instance.RemoveBindings();
            PtxRuleProperties.sm_Instance.RemoveBindings();

            RemoveBindings();
            WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
            if (button != null)
                button.Activate();
			//m_WidgetDelEvent.Activate();
			ViewPlugIn.Instance.RebuildAllPanels();
		}

		private void Menu_Save_Activate(object sender, System.EventArgs e)
		{
			m_WidgetSave.Activate();
		}

		private void Menu_SaveAll_Activate(object sender, System.EventArgs e)
		{
			m_WidgetSaveAll.Activate();
		}

		private void m_MenuSaveAs_Activate(object sender, System.EventArgs e)
		{
			SaveEffectAs();
		}

		private void TimeLine1_OnReOrderChannels(object sender, System.EventArgs e)
		{
			//Must set the channel's indexes...
			foreach(ptxEvtData d in m_TimeLineEventObjs)
			{
				ragPtxTimeLine.ptxTimeLine.ptxChannel c =d.m_PtxChannel;
				d.Index=c.m_Index;
			}
            string path = m_WidgetReorderChannels.FindPath();
            EmitterProperties.sm_Instance.RemoveBindings();
            MainPtxWindow.Instance.RemoveBindings();
            PtxRuleProperties.sm_Instance.RemoveBindings();

            RemoveBindings();
            WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
            if (button != null)
                button.Activate();
            //m_WidgetReorderChannels.Activate();
			ViewPlugIn.Instance.RebuildAllPanels();
		}

		private void m_SliderDurationMax_ValueChanged(object sender, System.EventArgs e)
		{
			UpdateTimeLine();
		}

		private void m_KeyframeList_DoubleClick(object sender, System.EventArgs e)
		{
            ViewPlugIn.Instance.CreateAndOpenKeyframeEditorView( m_KeyframeList.SelectedItem as WidgetGroup,"EffectRule: "+m_EffectName+"/" );
		}

		private void m_ButtonBakeZoom_Click(object sender, System.EventArgs e)
		{
			m_WidgetBakeZoom.Activate();
		}

        private void m_MenuAddEvo_Click(object sender, EventArgs e)
        {
            TextPromptDialog dlg = new TextPromptDialog();
            dlg.PromptText = "Enter New Evolution Name";
            dlg.ShowDialog();
            if (dlg.ReturnState)
            {
                //Search for duplicates
                if(m_EvolutionList.FindStringExact(dlg.Value)==-1)
                {
                    m_EvolutionList.Items.Add(dlg.Value);
                    m_WidgetAddEvoName.String = dlg.Value;
                    string path = m_WidgetAddEvo.FindPath();
                    EmitterProperties.sm_Instance.RemoveBindings();
                    MainPtxWindow.Instance.RemoveBindings();
                    PtxRuleProperties.sm_Instance.RemoveBindings();

                    RemoveBindings();
                    WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
                    if (button != null)
                        button.Activate();
                    //m_WidgetAddEvo.Activate();
                    ViewPlugIn.Instance.RebuildAllPanels();
                }
            }
        }

        private void m_EvolutionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_EvolutionList.SelectedItem == null)
                return;
            
            m_EvoProps.SelectEvolution(m_EvolutionGroupWidgetGroup, m_EvolutionList.SelectedItem.ToString(), m_EffectName);
            m_SelectedEvo = m_EvolutionList.SelectedItem.ToString();
        }

        private void m_MenuDelEvo_Click(object sender, EventArgs e)
        {
            m_WidgetAddEvoName.String = m_EvolutionList.SelectedItem.ToString();
            string path = m_WidgetDelEvo.FindPath();
            EmitterProperties.sm_Instance.RemoveBindings();
            MainPtxWindow.Instance.RemoveBindings();
            PtxRuleProperties.sm_Instance.RemoveBindings();

            RemoveBindings();
            WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
            if (button != null)
                button.Activate();
            //m_WidgetDelEvo.Activate();
            ViewPlugIn.Instance.RebuildAllPanels();

        }
        private void m_ContextMenuEvo_Opening(object sender, CancelEventArgs e)
        {
            m_MenuAddEvo.Enabled=false;
            m_MenuDelEvo.Enabled=false;
            
            if (m_EvolutionList.SelectedItem != null)
                m_MenuDelEvo.Enabled = true;

            if (m_EvolutionList.Items.Count < 4)
                m_MenuAddEvo.Enabled = true;
        }
	}
}

