using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using ragCore;
using ragWidgets;


namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for VelDomainProperties.
	/// </summary>
	public class VelDomainProperties : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox m_CheckWorldSpace;
		private System.Windows.Forms.ListBox m_ListDomainKeyframes;
		private System.Windows.Forms.CheckBox m_CheckBoxShowDomain;
		private System.Windows.Forms.CheckBox m_CheckPointRel;
		private System.Windows.Forms.ComboBox m_ComboShape;
		private System.Windows.Forms.Label label2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public VelDomainProperties()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.m_CheckWorldSpace = new System.Windows.Forms.CheckBox();
			this.m_ListDomainKeyframes = new System.Windows.Forms.ListBox();
			this.m_CheckBoxShowDomain = new System.Windows.Forms.CheckBox();
			this.m_CheckPointRel = new System.Windows.Forms.CheckBox();
			this.m_ComboShape = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(7, 68);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(118, 13);
			this.label1.TabIndex = 10;
			this.label1.Text = "Properties:";
			// 
			// m_CheckWorldSpace
			// 
			this.m_CheckWorldSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_CheckWorldSpace.Location = new System.Drawing.Point(16, 34);
			this.m_CheckWorldSpace.Name = "m_CheckWorldSpace";
			this.m_CheckWorldSpace.Size = new System.Drawing.Size(125, 24);
			this.m_CheckWorldSpace.TabIndex = 9;
			this.m_CheckWorldSpace.Text = "World Space";
			// 
			// m_ListDomainKeyframes
			// 
			this.m_ListDomainKeyframes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.m_ListDomainKeyframes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_ListDomainKeyframes.Location = new System.Drawing.Point(8, 86);
			this.m_ListDomainKeyframes.Name = "m_ListDomainKeyframes";
			this.m_ListDomainKeyframes.Size = new System.Drawing.Size(328, 147);
			this.m_ListDomainKeyframes.TabIndex = 8;
			this.m_ListDomainKeyframes.DoubleClick += new System.EventHandler(this.m_ListDomainKeyframes_DoubleClick);
			// 
			// m_CheckBoxShowDomain
			// 
			this.m_CheckBoxShowDomain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_CheckBoxShowDomain.Location = new System.Drawing.Point(16, 8);
			this.m_CheckBoxShowDomain.Name = "m_CheckBoxShowDomain";
			this.m_CheckBoxShowDomain.Size = new System.Drawing.Size(117, 24);
			this.m_CheckBoxShowDomain.TabIndex = 7;
			this.m_CheckBoxShowDomain.Text = "Show Domain";
			// 
			// m_CheckPointRel
			// 
			this.m_CheckPointRel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_CheckPointRel.Location = new System.Drawing.Point(112, 32);
			this.m_CheckPointRel.Name = "m_CheckPointRel";
			this.m_CheckPointRel.Size = new System.Drawing.Size(125, 24);
			this.m_CheckPointRel.TabIndex = 11;
			this.m_CheckPointRel.Text = "Point Relative";
			// 
			// m_ComboShape
			// 
			this.m_ComboShape.Location = new System.Drawing.Point(200, 12);
			this.m_ComboShape.Name = "m_ComboShape";
			this.m_ComboShape.Size = new System.Drawing.Size(121, 21);
			this.m_ComboShape.TabIndex = 12;
			this.m_ComboShape.SelectedIndexChanged += new System.EventHandler(this.m_ComboShape_SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(120, 12);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(93, 16);
			this.label2.TabIndex = 13;
			this.label2.Text = "Domain Shape";
			// 
			// VelDomainProperties
			// 
			this.Controls.Add(this.m_ComboShape);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.m_CheckPointRel);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.m_CheckWorldSpace);
			this.Controls.Add(this.m_ListDomainKeyframes);
			this.Controls.Add(this.m_CheckBoxShowDomain);
			this.Name = "VelDomainProperties";
			this.Size = new System.Drawing.Size(344, 248);
			this.ResumeLayout(false);

		}
		#endregion

		WidgetGroup					m_WidgetRoot=null;
		string						m_EmitterName="";
		ragWidgets.WidgetBinding	m_BindShowDomain=null;
		ragWidgets.WidgetBinding	m_BindWorldSpace=null;
		ragWidgets.WidgetBinding	m_BindPointRel=null;
		ragWidgets.WidgetBinding	m_BindComboShape=null;
		bool						m_AllowComboShapeEvents=false;

		public void BindToData(WidgetGroup rootPath,string emitterName)
		{
			m_EmitterName=emitterName;
			if(rootPath==null)
			{
				this.Enabled=false;
				return;
			}
			else
				this.Enabled = true;

			SetWidgetRoot(rootPath);
			FindWidgetHandles();
		}
		protected void SetWidgetRoot(WidgetGroup newRoot)
		{
			if (m_WidgetRoot != newRoot)
			{
				m_WidgetRoot = newRoot;
				FindWidgetHandles();
			}
		}

		protected void RemoveBindings()
		{
			if(m_BindShowDomain != null) 
				m_BindShowDomain.DisconnectEvents();
			if(m_BindWorldSpace != null) 
				m_BindWorldSpace.DisconnectEvents();
			if(m_BindPointRel != null) 
				m_BindPointRel.DisconnectEvents();
			if(m_BindComboShape != null) 
				m_BindComboShape.DisconnectEvents();
		}

		protected void FindWidgetHandles()
		{
			RemoveBindings();
			if(m_WidgetRoot==null)
				return;

			
			//Bind Show,type,worldspace and properties
			ragCore.Widget widget =null;

			//Show Domain
			widget= m_WidgetRoot.FindFirstWidgetFromPath("ShowVelocityDomain");
			if(widget!= null)
			{
				m_CheckBoxShowDomain.Enabled=true;
				m_BindShowDomain= ragWidgets.WidgetBinding.Bind(widget, m_CheckBoxShowDomain,"Checked");
			}
			else
				m_CheckBoxShowDomain.Enabled=false;

			//WorldSpace
			widget= m_WidgetRoot.FindFirstWidgetFromPath("WorldSpace");
			if(widget!= null)
			{
				m_CheckWorldSpace.Enabled=true;
				m_BindWorldSpace= ragWidgets.WidgetBinding.Bind(widget, m_CheckWorldSpace,"Checked");
			}
			else
				m_CheckWorldSpace.Enabled=false;

			//PointRel
			widget= m_WidgetRoot.FindFirstWidgetFromPath("PointRelative");
			if(widget!= null)
			{
				m_CheckPointRel.Enabled=true;
				m_BindPointRel= ragWidgets.WidgetBinding.Bind(widget, m_CheckPointRel,"Checked");
			}
			else
				m_CheckPointRel.Enabled=false;


			//Types
			m_AllowComboShapeEvents=false;
			m_ComboShape.BeginUpdate();
			m_ComboShape.Items.Clear();
			WidgetGroup types = m_WidgetRoot.FindFirstWidgetFromPath("Types") as WidgetGroup;
			if (types != null)
			{
				foreach(Widget k in types.List)
				{
					m_ComboShape.Items.Add(k);
				}
			}

			//Type
			
			widget= m_WidgetRoot.FindFirstWidgetFromPath("TuneType");
			if(widget!= null)
			{
				m_ComboShape.Enabled=true;
				m_BindComboShape= ragWidgets.WidgetBinding.Bind(widget, m_ComboShape,"SelectedIndex");
			}
			else
				m_ComboShape.Enabled=false;
			m_ComboShape.EndUpdate();
			m_AllowComboShapeEvents=true;


			//Properties
			m_ListDomainKeyframes.BeginUpdate();
			m_ListDomainKeyframes.Items.Clear();
			
			WidgetGroup keys = m_WidgetRoot.FindFirstWidgetFromPath("DomainKeyframes") as WidgetGroup;
			if (keys != null)
			{
				foreach(Widget k in keys.List)
				{
					m_ListDomainKeyframes.Items.Add(k);
				}
			}
			m_ListDomainKeyframes.EndUpdate();


		}

		private void m_ListDomainKeyframes_DoubleClick(object sender, System.EventArgs e)
		{
			if (m_ListDomainKeyframes.SelectedItem is WidgetGroup)
			{
				KeyframeEditor editor = ViewPlugIn.Instance.CreateView(new ragWidgetPage.WidgetPage.CreatePageDel(KeyframeEditor.CreateView)) as KeyframeEditor;
				editor.Init(m_ListDomainKeyframes.SelectedItem as WidgetGroup, m_WidgetRoot.ToString()+": "+m_EmitterName+"/");
			}
		}

		private void m_ComboShape_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(!m_AllowComboShapeEvents) return;
			ViewPlugIn.Instance.RebuildAllPanels();
		
		}
	}
}
