using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using ragKeyframeEditor;

using ragUi;
using ragCore;
using ragWidgets;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for KeyframeEditor.
	/// </summary>
	public class KeyframeEditor : ragWidgetPage.WidgetPage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public KeyframeEditor()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );

		}

		private ragKeyframeEditorControl.KeyframeEditor m_KeyframeEditor;
		private ragUi.ControlToggle m_Worldspace;
		private ragUi.ControlToggle m_VelocityAlignment;
		private ragUi.ControlToggle m_DrawDirectional;
		private ragUi.ControlToggle m_ScaleByLife;
		private ragUi.ControlToggle m_UseSystemTime;


		public KeyframeEditor(String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl)
			: base(appName, bankMgr, dockControl)
		{
			InitializeComponent();

            // get rid of the window when it's closed
            dockControl.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;

            // don't save this window with the SandDock layout
            dockControl.PersistState = false;
        }

        public static string MyName = "Keyframe Editor";

		public static ragWidgetPage.WidgetPage CreateView(String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl)
		{

            KeyframeEditor editor = new KeyframeEditor(appName, bankMgr, dockControl);
            dockControl.FloatingSize = new Size(editor.Width, editor.Height);
            dockControl.FloatingLocation = new Point(Cursor.Position.X-editor.Height/3,Cursor.Position.Y-editor.Height/2);
            dockControl.Closing += new TD.SandDock.DockControlClosingEventHandler( editor.CloseKeyframeEditor );
			sm_KeyframeEditors.Add(editor);
			return editor;
		}
        
        public override bool InitiallyFloating
        {
            get {
                return true;
            }
        }

		protected string m_ParentName;

		public void Init(WidgetGroup widgetRoot, string parentName)
		{
//            m_DockControl.OpenFloating(TD.SandDock.WindowOpenMethod.OnScreenActivate);
			
			m_WidgetRoot = widgetRoot;

			m_ParentName = parentName;

			m_DockControl.Text = m_DockControl.TabText = GetTabText();

            FindWidgetHandles();
		}

		private void FindWidgetHandles()
		{
            RemoveBindings();
			if (m_WidgetRoot == null)
			{
				this.Enabled = false;
			}
			else
			{
				int left =64;
				int top = 0;
				int spacing=8;
				if(Bind("UseSystemTime", m_UseSystemTime))
				{
					m_UseSystemTime.Visible=true;
					m_UseSystemTime.Left=left;
					m_UseSystemTime.Top=top;
					left+=m_UseSystemTime.Width+spacing;
				}
				else
					m_UseSystemTime.Visible=false;
				if(Bind("WorldSpace", m_Worldspace))
				{
					m_Worldspace.Visible=true;
					m_Worldspace.Left=left;
					m_Worldspace.Top=top;
					left+=m_Worldspace.Width+spacing;
				}
				else
					m_Worldspace.Visible=false;
				
				if(Bind("DrawDirectional", m_DrawDirectional))
				{
					m_DrawDirectional.Visible=true;
					m_DrawDirectional.Left=left;
					m_DrawDirectional.Top=top;
					left+=m_DrawDirectional.Width+spacing;
				}
				else
					m_DrawDirectional.Visible=false;

				if(Bind("VelocityAlignment", m_VelocityAlignment))
				{
					m_VelocityAlignment.Visible=true;
					m_VelocityAlignment.Left=left;
					m_VelocityAlignment.Top = top;
					left+=m_VelocityAlignment.Width+spacing;
				}
				else
					m_VelocityAlignment.Visible=false;
				
				if(Bind("ScaleByParticleLife", m_ScaleByLife))
				{
					m_ScaleByLife.Visible=true;
					m_ScaleByLife.Left=left;
					m_ScaleByLife.Top=top;
					left+=m_ScaleByLife.Width+spacing;
				}
				else
					m_ScaleByLife.Visible=false;

				List<ragCore.Widget> widgets = m_WidgetRoot.List;
				ragKeyframeEditor.WidgetKeyframeEditor kf = null;
				foreach(Widget w in widgets) 
				{
					kf = w as ragKeyframeEditor.WidgetKeyframeEditor;
					if (kf != null) 
					{
						break;
					}
				}
			
				if (kf == null) 
				{
					m_KeyframeEditor.Enabled = false;
				}
				else 
				{
					kf.ConfigureControl(m_KeyframeEditor);
                    Bind(kf.Title, m_KeyframeEditor, "Value");
				}
			}
		}

        /// <summary>
        /// Used for identifying this WidgetPage for SandDock related operations.  Because the return
        /// value is dynamic, ViewPlugIn has been written specifically to handle this.
        /// </summary>
        /// <returns></returns>
        public override string GetMyName()
        {
            return GetTabText();
        }

		public override string GetTabText()
		{
			if (m_WidgetRoot != null)
			{
				return m_ParentName + " " + m_WidgetRoot.ToString();
			}
			else {
				return MyName;
			}
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_KeyframeEditor = new ragKeyframeEditorControl.KeyframeEditor();
			this.m_UseSystemTime = new ragUi.ControlToggle();
			this.m_Worldspace = new ragUi.ControlToggle();
			this.m_VelocityAlignment = new ragUi.ControlToggle();
			this.m_DrawDirectional = new ragUi.ControlToggle();
			this.m_ScaleByLife = new ragUi.ControlToggle();
			this.SuspendLayout();
			// 
			// m_KeyframeEditor
			// 
			this.m_KeyframeEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.m_KeyframeEditor.Location = new System.Drawing.Point(0, 24);
			this.m_KeyframeEditor.Name = "m_KeyframeEditor";
			this.m_KeyframeEditor.Size = new System.Drawing.Size(960, 418);
			this.m_KeyframeEditor.TabIndex = 0;
			// 
			// m_UseSystemTime
			// 
			this.m_UseSystemTime.Location = new System.Drawing.Point(64, 16);
			this.m_UseSystemTime.Name = "m_UseSystemTime";
			this.m_UseSystemTime.Size = new System.Drawing.Size(112, 24);
			this.m_UseSystemTime.TabIndex = 1;
			this.m_UseSystemTime.Text = "UseSystemTime";
			this.m_UseSystemTime.Value = false;
			this.m_UseSystemTime.ValueXPath = null;
			// 
			// m_Worldspace
			// 
			this.m_Worldspace.Location = new System.Drawing.Point(184, 16);
			this.m_Worldspace.Name = "m_Worldspace";
			this.m_Worldspace.Size = new System.Drawing.Size(96, 24);
			this.m_Worldspace.TabIndex = 0;
			this.m_Worldspace.Text = "WorldSpace";
			this.m_Worldspace.Value = false;
			this.m_Worldspace.ValueXPath = null;
			// 
			// m_VelocityAlignment
			// 
			this.m_VelocityAlignment.Location = new System.Drawing.Point(288, 16);
			this.m_VelocityAlignment.Name = "m_VelocityAlignment";
			this.m_VelocityAlignment.Size = new System.Drawing.Size(120, 24);
			this.m_VelocityAlignment.TabIndex = 2;
			this.m_VelocityAlignment.Text = "VelocityAlignment";
			this.m_VelocityAlignment.Value = false;
			this.m_VelocityAlignment.ValueXPath = null;
			// 
			// m_DrawDirectional
			// 
			this.m_DrawDirectional.Location = new System.Drawing.Point(420, 16);
			this.m_DrawDirectional.Name = "m_DrawDirectional";
			this.m_DrawDirectional.Size = new System.Drawing.Size(108, 24);
			this.m_DrawDirectional.TabIndex = 3;
			this.m_DrawDirectional.Text = "Draw Directional";
			this.m_DrawDirectional.Value = false;
			this.m_DrawDirectional.ValueXPath = null;
			// 
			// m_ScaleByLife
			// 
			this.m_ScaleByLife.Location = new System.Drawing.Point(544, 16);
			this.m_ScaleByLife.Name = "m_ScaleByLife";
			this.m_ScaleByLife.Size = new System.Drawing.Size(128, 24);
			this.m_ScaleByLife.TabIndex = 4;
			this.m_ScaleByLife.Text = "Scale by particle life";
			this.m_ScaleByLife.Value = false;
			this.m_ScaleByLife.ValueXPath = null;
			// 
			// KeyframeEditor
			// 
			this.Controls.Add(this.m_ScaleByLife);
			this.Controls.Add(this.m_DrawDirectional);
			this.Controls.Add(this.m_VelocityAlignment);
			this.Controls.Add(this.m_Worldspace);
			this.Controls.Add(this.m_UseSystemTime);
			this.Controls.Add(this.m_KeyframeEditor);
			this.Name = "KeyframeEditor";
			this.Size = new System.Drawing.Size(960, 440);
			this.ResumeLayout(false);

		}
		#endregion

        public static void Shutdown()
        {
            // we don't want to persist *any* information about a KeyframeEditor,
            // so close its window which will remove any data it might have saved
            // in DllInfo.PluginDataHash
            while ( sm_KeyframeEditors.Count > 0 )
            {
                sm_KeyframeEditors[0].DockControl.Close();
            }
        }

        public static List<KeyframeEditor> sm_KeyframeEditors = new List<KeyframeEditor>();
		public object m_SavedStateData;

        /// <summary>
        /// Finds the KeyframeEditor with the matching widgetRoot and parentName
        /// </summary>
        /// <param name="widgetRoot"></param>
        /// <param name="parentName"></param>
        /// <returns>null if not found</returns>
        public static KeyframeEditor FindKeyframeEditor( WidgetGroup widgetRoot, String parentName )
        {
            foreach ( KeyframeEditor editor in sm_KeyframeEditors )
            {
                if ( (editor.m_WidgetRoot == widgetRoot) && (editor.m_ParentName == parentName) )
                {
                    return editor;
                }
            }

            return null;
        }

        private void CloseKeyframeEditor( object sender, TD.SandDock.DockControlClosingEventArgs e )
		{
			// remove it from the static list
			sm_KeyframeEditors.Remove(this);
		}

		public override object GetStateData()
		{
			return ragWidgetPage.WidgetPagePlugIn.BasicGetStateData(this);
		}

		public override void SetStateData(object d)
		{
			ragWidgetPage.WidgetPagePlugIn.BasicSetStateData(this, d);
			FindWidgetHandles();
		}
	}
}
