namespace ragParticleEditor
{
    partial class EvoProperties
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.m_LabelEvolutionName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_ComboBlendMode = new System.Windows.Forms.ComboBox();
            this.m_ButtonCheckEvo = new System.Windows.Forms.Button();
            this.m_CheckOverrideProcControl = new System.Windows.Forms.CheckBox();
            this.m_SliderTestEvo = new ragUi.ControlSlider();
            this.m_ListEventProps = new System.Windows.Forms.ListBox();
            this.m_ContextEvoProp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_MenuAddEvoProp = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuDelEvoProp = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuEditEvoProp = new System.Windows.Forms.ToolStripMenuItem();
            this.m_LabelEvtProps = new System.Windows.Forms.Label();
            this.m_ListGlobalProps = new System.Windows.Forms.ListBox();
            this.m_ContextGlobalProp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.m_MenuGlobAddEvoProp = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuGlobDelEvoProp = new System.Windows.Forms.ToolStripMenuItem();
            this.m_MenuGlobEditEvoProp = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.m_comboGloblend = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.m_ContextEvoProp.SuspendLayout();
            this.m_ContextGlobalProp.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_LabelEvolutionName
            // 
            this.m_LabelEvolutionName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_LabelEvolutionName.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_LabelEvolutionName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelEvolutionName.Location = new System.Drawing.Point(0, 0);
            this.m_LabelEvolutionName.Name = "m_LabelEvolutionName";
            this.m_LabelEvolutionName.Size = new System.Drawing.Size(386, 19);
            this.m_LabelEvolutionName.TabIndex = 0;
            this.m_LabelEvolutionName.Text = "Evolution Set: ";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.m_comboGloblend);
            this.panel1.Controls.Add(this.m_ComboBlendMode);
            this.panel1.Controls.Add(this.m_ButtonCheckEvo);
            this.panel1.Controls.Add(this.m_CheckOverrideProcControl);
            this.panel1.Controls.Add(this.m_SliderTestEvo);
            this.panel1.Controls.Add(this.m_ListEventProps);
            this.panel1.Controls.Add(this.m_LabelEvtProps);
            this.panel1.Controls.Add(this.m_ListGlobalProps);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.m_LabelEvolutionName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(388, 808);
            this.panel1.TabIndex = 1;
            // 
            // m_ComboBlendMode
            // 
            this.m_ComboBlendMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ComboBlendMode.Items.AddRange(new object[] {
            "Average",
            "Add",
            "Max"});
            this.m_ComboBlendMode.Location = new System.Drawing.Point(223, 174);
            this.m_ComboBlendMode.MaxDropDownItems = 12;
            this.m_ComboBlendMode.Name = "m_ComboBlendMode";
            this.m_ComboBlendMode.Size = new System.Drawing.Size(152, 21);
            this.m_ComboBlendMode.TabIndex = 25;
            this.m_ComboBlendMode.Text = "Add";
            // 
            // m_ButtonCheckEvo
            // 
            this.m_ButtonCheckEvo.Location = new System.Drawing.Point(254, 680);
            this.m_ButtonCheckEvo.Name = "m_ButtonCheckEvo";
            this.m_ButtonCheckEvo.Size = new System.Drawing.Size(121, 36);
            this.m_ButtonCheckEvo.TabIndex = 24;
            this.m_ButtonCheckEvo.Text = "Build Ptx Evo Props";
            this.m_ButtonCheckEvo.UseVisualStyleBackColor = true;
            this.m_ButtonCheckEvo.Click += new System.EventHandler(this.m_ButtonCheckEvo_Click);
            // 
            // m_CheckOverrideProcControl
            // 
            this.m_CheckOverrideProcControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CheckOverrideProcControl.Location = new System.Drawing.Point(6, 692);
            this.m_CheckOverrideProcControl.Name = "m_CheckOverrideProcControl";
            this.m_CheckOverrideProcControl.Size = new System.Drawing.Size(369, 24);
            this.m_CheckOverrideProcControl.TabIndex = 23;
            this.m_CheckOverrideProcControl.Text = "Override Procedural Control";
            // 
            // m_SliderTestEvo
            // 
            this.m_SliderTestEvo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderTestEvo.IsFloat = true;
            this.m_SliderTestEvo.Location = new System.Drawing.Point(6, 760);
            this.m_SliderTestEvo.Maximum = 1F;
            this.m_SliderTestEvo.Minimum = 0F;
            this.m_SliderTestEvo.Name = "m_SliderTestEvo";
            this.m_SliderTestEvo.Size = new System.Drawing.Size(369, 24);
            this.m_SliderTestEvo.Step = 0.01F;
            this.m_SliderTestEvo.TabIndex = 22;
            this.m_SliderTestEvo.Text = "Test Evolution";
            this.m_SliderTestEvo.UseTrackbar = false;
            this.m_SliderTestEvo.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderTestEvo.ValueXPath = null;
            // 
            // m_ListEventProps
            // 
            this.m_ListEventProps.ContextMenuStrip = this.m_ContextEvoProp;
            this.m_ListEventProps.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.m_ListEventProps.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.m_ListEventProps.FormattingEnabled = true;
            this.m_ListEventProps.Location = new System.Drawing.Point(6, 198);
            this.m_ListEventProps.Name = "m_ListEventProps";
            this.m_ListEventProps.Size = new System.Drawing.Size(369, 472);
            this.m_ListEventProps.TabIndex = 4;
            this.m_ListEventProps.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.m_ListEventProps_DrawItem);
            this.m_ListEventProps.DoubleClick += new System.EventHandler(this.m_ListEventProps_DoubleClick);
            // 
            // m_ContextEvoProp
            // 
            this.m_ContextEvoProp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_MenuAddEvoProp,
            this.m_MenuDelEvoProp,
            this.m_MenuEditEvoProp});
            this.m_ContextEvoProp.Name = "contextMenuStrip1";
            this.m_ContextEvoProp.Size = new System.Drawing.Size(197, 70);
            this.m_ContextEvoProp.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // m_MenuAddEvoProp
            // 
            this.m_MenuAddEvoProp.Name = "m_MenuAddEvoProp";
            this.m_MenuAddEvoProp.Size = new System.Drawing.Size(196, 22);
            this.m_MenuAddEvoProp.Text = "Add Evolution Property";
            this.m_MenuAddEvoProp.Click += new System.EventHandler(this.m_MenuAddEvoProp_Click);
            // 
            // m_MenuDelEvoProp
            // 
            this.m_MenuDelEvoProp.Name = "m_MenuDelEvoProp";
            this.m_MenuDelEvoProp.Size = new System.Drawing.Size(196, 22);
            this.m_MenuDelEvoProp.Text = "Del Evolution Property";
            this.m_MenuDelEvoProp.Click += new System.EventHandler(this.m_MenuDelEvoProp_Click);
            // 
            // m_MenuEditEvoProp
            // 
            this.m_MenuEditEvoProp.Name = "m_MenuEditEvoProp";
            this.m_MenuEditEvoProp.Size = new System.Drawing.Size(196, 22);
            this.m_MenuEditEvoProp.Text = "Edit Property";
            this.m_MenuEditEvoProp.Click += new System.EventHandler(this.m_MenuEditEvoProp_Click);
            // 
            // m_LabelEvtProps
            // 
            this.m_LabelEvtProps.AutoSize = true;
            this.m_LabelEvtProps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelEvtProps.Location = new System.Drawing.Point(3, 182);
            this.m_LabelEvtProps.Name = "m_LabelEvtProps";
            this.m_LabelEvtProps.Size = new System.Drawing.Size(169, 13);
            this.m_LabelEvtProps.TabIndex = 3;
            this.m_LabelEvtProps.Text = "Evolution Properties (Global)";
            // 
            // m_ListGlobalProps
            // 
            this.m_ListGlobalProps.ContextMenuStrip = this.m_ContextGlobalProp;
            this.m_ListGlobalProps.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.m_ListGlobalProps.FormattingEnabled = true;
            this.m_ListGlobalProps.Location = new System.Drawing.Point(6, 47);
            this.m_ListGlobalProps.Name = "m_ListGlobalProps";
            this.m_ListGlobalProps.Size = new System.Drawing.Size(369, 121);
            this.m_ListGlobalProps.TabIndex = 2;
            this.m_ListGlobalProps.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.m_ListGlobalProps_DrawItem);
            this.m_ListGlobalProps.DoubleClick += new System.EventHandler(this.m_ListGlobalProps_DoubleClick);
            // 
            // m_ContextGlobalProp
            // 
            this.m_ContextGlobalProp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_MenuGlobAddEvoProp,
            this.m_MenuGlobDelEvoProp,
            this.m_MenuGlobEditEvoProp});
            this.m_ContextGlobalProp.Name = "contextMenuStrip1";
            this.m_ContextGlobalProp.Size = new System.Drawing.Size(197, 70);
            this.m_ContextGlobalProp.Opening += new System.ComponentModel.CancelEventHandler(this.m_ContextGlobalProp_Opening);
            // 
            // m_MenuGlobAddEvoProp
            // 
            this.m_MenuGlobAddEvoProp.Name = "m_MenuGlobAddEvoProp";
            this.m_MenuGlobAddEvoProp.Size = new System.Drawing.Size(196, 22);
            this.m_MenuGlobAddEvoProp.Text = "Add Evolution Property";
            this.m_MenuGlobAddEvoProp.Click += new System.EventHandler(this.m_MenuGlobAddEvoProp_Click);
            // 
            // m_MenuGlobDelEvoProp
            // 
            this.m_MenuGlobDelEvoProp.Name = "m_MenuGlobDelEvoProp";
            this.m_MenuGlobDelEvoProp.Size = new System.Drawing.Size(196, 22);
            this.m_MenuGlobDelEvoProp.Text = "Del Evolution Property";
            this.m_MenuGlobDelEvoProp.Click += new System.EventHandler(this.m_MenuGlobDelEvoProp_Click);
            // 
            // m_MenuGlobEditEvoProp
            // 
            this.m_MenuGlobEditEvoProp.Name = "m_MenuGlobEditEvoProp";
            this.m_MenuGlobEditEvoProp.Size = new System.Drawing.Size(196, 22);
            this.m_MenuGlobEditEvoProp.Text = "Edit Property";
            this.m_MenuGlobEditEvoProp.Click += new System.EventHandler(this.m_MenuGlobEditEvoProp_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Evolution Properties (Global)";
            // 
            // m_comboGloblend
            // 
            this.m_comboGloblend.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_comboGloblend.Items.AddRange(new object[] {
            "Average",
            "Add",
            "Max"});
            this.m_comboGloblend.Location = new System.Drawing.Point(223, 23);
            this.m_comboGloblend.MaxDropDownItems = 12;
            this.m_comboGloblend.Name = "m_comboGloblend";
            this.m_comboGloblend.Size = new System.Drawing.Size(152, 21);
            this.m_comboGloblend.TabIndex = 27;
            this.m_comboGloblend.Text = "Add";
            // 
            // EvoProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "EvoProperties";
            this.Size = new System.Drawing.Size(388, 808);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.m_ContextEvoProp.ResumeLayout(false);
            this.m_ContextGlobalProp.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label m_LabelEvolutionName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox m_ListGlobalProps;
        private System.Windows.Forms.ListBox m_ListEventProps;
        private System.Windows.Forms.Label m_LabelEvtProps;
        private System.Windows.Forms.ToolStripMenuItem m_MenuAddEvoProp;
        private System.Windows.Forms.ToolStripMenuItem m_MenuDelEvoProp;
        private System.Windows.Forms.ToolStripMenuItem m_MenuEditEvoProp;
        public System.Windows.Forms.ContextMenuStrip m_ContextEvoProp;
        public System.Windows.Forms.ContextMenuStrip m_ContextGlobalProp;
        private System.Windows.Forms.ToolStripMenuItem m_MenuGlobAddEvoProp;
        private System.Windows.Forms.ToolStripMenuItem m_MenuGlobDelEvoProp;
        private System.Windows.Forms.ToolStripMenuItem m_MenuGlobEditEvoProp;
        public ragUi.ControlSlider m_SliderTestEvo;
        public System.Windows.Forms.CheckBox m_CheckOverrideProcControl;
        private System.Windows.Forms.Button m_ButtonCheckEvo;
        private System.Windows.Forms.ComboBox m_ComboBlendMode;
        private System.Windows.Forms.ComboBox m_comboGloblend;
    }
}
