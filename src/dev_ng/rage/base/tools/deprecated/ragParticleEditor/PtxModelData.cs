using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using ragUi;
using ragCore;
using ragWidgets;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for PtxModelData.
	/// </summary>
	public class PtxModelData : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.ListBox m_ListModels;
		private System.Windows.Forms.Button m_ButtonRemove;
		private System.Windows.Forms.Button m_ButtonAdd;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		public ragUi.ControlSlider m_SliderRotZ;
		public ragUi.ControlSlider m_SliderRotY;
		public ragUi.ControlSlider m_SliderRotX;
		public ragUi.ControlSlider m_SliderRotVarZ;
		public ragUi.ControlSlider m_SliderRotVarY;
		public ragUi.ControlSlider m_SliderRotVarX;
		public ragUi.ControlSlider m_SliderRotSpeed;
		public ragUi.ControlSlider m_SliderRotSpeedVar;
		private System.Windows.Forms.CheckBox m_CheckBoxOrientateEffectMtx;
        private CheckBox m_CheckPropSize;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PtxModelData()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.m_ListModels = new System.Windows.Forms.ListBox();
            this.m_ButtonRemove = new System.Windows.Forms.Button();
            this.m_ButtonAdd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.m_SliderRotZ = new ragUi.ControlSlider();
            this.m_SliderRotY = new ragUi.ControlSlider();
            this.m_SliderRotX = new ragUi.ControlSlider();
            this.label4 = new System.Windows.Forms.Label();
            this.m_SliderRotVarZ = new ragUi.ControlSlider();
            this.m_SliderRotVarY = new ragUi.ControlSlider();
            this.m_SliderRotVarX = new ragUi.ControlSlider();
            this.label2 = new System.Windows.Forms.Label();
            this.m_SliderRotSpeed = new ragUi.ControlSlider();
            this.m_SliderRotSpeedVar = new ragUi.ControlSlider();
            this.m_CheckBoxOrientateEffectMtx = new System.Windows.Forms.CheckBox();
            this.m_CheckPropSize = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // m_ListModels
            // 
            this.m_ListModels.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ListModels.Location = new System.Drawing.Point(16, 24);
            this.m_ListModels.Name = "m_ListModels";
            this.m_ListModels.Size = new System.Drawing.Size(192, 108);
            this.m_ListModels.TabIndex = 0;
            // 
            // m_ButtonRemove
            // 
            this.m_ButtonRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonRemove.Location = new System.Drawing.Point(216, 72);
            this.m_ButtonRemove.Name = "m_ButtonRemove";
            this.m_ButtonRemove.Size = new System.Drawing.Size(56, 23);
            this.m_ButtonRemove.TabIndex = 1;
            this.m_ButtonRemove.Text = "Remove";
            this.m_ButtonRemove.Click += new System.EventHandler(this.m_ButtonRemove_Click);
            // 
            // m_ButtonAdd
            // 
            this.m_ButtonAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonAdd.Location = new System.Drawing.Point(216, 40);
            this.m_ButtonAdd.Name = "m_ButtonAdd";
            this.m_ButtonAdd.Size = new System.Drawing.Size(56, 24);
            this.m_ButtonAdd.TabIndex = 2;
            this.m_ButtonAdd.Text = "Add...";
            this.m_ButtonAdd.Click += new System.EventHandler(this.m_ButtonAdd_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 3;
            this.label1.Text = "Models";
            // 
            // m_SliderRotZ
            // 
            this.m_SliderRotZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotZ.IsFloat = true;
            this.m_SliderRotZ.Location = new System.Drawing.Point(24, 264);
            this.m_SliderRotZ.Maximum = 1000F;
            this.m_SliderRotZ.Minimum = -1000F;
            this.m_SliderRotZ.Name = "m_SliderRotZ";
            this.m_SliderRotZ.Size = new System.Drawing.Size(112, 24);
            this.m_SliderRotZ.Step = 0.01F;
            this.m_SliderRotZ.TabIndex = 42;
            this.m_SliderRotZ.Text = "Z";
            this.m_SliderRotZ.UseTrackbar = true;
            this.m_SliderRotZ.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderRotZ.ValueXPath = null;
            // 
            // m_SliderRotY
            // 
            this.m_SliderRotY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotY.IsFloat = true;
            this.m_SliderRotY.Location = new System.Drawing.Point(24, 240);
            this.m_SliderRotY.Maximum = 1000F;
            this.m_SliderRotY.Minimum = -1000F;
            this.m_SliderRotY.Name = "m_SliderRotY";
            this.m_SliderRotY.Size = new System.Drawing.Size(112, 24);
            this.m_SliderRotY.Step = 0.01F;
            this.m_SliderRotY.TabIndex = 41;
            this.m_SliderRotY.Text = "Y";
            this.m_SliderRotY.UseTrackbar = true;
            this.m_SliderRotY.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderRotY.ValueXPath = null;
            // 
            // m_SliderRotX
            // 
            this.m_SliderRotX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotX.IsFloat = true;
            this.m_SliderRotX.Location = new System.Drawing.Point(24, 216);
            this.m_SliderRotX.Maximum = 1000F;
            this.m_SliderRotX.Minimum = -1000F;
            this.m_SliderRotX.Name = "m_SliderRotX";
            this.m_SliderRotX.Size = new System.Drawing.Size(112, 24);
            this.m_SliderRotX.Step = 0.01F;
            this.m_SliderRotX.TabIndex = 40;
            this.m_SliderRotX.Text = "X";
            this.m_SliderRotX.UseTrackbar = true;
            this.m_SliderRotX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderRotX.ValueXPath = null;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(48, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 23);
            this.label4.TabIndex = 43;
            this.label4.Text = "Rotation Axis";
            // 
            // m_SliderRotVarZ
            // 
            this.m_SliderRotVarZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotVarZ.IsFloat = true;
            this.m_SliderRotVarZ.Location = new System.Drawing.Point(152, 264);
            this.m_SliderRotVarZ.Maximum = 1000F;
            this.m_SliderRotVarZ.Minimum = -1000F;
            this.m_SliderRotVarZ.Name = "m_SliderRotVarZ";
            this.m_SliderRotVarZ.Size = new System.Drawing.Size(112, 24);
            this.m_SliderRotVarZ.Step = 0.01F;
            this.m_SliderRotVarZ.TabIndex = 46;
            this.m_SliderRotVarZ.Text = "Z";
            this.m_SliderRotVarZ.UseTrackbar = true;
            this.m_SliderRotVarZ.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderRotVarZ.ValueXPath = null;
            // 
            // m_SliderRotVarY
            // 
            this.m_SliderRotVarY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotVarY.IsFloat = true;
            this.m_SliderRotVarY.Location = new System.Drawing.Point(152, 240);
            this.m_SliderRotVarY.Maximum = 1000F;
            this.m_SliderRotVarY.Minimum = -1000F;
            this.m_SliderRotVarY.Name = "m_SliderRotVarY";
            this.m_SliderRotVarY.Size = new System.Drawing.Size(112, 24);
            this.m_SliderRotVarY.Step = 0.01F;
            this.m_SliderRotVarY.TabIndex = 45;
            this.m_SliderRotVarY.Text = "Y";
            this.m_SliderRotVarY.UseTrackbar = true;
            this.m_SliderRotVarY.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderRotVarY.ValueXPath = null;
            // 
            // m_SliderRotVarX
            // 
            this.m_SliderRotVarX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotVarX.IsFloat = true;
            this.m_SliderRotVarX.Location = new System.Drawing.Point(152, 216);
            this.m_SliderRotVarX.Maximum = 1000F;
            this.m_SliderRotVarX.Minimum = -1000F;
            this.m_SliderRotVarX.Name = "m_SliderRotVarX";
            this.m_SliderRotVarX.Size = new System.Drawing.Size(112, 24);
            this.m_SliderRotVarX.Step = 0.01F;
            this.m_SliderRotVarX.TabIndex = 44;
            this.m_SliderRotVarX.Text = "X";
            this.m_SliderRotVarX.UseTrackbar = true;
            this.m_SliderRotVarX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderRotVarX.ValueXPath = null;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(176, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 23);
            this.label2.TabIndex = 47;
            this.label2.Text = "Rotation Axis Bias";
            // 
            // m_SliderRotSpeed
            // 
            this.m_SliderRotSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotSpeed.IsFloat = true;
            this.m_SliderRotSpeed.Location = new System.Drawing.Point(24, 304);
            this.m_SliderRotSpeed.Maximum = 1000F;
            this.m_SliderRotSpeed.Minimum = -1000F;
            this.m_SliderRotSpeed.Name = "m_SliderRotSpeed";
            this.m_SliderRotSpeed.Size = new System.Drawing.Size(224, 24);
            this.m_SliderRotSpeed.Step = 0.01F;
            this.m_SliderRotSpeed.TabIndex = 48;
            this.m_SliderRotSpeed.Text = "Rotation Speed Min";
            this.m_SliderRotSpeed.UseTrackbar = false;
            this.m_SliderRotSpeed.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderRotSpeed.ValueXPath = null;
            // 
            // m_SliderRotSpeedVar
            // 
            this.m_SliderRotSpeedVar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRotSpeedVar.IsFloat = true;
            this.m_SliderRotSpeedVar.Location = new System.Drawing.Point(24, 328);
            this.m_SliderRotSpeedVar.Maximum = 1000F;
            this.m_SliderRotSpeedVar.Minimum = -1000F;
            this.m_SliderRotSpeedVar.Name = "m_SliderRotSpeedVar";
            this.m_SliderRotSpeedVar.Size = new System.Drawing.Size(224, 24);
            this.m_SliderRotSpeedVar.Step = 0.01F;
            this.m_SliderRotSpeedVar.TabIndex = 49;
            this.m_SliderRotSpeedVar.Text = "Rotation Speed Max";
            this.m_SliderRotSpeedVar.UseTrackbar = false;
            this.m_SliderRotSpeedVar.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderRotSpeedVar.ValueXPath = null;
            // 
            // m_CheckBoxOrientateEffectMtx
            // 
            this.m_CheckBoxOrientateEffectMtx.Location = new System.Drawing.Point(32, 168);
            this.m_CheckBoxOrientateEffectMtx.Name = "m_CheckBoxOrientateEffectMtx";
            this.m_CheckBoxOrientateEffectMtx.Size = new System.Drawing.Size(224, 24);
            this.m_CheckBoxOrientateEffectMtx.TabIndex = 50;
            this.m_CheckBoxOrientateEffectMtx.Text = "Orientate To Effect Matrix";
            // 
            // m_CheckPropSize
            // 
            this.m_CheckPropSize.Location = new System.Drawing.Point(32, 147);
            this.m_CheckPropSize.Name = "m_CheckPropSize";
            this.m_CheckPropSize.Size = new System.Drawing.Size(224, 24);
            this.m_CheckPropSize.TabIndex = 51;
            this.m_CheckPropSize.Text = "Proportional Size";
            // 
            // PtxModelData
            // 
            this.Controls.Add(this.m_CheckPropSize);
            this.Controls.Add(this.m_CheckBoxOrientateEffectMtx);
            this.Controls.Add(this.m_SliderRotSpeedVar);
            this.Controls.Add(this.m_SliderRotSpeed);
            this.Controls.Add(this.m_SliderRotVarZ);
            this.Controls.Add(this.m_SliderRotVarY);
            this.Controls.Add(this.m_SliderRotVarX);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.m_SliderRotZ);
            this.Controls.Add(this.m_SliderRotY);
            this.Controls.Add(this.m_SliderRotX);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_ButtonAdd);
            this.Controls.Add(this.m_ButtonRemove);
            this.Controls.Add(this.m_ListModels);
            this.Controls.Add(this.label1);
            this.Name = "PtxModelData";
            this.Size = new System.Drawing.Size(288, 360);
            this.ResumeLayout(false);

		}
		#endregion

		public List<ragCore.Widget> m_ModelWidgets;
		public WidgetButton m_WidgetAddModelBtn;
		public WidgetButton m_WidgetRemoveModelBtn;
		public WidgetText m_WidgetModelName;
        private List<WidgetBinding> m_Bindings = new List<WidgetBinding>();
		public PtxRuleProperties m_Parent;
	
		public void BindToData(WidgetGroup group)
		{
			RemoveBindings();
			WidgetGroup info = group.FindFirstWidgetFromPath("Info") as WidgetGroup;
			if(info == null) return;

			WidgetGroup models = info.FindFirstWidgetFromPath("Models") as WidgetGroup;
			if(models != null)
			{
				m_WidgetModelName = models.FindFirstWidgetFromPath("ModelName") as WidgetText;
				m_WidgetAddModelBtn = models.FindFirstWidgetFromPath("Add Model") as WidgetButton;
				m_WidgetRemoveModelBtn = models.FindFirstWidgetFromPath("Remove Model") as WidgetButton;

				m_ModelWidgets = models.FindWidgetsFromPath("CurrModel");
                Bind(models.FindFirstWidgetFromPath("ProportionalSize"), m_CheckPropSize, "Checked");
			}

			WidgetGroup rotation = info.FindFirstWidgetFromPath("Rotation") as WidgetGroup;
			if(rotation != null)
			{
				Bind(rotation.FindFirstWidgetFromPath("InitRotateToEffectMatrix"),m_CheckBoxOrientateEffectMtx,"Checked");
				Bind(rotation.FindFirstWidgetFromPath("Rotation X"), m_SliderRotX);
				Bind(rotation.FindFirstWidgetFromPath("Rotation Y"), m_SliderRotY);
				Bind(rotation.FindFirstWidgetFromPath("Rotation Z"), m_SliderRotZ);

				Bind(rotation.FindFirstWidgetFromPath("Rotation Var X"), m_SliderRotVarX);
				Bind(rotation.FindFirstWidgetFromPath("Rotation Var Y"), m_SliderRotVarY);
				Bind(rotation.FindFirstWidgetFromPath("Rotation Var Z"), m_SliderRotVarZ);

				Bind(rotation.FindFirstWidgetFromPath("Rotation Speed Min"), m_SliderRotSpeed);
				Bind(rotation.FindFirstWidgetFromPath("Rotation Speed Max"), m_SliderRotSpeedVar);
			}

			UpdateList();
		}

		private void RemoveBindings()
		{
			foreach(WidgetBinding b in m_Bindings)
			{
				b.DisconnectEvents();
			}
			m_Bindings.Clear();
		}

		private void Bind(Widget w, System.Windows.Forms.Control ctrl, string controlProp)
		{
			WidgetBinding bind = ragWidgets.WidgetBinding.Bind(w,ctrl,controlProp);
			if(bind != null)
				m_Bindings.Add(bind);
		}

		private void Bind(Widget w,ControlBase ctrl,string controlProp)
		{
			WidgetBinding bind = ragWidgets.WidgetBinding.Bind(w,ctrl,controlProp);
			if(bind != null)
				m_Bindings.Add(bind);
		}
		private void Bind(Widget w,ControlBase ctrl)
		{
			WidgetBinding bind = ragWidgets.WidgetBinding.Bind(w,ctrl);
			if(bind != null)
				m_Bindings.Add(bind);
		}

		public void UpdateList()
		{
			m_ListModels.BeginUpdate();
			m_ListModels.Items.Clear();
			if (m_ModelWidgets != null)
			{
				foreach(WidgetText text in m_ModelWidgets) 
				{
					m_ListModels.Items.Add(text);
				}
			}
			m_ListModels.EndUpdate();

			m_ListModels.ValueMember = "String";
		}

		private void m_ButtonRemove_Click(object sender, System.EventArgs e)
		{
			m_WidgetModelName.String = (m_ListModels.SelectedItem as WidgetText).String;
			m_WidgetRemoveModelBtn.Activate();
			ViewPlugIn.Instance.RebuildAllPanels();
		}

		private void m_ButtonAdd_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			if(UserData.LastModelPath.Length>0)
				ofd.InitialDirectory = UserData.LastModelPath;
			ofd.CheckFileExists=true;
			ofd.Filter="Type Files(*.type)|*.type";

			if (ofd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastModelPath = System.IO.Path.GetDirectoryName(ofd.FileName);
				string name = System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(ofd.FileName));
				m_WidgetModelName.String = name;
				m_WidgetAddModelBtn.Activate();
				ViewPlugIn.Instance.RebuildAllPanels();
			}
		}
	}
}
