using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using ragCore;
using ragWidgets;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for evtEmitterData.
	/// </summary>
	public class ptxEvtDataEmitter : ptxEvtData
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel1;
		public ragUi.ControlSlider m_Slider_PlaybackRateMax;
		public ragUi.ControlSlider m_SliderPlaybackRateMin;
		public ragUi.ControlSlider m_SliderStartTime;
		private System.Windows.Forms.Label m_LabelName;
		public System.Windows.Forms.CheckBox m_ActiveToggle;
		public ragUi.ControlSlider m_SliderDurationScalarMin;
		public ragUi.ControlSlider m_SliderDurationScalarMax;

		#region Variables
		public WidgetSliderFloat m_EmitterDuration;
		#endregion
		
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ptxEvtDataEmitter(string name, ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
		{
			InitializeComponent();
			m_Name = name;
			m_PtxChannel = channel;
			UpdateInterface();

		}
		public ptxEvtDataEmitter()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.m_ActiveToggle = new System.Windows.Forms.CheckBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.m_SliderDurationScalarMax = new ragUi.ControlSlider();
			this.m_SliderDurationScalarMin = new ragUi.ControlSlider();
			this.m_Slider_PlaybackRateMax = new ragUi.ControlSlider();
			this.m_SliderPlaybackRateMin = new ragUi.ControlSlider();
			this.m_SliderStartTime = new ragUi.ControlSlider();
			this.m_LabelName = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_SliderIndex
			// 
			this.m_SliderIndex.Name = "m_SliderIndex";
			// 
			// label1
			// 
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 1);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 15);
			this.label1.TabIndex = 15;
			this.label1.Text = "Emitter Event";
			// 
			// m_ActiveToggle
			// 
			this.m_ActiveToggle.Checked = true;
			this.m_ActiveToggle.CheckState = System.Windows.Forms.CheckState.Checked;
			this.m_ActiveToggle.Location = new System.Drawing.Point(224, -2);
			this.m_ActiveToggle.Name = "m_ActiveToggle";
			this.m_ActiveToggle.TabIndex = 16;
			this.m_ActiveToggle.Text = "Active";
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.m_SliderDurationScalarMax);
			this.panel1.Controls.Add(this.m_SliderDurationScalarMin);
			this.panel1.Controls.Add(this.m_Slider_PlaybackRateMax);
			this.panel1.Controls.Add(this.m_SliderPlaybackRateMin);
			this.panel1.Controls.Add(this.m_SliderStartTime);
			this.panel1.Controls.Add(this.m_LabelName);
			this.panel1.Location = new System.Drawing.Point(8, 19);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(280, 269);
			this.panel1.TabIndex = 17;
			this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
			// 
			// m_SliderDurationScalarMax
			// 
			this.m_SliderDurationScalarMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_SliderDurationScalarMax.IsFloat = true;
			this.m_SliderDurationScalarMax.Location = new System.Drawing.Point(8, 78);
			this.m_SliderDurationScalarMax.Maximum = 100F;
			this.m_SliderDurationScalarMax.Minimum = 0F;
			this.m_SliderDurationScalarMax.Name = "m_SliderDurationScalarMax";
			this.m_SliderDurationScalarMax.Size = new System.Drawing.Size(208, 24);
			this.m_SliderDurationScalarMax.Step = 0.01F;
			this.m_SliderDurationScalarMax.TabIndex = 23;
			this.m_SliderDurationScalarMax.Text = "Duration Scalar Max";
			this.m_SliderDurationScalarMax.UseTrackbar = false;
			this.m_SliderDurationScalarMax.Value = new System.Decimal(new int[] {
																					0,
																					0,
																					0,
																					0});
			this.m_SliderDurationScalarMax.ValueXPath = null;
			this.m_SliderDurationScalarMax.ValueChanged += new System.EventHandler(this.m_SliderDurationScalarMax_ValueChanged);
			// 
			// m_SliderDurationScalarMin
			// 
			this.m_SliderDurationScalarMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_SliderDurationScalarMin.IsFloat = true;
			this.m_SliderDurationScalarMin.Location = new System.Drawing.Point(8, 51);
			this.m_SliderDurationScalarMin.Maximum = 100F;
			this.m_SliderDurationScalarMin.Minimum = 0F;
			this.m_SliderDurationScalarMin.Name = "m_SliderDurationScalarMin";
			this.m_SliderDurationScalarMin.Size = new System.Drawing.Size(208, 24);
			this.m_SliderDurationScalarMin.Step = 0.01F;
			this.m_SliderDurationScalarMin.TabIndex = 22;
			this.m_SliderDurationScalarMin.Text = "Duration Scalar Min";
			this.m_SliderDurationScalarMin.UseTrackbar = false;
			this.m_SliderDurationScalarMin.Value = new System.Decimal(new int[] {
																					0,
																					0,
																					0,
																					0});
			this.m_SliderDurationScalarMin.ValueXPath = null;
			this.m_SliderDurationScalarMin.ValueChanged += new System.EventHandler(this.m_SliderDurationScalarMin_ValueChanged);
			// 
			// m_Slider_PlaybackRateMax
			// 
			this.m_Slider_PlaybackRateMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_Slider_PlaybackRateMax.IsFloat = true;
			this.m_Slider_PlaybackRateMax.Location = new System.Drawing.Point(8, 132);
			this.m_Slider_PlaybackRateMax.Maximum = 100F;
			this.m_Slider_PlaybackRateMax.Minimum = 0F;
			this.m_Slider_PlaybackRateMax.Name = "m_Slider_PlaybackRateMax";
			this.m_Slider_PlaybackRateMax.Size = new System.Drawing.Size(208, 24);
			this.m_Slider_PlaybackRateMax.Step = 0.01F;
			this.m_Slider_PlaybackRateMax.TabIndex = 20;
			this.m_Slider_PlaybackRateMax.Text = "PlaybackRate Max";
			this.m_Slider_PlaybackRateMax.UseTrackbar = false;
			this.m_Slider_PlaybackRateMax.Value = new System.Decimal(new int[] {
																				   0,
																				   0,
																				   0,
																				   0});
			this.m_Slider_PlaybackRateMax.ValueXPath = null;
			// 
			// m_SliderPlaybackRateMin
			// 
			this.m_SliderPlaybackRateMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_SliderPlaybackRateMin.IsFloat = true;
			this.m_SliderPlaybackRateMin.Location = new System.Drawing.Point(8, 105);
			this.m_SliderPlaybackRateMin.Maximum = 100F;
			this.m_SliderPlaybackRateMin.Minimum = 0F;
			this.m_SliderPlaybackRateMin.Name = "m_SliderPlaybackRateMin";
			this.m_SliderPlaybackRateMin.Size = new System.Drawing.Size(208, 24);
			this.m_SliderPlaybackRateMin.Step = 0.01F;
			this.m_SliderPlaybackRateMin.TabIndex = 19;
			this.m_SliderPlaybackRateMin.Text = "PlaybackRate Min";
			this.m_SliderPlaybackRateMin.UseTrackbar = false;
			this.m_SliderPlaybackRateMin.Value = new System.Decimal(new int[] {
																				  0,
																				  0,
																				  0,
																				  0});
			this.m_SliderPlaybackRateMin.ValueXPath = null;
			// 
			// m_SliderStartTime
			// 
			this.m_SliderStartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_SliderStartTime.IsFloat = true;
			this.m_SliderStartTime.Location = new System.Drawing.Point(8, 21);
			this.m_SliderStartTime.Maximum = 100F;
			this.m_SliderStartTime.Minimum = 0F;
			this.m_SliderStartTime.Name = "m_SliderStartTime";
			this.m_SliderStartTime.Size = new System.Drawing.Size(208, 24);
			this.m_SliderStartTime.Step = 0.01F;
			this.m_SliderStartTime.TabIndex = 18;
			this.m_SliderStartTime.Text = "Start Time";
			this.m_SliderStartTime.UseTrackbar = false;
			this.m_SliderStartTime.Value = new System.Decimal(new int[] {
																			0,
																			0,
																			0,
																			0});
			this.m_SliderStartTime.ValueXPath = null;
			this.m_SliderStartTime.ValueChanged += new System.EventHandler(this.m_SliderStartTime_ValueChanged);
			// 
			// m_LabelName
			// 
			this.m_LabelName.Location = new System.Drawing.Point(3, 3);
			this.m_LabelName.Name = "m_LabelName";
			this.m_LabelName.Size = new System.Drawing.Size(256, 23);
			this.m_LabelName.TabIndex = 21;
			this.m_LabelName.Text = "label2";
			// 
			// ptxEvtDataEmitter
			// 
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.m_ActiveToggle);
			this.Controls.Add(this.label1);
			this.Name = "ptxEvtDataEmitter";
			this.Size = new System.Drawing.Size(296, 296);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion


		private void UpdateChannel()
		{
			m_PtxChannel.StartTime = (float)m_SliderStartTime.Value;
			float duration =(float) (m_EmitterDuration!=null?m_EmitterDuration.Value:0.0f);
			float scale = (float) ((m_SliderDurationScalarMin.Value+m_SliderDurationScalarMax.Value)/2);
			m_PtxChannel.Duration = (float)(duration * scale);
		}
		public override void UpdateInterface()
		{
			m_SliderStartTime.Value = (decimal)m_PtxChannel.StartTime;
			m_LabelName.Text = m_Name;
		}
		public override void OnSelected()
		{
			if(MainPtxWindow.sm_Instance!=null)
				MainPtxWindow.sm_Instance.SelectEmitter(m_Name);
		}
		private void m_SliderStartTime_ValueChanged(object sender, System.EventArgs e)
		{
			UpdateChannel();
		}
		
		private void m_SliderDurationScalarMin_ValueChanged(object sender, System.EventArgs e)
		{
			UpdateChannel();
		}

		private void m_SliderDurationScalarMax_ValueChanged(object sender, System.EventArgs e)
		{
			UpdateChannel();
		}

		private void panel1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		
		}
	}
}
