using System;
using ragCore;
using ragWidgets;
namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for ptxEvtData.
	/// </summary>
	public class ptxEvtData : System.Windows.Forms.UserControl
	{
        public WidgetGroup  m_EvoProps = null;
        public WidgetButton m_CheckEvoProps = null;
        
		public ptxEvtData()
		{
			//
			// TODO: Add constructor logic here
			//
			m_SliderIndex = new ragUi.ControlSlider();
            m_EvoProps = null;
		}
		#region Variables
		protected string m_Name;
		public ragPtxTimeLine.ptxTimeLine.ptxChannel m_PtxChannel;
		public int m_Index=-1;
		public ragUi.ControlSlider m_SliderIndex=null;
		#endregion

		#region Properties
		public int Index
		{
			get
			{
				return m_Index;
			}
			set
			{
				m_Index=value;
				m_SliderIndex.Value=value;
			}
		}
		#endregion


		public virtual void UpdateInterface()
		{
		}
		public virtual void OnSelected()
		{
		}
	}
}
