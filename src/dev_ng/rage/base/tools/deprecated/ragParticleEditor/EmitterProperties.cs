using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using ragParticleEditor;

using ragCore;
using ragWidgets;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for EmitterProperties.
	/// </summary>
	public class EmitterProperties : ragWidgetPage.WidgetPage
    {
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.ListBox m_ListPtxRules;
		private ragUi.ControlSlider m_SliderDuration;
		private System.Windows.Forms.Label m_LabelDrawTime;
		private System.Windows.Forms.Label m_LabelUpdateTime;
		private System.Windows.Forms.Label m_LabelCulledInstances;
		private System.Windows.Forms.Label m_LabelActiveInstances;
        private System.Windows.Forms.Label m_LabelTotalPoints;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ListBox m_ListEmissionKeyframes;
		private System.Windows.Forms.ContextMenu m_PtxRulesMenu;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TabControl m_TabDomains;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.CheckBox m_CheckboxOneShot;
		private ragParticleEditor.ptxDomainProperties m_EmitDomainProp;
		private ragParticleEditor.ptxDomainProperties m_VelDomainProp;
		private System.Windows.Forms.MenuItem m_MenuAddNewPtxRule;
		private System.Windows.Forms.MenuItem m_MenuAddNewSpriteRule;
		private System.Windows.Forms.MenuItem m_MenuAddNewModelRule;
        private System.Windows.Forms.MenuItem m_MenuLoadPtxRule;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem reloadToolStripMenuItem;
        private ToolStripMenuItem saveToolStripMenuItem;
        private ToolStripMenuItem saveAllToolStripMenuItem;
        private ToolStripMenuItem saveAsToolStripMenuItem;
        private ToolStripMenuItem loadPtxRuleToolStripMenuItem;
        private ToolStripMenuItem createNewPtxRuleToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem spriteToolStripMenuItem;
        private ToolStripMenuItem modelToolStripMenuItem;
		private System.ComponentModel.IContainer components;

		public EmitterProperties()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

        public override string GetMyName()
        {
            return MyName;
        }

		public override string GetTabText() 
		{
            if ( this.Enabled && (m_EmitterName != null) )
            {
                return "Emitter: " + m_EmitterName;
            }
            else
			{
                return "Emitter: (none)";
			}
		}

        public override bool IsSingleInstance
        {
            get
            {
                return true;
            }
        }

		public static EmitterProperties sm_Instance;
        public static string MyName = "Emitter Properties";

		public static ragWidgetPage.WidgetPage CreatePage(String appName,IBankManager bankMgr,TD.SandDock.DockControl dockControl)
		{
			if (sm_Instance == null) 
			{	
				sm_Instance = new EmitterProperties(appName, bankMgr, dockControl);

                if ( ViewPlugIn.Instance.CheckPluginStatus() )
                {
                    Init();
                }

				return sm_Instance;
			}
			else
			{
				return sm_Instance;
			}
		}

        public static void Init()
        {
            if ( sm_Instance != null )
            {
                sm_Instance.FindWidgetHandles();
                sm_Instance.timer1.Enabled = true;
            }
        }
		
		public static void Shutdown()
		{
            if ( sm_Instance != null )
            {
                sm_Instance.ClearView();
                sm_Instance = null;
            }
		}


		public EmitterProperties(String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl)
			: base(appName, bankMgr, dockControl)
		{
			InitializeComponent();
		}

		// PreserveSelection should be true if you expect that the newRoot has the same contents as the old.
		// All the widget handles we'll need
		
		private WidgetButton		m_WidgetSave;
		private WidgetButton		m_WidgetSaveAll;
		//private WidgetButton		m_WidgetLoad;
		private WidgetButton		m_WidgetLoadPtx;
		private WidgetText			m_WidgetLoadPtxName;
		private WidgetSliderInt		m_WidgetTotalPoints;
		private WidgetSliderFloat	m_WidgetUpdateCpu;
		private WidgetSliderFloat	m_WidgetDrawCpu;
		private WidgetGroup			m_WidgetPtxRules;
		
		private WidgetText			m_WidgetEmitterName=null;

		private string				m_EmitterName;

		struct ListItemData
		{
			public Color	m_Color;
			public string  m_Name;
		};
		private ArrayList		m_ListItems = new ArrayList();



		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.m_ListEmissionKeyframes = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.m_ListPtxRules = new System.Windows.Forms.ListBox();
            this.m_PtxRulesMenu = new System.Windows.Forms.ContextMenu();
            this.m_MenuLoadPtxRule = new System.Windows.Forms.MenuItem();
            this.m_MenuAddNewPtxRule = new System.Windows.Forms.MenuItem();
            this.m_MenuAddNewSpriteRule = new System.Windows.Forms.MenuItem();
            this.m_MenuAddNewModelRule = new System.Windows.Forms.MenuItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.m_CheckboxOneShot = new System.Windows.Forms.CheckBox();
            this.m_SliderDuration = new ragUi.ControlSlider();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_LabelDrawTime = new System.Windows.Forms.Label();
            this.m_LabelUpdateTime = new System.Windows.Forms.Label();
            this.m_LabelCulledInstances = new System.Windows.Forms.Label();
            this.m_LabelActiveInstances = new System.Windows.Forms.Label();
            this.m_LabelTotalPoints = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer( this.components );
            this.label2 = new System.Windows.Forms.Label();
            this.m_TabDomains = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.m_EmitDomainProp = new ragParticleEditor.ptxDomainProperties();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.m_VelDomainProp = new ragParticleEditor.ptxDomainProperties();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.loadPtxRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createNewPtxRuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.m_TabDomains.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_ListEmissionKeyframes
            // 
            this.m_ListEmissionKeyframes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ListEmissionKeyframes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.m_ListEmissionKeyframes.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_ListEmissionKeyframes.ForeColor = System.Drawing.Color.FromArgb( ((int)(((byte)(255)))), ((int)(((byte)(100)))), ((int)(((byte)(0)))) );
            this.m_ListEmissionKeyframes.Location = new System.Drawing.Point( 0, 0 );
            this.m_ListEmissionKeyframes.Name = "m_ListEmissionKeyframes";
            this.m_ListEmissionKeyframes.Size = new System.Drawing.Size( 450, 270 );
            this.m_ListEmissionKeyframes.TabIndex = 0;
            this.m_ListEmissionKeyframes.DrawItem += new System.Windows.Forms.DrawItemEventHandler( this.m_ListEmissionKeyframes_DrawItem );
            this.m_ListEmissionKeyframes.DoubleClick += new System.EventHandler( this.m_ListEmissionKeyframes_DoubleClick );
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.label6.Location = new System.Drawing.Point( 16, 224 );
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size( 100, 14 );
            this.label6.TabIndex = 24;
            this.label6.Text = "PtxRules";
            // 
            // m_ListPtxRules
            // 
            this.m_ListPtxRules.ContextMenu = this.m_PtxRulesMenu;
            this.m_ListPtxRules.Items.AddRange( new object[] {
            "Candle_Smoke_01"} );
            this.m_ListPtxRules.Location = new System.Drawing.Point( 16, 240 );
            this.m_ListPtxRules.Name = "m_ListPtxRules";
            this.m_ListPtxRules.Size = new System.Drawing.Size( 456, 56 );
            this.m_ListPtxRules.TabIndex = 23;
            this.m_ListPtxRules.SelectedIndexChanged += new System.EventHandler( this.m_ListPtxRules_SelectedIndexChanged );
            // 
            // m_PtxRulesMenu
            // 
            this.m_PtxRulesMenu.MenuItems.AddRange( new System.Windows.Forms.MenuItem[] {
            this.m_MenuLoadPtxRule,
            this.m_MenuAddNewPtxRule} );
            // 
            // m_MenuLoadPtxRule
            // 
            this.m_MenuLoadPtxRule.Index = 0;
            this.m_MenuLoadPtxRule.Text = "Load PtxRule";
            this.m_MenuLoadPtxRule.Click += new System.EventHandler( this.m_MenuLoadPtxRule_Click );
            // 
            // m_MenuAddNewPtxRule
            // 
            this.m_MenuAddNewPtxRule.Index = 1;
            this.m_MenuAddNewPtxRule.MenuItems.AddRange( new System.Windows.Forms.MenuItem[] {
            this.m_MenuAddNewSpriteRule,
            this.m_MenuAddNewModelRule} );
            this.m_MenuAddNewPtxRule.Text = "Create New PtxRule";
            // 
            // m_MenuAddNewSpriteRule
            // 
            this.m_MenuAddNewSpriteRule.Index = 0;
            this.m_MenuAddNewSpriteRule.Text = "Sprite...";
            this.m_MenuAddNewSpriteRule.Click += new System.EventHandler( this.m_MenuAddNewSpriteRule_Click );
            // 
            // m_MenuAddNewModelRule
            // 
            this.m_MenuAddNewModelRule.Index = 1;
            this.m_MenuAddNewModelRule.Text = "Model...";
            this.m_MenuAddNewModelRule.Click += new System.EventHandler( this.m_MenuItemNewModel_Activate );
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add( this.m_CheckboxOneShot );
            this.groupBox3.Controls.Add( this.m_SliderDuration );
            this.groupBox3.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.groupBox3.Location = new System.Drawing.Point( 14, 140 );
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size( 461, 76 );
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Info";
            // 
            // m_CheckboxOneShot
            // 
            this.m_CheckboxOneShot.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_CheckboxOneShot.Location = new System.Drawing.Point( 10, 18 );
            this.m_CheckboxOneShot.Name = "m_CheckboxOneShot";
            this.m_CheckboxOneShot.Size = new System.Drawing.Size( 104, 24 );
            this.m_CheckboxOneShot.TabIndex = 11;
            this.m_CheckboxOneShot.Text = "One Shot";
            // 
            // m_SliderDuration
            // 
            this.m_SliderDuration.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_SliderDuration.IsFloat = true;
            this.m_SliderDuration.Location = new System.Drawing.Point( 8, 46 );
            this.m_SliderDuration.Maximum = 9000F;
            this.m_SliderDuration.Minimum = 0F;
            this.m_SliderDuration.Name = "m_SliderDuration";
            this.m_SliderDuration.Size = new System.Drawing.Size( 208, 24 );
            this.m_SliderDuration.Step = 1F;
            this.m_SliderDuration.TabIndex = 10;
            this.m_SliderDuration.Text = "Duration";
            this.m_SliderDuration.UseTrackbar = false;
            this.m_SliderDuration.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.m_SliderDuration.ValueXPath = null;
            // 
            // groupBox2
            // 
            this.groupBox2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.groupBox2.Location = new System.Drawing.Point( 248, 24 );
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size( 224, 112 );
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Debugging";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add( this.m_LabelDrawTime );
            this.groupBox1.Controls.Add( this.m_LabelUpdateTime );
            this.groupBox1.Controls.Add( this.m_LabelCulledInstances );
            this.groupBox1.Controls.Add( this.m_LabelActiveInstances );
            this.groupBox1.Controls.Add( this.m_LabelTotalPoints );
            this.groupBox1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.groupBox1.Location = new System.Drawing.Point( 16, 24 );
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size( 222, 112 );
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Profiling";
            // 
            // m_LabelDrawTime
            // 
            this.m_LabelDrawTime.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_LabelDrawTime.Location = new System.Drawing.Point( 8, 88 );
            this.m_LabelDrawTime.Name = "m_LabelDrawTime";
            this.m_LabelDrawTime.Size = new System.Drawing.Size( 144, 16 );
            this.m_LabelDrawTime.TabIndex = 4;
            this.m_LabelDrawTime.Text = "DrawTime (cpu)";
            // 
            // m_LabelUpdateTime
            // 
            this.m_LabelUpdateTime.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_LabelUpdateTime.Location = new System.Drawing.Point( 8, 69 );
            this.m_LabelUpdateTime.Name = "m_LabelUpdateTime";
            this.m_LabelUpdateTime.Size = new System.Drawing.Size( 160, 16 );
            this.m_LabelUpdateTime.TabIndex = 3;
            this.m_LabelUpdateTime.Text = "Update Time (cpu)";
            // 
            // m_LabelCulledInstances
            // 
            this.m_LabelCulledInstances.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_LabelCulledInstances.Location = new System.Drawing.Point( 8, 50 );
            this.m_LabelCulledInstances.Name = "m_LabelCulledInstances";
            this.m_LabelCulledInstances.Size = new System.Drawing.Size( 144, 16 );
            this.m_LabelCulledInstances.TabIndex = 2;
            this.m_LabelCulledInstances.Text = "Culled Instances";
            // 
            // m_LabelActiveInstances
            // 
            this.m_LabelActiveInstances.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_LabelActiveInstances.Location = new System.Drawing.Point( 8, 34 );
            this.m_LabelActiveInstances.Name = "m_LabelActiveInstances";
            this.m_LabelActiveInstances.Size = new System.Drawing.Size( 144, 16 );
            this.m_LabelActiveInstances.TabIndex = 1;
            this.m_LabelActiveInstances.Text = "Active Instances";
            // 
            // m_LabelTotalPoints
            // 
            this.m_LabelTotalPoints.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_LabelTotalPoints.Location = new System.Drawing.Point( 8, 18 );
            this.m_LabelTotalPoints.Name = "m_LabelTotalPoints";
            this.m_LabelTotalPoints.Size = new System.Drawing.Size( 136, 16 );
            this.m_LabelTotalPoints.TabIndex = 0;
            this.m_LabelTotalPoints.Text = "Total Points: ";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler( this.UpdateProfiling );
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.label2.Location = new System.Drawing.Point( 16, 328 );
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size( 128, 14 );
            this.label2.TabIndex = 25;
            this.label2.Text = "Emitter Properties";
            // 
            // m_TabDomains
            // 
            this.m_TabDomains.Controls.Add( this.tabPage1 );
            this.m_TabDomains.Controls.Add( this.tabPage2 );
            this.m_TabDomains.Controls.Add( this.tabPage3 );
            this.m_TabDomains.Location = new System.Drawing.Point( 16, 352 );
            this.m_TabDomains.Name = "m_TabDomains";
            this.m_TabDomains.SelectedIndex = 0;
            this.m_TabDomains.Size = new System.Drawing.Size( 458, 296 );
            this.m_TabDomains.TabIndex = 28;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add( this.m_EmitDomainProp );
            this.tabPage1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.tabPage1.Location = new System.Drawing.Point( 4, 22 );
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size( 450, 270 );
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Emitter Domain";
            // 
            // m_EmitDomainProp
            // 
            this.m_EmitDomainProp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_EmitDomainProp.Location = new System.Drawing.Point( 0, 0 );
            this.m_EmitDomainProp.Name = "m_EmitDomainProp";
            this.m_EmitDomainProp.Size = new System.Drawing.Size( 450, 270 );
            this.m_EmitDomainProp.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add( this.m_VelDomainProp );
            this.tabPage2.Location = new System.Drawing.Point( 4, 22 );
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size( 450, 270 );
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Velocity Domain";
            // 
            // m_VelDomainProp
            // 
            this.m_VelDomainProp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_VelDomainProp.Location = new System.Drawing.Point( 0, 0 );
            this.m_VelDomainProp.Name = "m_VelDomainProp";
            this.m_VelDomainProp.Size = new System.Drawing.Size( 450, 270 );
            this.m_VelDomainProp.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add( this.m_ListEmissionKeyframes );
            this.tabPage3.Location = new System.Drawing.Point( 4, 22 );
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size( 450, 270 );
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Emission Keyframes";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem} );
            this.menuStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size( 480, 24 );
            this.menuStrip1.TabIndex = 29;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.reloadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAllToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.loadPtxRuleToolStripMenuItem,
            this.createNewPtxRuleToolStripMenuItem} );
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size( 35, 20 );
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // reloadToolStripMenuItem
            // 
            this.reloadToolStripMenuItem.Enabled = false;
            this.reloadToolStripMenuItem.Name = "reloadToolStripMenuItem";
            this.reloadToolStripMenuItem.Size = new System.Drawing.Size( 176, 22 );
            this.reloadToolStripMenuItem.Text = "&Reload";
            this.reloadToolStripMenuItem.Click += new System.EventHandler( this.m_MenuItemReload_Activate );
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size( 176, 22 );
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler( this.m_MenuItemSave_Activate );
            // 
            // saveAllToolStripMenuItem
            // 
            this.saveAllToolStripMenuItem.Name = "saveAllToolStripMenuItem";
            this.saveAllToolStripMenuItem.Size = new System.Drawing.Size( 176, 22 );
            this.saveAllToolStripMenuItem.Text = "&Save Al&l";
            this.saveAllToolStripMenuItem.Click += new System.EventHandler( this.m_MenuSaveAll_Activate );
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size( 176, 22 );
            this.saveAsToolStripMenuItem.Text = "Save &As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler( this.m_MenuItemSaveAs_Activate );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 173, 6 );
            // 
            // loadPtxRuleToolStripMenuItem
            // 
            this.loadPtxRuleToolStripMenuItem.Name = "loadPtxRuleToolStripMenuItem";
            this.loadPtxRuleToolStripMenuItem.Size = new System.Drawing.Size( 176, 22 );
            this.loadPtxRuleToolStripMenuItem.Text = "&Load Ptx Rule...";
            this.loadPtxRuleToolStripMenuItem.Click += new System.EventHandler( this.m_MenuLoadPtxRule_Click );
            // 
            // createNewPtxRuleToolStripMenuItem
            // 
            this.createNewPtxRuleToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.spriteToolStripMenuItem,
            this.modelToolStripMenuItem} );
            this.createNewPtxRuleToolStripMenuItem.Name = "createNewPtxRuleToolStripMenuItem";
            this.createNewPtxRuleToolStripMenuItem.Size = new System.Drawing.Size( 176, 22 );
            this.createNewPtxRuleToolStripMenuItem.Text = "&Create New Ptx Rule";
            // 
            // spriteToolStripMenuItem
            // 
            this.spriteToolStripMenuItem.Name = "spriteToolStripMenuItem";
            this.spriteToolStripMenuItem.Size = new System.Drawing.Size( 106, 22 );
            this.spriteToolStripMenuItem.Text = "Sp&rite";
            this.spriteToolStripMenuItem.Click += new System.EventHandler( this.m_MenuAddNewSpriteRule_Click );
            // 
            // modelToolStripMenuItem
            // 
            this.modelToolStripMenuItem.Name = "modelToolStripMenuItem";
            this.modelToolStripMenuItem.Size = new System.Drawing.Size( 106, 22 );
            this.modelToolStripMenuItem.Text = "&Model";
            this.modelToolStripMenuItem.Click += new System.EventHandler( this.m_MenuItemNewModel_Activate );
            // 
            // EmitterProperties
            // 
            this.Controls.Add( this.m_TabDomains );
            this.Controls.Add( this.label2 );
            this.Controls.Add( this.label6 );
            this.Controls.Add( this.m_ListPtxRules );
            this.Controls.Add( this.groupBox3 );
            this.Controls.Add( this.groupBox2 );
            this.Controls.Add( this.groupBox1 );
            this.Controls.Add( this.menuStrip1 );
            this.Name = "EmitterProperties";
            this.Size = new System.Drawing.Size( 480, 656 );
            this.groupBox3.ResumeLayout( false );
            this.groupBox1.ResumeLayout( false );
            this.m_TabDomains.ResumeLayout( false );
            this.tabPage1.ResumeLayout( false );
            this.tabPage2.ResumeLayout( false );
            this.tabPage3.ResumeLayout( false );
            this.menuStrip1.ResumeLayout( false );
            this.menuStrip1.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

		}
		#endregion
		
		public void SelectEmitter(string emittName)
		{
			if(emittName== m_EmitterName)
				return;
			m_EmitterName=emittName;
			FindWidgetHandles();
			if(m_ListPtxRules.Items.Count>0)
				if(MainPtxWindow.Instance!=null)
					MainPtxWindow.Instance.SelectPtxRule(m_ListPtxRules.Items[0].ToString());

		}

		void FindWidgetHandles()
		{
			RemoveBindings();

			WidgetGroup emitGroup =null;
			// TEST if(m_WidgetRoot==null)
				m_WidgetRoot = MainPtxWindow.Instance.m_WidgetEmitterList;

			if(m_WidgetRoot!=null)
				emitGroup = m_WidgetRoot.FindFirstWidgetFromPath(m_EmitterName) as WidgetGroup;
			
			if(emitGroup== null)
			{
				this.Enabled=false;
                m_DockControl.Text = m_DockControl.TabText = GetTabText();
				return;
			}

			this.Enabled = true;
            m_DockControl.Text = m_DockControl.TabText = GetTabText();

			m_WidgetEmitterName = emitGroup.FindFirstWidgetFromPath("Name") as WidgetText;
			m_WidgetSave = emitGroup.FindFirstWidgetFromPath("Save") as WidgetButton;
			m_WidgetSaveAll = emitGroup.FindFirstWidgetFromPath("SaveAll") as WidgetButton;
			//m_WidgetLoad = emitGroup.FindFirstWidgetFromPath("Load") as WidgetButton;
			
			m_WidgetLoadPtx = emitGroup.FindFirstWidgetFromPath("PtxRules/Load PtxRule") as WidgetButton;
			m_WidgetLoadPtxName = emitGroup.FindFirstWidgetFromPath("PtxRules/PtxRule Name:") as WidgetText;

			/*
			m_WidgetReload =			m_WidgetRoot.FindFirstWidgetFromPath("Load") as WidgetButton;
			m_WidgetRemovePtx =			m_WidgetRoot.FindFirstWidgetFromPath("PtxRules/Remove PtxRule") as WidgetButton;
			m_WidgetRemovePtxName =		m_WidgetRoot.FindWidgetsFromPath("PtxRules/PtxRule Name:")[1] as WidgetText;
			*/

			WidgetGroup info = emitGroup.FindFirstWidgetFromPath("Info") as WidgetGroup;
			ReferenceGroup(m_EmitterName+"/Info");
			if(info != null)
			{
				Bind(info,"Duration", m_SliderDuration);
				Bind(info,"One Shot", m_CheckboxOneShot, "Checked");
			}

			this.ReferenceGroup(m_EmitterName+"/Profiling");
			WidgetGroup profiling = emitGroup.FindFirstWidgetFromPath("Profiling") as WidgetGroup;
			if(profiling != null)
			{
				
				m_WidgetTotalPoints =		profiling.FindFirstWidgetFromPath("Total Points") as WidgetSliderInt;
				//m_WidgetActiveInstances =	profiling.FindFirstWidgetFromPath("Active Instances") as WidgetSliderInt;
				//m_WidgetCulledInstances =	profiling.FindFirstWidgetFromPath("Culled Instances") as WidgetSliderInt;
				m_WidgetUpdateCpu =			profiling.FindFirstWidgetFromPath("CPU Update Time (ms)") as WidgetSliderFloat;
				m_WidgetDrawCpu =			profiling.FindFirstWidgetFromPath("CPU Draw Time (ms)") as WidgetSliderFloat;
			}

			BuildEmitterData(emitGroup);
			BuildKeyframeList(emitGroup);
			BuildRuleList(emitGroup,false);
			
			/*
			m_WidgetGlobalNewPtxRule = m_BankManager.FindFirstWidgetFromPath("RMPTFX/FileIO/New PtxRule") as WidgetButton;
			m_WidgetGlobalNewPtxRuleType = m_BankManager.FindFirstWidgetFromPath("RMPTFX/FileIO/Rule Type") as WidgetCombo;
			m_WidgetGlobalNewPtxRuleName = m_BankManager.FindFirstWidgetFromPath("RMPTFX/FileIO/ptxRule Name") as WidgetText;
			m_CheckboxShowCullspheres.Enabled = m_CheckboxShowInstances.Checked;
			*/
		}


		private void BuildRuleList(WidgetGroup emitGroup, bool preserveSelection)
		{
			if(m_WidgetRoot == null) return;
			
			WidgetGroup rules = emitGroup.FindFirstWidgetFromPath("PtxRules") as WidgetGroup;
			if(rules == null) return;
			m_WidgetPtxRules = rules.FindFirstWidgetFromPath("Rules") as WidgetGroup;
			if(m_WidgetPtxRules == null) return;

			int prevIndex = m_ListPtxRules.SelectedIndex;

			m_ListPtxRules.BeginUpdate();
			m_ListPtxRules.Items.Clear();

			if (m_WidgetPtxRules != null) 
			{
				foreach(Widget w in m_WidgetPtxRules.List)
				{
					if (w is WidgetGroup)
					{
						m_ListPtxRules.Items.Add(w);
					}
				}
			}

			m_ListPtxRules.ClearSelected();
			m_ListPtxRules.EndUpdate();

			if (preserveSelection) 
			{
				m_ListPtxRules.SelectedIndex = prevIndex;
			}
			else 
			{
				m_ListPtxRules.SelectedIndex = -1;
			}
		}

		private void BuildEmitterData(WidgetGroup emitGroup)
		{
			if(m_WidgetRoot==null)
				return;
			WidgetGroup eD = emitGroup.FindFirstWidgetFromPath("Emitter Domain") as WidgetGroup;
			WidgetGroup vD = emitGroup.FindFirstWidgetFromPath("Velocity Domain") as WidgetGroup;

			
			if(eD != null)
				m_EmitDomainProp.BindToData(eD,this.m_EmitterName);
			else
				m_EmitDomainProp.BindToData(null,"");
			if(vD != null)
				m_VelDomainProp.BindToData(vD,this.m_EmitterName);
			else
				m_VelDomainProp.BindToData(null,"");
		}
		
		private void BuildKeyframeList(WidgetGroup emitGroup)
		{
			if(m_WidgetRoot == null)
				return;

			WidgetGroup keyframes = emitGroup.FindFirstWidgetFromPath("Keyframes") as WidgetGroup;
			if(keyframes == null)
				return;

			m_ListItems.Clear();
			m_ListEmissionKeyframes.BeginUpdate();
			m_ListEmissionKeyframes.Items.Clear();
			WidgetGroup Toggle = keyframes.FindFirstWidgetFromPath("Toggleable") as WidgetGroup;
			if(Toggle != null)
			{
				foreach(Widget w in Toggle.List) 
				{
					m_ListEmissionKeyframes.Items.Add(w);
					ListItemData d = new ListItemData();
					d.m_Name = w.ToString();
					d.m_Color = Color.FromArgb(255,0,0,0);
					m_ListItems.Add(d);
				}
			}

			WidgetGroup OverLife = keyframes.FindFirstWidgetFromPath("OverLife") as WidgetGroup;
			if(OverLife != null)
			{
				foreach(Widget w in OverLife.List) 
				{
					m_ListEmissionKeyframes.Items.Add(w);
					ListItemData d = new ListItemData();
					d.m_Name = w.ToString();
					d.m_Color = Color.FromArgb(255,255,100,0);
					m_ListItems.Add(d);
				}
			}

			WidgetGroup OverTime = keyframes.FindFirstWidgetFromPath("OverTime") as WidgetGroup;
			if(OverTime != null)
			{
				foreach(Widget w in OverTime.List) 
				{
					m_ListEmissionKeyframes.Items.Add(w);
					ListItemData d = new ListItemData();
					d.m_Name = w.ToString();
					d.m_Color = Color.FromArgb(255,0,100,255);
					m_ListItems.Add(d);
				}
			}
			m_ListEmissionKeyframes.EndUpdate();
		
		}

		private void UpdateProfiling(object sender, System.EventArgs e)
		{
			SetLabel(m_LabelTotalPoints, "Total Points: {0}", m_WidgetTotalPoints);
			SetLabel(m_LabelUpdateTime, "Update Time (cpu): {0:#0.000}", m_WidgetUpdateCpu);
			SetLabel(m_LabelDrawTime, "Draw Time (cpu): {0:#0.000}", m_WidgetDrawCpu);
		}


		private void m_MenuItemReload_Activate(object sender, System.EventArgs e)
		{
			/*
			m_WidgetReload.Activate();
			ViewPlugIn.Instance.RebuildAllPanels();
			*/
		}

		private void m_MenuItemSave_Activate(object sender, System.EventArgs e)
		{
			m_WidgetSave.Activate();
		}


		private void m_MenuItemNewModel_Activate(object sender, System.EventArgs e)
		{
			string ptxname = MainPtxWindow.Instance.NewPtxModelRule();
			if(ptxname.Length>0)
			{
				m_WidgetLoadPtxName.String=ptxname;
				m_WidgetLoadPtx.Activate();
			}
		}

		public class StateData {
			public string m_WidgetRootPath;
			public string m_CurrRule;
		}

		public static StateData sm_SavedStateData;

		public void SaveEmitterAs()
		{
			SaveFileDialog sfd = new SaveFileDialog();
			if(UserData.LastEmitterPath.Length>0)
				sfd.InitialDirectory = UserData.LastEmitterPath;
			sfd.AddExtension=true;
			sfd.Filter="PtxEmitter (*.emitrule)|*.emitrule";
			sfd.ValidateNames=true;
			sfd.OverwritePrompt=true;
			sfd.Title="Save As:";
			if (sfd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastEmitterPath = System.IO.Path.GetDirectoryName(sfd.FileName);
				string oldname = m_WidgetEmitterName.String;
				string newname = System.IO.Path.GetFileNameWithoutExtension(sfd.FileName);
				m_WidgetEmitterName.String = newname;
				m_WidgetSaveAll.Activate();
				m_WidgetEmitterName.String=oldname;
			}

		}
		public override object GetStateData()
		{
			StateData ret = new StateData();
			return ret;
		}

		public override void SetStateData(object d)
		{
			m_WidgetRoot=null;
            m_EmitterName = "";
			FindWidgetHandles();
		}

		private void m_ListEmissionKeyframes_DoubleClick(object sender, System.EventArgs e)
		{
			if (m_ListEmissionKeyframes.SelectedItem is WidgetGroup)
			{
                ViewPlugIn.Instance.CreateAndOpenKeyframeEditorView( m_ListEmissionKeyframes.SelectedItem as WidgetGroup, "EmitRule: " + m_EmitterName + "/" );
			}
		}

		private void m_ItemDeletePtxRule_Popup(object sender, System.EventArgs e)
		{
			
		}


		private void m_MenuSaveAll_Activate(object sender, System.EventArgs e)
		{
			m_WidgetSaveAll.Activate();
		}

		private void m_ListPtxRules_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(m_ListPtxRules.SelectedItem==null)
				return;
			if(MainPtxWindow.Instance!=null)
				MainPtxWindow.Instance.SelectPtxRule(m_ListPtxRules.SelectedItem.ToString());
		}

		private void m_MenuItemSaveAs_Activate(object sender, System.EventArgs e)
		{
			SaveEmitterAs();
		}

		private void m_MenuLoadPtxRule_Click(object sender, System.EventArgs e)
		{
			string ptxname = MainPtxWindow.Instance.LoadPtxRule("Select a PtxRule");
			if(ptxname.Length>0)
			{
				m_WidgetLoadPtxName.String = ptxname;
				m_WidgetLoadPtx.Activate();
			}
		}

		private void m_MenuAddNewSpriteRule_Click(object sender, System.EventArgs e)
		{
			string ptxname = MainPtxWindow.Instance.NewPtxSpriteRule();
			if(ptxname.Length>0)
			{
				m_WidgetLoadPtxName.String=ptxname;
				m_WidgetLoadPtx.Activate();
			}
		}
		private void m_ListEmissionKeyframes_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
		{
			ListItemData data = (ListItemData)m_ListItems[e.Index];
			e.DrawBackground(); 
			e.DrawFocusRectangle();
			Color c = data.m_Color;
			e.Graphics.DrawString(data.m_Name,new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Regular),new SolidBrush(c),e.Bounds); 

		}

	}
}
