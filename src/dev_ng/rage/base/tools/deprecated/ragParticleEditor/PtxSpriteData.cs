using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using ragCore;
using ragWidgets;
using ragUi;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for PtxSpriteData.
	/// </summary>
	public class PtxSpriteData : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button m_ChooseFileButton;
		private System.Windows.Forms.TextBox m_TextTextureName;
		public ragUi.ControlSlider m_SliderRandomEnd;
		public ragUi.ControlSlider m_SliderRandomStart;
		public ragUi.ControlSlider m_SliderTextureTilesY;
		public ragUi.ControlSlider m_SliderTextureTilesX;
		private System.Windows.Forms.Label label1;
		public System.Windows.Forms.CheckBox m_Volumetric;
		public ragUi.ControlSlider m_albedo;
		public ragUi.ControlSlider m_opacity;
		public System.Windows.Forms.CheckBox m_ButtonUseRandomColor;
		public System.Windows.Forms.CheckBox m_DrawDirectional;
		private System.Windows.Forms.GroupBox groupBox1;
		public ragUi.ControlSlider m_SliderAnimFrameEnd;
		public System.Windows.Forms.CheckBox m_AnimateRandom;
		public System.Windows.Forms.CheckBox m_AnimateTextures;
		public System.Windows.Forms.CheckBox m_HoldLastFrame;
		public System.Windows.Forms.CheckBox m_CheckScaleAnimRateByLife;
        public CheckBox m_AnchorToSpawnPos;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PtxSpriteData()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		public WidgetButton		m_WidgetLoadTexture;
		private	ArrayList		m_Bindings= new ArrayList();
		//private string			m_LastTexturePath="";


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label4 = new System.Windows.Forms.Label();
            this.m_ChooseFileButton = new System.Windows.Forms.Button();
            this.m_TextTextureName = new System.Windows.Forms.TextBox();
            this.m_SliderRandomEnd = new ragUi.ControlSlider();
            this.m_SliderRandomStart = new ragUi.ControlSlider();
            this.m_SliderTextureTilesY = new ragUi.ControlSlider();
            this.m_SliderTextureTilesX = new ragUi.ControlSlider();
            this.label1 = new System.Windows.Forms.Label();
            this.m_Volumetric = new System.Windows.Forms.CheckBox();
            this.m_albedo = new ragUi.ControlSlider();
            this.m_opacity = new ragUi.ControlSlider();
            this.m_ButtonUseRandomColor = new System.Windows.Forms.CheckBox();
            this.m_DrawDirectional = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_CheckScaleAnimRateByLife = new System.Windows.Forms.CheckBox();
            this.m_SliderAnimFrameEnd = new ragUi.ControlSlider();
            this.m_AnimateRandom = new System.Windows.Forms.CheckBox();
            this.m_AnimateTextures = new System.Windows.Forms.CheckBox();
            this.m_HoldLastFrame = new System.Windows.Forms.CheckBox();
            this.m_AnchorToSpawnPos = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(264, 36);
            this.label4.TabIndex = 15;
            this.label4.Text = "Note: Texture is now adjusted from Tune Shader Vars";
            // 
            // m_ChooseFileButton
            // 
            this.m_ChooseFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ChooseFileButton.Location = new System.Drawing.Point(208, 21);
            this.m_ChooseFileButton.Name = "m_ChooseFileButton";
            this.m_ChooseFileButton.Size = new System.Drawing.Size(64, 23);
            this.m_ChooseFileButton.TabIndex = 14;
            this.m_ChooseFileButton.Text = "Browse...";
            this.m_ChooseFileButton.Visible = false;
            this.m_ChooseFileButton.Click += new System.EventHandler(this.ChooseTexture);
            // 
            // m_TextTextureName
            // 
            this.m_TextTextureName.AcceptsReturn = true;
            this.m_TextTextureName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_TextTextureName.Location = new System.Drawing.Point(8, 24);
            this.m_TextTextureName.Name = "m_TextTextureName";
            this.m_TextTextureName.Size = new System.Drawing.Size(192, 20);
            this.m_TextTextureName.TabIndex = 13;
            this.m_TextTextureName.Visible = false;
            this.m_TextTextureName.TextChanged += new System.EventHandler(this.ChangeTextureName);
            // 
            // m_SliderRandomEnd
            // 
            this.m_SliderRandomEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRandomEnd.IsFloat = false;
            this.m_SliderRandomEnd.Location = new System.Drawing.Point(8, 153);
            this.m_SliderRandomEnd.Maximum = 100F;
            this.m_SliderRandomEnd.Minimum = 0F;
            this.m_SliderRandomEnd.Name = "m_SliderRandomEnd";
            this.m_SliderRandomEnd.Size = new System.Drawing.Size(208, 24);
            this.m_SliderRandomEnd.Step = 1F;
            this.m_SliderRandomEnd.TabIndex = 12;
            this.m_SliderRandomEnd.Text = "Max";
            this.m_SliderRandomEnd.UseTrackbar = false;
            this.m_SliderRandomEnd.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderRandomEnd.ValueXPath = null;
            this.m_SliderRandomEnd.ValueChanged += new System.EventHandler(this.TileValueChanged);
            // 
            // m_SliderRandomStart
            // 
            this.m_SliderRandomStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderRandomStart.IsFloat = false;
            this.m_SliderRandomStart.Location = new System.Drawing.Point(8, 129);
            this.m_SliderRandomStart.Maximum = 100F;
            this.m_SliderRandomStart.Minimum = 0F;
            this.m_SliderRandomStart.Name = "m_SliderRandomStart";
            this.m_SliderRandomStart.Size = new System.Drawing.Size(208, 24);
            this.m_SliderRandomStart.Step = 1F;
            this.m_SliderRandomStart.TabIndex = 11;
            this.m_SliderRandomStart.Text = "Min";
            this.m_SliderRandomStart.UseTrackbar = false;
            this.m_SliderRandomStart.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderRandomStart.ValueXPath = null;
            this.m_SliderRandomStart.ValueChanged += new System.EventHandler(this.TileValueChanged);
            // 
            // m_SliderTextureTilesY
            // 
            this.m_SliderTextureTilesY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderTextureTilesY.IsFloat = false;
            this.m_SliderTextureTilesY.Location = new System.Drawing.Point(8, 80);
            this.m_SliderTextureTilesY.Maximum = 100F;
            this.m_SliderTextureTilesY.Minimum = 1F;
            this.m_SliderTextureTilesY.Name = "m_SliderTextureTilesY";
            this.m_SliderTextureTilesY.Size = new System.Drawing.Size(208, 24);
            this.m_SliderTextureTilesY.Step = 1F;
            this.m_SliderTextureTilesY.TabIndex = 10;
            this.m_SliderTextureTilesY.Text = "Frames Height";
            this.m_SliderTextureTilesY.UseTrackbar = false;
            this.m_SliderTextureTilesY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.m_SliderTextureTilesY.ValueXPath = null;
            this.m_SliderTextureTilesY.ValueChanged += new System.EventHandler(this.TileValueChanged);
            // 
            // m_SliderTextureTilesX
            // 
            this.m_SliderTextureTilesX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderTextureTilesX.IsFloat = false;
            this.m_SliderTextureTilesX.Location = new System.Drawing.Point(8, 56);
            this.m_SliderTextureTilesX.Maximum = 100F;
            this.m_SliderTextureTilesX.Minimum = 1F;
            this.m_SliderTextureTilesX.Name = "m_SliderTextureTilesX";
            this.m_SliderTextureTilesX.Size = new System.Drawing.Size(208, 24);
            this.m_SliderTextureTilesX.Step = 1F;
            this.m_SliderTextureTilesX.TabIndex = 9;
            this.m_SliderTextureTilesX.Text = "Frames Width";
            this.m_SliderTextureTilesX.UseTrackbar = false;
            this.m_SliderTextureTilesX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.m_SliderTextureTilesX.ValueXPath = null;
            this.m_SliderTextureTilesX.ValueChanged += new System.EventHandler(this.TileValueChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 23);
            this.label1.TabIndex = 19;
            this.label1.Text = "Initial Random Frame";
            // 
            // m_Volumetric
            // 
            this.m_Volumetric.Location = new System.Drawing.Point(8, 347);
            this.m_Volumetric.Name = "m_Volumetric";
            this.m_Volumetric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.m_Volumetric.Size = new System.Drawing.Size(192, 16);
            this.m_Volumetric.TabIndex = 22;
            this.m_Volumetric.Text = "Volumetric Self Shadowing";
            this.m_Volumetric.Visible = false;
            // 
            // m_albedo
            // 
            this.m_albedo.IsFloat = true;
            this.m_albedo.Location = new System.Drawing.Point(0, 363);
            this.m_albedo.Maximum = 1F;
            this.m_albedo.Minimum = 0F;
            this.m_albedo.Name = "m_albedo";
            this.m_albedo.Size = new System.Drawing.Size(288, 24);
            this.m_albedo.Step = 0.0001F;
            this.m_albedo.TabIndex = 23;
            this.m_albedo.Text = "Albedo ";
            this.m_albedo.UseTrackbar = true;
            this.m_albedo.Value = new decimal(new int[] {
            95,
            0,
            0,
            131072});
            this.m_albedo.ValueXPath = null;
            this.m_albedo.Visible = false;
            // 
            // m_opacity
            // 
            this.m_opacity.IsFloat = true;
            this.m_opacity.Location = new System.Drawing.Point(0, 387);
            this.m_opacity.Maximum = 2F;
            this.m_opacity.Minimum = 0F;
            this.m_opacity.Name = "m_opacity";
            this.m_opacity.Size = new System.Drawing.Size(288, 24);
            this.m_opacity.Step = 0.001F;
            this.m_opacity.TabIndex = 24;
            this.m_opacity.Text = "opacity";
            this.m_opacity.UseTrackbar = true;
            this.m_opacity.Value = new decimal(new int[] {
            15,
            0,
            0,
            131072});
            this.m_opacity.ValueXPath = null;
            this.m_opacity.Visible = false;
            // 
            // m_ButtonUseRandomColor
            // 
            this.m_ButtonUseRandomColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonUseRandomColor.Location = new System.Drawing.Point(13, 311);
            this.m_ButtonUseRandomColor.Name = "m_ButtonUseRandomColor";
            this.m_ButtonUseRandomColor.Size = new System.Drawing.Size(144, 16);
            this.m_ButtonUseRandomColor.TabIndex = 28;
            this.m_ButtonUseRandomColor.Text = "Use Random Color";
            // 
            // m_DrawDirectional
            // 
            this.m_DrawDirectional.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_DrawDirectional.Location = new System.Drawing.Point(13, 327);
            this.m_DrawDirectional.Name = "m_DrawDirectional";
            this.m_DrawDirectional.Size = new System.Drawing.Size(112, 18);
            this.m_DrawDirectional.TabIndex = 27;
            this.m_DrawDirectional.Text = "Draw Directional";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.m_CheckScaleAnimRateByLife);
            this.groupBox1.Controls.Add(this.m_SliderAnimFrameEnd);
            this.groupBox1.Controls.Add(this.m_AnimateRandom);
            this.groupBox1.Controls.Add(this.m_AnimateTextures);
            this.groupBox1.Controls.Add(this.m_HoldLastFrame);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 184);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(280, 120);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Texture Animation";
            // 
            // m_CheckScaleAnimRateByLife
            // 
            this.m_CheckScaleAnimRateByLife.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CheckScaleAnimRateByLife.Location = new System.Drawing.Point(132, 16);
            this.m_CheckScaleAnimRateByLife.Name = "m_CheckScaleAnimRateByLife";
            this.m_CheckScaleAnimRateByLife.Size = new System.Drawing.Size(140, 24);
            this.m_CheckScaleAnimRateByLife.TabIndex = 31;
            this.m_CheckScaleAnimRateByLife.Text = "Scale Anim Rate by life";
            // 
            // m_SliderAnimFrameEnd
            // 
            this.m_SliderAnimFrameEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderAnimFrameEnd.IsFloat = false;
            this.m_SliderAnimFrameEnd.Location = new System.Drawing.Point(8, 40);
            this.m_SliderAnimFrameEnd.Maximum = 100F;
            this.m_SliderAnimFrameEnd.Minimum = 0F;
            this.m_SliderAnimFrameEnd.Name = "m_SliderAnimFrameEnd";
            this.m_SliderAnimFrameEnd.Size = new System.Drawing.Size(208, 24);
            this.m_SliderAnimFrameEnd.Step = 1F;
            this.m_SliderAnimFrameEnd.TabIndex = 29;
            this.m_SliderAnimFrameEnd.Text = "Last Frame";
            this.m_SliderAnimFrameEnd.UseTrackbar = false;
            this.m_SliderAnimFrameEnd.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderAnimFrameEnd.ValueXPath = null;
            // 
            // m_AnimateRandom
            // 
            this.m_AnimateRandom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_AnimateRandom.Location = new System.Drawing.Point(10, 88);
            this.m_AnimateRandom.Name = "m_AnimateRandom";
            this.m_AnimateRandom.Size = new System.Drawing.Size(112, 24);
            this.m_AnimateRandom.TabIndex = 28;
            this.m_AnimateRandom.Text = "Animate Random";
            // 
            // m_AnimateTextures
            // 
            this.m_AnimateTextures.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_AnimateTextures.Location = new System.Drawing.Point(12, 16);
            this.m_AnimateTextures.Name = "m_AnimateTextures";
            this.m_AnimateTextures.Size = new System.Drawing.Size(136, 24);
            this.m_AnimateTextures.TabIndex = 27;
            this.m_AnimateTextures.Text = "Texture Animation";
            // 
            // m_HoldLastFrame
            // 
            this.m_HoldLastFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_HoldLastFrame.Location = new System.Drawing.Point(11, 64);
            this.m_HoldLastFrame.Name = "m_HoldLastFrame";
            this.m_HoldLastFrame.Size = new System.Drawing.Size(112, 24);
            this.m_HoldLastFrame.TabIndex = 30;
            this.m_HoldLastFrame.Text = "Hold Last Frame";
            // 
            // m_AnchorToSpawnPos
            // 
            this.m_AnchorToSpawnPos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_AnchorToSpawnPos.Location = new System.Drawing.Point(135, 327);
            this.m_AnchorToSpawnPos.Name = "m_AnchorToSpawnPos";
            this.m_AnchorToSpawnPos.Size = new System.Drawing.Size(137, 18);
            this.m_AnchorToSpawnPos.TabIndex = 30;
            this.m_AnchorToSpawnPos.Text = "Anchor Spawn pos";
            // 
            // PtxSpriteData
            // 
            this.Controls.Add(this.m_AnchorToSpawnPos);
            this.Controls.Add(this.m_ButtonUseRandomColor);
            this.Controls.Add(this.m_DrawDirectional);
            this.Controls.Add(this.m_opacity);
            this.Controls.Add(this.m_albedo);
            this.Controls.Add(this.m_Volumetric);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_ChooseFileButton);
            this.Controls.Add(this.m_TextTextureName);
            this.Controls.Add(this.m_SliderRandomEnd);
            this.Controls.Add(this.m_SliderRandomStart);
            this.Controls.Add(this.m_SliderTextureTilesY);
            this.Controls.Add(this.m_SliderTextureTilesX);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "PtxSpriteData";
            this.Size = new System.Drawing.Size(286, 392);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		public void BindToData(WidgetGroup group)
		{
			RemoveBindings();
			m_WidgetLoadTexture = group.FindFirstWidgetFromPath("Info/LoadTexture") as WidgetButton;
			Bind(group.FindFirstWidgetFromPath("Info/TextureName"),m_TextTextureName,"Text");

			Bind(group.FindFirstWidgetFromPath("Info/NumTextureTilesX"), m_SliderTextureTilesX);
			Bind(group.FindFirstWidgetFromPath("Info/NumTextureTilesY"), m_SliderTextureTilesY);
			Bind(group.FindFirstWidgetFromPath("Info/StartTextureTile"), m_SliderRandomStart);
			Bind(group.FindFirstWidgetFromPath("Info/EndTextureTile"), m_SliderRandomEnd);
			Bind(group.FindFirstWidgetFromPath("Info/EndAnimTextureTile"), m_SliderAnimFrameEnd);

			Bind(group.FindFirstWidgetFromPath("Info/DrawDirectional"),m_DrawDirectional,"Checked");
            Bind(group.FindFirstWidgetFromPath("Info/Anchor to Spawn Pos"), m_AnchorToSpawnPos, "Checked");
			Bind(group.FindFirstWidgetFromPath("Info/AnimateTextures"), m_AnimateTextures,"Checked");
			Bind(group.FindFirstWidgetFromPath("Info/Animate Random"),  m_AnimateRandom,"Checked");
			Bind(group.FindFirstWidgetFromPath("Info/Hold Last Frame"), m_HoldLastFrame,"Checked");
			Bind(group.FindFirstWidgetFromPath("Info/ScaleByParticleLife"), m_CheckScaleAnimRateByLife,"Checked");
			Bind(group.FindFirstWidgetFromPath("Info/UseRandomColor"), m_ButtonUseRandomColor,"Checked");
		}
		private void RemoveBindings()
		{
			foreach(WidgetBinding b in m_Bindings)
			{
				b.DisconnectEvents();
			}
			m_Bindings.Clear();
		}

		private void Bind(Widget w, System.Windows.Forms.Control ctrl, string controlProp)
		{
			WidgetBinding bind = ragWidgets.WidgetBinding.Bind(w,ctrl,controlProp);
			if(bind != null)
				m_Bindings.Add(bind);
		}
		private void Bind(Widget w,ControlBase ctrl,string controlProp)
		{
			WidgetBinding bind = ragWidgets.WidgetBinding.Bind(w,ctrl,controlProp);
			if(bind != null)
				m_Bindings.Add(bind);
		}
		private void Bind(Widget w,ControlBase ctrl)
		{
			WidgetBinding bind = ragWidgets.WidgetBinding.Bind(w,ctrl);
			if(bind != null)
				m_Bindings.Add(bind);
		}
		private void UpdateLayout()
		{
			m_AnimateRandom.Enabled = m_AnimateTextures.Checked;
			m_SliderAnimFrameEnd.Enabled = m_AnimateTextures.Checked;
			m_HoldLastFrame.Enabled = m_AnimateTextures.Checked;
		}
		
		private void ChooseTexture(object sender, System.EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			if(UserData.LastTexturePath.Length>0)
				ofd.InitialDirectory = UserData.LastTexturePath;

			if (ofd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastTexturePath = System.IO.Path.GetDirectoryName(ofd.FileName);
				m_TextTextureName.Text = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);
				m_WidgetLoadTexture.Activate();
			}
		}

		private void ChangeTextureName(object sender, System.EventArgs e)
		{
			m_WidgetLoadTexture.Activate();
		}

		private void TileValueChanged(object sender, System.EventArgs e)
		{
			float realMax = (float)(m_SliderTextureTilesX.Value * m_SliderTextureTilesY.Value) - 1.0f;

			m_SliderRandomStart.Minimum = 0;
			m_SliderRandomStart.Maximum = (float)m_SliderRandomEnd.Value;

			m_SliderRandomEnd.Minimum = (float)m_SliderRandomStart.Value;
			m_SliderRandomEnd.Maximum = realMax;

			m_SliderAnimFrameEnd.Minimum=0;
			m_SliderAnimFrameEnd.Maximum = realMax;
		}

		private void m_AnimateTextures_CheckedChanged(object sender, System.EventArgs e)
		{
			UpdateLayout();
		}

	}
}
