using System;
using System.Collections.Generic;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for UserData.
	/// </summary>
	public class UserData
	{
		public UserData()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		static public void InitPlugIn(ragCore.PlugInHostData hostData)
		{
			sm_Instance = new UserData();
			IOData = hostData.DllInfo.PluginDataHash;
		}
        
        static public void RemovePlugIn()
        {
            sm_Instance = null;
        }

		public static string LastEffectPath
		{
			get
			{
                string path;
                if ( IOData.TryGetValue( "m_LastEffectPath", out path) )
                {
                    return path;
                }
                return "";
			}
			set
			{
				IOData["m_LastEffectPath"] = value;
			}
		}
		public static string LastEmitterPath
		{
			get
			{
                string path;
                if ( IOData.TryGetValue( "m_LastEmitterPath", out path ) )
                {
                    return path;
                }
                return "";
			}
			set
			{
				IOData["m_LastEmitterPath"] = value;
			}
		}
		public static string LastTexturePath
		{
			get
			{
                string path;
                if ( IOData.TryGetValue( "m_LastTexturePath", out path ) )
                {
                    return path;
                }
                return "";
			}
			set
			{
				IOData["m_LastTexturePath"] = value;
			}
		}
		public static string LastModelPath
		{
			get
			{
                string path;
                if ( IOData.TryGetValue( "m_LastModelPath", out path ) )
                {
                    return path;
                }
                return "";
			}
			set
			{
				IOData["m_LastModelPath"] = value;
			}
		}
		public static string LastPtxRulePath
		{
			get
			{
                string path;
                if ( IOData.TryGetValue( "m_LastPtxRulePath", out path ) )
                {
                    return path;
                }
                return "";
			}
			set
			{
				IOData["m_LastPtxRulePath"] = value;
			}
		}

		public static string LastDomainPath
		{
			get
			{
                string path;
                if ( IOData.TryGetValue( "m_LastDomainPath", out path ) )
                {
                    return path;
                }
                return "";
			}
			set
			{
				IOData["m_LastDomainPath"] = value;
			}
		}

		public static Dictionary<string,string> IOData
		{
			get
			{
				return sm_Instance.m_IOData;
			}
			set
			{
				sm_Instance.m_IOData = value;
			}
		}

        protected Dictionary<string, string> m_IOData;
		protected static UserData sm_Instance;
	}
}
