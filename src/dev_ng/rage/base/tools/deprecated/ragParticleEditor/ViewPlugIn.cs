using System;
using System.Collections.Generic;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for ViewPlugIn.
	/// </summary>
	public class ViewPlugIn : ragWidgetPage.WidgetPagePlugIn
    {
        #region WidgetPagePlugIn overrides
        public override string Name
		{
			get
			{
				return "Particle Editor";
			}
		}

		public override void InitPlugIn(ragCore.PlugInHostData hostData)
		{
            sm_Instance = this;
			base.InitPlugIn(hostData);
			UserData.InitPlugIn(hostData);

            base.m_pageNameToCreateDelMap[MainPtxWindow.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( MainPtxWindow.CreatePage );
            base.m_pageNameToCreateDelMap[PlayControl.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( PlayControl.CreatePage );
            base.m_pageNameToCreateDelMap[EffectProperties.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( EffectProperties.CreatePage );
            base.m_pageNameToCreateDelMap[EmitterProperties.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( EmitterProperties.CreatePage );
            base.m_pageNameToCreateDelMap[PtxRuleProperties.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( PtxRuleProperties.CreatePage );
            base.m_pageNameToCreateDelMap[ptxShader.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel(ptxShader.CreateView);
            base.m_pageNameToCreateDelMap[KeyframeEditor.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel(KeyframeEditor.CreateView);
		}

        public override void RemovePlugIn()
        {
            base.RemovePlugIn();
            UserData.RemovePlugIn();

            MainPtxWindow.Shutdown();
            PlayControl.Shutdown();
            EffectProperties.Shutdown();
            EmitterProperties.Shutdown();
            PtxRuleProperties.Shutdown(); 
            ptxShader.Shutdown();
            KeyframeEditor.Shutdown();

            sm_Instance = null;
        }

        protected override void MainBankManager_InitFinished()
        {
            if ( CheckPluginStatus() )
            {
                MainPtxWindow.Init();
                PlayControl.Init();
                EffectProperties.Init();
                EmitterProperties.Init();
                PtxRuleProperties.Init();
                //ptxShader.Init();
            }
            else
            {
                base.MainBankManager_InitFinished();
            }
        }

        public override bool CheckPluginStatus()
		{
			return sm_HostData.MainRageApplication.BankMgr.FindFirstWidgetFromPath("rage - Rmptfx") != null;
		}

        public override void ActivatePlugInMenuItem( object sender, EventArgs args )
        {
            // Create the windows if they don't already exist
            if ( MainPtxWindow.sm_Instance == null )
            {
                base.CreateAndOpenView( base.m_pageNameToCreateDelMap[MainPtxWindow.MyName] );
            }
            else if ( !MainPtxWindow.sm_Instance.DockControl.IsOpen )
            {
                MainPtxWindow.sm_Instance.DockControl.Open();
            }

            if ( PlayControl.sm_Instance == null )
            {
                base.CreateAndOpenView( base.m_pageNameToCreateDelMap[PlayControl.MyName] );
            }
            else if ( !PlayControl.sm_Instance.DockControl.IsOpen )
            {
                PlayControl.sm_Instance.DockControl.Open();
            }

            if ( EffectProperties.sm_Instance == null )
            {
                base.CreateAndOpenView( base.m_pageNameToCreateDelMap[EffectProperties.MyName] );
            }
            else if ( !EffectProperties.sm_Instance.DockControl.IsOpen )
            {
                EffectProperties.sm_Instance.DockControl.Open();
            }

            if ( EmitterProperties.sm_Instance == null )
            {
                base.CreateAndOpenView( base.m_pageNameToCreateDelMap[EmitterProperties.MyName] );
            }
            else if ( !EmitterProperties.sm_Instance.DockControl.IsOpen )
            {
                EmitterProperties.sm_Instance.DockControl.Open();
            }

            if ( PtxRuleProperties.sm_Instance == null )
            {
                base.CreateAndOpenView( base.m_pageNameToCreateDelMap[PtxRuleProperties.MyName] );
            }
            else if ( !PtxRuleProperties.sm_Instance.DockControl.IsOpen )
            {
                PtxRuleProperties.sm_Instance.DockControl.Open();
            }

            //if (ptxShader.sm_Instance == null)
            //{
            //    base.CreateAndOpenView(base.m_pageNameToCreateDelMap[ptxShader.MyName]);
            //}
            //else if (!ptxShader.sm_Instance.DockControl.IsOpen)
            //{
            //    ptxShader.sm_Instance.DockControl.Open();
            //}
        }

		public override void RebuildAllPanels()
		{
			foreach(KeyframeEditor key in KeyframeEditor.sm_KeyframeEditors)
			{
				key.m_SavedStateData = key.GetStateData();
			}

            foreach (ptxShader shader in ptxShader.sm_Editors)
            {
                shader.m_SavedStateData = shader.GetStateData();
            }

			base.RebuildAllPanels();
		}

		public override void RebuildAfterWidgetUpdate(object sender, EventArgs e)
		{
			base.RebuildAfterWidgetUpdate(sender, e);
			foreach(KeyframeEditor key in KeyframeEditor.sm_KeyframeEditors)
			{
				key.SetStateData(key.m_SavedStateData);
				key.m_SavedStateData = null;
			}

            foreach (ptxShader shader in ptxShader.sm_Editors)
            {
                shader.SetStateData(shader.m_SavedStateData);
                shader.m_SavedStateData = null;
            }
        }        
        #endregion

        public static ViewPlugIn Instance
        {
            get
            {
                return sm_Instance;
            }
        }

        protected static ViewPlugIn sm_Instance;

        /// <summary>
        /// If the MainWindow has been created (it's probably hiding), open it.  Otherwise, create and open it.
        /// </summary>
        public void CreateAndOpenMainWindowView()
        {
            if ( MainPtxWindow.sm_Instance == null )
            {
                base.CreateAndOpenView( base.m_pageNameToCreateDelMap[MainPtxWindow.MyName] );
            }
            else if ( !MainPtxWindow.sm_Instance.DockControl.IsOpen )
            {
                MainPtxWindow.sm_Instance.DockControl.Open();
            }
        }

        /// <summary>
        /// If the KeyframeEditor we want to open is already created, open its window.  Otherwise, create a new one and open it.
        /// </summary>
        /// <param name="widgetRoot"></param>
        /// <param name="parentName"></param>
        /// <returns></returns>
        public KeyframeEditor CreateAndOpenKeyframeEditorView( ragCore.WidgetGroup widgetRoot, String parentName )
        {
            KeyframeEditor editor = KeyframeEditor.FindKeyframeEditor( widgetRoot, parentName );
            if ( editor != null )
            {
                // it's already open so just activate it
                editor.DockControl.Open( TD.SandDock.WindowOpenMethod.OnScreenActivate );
                return editor;
            }

            // Create and open it manually so we can call its Init function
            ragCore.IDockableView view = CreateView( m_pageNameToCreateDelMap[KeyframeEditor.MyName] );
            if ( OpenView( view.DockControl, view.InitiallyFloating, view.InitialDockLocation ) )
            {
                editor = view as KeyframeEditor;
                editor.Init( widgetRoot, parentName );

                PostOpenView( view.DockControl, view );
                return editor;
            }

            return null;
        }
        public ptxShader CreateAndOpenPtxShaderEditorView(String ptxName, String shaderName)
        {
            ptxShader editor = ptxShader.FindEditor(ptxName, shaderName);
            if (editor != null)
            {
                // it's already open so just activate it
                editor.DockControl.Open(TD.SandDock.WindowOpenMethod.OnScreenActivate);
                return editor;
            }

            // Create and open it manually so we can call its Init function
            ragCore.IDockableView view = CreateView(m_pageNameToCreateDelMap[ptxShader.MyName]);
            if (OpenView(view.DockControl, view.InitiallyFloating, view.InitialDockLocation))
            {
                editor = view as ptxShader;
                editor.SelectPtxRule(ptxName);
                editor.SelectShader(shaderName);

                PostOpenView(view.DockControl, view);
                return editor;
            }
            return null;
        }
    }
}
