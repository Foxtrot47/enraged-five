using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragWidgets;

namespace ragParticleEditor
{
    public partial class EvoProperties : ragWidgetPage.WidgetPage
    {
        ArrayList m_EvoPropData;
        ArrayList m_EvoGlobeData;
        WidgetButton m_ButtonCheckEvoData = null;
        public ptxEvtData m_CurSelEventData = null;

        public EvoProperties()
        {
            InitializeComponent();
            m_EvoPropData = new ArrayList();
            m_EvoGlobeData = new ArrayList();
        }

        public void SelectEvolution(WidgetGroup evoGroup,string evoName,string effectName)        
        {
            ClearInterface();

            if (evoGroup == null)
                return;
                        
            WidgetGroup  curEvoGroup= evoGroup.FindFirstWidgetFromPath(evoName) as WidgetGroup;
            if (curEvoGroup == null)
                return;

            Bind(evoGroup, "EvoMode", m_comboGloblend, "SelectedIndex");

            //Bind the buttons
            Bind(curEvoGroup, "Test Evolution", m_SliderTestEvo);
            Bind(curEvoGroup, "Override Procedural Control", m_CheckOverrideProcControl, "Checked");
            

            //Set the label
            m_LabelEvolutionName.Text = "Evolution Set: " + evoName;

            //Get the registered properties list
            WidgetGroup evoGlobalProps = curEvoGroup.FindFirstWidgetFromPath("GlobalEffectProps") as WidgetGroup;
            if (evoGlobalProps == null)
                return;

            WidgetGroup activePropWG = evoGlobalProps.FindFirstWidgetFromPath("ActiveProps") as WidgetGroup;

            WidgetGroup evoGlobalPropRef = evoGlobalProps.FindFirstWidgetFromPath("RegPropList") as WidgetGroup;
            if (evoGlobalPropRef == null)
                return;

            //Populate the list box with registered propery info
            m_ListGlobalProps.BeginUpdate();
            foreach (Widget prop in evoGlobalPropRef.List)
            {
                if (prop is WidgetGroup)
                {
                    WidgetGroup propgroup = prop as WidgetGroup;
                    evoPropData d = new evoPropData();
                    d.m_WidgetName = prop.ToString();
                    d.m_PropGroupWG = propgroup;
                    d.m_Toggle = propgroup.FindFirstWidgetFromPath("ActiveState") as WidgetToggle;

                    if ( (d.m_Toggle != null) && (d.m_Toggle.Checked) )
                    {
                        d.m_Title = "*" + prop.ToString();
                        //d.m_IsActive = true;
                    }
                    else
                    {
                        //d.m_IsActive = false;
                        d.m_Title = prop.ToString();
                    }

                    d.m_ActivePropWG = propgroup.FindFirstWidgetFromPath("KeyframeData") as WidgetGroup;
                    d.m_ButtonActivate = propgroup.FindFirstWidgetFromPath("Activate") as WidgetButton;
                    d.m_ButtonDeactivate = propgroup.FindFirstWidgetFromPath("Deactivate") as WidgetButton;
                    m_EvoGlobeData.Add(d);
                    m_ListGlobalProps.Items.Add(d);
                }
            }
            m_ListGlobalProps.EndUpdate();
            //SelectEvent(m_CurSelEventData);
        }
        public void SelectEvent(string evoname,ptxEvtData data)
        {
            m_ListEventProps.Items.Clear();
            m_EvoPropData.Clear();
            if (data == null)
                return;
            if (data.m_EvoProps == null)
                return;
            
            //Set the label
            m_LabelEvtProps.Text = "Evolution Properties: (" + data.m_PtxChannel.m_Name + ")";
            m_ButtonCheckEvoData = data.m_CheckEvoProps;
            
            WidgetGroup selevo =null;
            
            //Find the correct evolution
            if (data.m_EvoProps != null)
            {
                Bind(data.m_EvoProps, "EvoMode", m_ComboBlendMode, "SelectedIndex");
                foreach (Widget eve in data.m_EvoProps.List)
                {
                    if (eve.Title == evoname)
                    {
                        selevo = eve as WidgetGroup;
                        break;
                    }
                }
            }
         
            if(selevo != null)
            {
                foreach (Widget w in selevo.List)
                {
                    if (w is WidgetGroup)
                    {
                        WidgetGroup evogroup = w as WidgetGroup;
                        WidgetGroup props = evogroup.FindFirstWidgetFromPath("RegPropList") as WidgetGroup;
                        WidgetGroup activepropWG = evogroup.FindFirstWidgetFromPath("ActiveProps") as WidgetGroup;
                        if (props != null)
                        {
                            m_ListEventProps.BeginUpdate();
                            foreach (Widget prop in props.List)
                            {

                                if (prop is WidgetGroup)
                                {
                                    WidgetGroup propgroup = prop as WidgetGroup;
                                    evoPropData d = new evoPropData();
                                    d.m_WidgetName = prop.ToString();
                                    d.m_PropGroupWG = propgroup;
                                    d.m_Toggle = propgroup.FindFirstWidgetFromPath("ActiveState") as WidgetToggle;

                                    if ((d.m_Toggle != null) && (d.m_Toggle.Checked))
                                    {
                                        d.m_Title = "*" + evogroup.ToString() + " : " + prop.ToString();
                                        //d.m_IsActive=true;
                                    }
                                    else
                                    {
                                        d.m_Title = evogroup.ToString() + " : " + prop.ToString();
                                        //d.m_IsActive=false;
                                    }
                                    d.m_ActivePropWG = propgroup.FindFirstWidgetFromPath("KeyframeData") as WidgetGroup;
                                    d.m_ButtonActivate = propgroup.FindFirstWidgetFromPath("Activate") as WidgetButton;
                                    d.m_ButtonDeactivate = propgroup.FindFirstWidgetFromPath("Deactivate") as WidgetButton;
                                    m_EvoPropData.Add(d);
                                    m_ListEventProps.Items.Add(d);
                                }
                            }
                            m_ListEventProps.EndUpdate();
                        }
                    }
                }
             }

            m_CurSelEventData = null;
        }

        public void ClearInterface()
        {
            RemoveBindings();
            m_LabelEvolutionName.Text = "";
            m_ListGlobalProps.Items.Clear();
            m_ListEventProps.Items.Clear();
            m_EvoPropData.Clear();
            m_EvoGlobeData.Clear();
            m_CurSelEventData = null;
            m_ButtonCheckEvoData = null;
        }

        private void ActivateEvoProperty(evoPropData d)
        {
            if (!d.m_Toggle.Checked)
            {
                if(d.m_ButtonActivate != null)
                 d.m_ButtonActivate.Activate();
             d.m_Toggle.Checked = true;
                //ViewPlugIn.Instance.RebuildAllPanels();
            }
        }

        private void DeActivateEvoProperty(evoPropData d)
        {
            if (d.m_Toggle.Checked)
            {
                
                if (d.m_ButtonDeactivate != null)
                  d.m_ButtonDeactivate.Activate();
              d.m_Toggle.Checked = false;
                //ViewPlugIn.Instance.RebuildAllPanels();
            }
        }

        private void DisplayEvoProperty(evoPropData d)
        {
            if(!d.m_Toggle.Checked)
            {
                ActivateEvoProperty(d);
                return;
            }

            //find the widget in the active list
            Widget keyg = d.m_ActivePropWG.FindFirstWidgetFromPath(d.m_WidgetName);
            if (keyg is WidgetGroup)
            {
                ViewPlugIn.Instance.CreateAndOpenKeyframeEditorView( keyg as WidgetGroup, d.m_Title );
            }
        }

        private void Property_DoubleClick(object sender, EventArgs e)
        {
            ListBox lbox = sender as ListBox;
            evoPropData d = lbox.SelectedItem as evoPropData;

            if (!d.m_Toggle.Checked)
            {
                ActivateEvoProperty(d);
                return;
            }

            DisplayEvoProperty(d);

        }

        private void m_ListEventProps_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0 || e.Index > m_EvoPropData.Count)
                return;
            
            evoPropData d = m_EvoPropData[e.Index] as evoPropData;
            if (d == null)
                return;
            e.DrawBackground();
            e.DrawFocusRectangle();
            if (d.m_Toggle.Checked)
                e.Graphics.DrawString(d.m_Title,new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Bold),new SolidBrush(Color.FromKnownColor(KnownColor.ControlText)),e.Bounds); 
            else
                e.Graphics.DrawString(d.m_Title, new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Regular), new SolidBrush(Color.FromKnownColor(KnownColor.ControlText)), e.Bounds); 
        }

        private void m_ListGlobalProps_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index < 0 || e.Index > m_EvoGlobeData.Count)
                return;

            evoPropData d = m_EvoGlobeData[e.Index] as evoPropData;
            if (d == null)
                return;
            e.DrawBackground();
            e.DrawFocusRectangle();
            if (d.m_Toggle.Checked)
                e.Graphics.DrawString(d.m_Title, new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Bold), new SolidBrush(Color.FromKnownColor(KnownColor.ControlText)), e.Bounds);
            else
                e.Graphics.DrawString(d.m_Title, new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Regular), new SolidBrush(Color.FromKnownColor(KnownColor.ControlText)), e.Bounds);
        }

        private void m_MenuAddEvoProp_Click(object sender, EventArgs e)
        {
            ListBox lbox = m_ListEventProps;
            evoPropData d = lbox.SelectedItem as evoPropData;
            ActivateEvoProperty(d);
        }

        private void m_MenuDelEvoProp_Click(object sender, EventArgs e)
        {
            ListBox lbox = m_ListEventProps;
            evoPropData d = lbox.SelectedItem as evoPropData;
            DeActivateEvoProperty(d);
        }

        private void m_MenuEditEvoProp_Click(object sender, EventArgs e)
        {
            ListBox lbox = m_ListEventProps;
            evoPropData d = lbox.SelectedItem as evoPropData;
            DisplayEvoProperty(d);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

            ListBox lbox = m_ListEventProps;
            if (lbox.SelectedItem == null)
            {
                e.Cancel = true;
                return;
            }
            m_MenuAddEvoProp.Enabled = false;
            m_MenuDelEvoProp.Enabled = false;
            m_MenuEditEvoProp.Enabled = false;

            evoPropData d = lbox.SelectedItem as evoPropData;
            
            if(d.m_Toggle.Checked)
            {
                m_MenuDelEvoProp.Enabled = true;
                m_MenuEditEvoProp.Enabled = true;
                return;
            }
            else
            {
                m_MenuAddEvoProp.Enabled = true;
            }

        }

        private void m_ContextGlobalProp_Opening(object sender, CancelEventArgs e)
        {
            ListBox lbox = m_ListGlobalProps;
            if (lbox.SelectedItem == null)
            {
                e.Cancel = true;
                return;
            }
            m_MenuGlobAddEvoProp.Enabled = false;
            m_MenuGlobDelEvoProp.Enabled = false;
            m_MenuGlobEditEvoProp.Enabled = false;

            evoPropData d = lbox.SelectedItem as evoPropData;

            if (d.m_Toggle.Checked)
            {
                m_MenuGlobDelEvoProp.Enabled = true;
                m_MenuGlobEditEvoProp.Enabled = true;
                return;
            }
            else
            {
                m_MenuGlobAddEvoProp.Enabled = true;
            }

        }

        private void m_MenuGlobAddEvoProp_Click(object sender, EventArgs e)
        {
            ListBox lbox = m_ListGlobalProps;
            evoPropData d = lbox.SelectedItem as evoPropData;
            ActivateEvoProperty(d);
        }

        private void m_MenuGlobDelEvoProp_Click(object sender, EventArgs e)
        {
            ListBox lbox = m_ListGlobalProps;
            evoPropData d = lbox.SelectedItem as evoPropData;
            DeActivateEvoProperty(d);
        }

        private void m_MenuGlobEditEvoProp_Click(object sender, EventArgs e)
        {
            ListBox lbox = m_ListGlobalProps;
            evoPropData d = lbox.SelectedItem as evoPropData;
            DisplayEvoProperty(d);
        }

        private void m_ListEventProps_DoubleClick(object sender, EventArgs e)
        {
            ListBox lbox = m_ListEventProps;
            evoPropData d = lbox.SelectedItem as evoPropData;
            if (d == null)
                return;

            if (!d.m_Toggle.Checked)
            {
                ActivateEvoProperty(d);
                return;
            }

            DisplayEvoProperty(d);
        }

        private void m_ListGlobalProps_DoubleClick(object sender, EventArgs e)
        {
            ListBox lbox = m_ListGlobalProps;
            evoPropData d = lbox.SelectedItem as evoPropData;
            if (d == null)
                return;
            if (!d.m_Toggle.Checked)
            {
                ActivateEvoProperty(d);
                return;
            }

            DisplayEvoProperty(d);

        }

        private void m_ButtonCheckEvo_Click(object sender, EventArgs e)
        {
            if (m_ButtonCheckEvoData != null)
            {
                string path = m_ButtonCheckEvoData.FindPath();
                RemoveBindings();
                WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
                if (button != null)
                    button.Activate();
                //m_ButtonCheckEvoData.Activate();
                ViewPlugIn.Instance.RebuildAllPanels();
            }
        }
    }

    public class evoPropData
    {
        public string m_Title = "";
        public string m_WidgetName = "";
        public WidgetGroup m_ActivePropWG = null;
        public WidgetGroup m_PropGroupWG = null;
        public WidgetButton m_ButtonActivate = null;
        public WidgetButton m_ButtonDeactivate = null;
        public WidgetToggle m_Toggle = null;
        //public bool m_IsActive = false;
        public override string ToString()
        {
            return m_Title;
        }
    };

}
