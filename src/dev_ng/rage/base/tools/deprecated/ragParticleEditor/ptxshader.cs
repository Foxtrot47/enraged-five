using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragWidgets;

namespace ragParticleEditor
{
    public class ptxShader : ragWidgetPage.WidgetPage
    {
        //public static ptxShader sm_Instance;
        public static string MyName = "PtxShader";
        public string m_SelectedPtxRule;
        public string m_SelectedShader = null;
        
        private List<Widget> m_WidgetList;
        public static List<ptxShader> sm_Editors = new List<ptxShader>();
        private ArrayList m_LoadTexButtons = new ArrayList();
        private ArrayList m_TexBoxes = new ArrayList();
        private ragUi.ControlToggle controlToggle1;
        private GroupBox groupBox3;
        private Button button1;
        private TextBox textBox1;
        private Label label1;
        public object m_SavedStateData;
        private ListBox listBox1;

        struct ListBoxItem
        {
            public ListBoxItem(WidgetGroup _w, Color _color) { w = _w; color = _color; }
            public WidgetGroup w;
            public Color color;
        };

        public ptxShader(String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl)
			: base(appName, bankMgr, dockControl)
	    {
            InitializeComponent();

            // get rid of the window when it's closed
            dockControl.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;

            // don't save this window with the SandDock layout
            dockControl.PersistState = false;

            this.groupBox3.Controls.Clear();
	    }

        private void InitializeComponent()
        {
            this.controlToggle1 = new ragUi.ControlToggle();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // controlToggle1
            // 
            this.controlToggle1.Location = new System.Drawing.Point(6, 42);
            this.controlToggle1.Name = "controlToggle1";
            this.controlToggle1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.controlToggle1.Size = new System.Drawing.Size(315, 22);
            this.controlToggle1.TabIndex = 0;
            this.controlToggle1.Text = "ToggleNameToggleNameToggleNameToggleNameToggle";
            this.controlToggle1.Value = false;
            this.controlToggle1.ValueXPath = null;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listBox1);
            this.groupBox3.Controls.Add(this.controlToggle1);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(384, 95);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Shader Variables";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(9, 70);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(312, 17);
            this.listBox1.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(327, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(51, 20);
            this.button1.TabIndex = 2;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(185, 16);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(136, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "TextureNameTextureNameTextureN";
            // 
            // ptxShader
            // 
            this.Controls.Add(this.groupBox3);
            this.Name = "ptxShader";
            this.Size = new System.Drawing.Size(402, 425);
            this.Load += new System.EventHandler(this.ptxShader_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        public override string GetMyName()
        {
            return GetTabText();
        }

        public override string GetTabText()
        {
            if (this.Enabled && (m_SelectedShader != null))
            {
                return "Shader: " + m_SelectedShader;
            }
            else
            {
                return "Shader: none";
            }
        }

        public override object GetStateData()
        {
            return ragWidgetPage.WidgetPagePlugIn.BasicGetStateData(this);
        }

        public override void SetStateData(object d)
        {
            ragWidgetPage.WidgetPagePlugIn.BasicSetStateData(this, d);
            FindWidgetHandles();
        }


        public static ragWidgetPage.WidgetPage CreateView(String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl)
        {
            ptxShader editor = new ptxShader(appName, bankMgr, dockControl);
            dockControl.FloatingSize = new Size(editor.Width, editor.Height);
            dockControl.FloatingLocation = new Point(Cursor.Position.X-editor.Width/3,Cursor.Position.Y-editor.Height/2);
            dockControl.Closing += new TD.SandDock.DockControlClosingEventHandler(editor.CloseEditor);
            sm_Editors.Add(editor);
            return editor;
        }
        
        public static ptxShader FindEditor(String ptxName, String shaderName)
        {
            foreach (ptxShader editor in sm_Editors)
            {
                if ((editor.m_SelectedPtxRule == ptxName) && (editor.m_SelectedShader == shaderName))
                {
                    return editor;
                }
            }
            return null;
        }

        public static void RemoveAllBindings()
        {
            foreach (ptxShader s in sm_Editors)
                s.RemoveBindings();
        }

        public override bool InitiallyFloating
        {
            get
            {
                return true;
            }
        }

        public static void Shutdown()
        {
            // we don't want to persist *any* information about a KeyframeEditor,
            // so close its window which will remove any data it might have saved
            // in DllInfo.PluginDataHash
            while (sm_Editors.Count > 0)
            {
                sm_Editors[0].DockControl.Close();
            }
        }

        private void CloseEditor(object sender, TD.SandDock.DockControlClosingEventArgs e)
        {
            // remove it from the static list
            RemoveBindings();
            sm_Editors.Remove(this);
        }
        
        void FindWidgetHandles()
        {
            const int GROUPBOX3_TOP = 15;
            const int GROUPBOX3_INTERVAL = 23;

            // Clear previous 
            RemoveBindings();
            m_LoadTexButtons.Clear();
            m_TexBoxes.Clear();
            this.groupBox3.Controls.Clear();

            // Find shader widgets
            m_WidgetRoot = m_BankManager.FindFirstWidgetFromPath("rage - Rmptfx") as WidgetGroupBase;
            WidgetGroup ptxrules = m_WidgetRoot.FindFirstWidgetFromPath("PtxRules") as WidgetGroup;
            if (ptxrules == null) return;
            WidgetGroup rules = ptxrules.FindFirstWidgetFromPath("Rules") as WidgetGroup;
            if (m_SelectedPtxRule == null) return;
            WidgetGroup emit = rules.FindFirstWidgetFromPath(m_SelectedPtxRule) as WidgetGroup;
            if (emit == null) return;
            WidgetGroup info = emit.FindFirstWidgetFromPath("Info") as WidgetGroup;
            if (info == null) return;
            WidgetGroup shaders = info.FindFirstWidgetFromPath("Shaders") as WidgetGroup;
            if (shaders == null) return;
            WidgetText shadername = shaders.FindFirstWidgetFromPath("ShaderName") as WidgetText;
            if (shadername == null) return;
            m_SelectedShader = shadername.String;
            WidgetGroup shadergroup = shaders.FindFirstWidgetFromPath(shadername.String) as WidgetGroup;
            if (shadergroup == null)
            {
                m_SelectedShader = null;
                return;
            }
            // Retreive shader widgets
            m_WidgetList = shadergroup.FindWidgetsFromPath("");
            if (m_WidgetList == null) return;

            // Populate UI lists
            this.groupBox3.SuspendLayout();
            int UICount = 0;
            int TextureCount = 0;
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            foreach (WidgetGroup w in m_WidgetList)
            {
                WidgetData keyframeableData = w.FindFirstWidgetFromPath("Keyframeable") as WidgetData;
                if (keyframeableData == null) continue;
                bool keyframeable = enc.GetString(keyframeableData.Data).CompareTo("1") == 0;

                WidgetData type = w.FindFirstWidgetFromPath("Type") as WidgetData;
                if (type == null) continue;
                string str = enc.GetString(type.Data);

                if (str.Equals("Float") || str.Equals("Float2") || str.Equals("Float3") || str.Equals("Float4"))
                {
                    ListBox listbox = new ListBox();
                    listbox.Location = new System.Drawing.Point(listBox1.Location.X, GROUPBOX3_TOP + UICount * GROUPBOX3_INTERVAL);
                    listbox.Size = listBox1.Size;
                    listbox.DrawItem+=new DrawItemEventHandler(listbox_DrawItem);
                    listbox.Click += new EventHandler(listbox_Click);
                    listbox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;

                    Color color;
                    if (keyframeable) color = Color.FromArgb(255, 255, 100, 0);
                    else color = Color.FromArgb(255, 0, 100, 255);
                    listbox.Items.Add(new ListBoxItem(w,color));
                    groupBox3.Controls.Add(listbox);
                }
                else if(str.Equals("Bool"))
                {
                    ragUi.ControlToggle toggle = new ragUi.ControlToggle();
                    toggle.Location = new System.Drawing.Point(controlToggle1.Left, GROUPBOX3_TOP + UICount * GROUPBOX3_INTERVAL);
                    toggle.Size = controlToggle1.Size;
                    toggle.Text = w.Title;

                    Bind(w, w.Title, toggle);
                    groupBox3.Controls.Add(toggle);
                }
                else if(str.Equals("Texture"))
                {
                    m_LoadTexButtons.Add(w.FindFirstWidgetFromPath("Load Texture") as WidgetButton);
                 
                    Button button = new System.Windows.Forms.Button();
                    button.Location = new System.Drawing.Point(button1.Left, GROUPBOX3_TOP + UICount * GROUPBOX3_INTERVAL);    
                    button.Text = "Browse...";
                    button.Size = button1.Size;
                    button.Name = "" + TextureCount;
                   
                    button.Click += new System.EventHandler(this.button_Click);

                    TextBox textBox = new System.Windows.Forms.TextBox();
                    textBox.Location = new System.Drawing.Point(textBox1.Left, GROUPBOX3_TOP + UICount * GROUPBOX3_INTERVAL);
                    textBox.Size = new System.Drawing.Size(textBox1.Width, textBox1.Height);
                    textBox.KeyPress += new KeyPressEventHandler(textbox_KeyPress);
                    textBox.Name = "" + TextureCount;
                    m_TexBoxes.Add(textBox);
                    Bind(w,"Texture Name",textBox,"Text");
                    Label label = new System.Windows.Forms.Label();
                    label.Location = new System.Drawing.Point(label1.Left, GROUPBOX3_TOP + UICount * GROUPBOX3_INTERVAL);
                    label.Size = label1.Size;
                    label.Text = w.Title;

                    groupBox3.Controls.Add(button);
                    groupBox3.Controls.Add(textBox);
                    groupBox3.Controls.Add(label);
                    TextureCount++;
                }
                UICount++;
            }

            this.groupBox3.Size = new System.Drawing.Size(groupBox3.Width, 20+UICount * GROUPBOX3_INTERVAL);
            this.groupBox3.ResumeLayout();
        }

        public void SelectPtxRule(string name)
        {
            m_SelectedPtxRule = name;
            FindWidgetHandles();
            m_DockControl.Text = m_DockControl.TabText = m_SelectedPtxRule+": "+GetTabText();
        }

        public void SelectShader(string name)
        {
            FindWidgetHandles();
            m_DockControl.Text = m_DockControl.TabText = m_SelectedPtxRule + ": " + GetTabText();
        }

        private void listbox_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            if (e.Index < 0) return;
            e.DrawFocusRectangle();
            ListBoxItem item = (ListBoxItem)(sender as ListBox).Items[0];
            e.Graphics.DrawString(item.w.Title, new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Regular), new SolidBrush(item.color), e.Bounds);
        }
        private void listbox_Click(object sender, System.EventArgs e)
        {
            ListBox listbox = sender as ListBox;
            if (!(listbox.SelectedItem is ListBoxItem)) return;

            ListBoxItem item = (ListBoxItem)listbox.SelectedItem;
            KeyframeEditor kf = ViewPlugIn.Instance.CreateAndOpenKeyframeEditorView(item.w as WidgetGroup, item.w.Title + "/");
        }

        private void textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                int index = Int32.Parse((sender as TextBox).Name);
                (m_LoadTexButtons[index] as WidgetButton).Activate();
            }
        }

        private void button_Click(object sender, EventArgs e)
        {            
            OpenFileDialog ofd = new OpenFileDialog();
            if (UserData.LastTexturePath.Length > 0)
                ofd.InitialDirectory = UserData.LastTexturePath;
            else
                ofd.InitialDirectory = System.IO.Directory.GetCurrentDirectory();

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                int index = Int32.Parse((sender as Button).Name);
                UserData.LastTexturePath = System.IO.Path.GetDirectoryName(ofd.FileName);
                (m_TexBoxes[index] as TextBox).Text = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);
                (m_LoadTexButtons[index] as WidgetButton).Activate();
            }      
        }

        private void ptxShader_Load(object sender, EventArgs e)
        {

        }
    }
}
