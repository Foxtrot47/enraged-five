using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using ragCore;
using ragWidgets;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for ptxEvtDataEffect.
	/// </summary>
	public class ptxEvtDataEffect : ptxEvtData
	{
		private System.Windows.Forms.Panel panel1;
		public ragUi.ControlSlider m_SliderStartTime;
		private System.Windows.Forms.Label m_LabelName;
		public System.Windows.Forms.CheckBox m_ActiveToggle;
		private System.Windows.Forms.Label label1;
		public ragUi.ControlSlider m_SliderTriggerMax;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel m_PanelDomainProp;
		public System.Windows.Forms.CheckBox m_CheckShowAxis;
		public ragUi.ControlSlider m_SliderRotationZ;
		public ragUi.ControlSlider m_SliderRotationY;
		public ragUi.ControlSlider m_SliderRotationX;

		#region Variables
		public ptxDomainProperties m_DomainProperties;
		#endregion
	
		public ptxEvtDataEffect(string name, ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
		{
			InitializeComponent();
			m_Name = name;
			m_PtxChannel = channel;
			m_DomainProperties = new ptxDomainProperties();
			m_PanelDomainProp.SuspendLayout();
			m_PanelDomainProp.Controls.Add(m_DomainProperties);
			m_DomainProperties.Left=0;
			m_DomainProperties.Top=0;
			m_DomainProperties.label1.Top-=30;
			m_DomainProperties.m_ListDomainKeyframes.Top-=30;
			m_PanelDomainProp.ResumeLayout();
			UpdateInterface();
		}

		public ptxEvtDataEffect()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			//
			// TODO: Add constructor logic here
			//

		}

		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.m_CheckShowAxis = new System.Windows.Forms.CheckBox();
			this.m_PanelDomainProp = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.m_SliderRotationZ = new ragUi.ControlSlider();
			this.m_SliderRotationY = new ragUi.ControlSlider();
			this.m_SliderRotationX = new ragUi.ControlSlider();
			this.m_SliderTriggerMax = new ragUi.ControlSlider();
			this.m_SliderStartTime = new ragUi.ControlSlider();
			this.m_LabelName = new System.Windows.Forms.Label();
			this.m_ActiveToggle = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// m_SliderIndex
			// 
			this.m_SliderIndex.Name = "m_SliderIndex";
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.m_CheckShowAxis);
			this.panel1.Controls.Add(this.m_PanelDomainProp);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.m_SliderRotationZ);
			this.panel1.Controls.Add(this.m_SliderRotationY);
			this.panel1.Controls.Add(this.m_SliderRotationX);
			this.panel1.Controls.Add(this.m_SliderTriggerMax);
			this.panel1.Controls.Add(this.m_SliderStartTime);
			this.panel1.Controls.Add(this.m_LabelName);
			this.panel1.Location = new System.Drawing.Point(8, 19);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(368, 269);
			this.panel1.TabIndex = 20;
			// 
			// m_CheckShowAxis
			// 
			this.m_CheckShowAxis.Location = new System.Drawing.Point(128, 71);
			this.m_CheckShowAxis.Name = "m_CheckShowAxis";
			this.m_CheckShowAxis.TabIndex = 28;
			this.m_CheckShowAxis.Text = "Show Axis";
			// 
			// m_PanelDomainProp
			// 
			this.m_PanelDomainProp.Location = new System.Drawing.Point(0, 96);
			this.m_PanelDomainProp.Name = "m_PanelDomainProp";
			this.m_PanelDomainProp.Size = new System.Drawing.Size(368, 176);
			this.m_PanelDomainProp.TabIndex = 27;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(272, 4);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 17);
			this.label2.TabIndex = 26;
			this.label2.Text = "Rotation:";
			// 
			// m_SliderRotationZ
			// 
			this.m_SliderRotationZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_SliderRotationZ.IsFloat = true;
			this.m_SliderRotationZ.Location = new System.Drawing.Point(240, 68);
			this.m_SliderRotationZ.Maximum = 360F;
			this.m_SliderRotationZ.Minimum = -360F;
			this.m_SliderRotationZ.Name = "m_SliderRotationZ";
			this.m_SliderRotationZ.Size = new System.Drawing.Size(112, 24);
			this.m_SliderRotationZ.Step = 0.1F;
			this.m_SliderRotationZ.TabIndex = 25;
			this.m_SliderRotationZ.Text = "Z";
			this.m_SliderRotationZ.UseTrackbar = false;
			this.m_SliderRotationZ.Value = new System.Decimal(new int[] {
																			0,
																			0,
																			0,
																			0});
			this.m_SliderRotationZ.ValueXPath = null;
			// 
			// m_SliderRotationY
			// 
			this.m_SliderRotationY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_SliderRotationY.IsFloat = true;
			this.m_SliderRotationY.Location = new System.Drawing.Point(240, 44);
			this.m_SliderRotationY.Maximum = 360F;
			this.m_SliderRotationY.Minimum = -360F;
			this.m_SliderRotationY.Name = "m_SliderRotationY";
			this.m_SliderRotationY.Size = new System.Drawing.Size(112, 24);
			this.m_SliderRotationY.Step = 0.1F;
			this.m_SliderRotationY.TabIndex = 24;
			this.m_SliderRotationY.Text = "Y";
			this.m_SliderRotationY.UseTrackbar = false;
			this.m_SliderRotationY.Value = new System.Decimal(new int[] {
																			0,
																			0,
																			0,
																			0});
			this.m_SliderRotationY.ValueXPath = null;
			// 
			// m_SliderRotationX
			// 
			this.m_SliderRotationX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_SliderRotationX.IsFloat = true;
			this.m_SliderRotationX.Location = new System.Drawing.Point(240, 20);
			this.m_SliderRotationX.Maximum = 360F;
			this.m_SliderRotationX.Minimum = -360F;
			this.m_SliderRotationX.Name = "m_SliderRotationX";
			this.m_SliderRotationX.Size = new System.Drawing.Size(112, 24);
			this.m_SliderRotationX.Step = 0.1F;
			this.m_SliderRotationX.TabIndex = 23;
			this.m_SliderRotationX.Text = "X";
			this.m_SliderRotationX.UseTrackbar = false;
			this.m_SliderRotationX.Value = new System.Decimal(new int[] {
																			0,
																			0,
																			0,
																			0});
			this.m_SliderRotationX.ValueXPath = null;
			// 
			// m_SliderTriggerMax
			// 
			this.m_SliderTriggerMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_SliderTriggerMax.IsFloat = true;
			this.m_SliderTriggerMax.Location = new System.Drawing.Point(8, 48);
			this.m_SliderTriggerMax.Maximum = 9000F;
			this.m_SliderTriggerMax.Minimum = -1F;
			this.m_SliderTriggerMax.Name = "m_SliderTriggerMax";
			this.m_SliderTriggerMax.Size = new System.Drawing.Size(208, 24);
			this.m_SliderTriggerMax.Step = 0.01F;
			this.m_SliderTriggerMax.TabIndex = 22;
			this.m_SliderTriggerMax.Text = "Trigger Max";
			this.m_SliderTriggerMax.UseTrackbar = false;
			this.m_SliderTriggerMax.Value = new System.Decimal(new int[] {
																			 0,
																			 0,
																			 0,
																			 0});
			this.m_SliderTriggerMax.ValueXPath = null;
			// 
			// m_SliderStartTime
			// 
			this.m_SliderStartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.m_SliderStartTime.IsFloat = true;
			this.m_SliderStartTime.Location = new System.Drawing.Point(8, 21);
			this.m_SliderStartTime.Maximum = 100F;
			this.m_SliderStartTime.Minimum = 0F;
			this.m_SliderStartTime.Name = "m_SliderStartTime";
			this.m_SliderStartTime.Size = new System.Drawing.Size(208, 24);
			this.m_SliderStartTime.Step = 0.01F;
			this.m_SliderStartTime.TabIndex = 18;
			this.m_SliderStartTime.Text = "Start Time";
			this.m_SliderStartTime.UseTrackbar = false;
			this.m_SliderStartTime.Value = new System.Decimal(new int[] {
																			0,
																			0,
																			0,
																			0});
			this.m_SliderStartTime.ValueXPath = null;
			this.m_SliderStartTime.ValueChanged += new System.EventHandler(this.m_SliderStartTime_ValueChanged);
			// 
			// m_LabelName
			// 
			this.m_LabelName.Location = new System.Drawing.Point(3, 3);
			this.m_LabelName.Name = "m_LabelName";
			this.m_LabelName.Size = new System.Drawing.Size(256, 23);
			this.m_LabelName.TabIndex = 21;
			this.m_LabelName.Text = "label2";
			// 
			// m_ActiveToggle
			// 
			this.m_ActiveToggle.Checked = true;
			this.m_ActiveToggle.CheckState = System.Windows.Forms.CheckState.Checked;
			this.m_ActiveToggle.Location = new System.Drawing.Point(224, -1);
			this.m_ActiveToggle.Name = "m_ActiveToggle";
			this.m_ActiveToggle.Size = new System.Drawing.Size(104, 23);
			this.m_ActiveToggle.TabIndex = 19;
			this.m_ActiveToggle.Text = "Active";
			// 
			// label1
			// 
			this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 1);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 15);
			this.label1.TabIndex = 18;
			this.label1.Text = "Effect Event";
			// 
			// ptxEvtDataEffect
			// 
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.m_ActiveToggle);
			this.Controls.Add(this.label1);
			this.Name = "ptxEvtDataEffect";
			this.Size = new System.Drawing.Size(384, 296);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		public override void UpdateInterface()
		{
			m_SliderStartTime.Value = (decimal)m_PtxChannel.StartTime;
			m_LabelName.Text = m_Name;
		}

		public override void OnSelected()
		{
		}
		private void UpdateChannel()
		{
			m_PtxChannel.StartTime = (float)m_SliderStartTime.Value;
		}

		private void m_SliderStartTime_ValueChanged(object sender, System.EventArgs e)
		{
			UpdateChannel();
		}

	
	}
}
