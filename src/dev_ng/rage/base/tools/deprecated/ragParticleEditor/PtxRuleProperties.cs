using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using ragUi;
using ragCore;
using ragWidgets;

namespace ragParticleEditor
{
	/// <summary>
	/// Summary description for PtxRuleProperties.
	/// </summary>
	public class PtxRuleProperties : ragWidgetPage.WidgetPage
    {
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label m_LabelSortTime;
		private System.Windows.Forms.Label m_LabelDrawTime;
		private System.Windows.Forms.Label m_LabelUpdateTime;
		private System.Windows.Forms.Label m_LabelTotalPoints;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox m_CheckboxSort;
		private System.Windows.Forms.ComboBox m_ComboShader;
		private System.Windows.Forms.Button m_ButtonShaderVars;
		private System.Windows.Forms.CheckBox m_CheckboxAlphaBlend;
		private System.Windows.Forms.CheckBox m_CheckboxDepthTest;
		private System.Windows.Forms.CheckBox m_CheckboxDepthWrite;
		private System.Windows.Forms.ListBox m_ListKeyframeProperties;
		private System.Windows.Forms.ComboBox m_ComboBlendMode;
		private System.Windows.Forms.ComboBox m_ComboLightingMode;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.GroupBox m_PtxTypeData;
		private System.Windows.Forms.GroupBox groupBox3;
		private ragUi.ControlSlider m_SliderPosVarZ;
		private ragUi.ControlSlider m_SliderPosVarY;
		private ragUi.ControlSlider m_SliderPosVarX;
		private System.Windows.Forms.Label label4;
		private ragUi.ControlSlider m_SliderVelVarZ;
		private ragUi.ControlSlider m_SliderVelVarY;
		private ragUi.ControlSlider m_SliderVelVarX;
		private ragUi.ControlSlider m_SliderVelZ;
		private ragUi.ControlSlider m_SliderVelY;
		private ragUi.ControlSlider m_SliderVelX;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private ragUi.ControlToggle m_WorldspaceVel;
		private ragUi.ControlToggle m_WorldspacePosition;
		private System.Windows.Forms.GroupBox groupBox2;
		private ragUi.ControlSlider m_SliderLife;
		private ragUi.ControlSlider m_SliderLifeBias;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.CheckBox m_CheckMatchDuration;
		private System.Windows.Forms.TextBox m_TextOnDeathEvent;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox m_TextOnBirthEvent;
		private System.Windows.Forms.TextBox m_TextShaderName;
		private System.Windows.Forms.Label m_ShaderLabel;
		private System.Windows.Forms.Button m_ButtonSetShader;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem reloadToolStripMenuItem;
        private ToolStripMenuItem saveToolStripMenuItem;
        private ToolStripMenuItem saveAsToolStripMenuItem;
        private Button m_ButtonShaderVar;
		private System.ComponentModel.IContainer components;

		public class StateData 
		{
			public string m_WidgetRootPath;
		}

        public override string GetMyName()
        {
            return MyName;
        }

		public override string GetTabText() 
		{
            if ( this.Enabled && (m_PtxRuleName != null) && (m_RuleTypeString != null) )
            {
                return m_RuleTypeString + ": " + m_PtxRuleName;
            }
            else
            {
                return "Particle Rule: (none)";
            }
		}

        public override bool IsSingleInstance
        {
            get
            {
                return true;
            }
        }

		public static PtxRuleProperties sm_Instance;
        public static string MyName = "Particle Properties";

		public static ragWidgetPage.WidgetPage CreatePage(String appName,IBankManager bankMgr,TD.SandDock.DockControl dockControl)
		{
			if (sm_Instance == null) 
			{	
				sm_Instance = new PtxRuleProperties(appName, bankMgr, dockControl);

                if ( ViewPlugIn.Instance.CheckPluginStatus() )
                {
                    Init();
                }

				return sm_Instance;
			}
			else
			{
				return sm_Instance;
			}
		}

        public static void Init()
        {
            if ( sm_Instance != null )
            {
                sm_Instance.FindWidgetHandles();
                sm_Instance.timer1.Enabled = true;
            }
        }

		public static void Shutdown()
		{
            if ( sm_Instance != null )
            {
                sm_Instance.ClearView();
                sm_Instance = null;
            }
		}

		public PtxRuleProperties(String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl)
			: base(appName, bankMgr, dockControl)
		{
			InitializeComponent();

			m_DockControl.Text = "Particle Rule: (none)";
			m_DockControl.TabText = m_DockControl.Text;			
		}

		
		public WidgetGroupBase WidgetRoot
		{
			get 
			{
				return m_WidgetRoot;
			}
			set 
			{
				if (m_WidgetRoot != value) 
				{
					m_WidgetRoot = value;
					FindWidgetHandles();
				}
			}
		}

		private WidgetSliderInt		m_WidgetTotalPoints;
		private WidgetSliderFloat	m_WidgetUpdateTime;
		private WidgetSliderFloat	m_WidgetDrawTime;
		private WidgetSliderFloat	m_WidgetSortTime;
		private WidgetGroup			m_WidgetKeyframes;

		private WidgetButton		m_WidgetSave;
		private WidgetButton		m_WidgetLoad;
		private WidgetButton		m_WidgetSetShader;
		private WidgetText			m_WidgetPtxRuleName=null;
		private string				m_PtxRuleName;
        private string m_RuleTypeString;
		public static StateData sm_SavedStateData;

		struct ListItemData
		{
			public Color	m_Color;
			public string  m_Name;
		};
		private ArrayList		m_ListItems = new ArrayList();

		public void SelectPtxRule(string ruleName)
		{
			if(ruleName== m_PtxRuleName)
				return;
			m_PtxRuleName=ruleName;
			FindWidgetHandles();
		}
		private void FindWidgetHandles()
		{
			RemoveBindings();

			WidgetGroup ruleGroup =null;
			m_WidgetRoot = MainPtxWindow.Instance.m_WidgetPtxRuleList;

			if(m_WidgetRoot!=null)
				ruleGroup = m_WidgetRoot.FindFirstWidgetFromPath(m_PtxRuleName) as WidgetGroup;
			
			if(ruleGroup== null)
			{
				this.Enabled=false;
				return;
			}

			this.Enabled = true;
			//Check for ptxRuleType
			WidgetText ruleType = ruleGroup.FindFirstWidgetFromPath("RuleType") as WidgetText;

            m_RuleTypeString = ruleType.String;
            m_DockControl.Text = m_DockControl.TabText = GetTabText();

			m_WidgetPtxRuleName = ruleGroup.FindFirstWidgetFromPath("Name") as WidgetText;
			m_WidgetSave = ruleGroup.FindFirstWidgetFromPath("Save") as WidgetButton;
			m_WidgetLoad = ruleGroup.FindFirstWidgetFromPath("Load") as WidgetButton;
			
			//Profiling
			this.ReferenceGroup(m_PtxRuleName+"/Profiling");
			WidgetGroup profiling = ruleGroup.FindFirstWidgetFromPath("Profiling") as WidgetGroup;
			if(profiling != null)
			{
				m_WidgetTotalPoints =	profiling.FindFirstWidgetFromPath("Num Active Points") as WidgetSliderInt;
				m_WidgetUpdateTime =	profiling.FindFirstWidgetFromPath("CPU Update Time (ms)") as WidgetSliderFloat;
				m_WidgetDrawTime =		profiling.FindFirstWidgetFromPath("CPU Draw Time (ms)") as WidgetSliderFloat;
				m_WidgetSortTime =		profiling.FindFirstWidgetFromPath("CPU Sort Time (ms)") as WidgetSliderFloat;
			}
			
			//Info
			WidgetGroup info = ruleGroup.FindFirstWidgetFromPath("Info") as WidgetGroup;
			if(info != null)
			{

				WidgetGroup render = info.FindFirstWidgetFromPath("RenderState") as WidgetGroup;
				ReferenceGroup(m_PtxRuleName+"Info/RenderState");
				if(render != null)
				{
					Bind(render,"BlendSet", m_ComboBlendMode, "SelectedIndex");
					Bind(render,"LightingMode", m_ComboLightingMode, "SelectedIndex");
					Bind(render,"DepthWrite", m_CheckboxDepthWrite, "Checked");
					Bind(render,"AlphaBlend", m_CheckboxAlphaBlend, "Checked");
					Bind(render,"DepthTest", m_CheckboxDepthTest, "Checked");
				}
				Bind(info,"DrawSorted", m_CheckboxSort, "Checked");
				
				Bind(info,"Match Point Duration",m_CheckMatchDuration,"Checked");
				Bind(info,"On Birth Effect",m_TextOnBirthEvent,"Text");
				Bind(info,"On Death Effect",m_TextOnDeathEvent,"Text");
				
				m_PtxTypeData.Controls.Clear();
				if(ruleType.String == "ptxsprite")
				{
					m_PtxTypeData.SuspendLayout();
					PtxSpriteData data = new PtxSpriteData();
					data.BindToData(ruleGroup);
					data.Location = new Point(8,16);
					m_PtxTypeData.Controls.Add(data);
					m_PtxTypeData.Text = "Sprite Info";
					m_PtxTypeData.ResumeLayout();
				}
				if(ruleType.String == "ptxmodel")
				{
					m_PtxTypeData.SuspendLayout();
					PtxModelData data = new PtxModelData();
					data.BindToData(ruleGroup);
					data.Location = new Point(8,16);
					m_PtxTypeData.Controls.Add(data);
					m_PtxTypeData.Text = "Model Info";
					m_PtxTypeData.ResumeLayout();
				}
			}


            m_TextShaderName.Enabled = false;
            m_ButtonSetShader.Enabled = false;
            m_ButtonShaderVar.Enabled = false;

            WidgetGroup shaderRoot = info.FindFirstWidgetFromPath("Shaders") as WidgetGroup;
			if(shaderRoot != null)
			{
				m_WidgetSetShader = shaderRoot.FindFirstWidgetFromPath("SetShader") as WidgetButton;
				Bind(shaderRoot,"ShaderName",m_TextShaderName,"Text");
                m_TextShaderName.Enabled = true;
                m_ButtonSetShader.Enabled = true;
                m_ButtonShaderVar.Enabled = (m_TextShaderName.Text.Length > 0);
			}
			
			BuildKeyframeList(ruleGroup);
		}


		protected void BuildKeyframeList(WidgetGroup group)
		{
			m_ListItems.Clear();
			m_ListKeyframeProperties.BeginUpdate();
			m_ListKeyframeProperties.Items.Clear();
			m_WidgetKeyframes = group.FindFirstWidgetFromPath("KeyFrames") as WidgetGroup;
			
			if (m_WidgetKeyframes != null)
			{

				WidgetGroup Toggle = m_WidgetKeyframes.FindFirstWidgetFromPath("Toggleable") as WidgetGroup;
				if(Toggle != null)
				{
					foreach(Widget w in Toggle.List) 
					{
						m_ListKeyframeProperties.Items.Add(w);
						ListItemData d = new ListItemData();
						d.m_Name = w.ToString();
						d.m_Color = Color.FromArgb(255,0,0,0);
						m_ListItems.Add(d);
					}
				}

				WidgetGroup OverLife = m_WidgetKeyframes.FindFirstWidgetFromPath("OverLife") as WidgetGroup;
				if(OverLife != null)
				{
					foreach(Widget w in OverLife.List) 
					{
						m_ListKeyframeProperties.Items.Add(w);
						ListItemData d = new ListItemData();
						d.m_Name = w.ToString();
						d.m_Color = Color.FromArgb(255,255,100,0);
						m_ListItems.Add(d);
					}
				}

				WidgetGroup OverTime = m_WidgetKeyframes.FindFirstWidgetFromPath("OverTime") as WidgetGroup;
				if(OverTime != null)
				{
					foreach(Widget w in OverTime.List) 
					{
						m_ListKeyframeProperties.Items.Add(w);
						ListItemData d = new ListItemData();
						d.m_Name = w.ToString();
						d.m_Color = Color.FromArgb(255,0,100,255);
						m_ListItems.Add(d);
					}
				}
			}
			m_ListKeyframeProperties.EndUpdate();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.m_PtxTypeData = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_ComboLightingMode = new System.Windows.Forms.ComboBox();
            this.m_ComboBlendMode = new System.Windows.Forms.ComboBox();
            this.m_CheckboxSort = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_ComboShader = new System.Windows.Forms.ComboBox();
            this.m_ButtonShaderVars = new System.Windows.Forms.Button();
            this.m_CheckboxAlphaBlend = new System.Windows.Forms.CheckBox();
            this.m_CheckboxDepthTest = new System.Windows.Forms.CheckBox();
            this.m_CheckboxDepthWrite = new System.Windows.Forms.CheckBox();
            this.m_ListKeyframeProperties = new System.Windows.Forms.ListBox();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.m_LabelSortTime = new System.Windows.Forms.Label();
            this.m_LabelDrawTime = new System.Windows.Forms.Label();
            this.m_LabelUpdateTime = new System.Windows.Forms.Label();
            this.m_LabelTotalPoints = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.m_SliderLifeBias = new ragUi.ControlSlider();
            this.m_SliderLife = new ragUi.ControlSlider();
            this.m_WorldspaceVel = new ragUi.ControlToggle();
            this.m_WorldspacePosition = new ragUi.ControlToggle();
            this.m_SliderPosVarZ = new ragUi.ControlSlider();
            this.m_SliderPosVarY = new ragUi.ControlSlider();
            this.m_SliderPosVarX = new ragUi.ControlSlider();
            this.label4 = new System.Windows.Forms.Label();
            this.m_SliderVelVarZ = new ragUi.ControlSlider();
            this.m_SliderVelVarY = new ragUi.ControlSlider();
            this.m_SliderVelVarX = new ragUi.ControlSlider();
            this.m_SliderVelZ = new ragUi.ControlSlider();
            this.m_SliderVelY = new ragUi.ControlSlider();
            this.m_SliderVelX = new ragUi.ControlSlider();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.m_ButtonShaderVar = new System.Windows.Forms.Button();
            this.m_ButtonSetShader = new System.Windows.Forms.Button();
            this.m_TextShaderName = new System.Windows.Forms.TextBox();
            this.m_TextOnBirthEvent = new System.Windows.Forms.TextBox();
            this.m_TextOnDeathEvent = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_CheckMatchDuration = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.m_ShaderLabel = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_PtxTypeData
            // 
            this.m_PtxTypeData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_PtxTypeData.Location = new System.Drawing.Point(272, 208);
            this.m_PtxTypeData.Name = "m_PtxTypeData";
            this.m_PtxTypeData.Size = new System.Drawing.Size(304, 424);
            this.m_PtxTypeData.TabIndex = 23;
            this.m_PtxTypeData.TabStop = false;
            this.m_PtxTypeData.Text = "Texture Info";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.m_ComboLightingMode);
            this.groupBox4.Controls.Add(this.m_ComboBlendMode);
            this.groupBox4.Controls.Add(this.m_CheckboxSort);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.m_ComboShader);
            this.groupBox4.Controls.Add(this.m_ButtonShaderVars);
            this.groupBox4.Controls.Add(this.m_CheckboxAlphaBlend);
            this.groupBox4.Controls.Add(this.m_CheckboxDepthTest);
            this.groupBox4.Controls.Add(this.m_CheckboxDepthWrite);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(272, 24);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(304, 176);
            this.groupBox4.TabIndex = 22;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Render Info";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 23);
            this.label3.TabIndex = 12;
            this.label3.Text = "Lighting Mode";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 23);
            this.label1.TabIndex = 11;
            this.label1.Text = "Blend Mode";
            // 
            // m_ComboLightingMode
            // 
            this.m_ComboLightingMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ComboLightingMode.Items.AddRange(new object[] {
            "None",
            "Directional",
            "Point"});
            this.m_ComboLightingMode.Location = new System.Drawing.Point(96, 48);
            this.m_ComboLightingMode.Name = "m_ComboLightingMode";
            this.m_ComboLightingMode.Size = new System.Drawing.Size(152, 21);
            this.m_ComboLightingMode.TabIndex = 10;
            this.m_ComboLightingMode.Text = "None";
            // 
            // m_ComboBlendMode
            // 
            this.m_ComboBlendMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ComboBlendMode.Items.AddRange(new object[] { 
            "Normal",
            "Add",
            "Subtract",
            "Lightmap",
            "Matte",
            "Overwrite",
            "Dest",
            "Alpha Add",        
            "Reverse Subtract",
            "Min",
            "Max",
            "Multiply",           
            "Hybrid Add"});
            this.m_ComboBlendMode.Location = new System.Drawing.Point(96, 24);
            this.m_ComboBlendMode.MaxDropDownItems = 12;
            this.m_ComboBlendMode.Name = "m_ComboBlendMode";
            this.m_ComboBlendMode.Size = new System.Drawing.Size(152, 21);
            this.m_ComboBlendMode.TabIndex = 9;
            this.m_ComboBlendMode.Text = "Normal";
            // 
            // m_CheckboxSort
            // 
            this.m_CheckboxSort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CheckboxSort.Location = new System.Drawing.Point(120, 96);
            this.m_CheckboxSort.Name = "m_CheckboxSort";
            this.m_CheckboxSort.Size = new System.Drawing.Size(104, 24);
            this.m_CheckboxSort.TabIndex = 8;
            this.m_CheckboxSort.Text = "Sort";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Shader:";
            // 
            // m_ComboShader
            // 
            this.m_ComboShader.Enabled = false;
            this.m_ComboShader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ComboShader.Items.AddRange(new object[] {
            "<None>",
            "PerPixel Lighting",
            "Glass",
            "HDR"});
            this.m_ComboShader.Location = new System.Drawing.Point(16, 144);
            this.m_ComboShader.Name = "m_ComboShader";
            this.m_ComboShader.Size = new System.Drawing.Size(121, 21);
            this.m_ComboShader.TabIndex = 6;
            this.m_ComboShader.Text = "<None>";
            // 
            // m_ButtonShaderVars
            // 
            this.m_ButtonShaderVars.Enabled = false;
            this.m_ButtonShaderVars.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonShaderVars.Location = new System.Drawing.Point(144, 144);
            this.m_ButtonShaderVars.Name = "m_ButtonShaderVars";
            this.m_ButtonShaderVars.Size = new System.Drawing.Size(104, 23);
            this.m_ButtonShaderVars.TabIndex = 5;
            this.m_ButtonShaderVars.Text = "Shader Variables";
            // 
            // m_CheckboxAlphaBlend
            // 
            this.m_CheckboxAlphaBlend.Checked = true;
            this.m_CheckboxAlphaBlend.CheckState = System.Windows.Forms.CheckState.Checked;
            this.m_CheckboxAlphaBlend.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CheckboxAlphaBlend.Location = new System.Drawing.Point(120, 72);
            this.m_CheckboxAlphaBlend.Name = "m_CheckboxAlphaBlend";
            this.m_CheckboxAlphaBlend.Size = new System.Drawing.Size(104, 24);
            this.m_CheckboxAlphaBlend.TabIndex = 3;
            this.m_CheckboxAlphaBlend.Text = "Alpha Blend";
            // 
            // m_CheckboxDepthTest
            // 
            this.m_CheckboxDepthTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CheckboxDepthTest.Location = new System.Drawing.Point(16, 96);
            this.m_CheckboxDepthTest.Name = "m_CheckboxDepthTest";
            this.m_CheckboxDepthTest.Size = new System.Drawing.Size(104, 24);
            this.m_CheckboxDepthTest.TabIndex = 2;
            this.m_CheckboxDepthTest.Text = "Depth Test";
            // 
            // m_CheckboxDepthWrite
            // 
            this.m_CheckboxDepthWrite.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CheckboxDepthWrite.Location = new System.Drawing.Point(16, 72);
            this.m_CheckboxDepthWrite.Name = "m_CheckboxDepthWrite";
            this.m_CheckboxDepthWrite.Size = new System.Drawing.Size(104, 24);
            this.m_CheckboxDepthWrite.TabIndex = 1;
            this.m_CheckboxDepthWrite.Text = "Depth Write";
            // 
            // m_ListKeyframeProperties
            // 
            this.m_ListKeyframeProperties.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.m_ListKeyframeProperties.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ListKeyframeProperties.Location = new System.Drawing.Point(8, 16);
            this.m_ListKeyframeProperties.Name = "m_ListKeyframeProperties";
            this.m_ListKeyframeProperties.Size = new System.Drawing.Size(240, 176);
            this.m_ListKeyframeProperties.TabIndex = 16;
            this.m_ListKeyframeProperties.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.m_ListKeyframeProperties_DrawItem);
            this.m_ListKeyframeProperties.DoubleClick += new System.EventHandler(this.m_ListKeyframeProperties_DoubleClick);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "Re Load";
            // 
            // menuItem1
            // 
            this.menuItem1.Index = -1;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem2,
            this.menuItem3,
            this.menuItem4});
            this.menuItem1.Text = "File";
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "Save";
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 2;
            this.menuItem4.Text = "Save As";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.m_LabelSortTime);
            this.groupBox1.Controls.Add(this.m_LabelDrawTime);
            this.groupBox1.Controls.Add(this.m_LabelUpdateTime);
            this.groupBox1.Controls.Add(this.m_LabelTotalPoints);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(8, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(256, 112);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Profiling";
            // 
            // m_LabelSortTime
            // 
            this.m_LabelSortTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelSortTime.Location = new System.Drawing.Point(8, 88);
            this.m_LabelSortTime.Name = "m_LabelSortTime";
            this.m_LabelSortTime.Size = new System.Drawing.Size(184, 16);
            this.m_LabelSortTime.TabIndex = 5;
            this.m_LabelSortTime.Text = "SortTime (ms)";
            // 
            // m_LabelDrawTime
            // 
            this.m_LabelDrawTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelDrawTime.Location = new System.Drawing.Point(8, 64);
            this.m_LabelDrawTime.Name = "m_LabelDrawTime";
            this.m_LabelDrawTime.Size = new System.Drawing.Size(184, 16);
            this.m_LabelDrawTime.TabIndex = 4;
            this.m_LabelDrawTime.Text = "DrawTime (cpu)";
            // 
            // m_LabelUpdateTime
            // 
            this.m_LabelUpdateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelUpdateTime.Location = new System.Drawing.Point(8, 38);
            this.m_LabelUpdateTime.Name = "m_LabelUpdateTime";
            this.m_LabelUpdateTime.Size = new System.Drawing.Size(184, 16);
            this.m_LabelUpdateTime.TabIndex = 3;
            this.m_LabelUpdateTime.Text = "Update Time (cpu)";
            // 
            // m_LabelTotalPoints
            // 
            this.m_LabelTotalPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_LabelTotalPoints.Location = new System.Drawing.Point(8, 18);
            this.m_LabelTotalPoints.Name = "m_LabelTotalPoints";
            this.m_LabelTotalPoints.Size = new System.Drawing.Size(184, 16);
            this.m_LabelTotalPoints.TabIndex = 0;
            this.m_LabelTotalPoints.Text = "Total Points: ";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.UpdateProfiling);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.m_SliderLifeBias);
            this.groupBox3.Controls.Add(this.m_SliderLife);
            this.groupBox3.Controls.Add(this.m_WorldspaceVel);
            this.groupBox3.Controls.Add(this.m_WorldspacePosition);
            this.groupBox3.Controls.Add(this.m_SliderPosVarZ);
            this.groupBox3.Controls.Add(this.m_SliderPosVarY);
            this.groupBox3.Controls.Add(this.m_SliderPosVarX);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.m_SliderVelVarZ);
            this.groupBox3.Controls.Add(this.m_SliderVelVarY);
            this.groupBox3.Controls.Add(this.m_SliderVelVarX);
            this.groupBox3.Controls.Add(this.m_SliderVelZ);
            this.groupBox3.Controls.Add(this.m_SliderVelY);
            this.groupBox3.Controls.Add(this.m_SliderVelX);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(592, 56);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(256, 288);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Birth Info";
            this.groupBox3.Visible = false;
            // 
            // m_SliderLifeBias
            // 
            this.m_SliderLifeBias.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderLifeBias.IsFloat = true;
            this.m_SliderLifeBias.Location = new System.Drawing.Point(8, 257);
            this.m_SliderLifeBias.Maximum = 1000F;
            this.m_SliderLifeBias.Minimum = -1000F;
            this.m_SliderLifeBias.Name = "m_SliderLifeBias";
            this.m_SliderLifeBias.Size = new System.Drawing.Size(152, 24);
            this.m_SliderLifeBias.Step = 0.01F;
            this.m_SliderLifeBias.TabIndex = 45;
            this.m_SliderLifeBias.Text = "Life Bias";
            this.m_SliderLifeBias.UseTrackbar = true;
            this.m_SliderLifeBias.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderLifeBias.ValueXPath = null;
            // 
            // m_SliderLife
            // 
            this.m_SliderLife.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderLife.IsFloat = true;
            this.m_SliderLife.Location = new System.Drawing.Point(8, 226);
            this.m_SliderLife.Maximum = 1000F;
            this.m_SliderLife.Minimum = -1000F;
            this.m_SliderLife.Name = "m_SliderLife";
            this.m_SliderLife.Size = new System.Drawing.Size(152, 24);
            this.m_SliderLife.Step = 0.01F;
            this.m_SliderLife.TabIndex = 44;
            this.m_SliderLife.Text = "Life";
            this.m_SliderLife.UseTrackbar = true;
            this.m_SliderLife.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderLife.ValueXPath = null;
            // 
            // m_WorldspaceVel
            // 
            this.m_WorldspaceVel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_WorldspaceVel.Location = new System.Drawing.Point(144, 72);
            this.m_WorldspaceVel.Name = "m_WorldspaceVel";
            this.m_WorldspaceVel.Size = new System.Drawing.Size(88, 32);
            this.m_WorldspaceVel.TabIndex = 41;
            this.m_WorldspaceVel.Text = "Worldspace Velocity";
            this.m_WorldspaceVel.Value = false;
            this.m_WorldspaceVel.ValueXPath = null;
            // 
            // m_WorldspacePosition
            // 
            this.m_WorldspacePosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_WorldspacePosition.Location = new System.Drawing.Point(144, 32);
            this.m_WorldspacePosition.Name = "m_WorldspacePosition";
            this.m_WorldspacePosition.Size = new System.Drawing.Size(88, 32);
            this.m_WorldspacePosition.TabIndex = 40;
            this.m_WorldspacePosition.Text = "Worldspace Position";
            this.m_WorldspacePosition.Value = false;
            this.m_WorldspacePosition.ValueXPath = null;
            // 
            // m_SliderPosVarZ
            // 
            this.m_SliderPosVarZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderPosVarZ.IsFloat = true;
            this.m_SliderPosVarZ.Location = new System.Drawing.Point(8, 88);
            this.m_SliderPosVarZ.Maximum = 1000F;
            this.m_SliderPosVarZ.Minimum = 0F;
            this.m_SliderPosVarZ.Name = "m_SliderPosVarZ";
            this.m_SliderPosVarZ.Size = new System.Drawing.Size(112, 24);
            this.m_SliderPosVarZ.Step = 0.01F;
            this.m_SliderPosVarZ.TabIndex = 32;
            this.m_SliderPosVarZ.Text = "Z";
            this.m_SliderPosVarZ.UseTrackbar = true;
            this.m_SliderPosVarZ.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderPosVarZ.ValueXPath = null;
            // 
            // m_SliderPosVarY
            // 
            this.m_SliderPosVarY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderPosVarY.IsFloat = true;
            this.m_SliderPosVarY.Location = new System.Drawing.Point(8, 64);
            this.m_SliderPosVarY.Maximum = 1000F;
            this.m_SliderPosVarY.Minimum = 0F;
            this.m_SliderPosVarY.Name = "m_SliderPosVarY";
            this.m_SliderPosVarY.Size = new System.Drawing.Size(112, 24);
            this.m_SliderPosVarY.Step = 0.01F;
            this.m_SliderPosVarY.TabIndex = 31;
            this.m_SliderPosVarY.Text = "Y";
            this.m_SliderPosVarY.UseTrackbar = true;
            this.m_SliderPosVarY.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderPosVarY.ValueXPath = null;
            // 
            // m_SliderPosVarX
            // 
            this.m_SliderPosVarX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderPosVarX.IsFloat = true;
            this.m_SliderPosVarX.Location = new System.Drawing.Point(8, 40);
            this.m_SliderPosVarX.Maximum = 1000F;
            this.m_SliderPosVarX.Minimum = 0F;
            this.m_SliderPosVarX.Name = "m_SliderPosVarX";
            this.m_SliderPosVarX.Size = new System.Drawing.Size(112, 24);
            this.m_SliderPosVarX.Step = 0.01F;
            this.m_SliderPosVarX.TabIndex = 30;
            this.m_SliderPosVarX.Text = "X";
            this.m_SliderPosVarX.UseTrackbar = true;
            this.m_SliderPosVarX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderPosVarX.ValueXPath = null;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 23);
            this.label4.TabIndex = 39;
            this.label4.Text = "Position Bias";
            // 
            // m_SliderVelVarZ
            // 
            this.m_SliderVelVarZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderVelVarZ.IsFloat = true;
            this.m_SliderVelVarZ.Location = new System.Drawing.Point(136, 192);
            this.m_SliderVelVarZ.Maximum = 1000F;
            this.m_SliderVelVarZ.Minimum = 0F;
            this.m_SliderVelVarZ.Name = "m_SliderVelVarZ";
            this.m_SliderVelVarZ.Size = new System.Drawing.Size(112, 24);
            this.m_SliderVelVarZ.Step = 0.01F;
            this.m_SliderVelVarZ.TabIndex = 38;
            this.m_SliderVelVarZ.Text = "Z";
            this.m_SliderVelVarZ.UseTrackbar = true;
            this.m_SliderVelVarZ.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderVelVarZ.ValueXPath = null;
            // 
            // m_SliderVelVarY
            // 
            this.m_SliderVelVarY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderVelVarY.IsFloat = true;
            this.m_SliderVelVarY.Location = new System.Drawing.Point(136, 168);
            this.m_SliderVelVarY.Maximum = 1000F;
            this.m_SliderVelVarY.Minimum = 0F;
            this.m_SliderVelVarY.Name = "m_SliderVelVarY";
            this.m_SliderVelVarY.Size = new System.Drawing.Size(112, 24);
            this.m_SliderVelVarY.Step = 0.01F;
            this.m_SliderVelVarY.TabIndex = 37;
            this.m_SliderVelVarY.Text = "Y";
            this.m_SliderVelVarY.UseTrackbar = true;
            this.m_SliderVelVarY.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderVelVarY.ValueXPath = null;
            // 
            // m_SliderVelVarX
            // 
            this.m_SliderVelVarX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderVelVarX.IsFloat = true;
            this.m_SliderVelVarX.Location = new System.Drawing.Point(136, 144);
            this.m_SliderVelVarX.Maximum = 1000F;
            this.m_SliderVelVarX.Minimum = 0F;
            this.m_SliderVelVarX.Name = "m_SliderVelVarX";
            this.m_SliderVelVarX.Size = new System.Drawing.Size(112, 24);
            this.m_SliderVelVarX.Step = 0.01F;
            this.m_SliderVelVarX.TabIndex = 36;
            this.m_SliderVelVarX.Text = "X";
            this.m_SliderVelVarX.UseTrackbar = true;
            this.m_SliderVelVarX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderVelVarX.ValueXPath = null;
            // 
            // m_SliderVelZ
            // 
            this.m_SliderVelZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderVelZ.IsFloat = true;
            this.m_SliderVelZ.Location = new System.Drawing.Point(8, 192);
            this.m_SliderVelZ.Maximum = 1000F;
            this.m_SliderVelZ.Minimum = -1000F;
            this.m_SliderVelZ.Name = "m_SliderVelZ";
            this.m_SliderVelZ.Size = new System.Drawing.Size(112, 24);
            this.m_SliderVelZ.Step = 0.01F;
            this.m_SliderVelZ.TabIndex = 35;
            this.m_SliderVelZ.Text = "Z";
            this.m_SliderVelZ.UseTrackbar = true;
            this.m_SliderVelZ.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderVelZ.ValueXPath = null;
            // 
            // m_SliderVelY
            // 
            this.m_SliderVelY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderVelY.IsFloat = true;
            this.m_SliderVelY.Location = new System.Drawing.Point(8, 168);
            this.m_SliderVelY.Maximum = 1000F;
            this.m_SliderVelY.Minimum = -1000F;
            this.m_SliderVelY.Name = "m_SliderVelY";
            this.m_SliderVelY.Size = new System.Drawing.Size(112, 24);
            this.m_SliderVelY.Step = 0.01F;
            this.m_SliderVelY.TabIndex = 34;
            this.m_SliderVelY.Text = "Y";
            this.m_SliderVelY.UseTrackbar = true;
            this.m_SliderVelY.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderVelY.ValueXPath = null;
            // 
            // m_SliderVelX
            // 
            this.m_SliderVelX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_SliderVelX.IsFloat = true;
            this.m_SliderVelX.Location = new System.Drawing.Point(8, 144);
            this.m_SliderVelX.Maximum = 1000F;
            this.m_SliderVelX.Minimum = -1000F;
            this.m_SliderVelX.Name = "m_SliderVelX";
            this.m_SliderVelX.Size = new System.Drawing.Size(112, 24);
            this.m_SliderVelX.Step = 0.01F;
            this.m_SliderVelX.TabIndex = 33;
            this.m_SliderVelX.Text = "X";
            this.m_SliderVelX.UseTrackbar = true;
            this.m_SliderVelX.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.m_SliderVelX.ValueXPath = null;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 42;
            this.label5.Text = "Velocity";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(136, 128);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 23);
            this.label6.TabIndex = 43;
            this.label6.Text = "Velocity Bias";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.m_ListKeyframeProperties);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(8, 434);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(256, 198);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Keyframes";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.m_ButtonShaderVar);
            this.groupBox5.Controls.Add(this.m_ButtonSetShader);
            this.groupBox5.Controls.Add(this.m_TextShaderName);
            this.groupBox5.Controls.Add(this.m_TextOnBirthEvent);
            this.groupBox5.Controls.Add(this.m_TextOnDeathEvent);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.m_CheckMatchDuration);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.m_ShaderLabel);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(8, 144);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(256, 288);
            this.groupBox5.TabIndex = 25;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Misc";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // m_ButtonShaderVar
            // 
            this.m_ButtonShaderVar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonShaderVar.Location = new System.Drawing.Point(14, 229);
            this.m_ButtonShaderVar.Name = "m_ButtonShaderVar";
            this.m_ButtonShaderVar.Size = new System.Drawing.Size(218, 23);
            this.m_ButtonShaderVar.TabIndex = 9;
            this.m_ButtonShaderVar.Text = "Tune Shader Vars";
            this.m_ButtonShaderVar.Click += new System.EventHandler(this.m_ButtonShaderVar_Click);
            // 
            // m_ButtonSetShader
            // 
            this.m_ButtonSetShader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonSetShader.Location = new System.Drawing.Point(14, 200);
            this.m_ButtonSetShader.Name = "m_ButtonSetShader";
            this.m_ButtonSetShader.Size = new System.Drawing.Size(218, 23);
            this.m_ButtonSetShader.TabIndex = 8;
            this.m_ButtonSetShader.Text = "Set Shader";
            this.m_ButtonSetShader.Click += new System.EventHandler(this.m_ButtonSetShader_Click);
            // 
            // m_TextShaderName
            // 
            this.m_TextShaderName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_TextShaderName.Location = new System.Drawing.Point(13, 176);
            this.m_TextShaderName.Name = "m_TextShaderName";
            this.m_TextShaderName.Size = new System.Drawing.Size(221, 20);
            this.m_TextShaderName.TabIndex = 6;
            // 
            // m_TextOnBirthEvent
            // 
            this.m_TextOnBirthEvent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_TextOnBirthEvent.Location = new System.Drawing.Point(12, 64);
            this.m_TextOnBirthEvent.Name = "m_TextOnBirthEvent";
            this.m_TextOnBirthEvent.Size = new System.Drawing.Size(222, 20);
            this.m_TextOnBirthEvent.TabIndex = 5;
            // 
            // m_TextOnDeathEvent
            // 
            this.m_TextOnDeathEvent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_TextOnDeathEvent.Location = new System.Drawing.Point(13, 107);
            this.m_TextOnDeathEvent.Name = "m_TextOnDeathEvent";
            this.m_TextOnDeathEvent.Size = new System.Drawing.Size(221, 20);
            this.m_TextOnDeathEvent.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 23);
            this.label8.TabIndex = 4;
            this.label8.Text = "On Death Spawn Effect";
            // 
            // m_CheckMatchDuration
            // 
            this.m_CheckMatchDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_CheckMatchDuration.Location = new System.Drawing.Point(12, 39);
            this.m_CheckMatchDuration.Name = "m_CheckMatchDuration";
            this.m_CheckMatchDuration.Size = new System.Drawing.Size(136, 24);
            this.m_CheckMatchDuration.TabIndex = 2;
            this.m_CheckMatchDuration.Text = "Match Point Duration";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 23);
            this.label7.TabIndex = 1;
            this.label7.Text = "On Birth Spawn Effect";
            // 
            // m_ShaderLabel
            // 
            this.m_ShaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ShaderLabel.Location = new System.Drawing.Point(10, 158);
            this.m_ShaderLabel.Name = "m_ShaderLabel";
            this.m_ShaderLabel.Size = new System.Drawing.Size(100, 23);
            this.m_ShaderLabel.TabIndex = 7;
            this.m_ShaderLabel.Text = "Shader Name";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(880, 24);
            this.menuStrip1.TabIndex = 26;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reloadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // reloadToolStripMenuItem
            // 
            this.reloadToolStripMenuItem.Enabled = false;
            this.reloadToolStripMenuItem.Name = "reloadToolStripMenuItem";
            this.reloadToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.reloadToolStripMenuItem.Text = "&Reload";
            this.reloadToolStripMenuItem.Click += new System.EventHandler(this.m_MenuItemReload_Activate);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.m_MenuItemSave_Activate);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.m_MenuItemSaveAs_Activate);
            // 
            // PtxRuleProperties
            // 
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.m_PtxTypeData);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "PtxRuleProperties";
            this.Size = new System.Drawing.Size(880, 640);
            this.groupBox4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		public override object GetStateData()
		{
			return ragWidgetPage.WidgetPagePlugIn.BasicGetStateData(this);
		}

		public override void SetStateData(object d)
		{
            m_PtxRuleName = "";
			m_WidgetRoot=null;
			FindWidgetHandles();
		}


		public void SavePtxRuleAs()
		{
			SaveFileDialog sfd = new SaveFileDialog();
			if(UserData.LastPtxRulePath.Length>0)
				sfd.InitialDirectory = UserData.LastPtxRulePath;
			sfd.AddExtension=true;
			sfd.Filter="PtxRule (*.ptxrule)|*.ptxrule";
			sfd.ValidateNames=true;
			sfd.OverwritePrompt=true;
			sfd.Title="Save As:";
			if (sfd.ShowDialog() == DialogResult.OK) 
			{
				UserData.LastPtxRulePath = System.IO.Path.GetDirectoryName(sfd.FileName);
				string oldname = m_WidgetPtxRuleName.String;
				string newname = System.IO.Path.GetFileNameWithoutExtension(sfd.FileName);
				m_WidgetPtxRuleName.String = newname;
				m_WidgetSave.Activate();
				m_WidgetPtxRuleName.String=oldname;
			}
		}
		private void UpdateProfiling(object sender, System.EventArgs e)
		{
			SetLabel(m_LabelTotalPoints, "Total Points: {0}", m_WidgetTotalPoints);
			SetLabel(m_LabelUpdateTime, "Update Time (cpu): {0:#0.000}", m_WidgetUpdateTime);
			SetLabel(m_LabelDrawTime, "Draw Time (cpu): {0:#0.000}", m_WidgetDrawTime);
			SetLabel(m_LabelSortTime, "Sort Time (cpu): {0:#0.000}", m_WidgetSortTime);
		}

		private void m_ListKeyframeProperties_DoubleClick(object sender, System.EventArgs e)
		{
            ViewPlugIn.Instance.CreateAndOpenKeyframeEditorView( m_ListKeyframeProperties.SelectedItem as WidgetGroup, "PtxRule: " + m_PtxRuleName + "/" );
		}

		private void m_MenuItemSave_Activate(object sender, System.EventArgs e)
		{
			m_WidgetSave.Activate();
		}

		private void m_MenuItemReload_Activate(object sender, System.EventArgs e)
		{
            m_WidgetLoad.Activate();
			ViewPlugIn.Instance.RebuildAllPanels();
		}

		private void groupBox5_Enter(object sender, System.EventArgs e)
		{
		
		}

		private void m_ButtonSetShader_Click(object sender, System.EventArgs e)
		{
            string path = m_WidgetSetShader.FindPath();
            EffectProperties.sm_Instance.RemoveBindings();
            EmitterProperties.sm_Instance.RemoveBindings();
            MainPtxWindow.sm_Instance.RemoveBindings();
            RemoveBindings();
            WidgetButton button = m_BankManager.FindFirstWidgetFromPath(path) as WidgetButton;
            if (button != null)
                button.Activate();
            ViewPlugIn.Instance.RebuildAllPanels();

            //m_WidgetSetShader.Activate();
            //ViewPlugIn.Instance.RebuildAllPanels();
		}

		private void m_MenuItemSaveAs_Activate(object sender, System.EventArgs e)
		{
			SavePtxRuleAs();
		}

		private void m_ListKeyframeProperties_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
		{
			ListItemData data = (ListItemData)m_ListItems[e.Index];
			e.DrawBackground(); 
			e.DrawFocusRectangle();
			Color c = data.m_Color;
 			e.Graphics.DrawString(data.m_Name,new Font(FontFamily.GenericSansSerif, 8.25f, FontStyle.Regular),new SolidBrush(c),e.Bounds); 

		}

        private void m_ButtonShaderVar_Click(object sender, EventArgs e)
        {
            ViewPlugIn.Instance.CreateAndOpenPtxShaderEditorView(m_PtxRuleName, m_TextShaderName.Text);
        }

	}
}
