﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace ragePerforceLinker
{
    public partial class mainForm : Form
    {
        // file path specified at invocation
        string filePath;
        
        // context menu used for the options
        ContextMenuStrip rootMenu = new ContextMenuStrip();

        // initialise the form
        public mainForm()
        {
            InitializeComponent();
        }

        // when loading form just add blank context menu and event handler for it popping up
        private void mainForm_Load(object sender, EventArgs e)
        {
            rootMenu.ShowImageMargin = false;
            rootMenu.Closed += new ToolStripDropDownClosedEventHandler(rootMenu_Closed);

            if (Environment.GetCommandLineArgs().Length == 2)
            {
                filePath = Environment.GetCommandLineArgs()[1].Replace("p4link:", "");
            }

            if (filePath != null)
            {
                rootMenu.Items.Add(new ToolStripMenuItem("Show", null, new EventHandler(this.rootMenu_Items_Show)));
                rootMenu.Items.Add(new ToolStripMenuItem("View", null, new EventHandler(this.rootMenu_Items_View)));
                rootMenu.Items.Add(new ToolStripMenuItem("Edit", null, new EventHandler(this.rootMenu_Items_Edit)));
            }

            rootMenu.Items.Add(new ToolStripMenuItem("Cancel", null, new EventHandler(this.rootMenu_Items_Cancel)));
        }

        private void mainForm_Activated(object sender, EventArgs e)
        {
            rootMenu.Show(System.Windows.Forms.Cursor.Position);
        }

        private void rootMenu_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        {
            this.Close();
        }
        
        private void rootMenu_Items_Show(object sender, EventArgs e)
        {
            Process runProgram = new Process();
            runProgram.StartInfo.FileName = "p4v.exe";
            runProgram.StartInfo.Arguments = "-cmd \"open " + filePath + "\"";
            runProgram.Start();
        }
        
        private void rootMenu_Items_View(object sender, EventArgs e)
        {
            Process runProgram = new Process();
            runProgram.StartInfo.FileName = "p4.exe";
            runProgram.StartInfo.Arguments = "sync \"" + filePath + "\"";
            runProgram.Start();
            runProgram.WaitForExit();

            runProgram = new Process();
            runProgram.StartInfo.FileName = "p4.exe";
            runProgram.StartInfo.Arguments = "where \"" + filePath + "\"";
            runProgram.StartInfo.RedirectStandardOutput = true;
            runProgram.StartInfo.UseShellExecute = false;
            runProgram.Start();
            string [] where = runProgram.StandardOutput.ReadLine().Split();
            runProgram.WaitForExit();

            if (where.Length == 3)
            {
                string localFilePath = where[2];
                runProgram = new Process();
                runProgram.StartInfo.FileName = localFilePath;
                runProgram.Start();
            }
        }
        
        private void rootMenu_Items_Edit(object sender, EventArgs e)
        {
            Process runProgram = new Process();
            runProgram.StartInfo.FileName = "p4.exe";
            runProgram.StartInfo.Arguments = "sync \"" + filePath + "\"";
            runProgram.Start();
            runProgram.WaitForExit();

            runProgram = new Process();
            runProgram.StartInfo.FileName = "p4.exe";
            runProgram.StartInfo.Arguments = "edit \"" + filePath + "\"";
            runProgram.Start();
            runProgram.WaitForExit();

            runProgram = new Process();
            runProgram.StartInfo.FileName = "p4.exe";
            runProgram.StartInfo.Arguments = "where \"" + filePath + "\"";
            runProgram.StartInfo.RedirectStandardOutput = true;
            runProgram.StartInfo.UseShellExecute = false;
            runProgram.Start();
            string[] where = runProgram.StandardOutput.ReadLine().Split();
            runProgram.WaitForExit();

            if (where.Length == 3)
            {
                string localFilePath = where[2];
                runProgram = new Process();
                runProgram.StartInfo.FileName = localFilePath;
                runProgram.Start();
            }
        }
        
        private void rootMenu_Items_Cancel(object sender, EventArgs e)
        {
            this.Close();
        }

        // no idea how this works but it hides the program from alt tab
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x80;
                return cp;
            }
        }
    }
}
