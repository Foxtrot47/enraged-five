// 
// /rageTextureGEOTool.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#pragma warning(push)
#pragma warning(disable : 4668)

#include <windows.h>
#include <comdef.h>
#include <vector>
#include <string>

#import <mscorlib.tlb> rename("ReportEvent","mgdReportEvent") rename("Assert","mgdAssert")
#import "..\rageGEODataSet\rageGEODataSet.tlb" named_guids raw_interfaces_only // imports from 'rage/base/tools/deprecated/rageGeoDataSet'

#pragma warning(pop)

#include "atl/string.h"
#include "file/asset.h"
#include "init/moduleSettings.h"
#include "system/main.h"
#include "system/param.h"

#include "rageTextureGEOLib/rageTextureGEOLib.h"

using namespace std;
using namespace rage;
using namespace rageGEODataSet;

//-----------------------------------------------------------------------------

PARAM(typepath,		"[rageTextureGEO] Path to where the TextureOptions geo type file is located");
PARAM(templatepath,	"[rageTextureGEO] Path to where the BaseTextureOptions geo template file is located");
PARAM(create,		"[rageTextureGEO] Creates a texture geo for the specified texture.");
PARAM(base,			"[rageTextureGEO] Specifies the base type for the type to be created.");
PARAM(update,		"[rageTextureGEO] Update the geo for the specified texture.");
PARAM(platform,		"[rageTextureGEO] Sets the current platform.");

//Parameters used by the rage texture converter.
PARAM(miplevels,	"[rageTextureGEO] No of mip map levels to create");
PARAM(hdrc,			"[rageTextureGEO] Specifies that the HDR exponent and offset values should be calculated from the input texture.");
PARAM(e,			"[rageTextureGEO] Sets the exposure for the input texture");
PARAM(etlist,		"[rageTextureGEO] Specifies a list of textures and their corresponding exposures. E.g. dirtA.tif,0.0,dirtB.tif,1.0 ...");
PARAM(mipsharp,		"[rageTextureGEO] Enables mip-map sharpening");

//-----------------------------------------------------------------------------

#define CHECK_COM_ERROR_RETURN(_hr,_errRet)			\
	{	if(FAILED(_hr))							    \
		{										    \
			DisplayCOMError(_hr);					\
			return _errRet;							\
		}										    \
	}

#define CHECK_COM_ERROR_AND_IPTR_RETURN(_hr,_iPtr,_errRet)			\
	{	if(FAILED(_hr))												\
		{															\
			DisplayCOMError(_hr);									\
			return _errRet;											\
		}															\
		if(!_iPtr.GetInterfacePtr())								\
		{															\
			Errorf("Interface pointer check failed in file: \"%s\" on line: %d", __FILE__, __LINE__);	\
		}															\
	}

//-----------------------------------------------------------------------------

void DisplayCOMError(HRESULT hr)
{
	LPTSTR lpMsgBuf;
	FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			hr,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
			(LPTSTR) &lpMsgBuf,
			0,
			NULL
	);
	
	_bstr_t strMsg(lpMsgBuf);
	Errorf("%s",((const char*)strMsg));
	LocalFree( lpMsgBuf );
}

//-----------------------------------------------------------------------------

SAFEARRAY* CreateAndFillSafeArray(_bstr_t* pSourceData, int count)
{
	HRESULT hr;
	SAFEARRAY* pSA;

	SAFEARRAYBOUND rgsabound[1];
	rgsabound[0].lLbound = 0;
	rgsabound[0].cElements = count;
	pSA = SafeArrayCreate(VT_BSTR, 1, rgsabound);

	for(LONG i=0; i<(LONG)count; i++)
		hr = SafeArrayPutElement(pSA, &i, pSourceData[i].GetBSTR());

	return pSA;
}

//-----------------------------------------------------------------------------

bool SetFloatOptionsMember(IGeoTypePtr& texOptInst, const char* memberName, float val)
{
	HRESULT hr;

	IGeoTypeMemberBasePtr pMbrBase;
	hr = texOptInst->GetMemberByName(_bstr_t(memberName), &pMbrBase);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrBase, false);

	IGeoTypeMemberFloatPtr pMbrFloat;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberFloat, &pMbrFloat);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrFloat, false);

	pMbrFloat->put_Value(val);

	pMbrBase->put_UseInheritedValue(false);
	texOptInst->put_Dirty(true);

	return true;
}

//-----------------------------------------------------------------------------

bool SetIntOptionsMember(IGeoTypePtr& texOptInst, const char* memberName, int val)
{
	HRESULT hr;
	IGeoTypeMemberBasePtr pMbrBase;
	hr = texOptInst->GetMemberByName(_bstr_t(memberName), &pMbrBase);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrBase, false);

	IGeoTypeMemberIntPtr pMbrInt;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberInt, &pMbrInt);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrInt, false);

	pMbrInt->put_Value(val);

	pMbrBase->put_UseInheritedValue(false);
	texOptInst->put_Dirty(true);

	return true;
}

//-----------------------------------------------------------------------------

bool SetBoolOptionsMember(IGeoTypePtr& texOptInst, const char* memberName, bool val)
{
	HRESULT hr;
	IGeoTypeMemberBasePtr pMbrBase;
	hr = texOptInst->GetMemberByName(_bstr_t(memberName), &pMbrBase);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrBase, false);

	IGeoTypeMemberBoolPtr pMbrBool;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberBool, &pMbrBool);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrBool, false);

	pMbrBool->put_Value(val);

	pMbrBase->put_UseInheritedValue(false);
	texOptInst->put_Dirty(true);

	return true;
}

//-----------------------------------------------------------------------------

bool SetFileOptionsMember(IGeoTypePtr& texOptInst, const char* memberName, std::vector< std::string >& val)
{
	HRESULT hr;

	//Get the specified member.  This member needs to be an array type.
	IGeoTypeMemberBasePtr pMbrBase;
	hr = texOptInst->GetMemberByName(_bstr_t(memberName), &pMbrBase);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrBase, false);

	IGeoTypeMemberArrayPtr pMbrArr;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberArray, &pMbrArr);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrArr, false);

	//Get the template object for the array
	IGeoTemplateMemberBasePtr pMbrBaseTemplate;
	hr = pMbrBase->get_TemplateMemberBase(&pMbrBaseTemplate);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrBaseTemplate, false);

	IGeoTemplateMemberArrayPtr pMbrBaseArrTemplate;
	hr = pMbrBaseTemplate.QueryInterface(IID_IGeoTemplateMemberArray, &pMbrBaseArrTemplate);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrBaseArrTemplate, false);

	//Get the template object for the items in the array
	IGeoTemplateMemberBasePtr pMbrArrayChildBaseTemplate;
	hr = pMbrBaseArrTemplate->get_ChildTemplate(&pMbrArrayChildBaseTemplate);
	CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrArrayChildBaseTemplate, false);

	int valCount = val.size();
	for(int i=0; i<valCount; i++)
	{
		//Create a new element that can be inserted into the array
		IGeoTypeMemberBasePtr pArrayMbrBase;
		hr = pMbrArrayChildBaseTemplate->CreateTypeMember(texOptInst, &pArrayMbrBase);
		CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pArrayMbrBase, false);

		//Set the value of the string element
		IGeoTypeMemberFilePtr pMbrFile;
		hr = pArrayMbrBase.QueryInterface(IID_IGeoTypeMemberFile, &pMbrFile);
		CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrFile, false);

		pMbrFile->put_Value(_bstr_t(val[i].c_str()));

		//Add the element to the array type
		IGeoTypeMemberBasePtr pMbrFileBase;
		hr = pMbrFile.QueryInterface(IID_IGeoTypeMemberBase, &pMbrFileBase);
		CHECK_COM_ERROR_AND_IPTR_RETURN(hr, pMbrFileBase, false);

		pMbrArr->AddElement(pMbrFileBase);
	}

	pMbrBase->put_UseInheritedValue(false);
	texOptInst->put_Dirty(true);

	return true;
}

//-----------------------------------------------------------------------------

bool SetTextureOptions(IGeoTypePtr& texOptInst, const char* platform)
{
	HRESULT hr;
	bool	bDirty = false;

	//Get the platform options type
	IGeoTypePtr				pPlatformType;
	IGeoTypeMemberBasePtr	pMbrBase;
	
	if(strcmpi(platform, PLATFORM_PC)==0)
	{
		hr = texOptInst->GetMemberByName(_bstr_t("PC"), &pMbrBase);
		CHECK_COM_ERROR_RETURN(hr, false);
	}
	else if(strcmpi(platform, PLATFORM_XBOX360)==0)
	{
		hr = texOptInst->GetMemberByName(_bstr_t("Xbox360"), &pMbrBase);
		CHECK_COM_ERROR_RETURN(hr, false);
	}
	else if(strcmpi(platform, PLATFORM_PS3)==0)
	{
		hr = texOptInst->GetMemberByName(_bstr_t("PS3"), &pMbrBase);
		CHECK_COM_ERROR_RETURN(hr, false);
	}

	IGeoTypeMemberGeoPtr pMbrGeo;
	hr = pMbrBase.QueryInterface(IID_IGeoTypeMemberGeo, &pMbrGeo);
	CHECK_COM_ERROR_RETURN(hr, false);

	hr = pMbrGeo->get_GeoTypeThatIContain(&pPlatformType);
	CHECK_COM_ERROR_RETURN(hr, false);

	//Mip-Levels
	if(PARAM_miplevels.Get())
	{
		int mipLevels = 0;
		PARAM_miplevels.Get(mipLevels);

		SetIntOptionsMember(pPlatformType, "MipLevels", mipLevels);
		bDirty = true;
	}

	//Mip Sharpening
	if(PARAM_mipsharp.Get())
	{
		SetBoolOptionsMember(texOptInst, "SharpenMipMaps", true);
		bDirty = true;
	}

	//Exposure
	if(PARAM_e.Get())
	{
		float exposure = 0.0f;
		PARAM_e.Get(exposure);

		SetFloatOptionsMember(texOptInst, "Exposure", exposure);
		bDirty = true;
	}

	//Compute HDR offset/scale
	if(PARAM_hdrc.Get())
	{
		bool bVal = true;
		SetBoolOptionsMember(texOptInst, "CalcHDRScaleAndBias", bVal);
		bDirty = true;
	}

	//Exposure textures and exposure values.
	if(PARAM_etlist.Get())
	{
		std::vector< std::string > expTexPaths;

		const char* etVals[32];
		char etValBuffer[2048];
		int etValCount = PARAM_etlist.GetArray(&etVals[0], 32, etValBuffer, 2048);
		
		int etIdx = 0;
		while(etIdx < etValCount)
		{
			expTexPaths.push_back( etVals[etIdx++] );
		}
		
		SetFileOptionsMember(texOptInst, "ExposureTextures", expTexPaths);
		bDirty = true;
	}

	//If we actually did change something in the options file, then mark it as dirty
	if(bDirty)
	{
		pMbrBase->put_UseInheritedValue(false);
		texOptInst->put_Dirty(true);
	}

	return true;
}

//-----------------------------------------------------------------------------

bool Init(IGeoDataSetPtr& pGeoDataSet)
{
	HRESULT hr;

	INIT_PARSER;
	INIT_TEXTUREOPTIONS;

	//Retrieve the path to the template and type files.  First trying the module settings file, then try the command line.
	//Note that anything explicitly specified on the command line will override the module settings information.
	char texOptionsTemplatePath[MAX_PATH];
	memset(texOptionsTemplatePath, 0, MAX_PATH);
	char texOptionsTypePath[MAX_PATH];
	memset(texOptionsTypePath, 0, MAX_PATH);

	atString baseGEOPath;
	if(initModuleSettings::GetModuleSetting("BaseGEOPath", baseGEOPath))
	{
		if( (!baseGEOPath.EndsWith("/")) &&
			(!baseGEOPath.EndsWith("\\")) )
			baseGEOPath += "/";

		sprintf(texOptionsTemplatePath, "%sGEOTemplates", (const char*)baseGEOPath);
		sprintf(texOptionsTypePath, "%sGEOTypes", (const char*)baseGEOPath);
	}

	const char* cmdLineTemplatePath = NULL;
	if(PARAM_templatepath.Get(cmdLineTemplatePath))
	{
		if(strlen(cmdLineTemplatePath) != 0)
		{
			strcpy(texOptionsTemplatePath, cmdLineTemplatePath);
			Displayf("Using cmd line template path \"%s\"", texOptionsTemplatePath);
		}
	}

	const char* cmdLineTypePath = NULL;
	if(PARAM_typepath.Get(cmdLineTypePath))
	{
		if(strlen(cmdLineTypePath) != 0)
		{
			strcpy(texOptionsTypePath, cmdLineTypePath);
			Displayf("Using cmd line type path \"%s\"", texOptionsTypePath);
		}
	}

	if( strlen(texOptionsTemplatePath) && strlen(texOptionsTypePath) )
	{
		TEXTUREOPTIONS.SetTextureGeoTemplateDirectory(texOptionsTemplatePath);
		TEXTUREOPTIONS.SetTextureGeoTypeDirectory(texOptionsTypePath);
	}
	else
	{
		Errorf("Failed to locate Texture Options GEO type and/or template path(s)");
		return false;
	}

	//Create the helper object...
	IGeoDataSetHelperPtr pDataSetHelper;
	hr = pDataSetHelper.CreateInstance(CLSID_GeoDataSetHelper);
	CHECK_COM_ERROR_RETURN(hr, false);
	if(!pDataSetHelper.GetInterfacePtr())
	{
		Errorf("Failed to create instance of GeoDataSetHelper object.");
		return false;
	}

	//Determine what type type and template files we need to load
	const char* baseTypeName = TEXTURE_OPTIONS_TYPE_NAME;
	if(PARAM_base.Get())
	{
		PARAM_base.Get(baseTypeName);
	}

	_bstr_t bstrInputFiles[1];
	bstrInputFiles[0] = texOptionsTypePath;
	bstrInputFiles[0] += "\\";
	bstrInputFiles[0] += baseTypeName;
	bstrInputFiles[0] += ".geotype";

	SAFEARRAY *pInputFilesSA = CreateAndFillSafeArray(bstrInputFiles, 1);
	if(!pInputFilesSA)
		return false;

	SAFEARRAYBOUND rgsabound[1];
	rgsabound[0].lLbound = 0;
	rgsabound[0].cElements = 64;

	SAFEARRAY* pTemplatesSA;
	pTemplatesSA = SafeArrayCreate(VT_BSTR, 1, rgsabound);

	SAFEARRAY* pTypesSA;
	pTypesSA = SafeArrayCreate(VT_BSTR, 1, rgsabound);

	SAFEARRAY* pInstancesSA;
	pInstancesSA = SafeArrayCreate(VT_BSTR, 1, rgsabound);

	pDataSetHelper->BuildDataSetFileListsFromFiles( _bstr_t(texOptionsTemplatePath), _bstr_t(texOptionsTypePath),
		pInputFilesSA, &pTemplatesSA, &pTypesSA, &pInstancesSA);

	//Create the GEO Data Set
	if(pGeoDataSet.GetInterfacePtr())
	{
		pGeoDataSet->Release();
		pGeoDataSet.Detach();
	}
	
	hr = pGeoDataSet.CreateInstance(CLSID_GeoDataSet);
	if(FAILED(hr))
	{
		DisplayCOMError(hr);
		return false;
	}
	else if(!pGeoDataSet.GetInterfacePtr())
	{
		Errorf("Failed to create instance of GeoDataSet object.");
		return false;
	}

	//Load the necessary type and template files into the data set
	hr = pGeoDataSet->LoadFromFileLists(pTemplatesSA, pTypesSA, pInstancesSA);
	
	//Clean up
	SafeArrayDestroy(pTemplatesSA);
	SafeArrayDestroy(pTypesSA);
	SafeArrayDestroy(pInstancesSA);
	SafeArrayDestroy(pInputFilesSA);
	
	CHECK_COM_ERROR_RETURN(hr, false);

	return true;
}

//-----------------------------------------------------------------------------

int Main(void)
{
	HRESULT hr;
	CoInitialize(NULL);

	IGeoDataSetPtr	pGeoDataSet;
	
	if(!Init(pGeoDataSet))
	{
		Errorf("Failed to initialize required subsystems, execution aborted.");
		return 1;
	}

	const char* platform = PLATFORM_PC;
	PARAM_platform.Get(platform);

	//Update a texture options file.
	const char* updateSrcTex = NULL;
	if(PARAM_update.Get(updateSrcTex))
	{
		if(strlen(updateSrcTex) == 0)
		{
			Errorf("Argument -update was specified but a path to the texture to update the options file for was not given, execution aborted.");
			return 1;
		}

		char pathBuf[MAX_PATH];
		TEXTUREOPTIONS.TexturePathToOptionsFilePath(pathBuf, MAX_PATH, updateSrcTex);

		DWORD fAttr = GetFileAttributes(pathBuf);
		if( fAttr == 0xFFFFFFFF)
		{
			Errorf("Cannot update \"%s\", the file does not exist, execution aborted.", pathBuf);
			return 1;
		}
		if( fAttr & FILE_ATTRIBUTE_READONLY )
		{
			Errorf("Cannot update \"%s\", the file is marked as read only, execution aborted.", pathBuf);
			return 1;
		}

		IGeoTypePtr pTextureOptions;
		hr = pGeoDataSet->LoadInstanceFromFile(_bstr_t(pathBuf), &pTextureOptions);
		CHECK_COM_ERROR_RETURN(hr, 1);
		if(!pTextureOptions.GetInterfacePtr())
		{
			Errorf("Failed to load the options file \"%s\", execution aborted.", pathBuf);
			return 1;
		}

		SetTextureOptions(pTextureOptions, platform);

		pTextureOptions->SaveAs(_bstr_t(pathBuf));
	}

	//Create a texture options file.
	const char* createSrcTex = NULL;
	if(PARAM_create.Get(createSrcTex))
	{
		if(strlen(createSrcTex) == 0)
		{
			Errorf("Argument -create was specified but a path to the texture to create an options file for was not given, execution aborted.");
			return 1;
		}

		char baseName[MAX_PATH];
		ASSET.RemoveExtensionFromPath(baseName, MAX_PATH, ASSET.FileName(createSrcTex));

		IGeoTypePtr pTextureType;

		const char* baseTypeName = TEXTURE_OPTIONS_TYPE_NAME;
		if(PARAM_base.Get())
		{
			PARAM_base.Get(baseTypeName);
		}
			
		hr = pGeoDataSet->GetTopLevelType(_bstr_t(baseTypeName), &pTextureType);
		if(FAILED(hr))
		{
			DisplayCOMError(hr);
			return 1;
		}
		else if(!pTextureType.GetInterfacePtr())
		{
			Errorf("Failed to locate GEO type named \"%s\"", baseTypeName);
			return 1;
		}

		char pathBuf[MAX_PATH];
		TEXTUREOPTIONS.TexturePathToOptionsFilePath(pathBuf, MAX_PATH, createSrcTex);

		u32 fileAttrib = ASSET.GetAttributes(pathBuf, "geo");
		if(fileAttrib == FILE_ATTRIBUTE_READONLY)
		{
			Errorf("Cannot write to file '%s', file is marked as read-only.");
			return 1;
		}
		
		IGeoTypePtr pInstance;
		hr = pGeoDataSet->CreateChildTypeFromTypeWithName(pTextureType, true, _bstr_t(TEXTURE_OPTIONS_INST_NAME), &pInstance);
		if(FAILED(hr))
		{
			DisplayCOMError(hr);
			return 1;
		}
		pInstance->put_FileName(_bstr_t(pathBuf));
		pInstance->put_Name(_bstr_t(baseName));

		SetTextureOptions(pInstance, platform);

		pInstance->SaveAs(_bstr_t(pathBuf));
	}

	SHUTDOWN_TEXTUREOPTIONS;
	SHUTDOWN_PARSER;
	CoUninitialize();
	return 0;
}

