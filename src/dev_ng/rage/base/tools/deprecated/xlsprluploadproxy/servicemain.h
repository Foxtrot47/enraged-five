// 
// servicemain.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

//When 0, we run as a win32 app.  When 1, we run as a service.
//Services are difficult to debug, so during development running
//as an application is very useful.
#define __WIN32SERVICE 1

//External functions that must be defined somewhere by title, 
//regardless of whether running as a service or app.
extern bool ServiceInit();
extern void ServiceShutdown();
extern bool ServiceUpdate();


#if __WIN32SERVICE

//Name of service, filled in when the service starts.  Useful for debugging,
//prefixing logfiles, etc.
extern char g_ServiceName[256];

#endif
