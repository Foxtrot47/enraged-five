//#include "../matrix/mx_util.h"
#include "system/criticalsection.h"
#define MAX_QUEUE_MESSAGE_LEN 32768
#ifndef UNDER_UNIX
#pragma comment (lib, "wsock32")
#endif
#include <string.h>
#include <stdio.h>
#ifndef NO_PERF_QUEUE
#include "../matrix/mx_queue.h"
MXWriteQueue perfQueue;
#endif
#undef _strdup

#ifdef UNDER_UNIX
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#define WINAPI
#define _strdup strdup
typedef int DWORD;
#endif

static const int SELECT_TIMEOUT_SEC = 0;
static const int SELECT_TIMEOUT_USEC = 500000;

char AppName[256];
TCHAR MachineName[256];
struct MXCriticalSection
{
    MXCriticalSection()
        : m_Cs(m_CsToken)
    {
    }
    void Enter(){m_Cs.Enter();}
    void Leave(){m_Cs.Exit();}
    rage::sysCriticalSectionToken m_CsToken;
    rage::sysCriticalSection m_Cs;
};
#define strncpyz rage::safecpy
#define snprintf _snprintf
MXCriticalSection recordProtect;



typedef struct _PerfRecord
{
	char shortName[50];
	char counter[256];
	char instance[256];
	int lastValue;
} PerfRecord;

// Use #ifndef so this can be overriden in the project settings
#ifndef MAX_PERF_STATS
#define MAX_PERF_STATS 50
#endif

// Use #ifndef so this can be overriden in the project settings
#ifndef SEND_BUF_SIZE
#define SEND_BUF_SIZE 8192
#endif


PerfRecord perfStats[MAX_PERF_STATS];
int numPerfStats = 0;
char *okStatus = "OK";
char *lastStatus = okStatus;


SOCKET s;
bool stopWorker;
bool queueInit = false;
bool tcpInit = false;
DWORD lastPerfLog ;

#ifdef UNDER_UNIX
	pthread_t thread;
#else
	HANDLE thread;
#endif

#ifndef NO_PERF_QUEUE
void InitPerfLib(char *queueMachine, char *queueName, char *applicationName)
{
	DWORD nSize = sizeof(MachineName);
	if (queueName == NULL)
		queueName = "perfqueue";
	perfQueue.OpenQueue(queueMachine, queueName);
	strncpyz(AppName, applicationName, sizeof(AppName));
	GetComputerName(MachineName, &nSize);
	queueInit = true;
	lastPerfLog = GetTickCount();
}
#endif 

void LogIntPerfData(char *shortname, char *counter, char *instance, int value)
{
	recordProtect.Enter();

	lastPerfLog = GetTickCount();
	if (instance == NULL)
		instance = "";

#ifndef NO_PERF_QUEUE
	if (queueInit)
	{
		char sql[1024];
		SYSTEMTIME st;
		GetLocalTime(&st);
		sprintf(sql, "EXEC LogPerfEntry '%d/%d/%d %d:%d:%d','%s','%s','%s','%s',%d",
			st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, MachineName, AppName, counter, instance, value);
		perfQueue.SendMsg(L"Perf Entry", sql, (int)strlen(sql) + 1, 0, 3, 0);
	}
#endif
	//find it in our list..
	int i;
	for (i = 0 ; i < numPerfStats ; i++)
	{
		if (strcmp(shortname, perfStats[i].shortName) == 0  && strcmp(instance, perfStats[i].instance) == 0)
			break;
	}
	if (i == numPerfStats) //need to allocate a new one
	{
		if (numPerfStats == MAX_PERF_STATS) //overwrite the last one..
			numPerfStats--;
		strncpyz(perfStats[numPerfStats].shortName, shortname, sizeof(perfStats[0].shortName));
		strncpyz(perfStats[numPerfStats].counter, counter, sizeof(perfStats[0].counter));
		strncpyz(perfStats[numPerfStats].instance, instance, sizeof(perfStats[0].instance));
		numPerfStats++;
	}
	perfStats[i].lastValue = value;

	recordProtect.Leave();
}


char *BuildSendBuffer(char *buf)
{
	recordProtect.Enter();

	int buflen = 0;
	int reserved = 100; // reserve 100 bytes at the end so we can write UpdateAge and close tag
	int bytesleft = SEND_BUF_SIZE - reserved;

	buflen = snprintf(buf, bytesleft, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<perfdata appname=\"%s\" machinename=\"%s\">\n<status>%s</status>\n", AppName, MachineName, lastStatus);
	bytesleft -= buflen;

	for (int i = 0 ; i < numPerfStats && bytesleft > 0; i++)
	{
		// Do a length precheck so that we don't write truncated XML to the buffer
		int linelen = (int)strlen(perfStats[i].shortName)*2 + (int)strlen(perfStats[i].counter) 
			+ (int)strlen(perfStats[i].instance) + 10 + 30; // 10 = strlen(INT_MAX)

		if (bytesleft > linelen)
		{
			int byteswritten = snprintf(buf + buflen, bytesleft, "<%s counter=\"%s\" instance=\"%s\">%d</%s>\n",
				perfStats[i].shortName, perfStats[i].counter, perfStats[i].instance, perfStats[i].lastValue,
				perfStats[i].shortName);
			buflen    += byteswritten;
			bytesleft -= byteswritten;
		}
	}

	// Add back in the reserved space, this write will not overrun
	bytesleft += reserved;
	buflen += sprintf(buf + buflen, "<%s counter=\"%s\" instance=\"%s\">%d</%s>\n",
		"updateage", "Update Age", "", (int)((GetTickCount() - lastPerfLog) / 1000),
			"updateage");
	bytesleft -= buflen;
	strcpy(buf + buflen, "</perfdata>\n");

	recordProtect.Leave();

	return buf;
}


DWORD WINAPI TCPPerfWorker(void* /*ignore*/)
{
	SOCKET cs;
	char sendBuffer[SEND_BUF_SIZE];
	fd_set readSet;
	int nfds = (int)s + 1;
	int result;
	struct timeval timeout = { SELECT_TIMEOUT_SEC, SELECT_TIMEOUT_USEC };
	FD_ZERO(&readSet);
	while (!stopWorker)
	{
		cs = INVALID_SOCKET;	
		try
		{
			FD_SET(s, &readSet);
			result = select(nfds, &readSet, NULL, NULL, &timeout);
			if ((result > 0) && (FD_ISSET(s, &readSet)))
			{
				cs = accept(s,NULL,0);
				if (cs == INVALID_SOCKET)
					continue;
				BuildSendBuffer(sendBuffer);
				send(cs,sendBuffer,(int)strlen(sendBuffer),0);
				shutdown(cs,2);
			}
		}
		catch (...) //in case anything inside goes wonky
		{
		}
		if (cs != INVALID_SOCKET)
			closesocket(cs);
	}
	return 0;
}

void ClearPerfErrorState()
{
	recordProtect.Enter();

	if (lastStatus != okStatus)
		free(lastStatus);
	lastStatus = okStatus;

	recordProtect.Leave();
}


bool InitTCPPerfLib(const char *localAddr, int sentryPort, const char *applicationName)
{
	DWORD nSize = sizeof(MachineName);


	int ret;
	int ipad;
	struct sockaddr_in saddr;

	strncpyz(AppName, applicationName, sizeof(AppName));
#ifdef UNDER_UNIX
	gethostname(MachineName, nSize);
	char* end;
	if ((end = strchr(MachineName, '.')) != NULL) *end = 0; // trim off domain
#else
	GetComputerName(MachineName, &nSize);
#endif
	ClearPerfErrorState();
	ipad = (localAddr && localAddr[0]) ? inet_addr(localAddr) : 0;

	s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (s == INVALID_SOCKET)
		return false;
#ifdef UNDER_UNIX
	ret = fcntl(s, F_SETFL, O_NONBLOCK);
	if (ret == -1)
		return false;
#else
	u_long arg = 1;
	ret = ioctlsocket(s, FIONBIO, &arg);
	if (ret != 0)
		return false;
#endif
	saddr.sin_addr.s_addr = ipad;
	saddr.sin_port = htons((unsigned short)sentryPort);
	saddr.sin_family = AF_INET;
	ret = bind(s, (struct sockaddr *)&saddr, sizeof(saddr));
	if (ret != 0)
		return false;// 0;
	ret = listen(s, 10);
	if (ret != 0)
		return false;// 0;

	stopWorker = false;
#ifdef UNDER_UNIX
	pthread_create(&thread, NULL, (void*(*)(void*))TCPPerfWorker, NULL);
#else
	DWORD tid;
	thread = CreateThread(NULL, 0, TCPPerfWorker, 0, 0, &tid);
#endif

	tcpInit = true;
	lastPerfLog = GetTickCount();
	
	return true;

}

void FreeTCPPerfLib()
{
	stopWorker = true;
#ifdef UNDER_UNIX
	pthread_join(thread, NULL);
#else
	WaitForSingleObject(thread, INFINITE);
	CloseHandle(thread);
#endif
	if(s != INVALID_SOCKET)
	{
		closesocket(s);
		s = INVALID_SOCKET;
	}
}

void SetPerfErrorState(const char *errorStr)
{
	recordProtect.Enter();

    ClearPerfErrorState();

	lastStatus = _strdup(errorStr);

	recordProtect.Leave();
}


#ifdef TEST_APP
int main(int argc, char **argv)
{
	WSADATA data;
	WSAStartup(0x0101, &data);
	//InitPerfLib("kroq", "perfqueue", "testapp");
	LogIntPerfData("tc", "testcounter","testinstance", 5);
	//LogIntPerfData("tc", "testcounter","testinstance", 10);
	InitTCPPerfLib(NULL, 6911, "testapp");
	SetPerfErrorState("fux0r3d");
	Sleep(-1);
	WSACleanup();
	return 0;
}

#endif
