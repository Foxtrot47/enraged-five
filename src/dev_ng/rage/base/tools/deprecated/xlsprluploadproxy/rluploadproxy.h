// 
// rlUploadProxy
//
// An XLSP title server that acts as a proxy between Sake and rlUpload on Xenon consoles.
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLUPLOADPROXY_H
#define RLUPLOADPROXY_H

#if __WIN32PC

#include "commandserver.h"
#include "rline/rlupload.h"
#include "net/nethardware.h"
#include "atl/inlist.h"
#include "atl/inmap.h"
#include "system/criticalsection.h"
#include "system/simpleallocator.h"

#define WIN32_LEAN_AND_MEAN
#include "system/xtl.h" //NOTE(RDT): Use instead of windows.h
#include <winsock.h>

namespace rage
{

class rlUploadProxy
{
public:
    rlUploadProxy();
    ~rlUploadProxy();

    bool Init();
    void Shutdown();
    bool Update();

private:
    void Reset();
    
    bool InitNet();
    bool InitRline();
    //bool InitTcp();
    bool InitNorad();
    bool InitTelnet();

    bool Start();
    void Stop();

    void CheckCompletedRequests();
    void CheckTimedOutConnections();  
    void SendToClients();

private:
    static const unsigned INVALID_CLIENTID = 0xFFFFFFFF;

    //Message queued to be sent to a client.
    struct OutboundMsg
    {
        OutboundMsg();
        ~OutboundMsg();

        bool Init(const void* data, const unsigned size);

        u8* m_MsgBuf;
        unsigned m_MsgSize;

        inlist_node<OutboundMsg> m_ListLink;
    };
    typedef inlist<OutboundMsg, &OutboundMsg::m_ListLink> OutboundMsgList;

    //Current state of a client connection
    struct ClientRec
    {
        ClientRec();
        ~ClientRec();

        unsigned    m_ClientId;
        netAddress  m_Address;
        unsigned    m_NumRequestsInProgress;
        unsigned    m_LastActiveTime;

        SOCKET      m_Socket;
        
        //Receiving
        u8*         m_MsgBuf;
        unsigned    m_MsgBytesRead;
        unsigned    m_MsgSize;
        unsigned    m_MsgSizeBytesRead;

        //Sending
        OutboundMsgList m_OutboundMsgList;
        unsigned        m_MsgBytesSent;

        inmap_node<unsigned, ClientRec> m_ByIdLink;
    };
    typedef inmap<unsigned, ClientRec, &ClientRec::m_ByIdLink> ClientMap;
    ClientMap m_ClientMap;

    //Request that client has sent to us
    enum eRequestType
    {
        REQUEST_INVALID = -1,
        REQUEST_GET_TABLE_SIZE,
        REQUEST_GET_NUM_OWNED,
        REQUEST_READ_RECORDS_BY_GAMER,
        REQUEST_READ_RECORDS_BY_RANK,
        REQUEST_READ_RANDOM_RECORD,
        REQUEST_READ_RECORD,
        REQUEST_RATE_RECORD,
        REQUEST_DELETE_RECORD,
        REQUEST_UPLOAD_FILE,
        REQUEST_OVERWRITE_FILE,
        REQUEST_DOWNLOAD_FILE,
        REQUEST_OVERWRITE_USER_DATA,
        NUM_REQUESTS
    };

    struct Request
    {
        Request(const int type, const unsigned clientId);
        virtual ~Request();

        bool Init(const char* tableId);

        const int       m_Type;
        const unsigned  m_ClientId;

        char        m_TableId[RLUPLOAD_MAX_TABLEID_CHARS];
        float       m_Progress;
        float       m_LastProgress;
        unsigned    m_LastProgressReportTime;
        netStatus   m_Status;

        unsigned    m_StartTime;

        inlist_node<Request> m_ListLink;
    };
    typedef inlist<Request, &Request::m_ListLink> RequestList;
    RequestList m_RequestList;

    struct GetTableSizeRequest : public Request
    {
        GetTableSizeRequest(const unsigned clientId);
        virtual ~GetTableSizeRequest();

        bool Init(const char* tableId,
                  const unsigned minNumRatings,
                  const unsigned maxNumRatings);

        unsigned m_MinNumRatings;
        unsigned m_MaxNumRatings;
        unsigned m_NumRecords;
    };

    struct GetNumOwnedRequest : public Request
    {
        GetNumOwnedRequest(const unsigned clientId);
        virtual ~GetNumOwnedRequest();

        bool Init(const char* tableId,
                  const rlGamerHandle& gamerHandle);

        rlGamerHandle m_GamerHandle;
        unsigned m_NumOwned;
    };

    struct ReadRecordsByGamerRequest : public Request
    {
        ReadRecordsByGamerRequest(const unsigned clientId);
        virtual ~ReadRecordsByGamerRequest();
        
        bool Init(const char* tableId,
                  const rlGamerHandle& gamerHandle,
                  const unsigned maxRecords);

        rlGamerHandle m_GamerHandle;
        unsigned m_MaxRecords;
        unsigned m_NumRecordsRead;
        rlUploadRecord* m_Records;
    };

    struct ReadRecordsByRankRequest : public Request
    {
        ReadRecordsByRankRequest(const unsigned clientId);
        virtual ~ReadRecordsByRankRequest();
        
        bool Init(const char* tableId,
                  const unsigned startingRank,
                  const unsigned minNumRatings,
                  const unsigned maxRecords);

        unsigned m_StartingRank;
        unsigned m_MinNumRatings;
        unsigned m_MaxRecords;
        unsigned m_NumRecordsRead;
        rlUploadRecord* m_Records;
    };

    struct ReadRandomRecordRequest : public Request
    {
        ReadRandomRecordRequest(const unsigned clientId);
        virtual ~ReadRandomRecordRequest();
        
        bool Init(const char* tableId,
                  const rlGamerHandle& gamerHandle,
                  const unsigned minNumRatings,
                  const unsigned maxNumRatings,
                  const u32 contentIdFlags);

        rlGamerHandle m_GamerHandle;
        unsigned m_MinNumRatings;
        unsigned m_MaxNumRatings;
        u32 m_ContentIdFlags;
        rlUploadRecord m_Record;
        unsigned m_NumRecordsRead;
    };

    struct ReadRecordRequest : public Request
    {
        ReadRecordRequest(const unsigned clientId);
        virtual ~ReadRecordRequest();
        
        bool Init(const char* tableId,
                  const int recordId);

        int m_RecordId;
        rlUploadRecord m_Record;
        unsigned m_NumRecordsRead;
    };

    struct RateRecordRequest : public Request
    {
        RateRecordRequest(const unsigned clientId);
        virtual ~RateRecordRequest();
        
        bool Init(const char* tableId, 
                  const rlUploadRecord& record,
                  const u8 rating);

        rlUploadRecord m_Record;
        u8 m_Rating;
        unsigned m_NumRatings;
        float m_AverageRating;
    };

    struct DeleteRecordRequest : public Request
    {
        DeleteRecordRequest(const unsigned clientId);
        virtual ~DeleteRecordRequest();
        
        bool Init(const char* tableId, const int recordId);

        int m_RecordId;
    };

    struct UploadFileRequest : public Request
    {
        UploadFileRequest(const unsigned clientId);        
        virtual ~UploadFileRequest();

        bool Init(const char* tableId,
                  const unsigned recordLimit,
                  const rlGamerHandle& gamerHandle,
                  const char* gamerName,
                  const unsigned numFiles,
                  const u8** fileData,
                  const unsigned* fileSizes,
                  const void* userData,
                  const unsigned userDataSize,
                  const u8 contentId);

        unsigned        m_RecordLimit;
        rlGamerHandle   m_GamerHandle;
        char            m_GamerName[RL_MAX_NAME_CHARS];
        unsigned        m_NumFiles;
        u8*             m_FileData[rlUploadRecord::MAX_FILES];
        unsigned        m_FileSizes[rlUploadRecord::MAX_FILES];
        u8              m_UserData[RLUPLOAD_MAX_USER_DATA_SIZE];
        unsigned        m_UserDataSize;
        u8              m_ContentId;
        rlUploadRecord  m_Record;
    };

    struct OverwriteFileRequest : public Request
    {
        OverwriteFileRequest(const unsigned clientId);        
        virtual ~OverwriteFileRequest();

        bool Init(const char* tableId,
                  const rlUploadRecord& currentRecord,
                  const unsigned numFiles,
                  const u8** fileData,
                  const unsigned* fileSizes,
                  const void* userData,
                  const unsigned userDataSize,
                  const u8 contentId);

        rlUploadRecord  m_CurrentRecord;
        unsigned        m_NumFiles;
        u8*             m_FileData[rlUploadRecord::MAX_FILES];
        unsigned        m_FileSizes[rlUploadRecord::MAX_FILES];
        u8              m_UserData[RLUPLOAD_MAX_USER_DATA_SIZE];
        unsigned        m_UserDataSize;
        u8              m_ContentId;
        rlUploadRecord  m_NewRecord;
    };

    struct DownloadFileRequest : public Request
    {
        DownloadFileRequest(const unsigned clientId);
        virtual ~DownloadFileRequest();
        
        bool Init(const int fileId, const unsigned bufSize);

        int m_FileId;
        u8* m_Buf;
        unsigned m_BufSize;
        unsigned m_NumBytesDownloaded;
    };

    struct OverwriteUserDataRequest : public Request
    {
        OverwriteUserDataRequest(const unsigned clientId);
        virtual ~OverwriteUserDataRequest();

        bool Init(const char* tableId,
                  const rlUploadRecord& currentRecord,
                  const void* userData,
                  const unsigned userDataSize);

        rlUploadRecord  m_CurrentRecord;
        u8              m_UserData[RLUPLOAD_MAX_USER_DATA_SIZE];
        unsigned        m_UserDataSize;
        rlUploadRecord  m_NewRecord;
    };

    template<class T>
    class Stat
    {
    public:
        Stat()
        : m_Max(0)
        , m_Min(0) 
        , m_Avg(0.0f)
        , m_NumSamples(0)
        {
        }

        void AddSample(const T& sample)
        {
            if(!m_NumSamples || (sample > m_Max))
            {
                m_Max = sample;
            }

            if(!m_NumSamples || (sample < m_Min))
            {
                m_Min = sample;
            }

            m_Avg *= m_NumSamples;
            m_Avg += float(sample);
            ++m_NumSamples;
            m_Avg /= m_NumSamples;
        }

        void AddStats(const Stat<T>& s)
        {
            if(s.m_Max > m_Max)
            {
                m_Max = s.m_Max;
            }

            if(s.m_Min < m_Min)
            {
                m_Min = s.m_Min;
            }

            m_Avg = (m_Avg*m_NumSamples) + (s.m_Avg*s.m_NumSamples);
            m_NumSamples += s.m_NumSamples;
            m_Avg /= m_NumSamples;
        }

        const T& GetMax() const { return m_Max; }
        const T& GetMin() const { return m_Min; }
        unsigned GetNumSamples() const { return m_NumSamples; }
        float    GetAvg() const { return m_Avg; }

    private:
        T m_Max;
        T m_Min;
        unsigned m_NumSamples;
        float m_Avg;
    };

    struct HourStats
    {
        Stat<unsigned>  m_HeapUsed;
        Stat<unsigned>  m_FrameMs;
        Stat<unsigned>  m_NumNewConnections;
        Stat<unsigned>  m_NumConcurrent;
        Stat<float>     m_IncomingKBps;
        Stat<float>     m_OutgoingKBps;
        Stat<unsigned>  m_ResponseMs;
        unsigned        m_NumRequests;

        HourStats()
        : m_NumRequests(0)
        {
        }
    };

    struct RequestStats
    {
        Stat<unsigned> m_Size;
        Stat<unsigned> m_ResponseMs;
        unsigned m_NumResults[rlUpload::NUM_RESULT_CODES];

        RequestStats()
        {
            memset(m_NumResults, 0, sizeof(m_NumResults));
        }
    };

    struct ReplyStats
    {
        Stat<unsigned> m_Size;
    };

    struct DayStats
    {
        HourStats       m_HourStats[24];
        RequestStats    m_RequestStats[NUM_REQUESTS];
        ReplyStats      m_ReplyStats[NUM_REQUESTS];
    };

    ClientMap::iterator RemoveClient(const unsigned clientId);

    bool ReceiveFromClient(ClientRec* c,
                           const void* data,
                           const unsigned size);
    void QueueRequest(Request* r);
    bool ReceiveRequestGetTableSize(ClientRec* c, const rlUploadMsgGetTableSizeRequest& req);
    bool ReceiveRequestGetNumOwned(ClientRec* c, const rlUploadMsgGetNumOwnedRequest& req);
    bool ReceiveRequestReadRecordsByRank(ClientRec* c, const rlUploadMsgReadRecordsByRankRequest& req);
    bool ReceiveRequestReadRecordsByGamer(ClientRec* c, const rlUploadMsgReadRecordsByGamerRequest& req);
    bool ReceiveRequestReadRandomRecord(ClientRec* c, const rlUploadMsgReadRandomRecordRequest& req);
    bool ReceiveRequestReadRecord(ClientRec* c, const rlUploadMsgReadRecordRequest& req);
    bool ReceiveRequestRateRecord(ClientRec* c, const rlUploadMsgRateRecordRequest& req);
    bool ReceiveRequestDeleteRecord(ClientRec* c, const rlUploadMsgDeleteRecordRequest& req);
    bool ReceiveRequestUploadFile(ClientRec* c, const rlUploadMsgUploadFileRequest& req);
    bool ReceiveRequestOverwriteFile(ClientRec* c, const rlUploadMsgOverwriteFileRequest& req);
    bool ReceiveRequestDownloadFile(ClientRec* c, const rlUploadMsgDownloadFileRequest& req);
    bool ReceiveRequestOverwriteUserData(ClientRec* c, const rlUploadMsgOverwriteUserDataRequest& req);

    void ProcessCompletedRequest(Request* r);

    bool SendToClient(ClientRec* c);
    bool SendToClient(ClientRec* c, 
                      const void* data,
                      const unsigned size);
    bool SendProgressMsg(const unsigned clientId, const float progress);
    bool SendReplyGetTableSize(ClientRec* c, GetTableSizeRequest* r);
    bool SendReplyGetNumOwned(ClientRec* c, GetNumOwnedRequest* r);
    bool SendReplyReadRecordsByRank(ClientRec* c, ReadRecordsByRankRequest* r);
    bool SendReplyReadRecordsByGamer(ClientRec* c, ReadRecordsByGamerRequest* r);
    bool SendReplyReadRandomRecord(ClientRec* c, ReadRandomRecordRequest* r);
    bool SendReplyReadRecord(ClientRec* c, ReadRecordRequest* r);
    bool SendReplyRateRecord(ClientRec* c, RateRecordRequest* r);
    bool SendReplyDeleteRecord(ClientRec* c, DeleteRecordRequest* r);
    bool SendReplyUploadFile(ClientRec* c, UploadFileRequest* r);
    bool SendReplyOverwriteFile(ClientRec* c, OverwriteFileRequest* r);
    bool SendReplyDownloadFile(ClientRec* c, DownloadFileRequest* r);
    bool SendReplyOverwriteUserData(ClientRec* c, OverwriteUserDataRequest* r);

    bool CreateServerSocket();
    bool StartAcceptThread();
    void CheckNewConnections();
    void CheckNewRequests();
    bool ReceiveFromClient(ClientRec* c);
    bool WaitForSocket(SOCKET s, 
                       const bool forRead, //if false, it's for writing
                       const unsigned timeoutSecs);
    
    void UpdateStats();
    void ReportStats();

    void UpdateTelnet();

    void PrintRequestStats(char* buf, 
                           const unsigned sizeofBuf,
                           const char* label,
                           RequestStats* r);

private:
    netHardware m_NetHw;
    netSocket   m_NetSkt;

    void* m_Heap;
    sysMemSimpleAllocator* m_Allocator;
    sysMemAllocator* m_OldAllocator;

    rlUpload    m_Upload;
    unsigned    m_CurTime;  //Current system time, updated at start of frame.  This is cached to prevent hundreds of calls to get it.
    bool        m_Initialized   : 1;
    bool        m_Started       : 1;

    //Stats-related data.
    u64         m_UptimeMs;
    DayStats    m_DayStats;
    unsigned    m_CurHour;
    u64         m_IncomingBytesAccum;
    u64         m_OutgoingBytesAccum;
    int         m_MicroReportMs;
    unsigned    m_MicroReportTimer;
    unsigned    m_DailyEmailHour;
    unsigned    m_EmailFixupMs;

    //TCP socket for communicating w/ clients
    SOCKET m_ServerSocket;

    //Worker thread that does blocking accept(), and places accepted sockets
    //in a queue for the main thread.
    static void AcceptThreadFunc(void* userdata);
    sysIpcSema      m_WaitSema;
    sysIpcThreadId  m_ThreadHandle;

    struct AcceptedConnection
    {
        AcceptedConnection() 
        {
            m_Socket = INVALID_SOCKET;
            m_ClientId = unsigned(INVALID_CLIENTID);
        }

        SOCKET      m_Socket;
        unsigned    m_ClientId;
        netAddress  m_Address;
        inlist_node<AcceptedConnection> m_ListLink;
    };

    typedef inlist<AcceptedConnection, &AcceptedConnection::m_ListLink> AcceptedList;
    AcceptedList m_AcceptedList;
    unsigned m_InactivityTimeoutMs;
    
    mutable sysCriticalSectionToken m_CsServerSocket;
    mutable sysCriticalSectionToken m_CsAcceptedList;    

    bool m_StartedNorad;

    CommandServer m_CmdServer;
    int m_TelnetSkt;    
};

}   //namespace rage

#endif //__WIN32PC

#endif // RLUPLOADPROXY_H
