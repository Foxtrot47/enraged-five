/*
Module : CPDH.CPP
Purpose: Defines the implementation for a wrapper class for the PDH API
Created: PJN / 06-02-1999
History: PJN / 13-09-1999 1. Fixed tab problem in the source code
2. Fixed a problem which was causing an unhandled 
exception being thrown when the PDH.DLL is not present on client machines..
PJN / 25-01-2000 1. Minor update to improve the trace statements when exceptions
are thrown

  Copyright (c) 1999 - 2000 by PJ Naughter.  
  All rights reserved.
  
*/


/////////////////////////////////  Includes  //////////////////////////////////
#include "cpdh.h"
#include <pdhmsg.h>
#include <assert.h>
//#include <tchar.h>
//#include <stdlib.h>

#define ASSERT assert


////////////////////////////////// Implementation /////////////////////////////


CPdhCounter::CPdhCounter(LPCTSTR pszFullCounterPath, DWORD dwUserData)
{
	//Validate our parameters
	ASSERT(_tcslen(pszFullCounterPath) != 0);
	
	//Store away the parameters
	m_dwUserData = dwUserData;
	//m_sCounterPath = _tcsdup(pszFullCounterPath);
    m_sCounterPath = _strdup(pszFullCounterPath);
	m_hCounter = NULL;
}

CPdhCounter::CPdhCounter(PDH_COUNTER_PATH_ELEMENTS& CounterPathElements, DWORD dwUserData)
{
		//Store away the parameters
	m_dwUserData = dwUserData;
	m_hCounter = NULL;
	m_sCounterPath = NULL;

	//First determine the size of the string to allocate
	DWORD dwBufferSize = 0;
	PDH_STATUS Status = PdhMakeCounterPath(&CounterPathElements, NULL, &dwBufferSize, 0);
	if (Status == ERROR_SUCCESS)
	{
		//Call again with the newly allocated buffer
		m_sCounterPath = (LPTSTR) malloc(dwBufferSize);
		PdhMakeCounterPath(&CounterPathElements, m_sCounterPath, &dwBufferSize, 0);
	}

}

CPdhCounter::~CPdhCounter()
{
	if (m_sCounterPath != NULL)
		free(m_sCounterPath);
	m_sCounterPath = NULL;
}

PDH_STATUS CPdhCounter::GetRawValue(PDH_RAW_COUNTER& value, DWORD &dwType)
{
	ASSERT(m_hCounter != NULL); //Did you forget to call CPdhQuery::Add
	
	return PdhGetRawCounterValue(m_hCounter, &dwType, &value);	
}
/*
void CPdhCounter::SplitPath(CStr& sMachineName, CStr& sObjectName, CStr& sInstanceName,
                            CStr& sParentInstance, DWORD& dwInstanceIndex, CStr& sCounterName)
{
	ASSERT(!m_sCounterPath.IsEmpty());
	
	//First call the function to determine the size of buffer required
	DWORD dwBufferSize = 0;
	PDH_STATUS Status = PdhParseCounterPath(m_sCounterPath, NULL, &dwBufferSize, 0);
	if (Status == ERROR_SUCCESS)
	{
		//Call again with the newly allocated buffer
		PDH_COUNTER_PATH_ELEMENTS* pszBuffer = (PDH_COUNTER_PATH_ELEMENTS*) new BYTE[dwBufferSize];
		Status = PdhParseCounterPath(m_sCounterPath, pszBuffer, &dwBufferSize, 0);
		if (Status == ERROR_SUCCESS)
		{
			//Assign the data into the strings sent into us
			sMachineName    = pszBuffer->szMachineName;
			sObjectName     = pszBuffer->szObjectName;
			sInstanceName   = pszBuffer->szInstanceName;
			sParentInstance = pszBuffer->szParentInstance;
			dwInstanceIndex = pszBuffer->dwInstanceIndex;
			sCounterName    = pszBuffer->szCounterName;
		}
		
		//Free up the memory we used
		delete [] pszBuffer;
	}
	
	//Throw an exception if anything goes wrong  
	if (Status != ERROR_SUCCESS)
		CPdh::ThrowPdhException(Status);
}
*/

PDH_STATUS CPdhCounter::GetInfo(PDH_COUNTER_INFO *info, DWORD dwBufferSize)
{
	ASSERT(m_hCounter != NULL); //Did you forget to call CPdhQuery::Add
	
	return PdhGetCounterInfo(m_hCounter, TRUE, &dwBufferSize, info);
}

PDH_STATUS CPdhCounter::CalculateFromRawValue(DWORD dwFormat, PDH_RAW_COUNTER& rawValue1, 
                                        PDH_RAW_COUNTER& rawValue2, PDH_FMT_COUNTERVALUE& fmtValue)
{
	ASSERT(m_hCounter != NULL); //Did you forget to call CPdhQuery::Add
	
	return PdhCalculateCounterFromRawValue(m_hCounter, dwFormat, &rawValue1, &rawValue2, &fmtValue);
}

PDH_STATUS CPdhCounter::GetFormattedValue(DWORD dwFormat, PDH_FMT_COUNTERVALUE& value)
{
	ASSERT(m_hCounter != NULL); //Did you forget to call CPdhQuery::Add
	
	DWORD dwType;
	return PdhGetFormattedCounterValue(m_hCounter, dwFormat, &dwType, &value);	
}

PDH_STATUS CPdhCounter::ComputeStatistics(DWORD dwFormat, DWORD dwFirstEntry, DWORD dwNumEntries, 
                                    PPDH_RAW_COUNTER lpRawValueArray, PPDH_STATISTICS data)
{
	ASSERT(m_hCounter != NULL); //Did you forget to call CPdhQuery::Add
	
	return PdhComputeCounterStatistics(m_hCounter, dwFormat, dwFirstEntry, dwNumEntries, lpRawValueArray, data);
}

PDH_STATUS CPdhCounter::SetScaleFactor(LONG lFactor)
{
	ASSERT(m_hCounter != NULL); //Did you forget to call CPdhQuery::Add
	
	return PdhSetCounterScaleFactor(m_hCounter, lFactor);	
}




CPdhQuery::CPdhQuery()
{
	m_hQuery = NULL;
}

CPdhQuery::~CPdhQuery()
{
	//Ensure we don't leak a query handle
	Close();
}

PDH_STATUS CPdhQuery::Open(DWORD dwUserData)
{
	return PdhOpenQuery(NULL, dwUserData, &m_hQuery);

}

void CPdhQuery::Close()
{
	if (m_hQuery)
		PdhCloseQuery(m_hQuery);
	m_hQuery = NULL;
}

PDH_STATUS CPdhQuery::Add(CPdhCounter& counter)
{
	ASSERT(m_hQuery != NULL); //Did you forget to call Open
	
	return PdhAddCounter(m_hQuery, counter.m_sCounterPath, 
		counter.m_dwUserData, &counter.m_hCounter);
}

PDH_STATUS CPdhQuery::Remove(CPdhCounter& counter)
{
	ASSERT(m_hQuery != NULL); //Did you forget to call Open
	
	return PdhRemoveCounter(counter.m_hCounter);
}

PDH_STATUS CPdhQuery::Collect()
{
	ASSERT(m_hQuery != NULL); //Did you forget to call Open
	
	return PdhCollectQueryData(m_hQuery);	
}




PDH_STATUS CPdh::ValidatePath(LPCTSTR pszFullCounterPath)
{
	return PdhValidatePath(pszFullCounterPath);
}

PDH_STATUS CPdh::ExpandPath(LPCTSTR pszWildCardPath, LPTSTR expandedPaths, DWORD dwBufferSize)
{
	//First call the function to determine the size of buffer required
	return PdhExpandCounterPath(pszWildCardPath, expandedPaths, &dwBufferSize);
}


PDH_STATUS CPdh::ConnectMachine(LPCTSTR pszMachineName)
{
	return PdhConnectMachine(pszMachineName);
	
}

PDH_STATUS CPdh::EnumMachines(LPTSTR machineNames, DWORD dwBufferSize)
{
	//First call the function to determine the size of buffer required
	return PdhEnumMachines(NULL, machineNames, &dwBufferSize);
}

PDH_STATUS CPdh::EnumObjects(LPCTSTR pszMachineName, LPTSTR objectNames, DWORD dwBufferSize, DWORD dwDetailLevel, BOOL bRefresh)
{
	return PdhEnumObjects(NULL, pszMachineName, objectNames, &dwBufferSize, dwDetailLevel, bRefresh);
	
}

PDH_STATUS CPdh::EnumObjectItems(LPCTSTR pszMachineName, LPCTSTR pszObjectName, LPTSTR counters, DWORD dwCounterBufferSize,
								 LPTSTR instances, DWORD dwInstanceBufferSize,
								 DWORD dwDetailLevel)
{
	return PdhEnumObjectItems(NULL, pszMachineName, pszObjectName, counters, &dwCounterBufferSize,
		instances, &dwInstanceBufferSize, dwDetailLevel, 0);
	
}

PDH_STATUS CPdh::GetDefaultCounter(LPCTSTR pszMachineName, LPCTSTR pszObjectName, LPTSTR sDefaultCounterName, DWORD dwBufferSize)
{
	return PdhGetDefaultPerfCounter(NULL, pszMachineName, pszObjectName, sDefaultCounterName, &dwBufferSize);
	
}

PDH_STATUS CPdh::GetDefaultObject(LPCTSTR pszMachineName, LPTSTR sDefaultObjectName, DWORD dwBufferSize)
{
	return PdhGetDefaultPerfObject(NULL, pszMachineName, sDefaultObjectName, &dwBufferSize);

}

PDH_STATUS CPdh::BrowseCounters(PDH_BROWSE_DLG_CONFIG& BrowseDlgData)
{
	return PdhBrowseCounters(&BrowseDlgData);
}

PDH_STATUS CPdh::ParseInstanceName(LPCTSTR pszInstanceString, LPTSTR sInstanceName, DWORD dwInstanceNameBufferSize, LPTSTR sParentName, DWORD dwParentNameBufferSize, DWORD& Index)
{
	return PdhParseInstanceName(pszInstanceString, sInstanceName, &dwInstanceNameBufferSize, sParentName, &dwParentNameBufferSize, &Index);

}

