// xlsprluploadproxy/xlsprluploadproxy.cpp 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 

#include "rluploadproxy.h"

using namespace rage;

rlUploadProxy g_rlUploadProxy;

bool 
ServiceInit()
{
    return g_rlUploadProxy.Init();
}

void 
ServiceShutdown()
{
    g_rlUploadProxy.Shutdown();
}

bool 
ServiceUpdate()
{
    return !g_rlUploadProxy.Update();
}
