set ARCHIVE=xlsprluploadproxy

set FILES=rluploadproxy servicemain commandserver serverdiag

set HEADONLY=servicemsg

set CUSTOM=servicemsg.rc servicemsg.mc servicemsg_MSG00001.bin

set LIBS=%RAGE_CORE_LIBS% %RAGE_NET_LIBS%

set LIBS_xlsprluploadproxy=%LIBS%

set TESTERS=xlsprluploadproxy

set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\src
