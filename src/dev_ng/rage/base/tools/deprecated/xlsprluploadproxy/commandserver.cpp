// 
// server/commandserver.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "commandserver.h"

#include "serverdiag.h"
#include "diag/output.h"
#include "string/string.h"

# undef STRICT
# define STRICT
# pragma warning(push)
# pragma warning(disable: 4668)
# include <winsock2.h>
# pragma warning(pop)

#if !__NO_OUTPUT

#define cmdDebug1(a)    slogDebug1("cmd", a)
#define cmdDebug2(a)    slogDebug2("cmd", a)
#define cmdDebug3(a)    slogDebug3("cmd", a)
#define cmdWarning(a)   slogWarning("cmd", a)
#define cmdError(a)     slogError("cmd", a)

#else

#define cmdDebug1(a)
#define cmdDebug2(a)
#define cmdDebug3(a)
#define cmdWarning(a)
#define cmdError(a)

#endif  //__NO_OUTPUT

using namespace rage;

CommandServer::CommandServer()
: m_ListenSkt(-1)
, m_CurSkt(-1)
, m_Head(NULL)
, m_Tail(NULL)
, m_ThreadId(sysIpcThreadIdInvalid)
, m_WaitSema(sysIpcCreateSema(0))
, m_Started(false)
, m_StopWorker(false)
{
}

CommandServer::~CommandServer()
{
    this->Stop();
}

bool
CommandServer::Start(const int listenSkt)
{
    Assert(!m_Started);
    Assert(listenSkt >= 0);
    Assert(-1 == m_ListenSkt);

    cmdDebug1(("Starting command server..."));

    m_StopWorker = false;

    m_ListenSkt = listenSkt;

    m_ThreadId =
        sysIpcCreateThread(&CommandServer::Worker,
                            this,
                            sysIpcMinThreadStackSize,
                            PRIO_NORMAL,
                            "Command Worker");

    if(sysIpcThreadIdInvalid == m_ThreadId)
    {
       cmdError(("Failed to create worker thread"));
       return false;
    }

    sysIpcWaitSema(m_WaitSema);

    m_Started = true;

    cmdDebug1(("Command server started."));

    return true;
}

void
CommandServer::Stop()
{
    if(m_Started)
    {
        cmdDebug1(("Stopping command server..."));

        m_StopWorker = true;

        closesocket(m_ListenSkt);

        if(sysIpcThreadIdInvalid != m_ThreadId)
        {
            sysIpcWaitSema(m_WaitSema);
            m_ThreadId = sysIpcThreadIdInvalid;
        }

        m_ListenSkt = m_CurSkt = -1;
        m_StopWorker = false;

        for(Command* cmd = this->NextCommand(); cmd; cmd = this->NextCommand())
        {
            this->DisposeCommand(cmd);
        }

        m_Started = false;

        cmdDebug1(("Command server stopped."));
    }
}

void
CommandServer::Say(const char* say) const
{
    SYS_CS_SYNC(m_SktCs);

    if(m_CurSkt >= 0)
    {
        send(m_CurSkt, say, strlen(say) + 1, 0);
        send(m_CurSkt, "\r\n> ", sizeof("\r\n"), 0);
    }
}

Command*
CommandServer::NextCommand()
{
    Command* cmd = NULL;

    SYS_CS_SYNC(m_CmdCs);

    if(m_Head)
    {
        cmd = m_Head;
        m_Head = m_Head->m_Next;
        if(!m_Head)
        {
            m_Tail = NULL;
        }
    }

    return cmd;
}

void
CommandServer::DisposeCommand(Command* cmd)
{
    delete [] (char*) cmd;
}

//private:

void
CommandServer::Worker(void* ctx)
{
    CommandServer* svr = (CommandServer*) ctx;
    sysIpcSignalSema(svr->m_WaitSema);

    Assert(svr->m_ListenSkt >= 0);
    Assert(-1 == svr->m_CurSkt);

    while(!svr->m_StopWorker)
    {
        sockaddr_in addr;
        int sizeofAddr = sizeof(addr);

        svr->m_CurSkt = accept(svr->m_ListenSkt, (sockaddr*) &addr, &sizeofAddr);

        if(-1 == svr->m_CurSkt){continue;}

        cmdDebug1(("CMD connection from: %d.%d.%d.%d",
                    ((ntohl(addr.sin_addr.s_addr)>>24)&0xFF),
                    ((ntohl(addr.sin_addr.s_addr)>>16)&0xFF),
                    ((ntohl(addr.sin_addr.s_addr)>>8)&0xFF),
                    ((ntohl(addr.sin_addr.s_addr)>>0)&0xFF)));

        static const char SALUTE[] = {"\r\nHello\r\n"};

        svr->Say(SALUTE);

        char buf[1024];
        char* p = buf;
        bool done = false;

        while(!done && !svr->m_StopWorker)
        {
            fd_set readfds;
            FD_ZERO(&readfds);

            fd_set exceptionfds;
            FD_ZERO(&exceptionfds);

            SOCKET highestSocket = svr->m_CurSkt;
            
            FD_SET((SOCKET)svr->m_CurSkt, &readfds);
            FD_SET((SOCKET)svr->m_CurSkt, &exceptionfds);

            timeval t;
            t.tv_sec  = 0;
            t.tv_usec = 100000;

            int result = select(highestSocket, &readfds, NULL, &exceptionfds, &t);

            if(result < 0)
            {
                cmdDebug1(("Exception on CMD connection, closing"));
                break;
            }
            else if(!result || !FD_ISSET((SOCKET)svr->m_CurSkt, &readfds))
            {
                //Timeout; nothing is ready
                continue;
            }

            const int len = recv(svr->m_CurSkt, p, &buf[sizeof(buf)] - p - 1, 0);

            if(len <= 0)
            {
                cmdDebug1(("CMD connection closed"));
                break;
            }

            const char* eop = p + len;

            bool more;

            do
            {
                more = false;

                while(p < eop)
                {
                    bool haveCmd = false;

                    if(p <= eop-2
                        && (('\r' == *p && '\n' == p[1])
                            || ('\n' == *p && '\r' == p[1])))
                    {
                        *p++ = '\0';
                        ++p;
                        haveCmd = true;
                    }
                    else if('\n' == *p)
                    {
                        *p++ = '\0';
                        haveCmd = true;
                    }
                    else
                    {
                        ++p;
                    }

                    if(haveCmd)
                    {
                        const unsigned cmdLen = strlen(buf) + 1;
                        const unsigned sizeofCmd = sizeof(Command) + cmdLen;

                        Command* cmd = (Command*) rage_new char[sizeofCmd];
                        new (cmd) Command;

                        cmd->m_Str = (char*) &cmd[1];
                        safecpy(cmd->m_Str, buf, cmdLen);

                        {
                            SYS_CS_SYNC(svr->m_CmdCs);
                            if(svr->m_Tail)
                            {
                                svr->m_Tail->m_Next = cmd;
                                svr->m_Tail = cmd;
                            }
                            else
                            {
                                svr->m_Head = svr->m_Tail = cmd;
                            }
                        }

                        //Copy the remainder of the buffer
                        //to the beginning of buf.
                        const int rem = eop - p;
                        Assert(rem >= 0);

                        if(rem)
                        {
                            more = true;
                            memcpy(buf, p, rem);
                        }

                        eop -= (p - buf);
                        p = buf;
                    }
                }
            }
            while(more);

            if(p == &buf[sizeof(buf)])
            {
                cmdError(("Command buffer overflowed"));
                p = buf;
            }
        }

        SYS_CS_SYNC(svr->m_SktCs);
        closesocket(svr->m_CurSkt);
        svr->m_CurSkt = -1;
    }

    sysIpcSignalSema(svr->m_WaitSema);
}

