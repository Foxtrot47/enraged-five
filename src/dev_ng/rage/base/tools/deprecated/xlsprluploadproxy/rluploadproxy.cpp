// 
// rlUploadProxy
//
// An XLSP title server that acts as a proxy between Sake and Xenon consoles for
// rlUpload functionality.
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#if __WIN32PC

#include "rluploadproxy.h"
#include "servicemain.h"
#include "rline/rlgamespy.h"
#include "rline/rlgamespysake.h"
#include "rline/rl.h"
#include "rline/rltitleid.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/simpleallocator.h"
#include "system/timer.h"
#include "system/xtl.h"

#define NO_PERF_QUEUE
#pragma comment(lib, "pdh")
#include "cpdh.cpp"
#include "perflib.cpp"

using namespace rage;

const unsigned DEFAULT_HEAP_MB                  = 250;
const unsigned DEFAULT_TITLE_SERVER_PORT        = 8001;
const unsigned DEFAULT_TELNET_PORT              = 1234;
const unsigned DEFAULT_ACCEPT_SLEEP_MS          = 10;
const unsigned DEFAULT_INACTIVITY_TIMEOUT_MS    = 10 * 60 * 1000; //10 min
const unsigned DEFAULT_EMAIL_HOUR               = 1; //1am
const unsigned DEFAULT_MAX_MESSAGE_BYTES        = 1024 * 1024; //Enough for most uploads

static unsigned s_MaxMsgSize;

//Helper macro for reporting failed rchecks() in a uniform way
#if !__FINAL
#define RLCHECK_SET_CTX(name) const char* s_rlCheckContext = #name ;
#define RLCHECK_CTX(cond) rcheck(cond, catchall, rlupError(("%s: \"" #cond "\" failed", s_rlCheckContext)));
#else
#define RLCHECK_SET_CTX(name) 
#define RLCHECK_CTX(cond) rcheck(cond, catchall, );
#endif

extern bool SendAdminEmail(const char* subject, const char *text);

///////////////////////////////////////////////////////////////////////////////
//Cmdline parameters
///////////////////////////////////////////////////////////////////////////////
PARAM(inactivitytimeoutms,  "ms without receiving anything from client before we disconnect them");
PARAM(acceptsleepms,        "ms that accept() thread sleeps");
PARAM(dailyemailhour,       "Hour at which to send daily stats email (defaults to 1am PST)");
PARAM(microreportms,        "Used for testing.  This forces a stats report to be sent every n ms (if n > 0).");
PARAM(titleserverport,      "Port that title server listens on");
PARAM(heapmb,               "Size of heap, in MB (default is 768)");
PARAM(maxmsgkb,             "Maximum size of messages (in KB) we'll accept from clients");

//Gamespy config
PARAM(gsEnvironment,        "Environment to run in (part, cert, prod)");
PARAM(gsGameName,           "Gamespy game name (ex. \"rockstardev\")");
PARAM(gsSecretKey,          "Gamespy secret key (ex. \"1a8bBi\")");
PARAM(gsProductId,          "Gamespy product ID (ex. 10947)");
PARAM(gsGameId,             "Gamespy game ID (ex. 1572)");
PARAM(gsLoginEmail,         "Email used to login to Gamespy (ex. \"test1@rage\")");
PARAM(gsLoginPassword,      "Password used to login to Gamespy (ex. \"password\"");

//Norad options
PARAM(norad_ip,             "IP address of network interface on which to listen for requests.");
PARAM(norad_port,           "Port on which to listen for requests.");
PARAM(norad_appname,        "Application name.");

//Telnet
PARAM(telnet_ip,            "IP address of network interface on which to listen for requests.");
PARAM(telnet_port,          "Port on which to listen for requests.");


#if !__NO_OUTPUT

#define rlupDebug1(a)     SpewDebug(1, ::rage::rlGetDebugLevel(), "rlUploadProxy") a
#define rlupDebug2(a)     SpewDebug(2, ::rage::rlGetDebugLevel(), "rlUploadProxy") a
#define rlupDebug3(a)     SpewDebug(3, ::rage::rlGetDebugLevel(), "rlUploadProxy") a

#define rlupWarning(a)    SpewWarning("rlUploadProxy") a
#define rlupError(a)      SpewError("rlUploadProxy") a

#else

#define rlupDebug1(a)
#define rlupDebug2(a)
#define rlupDebug3(a)

#define rlupWarning(a)
#define rlupError(a)

#endif  //__NO_OUTPUT



///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::Request
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::Request::Request(const int type, const unsigned clientId)
: m_Type(type)
, m_ClientId(clientId)
, m_Progress(0.0f)
, m_LastProgress(0.0f)
, m_LastProgressReportTime(0)
, m_StartTime(0)
{
    Assert(m_Type != REQUEST_INVALID);
    Assert(m_ClientId != INVALID_CLIENTID);
    sysMemSet(m_TableId, 0, sizeof(m_TableId));
}

rlUploadProxy::Request::~Request()
{
    if(m_Status.Pending())
    {
        m_Status.SetFailed();
    }
}

bool
rlUploadProxy::Request::Init(const char* tableId)
{
    sysMemSet(m_TableId, 0, sizeof(m_TableId));

    if(tableId)
    {
        safecpy(m_TableId, tableId, sizeof(m_TableId));
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::GetTableSizeRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::GetTableSizeRequest::GetTableSizeRequest(const unsigned clientId)
: Request(REQUEST_GET_TABLE_SIZE, clientId)
, m_MinNumRatings(0)
, m_MaxNumRatings(0)
, m_NumRecords(0)
{
}

rlUploadProxy::GetTableSizeRequest::~GetTableSizeRequest()
{
}

bool
rlUploadProxy::GetTableSizeRequest::Init(const char* tableId,
                                         const unsigned minNumRatings,
                                         const unsigned maxNumRatings)
{
    RLCHECK_SET_CTX(GetTableSizeRequest::Init);

    rtry
    {
        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));

        m_MinNumRatings = minNumRatings;
        m_MaxNumRatings = maxNumRatings;
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::GetNumOwnedRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::GetNumOwnedRequest::GetNumOwnedRequest(const unsigned clientId)
: Request(REQUEST_GET_NUM_OWNED, clientId)
, m_NumOwned(0)
{
}

rlUploadProxy::GetNumOwnedRequest::~GetNumOwnedRequest()
{
}

bool
rlUploadProxy::GetNumOwnedRequest::Init(const char* tableId,
                                        const rlGamerHandle& gamerHandle)
{
    RLCHECK_SET_CTX(GetNumOwnedRequest::Init);

    rtry
    {
        m_GamerHandle = gamerHandle;
        RLCHECK_CTX(m_GamerHandle.IsValid());

        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::ReadRecordsByGamerRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::ReadRecordsByGamerRequest::ReadRecordsByGamerRequest(const unsigned clientId)
: Request(REQUEST_READ_RECORDS_BY_GAMER, clientId)
, m_MaxRecords(0)
, m_NumRecordsRead(0)
, m_Records(0)
{
}

rlUploadProxy::ReadRecordsByGamerRequest::~ReadRecordsByGamerRequest()
{
    delete [] m_Records;
}

bool
rlUploadProxy::ReadRecordsByGamerRequest::Init(const char* tableId,
                                               const rlGamerHandle& gamerHandle,
                                               const unsigned maxRecords)
{
    RLCHECK_SET_CTX(ReadRecordsByGamerRequest::Init);

    rtry
    {
        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));

        m_GamerHandle = gamerHandle;
        RLCHECK_CTX(m_GamerHandle.IsValid());

        m_MaxRecords = maxRecords;
        RLCHECK_CTX(m_MaxRecords > 0);
        RLCHECK_CTX((m_MaxRecords > 0) && (m_MaxRecords < 100000));
        RLCHECK_CTX(0 != (m_Records = rage_new rlUploadRecord[m_MaxRecords]));
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::ReadRecordsByRankRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::ReadRecordsByRankRequest::ReadRecordsByRankRequest(const unsigned clientId)
: Request(REQUEST_READ_RECORDS_BY_RANK, clientId)
, m_StartingRank(0)
, m_MinNumRatings(0)
, m_MaxRecords(0)
, m_NumRecordsRead(0)
, m_Records(0)
{
}

rlUploadProxy::ReadRecordsByRankRequest::~ReadRecordsByRankRequest()
{
    delete [] m_Records;
}

bool
rlUploadProxy::ReadRecordsByRankRequest::Init(const char* tableId,
                                              const unsigned startingRank,
                                              const unsigned minNumRatings,
                                              const unsigned maxRecords)
{
    RLCHECK_SET_CTX(ReadRecordsByRankRequest::Init);

    rtry
    {
        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));

        m_StartingRank = startingRank;
        RLCHECK_CTX(m_StartingRank > 0);

        m_MinNumRatings = minNumRatings;

        m_MaxRecords = maxRecords;
        RLCHECK_CTX(m_MaxRecords > 0);
        RLCHECK_CTX((m_MaxRecords > 0) && (m_MaxRecords < 100000));
        RLCHECK_CTX(0 != (m_Records = rage_new rlUploadRecord[m_MaxRecords]));
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::ReadRandomRecordRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::ReadRandomRecordRequest::ReadRandomRecordRequest(const unsigned clientId)
: Request(REQUEST_READ_RANDOM_RECORD, clientId)
, m_NumRecordsRead(0)
, m_MinNumRatings(0)
, m_MaxNumRatings(0)
, m_ContentIdFlags(0)
{
}

rlUploadProxy::ReadRandomRecordRequest::~ReadRandomRecordRequest()
{
}

bool
rlUploadProxy::ReadRandomRecordRequest::Init(const char* tableId,
                                             const rlGamerHandle& gamerHandle,
                                             const unsigned minNumRatings,
                                             const unsigned maxNumRatings,
                                             const u32 contentIdFlags)
{
    RLCHECK_SET_CTX(ReadRandomRecordRequest::Init);

    rtry
    {
        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));

        m_GamerHandle = gamerHandle;
        m_MinNumRatings = minNumRatings;
        m_MaxNumRatings = maxNumRatings;
        m_ContentIdFlags = contentIdFlags;
        
        RLCHECK_CTX(m_GamerHandle.IsValid());
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::ReadRecordRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::ReadRecordRequest::ReadRecordRequest(const unsigned clientId)
: Request(REQUEST_READ_RECORD, clientId)
, m_RecordId(0)
, m_NumRecordsRead(0)
{
}

rlUploadProxy::ReadRecordRequest::~ReadRecordRequest()
{
}

bool
rlUploadProxy::ReadRecordRequest::Init(const char* tableId,
                                       const int recordId)
{
    RLCHECK_SET_CTX(ReadRecordRequest::Init);

    rtry
    {
        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));

        m_RecordId = recordId;
        RLCHECK_CTX(m_RecordId != 0);
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::RateRecordRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::RateRecordRequest::RateRecordRequest(const unsigned clientId)
: Request(REQUEST_RATE_RECORD, clientId)
, m_Rating(0)
, m_NumRatings(0)
, m_AverageRating(0.0f)
{
}

rlUploadProxy::RateRecordRequest::~RateRecordRequest()
{
}

bool
rlUploadProxy::RateRecordRequest::Init(const char* tableId,
                                       const rlUploadRecord& record,
                                       const u8 rating)
{
    RLCHECK_SET_CTX(RateRecordRequest::Init);

    rtry
    {
        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));

        m_Record = record;
        m_Rating = rating;
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::DeleteRecordRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::DeleteRecordRequest::DeleteRecordRequest(const unsigned clientId)
: Request(REQUEST_DELETE_RECORD, clientId)
, m_RecordId(0)
{
}

rlUploadProxy::DeleteRecordRequest::~DeleteRecordRequest()
{
}

bool
rlUploadProxy::DeleteRecordRequest::Init(const char* tableId,
                                         const int recordId)
{
    RLCHECK_SET_CTX(DeleteRecordRequest::Init);

    rtry
    {
        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));

        m_RecordId = recordId;
        RLCHECK_CTX(m_RecordId != 0);
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::UploadFileRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::UploadFileRequest::UploadFileRequest(const unsigned clientId)
: Request(REQUEST_UPLOAD_FILE, clientId)
, m_RecordLimit(0)
, m_NumFiles(0)
, m_UserDataSize(0)
{
    sysMemSet(m_FileData, 0, sizeof(m_FileData));
    sysMemSet(m_FileSizes, 0, sizeof(m_FileSizes));
}

rlUploadProxy::UploadFileRequest::~UploadFileRequest()
{
    for(unsigned i = 0; i < NELEM(m_FileData); i++)
    {
        delete [] m_FileData[i];
    }
}

bool
rlUploadProxy::UploadFileRequest::Init(const char* tableId,
                                       const unsigned recordLimit,
                                       const rlGamerHandle& gamerHandle,
                                       const char* gamerName,
                                       const unsigned numFiles,
                                       const u8** fileData,
                                       const unsigned* fileSizes,
                                       const void* userData,
                                       const unsigned userDataSize,
                                       const u8 contentId)
{
    RLCHECK_SET_CTX(UploadFileRequest::Init);

    rtry
    {
        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));

        m_RecordLimit = recordLimit;

        m_GamerHandle = gamerHandle;
        RLCHECK_CTX(m_GamerHandle.IsValid());

        RLCHECK_CTX(gamerName);
        safecpy(m_GamerName, gamerName, sizeof(m_GamerName));

        m_NumFiles = numFiles;
        RLCHECK_CTX(m_NumFiles);

        for(unsigned i = 0; i < m_NumFiles; i++)
        {
            m_FileSizes[i] = fileSizes[i];
            RLCHECK_CTX(m_FileSizes[i]);
            
            RLCHECK_CTX(fileData[i]);
            RLCHECK_CTX(!m_FileData[i]);
            RLCHECK_CTX(m_FileSizes[i] <= s_MaxMsgSize);
            RLCHECK_CTX(0 != (m_FileData[i] = rage_new u8[m_FileSizes[i]]));
            sysMemCpy(m_FileData[i], fileData[i], m_FileSizes[i]);
        }

        m_UserDataSize = userDataSize;
        if(m_UserDataSize)
        {
            RLCHECK_CTX(userData);
            sysMemCpy(m_UserData, userData, m_UserDataSize);
        }

        m_ContentId = contentId;
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::OverwriteFileRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::OverwriteFileRequest::OverwriteFileRequest(const unsigned clientId)
: Request(REQUEST_OVERWRITE_FILE, clientId)
, m_NumFiles(0)
, m_UserDataSize(0)
{
    sysMemSet(m_FileData, 0, sizeof(m_FileData));
    sysMemSet(m_FileSizes, 0, sizeof(m_FileSizes));
}

rlUploadProxy::OverwriteFileRequest::~OverwriteFileRequest()
{
    for(unsigned i = 0; i < NELEM(m_FileData); i++)
    {
        delete [] m_FileData[i];
    }
}

bool
rlUploadProxy::OverwriteFileRequest::Init(const char* tableId,
                                          const rlUploadRecord& currentRecord,
                                          const unsigned numFiles,
                                          const u8** fileData,
                                          const unsigned* fileSizes,
                                          const void* userData,
                                          const unsigned userDataSize,
                                          const u8 contentId)
{
    RLCHECK_SET_CTX(OverwriteFileRequest::Init);

    rtry
    {
        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));

        m_CurrentRecord = currentRecord;
        RLCHECK_CTX(0 != m_CurrentRecord.GetRecordId());

        m_NumFiles = numFiles;
        RLCHECK_CTX(m_NumFiles);

        for(unsigned i = 0; i < m_NumFiles; i++)
        {
            m_FileSizes[i] = fileSizes[i];
            RLCHECK_CTX(m_FileSizes[i]);
            
            RLCHECK_CTX(fileData[i]);
            RLCHECK_CTX(!m_FileData[i]);
            RLCHECK_CTX(m_FileSizes[i] <= s_MaxMsgSize);
            RLCHECK_CTX(0 != (m_FileData[i] = rage_new u8[m_FileSizes[i]]));
            sysMemCpy(m_FileData[i], fileData[i], m_FileSizes[i]);
        }

        m_UserDataSize  = userDataSize;
        if(m_UserDataSize)
        {
            RLCHECK_CTX(userData);
            sysMemCpy(m_UserData, userData, m_UserDataSize);
        }

        m_ContentId = contentId;
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::DownloadFileRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::DownloadFileRequest::DownloadFileRequest(const unsigned clientId)
: Request(REQUEST_DOWNLOAD_FILE, clientId)
, m_FileId(0)
, m_BufSize(0)
, m_Buf(0)
, m_NumBytesDownloaded(0)
{
}

rlUploadProxy::DownloadFileRequest::~DownloadFileRequest()
{
    if(m_Buf) delete [] m_Buf;
}

bool
rlUploadProxy::DownloadFileRequest::Init(const int fileId,
                                         const unsigned bufSize)
{
    RLCHECK_SET_CTX(DownloadFileRequest::Init);

    rtry
    {
        RLCHECK_CTX(Request::Init(0));

        m_FileId = fileId;
        RLCHECK_CTX(0 != m_FileId);

        m_BufSize = bufSize;
        RLCHECK_CTX(m_BufSize > 0);       
        RLCHECK_CTX(!m_Buf);
        RLCHECK_CTX(m_BufSize <= s_MaxMsgSize);
        RLCHECK_CTX(0 != (m_Buf = rage_new u8[m_BufSize]));
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::OverwriteUserDataRequest
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::OverwriteUserDataRequest::OverwriteUserDataRequest(const unsigned clientId)
: Request(REQUEST_OVERWRITE_USER_DATA, clientId)
, m_UserDataSize(0)
{
}

rlUploadProxy::OverwriteUserDataRequest::~OverwriteUserDataRequest()
{
}

bool
rlUploadProxy::OverwriteUserDataRequest::Init(const char* tableId,
                                              const rlUploadRecord& currentRecord,
                                              const void* userData,
                                              const unsigned userDataSize)
{
    RLCHECK_SET_CTX(OverwriteUserData::Init);

    rtry
    {
        RLCHECK_CTX(tableId != 0);
        RLCHECK_CTX(Request::Init(tableId));

        m_CurrentRecord = currentRecord;
        RLCHECK_CTX(0 != m_CurrentRecord.GetRecordId());

        m_UserDataSize  = userDataSize;
        RLCHECK_CTX(0 < m_UserDataSize);

        RLCHECK_CTX(userData);
        sysMemCpy(m_UserData, userData, m_UserDataSize);
    }
    rcatchall
    {
        return false;
    }
    return true;
}


///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::OutboundMsg
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::OutboundMsg::OutboundMsg()
: m_MsgBuf(0)
, m_MsgSize(0)
{
}

rlUploadProxy::OutboundMsg::~OutboundMsg()
{
    if(m_MsgBuf) 
    {
        delete [] m_MsgBuf;
        m_MsgBuf = 0;
    }
}

bool
rlUploadProxy::OutboundMsg::Init(const void* data, const unsigned size)
{
    RLCHECK_SET_CTX(OutboundMsg::Init);

    rtry
    {       
        m_MsgSize = size;
        RLCHECK_CTX(m_MsgSize > 0);
        RLCHECK_CTX(0 != (m_MsgBuf = rage_new u8[m_MsgSize]));

        RLCHECK_CTX(data);
        sysMemCpy(m_MsgBuf, data, m_MsgSize);
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy::ClientRec
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::ClientRec::ClientRec()
: m_ClientId(INVALID_CLIENTID)
, m_NumRequestsInProgress(0)
, m_LastActiveTime(0)
, m_Socket(INVALID_SOCKET)
, m_MsgBuf(0)
, m_MsgBytesRead(0)
, m_MsgSize(0)
, m_MsgSizeBytesRead(0)
, m_MsgBytesSent(0)
{
}

rlUploadProxy::ClientRec::~ClientRec()
{
    if(INVALID_SOCKET != m_Socket)
    {
        closesocket(m_Socket);
    }

    if(m_MsgBuf) 
    {
        delete [] m_MsgBuf;
        m_MsgBuf = 0;
    }

    while(!m_OutboundMsgList.empty())
    {
        OutboundMsg* m = *m_OutboundMsgList.begin();
        m_OutboundMsgList.erase(m_OutboundMsgList.begin());
        delete m;
    }
}

///////////////////////////////////////////////////////////////////////////////
//rlUploadProxy
///////////////////////////////////////////////////////////////////////////////
rlUploadProxy::rlUploadProxy()
: m_Initialized(false)
, m_Started(false)
, m_Heap(0)
, m_Allocator(0)
, m_OldAllocator(0)
, m_ServerSocket(INVALID_SOCKET)
, m_WaitSema(0)
, m_ThreadHandle(sysIpcThreadIdInvalid)
, m_UptimeMs(0)
, m_CurTime(sysTimer::GetSystemMsTime())
, m_CurHour(0)
, m_IncomingBytesAccum(0)
, m_OutgoingBytesAccum(0)
, m_MicroReportMs(0)
, m_DailyEmailHour(DEFAULT_EMAIL_HOUR)
, m_MicroReportTimer(0)
, m_EmailFixupMs(0)
, m_StartedNorad(false)
, m_TelnetSkt(-1)
, m_InactivityTimeoutMs(DEFAULT_INACTIVITY_TIMEOUT_MS)
{
}

rlUploadProxy::~rlUploadProxy()
{
    if(m_Initialized)
    {
        Shutdown();
    }
}

bool
rlUploadProxy::Init()
{
    RLCHECK_SET_CTX(Init);

    rtry
    {
        RLCHECK_CTX(!m_Initialized);

        //Allocate a the heap that everything will use
        unsigned heapmb = DEFAULT_HEAP_MB;
        PARAM_heapmb.Get(heapmb);

        unsigned sizeofHeap = heapmb * 1024 * 1024;

        m_Heap = sysMemVirtualAllocate(sizeofHeap);
        RLCHECK_CTX(m_Heap);

        m_Allocator = rage_new sysMemSimpleAllocator(m_Heap, sizeofHeap, sysMemSimpleAllocator::HEAP_NET);
        m_OldAllocator = &sysMemAllocator::GetCurrent();
        sysMemAllocator::SetCurrent(*m_Allocator);

        //Cache config params
        PARAM_microreportms.Get(m_MicroReportMs);
        m_MicroReportTimer = m_MicroReportMs;

        m_DailyEmailHour = DEFAULT_EMAIL_HOUR;
        PARAM_dailyemailhour.Get(m_DailyEmailHour);

        m_InactivityTimeoutMs = DEFAULT_INACTIVITY_TIMEOUT_MS;
        PARAM_acceptsleepms.Get(m_InactivityTimeoutMs);

        s_MaxMsgSize = DEFAULT_MAX_MESSAGE_BYTES;
        if(PARAM_maxmsgkb.Get(s_MaxMsgSize))
        {
            s_MaxMsgSize *= 1024;
        }

        rlupDebug2(("%s: heapmb             = %d", s_rlCheckContext, heapmb));
        rlupDebug2(("%s: microreportms      = %d", s_rlCheckContext, m_MicroReportTimer));
        rlupDebug2(("%s: dailyemailhour     = %d", s_rlCheckContext, m_DailyEmailHour));
        rlupDebug2(("%s: acceptsleepms      = %d", s_rlCheckContext, m_InactivityTimeoutMs));

        //Init subsystems
        RLCHECK_CTX(InitNet());
        RLCHECK_CTX(InitRline());
        RLCHECK_CTX(m_Upload.Init(1));
        RLCHECK_CTX(InitNorad());
        RLCHECK_CTX(InitTelnet());

        m_Initialized = true;

        RLCHECK_CTX(Start());
    }
    rcatchall
    {
        Reset();
        return false;
    }
    return true;
}

bool
rlUploadProxy::Start()
{
    RLCHECK_SET_CTX(Start);

    rtry
    {
        RLCHECK_CTX(!m_Started);

        rlupDebug2(("%s: Initializing TCP...", s_rlCheckContext));
        RLCHECK_CTX(CreateServerSocket());
        RLCHECK_CTX(StartAcceptThread());

        m_Started = true;

        rlupDebug2(("%s: succeeded.", s_rlCheckContext));
    }
    rcatchall
    {
        Reset();
        return false;
    }
    return true;
}

void
rlUploadProxy::Stop()
{
    //Shutdown TCP
    {
        SYS_CS_SYNC(m_CsServerSocket);

        if(INVALID_SOCKET != m_ServerSocket)
        {
            closesocket(m_ServerSocket);
            m_ServerSocket = INVALID_SOCKET;
        }
    }

    if(m_ThreadHandle != sysIpcThreadIdInvalid)
    {
        sysIpcWaitThreadExit(m_ThreadHandle);
        m_ThreadHandle = sysIpcThreadIdInvalid;
    }

    if(m_WaitSema)
    {
        sysIpcDeleteSema(m_WaitSema);
        m_WaitSema = 0;
    }

    m_Started = false;
}

void
rlUploadProxy::Reset()
{
    rlupDebug2(("rlUploadProxy::Reset"));

    Stop();

    //Shutdown telnet
    m_CmdServer.Stop();

    if(m_TelnetSkt >= 0)
    {
        closesocket(m_TelnetSkt);
        m_TelnetSkt = -1;
    }

    //Shutdown Norad
    if(m_StartedNorad)
    {
        FreeTCPPerfLib();
        m_StartedNorad = false;
    }

    //Stop our uploads.
    m_Upload.Shutdown();

    //Clear the request queue
    while(!m_RequestList.empty())
    {
        Request* r = *m_RequestList.begin();
        m_RequestList.erase(m_RequestList.begin());
        delete r;
    }

    //Clear the client map
    while(!m_ClientMap.empty())
    {
        ClientRec* c = m_ClientMap.begin()->second;
        m_ClientMap.erase(m_ClientMap.begin());
        delete c;
    }

    //Shutdown rline
    if(rlIsInitialized())
    {
        rlShutdown();
    }

    //Shutdown net
    if(m_NetSkt.GetHardware())
    {
        m_NetHw.DestroySocket(&m_NetSkt);
    }

    //Free heap and restore allocators
    if(m_OldAllocator)
    {
        sysMemAllocator::SetCurrent(*m_OldAllocator);
        delete m_Allocator;
        m_Allocator = NULL;
        m_OldAllocator = NULL;
        sysMemVirtualFree(m_Heap);
        m_Heap = NULL;
    }
}

void
rlUploadProxy::Shutdown()
{    
    Assert(m_Initialized);

    rlupDebug2(("rlUploadProxy::Shutdown() starting"));

    Reset();

    m_Initialized = false;

    rlupDebug2(("rlUploadProxy::Shutdown() finished"));
}

bool
rlUploadProxy::Update()
{
    m_CurTime = sysTimer::GetSystemMsTime();

    m_NetHw.Update();

    rlUpdate();

    m_Upload.Update();

    CheckCompletedRequests();

    CheckTimedOutConnections();

    CheckNewConnections();

    CheckNewRequests();

    SendToClients();

    UpdateStats();

    UpdateTelnet();

    return true;
}

bool 
rlUploadProxy::InitNet()
{
    RLCHECK_SET_CTX(InitNet);

    rtry
    {
        rlupDebug2(("%s: Initializing...", s_rlCheckContext));

        //NOTE: Since we don't actually send or receive anything on this socket,
        //      we just grab any available port.
        RLCHECK_CTX(m_NetHw.CreateSocket(&m_NetSkt, 0, NET_PROTO_UDP));

        rlupDebug2(("%s: Succeeded", s_rlCheckContext));
    }
    rcatchall
    {
        return false;
    }
    return true;
}

bool 
rlUploadProxy::InitRline()
{
    RLCHECK_SET_CTX(InitRline);

    //Initialize the rline subsystem.
    rtry
    {
        rlupDebug2(("%s: Initializing...", s_rlCheckContext));

        //Get title ID info
        const char* gsEnvironment   = 0;
        const char* gsGameName      = 0;
        const char* gsSecretKey     = 0;
        const char* gsLoginEmail    = 0;
        const char* gsLoginPassword = 0;
        int gsProductId             = 0;
        int gsGameId                = 0;
 
        //Required params
        RLCHECK_CTX(PARAM_gsEnvironment.Get(gsEnvironment));
        rlupDebug2(("%s: gsEnvironment    = %s", s_rlCheckContext, gsEnvironment));

        RLCHECK_CTX(PARAM_gsGameName.Get(gsGameName));
        rlupDebug2(("%s: gsGameName       = %s", s_rlCheckContext, gsGameName));

        RLCHECK_CTX(PARAM_gsSecretKey.Get(gsSecretKey));
        rlupDebug2(("%s: gsSecretKey      = %s", s_rlCheckContext, gsSecretKey));

        RLCHECK_CTX(PARAM_gsProductId.Get(gsProductId));
        rlupDebug2(("%s: gsProductId      = %d", s_rlCheckContext, gsProductId));

        RLCHECK_CTX(PARAM_gsGameId.Get(gsGameId));
        rlupDebug2(("%s: gsGameId         = %d", s_rlCheckContext, gsGameId));

        RLCHECK_CTX(PARAM_gsLoginEmail.Get(gsLoginEmail));
        rlupDebug2(("%s: gsLoginEmail     = %s", s_rlCheckContext, gsLoginEmail));

        RLCHECK_CTX(PARAM_gsLoginPassword.Get(gsLoginPassword));
        rlupDebug2(("%s: gsLoginPassword  = %s", s_rlCheckContext, gsLoginPassword));

        
        //Determine our environment
        rlGamespyTitleId::eEnvironment env = rlGamespyTitleId::DEVELOPMENT;
        if(!strcmp(gsEnvironment, "part"))
        {
            env = rlGamespyTitleId::DEVELOPMENT;
        }
        else if(!strcmp(gsEnvironment, "cert"))
        {
            env = rlGamespyTitleId::CERTIFICATION;
        }
        else if(!strcmp(gsEnvironment, "prod"))
        {
            env = rlGamespyTitleId::PRODUCTION;
        }

        rlGamespyTitleId gamespyTitleId(env,
                                        gsGameName,
                                        gsSecretKey,
                                        gsProductId,
                                        gsGameId,
                                        0, 0, 0,    //Not using Atlas
                                        false);     //Never need to encrypt Sake, since we're behind XLSP

        rlTitleId titleId(gamespyTitleId);

        static const unsigned MIN_AGE_RATING = 0;

        rlInit(m_Allocator, &m_NetSkt, &titleId, MIN_AGE_RATING);

        //Set the account Gamespy should login with.
        RLCHECK_CTX(g_rlGamespy.SetLoginInfo(gsLoginEmail, gsLoginPassword));

        //IMPORTANT: Set Sake to use manual rating.  This is necessary since all rating is
        //           being done through the single account.
        g_rlGamespySake.UseManualRating(true);

        //IMPORTANT: Set rlGamespy to stay connected to GP.  This is necessary so we can detect
        //           GS going down for any reason, and auto-log back in once available again.
        g_rlGamespy.SetRemainConnectedToGp(true);

        rlupDebug2(("%s: Succeeded", s_rlCheckContext));
    }
    rcatchall
    {
        return false;
    }
    return true;
}

bool
rlUploadProxy::InitNorad()
{
    RLCHECK_SET_CTX(InitNorad);

    //Initialize gamespy's performance monitoring lib.
    const char* noradIpStr = "";
    int noradPort;
    const char* noradAppName;

    rtry
    {
        rlupDebug2(("%s: Initializing...", s_rlCheckContext));

        if(PARAM_norad_ip.Get(noradIpStr) 
           && PARAM_norad_port.Get(noradPort)
           && PARAM_norad_appname.Get(noradAppName))
        {
            rlupDebug2(("%s: norad_ip       = %s", s_rlCheckContext, noradIpStr));
            rlupDebug2(("%s: norad_port     = %d", s_rlCheckContext, noradPort));
            rlupDebug2(("%s: norad_appname  = %s", s_rlCheckContext, noradAppName));
            
            RLCHECK_CTX(true == (m_StartedNorad = InitTCPPerfLib(noradIpStr, noradPort, noradAppName)));

            rlupDebug2(("%s: Succeeded", s_rlCheckContext));
        }
        else
        {
            rlupDebug2(("%s: Not all parameters supplied; not using Norad", s_rlCheckContext));
        }
    }
    rcatchall
    {
        return false;
    }
    return true;
}

bool
rlUploadProxy::InitTelnet()
{
    RLCHECK_SET_CTX(InitTelnet);

    rtry
    {
        rlupDebug2(("%s: Initializing...", s_rlCheckContext));

        RLCHECK_CTX(m_TelnetSkt == -1);

        unsigned port = DEFAULT_TELNET_PORT;
        PARAM_telnet_port.Get(port);

        unsigned ip = INADDR_NONE;
        const char* ipStr = "127.0.0.1";

        if(!PARAM_telnet_ip.Get(ipStr))
        {
            ipStr = "";

            char hostname[128];

            rcheck(SOCKET_ERROR != gethostname(hostname, sizeof(hostname)-1),
                    catchall,
                    rlupError(("Error calling gethostname: %d", WSAGetLastError())));

            struct hostent* he = gethostbyname(hostname);

            rcheck(he,
                   catchall,
                   rlupError(("Error calling gethostbyname: %d", WSAGetLastError())));

            Assert(sizeof(ip) == he->h_length);
            memcpy(&ip, he->h_addr_list[0], he->h_length);

            if(INADDR_NONE == ip)
            {
                ip = inet_addr("127.0.0.1");
            }
        }
        else
        {
            ip = inet_addr(ipStr);
        }

        ip = ntohl(ip);

        rlupDebug2(("%s: telnet_ip      = %s", s_rlCheckContext, ipStr));
        rlupDebug2(("%s: telnet_port    = %d", s_rlCheckContext, port));

        netAddress addr(ip, (u16)port);
        rlupDebug2(("%s: Listening on %d.%d.%d.%d:%d", s_rlCheckContext, NET_ADDR_FOR_PRINTF(addr)));

        sockaddr_in sin = {0};
        sin.sin_family = AF_INET;
        sin.sin_port = htons(unsigned short(port));
        sin.sin_addr.s_addr = htonl(ip);

        m_TelnetSkt = socket(AF_INET, SOCK_STREAM, 0);

        rcheck(m_TelnetSkt >= 0,
               catchall,
               rlupError(("Error creating command server listen socket: %d", WSAGetLastError())));
        
        rcheck(0 == bind(m_TelnetSkt, (sockaddr*) &sin, sizeof(sin)),
               catchall,
               rlupError(("Error binding command server listen socket: %d", WSAGetLastError())));
        
        //u_long nonblock = 1;
        //rcheck(0 == ioctlsocket(m_TelnetSkt, FIONBIO, &nonblock),
        //       catchall,
        //       rlupError(("Failed to set telnet socket to non-blocking")));

        rcheck(0 == listen(m_TelnetSkt, SOMAXCONN),
               catchall,
               rlupError(("Error listening on command server socket: %d", WSAGetLastError())));

        m_CmdServer.Start(m_TelnetSkt);

        rlupDebug2(("%s: Succeeded", s_rlCheckContext));
    }
    rcatchall
    {
        if(m_TelnetSkt >= 0)
        {
            closesocket(m_TelnetSkt);
            m_TelnetSkt = -1;
        }

        return false;
    }
    return true;
}

void
rlUploadProxy::UpdateTelnet()
{
    Command* cmd = m_CmdServer.NextCommand();

    if(cmd)
    {
        bool knowCmd = false;
        const char* cmdStr = cmd->GetString();

        rlupDebug1(("Received command: %s", cmdStr));

        if(strstr(cmdStr, "set debug") == cmdStr)
        {
            knowCmd = true;

            int dbgLvl;
            if(sscanf(cmdStr, "set debug %d", &dbgLvl) == 1)
            {
                rlupDebug1(("Setting debug level to: %d", dbgLvl));
                rlSetDebugLevel(dbgLvl);
            }

            char buf[256];
            formatf(buf, sizeof(buf), "debug level is %d", rlGetDebugLevel());
            m_CmdServer.Say(buf);
        }
        else if(!strcmp(cmdStr, "stop"))
        {
            knowCmd = true;

            Stop();
            m_CmdServer.Say("stopped");
        }
        else if(!strcmp(cmdStr, "start"))
        {
            knowCmd = true;

            if(!m_Started)
            {
                if(Start())
                {
                    m_CmdServer.Say("started");
                }
                else
                {
                    m_CmdServer.Say("failed to start");
                }
            }
            else
            {
                m_CmdServer.Say("already started");
            }
        }

        if(!knowCmd)
        {
            char buf[256];
            formatf(buf, sizeof(buf), "unknown command \"%s\"", cmdStr);
            m_CmdServer.Say(buf);
        }

        m_CmdServer.DisposeCommand(cmd);
    }
}

void
rlUploadProxy::CheckCompletedRequests()
{
    //Go through list of requests and process ones that are no longer pending.
    if(!m_RequestList.empty())
    {
        const unsigned SEND_PROGRESS_MS = 500;

        RequestList::iterator it = m_RequestList.begin();
        while(it != m_RequestList.end())
        {
            Request* r = *it;
            Assert(r);

            if(!r->m_Status.Pending())
            {
                ++m_DayStats.m_RequestStats[r->m_Type].m_NumResults[r->m_Status.GetResultCode()];

                ProcessCompletedRequest(r);

                RequestList::iterator next = it;
                ++next;
                m_RequestList.erase(it);
                it = next;
                delete r;
                continue;
            }
            else if((r->m_Progress != r->m_LastProgress) && ((m_CurTime - r->m_LastProgressReportTime) > SEND_PROGRESS_MS))
            {
                SendProgressMsg(r->m_ClientId, r->m_Progress);

                r->m_LastProgress = r->m_Progress;
                r->m_LastProgressReportTime = m_CurTime;
            }
           
            ++it;
        }
    }

    m_EmailFixupMs = 0;
}

void
rlUploadProxy::CheckTimedOutConnections()
{
    //Check for connections that have been inactive for too long
    if(!m_ClientMap.empty())
    {
        ClientMap::iterator it = m_ClientMap.begin();
        while(it != m_ClientMap.end())
        {
            ClientRec* c = it->second;
            Assert(c);

            if(c->m_NumRequestsInProgress)
            {
                c->m_LastActiveTime = m_CurTime;
            }
            else
            {
                if((m_CurTime - c->m_LastActiveTime) >= m_InactivityTimeoutMs)
                {
                    rlupDebug2(("Disconnecting inactive client connection %d.%d.%d.%d:%d",
                              NET_ADDR_FOR_PRINTF(c->m_Address)));
                    
                    it = RemoveClient(c->m_ClientId);
                    continue;
                }
            }

            ++it;
        }
    }
}

rlUploadProxy::ClientMap::iterator
rlUploadProxy::RemoveClient(const unsigned clientId)
{
    ClientMap::iterator it = m_ClientMap.find(clientId);
    if(it != m_ClientMap.end())
    {
        ClientRec* c = it->second;
        ClientMap::iterator next = it;
        ++next;
        m_ClientMap.erase(it);
        it = next;
        delete c;
    }
    return it;
}

bool
rlUploadProxy::ReceiveFromClient(ClientRec* c,
                                 const void* data,
                                 const unsigned size)
{
    Assert(c && data && size);

    RLCHECK_SET_CTX(ReceiveFromClient);

    bool success = false; 

    rtry
    {
        unsigned msgId;
        RLCHECK_CTX(netMessage::GetId(&msgId, data, size));

#define HANDLE_REQUEST(name, type) \
        if(!success && msgId == rlUploadMsg##name##Request::MSG_ID()) \
        { \
            rlupDebug2(("%s: " #name " request (%u bytes) from %d.%d.%d.%d:%d", s_rlCheckContext, size, NET_ADDR_FOR_PRINTF(c->m_Address))); \
            ++m_DayStats.m_HourStats[m_CurHour].m_NumRequests; \
            m_DayStats.m_RequestStats[ REQUEST_##type ].m_Size.AddSample(size); \
            rlUploadMsg##name##Request req; \
            RLCHECK_CTX(req.Import(data, size)); \
            RLCHECK_CTX(ReceiveRequest##name##(c, req)); \
            ++c->m_NumRequestsInProgress; \
            success = true; \
        }

        HANDLE_REQUEST(GetTableSize, GET_TABLE_SIZE);
        HANDLE_REQUEST(GetNumOwned, GET_NUM_OWNED);
        HANDLE_REQUEST(ReadRecordsByRank, READ_RECORDS_BY_RANK);
        HANDLE_REQUEST(ReadRecordsByGamer, READ_RECORDS_BY_GAMER);
        HANDLE_REQUEST(ReadRandomRecord, READ_RANDOM_RECORD);
        HANDLE_REQUEST(ReadRecord, READ_RECORD);
        HANDLE_REQUEST(RateRecord, RATE_RECORD);
        HANDLE_REQUEST(DeleteRecord, DELETE_RECORD);
        HANDLE_REQUEST(UploadFile, UPLOAD_FILE);
        HANDLE_REQUEST(OverwriteFile, OVERWRITE_FILE);
        HANDLE_REQUEST(DownloadFile, DOWNLOAD_FILE);
        HANDLE_REQUEST(OverwriteUserData, OVERWRITE_USER_DATA);

#undef HANDLE_REQUEST

        if(!success)
        {
            rlupError(("%s: Unhandled msg ID (%d), ignoring", s_rlCheckContext, msgId));
        }
    }
    rcatchall
    {
    }

    return success;
}

void
rlUploadProxy::QueueRequest(Request* r)
{
    r->m_StartTime = m_CurTime;
    m_RequestList.push_back(r);
}

bool
rlUploadProxy::ReceiveRequestGetTableSize(ClientRec* c, const rlUploadMsgGetTableSizeRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestGetTableSize);

    GetTableSizeRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new GetTableSizeRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, req.m_MinNumRatings, req.m_MaxNumRatings));
        RLCHECK_CTX(m_Upload.GetTableSize(r->m_TableId, r->m_MinNumRatings, r->m_MaxNumRatings, &r->m_NumRecords, &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestGetNumOwned(ClientRec* c, const rlUploadMsgGetNumOwnedRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequest);

    GetNumOwnedRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new GetNumOwnedRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, req.m_GamerHandle));
        RLCHECK_CTX(m_Upload.GetNumOwned(r->m_TableId, r->m_GamerHandle, &r->m_NumOwned, &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestReadRecordsByRank(ClientRec* c, const rlUploadMsgReadRecordsByRankRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestReadRecordsByRank);

    ReadRecordsByRankRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new ReadRecordsByRankRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, req.m_StartingRank, req.m_MinNumRatings, req.m_MaxRecords));
        RLCHECK_CTX(m_Upload.ReadRecordsByRank(r->m_TableId, r->m_StartingRank, r->m_MinNumRatings, r->m_MaxRecords, r->m_Records, &r->m_NumRecordsRead, &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestReadRecordsByGamer(ClientRec* c, const rlUploadMsgReadRecordsByGamerRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestReadRecordsByGamer);

    ReadRecordsByGamerRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new ReadRecordsByGamerRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, req.m_GamerHandle, req.m_MaxRecords));
        RLCHECK_CTX(m_Upload.ReadRecordsByGamer(r->m_TableId, r->m_GamerHandle, r->m_MaxRecords, r->m_Records, &r->m_NumRecordsRead, &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestReadRandomRecord(ClientRec* c, const rlUploadMsgReadRandomRecordRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestReadRandomRecord);

    ReadRandomRecordRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new ReadRandomRecordRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, 
                            req.m_GamerHandle, 
                            req.m_MinNumRatings, 
                            req.m_MaxNumRatings,
                            req.m_ContentIdFlags));
        RLCHECK_CTX(m_Upload.ReadRandomRecord(r->m_TableId, 
                                              r->m_GamerHandle, 
                                              r->m_MinNumRatings, 
                                              r->m_MaxNumRatings, 
                                              r->m_ContentIdFlags,                                              
                                              &r->m_Record, 
                                              &r->m_NumRecordsRead, 
                                              &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestReadRecord(ClientRec* c, const rlUploadMsgReadRecordRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestReadRecord);

    ReadRecordRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new ReadRecordRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, req.m_RecordId));
        RLCHECK_CTX(m_Upload.ReadRecord(r->m_TableId, r->m_RecordId, &r->m_Record, &r->m_NumRecordsRead, &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestRateRecord(ClientRec* c, const rlUploadMsgRateRecordRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestRateRecord);

    RateRecordRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new RateRecordRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, req.m_Record, req.m_Rating));
        RLCHECK_CTX(m_Upload.RateRecord(r->m_TableId, r->m_Record, r->m_Rating, /*&r->m_NumRatings, &r->m_AverageRating,*/&r->m_Record, &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestDeleteRecord(ClientRec* c, const rlUploadMsgDeleteRecordRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestDeleteRecord);

    DeleteRecordRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new DeleteRecordRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, req.m_RecordId));
        RLCHECK_CTX(m_Upload.DeleteRecord(r->m_TableId, r->m_RecordId, &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestUploadFile(ClientRec* c, const rlUploadMsgUploadFileRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestUploadFile);

    UploadFileRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new UploadFileRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, 
                       req.m_RecordLimit,
                       req.m_GamerHandle,
                       req.m_GamerName,
                       req.m_NumFiles,
                       (const u8**)req.m_FileData,
                       req.m_FileSizes,
                       req.m_UserData,
                       req.m_UserDataSize,
                       req.m_ContentId));
        RLCHECK_CTX(m_Upload.UploadFile(r->m_TableId, 
                                   r->m_RecordLimit,
                                   r->m_GamerHandle,
                                   r->m_GamerName,
                                   r->m_NumFiles,
                                   (const u8**)r->m_FileData,
                                   r->m_FileSizes,
                                   r->m_UserData,
                                   r->m_UserDataSize,
                                   r->m_ContentId,
                                   &r->m_Record, 
                                   &r->m_Progress,
                                   &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestOverwriteFile(ClientRec* c, const rlUploadMsgOverwriteFileRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestOverwriteFile);

    OverwriteFileRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new OverwriteFileRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, 
                       req.m_CurrentRecord,
                       req.m_NumFiles,
                       (const u8**)req.m_FileData,
                       req.m_FileSizes,
                       req.m_UserData,
                       req.m_UserDataSize,
                       req.m_ContentId));
        RLCHECK_CTX(m_Upload.OverwriteFile(r->m_TableId, 
                                      r->m_CurrentRecord,
                                      r->m_NumFiles,
                                      (const u8**)r->m_FileData,
                                      r->m_FileSizes,
                                      r->m_UserData,
                                      r->m_UserDataSize,
                                      r->m_ContentId,
                                      &r->m_NewRecord, 
                                      &r->m_Progress,
                                      &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestDownloadFile(ClientRec* c, const rlUploadMsgDownloadFileRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestDownloadFile);

    DownloadFileRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new DownloadFileRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_FileId, req.m_BufSize));
        RLCHECK_CTX(m_Upload.DownloadFile(r->m_FileId, 
                                     r->m_Buf, 
                                     r->m_BufSize, 
                                     &r->m_NumBytesDownloaded, 
                                     &r->m_Progress,
                                     &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlUploadProxy::ReceiveRequestOverwriteUserData(ClientRec* c, const rlUploadMsgOverwriteUserDataRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestOverwriteUserData);

    OverwriteUserDataRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new OverwriteUserDataRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableId, 
                       req.m_CurrentRecord,
                       req.m_UserData,
                       req.m_UserDataSize));
        RLCHECK_CTX(m_Upload.OverwriteUserData(r->m_TableId, 
                                          r->m_CurrentRecord,
                                          r->m_UserData,
                                          r->m_UserDataSize,
                                          &r->m_NewRecord, 
                                          &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

void
rlUploadProxy::ProcessCompletedRequest(Request* r)
{
    Assert(r);

    RLCHECK_SET_CTX(ProcessCompletedRequest);

    //Record the time it took to handle the request (minus any time it took to send email in previous frame).
    unsigned responseMs = m_CurTime - r->m_StartTime - m_EmailFixupMs;
    m_DayStats.m_HourStats[m_CurHour].m_ResponseMs.AddSample(responseMs);
    m_DayStats.m_RequestStats[r->m_Type].m_ResponseMs.AddSample(responseMs);

    //Find the client that made the request
    ClientMap::iterator it = m_ClientMap.find(r->m_ClientId);
    if(m_ClientMap.end() == it)
    {
        return; //Client no longer connected; ignore
    }

    ClientRec* c = it->second;
    Assert(c);

    //Mark client as no longer performing this request
    --c->m_NumRequestsInProgress;
    Assert(c->m_NumRequestsInProgress >= 0);

    //Handle based on the request type
#define REQUEST_CASE(name, type) \
    case( REQUEST_##type ): \
        rlupDebug2(("%s: " #name " completed (%s) for %d.%d.%d.%d:%d", \
                  s_rlCheckContext, \
                  r->m_Status.Succeeded() ? "success" : "failed", \
                  NET_ADDR_FOR_PRINTF(c->m_Address))); \
        if(!SendReply##name##(c, (name##Request*)r)) \
        { \
            rlupError(("rlUploadProxy::ProcessCompletedRequest: Failed to send " #name " reply to %d.%d.%d.%d:%d; disconnecting...", \
                     NET_ADDR_FOR_PRINTF(c->m_Address))); \
            RemoveClient(c->m_ClientId); \
        } \
        break;

    switch(r->m_Type)
    {
    REQUEST_CASE(GetTableSize, GET_TABLE_SIZE);
    REQUEST_CASE(GetNumOwned, GET_NUM_OWNED);
    REQUEST_CASE(ReadRecordsByRank, READ_RECORDS_BY_RANK);
    REQUEST_CASE(ReadRecordsByGamer, READ_RECORDS_BY_GAMER);
    REQUEST_CASE(ReadRandomRecord, READ_RANDOM_RECORD);
    REQUEST_CASE(ReadRecord, READ_RECORD);
    REQUEST_CASE(RateRecord, RATE_RECORD);
    REQUEST_CASE(DeleteRecord, DELETE_RECORD);
    REQUEST_CASE(UploadFile, UPLOAD_FILE);
    REQUEST_CASE(OverwriteFile, OVERWRITE_FILE);
    REQUEST_CASE(DownloadFile, DOWNLOAD_FILE);
    REQUEST_CASE(OverwriteUserData, OVERWRITE_USER_DATA);
    default:
        rlupError(("Unhandled request case (%d)", r->m_Type));
    };

#undef REQUEST_CASE
}

bool
rlUploadProxy::SendToClient(ClientRec* c, 
                            const void* data,
                            const unsigned size)
{
    Assert(c && data && size);

    OutboundMsg* m = rage_new OutboundMsg();
    
    if(!m)
    {
        rlupError(("rlUploadProxy::SendToClient: Failed to allocate OutboundMsg"));
        return false;
    }

    if(!m->Init(data, size))
    {
        rlupError(("rlUploadProxy::SendToClient: Failed to Init() OutboundMsg (%d bytes)", size));
        return false;
    }

    c->m_OutboundMsgList.push_back(m);

    return true;
}

#define RLUPLOADPROXY_SEND(c, msg, maxSize, type, id) \
    char* buf = rage_new char[maxSize]; \
    if(!buf) \
    {  \
        rlupError(("RLUPLOADPROXY_SEND(" #type "): Failed to allocate %d bytes", maxSize)); \
        return false; \
    } \
    unsigned size = 0; \
    bool success = true; \
    if(!msg.Export(buf, maxSize, &size)) \
    { \
        rlupError(("RLUPLOADPROXY_SEND(" #type "): Export failed")); \
        success = false; \
    } \
    if(success && !SendToClient(c, buf, size)) \
    { \
        rlupError(("RLUPLOADPROXY_SEND(" #type "): SendToClient failed")); \
        success = false; \
    } \
    delete [] buf; \
    if(REQUEST_INVALID != REQUEST_##id ) m_DayStats.m_ReplyStats[ REQUEST_##id ].m_Size.AddSample(size); \
    return success;

bool
rlUploadProxy::SendProgressMsg(const unsigned clientId,
                               const float progress)
{
    ClientMap::iterator it = m_ClientMap.find(clientId);
    if(m_ClientMap.end() == it)
    {
        return true; //Client no longer connected; ignore
    }

    ClientRec* c = it->second;
    Assert(c);

    rlUploadMsgProgress msg;
    msg.Reset(progress);

    RLUPLOADPROXY_SEND(c, msg, 32, rlUploadMsgProgress, INVALID);
}

bool
rlUploadProxy::SendReplyGetTableSize(ClientRec* c, GetTableSizeRequest* r)
{
    rlUploadMsgGetTableSizeReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_NumRecords);

    RLUPLOADPROXY_SEND(c, msg, 32, rlUploadMsgGetTableSizeReply, GET_TABLE_SIZE);
}

bool
rlUploadProxy::SendReplyGetNumOwned(ClientRec* c, GetNumOwnedRequest* r)
{
    rlUploadMsgGetNumOwnedReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_NumOwned);

    RLUPLOADPROXY_SEND(c, msg, 32, rlUploadMsgGetNumOwnedReply, GET_NUM_OWNED);
}

bool
rlUploadProxy::SendReplyReadRecordsByGamer(ClientRec* c, ReadRecordsByGamerRequest* r)
{
    rlUploadMsgReadRecordsByGamerReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_MaxRecords, &r->m_NumRecordsRead, r->m_Records);

    RLUPLOADPROXY_SEND(c, msg, 64 + (r->m_NumRecordsRead * sizeof(rlUploadRecord)), rlUploadMsgReadRecordsByGamerReply, READ_RECORDS_BY_GAMER);
}

bool
rlUploadProxy::SendReplyReadRecordsByRank(ClientRec* c, ReadRecordsByRankRequest* r)
{
    rlUploadMsgReadRecordsByRankReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_MaxRecords, &r->m_NumRecordsRead, r->m_Records);

    RLUPLOADPROXY_SEND(c, msg, 64 + (r->m_NumRecordsRead * sizeof(rlUploadRecord)), rlUploadMsgReadRecordsByRankReply, READ_RECORDS_BY_RANK);
}

bool
rlUploadProxy::SendReplyReadRandomRecord(ClientRec* c, ReadRandomRecordRequest* r)
{
    rlUploadMsgReadRandomRecordReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_NumRecordsRead, r->m_Record);

    RLUPLOADPROXY_SEND(c, msg, 512, rlUploadMsgReadRandomRecordReply, READ_RANDOM_RECORD);
}

bool
rlUploadProxy::SendReplyReadRecord(ClientRec* c, ReadRecordRequest* r)
{
    rlUploadMsgReadRecordReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_NumRecordsRead, r->m_Record);

    RLUPLOADPROXY_SEND(c, msg, 512, rlUploadMsgReadRecordReply, READ_RECORD);
}

bool
rlUploadProxy::SendReplyRateRecord(ClientRec* c, RateRecordRequest* r)
{
    rlUploadMsgRateRecordReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), /*r->m_NumRatings*/r->m_Record.GetNumRatings(), /*r->m_AverageRating*/r->m_Record.GetAverageRating());

    RLUPLOADPROXY_SEND(c, msg, 32, rlUploadMsgRateRecordReply, RATE_RECORD);
}

bool
rlUploadProxy::SendReplyDeleteRecord(ClientRec* c, DeleteRecordRequest* r)
{
    rlUploadMsgDeleteRecordReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode());

    RLUPLOADPROXY_SEND(c, msg, 32, rlUploadMsgDeleteRecordReply, DELETE_RECORD);
}

bool
rlUploadProxy::SendReplyUploadFile(ClientRec* c, UploadFileRequest* r)
{
    rlUploadMsgUploadFileReply msg;    
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_Record);

    RLUPLOADPROXY_SEND(c, msg, 32 + sizeof(rlUploadRecord), rlUploadMsgUploadFileReply, UPLOAD_FILE);
}

bool
rlUploadProxy::SendReplyOverwriteFile(ClientRec* c, OverwriteFileRequest* r)
{
    rlUploadMsgOverwriteFileReply msg;    
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_NewRecord);

    RLUPLOADPROXY_SEND(c, msg, 32 + sizeof(rlUploadRecord), rlUploadMsgOverwriteFileReply, OVERWRITE_FILE);
}

bool
rlUploadProxy::SendReplyDownloadFile(ClientRec* c, DownloadFileRequest* r)
{
    rlUploadMsgDownloadFileReply msg(r->m_Buf, r->m_BufSize);
    
    Assert(!r->m_Status.Succeeded() || (r->m_BufSize >= r->m_NumBytesDownloaded));

    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_NumBytesDownloaded);

    RLUPLOADPROXY_SEND(c, msg, 32 + r->m_NumBytesDownloaded, rlUploadMsgDownloadFileReply, DOWNLOAD_FILE);
}

bool
rlUploadProxy::SendReplyOverwriteUserData(ClientRec* c, OverwriteUserDataRequest* r)
{
    rlUploadMsgOverwriteUserDataReply msg;    
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_NewRecord);

    RLUPLOADPROXY_SEND(c, msg, 32 + sizeof(rlUploadRecord), rlUploadMsgOverwriteUserDataReply, OVERWRITE_USER_DATA);
}

bool
rlUploadProxy::CreateServerSocket()
{
    RLCHECK_SET_CTX(CreateServerSocket);

    rtry
    {
        SYS_CS_SYNC(m_CsServerSocket);

        RLCHECK_CTX(-1 != (m_ServerSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)));

        unsigned titleserverport = DEFAULT_TITLE_SERVER_PORT;
        if(!PARAM_titleserverport.Get(titleserverport))
        {
            rlupDebug2(("%s: titleserverport parameter not specified; using default (%d)", titleserverport));
        }

        sockaddr_in addr;
        sysMemSet(&addr, 0, sizeof(addr));
        addr.sin_family         = AF_INET;
        addr.sin_addr.s_addr    = INADDR_ANY;
        addr.sin_port           = htons((unsigned short)titleserverport);

        rcheck(SOCKET_ERROR != bind(m_ServerSocket, (struct sockaddr*)&addr, sizeof(addr)),
               catchall,
               rlupError(("%s: Failed to bind server socket to port %d (error=%d)", 
                        s_rlCheckContext, titleserverport, WSAGetLastError())));

        u_long nonblock = 1;
        rcheck(0 == ioctlsocket(m_ServerSocket, FIONBIO, &nonblock),
               catchall,
               netError(("Failed to set socket to non-blocking")));

        const int LISTEN_BACKLOG = 500; //This will automatically get capped by underlying code.
        rcheck(SOCKET_ERROR != listen(m_ServerSocket, LISTEN_BACKLOG),
               catchall,
               rlupError(("%s: Failed to listen() (error=%d)", s_rlCheckContext, WSAGetLastError())));

        rlupDebug2(("%s: Server listening for connections on port %d", s_rlCheckContext, titleserverport));
    }
    rcatchall
    {
        return false;
    }
    return true;
}

bool
rlUploadProxy::StartAcceptThread()
{
    RLCHECK_SET_CTX(StartAcceptThread);

    Assert(!m_WaitSema);
    Assert(sysIpcThreadIdInvalid == m_ThreadHandle);

    rtry
    {
        m_WaitSema = sysIpcCreateSema(0);

        RLCHECK_CTX(m_WaitSema);

        m_ThreadHandle = sysIpcCreateThread(&rlUploadProxy::AcceptThreadFunc,
                                            this,
                                            sysIpcMinThreadStackSize,
                                            PRIO_NORMAL,
                                            "rlUploadProxy::AcceptThreadFunc");

        RLCHECK_CTX(sysIpcThreadIdInvalid != m_ThreadHandle);

        //Wait for the thread to start.
        sysIpcWaitSema(m_WaitSema);
    }
    rcatchall
    {
        return false;
    }
    return true;
}

void
rlUploadProxy::AcceptThreadFunc(void *userdata)
{
    rlupDebug2(("rlUploadProxy::AcceptThreadFunc() starting..."));

    rlUploadProxy* p = (rlUploadProxy*)userdata;
    Assert(p);

    sysIpcSignalSema(p->m_WaitSema);

    unsigned sleepms = DEFAULT_ACCEPT_SLEEP_MS;
    PARAM_acceptsleepms.Get(sleepms);

    while(1)
    {
        SYS_CS_SYNC(p->m_CsServerSocket);

        if(p->m_ServerSocket != INVALID_SOCKET)
        {
            sockaddr_in addr;
            sysMemSet(&addr, 0, sizeof(addr));

            int size = sizeof(addr);

            SOCKET s = accept(p->m_ServerSocket, (struct sockaddr*)&addr, &size);

            if(INVALID_SOCKET != s)
            {
                SYS_CS_SYNC(p->m_CsAcceptedList);
                
                AcceptedConnection* a = rage_new AcceptedConnection();
                a->m_Socket  = s;
                a->m_Address = netAddress(ntohl(addr.sin_addr.s_addr), ntohs(addr.sin_port));

                //rlupDebug2(("rlUploadProxy::AcceptThreadFunc(): Accepted connection from %d.%d.%d.%d:%d",
                //          NET_ADDR_FOR_PRINTF(a->m_Address)));

                static unsigned s_id = 0;
                while(++s_id == INVALID_CLIENTID);
                a->m_ClientId = s_id;
                
                p->m_AcceptedList.push_back(a);
            }
            else if(WSAEWOULDBLOCK == WSAGetLastError())
            {
                sysIpcSleep(sleepms);
            }
            else
            {
                rlupDebug2(("Error accepting connection (%d)", WSAGetLastError()));
            }
        }
        else
        {
            break; //Server socket closed, shuttting down.
        }
    }
}

void
rlUploadProxy::CheckNewConnections()
{
    //Create client records for each accepted connection.
    SYS_CS_SYNC(m_CsAcceptedList);

    if(!m_AcceptedList.empty())
    {
        //rlupDebug2(("Accepting %d connections...", m_AcceptedList.size()));

        HourStats& hs = m_DayStats.m_HourStats[m_CurHour];
        hs.m_NumNewConnections.AddSample(m_AcceptedList.size());

        while(!m_AcceptedList.empty())
        {
            AcceptedConnection* a = *m_AcceptedList.begin();
            m_AcceptedList.pop_front();

            ClientMap::iterator it = m_ClientMap.find(a->m_ClientId);

            if(m_ClientMap.end() != it)
            {
                rlupError(("Got connection from client already in our client map. Removing previous client."));            
                ClientRec *c = it->second;
                m_ClientMap.erase(it);
                delete c;
            }

            ClientRec* c = rage_new ClientRec();

            if(c)
            {
                c->m_ClientId       = a->m_ClientId;
                c->m_Socket         = a->m_Socket;
                c->m_Address        = a->m_Address;
                c->m_LastActiveTime = m_CurTime;
                
                m_ClientMap.insert(c->m_ClientId, c);

                rlupDebug2(("Created clientrec (ID=%d) for new connection %d.%d.%d.%d:%d",
                          c->m_ClientId, NET_ADDR_FOR_PRINTF(c->m_Address)));
            }
            else
            {
                rlupError(("Failed to allocate ClientRec; closing connection"));
                closesocket(a->m_Socket);
            }

            delete a;
        }
    }

    Assert(m_AcceptedList.empty());
}

void
rlUploadProxy::CheckNewRequests()
{
    if(m_ClientMap.empty())
    {
        return; //No sockets to check
    }

    rtry
    {
        //Select sockets that are readable (or exceptioned)
        fd_set readfds;
        FD_ZERO(&readfds);

        fd_set exceptionfds;
        FD_ZERO(&exceptionfds);

        SOCKET highestSocket = 0;
        
        ClientMap::iterator it = m_ClientMap.begin();
        while(it != m_ClientMap.end())
        {
            ClientRec* c = it->second;
            Assert(c);

            if(c->m_Socket != INVALID_SOCKET)
            {
                FD_SET(c->m_Socket, &readfds);
                FD_SET(c->m_Socket, &exceptionfds);

                if(c->m_Socket > highestSocket)
                {
                    highestSocket = c->m_Socket;
                }
            }
            else
            {
                rlupError(("Client %d.%d.%d.%d:%d has invalid socket; removing client...",
                         NET_ADDR_FOR_PRINTF(c->m_Address)));
                it = RemoveClient(c->m_ClientId);
                rcheck(!m_ClientMap.empty(), catchall, ); //Our last client was removed
                continue;
            }

            ++it;
        }

        timeval t;
        t.tv_sec  = 0;
        t.tv_usec = 100000;

        int result = select(highestSocket, &readfds, NULL, &exceptionfds, &t);

        rcheck(-1 != result, catchall, rlupError(("select() error (%d)", WSAGetLastError())));
        rcheck(result, catchall, ); //Timeout, no biggie

        //Handle the reads and exceptions
        it = m_ClientMap.begin();
        while(it != m_ClientMap.end())
        {
            ClientRec* c = it->second;
            Assert(c);

            if(FD_ISSET(c->m_Socket, &exceptionfds))
            {
                rlupError(("Client %d.%d.%d.%d:%d had socket exception (%d); removing client...",
                         NET_ADDR_FOR_PRINTF(c->m_Address),
                         WSAGetLastError()));
                it = RemoveClient(c->m_ClientId);
            }

            if(FD_ISSET(c->m_Socket, &readfds))
            {
                //rlupDebug2(("Socket is readable for client %d [%d.%d.%d.%d:%d]",
                //          c->m_ClientId, NET_ADDR_FOR_PRINTF(c->m_Address)));

                if(!ReceiveFromClient(c))
                {
                    //Client disconnected, or there was read error
                    it = RemoveClient(c->m_ClientId);
                    continue;
                }
            }

            ++it;
        }
    }
    rcatchall
    {
    }
}

bool
rlUploadProxy::ReceiveFromClient(ClientRec* c)
{
    Assert(c);

    //Have we read the msg size?  If not, continue reading it.
    if(c->m_MsgSizeBytesRead < sizeof(c->m_MsgSize))
    {
        //Continue reading the message size
        int bytesRead = recv(c->m_Socket, 
                             &(((char*)&c->m_MsgSize)[c->m_MsgSizeBytesRead]),
                             sizeof(c->m_MsgSize) - c->m_MsgSizeBytesRead,
                             0);

        if(0 < bytesRead)
        {
            c->m_MsgSizeBytesRead += bytesRead;
            m_IncomingBytesAccum += bytesRead;
        }
        else if(0 == bytesRead)
        {
            rlupDebug2(("Client %d.%d.%d.%d:%d disconnected", NET_ADDR_FOR_PRINTF(c->m_Address)));
            return false;
        }
        else
        {
            rlupDebug2(("Client %d.%d.%d.%d:%d had error reading msg size (err=%d, %d of %d bytes)", 
                      NET_ADDR_FOR_PRINTF(c->m_Address),
                      WSAGetLastError(), 
                      c->m_MsgSizeBytesRead, 
                      sizeof(c->m_MsgSize)));
            return false;
        }

        //If still not done, return.
        if(c->m_MsgSizeBytesRead < sizeof(c->m_MsgSize))
        {
            return true;
        }

        c->m_MsgSize = ntohl(c->m_MsgSize);

        if(c->m_MsgSize > s_MaxMsgSize)
        {
            rlupDebug2(("Client %d.%d.%d.%d:%d msg too big (%u bytes, %u is max)", 
                      NET_ADDR_FOR_PRINTF(c->m_Address),
                      c->m_MsgSize,
                      s_MaxMsgSize));
            return false;
        }

        Assert(!c->m_MsgBuf);
        Assert(!c->m_MsgBytesRead);

        //rlupDebug2(("Got msg size: %u", c->m_MsgSize));

        return true; //One recv() per call
    }

    Assert(c->m_MsgSizeBytesRead == sizeof(c->m_MsgSize));

    //Allocate our message buffer, if necessary
    if(!c->m_MsgBuf)
    {
        Assert(c->m_MsgSize <= s_MaxMsgSize);

        c->m_MsgBuf = rage_new u8[c->m_MsgSize];

        if(!c->m_MsgBuf)
        {
            rlupError(("Failed to allocated message buffer (%d bytes)", c->m_MsgSize));
            return false;
        }
    }

    Assert(c->m_MsgBuf);
    Assert(c->m_MsgBytesRead < c->m_MsgSize);

    //Continue reading the message
    int bytesRead = recv(c->m_Socket, 
                         (char*)&c->m_MsgBuf[c->m_MsgBytesRead],
                         c->m_MsgSize - c->m_MsgBytesRead,
                         0);

    if(0 < bytesRead)
    {
        c->m_MsgBytesRead += bytesRead;
        m_IncomingBytesAccum += bytesRead;
    }
    else if(0 == bytesRead)
    {
        rlupDebug2(("Client %d.%d.%d.%d:%d disconnected while reading msg (%d of %d bytes)", 
                  NET_ADDR_FOR_PRINTF(c->m_Address),
                  c->m_MsgBytesRead, 
                  c->m_MsgSize));
        return false;
    }
    else
    {
        rlupDebug2(("Client %d.%d.%d.%d:%d had error while reading msg (err=%d, %d of %d bytes)", 
                  NET_ADDR_FOR_PRINTF(c->m_Address),
                  WSAGetLastError(), 
                  c->m_MsgBytesRead, 
                  c->m_MsgSize));
        return false;
    }

    Assert(c->m_MsgBytesRead <= c->m_MsgSize);

    //If done...
    if(c->m_MsgBytesRead == c->m_MsgSize)
    {
        //rlupDebug2(("Received msg from %d.%d.%d.%d:%d (%u bytes)", 
        //          NET_ADDR_FOR_PRINTF(c->m_Address),
        //          c->m_MsgBytesRead));

        //Handle the message
        ReceiveFromClient(c, c->m_MsgBuf, c->m_MsgSize);

        //Reset message reading
        delete [] c->m_MsgBuf;
        c->m_MsgBuf = 0;
        c->m_MsgBytesRead = 0;
        c->m_MsgSize = 0;
        c->m_MsgSizeBytesRead = 0;
    }

    return true;
}

void 
rlUploadProxy::SendToClients()
{
    if(m_ClientMap.empty())
    {
        return;
    }

    ClientMap::iterator it = m_ClientMap.begin();
    while(it != m_ClientMap.end())
    {
        ClientRec* c = it->second;
        Assert(c);

        //If client somehow has an invalid socket, then remove them.
        if(c->m_Socket == INVALID_SOCKET)
        {
            it = RemoveClient(c->m_ClientId);
            continue;
        }

        //If client has something to send and their socket is writable, then continue sending.
        if(!c->m_OutboundMsgList.empty() && WaitForSocket(c->m_Socket, false, 0))
        {
            if(!SendToClient(c))
            {
                it = RemoveClient(c->m_ClientId);
                continue;
            }
        }

        ++it;
    }
}

bool
rlUploadProxy::SendToClient(ClientRec* c)
{
    Assert(!c->m_OutboundMsgList.empty());

    OutboundMsg* m = *c->m_OutboundMsgList.begin();
    Assert(m);

    //Have we written the msg size?  If not, continue writing it.
    if(c->m_MsgBytesSent < sizeof(m->m_MsgSize))
    {
        u32 msgSize = htonl(m->m_MsgSize);
        Assert(sizeof(m->m_MsgSize) == sizeof(msgSize));

        int bytesSent = send(c->m_Socket, 
                             &(((char*)&msgSize)[c->m_MsgBytesSent]),
                             sizeof(msgSize) - c->m_MsgBytesSent,
                             0);

        if(0 < bytesSent)
        {
            c->m_MsgBytesSent += bytesSent;
            m_OutgoingBytesAccum += bytesSent;
        }
        else
        {
            rlupDebug2(("Client %d.%d.%d.%d:%d had error sending msg size (err=%d, %d of %d bytes)", 
                      NET_ADDR_FOR_PRINTF(c->m_Address),
                      WSAGetLastError(), c->m_MsgBytesSent, m->m_MsgSize));
            return false;
        }

        //If still not done, return.
        if(c->m_MsgBytesSent < sizeof(msgSize))
        {
            return true;
        }

        //rlupDebug2(("Wrote msg size: %u", m->m_MsgSize));

        return true; //One send() per call
    }

    Assert(m->m_MsgBuf);
    Assert(c->m_MsgBytesSent < (m->m_MsgSize + sizeof(m->m_MsgSize)));

    //Continue reading the message
    int offset = c->m_MsgBytesSent - sizeof(m->m_MsgSize);
    int bytesSent = send(c->m_Socket, 
                         (char*)&m->m_MsgBuf[offset],
                         m->m_MsgSize - offset,
                         0);

    if(0 < bytesSent)
    {
        c->m_MsgBytesSent += bytesSent;
        m_OutgoingBytesAccum += bytesSent;
    }
    else
    {
        rlupDebug2(("Client %d.%d.%d.%d:%d had error while sending msg (err=%d, %d of %d bytes)", 
                  NET_ADDR_FOR_PRINTF(c->m_Address),
                  WSAGetLastError(), offset, m->m_MsgSize));
        return false;
    }

    Assert(c->m_MsgBytesSent <= (m->m_MsgSize + sizeof(m->m_MsgSize)));

    //If done...
    if(c->m_MsgBytesSent == (m->m_MsgSize + sizeof(m->m_MsgSize)))
    {
        rlupDebug2(("Sent msg to client %d.%d.%d.%d:%d (%u bytes)",
                  NET_ADDR_FOR_PRINTF(c->m_Address),
                  c->m_MsgBytesSent - sizeof(m->m_MsgSize)));

        c->m_OutboundMsgList.erase(c->m_OutboundMsgList.begin());
        delete m;
        c->m_MsgBytesSent = 0;
    }

    return true;
}

bool
rlUploadProxy::WaitForSocket(SOCKET s, 
                             const bool forRead, //if false, it's for writing
                             const unsigned timeoutSecs)
{
    rtry
    {
        fd_set fds;
        fd_set exceptionfds;

        FD_ZERO(&fds);
        FD_SET(s, &fds);

        FD_ZERO(&exceptionfds);
        FD_SET(s, &exceptionfds);

        timeval t;
        t.tv_sec  = timeoutSecs;
        t.tv_usec = 0;

        int result = select(s+1, forRead ? &fds : NULL, forRead ? NULL : &fds, &exceptionfds, &t);

        rcheck(-1 != result, catchall, rlupError(("select() error (%d)", WSAGetLastError())));
//        rcheck(result, catchall, rlupDebug2(("select() timed out after %d seconds", t.tv_sec)));
        rcheck(!FD_ISSET(s, &exceptionfds), catchall, rlupError(("select() found exception on our socket")));
        rcheck(FD_ISSET(s, &fds), catchall, rlupError(("select() returned sockets, but our socket is not one of them; most likely an error")));
    }
    rcatchall 
    {
        return false;
    }
    return true;
}

void
rlUploadProxy::UpdateStats()
{
    //Get frame time
    unsigned frameMs = 0;

    static unsigned s_LastTime = 0;
    if(s_LastTime)
    {
        frameMs = m_CurTime - s_LastTime;
    }
    s_LastTime = m_CurTime;

    //Add to uptime
    m_UptimeMs += frameMs;

    //Update hour stats
    HourStats& hs = m_DayStats.m_HourStats[m_CurHour];
    hs.m_HeapUsed.AddSample(m_Allocator->GetMemoryUsed(-1));
    hs.m_FrameMs.AddSample(frameMs);
    hs.m_NumConcurrent.AddSample(m_ClientMap.size());

    const unsigned BW_PERIOD_MS = 1000;
    static unsigned s_LastBwTime = 0;
    unsigned bwDeltaMs = m_CurTime - s_LastBwTime;
    if(bwDeltaMs > BW_PERIOD_MS)
    {
        if(s_LastBwTime)
        {
            hs.m_IncomingKBps.AddSample((bwDeltaMs * m_IncomingBytesAccum / 1000.0f)/1000);
            m_IncomingBytesAccum = 0;

            hs.m_OutgoingKBps.AddSample((bwDeltaMs * m_OutgoingBytesAccum / 1000.0f)/1000);
            m_OutgoingBytesAccum = 0;
        }

        s_LastBwTime = m_CurTime;
    }

    //Advance hour, and send daily report if necessary.
    SYSTEMTIME t;
    GetLocalTime(&t);

    if(m_CurHour != t.wHour)
    {
        m_CurHour = t.wHour;

        //If it is the daily email hour, then email a stats report and flush all stats.
        if(m_CurHour == m_DailyEmailHour)
        {
            ReportStats();
            m_DayStats = DayStats();
        }
    }

    //If we are configured to send a periodic report (like for stress testing),
    //see if it is time to send.
    if(m_MicroReportMs)
    {
        if(frameMs > m_MicroReportTimer)
        {
            ReportStats();
            m_DayStats = DayStats();
            m_MicroReportTimer = (unsigned)m_MicroReportMs;
        }
        else
        {
            m_MicroReportTimer -= frameMs;
        }
    }
}

void
rlUploadProxy::PrintRequestStats(char* buf, 
                                 const unsigned sizeofBuf,
                                 const char* label,
                                 RequestStats* r)
{
    Assert(buf && sizeofBuf && label && r);

    formatf(buf, sizeofBuf, "%s   num=%u", label, r->m_Size.GetNumSamples());

    char catBuf[256];

    if(r->m_Size.GetNumSamples())
    {
        for(int i = 0; i < rlUpload::NUM_RESULT_CODES; i++)
        {
            if(r->m_NumResults[i])
            {
                formatf(catBuf, sizeof(catBuf), 
                        " r%d=%0.0f%%(%u)",
                        i, ((float)r->m_NumResults[i]/r->m_Size.GetNumSamples()) * 100.0f, r->m_NumResults[i]);

                safecat(buf, catBuf, sizeofBuf);
            }
        }
    }

    formatf(catBuf, sizeof(catBuf), " avgSize=%u  maxSize=%u  minMs=%u  maxMs=%u  avgMs=%u",
            unsigned(r->m_Size.GetAvg()),
            r->m_Size.GetMax(),
            r->m_ResponseMs.GetMin(),
            r->m_ResponseMs.GetMax(),
            (unsigned)r->m_ResponseMs.GetAvg());

    safecat(buf, catBuf, sizeofBuf);
}

void
rlUploadProxy::ReportStats()
{
    char buf[1024];
    memset(buf, 0, sizeof(buf));
    
    char reportBuf[10000];
    memset(reportBuf, 0, sizeof(reportBuf));

#define REPORTCAT(text) \
    rlupDebug2(("%s", text)); \
    safecat(reportBuf, text, sizeof(reportBuf)); \
    safecat(reportBuf, "\n", sizeof(reportBuf));

    float secs = m_UptimeMs/1000.0f;
    formatf(buf, sizeof(buf), "UPTIME: %02uD %02uH %02uM %02.0fS", 
            unsigned(secs/(24*60*60)),
            unsigned(secs/(60*60))%24,
            unsigned(secs/60)%60,
            fmodf(secs, 60.0f));
    REPORTCAT(buf);

    //--------------------------------------------------------------------------
    //Per-request stats
    //--------------------------------------------------------------------------
    formatf(buf, sizeof(buf), "-------- STATS BY REQUEST TYPE ---------");
    REPORTCAT(buf);

    RequestStats* reqStats = 0;
#define REPORT_REQUEST_STATS(label, type)\
    reqStats = &m_DayStats.m_RequestStats[ REQUEST_##type ]; \
    PrintRequestStats(buf, sizeof(buf), label, reqStats); \
    REPORTCAT(buf);

    REPORT_REQUEST_STATS("GetTableSize      ", GET_TABLE_SIZE);
    REPORT_REQUEST_STATS("GetNumOwned       ", GET_NUM_OWNED);
    REPORT_REQUEST_STATS("ReadRecordsByRank ", READ_RECORDS_BY_RANK);
    REPORT_REQUEST_STATS("ReadRecordsByGamer", READ_RECORDS_BY_GAMER);
    REPORT_REQUEST_STATS("ReadRandomRecord  ", READ_RANDOM_RECORD);
    REPORT_REQUEST_STATS("ReadRecord        ", READ_RECORD);
    REPORT_REQUEST_STATS("RateRecord        ", RATE_RECORD);
    REPORT_REQUEST_STATS("DeleteRecord      ", DELETE_RECORD);
    REPORT_REQUEST_STATS("UploadFile        ", UPLOAD_FILE);
    REPORT_REQUEST_STATS("OverwriteFile     ", OVERWRITE_FILE);
    REPORT_REQUEST_STATS("DownloadFile      ", DOWNLOAD_FILE);
    REPORT_REQUEST_STATS("OverwriteUserData ", OVERWRITE_USER_DATA);

#undef REPORT_REQUEST_STATS

    //--------------------------------------------------------------------------
    //Per-reply stats
    //--------------------------------------------------------------------------
    formatf(buf, sizeof(buf), "-------- STATS BY REPLY TYPE ---------");
    REPORTCAT(buf);

    ReplyStats* replyStats = 0;
#define REPORT_REPLY_STATS(label, type)\
    replyStats = &m_DayStats.m_ReplyStats[ REQUEST_##type ]; \
    formatf(buf, sizeof(buf), "%s   num=%u  avgSize=%u  maxSize=%u", \
            label, \
            replyStats->m_Size.GetNumSamples(), \
            unsigned(replyStats->m_Size.GetAvg()), \
            replyStats->m_Size.GetMax()); \
    REPORTCAT(buf);

    REPORT_REPLY_STATS("GetTableSize      ", GET_TABLE_SIZE);
    REPORT_REPLY_STATS("GetNumOwned       ", GET_NUM_OWNED);
    REPORT_REPLY_STATS("ReadRecordsByRank ", READ_RECORDS_BY_RANK);
    REPORT_REPLY_STATS("ReadRecordsByGamer", READ_RECORDS_BY_GAMER);
    REPORT_REPLY_STATS("ReadRandomRecord  ", READ_RANDOM_RECORD);
    REPORT_REPLY_STATS("ReadRecord        ", READ_RECORD);
    REPORT_REPLY_STATS("RateRecord        ", RATE_RECORD);
    REPORT_REPLY_STATS("DeleteRecord      ", DELETE_RECORD);
    REPORT_REPLY_STATS("UploadFile        ", UPLOAD_FILE);
    REPORT_REPLY_STATS("OverwriteFile     ", OVERWRITE_FILE);
    REPORT_REPLY_STATS("DownloadFile      ", DOWNLOAD_FILE);
    REPORT_REPLY_STATS("OverwriteUserData ", OVERWRITE_USER_DATA);

#undef REPORT_REPLY_STATS

    //--------------------------------------------------------------------------
    //Per-hour stats
    //--------------------------------------------------------------------------
    formatf(buf, sizeof(buf), "-------- HOURLY STATS ---------");
    REPORTCAT(buf);

    for(unsigned i = 0; i < 24; i++)
    {
        HourStats& hs = m_DayStats.m_HourStats[i];

        if(hs.m_NumRequests || hs.m_HeapUsed.GetMax())
        {
            formatf(buf, sizeof(buf), "%02u00 - %02u00:", i, i+1);
            REPORTCAT(buf);

#define PRINT_STAT(label, statName) \
            formatf(buf, sizeof(buf), "    %s  min=%u  max=%u  avg=%0.1f", \
                    label, (unsigned)hs.m_##statName .GetMin(), (unsigned)hs.m_##statName .GetMax(), hs.m_##statName .GetAvg()); \
            REPORTCAT(buf);

            PRINT_STAT("HeapUsed         ", HeapUsed);
            PRINT_STAT("FrameMs          ", FrameMs);
            PRINT_STAT("NumNewConnections", NumNewConnections);
            PRINT_STAT("NumConcurrent    ", NumConcurrent);
            PRINT_STAT("IncomingKBps     ", IncomingKBps);
            PRINT_STAT("OutgoingKBps     ", OutgoingKBps);
            PRINT_STAT("ResponseMs       ", ResponseMs);

            formatf(buf, sizeof(buf), "    NumRequests        %u", hs.m_NumRequests);
            REPORTCAT(buf);
        }
    }

    unsigned emailStartTime = sysTimer::GetSystemMsTime();

    SendAdminEmail("Daily stats report", reportBuf);

    m_EmailFixupMs = sysTimer::GetSystemMsTime() - emailStartTime;
}

#endif

