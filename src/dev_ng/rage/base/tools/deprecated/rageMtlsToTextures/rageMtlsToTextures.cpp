// 
// /rageMtlsToTextures.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
//
#include "atl/string.h"
#include "file/asset.h"
#include "file/device.h"
#include "init/modulesettings.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "system/main.h"
#include "system/param.h"

#include "rageShaderMaterial/rageShaderMaterialTemplate.h"
#include "rageShaderMaterial/rageShaderMaterial.h"

#include "rageTextureGEOLib/rageTextureGEOLib.h"

// #include "shaderMaterial/shaderMaterialGeoParamValueBool.h"
// #include "shaderMaterial/shaderMaterialGeoParamValueFloat.h"
#include "shaderMaterial/shaderMaterialGeoParamValueInt.h"
#include "shaderMaterial/shaderMaterialGeoParamValueMatrix34.h"
#include "shaderMaterial/shaderMaterialGeoParamValueMatrix44.h"
#include "shaderMaterial/shaderMaterialGeoParamValueTexture.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector2.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector3.h"
#include "shaderMaterial/shaderMaterialGeoParamValueVector4.h"
#include "shaderMaterial/shaderMaterialGeoParamValueString.h"

#include <io.h>
#include <string>
#include <sstream>
#include <vector>
#include <map>

#pragma warning(push)
#pragma warning(disable:4668)
#include <windows.h>
#pragma warning(pop)
#pragma warning(disable:4702)

#define BAIL_WITH_ERROR_CODE(c)	\
{ \
	Shutdown();	\
	return c; \
} \


using namespace std;
using namespace rage;

//
// Example command lines:
//
//rageMtlsToTextures.exe -depfile="T:\rage\assets\rageland\ragewood\terrain\rageMaterials.dep" -batchout="C:\DOCUME~1\krose\LOCALS~1\Temp\fish.bat"
//rageMtlsToTextures.exe -mtlfile="C:\soft\rage\base\tools\rageMtlsToTextures\AutomatedTests\source\rage_diffuse_diffuse.mtl" -batchout="C:\DOCUME~1\krose\LOCALS~1\Temp\fish.bat"
//

//-----------------------------------------------------------------------------

PARAM(batchout,		"[rageMtlsToTextures] Output texture conversion batch file path");
PARAM(depfile,		"[rageMtlsToTextures] Input material dependency file path");
PARAM(mtlfile,		"[rageMtlsToTextures] Input material file path");
PARAM(platform,		"[rageMtlsToTextures] Platform to output conversion settings for");
PARAM(texout,		"[rageMtlsToTextures] Output path for converted textures");
PARAM(typepath,		"[rageMtlsToTextures] Path to where the TextureOptions geo type file is located");
PARAM(templatepath,	"[rageMtlsToTextures] Path to where the BaseTextureOptions geo template file is located");

//-----------------------------------------------------------------------------

struct RSMTextureData
{
	string	m_TexturePath;
	string	m_TextureFormat;
	int		m_MipCount;
};
typedef vector< RSMTextureData > RSMTextureDataList;

//-----------------------------------------------------------------------------

struct RSMFileData
{
	~RSMFileData()
	{
		for(vector<rageShaderMaterial*>::iterator it = m_rageShaderMaterials.begin();
			it != m_rageShaderMaterials.end();
			++it)
		{
			if((*it)) 
				delete (*it);
		}
	}

	string							m_outputFilePath;
	vector<rageShaderMaterial*>		m_rageShaderMaterials;
};

//-----------------------------------------------------------------------------

void PrintUsage()
{
	Displayf("Usage : rageMtlsToTextures [-platform (platform string)] [-depfile (path)] [-mtlfile (path)] [-texout (path)] -batchout (path)");
	Displayf("");
	Displayf("If a material dependency file is used, it must be of the form:");
	Displayf("");
	Displayf("------------------------------------------------------------------");
	Displayf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	Displayf("<MtlDependencies OutputPath=\"t:/rage/assets/GfsPopulate_MaterialBasedShaders/\">");
	Displayf("\t<MtlFile FilePath=\"t:/rage/art/testdata/skinned characters/GfsPopulate/materials/rage_bump_spec_gfs_eyedarkcol.mtl\">");
	Displayf("\t\t<Textures>");
	Displayf("\t\t\t<Texture ShaderParam=\"DiffuseTex\">T:/rage/art/TestData/Skinned Characters/GfsPopulate/dependences/Gfs_EyeDarkCol.tif</Texture>");
	Displayf("\t\t\t<Texture ShaderParam=\"BumpTex\">t:/rage/art/testdata/skinned characters/GfsPopulate/dependences/Gfs_NeutralNorm.tif</Texture>");
	Displayf("\t\t\t<Texture ShaderParam=\"SpecularTex\">t:/rage/art/testdata/skinned characters/GfsPopulate/dependences/Gfs_NeutralSpec.tif</Texture>");
	Displayf("\t\t</Textures>");
	Displayf("\t</MtlFile>");
	Displayf("\t<MtlFile FilePath=\"t:/rage/art/testdata/skinned characters/GfsPopulate/materials/rage_bump_spec_gfs_scarf1col.mtl\">");
	Displayf("\t\t<Textures>");
	Displayf("\t\t\t<Texture ShaderParam=\"DiffuseTex\">T:/rage/art/TestData/Skinned Characters/GfsPopulate/dependences/Gfs_Scarf1Col.tif</Texture>");
	Displayf("\t\t\t<Texture ShaderParam=\"BumpTex\">t:/rage/art/testdata/skinned characters/GfsPopulate/dependences/Gfs_NeutralNorm.tif</Texture>");
	Displayf("\t\t\t<Texture ShaderParam=\"SpecularTex\">t:/rage/art/testdata/skinned characters/gfspopulate/dependences/gfs_scarf1spec.tif</Texture>");
	Displayf("\t\t</Textures>");
	Displayf("\t</MtlFile>");
	Displayf("</MtlDependencies>");
	Displayf("------------------------------------------------------------------");
	Displayf("");
}

//-----------------------------------------------------------------------------
//GetTextureSamplerParamVec4Value searches through the vector4 parameters in the material looking for a parameter
//that matches the pattern "(texture sampler name)(suffix)" and returns the value.  This is used for retrieving shader
//parameter values for the HDR system which use this setup for supplying the HDR texture exponent and offset values.
bool GetTextureSamplerParamVec4Value(rageShaderMaterial& mtl, const char* samplerName, const char* paramSuffix, Vector4& retVal)
{
	for(int i=0; i<mtl.GetParamCount(); i++)
	{
		shaderMaterialGeoParamValue* pParam = mtl.GetParam(i);
		if(pParam->GetType() == grcEffect::VT_VECTOR4)
		{
			const char* paramName = pParam->GetName();
			int samplerNameLen = strlen(samplerName);
			
			if(strncmp(paramName, samplerName, samplerNameLen) == 0)
			{
				if(strcmp(&paramName[samplerNameLen], paramSuffix) == 0)
				{
					retVal = ((shaderMaterialGeoParamValueVector4*)pParam)->GetValue();
					return true;
				}
			}
		}
	}
	return false;
}

//-----------------------------------------------------------------------------

int ParseRSMDependencyFile(const char* depFilePath, bool bAllowTextureOverrides, RSMFileData& outDepFileData)
{
	//Load and parse the material dependency XML file
	parTree* pDepXMLFileTree = PARSER.LoadTree(depFilePath, "");
	if(!pDepXMLFileTree)
	{
		//Failed to load
		Errorf("Unable to create xml parser object from file \"%s\"", depFilePath);
		delete pDepXMLFileTree;
		return -1;
	}

	//Get the root node of the dependency tree
	const parTreeNode* pDepXMLRoot = pDepXMLFileTree->GetRoot();
	if(!pDepXMLRoot)
	{
		//Root node isn't the correct type, maybe not an material dependency file?
		Errorf("Root node note found in file \"%s\"", depFilePath);
		delete pDepXMLFileTree;
		return -1;
	}

	// Root node should be a MtlDependencies node, so make sure it is
	const parElement& rootElement = pDepXMLRoot->GetElement();
	if(strcmp(rootElement.GetName(), "MtlDependencies") != 0)
	{
		// Root node is of wrong type
		Errorf("The root node of \"%s\" needs to be of type \"MtlDependencies\"", depFilePath);
		delete pDepXMLFileTree;
		return -1;
	}

	// Got element, now try and get output path from it
	const parAttribute* pOutputPathAttribute = rootElement.FindAttribute("OutputPath");
	if(!pOutputPathAttribute)
	{
		// Unable to find output path on root node
		Errorf("Unable to find output path on root node in \"%s\"", depFilePath);
		delete pDepXMLFileTree;
		return -1;
	}

	// Get value for output path
	outDepFileData.m_outputFilePath = pOutputPathAttribute->GetStringValue();
	char lastChar = *(outDepFileData.m_outputFilePath.end() - 1);
	if( (lastChar != '\\') && (lastChar != '/') )
		outDepFileData.m_outputFilePath += '/';

	parTreeNode::ChildNodeIterator cIter = pDepXMLRoot->BeginChildren();
	while(*cIter)
	{
		parTreeNode *pMtlFileNode = (*cIter);

		// Only consider nodes of the tag "MtlFile"
		if(strcmp(pMtlFileNode->GetElement().GetName(), "MtlFile") != 0)
		{
			// Not a MtlFile tag, so skip it
			++cIter;
			continue;
		}

		// Get the path to the material file
		const parAttribute* pMtlFilePathAttr = pMtlFileNode->GetElement().FindAttribute("FilePath");
		if(!pMtlFilePathAttr)
		{
			Errorf("MtlFile node doesn't have a 'FilePath' attribute in file \"%s\"", depFilePath);
			delete pDepXMLFileTree;
			return -1;
		}
		const char* pMtlFilePath = pMtlFilePathAttr->GetStringValue();

		// Load the material file
		rageShaderMaterial* pShaderMaterial = new rageShaderMaterial();
		bool bLoadRes = pShaderMaterial->LoadFromMTL(pMtlFilePath);
		if(!bLoadRes)
		{
			Errorf("Unable to load mtl file \"%s\", skipping", pMtlFilePath);
			delete pShaderMaterial;
			++cIter;
			continue;
		}

		// Override any textures that need it
		if(bAllowTextureOverrides)
		{
			const parTreeNode* pTexturesNode = pMtlFileNode->FindChildWithName("Textures");
			if(pTexturesNode != NULL)
			{
				parTreeNode::ChildNodeIterator texIter = pTexturesNode->BeginChildren();
				while(*texIter)
				{
					const parTreeNode* pTextureNode = (*texIter);
					
					// Get Param name
					const parAttribute* pShaderParamAttr = pTextureNode->GetElement().FindAttribute("ShaderParam");
					if(!pShaderParamAttr)
					{
						// Unable to find output path on root node
						Errorf("Unable to find shader param on Texture node in \"%s\"", depFilePath);
						delete pDepXMLFileTree;
						return -1;
					}

					// Get value for output path
					const char* pcShaderParamName = pShaderParamAttr->GetStringValue();
					const char* pcTextureFilename = pTextureNode->GetData();
	
					// Override material param's value
					shaderMaterialGeoParamValueTexture* pTextureParam = (shaderMaterialGeoParamValueTexture*)pShaderMaterial->GetParam(pcShaderParamName);
					if(pTextureParam)
					{
						pTextureParam->SetValue(pcTextureFilename);
					}
					
					++texIter;
				}
			}
		}

		outDepFileData.m_rageShaderMaterials.push_back(pShaderMaterial);
		++cIter;

	}//while(*cIter)

	delete pDepXMLFileTree;
	return (int)outDepFileData.m_rageShaderMaterials.size();
}

//-----------------------------------------------------------------------------

inline void FlipBackSlashesToForwardSlashes(string& st)
{
	int len = (int)st.size();
	for(int i=0; i<len;i++)
	{
		if(st[i] == '\\') 
			st[i] = '/';
	}
}

//-----------------------------------------------------------------------------

void ExtractTextureDataFromRSM(rageShaderMaterial* pMaterial, const char* platform, RSMTextureDataList& outTextureData)
{
	// Iterate over all the textures in the material file
	int paramCount = pMaterial->GetParamCount();
	for(int paramIdx=0; paramIdx<paramCount; paramIdx++)
	{
		shaderMaterialGeoParamValue* pParam = pMaterial->GetParam(paramIdx);
		if(pParam->GetType() == grcEffect::VT_TEXTURE)
		{
			shaderMaterialGeoParamValueTexture *pTextureParam = static_cast<shaderMaterialGeoParamValueTexture*>(pParam);

			string fullTexturePath = pTextureParam->GetValue();
			if( fullTexturePath.size() &&
				(strcmpi(fullTexturePath.c_str(), "none") != 0) &&
				(strcmpi(fullTexturePath.c_str(), "null") != 0) )
			{
				FlipBackSlashesToForwardSlashes(fullTexturePath);

				//Get the mip count set in the material
				int mipCount = pTextureParam->GetParamDescription()->GetNoOfMipLevels();

				//Get the output texture format to use
				string exportFormat = "DXT5";

				//Get the format string specified in the mtl xml
				const char* origMtlFmtString = pTextureParam->GetParamDescription()->GetTextureOutputFormats();
				int origMtlFmtStringLen = strlen(origMtlFmtString);

				//Remove any whitespace from the format string
				string mtlFmtString;
				mtlFmtString.reserve(origMtlFmtStringLen);

				for(int c=0; c<origMtlFmtStringLen; c++)
				{
					if(origMtlFmtString[c] != ' ')
						mtlFmtString += origMtlFmtString[c];
				}

				//Does the material specify differing formats for each platform?
				if( platform && (mtlFmtString.find(platform) != string::npos) )
				{
					int iPosOfPlatform = mtlFmtString.find(platform);
					int iEqualSignPos = mtlFmtString.find_first_of("=", iPosOfPlatform);
					int iSemiColonPos = mtlFmtString.find_first_of(";", iEqualSignPos);
					if(iSemiColonPos == string::npos) 
						iSemiColonPos = mtlFmtString.length();

					// Get export format
					exportFormat = mtlFmtString.substr(iEqualSignPos + 1, (iSemiColonPos - iEqualSignPos - 1));
				}
				else
				{
					//Just grab the first available format specification
					int iEqualSignPos = mtlFmtString.find_first_of("=", 0);
					if(iEqualSignPos == string::npos) 
						iEqualSignPos = 0;
					int iSemiColonPos = mtlFmtString.find_first_of(";", iEqualSignPos);
					if(iSemiColonPos == string::npos) 
						iSemiColonPos = mtlFmtString.length();
					
					if(iEqualSignPos == 0) iEqualSignPos = -1;

					// Get export format
					exportFormat = mtlFmtString.substr(iEqualSignPos + 1, (iSemiColonPos - iEqualSignPos - 1));
				}

				//Store the information we've gathered
				RSMTextureData tData;
				tData.m_TexturePath = fullTexturePath;
				tData.m_TextureFormat = exportFormat;
				tData.m_MipCount = mipCount;
				outTextureData.push_back(tData);
			}
		}//End if(pParam->GetType() == grcEffect::VT_TEXTURE)
	}//End for(int paramIdx=0...
}

//-----------------------------------------------------------------------------

const GeoTexturePlatformOptions* PlatformStringToPlatformOptions(GeoTextureOptions* pGEOData, const char* platform)
{
	if(strcmpi(platform, PLATFORM_PC) == 0)
		return &pGEOData->GetPCOptions();
	else if(strcmpi(platform, PLATFORM_XBOX360) == 0)
		return &pGEOData->GetXBox360Options();
	else if(strcmpi(platform, PLATFORM_PS3) == 0)
		return &pGEOData->GetPS3Options();
	else 
		return NULL;
}

//-----------------------------------------------------------------------------

string BuildExposureTextureList(const GeoTextureOptions* pMasterOptions, const char* masterTexturePath)
{
	stringstream etList;

	char optionPathBuf[MAX_PATH];

	etList << "\"" << masterTexturePath << "\"" << "," << pMasterOptions->GetExposure() << ",";

	int nExposureTextures = pMasterOptions->GetNumExposureTextures();
	for(int texIdx=0; texIdx < nExposureTextures; texIdx++)
	{
		memset(optionPathBuf, 0, MAX_PATH);
		const char* expTexPath = pMasterOptions->GetExposureTexture(texIdx);
		if(!strlen(expTexPath))
			continue;

		//Get the exposure for the texture
		float exposure;

		TEXTUREOPTIONS.TexturePathToOptionsFilePath(optionPathBuf, MAX_PATH, expTexPath);
		if(GetFileAttributes(optionPathBuf) == 0xFFFFFFFF)
		{
			Errorf("Texture option file \"%s\" does not exist, defaulting exposure value for the texture \"%s\" to 0.0",
				optionPathBuf, expTexPath);
			exposure = 0.0f;
		}
		else
		{
			GeoTextureOptions* pExpTexOptions = TEXTUREOPTIONS.LoadOptionsFile(optionPathBuf);
			if(!pExpTexOptions)
			{
				Errorf("Texture option file \"%s\" exists, but failed to load, defaulting exposure value for the texture \"%s\" to 0.0",
					optionPathBuf, expTexPath);
				exposure = 0.0f;
			}
			else
			{
				exposure = pExpTexOptions->GetExposure();
			}
		}
		etList << "\"" << expTexPath << "\"" << "," << exposure;
		if(texIdx != nExposureTextures-1)
			etList << ",";
	}

	return etList.str();
}


//-----------------------------------------------------------------------------

string CreateCmdLineOptionString(const char* inputTexturePath, 
								 const char* outputTexturePath,
								 const char* platform,
								 RSMTextureData* pRSMData, 
								 GeoTextureOptions* pTextureOptions)
{
	bool bIncludeInputTexturePath = true;
	stringstream cmdOptions;

	//Get the platform specific texture options from the GEO Data
	const GeoTexturePlatformOptions* pPlatformOptions = NULL;
	if( pTextureOptions )
	{
		if(platform &&  (strlen(platform) != 0))
			pPlatformOptions = PlatformStringToPlatformOptions(pTextureOptions, platform);
		else
			pPlatformOptions = &pTextureOptions->GetPCOptions();
		
		Assert(pPlatformOptions);
	}
	
	//Texture output format...
	if( pRSMData )
	{
		//If there is RSM data then always take the output format from it, since the shader 
		//author should set the format expected in shader.
		cmdOptions << "-f " << pRSMData->m_TextureFormat << " ";
	}
	else if( pTextureOptions )
	{
		//There isn't any RSM data, but there is GEO data so try to grab the output format from it
		cmdOptions << "-f " << pPlatformOptions->GetFormat() << " ";
	}
	else
	{
		//No RSM data, and no GEO Data, so just default to DXT5
		cmdOptions << "-f DXT5 ";
	}

	//Mip levels...
	if( pTextureOptions )
	{
		//Only include this flag if user created mip-maps have not been specified
		if(pTextureOptions->GetNumMipTextures() == 0)
			cmdOptions << "-miplevels " << pPlatformOptions->GetMipLevels() << " ";
	}
	else if( pRSMData )
	{
		cmdOptions << "-miplevels " << pRSMData->m_MipCount << " ";
	}
	else
	{
		cmdOptions << "-miplevels 0 ";
	}

	
	//Retrieve any options that are stored only in the texture options files...
	if( pTextureOptions )
	{
		//Maximum width
		if( pTextureOptions->GetMaxWidth() )
			cmdOptions << "-maxwidth " << pTextureOptions->GetMaxWidth() << " ";

		//Maximum height
		if( pTextureOptions->GetMaxHeight() )
			cmdOptions << "-maxheight " << pTextureOptions->GetMaxHeight() << " ";

		//Mip Sharpening
		if( pTextureOptions->GetSharpenMipMaps() )
		{
			cmdOptions << "-mipsharp ";

			//Mip-Map Sharpening Filter
			GeoTextureOptionsType::MipSharpenFilter sharpFilter = pTextureOptions->GetSharpenMipFilter();
			cmdOptions << "-mipsharpfilter ";
			switch(sharpFilter)
			{
			case GeoTextureOptionsType::MIPSHARP_SOFT:
				cmdOptions << "soft ";
				break;
			case GeoTextureOptionsType::MIPSHARP_MEDIUM:
				cmdOptions << "medium ";
				break;
			case GeoTextureOptionsType::MIPSHARP_HARD:
				cmdOptions << "hard ";
				break;
			}
		}

		//SetSRGBGamma Flag
		if( pTextureOptions->GetSetSRGBGamma() )
			cmdOptions << "-srgbgamma ";

		//User created mip maps
		int nMips = pTextureOptions->GetNumMipTextures();
		if(nMips)
		{
			cmdOptions << "-mips " << "\"" << inputTexturePath << "\",";
			for(int i=0; i<nMips; i++)
			{
				cmdOptions << "\"" << pTextureOptions->GetMipTexture(i) << "\"";
				if(i != nMips-1)
					cmdOptions << ",";
				else
					cmdOptions << " ";
			}
			bIncludeInputTexturePath = false;
		}

		//Cube maps
		if ( pTextureOptions->HasCubeMapTextures() )
		{
			cmdOptions << "-cubefaces ";

			for(int i=0; i<6; i++)
			{
				const char* pCubeMapTexture = pTextureOptions->GetCubeMapTexture( (GeoTextureOptions::CubeMapFace)i);
				if( (pCubeMapTexture == NULL) || (strlen(pCubeMapTexture) == 0) )
				{
					Errorf("Not all 6 Cube-Map face textures have been specified in the GEO options file for the texture \"%s\"", inputTexturePath);
					return "";
				}

				cmdOptions << "\"" << pCubeMapTexture << "\"";
				if(i != 5)
					cmdOptions << ",";
				else
					cmdOptions << " ";
			}
			bIncludeInputTexturePath = false;
		}

		//HDR Settings
		if( pTextureOptions->GetNumExposureTextures() )
		{
			string expTexList = BuildExposureTextureList( pTextureOptions, inputTexturePath );
			if(expTexList.size())
			{
				cmdOptions << "-etlist " << expTexList << " ";
				bIncludeInputTexturePath = false;
			}
		}
		else
		{
			if(pTextureOptions->GetCalcHDRScaleAndBias())
			{
				cmdOptions << "-hdrc ";
			}

			if(pTextureOptions->GetExposure() != 0)
			{
				cmdOptions << "-e " << pTextureOptions->GetExposure() << " ";
			}
		}
	}//End if( pTextureOptions )

	
	if( !pTextureOptions && pRSMData )
	{
		//Set the flag to compute the HDR offset and scale if the format has been specified to be an HDR format
		if( (strcmpi(pRSMData->m_TextureFormat.c_str(), "HDR_DXT1") == 0) ||
			(strcmpi(pRSMData->m_TextureFormat.c_str(), "HDR_DXT5") == 0) )
		{
			cmdOptions << "-hdrc ";
		}
	}

	if(bIncludeInputTexturePath)
		cmdOptions << "-in \"" << inputTexturePath << "\" " << "-out \"" << outputTexturePath << "\"";
	else
		cmdOptions << "-out \"" << outputTexturePath << "\"";

	return cmdOptions.str();
}

//-----------------------------------------------------------------------------

bool Init(bool& outUseGEOs)
{
	INIT_PARSER;

	INIT_TEXTUREOPTIONS;

	char texOptionsTemplatePath[MAX_PATH];
	memset(texOptionsTemplatePath, 0, MAX_PATH);
	char texOptionsTypePath[MAX_PATH];
	memset(texOptionsTypePath, 0, MAX_PATH);

	atString baseGEOPath;
	if(initModuleSettings::GetModuleSetting("BaseGEOPath", baseGEOPath))
	{
		if( (!baseGEOPath.EndsWith("/")) &&
			(!baseGEOPath.EndsWith("\\")) )
			baseGEOPath += "/";

		sprintf(texOptionsTemplatePath, "%sGEOTemplates", (const char*)baseGEOPath);
		sprintf(texOptionsTypePath, "%sGEOTypes", (const char*)baseGEOPath);
	}

	const char* cmdLineTemplatePath = NULL;
	if(PARAM_templatepath.Get(cmdLineTemplatePath))
	{
		if(strlen(cmdLineTemplatePath) != 0)
		{
			strcpy(texOptionsTemplatePath, cmdLineTemplatePath);
			Displayf("Using cmd line template path \"%s\"", texOptionsTemplatePath);
		}
	}

	const char* cmdLineTypePath = NULL;
	if(PARAM_typepath.Get(cmdLineTypePath))
	{
		if(strlen(cmdLineTypePath) != 0)
		{
			strcpy(texOptionsTypePath, cmdLineTypePath);
			Displayf("Using cmd line type path \"%s\"", texOptionsTypePath);
		}
	}

	if( strlen(texOptionsTemplatePath) && strlen(texOptionsTypePath) )
	{
		TEXTUREOPTIONS.SetTextureGeoTemplateDirectory(texOptionsTemplatePath);
		TEXTUREOPTIONS.SetTextureGeoTypeDirectory(texOptionsTypePath);
		outUseGEOs = true;
	}
	else
		outUseGEOs = false;
	
	rageShaderMaterialTemplate::InitShaderMaterialLibrary();

	return true;
}

//-----------------------------------------------------------------------------

void Shutdown(void)
{
	rageShaderMaterialTemplate::ShutdownShaderMaterialLibrary();
	SHUTDOWN_TEXTUREOPTIONS;
	SHUTDOWN_PARSER;
}

//-----------------------------------------------------------------------------

int Main(void)
{
	bool bUseGEOs = false;

	if(!Init(bUseGEOs))
	{
		Errorf("Application initialization failed, execution aborted.");
		return -1;
	}

	if(bUseGEOs)
		Displayf("Texture option GEOs enabled.");
	else
		Displayf("Texture option GEOs disabled.");

	RSMFileData rsmFileData;

	//Check to see if a material dependency file was specified
	const char* depFilePath = NULL;
	if(PARAM_depfile.Get(depFilePath))
	{
		if(!strlen(depFilePath))
		{
			Errorf("-depfile cmd line argument specfied, but no path was supplied, execution aborted.");
			PrintUsage();
			BAIL_WITH_ERROR_CODE(-1);
		}

		if(GetFileAttributes(depFilePath) == 0xFFFFFFFF)
		{
			Errorf("Specified material dependency file \"%s\" does not exist, execution aborted.");
			BAIL_WITH_ERROR_CODE(-1);
		}

		int mtlCount = ParseRSMDependencyFile(depFilePath, true, rsmFileData);
		if(mtlCount == 0)
		{
			Errorf("Material dependency file \"%s\" contains no material references, execution aborted.", depFilePath);
			BAIL_WITH_ERROR_CODE(-1);
		}
		else if (mtlCount == -1)
		{
			Errorf("An error occured while parsing the material dependency file \"%s\", execution aborted.", depFilePath);
			BAIL_WITH_ERROR_CODE(-1);
		}
	}

	//Check to see if an individual material file was specified
	const char* mtlFilePath = NULL;
	if(PARAM_mtlfile.Get(mtlFilePath))
	{
		if(!strlen(mtlFilePath))
		{
			Errorf("-mtlfile cmd line argument specfied, but no path was supplied, execution aborted.");
			PrintUsage();
			BAIL_WITH_ERROR_CODE(-1);
		}

		if(GetFileAttributes(mtlFilePath) == 0xFFFFFFFF)
		{
			Errorf("Material specified for the -mtlfile argument \"%s\" does not exist, execution aborted.", mtlFilePath);
			BAIL_WITH_ERROR_CODE(-1);
		}

		rageShaderMaterial* pShaderMaterial = new rageShaderMaterial();
		bool bLoadRes = pShaderMaterial->LoadFromMTL(mtlFilePath);
		if(!bLoadRes)
		{
			Errorf("Failed to load material \"%s\", execution aborted.", mtlFilePath);
			delete pShaderMaterial;
			BAIL_WITH_ERROR_CODE(-1);
		}
		else
		{
			rsmFileData.m_rageShaderMaterials.push_back(pShaderMaterial);
		}
	}
	
	//Get the output path for the textures, if specified on the command line
	const char* texOutPath = NULL;
	if(PARAM_texout.Get(texOutPath))
	{
		if(!strlen(texOutPath))
		{
			Errorf("-texout cmd line argument specfied, but no path was supplied, execution aborted.");
			PrintUsage();
			BAIL_WITH_ERROR_CODE(-1);
		}
		rsmFileData.m_outputFilePath = texOutPath;
	}

	//Get the output platform
	const char* platform = "PC";
	PARAM_platform.Get(platform);

	//Get the ouptpt path for the resulting batch file
	const char* batchOutPath = NULL;
	if(!PARAM_batchout.Get(batchOutPath))
	{
		Errorf("Must specify the batch file output path using the -batchout cmd line argument, execution aborted.");
		PrintUsage();
		BAIL_WITH_ERROR_CODE(-1);
	}

	//Open the output batch file
	FILE* pOutputBatFile = fopen(batchOutPath,"w");
	if (!pOutputBatFile) 
	{
		Errorf("Unable to open output file \"%s\" for write, execution aborted.", batchOutPath);
		BAIL_WITH_ERROR_CODE(-1);
	}

	//Iterate over all the materials and their textures, generating the command line arguments for the rageTextureConverter
	map<string, string>	texOutLookup;
	map<string, string>::iterator toIt;
	bool bWriteConvStr;

	int mtlCount = (int)rsmFileData.m_rageShaderMaterials.size();
	for(int mtlIdx=0; mtlIdx<mtlCount; mtlIdx++)
	{
		rageShaderMaterial *pShaderMtl = rsmFileData.m_rageShaderMaterials[mtlIdx];
		
		RSMTextureDataList texDataList;
		ExtractTextureDataFromRSM(pShaderMtl, platform, texDataList);
		int texCount = (int)texDataList.size();
		for(int texIdx = 0; texIdx < texCount; texIdx++)
		{
			RSMTextureData& texData = texDataList[texIdx];

			string outputTexturePath = rsmFileData.m_outputFilePath;
			outputTexturePath += ASSET.FileName(texData.m_TexturePath.c_str());
			outputTexturePath.erase( outputTexturePath.find_last_of('.') + 1 );
			outputTexturePath += "dds";
			FlipBackSlashesToForwardSlashes(outputTexturePath);

			GeoTextureOptions* pTexOptions = NULL;
			if(bUseGEOs)
			{
				char optionPath[MAX_PATH];
				TEXTUREOPTIONS.TexturePathToOptionsFilePath(optionPath, MAX_PATH, texData.m_TexturePath.c_str());
				
				if(GetFileAttributes(optionPath) != 0xFFFFFFFF)
					pTexOptions = TEXTUREOPTIONS.LoadOptionsFile(optionPath);

				if(!pTexOptions)
					pTexOptions = TEXTUREOPTIONS.GetDefaultOptions();
			}

			string cmdLineOptions = CreateCmdLineOptionString(texData.m_TexturePath.c_str(), 
															  outputTexturePath.c_str(), 
															  platform, 
															  &texData, pTexOptions);
			toIt = texOutLookup.find(texData.m_TexturePath);
			if(toIt == texOutLookup.end())
			{
				texOutLookup[texData.m_TexturePath] = cmdLineOptions;
				bWriteConvStr = true;
			}
			else
			{
				if(toIt->second != cmdLineOptions)
				{
					Errorf("The texture \"%s\" has been used multiple times with different export settings.\n\t(1)%s\n\t(2)%s",
						texData.m_TexturePath.c_str(), toIt->second.c_str(), cmdLineOptions.c_str());
				}
				bWriteConvStr = false;
			}
			
			if(bWriteConvStr)
				fprintf(pOutputBatFile, "rageTextureConvert.exe %s \r\n", cmdLineOptions.c_str());
		}
	}

	// Clean up
	fclose(pOutputBatFile);

	BAIL_WITH_ERROR_CODE(0);
}

