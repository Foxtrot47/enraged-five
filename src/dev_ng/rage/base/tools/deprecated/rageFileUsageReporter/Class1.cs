using System;
using System.IO;
using System.Collections;
using rageUsefulCSharpToolClasses;

namespace rageFileUsageReporter
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		//		public const string RAGE_MODULE_PATH_ENVIRONMENT_VARIABLE	= "RAGE_MODULE_PATH";
		public const string DEFAULT_SOURCE_LOCATION					= "t:/pong/assets/";
		public const string FILE_EXTENSION_TO_SEARCH_FOR			= ".sps";
		public static string[] WHITESPACE_CHARS							= {" ", "\t", "\n", "\r", ","};

		static void OutputUsageInfo()
		{
			System.Console.WriteLine("Usage : rageFileUsageReporter [-verbose] [-filesPerTypeFile] [-typeFilesPerFile] [-source <source folder>] [-ext <extension to report on>]");
			System.Console.WriteLine("[-source <source folder>]\tFolder to look for usage in, defaults to {0}", DEFAULT_SOURCE_LOCATION);
			System.Console.WriteLine("[-ext <extension to report on>]\tExtension to report on, defaults to {0}", FILE_EXTENSION_TO_SEARCH_FOR);
		}

		static bool IsWhiteSpace(string strStringToTest)
		{
			for(int i=0; i<strStringToTest.Length; i++)
			{
				string strCharToTest = strStringToTest.Substring(i, 1);
				bool bWhiteSpace = false;
				foreach(string strWhiteSpace in WHITESPACE_CHARS)
				{
					if(strCharToTest == strWhiteSpace)
					{
						bWhiteSpace = true;
						break;
					}
				}
				if(!bWhiteSpace) return false;
			}
			return true;
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static int Main(string[] args)
		{
			string strSourceLocation = DEFAULT_SOURCE_LOCATION;
			string strFileExtensionToSearchFor = FILE_EXTENSION_TO_SEARCH_FOR;
			bool bOutputFoundFilesPerTypeFile = false;
			bool bOutputTypeFilesPerFoundFile = false;
			bool bVerbose = false;
			for(int i=0; i<args.GetLength(0); i++)
			{
				if(args[i] == "-source")
				{
					strSourceLocation = args[++i];
				}
				else if(args[i] == "-ext")
				{
					strFileExtensionToSearchFor = args[++i];
				}
				else if(args[i] == "-verbose")
				{
					bVerbose = true;
				}
				else if(args[i] == "-filesPerTypeFile")
				{
					bOutputFoundFilesPerTypeFile = true;
				}
				else if(args[i] == "-typeFilesPerFile")
				{
					bOutputTypeFilesPerFoundFile = true;
				}
				else
				{
					System.Console.WriteLine("Unknown argument {0}", args[i]);
					OutputUsageInfo();
					return -1;
				}
			}
			if((!bOutputFoundFilesPerTypeFile) && (!bOutputTypeFilesPerFoundFile))
			{
				// Need at least one of those args, or there is nothing to do!
				System.Console.WriteLine("You need to at least supply either -filesPerTypeFile or -typeFilesPerFile, without at least one of those arguments there is nothing to do!");
				OutputUsageInfo();
				return -1;
			}

			// Convert all slashes to nice friendly '/'
			strSourceLocation = strSourceLocation.Replace("\\", "/");

			// Make sure the given paths end in "/"
			if(strSourceLocation[strSourceLocation.Length - 1] != '/') strSourceLocation += "/";
			strFileExtensionToSearchFor = strFileExtensionToSearchFor.ToLower();

			// Get a list of type files
			ArrayList obAStrTypeFiles = rageFileUtilities.GetFileList(strSourceLocation, "*.type");

			// Create structures to record the information
			ArrayList obTypeFiles = new ArrayList();
			ArrayList obFoundFiles = new ArrayList();

			// Got a list of type files, now look for references to the given file extension in them
			foreach(string strTypeFile in obAStrTypeFiles)
			{
				// Create structure to record the information
				FileWithFileReferences obTypeFile = new FileWithFileReferences();
				obTypeFile.Filename = strTypeFile;

				// Open file
				// System.Console.WriteLine("Parsing : "+ strTypeFile);
				StreamReader sr = File.OpenText(strTypeFile);

				// Parse file
				string strInput;
				while ((strInput=sr.ReadLine())!=null) 
				{
					// Does strInput contain anything relavent?
					int iPosOfFileExtension = strInput.ToLower().IndexOf(strFileExtensionToSearchFor);
					if(iPosOfFileExtension != -1)
					{
						// Bingo!  Found an occurance of the extension
						// System.Console.WriteLine("\""+ strInput +"\" contains \""+ strFileExtensionToSearchFor +"\"");

						// Ok, it contains the string I'm looking for, but that means nothing
						// To be a file, the  string needs to have white space after it
						int iEndPosOfFileExtension = iPosOfFileExtension + strFileExtensionToSearchFor.Length;
						bool bEndLooksGood = (iEndPosOfFileExtension >= strInput.Length);
						if(!bEndLooksGood)
						{
							// Hmmmm, there is something after the filename, is it white space?
							string strCharAfterTheFilename = strInput.Substring(iEndPosOfFileExtension, 1);
							// System.Console.WriteLine("strCharAfterTheFilename = \""+ strCharAfterTheFilename +"\"");

							if(IsWhiteSpace(strCharAfterTheFilename))
							{
								// Ok, filename ends in white space, so is valid!  Wohoo!
								// Just clean it up and store it then
								int iStartOfFilename = iPosOfFileExtension;
								while((iStartOfFilename > -1) && (!IsWhiteSpace(strInput.Substring(iStartOfFilename, 1))))
								{
									iStartOfFilename--;
								}
								iStartOfFilename++;
								string strFilename = strInput.Substring(iStartOfFilename, iEndPosOfFileExtension - iStartOfFilename);
								// System.Console.WriteLine("strFilename = \""+ strFilename +"\"");

								// Add found file to type file list
								obTypeFile.AddFileRefrence(strFilename);

								// Add type file to found file list
								// Does the found file already exist in list of found files?
								bool bAddMe = true;
								for(int i=0; i<obFoundFiles.Count; i++)
								{
									if(((FileWithFileReferences)obFoundFiles[i]).Filename == strFilename)
									{
										// Bingo, found existing reference
										((FileWithFileReferences)obFoundFiles[i]).AddFileRefrence(strTypeFile);
										bAddMe = false;
										break;
									}
								}
								if(bAddMe)
								{
									// No existing reference, so add it
									FileWithFileReferences obFoundFile = new FileWithFileReferences();
									obFoundFile.Filename = strFilename;
									obFoundFile.AddFileRefrence(strTypeFile);
									obFoundFiles.Add(obFoundFile);
								}

							}
						}
					}
				}

				// Close file
				sr.Close();

				obTypeFiles.Add(obTypeFile);

			}

			// Output stuff
			// Verbose
			if(bVerbose && bOutputFoundFilesPerTypeFile)
			{
				// Type files
				foreach(FileWithFileReferences obTypeFile in obTypeFiles)
				{
					System.Console.WriteLine(obTypeFile.Filename +" references...");
					foreach(FileReference obFileReference in obTypeFile.GetFileReferences())
					{
						System.Console.WriteLine(obFileReference.Filename +" "+ obFileReference.ReferenceCount +" time"+ ((obFileReference.ReferenceCount > 1) ? "s" : ""));
					}
				}
			}

			if(bVerbose && bOutputTypeFilesPerFoundFile)
			{
				// Found files
				foreach(FileWithFileReferences obFoundFile in obFoundFiles)
				{
					System.Console.WriteLine(obFoundFile.Filename +" appears in...");
					foreach(FileReference obFileReference in obFoundFile.GetFileReferences())
					{
						System.Console.WriteLine(obFileReference.Filename +" "+ obFileReference.ReferenceCount +" time"+ ((obFileReference.ReferenceCount > 1) ? "s" : ""));
					}
				}
			}
		
			// Summary
			if(!bVerbose && bOutputFoundFilesPerTypeFile)
			{
				// Type files
				foreach(FileWithFileReferences obTypeFile in obTypeFiles)
				{
					int iNoOfReferences = 0;
					foreach(FileReference obFileReference in obTypeFile.GetFileReferences())
					{
						iNoOfReferences += obFileReference.ReferenceCount;
					}
					System.Console.WriteLine(obTypeFile.Filename +" contains "+ iNoOfReferences +" reference"+ ((iNoOfReferences > 1) ? "s" : ""));
				}
			}

			if(!bVerbose && bOutputTypeFilesPerFoundFile)
			{
				// Found files
				foreach(FileWithFileReferences obFoundFile in obFoundFiles)
				{
					int iNoOfReferences = 0;
					foreach(FileReference obFileReference in obFoundFile.GetFileReferences())
					{
						iNoOfReferences += obFileReference.ReferenceCount;
					}
					System.Console.WriteLine(obFoundFile.Filename +" is referenced "+ iNoOfReferences +" time"+ ((iNoOfReferences > 1) ? "s" : ""));
				}
			}
		
			return 0;
		}
	}
}
