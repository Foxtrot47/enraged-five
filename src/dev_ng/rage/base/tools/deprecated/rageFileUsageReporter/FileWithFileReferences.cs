using System;
using System.Collections;

namespace rageFileUsageReporter
{
	/// <summary>
	/// Summary description for FileWithFileReferences.
	/// </summary>
	public class FileWithFileReferences
	{
		public FileWithFileReferences()
		{
			m_aobFileReferences = new ArrayList();
		}

		public void AddFileRefrence(string strFileReference)
		{
			// Does the file already exist in my list of files?
			bool bAddMe = true;
			for(int i=0; i<m_aobFileReferences.Count; i++)
			{
				if(((FileReference)m_aobFileReferences[i]).Filename == strFileReference)
				{
					// Bingo, found existing reference
					((FileReference)m_aobFileReferences[i]).ReferenceCount++;
					bAddMe = false;
					break;
				}
			}
			if(bAddMe)
			{
				// No existing reference, so add it
				FileReference obFileReference = new FileReference();
				obFileReference.Filename = strFileReference;
				obFileReference.ReferenceCount = 1;
				m_aobFileReferences.Add(obFileReference);
			}
		}

		public ArrayList	GetFileReferences()
		{
			return m_aobFileReferences;
		}

		private ArrayList	m_aobFileReferences;

		private string		m_strFilename;
		// Declare a Filename property of type string:
		public string Filename
		{
			get 
			{
				return m_strFilename; 
			}
			set 
			{
				m_strFilename = value; 
			}
		}
	}
}
