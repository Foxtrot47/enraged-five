using System;

namespace rageFileUsageReporter
{
	/// <summary>
	/// Summary description for FileReference.
	/// </summary>
	public class FileReference
	{
		public FileReference()
		{
			m_iReferenceCount = 0;
		}

		private string		m_strFilename;
		// Declare a Filename property of type string:
		public string Filename
		{
			get 
			{
				return m_strFilename; 
			}
			set 
			{
				m_strFilename = value; 
			}
		}

		private int		m_iReferenceCount;
		// Declare a ReferenceCount property of type int:
		public int ReferenceCount
		{
			get 
			{
				return m_iReferenceCount; 
			}
			set 
			{
				m_iReferenceCount = value; 
			}
		}
	}
}
