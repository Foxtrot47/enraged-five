using System;
using System.Collections.Generic;
using System.Text;

namespace rageGenerateToolDocs
{
    public class Build
    {
        override public string ToString()
        {
            string strOutputMe = "<Build>\n";
            strOutputMe += ("\t\t\t<BuildName>" + BuildName + "</BuildName>\n");
            strOutputMe += ("\t\t\t<BuildLocation>" + BuildLocation + "</BuildLocation>\n");
            strOutputMe += ("\t\t\t<BuiltRegularly>" + BuiltRegularly + "</BuiltRegularly>\n");
            strOutputMe += ("\t\t\t<ReleaseLocations>\n");
            foreach(string strReleaseLocation in ReleaseLocations)
            {
                strOutputMe += ("\t\t\t\t<ReleaseLocation>"+ strReleaseLocation +"</ReleaseLocation>\n");
            }
            strOutputMe += ("\t\t\t</ReleaseLocations>\n");
            strOutputMe += "</Build>\n";
            return strOutputMe;
        }

        public string ToHtml()
        {
            string strReleaseLocationAsString = null;
            foreach(string strReleaseLocation in ReleaseLocations)
            {
                if (strReleaseLocationAsString == null) strReleaseLocationAsString = strReleaseLocation;
                else strReleaseLocationAsString += ", "+ strReleaseLocation;
            }

            string strOutputMe = "<tr><td><font face=\"Arial\" size=1>" + Class1.Htmlize(BuildName) + "</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(BuildLocation) + "</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(strReleaseLocationAsString) + "</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(BuiltRegularly.ToString()) + "</td></tr>\n";
            return strOutputMe;
        }

        private string m_strBuildName;
        public string BuildName
        {
            get
            {
                return m_strBuildName;
            }
            set
            {
                m_strBuildName = value;

                // Evil hacks for Maya builds
                if(m_strBuildName.Contains(".") && m_strBuildName.EndsWith("Release|Win32"))
                {
                    string strMayaVersionNo = m_strBuildName.Substring(0, 3);
                    float fMayaVersionNo;
                    if (float.TryParse(strMayaVersionNo, out fMayaVersionNo))
                    {
                        // Looks like a valid Maya build, so automatically fake release path
                        ReleaseLocation = "t:/rage/tools/modules/maya/"+ strMayaVersionNo +"/rage/plug-ins";
                        SourceOfReleaseLocation = "BuildName";
                    }
                }
            }
        }
        private string m_strSourceOfBuildName;
        public string SourceOfBuildName
        {
            get
            {
                return m_strSourceOfBuildName;
            }
            set
            {
                m_strSourceOfBuildName = value;
            }
        }

        private string m_strBuildLocation;
        public string BuildLocation
        {
            get
            {
                return m_strBuildLocation;
            }
            set
            {
                m_strBuildLocation = value.Replace("\\", "/").ToLower();
            }
        }
        private string m_strSourceOfBuildLocation;
        public string SourceOfBuildLocation
        {
            get
            {
                return m_strSourceOfBuildLocation;
            }
            set
            {
                m_strSourceOfBuildLocation = value;
            }
        }

        private bool m_bBuiltRegularly;
        public bool BuiltRegularly
        {
            get
            {
                return m_bBuiltRegularly;
            }
            set
            {
                m_bBuiltRegularly = value;
            }
        }
  
        private string m_strSourceOfBuiltRegularly;
        public string SourceOfBuiltRegularly
        {
            get
            {
                return m_strSourceOfBuiltRegularly;
            }
            set
            {
                if ((value == null) || (value == ""))
                {
                    m_strSourceOfBuiltRegularly = null;
                }
                else if ((m_strSourceOfBuiltRegularly == null) || (m_strSourceOfBuiltRegularly == ""))
                {
                    m_strSourceOfBuiltRegularly = value;
                }
                else
                {
                    m_strSourceOfBuiltRegularly += (", "+ value);
                }
            }
        }

        private List<string> m_astrReleaseLocation = new List<string>();
        public List<string> ReleaseLocations
        {
            get
            {
                return m_astrReleaseLocation;
            }
        }
        public string ReleaseLocation
        {
            set
            {
                if ((value == null) || (value == ""))
                {
                    m_astrReleaseLocation.Clear();
                }
                else
                {
                    string strReleaseLocation = value.Replace("\\", "/").ToLower().Replace("%ab_dir%", "t:/rage/").Replace("//", "/");
                    if (strReleaseLocation.EndsWith(".mel"))
                    {
                        // Mel is always "built" when Maya is loaded
                        BuiltRegularly = true;
                        SourceOfBuiltRegularly = "ReleaseLocation";
                        SourceOfBuiltRegularly = "mel";
                        BuildName = "mel";
                        SourceOfBuildName = "ReleaseLocation";
                    }
                    m_astrReleaseLocation.Add(strReleaseLocation);
                }
            }
        }
        private List<string> m_astrSourceOfReleaseLocation = new List<string>();
        public List<string> SourceOfReleaseLocations
        {
            get
            {
                return m_astrSourceOfReleaseLocation;
            }
        }
        public string SourceOfReleaseLocation
        {
            set
            {
                if ((value == null) || (value == ""))
                {
                    m_astrSourceOfReleaseLocation.Clear();
                }
                else
                {
                    m_astrSourceOfReleaseLocation.Add(value);
                }
            }
        }
    }
}
