using System;
using System.Collections.Generic;
using rageUsefulCSharpToolClasses;

namespace rageGenerateToolDocs
{
	/// <summary>
	/// Summary description for Tool.
	/// </summary>
	public class Tool
	{
        override public string ToString()
        {
            string strOutputMe = "<Tool>\n";
            if ((SourceProjectLocation != null) && (SourceProjectLocation != "")) strOutputMe += ("\t<SourceProjectLocation>" + SourceProjectLocation + "</SourceProjectLocation>\n");
            strOutputMe += m_obSlnFile;
            if ((Description != null) && (Description != "")) strOutputMe += ("\t<Description>" + Description + "</Description>\n");
            if ((LinksToExternalDocs != null) && (LinksToExternalDocs != "")) strOutputMe += ("\t<LinksToExternalDocs>" + LinksToExternalDocs + "</LinksToExternalDocs>\n");
            if ((Owner != null) && (Owner != "")) strOutputMe += ("\t<Owner>" + Owner + "</Owner>\n");
            if ((InActiveUse != null) && (InActiveUse != "")) strOutputMe += ("\t<InActiveUse>" + InActiveUse + "</InActiveUse>\n");
            strOutputMe += "</Tool>\n";
            return strOutputMe;
        }

        public string ToHtml()
        {
            int iRowSpan = SlnFileObject.Builds.Count;
            if (iRowSpan == 0) iRowSpan = 1;
            string strTDTag = "<td rowspan=" + iRowSpan + "><font face=\"Arial\" size=1>";
            string strOutputMe = "<!-- " + Class1.Htmlize(SourceProjectLocation) + " --><tr>" + strTDTag + Class1.Htmlize(SourceProjectLocation) + "</td>" + strTDTag + Class1.Htmlize(Description) + "</td>" + strTDTag + Class1.Htmlize(LinksToExternalDocs) + "</td>" + strTDTag + Class1.Htmlize(Owner) + "</td>" + strTDTag + Class1.Htmlize(InActiveUse) + "</td>" + SlnFileObject.ToHtml() + "</tr>\n";
            return strOutputMe;
        }

        public List<string> ToReleasedToolsHtml(bool bIncludeSources)
        {
            List<string> astrReleasedToolsAsHtml = new List<string>();
            foreach(Build obBuild in SlnFileObject.Builds)
            {
                // Does it have a release location?
                for (int r = 0; r < obBuild.ReleaseLocations.Count; r++ )
                {
                    // It is released, so worth bothering with
                    string strHtmlForBuild = "<tr valign=TOP>";
                    if (bIncludeSources)
                    {
                        strHtmlForBuild = "<td><font face=\"Arial\" size=1>" + Class1.Htmlize(obBuild.ReleaseLocations[r]) + " [" + obBuild.SourceOfReleaseLocations[r] + "]</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(Description) + " [" + SourceOfDescription + "]</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(LinksToExternalDocs) + " [" + SourceOfLinksToExternalDocs + "]</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(Owner) + " [" + SourceOfOwner + "]</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(InActiveUse) + " [" + SourceOfInActiveUse + "]</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(SourceProjectLocation) + " [" + SourceOfSourceProjectLocation + "]</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(obBuild.BuildName) + " [" + obBuild.SourceOfBuildName + "]</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(obBuild.BuiltRegularly.ToString()) + " [" + obBuild.SourceOfBuiltRegularly + "]</td>";
                    }
                    else
                    {
                        strHtmlForBuild = "<td><font face=\"Arial\" size=1>" + Class1.Htmlize(obBuild.ReleaseLocations[r]) + "</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(Description) + "</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(LinksToExternalDocs) + "</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(Owner) + "</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(InActiveUse) + "</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(SourceProjectLocation) + "</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(obBuild.BuildName) + "</td><td><font face=\"Arial\" size=1>" + Class1.Htmlize(obBuild.BuiltRegularly.ToString());
                    }

                    strHtmlForBuild += "</tr>";
                    astrReleasedToolsAsHtml.Add(strHtmlForBuild);
                }
            }
            return astrReleasedToolsAsHtml;
        }

        public bool ProbablyTheSameTool(Tool obOtherTool)
        {
            if((obOtherTool.SourceProjectLocation != null) && (SourceProjectLocation == obOtherTool.SourceProjectLocation))
            {
                // They have the same SourceProjectLocation so probably the same tool
                return true;
            }
            return false;
        }

        public void UpdateFromTool(Tool obOtherTool)
        {
            if (obOtherTool.SourceProjectLocation != null)    SourceProjectLocation = obOtherTool.SourceProjectLocation;
            if (obOtherTool.Description != null)          Description = obOtherTool.Description;
            if (obOtherTool.LinksToExternalDocs != null) LinksToExternalDocs = obOtherTool.LinksToExternalDocs;
            if (obOtherTool.Owner != null) Owner = obOtherTool.Owner;
            InActiveUse = obOtherTool.InActiveUse;
        }

		private string m_strSourceProjectLocation;
		public string SourceProjectLocation
		{
			get
			{
				return m_strSourceProjectLocation;
			}
			set
			{
                m_strSourceProjectLocation = rageFileUtilities.RemoveFileExtension(value.Replace("\\", "/").ToLower());
                m_obSlnFile = new SlnFile(m_strSourceProjectLocation);
			}
		}

        private string m_strSourceOfSourceProjectLocation;
        public string SourceOfSourceProjectLocation
        {
            get
            {
                return m_strSourceOfSourceProjectLocation;
            }
            set
            {
                m_strSourceOfSourceProjectLocation = value;
            }
        }

        private SlnFile m_obSlnFile = new SlnFile();
        public SlnFile SlnFileObject
        {
            get
            {
                return m_obSlnFile;
            }
        }

		private string m_strDescription;
		public string Description
		{
			get
			{
				return m_strDescription;
			}
			set
			{
                if ((value != null) && (value != "") && (value != "unknown"))
                {
                    m_strDescription = value;
                }
			}
		}
        private string m_strSourceOfDescription;
        public string SourceOfDescription
        {
            get
            {
                return m_strSourceOfDescription;
            }
            set
            {
                m_strSourceOfDescription = value;
            }
        }

		private string m_strOwner;
		public string Owner
		{
			get
			{
				return m_strOwner;
			}
			set
			{
                if ((value != null) && (value != "") && (value != "unknown"))
                {
                    m_strOwner = value;
                }
			}
		}
        private string m_strSourceOfOwner;
        public string SourceOfOwner
        {
            get
            {
                return m_strSourceOfOwner;
            }
            set
            {
                m_strSourceOfOwner = value;
            }
        }

		private string m_strInActiveUse;
        public string InActiveUse
		{
			get
			{
				return m_strInActiveUse;
			}
			set
			{
                if ((value != null) && (value != "") && (value != "unknown"))
                {
                    m_strInActiveUse = value;
                }
			}
		}
        private string m_strSourceOfInActiveUse;
        public string SourceOfInActiveUse
        {
            get
            {
                return m_strSourceOfInActiveUse;
            }
            set
            {
                m_strSourceOfInActiveUse = value;
            }
        }

        private string m_strLinksToExternalDocs;
        public string LinksToExternalDocs
        {
            get
            {
                return m_strLinksToExternalDocs;
            }
            set
            {
                if ((value != null) && (value != "") && (value != "unknown"))
                {
                    m_strLinksToExternalDocs = value;
                }
            }
        }
        private string m_strSourceOfLinksToExternalDocs;
        public string SourceOfLinksToExternalDocs
        {
            get
            {
                return m_strSourceOfLinksToExternalDocs;
            }
            set
            {
                m_strSourceOfLinksToExternalDocs = value;
            }
        }

        private string m_strWhereTheToolWasFound;
        public string WhereTheToolWasFound
        {
            get
            {
                return m_strWhereTheToolWasFound;
            }
            set
            {
                m_strWhereTheToolWasFound = value;
            }
        }
    }
}
