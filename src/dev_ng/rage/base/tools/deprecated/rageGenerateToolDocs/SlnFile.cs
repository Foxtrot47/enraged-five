using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using rageUsefulCSharpToolClasses;

namespace rageGenerateToolDocs
{
    public class SlnFile
    {
        public SlnFile()
        {
            m_obBuilds = new List<Build>();
        }
        public SlnFile(string strSlnFilename)
		{
            ProjectPathAndFilename = strSlnFilename;
		}

        override public string ToString()
        {
            string strOutputMe = "<SlnFile>\n";
            strOutputMe += ("\t\t<ProjectPathAndFilename>" + ProjectPathAndFilename + "</ProjectPathAndFilename>\n");
            strOutputMe += ("\t\t<VcprojPathAndFilename>" + VcprojPathAndFilename + "</VcprojPathAndFilename>\n");
            strOutputMe += ("\t\t<CsprojPathAndFilename>" + CsprojPathAndFilename + "</CsprojPathAndFilename>\n");
            strOutputMe += "\t\t<Builds>\n";
            foreach (Build obBuild in m_obBuilds)
            {
                strOutputMe += obBuild;
            }
            strOutputMe += "\t\t</Builds>\n";
            strOutputMe += "</SlnFile>\n";
            return strOutputMe;
        }

        public string ToHtml()
        {
            if (m_obBuilds.Count > 0)
            {
//                string strOutputMe = "<table border=1 cellspacing=1 cellpadding=1 width=100%>\n";
//                strOutputMe += "<tr><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>BuildName</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>BuildLocation</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>ReleaseLocation</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>BuiltRegularly</td></tr>\n";
                string strOutputMe = "";
                foreach (Build obBuild in m_obBuilds)
                {
                    strOutputMe += obBuild.ToHtml();
                }
//                strOutputMe += "</table>";
                return strOutputMe.Substring(4);
            }
            return "<td><font face=\"Arial\" size=1>N/A</td><td><font face=\"Arial\" size=1>N/A</td><td><font face=\"Arial\" size=1>N/A</td><td><font face=\"Arial\" size=1>N/A</td></tr>\n";
        }

        private void GetWhatICanOutOfTheSlnFile()
        {
            // Ok, I have a sln file, what info can I extract out of it?
            // Hopefully I can get a vcproj file with the same name
            string strVcprojPathAndFilename = rageFileUtilities.RemoveFileExtension(ProjectPathAndFilename) +".vcproj";
            if(File.Exists(strVcprojPathAndFilename))
            {
                // Set the Vcproj file
                VcprojPathAndFilename = strVcprojPathAndFilename;
            }

            // Is there a csproj?
            string strCsprojPathAndFilename = rageFileUtilities.RemoveFileExtension(ProjectPathAndFilename) + ".csproj";
            if (File.Exists(strCsprojPathAndFilename))
            {
                // Set the Csproj file
                CsprojPathAndFilename = strCsprojPathAndFilename;
            }
        }

        private void GetWhatICanOutOfTheVcprojFile()
        {
            // Get the project name
            string strProjectName = rageFileUtilities.RemoveFileExtension(rageFileUtilities.GetFilenameFromFilePath(VcprojPathAndFilename));

            // Ok, I have a Vcproj file, what info can I extract out of it?
            XmlTextReader obXmlReader = new XmlTextReader(VcprojPathAndFilename);
            while (obXmlReader.Read())
            {
                // Is it a tag?
                // Console.WriteLine(obXmlReader.Name + " is of type " + obXmlReader.NodeType);
                if(obXmlReader.NodeType == XmlNodeType.Element)
                {
                    // what tag is it?
                    // Console.WriteLine("obXmlReader.Name = " + obXmlReader.Name);
                    if(obXmlReader.Name == "Configuration")
                    {
                        // Found a build configuration, extract the name
                        obXmlReader.MoveToAttribute("Name");
                        Build obBuild = new Build();
                        obBuild.BuildName = obXmlReader.Value;
                        obBuild.SourceOfBuildName = VcprojPathAndFilename;
                        string strOutputDirectory = null;
                        if(obXmlReader.MoveToAttribute("OutputDirectory"))
                        {
                            strOutputDirectory = obXmlReader.Value;
                        }
                        

                        // Got a name, so try and find an OutputFile
                        while (obBuild.BuildLocation == null)
                        {
                            obXmlReader.Read();
                            if ((obXmlReader.Name == "Configuration") && (obXmlReader.NodeType == XmlNodeType.EndElement))
                            {
                                // I'm leaving the configuration, so bail
                                break;
                            }
                            if((obXmlReader.NodeType == XmlNodeType.Element) && (obXmlReader.Name == "Tool"))
                            {
                                // Found a tool tag, does it have an output folder?
                                if (obXmlReader.MoveToAttribute("OutputFile"))
                                {
                                    obBuild.BuildLocation = obXmlReader.Value;
                                    obBuild.SourceOfBuildLocation = VcprojPathAndFilename;
                                    if (strOutputDirectory != null)
                                    {
                                        obBuild.BuildLocation = obBuild.BuildLocation.Replace("$(outdir)", strOutputDirectory);
                                    }
                                    obBuild.BuildLocation = obBuild.BuildLocation.Replace("$(projectname)", strProjectName);                                    
                                    break;
                                }
                            }
                        }
                        m_obBuilds.Add(obBuild);
                    }
                }
            }
        }

        private void GetWhatICanOutOfTheCsprojFile()
        {
            // Get the project name
            string strProjectName = rageFileUtilities.RemoveFileExtension(rageFileUtilities.GetFilenameFromFilePath(CsprojPathAndFilename));

            // Ok, I have a Csproj file, what info can I extract out of it?
            XmlTextReader obXmlReader = new XmlTextReader(CsprojPathAndFilename);
            while (obXmlReader.Read())
            {
                // Is it a tag?
                // Console.WriteLine(obXmlReader.Name + " is of type " + obXmlReader.NodeType);
                if (obXmlReader.NodeType == XmlNodeType.Element)
                {
                    // what tag is it?
                    // Console.WriteLine("obXmlReader.Name = " + obXmlReader.Name);
                    if (obXmlReader.Name == "Settings")
                    {
                        // Found a build configuration, extract the name
                        obXmlReader.MoveToAttribute("AssemblyName");
                        string strAssemblyName = obXmlReader.Value;
                        obXmlReader.MoveToAttribute("OutputType");
                        string strOutputType = obXmlReader.Value;
                        if ((strOutputType == "WinExe") || (strOutputType == "Exe")) strOutputType = "exe";

                        // Got a name, so try and find an OutputPath
                        while (!((obXmlReader.Name == "Settings") && (obXmlReader.NodeType == XmlNodeType.EndElement)))
                        {
                            obXmlReader.Read();
                            if ((obXmlReader.NodeType == XmlNodeType.Element) && (obXmlReader.Name == "Config"))
                            {
                                Build obBuild = new Build();
                                // Found a tool tag, does it have an output folder?
                                if (obXmlReader.MoveToAttribute("Name"))
                                {
                                    obBuild.BuildName = obXmlReader.Value;
                                    obBuild.SourceOfBuildName = CsprojPathAndFilename;
                                }
                                if (obXmlReader.MoveToAttribute("OutputPath"))
                                {
                                    obBuild.BuildLocation = rageFileUtilities.GetLocationFromPath(CsprojPathAndFilename) + obXmlReader.Value + strAssemblyName +"."+ strOutputType;
                                    obBuild.SourceOfBuildLocation = CsprojPathAndFilename;
                                }
                                m_obBuilds.Add(obBuild);
                            }
                        }
                    }
                }
            }
        }

        private string m_strProjectPathAndFilename;
        public string ProjectPathAndFilename
		{
			get
			{
                return m_strProjectPathAndFilename;
			}
			set
			{
                m_strProjectPathAndFilename = rageFileUtilities.RemoveFileExtension(value.Replace("\\", "/").ToLower());
                GetWhatICanOutOfTheSlnFile();
			}
		}

        private string m_strVcprojPathAndFilename;
        public string VcprojPathAndFilename
        {
            get
            {
                return m_strVcprojPathAndFilename;
            }
            set
            {
                m_strVcprojPathAndFilename = value.Replace("\\", "/").ToLower();
                GetWhatICanOutOfTheVcprojFile();
            }
        }

        private string m_strCsprojPathAndFilename;
        public string CsprojPathAndFilename
        {
            get
            {
                return m_strCsprojPathAndFilename;
            }
            set
            {
                m_strCsprojPathAndFilename = value.Replace("\\", "/").ToLower();
                GetWhatICanOutOfTheCsprojFile();
            }
        }

        private List<Build> m_obBuilds = new List<Build>();
        public List<Build> Builds
        {
            get
            {
                return m_obBuilds;
            }
        }

    }
}
