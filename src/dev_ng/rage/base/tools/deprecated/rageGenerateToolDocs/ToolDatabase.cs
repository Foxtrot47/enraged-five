using System;
using System.IO;
using System.Collections.Generic;
using rageUsefulCSharpToolClasses;

namespace rageGenerateToolDocs
{
	/// <summary>
	/// Summary description for ToolDatabase.
	/// </summary>
	public class ToolDatabase
	{
		public ToolDatabase()
		{
		}

		void AddTool(Tool obToolToAdd)
		{
            // Does the tool already exist?
            foreach(Tool obTool in m_obTools)
            {
                if(obTool.ProbablyTheSameTool(obToolToAdd))
                {
                    // I already have the tool, so just update it
                    obTool.UpdateFromTool(obToolToAdd);
                    return;
                }
            }
            
            // New tool, so just add it
            m_obTools.Add(obToolToAdd);
		}

		public void AddFolderOfToolSources(string strFolderName)
		{
			// Find all sln files, each one represents a tool
			// Does it contain files?
            foreach (string strFile in Directory.GetFiles(strFolderName, "*.vcproj"))
			{
				// Found a sln file, so fill out a tool object
                if ((!strFile.Contains("et2005")) && (!strFile.Contains("_2005")) && (!strFile.ToLower().Contains("backup")))
                {
                    Tool obNewTool = new Tool();
                    obNewTool.SourceProjectLocation = strFile;
                    obNewTool.SourceOfSourceProjectLocation = "AddFolderOfToolSources(\"" + strFolderName + "\")";
                    obNewTool.WhereTheToolWasFound = "AddFolderOfToolSources(\"" + strFolderName + "\")";
                    AddTool(obNewTool);
                }
			}

            foreach (string strFile in Directory.GetFiles(strFolderName, "*.csproj"))
            {
                // Found a sln file, so fill out a tool object
                if ((!strFile.Contains("et2005")) && (!strFile.Contains("_2005")) && (!strFile.ToLower().Contains("backup")))
                {
                    Tool obNewTool = new Tool();
                    obNewTool.SourceProjectLocation = strFile;
                    obNewTool.SourceOfSourceProjectLocation = "AddFolderOfToolSources(\"" + strFolderName + "\")";
                    obNewTool.WhereTheToolWasFound = "AddFolderOfToolSources(\"" + strFolderName + "\")";
                    AddTool(obNewTool);
                }
            }

            // Recurse
            foreach (string strSubDir in Directory.GetDirectories(strFolderName, "*"))
            {
                // It has sub directories, so delete them
                AddFolderOfToolSources(strSubDir);
            }
        }

        public void AddTextFileForBuilding(string strFilename)
        {
            // Open the file for parsing
            StreamReader sr = File.OpenText(strFilename);
			string strInput;
            while ((strInput = sr.ReadLine()) != null)
            {
                // Split input line on ","
                string[] astrInputParts = strInput.Split(",".ToCharArray());
                if (astrInputParts.Length >= 2)
                {
                    string strSlnPath = astrInputParts[0].Replace("\\", "/").ToLower();
                    string strSlnName = astrInputParts[1].ToLower();
                    string strProjectPathAndFilename = rageFileUtilities.GetLocationFromPath(rageFileUtilities.GetLocationFromPath(strFilename)) + "/" + strSlnPath + "/" + strSlnName;
                    strProjectPathAndFilename = strProjectPathAndFilename.ToLower();

                    // Does this already exist in the list of tools?
                    Tool obToolInDatabase = null;
                    foreach (Tool obTool in m_obTools)
                    {
                        if (rageFileUtilities.RemoveFileExtension(obTool.SourceProjectLocation).EndsWith(strProjectPathAndFilename))
                        {
                            // I already have the tool, so just update it
                            // Console.WriteLine("Bingo! " + strProjectPathAndFilename);
                            obToolInDatabase = obTool;
                            break;
                        }
                    }
                    if (obToolInDatabase == null)
                    {
                        // This is new tool, odd, but better log it
                        obToolInDatabase = new Tool();
                        obToolInDatabase.SourceProjectLocation = strProjectPathAndFilename;
                        obToolInDatabase.SourceOfSourceProjectLocation = "AddTextFileForBuilding(\"" + strFilename + "\")";
                        obToolInDatabase.WhereTheToolWasFound = "AddTextFileForBuilding(\"" + strFilename + "\")";
                        AddTool(obToolInDatabase);
                    }

                    // Add builds
                    // Flag builds that are build
                    if (astrInputParts.Length == 3)
                    {
                        string[] astrBuilds = astrInputParts[2].Replace(" ", "|").Split("\"".ToCharArray());
                        foreach (string strBuild in astrBuilds)
                        {
                            // Find the build in the list of builds
                            foreach (Build obBuild in obToolInDatabase.SlnFileObject.Builds)
                            {
                                // Console.WriteLine(obBuild.BuildName +"=="+ strBuild);
                                if (obBuild.BuildName == strBuild)
                                {
                                    // Bingo, found a build I know about, so flag it
                                    // Console.WriteLine(strBuild +" is built regularly");
                                    obBuild.BuiltRegularly = true;
                                    obBuild.SourceOfBuiltRegularly = strFilename;
                                }
                            }
                        }
                    }
                    else
                    {
                        // No builds are specified, so they are all built regularly
                        foreach (Build obBuild in obToolInDatabase.SlnFileObject.Builds)
                        {
                            // Bingo, found a build I know about, so flag it
                            // Console.WriteLine(strBuild +" is built regularly");
                            obBuild.SourceOfBuiltRegularly = strFilename;
                            obBuild.BuiltRegularly = true;
                        }
                    }
                }
            }
        }

        public void AddBatFileForBuilding(string strFilename)
        {
            // Open the file for parsing
            StreamReader sr = File.OpenText(strFilename);
            string strInput;
            while ((strInput = sr.ReadLine()) != null)
            {
                // Does it start with something we care about?
                if((strInput.StartsWith("CALL :copytool")) || (strInput.StartsWith("move")))
                {
                    // We care!  Split on ' '
                    string[] astrInputParts = strInput.ToLower().Replace("\"", "").Split(" ".ToCharArray());
                    
                    // Last part is the released name and the penultimate part is the source build location
                    string strReleasedPathAndFileName = astrInputParts[astrInputParts.Length - 1];
                    string strBuiltPathAndFileName = astrInputParts[astrInputParts.Length - 2];
                    string strBuiltFileName = rageFileUtilities.GetFilenameFromFilePath(strBuiltPathAndFileName);

                    // Now in most cases I have something like:
                    //
                    // strReleasedPathAndFileName = t:/rage/tools/modules/base/exes/fish.exe
                    // strBuiltPathAndFileName = c:/soft/rage/base/tools/fish/debug/fish.exe
                    //
                    // BUT sometimes I have something like:
                    //
                    // strReleasedPathAndFileName = t:/rage/tools/modules/base/exes
                    // strBuiltPathAndFileName = c:/soft/rage/base/tools/fish/debug/fish.exe
                    //
                    // I need to detect this and append the filename on the end of the released path and filename
                    if (!rageFileUtilities.GetFilenameFromFilePath(strReleasedPathAndFileName).Contains("."))
                    {
                        // A little dodgy, but if there is no "." in the filename, I assume there is no fileextension
                        // and therefore no file
                        strReleasedPathAndFileName += "/" + strBuiltFileName;
                    }


                    // See if I can find a tool with matching built name
                    bool bUnableToFindTool = true;
                    foreach (Tool obTool in m_obTools)
                    {
                        bool bUnableToFindBuild = true;
                        bool bThisIsTheRightTool = false;
                        foreach (Build obBuild in obTool.SlnFileObject.Builds)
                        {
                            if((obBuild.BuildLocation != null) && (rageFileUtilities.GetFilenameFromFilePath(obBuild.BuildLocation) == strBuiltFileName))
                            {
                                // Bingo, found a build I know about, so flag it
                                bUnableToFindTool = false;
                                bThisIsTheRightTool = true;

                                // Is this one actually built regularly though?
                                if (obBuild.BuiltRegularly == true)
                                {
                                    // Console.WriteLine(obTool.SourceProjectLocation + " is built to " + strReleasedPathAndFileName);
                                    obBuild.ReleaseLocation = strReleasedPathAndFileName;
                                    obBuild.SourceOfReleaseLocation = strFilename;
                                    bUnableToFindBuild = false;
                                }
                            }
                        }
                        // Is this my tool?
                        if ((bThisIsTheRightTool) && (bUnableToFindBuild))
                        {
                            // I found the tool, but not the build, so add an unknown build
                            Build obBuild = new Build();
                            obBuild.ReleaseLocation = strReleasedPathAndFileName;
                            obBuild.SourceOfReleaseLocation = strFilename;
                            obTool.SlnFileObject.Builds.Add(obBuild);
                        }
                    }
                    if (bUnableToFindTool)
                    {
                        Console.WriteLine("Nothing builds " + strReleasedPathAndFileName);
                    }
                }
            }
        }

        public void AddFolderOfReleasedTools(string strFolderName)
        {
            // Find all files, each one represents a tool
            // Skip certain folders
            if (strFolderName.Contains("bonustools") || strFolderName.Contains("leveltools") || strFolderName.Contains("unsupported"))
            {
                return;
            }
            // Does it contain files?
            foreach (string strFile in Directory.GetFiles(strFolderName, "*.*"))
            {
                // Found a file, so fill out a tool object
                // Last part is the released name and the penultimate part is the source build location
                string strReleasedPathAndFileName = strFile.ToLower().Replace("\\", "/");
                string strReleasedFileName = rageFileUtilities.GetFilenameFromFilePath(strReleasedPathAndFileName);

                // See if I can find a tool with matching release name?
                bool bFoundBuildThatForThisRelease = false;
                foreach (Tool obTool in m_obTools)
                {
                    foreach (Build obBuild in obTool.SlnFileObject.Builds)
                    {
                        foreach(string strReleaseLocation in obBuild.ReleaseLocations)
                        {
                            if (strReleaseLocation == strReleasedPathAndFileName) 
                            {
                                // Bingo, found a release I know about, so, nothing needs to be done for this file
                                bFoundBuildThatForThisRelease = true;
                                break;
                            }
                        }
                    }
                    if (bFoundBuildThatForThisRelease)
                    {
                        break;
                    }
                }

                // Did that work?
                if (!bFoundBuildThatForThisRelease)
                {
                    // I am not able to find something that releases this tool :(
                    // Can I find something that builds a tool with the same name?
                    bool bFoundToolThatMightResultInThisRelease = false;
                    foreach (Tool obTool in m_obTools)
                    {
                        foreach (Build obBuild in obTool.SlnFileObject.Builds)
                        {
                            if ((obBuild.BuildLocation != null) && (rageFileUtilities.GetFilenameFromFilePath(obBuild.BuildLocation) == strReleasedFileName))
                            {
                                // Bingo, found a tool that results in this file, but not sure which build so add an unknown build
                                Build obNewBuild = new Build();
                                obNewBuild.ReleaseLocation = strReleasedPathAndFileName;
                                obNewBuild.SourceOfReleaseLocation = "AddFolderOfReleasedTools(\""+ strFolderName +"\")";
                                obTool.SlnFileObject.Builds.Add(obNewBuild);
                                bFoundToolThatMightResultInThisRelease = true;
                                break;
                            }
                        }
                        if (bFoundToolThatMightResultInThisRelease)
                        {
                            break;
                        }
                    }

                    // Did that work?
                    if (!bFoundToolThatMightResultInThisRelease)
                    {
                        // I am not able to find something that releases this tool :(
                        // Or something that builds a tool with the same name :(
                        // So just add as a mystry tool
                        Tool obNewTool = new Tool();
                        obNewTool.WhereTheToolWasFound = "AddFolderOfReleasedTools(\"" + strFolderName + "\")";
                        Build obBuild = new Build();
                        obBuild.ReleaseLocation = strReleasedPathAndFileName;
                        obBuild.SourceOfReleaseLocation = "AddFolderOfReleasedTools(\"" + strFolderName + "\")";
                        obNewTool.SlnFileObject.Builds.Add(obBuild);
                        m_obTools.Add(obNewTool);
                    }
                }
            }

            // Recurse
            foreach (string strSubDir in Directory.GetDirectories(strFolderName, "*"))
            {
                // It has sub directories, so delete them
                AddFolderOfReleasedTools(strSubDir.ToLower());
            }
        }

        public void AddTextFileForExtraData(string strFilename)
        {
            // Open the file for parsing
            // Console.WriteLine("AddTextFileForExtraData(\""+ strFilename +"\")");
            StreamReader sr = File.OpenText(strFilename);

            // Skip header
            sr.ReadLine();

            // Parse the rest of the file
            string strInput;
            while ((strInput = sr.ReadLine()) != null)
            {
                // Split input line on ","
                // Console.WriteLine("strInput = " + strInput);
                string[] astrInputParts = strInput.Split(",".ToCharArray());
                // Console.WriteLine("astrInputParts.Length = " + astrInputParts.Length);
                if (astrInputParts.Length > 1)
                {
                    string strSearchString = astrInputParts[0].ToLower();
                    string strOwner = astrInputParts[1];
                    string strInUse = astrInputParts[2];
                    string strDescription = astrInputParts[3];
                    string strLinksToExternalDocs = null;
                    if (astrInputParts.Length > 4)
                    {
                        strLinksToExternalDocs = astrInputParts[4];
                    }

                    // Can I find it
                    List<Tool> obToolsToAddInfoTo = new List<Tool>();
                    foreach (Tool obTool in m_obTools)
                    {
                        // Search string should either be the obTool.SourceProjectLocation...
                        if (obTool.SourceProjectLocation != null)
                        {
                            if (obTool.SourceProjectLocation == strSearchString)
                            {
                                // Console.WriteLine(obTool.SourceProjectLocation + " ~ " + strSearchString);
                                // Bingo, found a build I know about, so flag it
                                // Console.WriteLine(obTool.SourceProjectLocation + " is built to " + strReleasedPathAndFileName);
                                obToolsToAddInfoTo.Add(obTool);
                            }
                        }

                        // ...or as the ReleaseLocation of a build.
                        foreach(Build obBuild in obTool.SlnFileObject.Builds)
                        {
                            foreach(string strReleaseLocation in obBuild.ReleaseLocations)
                            {
                                if (strReleaseLocation == strSearchString)
                                {
                                    // Bingo, found a build I know about, so flag it
                                    obToolsToAddInfoTo.Add(obTool);
                                }
                            }
                        }
                    }
                    if (obToolsToAddInfoTo.Count > 0)
                    {
                        // Console.WriteLine(obTool.SourceProjectLocation + " ~ " + strSearchString);
                        // Bingo, found a build I know about, so flag it
                        // Console.WriteLine(obTool.SourceProjectLocation + " is built to " + strReleasedPathAndFileName);
                        foreach(Tool obTool in obToolsToAddInfoTo)
                        {
                            obTool.Owner = strOwner;
                            obTool.InActiveUse = strInUse;
                            obTool.Description = strDescription;
                            obTool.LinksToExternalDocs = strLinksToExternalDocs;
                        }
                    }
                    else
                    {
                        Console.WriteLine("I have info on " + strSearchString +" but nowhere to put it");
                    }
                }
            }
            sr.Close();
        }

        override public string ToString()
        {
            string strOutputMe = "<ToolDatabase>\n";
            foreach(Tool obTool in m_obTools)
            {
                strOutputMe += obTool;
            }
            strOutputMe += "</ToolDatabase>\n";
            return strOutputMe;
        }


        public void OutputAsXMLFile(string strXMLFilename)
        {
            // Open file for write
            StreamWriter obDBWriter;
            try
            {
                obDBWriter = File.CreateText(strXMLFilename);
            }
            catch (Exception e)
            {
                // Something bad happened
                Console.WriteLine("Failed to create file " + strXMLFilename + " : " + e.ToString());
                return;
            }


            // Write it out
            obDBWriter.WriteLine(ToString());

            // Close file
            obDBWriter.Close();
        }

        private void WriteOutHtmlHeader(StreamWriter obWriter, string strTitle)
        {
            // Write header
            obWriter.WriteLine("<html><head><title>" + strTitle + "</title></head><body bgcolor=#EFEFFF>");
            obWriter.WriteLine("<h1>" + strTitle + "</h1><p>");

            // Links to other forms
            obWriter.WriteLine("<body><font face=\"Arial\" size=1>");
            obWriter.WriteLine("[<a href=\"tools_everything.html\">All Tools</a>]");
            obWriter.WriteLine("[<a href=\"tools_just_released.html\">Released Tools</a>]");
            obWriter.WriteLine("[<a href=\"toolsInput.csv\">Tool Descriptions .csv</a>]");
            obWriter.WriteLine("<p>");
        }


        private void WriteOutHtmlFooter(StreamWriter obWriter)
        {
            // Write footer
            obWriter.WriteLine("<center>Automagically Generated By Kevin's C# HTML Tool Documenterizer</center>");
            obWriter.WriteLine("<center>Last update: " + System.DateTime.Now.ToLongTimeString() + " " + System.DateTime.Now.ToLongDateString() + "</center>");
            obWriter.WriteLine("<center><SCRIPT LANGUAGE=\"JavaScript\">\nvar theDate = document.lastModified\ndocument.write(\"Last upload: \");\ndocument.write(theDate);\n</SCRIPT></center>");
        }

        public void OutputAsHtmlFile(string strHtmlFilename)
        {
            // Open file for write
            StreamWriter obDBWriter;
            try
            {
                obDBWriter = File.CreateText(strHtmlFilename);
            }
            catch (Exception e)
            {
                // Something bad happened
                Console.WriteLine("Failed to create file " + strHtmlFilename + " : " + e.ToString());
                return;
            }


            // Write it out
            WriteOutHtmlHeader(obDBWriter, "All The Tools");
            obDBWriter.WriteLine(ToHtml());

            // Close file
            WriteOutHtmlFooter(obDBWriter);
            obDBWriter.Close();
        }


        public string ToHtml()
        {
            string strOutputMe = "<table border=1 cellspacing=1 cellpadding=1 width=100%>\n";
            strOutputMe += "<tr valign=TOP><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>SourceProjectLocation</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Description</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Links To External Docs</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Owner</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>InActiveUse</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Build Name</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Build Location</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Release Location</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Built Regularly</td></tr>";
            List<string> astrLinesOfHtmlToOutput = new List<string>();
            foreach (Tool obTool in m_obTools)
            {
                astrLinesOfHtmlToOutput.Add(obTool.ToHtml());
            }
            astrLinesOfHtmlToOutput.Sort();
            foreach(string strLineOfOutput in astrLinesOfHtmlToOutput)
            {
                strOutputMe += strLineOfOutput;
            }
            strOutputMe += "</table>\n";
            return strOutputMe;
        }

        public void OutputAsReleasedToolsHtmlFile(string strHtmlFolderName)
        {
            OutputAsReleasedToolsHtmlFile(strHtmlFolderName, false);
        }
        public void OutputAsReleasedToolsWithSourcesHtmlFile(string strHtmlFolderName)
        {
            OutputAsReleasedToolsHtmlFile(strHtmlFolderName, true);
        }

        private void OutputAsReleasedToolsHtmlFile(string strHtmlFolderName, bool bIncludeSources)
        {
            // Open file for write
            StreamWriter obDBWriter;
            string strOutputFilename;
            if(bIncludeSources)
            {
                strOutputFilename = strHtmlFolderName + "/tools_just_released_with_sources.html";
            }
            else
            {
                strOutputFilename = strHtmlFolderName + "/tools_just_released.html";
            }
            try
            {
                obDBWriter = File.CreateText(strOutputFilename);
            }
            catch (Exception e)
            {
                // Something bad happened
                Console.WriteLine("Failed to create file " + strOutputFilename + " : " + e.ToString());
                return;
            }


            // Write it out
            WriteOutHtmlHeader(obDBWriter, "All Released Tools");

            // Write it out as a table sorted by release folder
            obDBWriter.WriteLine("<table border=1 cellspacing=1 cellpadding=1 width=100%>");
            obDBWriter.WriteLine("<tr valign=TOP><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Release Location</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Description</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Links to external docs</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Owner</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>InActiveUse</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Sln File</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Build Name</td><td bgcolor=#DEDEEE><font face=\"Arial\" size=1>Built Regularly</td></tr>");
            List<string> astrReleasedToolsAsHtml = new List<string>();
            foreach (Tool obTool in m_obTools)
            {
                astrReleasedToolsAsHtml.AddRange(obTool.ToReleasedToolsHtml(bIncludeSources));
            }
            astrReleasedToolsAsHtml.Sort();

            foreach(string strLineOfHtml in astrReleasedToolsAsHtml)
            {
                obDBWriter.WriteLine(strLineOfHtml);
            }
            obDBWriter.WriteLine("</table>");

            // Close file
            WriteOutHtmlFooter(obDBWriter);
            obDBWriter.Close();
        }


        public void OutputTextFileForExtraData(string strFilename)
        {
            // Open file for write
            StreamWriter obDBWriter;
            try
            {
                obDBWriter = File.CreateText(strFilename);
            }
            catch (Exception e)
            {
                // Something bad happened
                Console.WriteLine("Failed to create file " + strFilename +" : " + e.ToString());
                return;
            }

            // Output header
            obDBWriter.WriteLine("PROJECT,OWNER,ACTIVE USE,DESCRIPTION,EXTERNAL DOCUMENTATION");

            // Output info
            List<string> astrToolInfos = new List<string>();
            foreach (Tool obTool in m_obTools)
            {
                if(obTool.SourceProjectLocation != null)
                {
                    astrToolInfos.Add(obTool.SourceProjectLocation
                        + "," +
                        obTool.Owner
                        + "," +
                        obTool.InActiveUse
                        + "," +
                        obTool.Description
                        + "," +
                        obTool.LinksToExternalDocs
                        );
                }
                else
                {
                    foreach(Build obBuild in obTool.SlnFileObject.Builds)
                    {
                        foreach(string strReleaseLocation in obBuild.ReleaseLocations)
                        {
                            astrToolInfos.Add(strReleaseLocation
                                + "," +
                                obTool.Owner
                                + "," +
                                obTool.InActiveUse
                                + "," +
                                obTool.Description
                                + "," +
                                obTool.LinksToExternalDocs
                                );
                        }
                    }
                }
            }
            astrToolInfos.Sort();

            foreach (string strLineOfInfo in astrToolInfos)
            {
                obDBWriter.WriteLine(strLineOfInfo);
            }

            // Close file
            obDBWriter.Close();
        }

        private List<Tool> m_obTools = new List<Tool>();
	}
}

