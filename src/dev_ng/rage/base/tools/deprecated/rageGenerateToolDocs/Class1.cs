using System;

namespace rageGenerateToolDocs
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
        public static string Htmlize(string strInputString)
        {
            if ((strInputString == null) || (strInputString == ""))
            {
                return "N/A";
            }
            return strInputString;
        }

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			//
			// TODO: Add code to start application here
			//
            ToolDatabase obToolDatabase = new ToolDatabase();
            obToolDatabase.AddFolderOfToolSources("c:/soft/rage/base/tools");
            //obToolDatabase.AddFolderOfToolSources("c:/soft/rage");
            obToolDatabase.AddTextFileForBuilding("C:/soft/rage/qa/tools.txt");
            obToolDatabase.AddTextFileForBuilding("C:/soft/rage/qa/maya60_tools.txt");
            obToolDatabase.AddTextFileForBuilding("C:/soft/rage/qa/maya65_tools.txt");
            obToolDatabase.AddTextFileForBuilding("C:/soft/rage/qa/maya70_tools.txt");
            obToolDatabase.AddBatFileForBuilding("C:/soft/rage/qa/copytools.bat");
            obToolDatabase.AddBatFileForBuilding("C:/soft/rage/qa/build_and_copy_maya_tools.bat");
            obToolDatabase.AddFolderOfReleasedTools("t:/rage/tools/modules");
//            obToolDatabase.AddTextFileForExtraData(Environment.GetEnvironmentVariable("TEMP") + "/tools.csv");
            obToolDatabase.AddTextFileForExtraData(Environment.GetEnvironmentVariable("TEMP") + "/toolsInput.csv");
            obToolDatabase.OutputAsXMLFile(Environment.GetEnvironmentVariable("TEMP") + "/Tools.xml");
            obToolDatabase.OutputAsHtmlFile(Environment.GetEnvironmentVariable("TEMP") + "/Tools_Everything.html");
            obToolDatabase.OutputAsReleasedToolsHtmlFile(Environment.GetEnvironmentVariable("TEMP"));
            obToolDatabase.OutputAsReleasedToolsWithSourcesHtmlFile(Environment.GetEnvironmentVariable("TEMP"));
            obToolDatabase.OutputTextFileForExtraData(Environment.GetEnvironmentVariable("TEMP") + "/toolsInput.csv");
            // Console.WriteLine(obToolDatabase);
        }
	}
}
