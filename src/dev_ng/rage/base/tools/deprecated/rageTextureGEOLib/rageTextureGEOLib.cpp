// 
// rageTextureGEOLib/rageTextureGEOLib.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "atl/array.h"
#include "rageTextureGEOLib.h"

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------

void GeoTexturePlatformOptionsType::LoadFromXML(parTreeNode &data)
{
	parTreeNode *pFormatNode = data.FindChildWithName("Format");
	if(pFormatNode && pFormatNode->HasData())
		m_Format = pFormatNode->GetData();

	parTreeNode *pMipLevelsNode = data.FindChildWithName("MipLevels");
	if(pMipLevelsNode)
	{
		parAttribute* pAttr = pMipLevelsNode->GetElement().FindAttribute("value");
		if(pAttr)
			m_MipLevels = atoi(pAttr->GetStringValue());
	}
}

//-----------------------------------------------------------------------------

GeoTypeBase* GeoTexturePlatformOptionsTypeLoader::LoadType(GeoCoreManager& UNUSED_PARAM(manager), const char* UNUSED_PARAM(name), parTreeNode& data, GeoTypeLoaderContext* UNUSED_PARAM(context))
{
	GeoTexturePlatformOptionsType* pType = new GeoTexturePlatformOptionsType();

	//Load the type specific data
	pType->LoadFromXML(data);
	
	return pType;
}

//-----------------------------------------------------------------------------

void GeoTexturePlatformOptions::LoadFromXML(parTreeNode& UNUSED_PARAM(data))
{
	//No instance only data to load yet
}

//-----------------------------------------------------------------------------

GeoInstanceBase* GeoTexturePlatformOptionsLoader::LoadInstance(GeoCoreManager& manager, const char* UNUSED_PARAM(name), parTreeNode& data, GeoTypeLoaderContext* UNUSED_PARAM(context))
{
	GeoTexturePlatformOptionsType *pBaseType = dynamic_cast<GeoTexturePlatformOptionsType*>(manager.GetType(TEXTURE_OPTIONS_TYPE_NAME, TEXTURE_OPTIONS_TEMPLATE_NAME));
	if(!pBaseType)
		return NULL;

	GeoTexturePlatformOptions* pInstance = new GeoTexturePlatformOptions();
	
	//Set the contained type to the geo type file values.
	pInstance->m_Type = (*pBaseType);
	
	//Override any default type values with those specified in the instance.
	pInstance->m_Type.LoadFromXML(data);

	//Load the instance specific data
	pInstance->LoadFromXML(data);	

	return pInstance;
}

//-----------------------------------------------------------------------------

void GeoTextureOptionsType::LoadFromXML( parTreeNode& data )
{
	parTreeNode *pSharpenMipNode = data.FindChildWithName("SharpenMipMaps");
	if(pSharpenMipNode)
	{
		parAttribute* pAttr = pSharpenMipNode->GetElement().FindAttribute("value");
		if(pAttr)
		{
			if(strcmpi(pAttr->GetStringValue(), "false") == 0)
				m_SharpenMipMaps = false;
			else
				m_SharpenMipMaps = true;
		}
	}

	parTreeNode *pSharpenMipFilterNode = data.FindChildWithName("SharpenMipFilter");
	if(pSharpenMipFilterNode)
	{
		if(pSharpenMipFilterNode->HasData())
		{
			const char* filterStr = pSharpenMipFilterNode->GetData();
			if(strcmpi(filterStr, "Soft") == 0)
				m_SharpenMipFilter = MIPSHARP_SOFT;
			else if(strcmpi(filterStr, "Medium") == 0)
				m_SharpenMipFilter = MIPSHARP_MEDIUM;
			else if(strcmpi(filterStr, "Hard") == 0)
				m_SharpenMipFilter = MIPSHARP_HARD;
			else
			{
				Assertf(0, "Invalid mip-sharpening filter \"%s\" specified in texture options data.", filterStr);
			}
		}
	}

	parTreeNode *pFadeMipNode = data.FindChildWithName("FadeMipMaps");
	if(pFadeMipNode)
	{
		parAttribute* pAttr = pFadeMipNode->GetElement().FindAttribute("value");
		if(pAttr)
		{
			if(strcmpi(pAttr->GetStringValue(), "false") == 0)
				m_FadeMipMaps = false;
			else
				m_FadeMipMaps = true;
		}
	}

	parTreeNode *pFadeMipMapsAmountNode = data.FindChildWithName("FadeMipMapsAmount");
	if(pFadeMipMapsAmountNode)
	{
		parAttribute* pAttr = pFadeMipMapsAmountNode->GetElement().FindAttribute("value");
		if(pAttr)
			m_FadeMipMapsAmount = (float)atof(pAttr->GetStringValue());
	}

	parTreeNode *pExposureNode = data.FindChildWithName("Exposure");
	if(pExposureNode)
	{
		parAttribute* pAttr = pExposureNode->GetElement().FindAttribute("value");
		if(pAttr)
			m_Exposure = (float)atof(pAttr->GetStringValue());
	}

	parTreeNode *pCalcHDRNode = data.FindChildWithName("CalcHDRScaleAndBias");
	if(pCalcHDRNode)
	{
		parAttribute* pAttr = pCalcHDRNode->GetElement().FindAttribute("value");
		if(pAttr)
		{
			if(strcmpi(pAttr->GetStringValue(), "false") == 0)
				m_CalcHDRScaleAndBias = false;
			else
				m_CalcHDRScaleAndBias = true;
		}
	}

	parTreeNode *pMaxWidthNode = data.FindChildWithName("MaxWidth");
	if(pMaxWidthNode)
	{
		parAttribute* pAttr = pMaxWidthNode->GetElement().FindAttribute("value");
		if(pAttr)
		{
			m_MaxWidth = atoi(pAttr->GetStringValue());
		}
	}

	parTreeNode *pMaxHeightNode = data.FindChildWithName("MaxHeight");
	if(pMaxHeightNode)
	{
		parAttribute* pAttr = pMaxHeightNode->GetElement().FindAttribute("value");
		if(pAttr)
		{
			m_MaxHeight = atoi(pAttr->GetStringValue());
		}
	}

	parTreeNode *pSetSRGBGammaNode = data.FindChildWithName("SetSRGBGamma");
	if(pSetSRGBGammaNode)
	{
		parAttribute* pAttr = pSetSRGBGammaNode->GetElement().FindAttribute("value");
		if(pAttr)
		{
			if(strcmpi(pAttr->GetStringValue(), "false") == 0)
				m_SetSRGBGamma = false;
			else
				m_SetSRGBGamma = true;
		}
	}

	parTreeNode* pPCNode = data.FindChildWithName("PC");
	if(pPCNode)
	{
		m_PC.LoadFromXML(*pPCNode);
	}

	parTreeNode* pXbox360Node = data.FindChildWithName("Xbox360");
	if(pXbox360Node)
	{
		m_Xbox360.LoadFromXML(*pXbox360Node);
	}

	parTreeNode* pPS3Node = data.FindChildWithName("PS3");
	if(pPS3Node)
	{
		m_PS3.LoadFromXML(*pPS3Node);
	}
}

//-----------------------------------------------------------------------------

GeoTypeBase* GeoTextureOptionsTypeLoader::LoadType(GeoCoreManager& UNUSED_PARAM(manager), const char* UNUSED_PARAM(name), parTreeNode& data, GeoTypeLoaderContext* UNUSED_PARAM(context))
{
	GeoTextureOptionsType *pType = new GeoTextureOptionsType();

	pType->LoadFromXML(data);

	return pType;
}

//-----------------------------------------------------------------------------

bool GeoTextureOptions::HasCubeMapTextures() const
{
	//If any texture is specified for the cube map, then the
	//we should return that cube map textures have been specified
	//in the options data
	for(int i=0; i<6; i++)
	{
		if(m_CubeMapTextures[i].size() != 0)
			return true;
	}
	return false;
}

//-----------------------------------------------------------------------------

void GeoTextureOptions::LoadFromXML( parTreeNode& data )
{
	//Load the instance specific data...

	//Load the exposure textures
	parTreeNode *pExposureTexNode = data.FindChildWithName("ExposureTextures");
	if(pExposureTexNode)
	{
		parTreeNode::ChildNodeIterator cIter = pExposureTexNode->BeginChildren();
		while(*cIter)
		{
			parTreeNode *pItem = (*cIter);
			if(pItem->HasData())
				m_ExposureTextures.push_back( pItem->GetData() );
			++cIter;
		}
	}

	//Load the user created mip-map textures
	parTreeNode *pMipTexturesNode = data.FindChildWithName("MipTextures");
	if(pMipTexturesNode)
	{
		parTreeNode::ChildNodeIterator cIter = pMipTexturesNode->BeginChildren();
		while(*cIter)
		{
			parTreeNode *pItem = (*cIter);
			if(pItem->HasData())
				m_MipTextures.push_back( pItem->GetData() );
			++cIter;
		}
	}

	//Load the user specified cube-map textures
	parTreeNode *pCubeNode = data.FindChildWithName("CubeMapPX");
	if(pCubeNode && pCubeNode->HasData())
	{
		m_CubeMapTextures[CUBEMAP_PX] = pCubeNode->GetData();
	}
	pCubeNode = data.FindChildWithName("CubeMapNX");
	if(pCubeNode && pCubeNode->HasData())
	{
		m_CubeMapTextures[CUBEMAP_NX] = pCubeNode->GetData();
	}
	pCubeNode = data.FindChildWithName("CubeMapPY");
	if(pCubeNode && pCubeNode->HasData())
	{
		m_CubeMapTextures[CUBEMAP_PY] = pCubeNode->GetData();
	}
	pCubeNode = data.FindChildWithName("CubeMapNY");
	if(pCubeNode && pCubeNode->HasData())
	{
		m_CubeMapTextures[CUBEMAP_NY] = pCubeNode->GetData();
	}
	pCubeNode = data.FindChildWithName("CubeMapPZ");
	if(pCubeNode && pCubeNode->HasData())
	{
		m_CubeMapTextures[CUBEMAP_PZ] = pCubeNode->GetData();
	}
	pCubeNode = data.FindChildWithName("CubeMapNZ");
	if(pCubeNode && pCubeNode->HasData())
	{
		m_CubeMapTextures[CUBEMAP_NZ] = pCubeNode->GetData();
	}
}

//-----------------------------------------------------------------------------

GeoInstanceBase* GeoTextureOptionsLoader::LoadInstance(GeoCoreManager& manager, const char* name, parTreeNode& data, GeoTypeLoaderContext* UNUSED_PARAM(context))
{
	const char* baseTypeName = TEXTURE_OPTIONS_TYPE_NAME;
	parAttribute *pBaseAttr = data.GetElement().FindAttribute("base");
	if(pBaseAttr)
	{
		baseTypeName = pBaseAttr->GetStringValue();
	}
	else
	{
		Warningf("Attribute 'base' not defined for GEO \"%s\" defaulting to \"%s\"", name, TEXTURE_OPTIONS_TYPE_NAME);
	}

	GeoTextureOptionsType* pBaseType = dynamic_cast<GeoTextureOptionsType*>(manager.GetType(baseTypeName, TEXTURE_OPTIONS_TEMPLATE_NAME));
	if(!pBaseType)
	{
		Errorf("Failed to load GEO base type \"%s/%s.geotype\"", manager.GetGeoTypePath(), baseTypeName);
		return NULL;
	}

	GeoTextureOptions* pInstance = new GeoTextureOptions();

	//Set the contained type to the geo type file values.
	pInstance->m_Type = (*pBaseType);
	
	//Override any default type values with those specified in the instance.
	pInstance->m_Type.LoadFromXML(data);

	//Load the instance specific data
	pInstance->LoadFromXML(data);	

	//TODO?? Should this be using the GEO Load calls rather than doing this manually?
	//Set the contined type data
	pInstance->m_PC.m_Type = pInstance->m_Type.GetPCOptions();
	pInstance->m_Xbox360.m_Type = pInstance->m_Type.GetXBox360Options();
	pInstance->m_PS3.m_Type = pInstance->m_Type.GetPS3Options();

	//Load the contained type instance data which can override the default type data set 
	//in the previous calls
	parTreeNode* pPCNode = data.FindChildWithName("PC");
	if(pPCNode)
	{
		pInstance->m_PC.LoadFromXML(*pPCNode);
	}

	parTreeNode* pXbox360Node = data.FindChildWithName("Xbox360");
	if(pXbox360Node)
	{
		pInstance->m_Xbox360.LoadFromXML(*pXbox360Node);
	}

	parTreeNode* pPS3Node = data.FindChildWithName("PS3");
	if(pPS3Node)
	{
		pInstance->m_PS3.LoadFromXML(*pPS3Node);
	}

	//Set the instance specific data
	pInstance->m_Name = name;

	return pInstance;
}

//-----------------------------------------------------------------------------

GeoTextureOptionsInstanceManager::~GeoTextureOptionsInstanceManager()
{
	for(INSTANCE_TABLE::iterator it = m_Instances.begin(); it != m_Instances.end(); ++it)
	{
		if( it->second )
			delete it->second;
	}
}

//-----------------------------------------------------------------------------

void GeoTextureOptionsInstanceManager::AddInstance(GeoInstanceBase& inst)
{
	GeoTextureOptions* pTexInst = dynamic_cast<GeoTextureOptions*>(&inst);
	if(pTexInst)
	{
		INSTANCE_TABLE::iterator it = m_Instances.find( pTexInst->GetName() );
		if( it == m_Instances.end() )
			m_Instances[ pTexInst->GetName() ] = pTexInst;
		else
		{
			//Replace the existing instance with the new one
			Warningf("Replacing texture options for '%s' with updated version.", pTexInst->GetName() );

			delete it->second;
			m_Instances.erase(it);
			m_Instances[ pTexInst->GetName() ] = pTexInst;
		}
	}
}

//-----------------------------------------------------------------------------

GeoTextureOptions*	GeoTextureOptionsInstanceManager::GetInstance(const std::string& name)
{
	INSTANCE_TABLE::iterator it = m_Instances.find( name );
	if( it != m_Instances.end() )
		return it->second;
	else
		return NULL;
}

//-----------------------------------------------------------------------------

TextureOptionsManager::TextureOptionsManager()
	: m_pDefaultOptionsInstance(NULL)
{
	m_geoCoreMananger.RegisterTemplate(TEXTURE_OPTIONS_TEMPLATE_NAME, 
										&m_textureOptionsLoader, 
										&m_textureOptionsTypeLoader,
										&m_textureInstanceManager);
}


//-----------------------------------------------------------------------------

GeoTextureOptions* TextureOptionsManager::LoadOptionsFile(const char* path)
{
	atArray<GeoInstanceBase*> retInstanceArr;
	m_geoCoreMananger.LoadFile(path, &retInstanceArr);
	if(retInstanceArr.GetCount())
		return dynamic_cast<GeoTextureOptions*>(retInstanceArr[0]);
	else
		return NULL;
}

//-----------------------------------------------------------------------------

GeoTextureOptions*	TextureOptionsManager::GetDefaultOptions()
{
	if(m_pDefaultOptionsInstance)
		return m_pDefaultOptionsInstance;
	else
	{
		GeoTextureOptionsType* pBaseType = dynamic_cast<GeoTextureOptionsType*>(m_geoCoreMananger.GetType(TEXTURE_OPTIONS_TYPE_NAME, TEXTURE_OPTIONS_TEMPLATE_NAME));
		if(!pBaseType)
			return NULL;

		m_pDefaultOptionsInstance = new GeoTextureOptions();

		//Set the contained type to the geo type file values.
		m_pDefaultOptionsInstance->m_Type = (*pBaseType);
		m_pDefaultOptionsInstance->m_PC.m_Type = pBaseType->GetPCOptions();
		m_pDefaultOptionsInstance->m_Xbox360.m_Type = pBaseType->GetXBox360Options();
		m_pDefaultOptionsInstance->m_PS3.m_Type = pBaseType->GetPS3Options();

		m_pDefaultOptionsInstance->m_Name = "default";
		return m_pDefaultOptionsInstance;
	}
}

//-----------------------------------------------------------------------------

void TextureOptionsManager::TexturePathToOptionsFilePath(char* outPath, int outBufLen, const char* texturePath)
{
	ASSET.BaseName(outPath, outBufLen, texturePath);
	int pos = strlen(outPath);
	int extLen = strlen(TEXTURE_OPTIONS_FILE_EXT);
	strncat(outPath, TEXTURE_OPTIONS_FILE_EXT, Min<int>(extLen, (outBufLen-pos-1)));
}

//-----------------------------------------------------------------------------

