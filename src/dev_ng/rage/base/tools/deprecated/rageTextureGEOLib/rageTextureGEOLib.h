// 
// rageTextureGEOLib/rageTextureGEOLib.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#ifndef __RAGE_TEXTUREGEOLIB_H__
#define __RAGE_TEXTUREGEOLIB_H__

#include <string>
#include <vector>
#include <map>

#include "atl/singleton.h"
#include "geo/geoload.h"
#include "parser/manager.h"

using namespace rage;
using namespace std;

//-----------------------------------------------------------------------------

#define TEXTURE_OPTIONS_TEMPLATE_NAME	"BaseTextureOptions"
#define TEXTURE_OPTIONS_TYPE_NAME		"TextureOptions"
#define TEXTURE_OPTIONS_INST_NAME		"BaseTextureOptionsInst"
#define TEXTURE_OPTIONS_FILE_EXT		".geo"

//-----------------------------------------------------------------------------

#define PLATFORM_PC			"PC"
#define PLATFORM_XBOX360	"XBOX360"
#define PLATFORM_PS3		"PS3"

//-----------------------------------------------------------------------------

// PURPOSE: Class to represent the type data for the platform specific texture options
class GeoTexturePlatformOptionsType : public GeoTypeBase
{
public:
	GeoTexturePlatformOptionsType()
		: m_Format("DXT5")
		, m_MipLevels(0)
	{};

	GeoTexturePlatformOptionsType(const GeoTexturePlatformOptionsType& other)
		: m_Format(other.m_Format)
		, m_MipLevels(other.m_MipLevels)
	{};

	void operator==(const GeoTexturePlatformOptionsType& other)
	{
		m_Format = other.m_Format;
		m_MipLevels = other.m_MipLevels;
	}

	//PURPOSE:	Utility function that loads the platform specific type data from an XML tree.
	void LoadFromXML( parTreeNode& data );

	//PURPOSE:	Retrieves the texture output format (i.e. DXT1, DXT5 etc..)
	const char*	GetFormat() const { return m_Format.c_str(); }
	
	//PURPOSE:	Retrieves the number of mip-levels that should be created for the texture
	int			GetMipLevels() const { return m_MipLevels; }
	
private:
	std::string						m_Format;
	int								m_MipLevels;
};

//-----------------------------------------------------------------------------

//PURPOSE:	Utility class used by the GEO module for loading GeoTexturePlatformOptionsType objects
class GeoTexturePlatformOptionsTypeLoader : public GeoTypeLoaderBase
{
public:
	virtual GeoTypeBase* LoadType(GeoCoreManager& manager, const char* name, parTreeNode& data, GeoTypeLoaderContext *context = NULL);
};

//-----------------------------------------------------------------------------

//PURPOSE:	Class to represent the instance data for the platform specific texture options
class GeoTexturePlatformOptions : public GeoInstanceBase
{
friend class GeoTexturePlatformOptionsLoader;
friend class GeoTextureOptionsLoader;
friend class TextureOptionsManager;

public:
	GeoTexturePlatformOptions() {};

	//PURPOSE:	Utility function that loads the platform specific type data from an XML tree.
	void LoadFromXML( parTreeNode& data );

	//PURPOSE:	Retrieves the texture output format (i.e. DXT1, DXT5 etc..)
	const char*	GetFormat() const { return m_Type.GetFormat(); }

	//PURPOSE:	Retrieves the number of mip-levels that should be created for the texture
	int			GetMipLevels() const { return m_Type.GetMipLevels(); }

private:
	GeoTexturePlatformOptionsType	m_Type;
};

//-----------------------------------------------------------------------------

//PURPOSE:	Utility class used by the GEO module for loading GeoTexturePlatformOptions objects
class GeoTexturePlatformOptionsLoader : public GeoInstanceLoaderBase
{
public:
	virtual GeoInstanceBase* LoadInstance(GeoCoreManager& manager, const char* name, parTreeNode& data, GeoTypeLoaderContext *context = NULL);
};

//-----------------------------------------------------------------------------

//PURPOSE:	Class to hold the type data for the texture options
class GeoTextureOptionsType : public GeoTypeBase
{
public:
	enum MipSharpenFilter
	{
		MIPSHARP_SOFT,
		MIPSHARP_MEDIUM,
		MIPSHARP_HARD
	};

public:
	GeoTextureOptionsType() 
		: m_SharpenMipMaps(false)
		, m_SharpenMipFilter(MIPSHARP_MEDIUM)
		, m_FadeMipMaps(false)
		, m_FadeMipMapsAmount(0.15f)
		, m_Exposure(0.0f)
		, m_CalcHDRScaleAndBias(true) 
		, m_MaxWidth(0)
		, m_MaxHeight(0)
		, m_SetSRGBGamma(false)
	{
	}

	GeoTextureOptionsType(const GeoTextureOptionsType& other)
		: m_SharpenMipMaps(other.m_SharpenMipMaps)
		, m_SharpenMipFilter(other.m_SharpenMipFilter)
		, m_FadeMipMaps(other.m_FadeMipMaps)
		, m_FadeMipMapsAmount(other.m_FadeMipMapsAmount)
		, m_Exposure(other.m_Exposure)
		, m_CalcHDRScaleAndBias(other.m_CalcHDRScaleAndBias) 
		, m_MaxWidth(other.m_MaxWidth)
		, m_MaxHeight(other.m_MaxHeight)
		, m_SetSRGBGamma(other.m_SetSRGBGamma)
		, m_PC(other.m_PC)
		, m_Xbox360(other.m_Xbox360)
		, m_PS3(other.m_PS3)
	{};

	void operator==(const GeoTextureOptionsType& other)
	{
		m_SharpenMipMaps = other.m_SharpenMipMaps;
		m_SharpenMipFilter = other.m_SharpenMipFilter;
		m_FadeMipMaps = other.m_FadeMipMaps;
		m_FadeMipMapsAmount = other.m_FadeMipMapsAmount;
		m_Exposure = other.m_Exposure;
		m_CalcHDRScaleAndBias = other.m_CalcHDRScaleAndBias;
		m_MaxWidth = other.m_MaxWidth;
		m_MaxHeight = other.m_MaxHeight;
		m_SetSRGBGamma = other.m_SetSRGBGamma;
		m_PC = other.m_PC;
		m_Xbox360 = other.m_Xbox360;
		m_PS3 = other.m_PS3;
	}

	//PURPOSE:	Utility function that loads the platform specific type data from an XML tree.
	void LoadFromXML( parTreeNode& data );

	//PURPOSE: Retrieves the flag indicating if mip map sharpening should be performed
	bool GetSharpenMipMaps() const { return m_SharpenMipMaps; }

	//PURPOSE: Retrieves the filter to be used for sharpening mip maps
	MipSharpenFilter GetSharpenMipFilter() const { return m_SharpenMipFilter; }

	//PURPOSE: Retrieves the flag indicating if mip map fading should be performed
	bool GetFadeMipMaps() const { return m_FadeMipMaps; }

	//PURPOSE:	Retrieves the exposure value represented by the texture
	float GetFadeMipMapsAmount() const { return m_FadeMipMapsAmount; }

	//PURPOSE:	Retrieves the exposure value represented by the texture
	float GetExposure() const { return m_Exposure; }

	//PURPOSE:	Retrieves the value of a flag which indicates if the HDR scale and bias values should
	//			be computed for the texture
	bool GetCalcHDRScaleAndBias() const { return m_CalcHDRScaleAndBias; }

	//PURPOSE:	Retrieves the maximum texture width to allow for conversion
	//			A value of '0' indicates any size should be allowed
	int	GetMaxWidth() const { return m_MaxWidth; }

	//PURPOSE:	Retrieves the maximum texture height to allow for conversion
	//			A value of '0' indicates any size should be allowed
	int GetMaxHeight() const { return m_MaxHeight; }

	//PURPOSE:	Retrieves the flag which indicates whether the sRGB gamma value of 2.2 should
	//			be pushed into the DDS header
	bool GetSetSRGBGamma() const { return m_SetSRGBGamma; }

	//PURPOSE:	Retrieve the PC specific platform options
	const GeoTexturePlatformOptionsType&	GetPCOptions() const { return m_PC; }

	//PURPOSE:	Retrieve the XBox 360 specific platform options
	const GeoTexturePlatformOptionsType&	GetXBox360Options() const { return m_Xbox360; }

	//PURPOSE:	Retrieve the PS3 specific platform options
	const GeoTexturePlatformOptionsType&	GetPS3Options() const { return m_PS3; }

private:
	bool							m_SharpenMipMaps;
	MipSharpenFilter				m_SharpenMipFilter;

	bool							m_FadeMipMaps;
	float							m_FadeMipMapsAmount;

	float							m_Exposure;
	bool							m_CalcHDRScaleAndBias;

	int								m_MaxWidth;
	int								m_MaxHeight;

	bool							m_SetSRGBGamma;

	GeoTexturePlatformOptionsType	m_PC;
	GeoTexturePlatformOptionsType	m_Xbox360;
	GeoTexturePlatformOptionsType	m_PS3;
};

//-----------------------------------------------------------------------------

//PURPOSE:	Utility class used by the GEO module for loading GeoTextureOptionsType objects
class GeoTextureOptionsTypeLoader : public GeoTypeLoaderBase
{
public:
	virtual GeoTypeBase* LoadType(GeoCoreManager& manager, const char* name, parTreeNode& data, GeoTypeLoaderContext *context = NULL);
};

//-----------------------------------------------------------------------------

//PURPOSE:	Class to hold the instance data for the texture options
class GeoTextureOptions : public GeoInstanceBase
{
friend class TextureOptionsManager;
friend class GeoTextureOptionsLoader;
public:
	enum CubeMapFace
	{
		CUBEMAP_PX,
		CUBEMAP_NX,
		CUBEMAP_PY,
		CUBEMAP_NY,
		CUBEMAP_PZ,
		CUBEMAP_NZ
	};

public:
	GeoTextureOptions() {};
	virtual ~GeoTextureOptions() {};

	//PURPOSE:	Utility function that loads the platform specific type data from an XML tree.
	void LoadFromXML( parTreeNode& data );

	//PURPOSE:	Retrieve the name associated with the texture options
	const char* GetName() const { return m_Name.c_str(); }

	//PURPOSE:	Retrieve the PC specific platform options
	const GeoTexturePlatformOptions&	GetPCOptions() const { return m_PC; }

	//PURPOSE:	Retrieve the XBox 360 specific platform options
	const GeoTexturePlatformOptions&	GetXBox360Options() const { return m_Xbox360; }

	//PURPOSE:	Retrieve the PS3 specific platform options
	const GeoTexturePlatformOptions&	GetPS3Options() const { return m_PS3; }

	//PURPOSE: Retrieves the flag indicating if mip map sharpening should be performed
	bool GetSharpenMipMaps() const { return m_Type.GetSharpenMipMaps(); }

	//PURPOSE: Retrieves the filter to be used for sharpening mip maps
	GeoTextureOptionsType::MipSharpenFilter GetSharpenMipFilter() const { return m_Type.GetSharpenMipFilter(); }

	//PURPOSE: Retrieves the flag indicating if mip map fading should be performed
	bool GetFadeMipMaps() const { return m_Type.GetFadeMipMaps(); }

	//PURPOSE:	Retrieves the exposure value represented by the texture
	float GetFadeMipMapsAmount() const { return m_Type.GetFadeMipMapsAmount(); }

	//PURPOSE:	Retrieves the exposure value represented by the texture
	float GetExposure() const { return m_Type.GetExposure(); }

	//PURPOSE:	Retrieves the value of a flag which indicates if the HDR scale and bias values should
	//			be computed for the texture
	bool GetCalcHDRScaleAndBias() const { return m_Type.GetCalcHDRScaleAndBias(); }

	//PURPOSE:	Retrieves the maximum texture width to allow for conversion
	//			A value of '0' indicates any size should be allowed
	int	GetMaxWidth() const { return m_Type.GetMaxWidth(); }

	//PURPOSE:	Retrieves the maximum texture height to allow for conversion
	//			A value of '0' indicates any size should be allowed
	int GetMaxHeight() const { return m_Type.GetMaxHeight(); }

	//PURPOSE:	Retrieves the flag which indicates whether the sRGB gamma value of 2.2 should
	//			be pushed into the DDS header
	bool GetSetSRGBGamma() const { return m_Type.GetSetSRGBGamma(); }

	//PURPOSE:	Retrieves the number of exposure textures that are linked with this texture
	int	 GetNumExposureTextures() const { return (int)m_ExposureTextures.size(); }

	//PURPOSE:	Retrieves the full path to an exposure texture linked with this texture
	const char*	GetExposureTexture(int idx) const { Assert(idx < (int)m_ExposureTextures.size()); return m_ExposureTextures[idx].c_str(); }

	//PURPOSE:	Retrieves the number of user created mip map textures that are linked with this texture
	int GetNumMipTextures() const { return (int)m_MipTextures.size(); }

	//PURPOSE:	Retrieves the full path to a user created mip texture linked with this texture
	const char* GetMipTexture(int idx) const { Assert(idx < (int)m_MipTextures.size()); return m_MipTextures[idx].c_str(); }

	//PURPOSE:	Returns true if Cube-Map textures were specified in the options, false otherwise
	bool		HasCubeMapTextures() const;

	//PURPOSE:	Retrieves the full path to the cube map texture for the specified face.
	const char* GetCubeMapTexture(CubeMapFace face) const { return m_CubeMapTextures[(unsigned int)face].c_str(); }
	
private:
	GeoTextureOptionsType	m_Type;

	GeoTexturePlatformOptions	m_PC;
	GeoTexturePlatformOptions	m_Xbox360;
	GeoTexturePlatformOptions	m_PS3;

	std::vector< std::string >	m_ExposureTextures;
	std::vector< std::string >	m_MipTextures;

	std::string m_CubeMapTextures[6];

	std::string	m_Name;
};

//-----------------------------------------------------------------------------

//PURPOSE:	Utility class used by the GEO module for loading GeoTextureOptions objects
class GeoTextureOptionsLoader : public GeoInstanceLoaderBase
{
public:
	virtual GeoInstanceBase* LoadInstance(GeoCoreManager& manager, const char* name, parTreeNode& data, GeoTypeLoaderContext *context = NULL);
};

//-----------------------------------------------------------------------------

//PURPOSE:	Utility class used by the GEO module for managing loaded texture options instance objects
class GeoTextureOptionsInstanceManager : public GeoInstanceManagerBase
{
public:
	virtual ~GeoTextureOptionsInstanceManager();
	virtual void AddInstance(GeoInstanceBase&);

	GeoTextureOptions*	GetInstance(const std::string& name);

private:
	typedef std::map< std::string, GeoTextureOptions* > INSTANCE_TABLE;
	INSTANCE_TABLE m_Instances;
};

//-----------------------------------------------------------------------------

//PURPOSE:	Singleton class used to access the texture options files and data.
class TextureOptionsManager
{
public:
	~TextureOptionsManager() 
	{
		if(m_pDefaultOptionsInstance) delete m_pDefaultOptionsInstance;
	};

	//PURPOSE:	Sets the directory where the texture options template file is located
	void SetTextureGeoTemplateDirectory(const char* path) { m_geoCoreMananger.SetGEOTemplatePath(path); }

	//PURPOSE:	Sets the directory where the texture options type file is located
	void SetTextureGeoTypeDirectory(const char* path) { m_geoCoreMananger.SetGeoTypePath(path); }

	//PURPOSE:	Loads a texture options file
	//RETURNS:	A pointer to the texture options.  The memory for the returned object is manged by the GeoTextureOptionsInstanceManaged
	//			and does not need to be freed by the caller.
	GeoTextureOptions*	LoadOptionsFile(const char* path);

	//PURPOSE:	Retrieves the default texture options as speicifed byt the texture options type file.
	//RETURNS:	A pointer to the texture options.  The memory for the returned object is manged by the GeoTextureOptionsInstanceManaged
	//			and does not need to be freed by the caller.
	GeoTextureOptions*	GetDefaultOptions();

	//PURPOSE:	Utility method which when supplied with the path to a teture file will fill a buffer with a path
	//			to the associated options file.
	static void TexturePathToOptionsFilePath(char* outPath, int outBufLen, const char* texturePath);

protected:
	TextureOptionsManager();

private:
	GeoCoreManager					m_geoCoreMananger;
	GeoTextureOptionsTypeLoader		m_textureOptionsTypeLoader;
	GeoTextureOptionsLoader			m_textureOptionsLoader;

	GeoTextureOptionsInstanceManager	m_textureInstanceManager;

	GeoTextureOptions*					m_pDefaultOptionsInstance;
};


typedef atSingleton<TextureOptionsManager> textureOptionsManagerSingleton;

//Singleton accessors for the TextureOptionsManager
#define TEXTUREOPTIONS				::textureOptionsManagerSingleton::InstanceRef()
#define INIT_TEXTUREOPTIONS			::textureOptionsManagerSingleton::Instantiate()
#define SHUTDOWN_TEXTUREOPTIONS		::textureOptionsManagerSingleton::Destroy()

//-----------------------------------------------------------------------------

#endif //__RAGE_TEXTUREGEOLIB_H__

