using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace docsamples
{
	class StepDesc : System.Object
	{
		
		public int			StepNum;
		public String		Desc;
		public ArrayList	StepBody = new ArrayList();

		public override String ToString() {return Convert.ToString(StepNum);}
	};

	/// <summary>
	/// Summary description for DocSample.
	/// </summary>
	class DocSample
	{
		enum EnumState
		{
			FIND_TITLE,
			FIND_PURPOSE,
			FIND_STEP,
			IN_STEP,
			IN_CODE
		};

		static String		Title;						// sample title
		static ArrayList	Purpose= new ArrayList();	// will be array of strings
		static ArrayList	Steps= new ArrayList();		// will be array of StepDesc
		static int			StepNum;					// current Step Num

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if (args.Length<1)
			{
				PrintUsage();
				return;
			}
	
			ParseInput(args[0]);
			OutputDtx();
		}

		static void ParseInput(String fileName)
		{
			EnumState state = EnumState.FIND_TITLE;

			using (StreamReader sr = new StreamReader(fileName)) 
			{
				String line = null;
				Regex rExpCommentStart= new Regex(@"\A\s*//");
				StepDesc currentStep=null;
				bool useLineContents=false;
				bool waitingToCloseCodeTag=false;
		
				while ((useLineContents && line!=null) || (line = sr.ReadLine()) != null) 
				{
					useLineContents=false;

					line.Trim();

					if (state==EnumState.FIND_TITLE)
					{
						Regex rExpTitle=new Regex(@"\A\s*//\s*TITLE:");

						if (rExpTitle.Match(line).Success==true)
						{
							Title =rExpTitle.Replace(line,"",1).Trim();
							state = EnumState.FIND_PURPOSE;
						}
					}
					else if (state==EnumState.FIND_PURPOSE)
					{	
						Regex rExpPurpose=new Regex(@"\A\s*//\s*PURPOSE:");

						if (rExpPurpose.Match(line).Success==true)
						{
							Purpose.Add(rExpPurpose.Replace(line,"",1).Trim());

							// read rest of purpose:
							while ((line = sr.ReadLine()) != null)
							{
								if (rExpCommentStart.Match(line).Success==true)
								{
									// add rest of purpose comment:
									Purpose.Add(rExpCommentStart.Replace(line,"",1).Trim());
								}
								else 
									break;
							}
							
							// switch to next step:
							state=EnumState.FIND_STEP;
						}
					} 
					else 
					{
						// "// STEP #<num.":
						Regex rExpStep= new Regex(@"\A\s*//\s*STEP\s*#\s*(?<Num>[0-9]+)\.");

						// "// -- ":
						Regex rExpDashes=new Regex(@"\A\s*//\s*--\s*");

						Regex rExpStop=new Regex(@"\A\s*//\s*-STOP\s*");

						// Replaces \ with \\, unless the \ is being used to escape a / in an XML close tag "<\/something>"
						Regex rExpBackslashEscape = new Regex(@"\\(?!/)");

						// find the step #:
						if (rExpStep.Match(line).Success)
						{
							currentStep=new StepDesc();
							Steps.Add(currentStep);

							currentStep.StepNum=++StepNum;

							// get step description:
							currentStep.Desc=rExpStep.Replace(line,"",1).Trim();
							state=EnumState.IN_STEP;
						}
						else if (state==EnumState.IN_STEP)	
						{
							while (line!=null && rExpDashes.Match(line).Success)
							{
								currentStep.StepBody.Add(rExpDashes.Replace(line,"",1).Trim());

								// read rest of subsection comment:
								useLineContents=false;
								bool foundEndDelim=false;
								while ((useLineContents && line!=null) || ((line = sr.ReadLine()) != null))
								{
									line.Trim();

									// see if we've found a new step:
									if (rExpStep.Match(line).Success)
									{
										if (state==EnumState.IN_CODE)
										{
											if (waitingToCloseCodeTag)
												currentStep.StepBody.Add("</CODE>\n");
											waitingToCloseCodeTag=false;
										}

										state=EnumState.IN_STEP;
										useLineContents=true;
										break;
									}
									else if (state==EnumState.IN_STEP)
									{
										if (rExpCommentStart.Match(line).Success)
										{
											// add rest of purpose comment:
											currentStep.StepBody.Add(rExpCommentStart.Replace(line,"",1).Trim());
										}
										else 
										{
											state=EnumState.IN_CODE;
											if (rExpStop.Match(line).Success)
											{
												foundEndDelim=true;
											}
											else if (foundEndDelim==false)
											{
												currentStep.StepBody.Add("<CODE>\n");
												waitingToCloseCodeTag=true;

												line = rExpBackslashEscape.Replace(line,@"\\");
											
												currentStep.StepBody.Add(line);
											}
										}
									}
									else if (state==EnumState.IN_CODE)
									{
										// found a new step or found next substep:
										if (rExpStep.Match(line).Success || rExpDashes.Match(line).Success)
										{
											state=EnumState.IN_STEP;
											if (waitingToCloseCodeTag)
												currentStep.StepBody.Add("</CODE>");
											waitingToCloseCodeTag=false;
											useLineContents=true;
											break;
										}
										else if (rExpStop.Match(line).Success)
										{
											// we found the stop token:
											if (waitingToCloseCodeTag)
												currentStep.StepBody.Add("</CODE>");
											waitingToCloseCodeTag=false;
											foundEndDelim=true;
											useLineContents=false;
										}
										else if (!foundEndDelim) 
										{
											line = rExpBackslashEscape.Replace(line,@"\\");
											currentStep.StepBody.Add(line);
										}
									}
								}
							}
						}
					}
				}

				if (waitingToCloseCodeTag==true)
				{
					currentStep.StepBody.Add("</CODE>");
				}
			}
		}

		static void OutputDtx()
		{
			Console.WriteLine("@@" + Title + " Sample");
			Console.WriteLine("<GROUP Working Examples>");
			

			// write out purpose:
			foreach (string str in Purpose)
			{
				Console.WriteLine(str);
			}

			//Console.WriteLine("* " + Title + " Sample *");
			if (Steps.Count>0)
			{
				Console.WriteLine("The sample is broken into several steps:");
				
				// write out list of steps:
				int i=1;
				foreach (StepDesc step in Steps)
				{
					Console.WriteLine(i++ + ". " + step.Desc);
				}				

				// write out steps:
				foreach (StepDesc step in Steps)
				{
					Console.WriteLine();
					Console.WriteLine("* " + step.Desc + " *");
					foreach (string str in step.StepBody)
					{
						Console.WriteLine(str);
					}
				}
			}
		}

		static void PrintUsage()
		{
			Console.WriteLine("usage: docsamples.exe <.cpp input file>");
		}
	}
}
