//------------------------------------------------------------------------------
// $Workfile: Rasterizer.c  $
//   Created: Clemens Pecinovsky
// Copyright: 2005, Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
//------------------------------------------------------------------------------
// Description: 
// This file defines a general rasterizer which is used in all rage mr shaders.
// -----------------------------------------------------------------------------

#include "Rasterizer.h"

#include <stdlib.h> //for min and max macros.

//-----------------------------------------------------------------------------
void combineVectors( miVector *res, 
                    miVector const *a, 
                    miVector const *b,
                    miVector const	*c, 
                    miVector const *bary)
//-----------------------------------------------------------------------------
{
    res->x = bary->x * a->x + bary->y * b->x + bary->z * c->x;
    res->y = bary->x * a->y + bary->y * b->y + bary->z * c->y;
    res->z = bary->x * a->z + bary->y * b->z + bary->z * c->z;
}


//-----------------------------------------------------------------------------
void baryFixup(miVector *bary)
//-----------------------------------------------------------------------------
{
    /*
    Correct barycentric coordinates by projecting them to the
    barycentric plane.  The plane equation is (P-u)*n = 0, where
    'u' is e.g. (1 0 0) and 'n' is the plane normal (1 1 1).
    We seek a scalar s so that
    (B-sn-u)*n = 0 => s = ((u-B)*n) / (n*n)
    and then add s*n to B.
    We then clip the barycentric coordinates and as a final touch,
    compute z as a function of x and y since they are not independent.
    This means that we can leave z out of the projection and
    clipping phase
    */
    float		s;

    s = (1.0f - bary->x - bary->y - bary->z)/3.0f;
    bary->x += s;
    bary->y += s;

    /* now clip coordinates */
    if (bary->x < 0.0f)
    {
        bary->x = 0.0f;
    }
    else if (bary->x > 1.0f)
    {
        bary->x = 1.0f;
    }

    if (bary->y < 0.0f)
    {
        bary->y = 0.0f;
    }
    else if (bary->y + bary->x > 1.0f)
    {
        bary->y = 1.0f-bary->x;
    }

    /* Finally, compute the dependent z */
    bary->z = 1.0f - bary->x - bary->y;
}


//-----------------------------------------------------------------------------
int computeOutCode(int xFrame, int yFrame, float x, float y)
//-----------------------------------------------------------------------------
{
    //based on the idea of the cohan sutterland line clipping algorithm
    //this helper function returns a code for the descrete segment the 
    //given point is located.
    //
    //  9 | 1 | 5
    // -----------
    //  8 | 0 | 4
    // -----------
    // 10 | 2 | 6
    //

    int ret=0;
    if (y < yFrame)
    {
        ret +=1;
    }
    else if (y > yFrame+1)
    {
        ret +=2;
    }

    if (x > xFrame+1) 
    {
        ret +=4;
    }
    else if (x < xFrame)
    {
        ret +=8;
    }
    return ret;
}


//-----------------------------------------------------------------------------
int goesThrough(int x, int y, float x1, float y1, float x2, float y2, float x3, float y3)
//-----------------------------------------------------------------------------
{
    //based on the idea of the cohan sutterland line clipping algorithm
    //this helper function evaluates if one of the lines goes through the pixel (x,y) 
    //this is done by doing a logical and of the OutCode. 

    int code1=computeOutCode(x,y, x1,y1);
    int code2=computeOutCode(x,y, x2,y2);
    int code3=computeOutCode(x,y, x3,y3);
    return ( ((code1 & code2)==0) ||
        ((code1 & code3)==0) ||
        ((code2 & code3)==0) );
}


//-----------------------------------------------------------------------------
float lineEquation(int x, int y, float x1, float y1, float x2, float y2)
//-----------------------------------------------------------------------------
{
    //this implements the line equation. 
    //it is used to query if a point (x,y) is on the left or on the right side 
    //The result is negative if the point lies on the left side of the line
    //otherwhise it is on the right. if the result is 0 the point is exactly 
    //on the line.
    //note that the line is defined by the two points in 2d space, but a line
    //has no begin and no end.
    return (x2 - x1) * (y - y1) - (y2 - y1) * (x - x1);
}


//-----------------------------------------------------------------------------
void doRasterization(VertexDefinition const *a,
                     VertexDefinition const *b,
                     VertexDefinition const *c,
                     float width, float height,
                     RasterizationPixelCallback pixelCallback,
                     void *userData)
//-----------------------------------------------------------------------------
{
    //this function does the rasterization and calls the callback function
    //for each pixel found.

    float x1 = (a->tex.x * (width+1));
    float x2 = (b->tex.x * (width+1));
    float x3 = (c->tex.x * (width+1));
    float y1 = (a->tex.y * (height+1));
    float y2 = (b->tex.y * (height+1));
    float y3 = (c->tex.y * (height+1));

    // Bounding rectangle
    int minx = (int)max(0,floor(min(min(x1, x2), x3)));
    int maxx = (int)min(width,ceil(max(max(x1, x2), x3)));
    int miny = (int)max(0,floor(min(min(y1, y2), y3)));
    int maxy = (int)min(height,ceil(max(max(y1, y2), y3)));
    int x,y;

    // Scan through bounding rectangle
    for(y = miny; y <= maxy; y++)
    {
        for(x = minx; x <= maxx; x++)
        {
            float result1Line1 = lineEquation(x,y, x1,y1, x2,y2);
            float result1Line2 = lineEquation(x,y, x2,y2, x3,y3);
            float result1Line3 = lineEquation(x,y, x3,y3, x1,y1);

            float result2Line1 = lineEquation(x+1,y, x1,y1, x2,y2);
            float result2Line2 = lineEquation(x+1,y, x2,y2, x3,y3);
            float result2Line3 = lineEquation(x+1,y, x3,y3, x1,y1);

            float result3Line1 = lineEquation(x,y+1, x1,y1, x2,y2);
            float result3Line2 = lineEquation(x,y+1, x2,y2, x3,y3);
            float result3Line3 = lineEquation(x,y+1, x3,y3, x1,y1);

            float result4Line1 = lineEquation(x+1,y+1, x1,y1, x2,y2);
            float result4Line2 = lineEquation(x+1,y+1, x2,y2, x3,y3);
            float result4Line3 = lineEquation(x+1,y+1, x3,y3, x1,y1);

            int resultsLine1GreatherZero=0;
            int resultsLine2GreatherZero=0;
            int resultsLine3GreatherZero=0;

            if (result1Line1>0) resultsLine1GreatherZero++;
            if (result2Line1>0) resultsLine1GreatherZero++;
            if (result3Line1>0) resultsLine1GreatherZero++;
            if (result4Line1>0) resultsLine1GreatherZero++;

            if (result1Line2>0) resultsLine2GreatherZero++;
            if (result2Line2>0) resultsLine2GreatherZero++;
            if (result3Line2>0) resultsLine2GreatherZero++;
            if (result4Line2>0) resultsLine2GreatherZero++;

            if (result1Line3>0) resultsLine3GreatherZero++;
            if (result2Line3>0) resultsLine3GreatherZero++;
            if (result3Line3>0) resultsLine3GreatherZero++;
            if (result4Line3>0) resultsLine3GreatherZero++;

            if ( (result1Line1>0) && (result1Line2>0) && (result1Line3>0))
            {
                pixelCallback(x, y, width, height, a, b, c, userData);
            }
            else if ( (result2Line1>0) && (result2Line2>0) && (result2Line3>0))
            {
                pixelCallback(x, y, width, height, a, b, c, userData);
            }
            else if ( (result3Line1>0) && (result3Line2>0) && (result3Line3>0))
            {
                pixelCallback(x, y, width, height, a, b, c, userData);
            }
            else if ( (result4Line1>0) && (result4Line2>0) && (result4Line3>0))
            {
                pixelCallback(x, y, width, height, a, b, c, userData);
            }
            else
            {
                int linesInside=0;
                if ((resultsLine1GreatherZero>0) && (resultsLine1GreatherZero<4))
                {
                    linesInside++;
                }
                if ((resultsLine2GreatherZero>0) && (resultsLine2GreatherZero<4))
                {
                    linesInside++;
                }
                if ((resultsLine3GreatherZero>0) && (resultsLine3GreatherZero<4))
                {
                    linesInside++;
                }
                if ((linesInside>1) && goesThrough(x,y, x1,y1, x2,y2, x3,y3))
                {
                    pixelCallback(x, y, width, height, a, b, c, userData);
                }
            }
        }
    }
}


//-----------------------------------------------------------------------------
void doRasterizationForTriangles(miRclm_mesh_render const *arg, 
                                 char* logInfo,
                                 float width, float height,
                                 RasterizationTriangleCallback triangleCallback,
                                 RasterizationPixelCallback pixelCallback,
                                 void *userData)
//-----------------------------------------------------------------------------
{
    int i;
    VertexDefinition const	*resdata;
    resdata = (VertexDefinition const *)arg->vertex_data;

    for (i=0; i < arg->no_triangles; i++)
    {
        if (logInfo!=NULL)
        {
            mi_progress("%s (for triangle %d / %d)",logInfo, i, arg->no_triangles );
        }
        
        if (triangleCallback!=NULL)
        {
            if (triangleCallback(arg->pri, 
                        arg->triangles[i].pri_idx,
                        width, height,
                        &resdata[arg->triangles[i].a],
                        &resdata[arg->triangles[i].b],
                        &resdata[arg->triangles[i].c],
                        userData) )
            {
                doRasterization(&resdata[arg->triangles[i].a],
                    &resdata[arg->triangles[i].b],
                    &resdata[arg->triangles[i].c],
                    width, height, pixelCallback, userData);
            }
        }
        else        
        {
            doRasterization(&resdata[arg->triangles[i].a],
                            &resdata[arg->triangles[i].b],
                            &resdata[arg->triangles[i].c],
                            width, height, pixelCallback, userData);
        }
    }
}
