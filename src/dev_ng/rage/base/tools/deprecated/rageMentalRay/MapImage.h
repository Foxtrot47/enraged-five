//------------------------------------------------------------------------------
// $Workfile: MapImage.h  $
//   Created: Clemens Pecinovsky
// Copyright: 2005, Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
//------------------------------------------------------------------------------
// Description: 
// This file declares functions for reading and writing mr map image files.


#ifndef RSV_MR_MAPIMAGE_H
#define RSV_MR_MAPIMAGE_H

#include <stdio.h>

class MapImage
{
public: 
    MapImage(const char* readFromFile);
    MapImage(int width, int height);

    ~MapImage();

    bool writeToFile(const char* fileName);

    void getPixel (int x, int y, float &red, float &green, float &blue, float &alpha);
    void putPixel (int x, int y, float red, float green, float blue, float alpha);

    int getHeight();
    int getWidth();

    void copyBuffer(float* toBuffer);
    static bool isMapFile(const char* fileName);

private:

    bool allocBuffer();
    bool readPixels(FILE *in);
    bool readHeader(FILE *in);

    bool writePixels(FILE *out);
    bool writeHeader(FILE *out);

    int mWidth;
    int mHeight;

    float* mBuffer;
};


#endif //RSV_MR_MAPIMAGE_H
