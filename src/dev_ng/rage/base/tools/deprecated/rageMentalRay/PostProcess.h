//------------------------------------------------------------------------------
// $Workfile: PostProcess.h  $
//   Created: Clemens Pecinovsky
// Copyright: 2005, Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
//------------------------------------------------------------------------------
// Description: 
// This file defines the structures used for the mr lightmap shader.


#ifndef RSV_MR_POSTPROCESS_H
#define RSV_MR_POSTPROCESS_H

class MapImage;

void fillTextureSeamsOneTexel(MapImage &image);

void fillTextureSeams(MapImage &image,               
                      int numTexelsToFill);

bool imageDownSample(MapImage &outImage,
                          MapImage &inImage,
                          double gaussSigma,
                          double filterSize);

void imageDownSampleWithFilter(MapImage &outImage,
                               MapImage &inImage,
                               float *filter,
                               int filterSize);

void getBoxFilterMatrix(float* filterMatrix, int filterSize);
void getGaussFilterMatrix(float* filterMatrix, int filterSize, double sigma);


#endif //RSV_MR_POSTPROCESS_H
