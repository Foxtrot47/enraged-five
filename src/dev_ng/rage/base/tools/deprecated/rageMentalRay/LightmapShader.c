//------------------------------------------------------------------------------
// $Workfile: LightmapShader.c  $
//   Created: Clemens Pecinovsky
// Copyright: 2005, Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
//------------------------------------------------------------------------------
// Description: 
// This file implements the rage mr lightmap shader.


#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>

#include "Rasterizer.h"
#include "LightmapShader.h"

//-----------------------------------------------------------------------------
DLLEXPORT int rageLightmapWrite_version(void) 
//-----------------------------------------------------------------------------
{
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);
    return 1;
}


//-----------------------------------------------------------------------------
DLLEXPORT void rageLightmapWrite_init(miState         *state,
                             rage_lightmap_write_param *param,
                             miBoolean       *inst_req)
//-----------------------------------------------------------------------------
{
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);
    if (!param)
    {
        //init call
        *inst_req = miTRUE;
    }
    else 
    {
        miBoolean status = miTRUE;
        miImg_file outImagefp;
        char* fileName = NULL;
        miTag fileNameTag;

        //call per instance
        miLock **lock;
        mi_query(miQ_FUNC_USERPTR, state, 0, &lock);
        (*lock)= (miLock*)mi_mem_allocate(sizeof(miLock));

        status = mi_init_lock(*lock);
        if (status==miFALSE)
        {
            mi_mem_release(*lock);
            *lock=NULL;
        }
        else
        {
            //initialize the file...
            fileNameTag = *mi_eval_tag(&param->fileNameTag);
            if (fileNameTag != miNULLTAG)
            {
                char *fileExtension=NULL;
                int width = *mi_eval_integer(&param->width);
                int height = *mi_eval_integer(&param->height);
                miImg_format fileFormat = miIMG_FORMAT_TIFF;
                fileName = (char *) mi_db_access(fileNameTag);
                fileExtension = strrchr(fileName,'.');
                if (fileExtension!=NULL)
                {
                    //no file extension, or unknown - so use TIFF
                    fileFormat = mi_img_format_identify(fileExtension+1);
                    if (fileFormat==miIMG_FORMAT_ERROR)
                    {
                        fileFormat = miIMG_FORMAT_TIFF;
                    }
                }
                memset(&outImagefp, 0, sizeof(outImagefp));
                outImagefp.type	= miIMG_TYPE_RGBA_FP;
                outImagefp.ftype = miIMG_TYPE_RGBA_FP;

                if (mi_img_create(&outImagefp, fileName,
                    miIMG_TYPE_RGBA_FP, fileFormat,
                    width, height))
                {
                    mi_img_close(&outImagefp);
                }
            }
        }
        if (fileName!=NULL)
        {
            mi_db_unpin(fileNameTag);
        }

    }

}


//-----------------------------------------------------------------------------
DLLEXPORT void rageLightmapWrite_exit(miState         *state,
                                    rage_lightmap_write_param *param)
//-----------------------------------------------------------------------------
{
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);
    if (param)
    {
        miLock **lock;
        mi_query(miQ_FUNC_USERPTR, state, 0, &lock);
        if (*lock!=NULL)
        {
            mi_delete_lock(*lock);
            mi_mem_release(*lock);
            *lock=NULL;
        }
    }
    return;
}


//-----------------------------------------------------------------------------
DLLEXPORT miBoolean rageLightmapWrite( VertexDefinition *result,
                                    miState *state,
                                    rage_lightmap_write_param *param,
                                    miRclm_mesh_render const *arg)
//-----------------------------------------------------------------------------
{
    LightmapData lightmapData;
    miImg_file outImagefp;
    miImg_file inImagefp;
    int numFgPoints=0;
    int maxNumFgPoints=0;
    int sampleNumber=0;
    char* fileName = NULL;
    miTag fileNameTag;
    miBoolean status = miTRUE;

//	mi_info("Line : %d File : %s", __LINE__, __FILE__);
    

    switch (state->type) 
    {
    case miRAY_LM_VERTEX:
        {
            /* Gathering vertex data */
            result->point  = state->point;
            result->normal = state->normal;
            result->tex = state->tex_list[1];
            mi_vector_normalize(&result->normal);
        }        
        break;

    case miRAY_LM_MESH:
        {
            miLock **lock;
            mi_query(miQ_FUNC_USERPTR, state, 0, &lock);
            mi_lock(**lock);

            //fill out the callback parameter data structure.
            lightmapData.state = state;
            lightmapData.fgSampleNumber=0;
            lightmapData.fgSamplePointNoise[0]=0;
            lightmapData.fgSamplePointNoise[1]=0;
            lightmapData.ambientOcclusionSamples=NULL;
            lightmapData.img=NULL;

            if (!arg)
            {
                status = miFALSE;
            }                       
            if (status)
            {
                
                fileNameTag = *mi_eval_tag(&param->fileNameTag);
                if (fileNameTag == miNULLTAG)
                {
                    status = miFALSE;
                }
                else
                {

                    char *fileExtension=NULL;
                    int width = *mi_eval_integer(&param->width);
                    int height = *mi_eval_integer(&param->height);
                    miImg_format fileFormat = miIMG_FORMAT_TIFF;
                    fileName = (char *) mi_db_access(fileNameTag);
                    fileExtension = strrchr(fileName,'.');
                    if (fileExtension!=NULL)
                    {
                        //no file extension, or unknown - so use TIFF
                        fileFormat = mi_img_format_identify(fileExtension+1);
                        if (fileFormat==miIMG_FORMAT_ERROR)
                        {
                            fileFormat = miIMG_FORMAT_TIFF;
                        }
                    }


                    memset(&inImagefp, 0, sizeof(inImagefp));
                    inImagefp.type	= miIMG_TYPE_RGBA_FP;
                    inImagefp.ftype = miIMG_TYPE_RGBA_FP;
                    
                    if (!mi_img_open(&inImagefp, fileName, miIMG_TYPE_RGBA_FP))
                    {
                         status = miFALSE;
                    }
                    else
                    {
                        miImg_image *img=NULL;
                        img = mi_img_image_alloc(&inImagefp);
                        if (img != NULL)
                        {
                            mi_img_image_read(&inImagefp, img );
                            mi_img_close(&inImagefp);
                            memset(&outImagefp, 0, sizeof(outImagefp));
                            outImagefp.type	= miIMG_TYPE_RGBA_FP;
                            outImagefp.ftype = miIMG_TYPE_RGBA_FP;

                            if (!mi_img_create(&outImagefp, fileName,
                                miIMG_TYPE_RGBA_FP, fileFormat,
                                width, height))
                            {
                                status = miFALSE;
                            }
                            else
                            {
                                lightmapData.img = mi_img_image_alloc(&outImagefp); 
                                if (lightmapData.img == NULL)
                                {
                                    status = miFALSE;
                                }
                                else
                                {
                                    int x,y;
                                    for (y=0;y<img->height;y++)
                                    {
                                        for (x=0;x<img->width;x++)
                                        {
                                            miColor color;
                                            mi_img_get_color(img, &color, x, y);
                                            mi_img_put_color(lightmapData.img, &color, x, y);
                                        }
                                    }

                                }
                            }
                            mi_img_image_release(img);                            
                        }
                           
                    }

                }
            }

            // image file is ready for processing
            if (status)
            {
                lightmapData.fgSamplePerTexel = *mi_eval_scalar(&param->fgSamplePerTexel);
                lightmapData.fgNoiseSize= *mi_eval_scalar(&param->fgNoiseSize);

                lightmapData.ambientOcclusionSamplesPerTexel   = *mi_eval_scalar(&param->ambientOcclusionSamplesPerTexel);
                lightmapData.ambientOcclusionSamplesPerPoint   = *mi_eval_integer(&param->ambientOcclusionSamplesPerPoint);
                lightmapData.ambientOcclusionBright = *mi_eval_color(&param->ambientOcclusionBright);
                lightmapData.ambientOcclusionDark= *mi_eval_color(&param->ambientOcclusionDark);
                lightmapData.ambientOcclusionSpread  = *mi_eval_scalar(&param->ambientOcclusionSpread);
                lightmapData.ambientOcclusionMaxDistance  = *mi_eval_scalar(&param->ambientOcclusionMaxDistance);

                // get the number of scene lights
                mi_query(miQ_NUM_GLOBAL_LIGHTS, state, miNULLTAG, &lightmapData.n_l);

                // if there are any, get an array of light tags
                if (lightmapData.n_l > 0)
                {
                    mi_query(miQ_GLOBAL_LIGHTS, state, miNULLTAG, &lightmapData.light);
                }
                else
                {
                    lightmapData.light = miNULLTAG;
                }

				
                if (status && (!calculateFinalGatherPoints(&lightmapData, arg)))
                {
                    status = miFALSE;
                }

                //do ambientOcclusionValues
                if ( status && 
                    (lightmapData.ambientOcclusionSamplesPerTexel>0) && 
                    (lightmapData.ambientOcclusionSamplesPerPoint>0) )
                {
                    int maxNumAOPoints;
                    int i;
                    lightmapData.numAOPointsWidth = (int) (lightmapData.img->width * lightmapData.ambientOcclusionSamplesPerTexel);
                    lightmapData.numAOPointsHeight = (int) (lightmapData.img->width * lightmapData.ambientOcclusionSamplesPerTexel);
                    maxNumAOPoints = lightmapData.numAOPointsWidth * lightmapData.numAOPointsHeight;

                    lightmapData.ambientOcclusionSamples = (float*)mi_mem_allocate(maxNumAOPoints * sizeof(float));
                    if (lightmapData.ambientOcclusionSamples==NULL)
                    {
                        mi_fatal("out of memory. could not get %d bytes of memory",maxNumAOPoints * sizeof(FgPoint));
                        return (miFALSE);
                    }
                    for (i=0;i<maxNumAOPoints;i++)
                    {
                        lightmapData.ambientOcclusionSamples[i] = -1.0f;
                    }
                    doRasterizationForTriangles(arg, 
                        "doing ambientOcclusion calculation",
                        (float)lightmapData.numAOPointsWidth-1,
                        (float)lightmapData.numAOPointsHeight-1,
                        triangleCallback,
                        ambientOcclusionPixelCallback,
                        &lightmapData);
                }

                // main light calculation - done always
                if (status)
                {
                    doRasterizationForTriangles(arg, 
                        "doing light calculation",
                        (float)lightmapData.img->width-1,
                        (float)lightmapData.img->height-1,
                        triangleCallback,
                        lightPixelCallback,
                        &lightmapData);
                }
            }

            //free the allocations.
            if (lightmapData.ambientOcclusionSamples!=NULL)
            {
                mi_mem_release(lightmapData.ambientOcclusionSamples);
				lightmapData.ambientOcclusionSamples=NULL;
            }
            if (fileName!=NULL)
            {
                mi_db_unpin(fileNameTag);
            }
            if (lightmapData.img != NULL)
            {
                status = mi_img_image_write(&outImagefp, lightmapData.img);
                mi_img_image_release(lightmapData.img);
            }
            mi_img_close(&outImagefp);

            mi_unlock(**lock);
        }
        break;
    }
    return status;
}


//-----------------------------------------------------------------------------
int triangleCallback(void *pri,
                miGeoIndex pri_idx,
                float width,
                float height,
                const VertexDefinition *a,
                const VertexDefinition *b,
                const VertexDefinition *c,
                void *userdata)
//-----------------------------------------------------------------------------
{
    LightmapData *lightmapData=(LightmapData *) userdata;
    miVector			d1, d2;
    miState *state = lightmapData->state;
    miMatrix tmp1, tmp2;

    float x1 = (a->tex.x * width);
    float x2 = (b->tex.x * width);
    float x3 = (c->tex.x * width);
    float y1 = (a->tex.y * height);
    float y2 = (b->tex.y * height);
    float y3 = (c->tex.y * height);

//	mi_info("Line : %d File : %s", __LINE__, __FILE__);

    // pixel to barycentric coordinate transform. This is a 2D homogeneous
    // problem (to allow for translation) so the third component is set to
    // 1 and we have a 3-by-3 matrix equation.
    mi_matrix_ident(tmp1);
    tmp1[ 0] = x1;
    tmp1[ 4] = x2;
    tmp1[ 8] = x3;
    tmp1[ 1] = y1;
    tmp1[ 5] = y2;
    tmp1[ 9] = y3;
    tmp1[ 2] = 1.0f;
    tmp1[ 6] = 1.0f;
    tmp1[10] = 1.0f;
    mi_matrix_ident(tmp2);	// corresponds to barycentric vectors

    // solve pix * pix_to_space = bary
    if (!mi_matrix_solve(lightmapData->pixelToBary, tmp1, tmp2, 4))
    {
        return miFALSE;
    }

    /* compute geometric normal of the triangle */
    mi_vector_sub(&d1, &b->point, &a->point);
    mi_vector_sub(&d2, &c->point, &a->point);
    mi_vector_prod(&state->normal_geom, &d1, &d2);
    mi_vector_normalize(&state->normal_geom);

    /* set up primitive */
    state->pri     = pri;
    state->pri_idx = pri_idx;

    return miTRUE;
}


//-----------------------------------------------------------------------------
void fillState(miState *state,
               const miVector *bary,
               const VertexDefinition *a,
               const VertexDefinition *b,
               const VertexDefinition *c)
//-----------------------------------------------------------------------------
{
    int j;
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);

    // pixel center is inside triangle 
    combineVectors(&state->point,
                &a->point, &b->point, &c->point,
                bary);

    combineVectors(&state->normal,
                &a->normal, &b->normal, &c->normal,
                bary);
    mi_vector_normalize(&state->normal);            

    // set state->dir to directly incoming
    state->dir.x = -state->normal.x;
    state->dir.y = -state->normal.y;
    state->dir.z = -state->normal.z;
    state->dot_nd = 1.0f;

    // Fill out state->tex_list
    for(j=0; ; j++) 
    {
        miVector *ta, *tb, *tc;
        if (!mi_tri_vectors(state, 't', j, &ta, &tb, &tc))
            break;
        combineVectors(&state->tex_list[j], ta, tb, tc, bary);
    }

    state->bary[0]=bary->x;
    state->bary[1]=bary->y;
    state->bary[2]=bary->z;
    state->bary[3]=0;

    if (state->options->shadow && state->type != miRAY_SHADOW ) 
    {
        double d_shadow_tol, tmp_d;

        d_shadow_tol = mi_vector_dot_d(&state->normal, &a->point);
        tmp_d = mi_vector_dot_d(&state->normal, &b->point);
        if (d_shadow_tol < tmp_d)
        {
            d_shadow_tol = tmp_d;
        }
        tmp_d = mi_vector_dot_d(&state->normal, &c->point);
        if (d_shadow_tol < tmp_d)
        {
            d_shadow_tol = tmp_d;
        }
        state->shadow_tol = d_shadow_tol - mi_vector_dot_d(&state->normal, &state->point);
    }
}

//-----------------------------------------------------------------------------
void fgPixelCallback(int x, 
                int y,
                float width, 
                float height, 
                const VertexDefinition *a,
                const VertexDefinition *b,
                const VertexDefinition *c,
                void *userdata)
//-----------------------------------------------------------------------------
{
    LightmapData *lightmapData=(LightmapData *) userdata;
    miState *state = lightmapData->state;
    miVector bary;
    miVector q;
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);

    q.x = x+0.5f;
    q.y = y+0.5f;
    q.z = 1.0f;

    //jittered raster location
#ifdef RAY32
    {
        double	 jitter[2];
        state->raster_x	= q.x;
        state->raster_y = q.y;
        /* 143 is miQ_PIXEL_SAMPLE */
        if (mi_query(143, state, 0, jitter)	&& state->options->jitter) 
        {
            q.x += (float)jitter[0];
            q.y += (float)jitter[1];
        }
    }
#endif

    //as suggested from mr schaedlich (mr) add some noise
    mi_sample(lightmapData->fgSamplePointNoise, &lightmapData->fgSampleNumber, state, 2, 0);
    q.x+= (miScalar) (lightmapData->fgSamplePointNoise[0] * lightmapData->fgNoiseSize-0.5);
    q.y+= (miScalar) (lightmapData->fgSamplePointNoise[1] * lightmapData->fgNoiseSize-0.5);
    
    //clamp
    if (q.x<0) q.x=0;
    else if (q.x>width) q.x=width;
    if (q.y<0) q.y=0;
    else if (q.y>height) q.y=height;

    mi_vector_transform(&bary, &q, lightmapData->pixelToBary);

    fillState(state, &bary, a, b, c);
    
    //add it into the array.
    if ( (x>=0) && (x<lightmapData->numFgPointsWidth) &&
            (y>=0) && (y<lightmapData->numFgPointsHeight))
    {
        lightmapData->fgPoints[y*lightmapData->numFgPointsWidth + x].numEntries++;
        mi_vector_add( &(lightmapData->fgPoints[y*lightmapData->numFgPointsWidth + x].normal),
                    &(lightmapData->fgPoints[y*lightmapData->numFgPointsWidth + x].normal),
                    &(state->normal));
        mi_vector_add( &(lightmapData->fgPoints[y*lightmapData->numFgPointsWidth + x].normal_geom),
            &(lightmapData->fgPoints[y*lightmapData->numFgPointsWidth + x].normal_geom),
            &(state->normal_geom));
        mi_vector_add( &(lightmapData->fgPoints[y*lightmapData->numFgPointsWidth + x].point),
            &(lightmapData->fgPoints[y*lightmapData->numFgPointsWidth + x].point),
            &(state->point));
    }
    else
    {
        mi_error("fg points index out of range for %d,%d (%d,%d)",x,y,lightmapData->numFgPointsWidth, lightmapData->numFgPointsHeight);
    }
}


//-----------------------------------------------------------------------------
void doLight(miState *state,
            miColor *color, 
            int	 n_l,
            miTag *light)
//-----------------------------------------------------------------------------
{
    
    miColor radiance;
    int n;
    miVector	dir;
    double divPI = 1.0f/M_PI;
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);

    /* Loop over all light sources */
    for (n=0; n < n_l; n++, light++) 
    {
        miScalar dot_nl;
        miColor colorFromLight[9];
        miColor colorFromLightSum;
        int samples = 0;
        colorFromLightSum.r = colorFromLightSum.g = colorFromLightSum.b = 0;

        while (mi_sample_light(colorFromLight, &dir, &dot_nl, state, *light, &samples)) 
        {
            colorFromLightSum.r += dot_nl * colorFromLight[0].r;
            colorFromLightSum.g += dot_nl * colorFromLight[0].g;
            colorFromLightSum.b += dot_nl * colorFromLight[0].b;
        }
        if (samples) 
        {
            color->r += colorFromLightSum.r / samples;
            color->g += colorFromLightSum.g / samples;
            color->b += colorFromLightSum.b / samples;
        }
    }

    /* add contribution from indirect illumination (caustics) */
    mi_compute_irradiance(&radiance, state);

    color->r += (float)(radiance.r * divPI);
    color->g += (float)(radiance.g * divPI);
    color->b += (float)(radiance.b * divPI);
    
}


//-----------------------------------------------------------------------------
float doAmbientOcclusion(miState *state,                            
                         const VertexDefinition *a,
                         const VertexDefinition *b,
                         const VertexDefinition *c,
                         miVector *point,
                         miMatrix *pixelToBary,
                         miUint   samples,
                         const miScalar spread,
                         const miScalar clipdist)
//-----------------------------------------------------------------------------
{
    miScalar output = 0.0; 
    miScalar samplesdone = 0.0;
    int counter = 0;
    double sample[5];
    double near_clip, far_clip;
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);
    
    if (samples<1)
    {
        return -1.0f;
    }

    far_clip = near_clip = clipdist;       
    if (clipdist > 0.0)
    {    
        mi_ray_falloff(state, &near_clip, &far_clip);
    }

    while (mi_sample(sample, &counter, state, 5, &samples)) 
    {
        miVector trace_dir;
        miVector bary;
        miVector q;
    
        q.x = (float) (point->x+(sample[0] - 0.5) * spread);
        q.y = (float) (point->y+(sample[1] - 0.5) * spread);
        q.z = 1.0f;

        mi_vector_transform(&bary, &q, *pixelToBary);
        // constrain barycentric coordinates to triangle 
        baryFixup(&bary);

        fillState(state, &bary, a, b, c);

        trace_dir.x = (miScalar) (state->normal.x + (sample[2] - 0.5) * spread);
        trace_dir.y = (miScalar) (state->normal.y + (sample[3] - 0.5) * spread);
        trace_dir.z = (miScalar) (state->normal.z + (sample[4] - 0.5) * spread);

        mi_vector_normalize(&trace_dir);

        if (mi_vector_dot(&trace_dir, &state->normal_geom) < 0.0) 
            continue;

        output      += 1.0f; /* Add one */
        samplesdone += 1.0f;

        if (state->options->shadow &&
            mi_trace_probe(state, &trace_dir, &state->point)) 
        {
            /* we hit something */
            if (clipdist == 0.0) 
            {
                output -= 1.0f;
            }
            else if (state->child->dist < clipdist) 
            {
                miScalar f = (miScalar) (state->child->dist / clipdist);

                output -= (1.0f - f);
            }
        }
    }

    if (clipdist > 0.0f)
    {
        mi_ray_falloff(state, &near_clip, &far_clip);
    }

    if (samplesdone <= 0.0f) /* No samples? */
    {
        samplesdone = 1.0f;  /* 1.0 to not to break divisons below */
    }

    output /= (miScalar) samplesdone;
    return output;
}


//-----------------------------------------------------------------------------
void ambientOcclusionPixelCallback(int x,
                        int y,
                        float width,
                        float height, 
                        const VertexDefinition *a,
                        const VertexDefinition *b,
                        const VertexDefinition *c,
                        void *userdata)
//-----------------------------------------------------------------------------
{
    LightmapData *lightmapData=(LightmapData *) userdata;
    miVector p;
    miColor sum;
    miState *state = lightmapData->state;
    float ambientValue;
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);

    sum.r = sum.g = sum.b = 0;
    p.x = x+0.5f;
    p.y = y+0.5f;
    p.z = 1.0f;

    ambientValue = doAmbientOcclusion(state, 
        a,b,c,
        &p, &lightmapData->pixelToBary,
        lightmapData->ambientOcclusionSamplesPerPoint, 
        lightmapData->ambientOcclusionSpread, 
        lightmapData->ambientOcclusionMaxDistance);

    lightmapData->ambientOcclusionSamples[y * lightmapData->numAOPointsWidth + x] = ambientValue;
}


//-----------------------------------------------------------------------------
float getAmbientOcclusion(float *ambientOcclusionSamples, int width, int height, 
                          float left, float top, float right, float bottom)
//-----------------------------------------------------------------------------
{
    int iLeft = (int)left;
    int iTop = (int)top;
    int iRight = (int)right;
    int iBottom = (int)bottom;
    float sum = 0;
    float sumWeight=0;
    int x,y;
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);

    if ((right<0) || (left>=width) || (bottom<0) || (top>=height) )
    {
        return -1;
    }
    
    for (y=iTop;y<=iBottom; y++)
    {
        for (x=iLeft;x<=iRight; x++)
        {
            float val = ambientOcclusionSamples[y * width + x];
            if (val >=0)
            {
                float weight = 1.0f;
                if (x<left)
                {
                    weight*= (1.0f-(left-x)); 
                }
                if (x+1>right)
                {
                    weight*= (right-x); 
                }
                if (y<top)
                {
                    weight*= (1.0f-(top-y)); 
                }
                if (y+1>bottom)
                {
                    weight*= (bottom-y); 
                }        
                sumWeight+=weight;
                sum += val * weight;
            }
        }
    }
    if (sumWeight<=0)
    {
        return -1;
    }
    else
    {
        return sum/sumWeight;
    }
}


//-----------------------------------------------------------------------------
void lightPixelCallback(int x,
                        int y,
                        float width,
                        float height, 
                        const VertexDefinition *a,
                        const VertexDefinition *b,
                        const VertexDefinition *c,
                        void *userdata)
//-----------------------------------------------------------------------------
{
    LightmapData *lightmapData=(LightmapData *) userdata;
    miVector p;
    miState *state = lightmapData->state;
    miVector bary;
    miColor color;
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);

    p.x = x+0.5f;
    p.y = y+0.5f;
    p.z = 1.0f;

    mi_img_get_color(lightmapData->img, &color, x, y);
    if (color.a>0)
    {
        return; //optimization. there is allready a pixel.
    }
    color.r=0;
    color.g=0;
    color.b=0;

    state->raster_x	= p.x;
    state->raster_y = p.y;

    //jittered raster location
    {
        double	 jitter[2];
        // 143 is miQ_PIXEL_SAMPLE
        if (mi_query(143, state, 0, jitter)	&& state->options->jitter) 
        {
            p.x += (float)jitter[0];
            p.y += (float)jitter[1];
        }
    }

    mi_vector_transform(&bary, &p, lightmapData->pixelToBary);
    // constrain barycentric coordinates to triangle 
    baryFixup(&bary);

    fillState(state, &bary, a, b, c);

    doLight(state, 
        &color,
        lightmapData->n_l, 
        lightmapData->light);

    if (lightmapData->ambientOcclusionSamples!=NULL)
    {
        float widthScale=(lightmapData->numAOPointsWidth-1)/width;
        float heightScale = (lightmapData->numAOPointsHeight-1)/height;
        float ambientOcclusionVal = getAmbientOcclusion(lightmapData->ambientOcclusionSamples,
                                        lightmapData->numAOPointsWidth,
                                        lightmapData->numAOPointsHeight,
                                        x *widthScale,
                                        y * heightScale,
                                        (x+1)*widthScale,
                                        (y+1)*heightScale);

        if (ambientOcclusionVal>=0)
        {
            color.r *= lightmapData->ambientOcclusionBright.r * ambientOcclusionVal + 
                      lightmapData->ambientOcclusionDark.r * (1.0f - ambientOcclusionVal);
            color.g *= lightmapData->ambientOcclusionBright.g * ambientOcclusionVal + 
                      lightmapData->ambientOcclusionDark.g * (1.0f - ambientOcclusionVal);
            color.b *= lightmapData->ambientOcclusionBright.b * ambientOcclusionVal + 
                      lightmapData->ambientOcclusionDark.b * (1.0f - ambientOcclusionVal);
        }
        else
        {
            //internal error - what should i do here?
            mi_error("internal error, could not lookup ambientOcclusion values.");
        }
    }

    color.a = 1.0f;

    mi_img_put_color(lightmapData->img, &color, x, y);
}


//-----------------------------------------------------------------------------
miBoolean calculateFinalGatherPoints(LightmapData *lightmapData, miRclm_mesh_render const *arg)
//-----------------------------------------------------------------------------
{
//	mi_info("Line : %d File : %s", __LINE__, __FILE__);
    if ((lightmapData->fgSamplePerTexel>0) && 
        (lightmapData->state->options->finalgather!=0))
    {
        int fgX,fgY;
        int maxNumFgPoints;
        int numFgPoints=0;

        lightmapData->numFgPointsWidth = (int) ceil(lightmapData->img->width *lightmapData->fgSamplePerTexel)+1;
        lightmapData->numFgPointsHeight = (int) ceil(lightmapData->img->width *lightmapData->fgSamplePerTexel)+1;
        maxNumFgPoints = lightmapData->numFgPointsWidth * lightmapData->numFgPointsHeight;

        lightmapData->fgPoints = (FgPoint*)mi_mem_allocate(maxNumFgPoints * sizeof(FgPoint));
        if (lightmapData->fgPoints==NULL)
        {
            mi_fatal("out of memory. could not get %d bytes of memory",maxNumFgPoints * sizeof(FgPoint));
            return (miFALSE);
        }
        memset(lightmapData->fgPoints, 0, maxNumFgPoints * sizeof(FgPoint));

        doRasterizationForTriangles(arg, 
            "doing fg points",
            (float)(lightmapData->img->width * lightmapData->fgSamplePerTexel)-1,
            (float)(lightmapData->img->height * lightmapData->fgSamplePerTexel)-1,
            triangleCallback,
            fgPixelCallback, 
            lightmapData);

        for (fgY=0;fgY<lightmapData->numFgPointsHeight; fgY++)  
        {
            for (fgX=0;fgX<lightmapData->numFgPointsWidth; fgX++)
            {
                int index=fgY*lightmapData->numFgPointsWidth + fgX;
                int numEntries = lightmapData->fgPoints[index].numEntries;
                if (numEntries>0)
                {
                    miColor dummy[9];
                    mi_vector_div(&(lightmapData->fgPoints[index].normal), numEntries);
                    mi_vector_div(&(lightmapData->fgPoints[index].normal_geom), numEntries);
                    mi_vector_div(&(lightmapData->fgPoints[index].point), numEntries);

                    mi_vector_normalize(&(lightmapData->fgPoints[index].normal));
                    mi_vector_normalize(&(lightmapData->fgPoints[index].normal_geom));
                    lightmapData->state->normal=lightmapData->fgPoints[index].normal;
                    lightmapData->state->normal_geom=lightmapData->fgPoints[index].normal_geom;
                    lightmapData->state->point=lightmapData->fgPoints[index].point;
                    mi_finalgather_store(dummy, lightmapData->state, miFG_STORE_COMPUTE);
                    numFgPoints++;
                }
            }
        }
        mi_mem_release(lightmapData->fgPoints);
        lightmapData->fgPoints=NULL;
        mi_info("generated %d fg points.\n",numFgPoints);
    }

    return miTRUE;
}


