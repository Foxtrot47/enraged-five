//------------------------------------------------------------------------------
// $Workfile: PostProcess.c  $
//   Created: Clemens Pecinovsky
// Copyright: 2005, Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
//------------------------------------------------------------------------------
// Description: 
// This file implements the rage PostProcess.


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>

#include "PostProcess.h"
#include "MapImage.h"

struct Settings
{
    const char* inputFilename;
    const char* outputFilename;
    int width;
    int height;
    float filterSize;
    float gaussSigma;
    int textureFill;

    Settings()
    {
        inputFilename = NULL;
        outputFilename = NULL;
        gaussSigma = -1.0f;        
        width = -1;
        height = -1;
        filterSize = 1.0f;
        textureFill = 0;
    }
};


//-----------------------------------------------------------------------------
void usage(char* prgname)
//-----------------------------------------------------------------------------
{
    printf("USAGE: %s [-o Overfill] [-f filterSize] [-g gaussSigma] [-s width height] <inputname> <outputname> \n",prgname);
    printf(" -o texture seam overfill (in pixel)\n");
    printf(" -f filterSize (of destination image e.g 1.3)\n");
    printf(" -g gaussSigma (sigma parameter of gauss filter. <=0 means box filter)\n");
    printf(" -s resize\n");
    printf("\n");
}


//-----------------------------------------------------------------------------
int parseCommandLine(int argc, char**argv, Settings &settings)
//-----------------------------------------------------------------------------
{
    char* lastDelimiter = strrchr(argv[0],'\\');
    if (lastDelimiter!=NULL)
    {
        strcpy(argv[0],lastDelimiter+1);
    }

    for (int i=1; i<argc; i++)
    {
        if (strcmp("-o",argv[i])==0)
        {
            i++;
            if (i>=argc)
            {
                return 1;
            }
            settings.textureFill = atoi(argv[i]);
        }
        else if (strcmp("-f",argv[i])==0)
        {
            i++;
            if (i>=argc)
            {
                return 1;
            }
            settings.filterSize = (float) atof(argv[i]);
        }
        else if (strcmp("-g",argv[i])==0)
        {
            i++;
            if (i>=argc)
            {
                return 1;
            }
            settings.gaussSigma = (float) atof(argv[i]);
        }
        else if (strcmp("-s",argv[i])==0)
        {
            i++;
            if (i>=argc)
            {
                return 1;
            }
            settings.width = atoi(argv[i]);

            i++;
            if (i>=argc)
            {
                return 1;
            }
            settings.height = atoi(argv[i]);
        }
        else
        {
            if (settings.inputFilename==NULL)
            {
                settings.inputFilename = argv[i];
            }
            else if (settings.outputFilename==NULL)
            {
                settings.outputFilename = argv[i];
            }
            else
            {
                return 1;
            }
        }
    }
    if (settings.inputFilename==NULL)
    {
        return 2;
    }
    if (settings.outputFilename==NULL)
    {
        return 3;
    }
    return 0;
}

//-----------------------------------------------------------------------------
void printError(int error)
//-----------------------------------------------------------------------------
{
    printf("Error: ");
    switch (error)
    {
    case 1:
        printf("Invalid Parameter\n");
        break;
    case 2:
        printf("No Input filename given\n");
        break;
    case 3:
        printf("No Output filename given\n");
        break;
    case 4:
        printf("Invalid format\n");
        break;
    case 5:
        printf("Could not load texture\n");
        break;
    case 6:
        printf("Could not write texture\n");
        break;
    default:
        printf("Unknown Error\n");
        break;

    }
}

//-----------------------------------------------------------------------------
int main(int argc, char**argv)
//-----------------------------------------------------------------------------
{
    Settings settings;

    int error = 0;
    if (argc>=1)
    {
        error = parseCommandLine(argc, argv, settings);
    }
    if (error!=0)
    {
        usage(argv[0]);
        printError(error);
        return error;
    }

    MapImage inImage(settings.inputFilename);
    if (!((inImage.getHeight()>0) && (inImage.getWidth()>0)))
    {
        error = 4;
        printError(error);
        return error;
    }
    
    if (settings.width<=0)
    {
        settings.width = inImage.getWidth();
    }
    if (settings.height<=0)
    {
        settings.height = inImage.getHeight();
    }

    MapImage outImage(settings.height,settings.width);

    imageDownSample(outImage, inImage, settings.gaussSigma, settings.filterSize);
    

    if (settings.textureFill>0)
    {
        fillTextureSeams(outImage, settings.textureFill);
    }

    outImage.writeToFile(settings.outputFilename);

    return 0;
}


//-----------------------------------------------------------------------------
void fillTextureSeamsOneTexel(MapImage &img)
//-----------------------------------------------------------------------------
{
    int x;
    int y;
    int i;
    int j;

    for (y = 0; y < img.getHeight(); y++)
    {
        for (x = 0; x < img.getWidth(); x++)
        {            
            float tmpred =0;
            float tmpgreen = 0;
            float tmpblue =0;
            float tmpalpha =0;
            img.getPixel(x,y, tmpred, tmpgreen, tmpblue, tmpalpha);

            if (tmpalpha==0)
            {
                float red =0;
                float green = 0;
                float blue =0;
                float alpha =0;

                int numSamples = 0;

                for (i=-1;i<2;i++)
                {
                    for (j=-1;j<2;j++)
                    {
                        int tmpX= x+i;
                        int tmpY= y+j;
                        int weight = (1-abs(i)) + (1-abs(j)) ;
                                                
                        if ((tmpX>=0) && (tmpX<img.getWidth()) && 
                            (tmpY>=0) && (tmpY<img.getHeight()) && (weight>0) )
                        {
                            img.getPixel(tmpX,tmpY, tmpred, tmpgreen, tmpblue, tmpalpha);
                            if (tmpalpha==1.0f)
                            {
                                red+= tmpred * weight;
                                green+= tmpgreen * weight;
                                blue+= tmpblue * weight;
                                numSamples+=weight;
                            }
                        }
                    }
                }
                if (numSamples>0)
                {
                    float correction = 1.0f / ((float)numSamples);
                    red*= correction;
                    green*= correction;
                    blue*= correction;

                    alpha = 2;
                    img.putPixel(x,y, red, green, blue, alpha);
                }
            }
        }
    }

    for (y = 0; y < img.getHeight(); y++)
    {
        for (x = 0; x < img.getWidth(); x++)
        {
            float tmpred =0;
            float tmpgreen = 0;
            float tmpblue =0;
            float tmpalpha =0;
            img.getPixel(x,y, tmpred, tmpgreen, tmpblue, tmpalpha);

            if (tmpalpha==2)
            {
                tmpalpha = 1;
                img.putPixel(x,y, tmpred, tmpgreen, tmpblue, tmpalpha);
            }
        }
    }
}


//-----------------------------------------------------------------------------
void fillTextureSeams(MapImage &img, 
                      int numTexelsToFill)
//-----------------------------------------------------------------------------
{
    int i;
    for (i=0; i<numTexelsToFill; i++)
    {
        fillTextureSeamsOneTexel(img);
    }
}


//-----------------------------------------------------------------------------
void getBoxFilterMatrix(float* filterMatrix, int filterSize)
//-----------------------------------------------------------------------------
{
    int x,y;

    for (y=0;y<filterSize;y++)
    {
        for (x=0;x<filterSize;x++)
        {
            filterMatrix[y * filterSize + x] = 1.0f;
        }
    }
}


//-----------------------------------------------------------------------------
void getGaussFilterMatrix(float* filterMatrix, int filterSize, double sigma)
//-----------------------------------------------------------------------------
{
    int x,y;

    for (y=0;y<filterSize;y++)
    {
        for (x=0;x<filterSize;x++)
        {
            float xx = x- (((float)filterSize-1) * 0.5f);
            float yy = y- (((float)filterSize-1) * 0.5f);
            float radiusSquere = (xx*xx + yy*yy);
            float val = (float) pow(2.71828182845904523536f, (float) (-radiusSquere / (2.0f * sigma* sigma)) );
            filterMatrix[y * filterSize + x] = val;
        }
    }
}


//-----------------------------------------------------------------------------
bool imageDownSample(MapImage &outImage, MapImage &inImage,
                          double gaussSigma,
                          double filterSize)
//-----------------------------------------------------------------------------
{
    float sampleSize = sqrt( (((float)inImage.getWidth())/((float)outImage.getWidth())) * 
                             (((float)inImage.getHeight())/((float)outImage.getHeight())) );

    int iFilterSize = (int) ceil(sampleSize * filterSize);
    float *filterMatrix=new float[(iFilterSize * iFilterSize)];

    if (filterMatrix==NULL)
    {
        return false;
    }

    if (gaussSigma<=0)
    {
        getBoxFilterMatrix(filterMatrix, iFilterSize);
    }
    else
    {
        getGaussFilterMatrix(filterMatrix, iFilterSize, gaussSigma);
    }

    imageDownSampleWithFilter(outImage, 
        inImage, 
        filterMatrix,
        iFilterSize);

    delete filterMatrix;
    return true;
}


//-----------------------------------------------------------------------------
void imageDownSampleWithFilter(MapImage &outImage, MapImage &inImage,
                               float *filter,
                               int filterSize)
//-----------------------------------------------------------------------------
{
    int x,y, xx, yy;
    float widthRatio = (float) inImage.getWidth() / (float) outImage.getWidth();
    float heightRatio = (float) inImage.getHeight() / (float) outImage.getHeight();

    for (y=0;y<outImage.getHeight(); y++)
    {
        for (x=0;x<outImage.getWidth(); x++)
        {
            int xInHeighDetail = (int) (((x+0.5f) * widthRatio) - (filterSize*0.5f));
            int yInHeighDetail = (int) (((y+0.5f) * heightRatio) - (filterSize*0.5f));
            float sumWeight=0;
            float sumRed=0;
            float sumGreen=0;
            float sumBlue=0;

            for (yy = 0; yy<filterSize; yy++)
            {
                for (xx = 0; xx<filterSize; xx++)
                {
                    if ( ((yInHeighDetail + yy)>=0) && 
                        ((yInHeighDetail + yy)<inImage.getHeight()) &&
                        ((xInHeighDetail + xx)>=0) && 
                        ((xInHeighDetail + xx)<inImage.getWidth()))
                    {
                        float tmpred =0;
                        float tmpgreen = 0;
                        float tmpblue =0;
                        float tmpalpha =0;
                        inImage.getPixel((xInHeighDetail+xx),(yInHeighDetail + yy), tmpred, tmpgreen, tmpblue, tmpalpha);
                        
                        if (tmpalpha>0)
                        {
                            float weight = filter[yy * filterSize + xx];
                            sumRed += tmpred * weight;
                            sumGreen += tmpgreen * weight;
                            sumBlue += tmpblue * weight;
                            sumWeight += weight;
                        }
                    }
                }
            }

            if (sumWeight>0)
            {
                float divForNormalize = 1.0f/sumWeight;
                sumRed *= divForNormalize ;
                sumGreen *= divForNormalize ;
                sumBlue *= divForNormalize ;
                
                outImage.putPixel(x,y, sumRed, sumGreen, sumBlue, 1);
            }
        }
    }
}

