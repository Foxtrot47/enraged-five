//------------------------------------------------------------------------------
// $Workfile: Rasterizer.h  $
//   Created: Clemens Pecinovsky
// Copyright: 2005, Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
//------------------------------------------------------------------------------
// Description: 
// This file defines a general rasterizer which is used in all rage mr shaders.
// -----------------------------------------------------------------------------

#include "shader.h"

#ifndef RSV_MR_RASTERIZER_H
#define RSV_MR_RASTERIZER_H

// vertex definition. (=result data type for lightmap shader)
typedef struct VertexDefinition
{
    miVector	point;		//point in space
    miVector	tangent;	//vertex tangent
    miVector	binormal;	//vertex binormal
    miVector	normal;		//vertex normal
    miVector	tex;		//texture coordinates of vertex
} VertexDefinition;


typedef void (* RasterizationPixelCallback) (int x, 
                                            int y,
                                            float width, 
                                            float height,
                                            const VertexDefinition *a,
                                            const VertexDefinition *b,
                                            const VertexDefinition *c,
                                            void *userdata);

typedef int (* RasterizationTriangleCallback) (void *pri,
                                             miGeoIndex pri_idx,
                                             float width, 
                                             float height,
                                             const VertexDefinition *a,
                                             const VertexDefinition *b,
                                             const VertexDefinition *c,
                                             void *userdata);


void doRasterizationForTriangles(miRclm_mesh_render const *arg, 
                                 char* logInfo,
                                 float width, float height,
                                 RasterizationTriangleCallback triangleCallback,
                                 RasterizationPixelCallback pixelCallback,
                                 void *userData);

void doRasterization(VertexDefinition const *a,
                     VertexDefinition const *b,
                     VertexDefinition const *c,
                     float width, float height,
                     RasterizationPixelCallback pixelCallback,
                     void *userData);

void baryFixup(miVector *bary);

void combineVectors(miVector *res, 
                    miVector const *a, 
                    miVector const *b,
                    miVector const	*c, 
                    miVector const *bary);

#endif //RSV_MR_RASTERIZER_H
