//------------------------------------------------------------------------------
// $Workfile: LightmapShader.h  $
//   Created: Clemens Pecinovsky
// Copyright: 2005, Rockstar Vienna
// $Revision$
//   $Author$
//     $Date$
//------------------------------------------------------------------------------
// Description: 
// This file defines the structures used for the mr lightmap shader.

#include "shader.h"
#include "Rasterizer.h"

#ifndef RSV_MR_LIGHTMAP_SHADER_H
#define RSV_MR_LIGHTMAP_SHADER_H

//parameter type for rage_lightmap_write
typedef struct rage_lightmap_write_param
{
    miTag       fileNameTag;
    int         width;
    int         height;
    miScalar    fgSamplePerTexel;  //number of fg samples per texel
    miScalar    fgNoiseSize; //size of the noise.
    miScalar    ambientOcclusionSamplesPerTexel;
    int         ambientOcclusionSamplesPerPoint;
    miColor     ambientOcclusionBright;
    miColor     ambientOcclusionDark;
    miScalar    ambientOcclusionSpread;
    miScalar    ambientOcclusionMaxDistance;
} rage_lightmap_write_param;


typedef struct FgPoint 
{
    miVector    point;
    miVector    normal;
    miVector    normal_geom;
    int numEntries;
} FgPoint;


typedef struct LightmapData
{
    miState	*state;
    miImg_image *img;
    float *ambientOcclusionSamples;
    void *pri;
    miGeoIndex pri_idx;
    double ambientOcclusionSamplesPerTexel;
    int ambientOcclusionSamplesPerPoint;
    miScalar ambientOcclusionSpread;
    miScalar ambientOcclusionMaxDistance;
    miColor ambientOcclusionBright;
    miColor ambientOcclusionDark;
    int numAOPointsWidth;
    int numAOPointsHeight;
    int	 n_l;
    miTag *light;
    double fgSamplePerTexel;
    double fgNoiseSize;
    int fgSampleNumber;
    double fgSamplePointNoise[2];
    int numFgPointsHeight;
    int numFgPointsWidth;
    FgPoint *fgPoints;
    miMatrix pixelToBary;
} LightmapData;


miBoolean calculateFinalGatherPoints(LightmapData *lightmapData, 
                            miRclm_mesh_render const *arg);

void fillTextureSeams(miImg_image *img, 
                      char* textureFillMask, 
                      int numTexelsToFill);

void doLight(miState *state,
             miColor *color, 
             int	 n_l,
             miTag *light);

float doAmbientOcclusion(miState *state, 
                         const VertexDefinition *a,
                         const VertexDefinition *b,
                         const VertexDefinition *c,
                         miVector *point,
                         miMatrix *pixelToBary,
                         miUint   samples,
                         const miScalar spread,
                         const miScalar clipdist);

int triangleCallback(void *pri,
                     miGeoIndex pri_idx,
                     float width, 
                     float height,
                     const VertexDefinition *a,
                     const VertexDefinition *b,
                     const VertexDefinition *c,
                     void *userdata);

void fillState(miState *state,
               const miVector *bary,
               const VertexDefinition *a,
               const VertexDefinition *b,
               const VertexDefinition *c);

void fgPixelCallback(int x, 
                    int y,
                    float width, 
                    float height, 
                    const VertexDefinition *a,
                    const VertexDefinition *b,
                    const VertexDefinition *c,
                    void *userdata);


void lightPixelCallback(int x,
                    int y,
                    float width,
                    float height, 
                    const VertexDefinition *a,
                    const VertexDefinition *b,
                    const VertexDefinition *c,
                    void *userdata);

void ambientOcclusionPixelCallback(int x,
                        int y,
                        float width,
                        float height, 
                        const VertexDefinition *a,
                        const VertexDefinition *b,
                        const VertexDefinition *c,
                        void *userdata);


float getAmbientOcclusion(float *ambientOcclusionSamples, int width, int height, 
                          float left, float top, float right, float bottom);


miBoolean calculateFgPoints(LightmapData *lightmapData, miRclm_mesh_render const *arg);


DLLEXPORT int rageLightmapWrite_version(void);

DLLEXPORT miBoolean rageLightmapWrite( VertexDefinition	*result,
                                     miState				*state,
                                     rage_lightmap_write_param	*param,
                                     miRclm_mesh_render const	*arg);

DLLEXPORT void rageLightmapWrite_init(miState         *state,
                             rage_lightmap_write_param *param,
                             miBoolean       *inst_req);

DLLEXPORT void rageLightmapWrite_exit(miState         *state,
                             rage_lightmap_write_param *param);


#endif //RSV_MR_LIGHTMAP_SHADER_H
