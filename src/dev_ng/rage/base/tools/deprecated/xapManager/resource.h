//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by xapManager.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_XAPMANAGER_DIALOG           102
#define IDS_APP_TITLE                   102
#define IDS_XAP_FILE_FILTER             103
#define IDD_MSGDLG                      103
#define IDS_NONE                        104
#define IDS_WORKING                     105
#define IDS_PLEASE_WAIT                 106
#define IDD_OPTIONSDLG                  107
#define IDR_MAINFRAME                   128
#define IDC_LIST1                       1000
#define IDC_BUTTON_EXTRACT              1001
#define IDC_BUTTON_MERGE                1002
#define IDC_BUTTON_OPEN                 1003
#define IDC_BUTTON_NEW_BANK             1004
#define ID_PROJECT_TEXT                 1005
#define IDC_EDIT1                       1006
#define IDC_GROUP1                      1007
#define IDC_CHECK_OPT_SAVE_AUTO_OPEN    1008
#define IDC_MESSAGE                     1009
#define IDC_BUTTON_OPTIONS              1010
#define IDC_EDIT_BUILD_CMD              1011
#define IDC_BUTTON1                     1012
#define IDC_BUTTON_BUILD                1012
#define IDC_STATIC_VERSION              1013
#define IDC_STATIC_COPYRIGHT            1014
#define IDC_CHECK_OPT_LEAVE_BACKUP      1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           108
#endif
#endif
