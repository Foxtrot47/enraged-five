// xapManager.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "xapManager.h"
#include "xapManagerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CxapManagerApp

BEGIN_MESSAGE_MAP(CxapManagerApp, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


// CxapManagerApp construction

CxapManagerApp::CxapManagerApp()
	: m_hMutex(NULL)
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CxapManagerApp object
CxapManagerApp theApp;

HWND g_hWnd = NULL;

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)
{
	CString WindowText;
	CResourceString	Title(IDS_APP_TITLE);

	CWnd::FromHandle(hwnd)->GetWindowText(WindowText);

	if (WindowText.Find(Title) == 0)
	{
		//Found a window whose title starts with our APP_TITLE, so let's hope it's ours.  Worst case is we bring the wrong window to the front :)
		g_hWnd = hwnd;
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

void CxapManagerCommandLineInfo::ParseParam(const char* pszParam, BOOL bFlag, BOOL bLast)
{
	static bool projectFileNext = false;

	if (bFlag)
	{
		if (!stricmp(pszParam, "project"))
		{
			projectFileNext = true;
		}
	}
	else if (projectFileNext)
	{
		static_cast<CxapManagerApp*>(AfxGetApp())->SetCmdLinePath(pszParam);
		projectFileNext = false;
	}

	CCommandLineInfo::ParseParam(pszParam, bFlag, bLast);
}

// CxapManagerApp initialization
BOOL CxapManagerApp::InitInstance()
{
	m_hMutex = ::CreateMutex(NULL, false, "R* RAGE xapManager Mutex"); //A relatively unique string so we don't get false positives

	if (m_hMutex && GetLastError() == ERROR_ALREADY_EXISTS)
	{
		::ReleaseMutex(m_hMutex);

		//Enumerate all windows, if we find one that's ours, g_hWnd will be valid
		::EnumWindows(EnumWindowsProc, 0); 

		if (g_hWnd)
		{
			// g_hWnd holds the handle to a previous instance so bring it to the foreground.
			::SetForegroundWindow(g_hWnd);
		}

		return FALSE;
	}

	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	CWinApp::InitInstance();

	CxapManagerCommandLineInfo cmdLineInfo;
	ParseCommandLine(cmdLineInfo);

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Rockstar Games"));

	CxapManagerDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CxapManagerApp::ExitInstance()
{
	if (m_hMutex)
		::ReleaseMutex(m_hMutex);

	return CWinApp::ExitInstance();
}
