// xapManager.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols


// CxapManagerApp:
// See xapManager.cpp for the implementation of this class
//
class CxapManagerApp : public CWinApp
{
public:
	CxapManagerApp();

	void SetCmdLinePath(const char* path) { m_CommandLinePath = path; }
	CString& GetCmdLinePath(void)		  { return m_CommandLinePath; }

// Overrides
	public:
	virtual BOOL InitInstance();
	virtual int  ExitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()

private:
	HANDLE m_hMutex; //For single instance-ness :)
	CString m_CommandLinePath;
};

//Silly little class to make loading strings from resources available via the constructor.
//Not sure why CString doesn't implement this directly.
class CResourceString : public CString
{
public:
	CResourceString(UINT resourceID) { LoadString(resourceID); }
};

class CMsgDialog : public CDialog
{
public:
	CMsgDialog(CWnd* pParentWnd, CString strTitle, CString strMessage)
		: m_strTitle(strTitle), m_strMessage(strMessage), m_bIsDisplaying(false)
	{
		Create(IDD_MSGDLG, pParentWnd);
	}

	void Display(void)
	{
		SetWindowText(m_strTitle);
		GetDlgItem(IDC_MESSAGE)->SetWindowText(m_strMessage);
		CenterWindow();
		ShowWindow(SW_SHOWNORMAL);
		UpdateWindow();

		m_bIsDisplaying = true;
	}

	void Close(void)
	{
		if (m_bIsDisplaying)
			DestroyWindow();

		m_bIsDisplaying = false;
	}

private:
	CString	m_strTitle;
	CString m_strMessage;

	bool m_bIsDisplaying;
};

class CxapManagerCommandLineInfo : public CCommandLineInfo
{
public:
	virtual void ParseParam(const char* pszParam, BOOL bFlag, BOOL bLast);
};

extern CxapManagerApp theApp;
