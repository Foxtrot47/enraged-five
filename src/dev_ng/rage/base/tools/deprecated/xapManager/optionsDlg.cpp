// optionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "xapManager.h"
#include "optionsDlg.h"


// COptionsDlg dialog

IMPLEMENT_DYNAMIC(COptionsDlg, CDialog)
COptionsDlg::COptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDlg::IDD, pParent)
	, m_bAutoOpen(false)
	, m_bLeaveBackupsAfterMerge(false)
	, m_strBuildCmd(_T(""))
{
}

COptionsDlg::~COptionsDlg()
{
}

void COptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_OPT_SAVE_AUTO_OPEN, m_bAutoOpen);
	DDX_Check(pDX, IDC_CHECK_OPT_LEAVE_BACKUP, m_bLeaveBackupsAfterMerge);
	DDX_Text(pDX, IDC_EDIT_BUILD_CMD, m_strBuildCmd);
}

BEGIN_MESSAGE_MAP(COptionsDlg, CDialog)
END_MESSAGE_MAP()


// COptionsDlg message handlers
