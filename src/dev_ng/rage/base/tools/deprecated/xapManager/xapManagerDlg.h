// xapManagerDlg.h : header file
//

#include "afxwin.h"
#include "optionsDlg.h"
#include "..\xapcore\xapfile.h"
#pragma once

#define	REGISTRY_SECTION_DEFAULTS		"Defaults"
#define	REGISTRY_ENTRY_LAST_PATH		"LastMasterProjectPath"
#define REGISTRY_ENTRY_LAST_TITLE		"LastMasterProjectTitle"
#define REGISTRY_ENTRY_BUILD_CMD		"BuildCommand"
#define REGISTRY_ENTRY_AUTO_OPEN		"AutoOpenLast"
#define REGISTRY_ENTRY_LEAVE_BACKUPS	"LeaveBackupsAfterMerge"
#define REGISTRY_ENTRY_WINDOW_PLC		"Placement"

// CxapManagerDlg dialog
class CxapManagerDlg : public CDialog
{
// Construction
public:
	CxapManagerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_XAPMANAGER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL ContinueModal(); //override as a hook to call UpdateDialogControls()
	virtual void OnCancel();
	virtual void OnOK();

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	afx_msg void OnUpdateButtonExtract(CCmdUI* pCmdUI);
	afx_msg void OnUpdateButtonMerge(CCmdUI* pCmdUI);
	afx_msg void OnUpdateButtonBuild(CCmdUI* pCmdUI);
	afx_msg void OnUpdateButtonNewBank(CCmdUI* pCmdUI); //For the button itself
	afx_msg void OnUpdateNewBank(CCmdUI* pCmdUI); //For the group and edit box

	DECLARE_MESSAGE_MAP()

	bool OpenMasterFile(CString& strPath, CString& strTitle, bool bIsReload = false);
	void ExtractBank(CString& bankTitle, CStdioFile& outFile);
	void FixupGuids(CStdioFile& mergeFile, CStdioFile& mergeFileTemp);
	void MergeBank(CStdioFile& mergeFile, CStdioFile& newOutputMasterFile);
	void NewBank(CStdioFile& outFile);
	void CustomInit();
	void Cleanup();
	void CloseMasterFile();

public:
	afx_msg void OnBnClickedButtonOpen();
	afx_msg void OnBnClickedButtonMerge();
	afx_msg void OnBnClickedButtonExtract();
	afx_msg void OnBnClickedButtonBuild();
	afx_msg void OnBnClickedButtonNewBank();
	afx_msg void OnBnClickedButtonOptions();

private:
	CString m_strMasterProjectPath;
	CString m_strMasterProjectTitle;
	bool	m_bMasterProjectOpen;
	bool	m_bInitialized;

	CStdioFile m_MasterFile;
	XAPFile*   m_pMasterXAP;


protected:
	CListBox m_ListBox;			// The list box :)
	CEdit	 m_NewBankEditBox;  // The new bank edit box
	
	COptionsDlg m_OptionsDlg;
};
