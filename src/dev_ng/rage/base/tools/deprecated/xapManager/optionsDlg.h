#include "afxwin.h"
#pragma once


// COptionsDlg dialog

class COptionsDlg : public CDialog
{
	DECLARE_DYNAMIC(COptionsDlg)

public:
	COptionsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptionsDlg();

	BOOL GetAutoOpen(void) { return m_bAutoOpen; }
	void SetAutoOpen(BOOL bAutoOpen) { m_bAutoOpen = bAutoOpen; }
	BOOL GetLeaveBackupsAfterMerge(void) { return m_bLeaveBackupsAfterMerge; }
	void SetLeaveBackupsAfterMerge(BOOL bLeaveBackups) { m_bLeaveBackupsAfterMerge = bLeaveBackups; }
	const CString& GetBuildCommand(void) { return m_strBuildCmd; }
	void SetBuildCommand(CString& strBuildCmd) { m_strBuildCmd = strBuildCmd; }
	
// Dialog Data
	enum { IDD = IDD_OPTIONSDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
private:
	BOOL    m_bAutoOpen;
	BOOL	m_bLeaveBackupsAfterMerge;
	CString m_strBuildCmd;
};
