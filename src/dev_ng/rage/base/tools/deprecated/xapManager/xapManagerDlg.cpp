// xapManagerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "xapManager.h"
#include "xapManagerDlg.h"
#include "moduleVersion.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	void SetVersion(CString strVersion) { m_strVersion = strVersion; }
	void SetCopyright(CString strCopyright) { m_strCopyright = strCopyright; }

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	CString m_strVersion;
	CString m_strCopyright;
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
, m_strVersion(_T("9.9.9.9"))
, m_strCopyright(_T("LegalCopyright"))
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_STATIC_VERSION, m_strVersion);
	DDX_Text(pDX, IDC_STATIC_COPYRIGHT, m_strCopyright);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CxapManagerDlg dialog



CxapManagerDlg::CxapManagerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CxapManagerDlg::IDD, pParent)
	, m_bMasterProjectOpen(false)
	, m_bInitialized(false)
	, m_pMasterXAP(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CxapManagerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ListBox);
	DDX_Control(pDX, IDC_EDIT1, m_NewBankEditBox);
}

//Added as a convenient hook to emulate non-CDialog based command ui message handling
BOOL CxapManagerDlg::ContinueModal()
{
	static bool bOnce = true;

	if (m_bInitialized && bOnce)
	{
		CustomInit();
		bOnce = false;
	}

	UpdateDialogControls(this, false);
	return CDialog::ContinueModal();
}

void CxapManagerDlg::OnOK()
{
	//Do nothing! Don't call the base class.  This is so we do not close due to the user hitting enter (the "virtual" OK of the dialog).
}

void CxapManagerDlg::OnCancel()
{
	Cleanup();
	CDialog::OnCancel();
}

BEGIN_MESSAGE_MAP(CxapManagerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_ACTIVATE()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_OPEN, OnBnClickedButtonOpen)
	ON_BN_CLICKED(IDC_BUTTON_MERGE, OnBnClickedButtonMerge)
	ON_BN_CLICKED(IDC_BUTTON_EXTRACT, OnBnClickedButtonExtract)
	ON_BN_CLICKED(IDC_BUTTON_NEW_BANK, OnBnClickedButtonNewBank)
	ON_BN_CLICKED(IDC_BUTTON_OPTIONS, OnBnClickedButtonOptions)
	ON_UPDATE_COMMAND_UI(IDC_BUTTON_NEW_BANK, OnUpdateButtonNewBank)
	ON_UPDATE_COMMAND_UI(IDC_BUTTON_EXTRACT, OnUpdateButtonExtract)
	ON_UPDATE_COMMAND_UI(IDC_BUTTON_MERGE, OnUpdateButtonMerge)
	ON_UPDATE_COMMAND_UI(IDC_BUTTON_BUILD, OnUpdateButtonBuild)
	ON_UPDATE_COMMAND_UI_RANGE(IDC_EDIT1, IDC_GROUP1, OnUpdateNewBank)
	ON_BN_CLICKED(IDC_BUTTON_BUILD, OnBnClickedButtonBuild)
END_MESSAGE_MAP()


// CxapManagerDlg message handlers

BOOL CxapManagerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CResourceString strAboutMenu(IDS_ABOUTBOX);
	
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	SetWindowText(CResourceString(IDS_APP_TITLE));
	GetDlgItem(ID_PROJECT_TEXT)->SetWindowText(CResourceString(IDS_NONE));

	//Restore window position from last time
	WINDOWPLACEMENT* lpPlacement = NULL;
	UINT uSize;

	if (AfxGetApp()->GetProfileBinary(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_WINDOW_PLC, (LPBYTE*) &lpPlacement, &uSize) && (uSize == sizeof(WINDOWPLACEMENT)))
	{
		SetWindowPlacement(lpPlacement);
		delete [] lpPlacement;
	}

	m_bInitialized = true; //So we can do some initialization work that requires this dialog to already have been created

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CxapManagerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		
		//Initialize the version string.  When this app changes, the VS_VERSION_INFO Version resource should be changed to reflect.
		CModuleVersion ver;

		if (ver.GetFileVersionInfo(NULL)) 
		{
			dlgAbout.SetVersion(ver.GetValue(_T("ProductVersion")));
			dlgAbout.SetCopyright(ver.GetValue(_T("LegalCopyright")));
		}

		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CxapManagerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CxapManagerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//////////////////////////////////////////////////////////////////////////
// Command UI updating
//////////////////////////////////////////////////////////////////////////
void CxapManagerDlg::OnUpdateButtonExtract(CCmdUI* pCmdUI)
{
	//Enabled if something in the list is selected
	bool bEnable = m_ListBox.GetCurSel() >= 0 ? true : false;
	pCmdUI->Enable(bEnable);
}

void CxapManagerDlg::OnUpdateButtonMerge(CCmdUI* pCmdUI)
{
	//Enabled if a project has been opened
	pCmdUI->Enable(m_bMasterProjectOpen);
}

void CxapManagerDlg::OnUpdateButtonBuild(CCmdUI* pCmdUI)
{
	//Enabled if a build command has been set
	pCmdUI->Enable(m_bMasterProjectOpen && !m_OptionsDlg.GetBuildCommand().IsEmpty());
}

void CxapManagerDlg::OnUpdateButtonNewBank(CCmdUI* pCmdUI)
{
	//Enabled if there is text in the edit box
	CString text;
	m_NewBankEditBox.GetWindowText(text);
	pCmdUI->Enable(!text.IsEmpty());
}

void CxapManagerDlg::OnUpdateNewBank(CCmdUI* pCmdUI)
{
	//Enabled if a master project is open
	pCmdUI->Enable(m_bMasterProjectOpen);	
}

//////////////////////////////////////////////////////////////////////////
// Button click events
//////////////////////////////////////////////////////////////////////////
void CxapManagerDlg::OnBnClickedButtonOpen()
{
	CFileDialog	dlg(true, ".xap", NULL, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_ENABLESIZING, CResourceString(IDS_XAP_FILE_FILTER), this);

	if (dlg.DoModal() == IDOK)
	{
		OpenMasterFile(dlg.GetPathName(), dlg.GetFileTitle());
	}
}

void CxapManagerDlg::OnBnClickedButtonMerge()
{
	bool		bSuccess = true;
	CStdioFile	mergeFile;
	CStdioFile  mergeFileTemp;
	CStdioFile	newOutputMasterFile;
	CString		strMsg;
	CString		strNewOutputMasterFilePath = m_strMasterProjectPath + ".new";
	CFileDialog	dlg(true, ".xap", NULL, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_ENABLESIZING, CResourceString(IDS_XAP_FILE_FILTER), this);

	//If the master file was read-only when it was opened, 

	CFileStatus status;
	bool bIsReadOnly = false;

	if (CFile::GetStatus(m_strMasterProjectPath, status))
	{
		if (status.m_attribute & CFile::readOnly)
			bIsReadOnly = true;
	}

	if (bIsReadOnly)
	{
		MessageBox("Master file is read-only.  Please check out of AlienBrain and try again!");
		return;
	}

	if (dlg.DoModal() == IDOK)
	{
		CFileException	fe;
		CString			mergeFilePathName = dlg.GetPathName();
		CString			mergeFileTempPathName = mergeFilePathName + ".tmp";
		CMsgDialog		dlgWorking(this, CResourceString(IDS_WORKING), CResourceString(IDS_PLEASE_WAIT));

		dlgWorking.Display();
		UpdateWindow();
		
		try
		{
			if (!mergeFile.Open(mergeFilePathName, CFile::modeRead | CFile::shareDenyWrite | CFile::typeBinary | CFile::osSequentialScan, &fe))
				throw &fe;

			if (!mergeFileTemp.Open(mergeFileTempPathName, CFile::modeCreate | CFile::modeReadWrite | CFile::shareDenyWrite| CFile::typeBinary | CFile::osSequentialScan, &fe))
				throw &fe;

			FixupGuids(mergeFile, mergeFileTemp);

			if (!newOutputMasterFile.Open(strNewOutputMasterFilePath, CFile::modeCreate | CFile::modeReadWrite | CFile::shareDenyWrite | CFile::typeBinary | CFile::osSequentialScan, &fe))
				throw &fe;

			MergeBank(mergeFileTemp, newOutputMasterFile);
		}
		catch(CFileException* fe)
		{
			dlgWorking.Close();
			fe->ReportError();
			bSuccess = false;
		}
		catch(XapException& e)
		{
			dlgWorking.Close();
			MessageBox(e.GetMessage(), "Merge Bank Error!");
			bSuccess = false;
		}
	
		if (mergeFile.m_hFile != CFile::hFileNull)
			mergeFile.Close();

		if (mergeFileTemp.m_hFile != CFile::hFileNull)
			mergeFileTemp.Close();

		if (newOutputMasterFile.m_hFile != CFile::hFileNull)
			newOutputMasterFile.Close();

		if (bSuccess)
		{
			dlgWorking.Close();

			//Remove the temporary merge file (the one with the fixed-up guids).
			CFile::Remove(mergeFileTempPathName);

			CloseMasterFile();

			// rename new file to old file
			CFile::Remove(m_strMasterProjectPath);
			CFile::Rename(strNewOutputMasterFilePath, m_strMasterProjectPath);

			if (OpenMasterFile(m_strMasterProjectPath, m_strMasterProjectTitle, true))
			{
				//Opening the new master file worked properly so...

				//Create a backup file name (<file.xap>.bak)
				CString strBackupPathName = mergeFilePathName + ".bak";

				try
				{	//Remove previous backup if one exists.  Need to 'try' this since it may throw an exception if it doesn't exist.
					CFile::Remove(strBackupPathName);
				}
				catch(CFileException* e)
				{
					e->Delete();
				}

				if (m_OptionsDlg.GetLeaveBackupsAfterMerge())
				{
					//Rename the merge file with the backup pathname
					CFile::Rename(mergeFilePathName, strBackupPathName);

					//Make the backup file hidden
					CFileStatus status;
					CFile::GetStatus(strBackupPathName, status);
					status.m_attribute |= CFile::hidden;

					CFile::SetStatus(strBackupPathName, status);
				}
				else
				{
					//Remove the merge file altogether
					CFile::Remove(mergeFilePathName);
				}
			}
		}
		else
		{
			try
			{
				//May not exist if we failed to open merge file or create the new output master file.
				CFile::Remove(strNewOutputMasterFilePath);
				CFile::Remove(mergeFileTempPathName);
			}
			catch (CFileException* e)
			{
				e->Delete();
			}
		}
	}
}

void CxapManagerDlg::OnBnClickedButtonExtract()
{
	//Something must be selected in the list, or this control would be disabled...
	int nCurSel = m_ListBox.GetCurSel();

	CString strSelection;
	m_ListBox.GetText(nCurSel, strSelection);

	bool			bSuccess = true;
	CFileException	fe;
	CString			strMsg;
	CStdioFile		outFile;
	CString			strOutFilePath = m_strMasterProjectPath;
	CMsgDialog		dlgWorking(this, CResourceString(IDS_WORKING), CResourceString(IDS_PLEASE_WAIT));

	dlgWorking.Display();

	try
	{	
		if (strOutFilePath.Replace(m_strMasterProjectTitle, strSelection) != 1) //better be replacing a single instance!
		{
			strMsg.Format("Cannot extract bank %s.  The name is in the master\nproject path (%s)!", strSelection, m_strMasterProjectPath);
			throw strMsg;
		}

		if (!outFile.Open(strOutFilePath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite | CFile::typeBinary | CFile::osSequentialScan, &fe))
			throw &fe;

		ExtractBank(strSelection, outFile);
	}
	catch(CString& str)
	{
		dlgWorking.Close(); //Close the working dialog so we can see the message box!
		MessageBox(str, "Extract Bank Error");
		bSuccess = false;
	}
	catch(CFileException* fe)
	{
		dlgWorking.Close(); //Close the working dialog so we can see the message box!
		fe->ReportError();
		bSuccess = false;
	}
	catch(XapException& e)
	{
		//must have been thrown from SpitBank()
		dlgWorking.Close(); //Close the working dialog so we can see the message box!
		CFile::Remove(strOutFilePath);
		MessageBox(e.GetMessage(), "Extract Bank Error");
		bSuccess = false;
	}

	if (outFile.m_hFile != CFile::hFileNull)
		outFile.Close();

	if (bSuccess)
	{
		dlgWorking.Close();
		strMsg.Format("Extracted bank %s.", strOutFilePath);
		MessageBox(strMsg, "Extraction successful!");
	}
}

void CxapManagerDlg::OnBnClickedButtonNewBank()
{
	bool bSuccess = true;
	CString strBankName;
	m_NewBankEditBox.GetWindowText(strBankName);

	int nDot = strBankName.Find('.');

	if (nDot != -1)
		strBankName = strBankName.Left(nDot);

	CFileException	fe;
	CStdioFile		outFile;
	CString			strMsg;
	CString			strOutFilePath = m_strMasterProjectPath;
	
	try
	{
		if (strOutFilePath.Replace(m_strMasterProjectTitle, strBankName) != 1) //better be replacing a single instance!
		{
			strMsg.Format("Cannot create new bank %s.  The name is in the master\nproject path (%s)!", strBankName, m_strMasterProjectPath);
			throw strMsg;
		}

		if (!outFile.Open(strOutFilePath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite | CFile::typeBinary | CFile::osSequentialScan, &fe))
			throw &fe;

		NewBank(outFile);
	}
	catch(CString& str)
	{
		//thrown from above
		MessageBox(str, "Create Bank Error");
		bSuccess = false;
	}
	catch(CFileException* fe)
	{
		fe->ReportError();
		bSuccess = false;
	}
	catch(XapException& e)
	{
		//must have been thrown from NewBank()
		outFile.Close();
		CFile::Remove(strOutFilePath);
		MessageBox(e.GetMessage(), "Create Bank Error");
		bSuccess = false;
	}

	if (outFile.m_hFile != CFile::hFileNull)
		outFile.Close();

	if (bSuccess)
	{
		strMsg.Format("Created new bank %s.", strOutFilePath);
		MessageBox(strMsg, "Creation successful!");

		m_NewBankEditBox.SetWindowText("");
	}
}

void CxapManagerDlg::OnBnClickedButtonOptions()
{
	m_OptionsDlg.DoModal();
}

//////////////////////////////////////////////////////////////////////////
// Protected internal methods
//////////////////////////////////////////////////////////////////////////
bool CxapManagerDlg::OpenMasterFile(/*in*/CString& strPath, /*in*/CString& strTitle, bool bIsReload /*=false*/)
{
	//Clear out the list box
	m_ListBox.ResetContent();

	if (m_pMasterXAP)
	{
		delete m_pMasterXAP;
		m_pMasterXAP = NULL;
	}

	//Close previous master file if open, NULL out XAPFile*
	if (m_MasterFile.m_hFile != CFile::hFileNull)
		m_MasterFile.Close();

	//Change the window's title to the opened file's title.
	CString fileName = CResourceString(IDS_APP_TITLE) + CString(" - ") + strTitle;
	SetWindowText(fileName);

	//Show the full path above the list box
	GetDlgItem(ID_PROJECT_TEXT)->SetWindowText(strPath);

	UpdateWindow(); //Update the main window so everything is drawn in properly since we're going to throw up a loading dialog

	bool			bSuccess = true;
	CFileException	fe;
	CString			loadingTitle = (bIsReload ? "Reloading " : "Loading ") + strTitle + CString("...");
	CMsgDialog		loadingDlg(this, loadingTitle, CResourceString(IDS_PLEASE_WAIT));
	
	//Display a little loading dialog
	loadingDlg.Display();

	try
	{
		// open the input file
		if (!m_MasterFile.Open(strPath, CFile::modeRead | CFile::shareDenyWrite | CFile::typeBinary | CFile::osSequentialScan, &fe))
			throw &fe;

		m_pMasterXAP = new XAPFile(m_MasterFile, true);

		int nNumSoundBanks = m_pMasterXAP->GetNumSoundBanks();

		for (int i = 0; i < nNumSoundBanks; i++)
		{
			CString strBankName(m_pMasterXAP->GetSoundBank(i).GetName());
			m_ListBox.AddString(strBankName);
			m_ListBox.UpdateWindow();
		}
	}
	catch(CFileException* fe)
	{
		loadingDlg.Close(); //Close the loading dialog so we can see the message box!
		fe->ReportError();
		bSuccess = false;
	}
	catch (XapException e)
	{
		loadingDlg.Close(); //Close the loading dialog so we can see the message box!
		MessageBox(e.GetMessage(), "Error");
		bSuccess = false;
	}

	if (bSuccess)
	{	
		loadingDlg.Close();
		m_bMasterProjectOpen    = true;
		m_strMasterProjectPath  = strPath;
		m_strMasterProjectTitle = strTitle;

		//Set the current directory explicitly since we may have been called directly (without an Open File dialog being presented which has the
		//side effect of setting the cwd for us).  This can happen if -project is on the command line or if they've chosen to auto-open the last 
		//project file.
		CString strCurrentDir = m_strMasterProjectPath.Left(m_strMasterProjectPath.ReverseFind('\\'));
		SetCurrentDirectory(strCurrentDir);
	}
	else
	{
		m_bMasterProjectOpen = false;
		m_strMasterProjectPath.Empty();
		m_strMasterProjectTitle.Empty();

		//Change the window's title back to default.
		SetWindowText(CResourceString(IDS_APP_TITLE));

		//Show "NONE" next to "Project:" above the list box
		GetDlgItem(ID_PROJECT_TEXT)->SetWindowText(CResourceString(IDS_NONE));
	}

	return bSuccess;
}

void CxapManagerDlg::ExtractBank(CString& bankTitle, CStdioFile& outFile)
{
	int nWaveBankIndex  = m_pMasterXAP->FindWaveBank(bankTitle);
	int nSoundBankIndex = m_pMasterXAP->FindSoundBank(bankTitle);

	if (nWaveBankIndex != -1 && nSoundBankIndex != -1)
	{
		m_pMasterXAP->WriteXapGlobalSettings(outFile);
		m_pMasterXAP->WriteXapWaveBank(nWaveBankIndex, outFile);
		m_pMasterXAP->WriteXapSoundBank(nSoundBankIndex, outFile);
	}
	else
	{
		CString msg;
		msg.Format("Unable to find section: %s", bankTitle);
		throw XapException(msg);
	}
}

void CxapManagerDlg::FixupGuids(CStdioFile& mergeFile, CStdioFile& mergeFileTemp)
{
	// parse bank file
	XAPFile bankXap(mergeFile, true);

	if (!m_pMasterXAP->FixupGuids(&bankXap, mergeFileTemp))
	{
		CString strMsg("Problems encountered while fixing up GUIDs!");
		throw XapException(strMsg);
	}
}

void CxapManagerDlg::MergeBank(CStdioFile& mergeFile, CStdioFile& newOutputMasterFile)
{
	// parse bank file
	XAPFile bankXap(mergeFile, true);

	// verify the global settings
	if (!m_pMasterXAP->MatchGlobalSettings(&bankXap))
	{
		CString strMsg("Global Settings do not match!");
		throw XapException(strMsg);
	}

	// check for existing wave and sound banks
	int nWaveBankIndex  = m_pMasterXAP->FindWaveBank(bankXap.GetWaveBank(0).GetName());
	int nSoundBankIndex = m_pMasterXAP->FindSoundBank(bankXap.GetSoundBank(0).GetName());

	if (nWaveBankIndex != nSoundBankIndex)
		throw XapException("Wave bank and sound bank count in master file do not match!");

	// appending
	if (nWaveBankIndex == -1 && nSoundBankIndex == -1)
	{
		m_pMasterXAP->AppendXapWaveBank(&bankXap, newOutputMasterFile);
		m_pMasterXAP->AppendXapSoundBank(&bankXap, newOutputMasterFile);
	}
	else // merging
	{
		m_pMasterXAP->ReplaceXapWaveBank(nWaveBankIndex, &bankXap, newOutputMasterFile);
		m_pMasterXAP->ReplaceXapSoundBank(nWaveBankIndex, nSoundBankIndex, &bankXap, newOutputMasterFile);
	}
}

void CxapManagerDlg::NewBank(CStdioFile& outFile)
{
	m_pMasterXAP->WriteXapGlobalSettings(outFile);
	m_pMasterXAP->CreateXapWaveBank(outFile);
	m_pMasterXAP->CreateXapSoundBank(outFile);
}

void CxapManagerDlg::CustomInit()
{
	//See if they want to auto open last
	int nVal = AfxGetApp()->GetProfileInt(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_AUTO_OPEN, -1);

	if (nVal != -1)
		m_OptionsDlg.SetAutoOpen(nVal ? true : false);

	//See if they want to leave backups after a merge
	nVal = AfxGetApp()->GetProfileInt(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_LEAVE_BACKUPS, -1);

	if (nVal != -1)
		m_OptionsDlg.SetLeaveBackupsAfterMerge(nVal ? true : false);

	m_OptionsDlg.SetBuildCommand(AfxGetApp()->GetProfileString(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_BUILD_CMD));

	//See if a project was specified on the cmd line
	CString& commandLinePath = static_cast<CxapManagerApp*>(AfxGetApp())->GetCmdLinePath();
	if (commandLinePath.IsEmpty())
	{
		//See if the last master project file got saved
		CString strLastPath = AfxGetApp()->GetProfileString(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_LAST_PATH);
		CString strLastTitle = AfxGetApp()->GetProfileString(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_LAST_TITLE);

		if (m_OptionsDlg.GetAutoOpen() && !strLastPath.IsEmpty() && !strLastTitle.IsEmpty())
		{
			OpenMasterFile(strLastPath, strLastTitle);
		}
	}
	else
	{
		//Path specified on the command line!
		CString strTitle = commandLinePath;

		int nStart = strTitle.ReverseFind('\\') + 1;
		strTitle = strTitle.Tokenize(".", nStart);

		OpenMasterFile(commandLinePath, strTitle);
	}
	
}

void CxapManagerDlg::Cleanup()
{
	CloseMasterFile();

	WINDOWPLACEMENT placement;
	GetWindowPlacement(&placement);

	UpdateData(true); //Make sure member vars, etc. are all updated

	//Save some data to the registry.
	AfxGetApp()->WriteProfileString(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_LAST_PATH, (LPCSTR) m_strMasterProjectPath);
	AfxGetApp()->WriteProfileString(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_LAST_TITLE, (LPCSTR) m_strMasterProjectTitle);
	AfxGetApp()->WriteProfileInt(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_AUTO_OPEN, m_OptionsDlg.GetAutoOpen() ? 1 : 0);
	AfxGetApp()->WriteProfileInt(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_LEAVE_BACKUPS, m_OptionsDlg.GetLeaveBackupsAfterMerge() ? 1 : 0);
	AfxGetApp()->WriteProfileString(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_BUILD_CMD, m_OptionsDlg.GetBuildCommand());

	//Store off the window position
	AfxGetApp()->WriteProfileBinary(REGISTRY_SECTION_DEFAULTS, REGISTRY_ENTRY_WINDOW_PLC, (LPBYTE) &placement, sizeof(WINDOWPLACEMENT));
}

void CxapManagerDlg::CloseMasterFile()
{
	if (m_MasterFile.m_hFile != CFile::hFileNull)
		m_MasterFile.Close();

	if (m_pMasterXAP)
	{
		delete m_pMasterXAP;
		m_pMasterXAP = NULL;
	}

	m_bMasterProjectOpen = false;
}

void CxapManagerDlg::OnBnClickedButtonBuild()
{
	CString strMessage;
	CString strFullPath;

	_fullpath(strFullPath.GetBufferSetLength(MAX_PATH), m_OptionsDlg.GetBuildCommand(), MAX_PATH);
	strFullPath.ReleaseBuffer();

	strMessage.Format("This will run the following command: '%s'\nAre you sure you wish to continue?", strFullPath);

	if (MessageBox(strMessage, "Build Confirmation", MB_YESNO) == IDYES)
	{
		system(strFullPath);
	}
}
