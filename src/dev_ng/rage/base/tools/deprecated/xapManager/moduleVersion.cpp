
#include "StdAfx.h"
#include <stdexcept>
#include "moduleVersion.h"

using namespace std;


CModuleVersion::CModuleVersion()
{
   m_lpVersionInfo = NULL;           // raw version info data 
}

//////////////////
// Destroy: delete version info
//
CModuleVersion::~CModuleVersion()
{
   if (m_lpVersionInfo)
      delete [] m_lpVersionInfo;
}

//////////////////
// Get file version info for a given module
// Allocates storage for all info, fills "this" with
// VS_FIXEDFILEINFO, and sets codepage.
//
BOOL CModuleVersion::GetFileVersionInfo(LPCTSTR szModuleName)
{
   if (m_lpVersionInfo) {
      delete [] m_lpVersionInfo;
      m_lpVersionInfo = NULL;
   }

   try {
      m_translation.charset = 1252;    // default = ANSI code page
      ZeroMemory(this, sizeof(VS_FIXEDFILEINFO)); // zero out the VS_FIXEDFILEINFO struct members variables
      // get module handle
      _TCHAR  szFileName[_MAX_PATH];
      HMODULE hModule;

      // First try calling GetModuleHandle with szModuleName.  If this fails, 
      // try calling it with NULL to get the handle of this file (the file used
      // to create the calling process).
      if (NULL == (hModule = GetModuleHandle(szModuleName)))
         if (NULL == (hModule = GetModuleHandle(NULL)))
            throw logic_error((""));

      // get module file name
      DWORD dwLen;
      if ((dwLen = GetModuleFileName(hModule, 
                                     szFileName,
                                     sizeof(szFileName)/sizeof(_TCHAR))) <= 0)
         throw logic_error((""));

      // read file version info
      DWORD dwDummyHandle; // will always be set to zero
      if ((dwLen = GetFileVersionInfoSize(szFileName, &dwDummyHandle)) <= 0)
         throw logic_error((""));

      m_lpVersionInfo = new BYTE[dwLen]; // allocate version info
      if (! ::GetFileVersionInfo(szFileName, 0, dwLen, m_lpVersionInfo))
         throw logic_error((""));

      LPVOID lpvi;
      UINT   uLen;
      if (!VerQueryValue(m_lpVersionInfo, _T("\\"), &lpvi, &uLen))
         throw logic_error((""));

      // copy fixed info to myself, which am derived from VS_FIXEDFILEINFO
      *(static_cast<VS_FIXEDFILEINFO *>(this)) = *(static_cast<VS_FIXEDFILEINFO *>(lpvi));

      // Get translation info
      if (VerQueryValue(m_lpVersionInfo,_T("\\VarFileInfo\\Translation"), &lpvi, &uLen) && uLen >= 4) {
         m_translation = *(static_cast<TRANSLATION *> (lpvi));
         TRACE(_T("code page: %d, Lang ID : 0x%04x\n"), m_translation.charset, m_translation.langID);
      }
   }
   catch (logic_error e) {
      if (m_lpVersionInfo) {
         delete [] m_lpVersionInfo;
         m_lpVersionInfo = NULL;
      }
   }
   // To get a string value must pass query in the form
   //
   //    "\StringFileInfo\\keyname"
   //
   // where  is the languageID concatenated with the
   // code page, in hex.
   return dwSignature == VS_FFI_SIGNATURE;
}

//////////////////
// Get string file info.
// Key name is something like "CompanyName".
// returns the value as a CString.
//
LPCTSTR CModuleVersion::GetValue(LPCTSTR lpszKeyName)
{
   LPCTSTR lpszVal = NULL;
   if (m_lpVersionInfo) {

      // To get a string value must pass query in the form
      //
      //    "\StringFileInfo\\keyname"
      //
      // where  is the languageID concatenated with the
      // code page, in hex. Wow.
      //
      CString strQuery;
      strQuery.Format(_T("\\StringFileInfo\\%04x%04x\\%s"),
                      m_translation.langID,
                      m_translation.charset,
                      lpszKeyName);

      TRACE(_T("Query : %s\n"), strQuery);

      UINT    uLenVal;
      if (VerQueryValue(m_lpVersionInfo, 
                        strQuery.GetBuffer(strQuery.GetLength()),
                        static_cast<LPVOID *>(static_cast<LPVOID>(&lpszVal)),
                        &uLenVal)) {
      }
   }
   return lpszVal;
}

// typedef for DllGetVersion proc
typedef HRESULT (CALLBACK* DLLGETVERSIONPROC)(DLLVERSIONINFO *);

/////////////////
// Get DLL Version by calling DLL's DllGetVersion proc
//
BOOL CModuleVersion::DllGetVersion(LPCTSTR szModuleName, DLLVERSIONINFO& dvi)
{
   HINSTANCE hinst = LoadLibrary(szModuleName);
   if (!hinst)
      return FALSE;

   // Must use GetProcAddress because the DLL might not implement 
   // DllGetVersion. Depending upon the DLL, the lack of implementation of the 
   // function may be a version marker in itself.
   //
   DLLGETVERSIONPROC pDllGetVersion =
      (DLLGETVERSIONPROC)GetProcAddress(hinst, ("DllGetVersion")); // Use a string resources 
                                                                   // in such cases. where the 
                                                                   // the API requires a char*
                                                                   // and does not accept a 
                                                                   // wchar_t*
   if (!pDllGetVersion)         
      return FALSE;

   ZeroMemory(&dvi, sizeof(DLLVERSIONINFO)); // clear
   dvi.cbSize = sizeof(DLLVERSIONINFO);      // set size for Windows

   return SUCCEEDED((*pDllGetVersion)(&dvi));
}
