#ifndef __HDRCLIENTPIPE
#define __HDRCLIENTPIPE

#include	"system/namedpipe.h"
#include 	"grpostfx/postfx.h"
#include 	"vector/vector3.h"

#define MAX_PATH 260
class hdrClientPipe : public rage::sysNamedPipe
{
public:
	enum Command { ACK, 
		PAUSE_CLIENT,
		SET_CLIENT_PPP,

		ADD_REFERENCE_TEXTURE, 
		DELETE_CLIENT_TEXTURE, 
		BRING_CLIENTTEX_TO_FOREGROUND,
		MOVE_CLIENT_TEXTURE, 
		SCALE_CLIENT_TEXTURE, 
		LIGHT_CLIENT_TEXTURE, 
		SET_CLIENT_TEXTURE_DATA, 
		GET_CLIENT_TEXTURE_DATA,

		ADD_EXPOSURE_TEXTURE, 
		SET_TEXTURE_EXPOSURE,
		SET_IGNORE_ALPHA, 
		SET_DISPLAY_EXPOSURE, 
		DELETE_EXPOSURE_TEXTURE, 

	};

	// Computer waits for other computer to send ACK
	void WaitForAck();

	// Send ACK to waiting computer.
	void SendAck();

	// Data transfer structures
	struct DataPacket
	{
		DataPacket(Command command) : m_Command(command) {};
		Command m_Command;
	};

	struct PauseClient : public DataPacket
	{
		PauseClient() : DataPacket(PAUSE_CLIENT) {} ;
		bool m_Pause;
	};

	struct AddReferenceTexture : public DataPacket
	{
		AddReferenceTexture() : DataPacket(ADD_REFERENCE_TEXTURE) {} ;
		int m_DrawOrder;
		char m_Filename[MAX_PATH];
	};

	struct AddExposureTexture : public DataPacket
	{
		AddExposureTexture() : DataPacket(ADD_EXPOSURE_TEXTURE) {} ;
		int m_DrawOrder;
		float m_Exposure;
		char m_Filename[MAX_PATH];
	};

	struct DeleteClientTexture : public DataPacket
	{
		DeleteClientTexture() : DataPacket(DELETE_CLIENT_TEXTURE) {} ;
		int m_TextureId;
	};

	struct BringToForeground : public DataPacket
	{
		BringToForeground() : DataPacket(BRING_CLIENTTEX_TO_FOREGROUND) {} ;
		int m_TextureId;
	};

	struct MoveClientTexture : public DataPacket
	{
		MoveClientTexture() : DataPacket(MOVE_CLIENT_TEXTURE) {} ;
		int m_TextureId;
		int m_X;
		int m_Y;
	};

	struct ScaleClientTexture : public DataPacket
	{
		ScaleClientTexture() : DataPacket(SCALE_CLIENT_TEXTURE) {} ;
		int m_TextureId;
		int m_Width;
		int m_Height;
	};

	struct LightClientTexture : public DataPacket
	{
		LightClientTexture() : DataPacket(LIGHT_CLIENT_TEXTURE) {} ;
		int m_TextureId;
		float m_Exposure;
	};

	struct SetTextureExposure : public DataPacket
	{
		SetTextureExposure() : DataPacket(SET_TEXTURE_EXPOSURE) {} ;
		int m_TextureId;
		// texture specific parameters
		float m_Exposure;
		rage::Vector3 m_ColorExponent;
		rage::Vector3 m_ColorOffset;
		// global parameters
		float m_DisplayExposure;
		bool m_UseGlobalExponents;
	};

	struct SetIgnoreAlpha : public DataPacket
	{
		SetIgnoreAlpha() : DataPacket(SET_IGNORE_ALPHA) {} ;
		bool m_IgnoreAlpha;
	};

	struct SetDisplayExposure : public DataPacket
	{
		SetDisplayExposure() : DataPacket(SET_DISPLAY_EXPOSURE) {} ;
		float m_DisplayExposure;
	};

	struct SetClientPPP : public DataPacket
	{
		SetClientPPP() : DataPacket(SET_CLIENT_PPP) {} ;
		int m_TextureId;
		rage::grPostFX::grcPPPPreset m_PPPPreset;
	};


	struct SetClientTextureData : public DataPacket
	{
		SetClientTextureData() : DataPacket(SET_CLIENT_TEXTURE_DATA), m_X(0), m_Y(0), m_Width(0), m_Height(0)  {} ;
		int m_TextureId;
		int m_X;
		int m_Y;
		int m_Width;
		int m_Height;
	};

	struct GetClientTextureData : public DataPacket
	{
		GetClientTextureData() : DataPacket(GET_CLIENT_TEXTURE_DATA)  {} ;
		int m_TextureId;
		int m_X;
		int m_Y;
		int m_Width;
		int m_Height;
	};

	// Send a command with associated data to other computer.
	void Send( DataPacket& packet );

	// Receive command from other computer
	// Returns length of received data.
	int Receive( Command& command, bool blocking );

	DataPacket* GetReceivedData() { return (DataPacket*)m_RcvBuffer; }

protected:
	char m_SendBuffer[512];
	char m_RcvBuffer[512];
};

#endif
