#ifndef __MAXEXPONENTSHADER_H
#define __MAXEXPONENTSHADER_H
#include	"grcore/effect.h"
#include	"vector/color32.h"

using	namespace	rage;

namespace	rage
{
	class grmShader;
	class grcRenderTarget;
	class grcTexture;
}

class MaxExponentShader
{
public:

	void SetPickingTexture(grcTexture* tex);
	void SetExponentTexture(grcTexture* tex);
	void SetBlitTexture(grcTexture* tex);

	void Init();
	void InitVars();
	void Blit(int x1,int y1,int x2,int y2,float z,float u1,float v1,float u2,float v2,Color32 color,const char *tech) const;
	void Shutdown();
protected:
	grmShader* m_Shader;
	grcEffectVar m_PickingTexIdx;
	grcEffectVar m_ExponentTexIdx;
	grcEffectVar m_BlitTexIdx;
};

#endif
