//**************************************************************//
//  Debug shader
//
//  Alexander Ehrath 1/06
//**************************************************************//
#ifndef __DEBUGF_FXH
#define __DEBUGF_FXH

shared float NumTex = 0.0/255.0;
shared float Exposures[16];
shared float DisplayExposure=0.0;
shared float DebugVal0;

#endif