//**************************************************************//
//  HDR texture merging shader
//
//  Alexander Ehrath 1/06
//**************************************************************//
#include "../../../src/shaderlib/rage_common.fxh"
#include "../../../src/shaderlib/rage_samplers.fxh"

float3 MergeColorExponent = {0,0,0};
float3 MergeColorOffset = {0,0,0};
float MergeNumTex = 0.0/255.0;
float MergeExposures[16];
float MergeDisplayExposure=0.0;
bool MergeuseGlobalExponents = false;
bool MergeconvertToRGBE = true;
bool MergeconvertFromRGBE = false;
bool MergeshowExponent = false;


struct VS_OUTPUT 
{
   float4 Pos:       POSITION;
   float2 TexCoord:   TEXCOORD0;
};


#pragma dcl position normal texcoord0 tangent

VS_OUTPUT vs_main(float4 inPos: POSITION, float3 inNormal: NORMAL,
                  float2 inTxr: TEXCOORD0, float3 inTangent: TANGENT)
{
	VS_OUTPUT OUT;
	OUT.Pos = inPos;
	OUT.TexCoord = inTxr;
	return OUT;
}

float4	ColorTexExp	:	ColorTexExp
<
	string	UIName = "Color Texture Exponent";
	float	UIMin = 0.0;
	float	UIMax = 8.0;
	float	UIStep = 0.05;
> = { 1.0, 1.0, 1.0, 1.0f };

float4	ColorTexOfs	:	ColorTexOfs
<
	string	UIName = "Color Texture Offset";
	float	UIMin = 0.0;
	float	UIMax = 8.0;
	float	UIStep = 0.05;
> = { 0.0, 0.0, 0.0, 0.0 };


BeginSampler(sampler2D, ColorTexture, ColorSampler, ColorTex)
	string	UIName = "Color Texture";
	string	UvSetName = "map1";
	int		UvSetIndex = 0;
ContinueSampler(sampler2D, ColorTexture, ColorSampler, ColorTex)
	AddressU  = WRAP;
	AddressV  = WRAP;
	AddressW  = WRAP;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
EndSampler;


float4 ps(VS_OUTPUT IN) : COLOR 
{
	float4 TextureColor = tex2D(ColorSampler,IN.TexCoord);
	TextureColor.xyz *= ColorTexExp.xyz;
	TextureColor.xyz += ColorTexOfs.xyz;
	return TextureColor;
}

technique draw
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps();
   }
}

