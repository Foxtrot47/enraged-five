set ARCHIVE=ragehdrlib
set FILES=hdrtextureset defaultshader mergetexturesshader picktexturesshader maxexponentshader views hdrtexturetoollib hdrclientpipe
set HEADONLY=rgbeconv uielement
set EMBEDDEDSHADERS=debugf.fx max_exponent.fx merge_textures.fx pick_textures.fx blit_textures.fx
set LIBS_CORE=system diag file data string paging forceinclude vector bank input math mathext atl qa profile devcam text stlport
set LIBS_GFX=grcore grmodel mesh jpeg grpostfx 
set LIBS= %LIBS_CORE% %LIBS_GFX%
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\migrate\src %RAGE_DIR%\suite\src

