#include "atl/array.h"
#include "hdrtextureset.h"
#include "grcore/texture.h"
#include "file/asset.h"

hdrTextureSet::~hdrTextureSet()
{
	Shutdown();
}

void hdrTextureSet::Init(int maxExposures)
{
	m_Texture.Reserve((unsigned short) maxExposures);
}

void hdrTextureSet::DeleteAll()
{
	while(m_Texture.GetCount())
	{
		m_Texture.Delete(0);
	}
}

void hdrTextureSet::Shutdown()
{
	DeleteAll(); // calling this makes atArray blow up!! TODO: Track this down.. may be a RAGE bug?
	m_Texture.Reset();
}

bool hdrTextureSet::HasAlpha()
{ 
	
	for(int i=0; i<m_Texture.GetCount(); i++) 
	{ 
		if(m_Texture[i].HasAlpha())
			return true; 
	} 
	return false; 
}

int hdrTextureSet::AddTexture(grcImage* image, float exposure, const char* filename, bool hasAlpha, int draworder)
{
	hdrExposureTexture& tex = m_Texture.Append();
	tex.m_HasAlpha = hasAlpha;
	tex.m_pTexture = grcTextureFactory::GetInstance().Create(image);
	tex.m_Exposure = exposure;												// *0.5 compensate for discrepancy in exposure half steps.
	tex.m_Filename = 	StringDuplicate(filename);
	int id = m_Texture.GetCount()-1;
	if(draworder < 0)
		tex.m_DrawOrder = id;
	else
		tex.m_DrawOrder = draworder;
	return id;
}

int hdrTextureSet::AddTexture(const char* filename, float exposure, int draworder)
{
	fiStream* dummy = ASSET.Open(filename, "dds", true, true);
	if(dummy)
	{
		dummy->Close();
		hdrExposureTexture& tex = m_Texture.Append();
		tex.m_pTexture = grcTextureFactory::GetInstance().Create(filename);
		tex.m_Exposure = exposure;												// *0.5 compensate for discrepancy in exposure half steps.
		tex.m_Filename = 	StringDuplicate(filename);
		int id = m_Texture.GetCount()-1;
		if(draworder < 0)
			tex.m_DrawOrder = id;
		else
			tex.m_DrawOrder = draworder;
		return id;
	}
	return -1;
}

void hdrTextureSet::RemoveTexture(int Id)
{
	BringToForeground(Id);
	m_Texture.Delete(Id);
}

void hdrTextureSet::BringToForeground(int Id)
{
	int curOrder = m_Texture[Id].m_DrawOrder;
	for(int i=0; i<m_Texture.GetCount(); i++)
	{
		if(m_Texture[i].m_DrawOrder > curOrder) 
		{
			m_Texture[i].m_DrawOrder--;
		}
	}
	m_Texture[Id].m_DrawOrder = m_Texture.GetCount()-1;
}
