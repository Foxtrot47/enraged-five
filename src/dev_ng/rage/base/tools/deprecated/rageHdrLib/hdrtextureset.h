#ifndef __HDRTEXTURESET_H
#define __HDRTEXTURESET_H
#include "atl/array.h"
#include "atl/functor.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"
#if __WIN32PC
#include "system/xtl.h"
#endif
using	namespace	rage;
namespace rage
{
	class grcImage;
}
class hdrTextureSet;

class hdrExposureTexture
{
friend class hdrTextureSet;
public:
	hdrExposureTexture() : m_pTexture(0), m_Exposure(0.0f), m_Filename(0), m_DrawOrder(0), m_HasAlpha(false)
#if __WIN32PC
	, m_Hwnd(0)
#endif
	{
	}

	~hdrExposureTexture()
	{
		if(m_pTexture)
			m_pTexture->Release();
		if(m_Filename)
			delete m_Filename;

	}
	hdrExposureTexture& hdrExposureTexture::operator=(const hdrExposureTexture& p) 
	{
		if (this != &p) {  // make sure not same object
			if(m_pTexture)
				m_pTexture->Release();
			if(m_Filename)
				delete m_Filename;
			m_pTexture = p.m_pTexture;
			p.m_pTexture->AddRef();
			m_Filename = StringDuplicate(p.m_Filename);
			m_Exposure = p.m_Exposure;
			m_DrawOrder = p.m_DrawOrder;
			m_HasAlpha = p.m_HasAlpha;

#if __WIN32PC
			m_Hwnd = p.m_Hwnd;
			m_Viewport = p.m_Viewport;
#endif
		}
		return *this;    // Return ref for multiple assignment
	}

	grcTexture* GetTexture() { return m_pTexture; }
	const char* GetTextureName() { return m_Filename; }
	float& GetExposure() { return m_Exposure; }
	int GetDrawOrder() { return m_DrawOrder; }
	void SetDrawOrder(int order) { m_DrawOrder = order; }
	bool HasAlpha() { return m_HasAlpha; }
#if __WIN32PC
	grcViewport& GetViewport() { return m_Viewport; }
	HWND GetHWND() { return m_Hwnd; }
	void SetHWND(HWND hwnd) { m_Hwnd = hwnd; }
#endif
protected:
	grcTexture* m_pTexture;
	float m_Exposure;
	const char * m_Filename;
	int m_DrawOrder;
	bool m_HasAlpha;

#if __WIN32PC
	// PC specific variables so the texture can be opened in its own window
	HWND m_Hwnd;
	grcViewport m_Viewport;
#endif
};


class hdrTextureSet
{
public:
	~hdrTextureSet();
	// 
	// PURPOSE
	//	Initializes the class
	// PARAMS
	//	maxExposures - maximum number of possible exposures.
	void Init(int maxExposures);

	void Shutdown();
	void DeleteAll();

	// 
	// PURPOSE
	//	Adds a reference texture to current HDR texture.
	// PARAMS
	//	filename - filename of texture to load
	//	exposure - exposure level that texture represents.
	// RETURNS
	//	Id of texture created. 
	//
	int AddTexture(const char* filename, float exposure, int draworder = -1);

	// 
	// PURPOSE
	//	Adds a reference texture to current HDR texture.
	// PARAMS
	//	image - pointer to image data of texture
	//	exposure - exposure level that texture represents.
	// RETURNS
	//	Id of texture created. 
	//
	int AddTexture(grcImage* image, float exposure, const char* filename, bool hasAlpha, int draworder = -1);			

	// 
	// PURPOSE
	//	Removes a reference texture to current HDR texture.
	// PARAMS
	//	Id - Id of texture to be deleted
	//
	void RemoveTexture(int Id);

	// 
	// PURPOSE
	//	Get the number of currently allocated textures.
	// RETURNS
	//	number of reference textures in this set.
	//
	int GetNumTextures() { return m_Texture.GetCount(); }

	bool HasAlpha();

	hdrExposureTexture& GetTexture(int Id) { Assert(Id < m_Texture.GetCount()); return m_Texture[Id]; }

	void BringToForeground(int Id);
	bool IsInForeground(int Id) { return m_Texture[Id].m_DrawOrder == m_Texture.GetCount()-1; }
private:
	atArray<hdrExposureTexture> m_Texture;

};
#endif
