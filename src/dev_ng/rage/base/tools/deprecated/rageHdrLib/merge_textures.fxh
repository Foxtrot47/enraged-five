
float4 rgb3e2float(float4 rgb, float3 ofs, float3 exp)
{
  float4 retvalRGB;
  retvalRGB.xyz = rgb.xyz * exp + ofs;
  retvalRGB.w = 0.0;
  return retvalRGB;
}

float3 float2rgb3e(float3 rgb, float3 ofs, float3 exp)
{
	rgb -= ofs;
	rgb /= exp;
	return rgb;
}

/*
float4   float2rgbe(float4 rgb)
{
	float3 v;
	float e;
    v = ColorExponent;
    rgb.xyz -= ColorOffset;
	if (v.x < 1e-32) {
		v.x = 0;
	}
	else
	{
		v.x = frexp(v.x,e) / v.x;
	}
	if (v.y < 1e-32) {
		v.y = 0;
	}
	else
	{
		v.y = frexp(v.y,e) / v.y;
	}
	if (v.z < 1e-32) {
		v.z = 0;
	}
	else
	{
		v.z = frexp(v.z,e) / v.z;
	}
	retvalRGBE.xyz = (rgb.xyz * v.xyz);
	retvalRGBE.w = 0.0;
}
*/

float4 float2rgbe(float4 rgb)
{
  float4 retvalRGBE;
  {
	float v;
	float e;
	v = rgb.x;
	if (rgb.y > v) v = rgb.y;
	if (rgb.z > v) v = rgb.z;
	if (v < 1e-32) {
		retvalRGBE = float4(0.0,0.0,0.0,0.0);
	}
	else {
		v = frexp(v,e) / v;
		retvalRGBE.xyz = (rgb.xyz * v);
		retvalRGBE.w = (e + 128);
		retvalRGBE.w *= (1.0/255.0);
	}
  }
  return retvalRGBE;
}


float4 float2rgbe2(float4 rgb)
{
  float v;
  float e;
  float4 rgbe;

  v = rgb.x;
  if (rgb.y > v) v = rgb.y;
  if (rgb.z > v) v = rgb.z;
//  if (SingleExponent > 0.0) 
//	v = SingleExponent;
  if (v < 1e-32) {
    rgbe = float4(0.0,0.0,0.0,0.0);
  }
  else {
    v = frexp(v,e) / v;
    rgbe.x = (rgb.x * v);
    rgbe.y = (rgb.y * v);
    rgbe.z = (rgb.z * v);
    rgbe.w = (e + 128);
    rgbe.w *= (1.0/255.0);
  }
  return rgbe;
}


float4 rgbe2float(float4 rgbe)
{
  float4 rgb;
  float f;
  rgbe *= 255.0;
  rgb.w = 0.0;
  if (rgbe.w) {   /*nonzero pixel*/
    f = ldexp(1.0,rgbe.w-(128));
    rgb.x = rgbe.x * f;
    rgb.y = rgbe.y * f;
    rgb.z = rgbe.z * f;
	rgb *= (1.0/255.0);
  }
  else
  {
    rgb = float4(0.0,0.0,0.0,0.0);
  }
  return rgb;
}

