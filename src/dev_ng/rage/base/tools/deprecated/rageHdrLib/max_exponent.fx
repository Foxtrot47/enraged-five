//**************************************************************//
//  HDR texture merging shader
//
//  Alexander Ehrath 1/06
//**************************************************************//

#include "../../../src/shaderlib/rage_common.fxh"
#include "../../../src/shaderlib/rage_samplers.fxh"

float3 MaxColorExponent = {0,0,0};
float3 MaxColorOffset = {0,0,0};
float MaxNumTex = 0.0/255.0;
float MaxExposures[16];
float MaxDisplayExposure=0.0;
bool MaxuseGlobalExponents = false;
bool MaxconvertFromRGBE = false;


BeginSampler(sampler2D,exponentTexture,ExponentTextureSampler,ExponentTex)
string UIName="Exponent Texture";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,exponentTexture,ExponentTextureSampler,ExponentTex)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture,DiffuseTextureSampler,DiffuseTex)
string UIName="Diffuse Texture 0";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture,DiffuseTextureSampler,DiffuseTex)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;
struct VS_OUTPUT 
{
   float4 Pos:       POSITION;
   float2 TexCoord:   TEXCOORD0;
};

BeginSampler(sampler2D,pickingTexture,PickingTextureSampler,PickingTex)
string UIName="Picking Texture";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,pickingTexture,PickingTextureSampler,PickingTex)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

#pragma dcl position normal texcoord0 tangent

VS_OUTPUT vs_main(float4 inPos: POSITION, float3 inNormal: NORMAL,
                  float2 inTxr: TEXCOORD0, float3 inTangent: TANGENT)
{
	VS_OUTPUT OUT;
	OUT.Pos = inPos;
	OUT.TexCoord = inTxr;
	return OUT;
}

#include "pick_textures_samplers.fxh"

#include "merge_textures.fxh"

float4 ps_main(VS_OUTPUT IN) : COLOR 
{
	float4 pickcol= float4(1.0, 0.0, 1.0, 1.0);;// = pickTexCol(IN.TexCoord, MaxExposures);	
	pickcol = pickcol * (pow( 2, 0.0));	
	if(MaxconvertFromRGBE)
	{
		pickcol = float2rgbe(pickcol);
		if(MaxuseGlobalExponents)
		{
			pickcol = rgb3e2float(pickcol, MaxColorOffset, MaxColorExponent);
		}
		else
		{
			pickcol = rgbe2float(pickcol);
		}
	}
	float4 exponentcol = tex2D(ExponentTextureSampler,float2(0.0,0.0));
	if(pickcol.w > exponentcol.w)
	{
		exponentcol.x = pickcol.w;
		exponentcol.y = pickcol.w;
		exponentcol.z = pickcol.w;
		exponentcol.w = pickcol.w;
	}
	return exponentcol;
}


technique max_exponent
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;

      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps_main();
   }
}

float4 ps_simple(VS_OUTPUT IN) : COLOR 
{
	return tex2D(DiffuseTextureSampler,IN.TexCoord);
}

technique blit_simple
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;

      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps_simple();
   }
}

technique draw
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE;

      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps_simple();
   }
}
	


