#include "hdrclientpipe.h"
#include 	"grpostfx/postfx.h"
#include	"system/endian.h"
#include	"system/ipc.h"

void Swapper(unsigned &a) { a = rage::sysEndian::LtoN(a); }
void Swapper(int &a) { a = rage::sysEndian::LtoN(a); }
void Swapper(bool &) { }
void Swapper(float &a) { a = rage::sysEndian::LtoN(a); }
void Swapper(rage::Vector3 &a) { 
	a.x = rage::sysEndian::LtoN(a.x); 
	a.y = rage::sysEndian::LtoN(a.y); 
	a.z = rage::sysEndian::LtoN(a.z); 
}
void Swapper(rage::Vector4 &a) { 
	a.x = rage::sysEndian::LtoN(a.x); 
	a.y = rage::sysEndian::LtoN(a.y); 
	a.z = rage::sysEndian::LtoN(a.z); 
	a.w = rage::sysEndian::LtoN(a.w); 
}


void ByteSwapPPPPreset(rage::grPostFX::grcPPPPreset &preset);

void hdrClientPipe::SendAck()
{
	m_SendBuffer[0] = ACK;
	Write(m_SendBuffer, 1);
}

void hdrClientPipe::WaitForAck()
{
	while(!Read(m_RcvBuffer, sizeof(m_RcvBuffer))) rage::sysIpcSleep(1);
	Assert(m_RcvBuffer[0] == ACK);
}

void hdrClientPipe::Send(DataPacket& packet)
{

	int scommand = (int)packet.m_Command;

	int command = (int)packet.m_Command;
	Swapper(command);
	packet.m_Command = (Command) command;

	switch(scommand)
	{
	case PAUSE_CLIENT:
		{
			PauseClient* pPacket = (PauseClient*)&packet;
			Swapper(pPacket->m_Pause);
			Write(pPacket, sizeof(PauseClient));
			break;
		}
	case ADD_REFERENCE_TEXTURE:
		{
			AddReferenceTexture* pPacket = (AddReferenceTexture*)&packet;
			Swapper(pPacket->m_DrawOrder);
			Write(pPacket, sizeof(AddReferenceTexture) - sizeof(pPacket->m_Filename) + strlen(pPacket->m_Filename)+1);
			break;
		}
	case ADD_EXPOSURE_TEXTURE:
		{
			AddExposureTexture* pPacket = (AddExposureTexture*)&packet;
			Swapper(pPacket->m_DrawOrder);
			Swapper(pPacket->m_Exposure);
			Write(pPacket, sizeof(AddExposureTexture) - sizeof(pPacket->m_Filename) + strlen(pPacket->m_Filename)+1);
			break;
		}
	case DELETE_CLIENT_TEXTURE:
		{
			DeleteClientTexture* pPacket = (DeleteClientTexture*)&packet;
			Swapper(pPacket->m_TextureId);
			Write(pPacket, sizeof(DeleteClientTexture));
			break;
		}
	case BRING_CLIENTTEX_TO_FOREGROUND:
		{
			BringToForeground* pPacket = (BringToForeground*)&packet;
			Swapper(pPacket->m_TextureId);
			Write(pPacket, sizeof(BringToForeground));
			break;
		}
	case MOVE_CLIENT_TEXTURE:
		{
			MoveClientTexture* pPacket = (MoveClientTexture*)&packet;
			Swapper(pPacket->m_TextureId);
			Swapper(pPacket->m_X);
			Swapper(pPacket->m_Y);
			Write(pPacket, sizeof(MoveClientTexture));
			break;
		}
	case SET_CLIENT_TEXTURE_DATA:
		{
			SetClientTextureData* pPacket = (SetClientTextureData*)&packet;
			Swapper(pPacket->m_TextureId);
			Swapper(pPacket->m_X);
			Swapper(pPacket->m_Y);
			Swapper(pPacket->m_Width);
			Swapper(pPacket->m_Height);
			Write(pPacket, sizeof(SetClientTextureData));
			break;
		}
	case GET_CLIENT_TEXTURE_DATA:
		{										
			GetClientTextureData* pPacket = (GetClientTextureData*)&packet;
			Swapper(pPacket->m_TextureId);
			Swapper(pPacket->m_X);
			Swapper(pPacket->m_Y);
			Swapper(pPacket->m_Width);
			Swapper(pPacket->m_Height);
			Write(pPacket, sizeof(GetClientTextureData));
			break;
		}
	case LIGHT_CLIENT_TEXTURE:
		{
			LightClientTexture* pPacket = (LightClientTexture*)&packet;
			Swapper(pPacket->m_TextureId);
			Swapper(pPacket->m_Exposure);
			Write(pPacket, sizeof(LightClientTexture));
			break;
		}
	case SET_TEXTURE_EXPOSURE:
		{
			SetTextureExposure* pPacket = (SetTextureExposure*)&packet;
			Swapper(pPacket->m_TextureId);
			Swapper(pPacket->m_Exposure);
			Swapper(pPacket->m_ColorExponent);
			Swapper(pPacket->m_ColorOffset);
			Swapper(pPacket->m_DisplayExposure);
			Swapper(pPacket->m_UseGlobalExponents);
			Write(pPacket, sizeof(SetTextureExposure));
			break;
		}
	case SET_IGNORE_ALPHA:
		{
			SetIgnoreAlpha* pPacket = (SetIgnoreAlpha*)&packet;
			Swapper(pPacket->m_IgnoreAlpha);
			Write(pPacket, sizeof(SetIgnoreAlpha));
			break;
		}
	case SET_DISPLAY_EXPOSURE:
		{
			SetDisplayExposure* pPacket = (SetDisplayExposure*)&packet;
			Swapper(pPacket->m_DisplayExposure);
			Write(pPacket, sizeof(SetDisplayExposure));
			break;
		}
	case SCALE_CLIENT_TEXTURE:
		{
			ScaleClientTexture* pPacket = (ScaleClientTexture*)&packet;
			Swapper(pPacket->m_TextureId);
			Swapper(pPacket->m_Width);
			Swapper(pPacket->m_Height);
			Write(pPacket, sizeof(ScaleClientTexture));
			break;
		}
	case SET_CLIENT_PPP:
		{
			SetClientPPP* pPacket = (SetClientPPP*)&packet;
			ByteSwapPPPPreset(pPacket->m_PPPPreset);
			Write(pPacket, sizeof(SetClientPPP));
			break;
		}
	default:
		break;
	}
	
	command = (int)packet.m_Command;
	Swapper(command);
	packet.m_Command = (Command) command;
}


int hdrClientPipe::Receive(Command& com, bool blocking)
{
block:
	int numBytesRead = Read(m_RcvBuffer, sizeof(m_RcvBuffer));
	if(blocking && !numBytesRead)
	{
		rage::sysIpcSleep(1);
		goto block;
	}
	if(numBytesRead)
	{

		memcpy(&com, m_RcvBuffer, sizeof(Command));

// this is a bit hacky to get things to compile
		int command = (int)com;
		Swapper(command);		
		com = (Command) command;
// end a bit hacky

		switch(command)		
		{
		case PAUSE_CLIENT:
			{
				PauseClient* pPacket = (PauseClient*)GetReceivedData();
				Swapper(pPacket->m_Pause);
				break;
			}
		case ADD_REFERENCE_TEXTURE:
			{
				AddReferenceTexture* pPacket = (AddReferenceTexture*)GetReceivedData();
				Swapper(pPacket->m_DrawOrder);
				break;
			}
			
		case ADD_EXPOSURE_TEXTURE:
			{
				AddExposureTexture* pPacket = (AddExposureTexture*)GetReceivedData();
				Swapper(pPacket->m_DrawOrder);
				Swapper(pPacket->m_Exposure);
				break;
			}
			
		case BRING_CLIENTTEX_TO_FOREGROUND:
			{
				BringToForeground* pPacket = (BringToForeground*)GetReceivedData();
				Swapper(pPacket->m_TextureId);
				break;
			}
		case DELETE_CLIENT_TEXTURE:
			{
				DeleteClientTexture* pPacket = (DeleteClientTexture*)GetReceivedData();
				Swapper(pPacket->m_TextureId);
				break;
			}
		case MOVE_CLIENT_TEXTURE:
			{
				MoveClientTexture* pPacket = (MoveClientTexture*)GetReceivedData();
				Swapper(pPacket->m_TextureId);
				Swapper(pPacket->m_X);
				Swapper(pPacket->m_Y);
				break;
			}
		case SET_CLIENT_TEXTURE_DATA:
			{
				SetClientTextureData* pPacket = (SetClientTextureData*)GetReceivedData();
				Swapper(pPacket->m_TextureId);
				Swapper(pPacket->m_X);
				Swapper(pPacket->m_Y);
				Swapper(pPacket->m_Width);
				Swapper(pPacket->m_Height);
				break;
			}
		case GET_CLIENT_TEXTURE_DATA:
			{										
				GetClientTextureData* pPacket = (GetClientTextureData*)GetReceivedData();
				Swapper(pPacket->m_TextureId);
				Swapper(pPacket->m_X);
				Swapper(pPacket->m_Y);
				Swapper(pPacket->m_Width);
				Swapper(pPacket->m_Height);
				break;
			}
		case LIGHT_CLIENT_TEXTURE:
			{
				LightClientTexture* pPacket = (LightClientTexture*)GetReceivedData();
				Swapper(pPacket->m_TextureId);
				Swapper(pPacket->m_Exposure);
				break;
			}
		case SET_TEXTURE_EXPOSURE:
			{
				SetTextureExposure* pPacket = (SetTextureExposure*)GetReceivedData();
				Swapper(pPacket->m_TextureId);
				Swapper(pPacket->m_Exposure);
				Swapper(pPacket->m_ColorExponent);
				Swapper(pPacket->m_ColorOffset);
				Swapper(pPacket->m_DisplayExposure);
				Swapper(pPacket->m_UseGlobalExponents);
				break;
			}
		case SET_IGNORE_ALPHA:
			{
				SetIgnoreAlpha* pPacket = (SetIgnoreAlpha*)GetReceivedData();
				Swapper(pPacket->m_IgnoreAlpha);
				break;
			}
		case SET_DISPLAY_EXPOSURE:
			{
				SetDisplayExposure* pPacket = (SetDisplayExposure*)GetReceivedData();
				Swapper(pPacket->m_DisplayExposure);
				break;
			}
			
		case SCALE_CLIENT_TEXTURE:
			{
				ScaleClientTexture* pPacket = (ScaleClientTexture*)GetReceivedData();
				Swapper(pPacket->m_TextureId);
				Swapper(pPacket->m_Width);
				Swapper(pPacket->m_Height);
				break;
			}
		case SET_CLIENT_PPP:
			{
				SetClientPPP* pPacket = (SetClientPPP*)GetReceivedData();
				ByteSwapPPPPreset(pPacket->m_PPPPreset);
				break;
			}
		default:
			break;
		}

	}
	return numBytesRead;
}
// Helper function to byteswap the post processing pipeline presets for the XENON
void ByteSwapPPPPreset(rage::grPostFX::grcPPPPreset &preset)
{

	int i=(int)preset.m_iChooseTechnique;
	Swapper(i);			// chooses different techniques ... this is in there to demonstrate the capabilities
	preset.m_iChooseTechnique = (rage::pppEffects) i;

	Swapper(preset.m_ClearColor);				// color for color correction
	Swapper(preset.m_ColorCorrect);				// color for color correction
	Swapper(preset.m_ConstAdd);					// add a color for color correction
	Swapper(preset.m_DofParamters);
	Swapper(preset.m_Scale);					// scaling parameter for radial blur
//	Swapper(preset.m_Exposure);					// variable that holds the exposure value
	Swapper(preset.m_BrightPassThreshold);		// threshold value for bright pass
	Swapper(preset.m_BrightPassOffset);			// offset value for bright pass
	Swapper(preset.m_MiddleGray);				// The middle gray key value. This value is used in the bright pass filter and the tone mapping stage
	Swapper(preset.m_BrightPassThresholdTwo);	// threshold value for bright pass
	Swapper(preset.m_BrightPassOffsetTwo);		// offset value for bright pass
	Swapper(preset.m_MiddleGrayTwo);			// The middle gray key value. This value is used in the bright pass filter and the tone mapping stage
	Swapper(preset.m_fBrightPassWhiteTwo);		// dedicated white value for the bright pass
	Swapper(preset.m_fAdapt);					// adaption variable for simulating the bright/dark adaption
	Swapper(preset.m_fBloomIntensity);			// holds value for bloom intensity.
	Swapper(preset.m_deSat);					// de-saturate color by lerping between color and monochrome
	Swapper(preset.m_deSatTwo);					// Police Camera: de-saturate color by lerping between color and monochrome
	Swapper(preset.m_fWhite);					// this is used to minimize loss of contrast when tone mapping a low-dynamic range
	Swapper(preset.m_fBrightPassWhite);			// dedicated white value for the bright pass
	Swapper(preset.m_fContrast);				// contrast
	Swapper(preset.m_fContrastTwo);				// Police Camera: contrast
	Swapper(preset.m_BlueShiftColor);			// Night blue shift color
	Swapper(preset.m_BlueShiftParams);			// Night blue shift parameters r=scale, g=offset, b=light adaption value scale
	Swapper(preset.m_bLightAdaption);			// switches light adaption on and off
	Swapper(preset.m_fGrainIntensity);			// film grain effect: intensity
	Swapper(preset.m_fInterlaceIntensity);			// film grain effect: intensity
	Swapper(preset.m_fTimeFrame);				// film grain effect: time frame for going though the volume texture
	Swapper(preset.m_fInterference);			// interference due to offsetting texture look-ups
	Swapper(preset.m_SwitchPoliceCam);
	Swapper(preset.m_fGamma);					// gamma control
	Swapper(preset.m_fLowerLimitAdaption);		// lower limit of the light adaption process
	Swapper(preset.m_fHigherLimitAdaption);		// higher limit of the light adaption process
	Swapper(preset.m_ShowRenderTargets);
	Swapper(preset.m_FogMinColor);				// Color at the top of fog
	Swapper(preset.m_FogMaxColor);				// Color at the bottom of fog
	Swapper(preset.m_FogParams);				// Fog parameters, x=top height in meters, y=bottom height in meters, z=fogdensity.
	Swapper(preset.m_BrightPassParams);			// Bright pass parameters, x=current kawase bloom pass, y=persistent fog tone down scaler
	Swapper(preset.m_ParticleFogParams);		// Fog parameters, x=top height in meters, y=bottom height in meters, z=fogdensity.
	Swapper(preset.m_StreakParams);				// streak parameters
	Swapper(preset.m_StreakAttenuation[MAX_STREAK_ITERATIONS*4]);
	Swapper(preset.m_StreakSamples);
	Swapper(preset.m_StreakIterations);			// number of iterations we are ping ponging in between
	Swapper(preset.m_BloomIterations);			// number of iterations we are ping ponging in between
	Swapper(preset.m_BloomIntensity);			// how much do we preserve between samples? (4 samples taken in shader, so should be 1.0/4.0)
	Swapper(preset.m_StreakIntensity);			// how much do we blend the streak into the frame buffer?
	Swapper(preset.m_NumStreaks);
	Swapper(preset.m_StreakEnabled);
	Swapper(preset.m_DistortionFreq);
	Swapper(preset.m_DistortionScale);
	Swapper(preset.m_DistortionRoll);
}

