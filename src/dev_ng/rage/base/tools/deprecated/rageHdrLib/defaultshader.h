#ifndef __DefaultShader_H
#define __DefaultShader_H
#include	"grcore/effect.h"
#include	"vector/color32.h"

using	namespace	rage;

namespace	rage
{
	class grmShader;
	class grcRenderTarget;
	class grcTexture;
}

#define MAX_EXPOSURES 14

class DefaultShader
{
public:

	void Init();
	void InitVars();
	void Blit(int x1,int y1,int x2,int y2,float z,float u1,float v1,float u2,float v2,Color32 color,const char *tech) const;
	void Shutdown();
	void SetInputTexture(grcTexture* inputTexture);
	void SetTextureExp(Vector3 &exponent);
	void SetTextureOfs(Vector3 &offset);
protected:
	grmShader* m_Shader;
	grcEffectVar m_TextureIdx;
	grcEffectVar m_TextureExpIdx;
	grcEffectVar m_TextureOfsIdx;
};

#endif
