#ifndef __UIELEMENT_H
#define __UIELEMENT_H
class UiElement
{
public:
	UiElement() : m_ScreenX(0), m_ScreenY(0), m_Width(0), m_Height(0) {}
	virtual void Draw() = 0;
	void SetScreenPos(int x, int y) { m_ScreenX = x; m_ScreenY = y; }
	void SetDimensions(int x, int y) { m_Width = x; m_Height = y; }
	int GetScreenX() { return m_ScreenX; }
	int GetScreenY() { return m_ScreenY; }
	int GetWidth() { return m_Width; }
	int GetHeight() { return m_Height; }
	//	virtual void Update();
protected:
	int m_ScreenX;
	int m_ScreenY;
	int m_Width;
	int m_Height;
};
#endif
