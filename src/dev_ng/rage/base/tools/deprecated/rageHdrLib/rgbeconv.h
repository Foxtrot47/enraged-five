inline Vector4 float2rgbe(Vector4 &rgb)
{
	float v;
	int e;
	Vector4 rgbe;

	v = rgb.x;
	if (rgb.y > v) v = rgb.y;
	if (rgb.z > v) v = rgb.z;
	if (v < 1e-32) {
		rgbe = Vector4(0.0,0.0,0.0,0.0);
	}
	else {
		v = frexp(v,&e) * 1.0f/v;
		rgbe.x = (rgb.x * v);
		rgbe.y = (rgb.y * v);
		rgbe.z = (rgb.z * v);
		rgbe.w = (e + 128.0f);
	}
	return rgbe;
}


inline Vector4 rgbe2float(Vector4 &rgbe)
{
	Vector4 rgb;
	float f;
	rgb.w = 0.0;
	if (rgbe.w) {   /*nonzero pixel*/
		f = ldexp(1.0f,(int)(rgbe.w-(128.0f)));
		rgb.x = rgbe.x * f;
		rgb.y = rgbe.y * f;
		rgb.z = rgbe.z * f;
		rgb *= (1.0f/255.0f);
	}
	else
	{
		rgb = Vector4(0.0,0.0,0.0,0.0);
	}
	return rgb;
}

