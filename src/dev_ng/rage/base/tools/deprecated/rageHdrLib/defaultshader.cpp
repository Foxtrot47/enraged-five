#include	"DefaultShader.h"
#include	"grcore/texture.h"
#include	"grmodel/shader.h"
#include	"grmodel/shaderfx.h"
#include	"grmodel/shadergroup.h"

#if __WIN32PC
#include "embedded_blit_textures_win32_30.h"
#elif __PPU
#include "embedded_blit_textures_psn.h"
#elif __XENON
#include "embedded_blit_textures_fxl_final.h"
#endif


void DefaultShader::SetInputTexture(grcTexture* inputTexture)	{ m_Shader->SetVar(m_TextureIdx, inputTexture); }
void DefaultShader::SetTextureExp(Vector3 &exponent)	{ m_Shader->SetVar(m_TextureExpIdx, exponent); }
void DefaultShader::SetTextureOfs(Vector3 &offset)	{ m_Shader->SetVar(m_TextureOfsIdx, offset); }


void DefaultShader::Init()
{
	m_Shader = grmShaderFactory::GetInstance().Create("blit_textures");
	m_Shader->Load("embedded:/blit_textures",0,0);
	InitVars();
}

void DefaultShader::InitVars()
{
	m_TextureIdx = m_Shader->LookupVar("ColorTex");
	m_TextureExpIdx = m_Shader->LookupVar("ColorTexExp");
	m_TextureOfsIdx = m_Shader->LookupVar("ColorTexOfs");
}
#define SCR_TO_VIEW_X(x) ( Clamp( (float)x/(GRCDEVICE.GetWidth()*0.5f)-1.0f, -1.0f, 1.0f) )
#define SCR_TO_VIEW_Y(x) ( Clamp( (float)-x/(GRCDEVICE.GetHeight()*0.5f)+1.0f, -1.0f, 1.0f) )


void DefaultShader::Blit(int x1,int y1,int x2,int y2,float z,float u1,float v1,float u2,float v2,Color32 color,const char *tech) const
{
	m_Shader->TWODBlit(SCR_TO_VIEW_X(x1),SCR_TO_VIEW_Y(y1),SCR_TO_VIEW_X(x2),SCR_TO_VIEW_Y(y2),z,u1,v1,u2,v2,color,m_Shader->LookupTechnique(tech));
}


void DefaultShader::Shutdown()
{
	delete m_Shader;
}
