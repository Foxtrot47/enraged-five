#ifndef __MERGETEXTURESSHADER_H
#define __MERGETEXTURESSHADER_H
#include	"grcore/effect.h"
#include	"vector/color32.h"

using	namespace	rage;

namespace	rage
{
	class grmShader;
	class grcRenderTarget;
	class grcTexture;
}

#define MAX_EXPOSURES 14

class MergeTexturesShader
{
public:

	void SetUseGlobalExponents(bool on);
	void SetConvertToRGBE(bool on);
	void SetConvertFromRGBE(bool on);
	void SetRenderExponents(bool on);
	void SetIgnoreAlpha(bool on);
	void SetDisplayExposure(float exposure);
	void SetInputMergeBit_RT(grcRenderTarget* inputRT);
	void SetInputRGBE_RT(grcRenderTarget* inputRT);
	void SetInputFloat_RT(grcRenderTarget* inputRT);
	void SetInputTexture(grcTexture* inputTexture, int idx);
	void SetColorExponent(Vector3& exponent);
	void SetColorOffset(Vector3& offset);
	void SetNumExposures(int numExposures);
	void SetExposure(int texIdx, float exposure);
	void Init();
	void InitVars();
	void Blit(int x1,int y1,int x2,int y2,float z,float u1,float v1,float u2,float v2,Color32 color,const char *tech) const;
	const char **GetShaderTexVars() { return sm_ShaderTexVars; }
	void Shutdown();
protected:
	grmShader* m_Shader;
//	grcEffect m_Effect;
	grcEffectVar m_MergePickingTexIdx;
	grcEffectVar m_UseGlobalExponentsIdx;
	grcEffectVar m_DisplayExposureIdx;
	grcEffectVar m_ColorExponentIdx;
	grcEffectVar m_ColorOffsetIdx;

	grcEffectVar m_ConvertToRGBEIdx;
	grcEffectVar m_ConvertFromRGBEIdx;
	grcEffectVar m_ShowExponentIdx;
	grcEffectVar m_IgnoreAlphaIdx;
	grcEffectVar m_RGBETextureIdx ;
	grcEffectVar m_ComposedFloatTextureIdx;
	grcEffectVar m_TextureIdx[MAX_EXPOSURES];
	grcEffectVar m_NumTexIdx;
	grcEffectVar m_ExposuresIdx;

	static const char* sm_ShaderTexVars[MAX_EXPOSURES];
	float m_Exposures[MAX_EXPOSURES];
	int m_NumExposures;
};

#endif
