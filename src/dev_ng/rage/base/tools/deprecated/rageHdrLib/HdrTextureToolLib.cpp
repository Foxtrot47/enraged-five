#if !__PPU
#include "system/xtl.h"

#if __WIN32
#include "system/wndproc.h"
#include <d3d9.h>
#include <D3d9types.h>
#include <D3dx9math.h>
#endif

#include	"hdrtexturetoollib.h"
#include	"system/param.h"
#include	"system/timemgr.h"
#include	"grmodel/setup.h"	// TEMPORARY
#include	"grmodel/shader.h"
#include	"grmodel/shaderfx.h"
#include	"grmodel/shadergroup.h"
#include	"grcore/effect.h"
#include	"grcore/state.h"
#include	"grcore/font.h"
#include	"grcore/image.h"
#include	"grcore/im.h"
#include	"grcore/texturepc.h"
#include	"grcore/dds.h"
#include	"bank/bank.h"
#include	"bank/bkmgr.h"
#include	"file/stream.h"
#include	"file/token.h"
#include	"file/asset.h"
#include 	"grpostfx/postfx.h"
#include	"ragehdrlib/hdrtextureset.h"
#include	"ragehdrlib/mergetexturesshader.h"
#include	"ragehdrlib/picktexturesshader.h"
#include	"ragehdrlib/maxexponentshader.h"
#include	"ragehdrlib/rgbeconv.h"
#include	"vector/color64.h"
#include	"data/struct.h"
#include	"hdrclientpipe.h"

#define MAX_PATH_LEN 512

#include	"devil/ilu.h"
#ifdef __cplusplus
extern "C" {
#endif
#include	"devil/il_internal.h"
#ifdef __cplusplus
}
#endif

#define USE_GRPOSTFX 1
#define __DISPLAY_PICK_TEX 0

using	namespace	rage;

#define	CPU_TRACE	0

#if	__WIN32PC
namespace	rage
{
	extern	bool	g_ManageTextures;
}
#endif

PARAM(listfile, "A file list to batch convert textures");
PARAM(client,"Run HDR tool in client mode on the XENON");
PARAM(server,"Run HDR tool in server mode on the PC and kick off XENON client");
PARAM(noclient,"Run HDR tool in server mode on the PC and DO NOT kick off XENON client");

#if __BANK
atArray<bkSlider *> m_BkSliders;
bkBank* m_BkExposure = NULL;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Color32 white(1.0f,0.0f,1.0f,1.0f);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


grcEffectVar m_MaxExponentPickingTexIdx;
grcEffectVar m_MaxExponentExponentTexIdx;
grcEffectVar m_MaxExponentBlitTexIdx;

HDRTextureTool* HDRTextureTool::sm_CurrentInstance = NULL;
static rage::grPostFX::grcPPPPreset s_CurrentPPPPreset;		

void HDRTextureTool::ReleaseRenderTargets()
{
	if(m_PickTexturesRT)
		m_PickTexturesRT->Release();
	m_PickTexturesRT = NULL;
	if(m_MaxExponentRT)
		m_MaxExponentRT->Release();
	m_MaxExponentRT = NULL;
	if(m_ComposedRT)
		m_ComposedRT->Release();
	m_ComposedRT = NULL;
	if(m_SaveRT)
		m_SaveRT->Release();
	m_SaveRT = NULL;
#if __WIN32PC
	if(m_SaveTargetSurface)
		m_SaveTargetSurface->Release();
	m_SaveTargetSurface = NULL;
	if(m_FloatTargetSurface)
		m_FloatTargetSurface->Release();	
	m_FloatTargetSurface = NULL;
#endif
}
void HDRTextureTool::InitRenderTargets(int width, int height, bool initSaveRTOnly)
{
	m_BlitFromComposedFloat.SetDimensions(width,height);

	m_BlitRGBEToFloatTextureEndResult.SetDimensions(width,height);

	m_ComposedFloatToRGBETextureOnScreen.SetDimensions(width,height);

	m_ComposeFloatTexture.SetDimensions(width,height);

	m_ComposedFloatToRGBETexture.SetDimensions(width,height);


	grcTextureFactory::CreateParams params;
	params.Multisample = 0;
	params.HasParent = true;
	params.Parent = NULL; 
	params.UseFloat = true;
	params.Lockable = true;

	params.Format = grctfA8R8G8B8;
	m_SaveRT = grcTextureFactory::GetInstance().CreateRenderTarget("Final RGBE texture", grcrtPermanent, width,height, 32, &params);
#if __WIN32PC
	GRCDEVICE.GetCurrent()->CreateOffscreenPlainSurface(width,height,D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, &m_SaveTargetSurface,NULL);
#endif
	if(!initSaveRTOnly)
	{
		params.Format = grctfA16B16G16R16F;
		m_PickTexturesRT = grcTextureFactory::GetInstance().CreateRenderTarget("Exposed FS", grcrtPermanent, width,height, 64, &params);
		m_MaxExponentRT = grcTextureFactory::GetInstance().CreateRenderTarget("High Exponent FS", grcrtPermanent, width,height, 64, &params);
		m_ComposedRT = grcTextureFactory::GetInstance().CreateRenderTarget("Final float texture", grcrtPermanent, width,height, 64, &params);
#if __WIN32PC
		GRCDEVICE.GetCurrent()->CreateOffscreenPlainSurface(width,height,D3DFMT_A16B16G16R16F, D3DPOOL_SYSTEMMEM, &m_FloatTargetSurface,NULL);
#endif
	}
}

void HDRTextureTool::ReleaseAllTextures()
{

	DeleteAllClientTextures();
	hdrClientPipe::DeleteClientTexture packet;
	if(m_Pipe && m_IsServer)
	{
		packet.m_TextureId = -1;
		m_Pipe->Send(packet);
		m_Pipe->WaitForAck();					
	}
	if(m_ExposureTextureSet->GetNumTextures())
	{
		m_ExposureTextureSet->Shutdown();
		m_ExposureTextureSet->Init(MAX_EXPOSURES);
#if __BANK
		while(m_BkSliders.GetCount())
		{
			if(m_BkExposure)
			{
				m_BkExposure->Remove(*((rage::bkWidget*)(m_BkSliders[0])));
				m_BkSliders.Delete(0);
			}
		}
#endif
	}
	m_DisplayHDR_DDS = false;
}



void HDRTextureTool::AssignTexturesToShaders(int id)
{
	if(id >= 0) 
	{
		hdrExposureTexture& tex = m_ExposureTextureSet->GetTexture(id);
		m_PickTexturesShader->SetInputTexture(tex.GetTexture(), id);
		m_MergeTexturesShader->SetInputTexture(tex.GetTexture(), id);
		m_PickTexturesShader->SetNumTextures(m_ExposureTextureSet->GetNumTextures());
		m_MergeTexturesShader->SetNumExposures(m_ExposureTextureSet->GetNumTextures());
		for(int i=0; i<m_ExposureTextureSet->GetNumTextures(); i++)
		{
			m_MergeTexturesShader->SetExposure(i, m_ExposureTextureSet->GetTexture(i).GetExposure());
		}
	}
}
grcImage* LoadTif(const char *filename, bool* pHasAlpha, int width, int height, float /*gammaCorrect*/)
{

	grcImage *image = NULL;
	grcImage *retval = NULL;
	rage::TextureConvert* tconv = HDRTextureTool::sm_CurrentInstance->m_TextureConvert;
	
	if(strcmpi(ASSET.FindExtensionInPath(filename), ".dds")==0)
	{
		retval = grcImage::Load(filename);
	}
	else if(tconv->loadTexture(filename))
	{
		tconv->gammaCorrect(1.0f);
		TextureConvert::AlphaCheckResult alphaRes = tconv->hasAlpha();
		if( (alphaRes == TextureConvert::RTC_HASMULTIBITALPHA) || (alphaRes == TextureConvert::RTC_HASONEBITALPHA) )
			*pHasAlpha = true;
		else
			*pHasAlpha = false;

		if(!width || !height)
		{
			width = tconv->getWidth();
			height = tconv->getHeight();
		}
		int mWidth = width;
		int mHeight = height;
		int mips = 0;
		while(mWidth > 1 && mHeight > 1)
		{
			mWidth = mWidth >> 1;
			mHeight = mHeight >> 1;
			mips ++;
		}
		rage::TCConvertOptions tcOps;
		tcOps.Format = D3DFMT_A32B32G32R32F;
		tcOps.Width = width;
		tcOps.Height = height;
		tcOps.MipLevels = mips;
		tconv->convertToFormat(tcOps);
		D3DLOCKED_RECT rect;
		LPDIRECT3DTEXTURE9      pTexture = tconv->GetTexture();

		pTexture->LockRect(0,&rect,0,0);
		pTexture->UnlockRect(0);
		
		retval = image = grcImage::Create(width,height,1, grcImage::A32B32G32R32F, grcImage::STANDARD,mips,0);
		int level=0;
		while(image && mips--)
		{
			pTexture->LockRect(level,&rect,0,0);
			u8* bits = image->GetBits();
			memcpy(bits, rect.pBits, width*height*16);
			width >>= 1;
			height >>= 1;
			image = image->GetNext();
			pTexture->UnlockRect(level++);
		}
	}

	return retval;
}

int HDRTextureTool::AddExposureTexture(const char* filename, float exposure, int draworder, int width, int height, float gammaCorrect)
{
	int id = -1;
	char tifFilename[256];
	const char* fileExt = ASSET.FindExtensionInPath(filename);
	fileExt++;

	ASSET.FullPath(tifFilename, 256, filename, fileExt);
	
	bool hasAlpha = false;
	grcImage* image = LoadTif(tifFilename, &hasAlpha, width, height, gammaCorrect);
	if(image)
	{
		id = m_ExposureTextureSet->AddTexture(image, exposure, filename, hasAlpha, draworder);
		image->Release();
	}
	if(id<0)
	{
		id = m_ExposureTextureSet->AddTexture(filename, exposure, draworder);
	}
	if(id>=0)
	{
		AssignTexturesToShaders(id);
		hdrExposureTexture& tex = m_ExposureTextureSet->GetTexture(id);
		m_ColorExponent.Set(tex.GetTexture()->GetColorExponent());
		m_ColorOffset.Set(tex.GetTexture()->GetColorOffset());
		// if DDS file has an exponent, then we need to do things differently because it is an HDR texture
		if(m_ColorExponent.x != 1.0f || m_ColorExponent.y != 1.0f || m_ColorExponent.z != 1.0f ||
			m_ColorOffset.x != 0.0f || m_ColorOffset.y != 0.0f || m_ColorOffset.z != 0.0f) 
		{
			if(m_DisplayHDR_DDS || m_ExposureTextureSet->GetNumTextures() > 1)
			{
				Warningf("Only one HDR texture can be loaded at a time!");
				return -1;
			}
			m_DisplayHDR_DDS = true;
		}
#if __BANK
		char buf[64];
		formatf(buf, 64, "%d: %s",id, filename);
		if(m_BkExposure)
		{
			m_BkSliders.Grow(1) =	m_BkExposure->AddSlider(buf, &tex.GetExposure(), -24.0f, 24.0f, 0.05f,datCallback(MFA1(HDRTextureTool::ComputeColorExponent), this, (int*)id));
		}
#endif
	}
	return id;
}

int HDRTextureTool::AddReferenceTexture(const char* filename, float exposure, int draworder, int width, int height, float gammaCorrect)
{
	int id = -1;
	char tifFilename[256];
	ASSET.FullPath(tifFilename, 256, filename, "tif");
	bool hasAlpha = false;
	grcImage* image = LoadTif(tifFilename, &hasAlpha, width, height, gammaCorrect);
	if(image)
	{
		id = m_ReferenceTextureSet->AddTexture(image, exposure, filename, hasAlpha, draworder);
		image->Release();
	}
	if(id<0)
	{
		id = m_ReferenceTextureSet->AddTexture(filename, exposure, draworder);
	}
	return id;
}

#if __WIN32PC
namespace rage {
extern int g_NewWidth;
};

bool OpenFileDialog(char *file, const char *title, const char *filter)
{
	OPENFILENAME OpenFileName;
	char szFile[MAX_PATH_LEN];
	char CurrentDir[MAX_PATH_LEN];
    ZeroMemory(&OpenFileName, sizeof(OpenFileName));

	szFile[0] = 0;
	GetCurrentDirectory( MAX_PATH_LEN, CurrentDir );

	OpenFileName.lStructSize = sizeof( OPENFILENAME );
	OpenFileName.hwndOwner = NULL;
    OpenFileName.lpstrFilter = filter;
	OpenFileName.lpstrCustomFilter = NULL;
	OpenFileName.nMaxCustFilter = 0;
	OpenFileName.nFilterIndex = 0;
	OpenFileName.lpstrFile = szFile;
	OpenFileName.nMaxFile = sizeof( szFile );
	OpenFileName.lpstrFileTitle = NULL;
	OpenFileName.nMaxFileTitle = 0;
	OpenFileName.lpstrInitialDir = CurrentDir;
	OpenFileName.lpstrTitle = title;
	OpenFileName.nFileOffset = 0;
	OpenFileName.nFileExtension = 0;
	OpenFileName.lpstrDefExt = NULL;
	OpenFileName.lCustData = 0;
	OpenFileName.lpfnHook = NULL;
	OpenFileName.lpTemplateName = NULL;
	OpenFileName.Flags = OFN_FILEMUSTEXIST | OFN_EXPLORER;
 
	if( GetOpenFileName( &OpenFileName ) )
	{
		strcpy( file, szFile );
		return true;
	}
	else
		return false;
}

bool SaveFileDialog(char *file, const char *title, const char *filter)
{
	OPENFILENAME SaveFileName;
	char szFile[MAX_PATH_LEN];
	char CurrentDir[MAX_PATH_LEN];
    ZeroMemory(&SaveFileName, sizeof(SaveFileName));

	szFile[0] = 0;
	GetCurrentDirectory( MAX_PATH_LEN, CurrentDir );

	SaveFileName.lStructSize = sizeof( OPENFILENAME );
	SaveFileName.hwndOwner = NULL;
    SaveFileName.lpstrFilter = filter;
	SaveFileName.lpstrCustomFilter = NULL;
	SaveFileName.nMaxCustFilter = 0;
	SaveFileName.nFilterIndex = 0;
	SaveFileName.lpstrFile = szFile;
	SaveFileName.nMaxFile = sizeof( szFile );
	SaveFileName.lpstrFileTitle = NULL;
	SaveFileName.nMaxFileTitle = 0;
	SaveFileName.lpstrInitialDir = CurrentDir;
	SaveFileName.lpstrTitle = title;
	SaveFileName.nFileOffset = 0;
	SaveFileName.nFileExtension = 0;
	SaveFileName.lpstrDefExt = NULL;
	SaveFileName.lCustData = 0;
	SaveFileName.lpfnHook = NULL;
	SaveFileName.lpTemplateName = NULL;
	SaveFileName.Flags = OFN_EXPLORER | OFN_OVERWRITEPROMPT;
 
	if( GetSaveFileName( &SaveFileName ) )
	{
		strcpy( file, szFile );
		return true;
	}
	else
		return false;
}
#endif

void HDRTextureTool::LoadTexture(void *)
{
#if __WIN32PC
	OPENFILENAME OpenFileName;
	char szFile[MAX_PATH_LEN];
	char CurrentDir[MAX_PATH_LEN];
    ZeroMemory(&OpenFileName, sizeof(OpenFileName));

	szFile[0] = 0;
	GetCurrentDirectory( MAX_PATH_LEN, CurrentDir );

	OpenFileName.lStructSize = sizeof( OPENFILENAME );
	OpenFileName.hwndOwner = NULL;
    OpenFileName.lpstrFilter = "TIF Files (*.tif)\0*.tif\0DDS Files (*.dds)\0*.dds\0All Files (*.*)\0*.*\0";
	OpenFileName.lpstrCustomFilter = NULL;
	OpenFileName.nMaxCustFilter = 0;
	OpenFileName.nFilterIndex = 0;
	OpenFileName.lpstrFile = szFile;
	OpenFileName.nMaxFile = sizeof( szFile );
	OpenFileName.lpstrFileTitle = NULL;
	OpenFileName.nMaxFileTitle = 0;
	OpenFileName.lpstrInitialDir = CurrentDir;
	OpenFileName.lpstrTitle = "Load Texture";
	OpenFileName.nFileOffset = 0;
	OpenFileName.nFileExtension = 0;
	OpenFileName.lpstrDefExt = NULL;
	OpenFileName.lCustData = 0;
	OpenFileName.lpfnHook = NULL;
	OpenFileName.lpTemplateName = NULL;
	OpenFileName.Flags = OFN_FILEMUSTEXIST | OFN_ALLOWMULTISELECT | OFN_EXPLORER;

	if( GetOpenFileName( &OpenFileName ) )
	{
		GetCurrentDirectory( MAX_PATH_LEN, CurrentDir );
		if(strcmp(CurrentDir, szFile))
		{
			LoadExposureTexture(szFile);
		}
		else
		{
			char dir[MAX_PATH_LEN];
			strcpy(dir, szFile);
			strcat(dir,"\\");
			int size = strlen(szFile);
			bool moreFiles = true;
			while(moreFiles)
			{
				char* file = szFile + size + 1;
				int fileSize = strlen(file);
				if(!fileSize)  
					moreFiles = false;
				else
				{
					size += (fileSize + 1);
					char openName[MAX_PATH_LEN];
					sprintf(openName, "%s%s", dir, file);
					LoadExposureTexture(openName);
				}
			}
		}
	}

#endif
}

#if __WIN32PC
LRESULT CALLBACK TextureToolWndProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	static int startx;
	static int starty;

	int textureID = 0;
	HDRTextureTool* inst = HDRTextureTool::sm_CurrentInstance;
	for(textureID=0; textureID < inst->m_ReferenceTextureSet->GetNumTextures(); textureID++)
	{
		if(hwnd == inst->m_ReferenceTextureSet->GetTexture(textureID).GetHWND())
		{
			break;
		}
	}
	if(textureID >= inst->m_ReferenceTextureSet->GetNumTextures())
	{
		//must be the main window
		textureID = -1;
	}
	{
		switch(msg)
		{
			case WM_PAINT:
			{
				if(textureID >= 0)
				{
					hdrExposureTexture& tex = inst->m_ReferenceTextureSet->GetTexture(textureID);
					GRCDEVICE.BeginFrame();
					g_hwndOverride = tex.GetHWND();
					tex.GetViewport().Screen();
					grcViewport::SetCurrent(&tex.GetViewport());
					GRCDEVICE.Clear(true,Color32(0.5f,0.5f,0.5f,0.0f), false,1.f,false);
					inst->m_DefaultShader->SetInputTexture(tex.GetTexture());	
					inst->m_DisplayTexture[textureID].Draw();
					GRCDEVICE.EndFrame();
					g_hwndOverride = NULL;
				}
				break;
			}
			case WM_CLOSE:
			{
				if(textureID >= 0)
				{
					hdrClientPipe::DeleteClientTexture packet;
					packet.m_TextureId = textureID;
					if(inst->m_Pipe)
					{
						inst->m_Pipe->Send(packet);
					}
					inst->m_ReferenceTextureSet->RemoveTexture(textureID);
					inst->m_DisplayTexture.Delete(textureID);
					if(inst->m_Pipe)
					{
						inst->m_Pipe->WaitForAck();					
					}
				}
				break;
			}
			case WM_LBUTTONDOWN:
				// only do this if we are networked (client/server mode) and the control key has been pressed
				if(inst->m_Pipe && wParam & MK_CONTROL)
				{
					// if texture ID is less than 0, then we want to bring the multiple exposure texture to the foreground
					if(textureID < 0)
					{
						if(inst->m_ExposureTextureSet->GetNumTextures())
						{
							inst->m_ExposureTextureSet->GetTexture(0).SetDrawOrder(inst->m_ReferenceTextureSet->GetNumTextures());
						}
					}
					// if texture ID is not less than 0, then we want to bring a reference texture to the foreground
					else if(inst->m_ExposureTextureSet->GetNumTextures() > 0)
					{
						hdrExposureTexture& exptex = inst->m_ExposureTextureSet->GetTexture(0);
						// only do this if the reference texture isnt already in the foreground
						if((!inst->m_ReferenceTextureSet->IsInForeground(textureID) ||
							// or the multiple exposure HDR texture is in the foreground
							exptex.GetDrawOrder() == inst->m_ReferenceTextureSet->GetNumTextures()))
						{
							// bring the proper reference texture to the foreground
							inst->m_ReferenceTextureSet->BringToForeground(textureID);
							// if we have a multiple exposure HDR texture then
							if(inst->m_ExposureTextureSet->GetNumTextures())
							{
								// push it one level to the backtground.
								int draworder = exptex.GetDrawOrder();
								if(draworder > 0) exptex.SetDrawOrder(draworder-1);
							}
						}
					}
					// now lets let the client know we are doing this.
					hdrClientPipe::BringToForeground packet;
					packet.m_TextureId = textureID;
					inst->m_Pipe->Send(packet);
				}
				break;
			case WM_MOUSEMOVE:
				if(wParam & MK_LBUTTON)
				{
					if((wParam & MK_SHIFT) && (wParam & MK_CONTROL) )
					{
						if(textureID >= 0)
						{
							hdrExposureTexture& tex = inst->m_ReferenceTextureSet->GetTexture(textureID);
							int x;
							int y;
							if(startx != -10000000)
							{
								x = LOWORD(lParam) - startx;
								y = HIWORD(lParam) - starty;
							} else {
								x = 0;
								y = 0;
							}
							hdrClientPipe::LightClientTexture packet;
							float adjust = (float)y * 0.01f;
							tex.GetExposure() += adjust;

							packet.m_Exposure = tex.GetExposure();
							packet.m_TextureId = textureID;
							if(inst->m_Pipe)
							{
								inst->m_Pipe->Send(packet);
							}
							startx = LOWORD(lParam);
							starty = HIWORD(lParam);

							// update the text of our current window with the new exposure value
							char tbuf[256];
							GetWindowText(hwnd,tbuf,256);
							int i=0;
							while(tbuf[i] && tbuf[i] != ' ') i++;
							tbuf[i] = 0;
							sprintf(tbuf, "%s %f", tbuf, tex.GetExposure());
							SetWindowText(hwnd, tbuf);
						}
					}
					else
					{
						hdrClientPipe::MoveClientTexture packet;
						if(startx != -10000000)
						{
							packet.m_X = LOWORD(lParam) - startx;
							packet.m_Y = HIWORD(lParam) - starty;
						} else {
							packet.m_X = 0;
							packet.m_Y = 0;
						}
						packet.m_TextureId = textureID;
						if(inst->m_Pipe)
						{
							inst->m_Pipe->Send(packet);
						}
						startx = LOWORD(lParam);
						starty = HIWORD(lParam);
						WINDOWPLACEMENT plc;
						GetWindowPlacement(hwnd, &plc);
					}

					break;
				} else 
				if(wParam & MK_MBUTTON)
				{
					break;
				} else 
				if(wParam & MK_RBUTTON)
				{
					hdrClientPipe::ScaleClientTexture packet;
					packet.m_TextureId = textureID;
					
					if(startx != -10000000)
					{
						packet.m_Width = LOWORD(lParam) - startx;
						packet.m_Height = HIWORD(lParam) - starty;
						if(wParam & MK_SHIFT)
						{
							if(abs(packet.m_Width) > abs(packet.m_Height))
							{
								packet.m_Height = packet.m_Width;
							}
							else
							{
								packet.m_Width = packet.m_Height;
							}
						}
					} else {
						packet.m_Width = 0;
						packet.m_Height = 0;
					}
					if(inst->m_Pipe)
					{
						inst->m_Pipe->Send(packet);
					}
					startx = LOWORD(lParam);
					starty = HIWORD(lParam);
					break;
				} else {
					startx = -10000000;
				}
				

		}
	}
	return DefWindowProc(hwnd,msg,wParam,lParam);
}
#endif

#if __WIN32PC
void HDRTextureTool::LoadClientTextureMultiDialog()
{
	OPENFILENAME OpenFileName;
	char szFile[MAX_PATH_LEN];
	char CurrentDir[MAX_PATH_LEN];
    ZeroMemory(&OpenFileName, sizeof(OpenFileName));

	szFile[0] = 0;
	GetCurrentDirectory( MAX_PATH_LEN, CurrentDir );

	OpenFileName.lStructSize = sizeof( OPENFILENAME );
	OpenFileName.hwndOwner = NULL;
    OpenFileName.lpstrFilter = "DDS Files (*.dds)\0*.dds\0TIF Files (*.tif)\0*.tif\0All Files (*.*)\0*.*\0";
	OpenFileName.lpstrCustomFilter = NULL;
	OpenFileName.nMaxCustFilter = 0;
	OpenFileName.nFilterIndex = 0;
	OpenFileName.lpstrFile = szFile;
	OpenFileName.nMaxFile = sizeof( szFile );
	OpenFileName.lpstrFileTitle = NULL;
	OpenFileName.nMaxFileTitle = 0;
	OpenFileName.lpstrInitialDir = CurrentDir;
	OpenFileName.lpstrTitle = "Load Texture";
	OpenFileName.nFileOffset = 0;
	OpenFileName.nFileExtension = 0;
	OpenFileName.lpstrDefExt = NULL;
	OpenFileName.lCustData = 0;
	OpenFileName.lpfnHook = NULL;
	OpenFileName.lpTemplateName = NULL;
	OpenFileName.Flags = OFN_FILEMUSTEXIST | OFN_ALLOWMULTISELECT | OFN_EXPLORER;

	if( GetOpenFileName( &OpenFileName ) )
	{
		GetCurrentDirectory( MAX_PATH_LEN, CurrentDir );
		if(strcmp(CurrentDir, szFile))
		{
			LoadClientTexture(szFile);
		}
		else
		{
			char dir[MAX_PATH_LEN];
			strcpy(dir, szFile);
			strcat(dir,"\\");
			int size = strlen(szFile);
			bool moreFiles = true;
			while(moreFiles)
			{
				char* file = szFile + size + 1;
				int fileSize = strlen(file);
				if(!fileSize)  
					moreFiles = false;
				else
				{
					size += (fileSize + 1);
					char openName[MAX_PATH_LEN];
					sprintf(openName, "%s%s", dir, file);
					LoadClientTexture(openName);
				}
			}
		}
	}
}
#endif


void HDRTextureTool::LoadExposureTexture(const char *filename, int draworder, float exposure)
{
	if(m_Pipe) 
	{
		hdrClientPipe::AddExposureTexture packet;
		packet.m_DrawOrder = draworder;
		packet.m_Exposure = exposure;
		strcpy(packet.m_Filename, filename);
		m_Pipe->Send(packet);
	}
	AddExposureTexture(filename, exposure);
	if(m_Pipe) 
	{
		m_Pipe->WaitForAck();
	}
}

#if __WIN32PC
void HDRTextureTool::LoadClientTexture(const char* filename, int draworder )
{
	WNDCLASSEX wcx;
    wcx.cbSize = sizeof(wcx);          // size of structure 
    wcx.style = CS_HREDRAW | 
        CS_VREDRAW;                    // redraw if size changes 
    wcx.lpfnWndProc = TextureToolWndProc;     // points to window procedure 
    wcx.cbClsExtra = 0;                // no extra class memory 
    wcx.cbWndExtra = 0;                // no extra window memory 
    wcx.hInstance = ::GetModuleHandle(0);         // handle to instance 
    wcx.hIcon = LoadIcon(NULL, 
        IDI_APPLICATION);              // predefined app. icon 
    wcx.hCursor = LoadCursor(NULL, 
        IDC_ARROW);                    // predefined arrow 
    wcx.hbrBackground = NULL;          // white background brush 
    wcx.lpszMenuName =  "MainMenu";    // name of menu resource 
    wcx.lpszClassName = "STATIC";  // name of window class 
    wcx.hIconSm = NULL; 
 
    // Register the window class. 
 
	RegisterClassEx(&wcx); 
	if(m_Pipe) 
	{
		hdrClientPipe::AddReferenceTexture packet;
		packet.m_DrawOrder = draworder;
		strcpy(packet.m_Filename, filename);
		m_Pipe->Send(packet);
	}
	int i = AddReferenceTexture(filename, 0.0f);
	if(i>=0)
	{
		hdrExposureTexture& tex = m_ReferenceTextureSet->GetTexture(i);
		char tbuf[256];
		sprintf(tbuf, "%s %f", filename, tex.GetExposure());
		tex.SetHWND(::CreateWindow("STATIC",tbuf,WS_OVERLAPPEDWINDOW ,30*(i),30*(i),tex.GetTexture()->GetWidth(),tex.GetTexture()->GetHeight(),g_hwndMain,NULL,NULL,0));
		ShowWindow(tex.GetHWND(), SW_SHOW);
		DisplayTexture& displayTex = m_DisplayTexture.Append();
		displayTex.SetDimensions((int)((float)tex.GetTexture()->GetWidth() / 
			grcViewport::GetCurrent()->GetAspectRatio()),tex.GetTexture()->GetHeight());		
		displayTex.SetScreenPos(0,0);
	}
	if(m_Pipe) 
	{
		m_Pipe->WaitForAck();
	}
}
#else
void HDRTextureTool::LoadClientTexture(const char* /*filename*/, int /*draworder*/ )
{
}
#endif

struct ILHeader
{
	u16 m_WidthL;
	u16 m_WidthH;
	u16 m_HeightL;
	u16 m_HeightH;
	u16 m_DepthL;
	u16 m_DepthH;
	u8 m_Bpp;
	u8 m_Bpc;
};

void HDRTextureTool::SaveWorkspace()
{
#if __WIN32PC
	if(!SaveFileDialog(m_BufferWorkspaceFile, "Save Workspace", "HDR Workspace Files (*.hdrproj)\0*.hdrproj\0"))
		return;
	Displayf("Saving... %s", m_BufferWorkspaceFile);
	fiStream* workspaceFile = ASSET.Create(m_BufferWorkspaceFile, "hdrproj", false);
	if(workspaceFile)
	{
		fiTokenizer workspace("Workspace", workspaceFile);
		workspace.PutStr("HDR Texture Tool Workspace file Version %d\n", m_Version);
		workspace.Put("NumExposures\t");
		workspace.Put(m_ExposureTextureSet->GetNumTextures());
		workspace.Put("\nColorExponent\t");
		workspace.Put(m_ColorExponent);
		workspace.Put("\nColorOffset\t");
		workspace.Put(m_ColorOffset);
		hdrClientPipe::GetClientTextureData packet;
		hdrClientPipe::Command command;
		hdrClientPipe::GetClientTextureData* pPacket;
		packet.m_TextureId = -1;
		if(m_Pipe)
		{
			m_Pipe->Send(packet);
			m_Pipe->Receive(command, true);
			Assert(command == hdrClientPipe::GET_CLIENT_TEXTURE_DATA);
			pPacket = (hdrClientPipe::GetClientTextureData*)m_Pipe->GetReceivedData();
		}
		else
		{
			pPacket = (hdrClientPipe::GetClientTextureData *)&m_BackupExposureTextureLocation;
		}
		workspace.Put("\nclientTextureX\t");
		workspace.Put(pPacket->m_X);
		workspace.Put("\nclientTextureY\t");
		workspace.Put(pPacket->m_Y);
		workspace.Put("\nclientTextureWidth\t");
		workspace.Put(pPacket->m_Width);
		workspace.Put("\nclientTextureHeight\t");
		workspace.Put(pPacket->m_Height);

		for(int i=0; i<m_ExposureTextureSet->GetNumTextures(); i++)
		{
			hdrExposureTexture& tex = m_ExposureTextureSet->GetTexture(i);
			workspace.Put("\n");
			workspace.Put(tex.GetTextureName());
			workspace.Put("\t\t");
			workspace.Put(tex.GetDrawOrder());
			workspace.Put("\t\t");
			workspace.Put(tex.GetExposure());
		}
		workspace.Put("\n\nNumClientTextures\t");
		workspace.Put(m_ReferenceTextureSet->GetNumTextures());
		for(int i=0; i<m_ReferenceTextureSet->GetNumTextures(); i++)
		{
			hdrExposureTexture& tex = m_ReferenceTextureSet->GetTexture(i);
			workspace.Put("\n\nTextureName\t");
			workspace.Put(tex.GetTextureName());
			workspace.Put("\nExposure\t");
			workspace.Put(tex.GetExposure());
			workspace.Put("\nDrawOrder\t");
			workspace.Put(tex.GetDrawOrder());

			WINDOWPLACEMENT plc;
			GetWindowPlacement(tex.GetHWND(), &plc);
			workspace.Put("\nflags\t");
			workspace.Put((int)plc.flags);
			workspace.Put("\nshowCmd\t");
			workspace.Put((int)plc.showCmd);
			workspace.Put("\nptMinPosition.x\t");
			workspace.Put(plc.ptMinPosition.x);
			workspace.Put("\nptMinPosition.y\t");
			workspace.Put(plc.ptMinPosition.y);
			workspace.Put("\nptMaxPosition.x\t");
			workspace.Put(plc.ptMaxPosition.x);
			workspace.Put("\nptMaxPosition.y\t");
			workspace.Put(plc.ptMaxPosition.y);
			workspace.Put("\nrcNormalPosition.left\t");
			workspace.Put(plc.rcNormalPosition.left);
			workspace.Put("\nrcNormalPosition.top\t");
			workspace.Put(plc.rcNormalPosition.top);
			workspace.Put("\nrcNormalPosition.right\t");
			workspace.Put(plc.rcNormalPosition.right);
			workspace.Put("\nrcNormalPosition.bottom\t");
			workspace.Put(plc.rcNormalPosition.bottom);
			packet.m_TextureId = i;
			if(m_Pipe)
			{
				m_Pipe->Send(packet);
				m_Pipe->Receive(command, true);
				Assert(command == hdrClientPipe::GET_CLIENT_TEXTURE_DATA);
				pPacket = (hdrClientPipe::GetClientTextureData*)m_Pipe->GetReceivedData();
			}
			else
			{
				pPacket = (hdrClientPipe::GetClientTextureData *)&m_BackupClientTextureLocations[i];
			}
			workspace.Put("\nclientTextureX\t");
			workspace.Put(pPacket->m_X);
			workspace.Put("\nclientTextureY\t");
			workspace.Put(pPacket->m_Y);
			workspace.Put("\nclientTextureWidth\t");
			workspace.Put(pPacket->m_Width);
			workspace.Put("\nclientTextureHeight\t");
			workspace.Put(pPacket->m_Height);
		}
		workspaceFile->Close();
	}
#endif
}

void HDRTextureTool::DeleteAllClientTextures()
{
#if __WIN32PC
	while(m_ReferenceTextureSet->GetNumTextures())
	{
		hdrClientPipe::DeleteClientTexture packet;
		if(m_Pipe)
		{
			packet.m_TextureId = 0;
			m_Pipe->Send(packet);
			m_Pipe->WaitForAck();					
		}
		::DestroyWindow(m_ReferenceTextureSet->GetTexture(0).GetHWND());
		m_ReferenceTextureSet->RemoveTexture(0);
		m_DisplayTexture.Delete(0);
	}
#endif
}

void HDRTextureTool::LoadWorkspaceFileDialog()
{
#if __WIN32PC
	if(!OpenFileDialog(m_BufferWorkspaceFile, "Load Workspace", "HDR Workspace Files (*.hdrproj)\0*.hdrproj\0"))
		return;
	LoadWorkspace();
#endif
}

void HDRTextureTool::LoadWorkspace()
{
#if __WIN32PC
	Displayf("Loading... %s", m_BufferWorkspaceFile);
	fiStream* workspaceFile = ASSET.Open(m_BufferWorkspaceFile, "hdrproj", false, true);
	if(workspaceFile)
	{
		m_BackupClientTextureLocations.Reset();
		hdrClientPipe::PauseClient packet;
		if(m_Pipe)
		{
			packet.m_Pause = true;
			m_Pipe->Send(packet);
			m_Pipe->WaitForAck();
		}

		DeleteAllTextures();

		fiTokenizer workspace("Workspace", workspaceFile);
		char token[MAX_STR_LENGTH];
		token[0] = 0;
		while(strcmp(token, "Version") && workspaceFile->Size() > workspaceFile->Tell())
			workspace.GetToken(token, MAX_STR_LENGTH);
		float version = workspace.GetFloat();
		int numExposures = 0;
		if(version != m_Version)
		{
			Errorf("Version %d does not match tool version %d!", version, m_Version);
		}
		else
		{
			DeleteAllTextures();
			numExposures = workspace.MatchInt("NumExposures");
			workspace.MatchVector("ColorExponent", m_ColorExponent);
			workspace.MatchVector("ColorOffset", m_ColorOffset);
			hdrClientPipe::SetClientTextureData packet;
			packet.m_TextureId = -1;
			packet.m_X = workspace.MatchInt("clientTextureX");
			packet.m_Y = workspace.MatchInt("clientTextureY");
			packet.m_Width = workspace.MatchInt("clientTextureWidth");
			packet.m_Height = workspace.MatchInt("clientTextureHeight");
			if(m_Pipe)
			{
				m_Pipe->Send(packet);
				m_Pipe->WaitForAck();
			}
			else
			{
				m_BackupExposureTextureLocation = packet;
			}
			for(int i=0; i<numExposures; i++)
			{
				workspace.GetToken(token, MAX_STR_LENGTH);
				int draworder = workspace.GetInt();
				float exposure = workspace.GetFloat();
				LoadExposureTexture(token, draworder, exposure);
			}
		}
		int clientTexCount = workspace.MatchInt("NumClientTextures");
		for(int i=0; i<clientTexCount; i++)
		{
			workspace.MatchToken("TextureName");
			workspace.GetToken(token, MAX_STR_LENGTH);
			float exposure = workspace.MatchFloat("Exposure");
			int draworder = workspace.MatchInt("DrawOrder");
			WINDOWPLACEMENT plc;
			plc.length = sizeof(plc);
			plc.flags = (UINT) workspace.MatchInt("flags");
			plc.showCmd = (UINT) workspace.MatchInt("showCmd");
			plc.ptMinPosition.x = workspace.MatchInt("ptMinPosition.x");
			plc.ptMinPosition.y = workspace.MatchInt("ptMinPosition.y");
			plc.ptMaxPosition.x = workspace.MatchInt("ptMaxPosition.x");
			plc.ptMaxPosition.y = workspace.MatchInt("ptMaxPosition.y");
			plc.rcNormalPosition.left = workspace.MatchInt("rcNormalPosition.left");
			plc.rcNormalPosition.top = workspace.MatchInt("rcNormalPosition.top");
			plc.rcNormalPosition.right = workspace.MatchInt("rcNormalPosition.right");
			plc.rcNormalPosition.bottom = workspace.MatchInt("rcNormalPosition.bottom");			

			if(!m_SoftwareMode)
			{
				LoadClientTexture(token, draworder);
				hdrExposureTexture& tex = m_ReferenceTextureSet->GetTexture(i);
				tex.GetExposure() = exposure;
				tex.SetDrawOrder(draworder);
				SetWindowPlacement(tex.GetHWND(), &plc);
			}

			hdrClientPipe::SetClientTextureData packet;
			packet.m_TextureId = i;
			packet.m_X = workspace.MatchInt("clientTextureX");
			packet.m_Y = workspace.MatchInt("clientTextureY");
			packet.m_Width = workspace.MatchInt("clientTextureWidth");
			packet.m_Height = workspace.MatchInt("clientTextureHeight");
			if(m_Pipe)
			{
				m_Pipe->Send(packet);
				m_Pipe->WaitForAck();
			}
			else
			{
				m_BackupClientTextureLocations.Grow() = packet;
			}
		}
//		Assert(m_ReferenceTextureSet->GetNumTextures() == clientTexCount);
		workspaceFile->Close();
		if(m_Pipe)
		{
			packet.m_Pause = false;
			m_Pipe->Send(packet);
			m_Pipe->WaitForAck();
		}
		if(!m_SoftwareMode)
		{
			ComputeColorExponent();
		}
	}
#endif
}

void HDRTextureTool::ExportTexture()
{
#if __WIN32PC
	if(m_ExportFileDialog)
	{
		if(!SaveFileDialog(m_BufferExportFile, "Export texture", "DDS Files (*.dds)\0*.dds\0All Files (*.*)\0*.*\0"))
		{
			return;
		}
	}
#endif
	ComputeColorExponent();
	if(m_SoftwareMode)
	{
		ComposeExposureTextures();
	}
	if(!m_SaveRT && !m_SoftwareMode)
	{
		Warningf("WARNING! Couldn't export %s!!!", m_BufferExportFile);
		return;
	}
	bool hasAlpha = m_ExposureTextureSet->HasAlpha();
	Displayf("Exporting... %s", m_BufferExportFile);

	D3DSURFACE_DESC desc;
	hdrExposureTexture& tex = m_ExposureTextureSet->GetTexture(0);
	if(m_SoftwareMode)
	{
		desc.Width = tex.GetTexture()->GetWidth();
		desc.Height = tex.GetTexture()->GetHeight();
	}
	else
	{
		static_cast<IDirect3DTexture9 *>(m_SaveRT->GetTexturePtr())->GetLevelDesc(0, &desc);
	}
	u8* pColBuffer = new u8[desc.Width * desc.Height * 4 + sizeof(ILHeader)];
	Assert(pColBuffer);
	ILHeader* pHeader = (ILHeader*)pColBuffer;
	pHeader->m_Bpp = 4;
	pHeader->m_Bpc = 1;
	pHeader->m_WidthL = (u16)desc.Width;
	pHeader->m_HeightL = (u16)desc.Height;
	pHeader->m_DepthL = 1;
	pHeader->m_WidthH = 
	pHeader->m_HeightH =
	pHeader->m_DepthH = 0;

#ifdef __SUPPORT_RGBE_PER_PIXEL // lets not deal with this for now TODO: fixme!
	u8* pExpBuffer = new u8[desc.Width * desc.Height * 1 + sizeof(ILHeader)];
	Assert(pExpBuffer);
	pHeader = (ILHeader*)pExpBuffer;
	pHeader->m_Bpp = 1;
	pHeader->m_Bpc = 1;
	pHeader->m_WidthL = (u16)(desc.Width>>1);
	pHeader->m_HeightL = (u16)(desc.Height>>1);
	pHeader->m_DepthL = 1;
	pHeader->m_WidthH = 
	pHeader->m_HeightH =
	pHeader->m_DepthH = 0;
#endif
#if __WIN32PC
	u8* pColData = pColBuffer + sizeof(ILHeader);
#ifdef __SUPPORT_RGBE_PER_PIXEL // lets not deal with this for now TODO: fixme!
	u8* pLumData = pExpBuffer + sizeof(ILHeader);
#endif

	IDirect3DSurface9 *pRTSurface = NULL;
	D3DLOCKED_RECT rect;
	if(!m_SoftwareMode)
	{
		static_cast<IDirect3DTexture9 *>(m_SaveRT->GetTexturePtr())->GetSurfaceLevel(0, &pRTSurface);
		GRCDEVICE.GetCurrent()->GetRenderTargetData(pRTSurface, m_SaveTargetSurface);
	}
	m_SaveTargetSurface->LockRect(&rect, NULL,0);
	{
		u8* colors = (u8*)rect.pBits;
		if(m_SoftwareMode)
		{
			memcpy(pColData, colors, desc.Width * desc.Height * 4);
		} else {
			for(unsigned int i=0; i< desc.Width * desc.Height * 4; i+=4, pColData+=4, colors+=4)
			{
				pColData[3] = colors[3]; //g
				pColData[2] = colors[0]; //r
				pColData[1] = colors[1]; //a
				pColData[0] = colors[2]; //b
			}
		}
#ifdef __SUPPORT_RGBE_PER_PIXEL // lets not deal with this for now TODO: fixme!
		for(unsigned int i=0; i< desc.Width * desc.Height * 4; i+=4, pColData+=4, colors+=4)
		{
			if( ((i/desc.Width) & 1*4) == 0 && (i & 1*4) == 0)
			{
				pLumData[0] = colors[3]; //a

				if(colors[3+4] > pLumData[0])
					pLumData[0] = colors[3+4]; //a
				if(colors[3+4*desc.Width] > pLumData[0])
					pLumData[0] = colors[3+4*desc.Width]; //a
				if(colors[3+4+4*desc.Width] > pLumData[0])
					pLumData[0] = colors[3+4+4*desc.Width]; //a

				pLumData++;
			}
		}
#endif
	}
	m_SaveTargetSurface->UnlockRect();
#endif
	// Save color data
	ILimage *colImage;
	colImage = ilNewImage(desc.Width, desc.Height, 1, 4, 1);
	ilReplaceCurImage(colImage);
	ilLoadRawL(pColBuffer, desc.Width * desc.Height * 4 + sizeof(ILHeader));
#define	__QUANTIZE_IMAGE 0
#if __QUANTIZE_IMAGE
	ilSetInteger(IL_MAX_QUANT_INDEXS, 256);
	colImage = iConvertImage(colImage, IL_COLOUR_INDEX, IL_BYTE);
	ilReplaceCurImage(colImage);
#endif
	ilSetInteger(IL_DXTC_FORMAT, hasAlpha ? IL_DXT5 :IL_DXT1);	// Set the dxt format
	iluBuildMipmaps();						// Generate mip-maps

	char outputTextureName[MAX_STR_LENGTH];
	strcpy(outputTextureName, m_BufferExportFile);
	if(stricmp(&outputTextureName[strlen(outputTextureName)-4],".dds"))
		strcat(outputTextureName, ".dds");

	ilSaveImage(ILstring(outputTextureName));
	fiStream *ddsTex;
	if((ddsTex = ASSET.Open(outputTextureName, "", false, false)) != 0)
	{
		DDSURFACEDESC2 dummy;
		float gamma = 2.2f;
		int Size = ddsTex->Size();
		ddsTex->Seek((int)&dummy.fGamma - (int)&dummy + 4); // +4 for magic number header.
		ddsTex->WriteFloat((float*)&gamma, 1);
		ddsTex->WriteFloat((float*)&m_ColorExponent, 3);
		ddsTex->WriteFloat((float*)&m_ColorOffset, 3);
		ddsTex->Seek(Size);
		ddsTex->Close();
	}
	
#ifdef __SUPPORT_RGBE_PER_PIXEL // lets not deal with this for now TODO: fixme!
	// Save exponent texture if necessary
	if(!m_UseGlobalExponents)
	{
		IDirect3DTexture9* pTexture;
		D3DXCreateTexture(GRCDEVICE.GetCurrent(),desc.Width>>1, desc.Height>>1, 1, 0, D3DFMT_L8, D3DPOOL_MANAGED, &pTexture);
		D3DLOCKED_RECT rect;
		pTexture->LockRect(0, &rect, NULL,0);
		memcpy(rect.pBits,	pExpBuffer + sizeof(ILHeader), (desc.Width>>1) * (desc.Height>>1));
		pTexture->UnlockRect(0);

		strcpy(outputTextureName, m_BufferExportFile);
		if(outputTextureName[strlen(outputTextureName)-4] == '.')
			outputTextureName[strlen(outputTextureName)-4] = 0;
		strcat(outputTextureName, "_e.dds");

		//ilSaveImage(ILstring(outputTextureName));
		D3DXSaveTextureToFile(outputTextureName,D3DXIFF_DDS, pTexture, NULL);
		pTexture->Release();
	}
	delete pExpBuffer;
#endif
	delete pColBuffer;
#if __WIN32PC
	if(!m_SoftwareMode)
	{
		pRTSurface->Release();
	}
#endif
}

#if __WIN32PC
void HDRTextureTool::ComputeColorExponent(int id)
{
	// if we are networked, then lets communicate the texture exposure to the client.
	if(m_DisplayHDR_DDS || ((!m_ComposedRT || !m_UseGlobalExponents) && !m_SoftwareMode)) return;
	if(m_Pipe && m_IsServer && m_ExposureTextureSet->GetNumTextures()) 
	{
		if(id < 0) id = 0;
		hdrExposureTexture& tex = m_ExposureTextureSet->GetTexture(id);
		hdrClientPipe::SetTextureExposure packet;
		packet.m_Exposure = tex.GetExposure();
		packet.m_TextureId = id;
		packet.m_UseGlobalExponents = m_UseGlobalExponents;
		packet.m_ColorExponent = m_ColorExponent;
		packet.m_ColorOffset = m_ColorOffset;
		packet.m_DisplayExposure = m_DisplayExposure;
		m_Pipe->Send(packet);
	}

	Vector3 maxColors(-1000000.0f, -1000000.0f, -1000000.0f);
	Vector3 minColors(1000000.0f, 1000000.0f, 1000000.0f);
	if(m_SoftwareMode)
	{
		hdrExposureTexture* tex[MAX_EXPOSURES];
		rage::grcTextureLock lock[MAX_EXPOSURES];
		float* colors[MAX_EXPOSURES];
		for(int i=0; i<m_ExposureTextureSet->GetNumTextures(); i++)
		{
			tex[i] = &m_ExposureTextureSet->GetTexture(i);
			tex[i]->GetTexture()->LockRect(0, 0, lock[i]);
			colors[i] = (float*)lock[i].Base;
		}
		int width = tex[0]->GetTexture()->GetWidth();
		int height = tex[0]->GetTexture()->GetHeight();
		for(int y=0; y < height; y++)
		{
			for(int x=0; x< width; x++)
			{
				Vector4 totalcolors(0.0f,0.0f,0.0f,0.0f);
				Vector4 exposedcol(0.0f, 0.0f, 0.0f, -1.0f);
				int numtex = m_ExposureTextureSet->GetNumTextures();
				for(int texidx=0; texidx < numtex; texidx++)
				{
					bool ignoreAlpha = !tex[texidx]->HasAlpha() || m_IgnoreAlpha;
					// Only use colors that don't have 0.0 or 1.0 in the color channels (invalid HDR values) and make sure that alpha is not 0
					if(colors[texidx][3] != 0.0f || ignoreAlpha)
					{
							if(
								m_IgnoreBadPixels || 
								(colors[texidx][0] != 0.0f && colors[texidx][0] != 1.0f))
							{
								float r = (colors[texidx][0]) * pow(2.0f, -tex[texidx]->GetExposure());
								Vector4 newcol(r,0.0f,0.0f, colors[texidx][3]);
								totalcolors.x += 1.0f;
								float newcolorweight = 1.0f/totalcolors.x;
								newcolorweight *= (ignoreAlpha || numtex == 1) ? 1.0f : newcol.w;
								float oldcolorweight = 1.0f-newcolorweight;
								exposedcol.x = (newcol.x * newcolorweight) + (exposedcol.x * oldcolorweight);
							}
							if(
								m_IgnoreBadPixels || 
								(colors[texidx][1] != 0.0f && colors[texidx][1] != 1.0f))
							{
								float g = (colors[texidx][1]) * pow(2.0f, -tex[texidx]->GetExposure());
								Vector4 newcol(0.0f,g,0.0f, colors[texidx][3]);
								totalcolors.y += 1.0f;
								float newcolorweight = 1.0f/totalcolors.y;
								newcolorweight *= (ignoreAlpha || numtex == 1) ? 1.0f : newcol.w;
								float oldcolorweight = 1.0f-newcolorweight;
								exposedcol.y = (newcol.y * newcolorweight) + (exposedcol.y * oldcolorweight);
							}
							if(
								m_IgnoreBadPixels || 
								(colors[texidx][2] != 0.0f && colors[texidx][2] != 1.0f))
							{
								float b = (colors[texidx][2]) * pow(2.0f, -tex[texidx]->GetExposure());
								Vector4 newcol(0.0f,0.0f,b, colors[texidx][3]);
								totalcolors.z += 1.0f;
								float newcolorweight = 1.0f/totalcolors.z;
								newcolorweight *= (ignoreAlpha || numtex == 1) ? 1.0f : newcol.w;
								float oldcolorweight = 1.0f-newcolorweight;
								exposedcol.z = (newcol.z * newcolorweight) + (exposedcol.z * oldcolorweight);
							}
							Vector4 newcol(0.0f,0.0f,0.0f, colors[texidx][3]);
							totalcolors.w += 1.0f;
							float newcolorweight = 1.0f/totalcolors.w;
							newcolorweight *= (ignoreAlpha || numtex == 1) ? 1.0f : newcol.w;
							float oldcolorweight = 1.0f-newcolorweight;
							exposedcol.w = (newcol.w * newcolorweight) + (exposedcol.w * oldcolorweight);
					}
					colors[texidx] += 4;
				}
				if(exposedcol.w > -1.0f)			
				{
					if(exposedcol.x > maxColors.x) maxColors.x = exposedcol.x;
					if(exposedcol.y > maxColors.y) maxColors.y = exposedcol.y;
					if(exposedcol.z > maxColors.z) maxColors.z = exposedcol.z;

					if(exposedcol.x < minColors.x) minColors.x = exposedcol.x;
					if(exposedcol.y < minColors.y) minColors.y = exposedcol.y;
					if(exposedcol.z < minColors.z) minColors.z = exposedcol.z;
				}
			}
		}
		for(int i=0; i<m_ExposureTextureSet->GetNumTextures(); i++)
		{
			tex[i] = &m_ExposureTextureSet->GetTexture(i);
			tex[i]->GetTexture()->UnlockRect(lock[i]);
		}
	}
	else
	{

		IDirect3DSurface9 *pRTSurface = NULL;

		D3DSURFACE_DESC desc;
		D3DLOCKED_RECT rect;

		m_ComposedRT->GetTexturePtr();
		static_cast<IDirect3DTexture9 *>(m_ComposedRT->GetTexturePtr())->GetSurfaceLevel(0, &pRTSurface);
		GRCDEVICE.GetCurrent()->GetRenderTargetData(pRTSurface, m_FloatTargetSurface);

		m_FloatTargetSurface->GetDesc(&desc);
		m_FloatTargetSurface->LockRect(&rect, NULL,0);

		{
			D3DXFLOAT16* colors = (D3DXFLOAT16*)rect.pBits;
			for(u32 i=0; i< desc.Width*desc.Height; i++)
			{
				Vector4 exposedcol;
				exposedcol.x = (*colors++);
				exposedcol.y = (*colors++);
				exposedcol.z = (*colors++);
				exposedcol.w = (*colors++);
				// If this color has 0 alpha, don't consider it.
				if(exposedcol.w || m_IgnoreAlpha)			
				{
					if(exposedcol.x > maxColors.x) maxColors.x = exposedcol.x;
					if(exposedcol.y > maxColors.y) maxColors.y = exposedcol.y;
					if(exposedcol.z > maxColors.z) maxColors.z = exposedcol.z;

					if(exposedcol.x < minColors.x) minColors.x = exposedcol.x;
					if(exposedcol.y < minColors.y) minColors.y = exposedcol.y;
					if(exposedcol.z < minColors.z) minColors.z = exposedcol.z;
				}
//#define PRINT_FLOAT_COLORS
#ifdef PRINT_FLOAT_COLORS
				Displayf("r: %f\t g: %f\t b: %f", fcolors.x, fcolors.y, fcolors.z);
#endif
			}
		}
		m_FloatTargetSurface->UnlockRect();
		pRTSurface->Release();
	}
	m_ColorExponent = maxColors - minColors;
	m_ColorOffset = minColors;
}
#else
void HDRTextureTool::ComputeColorExponent(int /*id*/)
{
}
#endif

void HDRTextureTool::ReadIniFile()
{
	fiStream* iniFile = ASSET.Open("ragehdrtexturetool","ini");
	if(iniFile)
	{
		fiTokenizer iniTokenizer("inifile", iniFile);
		iniTokenizer.MatchToken("SrcPath:");
		iniTokenizer.GetToken(m_BufferSrcPath, MAX_STR_LENGTH);

		iniTokenizer.MatchToken("DstPath:");
		iniTokenizer.GetToken(m_BufferDstPath, MAX_STR_LENGTH);
		iniFile->Close();
	}
	else
	{
		strcpy(m_BufferSrcPath, "c:\\");
		strcpy(m_BufferDstPath, "c:\\");
	}
}

void HDRTextureTool::WriteIniFile()
{
	fiStream* iniFile = ASSET.Create("ragehdrtexturetool","ini", false);
	if(iniFile)
	{
		fiTokenizer iniTokenizer("inifile", iniFile);
		iniTokenizer.PutStr("SrcPath: %s\r\n", m_BufferSrcPath);
		iniTokenizer.PutStr("DstPath: %s\r\n", m_BufferDstPath);
		iniFile->Close();
	}
	else
	{
		strcpy(m_BufferSrcPath, "c:\\");
		strcpy(m_BufferDstPath, "c:\\");
	}
}

grmSetup	Setup;

namespace rage {
	XPARAM(hidewindow);
}

// Init
// needs to be called before running the tool

void HDRTextureTool::Init(const char* path, bool noWindow)
{
	sm_CurrentInstance = this;
	if (noWindow)
	{
		PARAM_hidewindow.Set("");
	}
	
	// allocate and initialize shaders used by the tool
	m_MergeTexturesShader = new MergeTexturesShader;
	m_PickTexturesShader = new PickTexturesShader;
	m_MaxExponentShader = new MaxExponentShader;
	m_DefaultShader = new DefaultShader;

	// allocate texture sets
	m_ExposureTextureSet = new hdrTextureSet;
	m_ReferenceTextureSet = new hdrTextureSet;

	PARAM_listfile.Get(m_ListFilename);

#if	__WIN32PC
	_controlfp((u32)~0,_MCW_EM);
#endif
	ilInit();
	iluInit();
	ilEnable(IL_FILE_OVERWRITE);			// Set this to overwrite existing file

#if __XENON
	GRCDEVICE.SetSize(1280,720);
#else
	GRCDEVICE.SetSize(1024,1024);
#endif

	Setup.Init(path, "High Dynamic Range Tool");

	ReadIniFile();

	Setup.BeginGfx(true);
	Setup.CreateDefaultFactories();

	// initialize devil library
	ilResetMemory();
	ilResetRead();
	ilResetWrite();

	// Initialize shaders.
	m_MergeTexturesShader->Init();
	m_PickTexturesShader->Init();
	m_MaxExponentShader->Init();
	m_DefaultShader->Init();

	// Initialize the texture converter
	m_TextureConvert = new TextureConvert;
	m_TextureConvert->DisableTIFFWarnings();
	
#if USE_GRPOSTFX
	if(!grPostFX::GetInstance())
		grPostFX::Init();
#if __BANK
	GRPOSTFX->SetGammaEditable(true);
	GRPOSTFX->AddWidgets(BANKMGR.CreateBank("grpostfx",0,50));
#endif
	//Default to no render target debug display.
	GRPOSTFX->SetShowRenderTargetFlag(false);
	//Default to disabled post-processing
	GRPOSTFX->InitShaders("shaders/misc");

		// this is a nasty hack to adjust the render targets for window re-sizing
	int iViewportWidth = GRCDEVICE.GetWidth();
	int iViewportHeight = GRCDEVICE.GetHeight();

	GRPOSTFX->CreateRenderTargets(iViewportWidth, iViewportHeight);
	GRPOSTFX->SetChooseTechnique(pppNoPostProcessing);
	memcpy(&s_CurrentPPPPreset, &GRPOSTFX->GetPPPPresets(), sizeof(rage::grPostFX::grcPPPPreset) );
//	GRPOSTFX->SetChooseTechnique(pppToneMappingDepthOfField);

	GRPOSTFX->SetGamma(1.0f/2.2f);
#endif

	// Assign Shaders to different view classes.
	ComposeFloatTexture::SetShader(m_MergeTexturesShader);
	ComposedFloatToRGBETexture::SetShader(m_MergeTexturesShader);
	BlitRGBEToFloatTexture::SetShader(m_MergeTexturesShader);
	BlitFromComposedFloat::SetShader(m_MergeTexturesShader);
	DisplayTexture::SetShader(m_DefaultShader);

	m_DisplayHDR_DDS = false;
	m_Initialized = true;
	m_Pipe = NULL;
#if __BANK
	if (PARAM_client.Get())
	{
		Printf("Connecting to server...");
		m_IsServer=false;
		m_Pipe=new hdrClientPipe;
		while (m_Pipe->Open(bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_USER1))!=true);
		Displayf("connected.");
	}
#if __WIN32PC
	else if (PARAM_server.Get())
	{
		if(!PARAM_noclient.Get())
		{
			Displayf("Kicking off client on xbox...");
			char cmdLine[256];
			formatf(cmdLine, 256,"%s\\bin\\win32\\xbmkdir xE:\\rageHdrTextureTool", getenv("XEDK"));
			system(cmdLine);
			formatf(cmdLine, 256,"%s\\bin\\win32\\xbcp /F %s\\rfs.dat xE:\\rageHdrTextureTool",getenv("XEDK"), getenv("TEMP"));
			system(cmdLine);
			formatf(cmdLine, 256,"%s\\bin\\win32\\xbcp /F %s\\tools\\base\\xenon\\rageHdrTextureTool.xex xE:\\rageHdrTextureTool",getenv("XEDK"), getenv("RAGE_ROOT_FOLDER"));
			system(cmdLine);
			formatf(cmdLine, 256,"%s\\bin\\win32\\xbreboot xE:\\rageHdrTextureTool\\rageHdrTextureTool.xex -client -hdr",getenv("XEDK"));
			system(cmdLine);
		}		
		m_IsServer=true;
		Displayf("Waiting for client to connect...");
		m_Pipe=new hdrClientPipe;
		while (m_Pipe->Create(bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_USER1))!=true);
		GRCDEVICE.SetBlockOnLostFocus(false);
		Displayf("client connected.");
	}
	rage::g_WindowProc = TextureToolWndProc;     // points to window procedure 

#endif
	m_DisplayTexture.Reserve(MAX_CLIENT_TEXTURES);
#endif
}

void HDRTextureTool::Shutdown()
{
#if __WIN32PC
	rage::g_WindowProc = DefWindowProc;
#endif

	m_MergeTexturesShader->Shutdown();
	m_PickTexturesShader->Shutdown();
	m_MaxExponentShader->Shutdown();
	m_DefaultShader->Shutdown();

	delete m_MergeTexturesShader;
	delete m_PickTexturesShader;
	delete m_MaxExponentShader;
	delete m_DefaultShader;
	delete m_TextureConvert;

	ReleaseRenderTargets();
#if USE_GRPOSTFX
	// run post-processing pipeline
	GRPOSTFX->DeleteRenderTargets();
	GRPOSTFX->Terminate();
#endif //USE_GRPOSTFX

	ReleaseAllTextures();

#if __BANK
	if(m_BkExposure)
	{
		BANKMGR.DestroyBank(*m_BkExposure);
		m_BkExposure=NULL;
	}
#endif

	delete m_ExposureTextureSet;
	delete m_ReferenceTextureSet;
	m_DisplayTexture.Reset();
#if __WIN32PC
	if(m_IsServer)
	{
		char cmdLine[256];
		formatf(cmdLine, 256,"%s\\bin\\win32\\xbreboot",getenv("XEDK"));
		system(cmdLine);
	}
#endif
	Setup.DestroyFactories();
	Setup.EndGfx();
	Setup.Shutdown();
	m_Initialized = false;
}


void HDRTextureTool::DrawMultipleExposureTexture()
{
	if(m_PickTexturesRT && m_ExposureTextureSet->GetNumTextures())
	{
		for(int i=0; i<m_ExposureTextureSet->GetNumTextures(); i++)
		{
			hdrExposureTexture& tex = m_ExposureTextureSet->GetTexture(i);
			m_PickTexturesShader->SetInputTexture(tex.GetTexture(), i);
			m_MergeTexturesShader->SetInputTexture(tex.GetTexture(), i);
		}

		m_PickTexturesShader->SetNumTextures(m_ExposureTextureSet->GetNumTextures());
		m_MergeTexturesShader->SetNumExposures(m_ExposureTextureSet->GetNumTextures());
		for(int i=0; i<m_ExposureTextureSet->GetNumTextures(); i++)
		{
			m_MergeTexturesShader->SetExposure(i, m_ExposureTextureSet->GetTexture(i).GetExposure());
		}
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_PickTexturesRT, NULL);
		GRCDEVICE.Clear(true,Color32(0.0f,0.0f,1.0f,1.0f), false,1.f,false);
		m_PickTexturesShader->SetDrawBadPixels(m_DisplayBadPixels);
		m_PickTexturesShader->Blit(0, 0, m_PickTexturesRT->GetWidth(),	m_PickTexturesRT->GetHeight(),
			0.1f, 0.0f,	0.0f, 1.0f, 1.0f, white, "draw");
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);

		
		m_MergeTexturesShader->SetColorExponent(m_ColorExponent);
		m_MergeTexturesShader->SetColorOffset(m_ColorOffset);
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_MaxExponentRT, NULL);
		GRCDEVICE.Clear(true,Color32(0.0f,0.0f,0.0f,0.0f), false,1.f,false);
		// Draw image in RGBE mode
		m_ComposeFloatTexture.Draw();
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);
		
		// paint high precision float render target, composed of all source textures.

		grcTextureFactory::GetInstance().LockRenderTarget(0, m_ComposedRT, NULL);
		GRCDEVICE.Clear(true,Color32(0.0f,0.0f,0.0f,0.0f), false,1.f,false);
		grcState::SetBlendSet(grcbsOverwrite);
		grcState::SetColorWrite(grccwRGBA);

		m_ComposeFloatTexture.Draw();

		grcTextureFactory::GetInstance().UnlockRenderTarget(0);


		// paint 8 bit render target that gets saved out from previous render target. Apply exponent and offset to it.

		grcTextureFactory::GetInstance().LockRenderTarget(0, m_SaveRT, NULL);
		GRCDEVICE.Clear(true,Color32(0.0f,0.0f,0.0f,0.0f), false,1.f,false);
		grcState::SetBlendSet(grcbsOverwrite);
		grcState::SetColorWrite(grccwRGBA);

		if(m_DisplayHDR_DDS)
		{
			m_BlitFromComposedFloat.Draw();
		}
		else
		{
			m_ComposedFloatToRGBETexture.SetUseGlobalExponents(m_UseGlobalExponents);
			m_ComposedFloatToRGBETexture.Draw();
		}

		grcTextureFactory::GetInstance().UnlockRenderTarget(0);

		m_MergeTexturesShader->SetDisplayExposure(m_DisplayExposure);
		// Display RGBE texture converted back to a normal texture
		m_BlitRGBEToFloatTextureEndResult.SetUseGlobalExponents(m_UseGlobalExponents);
		m_BlitRGBEToFloatTextureEndResult.Draw();
	}
}
void HDRTextureTool::DrawClientView()
{
	if(m_Pause) return;
#if USE_GRPOSTFX
		// Bind render target for PC
		GRPOSTFX->BindPCRenderTarget(true);
#endif //USE_GRPOSTFX

		m_MergeTexturesShader->SetIgnoreAlpha(m_IgnoreAlpha);
		
		float LinearGrey = powf((0.5f + 0.055f) / 1.055f, 1.0f/GRPOSTFX->GetGamma());
		GRCDEVICE.Clear(true,Color32(LinearGrey,LinearGrey,LinearGrey,0.0f), false,1.f,false);
		
		grcState::SetBlendSet(grcbsNormal);

		for(int j=0; j<m_ReferenceTextureSet->GetNumTextures(); j++)
		{
			hdrExposureTexture* reftex = NULL;
			int i;
			if(m_ExposureTextureSet->GetNumTextures() && m_ExposureTextureSet->GetTexture(0).GetDrawOrder() == j)
			{
				DrawMultipleExposureTexture();
			}
			for(i=0; i<m_ReferenceTextureSet->GetNumTextures(); i++)
			{
				reftex = &m_ReferenceTextureSet->GetTexture(i);
				if(reftex->GetDrawOrder() == j) break;
			}
			// check to see if the texture we need to draw is part of the reference textures
			Assert(i < m_ReferenceTextureSet->GetNumTextures());
			m_DefaultShader->SetInputTexture(reftex->GetTexture());	
			Vector3 exp(reftex->GetTexture()->GetColorExponent());
			Vector3 ofs(reftex->GetTexture()->GetColorOffset());
			float exposure = pow(2.0f,reftex->GetExposure());
			exp *= exposure;
			ofs *= exposure;
			m_DefaultShader->SetTextureExp(exp);
			m_DefaultShader->SetTextureOfs(ofs);
			m_DisplayTexture[i].Draw();
		}
		// Draw the multiple exposure HDR texture if the draworder is last, or if there are no reference textures.
		if(m_ExposureTextureSet->GetNumTextures() && (m_ExposureTextureSet->GetTexture(0).GetDrawOrder() >= m_ReferenceTextureSet->GetNumTextures() || m_ReferenceTextureSet->GetNumTextures() == 0))
		{
			DrawMultipleExposureTexture();
		}
//	GRCDEVICE.Clear(true,Color32(0.5f,0.5f,0.5f,0.5f), false,1.f,false);
}

void HDRTextureTool::DrawMainView()
{
	float ExposureStep = 1.0f;
	float NumColumns = 0.0f;
	float deltaSX = 0.0f;
	int texcount = 0;
	float TextHeight = 30.0f;
	grcViewport::SetCurrent(&m_Viewport);
	if(m_PickTexturesRT && m_ExposureTextureSet->GetNumTextures())
	{
		hdrExposureTexture& tex = m_ExposureTextureSet->GetTexture(0);
		for(int i=0; i<m_ExposureTextureSet->GetNumTextures(); i++)
		{
			hdrExposureTexture& tex = m_ExposureTextureSet->GetTexture(i);
			m_PickTexturesShader->SetInputTexture(tex.GetTexture(), i);
			m_MergeTexturesShader->SetInputTexture(tex.GetTexture(), i);
		}

		m_PickTexturesShader->SetNumTextures(m_ExposureTextureSet->GetNumTextures());
		m_MergeTexturesShader->SetNumExposures(m_ExposureTextureSet->GetNumTextures());
		for(int i=0; i<m_ExposureTextureSet->GetNumTextures(); i++)
		{
			m_MergeTexturesShader->SetExposure(i, m_ExposureTextureSet->GetTexture(i).GetExposure());
		}
#if !__DISPLAY_PICK_TEX
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_PickTexturesRT, NULL);
#endif
		GRCDEVICE.Clear(true,Color32(0.0f,0.0f,1.0f,1.0f), false,1.f,false);
		m_PickTexturesShader->SetDrawBadPixels(m_DisplayBadPixels);
		m_PickTexturesShader->Blit(0, 0, m_PickTexturesRT->GetWidth(),	m_PickTexturesRT->GetHeight(),
			0.1f, 0.0f,	0.0f, 1.0f, 1.0f, white, "draw");
#if !__DISPLAY_PICK_TEX
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);

		
		if(m_NumExposures < m_NumRows) m_NumRows = m_NumExposures;
		ExposureStep = 1.0f;
		NumColumns = (m_NumExposures/(m_NumRows*ExposureStep));
		deltaSX = (m_NumExposures == 1.0f) ? tex.GetTexture()->GetWidth() / NumColumns : GRCDEVICE.GetWidth() / NumColumns;

		m_MergeTexturesShader->SetColorExponent(m_ColorExponent);
		m_MergeTexturesShader->SetColorOffset(m_ColorOffset);
		grcTextureFactory::GetInstance().LockRenderTarget(0, m_MaxExponentRT, NULL);
		GRCDEVICE.Clear(true,Color32(0.0f,0.0f,0.0f,0.0f), false,1.f,false);
		// Draw image in RGBE mode
		m_ComposeFloatTexture.Draw();
		grcTextureFactory::GetInstance().UnlockRenderTarget(0);
		
		// paint high precision float render target, composed of all source textures.

		grcTextureFactory::GetInstance().LockRenderTarget(0, m_ComposedRT, NULL);
		GRCDEVICE.Clear(true,Color32(0.0f,0.0f,0.0f,0.0f), false,1.f,false);
		grcState::SetBlendSet(grcbsOverwrite);
		grcState::SetColorWrite(grccwRGBA);

		m_ComposeFloatTexture.Draw();

		grcTextureFactory::GetInstance().UnlockRenderTarget(0);


		// paint 8 bit render target that gets saved out from previous render target. Apply exponent and offset to it.

		grcTextureFactory::GetInstance().LockRenderTarget(0, m_SaveRT, NULL);
		GRCDEVICE.Clear(true,Color32(0.0f,0.0f,0.0f,0.0f), false,1.f,false);
		grcState::SetBlendSet(grcbsOverwrite);
		grcState::SetColorWrite(grccwRGBA);

		if(m_DisplayHDR_DDS)
		{
			m_BlitFromComposedFloat.Draw();
		}
		else
		{
			m_ComposedFloatToRGBETexture.SetUseGlobalExponents(m_UseGlobalExponents);
			m_ComposedFloatToRGBETexture.Draw();
		}

		grcTextureFactory::GetInstance().UnlockRenderTarget(0);


#if USE_GRPOSTFX
		// Bind render target for PC
		if(!m_Pipe || !m_IsServer)
		{
			GRPOSTFX->BindPCRenderTarget(true);
		}
#endif //USE_GRPOSTFX


		GRCDEVICE.Clear(true,Color32(0.5f,0.5f,0.5f,0.5f), false,1.f,false);

		m_MergeTexturesShader->SetDisplayExposure(m_DisplayExposure);
		m_MergeTexturesShader->SetIgnoreAlpha(m_IgnoreAlpha);

		m_BlitRGBEToFloatTextureEndResult.SetScreenPos(tex.GetTexture()->GetWidth(),(int)(deltaSX*(m_NumExposures/(int)NumColumns) + (m_NumExposures/(int)NumColumns+1) * TextHeight));

		// Display RGBE texture
		m_ComposedFloatToRGBETextureOnScreen.Draw();
		m_ComposedFloatToRGBETextureOnScreen.SetUseGlobalExponents(m_UseGlobalExponents);

		m_MergeTexturesShader->SetDisplayExposure(m_DisplayExposure);
		// Display RGBE texture converted back to a normal texture
		m_BlitRGBEToFloatTextureEndResult.SetUseGlobalExponents(m_UseGlobalExponents);
		m_BlitRGBEToFloatTextureEndResult.Draw();

		// Draw visual textures to screen
		m_BlitRGBEToFloatTextureMultiExposure.SetUseGlobalExponents(m_UseGlobalExponents);
		for(float expval=0.0f; expval<m_NumExposures; expval+=ExposureStep)
		{

			m_ComposedFloatToRGBETextureOnScreen.SetScreenPos(0,(int)(deltaSX*(m_NumExposures/(int)NumColumns) + (m_NumExposures/(int)NumColumns+1) * TextHeight));
			m_MergeTexturesShader->SetDisplayExposure(m_DisplayExposure + expval - (m_NumExposures * 0.5f - 0.5f));
			m_BlitRGBEToFloatTextureMultiExposure.SetScreenPos((int)(deltaSX*(texcount%(int)NumColumns)), (int)(deltaSX*(texcount/(int)NumColumns) + (texcount/(int)NumColumns+1) * TextHeight));
			m_BlitRGBEToFloatTextureMultiExposure.SetDimensions((int)(deltaSX) ,(int)(deltaSX));
			m_BlitRGBEToFloatTextureMultiExposure.Draw();
			texcount++;
		}
	}
}

void HDRTextureTool::DrawMainViewText()
{
	float ExposureStep = 1.0f;
	float NumColumns = 0.0f;
	float deltaSX = 0.0f;
	int texcount = 0;
	float TextHeight = 30.0f;
	if(m_PickTexturesRT)
	{
		hdrExposureTexture& tex = m_ExposureTextureSet->GetTexture(0);
		if(m_NumExposures < m_NumRows) m_NumRows = m_NumExposures;
		ExposureStep = 1.0f;
		NumColumns = (m_NumExposures/(m_NumRows*ExposureStep));
		deltaSX = (m_NumExposures == 1.0f) ? tex.GetTexture()->GetWidth() / NumColumns : GRCDEVICE.GetWidth() / NumColumns;

		char buffer[64];
		formatf(buffer,sizeof(buffer),"Final Texture Exp: %6.1f",m_DisplayExposure);
		grcFont::GetCurrent().DrawScaled(-1.0f+(float)1*tex.GetTexture()->GetWidth(),
			-1.0f+(float)(deltaSX*(m_NumExposures/(int)NumColumns) + (m_NumExposures/(int)NumColumns+0.5f) * (TextHeight)),0.0f,
			Color32(0,0,0),1.0f,1.1f,buffer);
		grcFont::GetCurrent().DrawScaled((float)1*tex.GetTexture()->GetWidth(),
			(float)(deltaSX*(m_NumExposures/(int)NumColumns) + (m_NumExposures/(int)NumColumns+0.5f) * TextHeight),0.0f,
			Color32(255,255,255),1.0f,1.0f,buffer);
		grcFont::GetCurrent().DrawScaled(-1.0f+(float)0*tex.GetTexture()->GetWidth(),
			-1.0f+(float)(deltaSX*(m_NumExposures/(int)NumColumns) + (m_NumExposures/(int)NumColumns+0.5f) * (TextHeight)),0.0f,
			Color32(0,0,0),1.0f,1.1f,"RGBE Texture");
		grcFont::GetCurrent().DrawScaled((float)0*tex.GetTexture()->GetWidth(),(float)(deltaSX*(m_NumExposures/(int)NumColumns) + (m_NumExposures/(int)NumColumns+0.5f) * TextHeight),0.0f,
			Color32(255,255,255),1.0f,1.0f,"RGBE Texture");
		texcount = 0;
		for(float expval=0.0f; expval<m_NumExposures; expval+=ExposureStep)
		{


			char buffer[64];
			formatf(buffer,sizeof(buffer),"Exp: %6.1f",m_DisplayExposure + expval - (m_NumExposures * 0.5f - 0.5f));
			grcFont::GetCurrent().DrawScaled(-1.0f+(deltaSX*(texcount%(int)NumColumns)),
				-1.0f+(deltaSX*(texcount/(int)NumColumns) + (texcount/(int)NumColumns+0.5f) * TextHeight),0.0f,
				Color32(0,0,0),1.0f,1.1f,buffer);
			grcFont::GetCurrent().DrawScaled((deltaSX*(texcount%(int)NumColumns)),(deltaSX*(texcount/(int)NumColumns) + (texcount/(int)NumColumns+0.5f) * TextHeight),0.0f,
				Color32(255,255,255),1.0f,1.0f,buffer);
			texcount++;
		}
	}
}

void HDRTextureTool::ProcessClientPipe()
{
	int numBytesRead = 0;
	if(m_Pipe && !m_IsServer)
	{
		hdrClientPipe::Command command;
		numBytesRead = m_Pipe->Receive(command, false);
		if(numBytesRead)
		{
			switch(command)
			{
			case hdrClientPipe::PAUSE_CLIENT:
				{
					hdrClientPipe::PauseClient *packet = (hdrClientPipe::PauseClient *) m_Pipe->GetReceivedData();
					m_Pause = packet->m_Pause;
					m_Pipe->SendAck();
					break;
				}
			case hdrClientPipe::ADD_REFERENCE_TEXTURE:
				{
					hdrClientPipe::AddReferenceTexture *packet = (hdrClientPipe::AddReferenceTexture *) m_Pipe->GetReceivedData();
					Displayf("Loading texture %s", (const char*)&packet->m_Filename);
					AddReferenceTexture((const char*)&packet->m_Filename, 0.0f, packet->m_DrawOrder);									// We are the client so lets load the texture requested by the server
					DisplayTexture& displayTex = m_DisplayTexture.Append();				// Add another view to the standard display texture shader
					int i = m_ReferenceTextureSet->GetNumTextures()-1;
					displayTex.SetDimensions(m_ReferenceTextureSet->GetTexture(i).GetTexture()->GetWidth(),
											 m_ReferenceTextureSet->GetTexture(i).GetTexture()->GetHeight());		
					displayTex.SetScreenPos(0,0);
					m_Pipe->SendAck();
					Displayf("Adding texture %d", i);
					break;
				}
			case hdrClientPipe::ADD_EXPOSURE_TEXTURE:
				{
					hdrClientPipe::AddExposureTexture *packet = (hdrClientPipe::AddExposureTexture *) m_Pipe->GetReceivedData();
					Displayf("Loading texture %s", (const char*)&packet->m_Filename);
					AddExposureTexture((const char*)&packet->m_Filename, packet->m_Exposure, packet->m_DrawOrder);									// We are the client so lets load the texture requested by the server
					DisplayTexture& displayTex = m_DisplayTexture.Append();				// Add another view to the standard display texture shader
					int i = m_ExposureTextureSet->GetNumTextures()-1;
					displayTex.SetDimensions(m_ExposureTextureSet->GetTexture(i).GetTexture()->GetWidth(),
											 m_ExposureTextureSet->GetTexture(i).GetTexture()->GetHeight());		
					displayTex.SetScreenPos(0,0);
					m_Pipe->SendAck();
					Displayf("Adding texture %d", i);
					break;
				}
			case hdrClientPipe::DELETE_CLIENT_TEXTURE:
				{
					hdrClientPipe::DeleteClientTexture *packet = (hdrClientPipe::DeleteClientTexture *) m_Pipe->GetReceivedData();
					if(packet->m_TextureId < 0)
					{
						DeleteAllTextures();
					}
					else
					{
						if(m_ReferenceTextureSet->GetNumTextures())
						{
							m_ReferenceTextureSet->RemoveTexture(packet->m_TextureId);
							m_DisplayTexture.Delete(packet->m_TextureId);
						}
					}
					m_Pipe->SendAck();
					Displayf("Deleting texture %d", packet->m_TextureId);
					break;
				}
			case hdrClientPipe::BRING_CLIENTTEX_TO_FOREGROUND:
				{
					hdrClientPipe::BringToForeground* packet = (hdrClientPipe::BringToForeground*) m_Pipe->GetReceivedData();
					// if texture ID is less than 0, we need to bring the multiple exposure texture to the foreground
					if(packet->m_TextureId < 0)
					{
						if(m_ExposureTextureSet->GetNumTextures())
						{
							m_ExposureTextureSet->GetTexture(0).SetDrawOrder(m_ReferenceTextureSet->GetNumTextures());
						}
					}
					else
					// otherwise the user clicked on a reference texture to be brought to the foreground
					{
						m_ReferenceTextureSet->BringToForeground(packet->m_TextureId);
						if(m_ExposureTextureSet->GetNumTextures())
						{
							int draworder = m_ExposureTextureSet->GetTexture(0).GetDrawOrder();
							if(draworder > 0) m_ExposureTextureSet->GetTexture(0).SetDrawOrder(draworder-1);
						}
					}
					break;
				}
			case hdrClientPipe::MOVE_CLIENT_TEXTURE:
				{
					hdrClientPipe::MoveClientTexture *packet = (hdrClientPipe::MoveClientTexture *) m_Pipe->GetReceivedData();
					if(packet->m_TextureId < 0)
					{
						m_BlitRGBEToFloatTextureEndResult.SetScreenPos(
							m_BlitRGBEToFloatTextureEndResult.GetScreenX()+packet->m_X,
							m_BlitRGBEToFloatTextureEndResult.GetScreenY()+packet->m_Y);
					}
					else
					{
						m_DisplayTexture[packet->m_TextureId].SetScreenPos(
							m_DisplayTexture[packet->m_TextureId].GetScreenX()+packet->m_X,
							m_DisplayTexture[packet->m_TextureId].GetScreenY()+packet->m_Y);
					}
					break;
				}
			case hdrClientPipe::SET_CLIENT_TEXTURE_DATA:
				{
					hdrClientPipe::SetClientTextureData *packet = (hdrClientPipe::SetClientTextureData*) m_Pipe->GetReceivedData();
					if(packet->m_TextureId < 0)
					{
						m_BlitRGBEToFloatTextureEndResult.SetScreenPos(packet->m_X,packet->m_Y);
						m_BlitRGBEToFloatTextureEndResult.SetDimensions(packet->m_Width,packet->m_Height);
					}
					else
					{
						m_DisplayTexture[packet->m_TextureId].SetScreenPos(packet->m_X,packet->m_Y);
						m_DisplayTexture[packet->m_TextureId].SetDimensions(packet->m_Width,packet->m_Height);
					}
					m_Pipe->SendAck();
					break;
				}
			case hdrClientPipe::GET_CLIENT_TEXTURE_DATA:
				{										
					hdrClientPipe::GetClientTextureData *packet = (hdrClientPipe::GetClientTextureData *) m_Pipe->GetReceivedData();
					hdrClientPipe::GetClientTextureData sendpacket;
					if(packet->m_TextureId < 0)
					{
						sendpacket.m_X = m_BlitRGBEToFloatTextureEndResult.GetScreenX();
						sendpacket.m_Y = m_BlitRGBEToFloatTextureEndResult.GetScreenY();
						sendpacket.m_Width = m_BlitRGBEToFloatTextureEndResult.GetWidth();
						sendpacket.m_Height = m_BlitRGBEToFloatTextureEndResult.GetHeight();
					}
					else
					{
						sendpacket.m_X = m_DisplayTexture[packet->m_TextureId].GetScreenX();
						sendpacket.m_Y = m_DisplayTexture[packet->m_TextureId].GetScreenY();
						sendpacket.m_Width = m_DisplayTexture[packet->m_TextureId].GetWidth();
						sendpacket.m_Height = m_DisplayTexture[packet->m_TextureId].GetHeight();
					}
					sendpacket.m_TextureId = packet->m_TextureId;
					m_Pipe->Send(sendpacket);
					break;
				}
			case hdrClientPipe::LIGHT_CLIENT_TEXTURE:
				{
					hdrClientPipe::LightClientTexture* packet = (hdrClientPipe::LightClientTexture *) m_Pipe->GetReceivedData();
					m_ReferenceTextureSet->GetTexture(packet->m_TextureId).GetExposure() = packet->m_Exposure;
					break;
				}
			case hdrClientPipe::SET_TEXTURE_EXPOSURE:
				{
					hdrClientPipe::SetTextureExposure* packet = (hdrClientPipe::SetTextureExposure*) m_Pipe->GetReceivedData();
					m_ExposureTextureSet->GetTexture(packet->m_TextureId).GetExposure() = packet->m_Exposure;
					m_UseGlobalExponents = packet->m_UseGlobalExponents;
					m_ColorExponent.x = packet->m_ColorExponent.x;
					m_ColorExponent.y = packet->m_ColorExponent.y;
					m_ColorExponent.z = packet->m_ColorExponent.z;
					m_ColorOffset.x = packet->m_ColorOffset.x;
					m_ColorOffset.y = packet->m_ColorOffset.y;
					m_ColorOffset.z = packet->m_ColorOffset.z;
					m_DisplayExposure = packet->m_DisplayExposure;
					m_MergeTexturesShader->SetColorExponent(m_ColorExponent);
					m_MergeTexturesShader->SetColorOffset(m_ColorOffset);
					m_MergeTexturesShader->SetDisplayExposure(m_DisplayExposure);
					break;
				}
			case hdrClientPipe::SET_IGNORE_ALPHA:
				{
					hdrClientPipe::SetIgnoreAlpha* packet = (hdrClientPipe::SetIgnoreAlpha*) m_Pipe->GetReceivedData();
					m_IgnoreAlpha = packet->m_IgnoreAlpha;
					break;
				}
			case hdrClientPipe::SET_DISPLAY_EXPOSURE:
				{
					hdrClientPipe::SetDisplayExposure* packet = (hdrClientPipe::SetDisplayExposure*) m_Pipe->GetReceivedData();
					m_DisplayExposure = packet->m_DisplayExposure;
					break;
				}
			case hdrClientPipe::SCALE_CLIENT_TEXTURE:
				{
					hdrClientPipe::ScaleClientTexture* packet = (hdrClientPipe::ScaleClientTexture *) m_Pipe->GetReceivedData();
					if(packet->m_TextureId < 0)
					{
						m_BlitRGBEToFloatTextureEndResult.SetDimensions(
							m_BlitRGBEToFloatTextureEndResult.GetWidth()+packet->m_Width,
							m_BlitRGBEToFloatTextureEndResult.GetHeight()+packet->m_Height);
					}
					else
					{
						m_DisplayTexture[packet->m_TextureId].SetDimensions(
							m_DisplayTexture[packet->m_TextureId].GetWidth()+packet->m_Width,
							m_DisplayTexture[packet->m_TextureId].GetHeight()+packet->m_Height);
					}
					break;
				}
			case hdrClientPipe::SET_CLIENT_PPP:
				{
					hdrClientPipe::SetClientPPP* packet = (hdrClientPipe::SetClientPPP*) m_Pipe->GetReceivedData();
					GRPOSTFX->SetPPPPresets(packet->m_PPPPreset);
					break;
				}
			default:
				break;
			}
		}
	}
}

void HDRTextureTool::DeleteAllTextures()
{
	ReleaseAllTextures();
	ReleaseRenderTargets();
}


// Composes HDR texture from exposure textures on the CPU.
void HDRTextureTool::ComposeExposureTextures()
{
#if __WIN32PC
	D3DLOCKED_RECT rect;
	IDirect3DSurface9 *pRTSurface = NULL;
	static_cast<IDirect3DTexture9 *>(m_SaveRT->GetTexturePtr())->GetSurfaceLevel(0, &pRTSurface);
	GRCDEVICE.GetCurrent()->GetRenderTargetData(pRTSurface, m_SaveTargetSurface);
	m_SaveTargetSurface->LockRect(&rect, NULL,0);

	hdrExposureTexture* tex[MAX_EXPOSURES];
	rage::grcTextureLock lock[MAX_EXPOSURES];
	float* colors[MAX_EXPOSURES];
	for(int i=0; i<m_ExposureTextureSet->GetNumTextures(); i++)
	{
		tex[i] = &m_ExposureTextureSet->GetTexture(i);
		tex[i]->GetTexture()->LockRect(0, 0, lock[i]);
		colors[i] = (float*)lock[i].Base;
	}
	int width = tex[0]->GetTexture()->GetWidth();
	int height = tex[0]->GetTexture()->GetHeight();
	u8* pColData = (u8*)rect.pBits;
	Vector3 maxColors(-1000000.0f, -1000000.0f, -1000000.0f);
	Vector3 minColors(1000000.0f, 1000000.0f, 1000000.0f);
	Vector3 invColorExponent(255.0f/m_ColorExponent.x,255.0f/m_ColorExponent.y,255.0f/m_ColorExponent.z);
	
	for(int y=0; y < height; y++)
	{
		for(int x=0; x< width; x++)
		{
			Vector4 totalcolors(0.0f,0.0f,0.0f,0.0f);
			Vector4 exposedcol(0.0f, 0.0f, 0.0f, -1.0f);
			int numtex = m_ExposureTextureSet->GetNumTextures();
			for(int texidx=0; texidx < numtex; texidx++)
			{
				bool ignoreAlpha = !tex[texidx]->HasAlpha() || m_IgnoreAlpha;
				// Only use colors that don't have 0.0 or 1.0 in the color channels (invalid HDR values) and make sure that alpha is not 0
				if(colors[texidx][3] != 0.0f || ignoreAlpha)
				{
						if(
							m_IgnoreBadPixels || 
							(colors[texidx][0] != 0.0f && colors[texidx][0] != 1.0f))
						{
							float r = (colors[texidx][0]) * pow(2.0f, -tex[texidx]->GetExposure());
							Vector4 newcol(r,0.0f,0.0f, colors[texidx][3]);
							totalcolors.x += 1.0f;
							float newcolorweight = 1.0f/totalcolors.x;
							newcolorweight *= (ignoreAlpha || numtex == 1) ? 1.0f : newcol.w;
							float oldcolorweight = 1.0f-newcolorweight;
							exposedcol.x = (newcol.x * newcolorweight) + (exposedcol.x * oldcolorweight);
						}
						if(
							m_IgnoreBadPixels || 
							(colors[texidx][1] != 0.0f && colors[texidx][1] != 1.0f))
						{
							float g = (colors[texidx][1]) * pow(2.0f, -tex[texidx]->GetExposure());
							Vector4 newcol(0.0f,g,0.0f, colors[texidx][3]);
							totalcolors.y += 1.0f;
							float newcolorweight = 1.0f/totalcolors.y;
							newcolorweight *= (ignoreAlpha || numtex == 1) ? 1.0f : newcol.w;
							float oldcolorweight = 1.0f-newcolorweight;
							exposedcol.y = (newcol.y * newcolorweight) + (exposedcol.y * oldcolorweight);
						}
						if(
							m_IgnoreBadPixels || 
							(colors[texidx][2] != 0.0f && colors[texidx][2] != 1.0f))
						{
							float b = (colors[texidx][2]) * pow(2.0f, -tex[texidx]->GetExposure());
							Vector4 newcol(0.0f,0.0f,b, colors[texidx][3]);
							totalcolors.z += 1.0f;
							float newcolorweight = 1.0f/totalcolors.z;
							newcolorweight *= (ignoreAlpha || numtex == 1) ? 1.0f : newcol.w;
							float oldcolorweight = 1.0f-newcolorweight;
							exposedcol.z = (newcol.z * newcolorweight) + (exposedcol.z * oldcolorweight);
						}
						Vector4 newcol(0.0f,0.0f,0.0f, colors[texidx][3]);
						totalcolors.w += 1.0f;
						float newcolorweight = 1.0f/totalcolors.w;
						newcolorweight *= (ignoreAlpha || numtex == 1) ? 1.0f : newcol.w;
						float oldcolorweight = 1.0f-newcolorweight;
						exposedcol.w = (newcol.w * newcolorweight) + (exposedcol.w * oldcolorweight);
				}
#define FLOATTOU8EXPOFS(X, EXP, OFS) ( (u8)( ( X - OFS ) * EXP + 0.5f)  )
				colors[texidx] += 4;
			}
			pColData[0] = FLOATTOU8EXPOFS(exposedcol.x, invColorExponent.x, m_ColorOffset.x);
			pColData[1] = FLOATTOU8EXPOFS(exposedcol.y, invColorExponent.y, m_ColorOffset.y);
			pColData[2] = FLOATTOU8EXPOFS(exposedcol.z, invColorExponent.z, m_ColorOffset.z);
			pColData[3] = FLOATTOU8EXPOFS(exposedcol.w, 255.0f, 0.0f); // Should not use this since it is not calculated for alpha
			pColData+=4;
		}
	}
	for(int i=0; i<m_ExposureTextureSet->GetNumTextures(); i++)
	{
		tex[i] = &m_ExposureTextureSet->GetTexture(i);
		tex[i]->GetTexture()->UnlockRect(lock[i]);
	}
	m_SaveTargetSurface->UnlockRect();
	pRTSurface->Release();
#endif
}

void HDRTextureTool::SetClientIgnoreAlpha()
{
	if(m_Pipe && m_IsServer) 
	{
		hdrClientPipe::SetIgnoreAlpha packet;
		packet.m_IgnoreAlpha = m_IgnoreAlpha;
		m_Pipe->Send(packet);
	}
}

void HDRTextureTool::SetClientDisplayExposure()
{
	if(m_Pipe && m_IsServer) 
	{
		hdrClientPipe::SetDisplayExposure packet;
		packet.m_DisplayExposure = m_DisplayExposure;
		m_Pipe->Send(packet);
	}
}

int	HDRTextureTool::Run(RunParams* params)
{
	// unless we are passed in specific export parameters, we need to get our export filename from a dialog
	m_ExportFileDialog = (params == 0);
	m_Params = params;

	// if we are passed in specific export parameters, we need to make sure the dimensions are power of 2 or we are screwed
	bool ranOnce = true;
	if(params)
	{
		bool powerOf2 = ((params->outWidth & (params->outWidth-1)) == 0) && ((params->outHeight & (params->outHeight-1)) == 0);
		if(!powerOf2) return -1;
		// if we run in export mode, then don't use the pixelshader to compute the DDS.
		m_SoftwareMode = params->doExport;
		m_IgnoreAlpha = params->ignoreAlpha;
		m_IgnoreBadPixels = true;
		if(params->inputFiles.GetCount() > 1) {
			m_IgnoreBadPixels = false;
			m_IgnoreAlpha = false;
		}
	}
#if __WIN32PC
	GRCDEVICE.SetBlockOnLostFocus(false);
#endif
	if(params && params->doExport)
	{
		if( !params->inputFiles.GetCount()) 
		{
			Warningf("Exporting HDR texture failed because no input file was specified!!!");
			return -1; 
		}
	}
	m_DoneExporting = false;
	m_UseGlobalExponents = true;
	m_DisplayBadPixels = false;
#if	__BANK
	if(m_ListFilename)
	{
		m_ListFile = ASSET.Open(m_ListFilename,"");
	}

	if(m_ListFile)
	{
		m_ListTokenizer = new fiTokenizer("listfile", m_ListFile);
		m_NumFilesToProcess = m_ListTokenizer->MatchInt("NumFiles");
	}
	else if (!params || !params->doExport)
	{
		Setup.AddWidgets(BANKMGR.CreateBank("grcore",0,50));



		m_BkExposure = &BANKMGR.CreateBank("HDR Exposure Tool");
		m_BkExposure->AddTitle("File");
		m_BkExposure->AddButton("Load Workspace",datCallback(MFA(HDRTextureTool::LoadWorkspaceFileDialog),this));
		m_BkExposure->AddButton("Save Workspace",datCallback(MFA(HDRTextureTool::SaveWorkspace),this));
		m_BkExposure->AddButton("Add Exposure Texture",datCallback(MFA1(HDRTextureTool::LoadTexture),this, m_BufferAddFile));
#if __WIN32PC
		if(m_Pipe && m_IsServer)
		{
			m_BkExposure->AddButton("Add Reference Texture",datCallback(MFA(HDRTextureTool::LoadClientTextureMultiDialog),this));
		}
#endif
		m_BkExposure->AddButton("Export HDR Texture",datCallback(MFA(HDRTextureTool::ExportTexture),this));

		m_BkExposure->AddTitle("Display controls");
		m_BkExposure->AddSlider("Display Exposure", &m_DisplayExposure, -14.0f, 14.0f, 0.05f,datCallback(MFA(HDRTextureTool::SetClientDisplayExposure),this));
		m_BkExposure->AddSlider("Num Exposures", &m_NumExposures, 1, 14, 1);
		m_BkExposure->AddSlider("Num Rows", &m_NumRows, 1, 14, 1);
		m_BkExposure->AddToggle("Draw undefined pixels", &m_DisplayBadPixels);

		m_BkExposure->AddTitle("Other functions");
		m_BkExposure->AddButton("Delete all Textures",datCallback(MFA(HDRTextureTool::DeleteAllTextures),this));
		m_BkExposure->AddButton("Compute Color Exponent",datCallback(MFA1(HDRTextureTool::ComputeColorExponent), this,(int*) -1));
		m_BkExposure->AddToggle("Use global exponents", &m_UseGlobalExponents, datCallback(MFA1(HDRTextureTool::ComputeColorExponent), this, (int*) -1));
		m_BkExposure->AddToggle("Ignore source alpha", &m_IgnoreAlpha,datCallback(MFA(HDRTextureTool::SetClientIgnoreAlpha),this));

		m_BkExposure->AddTitle("Info only");
#if 1	// DEBUGGING ONLY
		m_BkExposure->AddSlider("Color Exponent", &m_ColorExponent, 0.0f, 127.0f, 0.0f);
		m_BkExposure->AddSlider("Color Offset", &m_ColorOffset, 0.0f, 127.0f, 0.0f);
#endif
	}
#endif

	m_ExposureTextureSet->Init(MAX_EXPOSURES);
	m_ReferenceTextureSet->Init(MAX_CLIENT_TEXTURES);

	if(params)
	{
		if(params->inputFiles.GetCount() == 1 && strstr(params->inputFiles[0], ".hdrproj"))
		{
			strcpy(m_BufferWorkspaceFile, params->inputFiles[0]);
			LoadWorkspace();		
		}
		else
		{
			for(int i=0; i<params->inputFiles.GetCount() ; i++)
			{
				strcpy(m_BufferAddFile, params->inputFiles[i]);
				if(0 > AddExposureTexture(params->inputFiles[i],params->exposures[i], -1, params->outWidth, params->outHeight, params->gammaCorrect))
				{
					Warningf("Exporting HDR texture %s failed because input file is not valid!!!", params->inputFiles[i]);
					return 0;
				}
			}
		}
		if(params->outputFile)
		{
			strcpy(m_BufferExportFile, params->outputFile);
		}
	}
	else
	{
		m_ColorExponent.Set(1.0f,1.0f,1.0f);
		m_ColorOffset.Set(0,0,0);
	}
#if 0 // DEBUGGING ONLY
	AddExposureTexture("window_-6",-3);
	AddExposureTexture("window_-2",-1);
	AddExposureTexture("window_0",0);
	AddExposureTexture("window_+2",1);
#endif
	do
	{
		TIME.Update();
		ProcessClientPipe();
#if __WIN32PC
		for(int i=0; i < m_ReferenceTextureSet->GetNumTextures(); i++)
		{
			UpdateWindow(m_ReferenceTextureSet->GetTexture(i).GetHWND()); 
		}
#endif
		m_MergeTexturesShader->InitVars();
		m_PickTexturesShader->InitVars();
		m_MaxExponentShader->InitVars();
		m_DefaultShader->InitVars();

		char token[MAX_STR_LENGTH];
		if(m_ListTokenizer)
		{
			m_ListTokenizer->GetToken(token, MAX_STR_LENGTH);
			strcpy(m_BufferWorkspaceFile, token);
			LoadWorkspace();
		}
		if(!m_PickTexturesRT && (m_ExposureTextureSet->GetNumTextures() || (m_Pipe && !m_IsServer && m_ReferenceTextureSet->GetNumTextures() )))
		{
#if 0			// for some reason when initializing this to a lower res, mipping goes wonky. TODO: Fix me!
			if(params && params->outWidth && params->outHeight)
			{
				InitRenderTargets(params->outWidth, params->outHeight);
			}
			else
#endif
			{
				if(m_ExposureTextureSet->GetNumTextures())
				{
					InitRenderTargets(m_ExposureTextureSet->GetTexture(0).GetTexture()->GetWidth(), m_ExposureTextureSet->GetTexture(0).GetTexture()->GetHeight(), m_SoftwareMode);
				}
				else
				{
					// HACK we really shouldn't initialize the render targets just on m_ReferenceTextureSet textures.
					InitRenderTargets(m_ReferenceTextureSet->GetTexture(0).GetTexture()->GetWidth(), m_ReferenceTextureSet->GetTexture(0).GetTexture()->GetHeight());
				}
			}
		}
		if(m_PickTexturesRT && (m_ExposureTextureSet->GetNumTextures() || (m_Pipe && !m_IsServer && m_ReferenceTextureSet->GetNumTextures() )))
		{
			m_MergeTexturesShader->SetInputMergeBit_RT(m_PickTexturesRT);
			m_MergeTexturesShader->SetInputFloat_RT(m_ComposedRT);
			m_MergeTexturesShader->SetInputRGBE_RT(m_SaveRT);
			m_MaxExponentShader->SetPickingTexture(m_PickTexturesRT);
			m_MaxExponentShader->SetExponentTexture(m_MaxExponentRT);
			m_MaxExponentShader->SetBlitTexture(m_MaxExponentRT);
		}
		
		grcViewport::SetCurrent(grcViewport::GetDefaultScreen());
		grcViewport::SetCurrentWorldIdentity();
		Setup.BeginUpdate();

		Setup.EndUpdate();

		Setup.BeginDraw();

		grcState::SetDepthTest(false);
		grcState::SetDepthWrite(false);
		grcState::SetAlphaBlend(false);
		grcState::SetAlphaTest(false);
		grcState::SetBlendSet(grcbsOverwrite);
		

		if(m_PickTexturesRT)
		{
			if(m_Pipe && !m_IsServer)
			{
				DrawClientView();
			}
			else
			{
				DrawMainView();
			}
		}
#if __WIN32PC
		if(m_Pipe)
		{
			if(memcmp(&s_CurrentPPPPreset, &GRPOSTFX->GetPPPPresets(), sizeof(rage::grPostFX::grcPPPPreset) ))
			{
				hdrClientPipe::SetClientPPP packet;
				memcpy(&packet.m_PPPPreset, &GRPOSTFX->GetPPPPresets(), sizeof(rage::grPostFX::grcPPPPreset));
				m_Pipe->Send(packet);
				memcpy(&s_CurrentPPPPreset, &GRPOSTFX->GetPPPPresets(), sizeof(rage::grPostFX::grcPPPPreset) );
			}
		}
#endif
		if(m_PickTexturesRT)
		{
#endif
#if USE_GRPOSTFX
		// run post-processing pipeline
			if(!m_Pipe || !m_IsServer)
			{
				GRPOSTFX->Process();
			}
#endif //USE_GRPOSTFX
			if(!m_Pipe || m_IsServer)
			{
				DrawMainViewText();
			}
		}
		//		pGame->Draw();
		if(m_ListTokenizer || (params && params->doExport))
		{
			if(!ranOnce)
			{
				ComputeColorExponent();
				ranOnce = true;
				continue;
			}

			ExportTexture();
			m_NumFilesProcessed++;
			if(m_NumFilesProcessed == m_NumFilesToProcess || (params && params->doExport))
				m_DoneExporting = true;
		}

#if __XENON
		GRCDEVICE.SetShaderGPRAllocation(0,0);
#endif //__XENON

		Setup.EndDraw();
	}	while	(!m_SoftwareMode && !Setup.WantExit() && !m_DoneExporting);

	if(params )
	{
		params->exponent = m_ColorExponent;
		params->offset = m_ColorOffset;
		if(m_ExposureTextureSet->GetNumTextures())
		{
			params->exposures[0] = m_ExposureTextureSet->GetTexture(0).GetExposure();
		}
		while(m_ExposureTextureSet->GetNumTextures() > params->inputFiles.GetCount())
		{
			int i=params->inputFiles.GetCount();
			params->inputFiles.Grow() = StringDuplicate(m_ExposureTextureSet->GetTexture(i).GetTextureName());
			params->exposures.Grow() = m_ExposureTextureSet->GetTexture(i).GetExposure();
		}
		DeleteAllTextures();
		m_ExposureTextureSet->Shutdown();
		m_ReferenceTextureSet->Shutdown();

	}
#if __WIN32PC
	m_BackupClientTextureLocations.Reset();
	DeleteAllClientTextures();
#endif
	if(m_ListFile)
	{
		m_ListFile->Close();
	}
	WriteIniFile();
	return(0);
}
#endif
