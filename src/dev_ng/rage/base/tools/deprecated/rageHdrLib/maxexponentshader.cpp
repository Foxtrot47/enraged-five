#include	"MaxExponentShader.h"
#include	"grcore/texture.h"
#include	"grmodel/shader.h"
#include	"grmodel/shaderfx.h"
#include	"grmodel/shadergroup.h"

#if __WIN32PC
#include "embedded_max_exponent_win32_30.h"
#elif __XENON
#include "embedded_max_exponent_fxl_final.h"
#endif

void MaxExponentShader::SetPickingTexture(grcTexture* tex)	{ m_Shader->SetVar(m_PickingTexIdx, tex); }
void MaxExponentShader::SetExponentTexture(grcTexture* tex)	{ m_Shader->SetVar(m_ExponentTexIdx, tex); }
void MaxExponentShader::SetBlitTexture(grcTexture* tex)	{ m_Shader->SetVar(m_BlitTexIdx, tex); }

void MaxExponentShader::Init()
{
	m_Shader = grmShaderFactory::GetInstance().Create("max_exponent");
	m_Shader->Load("embedded:/max_exponent",0,0);
	InitVars();

}

void MaxExponentShader::InitVars()
{
	m_PickingTexIdx = m_Shader->LookupVar("PickingTex");
	m_ExponentTexIdx = m_Shader->LookupVar("ExponentTex");
	m_BlitTexIdx = m_Shader->LookupVar("DiffuseTex");
}

#define SCR_TO_VIEW_X(x) ( Clamp( (float)x/(GRCDEVICE.GetWidth()*0.5f)-1.0f, -1.0f, 1.0f) )
#define SCR_TO_VIEW_Y(x) ( Clamp( (float)-x/(GRCDEVICE.GetHeight()*0.5f)+1.0f, -1.0f, 1.0f) )

void MaxExponentShader::Blit(int x1,int y1,int x2,int y2,float z,float u1,float v1,float u2,float v2,Color32 color,const char *tech) const
{
	m_Shader->TWODBlit(SCR_TO_VIEW_X(x1),SCR_TO_VIEW_Y(y1),SCR_TO_VIEW_X(x2),SCR_TO_VIEW_Y(y2),z,u1,v1,u2,v2,color,m_Shader->LookupTechnique(tech));
}

void MaxExponentShader::Shutdown()
{
	delete m_Shader;
}
