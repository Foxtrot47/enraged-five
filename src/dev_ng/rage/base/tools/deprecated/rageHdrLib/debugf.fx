//**************************************************************//
//  Debug shader
//
//  Alexander Ehrath 1/06
//**************************************************************//

#include "../../../src/shaderlib/rage_common.fxh"
#include "../../../src/shaderlib/rage_samplers.fxh"

float DebugVal0;

BeginSampler(sampler2D,fontTexture,FontSampler,FontTex)
string UIName="Debug Font Texture";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,fontTexture,FontSampler,FontTex)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = LINEAR;
MINFILTER = LINEAR;
MAGFILTER = LINEAR;
EndSampler;

struct VS_OUTPUT 
{
   float4 Pos:       POSITION;
   float2 TexCoord:   TEXCOORD0;
};

#pragma dcl position normal texcoord0 tangent

VS_OUTPUT vs_main(float4 inPos: POSITION, float3 inNormal: NORMAL,
                  float2 inTxr: TEXCOORD0, float3 inTangent: TANGENT)
{
	VS_OUTPUT OUT;
	OUT.Pos = inPos;
	OUT.TexCoord = inTxr;
	return OUT;
}

float4 ps_main(VS_OUTPUT IN) : COLOR 
{
	float val = DebugVal0;
	float4 outcol;
	float2 texc = IN.TexCoord;
	bool period = false;
	bool negative = false;
	if(texc.x > 120.0/128.0)
	{
		texc.x -= 120.0/128.0;
		val = val * (100000.0/1.0);
	}
	else if(texc.x > 112.0/128.0)
	{
		texc.x -= 112.0/128.0;
		val = val * (10000.0/1.0);
	}
	else if(texc.x > 104.0/128.0)
	{
		texc.x -= 104.0/128.0;
		val = val * (1000.0/1.0);
	}
	else if(texc.x > 96.0/128.0)
	{
		texc.x -= 96.0/128.0;
		val = val * (100.0/1.0);
	}
	else if(texc.x > 88.0/128.0)
	{
		texc.x -= 88.0/128.0;
		val = val * (10.0/1.0);
	}
	else if(texc.x > 80.0/128.0)
	{
		texc.x -= 80.0/128.0;
		val = val * (1.0/1.0);
	}
	else if(texc.x > 72.0/128.0)
	{
		texc.x -= 72.0/128.0;
		period = true;
	}
	else if(texc.x > 64.0/128.0)
	{
		texc.x -= 64.0/128.0;
		val = val * (1.0/10.0);
	}
	else if(texc.x > 56.0/128.0)
	{
		texc.x -= 56.0/128.0;
		val = val * (1.0/100.0);
	}
	else if(texc.x > 48.0/128.0)
	{
		texc.x -= 48.0/128.0;
		val = val * (1.0/1000.0);
	}
	else if(texc.x > 40.0/128.0)
	{
		texc.x -= 40.0/128.0;
		val = val * (1.0/10000.0);
	}
	else if(texc.x > 32.0/128.0)
	{
		texc.x -= 32.0/128.0;
		val = val * (1.0/100000.0);
	}
	else if(texc.x > 24.0/128.0)
	{
		texc.x -= 24.0/128.0;
		val = val * (1.0/1000000.0);
	}
	else if(texc.x > 16.0/128.0)
	{
		texc.x -= 16.0/128.0;
		val = val * (1.0/10000000.0);
	}
	else if(texc.x > 8.0/128.0)
	{
		texc.x -= 8.0/128.0;
		val = val * (1.0/100000000.0);
	}
	else
	{
		if(val < 0.0)
		{
			negative = true;
		}
		val = val * (1.0/1000000000.0);
	}
	val = abs(val);
	int val2 = (int) val;
	val = val - (float)val2;
	texc.y *= 1.0/8.0;
	texc.y += 6.0*(8.0/128.0);
	if(!period && !negative)
	{
		if(val < 0.1)
		{
			texc.x += (120.0/128.0);
			texc.y -= 2.0*(8.0/128.0);
		}
		else if (val < 0.2)
		{
			texc.x += (0.0/128.0);
		}
		else if (val < 0.3)
		{
			texc.x += (8.0/128.0);
		}
		else if (val < 0.4)
		{
			texc.x += (16.0/128.0);
		}
		else if (val < 0.5)
		{
			texc.x += (24.0/128.0);
		}
		else if (val < 0.6)
		{
			texc.x += (32.0/128.0);
		}
		else if (val < 0.7)
		{
			texc.x += (40.0/128.0);
		}
		else if (val < 0.8)
		{
			texc.x += (48.0/128.0);
		}
		else if (val < 0.9)
		{
			texc.x += (56.0/128.0);
		}
	}
	else
	{	
		texc.y -= 2.0*(8.0/128.0);
		if(period)
		{
			texc.x += (104.0/128.0);
		}
		else
		{
			texc.x += (96.0/128.0);
		}
	}
	

	outcol = tex2D(FontSampler,texc);		
	return outcol;
}


//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps_main();
   }
}

