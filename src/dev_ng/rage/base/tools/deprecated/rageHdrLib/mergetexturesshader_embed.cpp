#include	"mergetexturesshader.h"
#include	"grcore/texture.h"
#include	"grmodel/shader.h"
#include	"grmodel/shaderfx.h"
#include	"grmodel/shadergroup.h"

#include "shader_merge_textures_win32_20.h"
#include "shader_merge_textures_win32_30.h"
#include "shader_merge_textures_fxl_final.h"

const char* MergeTexturesShader::sm_ShaderTexVars[MAX_EXPOSURES] = { 
	"DiffuseTex0", "DiffuseTex1", "DiffuseTex2", "DiffuseTex3",
		"DiffuseTex4", "DiffuseTex5", "DiffuseTex6", "DiffuseTex7",
		"DiffuseTex8", "DiffuseTex9", "DiffuseTex10", "DiffuseTex11",
		"DiffuseTex12", "DiffuseTex13"
		//		, "DiffuseTex14", "DiffuseTex15"
};


void MergeTexturesShader::SetConvertToRGBE(bool on) { m_Effect.SetGlobalVar(m_ConvertToRGBEIdx, on); }
void MergeTexturesShader::SetConvertFromRGBE(bool on) { m_Effect.SetGlobalVar(m_ConvertFromRGBEIdx, on); }
void MergeTexturesShader::SetRenderExponents(bool on) { m_Effect.SetGlobalVar(m_ShowExponentIdx, on); }
void MergeTexturesShader::SetDisplayExposure(float exposure) { m_Effect.SetGlobalVar(m_DisplayExposureIdx, exposure); }
void MergeTexturesShader::SetInputMergeBit_RT(grcRenderTarget* inputRT)	{ m_Effect.SetVar(m_MergePickingTexIdx, inputRT); }
void MergeTexturesShader::SetInputRGBE_RT(grcRenderTarget* inputRT)	{ m_Effect.SetVar(m_RGBETextureIdx, inputRT); }
void MergeTexturesShader::SetInputTexture(grcTexture* inputTexture, int idx)	{ m_Effect.SetVar(m_TextureIdx[idx], inputTexture); }
void MergeTexturesShader::SetSingleExponent(float exponent) {	m_Effect.SetGlobalVar(m_SingleExponentIdx, exponent ); }
void MergeTexturesShader::SetExposures(float *exposureArray, int numExposures)
{
	m_Effect.SetGlobalVar(m_NumTexIdx, (float)(1 << (numExposures-1))/255.0f);
	m_Effect.SetGlobalVar(m_ExposuresIdx, exposureArray ,numExposures);
}
void MergeTexturesShader::Init()
{
	m_Effect.Init("embedded:/merge_textures");
	m_MergePickingTexIdx = m_Effect.LookupVar("PickingTex");
	m_RGBETextureIdx = m_Effect.LookupVar("RGBETex");
	m_DisplayExposureIdx = m_Effect.LookupGlobalVar("DisplayExposure");
	m_SingleExponentIdx = m_Effect.LookupGlobalVar("SingleExponent");

	m_ConvertToRGBEIdx = m_Effect.LookupGlobalVar("convertToRGBE");
	m_ConvertFromRGBEIdx = m_Effect.LookupGlobalVar("convertFromRGBE");
	m_ShowExponentIdx = m_Effect.LookupGlobalVar("showExponent");
	m_NumTexIdx = m_Effect.LookupGlobalVar("NumTex");
	m_ExposuresIdx = m_Effect.LookupGlobalVar("Exposures");
	for(int i=0; i<MAX_EXPOSURES; i++)
	{
		m_TextureIdx[i] = m_Effect.LookupVar(sm_ShaderTexVars[i]);
	}
}
void MergeTexturesShader::Blit(int x1,int y1,int x2,int y2,float z,float u1,float v1,float u2,float v2,Color32 color,const char *tech) const
{
	int numPasses = m_Effect.BeginDraw(m_Effect.LookupTechnique(tech),false);
	for (int i = 0; i < numPasses; ++i) {
		m_Effect.BeginPass(i);
		GRCDEVICE.BlitRectf((float)x1,(float)y1,(float)x2,(float)y2,z,u1,v1,u2,v2,color);
		m_Effect.EndPass();
	}
	m_Effect.EndDraw();
}
