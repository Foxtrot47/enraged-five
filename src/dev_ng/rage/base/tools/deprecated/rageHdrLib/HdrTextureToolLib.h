#ifndef __RAGEHDRTEXTURETOOLLIB_H
#define __RAGEHDRTEXTURETOOLLIB_H

#include "ragehdrlib/views.h"
#include "ragetexturelib/ragetextureconvert.h"
#include "data/base.h"
#include "vector/vector3.h"
#include "atl/array.h"
#include "grcore/viewport.h"
#if __WIN32PC
#include "system/xtl.h"
#include	"hdrclientpipe.h"
#endif

#define MAX_EXPOSURES 14
#define MAX_CLIENT_TEXTURES 20

namespace rage
{
	class grcRenderTarget;
	class fiTokenizer;
	class fiStream;
	class sysNamedPipe;
	class grcViewport;
};

class hdrTextureSet;

class MergeTexturesShader;
class PickTexturesShader;
class MaxExponentShader;
class hdrClientPipe;

struct IDirect3DSurface9;
class HDRTextureTool : public datBase
{
#if !__XENON
	// Other windows this app may open, must have access to this class!
	friend LRESULT CALLBACK TextureToolWndProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);
#endif
public:
	struct RunParams
	{
		RunParams() : outputFile(NULL), exponent(0,0,0), offset(0,0,0), outWidth(0), outHeight(0), 
			gammaCorrect(0.625f), doExport(false), ignoreAlpha(false)
		{
		}
		atArray <const char*> inputFiles;
		atArray	<float> exposures; 
		// exponent and offset get returned by tool.
		Vector3 exponent; 
		Vector3 offset;
		const char* outputFile; 
		int outWidth;
		int outHeight;
		float gammaCorrect;
		bool doExport;
		// specify ignore alpha when alpha is not used as transparency (height map for example).
		bool ignoreAlpha;
	};

	static const int MAX_STR_LENGTH = 512;
	HDRTextureTool() : m_Version(2), m_TextureConvert(NULL), m_Params(NULL), m_Initialized(false), m_PickTexturesRT(NULL), m_MaxExponentRT(NULL), m_ComposedRT(NULL), m_SaveRT(NULL), m_SaveTargetSurface(NULL), m_FloatTargetSurface(NULL),
	m_NumExposures(8), m_NumRows(1),
	m_ListFilename(NULL), m_ListTokenizer(NULL), m_ListFile(NULL), m_DoneExporting(false), m_NumFilesToProcess(0), m_NumFilesProcessed(0),
	m_UseGlobalExponents(false),m_IgnoreAlpha(false),m_IgnoreBadPixels(false), m_SoftwareMode(false) , m_DisplayExposure(0.0f), m_ColorExponent(0,0,0), m_ColorOffset(0,0,0), m_DisplayBadPixels(true), m_Pause(false)
	{
		m_BufferWorkspaceFile[0] = 0;
		m_BufferAddFile[0] = 0;
		m_BufferExportFile[0] = 0;
		m_BufferSrcPath[0] = 0;
		m_BufferDstPath[0] = 0;
		m_BufferAddClientFile[0] = 0;
	}
	int	Run(RunParams* params = NULL);

	void Init(const char* path = "", bool noWindow=false);
	void Shutdown();

	void InitRenderTargets(int width, int height, bool initSaveRTOnly=false);

	void ReleaseRenderTargets();
	void ReleaseAllTextures();

	// Calls the two functions above. Required for callback.
	void DeleteAllTextures();

	// loads the texture to be sent to the client and displays it in a separate window.
	int AddReferenceTexture(const char* filename, float exposure,  int draworder = -1, int width = 0, int height = 0, float gammaCorrect = 0.625f);

	// loads a texture to be added to the set of textures that defines the final HDR image. (This is for the widget callback)
	void LoadTexture(void *file);

	// loads a texture to be added to the set of textures that defines the final HDR image.
	int AddExposureTexture(const char* filename, float exposure, int draworder = -1, int width = 0, int height = 0, float gammaCorrect = 0.625f);

	void AssignTexturesToShaders(int id);

	void ProcessClientPipe();

	void LoadExposureTexture(const char *filename, int draworder = -1, float exposure = 0.0f);

	void LoadClientTextureMultiDialog();
	void LoadClientTexture(const char *filename, int draworder = -1);
	void DeleteAllClientTextures();
	void ExportTexture();

	// Communication functions
	void SetClientIgnoreAlpha();
	void SetClientDisplayExposure();

	// Composes and draws HDR texture composed by multiple exposures
	void DrawMultipleExposureTexture();

	// Draw function when tool runs as client
	void DrawClientView();

	// Main Draw function
	void DrawMainView();

	// Draw text for main view (separate so it can be drawn after the PPP)
	void DrawMainViewText();

	// CPU version of composing an HDR texture from several expsoure textures. usually done on the GPU.
	void ComposeExposureTextures();

	void LoadWorkspace();
	void LoadWorkspaceFileDialog();
	void SaveWorkspace();

	void ComputeColorExponent(int id=-1);

	void ReadIniFile();
	void WriteIniFile();
	bool Initialized() { return m_Initialized; }
	static HDRTextureTool* sm_CurrentInstance;
	TextureConvert* m_TextureConvert;
private:
	///////////////////////////////////// FILE VERSION, INCREMENT THIS WHEN FILE FORMAT CHANGES!! ////////////////////////
	int m_Version;
	/////////////////////////////////// END FILE VERSION, INCREMENT THIS WHEN FILE FORMAT CHANGES!! //////////////////////


	RunParams* m_Params;
	bool m_Initialized;
	bool m_ExportFileDialog;
	// different views that the texture can be displayed in
	BlitFromComposedFloat m_BlitFromComposedFloat;
	ComposedFloatToRGBETexture m_ComposedFloatToRGBETextureOnScreen;
	ComposedFloatToRGBETexture m_ComposedFloatToRGBETexture;
	ComposeFloatTexture m_ComposeFloatTexture;
	BlitRGBEToFloatTexture m_BlitRGBEToFloatTextureMultiExposure;
	BlitRGBEToFloatTexture m_BlitRGBEToFloatTextureEndResult;

	// Necessary render targets
	grcRenderTarget* m_PickTexturesRT;
	grcRenderTarget* m_MaxExponentRT;
	grcRenderTarget* m_ComposedRT;
	grcRenderTarget* m_SaveRT;
	IDirect3DSurface9* m_SaveTargetSurface;
	IDirect3DSurface9* m_FloatTargetSurface;


	// Layout of textures, this might change
	int m_NumExposures;
	int m_NumRows;

	// Different buffers for filenames.
	char m_BufferWorkspaceFile[MAX_STR_LENGTH];
	char m_BufferAddFile[MAX_STR_LENGTH];
	char m_BufferExportFile[MAX_STR_LENGTH];
	char m_BufferSrcPath[MAX_STR_LENGTH];
	char m_BufferDstPath[MAX_STR_LENGTH];
	char m_BufferAddClientFile[MAX_STR_LENGTH];

	// List file related stuff
	const char *m_ListFilename;
	fiTokenizer* m_ListTokenizer;
	fiStream *m_ListFile;
	bool m_DoneExporting;

	int m_NumFilesToProcess;
	int m_NumFilesProcessed;

	// switches for different display options.
	bool m_UseGlobalExponents;

	// Ignore alpha on incoming exposure textures.
	bool m_IgnoreAlpha;

	// Ignore pixels with invalid pixelcolor
	bool m_IgnoreBadPixels;

	// draws undefined pixels in hot pink color.
	bool m_DisplayBadPixels;

	// if we load a DDS that has HDR information, this should be true.
	bool m_DisplayHDR_DDS;

	// Run in SoftwareMode, use no rendertargets if true
	bool m_SoftwareMode;

	// global display values
	float m_DisplayExposure;
	Vector3 m_ColorExponent;
	Vector3 m_ColorOffset;

	// Set of textures to compose an HDR texture.
	hdrTextureSet* m_ExposureTextureSet;

	// Set of textures to be displayed on client side.
	hdrTextureSet* m_ReferenceTextureSet;

	// global shaders to be used by different views.
	MergeTexturesShader* m_MergeTexturesShader;
	PickTexturesShader* m_PickTexturesShader;
	MaxExponentShader* m_MaxExponentShader;
	DefaultShader* m_DefaultShader;

	// Client Server variables
	bool m_IsServer;
	hdrClientPipe* m_Pipe;
	bool m_Pause;
	// main viewport
	grcViewport m_Viewport;

	// new windows that can be created
#if __WIN32PC
	atArray<hdrClientPipe::SetClientTextureData> m_BackupClientTextureLocations;
	hdrClientPipe::SetClientTextureData m_BackupExposureTextureLocation;
#endif
	atArray<DisplayTexture> m_DisplayTexture;


};


#endif
