#include "views.h"

MergeTexturesShader *ComposeFloatTexture::sm_Shader;
MergeTexturesShader *ComposedFloatToRGBETexture::sm_Shader;
MergeTexturesShader *BlitRGBEToFloatTexture::sm_Shader;
MergeTexturesShader *BlitFromComposedFloat::sm_Shader;
DefaultShader *DisplayTexture::sm_Shader;
DisplayTexture::DisplayTexture()
{
}

void DisplayTexture::Draw()
{
	sm_Shader->Blit( m_ScreenX, m_ScreenY, m_ScreenX+m_Width, m_ScreenY+m_Height,
		0.1f, 0.0f, 0.0f, 1.0f, 1.0f, Color32(1.0f,1.0f,1.0f,1.0f),"draw");
}


BlitFromComposedFloat::BlitFromComposedFloat()
{
}

void BlitFromComposedFloat::Draw()
{
	sm_Shader->SetDisplayExposure(0.0f);
	sm_Shader->Blit( m_ScreenX, m_ScreenY, m_ScreenX+m_Width, m_ScreenY+m_Height,
		0.1f, 0.0f, 0.0f, 1.0f, 1.0f, Color32(1.0f,1.0f,1.0f,1.0f),"BlitFromComposedFloat");
}


ComposeFloatTexture::ComposeFloatTexture()
{
}

void ComposeFloatTexture::Draw()
{
	sm_Shader->SetDisplayExposure(0.0f);
	sm_Shader->Blit( m_ScreenX, m_ScreenY, m_ScreenX+m_Width, m_ScreenY+m_Height,
		0.1f, 0.0f, 0.0f, 1.0f, 1.0f, Color32(1.0f,1.0f,1.0f,1.0f),"BlitToComposedFloatTexture");
}

ComposedFloatToRGBETexture::ComposedFloatToRGBETexture()
{
	m_UseGlobalExponents = false;
}
void ComposedFloatToRGBETexture::Draw()
{
	sm_Shader->SetUseGlobalExponents(m_UseGlobalExponents);
	sm_Shader->Blit( m_ScreenX, m_ScreenY, m_ScreenX+m_Width, m_ScreenY+m_Height,
		0.1f, 0.0f, 0.0f, 1.0f, 1.0f, Color32(1.0f,1.0f,1.0f,1.0f),"BlitComposedFloatToRGBETexture");
}


BlitRGBEToFloatTexture::BlitRGBEToFloatTexture() : m_UseGlobalExponents(false)
{
	m_UseGlobalExponents = false;
}

void BlitRGBEToFloatTexture::Draw()
{
	sm_Shader->SetUseGlobalExponents(m_UseGlobalExponents);
	sm_Shader->Blit( m_ScreenX, m_ScreenY, m_ScreenX+m_Width, m_ScreenY+m_Height,
		0.1f, 0.0f, 0.0f, 1.0f, 1.0f, Color32(1.0f,1.0f,1.0f,1.0f),"BlitRGBEToFloatTexture");
}


