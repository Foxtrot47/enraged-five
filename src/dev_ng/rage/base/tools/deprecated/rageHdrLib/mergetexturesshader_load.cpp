#include	"mergetexturesshader.h"
#include	"grcore/texture.h"
#include	"grmodel/shader.h"
#include	"grmodel/shaderfx.h"
#include	"grmodel/shadergroup.h"

const char* MergeTexturesShader::sm_ShaderTexVars[MAX_EXPOSURES] = { 
	"DiffuseTex0", "DiffuseTex1", "DiffuseTex2", "DiffuseTex3",
		"DiffuseTex4", "DiffuseTex5", "DiffuseTex6", "DiffuseTex7",
		"DiffuseTex8", "DiffuseTex9", "DiffuseTex10", "DiffuseTex11",
		"DiffuseTex12", "DiffuseTex13"
		//		, "DiffuseTex14", "DiffuseTex15"
};


void MergeTexturesShader::SetConvertToRGBE(bool on) { m_Shader->GetEffect().SetGlobalVar(m_ConvertToRGBEIdx, on); }
void MergeTexturesShader::SetConvertFromRGBE(bool on) { m_Shader->GetEffect().SetGlobalVar(m_ConvertFromRGBEIdx, on); }
void MergeTexturesShader::SetRenderExponents(bool on) { m_Shader->GetEffect().SetGlobalVar(m_ShowExponentIdx, on); }
void MergeTexturesShader::SetDisplayExposure(float exposure) { m_Shader->GetEffect().SetGlobalVar(m_DisplayExposureIdx, exposure); }
void MergeTexturesShader::SetInputMergeBit_RT(grcRenderTarget* inputRT)	{ m_Shader->GetEffect().SetVar(m_MergePickingTexIdx, inputRT); }
void MergeTexturesShader::SetInputRGBE_RT(grcRenderTarget* inputRT)	{ m_Shader->GetEffect().SetVar(m_RGBETextureIdx, inputRT); }
void MergeTexturesShader::SetInputTexture(grcTexture* inputTexture, int idx)	{ m_Shader->GetEffect().SetVar(m_TextureIdx[idx], inputTexture); }
void MergeTexturesShader::SetSingleExponent(float exponent) {	m_Shader->GetEffect().SetGlobalVar(m_SingleExponentIdx, exponent ); }
void MergeTexturesShader::SetExposures(float *exposureArray, int numExposures)
{
	m_Shader->GetEffect().SetGlobalVar(m_NumTexIdx, (float)(1 << (numExposures-1))/255.0f);
	m_Shader->GetEffect().SetGlobalVar(m_ExposuresIdx, exposureArray ,numExposures);
}
void MergeTexturesShader::Init()
{
	m_Shader = grmShaderFactory::GetInstance().Create("merge_textures");
	m_Shader->Load("shaders/misc/merge_textures",0,0);
	m_MergePickingTexIdx = m_Shader->GetEffect().LookupVar("PickingTex");
	m_RGBETextureIdx = m_Shader->GetEffect().LookupVar("RGBETex");
	m_DisplayExposureIdx = m_Shader->GetEffect().LookupGlobalVar("DisplayExposure");
	m_SingleExponentIdx = m_Shader->GetEffect().LookupGlobalVar("SingleExponent");

	m_ConvertToRGBEIdx = m_Shader->GetEffect().LookupGlobalVar("convertToRGBE");
	m_ConvertFromRGBEIdx = m_Shader->GetEffect().LookupGlobalVar("convertFromRGBE");
	m_ShowExponentIdx = m_Shader->GetEffect().LookupGlobalVar("showExponent");
	m_NumTexIdx = m_Shader->GetEffect().LookupGlobalVar("NumTex");
	m_ExposuresIdx = m_Shader->GetEffect().LookupGlobalVar("Exposures");
	for(int i=0; i<MAX_EXPOSURES; i++)
	{
		m_TextureIdx[i] = m_Shader->GetEffect().LookupVar(sm_ShaderTexVars[i]);
	}
}
void MergeTexturesShader::Blit(int x1,int y1,int x2,int y2,float z,float u1,float v1,float u2,float v2,Color32 color,const char *tech) const
{
	m_Shader->Blit(x1,y1,x2,y2,z,u1,v1,u2,v2,color,m_Shader->GetEffect().LookupTechnique(tech));
}
