//**************************************************************//
//  Pick HDR color shader
//
//  Alexander Ehrath 1/06
//**************************************************************//
#include "../../../src/shaderlib/rage_common.fxh"
#include "../../../src/shaderlib/rage_samplers.fxh"

float PickNumTex;
#define Epsilon 0.0001
bool PickDrawBadPixels;

#define DRAW_UNDEFINED_AREAS_PINK 0

#include "pick_textures_samplers.fxh"

struct VS_OUTPUT 
{
   float4 Pos:       POSITION;
   float2 TexCoord:   TEXCOORD0;
};

#pragma dcl position normal texcoord0 tangent

VS_OUTPUT vs_main(float4 inPos: POSITION, float3 inNormal: NORMAL,
                  float2 inTxr: TEXCOORD0, float3 inTangent: TANGENT)
{
	VS_OUTPUT OUT;
	OUT.Pos = inPos;
	OUT.TexCoord = inTxr;
	return OUT;
}

bool IsColorValid(float4 texcol)
{
	return ( (texcol.r > Epsilon && texcol.r < 1.0-Epsilon) || (texcol.g > Epsilon && texcol.g < 1.0-Epsilon) || (texcol.b > Epsilon && texcol.b < 1.0-Epsilon) );
}

float4 ps_main(VS_OUTPUT IN) : COLOR 
{
	float4 outcol = float4(0.0,0.0,0.0,0.0);
	float TexBits = 1.0/255.0;
	float pickNumTex = PickNumTex;
	if(TexBits <= pickNumTex)		// 1.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler0,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(TexBits, 0.0, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 2.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler1,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(TexBits, 0.0, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 4.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler2,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(TexBits, 0.0, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 8.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler3,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(TexBits, 0.0, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 16.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler4,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(TexBits, 0.0, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 32.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler5,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(TexBits, 0.0, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 64.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler6,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(TexBits, 0.0, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 128.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler7,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(TexBits, 0.0, 0.0, 0.0);					// bit 9
		}
	}
	
// if we merge more than 8 textures, overflow bits from red to green component	TODO: finish other logic to work with more than 8 textures (Might not be necessary)
	pickNumTex *= (1.0/256.0);
	TexBits = 1.0/255.0;
	if(TexBits <= pickNumTex)		// 256.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler8,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(0.0, TexBits, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 512.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler9,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(0.0, TexBits, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 1024.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler10,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(0.0, TexBits, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 2048.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler11,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(0.0, TexBits, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 4096.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler12,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(0.0, TexBits, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 8192.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler13,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(0.0, TexBits, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
#if 0	
	if(TexBits <= pickNumTex)		// 16384.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler14,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(0.0, TexBits, 0.0, 0.0);					// bit 9
		}
	}
	TexBits *= 2.0;
	if(TexBits <= pickNumTex)		// 32768.0/255.0;
	{
		float4 texcol = tex2D(TextureSampler15,IN.TexCoord);
		if(IsColorValid(texcol))
		{
			outcol += float4(0.0, TexBits, 0.0, 0.0);					// bit 9
		}
	}
#endif	
	if(!outcol.r && !outcol.g && !PickDrawBadPixels)
	{
// if we didn't find a valid pixel, just use the first textures color	
		outcol += float4(1.0/255.0, 0.0, 0.0, 0.0);					// bit 9
	}
#if DRAW_UNDEFINED_AREAS_PINK	
	if(outcol.r == 0.0 && outcol.g == 0.0 &&  outcol.b == 0)
	{
		outcol = float4(1.0, 0.0, 1.0, 1.0);;	
	}
	else
	{
		outcol = float4(0.0, 0.0, 0.0, 1.0);;	
	}
#endif
	return outcol;
}


//--------------------------------------------------------------//
technique draw
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;

      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps_main();
   }
}

