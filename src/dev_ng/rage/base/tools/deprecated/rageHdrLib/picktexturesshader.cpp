#include	"PickTexturesShader.h"
#include	"grcore/texture.h"
#include	"grmodel/shader.h"
#include	"grmodel/shaderfx.h"
#include	"grmodel/shadergroup.h"

#if __WIN32PC
#include "embedded_pick_textures_win32_30.h"
#elif __XENON
#include "embedded_pick_textures_fxl_final.h"
#endif

const char* PickTexturesShader::sm_ShaderTexVars[MAX_EXPOSURES] = { 
	"DiffuseTex0", "DiffuseTex1", "DiffuseTex2", "DiffuseTex3",
		"DiffuseTex4", "DiffuseTex5", "DiffuseTex6", "DiffuseTex7",
		"DiffuseTex8", "DiffuseTex9", "DiffuseTex10", "DiffuseTex11",
		"DiffuseTex12", "DiffuseTex13"
		//		, "DiffuseTex14", "DiffuseTex15"
};

void PickTexturesShader::SetInputTexture(grcTexture* inputTexture, int idx)	{ m_Shader->SetVar(m_TextureIdx[idx], inputTexture); }
void PickTexturesShader::SetNumTextures(int numTextures)	{ m_Shader->SetVar(m_NumTexIdx, (float)(1 << (numTextures-1))/255.0f); }
void PickTexturesShader::SetDrawBadPixels(bool on) { m_Shader->SetVar(m_DrawBadPixelsIdx, on); }

void PickTexturesShader::Init()
{
	m_Shader = grmShaderFactory::GetInstance().Create("pick_textures");
	m_Shader->Load("embedded:/pick_textures",0,0);
	InitVars();
}
void PickTexturesShader::InitVars()
{
	m_DrawBadPixelsIdx = m_Shader->LookupVar("PickDrawBadPixels");

	m_NumTexIdx = m_Shader->LookupVar("PickNumTex");
	for(int i=0; i<MAX_EXPOSURES; i++)
	{
		m_TextureIdx[i] = m_Shader->LookupVar(sm_ShaderTexVars[i]);
	}
}

#define SCR_TO_VIEW_X(x) ( Clamp( (float)x/(GRCDEVICE.GetWidth()*0.5f)-1.0f, -1.0f, 1.0f) )
#define SCR_TO_VIEW_Y(x) ( Clamp( (float)-x/(GRCDEVICE.GetHeight()*0.5f)+1.0f, -1.0f, 1.0f) )

void PickTexturesShader::Blit(int x1,int y1,int x2,int y2,float z,float u1,float v1,float u2,float v2,Color32 color,const char *tech) const
{
	m_Shader->TWODBlit(SCR_TO_VIEW_X(x1),SCR_TO_VIEW_Y(y1),SCR_TO_VIEW_X(x2),SCR_TO_VIEW_Y(y2),z,u1,v1,u2,v2,color,m_Shader->LookupTechnique(tech));
}

void PickTexturesShader::Shutdown()
{
	delete m_Shader;
}
