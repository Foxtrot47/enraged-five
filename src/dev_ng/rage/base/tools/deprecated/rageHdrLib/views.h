#ifndef __VIEWS_H
#define __VIEWS_H

#include "ragehdrlib/uielement.h"
#include "ragehdrlib/mergetexturesshader.h"
#include "ragehdrlib/defaultshader.h"

class BlitFromComposedFloat : public UiElement
{
public:
	BlitFromComposedFloat();
	static void SetShader(MergeTexturesShader *shader) { sm_Shader = shader; }
	virtual void Draw();
	//	virtual void Update();
private:
	static MergeTexturesShader *sm_Shader;
};

class ComposeFloatTexture : public UiElement
{
public:
	ComposeFloatTexture();
	static void SetShader(MergeTexturesShader *shader) { sm_Shader = shader; }
	virtual void Draw();
	//	virtual void Update();
private:
	static MergeTexturesShader *sm_Shader;
};

class ComposedFloatToRGBETexture : public UiElement
{
public:
	ComposedFloatToRGBETexture();
	static void SetShader(MergeTexturesShader *shader) { sm_Shader = shader; }
	virtual void Draw();
	void SetUseGlobalExponents(bool on) { m_UseGlobalExponents = on; }
	//	virtual void Update();
private:
	bool m_UseGlobalExponents;
	static MergeTexturesShader *sm_Shader;
};

class BlitRGBEToFloatTexture : public UiElement
{
public:
	BlitRGBEToFloatTexture();
	static void SetShader(MergeTexturesShader *shader) { sm_Shader = shader; }
	virtual void Draw();
	void SetUseGlobalExponents(bool on) { m_UseGlobalExponents = on; }
	//	virtual void Update();
private:
	bool m_UseGlobalExponents;
	static MergeTexturesShader *sm_Shader;
};


class DisplayTexture : public UiElement
{
public:
	DisplayTexture();
	static void SetShader(DefaultShader *shader) { sm_Shader = shader; }
	virtual void Draw();
	//	virtual void Update();
private:
	static DefaultShader *sm_Shader;
};

#endif
