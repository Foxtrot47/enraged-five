#ifndef __PICKTEXTURESSHADER_H
#define __PICKTEXTURESSHADER_H
#include	"grcore/effect.h"
#include	"vector/color32.h"

using	namespace	rage;

namespace	rage
{
	class grmShader;
	class grcRenderTarget;
	class grcTexture;
}

#define MAX_EXPOSURES 14

class PickTexturesShader
{
public:
	void SetDrawBadPixels(bool on);
	void SetInputTexture(grcTexture* inputTexture, int idx);
	void SetNumTextures(int numTextures);

	void Init();
	void InitVars();
	void Blit(int x1,int y1,int x2,int y2,float z,float u1,float v1,float u2,float v2,Color32 color,const char *tech) const;
	const char **GetShaderTexVars() { return sm_ShaderTexVars; }
	void Shutdown();
protected:
	grmShader* m_Shader;
//	grcEffect m_Effect;

	grcEffectVar m_TextureIdx[MAX_EXPOSURES];
	grcEffectVar m_NumTexIdx;
	grcEffectVar m_DrawBadPixelsIdx;
	static const char* sm_ShaderTexVars[MAX_EXPOSURES];
};

#endif
