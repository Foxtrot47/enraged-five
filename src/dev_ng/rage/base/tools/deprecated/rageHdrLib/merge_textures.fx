//**************************************************************//
//  HDR texture merging shader
//
//  Alexander Ehrath 1/06
//**************************************************************//
#include "../../../src/shaderlib/rage_common.fxh"
#include "../../../src/shaderlib/rage_samplers.fxh"

float3 MergeColorExponent = {0,0,0};
float3 MergeColorOffset = {0,0,0};
float MergeNumTex = 0.0/255.0;
float MergeExposures[16];
float MergeDisplayExposure=0.0;
bool MergeuseGlobalExponents = false;
bool MergeconvertToRGBE = true;
bool MergeconvertFromRGBE = false;
bool MergeshowExponent = false;
bool MergeIgnoreAlpha = false;
bool MergeUseTex0Alpha = true;


#include "pick_textures_samplers.fxh"


BeginSampler(sampler2D,composedFloatTexture,ComposedFloatTextureSampler,MergeFloatTex)
string UIName="Diffuse Texture 0";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,composedFloatTexture,ComposedFloatTextureSampler,MergeFloatTex)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,rgbeTexture,RGBETextureSampler,MergeRGBETex)
string UIName="Diffuse Texture 0";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,rgbeTexture,RGBETextureSampler,MergeRGBETex)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,pickingTexture,PickingTextureSampler,MergePickingTex)
string UIName="Diffuse Texture 0";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,pickingTexture,PickingTextureSampler,MergePickingTex)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;



///////////////////// TOTAL FUCKING HACK, THIS SHOULD BE IN A HEADER /////////////////////////////


float4 ComputeNewExposedColor(float totalcolors, float4 texcol, float4 exposedcol, int exposure)
{
	float4 newcol = texcol * (pow( 2, -MergeExposures[exposure]));
	float newcolorweight = 1.0/totalcolors;
	if(!MergeIgnoreAlpha)
	{
		newcolorweight *= texcol.w;
	}
	float oldcolorweight = 1.0-newcolorweight;
	exposedcol = (newcol * newcolorweight) + (exposedcol * oldcolorweight);
	return exposedcol;
}

float4 pickTexCol(float2 TexCoord)//, float Exposures[16])
{
	float4 pickcol = tex2D(PickingTextureSampler,TexCoord);
	float4 texcol = float4(1.0, 0.0, 1.0, 1.0);
	float4 exposedcol = float4(1.0, 0.0, 1.0, 1.0);
	
	// check the first 8 bits for valid texture value.
	pickcol *= 255.0;
	float pickfac = 255.0*3.0;
	float totalcolors = 0.0;
	
	if(pickcol.r >= 127.9)
	{
		texcol = tex2D(TextureSampler7,TexCoord);		
		{
			pickcol.r -= 128.0; 
			totalcolors+= 1.0;
			exposedcol = ComputeNewExposedColor(totalcolors, texcol, exposedcol, 7);
		}
	}
	if(pickcol.r >= 63.9)
	{
		texcol = tex2D(TextureSampler6,TexCoord);
		{
			pickcol.r -= 64.0; 
			totalcolors+= 1.0;
			exposedcol = ComputeNewExposedColor(totalcolors, texcol, exposedcol, 6);
		}
	}
	if(pickcol.r >= 31.9)
	{
		texcol = tex2D(TextureSampler5,TexCoord);
		{
			pickcol.r -= 32.0; 
			totalcolors+= 1.0;
			exposedcol = ComputeNewExposedColor(totalcolors, texcol, exposedcol, 5);
		}
	}
	if(pickcol.r >= 15.9)
	{
		texcol = tex2D(TextureSampler4,TexCoord);
		{
			pickcol.r -= 16.0; 
			totalcolors+= 1.0;
			exposedcol = ComputeNewExposedColor(totalcolors, texcol, exposedcol, 4);
		}
	}
	if(pickcol.r >= 7.9)
	{
		texcol = tex2D(TextureSampler3,TexCoord);
		{
			pickcol.r -= 8.0; 
			totalcolors+= 1.0;
			exposedcol = ComputeNewExposedColor(totalcolors, texcol, exposedcol, 3);
		}
	}
	if(pickcol.r >= 3.9)
	{
		texcol = tex2D(TextureSampler2,TexCoord);
		{
			pickcol.r -= 4.0; 
			totalcolors+= 1.0;
			exposedcol = ComputeNewExposedColor(totalcolors, texcol, exposedcol, 2);
		}
	}
	if(pickcol.r >= 1.9)
	{
		texcol = tex2D(TextureSampler1,TexCoord);
		{
			pickcol.r -= 2.0; 
			totalcolors+= 1.0;
			exposedcol = ComputeNewExposedColor(totalcolors, texcol, exposedcol, 1);
		}
	}
	if(pickcol.r >= 0.9)
	{
		texcol = tex2D(TextureSampler0,TexCoord);
		{
			pickcol.r -= 1.0; 
			totalcolors+= 1.0;
			exposedcol = ComputeNewExposedColor(totalcolors, texcol, exposedcol, 0);
		}
	}
	if(MergeUseTex0Alpha)
	{
		exposedcol.w = tex2D(TextureSampler0,TexCoord).w;
	}
	return exposedcol;
}

//////////////////////////////////////////////////////////////////////////////////////////////////



struct VS_OUTPUT 
{
   float4 Pos:       POSITION;
   float2 TexCoord:   TEXCOORD0;
};

#pragma dcl position normal texcoord0 tangent

VS_OUTPUT vs_main(float4 inPos: POSITION, float3 inNormal: NORMAL,
                  float2 inTxr: TEXCOORD0, float3 inTangent: TANGENT)
{
	VS_OUTPUT OUT;
	OUT.Pos = inPos;
	OUT.TexCoord = inTxr;
	return OUT;
}

#include "merge_textures.fxh"

float4 ps_merge(VS_OUTPUT IN) : COLOR 
{
	float4 mergecol = pickTexCol(IN.TexCoord);	
	return mergecol;
}

float4 ps_BlitComposedFloatToRGBETexture(VS_OUTPUT IN) : COLOR 
{
	float4 col = tex2D(ComposedFloatTextureSampler, IN.TexCoord);	
	if(MergeuseGlobalExponents)
	{
		col.xyz = float2rgb3e(col.xyz, MergeColorOffset, MergeColorExponent);
	}
	else
	{
		col = float2rgbe(col);
	}
	return col;
}

float4 ps_BlitRGBEToFloatTexture(VS_OUTPUT IN) : COLOR 
{
	float4 col = tex2D(RGBETextureSampler, IN.TexCoord);	
	if(MergeuseGlobalExponents)
	{
		col = rgb3e2float(col, MergeColorOffset, MergeColorExponent);
	}
	else
	{
		col = rgbe2float(col);
	}
	col = col * (pow( 2, MergeDisplayExposure));	
	return col;
}

float4 ps_BlitFromComposedFloat(VS_OUTPUT IN) : COLOR 
{
	return tex2D(ComposedFloatTextureSampler, IN.TexCoord);	
}


technique BlitToComposedFloatTexture
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps_merge();
   }
}

technique BlitComposedFloatToRGBETexture
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps_BlitComposedFloatToRGBETexture();
   }
}

technique BlitRGBEToFloatTexture
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps_BlitRGBEToFloatTexture();
   }
}

technique BlitFromComposedFloat
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps_BlitFromComposedFloat();
   }
}

technique draw
{
   pass p0
   {
		CullMode = NONE;
 		ZEnable = false;
		ZWriteEnable = false;
        AlphaBlendEnable = false;
        AlphaTestEnable = false;
		ColorWriteEnable = RED|GREEN|BLUE|ALPHA;

      VertexShader = compile vs_3_0 vs_main();
      PixelShader = compile ps_3_0 ps_merge();
   }
}



