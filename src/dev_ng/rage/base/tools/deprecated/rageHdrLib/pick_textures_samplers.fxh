BeginSampler(sampler2D,diffuseTexture0,TextureSampler0,DiffuseTex0)
string UIName="Diffuse Texture 0";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture0,TextureSampler0,DiffuseTex0)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;


BeginSampler(sampler2D,diffuseTexture1,TextureSampler1,DiffuseTex1)
string UIName="Diffuse Texture 1";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture1,TextureSampler1,DiffuseTex1)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture2,TextureSampler2,DiffuseTex2)
string UIName="Diffuse Texture 2";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture2,TextureSampler2,DiffuseTex2)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture3,TextureSampler3,DiffuseTex3)
string UIName="Diffuse Texture 3";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture3,TextureSampler3,DiffuseTex3)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture4,TextureSampler4,DiffuseTex4)
string UIName="Diffuse Texture 4";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture4,TextureSampler4,DiffuseTex4)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture5,TextureSampler5,DiffuseTex5)
string UIName="Diffuse Texture 5";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture5,TextureSampler5,DiffuseTex5)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture6,TextureSampler6,DiffuseTex6)
string UIName="Diffuse Texture 6";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture6,TextureSampler6,DiffuseTex6)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture7,TextureSampler7,DiffuseTex7)
string UIName="Diffuse Texture 7";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture7,TextureSampler7,DiffuseTex7)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture8,TextureSampler8,DiffuseTex8)
string UIName="Diffuse Texture 8";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture8,TextureSampler8,DiffuseTex8)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture9,TextureSampler9,DiffuseTex9)
string UIName="Diffuse Texture 9";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture9,TextureSampler9,DiffuseTex9)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture10,TextureSampler10,DiffuseTex10)
string UIName="Diffuse Texture 10";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture10,TextureSampler10,DiffuseTex10)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture11,TextureSampler11,DiffuseTex11)
string UIName="Diffuse Texture 11";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture11,TextureSampler11,DiffuseTex11)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture12,TextureSampler12,DiffuseTex12)
string UIName="Diffuse Texture 12";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture12,TextureSampler12,DiffuseTex12)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

BeginSampler(sampler2D,diffuseTexture13,TextureSampler13,DiffuseTex13)
string UIName="Diffuse Texture 13";
#if DIFFUSE_UV1
string UIHint="uv1";
#endif
ContinueSampler(sampler2D,diffuseTexture13,TextureSampler13,DiffuseTex13)
AddressU  = WRAP;        
AddressV  = WRAP;
AddressW  = WRAP;
MIPFILTER = POINT;
MINFILTER = POINT;
MAGFILTER = POINT;
EndSampler;

// Reset samplers to zero
#pragma sampler 0
