#include	"mergetexturesshader.h"
#include	"grcore/texture.h"
#include	"grmodel/shader.h"
#include	"grmodel/shaderfx.h"
#include	"grmodel/shadergroup.h"

#if __WIN32PC
#include "embedded_merge_textures_win32_30.h"
#elif __XENON
#include "embedded_merge_textures_fxl_final.h"
#endif

const char* MergeTexturesShader::sm_ShaderTexVars[MAX_EXPOSURES] = { 
	"DiffuseTex0", "DiffuseTex1", "DiffuseTex2", "DiffuseTex3",
		"DiffuseTex4", "DiffuseTex5", "DiffuseTex6", "DiffuseTex7",
		"DiffuseTex8", "DiffuseTex9", "DiffuseTex10", "DiffuseTex11",
		"DiffuseTex12", "DiffuseTex13"
		//		, "DiffuseTex14", "DiffuseTex15"
};


void MergeTexturesShader::SetUseGlobalExponents(bool on) { m_Shader->SetVar(m_UseGlobalExponentsIdx, on); }
void MergeTexturesShader::SetConvertToRGBE(bool on) { m_Shader->SetVar(m_ConvertToRGBEIdx, on); }
void MergeTexturesShader::SetConvertFromRGBE(bool on) { m_Shader->SetVar(m_ConvertFromRGBEIdx, on); }
void MergeTexturesShader::SetRenderExponents(bool on) { m_Shader->SetVar(m_ShowExponentIdx, on); }
void MergeTexturesShader::SetIgnoreAlpha(bool on) { m_Shader->SetVar(m_IgnoreAlphaIdx, on); }
void MergeTexturesShader::SetDisplayExposure(float exposure) { m_Shader->SetVar(m_DisplayExposureIdx, exposure); }
void MergeTexturesShader::SetInputMergeBit_RT(grcRenderTarget* inputRT)	{ m_Shader->SetVar(m_MergePickingTexIdx, inputRT); }
void MergeTexturesShader::SetInputFloat_RT(grcRenderTarget* inputRT)	{ m_Shader->SetVar(m_ComposedFloatTextureIdx, inputRT); }
void MergeTexturesShader::SetInputRGBE_RT(grcRenderTarget* inputRT)	{ m_Shader->SetVar(m_RGBETextureIdx, inputRT); }
void MergeTexturesShader::SetInputTexture(grcTexture* inputTexture, int idx)	{ m_Shader->SetVar(m_TextureIdx[idx], inputTexture); }
void MergeTexturesShader::SetColorExponent(Vector3& exponent) {	m_Shader->SetVar(m_ColorExponentIdx, exponent ); }
void MergeTexturesShader::SetColorOffset(Vector3& offset) {	m_Shader->SetVar(m_ColorOffsetIdx, offset ); }
void MergeTexturesShader::SetNumExposures(int numExposures) { m_NumExposures = numExposures; }
void MergeTexturesShader::SetExposure(int texIdx, float exposure)
{
	m_Exposures[texIdx] = exposure;
}

void MergeTexturesShader::Init()
{
	m_Shader = grmShaderFactory::GetInstance().Create("merge_textures");
	m_Shader->Load("embedded:/merge_textures",0,0);
	InitVars();
}

void MergeTexturesShader::InitVars()
{
	m_MergePickingTexIdx = m_Shader->LookupVar("MergePickingTex");
	m_RGBETextureIdx = m_Shader->LookupVar("MergeRGBETex");
	m_ComposedFloatTextureIdx = m_Shader->LookupVar("MergeFloatTex");
	m_DisplayExposureIdx = m_Shader->LookupVar("MergeDisplayExposure");
	m_ColorExponentIdx = m_Shader->LookupVar("MergeColorExponent");
	m_ColorOffsetIdx = m_Shader->LookupVar("MergeColorOffset");

	m_UseGlobalExponentsIdx = m_Shader->LookupVar("MergeuseGlobalExponents");
	m_ConvertToRGBEIdx = m_Shader->LookupVar("MergeconvertToRGBE");
	m_ConvertFromRGBEIdx = m_Shader->LookupVar("MergeconvertFromRGBE");
	m_ShowExponentIdx = m_Shader->LookupVar("MergeshowExponent");
	m_IgnoreAlphaIdx = m_Shader->LookupVar("MergeIgnoreAlpha");
	m_NumTexIdx = m_Shader->LookupVar("MergeNumTex");
	m_ExposuresIdx = m_Shader->LookupVar("MergeExposures");
	for(int i=0; i<MAX_EXPOSURES; i++)
	{
		m_TextureIdx[i] = m_Shader->LookupVar(sm_ShaderTexVars[i]);
	}
}

#define SCR_TO_VIEW_X(x) ( Clamp( (float)x/(GRCDEVICE.GetWidth()*0.5f)-1.0f, -1.0f, 1.0f) )
#define SCR_TO_VIEW_Y(x) ( Clamp( (float)-x/(GRCDEVICE.GetHeight()*0.5f)+1.0f, -1.0f, 1.0f) )

void MergeTexturesShader::Blit(int x1,int y1,int x2,int y2,float z,float u1,float v1,float u2,float v2,Color32 color,const char *tech) const
{
	m_Shader->SetVar(m_NumTexIdx, (float)(1 << (m_NumExposures-1))/255.0f);
	m_Shader->SetVar(m_ExposuresIdx, m_Exposures ,m_NumExposures);
	m_Shader->TWODBlit(SCR_TO_VIEW_X(x1),SCR_TO_VIEW_Y(y1),SCR_TO_VIEW_X(x2),SCR_TO_VIEW_Y(y2),z,u1,v1,u2,v2,color,m_Shader->LookupTechnique(tech));
}


void MergeTexturesShader::Shutdown()
{
	delete m_Shader;
}
