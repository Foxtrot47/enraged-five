#ifdef __DEBUG_SHADER
#include "grcore/setup.h"
#if __WIN32PC
#include "embedded_debugf_win32_30.h"
#elif __XENON
#include "embedded_debugf_fxl_final.h"
#endif
grcTexture* m_DebugfTex;
grmShader* m_DebugfShader;

grcEffectVar m_DebugfTextureIdx;
grcEffectVar m_DebugfValIdx;
void InitDebugShader()
{
	const int texWidth = 128, texHeight = 64;	// TODO: Needs work

	grcImage *image = grcImage::Create(texWidth,texHeight,1,grcImage::DXT1,grcImage::STANDARD,0,0);
	int charWidth = 8;
	int charHeight = 8;
	int charCount = 127;
	int charsPerRow = texWidth / charWidth;
	u8* bits = grcSetup::GetInternalFontBits();
	for (int c=0; c<charCount; c++) {
		int x = (c % charsPerRow) * charWidth;
		int y = (c / charsPerRow) * charHeight;
		const u32 white = ~0U, black = 0;
		for (int row=0; row<charHeight; row++) {
			for (int col=0; col<charWidth; col++) {
				image->SetPixel(x+col,y+row,bits[col>>3] & (128 >> (col&7))? white : black);
			}
			bits += charWidth>>3;
		}
	}
	m_DebugfTex = grcTextureFactory::GetInstance().Create(image);
	m_DebugfShader = grmShaderFactory::GetInstance().Create("debugf");
	m_DebugfShader->Load("embedded:/debugf",0,0);
	m_DebugfTextureIdx = m_DebugfShader->GetEffect().LookupVar("FontTex");
	m_DebugfValIdx = m_DebugfShader->GetEffect().LookupVar("DebugVal0");	
	m_DebugfShader->GetEffect().SetVar(m_DebugfTextureIdx, m_DebugfTex );
	m_DebugfShader->GetEffect().SetVar(m_DebugfValIdx, 1.2345f );
	image->Release();

}
#endif
