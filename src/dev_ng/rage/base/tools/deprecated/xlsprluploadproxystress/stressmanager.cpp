// StressManager
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 

#if __WIN32PC

#include "stressmanager.h"
#include "system/param.h"
#include "system/timer.h"

using namespace rage;

///////////////////////////////////////////////////////////////////////////////
//Cmdline parameters
///////////////////////////////////////////////////////////////////////////////
PARAM(numclients,       "Number of clients to run");
PARAM(secs,             "Seconds to run test. 0 means infinite.");
PARAM(serveraddr,       "Address of server");
PARAM(serverport,       "Bottom of server port range");
PARAM(serverportend,    "Top of server port range");
PARAM(tableid,          "Sake table ID to work on");
PARAM(mindelayms,       "Minimum delay between request chains, in ms");
PARAM(delayrangems,     "Range of additional delay added to minimum, in ms");
PARAM(uploadfilesize,   "Size of test files to upload/download, in bytes");
PARAM(corruptsendchance,"Chance (0-100) of sending a corrupt byte in a request");
PARAM(disconnectrangems,"Time period over which we will randomly disconnect from server during a request; 0 never disconnects");
PARAM(gamername,        "Prefix to use for gamerhandle for each client");
PARAM(baseprofileid,    "Base Gamespy profile ID for clients run by this instance");

///////////////////////////////////////////////////////////////////////////////
//StressManager
///////////////////////////////////////////////////////////////////////////////
StressManager::StressManager()
: m_Initialized(false)
, m_Done(false)
{
}

StressManager::~StressManager()
{
    Shutdown();
}

bool
StressManager::Init()
{
    rtry
    {
        rcheck(!m_Initialized, catchall, rlError(("Already initialized")));

        //Read cmdline params
        int secs = 0;
        PARAM_secs.Get(secs);
        m_Config.m_TestMs = secs * 1000;

        int numClients = 1;
        PARAM_numclients.Get(numClients);

        const char* serverAddrStr = "";
        PARAM_serveraddr.Get(serverAddrStr);
        m_Config.m_ServerAddr = netAddress(net_htonl(net_inet_addr(serverAddrStr)), 0);

        PARAM_serverport.Get(m_Config.m_ServerPort);
        m_Config.m_ServerPortEnd = m_Config.m_ServerPort;
        PARAM_serverportend.Get(m_Config.m_ServerPortEnd);

        m_Config.m_TableId = "";
        PARAM_tableid.Get(m_Config.m_TableId);

        m_Config.m_GamerName = "";
        PARAM_gamername.Get(m_Config.m_GamerName);

        PARAM_mindelayms.Get(m_Config.m_MinDelayMs);
        PARAM_delayrangems.Get(m_Config.m_DelayRangeMs);
        PARAM_uploadfilesize.Get(m_Config.m_UploadFileSize);
        PARAM_corruptsendchance.Get(m_Config.m_CorruptSendChance);
        PARAM_disconnectrangems.Get(m_Config.m_DisconnectRangeMs);
        PARAM_baseprofileid.Get(m_Config.m_BaseProfileId);

        rlDebug2(("Cmdline parameters:"));
        rlDebug2(("secs              = %d", secs));
        rlDebug2(("numclients        = %d", numClients));
        rlDebug2(("serveraddr        = %s", serverAddrStr));
        rlDebug2(("serverport        = %d", m_Config.m_ServerPort));
        rlDebug2(("serverportend     = %d", m_Config.m_ServerPortEnd));
        rlDebug2(("tableid           = %s", m_Config.m_TableId));
        rlDebug2(("mindelayms        = %d", m_Config.m_MinDelayMs));
        rlDebug2(("uploadfilesize    = %d", m_Config.m_UploadFileSize));
        rlDebug2(("delayrangems      = %d", m_Config.m_DelayRangeMs));
        rlDebug2(("corruptsendchance = %d", m_Config.m_CorruptSendChance));
        rlDebug2(("disconnectrangems = %d", m_Config.m_DisconnectRangeMs));
        rlDebug2(("gamername         = %s", m_Config.m_GamerName));
        rlDebug2(("baseprofileid     = %d", m_Config.m_BaseProfileId));

        //Check the parameters
        rcheck(m_Config.m_ServerAddr.GetIp(),
               catchall,
               rlError(("Invalid server IP")));

        rcheck(m_Config.m_ServerPort <= m_Config.m_ServerPortEnd,
               catchall,
               rlError(("Invalid server port range")));

        //Create the client instances
        for(unsigned i = 0; i < (unsigned)numClients; i++)
        {
            StressClient* c  = rage_new StressClient();
            rcheck(c, catchall, rlError(("Failed to allocate client %u", i)));
            
            rcheck(c->Init(this, i), 
                   catchall,
                   rlError(("Failed to init client %u", i)));

            m_ClientList.push_back(c);
        }

        m_Initialized = true;
    }
    rcatchall
    {
        Shutdown();
        return false;
    }
    return true;
}

void
StressManager::Shutdown()
{    
    while(!m_ClientList.empty())
    {
        StressClient* c = *m_ClientList.begin();
        m_ClientList.erase(m_ClientList.begin());
        delete c;
    }

    m_Initialized = false;
}

void
StressManager::Update()
{
    static unsigned s_LastTime = 0;
    unsigned curTime = sysTimer::GetSystemMsTime();

    if(s_LastTime)
    {
        unsigned deltaMs = curTime - s_LastTime;

        if(!m_ClientList.empty())
        {
            ClientList::iterator it = m_ClientList.begin();

            while(it != m_ClientList.end())
            {
                StressClient* c = *it;
                Assert(c);

                c->Update(deltaMs);

                ++it;
            }
        }

        if(m_Config.m_TestMs)
        {
            if(m_Config.m_TestMs <= deltaMs)
            {
                m_Done = true;
            }
            else
            {
                m_Config.m_TestMs -= deltaMs;
            }
        }
    }

    s_LastTime = curTime;
}

#endif

