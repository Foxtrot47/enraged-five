// StressManager
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 

#ifndef STRESSMANAGER_H
#define STRESSMANAGER_H

#if __WIN32PC

#include "stressclient.h"
#include "atl/inlist.h"
#include "net/netaddress.h"
#include "rline/rlupload.h"

#define WIN32_LEAN_AND_MEAN
#include "system/xtl.h" //NOTE(RDT): Use instead of windows.h

namespace rage
{

class StressManager
{
public:
    struct Config
    {
        enum { DEFAULT_SERVER_PORT = 8001 };

        unsigned    m_TestMs;             //Milliseconds to run test. 0 means forever.
        netAddress  m_ServerAddr;
        int         m_ServerPort;
        int         m_ServerPortEnd;
        const char* m_TableId;
        int         m_MinDelayMs;
        int         m_DelayRangeMs;
        int         m_UploadFileSize;
        int         m_CorruptSendChance;
        int         m_DisconnectRangeMs;
        const char* m_GamerName;
        int         m_BaseProfileId;

        Config()
        : m_TestMs(0)
        , m_ServerPort(DEFAULT_SERVER_PORT)
        , m_ServerPortEnd(DEFAULT_SERVER_PORT)
        , m_TableId(0)
        , m_MinDelayMs(0)
        , m_DelayRangeMs(0)
        , m_UploadFileSize(2048)
        , m_CorruptSendChance(0)
        , m_DisconnectRangeMs(0)
        , m_GamerName(0)
        , m_BaseProfileId(1000)
        {
        }
    };

    StressManager();
    ~StressManager();

    bool Init();
    void Shutdown();
    void Update();

    bool IsDone() const { return m_Done; }

    const Config& GetConfig() const { return m_Config; }

private:
    bool m_Initialized;
    bool m_Done;

    Config m_Config;

    typedef inlist<StressClient, &StressClient::m_ListLink> ClientList;
    ClientList m_ClientList;
};

}   //namespace rage

#endif //__WIN32PC

#endif
