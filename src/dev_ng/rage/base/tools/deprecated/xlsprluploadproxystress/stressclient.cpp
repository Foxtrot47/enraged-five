// StressClient
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 

#if __WIN32PC

#include "stressclient.h"
#include "stressmanager.h"
#include "system\nelem.h"

using namespace rage;

u8* g_FileBufs[rlUploadRecord::MAX_FILES] = { 0 };
unsigned g_FileSizes[rlUploadRecord::MAX_FILES] = { 0 };

u8 TEST_CONTENT_ID = 0;

///////////////////////////////////////////////////////////////////////////////
//StressClient
///////////////////////////////////////////////////////////////////////////////
StressClient::StressClient()
: m_Sm(0)
, m_Id(0)
, m_State(STATE_INVALID)
, m_Socket(INVALID_SOCKET)
, m_SocketStatus(0)
, m_SocketExceptionErr(0)
, m_Buf(0)
, m_BufSize(0)
, m_BytesProcessed(0)
, m_DelayMs(0)
, m_NumRecordsRead(0)
, m_FileBuf(0)
, m_FileBufSize(0)
{
}

StressClient::~StressClient()
{
    Reset();

    if(m_FileBuf)
    {
        delete [] m_FileBuf;
        m_FileBuf = 0;
    }
}

bool
StressClient::Init(StressManager* sm,
                   const unsigned id)
{
    m_Sm = sm;
    m_Id = id;

    m_GamerHandle.ResetGamespy(m_Sm->GetConfig().m_BaseProfileId + m_Id); //0 is invalid
    Assert(m_GamerHandle.IsValid());

    if(m_Sm->GetConfig().m_GamerName && strlen(m_Sm->GetConfig().m_GamerName))
    {
        formatf(m_GamerName, sizeof(m_GamerName), "%s_%u", m_Sm->GetConfig().m_GamerName, m_Id);
    }
    else
    {
        formatf(m_GamerName, sizeof(m_GamerName), "%s_%u", getenv("USERNAME"), m_Id);
    }

    Reset();

    return true;
}

void
StressClient::Update(const unsigned deltaMs)
{
    UpdateSocketStatus();

    if(m_SocketStatus & SS_EXCEPTION)
    {
        if(WSAGetLastError() != WSAEWOULDBLOCK)
        {
            scError(("%u: Connection failed (exception err: %d)", m_Id, m_SocketExceptionErr));
            Reset();
            return;        
        }
    }

    switch(m_State)
    {
    case STATE_DELAYING:
        if(deltaMs >= m_DelayMs)
        {
            ConnectToServer();
        }
        else
        {
            m_DelayMs -= deltaMs;
        }
        break;

    case STATE_CONNECTING_TO_SERVER:
        if(deltaMs >= m_DelayMs)
        {
            scError(("%u: Connect failed", m_Id));
            Reset();
        }
        else
        {
            m_DelayMs -= deltaMs;

            if(m_SocketStatus & SS_WRITABLE)
            {
                scDebug(("%u: Connected to server", m_Id));
                
                if(m_Sm->GetConfig().m_DisconnectRangeMs)
                {
                    m_DelayMs = unsigned(rand() % m_Sm->GetConfig().m_DisconnectRangeMs);
                }
                else
                {
                    m_DelayMs = 0;
                }
                
                SendRequest();
            }
        }
        break;

    case STATE_SENDING_REQUEST:
        if(m_DelayMs)
        {
            if(deltaMs >= m_DelayMs)
            {
                scError(("%u: Doing random disconnect", m_Id));
                Reset();
                break;
            }
            m_DelayMs -= deltaMs;
        }
        
        if(UpdateSending())
        {
            ReadReply();
        }
        break;

    case STATE_READING_REPLY:
        if(m_DelayMs)
        {
            if(deltaMs >= m_DelayMs)
            {
                scError(("%u: Doing random disconnect", m_Id));
                Reset();
                break;
            }
            m_DelayMs -= deltaMs;
        }
        
        if(UpdateReading())
        {
            //Don't do a hard reset; we want to retain some state between requests.
            Reset(false);
        }
        break;

    default:
        scError(("%u: Unhandled state %d", m_Id, m_State));
    };
}

void
StressClient::Reset(const bool hardReset)
{
    scDebug(("%u: Resetting (%s)", m_Id, hardReset ? "hard reset" : "end of request"));

    //Close socket
    if(INVALID_SOCKET != m_Socket)
    {
        closesocket(m_Socket);
        m_Socket = INVALID_SOCKET;
    }
    m_SocketStatus = 0;
    m_SocketExceptionErr = 0;

    //Clear send/receive buffer
    if(m_Buf)
    {
        //scDebug(("%u: Deleting m_Buf", m_Id));
        delete [] m_Buf;
        m_Buf = 0;
    }
    m_BufSize = 0;

    if(hardReset)
    {
        //Reset results cache
        m_NumRecordsRead = 0;

        //Reset file upload/download buffer
        if(m_FileBuf)
        {
            delete [] m_FileBuf;
            m_FileBuf = 0;
        }

        AssertVerify(0 != (m_FileBuf = rage_new u8[m_Sm->GetConfig().m_UploadFileSize]));
        m_FileBufSize = (unsigned)m_Sm->GetConfig().m_UploadFileSize;

        for(unsigned i = 0; i < NELEM(g_FileBufs); i++)
        {
            g_FileBufs[i] = m_FileBuf;
            g_FileSizes[i] = m_FileBufSize - 1;
        }

        //Clear request queue
        while(!m_RequestList.empty())
        {
            Request* r = *m_RequestList.begin();
            m_RequestList.erase(m_RequestList.begin());
            delete r;
        }
    }

    AssertVerify(m_FileBuf && m_FileBufSize);

    //Restart state machine
    if(m_RequestList.empty())
    {
        m_DelayMs = m_Sm->GetConfig().m_MinDelayMs + 
                    (m_Sm->GetConfig().m_DelayRangeMs ? (rand() % m_Sm->GetConfig().m_DelayRangeMs) : 0);
    }
    else
    {
        m_DelayMs = 0;
    }
    m_State = STATE_DELAYING;
}

void
StressClient::UpdateSocketStatus()
{
    m_SocketStatus = 0;

    if(INVALID_SOCKET != m_Socket)
    {
        fd_set readfds, writefds, exceptionfds;

        FD_ZERO(&readfds);
        FD_SET(m_Socket, &readfds);

        FD_ZERO(&writefds);
        FD_SET(m_Socket, &writefds);

        FD_ZERO(&exceptionfds);
        FD_SET(m_Socket, &exceptionfds);

        timeval t;
        t.tv_sec  = 0;
        t.tv_usec = 0;

        if(-1 != select(m_Socket+1, &readfds, &writefds, &exceptionfds, &t))
        {
            if(FD_ISSET(m_Socket, &readfds))        m_SocketStatus |= SS_READABLE;
            if(FD_ISSET(m_Socket, &writefds))       m_SocketStatus |= SS_WRITABLE;
            if(FD_ISSET(m_Socket, &exceptionfds))   
            {
                m_SocketStatus |= SS_EXCEPTION;

                int errLen = sizeof(m_SocketExceptionErr);
                getsockopt(m_Socket, SOL_SOCKET, SO_ERROR, (char*)&m_SocketExceptionErr, &errLen);
            }
        }
        else
        {
            scError(("%u: select() error (%d)", m_Id, WSAGetLastError()));
        }
    }
}

void
StressClient::ConnectToServer()
{
    rtry
    {
        rcheck(INVALID_SOCKET != (m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)),
               catchall,
               scError(("%u: Socket creation failed", m_Id)));

        u_long nonblock = 1;
        rcheck(0 == ioctlsocket(m_Socket, FIONBIO, &nonblock),
               catchall,
               scError(("%u: Failed to set socket to non-blocking", m_Id)));

        int portRange = m_Sm->GetConfig().m_ServerPortEnd - m_Sm->GetConfig().m_ServerPort + 1;

        netAddress serverAddr(m_Sm->GetConfig().m_ServerAddr.GetIp(), 
                             (unsigned short)(m_Sm->GetConfig().m_ServerPort + (rand() % portRange)));

        sockaddr_in addr;
        ZeroMemory(&addr, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = htonl(serverAddr.GetIp());
        addr.sin_port   = htons(serverAddr.GetPort());

        int err = connect(m_Socket, (struct sockaddr*)&addr, sizeof(addr));

        if(err)
        {
            rcheck(WSAGetLastError() == WSAEWOULDBLOCK,
                   catchall,
                   scDebug(("%u: connect() call failed", m_Id)));
        }

        scDebug(("%u: Connecting to server at %d.%d.%d.%d:%d", m_Id, NET_ADDR_FOR_PRINTF(serverAddr)));

        m_State = STATE_CONNECTING_TO_SERVER;
        m_DelayMs = 10000;
    }
    rcatchall
    {
        Reset();
    }
}

void
StressClient::SendRequest()
{
    rtry
    {
        //Generate a new request chain if necessary.
        if(m_RequestList.empty())
        {
#if 1

#define CHANCE(x) ((rand() % 100) <= x)

            if(CHANCE(25))
            {
                //Upload a new ride upon entering RMR
                m_RequestList.push_back(rage_new Request(REQUEST_READ_RECORDS_BY_GAMER));
                m_RequestList.push_back(rage_new Request(REQUEST_UPLOAD_FILE));
            }

            if(CHANCE(25))
            {
                //Overwrite your existing ride
                m_RequestList.push_back(rage_new Request(REQUEST_READ_RECORDS_BY_GAMER));
                m_RequestList.push_back(rage_new Request(REQUEST_DOWNLOAD_FILE));
                m_RequestList.push_back(rage_new Request(REQUEST_OVERWRITE_FILE));
            }

            if(CHANCE(25))
            {
                //De-list your ride
                m_RequestList.push_back(rage_new Request(REQUEST_READ_RECORDS_BY_GAMER));
                m_RequestList.push_back(rage_new Request(REQUEST_DELETE_RECORD));
            }

            if(CHANCE(50))
            {
                //Get the leaderboard
                m_RequestList.push_back(rage_new Request(REQUEST_READ_RECORDS_BY_RANK));
                m_RequestList.push_back(rage_new Request(REQUEST_DOWNLOAD_FILE));
            }

            if(CHANCE(50))
            {
                //Rate a random number of rides
                unsigned numRides = rand() % 20;
                for(unsigned i = 0; i < numRides; i++)
                {
                    m_RequestList.push_back(rage_new Request(REQUEST_READ_RANDOM_RECORD));
                    m_RequestList.push_back(rage_new Request(REQUEST_DOWNLOAD_FILE));
                    m_RequestList.push_back(rage_new Request(REQUEST_RATE_RECORD));
                }
            }

            if(CHANCE(25))
            {
                //Overwrite your user data
                m_RequestList.push_back(rage_new Request(REQUEST_READ_RECORDS_BY_GAMER));
                m_RequestList.push_back(rage_new Request(REQUEST_OVERWRITE_USER_DATA));
            }
#endif

            if(m_RequestList.empty())
            {
                //If nothing else, go through set routine
                m_RequestList.push_back(rage_new Request(REQUEST_GET_TABLE_SIZE));
                m_RequestList.push_back(rage_new Request(REQUEST_GET_NUM_OWNED));
                m_RequestList.push_back(rage_new Request(REQUEST_UPLOAD_FILE));
                m_RequestList.push_back(rage_new Request(REQUEST_READ_RECORDS_BY_RANK));
                m_RequestList.push_back(rage_new Request(REQUEST_READ_RANDOM_RECORD));
                m_RequestList.push_back(rage_new Request(REQUEST_READ_RECORDS_BY_GAMER));
                m_RequestList.push_back(rage_new Request(REQUEST_RATE_RECORD));
                m_RequestList.push_back(rage_new Request(REQUEST_DOWNLOAD_FILE));
                m_RequestList.push_back(rage_new Request(REQUEST_OVERWRITE_USER_DATA));
                m_RequestList.push_back(rage_new Request(REQUEST_OVERWRITE_FILE));
                m_RequestList.push_back(rage_new Request(REQUEST_DELETE_RECORD));
            }
        }

        //Send the next request in the queue.
        Assert(!m_RequestList.empty());

        Request* r = *m_RequestList.begin();   
        m_RequestList.erase(m_RequestList.begin());
        
        switch(r->m_Type)
        {
#define REQUEST_CASE(id, name) \
        case id: \
            rcheck(Send##name##Request(), \
                   catchall, \
                   scError(("%u: Failed to send " #name " request", m_Id))); \
                   scDebug(("%u: Send " #name " request", m_Id)); \
            break;

        REQUEST_CASE(REQUEST_GET_TABLE_SIZE, GetTableSize);
        REQUEST_CASE(REQUEST_GET_NUM_OWNED, GetNumOwned);
        REQUEST_CASE(REQUEST_READ_RECORDS_BY_GAMER, ReadRecordsByGamer);
        REQUEST_CASE(REQUEST_READ_RECORDS_BY_RANK, ReadRecordsByRank);
        REQUEST_CASE(REQUEST_READ_RANDOM_RECORD, ReadRandomRecord);
        REQUEST_CASE(REQUEST_RATE_RECORD, RateRecord);
        REQUEST_CASE(REQUEST_DELETE_RECORD, DeleteRecord);
        REQUEST_CASE(REQUEST_UPLOAD_FILE, UploadFile);
        REQUEST_CASE(REQUEST_OVERWRITE_FILE, OverwriteFile);
        REQUEST_CASE(REQUEST_DOWNLOAD_FILE, DownloadFile);
        REQUEST_CASE(REQUEST_OVERWRITE_USER_DATA, OverwriteUserData);

#undef REQUEST_CASE
        }

        delete r;

        //TEST: To force a server crash from allocating too much memory
        //m_BufSize = maxBufSize;

        m_State = STATE_SENDING_REQUEST;
    }
    rcatchall
    {
        Reset();
    }
}

bool
StressClient::UpdateSending()
{
    if(!(m_SocketStatus & SS_WRITABLE))
    {
        return false;
    }

    rtry
    {
        //Have we written the msg size?  If not, continue writing it.
        if(m_BytesProcessed < sizeof(m_BufSize))
        {
            u32 msgSize = htonl(m_BufSize);
            Assert(sizeof(m_BufSize) == sizeof(msgSize));

            int bytesSent = send(m_Socket, 
                                 &(((char*)&msgSize)[m_BytesProcessed]),
                                 sizeof(msgSize) - m_BytesProcessed,
                                 0);

            rcheck(0 < bytesSent,
                   catchall,
                   scError(("%u: Error sending msg size (err=%d, %d of %d bytes)", m_Id, 
                            WSAGetLastError(), m_BytesProcessed, sizeof(msgSize))));
            
            m_BytesProcessed += bytesSent;

            return false; //One send() per call
        }

        Assert(m_Buf);
        Assert(m_BytesProcessed < (m_BufSize + sizeof(m_BufSize)));

        //Continue reading the message
        int offset = m_BytesProcessed - sizeof(m_BufSize);
        int bytesSent = send(m_Socket, 
                             (char*)&m_Buf[offset],
                             m_BufSize - offset,
                             0);

        rcheck(0 < bytesSent,
               catchall,
               scError(("%u: Error sending msg buf (err=%d, %d of %d bytes)", m_Id, 
                        WSAGetLastError(), m_BytesProcessed, m_BufSize)));

        m_BytesProcessed += bytesSent;

        //If done...
        if(m_BytesProcessed == (m_BufSize + sizeof(m_BufSize)))
        {
            scDebug(("%u: Sent msg successfully (%u bytes)", m_Id,
                      m_BytesProcessed - sizeof(m_BufSize)));

            if(m_Buf)
            {
                //scDebug(("%u: Deleting m_Buf", m_Id));
                delete [] m_Buf;
                m_Buf = 0;
            }
            m_BufSize = 0;
            m_BytesProcessed = 0;

            return true;
        }
    }
    rcatchall
    {
        Reset();
    }

    return false;
}

void
StressClient::ReadReply()
{
    Assert(!m_Buf);
    m_State = STATE_READING_REPLY;
}

bool
StressClient::UpdateReading()
{
    if(!(m_SocketStatus & SS_READABLE))
    {
        return false;
    }

    rtry
    {
        //Have we read the msg size?  If not, continue reading it.
        if(m_BytesProcessed < sizeof(m_BufSize))
        {
            int bytesRead = recv(m_Socket, 
                                 &(((char*)&m_BufSize)[m_BytesProcessed]),
                                 sizeof(m_BufSize) - m_BytesProcessed,
                                 0);

            rcheck(0 != bytesRead,
                   catchall,
                   scDebug(("%u: Disconnected while reading msg size", m_Id)));

            rcheck(0 < bytesRead,
                   catchall,
                   scError(("%u: Error reading msg size (err=%d, %d of %d bytes)", m_Id, 
                            WSAGetLastError(), m_BytesProcessed, m_BufSize)))

            m_BytesProcessed += bytesRead;

            if(m_BytesProcessed >= sizeof(m_BufSize))
            {
                m_BufSize = ntohl(m_BufSize);
                //scDebug(("%u: Got msg size: %u", m_Id, m_BufSize));
            }

            return false; //One recv() per call
        }

        Assert(m_BytesProcessed >= sizeof(m_BufSize));

        //Allocate our message buffer first time through
        if(!m_Buf)
        {
            //scDebug(("%u: Allocating m_Buf (%u bytes)", m_Id, m_BufSize));
            rcheck(0 != (m_Buf = rage_new u8[m_BufSize]),
                   catchall,
                   scError(("%u: Failed to allocate message buffer (%d bytes)", m_Id, m_BufSize)));
        }

        //Continue reading the message
        Assert(m_BytesProcessed < (m_BufSize + sizeof(m_BufSize)));

        //Continue reading the message
        int offset = m_BytesProcessed - sizeof(m_BufSize);

        int bytesRead = recv(m_Socket, 
                             (char*)&m_Buf[offset],
                             m_BufSize - offset,
                             0);

        rcheck(0 != bytesRead,
               catchall,
               scDebug(("%u: Disconnected while reading msg buf", m_Id)));

        rcheck(0 < bytesRead,
               catchall,
               scError(("%u: Error reading msg buf (err=%d, %d of %d bytes)", m_Id, 
                        WSAGetLastError(), m_BytesProcessed, m_BufSize)))

        m_BytesProcessed += bytesRead;

        //If done...
        if(m_BytesProcessed == (m_BufSize + sizeof(m_BufSize)))
        {
            //Handle the message
            //scDebug(("%u: Got all of msg", m_Id));
            bool gotReply = ProcessReply(m_Buf, m_BufSize);

            //Reset message reading
            if(m_Buf)
            {
                //scDebug(("%u: Deleting m_Buf", m_Id));
                delete [] m_Buf;
                m_Buf = 0;
            }
            m_BufSize = 0;
            m_BytesProcessed = 0;

            return gotReply;
        }
    }
    rcatchall
    {
        Reset();
    }

    return false;
}

#define EXPORT_REQUEST(maxBufSize) \
    Assert(!m_Buf); \
    if(0 == (m_Buf = rage_new u8[maxBufSize])) \
    { \
       scError(("%u: Failed to allocate send buffer of %d bytes", m_Id, maxBufSize)); \
       return false; \
    } \
    if(!r.Export(m_Buf, maxBufSize, &m_BufSize)) \
    { \
       scError(("%u: Failed to export message into buffer", m_Id)); \
       return false; \
    } \
    if(m_Sm->GetConfig().m_CorruptSendChance && ((rand() % 100) < m_Sm->GetConfig().m_CorruptSendChance))\
    { \
        unsigned corruptByteIndex = unsigned(rand() % m_BufSize); \
        u8 corruptByteValue = u8(rand()); \
        scDebug(("Sending corrupt data (byte %u = %u)", corruptByteIndex, corruptByteValue)); \
        m_Buf[corruptByteIndex] = corruptByteValue; \
    }

bool 
StressClient::SendGetTableSizeRequest()
{
    rlUploadMsgGetTableSizeRequest r;
    r.Reset(m_Sm->GetConfig().m_TableId, 0, 0);
    EXPORT_REQUEST(64);
    return true;
}

bool 
StressClient::SendGetNumOwnedRequest() 
{ 
    rlUploadMsgGetNumOwnedRequest r;
    r.Reset(m_Sm->GetConfig().m_TableId, m_GamerHandle);
    EXPORT_REQUEST(128);
    return true;
}

bool 
StressClient::SendReadRecordsByGamerRequest() 
{ 
    rlUploadMsgReadRecordsByGamerRequest r;
    r.Reset(m_Sm->GetConfig().m_TableId, m_GamerHandle, MAX_RECORDS);
    EXPORT_REQUEST(256);
    return true;
}

bool 
StressClient::SendReadRecordsByRankRequest() 
{ 
    rlUploadMsgReadRecordsByRankRequest r;
    r.Reset(m_Sm->GetConfig().m_TableId, 1, 1, MAX_RECORDS);
    EXPORT_REQUEST(256);
    return true;
}

bool 
StressClient::SendReadRandomRecordRequest() 
{ 
    rlUploadMsgReadRandomRecordRequest r;
    r.Reset(m_Sm->GetConfig().m_TableId, m_GamerHandle, 0, 0, 0);
    EXPORT_REQUEST(256);
    return true;
}

bool 
StressClient::SendRateRecordRequest() 
{ 
    if(m_NumRecordsRead)
    {
        rlUploadMsgRateRecordRequest r;
        r.Reset(m_Sm->GetConfig().m_TableId, m_Records[0], u8(rand() % 100));
        EXPORT_REQUEST(256);
        return true;
    }
    return false;
}

bool 
StressClient::SendDeleteRecordRequest() 
{ 
    if(m_NumRecordsRead)
    {
        rlUploadMsgDeleteRecordRequest r;
        r.Reset(m_Sm->GetConfig().m_TableId, m_Records[0].GetRecordId());
        EXPORT_REQUEST(128);
        return true;
    }
    return false;
}

bool 
StressClient::SendUploadFileRequest() 
{ 
    rlUploadMsgUploadFileRequest r;
    if(r.Reset(m_Sm->GetConfig().m_TableId,
               1, //Record limit
               m_GamerHandle,         
               m_GamerName,
               NELEM(g_FileBufs),
               g_FileBufs,
               g_FileSizes,
               m_UserDataBuf,
               sizeof(m_UserDataBuf),
               TEST_CONTENT_ID))
    {
        EXPORT_REQUEST(256 + (m_FileBufSize * NELEM(g_FileBufs)) - 1);
        return true;
    }
    return false; 
}

bool 
StressClient::SendOverwriteFileRequest() 
{ 
    rlUploadMsgOverwriteFileRequest r;
    if(r.Reset(m_Sm->GetConfig().m_TableId,
               m_Records[0],
               NELEM(g_FileBufs),
               g_FileBufs,
               g_FileSizes,
               m_UserDataBuf,
               sizeof(m_UserDataBuf),
               TEST_CONTENT_ID))
    {
        EXPORT_REQUEST(256 + (m_FileBufSize * NELEM(g_FileBufs)) - 1);
        return true;
    }
    return false; 
}

bool 
StressClient::SendDownloadFileRequest() 
{ 
    if(m_Records[0].GetNumFiles())
    {
        rlUploadMsgDownloadFileRequest r;
        r.Reset(m_Records[0].GetFileId(0), m_FileBufSize);
        EXPORT_REQUEST(256);
        return true;
    }
    return false;
}

bool 
StressClient::SendOverwriteUserDataRequest() 
{ 
    rlUploadMsgOverwriteUserDataRequest r;
    if(r.Reset(m_Sm->GetConfig().m_TableId,
               m_Records[0],
               m_UserDataBuf,
               sizeof(m_UserDataBuf)))
    {
        EXPORT_REQUEST(256 + sizeof(m_UserDataBuf));
        return true;
    }
    return false; 
}

bool
StressClient::ProcessReply(const u8* data, const unsigned size)
{
    unsigned msgId;
    if(!netMessage::GetId(&msgId, data, size))
    {
        scError(("%u: Failed to get reply message ID", m_Id));
        return false;
    }

#define PROCESS_REPLY(name) \
    if(msgId == rlUploadMsg##name##Reply::MSG_ID()) \
    { \
        scDebug(("%u: Received " #name " reply (%u bytes)", m_Id, size)); \
        ProcessReply##name##(data, size); \
        return true; \
    }

    PROCESS_REPLY(GetTableSize);
    PROCESS_REPLY(GetNumOwned);
    PROCESS_REPLY(ReadRecordsByRank);
    PROCESS_REPLY(ReadRecordsByGamer);
    PROCESS_REPLY(ReadRandomRecord);
    PROCESS_REPLY(RateRecord);
    PROCESS_REPLY(DeleteRecord);
    PROCESS_REPLY(UploadFile);
    PROCESS_REPLY(OverwriteFile);
    PROCESS_REPLY(DownloadFile);
    PROCESS_REPLY(OverwriteUserData);

#undef PROCESS_REPLY

    return false;
}

#define IMPORT_REPLY(r, data, size) \
    if(!r##.Import(data, size)) \
    { \
        scError(("%u: Error importing reply", m_Id)); \
        return; \
    } \
    if(!r##.m_Success) \
    { \
        scError(("%u: Request failed (%d)", m_Id, r##.m_ResultCode)); \
        return; \
    }

void 
StressClient::ProcessReplyGetTableSize(const u8* data, const unsigned size)
{
    rlUploadMsgGetTableSizeReply r;
    IMPORT_REPLY(r, data, size);
    scDebug(("%u: Table size = %u", m_Id, r.m_NumRecords));
}

void 
StressClient::ProcessReplyGetNumOwned(const u8* data, const unsigned size)
{
    rlUploadMsgGetNumOwnedReply r;
    IMPORT_REPLY(r, data, size);
    scDebug(("%u: Num owned = %u", m_Id, r.m_NumOwned));
}

void 
StressClient::ProcessReplyReadRecordsByGamer(const u8* data, const unsigned size)
{
    rlUploadMsgReadRecordsByGamerReply r;
    r.Reset(false, -1, MAX_RECORDS, &m_NumRecordsRead, m_Records);
    IMPORT_REPLY(r, data, size);
    scDebug(("%u: Num records read = %u", m_Id, m_NumRecordsRead));
}

void 
StressClient::ProcessReplyReadRecordsByRank(const u8* data, const unsigned size)
{
    rlUploadMsgReadRecordsByRankReply r;
    r.Reset(false, -1, MAX_RECORDS, &m_NumRecordsRead, m_Records);
    IMPORT_REPLY(r, data, size);
    scDebug(("%u: Num records read = %u", m_Id, m_NumRecordsRead));
}

void 
StressClient::ProcessReplyReadRandomRecord(const u8* data, const unsigned size)
{
    rlUploadMsgReadRandomRecordReply r;
    IMPORT_REPLY(r, data, size);
    scDebug(("%u: Num records read = %u", m_Id, r.m_NumRecordsRead));
    m_NumRecordsRead = r.m_NumRecordsRead;
    if(m_NumRecordsRead)
    {
        m_Records[0] = r.m_Record;
    }
}

void 
StressClient::ProcessReplyRateRecord(const u8* data, const unsigned size)
{
    rlUploadMsgRateRecordReply r;
    IMPORT_REPLY(r, data, size);
    scDebug(("%u: Num ratings = %u   Avg rating = %0.1f", m_Id, r.m_NumRatings, r.m_AvgRating));
}

void 
StressClient::ProcessReplyDeleteRecord(const u8* data, const unsigned size)
{
    rlUploadMsgDeleteRecordReply r;
    IMPORT_REPLY(r, data, size);
    scDebug(("%u: Record deleted", m_Id));
}

void 
StressClient::ProcessReplyUploadFile(const u8* data, const unsigned size)
{
    rlUploadMsgUploadFileReply r;
    IMPORT_REPLY(r, data, size);
    m_Records[0] = r.m_Record;
    scDebug(("%u: Record created = %d", m_Id, r.m_Record.GetRecordId()));
}

void 
StressClient::ProcessReplyOverwriteFile(const u8* data, const unsigned size)
{
    rlUploadMsgOverwriteFileReply r;
    IMPORT_REPLY(r, data, size);
    m_Records[0] = r.m_NewRecord;
    scDebug(("%u: Record created = %d", m_Id, r.m_NewRecord.GetRecordId()));
}

void 
StressClient::ProcessReplyDownloadFile(const u8* data, const unsigned size)
{
    rlUploadMsgDownloadFileReply r(m_FileBuf, m_FileBufSize);
    IMPORT_REPLY(r, data, size);
    scDebug(("%u: Bytes downloaded = %d", m_Id, r.m_NumBytesDownloaded));
}

void 
StressClient::ProcessReplyOverwriteUserData(const u8* data, const unsigned size)
{
    rlUploadMsgOverwriteUserDataReply r;
    IMPORT_REPLY(r, data, size);
}

#endif

