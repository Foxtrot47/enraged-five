// StressClient
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 

#ifndef STRESSCLIENT_H
#define STRESSCLIENT_H

#if __WIN32PC

#define WIN32_LEAN_AND_MEAN

#include "atl/inlist.h"
#include "rline/rlupload.h"
#include "system/xtl.h" //NOTE(RDT): Use instead of windows.h
#include <winsock.h>

namespace rage
{

class StressManager;

class StressClient
{
public:
    StressClient();
    ~StressClient();

    bool Init(StressManager* sm,
              const unsigned id);

    void Update(const unsigned deltaMs);

    inlist_node<StressClient> m_ListLink;

private:
    void Reset(const bool hardReset = true);
    void UpdateSocketStatus();
    void ConnectToServer();
    void SendRequest();
    bool UpdateSending();
    void ReadReply();
    bool UpdateReading();

    bool SendGetTableSizeRequest();
    bool SendGetNumOwnedRequest();
    bool SendReadRecordsByGamerRequest();
    bool SendReadRecordsByRankRequest();
    bool SendReadRandomRecordRequest();
    bool SendRateRecordRequest();
    bool SendDeleteRecordRequest();
    bool SendUploadFileRequest();
    bool SendOverwriteFileRequest();
    bool SendDownloadFileRequest();
    bool SendOverwriteUserDataRequest();

    bool ProcessReply(const u8* data, const unsigned size);

    void ProcessReplyGetTableSize(const u8* data, const unsigned size);
    void ProcessReplyGetNumOwned(const u8* data, const unsigned size);
    void ProcessReplyReadRecordsByGamer(const u8* data, const unsigned size);
    void ProcessReplyReadRecordsByRank(const u8* data, const unsigned size);
    void ProcessReplyReadRandomRecord(const u8* data, const unsigned size);
    void ProcessReplyRateRecord(const u8* data, const unsigned size);
    void ProcessReplyDeleteRecord(const u8* data, const unsigned size);
    void ProcessReplyUploadFile(const u8* data, const unsigned size);
    void ProcessReplyOverwriteFile(const u8* data, const unsigned size);
    void ProcessReplyDownloadFile(const u8* data, const unsigned size);
    void ProcessReplyOverwriteUserData(const u8* data, const unsigned size);

private:
    StressManager* m_Sm;
    unsigned m_Id;

    enum eState
    {
        STATE_INVALID = -1,
        STATE_DELAYING,
        STATE_CONNECTING_TO_SERVER,
        STATE_SENDING_REQUEST,
        STATE_READING_REPLY
    };

    int m_State;

    SOCKET m_Socket;
    
    enum eSocketStatusFlags
    {
        SS_READABLE     = 1 << 0,
        SS_WRITABLE     = 1 << 1,
        SS_EXCEPTION    = 1 << 2,
    };
    u8 m_SocketStatus;
    int m_SocketExceptionErr; //Error code set on exception

    u8* m_Buf;
    unsigned m_BufSize;
    unsigned m_BytesProcessed;
    unsigned m_DelayMs;

    rlGamerHandle m_GamerHandle;
    char m_GamerName[RL_MAX_NAME_CHARS];
    
    u8 m_UserDataBuf[RLUPLOAD_MAX_USER_DATA_SIZE]; 

    //enum { FILE_BUF_SIZE = 2048 };
    //u8 m_FileBuf[FILE_BUF_SIZE]; 
    u8* m_FileBuf;
    unsigned m_FileBufSize;

    unsigned m_NumRecordsRead;

    enum { MAX_RECORDS = 10 };
    rlUploadRecord m_Records[MAX_RECORDS];

    enum eRequestType
    {
        REQUEST_INVALID = -1,
        REQUEST_GET_TABLE_SIZE,
        REQUEST_GET_NUM_OWNED,
        REQUEST_READ_RECORDS_BY_GAMER,
        REQUEST_READ_RECORDS_BY_RANK,
        REQUEST_READ_RANDOM_RECORD,
        REQUEST_RATE_RECORD,
        REQUEST_DELETE_RECORD,
        REQUEST_UPLOAD_FILE,
        REQUEST_OVERWRITE_FILE,
        REQUEST_DOWNLOAD_FILE,
        REQUEST_OVERWRITE_USER_DATA,
        NUM_REQUESTS
    };

    struct Request
    {
        Request(const int type = REQUEST_INVALID) { m_Type = type; }
        int m_Type;

        inlist_node<Request> m_ListLink;
    };
    typedef inlist<Request, &Request::m_ListLink> RequestList;

    RequestList m_RequestList;
};

}   //namespace rage

//#if !__NO_OUTPUT
//#define scDebug(a)      SpewDebug(1, 1, "sc") a
//#define scWarning(a)    SpewWarning("sc") a
//#define scError(a)      SpewError("sc") a
//
//#else
#define scDebug(a)
#define scWarning(a)
#define scError(a)

//#endif  //__NO_OUTPUT

#endif //__WIN32PC

#endif
