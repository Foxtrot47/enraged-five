set ARCHIVE=xlsprluploadproxystress

set FILES=stressmanager stressclient

REM set HEADONLY=servicemsg

REM set CUSTOM=servicemsg.rc servicemsg.mc

set LIBS=%RAGE_CORE_LIBS% %RAGE_NET_LIBS%

set LIBS_xlsprluploadproxystress=%LIBS%

set TESTERS=xlsprluploadproxystress

set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\src
