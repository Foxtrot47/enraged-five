// 
// xlsprluploadproxystress/xlsprluploadproxystress.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 
#include "system/main.h"
#include "stressmanager.h"

using namespace rage;

int Main()
{
    StressManager sm;

    AssertVerify(sm.Init());

    while(!sm.IsDone())
    {
        sm.Update();
        sysIpcSleep(1);
    }

    sm.Shutdown();

    return 0;
}
