using System;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;

namespace docparseparams
{
	class ParamInfo : IComparable
	{
		public string	ModuleName;
		public string	FileName;
		public string	ParamName;
		public string	Desc;
		public int		NumArguments;

		/// <summary>
		/// IComparable.CompareTo implementation.
		/// </summary>
		public int CompareTo(object obj) 
		{
			if(obj is ParamInfo) 
			{
				ParamInfo param = (ParamInfo) obj;

				return ParamName.CompareTo(param.ParamName);
			}
        
			throw new ArgumentException("object is not a ParamInfo");    
		}

	};

	/// <summary>
	/// Summary description for docParseParams.
	/// </summary>
	class docParseParams
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
        [STAThread]
        static void Main( string[] args )
        {

            if ( args.Length < 1 )
            {
                Console.Error.WriteLine( "usage: docparseparams.exe <file with grepped PARAMs>" );
                Console.Error.WriteLine( "param file should be generated like this: \n" +
                                  "\tgrep \"PARAM(\" */*.cpp */*.c | grep -v UNUSED_PARAM > params" );
                return;
            }

            // Create an array representing the files in the current directory.
            int longestName = 0;
            int longestDesc = 0;
            int longestFilename = 0;

            if ( File.Exists( args[0] ) )
            {
                ArrayList allParams = new ArrayList();

                using ( StreamReader sr = File.OpenText( args[0] ) )
                {
                    while ( sr.Peek() != -1 )
                    {
                        string line = sr.ReadLine();

                        // find module name:
                        string splitArg = ":";
                        string[] colonSplit = line.Split( splitArg.ToCharArray() );
                        if ( colonSplit.Length <= 1 )
                        {
                            Console.Error.WriteLine( "Warning: couldn't find module name in: '" + line + "'" );
                            continue;
                        }

                        ParamInfo param = new ParamInfo();

                        splitArg = "/";
                        string[] moduleSplit = colonSplit[0].Split( splitArg.ToCharArray() );

                        if ( moduleSplit.Length <= 1 )
                        {
                            param.ModuleName = null;
                            param.FileName = colonSplit[0];
                        }
                        else
                        {
                            param.ModuleName = moduleSplit[0];
                            param.FileName = moduleSplit[1];
                        }

                        // get param name:
                        string paramLine = colonSplit[1].Trim();

                        bool hasMoreThanOneArgument = false;
                        int argCheck = 1;
                        string substring = null;
                        
                        if ( paramLine.StartsWith( "PARAM" ) )
                        {
                            substring = paramLine.Substring( 5 );
                        }
                        else if ( paramLine.StartsWith( "REQ_PARAM" ) )
                        {
                            substring = paramLine.Substring( 9 );
                        }
                        else if ( paramLine.StartsWith( "FPARAM" ) )
                        {
                            substring = paramLine.Substring( 6 );

                            hasMoreThanOneArgument = true;
                            argCheck++;
                        }
                        else if ( paramLine.StartsWith( "XPARAM" ) )
                        {
                            continue;
                        }
                        else
                        {
                            Console.Error.WriteLine( "Warning: couldn't find parameter name in: '" + line + "'" );
                            continue;
                        }

                        // get rid of whitespace and "(":
                        int offset = 0;
                        foreach ( char c in substring.ToCharArray() )
                        {
                            offset++;
                            if ( c == '(' )
                                break;
                        }
                        substring = substring.Substring( offset );

                        splitArg = ",";
                        string[] paramNameAndDescription = substring.Split( splitArg.ToCharArray() );
                        if ( paramNameAndDescription.Length <= argCheck )
                        {
                            Console.Error.WriteLine( "Warning: couldn't find parameter name in: '" + line + "'" );
                            continue;
                        }

                        int startIndex = 0;
                        if ( hasMoreThanOneArgument == true )
                        {
                            param.NumArguments = Convert.ToInt32( paramNameAndDescription[startIndex++], 10 );
                        }
                        else
                            param.NumArguments = 0;

                        param.ParamName = paramNameAndDescription[startIndex++].Trim();
                        if ( hasMoreThanOneArgument == true )
                            param.Desc = substring.Substring( paramNameAndDescription[0].Length + paramNameAndDescription[1].Length + 2 ); //+2 for ","
                        else
                            param.Desc = substring.Substring( paramNameAndDescription[0].Length + 1 ); //+1 for ","
                        string trimArg = ",";
                        param.Desc = param.Desc.Trim( trimArg.ToCharArray() );
                        trimArg = ";";
                        param.Desc = param.Desc.Trim( trimArg.ToCharArray() );
                        trimArg = ")";
                        param.Desc = param.Desc.Trim( trimArg.ToCharArray() );
                        param.Desc = param.Desc.Trim();
                        trimArg = "\"";
                        param.Desc = param.Desc.Trim( trimArg.ToCharArray() );

                        Regex regExParamModule = new Regex( @"\A\s*\[\S+\]\s*" );

                        // get rid of module name if it's there:
                        if ( regExParamModule.Match( param.Desc ).Success )
                            param.Desc = regExParamModule.Replace( param.Desc, "" );

                        //Console.WriteLine("\"" + param.ModuleName + "\",\"" + param.FileName +
                        //				  "\", \"" + param.ParamName + "\",\"" + param.Desc +
                        //				  "\",\"" + param.NumArguments + "\"");

                        // find the longest names:
                        if ( longestName < param.ParamName.Length )
                            longestName = param.ParamName.Length;

                        if ( longestDesc < param.Desc.Length )
                            longestDesc = param.Desc.Length;

                        if ( longestFilename < param.FileName.Length )
                            longestFilename = param.FileName.Length;

                        allParams.Add( param );
                    }
                }

                if ( allParams.Count >= 1 )
                {
                    // done reading, output table:
                    allParams.Sort();

                    Console.WriteLine( "@@Command Line Options" );
                    Console.WriteLine( "<TABLE>" );

                    // write header:
                    int nameLength = WriteTableString( "Parameter Name", longestName );
                    int filenameLength = WriteTableString( "In File", longestFilename );
                    int descLength = WriteTableString( "Description", longestDesc );
                    Console.WriteLine();

                    // write separator:
                    WriteSeparator( nameLength );
                    WriteSeparator( filenameLength );
                    WriteSeparator( descLength );
                    Console.WriteLine();

                    // write params:
                    foreach ( ParamInfo param in allParams )
                    {
                        WriteTableString( param.ParamName, nameLength );
                        WriteTableString( param.FileName, filenameLength );
                        WriteTableString( param.Desc, descLength );
                        Console.WriteLine();
                    }

                    Console.WriteLine( "</TABLE>" );
                }
            }
        }

		static int WriteTableString(string header,int longestLength)
		{
			int numWritten=header.Length;
			Console.Write(header);
			for (int i=0;i<longestLength-header.Length;i++)
			{
				Console.Write(" ");
				numWritten++;
			}
		
			Console.Write("  ");
			//numWritten+=2;

			return numWritten;
		}

		static void WriteSeparator(int length)
		{
			for (int i=0;i<length;i++)
			{
				Console.Write("-");
			}

			Console.Write("  ");
		}
	}
}
