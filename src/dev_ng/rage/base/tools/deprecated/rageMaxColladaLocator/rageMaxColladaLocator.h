/**********************************************************************
 *<
	FILE: rageMaxColladaLocator.h

	DESCRIPTION:	Includes for Plugins

	CREATED BY:

	HISTORY:

 *>	Copyright (c) 2003, All Rights Reserved.
 **********************************************************************/

#ifndef __rageMaxColladaLocator__H
#define __rageMaxColladaLocator__H

#pragma comment(lib, "OpenGL32.lib")
#ifdef _DEBUG
#pragma comment(lib, "../3rdParty/COLLADA_DOM/lib-dbg/1.4/libcollada_dae_d.lib")
#pragma comment(lib, "../3rdParty/COLLADA_DOM/lib-dbg/1.4/libcollada_dom_d.lib")
#pragma comment(lib, "../3rdParty/COLLADA_DOM/lib-dbg/1.4/libcollada_LIBXMLPlugin_d.lib")
#pragma comment(lib, "../3rdParty/COLLADA_DOM/lib-dbg/1.4/libcollada_stdErrPlugin_d.lib")
#pragma comment(lib, "../3rdParty/COLLADA_DOM/lib-dbg/1.4/libcollada_STLDatabase_d.lib")
#else
#pragma comment(lib, "../3rdParty/COLLADA_DOM/lib/1.4/libcollada_dae.lib")
#pragma comment(lib, "../3rdParty/COLLADA_DOM/lib/1.4/libcollada_dom.lib")
#pragma comment(lib, "../3rdParty/COLLADA_DOM/lib/1.4/libcollada_LIBXMLPlugin.lib")
#pragma comment(lib, "../3rdParty/COLLADA_DOM/lib/1.4/libcollada_stdErrPlugin.lib")
#pragma comment(lib, "../3rdParty/COLLADA_DOM/lib/1.4/libcollada_STLDatabase.lib")
#endif
// #pragma comment(lib, "../3rdParty/COLLADA_DOM/external-libs/libxml2/win32/lib/libxml2.lib")
#pragma comment(lib, "../3rdParty/COLLADA_DOM/external-libs/libxml2/win32/lib/libxml2_a.lib")
#pragma comment(lib, "../3rdParty/COLLADA_DOM/external-libs/zlib/lib/zdll.lib")
#pragma comment(lib, "../3rdParty/libxml/lib/iconv_a.lib")
#pragma comment(lib, "../3rdParty/Devil/lib/DevIL.lib")
#pragma comment(lib, "../3rdParty/Devil/lib/ILUT.lib")
#pragma comment(lib, "../3rdParty/Devil/lib/ILU.lib")

#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
//SIMPLE TYPE


extern TCHAR *GetString(int id);
extern HINSTANCE hInstance;

#define rageMaxColladaLocator_CLASS_ID	Class_ID(0x4961fa93, 0x7ce5ace3)
#define PBLOCK_REF	0

class rageMaxColladaLocator : public HelperObject {
public:
	// Class vars
	static IObjParam *iObjParams;

	// Parameter block
	IParamBlock2	*pblock;	//ref 0

	// Loading/Saving
	IOResult Load(ILoad *iload) {return IO_OK;}
	IOResult Save(ISave *isave) {return IO_OK;}

	//From Animatable
	Class_ID ClassID() {return rageMaxColladaLocator_CLASS_ID;}		
	SClass_ID SuperClassID() { return HELPER_CLASS_ID; }
	void GetClassName(TSTR& s) {s = GetString(IDS_CLASS_NAME);}

	RefTargetHandle Clone( RemapDir &remap );
	RefResult NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
		PartID& partID,  RefMessage message);

	int NumSubs() { return 1; }

	int	NumParamBlocks() { return 1; }					// return number of ParamBlocks in this instance
	IParamBlock2* GetParamBlock(int i) { return pblock; } // return i'th ParamBlock
	IParamBlock2* GetParamBlockByID(BlockID id) { return (pblock->ID() == id) ? pblock : NULL; } // return id'd ParamBlock

	void DeleteThis() { delete this; }		

	void SetSpecifyLength(bool onOff) {m_bSpecifyLength = onOff;}
	void SetColladaFilename(const char* pcColladaFilename) {strcpy(m_acColladaFilename, pcColladaFilename);}

	//Constructor/Destructor
	rageMaxColladaLocator();
	~rageMaxColladaLocator();

	// Pure virtuals from above
	// From BaseObject
	int HitTest(TimeValue t, INode* inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt);
	//void Snap(TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt);
	int Display(TimeValue t, INode* inode, ViewExp *vpt, int flags);
	CreateMouseCallBack* GetCreateMouseCallBack();
	void BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev);
	void EndEditParams( IObjParam *ip, ULONG flags,Animatable *next);
	TCHAR *GetObjectName() { return GetString(IDS_DB_TAPE); }

	// From Object
//	virtual int IsRenderable()=0;  // is this a renderable object?
	void InitNodeName(TSTR& s) { s = GetString(IDS_DB_TAPE); }
	ObjectState Eval(TimeValue time);
	Interval ObjectValidity();
	Interval ObjectValidity(TimeValue time);

	// From GeomObject
	int IntersectRay(TimeValue t, Ray& r, float& at);
	void GetWorldBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box );
	void GetLocalBoundBox(TimeValue t, INode *mat, ViewExp *vpt, Box3& box );
	void GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel );

	void BuildMeshFromColladaFile(const char* pcColladaPathAndFilename);

private:
	void GetMat(TimeValue t, INode* inod, ViewExp *vpt, Matrix3& mat);

	void BuildMesh();

	// Class vars
	static Mesh m_obMesh;
	static bool m_bMeshBuilt;
	bool m_bSpecifyLength;
	char	m_acColladaFilename[512];
	bool m_bEditting;
	Mesh m_obMyMesh;
	static HWND m_hMaxColladaLocatorParams;

	friend INT_PTR CALLBACK rageMaxColladaLocatorParamDialogProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam );
};


#endif
