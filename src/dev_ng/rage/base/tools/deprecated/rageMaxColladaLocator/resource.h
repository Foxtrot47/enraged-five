//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by rageMaxColladaLocator.rc
//
#define IDS_LIBDESCRIPTION              1
#define IDS_CATEGORY                    2
#define IDS_CLASS_NAME                  3
#define IDS_PARAMS                      4
#define IDS_SPIN                        5
#define IDD_PANEL                       101
#define IDC_CLOSEBUTTON                 1000
#define IDC_DOSTUFF                     1000
#define IDC_SPEC_LEN                    1000
#define IDC_EDIT1                       1002
#define IDC_COLLADAFILENAME_EDIT        1002
#define IDC_COLOR                       1456
#define IDC_EDIT                        1490
#define IDC_SPIN                        1496
#define IDS_DB_TAPE_LENGTH              20326
#define IDS_RB_PARAMETERS               30028
#define IDS_DB_TAPE                     30075

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
