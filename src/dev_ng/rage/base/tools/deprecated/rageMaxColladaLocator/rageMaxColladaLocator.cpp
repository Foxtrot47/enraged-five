/**********************************************************************
 *<
	FILE: rageMaxColladaLocator.cpp

	DESCRIPTION:	Appwizard generated plugin

	CREATED BY: 

	HISTORY: 

 *>	Copyright (c) 2003, All Rights Reserved.
 **********************************************************************/
#include "3dsmaxport.h"

#include "rageMaxColladaLocator.h"
#include "rageMaxColladaLocatorCreateCallBack.h"
#include "rageMaxColladaLocatorClassDesc.h"
#include "../rageCollada/rageRolladaScene.h"

using namespace rage;

// class variable for measuring tape class.
Mesh rageMaxColladaLocator::m_obMesh;
bool rageMaxColladaLocator::m_bMeshBuilt=false;
IObjParam *rageMaxColladaLocator::iObjParams;
HWND rageMaxColladaLocator::m_hMaxColladaLocatorParams = NULL;

static rageMaxColladaLocatorClassDesc rageMaxColladaLocatorDesc;
ClassDesc2* GetrageMaxColladaLocatorDesc() { return &rageMaxColladaLocatorDesc; }


static rageMaxColladaLocatorCreateCallBack rageMaxColladaLocatorCreateCB;

CreateMouseCallBack* rageMaxColladaLocator::GetCreateMouseCallBack() 
{
	rageMaxColladaLocatorCreateCB.SetObj(this);
	return(&rageMaxColladaLocatorCreateCB);
}

//
// Reference Managment:
//

// This is only called if the object MAKES references to other things.
RefResult rageMaxColladaLocator::NotifyRefChanged(Interval changeInt, RefTargetHandle hTarget, 
										   PartID& partID, RefMessage message ) 
{
	switch (message) 
	{
	case REFMSG_CHANGE:
		{
			if (iObjParams)
			{
//				UpdateUI(iObjParams->GetTime());
			}
			break;
		}

	case REFMSG_GET_PARAM_DIM: 
		{
			GetParamDim *gpd = (GetParamDim*)partID;
			switch (gpd->index) 
			{
			case 0:
				{
					gpd->dim = stdWorldDim;
					break;            
				}
			}
			return REF_STOP; 
	   }
	case REFMSG_GET_PARAM_NAME: 
		{
			GetParamName *gpn = (GetParamName*)partID;
			switch (gpn->index) 
			{
			case 0:
				{
					gpn->name = TSTR(GetString(IDS_DB_TAPE_LENGTH));
					break;                                    
				}
			}
			return REF_STOP; 
		}
	}
	return(REF_SUCCEED);
}

rageMaxColladaLocator::rageMaxColladaLocator() 
: HelperObject() 
{
	ParamBlockDesc desc[] = {
		{ TYPE_FLOAT, NULL, TRUE } };

		// RB: Need to make a reference to the parameter block!
		pblock = NULL;
		ReplaceReference( 0, CreateParameterBlock( desc, 1 ));

		//enable = 0;
		//m_bEditting = 0;
		//lastDist = 0.0f;
		//SetLength( TimeValue(0), dlgLength );
		//m_bSpecifyLength = dlgSpecLen;
		for(int i=0; i<512; i++) m_acColladaFilename[i] = '\0';
		strcpy(m_acColladaFilename, "Hamsters will rule the world");
		BuildMesh();
		//dirPt.x = dirPt.y = dirPt.z = 0.0f; 

		BuildMeshFromColladaFile("t:/rage/assets/geos/GEOInstanceProxies/ElmStocky_Fall_1.dae");
}

rageMaxColladaLocator::~rageMaxColladaLocator()
{
	DeleteAllRefsFromMe();
	pblock = NULL;
}



void rageMaxColladaLocator::BuildMesh()
{
	if(m_bMeshBuilt)
	{
		return;
	}
	int nverts = 5;
	int nfaces = 6;
	m_obMesh.setNumVerts(nverts);
	m_obMesh.setNumFaces(nfaces);

	float d =  5.0f;
	float h = 10.0f;

	m_obMesh.setVert(0, Point3(  -d, -d, 0.0f ));
	m_obMesh.setVert(1, Point3(   d, -d, 0.0f ));
	m_obMesh.setVert(2, Point3(   d,  d, 0.0f ));
	m_obMesh.setVert(3, Point3(  -d,  d, 0.0f ));
	m_obMesh.setVert(4, Point3(0.0f,0.0f, -h  ));
	m_obMesh.faces[0].setVerts(0, 3, 1);
	m_obMesh.faces[0].setEdgeVisFlags(1,1,0);
	m_obMesh.faces[1].setVerts(3, 2, 1);
	m_obMesh.faces[1].setEdgeVisFlags(1,1,0);
	m_obMesh.faces[2].setVerts(0, 1, 4);
	m_obMesh.faces[2].setEdgeVisFlags(1,1,1);
	m_obMesh.faces[3].setVerts(1, 2, 4);
	m_obMesh.faces[3].setEdgeVisFlags(1,1,1);
	m_obMesh.faces[4].setVerts(2, 3, 4);
	m_obMesh.faces[4].setEdgeVisFlags(1,1,1);
	m_obMesh.faces[5].setVerts(3, 0, 4);
	m_obMesh.faces[5].setEdgeVisFlags(1,1,1);
#if 0
	// whoops- rotate 180 about x to get it facing the right way
	Matrix3 mat;
	mat.IdentityMatrix();
	mat.RotateX(DegToRad(180.0));
	for (int i=0; i<nverts; i++)
		m_obMesh.getVert(i) = mat*mesh.getVert(i);
#endif
	m_obMesh.buildNormals();
	m_obMesh.EnableEdgeList(1);
	m_bMeshBuilt = true;
}

ObjectState rageMaxColladaLocator::Eval(TimeValue time)
{
	return ObjectState(this);
}

RefTargetHandle rageMaxColladaLocator::Clone(RemapDir& remap) 
{
	rageMaxColladaLocator* newob = new rageMaxColladaLocator();
	newob->ReplaceReference(0,remap.CloneRef(pblock));
	newob->m_bSpecifyLength = m_bSpecifyLength;
//	newob->ivalid.SetEmpty();
//	newob->enable = enable;
	newob->SetColladaFilename(m_acColladaFilename);
	BaseClone(this, newob, remap);
	return(newob);
}

void rageMaxColladaLocator::GetMat(TimeValue t, INode* inode, ViewExp* vpt, Matrix3& tm) 
{
	tm = inode->GetObjectTM(t);
	tm.NoScale();
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(tm.GetTrans())/(float)360.0;
	tm.Scale(Point3(scaleFactor,scaleFactor,scaleFactor));
}

int rageMaxColladaLocator::Display(TimeValue t, INode* inode, ViewExp *vpt, int flags) 
{
	Matrix3 m;
	GraphicsWindow *gw = vpt->getGW();
	Material *mtl = gw->getMaterial();

	GetMat(t,inode,vpt,m);
	gw->setTransform(m);
	DWORD rlim = gw->getRndLimits();
	gw->setRndLimits(GW_WIREFRAME|GW_EDGES_ONLY|GW_BACKCULL);
	if (inode->Selected()) 
	{
		gw->setColor( LINE_COLOR, GetSelColor());
	}
	else if(!inode->IsFrozen() && !inode->Dependent())
	{
		gw->setColor( LINE_COLOR, GetUIColor(COLOR_TAPE_OBJ));
	}
	// m_obMesh.render( gw, mtl, NULL, COMP_ALL); 
	m_obMyMesh.render( gw, mtl, NULL, COMP_ALL); 
	//DrawLine(t,inode,gw,1);
	//gw->setRndLimits(rlim);
	//if(m_bEditting && !m_bSpecifyLength) {
	//	Point3 pt(0,0,0);
	//	Matrix3 tm = inode->GetObjectTM(t);
	//	GetTargetPoint(t,inode,pt);
	//	float den = Length(tm.GetRow(2));
	//	float dist = (den!=0)?Length(tm.GetTrans()-pt)/den : 0.0f;
	//	lengthSpin->SetValue( lastDist = dist, FALSE );
	//}
	//if(m_bEditting) {
	//	float len;
	//	m.NoTrans();
	//	dirPt = m * Point3(0,0,1);
	//	if(len = Length(dirPt))
	//		dirPt *= 1.0f/len;
	//	UpdateUI(iObjParams->GetTime());
	//}
	return(0);
}


Interval rageMaxColladaLocator::ObjectValidity(TimeValue time) 
{
	Interval ivalid;
	//ivalid.SetInfinite();
	ivalid.SetEmpty();
	//GetLength(time, ivalid);
	//UpdateUI(time);
	return ivalid; 
}

int rageMaxColladaLocator::HitTest(TimeValue t, INode *inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt) 
{
	HitRegion hitRegion;
	DWORD savedLimits;
	// int res;
	Matrix3 m;
//	if (!enable) return  0;
	GraphicsWindow *gw = vpt->getGW();  
	Material *mtl = gw->getMaterial();
	MakeHitRegion(hitRegion,type,crossing,4,p);  
	gw->setRndLimits(((savedLimits = gw->getRndLimits()) | GW_PICK) & ~GW_ILLUM);
	GetMat(t,inode,vpt,m);
	gw->setTransform(m);
	// if we get a hit on the mesh, we're done
	gw->clearHitCode();
	if (m_obMesh.select( gw, mtl, &hitRegion, flags & HIT_ABORTONHIT )) 
		return TRUE;
	// if not, check the target line, and set the pair flag if it's hit
	// this special case only works with point selection
	if(type != HITTYPE_POINT)
		return 0;
	// don't let line be active if only looking at selected stuff and target isn't selected
	if((flags & HIT_SELONLY) && (inode->GetTarget()) && !inode->GetTarget()->Selected() )
		return 0;
	gw->clearHitCode();
	//if(res = DrawLine(t,inode,gw,-1))
	//	inode->SetTargetNodePair(1);
	gw->setRndLimits(savedLimits);
	return FALSE;
}

// From GeomObject
int rageMaxColladaLocator::IntersectRay(TimeValue t, Ray& r, float& at) { return(0); }

void rageMaxColladaLocator::GetDeformBBox(TimeValue t, Box3& box, Matrix3 *tm, BOOL useSel )
{
	box = m_obMesh.getBoundingBox(tm);
}

void rageMaxColladaLocator::GetLocalBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box ) 
{
	Matrix3 m = inode->GetObjectTM(t);
	Point3 pt;
	Point3 q[4];
	float scaleFactor = vpt->NonScalingObjectSize()*vpt->GetVPWorldWidth(m.GetTrans())/(float)360.0;
	box = m_obMesh.getBoundingBox();
	box.Scale(scaleFactor);

	//float d;
	//if (GetTargetPoint(t,inode,pt)){
	//	d = Length(m.GetTrans()-pt)/Length(inode->GetObjectTM(t).GetRow(2));
	//	box += Point3(float(0),float(0),-d);
	//}
	//if(GetSpecLen()) {
	//	GetLinePoints(t, q, GetLength(t) );
	//	box += q[0];
	//	box += q[1];
	//}
}

void rageMaxColladaLocator::GetWorldBoundBox(TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	int i, nv;
	Matrix3 tm;
	//float dtarg;
	Point3 pt;
	Point3 q[2];
	GetMat(t,inode,vpt,tm);
	nv = m_obMesh.getNumVerts();
	box.Init();
	for (i=0; i<nv; i++) 
	{
		box += tm*m_obMesh.getVert(i);
	}
	//if (GetTargetPoint(t,inode,pt)) {
	//	tm = inode->GetObjectTM(t);
	//	dtarg = Length(tm.GetTrans()-pt)/Length(tm.GetRow(2));
	//	box += tm*Point3(float(0),float(0),-dtarg);
	//}
	//if(GetSpecLen()) {
	//	GetLinePoints(t, q, GetLength(t) );
	//	box += tm * q[0];
	//	box += tm * q[1];
	//}
}


INT_PTR CALLBACK rageMaxColladaLocatorParamDialogProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam )
{
	rageMaxColladaLocator *th = DLGetWindowLongPtr<rageMaxColladaLocator *>( hDlg); 
	if ( !th && message != WM_INITDIALOG ) return FALSE;

	switch ( message ) {
			case WM_INITDIALOG:
				th = (rageMaxColladaLocator *)lParam;
				DLSetWindowLongPtr( hDlg, th);
				SetDlgFont( hDlg, th->iObjParams->GetAppHFont() );

				CheckDlgButton( hDlg, IDC_SPEC_LEN, th->m_bSpecifyLength );
				SetDlgItemText( hDlg, IDC_COLLADAFILENAME_EDIT, th->m_acColladaFilename);
				// EnableWindow(GetDlgItem(hDlg, IDC_LENGTH), th->m_bSpecifyLength);
				// EnableWindow(GetDlgItem(hDlg, IDC_LENSPINNER), th->m_bSpecifyLength);

				return FALSE;        

			case WM_DESTROY:
				return FALSE;

			case CC_SPINNER_BUTTONDOWN:
				theHold.Begin();
				return TRUE;

			case CC_SPINNER_CHANGE:
				if (!theHold.Holding()) theHold.Begin();
			//	switch ( LOWORD(wParam) ) {
			//case IDC_LENSPINNER:
			//	th->iObjParams->RedrawViews(th->iObjParams->GetTime(),REDRAW_INTERACTIVE);
			//	break;
			//	}
			//	return TRUE;

			case WM_CUSTEDIT_ENTER:
			//case CC_SPINNER_BUTTONUP:
			//	if (HIWORD(wParam) || message==WM_CUSTEDIT_ENTER) theHold.Accept(GetString(IDS_DS_PARAMCHG));
			//	else theHold.Cancel();
			//	th->iObjParams->RedrawViews(th->iObjParams->GetTime(),REDRAW_END);
			//	return TRUE;

			case WM_MOUSEACTIVATE:
				th->iObjParams->RealizeParamPanel();
				return FALSE;

			case WM_LBUTTONDOWN:
			case WM_LBUTTONUP:
			case WM_MOUSEMOVE:
				th->iObjParams->RollupMouseMessage(hDlg,message,wParam,lParam);
				return FALSE;

			case WM_COMMAND:        
				switch( LOWORD(wParam) ) {
				case IDC_SPEC_LEN:               
					th->SetSpecifyLength( IsDlgButtonChecked( hDlg, IDC_SPEC_LEN ) == TRUE );
					//EnableWindow(GetDlgItem(hDlg, IDC_LENGTH), th->m_bSpecifyLength);
					//EnableWindow(GetDlgItem(hDlg, IDC_LENSPINNER), th->m_bSpecifyLength);             
					th->iObjParams->RedrawViews(th->iObjParams->GetTime());
					break;
				case IDC_COLLADAFILENAME_EDIT:
					GetDlgItemText(hDlg, IDC_COLLADAFILENAME_EDIT, th->m_acColladaFilename, 512);
					// th->SetColladaFilename( IsDlgButtonChecked( hDlg, IDC_SPEC_LEN ) == TRUE );
					//EnableWindow(GetDlgItem(hDlg, IDC_LENGTH), th->m_bSpecifyLength);
					//EnableWindow(GetDlgItem(hDlg, IDC_LENSPINNER), th->m_bSpecifyLength);             
					th->iObjParams->RedrawViews(th->iObjParams->GetTime());
					break;
				}
				return FALSE;

			default:
				return FALSE;
	}
}



void rageMaxColladaLocator::BeginEditParams( IObjParam *ip, ULONG flags,Animatable *prev )
{
	iObjParams = ip;
	m_bEditting = TRUE;

	if ( !m_hMaxColladaLocatorParams ) 
	{
		m_hMaxColladaLocatorParams = ip->AddRollupPage( 
			hInstance, 
			MAKEINTRESOURCE(IDD_PANEL),
			rageMaxColladaLocatorParamDialogProc, 
			GetString(IDS_RB_PARAMETERS), 
			(LPARAM)this );   

		ip->RegisterDlgWnd(m_hMaxColladaLocatorParams);

	} 
	else 
	{
		DLSetWindowLongPtr( m_hMaxColladaLocatorParams, this);

		// Init the dialog to our values.
		SetSpecifyLength( IsDlgButtonChecked(m_hMaxColladaLocatorParams,IDC_SPEC_LEN) == TRUE);
		//if(m_bSpecifyLength)
		//	lengthSpin->SetValue( GetLength(ip->GetTime()), FALSE );
		//else 
		//	lengthSpin->SetValue( lastDist, FALSE );
	}
}

void rageMaxColladaLocator::EndEditParams( IObjParam *ip, ULONG flags,Animatable *prev)
{
	m_bEditting = FALSE;

	//if(m_bSpecifyLength)
	//	dlgLength = GetLength(ip->GetTime());
	//dlgSpecLen = IsDlgButtonChecked(m_hMaxColladaLocatorParams, IDC_SPEC_LEN);

	if ( flags&END_EDIT_REMOVEUI ) 
	{
		ip->UnRegisterDlgWnd(m_hMaxColladaLocatorParams);
		ip->DeleteRollupPage(m_hMaxColladaLocatorParams);
		m_hMaxColladaLocatorParams = NULL;          
	} 
	else 
	{
		DLSetWindowLongPtr( m_hMaxColladaLocatorParams, 0);
	}

	iObjParams = NULL;
}

void rageMaxColladaLocator::BuildMeshFromColladaFile(const char* pcColladaPathAndFilename)
{
	//rageRolladaScene* pobRolladaScene = NULL;
	//pobRolladaScene = new rageRolladaScene();
	//pobRolladaScene->CreateFromFile(pcColladaPathAndFilename);
	rageRolladaScene* pobRolladaScene = new rageRolladaScene();
	pobRolladaScene->CreateFromFile(pcColladaPathAndFilename);
	// return;

	// Covert the collada sub meshes into MAX meshes
	if(pobRolladaScene->GetColladaScene() != NULL)
	{
		const rageColladaScene* pobColladaScene = pobRolladaScene->GetColladaScene();
		// const rageColladaScene* pobColladaScene2 = pobRolladaScene->m_pobRageColladaScene;
		// const vector<rageColladaMesh*>&	ObArrayOfMeshes = pobColladaScene->GetArrayOfMeshes();
		// for(vector<rageColladaMesh*>::const_iterator m = ObArrayOfMeshes.begin(); m != ObArrayOfMeshes.end(); m++)
		for(unsigned m=0; m<pobColladaScene->GetArrayOfMeshes().size(); m++)
		{
			// const rageColladaMesh* pobColladaMesh = (*m);// .pobColladaScene->GetArrayOfMeshes()[m];
			const rageColladaMesh* pobColladaMesh = pobColladaScene->GetArrayOfMeshes()[m];
			for(unsigned sm=0; sm<pobColladaMesh->GetArrayOfSubMeshes().size(); sm++)
			{
				const rageColladaSubMesh* pobColladaSubMesh = pobColladaMesh->GetArrayOfSubMeshes()[sm];

				// Convert the submesh to a max mesh
				int iNoOfVerts = pobColladaSubMesh->GetVertices()->GetArrayOfVertexPositions().size();
				m_obMyMesh.setNumVerts(iNoOfVerts);
				for(int iV = 0; iV<iNoOfVerts; iV++)
				{
					rageColladaPoint obPos = pobColladaSubMesh->GetVertices()->GetArrayOfVertexPositions()[iV];
					m_obMyMesh.setVert(iV, Point3(obPos.GetX(), obPos.GetY(), obPos.GetZ()));
				}

				int iNoOfFaces = pobColladaSubMesh->GetArrayOfFaces().size();
				m_obMyMesh.setNumFaces(iNoOfFaces);
				for(int f = 0; f<iNoOfFaces; f++)
				{
					m_obMyMesh.faces[f].setVerts(pobColladaSubMesh->GetGlobalVertexNumber(f, 0), 
						pobColladaSubMesh->GetGlobalVertexNumber(f, 1), 
						pobColladaSubMesh->GetGlobalVertexNumber(f, 2));
					m_obMyMesh.faces[f].setEdgeVisFlags(1,1,1);
				}
				
				/*
				int nverts = 5;
				int nfaces = 6;
				m_obMesh.setNumVerts(nverts);
				m_obMesh.setNumFaces(nfaces);

				float d =  5.0f;
				float h = 10.0f;

				m_obMesh.setVert(0, Point3(  -d, -d, 0.0f ));
				m_obMesh.setVert(1, Point3(   d, -d, 0.0f ));
				m_obMesh.setVert(2, Point3(   d,  d, 0.0f ));
				m_obMesh.setVert(3, Point3(  -d,  d, 0.0f ));
				m_obMesh.setVert(4, Point3(0.0f,0.0f, -h  ));
				m_obMesh.faces[0].setVerts(0, 3, 1);
				m_obMesh.faces[0].setEdgeVisFlags(1,1,0);
				m_obMesh.faces[1].setVerts(3, 2, 1);
				m_obMesh.faces[1].setEdgeVisFlags(1,1,0);
				m_obMesh.faces[2].setVerts(0, 1, 4);
				m_obMesh.faces[2].setEdgeVisFlags(1,1,1);
				m_obMesh.faces[3].setVerts(1, 2, 4);
				m_obMesh.faces[3].setEdgeVisFlags(1,1,1);
				m_obMesh.faces[4].setVerts(2, 3, 4);
				m_obMesh.faces[4].setEdgeVisFlags(1,1,1);
				m_obMesh.faces[5].setVerts(3, 0, 4);
				m_obMesh.faces[5].setEdgeVisFlags(1,1,1);
*/

				m_obMyMesh.buildNormals();
				m_obMyMesh.EnableEdgeList(1);
				return;
			}
		}
	}
}

