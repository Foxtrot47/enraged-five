#include "rageMaxColladaLocatorCreateCallBack.h"
// #define _OSNAP
#define _3D_CREATE

int rageMaxColladaLocatorCreateCallBack::proc(ViewExp *vpt,int msg, int point, int flags, IPoint2 m, Matrix3& mat ) 
{
//	ob->enable = 1;

#ifdef _OSNAP
	if (msg == MOUSE_FREEMOVE)
	{
#ifdef _3D_CREATE
		vpt->SnapPreview(m,m,NULL, SNAP_IN_3D);
#else
		vpt->SnapPreview(m,m,NULL, SNAP_IN_PLANE);
#endif
	}
#endif

	if (msg==MOUSE_POINT||msg==MOUSE_MOVE) {
#ifdef _3D_CREATE
		mat.SetTrans( vpt->SnapPoint(m,m,NULL,SNAP_IN_3D) );
#else
		mat.SetTrans( vpt->SnapPoint(m,m,NULL,SNAP_IN_PLANE) );
#endif
		if (point==1 && msg==MOUSE_POINT) 
			return 0;
	}
	else
		if (msg == MOUSE_ABORT)
			return CREATE_ABORT;

	return TRUE;
}
