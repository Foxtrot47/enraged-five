#pragma once

#include "..\xapcore\xapfile.h"
#include "resource.h"
#include <afxtempl.h>

class XAPFile;

#define VERSION     "1.1"   // Increment this every time the program changes


struct OPTIONS
{
    bool isCreate;  // create a new XAP file with global settings from master file
    bool isExtract; // extract individual project XAP from master file
    bool isMerge;   // merge individual XAP into project XAP  
	bool isVerbose; // be verbose
};


bool ParseCommandLine(int argc, char *argv[], OPTIONS* opts);
void ShowUsage(char* exename);
