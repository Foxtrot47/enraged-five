Imports System.IO

Public Class TreeView
    Inherits System.Windows.Forms.Form

    Enum WarningTypes
        NO_WARNING
        DTX_SECTION_NOT_FILLED_IN
        POSSIBLE_STRAY_TOPIC
        SYMBOL_NO_DESC
        FUNC_PARAM_NO_DESC
        FUNC_PARAM_NO_NAME
        FUNC_RETURN_VAL_NO_DESC
    End Enum

    Public Class TableInfo
        Public Table As New Hashtable
        Public Root As MyTreeNode
        Public NumWarnings As Integer() = {0, 0, 0, 0, 0, 0, 0}
      
        Public Dirty As Boolean = False

        Sub UpdateRootText()
            Dim displayOpeningParen As Boolean = True
            Root.Text = Root.ActualName

            Dim SectionDesc As String() = {" No Warnings", _
                                            " .dtx Sections Not Filled In", _
                                            " Possible Stray Topics", _
                                            " Symbols Not Documented", _
                                            " Function Parameters Not Described", _
                                            " Function Parameters Not Named", _
                                            " Function Return Values Not Described" _
                                            }

            '' loop through and construct text string:
            For warn As Integer = WarningTypes.DTX_SECTION_NOT_FILLED_IN To WarningTypes.FUNC_RETURN_VAL_NO_DESC
                If NumWarnings(warn) >= 1 Then
                    '' display open paren:
                    If displayOpeningParen = True Then
                        displayOpeningParen = False
                        Root.Text = Root.Text + " ("
                    Else
                        Root.Text = Root.Text + ", "
                    End If
                    Root.Text = Root.Text + NumWarnings(warn).ToString() + SectionDesc(warn)
                End If
            Next

            If displayOpeningParen = False Then
                Root.Text = Root.Text + ")"
            End If
        End Sub

        Sub AddWarningType(ByVal str As String)
            Dim type As WarningTypes = ConvertWarnTypeString(str)
            If type <> WarningTypes.NO_WARNING Then
                NumWarnings(type) = NumWarnings(type) + 1
            End If
        End Sub

        Sub SubtractWarningType(ByVal str As String)
            Dim type As WarningTypes = ConvertWarnTypeString(str)
            If type <> WarningTypes.NO_WARNING Then
                NumWarnings(type) = NumWarnings(type) - 1
            End If
        End Sub
    End Class

    Enum SortType
        NAME
        WARN_TYPE
    End Enum

    '' member variables:
    Public AllIgnoreHashes As New Hashtable
    Public AllNodeHashes As New Hashtable
    Public Loaded As Boolean = False
    Public CheckedNodes As ArrayList
    Public CheckedNodesValid As New ArrayList
    Public CheckedNodesIgnore As New ArrayList
    Public LoadPath As String = ""

    Public Class MyTreeNode
        Inherits TreeNode
        Public ActualName As String = ""
        Public WarnType As String = ""
        Public RootNode As MyTreeNode = Nothing

        Public Shared Type As SortType = SortType.WARN_TYPE

        Sub New(ByRef name As String, ByRef warningType As String)
            MyBase.New(name) ' Call MyBase.New if this is a derived class.

            ActualName = name

            WarnType = warningType

            SetText()
        End Sub

        Sub SetText()
            If WarnType = WarningTypes.SYMBOL_NO_DESC.ToString() Then
                ForeColor = Color.DarkRed
                If Type = SortType.NAME Then
                    Text = ActualName + " - SYMBOL NOT DOCUMENTED"
                Else
                    Text = "SYMBOL NOT DOCUMENTED - " + ActualName
                End If
            ElseIf WarnType = WarningTypes.DTX_SECTION_NOT_FILLED_IN.ToString() Then
                ForeColor = Color.DarkOrange
                If Type = SortType.NAME Then
                    Text = ActualName + " - .DTX SECTION NOT FILLED IN"
                Else
                    Text = ".DTX SECTION NOT FILLED IN - " + ActualName
                End If
            ElseIf WarnType = WarningTypes.POSSIBLE_STRAY_TOPIC.ToString() Then
                ForeColor = Color.DarkMagenta
                If Type = SortType.NAME Then
                    Text = ActualName + " - POSSIBLE STRAY TOPIC"
                Else
                    Text = "POSSIBLE STRAY TOPIC - " + ActualName
                End If
            ElseIf WarnType = WarningTypes.FUNC_PARAM_NO_DESC.ToString() Then
                ForeColor = Color.DarkGreen
                If Type = SortType.NAME Then
                    Text = ActualName + " - FUNCTION PARAMETER NOT DESCRIBED"
                Else
                    Text = "FUNCTION PARAMETER NOT DESCRIBED - " + ActualName
                End If
            ElseIf WarnType = WarningTypes.FUNC_PARAM_NO_NAME.ToString() Then
                ForeColor = Color.DarkBlue
                If Type = SortType.NAME Then
                    Text = ActualName + " - FUNCTION PARAMETER HAS NO NAME"
                Else
                    Text = "FUNCTION PARAMETER HAS NO NAME - " + ActualName
                End If
            ElseIf WarnType = WarningTypes.FUNC_RETURN_VAL_NO_DESC.ToString() Then
                ForeColor = Color.DarkKhaki
                If Type = SortType.NAME Then
                    Text = ActualName + " - FUNCTION RETURN VALUE NOT DESCRIBED"
                Else
                    Text = "FUNCTION RETURN VALUE NOT DESCRIBED - " + ActualName
                End If
            End If
        End Sub

        Sub SetCheckColor()
            If (Checked) Then
                BackColor = Color.LightGray
            Else
                BackColor = Color.White
            End If
        End Sub

    End Class

    Public Shared Function ConvertWarnTypeString(ByVal str As String) As WarningTypes
        Select Case str
            Case "SYMBOL_NO_DESC"
                Return WarningTypes.SYMBOL_NO_DESC
            Case "FUNC_PARAM_NO_DESC"
                Return WarningTypes.FUNC_PARAM_NO_DESC
            Case "FUNC_PARAM_NO_NAME"
                Return WarningTypes.FUNC_PARAM_NO_NAME
            Case "FUNC_RETURN_VAL_NO_DESC"
                Return WarningTypes.FUNC_RETURN_VAL_NO_DESC
            Case "POSSIBLE_STRAY_TOPIC"
                Return WarningTypes.POSSIBLE_STRAY_TOPIC
            Case "DTX_SECTION_NOT_FILLED_IN"
                Return WarningTypes.DTX_SECTION_NOT_FILLED_IN
            Case Else
                Return WarningTypes.NO_WARNING '' unknown
        End Select

    End Function

    Public Sub ReadFile(ByVal fileName As String)

        '' figure out module name:
        Dim nameSplit As String() = fileName.Split(".")
        Dim moduleName As String
        moduleName = String.Empty
        If nameSplit.Length = 2 Then
            moduleName = Path.GetFileName(nameSplit(0))
            Dim prefix As String = "qa_"
            moduleName = moduleName.Remove(0, prefix.Length)
        End If

        '' don't load it if it's already loaded:
        If AllIgnoreHashes.ContainsKey(moduleName) = True Or AllNodeHashes.ContainsKey(moduleName) Then
            MsgBox("module '" + moduleName + "' already loaded.", MsgBoxStyle.OKOnly, "Module Already Loaded!")
            Exit Sub
        End If

        Dim rootNodeIgnore As New MyTreeNode(moduleName, WarningTypes.NO_WARNING.ToString())
        Dim ignoreInfo As New TableInfo
        Dim parentNodeIgnore As MyTreeNode = Nothing
        ignoreInfo.Root = rootNodeIgnore

        AllIgnoreHashes.Add(moduleName, ignoreInfo)

        '' read in ignore file:
        Dim ignoreName As String = Path.GetDirectoryName(fileName) + "\qaignore_" + moduleName + ".txt"
        If (File.Exists(ignoreName)) Then
            Dim ignoreRef As StreamReader = File.OpenText(ignoreName)
            While ignoreRef.Peek <> -1
                Dim line As String = ignoreRef.ReadLine()
                line.Trim()
                Dim lineParts As String() = line.Split("$")
                If ignoreInfo.Table.ContainsKey(line) = False Then
                    Dim key As String
                    If lineParts(0) = "1" Then
                        key = lineParts(1) + "$" + lineParts(2) + "$IS_TREE"
                    Else
                        key = lineParts(1) + "$" + lineParts(2)
                    End If
                    ignoreInfo.Table.Add(key, key)
                End If
            End While

            ignoreRef.Close()
        End If

        '' add it to the big table:

        '' all view:
        Dim rootNode As New MyTreeNode(moduleName, WarningTypes.NO_WARNING.ToString())
        Dim warnInfo As New TableInfo
        Dim parentNode As MyTreeNode = Nothing
        warnInfo.Root = rootNode
        AllNodeHashes.Add(moduleName, warnInfo)

        '' make the all view current:
        TreeView1.Nodes.Add(rootNode)
        Item_ViewAll.Checked = True
        Item_ViewIgnored.Checked = False

        '' read through file:
        Dim fileRef As StreamReader = File.OpenText(fileName)
        While fileRef.Peek <> -1
            Dim line As String = fileRef.ReadLine()
            line = line.Trim()
            Dim lineParts As String() = line.Split("$")

            '' read in tree:
            If lineParts.Length >= 2 Then
                If lineParts(0) = "3" Then
                    parentNode = Nothing
                    parentNodeIgnore = Nothing
                    Dim node As New MyTreeNode(lineParts(1), lineParts(2))
                    node.WarnType = lineParts(2)
                    If ignoreInfo.Table.ContainsKey(lineParts(1) + "$" + lineParts(2)) = False Then
                        warnInfo.Table.Add(lineParts(1) + "$" + lineParts(2), node)
                        node.RootNode = rootNode
                        rootNode.Nodes.Add(node)
                        warnInfo.AddWarningType(node.WarnType)
                    Else
                        ignoreInfo.Table.Item(lineParts(1) + "$" + lineParts(2)) = node
                        node.RootNode = rootNodeIgnore
                        rootNodeIgnore.Nodes.Add(node)
                        ignoreInfo.AddWarningType(node.WarnType)
                    End If
                ElseIf lineParts(0) = "1" Then
                    parentNode = New MyTreeNode(lineParts(1), lineParts(2))
                    parentNodeIgnore = New MyTreeNode(lineParts(1), lineParts(2))
                    If ignoreInfo.Table.ContainsKey(lineParts(1) + "$" + lineParts(2) + "$IS_TREE") = False Then
                        warnInfo.Table.Add(lineParts(1) + "$" + lineParts(2) + "$IS_TREE", parentNode)
                        ''rootNode.Nodes.Add(parentNode)
                        ''parentNode.RootNode = rootNode
                    Else
                        ignoreInfo.Table.Item(lineParts(1) + "$" + lineParts(2) + "$IS_TREE") = parentNodeIgnore
                        ''rootNodeIgnore.Nodes.Add(parentNodeIgnore)
                        ''parentNodeIgnore.RootNode = rootNodeIgnore
                    End If
                ElseIf lineParts(0) = "2" Then
                    Dim node As New MyTreeNode(lineParts(1), lineParts(2))
                    node.WarnType = lineParts(2)
                    If ignoreInfo.Table.ContainsKey(lineParts(1) + "$" + lineParts(2)) = False Or parentNodeIgnore Is Nothing Then
                        warnInfo.Table.Add(lineParts(1) + "$" + lineParts(2), node)
                        node.RootNode = rootNode
                        parentNode.Nodes.Add(node)
                        warnInfo.AddWarningType(node.WarnType)
                        '' parent it to the node tree if it's not already there:
                        If parentNode.Parent Is Nothing Then
                            rootNode.Nodes.Add(parentNode)
                            parentNode.RootNode = rootNode
                        End If
                    Else
                        ignoreInfo.Table.Item(lineParts(1) + "$" + lineParts(2)) = node
                        node.RootNode = rootNodeIgnore
                        parentNodeIgnore.Nodes.Add(node)
                        ignoreInfo.AddWarningType(node.WarnType)
                        '' parent it to the node tree if it's not already there:
                        If parentNodeIgnore.Parent Is Nothing Then
                            rootNodeIgnore.Nodes.Add(parentNodeIgnore)
                            parentNodeIgnore.RootNode = rootNodeIgnore
                        End If
                    End If
                    End If
            End If

        End While
        fileRef.Close()

        warnInfo.UpdateRootText()
        ignoreInfo.UpdateRootText()

        Item_SortByName.Checked = False
        Item_SortByWarnType.Checked = True

        Loaded = True
        Item_Save.Enabled = True
        CheckedNodes = CheckedNodesValid

        warnInfo.Root.EnsureVisible()
        TreeView1.Refresh()
        GroupBox_CurrentView.Text = "Viewing Valid Warnings"
    End Sub

    Public Sub ReadFiles(ByVal pathStr As String)
        'Add any initialization after the InitializeComponent() call
        Dim qaPath As String = pathStr
        Dim files As String() = Directory.GetFiles(qaPath, "qa_*.txt")
        Dim fileName As String

        Cursor.Current = Cursors.WaitCursor

        For Each fileName In files
            ReadFile(fileName)
        Next

        Cursor.Current = System.Windows.Forms.Cursors.Default
    End Sub

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents Item_ViewAll As System.Windows.Forms.MenuItem
    Friend WithEvents Item_ViewIgnored As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents Item_SortByName As System.Windows.Forms.MenuItem
    Friend WithEvents Item_SortByWarnType As System.Windows.Forms.MenuItem
    Friend WithEvents OpenDialog As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Item_Open As System.Windows.Forms.MenuItem
    Friend WithEvents Item_Save As System.Windows.Forms.MenuItem
    Friend WithEvents Item_OpenPath As System.Windows.Forms.MenuItem
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents Item_EmptyTree As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox_CurrentView As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(TreeView))
        Me.TreeView1 = New System.Windows.Forms.TreeView
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.Item_Open = New System.Windows.Forms.MenuItem
        Me.Item_OpenPath = New System.Windows.Forms.MenuItem
        Me.Item_Save = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.Item_EmptyTree = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.Item_ViewAll = New System.Windows.Forms.MenuItem
        Me.Item_ViewIgnored = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.Item_SortByName = New System.Windows.Forms.MenuItem
        Me.Item_SortByWarnType = New System.Windows.Forms.MenuItem
        Me.GroupBox_CurrentView = New System.Windows.Forms.GroupBox
        Me.OpenDialog = New System.Windows.Forms.FolderBrowserDialog
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog
        Me.SuspendLayout()
        '
        'TreeView1
        '
        Me.TreeView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TreeView1.CheckBoxes = True
        Me.TreeView1.ContextMenu = Me.ContextMenu1
        Me.TreeView1.ImageIndex = -1
        Me.TreeView1.Location = New System.Drawing.Point(16, 24)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.SelectedImageIndex = -1
        Me.TreeView1.Size = New System.Drawing.Size(888, 544)
        Me.TreeView1.Sorted = True
        Me.TreeView1.TabIndex = 0
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Ignore Checked Warning(s)"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem3, Me.MenuItem6})
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.Item_Open, Me.Item_OpenPath, Me.Item_Save, Me.MenuItem5, Me.MenuItem4})
        Me.MenuItem2.Text = "&File"
        '
        'Item_Open
        '
        Me.Item_Open.Index = 0
        Me.Item_Open.Shortcut = System.Windows.Forms.Shortcut.CtrlO
        Me.Item_Open.Text = "&Open..."
        '
        'Item_OpenPath
        '
        Me.Item_OpenPath.Index = 1
        Me.Item_OpenPath.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftO
        Me.Item_OpenPath.Text = "Open Path..."
        '
        'Item_Save
        '
        Me.Item_Save.Enabled = False
        Me.Item_Save.Index = 2
        Me.Item_Save.Shortcut = System.Windows.Forms.Shortcut.CtrlS
        Me.Item_Save.Text = "&Save"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 4
        Me.MenuItem4.Text = "E&xit"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.Item_EmptyTree})
        Me.MenuItem3.Text = "&Edit"
        '
        'Item_EmptyTree
        '
        Me.Item_EmptyTree.Index = 0
        Me.Item_EmptyTree.Text = "&Empty Tree"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 2
        Me.MenuItem6.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.Item_ViewAll, Me.Item_ViewIgnored, Me.MenuItem8, Me.Item_SortByName, Me.Item_SortByWarnType})
        Me.MenuItem6.Text = "&View"
        '
        'Item_ViewAll
        '
        Me.Item_ViewAll.Index = 0
        Me.Item_ViewAll.Text = "View &All"
        '
        'Item_ViewIgnored
        '
        Me.Item_ViewIgnored.Index = 1
        Me.Item_ViewIgnored.Text = "View &Ignored"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 2
        Me.MenuItem8.Text = "-"
        '
        'Item_SortByName
        '
        Me.Item_SortByName.Index = 3
        Me.Item_SortByName.Text = "Sort By &Name"
        '
        'Item_SortByWarnType
        '
        Me.Item_SortByWarnType.Index = 4
        Me.Item_SortByWarnType.Text = "Sort By Warning &Type"
        '
        'GroupBox_CurrentView
        '
        Me.GroupBox_CurrentView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox_CurrentView.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox_CurrentView.Name = "GroupBox_CurrentView"
        Me.GroupBox_CurrentView.Size = New System.Drawing.Size(904, 568)
        Me.GroupBox_CurrentView.TabIndex = 1
        Me.GroupBox_CurrentView.TabStop = False
        '
        'OpenDialog
        '
        Me.OpenDialog.Description = "Directory To Read qa_*.txt files from"
        Me.OpenDialog.RootFolder = System.Environment.SpecialFolder.MyComputer
        Me.OpenDialog.SelectedPath = "C:\age\src\doc"
        Me.OpenDialog.ShowNewFolderButton = False
        '
        'OpenFileDialog
        '
        Me.OpenFileDialog.DefaultExt = "txt"
        Me.OpenFileDialog.Filter = "QA Text files (*.txt)|qa_*.txt"
        Me.OpenFileDialog.InitialDirectory = "c:\age\src\doc\qa\"
        Me.OpenFileDialog.Multiselect = True
        Me.OpenFileDialog.Title = "Read Single QA File"
        '
        'TreeView
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(920, 581)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.GroupBox_CurrentView)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Menu = Me.MainMenu1
        Me.Name = "TreeView"
        Me.Text = "QA Doctor"
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub SaveNode(ByRef fileRef As StreamWriter, ByRef node As MyTreeNode)
        '' see if we're not a parent node:
        If node.Nodes.Count = 0 Then
            fileRef.WriteLine("3$" + node.ActualName + "$" + node.WarnType)
        Else
            '' we're a parent node:
            fileRef.WriteLine("1$" + node.ActualName + "$" + node.WarnType + "$IS_TREE")

            '' save my children - please!
            For Each child As MyTreeNode In node.Nodes
                fileRef.WriteLine("    2$" + child.ActualName + "$" + child.WarnType)
            Next
        End If
    End Sub

    Private Sub SaveIgnoreFiles()
        Dim hashEnum As IDictionaryEnumerator = AllIgnoreHashes.GetEnumerator
        While hashEnum.MoveNext

            Dim fileName As String = LoadPath + "\qaignore_" + hashEnum.Key() + ".txt"
            Try
                Dim fileRef As StreamWriter = File.CreateText(fileName)
                Dim ignoreInfo As TableInfo = hashEnum.Value
                For Each child As MyTreeNode In ignoreInfo.Root.Nodes
                    SaveNode(fileRef, child)
                Next
                fileRef.Close()
            Catch e As Exception
                MsgBox("couldn't open file '" + fileName + "'", MsgBoxStyle.Exclamation, "Couldn't Open File")
            End Try

        End While
    End Sub

    Private Function FindNode(ByVal root As MyTreeNode, ByVal name As String) As MyTreeNode
        '' found it, return the node:
        If root.ActualName + "$" + root.WarnType = name Then
            Return root
        End If

        For Each nodeRef As MyTreeNode In root.Nodes
            Dim found As MyTreeNode = FindNode(nodeRef, name)

            '' found it!
            If Not found Is Nothing Then
                Return found
            End If
        Next

        '' didn't find it
        Return Nothing
    End Function

    Private Sub RemoveNode(ByRef node As MyTreeNode, ByRef fromTree As Hashtable, ByRef toTree As Hashtable)
        '' don't remove the root node!
        If node.RootNode Is Nothing Then
            Exit Sub
        End If

        Dim rootName As String = node.RootNode.ActualName

        If toTree.ContainsKey(rootName) Then
            Dim hashTo As TableInfo = toTree.Item(rootName)
            hashTo.Dirty = True

            Dim nodeName As String = node.ActualName + "$" + node.WarnType

            '' if it's not there already, add it:
            If hashTo.Table.ContainsKey(nodeName) = False Then
                '' remove the node from the tree:
                Dim hashNodes As Hashtable = fromTree.Item(rootName).Table
                If hashNodes.ContainsKey(nodeName) Then
                    Dim hashNode As MyTreeNode = hashNodes.Item(nodeName)

                    '' we're underneath a group, so add the group to the tree if it's not there:
                    Dim oldParent As MyTreeNode = hashNode.Parent

                    '' move it to the ignore tree:
                    hashNode.RootNode = hashTo.Root
                    hashNode.Checked = False
                    hashNode.SetCheckColor()

                    '' remove it from the valid graph:
                    hashNode.Remove()

                    If Not oldParent Is node.RootNode Then
                        '' see if the parent node is already there:
                        Dim newParent As MyTreeNode = FindNode(hashTo.Root, oldParent.ActualName + "$" + oldParent.WarnType)

                        '' if it's not, create a new parent node:
                        If newParent Is Nothing Then
                            Dim newNode As New MyTreeNode(oldParent.ActualName, oldParent.WarnType)
                            newNode.RootNode = hashTo.Root

                            hashTo.Root.Nodes.Add(newNode)
                            newParent = newNode
                        End If

                        '' parent it:
                        newParent.Nodes.Add(hashNode)
                    Else
                        '' parent it directly to the root node:
                        hashTo.Root.Nodes.Add(hashNode)
                    End If

                    '' add to the hash tree:
                    hashTo.Table.Add(nodeName, node)

                    '' remove it from the hash table:
                    hashNodes.Remove(nodeName)

                    '' subtract stats:
                    If (fromTree.ContainsKey(rootName)) Then
                        Dim hashFrom As TableInfo = fromTree.Item(rootName)
                        hashFrom.SubtractWarningType(node.WarnType)
                    End If

                    '' add stats:
                    hashTo.AddWarningType(node.WarnType)

                    '' if the old parent lost all of its children, then remove the parent node itself:
                    If oldParent.Nodes.Count = 0 Then
                        oldParent.Remove()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub UpdateNodeText(ByRef node As MyTreeNode)
        node.SetText()
        Dim nodeRef As MyTreeNode = node.FirstNode
        While Not nodeRef Is Nothing
            UpdateNodeText(nodeRef)
            nodeRef = nodeRef.NextNode
        End While

    End Sub

    Private Sub MoveCheckedNodes(ByVal node As MyTreeNode, ByRef fromTree As Hashtable, ByRef toTree As Hashtable)
        '' remove children nodes:
        Dim nodeRef As MyTreeNode = node.FirstNode
        While Not nodeRef Is Nothing
            Dim nextNode As MyTreeNode = nodeRef.NextNode
            MoveCheckedNodes(nodeRef, fromTree, toTree)
            nodeRef = nextNode
        End While

        '' if it's ignored, remove it:
        If node.Checked = True Then
            RemoveNode(node, fromTree, toTree)
        End If

    End Sub

    Private Sub MenuItem1_Popup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContextMenu1.Popup
        Dim oneIsChecked As Boolean = False

        '' see if there's one checked:
        If TreeView1.Nodes.Count >= 1 Then
            Dim nodeRef As MyTreeNode = TreeView1.Nodes.Item(0)
            While Not nodeRef Is Nothing
                If nodeRef.Checked = True Then
                    oneIsChecked = True
                    Exit While
                End If
                nodeRef = nodeRef.NextNode
            End While
        End If

        '' if there's one checked, change message in menu item to match:
        If CheckedNodes.Count > 0 Then
            MenuItem1.Enabled = True
            If Item_ViewIgnored.Checked = True Then
                MenuItem1.Text = "Remember Checked Warnings (" + CheckedNodes.Count.ToString() + " nodes)"
            Else
                MenuItem1.Text = "Ignore Checked Warnings (" + CheckedNodes.Count.ToString() + " nodes)"
            End If
        Else
            MenuItem1.Enabled = False
            MenuItem1.Text = "Nothing Checked"
        End If

    End Sub

  
    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        '' recurse through all nodes, looking for checked nodes:
        ''Dim nodeRef As MyTreeNode = TreeView1.Nodes.Item(0)
        ''While Not nodeRef Is Nothing
        ''Dim nextNode As MyTreeNode = nodeRef.NextNode
        ''If Item_ViewIgnored.Checked = True Then
        ''MoveCheckedNodes(nodeRef, AllIgnoreHashes, AllNodeHashes)
        ''Else
        ''    MoveCheckedNodes(nodeRef, AllNodeHashes, AllIgnoreHashes)
        ''End If
        ''nodeRef = nextNode
        ''End While

        '' remove all checked nodes:
        For Each node As MyTreeNode In CheckedNodes
            If Item_ViewIgnored.Checked = True Then
                RemoveNode(node, AllIgnoreHashes, AllNodeHashes)
            Else
                RemoveNode(node, AllNodeHashes, AllIgnoreHashes)
            End If
        Next
        CheckedNodes.Clear()

        '' enumerate and update text for root node for both tables:
        Dim hashEnum As IDictionaryEnumerator = AllIgnoreHashes.GetEnumerator
        While hashEnum.MoveNext
            hashEnum.Value.UpdateRootText()
        End While

        hashEnum = AllNodeHashes.GetEnumerator
        While hashEnum.MoveNext
            hashEnum.Value.UpdateRootText()
        End While
    End Sub

    ' Updates all child tree nodes recursively.
    Private Sub CheckAllChildNodes(ByVal treeNode As TreeNode, ByVal nodeChecked As Boolean)
        Dim node As MyTreeNode
        For Each node In treeNode.Nodes
            node.Checked = nodeChecked
            node.SetCheckColor()

            If node.Checked = True Then
                CheckedNodes.Add(node)
            Else
                CheckedNodes.Remove(node)
            End If

            If node.Nodes.Count > 0 Then
                ' If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                Me.CheckAllChildNodes(node, nodeChecked)
            End If
        Next node
    End Sub

    ' NOTE   This code can be added to the BeforeCheck event handler instead of the AfterCheck event.
    ' After a tree node's Checked property is changed, all its child nodes are updated to the same value.
    Private Sub node_AfterCheck(ByVal sender As Object, ByVal e As TreeViewEventArgs) Handles TreeView1.AfterCheck
        ' The code only executes if the user caused the checked state to change.
        If e.Action <> TreeViewAction.Unknown Then

            CType(e.Node, MyTreeNode).SetCheckColor()
            If e.Node.Checked = True Then
                CheckedNodes.Add(e.Node)
            Else
                CheckedNodes.Remove(e.Node)
            End If

            If e.Node.Nodes.Count > 0 Then
                ' Calls the CheckAllChildNodes method, passing in the current 
                ' Checked value of the TreeNode whose checked state changed. 
                Me.CheckAllChildNodes(e.Node, e.Node.Checked)
            End If
        End If
    End Sub


    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Item_ViewIgnored.Click
        If Item_ViewIgnored.Checked = False Then
            TreeView1.Nodes.Clear()
            Dim ignoreEnum As IDictionaryEnumerator = AllIgnoreHashes.GetEnumerator()
            While ignoreEnum.MoveNext
                TreeView1.Nodes.Add(ignoreEnum.Value.Root)
            End While
            Item_ViewIgnored.Checked = True
            Item_ViewAll.Checked = False
            GroupBox_CurrentView.Text = "Viewing Ignored Warnings"
            CheckedNodes = CheckedNodesIgnore
        End If
    End Sub

    Private Sub Item_ViewAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Item_ViewAll.Click
        If Item_ViewAll.Checked = False Then
            TreeView1.Nodes.Clear()
            Dim validEnum As IDictionaryEnumerator = AllNodeHashes.GetEnumerator()
            While validEnum.MoveNext
                TreeView1.Nodes.Add(validEnum.Value.Root)
            End While
            Item_ViewAll.Checked = True
            Item_ViewIgnored.Checked = False
            GroupBox_CurrentView.Text = "Viewing Valid Warnings"
            CheckedNodes = CheckedNodesValid
        End If
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Item_Save.Click
        SaveIgnoreFiles()
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Close()
    End Sub

    Private Sub UpdateAllTextNodes(ByVal type As SortType)
        MyTreeNode.Type = type

        TreeView1.BeginUpdate()

        Cursor.Current = Cursors.WaitCursor

        '' update text for all ignored hashes:
        Dim ignoreEnum As IDictionaryEnumerator = AllIgnoreHashes.GetEnumerator()
        While ignoreEnum.MoveNext
            Dim info As TableInfo = ignoreEnum.Value
            UpdateNodeText(info.Root)
        End While

        '' update text for all valid nodes:
        Dim validEnum As IDictionaryEnumerator = AllNodeHashes.GetEnumerator()
        While validEnum.MoveNext
            Dim info As TableInfo = validEnum.Value
            UpdateNodeText(info.Root)
        End While

        '' update tree so it's sorted:
        TreeView1.Nodes.Clear()
        Dim dictEnum As IDictionaryEnumerator
        If Item_ViewAll.Checked Then
            dictEnum = AllNodeHashes.GetEnumerator()
        Else
            dictEnum = AllIgnoreHashes.GetEnumerator()
        End If

        '' read all trees:
        While dictEnum.MoveNext
            TreeView1.Nodes.Add(dictEnum.Value.Root)
        End While

        TreeView1.EndUpdate()
        TreeView1.Refresh()

        Cursor.Current = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub MenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Item_SortByName.Click
        Item_SortByName.Checked = True
        Item_SortByWarnType.Checked = False
        UpdateAllTextNodes(SortType.NAME)
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Item_SortByWarnType.Click
        Item_SortByName.Checked = False
        Item_SortByWarnType.Checked = True
        UpdateAllTextNodes(SortType.WARN_TYPE)
    End Sub

    Private Sub MenuItem7_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Item_OpenPath.Click
        OpenDialog.SelectedPath = GetSetting("QaDoctor", "GENERAL", "OPEN_PATH_PATH", "c:\age\src\doc\qa\")
        Dim result As DialogResult = OpenDialog.ShowDialog()
        If result = DialogResult.OK Then
            SaveSetting("QaDoctor", "GENERAL", "OPEN_PATH_PATH", OpenDialog.SelectedPath)
            ReadFiles(OpenDialog.SelectedPath)
            LoadPath = OpenDialog.SelectedPath
        End If
    End Sub

    Private Sub Item_OpenPath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Item_Open.Click
        OpenFileDialog.InitialDirectory = GetSetting("QaDoctor", "GENERAL", "OPEN_FILE_PATH", "c:\age\src\doc\qa\")
        Dim result As DialogResult = OpenFileDialog.ShowDialog()
        If result = DialogResult.OK Then
            SaveSetting("QaDoctor", "GENERAL", "OPEN_FILE_PATH", Path.GetDirectoryName(OpenFileDialog.FileName))

            Cursor.Current = Cursors.WaitCursor

            '' open the multiple selected files:
            For Each fileName As String In OpenFileDialog.FileNames
                ReadFile(fileName)
            Next

            Cursor.Current = System.Windows.Forms.Cursors.Default
            LoadPath = Path.GetDirectoryName(OpenFileDialog.FileName)
        End If
    End Sub

    Private Sub MenuItem7_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Item_EmptyTree.Click
        Dim result As MsgBoxResult = MsgBox("Do You Want To Save Any Changes Before Emptying The Tree?", MsgBoxStyle.YesNoCancel, "Save Before Empty?")
        If result = MsgBoxResult.Cancel Then
            Exit Sub
        End If

        '' save if user decided to:
        If result = MsgBoxResult.Yes Then
            SaveIgnoreFiles()
        End If

        '' clear out all nodes:
        AllIgnoreHashes.Clear()
        AllNodeHashes.Clear()
        TreeView1.Nodes.Clear()
        Loaded = False
        CheckedNodesValid.Clear()
        CheckedNodesIgnore.Clear()
        LoadPath = ""
    End Sub
End Class
