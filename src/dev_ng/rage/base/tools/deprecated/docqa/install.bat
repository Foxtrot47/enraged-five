call :installFile docqagui\bin\docqagui.exe ..\..\..\top\doc\bin\docqagui.exe
call :installFile bin\docqa.exe ..\..\..\top\doc\bin\docqa.exe

pause
goto :eof


:installFile

	p4 edit %2
	xcopy %1 %2 /R /Y /D
	p4 revert -a %2

	GOTO eof

:eof