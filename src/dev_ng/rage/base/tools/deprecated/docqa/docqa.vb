
Imports System.IO

Module QaDoc

    Enum WarningTypes
        SYMBOL_NO_DESC
        FUNC_PARAM_NO_DESC
        FUNC_PARAM_NO_NAME
        FUNC_RETURN_VAL_NO_DESC
        POSSIBLE_STRAY_TOPIC
        DTX_SECTION_NOT_FILLED_IN
    End Enum

    Public Class SymbolEntry
        Public Name As String = ""
        Public WarnType As WarningTypes = WarningTypes.SYMBOL_NO_DESC

        Public Overloads Function ToString() As String
            Return Name
        End Function
    End Class

    Public Class SymbolList
        Public Name As String = ""
        Public List As New ArrayList

        Public Overloads Function ToString() As String
            Return Name
        End Function
    End Class

    Public Class SymbolListComparer
        Implements IComparer
        ' Calls CaseInsensitiveComparer.Compare with the parameters reversed.
        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer _
           Implements IComparer.Compare
            Return New CaseInsensitiveComparer().Compare(x.Name(), y.Name())
        End Function 'IComparer.Compare
    End Class

    Sub AddToDict(ByRef symbolDict As Hashtable, ByRef splitLine As String(), ByVal warnType As WarningTypes)
        Dim splitSymbol As String() = splitLine(1).Split(":")
        Dim firstColonIndex As Integer = splitLine(1).IndexOf(":")
        Dim firstWhitespaceIndex As Integer = -1

        If splitSymbol.Length >= 1 Then firstWhitespaceIndex = splitSymbol(0).IndexOf(" ")

        '' section based on class or namespace name:
        Dim symList As SymbolList
        Dim key As String
        Dim hasParent As Boolean = False
        '' see if there's already a corresponding parent:
        If symbolDict.ContainsKey("1$" + splitLine(1) + "$" + warnType.ToString) Then
            '' use whole name, don't parent:
            key = "1$" + splitLine(1) + "$" + warnType.ToString
            hasParent = True

        ElseIf splitSymbol.Length > 1 And firstWhitespaceIndex = -1 Or firstWhitespaceIndex > firstColonIndex Then
            '' we can add a parent for it:
            key = "1$" + splitSymbol(0) + "$" + warnType.ToString
            hasParent = True

            If symbolDict.ContainsKey("3$" + splitSymbol(0) + "$" + warnType.ToString) Then
                '' it was by itself before, let's turn it into a parent - remove the old symList:
                Dim oldKey As String = "3$" + splitSymbol(0) + "$" + warnType.ToString
                symbolDict.Remove(oldKey)

                '' re add it with the new key:
                key = "1$" + splitSymbol(0) + "$" + warnType.ToString()
                symList = New SymbolList
                symList.Name = key
                symbolDict.Add("1$" + splitSymbol(0) + "$" + warnType.ToString, symList)
                hasParent = True
            End If

        Else
            '' otherwise no parent:
            key = "3$" + splitLine(1) + "$" + warnType.ToString()
        End If

        '' see if it's already there, add it to the list:
        If symbolDict.ContainsKey(key) Then
            symList = symbolDict.Item(key)
        Else
            symList = New SymbolList
            symList.Name = key
            symbolDict.Add(key, symList)
        End If

        '' add it:
        Dim name As String
        If hasParent Then
            name = "2$" + splitLine(1) + "$" + warnType.ToString
        Else
            name = "3$" + splitLine(1) + "$" + warnType.ToString
        End If

        If symList.List.Contains(name) = False Then
            Dim symEntry As New SymbolEntry
            symEntry.Name = name

            symEntry.WarnType = warnType
            symList.List.Add(symEntry)
        End If
    End Sub


    Function MyFileOpen(ByVal fileName As String) As StreamReader
        Try
            MyFileOpen = File.OpenText(fileName)
        Catch e As Exception
            Console.WriteLine("'" + fileName + "' couldn't be opened!")
            MyFileOpen = Nothing
        End Try

        If MyFileOpen Is Nothing Then Environment.Exit(1)
    End Function

    Function MyFileCreate(ByVal fileName As String) As StreamWriter
        Try
            MyFileCreate = File.CreateText(fileName)
        Catch e As Exception
            Console.WriteLine("'" + fileName + "' couldn't be created!")
            MyFileCreate = Nothing
        End Try

        If MyFileCreate Is Nothing Then Environment.Exit(1)
    End Function

    Sub Main(ByVal cmdArgs() As String)
        Dim outFile As StreamWriter
        Dim moduleName As String
        Dim docName As String
        Dim inFile As StreamReader
        Dim inDir As String
        Dim outFilename As String
        Dim template As String

        docName = String.Empty
        moduleName = String.Empty
        inDir = String.Empty
        outFilename = String.Empty
        template = String.Empty

        Dim arg As String
        For Each arg In cmdArgs
            '' look for "/indir" arg:
            If arg.StartsWith("/created_dir:") Then
                Dim argName As String = "/created_dir:"
                inDir = arg.Remove(0, argName.Length)
            ElseIf arg.StartsWith("/module:") Then
                Dim argName As String = "/module:"
                moduleName = arg.Remove(0, argName.Length)
            ElseIf arg.StartsWith("/docname:") Then
                Dim argName As String = "/docname:"
                docName = arg.Remove(0, argName.Length)
                '' look for "/out_dir" arg:
            ElseIf arg.StartsWith("/out_file:") Then
                Dim argName As String = "/out_file:"
                outFilename = arg.Remove(0, argName.Length)
            ElseIf arg.StartsWith("/template:") Then
                Dim argName As String = "/template:"
                template = arg.Remove(0, argName.Length)
            End If
        Next

        If docName = "" Then
            docName = moduleName
        End If

        '' make sure we have command line arguments:
        If inDir = "" Or outFilename = "" Or template = "" Then
            Console.WriteLine("usage: docqa.exe /created_help2_dir:<directory where the warning file is> /out_file:<path and filename to output to>" + _
                              "/module:<module name> /template:<.dtx template> [/docname:<name of documentation file>]")
            Exit Sub
        End If

        '' open files:
        Dim txtFileName As String
        If moduleName.ToLower() = docName.ToLower() Then
            txtFileName = moduleName
        Else
            txtFileName = docName + "_" + moduleName
        End If
        inFile = MyFileOpen(inDir + "\" + txtFileName + ".txt")

        outFile = MyFileCreate(outFilename)

        '' open template dtx file:
        Dim templateDtxFile As StreamReader = MyFileOpen(template)
        Dim dtxFile As StreamReader

        '' loop through dtx file:
        Dim nextLine As String = Nothing
        Do While templateDtxFile.Peek() <> -1 OrElse Not nextLine Is Nothing
            Dim lineTemplate As String
            If nextLine Is Nothing Then
                lineTemplate = templateDtxFile.ReadLine()
            Else
                lineTemplate = nextLine
                nextLine = Nothing
            End If
            lineTemplate.Trim()


            '' see if we found a section:
            If lineTemplate.StartsWith("@@") Then
                '' open dtx file:
                dtxFile = MyFileOpen(inDir + "\..\" + docName + ".dtx")

                '' find the corresponding section:
                Do Until dtxFile.Peek() = -1
                    Dim lineDtx As String = dtxFile.ReadLine()
                    lineDtx.Trim()

                    '' found the section, keep reading lines from files until they don't match:
                    If lineDtx.ToLower() = lineTemplate.ToLower() Then
                        Dim section As String = lineTemplate
                        section.Trim("@")

                        Dim sectionMatches As Boolean = True
                        Do Until templateDtxFile.Peek() = -1 OrElse dtxFile.Peek() = -1
                            lineTemplate = templateDtxFile.ReadLine()
                            lineTemplate.Trim()
                            lineDtx = dtxFile.ReadLine()
                            lineDtx.Trim()

                            '' make sure every line matches:
                            If lineTemplate.ToLower() <> lineDtx.ToLower() Then
                                sectionMatches = False
                                Exit Do
                            ElseIf lineTemplate.StartsWith("@@") Then

                                '' reached the next section, save the line to use for next section check:
                                nextLine = lineTemplate
                                Exit Do
                            End If
                        Loop

                        '' one ran out before the other:
                        If (templateDtxFile.Peek = -1 OrElse dtxFile.Peek() = -1) And _
                            Not (templateDtxFile.Peek = -1 And dtxFile.Peek() = -1) Then
                            sectionMatches = False
                        End If

                        '' section matches, output warning message:
                        If sectionMatches = True Then
                            outFile.WriteLine("3$" + section + "$" + WarningTypes.DTX_SECTION_NOT_FILLED_IN.ToString())
                        End If
                        Exit Do
                    End If
                Loop
                dtxFile.Close()
            End If
        Loop
        templateDtxFile.Close()

        '' read in sections (helps us look for stray sections:
        dtxFile = MyFileOpen(inDir + "\..\" + docName + ".dtx")
        Dim sectionsHash As New Hashtable
        '' find the corresponding section:
        Do Until dtxFile.Peek() = -1
            Dim line As String = dtxFile.ReadLine()
            If line.StartsWith("@@") Then
                Dim section As String = line.Trim("@")
                section = section.Replace(" ", "_")
                section = section.Replace(":", "_")
                If (sectionsHash.ContainsKey(section)) Then
                    Console.WriteLine("WARNING: " + section + " was encountered more than once")
                Else
                    sectionsHash.Add(section, section)
                End If
            End If

        Loop
        dtxFile.Close()

        '' open topic file:
        Dim hxtFilename As String = inDir + "\" + docName + ".HxT"
        If Not File.Exists(hxtFilename) Then
            hxtFilename = inDir + "\" + docName + "_help2.HxT"
        End If

        If File.Exists(hxtFilename) Then
            Dim hxtFile As StreamReader
            hxtFile = MyFileOpen(hxtFilename)

            '' loop through topics to see if there's any stray topics:
            Dim level As Integer = 0
            Do Until hxtFile.Peek() = -1
                Dim line As String = hxtFile.ReadLine()
                line = line.Trim()

                If line.StartsWith("<HelpTOCNode") Then
                    Dim split As String() = line.Split()

                    Dim title As String = Nothing
                    Dim url As String = Nothing

                    Dim inTitle As Boolean = False
                    Dim inUrl As Boolean = False

                    '' Parse the Title and Url
                    For i As Integer = 0 To split.Length - 1
                        If split(i) = "Title" Then
                            inTitle = True
                            inUrl = False
                        ElseIf split(i) = "Url" Then
                            inTitle = False
                            inUrl = True
                        ElseIf inTitle = True Then
                            If split(i).StartsWith("""") Then
                                title = split(i).Substring(1)
                            ElseIf split(i).EndsWith("""") Then
                                title = title + " " + split(i).Substring(0, split(i).Length - 1)
                                inTitle = False
                            Else
                                title = title + " " + split(i)
                            End If
                        ElseIf inUrl = True Then
                            If split(i).StartsWith("""") Then
                                url = split(i).Substring(1)
                            ElseIf split(i).EndsWith("""") Then
                                url = url + " " + split(i).Substring(0, split(i).Length - 1)
                                inUrl = False
                            Else
                                url = url + " " + split(i)
                            End If
                        End If
                    Next

                    '' increment level if we don't see the end specifier:
                    If split(split.Length - 1) = ">" Then
                        level = level + 1                        
                    ElseIf split(split.Length - 1) = "/>" Then
                        Dim section As String = title
                        section = section.Replace(" ", "_")
                        section = section.Replace(":", "_")

                        '' Check for stray topic
                        If title <> Nothing AndAlso url = Nothing AndAlso sectionsHash.ContainsKey(section) = False Then
                            outFile.WriteLine("3$" + title + "$" + WarningTypes.POSSIBLE_STRAY_TOPIC.ToString())
                        End If
                    End If
                ElseIf line.StartsWith("</HelpTOCNode>") Then
                    '' found an closing tag, so we pop a level:
                    level = level - 1
                End If
            Loop
            hxtFile.Close()
        End If

        Dim symbolDict As New Hashtable

        '' loop through contents of warnings file:
        Do Until inFile.Peek() = -1
            Dim line As String = inFile.ReadLine
            line = Trim(line)

            '' parse "the symbol" messages:
            Dim splitLine As String() = line.Split("""")
            If splitLine.Length = 3 Then
                If line.StartsWith("[404]") Then '' The symbol "????" does not have a description 
                    AddToDict(symbolDict, splitLine, WarningTypes.SYMBOL_NO_DESC)
                ElseIf line.StartsWith("[405]") Then '' One or more parameters of function "????" do not have a description
                    AddToDict(symbolDict, splitLine, WarningTypes.FUNC_PARAM_NO_DESC)
                ElseIf line.StartsWith("[411]") Then '' One or more parameters of function "????" do not have a name
                    AddToDict(symbolDict, splitLine, WarningTypes.FUNC_PARAM_NO_NAME)
                ElseIf line.StartsWith("[406]") Then '' The return value of function "????" does not have a description
                    AddToDict(symbolDict, splitLine, WarningTypes.FUNC_RETURN_VAL_NO_DESC)
                Else
                    Console.WriteLine("skipping '" + line + "'")
                End If
            End If
        Loop

        Dim comparer As New SymbolListComparer

        '' sort alphabetically:
        Dim symbolDictList As New ArrayList
        Dim dictEnum As IDictionaryEnumerator = symbolDict.GetEnumerator()
        While dictEnum.MoveNext()
            dictEnum.Value().List.Sort(comparer)
            symbolDictList.Add(dictEnum.Value())
        End While
        symbolDictList.Sort(comparer)

        '' output the symbol list:
        Dim symList As SymbolList
        For Each symList In symbolDictList
            '' symbol name has a list of member variables and functions:
            If symList.List.Count > 1 Then
                Dim symListList As SymbolEntry
                outFile.WriteLine(symList.Name)
                For Each symListList In symList.List
                    outFile.WriteLine("    " + symListList.Name)
                Next
                '' symbol is all by itself:
            ElseIf symList.List.Count = 1 Then
                Dim outputName As String = symList.List.Item(0).Name
                If outputName.StartsWith("2$") Or outputName.StartsWith("1$") Then
                    outputName = outputName.Remove(0, 2)
                    outputName = "3$" + outputName
                End If
                outFile.WriteLine(outputName)
            End If
        Next

        '' cleanup:
        inFile.Close()
        outFile.Close()
    End Sub

End Module
