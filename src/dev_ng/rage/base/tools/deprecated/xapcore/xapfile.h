#pragma once
#include <iostream>
#include <afxtempl.h>

using namespace std;

#define XENON_DVD_CACHE_SIZE    (163840) //160kb is the size of the Xenon's DVD cache as reported in several newsgroup posts
#define DVD_ROM_SECTOR_SIZE     (2048)
#define FILE_BUF_SIZE           (64*1024)

class XapException
{
public:
	XapException(const char* message) { SetMessage(message); }
	const char* GetMessage(void) { return m_Message; }

private:
	void SetMessage(const char* message)
    {
        strncpy(m_Message, message, sizeof(m_Message) - 1);
    }
	char m_Message[256];
};


class XACTGlobalSettings
{
    friend class XAPFile;

public:
    XACTGlobalSettings()
        : m_Offset(0)
        , m_Length(0)
        , m_ModifiedLoTime(0)
        , m_ModifiedHiTime(0)
    { }

	
	void    AddGuid(CString& guidString) { m_GuidList.Add(guidString); } //Adds a guid string to the GUID list
	int     GetNumGuids(void) const      { return (int) m_GuidList.GetCount(); } //Gets number of GUIDs
	CString GetGuid(int index) const     { ASSERT(index < m_GuidList.GetCount()); return m_GuidList[index]; } //Gets a GUID

	XACTGlobalSettings(const XACTGlobalSettings& settings) 
	{
		m_Offset = settings.m_Offset;
		m_Length = settings.m_Length;
		m_ModifiedLoTime = settings.m_ModifiedLoTime;
		m_ModifiedHiTime = settings.m_ModifiedHiTime;

		for (int i = 0; i < settings.GetNumGuids(); i++)
		{
			m_GuidList.Add(settings.m_GuidList[i]);
		}
	}

	const XACTGlobalSettings& operator=(const XACTGlobalSettings& settings)
	{
		m_Offset = settings.m_Offset;
		m_Length = settings.m_Length;
		m_ModifiedLoTime = settings.m_ModifiedLoTime;
		m_ModifiedHiTime = settings.m_ModifiedHiTime;

		m_GuidList.RemoveAll();

		for (int i = 0; i < settings.GetNumGuids(); i++)
		{
			m_GuidList.Add(settings.m_GuidList[i]);
		}

		return *this;
	}

private:
	CArray<CString> m_GuidList;
    ULONGLONG       m_Offset;
    ULONGLONG       m_Length;
    unsigned long   m_ModifiedLoTime;
    unsigned long   m_ModifiedHiTime;
};


class XACTWave
{
    friend class XAPFile;

public:
    XACTWave()
        : m_NumChannels(0)
        , m_SampleRate(0)
        , m_PlayLengthBytes(0)
        { }

    const CString&  GetName(void) const             { return m_Name; }
    const int       GetPlayLengthBytes(void) const  { return m_PlayLengthBytes; }
	const int		GetNumChannels(void) const		{ return m_NumChannels;	}
	const int		GetSampleRate(void) const		{ return m_SampleRate; }

    const XACTWave& operator=(const XACTWave& wave)
    {
        m_Name = wave.m_Name;
        m_NumChannels = wave.m_NumChannels;
        m_SampleRate  = wave.m_SampleRate;
        m_PlayLengthBytes = wave.m_PlayLengthBytes;
        return *this;
    }

private:
    CString m_Name;
    int  m_NumChannels;
    int  m_SampleRate;
    int  m_PlayLengthBytes;
};

class XACTSound
{
    friend class XAPFile;

public:
	XACTSound() 
		: m_LengthInMs(-1)
    { }

    const XACTSound& operator=(const XACTSound& sound)
    {
        m_Name = sound.m_Name;
		m_LengthInMs = sound.m_LengthInMs;

        return *this;
    }

    const CString&	GetName(void) const		  { return m_Name;		}
	const int		GetLengthInMs(void) const { return m_LengthInMs;}
private:
    CString	m_Name;
	int		m_LengthInMs; //NOTE: Only reflects the length of the last wave of this sound (if more than one "Wave Entry")!
};

class XACTCue
{
    friend class XAPFile;

public:
    XACTCue()
        : m_Looping(false),
		  m_LengthInMs(-1)
    { }

    const XACTCue& operator=(const XACTCue& cue)
    {
        m_Name = cue.m_Name;
        m_Looping = cue.m_Looping;
		m_LengthInMs = cue.m_LengthInMs;

        return *this;
    }

    const CString&	GetName(void) const			{ return m_Name;		}
    const bool		IsLooping(void) const		{ return m_Looping;		}
	const int		GetLenghtInMs(void) const	{ return m_LengthInMs;	}

private:
    CString	m_Name;
    bool	m_Looping;
	int		m_LengthInMs; //NOTE: Only reflects the length of the last wave of the last sound in this cue!!
};


class XACTWaveBank
{
    friend class XAPFile;
    friend class XACTSoundBank;

public:
    XACTWaveBank()
        : m_IsStreaming(false)
        , m_Offset(0)
        , m_Length(0)
    { }

    // Copy constructor to make CArray happy
    XACTWaveBank(const XACTWaveBank& wb) 
    {
        m_Name = wb.m_Name;
		m_IsStreaming = wb.m_IsStreaming;

        for (int i = 0; i < wb.GetNumWaves(); i++)
        {
            m_Waves.Add(wb.m_Waves[i]);
        }
    }

    const XACTWaveBank& operator=(const XACTWaveBank& wb)
    {
        m_Name = wb.m_Name;
		m_IsStreaming = wb.m_IsStreaming;

        m_Waves.RemoveAll();

        for (int i = 0; i < wb.GetNumWaves(); i++)
        {
            m_Waves.Add(wb.m_Waves[i]);
        }

        return *this;
    }

    const bool      IsStreaming(void) const     { return m_IsStreaming; }
    const CString&  GetName(void) const         { return m_Name; }
    const int       GetNumWaves(void) const     { return (int) m_Waves.GetCount(); }
    const XACTWave& GetWave(int index) const    { return m_Waves[index]; }

	const bool GetWaveByName(CString& waveName /* in */, XACTWave& theWave /* out */) const
	{
		bool bFound    = false;
		int  numWaves = (int) m_Waves.GetCount();

		for (int i = 0; i < numWaves; i++)
		{
			if (m_Waves[i].GetName().CompareNoCase(waveName) == 0)
			{
				bFound   = true;
				theWave = m_Waves[i];
				break;
			}
		}

		return bFound;
	}

private:
    CString     m_Name;
    bool        m_IsStreaming;
    ULONGLONG   m_Offset;
    ULONGLONG   m_Length;
    CArray<XACTWave, XACTWave> m_Waves;
};


class XACTSoundBank
{
    friend class XAPFile;

public:
    XACTSoundBank()
        : m_Offset(0)
        , m_Length(0)
    { }

    // Copy constructor to make CArray happy
    XACTSoundBank(const XACTSoundBank& sb) 
    {
        m_Name = sb.m_Name;

        for (int i = 0; i < sb.m_Sounds.GetCount(); i++)
        {
            m_Sounds.Add(sb.m_Sounds[i]);
        }

        for (int i = 0; i < sb.m_Cues.GetCount(); i++)
        {
            m_Cues.Add(sb.m_Cues[i]);
        }
    }

    const XACTSoundBank& operator=(const XACTSoundBank& sb)
    {
        m_Name = sb.m_Name;

        m_Sounds.RemoveAll();
        m_Cues.RemoveAll();

        for (int i = 0; i < sb.m_Sounds.GetCount(); i++)
        {
            m_Sounds.Add(sb.m_Sounds[i]);
        }

        for (int i = 0; i < sb.m_Cues.GetCount(); i++)
        {
            m_Cues.Add(sb.m_Cues[i]);
        }

        return *this;
    }

    ULONG CalcStreamingBufferSize(void) const
    {
        // Will return true if *any* referenced wave bank is a streaming bank
        if (!IsStreaming())
        {
            return 0;
        }

        // Go through all the waves associated with this bank and get the average play region length.
        // From that, an appropriate buffer size will be determined.
        ULONG totalWaveLengthBytes = 0;
        ULONG totalWaves = 0;

        for (int i = 0; i < m_WaveBankReferences.GetCount(); i++)
        {
            if (m_WaveBankReferences[i].IsStreaming()) //Only count streaming wave banks
            {
                for (int j = 0; j < m_WaveBankReferences[i].GetNumWaves(); j++)
                {
                    totalWaveLengthBytes += m_WaveBankReferences[i].GetWave(j).GetPlayLengthBytes();
                    totalWaves++;
                }
            }
        }

        ULONG averageStreamLength = totalWaveLengthBytes / totalWaves;

        // round up to the nearest sector size
        if (averageStreamLength % DVD_ROM_SECTOR_SIZE)
        {
            averageStreamLength += DVD_ROM_SECTOR_SIZE;
            averageStreamLength &= ~(DVD_ROM_SECTOR_SIZE - 1);
        }

        // If the average length of streams is > XENON_DVD_CACHE_SIZE, then that should be our buffer size or else we'd just
        // end up throwing away data in the DVD cache, only to ask for it again very soon.
        return (averageStreamLength >= XENON_DVD_CACHE_SIZE ? XENON_DVD_CACHE_SIZE : averageStreamLength);
    }

    const CString&  GetName(void) const { return m_Name; }

    int	GetNumCues(void) const          { return (int) m_Cues.GetCount();   }
    int	GetNumSounds(void) const        { return (int) m_Sounds.GetCount(); }
    int GetNumWaveBankRefs(void) const  { return (int) m_WaveBankReferences.GetCount(); }

    const XACTCue&      GetCue(int i) const         { ASSERT(i < m_Cues.GetCount()); return m_Cues[i];     }
    const XACTSound&    GetSound(int i) const       { ASSERT(i < m_Sounds.GetCount()); return m_Sounds[i]; }
    const XACTWaveBank& GetWaveBankRef(int i) const { ASSERT(i < m_WaveBankReferences.GetCount()); return m_WaveBankReferences[i]; }

	const bool GetSoundByName(CString& soundName /* in */, XACTSound& theSound /* out */) const
	{
		bool bFound    = false;
		int  numSounds = (int) m_Sounds.GetCount();

		for (int i = 0; i < numSounds; i++)
		{
			if (m_Sounds[i].GetName().CompareNoCase(soundName) == 0)
			{
				bFound   = true;
				theSound = m_Sounds[i];
				break;
			}
		}

		return bFound;
	}

    const bool IsStreaming(void) const
    {
        bool isStreaming = false;

        //If any wave banks referenced are streaming, we're a "streaming" sound bank.
        for (int i = 0; i < m_WaveBankReferences.GetCount(); i++)
        {
            if (m_WaveBankReferences[i].IsStreaming())
            {
                isStreaming = true;
                break;
            }
        }

        return isStreaming;
    }

private:
    CString m_Name;

    ULONGLONG   m_Offset;
    ULONGLONG   m_Length;

    CArray<XACTSound, XACTSound>        m_Sounds;
    CArray<XACTCue, XACTCue>            m_Cues;
    CArray<XACTWaveBank, XACTWaveBank>  m_WaveBankReferences; //Wave banks referenced by this sound bank
};


class XAPFile
{
public: 
    XAPFile(CStdioFile& xapFile, bool isStrict)
        : m_StdioFile(xapFile)
        , m_ParseState(PS_IN_XAP)
        , m_IoBuffer(new char [FILE_BUF_SIZE])
        , m_FoundWaveBank(false)
        , m_FoundSoundBank(false)
        , m_CurrentWaveBank(-1)
        , m_CurrentWave(-1)
        , m_CurrentSoundBank(-1)
        , m_CurrentSound(-1)
        , m_CurrentCue(-1)
    {
		m_StdioFile.SeekToBegin();

        ParseXapFile();

        if (isStrict)
        {
            CheckValidity();
        }
    }

    ~XAPFile()
    {
        delete [] m_IoBuffer;
    }

    void ReorderCues(CStdioFile& HeaderFile);
    void DumpStats(ostream& stream);

    int GetNumWaveBanks(void) const  { return (int) m_WaveBanks.GetCount();  }
    int GetNumSoundBanks(void) const { return (int) m_SoundBanks.GetCount(); }

    const XACTGlobalSettings& GetGlobalSettings(void) const { return m_GlobalSettings; }

    const XACTSoundBank& GetSoundBank(int index) const { ASSERT(index < GetNumSoundBanks()); return m_SoundBanks[index];  }
    const XACTWaveBank&  GetWaveBank(int index)  const { ASSERT(index < GetNumWaveBanks());   return m_WaveBanks[index];  }

    bool    MatchGlobalSettings(XAPFile* bankXap);
	bool	FixupGuids(XAPFile* bankXap, CStdioFile& tempFile);

    int     FindWaveBank(const CString& name);
    int     FindSoundBank(const CString& name);

    void    CreateXapWaveBank(CStdioFile& outfile);
    void    CreateXapSoundBank(CStdioFile& outfile);

    void    AppendXapWaveBank(XAPFile* bankXap, CStdioFile& outFile);
    void    AppendXapSoundBank(XAPFile* bankXap, CStdioFile& outFile);

    void    ReplaceXapWaveBank(int index, XAPFile* bankXap, CStdioFile& outFile);
    void    ReplaceXapSoundBank(int wbIndex, int sbIndex, XAPFile* bankXap, CStdioFile& outFile);

    void    WriteXapGlobalSettings(CStdioFile& outFile);
    void    WriteXapWaveBank(int index, CStdioFile& outFile);
    void    WriteXapSoundBank(int index, CStdioFile& outFile);

private:
    enum ParseState
    {
        PS_IN_XAP,
        PS_IN_GLOBAL_SETTINGS,
        PS_IN_WAVE_BANK,
        PS_IN_WAVE,
        PS_IN_SOUND_BANK,
        PS_IN_SOUND,
        PS_IN_CUE
    };

    void    ParseXapFile();

    CString GetStringValueFromXAPLine(CString& strLine);
    int     GetIntValueFromXAPLine(CString& strLine);
    long    GetLongValueFromXAPLine(CString& strLine);
    CString GetNameFromHeaderLine(CString bankName, CString& strLine);
    bool    GetCueEntryByName(CArray<XACTCue, XACTCue>& unorderedCues /*in*/, CString& cueName /*in*/, XACTCue& cueEntry /*out*/);
    void    ReorderCuesByHeaderFile(CStdioFile& HeaderFile);
    void    CheckValidity(void);

    void GotoState_InGlobalSettings(void);
    void GotoState_InWaveBank(void);
    void GotoState_InWave(void);
    void GotoState_InSoundBank(void);
    void GotoState_InSound(void);
    void GotoState_InCue(void);

    void CopyFile(CStdioFile& outFile, ULONGLONG size);
    void CopyXapFile(CStdioFile& outFile, XAPFile* xapFile, ULONGLONG size);
    void CopyXapSoundBank(CStdioFile& outFile, XAPFile* xapBank, int index);

    CStdioFile& m_StdioFile;

    ParseState  m_ParseState;

    char*   m_IoBuffer;

    bool m_FoundWaveBank;
    bool m_FoundSoundBank;

    int m_CurrentWaveBank;
    int m_CurrentWave;
    int m_CurrentSoundBank;
    int m_CurrentSound;
    int m_CurrentCue;

    CArray<XACTSoundBank, XACTSoundBank>    m_SoundBanks;
    CArray<XACTWaveBank, XACTWaveBank>      m_WaveBanks;
    XACTGlobalSettings m_GlobalSettings;
};

