#include "xapfile.h"

using namespace std;

 // Names of sounds, cues, etc. in XAP files use this identifier
static const char* NAME_FIELD_ID = "Name = ";
static const char* CUE_FIELD_ID = "XACT_CUE_";


//------------------------------------------------------------------------------
// PUBLIC METHODS
//------------------------------------------------------------------------------
void XAPFile::ReorderCues(CStdioFile& HeaderFile)
{
    ReorderCuesByHeaderFile(HeaderFile);
}

void XAPFile::DumpStats(ostream& stream) 
{
    stream << "Parsed " << GetNumWaveBanks() << " wave banks and " << GetNumSoundBanks() << " sound banks!" << endl << endl;

    for (int i = 0; i < GetNumWaveBanks(); i++)
    {
        stream << "Wave Bank: " << m_WaveBanks[i].m_Name << endl;
        stream << "  Streaming: " << (m_WaveBanks[i].m_IsStreaming ? "YES" : "NO") << endl;
        stream << "  Num Waves: " << (int) m_WaveBanks[i].m_Waves.GetCount() << endl;

        for (int j = 0; j < m_WaveBanks[i].m_Waves.GetCount(); j++)
        {
            stream << "    Wave Name:  " << m_WaveBanks[i].m_Waves[j].m_Name << endl;
            stream << "    Channels:   " << m_WaveBanks[i].m_Waves[j].m_NumChannels << endl;
            stream << "    SampleRate: " << m_WaveBanks[i].m_Waves[j].m_SampleRate << endl;
            stream << "    Length:     " << m_WaveBanks[i].m_Waves[j].m_PlayLengthBytes << endl;
            stream << endl;
        }

        stream << endl;
    }

    stream << endl;

    for (int i = 0; i < GetNumSoundBanks(); i++)
    {
        stream << "Sound Bank: " << m_SoundBanks[i].m_Name << endl;
        stream << "  Num Sounds: " << (int) m_SoundBanks[i].m_Sounds.GetCount() << endl;

        for (int j = 0; j < m_SoundBanks[i].m_Sounds.GetCount(); j++)
            stream << "    Sound Name:  " << m_SoundBanks[i].m_Sounds[j].m_Name << endl;

        stream << "  Num Cues: " << (int) m_SoundBanks[i].m_Cues.GetCount() << endl;

        for (int j = 0; j < m_SoundBanks[i].m_Cues.GetCount(); j++)
            stream << "      Cue Name:  " << m_SoundBanks[i].m_Cues[j].m_Name << endl;

        stream << endl;
    }
}


bool XAPFile::MatchGlobalSettings(XAPFile* bankXap)
{
// TODO: Cannot use this simple test until MS fixes XACT tool (so that the XACT tool does
//       not update the global settings time stamp just because you opened the XAP file.
#if 0
    return ((m_GlobalSettings.m_ModifiedLoTime == bankXap->GetGlobalSettings().m_ModifiedLoTime) &&
            (m_GlobalSettings.m_ModifiedHiTime == bankXap->GetGlobalSettings().m_ModifiedHiTime));
#else
    bool status = true;

    m_StdioFile.Seek(m_GlobalSettings.m_Offset, CFile::begin);
    bankXap->m_StdioFile.Seek(bankXap->GetGlobalSettings().m_Offset, CFile::begin);

    ULONGLONG endPosition = m_GlobalSettings.m_Offset + m_GlobalSettings.m_Length;
    while (m_StdioFile.GetPosition() < endPosition)
    {
        CString bankLine;
        CString masterLine;

        bankXap->m_StdioFile.ReadString(bankLine);
        m_StdioFile.ReadString(masterLine);

        // Check each line for a match, except for the Last Modified lines which may well not match due to MS silliness.
		// Also don't compare file names as in "File = BLAH.xgs" or "Header File = BLAH\BLAH\BLAH.h"
        if (masterLine.Find("Last Modified") == -1 && 
			masterLine.Find("File = ")		 == -1 && 
			masterLine != bankLine)
        {
            status = false;
            break;
        }
    }

    return status;
#endif
}

bool XAPFile::FixupGuids(XAPFile* bankXap, CStdioFile& tempFile)
{
	CString strLine;

	bankXap->m_StdioFile.SeekToBegin();

	while (bankXap->m_StdioFile.ReadString(strLine))
	{
		if (strLine.Find("::") != -1)
		{
			CString strCompareString = GetStringValueFromXAPLine(strLine);

			//Found a guid line so look it up in the bank xap's guid list
			int guidIndexInBankXap = -1;
			for (int i = 0; i < m_GlobalSettings.GetNumGuids(); i++)
			{
				if (strCompareString == bankXap->m_GlobalSettings.GetGuid(i))
				{
					guidIndexInBankXap = i;
					break;
				}
			}

			//Now replace the guid with the corresponding guid from the *master* project's guid list
			if (guidIndexInBankXap >= 0)
				strLine.Replace(strCompareString, m_GlobalSettings.GetGuid(guidIndexInBankXap));
			else
				return false;
		}

		tempFile.WriteString(strLine);
		tempFile.WriteString("\n");
	}

	return true;
}

int XAPFile::FindWaveBank(const CString& name)
{
    int index = -1;

    for (int i = 0; i < GetNumWaveBanks(); i++)
    {
        if (m_WaveBanks[i].m_Name == name)
        {
            index = i;
            break;
        }
    }

    return index;
}


int XAPFile::FindSoundBank(const CString& name)
{
    int index = -1;

    for (int i = 0; i < GetNumSoundBanks(); i++)
    {
        if (m_SoundBanks[i].m_Name == name)
        {
            index = i;
            break;
        }
    }

    return index;
}


void XAPFile::CreateXapWaveBank(CStdioFile& outFile)
{
    CString name;

    outFile.WriteString("Wave Bank\r\n");
    outFile.WriteString("{\r\n");

    name.Format("    Name = %s;\r\n", outFile.GetFileTitle());
    if (name.Replace(".xap", "") == 0)
		name.Replace(".XAP", "");

    outFile.WriteString(name);

    name.Format("    File = %s;\r\n", outFile.GetFilePath());
    if (name.Replace(".xap", ".xwb") == 0)
		name.Replace(".XAP", ".xwb");

    outFile.WriteString(name);

    outFile.WriteString("}\r\n\r\n");
}


void XAPFile::CreateXapSoundBank(CStdioFile& outFile)
{
    CString name;

    outFile.WriteString("Sound Bank\r\n");
    outFile.WriteString("{\r\n");

    name.Format("    Name = %s;\r\n", outFile.GetFileTitle());
    if (name.Replace(".xap", "") == 0)
		name.Replace(".XAP", "");

    outFile.WriteString(name);

    name.Format("    File = %s;\r\n", outFile.GetFilePath());
    if (name.Replace(".xap", ".xsb") == 0)
		name.Replace(".XAP", ".xsb");

    outFile.WriteString(name);

    outFile.WriteString("}\r\n\r\n");
}


void XAPFile::AppendXapWaveBank(XAPFile* bankXap, CStdioFile& outFile)
{
    // copy existing wave banks to output file
    m_StdioFile.Seek(0, CFile::begin);
    CopyFile(outFile, m_WaveBanks[GetNumWaveBanks() - 1].m_Offset + m_WaveBanks[GetNumWaveBanks() - 1].m_Length);

    outFile.WriteString("\r\nWave Bank\r\n");

    // append the new wave bank
    bankXap->m_StdioFile.Seek(bankXap->GetWaveBank(0).m_Offset, CFile::begin);
    CopyXapFile(outFile, bankXap, bankXap->GetWaveBank(0).m_Length);
}


void XAPFile::AppendXapSoundBank(XAPFile* bankXap, CStdioFile& outFile)
{
    // copy existing sound banks to output file
    CopyFile(outFile, m_StdioFile.GetLength() - m_StdioFile.GetPosition());

    outFile.WriteString("\r\nSound Bank\r\n");

    // append the new sound bank
    CopyXapSoundBank(outFile, bankXap, GetNumWaveBanks());
}


void XAPFile::ReplaceXapWaveBank(int index, XAPFile* bankXap, CStdioFile& outFile)
{
    // copy existing file from the beginning to the offset of the wave bank being replaced
    m_StdioFile.Seek(0, CFile::begin);
    CopyFile(outFile, m_WaveBanks[index].m_Offset);

    // copy the replacement wave bank
    bankXap->m_StdioFile.Seek(bankXap->GetWaveBank(0).m_Offset, CFile::begin);
    CopyXapFile(outFile, bankXap, bankXap->GetWaveBank(0).m_Length);

    // copy the remaining existing wave banks
    m_StdioFile.Seek(m_WaveBanks[index].m_Offset + m_WaveBanks[index].m_Length, CFile::begin);
    if (GetNumWaveBanks() > 1)
    {
        CopyFile(outFile, (m_WaveBanks[GetNumWaveBanks() - 1].m_Offset + m_WaveBanks[GetNumWaveBanks() - 1].m_Length) - m_StdioFile.GetPosition());
    }
}


void XAPFile::ReplaceXapSoundBank(int wbIndex, int sbIndex, XAPFile* bankXap, CStdioFile& outFile)
{
    // copy existing sound banks
    CopyFile(outFile, m_SoundBanks[sbIndex].m_Offset - m_StdioFile.GetPosition());

    // copy the replacement sound bank
    CopyXapSoundBank(outFile, bankXap, wbIndex);

    // copy the remaining sound banks
    if (GetNumSoundBanks() > 1)
    {
        m_StdioFile.Seek(m_SoundBanks[sbIndex].m_Offset + m_SoundBanks[sbIndex].m_Length, CFile::begin);
        CopyFile(outFile, m_StdioFile.GetLength() - m_StdioFile.GetPosition());
    }
}


void XAPFile::WriteXapGlobalSettings(CStdioFile& outFile)
{
    outFile.WriteString("Signature = XACT2;\r\n");
    outFile.WriteString("Version = 4;\r\n\r\n");
    outFile.WriteString("Options\r\n{\r\n}\r\n\r\n");
    outFile.WriteString("Global Settings\r\n");

    m_StdioFile.Seek(m_GlobalSettings.m_Offset, CFile::begin);
    CopyFile(outFile, m_GlobalSettings.m_Length);

    outFile.WriteString("\r\n");
}


void XAPFile::WriteXapWaveBank(int index, CStdioFile& outFile)
{
    outFile.WriteString("Wave Bank\r\n");

    m_StdioFile.Seek(m_WaveBanks[index].m_Offset, CFile::begin);
    CopyFile(outFile, m_WaveBanks[index].m_Length);

    outFile.WriteString("\r\n");
}


void XAPFile::WriteXapSoundBank(int index, CStdioFile& outFile)
{
    outFile.WriteString("Sound Bank\r\n");

    m_StdioFile.Seek(m_SoundBanks[index].m_Offset, CFile::begin);
    while (m_StdioFile.GetPosition() - m_SoundBanks[index].m_Offset < m_SoundBanks[index].m_Length)
    {
        CString line;
        m_StdioFile.ReadString(m_IoBuffer, FILE_BUF_SIZE);
        line = m_IoBuffer;
        if (line.Find("Bank Index") != -1)
        {
            int pos = 0;
            line = line.Tokenize("=;", pos);
            line.Append("= 0;\r\n");
            outFile.WriteString(line);
        }
        else
        {
            outFile.WriteString(m_IoBuffer);
        }
    }
}


//------------------------------------------------------------------------------
// PRIVATE METHODS
//------------------------------------------------------------------------------
void XAPFile::ParseXapFile()
{
    CString strLine;
    int indentLevel = 0;

    while (m_StdioFile.ReadString(strLine))
    {
        strLine.Trim();  // remove indentation and line feed

        if (strLine == "{")
        {
            indentLevel++;
        }

        if (strLine == "}")
        {
            indentLevel--;
        }

        switch (m_ParseState)
        {
        case PS_IN_XAP:
            if ((strLine == "Global Settings") && (indentLevel == 0))
            {
                GotoState_InGlobalSettings();
            }
            else if ((strLine == "Wave Bank") && (indentLevel == 0))
            {
                GotoState_InWaveBank();
            }
            else if ((strLine == "Sound Bank") && (indentLevel == 0))
            {
                GotoState_InSoundBank();
            }
            break;

        case PS_IN_GLOBAL_SETTINGS:
            if ((strLine == "Last Modified Low") && (indentLevel == 1))
            {
                m_GlobalSettings.m_ModifiedLoTime = GetLongValueFromXAPLine(strLine);
            }
            else if ((strLine == "Last Modified High") && (indentLevel == 1))
            {
                m_GlobalSettings.m_ModifiedHiTime = GetLongValueFromXAPLine(strLine);
            }
			else if ((strLine.Find("::") != -1))
			{
				m_GlobalSettings.AddGuid(GetStringValueFromXAPLine(strLine));
			}
            else if ((strLine == "}") && (indentLevel == 0))
            {
                m_GlobalSettings.m_Length = m_StdioFile.GetPosition() - m_GlobalSettings.m_Offset;
                m_ParseState = PS_IN_XAP;
            }
            break;
 
        case PS_IN_WAVE_BANK:
            if ((strLine.Find(NAME_FIELD_ID) == 0) && (indentLevel == 1))
            {
                m_WaveBanks[m_CurrentWaveBank].m_Name = GetStringValueFromXAPLine(strLine);
            }
            else if ((strLine == "Streaming = 1;") && (indentLevel == 1)) //See if the streaming flag is set
            {
                m_WaveBanks[m_CurrentWaveBank].m_IsStreaming = true;
            }
            else if ((strLine == "Wave") && (indentLevel == 1))
            {
                GotoState_InWave();
            }
            else if ((strLine == "}") && (indentLevel == 0))
            {
                m_WaveBanks[m_CurrentWaveBank].m_Length = m_StdioFile.GetPosition() - m_WaveBanks[m_CurrentWaveBank].m_Offset;
                m_ParseState = PS_IN_XAP;
            }
            break;

        case PS_IN_WAVE:
            if ((strLine.Find(NAME_FIELD_ID) == 0) && (indentLevel == 2))
            {
                m_WaveBanks[m_CurrentWaveBank].m_Waves[m_CurrentWave].m_Name = GetStringValueFromXAPLine(strLine);
            }
            else if ((strLine.Find("Sampling Rate = ") == 0) && (indentLevel == 3))
            {
                m_WaveBanks[m_CurrentWaveBank].m_Waves[m_CurrentWave].m_SampleRate = GetIntValueFromXAPLine(strLine);
            }
            else if ((strLine.Find("Channels = ") == 0) && (indentLevel == 3))
            {
                m_WaveBanks[m_CurrentWaveBank].m_Waves[m_CurrentWave].m_NumChannels = GetIntValueFromXAPLine(strLine);
            }
            else if ((strLine.Find("Play Region Length = ") == 0) && (indentLevel == 3))
            {
                m_WaveBanks[m_CurrentWaveBank].m_Waves[m_CurrentWave].m_PlayLengthBytes = GetIntValueFromXAPLine(strLine);
            }
            else if ((strLine == "}") && (indentLevel == 1))
            {
                m_ParseState = PS_IN_WAVE_BANK;
            }
            break;

        case PS_IN_SOUND_BANK:
            if ((strLine.Find(NAME_FIELD_ID) == 0) && (indentLevel == 1))
            {
                m_SoundBanks[m_CurrentSoundBank].m_Name = GetStringValueFromXAPLine(strLine);
            }
            else if ((strLine == "Sound") && (indentLevel == 1))
            {
                GotoState_InSound();
            }
            else if ((strLine == "Cue") && (indentLevel == 1))
            {
                GotoState_InCue();
            }
            else if ((strLine == "}") && (indentLevel == 0))
            {
                m_SoundBanks[m_CurrentSoundBank].m_Length = m_StdioFile.GetPosition() - m_SoundBanks[m_CurrentSoundBank].m_Offset;
                m_ParseState = PS_IN_XAP;
            }
            break;

        case PS_IN_SOUND:
            if ((strLine.Find(NAME_FIELD_ID) == 0) && (indentLevel == 2))
            {
                m_SoundBanks[m_CurrentSoundBank].m_Sounds[m_CurrentSound].m_Name = GetStringValueFromXAPLine(strLine);
            }
            else if ((strLine.Find("Bank Name = ") == 0) && (indentLevel == 5))
            {
                //Record the wave banks that this sound bank references.  This assumes that wave banks will always appear
                //before any sound banks in an .xap file, thus we will have already catalogued all wave banks prior to 
                //getting here.
                CString waveBankName = GetStringValueFromXAPLine(strLine);

                bool AlreadyReferenced = false;
                for (int i = 0; i < m_SoundBanks[m_CurrentSoundBank].m_WaveBankReferences.GetCount(); i++)
                {
                    if (m_SoundBanks[m_CurrentSoundBank].m_WaveBankReferences[i].GetName() == waveBankName)
                    {
                        AlreadyReferenced = true;
                        break;
                    }
                }

                if (!AlreadyReferenced)
                {
                    for (int i = 0; i < GetNumWaveBanks(); i++)
                    {
                        if (m_WaveBanks[i].GetName() == waveBankName)
                        {
                            m_SoundBanks[m_CurrentSoundBank].m_WaveBankReferences.Add(m_WaveBanks[i]);
                            break;
                        }
                    }
                }
            }
			else if ((strLine.Find("Entry Name = ") == 0) && (indentLevel == 5))
			{
				//Figure out the length of this sound.  Note that this is the length of the LAST wave of 
				//the sound (assuming more than one "Wave Entry" section)!!
				CString waveName = GetStringValueFromXAPLine(strLine);

				XACTWave wave;

				int numWaveBanks = (int) m_WaveBanks.GetCount();
				bool bFound = false;

				int waveBankIndex = -1;

				//Find the corresponding wave bank for this sound bank
				for (int i = 0; i < numWaveBanks; i++)
				{
					if (m_WaveBanks[i].GetName().CompareNoCase(m_SoundBanks[m_CurrentSoundBank].GetName()) == 0)
					{
						waveBankIndex = i;
						break;
					}
				}

				if (waveBankIndex != -1 && m_WaveBanks[waveBankIndex].GetWaveByName(waveName, wave))
					bFound = true;
			
				if (!bFound)
				{
					CString error;

					if (waveBankIndex == -1)
						error.Format("Unable to find matching wave bank for sound bank %s", m_SoundBanks[m_CurrentSoundBank].GetName());
					else
						error.Format("Unable to find wave %s in any of the %d wave banks", waveName, numWaveBanks);

					throw XapException((LPCSTR)error);
				}

				double bytesPerSecond = 2.0 /* assumes 16-bit source*/ * wave.GetNumChannels() * wave.GetSampleRate();

				int lengthInMs = (int) ((wave.GetPlayLengthBytes() / bytesPerSecond) * 1000.0);

				m_SoundBanks[m_CurrentSoundBank].m_Sounds[m_CurrentSound].m_LengthInMs = lengthInMs;
			}
            else if ((strLine == "}") && (indentLevel == 1))
            {
                m_ParseState = PS_IN_SOUND_BANK;
            }
            break;

        case PS_IN_CUE:
            if ((strLine.Find(NAME_FIELD_ID) == 0) && (indentLevel == 2))
            {
                XACTCue	cue;
                CString cueName = GetStringValueFromXAPLine(strLine);

                m_SoundBanks[m_CurrentSoundBank].m_Cues[m_CurrentCue].m_Name = cueName;

                //Relying on cue name to determine looping because with the complex scripting available,
                //it would be very difficult if not impossible to automatically detect looping sounds
                //correctly 100% of the time.  Perhaps at some point, XACT 2 will offer an easy way 
                //(ie. checkbox or something) for the sound designers to specify looping/non-looping.
                bool Looping = (cueName.Find("_LP") != -1) || (cueName.Find("_LOOP") != -1) || 
                               (cueName.Find("_Lp") != -1) || (cueName.Find("_Loop") != -1) ||
                               (cueName.Find("_lp") != -1) || (cueName.Find("_loop") != -1);

                m_SoundBanks[m_CurrentSoundBank].m_Cues[m_CurrentCue].m_Looping = Looping;
            }
			else if ((strLine.Find(NAME_FIELD_ID) == 0) && (indentLevel == 3))
			{
				//In the "Sound Entry" section of the cue
				CString soundName = GetStringValueFromXAPLine(strLine);
				
				XACTSound sound;

				if (!m_SoundBanks[m_CurrentSoundBank].GetSoundByName(soundName, sound))
				{
					CString error;
					error.Format("Unable to find sound %s in sound bank %s", soundName, m_SoundBanks[m_CurrentSoundBank].GetName());
					throw XapException((LPCSTR)error);
				}

				m_SoundBanks[m_CurrentSoundBank].m_Cues[m_CurrentCue].m_LengthInMs = sound.GetLengthInMs();
			}
            else if ((strLine == "}") && (indentLevel == 1))
            {
                m_ParseState = PS_IN_SOUND_BANK;
            }
            break;

        default:
            throw XapException("Got into an unknown state parsing .xap file!");
        }
    }
}


void XAPFile::GotoState_InGlobalSettings(void)
{
    m_GlobalSettings.m_Offset = m_StdioFile.GetPosition();
    m_ParseState = PS_IN_GLOBAL_SETTINGS;
}


void XAPFile::GotoState_InWaveBank(void)
{
    //Add a wave bank to the array
    XACTWaveBank waveBank;
    m_WaveBanks.Add(waveBank);

    m_CurrentWaveBank++;
    m_CurrentWave = -1;

    m_WaveBanks[m_CurrentWaveBank].m_Offset = m_StdioFile.GetPosition();

    m_ParseState = PS_IN_WAVE_BANK;
}


void XAPFile::GotoState_InSoundBank(void)
{
    //Add a new sound bank to the array
    XACTSoundBank soundBank;
    m_SoundBanks.Add(soundBank);

    m_CurrentSoundBank++;
    m_CurrentSound = -1;
    m_CurrentCue = -1;

    m_SoundBanks[m_CurrentSoundBank].m_Offset = m_StdioFile.GetPosition();

    m_ParseState = PS_IN_SOUND_BANK;
}

void XAPFile::GotoState_InWave(void)
{
    //Add another wave to the wave bank
    XACTWave wave;
    m_WaveBanks[m_CurrentWaveBank].m_Waves.Add(wave);

    m_CurrentWave++;

    m_ParseState = PS_IN_WAVE;
}

void XAPFile::GotoState_InSound(void)
{
    //Add another sound to the sound bank
    XACTSound sound;
    m_SoundBanks[m_CurrentSoundBank].m_Sounds.Add(sound);

    m_CurrentSound++;

    m_ParseState = PS_IN_SOUND;
}

void XAPFile::GotoState_InCue(void)
{
    //Add another cue to the sound bank
    XACTCue cue;
    m_SoundBanks[m_CurrentSoundBank].m_Cues.Add(cue);

    m_CurrentCue++;

    m_ParseState = PS_IN_CUE;
}


void XAPFile::ReorderCuesByHeaderFile(CStdioFile& HeaderFile)
{
    bool bSuccess = true;
    CString strLine;

    for (int i = 0; i < m_SoundBanks.GetCount(); i++)
    {
        //Move all the cues in this sound bank into a temp array and empty the original.  We'll then
        //fill the original back up with the correct order.
        CArray<XACTCue, XACTCue> UnorderedCues;

        int numCuesInSoundBank = (int) m_SoundBanks[i].m_Cues.GetCount();
        int numCuesInHeader    = 0;

        for (int j = 0; j < m_SoundBanks[i].m_Cues.GetCount(); j++)
            UnorderedCues.Add(m_SoundBanks[i].m_Cues[j]);

        m_SoundBanks[i].m_Cues.RemoveAll();

        CString cueFieldId = CUE_FIELD_ID + m_SoundBanks[i].m_Name + CString("_");
		
		cueFieldId.MakeUpper(); //enums are all upper case, but the name of the bank may not be

        while(HeaderFile.ReadString(strLine))
        {
            if (strLine.Find(cueFieldId) != -1 && strLine.Find("}") == -1 && strLine.Find("#define") == -1) 
            {
                numCuesInHeader++;

                XACTCue	cue;
                CString cueName = GetNameFromHeaderLine(m_SoundBanks[i].m_Name, strLine);

                if (GetCueEntryByName(UnorderedCues, cueName, cue))
                {
                    m_SoundBanks[i].m_Cues.Add(cue);
                }
                else
                {
                    CString error;
                    error.Format("Unable to find cue %s in sound bank %s", cueName, m_SoundBanks[i].GetName());
                    throw XapException((LPCSTR)error);
                }
            }
        }

        if (numCuesInHeader != numCuesInSoundBank)
        {
            CString error;
            error.Format("Found %d cues in sound bank '%s' but %d corresponding cues in header file enum!", numCuesInSoundBank, m_SoundBanks[i].GetName(), numCuesInHeader);
            throw XapException((LPCSTR)error);
        }

        HeaderFile.SeekToBegin();
    }
}

CString XAPFile::GetStringValueFromXAPLine(CString& strLine)
{
    int pos = 0;
    CString field, value;

    field = strLine.Tokenize("=;", pos);
    value = strLine.Tokenize("=;", pos);
    value.TrimLeft();

    return value;
}

int XAPFile::GetIntValueFromXAPLine(CString& strLine)
{
    int pos = 0;
    CString field, value;

    field = strLine.Tokenize("=;", pos);
    value = strLine.Tokenize("=;", pos);
    value.TrimLeft();

    return atoi((LPCSTR)value);
}

long XAPFile::GetLongValueFromXAPLine(CString& strLine)
{
    int pos = 0;
    CString field, value;

    field = strLine.Tokenize("=;", pos);
    value = strLine.Tokenize("=;", pos);
    value.TrimLeft();

    return atol((LPCSTR)value);
}

CString XAPFile::GetNameFromHeaderLine(CString bankName, CString& strLine)
{
    CString strName = strLine;

    strName.TrimLeft("\t "); //Remove tabs and spaces from the front of the line

    int theEnd = strName.Find(" "); //Since we trimmed whitespace from the left, the first space indicates the end of the name

    int charsToRemoveFromRight = strName.GetLength() - theEnd;
    int charsToRemoveFromLeft  = (CString(CUE_FIELD_ID).GetLength() + (bankName.GetLength() + 1 /* for the _ */));

    //Trim the cue field id off the left
    strName = strName.Right(strName.GetLength() - charsToRemoveFromLeft); //isolate just the name

    //Trim the " = n," crap from the end
    strName = strName.Left(strName.GetLength() - charsToRemoveFromRight);

    return strName;
}

bool XAPFile::GetCueEntryByName(CArray<XACTCue, XACTCue>& unorderedCues /*in*/, CString& cueName /*in*/, XACTCue& cueEntry /*out*/)
{
    bool bFound = false;

    for (int i = 0; i < unorderedCues.GetCount(); i++)
    {
        //Make sure we do a case-insensitive compare since the enums are always UPPER case
        if (unorderedCues[i].m_Name.CompareNoCase(cueName) == 0)
        {
            cueEntry = unorderedCues[i];
            bFound = true;
            break;
        }
    }

    return bFound;
}

// Until cross references between wave and sound banks are supported,
// provide some consistency checking so this tool doesn't explode :)
void XAPFile::CheckValidity(void)
{
    if (m_SoundBanks.GetCount() != m_WaveBanks.GetCount())
    {
        CString error;

        if (m_SoundBanks.GetCount() > m_WaveBanks.GetCount())
        {
            error.Format( "Found %d sound banks but only %d wave banks!", m_SoundBanks.GetCount(), m_WaveBanks.GetCount());
        }
        else
        {
            error.Format( "Found %d wave banks but only %d sound banks!", m_WaveBanks.GetCount(), m_SoundBanks.GetCount());
        }

        throw XapException((LPCSTR)error);
    }

    // Verify that each sound bank has a single wave bank reference
    for (int i = 0; i < m_SoundBanks.GetCount(); i++)
    {
        if (m_SoundBanks[i].m_WaveBankReferences.GetCount() != 1)
        {
            CString error;
            error.Format("Sound bank '%s' has a wave bank reference count != 1!", m_SoundBanks[i].GetName());
            throw XapException((LPCSTR)error);
        }
    }

    //Verify that each sound bank's referenced wave bank has the same name
    for (int i = 0; i < m_SoundBanks.GetCount(); i++)
    {
        if (m_SoundBanks[i].m_WaveBankReferences[0].GetName() != m_SoundBanks[i].GetName())
        {
            CString error;
            error.Format("Sound bank '%s' references wave bank '%s'!", m_SoundBanks[i].GetName(), m_SoundBanks[i].m_WaveBankReferences[0].GetName());
            throw XapException((LPCSTR)error);
        }
    }
}


void XAPFile::CopyFile(CStdioFile& outFile, ULONGLONG size)
{
    while (size)
    {
        UINT count = (UINT)size;
        if (count > FILE_BUF_SIZE)
        {
            count = FILE_BUF_SIZE;
        }
        size -= count;

        count = m_StdioFile.Read(m_IoBuffer, count);
        outFile.Write(m_IoBuffer, count);
    }
}


void XAPFile::CopyXapFile(CStdioFile& outFile, XAPFile* xapFile, ULONGLONG size)
{
    while (size)
    {
        UINT count = (UINT)size;
        if (count > FILE_BUF_SIZE)
        {
            count = FILE_BUF_SIZE;
        }
        size -= count;

        count = xapFile->m_StdioFile.Read(m_IoBuffer, count);
        outFile.Write(m_IoBuffer, count);
    }
}


void XAPFile::CopyXapSoundBank(CStdioFile& outFile, XAPFile* bankXap, int index)
{
    bankXap->m_StdioFile.Seek(bankXap->GetSoundBank(0).m_Offset, CFile::begin);
    while (bankXap->m_StdioFile.GetPosition() - bankXap->GetSoundBank(0).m_Offset < bankXap->GetSoundBank(0).m_Length)
    {
        CString line;
        bankXap->m_StdioFile.ReadString(m_IoBuffer, FILE_BUF_SIZE);
        line = m_IoBuffer;
        if (line.Find("Bank Index") != -1)
        {
            int pos = 0;
            line = line.Tokenize("=;", pos);
            line.AppendFormat("= %d;\r\n", index);  // bank index fix-up
            outFile.WriteString(line);
        }
        else
        {
            outFile.WriteString(m_IoBuffer);
        }
    }
}
