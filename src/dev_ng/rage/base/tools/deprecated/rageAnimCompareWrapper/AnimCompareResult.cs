using System;

namespace rageAnimCompareWrapper
{
	/// <summary>
	/// Summary description for AnimCompareResult.
	/// </summary>
	public class AnimCompareResult  : IComparable
	{
		/// <summary>
		/// IComparable.CompareTo implementation.
		/// </summary>
		public int CompareTo(object obj) 
		{
			if(obj is AnimCompareResult) 
			{
				AnimCompareResult temp = (AnimCompareResult) obj;

				return m_fSimilarity.CompareTo(temp.Similarity);
			}
        
			throw new ArgumentException("object is not a AnimCompareResult");    
		}

		// Constructor
		public AnimCompareResult(string strCSVOfResult)
		{
			// Split the string on ,
			string[] astrStringParts = strCSVOfResult.Split(",".ToCharArray());
			m_strSourceFile	= astrStringParts[0];
			m_fSourceTime	= Decimal.Parse(astrStringParts[1]);
			m_strDestFile	= astrStringParts[2];
			m_fDestTime		= Decimal.Parse(astrStringParts[3]);
			m_fSimilarity	= Decimal.Parse(astrStringParts[4]);
		}

		private string m_strSourceFile;
		public string SourceFile 
		{
			get 
			{
				return m_strSourceFile; 
			}
		}
		private decimal m_fSourceTime;
		public decimal SourceTime 
		{
			get 
			{
				return m_fSourceTime; 
			}
		}
		private string m_strDestFile;
		public string DestFile 
		{
			get 
			{
				return m_strDestFile; 
			}
		}
		private decimal m_fDestTime;
		public decimal DestTime 
		{
			get 
			{
				return m_fDestTime; 
			}
		}
		private decimal m_fSimilarity;
		public decimal Similarity 
		{
			get 
			{
				return m_fSimilarity; 
			}
		}
	}
}
