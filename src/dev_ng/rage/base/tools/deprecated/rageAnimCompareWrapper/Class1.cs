using System;
using System.IO;
using System.Collections;
using rageUsefulCSharpToolClasses;

namespace rageAnimCompareWrapper
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		static void OutputUsageInfo()
		{
			Console.WriteLine("Usage : rageAnimCompareWrapper <file name>");
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			// Got good params?
			if((args.GetLength(0) != 1) || (!File.Exists(args[0])))
			{
				OutputUsageInfo();
				return;
			}

			// Open the given file
			string strFilename = args[0];
			StreamReader sr = File.OpenText(strFilename);

			// Parse file
			string strInput;
			rageStatus obStatus;
			ArrayList obAObAnimCompareResults = new ArrayList();
			while ((strInput=sr.ReadLine())!=null) 
			{
				// Format of the file is like this:
				//	char_austria\char_austria.skel,m_skp_for_WAL.anim,0.0,m_skp_for_WAL.anim,1.0
				//	char_austria\char_austria.skel,m_skp_for_WAL.anim,0.0,m_trn_skp_for_skp_stn_WAL.anim,0.0
				//	char_austria\char_austria.skel,m_trn_skp_for_skp_stn_WAL.anim,1.0,rdy_stn_WAL_01.anim,0.0
				//	char_austria\char_austria.skel,rdy_stn_WAL_01.anim,0.0,rdy_stn_WAL_01.anim,1.0
				//	char_austria\char_austria.skel,rdy_stn_WAL_01.anim,0.0,m_trn_skp_stn_skp_for_WAL.anim,0.0
				//	char_austria\char_austria.skel,m_trn_skp_stn_skp_for_WAL.anim,1.0,m_skp_skp_for_WAL.anim,0.0
				//	char_austria\char_austria.skel,rdy_stn_WAL_01.anim,0.0,m_gameidle_003.anim,0.0
				//	char_austria\char_austria.skel,m_gameidle_003.anim,0.0,m_gameidle_003.anim,1.0
				//	...
				//
				// So split line on commas

				string[] astrLineParts = strInput.Split(",".ToCharArray());
				if(astrLineParts.GetLength(0) == 5)
				{
					string strSkeletonFilename	= astrLineParts[0];
					string strAnim1Filename		= astrLineParts[1];
					string strAnim1Phase		= astrLineParts[2];
					string strAnim2Filename		= astrLineParts[3];
					string strAnim2Phase		= astrLineParts[4];

					// Execute comparer
					rageExecuteCommand obCommand = new rageExecuteCommand();
					obCommand.Command = "animcompare";
					obCommand.Arguments = "-csv -noroot -skel="+ strSkeletonFilename +" -anim="+ strAnim1Filename +";phase="+ strAnim1Phase +","+ strAnim2Filename +";phase="+ strAnim2Phase;
					obCommand.EchoToConsole = false;
					// obCommand.EchoToConsole = true;
					obCommand.UseBusySpinner = false;
					obCommand.UpdateLogFileInRealTime = false;
					obCommand.Execute(out obStatus);

					if(obStatus.Success())
					{
						// Get the results
//						Console.WriteLine(obCommand.GetDosCommand());
						ArrayList obAStrResults = obCommand.GetLogAsArray();
//						foreach(string strResult in obAStrResults)
//						{
//							Console.WriteLine(strResult);
//						}
						obAObAnimCompareResults.Add(new AnimCompareResult(obAStrResults[obAStrResults.Count - 1] as string));
					}
				}
			}
			sr.Close();
			obAObAnimCompareResults.Sort();
			obAObAnimCompareResults.Reverse();

			// Output a sorted list
			Console.WriteLine("Similarity,SourceFilename,SourceTime,DestFilename,DestTime");
			foreach(AnimCompareResult obResult in obAObAnimCompareResults)
			{
				Console.WriteLine(obResult.Similarity+","+obResult.SourceFile+","+obResult.SourceTime+","+obResult.DestFile+","+obResult.DestTime);
			}
		}
	}
}
