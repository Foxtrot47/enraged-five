// 
// uvOptimize/uvOptimize.h 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 

#ifndef UVOPTIMIZE_UVOPTIMIZE_H
#define UVOPTIMIZE_UVOPTIMIZE_H

//#include "deedge.h"
//#include "devertex.h"

#include "atl/array.h"
#include "spatialdata/grid2d.h"
#include "zlib/zconf.h"
#include "mesh/mesh.h"


#include <limits.h>
#include <vector>



namespace rage {

	struct mshMaterial;
	class mshMesh;


	class TextureEntry
	{
	public:
		char	Name[64];
		char	Path[256];
		uLong	Crc;
		Bytef	*Bits;
		int		Size;

		// texture usage extents (not normalized!)
		float	u_min;
		float	u_max;
		float	v_min;
		float	v_max;

		int	texAddressModeBits_u;// texture may be addressed in more than one mode so this is a bitfield
		int	texAddressModeBits_v;// texture may be addressed in more than one mode so this is a bitfield

		// normalized version - after determining proper extents based on texture addressing modes used with uv coords.
		float	u_min_norm;
		float	u_max_norm;
		float	v_min_norm;
		float	v_max_norm;
	};

	class uvOptimizer
	{
	public:
		uvOptimizer();
		~uvOptimizer();

		// list of textures 'with extension' that should we should skip uv optimizing.
		// global textures cannot be spatially optimized because the list of meshes to be loaded and have thier uv sets checked are not the only 'owners' of global textures.
		static bool LoadGlobalTexlist( const char *filename );

		// list of entities+meshes+textures that we analyze as if it was all one object.
		// we take traverse each mesh and build a uv extent for each texture.
		// then we union the uv sets.
		static bool LoadEntityList( const char *filename );

		// Returns how much memory would be consumed adding all unique entries from the listfil to the destination texture entry array.
		static u32  BuildTextureDictionaryFromList( atArray<TextureEntry>& DestList, const char *texlist, const bool testExcludeList, atArray<TextureEntry>& ExcludeList ); // , const char *outputPath, const char *rscName );

		static bool	TextureExists( const char *str, atArray<TextureEntry>& searchList );


		// local texture list - list of uniquely named textures built from parsing all the .sva files from all the entity.type files loaded by 'LoadEntityList'.
		//   we also need to make sure the list was filtered against the 'LoadGlobalTexlist' as well.
	};


	class uvMesh
	{
	public:

		uvMesh();
		~uvMesh();

		// Load optimization package.
		// - load a list of 

		// PURPOSE: Creates a mesh from an rmcDrawable entity (entity.type + mesh files + shader variable files)
		static uvMesh* CreateFromEntity(const char* filename);

		// Can load a mshMesh, a uvMesh, or an entity.type
		static uvMesh* CreateFromFile(const char* filename);


		void RemoveUnusedMaterials();

		struct MaterialData {
			mshMaterial* m_Material;
			ConstString m_ShaderPreset;		// name of the .sps file
			ConstString m_ShaderVariables;	// contents of the .sva file

			// new data (cmc)
			ConstString m_ShaderVarFilename;// name of the .sva file?


			MaterialData()
				: m_Material(NULL)
			{
			}

			MaterialData& operator=(const MaterialData& other) {
				if (!m_Material && other.m_Material) {
					m_Material = new mshMaterial;
				}
				other.m_Material->CopyNongeometricData(*this->m_Material);
				m_ShaderPreset = other.m_ShaderPreset;
				m_ShaderVariables = other.m_ShaderVariables;
				return *this;
			}

			bool operator==(const MaterialData& other) {
				Assert(m_Material && other.m_Material);
				if(!(*m_Material == *other.m_Material)) {
					return false;
				}
				if (!(m_ShaderPreset == other.m_ShaderPreset)) {
					return false;
				}
				if (!(m_ShaderVariables == other.m_ShaderVariables)) {
					return false;
				}
				return true;
			}
		};

		atArray<MaterialData> m_Materials; // materials w/o primitives, verts or channel data
	};

} // namespace rage

#endif
