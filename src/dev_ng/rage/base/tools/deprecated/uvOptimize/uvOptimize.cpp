// 
// uvOptimize/uvOptimize.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 
#include "uvOptimize.h"

#include "atl/array.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/token.h"
#include "grcore/texture.h"
#include "grcore/texturedefault.h"
#include "grmodel/modelfactory.h"
#include "grmodel/geometry.h"

#include "phcore/materialmgrimpl.h"
#include "rmcore/drawable.h"
#include "rmcore/typefileparser.h"
#include "system/param.h"
#include "system/timer.h"
#include "vector/matrix34.h"
#include "vector/quaternion.h"
#include "zlib/zlib.h"

#define	TEXTURE_DICTIONARY_LISTFILE_VERSION 1
using namespace rage;


bool	uvOptimizer::TextureExists( const char *str, atArray<TextureEntry>& searchList )
{
	for (int i=0; i<searchList.GetCount(); i++) 
	{
		if(strcmp( searchList[i].Name, str ) == 0)
			return true;
	}

	return false;
}

u32 uvOptimizer::BuildTextureDictionaryFromList( atArray<TextureEntry>& DestList, const char *texlist, const bool testExcludeList, atArray<TextureEntry>& ExcludeList )
{
#if 0
	----sample file format----
		version 1
		texcount 2

T:\rdr2\Art\Worlds\playgrounds\anim\textures\
  anim_wall01.dds

T:\rdr2\Art\Worlds\playgrounds\anim\textures\
  anim_wall02.dds

  --------------------------
#endif
	int GrownSize = 0;

	fiStream *S = ASSET.Open(texlist,"txt");
	Assert(S);

	fiTokenizer T(texlist,S);

	// Parse texturelist and grow texture list.

	int version = T.MatchInt("version");
	Assert(version == TEXTURE_DICTIONARY_LISTFILE_VERSION);
	int numtextures = T.MatchInt("texcount");

	for (int nt = 0; nt < numtextures; nt++ )
	{
		char texPath[ 512 ];
		char texfile[ 512 ];
		T.GetToken( texPath ,sizeof(texfile));// read path
		T.GetToken( texfile ,sizeof(texfile));// read filename

		bool	IsNotExcluded = TextureExists( texfile, ExcludeList );

		// if we are testing the excludelist and the filename isnt in the 'excludelist', then add it
		if( testExcludeList && IsNotExcluded )
		{
			bool TextureIsntDuplicatedInSourceList = TextureExists( texfile, DestList );
			Assert( TextureIsntDuplicatedInSourceList == false );
			// okay, texture isnt flagged to not be added to the destination list, and its not listed more than once in the source texture list, so add it!

			ASSET.SetPath( texPath );// set path

			fiStream *tex = ASSET.Open( texfile ,"");// open file
			Assert(tex);
			int texSize = tex->Size();
			Bytef *buffer = new Bytef[texSize];
			Assert( buffer );

			tex->Read(buffer,texSize);
			tex->Close();

			uLong crc = crc32(0L, buffer, texSize);

			// Add new texture entry, we don't check for duplicates because its assumed whatever created the list and passed it to this tool already did that.
			TextureEntry &t = DestList.Grow();

			// save name, this is what gets hashed.
			safecpy(t.Name, texfile ,sizeof(t.Name));
			// save path
			safecpy(t.Path, texPath ,sizeof(t.Path));

			t.Crc = crc;
			t.Bits = buffer;
			t.Size = texSize;

			t.texAddressModeBits_u = 0;
			t.texAddressModeBits_v = 0;
			t.u_max			= -999999.f;
			t.u_max_norm	= -999999.f;
			t.v_min			=  999999.f;
			t.v_min_norm	=  999999.f;

			GrownSize += texSize;
		}
	}
	return GrownSize;
}