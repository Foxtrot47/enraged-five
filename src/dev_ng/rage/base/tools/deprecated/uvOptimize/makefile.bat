set ARCHIVE=uvOptimize
set FILES=uvOptimize

set TESTERS=uvOptimizerApp
set PH_LIBS=curve phbound phcore devcam
set SAMPLE_LIBS=sample_rmcore %RAGE_SAMPLE_GRCORE_LIBS% parser parsercore
set LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS% %SAMPLE_LIBS% %PH_LIBS% 
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples