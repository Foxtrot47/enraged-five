// 
// uvOptimize/uvOptimizerApp.cpp 
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
// 

#include "phcore/materialmgrflag.h"
#include "phcore/materialmgrimpl.h"
#include "system/main.h"
#include "system/param.h"

#include "sample_rmcore/sample_rmcore.h"

#include "uvOptimize.h"

using namespace rage;
using namespace ragesamples;

FPARAM(1, infile,	"The filename of the type file to load (default is entity.type)");
FPARAM(2, outfile,	"The output bound file name (default is bounds.bnd)");

PARAM(file,		"The filename of the type file to load (default is entity.type)");
PARAM(in,		"The filename of the type file to load (default is entity.type)");
PARAM(dir,		"Drawable directory (default is assert path)");
PARAM(out,		"The output bound file name (default is bounds.bnd)");

PARAM(nosimplify,	"Do not simplify the bounds by converting triangles to quads (default is to simplify)");
PARAM(bind,			"Material binding list (default is materialbind.lst)");
PARAM(flags,		"Bound flag image list (default is boundflags.lst)");
PARAM(path,			"Asset search path");
PARAM(uvset,		"Specifies which UV set to use when applying the channel and AI maps (default is 0)");


class uvOptimizerManager : public rmcSampleManager
{
protected:
	void InitClient()
	{
		rmcSampleManager::InitClient();

		phMaterialMgrImpl<phMaterial, phMaterialMgrFlag>::Create();
		MATERIALMGRFLAG.SetAutoAssignFlags(true);

		char mtlPath[1024];
		sprintf(mtlPath,"%s/%s",GetFullAssetPath(),"tune/materials");
		ASSET.PushFolder(mtlPath);
		MATERIALMGR.Load();
		ASSET.PopFolder();

		uvOptimizer uvOptimizer;

		const char* typeFileName = "entity.type";
		PARAM_in.Get(typeFileName);
		PARAM_file.Get(typeFileName);
		PARAM_infile.Get(typeFileName);
		uvOptimizer.SetTypeFileName(typeFileName);

		const char* outFile = "bounds.bnd";
		PARAM_out.Get(outFile);
		PARAM_outfile.Get(outFile);
		uvOptimizer.SetOutputFileName(outFile);

		int uvSet = 0;
		PARAM_uvset.Get(uvSet);
		uvOptimizer.SetUVSet(uvSet);

		uvOptimizer.SetSimplify(!PARAM_nosimplify.Get());

		const char* mapFileName = "materialbind";
		PARAM_bind.Get(mapFileName);
		rageTextureMaterialMapFile materialMap(mapFileName);
		uvOptimizer.SetTextureMaterialMap(materialMap);

		const char* flagFileName = "materialflags";
		PARAM_flags.Get(flagFileName);
		rageFlagImageListFile flagImageList(flagFileName);
		uvOptimizer.SetFlagImageList(flagImageList);

		uvOptimizer.BuildBound();
	}

	void ShutdownClient()
	{
		MATERIALMGR.Destroy();

		rmcSampleManager::ShutdownClient();
	}
};

int Main()
{
	uvOptimizerManager uvOptimizer;

	const char* dir = NULL;
	PARAM_dir.Get(dir);

	uvOptimizer.Init(dir);
	uvOptimizer.Shutdown();

	return 0;
}
