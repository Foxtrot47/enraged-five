#pragma once

#include "..\xapcore\xapfile.h"
#include "resource.h"
#include <afxtempl.h>

#define MAX_NAME_LENGTH	256

#define VERSION_STR		"1.5" //This should be incremented any time this program changes.
#define TD_VERSION_STR	"5.0" //This must be incremented any time a change is made to the .td file format

class XAPFile;

void	WaitForKey(int timeOutMS = 2000);
void	ShowUsage(void);
void	WriteTdFiles(const XAPFile* xapFile);


class CXap2tdCommandLineInfo : public CCommandLineInfo
{
	virtual void ParseParam(const char* pszParam, BOOL bFlag, BOOL bLast);
};
