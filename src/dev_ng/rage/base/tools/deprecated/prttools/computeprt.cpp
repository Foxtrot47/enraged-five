// 
// prttools/computeprt.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "prtsim.h"

#include "direct.h"
#include "lightmapprt.h"
#include "progressrad.h"

#include "atl/array.h"
#include "file/asset.h"
#include "grcore/config.h"
#include "grcore/device.h"
#include "grmodel/setup.h"
#include "system/main.h"
#include "system/param.h"

#if !__D3D
#error "This tester only compiles as a Direct3D application."
#else
#define _WIN32_WINNT 0x500
#include "system/xtl.h"
//#include <d3d9.h>
//#include <d3dx9mesh.h>
#if !__OPTIMIZED
#pragma comment(lib,"d3dx9d.lib")
#pragma comment(lib,"d3d9.lib")
#else
#pragma comment(lib,"d3dx9.lib")
#pragma comment(lib,"d3d9.lib")
#endif
#endif


using namespace rage;

void Help() {
	Displayf("computeprt -input <inputPath> -output <outputPath> [-reload <reloadPath>]");
}

#if __D3D	

PARAM(input, "the path of the  .mesh input file.");
PARAM(output,"the path of the .x output file.");
PARAM(ascii,".x file is exported as an ascii file.");
PARAM(reload,"the path to reload from (can only be used with -pervertex).");
PARAM(subdivide,"specifies # of segments per edge to tessellate the result mesh (can be a floating point #).");
PARAM(numtimesteps,"# of time steps to same time of day lighting from");
PARAM(timestepstart,"the index to start the timestepping (must be less than -numtimesteps");
PARAM(saveresults,"save results from PRT calcuation (can only be used with -pervertex).");
PARAM(prtpervertex,"use per vertex PRT instead of per pixel PRT.");
PARAM(prt,"compute using non-PRT lighting.");
PARAM(radiosity,"compute using radiosity lighting.");
PARAM(ambientocclusion,"compute using radiosity lighting.");
PARAM(shaderpath,"Set shader root path (tune/shaders/lib and tune/shaders/db are appended)");

int Main() 
{
	INIT_PARSER;

	grmSetup setup;
	const char* assetPath="t:\\rage\\assets\\prt\\testModel\\";
	setup.Init(assetPath,"computeprt");
	
	// get the shader root (set it so that shaders are loaded correctly 
	// in the call to BeginGfx() below:

	// RAY - temp
	const char* shaderRoot="t:\\rage\\assets";
	PARAM_shaderpath.Get(shaderRoot);
	ASSET.SetPath(shaderRoot);

	setup.BeginGfx(true);
	setup.CreateDefaultFactories();
 
	//prtGeometry::InitShaderSystems(shaderRoot);
	
	const char* inputPath=NULL;
	const char* outputPath=NULL;

	PARAM_input.Get(inputPath);
	PARAM_output.Get(outputPath);
	if (!outputPath )
	{
		outputPath = inputPath;
	}

	if (!inputPath || !outputPath)
	{
		Help();
		return -1;
	}

	// set the asset path:
	char* inputPathOnly=StringDuplicate(inputPath);
	ASSET.RemoveNameFromPath(inputPathOnly,strlen(inputPathOnly),inputPathOnly);
	ASSET.SetPath(inputPathOnly);
	
	// create and initialize the lightmap:
	const int PACKED_WIDTH=1024;
	const int PACKED_HEIGHT=1024;

	
	// make sure we're always running:
	GRCDEVICE.SetBlockOnLostFocus(false);

	bool ambientOcclusion=PARAM_ambientocclusion.Get();

	if (PARAM_prt.Get())
	{
		prtSimulator* simulator=new prtSimulator;
		simulator->LoadMesh(inputPath);

		prtSimulatorOptions options;
		options.m_OutputConcatPRTMesh=outputPath;
		if (!simulator->Convert(options))
			return -1;

		pltLightMapPrt lightMap(*simulator,PACKED_WIDTH,PACKED_HEIGHT);
		lightMap.Init();

		const char* loadDirectory=NULL;
		PARAM_reload.Get(loadDirectory);

		// validate command line options:
		if (!PARAM_prtpervertex.Get())
		{
			if (loadDirectory!=NULL)
			{
				Quitf("-pervertex must be used with -reload");
			}
			else if (PARAM_saveresults.Get())
			{
				Quitf("-pervertex must be used with -saveresults");
			}
		}

		// load the data instead of rerunning the simulation...
		if (loadDirectory!=NULL)
		{
			simulator->Load(loadDirectory);
		}
		// ...or actually rerun the simulation:
		else 
		{
			// run the simulation to get our PRT values:
			simulator->Run(options,NULL,true,PACKED_WIDTH,PACKED_HEIGHT);
			if (PARAM_saveresults.Get())
			{
				Displayf("saving simulator results...");
				simulator->Save(assetPath);
			}
		}
		

#if 0
		setup.BeginUpdate();
		setup.EndUpdate();

		setup.BeginDraw(true);

		while (numTimeSteps<maxTimeSteps)
		{
			char textureName[256];
			Displayf("creating and saving texture #%d...",numTimeSteps+1);
			sprintf(textureName,"c:\\lightmaps\\lightmap_%02d.dds",numTimeSteps+1);

			Vector3 lightDirObjectSpace(0.0f,0.0f,1.0f);
			Matrix34 rotateMat;
			rotateMat.Identity();
			rotateMat.MakeRotateX(angleIncr*numTimeSteps*DtoR);
			rotateMat.Transform(lightDirObjectSpace);

			// compute lightmaps:
			simulator->UpdateLightingEnvironment(lightDirObjectSpace);
			lightMap.Render();
			lightMap.SaveTexture(textureName,grcImage::DXT5);

			numTimeSteps++;
		}
#endif

		simulator->OutputMesh("lightmappedMesh",false);
		simulator->OutputMesh("tesselatedMesh",true);


	}
	else
	{
		int numTimeSteps=0;
		PARAM_timestepstart.Get(numTimeSteps);
		int maxTimeSteps=1;
		PARAM_numtimesteps.Get(maxTimeSteps);
		if (numTimeSteps>maxTimeSteps)
			Quitf("The time step to start on (%d) is greater than the max time steps (%d)",numTimeSteps,maxTimeSteps);

		float angleIncr=360.0f/maxTimeSteps;



		pltLightMapDirect::InitClass();

		pltLightMapProgressiveRadiosity::InitClass();

		// initialize and add our geometries:
		prtDirectLighting direct;
		direct.Init();
		prtGeometry geom;
		geom.Init(direct.GetConvert(),inputPath,direct,PACKED_WIDTH,PACKED_HEIGHT);
		direct.AddGeometry(geom);
		direct.SaveGeometries();

		while (numTimeSteps<maxTimeSteps)
		{
			Displayf("Doing time step %d out of %d...",numTimeSteps,maxTimeSteps);

			//Vector3 lightDirObjectSpace(0.0f,0.0f,100.0f);
			Vector3 lightDirObjectSpace(10.0f,10.0f,10.0f);
			Matrix34 rotateMat;
			rotateMat.Identity();
			rotateMat.MakeRotateX(angleIncr*numTimeSteps*DtoR);
			rotateMat.Transform(lightDirObjectSpace);

			//direct.SetLight(lightDirObjectSpace,Vector3(1.0f,1.0f,1.0f),10.0f);
			direct.SetLight(lightDirObjectSpace,Vector3(1.0f,0.75f,0.0f),100000.0f);
		
			setup.BeginUpdate();
			setup.EndUpdate();

			setup.BeginDraw(true);

			direct.ComputeLightMapsAlt();
			//direct.ComputeLightMaps();

			// save the lightmap:
			char textureName[256];
			Displayf("creating and saving texture #%d...",numTimeSteps+1);
			sprintf(textureName,"d:\\lightmaps\\lightmap_%02d.dds",numTimeSteps+1);
			direct.SaveLightMaps(textureName,!ambientOcclusion);


			numTimeSteps++;
		}

		// do radiosity computation;
		if (PARAM_radiosity.Get())
		{
			prtProgressiveRadiosity radiosity(direct);

			radiosity.Iterate(16);
		}

		if (ambientOcclusion)
		{
			geom.ComputeAmbientOcclusionMaps(direct.GetGeometries());		
			geom.SaveAmbientOcclusionMaps("c:\\ambientOcclusion.dds");
		}
	}

	setup.EndDraw();

	setup.EndGfx();

	SHUTDOWN_PARSER;

	return -1;
}

#endif
