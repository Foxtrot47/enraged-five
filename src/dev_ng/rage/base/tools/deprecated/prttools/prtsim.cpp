//
// prttools/prtsim.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "config.h"
#include "prtsim.h"
#include "prtsim_parser.h"

#include "mesh/serialize.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/timemgr.h"
#include "uvatlas/uvatlas.h"

#include <d3dx9mesh.h>

#include <stdio.h>
#include <conio.h>
#include <fstream>

namespace rage {



// helper functions
static void SaveVectorAsTexture( const char* fileName, Vector4* vals, int width, int height );
static void SavePRTCoeffsToQuantizedTexture( ID3DXPRTBuffer* prt , const char* path, const char* prefix  );
static void Save3rdOrderPRTCoeffsToQuantizedTextures( ID3DXPRTBuffer* prt , const char* path, const char* prefix  );

struct prtState
{
    CRITICAL_SECTION	m_Cs;
    bool				m_UserAbort;
    bool				m_StopSimulator;
    bool				m_Running;
    bool				m_Failed;
    bool				m_ProgressMode;
    int					m_NumPasses;
    int					m_CurPassNum;
    float				m_PercentDone;
};		

HRESULT WINAPI StaticPRTSimulatorCB( float fPercentDone, LPVOID pParam );

DWORD WINAPI StaticUIThreadProc( LPVOID lpParameter )
{   
	prtState& theState				= *((rage::prtState*) lpParameter);
    bool stop						= false;
    float lastPercent				= -1.0f;
    double lastPercentAnnounceTime	= 0.0f;
	
    while( !stop )
    {
        EnterCriticalSection( &theState.m_Cs );

        if( theState.m_ProgressMode )
        {
            if( theState.m_PercentDone < 0.0f || TIME.GetElapsedTime() < lastPercentAnnounceTime + 5.0f )        
				Printf(".");
            else
            {
				Printf("%0.0f%%.", theState.m_PercentDone*100.0f);
                lastPercent = theState.m_PercentDone;
                lastPercentAnnounceTime = TIME.GetElapsedTime();
            }
            
        }

        while( _kbhit() )
        {
            int nChar = _getch();
            if( nChar == VK_ESCAPE )
            {
                Printf( "\n\nEscape key pressed. Aborting!\n" );
                theState.m_StopSimulator = true;
                theState.m_UserAbort = true;
                break;
            }
        }

		TIME.Update();

        stop = theState.m_StopSimulator;
        LeaveCriticalSection( &theState.m_Cs );

        if( !stop ) 
             sysIpcSleep( 1000 );

	
    }

    return 0;
}

prtRemapInfo::prtRemapInfo()
{
	REGISTER_PARSABLE_CLASS(prtRemapInfo);
}

bool prtRemapInfo::Load(const char* fileName)
{
	return PARSER.LoadObject(fileName,"rbf",*this);
}

bool prtRemapInfo::Save(const char* fileName)
{
	Assert(m_FaceRemaps.GetCount()>0);
	Assert(m_VertRemaps.GetCount()>0);
	Assert(m_VertWeights.GetCount()>0);
	return PARSER.SaveObject(fileName,"rbf",this,parManager::BINARY);
}


void prtRemapInfo::CopyFromArrays(UINT* faceRemaps,int faceRemapCount,UINT* vertRemaps,int vertRemapsCount,FLOAT* vertWeights,int vertWeightsCount)
{
	m_FaceRemaps.CopyFrom(faceRemaps,faceRemapCount);
	m_VertRemaps.CopyFrom(vertRemaps,vertRemapsCount);
	m_VertWeights.CopyFrom(vertWeights,vertWeightsCount);
}


void prtSimulator::ConcatMesh::CopyRemap(prtRemapInfo& info)
{
	m_RemapInfo=&info;
	m_FaceRemaps=&m_RemapInfo->m_FaceRemaps[0];
	m_VertRemaps=&m_RemapInfo->m_VertRemaps[0];
	m_VertWeights=&m_RemapInfo->m_VertWeights[0];
}

void prtSimulator::LoadMesh(const char* inputPath)
{
	delete m_RageMesh;
	m_RageMesh=m_Convert.LoadRageMesh(inputPath);
}

bool prtSimulator::Convert(const prtSimulatorOptions& options)
{
	m_ConcatDxMesh.m_DxMesh=NULL;

	if (!m_RageMesh)
		return false;

	m_ConcatDxMesh.m_DxMesh=m_Convert.CreateD3DXMesh(*m_RageMesh,m_ConcatDxMesh.m_DxMeshMaterialNames);
	if (!m_ConcatDxMesh.m_DxMesh)
		return false;

	m_ConcatDxMesh.m_Materials.Resize(m_RageMesh->GetMtlCount());
	m_ConcatDxMesh.m_SHMaterials.Resize(m_RageMesh->GetMtlCount());
	for( int iMesh=0; iMesh<m_ConcatDxMesh.m_Materials.GetCount(); iMesh++ )
	{
		D3DXMATERIAL mat;
		D3DCOLORVALUE& diffuse=mat.MatD3D.Diffuse;
		diffuse.r=diffuse.g=diffuse.b=1.0f;
		D3DCOLORVALUE& ambient=mat.MatD3D.Ambient;
		ambient.r=ambient.g=ambient.b=0.25f;
		D3DCOLORVALUE& specular=mat.MatD3D.Specular;
		specular.r=specular.g=specular.b=0.25f;
		D3DCOLORVALUE& emissive=mat.MatD3D.Emissive;
		emissive.r=emissive.g=emissive.b=0.0f;
		mat.MatD3D.Power=1.0f;
		mat.pTextureFilename="";
		m_ConcatDxMesh.m_Materials[iMesh]=mat;
		m_ConcatDxMesh.m_SHMaterials[iMesh]=options.m_InputMeshes[0].m_SHMaterials[0];
	}

	return m_ConcatDxMesh.m_DxMesh!=NULL;
		
}

void prtSimulator::ExtractCompressedDataForPRTShader(ID3DXMesh& dxMesh,int width,int height) 
{ 
	// First call ID3DXPRTCompBuffer::NormalizeData.  This isn't nessacary, 
	// but it makes it easier to use data formats that have little presision.
	// It normalizes the PCA weights so that they are between [-1,1]
	// and modifies the basis vectors accordingly.  
	V( m_PrtCompBuffer->NormalizeData() );

	UINT numSamples  = m_PrtCompBuffer->GetNumSamples();
	UINT numCoeffs   = m_PrtCompBuffer->GetNumCoeffs();
	UINT numChannels = m_PrtCompBuffer->GetNumChannels();
	UINT numClusters = m_PrtCompBuffer->GetNumClusters();
	UINT numPCA      = m_PrtCompBuffer->GetNumPCA();

	 m_NumPca=m_PrtCompBuffer->GetNumPCA();

	// With clustered PCA, each vertex is assigned to a cluster.  To figure out 
	// which vertex goes with which cluster, call ID3DXPRTCompBuffer::ExtractClusterIDs.
	// This will return a cluster ID for every vertex.  Simply pass in an array of UINTs
	// that is the size of the number of vertices (which also equals the number of samples), and 
	// the cluster ID for vertex N will be at puClusterIDs[N].
	delete [] m_ClusterIDs;
	m_ClusterIDs = new UINT[ numSamples ];
	V( m_PrtCompBuffer->ExtractClusterIDs( m_ClusterIDs ) );

	D3DVERTEXELEMENT9 declCur[MAX_FVF_DECL_SIZE];
	dxMesh.GetDeclaration( declCur );


	// per pixel PRT...
	if (m_PerPixel)
	{
		delete [] m_PrtTextures;
		m_PrtTextures=new LPDIRECT3DTEXTURE9[numPCA];
		
	
		for (UINT i=0;i<numPCA;i++)
		{
			AssertVerify(SUCCEEDED(GRCDEVICE.GetCurrent()->CreateTexture(
				width, height,
				1, D3DUSAGE_DYNAMIC, D3DFMT_R32F, 
				D3DPOOL_DEFAULT, &m_PrtTextures[i], NULL )));

			V( m_PrtCompBuffer->ExtractTexture(i,1,m_PrtTextures[i]));
		}
	}
	// ...per vertex PRT:
	else
	{
		// Now use this cluster ID info to store a value in the mesh in the 
		// D3DDECLUSAGE_BLENDWEIGHT[0] which is declared in the vertex decl to be a float1
		// This value will be passed into the vertex shader to allow the shader 
		// use this number as an offset into an array of shader constants.  
		// The value we set per vertex is based on the cluster ID and the stride 
		// of the shader constants array.  
		uvaUvAtlasHelper::Vertex* vertex = NULL;
		V( dxMesh.LockVertexBuffer( 0, (void**) &vertex ) );
		//UINT stride = dxMesh.GetNumBytesPerVertex();
		for( u32 vert = 0; vert < numSamples; vert++ ) 
		{
			vertex->m_BlendWeight0=(m_ClusterIDs[vert] * (1+3*(numPCA/4))); 
			vertex++;
		}
		dxMesh.UnlockVertexBuffer();

		// Now we also need to store the per vertex PCA weights.  Earilier when
		// the mesh was loaded, we changed the vertex decl to make room to store these
		// PCA weights.  In this sample, we will use D3DDECLUSAGE_BLENDWEIGHT[1] to 
		// D3DDECLUSAGE_BLENDWEIGHT[6].  Using D3DDECLUSAGE_BLENDWEIGHT intead of some other 
		// usage was an arbritatey decision.  Since D3DDECLUSAGE_BLENDWEIGHT[1-6] were 
		// declared as float4 then we can store up to 6*4 PCA weights per vertex.  They don't
		// have to be declared as float4, but its a reasonable choice.  So for example, 
		// if numPCAVectors=16 the function will write data to D3DDECLUSAGE_BLENDWEIGHT[1-4]
		V( m_PrtCompBuffer->ExtractToMesh( numPCA, D3DDECLUSAGE_BLENDWEIGHT, 1, &dxMesh ) );
	}

	// Extract the cluster bases into a large array of floats.  
	// ID3DXPRTCompBuffer::ExtractBasis will extract the basis 
	// for a single cluster.  
	//
	// A single cluster basis is an array of
	// (NumPCA+1)*NumCoeffs*NumChannels floats
	// The "1+" is for the cluster mean.
	int clusterBasisSize = (numPCA+1) * numCoeffs * numChannels;  
	int bufferSize       = clusterBasisSize * numClusters; 

	SAFE_DELETE_ARRAY( m_PrtClusterBases );
	m_PrtClusterBases = new float[bufferSize];

	for( u32 cluster = 0; cluster < numClusters; cluster++ ) 
	{
		// ID3DXPRTCompBuffer::ExtractBasis() extracts the basis for a single cluster at a time.
		V( m_PrtCompBuffer->ExtractBasis( cluster, &m_PrtClusterBases[cluster * clusterBasisSize] ) );
	}

	delete[] m_PrtConstants;
	m_PrtConstants = new float[numClusters*(4+numChannels*numPCA)];
	// Default memory allocator should give us suitably-aligned memory, but just in case:
	Assert(((size_t)m_PrtConstants & 15) == 0);
}

ID3DXPRTBuffer* prtSimulator::CreateBuffer(int numSamples, const prtSimulatorOptions& options,bool perPixel,int width,int height)
{
	ID3DXPRTBuffer* newBuffer;
	if (perPixel)
	{
		V( D3DXCreatePRTBufferTex( width, height, options.m_Order*options.m_Order, 
			options.m_NumChannels, &newBuffer ) );
	}
	else 
	{
		V( D3DXCreatePRTBuffer( numSamples, options.m_Order*options.m_Order, 
			options.m_NumChannels, &newBuffer ) );
	}

	return newBuffer;
}

bool prtSimulator::Run(const prtSimulatorOptions& options, ConcatMesh* blockerMesh,bool perPixel,int width,int height)
{
    DWORD dwResult;
    DWORD dwThreadId;
 
	m_PerPixel=perPixel;

    // Check if any SH material need subsurface scattering
    bool subsurfaceScattering = false;
   
	// setup state:
    prtState theState;
    ZeroMemory( &theState, sizeof(prtState) ); 
    InitializeCriticalSection( &theState.m_Cs );   
    theState.m_StopSimulator = false;
    theState.m_Running = true;
    theState.m_NumPasses = options.m_NumBounces;
    if( subsurfaceScattering )
        theState.m_NumPasses *= 2;
    if( options.m_EnableTessellation && options.m_RobustMeshRefine )
        theState.m_NumPasses++;
    if( options.m_EnableTessellation )
        theState.m_NumPasses++;
    if( options.m_EnableCompression )
        theState.m_NumPasses += 2;
	if ( options.m_outputAmbientOcclusion )
		theState.m_NumPasses++;

    theState.m_NumPasses += 2;
    theState.m_CurPassNum = 1;
    theState.m_PercentDone = -1.0f;
    theState.m_ProgressMode = true;

	// start the simulator:
    Displayf( "\nStarting simulator.  Press ESC to abort\n" );
	Printf("\nStage %d of %d: Initializing PRT engine..", theState.m_CurPassNum, theState.m_NumPasses );
    HANDLE hThreadId = CreateThread( NULL, 0, StaticUIThreadProc, (LPVOID) &theState, 0, &dwThreadId );

    HRESULT				hr;
    ID3DXPRTEngine*		prtEngine		= NULL;
    ID3DXPRTBuffer*		dataTotal		= NULL;
    ID3DXPRTBuffer*		bufferA			= NULL;
    ID3DXPRTBuffer*		bufferB			= NULL;
    ID3DXPRTBuffer*		bufferC			= NULL;
 
	// create the PRT engine:
    DWORD* adjs = new DWORD[m_ConcatDxMesh.m_DxMesh->GetNumFaces()*3];
    //m_ConcatDxMesh.m_DxMesh->GenerateAdjacency(1e-6f,adjs);

	hr = m_ConcatDxMesh.m_DxMesh->ConvertPointRepsToAdjacency(NULL, adjs );
	if (FAILED(hr))
	{
		Warningf( "ConvertPointRepsToAdjacency() failed: !\n");
	}
	//V( D3DXCreatePRTEngine( m_ConcatDxMesh.m_DxMesh, adjs, FALSE, blockerMesh?blockerMesh->m_DxMesh:NULL, &prtEngine ) );

	// move texture coordinate set 2 to 1 for per pixel PRT (since the engine uses only the first set of texture coordinates):
	Vector2* texCoords=NULL;
	if (m_PerPixel)
	{
		// get face vertices:
		uvaUvAtlasHelper::Vertex* vertices;
		m_ConcatDxMesh.m_DxMesh->LockVertexBuffer(0x0,(LPVOID*)&vertices);
		texCoords=new Vector2[m_ConcatDxMesh.m_DxMesh->GetNumVertices()];
		for (u32 vert=0;vert<(u32)m_ConcatDxMesh.m_DxMesh->GetNumVertices();vert++)
		{
			texCoords[vert]=vertices[vert].m_Texcoord;
			vertices[vert].m_Texcoord=vertices[vert].m_Texcoord2;
		}
		m_ConcatDxMesh.m_DxMesh->UnlockVertexBuffer();
	}

	V( D3DXCreatePRTEngine( m_ConcatDxMesh.m_DxMesh, NULL, m_PerPixel?TRUE:FALSE, blockerMesh?blockerMesh->m_DxMesh:NULL, &prtEngine ) );


    delete[] adjs;

    V( prtEngine->SetCallBack( StaticPRTSimulatorCB, 0.001f, &theState ) );
	V( prtEngine->SetSamplingInfo( options.m_NumRays, m_PerPixel?TRUE:FALSE, m_PerPixel?FALSE:TRUE, FALSE, 0.0f ) );

//    if( options.m_EnableTessellation && m_ConcatDxMesh.materialArray.GetAt(1).->GetAlbedoTexture() )
    {
//      V( prtEngine->SetPerTexelAlbedo( m_m_ConcatDxMesh.GetAlbedoTexture(), 
//                                        options.m_NumChannels, NULL ) );
    }

    bool setAlbedoFromMaterial = true;
//    if( options.m_EnableTessellation && m_m_ConcatDxMesh.GetAlbedoTexture() )
//        bSetAlbedoFromMaterial = false;

	// set materials:
    D3DXSHMATERIAL** pMatPtrArray = new D3DXSHMATERIAL*[m_ConcatDxMesh.m_SHMaterials.GetCount()];
    for( int i=0; i<m_ConcatDxMesh.m_SHMaterials.GetCount(); ++i )
        pMatPtrArray[i] = &(m_ConcatDxMesh.m_SHMaterials[i]);
    V( prtEngine->SetMeshMaterials( (const D3DXSHMATERIAL**)pMatPtrArray, m_ConcatDxMesh.m_SHMaterials.GetCount(), 
                                       options.m_NumChannels, 
                                       setAlbedoFromMaterial, options.m_LengthScale ) );

	V(prtEngine->SetMinMaxIntersection( 0.000001f, 1.0f) );

	bool enableTesselation=(options.m_EnableTessellation && !m_PerPixel)?true:false;
    if( !subsurfaceScattering )
    {
        // Not doing subsurface scattering
        if( enableTesselation && options.m_RobustMeshRefine && !perPixel ) 
        {
            EnterCriticalSection( &theState.m_Cs );
            theState.m_CurPassNum++;
            theState.m_PercentDone = -1.0f;
            Printf("\nStage %d of %d: Robust Mesh Refine..", theState.m_CurPassNum, theState.m_NumPasses );
             LeaveCriticalSection( &theState.m_Cs );

            V( prtEngine->RobustMeshRefine( options.m_RobustMeshRefineMinEdgeLength, options.m_RobustMeshRefineMaxSubdiv ) );
        }

        DWORD numSamples = prtEngine->GetNumVerts();
		dataTotal=CreateBuffer(numSamples,options,m_PerPixel,width,height);

        EnterCriticalSection( &theState.m_Cs );
        theState.m_CurPassNum++;
        Printf("\nStage %d of %d: Computing Direct Lighting..", theState.m_CurPassNum, theState.m_NumPasses );
        theState.m_PercentDone = 0.0f;
        LeaveCriticalSection( &theState.m_Cs );

        if( enableTesselation && options.m_AdaptiveDL && !perPixel )
        {
            hr = prtEngine->ComputeDirectLightingSHAdaptive( options.m_Order, 
                                                              options.m_AdaptiveDLThreshold, options.m_AdaptiveDLMinEdgeLength, options.m_AdaptiveDLMaxSubdiv, 
                                                              dataTotal );
            if( FAILED(hr) )
                goto LEarlyExit; // handle user aborting simulator via callback 
        }
        else
        {
			if (  false && m_PerPixel)
			{
				//D3DXSHGPUSIMOPT_SHADOWRES1024
				hr = prtEngine->ComputeDirectLightingSHGPU( GRCDEVICE.GetCurrent(),D3DXSHGPUSIMOPT_SHADOWRES1024 | D3DXSHGPUSIMOPT_HIGHQUALITY, options.m_Order, 
															0.0001f,0.001f, dataTotal ); // was 0.005f
			}
			else
			{
				hr = prtEngine->ComputeDirectLightingSH( options.m_Order, dataTotal );
			}
	        if( FAILED(hr) )
                goto LEarlyExit; // handle user aborting simulator via callback 
        }

        if( options.m_NumBounces > 1 )
        {
            numSamples = prtEngine->GetNumVerts();
			bufferA=CreateBuffer(numSamples,options,m_PerPixel,width,height);
			bufferB=CreateBuffer(numSamples,options,m_PerPixel,width,height);
            V( bufferA->AddBuffer( dataTotal ) );
        }

        for( u32 iBounce=1; iBounce<options.m_NumBounces; ++iBounce )
        {
            EnterCriticalSection( &theState.m_Cs );
            theState.m_CurPassNum++;
            Printf("\nStage %d of %d: Computing Bounce %d Lighting..", theState.m_CurPassNum, theState.m_NumPasses, iBounce+1 );
            theState.m_PercentDone = 0.0f;
            LeaveCriticalSection( &theState.m_Cs );

            if( enableTesselation && options.m_AdaptiveBounce )
                hr = prtEngine->ComputeBounceAdaptive( bufferA, options.m_AdaptiveBounceThreshold, options.m_AdaptiveBounceMinEdgeLength, options.m_AdaptiveBounceMaxSubdiv, bufferB, dataTotal );
            else
                hr = prtEngine->ComputeBounce( bufferA, bufferB, dataTotal );

            if( FAILED(hr ) )
                goto LEarlyExit; // handle user aborting simulator via callback 

            // Swap bufferA and bufferB
            ID3DXPRTBuffer* pPRTBufferTemp = NULL;
            pPRTBufferTemp = bufferA;
            bufferA = bufferB;
            bufferB = pPRTBufferTemp;
        }
    }
    else
    {
        // Doing subsurface scattering
        if( enableTesselation && options.m_RobustMeshRefine && !perPixel ) 
            V( prtEngine->RobustMeshRefine( options.m_RobustMeshRefineMinEdgeLength, options.m_RobustMeshRefineMaxSubdiv ) );

        DWORD numSamples = prtEngine->GetNumVerts();
		bufferA		=CreateBuffer(numSamples,options,m_PerPixel,width,height);
		bufferB		=CreateBuffer(numSamples,options,m_PerPixel,width,height);
		dataTotal	=CreateBuffer(numSamples,options,m_PerPixel,width,height);

        EnterCriticalSection( &theState.m_Cs );
        theState.m_CurPassNum++;
        Printf("\nStage %d of %d: Computing Direct Lighting..", theState.m_CurPassNum, theState.m_NumPasses );
        theState.m_PercentDone = 0.0f;
        LeaveCriticalSection( &theState.m_Cs );

        if( enableTesselation && options.m_AdaptiveDL && !perPixel )
        {
            hr = prtEngine->ComputeDirectLightingSHAdaptive( options.m_Order, 
                                                                options.m_AdaptiveDLThreshold, options.m_AdaptiveDLMinEdgeLength, options.m_AdaptiveDLMaxSubdiv, 
                                                                bufferA );
            if( FAILED(hr) )
                goto LEarlyExit; // handle user aborting simulator via callback 
        }
        else
        {
            hr = prtEngine->ComputeDirectLightingSH( options.m_Order, bufferA );
			//hr = prtEngine->ComputeDirectLightingSHGPU( pd3dDevice,D3DXSHGPUSIMOPT_SHADOWRES1024, options.m_Order, 0.005f,0.005f, bufferA );
            if( FAILED(hr) )
                goto LEarlyExit; // handle user aborting simulator via callback 
        }

        EnterCriticalSection( &theState.m_Cs );
        theState.m_CurPassNum++;
        Printf("\nStage %d of %d: Computing Subsurface Direct Lighting..", theState.m_CurPassNum, theState.m_NumPasses );
        LeaveCriticalSection( &theState.m_Cs );

        hr = prtEngine->ComputeSS( bufferA, bufferB, dataTotal );
        if( FAILED(hr) )
            goto LEarlyExit; // handle user aborting simulator via callback 

        for( u32 iBounce=1; iBounce<options.m_NumBounces; ++iBounce )
        {
            EnterCriticalSection( &theState.m_Cs );
            theState.m_CurPassNum++;
            Printf("\nStage %d of %d: Computing Bounce %d Lighting..", theState.m_CurPassNum, theState.m_NumPasses, iBounce+1 );
            theState.m_PercentDone = 0.0f;
            LeaveCriticalSection( &theState.m_Cs );

            if( enableTesselation && options.m_AdaptiveBounce )
                hr = prtEngine->ComputeBounceAdaptive( bufferB, options.m_AdaptiveBounceThreshold, options.m_AdaptiveBounceMinEdgeLength, options.m_AdaptiveBounceMaxSubdiv, bufferA, NULL );
            else
                hr = prtEngine->ComputeBounce( bufferB, bufferA, NULL );

            if( FAILED(hr ) )
                goto LEarlyExit; // handle user aborting simulator via callback 

            EnterCriticalSection( &theState.m_Cs );
            theState.m_CurPassNum++;
            Printf("\nStage %d of %d: Computing Subsurface Bounce %d Lighting..", theState.m_CurPassNum, theState.m_NumPasses, iBounce+1 );
            LeaveCriticalSection( &theState.m_Cs );

            hr = prtEngine->ComputeSS( bufferA, bufferB, dataTotal );
            if( FAILED(hr ) )
                goto LEarlyExit; // handle user aborting simulator via callback 
        }

    }

	ID3DXMesh* adaptedMesh=NULL;
    if( enableTesselation && !perPixel )
    {
		// allocate information for the remapping information:
		int numFaces=prtEngine->GetNumFaces();
		int numVerts=prtEngine->GetNumVerts();
		m_ConcatDxMesh.m_FaceRemaps=new UINT[numFaces];
		m_ConcatDxMesh.m_VertRemaps=new UINT[numVerts*3];
		m_ConcatDxMesh.m_VertWeights=new FLOAT[numVerts*3];

		V( prtEngine->GetAdaptedMesh( m_Convert.GetDevice(), m_ConcatDxMesh.m_FaceRemaps, m_ConcatDxMesh.m_VertRemaps, m_ConcatDxMesh.m_VertWeights, &adaptedMesh ) );

		m_Convert.AdjustMeshDecl(adaptedMesh);
    
        EnterCriticalSection( &theState.m_Cs );
        theState.m_CurPassNum++;
        theState.m_PercentDone = 0.0f;
        Printf("\nStage %d of %d: Saving tessellated mesh to %s", theState.m_CurPassNum, theState.m_NumPasses, options.m_OutputTessellatedMesh );
        theState.m_ProgressMode = false;
        LeaveCriticalSection( &theState.m_Cs );
    }

	if (adaptedMesh!=NULL)
	{
		m_ConcatDxMesh.m_OriginalDxMesh=m_ConcatDxMesh.m_DxMesh;
		m_ConcatDxMesh.m_DxMesh=adaptedMesh;
	}

    SAFE_RELEASE( bufferA );
    SAFE_RELEASE( bufferB );    

    EnterCriticalSection( &theState.m_Cs );
    theState.m_CurPassNum++;
    theState.m_PercentDone = 0.0f;
    Printf( "\nStage %d of %d: Saving PRT buffer to %s", theState.m_CurPassNum, theState.m_NumPasses, options.m_OutputPRTBuffer );
    theState.m_ProgressMode = false;
    LeaveCriticalSection( &theState.m_Cs );

    // Save the PRT buffer results for future sessions so you can skip the simulator step
    // You can also use D3DXSavePRTCompBufferToFile to save the compressed PRT buffer to 
    // skip the PRT compression step upon load.  
    if( FAILED( hr = D3DXSavePRTBufferToFile( options.m_OutputPRTBuffer, dataTotal ) ) )
    {
        Printf("\nError: Failed saving to %s", options.m_OutputPRTBuffer );
        goto LEarlyExit; 
    }

	//--- ambientOcclusion
	if ( options.m_outputAmbientOcclusion )
	{

		EnterCriticalSection( &theState.m_Cs );
		theState.m_ProgressMode = true;
		theState.m_CurPassNum++;
		theState.m_PercentDone = 0.0f;
		Printf("\nStage %d of %d: Saving Ambinet Occlusion..", theState.m_CurPassNum, theState.m_NumPasses );
		LeaveCriticalSection( &theState.m_Cs );
	
		SavePRTCoeffsToQuantizedTexture( dataTotal, ASSET.GetPath(), "prt" );
	//	Save3rdOrderPRTCoeffsToQuantizedTextures( dataTotal, ASSET.GetPath(), "prt3rd" );
		
		//LPDIRECT3DTEXTURE9	AmbientTexture;

		//AssertVerify(SUCCEEDED(GRCDEVICE.GetCurrent()->CreateTexture(
		//	width, height,
		//	1, D3DUSAGE_DYNAMIC, D3DFMT_L8, 
		//	D3DPOOL_DEFAULT, &AmbientTexture, NULL )));

		//const float ScaleZeroCoeff = 0.278f;

		//V( dataTotal->ScaleBuffer( 1.0f / ScaleZeroCoeff ) );
		//V( dataTotal->ExtractTexture( 0, 0,	1,	AmbientTexture	) );

		//V( dataTotal->ScaleBuffer(  ScaleZeroCoeff ) ); // and scale back again


		//V( D3DXSaveTextureToFile( 
		//	"c:\\lightmaps\\2ndOrderprt.dds",
		//	D3DXIFF_DDS,
		//	AmbientTexture,
		//	NULL
		//	) );

		//AmbientTexture->Release();
	}
    if( options.m_EnableCompression )
    {
        EnterCriticalSection( &theState.m_Cs );
        theState.m_ProgressMode = true;
        theState.m_CurPassNum++;
        theState.m_PercentDone = 0.0f;
        Printf("\nStage %d of %d: Compressing PRT buffer..", theState.m_CurPassNum, theState.m_NumPasses );
        LeaveCriticalSection( &theState.m_Cs );

        // Limit the max number of PCA vectors to # channels * # coeffs
        hr = D3DXCreatePRTCompBuffer( options.m_Quality, options.m_NumClusters, options.m_NumPCA, StaticPRTSimulatorCB, &theState, dataTotal, &m_PrtCompBuffer );
        if( FAILED(hr ) )
            goto LEarlyExit; // handle user aborting simulator via callback 

        EnterCriticalSection( &theState.m_Cs );
        theState.m_ProgressMode = false;
        theState.m_CurPassNum++;
        theState.m_PercentDone = 0.0f;
        Printf("\nStage %d of %d: Saving Compressed PRT buffer to %s", theState.m_CurPassNum, theState.m_NumPasses, options.m_OutputCompPRTBuffer );
        LeaveCriticalSection( &theState.m_Cs );

		ExtractCompressedDataForPRTShader(*m_ConcatDxMesh.m_DxMesh,width,height);
        if( FAILED( hr = D3DXSavePRTCompBufferToFile( options.m_OutputCompPRTBuffer, m_PrtCompBuffer ) ) )
        {
            Printf("\nError: Failed saving to %s", options.m_OutputCompPRTBuffer );
            goto LEarlyExit; 
        }
    }

	if (m_PerPixel)
	{
		// get face vertices:
		uvaUvAtlasHelper::Vertex* vertices;
		m_ConcatDxMesh.m_DxMesh->LockVertexBuffer(0x0,(LPVOID*)&vertices);
		for (u32 vert=0;vert<(u32)m_ConcatDxMesh.m_DxMesh->GetNumVertices();vert++)
		{
			vertices[vert].m_Texcoord=texCoords[vert];
		}
		m_ConcatDxMesh.m_DxMesh->UnlockVertexBuffer();

		delete [] texCoords;
	}

LEarlyExit:

    // Usually fails becaused user stoped the simulator
    EnterCriticalSection( &theState.m_Cs );
    theState.m_Running = false;
    theState.m_StopSimulator = true;
    if( FAILED(hr) ) 
        theState.m_Failed = true;
    //m_UserAbort = theState.m_UserAbort;
    LeaveCriticalSection( &theState.m_Cs );

    if( SUCCEEDED(hr) ) 
        Printf( "\n\nDone!\n" );

    // Wait for it to close
    dwResult = WaitForSingleObject( hThreadId, 10000 );
    if( dwResult == WAIT_TIMEOUT )
        return false;

    DeleteCriticalSection( &theState.m_Cs );  

    SAFE_RELEASE( bufferA );
    SAFE_RELEASE( bufferB );
    SAFE_RELEASE( bufferC );
    SAFE_RELEASE( dataTotal );
    SAFE_RELEASE( prtEngine );
    SAFE_DELETE_ARRAY( pMatPtrArray );

    // It returns E_FAIL if the simulation was aborted from the callback
    if( hr == E_FAIL ) 
        return false;

    if( FAILED(hr) ) 
    {
		// DXTRACE_ERR( TEXT("ID3DXPRTEngine"), hr );
        return false;
    }

    return true;
}

bool prtSimulator::Load(const char* directory)
{
	atString directoryName(directory);
	if (!directoryName.EndsWith("\\"))
	{
		directoryName += "\\";
	}

	DWORD options=D3DXMESH_MANAGED|D3DXMESH_32BIT;

	Displayf("loading meshes...");

	atString fileName;
	fileName = directoryName;
	fileName += "originalMesh.mesh";
	ID3DXMesh* originalMesh;
	DWORD numMaterials=~0U;
	HRESULT hr=D3DXLoadMeshFromXA(fileName,options,GRCDEVICE.GetCurrent(),NULL,&m_MaterialBuffer, NULL, &numMaterials, &originalMesh);
	if (hr!=S_OK)
	{
		Errorf("Failed to load '%s'",fileName);
		return false;
	}

	ID3DXMesh* tesselatedMesh;
	fileName = directoryName;
	fileName += "tesselatedMesh.mesh";
	hr=D3DXLoadMeshFromXA(fileName,options,GRCDEVICE.GetCurrent(),NULL,&m_MaterialBuffer, NULL, &m_NumMaterials, &tesselatedMesh);

	if (m_NumMaterials!=numMaterials)
	{
		Errorf("Tesselated mesh and original mesh # of materials don't match (%d vs. %d)",m_NumMaterials,numMaterials);
		return false;
	}

	if (hr!=S_OK)
	{
		Errorf("Failed to load '%s'",fileName);
		return false;
	}

	m_ConcatDxMesh.m_OriginalDxMesh=originalMesh;
	m_ConcatDxMesh.m_DxMesh=tesselatedMesh;
	m_Materials=(D3DXMATERIAL*)m_MaterialBuffer->GetBufferPointer();

	Displayf("loading remap info...");

	// reload the remapping info:
	prtRemapInfo* info=new prtRemapInfo;
	fileName = directoryName;
	fileName += "remappinginfo";
	info->Load(fileName);
	m_ConcatDxMesh.CopyRemap(*info);

	// verify data matches:
	if (tesselatedMesh->GetNumFaces()!=(DWORD)info->m_FaceRemaps.GetCount())
	{
		Errorf("# of face remaps (%d) don't match the # of faces (%d) in the tesselated mesh",info->m_FaceRemaps.GetCount(),tesselatedMesh->GetNumFaces());
		return false;
	}

	if (tesselatedMesh->GetNumVertices()*3!=(DWORD)info->m_VertRemaps.GetCount())
	{
		Errorf("# of vertex remaps (%d) don't match the # of vertices*3 (%d) in the tesselated mesh",info->m_VertRemaps.GetCount(),tesselatedMesh->GetNumVertices()*3);
		return false;
	}

	if (tesselatedMesh->GetNumVertices()*3!=(DWORD)info->m_VertWeights.GetCount())
	{
		Errorf("# of vertex weights (%d) don't match the # of vertices*3 (%d) in the tesselated mesh",info->m_VertWeights.GetCount(),tesselatedMesh->GetNumVertices()*3);
		return false;
	}

	Displayf("loading compressed coefficients...");
	// load the saved PRT compressed buffer:
	fileName = directoryName;
	fileName += "prtCompBuffer.pca";
	if( FAILED( hr = D3DXLoadPRTCompBufferFromFileA(fileName,&m_PrtCompBuffer) ) )
	{
		Errorf("Failed loading from %s", fileName );
		return false;
	}

	// extract the compressed data:
	ExtractCompressedDataForPRTShader(*m_ConcatDxMesh.m_DxMesh,0,0);

	return true;
}

bool prtSimulator::Save(const char* directory)
{
	atString fileName;
	atString directoryName(directory);
	if (!directoryName.EndsWith("\\"))
	{
		directoryName += "\\";
	}

	// save the directx meshes:
	DWORD flags = D3DXF_FILEFORMAT_BINARY;
	HRESULT hr;
	
	fileName = directoryName;
	fileName += "originalMesh.x";
	V_RETURN( D3DXSaveMeshToX( fileName, m_ConcatDxMesh.m_OriginalDxMesh, NULL, &m_ConcatDxMesh.m_Materials[0], NULL, m_ConcatDxMesh.m_Materials.GetCount(), flags ) );

	fileName = directoryName;
	fileName += "tesselatedMesh.x";
	V_RETURN( D3DXSaveMeshToX( fileName, m_ConcatDxMesh.m_DxMesh, NULL, &m_ConcatDxMesh.m_Materials[0], NULL, m_ConcatDxMesh.m_Materials.GetCount(), flags ) );

	// save the remapping info:
	prtRemapInfo* info=new prtRemapInfo;
	info->CopyFromArrays(m_ConcatDxMesh.m_FaceRemaps,m_ConcatDxMesh.m_DxMesh->GetNumFaces(),
						 m_ConcatDxMesh.m_VertRemaps,m_ConcatDxMesh.m_DxMesh->GetNumVertices()*3,
						 m_ConcatDxMesh.m_VertWeights,m_ConcatDxMesh.m_DxMesh->GetNumVertices()*3);
	fileName = directoryName;
	fileName += "remappinginfo";
	info->Save(fileName);
	m_ConcatDxMesh.CopyRemap(*info);

	// save the saved PRT compressed buffer:
	fileName = directoryName;
	fileName += "prtCompBuffer.pca";
	if( FAILED( hr = D3DXSavePRTCompBufferToFile( fileName, m_PrtCompBuffer ) ) )
	{
		Errorf("Failed saving to %s", fileName );
		return false;
	}

	return true;
}

void prtSimulator::OutputMesh(atString fileName,bool adaptedMesh)
{
	if (adaptedMesh && m_ConcatDxMesh.m_DxMesh==NULL)
	{
		Errorf("We don't have an adapted mesh!");
		return;
	}

	// output mesh:
	ID3DXMesh& mesh=*((m_ConcatDxMesh.m_OriginalDxMesh && !adaptedMesh)?m_ConcatDxMesh.m_OriginalDxMesh:m_ConcatDxMesh.m_DxMesh);
	m_Convert.OutputMesh(fileName,mesh,m_PrtConstants,m_NumPca,m_ConcatDxMesh.m_DxMeshMaterialNames,false);
}

u32 prtSimulator::GetOrderFromNumCoeffs()
{
	u32 numCoeffs=m_PrtCompBuffer->GetNumCoeffs();
	u32 order=1; 
	while(order*order < numCoeffs) 
		order++;

	return order;
}

void prtSimulator::UpdateLightingEnvironment(const Vector3& lightDirObjectSpace)
{
	DWORD prtOrder=GetOrderFromNumCoeffs();
	 
	float light[MAX_LIGHTS][3][D3DXSH_MAXORDER*D3DXSH_MAXORDER];  

	D3DXCOLOR lightColor(1.0f, 1.0f, 0.0f, 1.0f);        
	//lightColor *= fLightScale;
	lightColor *= 1.92f;

	// Pass in the light direction, the intensity of each channel, and it returns
	// the source radiance as an array of order^2 SH coefficients for each color channel.  
	D3DXVECTOR3 lightDirObjectSpaceDx=(D3DXVECTOR3&)lightDirObjectSpace;
	D3DXMATRIX worldInv;

	int i;
	for( i=0; i<2; i++ )
	{
		// Transform the world space light dir into object space
		// Note that if there's multiple objects using PRT in the scene, then
		// for each object you need to either evaulate the lights in object space
		// evaulate the lights in world and rotate the light coefficients 
		// into object space.
#ifdef CRAP
		D3DXVECTOR3 vLight = m_LightDirection;
		D3DXVec3TransformNormal( &lightDirObjectSpace, &vLight, &mWorldInv );
#else
		
		//lightDirObjectSpace = m_LightDirection;
		//lightDirObjectSpace.x=-0.94728065f;
		//lightDirObjectSpace.y=-0.0024213865f;
		//lightDirObjectSpace.z=-0.32039565f;
#endif

		// This sample uses D3DXSHEvalDirectionalLight(), but there's other 
		// types of lights provided by D3DXSHEval*.  Pass in the 
		// order of SH, color of the light, and the direction of the light 
		// in object space.
		// The output is the source radiance coefficients for the SH basis functions.  
		// There are 3 outputs, one for each channel (R,G,B). 
		// Each output is an array of m_dwOrder^2 floats.  
#ifdef CRAP
		if (i==0)
#endif
		{
		float coneRadius = 0.017453292f;
		V( D3DXSHEvalConeLight( prtOrder, &lightDirObjectSpaceDx, coneRadius,
			lightColor.r, lightColor.g, lightColor.b,
			light[i][0], light[i][1], light[i][2] ) );
		}
#ifdef CRAP
		else
		{
			D3DXCOLOR top;
			top.r=1.0f;
			top.g=0.0f;
			top.b=0.0f;
			top.a=1.0f;
			D3DXCOLOR bottom;
			bottom.r=0.0f;
			bottom.g=0.0f;
			bottom.b=1.0f;
			bottom.a=1.0f;
			V( D3DXSHEvalHemisphereLight( prtOrder, &lightDirObjectSpaceDx, top, bottom,light[i][0], light[i][1], light[i][2] ) );
		}
#endif
	}

	float sum[3][D3DXSH_MAXORDER*D3DXSH_MAXORDER];  
	ZeroMemory( sum, 3*D3DXSH_MAXORDER*D3DXSH_MAXORDER*sizeof(float) );

	// For multiple lights, just them sum up using D3DXSHAdd().
	for( i=0; i<m_NumActiveLights; i++ )
	{
		// D3DXSHAdd will add Order^2 floats.  There are 3 color channels, 
		// so call it 3 times.
		D3DXSHAdd( sum[0], prtOrder, sum[0], light[i][0] );
		D3DXSHAdd( sum[1], prtOrder, sum[1], light[i][1] );
		D3DXSHAdd( sum[2], prtOrder, sum[2], light[i][2] );
	}

	float env1Scaler=0.3f;
	float fLightProbe1[3][D3DXSH_MAXORDER*D3DXSH_MAXORDER];  
	D3DXSHScale( fLightProbe1[0], prtOrder, m_LightProbe.GetSHData(0), env1Scaler );
	D3DXSHScale( fLightProbe1[1], prtOrder, m_LightProbe.GetSHData(1), env1Scaler );
	D3DXSHScale( fLightProbe1[2], prtOrder, m_LightProbe.GetSHData(2), env1Scaler );
	D3DXSHAdd( sum[0], prtOrder, sum[0], fLightProbe1[0] );
	D3DXSHAdd( sum[1], prtOrder, sum[1], fLightProbe1[1] );
	D3DXSHAdd( sum[2], prtOrder, sum[2], fLightProbe1[2] );

	/*
	float fLightProbe2[3][D3DXSH_MAXORDER*D3DXSH_MAXORDER];  
	float fLightProbe2Rot[3][D3DXSH_MAXORDER*D3DXSH_MAXORDER];  
	D3DXSHScale( fLightProbe2[0], prtOrder, g_LightProbe[g_dwLightProbeB].GetSHData(0), fEnv2Scaler*fEnvBlendScaler );
	D3DXSHScale( fLightProbe2[1], prtOrder, g_LightProbe[g_dwLightProbeB].GetSHData(1), fEnv2Scaler*fEnvBlendScaler );
	D3DXSHScale( fLightProbe2[2], prtOrder, g_LightProbe[g_dwLightProbeB].GetSHData(2), fEnv2Scaler*fEnvBlendScaler );
	D3DXSHRotate( fLightProbe2Rot[0], prtOrder, &mWorldInv, fLightProbe2[0] );
	D3DXSHRotate( fLightProbe2Rot[1], prtOrder, &mWorldInv, fLightProbe2[1] );
	D3DXSHRotate( fLightProbe2Rot[2], prtOrder, &mWorldInv, fLightProbe2[2] );
	D3DXSHAdd( sum[0], prtOrder, sum[0], fLightProbe2Rot[0] );
	D3DXSHAdd( sum[1], prtOrder, sum[1], fLightProbe2Rot[1] );
	D3DXSHAdd( sum[2], prtOrder, sum[2], fLightProbe2Rot[2] );
	*/

	ComputeShaderConstants( sum[0], sum[1], sum[2], prtOrder*prtOrder );
}

void prtSimulator::ComputeShaderConstants( float* shCoeffsRed, float* shCoeffsGreen, float* shCoeffsBlue, DWORD ASSERT_ONLY(numCoeffsPerChannel) )
{
	Assert( numCoeffsPerChannel == m_PrtCompBuffer->GetNumCoeffs() );

	UINT numCoeffs   = m_PrtCompBuffer->GetNumCoeffs();
	UINT order       = GetOrderFromNumCoeffs();
	UINT numChannels = m_PrtCompBuffer->GetNumChannels();
	UINT numClusters = m_PrtCompBuffer->GetNumClusters();
	UINT numPCA      = m_PrtCompBuffer->GetNumPCA();

	//
	// With compressed PRT, a single diffuse channel is caluated by:
	//       R[p] = (M[k] dot L') + sum( w[p][j] * (B[k][j] dot L');
	// where the sum runs j between 0 and # of PCA vectors
	//       R[p] = exit radiance at point p
	//       M[k] = mean of cluster k 
	//       L' = source radiance approximated with SH coefficients
	//       w[p][j] = the j'th PCA weight for point p
	//       B[k][j] = the j'th PCA basis vector for cluster k
	//
	// Note: since both (M[k] dot L') and (B[k][j] dot L') can be computed on the CPU, 
	// these values are passed as constants using the array m_PrtConstants.   
	// 
	// So we compute an array of floats, m_PrtConstants, here.
	// This array is the L' dot M[k] and L' dot B[k][j].
	// The source radiance is the lighting environment in terms of spherical
	// harmonic coefficients which can be computed with D3DXSHEval* or D3DXSHProjectCubeMap.  
	// M[k] and B[k][j] are also in terms of spherical harmonic basis coefficients 
	// and come from ID3DXPRTCompBuffer::ExtractBasis().
	//
	DWORD clusterStride = numChannels*numPCA + 4;
	DWORD basisStride = numCoeffs*numChannels*(numPCA + 1);  

	for( DWORD cluster = 0; cluster < numClusters; cluster++ ) 
	{
		// For each cluster, store L' dot M[k] per channel, where M[k] is the mean of cluster k
		m_PrtConstants[cluster*clusterStride + 0] = D3DXSHDot( order, &m_PrtClusterBases[cluster*basisStride + 0*numCoeffs], shCoeffsRed );
		m_PrtConstants[cluster*clusterStride + 1] = D3DXSHDot( order, &m_PrtClusterBases[cluster*basisStride + 1*numCoeffs], shCoeffsGreen );
		m_PrtConstants[cluster*clusterStride + 2] = D3DXSHDot( order, &m_PrtClusterBases[cluster*basisStride + 2*numCoeffs], shCoeffsBlue );
		m_PrtConstants[cluster*clusterStride + 3] = 0.0f;

		// Then per channel we compute L' dot B[k][j], where B[k][j] is the jth PCA basis vector for cluster k
		float* pcaStart = &m_PrtConstants[cluster*clusterStride + 4];
		for( DWORD pca = 0; pca < numPCA; pca++ ) 
		{
			int nOffset = cluster*basisStride + (pca+1)*numCoeffs*numChannels;

			pcaStart[0*numPCA + pca] = D3DXSHDot( order, &m_PrtClusterBases[nOffset + 0*numCoeffs], shCoeffsRed );
			pcaStart[1*numPCA + pca] = D3DXSHDot( order, &m_PrtClusterBases[nOffset + 1*numCoeffs], shCoeffsGreen );
			pcaStart[2*numPCA + pca] = D3DXSHDot( order, &m_PrtClusterBases[nOffset + 2*numCoeffs], shCoeffsBlue );
		}
	}

	//V( m_pPRTEffect->SetFloatArray( "aPRTConstants", (float*)m_PrtConstants, numClusters*(4+numChannels*numPCA) ) );
}

Vector4 prtSimulator::GetPRTDiffuse( int clusterOffset, Vector4* pcaWeights/*[NUM_PCA/4]*/ ) const
{
	return m_Convert.GetPRTDiffuse(clusterOffset,pcaWeights,m_PrtConstants,m_NumPca);
}

//-----------------------------------------------------------------------------
// static helper function
//-----------------------------------------------------------------------------
HRESULT WINAPI StaticPRTSimulatorCB( float fPercentDone, LPVOID pParam )
{
    prtState* theState = (prtState*)pParam;

    EnterCriticalSection( &theState->m_Cs );
    theState->m_PercentDone = fPercentDone;

    // In this callback, returning anything except S_OK will stop the simulator
    HRESULT hr = S_OK;
    if( theState->m_StopSimulator )
        hr = E_FAIL; 

    LeaveCriticalSection( &theState->m_Cs );

    return hr;
}

//----------------------------------------------------------------
//  traits for vectors
//----------------------------------------------------------------

template<class T>
struct traits
{};

template<>
struct traits<Vector4>
{
	static Vector4		MinimumValue()	{ return Vector4( -FLT_MAX, -FLT_MAX, -FLT_MAX, -FLT_MAX ); }
	static Vector4		MaximumValue()	{ return Vector4( FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX ); }
	static const char*	SvaName()		{ return "Vector4"; }
	grcImage::Format	Fileformat()	{ return grcImage::DXT5; }
};

template<>
struct traits<Vector3>
{
	static Vector3 MinimumValue() { return Vector3( -FLT_MAX, -FLT_MAX, -FLT_MAX); }
	static Vector3 MaximumValue() { return Vector3( FLT_MAX, FLT_MAX, FLT_MAX ); }
	static const char* SvaName()	{ return "Vector3"; }
	grcImage::Format	Fileformat() { return grcImage::DXT1; }
};

std::ostream& operator<<( std::ostream& out , const Vector4& val )
{
	out << val.x << " " << val.y << " " << val.z << " " << val.w;
	return out;
} 
std::ostream& operator<<( std::ostream& out , const Vector3& val )
{
	out << val.x << " " << val.y << " " << val.z;
	return out;
} 
//
//	PURPOSE
//		Stores the first four coefficients out as a 8 bit per channel texture
//		it only uses the first channel and also stores out a text file listing
//		the scales to rescale from 0 - 1 to the coefficient range
//


template<class VECTOR>
void QuantizeVectorAndSaveAsTexture( VECTOR*	results, 
									int width, 
									int height, 
									std::ofstream& out, 
									const char* fileName, 
									const char* texName,
									const char* prefix )
{
	VECTOR			maximum = traits<VECTOR>::MinimumValue();
	VECTOR			minimum = traits<VECTOR>::MaximumValue();

	// find max and min
	int numSamples = width * height;
	for ( int i = 0; i < numSamples ; i++ )
	{
		maximum.Max( results[i], maximum );
		minimum.Min( results[i], minimum );
	}

	// now rescale
	VECTOR  invScale =  ( maximum - minimum );
	invScale.InvertSafe();

	for ( int i = 0; i < numSamples; i++ )
	{
		results[i] = ( results[i] - minimum ) * invScale;
		Assert( results[i].x >= -0.0001f && results[i].x <= 1.0f );
		Assert( results[i].y >= -0.0001f && results[i].y <= 1.0f );
		Assert( results[i].z >= -0.0001f && results[i].z <= 1.0f );
	}
	// And now save as texture
	SaveVectorAsTexture( fileName, results , width, height );

	// output sva values
	if ( out )
	{
		VECTOR quantScale = ( maximum - minimum );
		VECTOR quantOffset = ( minimum );
		out << prefix << "TextureScale \n { \n\t " << traits<VECTOR>::SvaName() << " " << quantScale << "\n } \n";
		out << prefix << "TextureShift \n { \n\t " << traits<VECTOR>::SvaName() << " "  << quantOffset << "\n } \n";	
		out << prefix << "Texture \n { \n\t grcTexture " << texName  << ".dds \n } \n";	
	}
}

static void SavePRTCoeffsToQuantizedTexture( ID3DXPRTBuffer* prt , const char* path, const char* prefix  )
{
	Assert( prt->IsTexture() );
	
	Vector4*		results = new Vector4[ prt->GetNumSamples() ];

	float*	coeffs;
	V( prt->LockBuffer( 0, prt->GetNumSamples(), &coeffs)  );

	int packetSize = prt->GetNumChannels() * prt->GetNumCoeffs();
	int idx = 0;

	// now get the coefficients we want into vector form and calculate the range
	for ( unsigned int i = 0 ; i < prt->GetNumSamples(); i++ , idx += packetSize )
	{
		// NOTE : rearranged coefficents so should work better with DXT5 compression
		results[i].w = coeffs[ idx ];
		results[i].x = coeffs[ idx + 1];
		results[i].y = coeffs[ idx + 2];
		results[i].z = coeffs[ idx + 3];
	};	

	V( prt->UnlockBuffer() );

	std::string filePath = std::string( path ) + std::string( "\\qantprt.sva" );
	std::ofstream	out( filePath.c_str() );
	Assert( out );

	std::string  pathName = std::string( path ) + "\\\\" + std::string( prefix ) + "_quantprt.dds";
	std::string  texName =  std::string(prefix ) + "_quantprt";

	QuantizeVectorAndSaveAsTexture( results , 
									prt->GetWidth(), 
									prt->GetHeight(), 
									out, 
									pathName.c_str(), 
									texName.c_str(), 
									"QuantPrt" );

	delete [] results;
}
static void Save3rdOrderPRTCoeffsToQuantizedTextures( ID3DXPRTBuffer* prt , const char* path, const char* prefix  )
{
	Assert( prt->IsTexture() );
	Assert( prt->GetNumCoeffs() >= 9 );
	float*	coeffs;

	Vector3*		results1 = new Vector3[ prt->GetNumSamples() ];
	Vector3*		results2 = new Vector3[ prt->GetNumSamples() ];
	Vector3*		results3 = new Vector3[ prt->GetNumSamples() ];

	V( prt->LockBuffer( 0, prt->GetNumSamples(), &coeffs)  );

	int packetSize = prt->GetNumChannels() * prt->GetNumCoeffs();
	int	idx = 0;
	// now get the coefficients we want into vector form and calculate the range
	for ( unsigned int i = 0; i < prt->GetNumSamples(); i++ , idx += packetSize )
	{
		// NOTE : arranged for 3 Dxt1 texture compression
		//  and for fast computation on other side
		results1[i].x = coeffs[ idx + 1];	// directional x,y,z term
		results1[i].y = coeffs[ idx + 2 ];
		results1[i].z = coeffs[ idx + 3];

		results2[i].x = coeffs[ idx + 4];  // directional x2, y2, z2 term
		results2[i].y = coeffs[ idx + 5];
		results2[i].z = coeffs[ idx + 6];

		results3[i].x = coeffs[ idx + 7];	// weird constant terms
		results3[i].y = coeffs[ idx + 8];
		results3[i].z = coeffs[ idx + 0];
	};	


	V( prt->UnlockBuffer() );

	std::string filePath = std::string( path ) + std::string( "\\qantprt3rd.sva" );
	std::ofstream	out( filePath.c_str() );
	Assert( out );

	std::string  pathName = std::string( path ) + "\\\\" + std::string( prefix ) + "_quantprt3rd1.dds";
	std::string  texName =  std::string(prefix ) + "_quantprt3rd1";

	QuantizeVectorAndSaveAsTexture( results1 , prt->GetWidth(), prt->GetHeight(), out, pathName.c_str(), texName.c_str(), "QuantPrt3rd1" );

	pathName = std::string( path ) + "\\\\" + std::string( prefix ) + "_quantprt3rd2.dds";
	texName =  std::string(prefix ) + "_quantprt3rd2";

	QuantizeVectorAndSaveAsTexture( results2 , prt->GetWidth(), prt->GetHeight(), out, pathName.c_str(), texName.c_str(), "QuantPrt3rd2" );

	pathName = std::string( path ) + "\\\\" + std::string( prefix ) + "_quantprt3rd3.dds";
	texName =  std::string(prefix ) + "_quantprt3rd3";
	QuantizeVectorAndSaveAsTexture( results3 , prt->GetWidth(), prt->GetHeight(), out, pathName.c_str(), texName.c_str(), "QuantPrt3rd3" );

	delete [] results1;
	delete [] results2;
	delete [] results3;
}

static void SaveVectorAsTexture( const char* fileName, Vector4* vals, int width, int height )
{
	grcImage*   textureImage=grcImage::Create( width, height,1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);// , grcImage::A8R8G8B8
	Assert( textureImage );

	// loop over each pixel in the map:
	for (int x=0; x< width ; x++)
	{
		for ( int y = 0; y < height; y++ )
		{
			// compute and store color
			Vector4 v = vals[ x + y * width ];
			Color32 color( v.x, v.y, v.z, v.w );
			textureImage->SetPixel( x,y,color.GetColor());
		}
	}
	 textureImage->SaveDDS( fileName );
	//Assert( result );

	textureImage->Release();
}
	
static void SaveVectorAsTexture( const char* fileName, Vector3* vals, int width, int height )
{
	grcImage*   textureImage=grcImage::Create( width, height,1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);// , grcImage::A8R8G8B8
	Assert( textureImage );

	// loop over each pixel in the map:
	for (int x=0; x< width ; x++)
	{
		for ( int y = 0; y < height; y++ )
		{
			// compute and store color
			Vector3 v = vals[ x + y * width ];
			Color32 color( v.x, v.y, v.z, 1.0f );
			textureImage->SetPixel( x,y,color.GetColor());
		}
	}
	textureImage->SaveDDS( fileName );

	textureImage->Release();
	//Assert( result );
}

} // namespace rage
