// 
// prttools/ambientocclusion.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PRTTOOLS_AMBIENTOCCLUSION_H
#define PRTTOOLS_AMBIENTOCCLUSION_H

#include "direct.h"

#include "atl/array.h"
#include "grcore/viewport.h"

namespace rage {

class grmShader;
class grmShaderGroup;

class pltLightMapAmbientOcclusion : public pltLightMapDirect
{
public:
	static const float NEAR_CLIP_PLANE;
	static const float FAR_CLIP_PLANE;
	static const float HORIZONTAL_FIELD_OF_VIEW;

	enum {LEXEL_VIEW_WIDTH=512,LEXEL_VIEW_HEIGHT=512,LEXEL_VIEW_NUM_PIXELS=LEXEL_VIEW_WIDTH*LEXEL_VIEW_HEIGHT};

	pltLightMapAmbientOcclusion(pltLightMapDirect& lightMapDirect,int numViewpoints);

	virtual void Init(float =0.4f,float =4.0f)
	{
		// we intentionally don't call the base class Init()!
	}

	virtual ID3DXMesh& GetDxMesh()
	{
		return m_LightMapDirect->GetDxMesh();
	}

	virtual void SetDxMesh(ID3DXMesh&)
	{
		Quitf("Progressive Radiosity Maps don't need to set the mesh");
	}

	void FillLightMap(atArray<prtGeometry*>& geometries);

protected:
	void RenderToTexture();

private:
	Vector3					m_CurrentTexelPosition;
	Vector3					m_CurrentTexelNormal;
	grcViewport				m_Viewport;
	atArray<prtGeometry*>	m_Geometries;
	pltLightMapDirect*		m_LightMapDirect;
	IDirect3DQuery9**		m_OcclusionQueries;
	int						m_CurrentQuery;
	Vector4					m_CombinedBoundSphere;
	int						m_NumViewpoints;
};

}; // namespace rage

#endif //PRTTOOLS_AMBIENTOCCLUSION_H
