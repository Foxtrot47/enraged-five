// 
// prttools/direct.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "direct.h"

#include "grcore/im.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grmodel/model.h"
#include "phcore/segment.h"
#include "physics/levelnew.h"
#include "system/bootmgr.h"
#include "system/timer.h"
#include "vectormath/vec3v.h"


namespace rage {


const float		prtDirectLighting::NEAR_CLIP_PLANE			=0.01f;
const float		prtDirectLighting::FAR_CLIP_PLANE			=100.0f;
const float		prtDirectLighting::HORIZONTAL_FIELD_OF_VIEW	=170.0f;

grmShaderGroup*	pltLightMapDirect::sm_AllBlackShaderGroup;
grmShader*		pltLightMapDirect::sm_AllBlackShader;

//#define SAVE_INDEX_MAP

void pltLightMapDirect::InitClass()
{
	sm_AllBlackShaderGroup = grmShaderFactory::GetInstance().CreateGroup();
	Assert(sm_AllBlackShaderGroup);

#if __TOOL
	grmShaderFactory::GetInstance().SetShaderLibPath("tune\\shaders\\lib");
#endif
	sm_AllBlackShader = grmShaderFactory::GetInstance().Create("allblack");
	Assert(sm_AllBlackShader);
	AssertVerify(sm_AllBlackShader->Load("allblack", 0 ,0, NULL));
}

pltLightMapDirect::pltLightMapDirect(prtDirectLighting& direct,prtConvert& convert,prtGeometry& geometry,int meshIndex,int width,int height) : 
pltLightMap(width,height),
	m_Convert(&convert),
	m_Geometry(&geometry),
	m_MeshIndex(meshIndex),
	m_Direct(&direct),
	m_IndexMapSurface(NULL)
{
	m_Viewport.Perspective(prtDirectLighting::HORIZONTAL_FIELD_OF_VIEW,0.0f,prtDirectLighting::NEAR_CLIP_PLANE,prtDirectLighting::FAR_CLIP_PLANE);
}

bool pltLightMapDirect::GetPosAndNormalFromIndexMap(uvaUvAtlasHelper::Vertex vertices[],u32 indices[],Color32 pixelsRead[],u32 x,u32 y,Vector3& outPos,Vector3& outNrm)
{
	u32 texel=GetTexel(pixelsRead,x,y).GetColor();

	// unused color:
	if (texel==ILLEGAL_TEXEL_COLOR)
	{
		return false;
	}

	// remove opaque alpha:
	texel&=0x00ffffff;
	texel--;

	uvaUvAtlasHelper::Vertex& vert0=vertices[indices[(texel*3)+0]];
	uvaUvAtlasHelper::Vertex& vert1=vertices[indices[(texel*3)+1]];
	uvaUvAtlasHelper::Vertex& vert2=vertices[indices[(texel*3)+2]];

	// solve for the barycentric coordinates (see http://www.algebra.com/algebra/about/history/Barycentric-coordinates.wikipedia for explanation):
	float a = (vert0.m_Texcoord2.x*(float)GetTexWidth())-(vert2.m_Texcoord2.x*(float)GetTexWidth());
	float b = (vert1.m_Texcoord2.x*(float)GetTexWidth())-(vert2.m_Texcoord2.x*(float)GetTexWidth());
	float c = (vert2.m_Texcoord2.x*(float)GetTexWidth())-(float)x;

	float d = (vert0.m_Texcoord2.y*(float)GetTexHeight())-(vert2.m_Texcoord2.y*(float)GetTexHeight());
	float e = (vert1.m_Texcoord2.y*(float)GetTexHeight())-(vert2.m_Texcoord2.y*(float)GetTexHeight());
	float f = (vert2.m_Texcoord2.y*(float)GetTexHeight())-(float)y;

	// compute weights:
	float weight0,weight1,weight2;
	if ((a*e - b*d)==0)
	{
		weight0=1.0f;
		weight1=0.0f;
		weight2=0.0f;
	}
	else 
	{
		weight0=(b*f - c*e)/
			(a*e - b*d);
		weight1=(a*f - c*d)/
			(b*d - a*e);
		weight2=1.0f-weight0-weight1;
	}

	// compute position:
	outPos=vert0.m_Position;
	outPos.Scale(weight0);
	Vector3 texelPositionTemp(vert1.m_Position);
	texelPositionTemp.Scale(weight1);
	outPos.Add(texelPositionTemp);
	texelPositionTemp.Scale(vert2.m_Position,weight2);
	outPos.Add(texelPositionTemp);

	// compute normal:
	outNrm=vert0.m_Normal;
	outNrm.Scale(weight0);
	Vector3 texelNormalTemp(vert1.m_Normal.x,vert1.m_Normal.y,vert1.m_Normal.z);
	texelNormalTemp.Scale(weight1);
	outNrm.Add(texelNormalTemp);
	texelNormalTemp.Scale(vert2.m_Normal,weight2);
	outNrm.Add(texelNormalTemp);
	outNrm.Normalize();

	return true;
}

void pltLightMapDirect::PrintProgress(int& lastLen,float totalTime,int y)
{
	char strPos[128];

	if (sysBootManager::IsDebuggerPresent())
	{
		Printf("Line #");
	}
	else
	{
		// backspace:
		if (lastLen!=0)
		{
			while (lastLen-->0)
			{
				Printf("\b");
			}
		}
	}

	if (y==0)
		sprintf(strPos,"%d",y);
	else
		sprintf(strPos,"%d (Approx. time to completion: %0.2f)",y,(totalTime/(float)(y))*(float)(GetTexHeight()-y));

	lastLen=strlen(strPos);
	if (sysBootManager::IsDebuggerPresent())
		Displayf(strPos);
	else
		Printf(strPos);
}

void pltLightMapDirect::RenderFromLightPOVBetter(atArray<prtGeometry*>& geometries)
{	
	// render to the light map, and then do the direct lighting rendering:
	Render();

	m_IndexMapSurface=m_TextureRenderTarget;

	Matrix34 cameraMatrix;

	int renderTargetWidth=GetTexWidth();
	int renderTargetHeight=GetTexHeight();

	// lock buffers:
	u32* indices=NULL;
	GetDxMesh().LockIndexBuffer(0x0,(LPVOID*)&indices);

	uvaUvAtlasHelper::Vertex* vertices=NULL;
	GetDxMesh().LockVertexBuffer(0x0,(LPVOID*)&vertices);

	// create a render target to handle the texture:
	grcRenderTarget*	renderTarget			=grcTextureFactory::GetInstance().CreateRenderTarget("directLexelviewColor",
		grcrtPermanent,
		renderTargetWidth,
		renderTargetHeight,
		32);
	grcRenderTarget*	renderTargetDepth		=grcTextureFactory::GetInstance().CreateRenderTarget("directLexelviewDepth",
		grcrtDepthBuffer,
		renderTargetWidth,
		renderTargetHeight,
		32);
	grcRenderTarget*	renderTargetFinal			=grcTextureFactory::GetInstance().CreateRenderTarget("directLexelviewColorFinal",
		grcrtPermanent,
		renderTargetWidth,
		renderTargetHeight,
		32);

	// clear the lightmap render target:
	// use the current render target, and render into the lightmap render target:
	grcTextureFactory::GetInstance().LockRenderTarget(0, renderTargetFinal, renderTargetDepth,grcPositiveX,true);
	GRCDEVICE.Clear(true,Color32(ILLEGAL_TEXEL_COLOR),true,grcDepthFarthest,0);
	grcTextureFactory::GetInstance().UnlockRenderTarget(0);

#ifdef SAVE_INDEX_MAP
	{
		// read back from the texture (this will probably only work on the PC):
		D3DLOCKED_RECT	rectRead;
		HRESULT hr;
		hr=m_TextureRenderTarget->LockRect(&rectRead, NULL, D3DLOCK_READONLY);
		Assert(hr==S_OK);

		Color32* pixelsRead=(Color32*)rectRead.pBits;
		grcImage* indexMapImage=grcImage::Create(GetTexWidth(),GetTexHeight(),1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
		for (int y=0;y<GetTexHeight();y++)
		{
			for (int x=0;x<GetTexWidth();x++)
			{
				Color32 color=GetTexel(pixelsRead,x,y);
				if (color.GetColor()!=0x00000000)
				{
					color.Set(color.GetColor()*100);
					indexMapImage->SetPixel(x,y,color.GetColor());
				}
				else
				{
					indexMapImage->SetPixel(x,y,0xffffff00);
				}
			}

		}

		m_TextureRenderTarget->UnlockRect();

		indexMapImage->SaveDDS("c:\\indexmap.dds");
	}
#endif

//#define SAVE_IMAGE_POV
#ifdef SAVE_IMAGE_POV
	grcImage* image=grcImage::Create(renderTargetWidth,renderTargetHeight,1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
#endif

	// go over all the faces and render each triangle direct lighting for each face:
	for (int light=0;light<prtDirectLighting::LIGHT_COUNT;light++)
	{
		for (UINT i=0;i<GetDxMesh().GetNumFaces()*3;i+=3)
		{
			// get triangle:
			uvaUvAtlasHelper::Vertex verts[3];
			verts[0]=vertices[indices[i+0]];
			verts[1]=vertices[indices[i+1]];
			verts[2]=vertices[indices[i+2]];

			// compute face normal:
			Vector3 rightEdge(verts[2].m_Position),leftEdge(verts[1].m_Position);
			leftEdge.Subtract(verts[0].m_Position);
			rightEdge.Subtract(verts[0].m_Position);
			Vector3 polyNormal(leftEdge);
			polyNormal.Cross(leftEdge,rightEdge);
			polyNormal.Normalize();

			Vector2 texCoordsTri[3];

			bool backface=geomPoints::IsPointBehindPlane(m_Direct->m_Lights[light].m_Position,verts[0].m_Position,polyNormal);
			// polygon is backfacing to light - skip this triangle:
			if (!backface)
			{
				grcTextureFactory::GetInstance().LockRenderTarget(0, renderTarget, renderTargetDepth);
		
				// clear render target:
				Color32 clearColor(ILLEGAL_TEXEL_COLOR);
				GRCDEVICE.Clear(true,clearColor,true,grcDepthFarthest,0);

				// set camera position and orientation so it's looking at the polygon:
				cameraMatrix.Identity();
				cameraMatrix.c.Set(polyNormal.x,polyNormal.y,polyNormal.z);
				cameraMatrix.c.MakeOrthonormals(cameraMatrix.a,cameraMatrix.b);
				cameraMatrix.d=m_Direct->m_Lights[light].m_Position;

				Vector3 vertsInCameraSpace[3];
				cameraMatrix.UnTransform(verts[0].m_Position,vertsInCameraSpace[0]);
				cameraMatrix.UnTransform(verts[1].m_Position,vertsInCameraSpace[1]);
				cameraMatrix.UnTransform(verts[2].m_Position,vertsInCameraSpace[2]);

				int maxIndex[2],minIndex[2];
				Vector2 max,min;
				// only loop over x and y:
				for (int p=0;p<2;p++)
				{
					// find the top and bottom points:
					for (int v=0;v<3;v++)
					{
						if (v==0 || vertsInCameraSpace[v][p]>max[p])
						{
							max[p]=vertsInCameraSpace[v][p];
							maxIndex[p]=v;
						}

						if (v==0 || vertsInCameraSpace[v][p]<min[p])
						{
							min[p]=vertsInCameraSpace[v][p];
							minIndex[p]=v;
						}
					}
				}

				enum
				{
					POS_LEFT_BOT	=0,	
					POS_LEFT_TOP	=1,
					POS_RIGHT_BOT	=2,
					POS_RIGHT_TOP	=3
				};
				// figure out worldspace location of view plane:
				Vector3 worldPosLightmap[4];

				// left bot:
				worldPosLightmap[POS_LEFT_BOT]=verts[minIndex[0]].m_Position;
				if (vertsInCameraSpace[minIndex[0]].y>vertsInCameraSpace[minIndex[1]].y)
				{
					float scaleValue=vertsInCameraSpace[minIndex[1]].y-vertsInCameraSpace[minIndex[0]].y;
					worldPosLightmap[POS_LEFT_BOT].AddScaled(cameraMatrix.b,scaleValue);
				}

				// left top:
				worldPosLightmap[POS_LEFT_TOP]=verts[minIndex[0]].m_Position;
				if (vertsInCameraSpace[maxIndex[1]].y>vertsInCameraSpace[minIndex[0]].y)
				{
					float scaleValue=vertsInCameraSpace[maxIndex[1]].y-vertsInCameraSpace[minIndex[0]].y;
					worldPosLightmap[POS_LEFT_TOP].AddScaled(cameraMatrix.b,scaleValue);
				}

				// right bot:
				worldPosLightmap[POS_RIGHT_BOT]=verts[maxIndex[0]].m_Position;
				if (vertsInCameraSpace[maxIndex[0]].y>vertsInCameraSpace[minIndex[1]].y)
				{
					float scaleValue=vertsInCameraSpace[minIndex[1]].y-vertsInCameraSpace[maxIndex[0]].y;
					worldPosLightmap[POS_RIGHT_BOT].AddScaled(cameraMatrix.b,scaleValue);
				}

				// right top:
				worldPosLightmap[POS_RIGHT_TOP]=verts[maxIndex[0]].m_Position;
				if (vertsInCameraSpace[maxIndex[1]].y>vertsInCameraSpace[maxIndex[0]].y)
				{
					float scaleValue=vertsInCameraSpace[maxIndex[1]].y-vertsInCameraSpace[maxIndex[0]].y;
					worldPosLightmap[POS_RIGHT_TOP].AddScaled(cameraMatrix.b,scaleValue);
				}
		
				// modify camera matrix so it's rotated around the z axis so it lines up with the lightmap:
				cameraMatrix.a.Subtract(worldPosLightmap[POS_RIGHT_BOT],worldPosLightmap[POS_LEFT_BOT]);
				cameraMatrix.a.Normalize();
				cameraMatrix.b.Subtract(worldPosLightmap[POS_LEFT_TOP],worldPosLightmap[POS_LEFT_BOT]);
				cameraMatrix.b.Normalize();

				// find the distance from the plane to the origin:
				Vector3 positionOnPlane(cameraMatrix.d);
				Vector3 cameraLookDownAxis(cameraMatrix.c);
				positionOnPlane.AddScaled(Vector3(-cameraLookDownAxis.x,-cameraLookDownAxis.y,-cameraLookDownAxis.z),prtDirectLighting::NEAR_CLIP_PLANE);
				float planeDistanceToOrigin=cameraLookDownAxis.Dot(positionOnPlane);

				// find the world space positions of the off axis projection corners:
				Vector4 thePlane(cameraMatrix.c.x,cameraMatrix.c.y,cameraMatrix.c.z,planeDistanceToOrigin);
				Vector3 cameraSpaceProj[4];
				Vector3 worldSpaceProj[4];
				for (int c=0;c<4;c++)
				{
					Vector3 raySlope(cameraMatrix.d);
					raySlope.Subtract(worldPosLightmap[c]);
					raySlope.Normalize();
					float tValue;
					AssertVerify(geomSegments::CollideRayPlane(worldPosLightmap[c],raySlope,thePlane,&tValue));
					worldSpaceProj[c].AddScaled(worldPosLightmap[c],raySlope,tValue);
					cameraMatrix.UnTransform(worldSpaceProj[c],cameraSpaceProj[c]);
				}

				// compute projection matrix: 
				D3DXMATRIX matrixD3d;
				D3DXMatrixPerspectiveOffCenterRH(&matrixD3d,
					cameraSpaceProj[0].x,
					cameraSpaceProj[2].x,
					cameraSpaceProj[0].y,
					cameraSpaceProj[1].y,
					prtDirectLighting::NEAR_CLIP_PLANE,prtDirectLighting::FAR_CLIP_PLANE);
				Matrix44 matrixProj;

				matrixProj.a.x=matrixD3d.m[0][0];
				matrixProj.a.y=matrixD3d.m[0][1];
				matrixProj.a.z=matrixD3d.m[0][2];
				matrixProj.a.w=matrixD3d.m[0][3];

				matrixProj.b.x=matrixD3d.m[1][0];
				matrixProj.b.y=matrixD3d.m[1][1];
				matrixProj.b.z=matrixD3d.m[1][2];
				matrixProj.b.w=matrixD3d.m[1][3];

				matrixProj.c.x=matrixD3d.m[2][0];
				matrixProj.c.y=matrixD3d.m[2][1];
				matrixProj.c.z=matrixD3d.m[2][2];
				matrixProj.c.w=matrixD3d.m[2][3];

				matrixProj.d.x=matrixD3d.m[3][0];
				matrixProj.d.y=matrixD3d.m[3][1];
				matrixProj.d.z=matrixD3d.m[3][2];
				matrixProj.d.w=matrixD3d.m[3][3];

				// setup camera:
				grcState::Default();
				grcState::Default();
				grcState::SetCullMode(grccmNone);
				grcViewport::SetCurrent(&m_Viewport);
				m_Viewport.ResetWindow();
				grcViewport::SetCurrentWorldIdentity();
				m_Viewport.SetCameraMtx(cameraMatrix);
				m_Viewport.SetProjection(matrixProj);

				grcBindTexture(NULL);

				grcLightState::SetEnabled(false);
				grcState::SetDepthWrite(true);
				

#if __D3D
				// set up the stencil buffer to always write:
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILENABLE,			TRUE);
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILFUNC,			D3DCMP_ALWAYS);
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILZFAIL,			D3DSTENCILOP_KEEP);
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILFAIL,			D3DSTENCILOP_KEEP);
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILREF,			0x1);
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILWRITEMASK,		0x1);
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILMASK,			0x1);
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILPASS,			D3DSTENCILOP_REPLACE);
#endif

				// clear stencil and zbuffer ONLY:
				//GRCDEVICE.Clear(false,clearColor,true,grcDepthFarthest,0);

				Color32 white(255,255,255);

				// draw this triangle first:
				grcBegin(drawTris,3);
				grcVertex(vertices[indices[i+0]].m_Position.x,vertices[indices[i+0]].m_Position.y,vertices[indices[i+0]].m_Position.z,0,1,0,white,0.0f,0.0f);
				grcVertex(vertices[indices[i+1]].m_Position.x,vertices[indices[i+1]].m_Position.y,vertices[indices[i+1]].m_Position.z,-1,0,0,white,0.0f,0.0f);
				grcVertex(vertices[indices[i+2]].m_Position.x,vertices[indices[i+2]].m_Position.y,vertices[indices[i+2]].m_Position.z,1,0,0,white,0.0f,0.0f);
				grcEnd();

#if __D3D
				// compare vs. stencil buffer:
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILFUNC,D3DCMP_EQUAL);
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILREF,0x1);
#endif

				grmModel::SetForceShader(sm_AllBlackShader);
				grcState::SetDepthTest(true);
				grcState::Default();

				// render all geometries, normal pass:
				for (int g=0;g<geometries.GetCount();g++)
				{
					geometries[g]->Render(true);
				}

				// HACK: for some reason drawables screw with drawing the final stretched lightmap, so we draw an invisible polygon to get the drawable stuff
				// to show up:
				grcState::SetDepthWrite(false);
				grcState::SetColorWrite(grccwNone);
				grcBegin(drawTris,3);
				grcVertex(vertices[indices[0]].m_Position.x,vertices[indices[0]].m_Position.y,vertices[indices[0]].m_Position.z,0,1,0,Color32(0,0,0,0),0.0f,0.0f);
				grcVertex(vertices[indices[1]].m_Position.x,vertices[indices[1]].m_Position.y,vertices[indices[1]].m_Position.z,-1,0,0,Color32(0,0,0,0),0.0f,0.0f);
				grcVertex(vertices[indices[2]].m_Position.x,vertices[indices[2]].m_Position.y,vertices[indices[2]].m_Position.z,1,0,0,Color32(0,0,0,0),0.0f,0.0f);
				grcEnd();
	
				grmModel::SetForceShader(NULL);

				grcState::Default();
				grcState::Default();

				grcViewport::SetCurrent(NULL);

				grcTextureFactory::GetInstance().UnlockRenderTarget(0);

#ifdef SAVE_IMAGE_POV
				IDirect3DSurface9* surfaceSource=NULL;
				IDirect3DSurface9* surfaceDestResult=CreateOffscreenSurfaceStatic(renderTargetWidth,renderTargetHeight);

				static_cast<IDirect3DTexture9*>(renderTarget->GetTexturePtr())->GetSurfaceLevel(0,&surfaceSource);
				GRCDEVICE.GetCurrent()->GetRenderTargetData(surfaceSource, surfaceDestResult);

				Color32* colors=LockImageStatic(*surfaceDestResult);

				for (int lexelY=0;lexelY<renderTargetWidth;lexelY++)
					for (int lexelX=0;lexelX<renderTargetHeight;lexelX++)
					{
						Color32 color=GetTexel(colors,lexelX,lexelY,renderTargetWidth);
						image->SetPixel(lexelX,lexelY,color.GetColor());
					}

				char name[128];
				sprintf(name,"d:\\lightmaps\\%d.dds",i/3);
				image->SaveDDS(name);

				UnlockImageStatic(*surfaceDestResult);
				surfaceSource->Release();
				surfaceDestResult->Release();
#endif

#if __D3D
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILENABLE, FALSE);
				GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_STENCILFUNC,D3DCMP_ALWAYS);
#endif

				// compute texture coordinates for rendered triangle:
				cameraMatrix.UnTransform(verts[0].m_Position,vertsInCameraSpace[0]);
				cameraMatrix.UnTransform(verts[1].m_Position,vertsInCameraSpace[1]);
				cameraMatrix.UnTransform(verts[2].m_Position,vertsInCameraSpace[2]);

				cameraMatrix.UnTransform(worldPosLightmap[POS_LEFT_BOT]);
				cameraMatrix.UnTransform(worldPosLightmap[POS_RIGHT_TOP]);

				Vector2 scale;
				scale.x=worldPosLightmap[POS_RIGHT_TOP].x-worldPosLightmap[POS_LEFT_BOT].x;
				scale.y=worldPosLightmap[POS_RIGHT_TOP].y-worldPosLightmap[POS_LEFT_BOT].y;

				for (int i=0;i<3;i++)
				{
					texCoordsTri[i].Set(vertsInCameraSpace[i].x,vertsInCameraSpace[i].y);
					texCoordsTri[i].Subtract(Vector2(worldPosLightmap[POS_LEFT_BOT].x,worldPosLightmap[POS_LEFT_BOT].y));
					texCoordsTri[i].x/=scale.x;
					texCoordsTri[i].y/=scale.y;
				}
			}

			// use the current render target, and render into the lightmap render target:
			grcTextureFactory::GetInstance().LockRenderTarget(0, renderTargetFinal, renderTargetDepth,grcPositiveX,true);

			grcState::Default();
			grcState::Default();

			grcViewport viewport;
			viewport.Ortho(0.0f,1.0f,0.0f,1.0f,0.0f,1.0f);
			grcViewport::SetCurrent(&viewport);

			grcViewport::SetCurrentWorldIdentity();
			m_Viewport.SetCameraMtx(M34_IDENTITY);
			grcBindTexture(backface?grcTexture::None:renderTarget);

			grcLightState::SetEnabled(false);
			grcState::SetCullMode(grccmNone);
			grcState::SetDepthTest(false);
			grcState::SetAlphaBlend(false);
			

			// draw this triangle first:
			grcBegin(drawTris,3);
			grcColor(backface?Color32(0,0,0,255):Color32(255,255,255,255));
			grcTexCoord2f(texCoordsTri[0].x,-texCoordsTri[0].y);
			grcVertex2f(verts[0].m_Texcoord2);
			grcTexCoord2f(texCoordsTri[1].x,-texCoordsTri[1].y);
			grcVertex2f(verts[1].m_Texcoord2);
			grcTexCoord2f(texCoordsTri[2].x,-texCoordsTri[2].y);
			grcVertex2f(verts[2].m_Texcoord2);
			grcEnd();

			grcViewport::SetCurrent(NULL);

			grcTextureFactory::GetInstance().UnlockRenderTarget(0);
		}
	}

	// lock images:
	IDirect3DSurface9* surfaceSourceFinal=NULL;
	IDirect3DSurface9* surfaceDestResultFinal=CreateOffscreenSurfaceStatic(renderTargetWidth,renderTargetHeight);
	static_cast<IDirect3DTexture9*>(renderTargetFinal->GetTexturePtr())->GetSurfaceLevel(0,&surfaceSourceFinal);
	GRCDEVICE.GetCurrent()->GetRenderTargetData(surfaceSourceFinal, surfaceDestResultFinal);
	Color32* pixelsRead=LockImageStatic(*surfaceDestResultFinal);
	Color32* pixelsWrite=LockImage();

	int addedTexelsCount=renderTargetWidth*renderTargetHeight;
	bool* addedTexels	=new bool[addedTexelsCount];
	memset(addedTexels,addedTexelsCount,sizeof(bool));

#ifdef SAVE_IMAGE_POV
	for (int lexelY=0;lexelY<renderTargetWidth;lexelY++)
		for (int lexelX=0;lexelX<renderTargetHeight;lexelX++)
		{
			Color32 color=GetTexel(pixelsRead,lexelX,lexelY,renderTargetWidth);
			image->SetPixel(lexelX,lexelY,color.GetColor());
		}

		image->SaveDDS("d:\\lightmaps\\full_before");
#endif
	
	// flip vertically:
	for (int lexelY=0;lexelY<renderTargetWidth;lexelY++)
		for (int lexelX=0;lexelX<renderTargetHeight;lexelX++)
		{
			Color32 colorTop	=GetTexel(pixelsRead,lexelX,lexelY,renderTargetWidth);
			SetTexel(pixelsWrite,lexelX,renderTargetHeight-lexelY-1,colorTop);
			//SetTexel(pixelsWrite,lexelX,lexelY,colorTop);
		}

	AddSmearedBorder(addedTexels,pixelsWrite,pixelsWrite,2.0f);

	// clean up:
	delete [] addedTexels;

	UnlockImage();
	UnlockImageStatic(*surfaceDestResultFinal);

	surfaceDestResultFinal->Release();
	surfaceSourceFinal->Release();

	renderTarget->Release();
	renderTargetDepth->Release();
	renderTargetFinal->Release();
}

bool pltLightMapDirect::FillLightMap()
{
	// render to the light map, and then do the direct lighting rendering:
	Render();

	m_IndexMapSurface=m_TextureRenderTarget;

	// read back from the texture (this will probably only work on the PC):
	D3DLOCKED_RECT	rectRead,rectWrite;
	V_RETURN(m_IndexMapSurface->LockRect(&rectRead, NULL, D3DLOCK_READONLY));

	// create the new surface for writing the lightmap with direct lighting only:
	m_TextureRenderTarget=CreateOffscreenSurface();
	if (m_TextureRenderTarget==NULL)
		return false;

	V_RETURN(m_TextureRenderTarget->LockRect(&rectWrite, NULL, D3DLOCK_NO_DIRTY_UPDATE));

	Color32* pixelsRead=(Color32*)rectRead.pBits;
	Color32* pixelsWrite=(Color32*)rectWrite.pBits;

#ifdef SAVE_INDEX_MAP
	grcImage* indexMapImage=grcImage::Create(GetTexWidth(),GetTexHeight(),1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
	for (int y=0;y<GetTexHeight();y++)
	{
		for (int x=0;x<GetTexWidth();x++)
		{
			Color32 color=GetTexel(pixelsRead,x,y);
			if (color.GetColor()!=0x00000000)
			{
				//color.Set(color.GetColor()*(0x00ffffff/30));
				indexMapImage->SetPixel(x,y,color.GetColor());
			}
			else
			{
				indexMapImage->SetPixel(x,y,0xffffff00);
			}
		}
	}

	indexMapImage->SaveDDS("c:\\indexmap.dds");
#endif

	// lock buffers:
	u32* indices=NULL;
	GetDxMesh().LockIndexBuffer(0x0,(LPVOID*)&indices);

	uvaUvAtlasHelper::Vertex* vertices=NULL;
	GetDxMesh().LockVertexBuffer(0x0,(LPVOID*)&vertices);

	// only support 2^24 # of polys:
	Assert(GetDxMesh().GetNumFaces()<0x00ffffff);

	Displayf("filling lightmap...");
	int lastLen=0;
	
	float totalTime=0.0f;
	if (!sysBootManager::IsDebuggerPresent())
		Printf("Line #");

	bool* addedTexels=new bool[GetTexHeight()*GetTexWidth()];

//#define OCCLUSION_BOUNDS

#ifndef OCCLUSION_BOUNDS
	// create a render target to handle the texture:
	grcRenderTarget*	renderTarget		=grcTextureFactory::GetInstance().CreateRenderTarget("directLexelviewColor",
		grcrtPermanent,
		prtDirectLighting::LEXEL_VIEW_WIDTH,
		prtDirectLighting::LEXEL_VIEW_HEIGHT,
		32);
	grcRenderTarget*	renderTargetDepth	=grcTextureFactory::GetInstance().CreateRenderTarget("directLexelviewDepth",
		grcrtDepthBuffer,
		prtDirectLighting::LEXEL_VIEW_WIDTH,
		prtDirectLighting::LEXEL_VIEW_HEIGHT,
		32);
#endif

	// loop over height:
	Vector3 occlusionResult;
	for (int y=0;y<GetTexHeight();y++)
	{
		PrintProgress(lastLen,totalTime,y);

		sysTimer timer;

		// loop over width, read from index and compute direct lighting:
		for (int x=0;x<GetTexWidth();x++)
		{
			SetTexel(addedTexels,x,y,false);

			Vector3 texelPosition,texelNormal;
			if (!pltLightMapDirect::GetPosAndNormalFromIndexMap(vertices,indices,pixelsRead,x,y,texelPosition,texelNormal))
			{
				SetTexel(pixelsWrite,x,y,(Color32)ILLEGAL_TEXEL_COLOR);
			}
			else
			{
//#define MAKE_DIRECT_PASS_GO_FAST
#ifndef MAKE_DIRECT_PASS_GO_FAST
#ifdef OCCLUSION_BOUNDS
				// grab the new color:
				SetTexel(pixelsWrite,x,y,EvaluateColor(texelPosition,texelNormal));
#else
				SetTexel(pixelsWrite,x,y,RenderToTextureOcclusionCheck(*renderTarget,*renderTargetDepth,texelPosition,texelNormal));
#endif
#else
				SetTexel(pixelsWrite,x,y,Color32(0xff00ff00));
#endif
			}
		}

		totalTime+=timer.GetTime();
	}

#ifndef OCCLUSION_BOUNDS
	renderTarget->Release();
	renderTargetDepth->Release();
#endif

	// add smeared borders are the texture charts:
	AddSmearedBorder(addedTexels,pixelsRead,pixelsWrite,2.0f);

	delete [] addedTexels;

	GetDxMesh().UnlockIndexBuffer();
	GetDxMesh().UnlockVertexBuffer();

	m_TextureRenderTarget->UnlockRect();
	m_IndexMapSurface->UnlockRect();

	return true;
}

Color32 pltLightMapDirect::EvaluateColor(const Vector3& worldSpacePos, const Vector3 &normal)
{
	Vector3 result;
	result.Zero();
	for (int i=0;i<prtDirectLighting::LIGHT_COUNT;i++)
	{
		Vector3 d = m_Direct->m_Lights[i].m_Position - worldSpacePos;
		float intensity = m_Direct->m_Lights[i].m_Intensity / d.Mag2(); // I/r^2 attenuation
		intensity=Clamp(intensity,0.0f,1.0f);
		d.Normalize();
		float dot = d.Dot(normal);
		if (dot<=0.f) // texel backface
			continue;

		phSegment segment;
		segment.Set(worldSpacePos,m_Direct->m_Lights[i].m_Position);
		
		// scale it away a millimeter from the polygon so we don't hit the polygon:
		segment.A.AddScaled(normal,0.001f);

		// check if something is in the way:
		if (PHLEVEL->TestProbe(segment,NULL))
			continue;

		result.AddScaled(m_Direct->m_Lights[i].m_Color,intensity * dot);
	}
	return Color32(result);
}

void pltLightMapDirect::RenderToTexture()
{
	// lock buffers:
	u32* indices=NULL;
	GetDxMesh().LockIndexBuffer(0x0,(LPVOID*)&indices);

	uvaUvAtlasHelper::Vertex* vertices=NULL;
	GetDxMesh().LockVertexBuffer(0x0,(LPVOID*)&vertices);

	// only support 2^24 # of polys:
	Assert(GetDxMesh().GetNumFaces()<0x00ffffff);

	// go over all the faces and render each triangle direct lighting for each face:
	Color32 color;
	u32 currentColor=0xff000001;

	Displayf("rendering triangles to index map...");
	int dimensions[2] = {GetTexWidth(),GetTexHeight()};
	bool tooSmall[2] = {false,false};
	for (UINT i=0;i<GetDxMesh().GetNumFaces()*3;i+=3)
	{
		uvaUvAtlasHelper::Vertex* verts[3];
		verts[0]=&vertices[indices[i+0]];
		verts[1]=&vertices[indices[i+1]];
		verts[2]=&vertices[indices[i+2]];

		int minIndices[2];
		int maxIndices[2];
		
		for (int j=0;j<2;j++)
		{
			// min index:
			if (verts[0]->m_Texcoord2[j]<verts[1]->m_Texcoord2[j])
			{
				if (verts[0]->m_Texcoord2[j]<verts[2]->m_Texcoord2[j])
					minIndices[j]=0;
				else 
					minIndices[j]=2;
			}
			else if (verts[1]->m_Texcoord2[j]<verts[2]->m_Texcoord2[j]) 
				minIndices[j]=1;
			else 
				minIndices[j]=2;

			// min index:
			if (verts[0]->m_Texcoord2[j]>verts[1]->m_Texcoord2[j])
			{
				if (verts[0]->m_Texcoord2[j]>verts[2]->m_Texcoord2[j])
					maxIndices[j]=0;
				else 
					maxIndices[j]=2;
			}
			else if (verts[1]->m_Texcoord2[j]>verts[2]->m_Texcoord2[j]) 
				maxIndices[j]=1;
			else 
				maxIndices[j]=2;

			// make sure we're at least one pixel apart:
			float difference=verts[maxIndices[j]]->m_Texcoord2[j]*dimensions[j]-verts[minIndices[j]]->m_Texcoord2[j]*dimensions[j];
			if (difference<1.0f)
			{
				tooSmall[j]=true;
			}
		}

		// adjust output coordinates so we can see the texture:
		Vector2 triCoords[3];
		triCoords[0].x=verts[0]->m_Texcoord2.x*(GetTexWidth());
		triCoords[0].y=verts[0]->m_Texcoord2.y*(GetTexHeight());

		triCoords[1].x=verts[1]->m_Texcoord2.x*(GetTexWidth());
		triCoords[1].y=verts[1]->m_Texcoord2.y*(GetTexHeight());

		triCoords[2].x=verts[2]->m_Texcoord2.x*(GetTexWidth());
		triCoords[2].y=verts[2]->m_Texcoord2.y*(GetTexHeight());

		for (int j=0;j<3;j++)
		{
			for (int k=0;k<2;k++)
			{
				if (j==minIndices[k])
				{
					triCoords[j][k]-=0.5f;
				}
				else if (j==maxIndices[k])
				{
					triCoords[j][k]+=0.5f;
				}
			}
		}


		grcState::SetCullMode(grccmNone);
		color.Set(currentColor++);

		// render the triangle to the lightmap render target:
		grcBegin(drawTris,3);
		grcColor(color);
		grcVertex2f(triCoords[0].x,triCoords[0].y);
		grcVertex2f(triCoords[1].x,triCoords[1].y);
		grcVertex2f(triCoords[2].x,triCoords[2].y);
		grcEnd();

		// draw a vertical line instead of a triangle to make sure this triangle shows up...
		if (tooSmall[0])
		{
			grcBegin(drawLines,2);
			grcColor(color);
			grcVertex2f(verts[minIndices[1]]->m_Texcoord2.x*(GetTexWidth())+0.5f,verts[minIndices[1]]->m_Texcoord2.y*(GetTexHeight())+0.5f);
			grcVertex2f(verts[maxIndices[1]]->m_Texcoord2.x*(GetTexWidth())+0.5f,verts[maxIndices[1]]->m_Texcoord2.y*(GetTexHeight())+0.5f);
			grcEnd();
		}
		// ...draw a horizontal line instead of a triangle to make sure this triangle shows up...
		else if (tooSmall[1])
		{
			grcBegin(drawLines,2);
			grcColor(color);
			grcVertex2f(verts[minIndices[0]]->m_Texcoord2.x*(GetTexWidth())+0.5f,verts[minIndices[0]]->m_Texcoord2.y*(GetTexHeight())+0.5f);
			grcVertex2f(verts[maxIndices[0]]->m_Texcoord2.x*(GetTexWidth())+0.5f,verts[maxIndices[0]]->m_Texcoord2.y*(GetTexHeight())+0.5f);
			grcEnd();
		}
	}

	// unlock buffers:
	GetDxMesh().UnlockIndexBuffer();
	GetDxMesh().UnlockVertexBuffer();
}

Color32 pltLightMapDirect::RenderToTextureOcclusionCheck(grcRenderTarget& renderTarget,grcRenderTarget& depth,Vector3::Vector3Param worldSpacePos,Vector3::Vector3Param normal)
{
	Color32 result(0,0,0,255);
	Vector3 litPixel(0.0f,0.0f,0.0f);

	struct LightInfo
	{
		Vector3 d;
		bool	backfacing;
		float	dot;
	} lightInfos[prtDirectLighting::LIGHT_COUNT];

	// compute some information beforehand:
	bool allBackfacing=true;
	for (int i=0;i<prtDirectLighting::LIGHT_COUNT;i++)
	{
		// see if we're backfacing:
		lightInfos[i].d= m_Direct->m_Lights[i].m_Position - worldSpacePos;
		lightInfos[i].d.Normalize();
		lightInfos[i].dot = lightInfos[i].d.Dot(normal);
		lightInfos[i].backfacing=lightInfos[i].dot<=0.f;
		if (lightInfos[i].backfacing) // texel backface
			continue;
		else 
			allBackfacing=false;
	}

	// maybe backfacing from the lights, so let's just exit:
	if (allBackfacing)
		return result;

	// lock our render target:
	grcTextureFactory::GetInstance().LockRenderTarget(0, &renderTarget, &depth);

	Color32 clearColor(0xffffffff);
	GRCDEVICE.Clear(true,clearColor,true,1.0f,0);

	grcState::SetCullMode(grccmNone);
	

	// setup camera:
	Matrix34 matrix;
	matrix.Identity();
	matrix.LookDown(normal,YAXIS);
	matrix.d=worldSpacePos;
	m_Viewport.SetCameraMtx(matrix);

	grcViewport::SetCurrent(&m_Viewport);

	// render everything:
	atArray<prtGeometry*>& geometries=m_Direct->GetGeometries();
	for (int i=0;i<geometries.GetCount();i++)
	{
		geometries[i]->Render();
	}

	for (int i=0;i<prtDirectLighting::LIGHT_COUNT;i++)
	{
		// Add an end marker to the command buffer queue.
		m_Direct->GetOcclusionQuery().Issue(D3DISSUE_BEGIN);
		grcBegin(drawPoints,1);
		grcColor3f(Vector3(1.0f,1.0f,1.0f));
		grcVertex3f(m_Direct->m_Lights[i].m_Position);
		grcEnd();
		m_Direct->GetOcclusionQuery().Issue(D3DISSUE_END);

		// Force the driver to execute the commands from the command buffer.
		// Empty the command buffer and wait until the GPU is idle.
		DWORD numberOfPixelsDrawn;
		while(S_FALSE == m_Direct->GetOcclusionQuery().GetData( &numberOfPixelsDrawn, sizeof(DWORD), D3DGETDATA_FLUSH ))
			; // wait until we get the result

		// light was occluded, continue:
		if (numberOfPixelsDrawn<=0)
			continue;

		// compute intensity:
		float intensity = m_Direct->m_Lights[i].m_Intensity / lightInfos[i].d.Mag2(); // I/r^2 attenuation
		intensity=Clamp(intensity,0.0f,1.0f);

		litPixel.AddScaled(m_Direct->m_Lights[i].m_Color,intensity * lightInfos[i].dot);
	}

	grcViewport::SetCurrent(NULL);
	grcTextureFactory::GetInstance().UnlockRenderTarget(0);


	result.Setf(litPixel.x,litPixel.y,litPixel.z,1.0f);
	return result;
}

Color32* pltLightMapDirect::LockIndexMap()
{
	Assert(m_IndexMapSurface);
	D3DLOCKED_RECT rect;
	HRESULT hr=m_IndexMapSurface->LockRect(&rect, NULL, D3DLOCK_READONLY);
	if (hr!=S_OK)
		return NULL;

	return (Color32*)rect.pBits;
}

void pltLightMapDirect::UnlockIndexMap()
{
	Assert(m_IndexMapSurface);
	m_IndexMapSurface->UnlockRect();
}

void prtDirectLighting::Init()
{
	// initialize the physics level:
	const int MAX_OCTREE_NODES = 1000;
	const int MAX_ACTIVE_OBJECTS = 500;
	int MAX_OBJECTS = 500;
	const Vec3V WORLD_MIN(-999.0f,-999.0f,-999.0f);
	const Vec3V WORLD_MAX( 999.0f, 999.0f, 999.0f);

	phLevelNew::SetActiveInstance(new phLevelNew);
	PHLEVEL->SetExtents(WORLD_MIN, WORLD_MAX);
	PHLEVEL->SetMaxObjects(MAX_OBJECTS);
	PHLEVEL->SetMaxActive(MAX_ACTIVE_OBJECTS);
	PHLEVEL->SetNumOctreeNodes(MAX_OCTREE_NODES);
	PHLEVEL->Init();

	// make sure occlusion querys are supported:
	HRESULT hr=GRCDEVICE.GetCurrent()->CreateQuery(D3DQUERYTYPE_EVENT, NULL);
	if (hr!=S_OK)
	{
		Quitf("Hardware occlusion querying not supported!");
	}

	GRCDEVICE.GetCurrent()->CreateQuery(D3DQUERYTYPE_OCCLUSION, &m_OcclusionQuery);
}

void prtDirectLighting::AddGeometry(prtGeometry& geometry)
{
	m_Geometries.Grow()=&geometry;
	PHLEVEL->AddFixedObject(&geometry.GetPhysInst(),false);
}

void prtDirectLighting::SaveGeometries()
{
	// output our geometries:
	for (int i=0;i<m_Geometries.GetCount();i++)
	{
		m_Geometries[i]->OutputMeshes("shit");
	}
}

void prtDirectLighting::ComputeLightMapsAlt()
{
	for (int i=0;i<m_Geometries.GetCount();i++)
	{
		m_Geometries[i]->ComputeDirectLightMapsAlt(m_Geometries);
	}
}

void prtDirectLighting::ComputeLightMaps()
{
	for (int i=0;i<m_Geometries.GetCount();i++)
	{
		m_Geometries[i]->ComputeDirectLightMaps();
	}
}

void prtDirectLighting::SaveLightMaps(const char* fileName,bool reload)
{
	// save out the lightmaps;
	for (int i=0;i<m_Geometries.GetCount();i++)
	{
		m_Geometries[i]->SaveDirectLightMaps(fileName,reload);
	}
}


} // namespace rage
