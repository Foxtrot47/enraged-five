//
// prttools/lightprobe.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "lightprobe.h"

#include "convert.h"

#include "file/asset.h"

namespace rage {

//-----------------------------------------------------------------------------
struct SHCubeProj 
{
    float *Red,*Green,*Blue;
    int OrderUse; // order to use
    float ConvCoeffs[6]; // convolution coefficients

    void InitDiffCubeMap(float *r, float *g, float *b)
    {
        Red = r;
        Green = g;
        Blue = b;

        OrderUse = 3; // go to 5 is a bit more accurate...
        ConvCoeffs[0] = 1.0f;
        ConvCoeffs[1] = 2.0f/3.0f;
        ConvCoeffs[2] = 1.0f/4.0f;
        ConvCoeffs[3] = ConvCoeffs[5] = 0.0f;
        ConvCoeffs[4] = -6.0f/144.0f; // 
    }

    void Init(float *r, float *g, float *b)
    {
        Red = r;
        Green = g;
        Blue = b;

        OrderUse = 6;
        for(int i=0;i<6;i++) ConvCoeffs[i] = 1.0f;
    }
};

//-----------------------------------------------------------------------------
CLightProbe::CLightProbe()
{
    m_Created          = FALSE;
    m_EnvironmentMap   = NULL;
    m_SHEnvironmentMap = NULL;
}

//-----------------------------------------------------------------------------
CLightProbe::~CLightProbe()
{
	SAFE_RELEASE( m_EnvironmentMap );
	SAFE_RELEASE( m_SHEnvironmentMap );
	m_Created = false;
}



//--------------------------------------------------------------------------------------
void WINAPI SHCubeFill(D3DXVECTOR4* out, 
                       CONST D3DXVECTOR3* texCoord, 
                       LPVOID data)
{
    SHCubeProj* cp = (SHCubeProj*) data;
    D3DXVECTOR3 vDir;

    D3DXVec3Normalize(&vDir,texCoord);

    float vals[36];
    D3DXSHEvalDirection( vals, cp->OrderUse, &vDir );

    (*out) = D3DXVECTOR4(0,0,0,0); // just clear it out...

    int l, m, uIndex = 0;
    for( l=0; l<cp->OrderUse; l++ ) 
    {
        const float convUse = cp->ConvCoeffs[l];
        for( m=0; m<2*l+1; m++ ) 
        {
            out->x += convUse*vals[uIndex]*cp->Red[uIndex];
            out->y += convUse*vals[uIndex]*cp->Green[uIndex];
            out->z += convUse*vals[uIndex]*cp->Blue[uIndex];
            out->w = 1;

            uIndex++;
        }
    }
}


//-----------------------------------------------------------------------------
HRESULT CLightProbe::Load(const char* strCubeMapFile, bool createSHEnvironmentMapTexture )
{
	char fullPath[512];
	ASSET.FullPath(fullPath,sizeof(fullPath),strCubeMapFile,"dds");

    HRESULT hr;

    // Note that you should optimally read these HDR light probes on device that supports
    // floating point.  If this current device doesn't support floating point then
    // D3DX will automatically convert them to interger format which won't be optimal.
    // If you wanted to perform this step in a machine without a good video card, then 
    // use a D3DDEVTYPE_NULLREF device instead.
  	V_RETURN( D3DXCreateCubeTextureFromFileEx( GRCDEVICE.GetCurrent(), fullPath, 512, 1, 0, D3DFMT_A16B16G16R16F, 
                                               D3DPOOL_MANAGED, D3DX_FILTER_LINEAR, D3DX_FILTER_LINEAR, 0, NULL, NULL, &m_EnvironmentMap ) );

    // Some devices don't support D3DFMT_A16B16G16R16F textures and thus
    // D3DX will return the texture in a HW compatible format with the possibility of losing its 
    // HDR lighting information.  This will change the SH values returned from D3DXSHProjectCubeMap()
    // as the cube map will no longer be HDR.  So if this happens, create a load the cube map on 
    // scratch memory and project using that cube map.  But keep the other one around to render the 
    // background texture with.
    bool usedScratchMem = false;
    D3DSURFACE_DESC desc;
    ZeroMemory( &desc, sizeof(D3DSURFACE_DESC) );
    m_EnvironmentMap->GetLevelDesc( 0, &desc );
    if( desc.Format != D3DFMT_A16B16G16R16F )
    {
        LPDIRECT3DCUBETEXTURE9 scratchEnvironmentMap = NULL;
		hr = D3DXCreateCubeTextureFromFileEx( GRCDEVICE.GetCurrent(), fullPath, 512, 1, 0, D3DFMT_A16B16G16R16F, 
                            D3DPOOL_SCRATCH, D3DX_FILTER_LINEAR, D3DX_FILTER_LINEAR, 0, NULL, NULL, &scratchEnvironmentMap );
        if( SUCCEEDED(hr) )
        {
            // prefilter the lighting environment by projecting onto the order 6 SH basis.  
            V( D3DXSHProjectCubeMap( 6, scratchEnvironmentMap, m_SHData[0], m_SHData[1], m_SHData[2] ) );
            usedScratchMem = true;
            SAFE_RELEASE( scratchEnvironmentMap );
        }
    }

    if( !usedScratchMem )
    {
        // prefilter the lighting environment by projecting onto the order 6 SH basis.  
        V( D3DXSHProjectCubeMap( 6, m_EnvironmentMap, m_SHData[0], m_SHData[1], m_SHData[2] ) );
    }

    if( createSHEnvironmentMapTexture )
    {
        // reconstruct the prefiltered lighting environment into a cube map
        V( D3DXCreateCubeTexture( GRCDEVICE.GetCurrent(), 256, 1, 0, D3DFMT_A16B16G16R16F, D3DPOOL_MANAGED, &m_SHEnvironmentMap ) );
        SHCubeProj projData;
        projData.Init(m_SHData[0],m_SHData[1],m_SHData[2]);
        V( D3DXFillCubeTexture( m_SHEnvironmentMap, (LPD3DXFILL3D)SHCubeFill, &projData ) );

        // This code will save a cubemap texture that represents the SH projection of the light probe if you want it
        // V( D3DXSaveTextureToFile( "shprojection.dds", D3DXIFF_DDS, m_SHEnvironmentMap, NULL ) ); 
    }

    m_Created = true;

    return S_OK;
}


} // namespace rage



