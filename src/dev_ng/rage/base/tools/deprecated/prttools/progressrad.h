// 
// prttools/progressrad.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PRTTOOLS_PROGRESSRAD_H
#define PRTTOOLS_PROGRESSRAD_H

#include "direct.h"

#include "atl/array.h"
#include "grcore/viewport.h"

namespace rage {

class grmShader;
class grmShaderGroup;

class pltLightMapProgressiveRadiosity : public pltLightMapDirect
{
public:
	pltLightMapProgressiveRadiosity(pltLightMapDirect& lightMapDirect);

	static void InitClass();

	void IterateLightmap(atArray<prtGeometry*>& geometries,float modulationMask[]);

	
	virtual void Init(float =0.4f,float =4.0f)
	{
		// we intentionally don't call the base class Init()!
	}

	virtual ID3DXMesh& GetDxMesh()
	{
		return m_LightMapDirect->GetDxMesh();
	}

	virtual void SetDxMesh(ID3DXMesh&)
	{
		Quitf("Progressive Radiosity Maps don't need to set the mesh");
	}

protected:
	void RenderToTexture();

private:
	Vector3					m_CurrentTexelPosition;
	Vector3					m_CurrentTexelNormal;
	grcViewport				m_Viewport;
	atArray<prtGeometry*>	m_Geometries;
	pltLightMapDirect*		m_LightMapDirect;

	// statics:
	static grmShaderGroup*	sm_ShaderGroup;
	static grmShader*		sm_Shader;
};

class prtProgressiveRadiosity
{
public:
	static const float NEAR_CLIP_PLANE;
	static const float FAR_CLIP_PLANE;
	static const float HORIZONTAL_FIELD_OF_VIEW;

	enum {LEXEL_VIEW_WIDTH=128,LEXEL_VIEW_HEIGHT=128,LEXEL_VIEW_NUM_PIXELS=LEXEL_VIEW_WIDTH*LEXEL_VIEW_HEIGHT};

	prtProgressiveRadiosity(prtDirectLighting& direct);
	void Iterate(int numIterations);
	void ComputeScreenModulationMask(float nearClip,float fov,int lightmapWidth,int lightmapHeight);

private:
	NON_COPYABLE(prtProgressiveRadiosity);

	prtDirectLighting&	m_DirectLighting;
	float*				m_ModulationMask;
};

}; // namespace rage

#endif //PRTTOOLS_PROGRESSRAD_H
