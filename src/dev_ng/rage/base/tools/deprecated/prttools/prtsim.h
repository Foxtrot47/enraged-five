//
// prttools/prtsim.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PRTTOOLS_PRTSIM_H
#define PRTTOOLS_PRTSIM_H

#include "config.h"
#include "convert.h"
#include "lightprobe.h"

#include "atl/array.h"
#include "parser/manager.h"

namespace rage {

struct prtRemapInfo
{
public:
	prtRemapInfo();
	virtual ~prtRemapInfo() {}

	bool Load(const char* fileName);
	bool Save(const char* fileName);
	void CopyFromArrays(UINT* faceRemaps,int faceRemapCount,UINT* vertRemaps,int vertRemapsCount,FLOAT* vertWeights,int vertWeightsCount);


	atArray<u32,unsigned int,UINT_MAX>		m_FaceRemaps;
	atArray<u32,unsigned int,UINT_MAX>		m_VertRemaps;
	atArray<float,unsigned int,UINT_MAX>	m_VertWeights;

	PAR_PARSABLE;
};

class prtSimulator 
{
public:
	

	struct ConcatMesh
	{
		ConcatMesh()
		{
			m_OriginalDxMesh=NULL;
			m_DxMesh=NULL;
			m_RemapInfo=NULL;
		}

		void CopyRemap(prtRemapInfo& info);

		ID3DXMesh*				m_OriginalDxMesh;
		ID3DXMesh*				m_DxMesh;
		atArray<atString>		m_DxMeshMaterialNames;
		atArray<D3DXMATERIAL>	m_Materials;
		atArray<D3DXSHMATERIAL> m_SHMaterials;
		
		UINT*					m_FaceRemaps;
		UINT*					m_VertRemaps;
		FLOAT*					m_VertWeights;

		prtRemapInfo*			m_RemapInfo;
	};

	prtSimulator()
	{
		m_LastPercentAnnounceTime	=0.0f;
		m_RageMesh					=NULL;
		m_PrtCompBuffer				=NULL;
		m_LightDirection.x			=0.0f;
		m_LightDirection.y			=0.0f;
		m_LightDirection.z			=1.0f;
		m_NumActiveLights			=2;
		m_PrtClusterBases			=NULL;
		m_PrtConstants				=NULL;
		m_ClusterIDs				=NULL;
		m_PrtTextures				=NULL;
		m_PerPixel					=false;

		m_LightProbe.Load("rnl_cross",false);
	}

	bool Load(const char* inputPath);
	bool Save(const char* inputPath);

	bool Convert(const prtSimulatorOptions& option);
	void LoadMesh(const char* inputPath);
	bool Run(const prtSimulatorOptions& options, ConcatMesh* blockerMesh,bool perPixel,int width,int height);
	void ExtractCompressedDataForPRTShader(ID3DXMesh& dxMesh,int width,int height); 
	Vector4 GetPRTDiffuse( int clusterOffset, Vector4* pcaWeights/*[NUM_PCA/4]*/ ) const;
	ConcatMesh& GetConcatMesh() {return m_ConcatDxMesh;}
	void OutputMesh(atString fileName,bool adaptedMesh);
	void UpdateLightingEnvironment(const Vector3& lightDirObjectSpace);
	
	const UINT* GetClusterIDs() const
	{
		return m_ClusterIDs;
	}

	const LPDIRECT3DTEXTURE9* GetPrtTextures() const 
	{
		return m_PrtTextures;
	}

	u32 GetNumPcas() const
	{
		return m_NumPca;
	}

	bool GetPerPixel() const
	{
		return m_PerPixel;
	}

	enum {MAX_LIGHTS=10};

	D3DXVECTOR3			m_LightDirection;
	int					m_NumActiveLights;

	prtConvert			m_Convert;
	float				m_LastPercentAnnounceTime;
	mshMesh*			m_RageMesh;
	ConcatMesh			m_ConcatDxMesh;
	ID3DXPRTCompBuffer* m_PrtCompBuffer;
	// The basis buffer is a large array of floats where 
	// Call ID3DXPRTCompBuffer::ExtractBasis() to extract the basis 
	// for every cluster.  The basis for a cluster is an array of
	// (NumPCAVectors+1)*(NumChannels*Order^2) floats. 
	// The "1+" is for the cluster mean.
	float*				m_PrtClusterBases;
	// m_PrtConstants stores the incident radiance dotted with the transfer function.
	// Each cluster has an array of floats which is the size of 
	// 4+MAX_NUM_CHANNELS*NUM_PCA_VECTORS. This number comes from: there can 
	// be up to 3 channels (R,G,B), and each channel can 
	// have up to NUM_PCA_VECTORS of PCA vectors.  Each cluster also has 
	// a mean PCA vector which is described with 4 floats (and hence the +4).
	float*				m_PrtConstants;
	u32					m_NumPca;
	D3DXMATERIAL*       m_Materials;
	DWORD				m_NumMaterials;
	ID3DXBuffer*        m_MaterialBuffer;
	CLightProbe			m_LightProbe;
	UINT*				m_ClusterIDs;
	LPDIRECT3DTEXTURE9*	m_PrtTextures;
	bool				m_PerPixel;

private:
	u32 GetOrderFromNumCoeffs();
	void ComputeShaderConstants( float* shCoeffsRed, float* shCoeffsGreen, float* shCoeffsBlue, DWORD numCoeffsPerChannel );
	struct ID3DXPRTBuffer* prtSimulator::CreateBuffer(int numSamples,const prtSimulatorOptions& options,bool perPixel,int width,int height);

};

} // namespace rage

#endif // PRTTOOLS_PRTSIM_H
