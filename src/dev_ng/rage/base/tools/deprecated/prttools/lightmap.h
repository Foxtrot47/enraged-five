// 
// prttools/lightmap.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PRTTOOLS_LIGHTMAP_H
#define PRTTOOLS_LIGHTMAP_H

#include "atl/array.h"
#include "grcore/image.h"
#include "uvatlas/uvatlas.h"

namespace rage {

class grcImage;
class prtConvert;

struct prtRectangle
{
	friend struct prtNode;
	
public:
	prtRectangle() 
	{
		m_Width=m_Top=m_Left=m_Height=0;
	}

	void Set(int left, int top, int width, int height) 
	{
		m_Left=left; m_Top=top; m_Width=width; m_Height=height; 
	} 

	// returns true if 'r' equals size of this-rect
	bool IsSizeEqual(const prtRectangle& r) const
	{ 
		return r.m_Width==m_Width && r.m_Height==m_Height; 
	} 

	bool IsSizeEqualRotated(const prtRectangle& r) const
	{ 
		return r.m_Width==m_Height && r.m_Height==m_Width; 
	} 

	// returns true if 'r' will fit in this-rect
	bool IsSizeSmaller(const prtRectangle& r) const
	{ 
		return m_Width <r.m_Width || m_Height <r.m_Height; 
	} 

	// returns true if 'r' will fit in this-rect
	bool IsSizeSmallerRotated(const prtRectangle& r) const
	{ 
		return m_Height <r.m_Width || m_Width <r.m_Height; 
	} 

	void Rotate()
	{
		int left=m_Left;
		m_Left=m_Top;
		m_Top=left;

		int width=m_Width;
		m_Width=m_Height;
		m_Height=width;
	}

	int GetArea() const
	{
		return m_Height*m_Width;
	}

	int GetLeft() const
	{
		return m_Left;
	}

	int GetTop() const
	{
		return m_Top;
	}

	int GetBottom() const
	{
		return m_Top-m_Height;
	}

	int GetHeight() const
	{
		return m_Height;
	}

	int GetWidth() const
	{
		return m_Width;
	}

protected:
	int	m_Left;
	int m_Top;
	int m_Height;
	int	m_Width;
};

struct prtNode
{
	prtNode()
	{
		m_Children[0]=m_Children[1]=NULL;
		m_LightMap=NULL;
	}


	void Insert(class LightMapInfo& lightMap);
	void CreateImage();
	void FillImage(grcImage& image);

	enum {NUM_CHILDREN=2};
	prtNode*			m_Children[NUM_CHILDREN];
	prtRectangle		m_Rect;
	class LightMapInfo*	m_LightMap;

private:
	prtNode* InsertInternal(class LightMapInfo& lightMap);
};

class prtLightMapTriangle
{
	Vector3	m_Verts[3];
	Vector4 m_Colors[3];
};

class LightMapInfo
{
public:
	LightMapInfo()
	{
		for (int i=0;i<3;i++)
		{
			m_UVs[i].Zero();
		}
		m_Index=0;
		m_Rotated=false;
		m_Node=NULL;
	}

	template<class _Type> void RotateVert(_Type x,_Type y,_Type& newX,_Type& newY,_Type oldWidth)
	{
		newX=y;
		newY=-x;

		newY=-x+oldWidth;
	}

	void Rotate()
	{

		RotateVert(m_UVs[0].x,m_UVs[0].y,m_UVs[0].x,m_UVs[0].y,(float)m_Rect.GetWidth());
		RotateVert(m_UVs[1].x,m_UVs[1].y,m_UVs[1].x,m_UVs[1].y,(float)m_Rect.GetWidth());
		RotateVert(m_UVs[2].x,m_UVs[2].y,m_UVs[2].x,m_UVs[2].y,(float)m_Rect.GetWidth());

		m_Rect.Rotate();

		m_Rotated=!m_Rotated;
	}

	Vector2 GetPackedUV(int index,int width,int height) const
	{
		Vector2 newUV;
		if (m_Node)
		{
			newUV.x=(m_Node->m_Rect.GetLeft()+m_UVs[index].x+1)/width;
			newUV.y=(m_Node->m_Rect.GetTop()+m_UVs[index].y+1)/height;

			newUV.y=1.0f-newUV.y;
		}
		else
			newUV.Zero();

		return newUV;
	}

	bool							m_Rotated;
	prtRectangle					m_Rect;
	Vector2							m_UVs[3];
	int								m_Index;
	struct prtNode*					m_Node;

	static int QSortCompareArea(const LightMapInfo* a,const LightMapInfo* b)
	{
		if (a->m_Rect.GetArea()>b->m_Rect.GetArea())
			return -1;
		else if (a->m_Rect.GetArea()<b->m_Rect.GetArea())
			return 1;
		else 
			return 0;
	}

	static int QSortCompareIndex(const LightMapInfo* a,const LightMapInfo* b)
	{
		if (a->m_Index>b->m_Index)
			return 1;
		else if (a->m_Index<b->m_Index)
			return -1;
		else 
			return 0;
	}
};


class FaceInfo
{
public:
	UINT	m_FaceIndex;
	UINT	m_ParentFaceIndex;

	static int QSortCompare(const FaceInfo* a,const FaceInfo* b)
	{
		if (a->m_ParentFaceIndex>b->m_ParentFaceIndex)
			return 1;
		else if (a->m_ParentFaceIndex<b->m_ParentFaceIndex)
			return -1;
		else 
			return 0;
	}
};

class pltLightMap : public uvaUvAtlasHelper
{
public:
	pltLightMap(int width,int height);
	virtual ~pltLightMap() {}

	virtual void Init(float stretch=0.4f,float gutter=4.0f);

	void Insert(class LightMapInfo& lightMap);
	void ComputeLightMapsUVs();
	atArray<LightMapInfo,unsigned int,UINT_MAX>& GetLightMaps()
	{
		return m_LightMaps;
	}
	int GetBorderThickness()
	{
		return 1;
	}


private:
	struct AdjustFaceInfo
	{
		Vector2 m_Adjust;
		int		m_VertIndices[2];
	};

	// just declare, don't define these:
	pltLightMap(const pltLightMap& lightMap);
	pltLightMap& operator=(const pltLightMap& lightMap);

	Vector2 ComputeImageUV(uvaUvAtlasHelper::Vertex& vertex,Vector2* uvs,float* weights,Vector4& outColor);
	void CalcLightMapCoords(const uvaUvAtlasHelper::Vertex& vert1,
							const uvaUvAtlasHelper::Vertex& vert2,
							const uvaUvAtlasHelper::Vertex& vert3,
							int& outTexcoordIndexX,
							int& outTexcoordIndexY);
	Vector2 ComputeMapSizeForPolygon(const uvaUvAtlasHelper::Vertex& vert1,
									 const uvaUvAtlasHelper::Vertex& vert2,
									 const uvaUvAtlasHelper::Vertex& vert3,
									 int texcoordIndexX,
									 int texcoordIndexY,
									 float density,
									 Vector2* outUVs,
									 Vector2& outAdjustUV);
	void ComputeTexUVsForSubTri(const Vector3& vert1,
								const Vector3& vert2,
								const Vector3& vert3,
								float density,
								Vector2* outUVs,
								const AdjustFaceInfo& info);

protected:
	virtual ID3DXMesh& GetDxMesh()=0;
	virtual void SetDxMesh(ID3DXMesh& mesh)=0;
	virtual prtConvert& GetConvert()=0;

	Vector2 LerpVert2D(const Vector2& vert1,const Vector2& vert2,const Vector2& vert3,float weights[]);

	prtNode*									m_RootNode;
	atArray<LightMapInfo,unsigned int,UINT_MAX>	m_LightMaps;
};

} // namespace rage


#endif // PRTTOOLS_LIGHTMAP_H
