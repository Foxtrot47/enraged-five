set ARCHIVE=prttools
set FILES=ambientocclusion config prtsim convert lightmap lightmapprt lightprobe direct progressrad geom
set TESTERS=computeprt
set XPROJ=%RAGE_DIR%\base\src
set LIBS=parser parsercore uvatlas %RAGE_PH_LIBS% %RAGE_GFX_LIBS% %RAGE_CORE_LIBS%
set PARSE=prtsim
