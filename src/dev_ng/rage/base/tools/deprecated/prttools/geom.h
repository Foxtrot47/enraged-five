// 
// prttools/geom.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PRTTOOLS_GEOM_H
#define PRTTOOLS_GEOM_H

#include "lightmap.h"

#include "atl/ptr.h"
#include "atl/referencecounter.h"
#include "atl/string.h"
#include "grmodel/shadergroup.h"
#include "physics/archetype.h"
#include "physics/inst.h"

struct ID3DXMesh;

namespace rage {

class crSkeleton;
class mshMesh;
class phBoundGeometry;
class pltLightMapAmbientOcclusion;
class pltLightMapDirect;
class pltLightMapProgressiveRadiosity;
class prtConvert;
class prtDirectLighting;
class rmcDrawable;

class prtGeometry : public atReferenceCounter
{
public:
	static void InitShaderSystems(const char* shaderRoot);

	prtGeometry();
	bool Init(prtConvert& convert,const char* fileName,prtDirectLighting& direct,int width,int height);

	ID3DXMesh& GetDxMesh(int meshIndex)
	{
		Assert(m_DxMeshes[meshIndex]);
		return *m_DxMeshes[meshIndex];
	}

	void SetDxMesh(int meshIndex,ID3DXMesh& mesh)
	{
		m_DxMeshes[meshIndex]=&mesh;
	}

	phInst& GetPhysInst() 
	{
		return m_PhysInst;
	}

	void Render(bool alwaysRender=false);
	void OutputMeshes(const char* /*fileNamePostfix*/);

	// lightmap computation and serialization:
	void ComputeDirectLightMaps();
	void ComputeDirectLightMapsAlt(atArray<prtGeometry*>& geometries);
	void SaveDirectLightMaps(const char* fileName,bool reload);
	
	// progressive radiosity:
	void ComputeProgressiveRadiosityLightMaps(atArray<prtGeometry*>& geometries,float modulationMask[]);
	void SaveProgressiveRadiosityLightMaps(const char* fileName);
	void LoadProgressiveRadiosityLightMaps(const char* fileName);

	// ambient occlusion maps:
	void ComputeAmbientOcclusionMaps(atArray<prtGeometry*>& geometries);
	void SaveAmbientOcclusionMaps(const char* fileName);

	void ReloadLightMap(const char* fileName);

private:
	void CreateBound();
	bool LoadDrawable();

	bool MeshHandler(mshMesh& mesh,const char* name);

	phBoundGeometry*	m_BoundGeom;
	phArchetypePhys		m_Archetype;
	phInst				m_PhysInst;

	rmcDrawable*								m_Drawable;
	crSkeleton*									m_Skeleton;
	atArray<mshMesh*>							m_Meshes;
	atArray<atString>							m_MeshNames;
	atArray<ID3DXMesh*>							m_DxMeshes;
	atArray<pltLightMapDirect*>					m_LightMapsDirect;
	atArray<pltLightMapProgressiveRadiosity*>	m_LightMapsProgRadiosityLast;
	atArray<pltLightMapAmbientOcclusion*>		m_LightMapsAmbientOcclusion;
	atArray<grcImage*>							m_ImagesProgRadiosityCurrent;
	atArray< atArray<atString> >				m_MaterialNames;
	atString									m_LightMapDirectory;
	atString									m_LightMapFullPath;
	grmShaderGroupVar							m_LightMapTextureIndex;
	int											m_LightMapWidth;
	int											m_LightMapHeight;
	atString									m_DrawablePath;
	prtDirectLighting*							m_DirectLighting;

	atPtr<prtConvert>	m_Converter;
};

} // namespace rage

#endif // PRTTOOLS_GEOM_H
