// 
// prttools/convert.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PRTTOOLS_CONVERT_H
#define PRTTOOLS_CONVERT_H

#include "atl/referencecounter.h"
#include "atl/string.h"
#include "grcore/device.h"
#include "mesh/mesh.h"
#include "system/xtl.h"

#include <dxerr9.h>
#include <d3d9types.h>
#include <d3dx9mesh.h>

#pragma comment(lib,"dxerr9.lib")

namespace rage {

#ifndef V
#define V(x)				\
	{						\
		HRESULT hr = x;				\
		if( FAILED(hr) )	\
		{	\
		Errorf("%s(%d): function call '%s' failed with error '%s', desc '%s'", __FILE__, __LINE__, #x, DXGetErrorString9A(hr), DXGetErrorDescription9A(hr)); \
		} \
	} 
#endif

#ifndef V_RETURN
#define V_RETURN(x)				\
	{							\
		HRESULT hr = x;					\
		if( FAILED(hr) )		\
		{						\
			Errorf("%s(%d): function call '%s' failed with error '%s', desc '%s'", __FILE__, (DWORD)__LINE__, #x, DXGetErrorString9A(hr), DXGetErrorDescription9A(hr)); \
			return false; \
		} \
	}
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(p)       { if(p) { delete (p);     (p)=NULL; } }
#endif    
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }
#endif    
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if(p) { (p)->Release(); (p)=NULL; } }
#endif

class prtConvert : public atReferenceCounter
{
public:
	prtConvert()
	{
		//CreateNULLRefDevice();
		m_Device=GRCDEVICE.GetCurrent();
	}

	~prtConvert()
	{
		m_Device=NULL;
	}

	mshMesh*	LoadRageMesh(const char* inputPath);
	ID3DXMesh*	CreateD3DXMesh(mshMesh& mesh,atArray<atString>& outMaterialNames);
	static void		OutputMesh(const char* fileName,ID3DXMesh& dxMesh,atArray<atString>& materialNames);
	static void		OutputMesh(const char* fileName,ID3DXMesh& dxMeshes,float* prtConstantsFloat,u32 numPcas,atArray<atString>& materialNames,bool outputCpvOnly);
	bool		AdjustMeshDecl(ID3DXMesh*& inOutDxMesh );
	bool		DoesMeshHaveUsage(ID3DXMesh& mesh, BYTE Usage ) const;
	static ID3DXMesh* SortByAttribute(ID3DXMesh& dxMesh);

	grcDeviceHandle* GetDevice()
	{
		return m_Device;
	}

	static Vector4 GetPRTDiffuse(int clusterOffset, const Vector4* pcaWeights, const float* prtConstantsFloat,u32 numPcas);

private:

	void CreateNULLRefDevice();

	grcDeviceHandle*	m_Device;
};

} // namespace rage

#endif // PRTTOOLS_CONVERT_H
