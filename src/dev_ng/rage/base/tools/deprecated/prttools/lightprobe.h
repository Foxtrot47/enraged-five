//
// prttools/lightprobe.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PRTTOOLS_LIGHTPROBE_H
#define PRTTOOLS_LIGHTPROBE_H

#include "system/xtl.h"
#include <d3dx9math.h>

struct IDirect3DCubeTexture9;

namespace rage {

class CLightProbe
{
public:
    CLightProbe();
	~CLightProbe();

    HRESULT Load( const char* strCubeMapFile, bool bCreateSHEnvironmentMapTexture );
   
    IDirect3DCubeTexture9* GetSHEnvironmentMap() { return m_SHEnvironmentMap; }
    IDirect3DCubeTexture9* GetEnvironmentMap()   { return m_EnvironmentMap; }
    float*  GetSHData( int nChannel ) { if( nChannel < 0 || nChannel > 3 ) return NULL; else return m_SHData[nChannel]; };
    bool    IsCreated() const { return m_Created; };

protected:
    float                           m_SHData[3][D3DXSH_MAXORDER*D3DXSH_MAXORDER];  
    bool                            m_Created;
    IDirect3DCubeTexture9*          m_EnvironmentMap;
    IDirect3DCubeTexture9*          m_SHEnvironmentMap;
};

};
#endif //PRTTOOLS_LIGHTPROBE_H
