<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef type="rage::prtRemapInfo">
	<array hideWidgets="true" indexBits="32" name="m_FaceRemaps" type="atArray">
		<u32/>
	</array>
	<array hideWidgets="true" indexBits="32" name="m_VertRemaps" type="atArray">
		<u32/>
	</array>
	<array hideWidgets="true" indexBits="32" name="m_VertWeights" type="atArray">
		<float/>
	</array>
</structdef>

</ParserSchema>