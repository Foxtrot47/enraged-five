// 
// prttools/ambientocclusion.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "ambientocclusion.h"

#include "direct.h"

#include "grcore/state.h"
#include "grcore/texture.h"
#include "grmodel/model.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "system/timer.h"

namespace rage {

const float		pltLightMapAmbientOcclusion::NEAR_CLIP_PLANE			=0.01f;
const float		pltLightMapAmbientOcclusion::FAR_CLIP_PLANE				=100.0f;
const float		pltLightMapAmbientOcclusion::HORIZONTAL_FIELD_OF_VIEW	=60.0f;

pltLightMapAmbientOcclusion::pltLightMapAmbientOcclusion(pltLightMapDirect& lightMapDirect,int numViewpoints) 
	: pltLightMapDirect(lightMapDirect.GetDirect(),
						lightMapDirect.GetConvert(),
						lightMapDirect.GetGeometry(),
						lightMapDirect.GetMeshIndex(),
						lightMapDirect.GetTexWidth(),
						lightMapDirect.GetTexHeight()),
	m_LightMapDirect(&lightMapDirect),
	m_NumViewpoints(numViewpoints)
{
	Assert(m_NumViewpoints>=1);
	m_NewDxMesh=&lightMapDirect.GetNewDxMesh();
	m_GutterSize=lightMapDirect.GetGutterSize();
}

#ifdef CRAP
// see http://www.gamasutra.com/features/20040319/hill_02.shtml to see what this is doing
void pltLightMapAmbientOcclusion::RenderSamples(int numSamples)
{
	int numSamplesSqrt=round(sqrtf(numSamples));

	// render all geometries, depth pass:
	// HACK - for now, just render one geometry:
	Assert(m_Geometries.GetCount());
	Vector3 boundSphere=m_Geometries[0]->ComputeBoundSphere();

	for(int i=0;i<numSamplesSqrt;i++) 
	{
		float size=boundSphere.w;
		float y=size*cosf(PI*float(i)/float(numSamplesSqrt>>1));
		float s=sinf(PI*float(i)/float(numSamplesSqrt>>1));
		for(int j=0;j<=numSamplesSqrt;j++) 
		{
			float x=size*s*cosf(2.0f*PI*float(j)/float(numSamplesSqrt));
			float z=size*s*sinf(2.0f*PI*float(j)/float(numSamplesSqrt));


			Matrix34 matrix;

			// setup camera:
			matrix.Identity();
			Vector3 boundSphereOrigin(boundSphere.x,boundSphere.y,boundSphere.z);
			matrix.LookAt(boundSphereOrigin);
			matrix.d.Set(x+boundSphereOrigin.x,y+boundSphereOrigin.y,z+boundSphereOrigin.z);
			m_Viewport.SetCameraMtx(matrix);

			grcState::SetBlendSet(grcbsNormal);
			grcState::SetCullMode(grccmNone);
			grcState::SetDepthTest(true);
			grcState::SetAlphaBlend(true);
			grcState::SetAlphaTest(false);
			

			m_Viewport.Ortho(0.0f,1.0f,0.0f,1.0f,0.0f,1.0f);
			m_Geometries[0]->Render();
		}
	}


}
#endif


void pltLightMapAmbientOcclusion::RenderToTexture()
{
	// clear render target:
	Color32 clearColor(0xffffffff);
	GRCDEVICE.Clear(true,clearColor,true,grcDepthFarthest,0);

	Matrix34 matrix;

	// setup camera:
	matrix.Identity();
	matrix.LookDown(m_CurrentTexelNormal,YAXIS);
	matrix.d=m_CurrentTexelPosition;
	m_Viewport.SetCameraMtx(matrix);
	

	// render all geometries, normal pass:
	for (int i=0;i<m_Geometries.GetCount();i++)
	{
		m_Geometries[i]->Render();
	}

	// render bound sphere, and do occlusion query:
	m_OcclusionQueries[m_CurrentQuery]->Issue(D3DISSUE_BEGIN);

	matrix.Identity();

#pragma warning(disable: 4996)
	GRCDEVICE.BlitRect(0,0,LEXEL_VIEW_WIDTH,LEXEL_VIEW_HEIGHT,grcDepthFarthest,0,0,0,0,Color32(255,0,0));

	m_OcclusionQueries[m_CurrentQuery]->Issue(D3DISSUE_END);
}

inline Vector3 ArbitraryVectorRotate(Vector3::Vector3Param pointToRotate, float angleinRadians, Vector3::Vector3Param axis)
{
	float costheta,vSintheta;

	costheta = (float)cosf(angleinRadians);
	vSintheta = (float)sinf(angleinRadians);

	Vector3 outResult;

	outResult.x=outResult.y=outResult.z=0.0f;

	outResult.x += (costheta + (1 - costheta) * axis.x * axis.x) * pointToRotate.x;
	outResult.x += ((1 - costheta) * axis.x * axis.y - axis.z * vSintheta) * pointToRotate.y;
	outResult.x += ((1 - costheta) * axis.x * axis.z + axis.y * vSintheta) * pointToRotate.z;

	outResult.y += ((1 - costheta) * axis.x * axis.y + axis.z * vSintheta) * pointToRotate.x;
	outResult.y += (costheta + (1 - costheta) * axis.y * axis.y) * pointToRotate.y;
	outResult.y += ((1 - costheta) * axis.y * axis.z - axis.x * vSintheta) * pointToRotate.z;

	outResult.z += ((1 - costheta) * axis.x * axis.z - axis.y * vSintheta) * pointToRotate.x;
	outResult.z += ((1 - costheta) * axis.y * axis.z + axis.x * vSintheta) * pointToRotate.y;
	outResult.z += (costheta + (1 - costheta) * axis.z * axis.z) * pointToRotate.z;

	return outResult;
}  

void pltLightMapAmbientOcclusion::FillLightMap(atArray<prtGeometry*>& geometries)
{
	grcState::Default();
	

	int lastLen=0;
	float totalTime=0.0f;

	m_Viewport.Perspective(HORIZONTAL_FIELD_OF_VIEW,0.0f,NEAR_CLIP_PLANE,FAR_CLIP_PLANE);
	m_Geometries=geometries;

	if (m_TextureRenderTarget==NULL)
		m_TextureRenderTarget=CreateOffscreenSurface();
	Color32* pixelsAmbientOcclusion=LockImage();
	Color32* pixelsRead		=m_LightMapDirect->LockIndexMap();

	// lock buffers:
	u32* indices=NULL;
	GetDxMesh().LockIndexBuffer(0x0,(LPVOID*)&indices);

	uvaUvAtlasHelper::Vertex* vertices=NULL;
	GetDxMesh().LockVertexBuffer(0x0,(LPVOID*)&vertices);

	// create a render target to handle the texture:
	grcRenderTarget*	renderTarget		=grcTextureFactory::GetInstance().CreateRenderTarget("lexelviewColor",
		grcrtPermanent,
		LEXEL_VIEW_WIDTH,
		LEXEL_VIEW_HEIGHT,
		32);
	grcRenderTarget*	renderTargetDepth	=grcTextureFactory::GetInstance().CreateRenderTarget("lexelviewDepth",
		grcrtDepthBuffer,
		LEXEL_VIEW_WIDTH,
		LEXEL_VIEW_HEIGHT,
		32);

//#define SAVE_IMAGE
#ifdef SAVE_IMAGE
	grcImage* image=grcImage::Create(LEXEL_VIEW_WIDTH,LEXEL_VIEW_HEIGHT,1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
	IDirect3DSurface9* surfaceDest=CreateOffscreenSurfaceStatic(LEXEL_VIEW_WIDTH,LEXEL_VIEW_HEIGHT);
#endif

	bool* addedTexels	=new bool[GetTexHeight()*GetTexWidth()];
	bool* blockStart	=new bool[GetTexHeight()*GetTexWidth()];

	int numPixelsInBlock=0;

	float degreesToRotate=(m_NumViewpoints-1)<1?0.0f:DtoR*360.0f/(m_NumViewpoints-1);

	// create the queries:
	HRESULT hr=GRCDEVICE.GetCurrent()->CreateQuery(D3DQUERYTYPE_EVENT, NULL);
	if (hr!=S_OK)
	{
		Quitf("Hardware occlusion querying not supported!");
	}
	m_OcclusionQueries=new IDirect3DQuery9*[m_NumViewpoints];
	for (int i=0;i<m_NumViewpoints;i++)
		GRCDEVICE.GetCurrent()->CreateQuery(D3DQUERYTYPE_OCCLUSION, &(m_OcclusionQueries[i]));

	// setup render state:
	grcLightState::SetEnabled(false);
	grcState::SetColorWrite(grccwNone);
	grcState::SetBlendSet(grcbsNormal);
	grcState::SetCullMode(grccmNone);
	grcState::SetDepthTest(true);
	grcState::SetDepthWrite(true);
	grcState::SetAlphaBlend(false);
	grcState::SetAlphaTest(false);
	grcBindTexture(NULL);

	for (int y=0;y<GetTexHeight();y++)
	{
		PrintProgress(lastLen,totalTime,y);

		sysTimer timer;

		float renderTime=0.0f;
		int xBlock=0,yBlock=0;
		for (int x=0;x<GetTexWidth();x++)
		{
			SetTexel(addedTexels,x,y,false);

			if (1)
			{
				bool alreadyInBlock=false;

				// check to see if are part of a block:
				if (x>0)
				{
					xBlock=x-1;
					yBlock=y;
					if (GetTexel(blockStart,xBlock,yBlock)==true)
						alreadyInBlock=true;
					else if (y>0)
					{
						yBlock=y-1;
						if (GetTexel(blockStart,xBlock,yBlock)==true)
							alreadyInBlock=true;
						else 
						{
							xBlock=x;
							if (GetTexel(blockStart,xBlock,yBlock)==true)
								alreadyInBlock=true;
						}
					}
				}

				// ...check if we can start a block:
				if (alreadyInBlock==false)
				{
					if (x>0 && y>0 && x<GetTexWidth()-2 && y<GetTexHeight()-2)
					{
						u32 currentValue=GetTexel(pixelsRead,x,y).GetColor();
						if (
							// left hand pixel line:
							GetTexel(pixelsRead,x-1,y).GetColor()==currentValue && 
							GetTexel(pixelsRead,x-1,y+1).GetColor()==currentValue && 

							// pixel line point is on:
							GetTexel(pixelsRead,x,y-1).GetColor()==currentValue &&
							GetTexel(pixelsRead,x,y+1).GetColor()==currentValue &&
							GetTexel(pixelsRead,x,y+2).GetColor()==currentValue &&

							// right hand pixel line:
							GetTexel(pixelsRead,x+1,y-1).GetColor()==currentValue &&
							GetTexel(pixelsRead,x+1,y).GetColor()==currentValue &&
							GetTexel(pixelsRead,x+1,y+1).GetColor()==currentValue &&
							GetTexel(pixelsRead,x+1,y+2).GetColor()==currentValue &&

							// right hand pixel line + 1:
							GetTexel(pixelsRead,x+2,y).GetColor()==currentValue && 
							GetTexel(pixelsRead,x+2,y+1).GetColor()==currentValue
							)
						{
							SetTexel(blockStart,x,y,true);
						}
					}
					else
						SetTexel(blockStart,x,y,false);

				}
				// pixel is part of a block, just use the color of the starting pixel (upper left corner pixel):
				else
				{
					SetTexel(blockStart,x,y,false);
					SetTexel(pixelsAmbientOcclusion,x,y,GetTexel(pixelsAmbientOcclusion,xBlock,yBlock));
					numPixelsInBlock++;
					continue;
				}
			}

			// get the position and render the scene from the point's point of view:
			if (pltLightMapDirect::GetPosAndNormalFromIndexMap(vertices,indices,pixelsRead,x,y,m_CurrentTexelPosition,m_CurrentTexelNormal))
			{
				sysTimer renderTimer;

				// lock our render target:
				grcTextureFactory::GetInstance().LockRenderTarget(0, renderTarget, renderTargetDepth);

	
				grcViewport::SetCurrent(&m_Viewport);
	
				// render from the normal:
				m_CurrentQuery=0;
				RenderToTexture();

				Vector3 baseNormal(m_CurrentTexelNormal);
				Vector3 ortho1,ortho2;
				baseNormal.MakeOrthonormals(ortho1,ortho2);
				// 0.5f==45 degrees:
				baseNormal.Lerp(0.5f,ortho1);
				baseNormal.Normalize();
				m_CurrentTexelNormal=baseNormal;

				// render from multiple viewpoints:
				for (int i=0;i<m_NumViewpoints-1;i++)
				{	
					m_CurrentQuery++;
					RenderToTexture();
					m_CurrentTexelNormal=ArbitraryVectorRotate(m_CurrentTexelNormal,degreesToRotate,baseNormal);
				}

				// we're done rendering into the render target:
				grcTextureFactory::GetInstance().UnlockRenderTarget(0);

#ifdef SAVE_IMAGE

				IDirect3DSurface9* surfaceSource=NULL;

				static_cast<IDirect3DTexture9*>(renderTarget->GetTexturePtr())->GetSurfaceLevel(0,&surfaceSource);
				GRCDEVICE.GetCurrent()->GetRenderTargetData(surfaceSource, surfaceDest);

				Color32* colors=LockImageStatic(*surfaceDest);

				bool colorOtherThanWhite=false;
				for (int lexelY=0;lexelY<LEXEL_VIEW_HEIGHT;lexelY++)
					for (int lexelX=0;lexelX<LEXEL_VIEW_WIDTH;lexelX++)
					{
						Color32 color=GetTexel(colors,lexelX,lexelY,LEXEL_VIEW_WIDTH);
						image->SetPixel(lexelX,lexelY,color.GetColor());
						if (color.GetRed()!=0xff || color.GetGreen()!=0xff || color.GetBlue()!=0xff)
							colorOtherThanWhite=true;
					}

					if (colorOtherThanWhite)
					{
						char fileName[256];
						sprintf(fileName,"d:\\lightmaps\\%dx%d",x,y);
						image->SaveDDS(fileName);
					}

				UnlockImageStatic(*surfaceDest);
#endif

				float accessibility=0.0f;

				// collect the result of the render:
				for (int i=0;i<m_NumViewpoints;i++)
				{
					DWORD numberOfPixelsDrawn;
					while(S_FALSE == m_OcclusionQueries[i]->GetData( &numberOfPixelsDrawn, sizeof(DWORD), D3DGETDATA_FLUSH ))
						; // wait until we get the result

					// see how many pixels of the bound sphere were drawn:
					accessibility+=(float)numberOfPixelsDrawn/(float)LEXEL_VIEW_NUM_PIXELS;
				}
	
				// get the average:
				accessibility/=m_NumViewpoints;
				Color32 finalColor(accessibility,accessibility,accessibility,1.0f);
		
				renderTime+=renderTimer.GetMsTime();

				SetTexel(pixelsAmbientOcclusion,x,y,finalColor);
			}
			else
				SetTexel(pixelsAmbientOcclusion,x,y,(Color32)ILLEGAL_TEXEL_COLOR);
		}

		totalTime+=timer.GetTime();
	}

	// add smeared borders are the texture charts:
	AddSmearedBorder(addedTexels,pixelsRead,pixelsAmbientOcclusion,2.0f);

	delete [] addedTexels;
	delete [] blockStart;

	grcViewport::SetCurrent(NULL);

	UnlockImage();
	m_LightMapDirect->UnlockIndexMap();

	GetDxMesh().UnlockIndexBuffer();
	GetDxMesh().UnlockVertexBuffer();
}

}; // namespace rage
