// 
// prttools/direct.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PRTTOOLS_DIRECT_H
#define PRTTOOLS_DIRECT_H

#include "convert.h"
#include "geom.h"

#include "atl/array.h"
#include "atl/ptr.h"
#include "grcore/viewport.h"
#include "system/noncopyable.h"
#include "vector/color32.h"

namespace rage {

class prtConvert;
class prtDirectLighting;
class prtGeometry;

class pltLightMapDirect : public pltLightMap
{
public:
	pltLightMapDirect(prtDirectLighting& direct,prtConvert& convert,prtGeometry& geometry,int meshIndex,int width,int height);

	static void InitClass();

	virtual ID3DXMesh& GetDxMesh()
	{
		return m_Geometry->GetDxMesh(m_MeshIndex);
	}

	virtual void SetDxMesh(ID3DXMesh& mesh)
	{
		m_Geometry->SetDxMesh(m_MeshIndex,mesh);
	}

	virtual ID3DXMesh& GetNewDxMesh()
	{
		Assert(m_NewDxMesh);
		return *m_NewDxMesh;
	}

	virtual prtConvert& GetConvert()
	{
		return *m_Convert;
	}

	prtDirectLighting& GetDirect()
	{
		return *m_Direct;
	}

	prtGeometry&	GetGeometry()
	{
		return *m_Geometry;
	}

	int GetMeshIndex() const
	{
		return m_MeshIndex;
	}

	bool FillLightMap();
	void RenderFromLightPOVBroken(atArray<prtGeometry*>& geometries);
	void RenderFromLightPOVBetter(atArray<prtGeometry*>& geometries);

	virtual void RenderToTexture();

	Color32* LockIndexMap();
	void UnlockIndexMap();

protected:
	bool GetPosAndNormalFromIndexMap(uvaUvAtlasHelper::Vertex vertices[],u32 indices[],Color32 pixelsRead[],u32 x,u32 y,Vector3& pos,Vector3& nrm);
	void PrintProgress(int& lastLen,float totalTime,int y);

private:
	NON_COPYABLE(pltLightMapDirect);

	Color32 RenderToTextureOcclusionCheck(grcRenderTarget& renderTarget,grcRenderTarget& depth,Vector3::Vector3Param worldSpacePos,Vector3::Vector3Param normalt);
	Color32 EvaluateColor(const Vector3 &worldSpacePos, const Vector3 &normal);

	IDirect3DSurface9*	m_IndexMapSurface;
	atPtr<prtGeometry>	m_Geometry;
	int					m_MeshIndex;
	atPtr<prtConvert>	m_Convert;
	prtDirectLighting*	m_Direct;
	Vector3				m_CurrentWorldSpacePos;
	Vector3				m_CurrentNormal;
	grcViewport			m_Viewport;

	// statics:
	static grmShaderGroup*	sm_AllBlackShaderGroup;
	static grmShader*		sm_AllBlackShader;
};

class prtDirectLighting
{
	friend class pltLightMapDirect;

public:
	
	static const float NEAR_CLIP_PLANE;
	static const float FAR_CLIP_PLANE;
	static const float HORIZONTAL_FIELD_OF_VIEW;

	enum {LEXEL_VIEW_WIDTH=128,LEXEL_VIEW_HEIGHT=128,LEXEL_VIEW_NUM_PIXELS=LEXEL_VIEW_WIDTH*LEXEL_VIEW_HEIGHT};

	struct LightTemp
	{
		Vector3 m_Position;
		Vector3 m_Color;
		float   m_Intensity;
	};

	void SetLight(const Vector3& position,const Vector3& color,float intensity)
	{
		m_Lights[0].m_Position		=position;
		m_Lights[0].m_Color			=color;
		m_Lights[0].m_Intensity		=intensity;
	}

	prtDirectLighting()
	{
		m_Convert.AddRef();
	}

	void Init();
	void AddGeometry(prtGeometry& geometry);
	void SaveGeometries();
	void ComputeLightMaps();
	void ComputeLightMapsAlt();
	
	atArray<prtGeometry*>& GetGeometries()
	{
		return m_Geometries;
	}

	void SaveLightMaps(const char* fileName,bool reload);

	prtConvert& GetConvert()
	{
		return m_Convert;
	}

	IDirect3DQuery9& GetOcclusionQuery() const
	{
		return *m_OcclusionQuery;
	}

protected:
	bool GetPosAndNormalFromIndexMap(u32 pixelsRead[],u32 x,u32 y,Vector3& pos,Vector3& nrm);

	enum {LIGHT_COUNT=1};

	atArray<prtGeometry*>		m_Geometries;
	prtConvert					m_Convert;
	LightTemp					m_Lights[LIGHT_COUNT];
	IDirect3DSurface9*			m_IndexMapSurface;
	IDirect3DQuery9*			m_OcclusionQuery;
};

} // namespace rage

#endif // PRTTOOLS_DIRECT_H
