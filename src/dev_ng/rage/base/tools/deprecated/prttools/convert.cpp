// 
// prttools/convert.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

// for GetConsoleWindow():
#define _WIN32_WINNT 0x500

#include "convert.h"

#include "lightmap.h"

#include "file/asset.h"
#include "file/stream.h"
#include "file/token.h"
#include "mesh/serialize.h"
#include "system/param.h"
#include "system/xtl.h"

namespace rage {

// Vertex declaration
D3DVERTEXELEMENT9 s_VertexDecl[MAX_FVF_DECL_SIZE] = 
{
	{0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,		0},
	{0, 12, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR,		2}, // dummy member for alignment with uvaAtlasHelper::Vertex
	{0, 16, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,			0},
	{0, 28, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR,		3}, // dummy member for alignment with uvaAtlasHelper::Vertex

	// To do CPCA, we need to store (g_dwNumPCAVectors + 1) scalers per vertex, so 
	// make the mesh have a known decl to store this data.  Since we can't do 
	// skinning and PRT at once, we use D3DDECLUSAGE_BLENDWEIGHT[0] 
	// to D3DDECLUSAGE_BLENDWEIGHT[6] to store our per vertex data needed for PRT.
	// Notice that D3DDECLUSAGE_BLENDWEIGHT[0] is a float1, and
	// D3DDECLUSAGE_BLENDWEIGHT[1]-D3DDECLUSAGE_BLENDWEIGHT[6] are float4.  This allows 
	// up to 24 PCA weights and 1 float that gives the vertex shader 
	// an index into the vertex's cluster's data
	{0, 32, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT,	1},
	{0, 48, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT,	2},
	{0, 64, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT,	3},
	{0, 80, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT,	4},
	{0, 96, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT,	5},
	{0,112, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT,	6},
	{0,128, D3DDECLTYPE_FLOAT1, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT,	0},

	{0,132, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,		0},
	{0,140, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,		1},
	{0,148, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR,		0},
	{0,152, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,		2}, // dummy member for alignment with uvaAtlasHelper::Vertex
	D3DDECL_END()
};

CompileTimeAssert(sizeof(uvaUvAtlasHelper::Vertex)==160);

PARAM(subdivide,"specifies # of segments per edge to tessellate the result mesh (can be a floating point #).");

void prtConvert::CreateNULLRefDevice()
{
	m_Device=NULL;

	HRESULT hr;
	IDirect3D9* pD3D = Direct3DCreate9( D3D_SDK_VERSION );
	if( NULL == pD3D )
		return;

	D3DDISPLAYMODE Mode;
	pD3D->GetAdapterDisplayMode(0, &Mode);

	D3DPRESENT_PARAMETERS pp;
	ZeroMemory( &pp, sizeof(D3DPRESENT_PARAMETERS) ); 
	pp.BackBufferWidth  = 1;
	pp.BackBufferHeight = 1;
	pp.BackBufferFormat = Mode.Format;
	pp.BackBufferCount  = 1;
	pp.SwapEffect       = D3DSWAPEFFECT_COPY;
	pp.Windowed         = TRUE;

	IDirect3DDevice9* pd3dDevice;
	hr = pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_NULLREF, GetConsoleWindow(), 
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &pp, &pd3dDevice );
	//SAFE_RELEASE( pD3D );
	if( FAILED(hr) || pd3dDevice == NULL )
		return;

	m_Device=pd3dDevice;
}

mshMesh* prtConvert::LoadRageMesh(const char* inputPath)
{
	mshMesh* mesh=new mshMesh;
	bool success = SerializeFromFile(inputPath,*mesh,"mesh");
	if (!success)
	{
		delete mesh;
		mesh=NULL;
		Quitf( "Couldn't load file '%s'!", inputPath );
	}

	return mesh;
}

ID3DXMesh* prtConvert::CreateD3DXMesh(mshMesh& mesh,atArray<atString>& outMaterialNames)
{
	int totalVertCount=0;
	int totalPrimCount=0;

	// count # of primitives and vertices:
	for (int mtlIdx=0;mtlIdx<mesh.GetMtlCount();mtlIdx++)
	{
		mshMaterial& mtl=mesh.GetMtl(mtlIdx);

		// keep material names:
		outMaterialNames.Grow()=mtl.Name;

		// count the # of degenerate triangles:
		int primCount=0;
		for (int i=0;i<mtl.Prim.GetCount();i++)
		{
			const mshPrimitive& prim=mtl.Prim[i];
			if (prim.Type==mshTRISTRIP || prim.Type==mshTRISTRIP2)
			{
				int primIndex=0;
				for (int j=0;j<prim.Idx.GetCount()-2;j++)
				{
					int index1=prim.Idx[primIndex];
					int index2=prim.Idx[primIndex+1];
					int index3=prim.Idx[primIndex+2];

					if (index1==index2 || index2==index3 || index1==index3)
					{
						// don't count degenerates!
					}
					else 
						primCount++;

					primIndex++;
				}

			}
			else if (prim.Type==mshTRIANGLES)
			{
				for (int j=0;j<prim.Idx.GetCount();j+=3)
				{
					int index1=prim.Idx[j];
					int index2=prim.Idx[j+1];
					int index3=prim.Idx[j+2];
					// don't count degenerates:
					if (index1==index2 || index2==index3 || index1==index3)
					{
						// don't count degenerates!
					}
					else
						primCount++;
				}
			}
		}

		totalVertCount+=mtl.GetVertexCount();
		totalPrimCount+=primCount;
	}

	// create the mesh that we'll fill with copied data:
	DWORD options=D3DXMESH_SYSTEMMEM|D3DXMESH_32BIT;
	ID3DXMesh* theMesh;
	D3DXCreateMesh(totalPrimCount,totalVertCount,options,s_VertexDecl,m_Device,&theMesh);
	
	// create the attribute table:
	D3DXATTRIBUTERANGE* attribTable=new D3DXATTRIBUTERANGE[mesh.GetMtlCount()];

	
	int currentVertCount=0;
	int currentPrimCount=0;
	for (int mtlIdx=0;mtlIdx<mesh.GetMtlCount();mtlIdx++)
	{
		const mshMaterial& mtl=mesh.GetMtl(mtlIdx);

		// copy the vertex buffer:
		uvaUvAtlasHelper::Vertex* dataTo;
		theMesh->LockVertexBuffer(0x0,(LPVOID*)&dataTo);
		int vertCount=mtl.GetVertexCount();
		for (int i=0;i<vertCount;i++)
		{
			uvaUvAtlasHelper::Vertex& theVert=dataTo[i+currentVertCount];
			theVert.m_Position.x=mtl.GetPos(i).x;
			theVert.m_Position.y=mtl.GetPos(i).y;
			theVert.m_Position.z=mtl.GetPos(i).z;
			theVert.m_Position.w=0.0f;

			Vector3 nrm=mtl.GetNormal(i);
			nrm.Normalize();

			// HACK: get rid of zero length normals:
			if (nrm.Mag2()<0.9f)
				nrm.Set(0.0f,1.0f,0.0f);

			theVert.m_Normal.x=nrm.x;
			theVert.m_Normal.y=nrm.y;
			theVert.m_Normal.z=nrm.z;
			theVert.m_Normal.w=0.0f;

			theVert.m_Texcoord.x=mtl.GetTexCoord(i,0).x;
			theVert.m_Texcoord.y=mtl.GetTexCoord(i,0).y;

			if (mtl.TexSetCount<=1)
			{
				theVert.m_Texcoord2.Zero();
			}
			else
			{
				theVert.m_Texcoord2.x=mtl.GetTexCoord(i,1).x;
				theVert.m_Texcoord2.y=mtl.GetTexCoord(i,1).y;
			}
		
			theVert.m_Color=0xFFFFFFFF;
			theVert.m_BlendWeight0=0;
			for (int j=0;j<uvaUvAtlasHelper::NUM_BLEND_WEIGHTS;j++)
			{
				Vector4& vector=theVert.m_BlendWeight[j];
				vector.x=vector.y=vector.z=vector.w=0.0f;
			}
		}
	
		theMesh->UnlockVertexBuffer();

		u32* dataToIdx;

		// copy the index buffer:
		theMesh->LockIndexBuffer(0x0,(LPVOID*)&dataToIdx);
		int index=0;
		int primCount=0;
		for (int i=0;i<mtl.Prim.GetCount();i++)
		{
			const mshPrimitive& prim=mtl.Prim[i];
			if (prim.Type==mshTRISTRIP || prim.Type==mshTRISTRIP2)
			{

				bool switchOrder=prim.Type==mshTRISTRIP;
				int primIndex=0;
				for (int j=0;j<prim.Idx.GetCount()-2;j++)
				{
					int index1=prim.Idx[primIndex];
					int index2=prim.Idx[primIndex+1];
					int index3=prim.Idx[primIndex+2];
					if (index1==index2 || index2==index3 || index1==index3)
					{
						// skip degenerate triangles!
					}
					else if (switchOrder)
					{
						dataToIdx[(currentPrimCount*3)+index++]=index1+currentVertCount;
						dataToIdx[(currentPrimCount*3)+index++]=index2+currentVertCount;
						dataToIdx[(currentPrimCount*3)+index++]=index3+currentVertCount;
						primCount++;
					}
					else 
					{
						dataToIdx[(currentPrimCount*3)+index++]=index3+currentVertCount;
						dataToIdx[(currentPrimCount*3)+index++]=index2+currentVertCount;
						dataToIdx[(currentPrimCount*3)+index++]=index1+currentVertCount;
						primCount++;
					}
					switchOrder=!switchOrder;
					primIndex++;
					
				}
			}
			else if (prim.Type==mshTRIANGLES)
			{
				for (int j=0;j<prim.Idx.GetCount();j+=3)
				{
					int index1=prim.Idx[j];
					int index2=prim.Idx[j+1];
					int index3=prim.Idx[j+2];
					if (index1==index2 || index2==index3 || index1==index3)
					{
						// skip degenerate triangles!
					}
					else
					{
						dataToIdx[(currentPrimCount*3)+index++]=index1+currentVertCount;
						dataToIdx[(currentPrimCount*3)+index++]=index2+currentVertCount;
						dataToIdx[(currentPrimCount*3)+index++]=index3+currentVertCount;
						primCount++;
					}
				}
			}
			else 
			{
				Errorf("Primitive Type is not supported!");
				return NULL;
			}

	
			//currentOffset+=primCount;
		}

		// set attribute table info:
		attribTable[mtlIdx].AttribId		=mtlIdx;
		attribTable[mtlIdx].FaceStart		=currentPrimCount;
		attribTable[mtlIdx].FaceCount		=primCount;
		attribTable[mtlIdx].VertexStart		=currentVertCount;
		attribTable[mtlIdx].VertexCount		=vertCount;

		currentVertCount+=vertCount;


	
	
		theMesh->UnlockIndexBuffer();

		// set attributes:
		DWORD* attribs;
		theMesh->LockAttributeBuffer(0x0,&attribs);
		for (int i=0;i<primCount;i++)
			attribs[currentPrimCount+i]=mtlIdx;
		theMesh->UnlockAttributeBuffer();

		currentPrimCount+=primCount;
	}

	theMesh->SetAttributeTable(attribTable,mesh.GetMtlCount());

	// cleanup the mesh:
	DWORD* aAdjacencyIn = new DWORD[theMesh->GetNumFaces() * 3]; 
	theMesh->GenerateAdjacency(1e-6f,aAdjacencyIn);
	DWORD* aAdjacencyOut = new DWORD[theMesh->GetNumFaces() * 3]; 

	// RAY - had to add bowties for the saloon  used to be D3DXCLEAN_OPTIMIZATION

	D3DXCleanMesh( D3DXCLEAN_SIMPLIFICATION,theMesh,aAdjacencyIn,&theMesh,aAdjacencyOut,NULL);
	
	HRESULT hr = D3DXValidMesh(theMesh,aAdjacencyOut, NULL );
	if ( hr != S_OK )
	{

		Warningf( "Mesh Is not valid " );
	}
	return theMesh;
}

//-----------------------------------------------------------------------------
// Make the mesh have a known decl in order to pass per vertex CPCA 
// data to the shader
//-----------------------------------------------------------------------------
bool prtConvert::AdjustMeshDecl(ID3DXMesh*& inOutDxMesh )
{
	LPD3DXMESH inMesh	= inOutDxMesh;
	LPD3DXMESH outMesh	= NULL;

	// To do CPCA, we need to store (g_dwNumPCAVectors + 1) scalers per vertex, so 
	// make the mesh have a known decl to store this data.  Since we can't do 
	// skinning and PRT at once, we use D3DDECLUSAGE_BLENDWEIGHT[0] 
	// to D3DDECLUSAGE_BLENDWEIGHT[6] to store our per vertex data needed for PRT.
	// Notice that D3DDECLUSAGE_BLENDWEIGHT[0] is a float1, and
	// D3DDECLUSAGE_BLENDWEIGHT[1]-D3DDECLUSAGE_BLENDWEIGHT[6] are float4.  This allows 
	// up to 24 PCA weights and 1 float that gives the vertex shader 
	// an index into the vertex's cluster's data
	V( inMesh->CloneMesh( inMesh->GetOptions(), s_VertexDecl, m_Device, &outMesh ) );

	// Make sure there are normals which are required for lighting
	if( !DoesMeshHaveUsage(*inMesh,D3DDECLUSAGE_NORMAL )) 
		V( D3DXComputeNormals( outMesh, NULL ) ); 

	SAFE_RELEASE( inOutDxMesh );

	inOutDxMesh = outMesh;

	return true;
}

Vector4 prtConvert::GetPRTDiffuse( int clusterOffset, const Vector4* pcaWeights/*[NUM_PCA/4]*/, const float* prtConstantsFloat,u32 numPcas)
{
	const Vector4* prtConstants=reinterpret_cast<const Vector4*>(prtConstantsFloat);

	// With compressed PRT, a single diffuse channel is caluated by:
	//       R[p] = (M[k] dot L') + sum( w[p][j] * (B[k][j] dot L');
	// where the sum runs j between 0 and # of PCA vectors
	//       R[p] = exit radiance at point p
	//       M[k] = mean of cluster k 
	//       L' = source radiance coefficients
	//       w[p][j] = the j'th PCA weight for point p
	//       B[k][j] = the j'th PCA basis vector for cluster k
	//
	// Note: since both (M[k] dot L') and (B[k][j] dot L') can be computed on the CPU, 
	// these values are passed in as the array aPRTConstants.   

	Vector4 vAccumR(0,0,0,0);
	Vector4 vAccumG(0,0,0,0);
	Vector4 vAccumB(0,0,0,0);

	// For each channel, multiply and sum all the vPCAWeights[j] by aPRTConstants[x] 
	// where: vPCAWeights[j] is w[p][j]
	//		  aPRTConstants[x] is the value of (B[k][j] dot L') that was
	//		  calculated on the CPU and passed in as a shader constant
	// Note this code is multipled and added 4 floats at a time since each 
	// register is a 4-D vector, and is the reason for using (NUM_PCA/4)
	Vector4 result(0.0f,0.0f,0.0f,0.0f);
	for (u32 j=0; j < (numPcas/4); j++) 
	{
		/*
		vAccumR += pcaWeights[j] * aPRTConstants[clusterOffset+1+(NUM_PCA/4)*0+j];
		vAccumG += pcaWeights[j] * aPRTConstants[clusterOffset+1+(NUM_PCA/4)*1+j];
		vAccumB += pcaWeights[j] * aPRTConstants[clusterOffset+1+(NUM_PCA/4)*2+j];
		*/
		result.Multiply(pcaWeights[j],prtConstants[clusterOffset+1+(numPcas/4)*0+j]);
		vAccumR.Add(result);
		result.Multiply(pcaWeights[j],prtConstants[clusterOffset+1+(numPcas/4)*1+j]);
		vAccumG.Add(result);
		result.Multiply(pcaWeights[j],prtConstants[clusterOffset+1+(numPcas/4)*2+j]);
		vAccumB.Add(result);
	}    

	// Now for each channel, sum the 4D vector and add aPRTConstants[x] 
	// where: aPRTConstants[x] which is the value of (M[k] dot L') and
	//		  was calculated on the CPU and passed in as a shader constant.
	Vector4 diffuse = prtConstants[clusterOffset];

	/*
	diffuse.x += dot(vAccumR,1);
	diffuse.y += dot(vAccumG,1);
	diffuse.z += dot(vAccumB,1);
	*/

	diffuse.x += vAccumR.Dot(Vector4(1.0f,1.0f,1.0f,1.0f));
	diffuse.y += vAccumG.Dot(Vector4(1.0f,1.0f,1.0f,1.0f));
	diffuse.z += vAccumB.Dot(Vector4(1.0f,1.0f,1.0f,1.0f));
	diffuse.w = 1.0f;


#ifdef CRAP
	// I have to clamp it for now:
	diffuse.x=Clamp(diffuse.x,0.0f,1.0f);
	diffuse.y=Clamp(diffuse.y,0.0f,1.0f);
	diffuse.z=Clamp(diffuse.z,0.0f,1.0f);
#endif
	
	return diffuse;
}

ID3DXMesh* prtConvert::SortByAttribute(ID3DXMesh& dxMesh)
{
	DWORD* adjacencyIn = new DWORD[dxMesh.GetNumFaces() * 3]; 
	DWORD* adjacencyOut = new DWORD[dxMesh.GetNumFaces() * 3]; 
	ID3DXMesh* sortedMesh;
	dxMesh.GenerateAdjacency(1e-6f,adjacencyIn);
	dxMesh.Optimize(D3DXMESHOPT_ATTRSORT,adjacencyIn,adjacencyOut,NULL,NULL,&sortedMesh);
	SAFE_DELETE_ARRAY(adjacencyIn);
	SAFE_DELETE_ARRAY(adjacencyOut);

	return sortedMesh;
}

void prtConvert::OutputMesh(const char* fileName,ID3DXMesh& dxMesh,atArray<atString>& materialNames)
{
	OutputMesh(fileName,dxMesh,NULL,0,materialNames,false);
}

void prtConvert::OutputMesh(const char* fileName,ID3DXMesh& dxMesh,float* prtConstants,u32 numPcas,atArray<atString>& materialNames,bool outputCpvOnly)
{
	// create the mesh:
	mshMesh& newMesh=*(new mshMesh(false));

	ID3DXMesh* sortedMesh=SortByAttribute(dxMesh);

	D3DXATTRIBUTERANGE* attribTable;
	DWORD tableSize;
	sortedMesh->GetAttributeTable(NULL,&tableSize);
	attribTable=new D3DXATTRIBUTERANGE[tableSize];
	sortedMesh->GetAttributeTable(attribTable,&tableSize);

	uvaUvAtlasHelper::Vertex* vertices;
	sortedMesh->LockVertexBuffer(0x0,(LPVOID*)&vertices);

	u32* indices;
	sortedMesh->LockIndexBuffer(0x0,(LPVOID*)&indices);

	// loop through each material:
	typedef char matName[128];
	atArray<matName> matNames;
	matNames.Resize(tableSize);
	int currentIndexOffset=0;
	for (DWORD table=0;table<tableSize;table++)
	{
		mshMaterial& material=*(new mshMaterial);
		material.TexSetCount=outputCpvOnly?1:2;

		material.Name=materialNames[table];

		const D3DXATTRIBUTERANGE& attr=attribTable[table];
#if __TOOL
		material.Verts.Reallocate(attr.VertexCount);
#endif
		Vector3 pos,normal;
		Vector2 texCoords[2];
		Vector4 color;
		mshBinding binding;
		binding.Reset();

		// copy vertices for material:
		for (DWORD vert=0;vert<attr.VertexCount;vert++)
		{
			const uvaUvAtlasHelper::Vertex& vertex=vertices[attr.VertexStart+vert];
			pos.Set(vertex.m_Position.x,vertex.m_Position.y,vertex.m_Position.z);
			normal.Set(vertex.m_Normal.x,vertex.m_Normal.y,vertex.m_Normal.z);
			normal.Normalize();
			texCoords[0].Set(vertex.m_Texcoord.x,vertex.m_Texcoord.y);
			color.Set(1.0f,1.0f,1.0f,1.0f);
			if (outputCpvOnly)
			{
#define GET_A(c) ((float)(((c)&0xff000000)>>24))
#define GET_R(c) ((float)(((c)&0x00ff0000)>>16))
#define GET_G(c) ((float)(((c)&0x0000ff00)>>8))
#define GET_B(c) ((float)((c)&0x000000ff))

				color=GetPRTDiffuse(vertex.m_BlendWeight0,vertex.m_BlendWeight,prtConstants,numPcas);
			}
			else 
			{
				texCoords[1].Set(vertex.m_Texcoord2.x,1.0f-vertex.m_Texcoord2.y);
			}

			// add the vertex:
			material.AddVertex(pos,binding,normal,color,Vector4(1,1,1,1),Vector4(1,1,1,1),material.TexSetCount,texCoords);
		}

		// copy faces for material:
		DWORD faceCount=attr.FaceCount;
		material.Prim.Resize(1);
		mshPrimitive& prim=material.Prim[0];
		prim.Type=mshTRIANGLES;
		
		prim.Idx.Resize(faceCount*3);
		prim.Priority=1;

		for (DWORD index=0;index<faceCount;index++)
		{
			prim.Idx[index*3+0]=indices[(attr.FaceStart+index)*3+0]-currentIndexOffset;
			prim.Idx[index*3+1]=indices[(attr.FaceStart+index)*3+1]-currentIndexOffset;
			prim.Idx[index*3+2]=indices[(attr.FaceStart+index)*3+2]-currentIndexOffset;
		}
	
		newMesh.AddMtl(material);

		currentIndexOffset+=attr.VertexCount;
	}

	// clean up:
	SAFE_DELETE_ARRAY(attribTable);

	// unlock buffers:
	sortedMesh->UnlockIndexBuffer();
	sortedMesh->UnlockVertexBuffer();

	SAFE_RELEASE(sortedMesh);

	// optimize our mesh:
	newMesh.Tristrip();

	// save the file:
	const char* name=fileName;
	fiSafeStream stream=ASSET.Create(name,"mesh");
	//fiBinTokenizer tokenizer;
	fiAsciiTokenizer tokenizer;
	tokenizer.Init(name,stream);
	mshSerializer serializer(tokenizer,true);
	newMesh.Serialize(serializer);
	delete &newMesh;
}

bool prtConvert::DoesMeshHaveUsage( ID3DXMesh& mesh, BYTE Usage ) const
{
	D3DVERTEXELEMENT9 decl[MAX_FVF_DECL_SIZE]; 
	mesh.GetDeclaration(decl); 

	for(int di=0;di<MAX_FVF_DECL_SIZE;di++) 
	{ 
		if( decl[di].Usage == Usage ) 
			return true; 
		if( decl[di].Stream==255 ) 
			return false; 
	} 

	return false; 
} 


} // namespace rage
