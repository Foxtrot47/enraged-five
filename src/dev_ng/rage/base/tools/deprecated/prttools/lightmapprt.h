// 
// prttools/lightmapprt.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PRTTOOLS_LIGHTMAPPRT_H
#define PRTTOOLS_LIGHTMAPPRT_H

#include "prtsim.h"
#include "lightmap.h"

namespace rage {

class pltLightMapPrt : public pltLightMap
{
public:
	pltLightMapPrt(prtSimulator& simulator,int width,int height);
	virtual ~pltLightMapPrt() {}

private:
	// just declare, don't define these:
	pltLightMapPrt(const pltLightMapPrt& lightMap);
	pltLightMapPrt& operator=(const pltLightMapPrt& lightMap);

protected:
	// overrides:
	ID3DXMesh& GetDxMesh()
	{
		Assert(m_Simulator.m_ConcatDxMesh.m_DxMesh);
		return *m_Simulator.m_ConcatDxMesh.m_DxMesh;
	}

	ID3DXMesh& GetOriginalDxMesh()
	{
		Assert(m_Simulator.m_ConcatDxMesh.m_OriginalDxMesh);
		return *m_Simulator.m_ConcatDxMesh.m_OriginalDxMesh;
	}

	void SetDxMesh(ID3DXMesh& mesh)
	{
		m_Simulator.m_ConcatDxMesh.m_DxMesh=&mesh;
	}

	virtual prtConvert& GetConvert()
	{
		return m_Simulator.m_Convert;
	}


	void RenderToTexture();
	void RenderLightMap();
	void RenderTriangles();

	prtSimulator&								m_Simulator;
	atArray<FaceInfo,unsigned int,UINT_MAX>		m_FaceInfos;
};

} // namespace rage


#endif // PRTTOOLS_LIGHTMAPPRT_H
