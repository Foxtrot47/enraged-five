// 
// prttools/progressrad.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "progressrad.h"

#include "direct.h"

#include "grcore/state.h"
#include "grcore/texture.h"
#include "grmodel/model.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "system/timer.h"

namespace rage {

grmShaderGroup*	pltLightMapProgressiveRadiosity::sm_ShaderGroup;
grmShader*		pltLightMapProgressiveRadiosity::sm_Shader;

const float		prtProgressiveRadiosity::NEAR_CLIP_PLANE			=0.01f;
const float		prtProgressiveRadiosity::FAR_CLIP_PLANE				=1000.0f;
const float		prtProgressiveRadiosity::HORIZONTAL_FIELD_OF_VIEW	=160.0f;


void pltLightMapProgressiveRadiosity::InitClass()
{
	sm_ShaderGroup = grmShaderFactory::GetInstance().CreateGroup();
	Assert(sm_ShaderGroup);

#if __TOOL
	grmShaderFactory::GetInstance().SetShaderLibPath("tune\\shaders\\lib");
#endif
	sm_Shader = grmShaderFactory::GetInstance().Create("radiosity");
	Assert(sm_Shader);
	sm_Shader->Load("radiosity", 0 ,0, NULL);
}

pltLightMapProgressiveRadiosity::pltLightMapProgressiveRadiosity(pltLightMapDirect& lightMapDirect) 
	: pltLightMapDirect(lightMapDirect.GetDirect(),
						lightMapDirect.GetConvert(),
						lightMapDirect.GetGeometry(),
						lightMapDirect.GetMeshIndex(),
						lightMapDirect.GetTexWidth(),
						lightMapDirect.GetTexHeight()),
	m_LightMapDirect(&lightMapDirect)
{
	m_NewDxMesh=&lightMapDirect.GetNewDxMesh();
	m_GutterSize=lightMapDirect.GetGutterSize();
}

void pltLightMapProgressiveRadiosity::IterateLightmap(atArray<prtGeometry*>& geometries,float modulationMask[])
{
	grcState::Default();
	

	int lastLen=0;
	float totalTime=0.0f;

	m_Viewport.Perspective(prtProgressiveRadiosity::HORIZONTAL_FIELD_OF_VIEW,0.0f,prtProgressiveRadiosity::NEAR_CLIP_PLANE,prtProgressiveRadiosity::FAR_CLIP_PLANE);
	m_Geometries=geometries;

	if (m_TextureRenderTarget==NULL)
		m_TextureRenderTarget=CreateOffscreenSurface();
	Color32* pixelsRadiosity=LockImage();
	Color32* pixelsDirect	=m_LightMapDirect->LockImage();
	Color32* pixelsRead		=m_LightMapDirect->LockIndexMap();

	// lock buffers:
	u32* indices=NULL;
	GetDxMesh().LockIndexBuffer(0x0,(LPVOID*)&indices);

	uvaUvAtlasHelper::Vertex* vertices=NULL;
	GetDxMesh().LockVertexBuffer(0x0,(LPVOID*)&vertices);

	// create a render target to handle the texture:
	grcRenderTarget*	renderTarget		=grcTextureFactory::GetInstance().CreateRenderTarget("lexelviewColor",
																								 grcrtPermanent,
																								 prtProgressiveRadiosity::LEXEL_VIEW_WIDTH,
																								 prtProgressiveRadiosity::LEXEL_VIEW_HEIGHT,
																								 32);
	grcRenderTarget*	renderTargetDepth	=grcTextureFactory::GetInstance().CreateRenderTarget("lexelviewDepth",
																								 grcrtDepthBuffer,
																								 prtProgressiveRadiosity::LEXEL_VIEW_WIDTH,
																								 prtProgressiveRadiosity::LEXEL_VIEW_HEIGHT,
																								 32);

	IDirect3DSurface9* surfaceDest=CreateOffscreenSurfaceStatic(prtProgressiveRadiosity::LEXEL_VIEW_WIDTH,prtProgressiveRadiosity::LEXEL_VIEW_HEIGHT);

//#define SAVE_IMAGE
#ifdef SAVE_IMAGE
	grcImage* image=grcImage::Create(prtProgressiveRadiosity::LEXEL_VIEW_WIDTH,prtProgressiveRadiosity::LEXEL_VIEW_HEIGHT,1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
#endif
	
	bool* addedTexels	=new bool[GetTexHeight()*GetTexWidth()];
	bool* blockStart	=new bool[GetTexHeight()*GetTexWidth()];

	int numPixelsInBlock=0;

	for (int y=0;y<GetTexHeight();y++)
	{
		PrintProgress(lastLen,totalTime,y);

		sysTimer timer;

		float resolveTime=0.0f;
		float averageTime=0.0f;
		float renderTime=0.0f;
		int xBlock=0,yBlock=0;
		for (int x=0;x<GetTexWidth();x++)
		{
			SetTexel(addedTexels,x,y,false);


			if (1)
			{
				bool alreadyInBlock=false;

				// check to see if are part of a block:
				if (x>0)
				{
					xBlock=x-1;
					yBlock=y;
					if (GetTexel(blockStart,xBlock,yBlock)==true)
						alreadyInBlock=true;
					else if (y>0)
					{
						yBlock=y-1;
						if (GetTexel(blockStart,xBlock,yBlock)==true)
							alreadyInBlock=true;
						else 
						{
							xBlock=x;
							if (GetTexel(blockStart,xBlock,yBlock)==true)
								alreadyInBlock=true;
						}
					}
				}
				
				// ...check if we can start a block:
				if (alreadyInBlock==false)
				{
					if (x>0 && y>0 && x<GetTexWidth()-2 && y<GetTexHeight()-2)
					{
						u32 currentValue=GetTexel(pixelsRead,x,y).GetColor();
						if (
							// left hand pixel line:
							GetTexel(pixelsRead,x-1,y).GetColor()==currentValue && 
							GetTexel(pixelsRead,x-1,y+1).GetColor()==currentValue && 
							
							// pixel line point is on:
							GetTexel(pixelsRead,x,y-1).GetColor()==currentValue &&
							GetTexel(pixelsRead,x,y+1).GetColor()==currentValue &&
							GetTexel(pixelsRead,x,y+2).GetColor()==currentValue &&

							// right hand pixel line:
							GetTexel(pixelsRead,x+1,y-1).GetColor()==currentValue &&
							GetTexel(pixelsRead,x+1,y).GetColor()==currentValue &&
							GetTexel(pixelsRead,x+1,y+1).GetColor()==currentValue &&
							GetTexel(pixelsRead,x+1,y+2).GetColor()==currentValue &&

							// right hand pixel line + 1:
							GetTexel(pixelsRead,x+2,y).GetColor()==currentValue && 
							GetTexel(pixelsRead,x+2,y+1).GetColor()==currentValue
							)
						{
							SetTexel(blockStart,x,y,true);
						}
					}
					else
						SetTexel(blockStart,x,y,false);
					
				}
				// pixel is part of a block, just use the color of the starting pixel (upper left corner pixel):
				else
				{
					SetTexel(blockStart,x,y,false);
					SetTexel(pixelsRadiosity,x,y,GetTexel(pixelsRadiosity,xBlock,yBlock));
					numPixelsInBlock++;
					continue;
				}
			}

			// get the position and render the scene from the point's point of view:
			if (pltLightMapDirect::GetPosAndNormalFromIndexMap(vertices,indices,pixelsRead,x,y,m_CurrentTexelPosition,m_CurrentTexelNormal))
			{
				sysTimer renderTimer;

				// lock our render target:
				grcTextureFactory::GetInstance().LockRenderTarget(0, renderTarget, renderTargetDepth);
				
				// clear render target:
				//Color32 clearColor(0xff000000);
				//Color32 clearColor(0xff7f7f7f);
				//Color32 clearColor(0xff000000);
				Color32 clearColor(0xff0000ff);
				GRCDEVICE.Clear(true,clearColor,true,grcDepthFarthest,0);

				grcViewport::SetCurrent(&m_Viewport);
				grcLightState::SetEnabled(false);
			
#ifdef CRAP
				Displayf("{{%d,%d},{%ff,%ff,%ff},{%ff,%ff,%ff}},",x,y,
					m_CurrentTexelPosition.x,m_CurrentTexelPosition.y,m_CurrentTexelPosition.z,
					m_CurrentTexelNormal.x,m_CurrentTexelNormal.y,m_CurrentTexelNormal.z);
#endif

				RenderToTexture();
		
				// we're done rendering into the render target:
				grcTextureFactory::GetInstance().UnlockRenderTarget(0);

				renderTime+=renderTimer.GetMsTime();

				// copy the render target back:
				IDirect3DSurface9* surfaceSource=NULL;
				
				sysTimer resolveTimer;
				static_cast<IDirect3DTexture9*>(renderTarget->GetTexturePtr())->GetSurfaceLevel(0,&surfaceSource);
				GRCDEVICE.GetCurrent()->GetRenderTargetData(surfaceSource, surfaceDest);
		
				Vector4 averageColor(0.0f,0.0f,0.0f,1.0f);

				Color32* colors=LockImageStatic(*surfaceDest);

				resolveTime+=resolveTimer.GetMsTime();

				Assert(colors);
	
				sysTimer timerAvg;
				// average the color:
				float average=0.0f;
				for (int lexelY=0;lexelY<prtProgressiveRadiosity::LEXEL_VIEW_HEIGHT;lexelY++)
					for (int lexelX=0;lexelX<prtProgressiveRadiosity::LEXEL_VIEW_WIDTH;lexelX++)
					{
						Color32 color=GetTexel(colors,lexelX,lexelY,prtProgressiveRadiosity::LEXEL_VIEW_WIDTH);
						float weight=GetTexel(modulationMask,lexelX,lexelY,prtProgressiveRadiosity::LEXEL_VIEW_WIDTH);
#ifdef SAVE_IMAGE
						image->SetPixel(lexelX,lexelY,color.GetColor());
#endif
						averageColor.x+=weight*color.GetRed()/255.0f;
						averageColor.y+=weight*color.GetGreen()/255.0f;
						averageColor.z+=weight*color.GetBlue()/255.0f;
						average+=weight;
					}
				averageTime+=timerAvg.GetMsTime();

		
		
				UnlockImageStatic(*surfaceDest);

				// compute the average:
				average=1.0f/average;
				averageColor.Multiply(Vector4(average,average,average,1.0f));

				Color32 color=GetTexel(pixelsDirect,x,y);

				Color32 average32(averageColor);

#ifdef SAVE_IMAGE
				if ((average32.GetColor()&0x00ffffff)!=0x0)
				{
					char fileName[256];
					sprintf(fileName,"d:\\lightmaps\\%dx%d",x,y);
					image->SaveDDS(fileName);
				}
#endif

				int red		=Clamp(color.GetRed()+(int)(averageColor.x*255),0,255);
				int green	=Clamp(color.GetGreen()+(int)(averageColor.y*255),0,255);
				int blue	=Clamp(color.GetBlue()+(int)(averageColor.z*255),0,255);

				color.Set(red,green,blue);

				

				SetTexel(pixelsRadiosity,x,y,color);
			}
			else
				SetTexel(pixelsRadiosity,x,y,(Color32)ILLEGAL_TEXEL_COLOR);
		}

		//Displayf("resolve time: %f average time: %f render time: %f percentage block pixels: %f",resolveTime,averageTime,renderTime,
		//			((float)numPixelsInBlock)/(float)((GetTexWidth()+1)*(y+1))*100.0f);
		
		totalTime+=timer.GetTime();
	}

	// add smeared borders are the texture charts:
	AddSmearedBorder(addedTexels,pixelsRead,pixelsRadiosity,2.0f);

	delete [] addedTexels;
	delete [] blockStart;

	grcViewport::SetCurrent(NULL);

	UnlockImage();
	m_LightMapDirect->UnlockImage();
	m_LightMapDirect->UnlockIndexMap();

	GetDxMesh().UnlockIndexBuffer();
	GetDxMesh().UnlockVertexBuffer();
}

void pltLightMapProgressiveRadiosity::RenderToTexture()
{
	Matrix34 matrix;

	// setup camera:
	matrix.Identity();
	matrix.LookDown(m_CurrentTexelNormal,YAXIS);
	matrix.d=m_CurrentTexelPosition;
	m_Viewport.SetCameraMtx(matrix);

	grcState::SetBlendSet(grcbsNormal);
	grcState::SetCullMode(grccmNone);
	grcState::SetDepthTest(true);
	grcState::SetAlphaBlend(true);
	grcState::SetAlphaTest(false);
	

	// render all geometries, normal pass:
	for (int i=0;i<m_Geometries.GetCount();i++)
	{
		m_Geometries[i]->Render();
	}

	// multiply luminance with the normal pass:
	grcState::SetBlendOp(grcboAdd);
	grcState::SetSrcBlendFactor(grcbfZero);
	grcState::SetDestBlendFactor(grcbfSrcAlpha);
	
	grcState::SetDepthTest(true);
	

	// render all geometries, radiosity luminance pass:
	grmModel::SetForceShader(sm_Shader);
	for (int i=0;i<m_Geometries.GetCount();i++)
	{
		m_Geometries[i]->Render();
	}
	grmModel::SetForceShader(NULL);
}

prtProgressiveRadiosity::prtProgressiveRadiosity(prtDirectLighting& direct) :
	m_DirectLighting(direct),
	m_ModulationMask(NULL)
{
	ComputeScreenModulationMask(NEAR_CLIP_PLANE,HORIZONTAL_FIELD_OF_VIEW,LEXEL_VIEW_WIDTH,LEXEL_VIEW_HEIGHT);
}

//                                                                                                                                    
//                                                                                                                                         
//                                                                                                                      
//                                    DISTANCE1						  DISTANCE2                   
//                                        |                               |      
//                                                                              
//                   .------------------=+##Xx=-----------------.  .,,,. .#  ,,,;                                      
//                  +#=-----------------.    ,;----------------=x-##---;,-.,-----==                                    
//                  -                      .                    ###------;-----;+##              
//              +, #-=++++++++++++++++++++++++++++++++++++++=-x####################   <---- SCREEN PLANE           
//            .#=  #                                         =+             .-,                                        
//            =#   #                                      -Xx           ;xx=                                           
//            -#   #                                    +#+         .=##=                                              
//            -#   #                                 .+x-        ,x##=                                                 
//            -#   #                               =###       ;x#X-                                                    
//            -#   #                            .+#+   ##  ;X#X-                                                       
//            -#   #                          ;X#+      ##Xx;                                                          
//           ;#   ###X#####X#=+            =#x.     -xx,.                                                             
//           ;#;   #      ^  x=#####x     x#+    xxX#=  ^                                                               
//  NEARCLIP ##    #      |       ,=###.X#;    =X+-     |                                                               
//        .   =#   #    ANGLE1       =#x   .+#X-      ANGLE2                                                               
//            -#   #               =x;  .x#x-                                                                          
//            -#   #            ,xx. .+#x,                                                                             
//            -#   #          -x+,;+X+.                                                                                
//            -#   #        =XX++x-                                                                                    
//            -#   #     .x##X+;                                                                                       
//            -#   #   -###+.                                                                                          
//            .#   # ###+                                                                                              
//             -#  ###                                                                                                 -                          
//                  <------------ CAMERA POSITION    
//
//
//			  	   (DISTANCE1+DISTANCE2)		 (DISTANCE1)
//	ANGLE2 = invtan(-------------------) - invtan(---------)
//				   (     NEARCLIP      )         (NEARCLIP )
//                
//  TEXEL_WEIGHT = ANGLE2 * cos(ANGLE1)
//
void prtProgressiveRadiosity::ComputeScreenModulationMask(float nearClip,float fov,int lightmapWidth,int lightmapHeight)
{
	// figure out the width of a texel find the length of the half 
	// of the horizontal screen width (using tan(theta)=opposite/adajenct):
	float halfWidthTexel=nearClip*tanf(fov*0.5f);

	float texelSizeInMeters=halfWidthTexel/(lightmapWidth*0.5f);

	delete [] m_ModulationMask;
	m_ModulationMask=new float[lightmapHeight*lightmapWidth];

	Vector2 origin(0.0f,0.0f);

	// loop over each pixel, setting the modulation mask:
	float halfWidth=lightmapWidth*0.5f;
	float halfHeight=lightmapHeight*0.5f;
	float minWeight=0.0f,maxWeight=0.0f;
	for (int x=0;x<lightmapWidth;x++)
		for (int y=0;y<lightmapHeight;y++)
		{
			float distInMeters=Vector2(x-halfWidth,y-halfHeight).Dist(origin)*texelSizeInMeters;

			float angle1=atanf(distInMeters/nearClip);
			float angle2=atanf((distInMeters+texelSizeInMeters)/nearClip) - angle1;
			float weight=angle2*cos(angle1);

			uvaUvAtlasHelper::SetTexel(m_ModulationMask,x,y,lightmapWidth,angle2*cos(angle1));

			// find limits for normalization:
			if (x==0 && y==0)
			{
				minWeight=weight;
				maxWeight=weight;
			}
			else
			{
				if (weight<minWeight)
					minWeight=weight;
				if (weight>maxWeight)
					maxWeight=weight;
			}
		}	

//#define SAVE_MODULATION_MASK_IMAGE
#ifdef SAVE_MODULATION_MASK_IMAGE
	grcImage* image=grcImage::Create(lightmapWidth,lightmapHeight,1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
#endif

	// normalize the image:
	float scaleFactor=maxWeight-minWeight;
	for (int x=0;x<lightmapWidth;x++)
		for (int y=0;y<lightmapHeight;y++)
		{
			float newWeight=(uvaUvAtlasHelper::GetTexel(m_ModulationMask,x,y,lightmapWidth)-minWeight)/scaleFactor;
			uvaUvAtlasHelper::SetTexel(m_ModulationMask,x,y,lightmapWidth,newWeight);
#ifdef SAVE_MODULATION_MASK_IMAGE
			image->SetPixel(x,y,Color32(newWeight,newWeight,newWeight).GetColor());
#endif
		}
		
#ifdef SAVE_MODULATION_MASK_IMAGE
	image->SaveDDS("c:\\modulation_mask.dds");
	image->Release();
#endif
}

void prtProgressiveRadiosity::Iterate(int numIterations)
{
	atArray<prtGeometry*>& geometries=m_DirectLighting.GetGeometries();
	for (int i=0;i<numIterations;i++)
	{
		// reload lightmaps:
		if (i!=0)
			for (int j=0;j<geometries.GetCount();j++)
				geometries[j]->LoadProgressiveRadiosityLightMaps("lightmap_01");

		// compute the light maps:
		for (int j=0;j<geometries.GetCount();j++)
			geometries[j]->ComputeProgressiveRadiosityLightMaps(geometries,m_ModulationMask);

		// save them out:
		for (int j=0;j<geometries.GetCount();j++)
			geometries[j]->SaveProgressiveRadiosityLightMaps("lightmap_01");
	}
}

}; // namespace rage
