//
// prttools/config.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "config.h"

namespace rage {

bool prtSimulatorOptions::Reset()
{
	// ZeroMemory( &options, sizeof(prtSimulatorOptions) );

    m_Order			= 2;			// don't need high order yet
    m_NumRays		= 512;		
	m_NumBounces	= 1;
    m_LengthScale	= 0.03f;//1.0f;		// very important ( was 1000.0f ) 
    m_NumChannels	= 1;			// ray just use one

    m_EnableCompression	= false;
    m_Quality			= D3DXSHCQUAL_SLOWHIGHQUALITY;
    m_NumClusters		= 1;
    m_NumPCA			= 24;

    m_EnableTessellation			= false;  // don't bother with this yet either
    m_RobustMeshRefine				= true;
    m_RobustMeshRefineMinEdgeLength = 0.0f;
    m_RobustMeshRefineMaxSubdiv		= 1;
    m_AdaptiveDL					= true;
    m_AdaptiveDLMinEdgeLength		= 0.03f;
    m_AdaptiveDLThreshold			= 8e-5f;
    m_AdaptiveDLMaxSubdiv			= 1;
    m_AdaptiveBounce				= false;
    m_AdaptiveBounceMinEdgeLength	= 0.03f;
    m_AdaptiveBounceThreshold		= 8e-5f;
    m_AdaptiveBounceMaxSubdiv		= 1;

    m_OutputConcatPRTMesh			= "meshConcat.mesh";
    m_OutputConcatBlockerMesh		= "blockerConcat.x";
    m_OutputTessellatedMesh			= "tesslatedMesh.mesh";
    m_OutputPRTBuffer				= "prtbuffer.prt";
    m_OutputCompPRTBuffer			= "prtCompBuffer.pca";   
    m_BinaryXFile					= false;

	m_outputAmbientOcclusion = true;

	m_InputMeshes.Resize(1);
    for( int iMesh=0; iMesh<m_InputMeshes.GetCount(); iMesh++ )
    {
        m_InputMeshes[iMesh].m_FileName			= "PRT Demo\\LandShark.x";
        m_InputMeshes[iMesh].m_IsBlockerMesh	= 0;
        m_InputMeshes[iMesh].m_Translate.Set(0,0,0);
        m_InputMeshes[iMesh].m_Scale.Set(1,1,1);
        m_InputMeshes[iMesh].m_Yaw				= 0.0f;
        m_InputMeshes[iMesh].m_Pitch			= 0.0f;
        m_InputMeshes[iMesh].m_Roll				= 0.0f;

        m_InputMeshes[iMesh].m_SHMaterials.Resize(1);
		D3DCOLORVALUE& diffuse=m_InputMeshes[iMesh].m_SHMaterials[0].Diffuse;
		diffuse.r = diffuse.g = diffuse.b = diffuse.a = 1.0f;
		D3DCOLORVALUE& absorption=m_InputMeshes[iMesh].m_SHMaterials[0].Absorption;
		absorption.r = 0.0030f; 
		absorption.g = 0.0030f;
		absorption.b = 0.0460f;
		absorption.a = 1.0f;
        m_InputMeshes[iMesh].m_SHMaterials[0].bSubSurf = false;
        m_InputMeshes[iMesh].m_SHMaterials[0].RelativeIndexOfRefraction = 1.3f;
		D3DCOLORVALUE& reducedScattering=m_InputMeshes[iMesh].m_SHMaterials[0].ReducedScattering;
		reducedScattering.r = 2.0f; 
		reducedScattering.g = 2.0f;
		reducedScattering.b = 2.0f;
		reducedScattering.a = 1.0f;
    }

    return true;
}

} // namespace rage
