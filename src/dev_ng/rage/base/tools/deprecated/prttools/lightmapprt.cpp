// 
// prttools/lightmap.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "lightmapprt.h"

#include "grcore/im.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grcore/viewport.h"

namespace rage {
pltLightMapPrt::pltLightMapPrt(prtSimulator& simulator,int width,int height) : pltLightMap(width,height),m_Simulator(simulator)
{
}

void pltLightMapPrt::RenderToTexture()
{
	if (m_Simulator.GetPerPixel())
	{
		RenderLightMap();
	}
	else
	{
		RenderTriangles();
	}
}

template<class __Type> class ImageArray
{
public:
	ImageArray(const void* array,int width)
	{
		Assert(array);
		m_Array=(__Type*)array;
		m_Width=width;
	}

	__Type Get(int x,int y) const
	{
		return m_Array[x+(y*m_Width)];
	}

	void Set(int x,int y,__Type value)
	{
		m_Array[x+(y*m_Width)]=value;
	}

private:
	const __Type*	m_Array;
	int				m_Width;
};

void pltLightMapPrt::RenderLightMap()
{
	const UINT*					clusterIDs		=m_Simulator.GetClusterIDs();
	const LPDIRECT3DTEXTURE9*	textures		=m_Simulator.GetPrtTextures();
	float*						coefficients	=(float*)Alloca(Vector4,m_Simulator.GetNumPcas()/4);
	D3DLOCKED_RECT*				rects			=Alloca(D3DLOCKED_RECT,m_Simulator.GetNumPcas());
	IDirect3DSurface9**			surfaces		=Alloca(IDirect3DSurface9*,m_Simulator.GetNumPcas());


	ImageArray<UINT> clusterIDArray(clusterIDs,GetTexWidth());

	// lock texture images:
	for (UINT i=0;i<m_Simulator.GetNumPcas();i++)
	{
		surfaces[i]=NULL;
		textures[i]->GetSurfaceLevel(0,&surfaces[i]);
		surfaces[i]->LockRect(&rects[i], NULL, D3DLOCK_READONLY);
	}

	// lock destination texture:
	if (m_TextureImage)
		m_TextureImage->Release();
	m_TextureImage=grcImage::Create(GetTexWidth(),GetTexHeight(),1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
	
	// loop over each pixel in the map:
	for (int x=0;x<GetTexWidth();x++)
	{
		for (int y=0;y<GetTexHeight();y++)
		{
			UINT clusterID=clusterIDArray.Get(x,y);
			for (UINT i=0;i<m_Simulator.GetNumPcas();i++)
			{
				ImageArray<float> imageArray(rects[i].pBits,GetTexWidth());
				coefficients[i]=imageArray.Get(x,y);
			}

			// compute and store color:
			Color32 color(m_Simulator.GetPRTDiffuse(clusterID,(Vector4*)coefficients));
			m_TextureImage->SetPixel(x,y,color.GetColor());
		}
	}
	
	// unlock surfaces:
	for (UINT i=0;i<m_Simulator.GetNumPcas();i++)
	{
		surfaces[i]->UnlockRect();
	}
}

void pltLightMapPrt::RenderTriangles()
{
	// create our array to be sorted:
	m_FaceInfos.Resize(GetDxMesh().GetNumFaces());
	for (int i=0;i<m_FaceInfos.GetCount();i++)
	{
		m_FaceInfos[i].m_FaceIndex=i;
		m_FaceInfos[i].m_ParentFaceIndex=m_Simulator.GetConcatMesh().m_FaceRemaps[i];
	}

	// sort the array so that all parent faces are close together:
	m_FaceInfos.QSort(0,m_FaceInfos.GetCount(),FaceInfo::QSortCompare);

	u32 numFacesOriginal=GetDxMesh().GetNumFaces();
	int newFaceIndex=0;
	Vector4 colors[3];

	// compute maps:
	u32* indices=NULL;
	GetDxMesh().LockIndexBuffer(0x0,(LPVOID*)&indices);

	uvaUvAtlasHelper::Vertex* vertices=NULL;
	GetDxMesh().LockVertexBuffer(0x0,(LPVOID*)&vertices);

	uvaUvAtlasHelper::Vertex* verticesOriginal=NULL;
	GetOriginalDxMesh().LockVertexBuffer(0x0,(LPVOID*)&verticesOriginal);

	int texWidth=GetTexWidth();
	int texHeight=GetTexHeight();

	Displayf("Populate Lightmaps...");
	for (u32 i=0;i<numFacesOriginal && newFaceIndex<m_FaceInfos.GetCount();i++)
	{
		u32 parentIndex=m_FaceInfos[newFaceIndex].m_ParentFaceIndex;
		// skip until we've found the matching polygon:
		if (parentIndex>i)
			continue;

		while (newFaceIndex<m_FaceInfos.GetCount() && parentIndex==i)
		{
			// get face indices:
			int faceIndex=m_FaceInfos[newFaceIndex].m_FaceIndex;
			int index1=indices[faceIndex*3+0];
			int index2=indices[faceIndex*3+1];
			int index3=indices[faceIndex*3+2];

			// find the triangle in UV space:
			Vector2 weightedVerts[3];
			UINT* remaps=m_Simulator.GetConcatMesh().m_VertRemaps;
			weightedVerts[0]=LerpVert2D(verticesOriginal[remaps[index1*3+0]].m_Texcoord2,
										verticesOriginal[remaps[index1*3+1]].m_Texcoord2,
										verticesOriginal[remaps[index1*3+2]].m_Texcoord2,
										&m_Simulator.GetConcatMesh().m_VertWeights[index1*3]);
			weightedVerts[1]=LerpVert2D(verticesOriginal[remaps[index2*3+0]].m_Texcoord2,
										verticesOriginal[remaps[index2*3+1]].m_Texcoord2,
										verticesOriginal[remaps[index2*3+2]].m_Texcoord2,
										&m_Simulator.GetConcatMesh().m_VertWeights[index2*3]);
			weightedVerts[2]=LerpVert2D(verticesOriginal[remaps[index3*3+0]].m_Texcoord2,
										verticesOriginal[remaps[index3*3+1]].m_Texcoord2,
										verticesOriginal[remaps[index3*3+2]].m_Texcoord2,
										&m_Simulator.GetConcatMesh().m_VertWeights[index3*3]);

			// compute color of position:
			colors[0]=m_Simulator.GetPRTDiffuse(vertices[index1].m_BlendWeight0,vertices[index1].m_BlendWeight);
			colors[1]=m_Simulator.GetPRTDiffuse(vertices[index2].m_BlendWeight0,vertices[index2].m_BlendWeight);
			colors[2]=m_Simulator.GetPRTDiffuse(vertices[index3].m_BlendWeight0,vertices[index3].m_BlendWeight);

			
			for (int i=0;i<3;i++)
			{
				// clamp it for now:
				colors[i].x=Clamp(colors[i].x,0.0f,1.0f);
				colors[i].y=Clamp(colors[i].y,0.0f,1.0f);
				colors[i].z=Clamp(colors[i].z,0.0f,1.0f);
				colors[i].w=1.0f;
			}

			// render the triangle to the lightmap render target:
			grcState::SetCullMode(grccmNone);
			grcBegin(drawTris,3);
			grcColor(colors[0]);
			grcVertex2f(weightedVerts[0].x*(float)texWidth,weightedVerts[0].y*(float)texHeight);
			grcColor(colors[1]);
			grcVertex2f(weightedVerts[1].x*(float)texWidth,weightedVerts[1].y*(float)texHeight);
			grcColor(colors[2]);
			grcVertex2f(weightedVerts[2].x*(float)texWidth,weightedVerts[2].y*(float)texHeight);
			grcEnd();

			newFaceIndex++;
		}

	}

	// unlock buffers:
	GetOriginalDxMesh().UnlockVertexBuffer();
	GetDxMesh().UnlockVertexBuffer();
	GetDxMesh().UnlockIndexBuffer();
}

} // namespace rage
