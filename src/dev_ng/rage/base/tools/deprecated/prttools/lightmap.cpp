// 
// prttools/lightmap.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "lightmap.h"

#include "convert.h"

namespace rage {

void prtNode::Insert(LightMapInfo& lightMap)
{
	static int numNotFitted=0;

	prtNode* theNode=InsertInternal(lightMap);
	if (theNode)
	{
		theNode->m_LightMap=&lightMap;
		lightMap.m_Node=theNode;
	}
#ifdef CRAP
	else 
	{
		Errorf("Couldn't fit image %dx%d (total: %d)",image.GetWidth(),image.GetHeight(),++numNotFitted);
	}
#endif
}

// general idea is from: http://www.blackpawn.com/texts/lightmaps/default.html
prtNode* prtNode::InsertInternal(LightMapInfo& lightMap)
{
	if (m_Children[0] || m_Children[1])
	{
		// try inserting into first child:
		prtNode* newNode=m_Children[0]->InsertInternal(lightMap);
		if (newNode!=NULL)
			return newNode;

		// no room, insert into second child:
		return m_Children[1]->InsertInternal(lightMap);
	}

	// if there's already a lightmap here, return:
	if (m_LightMap!=NULL)
		return NULL;

	// if we're too small return:
	if (m_Rect.IsSizeSmaller(lightMap.m_Rect))
	{
		if (m_Rect.IsSizeSmallerRotated(lightMap.m_Rect) || 1)
			return NULL;
		else 
			lightMap.Rotate();
	}

	// if we're a perfect fit, accept:
	if (m_Rect.IsSizeEqual(lightMap.m_Rect))
	{
		return this;
	}
	else if (m_Rect.IsSizeEqualRotated(lightMap.m_Rect) && 0)
	{
		lightMap.Rotate();
		return this;
	}

	// otherwise, we split this node and create shome hids:
	m_Children[0] = new prtNode;
	m_Children[1] = new prtNode;

	u32 height = lightMap.m_Rect.m_Height;
	u32 width = lightMap.m_Rect.m_Width;

	s32 diffHeight = m_Rect.m_Height - lightMap.m_Rect.m_Height; 
	s32 diffWidth = m_Rect.m_Width - lightMap.m_Rect.m_Width;

	// height remains the same...
	if (diffWidth > diffHeight)
	{ 
		m_Children[0]->m_Rect.Set(m_Rect.m_Left,       m_Rect.m_Top, width,   m_Rect.m_Height);
		m_Children[1]->m_Rect.Set(m_Rect.m_Left+width, m_Rect.m_Top, m_Rect.m_Width-width,  m_Rect.m_Height);
	}
	// ... or width remains the same:
	else
	{ 
		m_Children[0]->m_Rect.Set(m_Rect.m_Left, m_Rect.m_Top,        m_Rect.m_Width, height);
		m_Children[1]->m_Rect.Set(m_Rect.m_Left, m_Rect.m_Top+height, m_Rect.m_Width, m_Rect.m_Height-height);
	}


	return m_Children[0]->InsertInternal(lightMap);
}

pltLightMap::pltLightMap(int width,int height) : uvaUvAtlasHelper(width,height)
{
	m_RootNode=new prtNode();
	m_RootNode->m_Rect.Set(0,0,width,height);
}

void pltLightMap::Init(float stretch,float gutter)
{
	ComputeLightMapsUVs();

	DWORD* adjacency=new DWORD[GetDxMesh().GetNumFaces()*3];
	//float adjacencyEpsilon=1e-6f;
	float adjacencyEpsilon=0.001f;

	HRESULT hr = GetDxMesh().ConvertPointRepsToAdjacency(NULL, adjacency );
	if (FAILED(hr))
	{
		Warningf( "ConvertPointRepsToAdjacency() failed: !\n");
	}

	//GetDxMesh().GenerateAdjacency(adjacencyEpsilon,adjacency);
	uvaUvAtlasHelper::InitAtlas(GetDxMesh(),adjacencyEpsilon,adjacency);

	// create the new mesh:
	int width=m_RootNode->m_Rect.GetWidth();
	int height=m_RootNode->m_Rect.GetHeight();

	//ID3DXMesh* mesh=uvaUvAtlasHelper::Create(0.40f,width,height,gutter,1);
	ID3DXMesh* mesh=uvaUvAtlasHelper::Create(stretch,width,height,gutter,1);
	Assert(mesh);
	SetDxMesh(*mesh);

	// darn it, the uv atlas create function removes the attribute tables:
	mesh=GetConvert().SortByAttribute(GetDxMesh());
	SetDxMesh(*mesh);

	Assert(mesh);

	delete adjacency;

}

void pltLightMap::Insert(LightMapInfo& lightMap)
{
	m_RootNode->Insert(lightMap);
}


void pltLightMap::CalcLightMapCoords(const uvaUvAtlasHelper::Vertex& vert1,
									const uvaUvAtlasHelper::Vertex& vert2,
									const uvaUvAtlasHelper::Vertex& vert3,
									int& outTexcoordIndexX,
									int& outTexcoordIndexY)
{
	// compute face normal:
	Vector3 faceNormal;
	Vector3 edge12(vert1.m_Position.x-vert2.m_Position.x,vert1.m_Position.y-vert2.m_Position.y,vert1.m_Position.z-vert2.m_Position.z);
	Vector3 edge13(vert1.m_Position.x-vert3.m_Position.x,vert1.m_Position.y-vert3.m_Position.y,vert1.m_Position.z-vert3.m_Position.z);
	faceNormal.Cross(edge12,edge13);

	// Calculate the |Normal| of the polygon, using
	// the polygon's normal (this does an ABS() on each
	// element of the normal)
	Vector3 absNorm(fabsf(faceNormal.x),fabsf(faceNormal.y),fabsf(faceNormal.z));    // Does the polygon primarily face along the X-axis?
	if (absNorm.x >= absNorm.y && absNorm.x >= absNorm.z)
	{
		outTexcoordIndexX=2; // z
		outTexcoordIndexY=1; // y
	}    
	// Does the polygon primarily face along the Y-axis?
	else if (absNorm.y >= absNorm.x && absNorm.y >= absNorm.z)
	{
		outTexcoordIndexX=0; // x
		outTexcoordIndexY=2; // z
	}    
	// The polygon primarily face along the Z-axis
	else
	{
		outTexcoordIndexX=0; // x
		outTexcoordIndexY=1; // y
	}
}

Vector2 pltLightMap::ComputeMapSizeForPolygon(const uvaUvAtlasHelper::Vertex& vert1,
											  const uvaUvAtlasHelper::Vertex& vert2,
											  const uvaUvAtlasHelper::Vertex& vert3,
											  int texcoordIndexX,
											  int texcoordIndexY,
											  float density,
											  Vector2* outUVs,
											  Vector2& outAdjustUV)
{
	outUVs[0].x= vert1.m_Position[texcoordIndexX]*density;
	outUVs[0].y=-vert1.m_Position[texcoordIndexY]*density;

	outUVs[1].x= vert2.m_Position[texcoordIndexX]*density;
	outUVs[1].y=-vert2.m_Position[texcoordIndexY]*density;

	outUVs[2].x= vert3.m_Position[texcoordIndexX]*density;
	outUVs[2].y=-vert3.m_Position[texcoordIndexY]*density;

	// normalize position to 0,0:
	outAdjustUV.x=floorf(Min(outUVs[0].x,outUVs[1].x,outUVs[2].x));
	outAdjustUV.y=floorf(Min(outUVs[0].y,outUVs[1].y,outUVs[2].y));

	for (int i=0;i<3;i++)
	{
		outUVs[i].x-=outAdjustUV.x;
		outUVs[i].y-=outAdjustUV.y;
	}

	return Vector2(Max(outUVs[0].x,outUVs[1].x,outUVs[2].x),Max(outUVs[0].y,outUVs[1].y,outUVs[2].y));
}

Vector2 pltLightMap::LerpVert2D(const Vector2& vert1,const Vector2& vert2,const Vector2& vert3,float weights[])
{
	Vector2 lerpedVert;

	lerpedVert.x=(vert1.x*weights[0]) + (vert2.x*weights[1]) + (vert3.x*weights[2]);
	lerpedVert.y=(vert1.y*weights[0]) + (vert2.y*weights[1]) + (vert3.y*weights[2]);

	return lerpedVert;
}

void pltLightMap::ComputeLightMapsUVs()
{
	ID3DXMesh& mesh=GetDxMesh();

	// find the largest dimension for a polygon:
	int masterLightmapArea=0;

	const float TEXELS_PER_METER_DENSITY=30.0f;

	int maxWidth=0,maxHeight=0;

	m_LightMaps.Resize(mesh.GetNumFaces());

	atArray<AdjustFaceInfo,unsigned int,UINT_MAX> uvAdjust;
	uvAdjust.Resize(mesh.GetNumFaces());

	Displayf("Computing lightmap sizes...");
	int lightMapTextureWidth=m_RootNode->m_Rect.GetWidth();
	int lightMapTextureHeight=m_RootNode->m_Rect.GetHeight();

	// get face indices:
	u32* indices;
	mesh.LockIndexBuffer(0x0,(LPVOID*)&indices);

	// get face vertices:
	uvaUvAtlasHelper::Vertex* vertices;
	mesh.LockVertexBuffer(0x0,(LPVOID*)&vertices);

	for (int face=0;face<(int)mesh.GetNumFaces();face++)
	{
		int index1=indices[face*3+0];
		int index2=indices[face*3+1];
		int index3=indices[face*3+2];
	
		CalcLightMapCoords(vertices[index1],vertices[index2],vertices[index3],uvAdjust[face].m_VertIndices[0],uvAdjust[face].m_VertIndices[1]);
		Vector2 maxUV=ComputeMapSizeForPolygon(vertices[index1],
			vertices[index2],
			vertices[index3],
			uvAdjust[face].m_VertIndices[0],
			uvAdjust[face].m_VertIndices[1],
			TEXELS_PER_METER_DENSITY,
			m_LightMaps[face].m_UVs,
			uvAdjust[face].m_Adjust);

		// *2 since we have a border on the left, right, top, and bottom sides:
		maxUV.x+=pltLightMap::GetBorderThickness()*2;
		maxUV.y+=pltLightMap::GetBorderThickness()*2;

		m_LightMaps[face].m_Rect.Set(0,0,(int)ceilf(maxUV.x),(int)ceilf(maxUV.y));
		maxWidth=Max(m_LightMaps[face].m_Rect.GetWidth(),maxWidth);
		maxHeight=Max(m_LightMaps[face].m_Rect.GetHeight(),maxHeight);
		m_LightMaps[face].m_Index=face;
		masterLightmapArea+=m_LightMaps[face].m_Rect.GetArea();
	}


	Displayf("Combine lightmaps...");
	// fit maps into bigger map:
	for (int i=0;i<m_LightMaps.GetCount();i++)
	{
		m_RootNode->Insert(m_LightMaps[i]);
	}

	// compute and set texture coordinates:
	for (int face=0;face<(int)mesh.GetNumFaces();face++)
	{
		const LightMapInfo& info=m_LightMaps[face];
		vertices[indices[face*3+0]].m_Texcoord2=info.GetPackedUV(0,lightMapTextureWidth,lightMapTextureHeight);
		vertices[indices[face*3+1]].m_Texcoord2=info.GetPackedUV(1,lightMapTextureWidth,lightMapTextureHeight);
		vertices[indices[face*3+2]].m_Texcoord2=info.GetPackedUV(2,lightMapTextureWidth,lightMapTextureHeight);

		// subtract half pixel offset due to texture addressing rules:
		for (int i=0;i<3;i++)
		{
			vertices[indices[face*3+i]].m_Texcoord2.x-=0.5f/lightMapTextureWidth;
			vertices[indices[face*3+i]].m_Texcoord2.y-=0.5f/lightMapTextureHeight;
		}

	}
	

	mesh.UnlockIndexBuffer();
	mesh.UnlockVertexBuffer();
}

} // namespace rage
