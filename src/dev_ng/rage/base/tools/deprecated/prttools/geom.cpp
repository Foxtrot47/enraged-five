// 
// prttools/geom.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "geom.h"

#include "ambientocclusion.h"
#include "convert.h"
#include "direct.h"
#include "progressrad.h"

#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "grcore/texture.h"
#include "rmcore/drawable.h"
#include "mesh/mesh.h"
#include "phbound/boundgeom.h"

#include "mesh/serialize.h"

namespace rage {

prtGeometry::prtGeometry() :
	m_Converter(NULL),
	m_BoundGeom(NULL),
	m_Skeleton(NULL),
	m_Drawable(NULL)
{
	
	AddRef();
}

void prtGeometry::InitShaderSystems(const char* shaderRoot)
{
	char libPath[256];
	if (shaderRoot==NULL)
	{
		Quitf("shader root not found (use -shaderpath)");
	}

	sprintf(libPath,"%s/%s",shaderRoot,"tune/shaders/lib");
	grmShaderFactory::GetInstance().PreloadShaders(libPath);

	// Init the shader database
	s32 maxPresets = 1024;
	sprintf(libPath,"%s/%s",shaderRoot,"tune/shaders/db");
	grcMaterialLibrary::Push(grcMaterialLibrary::Preload(libPath));
}



bool prtGeometry::MeshHandler(mshMesh& mesh,const char* name)
{

	// add the mesh to our list:
	m_Meshes.Grow()			=&mesh;
	m_MeshNames.Grow()		=name;
	m_MaterialNames.Grow();
	m_DxMeshes.Grow()		=m_Converter->CreateD3DXMesh(mesh,m_MaterialNames[m_MaterialNames.GetCount()-1]);

	return false; // tell client that we don't want them to delete the mesh!!!
}

bool prtGeometry::Init(prtConvert& convert,const char* inputPath,prtDirectLighting& direct,int width,int height)
{
	m_Converter			=&convert;
	m_DirectLighting	=&direct;
	m_LightMapWidth		=width;
	m_LightMapHeight	=height;
	m_DrawablePath		=inputPath;

	LoadDrawable();

	CreateBound();

	// currently just one lightmap per drawable:
	//int lightMapCount=m_Meshes.GetCount();
	int lightMapCount=1;

	// allocate light map data:
	m_LightMapsDirect.Reset();
	m_LightMapsDirect.Resize(lightMapCount);
	
	m_LightMapsAmbientOcclusion.Reset();
	m_LightMapsAmbientOcclusion.Resize(lightMapCount);

	m_LightMapsProgRadiosityLast.Reset();
	m_LightMapsProgRadiosityLast.Resize(lightMapCount);

	m_ImagesProgRadiosityCurrent.Reset();
	m_ImagesProgRadiosityCurrent.Resize(lightMapCount);

	for (int i=0;i<lightMapCount;i++)
	{
		pltLightMapDirect* directLightMap		=new pltLightMapDirect(direct,*m_Converter,*this,i,m_LightMapWidth,m_LightMapHeight);

		// make sure we initialize the direct light map with no strecth and a 1 pixel border:
		directLightMap->Init();
		
		m_LightMapsDirect[i]					=directLightMap;
		m_LightMapsAmbientOcclusion[i]			=new pltLightMapAmbientOcclusion(*m_LightMapsDirect[i],8);
		m_LightMapsProgRadiosityLast[i]			=new pltLightMapProgressiveRadiosity(*m_LightMapsDirect[i]);
		m_ImagesProgRadiosityCurrent[i]			=grcImage::Create(m_LightMapWidth,m_LightMapHeight,1,grcImage::A8R8G8B8,grcImage::STANDARD,0,0);
	}


	// initialize archetype:
	m_Archetype.SetBound(m_BoundGeom);

	Matrix34 mat;
	mat.Identity();
	m_PhysInst.Init(m_Archetype,mat);


	return true;
}

void prtGeometry::CreateBound()
{
	m_BoundGeom=new phBoundGeometry();

	// find the vert and triangle count:
	int numVertices=0;
	int numSubPrimitives=0;
	for (int i=0;i<m_Meshes.GetCount();i++)
	{
		for (int j=0;j<m_Meshes[i]->GetMtlCount();j++)
		{
			const mshMaterial& mtl=m_Meshes[i]->GetMtl(j);	
			numVertices+=mtl.GetVertexCount();	
			for (int k=0;k<mtl.Prim.GetCount();k++)
			{
				numSubPrimitives+=mtl.Prim[k].GetSubPrimitiveCount();
			}
		}
	}

	m_BoundGeom->Init(numVertices,1,numSubPrimitives);

#if __TOOL
	int vertCount=0;
	for (int i=0;i<m_Meshes.GetCount();i++)
	{
		// copy vertices to the bound geometry:
		for (int j=0;j<m_Meshes[i]->GetMtlCount();j++)
		{
			const mshMaterial& mtl=m_Meshes[i]->GetMtl(j);
			for (int k=0;k<mtl.Verts.GetCount();k++)
			{
				const mshVertex& vert=mtl.Verts[k];
				m_BoundGeom->SetVertex(vertCount++,vert.Pos);
			}
		}
	}

	int numPolysAdded=0;
	int vertexOffset=0;
	for (int i=0;i<m_Meshes.GetCount();i++)
	{
		// add polygons:
		for (int j=0;j<m_Meshes[i]->GetMtlCount();j++)
		{
			const mshMaterial& mtl=m_Meshes[i]->GetMtl(j);
			for (int k=0;k<mtl.Prim.GetCount();k++)
			{
				const mshPrimitive& prim = mtl.Prim[k];
				switch (prim.Type)
				{
				case mshTRIANGLES:
					for (int l=0;l<prim.Idx.GetCount();l+=3)
					{
						m_BoundGeom->GetPolygon(numPolysAdded++).InitTriangle(prim.Idx[l+0]+vertexOffset,
							prim.Idx[l+1]+vertexOffset,
							prim.Idx[l+2]+vertexOffset,
							m_BoundGeom->GetVertex(prim.Idx[l+0]+vertexOffset),
							m_BoundGeom->GetVertex(prim.Idx[l+1]+vertexOffset),
							m_BoundGeom->GetVertex(prim.Idx[l+2]+vertexOffset));
					}
					break;

				case mshTRISTRIP:
				case mshTRISTRIP2:
					for (int l=0;l<prim.Idx.GetCount()-2;l++)
					{
						int idx0=prim.Idx[l+0]+vertexOffset;
						int idx1=prim.Idx[l+1]+vertexOffset;
						int idx2=prim.Idx[l+2]+vertexOffset;
						if (idx0!=idx1 && idx0!=idx2 && idx1!=idx2)
						{
							if (prim.Type==mshTRISTRIP && l%2==0 || prim.Type==mshTRISTRIP2 && l%2==1)
								m_BoundGeom->GetPolygon(numPolysAdded++).InitTriangle(idx0,idx1,idx2,
								m_BoundGeom->GetVertex(idx0), m_BoundGeom->GetVertex(idx1), m_BoundGeom->GetVertex(idx2));
							else
								m_BoundGeom->GetPolygon(numPolysAdded++).InitTriangle(idx2,idx1,idx0,
								m_BoundGeom->GetVertex(idx0), m_BoundGeom->GetVertex(idx1), m_BoundGeom->GetVertex(idx2));
						}
					}
					break;

				case mshQUADS:
					for (int l=0;l<prim.Idx.GetCount();l+=4)
					{
						int idx0=prim.Idx[l+0]+vertexOffset;
						int idx1=prim.Idx[l+1]+vertexOffset;
						int idx2=prim.Idx[l+2]+vertexOffset;
						int idx3=prim.Idx[l+3]+vertexOffset;
						m_BoundGeom->GetPolygon(numPolysAdded++).InitQuad(idx0,idx1,idx2,idx3,
							m_BoundGeom->GetVertex(idx0), m_BoundGeom->GetVertex(idx1), m_BoundGeom->GetVertex(idx2), m_BoundGeom->GetVertex(idx3));
					}
					break;

				case mshPOLYGON:
				default:
					Quitf("primitive type '%d' not supported yet!",prim.Type);
					break;


				}
			}
			vertexOffset+=m_Meshes[i]->GetMtl(j).GetVertexCount();
		}
	}

	m_BoundGeom->DecreaseNumPolys(numPolysAdded);
	m_BoundGeom->CalculateGeomExtents();
#endif
}

bool prtGeometry::LoadDrawable()
{
	// add the mesh to our list:
	m_Meshes.Reset();
	m_MeshNames.Reset();
	m_MaterialNames.Reset();
	m_DxMeshes.Reset();

	if (m_Drawable)
		m_Drawable->Release();

	// load drawable:
	m_Drawable = new rmcDrawable();

	// capture mesh loading:
	atFunctor2<bool,mshMesh&,const char*> meshHandler;
	meshHandler.Reset<prtGeometry,&prtGeometry::MeshHandler>(this);
	grmModel::SetMeshLoaderHook(meshHandler);

	// load the drawable:
	if( !m_Drawable->Load(m_DrawablePath) )
	{
		Errorf( "Couldn't load file '%s'!", m_DrawablePath );
		return false;
	}

	// compute the light map full path:
	char filePath[512];
	ASSET.RemoveNameFromPath(filePath,256,m_DrawablePath);
	m_LightMapDirectory = filePath;
	char directoryName[512];
	ASSET.RemoveNameFromPath(directoryName,256,filePath);
	m_LightMapFullPath = m_LightMapDirectory;
	m_LightMapFullPath += "\\__lightmap_";
	m_LightMapFullPath += directoryName;

	// get the shader index of the lightmap texture:
	m_LightMapTextureIndex=m_Drawable->GetShaderGroup().LookupVar("LightMapTex1");
	Assert(m_LightMapTextureIndex!=grmsgvNONE);
	
	// load skeleton:
	delete m_Skeleton;

	if (m_Drawable->GetSkeletonData())
	{
		int numBones = m_Drawable->GetSkeletonData()->GetNumBones();
		m_Skeleton = new crSkeleton();
		m_Skeleton->Init(*m_Drawable->GetSkeletonData(),NULL);
	}
	else 
		m_Skeleton=NULL;

	grmModel::RemoveMeshLoaderHook();

	// do this once since the drawable should never move:
	if ( m_Skeleton ) {
		m_Skeleton->Update();
	}

	return true;
}

void prtGeometry::OutputMeshes(const char* /*fileNamePostfix*/)
{
	for (int i=0;i<m_Meshes.GetCount();i++)
	{
		m_Converter->OutputMesh(m_MeshNames[i],*m_DxMeshes[i],m_MaterialNames[i]);
	}
}

void prtGeometry::Render(bool alwaysRender)
{
	enum {NUM_BUCKETS=4};

	u8 lod=0;
	Matrix34 center;
	center.Identity();

	if (alwaysRender || m_Drawable->IsVisible(center,*grcViewport::GetCurrent(),lod)) {
		//lod = LOD_VLOW; // force lod to be low so rendering is faster
		for (int bucket=0; bucket<NUM_BUCKETS; bucket++)
			if (m_Drawable->GetBucketMask(lod) & (1 << bucket)) {
				if (m_Drawable->GetSkeletonData())
					m_Drawable->DrawSkinned(*m_Skeleton->GetParentMtx(),*m_Skeleton,bucket,lod);
				else
					m_Drawable->Draw(center,bucket,lod);
			}
	}
}

void prtGeometry::ReloadLightMap(const char* fileName)
{
	m_Drawable->GetShaderGroup().SetVar(m_LightMapTextureIndex,NULL);

	// disconnect the shader dependency on the texture:
	//const char* fileName=ASSET.FileName(m_LightMapFullPath);
	grcTextureFactory::GetInstance().DeleteTextureReference(fileName);
	
	// load the texture and tell the shader about it:
	//atString prevPath=ASSET.GetPath();
	//ASSET.SetPath(m_LightMapDirectory);

	grcTexture* lightMapTexture=grcTextureFactory::GetInstance().Create(fileName);
	Assert(lightMapTexture);
	m_Drawable->GetShaderGroup().SetVar(m_LightMapTextureIndex,lightMapTexture);
	
	//ASSET.SetPath(prevPath);
}

void prtGeometry::ComputeDirectLightMaps()
{
	for (int i=0;i<m_LightMapsDirect.GetCount();i++)
	{
		m_LightMapsDirect[i]->FillLightMap();
	}	
}

void prtGeometry::ComputeDirectLightMapsAlt(atArray<prtGeometry*>& geometries)
{
	for (int i=0;i<m_LightMapsDirect.GetCount();i++)
	{
		m_LightMapsDirect[i]->RenderFromLightPOVBetter(geometries);
	}	
}

void prtGeometry::ComputeAmbientOcclusionMaps(atArray<prtGeometry*>& geometries)
{
	for (int i=0;i<m_LightMapsAmbientOcclusion.GetCount();i++)
	{
		m_LightMapsAmbientOcclusion[i]->FillLightMap(geometries);
	}	
}

void prtGeometry::SaveDirectLightMaps(const char* fileName,bool reload)
{
	for (int i=0;i<m_LightMapsDirect.GetCount();i++)
	{
		//m_LightMapsDirect[i]->SaveTexture(fileName,grcImage::DXT5,true,4);
		m_LightMapsDirect[i]->SaveTexture(fileName,grcImage::DXT5,true,0);
	}	

	if (reload)
	{
		// reload the drawable to get the mesh with second set of texture coordinates:
		LoadDrawable();

		// for now, we must have only lightmap:
		Assert(m_LightMapsDirect.GetCount()==1);
		ReloadLightMap(fileName);
	}
}

void prtGeometry::SaveAmbientOcclusionMaps(const char* fileName)
{
	for (int i=0;i<m_LightMapsDirect.GetCount();i++)
	{
		m_LightMapsAmbientOcclusion[i]->SaveTexture(fileName,grcImage::DXT5);
	}	
}

void prtGeometry::ComputeProgressiveRadiosityLightMaps(atArray<prtGeometry*>& geometries,float modulationMask[])
{
	for (int i=0;i<m_LightMapsProgRadiosityLast.GetCount();i++)
	{
		m_LightMapsProgRadiosityLast[i]->IterateLightmap(geometries,modulationMask);
	}	
}

void prtGeometry::SaveProgressiveRadiosityLightMaps(const char* fileName)
{
	for (int i=0;i<m_LightMapsProgRadiosityLast.GetCount();i++)
	{
		m_LightMapsProgRadiosityLast[i]->SaveTexture(fileName,grcImage::DXT5);
	}	

	// for now, we must have only lightmap:
	Assert(m_LightMapsDirect.GetCount()==1);
	ReloadLightMap(fileName);
}

void prtGeometry::LoadProgressiveRadiosityLightMaps(const char* fileName)
{
	for (int i=0;i<m_LightMapsProgRadiosityLast.GetCount();i++)
	{
		m_LightMapsProgRadiosityLast[i]->LoadTexture(fileName);
	}	
}

} // namespace rage
