//
// prttools/config.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef PRTTOOLS_CONFIG_H
#define PRTTOOLS_CONFIG_H

#include "atl/string.h"
#include "system/xtl.h"
#include "vector/vector3.h"

#include <d3dx9mesh.h>

namespace rage {

struct prtInputMesh
{
    atString				m_FileName;
    bool					m_IsBlockerMesh;
    Vector3					m_Translate;
    Vector3					m_Scale;
    float					m_Yaw;
    float					m_Pitch;
    float			        m_Roll;
	atArray<D3DXSHMATERIAL>	m_SHMaterials;
};

struct prtSimulatorOptions
{
	prtSimulatorOptions::prtSimulatorOptions()
	{
		Reset();
	}

	bool Reset();

	atArray<prtInputMesh>	m_InputMeshes;
  
    // Settings
    DWORD     m_Order;
    DWORD     m_NumRays;
    DWORD     m_NumBounces;
    float     m_LengthScale;
    DWORD     m_NumChannels;

    // Compression
    bool						m_EnableCompression;
    int							m_NumClusters;
    D3DXSHCOMPRESSQUALITYTYPE	m_Quality;
    int							m_NumPCA;

    // Mesh Tessellation 
    bool      m_EnableTessellation;
    bool      m_RobustMeshRefine;
    float     m_RobustMeshRefineMinEdgeLength;
    int		  m_RobustMeshRefineMaxSubdiv;
    bool      m_AdaptiveDL;
    float     m_AdaptiveDLMinEdgeLength;
    float     m_AdaptiveDLThreshold;
    DWORD     m_AdaptiveDLMaxSubdiv;
    bool      m_AdaptiveBounce;
    float     m_AdaptiveBounceMinEdgeLength;
    float     m_AdaptiveBounceThreshold;
    DWORD     m_AdaptiveBounceMaxSubdiv;
    
    // Output
    atString	m_OutputConcatPRTMesh;
    atString	m_OutputConcatBlockerMesh;
    atString	m_OutputTessellatedMesh;
    atString	m_OutputPRTBuffer;
    atString	m_OutputCompPRTBuffer;
	
	bool		m_outputAmbientOcclusion;
    bool		m_BinaryXFile;
};

} // namespace rage

#endif // PRTTOOLS_CONFIG_H
