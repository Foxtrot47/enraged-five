using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace rmptfxtuner
{
	/// <summary>
	/// Summary description for Keyframeeditor.
	/// </summary>
	public class Keyframeeditor : System.Windows.Forms.Form
	{
		private ragKeyframeEditor.KeyframeEditor keyframeEditor1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Keyframeeditor()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.keyframeEditor1 = new ragKeyframeEditor.KeyframeEditor();
			this.SuspendLayout();
			// 
			// keyframeEditor1
			// 
			this.keyframeEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.keyframeEditor1.Location = new System.Drawing.Point(0, 0);
			this.keyframeEditor1.Name = "keyframeEditor1";
			this.keyframeEditor1.Size = new System.Drawing.Size(760, 365);
			this.keyframeEditor1.TabIndex = 0;
			// 
			// Keyframeeditor
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(760, 365);
			this.Controls.Add(this.keyframeEditor1);
			this.Name = "Keyframeeditor";
			this.Text = "Keyframeeditor";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Keyframeeditor_Closing);
			this.ResumeLayout(false);

		}
		#endregion

		private void Keyframeeditor_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel=true;
			Keyframeeditor editor = sender as Keyframeeditor;
			editor.Hide();
		}
	}
}
