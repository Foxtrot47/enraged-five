using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace rmptfxtuner
{
	/// <summary>
	/// Summary description for properties.
	/// </summary>
	public class properties : System.Windows.Forms.Form
	{
		private ragUi.ControlSlider controlSlider1;
		private System.Windows.Forms.CheckBox checkBox1;
		private ragUi.ControlSlider controlSlider2;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem6;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public properties()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.controlSlider1 = new ragUi.ControlSlider();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.controlSlider2 = new ragUi.ControlSlider();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// controlSlider1
			// 
			this.controlSlider1.IsFloat = true;
			this.controlSlider1.Location = new System.Drawing.Point(8, 31);
			this.controlSlider1.Maximum = 100F;
			this.controlSlider1.Minimum = 0F;
			this.controlSlider1.Name = "controlSlider1";
			this.controlSlider1.Size = new System.Drawing.Size(328, 24);
			this.controlSlider1.Step = 1F;
			this.controlSlider1.TabIndex = 5;
			this.controlSlider1.Text = "Distance in front of camera";
			this.controlSlider1.UseTrackbar = false;
			this.controlSlider1.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider1.ValueXPath = null;
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(8, 7);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(152, 24);
			this.checkBox1.TabIndex = 4;
			this.checkBox1.Text = "Start Emitters At Origin";
			// 
			// controlSlider2
			// 
			this.controlSlider2.IsFloat = false;
			this.controlSlider2.Location = new System.Drawing.Point(8, 95);
			this.controlSlider2.Maximum = 100F;
			this.controlSlider2.Minimum = 0F;
			this.controlSlider2.Name = "controlSlider2";
			this.controlSlider2.Size = new System.Drawing.Size(328, 24);
			this.controlSlider2.Step = 1F;
			this.controlSlider2.TabIndex = 6;
			this.controlSlider2.Text = "Frame Rate";
			this.controlSlider2.UseTrackbar = false;
			this.controlSlider2.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider2.ValueXPath = null;
			// 
			// checkBox2
			// 
			this.checkBox2.Location = new System.Drawing.Point(8, 71);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(152, 24);
			this.checkBox2.TabIndex = 7;
			this.checkBox2.Text = "Fixed Frame Rate";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(3, 131);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 16);
			this.label1.TabIndex = 8;
			this.label1.Text = "Default  Path";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(8, 150);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(328, 20);
			this.textBox1.TabIndex = 9;
			this.textBox1.Text = "c:\\rmptfx\\emitters";
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem3,
																					  this.menuItem2,
																					  this.menuItem6});
			this.menuItem1.Text = "Edit";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "Background Color";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 0;
			this.menuItem3.Text = "Load Model";
			// 
			// menuItem6
			// 
			this.menuItem6.Checked = true;
			this.menuItem6.Index = 2;
			this.menuItem6.Text = "Grid";
			// 
			// properties
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(352, 182);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.checkBox2);
			this.Controls.Add(this.controlSlider2);
			this.Controls.Add(this.controlSlider1);
			this.Controls.Add(this.checkBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Menu = this.mainMenu1;
			this.Name = "properties";
			this.Text = "properties";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.properties_Closing);
			this.ResumeLayout(false);

		}
		#endregion

		private void properties_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
			properties prop = sender as properties;
			prop.Hide();
		}
	}
}
