using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace rmptfxtuner
{
	/// <summary>
	/// Summary description for emitterprop.
	/// </summary>
	public class emitterprop : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem menuItem11;
		private System.Windows.Forms.MenuItem menuItem12;
		private System.Windows.Forms.MenuItem menuItem13;
		private System.Windows.Forms.MenuItem menuItem14;
		private System.Windows.Forms.MenuItem menuItem15;
		private System.Windows.Forms.MenuItem menuItem16;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.CheckBox checkBox4;
		private System.Windows.Forms.CheckBox checkBox3;
		private ragUi.ControlSlider controlSlider5;
		private ragUi.ControlSlider controlSlider4;
		private ragUi.ControlSlider controlSlider3;
		private ragUi.ControlSlider controlSlider2;
		private ragUi.ControlSlider controlSlider1;
		private ragUi.ControlSlider controlSlider6;
		private ragUi.ControlSlider controlSlider7;
		private ragUi.ControlSlider controlSlider8;
		private ragUi.ControlSlider controlSlider9;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem menuItem5;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public emitterprop()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public rmptfxtuner.ptxprop ptxproperties = new rmptfxtuner.ptxprop();
		public Keyframeeditor keyframeA = new Keyframeeditor();
		public Keyframeeditor keyframeB = new Keyframeeditor();
		public Keyframeeditor keyframeC = new Keyframeeditor();


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem11 = new System.Windows.Forms.MenuItem();
			this.menuItem12 = new System.Windows.Forms.MenuItem();
			this.menuItem13 = new System.Windows.Forms.MenuItem();
			this.menuItem14 = new System.Windows.Forms.MenuItem();
			this.menuItem15 = new System.Windows.Forms.MenuItem();
			this.menuItem16 = new System.Windows.Forms.MenuItem();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.checkBox3 = new System.Windows.Forms.CheckBox();
			this.controlSlider5 = new ragUi.ControlSlider();
			this.controlSlider4 = new ragUi.ControlSlider();
			this.controlSlider3 = new ragUi.ControlSlider();
			this.controlSlider2 = new ragUi.ControlSlider();
			this.controlSlider1 = new ragUi.ControlSlider();
			this.checkBox4 = new System.Windows.Forms.CheckBox();
			this.controlSlider6 = new ragUi.ControlSlider();
			this.controlSlider7 = new ragUi.ControlSlider();
			this.controlSlider8 = new ragUi.ControlSlider();
			this.controlSlider9 = new ragUi.ControlSlider();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.label6 = new System.Windows.Forms.Label();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem3,
																					  this.menuItem2,
																					  this.menuItem4,
																					  this.menuItem11});
			this.menuItem1.Text = "&File";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 0;
			this.menuItem3.Text = "Re Load";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "&Save";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 2;
			this.menuItem4.Text = "Save As";
			// 
			// menuItem11
			// 
			this.menuItem11.Index = 3;
			this.menuItem11.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem12,
																					   this.menuItem16});
			this.menuItem11.Text = "Add";
			// 
			// menuItem12
			// 
			this.menuItem12.Index = 0;
			this.menuItem12.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.menuItem13,
																					   this.menuItem14,
																					   this.menuItem15});
			this.menuItem12.Text = "New Ptx Rule";
			// 
			// menuItem13
			// 
			this.menuItem13.Index = 0;
			this.menuItem13.Text = "PtxSprite";
			// 
			// menuItem14
			// 
			this.menuItem14.Index = 1;
			this.menuItem14.Text = "PtxModel";
			// 
			// menuItem15
			// 
			this.menuItem15.Index = 2;
			this.menuItem15.Text = "PtxTrail";
			// 
			// menuItem16
			// 
			this.menuItem16.Index = 1;
			this.menuItem16.Text = "Existing Ptx Rule";
			// 
			// checkBox1
			// 
			this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.checkBox1.Location = new System.Drawing.Point(8, 16);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.TabIndex = 0;
			this.checkBox1.Text = "Show Instances";
			// 
			// checkBox2
			// 
			this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.checkBox2.Location = new System.Drawing.Point(8, 40);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(136, 24);
			this.checkBox2.TabIndex = 1;
			this.checkBox2.Text = "Show CullSpheres";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(238, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(222, 112);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Profiling";
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(8, 90);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 16);
			this.label5.TabIndex = 4;
			this.label5.Text = "DrawTime (cpu)";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(8, 69);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 16);
			this.label4.TabIndex = 3;
			this.label4.Text = "Update Time (cpu)";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(8, 50);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 16);
			this.label3.TabIndex = 2;
			this.label3.Text = "Culled Instances";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(8, 34);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 16);
			this.label2.TabIndex = 1;
			this.label2.Text = "Active Instances";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 18);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Total Points: ";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.checkBox1);
			this.groupBox2.Controls.Add(this.checkBox2);
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox2.Location = new System.Drawing.Point(7, 8);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(192, 112);
			this.groupBox2.TabIndex = 10;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Debugging";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.checkBox3);
			this.groupBox3.Controls.Add(this.controlSlider5);
			this.groupBox3.Controls.Add(this.controlSlider4);
			this.groupBox3.Controls.Add(this.controlSlider3);
			this.groupBox3.Controls.Add(this.controlSlider2);
			this.groupBox3.Controls.Add(this.controlSlider1);
			this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox3.Location = new System.Drawing.Point(7, 122);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(461, 152);
			this.groupBox3.TabIndex = 11;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Info";
			this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
			// 
			// checkBox3
			// 
			this.checkBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.checkBox3.Location = new System.Drawing.Point(8, 24);
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.TabIndex = 15;
			this.checkBox3.Text = "One Shot";
			// 
			// controlSlider5
			// 
			this.controlSlider5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider5.IsFloat = false;
			this.controlSlider5.Location = new System.Drawing.Point(8, 120);
			this.controlSlider5.Maximum = 100F;
			this.controlSlider5.Minimum = 0F;
			this.controlSlider5.Name = "controlSlider5";
			this.controlSlider5.Size = new System.Drawing.Size(208, 24);
			this.controlSlider5.Step = 1F;
			this.controlSlider5.TabIndex = 14;
			this.controlSlider5.Text = "Num Loops";
			this.controlSlider5.UseTrackbar = false;
			this.controlSlider5.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider5.ValueXPath = null;
			// 
			// controlSlider4
			// 
			this.controlSlider4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider4.IsFloat = true;
			this.controlSlider4.Location = new System.Drawing.Point(240, 88);
			this.controlSlider4.Maximum = 100F;
			this.controlSlider4.Minimum = 0F;
			this.controlSlider4.Name = "controlSlider4";
			this.controlSlider4.Size = new System.Drawing.Size(208, 24);
			this.controlSlider4.Step = 1F;
			this.controlSlider4.TabIndex = 13;
			this.controlSlider4.Text = "Playback Rate Bias";
			this.controlSlider4.UseTrackbar = false;
			this.controlSlider4.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider4.ValueXPath = null;
			// 
			// controlSlider3
			// 
			this.controlSlider3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider3.IsFloat = true;
			this.controlSlider3.Location = new System.Drawing.Point(8, 88);
			this.controlSlider3.Maximum = 100F;
			this.controlSlider3.Minimum = 0F;
			this.controlSlider3.Name = "controlSlider3";
			this.controlSlider3.Size = new System.Drawing.Size(208, 24);
			this.controlSlider3.Step = 1F;
			this.controlSlider3.TabIndex = 12;
			this.controlSlider3.Text = "Playback Rate";
			this.controlSlider3.UseTrackbar = false;
			this.controlSlider3.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider3.ValueXPath = null;
			// 
			// controlSlider2
			// 
			this.controlSlider2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider2.IsFloat = true;
			this.controlSlider2.Location = new System.Drawing.Point(240, 56);
			this.controlSlider2.Maximum = 100F;
			this.controlSlider2.Minimum = 0F;
			this.controlSlider2.Name = "controlSlider2";
			this.controlSlider2.Size = new System.Drawing.Size(208, 24);
			this.controlSlider2.Step = 1F;
			this.controlSlider2.TabIndex = 11;
			this.controlSlider2.Text = "Duration Bias";
			this.controlSlider2.UseTrackbar = false;
			this.controlSlider2.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider2.ValueXPath = null;
			// 
			// controlSlider1
			// 
			this.controlSlider1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider1.IsFloat = true;
			this.controlSlider1.Location = new System.Drawing.Point(8, 56);
			this.controlSlider1.Maximum = 100F;
			this.controlSlider1.Minimum = 0F;
			this.controlSlider1.Name = "controlSlider1";
			this.controlSlider1.Size = new System.Drawing.Size(208, 24);
			this.controlSlider1.Step = 1F;
			this.controlSlider1.TabIndex = 10;
			this.controlSlider1.Text = "Duration";
			this.controlSlider1.UseTrackbar = false;
			this.controlSlider1.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider1.ValueXPath = null;
			// 
			// checkBox4
			// 
			this.checkBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.checkBox4.Location = new System.Drawing.Point(8, 20);
			this.checkBox4.Name = "checkBox4";
			this.checkBox4.Size = new System.Drawing.Size(120, 24);
			this.checkBox4.TabIndex = 16;
			this.checkBox4.Text = "Use Cull Sphere";
			// 
			// controlSlider6
			// 
			this.controlSlider6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider6.IsFloat = true;
			this.controlSlider6.Location = new System.Drawing.Point(8, 50);
			this.controlSlider6.Maximum = 100F;
			this.controlSlider6.Minimum = 0F;
			this.controlSlider6.Name = "controlSlider6";
			this.controlSlider6.Size = new System.Drawing.Size(176, 24);
			this.controlSlider6.Step = 1F;
			this.controlSlider6.TabIndex = 17;
			this.controlSlider6.Text = "Offset         X";
			this.controlSlider6.UseTrackbar = false;
			this.controlSlider6.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider6.ValueXPath = null;
			// 
			// controlSlider7
			// 
			this.controlSlider7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider7.IsFloat = true;
			this.controlSlider7.Location = new System.Drawing.Point(192, 50);
			this.controlSlider7.Maximum = 100F;
			this.controlSlider7.Minimum = 0F;
			this.controlSlider7.Name = "controlSlider7";
			this.controlSlider7.Size = new System.Drawing.Size(128, 24);
			this.controlSlider7.Step = 1F;
			this.controlSlider7.TabIndex = 18;
			this.controlSlider7.Text = "Y";
			this.controlSlider7.UseTrackbar = false;
			this.controlSlider7.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider7.ValueXPath = null;
			// 
			// controlSlider8
			// 
			this.controlSlider8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider8.IsFloat = true;
			this.controlSlider8.Location = new System.Drawing.Point(320, 50);
			this.controlSlider8.Maximum = 100F;
			this.controlSlider8.Minimum = 0F;
			this.controlSlider8.Name = "controlSlider8";
			this.controlSlider8.Size = new System.Drawing.Size(128, 24);
			this.controlSlider8.Step = 1F;
			this.controlSlider8.TabIndex = 19;
			this.controlSlider8.Text = "Z";
			this.controlSlider8.UseTrackbar = false;
			this.controlSlider8.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider8.ValueXPath = null;
			// 
			// controlSlider9
			// 
			this.controlSlider9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider9.IsFloat = true;
			this.controlSlider9.Location = new System.Drawing.Point(8, 82);
			this.controlSlider9.Maximum = 100F;
			this.controlSlider9.Minimum = 0F;
			this.controlSlider9.Name = "controlSlider9";
			this.controlSlider9.Size = new System.Drawing.Size(176, 24);
			this.controlSlider9.Step = 1F;
			this.controlSlider9.TabIndex = 21;
			this.controlSlider9.Text = "Radius";
			this.controlSlider9.UseTrackbar = false;
			this.controlSlider9.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider9.ValueXPath = null;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.controlSlider8);
			this.groupBox4.Controls.Add(this.controlSlider7);
			this.groupBox4.Controls.Add(this.controlSlider9);
			this.groupBox4.Controls.Add(this.controlSlider6);
			this.groupBox4.Controls.Add(this.checkBox4);
			this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox4.Location = new System.Drawing.Point(7, 274);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(461, 120);
			this.groupBox4.TabIndex = 12;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Cull Sphere";
			// 
			// listBox1
			// 
			this.listBox1.ContextMenu = this.contextMenu1;
			this.listBox1.Items.AddRange(new object[] {
														  "Candle_Smoke_01",
														  "Candle_Fire_01",
														  "Candle_Glow_01",
														  "Candle_Drip_01"});
			this.listBox1.Location = new System.Drawing.Point(8, 420);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(456, 82);
			this.listBox1.TabIndex = 13;
			this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
			this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label6.Location = new System.Drawing.Point(8, 401);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(100, 14);
			this.label6.TabIndex = 14;
			this.label6.Text = "PtxRules";
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.button3);
			this.groupBox5.Controls.Add(this.button2);
			this.groupBox5.Controls.Add(this.button1);
			this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox5.Location = new System.Drawing.Point(7, 507);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(457, 114);
			this.groupBox5.TabIndex = 18;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Emission Info";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(16, 24);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(168, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Spawn Rate Keyframe";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(16, 50);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(168, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "Playback Rate Keyframe";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(16, 78);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(168, 23);
			this.button3.TabIndex = 2;
			this.button3.Text = "Offset Keyframe";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.menuItem5});
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 0;
			this.menuItem5.Text = "Edit Rule";
			this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
			// 
			// emitterprop
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(473, 622);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.groupBox4);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Menu = this.mainMenu1;
			this.Name = "emitterprop";
			this.Text = "emitterprop";
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void groupBox3_Enter(object sender, System.EventArgs e)
		{
		
		}

		private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.groupBox5.Text = "Emission Info: "+this.listBox1.Items[this.listBox1.SelectedIndex] as String;
		}

		private void menuItem5_Click(object sender, System.EventArgs e)
		{
			this.ptxproperties.Show();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			keyframeA.Show();
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			keyframeB.Show();

		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			keyframeC.Show();
		}

		private void listBox1_DoubleClick(object sender, System.EventArgs e)
		{
			if(this.ptxproperties==null)
				this.ptxproperties = new ptxprop();
			this.ptxproperties.Show();

		}
	}
}
