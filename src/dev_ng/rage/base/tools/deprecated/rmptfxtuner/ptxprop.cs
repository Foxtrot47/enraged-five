using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace rmptfxtuner
{
	/// <summary>
	/// Summary description for ptxprop.
	/// </summary>
	public class ptxprop : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.GroupBox groupBox4;
		private ragUi.ControlSlider controlSlider1;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.CheckBox checkBox4;
		private ragUi.ControlSlider controlSlider2;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Label label2;
		private ragUi.ControlSlider controlSlider3;
		private ragUi.ControlSlider controlSlider4;
		private ragUi.ControlSlider controlSlider5;
		private ragUi.ControlSlider controlSlider6;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.CheckBox checkBox5;
		private System.Windows.Forms.Label label3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ptxprop()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.button1 = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.button2 = new System.Windows.Forms.Button();
			this.controlSlider2 = new ragUi.ControlSlider();
			this.checkBox4 = new System.Windows.Forms.CheckBox();
			this.checkBox3 = new System.Windows.Forms.CheckBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.controlSlider1 = new ragUi.ControlSlider();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button3 = new System.Windows.Forms.Button();
			this.controlSlider6 = new ragUi.ControlSlider();
			this.controlSlider5 = new ragUi.ControlSlider();
			this.controlSlider4 = new ragUi.ControlSlider();
			this.controlSlider3 = new ragUi.ControlSlider();
			this.checkBox5 = new System.Windows.Forms.CheckBox();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem2,
																					  this.menuItem3,
																					  this.menuItem4});
			this.menuItem1.Text = "File";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "Re Load";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Text = "Save";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 2;
			this.menuItem4.Text = "Save As";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(328, 9);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(240, 207);
			this.groupBox1.TabIndex = 15;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Profiling";
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label5.Location = new System.Drawing.Point(8, 59);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 16);
			this.label5.TabIndex = 4;
			this.label5.Text = "DrawTime (cpu)";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label4.Location = new System.Drawing.Point(8, 38);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 16);
			this.label4.TabIndex = 3;
			this.label4.Text = "Update Time (cpu)";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(8, 18);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Total Points: ";
			// 
			// listBox1
			// 
			this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.listBox1.Items.AddRange(new object[] {
														  "Life/Life Bias",
														  "Direction",
														  "Angle (min/max)",
														  "Speed",
														  "Size",
														  "Rotation",
														  "Color (will use color gradient control)",
														  "Acceleration",
														  "Dampening",
														  "MatrixWeight"});
			this.listBox1.Location = new System.Drawing.Point(8, 16);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(232, 160);
			this.listBox1.TabIndex = 16;
			this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.groupBox3);
			this.groupBox2.Controls.Add(this.listBox1);
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox2.Location = new System.Drawing.Point(8, 224);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(256, 272);
			this.groupBox2.TabIndex = 17;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Info";
			this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.button1);
			this.groupBox3.Controls.Add(this.checkBox1);
			this.groupBox3.Location = new System.Drawing.Point(8, 184);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(232, 80);
			this.groupBox3.TabIndex = 17;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Property:";
			this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(16, 48);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(144, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "Keyframe Editor";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// checkBox1
			// 
			this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.checkBox1.Location = new System.Drawing.Point(16, 16);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(160, 24);
			this.checkBox1.TabIndex = 0;
			this.checkBox1.Text = "Key off system time";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.checkBox5);
			this.groupBox4.Controls.Add(this.label2);
			this.groupBox4.Controls.Add(this.comboBox1);
			this.groupBox4.Controls.Add(this.button2);
			this.groupBox4.Controls.Add(this.controlSlider2);
			this.groupBox4.Controls.Add(this.checkBox4);
			this.groupBox4.Controls.Add(this.checkBox3);
			this.groupBox4.Controls.Add(this.checkBox2);
			this.groupBox4.Controls.Add(this.controlSlider1);
			this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox4.Location = new System.Drawing.Point(8, 8);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(312, 208);
			this.groupBox4.TabIndex = 18;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Render Info";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(16, 143);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 16);
			this.label2.TabIndex = 7;
			this.label2.Text = "Shader:";
			// 
			// comboBox1
			// 
			this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.comboBox1.Items.AddRange(new object[] {
														   "<None>",
														   "PerPixel Lighting",
														   "Glass",
														   "HDR"});
			this.comboBox1.Location = new System.Drawing.Point(16, 167);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(121, 21);
			this.comboBox1.TabIndex = 6;
			this.comboBox1.Text = "<None>";
			// 
			// button2
			// 
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button2.Location = new System.Drawing.Point(152, 167);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(120, 23);
			this.button2.TabIndex = 5;
			this.button2.Text = "Shader Variables";
			// 
			// controlSlider2
			// 
			this.controlSlider2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider2.IsFloat = false;
			this.controlSlider2.Location = new System.Drawing.Point(16, 56);
			this.controlSlider2.Maximum = 100F;
			this.controlSlider2.Minimum = 0F;
			this.controlSlider2.Name = "controlSlider2";
			this.controlSlider2.Size = new System.Drawing.Size(280, 24);
			this.controlSlider2.Step = 1F;
			this.controlSlider2.TabIndex = 4;
			this.controlSlider2.Text = "Lighting Mode";
			this.controlSlider2.UseTrackbar = false;
			this.controlSlider2.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider2.ValueXPath = null;
			// 
			// checkBox4
			// 
			this.checkBox4.Checked = true;
			this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.checkBox4.Location = new System.Drawing.Point(120, 83);
			this.checkBox4.Name = "checkBox4";
			this.checkBox4.TabIndex = 3;
			this.checkBox4.Text = "Alpha Blend";
			// 
			// checkBox3
			// 
			this.checkBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.checkBox3.Location = new System.Drawing.Point(16, 107);
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.TabIndex = 2;
			this.checkBox3.Text = "Depth Test";
			// 
			// checkBox2
			// 
			this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.checkBox2.Location = new System.Drawing.Point(16, 83);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.TabIndex = 1;
			this.checkBox2.Text = "Depth Write";
			// 
			// controlSlider1
			// 
			this.controlSlider1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider1.IsFloat = false;
			this.controlSlider1.Location = new System.Drawing.Point(16, 24);
			this.controlSlider1.Maximum = 100F;
			this.controlSlider1.Minimum = 0F;
			this.controlSlider1.Name = "controlSlider1";
			this.controlSlider1.Size = new System.Drawing.Size(280, 24);
			this.controlSlider1.Step = 1F;
			this.controlSlider1.TabIndex = 0;
			this.controlSlider1.Text = "Blend Mode";
			this.controlSlider1.UseTrackbar = false;
			this.controlSlider1.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider1.ValueXPath = null;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.textBox1);
			this.groupBox5.Controls.Add(this.button3);
			this.groupBox5.Controls.Add(this.controlSlider6);
			this.groupBox5.Controls.Add(this.controlSlider5);
			this.groupBox5.Controls.Add(this.controlSlider4);
			this.groupBox5.Controls.Add(this.controlSlider3);
			this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.groupBox5.Location = new System.Drawing.Point(272, 224);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(296, 270);
			this.groupBox5.TabIndex = 19;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Texture Info";
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox1.Location = new System.Drawing.Point(8, 24);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(208, 20);
			this.textBox1.TabIndex = 6;
			this.textBox1.Text = "Smoke_01.dds";
			// 
			// button3
			// 
			this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button3.Location = new System.Drawing.Point(8, 52);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(136, 23);
			this.button3.TabIndex = 5;
			this.button3.Text = "Load Texture";
			// 
			// controlSlider6
			// 
			this.controlSlider6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider6.IsFloat = false;
			this.controlSlider6.Location = new System.Drawing.Point(8, 216);
			this.controlSlider6.Maximum = 100F;
			this.controlSlider6.Minimum = 0F;
			this.controlSlider6.Name = "controlSlider6";
			this.controlSlider6.Size = new System.Drawing.Size(208, 24);
			this.controlSlider6.Step = 1F;
			this.controlSlider6.TabIndex = 4;
			this.controlSlider6.Text = "Random End Tile";
			this.controlSlider6.UseTrackbar = false;
			this.controlSlider6.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider6.ValueXPath = null;
			// 
			// controlSlider5
			// 
			this.controlSlider5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider5.IsFloat = false;
			this.controlSlider5.Location = new System.Drawing.Point(8, 184);
			this.controlSlider5.Maximum = 100F;
			this.controlSlider5.Minimum = 0F;
			this.controlSlider5.Name = "controlSlider5";
			this.controlSlider5.Size = new System.Drawing.Size(208, 24);
			this.controlSlider5.Step = 1F;
			this.controlSlider5.TabIndex = 3;
			this.controlSlider5.Text = "Random Start Tile";
			this.controlSlider5.UseTrackbar = false;
			this.controlSlider5.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider5.ValueXPath = null;
			// 
			// controlSlider4
			// 
			this.controlSlider4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider4.IsFloat = false;
			this.controlSlider4.Location = new System.Drawing.Point(8, 152);
			this.controlSlider4.Maximum = 100F;
			this.controlSlider4.Minimum = 0F;
			this.controlSlider4.Name = "controlSlider4";
			this.controlSlider4.Size = new System.Drawing.Size(192, 24);
			this.controlSlider4.Step = 1F;
			this.controlSlider4.TabIndex = 2;
			this.controlSlider4.Text = "Tiles Y";
			this.controlSlider4.UseTrackbar = false;
			this.controlSlider4.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider4.ValueXPath = null;
			// 
			// controlSlider3
			// 
			this.controlSlider3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.controlSlider3.IsFloat = false;
			this.controlSlider3.Location = new System.Drawing.Point(8, 120);
			this.controlSlider3.Maximum = 100F;
			this.controlSlider3.Minimum = 0F;
			this.controlSlider3.Name = "controlSlider3";
			this.controlSlider3.Size = new System.Drawing.Size(192, 24);
			this.controlSlider3.Step = 1F;
			this.controlSlider3.TabIndex = 1;
			this.controlSlider3.Text = "Tiles X";
			this.controlSlider3.UseTrackbar = false;
			this.controlSlider3.Value = new System.Decimal(new int[] {
																		 0,
																		 0,
																		 0,
																		 0});
			this.controlSlider3.ValueXPath = null;
			// 
			// checkBox5
			// 
			this.checkBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.checkBox5.Location = new System.Drawing.Point(120, 112);
			this.checkBox5.Name = "checkBox5";
			this.checkBox5.TabIndex = 8;
			this.checkBox5.Text = "Sort";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(8, 80);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 16);
			this.label3.TabIndex = 5;
			this.label3.Text = "SortTime (ms)";
			// 
			// ptxprop
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(576, 513);
			this.Controls.Add(this.groupBox5);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Menu = this.mainMenu1;
			this.Name = "ptxprop";
			this.Text = "ptxprop";
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.groupBox3.Text = "Property: "+ listBox1.Items[listBox1.SelectedIndex] as String;
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			Keyframeeditor keyframe= new Keyframeeditor();
			keyframe.Show();
		}

		private void groupBox2_Enter(object sender, System.EventArgs e)
		{
		
		}

		private void groupBox3_Enter(object sender, System.EventArgs e)
		{
		
		}
	}
}
