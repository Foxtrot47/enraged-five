// 
// rfxcompiler/struct.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "struct.h"

#include "string/string.h"

using namespace rage;

rfxcStruct::rfxcStruct() {
}

rfxcStruct::~rfxcStruct() {
}

rfxcParserBlock::eAction rfxcStruct::DetermineAction(const char *token,bool /*moreInfoPass*/) const {
	return (strcmp(token, "struct") == 0 ? ACCEPT : REJECT );
}

void rfxcStruct::Process(rfxcTokenizer *token) {
	const char *structName = 0;

	VerifySyntax(token, "struct");

	// If it's a struct, the next token has to be the name
	structName = GetNextToken(token);
	
	// Create a new type
	sm_VarTypeList.CreateType(rfxcTypeInfo::TYPE_CUSTOM, structName);
	
	SkipBlock(token);
	TestSyntax(token, ";");
}
