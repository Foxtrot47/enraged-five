// 
// rfxcompiler/tokenizer.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "tokenizer.h"

#include "atl/string.h"
#include "file/stream.h"

using namespace rage;

namespace rage {
	struct rfxcTokenEntry {
		atString m_String;		// Ascii text for this token
		s32	m_NewlineCount;		// Number of newlines within this token (valid if whitespace is true)
		bool m_WhiteSpace;		// Indicates this token represents one or more whitespace characters

		rfxcTokenEntry *m_Next;
		rfxcTokenEntry *m_Prev;
	};
	bool IsNewLine(char c) {
		return ( c == '\n' );
	}

	bool IsWhiteSpace(char c) {
		return (c == 32 || c == 9 || c == 10 || c == 13 || c == 0);
	}

	bool IsAlphaNumeric(char c) {
		return ( (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '#' || c == '_' || c == '.' );
	}

	void AddNewEntry(rfxcTokenEntry *active, rfxcTokenEntry **first, rfxcTokenEntry **last, char *buff, int &start, int &end, bool wasWhiteSpace, int newLineCount ) {
		rfxcTokenEntry *e;
		if ( !active ) {
			e = new rfxcTokenEntry;
			e->m_Next = 0;
			e->m_Prev = *last;
			if ( *last )
				(*last)->m_Next = e;
			*last = e;
		}
		else {
			e = active;
		}
		
		if ( !*first ) {
			*first = e;
		}
		
		char c = buff[end];
		buff[end] = '\0';
		e->m_String += &buff[start];
		buff[end] = c;
		e->m_WhiteSpace = wasWhiteSpace;
		e->m_NewlineCount = newLineCount;
		
		start = end;
	}
}

rfxcTokenGroup::rfxcTokenGroup() : m_BeginGroup(0), m_EndGroup(0), m_UserId(0xffffffff) {
	/* EMPTY */
}

rfxcTokenGroup::~rfxcTokenGroup() {
	/* EMPTY */
}

void rfxcTokenGroup::AddTokenBoundary(rfxcTokenGroup *beginGrp, rfxcTokenGroup *endGrp) {
	m_BeginGroup = beginGrp ? beginGrp : endGrp;
	m_EndGroup = endGrp ? endGrp : beginGrp;
}

void rfxcTokenGroup::AddCharacterRange(int startChar, int endChar) {
	Assert(endChar >= startChar);
	CharRange &r = m_CharRanges.Append();
	r.m_Begin = (u16) startChar;
	r.m_End = (u16) endChar;
}

void rfxcTokenGroup::AddTestCharacter(int character) {
	m_TestCharacters.Append() = (char) character;
}

void rfxcTokenGroup::SetUserId(u32 userId) {
	m_UserId = userId;
}

rfxcTokenizer::rfxcTokenizer() {
	m_First = 0;
	m_Last = 0;
	m_LineId = 0;
	
	rfxcTokenGroup *group = &m_TokenGroups[ALPHANUMERIC];
	group->AddCharacterRange('a', 'z');
	group->AddCharacterRange('A', 'Z');
	group->AddCharacterRange('0', '9');
	group->AddTestCharacter('#');
	group->AddTestCharacter('.');
	group->AddTestCharacter('_');
	group->SetUserId(ALPHANUMERIC);
	
	group = &m_TokenGroups[WHITESPACE];
	group->AddTestCharacter(' ');
	group->AddTestCharacter('\t');
	group->AddTestCharacter('\n');
	group->AddTestCharacter('\f');
	group->AddTestCharacter('\r');
	group->AddTestCharacter('\0');
	group->SetUserId(WHITESPACE);
	
	group = &m_TokenGroups[STRING];
	m_DoubleQuotes.AddTestCharacter('\"');
	group->AddTokenBoundary(&m_DoubleQuotes, &m_DoubleQuotes);
	group->SetUserId(STRING);

	group = &m_TokenGroups[MATH];
	group->AddTestCharacter('+');	
	group->AddTestCharacter('-');	
	group->AddTestCharacter('*');	
	group->AddTestCharacter('/');	
	group->SetUserId(MATH);

	group = &m_TokenGroups[ASSIGNMENT];
	group->AddTestCharacter('=');
	group->SetUserId(ASSIGNMENT);
}

rfxcTokenizer::~rfxcTokenizer() {
	KillList();
}

void rfxcTokenizer::KillList() {
	rfxcTokenEntry *t = m_First;
	while ( t ) {
		rfxcTokenEntry *n = t->m_Next;
		delete t;
		t = n;
	}
	m_First = 0;
	m_Last = 0;
}

int rfxcTokenizer::FindNextGroup(int c) {
	const int loopCount = m_TokenGroups.GetMaxCount();
	for (int i = 0; i < loopCount; ++i) {
		if ( m_TokenGroups[i].IsBeginBoundary(c) ) {
			return i;
		}
		else if ( m_TokenGroups[i].IsCharacterValid(c, false) ) {
			return i;
		}
	}
	return -1;
}

void rfxcTokenizer::Init(fiStream *s) {
	KillList();
	m_LineId = 0;
	m_First = 0;
	m_Last = 0;

	TokenizeStream(s);
	
	m_Working = 0;
}

void rfxcTokenizer::TokenizeStream(fiStream *s) {
	const int buffSize = 4097;
	char workBuf[buffSize];
	
	rfxcTokenEntry *entry = 0;
	int newLineCount = 0;
	int readCount = 0;
	int currentGroup = -1;
	do {
		readCount = s->Read(workBuf, buffSize-1);
		int workIdx = -1;
		if ( entry == 0 ){
			currentGroup = FindNextGroup(workBuf[0]);
			workIdx = 0;
		}
		int startIdx = 0;
		while ( ++workIdx < readCount ) {
			bool endGroup = false;
			if ( currentGroup < 0 ) {
				endGroup = true;
			}
			else if ( m_TokenGroups[currentGroup].IsEndBoundary(workBuf[workIdx]) ) {
				endGroup = true;
				workIdx++;
			}
			else if ( m_TokenGroups[currentGroup].IsCharacterValid(workBuf[workIdx], true) == false ) {
				endGroup = true;
			}
			else if ( m_TokenGroups[currentGroup].GetUserId() == WHITESPACE ) {
				if ( IsNewLine(workBuf[workIdx]) ) {
					newLineCount++;
				}
			}
			if ( endGroup ) {
				bool whitespace = currentGroup > 0 ? m_TokenGroups[currentGroup].GetUserId() == WHITESPACE : false;
				AddNewEntry(entry, &m_First, &m_Last, workBuf, startIdx, workIdx, whitespace, newLineCount);
				newLineCount = 0;
				entry = 0;
				currentGroup = FindNextGroup(workBuf[workIdx]);
			}
		}		
		if ( startIdx != workIdx ) {
			entry = new rfxcTokenEntry;
			entry->m_Next = 0;
			entry->m_Prev = m_Last;
			m_Last->m_Next = entry;
			m_Last = entry;
			bool whitespace = currentGroup > 0 ? m_TokenGroups[currentGroup].GetUserId() == WHITESPACE : false;
			AddNewEntry(entry, &m_First, &m_Last, workBuf, startIdx, workIdx, whitespace, newLineCount);
		}
	} while ( readCount == buffSize-1 );
}

const char *rfxcTokenizer::GetNextToken(bool includeWhiteSpace, bool *outIsWhiteSpace) {
	rfxcTokenEntry *next = m_Working ? m_Working->m_Next : m_First;
	if ( next ) {
		m_Working = next;
		if ( m_Working->m_WhiteSpace  ) {
			m_LineId += m_Working->m_NewlineCount;
			if ( !includeWhiteSpace ) {
				return GetNextToken(includeWhiteSpace, outIsWhiteSpace);
			}
		}
		if ( outIsWhiteSpace ) 
			*outIsWhiteSpace = next->m_WhiteSpace;
		return next->m_String;
	}
	return 0;
}

void rfxcTokenizer::JumpTo(rfxcTokenHandle handle) {
	// Reset the line count
	m_LineId = 0;
	rfxcTokenEntry *e = m_First;
	while ( e ) {
		if ( e->m_WhiteSpace ) {
			m_LineId += e->m_NewlineCount;
		}
		if ( e == handle )
			break;
		e = e->m_Next;
	}
	Assert( e == handle && "Handle not in list" );
	m_Working = e;
}

void rfxcTokenizer::InsertAfter(rfxcTokenHandle handle, const char *text) {
	rfxcTokenEntry *e = new rfxcTokenEntry;
	e->m_Next = handle->m_Next;
	e->m_Prev = handle;
	handle->m_Next = e;
	if ( m_Last == handle ) {
		m_Last = e;
	}
	if ( e->m_Next ) {
		e->m_Next->m_Prev = e;
	}
	e->m_String = text;
	// NOTE: Should we reparse this string to see if one or more tokens are needed??
}

void rfxcTokenizer::InsertBefore(rfxcTokenHandle handle, const char *text) {
	rfxcTokenEntry *e = new rfxcTokenEntry;
	e->m_Next = handle;
	e->m_Prev = handle->m_Prev;
	handle->m_Prev = e;
	if ( m_First == handle ) {
		m_First = e;
	}
	if ( e->m_Prev ) {
		e->m_Prev->m_Next = e;
	}
	e->m_String = text;
	// NOTE: Should we reparse this string to see if one or more tokens are needed??
}

bool rfxcTokenizer::Write(fiStream *s) {
	rfxcTokenEntry *e = m_First;
	while ( e ) {
		if ( s->Write(e->m_String, e->m_String.GetLength()) != e->m_String.GetLength() ) {
			return false;
		}
		e = e->m_Next;
	}
	return true;
}

