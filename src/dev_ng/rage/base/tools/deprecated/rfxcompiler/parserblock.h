// 
// rfxcompiler/parserblock.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RFXCOMPILER_PARSERBLOCK_H
#define RFXCOMPILER_PARSERBLOCK_H

#include "typelist.h"
#include "tokenizer.h"

#include "atl/string.h"

namespace rage {

class rfxcParserBlock {
public:
	// List of actions to be taken by this block (moreinfo means not enough data to make a decision)
	enum eAction { ACCEPT, MOREINFO, REJECT };
public:
	rfxcParserBlock();
	virtual ~rfxcParserBlock();

	static void InitStatics();
	static void ShutdownStatics();	

	// RETURNS: An action to take for this particular parser block based on the string passed in
	virtual eAction DetermineAction(const char *token,bool moreInfoPass) const = 0;
	// PURPOSE: Process the tokens needed to parse the block this instance is responsible for
	virtual void Process(rfxcTokenizer *token) = 0;

	// RETURNS: The next token in the file, NULL if end of file
	//	IMPORTANT: This string is only valid until the next GetNextToken() call is made
	static const char *GetNextToken(rfxcTokenizer *t, bool required = true);

	// PURPOSE: Ensures next token in file matches value specified -- generates error if not
	static bool VerifySyntax(rfxcTokenizer *t, const char *match);

	// PURPOSE: Check to see if next token matches the specified string
	//			On success, the token is extracted from the stream.  On failure, it is put back in
	static bool TestSyntax(rfxcTokenizer *t, const char *match);
	
	// PURPOSE: Let this block attempt to process the token itself
	bool AttemptToParse( rfxcTokenizer *token );
	
protected:
	bool TryParserBlock( rfxcTokenizer *token, rfxcParserBlock *block );

	// PURPOSE: Blindly jumps over an entire block of code -- only useful if you don't care about the contents
	void SkipBlock(rfxcTokenizer *token, char blockBegin = '{', char blockEnd = '}');	

	static rfxcTypeList	sm_VarTypeList;		// Contains a list of currently valid types

private:
	static rfxcTokenHandle sm_LastHandle;
};

// Simple parserblock example that is actually useful
class rfxcLineInfo : public rfxcParserBlock {
public:
	rfxcLineInfo();
	virtual ~rfxcLineInfo();
	
	virtual eAction DetermineAction(const char *token,bool moreInfoPass) const;
	virtual void Process(rfxcTokenizer *token);

protected:
	int m_TokenizerLineId;
	int m_FileLineId;
	atString m_Filename;
};

// Ignore floating semicolons -- overkill, but keeps parser code cleaner
class rfxcSemiColon : public rfxcParserBlock {
public:
	eAction DetermineAction(const char *token,bool moreInfoPass) const;
	void Process(rfxcTokenizer *token);
};


};		// namespace rage

#endif	// RFXCOMPILER_PARSERBLOCK_H
