// 
// rfxcompiler/struct.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RFXCOMPILER_STRUCT_H
#define RFXCOMPILER_STRUCT_H

#include "parserblock.h"

namespace rage {


// PURPOSE: Provide a simple fx wrapper for a struct - we can evolve this into more elaborate struct support,
//	but only after the base types are nailed down first
class rfxcStruct : public rfxcParserBlock {
public:
	rfxcStruct();
	~rfxcStruct();
	
	virtual eAction DetermineAction(const char *token,bool moreInfoPass) const;
	virtual void Process(rfxcTokenizer *token);
};



};		// namespace rage


#endif	// RFXCOMPILER_STRUCT_H
