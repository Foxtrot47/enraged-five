// 
// rfxcompiler/function.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "function.h"

#include "string/string.h"

using namespace rage;

rfxcFunction::rfxcFunction() {
}

rfxcFunction::~rfxcFunction() {
}

rfxcParserBlock::eAction rfxcFunction::DetermineAction(const char *token,bool moreInfoPass) const {
	// Parenthesis is dead giveaway
	if (!strcmp(token, "(")) {
		return ACCEPT;
	}

	// Dead giveaways for functions
	if ( !strcmp(token, "inline") || !strcmp(token, "target") || !strcmp(token, "void") ) {
		return ACCEPT;
	}


	// If it's a variable type, then it's a maybe
	if ( sm_VarTypeList.GetType(token) ) {
		return MOREINFO;
	}

	// Possibilities
	if ( !strcmp(token, "static") || !strcmp(token, "const") ) {
		return MOREINFO;
	}

	if ( moreInfoPass ) {
		if ( !strcmp(token, ";") ) {
			return REJECT;
		}
		else
			return MOREINFO;
	}
	
	return REJECT;
}

void rfxcFunction::Process(rfxcTokenizer *token) {
	SkipBlock(token);
}
