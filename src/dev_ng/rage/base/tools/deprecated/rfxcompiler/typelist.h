// 
// rfxcompiler/typelist.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RFXCOMPILER_TYPELIST_H
#define RFXCOMPILER_TYPELIST_H

#include "atl/bitset.h"
#include "atl/map.h"
#include "grmodel/shadervalue.h"

namespace rage {

// NOTE: This should probably 
struct rfxcTypeInfo {
friend class rfxcTypeList;
public:
	enum eType {
		TYPE_BOOL,
		TYPE_INT,
		TYPE_FLOAT,
		TYPE_HALF,
		TYPE_DOUBLE,
		TYPE_STRING,
		TYPE_BASIC_COUNT,	// MAKE SURE TO KEEP BASIC VARIABLE TYPES ABOVE THIS LINE!!!
		TYPE_TEXTURE,
		TYPE_SAMPLER,
		TYPE_VERTEXSHADER,
		TYPE_PIXELSHADER,
		TYPE_VECTOR,
		TYPE_MATRIX,
		TYPE_CUSTOM,		// User defined type (i.e. struct)
		TYPE_COUNT
	};

	enum eFlags {
		FLG_VECTOR,
		FLG_MATRIX,
		FLG_ILLEGAL,		// This item is not legal
		FLG_TYPEDEF,		// This item refers to another item
		FLG_EXTERN,			// For use with typedefs
		FLG_SHARED,			// For use with typedefs
		FLG_STATIC,			// For use with typedefs
		FLG_UNIFORM,		// For use with typedefs
		FLG_VOLATILE,		// For use with typedefs
		FLG_CONST,			// For use with typedefs
		FLG_ROWMAJOR,		// For use with typedefs
		FLG_COLMAJOR,		// For use with typedefs
		FLG_COUNT
	};

	atFixedBitSet<FLG_COUNT>	m_Flags;		// Special flags to use for this type
	char *m_Name;								// String name of this type (as appears in .fx file)
	eType m_Type;								// The fx type of this variable
	int m_VectorSize;							// The size of the vector term, if a vector
	int m_MatrixCols;							// The number of columns of a matrix type, only valid if it's a matrix type
	int m_MatrixRows;							// The number of rows of a matrix type, only valid if it's a matrix type
	int m_NumRegisters;							// The number of registers used
	rfxcTypeInfo * m_Child;						// Only valid if CUSTOM type
	rfxcTypeInfo * m_Sibling;					// Valid if part of a structure
	rfxcTypeInfo * m_Parent;					// Valid only if typedef type

protected:
	rfxcTypeInfo() : m_Name(0), m_Type(TYPE_COUNT), m_VectorSize(0), m_MatrixCols(0), m_MatrixRows(0), m_NumRegisters(1), m_Child(0), m_Sibling(0) {}
};

class rfxcTypeList {
public:
	rfxcTypeList();
	~rfxcTypeList();

	void Init();
	void Shutdown();

	// RETURNS: A valid type pointer for the variable type specified if found, null if not
	const rfxcTypeInfo *GetType(const char *name) const;

	// RETURNS: A rage type to represent the specified info
	grmShaderValue::grmVarType GetRageType( const rfxcTypeInfo *info ) const;

	// PURPOSE: Register a type to the list, useful for custom types such as structs
	rfxcTypeInfo *CreateType(rfxcTypeInfo::eType type, const char *typeName);

protected:
	void BuildIntrinsicTypeList();
	
	atMap<ConstString, rfxcTypeInfo *>	*m_TypeHash;
};


};		// namespace rage

#endif	// RFXCOMPILER_TYPELIST_H
