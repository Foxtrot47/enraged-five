// 
// rfxcompiler/parserblock.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "parserblock.h"

#include "tokenizer.h"

#include "string/string.h"

#include <stdlib.h>

using namespace rage;

rfxcTypeList rfxcParserBlock::sm_VarTypeList;
rfxcTokenHandle rfxcParserBlock::sm_LastHandle = 0;

rfxcParserBlock::rfxcParserBlock() {
}

rfxcParserBlock::~rfxcParserBlock() {
}

void rfxcParserBlock::InitStatics() {
	sm_VarTypeList.Init();
}

void rfxcParserBlock::ShutdownStatics() {
	sm_VarTypeList.Shutdown();
}

void rfxcParserBlock::SkipBlock(rfxcTokenizer *token, char blockBegin, char blockEnd ) {
	int openParenCount = 0;
	const char *work;
	while ((work = GetNextToken(token)) != 0) {
		if ( work[0] != '"' ) {
			// Only if not a string token
			if ( strchr(work, blockBegin) ) {
				openParenCount++;
			}
			if ( strchr(work, blockEnd) ) {
				// Use <= in case this function is called before first '{' is hit or after
				if ( --openParenCount <= 0 ) {	
					break;
				}
			}
		}
	}
}

const char *rfxcParserBlock::GetNextToken(rfxcTokenizer *t, bool required) {
	sm_LastHandle = t->GetCurrentHandle();
	const char *token = t->GetNextToken();

	if ( !token && required ) {
		Quitf("Unexpected end of file found");
	}

	return token;
}

bool rfxcParserBlock::VerifySyntax(rfxcTokenizer *t, const char *match) {
	const char *token = t->GetNextToken(0);
	if ( (!token) || strcmp(token, match) ) {
		Errorf("Invalid syntax -- expected %s, go %s", match, token);
		return false;
	}
	return true;
}

bool rfxcParserBlock::TestSyntax(rfxcTokenizer *t, const char *match) {
	rfxcTokenHandle handle;
	handle = t->GetCurrentHandle();
	const char *token = t->GetNextToken();
	if ( !token ) {
		return false;
	}
	if ( strcmp(token, match) ) {
		t->JumpTo(handle);
		return false;
	}
	return true;
}

bool rfxcParserBlock::AttemptToParse(rfxcTokenizer *token) {
	return TryParserBlock(token, this);
}

bool rfxcParserBlock::TryParserBlock(rfxcTokenizer *token, rfxcParserBlock *block) {
	const char *work = 0;
	rfxcTokenHandle handle;
	handle = token->GetCurrentHandle();
	bool moreInfoPass = false;
	while ( (work = GetNextToken(token)) != 0 ) {
		eAction res = block->DetermineAction(work, moreInfoPass);
		if ( res == ACCEPT ) {
			token->JumpTo(handle);
			block->Process(token);
			return true;
		}
		else if ( res == REJECT ) {
			token->JumpTo(handle);
			return false;
		}
		else if ( res == MOREINFO ) {
			moreInfoPass = true;
		}
	}
	Errorf("Unexpected end of file");
	return false;
}


// ---------------------------------------
// ---------------------------------------
//	rfxcLineInfo
// ---------------------------------------
// ---------------------------------------

rfxcLineInfo::rfxcLineInfo() {
	m_TokenizerLineId = 0;
	m_FileLineId = 0;
}

rfxcLineInfo::~rfxcLineInfo() {
}

rfxcParserBlock::eAction rfxcLineInfo::DetermineAction(const char *token,bool /*moreInfoPass*/) const {
	return (strcmp(token, "#line") == 0 ? ACCEPT : REJECT );
}

void rfxcLineInfo::Process(rfxcTokenizer *token) {
	VerifySyntax(token, "#line");
	m_TokenizerLineId = token->GetLineId();
	const char *work = GetNextToken(token);
	m_FileLineId = atoi(work);
	m_Filename = GetNextToken(token);
}



rfxcParserBlock::eAction rfxcSemiColon::DetermineAction(const char *token,bool /*moreInfoPass*/ ) const {
	if ( !strcmp(token, ";") ) {
		return ACCEPT;
	}
	return REJECT;
}
void rfxcSemiColon::Process(rfxcTokenizer *token) {
	VerifySyntax(token, ";");
}	
