// 
// rfxcompiler/variable.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "variable.h"

#include "file/token.h"
#include "grmodel/shadervalue.h"
#include "string/string.h"

#include <stdlib.h>

using namespace rage;


// Helper classes for parsing variables

/////////////////////////////////////////////////////////////
//// Storage Modifiers
/////////////////////////////////////////////////////////////
class rfxcVarStorage : public rfxcParserBlock {
public:
	rfxcVarStorage() : m_Extern(false), m_Static(false), m_Shared(false), m_Volatile(false), m_Uniform(false) {}
	virtual ~rfxcVarStorage() {}
	
	virtual eAction DetermineAction(const char *token,bool /*moreInfoPass*/) const {
		if ( !strcmp(token, "extern") || !strcmp(token,"shared") || !strcmp(token, "static") || 
				!strcmp(token, "uniform") || !strcmp(token, "volatile") ) {
			return ACCEPT;
		}
		else {
			return REJECT;
		}
	}

	virtual void Process(rfxcTokenizer *token) {
		const char *work = token->GetNextToken();
		
		if ( !strcmp(work, "extern") ) {
			m_Extern = true;
			if ( m_Static ) {
				Errorf("Can't declare variable as both 'extern' and 'static'");
			}			
		}
		else if ( !strcmp(work, "shared") ) {
			m_Shared = true;
		}
		else if ( !strcmp(work, "static") ) {
			m_Static = true;
			if ( m_Extern ) {
				Errorf("Can't declare variable as both 'extern' and 'static'");
			}			
		}
		else if ( !strcmp(work, "uniform") ) {
			m_Uniform = true;
		}
		else if ( !strcmp(work, "volatile") ) {
			m_Volatile = true;
		}
		else {
			Assert(false && "Should never get here");
		}

		// Set defaults
		if ( m_Shared )
			m_Uniform = true;
		if ( !m_Static ) 
			m_Extern = true;
	};

	bool IsStatic() const { return m_Static; }
	bool IsShared() const { return m_Shared; }

protected:
	bool m_Extern;
	bool m_Static;
	bool m_Shared;
	bool m_Volatile;	// Not sure what this is used for yet...
	bool m_Uniform;
};

/////////////////////////////////////////////////////////////
//// Type Modifiers
/////////////////////////////////////////////////////////////
class rfxcVarTypeModifier : public rfxcParserBlock {
public:
	rfxcVarTypeModifier() : m_Const(false), m_RowMajor(false), m_ColMajor(false) {}
	virtual ~rfxcVarTypeModifier() {}
	
	virtual eAction DetermineAction(const char *token,bool /*moreInfoPass*/) const {
		if ( !strcmp(token, "const") || !strcmp(token, "row_major") || !strcmp(token, "col_major") ) {
			return ACCEPT;
		}
		else {
			return REJECT;
		}
	}
	
	virtual void Process(rfxcTokenizer *t) {
		const char *token = GetNextToken(t);
		
		if ( strcmp(token, "const") ) {
			m_Const = true;
		}
		else if ( strcmp(token, "row_major") ) {
			m_RowMajor = true;
			if ( m_ColMajor ) {
				Errorf("Can't assign row_major & col_major to same variable");
			}
		}
		else if ( strcmp(token, "col_major") ) {
			m_ColMajor = true;
			if ( m_RowMajor ) {
				Errorf("Can't assign row_major & col_major to same variable");
			}
		}
		else {
			Assert(false && "Should never get here");
		}
	
		// Handle defaults
	}

	bool IsConst() const { return m_Const; }
	bool IsRowMajor() const { return m_RowMajor; }


protected:
	bool m_Const;
	bool m_RowMajor;
	bool m_ColMajor;
};

/////////////////////////////////////////////////////////////
//// Variable Name
/////////////////////////////////////////////////////////////
class rfxcVarName : public rfxcParserBlock {
public:
	rfxcVarName() : m_ArrayCount(-1) {};
	virtual ~rfxcVarName() {}
	
	virtual eAction DetermineAction(const char * /*token*/,bool /*moreInfoPass*/) const { return ACCEPT; }	// Always passes
	
	virtual void Process(rfxcTokenizer *token) {
		m_Name = GetNextToken(token);
		
		if ( TestSyntax(token, "[") ) {
			const char *work = GetNextToken(token);
			if ( work ) {
				if ( *work == ']' ) {
					Errorf("Arrays must have a count");
				}
				else {
					m_ArrayCount = atoi(work);
				}
			}
			VerifySyntax(token, "]");
		}
	}

	bool IsArray() const { return m_ArrayCount > 0; }
	s32 GetArrayCount() const { return m_ArrayCount; }
	const char *GetName() const { return m_Name; }

protected:
	atString	m_Name;
	s32			m_ArrayCount;
};
/////////////////////////////////////////////////////////////
//// Semantic Name
/////////////////////////////////////////////////////////////
class rfxcVarSemanticName : public rfxcParserBlock {
public:
	rfxcVarSemanticName() {}
	virtual ~rfxcVarSemanticName() {}
	
	virtual eAction DetermineAction(const char *token,bool moreInfo) const {
		if ( !strcmp(token, ":") ) {
			return MOREINFO;
		}
		else if ( moreInfo && strcmp(token, "register") ) {
			// Assume if we're still getting called, we're the one to use
			return ACCEPT;
		}
		return REJECT;
	}
	virtual void Process(rfxcTokenizer *token) {
		VerifySyntax(token, ":");
		const char *work = GetNextToken(token);	// First token is ":"
		m_Name = work;
	}

	const char *GetName() const { return m_Name; }

protected:
	atString m_Name;
};

/////////////////////////////////////////////////////////////
//// Initializers
/////////////////////////////////////////////////////////////
class rfxcVarInitializer : public rfxcParserBlock {
public:
	rfxcVarInitializer() {};
	virtual ~rfxcVarInitializer() {};
	
	virtual eAction DetermineAction(const char *token,bool /*moreInfoPass*/) const {
		if ( !strcmp(token,"=") ) {
			return ACCEPT;
		}
		return REJECT;
	}
	
	virtual void Process(rfxcTokenizer *token ) {
		// All we want to do for now, is just skip past this section
		VerifySyntax(token, "=");

		// NOTE: Temporarily ignoring sampler state
		TestSyntax(token, "sampler_state");

		const char *work;
		work = GetNextToken(token);
		
		// There are 3 options here:
		//	variable = 20;
		//	variable = { 20, 20, 20 };
		//	variable = float3( 20, 20, 20 );
		
		if ( sm_VarTypeList.GetType(work) ) {
			// Found a variable type, so ignore everything else
			VerifySyntax(token, "(");
			int depth = 1;
			while ( (work = GetNextToken(token)) != 0 ) {
				if ( !strcmp(work, "(") || !strcmp(work,"{") )
					depth++;
				else if ( !strcmp(work, ")") || !strcmp(work,"}") )
					depth--;
				if ( depth == 0 )
					break;
			}
		}
		else if ( !strcmp(work, "{") ) {
			int depth = 1;
			while ( (work = GetNextToken(token)) != 0 ) {
				if ( !strcmp(work, "(") || !strcmp(work,"{") )
					depth++;
				else if ( !strcmp(work, ")") || !strcmp(work,"}") )
					depth--;
				if ( depth == 0 )
					break;
			}
		}
		else {
			// Must be a single literal
			// If a sign is found, skip to next token
			if ( !strcmp(work, "-") || !strcmp(work, "+") ) {
				work = GetNextToken(token);
			}
		}	
	}

protected:
	rfxcTypeInfo *m_TargetType;
};

/////////////////////////////////////////////////////////////
//// Annotation
/////////////////////////////////////////////////////////////
class rfxcVarAnnotation : public rfxcParserBlock {
public:
	rfxcVarAnnotation() {};
	virtual ~rfxcVarAnnotation() {};
	
	virtual eAction DetermineAction(const char *token,bool /*moreInfoPass*/) const {
		if ( !strcmp(token, "<") ) {
			return ACCEPT;
		}
		return REJECT;
	}
	virtual void Process(rfxcTokenizer *token) {
		const char *work;
		char varName[1024];
		char valString[4096];
		s32 valInt;
		float valFloat;
		bool valBool;
		VerifySyntax(token, "<");
		while ( strcmp((work = GetNextToken(token)), ">") ) {
			// TODO: Rework this after getting real initializer parsing working
			//		We can then reuse the rfxcVariable parserblock to process each annotation
			//		This implementation is a little on the ugly side
			//		New code could look something like:
			//		while ( work != ">" ) {
			//			rfxcVariable var;
			//			var.Process(token, &work, 1);
			//			Extract needed annotation data from var object
			//		}

			valString[0] = '\0';
			// Ensure this matches one of our supported types
			const rfxcTypeInfo *info = sm_VarTypeList.GetType(work);
			if ( !info ) {
				Errorf("Unknown type in annotation: %s", work);
			}
			if ( info->m_Type > rfxcTypeInfo::TYPE_BASIC_COUNT ) {
				Errorf("RAGE Annotations currently only support basic types");
			}
			// Get the variable name
			formatf(varName, sizeof(varName), "%s", GetNextToken(token));
			VerifySyntax(token, "=");
			
			// Get the value
			work = GetNextToken(token);
			grmShaderValue *rageVal = 0;
			if ( info->m_Type == rfxcTypeInfo::TYPE_INT ) {
				bool negative = false;
				if ( !strcmp(work, "-") ) {
					negative = true;
					work = GetNextToken(token);
				}
				valInt = atoi(work);
				if ( negative ) {
					valInt = -valInt;
				}
				rageVal = grmShaderValue::Create(grmShaderValue::VT_INT);
				rageVal->SetInt(valInt);
			}
			else if ( info->m_Type == rfxcTypeInfo::TYPE_FLOAT || info->m_Type == rfxcTypeInfo::TYPE_DOUBLE || info->m_Type == rfxcTypeInfo::TYPE_HALF ) {
				bool negative = false;
				if ( !strcmp(work, "-") ) {
					negative = true;
					work = GetNextToken(token);
				}
				valFloat = (float) atof(work);
				if ( negative ) {
					valFloat = -valFloat;
				}			
				rageVal = grmShaderValue::Create(grmShaderValue::VT_FLOAT);
				rageVal->SetFloat(valFloat);
			}
			else if ( info->m_Type == rfxcTypeInfo::TYPE_BOOL ) {
				valBool = (!strcmp(work, "true") || !strcmp(work, "1") );
				if ( !valBool && ((!strcmp(work, "false") || !strcmp(work, "0")) == false) ) {
					Errorf("Invalid bool annotation syntax");
				}
				rageVal = grmShaderValue::Create(grmShaderValue::VT_BOOL);
				rageVal->SetBool(valBool);
			}
			else if ( info->m_Type == rfxcTypeInfo::TYPE_STRING ) {
				if ( work[0] != '\"' ) {
					Errorf("Invalid string annotation syntax");
				}
				formatf(valString, sizeof(valString), "%s", &work[1]);
				int len = strlen(valString);
				if ( len && valString[len-1] == '\"' )
					valString[len-1] = '\0';
			}
			else {
				Errorf("Unsupported type for annotation variable");
			}
			VerifySyntax(token, ";");
			// Create a new annotation entry
			AnnotationData &data = m_Data.Grow();
			data.m_Name = varName;
			data.m_StringValue = valString;
			data.m_Value = rageVal;
		}
	}
protected:
	struct AnnotationData {
		atString m_Name;
		grmShaderValue *m_Value;
		atString m_StringValue;
	};
	
	atArray<AnnotationData>	m_Data;
};

/////////////////////////////////////////////////////////////
//// Register
/////////////////////////////////////////////////////////////
class rfxcVarRegister : public rfxcParserBlock {
public:
	rfxcVarRegister() : m_Register(-1), m_Type(-1) {}
	virtual ~rfxcVarRegister() {}
	
	virtual eAction DetermineAction(const char *token,bool moreInfo) const {
		if ( !strcmp(token, ":") ) {
			return MOREINFO;
		}
		else if ( moreInfo ) {
			if ( !strcmp(token, "register") ) {
				return ACCEPT;
			}
			else {
				return REJECT;
			}
		}
		return REJECT;
	}
	virtual void Process(rfxcTokenizer *token) {
		VerifySyntax(token, ":");
		VerifySyntax(token, "register");
		VerifySyntax(token, "(");
		const char *work = GetNextToken(token);
		if ( work ) {
			char  regType = work[0];
			if ( regType == 'c' ) {
				m_Type = CONSTANT;
			}
			else if ( regType == 's' ) {
				m_Type = SAMPLER;
			}
			else {
				Errorf("Unknown register type");
			}
			work++;
			m_Register = atoi(work);
		}
		VerifySyntax(token, ")");
	}

	s32 GetRegisterId() const { return m_Register; }
	bool IsConstant() const { return m_Type == CONSTANT; }
	bool IsSampler() const { return m_Type == SAMPLER; }

protected:
	enum {CONSTANT, SAMPLER};

	s32 m_Register;
	s32 m_Type;
};



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//									VARIABLE											//
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
rfxcVariable::rfxcVariable() {
}

rfxcVariable::~rfxcVariable() {
}

rfxcParserBlock::eAction rfxcVariable::DetermineAction(const char *token, bool moreInfoPass) const {
	// Parenthesis is dead giveaway
	if (strchr(token, '<') || strchr(token, ';') || strchr(token, '=') ) {
		return ACCEPT;
	}

	// Dead giveaways for variables
	if ( !strcmp(token, "extern") || !strcmp(token, "shared") || !strcmp(token, "uniform") || !strcmp(token, "volatile") ||
				!strcmp(token, "row_major") || !strcmp(token, "col_major") ) {
		return ACCEPT;
	}


	// If it's a variable type, then it's a maybe
	if ( sm_VarTypeList.GetType(token) ) {
		return MOREINFO;
	}

	// Possibilities
	if ( !strcmp(token, "static") || !strcmp(token, "const") ) {
		return MOREINFO;
	}

	if ( moreInfoPass ) {
		if ( !strcmp(token, "(") ) {
			return REJECT;
		}
		else
			return MOREINFO;
	}
			
	return REJECT;
}

bool TestEndFound(const char *test, int &annotationCount) {
	if ( strchr(test, '{') || strchr(test, '<') ) {
		annotationCount++;
	}
	if ( strchr(test, '}') || strchr(test, '>') ) {
		annotationCount--;
		Assert(annotationCount>=0);
	}
	if ( strchr(test, ';') && !annotationCount ) {
		return true;
	}
	return false;
}

void rfxcVariable::Process(rfxcTokenizer *token) {
	enum { STORAGE, TYPE_MODIFIER, NAME, SEMANTIC, INITALIZER, ANNOTATION, REGISTER, COUNT };
	atFixedBitSet<COUNT> blocks;
	blocks.Reset();
	
	// The sub-parsers we will be using
	rfxcVarStorage storage;
	rfxcVarTypeModifier modifier;
	rfxcVarName name;
	rfxcVarSemanticName semantic;
	rfxcVarInitializer initializer;
	rfxcVarAnnotation annotation;
	rfxcVarRegister reg;
	const rfxcTypeInfo *info = 0;
	
	// Break apart the variable syntax
	
	// Try each subparser, if it passes, set a flag
	// Storage first
	if ( TryParserBlock(token, &storage) ) {
		blocks.Set(STORAGE, true);
	}
	// Type modifier
	if ( TryParserBlock(token, &modifier) ) {
		blocks.Set(TYPE_MODIFIER, true);
	}
	// Type
	const char *work = GetNextToken(token);
	info = sm_VarTypeList.GetType(work);
	if ( info == 0 ) {
		Errorf("Unknown type in variable : %s", work);
	}
	// Variable name
	if ( TryParserBlock(token, &name) ) {
		blocks.Set(NAME, true);
	}
	// The remaining 4 items can fall in any order...
	rfxcParserBlock *parsers[] = { &semantic, &initializer, &annotation, &reg };
	const int flagOffset = SEMANTIC;
	const loopCount = COUNT - flagOffset;
	while ( TestSyntax(token, ";") == false ) {
		bool found = false;
		for (int i = 0; i < loopCount; ++i) {
			if ( blocks.IsClear(i+flagOffset) && TryParserBlock(token, parsers[i]) ) {
				blocks.Set(i+flagOffset, true);
				found = true;
				break;
			}
		}
		if ( !found ) {
			Errorf("Unknown token in variable: %s", token->GetNextToken());
		}
	}
	
	static int globalReg = 256;
	static int localReg = 0;
	int regId = 0;
	int regCount = info->m_NumRegisters * (name.IsArray()) ? name.GetArrayCount() : 1;
	if ( storage.IsShared() ) {
		globalReg -= regCount;
		regId = globalReg;
	}
	else {
		localReg += regCount;
		regId = localReg;
	}
	if ( localReg >= globalReg ) {
		Errorf("Register stompage");
	}
	// Dump out the data
	const char *rageTypes[] = {"int", "float", "vector2", "vector3", "vector4", "matrix34", "matrix44", "color32", "texture", "bool", "unknown"};
	Displayf("Variable found\n\tName: %s\n\tRage Type: %s\n\tGlobal: %s\n\tArray Count: %d\n\tRegister: %d\n", name.GetName(), rageTypes[sm_VarTypeList.GetRageType(info)], storage.IsShared() ? "true" : "false", name.GetArrayCount(), regId);
	
}
