// 
// rfxcompiler/parser.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "parser.h"

#include "function.h"
#include "struct.h"
#include "technique.h"
#include "tokenizer.h"
#include "variable.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "file/stream.h"
#include "file/token.h"
#include "string/string.h"
#include "system/alloca.h"

#include <stdlib.h>


using namespace rage;


// Ignore floating semicolons -- overkill, but keeps parser code cleaner
class rfxcSemiColon : rage::rfxcParserBlock {
public:
	rfxcSemiColon() {}
	~rfxcSemiColon() {}
	
	eAction DetermineAction(const char *token,bool /*moreInfoPass*/ ) const {
		if ( !strcmp(token, ";") ) {
			return ACCEPT;
		}
		return REJECT;
	}
	void Process(rfxcTokenizer *token) {
		VerifySyntax(token, ";");
	}	
};

rfxcParser::rfxcParser() : m_In(0) {
	memset(m_ParserBlocks, 0, sizeof(rfxcParserBlock *) * PARSER_COUNT);

	rfxcParserBlock::InitStatics();
	m_ParserBlocks[TECHNIQUE] = new rfxcTechnique;
	m_ParserBlocks[STRUCT] = new rfxcStruct;
	m_ParserBlocks[FUNC] = new rfxcFunction;
	m_ParserBlocks[VAR] = new rfxcVariable;
	m_ParserBlocks[LINEINFO] = new rfxcLineInfo;
	m_ParserBlocks[SEMICOLON] = new rfxcSemiColon;
}

rfxcParser::~rfxcParser() {
	delete m_In;
	for (int i = 0; i < PARSER_COUNT; ++i) {
		delete m_ParserBlocks[i];
	}
	rfxcParserBlock::ShutdownStatics();
}

bool rfxcParser::ProcessFile(const char *src, const char *destPath, ePlatform p) {
	if ( !Init(src, destPath, p) )
		return false;

	const char *token;
	rfxcTokenHandle handle = m_In->GetCurrentHandle();
	while ( (token = m_In->GetNextToken()) != 0 ) {
		bool found = false;
		m_In->JumpTo(handle);
		// Now we know a token exists, let each block attempt to process it
		for (int i = 0; i < PARSER_COUNT; ++i) {
			if ( m_ParserBlocks[i]->AttemptToParse(m_In) ) {
				handle = m_In->GetCurrentHandle();
				found = true;
				break;
			}
		}
		// If no more info is requested, but the token wasn't accepted, display an error for an unknown token
		if ( !found ) {
			Warningf("Unknown token in parser: %s", token);
		}
	}

	return true;
}

bool rfxcParser::Preprocess(const char *src, const char *dest, ePlatform p) {
	// Build up preprocessor defines based on platform
	static const int maxDefines = 100;
	Defines *defines = (Defines*) alloca(sizeof(Defines) * maxDefines);
	
	int idx = 0;
	defines[idx++].Set("__RAGE", "1");
	defines[idx++].Set("__SHADERMODEL", "30");	// NOTE: PC builds will need 30 & 20
	defines[idx++].Set("__XENON", p == XENON ? "1" : "0");
	defines[idx++].Set("__WIN32PC", p == PCOGL || p == PCDX ? "1" : "0");
	defines[idx++].Set("__DX", p == PCDX || p == XENON ? "1" : "0");
	defines[idx++].Set("__CG", p == PCOGL || p == PSN ? "1" : "0");
	// TODO: Support project specific #defines via cmd line args
	
	// Build a cmd line to preprocess the fx file
	char cmdLine[4096];
	formatf(cmdLine, sizeof(cmdLine), "cl /E ");
	for (int i = 0; i < idx; ++i) {
		int len = strlen(cmdLine);
		formatf(&cmdLine[len], sizeof(cmdLine) - len, "/D%s=%s ", defines[i].m_Name, defines[i].m_Value);
	}
	int len = strlen(cmdLine);
	formatf(&cmdLine[len], sizeof(cmdLine) - len, "%s > %s ", src, dest);

	return system(cmdLine) >= 0;	
}

bool rfxcParser::Init(const char *src, const char * /*destPath*/, ePlatform p) {
	
	char tempFile[2048];
	char tempPath[1024];
	strcpy(tempPath, "c:\\temp");
	formatf(tempFile, sizeof(tempFile), "%s/work.fx", tempPath);
	
	// Preprocess the src file (removing any preprocessor/comment related issues)
	if ( !Preprocess( src, tempFile, p ) ) {
		Errorf("Could not preprocess file: %s", src);
		return false;
	}

	// Open the input file
	fiStream *in = fiStream::Open(tempFile);
	if ( !in ) {
		Errorf("Could not open working file: %s", tempFile);
		return false;
	}
	
	// Prepare the tokenizer
	m_In = new rfxcTokenizer;
	m_In->Init(in);
	in->Close();

	return true;
}

