// 
// rfxcompiler/function.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RFXCOMPILER_FUNCTION_H
#define RFXCOMPILER_FUNCTION_H

#include "parserblock.h"

namespace rage {


// PURPOSE: Provide a simple fx wrapper for a struct - we can evolve this into more elaborate struct support,
//	but only after the base types are nailed down first
class rfxcFunction : public rfxcParserBlock {
public:
	rfxcFunction();
	~rfxcFunction();
	
	virtual eAction DetermineAction(const char *token,bool moreInfoPass) const;
	virtual void Process(rfxcTokenizer *token);
};



};		// namespace rage


#endif	// RFXCOMPILER_FUNCTION_H
