// 
// rfxcompiler/parser.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RFXCOMPILER_PARSER_H
#define RFXCOMPILER_PARSER_H

namespace rage {

class rfxcTokenizer;
class rfxcParserBlock;

class rfxcParser {
public: 
	rfxcParser();
	~rfxcParser();

	enum ePlatform {
		PCDX,
		PCOGL,
		XENON,
		PSN,
		COUNT
	};

	bool ProcessFile(const char *src, const char *destPath, ePlatform p = PCDX);

protected:
	bool Preprocess(const char *src, const char *dest, ePlatform p);
	bool Init(const char *src, const char *destPath, ePlatform p);
	
	// List the parserblocks types we're going to use in order of priority
	enum {
		SEMICOLON,
		LINEINFO,
		TECHNIQUE,
		STRUCT,
		FUNC,
		VAR,
		PARSER_COUNT
	};
	
	
	struct Defines {
		void Set(const char *name, const char *value) {
			m_Name = name;
			m_Value = value;
		};
		
		const char *m_Name;
		const char *m_Value;
	};
	
	rfxcTokenizer *m_In;
	rfxcParserBlock *m_ParserBlocks[PARSER_COUNT];
};

};	// namespace rage

#endif	// RFXCOMPILER_PARSER_H
