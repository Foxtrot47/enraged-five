// 
// rfxcompiler/typelist.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "typelist.h"

using namespace rage;

rfxcTypeList::rfxcTypeList() {
}

rfxcTypeList::~rfxcTypeList() {
}

void rfxcTypeList::Init() {
	m_TypeHash = new atMap<ConstString, rfxcTypeInfo *>;
	m_TypeHash->Create(50);
	BuildIntrinsicTypeList();
}

void rfxcTypeList::Shutdown() {
	for (int i=0; i<m_TypeHash->GetNumSlots(); i++) {
		atMap<ConstString, rfxcTypeInfo *>::Entry *e = m_TypeHash->GetEntry(i);
		while (e) {
			// do something with e->key and/or e->data
			delete e->data;
			e = e->next;
		}
	}
	delete m_TypeHash;
}

const rfxcTypeInfo *rfxcTypeList::GetType(const char *name) const {
	if ( !name || strlen(name) == 0 )
		return 0;
	rfxcTypeInfo *const *info = m_TypeHash->Access(name);
	// See if it's a banned type
	if ( info && (*info)->m_Flags.IsSet(rfxcTypeInfo::FLG_ILLEGAL) ) {
		Errorf("Shader uses unsupported variable type: %s", name);
		info = 0;
	}
	return info ? *info : 0;
}

rfxcTypeInfo *rfxcTypeList::CreateType(rfxcTypeInfo::eType type, const char *typeName) {
	Assert(m_TypeHash->Access(typeName) == 0);
	rfxcTypeInfo *info = new rfxcTypeInfo;
	info->m_Type = type;
	m_TypeHash->Insert(typeName, info);
	return info;
}

void rfxcTypeList::BuildIntrinsicTypeList() {
	// NOTE: THIS MUST MATCH ENUM TYPE LIST OF rfxcTypeInfo
	const char *types[] = { "bool", "int", "float", "half", "double", "string", "", "texture", "sampler", 
						"vertexshader", "pixelshader", "vector", "matrix", "" };
	
	// Handle the simple stuff first
	for (int i = 0; i < rfxcTypeInfo::TYPE_COUNT; ++i) {
		if ( i != rfxcTypeInfo::TYPE_BASIC_COUNT && i != rfxcTypeInfo::TYPE_CUSTOM ) {
			rfxcTypeInfo *info = CreateType((rfxcTypeInfo::eType) i, types[i]);
			if ( i == rfxcTypeInfo::TYPE_MATRIX || i == rfxcTypeInfo::TYPE_VECTOR ) {
				// We don't want to bother with explicit matrix or vector types right now
				info->m_Flags.Set(rfxcTypeInfo::FLG_ILLEGAL, true);
			}
		}
	}

	// Handle special samplers
	CreateType(rfxcTypeInfo::TYPE_SAMPLER, "sampler2D");
	CreateType(rfxcTypeInfo::TYPE_SAMPLER, "sampler1D");
	CreateType(rfxcTypeInfo::TYPE_SAMPLER, "sampler3D");
	CreateType(rfxcTypeInfo::TYPE_SAMPLER, "samplerCUBE");
	

	// Handle the vector types next
	char buff[256];
	for (int i = 1; i <= 4; ++i) {
		for (int j = 0; j < rfxcTypeInfo::TYPE_BASIC_COUNT; ++j) {
			formatf(buff, sizeof(buff), "%s%d", types[j], i);
			rfxcTypeInfo *info = CreateType((rfxcTypeInfo::eType) j, buff);
			info->m_Flags.Set(rfxcTypeInfo::FLG_VECTOR, true);
			info->m_VectorSize = i;
		}
	}

	// Handle Matrix types next
	for (int i = 1; i <= 4; ++i) {
		for (int j = 1; j <= 4; ++j) {
			for (int k = 0; k < rfxcTypeInfo::TYPE_BASIC_COUNT; ++k) {
				formatf(buff, sizeof(buff), "%s%dx%d", types[k], i, j);
				rfxcTypeInfo *info = CreateType((rfxcTypeInfo::eType) k, buff);
				info->m_Flags.Set(rfxcTypeInfo::FLG_MATRIX, true);
				info->m_MatrixRows= i;
				info->m_MatrixCols = j;
				info->m_NumRegisters = i;
			}
		}
	}
}

grmShaderValue::grmVarType rfxcTypeList::GetRageType(const rfxcTypeInfo *info) const {
	const grmShaderValue::grmVarType notSupported = grmShaderValue::VT_COUNT;
	grmShaderValue::grmVarType retVal = notSupported;
	switch (info->m_Type) {
		case rfxcTypeInfo::TYPE_INT:
			retVal = grmShaderValue::VT_INT;
			break;
		case rfxcTypeInfo::TYPE_BOOL:
			retVal = grmShaderValue::VT_BOOL;
			break;
		case rfxcTypeInfo::TYPE_FLOAT:	// fallthrough
		case rfxcTypeInfo::TYPE_DOUBLE:	// fallthrough
		case rfxcTypeInfo::TYPE_HALF:
			// Currently support matrix44 & matrix34
			if ( info->m_MatrixCols == 4 && info->m_MatrixRows == 4 ) {
				retVal = grmShaderValue::VT_MATRIX44;
			}
			else if ( info->m_MatrixCols == 3 && info->m_MatrixRows == 4 ) {
				retVal = grmShaderValue::VT_MATRIX34;
			}
			else if ( info->m_MatrixCols == 1 && info->m_MatrixRows == 1 ) {
				retVal = grmShaderValue::VT_FLOAT;
			}
			else if ( info->m_MatrixCols > 0 || info->m_MatrixRows > 0 ) {
				retVal = grmShaderValue::VT_COUNT;
			}
			else if ( info->m_VectorSize == 4 ) {
				retVal = grmShaderValue::VT_VECTOR4;
			}
			else if ( info->m_VectorSize == 3 ) {
				retVal = grmShaderValue::VT_VECTOR3;
			}
			else if ( info->m_VectorSize == 2 ) {
				retVal = grmShaderValue::VT_VECTOR2;
			}
			else {
				retVal = grmShaderValue::VT_FLOAT;
			}
			break;
		case rfxcTypeInfo::TYPE_TEXTURE:
			retVal = grmShaderValue::VT_TEXTURE;
			break;
		default:
			retVal = grmShaderValue::VT_COUNT;
			break;
	}		
	return retVal;
}
