// 
// rfxcompiler/variable.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RFXCOMPILER_VARIABLE_H
#define RFXCOMPILER_VARIABLE_H

#include "parserblock.h"

#include "atl/map.h"
#include "atl/string.h"

namespace rage {

class grmVariableInfo;
struct rfxcTypeInfo;

struct rfxcVariableData {
	grmVariableInfo *m_RageInfo;	// If this variable is needed for rage

	rfxcTypeInfo *m_ToolInfo;		// Tool specific variable data
	
	atString m_SrcText;				// Potentially modified source text
	s32	m_SrcLine;					// Line number of original source
};


// PURPOSE: Provide a simple fx wrapper for a struct - we can evolve this into more elaborate struct support,
//	but only after the base types are nailed down first
class rfxcVariable : public rfxcParserBlock {
public:
	rfxcVariable();
	~rfxcVariable();
	
	virtual eAction DetermineAction(const char *token,bool moreInfoPass) const;
	virtual void Process(rfxcTokenizer *token);

	// Lookup a specific variable name
//	static const *rfxcVariableData FindVariableData(const char *name);

protected:
	static atMap<ConstString, rfxcVariableData *> *sm_VariableList;	// List of all variables in use
};



};		// namespace rage


#endif	// RFXCOMPILER_VARIABLE_H
