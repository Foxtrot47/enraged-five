// 
// rfxcompiler/tokenizer.h 
//	Represents a class that can break up an fx file into multiple tokens.
//	These tokens are stored in RAM as a linked list so that they can be reparsed, as well as 
//	have new tokens inserted.  The entire token list can then be saved out to another stream.
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RFXCOMPILER_TOKENIZER_H
#define RFXCOMPILER_TOKENIZER_H

#include "atl/array.h"

namespace rage {

class fiStream;
struct rfxcTokenEntry;
typedef rfxcTokenEntry* rfxcTokenHandle;


// rfxcTokenGroup represents one or more characters that are used to separate out a token stream
//	This is exposed so that higher level code can configure the token stream any way they wish
class rfxcTokenGroup {
friend class rfxcTokenizer;
public:
	rfxcTokenGroup();
	~rfxcTokenGroup();

	// PURPOSE: add a starting & ending group to flag the beginning/end of a token group -- anything
	//	that passes the groups passed in will flag the start/end of this particular group
	//	Useful for things like strings, which require " at start & end
	void AddTokenBoundary(rfxcTokenGroup *beginGrp, rfxcTokenGroup *endGrp);
	
	// PURPOSE: add a starting & ending character range to test for validity within a group
	void AddCharacterRange(int startChar, int endChar);
	
	// PURPOSE: add a single character to the list to be tested
	void AddTestCharacter(int character);

	// PURPOSE: Assign a user definable id
	void SetUserId(u32 userId);

protected:
	// These methods should be called by the tokenizer class only

	// PURPOSE: Determine if character fits within this group
	inline bool IsCharacterValid(int c, bool groupActive) const;
	
	// PURPOSE: Determine if character is a beginning boundary character
	inline bool IsBeginBoundary(int c) const;

	// PURPOSE: Determine if character is an ending boundary character
	inline bool IsEndBoundary(int c) const;

	// RETURNS: The user definable id
	inline u32 GetUserId() const;

private:
	struct CharRange {
		u16 m_Begin;	// Beginning character (inclusive)
		u16 m_End;		// Ending character (inclusive)
	};
	
	// Begin & End group are mutually inclusive -- if you only set one, it will be copied to the other
	rfxcTokenGroup *m_BeginGroup;	// Set this if you want to use a character set for group start (i.e. ")
	rfxcTokenGroup *m_EndGroup;		// Set this if you want to use a character set for group end (i.e. ")									

	atFixedArray<CharRange, 32>	m_CharRanges;	// Represents quick & dirty tests for character hits
	atFixedArray<char, 32> m_TestCharacters;	// Used for individual, non linear character tests

	u32 m_UserId;
};

class rfxcTokenizer {
public:
	rfxcTokenizer();
	~rfxcTokenizer();

	// PURPOSE: Process this stream to pull all tokens out
	void Init(fiStream *s);

	// RETURNS: The next token in the list
	// PARAMS:	outHandle - pointer to a token handle that is assigned if non-null
	const char *GetNextToken(bool includeWhiteSpace=false, bool *outIsWhiteSpace=0);
	
	// PURPOSE: Move token retrieval stream to handle specified
	void JumpTo(rfxcTokenHandle handle);
	
	// PURPOSE: Insert new token AFTER specified handle
	void InsertAfter(rfxcTokenHandle handle, const char *text);
	
	// PURPOSE: Insert new token BEFORE specified handle
	void InsertBefore(rfxcTokenHandle handle, const char *text);

	// PURPOSE: Write out token stream to the specified stream
	bool Write(fiStream *s);
	
	// RETURNS: The current line id
	inline u32 GetLineId() const;

	// RETURNS: The current working token handle
	inline rfxcTokenHandle GetCurrentHandle() const;
	
protected:
	void TokenizeStream(fiStream *s);
	int FindNextGroup(int c);
	
	void KillList();

	enum { ALPHANUMERIC, WHITESPACE, STRING, MATH, ASSIGNMENT, GROUPCOUNT };

	rfxcTokenEntry *m_First;
	rfxcTokenEntry *m_Last;
	
	rfxcTokenEntry *m_Working;		// Last token sent
	u32 m_LineId;					// Current line number

	atRangeArray<rfxcTokenGroup, GROUPCOUNT> m_TokenGroups;
	rfxcTokenGroup m_DoubleQuotes;
};

inline bool rfxcTokenGroup::IsCharacterValid(int c, bool groupActive) const {
	if ( m_BeginGroup || m_EndGroup ) {
		return groupActive;
	}
		
	const int loopCount = m_CharRanges.GetCount();
	// Try ranges first
	for (int i = 0; i < loopCount; ++i) {
		if ( c >= (int) m_CharRanges[i].m_Begin && c <= (int) m_CharRanges[i].m_End ) {
			return true;
		}
	}
	// Try unique characters next
	const int stop = m_TestCharacters.GetCount();
	int idx = 0;
	while (idx < stop) {
		if ( c == (int) m_TestCharacters[idx++] ) {
			return true;
		}
	}
	
	return false;
}

inline bool rfxcTokenGroup::IsBeginBoundary(int c) const {
	if ( m_BeginGroup ) {
		return m_BeginGroup->IsCharacterValid(c, false);
	}
	return false;
}

inline bool rfxcTokenGroup::IsEndBoundary(int c) const {
	if ( m_EndGroup ) {
		return m_EndGroup->IsCharacterValid(c, false);
	}
	return false;
}

inline u32 rfxcTokenGroup::GetUserId() const {
	return m_UserId;
}


u32 rfxcTokenizer::GetLineId() const {
	return m_LineId;
}

rfxcTokenHandle rfxcTokenizer::GetCurrentHandle() const {
	return m_Working;
}

}	// namespace rage

#endif
