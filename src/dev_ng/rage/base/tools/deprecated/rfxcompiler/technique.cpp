// 
// rfxcompiler/technique.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 


#include "technique.h"

#include "string/string.h"

using namespace rage;

rfxcTechnique::rfxcTechnique() {
}

rfxcTechnique::~rfxcTechnique() {
}

rfxcParserBlock::eAction rfxcTechnique::DetermineAction(const char *token,bool /*moreInfoPass*/) const {
	return (strncmp(token, "technique", strlen("technique")) == 0 ? ACCEPT : REJECT );
}

void rfxcTechnique::Process(rfxcTokenizer *token) {
	SkipBlock(token);
}
