// 
// rfxcompiler/rfxc.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "rfxcompiler/parser.h"
#include "system/main.h"
#include "string/string.h"
#include "system/param.h"

namespace rage {
	PARAM( file, ".FX file to process" );
	PARAM( destpath, ".FX file to process" );
	PARAM( platform, "Target platform to compile shader for [pcdx, pcogl, xenon, psn]" );
}

using namespace rage;

int Main() {
	if ( PARAM_file.Get() == false ) {
		Errorf("Missing source filename");
		return 0;
	}

	const char *filename = "";
	PARAM_file.Get(filename);
	const char *destPath = "";
	PARAM_destpath.Get(destPath);
	const char *target = "pcdx";
	PARAM_platform.Get(target);
	
	// NOTE: MUST MATCH ENUMS IN rfxParser!!!
	const char *options[] = { "pcdx", "pcogl", "xenon", "ps3" };
	int i;
	for (i = 0; i < rfxcParser::COUNT; ++i) {
		if ( stricmp(options[i], target) == 0 ) {
			break;
		}
	}	
	
	if ( i == rfxcParser::COUNT ) {
		Errorf("Unknown platform format: %s", target);
		return 0;
	}
	
	rfxcParser parser;
	return parser.ProcessFile(filename, destPath, (rfxcParser::ePlatform) i) ? 1 : 0;
}