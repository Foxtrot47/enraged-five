// ddstool.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;
typedef int u32;
typedef int DDPIXELFORMAT;
typedef short DDCAPS2;

struct DDSURFACEDESC2 {
	char DDSNAME [4];
	u32 dwSize;		// Size of structure. This member must be set to 124.
	u32 dwFlags;	// Flags to indicate valid fields. Always include DDSD_CAPS, DDSD_PIXELFORMAT, DDSD_WIDTH, DDSD_HEIGHT.
	u32 dwHeight;	// Height of the main image in pixels
	u32 dwWidth;	// Width of the main image in pixels
	u32 dwPitchOrLinearSize;	// For uncompressed formats, this is the number of bytes per scan line (DWORD> aligned) for the main image. dwFlags should include DDSD_PITCH in this case. For compressed formats, this is the total number of bytes for the main image. dwFlags should be include DDSD_LINEARSIZE in this case.
	u32 dwDepth;	// For volume textures, this is the depth of the volume. dwFlags should include DDSD_DEPTH in this case.
	u32 dwMipMapCount; // For items with mipmap levels, this is the total number of levels in the mipmap chain of the main image. dwFlags should include DDSD_MIPMAPCOUNT in this case.
	u32 dwReserved1[4]; //	Unused	
	float fGamma;		//  gamma value that this texture was saved at.
	float fColorExp[3]; //	Color scale (pre-calculated exponent) (R,G,B) for HDR values
	float fColorOfs[3];	//	Color offset (R,G,B) for HDR values
	DDPIXELFORMAT ddpfPixelFormat; // 32-byte value that specifies the pixel format structure.
	DDCAPS2 ddsCaps; // 16-byte value that specifies the capabilities structure.
	u32 dwReserved2; // 	Unused
};

DDSURFACEDESC2 desc;

long int flen(FILE *f)
{
	long int oldpos = ftell(f);
	fseek(f, 0, SEEK_END);
	long int retval = ftell(f);
	fseek(f, oldpos, SEEK_SET);
	return retval;
}

void SetGamma(const char* fname, float gamma)
{
	FILE* in;
	fopen_s(&in, fname, "rb");
	long int filelen=0;
	char *fbuf = NULL;
	if(in)
	{
		filelen = flen(in);
		fread(&desc, sizeof(desc), 1, in);
		fbuf = new char [ filelen - sizeof(desc)];
		if(fbuf)
		{
			fread(fbuf, filelen - sizeof(desc), 1, in);
		}
		fclose(in);
		if(!fbuf)
		{
			cerr << "Out of memory!" << endl << "Aborting..." << endl;
			return;
		}
	}
	else
	{
		cerr << "Cannot open " << fname << " for reading!" << endl << "Aborting..." << endl;
	}
	float oldgamma = desc.fGamma;
	FILE* out;
	fopen_s(&out, fname, "wb");
	if(out)
	{
		desc.fGamma = gamma;
		fwrite(&desc, sizeof(desc), 1, out);
		fwrite(fbuf, filelen - sizeof(desc), 1, out);
		fclose(out);
		delete fbuf;
		cout << "Gamma changed from " << oldgamma << " to: " << desc.fGamma << endl;
	}
	else
	{
		cerr << "Cannot open " << fname << " for writing!" << endl << "Aborting..." << endl;
	}
}

int _tmain(int argc, const char* argv[])
{
	cout << endl << "*******************************************" << endl;
	cout << "* DDS Furry Helper Tool (C) 2007 Rockstar *" << endl;
	cout << "*******************************************" << endl;
	if(argc == 2)
	{
		FILE* in;
		fopen_s(&in, argv[1], "rb");
		if(in)
		{
			fread(&desc, sizeof(desc), 1, in);
			fclose(in);
			cout << endl << "Width: " << desc.dwWidth << " Height: " << desc.dwHeight << endl;
			cout << endl << "Number of mips: " << desc.dwMipMapCount << endl;
			cout << endl << "Depth (for 3D textures): " << desc.dwDepth << endl;
			cout << endl << "Gamma:          " << desc.fGamma << endl << endl;
			cout << "RED   offset:   " << desc.fColorOfs[0] << endl;
			cout << "GREEN offset:   " << desc.fColorOfs[1] << endl;
			cout << "BLUE  offset:   " << desc.fColorOfs[2] << endl<< endl;

			cout << "RED   exponent: " << desc.fColorExp[0] << endl;
			cout << "GREEN exponent: " << desc.fColorExp[1] << endl;
			cout << "BLUE  exponent: " << desc.fColorExp[2] << endl;
			cout << endl << "Analysis: " << endl;
			if( desc.fColorOfs[0] == 0.0f && desc.fColorOfs[1] == 0.0f && desc.fColorOfs[2] == 0.0f &&
				desc.fColorExp[0] == 0.0f && desc.fColorExp[1] == 0.0f && desc.fColorExp[2] == 0.0f)
			{
				cout << "This texture is NOT an HDR texture!" << endl;
			}
			else if(desc.fColorOfs[0] == 0.0f || desc.fColorOfs[1] == 0.0f || desc.fColorOfs[2] == 0.0f)
			{
				cout << "This texture IS an HDR texture! But..." << endl;
				if(desc.fColorOfs[0] == 0.0f)
				{
					cout << "...the RED channel goes all the way to 0";
					if(desc.fColorExp[0] == 1.0f)
						cout << " and the range is 1.0 which is VERY BAD!" << endl;
					else
						cout << endl;

				}
				if(desc.fColorOfs[1] == 0.0f)
				{
					cout << "...the GREEN channel goes all the way to 0";
					if(desc.fColorExp[1] == 1.0f)
						cout << " and the range is 1.0 which is VERY BAD!" << endl;
					else
						cout << endl;

				}
				if(desc.fColorOfs[2] == 0.0f)
				{
					cout << "...the BLUE channel goes all the way to 0";
					if(desc.fColorExp[2] == 1.0f)
						cout << " and the range is 1.0 which is VERY BAD!" << endl;
					else
						cout << endl;

				}
			}
			else
			{
				cout << "This texture IS a GOOD HDR texture!" << endl;
			}
		}
		else
		{
			cerr << "Cannot open " << argv[1] << " for reading!" << endl << "Aborting..." << endl;
		}
	}
	else if(argc == 3)
	{
		if(!strcmp(argv[2], "-sg"))
		{
			SetGamma(argv[1], 2.2f);
		} else if(!strcmp(argv[2], "-cg"))
		{
			SetGamma(argv[1], 0.0f);
		}
	}
	else
	{
		cout << "Usage:" << endl << "ddstool <filename> [-sg|-cg]";
	}
	return 0;
}

