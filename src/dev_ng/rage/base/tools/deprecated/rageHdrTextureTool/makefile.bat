set TESTERS=rageHdrTextureTool
set CODEONLY=rageHdrTextureTool
set HEADONLY=mergetexturesshader

set LIBS_CORE=%RAGE_CORE_LIBS% %RAGE_CR_LIBS% init
set LIBS_GFX=%RAGE_GFX_LIBS% ragehdrlib devil libtiff ragetexturelib ragetexturegeolib 
set LIBS=%LIBS_CORE% %LIBS_GFX% %LIBS_PARSER% rageAssetLog
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\migrate\src %RAGE_DIR%\suite\src
