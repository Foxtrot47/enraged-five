#include	"system/main.h"
#include	"system/param.h"
#include	"ragehdrlib/HdrTextureToolLib.h"
#include	"grcore/gprallocation.h"
#include	"parser/manager.h"

PARAM(tif, "Input tif filename");
PARAM(dds, "Output dds filename");
PARAM(exposure, "Exposure for texture");

int	Main	(void)
{
#if __XENON
	grcGPRAllocation::Init();
#endif
	INIT_PARSER;

	HDRTextureTool tool;

	HDRTextureTool::RunParams params;
	if(PARAM_dds.Get(params.outputFile))
	{
		if(!PARAM_tif.Get(params.inputFiles.Grow(1)))
		{
			Displayf("Missing input file parameter!");
			exit(1);
		}
		if(!PARAM_exposure.Get(params.exposures.Grow(1)))
		{
			params.exposures[0] = 0.0f;
		}
		params.doExport = true;
		tool.Init("t:/hdrtool/assets");
		tool.Run(&params);
		tool.Shutdown();
	}
	else
	{
		tool.Init("t:/hdrtool/assets");
		tool.Run();
		tool.Shutdown();
	}
	SHUTDOWN_PARSER;

	return 0;
}
