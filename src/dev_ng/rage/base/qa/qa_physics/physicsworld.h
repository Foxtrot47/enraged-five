// 
// qa_physics/physicsworld.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef QA_PHYSICS_PHYSICSWORLD_H
#define QA_PHYSICS_PHYSICSWORLD_H

#include "physics/leveldefs.h"

namespace rage {

class phSimulator;

class qaPhysicsWorld
{
public:
	qaPhysicsWorld(float size);
	~qaPhysicsWorld();
	bool Update();

private:
	void InitLevel(float size);
	void InitSimulator();

	phLevelNew*  m_Level;
	phSimulator* m_Simulator;
};

} // namespace rage

#endif // QA_PHYSICS_PHYSICSWORLD_H
