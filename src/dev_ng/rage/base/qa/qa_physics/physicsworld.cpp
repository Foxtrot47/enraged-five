// 
// qa_physics/physicsworld.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "physicsworld.h"

#include "phcore/materialmgrimpl.h"
#include "phcore/surface.h"
#include "phsolver/contactmgr.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "system/timemgr.h"
#include "vectormath/classes.h"

namespace rage {


qaPhysicsWorld::qaPhysicsWorld(float size)
{
	phMaterialMgrImpl<phSurface>::Create();
	phSimulator::InitClass();

	InitLevel(size);
	InitSimulator();
}

qaPhysicsWorld::~qaPhysicsWorld()
{
	phSimulator::ShutdownClass();
	phMaterialMgr::GetInstance().Destroy();

	delete m_Simulator;
	m_Level->Shutdown();
	delete m_Level;
}

bool qaPhysicsWorld::Update()
{
	phLevelNew::SetActiveInstance(m_Level);
	phSimulator::SetActiveInstance(m_Simulator);
	TIME.SetFixedFrameMode(30.0f);

	TIME.Update();
	float timeStep = TIME.GetSeconds();
	m_Simulator->PreUpdateInstanceBehaviors(timeStep);
	m_Simulator->Update(timeStep);
	m_Simulator->UpdateInstanceBehaviors(timeStep);

	return false;
}

void qaPhysicsWorld::InitLevel(float size)
{
	m_Level = rage_new phLevelNew();
	phLevelNew::SetActiveInstance(m_Level);

	const int     maxOctreeNodes    = 1000;
	const int     maxActiveObjects  = 5000;
	const int     maxObjects        = 5000;
	const Vec3V   minWorld(-size,-size,-size);
	const Vec3V   maxWorld( size, size, size);

	m_Level->SetExtents(minWorld, maxWorld);
	m_Level->SetMaxObjects(maxObjects);
	m_Level->SetMaxActive(maxActiveObjects);
	m_Level->SetNumOctreeNodes(maxOctreeNodes);
	m_Level->Init();
}

void qaPhysicsWorld::InitSimulator()
{
	const int maxActiveObjects = 512;

	m_Simulator = rage_new rage::phSimulator;
	phSimulator::SetActiveInstance(m_Simulator);
	m_Simulator = rage::phSimulator::GetActiveInstance();
	phSimulator::InitParams params;
	params.maxManagedColliders = maxActiveObjects;
	m_Simulator->Init(m_Level, params);

}

} // namespace rage
