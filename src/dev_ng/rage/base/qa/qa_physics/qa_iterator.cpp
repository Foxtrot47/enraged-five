// 
// test_physics/qaIterator.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "qa/qa.h"

#if __QA

#include "physicsworld.h"

#include "math/random.h"
#include "phbound/bound.h"
#include "phcore/poly.h"
#include "phcore/segment.h"
#include "physics/archetype.h"
#include "physics/inst.h"
#include "physics/iterator.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "system/rageroot.h"

using namespace rage;

class qaIterator : public qaItem
{
public:
	void Init(const char* boundName, int numInsts);
	void Shutdown();

	void Update(qaResult& result);

private:
	qaPhysicsWorld* s_IteratorWorld;
	atArray<phInst*> s_Boxes;
};

void qaIterator::Init(const char* boundName, int numInsts)
{
	s_IteratorWorld = rage_new qaPhysicsWorld(10000);
	ASSET.SetPath(RAGE_ASSET_ROOT"/physics");

	phBound::DisableMessages();
	phBound::SetBoundFlag(phBound::WARN_BAD_POLYGONS, false);
#if __DEV || __TOOL
	phPolygon::DisableMessages();
#endif // __DEV || __TOOL
	phBound* bound = phBound::Load(boundName);
	Assert(bound);

	phArchetype* archetype = rage_new phArchetypePhys;
	archetype->SetBound(bound);
	archetype->SetMass(1.0f);

	Matrix34 matrix(M34_IDENTITY);

	s_Boxes.Resize(numInsts);

	mthRandom rand(0);

	int box = 0;
	for (int i=0; i<numInsts; i++)
	{
		matrix.d.x = rand.GetRanged(-100.0f, 100.0f);
		matrix.d.y = rand.GetRanged(-100.0f, 100.0f);
		matrix.d.z = rand.GetRanged(-100.0f, 100.0f);

		phInst* newInst = rage_new phInst;
		s_Boxes[box] = newInst;
		s_Boxes[box]->SetArchetype(archetype);
		s_Boxes[box]->SetMatrix(RCC_MAT34V(matrix));
		PHSIM->AddFixedObject(s_Boxes[box]);
		++box;
	}
}

void qaIterator::Shutdown()
{
	for (int box = 0; box < s_Boxes.GetCount(); ++box)
	{
		PHSIM->DeleteObject(s_Boxes[box]->GetLevelIndex());
	}

	delete s_Boxes[0]->GetArchetype()->GetBound();
	delete s_Boxes[0]->GetArchetype();

	for (int box = 0; box < s_Boxes.GetCount(); ++box)
	{
		delete s_Boxes[box];
	}

	s_Boxes.Reset();

	delete s_IteratorWorld;
}

void qaIterator::Update(qaResult& result)
{
#if ENABLE_PHYSICS_LOCK
	phIterator iterator(phIterator::PHITERATORLOCKTYPE_READLOCK);
#else
	phIterator iterator;
#endif
	iterator.InitCull_Sphere(ORIGIN, 100.0f);
	iterator.SetStateIncludeFlags(phLevelBase::STATE_FLAG_FIXED);
	u16 levelIndex = iterator.GetFirstLevelIndex(PHLEVEL);
	int numHits = 0;
	while(levelIndex != phInst::INVALID_INDEX)
	{
		++numHits;
		levelIndex = iterator.GetNextLevelIndex(PHLEVEL);
	}

	atArray<u16> array;
	array.Reserve(2048);

	iterator.GetAllLevelIndices(PHLEVEL, array);

	if (numHits == array.GetCount())
	{
		TST_PASS;
	}
	else
	{
		TST_FAIL;
	}
}

QA_ITEM_FAMILY(qaIterator, (const char* boundName, int numInsts), (boundName, numInsts));

QA_ITEM(qaIterator, ("crate/bound.bnd", 10), qaResult::FAIL_OR_AVERAGE_TIME);
QA_ITEM(qaIterator, ("crate/bound.bnd", 100), qaResult::FAIL_OR_AVERAGE_TIME);
QA_ITEM(qaIterator, ("crate/bound.bnd", 1000), qaResult::FAIL_OR_AVERAGE_TIME);

#endif // __QA

