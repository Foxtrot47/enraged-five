// 
// qa_physics/qa_bulletpaper.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "qa/qa.h"

#if __QA

#include "physicsworld.h"

#include "math/nan.h"
#include "phbound/boundbox.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundsphere.h"
#include "physics/archetype.h"
#include "physics/collider.h"
#include "physics/inst.h"
#include "physics/simulator.h"
#include "system/rageroot.h"
#include "system/timemgr.h"

using namespace rage;

#define SPEW_BULLET_INFO 0

class qaBulletPaper : public qaItem
{
public:
	void Init(const char* bulletBound, const char* paperBound, float startSpeed, float endSpeed, float speedDelta);
	void Shutdown();

	void Update(qaResult& result);

private:
	void Restart(float speed);

	void DeletePossiblyCompositeBound(phBound* bound);

	phInst* m_Bullet;
	phInst* m_Paper;
	qaPhysicsWorld* m_World;

	float m_Speed;

	float m_DropTime;

	float m_FirstSpeed;
	float m_MaxSpeed;
	float m_SpeedDelta;
};

void qaBulletPaper::Init(const char* bulletBound, const char* paperBound, float startSpeed, float endSpeed, float speedDelta)
{
	EnableNanSignal(true);

	m_World = rage_new qaPhysicsWorld(5000000);

	ASSET.SetPath(RAGE_ASSET_ROOT"physics");
	phBound::DisableMessages();
	phBound::SetBoundFlag(phBound::WARN_BAD_POLYGONS, false);
#if __DEV || __TOOL
	phPolygon::DisableMessages();
#endif // __DEV || __TOOL

	phBound* paper = phBound::Load(paperBound);
	Assert(paper);
	phArchetypePhys* paperType = rage_new phArchetypePhys;
	paperType->SetBound(paper);
	paperType->SetMass(1.0f);
	m_Paper = rage_new phInst;
	m_Paper->SetArchetype(paperType);

	phBound* bullet = phBound::Load(bulletBound);
	Assert(bullet);
	phArchetypePhys* bulletType = rage_new phArchetypePhys;
	bulletType->SetBound(bullet);
	bulletType->SetMass(1.0f);
	m_Bullet = rage_new phInst;
	m_Bullet->SetArchetype(bulletType);

	m_MaxSpeed = endSpeed;
	m_SpeedDelta = speedDelta;

	Restart(startSpeed);
}

void qaBulletPaper::Restart(float speed)
{
	m_Speed = speed;

	if (m_Bullet->IsInLevel())
	{
		PHSIM->DeleteObject(m_Bullet->GetLevelIndex());
	}

	if (m_Paper->IsInLevel())
	{
		PHSIM->DeleteObject(m_Paper->GetLevelIndex());
	}

	Matrix34 paperMatrix(M34_IDENTITY);
	paperMatrix.d.y -= m_Paper->GetArchetype()->GetBound()->GetBoundingBoxMax().GetYf();
	m_Paper->SetMatrix(RCC_MAT34V(paperMatrix));
	PHSIM->AddFixedObject(m_Paper);

	Matrix34 bulletMtx(M34_IDENTITY);
	bulletMtx.d.y = speed * speed * 0.5f / -GRAVITY - m_Bullet->GetArchetype()->GetBound()->GetBoundingBoxMin().GetYf();
	m_Bullet->SetMatrix(RCC_MAT34V(bulletMtx));
	PHSIM->AddActiveObject(m_Bullet, false);

#if SPEW_BULLET_INFO
	Displayf("Dropping bullet with speed %f, implying height %f", speed, bulletMtx.d.y);
#endif 

	phCollider* collider = PHSIM->GetCollider(m_Bullet->GetLevelIndex());
	Assert(collider);
	collider->SetMaxSpeed(1e30f);
	TIME.SetFixedFrameMode(30.0f);

	m_DropTime = speed / -GRAVITY + 2.0f;
}

void qaBulletPaper::Shutdown()
{
	DeletePossiblyCompositeBound(m_Bullet->GetArchetype()->GetBound());
	delete m_Bullet->GetArchetype();
	delete m_Bullet;

	DeletePossiblyCompositeBound(m_Paper->GetArchetype()->GetBound());
	delete m_Paper->GetArchetype();
	delete m_Paper;

	delete m_World;
}

void qaBulletPaper::DeletePossiblyCompositeBound(phBound* bound)
{
	if (bound->GetType() == phBound::COMPOSITE)
	{
		phBoundComposite* composite = static_cast<phBoundComposite*>(bound);

		for (int part = 0; part < composite->GetNumBounds(); ++part)
		{
			delete composite->GetBound(part);
		}
	}

	delete bound;
}

void qaBulletPaper::Update(qaResult& result)
{
	bool done = false;
	while (!done)
	{
		TIME.Update();

		m_World->Update();

#if SPEW_BULLET_INFO
		if (phCollider* collider = PHSIM->GetCollider(m_Bullet))
		{
			Displayf("bullet at: %f, %f, %f, vel:  %f, %f, %f", m_Bullet->GetMatrix().d.x, m_Bullet->GetMatrix().d.y, m_Bullet->GetMatrix().d.z, collider->GetVelocity().x, collider->GetVelocity().y, collider->GetVelocity().z);	
		}
		else
		{
			Displayf("bullet at: %f, %f, %f, stationary", m_Bullet->GetMatrix().d.x, m_Bullet->GetMatrix().d.y, m_Bullet->GetMatrix().d.z);
		}
#endif

		m_DropTime -= TIME.GetSeconds();

		if (m_DropTime <= 0.0f)
		{
			if (RCC_MATRIX34(m_Bullet->GetMatrix()).d.y >= -1.0f)
			{
				m_Speed += m_SpeedDelta;
				if (m_Speed > m_MaxSpeed)
				{
					TST_PASS;
					done = true;
				}
				Restart(m_Speed);
			}
			else
			{
				Displayf("Ending y %f", RCC_MATRIX34(m_Bullet->GetMatrix()).d.y);
				TST_EXTRA_RESULT(m_Speed);
				TST_FAIL;
				done = true;
			}
		}
	}
}

QA_ITEM_FAMILY(qaBulletPaper, (const char* bulletBound, const char* paperBound, float startSpeed, float endSpeed, float speedDelta), (bulletBound, paperBound, startSpeed, endSpeed, speedDelta));

// *** Box as paper ***
// Spheres
QA_ITEM(qaBulletPaper, ("sphere_056/bound.bnd",     "box_222/bound.bnd",    1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);
QA_ITEM(qaBulletPaper, ("sphere_007/bound.bnd",     "box_222/bound.bnd",    1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Box
QA_ITEM(qaBulletPaper, ("box_222/bound.bnd",        "box_222/bound.bnd",    1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Capsules
QA_ITEM(qaBulletPaper, ("capsule/bound.bnd",        "box_222/bound.bnd",    1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);
QA_ITEM(qaBulletPaper, ("capsule_small/bound.bnd",  "box_222/bound.bnd",    1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Composite
QA_ITEM(qaBulletPaper, ("barbell/bound.bnd",        "box_222/bound.bnd",    1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Geometry
QA_ITEM(qaBulletPaper, ("cinderblock/bound.bnd",    "box_222/bound.bnd",    1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// *** Sphere as paper ***

// Spheres
QA_ITEM(qaBulletPaper, ("sphere_056/bound.bnd",     "sphere_122/bound.bnd", 1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);
QA_ITEM(qaBulletPaper, ("sphere_007/bound.bnd",     "sphere_122/bound.bnd", 1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Box
QA_ITEM(qaBulletPaper, ("box_222/bound.bnd",        "sphere_122/bound.bnd", 1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Capsules
QA_ITEM(qaBulletPaper, ("capsule/bound.bnd",        "sphere_122/bound.bnd", 1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);
QA_ITEM(qaBulletPaper, ("capsule_small/bound.bnd",  "sphere_122/bound.bnd", 1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Composite
QA_ITEM(qaBulletPaper, ("barbell/bound.bnd",        "sphere_122/bound.bnd", 1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Geometry
QA_ITEM(qaBulletPaper, ("cinderblock/bound.bnd",    "sphere_122/bound.bnd", 1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// *** Octree as paper ***
// Spheres
QA_ITEM(qaBulletPaper, ("sphere_056/bound.bnd",     "splashot/bound.bnd",   1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);
QA_ITEM(qaBulletPaper, ("sphere_007/bound.bnd",     "splashot/bound.bnd",   1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Box
QA_ITEM(qaBulletPaper, ("box_222/bound.bnd",        "splashot/bound.bnd",   1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Capsules
QA_ITEM(qaBulletPaper, ("capsule/bound.bnd",        "splashot/bound.bnd",   1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);
QA_ITEM(qaBulletPaper, ("capsule_small/bound.bnd",  "splashot/bound.bnd",   1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Composite
QA_ITEM(qaBulletPaper, ("barbell/bound.bnd",        "splashot/bound.bnd",   1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

// Geometry
QA_ITEM(qaBulletPaper, ("cinderblock/bound.bnd",    "splashot/bound.bnd",   1.0f, 2000.0f, 0.5f), qaResult::PASS_OR_EXTRA);

#endif // __QA
