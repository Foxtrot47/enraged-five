// 
// qa_physics/qa_boxwall.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "qa/qa.h"

#if __QA

#include "physicsworld.h"

#include "bank/bkmgr.h"
#include "math/random.h"
#include "phbound/bound.h"
#include "phbound/boundbox.h"
#include "physics/archetype.h"
#include "physics/collider.h"
#include "physics/inst.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "system/rageroot.h"
#include "system/timemgr.h"
#include "vectormath/legacyconvert.h"

namespace rage {

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM(Wireframe);

}

using namespace rage;

class qaBoxWall : public qaItem
{
public:
	void Init(int stackSize, float boxSize, float boxMass);
	void Shutdown();

	virtual void Update(qaResult& result);
	virtual void Draw();

private:
	qaPhysicsWorld* s_BoxesWorld;
	atArray<phInst*> s_Boxes;
	int s_StackSize;
	phInst s_Terrain;
};

void qaBoxWall::Init(int stackSize, float boxSize, float boxMass)
{
	s_StackSize = stackSize;

#if __BANK && __PFDRAW
	if (bkManager::IsEnabled())
	{
		GetRageProfileDraw().Init(20000000);
		GetRageProfileDraw().SetEnabled(true);
	}
#endif // __BANK && __PFDRAW

	s_BoxesWorld = rage_new qaPhysicsWorld(3000);

	ASSET.SetPath(RAGE_ASSET_ROOT);

	PFD_GROUP_ENABLE(Physics, true);
	PFD_GROUP_ENABLE(Bounds, true);
	PFD_ITEM_ENABLE(Solid, true);
	PFD_ITEM_ENABLE(Wireframe, false);

	phBound::DisableMessages();
	phBound::SetBoundFlag(phBound::WARN_BAD_POLYGONS, false);
#if __DEV || __TOOL
	phPolygon::DisableMessages();
#endif // __DEV || __TOOL

	phBound* terrain = phBound::Load("physics/big_plane/bound.bnd");
	Assert(terrain);

	phArchetype* terrainArchetype = rage_new phArchetype;
	terrainArchetype->SetBound(terrain);

	s_Terrain.SetMatrix( Mat34V(V_IDENTITY) );
	s_Terrain.SetArchetype(terrainArchetype);
	PHSIM->AddFixedObject(&s_Terrain);

	phBoundBox* boxBound = rage_new phBoundBox(Vector3(boxSize, boxSize, boxSize));
	phArchetype* boxArchetype = rage_new phArchetypePhys;
	boxArchetype->SetBound(boxBound);
	boxArchetype->SetMass(boxMass);

	Matrix34 boxMatrix(M34_IDENTITY);

	s_Boxes.Resize(stackSize * (stackSize + 1) / 2);

	Vector3 boxMax(VEC3V_TO_VECTOR3(boxBound->GetBoundingBoxMax()));
	Vector3 boxMin(VEC3V_TO_VECTOR3(boxBound->GetBoundingBoxMin()));
	float size = boxMax.y - boxMin.y;
	float halfSize = 0.5f*size;
	float width = boxMax.x - boxMin.x;
	Vector3 boxPosition;
	int box = 0;
	for (int i=0; i<stackSize; i++)
	{
		for (int j=0; j<stackSize-i;j++)
		{
			float posWide = float(j) * width - (stackSize - i - 1) * width * 0.5f;
			float posHigh = halfSize + float(i) * size;

			boxMatrix.d.Set(posWide,posHigh,0.0f);

			phInst* newInst = rage_new phInst;
			s_Boxes[box] = newInst;
			s_Boxes[box]->SetArchetype(boxArchetype);
			s_Boxes[box]->SetMatrix(RCC_MAT34V(boxMatrix));
			PHSIM->AddActiveObject(s_Boxes[box], false);
			++box;
		}
	}

	Assert(box == s_Boxes.GetCount());
}

void qaBoxWall::Shutdown()
{
	for (int box = 0; box < s_Boxes.GetCount(); ++box)
	{
		PHSIM->DeleteObject(s_Boxes[box]->GetLevelIndex());
	}

	delete s_Boxes[0]->GetArchetype()->GetBound();
	delete s_Boxes[0]->GetArchetype();

	for (int box = 0; box < s_Boxes.GetCount(); ++box)
	{
		delete s_Boxes[box];
	}

	s_Boxes.Reset();

	PHSIM->DeleteObject(s_Terrain.GetLevelIndex());

	delete s_Terrain.GetArchetype()->GetBound();
	delete s_Terrain.GetArchetype();

#if __PFDRAW
	GetRageProfileDraw().Shutdown();
#endif

	delete s_BoxesWorld;
}

void qaBoxWall::Update(qaResult& result)
{
	TIME.Update();

	s_BoxesWorld->Update();

	Vector3 boxMax(VEC3V_TO_VECTOR3(s_Boxes[0]->GetArchetype()->GetBound()->GetBoundingBoxMax()));
	Vector3 boxMin(VEC3V_TO_VECTOR3(s_Boxes[0]->GetArchetype()->GetBound()->GetBoundingBoxMin()));
	float size = boxMax.y - boxMin.y;
	float halfSize = 0.5f*size;
	float width = boxMax.x - boxMin.x;
	Vector3 boxPosition;
	int box = 0;
	for (int i=0; i<s_StackSize; i++)
	{
		for (int j=0; j<s_StackSize-i;j++)
		{
			float posWide = float(j) * width - (s_StackSize - i - 1) * width * 0.5f;
			float posHigh = halfSize + float(i) * size;

			if (!RCC_MATRIX34(s_Boxes[box]->GetMatrix()).d.IsClose(Vector3(posWide, posHigh, 0.0f), 0.1f))
			{
				TST_FAIL;
				return;
			}

			++box;
		}
	}

	if (PHLEVEL->GetFirstActiveIndex() == phInst::INVALID_INDEX)
	{
		TST_PASS;
	}
}

void qaBoxWall::Draw()
{
#if __PFDRAW
	PHLEVEL->ProfileDraw();

	grcBindTexture(NULL);
	grcState::Default();
	grcLightState::SetEnabled(true);

	//const Matrix34 & cameraMtx = GetCamMgr().GetWorldMtx();
	//GetRageProfileDraw().SendDrawCameraForServer(cameraMtx);
	GetRageProfileDraw().Render();
#endif
}

QA_ITEM_FAMILY(qaBoxWall, (int stackSize, float boxSize, float boxMass), (stackSize, boxSize, boxMass));

QA_ITEM_DRAW(qaBoxWall, (3, 1.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (4, 1.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (5, 1.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (6, 1.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (7, 1.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (8, 1.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (9, 1.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (10, 1.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);

QA_ITEM_DRAW(qaBoxWall, (3, 2.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (4, 2.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (5, 2.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (6, 2.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (7, 2.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (8, 2.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (9, 2.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (10, 2.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);

QA_ITEM_DRAW(qaBoxWall, (3, 4.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (4, 4.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (5, 4.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (6, 4.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (7, 4.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (8, 4.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (9, 4.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (10, 4.0f, 1.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);

QA_ITEM_DRAW(qaBoxWall, (3, 1.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (4, 1.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (5, 1.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (6, 1.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (7, 1.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (8, 1.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (9, 1.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (10, 1.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);

QA_ITEM_DRAW(qaBoxWall, (3, 2.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (4, 2.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (5, 2.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (6, 2.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (7, 2.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (8, 2.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (9, 2.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (10, 2.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);

QA_ITEM_DRAW(qaBoxWall, (3, 4.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (4, 4.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (5, 4.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (6, 4.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (7, 4.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (8, 4.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (9, 4.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);
QA_ITEM_DRAW(qaBoxWall, (10, 4.0f, 10.0f), qaResult::FAIL_OR_AVERAGE_TIME | qaResult::FAIL_OR_NUM_UPDATES);

#endif // __QA

