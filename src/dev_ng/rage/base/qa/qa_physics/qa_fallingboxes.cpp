// 
// qa_physics/qa_fallingboxes.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "qa/qa.h"

#if __QA

#include "physicsworld.h"

#include "bank/bkmgr.h"
#include "math/random.h"
#include "phbound/bound.h"
#include "phbound/boundbox.h"
#include "physics/archetype.h"
#include "physics/collider.h"
#include "physics/inst.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "grprofile/drawmanager.h"
#include "system/rageroot.h"
#include "system/timemgr.h"

namespace rage {

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM(Wireframe);

}

using namespace rage;

class qaFallingBoxes : public qaItem
{
public:
	void Init(const char* terrainBound, int numBoxes);
	void Shutdown();

	virtual void Update(qaResult& result);
	virtual void Draw();

private:
	qaPhysicsWorld* s_BoxesWorld;
	atArray<phInst*> s_Boxes;
	phInst s_Terrain;
};

void qaFallingBoxes::Init(const char* terrainBound, int numBoxes)
{
#if __BANK && __PFDRAW
	if (bkManager::IsEnabled())
	{
		GetRageProfileDraw().Init(20000000);
		GetRageProfileDraw().SetEnabled(true);
	}
#endif // __BANK && __PFDRAW

	s_BoxesWorld = rage_new qaPhysicsWorld(3000);

	ASSET.SetPath(RAGE_ASSET_ROOT);

	PFD_GROUP_ENABLE(Physics, true);
	PFD_GROUP_ENABLE(Bounds, true);
	PFD_ITEM_ENABLE(Solid, true);
	PFD_ITEM_ENABLE(Wireframe, false);

	phBound::DisableMessages();
	phBound::SetBoundFlag(phBound::WARN_BAD_POLYGONS, false);
#if __DEV || __TOOL
	phPolygon::DisableMessages();
#endif // __DEV || __TOOL

	phBound* terrain = phBound::Load(terrainBound);
	Assert(terrain);

	phArchetype* terrainArchetype = rage_new phArchetype;
	terrainArchetype->SetBound(terrain);

	s_Terrain.SetMatrix( Mat34V(V_IDENTITY) );
	s_Terrain.SetArchetype(terrainArchetype);
	PHSIM->AddFixedObject(&s_Terrain);

	phBoundBox* boxBound = rage_new phBoundBox(Vector3(2.0f, 3.0f, 4.0f));
	phArchetype* boxArchetype = rage_new phArchetypePhys;
	boxArchetype->SetBound(boxBound);
	boxArchetype->SetMass(1.0f);

	Matrix34 boxMatrix(M34_IDENTITY);
	Vector3 mins = VEC3V_TO_VECTOR3(terrain->GetBoundingBoxMin());
	Vector3 maxs = VEC3V_TO_VECTOR3(terrain->GetBoundingBoxMax());

	mthRandom randSource;

	s_Boxes.Resize(numBoxes);

	for (int box = 0; box < s_Boxes.GetCount(); ++box)
	{
		phInst* newInst = rage_new phInst;
		s_Boxes[box] = newInst;
		s_Boxes[box]->SetArchetype(boxArchetype);
		boxMatrix.d.x = randSource.GetRanged(mins.x*0.5f, maxs.x*0.5f);
		boxMatrix.d.y = randSource.GetRanged(maxs.y, maxs.y + 30.0f);
		boxMatrix.d.z = randSource.GetRanged(mins.z*0.5f, maxs.z*0.5f);
		s_Boxes[box]->SetMatrix(RCC_MAT34V(boxMatrix));
		PHSIM->AddActiveObject(s_Boxes[box], false);
	}
}

void qaFallingBoxes::Shutdown()
{
	for (int box = 0; box < s_Boxes.GetCount(); ++box)
	{
		PHSIM->DeleteObject(s_Boxes[box]->GetLevelIndex());
	}

	delete s_Boxes[0]->GetArchetype()->GetBound();
	delete s_Boxes[0]->GetArchetype();

	for (int box = 0; box < s_Boxes.GetCount(); ++box)
	{
		delete s_Boxes[box];
	}

	s_Boxes.Reset();

	PHSIM->DeleteObject(s_Terrain.GetLevelIndex());

	delete s_Terrain.GetArchetype()->GetBound();
	delete s_Terrain.GetArchetype();

#if __PFDRAW
	GetRageProfileDraw().Shutdown();
#endif

	delete s_BoxesWorld;
}

void qaFallingBoxes::Update(qaResult& result)
{
	TIME.Update();

	s_BoxesWorld->Update();

	for (int activeIndex = PHLEVEL->GetFirstActiveIndex(); activeIndex != phInst::INVALID_INDEX; activeIndex = PHLEVEL->GetNextActiveIndex())
	{
		phInst* inst = PHLEVEL->GetInstance(activeIndex);

		if (RCC_MATRIX34(inst->GetMatrix()).d.y < -100.0f)
		{
			PHSIM->DeactivateObject(activeIndex);
		}
	}

	if (PHLEVEL->GetFirstActiveIndex() == phInst::INVALID_INDEX)
	{
		TST_PASS;
	}
}

void qaFallingBoxes::Draw()
{
#if __PFDRAW
	PHLEVEL->ProfileDraw();

	grcBindTexture(NULL);
	grcState::Default();
	grcLightState::SetEnabled(true);

	//const Matrix34 & cameraMtx = GetCamMgr().GetWorldMtx();
	//GetRageProfileDraw().SendDrawCameraForServer(cameraMtx);
	GetRageProfileDraw().Render();
#endif
}

QA_ITEM_FAMILY(qaFallingBoxes, (const char* terrainBound, int numBoxes), (terrainBound, numBoxes));

QA_ITEM_DRAW(qaFallingBoxes, ("physics/big_plane/bound.bnd", 1), qaResult::FAIL_OR_AVERAGE_TIME);
QA_ITEM_DRAW(qaFallingBoxes, ("physics/big_plane/bound.bnd", 10), qaResult::FAIL_OR_AVERAGE_TIME);
QA_ITEM_DRAW(qaFallingBoxes, ("physics/big_plane/bound.bnd", 150), qaResult::FAIL_OR_AVERAGE_TIME);
QA_ITEM_DRAW(qaFallingBoxes, ("physics/splashot/bound.bnd", 50), qaResult::FAIL_OR_AVERAGE_TIME);

#endif // __QA

