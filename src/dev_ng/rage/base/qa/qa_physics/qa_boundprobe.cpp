// 
// test_physics/qaBoundProbe.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#include "qa/qa.h"

#if __QA

#include "physicsworld.h"

#include "math/random.h"
#include "phbound/bound.h"
#include "phcore/poly.h"
#include "phcore/segment.h"
#include "physics/shapetest.h"
#include "system/rageroot.h"


using namespace rage;

class qaBoundProbe : public qaItem
{
public:
	void Init(const char* boundName, float percentOfBoundExtent);
	void Shutdown();

	void Update(qaResult& result);

private:
	float s_Length;
	phBound* s_Terrain;
	qaPhysicsWorld* s_TerrainWorld;
};

void qaBoundProbe::Init(const char* boundName, float percentOfBoundExtent)
{
	s_TerrainWorld = rage_new qaPhysicsWorld(10000);
	ASSET.SetPath(RAGE_ASSET_ROOT"/physics");

	phBound::DisableMessages();
	phBound::SetBoundFlag(phBound::WARN_BAD_POLYGONS, false);
#if __DEV || __TOOL
	phPolygon::DisableMessages();
#endif // __DEV || __TOOL
	s_Terrain = phBound::Load(boundName);
	Assert(s_Terrain);

	s_Length = percentOfBoundExtent;
	AssertMsg(s_Length <= 1.0f, "Probe length must be less than 1.0, which is the diagonal extent of the loaded bound");
}

void qaBoundProbe::Shutdown()
{
	delete s_Terrain;

	delete s_TerrainWorld;
}

void qaBoundProbe::Update(qaResult& result)
{
	mthRandom random;

	Assert(s_Terrain);
	Vector3 mins = VEC3V_TO_VECTOR3(s_Terrain->GetBoundingBoxMin());
	Vector3 maxs = VEC3V_TO_VECTOR3(s_Terrain->GetBoundingBoxMax());

	float length = mins.Dist(maxs) * s_Length;

	phShapeTest<phShapeProbe> probeTest;

	for (int probe = 0; probe < 1000000; ++probe)
	{
		phSegment segment;
		Vector3 start(random.GetRanged(mins.x, maxs.x),
					  random.GetRanged(mins.y, maxs.y),
					  random.GetRanged(mins.z, maxs.z));
		Vector3 direction(random.GetRanged(-1.0f, 1.0f),
						  random.GetRanged(-1.0f, 1.0f),
						  random.GetRanged(-1.0f, 1.0f));
		direction.Normalize();
		
		Vector3 end;
		end.AddScaled(start, direction, length);

		segment.Set(start, end);
		probeTest.InitProbe(segment);
		probeTest.TestOneObject(*s_Terrain);
	}

	TST_PASS;
}

//QA_ITEM_FAMILY(qaBoundProbe, (const char* boundName, float percentOfBoundExtent), (boundName, percentOfBoundExtent));

//QA_ITEM(qaBoundProbe, ("park/bound.bnd",       0.001f), qaResult::FAIL_OR_AVERAGE_TIME);
//QA_ITEM(qaBoundProbe, ("park/bound.bnd",       0.01f),  qaResult::FAIL_OR_AVERAGE_TIME);
//QA_ITEM(qaBoundProbe, ("park/bound.bnd",       0.1f),   qaResult::FAIL_OR_AVERAGE_TIME);

#endif // __QA

