//
// bank/bkmgr.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if !__BANK // We still need the Bank channel, even w/o __BANK
#include "diag/channel.h"
#include "system/param.h"
RAGE_DEFINE_CHANNEL(Bank);
#endif

#if __BANK

#include "bkmgr.h"

#include "bank.h"
#include "bkangle.h"
#include "bkmatrix.h"
#include "bkseparator.h"
#include "bkvector.h"
#include "button.h"
#include "color.h"
#include "combo.h"
#include "data.h"
#include "imageviewer.h"
#include "io.h"
#include "list.h"
#include "packet.h"
#include "pane.h"
#include "slider.h"
#include "text.h"
#include "title.h"
#include "toggle.h"
#include "treelist.h"

#include "atl/string.h"
#include "diag/output.h"
#include "diag/channel.h"
#include "file/asset.h"
#include "file/remote.h"		// for fiIsShowingMessageBox
#include "string/string.h"
#include "system/alloca.h"
#include "system/ipc.h"
#include "system/param.h"
#include "system/threadtype.h"
#include "system/timer.h"
#include "system/xtl.h"

#include <stdio.h>

RAGE_DEFINE_CHANNEL(Bank);

using namespace rage;

bkManager* bkManager::sm_Instance;
bool bkManager::sm_initFinished = false;
bool bkManager::sm_Active;
bool bkManager::sm_Quit;
float bkManager::sm_ScrollMultiplier=1.0f;
float bkManager::sm_ScrollTimeLeft  =0.0f;
float bkManager::sm_ScrollTimeRight =0.0f;
float bkManager::sm_ScrollTimer     =0.0f;
bool bkManager::sm_SendDelayedPingResponse = false;
u32 bkManager::sm_PingResponseMsg;

typedef sysMessageQueue<bkInvokeDelegate, 32, true> bkMgrInvokeQueue;
bkMgrInvokeQueue* g_bkMgrInvokeQueue = NULL;

#if __WIN32PC
HWND__* bkManager::sm_RenderWindow =NULL;
#endif
sysCriticalSectionToken bkManager::sm_csToken;

static const char *s_AppName=NULL;
static char *s_OpenFileDest;
static int s_OpenFileDestSize;
static bool s_OpenFileCancelled;

enum 
{
	OPEN_FILE_1		=bkRemotePacket::USER+0,
	OPEN_FILE_2		=bkRemotePacket::USER+1,
	INIT_FINISHED	=bkRemotePacket::USER+2,
	WINDOW_HANDLE	=bkRemotePacket::USER+3,
	QUIT			=bkRemotePacket::USER+4,
	TIMER_STRING	=bkRemotePacket::USER+5,
	PING			=bkRemotePacket::USER+6,
	CLIPBOARD_COPY	=bkRemotePacket::USER+7,
	CLIPBOARD_CLEAR	=bkRemotePacket::USER+8,
	MEMORY_STRING	=bkRemotePacket::USER+9,
	DEBUG_ASSERT1	=bkRemotePacket::USER+10,
	DEBUG_ASSERT2	=bkRemotePacket::USER+11,
	DEBUG_QUITF		=bkRemotePacket::USER+12,
	DEBUG_CRASH		=bkRemotePacket::USER+13,
	CREATE_OUTPUT_WINDOW=bkRemotePacket::USER+14,
	SUB_CHANNEL_INFO=bkRemotePacket::USER+15
};

bkManager::bkManager(const char *appName) : bkGroup(NullCB,appName?appName:bkRemotePacket::IsServer()? "REMOTE BANK MANAGER" : "Bank Manager",0,true) {
	s_AppName = StringDuplicate( appName );
#if __WIN32PC
	m_Pane = NULL;
#endif
	
	bkRemotePacket::AddType(bkGroup::GetStaticGuid(),bkGroup::RemoteHandler);
	bkRemotePacket::AddType(bkBank::GetStaticGuid(),bkBank::RemoteHandler);
	bkRemotePacket::AddType(bkManager::GetStaticGuid(), bkManager::RemoteHandler);

	bkRemotePacket::AddType(bkText::GetStaticGuid(),bkText::RemoteHandler);
	bkRemotePacket::AddType(bkTitle::GetStaticGuid(),bkTitle::RemoteHandler);
	bkRemotePacket::AddType(bkButton::GetStaticGuid(),bkButton::RemoteHandler);

	bkRemotePacket::AddType(bkToggleVal<bool>::GetStaticGuid(),bkToggleVal<bool>::RemoteHandler);
	bkRemotePacket::AddType(bkToggleVal<s32>::GetStaticGuid(),bkToggleVal<s32>::RemoteHandler);
	bkRemotePacket::AddType(bkToggleVal<u8>::GetStaticGuid(),bkToggleVal<u8>::RemoteHandler);
	bkRemotePacket::AddType(bkToggleVal<u16>::GetStaticGuid(),bkToggleVal<u16>::RemoteHandler);
	bkRemotePacket::AddType(bkToggleVal<u32>::GetStaticGuid(),bkToggleVal<u32>::RemoteHandler);
    bkRemotePacket::AddType(bkToggleVal<atBitSet>::GetStaticGuid(),bkToggleVal<atBitSet>::RemoteHandler);    
	bkRemotePacket::AddType(bkToggleVal<float>::GetStaticGuid(),bkToggleVal<float>::RemoteHandler);    

	bkRemotePacket::AddType(bkSliderVal<float>::GetStaticGuid(),bkSliderVal<float>::RemoteHandler);
	bkRemotePacket::AddType(bkSliderVal<u8>::GetStaticGuid(),bkSliderVal<u8>::RemoteHandler);
	bkRemotePacket::AddType(bkSliderVal<s8>::GetStaticGuid(),bkSliderVal<s8>::RemoteHandler);
	bkRemotePacket::AddType(bkSliderVal<u16>::GetStaticGuid(),bkSliderVal<u16>::RemoteHandler);
	bkRemotePacket::AddType(bkSliderVal<s16>::GetStaticGuid(),bkSliderVal<s16>::RemoteHandler);
	bkRemotePacket::AddType(bkSliderVal<u32>::GetStaticGuid(),bkSliderVal<u32>::RemoteHandler);
	bkRemotePacket::AddType(bkSliderVal<s32>::GetStaticGuid(),bkSliderVal<s32>::RemoteHandler);

	bkRemotePacket::AddType(bkComboVal<u8>::GetStaticGuid(),bkComboVal<u8>::RemoteHandler);
	bkRemotePacket::AddType(bkComboVal<s8>::GetStaticGuid(),bkComboVal<s8>::RemoteHandler);
	bkRemotePacket::AddType(bkComboVal<s16>::GetStaticGuid(),bkComboVal<s16>::RemoteHandler);
	bkRemotePacket::AddType(bkComboVal<s32>::GetStaticGuid(),bkComboVal<s32>::RemoteHandler);

	bkRemotePacket::AddType(bkColor::GetStaticGuid(),bkColor::RemoteHandler);
	bkRemotePacket::AddType(bkList::GetStaticGuid(),bkList::RemoteHandler);

    bkRemotePacket::AddType(bkTreeList::GetStaticGuid(),bkTreeList::RemoteHandler);
    bkRemotePacket::AddType(bkData::GetStaticGuid(),bkData::RemoteHandler);
	
	bkRemotePacket::AddType(bkVector2::GetStaticGuid(),bkVector2::RemoteHandler);
	bkRemotePacket::AddType(bkVector3::GetStaticGuid(),bkVector3::RemoteHandler);
	bkRemotePacket::AddType(bkVector4::GetStaticGuid(),bkVector4::RemoteHandler);

	bkRemotePacket::AddType(bkMatrix33::GetStaticGuid(),bkMatrix33::RemoteHandler);
	bkRemotePacket::AddType(bkMatrix34::GetStaticGuid(),bkMatrix34::RemoteHandler);
	bkRemotePacket::AddType(bkMatrix44::GetStaticGuid(),bkMatrix44::RemoteHandler);

	bkRemotePacket::AddType( bkSeparator::GetStaticGuid(), bkSeparator::RemoteHandler );

	bkRemotePacket::AddType( bkImageViewer::GetStaticGuid(), bkImageViewer::RemoteHandler );
	bkRemotePacket::AddType( bkAngle::GetStaticGuid(), bkAngle::RemoteHandler );

	if (!bkRemotePacket::IsServer())
		RemoteCreate();

#if __WIN32PC
	if (bkWidget::sm_EnableWindows)
	{
		WindowCreate();
	}
#endif

	g_bkMgrInvokeQueue = rage_new bkMgrInvokeQueue;
}


int bkManager::GetGuid() const { return GetStaticGuid(); }



void bkManager::RemoteCreate() {
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetGuid(),this);
	p.Send();
}


void bkManager::RemoteUpdate() {
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin(bkRemotePacket::CHANGED,GetGuid(),this);
	p.Write_s16(m_Open);
	p.Send();
}

void bkManager::SendTimerString(const char* str) {
	if (bkRemotePacket::IsConnectedToRag())
	{
		bkRemotePacket p;
		p.Begin(TIMER_STRING,GetGuid(),this);
		p.Write_const_char(str);
		p.Send();
	}
}

void bkManager::SendMemoryString(const char* str) {
	if (bkRemotePacket::IsConnectedToRag())
	{
		bkRemotePacket p;
		p.Begin(MEMORY_STRING,GetGuid(),this);
		p.Write_const_char(str);
		p.Send();
	}
}

void bkManager::CopyTextToClipboard(const char* text)
{
	if (bkRemotePacket::IsConnectedToRag())
	{
		const int BUFF_LEN=128;
		char buff[BUFF_LEN];
		int len=(int)strlen(text);

		// break up string into packets:
		while (len>0)
		{
			safecpy(buff,text,Min(BUFF_LEN,len+1));
			bkRemotePacket p;
			p.Begin(CLIPBOARD_COPY,GetGuid(),this);
			p.Write_const_char(buff);
			p.Send();		
			text+=BUFF_LEN-1; // account for NULL at end of string
			len-=BUFF_LEN-1;
		}

	}
}

void bkManager::ClearClipboard()
{
	if (bkRemotePacket::IsConnectedToRag())
	{
		bkRemotePacket p;
		p.Begin(CLIPBOARD_CLEAR,GetGuid(),this);
		p.Send();
	}
}

void bkManager::CreateOutputWindow( const char* name, const char* color )
{
	if (bkRemotePacket::IsConnectedToRag())
	{
        bkRemotePacket p;
        p.Begin( CREATE_OUTPUT_WINDOW, GetGuid(), this );
        p.Write_const_char( name );
        p.Write_const_char( color );
        p.Send();
	}
}

void bkManager::SendSubChannelInfo( const char* channel, const char* subChannel )
{
	bkRemotePacket p;
	p.Begin(SUB_CHANNEL_INFO, GetGuid(), this);
	p.Write_const_char(channel);
	p.Write_const_char(subChannel);
	p.Send();
}

void bkManager::Rebuild()
{
	// set to false so we don't re-assign Widget id's
	bkWidget::sm_setWidgetId = false;

	// this will call each Widget's FinishCreate().
	bkGroup::Rebuild();

	bkWidget::sm_setWidgetId = true;

	// this will retrigger the INIT_FINISHED event
	sm_initFinished = false;
}

void bkManager::RemoteHandler(const bkRemotePacket& packet) {
	switch(packet.GetCommand())
	{
	case bkRemotePacket::CREATE:
		{
			packet.Begin();
			u32 id = packet.GetId();
			packet.End();
			bkAssertf(sm_Instance, "bkManager has't been initted yet");
			bkRemotePacket::SetWidgetId(*sm_Instance, id);
		}
		break;
	case bkRemotePacket::CHANGED:
		{
			packet.Begin();
			sm_Instance->m_Open = sm_Instance->m_PrevOpen = packet.Read_s16();
			packet.End();
			sm_Instance->WindowUpdate();
			sm_Instance->Changed();
		}
		break;
	case OPEN_FILE_1:
		{ // open file
			if (bkRemotePacket::IsServer()) {
				packet.Begin();
				const char *origDest = packet.Read_const_char();
				u32 maxDest = packet.Read_u32();
				const char *mask = packet.Read_const_char();
				bool save = packet.Read_bool();
				const char *desc = packet.Read_const_char();
				packet.End();

				char *dest = Alloca(char,maxDest);
				safecpy(dest,origDest, maxDest);
				bool result = sm_Instance->OpenFile(dest,maxDest,mask,save,desc);

				bkRemotePacket reply;
				reply.Begin(OPEN_FILE_1,GetStaticGuid(),sm_Instance);
				reply.Write_bool(result);
				if (result)
					reply.Write_const_char(dest);
				reply.Send();
			}
			else {
				packet.Begin();
				bool result = packet.Read_bool();
				s_OpenFileCancelled = false;
				if (result) {
					const char *str = packet.Read_const_char();
					if (s_OpenFileDest)
						safecpy(s_OpenFileDest,str, s_OpenFileDestSize);
					bkDisplayf("OpenFile(%s)",str);
				}
				else
				{
					bkDisplayf("OpenFile cancelled");
					s_OpenFileCancelled = true;
				}
				packet.End();
				s_OpenFileDest = 0;
				s_OpenFileDestSize = 0;
			}
		}
		break;
	case OPEN_FILE_2:
		{ // open file
			if (bkRemotePacket::IsServer()) {
				packet.Begin();
				const char *mask = packet.Read_const_char();
				bool save = packet.Read_bool();
				const char *desc = packet.Read_const_char();
				const char *registryPrefix = packet.Read_const_char();
				packet.End();
				
				bkRemotePacket reply;
				reply.Begin(OPEN_FILE_2,GetStaticGuid(),sm_Instance);
				reply.Write_const_char(sm_Instance->OpenFile(mask,save,desc,registryPrefix));
				reply.Send();
			}
			else {
				s_OpenFileCancelled = false;
				packet.Begin();
				const char *result = packet.Read_const_char();
				if (result)
					safecpy(s_OpenFileDest,result,s_OpenFileDestSize);
				else
				{
					s_OpenFileDest[0] = 0;
					s_OpenFileCancelled = true;
				}
				packet.End();
				s_OpenFileDest = 0;
				s_OpenFileDestSize = 0;
			}
		}
		break;
	case WINDOW_HANDLE:
		{ // receive the window handle
			if (!bkRemotePacket::IsServer())
			{
				packet.Begin();
				u32 windowHandle=packet.Read_u32();
#if __WIN32PC
				sm_RenderWindow=(struct HWND__*)windowHandle;
#else
				(void)windowHandle;
#endif
				packet.End();
			}
		}
		break;
	case PING:
		{
			packet.Begin();
			u32 replyMsg = packet.Read_u32();
			bool delay = packet.Read_bool();
			packet.End();

			if (!delay) {
				bkRemotePacket reply;
				reply.Begin(PING, GetStaticGuid(), sm_Instance);
				reply.Write_u32(replyMsg);
				reply.Write_bool(false);
				reply.Send();
                bkRemotePacket::SendPackets();
			}
			else 
			{
				sm_SendDelayedPingResponse = true;
				sm_PingResponseMsg = replyMsg;
			}
		}
		break;
	case QUIT:
		{ // receive the window handle
			if (!bkRemotePacket::IsServer())
			{

				bkDisplayf("received Quit message");
				sm_Quit=true;

				// send a quit message confirmation:
				bkRemotePacket sendPacket;
				sendPacket.Begin(QUIT,GetStaticGuid(),sm_Instance);
				sendPacket.Send();
                bkRemotePacket::SendPackets();

				bkRemotePacket::Close();
			}
		}
		break;
	case DEBUG_ASSERT1:
		bkAssertf(0, "Received an assert1 message");
		break;
	case DEBUG_ASSERT2:
		bkAssertf(0, "Received an assert2 message");
		break;
	case DEBUG_QUITF:
		{
			Quitf("Received a quitf message");
		}
		break;
	case DEBUG_CRASH:
		{
			bkManager* nullPointer = NULL;
			bkWidget* nullDeref = nullPointer->m_Child;
			if (nullDeref->GetNext())
			{
			}
		}
		break;
	default:
		{
			bkErrorf("bkManager::RemoteHandler: Unknown message type %d", packet.GetCommand());
		}
	}

}


bkManager::~bkManager() 
{
	bkRemotePacket::ResetTypes();

	StringFree( s_AppName );

	delete g_bkMgrInvokeQueue;
	g_bkMgrInvokeQueue = NULL;
}


bkBank& bkManager::CreateBank(const char *name,int /*x*/,int /*y*/,bool /*addToMasterBank*/) 
{
    SYS_CS_SYNC( sm_csToken );

	bkBank& newBank = *(rage_new bkBank(name,NullCB,NullCB));
	AddChild(newBank).FinishCreate();
	return newBank;
}


// NOTES: Do NOT use this return value for anything except as a parameter to BANKMGR.DestroyBank.
bkBank& bkManager::RegisterBank(const char *name,datCallback creator,datCallback destructor) 
{
	bkAssertf((creator.GetType() == datCallback::NULL_CALLBACK || creator.GetType() == datCallback::USE_CALL_PARAM) , "add ',0,true' as final parameters of datCallback");
	bkAssertf((destructor.GetType() == datCallback::NULL_CALLBACK || destructor.GetType() == datCallback::USE_CALL_PARAM) , "add ',0,true' as final parameters of datCallback");
 
    SYS_CS_SYNC( sm_csToken );

    bkBank& newBank = *(rage_new bkBank(name,creator,destructor));
	AddChild(newBank).FinishCreate();
	return newBank;
}



static int s_ActiveButton;
static float s_RepeatTimer;
static float s_RepeatInterval;

void bkManager::Update() {

	// Handle any pending ping reply messages
	if (sm_SendDelayedPingResponse) {
		sm_SendDelayedPingResponse = false;

		bkRemotePacket reply;
		reply.Begin(PING, GetStaticGuid(), this);
		reply.Write_u32(sm_PingResponseMsg);
		reply.Write_bool(true);
		reply.Send();
	}

	bkInvokeDelegate del;
	while(g_bkMgrInvokeQueue->PopPoll(del))
	{
		del.m_Delegate(del.m_UserData);
	}

	bkRemotePacket::ReceivePackets();
	bkGroup::Update();
    bkRemotePacket::SendPackets();

	if (bkIo::GetInstance()) {
		int pressedButtons, buttons;
		float dial = 0;
		bkWidget* groupToPopTo=NULL;

		bkIo::GetInstance()->Input(pressedButtons, buttons, dial);

		if (pressedButtons == bkIo::TRIGGER)
			sm_Active = !sm_Active;

		if (sm_Active) {
			static utimer_t prev = sysTimer::GetTicks();
			utimer_t now = sysTimer::GetTicks();
			utimer_t delta = now - prev;
			prev = now;
			const float timeDelta = delta * sysTimer::GetTicksToSeconds();
			if (pressedButtons) {
				s_ActiveButton = pressedButtons;
				s_RepeatTimer = 0;
				s_RepeatInterval = 0.25f;
			}
			else if (buttons == s_ActiveButton)
				s_RepeatTimer += timeDelta;
			else {
				s_ActiveButton = 0;
				s_RepeatTimer = 0;
			}


			unsigned button = s_ActiveButton;
			
			if (button != bkIo::LEFT)
				sm_ScrollTimeLeft=0.0f;
			else
				sm_ScrollTimeLeft+=timeDelta;

			if (button != bkIo::RIGHT)
				sm_ScrollTimeRight=0.0f;
			else
				sm_ScrollTimeRight+=timeDelta;

			if (s_RepeatTimer && s_RepeatTimer < s_RepeatInterval)
				button = 0;
			else if (s_RepeatTimer > s_RepeatInterval) {
				s_RepeatTimer -= s_RepeatInterval;
				const float minInterval = 0.05f;
				s_RepeatInterval *= 0.80f;
				if (s_RepeatInterval < minInterval)
					s_RepeatInterval = minInterval;
			}


			if (button == bkIo::UP_TO_BANK)
			{
				// find the bank that this widget is contained in:
				bkWidget* currWidget=sm_Focus;
				while (currWidget && currWidget->GetParent()!=this)
					currWidget=currWidget->GetParent();

				groupToPopTo=currWidget;
			}
			else if (button == bkIo::DOWN)
				sm_Cursor++;
			else if (button == bkIo::UP)
				sm_Cursor--;
			else if (sm_Focus && ((button == bkIo::LEFT || button == bkIo::RIGHT) || dial)) {
				if (dial)
					sm_Focus->Message(DIAL,dial);
				else if (button == bkIo::LEFT)
				{
					// Determine how many multiples of scroll time have we elapsed
					int scrollScalar = (int) ((sm_ScrollTimer>0.f) ? (sm_ScrollTimeLeft / sm_ScrollTimer) : 1.f);
					float dialIncr=scrollScalar?sm_ScrollMultiplier * scrollScalar:1.0f;
					// Messagef("dialIncr: %f sm_ScrollTimeLeft: %f sm_ScrollTimer: %f",dialIncr,sm_ScrollTimeLeft,sm_ScrollTimer);
					sm_Focus->Message(DIAL,-dialIncr);
				}
				else
				{
					int scrollScalar = (int) ((sm_ScrollTimer>0.f) ? (sm_ScrollTimeRight / sm_ScrollTimer) : 1.f);
					float dialIncr=scrollScalar?sm_ScrollMultiplier * scrollScalar:1.0f;
					// Messagef("dialIncr: %f sm_ScrollTimeRight: %f sm_ScrollTimer: %f",dialIncr,sm_ScrollTimeRight,sm_ScrollTimer);
					sm_Focus->Message(DIAL,+dialIncr);
				}
			}
		}

		// if the input wants to pop to the top,
		// position widgets showing so that 
		// bank is first line and cursor is pointing
		// to the bank:
		if (groupToPopTo)
		{
			int pos=1; // include the bank itself
			bkWidget *i = m_Child;
			while (i)
			{
				if (i==groupToPopTo)
					break;
				pos+=i->GetNumWidgets(true);
				i = i->GetNext();
			}

			sm_Cursor=pos;
			sm_TopLine=pos;
		}

		if (sm_Cursor < 0)
			sm_Cursor = 0;
		else if (sm_Cursor > sm_MaxCursorUpdate)
			sm_Cursor = sm_MaxCursorUpdate;

		if (sm_Cursor < sm_TopLine)
			sm_TopLine = sm_Cursor;
		if (sm_Cursor >= sm_TopLine + sm_ShownLineCount)
			sm_TopLine = sm_Cursor - sm_ShownLineCount + 1;
	}

	// we assume that at the first update most of the bank 
	// initialization will be complete:
	if ( !sm_initFinished )
	{
		bkRemotePacket p;
		p.Begin(INIT_FINISHED,GetGuid(),this);
		p.Write_s32(m_Open);
		p.Send();
        bkRemotePacket::SendPackets();

		sysIpcSleep(1000);
		bkRemotePacket::ReceivePackets();

		sm_initFinished = true;
	}
}


bool bkManager::IsUsingPad(int padIndex) const {
	if (!bkIo::GetInstance()) 
		return false;
	return sm_Active && (padIndex == -1 || padIndex == bkIo::GetInstance()->GetPadIndex());
}


void bkManager::Draw() {
	if (sm_Active && bkIo::GetInstance()) {
		sm_Focus = 0;
		sm_MaxCursorDraw = 0;
		DrawLocal(0,-sm_TopLine);
		// Drawf(30,0,"Max = %d, Top = %d, Shown = %d",sm_MaxCursor,sm_TopLine,sm_ShownLineCount);
		if (sm_TopLine)
			Drawf(-2,0,"^^");
		if (sm_MaxCursorDraw >= sm_TopLine + sm_ShownLineCount)
			Drawf(-2,sm_ShownLineCount-1,"vv");
		sm_MaxCursorUpdate = sm_MaxCursorDraw;
	}
}


bkBank *bkManager::FindBank(const char *name) const 
{
    SYS_CS_SYNC( sm_csToken );

	bkWidget *i = m_Child;
	while (i) {
		if (!stricmp(i->m_Title,name))
			break;
		i = i->GetNext();
	}
	return (bkBank*) i;
}

bkWidget* bkManager::FindWidget( const char *path ) const
{
    SYS_CS_SYNC( sm_csToken );

    atString strPath( path );
    strPath.Replace( "\\", "/" );

    int indexOf = strPath.IndexOf( '/' );
    if ( indexOf != -1 )
    {
        atString bankName;
        bankName.Set( strPath, 0, indexOf );
        
        bkBank *bank = FindBank( bankName.c_str() );
        if ( bank != NULL )
        {
            atString bankPath;
            bankPath.Set( strPath, indexOf + 1 );

            return FindWidget( bank->m_Child, bankPath.c_str() );
        }
    }    

    return NULL;
}

bkWidget* bkManager::FindWidget( bkWidget *widget, const char *path ) const
{
    atString s( path );
    atArray<atString> split;
    s.Split( split, '/', true );

    if ( widget == NULL )
    {
        return NULL;
    }   

    bkWidget *w = widget;
    while ( w ) 
    {
        if ( stricmp( w->m_Title, split[0].c_str() ) == 0 )
        {
            break;
        }

        w = w->GetNext();
    }

    if ( w != NULL )
    {
        if ( split.GetCount() == 1 )
        {
            return w;
        }

        atString subPath( split[1] );
        for ( int i = 2; i < split.GetCount(); ++i )
        {
            subPath += "/";
            subPath += split[i];
        }

        w = w->m_Child;
        while ( w )
        {
            bkWidget *foundWidget = FindWidget( w, subPath.c_str() );
            if ( foundWidget != NULL )
            {
                return foundWidget;
            }

            w = w->GetNext();
        }
    }

    return NULL;
}

void bkManager::CreateBankManager(const char *appName) {
	bkAssertf(!sm_Instance, "bkManager has already been initted");
	sm_Instance = rage_new bkManager(appName);

	if (bkRemotePacket::IsConnectedToRag())
	{
		diagChannel::SendAllSubChannels();
	}
}



void bkManager::DeleteBankManager() 
{
	// Using delete here instead of calling Destroy is intentional;
	// we don't want the remote host to try to delete its own BANKMGR object.
	// It will know when we're back from the dead when we send across another
	// widget with no parent.
	if ((!bkRemotePacket::IsConnected() || bkRemotePacket::IsServer()) && sm_Instance)
		sm_Instance->Destroy();
	else
		delete sm_Instance;
	sm_Instance = 0;
}


void bkManager::DestroyBank(bkBank &b) 
{
    SYS_CS_SYNC( sm_csToken );

	b.Destroy();

#if __WIN32PC
	if (sm_Instance->GetPane())
		sm_Instance->GetPane()->Resize();
#endif
}


void bkManager::ActivateBank(const char * bankName) const 
{
    SYS_CS_SYNC( sm_csToken );
    
    bkBank *b = FindBank(bankName);
	if (b)
		b->Show();
}


bool bkManager::IsClosedGroup() const {
	return false;
}


void bkManager::SetAppName(const char *appName) {
	s_AppName = appName;
}

const char* bkManager::GetAppName() {
	return s_AppName;
}

bool bkManager::IsBankThread()
{
	return sysThreadType::IsUpdateThread();
}

void bkManager::Invoke(bkInvokeDelegate::DelType del, size_t udata)
{
	bkInvokeDelegate bkdel(del, udata);
	if (Verifyf(g_bkMgrInvokeQueue, "Can't invoke before the bank manager is initialized"))
	{
		if (!g_bkMgrInvokeQueue->IsNotFull())
		{
			Errorf("Invoke Queue is full - too many threads have called Invoke() without a call to BANKMGR.Update() to flush the queue");
		}
		g_bkMgrInvokeQueue->Push(bkdel);
	}
}

#if __WIN32PC
#pragma warning(disable: 4668)
#include <commdlg.h>
#pragma warning(error: 4668)
#endif

const char*	bkManager::OpenFile(const char *mask,bool save,const char *description,const char* registryPrefix)
{
	if (bkRemotePacket::IsConnectedToRag())
	{
		bkDisplayf("bkManager::OpenFile(const char *mask,bool save,const char *description,const char* registryPrefix) not current supported by RAG - send kelley@rockstarsandiego.com if you really need this implemented.");
		return NULL;
	}

#if __WIN32PC
	char			filter[128];
	static char		name[128];
	OPENFILENAME	fileName;
	char			path[128];
	
	char	regKeyName[128];
	HKEY	regKey=0;          // address of buffer for opened handle
	DWORD	disposition; 
	bool useRegistry=false;
	if (registryPrefix && s_AppName)
	{
		formatf(regKeyName,"Software\\AngelStudios\\%s\\%s",s_AppName,registryPrefix);
		if (registryPrefix)
			useRegistry=RegCreateKeyEx(HKEY_CURRENT_USER,regKeyName,0,"",REG_OPTION_NON_VOLATILE,
									   KEY_READ|KEY_WRITE,NULL,&regKey,&disposition)==ERROR_SUCCESS;
	}
	else if (registryPrefix && !s_AppName)
	{
		bkWarningf("You must call bkManager::SetAppName() to save path information in the registry");
	}

	bool queryRegistry=false;
	if (useRegistry)
	{
		unsigned long length=sizeof(path)-1;
		long result=RegQueryValueEx(regKey,"DefaultDirName",0,0,(BYTE*)path,&length);
		queryRegistry=result==ERROR_SUCCESS; 
		length=sizeof(name)-1;
		result=RegQueryValueEx(regKey,"DefaultFileName",0,0,(BYTE*)name,&length);
		queryRegistry=result==ERROR_SUCCESS; 
	}

	if (!queryRegistry)
	{
		// Apparently this one function is picky about pathnames...
		safecpy(path,ASSET.GetPath());
		if (strrchr(name,'.'))
			*strrchr(name,'.') = 0;
	}

	int sl = StringLength(path);
	if (sl && (path[sl-1]=='/' || path[sl-1]=='\\'))
		path[sl-1] = 0;
	while (strchr(path,'/'))
		*strchr(path,'/') = '\\';
	
	formatf(filter,"%s%c%s%cAll Files%c*.*%c%c%c",description?description:mask,0,mask,0,0,0,0,0);
	memset(&fileName,0x0,sizeof(OPENFILENAME));
	fileName.lStructSize=sizeof(OPENFILENAME);
	fileName.hwndOwner=GetActiveWindow(); 
	fileName.lpstrFilter=filter;
	fileName.lpstrFile=name; 
	fileName.lpstrInitialDir=path;
	fileName.nMaxFile=sizeof(name);
	fileName.Flags=OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_NOCHANGEDIR;

	const char* finalName=NULL;

	sysInterlockedIncrement(&fiIsShowingMessageBox);
	if (save)
		finalName=GetSaveFileName(&fileName)?name:NULL;
	else 
		finalName=GetOpenFileName(&fileName)?name:NULL;
	sysInterlockedDecrement(&fiIsShowingMessageBox);
	
	if (useRegistry && finalName)
	{
		char buff[128];
		safecpy(buff,name);
		const char* justFileName=ASSET.FileName(buff);
		RegSetValueEx(regKey, "DefaultFileName", 0, REG_SZ, (BYTE *)justFileName, StringLength(justFileName) + 1);   
		*(char*)justFileName=NULL;
		RegSetValueEx(regKey, "DefaultDirName", 0, REG_SZ, (BYTE *)buff, StringLength(buff) + 1); 
		RegCloseKey(regKey);
	}
	
	return finalName;
#else
	if (bkRemotePacket::IsConnected()) {
		static char name[RAGE_MAX_PATH];
		s_OpenFileCancelled = false;
		s_OpenFileDest = name;
		s_OpenFileDestSize = sizeof(name);
		sysInterlockedIncrement(&fiIsShowingMessageBox);
		bkRemotePacket p;
		p.Begin(OPEN_FILE_2,GetStaticGuid(),this);
		p.Write_const_char(mask);
		p.Write_bool(save);
		p.Write_const_char(description);
		p.Write_const_char(registryPrefix);
		p.Send();
        bkRemotePacket::SendPackets();
		bkDisplayf("%s - Waiting for remote server OpenFile reply",s_AppName);
		while (s_OpenFileDest) {
			bkRemotePacket::ReceivePackets();
			sysIpcSleep(50);
		}
		bkDisplayf("...got reply");
		sysInterlockedDecrement(&fiIsShowingMessageBox);
		return s_OpenFileCancelled ? NULL : (name[0]? name : 0);
	}
	else
		return 0;
#endif
}


bool bkManager::OpenFile(char *dest,int maxDest,const char *mask,bool save,const char *description)
{
	if (bkRemotePacket::IsConnected()) {
		s_OpenFileDest = dest;
		s_OpenFileDestSize = maxDest;
		s_OpenFileCancelled = false;
		bkRemotePacket p;
		p.Begin(OPEN_FILE_1,GetStaticGuid(),this);
		p.Write_const_char(dest);
		p.Write_u32(maxDest);
		p.Write_const_char(mask);
		p.Write_bool(save);
		p.Write_const_char(description);
		p.Send();
        bkRemotePacket::SendPackets();
		bkDisplayf("Waiting for remote server OpenFile reply");
		while (s_OpenFileDest) {
			bkRemotePacket::ReceivePackets();
			sysIpcSleep(50);
		}
		bkDisplayf("...got reply");
		if (!s_OpenFileCancelled && dest[0]!='\0')
		{
			return true;
		}
		else 
			return false;
	}
#if __WIN32PC
	else 
	{
		char			filter[128];
		OPENFILENAME	fileName;

		// Apparently this one function is picky about pathnames...
		int sl = StringLength(dest);
		if (sl && (dest[sl-1]=='/' || dest[sl-1]=='\\'))
			dest[sl-1] = 0;
		while (strchr(dest,'/'))
			*strchr(dest,'/') = '\\';

		if (strcmp(dest,".") == 0)
			GetCurrentDirectory(maxDest,dest);
		/* if (strrchr(dest,'.'))
			*strrchr(dest,'.') = 0; */

		formatf(filter,"%s%c%s%cAll Files%c*.*%c%c%c",description?description:mask,0,mask,0,0,0,0,0);
		memset(&fileName,0x0,sizeof(OPENFILENAME));
		fileName.lStructSize=sizeof(OPENFILENAME);
		fileName.hwndOwner=GetActiveWindow(); 
		fileName.lpstrFilter=filter;
		fileName.lpstrFile=dest; 
		fileName.lpstrInitialDir=dest;
		fileName.nMaxFile=maxDest;
		fileName.Flags=OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_NOCHANGEDIR;

		if (save)
			return GetSaveFileName(&fileName) != 0;
		else 
			return GetOpenFileName(&fileName) != 0;
	}
#else
	else
		return false;
#endif
}


#if __WIN32PC

HWND bkManager::sm_Owner;

PARAM(bankx, "[bank] X position of bank widgets");
PARAM(banky, "[bank] Y position of bank widgets");

void bkManager::WindowCreate() {
	if (!bkRemotePacket::IsConnectedToRag())
	{
		int x = 100;
		int y = 100;
		PARAM_bankx.Get(x);
		PARAM_banky.Get(y);
		m_Pane = rage_new bkPane(sm_Owner,this,m_Title,x,y,true);
	}
}


rageLRESULT bkManager::WindowMessage(rageUINT msg,rageWPARAM /*wParam*/,rageLPARAM /*lParam*/) {
	if (msg == WM_CLOSE && bkRemotePacket::IsServer())
		PostQuitMessage(0);
	return 0;
}


void bkManager::WindowDestroy() 
{
	delete m_Pane;
	m_Pane = NULL;
}


int bkManager::WindowResize(int /*x*/,int y,int /*width*/,int /*height*/) {
	bkErrorf("WindowResize doesn't work");
	return y;
}


bkPane* bkManager::GetPane() {
	return m_Pane;
}

#endif

void bkManager::RaiseAllBanks(bool /*ontop*/) {
#if __WIN32PC
	if (m_Pane)
	{
		BringWindowToTop(m_Pane->GetOuterHandle());
		bkWidget *i = m_Child;
		while (i) {
			if (i->IsOpen() && i->GetPane())
				BringWindowToTop(i->GetPane()->GetOuterHandle());
			i = i->GetNext();
		}
	}
#endif
}


bool bkManager::GetNextMessage(bool 
#if __WIN32PC
							   blockFlag
#endif
							   ) {
#if __WIN32PC
	MSG msg;
	while (!blockFlag ? PeekMessage(&msg,0,0,0,PM_REMOVE) : GetMessage(&msg,0,0,0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
		if (msg.message == WM_QUIT)
			return false;
	}
#endif
	return true;
}


#endif
