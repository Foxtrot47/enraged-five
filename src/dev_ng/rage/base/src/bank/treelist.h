// 
// bank/treelist.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BANK_TREELIST_H
#define BANK_TREELIST_H

#if __BANK

#include "widget.h"

#include "atl/atfunctor.h"

namespace rage
{

class Vector3;

#define DATE_TIME_MINIMUM "1/1/1753"
#define DATE_TIME_MAXIMUM "12/31/9998"

class bkTreeList : public bkWidget
{
    friend class bkGroup;
    friend class bkManager;

public:

    // START: Keep these enums in sync with ragTreeList/WidgetTreeListVisible.cs
    enum ColumnControlType
    {
        ColumnControlTypeNone,            // This column or item is not editable.
        ColumnControlTypeColumnTemplate,  // Use the same ItemControlType to edit all items in this column.
        ColumnControlTypeInPlaceTemplate, // Use a ItemControlType to edit items in this column.  Each item must specify what to use.
        ColumnControlTypeTextBox,         // built-in type
        ColumnControlTypeComboBox,        // built-in type
        ColumnControlTypeDateTimePicker   // built-in type
    };

    enum EditCondition
    {
        EditConditionNone,           // Column will be read-only
        EditConditionSingleClick,    // Activate the item editor with 1 click
        EditConditionDoubleClick,    // Activate the item editor with a double click
        EditConditionAlwaysVisible   // item editor is always visible.  only works with ColumnControlTypeColumnTemplate
    };

    enum NodeDataType
    {
        NodeDataTypeInvalidType,
        NodeDataTypeNative,		// no sub nodes; sub items will be int, float, and/or string
        NodeDataTypeStruct,		// sub nodes will be any combination of natives and vectors.
        NodeDataTypeVector,		// special case struct which will have 3 sub nodes for x, y, and z as floats
        NodeDataTypeThread,		// all sub nodes belong to a thread
    };

    enum ItemControlType
    {
        ItemControlTypeNone,      // When ColumnControlTypeInPlaceTemplate is used on a column, item(s) with this ItemControlType will be read-only.  Invalid with ColumnControlTypeColumnTemplate.
        ItemControlTypeTextBox,
        ItemControlTypeComboBox,
        ItemControlTypeDataTimePicker,
        ItemControlTypeNumericUpDown,
        ItemControlTypeButton,
        ItemControlTypeProgressBar,
        ItemControlTypeSlider
    };

    enum ItemDataType
    {
        ItemDataTypeInvalidType,
        ItemDataTypeFloat,
        ItemDataTypeInt,
        ItemDataTypeString,
        ItemDataTypeBool,
        ItemDataTypeVector
    };

    enum CheckBoxAlign
    {
        CheckBoxAlignNone,  // No check boxes on items in the column
        CheckBoxAlignLeft,
        CheckBoxAlignRight,
        CheckBoxAlignCenter
    };

    enum CheckBoxStyle
    {
        CheckBoxStyleNone,      // No check boxes on items in the column
        CheckBoxStyleTwoState,
        CheckBoxStyleThreeState,
    };

    enum CheckBoxState
    {
        CheckBoxStateUnchecked,
        CheckBoxStateIndeterminate,
        CheckBoxStateChecked
    };

    enum DateTimeFormat
    {
        DateTimeFormatLong,     // Tuesday, July 3, 2006
        DateTimeFormatShort,    // 6/29/2006
        DateTimeFormatTime,     // 12:00:00 AM
    };
    // STOP

public:
    typedef atFunctor3<void, s32, s32, const char*> UpdateItemFuncType;
    typedef atFunctor2<void, s32, s32> ItemCheckedFuncType;
    typedef atFunctor1<void, s32> SpecialFuncType;

    //
    // PURPOSE
    //	Set the functor that gets called when the GUI updates an item.
    //  UpdateItemFuncType receives arguments in this order :
    //	the s32 key, s32 dataType (cast to ItemDataType) and the const char* newValue.
    //  Must call SetItemValue if you want to update the Rag Widget.
    // PARAMS
    //	func - the functor we'll call when an item is updated by the GUI
    //
    void SetUpdateItemFunc( UpdateItemFuncType func );

    //
    // PURPOSE
    //	Set the functor that gets called when the GUI checks/unchecks an item.
    //  ItemCheckedFuncType receives arguments in this order :
    //	the s32 key and s32 checked state (cast to CheckBoxState)
    //  Must call SetItemCheckBox if you want to update the Rag Widget.
    // PARAMS
    //	func - the functor we'll call when an item is updated by the GUI
    //
    void SetItemCheckedFunc( ItemCheckedFuncType func );

    //
    // PURPOSE
    //  Set the functor that gets called when the GUI item is a button
    //  and is clicked.  SpecialFuncType receives arguments in this order:
    //  the s32 key for the button clicked
    // PARAMS
    //	func - the functor we'll call when a button item is clicked on the GUI
    //
    void SetItemButtonFunc( SpecialFuncType func );

    //
    // PURPOSE
    //  Set the functor that gets called when a tree node is dragged and dropped.
    //  At this time, only root nodes can be moved this way.  Both allowDrag
    //  and allowDrop must be true in order to receive this event.  One use of this
    //  function is to allow you to copy a node from one bkTreeList to another.
    // PARAMS
    //	func - the functor we'll call when node is dragged and dropped in the GUI
    //
    void SetDragDropFunc( SpecialFuncType func );

    //
    // PURPOSE
    //  Set the functor that gets called the user deletes a node in the GUI.  allowDelete
    //  must be true in order to receive this event.  You must then call RemoveNode
    //  yourself if you want it actually deleted.
    // PARAMS
    //	func - the functor we'll call when a node is deleted in the GUI
    //
    void SetDeleteNodeFunc( SpecialFuncType func );

    //
    // PURPOSE
    //  Add a read-only column to the tree layout.  Items in this column can only be added with AddReadOnlyItem
    //  or AddTextBoxItem.
    // PARAMS
    //  name - column header name
    //  checkStyle - if not CheckBoxStyleNone, every item in this column that has a checkKey will have a Check Box
    //  checkAlign - where in each item the Check Box appears: left, right or center
    //
    void AddReadOnlyColumn( const char* name, CheckBoxStyle checkStyle=CheckBoxStyleNone, CheckBoxAlign checkAlign=CheckBoxAlignLeft );

    //
    // PURPOSE
    //  Add a column that is a column template to the tree layout.  Items added to this column can only be of same type as itemCtrlType.
    //  For example: ItemControlTypeSlider -> AddSliderItem(...), ItemControlTextBox -> AddTextBoxItem(...), etc.
    // PARAMS
    //  name - column header name
    //  itemCtrlType - type of control for each item in this column
    //  editCond - how the edit control is activated for each item in this column.  Using EditConditionNone is the similar to calling AddReadOnlyColumn.
    //  checkStyle - if not CheckBoxStyleNone, every item in this column that has a checkKey will have a Check Box
    //  checkAlign - where in each item the Check Box appears: left, right or center
    //
    void AddColumnTemplateColumn( const char* name, ItemControlType itemCtrlType, EditCondition editCond=EditConditionDoubleClick, CheckBoxStyle checkStyle=CheckBoxStyleNone, CheckBoxAlign checkAlign=CheckBoxAlignLeft );

    //
    // PURPOSE
    //  Add a column that is an in-place template to the tree layout.  Items can be added to this column using any of the AddItem functions.
    //  Warning: there will be a large memory hit so it is recommended that this is used as little as possible.
    // PARAMS
    //  name - column header name
    //  editCond - how the edit control is activated for each item in this column.  Using EditConditionNone is similar to calling AddReadOnlyColumn.
    //  checkStyle - if not CheckBoxStyleNone, every item in this column that has a checkKey will have a Check Box
    //  checkAlign - where in each item the Check Box appears: left, right or center
    //
    void AddInPlaceTemplateColumn( const char* name, EditCondition editCond=EditConditionDoubleClick, CheckBoxStyle checkStyle=CheckBoxStyleNone, CheckBoxAlign checkAlign=CheckBoxAlignLeft );

    //
    // PURPOSE
    //  Add a column that is all text boxes to the tree layout.  Items can only be added to this column using AddTextBoxItem.
    // PARAMS
    //  name - column header name
    //  editCond - how the edit control is activated for each item in this column.  Using EditConditionNone is similar to calling AddReadOnlyColumn.
    //  checkStyle - if not CheckBoxStyleNone, every item in this column that has a checkKey will have a Check Box
    //  checkAlign - where in each item the Check Box appears: left, right or center
    //
    void AddTextBoxColumn( const char* name, EditCondition editCond=EditConditionDoubleClick, CheckBoxStyle checkStyle=CheckBoxStyleNone, CheckBoxAlign checkAlign=CheckBoxAlignLeft );

    //
    // PURPOSE
    //  Add a column that is all combo boxes to the tree layout.  Items can only be added to this column using AddComboBoxItem.
    // PARAMS
    //  name - column header name
    //  editCond - how the edit control is activated for each item in this column.  Using EditConditionNone is similar to calling AddReadOnlyColumn.
    //  checkStyle - if not CheckBoxStyleNone, every item in this column that has a checkKey will have a Check Box
    //  checkAlign - where in each item the Check Box appears: left, right or center
    //
    void AddComboBoxColumn( const char* name, EditCondition editCond=EditConditionDoubleClick, CheckBoxStyle checkStyle=CheckBoxStyleNone, CheckBoxAlign checkAlign=CheckBoxAlignLeft );
    
    //
    // PURPOSE
    //  Add a column that is all date time pickers to the tree layout.  Items can only be added to this column using AddDateTimePickerItem.
    // PARAMS
    //  name - column header name
    //  editCond - how the edit control is activated for each item in this column.  Using EditConditionNone is similar to calling AddReadOnlyColumn.
    //  checkStyle - if not CheckBoxStyleNone, every item in this column that has a checkKey will have a Check Box
    //  checkAlign - where in each item the Check Box appears: left, right or center
    //
    void AddDateTimePickerColumn( const char* name, EditCondition editCond=EditConditionDoubleClick, CheckBoxStyle checkStyle=CheckBoxStyleNone, CheckBoxAlign checkAlign=CheckBoxAlignLeft );
    
    //
    // PURPOSE
    //  Add a node to define the tree layout.  You will add item to the nodeKey you pass into this function.
    // PARAMS
    //  name - name of the node.  Will only be shown if no items are added to this node
    //  nodeKey - unique identifier for nodes in your TreeList.  Used to add items and hang sub-nodes
    //  dataType - type of the node.  used for display purposes.
    //  parentKey - parent of this node.  -1 to place at the top level
    //  expand - true to start expanded, otherwise the node will start collapsed
    //
    void AddNode( const char *name, int nodeKey, NodeDataType dataType, int parentKey=-1, bool expand=true );

    //
    // PURPOSE
    //  Adds a read-only item.  This item can be added to any column.
    // PARAMS
    //  key - unique identifier for this item in the TreeList
    //  val - value of the item.  this is what is displayed
    //  nodeKey - node that this item should be added to
    //  updateParent - when this item is part of a node that is of type NodeDataTypeStruct or NodeDataTypeVector, we'll update the parent node in the same column with data from all sub-nodes of the same column
    //  checkBoxKey - if a column has check boxes, set to enable the check box on this item and be able to receive messages when the check is changed.
    //  checkState - the initial state of the check box.  the column determines if it is a 2-state or a 3-state check box.
    //
    void AddReadOnlyItem( int key, int val, int nodeKey,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddReadOnlyItem( int key, float val, int nodeKey,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddReadOnlyItem( int key, const char* val, int nodeKey,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddReadOnlyItem( int key, bool val, int nodeKey,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );

    //
    // PURPOSE
    //  Adds a text box item.  You can only call this to add items to columns that were created with AddReadOnlyColumn,
    //  AddTextBoxColumn, AddInPlaceTemplateColumn, or AddColumnTemplateColumn (where its ItemControlType is ItemControlTypeTextBox)
    // PARAMS
    //  key - unique identifier for this item in the TreeList
    //  val - value of the item.  this is what is displayed
    //  nodeKey - node that this item should be added to
    //  readOnly - if a column is editable, set to true if you don't want this particular item edited
    //  updateParent - when this item is part of a node that is of type NodeDataTypeStruct or NodeDataTypeVector, we'll update the parent node in the same column with data from all sub-nodes of the same column
    //  checkBoxKey - if a column has check boxes, set to enable the check box on this item and be able to receive messages when the check is changed.
    //  checkState - the initial state of the check box.  the column determines if it is a 2-state or a 3-state check box.
    //
    void AddTextBoxItem( int key, int val, int nodeKey, bool readOnly=false, 
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddTextBoxItem( int key, float val, int nodeKey, bool readOnly=false,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddTextBoxItem( int key, const char* val, int nodeKey, bool readOnly=false,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddTextBoxItem( int key, bool val, int nodeKey, bool readOnly=false,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddTextBoxItem( int key, const Vector3 &val, int nodeKey, bool readOnly=false,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );

    //
    // PURPOSE
    //  Adds a text box item.  You can only call this to add items to columns that were created with AddReadOnlyColumn,
    //  AddComboBoxColumn, AddInPlaceTemplateColumn, or AddColumnTemplateColumn (where its ItemControlType is ItemControlTypeComboBox)
    // PARAMS
    //  key - unique identifier for this item in the TreeList
    //  val - value of the item.  should be one of the items in comboBoxItems, but doesn't have to be
    //  nodeKey - node that this item should be added to
    //  numComboBoxItems - number of items in comboBoxItems
    //  comboBoxItems - the list of items that appear in the drop-down box
    //  updateParent - when this item is part of a node that is of type NodeDataTypeStruct or NodeDataTypeVector, we'll update the parent node in the same column with data from all sub-nodes of the same column
    //  checkBoxKey - if a column has check boxes, set to enable the check box on this item and be able to receive messages when the check is changed.
    //  checkState - the initial state of the check box.  the column determines if it is a 2-state or a 3-state check box.
    //
    void AddComboBoxItem( int key, int val, int nodeKey, int numComboBoxItems, const int* comboBoxItems,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddComboBoxItem( int key, float val, int nodeKey, int numComboBoxItems, const float* comboBoxItems,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddComboBoxItem( int key, const char* val, int nodeKey, int numComboBoxItems, char** comboBoxItems,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );

    //
    // PURPOSE
    //  Adds a date time picker item.  You can only call this to add items to columns that were created with AddReadOnlyColumn,
    //  AddDateTimePickerColumn, AddInPlaceTemplateColumn, or AddColumnTemplateColumn (where its ItemControlType is ItemControlTypeDateTimePicker)
    // PARAMS
    //  key - unique identifier for this item in the TreeList
    //  val - date or time.
    //  nodeKey - node that this item should be added to
    //  format - the format of val and for display.  See DateTimeFormat enums above for the format descriptions.
    //  min - minimum date or time allowed
    //  max - maximum date or time allowed
    //  readOnly - if a column is editable, set to true if you don't want this particular item edited
    //  updateParent - when this item is part of a node that is of type NodeDataTypeStruct or NodeDataTypeVector, we'll update the parent node in the same column with data from all sub-nodes of the same column
    //  checkBoxKey - if a column has check boxes, set to enable the check box on this item and be able to receive messages when the check is changed.
    //  checkState - the initial state of the check box.  the column determines if it is a 2-state or a 3-state check box.
    //
    void AddDateTimePickerItem( int key, const char* val, int nodeKey, DateTimeFormat format=DateTimeFormatShort, 
        const char* min=DATE_TIME_MINIMUM, const char* max=DATE_TIME_MAXIMUM, bool readOnly=false, 
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );

    //
    // PURPOSE
    //  Adds a numeric up down item.  You can only call this to add items to columns that were created with AddReadOnlyColumn,
    //  AddInPlaceTemplateColumn, or AddColumnTemplateColumn (where its ItemControlType is ItemControlTypeNumericUpDown)
    // PARAMS
    //  key - unique identifier for this item in the TreeList
    //  val - value of the item
    //  nodeKey - node that this item should be added to
    //  min - minimum value that val can have
    //  max - maximum value that val can have
    //  step - how much to increment/decrement when the control's up/down button is pressed
    //  updateParent - when this item is part of a node that is of type NodeDataTypeStruct or NodeDataTypeVector, we'll update the parent node in the same column with data from all sub-nodes of the same column
    //  checkBoxKey - if a column has check boxes, set to enable the check box on this item and be able to receive messages when the check is changed.
    //  checkState - the initial state of the check box.  the column determines if it is a 2-state or a 3-state check box.
    //
    void AddNumericItem( int key, int val, int nodeKey, int min, int max, int step=1,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddNumericItem( int key, float val, int nodeKey, float min, float max, float step=0.1f,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );

    //
    // PURPOSE
    //  Adds a button item.  You can only call this to add items to columns that were created with AddReadOnlyColumn,
    //  AddInPlaceTemplateColumn, or AddColumnTemplateColumn (where its ItemControlType is ItemControlTypeButton)
    // PARAMS
    //  key - unique identifier for this item in the TreeList
    //  val - name of the button
    //  nodeKey - node that this item should be added to
    //
    void AddButtonItem( int key, const char* name, int nodeKey );

    //
    // PURPOSE
    //  Adds a progress bar item.  You can only call this to add items to columns that were created with AddReadOnlyColumn,
    //  AddInPlaceTemplateColumn, or AddColumnTemplateColumn (where its ItemControlType is ItemControlTypeProgressBar)
    // PARAMS
    //  key - unique identifier for this item in the TreeList
    //  val - value of the item
    //  nodeKey - node that this item should be added to
    //  min - minimum value that val can have
    //  max - maximum value that val can have
    //  updateParent - when this item is part of a node that is of type NodeDataTypeStruct or NodeDataTypeVector, we'll update the parent node in the same column with data from all sub-nodes of the same column
    //  checkBoxKey - if a column has check boxes, set to enable the check box on this item and be able to receive messages when the check is changed.
    //  checkState - the initial state of the check box.  the column determines if it is a 2-state or a 3-state check box.
    //
    void AddProgressBarItem( int key, int val, int nodeKey, int min=0, int max=100,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );

    //
    // PURPOSE
    //  Adds a slider item.  You can only call this to add items to columns that were created with AddReadOnlyColumn,
    //  AddInPlaceTemplateColumn, or AddColumnTemplateColumn (where its ItemControlType is ItemControlTypeSlider)
    // PARAMS
    //  key - unique identifier for this item in the TreeList
    //  val - value of the item
    //  nodeKey - node that this item should be added to
    //  min - minimum value that val can have
    //  max - maximum value that val can have
    //  updateParent - when this item is part of a node that is of type NodeDataTypeStruct or NodeDataTypeVector, we'll update the parent node in the same column with data from all sub-nodes of the same column
    //  checkBoxKey - if a column has check boxes, set to enable the check box on this item and be able to receive messages when the check is changed.
    //  checkState - the initial state of the check box.  the column determines if it is a 2-state or a 3-state check box.
    //
    void AddSliderItem( int key, int val, int nodeKey, int min=0, int max=100,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );
    void AddSliderItem( int key, float val, int nodeKey, float min=0.0f, float max=100.0f,
        bool updateParent=false, int checkBoxKey=-1, CheckBoxState checkState=CheckBoxStateIndeterminate );

    //
    // PURPOSE
    //  Remove a Node (and all of its items and sub-nodes) from the TreeList
    // PARAMS
    //  nodeKey - unique identifier of the node to remove
    // 
    void RemoveNode( int nodeKey );

    //
    // PURPOSE
    //  Remove an Item from the TreeList
    // PARAMS
    //  key - unique identifier of the item to remove
    // 
    void RemoveItem( int key );

    //
    // PURPOSE
    //  Sets the value of an item
    // PARAMS
    //  key - unique identifier of the item to change
    //  newVal - new value to apply to the key
    //
    void SetItemValue( int key, int newVal );
    void SetItemValue( int key, float newVal );
    void SetItemValue( int key, const char* newVal );
    void SetItemValue( int key, bool newVal );
    void SetItemValue( int key, const Vector3 &newVal );

    //
    // PURPOSE
    //  Set the state of an item's checkbox
    // PARAMS
    //  checkBoxKey - unique identifier of the checkBox
    //  state - new state to give the check box
    //
    void SetItemCheckBox( int checkBoxKey, CheckBoxState state );

protected:
    bkTreeList(const char *title,const char *memo,bool allowDrag,bool allowDrop,bool allowDelete,bool showRootLines=true, const char *fillColor=NULL, bool readOnly=false);
    ~bkTreeList();

    void AddColumn( const char* name, ColumnControlType colCtrlType, ItemControlType itemCtrlType, EditCondition editCond, CheckBoxStyle checkStyle, CheckBoxAlign checkAlign );
    void AddItem( int key, const char *val, ItemDataType dataType, int nodeKey, ItemControlType itemCtrlType, 
        bool readOnly, bool updateParent, int checkBoxKey, CheckBoxState checkState, int numComboBoxItems, char **comboBoxItems );

    static int GetStaticGuid() { return BKGUID('t','l','s','t'); }
    int GetGuid() const;

    int DrawLocal(int x,int y);

    void Update();
    static void RemoteHandler(const bkRemotePacket& p);

    void RemoteCreate();
    void RemoteUpdate();

#if __WIN32PC
    void WindowCreate();
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM) { return 0; }
#endif

    UpdateItemFuncType m_UpdateItemFunc;
    ItemCheckedFuncType m_ItemCheckedFunc;
    SpecialFuncType m_ItemButtonFunc;
    SpecialFuncType m_DragDropFunc;
    SpecialFuncType m_DeleteNodeFunc;

private:
    bkTreeList();
    const bkTreeList& operator=(bkTreeList&);

    bool m_allowDrag;
    bool m_allowDrop;
    bool m_allowDelete;
    bool m_showRootLines;
};

inline void bkTreeList::SetUpdateItemFunc( UpdateItemFuncType func )
{
    m_UpdateItemFunc = func;
}

inline void bkTreeList::SetItemCheckedFunc( ItemCheckedFuncType func )
{
    m_ItemCheckedFunc = func;
}

inline void bkTreeList::SetItemButtonFunc( SpecialFuncType func )
{
    m_ItemButtonFunc = func;
}

inline void bkTreeList::SetDragDropFunc( SpecialFuncType func )
{
    m_DragDropFunc = func;
}

inline void bkTreeList::SetDeleteNodeFunc( SpecialFuncType func )
{
    m_DeleteNodeFunc = func;
}

} // namespace rage

#endif // __BANK

#endif // BANK_TREELIST_H
