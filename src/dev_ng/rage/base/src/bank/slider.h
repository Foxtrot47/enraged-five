//
// bank/slider.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_SLIDER_H
#define BANK_SLIDER_H

#include "packet.h"
#include "widget.h"

#include "file/remote.h"
#include "math/amath.h"
#include "system/param.h"
#include "system/stack.h"

#include <float.h>
#include <limits>

#if __BANK
namespace rage {

class Float16;
class ScalarV;

class bkSlider: public bkWidget {
public:
#if __WIN32PC
	//
	// PURPOSE
	//	checks to see if a string in the edit control is allowed
	// PARAMS
	//	string - the string to check
	//	okOnly - if true, the entire string must be valid, otherwise 
	//  the partial string may be valid
	// RETURNS
	//	returns true if the string is valid, false if not
	//
	bool ValidateString(const char* string,bool okOnly) const;
	//
	// PURPOSE
	//	sets if the font used for the edit window is bold or not
	// PARAMS
	//	bold - true if the font should be bold, false if not
	//
	void SetFont(bool bold);
	//
	// PURPOSE
	//	processes a key pressed in the slider's edit box
	// PARAMS
	//	key - the keyboard key to check
	// RETURNS
	//	returns true if the key entered in the edit box 
	//  and the current string in that box create a valid string
	//
	bool CheckKey(char key) const;
#endif

	//
	// PURPOSE
	//  gets the step value
	// RETURNS
	//  the step value
	//
	virtual float GetStep() const = 0;

	//
	// PURPOSE
	//  set the step value
	// PARAMS
	//  step - if step is 0.0f, the widget becomes read-only
	//
	virtual void SetStep( float step ) = 0;

	//
	// PURPOSE
	//	gets a string representation of the slider widget
	// PARAMS
	//	dest - the character string 
	//  destSize - the length of the character buffer
	// RETURNS
	//	returns a pointer to dest
	//
	virtual const char* GetString(char *dest, int destSize) = 0;
	//
	// PURPOSE
	//	sets a the value in the slider widget from a character string
	// PARAMS
	//	dest - a pointer to the string that will set the value in the slider
	// RETURNS
	//	returns true if the string is within the slider's limits, false if not
	//
	virtual bool SetString(const char *dest) = 0;

	//
	// PURPOSE
	//	sets the minimum and maximum range of the slider widget
	// PARAMS
	//	 min - the minimum value for the range
	//	 max - the maximum value for the range
	//
	virtual void SetRange(float min,float max) = 0;
	
	// PURPOSE: sets the this widget as the widget that has focus
	// RETURNS: returns true if successful
	bool SetFocus();

	// PURPOSE: Accessor for the minimum allowed value by the float sliders
	// RETURNS: returns the minimum allowed value by the float sliders
	static float GetFloatMinValue();

	// PURPOSE: Accessor for the maximum allowed value by the float sliders
	// RETURNS: returns the maximum allowed value by the float sliders
	static float GetFloatMaxValue();

	// PURPOSE: Minimum value of floating point slider
	static const float FLOAT_MIN_VALUE;

	// PURPOSE: Maximum value of floating point slider
	static const float FLOAT_MAX_VALUE;

	static float GetScale(const char *tip);

    // PURPOSE: Retrieves a string representation of the current value of this widget.
    // PARAMS:
    //    buf - the buffer to write the string representation to.
    //    bufLen - the length of the buffer.
    virtual void GetStringRepr( char *buf, int bufLen );

    // PURPOSE: Sets the current value of this widget using the string representation of the new value.
    // PARAMS:
    //    buf - the new value.
    virtual void SetStringRepr( const char *buf );

protected:
	bkSlider(datCallback &callback,const char *title,const char *memo, const char *fillColor=NULL, bool exponential=false, bool readOnly=false);
	~bkSlider();

	virtual void SetAddr(void *addr) = 0;

	virtual bool CanBeNegative() const = 0;
	virtual bool CanBeFloatingPoint() const = 0;

protected:
#if __WIN32PC
	void WindowCreate();
	void WindowUpdate();
	void WindowDestroy();
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM);
	int WindowResize(int x,int y,int width,int height);
	struct HWND__* m_Button;
	struct HWND__* m_Edit;
#endif
	bool m_Exponential;
};

inline float bkSlider::GetFloatMinValue()
{
	return FLOAT_MIN_VALUE;
}

inline float bkSlider::GetFloatMaxValue()
{
	return FLOAT_MAX_VALUE;
}

template<typename _Type>
class bkSliderVal: public bkSlider {
protected:
	friend class bkGroup;
	friend class bkManager;
	bkSliderVal(datCallback &callback,const char *title,const char *memo,_Type *data,_Type mini,_Type maxi,_Type step, const char* fillColor=NULL, bool exponential=false, bool readOnly=false);
	~bkSliderVal();

	void Message(Action action,float value);
	int DrawLocal(int x,int y);


	static inline int GetStaticGuid();
	int GetGuid() const;
	void Update();

	static void RemoteHandler(const bkRemotePacket& packet);

	void SetAddr(void *addr);

	float GetStep() const;
	void SetStep(float step);
	void SetRange(float mini,float maxi);

	virtual bool CanBeNegative() const;
	virtual bool CanBeFloatingPoint() const;

	static _Type GetRangeEpsilon();

	static const char* GetFormatString();

	static const char* GetDrawString();

private: 
	const char *GetString(char *dest, int destSize);
	bool SetString(const char *src);
	void RemoteCreate();
	void RemoteUpdate();
	_Type *m_Value, m_PrevValue, m_Mini, m_Maxi, m_Step; 

public: 
	_Type GetValue() const;
	void SetValue(_Type value);
};

// Specialization for a half-precision float built on standard float implementation.
// Unfortunately I have to use the base-from-member idiom to initialize the fake
// float value before constructing the base class.
struct bkSliderValFloat16Helper 
{ 
	bkSliderValFloat16Helper(float value) : m_FakeValue(value) { }

	float m_FakeValue; 
};

template<>
class bkSliderVal<Float16>: private bkSliderValFloat16Helper, public bkSliderVal<float> {
private:

protected:
	friend class bkGroup;
	friend class bkManager;
	bkSliderVal(datCallback &callback,const char *title,const char *memo,Float16 *data,float mini,float maxi,float step, const char* fillColor=NULL,bool exponential=false, bool readOnly=false);

	void Update();
	void Changed();

private: 
	Float16* m_RealValue;
};

struct bkSliderValScalarVHelper 
{ 
	bkSliderValScalarVHelper(float value) : m_FakeValue(value) { }

	float m_FakeValue; 
};

template<>
class bkSliderVal<ScalarV>: private bkSliderValScalarVHelper, public bkSliderVal<float> {
private:

protected:
	friend class bkGroup;
	friend class bkManager;
	bkSliderVal(datCallback &callback,const char *title,const char *memo,ScalarV *data,float mini,float maxi,float step, const char* fillColor=NULL,bool exponential=false, bool readOnly=false);

	void Update();
	void Changed();

private: 
	ScalarV* m_RealValue;
};

// General implementations
template<typename _Type> inline bkSliderVal<_Type>::bkSliderVal(datCallback &callback,const char *title,const char *memo,_Type *data,_Type mini,_Type maxi,_Type step,const char* fillColor,bool exponential,bool readOnly)
: bkSlider(callback,title,memo,fillColor,exponential,readOnly)
, m_Value(data)
, m_PrevValue(*data)
, m_Mini(mini)
, m_Maxi(maxi)
, m_Step(step)
{ 
	char buffer[256];
	if (CanBeFloatingPoint() && (m_Mini<(FLOAT_MIN_VALUE) || m_Maxi>(FLOAT_MAX_VALUE)))
	{
		formatf(buffer,sizeof(buffer),"Widget '%s' min and max values (%.3f,%.3f) are out of allowed range.\n\nClick Ok to clamp it and continue, or Cancel to exit.",title,(float)mini,(float)maxi); 
		bkErrorf("%s",buffer); 
		sysStack::PrintStackTrace();
		int result = fiRemoteShowMessageBox(buffer,sysParam::GetProgramName(),MB_ICONERROR | MB_OKCANCEL | MB_TOPMOST, IDOK); 
		if (result == IDOK) { 
			m_Mini = Clamp(m_Mini,(_Type)(FLOAT_MIN_VALUE),(_Type)(FLOAT_MAX_VALUE)); 
			m_Maxi = Clamp(m_Maxi,(_Type)(FLOAT_MIN_VALUE),(_Type)(FLOAT_MAX_VALUE)); 
		} 
		else
		{
			Quitf("User-requested abort."); 
		}
	} 
	if (m_Step && !(*m_Value >= m_Mini - GetRangeEpsilon() && *m_Value <= m_Maxi + GetRangeEpsilon())) { 
		formatf(buffer,sizeof(buffer),"Widget '%s' initial value %.3f out of allowed range (%.3f,%.3f).\n\nClick Ok to clamp it and continue, or Cancel to exit.",title,(float)*data,(float)mini,(float)maxi); 
		bkErrorf("%s",buffer); 
		sysStack::PrintStackTrace();
		int result = fiRemoteShowMessageBox(buffer,sysParam::GetProgramName(),MB_ICONERROR | MB_OKCANCEL | MB_TOPMOST, IDOK); 
		if (result == IDOK) {
			*m_Value = (_Type) Clamp(*m_Value,m_Mini,m_Maxi);
		}
		else {
			Quitf("User-requested abort."); 
		}
	} 
}

template<typename _Type> bkSliderVal<_Type>::~bkSliderVal()
{
	if (bkRemotePacket::IsServer()) {
		delete m_Value;
	}
}

template<typename _Type> void bkSliderVal<_Type>::Message(Action action,float value)
{
	if (action == DIAL && m_Step)
	{
		_Type newValue;
		if (!m_Exponential)
		{
			// Change the value linearly (default).
			newValue = (_Type)(*m_Value + value * m_Step);
		}
		else
		{
			// Change the value exponentially (the linear change times the current value).
			newValue = (_Type)(*m_Value * (1.0f + value * m_Step));
		}

		newValue = Clamp(newValue,m_Mini,m_Maxi); 
		if (newValue != *m_Value) { 
			*m_Value = newValue; 
			WindowUpdate(); 
			Changed(); 
		} 
	} 
}

template<typename _Type> int bkSliderVal<_Type>::DrawLocal(int x, int y)
{
	Drawf(x,y,GetDrawString(),m_Title,(_Type)(*m_Value * bkSlider::GetScale(GetTooltip()))); 
	return bkWidget::DrawLocal(x,y);
}

template<typename _Type> int bkSliderVal<_Type>::GetGuid() const
{
	return GetStaticGuid();
}

template<typename _Type> void bkSliderVal<_Type>::Update()
{
	if (*m_Value != m_PrevValue) { 
		m_PrevValue = *m_Value; 
		FinishUpdate(); 
	}
}

template<typename _Type> void bkSliderVal<_Type>::RemoteHandler(const bkRemotePacket& packet)
{
	if (packet.GetCommand() == bkRemotePacket::CREATE)
	{
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;
		const char *title = packet.Read_const_char();
		const char *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();
		_Type *data = rage_new _Type;
		packet.ReadValue(*data);
		_Type mini, maxi, step;
		packet.ReadValue(mini);
		packet.ReadValue(maxi);
		packet.ReadValue(step);
		packet.End();
		bkSliderVal<_Type>& widget = *(rage_new bkSliderVal<_Type>(NullCB,title,memo,data,mini,maxi,step,fillColor,readOnly));
		bkRemotePacket::SetWidgetId(widget, id);
		parent->AddChild(widget);
	}
	else if (packet.GetCommand() == bkRemotePacket::CHANGED) 
	{
		packet.Begin();
		bkSliderVal<_Type>* widget = packet.ReadWidget<bkSliderVal<_Type> >();
		if (!widget) return;
		packet.ReadValue(*widget->m_Value);
		widget->m_PrevValue = *widget->m_Value;
		packet.End();
		widget->WindowUpdate();
		widget->Changed();
	}
}

template<typename _Type> void bkSliderVal<_Type>::SetAddr(void* addr) { 
	m_Value = (_Type*) addr; 
}

template<typename _Type> float bkSliderVal<_Type>::GetStep() const
{
	return (float)m_Step;
}

template<typename _Type> void bkSliderVal<_Type>::SetStep(float step)
{
	m_Step = (_Type) step;

	if ( !bkRemotePacket::IsConnected() )
	{
		return;
	}

	bkRemotePacket p;
	p.Begin( bkRemotePacket::USER+1, GetGuid(), this );
	p.WriteValue( m_Step );
	p.Send();
}

template<typename _Type> void bkSliderVal<_Type>::SetRange(float mini,float maxi)
{
	m_Mini = (_Type) mini; 
	m_Maxi = (_Type) maxi;

	if ( !bkRemotePacket::IsConnected() )
	{
		return;
	}

	// In case the value and the range changed at the "same time" we normally wouldn't send a 
	// value changed message until the next frame, so rag would see an old value that might be outside the new range.
	// So force update the value first, then set the range.
	RemoteUpdate();

	bkRemotePacket p;
	p.Begin( bkRemotePacket::USER+0, GetStaticGuid(), this );
	p.WriteValue( m_Mini );
	p.WriteValue( m_Maxi );
	p.Send();
}

template<typename _Type> bool bkSliderVal<_Type>::CanBeNegative() const
{
	return std::numeric_limits<_Type>::is_signed;
}

template<typename _Type> bool bkSliderVal<_Type>::CanBeFloatingPoint() const
{
	return !std::numeric_limits<_Type>::is_integer;
}

template<typename _Type> _Type bkSliderVal<_Type>::GetRangeEpsilon()
{
	return (_Type)(0);
}

template<typename _Type> const char* bkSliderVal<_Type>::GetFormatString()
{
	return "%d";
}

template<typename _Type> const char* bkSliderVal<_Type>::GetDrawString()
{
	return "%s: %d";
}

template<typename _Type> const char* bkSliderVal<_Type>::GetString(char *dest, int destSize) {
	formatf(dest,destSize, GetFormatString(),(_Type)(*m_Value * GetScale(GetTooltip()))); return dest;
}

template<typename _Type> bool bkSliderVal<_Type>::SetString(const char *src) {
	_Type temp = (_Type) (atof(src) / GetScale(GetTooltip())); 
	if (temp >= m_Mini && temp <= m_Maxi) { 
		*m_Value = temp; 
		return true; 
	} 
	else {
		return false;
	} 
}

template<typename _Type> void bkSliderVal<_Type>::RemoteCreate()
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetStaticGuid(),this);
	p.WriteWidget(m_Parent);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char( GetFillColor() );
	p.Write_bool(IsReadOnly());
	p.WriteValue(*m_Value);
	p.WriteValue(m_Mini);
	p.WriteValue(m_Maxi);
	p.WriteValue(m_Step);
	p.Write_bool(m_Exponential);
	p.Send();
}

template<typename _Type> void bkSliderVal<_Type>::RemoteUpdate()
{
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin(bkRemotePacket::CHANGED,GetStaticGuid(),this);
	p.WriteValue(*m_Value);
	p.Send();
}

template<typename _Type> _Type bkSliderVal<_Type>::GetValue() const {
	return *m_Value; 
} 

template<typename _Type> void bkSliderVal<_Type>::SetValue(_Type value) { 
	*m_Value = value; 
	Changed();
} 

// Specializations for the particular template instances we're using
template<> inline float bkSliderVal<float>::GetRangeEpsilon(){return 0.1f;}
template<> inline const char* bkSliderVal<float>::GetFormatString() {return HACK_GTA4?	"%1.4f"		: "%1.3f"		;}
template<> inline const char* bkSliderVal<float>::GetDrawString()	{return HACK_GTA4?	"%s: %1.4f"	: "%s: %1.3f"	;}

template<> inline int bkSliderVal<float>::GetStaticGuid() { return BKGUID('s','l','f','l'); }
template<> inline int bkSliderVal<u8>	::GetStaticGuid() { return BKGUID('s','l','u','8'); }
template<> inline int bkSliderVal<s8>	::GetStaticGuid() { return BKGUID('s','l','s','8'); }
template<> inline int bkSliderVal<u16>	::GetStaticGuid() { return BKGUID('s','l','u','1'); }
template<> inline int bkSliderVal<s16>	::GetStaticGuid() { return BKGUID('s','l','s','1'); }
template<> inline int bkSliderVal<u32>	::GetStaticGuid() { return BKGUID('s','l','u','3'); }
template<> inline int bkSliderVal<s32>	::GetStaticGuid() { return BKGUID('s','l','s','3'); }

}	// namespace rage

#endif

#endif
