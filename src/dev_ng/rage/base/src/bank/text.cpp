//
// bank/text.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "text.h"

#include "packet.h"
#include "pane.h"

#include "string/string.h"

using namespace rage;

const int MAX_STRING_PAYLOAD_LENGTH = sysPipePacket::DATA_SIZE - 11;
const int MAX_HASH_STRING_LENGTH = 80;
CompileTimeAssert(MAX_HASH_STRING_LENGTH <= MAX_STRING_PAYLOAD_LENGTH);

bkText::bkText( datCallback &callback, const char *title, const char *memo, bool *data, bool readOnly, const char *fillColor ) 
: bkWidget(callback,title,memo,fillColor,readOnly)
{
	m_dataType = BOOL;
	m_data.b = data;
	m_prevData.b = rage_new bool;
	*(m_prevData.b) = *data;

#if __WIN32PC
	m_Label = 0;
#endif
}

bkText::bkText( datCallback &callback, const char *title, const char *memo, float *data, bool readOnly, const char *fillColor ) 
: bkWidget(callback,title,memo,fillColor,readOnly)
{
	m_dataType = FLOAT;
	m_data.f = data;
	m_prevData.f = rage_new float;
	*(m_prevData.f) = *data;

#if __WIN32PC
	m_Label = 0;
#endif
}

bkText::bkText( datCallback &callback, const char *title, const char *memo, int *data, bool readOnly, const char *fillColor ) 
: bkWidget(callback,title,memo,fillColor,readOnly)
{
	m_dataType = INT;
	m_data.i = data;
	m_prevData.i = rage_new int;
	*(m_prevData.i) = *data;

#if __WIN32PC
	m_Label = 0;
#endif
}

bkText::bkText( datCallback &callback, const char *title, const char *memo, long *data, bool readOnly, const char *fillColor ) 
	: bkWidget(callback,title,memo,fillColor,readOnly)
{
	m_dataType = LONG;
	m_data.l = data;
	m_prevData.l = rage_new long;
	*(m_prevData.l) = *data;

#if __WIN32PC
	m_Label = 0;
#endif
}

bkText::bkText( datCallback &callback, const char *title, const char *memo, char *data, int dataSize, bool readOnly, const char *fillColor ) 
: bkWidget(callback,title,memo,fillColor,readOnly), 
m_StringSize(dataSize)
{
	m_dataType = STRING;
	m_data.s = data;
	if ( m_StringSize < StringLength(m_data.s)+1 )
	{
		bkErrorf( "bkText '%s' initial string '%s' is longer than buffer size of %d", m_Title, m_data.s, m_StringSize );
		m_StringSize = StringLength(m_data.s) + 1;
	}

	m_prevData.s = rage_new char[m_StringSize];
	safecpy( m_prevData.s, m_data.s, m_StringSize );

#if __WIN32PC
	m_Label = 0;
#endif
}

bkText::bkText( datCallback &callback, const char *title, const char *memo, atHashString *data, bool readOnly, const char *fillColor ) 
: bkWidget(callback,title,memo,fillColor,readOnly)
{
	m_dataType = ATHASHSTRING;
	m_data.h = data;
	m_prevData.h = rage_new atHashString;
	*(m_prevData.h) = *data;

	// Set m_StringSize to the largest size we can receive in a single packet.
	// (we could add more, but then receiving messages gets more complicated - and is probably unnecessary)
	m_StringSize = MAX_HASH_STRING_LENGTH;

	if ((u32)m_StringSize < m_data.h->GetLength() + 1)
	{
		bkErrorf( "bkText '%s' initial string '%s' is longer than buffer size of %d", m_Title, m_data.h->GetCStr(), m_StringSize );
		m_StringSize = m_data.h->GetLength() + 1;
	}

#if __WIN32PC
	m_Label = 0;
#endif
}

bkText::bkText( datCallback &callback, const char *title, const char *memo, atFinalHashString *data, bool readOnly, const char *fillColor ) 
: bkWidget(callback,title,memo,fillColor,readOnly)
{
	m_dataType = ATFINALHASHSTRING;
	m_data.fh = data;
	m_prevData.fh = rage_new atFinalHashString;
	*(m_prevData.fh) = *data;

	// Set m_StringSize to the largest size we can receive in a single packet.
	// (we could add more, but then receiving messages gets more complicated - and is probably unnecessary)
	m_StringSize = MAX_HASH_STRING_LENGTH;

	if ((u32)m_StringSize < m_data.fh->GetLength() + 1)
	{
		bkErrorf( "bkText '%s' initial string '%s' is longer than buffer size of %d", m_Title, m_data.fh->GetCStr(), m_StringSize );
		m_StringSize = m_data.fh->GetLength() + 1;
	}

#if __WIN32PC
	m_Label = 0;
#endif
}

bkText::~bkText() 
{
	switch ( m_dataType )
	{
	case BOOL:
		if (bkRemotePacket::IsServer()) delete [] m_data.b;
		delete m_prevData.b;
		break;
	case FLOAT:
		if (bkRemotePacket::IsServer()) delete [] m_data.f;
		delete m_prevData.f;
		break;
	case INT:
		if (bkRemotePacket::IsServer()) delete [] m_data.i;
		delete m_prevData.i;
		break;
	case LONG:
		if (bkRemotePacket::IsServer()) delete [] m_data.l;
		delete m_prevData.l;
		break;
	case STRING:
		if (bkRemotePacket::IsServer()) delete [] m_data.s;
		delete [] m_prevData.s;
		break;
	case ATHASHSTRING:
		if (bkRemotePacket::IsServer()) delete [] m_data.h;
		delete m_prevData.h;
		break;
	case ATFINALHASHSTRING:
		if (bkRemotePacket::IsServer()) delete [] m_data.fh;
		delete m_prevData.fh;
		break;
	}
}


int bkText::GetGuid() const 
{ 
	return GetStaticGuid(); 
}


int bkText::DrawLocal( int x, int y ) 
{
	switch ( m_dataType )
	{
	case BOOL:
		Drawf( x, y, "%s: [%s]", m_Title, *(m_data.b) ? "true" : "false" );
		break;
	case FLOAT:
		Drawf( x, y, "%s: [%f]", m_Title, *(m_data.f) );
		break;
	case INT:
		Drawf( x, y, "%s: [%d]", m_Title, *(m_data.i) );
		break;
	case LONG:
		Drawf( x, y, "%s: [%ld]", m_Title, *(m_data.l) );
		break;
	case STRING:
		Drawf( x, y, "%s: [%s]", m_Title, m_data.s );
		break;
	case ATHASHSTRING:
	case ATFINALHASHSTRING:
		Drawf( x, y, "%s: [%s]", m_Title, GetHashStringCStr() );
		break;
	}

	return bkWidget::DrawLocal( x, y );
}


void bkText::RemoteCreate() 
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin( bkRemotePacket::CREATE, GetStaticGuid(), this );
	p.WriteWidget( m_Parent );
	p.Write_const_char( m_Title );
	p.Write_const_char( GetTooltip() );
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );

	EDataType effectiveDataType = m_dataType;
	if ( m_dataType == ATHASHSTRING || m_dataType == ATFINALHASHSTRING )
	{
		// As far as the remote connection is concerned, this is just a regular old string.
		effectiveDataType = STRING;
	}

	p.Write_s32( effectiveDataType );

	if ( effectiveDataType == STRING )
	{
		p.Write_s32( m_StringSize );
	}

	p.Send();

	// this will send over the data
	RemoteUpdate();
}


void bkText::RemoteUpdate() 
{
	if ( !bkRemotePacket::IsConnected() )
	{
		return;
	}

	bkRemotePacket p;

	u16 offset = 0;
	u16 maxPacketSize = MAX_STRING_PAYLOAD_LENGTH;
	s16 packetsRemaining = 0;
	if ( m_dataType == STRING )
	{
		packetsRemaining = (s16)( m_StringSize / maxPacketSize );
	}

	while ( packetsRemaining >= 0 )
	{
		u16 count = 0;
		if ( m_dataType == STRING || m_dataType == ATHASHSTRING || m_dataType == ATFINALHASHSTRING)
		{
			count = (u16)( (m_StringSize - offset > maxPacketSize) ? maxPacketSize : (m_StringSize - offset) );
		}

		p.Begin( bkRemotePacket::CHANGED, GetStaticGuid(), this );
		p.Write_u16( count );
		p.Write_u16( offset );
		p.Write_s16( packetsRemaining );

		switch ( m_dataType )
		{
		case BOOL:
			p.Write_bool( *(m_data.b) );
			break;
		case FLOAT:
			p.Write_float( *(m_data.f) );
			break;
		case INT:
			p.Write_s32( *(m_data.i) );
			break;
		case LONG:
			p.Write_s64( *(m_data.l) );
			break;
		case STRING:
			{
				for ( int i = 0; i < count; ++i )
				{
					p.Write_u8( m_data.s[offset + i] );
				}
			}
			break;
		case ATHASHSTRING:
		case ATFINALHASHSTRING:
			{
				const char* stringData = TryGetHashStringCStr();
				int len = 0;
				if (stringData)
				{
					while(*stringData)
					{
						p.Write_u8(*stringData);
						++stringData;
						++len;
					}
				}
				for(int i = len; i < count; i++)
				{
					p.Write_u8(0);
				}
			}
			break;
		}

		p.Send();

		offset = offset + count;
		--packetsRemaining;
	}
}


void bkText::RemoteHandler(const bkRemotePacket& packet) 
{
	if ( packet.GetCommand() == bkRemotePacket::CREATE )
	{
		packet.Begin();

		u32 newWidgetId = packet.GetId();
		bkAssertf( newWidgetId != bkRemoteWidgetMap::INVALID_ID, "Got an invalid widget ID" );
		bkWidget *parent = packet.ReadWidget<bkWidget>();

		const char *title = packet.Read_const_char();
		const char *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();

		bkText* widget = NULL;

		EDataType dataType = (EDataType)packet.Read_s32();
		switch ( dataType )
		{
		case BOOL:
			{
				bool init_data = false; //packet.Read_bool();
				bool* data = rage_new bool;
				*data = init_data;

				widget = rage_new bkText( NullCB, title, memo, data, readOnly, fillColor );
			}
			break;
		case FLOAT:
			{
				float init_data = 0.0f; //packet.Read_float();
				float* data = rage_new float;
				*data = init_data;

				widget = rage_new bkText( NullCB, title, memo, data, readOnly, fillColor );
			}
			break;
		case INT:
			{
				int init_data = 0; //packet.Read_s32();
				int* data = rage_new int;
				*data = init_data;

				widget = rage_new bkText( NullCB, title, memo, data, readOnly, fillColor );
			}
			break;
		case LONG:
			{
				long init_data = 0; //packet.Read_s64();
				long* data = rage_new long;
				*data = init_data;

				widget = rage_new bkText( NullCB, title, memo, data, readOnly, fillColor );
			}
			break;
		case ATHASHSTRING:
		case ATFINALHASHSTRING:
			bkErrorf("Shouldn't create a remote atHashString widget - create a regular string instead");
			// Intentionally FALL THROUGH to STRING case:
		case STRING:
			{
				const char *init_data = ""; //packet.Read_const_char();
				int stringSize = packet.Read_s32();
				char *data = rage_new char[stringSize];
				safecpy(data,init_data,stringSize);

				widget = rage_new bkText( NullCB, title, memo, data, stringSize, readOnly, fillColor );
			}
			break;
		}


		packet.End();

		if ( widget != NULL )
		{
			parent->AddChild( *widget );
			bkRemotePacket::SetWidgetId( *widget, newWidgetId );
		}
	}
	else if ( packet.GetCommand() == bkRemotePacket::CHANGED )
	{
		packet.Begin();
		bkText* widget = packet.ReadWidget<bkText>();
		if ( !widget || widget->IsReadOnly() )
		{
			return;
		}

		u16 count = packet.Read_u16();
		u16 offset = packet.Read_u16();
		s16 packetsRemaining = packet.Read_s16();

		switch ( widget->m_dataType )
		{
		case BOOL:
			{
				bool data = packet.Read_bool();
				*(widget->m_data.b) = data;
				*(widget->m_prevData.b) = data;
			}
			break;
		case FLOAT:
			{
				float data = packet.Read_float();
				*(widget->m_data.f) = data;
				*(widget->m_prevData.f) = data;
			}
			break;
		case INT:
			{
				int data = packet.Read_s32();
				*(widget->m_data.i) = data;
				*(widget->m_prevData.i) = data;
			}
			break;
		case LONG:
			{
				long data = (long)packet.Read_s64();
				*(widget->m_data.l) = data;
				*(widget->m_prevData.l) = data;
			}
			break;
		case STRING:
			{
				for ( int i = offset; (i < offset + count) && (i < widget->m_StringSize); ++i )
				{
					u8 data = packet.Read_u8();
					widget->m_data.s[i] = data;
					widget->m_prevData.s[i] = data;
				}
			}
			break;
		case ATHASHSTRING:
			{
				bkAssertf(offset == 0 && count <= MAX_HASH_STRING_LENGTH, "String is too long - increase MAX_HASH_STRING_LENGTH");

				char stringData[MAX_STRING_PAYLOAD_LENGTH+1];
				stringData[MAX_STRING_PAYLOAD_LENGTH] = 0;
				for (int i = 0; i < count; ++i)
				{
					stringData[i] = packet.Read_s8();
				}
				*(widget->m_data.h) = stringData;
				*(widget->m_prevData.h) = *(widget->m_data.h);
			}
			break;
		case ATFINALHASHSTRING:
			{
				bkAssertf(offset == 0 && count <= MAX_HASH_STRING_LENGTH, "String is too long - increase MAX_HASH_STRING_LENGTH");

				char stringData[MAX_STRING_PAYLOAD_LENGTH+1];
				stringData[MAX_STRING_PAYLOAD_LENGTH] = 0;
				for (int i = 0; i < count; ++i)
				{
					stringData[i] = packet.Read_s8();
				}
				*(widget->m_data.fh) = stringData;
				*(widget->m_prevData.fh) = *(widget->m_data.fh);
			}
			break;
				
		}

		packet.End();

		if ( packetsRemaining == 0 )
		{
			widget->WindowUpdate();
			widget->Changed();
		}
	}
}

void bkText::GetStringRepr( char *buf, int bufLen )
{
    char cBuf[1024];

    switch ( m_dataType )
    {
    case BOOL:
        formatf( cBuf, "%s", *(m_data.b) ? "True" : "False" );
        break;
    case FLOAT:
        formatf( cBuf, "%f", *(m_data.f) );
        break;
    case INT:
        formatf( cBuf, "%d", *(m_data.i) );
		break;
	case LONG:
		formatf( cBuf, "%ld", *(m_data.l) );
		break;
    case STRING:
        formatf( cBuf, "%s", m_data.s );
        break;
	case ATHASHSTRING:
	case ATFINALHASHSTRING:
		formatf( cBuf, "%s", GetHashStringCStr());
		break;
    }

    safecpy( buf, cBuf, bufLen );
}

void bkText::SetStringRepr( const char *buf )
{
    switch ( m_dataType )
    {
    case BOOL:
        SetBool( (buf != NULL) && (strlen( buf ) > 0) && (buf[0] != '0') && (stricmp( buf, "f" ) != 0) && (stricmp( buf, "false" ) != 0) );
        break;
    case FLOAT:
        SetFloat( (float)atof( buf ) );
        break;
    case INT:
        SetInt( atoi( buf ) );
		break;
	case LONG:
		SetLong( atol( buf ) );
		break;
    case STRING:
        SetString( buf );
        break;
	case ATHASHSTRING:
		SetHashString( buf );
		break;
	case ATFINALHASHSTRING:
		SetFinalHashString( atFinalHashString(buf) );
		break;
    }
}

#if __WIN32PC
#include "system/xtl.h"

WNDPROC s_OldTextEditWinProc;

static LRESULT CALLBACK Text_WindowEditMessage( HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam )
{
	switch ( iMsg )
	{
	case WM_KEYDOWN:
		if ( wParam==VK_RETURN )
		{
			// SendMessage(hwnd,WM_SETFONT,(LPARAM)GetPane()->GetFont(),MAKELPARAM(TRUE, 0));	
			/* bkWinText& text=(bkWinText&)bkWinWidget::GetWidget(hwnd);
			text.SetFont(false);
			text.CheckValue(true); */
			((bkWidget*)GetWindowLongPtr( hwnd, GWLP_USERDATA ))->Changed();
		}
		else if ((wParam == VK_LEFT) || (wParam == VK_RIGHT) || (wParam == VK_UP) || (wParam == VK_DOWN) 
			|| (wParam == VK_HOME) || (wParam == VK_END) )
		{
			return CallWindowProc( s_OldTextEditWinProc, hwnd, iMsg, wParam, lParam );
		}
		else 
		{
			return 0;
		}
	case WM_CHAR:
		if ( wParam != '\r' )
		{
			// ((bkWinText&)bkWinWidget::GetWidget(hwnd)).SetFont(true);
			return CallWindowProc( s_OldTextEditWinProc, hwnd, iMsg, wParam, lParam );
		}
		else 
		{
			return 0;
		}
	case WM_KILLFOCUS:
		/* if (((bkWinManager&)bkManager::GetInstance()).IsDeleting())
		return 0; */
		// SendMessage(hwnd,WM_SETFONT,(LPARAM)GetPane()->GetFont(),MAKELPARAM(TRUE, 0));	
		// ((bkWinText&)bkWinWidget::GetWidget(hwnd)).CheckValue(true);
		return CallWindowProc( s_OldTextEditWinProc, hwnd, iMsg, wParam, lParam );
	default:
		return CallWindowProc( s_OldTextEditWinProc, hwnd, iMsg, wParam, lParam );
		break;
	}

	// return 0;
}


void bkText::WindowCreate() 
{
	if ( !bkRemotePacket::IsConnectedToRag() )
	{
		bkPane *pane = GetPane();
		m_Label = pane->AddWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX, 0 );
		pane->AddLastWindow( this, "EDIT", 0, ES_AUTOHSCROLL | ES_LEFT | WS_BORDER | (IsReadOnly() ? ES_READONLY : 0) );
		s_OldTextEditWinProc = (WNDPROC) SetWindowLongPtr( m_Hwnd, GWLP_WNDPROC, (LONG_PTR)Text_WindowEditMessage );

		SendMessage( m_Hwnd, EM_SETLIMITTEXT, m_StringSize, 0 );
	}
}


void bkText::WindowDestroy() 
{
	if ( m_Label )
	{
		DestroyWindow( m_Label );
		m_Label = 0;
	}

	bkWidget::WindowDestroy();
}


void bkText::WindowUpdate() 
{
	if ( m_Hwnd )
	{
		switch ( m_dataType )
		{
		case BOOL:
			{
				char text[8];
				safecpy( text, *(m_data.b) ? "true" : "false" );

				SetWindowText( m_Hwnd, text );
			}
			break;
		case FLOAT:
			{
				char text[64];
				formatf( text, "%f", *(m_data.f) );

				SetWindowText( m_Hwnd, text );
			}
			break;
		case INT:
			{
				char text[64];
				formatf( text, "%d", *(m_data.i) );

				SetWindowText( m_Hwnd, text );
			}
			break;
		case LONG:
			{
				char text[64];
				formatf( text, "%ld", *(m_data.l) );

				SetWindowText( m_Hwnd, text );
			}
			break;
		case STRING:
			SetWindowText( m_Hwnd, m_data.s );
			break;
		case ATHASHSTRING:
		case ATFINALHASHSTRING:
			SetWindowText( m_Hwnd, GetHashStringCStr() );
			break;
		}		
	}
}


rageLRESULT bkText::WindowMessage( rageUINT msg, rageWPARAM wParam, rageLPARAM /*lParam*/ )
{
	if ( (msg == WM_COMMAND) && (HIWORD(wParam) == EN_CHANGE) && !IsReadOnly() )
	{
		switch ( m_dataType )
		{
		case BOOL:
			{
				char text[8];
				GetWindowText( m_Hwnd, text, 8 );

				if ( strcmp( text, "true" ) == 0 )
				{
					*(m_data.b) = true;
				}
				else
				{
					*(m_data.b) = false;
				}
			}
			break;
		case FLOAT:
			{
				char text[64];
				GetWindowText( m_Hwnd, text, 64 );

				*(m_data.f) = (float)atof( text );
			}
			break;
		case INT:
			{
				char text[64];
				GetWindowText( m_Hwnd, text, 64 );

				*(m_data.i) = atoi( text );
			}
			break;
		case LONG:
			{
				char text[64];
				GetWindowText( m_Hwnd, text, 64 );

				*(m_data.l) = atol( text );
			}
			break;
		case STRING:
			GetWindowText( m_Hwnd, m_data.s, m_StringSize );
			break;
		case ATHASHSTRING:
			{
				char stringData[MAX_STRING_PAYLOAD_LENGTH];
				GetWindowText( m_Hwnd, stringData, m_StringSize);
				*(m_data.h) = stringData;
			}
			break;
		case ATFINALHASHSTRING:
			{
				char stringData[MAX_STRING_PAYLOAD_LENGTH];
				GetWindowText( m_Hwnd, stringData, m_StringSize);
				*(m_data.fh) = stringData;
			}
			break;
		}		
	}
	return 0;
}


int bkText::WindowResize( int x, int y, int width, int height )
{
	int textWidth = width / 2;
	int textStart = width - textWidth;
	
	if ( m_Label )
	{
		SetWindowPos( m_Label, 0, x, y, textStart, height, SWP_NOACTIVATE | SWP_NOZORDER );
	}

	if ( m_Hwnd )
	{
		SetWindowPos( m_Hwnd, 0, x + textStart, y, textWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );
	}

	return y+height;
}

#endif

void bkText::Update() 
{
	bool changed = false;
	switch ( m_dataType )
	{
	case BOOL:
		if ( *(m_data.b) != *(m_prevData.b) )
		{
			*(m_prevData.b) = *(m_data.b);
			changed = true;
		}
		break;
	case FLOAT:
		if ( *(m_data.f) != *(m_prevData.f) )
		{
			*(m_prevData.f) = *(m_data.f);
			changed = true;
		}
		break;
	case INT:
		if ( *(m_data.i) != *(m_prevData.i) )
		{
			*(m_prevData.i) = *(m_data.i);
			changed = true;
		}
		break;
	case LONG:
		if ( *(m_data.l) != *(m_prevData.l) )
		{
			*(m_prevData.l) = *(m_data.l);
			changed = true;
		}
		break;
	case STRING:
		if ( strcmp( m_data.s, m_prevData.s ) ) 
		{
			safecpy( m_prevData.s, m_data.s, m_StringSize );
			changed = true;
		}
		break;
	case ATHASHSTRING:
		if (*(m_data.h) != *(m_prevData.h) )
		{
			*(m_prevData.h) = *(m_data.h);
			changed = true;
		}
		break;
	case ATFINALHASHSTRING:
		if (*(m_data.fh) != *(m_prevData.fh) )
		{
			*(m_prevData.fh) = *(m_data.fh);
			changed = true;
		}
		break;
	}

	if ( changed )
	{
		RemoteUpdate();

#if __WIN32PC
		if ( m_Hwnd && (GetFocus() != m_Hwnd) )		// don't send update if we already had the focus
		{
			WindowUpdate();
		}
#endif
	}
}


#endif
