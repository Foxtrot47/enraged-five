//
// bank/bank.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "bank.h"

#include "bkmgr.h"
#include "pane.h"

#include "system/param.h"
#include "system/threadtype.h"
#include "system/xtl.h"

using namespace rage;

#if __WIN32
// #pragma warning(disable:4100)
#endif


bkBank::bkBank(const char *title,datCallback creator,datCallback destructor) : 
	bkGroup(creator,title,0,false), 
	m_Current(0), 
	m_Destructor(0)
{
	m_Current = this;

#if __WIN32PC
    m_Pane = NULL;
#endif

	if (destructor.GetType() != datCallback::NULL_CALLBACK) {
		m_Destructor = rage_new datCallback;
		*m_Destructor = destructor;
	}

}



bkWidget& bkBank::FinishCreate()
{
	bkWidget& widget=bkWidget::FinishCreate();

	// if we're connected to RAG, create the bank right from the start:
	if (m_ExtraData)
	{
		datCallback& cb = m_ExtraData->m_Callback;
		if (cb.GetType() != datCallback::NULL_CALLBACK && bkRemotePacket::IsConnectedToRag())
		{
			cb.Call(this);
			cb = NullCB;

			delete m_Destructor;

			m_Destructor=NULL;
		}
	}

	return widget;
}

void bkBank::Changed() {
	// Only banks created via RegisterBank have callbacks.
	if (m_ExtraData)
	{
		datCallback& cb = m_ExtraData->m_Callback;

		if (cb.GetType() != datCallback::NULL_CALLBACK) {
			if (!m_Open) {
				if (m_Destructor)
					m_Destructor->Call(this);
				while (GetChild())
					GetChild()->Destroy();
			}
			else {
				cb.Call(this);
			}
		}
	}
}


bkBank::~bkBank() {
	if (m_Current != this)
	{
		Quitf("Bank '%s': Mismatched PushGroup / PopGroup",m_Title);
	}

	delete m_Destructor;
}

void bkBank::Update() {
#if __WIN32PC
	if(GetPane() && GetPane()->GetNeedsResize())
		GetPane()->Resize();
#endif
	bkGroup::Update();
}

void bkBank::SetCurrentGroup(bkGroup &rGroup) 
{
	bkAssertf((m_Current == this) , "Cannot Set/Unset groups in combination with pushing and popping groups! - current group = \"%s\", this group = \"%s\"", m_Current->GetTitle(), GetTitle());
	m_Current = &rGroup;
}

void bkBank::UnSetCurrentGroup(bkGroup & ASSERT_ONLY(rGroup)) 
{
	bkAssertf((&rGroup == m_Current) , "Set/Unset group operation either displaced by push/pop operation (illegal) or not correctly paired - current group = \"%s\", this group = \"%s\", pop group = \"%s\"", m_Current->GetTitle(), GetTitle(), rGroup.GetTitle());
	m_Current = this;
}

bkGroup* bkBank::PushGroup(const char *title,bool startOpen,const char *memo,const char* fillColor) {
#if __ASSERT
	CheckThread("PushGroup", title);
#endif // __ASSERT
	bkGroup* grp = AddGroup(title, startOpen, memo, fillColor);
	m_Current = grp;
	return grp;
}

void bkBank::DeleteGroup(bkGroup& pGroup) {
	if (m_Current==&pGroup)
	{
		// Set the current group to the next sibling
		if (m_Current->m_Next)
		{
			m_Current=m_Current->m_Next;
		}
		// Otherwise, the parent?
		else if (m_Current->m_Parent)
		{
			m_Current = m_Current->m_Parent;
		}
		// Otherwise, set it to self
		else
		{
			m_Current = this;
		}
	}
	pGroup.Destroy();
}



void bkBank::PopGroup() { 
#if __ASSERT
	CheckThread("PopGroup", m_Current->GetTitle());
#endif // __ASSERT
	if (bkVerifyf(m_Current != this, "Can't pop group %s, too many pops", m_Title))
	{
		m_Current = m_Current->GetParent();
	}
}


void bkBank::Truncate(int newCount) {
	// make sure we have work to do (plus the code doesn't handle the borderline case correctly)
	if (newCount >= GetNumWidgets())
		return;

	// bkDisplayf("Truncate (%d)",newCount);
	// Print(4);
	bkWidget **i = &m_Child;
	while (newCount && *i) {
		if ((*i)->m_Child)			// check for a child
			i = &(*i)->m_Child;
		else if ((*i)->m_Next)		// check for a sibling
			i = &(*i)->m_Next;
		else {			// neither, so look up the tree
			do {
				i = &(*i)->m_Parent->m_Next;
			} while (!*i);
		}
		--newCount;
	}
	while (*i)
		(*i)->Destroy();

}


void bkBank::GetState(bool &WIN32PC_ONLY(shown),int &WIN32PC_ONLY(x),int &WIN32PC_ONLY(y),int &WIN32PC_ONLY(width),int &WIN32PC_ONLY(height)) { 
#if __WIN32PC
	if (m_Pane)
		m_Pane->GetState(shown,x,y,width,height);
#endif
}


void bkBank::SetState(bool WIN32PC_ONLY(shown),int WIN32PC_ONLY(x),int WIN32PC_ONLY(y),int WIN32PC_ONLY(width),int WIN32PC_ONLY(height)) {
#if __WIN32PC
	if (m_Pane)
		m_Pane->SetState(shown,x,y,width,height);
#endif
}


void bkBank::RemoteCreate() {
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetStaticGuid(),this);
	p.WriteWidget((bkRemotePacket::IsConnectedToRag() && m_Parent==&BANKMGR)?m_Parent:NULL);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char(GetFillColor());
	p.Write_bool(IsReadOnly());
	p.Write_s32((s32)m_Open);
	p.Send();
}


void bkBank::RemoteUpdate() {
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin(bkRemotePacket::CHANGED,GetGuid(),this);
	p.Write_s16(m_Open);
	p.Send();
}


void bkBank::RemoteHandler(const bkRemotePacket& packet) {
	USE_DEBUG_MEMORY();
	if (packet.GetCommand() == bkRemotePacket::CREATE) {
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget *parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;
		const char *title = packet.Read_const_char();
		packet.End();
		bkBank& widget = *(rage_new bkBank(title,NullCB,NullCB));
		bkRemotePacket::SetWidgetId(widget, id);
		parent->AddChild(widget);
		//local->Show();
	}
	else if (packet.GetCommand() == bkRemotePacket::CHANGED) {
		packet.Begin();
		bkBank* widget=packet.ReadWidget<bkBank>();
		if (!widget) return;
		widget->m_Open = widget->m_PrevOpen = packet.Read_s16();
		packet.End();
		widget->Changed();
	}
}


int bkBank::GetGuid() const { return GetStaticGuid(); }


bool bkBank::IsClosedGroup() const {
	return false;
}

#if __ASSERT
void bkBank::CheckThread(const char* funcName, const char* groupName)
{
	bkAssertf(bkManager::IsBankThread(), "%s(%s) called on non-update thread (type=%08x)", funcName, groupName, sysThreadType::GetCurrentThreadType());
}
#endif // __ASSERT

#if __WIN32PC
XPARAM(bankx);
XPARAM(banky);
void bkBank::WindowCreate() {
	if (!bkRemotePacket::IsConnectedToRag())
	{
		int x = 100;
		int y = 100;
		PARAM_bankx.Get(x);
		PARAM_banky.Get(y);
		m_Pane = rage_new bkPane(bkManager::GetOwner(),this,m_Title,x+250,y,false);
		m_Parent->GetPane()->AddLastWindow(this,"BUTTON",0,BS_PUSHBUTTON);
	}
}


rageLRESULT bkBank::WindowMessage(rageUINT /*msg*/,rageWPARAM wParam,rageLPARAM /*lParam*/) {
	if (HIWORD(wParam) == BN_CLICKED) {
		m_Open = !m_Open;
		if (m_Open)
			Show();
		else
			Hide();
		m_PrevOpen = m_Open;		// don't send redundant CHANGED message because of bkGroup::Update
		FinishUpdate();
		Changed();
	}

	return 0;
}


int bkBank::WindowResize(int x,int y,int width,int height) {
	return bkWidget::WindowResize(x,y,width,height);
}


void bkBank::WindowDestroy() {
	DestroyWindow(m_Hwnd);
	delete m_Pane;
	m_Pane = NULL;
}


bkPane* bkBank::GetPane() {
	return m_Pane;
}
#endif	// __WIN32PC


void bkBank::Show() {
#if __WIN32PC
	if (m_Pane)
		ShowWindow(m_Pane->GetOuterHandle(),SW_NORMAL);
#endif
}



void bkBank::Hide() {
#if __WIN32PC
	if (m_Pane)
		ShowWindow(m_Pane->GetOuterHandle(),SW_HIDE);
#endif
}


bool bkBank::IsShown() const
{ 
	// Return false if the on screen widgets aren�t shown and you�re not connected to RAG
	bool isShown = ( (bkManager::GetActive() || bkRemotePacket::IsConnectedToRag()) ? IsOpen() : false );

	return isShown;
}

inline bkWidget* bkBank::AddWidget(bkWidget& widget) 
{
	return &m_Current->AddChild(widget).FinishCreate();
}

#endif
