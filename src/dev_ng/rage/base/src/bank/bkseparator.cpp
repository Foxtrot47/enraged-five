// 
// bank/bkseparator.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#if __BANK

#include "bkseparator.h"

#include "packet.h"
#include "pane.h"

using namespace rage;

bkSeparator::bkSeparator( const char *title, const char *fillColor )
: bkWidget( NullCB, title, 0, fillColor, true )
{

}

bkSeparator::~bkSeparator() 
{

}

int bkSeparator::GetGuid() const
{
	return GetStaticGuid();
}

int bkSeparator::DrawLocal( int x,int y )
{
	Drawf( x, y, "------------------------" );
	return bkWidget::DrawLocal( x, y );
}

void bkSeparator::RemoteCreate()
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin( bkRemotePacket::CREATE, GetStaticGuid(), this );
	p.WriteWidget( m_Parent );
	p.Write_const_char( m_Title );
	p.Write_const_char( GetTooltip() );
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
	p.Send();
}

void bkSeparator::RemoteHandler( const bkRemotePacket& packet ) 
{
	if ( packet.GetCommand() == bkRemotePacket::CREATE )
	{
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if ( !parent )
		{
			return;
		}

		const char *title = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		packet.Read_bool();	// Read-only isn't used for separators...
		packet.End();
		
		bkSeparator& widget = *(rage_new bkSeparator( title, fillColor ));
		bkRemotePacket::SetWidgetId( widget, id );
		parent->AddChild( widget );
	}
}

#if __WIN32PC
#include "system/xtl.h"

void bkSeparator::WindowCreate()
{
	if ( !bkRemotePacket::IsConnectedToRag() )
	{
		GetPane()->AddLastWindow( this, "STATIC", 0, SS_CENTER | SS_NOPREFIX, "" );
	}
}

rageLRESULT bkSeparator::WindowMessage( rageUINT /*msg*/, rageWPARAM /*wParam*/, rageLPARAM /*lParam*/ )
{
	return 0;
}

#endif

#endif
