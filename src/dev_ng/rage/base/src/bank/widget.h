//
// bank/widget.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_WIDGET_H
#define BANK_WIDGET_H

//DOM-IGNORE-BEGIN
#define BKGUID(a,b,c,d)	((a) | ((b) << 8) | ((c) << 16) | ((d) << 24))
#define EXPAND_BKGUID(x) ((x) & 255), (((x) >> 8) & 255), (((x) >> 16) & 255), (((x) >> 24) & 255)
//DOM-IGNORE-END

#if __BANK && !__SPU

#include "atl/inmap.h"
#include "data/callback.h"
#include "system/wndproc.h"

struct HWND__;

namespace rage {

class bkPane;
class bkBank;
class bkManager;
class bkGroup;
class bkRemotePacket;

namespace bkAngleType {
	enum Enum
	{
		DEGREES,	// A float value from 0 to 360
		FRACTION,	// A float value from 0 to 1
		RADIANS,	// A float value from 0 to 2 PI
		VECTOR2		// An angle represented by a 2-dimensional vector at the origin.
	};
}

class bkWidget {
	friend class bkPane;
	friend class bkManager;
	friend class bkGroup;
	friend class bkBank;
	friend class bkRemoteWidgetMap;
public:
	enum Action {
		DIAL  // specifies a tick event for a widget (increment/decrement option for sliders)
	};

	//
	// PURPOSE:
	//  Constructor.  Note that incoming strings are duplicated with ConstStringDuplicate to save memory
	// PARAMS:
	//  callback - Initial callback to apply
	//  title - Initial title for control
	//  memo - Initial tooltip for control
	//  data - Pointer to initial data for control
	//  dataSize - Size of initial data (so we know how much to copy)
	//  fillColor - A specially formatted string representing the desired color of the widget.
	//   If this is an empty string, NULL, or "ARGBColor:0:0:0:0", the default system color 
	//   for Control is used.  
	//   "NamedColor:nameOfColor" can be used to colorize with a named Windows System color.
	//   "ARGBColor:x:x:x:x" can be used to colorize with an ARGB color, each value ranging from 0 to 255 (recommended alpha: 128).
	//	readOnly - Flag for specifying that this widget should be readonly.
	//
	bkWidget(datCallback &callback,const char *title,const char *memo,const char* fillColor=NULL, bool readOnly=false);

	//
	// PURPOSE:
	//	Debug display of control hierarchy.  Recursively
	//  traverses the entire control tree.
	// PARAMS:
	//	indent - the indentation level (# of whitespaces)
	//
	void Print(int indent);
	//
	// PURPOSE
	//	a message sent to the widget
	// PARAMS
	//	action - the action to apply
	//	value - the value for the action
	//
	virtual void Message(Action action,float value);

	// PURPOSE: should be called every frame
	virtual void Update();
	//
	// PURPOSE
	//	draws a local representation of the widget
	// PARAMS
	//	x - the horizontal position to draw
	//	y - the vertical position to draw
	// RETURNS
	//	returns the new vertical position after drawing the widget
	//
	virtual int DrawLocal(int x,int y);
	// PURPOSE: accessor for the guid of the widget type
	// RETURNS: returns the widget type guid 
	virtual int GetGuid() const = 0;
	// PURPOSE: sets this widget as the currently selected widget
	// RETURNS: returns true if this widget actually set focus
	virtual bool SetFocus();
	// PURPOSE: accessor that checks if this widget is the focus widget
	// RETURNS: true if this widget is the focused widget, false if not
	bool IsInFocus() const;

	//
	// PURPOSE: 
	//   Destroys a widget and all of its children.  This is harder than you'd expect because
	//   of remote widgets.  We always need to destroy our children before destroying ourselves, but we
	//   also need to make sure this happens correctly on the remote machine and we don't double-destroy
	//   anyway.
	//
	void Destroy();

	//
	// PURPOSE:
	//	Add a specified child to the end of our child list
	// PARAMS:
	//	newChild - Address of child to add to our child list
	// RETURNS:
	//	Input child, so this call can be used as a return result from control creation functions
	//	NOTES:
	//	NOT REALLY FOR EXTERNAL USE! Any given widget can only be attached to the tree once after creation.
	//
	bkWidget& AddChild(bkWidget& child);

	//
	// PURPOSE
	//  find a named child widget
	// PARAMS
	//  childTitle - the name of the widget we are searching for
	// RETURNS
	//	returns a pointer to the widget if it is found
	//
	bkWidget* FindChild(const char *childTitle) const;

	// PURPOSE: accessor for the title of the widget
	// RETURNS: returns the title of the widget
	const char* GetTitle() const;
	
	// PURPOSE: accessor for the tooltip associated with the widget
	// RETURNS: returns the tooltip of the widget
	const char* GetTooltip() const;
	
	// PURPOSE: accessor for the fill color of the widget
	// RETURNS: returns a specially formatted string representing the desired color of the widget.
	//   If this is an empty string, NULL, or "ARGBColor:0:0:0:0", the default system color 
	//   for Control is used.  
	//   "NamedColor:nameOfColor" can be used to colorize with a named Windows System color.
	//   "ARGBColor:x:x:x:x" can be used to colorize with an ARGB color, each value ranging from 0 to 255 (recommended alpha: 128).
	const char* GetFillColor() const;

	// PURPOSE: sets the fill color of the widget
	// PARAMS:
	//  fillColor - A specially formatted string representing the desired color of the widget.
	//   If this is an empty string, NULL, or "ARGBColor:0:0:0:0", the default system color 
	//   for Control is used.  
	//   "NamedColor:nameOfColor" can be used to colorize with a named Windows System color.
	//   "ARGBColor:x:x:x:x" can be used to colorize with an ARGB color, each value ranging from 0 to 255 (recommended alpha: 128).
	void SetFillColor( const char* fillColor );

	// PURPOSE: accessor for the readonly flag for this widget
	// RETURNS: returns whether the widget is readonly.
	bool IsReadOnly() const;

	// PURPOSE: sets the readonly state of the widget
	// PARAMS:
	//  value - new readonly value.
	void SetReadOnly( bool value );

	// PURPOSE: accessor for the parent of this widget
	// RETURNS: returns the parent widget
	bkWidget* GetParent() const;
	
	// PURPOSE: accessor for the sibling of this widget
	// RETURNS: returns the sibling widget
	bkWidget* GetNext() const;
	
	// PURPOSE: accessor for the first child of this widget
	// RETURNS: returns the first child of this widget
	bkWidget* GetChild() const;
	
	// PURPOSE: accessor to see if the widget thinks it's in its open state
	// RETURNS: true if the widget thinks it's open, false if not
	virtual bool IsOpen() const;
	
	// PURPOSE: close the widget/group. called from DestroyWindow()
	virtual void SetClosed();
	
	// PURPOSE: accessor to see if the widget thinks that any of its children are visible
	// RETURNS: true if the widget thinks thinks that any of its children are visible, false if not
	virtual bool AreAnyWidgetsShown() const;
	
	// PURPOSE: called whenever the value linked to the widget has changed.
	// It calls the callback associated with the widget.
	virtual void Changed();

	// PURPOSE: Accessor for if this group is closed or open
	// RETURNS: true if the group is closed, false if not
	virtual bool IsClosedGroup() const;

	// PURPOSE: Execute the RemoteCreate() function for myself and any child widgets.  Typically
	//  called when a game starts without Rag, then selects the text widget "Start Rag".
	virtual void Rebuild();

    // PURPOSE: Retrieves a string representation of the current value of this widget.
    // PARAMS:
    //    buf - the buffer to write the string representation to.
    //    bufLen - the length of the buffer.
    virtual void GetStringRepr( char *buf, int bufLen );

    // PURPOSE: Sets the current value of this widget using the string representation of the new value.
    // PARAMS:
    //    buf - the new value.
    virtual void SetStringRepr( const char *buf );

	//
	// PURPOSE
	//	adds the class' static widgets to the bank
	// PARAMS
	//	bank - the bank to add the widgets to
	//
	static void AddStaticWidgets(bkBank& bank);
	
	//
	// PURPOSE
	//	sets the number of lines that should be shown in the local
	//  representation of the banks
	// PARAMS
	//	count - number of lines to show
	//
	static void SetShownLineCount(int count);

#if __WIN32PC
	// PURPOSE
	// Called at the beginning of tools that are WIN32 that don't wish to have buttons.
	static void DisableWindows() { sm_EnableWindows = false; }
#endif

protected:
	void FinishUpdate();
	virtual bkWidget& FinishCreate();
	int GetNumWidgets(bool countOpenOnly=false) const;
	virtual void RemoteCreate() = 0;
	virtual void RemoteUpdate();
	virtual void RemoteDestroy();

	virtual void InternalDestroy();

#if __WIN32PC
	static bool sm_EnableWindows;
	virtual void WindowCreate() = 0;
	virtual void WindowUpdate();
	virtual int WindowResize(int x,int y,int width,int height);
	virtual rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM) = 0;
	virtual void WindowDestroy();
	virtual bkPane* GetPane();
	void WindowDestroyAllKids();
	void WindowCreateAllKids();
#else
	void WindowUpdate() { }
#endif

	// PURPOSE: Destructor.  Most of the work is done in Destroy.
	virtual ~bkWidget();

	static int sm_ShownLineCount;
	static int sm_Cursor;
	static int sm_MaxCursorDraw;
	static int sm_MaxCursorUpdate;
	static int sm_TopLine;
	static bkWidget *sm_Focus;
	static bool sm_setWidgetId;
	
	u32 GetId();

	//
	// PURPOSE:
	//	Removes self from control tree.
	// NOTES:
	//	Our children are unaffected.  No memory is freed.
	//
	void Detach();
	void Drawf(int,int,const char*,...) const;

	// vptr +0
	bkWidget *m_Next, *m_Parent, *m_Child;
	const char* m_Title;

	struct ExtraData
	{
		ExtraData() : m_Callback(NullCB), m_Tooltip(NULL), m_FillColor(NULL), m_ReadOnly(false) {}

		datCallback m_Callback;	
		const char *m_Tooltip, *m_FillColor;
		bool m_ReadOnly;
	};

	ExtraData* m_ExtraData; // Not all widgets have this, so only allocate it when we need it.

	inmap_node<u32, bkWidget>	m_RemoteToLocalNode;
#if __WIN32PC
	HWND__ *m_Hwnd;						
#endif
};												

inline bool bkWidget::IsInFocus() const 
{ 
	return sm_Focus == this; 
}

inline const char* bkWidget::GetTitle() const 
{ 
	return m_Title; 
}

inline const char* bkWidget::GetTooltip() const 
{ 
	return m_ExtraData ? m_ExtraData->m_Tooltip : NULL;
}

inline const char* bkWidget::GetFillColor() const
{
	return m_ExtraData ? m_ExtraData->m_FillColor : NULL;
}

inline bool bkWidget::IsReadOnly() const
{
	return m_ExtraData ? m_ExtraData->m_ReadOnly : false;
}

inline bkWidget* bkWidget::GetParent() const 
{ 
	return m_Parent; 
}

inline bkWidget* bkWidget::GetNext() const 
{ 
	return m_Next; 
}

inline bkWidget* bkWidget::GetChild() const 
{ 
	return m_Child; 
}

inline void bkWidget::SetShownLineCount(int count)
{	
	sm_ShownLineCount = count;
}

inline u32 bkWidget::GetId()
{
	return m_RemoteToLocalNode.m_key;
}

inline void bkWidget::GetStringRepr( char* /*buf*/, int /*bufLen*/ )
{

}

inline void bkWidget::SetStringRepr( const char* /*buf*/ )
{
    Changed();
}

}	// namespace rage

#endif

#endif
