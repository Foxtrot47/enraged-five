//
// bank/packet.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "packet.h"

#include "bkmgr.h"
#include "msgbox.h"

#include "atl/ptr.h"
#include "data/datcompressiondata.h"
#include "diag/channel.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/ficompressiondata.h"
#include "file/tcpip.h"
#include "math/amath.h"
#include "system/alloca.h"
#include "system/bootmgr.h"
#include "system/compressionfactory.h"
#include "system/exception.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/namedpipe.h"
#include "system/nocompressiondata.h"
#include "system/param.h"
#include "system/stack.h"
#include "system/timer.h"
#include "system/xtl.h"

#include <string.h>

#if __PPU
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>

typedef int SOCKET;
#endif

using namespace rage;

#define DEBUG_PACKETS		0


// PURPOSE:  Handshake value to help ensure that Rage's bank code remains synchronized with Rag.  Increment this value 
//  whenever there's a change in one of the following that requires a change in Rag code:
//		rage/base/src/bank/*.*
//		rage/base/src/data/compress.h
//		rage/base/src/data/datcompress.cpp
//		rage/base/src/data/datcompressiondata.*
//		rage/base/src/file/compress.h
//		rage/base/src/file/compress_internal.h
//		rage/base/src/file/decompress.*
//		rage/base/src/file/ficompress.cpp
//		rage/base/src/file/ficompressiondata.*
//		rage/base/src/system/compressiondata.*
//		rage/base/src/system/compressionfactory.*
//		rage/base/src/system/namedpipe.*
//		rage/base/src/system/pipepacket.*
//  Keep this in sync with RAG_VERSION in rage/base/tools/rag/RageApplication.cs.
#define RAG_VERSION			2.0f

#if DEBUG_PACKETS
static const char *cmds[] = { "CREATE", "DESTROY", "CHANGED", "USER0", "USER1", "USER2", "USER3", "USER4", "USER5", "USER6", "USER7", "USER8", "USER9", "USER10" };
#endif


static sysNamedPipe s_BankPipe;

bool bkRemotePacket::sm_IsServer;
bool bkRemotePacket::sm_IsConnectedToRag;
atFixedArray<bkRemotePacket::Type,45> bkRemotePacket::sm_Types;
sysCompressionFactory* bkRemotePacket::sm_pCompressionFactory = NULL;
u8 bkRemotePacket::sm_writeBuffer[];
int bkRemotePacket::sm_writeBufferLength = 0;
char bkRemotePacket::sm_address[128];

bkRemoteWidgetMap bkRemotePacket::sm_IdToWidgetMap;

static int s_RagBaseSocketPort=-1;

sysCriticalSectionToken bkRemotePacket::sm_csToken;

extern __THREAD int RAGE_LOG_DISABLE;

extern void (*sysExceptionHandledCallback)();

PARAM(socketbaseport,"[bank] the base socket port for RAG viewers");
PARAM(noPrintCommandLine, "[bank] suppresses the command line TTY message" );
PARAM(ragPS3TargetAddr,"[bank] Instruct Rag which PS3 Target to communicate with.  Use the name of your Target as it appears in the Target Manager.");
PARAM(ragConnectTimeout,"[bank] Number of seconds before giving up on trying to connect to RAG.  (default is infinite)");
PARAM(ragnotty,"[bank] Don't send logging output to RAG.");

void bkRemotePacket::AddType(int guid,void (*handler)(const bkRemotePacket&)) {
	Type t;
	t.m_Guid = guid;
	t.m_Handler = handler;
	if (sm_Types.Find(t) == -1)
		sm_Types.Append() = t;
	else
		bkErrorf("bkRemotePacket::AddType - Remote type with guid %x already added",guid);
}

// #include "core/hexdump.h"

#if __BE
inline unsigned short SWAPS(unsigned short s) { return (s << 8) | (s >> 8); }
inline unsigned SWAPL(unsigned s) {
	return (s >> 24) | ((s >> 8) & 0xFF00) | ((s << 8) & 0xFF0000) | (s << 24);
}
#endif

// const char s_PipeName[] = "Bank2";
PARAM(bankpipename,"[bank] alternate name of bank pipe");
/* static const char* sGetPipeName()
{
	const char* pipeName=s_PipeName;
	PARAM_bankpipename.Get(pipeName);
	if (pipeName==NULL)
		pipeName=s_PipeName;
	return pipeName;
} */


bkRemotePacket::bkRemotePacket() : sysPipePacket(s_BankPipe)
{
}

#if DISABLE_WIDGETS_FOR_CONTENT_CONTROLLED_BUILD	
bool bkRemotePacket::AllowWidgetGroup(const char* WidgetName)
{
	bool Allow = false; 
	int offset = 0;  

	u32 result = m_Storage[offset++];
	result |= m_Storage[offset++] << 8;
	result |= m_Storage[offset++] << 16;
	result |= m_Storage[offset++] << 24;

	bkWidget* pWidget  = sm_IdToWidgetMap.GetWidget(result);  

	if(pWidget)	
	{
		if(stricmp(pWidget->GetTitle(), WidgetName) == 0)
		{
			Allow = true; 
		}
		else
		{
			while(pWidget)
			{
				pWidget = pWidget->GetParent(); 

				if(pWidget)
				{
					if(stricmp(pWidget->GetTitle(), WidgetName) == 0)
					{
						Allow = true;
					}
				}
			}
		}
	}
	return Allow; 
}
#endif

void bkRemotePacket::Send() 
{
	SYS_CS_SYNC( sm_csToken );
	bool canSend = false;
    
#if DISABLE_WIDGETS_FOR_CONTENT_CONTROLLED_BUILD
	if(m_Guid == bkManager::GetStaticGuid())
	{
		canSend = true; 
	}
	
	if(!canSend)
	{
		canSend = AllowWidgetGroup("Cut Scene Debug") || AllowWidgetGroup("Preview Folder") || AllowWidgetGroup("Weather"); 
	}
		
#else
	canSend = true;
#endif

	if(canSend)
	{
		int length = m_Length + sysPipePacket::HEADER_SIZE;
		if ( length + sm_writeBufferLength >= MAX_WRITE_BUFFER_LENGTH )
		{
			bkRemotePacket::SendPackets();
		}

		SwapHeaderBytes();
		memcpy( &sm_writeBuffer[sm_writeBufferLength], &m_Length, length );
		sm_writeBufferLength += length;
	}   
}

PARAM(waitforpipe,"[bank] keep trying to connect to the pipe");

static rage::atScopedPtr<sysNamedPipe, rage::atMemDestructor> sRagOutputPipe;
static void (*s_OldPrintString)(const char*)=NULL;

void sPrintStringToPipe(const char* str)
{
	if (s_OldPrintString)
		s_OldPrintString(str);
	sRagOutputPipe->Write(str,StringLength(str)+1); // add the null-terminator
}

void sWriteString(sysPipePacket& packet,const char* string,int opcode,int whichApp)
{
	// 128 is arbitrary, it just to be less than what sysPipePacket::Write_const_char() handles
	enum {CHUNK_LEN=128};
	if (StringLength(string)>CHUNK_LEN-1)
	{
		do 
		{
			char buff[CHUNK_LEN];
			int len=StringLength(string);
			strncpy(buff,string,Min(len,CHUNK_LEN-1));
			buff[Min(len,CHUNK_LEN-1)]='\0';

	
			string+=CHUNK_LEN-1;

			packet.Begin(opcode,0,0);
			packet.Write_u32(whichApp); // which app
			packet.Write_const_char(buff);
			packet.Send();	

			if (len<=CHUNK_LEN-1)
			{
				break;
			}
		}
		while (true);
	}
	else
	{
		packet.Begin(opcode,0,0);
		packet.Write_u32(whichApp); // which app
		packet.Write_const_char(string);
		packet.Send();
	}
}

void bkRemotePacket::Init()
{
	if ( sm_pCompressionFactory == NULL )
	{
		sm_pCompressionFactory = rage_new sysCompressionFactory();
		sm_pCompressionFactory->RegisterCompressionData( rage_new fiCompressionData );
		sm_pCompressionFactory->RegisterCompressionData( rage_new datCompressionData );
        sm_pCompressionFactory->RegisterCompressionData( rage_new sysNoCompressionData );
	}

	bkRemotePacket::SetRagSocketAddress( fiDeviceTcpIp::GetLocalHost() );

	// Ensure no other system has set the callback functions and then set them
#if EXCEPTION_HANDLING
	Assert(sysException::PostExceptionDisplay == NULL);
	Assert(sysExceptionHandledCallback        == NULL);
	sysException::PostExceptionDisplay = bkRemotePacket::ForceCloseConnection;
	sysExceptionHandledCallback        = bkRemotePacket::ForceCloseConnection;
#endif
}

bool bkRemotePacket::ConnectToRag(bool viewerApp)
{
	SYS_CS_SYNC( sm_csToken );

	// connect to rag: 
	enum 
	{
		HANDSHAKE,
		NUM_APPS,
		APP_NAME,
		APP_VISIBLE_NAME,
		APP_ARGS,
		PIPE_NAME_BANK,
		PIPE_NAME_OUTPUT,
		PIPE_NAME_EVENTS,
		END_OUTPUT,
		WINDOW_HANDLE,
		PLATFORM_INFO,
		PS3_TARGET_ADDRESS,
		BUILD_CONFIG
	};

	// create pipe names:
	char pipeNameBank[128];
	formatf(pipeNameBank,"%s.pipe.bank",bkManager::GetAppName());

	char pipeNameOutput[128];
	formatf(pipeNameOutput,"%s.pipe.output",bkManager::GetAppName());

	char pipeNameEvents[128];
	formatf(pipeNameEvents,"%s.pipe.event_queue",bkManager::GetAppName());

	int ragTimeout = -1; 
	PARAM_ragConnectTimeout.Get(ragTimeout); // seconds
	sysTimer ragConnectTimer;

	bool result=false;
	if (!viewerApp)
	{
		// connect to pipe:
		sysNamedPipe ragPipe;
		do {
			if ( (ragTimeout != -1) && (ragConnectTimer.GetTime() >= ragTimeout) )
			{
				bkWarningf("Could not connect to rag in %d seconds.  Loading without rag.  If you don't want connect to rag run without -rag command line parameter.",ragTimeout);
				return false;
			}

			int tries = 20;
			do {
				result=ragPipe.Open( bkRemotePacket::GetRagSocketAddress(), SOCKET_HANDSHAKE );
				if (result) {
					break;
				}
				tries--;
				sysIpcSleep(500);
			} while(!result && tries > 0);

			if (!result)
			{
				int dlgResult = bkMessageBox("RAG Connection Failed", "Couldn't connect to RAG.exe. Keep trying?", bkMsgYesNo, bkMsgError);
				if (dlgResult != 1)
					return false;
				}
		} while(!result);

#if __WIN32PC
		char szWindowTitle[512];
		/* Get the console window title. */ 
		GetConsoleTitle(szWindowTitle, 512); 

		/* Get the handle to the console window. */ 
		HWND hwndConsole = FindWindow(NULL, szWindowTitle); 

		/* Hide the console window */ 
		ShowWindow(hwndConsole, SW_HIDE);
		FreeConsole();
#endif
		
		sysPipePacket packet(ragPipe);

		// immediately after connecting, send over the rag version number
		packet.Begin( HANDSHAKE, 0, 0 );
		packet.Write_float( RAG_VERSION );	// Rage's Rag Version # so Rag can check for mismatches
		packet.Send();

		// wait for the reply
		ragConnectTimer.Reset();
		if (!packet.Receive(ragTimeout, &ragConnectTimer))
		{
			bkWarningf("rag handshaking failed after %d seconds.",ragTimeout);
			return false;
		}

		// receive base port # and Rag Version #:
		packet.Begin();
		s_RagBaseSocketPort=packet.Read_s32();
		float ragVersion = 0.0f;
		if ( packet.GetLength() > 4 )
		{
			ragVersion = packet.Read_float();
		}

		if ( ragVersion != RAG_VERSION )
		{
			char title[64];
			char msg[128];
			if ( ragVersion == 0.0f )
			{
				formatf( title, "Rag Version Number Not Received" );
				formatf( msg, "Rage bank code and Rag are out of sync.  Need %f from Rag.\n\nPlease update Rag.", RAG_VERSION );
			}
			else
			{
				formatf( title, "Rag Version Mismatch" );
				formatf( msg, "Rage bank code and Rag are out of sync.  Expected %f but received %f from Rag.\n\nPlease update %s", 
					RAG_VERSION, ragVersion, (RAG_VERSION < ragVersion) ? "your bank code." : "Rag." );
			}

			bkMessageBox( title, msg, bkMsgOk, bkMsgError );
			Quitf( "%s", title );
		}

		// send # of apps:
		packet.Begin(NUM_APPS,0,0);
		packet.Write_u32(1);
		packet.Write_u32(0); // signifies #index of master app
		packet.Send();

		sWriteString(packet,sysParam::GetProgramName(),APP_NAME,0);

		bkAssertf(bkManager::GetAppName(), "Can't find app name");
		sWriteString(packet,bkManager::GetAppName(),APP_VISIBLE_NAME,0);

#if RSG_PC && RSG_CPU_X86
		const char *platformString = "Win32";
#elif RSG_PC && RSG_CPU_X64
		const char *platformString = "Win64";
#elif RSG_XENON
		const char *platformString = "Xbox360";
#elif RSG_PPU
		const char *platformString = "PlayStation3";
#elif __PSP2
		const char *platformString = "PlayStationPortable2";
#elif RSG_DURANGO
		const char *platformString = "XboxOne";
#elif RSG_ORBIS
		const char *platformString = "PlayStation4";
#else
		const char *platformString = "Unknown";
#endif
		sWriteString(packet,platformString,PLATFORM_INFO,0);

#if __PPU
		const char* ps3TargetAddr;
		if ( PARAM_ragPS3TargetAddr.Get( ps3TargetAddr ) )
		{
			sWriteString( packet, ps3TargetAddr, PS3_TARGET_ADDRESS, 0 );
		}
#endif // __PPU

#if __DEV && !__OPTIMIZED
		const char *buildConfigString = "Debug";
#elif __DEV && __OPTIMIZED
		const char *buildConfigString = "Beta";
#elif !__DEV && __OPTIMIZED
		const char *buildConfigString = "BankRelease";
#else
		const char *buildConfigString = "Unknown";
#endif
		sWriteString(packet,buildConfigString,BUILD_CONFIG,0);


		for (int i=1;i<sysParam::GetArgCount();i++) // skip program name
		{
			sWriteString(packet,sysParam::GetArg(i),APP_ARGS,0);
		}

		// write master app names:
		sWriteString(packet,pipeNameBank,PIPE_NAME_BANK,0);
		sWriteString(packet,pipeNameOutput,PIPE_NAME_OUTPUT,0);
		sWriteString(packet,pipeNameEvents,PIPE_NAME_EVENTS,0);

		// send a command stating that we're done:
		packet.Begin(END_OUTPUT,0,0);
		packet.Send();
	}
	else 
	{
		PARAM_socketbaseport.Get(s_RagBaseSocketPort);
	}

	sm_IsConnectedToRag=true;

	bkDisplayf( "Connecting to Rag Output Pipe..." );

	// connect to output pipe:
	sRagOutputPipe = rage_new sysNamedPipe;
	do 
	{
		result = sRagOutputPipe->Open( bkRemotePacket::GetRagSocketAddress(), bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_OUTPUT) );
		if (!result)
		{
			sysIpcSleep(100);
		}
	} while(result==false);
	sRagOutputPipe->SetBlocking(false);

	bkDisplayf( "Connected." );

	if (!PARAM_ragnotty.Get())
	{
		s_OldPrintString=diagPrintCallback;
		diagPrintCallback=sPrintStringToPipe;
	}

	Connect(true,pipeNameBank);
	
	return true;
}

bool bkRemotePacket::Connect(bool waitForPipe,const char* /*pipeName*/) 
{
	SYS_CS_SYNC( sm_csToken );

	bkRemotePacket::PrintCommandLine();

	// connect bank pipe:
	bkDisplayf("Connecting to server...");
	sm_IsServer = false;
	bool result = false;

	// wait if asked to:
	do {
		result=s_BankPipe.Open( bkRemotePacket::GetRagSocketAddress(), bkRemotePacket::GetRagSocketPort(bkRemotePacket::SOCKET_OFFSET_BANK) );
		if (!result)
		{
			sysIpcSleep(100);
		}
	} while((PARAM_waitforpipe.Get() || waitForPipe) && result==false);

	if ( result )
	{
		bkDisplayf( "Connected." );
		s_BankPipe.SetNoDelay( true );
		if (!sysBootManager::IsDebuggerPresent())
		{
			s_BankPipe.SetBlocking(false);
		}
	}
	else
	{
		bkErrorf( "Unable to connect" );
	}

	return result;
}


bool bkRemotePacket::IsConnected() {
	return s_BankPipe.IsValid();
}


void bkRemotePacket::Close() 
{
	SYS_CS_SYNC( sm_csToken );

	if ( sm_pCompressionFactory )
	{
		delete sm_pCompressionFactory;
		sm_pCompressionFactory = NULL;
	}

	s_BankPipe.Close();
}


void bkRemotePacket::ForceCloseConnection() 
{
	diagLoggedPrintf("bkRemotePacket: Force closing connection to Proxy \n");
	s_BankPipe.Close();
}


void bkRemotePacket::ReceivePackets() 
{
	SYS_CS_SYNC(sm_csToken);

	bkRemotePacket p;
	while ( s_BankPipe.Read(&(p.m_Length),1) == 1 ) 
    {
		s_BankPipe.SafeRead( 1 + (char*)(&(p.m_Length)), sysPipePacket::HEADER_SIZE - 1 );
		p.SwapHeaderBytes();

#if DEBUG_PACKETS
		bkDisplayf("RECV Command %s GUID %c%c%c%c",cmds[p.m_Command],EXPAND_BKGUID(p.m_Guid));
#endif

		if (p.m_Length)
        {
			s_BankPipe.SafeRead(p.m_Storage,p.m_Length);
        }

		// HexDump( &(p.m_Length), p.m_Length + 8 );
		if ( p.m_Command == DESTROY ) 
        {
			// Destroy is already virtual for all widgets
			p.Begin();
			bkWidget* widget = p.ReadWidget<bkWidget>();
			if (!widget) return;
			p.End();
			widget->Destroy();
		}
		else 
        {
			int i;
			for (i=0; i<sm_Types.GetCount(); i++) 
            {
				if (p.m_Guid == sm_Types[i].m_Guid) 
                {
					sm_Types[i].m_Handler(p);
					break;
				}
			}
			if (i == sm_Types.GetCount())
            {
				bkErrorf("Unknown guid '%c%c%c%c' (%x).", EXPAND_BKGUID(p.m_Guid), p.m_Guid);
		    }
	    }
    }
}

void bkRemotePacket::SendPackets()
{
    SYS_CS_SYNC( sm_csToken );

    if ( sm_writeBufferLength > 0 )
    {
#if !__TOOL && __DEV
        sysMemStartTemp();
#endif
		++RAGE_LOG_DISABLE;

        // compress the buffer
		u32 bufferSize = sm_pCompressionFactory->GetCompressedDataSize(sysNoCompressionData::GetStaticHeaderPrefix(), sm_writeBufferLength);

        //bkDisplayf( "Compressed %d to %d, sending...", sm_writeBufferLength, compressedSize );

		if (bufferSize != 0)
		{
			u8 *buffer = Alloca(u8, bufferSize);

			u32 compressedSize;
			u8* compressedStream = sm_pCompressionFactory->CompressData( sysNoCompressionData::GetStaticHeaderPrefix(),
				sm_writeBuffer, sm_writeBufferLength, compressedSize, buffer, bufferSize );

			//Displayf( "Compressed %d to %d, sending...", sm_writeBufferLength, compressedSize );

			// now send the entire thing
			if ( compressedStream && s_BankPipe.IsValid() )
			{
				if (sysBootManager::IsDebuggerPresent())
				{
					s_BankPipe.SafeWrite(compressedStream, compressedSize);
				}
				else
				{
					size_t result = s_BankPipe.WriteWithTimeout(compressedStream, compressedSize, 10000);
					if (result != (size_t)compressedSize)
					{
						Assertf(0, "Error communicating with rag (Wrote %" SIZETFMT "d of %u) - shutting down the rag connection", result, compressedSize);
						ForceCloseConnection();
					}
				}

			}

			// reset
			memset( sm_writeBuffer, 0, MAX_WRITE_BUFFER_LENGTH );
			sm_writeBufferLength = 0;
		}

		--RAGE_LOG_DISABLE;
#if !__TOOL && __DEV
        sysMemEndTemp();
#endif
    }
}

void bkRemotePacket::PrintCommandLine()
{
	if ( !PARAM_noPrintCommandLine.Get() )
	{
		Displayf( "================================\nCommandLine" );

		// If I'm a dll or mll or some such, there isn't even a Arg0
		if(sysParam::GetArgCount() > 0)
		{
			Printf( "\t%s", sysParam::GetArg( 0 ) );
		}

		bool inQuoteBlock = false;
		bool skippingParam = false;
		for ( int i = 1; i < sysParam::GetArgCount(); ++i )
		{
			const char* arg = sysParam::GetArg( i );

			const char* pos = strstr( arg, "\"" );
			while ( pos != NULL )
			{
				inQuoteBlock = !inQuoteBlock;

				if ( *(pos + 1) != 0 )
				{
					pos = strstr( (pos + 1), "\"" );
				}
				else
				{
					break;
				}
			}

			if ( !inQuoteBlock && (arg[0] == '-') && (strlen(arg) > 1) && ((arg[1] < '0') || (arg[1] > '9')) )
			{
				skippingParam = (arg[1] == 'X');
				if (!skippingParam)
				{
					Printf( "\n\t%s", arg );
				}
			}
			else if (!skippingParam)
			{
				Printf( " %s", arg );
			}
		}

		Printf( "\n" );

		Displayf( "/CommandLine\n================================" );
	}
}

int bkRemotePacket::GetRagSocketPort( int offset)
{
	return s_RagBaseSocketPort + offset;
}

const char* bkRemotePacket::GetRagSocketAddress()
{
	return sm_address;
}

void bkRemotePacket::SetRagSocketAddress( const char* addr )
{
	safecpy( sm_address, addr, sizeof(sm_address) );
}

#if __WIN32PC && 0
void bkRemotePacket::Listen() {
	sm_IsServer = true;
	bkDisplayf("Waiting for client to connect...");
	if (!s_BankPipe.Create(sGetPipeName())) {
		Quitf("Client already running, please kill it and restart server first.");
	}
	else {
		bkDisplayf("Connected.");
	}
}
#endif

u32 bkRemoteWidgetMap::FindNextId()
{
    SYS_CS_SYNC( m_csToken );

	if (m_IdsLooped) {
		// special case, need to recycle an old ID
		while(GetWidget(m_NextId) != NULL)
		{
			m_NextId++;
			if (m_NextId == INVALID_ID)
			{
				m_NextId++;
			}
		}
	}

	u32 id = m_NextId;
	m_NextId++;
	if (m_NextId == INVALID_ID)
	{
		m_IdsLooped = true;
		m_NextId = 0;
	}
	return id;
}

u32 bkRemoteWidgetMap::GetId(bkWidget& widget)
{
    SYS_CS_SYNC( m_csToken );

	u32 id = widget.GetId();
	bkAssertf(id != INVALID_ID && GetWidget(id) == &widget, "Can't find the widget in the remote widget map (or another widget has the same ID)");
	return id;
}

bool bkRemoteWidgetMap::WidgetHasId(bkWidget& widget)
{
	return widget.GetId() != INVALID_ID;
}

bkWidget* bkRemoteWidgetMap::GetWidget(u32 id)
{
    SYS_CS_SYNC( m_csToken );

	if (id == INVALID_ID)
	{
		return NULL;
	}
	IdToWidgetMap::iterator iter = m_RemoteToLocal.find(id);
	if (iter != m_RemoteToLocal.end())
	{
		return iter->second;
	}
	return NULL;
}

u32 bkRemoteWidgetMap::AddWidget(bkWidget& widget)
{
    SYS_CS_SYNC( m_csToken );

	u32 id = FindNextId();
	bkAssertf(id != INVALID_ID, "Ran out of unique IDs for widgets!");
	AddWidgetAndId(widget, id);
	return id;
}

void bkRemoteWidgetMap::AddWidgetAndId(bkWidget& widget, u32 id)
{
    SYS_CS_SYNC( m_csToken );

	bkAssertf(GetWidget(id) == NULL && widget.GetId() == INVALID_ID, "This widget (%s) or this ID (%ud) already used in widget map", widget.m_Title, id);
	m_RemoteToLocal.insert(id, &widget);
}

void bkRemoteWidgetMap::RemoveWidget(bkWidget& widget)
{
    SYS_CS_SYNC( m_csToken );

	if (widget.GetId() != INVALID_ID)
	{
		// make sure it was correctly in the map first
		u32 id = GetId(widget);
		m_RemoteToLocal.erase(id);
		widget.m_RemoteToLocalNode.m_key = (u32)INVALID_ID;
	}
}

#endif
