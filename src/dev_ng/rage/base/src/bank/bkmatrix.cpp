// 
// bank/matrix.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#if __BANK

#include "bkmatrix.h"

#include "pane.h"

#include "atl/string.h"
#include "math/amath.h"
#include "system/stack.h"
#include "system/xtl.h"

using namespace rage;

#if __WIN32PC

static WNDPROC s_oldWindowEditMessageCallback;

static LRESULT CALLBACK WindowEditMessageCallback( HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam )
{
	bkMatrix *matrixWidget = (bkMatrix *)GetWindowLongPtr( hwnd, GWLP_USERDATA );

	switch ( iMsg )
	{
	case WM_CREATE:
		return CallWindowProc( s_oldWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );

	case WM_KEYDOWN:
		{
			if ( wParam == VK_RETURN )
			{
				::SetFocus( NULL );

				char buf[128];
				GetWindowText( hwnd, buf, sizeof(buf) );
				matrixWidget->SetFont( false );

				int row=0, col=0;
				matrixWidget->GetComponentInFocus( row, col );
				if ( !matrixWidget->ValidateString( buf, true ) || !matrixWidget->SetString( row, col, buf ) )
				{
					matrixWidget->GetString( row, col, buf, sizeof(buf) );
					SetWindowText( hwnd, buf );
					MessageBeep( MB_ICONHAND );
				}
				else
				{
					matrixWidget->Changed();
					return 0;
				}

				return 0;
			}
			else if ( (wParam == VK_LEFT) || (wParam == VK_RIGHT) || (wParam == VK_UP) || (wParam == VK_DOWN) 
				|| (wParam == VK_HOME) || (wParam == VK_END) )
			{
				return CallWindowProc( s_oldWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );
			}
			else if ( wParam == VK_TAB ) 
			{
				int row=0, col=0;
				matrixWidget->GetComponentInFocus( row, col );
				if ( (row + 1 == matrixWidget->GetNumRows()) && (col + 1 == matrixWidget->GetNumColumns()) )
				{
					matrixWidget->SetComponentInFocus( 0, 0, false );

					bkWidget *i = matrixWidget->GetNext();
					while ( i && !i->SetFocus() )
					{
						i = i->GetNext();
					}

					if ( !i )
					{
						i = matrixWidget->GetParent()->GetChild();
						while ( i && !i->SetFocus() )
						{
							i = i->GetNext();
						}
					}

					bkDisplayf( "Next control!" );
				}
				else
				{
					if ( col + 1 == matrixWidget->GetNumColumns() )
					{
						++row;
						col = 0;
					}
					else
					{
						++col;
					}

					matrixWidget->SetComponentInFocus( row, col, true );
				}

				return 0;
			}
		}
		return 0;

	case WM_CHAR:
		{
			if ( wParam == '\t' ) 
			{
				return 0;
			}
			else if ( wParam != '\r' )
			{
				if ( !matrixWidget->CheckKey( (char)wParam) )
				{
					return 0;
				}

				matrixWidget->SetFont( true );

				return CallWindowProc( s_oldWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );
			}
		}
		return 0;

	case WM_SETFOCUS:
		{
			matrixWidget->SetWindowInFocus( hwnd );
			::SendMessage( hwnd, EM_SETSEL, 0, ~0 );
		}
		return CallWindowProc( s_oldWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );

	case WM_KILLFOCUS:
		{
			char buf[128];
			GetWindowText( hwnd, buf, sizeof(buf) );
			matrixWidget->SetFont( false );

			int row=0, col=0;
			matrixWidget->GetComponentInFocus( row, col );
			if ( !matrixWidget->ValidateString( buf, true ) || !matrixWidget->SetString( row, col, buf ) )
			{
				matrixWidget->GetString( row, col, buf, sizeof(buf) );
				SetWindowText( hwnd, buf );
				MessageBeep( MB_ICONHAND );
			}
			else
			{
				matrixWidget->Changed();
			}
		}
		return CallWindowProc( s_oldWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );

	default:
		return CallWindowProc( s_oldWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );
	}
}

static WNDPROC s_oldMatrixWindowScrollMessageCallback;

static LRESULT CALLBACK MatrixWindowScrollMessageCallback( HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam )
{
	bkMatrix *matrixWidget = (bkMatrix *)GetWindowLongPtr( hwnd, GWLP_USERDATA );

	switch ( iMsg )
	{
	case WM_KEYDOWN:
		{
			if ( wParam == VK_TAB ) 
			{
				int row=0, col=0;
				matrixWidget->GetComponentInFocus( row, col );
				if ( (row + 1 == matrixWidget->GetNumRows()) && (col + 1 == matrixWidget->GetNumColumns()) )
				{
					matrixWidget->SetComponentInFocus( 0, 0, false );

					bkWidget *i = matrixWidget->GetNext();
					while ( i && !i->SetFocus() )
					{
						i = i->GetNext();
					}

					if ( !i )
					{
						i = matrixWidget->GetParent()->GetChild();
						while ( i && !i->SetFocus() )
						{
							i = i->GetNext();
						}
					}

					bkDisplayf( "Next control!" );
				}
				else
				{
					if ( col + 1 == matrixWidget->GetNumColumns() )
					{
						++row;
						col = 0;
					}
					else
					{
						++col;
					}

					matrixWidget->SetComponentInFocus( row, col, true );
				}

				return 0;
			}
		}
		return CallWindowProc( s_oldMatrixWindowScrollMessageCallback, hwnd, iMsg, wParam, lParam );

	case WM_SETFOCUS:
		{
			matrixWidget->SetWindowInFocus( hwnd );
		}
		return CallWindowProc( s_oldMatrixWindowScrollMessageCallback, hwnd, iMsg, wParam, lParam );

	default:
		return CallWindowProc( s_oldMatrixWindowScrollMessageCallback, hwnd, iMsg, wParam, lParam );
	}
}

#endif // __WIN32PC

//#############################################################################

const float bkMatrix::FLOAT_MIN_VALUE = -FLT_MAX / 1000.0f;
const float bkMatrix::FLOAT_MAX_VALUE = +FLT_MAX / 1000.0f;

bkMatrix::bkMatrix( datCallback &callback, const char *title, const char *memo, EnumType enumType, float min, float max, float step, const char *fillColor, bool readOnly )
: bkWidget(callback,title,memo,fillColor,readOnly)
, m_enumType(enumType)
, m_minimum(min)
, m_maximum(max)
, m_step(step)
, m_drawLocalTopY(0)
{
#if __WIN32PC
	m_titleWindowHandle = NULL;

	for ( int i = 0; i < 4; ++i )
	{
		m_rowTitleWindowHandles[i] = NULL;
	}

	for ( int i = 0; i < 4; ++i )
	{
		for ( int j = 0; j < 12; ++j )
		{
			m_windowHandles[i][j] = NULL;
		}
	}

	m_rowInFocus = 0;
	m_colInFocus = 0;
#endif

	for ( int i = 0; i < 4; ++i )
	{
		m_drawLocalExpanded[i] = 0;
	}
}

bkMatrix::~bkMatrix()
{

}

bool bkMatrix::SetFocus() 
{
#if __WIN32PC
	int index = m_colInFocus * 3;
	if ( m_windowHandles[m_rowInFocus][index + 1] != NULL )
	{
		::SetFocus( m_windowHandles[m_rowInFocus][index + 1] );
	}
#endif

	sm_Focus = this;
	return m_step != 0.0f;
}

void bkMatrix::GetStringRepr( char *buf, int bufLen )
{
    char cBuf[512];
    cBuf[0] = 0;

    for ( int i = 0; i < GetNumRows(); ++i )
    {
        for ( int j = 0; j < GetNumColumns(); ++j )
        {
            if ( strlen( cBuf ) > 0 )
            {
                safecatf( cBuf, ",%f", GetValue( i, j ) );
            }
            else
            {
                safecatf( cBuf, "%f", GetValue( i, j ) );
            }            
        }
    }

    safecpy( buf, cBuf, bufLen );
}

void bkMatrix::SetStringRepr( const char *buf )
{
    atString strBuf( buf );
    atArray<atString> split;
    strBuf.Split( split, ',' );

    for ( int i = 0; i < GetNumRows(); ++i )
    {
        for ( int j = 0; j < GetNumColumns(); ++j )
        {
            int index = (i * GetNumColumns()) + j;
            if ( index < split.GetCount() )
            {
                split[index].Trim();
                SetString( i, j, split[index].c_str() );
            }
        }
    }

    Changed();
}

void bkMatrix::RemoteCreate()
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin( bkRemotePacket::CREATE, GetGuid(), this );
	p.WriteWidget (m_Parent );
	p.Write_const_char( m_Title );
	p.Write_const_char( GetTooltip() );
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
	WriteValues( p );
	p.Write_float( m_minimum );
	p.Write_float( m_maximum );
	p.Write_float( m_step );
	p.Send();
}

void bkMatrix::RemoteUpdate()
{
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin( bkRemotePacket::CHANGED, GetGuid(), this );
	WriteValues( p );
	p.Send();
}

#if __WIN32PC

void bkMatrix::WindowCreate()
{
	if ( !bkRemotePacket::IsConnectedToRag() )
	{			
		m_titleWindowHandle = GetPane()->AddWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX | SS_NOTIFY );

		for ( int i = 0; i < GetNumRows(); ++i )
		{
			char buf[64];
			formatf( buf, "Row %d", i + 1 );
			m_rowTitleWindowHandles[i] = GetPane()->AddWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX | SS_NOTIFY, buf );

			for ( int j = 0; j < GetNumColumns(); ++j )
			{
				AddComponentWindow( i, j );
			}
		}		
	}
}

void bkMatrix::AddComponentWindow( int row, int col )
{
	char* text[] = { "X", "Y", "Z", "W" };
	bkPane *pane = GetPane();
	int index = col * 3;

	m_windowHandles[row][index] = pane->AddWindow( this, "STATIC", 0, SS_CENTER | SS_NOPREFIX, text[col], GetTooltip() );

	m_windowHandles[row][index + 1] = pane->AddWindow( this, m_step == 0.0f ? "STATIC" : "EDIT", 
		0, m_step == 0.0f ? SS_CENTER | SS_NOPREFIX : ES_AUTOHSCROLL | ES_RIGHT | WS_BORDER, "", GetTooltip() );

	if ( m_windowHandles[row][index + 1] && (m_step > 0.0f) )
	{
		s_oldWindowEditMessageCallback = (WNDPROC)SetWindowLongPtr( m_windowHandles[row][index + 1], GWLP_WNDPROC,
			(LONG_PTR)WindowEditMessageCallback );
	}

	if ( (row < GetNumRows() - 1) || (col < GetNumColumns() - 1) )
	{
		m_windowHandles[row][index + 2] = pane->AddWindow( this, "SCROLLBAR", 0, SBS_VERT );

		if ( m_windowHandles[row][index + 2] && (m_step > 0.0f) )
		{
			s_oldMatrixWindowScrollMessageCallback = (WNDPROC)SetWindowLongPtr( m_windowHandles[row][index + 2], GWLP_WNDPROC,
				(LONG_PTR)MatrixWindowScrollMessageCallback );
		}
	}
	else
	{
		pane->AddLastWindow( this, "SCROLLBAR", 0, SBS_VERT );

		if ( m_Hwnd && (m_step > 0.0f) )
		{
			s_oldMatrixWindowScrollMessageCallback = (WNDPROC)SetWindowLongPtr( m_Hwnd, GWLP_WNDPROC,
				(LONG_PTR)MatrixWindowScrollMessageCallback );
		}
	}
}

void bkMatrix::WindowUpdate()
{
	for ( int i = 0; i < GetNumRows(); ++i )
	{
		for ( int j = 0; j < GetNumColumns(); ++j )
		{
			int index = j * 3;
			char buffer[128];
			SetWindowText( m_windowHandles[i][index + 1], GetString( i, j, buffer, sizeof(buffer) ) );
		}
	}	
}

void bkMatrix::WindowDestroy()
{
	if ( m_titleWindowHandle != NULL )
	{
		DestroyWindow( m_titleWindowHandle );
		m_titleWindowHandle = NULL;
	}

	for ( int i = 0; i < 4; ++i )
	{
		if ( m_rowTitleWindowHandles[i] != NULL )
		{
			DestroyWindow( m_rowTitleWindowHandles[i] );
			m_rowTitleWindowHandles[i] = NULL;
		}
	}

	for ( int i = 0; i < 4; ++i )
	{
		for ( int j = 0; j < 12; ++j )
		{
			if ( m_windowHandles[i][j] != NULL )
			{
				DestroyWindow( m_windowHandles[i][j] );
				m_windowHandles[i][j] = NULL;
			}
		}
	}

	bkWidget::WindowDestroy();
}

rageLRESULT bkMatrix::WindowMessage( rageUINT msg, rageWPARAM wParam, rageLPARAM /*lParam*/ )
{
	if ( msg == WM_VSCROLL )
	{
		if ( LOWORD(wParam) == SB_LINEUP )
		{				
			WindowMessage( DIAL, +1 );
		}
		else if ( LOWORD(wParam) == SB_LINEDOWN )
		{
			WindowMessage( DIAL, -1 );
		}
	}

	return 0;
}

void bkMatrix::WindowMessage( Action action, float value )
{
	if ( (action == DIAL) && (m_step > 0.0f) )
	{ 
		SetWindowInFocus( bkPane::WindowMessageHwnd );

		if ( SetValue( m_rowInFocus, m_colInFocus, GetValue( m_rowInFocus, m_colInFocus ) + (value * m_step) ) )
		{
			WindowUpdate();
			Changed();
		}
	}
}

int bkMatrix::WindowResize( int x, int y, int width, int height )
{
	int itemWidth = (width / 5);

	if ( m_titleWindowHandle != NULL )
	{
		SetWindowPos( m_titleWindowHandle, 0, x, y, width, height, SWP_NOACTIVATE | SWP_NOZORDER );
	}

	for ( int i = 0; i < GetNumRows(); ++i )
	{
		int rowY = y + (height * (i + 1));
		SetWindowPos( m_rowTitleWindowHandles[i], 0, x, rowY, itemWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );

		for ( int j = 0; j < GetNumColumns(); ++j )
		{
			ResizeComponentWindow( i, j, x + ((j + 1) * itemWidth), rowY, itemWidth, height );
		}
	}	

	return y + (height * (GetNumRows() + 1));
}

void bkMatrix::ResizeComponentWindow( int row, int col, int x, int y, int width, int height )
{
	const int scrollWidth = GetSystemMetrics( SM_CXVSCROLL );

	int index = col * 3;

	if ( m_windowHandles[row][index] != NULL )
	{
		SetWindowPos( m_windowHandles[row][index], 0, x, y, scrollWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );

		int itemWidth = width - (scrollWidth * 2);
		if ( itemWidth < 0 )
		{
			itemWidth = 0;
		}

		SetWindowPos( m_windowHandles[row][index + 1], 0, x + scrollWidth, y, itemWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );

		if ( (row < GetNumRows() - 1) || (col < GetNumColumns() - 1) )
		{
			SetWindowPos( m_windowHandles[row][index + 2], 0, x + scrollWidth + itemWidth, y, scrollWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );
		}
		else
		{
			if ( m_Hwnd != NULL )
			{
				SetWindowPos( m_Hwnd, 0, x + scrollWidth + itemWidth, y, scrollWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );
			}
		}
	}
}

#define MAKE_CASE_CHECK(state,chr)	(state | (chr << 16))

bool bkMatrix::ValidateString( const char* string, bool okOnly ) const
{
	enum EnumState		{STATE_INITIAL,STATE_IPART,STATE_FPART,STATE_ESIGN,STATE_EPART};
	enum EnumValidation {VALID_START,VALID_ERROR,VALID_PARTIAL,VALID_OK};

	// culled from http://www.codeproject.com/editctrl/validator.asp
	EnumState		state			= STATE_INITIAL;
	EnumValidation	validateState	= VALID_START;
	int				StringLengthgth		= StringLength(string);
	for ( int i = 0; validateState != VALID_ERROR && i < StringLengthgth; )
	{ 
		/* scan string */
		char ch = string[i];
		switch(MAKE_CASE_CHECK(state, ch))
		{ /* states */
		case MAKE_CASE_CHECK(STATE_INITIAL, ' '):
		case MAKE_CASE_CHECK(STATE_INITIAL, '\t'):
			i++;
			continue;

		case MAKE_CASE_CHECK(STATE_INITIAL, '+'):
			i++;
			validateState = VALID_PARTIAL;
			state = STATE_IPART;
			continue;

		case MAKE_CASE_CHECK(STATE_INITIAL, '-'):
			if ( m_minimum < 0.0f )
			{
				i++;
				validateState = VALID_PARTIAL;
				state = STATE_IPART;
			}
			else 
			{
				validateState = VALID_ERROR;
			}
			continue;

		case MAKE_CASE_CHECK(STATE_INITIAL, '0'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '1'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '2'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '3'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '4'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '5'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '6'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '7'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '8'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '9'):
			state = STATE_IPART;
			continue;

		case MAKE_CASE_CHECK(STATE_INITIAL, '.'):
			i++;
			validateState = VALID_PARTIAL;
			state = STATE_FPART;
			continue;

		case MAKE_CASE_CHECK(STATE_INITIAL, 'E'):
		case MAKE_CASE_CHECK(STATE_INITIAL, 'e'):
			i++;
			state = STATE_ESIGN;
			validateState = VALID_PARTIAL;
			continue;

		case MAKE_CASE_CHECK(STATE_IPART, '0'):
		case MAKE_CASE_CHECK(STATE_IPART, '1'):
		case MAKE_CASE_CHECK(STATE_IPART, '2'):
		case MAKE_CASE_CHECK(STATE_IPART, '3'):
		case MAKE_CASE_CHECK(STATE_IPART, '4'):
		case MAKE_CASE_CHECK(STATE_IPART, '5'):
		case MAKE_CASE_CHECK(STATE_IPART, '6'):
		case MAKE_CASE_CHECK(STATE_IPART, '7'):
		case MAKE_CASE_CHECK(STATE_IPART, '8'):
		case MAKE_CASE_CHECK(STATE_IPART, '9'):
			i++;
			validateState = VALID_OK;
			continue;

		case MAKE_CASE_CHECK(STATE_IPART, '.'):
			i++;
			validateState = VALID_OK;
			state = STATE_FPART;
			continue;

		case MAKE_CASE_CHECK(STATE_IPART, 'e'):
		case MAKE_CASE_CHECK(STATE_IPART, 'E'):
			i++;
			validateState = VALID_PARTIAL;
			state = STATE_ESIGN;
			continue;

		case MAKE_CASE_CHECK(STATE_FPART, '0'):
		case MAKE_CASE_CHECK(STATE_FPART, '1'):
		case MAKE_CASE_CHECK(STATE_FPART, '2'):
		case MAKE_CASE_CHECK(STATE_FPART, '3'):
		case MAKE_CASE_CHECK(STATE_FPART, '4'):
		case MAKE_CASE_CHECK(STATE_FPART, '5'):
		case MAKE_CASE_CHECK(STATE_FPART, '6'):
		case MAKE_CASE_CHECK(STATE_FPART, '7'):
		case MAKE_CASE_CHECK(STATE_FPART, '8'):
		case MAKE_CASE_CHECK(STATE_FPART, '9'):
			i++;
			validateState = VALID_OK;
			continue;

		case MAKE_CASE_CHECK(STATE_FPART, 'e'):
		case MAKE_CASE_CHECK(STATE_FPART, 'E'):
			i++;
			validateState = VALID_PARTIAL;
			state = STATE_ESIGN;
			continue;

		case MAKE_CASE_CHECK(STATE_ESIGN, '+'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '-'):
			i++;
			validateState = VALID_PARTIAL;
			state = STATE_EPART;
			continue;

		case MAKE_CASE_CHECK(STATE_ESIGN, '0'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '1'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '2'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '3'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '4'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '5'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '6'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '7'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '8'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '9'):
			state = STATE_EPART;
			continue;

		case MAKE_CASE_CHECK(STATE_EPART, '0'):
		case MAKE_CASE_CHECK(STATE_EPART, '1'):
		case MAKE_CASE_CHECK(STATE_EPART, '2'):
		case MAKE_CASE_CHECK(STATE_EPART, '3'):
		case MAKE_CASE_CHECK(STATE_EPART, '4'):
		case MAKE_CASE_CHECK(STATE_EPART, '5'):
		case MAKE_CASE_CHECK(STATE_EPART, '6'):
		case MAKE_CASE_CHECK(STATE_EPART, '7'):
		case MAKE_CASE_CHECK(STATE_EPART, '8'):
		case MAKE_CASE_CHECK(STATE_EPART, '9'):
			i++;
			validateState = VALID_OK;
			continue;

		default:
			validateState = VALID_ERROR;
			continue;
		} /* states */
	} /* scan string */

	return (validateState == VALID_OK) || (!okOnly && (validateState == VALID_PARTIAL));
}

bool bkMatrix::CheckKey( char key ) const
{	
	struct HWND__* hwnd = m_windowHandles[m_rowInFocus][(m_colInFocus * 3) + 1];

	char temp1[128];
	GetWindowText( hwnd, temp1, 128 );

	unsigned int start;
	unsigned int end;
	SendMessage( hwnd, EM_GETSEL, (WPARAM)&start, (LPARAM)&end ); 

	// backspace key:
	if ( key == 0x08 )
	{
		return true;
	}
	else if ( ((key >= '0') && (key <= '9')) || (key == '-') || (key == '.') || (key == '+') || (key == 'e') || (key == 'E') )
	{
		// check to see if the created string would be valid:
		char temp2[256];
		if ( start != 0 )
		{
			strncpy( temp2, temp1, start );
		}

		const char* postfix = temp1 + end;
		temp2[start] = key;
		temp2[start+1] = NULL;
		safecat( temp2, postfix );

		return ValidateString( temp2, false );
	}
	else 
	{
		return false;
	}
}

void bkMatrix::SetFont( bool bold ) 
{
	struct HWND__* hwnd = m_windowHandles[m_rowInFocus][(m_colInFocus * 3) + 1];

	if ( GetPane() )
	{
		if ( bold )
		{
			SendMessage( hwnd, WM_SETFONT, (WPARAM)GetPane()->GetBoldFont(), MAKELPARAM(TRUE, 0) );	
		}
		else
		{
			SendMessage( hwnd, WM_SETFONT, (WPARAM)GetPane()->GetFont(), MAKELPARAM(TRUE, 0) );	
		}
	}
}

void bkMatrix::SetComponentInFocus( int row, int col, bool setWindowFocus )
{
	if ( ((row != m_rowInFocus) || (col != m_colInFocus))
		&& (row >= 0) && (row < GetNumRows()) && (col >= 0) && (col < GetNumColumns()) )
	{
		int index = m_colInFocus * 3;

		// deselect any highlighted text
		if ( m_windowHandles[m_rowInFocus][index + 1] != NULL )
		{
			::SendMessage( m_windowHandles[m_rowInFocus][index + 1], EM_SETSEL, 0, 0 );
		}

		m_rowInFocus = row;
		m_colInFocus = col;
		index = m_colInFocus * 3;

		if ( setWindowFocus && (m_windowHandles[m_rowInFocus][index + 1] != NULL) )
		{
			::SendMessage( m_windowHandles[m_rowInFocus][index + 1], WM_SETFOCUS, 0, 0 );
		}

		bkDisplayf( "ComponentInFocus=%d,%d", m_rowInFocus, m_colInFocus );
	}
}

void bkMatrix::SetWindowInFocus( struct HWND__* hwnd )
{
	int newRow = m_rowInFocus;
	int newCol = m_colInFocus;

	bool found = false;
	for ( int i = 0; i < 4; ++i )
	{
		for ( int j = 0; j < 12; ++j )
		{
			if ( m_windowHandles[i][j] == hwnd )
			{
				newRow = i;
				
				switch ( j )
				{
				case 1:
				case 2:
					newCol = 0;				
					break;
				case 4:
				case 5:
					newCol = 1;
					break;
				case 7:
				case 8:
					newCol = 2;
					break;
				case 10:
				case 11:
					newCol = 3;
					break;
				}

				break;
			}
		}

		if ( found )
		{
			break;
		}
	}

	SetComponentInFocus( newRow, newCol, false );
}

struct HWND__* bkMatrix::GetWindowInFocus() const
{
	return m_windowHandles[m_rowInFocus][(m_colInFocus * 3) + 1];
}

#endif // __WIN32PC

void bkMatrix::Message( Action action, float value )
{
	if ( action == DIAL )
	{
		if ( sm_Cursor - sm_TopLine == m_drawLocalTopY )
		{
			return;
		}

		int rowA = m_drawLocalTopY + 1;
		for ( int i = 0; i < GetNumRows(); ++i )
		{
			int component = sm_Cursor - sm_TopLine - rowA;
			if ( component == 0 )
			{
				if ( value > 0.0f )
				{
					m_drawLocalExpanded[i] = -1;
				}
				else if ( value < 0.0f )
				{
					m_drawLocalExpanded[i] = 0;
				}

				break;
			}
			else if ( m_drawLocalExpanded[i] > 0 )
			{
				if ( component - 1 < GetNumColumns() )
				{
					if ( (m_step > 0.0f) && SetValue( i, component - 1, GetValue( i, component - 1 ) + (value * m_step) ) )
					{
						WindowUpdate();
						Changed();
					}
					break;
				}
				else
				{
					rowA += GetNumColumns() + 1;	// add one for the "Row #" line
				}
			}
			else
			{
				++rowA;
			}
		}
	}
}

int bkMatrix::DrawLocal( int x, int y )
{
	m_drawLocalTopY = y;

	Drawf( x, y, m_Title );
	y = bkWidget::DrawLocal( x, y );
	
	switch ( m_enumType )
	{
	case EMatrix33:
		for ( int i = 0; i < GetNumRows(); ++i )
		{
			if ( m_drawLocalExpanded[i] )
			{
				m_drawLocalExpanded[i] = y;

				Drawf( x, y, "  (-)Row %d", i );
				y = bkWidget::DrawLocal( x, y );

				static const char componentNames[4][2] = { "X", "Y", "Z", "W" };
				for ( int j = 0; j < GetNumColumns(); ++j )
				{
					char value[32];
					GetString( i, j, value, sizeof(value) );

					Drawf( x, y, "    %s %s", componentNames[j], value );
					y = bkWidget::DrawLocal( x, y );
				}
			}
			else
			{
				char value0[32];
				GetString( i, 0, value0, sizeof(value0) );

				char value1[32];
				GetString( i, 1, value1, sizeof(value1) );

				char value2[32];
				GetString( i, 2, value2, sizeof(value2) );

				Drawf( x, y, "  (+) %s %s %s", value0, value1, value2 );
				y = bkWidget::DrawLocal( x, y );
			}
		}
		break;
	case EMatrix34:
	case EMatrix44:
		for ( int i = 0; i < GetNumRows(); ++i )
		{
			if ( m_drawLocalExpanded[i] )
			{
				m_drawLocalExpanded[i] = y;

				Drawf( x, y, "  (-)Row %d", i );
				y = bkWidget::DrawLocal( x, y );

				static const char componentNames[4][2] = { "X", "Y", "Z", "W" };
				for ( int j = 0; j < GetNumColumns(); ++j )
				{
					char value[32];
					GetString( i, j, value, sizeof(value) );

					Drawf( x, y, "    %s %s", componentNames[j], value );
					y = bkWidget::DrawLocal( x, y );
				}
			}
			else
			{
				char value0[32];
				GetString( i, 0, value0, sizeof(value0) );

				char value1[32];
				GetString( i, 1, value1, sizeof(value1) );

				char value2[32];
				GetString( i, 2, value2, sizeof(value2) );

				char value3[32];
				GetString( i, 3, value3, sizeof(value3) );

				Drawf( x, y, "  (+) %s %s %s", value0, value1, value2, value3 );
				y = bkWidget::DrawLocal( x, y );
			}
		}
		break;
	}

	return y;
}

void bkMatrix::RemoteHandler( const bkRemotePacket& packet, EnumType enumType )
{
	if ( packet.GetCommand() == bkRemotePacket::CREATE ) 
	{
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;

		const char *title = packet.Read_const_char(), *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();

		float values[4][4];

		int rows = 0;
		int cols = 0;
		switch ( enumType )
		{
		case EMatrix33:
			rows = 3;
			cols = 3;
			break;
		case EMatrix34:
			rows = 4;
			cols = 3;
			break;
		case EMatrix44:
			rows = 4;
			cols = 4;
			break;
		}

		for ( int i = 0; i < rows; ++i )
		{
			for ( int j = 0; j < cols; ++j )
			{
				values[i][j] = packet.Read_float();
			}
		}

		float min = packet.Read_float();
		float max = packet.Read_float();
		float step = packet.Read_float();
		packet.End();

		bkMatrix *widget = NULL;
		switch ( enumType )
		{
		case EMatrix33:
			widget = rage_new bkMatrix33( NullCB, title, memo, 
				rage_new Matrix33( values[0][0], values[0][1], values[0][2], 
					values[1][0], values[1][1], values[1][2], 
					values[2][0], values[2][1], values[2][2] ), 
				min, max, step, fillColor, readOnly );
			break;
		case EMatrix34:
			widget = rage_new bkMatrix34( NullCB, title, memo, 
				rage_new Matrix34( values[0][0], values[0][1], values[0][2], 
					values[1][0], values[1][1], values[1][2], 
					values[2][0], values[2][1], values[2][2], 
					values[3][0], values[3][1], values[3][2] ), 
				min, max, step, fillColor, readOnly );
			break;
		case EMatrix44:
			widget = rage_new bkMatrix44( NullCB, title, memo, 
				rage_new Matrix44( values[0][0], values[0][1], values[0][2], values[0][3], 
					values[1][0], values[1][1], values[1][2], values[1][3], 
					values[2][0], values[2][1], values[2][2], values[2][3], 
					values[3][0], values[3][1], values[3][2], values[3][3] ), 
				min, max, step, fillColor, readOnly );
			break;
		}

		if ( widget != NULL )
		{
			bkRemotePacket::SetWidgetId( *widget, id );
			parent->AddChild( *widget );
		}
	}
	else if (packet.GetCommand() == bkRemotePacket::CHANGED) 
	{
		packet.Begin();

		bkMatrix *widget = NULL;
		switch ( enumType )
		{
		case EMatrix33:
			widget = packet.ReadWidget<bkMatrix33>();
			break;
		case EMatrix34:
			widget = packet.ReadWidget<bkMatrix34>();
			break;
		case EMatrix44:
			widget = packet.ReadWidget<bkMatrix44>();
			break;
		}

		if ( !widget )
		{
			return;
		}

		widget->ReadValues( packet );

		packet.End();

		widget->WindowUpdate();
		widget->Changed();
	}
	else if ( (packet.GetCommand() >= bkRemotePacket::USER)
		&& (packet.GetCommand() <= bkRemotePacket::USER + 11) )
	{
		packet.Begin();

		bkMatrix *widget = NULL;
		switch ( enumType )
		{
		case EMatrix33:
			widget = packet.ReadWidget<bkMatrix33>();
			break;
		case EMatrix34:
			widget = packet.ReadWidget<bkMatrix34>();
			break;
		case EMatrix44:
			widget = packet.ReadWidget<bkMatrix44>();
			break;
		}

		if ( !widget )
		{
			return;
		}

		Operation op = (Operation)packet.Read_s32();

		switch ( packet.GetCommand() )
		{
		case bkRemotePacket::USER:
			{
				widget->ExecuteOperation( op );
			}
			break;
		case bkRemotePacket::USER + 1:
			{
				float f = packet.Read_float();

				widget->ExecuteOperation( op, f );
			}
			break;
		case bkRemotePacket::USER + 2:
			{
				float f1 = packet.Read_float();
				float f2 = packet.Read_float();
				float f3 = packet.Read_float();

				widget->ExecuteOperation( op, f1, f2, f3 );
			}
			break;
		case bkRemotePacket::USER + 3:
			{
				float f1 = packet.Read_float();
				float f2 = packet.Read_float();
				float f3 = packet.Read_float();
				float f4 = packet.Read_float();

				widget->ExecuteOperation( op, f1, f2, f3, f4 );
			}
			break;
		case bkRemotePacket::USER + 4:
			{
				float v[4];

				if ( op == EMirrorOnPlane )
				{
					// special case: this is always a Vector4
					for ( int i = 0; i < 4; ++i )
					{
						v[i] = packet.Read_float();
					}
				}
				else
				{
					widget->ReadValues( packet, v );
				}

				widget->ExecuteOperation( op, v );
			}
			break;
		case bkRemotePacket::USER + 5:
			{
				float v1[4];
				widget->ReadValues( packet, v1 );

				float v2[4];
				widget->ReadValues( packet, v2 );

				widget->ExecuteOperation( op, v1, v2 );
			}
			break;
		case bkRemotePacket::USER + 6:
			{
				float v1[4];
				widget->ReadValues( packet, v1 );

				float v2[4];
				widget->ReadValues( packet, v2 );

				float f = packet.Read_float();

				widget->ExecuteOperation( op, v1, v2, f );
			}
			break;
		case bkRemotePacket::USER + 7:
			{
				float v[4];
				widget->ReadValues( packet, v );
				float f = packet.Read_float();

				widget->ExecuteOperation( op, v, f );
			}
			break;
		case bkRemotePacket::USER + 8:
			{
				float v[4];
				widget->ReadValues( packet, v );
				float f1 = packet.Read_float();
				float f2 = packet.Read_float();

				widget->ExecuteOperation( op, v, f1, f2 );
			}
			break;
		case bkRemotePacket::USER + 9:
			{
				float m[4][4];
				widget->ReadValues( packet, m );

				widget->ExecuteOperation( op, m );
			}
			break;
		case bkRemotePacket::USER + 10:
			{
				float m[4][4];
				widget->ReadValues( packet, m );
				float f = packet.Read_float();

				widget->ExecuteOperation( op, m, f );
			}
			break;
		case bkRemotePacket::USER + 11:
			{
				float f = packet.Read_float();
				int i = packet.Read_s32();

				widget->ExecuteOperation( op, f, i );
			}
			break;
		default:
			break;
		}

		widget->ClampValuesToAllowedRange();

		packet.End();

		widget->WindowUpdate();
		widget->Changed();
	}
}

void bkMatrix::CheckInitialValues()
{
	char buffer[256];

	if ( (m_minimum < FLOAT_MIN_VALUE) || (m_maximum > FLOAT_MAX_VALUE) )
	{
		formatf( buffer, sizeof(buffer), 
			"Widget '%s' min and max values (%.3f,%.3f) are out of allowed range.\n\nClick Ok to clamp it and continue, or Cancel to exit.",
			GetTitle(), m_minimum, m_maximum ); 
		bkErrorf( "%s", buffer ); 
		sysStack::PrintStackTrace();

		int result = fiRemoteShowMessageBox( buffer, sysParam::GetProgramName(), MB_ICONERROR | MB_OKCANCEL | MB_TOPMOST, IDOK );
		if ( result == IDOK ) 
		{ 
			m_minimum = Clamp( m_minimum, FLOAT_MIN_VALUE, FLOAT_MAX_VALUE ); 
			m_maximum = Clamp( m_maximum, FLOAT_MIN_VALUE, FLOAT_MAX_VALUE ); 
		} 
		else
		{
			Quitf("User-requested abort."); 
		}
	} 

	if ( m_step )
	{
		static const char componentNames[4][2] = { "X", "Y", "Z", "W" };
		for ( int i = 0; i < GetNumRows(); ++i )
		{
			for ( int j = 0; j < GetNumColumns(); ++j )
			{
				float value = GetValue( i, j );
				if ( !((value >= m_minimum) && (value <= m_maximum)) ) 
				{ 
					formatf( buffer, sizeof(buffer), 
						"Widget '%s' initial value Row%d %s=%.3f out of allowed range (%.3f,%.3f).\n\nClick Ok to clamp it and continue, or Cancel to exit.", 
						GetTitle(), i, componentNames[j], value, m_minimum, m_maximum ); 
					bkErrorf( "%s", buffer ); 
					sysStack::PrintStackTrace();

					int result = fiRemoteShowMessageBox( buffer, sysParam::GetProgramName(), MB_ICONERROR | MB_OKCANCEL | MB_TOPMOST, IDOK );
					if ( result == IDOK ) 
					{
						SetValue( i, j, Clamp( value, m_minimum, m_maximum ) );
					}
					else 
					{
						Quitf( "User-requested abort." ); 
					}
				} 
			}
		}
	}
}

//#############################################################################

bkMatrix33::bkMatrix33( datCallback &callback, const char *title, const char *memo, Matrix33 *data, float min, float max, float step, const char* fillColor, bool readOnly )
: bkMatrix( callback, title, memo, EMatrix33, min, max, step, fillColor, readOnly )
, m_pValue(data), m_prevValue(*data)
{
	CheckInitialValues();
}

bkMatrix33::~bkMatrix33()
{

}

void bkMatrix33::ReadValues( const bkRemotePacket& packet )
{
	float mtx[4][4];
	ReadValues( packet, mtx );

	m_pValue->Set( mtx[0][0], mtx[0][1], mtx[0][2], 
		mtx[1][0], mtx[1][1], mtx[1][2], 
		mtx[2][0], mtx[2][1], mtx[2][2] );

	m_prevValue = *m_pValue;
}

void bkMatrix33::ReadValues( const bkRemotePacket& packet, float mtx[4][4] )
{
	for ( int i = 0; i < 3; ++i )
	{
		for ( int j = 0; j < 3; ++j )
		{
			mtx[i][j] = packet.Read_float();
		}
	}
}

void bkMatrix33::ReadValues( const bkRemotePacket& packet, float vec[4] )
{
	for ( int i = 0; i < 3; ++i )
	{
		vec[i] = packet.Read_float();
	}
}

void bkMatrix33::WriteValues( bkRemotePacket& packet )
{
	for ( int i = 0; i < 3; ++i )
	{
		for ( int j = 0; j < 3; ++j )
		{
			packet.Write_float( m_pValue->GetElement( i, j ) );
		}
	}
}

void bkMatrix33::ExecuteOperation( Operation op )
{
	switch ( op )
	{
	case EIdentity:
		m_pValue->Identity();
		break;
	case EZero:
		m_pValue->Zero();
		break;
	case EAbs:
		m_pValue->Abs();
		break;
	case ENegate:
		m_pValue->Negate();
		break;
	case EMakeUpright:
		m_pValue->MakeUpright();
		break;
	case EInverse:
		m_pValue->Inverse();
		break;
	case EFastInverse:
		m_pValue->FastInverse();
		break;
	case ETranspose:
		m_pValue->Transpose();
		break;
	case ECoordinateInverseSafe:
		m_pValue->CoordinateInverseSafe();
		break;
	case ENormalize:
		m_pValue->Normalize();
		break;
	case ENormalizeSafe:
		m_pValue->NormalizeSafe();
		break;
	default:
		break;
	}
}

void bkMatrix33::ExecuteOperation( Operation op, float f )
{
	switch ( op )
	{
	case ERotateX:
		m_pValue->RotateX( f );
		break;
	case ERotateY:
		m_pValue->RotateY( f );
		break;
	case ERotateZ:
		m_pValue->RotateZ( f );
		break;
	case ERotateLocalX:
		m_pValue->RotateLocalX( f );
		break;
	case ERotateLocalY:
		m_pValue->RotateLocalY( f );
		break;
	case ERotateLocalZ:
		m_pValue->RotateLocalZ( f );
		break;
	case EMakeRotateX:
		m_pValue->MakeRotateX( f );
		break;
	case EMakeRotateY:
		m_pValue->MakeRotateY( f );
		break;
	case EMakeRotateZ:
		m_pValue->MakeRotateZ( f );
		break;
	case EScale:
		m_pValue->Scale( f );
		break;
	case EMakeScale:
		m_pValue->MakeScale( f );
		break;
	default:
		break;
	}
}

void bkMatrix33::ExecuteOperation( Operation op, float v[] )
{
	switch ( op )
	{
	case ESetDiagonal:
		m_pValue->SetDiagonal( Vector3( v[0], v[1], v[2] ) );
		break;
	case ECrossProduct:
		m_pValue->CrossProduct( Vector3( v[0], v[1], v[2] ) );
		break;
	case EDotCrossProdMtx:
		m_pValue->DotCrossProdMtx( Vector3( v[0], v[1], v[2] ) );
		break;
	case EDotCrossProdTranspose:
		m_pValue->DotCrossProdTranspose( Vector3( v[0], v[1], v[2] ) );
		break;
	case EMirrorOnPlane:
		m_pValue->MirrorOnPlane( Vector4( v[0], v[1], v[2], v[4] ) );
		break;
	default:
		break;
	}
}

void bkMatrix33::ExecuteOperation( Operation op, float v1[], float v2[] )
{
	switch ( op )
	{
	case EOuterProduct:
		m_pValue->OuterProduct( Vector3( v1[0], v1[1], v1[2] ), Vector3( v2[0], v2[1], v2[2] ) );
		break;
	case EMakeRotateTo:
		m_pValue->MakeRotateTo( Vector3( v1[0], v1[1], v1[2] ), Vector3( v2[0], v2[1], v2[2] ) );
		break;
	case EMakeDoubleCrossMatrix:
		m_pValue->MakeDoubleCrossMatrix( Vector3( v1[0], v1[1], v1[2] ), Vector3( v2[0], v2[1], v2[2] ) );
		break;
	default:
		break;
	}
}

void bkMatrix33::ExecuteOperation( Operation op, float v1[], float v2[], float f )
{
	switch ( op )
	{
	case ERotateTo:
		m_pValue->RotateTo( Vector3( v1[0], v1[1], v1[2] ), Vector3( v2[0], v2[1], v2[2] ), f );
		break;
	default:
		break;
	}
}

void bkMatrix33::ExecuteOperation( Operation op, float v[], float f )
{
	switch ( op )
	{
	case ERotateUnitAxis:
		m_pValue->RotateUnitAxis( Vector3( v[0], v[1], v[2] ), f );
		break;
	case ERotate:
		m_pValue->Rotate( Vector3( v[0], v[1], v[2] ), f );
		break;
	case EMakeRotateUnitAxis:
		m_pValue->MakeRotateUnitAxis( Vector3( v[0], v[1], v[2] ), f );
		break;
	case EMakeRotate:
		m_pValue->MakeRotate( Vector3( v[0], v[1], v[2] ), f );
		break;
	default:
		break;
	}
}

void bkMatrix33::ExecuteOperation( Operation op, float m[4][4]  )
{
	switch ( op )
	{
	case EAdd:
		m_pValue->Add( Matrix33( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2] ) );
		break;
	case ESubtract:
		m_pValue->Subtract( Matrix33( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2] ) );
		break;
	case EDot:
		m_pValue->Dot( Matrix33( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2] ) );
		break;
	case EDot3x3:
		m_pValue->Dot3x3( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2]) );
		break;
	case EDotFromLeft:
		m_pValue->DotFromLeft( Matrix33( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2] ) );
		break;
	case EDotTranspose:
		m_pValue->DotTranspose( Matrix33( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2] ) );
		break;
	default:
		break;
	}
}

void bkMatrix33::ExecuteOperation( Operation op, float f, int i )
{
	switch ( op )
	{
	case ERotateLocalAxis:
		m_pValue->RotateLocalAxis( f, i );
		break;
	default:
	    break;
	}
}

void bkMatrix33::ClampValuesToAllowedRange()
{
	float m[4][4];

	for ( int i = 0; i < 3; ++i )
	{
		for ( int j = 0; j < 3; ++j )
		{
			float f = m_pValue->GetElement( i, j );
			if ( f < m_minimum )
			{
				m[i][j] = m_minimum;
			}
			else if ( f > m_maximum )
			{
				m[i][j] = m_maximum;
			}
			else
			{
				m[i][j] = f;
			}
		}
	}

	m_pValue->Set( m[0][0], m[0][1], m[0][2],
		m[1][0], m[1][1], m[1][2],
		m[2][0], m[2][1], m[2][2] );
}

bool bkMatrix33::SetValue( int row, int col, float f )
{
	if ( (row >= 0) && (row < 3) && (col >= 0) && (col < 3) ) 
	{ 
		Vector3 *pRowVec = NULL;
		switch ( row )
		{
		case 0:
			pRowVec = &m_pValue->a;
			break;
		case 1:
			pRowVec = &m_pValue->b;
			break;
		case 2:
			pRowVec = &m_pValue->c;
			break;
		}

		float value = Clamp( f, m_minimum, m_maximum );
		float oldValue = value;
		
		switch ( col )
		{
		case 0:
			oldValue = pRowVec->GetX();
			pRowVec->SetX( value );
			break;
		case 1:
			oldValue = pRowVec->GetY();
			pRowVec->SetY( value );
			break;
		case 2:
			oldValue = pRowVec->GetZ();
			pRowVec->SetZ( value );
			break;
		}

		return value != oldValue; 
	} 
	else
	{
		return false;
	} 
}

float bkMatrix33::GetValue( int row, int col )
{
	if ( (row >= 0) && (row < 3) && (col >= 0) && (col < 3) ) 
	{ 
		Vector3 *pRowVec = NULL;
		switch ( row )
		{
		case 0:
			pRowVec = &m_pValue->a;
			break;
		case 1:
			pRowVec = &m_pValue->b;
			break;
		case 2:
			pRowVec = &m_pValue->c;
			break;
		}

		switch ( col )
		{
		case 0:
			return pRowVec->GetX();
		case 1:
			return pRowVec->GetY();
		case 2:
			return pRowVec->GetZ();
		}
	} 

	return 0.0f;
}

bool bkMatrix33::SetString( int row, int col, const char* src )
{
	return SetValue( row, col, (float)atof( src ) );
}

const char* bkMatrix33::GetString( int row, int col, char* dest, int destSize )
{
	float val = GetValue( row, col );
	if ( (val >= m_minimum) && (val <= m_maximum) )
	{
		formatf( dest, destSize, "%1.03f", val );
	}
	else
	{
		formatf( dest, destSize, "**invalid**" );
	}

	return dest;	
}

void bkMatrix33::RemoteHandler( const bkRemotePacket& packet )
{
	bkMatrix::RemoteHandler( packet, EMatrix33 );
}

//#############################################################################

bkMatrix34::bkMatrix34( datCallback &callback, const char *title, const char *memo, Matrix34 *data, float min, float max, float step, const char* fillColor, bool readOnly )
: bkMatrix( callback, title, memo, EMatrix34, min, max, step, fillColor, readOnly )
, m_pValue(data), m_prevValue(*data)
{
	CheckInitialValues();
}

bkMatrix34::~bkMatrix34()
{

}

void bkMatrix34::ReadValues( const bkRemotePacket& packet )
{
	float mtx[4][4];
	ReadValues( packet, mtx );

	m_pValue->Set( mtx[0][0], mtx[0][1], mtx[0][2], 
		mtx[1][0], mtx[1][1], mtx[1][2], 
		mtx[2][0], mtx[2][1], mtx[2][2], 
		mtx[3][0], mtx[3][1], mtx[3][2] );

	m_prevValue = *m_pValue;
}

void bkMatrix34::ReadValues( const bkRemotePacket& packet, float mtx[4][4] )
{
	for ( int i = 0; i < 4; ++i )
	{
		for ( int j = 0; j < 3; ++j )
		{
			mtx[i][j] = packet.Read_float();
		}
	}
}

void bkMatrix34::ReadValues( const bkRemotePacket& packet, float vec[4] )
{
	for ( int i = 0; i < 3; ++i )
	{
		vec[i] = packet.Read_float();
	}
}

void bkMatrix34::WriteValues( bkRemotePacket& packet )
{
	for ( int i = 0; i < 4; ++i )
	{
		for ( int j = 0; j < 3; ++j )
		{
			packet.Write_float( m_pValue->GetElement( i, j ) );
		}
	}
}

void bkMatrix34::ExecuteOperation( Operation op )
{
	switch ( op )
	{
	case EIdentity:
		m_pValue->Identity();
		break;
	case EZero:
		m_pValue->Zero();
		break;
	case EAbs:
		m_pValue->Abs();
		break;
	case ENegate:
		m_pValue->Negate();
		break;
	case EMakeUpright:
		m_pValue->MakeUpright();
		break;
	case EInverse:
		m_pValue->Inverse();
		break;
	case EFastInverse:
		m_pValue->FastInverse();
		break;
	case ETranspose:
		m_pValue->Transpose();
		break;
	case ECoordinateInverseSafe:
		m_pValue->CoordinateInverseSafe();
		break;
	case ENormalize:
		m_pValue->Normalize();
		break;
	case ENormalizeSafe:
		m_pValue->NormalizeSafe();
		break;
	case EIdentity3x3:
		m_pValue->Identity3x3();
		break;
	case EZero3x3:
		m_pValue->Zero3x3();
		break;
	case ENegate3x3:
		m_pValue->Negate3x3();
		break;
	case EInverse3x3:
		m_pValue->Inverse3x3();
		break;
	case ETranspose3x4:
		m_pValue->Transpose3x4();
		break;
	default:
		break;
	}
}

void bkMatrix34::ExecuteOperation( Operation op, float f )
{
	switch ( op )
	{
	case ERotateX:
		m_pValue->RotateX( f );
		break;
	case ERotateY:
		m_pValue->RotateY( f );
		break;
	case ERotateZ:
		m_pValue->RotateZ( f );
		break;
	case ERotateLocalX:
		m_pValue->RotateLocalX( f );
		break;
	case ERotateLocalY:
		m_pValue->RotateLocalY( f );
		break;
	case ERotateLocalZ:
		m_pValue->RotateLocalZ( f );
		break;
	case EMakeRotateX:
		m_pValue->MakeRotateX( f );
		break;
	case EMakeRotateY:
		m_pValue->MakeRotateY( f );
		break;
	case EMakeRotateZ:
		m_pValue->MakeRotateZ( f );
		break;
	case ERotateFullX:
		m_pValue->RotateFullX( f );
		break;
	case ERotateFullY:
		m_pValue->RotateFullY( f );
		break;
	case ERotateFullZ:
		m_pValue->RotateFullZ( f );
		break;
	case EScale:
		m_pValue->Scale( f );
		break;
	case EMakeScale:
		m_pValue->MakeScale( f );
		break;
	case EScaleFull:
		m_pValue->ScaleFull( f );
		break;
	default:
		break;
	}
}

void bkMatrix34::ExecuteOperation( Operation op, float f1, float f2, float f3, float f4 )
{
	switch ( op )
	{
	case EPolarView:
		m_pValue->PolarView( f1, f2, f3, f4 );
		break;
	default:
		break;
	}
}

void bkMatrix34::ExecuteOperation( Operation op, float v[] )
{
	switch ( op )
	{
	case ESetDiagonal:
		m_pValue->SetDiagonal( Vector3( v[0], v[1], v[2] ) );
		break;
	case ECrossProduct:
		m_pValue->CrossProduct( Vector3( v[0], v[1], v[2] ) );
		break;
	case EMirrorOnPlane:
		m_pValue->MirrorOnPlane( Vector4( v[0], v[1], v[2], v[4] ) );
		break;
	case ETranslate:
		m_pValue->Translate( Vector3( v[0], v[1], v[2] ) );
		break;
	case EMakeTranslate:
		m_pValue->MakeTranslate( Vector3( v[0], v[1], v[2] ) );
		break;
	case EDot3x3CrossProdMtx:
		m_pValue->Dot3x3CrossProdMtx( Vector3( v[0], v[1], v[2] ) );
		break;
	case EDot3x3CrossProdTranspose:
		m_pValue->Dot3x3CrossProdTranspose( Vector3( v[0], v[1], v[2] ) );
		break;
	default:
		break;
	}
}

void bkMatrix34::ExecuteOperation( Operation op, float v1[], float v2[] )
{
	switch ( op )
	{
	case EOuterProduct:
		m_pValue->OuterProduct( Vector3( v1[0], v1[1], v1[2] ), Vector3( v2[0], v2[1], v2[2] ) );
		break;
	case EMakeRotateTo:
		m_pValue->MakeRotateTo( Vector3( v1[0], v1[1], v1[2] ), Vector3( v2[0], v2[1], v2[2] ) );
		break;
	case EMakeDoubleCrossMatrix:
		m_pValue->MakeDoubleCrossMatrix( Vector3( v1[0], v1[1], v1[2] ), Vector3( v2[0], v2[1], v2[2] ) );
		break;
	case ELookDown:
		m_pValue->LookDown( Vector3( v1[0], v1[1], v1[2] ), Vector3( v2[0], v2[1], v2[2] ) );
		break;
	case ELookAt:
		m_pValue->LookAt( Vector3( v1[0], v1[1], v1[2] ), Vector3( v2[0], v2[1], v2[2] ) );
		break;
	default:
		break;
	}
}

void bkMatrix34::ExecuteOperation( Operation op, float v1[], float v2[], float f )
{
	switch ( op )
	{
	case ERotateTo:
		m_pValue->RotateTo( Vector3( v1[0], v1[1], v1[2] ), Vector3( v2[0], v2[1], v2[2] ), f );
		break;
	default:
		break;
	}
}

void bkMatrix34::ExecuteOperation( Operation op, float v[], float f )
{
	switch ( op )
	{
	case ERotateUnitAxis:
		m_pValue->RotateUnitAxis( Vector3( v[0], v[1], v[2] ), f );
		break;
	case ERotate:
		m_pValue->Rotate( Vector3( v[0], v[1], v[2] ), f );
		break;
	case EMakeRotateUnitAxis:
		m_pValue->MakeRotateUnitAxis( Vector3( v[0], v[1], v[2] ), f );
		break;
	case EMakeRotate:
		m_pValue->MakeRotate( Vector3( v[0], v[1], v[2] ), f );
		break;
	case ERotateFull:
		m_pValue->RotateFull( Vector3( v[0], v[1], v[2] ), f );
		break;
	case ERotateFullUnitAxis:
		m_pValue->RotateFullUnitAxis( Vector3( v[0], v[1], v[2] ), f );
		break;
	default:
		break;
	}
}

void bkMatrix34::ExecuteOperation( Operation op, float m[4][4]  )
{
	switch ( op )
	{
	case EAdd:
		m_pValue->Add( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ) );
		break;
	case ESubtract:
		m_pValue->Subtract( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ) );
		break;
	case EDot:
		m_pValue->Dot( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ) );
		break;
	case EDot3x3:
		m_pValue->Dot3x3( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ) );
		break;
	case EDotFromLeft:
		m_pValue->DotFromLeft( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ) );
		break;
	case EDotTranspose:
		m_pValue->DotTranspose( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ) );
		break;
	case EAdd3x3:
		m_pValue->Add3x3( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ) );
		break;
	case ESubtract3x3:
		m_pValue->Subtract3x3( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ) );
		break;
	case EDot3x3FromLeft:
		m_pValue->Dot3x3FromLeft( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ) );
		break;
	case EDot3x3Transpose:
		m_pValue->Dot3x3Transpose( Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ) );
		break;
	default:
		break;
	}
}

void bkMatrix34::ExecuteOperation( Operation op, float m[4][4], float f )
{
	switch ( op )
	{
	case EInterpolate:
		m_pValue->Interpolate( *m_pValue, Matrix34( m[0][0], m[0][1], m[0][2],
			m[1][0], m[1][1], m[1][2],
			m[2][0], m[2][1], m[2][2],
			m[3][0], m[3][1], m[3][2] ), f );
		break;
	default:
	    break;
	}
}

void bkMatrix34::ExecuteOperation( Operation op, float f, int i )
{
	switch ( op )
	{
	case ERotateLocalAxis:
		m_pValue->RotateLocalAxis( f, i );
		break;
	default:
		break;
	}
}

void bkMatrix34::ClampValuesToAllowedRange()
{
	float m[4][4];

	for ( int i = 0; i < 4; ++i )
	{
		for ( int j = 0; j < 3; ++j )
		{
			float f = m_pValue->GetElement( i, j );
			if ( f < m_minimum )
			{
				m[i][j] = m_minimum;
			}
			else if ( f > m_maximum )
			{
				m[i][j] = m_maximum;
			}
			else
			{
				m[i][j] = f;
			}
		}
	}

	m_pValue->Set( m[0][0], m[0][1], m[0][2],
		m[1][0], m[1][1], m[1][2],
		m[2][0], m[2][1], m[2][2],
		m[3][0], m[3][1], m[3][2] );
}

bool bkMatrix34::SetValue( int row, int col, float f )
{
	if ( (row >= 0) && (row < 4) && (col >= 0) && (col < 3) ) 
	{ 
		Vector3 *pRowVec = NULL;
		switch ( row )
		{
		case 0:
			pRowVec = &m_pValue->a;
			break;
		case 1:
			pRowVec = &m_pValue->b;
			break;
		case 2:
			pRowVec = &m_pValue->c;
			break;
		case 3:
			pRowVec = &m_pValue->d;
			break;
		}

		float value = Clamp( f, m_minimum, m_maximum );
		float oldValue = value;

		switch ( col )
		{
		case 0:
			oldValue = pRowVec->GetX();
			pRowVec->SetX( value );
			break;
		case 1:
			oldValue = pRowVec->GetY();
			pRowVec->SetY( value );
			break;
		case 2:
			oldValue = pRowVec->GetZ();
			pRowVec->SetZ( value );
			break;
		}

		return value != oldValue; 
	} 
	else
	{
		return false;
	} 
}

float bkMatrix34::GetValue( int row, int col )
{
	if ( (row >= 0) && (row < 4) && (col >= 0) && (col < 3) ) 
	{ 
		Vector3 *pRowVec = NULL;
		switch ( row )
		{
		case 0:
			pRowVec = &m_pValue->a;
			break;
		case 1:
			pRowVec = &m_pValue->b;
			break;
		case 2:
			pRowVec = &m_pValue->c;
			break;
		case 3:
			pRowVec = &m_pValue->d;
			break;
		}

		switch ( col )
		{
		case 0:
			return pRowVec->GetX();
		case 1:
			return pRowVec->GetY();
		case 2:
			return pRowVec->GetZ();
		}
	} 

	return 0.0f;
}

bool bkMatrix34::SetString( int row, int col, const char* src )
{
	return SetValue( row, col, (float)atof( src ) );
}

const char* bkMatrix34::GetString( int row, int col, char* dest, int destSize )
{
	float val = GetValue( row, col );
	if ( (val >= m_minimum) && (val <= m_maximum) )
	{
		formatf( dest, destSize, "%1.03f", val );
	}
	else
	{
		safecpy( dest, "**invalid**", destSize );
	}

	return dest;	
}

void bkMatrix34::RemoteHandler( const bkRemotePacket& packet )
{
	bkMatrix::RemoteHandler( packet, EMatrix34 );
}

//#############################################################################

bkMatrix44::bkMatrix44( datCallback &callback, const char *title, const char *memo, Matrix44 *data, float min, float max, float step, const char* fillColor, bool readOnly )
: bkMatrix( callback, title, memo, EMatrix44, min, max, step, fillColor, readOnly )
, m_pValue(data), m_prevValue(*data)
{
	CheckInitialValues();
}

bkMatrix44::~bkMatrix44()
{

}

void bkMatrix44::ReadValues( const bkRemotePacket& packet )
{
	float mtx[4][4];
	ReadValues( packet, mtx );
	Matrix44 m( mtx[0][0], mtx[0][1], mtx[0][2], mtx[0][3], 
		mtx[1][0], mtx[1][1], mtx[1][2], mtx[1][3], 
		mtx[2][0], mtx[2][1], mtx[2][2], mtx[2][3], 
		mtx[3][0], mtx[3][1], mtx[3][2], mtx[3][3] );
	m_pValue->Set( m );

	m_prevValue = *m_pValue;
}

void bkMatrix44::ReadValues( const bkRemotePacket& packet, float mtx[4][4] )
{
	for ( int i = 0; i < 4; ++i )
	{
		for ( int j = 0; j < 4; ++j )
		{
			mtx[i][j] = packet.Read_float();
		}
	}
}

void bkMatrix44::ReadValues( const bkRemotePacket& packet, float vec[4] )
{
	for ( int i = 0; i < 4; ++i )
	{
		vec[i] = packet.Read_float();
	}
}

void bkMatrix44::WriteValues( bkRemotePacket& packet )
{
	for ( int i = 0; i < 4; ++i )
	{
		for ( int j = 0; j < 4; ++j )
		{
			packet.Write_float( m_pValue->GetFloat( (4 * i) + j ) );
		}
	}
}

void bkMatrix44::ExecuteOperation( Operation op )
{
	switch ( op )
	{
	case EIdentity:
		m_pValue->Identity();
		break;
	case EZero:
		m_pValue->Zero();
		break;
	case EInverse:
		m_pValue->Inverse();
		break;
	case EFastInverse:
		m_pValue->FastInverse( *m_pValue );
		break;
	case ETranspose:
		m_pValue->Transpose();
		break;
	default:
	    break;
	}

}

void bkMatrix44::ExecuteOperation( Operation op, float f )
{
	switch ( op )
	{
	case EMakeScale:
		m_pValue->MakeScale( f );
		break;
	case EMakeScaleFull:
		m_pValue->MakeScaleFull( f );
		break;
	case EMakeRotX:
		m_pValue->MakeRotX( f );
		break;
	case EMakeRotY:
		m_pValue->MakeRotY( f );
		break;
	case EMakeRotZ:
		m_pValue->MakeRotZ( f );
		break;
	default:
	    break;
	}
}

void bkMatrix44::ExecuteOperation( Operation op, float f1, float f2, float f3 )
{
	switch ( op )
	{
	case EMakePos:
		m_pValue->MakePos( f1, f2, f3 );
		break;
	default:
	    break;
	}
}

void bkMatrix44::ExecuteOperation( Operation op, float v[] )
{
	switch ( op )
	{
	case EMakeReflect:
		m_pValue->MakeReflect( Vector4( v[0], v[1], v[2], v[3] ) );
		break;
	default:
	    break;
	}
}

void bkMatrix44::ExecuteOperation( Operation op, float v[], float f1, float f2 )
{
	switch ( op )
	{
	case EMakePosRotY:
		m_pValue->MakePosRotY( v[0], v[1], v[2], f1, f2 );
		break;
	default:
	    break;
	}
}

void bkMatrix44::ExecuteOperation( Operation op, float m[4][4]  )
{
	switch ( op )
	{
	case EAdd:
		m_pValue->Add( Matrix44( m[0][0], m[0][1], m[0][2], m[0][3],
			m[1][0], m[1][1], m[1][2], m[1][3],
			m[2][0], m[2][1], m[2][2], m[2][3],
			m[3][0], m[3][1], m[3][2], m[3][3] ) );
		break;
	case ESubtract:
		m_pValue->Subtract( Matrix44( m[0][0], m[0][1], m[0][2], m[0][3],
			m[1][0], m[1][1], m[1][2], m[1][3],
			m[2][0], m[2][1], m[2][2], m[2][3],
			m[3][0], m[3][1], m[3][2], m[3][3] ) );
		break;
	case EDot:
		m_pValue->Dot( Matrix44( m[0][0], m[0][1], m[0][2], m[0][3],
			m[1][0], m[1][1], m[1][2], m[1][3],
			m[2][0], m[2][1], m[2][2], m[2][3],
			m[3][0], m[3][1], m[3][2], m[3][3] ) );
	    break;
	default:
	    break;
	}
}

void bkMatrix44::ClampValuesToAllowedRange()
{
	float m[4][4];

	for ( int i = 0; i < 4; ++i )
	{
		for ( int j = 0; j < 4; ++j )
		{
			float f = m_pValue->GetFloat( (4 * i) + j );
			if ( f < m_minimum )
			{
				m[i][j] = m_minimum;
			}
			else if ( f > m_maximum )
			{
				m[i][j] = m_maximum;
			}
			else
			{
				m[i][j] = f;
			}
		}
	}

	Matrix44 mtx( m[0][0], m[0][1], m[0][2], m[0][3],
		m[1][0], m[1][1], m[1][2], m[1][3],
		m[2][0], m[2][1], m[2][2], m[2][3],
		m[3][0], m[3][1], m[3][2], m[3][3] );
	
	m_pValue->Set( mtx );
}

bool bkMatrix44::SetValue( int row, int col, float f )
{
	if ( (row >= 0) && (row < 4) && (col >= 0) && (col < 4) ) 
	{ 
		Vector4 *pRowVec = NULL;
		switch ( row )
		{
		case 0:
			pRowVec = &m_pValue->a;
			break;
		case 1:
			pRowVec = &m_pValue->b;
			break;
		case 2:
			pRowVec = &m_pValue->c;
			break;
		case 3:
			pRowVec = &m_pValue->d;
			break;
		}

		float value = Clamp( f, m_minimum, m_maximum );
		float oldValue = value;

		switch ( col )
		{
		case 0:
			oldValue = pRowVec->GetX();
			pRowVec->SetX( value );
			break;
		case 1:
			oldValue = pRowVec->GetY();
			pRowVec->SetY( value );
			break;
		case 2:
			oldValue = pRowVec->GetZ();
			pRowVec->SetZ( value );
			break;
		case 3:
			oldValue = pRowVec->GetW();
			pRowVec->SetW( value );
			break;
		}

		return value != oldValue; 
	} 
	else
	{
		return false;
	} 
}

float bkMatrix44::GetValue( int row, int col )
{
	if ( (row >= 0) && (row < 4) && (col >= 0) && (col < 4) ) 
	{ 
		Vector4 *pRowVec = NULL;
		switch ( row )
		{
		case 0:
			pRowVec = &m_pValue->a;
			break;
		case 1:
			pRowVec = &m_pValue->b;
			break;
		case 2:
			pRowVec = &m_pValue->c;
			break;
		case 3:
			pRowVec = &m_pValue->d;
			break;
		}

		switch ( col )
		{
		case 0:
			return pRowVec->GetX();
		case 1:
			return pRowVec->GetY();
		case 2:
			return pRowVec->GetZ();
		case 3:
			return pRowVec->GetW();
		}
	} 

	return 0.0f;
}

bool bkMatrix44::SetString( int row, int col, const char* src )
{
	return SetValue( row, col, (float)atof( src ) );
}

const char* bkMatrix44::GetString( int row, int col, char* dest, int destSize )
{
	float val = GetValue( row, col );
	if ( (val >= m_minimum) && (val <= m_maximum) )
	{
		formatf( dest, destSize, "%1.3f", val );
	}
	else
	{
		safecpy( dest, "**invalid**", destSize );
	}

	return dest;	
}

void bkMatrix44::RemoteHandler( const bkRemotePacket& packet )
{
	bkMatrix::RemoteHandler( packet, EMatrix44 );
}

#endif // __BANK
