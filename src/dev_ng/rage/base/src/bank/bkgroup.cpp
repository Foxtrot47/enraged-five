//
// bank/bkgroup.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "group.h" //RAGE_QA: INCLUDE_OK

#include "bkmgr.h"
#include "packet.h"
#include "pane.h"

#include "bkangle.h"
#include "bkmatrix.h"
#include "bkmgr.h"
#include "bkseparator.h"
#include "bkvector.h"
#include "button.h"
#include "color.h"
#include "combo.h"
#include "data.h"
#include "imageviewer.h"
#include "io.h"
#include "list.h"
#include "packet.h"
#include "pane.h"
#include "slider.h"
#include "text.h"
#include "title.h"
#include "toggle.h"
#include "treelist.h"

#include "string/string.h"
#include "system/param.h"
#include "system/xtl.h"
#include "vector/matrix33.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "diag/channel.h"



using namespace rage;

bkGroup::bkGroup(datCallback &callback,const char *title,const char *memo,bool startOpen,const char* fillColor,bool readOnly) :
	bkWidget(callback,title,memo,fillColor,readOnly), 
	m_Open(startOpen?1:0), 
	m_PrevOpen(startOpen?0:1)
{
}


bkGroup::~bkGroup() {
}

void bkGroup::Remove(bkWidget &widget) {
	widget.Destroy();
}


void bkGroup::Message(Action action,float /*value*/) {
	if (action == DIAL) {
		m_Open = m_Open>0?0:1;
		Changed();
	}
}

int bkGroup::DrawLocal(int x,int y) {
	y = bkWidget::DrawLocal(x,y)-1;

	if (m_Open>0) {
		bkWidget *i = m_Child;
		Drawf(x,y,"[-] %s",m_Title);
		y++;
		while (i) {
			y = i->DrawLocal(x+2,y);
			i = i->GetNext();
		}
		y--;
	}
	else
		Drawf(x,y,"[+] %s",m_Title);

	return y+1;
}


bool bkGroup::IsOpen() const {
	return m_Open>0;
}

bool bkGroup::IsShown() const {
#if __CONSOLE
	bool checkIfOpen = (bkManager::GetActive() || bkRemotePacket::IsConnectedToRag());
#else
	bool checkIfOpen = true;
#endif
	bool isShown = ( checkIfOpen ? IsOpen() : false );
	return isShown;
}

bool bkGroup::AreAnyWidgetsShown() const {
	bkWidget *child = m_Child;
	bool isShown = IsShown();
	while (child && !isShown) {
		isShown=child->AreAnyWidgetsShown();
		child = child->GetNext();
	}

	return isShown;
}

bool bkGroup::IsClosedGroup() const {
	if (m_Open<=0)
		return true;
	else
		return GetParent()->IsClosedGroup();
}

void bkGroup::SetClosed() {
	m_Open = 0;

	bkWidget *i = m_Child;
	while (i) {
		i->SetClosed();
		i = i->GetNext();
	}
}

void bkGroup::RemoteCreate() {
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetGuid(),this);
	p.WriteWidget(m_Parent);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char( GetFillColor() );
	p.Write_bool(IsReadOnly());
	p.Write_s32((s32)m_Open);
	p.Send();
}


void bkGroup::RemoteUpdate() {
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin(bkRemotePacket::CHANGED,GetGuid(),this);
	p.Write_s16(m_Open);
	p.Send();
}


void bkGroup::RemoteHandler(const bkRemotePacket& packet) {
	USE_DEBUG_MEMORY();
	if (packet.GetCommand() == bkRemotePacket::CREATE) {
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;
		const char *title = packet.Read_const_char();
		const char *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();
		int numOpen = packet.Read_s32();
		packet.End();
		bkGroup& widget = *(rage_new bkGroup(NullCB,title,memo,numOpen>0,fillColor,readOnly));
		bkRemotePacket::SetWidgetId(widget, id);
		parent->AddChild(widget);
	}
	else if (packet.GetCommand() == bkRemotePacket::CHANGED) {
		packet.Begin();
		// this is a little different from the normal changed message handler
		// because it's not unheard of for these messages to be referring to a
		// non-existant widget (if the widget list gets created, deleted, and recreated
		// the first create can cause group-changed messages to come in that
		// don't refer to a currently existing group)
		bkGroup* widget = packet.ReadOptionalWidget<bkGroup>(true);
		if (!widget) return;
		s16 openState = packet.Read_s16();
		widget->m_Open = widget->m_PrevOpen = openState;
		packet.End();
		widget->WindowUpdate();
		widget->Changed();
	}
}


int bkGroup::GetGuid() const 
{ 
	return GetStaticGuid(); 
}

void bkGroup::Update() {

	bool update = (m_Open || m_PrevOpen);
	if (m_Open != m_PrevOpen) {
		if(m_PrevOpen) {
			SetClosed();
		}
		m_PrevOpen = m_Open;
		FinishUpdate();
	}

	// always cause and update if rag is connected:
	if (update) {
		bkWidget *i = m_Child;
		while (i) {
			i->Update();
			i = i->GetNext();
		}
	}
}


inline bkWidget* bkGroup::AddWidget(bkWidget& widget) 
{
	return &AddChild(widget).FinishCreate();
}

bkGroup* bkGroup::AddGroup(const char* title,bool startOpen/* =true */,const char *memo/* =NULL */,const char* fillColor/* =NULL */, bool readOnly/*=false */)
{
	USE_DEBUG_MEMORY();
	return (bkGroup*) AddWidget( *(rage_new bkGroup(NullCB, title, memo, startOpen, fillColor, readOnly)));
}

bkSeparator* bkGroup::AddSeparator( const char *title, const char* fillColor )
{
	USE_DEBUG_MEMORY();
	return (bkSeparator*) AddWidget( *(rage_new bkSeparator( title, fillColor )) );
}

bkTitle* bkGroup::AddTitle( bool b, const char* fillColor )
{
	USE_DEBUG_MEMORY();
	return (bkTitle*) AddWidget(*(rage_new bkTitle(b ? "True" : "False",fillColor)));
}

bkTitle* bkGroup::AddTitle( float f, const char* fillColor )
{
	USE_DEBUG_MEMORY();
	char str[64];
	formatf( str, "%f", f );

	return (bkTitle*) AddWidget(*(rage_new bkTitle(str,fillColor)));
}

bkTitle* bkGroup::AddTitle( int i, const char* fillColor )
{
	USE_DEBUG_MEMORY();
	char str[64];
	formatf( str, "%d", i );

	return (bkTitle*) AddWidget(*(rage_new bkTitle(str,fillColor)));
}

bkTitle* bkGroup::AddTitle( const char *fmt, ... )
{ 
	USE_DEBUG_MEMORY();
	char str[512];

	va_list args;
	va_start( args, fmt );

	vformatf( str, sizeof(str), fmt ? fmt : "", args );

	va_end( args );

	return (bkTitle*) AddWidget(*(rage_new bkTitle(str)));
}

bkButton* bkGroup::AddButton(const char *title,datCallback callback,const char *tooltip,const char* fillColor,bool readOnly) { 
	USE_DEBUG_MEMORY();
	return (bkButton*) AddWidget(*(rage_new bkButton(callback,title,tooltip,fillColor,readOnly)));
}

bkButton* bkGroup::AddButton(const char *title, datCallback callback) { 
	return AddButton(title, callback, NULL);
}

bkButton* bkGroup::AddButton(const char* title, Static0 callback) {
	return AddButton(title, datCallback(callback), NULL);
}

bkText* bkGroup::AddText(const char *title,bool *b,bool readOnly,datCallback callback,const char* fillColor) {
	USE_DEBUG_MEMORY();
	return (bkText*) AddWidget(*(rage_new bkText(callback,title,0,b,readOnly,fillColor)));
}

bkText* bkGroup::AddText(const char *title,bool *b,bool readOnly) {
	return AddText(title, b, readOnly, NullCB);
}

bkText* bkGroup::AddText(const char *title,float *f,bool readOnly,datCallback callback,const char* fillColor) {
	USE_DEBUG_MEMORY();
	return (bkText*) AddWidget(*(rage_new bkText(callback,title,0,f,readOnly,fillColor)));
}

bkText* bkGroup::AddText(const char *title,float *f,bool readOnly) {
	return AddText(title, f, readOnly, NullCB);
}

bkText* bkGroup::AddText(const char *title,int *i,bool readOnly,datCallback callback,const char* fillColor) {
	USE_DEBUG_MEMORY();
	return (bkText*) AddWidget(*(rage_new bkText(callback,title,0,i,readOnly,fillColor)));
}

bkText* bkGroup::AddText(const char *title,int *i,bool readOnly) {
	return AddText(title, i, readOnly, NullCB);
}

bkText* bkGroup::AddText(const char *title,long *l,bool readOnly,datCallback callback,const char* fillColor) {
	USE_DEBUG_MEMORY();
	return (bkText*) AddWidget(*(rage_new bkText(callback,title,0,l,readOnly,fillColor)));
}

bkText* bkGroup::AddText(const char *title,long *l,bool readOnly) {
	return AddText(title, l, readOnly, NullCB);
}

bkText* bkGroup::AddText(const char *title,char *str,unsigned int len,bool readOnly,datCallback callback,const char* fillColor) {
	USE_DEBUG_MEMORY();
	return (bkText*) AddWidget(*(rage_new bkText(callback,title,0,str,len,readOnly,fillColor)));
}

bkText* bkGroup::AddText(const char *title,char *str,unsigned int len,bool readOnly) {
	return AddText(title, str, len, readOnly, NullCB);
}

bkText* bkGroup::AddText(const char *title,atNonFinalHashString *h,bool readOnly,datCallback callback,const char* fillColor) {
	USE_DEBUG_MEMORY();
	return (bkText*) AddWidget(*(rage_new bkText(callback,title,0,h,readOnly,fillColor)));
}

bkText* bkGroup::AddText(const char *title,atNonFinalHashString *h,bool readOnly) {
	return AddText(title, h, readOnly, NullCB);
}

bkText* bkGroup::AddText(const char *title,atFinalHashString *h,bool readOnly,datCallback callback,const char* fillColor) {
	USE_DEBUG_MEMORY();
	return (bkText*) AddWidget(*(rage_new bkText(callback,title,0,h,readOnly,fillColor)));
}

bkText* bkGroup::AddText(const char *title,atFinalHashString *h,bool readOnly) {
	return AddText(title, h, readOnly, NullCB);
}

bkToggle* bkGroup::AddToggle(const char *title,bool *value,datCallback callback,const char *memo,const char* fillColor,bool readOnly) { 
	USE_DEBUG_MEMORY();
	return (bkToggle*) AddWidget(*(rage_new bkToggleVal<bool>(callback,title,memo,value,0,fillColor,readOnly)));
}

bkToggle* bkGroup::AddToggle(const char *title,bool *value) {
	return AddToggle(title, value, NullCB);
}

bkToggle* bkGroup::AddToggle(const char *title,int *value,datCallback callback,const char *memo,const char* fillColor,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkToggle*) AddWidget(*(rage_new bkToggleVal<s32>(callback,title,memo,value,0,fillColor,readOnly)));
}

bkToggle* bkGroup::AddToggle(const char *title,int *value) {
	return AddToggle(title, value, NullCB);
}


bkToggle* bkGroup::AddToggle(const char *title,unsigned *value,unsigned mask,datCallback callback,const char *memo,const char* fillColor,bool readOnly) {
	USE_DEBUG_MEMORY();
	bkAssertf(mask, "Mask value for widget %s must be non-zero", title);
	return (bkToggle*) AddWidget(*(rage_new bkToggleVal<u32>(callback,title,memo,value,mask,fillColor,readOnly)));
}

bkToggle* bkGroup::AddToggle(const char *title,unsigned *value,unsigned mask) {
	return AddToggle(title, value, mask, NullCB);
}


bkToggle* bkGroup::AddToggle(const char *title,unsigned short *value,unsigned short mask,datCallback callback,const char *memo,const char* fillColor,bool readOnly) {
	USE_DEBUG_MEMORY();
	bkAssertf(mask, "Mask value for widget %s must be non-zero", title);
	return (bkToggle*) AddWidget(*(rage_new bkToggleVal<u16>(callback,title,memo,value,mask,fillColor,readOnly)));
}

bkToggle* bkGroup::AddToggle(const char *title,unsigned short *value,unsigned short mask) {
	return AddToggle(title, value, mask, NullCB);
}


bkToggle* bkGroup::AddToggle(const char *title,unsigned char *value,unsigned char mask,datCallback callback,const char *memo,const char* fillColor,bool readOnly) {
	USE_DEBUG_MEMORY();
	bkAssertf(mask, "Mask value for widget %s must be non-zero", title);
	return (bkToggle*) AddWidget(*(rage_new bkToggleVal<u8>(callback,title,memo,value,mask,fillColor,readOnly)));
}

bkToggle* bkGroup::AddToggle(const char *title,unsigned char *value,unsigned char mask) {
	return AddToggle(title, value, mask, NullCB);
}


bkToggle* bkGroup::AddToggle(const char *title,atBitSet *value,unsigned char mask,datCallback callback,const char *memo,const char* fillColor,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkToggle*) AddWidget(*(rage_new bkToggleVal<atBitSet>(callback,title,memo,value,mask,fillColor,readOnly)));
}

bkToggle* bkGroup::AddToggle(const char *title,atBitSet *value,unsigned char mask) {
	return AddToggle(title, value, mask, NullCB);
}


bkToggle* bkGroup::AddToggle(const char *title,float *value,datCallback callback,const char *memo,const char* fillColor,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkToggle*) AddWidget(*(rage_new bkToggleVal<float>(callback,title,memo,value,0,fillColor,readOnly)));
}

bkToggle* bkGroup::AddToggle(const char *title,float *value) {
	return AddToggle(title, value, NullCB);
}


#if __PPU
#pragma diag_suppress 174
#endif

bkSlider* bkGroup::AddSlider(const char *title,float *value,			float min,			float max,			float delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkSlider*) AddWidget(*(rage_new bkSliderVal<float>(callback,title,memo,value,min,max,delta,fillColor,exponential,readOnly)));
}

bkSlider* bkGroup::AddSlider(const char *title,float *value,			float min,			float max,			float delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,unsigned char* value,	unsigned char min,	unsigned char max,	unsigned char delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkSlider*) AddWidget(*(rage_new bkSliderVal<u8>(callback,title,memo,value,min,max,delta,fillColor,exponential,readOnly)));
}

bkSlider* bkGroup::AddSlider(const char *title,unsigned char* value,	unsigned char min,	unsigned char max,	unsigned char delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,signed char* value,	signed char min,	signed char max,	signed char delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkSlider*) AddWidget(*(rage_new bkSliderVal<s8>(callback,title,memo,value,min,max,delta,fillColor,exponential,readOnly)));
}

bkSlider* bkGroup::AddSlider(const char *title,signed char* value,	signed char min,	signed char max,	signed char delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,unsigned short* value,unsigned short min,	unsigned short max,	unsigned short delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkSlider*) AddWidget(*(rage_new bkSliderVal<u16>(callback,title,memo,value,min,max,delta,fillColor,exponential,readOnly)));
}

bkSlider* bkGroup::AddSlider(const char *title,unsigned short* value,unsigned short min,	unsigned short max,	unsigned short delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,signed short* value,	signed short min,	signed short max,	signed short delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkSlider*) AddWidget(*(rage_new bkSliderVal<s16>(callback,title,memo,value,min,max,delta,fillColor,exponential,readOnly)));
}

bkSlider* bkGroup::AddSlider(const char *title,signed short* value,	signed short min,	signed short max,	signed short delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,unsigned int* value,	unsigned int min,	unsigned int max,	unsigned int delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkSlider*) AddWidget(*(rage_new bkSliderVal<u32>(callback,title,memo,value,min,max,delta,fillColor,exponential,readOnly)));
}

bkSlider* bkGroup::AddSlider(const char *title,unsigned int* value,	unsigned int min,	unsigned int max,	unsigned int delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,signed int* value,	signed int min,		signed int max,		signed int delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkSlider*) AddWidget(*(rage_new bkSliderVal<s32>(callback,title,memo,value,min,max,delta,fillColor,exponential,readOnly)));
}

bkSlider* bkGroup::AddSlider(const char *title,signed int* value,	signed int min,		signed int max,		signed int delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,Vector2 *value,		float min,			float max,			float delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	char buffer[128];
	formatf(buffer, "%s X", title);
	bkSlider *x = AddSlider(buffer,&value->x,min,max,delta,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s Y", title);
	AddSlider(buffer,&value->y,min,max,delta,callback,memo,fillColor,exponential,readOnly);
	return x;	
}

bkSlider* bkGroup::AddSlider(const char *title,Vector2 *value,		float min,			float max,			float delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,Vector3 *value,		float min,			float max,			float delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	char buffer[128];
	formatf(buffer, "%s X", title);
	bkSlider *x = AddSlider(buffer,&value->x,min,max,delta,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s Y", title);
	AddSlider(buffer,&value->y,min,max,delta,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s Z", title);
	AddSlider(buffer,&value->z,min,max,delta,callback,memo,fillColor,exponential,readOnly);
	return x;	
}

bkSlider* bkGroup::AddSlider(const char *title,Vector3 *value,		float min,			float max,			float delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,Vector4 *value,		float min,			float max,			float delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	char buffer[128];
	formatf(buffer, "%s X", title);
	bkSlider *x = AddSlider(buffer,&value->x,min,max,delta,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s Y", title);
	AddSlider(buffer,&value->y,min,max,delta,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s Z", title);
	AddSlider(buffer,&value->z,min,max,delta,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s W", title);
	AddSlider(buffer,&value->w,min,max,delta,callback,memo,fillColor,exponential,readOnly);
	return x;	
}

bkSlider* bkGroup::AddSlider(const char *title,Vector4 *value,		float min,			float max,			float delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,Vector2 *value,const Vector2 &min,const Vector2 &max,const Vector2 &delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	char buffer[128];
	formatf(buffer, "%s X", title);
	bkSlider *x = AddSlider(buffer,&value->x,min.x,max.x,delta.x,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s Y", title);
	AddSlider(buffer,&value->y,min.y,max.y,delta.y,callback,memo,fillColor,exponential,readOnly);
	return x;	
}

bkSlider* bkGroup::AddSlider(const char *title,Vector2 *value,const Vector2 &min,const Vector2 &max,const Vector2 &delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,Vector3 *value,const Vector3 &min,const Vector3 &max,const Vector3 &delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	char buffer[128];
	formatf(buffer, "%s X", title);
	bkSlider *x = AddSlider(buffer,&value->x,min.x,max.x,delta.x,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s Y", title);
	AddSlider(buffer,&value->y,min.y,max.y,delta.y,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s Z", title);
	AddSlider(buffer,&value->z,min.z,max.z,delta.z,callback,memo,fillColor,exponential,readOnly);
	return x;	
}

bkSlider* bkGroup::AddSlider(const char *title,Vector3 *value,const Vector3 &min,const Vector3 &max,const Vector3 &delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,Vector4 *value,const Vector4 &min,const Vector4 &max,const Vector4 &delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	char buffer[128];
	formatf(buffer, "%s X", title);
	bkSlider *x = AddSlider(buffer,&value->x,min.x,max.x,delta.x,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s Y", title);
	AddSlider(buffer,&value->y,min.y,max.y,delta.y,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s Z", title);
	AddSlider(buffer,&value->z,min.z,max.z,delta.z,callback,memo,fillColor,exponential,readOnly);
	formatf(buffer, "%s W", title);
	AddSlider(buffer,&value->w,min.w,max.w,delta.w,callback,memo,fillColor,exponential,readOnly);
	return x;	
}

bkSlider* bkGroup::AddSlider(const char *title,Vector4 *value,const Vector4 &min,const Vector4 &max,const Vector4 &delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,Float16 *value,			float min,			float max,			float delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkSlider*) AddWidget(*(rage_new bkSliderVal<Float16>(callback,title,memo,value,min,max,delta,fillColor,exponential,readOnly)));
}

bkSlider* bkGroup::AddSlider(const char *title,Float16 *value,			float min,			float max,			float delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

bkSlider* bkGroup::AddSlider(const char *title,ScalarV *value,			float min,			float max,			float delta,datCallback callback,const char *memo,const char* fillColor,bool exponential,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkSlider*) AddWidget(*(rage_new bkSliderVal<ScalarV>(callback,title,memo,value,min,max,delta,fillColor,exponential,readOnly)));
}

bkSlider* bkGroup::AddSlider(const char *title,ScalarV *value,			float min,			float max,			float delta)
{
	return AddSlider(title, value, min, max, delta, NullCB);
}

#if __PPU
// #pragma diag_warning 174
#endif

bkCombo* bkGroup::AddCombo(const char *title,unsigned char *value,int numitems, const char **list,int valueOffset,datCallback callback,const char *memo,const char* fillColor,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkCombo*) AddWidget(*(rage_new bkComboVal<u8>(callback,title,memo,value,list,numitems,valueOffset,fillColor,readOnly)));
}

bkCombo* bkGroup::AddCombo(const char *title,unsigned char *value,int numitems, const char **list,int valueOffset)
{
	return AddCombo(title, value, numitems, list, valueOffset, NullCB);
}

bkCombo* bkGroup::AddCombo(const char *title,signed char *value,int numitems, const char **list,int valueOffset,datCallback callback,const char *memo,const char* fillColor,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkCombo*) AddWidget(*(rage_new bkComboVal<s8>(callback,title,memo,value,list,numitems,valueOffset,fillColor,readOnly)));
}

bkCombo* bkGroup::AddCombo(const char *title,signed char *value,int numitems, const char **list,int valueOffset)
{
	return AddCombo(title, value, numitems, list, valueOffset, NullCB);
}

bkCombo* bkGroup::AddCombo(const char *title,short *value,int numitems, const char **list,int valueOffset,datCallback callback,const char *memo,const char* fillColor,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkCombo*) AddWidget(*(rage_new bkComboVal<s16>(callback,title,memo,value,list,numitems,valueOffset,fillColor,readOnly)));
}

bkCombo* bkGroup::AddCombo(const char *title,short *value,int numitems, const char **list,int valueOffset)
{
	return AddCombo(title, value, numitems, list, valueOffset, NullCB);
}

bkCombo* bkGroup::AddCombo(const char *title,int *value,int numitems, const char **list,int valueOffset,datCallback callback,const char *memo,const char* fillColor,bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkCombo*) AddWidget(*(rage_new bkComboVal<s32>(callback,title,memo,value,list,numitems,valueOffset,fillColor,readOnly)));
}

bkCombo* bkGroup::AddCombo(const char *title,int *value,int numitems, const char **list,int valueOffset)
{
	return AddCombo(title, value, numitems, list, valueOffset, NullCB);
}


bkCombo* bkGroup::AddCombo(const char *title,unsigned char *value,int numitems, const char **list)	
{
	return AddCombo(title,value,numitems,list,0,NullCB);
}

bkCombo* bkGroup::AddCombo(const char *title,signed char *value,int numitems, const char **list)		
{
	return AddCombo(title,value,numitems,list,0,NullCB);
}

bkCombo* bkGroup::AddCombo(const char *title,short *value,int numitems, const char **list)			
{
	return AddCombo(title,value,numitems,list,0,NullCB);
}

bkCombo* bkGroup::AddCombo(const char *title,int *value,int numitems, const char **list)				
{
	return AddCombo(title,value,numitems,list,0,NullCB);
}



bkList* bkGroup::AddList(const char* title,bool readOnly,const char* memo,const char* fillColor)
{
	USE_DEBUG_MEMORY();
	return (bkList*)AddWidget(*(rage_new bkList(title,readOnly,memo,fillColor)));
}


bkTreeList* bkGroup::AddTreeList( const char* title, const char* memo, bool allowDrag, bool allowDrop, bool allowDelete, bool showRootLines, const char* fillColor, bool readOnly)
{
	USE_DEBUG_MEMORY();
	return (bkTreeList*)AddWidget(*(rage_new bkTreeList(title,memo,allowDrag,allowDrop,allowDelete,showRootLines,fillColor,readOnly)));
}


bkData* bkGroup::AddDataWidget( const char *title, u8 *data, u16 length, datCallback callback, const char *memo, bool showDataView, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	return (bkData*)AddWidget(*(rage_new bkData(callback,title,memo,data,length,showDataView,fillColor,readOnly)));
}


bkVector2* bkGroup::AddVector(const char *title, Vector2 *value,	float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	return (bkVector2*)AddWidget( *(rage_new bkVector2(callback, title, memo, value, min, max, delta, fillColor, readOnly )) );
}

bkVector2* bkGroup::AddVector(const char *title, Vector2 *value,	float min, float max, float delta)
{
	return AddVector(title, value, min, max, delta, NullCB);
}

bkVector3* bkGroup::AddVector(const char *title, Vector3 *value,	float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	return (bkVector3*)AddWidget( *(rage_new bkVector3(callback, title, memo, value, min, max, delta, fillColor, readOnly )) );
}

bkVector3* bkGroup::AddVector(const char *title, Vector3 *value,	float min, float max, float delta)
{
	return AddVector(title, value, min, max, delta, NullCB);
}

bkVector4* bkGroup::AddVector(const char *title, Vector4 *value,	float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	return (bkVector4*)AddWidget( *(rage_new bkVector4(callback, title, memo, value, min, max, delta, fillColor, readOnly )) );
}

bkVector4* bkGroup::AddVector(const char *title, Vector4 *value,	float min, float max, float delta)
{
	return AddVector(title, value, min, max, delta, NullCB);
}

bkVector2* bkGroup::AddVector(const char *title, Vec2V *value,	float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	return (bkVector2*)AddWidget( *(rage_new bkVector2(callback, title, memo, (Vector2*) value, min, max, delta, fillColor, readOnly )) );
}

bkVector2* bkGroup::AddVector(const char *title, Vec2V *value,	float min, float max, float delta)
{
	return AddVector(title, value, min, max, delta, NullCB);
}

bkVector3* bkGroup::AddVector(const char *title, Vec3V *value,	float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	return (bkVector3*)AddWidget( *(rage_new bkVector3(callback, title, memo, (Vector3*)value, min, max, delta, fillColor, readOnly )) );
}

bkVector3* bkGroup::AddVector(const char *title, Vec3V *value,	float min, float max, float delta)
{
	return AddVector(title, value, min, max, delta, NullCB);
}

bkVector4* bkGroup::AddVector(const char *title, Vec4V *value,	float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	return (bkVector4*)AddWidget( *(rage_new bkVector4(callback, title, memo, (Vector4*) value, min, max, delta, fillColor, readOnly )) );
}

bkVector4* bkGroup::AddVector(const char *title, Vec4V *value,	float min, float max, float delta)
{
	return AddVector(title, value, min, max, delta, NullCB);
}

bkVector3* bkGroup::AddVector(const char *title, Matrix33 *value, float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	char buffer[128];
	formatf(buffer, "%s Row 1", title);
	bkVector3 *row1 = AddVector( buffer, &value->a, min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 2", title);
	AddVector( buffer, &value->b, min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 3", title);
	AddVector( buffer, &value->c, min, max, delta, callback, memo, fillColor, readOnly );
	return row1;	
}

bkVector3* bkGroup::AddVector(const char *title, Matrix33 *value, float min, float max, float delta)
{
	return AddVector(title, value, min, max, delta, NullCB);
}

bkVector3* bkGroup::AddVector(const char *title, Matrix34 *value, float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	char buffer[128];
	formatf(buffer, "%s Row 1", title);
	bkVector3 *row1 = AddVector( buffer, &value->a, min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 2", title);
	AddVector( buffer, &value->b, min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 3", title);
	AddVector( buffer, &value->c, min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 4", title);
	AddVector( buffer, &value->d, min, max, delta, callback, memo, fillColor, readOnly );
	return row1;	
}

bkVector3* bkGroup::AddVector(const char *title, Matrix34 *value, float min, float max, float delta)
{
	return AddVector(title, value, min, max, delta, NullCB);
}

bkVector3* bkGroup::AddVector(const char *title, Mat34V *value, float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	char buffer[128];
	formatf(buffer, "%s Row 1", title);
	bkVector3 *row1 = AddVector( buffer, &value->GetCol0Ref(), min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 2", title);
	AddVector( buffer, &value->GetCol1Ref(), min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 3", title);
	AddVector( buffer, &value->GetCol2Ref(), min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 4", title);
	AddVector( buffer, &value->GetCol3Ref(), min, max, delta, callback, memo, fillColor, readOnly );
	return row1;	
}


bkVector3* bkGroup::AddVector(const char *title, Mat34V *value, float min, float max, float delta)
{
	return AddVector(title, value, min, max, delta, NullCB);
}


bkVector4* bkGroup::AddVector(const char *title, Matrix44 *value, float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	char buffer[128];
	formatf(buffer, "%s Row 1", title);
	bkVector4 *row1 = AddVector( buffer, &value->a, min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 2", title);
	AddVector( buffer, &value->b, min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 3", title);
	AddVector( buffer, &value->c, min, max, delta, callback, memo, fillColor, readOnly );
	formatf(buffer, "%s Row 4", title);
	AddVector( buffer, &value->d, min, max, delta, callback, memo, fillColor, readOnly );
	return row1;	
}

bkVector4* bkGroup::AddVector(const char *title, Matrix44 *value, float min, float max, float delta)
{
	return AddVector(title, value, min, max, delta, NullCB);
}


bkMatrix33* bkGroup::AddMatrix(const char *title, Matrix33 *value, float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	return (bkMatrix33*)AddWidget( *(rage_new bkMatrix33(callback, title, memo, value, min, max, delta, fillColor, readOnly )) );
}

bkMatrix33* bkGroup::AddMatrix(const char *title, Matrix33 *value, float min, float max, float delta)
{
	return AddMatrix(title, value, min, max, delta, NullCB);
}

bkMatrix34* bkGroup::AddMatrix(const char *title, Matrix34 *value, float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	return (bkMatrix34*)AddWidget( *(rage_new bkMatrix34(callback, title, memo, value, min, max, delta, fillColor, readOnly )) );
}

bkMatrix34* bkGroup::AddMatrix(const char *title, Matrix34 *value, float min, float max, float delta)
{
	return AddMatrix(title, value, min, max, delta, NullCB);
}


bkMatrix44* bkGroup::AddMatrix(const char *title, Matrix44 *value, float min, float max, float delta, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	return (bkMatrix44*)AddWidget( *(rage_new bkMatrix44(callback, title, memo, value, min, max, delta, fillColor, readOnly )) );
}

bkMatrix44* bkGroup::AddMatrix(const char *title, Matrix44 *value, float min, float max, float delta)
{
	return AddMatrix(title, value, min, max, delta, NullCB);
}



bkImageViewer* bkGroup::AddImageViewer( const char *title, const char *memo, const char *fillColor )
{
	USE_DEBUG_MEMORY();
	return (bkImageViewer*)AddWidget( *(rage_new bkImageViewer( title, memo, fillColor )) );
}

bkAngle* bkGroup::AddAngle( const char *title, float *data, bkAngle::EAngleType angleType, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	if ( bkRemotePacket::IsConnected() )
	{
		return (bkAngle*)AddWidget(*(rage_new bkAngle( callback, title, memo, data, angleType, -FLT_MAX / 1000.0f, FLT_MAX / 1000.0f, fillColor, readOnly )));
	}
	else
	{
		switch ( angleType )
		{
		case bkAngleType::DEGREES:
			AddSlider( title, data, -FLT_MAX / 1000.0f, FLT_MAX / 1000.0f, 1.0f, callback, memo, fillColor, readOnly );
			break;
		case bkAngleType::FRACTION:
			AddSlider( title, data, -FLT_MAX / 1000.0f, FLT_MAX / 1000.0f, 0.01f, callback, memo, fillColor, readOnly );
			break;
		case bkAngleType::RADIANS:
			AddSlider( title, data, -FLT_MAX / 1000.0f, FLT_MAX / 1000.0f, 0.01f, callback, memo, fillColor, readOnly );
			break;
		default:
			bkAssertf( angleType != bkAngleType::VECTOR2, "Invalid angle type %d", (int)angleType );
			break;
		}

		return NULL;
	}
}

bkAngle* bkGroup::AddAngle( const char *title, float *data, bkAngle::EAngleType angleType)
{
	return AddAngle(title, data, angleType, NullCB);
}

bkAngle* bkGroup::AddAngle( const char *title, Vector2 *data, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	if ( bkRemotePacket::IsConnected() )
	{
		return (bkAngle*)AddWidget(*(rage_new bkAngle( callback, title, memo, data, -FLT_MAX / 1000.0f, FLT_MAX / 1000.0f, fillColor, readOnly )));
	}
	else
	{
		AddVector( title, data, -FLT_MAX / 1000.0f, FLT_MAX / 1000.0f, 0.1f, callback, memo, fillColor, readOnly );
		return NULL;
	}
}

bkAngle* bkGroup::AddAngle( const char *title, Vector2 *data)
{
	return AddAngle(title, data, NullCB);
}

bkAngle* bkGroup::AddAngle( const char *title, float *data, bkAngle::EAngleType angleType, float min, float max, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	if ( bkRemotePacket::IsConnected() )
	{
		return (bkAngle*)AddWidget(*(rage_new bkAngle( callback, title, memo, data, angleType, min, max, fillColor, readOnly )));
	}
	else
	{
		switch ( angleType )
		{
		case bkAngleType::DEGREES:
			AddSlider( title, data, min, max, 1.0f, callback, memo, fillColor, readOnly );
			break;
		case bkAngleType::FRACTION:
			AddSlider( title, data, min, max, 0.01f, callback, memo, fillColor, readOnly );
			break;
		case bkAngleType::RADIANS:
			AddSlider( title, data, min, max, 0.01f, callback, memo, fillColor, readOnly );
			break;
		default:
			bkAssertf( angleType != bkAngleType::VECTOR2 , "Invalid angle type %d", (int)angleType);
			break;
		}

		return NULL;
	}
}

bkAngle* bkGroup::AddAngle( const char *title, float *data, bkAngle::EAngleType angleType, float min, float max)
{
	return AddAngle(title, data, angleType, min, max, NullCB);
}

bkAngle* bkGroup::AddAngle( const char *title, Vector2 *data, float min, float max, datCallback callback, const char *memo, const char* fillColor, bool readOnly )
{
	USE_DEBUG_MEMORY();
	if ( bkRemotePacket::IsConnected() )
	{
		return (bkAngle*)AddWidget(*(rage_new bkAngle( callback, title, memo, data, min, max, fillColor, readOnly )));
	}
	else
	{
		AddVector( title, data, min, max, 0.1f, callback, memo, fillColor, readOnly );
		return NULL;
	}
}

bkAngle* bkGroup::AddAngle( const char *title, Vector2 *data, float min, float max)
{
	return AddAngle(title, data, min, max, NullCB);
}



// Specifies on what platforms the color picker dialog box is supported
#define __CLR_DIALOG (__WIN32PC)

#if __CLR_DIALOG

typedef void (*TypeEditVecFunc)(float*);
static enum EnumDLLLoadState {NOT_LOADED,LOAD_FAILED,LOAD_SUCCESS};

static TypeEditVecFunc	sVecRGBFuncPtr; 
static TypeEditVecFunc	sVecRGBAFuncPtr;

static EnumDLLLoadState LoadColorDlgDLL()
{
	static EnumDLLLoadState	sDLLLoadStatus=NOT_LOADED;

	// already loaded, or at least we tried to load:
	if (sDLLLoadStatus!=NOT_LOADED)
		return sDLLLoadStatus;

	HINSTANCE hDLL;						// Handle to DLL

	hDLL = LoadLibrary("GTColorDialog");
	if (hDLL != NULL)
	{
		sVecRGBFuncPtr	= (TypeEditVecFunc)GetProcAddress(hDLL,"EditColorDialogVec3");
		sVecRGBAFuncPtr	= (TypeEditVecFunc)GetProcAddress(hDLL,"EditColorDialogVec4");
		if (!sVecRGBFuncPtr || !sVecRGBAFuncPtr)
		{
			sVecRGBFuncPtr	= NULL;
			sVecRGBAFuncPtr	= NULL;

			// handle the error
			FreeLibrary(hDLL);       
			sDLLLoadStatus=LOAD_FAILED;
		}
		else
			sDLLLoadStatus=LOAD_SUCCESS;
	}
	else 
	{
		bkWarningf("failed to load \'GTColorDialog.DLL\'");
	}

	return sDLLLoadStatus;
}

#endif


bkColor* bkGroup::AddColor(const char * title,Color32 * color,datCallback callback,const char *memo,const char* fillColor,bool inOutLinear, bool readOnly) {
	USE_DEBUG_MEMORY();
	return (bkColor*) AddWidget(*(rage_new bkColor(*color,callback,title,memo,fillColor,readOnly,inOutLinear)));
}

bkColor* bkGroup::AddColor(const char * title,Color32 * color) 
{
	return AddColor(title, color, NullCB);
}


bkColor* bkGroup::AddColor(const char * title,float * color,int floatArraySize, datCallback callback,const char *memo,const char* fillColor,bool inOutLinear, bool readOnly)
{
	USE_DEBUG_MEMORY();
	if (floatArraySize == 3 || floatArraySize ==4)
	{
		return (bkColor*) AddWidget(*(rage_new bkColor(color,floatArraySize,callback,title,memo,fillColor,readOnly,inOutLinear)));
	}
	else
	{
		bkErrorf("Can only call AddColor(float) with an array size of 3 or 4");
		return NULL;
	}
}

bkColor* bkGroup::AddColor(const char * title,float * color,int floatArraySize) 
{
	return AddColor(title, color, floatArraySize, NullCB);
}

bkColor* bkGroup::AddColor(const char * title,Float16 * color,int floatArraySize, datCallback callback,const char *memo,const char* fillColor,bool inOutLinear,bool readOnly)
{
	USE_DEBUG_MEMORY();
	if (floatArraySize == 3 || floatArraySize ==4)
	{
		return (bkColor*) AddWidget(*(rage_new bkColor(color,floatArraySize,callback,title,memo,fillColor,readOnly,inOutLinear)));
	}
	else
	{
		bkErrorf("Can only call AddColor(float) with an array size of 3 or 4");
		return NULL;
	}
}

bkColor* bkGroup::AddColor(const char * title,Float16 * color,int floatArraySize) 
{
	return AddColor(title, color, floatArraySize, NullCB);
}

bkColor* bkGroup::AddColor(const char * title,Vec4V * color, float delta, datCallback callback,const char *memo,const char* fillColor,bool inOutLinear,bool readOnly) {
	return AddColor(title,(Vector4*)color,delta,callback,memo,fillColor,inOutLinear,readOnly);
}

bkColor* bkGroup::AddColor(const char * title,Vec4V * color, float delta) 
{
	return AddColor(title, color, delta, NullCB);
}

bkColor* bkGroup::AddColor(const char * title,Vector4 * color, float /*delta*/, datCallback callback,const char *memo,const char* fillColor,bool inOutLinear,bool readOnly) {

	USE_DEBUG_MEMORY();
	return (bkColor*) AddWidget(*(rage_new bkColor(*color,callback,title,memo,fillColor,readOnly,inOutLinear)));
}

bkColor* bkGroup::AddColor(const char * title,Vector4 * color, float delta) 
{
	return AddColor(title, color, delta, NullCB);
}



bkColor* bkGroup::AddColor(const char * title,Vec3V * color, float delta, datCallback callback,const char *memo,const char* fillColor, bool inOutLinear, bool readOnly)
{
	return AddColor(title,(Vector3*)color,delta,callback,memo,fillColor, inOutLinear, readOnly);
}

bkColor* bkGroup::AddColor(const char * title,Vec3V * color, float delta) 
{
	return AddColor(title, color, delta, NullCB);
}

bkColor* bkGroup::AddColor(const char * title,Vector3 * color, float /*delta*/, datCallback callback,const char *memo,const char* fillColor, bool inOutLinear, bool readOnly) {

	USE_DEBUG_MEMORY();
	return (bkColor*) AddWidget(*(rage_new bkColor(*color,callback,title,memo,fillColor,readOnly,inOutLinear)));
}

bkColor* bkGroup::AddColor(const char * title,Vector3 * color, float delta) 
{
	return AddColor(title, color, delta, NullCB);
}


bkColor* bkGroup::AddColor(const char * title,Vector4 * color, datCallback callback,const char *memo,const char* fillColor,bool inOutLinear,bool readOnly) {
	return AddColor(title,color,1.0f / 256,callback,memo,fillColor,inOutLinear,readOnly);
}

bkColor* bkGroup::AddColor(const char * title,Vector4 * color) 
{
	return AddColor(title,color,1.0f / 256, NullCB);
}

bkColor* bkGroup::AddColor(const char * title,Vector3 * color, datCallback callback,const char *memo,const char* fillColor,bool inOutLinear,bool readOnly) {
	return AddColor(title,color,1.0f / 256,callback,memo,fillColor,inOutLinear,readOnly);
}

bkColor* bkGroup::AddColor(const char * title,Vector3 * color) 
{
	return AddColor(title,color,1.0f / 256, NullCB);
}

bkColor* bkGroup::AddColor(const char * title,Vec4V * color, datCallback callback,const char *memo,const char* fillColor,bool inOutLinear,bool readOnly) {
	return AddColor(title,color,1.0f / 256,callback,memo,fillColor,inOutLinear,readOnly);
}

bkColor* bkGroup::AddColor(const char * title,Vec4V * color) 
{
	return AddColor(title,color,1.0f / 256, NullCB);
}

bkColor* bkGroup::AddColor(const char * title,Vec3V * color, datCallback callback,const char *memo,const char* fillColor,bool inOutLinear,bool readOnly) {
	return AddColor(title,color,1.0f / 256,callback,memo,fillColor,inOutLinear,readOnly);
}

bkColor* bkGroup::AddColor(const char * title,Vec3V * color) 
{
	return AddColor(title,color,1.0f / 256, NullCB);
}

#if __WIN32PC
#include "system/xtl.h"

void bkGroup::WindowCreate() {
	if (!bkRemotePacket::IsConnectedToRag())
	{
		GetPane()->AddLastWindow(this, "BUTTON", 0, BS_AUTOCHECKBOX | BS_PUSHLIKE);
		SendMessage(m_Hwnd,WM_SETFONT,(WPARAM) GetPane()->GetBoldFont(),MAKELPARAM(TRUE,0));
	}
}


rageLRESULT bkGroup::WindowMessage(rageUINT msg,rageWPARAM wParam,rageLPARAM /*lParam*/) {
	if (msg == WM_COMMAND && HIWORD(wParam) == BN_CLICKED) {
		SetFocus();
		m_Open = SendMessage(m_Hwnd,BM_GETCHECK,0,0) != 0?1:0;
		if (GetGuid() == GetStaticGuid()) {		// LAME!  Detect bkGroup, not bkManager or bkBank.
			if (m_Open) {
				WindowCreateAllKids();
				GetPane()->Resize();
			}
			else
				WindowDestroyAllKids();
		}
		GetPane()->Resize();
	}
	return 0;
}


int bkGroup::WindowResize(int x,int y,int width,int height) {
	y = bkWidget::WindowResize(x,y,width,height);
	bkWidget *i = m_Child;
	while (i) {
		y = i->WindowResize(x,y,width,m_Open?height:0);
		i = i->GetNext();
	}
	return y;
}


void bkGroup::WindowUpdate() {
	if (m_Hwnd)
		SendMessage(m_Hwnd,BM_SETCHECK,(s32)m_Open,0);
}

#endif


#endif
