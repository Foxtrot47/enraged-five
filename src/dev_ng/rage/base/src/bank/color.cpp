//
// bank/color.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "color.h"

#include "packet.h"
#include "pane.h"
#include "atl/string.h"

using namespace rage;

bkColor::bkColor(Vector3& vector,datCallback cb,const char *title,const char *memo, const char *fillColor, bool readOnly, bool inOutLinear) 
: bkWidget(cb,title,memo,fillColor,readOnly)
, m_drawLocalExpanded(0)
{
	m_colorType=COLOR_TYPE_VECTOR3;
	m_Vector3 = &vector;
	m_inOutLinear = inOutLinear;
}

bkColor::bkColor(Vector4& vector,datCallback cb,const char *title,const char *memo, const char *fillColor, bool readOnly, bool inOutLinear) 
: bkWidget(cb,title,memo,fillColor,readOnly)
, m_drawLocalExpanded(0)
{
	m_colorType=COLOR_TYPE_VECTOR4;
	m_Vector4 = &vector;
	m_inOutLinear = inOutLinear;

}

bkColor::bkColor(Color32& color,datCallback cb,const char *title,const char *memo, const char *fillColor, bool readOnly, bool inOutLinear) 
: bkWidget(cb,title,memo,fillColor,readOnly)
, m_drawLocalExpanded(0)
{
	m_colorType=COLOR_TYPE_COLOR32;
	m_Color32 = &color;
	m_inOutLinear = inOutLinear;
}

bkColor::bkColor(float* pFloat,int arraySize,datCallback cb,const char *title,const char *memo, const char *fillColor, bool readOnly, bool inOutLinear) 
: bkWidget(cb,title,memo,fillColor,readOnly)
, m_drawLocalExpanded(0)
{
	if (arraySize==3)
	{
		m_colorType=COLOR_TYPE_FLOAT3;
	}
	else if (arraySize==4)
	{
		m_colorType=COLOR_TYPE_FLOAT4;
	}
	else
	{
		bkErrorf("Trying to construct a bkColor widget with invalid array size");
	}

	m_floats = pFloat;
	m_inOutLinear = inOutLinear;
}

bkColor::bkColor(Float16* pFloat,int arraySize,datCallback cb,const char *title,const char *memo, const char *fillColor, bool readOnly, bool inOutLinear) 
: bkWidget(cb,title,memo,fillColor,readOnly)
, m_drawLocalExpanded(0)
{
	if (arraySize==3)
	{
		m_colorType=COLOR_TYPE_FLOAT163;
	}
	else if (arraySize==4)
	{
		m_colorType=COLOR_TYPE_FLOAT164;
	}
	else
	{
		bkErrorf("Trying to construct a bkColor widget with invalid array size");
	}

	m_float16s = pFloat;
	m_inOutLinear = inOutLinear;
}

bkColor::~bkColor() {
}

int bkColor::GetAlpha() const
{
	switch ( m_colorType )
	{
	case COLOR_TYPE_VECTOR3:
	case COLOR_TYPE_FLOAT3:
	case COLOR_TYPE_FLOAT163:
		return (int)(1.0f * 255.0f);

	case COLOR_TYPE_VECTOR4:
		return (int)(m_Vector4->w * 255.0f);

	case COLOR_TYPE_COLOR32:
		return m_Color32->GetAlpha();

	case COLOR_TYPE_FLOAT4:
		return (int)(m_floats[3] * 255.0f);

	case COLOR_TYPE_FLOAT164:
		return (int)(m_float16s[3].GetFloat32_FromFloat16() * 255.0f);
	}

	return 0;
}

void bkColor::SetAlpha( int alpha )
{
    if ( SetComponent( ALPHA_COMPONENT, alpha ) )
    {
        Changed();
    }
}

int bkColor::GetRed() const
{
	switch ( m_colorType )
	{
	case COLOR_TYPE_VECTOR3:
		return (int)(m_Vector3->x * 255.0f);

	case COLOR_TYPE_VECTOR4:
		return (int)(m_Vector4->x * 255.0f);

	case COLOR_TYPE_COLOR32:
		return m_Color32->GetRed();

	case COLOR_TYPE_FLOAT3:
	case COLOR_TYPE_FLOAT4:
		return (int)(m_floats[0] * 255.0f);

	case COLOR_TYPE_FLOAT163:
	case COLOR_TYPE_FLOAT164:
		return (int)(m_float16s[0].GetFloat32_FromFloat16() * 255.0f);
	}

	return 0;
}

void bkColor::SetRed( int red )
{
    if ( SetComponent( RED_COMPONENT, red ) )
    {
        Changed();
    }
}

int bkColor::GetGreen() const
{
	switch ( m_colorType )
	{
	case COLOR_TYPE_VECTOR3:
		return (int)(m_Vector3->y * 255.0f);

	case COLOR_TYPE_VECTOR4:
		return (int)(m_Vector4->y * 255.0f);

	case COLOR_TYPE_COLOR32:
		return m_Color32->GetGreen();

	case COLOR_TYPE_FLOAT3:
	case COLOR_TYPE_FLOAT4:
		return (int)(m_floats[1] * 255.0f);

	case COLOR_TYPE_FLOAT163:
	case COLOR_TYPE_FLOAT164:
		return (int)(m_float16s[1].GetFloat32_FromFloat16() * 255.0f);
	}

	return 0;
}

void bkColor::SetGreen( int green )
{
    if ( SetComponent( GREEN_COMPONENT, green ) )
    {
        Changed();
    }
}

int bkColor::GetBlue() const
{
	switch ( m_colorType )
	{
	case COLOR_TYPE_VECTOR3:
		return (int)(m_Vector3->z * 255.0f);

	case COLOR_TYPE_VECTOR4:
		return (int)(m_Vector4->z * 255.0f);

	case COLOR_TYPE_COLOR32:
		return m_Color32->GetBlue();

	case COLOR_TYPE_FLOAT3:
	case COLOR_TYPE_FLOAT4:
		return (int)(m_floats[2] * 255.0f);

	case COLOR_TYPE_FLOAT163:
	case COLOR_TYPE_FLOAT164:
		return (int)(m_float16s[2].GetFloat32_FromFloat16() * 255.0f);
	}

	return 0;
}

void bkColor::SetBlue( int blue )
{
    if ( SetComponent( BLUE_COMPONENT, blue ) )
    {
        Changed();
    }
}

float bkColor::GetAlphaf() const
{
	switch ( m_colorType )
	{
	case COLOR_TYPE_VECTOR3:
	case COLOR_TYPE_FLOAT3:
	case COLOR_TYPE_FLOAT163:
		return 1.0f;

	case COLOR_TYPE_VECTOR4:
		return m_Vector4->w;

	case COLOR_TYPE_COLOR32:
		return m_Color32->GetAlphaf();

	case COLOR_TYPE_FLOAT4:
		return m_floats[3];

	case COLOR_TYPE_FLOAT164:
		return m_float16s[3].GetFloat32_FromFloat16();
	}

	return 0.0f;
}

void bkColor::SetAlphaf( float alpha )
{
    if ( SetComponentf( ALPHA_COMPONENT, alpha ) )
    {
        Changed();
    }
}

float bkColor::GetRedf() const
{
	switch ( m_colorType )
	{
	case COLOR_TYPE_VECTOR3:
		return m_Vector3->x;

	case COLOR_TYPE_VECTOR4:
		return m_Vector4->x;

	case COLOR_TYPE_COLOR32:
		return m_Color32->GetRedf();

	case COLOR_TYPE_FLOAT3:
	case COLOR_TYPE_FLOAT4:
		return m_floats[0];

	case COLOR_TYPE_FLOAT163:
	case COLOR_TYPE_FLOAT164:
		return m_float16s[0].GetFloat32_FromFloat16();
	}

	return 0.0f;
}

void bkColor::SetRedf( float red )
{
    if ( SetComponentf( RED_COMPONENT, red ) )
    {
        Changed();
    }
}

float bkColor::GetGreenf() const
{
	switch ( m_colorType )
	{
	case COLOR_TYPE_VECTOR3:
		return m_Vector3->y;

	case COLOR_TYPE_VECTOR4:
		return m_Vector4->y;

	case COLOR_TYPE_COLOR32:
		return m_Color32->GetGreenf();

	case COLOR_TYPE_FLOAT3:
	case COLOR_TYPE_FLOAT4:
		return m_floats[1];

	case COLOR_TYPE_FLOAT163:
	case COLOR_TYPE_FLOAT164:
		return m_float16s[1].GetFloat32_FromFloat16();
	}

	return 0.0f;
}

void bkColor::SetGreenf( float green )
{
    if ( SetComponentf( GREEN_COMPONENT, green ) )
    {
        Changed();
    }
}

float bkColor::GetBluef() const
{
	switch ( m_colorType )
	{
	case COLOR_TYPE_VECTOR3:
		return m_Vector3->z;

	case COLOR_TYPE_VECTOR4:
		return m_Vector4->z;

	case COLOR_TYPE_COLOR32:
		return m_Color32->GetBluef();

	case COLOR_TYPE_FLOAT3:
	case COLOR_TYPE_FLOAT4:
		return m_floats[2];

	case COLOR_TYPE_FLOAT163:
	case COLOR_TYPE_FLOAT164:
		return m_float16s[2].GetFloat32_FromFloat16();
	}

	return 0.0f;
}

void bkColor::SetBluef( float blue )
{
    if ( SetComponentf( BLUE_COMPONENT, blue ) )
    {
        Changed();
    }
}

void bkColor::Get( int &red, int &green, int &blue, int &alpha ) const
{
    red = GetRed();
    green = GetGreen();
    blue = GetBlue();
    alpha = GetAlpha();
}

void bkColor::Set( int red, int green, int blue, int alpha )
{
    bool changed = false;
    
    changed |= SetComponent( ALPHA_COMPONENT, alpha );
    changed |= SetComponent( RED_COMPONENT, red );
    changed |= SetComponent( GREEN_COMPONENT, green );
    changed |= SetComponent( BLUE_COMPONENT, blue );

    if ( changed )
    {
        Changed();
    }
}

void bkColor::Getf( float &red, float &green, float &blue, float &alpha ) const
{
    red = GetRedf();
    green = GetGreenf();
    blue = GetBluef();
    alpha = GetAlphaf();
}

void bkColor::Setf( float red, float green, float blue, float alpha )
{
    bool changed = false;

    changed |= SetComponentf( ALPHA_COMPONENT, alpha );
    changed |= SetComponentf( RED_COMPONENT, red );
    changed |= SetComponentf( GREEN_COMPONENT, green );
    changed |= SetComponentf( BLUE_COMPONENT, blue );    

    if ( changed )
    {
        Changed();
    }
}

void bkColor::GetStringRepr( char *buf, int bufLen )
{
    char cBuf[128];
    formatf( cBuf, "%f", GetComponentf( ALPHA_COMPONENT ) );
    for ( int i = 1; i < 4; ++i )
    {
        safecatf( cBuf, ",%f", GetComponentf( (EColorComponent)i ) );
    }

    safecpy( buf, cBuf, bufLen );
}

void bkColor::SetStringRepr( const char *buf )
{
    atString strBuf( buf );
    atArray<atString> split;
    strBuf.Split( split, ',' );

    int start = 0;
    int end = 3;
    if ( split.GetCount() == 3 )
    {
        start = 1;
    }

    for ( int i = start; (i <= end) && (i < split.GetCount()); ++i )
    {
        split[i - start].Trim();
        SetComponentf( (EColorComponent)i, (float)atof( split[i - start].c_str() ) );
    }

    Changed();
}

void bkColor::Message(Action action,float value) 
{
	if ( action == DIAL )
	{
		if ( m_drawLocalExpanded )
		{
			if ( m_drawLocalExpanded > 0 )
			{
				int component = sm_Cursor - sm_TopLine - m_drawLocalExpanded;
				if ( component == 0 )
				{
					if ( value < 0.0f )
					{
						m_drawLocalExpanded = 0;
					}
				}
				else
				{
					if ( m_colorType == COLOR_TYPE_VECTOR3 )
					{
						// remap.  Vector3 has no alpha (component 0)
						++component;
					}
					
					if ( SetComponent( (EColorComponent)(component - 1), GetComponent( (EColorComponent)(component - 1) ) + (int)value ) )
					{
						WindowUpdate();
						Changed();
					}
				}
			}
		}
		else
		{
			if ( value > 0.0f )
			{
				m_drawLocalExpanded = -1;
			}
		}
	}
}

int bkColor::DrawLocal(int x,int y) 
{
	if ( m_drawLocalExpanded )
	{
		m_drawLocalExpanded = y;

		Drawf( x, y, "(-)%s", m_Title );
		y = bkWidget::DrawLocal( x, y );

		int start = 0;
		if ( m_colorType == COLOR_TYPE_VECTOR3 )
		{
			start = 1;
		}

		static const char componentNames[4][2] = { "A", "R", "G", "B" };

		for ( int i = start; i < 4; ++i )
		{
			Drawf( x, y, "%s %d", componentNames[i], GetComponent( (EColorComponent)i ) );
			y = bkWidget::DrawLocal( x, y );
		}
	}
	else
	{
		if ( m_colorType == COLOR_TYPE_VECTOR3 )
		{
			Drawf( x, y, "(+)%s R=%d, G=%d, B=%d", m_Title, 
                GetComponent( RED_COMPONENT ), GetComponent( GREEN_COMPONENT ), GetComponent( BLUE_COMPONENT ) );
		}
		else
		{
			Drawf( x, y, "(+)%s A=%d, R=%d, G=%d, B=%d", m_Title, 
                GetComponent( ALPHA_COMPONENT ), GetComponent( RED_COMPONENT ), GetComponent( GREEN_COMPONENT ), GetComponent( BLUE_COMPONENT ) );
		}

		y = bkWidget::DrawLocal( x, y );
	}

	return y;
}


int bkColor::GetGuid() const { return GetStaticGuid(); }


void bkColor::RemoteCreate() {
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetStaticGuid(),this);
	p.WriteWidget(m_Parent);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
	// translate the float3 to vector3 for the remote side (rag adds an alpha widget for anything not a vector3)
	p.Write_u32((m_colorType == COLOR_TYPE_FLOAT3) ? COLOR_TYPE_VECTOR3 : m_colorType);
	RemoteWriteColor(p);
	p.Send();
}


void bkColor::RemoteWriteColor(bkRemotePacket& packet)
{
	float r=1.0f,g=1.0f,b=1.0f,a=1.0f;
	switch (m_colorType)
	{
	case COLOR_TYPE_VECTOR3:
		r=m_Vector3->x;
		g=m_Vector3->y;
		b=m_Vector3->z;
		break;
	case COLOR_TYPE_VECTOR4:
		r=m_Vector4->x;
		g=m_Vector4->y;
		b=m_Vector4->z;
		a=m_Vector4->w;
		break;
	case COLOR_TYPE_COLOR32:
		r=m_Color32->GetRedf();
		g=m_Color32->GetGreenf();
		b=m_Color32->GetBluef();
		a=m_Color32->GetAlphaf();
		break;
	case COLOR_TYPE_FLOAT3:
		r=m_floats[0];
		g=m_floats[1];
		b=m_floats[2];
		break;
	case COLOR_TYPE_FLOAT4:
		r=m_floats[0];
		g=m_floats[1];
		b=m_floats[2];
		a=m_floats[3];
		break;
	case COLOR_TYPE_FLOAT163:
		r=m_float16s[0].GetFloat32_FromFloat16();
		g=m_float16s[1].GetFloat32_FromFloat16();
		b=m_float16s[2].GetFloat32_FromFloat16();
		break;
	case COLOR_TYPE_FLOAT164:
		r=m_float16s[0].GetFloat32_FromFloat16();
		g=m_float16s[1].GetFloat32_FromFloat16();
		b=m_float16s[2].GetFloat32_FromFloat16();
		a=m_float16s[3].GetFloat32_FromFloat16();
		break;
	}

	// Linear to sRGB
	if (m_inOutLinear)
	{
		const float degamma = 1.0f / 2.2f;
		r = Saturate(pow(r, degamma));
		g = Saturate(pow(g, degamma));
		b = Saturate(pow(b, degamma));
	}

	packet.Write_float(r);
	packet.Write_float(g);
	packet.Write_float(b);
	packet.Write_float(a);
}

void bkColor::RemoteReadColor(const bkRemotePacket& packet)
{
	float r=packet.Read_float();
	float g=packet.Read_float();
	float b=packet.Read_float();
	float a=packet.Read_float();

	// sRGB to linear
	if (m_inOutLinear)
	{
		r = Saturate(pow(r, 2.2f));
		g = Saturate(pow(g, 2.2f));
		b = Saturate(pow(b, 2.2f));
	}

	switch (m_colorType)
	{
	case COLOR_TYPE_VECTOR3:
		m_Vector3->Set(r,g,b);
		break;
	case COLOR_TYPE_VECTOR4:
		m_Vector4->Set(r,g,b,a);
		break;
	case COLOR_TYPE_COLOR32:
		m_Color32->SetRed((int)(r*255.0f));
		m_Color32->SetGreen((int)(g*255.0f));
		m_Color32->SetBlue((int)(b*255.0f));
		m_Color32->SetAlpha((int)(a*255.0f));
		break;
	case COLOR_TYPE_FLOAT3:
		m_floats[0]=r;
		m_floats[1]=g;
		m_floats[2]=b;
		break;
	case COLOR_TYPE_FLOAT4:
		m_floats[0]=r;
		m_floats[1]=g;
		m_floats[2]=b;
		m_floats[3]=a;
		break;
	case COLOR_TYPE_FLOAT163:
		m_float16s[0].SetFloat16_FromFloat32(r);
		m_float16s[1].SetFloat16_FromFloat32(g);
		m_float16s[2].SetFloat16_FromFloat32(b);
		break;
	case COLOR_TYPE_FLOAT164:
		m_float16s[0].SetFloat16_FromFloat32(r);
		m_float16s[1].SetFloat16_FromFloat32(g);
		m_float16s[2].SetFloat16_FromFloat32(b);
		m_float16s[3].SetFloat16_FromFloat32(a);
		break;
	}
}

void bkColor::Update()
{
	switch (m_colorType)
	{
	case COLOR_TYPE_VECTOR3:
	{
		Vector3 vec3(m_PrevValue.m_Vector[0], m_PrevValue.m_Vector[1], m_PrevValue.m_Vector[2]);
		if ( vec3.IsNotEqual( *m_Vector3 ) ) 
		{ 
			m_PrevValue.m_Vector[0] = m_Vector3->x;
			m_PrevValue.m_Vector[1] = m_Vector3->y;
			m_PrevValue.m_Vector[2] = m_Vector3->z;
			FinishUpdate(); 
		}
	}
		break;
	case COLOR_TYPE_VECTOR4:
	{
		Vector4 vec4(m_PrevValue.m_Vector[0], m_PrevValue.m_Vector[1], m_PrevValue.m_Vector[2], m_PrevValue.m_Vector[3]);
		if ( vec4.IsNotEqual( *m_Vector4 ) ) 
		{ 
			m_PrevValue.m_Vector[0] = m_Vector4->x;
			m_PrevValue.m_Vector[1] = m_Vector4->y;
			m_PrevValue.m_Vector[2] = m_Vector4->z;
			m_PrevValue.m_Vector[3] = m_Vector4->w;

			FinishUpdate(); 
		}
	}
		break;
	case COLOR_TYPE_COLOR32:
		if (m_Color32->GetColor() != m_PrevValue.m_Color32) 
		{ 
			m_PrevValue.m_Color32 = m_Color32->GetColor(); 
			FinishUpdate(); 
		}
		break;
	case COLOR_TYPE_FLOAT3:
		{
			if ( m_PrevValue.m_Vector[0] != m_floats[0] ||
		   		 m_PrevValue.m_Vector[1] != m_floats[1] ||
				 m_PrevValue.m_Vector[2] != m_floats[2] ) 
			{ 
				m_PrevValue.m_Vector[0] = m_floats[0];
				m_PrevValue.m_Vector[1] = m_floats[1];
				m_PrevValue.m_Vector[2] = m_floats[2];
				FinishUpdate(); 
			}
		}
		break;
	case COLOR_TYPE_FLOAT4:
		{
			if ( m_PrevValue.m_Vector[0] != m_floats[0] ||
		   		 m_PrevValue.m_Vector[1] != m_floats[1] ||
				 m_PrevValue.m_Vector[2] != m_floats[2] ||
				 m_PrevValue.m_Vector[3] != m_floats[3]) 
			{ 
				m_PrevValue.m_Vector[0] = m_floats[0];
				m_PrevValue.m_Vector[1] = m_floats[1];
				m_PrevValue.m_Vector[2] = m_floats[2];
				m_PrevValue.m_Vector[3] = m_floats[3];

				FinishUpdate(); 
			}
		}
		break;
	case COLOR_TYPE_FLOAT163:
		{
			const float currX = m_float16s[0].GetFloat32_FromFloat16();
			const float currY = m_float16s[1].GetFloat32_FromFloat16();
			const float currZ = m_float16s[2].GetFloat32_FromFloat16();

			if ( m_PrevValue.m_Vector[0] != currX ||
				 m_PrevValue.m_Vector[1] != currY ||
				 m_PrevValue.m_Vector[2] != currZ ) 
			{ 
				m_PrevValue.m_Vector[0] = currX;
				m_PrevValue.m_Vector[1] = currY;
				m_PrevValue.m_Vector[2] = currZ;

				FinishUpdate(); 
			}
		}
		break;
	case COLOR_TYPE_FLOAT164:
		{
			const float currX = m_float16s[0].GetFloat32_FromFloat16();
			const float currY = m_float16s[1].GetFloat32_FromFloat16();
			const float currZ = m_float16s[2].GetFloat32_FromFloat16();
			const float currW = m_float16s[3].GetFloat32_FromFloat16();

			if ( m_PrevValue.m_Vector[0] != currX ||
				 m_PrevValue.m_Vector[1] != currY ||
				 m_PrevValue.m_Vector[2] != currZ ||
				 m_PrevValue.m_Vector[3] != currW ) 
			{ 
				m_PrevValue.m_Vector[0] = currX;
				m_PrevValue.m_Vector[1] = currY;
				m_PrevValue.m_Vector[2] = currZ;
				m_PrevValue.m_Vector[3] = currW;

				FinishUpdate(); 
			}
		}
		break;
	}
}

int bkColor::GetComponent( EColorComponent component ) const
{
	switch ( component )
	{
	case ALPHA_COMPONENT:
		return GetAlpha();
	case RED_COMPONENT:
		return GetRed();
	case GREEN_COMPONENT:
		return GetGreen();
	case BLUE_COMPONENT:
		return GetBlue();
	}

	return 0;
}

float bkColor::GetComponentf( EColorComponent component ) const
{
	switch ( component )
	{
	case ALPHA_COMPONENT:
		return GetAlphaf();
	case RED_COMPONENT:
		return GetRedf();
	case GREEN_COMPONENT:
		return GetGreenf();
	case BLUE_COMPONENT:
		return GetBluef();
	}

	return 0;
}

bool bkColor::SetComponent( EColorComponent component, int value )
{	
	switch ( m_colorType )
	{
	case COLOR_TYPE_VECTOR3:
		{
			float val = Clamp( value / 255.0f, 0.0f, 1.0f );
			float oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				break;
			case RED_COMPONENT:
				oldVal = m_Vector3->GetX();
				m_Vector3->SetX( val );
				break;
			case GREEN_COMPONENT:
				oldVal = m_Vector3->GetY();
				m_Vector3->SetY( val );
				break;
			case BLUE_COMPONENT:
				oldVal = m_Vector3->GetZ();
				m_Vector3->SetZ( val );
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_VECTOR4:
		{
			float val = Clamp( (float)value / 255.0f, 0.0f, 1.0f );
			float oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				oldVal = m_Vector4->GetW();
				m_Vector4->SetW( val );
				break;
			case RED_COMPONENT:
				oldVal = m_Vector4->GetX();
				m_Vector4->SetX( val );
				break;
			case GREEN_COMPONENT:
				oldVal = m_Vector4->GetY();
				m_Vector4->SetY( val );
				break;
			case BLUE_COMPONENT:
				oldVal = m_Vector4->GetZ();
				m_Vector3->SetZ( val );
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_COLOR32:
		{
			int val = Clamp( value, 0, 255 );
			int oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				oldVal = m_Color32->GetAlpha();
				m_Color32->SetAlpha( val );
				break;
			case RED_COMPONENT:
				oldVal = m_Color32->GetRed();
				m_Color32->SetRed( val );
				break;
			case GREEN_COMPONENT:
				oldVal = m_Color32->GetGreen();
				m_Color32->SetGreen( val );
				break;
			case BLUE_COMPONENT:
				oldVal = m_Color32->GetBlue();
				m_Color32->SetBlue( val );
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_FLOAT3:
		{
			float val = Clamp( value / 255.0f, 0.0f, 1.0f );
			float oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				break;
			case RED_COMPONENT:
				oldVal = m_floats[0];
				m_floats[0] = val;
				break;
			case GREEN_COMPONENT:
				oldVal = m_floats[1];
				m_floats[1] = val;
				break;
			case BLUE_COMPONENT:
				oldVal = m_floats[2];
				m_floats[2] = val;
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_FLOAT4:
		{
			float val = Clamp( (float)value / 255.0f, 0.0f, 1.0f );
			float oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				oldVal = m_floats[3];
				m_floats[3] = val;
				break;
			case RED_COMPONENT:
				oldVal = m_floats[0];
				m_floats[0] = val;
				break;
			case GREEN_COMPONENT:
				oldVal = m_floats[1];
				m_floats[1] = val;
				break;
			case BLUE_COMPONENT:
				oldVal = m_floats[2];
				m_floats[2] = val;
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_FLOAT163:
		{
			float val = Clamp( value / 255.0f, 0.0f, 1.0f );
			float oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				break;
			case RED_COMPONENT:
				oldVal = m_float16s[0].GetFloat32_FromFloat16();
				m_float16s[0].SetFloat16_FromFloat32(val);
				break;
			case GREEN_COMPONENT:
				oldVal = m_float16s[1].GetFloat32_FromFloat16();
				m_float16s[1].SetFloat16_FromFloat32(val);
				break;
			case BLUE_COMPONENT:
				oldVal = m_float16s[2].GetFloat32_FromFloat16();
				m_float16s[2].SetFloat16_FromFloat32(val);
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_FLOAT164:
		{
			float val = Clamp( (float)value / 255.0f, 0.0f, 1.0f );
			float oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				oldVal = m_float16s[3].GetFloat32_FromFloat16();
				m_float16s[3].SetFloat16_FromFloat32(val);
				break;
			case RED_COMPONENT:
				oldVal = m_float16s[0].GetFloat32_FromFloat16();
				m_float16s[0].SetFloat16_FromFloat32(val);
				break;
			case GREEN_COMPONENT:
				oldVal = m_float16s[1].GetFloat32_FromFloat16();
				m_float16s[1].SetFloat16_FromFloat32(val);
				break;
			case BLUE_COMPONENT:
				oldVal = m_float16s[2].GetFloat32_FromFloat16();
				m_float16s[2].SetFloat16_FromFloat32(val);
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	}

	return false;
}

bool bkColor::SetComponentf( EColorComponent component, float value )
{
	switch ( m_colorType )
	{
	case COLOR_TYPE_VECTOR3:
		{
			float val = Clamp( value, 0.0f, 1.0f );
			float oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				break;
			case RED_COMPONENT:
				oldVal = m_Vector3->GetX();
				m_Vector3->SetX( val );
				break;
			case GREEN_COMPONENT:
				oldVal = m_Vector3->GetY();
				m_Vector3->SetY( val );
				break;
			case BLUE_COMPONENT:
				oldVal = m_Vector3->GetZ();
				m_Vector3->SetZ( val );
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_VECTOR4:
		{
			float val = Clamp( value, 0.0f, 1.0f );
			float oldVal = value;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				oldVal = m_Vector4->GetW();
				m_Vector4->SetW( val );
				break;
			case RED_COMPONENT:
				oldVal = m_Vector4->GetX();
				m_Vector4->SetX( val );
				break;
			case GREEN_COMPONENT:
				oldVal = m_Vector4->GetY();
				m_Vector4->SetY( val );
				break;
			case BLUE_COMPONENT:
				oldVal = m_Vector4->GetZ();
				m_Vector4->SetZ( val );
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_COLOR32:
		{
			int val = Clamp( (int)(value * 255.0f), 0, 255 );
			int oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				oldVal = m_Color32->GetAlpha();
				m_Color32->SetAlpha( val );
				break;
			case RED_COMPONENT:
				oldVal = m_Color32->GetRed();
				m_Color32->SetRed( val );
				break;
			case GREEN_COMPONENT:
				oldVal = m_Color32->GetGreen();
				m_Color32->SetGreen( val );
				break;
			case BLUE_COMPONENT:
				oldVal = m_Color32->GetBlue();
				m_Color32->SetBlue( val );
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_FLOAT3:
		{
			float val = Clamp( value, 0.0f, 1.0f );
			float oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				break;
			case RED_COMPONENT:
				oldVal = m_floats[0];
				m_floats[0] = val;
				break;
			case GREEN_COMPONENT:
				oldVal = m_floats[1];
				m_floats[1] = val;
				break;
			case BLUE_COMPONENT:
				oldVal = m_floats[2];
				m_floats[2] = val;
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_FLOAT4:
		{
			float val = Clamp( value, 0.0f, 1.0f );
			float oldVal = value;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				oldVal = m_floats[3];
				m_floats[3] = val;
				break;
			case RED_COMPONENT:
				oldVal = m_floats[0];
				m_floats[0] = val;
				break;
			case GREEN_COMPONENT:
				oldVal = m_floats[1];
				m_floats[1] = val;
				break;
			case BLUE_COMPONENT:
				oldVal = m_floats[2];
				m_floats[2] = val;
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_FLOAT163:
		{
			float val = Clamp( value, 0.0f, 1.0f );
			float oldVal = val;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				break;
			case RED_COMPONENT:
				oldVal = m_float16s[0].GetFloat32_FromFloat16();
				m_float16s[0].SetFloat16_FromFloat32(val);
				break;
			case GREEN_COMPONENT:
				oldVal = m_float16s[1].GetFloat32_FromFloat16();
				m_float16s[1].SetFloat16_FromFloat32(val);
				break;
			case BLUE_COMPONENT:
				oldVal = m_float16s[2].GetFloat32_FromFloat16();
				m_float16s[2].SetFloat16_FromFloat32(val);
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	case COLOR_TYPE_FLOAT164:
		{
			float val = Clamp( value, 0.0f, 1.0f );
			float oldVal = value;

			switch ( component )
			{
			case ALPHA_COMPONENT:
				oldVal = m_float16s[3].GetFloat32_FromFloat16();
				m_float16s[3].SetFloat16_FromFloat32(val);
				break;
			case RED_COMPONENT:
				oldVal = m_float16s[0].GetFloat32_FromFloat16();
				m_float16s[0].SetFloat16_FromFloat32(val);
				break;
			case GREEN_COMPONENT:
				oldVal = m_float16s[1].GetFloat32_FromFloat16();
				m_float16s[1].SetFloat16_FromFloat32(val);
				break;
			case BLUE_COMPONENT:
				oldVal = m_float16s[2].GetFloat32_FromFloat16();
				m_float16s[2].SetFloat16_FromFloat32(val);
				break;
			}

			return val != oldVal;
		}
		/*NOTREACHED*/
		break;
	}

	return false;
}


void bkColor::RemoteUpdate() {
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin(bkRemotePacket::CHANGED,GetStaticGuid(),this);
	RemoteWriteColor(p);
	p.Send();
}


void bkColor::RemoteHandler(const bkRemotePacket& packet) {
	if (packet.GetCommand() == bkRemotePacket::CREATE) {
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;
		const char *title = packet.Read_const_char();
		const char *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();
		bkColor* widget=NULL;
		switch ((EColorType)packet.Read_u32())
		{
		case COLOR_TYPE_VECTOR3: 
			widget = rage_new bkColor(*(rage_new Vector3),NullCB,title,memo,fillColor,readOnly);	
			break;
		case COLOR_TYPE_VECTOR4: 
			widget = rage_new bkColor(*(rage_new Vector4),NullCB,title,memo,fillColor,readOnly);	
			break;
		case COLOR_TYPE_COLOR32: 
			widget = rage_new bkColor(*(rage_new Color32),NullCB,title,memo,fillColor,readOnly);	
			break;
		case COLOR_TYPE_FLOAT3: 
			widget = rage_new bkColor((rage_new float),3,NullCB,title,memo,fillColor,readOnly);	
			break;
		case COLOR_TYPE_FLOAT4: 
			widget = rage_new bkColor((rage_new float),4,NullCB,title,memo,fillColor,readOnly);	
			break;
		case COLOR_TYPE_FLOAT163: 
			widget = rage_new bkColor((rage_new Float16),3,NullCB,title,memo,fillColor,readOnly);	
			break;
		case COLOR_TYPE_FLOAT164: 
			widget = rage_new bkColor((rage_new Float16),4,NullCB,title,memo,fillColor,readOnly);	
			break;
		}
		
		widget->RemoteReadColor(packet);
		packet.End();
		
		bkRemotePacket::SetWidgetId(*widget, id);
		parent->AddChild(*widget);
	}
	else if (packet.GetCommand() == bkRemotePacket::CHANGED) {
		packet.Begin();
		bkColor* widget = packet.ReadWidget<bkColor>();
		if (!widget) return;
		widget->RemoteReadColor(packet);
		packet.End();
		widget->Changed();
	}
}


#if __WIN32PC

#include "system/xtl.h"


void bkColor::WindowCreate() {
	if (!bkRemotePacket::IsConnectedToRag())
	{
		GetPane()->AddLastWindow(this, "BUTTON", 0, BS_PUSHBUTTON);
	}
}


rageLRESULT bkColor::WindowMessage(rageUINT msg,rageWPARAM /*wParam*/,rageLPARAM /*lParam*/) {
	if (msg == WM_COMMAND) {
		RemoteUpdate();
		Changed();
	}

	return 0;
}



#endif
#endif
