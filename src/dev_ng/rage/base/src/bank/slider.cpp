//
// bank/slider.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "slider.h"

#include "packet.h"
#include "pane.h"

#include "file/remote.h"
#include "math/amath.h"
#include "system/param.h"
#include "system/xtl.h"

#include "math/float16.h"
#include "vectormath/scalarv.h"

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace rage;

const float bkSlider::FLOAT_MIN_VALUE = -FLT_MAX / 1000.0f;
const float bkSlider::FLOAT_MAX_VALUE = +FLT_MAX / 1000.0f;

bkSlider::bkSlider(datCallback &callback,const char *title,const char *memo,const char *fillColor,bool exponential,bool readOnly) :
	bkWidget(callback,title,memo,fillColor,readOnly), 
	m_Exponential(exponential)
{
#if __WIN32PC
	m_Button = m_Edit = 0;
#endif
}


bkSlider::~bkSlider() {
}

float bkSlider::GetScale(const char *tip) {
	if (tip) {
		if (*tip == 'x')
			return (float) atof(tip+1);
		// if tip is "degrees" assume input is in radians
		else if (!strcmp(tip,"degrees"))
			return 57.295779513082320876798154814105f;	// DtoR
		// if tip is "mph" assume input is in M/s
		else if (!strcmp(tip,"mph"))
			return 2.237f;
		else if (!strcmp(tip,"kph"))
			return 3.600f;
	}
	return 1.0f;
}

void bkSlider::GetStringRepr( char *buf, int bufLen )
{
    char cBuf[128];
    safecpy( buf, GetString( cBuf, sizeof(cBuf) ), bufLen );
}

void bkSlider::SetStringRepr( const char *buf )
{
    if ( SetString( buf ) )
    {
        Changed();
    }
}

bkSliderVal<Float16>::bkSliderVal(datCallback &callback,const char *title,const char *memo,Float16 *data,float mini,float maxi,float step, const char* fillColor,bool exponential,bool readOnly) :
	bkSliderValFloat16Helper(data->GetFloat32_FromFloat16()), bkSliderVal<float>(callback,title,memo,&m_FakeValue,mini,maxi,step,fillColor,exponential,readOnly), m_RealValue(data)
{
}

void bkSliderVal<Float16>::Update()
{
#if HACK_GTA4
	// keep copying the real value (the 16 bit one) over the fake value (the full float) in case 
	// the value gets changed somehow other than using the slider itself
	// e.g. in the timecycle editor this value gets overwritten when a different cycle keyframe gets editted
	m_FakeValue = m_RealValue->GetFloat32_FromFloat16();
#endif
	bkSliderVal<float>::Update();
}

void bkSliderVal<Float16>::Changed()
{
	m_RealValue->SetFloat16_FromFloat32(m_FakeValue);
	bkSliderVal<float>::Changed();
}

bkSliderVal<ScalarV>::bkSliderVal(datCallback &callback,const char *title,const char *memo,ScalarV *data,float mini,float maxi,float step, const char* fillColor,bool exponential,bool readOnly) :
bkSliderValScalarVHelper(data->Getf()), bkSliderVal<float>(callback,title,memo,&m_FakeValue,mini,maxi,step,fillColor,exponential,readOnly), m_RealValue(data)
{
}

void bkSliderVal<ScalarV>::Update()
{
#if HACK_GTA4
	// keep copying the real value over the fake value in case 
	// the value gets changed somehow other than using the slider itself
	m_FakeValue = m_RealValue->Getf();
#endif
	bkSliderVal<float>::Update();
}

void bkSliderVal<ScalarV>::Changed()
{
	m_RealValue->Setf(m_FakeValue);
	bkSliderVal<float>::Changed();
}

#if __WIN32PC

static WNDPROC s_OldSliderEditWinProc;

static LRESULT CALLBACK Slider_WindowEditMessage(HWND hwnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	bkSlider *slider = (bkSlider*) GetWindowLongPtr(hwnd,GWLP_USERDATA);

	switch (iMsg)
	{
	case WM_CREATE:
            // Create a blue brush to be used for the edit control's
            // background color.
            // hBrush = CreateSolidBrush(RGB(0, 0, 255));
			return CallWindowProc(s_OldSliderEditWinProc,hwnd,iMsg,wParam,lParam);

	case WM_KEYDOWN:
		if (wParam==VK_RETURN)
		{
			::SetFocus(NULL);
			char buf[64];
			GetWindowText(hwnd,buf,sizeof(buf));
			slider->SetFont(false);
			if (!slider->ValidateString(buf,true) || !slider->SetString(buf))
			{
				slider->GetString(buf, sizeof(buf));
				SetWindowText(hwnd,buf);
				MessageBeep(MB_ICONHAND);
			}
			else
			{
				slider->Changed();
				return 0;
			}
			return 0;
		}
		else if (wParam==VK_LEFT || wParam==VK_RIGHT || wParam==VK_UP || wParam==VK_DOWN ||
				 wParam==VK_HOME || wParam==VK_END)
			return CallWindowProc(s_OldSliderEditWinProc,hwnd,iMsg,wParam,lParam);
		else if (wParam==VK_TAB) {
			bkWidget *i = slider->GetNext();
			while (i && !i->SetFocus())
				i = i->GetNext();
			if (!i) {
				i = slider->GetParent()->GetChild();
				while (i && !i->SetFocus())
					i = i->GetNext();
			}
			
			bkDisplayf("Next control!");
			return 0;
		}
		else 
			return 0;

	case WM_CHAR:
		if (wParam == '\t') {
			return 0;
		}
		else if (wParam != '\r')
		{
			if (!slider->CheckKey((char)wParam))
				return 0;

			slider->SetFont(true);
			LRESULT result=CallWindowProc(s_OldSliderEditWinProc,hwnd,iMsg,wParam,lParam);
			return result;
		}
		else 
			return 0;

	case WM_SETFOCUS:
		::SendMessage(hwnd,EM_SETSEL,0,~0);
		return CallWindowProc(s_OldSliderEditWinProc,hwnd,iMsg,wParam,lParam);

	case WM_KILLFOCUS:
		char buf[64];
		GetWindowText(hwnd,buf,sizeof(buf));
		slider->SetFont(false);
		if (!slider->ValidateString(buf,true) || !slider->SetString(buf))
		{
			slider->GetString(buf, sizeof(buf));
			SetWindowText(hwnd,buf);
			MessageBeep(MB_ICONHAND);
		}
		else
			slider->Changed();
		
		return CallWindowProc(s_OldSliderEditWinProc,hwnd,iMsg,wParam,lParam);

	default:
		return CallWindowProc(s_OldSliderEditWinProc,hwnd,iMsg,wParam,lParam);
		break;
	}
}

static WNDPROC s_OldSliderButtonWinProc;

static LRESULT CALLBACK WindowButtonMessage(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {
	static POINT p, start;
	static long lastXPos=0;
	static bool captured;

	if (msg == WM_LBUTTONDOWN || msg == WM_LBUTTONDBLCLK) {
		bkAssertf(!captured, "mouse was already captured");
		SetFocus(NULL);
		ShowCursor(FALSE);
		SetCapture(hwnd);
		SendMessage(hwnd,BM_SETSTATE,1,0);
		p.x = LOWORD(lParam);
		p.y = HIWORD(lParam);
		MapWindowPoints(hwnd,NULL,&p,1);
		lastXPos = p.x;
		start = p;
		captured = true;
		return 0;
	}
	else if (msg == WM_LBUTTONUP && captured) {
		ReleaseCapture();
		ShowCursor(TRUE);
		SetCursorPos(start.x,start.y);
		SendMessage(hwnd,BM_SETSTATE,0,0);
		captured = false;
		return 0;
	}
	else if (msg == WM_MOUSEMOVE) {
		if (captured) {
			float scale = 0.25f;
			if (wParam & MK_CONTROL)
				scale *= 4.0f;
			else if (wParam & MK_SHIFT)
				scale = 0.25f;

			lastXPos = p.x;
			p.x = (int)(short)LOWORD(lParam);
			p.y = (int)(short)HIWORD(lParam);
			MapWindowPoints(hwnd,NULL,&p,1);
			if (p.x >= GetSystemMetrics(SM_CXSCREEN) - 8)
				SetCursorPos(p.x = 16,p.y);
			else if (p.x < 8)
				SetCursorPos(p.x = GetSystemMetrics(SM_CXSCREEN) - 16,p.y);
			else {
				bkSlider *widget = (bkSlider*) GetWindowLongPtr(hwnd,GWLP_USERDATA);
				widget->Message(bkWidget::DIAL,scale * (p.x - lastXPos));
			}
		}
		return 0;
	}
	return CallWindowProc(s_OldSliderButtonWinProc,hwnd,msg,wParam,lParam);
}


void bkSlider::WindowCreate() {
	if (bkRemotePacket::IsConnectedToRag())
		return;

	bkPane *pane = GetPane();
	m_Button = pane->AddWindow(this, GetStep()? "BUTTON" : "STATIC", 0, GetStep()? BS_LEFT | BS_PUSHBUTTON : SS_LEFT | SS_NOPREFIX | SS_NOTIFY);
	m_Edit = pane->AddWindow(this, !GetStep()? "STATIC" : "EDIT", 0, !GetStep()? SS_CENTER | SS_NOPREFIX : ES_AUTOHSCROLL | ES_RIGHT | WS_BORDER);
	pane->AddLastWindow(this, "SCROLLBAR", 0, SBS_VERT);

	if (m_Button && GetStep())
		s_OldSliderButtonWinProc = (WNDPROC) SetWindowLongPtr(m_Button,GWLP_WNDPROC,(LONG_PTR)WindowButtonMessage);
	if (m_Edit && GetStep())
		s_OldSliderEditWinProc = (WNDPROC) SetWindowLongPtr(m_Edit,GWLP_WNDPROC,(LONG_PTR)Slider_WindowEditMessage);
}


void bkSlider::WindowDestroy() {
	if (m_Button) {
		DestroyWindow(m_Button);
		m_Button = 0;
	}
	if (m_Edit) {
		DestroyWindow(m_Edit);
		m_Edit = 0;
	}
	bkWidget::WindowDestroy();
}


rageLRESULT bkSlider::WindowMessage(rageUINT msg,rageWPARAM wParam,rageLPARAM /*lParam*/) {
	if (msg == WM_VSCROLL) {
		if (LOWORD(wParam) == SB_LINEUP)
			Message(DIAL,+1);
		else if (LOWORD(wParam) == SB_LINEDOWN)
			Message(DIAL,-1);
	}

	return 0;
}


int bkSlider::WindowResize(int x,int y,int width,int height) {
	const int scrollWidth = GetSystemMetrics(SM_CXVSCROLL);
	int editWidth = (width / 2) - scrollWidth;
	int editStart = width - scrollWidth - editWidth;
	if (m_Button)
		SetWindowPos(m_Button,0,x,y,editStart,height,SWP_NOACTIVATE | SWP_NOZORDER);
	if (m_Edit)
		SetWindowPos(m_Edit,0,x+editStart,y,editWidth,height,SWP_NOACTIVATE | SWP_NOZORDER);
	if (m_Hwnd)
		SetWindowPos(m_Hwnd,0,x+width-scrollWidth,y,scrollWidth,height,SWP_NOACTIVATE | SWP_NOZORDER);
	return y+height;
}

#endif


#if __WIN32PC
#define MAKE_CASE_CHECK(state,chr)	(state | (chr << 16))

bool bkSlider::ValidateString(const char* string,bool okOnly) const
{
	enum EnumState		{STATE_INITIAL,STATE_IPART,STATE_FPART,STATE_ESIGN,STATE_EPART};
	enum EnumValidation {VALID_START,VALID_ERROR,VALID_PARTIAL,VALID_OK};

	// culled from http://www.codeproject.com/editctrl/validator.asp
	EnumState		state			= STATE_INITIAL;
	EnumValidation	validateState	= VALID_START;
	int				StringLengthgth		= StringLength(string);
	for(int i = 0; validateState != VALID_ERROR && i < StringLengthgth;)
	{ /* scan string */
		char ch = string[i];
		switch(MAKE_CASE_CHECK(state, ch))
		{ /* states */
			case MAKE_CASE_CHECK(STATE_INITIAL, ' '):
			case MAKE_CASE_CHECK(STATE_INITIAL, '\t'):
				i++;
				continue;

			case MAKE_CASE_CHECK(STATE_INITIAL, '+'):
				i++;
				validateState = VALID_PARTIAL;
				state = STATE_IPART;
				continue;

			case MAKE_CASE_CHECK(STATE_INITIAL, '-'):
				if (CanBeNegative())
				{
					i++;
					validateState = VALID_PARTIAL;
					state = STATE_IPART;
				}
				else 
					validateState = VALID_ERROR;
				continue;

			case MAKE_CASE_CHECK(STATE_INITIAL, '0'):
			case MAKE_CASE_CHECK(STATE_INITIAL, '1'):
			case MAKE_CASE_CHECK(STATE_INITIAL, '2'):
			case MAKE_CASE_CHECK(STATE_INITIAL, '3'):
			case MAKE_CASE_CHECK(STATE_INITIAL, '4'):
			case MAKE_CASE_CHECK(STATE_INITIAL, '5'):
			case MAKE_CASE_CHECK(STATE_INITIAL, '6'):
			case MAKE_CASE_CHECK(STATE_INITIAL, '7'):
			case MAKE_CASE_CHECK(STATE_INITIAL, '8'):
			case MAKE_CASE_CHECK(STATE_INITIAL, '9'):
				state = STATE_IPART;
				continue;
			
			case MAKE_CASE_CHECK(STATE_INITIAL, '.'):
				if (CanBeFloatingPoint())
				{
					i++;
					validateState = VALID_PARTIAL;
					state = STATE_FPART;
				}
				else 
					validateState = VALID_ERROR;
				continue;
			
			case MAKE_CASE_CHECK(STATE_INITIAL, 'E'):
			case MAKE_CASE_CHECK(STATE_INITIAL, 'e'):
				if (CanBeFloatingPoint())
				{
					i++;
					state = STATE_ESIGN;
					validateState = VALID_PARTIAL;
				}
				else 
					validateState = VALID_ERROR;
				continue;
			
			case MAKE_CASE_CHECK(STATE_IPART, '0'):
			case MAKE_CASE_CHECK(STATE_IPART, '1'):
			case MAKE_CASE_CHECK(STATE_IPART, '2'):
			case MAKE_CASE_CHECK(STATE_IPART, '3'):
			case MAKE_CASE_CHECK(STATE_IPART, '4'):
			case MAKE_CASE_CHECK(STATE_IPART, '5'):
			case MAKE_CASE_CHECK(STATE_IPART, '6'):
			case MAKE_CASE_CHECK(STATE_IPART, '7'):
			case MAKE_CASE_CHECK(STATE_IPART, '8'):
			case MAKE_CASE_CHECK(STATE_IPART, '9'):
				i++;
				validateState = VALID_OK;
				continue;
			
			case MAKE_CASE_CHECK(STATE_IPART, '.'):
				if (CanBeFloatingPoint())
				{
					i++;
					validateState = VALID_OK;
					state = STATE_FPART;
				}
				else 
					validateState = VALID_ERROR;
				continue;
			
			case MAKE_CASE_CHECK(STATE_IPART, 'e'):
			case MAKE_CASE_CHECK(STATE_IPART, 'E'):
				if (CanBeFloatingPoint())
				{
					i++;
					validateState = VALID_PARTIAL;
					state = STATE_ESIGN;
				}
				else 
					validateState = VALID_ERROR;
				continue;
		
			case MAKE_CASE_CHECK(STATE_FPART, '0'):
			case MAKE_CASE_CHECK(STATE_FPART, '1'):
			case MAKE_CASE_CHECK(STATE_FPART, '2'):
			case MAKE_CASE_CHECK(STATE_FPART, '3'):
			case MAKE_CASE_CHECK(STATE_FPART, '4'):
			case MAKE_CASE_CHECK(STATE_FPART, '5'):
			case MAKE_CASE_CHECK(STATE_FPART, '6'):
			case MAKE_CASE_CHECK(STATE_FPART, '7'):
			case MAKE_CASE_CHECK(STATE_FPART, '8'):
			case MAKE_CASE_CHECK(STATE_FPART, '9'):
				i++;
				validateState = VALID_OK;
				continue;
			
			case MAKE_CASE_CHECK(STATE_FPART, 'e'):
			case MAKE_CASE_CHECK(STATE_FPART, 'E'):
				if (CanBeFloatingPoint())
				{
					i++;
					validateState = VALID_PARTIAL;
					state = STATE_ESIGN;
				}
				else 
					validateState = VALID_ERROR;
				continue;
			
			case MAKE_CASE_CHECK(STATE_ESIGN, '+'):
			case MAKE_CASE_CHECK(STATE_ESIGN, '-'):
				i++;
				validateState = VALID_PARTIAL;
				state = STATE_EPART;
				continue;
			
			case MAKE_CASE_CHECK(STATE_ESIGN, '0'):
			case MAKE_CASE_CHECK(STATE_ESIGN, '1'):
			case MAKE_CASE_CHECK(STATE_ESIGN, '2'):
			case MAKE_CASE_CHECK(STATE_ESIGN, '3'):
			case MAKE_CASE_CHECK(STATE_ESIGN, '4'):
			case MAKE_CASE_CHECK(STATE_ESIGN, '5'):
			case MAKE_CASE_CHECK(STATE_ESIGN, '6'):
			case MAKE_CASE_CHECK(STATE_ESIGN, '7'):
			case MAKE_CASE_CHECK(STATE_ESIGN, '8'):
			case MAKE_CASE_CHECK(STATE_ESIGN, '9'):
				state = STATE_EPART;
				continue;
			
			case MAKE_CASE_CHECK(STATE_EPART, '0'):
			case MAKE_CASE_CHECK(STATE_EPART, '1'):
			case MAKE_CASE_CHECK(STATE_EPART, '2'):
			case MAKE_CASE_CHECK(STATE_EPART, '3'):
			case MAKE_CASE_CHECK(STATE_EPART, '4'):
			case MAKE_CASE_CHECK(STATE_EPART, '5'):
			case MAKE_CASE_CHECK(STATE_EPART, '6'):
			case MAKE_CASE_CHECK(STATE_EPART, '7'):
			case MAKE_CASE_CHECK(STATE_EPART, '8'):
			case MAKE_CASE_CHECK(STATE_EPART, '9'):
				i++;
				validateState = VALID_OK;
				continue;
			
			default:
				validateState = VALID_ERROR;
				continue;
		} /* states */
	} /* scan string */

	return validateState==VALID_OK || (!okOnly && validateState==VALID_PARTIAL);
}

bool bkSlider::CheckKey(char key) const
{
	char temp1[128];
	GetWindowText(m_Edit,temp1,128);
	unsigned int start;
	unsigned int end;
	SendMessage(m_Edit,EM_GETSEL,(WPARAM)&start,(LPARAM)&end); 
 
	// backspace key:
	if (key==0x08)
		return true;
	else if ((key>='0' && key<='9') || (key=='-') || (key=='.') || (key=='+') || (key=='e') || (key=='E'))
	{
		// check to see if the created string would be valid:
		char temp2[256];
		if (start!=0)
			strncpy(temp2,temp1,start);
		const char* postfix=temp1+end;
		temp2[start]=key;
		temp2[start+1]=NULL;
		safecat(temp2,postfix);

		return ValidateString(temp2,false);
	}
	else 
		return false;
}

void bkSlider::WindowUpdate() {
	if (m_Edit) {
		char buffer[64];
		SetWindowText(m_Edit,GetString(buffer, sizeof(buffer)));
	}
}

void bkSlider::SetFont(bool bold) {
	if (GetPane())
	{
		if (bold)
			SendMessage(m_Edit,WM_SETFONT,(WPARAM)GetPane()->GetBoldFont(),MAKELPARAM(TRUE, 0));	
		else
			SendMessage(m_Edit,WM_SETFONT,(WPARAM)GetPane()->GetFont(),MAKELPARAM(TRUE, 0));	
	}
}
#endif


bool bkSlider::SetFocus() {
#if __WIN32PC
	if (m_Edit)
		::SetFocus(m_Edit);
#endif
	sm_Focus = this;
	return IsReadOnly();	// if we're disabled, we shouldn't get focus
}


#endif
