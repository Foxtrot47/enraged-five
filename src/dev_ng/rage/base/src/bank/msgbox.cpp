//
// bank/msgbox.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "msgbox.h"

#include "file/remote.h"

#if !__FINAL

namespace rage {

int bkMessageBox(const char *title,const char *message,bkMessageBoxType type,bkMessageBoxIcon icon) {
	static u32 typeFlags[] = { MB_OK, MB_OKCANCEL, MB_YESNO, MB_YESNOCANCEL };
	static u32 iconFlags[] = { MB_ICONINFORMATION, MB_ICONQUESTION, MB_ICONERROR };

	int result = fiRemoteShowMessageBox(message, title, typeFlags[type] | iconFlags[icon], IDOK);

	switch (result) {
	case IDCANCEL:
	default:
		return -1;
	case IDOK:
	case IDYES: return 1;
	case IDNO: return 0;
	}
}

}

#endif
