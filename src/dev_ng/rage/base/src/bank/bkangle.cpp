//
// bank/bkangle.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "bkangle.h"

#include "packet.h"
#include "pane.h"
#include "math/amath.h"
#include "vector/vector2.h"

using namespace rage;

using namespace bkAngleType;

Vector2 bkAngle::sm_tempReturnVector;

bkAngle::bkAngle( datCallback &callback, const char *title, const char *memo, float *data, EAngleType angleType, float min, float max, const char *fillColor, bool readOnly )
	: bkWidget( callback, title, memo, fillColor, readOnly )
	, m_minValue( min )
	, m_maxValue( max )
{
	bkAssertf( angleType != VECTOR2, "AngleType type bkAngle::VECTOR2 is not supported with this constructor" );
	m_angleType = angleType;
	m_value.f = data;
	m_prevValue.f = rage_new float;
	*(m_prevValue.f) = *data;
}

bkAngle::bkAngle( datCallback &callback, const char *title, const char *memo, Vector2 *data, float min, float max, const char *fillColor, bool readOnly )
	: bkWidget( callback, title, memo, fillColor, readOnly )
	, m_minValue( min )
	, m_maxValue( max )
{
	m_angleType = VECTOR2;
	m_value.v2 = data;
	m_prevValue.v2 = rage_new Vector2( data->x, data->y );
}

bkAngle::~bkAngle() 
{
	if ( m_angleType == VECTOR2 ) 
	{
		delete m_prevValue.v2;
		m_prevValue.v2 = NULL;
	}
	else
	{
		delete m_prevValue.f;
		m_prevValue.f = NULL;
	}
}

float bkAngle::GetDegrees() const
{
    switch ( m_angleType )
    {
    case DEGREES:
    default:
        return *(m_value.f);
    case FRACTION:
        return *(m_value.f) * 360.0f;
    case RADIANS:
        return *(m_value.f) * RtoD;
    case VECTOR2:
        return m_value.v2->Angle( Vector2( 0.0f, 1.0f ) ) * RtoD;
    }
}

void bkAngle::SetDegrees( float f )
{
    switch ( m_angleType )
    {
    case DEGREES:
        *(m_value.f) = f;
        break;
    case FRACTION:
        *(m_value.f) = f / 360.0f;
        break;
    case RADIANS:
        *(m_value.f) = f * DtoR;
        break;
    case VECTOR2:
        f *= DtoR;
        m_value.v2->Set( sinf( f ), cosf( f ) );
        break;
    }

    Changed();
}

float bkAngle::GetFraction() const
{
    switch ( m_angleType )
    {
    case DEGREES:
        return *(m_value.f) / 360.0f;
    case FRACTION:
    default:
        return *(m_value.f);
    case RADIANS:
        return *(m_value.f) / (2 * PI);
    case VECTOR2:
        return m_value.v2->Angle( Vector2( 0.0f, 1.0f ) ) / (2 * PI);
    }
}

void bkAngle::SetFraction( float f )
{
    switch ( m_angleType )
    {
    case DEGREES:
        *(m_value.f) = f * 360.0f;
        break;
    case FRACTION:
        *(m_value.f) = f;
        break;
    case RADIANS:
        *(m_value.f) = f * 2 * PI;
        break;
    case VECTOR2:
        f *= 2 * PI;
        m_value.v2->Set( sinf( f ), cosf( f ) );
        break;
    }

    Changed();
}

float bkAngle::GetRadians() const
{
    switch ( m_angleType )
    {
    case DEGREES:
        return *(m_value.f) * DtoR;
    case FRACTION:
        return *(m_value.f) * 2 * PI;
    case RADIANS:
    default:
        return *(m_value.f);
    case VECTOR2:
        return m_value.v2->Angle( Vector2( 0.0f, 1.0f ) );
    }
}

void bkAngle::SetRadians( float f )
{
    switch ( m_angleType )
    {
    case DEGREES:
        *(m_value.f) = f * RtoD;
        break;
    case FRACTION:
        *(m_value.f) = f / (2 * PI);
        break;
    case RADIANS:
        *(m_value.f) = f;
        break;
    case VECTOR2:
        m_value.v2->Set( sinf( f ), cosf( f ) );
        break;
    }

    Changed();
}

const Vector2& bkAngle::GetVector() const
{
    switch ( m_angleType )
    {
    case DEGREES:
        {
            float f = *(m_value.f) * DtoR;
            sm_tempReturnVector.Set( sinf( f ), cosf( f ) );
            return sm_tempReturnVector;
        }
    case FRACTION:
        {
            float f = *(m_value.f) * 2 * PI;
            sm_tempReturnVector.Set( sinf( f ), cosf( f ) );
            return sm_tempReturnVector;
        }
    case RADIANS:
        {
            float f = *(m_value.f);
            sm_tempReturnVector.Set( sinf( f ), cosf( f ) );
            return sm_tempReturnVector;
        }
    case VECTOR2:
    default:
        return *(m_value.v2);
    }
}

void bkAngle::SetVector( const Vector2 &v )
{
    switch ( m_angleType )
    {
    case DEGREES:
        *(m_value.f) = v.Angle( Vector2( 0.0f, 1.0f ) ) * RtoD;
        break;
    case FRACTION:
        *(m_value.f) = v.Angle( Vector2( 0.0f, 1.0f ) ) / (2 * PI);
        break;
    case RADIANS:
        *(m_value.f) = v.Angle( Vector2( 0.0f, 1.0f ) );
        break;
    case VECTOR2:
        m_value.v2->Set( v );
        break;
    }

    Changed();
}

void bkAngle::Update()
{
	if ( m_angleType == VECTOR2 )
	{
		if ( !m_value.v2->IsEqual( *(m_prevValue.v2) ) )
		{
			m_prevValue.v2->Set( m_value.v2->x, m_value.v2->y );
			FinishUpdate(); 
		}
	}
	else
	{
		if ( *(m_value.f) != *(m_prevValue.f) )
		{
			*(m_prevValue.f) = *(m_value.f);
			FinishUpdate();
		}
	}
}

void bkAngle::RemoteHandler( const bkRemotePacket& packet )
{
	if ( packet.GetCommand() == bkRemotePacket::CREATE )
	{
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if ( !parent )
		{
			return;
		}

		const char *title = packet.Read_const_char();
		const char *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();
		
		bkAngle::EAngleType angleType = (bkAngle::EAngleType)packet.Read_s32();
		float min = packet.Read_float();
		float max = packet.Read_float();

		float f[2];
		f[0] = packet.Read_float();
		if ( angleType == VECTOR2 )
		{
			f[1] = packet.Read_float();
		}

		packet.End();

		bkAngle* widget;
		if ( angleType == VECTOR2 )
		{
			widget = rage_new bkAngle( NullCB, title, memo, rage_new Vector2( f[0], f[1] ), min, max, fillColor, readOnly );
		}
		else
		{
			widget = rage_new bkAngle( NullCB, title, memo, &f[0], angleType, min, max, fillColor, readOnly );
		}

		bkRemotePacket::SetWidgetId( *widget, id );
		parent->AddChild( *widget );
	}
	else if ( packet.GetCommand() == bkRemotePacket::CHANGED ) 
	{
		packet.Begin();
		bkAngle* widget = packet.ReadWidget<bkAngle>();
		if ( !widget )
		{
			return;
		}

		float f[2];
		f[0] = packet.Read_float();
		if ( widget->m_angleType == VECTOR2 )
		{
			f[1] = packet.Read_float();
			widget->m_value.v2->Set( f[0], f[1] );
			widget->m_prevValue.v2->Set( f[0], f[1] );
		}
		else
		{
			*(widget->m_value.f) = f[0];
			*(widget->m_prevValue.f) = f[0];
		}
		
		packet.End();
		
		widget->WindowUpdate();
		widget->Changed();
	}
}

void bkAngle::GetStringRepr( char *buf, int bufLen )
{
    char cBuf[128];
    if ( m_angleType == VECTOR2 )
    {
        formatf( cBuf, "%f,%f", m_value.v2->x, m_value.v2->y );
    }
    else
    {
        formatf( cBuf, "%f", *(m_value.f) );
    }
    
    safecpy( buf, cBuf, bufLen );
}

void bkAngle::SetStringRepr( const char *buf )
{
    if ( m_angleType == VECTOR2 )
    {
        char cBuf[128];
        safecpy( cBuf, buf );

        char *pX = cBuf;
        char *pY = NULL;

        const char *pComma = strstr( cBuf, "," );        
        if ( pComma != NULL )
        {
            pY = const_cast<char *>( pComma ) + 1;
        }
        else
        {
            pY = pX;
        }

        m_value.v2->Set( (float)atof( pX ), (float)atof( pY ) );
    }
    else
    {
        *(m_value.f) = (float)atof( buf );
    }

    Changed();
}

void bkAngle::RemoteCreate()
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin( bkRemotePacket::CREATE, GetStaticGuid(), this );
	p.WriteWidget( m_Parent );
	p.Write_const_char( m_Title );
	p.Write_const_char( GetTooltip() );
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );

	p.Write_s32( m_angleType );
	p.Write_float( m_minValue );
	p.Write_float( m_maxValue );

	if ( m_angleType == VECTOR2 )
	{
		p.Write_float( m_value.v2->x );
		p.Write_float( m_value.v2->y );
	}
	else
	{
		p.Write_float( *(m_value.f) );
	}

	p.Send();
}

void bkAngle::RemoteUpdate()
{
	if ( !bkRemotePacket::IsConnected() )
	{
		return;
	}

	bkRemotePacket p;
	p.Begin( bkRemotePacket::CHANGED, GetStaticGuid(), this );

	if ( m_angleType == VECTOR2 )
	{
		p.Write_float( m_value.v2->x );
		p.Write_float( m_value.v2->y );
	}
	else
	{
		p.Write_float( *(m_value.f) );
	}

	p.Send();
}

#endif
