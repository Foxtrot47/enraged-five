//
// bank/combo.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_COMBO_H
#define BANK_COMBO_H

#if __BANK

#include "channel.h"
#include "packet.h"
#include "widget.h"

namespace rage {

class bkCombo: public bkWidget {
public:
	//
	// PURPOSE
	//	Updates the string of a combo box item
	// PARAMS
	//	id - the index in the combo list of the item to update
	//	str - the string to update the item to
	//
	void SetString(int id,const char *str);

	//
	// PURPOSE
	//	Returns the string of a combo box item
	// PARAMS
	//	id - the index in the combo list of the item to get
	// RETURNS
	//	the string
	//
	const char* GetString(int id);

	//
	// PURPOSE
	//	Returns the index of a string of a combo box item
	// PARAMS
	//	str - the string of the item to get
	// RETURNS
	//	the index of the item in the combo box, or -1 if it doesn't exist
	//
	int GetStringIndex(const char *str);

    //
    // PURPOSE
    //	Changes a combo list that is already in the bank.
    // PARAMS
    //	title - the string that describes what the combo widget effects
    //	value - a pointer to the combo index
    //	numitems - the number of items that is in the combo list
    //	list - a pointer to an array of strings that describe the combo list
    //	valueOffset - the offset applied to value
    //	callback - the callback that is called whenever the combo list value is changed
    //	memo - documentation for that the combo list value is
    virtual void UpdateCombo( const char *title, void *value, int numitems, const char **list,
        int valueOffset, datCallback callback=NullCB, const char *memo=0 );

    virtual void UpdateCombo( const char *title, void *value, int numitems, const char **list,
        datCallback callback=NullCB, const char *memo=0 );

    // use these accessors if you do not wish to change certain values when calling UpdateCombo
    virtual void* GetValuePtr() const { return 0; }
    int GetValueOffset() const { return m_Offset; }
	datCallback* GetCallback() { 
		if (m_ExtraData) {
			return m_ExtraData->m_Callback.GetType() == datCallback::NULL_CALLBACK ? NULL : &m_ExtraData->m_Callback;
		} else { 
			return NULL;
		}
	}

    // PURPOSE: Retrieves a string representation of the current value of this widget.
    // PARAMS:
    //    buf - the buffer to write the string representation to.
    //    bufLen - the length of the buffer.
    virtual void GetStringRepr( char *buf, int bufLen );

    // PURPOSE: Sets the current value of this widget using the string representation of the new value.
    // PARAMS:
    //    buf - the new value.
    virtual void SetStringRepr( const char *buf );

protected:
    //
    // PURPOSE
    //	Shared functionality for the above functions for changing a combo list that is already in the bank.
    // PARAMS
    //	title - the string that describes what the combo widget effects
    //	numitems - the number of items that is in the combo list
    //	list - a pointer to an array of strings that describe the combo list
    //	valueOffset - the offset applied to value
    //	callback - the callback that is called whenever the combo list value is changed
    //	memo - documentation for that the combo list value is
    void UpdateCombo( const char *title, int numitems, const char **list,
        int valueOffset, datCallback callback, const char *memo );

	bkCombo(datCallback &callback,const char *title,const char *memo,const char** items,int count,int offset, const char *fillColor=NULL, bool readOnly=false);
	~bkCombo();

public:
	virtual int GetValue() const = 0;
	virtual void SetValue(int i) = 0;
	
protected:
	virtual void RemoteUpdateComboItem( int id,const char* str );
	virtual void RemoteUpdateCombo();

#if __WIN32PC
	void WindowCreate();
	void WindowDestroy();
	int WindowResize(int x,int y,int width,int height);
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM);
	void WindowUpdate();
	struct HWND__ *m_Label;
#endif
	
	const char **m_Items;
	int m_Count, m_Offset;
};

template<typename _Type>
class bkComboVal: public bkCombo {
	friend class bkGroup;
	friend class bkManager;
public:
	void UpdateCombo( const char *title, void *value, int numitems, const char **list,
		int valueOffset, datCallback callback=NullCB, const char *memo=0 );
	void UpdateCombo( const char *title, void *value, int numitems, const char **list,
		datCallback callback=NullCB, const char *memo=0 );

	void* GetValuePtr() const;
protected:
	bkComboVal(datCallback &callback,const char *title,const char *memo,_Type *data,const char** items,int count,int offset, const char* fillColor=NULL, bool readOnly=false);
	~bkComboVal();

	void Message(Action action,float value);

	int DrawLocal(int x,int y);

	static inline int GetStaticGuid();
	int GetGuid() const;
	static void RemoteHandler(const bkRemotePacket& packet);
	void Update();

public:
	int GetValue() const;
	void SetValue(int value);

protected:
	void RemoteUpdateComboItem(int id,const char* str);
	void RemoteUpdateCombo();
	void RemoteCreate();

	void RemoteUpdate();

	_Type *m_Value, m_PrevValue;
};

template<typename _Type> void bkComboVal<_Type>::UpdateCombo( const char *title, void *value, int numitems, const char **list,
				 int valueOffset, datCallback callback, const char *memo )
{

	FastAssert( m_Value );
	ASSERT_ONLY(_Type preValue = *m_Value;)
	m_Value = (_Type *)value;
	if ( m_Value )
	{
		if ( *m_Value >= valueOffset + numitems )
		{
			*m_Value = (_Type)(valueOffset + numitems - 1);
			bkAssertf( 0, "UpdateCombo: Capped value of combo '%s' from %d to %d", title, preValue, *m_Value );
		}
		if ( *m_Value < valueOffset )
		{
			*m_Value = (_Type)valueOffset;
			bkAssertf( 0, "UpdateCombo: Capped value of combo '%s' from %d to %d", title, preValue, *m_Value );
		}
	}
	bkCombo::UpdateCombo( title, numitems, list, valueOffset, callback, memo );
}

template<typename _Type> void bkComboVal<_Type>::UpdateCombo( const char *title, void *value, int numitems, const char **list,
				 datCallback callback, const char *memo )
{
	FastAssert( m_Value );
	ASSERT_ONLY(_Type preValue = *m_Value;)
	m_Value = (_Type *)value;
	if ( m_Value )
	{
		if ( *m_Value >= m_Offset + numitems )
		{
			*m_Value = (_Type)(m_Offset + numitems - 1);
			bkAssertf( 0, "UpdateCombo: Capped value of combo '%s' from %d to %d", title, preValue, *m_Value );
		}
		if ( *m_Value < m_Offset )
		{
			*m_Value = (_Type)m_Offset;
			bkAssertf( 0, "UpdateCombo: Capped value of combo '%s' from %d to %d", title, preValue, *m_Value );
		}
	}
	bkCombo::UpdateCombo( title, numitems, list, m_Offset, callback, memo );
}

template<typename _Type> void* bkComboVal<_Type>::GetValuePtr() const {
	return m_Value; 
}

template<typename _Type> bkComboVal<_Type>::bkComboVal(datCallback &callback,const char *title,const char *memo,_Type *data,const char** items,int count,int offset, const char* fillColor, bool readOnly )
	: bkCombo(callback,title,memo,items,count,offset,fillColor,readOnly)
	, m_Value(data)
	, m_PrevValue(*data) 
{ 
}

template<typename _Type> bkComboVal<_Type>::~bkComboVal() { 
	if (bkRemotePacket::IsServer()) {
		delete m_Value; 
	}
}

template<typename _Type> void bkComboVal<_Type>::Message(Action action,float value)
{
	if (action == DIAL && value == 0) {
		(*m_Value)++;
		if (*m_Value == m_Offset + m_Count) 
		{
			*m_Value = (_Type) m_Offset;
		}
		Changed();
	}
	else if (action == DIAL && value < 0) {
		if (*m_Value != m_Offset) {
			--*m_Value;
			Changed();
		}
	}
	else if (action == DIAL && value > 0) {
		if (*m_Value != m_Offset + m_Count - 1) {
			++*m_Value;
			Changed();
		}
	}
}

template<typename _Type> int bkComboVal<_Type>::DrawLocal(int x,int y)
{
	Drawf(x,y,"%s: [%s]",m_Title,
		*m_Value >= m_Offset && *m_Value < m_Offset + m_Count? 
		m_Items[*m_Value - m_Offset] : "**invalid**"); 
	return bkWidget::DrawLocal(x,y); 
}

template<typename _Type> int bkComboVal<_Type>::GetGuid() const {
	return GetStaticGuid(); 
}

template<typename _Type> void bkComboVal<_Type>::RemoteHandler(const bkRemotePacket& packet) 
{
	if (packet.GetCommand() == bkRemotePacket::CREATE) {
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;
		const char *title = packet.Read_const_char(), *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();
		_Type *data = rage_new _Type;
		packet.ReadValue(*data);
		int count = packet.Read_s32();
		int offset = packet.Read_s32();
		packet.End();
		bkComboVal<_Type>& widget = *(rage_new bkComboVal<_Type>(NullCB,title,memo,data,0,count,offset,fillColor,readOnly));
		bkRemotePacket::SetWidgetId(widget, id);
		parent->AddChild(widget);
	}
	else if (packet.GetCommand() == bkRemotePacket::CHANGED) {
		packet.Begin();
		bkComboVal<_Type>* widget = packet.ReadWidget<bkComboVal>();
		if (!widget) return;
		packet.ReadValue(*widget->m_Value);
		widget->m_PrevValue = *widget->m_Value;
		widget->WindowUpdate();
		packet.End();
		widget->Changed();
	}
	else if (packet.GetCommand() == bkRemotePacket::USER+0) {
		packet.Begin();
		bkComboVal<_Type>* widget = packet.ReadWidget<bkComboVal>();
		if (!widget) return;
		int i = packet.Read_s32();
		const char *data = packet.Read_const_char();
		packet.End();
		widget->SetString(i,data);
		if (i+1 == widget->m_Count) {
			widget->WindowUpdate();
		}
	}
	else if (packet.GetCommand() == bkRemotePacket::USER+1)
	{
		packet.Begin();
		bkComboVal<_Type>* widget = packet.ReadWidget<bkComboVal>();
		if (!widget) return;

		if ( widget->m_Title )
		{
			ConstStringFree( widget->m_Title );
		}
		const char *str = ConstStringDuplicate( packet.Read_const_char() );
		widget->m_Title = str;

		packet.ReadValue(*widget->m_Value);
		widget->m_PrevValue = *widget->m_Value;

		for (int i = 0; i < widget->m_Count; ++i)
		{
			ConstStringFree( widget->m_Items[i] );
		}
		delete [] widget->m_Items;

		widget->m_Count = packet.Read_s32();

		widget->m_Items = rage_new const char*[widget->m_Count];
		for (int i = 0; i < widget->m_Count; ++i)
		{
			str = ConstStringDuplicate( packet.Read_const_char() );
			widget->m_Items[i] = str;
		}

		widget->m_Offset = packet.Read_s32();

		if ( widget->GetTooltip() )
		{
			ConstStringFree( widget->m_ExtraData->m_Tooltip );
			widget->m_ExtraData->m_Tooltip = NULL;
		}
		if (!widget->m_ExtraData)
		{
			widget->m_ExtraData = rage_new ExtraData;
		}
		widget->m_ExtraData->m_Tooltip = ConstStringDuplicate( packet.Read_const_char() );

		packet.End();
		widget->WindowUpdate();
	}
}

template<typename _Type> void bkComboVal<_Type>::Update() {
	if (*m_Value != m_PrevValue) { 
		m_PrevValue = *m_Value; 
		FinishUpdate(); 
	}
}

template<typename _Type> int bkComboVal<_Type>::GetValue() const {
	return *m_Value; 
}

template<typename _Type> void bkComboVal<_Type>::SetValue(int value) {
	// doing my own clamp to avoid pulling in math/amath.h
	int t = (value < 0 ? 0 : (value > m_Count - 1 ? m_Count - 1 : value));
	if ( *m_Value != t ) { 
		*m_Value = (_Type) t; 
		Changed(); 
	}
}

template<typename _Type> void bkComboVal<_Type>::RemoteUpdateComboItem(int id,const char* str)
{
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin(bkRemotePacket::USER+0,GetStaticGuid(),this);
	p.Write_s32(id);
	p.Write_const_char(str);
	p.Send();
}

template<typename _Type> void bkComboVal<_Type>::RemoteUpdateCombo()
{
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin( bkRemotePacket::USER+1, GetStaticGuid(), this );
	p.Write_const_char( GetTitle() );
	p.Write_const_char( GetTooltip() );
	p.WriteValue( *m_Value );
	p.Write_s32( m_Count );
	p.Write_s32( m_Offset );
	p.Send();
	for (int i = 0; i < m_Count; ++i)
	{
		bkRemotePacket p2;
		p2.Begin( bkRemotePacket::USER, GetStaticGuid(), this );
		p2.Write_s32( i );
		p2.Write_const_char( m_Items[i] );
		p2.Send();
	}

}

template<typename _Type> void bkComboVal<_Type>::RemoteCreate()
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetStaticGuid(),this);
	p.WriteWidget(m_Parent);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
	p.WriteValue(*m_Value);
	p.Write_s32(m_Count);
	p.Write_s32(m_Offset);
	p.Send();
	for (int i=0; i<m_Count; i++) {
		bkRemotePacket p2;
		p2.Begin(bkRemotePacket::USER,GetStaticGuid(),this);
		p2.Write_s32(i);
		p2.Write_const_char(m_Items[i]);
		p2.Send();
	}

}

template<typename _Type> void bkComboVal<_Type>::RemoteUpdate()
{
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin(bkRemotePacket::CHANGED,GetStaticGuid(),this);
	p.WriteValue(*m_Value);
	p.Send();
}

template<> inline int bkComboVal<u8>::GetStaticGuid() {	return BKGUID('c','o','u','8'); }
template<> inline int bkComboVal<s8>::GetStaticGuid() { return BKGUID('c','o','s','8'); }
template<> inline int bkComboVal<s16>::GetStaticGuid() { return BKGUID('c','o','s','1'); }
template<> inline int bkComboVal<s32>::GetStaticGuid() { return BKGUID('c','o','s','3'); }

}	// namespace rage

#endif

#endif
