//
// bank/toggle.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "toggle.h"

#include "pane.h"

#include "atl/bitset.h"

using namespace rage;

bkToggle::bkToggle(datCallback &callback,const char *title,const char *memo, const char *fillColor, bool readOnly) :
	bkWidget(callback,title,memo,fillColor,readOnly) { 
}


bkToggle::~bkToggle() {
}

void bkToggle::GetStringRepr( char *buf, int bufLen )
{
    char cBuf[8];
    formatf( cBuf, "%s", GetBool() ? "True" : "False" );
    safecpy( buf, cBuf, bufLen );
}

void bkToggle::SetStringRepr( const char *buf )
{
    SetBool( (buf != NULL) && (strlen( buf ) > 0) && (buf[0] != '0') && (stricmp( buf, "f" ) != 0) && (stricmp( buf, "false" ) != 0) );    
}

#if __WIN32PC
#include "system/xtl.h"

void bkToggle::WindowCreate() {
	if (!bkRemotePacket::IsConnectedToRag())
		GetPane()->AddLastWindow(this, "BUTTON", 0, BS_AUTOCHECKBOX | BS_RIGHTBUTTON);
}


void bkToggle::WindowUpdate() {
	if (m_Hwnd)
		SendMessage(m_Hwnd,BM_SETCHECK,GetBool(),0);
}


rageLRESULT bkToggle::WindowMessage(rageUINT msg,rageWPARAM wParam,rageLPARAM /*lParam*/) {
	if (msg == WM_COMMAND && HIWORD(wParam) == BN_CLICKED) {
		SetBool(SendMessage(m_Hwnd,BM_GETCHECK,0,0) != 0);
	}
	return 0;
}
#endif

#endif
