//
// bank/title.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_TITLE_H
#define BANK_TITLE_H

#include "widget.h"

#if __BANK

namespace rage {

class bkTitle: public bkWidget
{
	friend class bkGroup;
	friend class bkManager;
protected:
	bkTitle(const char *title, const char *fillColor=NULL);
	~bkTitle();

	int DrawLocal(int x,int y);
	static int GetStaticGuid() { return BKGUID('t','i','t','l'); }
	int GetGuid() const;

	static void RemoteHandler(const bkRemotePacket& p);

private:
#if __WIN32PC
	void WindowCreate();
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM);
#endif
	void RemoteCreate();
};

}	// namespace rage

#endif

#endif
