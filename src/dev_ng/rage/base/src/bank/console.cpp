// 
// bank/console.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if !__FINAL

#include "console.h"
#include "diag/channel.h"

#include "file/tcpip.h"
#include "system/alloca.h"
#include "system/timemgr.h"
#include "atl/array.h"
#include "diag/output.h"

namespace rage {

bkConsole* bkConsole::sm_Instance=NULL;

bkConsole::bkConsole( bool usePowerConsole )
{
	// reserved words:
	CommandFunctor widgetCallback;
	widgetCallback.Reset<bkConsole,&bkConsole::Widget>(this);
	m_Map.Insert(ConstString("widget"),widgetCallback);
	CommandFunctor aliasCallback;
	aliasCallback.Reset<bkConsole,&bkConsole::Alias>(this);
	m_Map.Insert(ConstString("alias"),aliasCallback);
	CommandFunctor helpCallback;
	helpCallback.Reset<bkConsole,&bkConsole::Help>(this);
	m_Map.Insert(ConstString("help"),helpCallback);

	m_TcpHandle=fiHandleInvalid;
	m_SemaConnect=sysIpcCreateSema(true);

	m_usePowerConsole = usePowerConsole;
}


void bkConsole::WaitForConnection()
{
	// wait until a connection:
	while (true)
	{
		int port;
		const char* addr;
		if ( m_usePowerConsole )
		{
			port = CONSOLE_PORT;
			addr = fiDeviceTcpIp::GetLocalHost();
		}
		else 
		{
#if __DEV | __BANK
			port = bkRemotePacket::GetRagSocketPort( RAG_CONSOLE_PORT_OFFSET );
			addr = bkRemotePacket::GetRagSocketAddress();
#else
			port = CONSOLE_PORT;
			addr = fiDeviceTcpIp::GetLocalHost();
#endif // __DEV | __BANK
		}

		fiHandle handle=fiDeviceTcpIp::Connect( addr, port );
		if (handle==fiHandleInvalid)
		{
			sysIpcSleep(100);
		}
		else
		{
			sysIpcWaitSema(m_SemaConnect);
			bkDisplayf("Connected to console at port %d",port);
			m_TcpHandle=handle;
			sysIpcSignalSema(m_SemaConnect);
			break;
		}
	}
}

inline unsigned short SWAPS(unsigned short s) { return (s << 8) | (s >> 8); }
inline unsigned SWAPL(unsigned s) {
	return (s >> 24) | ((s >> 8) & 0xFF00) | ((s << 8) & 0xFF0000) | (s << 24);
}

void bkConsole::ReadAndExecuteCommand()
{
	struct CommandHeader
	{
		unsigned short		m_NumArgs;
		unsigned short		m_NumChars;
		unsigned short		m_CommandNameLength;
	};

	struct Command
	{
		CommandHeader		m_Header;
		char*				m_Chars;
		char*				m_CommandName;

		u32*				m_ArgOffsets;
		const char**		m_ArgPointers;

	} theCommand;

	CommandHeader& header=theCommand.m_Header;

	// read the initial header (m_NumArgs, m_NumChars):
	fiDeviceTcpIp::GetInstance().SafeRead(m_TcpHandle,&header,sizeof(CommandHeader));
	
#if __BE
	// byte swap:
	header.m_NumArgs			=SWAPS(header.m_NumArgs);
	header.m_NumChars			=SWAPS(header.m_NumChars);
	header.m_CommandNameLength	=SWAPS(header.m_CommandNameLength);
#endif
	
	theCommand.m_Chars			=Alloca(char,header.m_NumChars);
	theCommand.m_CommandName	=Alloca(char,header.m_CommandNameLength);
	
	// read the characters:
	fiDeviceTcpIp::GetInstance().SafeRead(m_TcpHandle,theCommand.m_Chars,header.m_NumChars);
	
	// read the argument char* offsets:
	if (header.m_NumArgs)
	{
		theCommand.m_ArgOffsets		=Alloca(u32,header.m_NumArgs);
		theCommand.m_ArgPointers	=Alloca(const char*,header.m_NumArgs);
		fiDeviceTcpIp::GetInstance().SafeRead(m_TcpHandle,theCommand.m_ArgOffsets,header.m_NumArgs*sizeof(int));

		for (int i=0;i<header.m_NumArgs;i++)
		{
#if __BE
			theCommand.m_ArgPointers[i]=theCommand.m_Chars+SWAPL(theCommand.m_ArgOffsets[i]);
#else
			theCommand.m_ArgPointers[i]=theCommand.m_Chars+theCommand.m_ArgOffsets[i];
#endif
		}
	}
	
	// read the command name:
	fiDeviceTcpIp::GetInstance().SafeRead(m_TcpHandle,theCommand.m_CommandName,header.m_CommandNameLength);

	// execute the command:
	CommandFunctor* functor=m_Map.Access(theCommand.m_CommandName);
	char returnMsg[1];
	if (functor && functor->IsValid())
	{
		OutputFunctor outputFunctor;
		outputFunctor.Reset<bkConsole,&bkConsole::SendOutput>(this);
		(*functor)(theCommand.m_ArgPointers,header.m_NumArgs,outputFunctor);

		returnMsg[0]=1;
	}
	else 
	{
		returnMsg[0]=0;
	}

	// send acknowledgement that command has been completed:
	fiDeviceTcpIp::GetInstance().SafeWrite(m_TcpHandle,returnMsg,1);
}
static void SplitString(  rage::atArray<const char*>& list, char* msg , const char delim = ',' )
{
	bool IsStringStart =true;
	int len = StringLength( msg );
	for ( int i =0; i < len; i++ )
	{
		if ( IsStringStart && !( msg[i] == delim || msg[i]=='\n'))
		{
			list.PushAndGrow( &msg[i] );
			IsStringStart = false;
		}
		if ( msg[i] == delim || msg[i]=='\n')
		{
			msg[i]='\0';
			IsStringStart = true;
		}
	}
}

// --- redirecting output part
void (*g_OldPrintString)(const char*)=NULL;
bkConsole::OutputFunctor*	g_OutputFunctor = 0;


void sPrintToConsole(const char* str)
{
	if (g_OldPrintString)
		g_OldPrintString(str);

	if ( g_OutputFunctor )
	{
		(*g_OutputFunctor)( str);
	} 
}
// --- redirecting output part
void bkConsole::PSReadAndExecuteCommand()
{
	char msg[1025];
	// read the characters: no byte swapping necessary
	int messageSize = fiDeviceTcpIp::GetInstance().Read(m_TcpHandle, &msg, 1024);
	msg[messageSize] = '\0'; // null terminate the end
	bkDisplayf( "PSH: %s", msg);

	msg[messageSize] = '\n';
	msg[messageSize + 1] = '\0';	

	// split up the input into tokens
	atArray<const char*> list;
	SplitString( list, msg ,',');
	
	if ( list.GetCount() == 0 )
	{
		bkErrorf("PSH: command sent with no command" );
		return;
	}

	// execute the command:
	CommandFunctor* functor=m_Map.Access(list[0]);
	if (functor && functor->IsValid())
	{
		OutputFunctor outputFunctor;
		outputFunctor.Reset<bkConsole,&bkConsole::SendOutput>(this);		

		DIAG_CONTEXT_MESSAGE("When Executed Console Command");

		// dump output to console
		g_OutputFunctor = &outputFunctor;
		OUTPUT_ONLY(g_OldPrintString = diagPrintCallback);
		OUTPUT_ONLY(rage::diagPrintCallback=sPrintToConsole);

		m_outputSent = false;

#if __XENON
		try
		{
			(*functor)(list.GetCount() > 1 ? &list[1] : 0 ,list.GetCount() - 1,outputFunctor);
		}
		catch(...) 
		{
			SendOutput("Exception occurred when running command");

		//	SendOutput(e->what());
		}
		if (!m_outputSent)  // send something to show that it worked
		{
			SendOutput("$true");
		}
#endif
		// reset
		g_OutputFunctor = 0;
		OUTPUT_ONLY(rage::diagPrintCallback=g_OldPrintString);
		OUTPUT_ONLY(g_OldPrintString=0);
	}
	else
	{
		SendOutput("Error 0: command not found");
	}
}

void bkConsole::ExecuteString(const char *cmsg)
{
	char msg[128];
	safecpy(msg,cmsg);

	// split up the input into tokens
	atArray<const char*> list;
	SplitString( list, msg ,' ');
	
	if ( list.GetCount() == 0 )
	{
		bkErrorf("PSH: command sent with no command" );
		return;
	}

	// execute the command:
	CommandFunctor* functor=m_Map.Access(list[0]);
	if (functor && functor->IsValid())
	{
		OutputFunctor outputFunctor;
		outputFunctor.Reset<sPrintToConsole>();		

		DIAG_CONTEXT_MESSAGE("When Executed Console Command" );

		// dump output to console
		OUTPUT_ONLY(g_OldPrintString = rage::diagPrintCallback);
		OUTPUT_ONLY(rage::diagPrintCallback = sPrintToConsole);

		(*functor)(list.GetCount() > 1 ? &list[1] : 0 ,list.GetCount() - 1,outputFunctor);

		OUTPUT_ONLY(rage::diagPrintCallback = g_OldPrintString);
	}
	else
		bkErrorf("Uknown console command '%s' (try 'help')",list[0]);
}


void bkConsole::Update()
{
	// receive and execute all commands...
	sysIpcWaitSema(m_SemaConnect);
	if (fiIsValidHandle(m_TcpHandle))
	{
		while (/* int readCount=*/ fiDeviceTcpIp::GetReadCount(m_TcpHandle))
		{
			if ( m_usePowerConsole )
			{
				PSReadAndExecuteCommand();
			}
			else
			{
				ReadAndExecuteCommand(); // using rag
			}
		}
		
	}
	sysIpcSignalSema(m_SemaConnect);
}

void bkConsole::AddCommand(const char* commandName,CommandFunctor callback)
{
	if (!sm_Instance)
	{
		return;
	}

	if (sm_Instance->m_Map.Access(commandName))
	{
		Quitf("Command '%s' already added to the console or is a reserved command!",commandName);
	}
	else
		sm_Instance->m_Map.Insert(ConstString(commandName),callback);
}

void bkConsole::RemoveCommand(const char* commandName)
{
	if (!sm_Instance)
	{
		return;
	}

	sm_Instance->m_Map.Delete(commandName);
}

void bkConsole::SendOutput(const char* output)
{
	int outputLen=(int)strlen(output);
	if (outputLen && fiIsValidHandle(m_TcpHandle))
	{
		if (outputLen>1 || output[0]>1)
		{
			fiDeviceTcpIp::GetInstance().SafeWrite(m_TcpHandle,output,(int)strlen(output));
			m_outputSent = true;
		}
	}
}


void bkConsole::Help(CONSOLE_ARG_LIST)
{
	CONSOLE_HELP_TEXT("List all known commands, or ask for help about one of them.");

	if (numArgs)
	{
		CommandFunctor* functor=m_Map.Access(args[0]);
		if (functor && functor->IsValid())
		{
			OutputFunctor outputFunctor;
			outputFunctor.Reset<bkConsole,&bkConsole::SendOutput>(this);
			const char *h = "-help";
			(*functor)(&h,1,outputFunctor);
			return;
		}
		else
			output("Unknown command, cannot help you.\n");
		return;
	}

	
	if ( !m_usePowerConsole )
	{
		output("All commands: use 'help <command>' or '<command> -help' for more information\n");
	}
	atMap<ConstString,CommandFunctor>::Iterator entry = m_Map.CreateIterator();
	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		output(entry.GetKey());
		if ( m_usePowerConsole )
		{
			output(",");
		}
		output("\n");
	}
}


void bkConsole::Alias(CONSOLE_ARG_LIST)
{
	CONSOLE_HELP_TEXT("Abbreviate a longer command (implemented by RAG)");
}


void bkConsole::Widget(CONSOLE_ARG_LIST)
{
	CONSOLE_HELP_TEXT("Manipulate a widget (implemented by RAG)");
}

} // namespace rage

#endif
