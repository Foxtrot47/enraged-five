//
// bank/io.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_IO_H
#define BANK_IO_H

#include "data/base.h"

namespace rage {

class bkBank;
class bkManager;
class bkWidget;

//
// PURPOSE
//	An interface for bridging input and the visual representation
//  of the bank.  A higher level module defines a subclass of this 
//  class to define its specific input and drawing implementations.
//  <FLAG Component>
//
class bkIo : public datBase {
	friend class bkManager;
	friend class bkWidget;

public:
	// PURPOSE: deletes the IO manager for the bank IO
	static void DeleteIoManager();

	//
	// PURPOSE
	//	sets the x and y starting position for the Draw() function
	// PARAMS
	//	x - the horizontal starting position
	//	y - the vertical starting position
	//
	static void SetBase(int x,int y);
	static int GetBaseX();
	static int GetBaseY();

	static bkIo *GetInstance();

#if __BANK
	static void AddWidgets();
	static void RemoveWidgets();
#endif

	static void SetPadIndex(int index);

protected:
	// PURPOSE: constructor
	bkIo();
	// PURPOSE: destructor
	virtual ~bkIo();

	//
	// PURPOSE
	//	function for drawing a text string
	// PARAMS
	//	x - the x position on the screen to draw the string 
	//	y - the y position on the screen to draw the string
	//	string - the string to draw
	//
	virtual void Draw(int x,int y,const char *string) const = 0;
	virtual void Input(int &pressed,int &down,float &dial) const = 0;

	static int GetPadIndex();

	enum { NONE, TRIGGER, UP, LEFT, RIGHT, DOWN, UP_TO_BANK };

private:
	static bkIo *sm_Instance;

protected:
	static int sm_BaseX, sm_BaseY, sm_PadIndex;

#if __BANK
	static bkBank *sm_pBank;
#endif
};

inline int bkIo::GetPadIndex() 
{ 
	return sm_PadIndex; 
}

inline void bkIo::SetPadIndex(int index)
{ 
	sm_PadIndex = index; 
}

inline bkIo* bkIo::GetInstance() 
{ 
	return sm_Instance; 
}

inline int bkIo::GetBaseX() 
{ 
	return sm_BaseX; 
}

inline int bkIo::GetBaseY() 
{ 
	return sm_BaseY; 
}

inline void bkIo::SetBase(int x,int y) 
{ 
	sm_BaseX = x; 
	sm_BaseY = y; 
}

inline void bkIo::DeleteIoManager()
{
#if __BANK
	bkIo::RemoveWidgets();
#endif

	delete sm_Instance;
	sm_Instance=NULL;
}


}	// namespace rage

#endif
