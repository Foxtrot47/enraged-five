//
// bank/list.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_LIST_H
#define BANK_LIST_H

#if __BANK

#include "widget.h"

#include "atl/atfunctor.h"

namespace rage {
class bkList : public bkWidget {
	friend class bkGroup;
	friend class bkManager;

	friend int sGetStaticGuid();

public:
	enum ItemType
	{
		STRING,
		INT,
		FLOAT
	};

	typedef atFunctor3<void, s32, s32, const char*> UpdateItemFuncType;
	typedef atFunctor1<void, s32> ClickItemFuncType;

	//
	// PURPOSE
	//	Set the functor that gets called when the GUI updates an item.
	//  UpdateItemFuncType receives arguments in this order :
	//	the s32 key, s32 column, and the const char* newValue.
	// PARAMS
	//	func - the functor we'll call when an item is updated by the GUI
	//
	void SetUpdateItemFunc(UpdateItemFuncType func);

	//
	// PURPOSE
	//  Set the functor that gets called when an item is double clicked in the GUI
	//	ClickItemFuncType recieved arguments in this order :
	//  the s32 key
	// PARAMS
	//  func - the functor we'll call when an item is double clicked in the GUI
	void SetDoubleClickItemFunc(ClickItemFuncType func);
	void SetSingleClickItemFunc(ClickItemFuncType func);

	void AddColumnHeader(int column,const char* heading,ItemType type);
	void AddItem(s32 key,int column,float value);
	void AddItem(s32 key,int column,int value);
	void AddItem(s32 key,int column,const char* value);
	void RemoveItem(s32 key);

protected:
	bkList(const char *title,bool readOnly,const char *memo, const char *fillColor=NULL);
	~bkList();


	static int GetStaticGuid() { return BKGUID('l','i','s','t'); }
	int GetGuid() const;

	int DrawLocal(int x,int y);

	void Update();
	static void RemoteHandler(const bkRemotePacket& p);

	UpdateItemFuncType m_UpdateItemFunc;
	ClickItemFuncType m_SingleClickItemFunc, m_DoubleClickItemFunc;

protected:
	void RemoteCreate();
	void RemoteUpdate();

#if __WIN32PC
	void WindowCreate();
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM) { return 0; }
#endif

private:
	bkList();
	const bkList& operator=(bkList&);
};


inline void bkList::SetUpdateItemFunc(UpdateItemFuncType func)
{
	m_UpdateItemFunc=func;
}

inline void bkList::SetDoubleClickItemFunc(ClickItemFuncType func)
{
	m_DoubleClickItemFunc=func;
}

inline void bkList::SetSingleClickItemFunc(ClickItemFuncType func)
{
	m_SingleClickItemFunc=func;
}

}	// namespace rage


#endif

#endif
