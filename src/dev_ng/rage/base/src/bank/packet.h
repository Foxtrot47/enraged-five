//
// bank/packet.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_PACKET_H
#define BANK_PACKET_H

#include "channel.h"
#include "widget.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "atl/inmap.h"
#include "system/criticalsection.h"
#include "system/pipepacket.h"
#include "system/typeinfo.h"

#if (__DEV || __BANK) && !__SPU

namespace rage {

class sysCompressionFactory;

// Handles mapping from remote ID to widget and vice versa

class bkRemoteWidgetMap
{
public:
	typedef inmap<u32, bkWidget, &bkWidget::m_RemoteToLocalNode> IdToWidgetMap;

	bkRemoteWidgetMap()
		: m_NextId(INVALID_ID + 1)
		, m_IdsLooped(false)
	{
	}

	enum {
		INVALID_ID = 0x0
	};

	// PURPOSE: Given a widget, return the corresponding widget ID
	// PARAMS: widget - A widget that has been previously registered
	// RETURNS: The widgets ID.
	u32 GetId(bkWidget& widget);

	// PURPOSE:
	//  Determines if the widget has a network ID
	// PARAMS:
	//	widget - The widget
	// RETURNS:
	//  true if the widget has an ID, otherwise false.
	bool WidgetHasId(bkWidget& widget);

	// PURPOSE: Given an ID, return the corresponding widget (or NULL)
	// PARAMS: id - The ID you want to search for
	// RETURNS: The widget with the corresponding ID, or NULL if no widget was found
	bkWidget* GetWidget(u32 id);

	// PURPOSE: Adds a widget to the map, assigns an unused ID to the widget
	// PARAMS: widget - a widget that doesn't currently have an ID
	// RETURNS: The new widget ID
	u32 AddWidget(bkWidget& widget);

	// PURPOSE: Adds a widget to the map with the specified ID
	// PARAMS:
	//	widget - A widget that doesn't currently have an ID.
	//  id - An id that isn't being used by a widget
	void AddWidgetAndId(bkWidget& widget, u32 id);

	// PURPOSE: Removes a widget from the map (removes its ID)
	// PARAMS:
	//	widget - A widget that was previously in the map
	void RemoveWidget(bkWidget& widget);

	// PURPOSE: Finds an unused ID.
	// NOTES: This is typically O(1) (it just increments the ID counter).
	//  Once the counter overflows (after assigning 2^32 ids), we have to search for an unused id.
	u32 FindNextId();

protected:
	IdToWidgetMap	m_RemoteToLocal;
	u32				m_NextId;
	bool			m_IdsLooped;
    
    sysCriticalSectionToken m_csToken;
};

#define DISABLE_WIDGETS_FOR_CONTENT_CONTROLLED_BUILD 0

//
// PURPOSE:
//	This class handles the communication of widgets on a local program
//  with widgets in a remote program.  The class itself has a packet
//  that can be written and read by the local and remote programs.  The
//  packet usually handles creation, updates, and destruction of widgets.
//
class bkRemotePacket : public sysPipePacket {
private:
	static bool sm_IsServer;
	static bool sm_IsConnectedToRag;

	struct Type {
		int m_Guid;
		void (*m_Handler)(const bkRemotePacket&);
		bool operator==(const Type &that) const { return m_Guid == that.m_Guid; }
	};

	static atFixedArray<Type,45> sm_Types;
//	static atMap<bkWidget*,bkWidget*> sm_RemoteToLocal;
//	static atMap<bkWidget*,bkWidget*> sm_LocalToRemote;

	static bkRemoteWidgetMap	sm_IdToWidgetMap;

	static sysCompressionFactory* sm_pCompressionFactory;
    enum { MAX_WRITE_BUFFER_LENGTH = 8 * 1024 };
    static u8 sm_writeBuffer[MAX_WRITE_BUFFER_LENGTH];
    static int sm_writeBufferLength;

	static char sm_address[128];

    static sysCriticalSectionToken sm_csToken;

#if DISABLE_WIDGETS_FOR_CONTENT_CONTROLLED_BUILD 
	 bool AllowWidgetGroup(const char* WidgetName);
#endif
public:
	// PURPOSE: pipe packet constructor
	// PARAMS: pipe - a reference to the associated pipe
	bkRemotePacket();

	enum 
	{ 
		CREATE,		// message to create a widget 
		DESTROY,	// message to destroy a widget
		CHANGED,	// message that the widget or the value has changed
		FILL_COLOR, // message that the widget has changed colors
		READ_ONLY,	// message that the widget has changed read-only state
		USER		// user defined message
	};

	//
	// PURPOSE
	//	resets the current packet
	//
	void Begin() const; 
	
	//
	// PURPOSE
	//	prepare the packet for its payload
	// PARAMS
	//	command - the type of command
	//	guid - the guid of the class to communicate with
	//	widget - the widget this command is being issued for
	//
	void Begin(int command,int guid,bkWidget* widget);

	//
	// PURPOSE
    //  Override that puts the message in the buffer for later sending
    virtual void Send();

	// PURPOSE:
	//	Writes a widget ID to the packet. This ID must corresponed to an existing widget or be INVALID_ID
	void WriteWidgetId(u32 id);

	// PURPOSE:
	//  Reads a widget ID from the packet.
	u32 ReadWidgetId() const;

	// PURPOSE:
	//	Given a widget, write its ID to the packet.
	// PARAMS:
	//	widget - Pointer to a widget. 
	// NOTES: 
	//  widget may be NULL (in which case we write INVALID_ID), but may
	//  not point to an unregistered widget. Use SetWidgetId or SetNewWidgetId first to assign
	//  an ID to the widget.
	void WriteWidget(bkWidget* widget);

	// PURPOSE: 
	//	Reads a widget ID from the packet. If the widget ID is valid, converts it to a bkWidget pointer.
	// NOTES:
	//	If the widget ID is INVALID_ID or is an ID that doesn't correspond to any widget, 
	//  this returns NULL. Otherwise it casts the bkWidget to _Type. If the cast fails, it
	//  prints an error and returns NULL.
	//	If the downcast fails, we always end the packet.
	// PARAMS:
	//	endPacket - should we end the packet if the widget doesn't exist?
	template<typename _Type> _Type* ReadOptionalWidget(bool endPacket) const;

	// PURPOSE:
	//	Reads a widget ID from the packet, returns a pointer to the corresponding widget.
	// NOTES:
	// ReadWidget checks to make sure the widget exists and is the correct type.
	// If the widget does not exist, ENDS THE PACKET. Make sure not to read any more data from the
	// packet. it is bad.
	// Normal usage would be something like this:
	//		bkList* widget = packet.ReadWidget<bkList>();
	//		if (!widget) return;
	//		continue reading data...
	template<typename _Type> _Type* ReadWidget() const;

	//
	// PURPOSE
	//	Adds a widget type and the handler for messages for that widget type
	// PARAMS
	//	guid - the unique identifier for widget type
	//	handler - a pointer to the function that will handle the packet
	//
	static void AddType(int guid,void (*handler)(const bkRemotePacket&));
	// PURPOSE: clears out the list of the remote types
	static void ResetTypes();


	// PURPOSE:
	//  Associates this widget with the specified ID (so that future network messages
	//  can refer to the widget via that ID)
	// PARAMS:
	//	widget - A bkWidget that hasn't previously been given an ID
	//  id - An id that isn't currently in use by another widget
	static void SetWidgetId(bkWidget& widget, u32 id);

	// PURPOSE:
	//	Releases the widget ID when you no longer want to recieve messages for this widget.
	//	(e.g. the widget has been deleted).
	//	The ID will be recycled eventually.
	// PARAMS:
	//	widget - A bkWidget that was previously registered
	static void UnsetWidgetId(bkWidget& widget);

	// PURPOSE:
	//	Sets the network widget ID for this widget to an unused ID.
	// PARAMS:
	//	widget - The widget
	static u32 SetNewWidgetId(bkWidget& widget);

	// PURPOSE:
	//  Determines if the widget has a network ID
	// PARAMS:
	//	widget - The widget
	// RETURNS:
	//  true if the widget has an ID, otherwise false.
	static bool WidgetHasId(bkWidget& widget);

	// PURPOSE: accessor to see if this program is running a server
	// RETURNS: true if this program is running a server, false if not
	static bool IsServer();
	
	// PURPOSE: accessor to see if this program and the remote program are connected
	// RETURNS: true if the programs are connected, false if not
	static bool IsConnected();

	enum {RAG_STRING_LEN=256,MAX_RAGE_APPS=10};
	typedef char TypeRagString[RAG_STRING_LEN];

	// PURPOSE: Initialize the compression factory and its Compression Data types
	static void Init();

	//
	// PURPOSE
	//  connects to the RAG application
	// PARAMS
	//  viewerApp - true if this application is just a viewer and not the main app
	// RETURNS
	//	returns true if the connection was successful, false if not
	// NOTES
	//	- must be called on the remote client
	static bool ConnectToRag(bool viewerApp);

	// PURPOSE: accessor to check if we're connected to RAG
	// RETURNS: returns true if connected to RAG, false if not
	static bool IsConnectedToRag();

	// PURPOSE: port offsets from Rag's base port number.  
	// NOTES:
	//  Mirrors PipeID.EnumSocketOffset in rage/base/tools/ragCore/PipeID.cs
	//  Access additional offsets with SOCKET_OFFSET_USER5+1 through SOCKET_OFFSET_USER5+40.
	enum EnumSocketOffset
	{
		SOCKET_OFFSET_BANK,		// offset off of base port for bank communication
		SOCKET_OFFSET_OUTPUT,	// offset off of base port for bank communication
		SOCKET_OFFSET_EVENTS,	// offset off of base port for bank communication
		SOCKET_OFFSET_PROFILE,
		SOCKET_OFFSET_USER0,	// offset off of base port for user socket
		SOCKET_OFFSET_USER1,	// offset off of base port for user socket
		SOCKET_OFFSET_USER2,	// offset off of base port for user socket
		SOCKET_OFFSET_USER3,	// offset off of base port for user socket
		SOCKET_OFFSET_USER4,	// offset off of base port for user socket
		SOCKET_OFFSET_USER5,	// offset off of base port for user socket
	};

	static const int SOCKET_HANDSHAKE = 2000;

	//
	// PURPOSE
	//	returns the port 
	// PARAMS
	//	offset - port numerical offset
	// RETURNS
	//  the computed socket port based on the passed in offset
	//
	static int GetRagSocketPort( int offset );

	//
	// PURPOSE
	//   returns the IP address of the Rag instance that this game is connected to
	// RETURNS
	//  the IP address.  The default is fiDeviceTcpIp::GetLocalHost().
	//
	static const char* GetRagSocketAddress();

	//
	// PURPOSE
	//  sets the IP address of the Rag instance that this game should be connected to
	// PARAMS
	//  addr - the IP address.  The default is fiDeviceTcpIp::GetLocalHost().
	static void SetRagSocketAddress( const char* addr );

	//
	// PURPOSE
	//  connects the remote client with the remote server
	// PARAMS
	//	waitForPipe - a reference to the local widget
	//	pipeName - a reference to the local widget
	// RETURNS
	//	returns true if the connection was successful, false if not
	// NOTES: 
	//	must be called on the remote client
	//
	static bool Connect(bool waitForPipe=false,const char* pipeName=NULL);
	// PURPOSE: close the connection with the remote program
	static void Close();
	// PURPOSE: 
	//   close the connection with the remote program without regard to thread safety
	//   does not do any cleanup - this is only meant to be used when the game has crashed
	//   anyway and as such is in a bad state anyway.
	static void ForceCloseConnection();
	
	// PURPOSE: waits for the remote client to connect
	// NOTES: must be called on the widget server
	static void Listen();

    // PURPOSE: reads from the remote program
    // NOTES: must be called on the widget server, and should be called at least once a frame
    static void ReceivePackets();
    // PURPOSE: sends buffered messages to the remote program
    // NOTES: must be called on the widget server, and should be called at least once a frame
    static void SendPackets();

	// PURPOSE: Prints the command line retrieved from sysParam if PARAM_noPrintCommandLine was not set
	static void PrintCommandLine();

	// PURPOSE: Wrappers for the Write_type() functions in sysPipePacket
	// NOTES:
	//	Uses these in templates where you don't know what type of data you'll be writing
	void WriteValue(bool data); 
	void WriteValue(u8 data); 
	void WriteValue(s8 data);
	void WriteValue(u16 data);
	void WriteValue(s16 data);
	void WriteValue(u32 data);
	void WriteValue(s32 data);
	void WriteValue(float data);

	// PURPOSE: Wrappers for the Read_type() functions in sysPipePacket
	// NOTES:
	//	Uses these in templates where you don't know what type of data you'll be reading
	void ReadValue(bool& data) const; 
	void ReadValue(u8& data) const; 
	void ReadValue(s8& data) const; 
	void ReadValue(u16& data) const;
	void ReadValue(s16& data) const;
	void ReadValue(u32& data) const;
	void ReadValue(s32& data) const;
	void ReadValue(float& data) const;
};

inline void bkRemotePacket::ResetTypes()
{
	sm_Types.Reset();
}

inline bool bkRemotePacket::IsServer() 
{ 
	return sm_IsServer; 
}

inline bool bkRemotePacket::IsConnectedToRag()
{
	return sm_IsConnectedToRag;
}

inline void bkRemotePacket::Begin() const
{
	sysPipePacket::Begin();
}

inline void bkRemotePacket::Begin(int command,int guid,bkWidget* widget)
{
	u32 id = (u32)bkRemoteWidgetMap::INVALID_ID;
	if (widget)
	{
		id = sm_IdToWidgetMap.GetId(*widget);
	}
	FastAssert(id != bkRemoteWidgetMap::INVALID_ID);

	sysPipePacket::Begin(command, guid, id);
}

inline void bkRemotePacket::WriteValue(bool data) {
	Write_bool(data);
}

inline void bkRemotePacket::WriteValue(u8 data) {
	Write_u8(data);
}

inline void bkRemotePacket::WriteValue(s8 data) {
	Write_s8(data);
}

inline void bkRemotePacket::WriteValue(u16 data) {
	Write_u16(data);
}

inline void bkRemotePacket::WriteValue(s16 data) {
	Write_s16(data);
}

inline void bkRemotePacket::WriteValue(u32 data) {
	Write_u32(data);
}

inline void bkRemotePacket::WriteValue(s32 data) {
	Write_s32(data);
}

inline void bkRemotePacket::WriteValue(float data) {
	Write_float(data);
}

inline void bkRemotePacket::ReadValue(bool& data) const {
	data = Read_bool();
}

inline void bkRemotePacket::ReadValue(u8& data) const {
	data = Read_u8();
}

inline void bkRemotePacket::ReadValue(s8& data) const {
	data = Read_s8();
}

inline void bkRemotePacket::ReadValue(u16& data) const {
	data = Read_u16();
}

inline void bkRemotePacket::ReadValue(s16& data) const {
	data = Read_s16();
}

inline void bkRemotePacket::ReadValue(u32& data) const {
	data = Read_u32();
}

inline void bkRemotePacket::ReadValue(s32& data) const {
	data = Read_s32();
}

inline void bkRemotePacket::ReadValue(float& data) const {
	data = Read_float();
}

inline void bkRemotePacket::WriteWidget(bkWidget* widget)
{
	if (widget)
	{
		u32 id = sm_IdToWidgetMap.GetId(*widget);
		FastAssert(id != bkRemoteWidgetMap::INVALID_ID);
		WriteWidgetId(id);
	}
	else
	{
		Write_u32((u32)bkRemoteWidgetMap::INVALID_ID);
	}
}

inline void bkRemotePacket::WriteWidgetId(u32 id)
{
	FastAssert(id == bkRemoteWidgetMap::INVALID_ID || sm_IdToWidgetMap.GetWidget(id) != NULL); // ID must point to valid widget
	Write_u32(id);
}

template<typename _Type>
inline _Type* bkRemotePacket::ReadOptionalWidget(bool endPacket) const
{
	u32 id = GetId();
	bkWidget* widget = sm_IdToWidgetMap.GetWidget(id);
	if (!widget)
	{
		if (endPacket)
		{
			m_CurrentOffset = m_Length;
			End();
		}
		return NULL;
	}

	_Type* ret = dynamic_cast<_Type*>(widget);
	if (!ret)
	{
		bkErrorf("Widget is incorrect type");
		m_CurrentOffset = m_Length;
		End(); // end the packet. It must be bad. Reading more from it would be an error
		return NULL;
	}
	return ret;
}

template<typename _Type>
inline _Type* bkRemotePacket::ReadWidget() const
{
	u32 id = GetId();

	bkWidget* widget = sm_IdToWidgetMap.GetWidget(id);
	if (!widget)
	{
		bkErrorf("ID %d doesn't correspond to any known widget", id);
		m_CurrentOffset = m_Length;
		End(); // end the packet. It must be bad. Reading more from it would be an error
		return NULL;
	}

	_Type* ret = dynamic_cast<_Type*>(widget);
	if (!ret)
	{
		bkErrorf("Widget is incorrect type");
		m_CurrentOffset = m_Length;
		End(); // end the packet. It must be bad. Reading more from it would be an error
		return NULL;
	}
	return ret;
}

inline u32 bkRemotePacket::ReadWidgetId() const 
{
	u32 id = Read_u32();
	return id;
}

inline void bkRemotePacket::SetWidgetId(bkWidget& widget, u32 id)
{
	sm_IdToWidgetMap.AddWidgetAndId(widget, id);
}

inline void bkRemotePacket::UnsetWidgetId(bkWidget& widget)
{
	sm_IdToWidgetMap.RemoveWidget(widget);
}

inline u32 bkRemotePacket::SetNewWidgetId(bkWidget& widget)
{
	return sm_IdToWidgetMap.AddWidget(widget);
}

inline bool bkRemotePacket::WidgetHasId(bkWidget& widget)
{
	return sm_IdToWidgetMap.WidgetHasId( widget );
}

}	// namespace rage

#endif	// __DEV | __BANK

#endif
