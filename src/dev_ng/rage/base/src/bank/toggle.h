//
// bank/toggle.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_TOGGLE_H
#define BANK_TOGGLE_H

#if __BANK && !__SPU

#include "packet.h"
#include "widget.h"

#include "atl/bitset.h"

namespace rage {

class bkToggle: public bkWidget {
protected:
	bkToggle(datCallback &callback,const char *title,const char *memo, const char *fillColor=NULL, bool readOnly=false);
	~bkToggle();

public:
	virtual bool GetBool() const = 0;
	virtual void SetBool(bool flag) = 0;

    // PURPOSE: Retrieves a string representation of the current value of this widget.
    // PARAMS:
    //    buf - the buffer to write the string representation to.
    //    bufLen - the length of the buffer.
    virtual void GetStringRepr( char *buf, int bufLen );

    // PURPOSE: Sets the current value of this widget using the string representation of the new value.
    // PARAMS:
    //    buf - the new value.
    virtual void SetStringRepr( const char *buf );

#if __WIN32PC
protected:
	void WindowCreate();
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM);
	void WindowUpdate();
#endif
};


// ToggleTraits is a helper type to map the normal _Type to the _Masktype
// (for example atBitSet uses a u8 mask type)
template<typename _Type>
struct ToggleTraits
{
	typedef _Type	MaskType;
};

template<>
struct ToggleTraits<atBitSet> {
	typedef u8		MaskType;
};

template<typename _Type>
class bkToggleVal: public bkToggle {
	friend class bkGroup;
	friend class bkManager;
protected:
	typedef typename ToggleTraits<_Type>::MaskType _Masktype;

	bkToggleVal(datCallback &callback,const char *title,const char *memo,_Type *data,_Masktype mask, const char* fillColor=NULL, bool readOnly=false);

	~bkToggleVal();
	void Message(Action action,float);
	int DrawLocal(int x,int y);

public:
	bool GetBool() const;
	void SetBool(bool);

protected:
	static inline int GetStaticGuid();
	int GetGuid() const;

	void Update();
	static void RemoteHandler(const bkRemotePacket& packet);

private:
	void RemoteCreate();
	void RemoteUpdate();
	_Type *m_Value;
	_Masktype m_Mask;
	bool m_PrevValue;
};

template<typename _Type> bkToggleVal<_Type>::bkToggleVal(datCallback &callback,const char *title,const char *memo,_Type *data,_Masktype mask, const char* fillColor, bool readOnly)
: bkToggle(callback,title,memo,fillColor,readOnly)
, m_Value(data)
, m_Mask(mask) 
{ 
	m_PrevValue = GetBool();
}

template<typename _Type> bkToggleVal<_Type>::~bkToggleVal()
{
	if (bkRemotePacket::IsServer()) 
		delete m_Value;
}

template<typename _Type> void bkToggleVal<_Type>::Message(Action action,float)
{
	if (action == DIAL) { 
		SetBool(!GetBool());
	}
}
template<typename _Type> int bkToggleVal<_Type>::DrawLocal(int x,int y)
{
	Drawf(x,y,"%s [%c]",m_Title,GetBool()?'X':' ');
	return bkWidget::DrawLocal(x,y);
}

template<typename _Type> int bkToggleVal<_Type>::GetGuid() const 
{
	return GetStaticGuid();
}

template<typename _Type> void bkToggleVal<_Type>::Update() {
	bool newValue = GetBool(); 
	if (newValue != m_PrevValue) { 
		m_PrevValue = newValue; 
		FinishUpdate();
	}
}

template<typename _Type> void bkToggleVal<_Type>::RemoteHandler(const bkRemotePacket& packet)
{
	if (packet.GetCommand() == bkRemotePacket::CREATE) {
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;
		const char *title = packet.Read_const_char(), *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();
		_Type *data = rage_new _Type;
		*data = packet.Read_bool();
		_Masktype mask;
		packet.ReadValue(mask);
		packet.End();
		bkToggleVal<_Type> &widget = *(rage_new bkToggleVal<_Type>(NullCB,title,memo,data,mask,fillColor,readOnly));
		bkRemotePacket::SetWidgetId(widget, id);
		parent->AddChild(widget);
	}
	else if (packet.GetCommand() == bkRemotePacket::CHANGED) {
		packet.Begin();
		bkToggleVal<_Type>* widget = packet.ReadWidget<bkToggleVal<_Type> >();
		if (!widget) return;
		bool value = packet.Read_bool();
		widget->SetBool(value); 
		widget->m_PrevValue = widget->GetBool();
		packet.End();
		widget->WindowUpdate();
	} 
}

template<typename _Type> void bkToggleVal<_Type>::RemoteCreate()
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetStaticGuid(),this);
	p.WriteWidget(m_Parent);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char( GetFillColor() );
	p.Write_bool(IsReadOnly());
	p.Write_bool(GetBool());
	p.WriteValue(m_Mask);
	p.Send();
}

template<typename _Type> void bkToggleVal<_Type>::RemoteUpdate()
{
	bkRemotePacket p;
	p.Begin(bkRemotePacket::CHANGED,GetStaticGuid(),this);
	p.Write_bool(GetBool());
	p.Send();
}


template<> inline int bkToggleVal<bool>		::GetStaticGuid() { return BKGUID('t','g','b','o'); }
template<> inline int bkToggleVal<s32>		::GetStaticGuid() { return BKGUID('t','g','s','3'); }
template<> inline int bkToggleVal<u8>		::GetStaticGuid() { return BKGUID('t','g','u','8'); }
template<> inline int bkToggleVal<u16>		::GetStaticGuid() { return BKGUID('t','g','u','1'); }
template<> inline int bkToggleVal<u32>		::GetStaticGuid() { return BKGUID('t','g','u','3'); }
template<> inline int bkToggleVal<atBitSet>	::GetStaticGuid() { return BKGUID('t','g','b','s'); }
template<> inline int bkToggleVal<float>	::GetStaticGuid() { return BKGUID('t','g','f','l'); }

template<> inline bool bkToggleVal<bool>	::GetBool() const { return *m_Value; }
template<> inline bool bkToggleVal<s32>		::GetBool() const { return *m_Value != 0; }
template<> inline bool bkToggleVal<u8>		::GetBool() const { return (*m_Value & m_Mask) != 0; }
template<> inline bool bkToggleVal<u16>		::GetBool() const { return (*m_Value & m_Mask) != 0; }
template<> inline bool bkToggleVal<u32>		::GetBool() const { return (*m_Value & m_Mask) != 0; }
template<> inline bool bkToggleVal<atBitSet>::GetBool() const { return m_Value->IsSet(m_Mask); }
template<> inline bool bkToggleVal<float>	::GetBool() const { return *m_Value > 0.5f; }

template<> inline void bkToggleVal<bool>	::SetBool(bool flag) { *m_Value = flag; Changed(); }
template<> inline void bkToggleVal<s32>		::SetBool(bool flag) { *m_Value = flag; Changed(); }
template<> inline void bkToggleVal<u8>		::SetBool(bool flag) { *m_Value = (u8) (flag? (*m_Value | m_Mask) : (*m_Value & ~m_Mask)); Changed(); }
template<> inline void bkToggleVal<u16>		::SetBool(bool flag) { *m_Value = (u16) (flag? (*m_Value | m_Mask) : (*m_Value & ~m_Mask)); Changed(); }
template<> inline void bkToggleVal<u32>		::SetBool(bool flag) { *m_Value = (u32) (flag? (*m_Value | m_Mask) : (*m_Value & ~m_Mask)); Changed(); }
template<> inline void bkToggleVal<atBitSet>::SetBool(bool flag) { m_Value->Set(m_Mask,flag); Changed(); }
template<> inline void bkToggleVal<float>	::SetBool(bool flag) { *m_Value = (float)flag; Changed(); }


// Since this is a template, gotta put the whole thing here separately... :(

// NOTE: To get remote communication to work we need an assymetric RemoteCreate. This is because
// the program doing the creating might have different template instantiations for <size> than
// the program recieving the message. So what we really need to do is create a different type
// on the message recieving end that doesn't depend on 'size'. Should be easy enough to do, needs
// to look just like bkToggleVal<bool> but send messages in the bkToggleVal_atFixedBitSet format.
template<int size> class bkToggleVal_atFixedBitSet : public bkToggle {
	friend class bkGroup;
protected:
	bkToggleVal_atFixedBitSet(datCallback &callback, const char *title, const char *memo, atFixedBitSet<size> *data, u32 mask, const char* fillColor=NULL, bool readOnly=false) :
		bkToggle(callback, title, memo, fillColor, readOnly), m_Value(data), m_Mask(mask) { m_PrevValue = GetBool(); }
	~bkToggleVal_atFixedBitSet() { if ( bkRemotePacket::IsServer() ) delete m_Value; }
	void Message(Action action, float /*value*/ ) {
		if ( action == DIAL ) { SetBool(!GetBool()); }
	}
	int DrawLocal(int x,int y) { 
		Drawf(x,y,"%s [%c]",m_Title,GetBool()?'X':' ');
		return bkWidget::DrawLocal(x,y);
	}
	int GetGuid() const { return GetStaticGuid(); }
	void Update() { bool newValue = GetBool(); if (newValue != m_PrevValue) { m_PrevValue = newValue; FinishUpdate(); } }
	static void RemoteHandler(const bkRemotePacket& packet) { 
		bkErrorf("Remote atFixedBitset widgets are not supported");
#if 0
		if (packet.GetCommand() == bkRemotePacket::CREATE) {
			packet.Begin(); 
			bkToggle_atFixedBitSet *remote = (bkToggle_atFixedBitSet*) packet.Read_bkWidget(); 
			FastAssert(remote);
			bkWidget *remoteParent = packet.Read_bkWidget(); 
			FastAssert(remoteParent);
			const char *title = packet.Read_const_char(), *memo = packet.Read_const_char(); 
			const char *fillColor = packet.Read_const_char();
			bool readOnly = packet.Read_bool();
			atFixedBitSet<size> *data = rage_new atFixedBitSet<size>; 
			*data = packet.Read_bool(); 
			u32 mask = packet.Read_u32(); 
			packet.End(); 
			bkToggle_atFixedBitSet& local = *(rage_new bkToggle_atFixedBitSet(NullCB,title,memo,data,mask,fillColor,readOnly)); 
			bkRemotePacket::Associate(*remote,local); 
			bkRemotePacket::RemoteToLocal(*remoteParent)->AddChild(local); 
		} 
		else if (packet.GetCommand() == bkRemotePacket::CHANGED) { 
			packet.Begin(); 
			bkWidget* remoteWidget=packet.Read_bkWidget();
			FastAssert(remoteWidget);
			bkToggle_atFixedBitSet *local = (bkToggle_atFixedBitSet*) bkRemotePacket::RemoteToLocal(*remoteWidget); 
			bool value = packet.Read_bool(); 
			local->SetBool(value);
            local->m_PrevValue = local->GetBool(); 
			packet.End(); 
			local->WindowUpdate();
		} 
#endif
	} 
	
    // Explicit overrides
public:
	bool GetBool() const { return m_Value->IsSet(m_Mask); }
	void SetBool(bool val) { m_Value->Set(m_Mask, val); Changed(); }

protected:
	static inline int GetStaticGuid() { return BKGUID('n', 'u', 'l', 'l'); }

private: 
	void RemoteCreate() {
		// atFixedBitset widgets are not supported
		bkWarningf("Can't create remote widgets for atFixedBitset toggles. Widget is '%s'", m_Title);
#if 0
		bkWidget::RemoteCreate();

		bkRemotePacket p; 
		p.Begin(bkRemotePacket::CREATE,GetStaticGuid()); 
		p.Write_bkWidget(m_Parent); 
		p.Write_const_char(m_Title);
		p.Write_const_char(m_Tooltip); 
		p.Write_const_char( m_FillColor );
		p.Write_bool(IsReadOnly());
		p.Write_bool(GetBool()); 
		p.Write_u32(m_Mask); 
		p.Send(); 
#endif
	}
	void RemoteUpdate() { 
		// atFixedBitset widgets are not supported
#if 0
		if (!bkRemotePacket::IsConnected()) return;
		bkRemotePacket p;
		p.Begin(bkRemotePacket::CHANGED,GetStaticGuid());
		bkWidget* localWidget=bkRemotePacket::LocalToRemote(*this);
		FastAssert(localWidget);
		p.Write_bool(GetBool());
		p.Send();
#endif
	}
	atFixedBitSet<size> *m_Value; 
	u32 m_Mask;
	bool m_PrevValue;

};

}	// namespace rage

#endif

#endif
