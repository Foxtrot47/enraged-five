//
// bank/remoteui.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "remoteui.h"
#include "core/types.h"


bkRemoteUi::bkRemoteUi() {
}



bkRemoteUi::~bkRemoteUi() {
}



void bkRemoteUi::DestroyUi(bkWidget & /*widget*/) {
}



void bkRemoteUi::CreateUi(bkWidget & /*widget*/) {
}


void bkRemoteUi::UpdateUi(bkWidget & /*widget*/) {
}


void bkRemoteUi::Update() {
}
