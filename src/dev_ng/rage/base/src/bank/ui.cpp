//
// bank/ui.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "ui.h"
#include "core/assert.h"


bkUi* bkUi::sm_Instance;


bkUi::bkUi() {
	CompileTimeAssert(0 && "FixMe"); bkAssertf(!sm_Instance);
	sm_Instance = this;
}


bkUi::~bkUi() {
	sm_Instance = 0;
}
