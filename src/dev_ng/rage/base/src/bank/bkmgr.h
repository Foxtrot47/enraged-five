//
// bank/bkmgr.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_BKMGR_H
#define BANK_BKMGR_H

#if __BANK && !__SPU

#include "group.h"
#include "atl/delegate.h"
#include "system/criticalsection.h"
#include "system/messagequeue.h"

namespace rage {

class bkBank;

struct bkInvokeDelegate {
	typedef atDelegate<void (size_t)> DelType;
	bkInvokeDelegate() : m_UserData(0) {}
	bkInvokeDelegate(DelType del, size_t udata) : m_Delegate(del), m_UserData(udata) {}

	DelType m_Delegate;
	size_t m_UserData;
};

//
// PURPOSE
//	This class is the singleton manager for all widget banks within the application.
//  <FLAG Component>
//
class bkManager: public bkGroup {
	friend class IgnoreGccWarning;
	friend class bkBank;
	
	bkManager(const char *appName);
	~bkManager();

public:
	//
	// PURPOSE
	//	Creates a bank.
	// PARAMS
	//	name - the name of the bank
	//	x - the starting x position of the bank
	//	y - the starting y position of the bank
	//	addToMasterBank - true if this bank should be visible in the master bank, false if it shouldn't
	// RETURNS
	//	returns a reference to the created bank
	//
	bkBank &CreateBank(const char *name,int x = 100,int y = 100,bool addToMasterBank = true);
	//
	// PURPOSE
	//	Register a bank that may be created later.
	// PARAMS
	//	name - the name of the bank.
	//	creator - the function that will be called when the bank needs to be created
	//	destructor - the function that will be called when the bank needs to be destroyed
	// RETURNS
	//	returns a reference to the created bank
	//
	bkBank &RegisterBank(const char *name,datCallback creator,datCallback destructor = NullCB);

	//
	// PURPOSE
	//	returns if a pointer to the pane so it's position can be changed later
	// RETURNS
	//	returns a pointer to a bkPane object
	//
	bkPane *GetPane();

	//
	// PURPOSE
	//	This function displays a dialog box that will allow the user to choose a file name.
	// PARAMS
	//  dest - a pointer to a string to save the requested file to
	//  maxDest - the maximum # of characters that maybe placed in dest
	//	mask - the type of files that we want to filter for the user (example: "*.mp3")
	//	save - should we display a save file dialog box instead of an open dialog box?
	//	desc - the description of the types of file
	//	registryPrefix - the registryPrefix to save the user selected directory to
	// RETURNS
	//	returns a pointer to the name of the file selected by the user (can return NULL)
	//
	const char*	OpenFile(const char *mask = NULL,bool save=false,const char *desc = NULL,const char* registryPrefix=NULL);
	// <COMBINE OpenFile@const char *@bool@const char *@const char *>
	bool		OpenFile(char *dest,int maxDest,const char *mask,bool save,const char *desc);
	//
	// PURPOSE
	//	Make all opened banks visible on top of all other windows 
	// PARAMS
	//	onTop - true if the the banks should be shown on top of everything else
	//
	void		RaiseAllBanks(bool onTop);
	// PURPOSE: Updates all banks.  Should be called at least once a frame.
	void		Update();
	// PURPOSE: Draws widgets on the game screen.  
	void		Draw();
	//
	// PURPOSE
	//	Get the next message under Windows.
	// PARAMS
	//	blockFlag - wait in this function until we get a new message
	// RETURNS
	//	returns FALSE if a WM_QUIT message was gotten, otherwise true is returned
	// NOTES
	//	Call this only if you don't have a message loop (i.e. not using input or gfx)
	//
	bool		GetNextMessage(bool blockFlag);

	//
	// PURPOSE
	//  find a named bank in this manager
	// PARAMS
	//	name - the name of the bank to find
	// RETURNS
	//	returns a pointer to the bank if it is found
	//
	bkBank* FindBank(const char *name) const;

    //
    // PURPOSE
    //  find a named widget in this manager
    // PARAMS
    //	path - the name and path of the widget to find
    // RETURNS
    //	returns a pointer to the widget if it is found
    //
    bkWidget* FindWidget( const char *path ) const;

	//
	// PURPOSE
	//	returns if a controller is currently be used to interact with the on screen bank.
	// PARAMS
	//	padIndex - the index of the pad to check (if this is -1, we check if any pad is in use)
	// RETURNS
	//	true if the requested pad is being used
	//
	bool IsUsingPad(int padIndex = -1) const;
	//
	// PURPOSE
	//	activates the named bank
	// PARAMS
	//	bankName - the name of the bank to activate
	//
	void ActivateBank(const char *bankName) const;
	
	//
	// PURPOSE
	//	set the scroll multiplier and the delay time until the multiplier takes effect.
	// PARAMS
	//	mult - the scroll multiplier 
	//	time - the delay time until the scroll takes effect
	//
	static void SetScrollIncreaseDelay(float mult,float time);

	// PURPOSE: accessor to check if there's a bank manager enabled
	// RETURNS: returns true if bank manager has been allocated, false if not
	static bool IsEnabled();
	//
	// PURPOSE
	//	create the singleton bank manager.
	// PARAMS
	//	appName - the name of the app that is creating the bank manager
	//
	static void CreateBankManager(const char *appName);
	// PURPOSE: deletes the bank manager singleton.
	static void DeleteBankManager();
	//
	// PURPOSE
	//	destroys a bank.
	// PARAMS
	//	b - the bank to destroy
	//
	static void DestroyBank(bkBank &b);
	
	// PURPOSE: gets the singleton bank instance
	// RETURNS: returns a reference to the bank manager singleton
	static bkManager& GetInstance();

	// PURPOSE:
	//	Returns true when called on the bank's update thread
	static bool IsBankThread();

	// PURPOSE: queues up a callback to be executed by the bank thread during the next bank update
	// PARAMS:
	//		del - A delegate that calls a function with a signature like: "void fn(u32)"
	static void Invoke(bkInvokeDelegate::DelType del, size_t udata);

	//
	// PURPOSE
	//	The function is called when handling communication from a remote bank client or server.
	// PARAMS
	//	packet - the packet passed from the remote bank client or server
	//
	static void RemoteHandler(const bkRemotePacket& packet);
	// PURPOSE: accessor for the unique id for this class (used in remote communication)
	// RETURNS: returns the bank class' GUID
	static int GetStaticGuid();
	// <ALIAS GetStaticGuid>
	int GetGuid() const;

	//
	// PURPOSE
	//	sets if the bank manager is currently active
	// PARAMS
	//	flag - true if we set the bank manager active, false if not
	//
	static void SetActive(bool flag);
	// PURPOSE: returns if the bank manager is active
	// RETURNS: returns true if the bank manager, false if not
	static bool GetActive();

	bool IsClosedGroup() const;

	//
	// PURPOSE
	//	sets the name of the application
	// PARAMS
	//	appName - the name of the application
	//
	static void SetAppName(const char *appName);

	// PURPOSE: accessor for the application name
	// RETURNS: returns the name of the application
	static const char* GetAppName();

	// PURPOSE: accessor for bank manager quit message
	// RETURNS: return true if quit message was sent, false if not
	static bool GotQuitMessage();

	//
	// PURPOSE
	//	sends timer information to RAG.  This string is displayed
	//  in the lower left portion of the status bar.
	// PARAMS
	//	str - the string that contains the timer information
	//
	void SendTimerString(const char* str);

	//
	// PURPOSE
	//	sends memory information to RAG.  This string is displayed
	//  in the lower right portion of the status bar.
	// PARAMS
	//	str - the string that contains the memory information
	//
	void SendMemoryString(const char* str);

#if __WIN32PC
	// PURPOSE: accessor for the render window handle.
	// RETURNS: the pointer to the render window handle
	// NOTES: This function is available on PC versions only.
	static struct HWND__* GetRenderWindow();
#endif

	//
	// PURPOSE
	//	Copies the passed in text to the Windows clipboard
	// PARAMS
	//	text - the text to be copied
	// NOTES
	//	The copy to clipboard will only work if RAG is running.
	//
	void CopyTextToClipboard(const char* text);

	// PURPOSE: Clear the Windows clipboard
	// NOTES: The clipboard clear will only work if RAG is running.
	void ClearClipboard();

	//
	// PURPOSE
	//  Creates a new message output window in Rag.  bkDisplayf and Printf
	//  messages that are prefixed with "[name] " will appear in the Output Window
	//  and in a new message window called "name".
	// PARAMS
	//  name - The name of the window and the prefix
	//  color - A specially formatted string representing the desired color of the text 
	//   in the window.  If this is an empty string, NULL, or "ARGBColor:0:0:0:0", the
	//   default system color for WindowText is used.
	//   "NamedColor:nameOfColor" can be used to colorize with a named Windows System color
	//   "ARGBColor:x:x:x:x" can be used to colorize with an ARGB color, each value ranging from 0 to 255
	// NOTES 
	//  A usage example:
	//  BANKMGR.CreateOutputWindow( "test1", "NamedColor:Blue" );
	//  BANKMGR.CreateOutputWindow( "test2", "NamedColor:Goldenrod" );
	//  BANKMGR.CreateOutputWindow( "test3", "ARGBColor:0:0:255:0" );	// green.
	//  BANKMGR.CreateOutputWindow( "test4", "ARGBColor:0:128:128:128" ); // gray
	//  bkDisplayf( "[test1] This message is displayed in the created window 'test1'." );
	//  bkDisplayf( "This message is not." );
	//
	void CreateOutputWindow( const char* name, const char* color );

	// PURPOSE: Called to inform RAG about the channel hierarchy.
	void SendSubChannelInfo( const char* channel, const char* subChannel );

	// PURPOSE: Execute the RemoteCreate() function for myself and any child widgets.  Typically
	//  called when a game starts without Rag, then selects the text widget "Start Rag".
	void Rebuild();

protected:    
	void RemoteCreate();
	void RemoteUpdate();

    // PURPOSE: Locates the widget using the specified path
    // PARAMS:
    //    widget - the widget to compare, including its children and siblings (this should never be a bkBank)
    //    path - the path to search (not including the name of the bkBank)
    // RETURNS: NULL if not found
    bkWidget* FindWidget( bkWidget *widget, const char *path ) const;

#if __WIN32PC
	static struct HWND__* GetOwner();
	void WindowCreate();
	void WindowDestroy();
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM);
	int WindowResize(int x,int y,int width,int height);
	
	bkPane *m_Pane;
	static struct HWND__* sm_Owner;
#endif

private:
	static bkManager *sm_Instance;
	static bool sm_initFinished;
	static bool sm_Active;
	static bool sm_Quit;
	static float sm_ScrollMultiplier;
	static float sm_ScrollTimeLeft;
	static float sm_ScrollTimeRight;
	static float sm_ScrollTimer;
	static bool sm_SendDelayedPingResponse;
	static u32 sm_PingResponseMsg;

#if __WIN32PC
	static HWND__* sm_RenderWindow;
#endif
    static sysCriticalSectionToken sm_csToken;
};

// convenience macro for accessing the bank manager instance
#define BANKMGR		::rage::bkManager::GetInstance()

inline void bkManager::SetScrollIncreaseDelay(float mult,float time) 
{
	sm_ScrollMultiplier=mult; 
	sm_ScrollTimer=time;
}

inline bool bkManager::IsEnabled() 
{ 
	return sm_Instance != 0; 
}

inline bkManager& bkManager::GetInstance() 
{ 
	FastAssert(sm_Instance);
	return *sm_Instance; 
}

inline int bkManager::GetStaticGuid() 
{ 
	return BKGUID('b','m','g','r'); 
}

inline void bkManager::SetActive(bool flag) 
{ 
	sm_Active = flag; 
}

inline bool bkManager::GetActive() 
{ 
	return sm_Active; 
}

inline bool bkManager::GotQuitMessage()
{
	return sm_Quit;
}

#if __WIN32PC
inline struct HWND__* bkManager::GetOwner() 
{ 
	return sm_Owner; 
}

inline struct HWND__* bkManager::GetRenderWindow()
{
	return sm_RenderWindow;
}
#endif

}	// namespace rage

#endif

#endif
