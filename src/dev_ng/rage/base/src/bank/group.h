//
// bank/group.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_GROUP_H
#define BANK_GROUP_H

#if __BANK && !__SPU

#include "toggle.h"
#include "widget.h"

namespace rage {

class Color32;
class Float16;
class Matrix33;
class Matrix34;
class Matrix44;
class Vector2;
class Vector3;
class Vector4;
class ScalarV;
class Vec2V;
class Vec3V;
class Vec4V;
class Mat34V;
class Mat44V;
class atBitSet;
class atNonFinalHashString;
class atFinalHashString;
template <int _T, typename _SU> class atFixedBitSet;
class bkAngle;
class bkButton;
class bkColor;
class bkCombo;
class bkData;
class bkImageViewer;
class bkList;
class bkMatrix33;
class bkMatrix34;
class bkMatrix44;
class bkSeparator;
class bkSlider;
class bkText;
class bkTitle;
class bkToggle;
class bkTreeList;
class bkVector2;
class bkVector3;
class bkVector4;

//
// PURPOSE
//	A bkGroup is a collection of widgets.  Several groups can 
//  be within a bank, and their contents can be visible or hidden.
//
class bkGroup: public bkWidget {
	friend class bkBank;

public:
	bool AreAnyWidgetsShown() const;

	//
	// PURPOSE
	//	Removes the passed in widget from the bank and destroys the widget.
	// PARAMS
	//	control - the widget to remove
	//
	void Remove(bkWidget& control);

	//
	// PURPOSE
	//  Adds a new widget group as a child of this group
	// PARAMS
	//	title - the displayed title of the group
	//	startOpen - should the group be started already expanded?
	//	memo - documentation for describing the group in finer detail
	//  color - A specially formatted string representing the desired color of the group.
	//   If this is an empty string, NULL, or "ARGBColor:0:0:0:0", the default system color 
	//   for Control is used.  
	//   "NamedColor:nameOfColor" can be used to colorize with a named Windows System color.
	//   "ARGBColor:x:x:x:x" can be used to colorize with an ARGB color, each value ranging from 0 to 255.
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created group (can be NULL)
	//
	bkGroup*	AddGroup(const char* title,bool startOpen=true,const char *memo=NULL,const char* fillColor=NULL, bool readOnly=false);

	//
	// PURPOSE
	//  Adds a widget to the bank that separates widgets without grouping them
	// PARAMS
	//  title - the name of the separator.  will not be displayed.
	//  fillColor - the color of the widget
	//
	bkSeparator* AddSeparator( const char *title="", const char* fillColor=NULL );

	//
	// PURPOSE
	//	Adds a widget to the bank that displays a static piece of text.
	// PARAMS
	//	fmt - the formatted string to display
	//  ... - the arguments to the formatted string
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkTitle* AddTitle( const char *fmt, ... );

	//
	// PURPOSE
	//	Adds a widget to the bank that displays a static piece of text.
	// PARAMS
	//	b - the value to display as a string
	//  fillColor - the arguments to the formatted string
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkTitle* AddTitle( bool b ,const char* fillColor=NULL );
	bkTitle* AddTitle( float f, const char* fillColor=NULL );
	bkTitle* AddTitle( int i, const char* fillColor=NULL );

	//
	// PURPOSE
	//	Adds a push button widget to the bank.
	// PARAMS
	//	title - the string displayed in the button
	//	callback - the callback to call when the button has been pushed
	//	tooltip - the tooltip documetation to display
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkButton* AddButton(const char *title,datCallback callback,const char *tooltip ,const char* fillColor=NULL, bool readOnly=false);
	bkButton* AddButton(const char *title,datCallback callback);
	bkButton* AddButton(const char *title, Static0 callback);

	//
	// PURPOSE
	//	Adds a text edit widget to the bank.
	// PARAMS
	//	title - the string that describes what text is being edited
	//	str - a pointer to the data that will be edited
	//	len - the maximum length that the string can be 
	//	readOnly - can the string actually be edited?
	//	callback - the callback to call whenever the text is changed
	//  fillColor - the color of the widget
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkText* AddText(const char *title,char *str,unsigned int len,bool readOnly,datCallback callback,const char* fillColor=NULL);
	bkText* AddText(const char *title,char *str,unsigned int len,bool readOnly);
	bkText* AddROText(const char *title,const char *str,unsigned int len) { return AddText(title,const_cast<char*>(str),len,true); }

	bkText* AddText(const char *title,bool *b,bool readOnly,datCallback callback,const char* fillColor=NULL);
	bkText* AddText(const char *title,bool *b,bool readOnly);

	bkText* AddText(const char *title,float *f,bool readOnly,datCallback callback,const char* fillColor=NULL);
	bkText* AddText(const char *title,float *f,bool readOnly);

	bkText* AddText(const char *title,int *i,bool readOnly,datCallback callback,const char* fillColor=NULL);
	bkText* AddText(const char *title,int *i,bool readOnly);

	bkText* AddText(const char *title,long *l,bool readOnly,datCallback callback,const char* fillColor=NULL);
	bkText* AddText(const char *title,long *l,bool readOnly);

	bkText* AddText(const char *title,atNonFinalHashString *h,bool readOnly,datCallback callback,const char* fillColor=NULL);
	bkText* AddText(const char *title,atNonFinalHashString *h,bool readOnly);

	bkText* AddText(const char *title,atFinalHashString *h,bool readOnly,datCallback callback,const char* fillColor=NULL);
	bkText* AddText(const char *title,atFinalHashString *h,bool readOnly);

	//
	// PURPOSE
	//	Adds a text edit widget to the bank.
	// PARAMS
	//	title - the string that describes what text is being edited
	//	str - a pointer to the data that will be edited
	//	len - the maximum length that the string can be 
	//	callback - the callback to call whenever the text is changed
	//  fillColor - the color of the widget
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkText* AddText(const char *title,char *str,unsigned int len,datCallback callback,const char* fillColor=NULL) {return AddText(title,str,len,false,callback,fillColor);}
	bkText* AddText(const char *title,char *str,unsigned int len) {return AddText(title,str,len,false);}

	bkText* AddText(const char *title,bool *b,datCallback callback,const char* fillColor=NULL) {return AddText(title,b,false,callback,fillColor);}
	bkText* AddText(const char *title,bool *b) {return AddText(title,b,false);}

	bkText* AddText(const char *title,float *f,datCallback callback,const char* fillColor=NULL) {return AddText(title,f,false,callback,fillColor);}
	bkText* AddText(const char *title,float *f) {return AddText(title,f,false);}

	bkText* AddText(const char *title,int *i,datCallback callback,const char* fillColor=NULL) {return AddText(title,i,false,callback,fillColor);}
	bkText* AddText(const char *title,int *i) {return AddText(title,i,false);}

	bkText* AddText(const char *title,long *l,datCallback callback,const char* fillColor=NULL) {return AddText(title,l,false,callback,fillColor);}
	bkText* AddText(const char *title,long *l) {return AddText(title,l,false);}

	bkText* AddText(const char *title,atNonFinalHashString *h,datCallback callback,const char* fillColor=NULL) {return AddText(title,h,false,callback,fillColor);}
	bkText* AddText(const char *title,atNonFinalHashString *h) {return AddText(title,h,false);}

	bkText* AddText(const char *title,atFinalHashString *h,datCallback callback,const char* fillColor=NULL) {return AddText(title,h,false,callback,fillColor);}
	bkText* AddText(const char *title,atFinalHashString *h) {return AddText(title,h,false);}

	//
	// PURPOSE
	//	Adds a toggle widget to the bank.
	// PARAMS
	//	title - the string that describes what the toggle does
	//	value - a pointer to the value to toggle
	//	callback - the callback that is called whenever the value is toggled
	//	memo - documentation for what this toggle actually does
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkToggle* AddToggle(const char *title,bool *value,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkToggle* AddToggle(const char *title,bool *value);

	bkToggle* AddToggle(const char *title,int *value,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkToggle* AddToggle(const char *title,int *value);

	bkToggle* AddToggle(const char *title,float *value,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkToggle* AddToggle(const char *title,float *value);

	//
	// PURPOSE
	//	Adds a toggle widget to the bank.
	// PARAMS
	//	title - the string that describes what the toggle does
	//	value - a pointer to the value to toggle
	//	mask - masks the bit to toggle on/off
	//	callback - the callback that is called whenever the value is toggled
	//	memo - documentation for what this toggle actually does
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkToggle* AddToggle(const char *title,unsigned *value,unsigned mask,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkToggle* AddToggle(const char *title,unsigned *value,unsigned mask);

	bkToggle* AddToggle(const char *title,unsigned short *value,unsigned short mask,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkToggle* AddToggle(const char *title,unsigned short *value,unsigned short mask);

	bkToggle* AddToggle(const char *title,unsigned char *value,unsigned char mask,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkToggle* AddToggle(const char *title,unsigned char *value,unsigned char mask);

	bkToggle* AddToggle(const char *title,atBitSet *value,unsigned char mask,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkToggle* AddToggle(const char *title,atBitSet *value,unsigned char mask);

	template <int size> bkToggle* AddToggle(const char *title,atFixedBitSet<size,u32> *value,unsigned bit,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);

	//
	// PURPOSE
	//	Adds a slider widget to the bank.
	// PARAMS
	//	title - the string that describes what the slider does
	//	value - a pointer to the value that the slider changes
	//	min - the minimum value that the slider value can be
	//	max - the maximum value that the slider value can be
	//	delta - the increment value that is applied when you click on the slider's arrows
	//	callback - the callback that is called whenever the slider value is changed
	//	memo - documentation for that the slider value is
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkSlider* AddSlider(const char *title,float *value,			float min,			float max,			float delta,		  datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,float *value,			float min,			float max,			float delta);

	bkSlider* AddSlider(const char *title,unsigned char* value,	unsigned char min,	unsigned char max,	unsigned char delta,  datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,unsigned char* value,	unsigned char min,	unsigned char max,	unsigned char delta);

	bkSlider* AddSlider(const char *title,signed char* value,	signed char min,	signed char max,	signed char delta,	  datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,signed char* value,	signed char min,	signed char max,	signed char delta);

	bkSlider* AddSlider(const char *title,unsigned short* value,unsigned short min,	unsigned short max,	unsigned short delta, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,unsigned short* value,unsigned short min,	unsigned short max,	unsigned short delta);

	bkSlider* AddSlider(const char *title,signed short* value,	signed short min,	signed short max,	signed short delta,	  datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,signed short* value,	signed short min,	signed short max,	signed short delta);

	bkSlider* AddSlider(const char *title,unsigned int* value,	unsigned int min,	unsigned int max,	unsigned int delta,	  datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,unsigned int* value,	unsigned int min,	unsigned int max,	unsigned int delta);

	bkSlider* AddSlider(const char *title,signed int* value,	signed int min,		signed int max,		signed int delta,	  datCallback callback, const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,signed int* value,	signed int min,		signed int max,		signed int delta);

	bkSlider* AddSlider(const char *title,Vector2 *value,		float min,			float max,			float delta,		  datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,Vector2 *value,		float min,			float max,			float delta);

	bkSlider* AddSlider(const char *title,Vector3 *value,		float min,			float max,			float delta,		  datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,Vector3 *value,		float min,			float max,			float delta);

	bkSlider* AddSlider(const char *title,Vector4 *value,		float min,			float max,			float delta,		  datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,Vector4 *value,		float min,			float max,			float delta);

	bkSlider* AddSlider(const char *title,Vector2 *value,		const Vector2 &min,	const Vector2 &max,	const Vector2 &delta, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,Vector2 *value,		const Vector2 &min,	const Vector2 &max,	const Vector2 &delta);

	bkSlider* AddSlider(const char *title,Vector3 *value,		const Vector3 &min,	const Vector3 &max,	const Vector3 &delta, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,Vector3 *value,		const Vector3 &min,	const Vector3 &max,	const Vector3 &delta);

	bkSlider* AddSlider(const char *title,Vector4 *value,		const Vector4 &min,	const Vector4 &max,	const Vector4 &delta, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,Vector4 *value,		const Vector4 &min,	const Vector4 &max,	const Vector4 &delta);

	bkSlider* AddSlider(const char *title,Float16 *value,		float min,			float max,			float delta,		  datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,Float16 *value,		float min,			float max,			float delta);

	bkSlider* AddSlider(const char *title,ScalarV *value,		float min,			float max,			float delta,		  datCallback callback,const char *memo=0,const char* fillColor=NULL,bool exponential=false,bool readOnly=false);
	bkSlider* AddSlider(const char *title,ScalarV *value,		float min,			float max,			float delta);

	//
	// PURPOSE
	//	Adds a combo list to the bank.
	// PARAMS
	//	title - the string that describes what the combo widget effects
	//	value - a pointer to the combo index
	//	numitems - the number of items that is in the combo list
	//	list - a pointer to an array of strings that describe the combo list
	//	valueOffset - the offset applied to value
	//	callback - the callback that is called whenever the combo list value is changed
	//	memo - documentation for that the combo list value is
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkCombo* AddCombo(const char *title,unsigned char *value,int numitems, const char **list,int valueOffset,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkCombo* AddCombo(const char *title,unsigned char *value,int numitems, const char **list,int valueOffset);

	bkCombo* AddCombo(const char *title,signed char *value,int numitems, const char **list,int valueOffset,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkCombo* AddCombo(const char *title,signed char *value,int numitems, const char **list,int valueOffset);

	bkCombo* AddCombo(const char *title,short *value,int numitems, const char **list,int valueOffset,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkCombo* AddCombo(const char *title,short *value,int numitems, const char **list,int valueOffset);

	bkCombo* AddCombo(const char *title,int *value,int numitems, const char **list,int valueOffset,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkCombo* AddCombo(const char *title,int *value,int numitems, const char **list,int valueOffset);

	bkCombo* AddCombo(const char *title,unsigned char *value,int numitems, const char **list,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkCombo* AddCombo(const char *title,unsigned char *value,int numitems, const char **list);

	bkCombo* AddCombo(const char *title,signed char *value,int numitems, const char **list,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkCombo* AddCombo(const char *title,signed char *value,int numitems, const char **list);

	bkCombo* AddCombo(const char *title,short *value,int numitems, const char **list,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkCombo* AddCombo(const char *title,short *value,int numitems, const char **list);

	bkCombo* AddCombo(const char *title,int *value,int numitems, const char **list,datCallback callback,const char *memo=0,const char* fillColor=NULL,bool readOnly=false);
	bkCombo* AddCombo(const char *title,int *value,int numitems, const char **list);


	//
	// PURPOSE
	//	Adds set of widgets to the bank that allow you to edit a color value.
	// PARAMS
	//	title - the string that describes what the color is for
	//	value - a pointer to the color vector that the sliders change
	//	delta - the increment value that is applied when you click on the slider's arrows for any of the color channels
	//	callback - the callback that is called whenever the color vector is changed
	//	memo - documentation for that the color vector is
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkColor* AddColor(const char * title,Vector4 * color, float delta, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,Vector4 * color, float delta);

	bkColor* AddColor(const char * title,Vec4V * color, float delta, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,Vec4V * color, float delta);

	bkColor* AddColor(const char * title,Vector3 * color, float delta, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,Vector3 * color, float delta);

	bkColor* AddColor(const char * title,Vec3V * color, float delta, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,Vec3V * color, float delta);

	bkColor* AddColor(const char * title,Vector4 * color, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,Vector4 * color);

	bkColor* AddColor(const char * title,Vec4V * color, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,Vec4V * color);

	bkColor* AddColor(const char * title,Vector3 * color, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,Vector3 * color);

	bkColor* AddColor(const char * title,Vec3V * color, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,Vec3V * color);

	bkColor* AddColor(const char * title,Color32 * color, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,Color32 * color);

	bkColor* AddColor(const char * title,float * color, int floatArraySize, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,float * color, int floatArraySize=3);

	bkColor* AddColor(const char * title,Float16 * color, int floatArraySize, datCallback callback,const char *memo=0,const char* fillColor=NULL,bool inOutLinear=false, bool readOnly=false);
	bkColor* AddColor(const char * title,Float16 * color, int floatArraySize=3);


	//
	// PURPOSE
	//	Adds a list widget
	// PARAMS
	//	title - the name of this widget
	//	readOnly - false if in-place editing should be allowed
	//	memo - a short description of what this list displays
	//  fillColor - the color of the widget
	// RETURNS
	//	a pointer to the created list
	//
	bkList* AddList(const char* title,bool readOnly=true,const char* memo=0,const char* fillColor=NULL);

	//
	// PURPOSE
	//	adds a tree list widget
	// PARAMS
	//	title - the name of this widget
	//	memo - a short description of what this list displays
	//  allowDrag - true if you want the TreeList to be able to copy nodes to other bkTreeLists
	//  allowDrop - true if you want the TreeList to be able to paste nodes from other bkTreeLists
	//  allowDelete - true if you want the TreeList to be able to delete nodes
	//  showRootLines - false if you do not want to show root lines.  Good if you will have a large number of nodes and need to speed things up.
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	a pointer to the created list
	//
	bkTreeList* AddTreeList( const char* title, const char* memo=0, bool allowDrag=false, bool allowDrop=false, bool allowDelete=false, bool showRootLines=true, const char* fillColor=NULL, bool readOnly=false );

	// PURPOSE
	//  adds a data widget, which is an invisible pipe for data communication between the game and Rag (or Rag Plugins).
	// PARAMS
	//	title - the name of this widget
	//  data - the buffer.  can be of any (reasonable) size.
	//  length - length of buffer
	//	callback - the callback that is called whenever new data is received
	//	memo - a short description of what this list displays
	//  showDataView - when true, creates a widget visible for debugging purposes
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS:
	//  a pointer to the created data widget
	// NOTES:
	//  If data is NULL and length is greater than zero, an internal buffer is allocated and managed for you.  Rag will limit its message
	//   sizes to the length specified.  
	//  If data is NULL and length is zero, the maximum data size is unbound.  An internal buffer is allocated (and subsequently managed for you)
	//   when the first message is received from Rag.  This buffer can then shrink/grow according to the size of the messages.
	bkData* AddDataWidget( const char *title, u8 *data=NULL, u16 length=0, datCallback callback=NullCB, const char *memo=0, bool showDataView=false, const char* fillColor=NULL, bool readOnly=false );

	//
	// PURPOSE
	//	Adds a vector widget to the bank.  This is more compact than a Slider widget that uses a vector, yet offers much 
	//  more functionality specific to vectors.
	// PARAMS
	//	title - the string that describes what the vector does
	//	value - a pointer to the value that the widget changes
	//	min - the minimum value that each component of the vector can be
	//	max - the maximum value that each component of the vector can be
	//	delta - the increment value that is applied when you click each vector component's arrows
	//	callback - the callback that is called whenever the vector value is changed
	//	memo - documentation for that the vector value is
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkVector2* AddVector(const char *title, Vector2 *value,	float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector2* AddVector(const char *title, Vector2 *value,	float min, float max, float delta );

	bkVector2* AddVector(const char *title, Vec2V *value,	float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector2* AddVector(const char *title, Vec2V *value,	float min, float max, float delta );

	bkVector3* AddVector(const char *title, Vector3 *value,	float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector3* AddVector(const char *title, Vector3 *value,	float min, float max, float delta );

	bkVector3* AddVector(const char *title, Vec3V *value,	float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector3* AddVector(const char *title, Vec3V *value,	float min, float max, float delta );

	bkVector4* AddVector(const char *title, Vector4 *value,	float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector4* AddVector(const char *title, Vector4 *value,	float min, float max, float delta );

	bkVector4* AddVector(const char *title, Vec4V *value,	float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector4* AddVector(const char *title, Vec4V *value,	float min, float max, float delta );

	bkVector3* AddVector(const char *title, Matrix33 *value, float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector3* AddVector(const char *title, Matrix33 *value, float min, float max, float delta );

	bkVector3* AddVector(const char *title, Matrix34 *value, float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector3* AddVector(const char *title, Matrix34 *value, float min, float max, float delta );

	bkVector3* AddVector(const char *title, Mat34V *value, float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector3* AddVector(const char *title, Mat34V *value, float min, float max, float delta );

	bkVector4* AddVector(const char *title, Matrix44 *value, float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector4* AddVector(const char *title, Matrix44 *value, float min, float max, float delta );

	bkVector4* AddVector(const char *title, Mat44V *value, float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkVector4* AddVector(const char *title, Mat44V *value, float min, float max, float delta );

	//
	// PURPOSE
	//	Adds a matrix widget to the bank.  This takes up slightly more space than a Vector widget that uses a matrix, but offers 
	//  much more functionality specific to matrices.
	// PARAMS
	//	title - the string that describes what the matrix does
	//	value - a pointer to the value that the widget changes
	//	min - the minimum value that each component of the matrix can be
	//	max - the maximum value that each component of the matrix can be
	//	delta - the increment value that is applied when you click each matrix component's arrows
	//	callback - the callback that is called whenever the matrix value is changed
	//	memo - documentation for that the matrix value is
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkMatrix33* AddMatrix(const char *title, Matrix33 *value, float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkMatrix33* AddMatrix(const char *title, Matrix33 *value, float min, float max, float delta );

	bkMatrix34* AddMatrix(const char *title, Matrix34 *value, float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkMatrix34* AddMatrix(const char *title, Matrix34 *value, float min, float max, float delta );

	bkMatrix44* AddMatrix(const char *title, Matrix44 *value, float min, float max, float delta, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkMatrix44* AddMatrix(const char *title, Matrix44 *value, float min, float max, float delta );

	//
	// PURPOSE
	//  adds an Image Viewer widget to the bank
	// PARAMS
	//	title - the string that describes what the matrix does
	//	memo - documentation for that the matrix value is
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkImageViewer* AddImageViewer( const char *title, const char *memo=0, const char *fillColor=0);

	//
	// PURPOSE
	//  adds an Angle widget to the bank
	// PARAMS
	//	title - the string that describes what the matrix does
	//  data - pointer to the value of the angle
	//  angleType - type type or units of the angle (radians, degrees, etc.).
	//  min - the minimum possible value.  when not given, the minimum is -FLT_MAX / 1000.0f.
	//  max - the maximum possible value.  when not given, the minimum is FLT_MAX / 1000.0f.
	//	memo - documentation for that the matrix value is
	//  fillColor - the color of the widget
	//  readOnly - Flag indicating whether this widget should be readonly.
	// RETURNS
	//	returns a pointer to the created widget (can be NULL)
	//
	bkAngle* AddAngle( const char *title, float *data, bkAngleType::Enum angleType, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkAngle* AddAngle( const char *title, float *data, bkAngleType::Enum angleType );

	bkAngle* AddAngle( const char *title, Vector2 *data, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkAngle* AddAngle( const char *title, Vector2 *data );

	bkAngle* AddAngle( const char *title, float *data, bkAngleType::Enum angleType, float min, float max, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkAngle* AddAngle( const char *title, float *data, bkAngleType::Enum angleType, float min, float max );

	bkAngle* AddAngle( const char *title, Vector2 *data, float min, float max, datCallback callback, const char *memo=0, const char* fillColor=NULL, bool readOnly=false );
	bkAngle* AddAngle( const char *title, Vector2 *data, float min, float max );

	//
	// PURPOSE
	//	adds a generic widget to the bank (used buy other Add* functions and derived widget types)
	// PARAMS
	//	widget - the widget to add 
	// RETURNS
	//	returns the same pointer as a pointer to the widget argument
	//
	virtual bkWidget* AddWidget(bkWidget& widget);


protected:
	//
	// PURPOSE
	//	constructor
	// PARAMS
	//	callback - called whenever the group is expanded or contracted
	//	title - the title of group
	//	memo - the string that shows up in the tooltip of the group
	//	startOpen - does the group startup expanded?
	//  fillColor - A specially formatted string representing the desired color of the widget.
	//   If this is an empty string, NULL, or "ARGBColor:0:0:0:0", the default system color 
	//   for Control is used.  
	//   "NamedColor:nameOfColor" can be used to colorize with a named Windows System color.
	//   "ARGBColor:x:x:x:x" can be used to colorize with an ARGB color, each value ranging from 0 to 255 (recommended alpha: 128).
	//  readOnly - Flag indicating whether this widget should be readonly.
	//
	bkGroup(datCallback &callback,const char *title,const char *memo,bool startOpen,const char* fillColor=NULL, bool readOnly=false);
	// PURPOSE: destructor
	virtual ~bkGroup();

	void Message(Action action,float value);
	int DrawLocal(int x,int y);
	void Update();

	static int GetStaticGuid() { return BKGUID('g','r','u','p'); }
	int GetGuid() const;
	static void RemoteHandler(const bkRemotePacket& p);
	bool IsShown() const;
	bool IsOpen() const;
	bool IsClosedGroup() const;
	void SetClosed();

protected:
#if __WIN32PC
	void WindowCreate();
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM);
	int WindowResize(int x,int y,int width,int height);
	void WindowUpdate();
#endif
	void RemoteCreate();
	void RemoteUpdate();
	s16 m_Open, m_PrevOpen; // Why aren't these just bools?
};


template <int size> bkToggle* bkGroup::AddToggle(const char *title,atFixedBitSet<size,u32> *value,unsigned bit,datCallback callback,const char *memo, const char *fillColor, bool readOnly)
{
	return (bkToggle*) AddWidget(*(rage_new bkToggleVal_atFixedBitSet<size>(callback,title,memo,value,bit,fillColor,readOnly)));
}

inline bkCombo* bkGroup::AddCombo(const char *title,unsigned char *value,int numitems, const char **list,datCallback callback,const char *memo, const char *fillColor, bool readOnly)	
{
	return AddCombo(title,value,numitems,list,0,callback,memo,fillColor,readOnly);
}

inline bkCombo* bkGroup::AddCombo(const char *title,signed char *value,int numitems, const char **list,datCallback callback,const char *memo, const char *fillColor, bool readOnly)		
{
	return AddCombo(title,value,numitems,list,0,callback,memo,fillColor,readOnly);
}

inline bkCombo* bkGroup::AddCombo(const char *title,short *value,int numitems, const char **list,datCallback callback,const char *memo, const char *fillColor, bool readOnly)			
{
	return AddCombo(title,value,numitems,list,0,callback,memo,fillColor,readOnly);
}

inline bkCombo* bkGroup::AddCombo(const char *title,int *value,int numitems, const char **list,datCallback callback,const char *memo, const char *fillColor, bool readOnly)				
{
	return AddCombo(title,value,numitems,list,0,callback,memo,fillColor,readOnly);
}

}	// namespace rage

#endif

#endif