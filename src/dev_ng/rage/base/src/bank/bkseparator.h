// 
// bank/bkseparator.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BANK_BKSEPARATOR_H
#define BANK_BKSEPARATOR_H

#include "widget.h"

#if __BANK

namespace rage
{
	class bkSeparator : public bkWidget
	{
		friend class bkGroup;
		friend class bkManager;
	
	protected:
		bkSeparator( const char *title, const char *fillColor=NULL );
		~bkSeparator();

		int DrawLocal( int x, int y );
		static int GetStaticGuid() { return BKGUID('s','e','p','r'); }
		int GetGuid() const;

		static void RemoteHandler( const bkRemotePacket& p );

	private:
#if __WIN32PC
		void WindowCreate();
		rageLRESULT WindowMessage( rageUINT, rageWPARAM, rageLPARAM );
#endif
		void RemoteCreate();
	};

}	// namespace rage

#endif

#endif
