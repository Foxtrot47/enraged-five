//
// bank/bank.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_BANK_H
#define BANK_BANK_H

#include "bkangle.h"	// for EAngleType
#include "group.h"
#include "toggle.h"		// For bitset template

#include "data/callback.h"

#if __BANK && !__SPU

namespace rage {

class atBitSet;
class Color32;
class Matrix33;
class Matrix34;
class Matrix44;
class Vector2;
class Vector3;
class Vector4;
class Float16;

class bkButton;
class bkCombo;
class bkData;
class bkImageViewer;
class bkList;
class bkMatrix33;
class bkMatrix34;
class bkMatrix44;
class bkSeparator;
class bkSlider;
class bkText;
class bkTitle;
class bkToggle;
class bkTreeList;
class bkVector2;
class bkVector3;
class bkVector4;

//
// PURPOSE:
//	This class specifies a collection of groups and widgets.  It contains <c>Add()</c> functions
//	for creating widgets.
//  <FLAG Component>
class bkBank: public bkGroup {
public:
	//
	// PURPOSE
	//	overloaded constructor
	// PARAMS
	//	name - the visible name of the bank window
	//	creator - the callback that will create the visual representation of the bank (callback must have only one argument to a bkBank)
	//	destructor - the callback that will destroy the visual representation of the bank (callback must have only one argument to a bkBank)
	//
	bkBank(const char *name,datCallback creator,datCallback destructor);
	
	// PURPOSE: destructor
	~bkBank();

	// PURPOSE: called at least once every frame.  The bank also calls updates on it's collection of widgets.
	void Update();

	// PURPOSE: Show the bank so that the user can see and interact with it.
	void Show();
	
	// PURPOSE: Hide the visual representation of the bank.
	void Hide();
	
	//
	// PURPOSE
	//	Starts a group in the bank.  Must be matched with an eventual PopGroup().
	//  Groups can be nested.
	// PARAMS
	//	title - the displayed title of the group
	//	startOpen - should the group be started already expanded?
	//	memo - documentation for describing the group in finer detail
	//  color - A specially formatted string representing the desired color of the group.
	//   If this is an empty string, NULL, or "ARGBColor:0:0:0:0", the default system color 
	//   for Control is used.  
	//   "NamedColor:nameOfColor" can be used to colorize with a named Windows System color.
	//   "ARGBColor:x:x:x:x" can be used to colorize with an ARGB color, each value ranging from 0 to 255.
	// RETURNS
	//	returns a pointer to the created group (can be NULL)
	//
	bkGroup* PushGroup(const char *title,bool startOpen=true,const char *memo=NULL,const char* fillColor=NULL);

	// PURPOSE: Called when a group is closed.  Must be matched to a previously called PushGroup().
	void PopGroup();
	
	//
	// PURPOSE
	//  Forces subsequent widget additions to get added to the specified group. CANNOT BE USED DURING NORMAL
	//	widget additions this is intended for adding widgets to the specified bank AFTER creation and at
	//  runtime. Not that Push/PopGroup operations between SetGroup /UnSetGroup are illegal and will
	//	mess things up. This function is no longer recommended for use - since you can add widgets directly
	//  to another group now.
	// PARAMS
	//	rGroup - Reference to the group that further items will be added to
	// NOTES
	//  Read the PUROSE section and use with care
	//  This functionality should be replaced without code that is more solid and flexible the next time the bkBank class
	//  gets re-factored (I would figure just making the banks hierarchical).
	void SetCurrentGroup(bkGroup &rGroup);

	//PURPOSE
	// Pair with SetGroup to leave the bank in a valid state
	// PARAMS
	//	rGroup - Reference to the group that we already called SetGroup with
	void UnSetCurrentGroup(bkGroup &rGroup);

	// PURPOSE: Accessor for the current group (which may be this if adding to the top-level bank)
	// RETURNS: the current group (which may be this if adding to the top-level bank)
	bkWidget* GetCurrentGroup() { return m_Current; }

	//
	// PURPOSE
	//	Destroys the passed in group.
	// PARAMS
	//	group - the group to be destroyed.
	//
	void DeleteGroup(bkGroup& group);

	//
	// PURPOSE
	//	shrinks the number of widgets in the bank to a certain count
	// PARAMS
	//	newCount - the number of widgets to shrink to
	//
	void Truncate(int newCount);
	
	// PURPOSE: accessor for the number of widgets that the bank contains
	// RETURNS: number of widgets in this bank
	int GetNumWidgets() const;
	
	// PURPOSE: is this bank visible
	// RETURNS: true if the bank is visible, false if not
	bool IsShown() const;

	//
	// PURPOSE
	//	Returns general information about this bank.
	// PARAMS
	//	outShown - returns if the bank is shown
	//	outX - returns the x position of the bank
	//	outY - returns the y position of the bank
	//	outWidth - returns the width of the bank
	//	outHeight - returns the height of the bank
	//
	void GetState(bool &outShown,int &outX,int &outY,int &outWidth,int &outHeight);
	
	//
	// PURPOSE
	//	Sets general information about this bank.
	// PARAMS
	//	shown - sets if the bank is shown
	//	x - sets the x position of the bank
	//	y - sets the y position of the bank
	//	width - sets the width of the bank
	//	height - sets the height of the bank
	//
	void SetState(bool shown,int x,int y,int width,int height);	
	
	//
	// PURPOSE
	//	The function is called when handling communication from a remote bank client or server.
	// PARAMS
	//	packet - the packet passed from the remote bank client or server
	//
	static void RemoteHandler(const bkRemotePacket& packet);
	
	// PURPOSE: accessor for the unique id for this class (used in remote communication)
	// RETURNS: returns the bank class' GUID
	static int GetStaticGuid();
	// <ALIAS GetStaticGuid>
	int GetGuid() const;

	bool IsClosedGroup() const;

	//
	// PURPOSE
	//	adds a generic widget to the bank (used buy other Add* functions and derived widget types)
	// PARAMS
	//	widget - the widget to add 
	// RETURNS
	//	returns the same pointer as a pointer to the widget argument
	//	Overridden so that we can add a widget to one of the internal groups.
	//
	virtual bkWidget* AddWidget(bkWidget& widget);

#if __ASSERT
	static void CheckThread(const char* funcName, const char* groupName);
#endif // __ASSERT

protected:
	virtual bkWidget& FinishCreate();

private:
	void Changed();
	bkWidget *m_Current;
	datCallback *m_Destructor;
	void RemoteCreate();
	void RemoteUpdate();

#if __WIN32PC
	void WindowCreate();
	void WindowDestroy();
	int WindowResize(int x,int y,int width,int height);
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM);
	bkPane* GetPane();
	bkPane *m_Pane;
#endif
};

// PURPOSE: A RAII way to call bank.PushGroup and bank.PopGroup
class bkAutoGroup
{
public:
	bkAutoGroup(bkBank* bank, const char* name, bool startOpen=true, const char* memo = NULL, const char* fillColor = NULL) {
		FastAssert(bank);
		m_Bank = bank;
		m_Bank->PushGroup(name, startOpen, memo, fillColor);
	}
	~bkAutoGroup() {m_Bank->PopGroup();}

protected:
	bkBank* m_Bank;
};

#define BANK_GROUP(bank, name, ...) ::rage::bkAutoGroup group_ ## __LINE__ ((bank) , (name) , ##__VA_ARGS__ );



inline int bkBank::GetNumWidgets() const 
{ 
	return bkWidget::GetNumWidgets() - 1; /* don't include self in answer */ 
}

//inline bool bkBank::IsShown() const
//{ 
//	return IsOpen(); 
//}

inline int bkBank::GetStaticGuid() 
{ 
	return BKGUID('b','a','n','k'); 
}

} // namespace rage

#endif

#endif
