//
// bank/button.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "button.h"

#include "packet.h"
#include "pane.h"

using namespace rage;

bkButton::bkButton(datCallback cb,const char *title,const char *memo, const char *fillColor, bool readOnly) :
bkWidget(cb,title,memo,fillColor,readOnly) { }


bkButton::~bkButton() {
}

void bkButton::Message(Action action,float /*value*/) {
	if (action == DIAL) {
		RemoteUpdate();
		Changed();
	}
}


int bkButton::DrawLocal(int x,int y) {
	Drawf(x,y,"[[%s]]",m_Title);
	return bkWidget::DrawLocal(x,y);
}


int bkButton::GetGuid() const { return GetStaticGuid(); }


void bkButton::RemoteCreate() {
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetStaticGuid(),this);
	p.WriteWidget(m_Parent);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
	p.Send();
}


void bkButton::RemoteUpdate() {
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin(bkRemotePacket::CHANGED,GetStaticGuid(),this);
	p.Send();
}


void bkButton::RemoteHandler(const bkRemotePacket& packet) {
	if (packet.GetCommand() == bkRemotePacket::CREATE) {
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;
		const char *title = packet.Read_const_char();
		const char *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();
		packet.End();
		bkButton& widget = *(rage_new bkButton(NullCB,title,memo,fillColor,readOnly));
		bkRemotePacket::SetWidgetId(widget, id);
		parent->AddChild(widget);
	}
	else if (packet.GetCommand() == bkRemotePacket::CHANGED) {
		packet.Begin();
		bkButton* widget = packet.ReadWidget<bkButton>();
		packet.End();
		if (widget)
			widget->Changed();
	}
}


#if __WIN32PC

#include "system/xtl.h"


void bkButton::WindowCreate() {
	if (!bkRemotePacket::IsConnectedToRag())
	{
		GetPane()->AddLastWindow(this, "BUTTON", 0, BS_PUSHBUTTON);
	}
}


rageLRESULT bkButton::WindowMessage(rageUINT msg,rageWPARAM /*wParam*/,rageLPARAM /*lParam*/) {
	if (msg == WM_COMMAND) {
		if (bkRemotePacket::IsConnected())
		{
			RemoteUpdate();
		}
		Changed();
	}

	return 0;
}



#endif
#endif
