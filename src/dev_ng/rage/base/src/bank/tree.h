//
// bank/tree.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_TREE_H
#define BANK_TREE_H

#if __WIN32PC // tree.h functionality is only availible in win32 pc builds!

#include "system/wndproc.h"

// Opaque pointer used by underlying implementation
struct _TREEITEM;
#define bkTreeViewerItem _TREEITEM

#if __WIN32
struct HWND__;
struct HFONT__;
struct tagNMTREEVIEWA;
#endif

namespace rage {

#define BKTV_GETLABEL	0
#define BKTV_GETCHILD	1
#define BKTV_GETNEXT	2
#define BKTV_NEWSEL		3
#define BKTV_POSTMENU	4
#define BKTV_KEYDOWN	5

// These match the TVI_FIRST, LAST, and SORT constants in Windows headers.
#define BKTV_FIRST		(bkTreeViewerItem*)(0xFFFF0001)
#define BKTV_LAST		(bkTreeViewerItem*)(0xFFFF0002)
#define BKTV_SORT		(bkTreeViewerItem*)(0xFFFF0003)

typedef void* (*bkTreeViewerCallback)(void *context,int cmd /*BKTV_...*/,void *user);

//
// PURPOSE
//	A widget for displaying a tree hierarchy.
//  <FLAG Component>
//
class bkTreeViewer {
public:
	//
	// PURPOSE
	//	constructor
	// PARAMS
	//	title - the title of the tree view control
	//	x - horizontal position of the tree view
	//	y - vertical position of the tree view
	//	width - horizontal dimension of the tree view
	//	height - vertical dimension of the tree view
	//
	bkTreeViewer(const char *title,int x = 100,int y = 300,int width = 200,int height = 300,bool standalone=false);
	// PURPOSE: destructor
	~bkTreeViewer();

	//
	// PURPOSE
	//	insert a tree view item into the tree
	// PARAMS
	//	parent - the parent item of the item to insert as a child of 
	//	after - the item to insert the tree view item after
	//	ctxt - the lParam of the tree view item 
	//	label - the visible text label of the item
	// RETURNS
	//	<describe what it returns>
	//
	bkTreeViewerItem* Insert(bkTreeViewerItem *parent,bkTreeViewerItem *after,void *ctxt,const char *label);
	//
	// PURPOSE
	//	deletes a tree view item from the tree
	// PARAMS
	//	item - a pointer of the item to delete
	// RETURNS
	//	returns true if the delete was successful, false if not
	//
	bool Delete(bkTreeViewerItem *item);
	//
	// PURPOSE
	//	sets the label of a tree item
	// PARAMS
	//	item - a pointer to the object which we want to change the label of 
	//	pszLabel - the character string that will replace the label
	// RETURNS
	//	returns true if the set was successful, false if not
	//
	bool SetItemLabel(bkTreeViewerItem *item, const char *pszLabel);
	//
	// PURPOSE
	//	gets the label of the currently selected tree item
	// PARAMS
	//	text - a pointer to buffer to which the text will be copied 
	//	size - the character string that will replace the label
	// RETURNS
	//	returns a pointer to the character pointer passed in
	//  the char pointer set to NULL if there is no current selection
	const char* GetCurSelectionLabel(char* text, int size);
	//
	// PURPOSE
	//	sets the node currently selected
	// PARAMS
	//	node - the node that should be selected
	//
    void SetSelection(void * node);
	// PURPOSE: redraws the tree view - should be called once per frame
	void Update();

	//
	// PURPOSE
	//	sets the font of the labels of the tree view nodes
	// PARAMS
	//	fontName - the name of the font
	//	height - the size of the font
	//
	void SetFont(const char* fontName,int height);

	//
	// PURPOSE
	//	makes a tree from a generic tree 
	// PARAMS
	//	cb - the callback that is called at every node
	//	root - a pointer to the root new of the tree
	//
	void MakeTree(bkTreeViewerCallback cb,void* root);
	//
	// PURPOSE
	//	constructs a view of a tree from a stream
	// PARAMS
	//	stream - a reference to the stream that contains the tree representation
	//
	void MakeTreeFromStream(class fiStream& stream);
	//
	// PURPOSE
	//	sets the user data for the tree
	// PARAMS
	//	u - a pointer to the user data
	//
	void SetUserData(void *u);
	// PURPOSE: accessor to the tree's user data
	// RETURNS: returns a pointer to the user data
	void* GetUserData() const;
	// PURPOSE: accessor to the tree's current selection
	// RETURNS: returns a pointer to the tree's current selection
	void* GetSelection() const;
	// PURPOSE: accessor for the tree view's title
	// RETURNS: returns a pointer to the tree view's title
	const char *GetTitle() const;
	//
	// PURPOSE
	//	sets the title of the tree view
	// PARAMS
	//	title - a pointer to the new title
	//
	void SetTitle(const char *title);
	// PURPOSE: deletes all items in the tree view
	void Reset();

	// PURPOSE: raise the tree view so it is visible on top of 
	// all other windows
	void Raise();

	//
	// PURPOSE
	//	sets if the tree view is always visible on top of other windows
	// PARAMS
	//	b - true if the tree view window should be always on top 
	//	of other visible widnwos
	//
	void SetAlwaysOnTop( bool b )	{ m_AlwaysOnTop = b; Raise(); }
	// PURPOSE: accessor to check if the tree view should always be on 
	// of all other windows
	// RETURNS: returns true if the window should always be on toop
	bool GetAlwaysOnTop() const		{ return m_AlwaysOnTop; }

	// PURPOSE: accessor to check if the tree view is alive
	// RETURNS: returns true if the tree view has been created
	bool IsAlive() const;

	//
	// PURPOSE
	//	sets the callback that is called whenever an item is selected
	// PARAMS
	//	cb - the callback
	//
	void SetCallback(bkTreeViewerCallback cb);
	
private:
	void Init(bool standalone=false);
	void RecurseTree(bkTreeViewerItem*,void*);
	bkTreeViewerCallback m_Callback;
	void *m_Selection;
	void *m_UserData;
	const char *m_Title;
	int m_X, m_Y, m_Width, m_Height;

	// Win32 PC specific stuff here:
	static rageLRESULT __stdcall WindowProc(struct HWND__ *,rageUINT,rageWPARAM,rageLPARAM);
	void Notify(tagNMTREEVIEWA*);
	HWND__* m_HwndParent, *m_HwndTree;
	HFONT__* m_Font;

	bool m_AlwaysOnTop;
};

inline void bkTreeViewer::SetSelection(void * node) 
{
	m_Selection = node; 
	if (m_Callback) 
		m_Callback(m_Selection,BKTV_NEWSEL,m_UserData);
}

inline void bkTreeViewer::SetUserData(void *u) 
{ 
	m_UserData = u; 
}

inline void* bkTreeViewer::GetUserData() const 
{ 
	return m_UserData; 
}

inline void* bkTreeViewer::GetSelection() const 
{ 
	return m_Selection; 
}

inline const char* bkTreeViewer::GetTitle() const 
{ 
	return m_Title; 
}

inline bool bkTreeViewer::IsAlive() const 
{
#if __WIN32
	return m_HwndTree != 0;
#else
	return true;
#endif
}

inline void bkTreeViewer::SetCallback(bkTreeViewerCallback cb) 
{ 
	m_Callback = cb; 
}

}	// namespace rage

#endif // __WIN32PC

#endif
