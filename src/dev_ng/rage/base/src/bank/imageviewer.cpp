//
// bank/imageviewer.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "imageviewer.h"

#include "packet.h"

using namespace rage;

bkImageViewer::bkImageViewer(const char *title,const char *memo, const char *fillColor) 
: bkWidget(NullCallback,title,memo,fillColor)
, m_pImageFilename(NULL)
{
}

bkImageViewer::~bkImageViewer()
{
    if ( m_pImageFilename != NULL )
    {
        StringFree( m_pImageFilename );
        m_pImageFilename = NULL;
    }
}

int bkImageViewer::GetGuid() const { return GetStaticGuid(); }

void bkImageViewer::RemoteHandler(const bkRemotePacket& )
{
}

void bkImageViewer::SetImage(const char* imageFileName)
{
    if ( m_pImageFilename != NULL )
    {
        StringFree( m_pImageFilename );
    }

    m_pImageFilename = StringDuplicate( imageFileName );

	if (!bkRemotePacket::IsConnected()) return;

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CHANGED,GetStaticGuid(),this);
	p.Write_const_char(imageFileName);
	p.Send();
}

void bkImageViewer::GetStringRepr( char *buf, int bufLen )
{
    safecpy( buf, m_pImageFilename, bufLen );
}

void bkImageViewer::RemoteCreate() 
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetStaticGuid(),this);
	p.WriteWidget(m_Parent);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char( GetFillColor() );
	p.Write_bool(IsReadOnly());
	p.Send();
}

#endif
