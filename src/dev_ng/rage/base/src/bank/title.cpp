//
// bank/title.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "title.h"

#include "packet.h"
#include "pane.h"

using namespace rage;

bkTitle::bkTitle(const char *title, const char *fillColor) : bkWidget(NullCB,title,0,fillColor,true) {
}



bkTitle::~bkTitle() {
}


int bkTitle::GetGuid() const { return GetStaticGuid(); }



int bkTitle::DrawLocal(int x,int y) {
	Drawf(x,y,"-- %s --",m_Title);
	return bkWidget::DrawLocal(x,y);
}


void bkTitle::RemoteCreate() {
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetStaticGuid(),this);
	p.WriteWidget(m_Parent);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char( GetFillColor() );
	p.Write_bool(IsReadOnly());
	p.Send();
}


void bkTitle::RemoteHandler(const bkRemotePacket& packet) {
	if (packet.GetCommand() == bkRemotePacket::CREATE) {
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;
		const char *title = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		packet.Read_bool();
		packet.End();
		bkTitle& widget = *(rage_new bkTitle(title,fillColor));
		bkRemotePacket::SetWidgetId(widget, id);
		parent->AddChild(widget);
	}
	/* else if (packet.m_Command == bkRemotePacket::CHANGED) {
		packet.Begin();
		bkTitle *local = (bkTitle*) bkRemotePacket::RemoteToLocal(packet.Read_bkWidget());
		const char *data = packet.Read_const_char();
		safecpy(m_String, data);
		safecpy(m_PrevString, data);
		packet.End();
		local->Changed();
	} */
}


#if __WIN32PC
#include "system/xtl.h"

void bkTitle::WindowCreate() {
	if (!bkRemotePacket::IsConnectedToRag())
		GetPane()->AddLastWindow(this, "STATIC", 0, SS_CENTER | SS_NOPREFIX);
}


rageLRESULT bkTitle::WindowMessage(rageUINT /*msg*/,rageWPARAM /*wParam*/,rageLPARAM /*lParam*/) {
	return 0;
}

#endif

#endif
