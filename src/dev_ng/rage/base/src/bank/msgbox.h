//
// bank/msgbox.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_MSGBOX_H
#define BANK_MSGBOX_H

namespace rage {

enum bkMessageBoxType {
	bkMsgOk,			// show just the "Ok" button
	bkMsgOkCancel,		// show the "Ok" and "Cancel" button
	bkMsgYesNo,			// show "Yes" and "No" buttons
	bkMsgYesNoCancel,	// show "Yes", "No", and "Cancel" buttons
};
enum bkMessageBoxIcon {
	bkMsgInfo,			// the info icon
	bkMsgQuestion,		// the question mark icon
	bkMsgError			// the error icon
};

#if !__FINAL
// 
//
// PURPOSE
//	post a message box.
// PARAMS
//	title - the title of the message box
//  message - the message string in the message box
//  type - the set of buttons to show in the message box
//  icon - the icon in the message box
// RETURNS
//  Returns -1 for Cancel (if appropriate), 0 for No (if appropriate), or 1 for Yes/Ok.
//	<FLAG Component>
//
int bkMessageBox(const char *title,const char *message,bkMessageBoxType type,bkMessageBoxIcon icon);
#endif

}	// namespace rage

#endif // BANK_MSGBOX_H
