//
// bank/pane.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_PANE_H
#define BANK_PANE_H

#include "widget.h"

#include "system/wndproc.h"

struct HWND__;
struct HFONT__;

#if __BANK

namespace rage {

//
// PURPOSE:
//	This class is the visual container for a set of widgets created in the Windows
//	environment.
//
class bkPane 
{
	friend class bkWidget;
public:
	//
	// PURPOSE
	//	constructor
	// PARAMS
	//	owner - a pointer to the Windows window that owns the pane
	//	widget - the bank widget that the pane will display
	//	title - the title text of the pane
	//	x - the horizontal position of the pane
	//	y - the vertical position of the pane
	//	isMaster - is this the master pane? 
	//
	bkPane(struct HWND__* owner,bkWidget *widget,const char *title,int x,int y,bool isMaster);
	// PURPOSE: destructor
	~bkPane();

	//
	// PURPOSE
	//	Adds a Windows window to the pane using the title and tooltip of the bkWidget.
	// PARAMS
	//	widget - bank widget of the added window
	//	className - the name of the Windows type control
	//	exStyle - the exStyle argument passed to the Windows function CreateWindowEx()
	//	style - the style argument passed to the Windows function CreateWindowEx()
	//	extraHeight - increment added to the window size 
	// RETURNS
	//	returns a pointer to the Windows created window
	//
	struct HWND__* AddWindow(bkWidget *widget,const char *className,unsigned exStyle,unsigned style,int extraHeight = 0);

	//
	// PURPOSE
	//	Adds a Windows window to the pane with the given title and tooltip.
	// PARAMS
	//	widget - bank widget of the added window
	//	className - the name of the Windows type control
	//	exStyle - the exStyle argument passed to the Windows function CreateWindowEx()
	//	style - the style argument passed to the Windows function CreateWindowEx()
	//  title - the title to give the window
	//  tooltip - the tooltip to give the window
	//	extraHeight - increment added to the window size 
	// RETURNS
	//	returns a pointer to the Windows created window
	//
	struct HWND__* AddWindow( bkWidget *widget, const char *className, unsigned exStyle, unsigned style, 
		const char* title, const char* tooltip=0, int extraHeight=0 );

	//
	// PURPOSE
	//	Adds a Windows window to the pane using the title and tooltip of the bkWidget.  This window is the last 
	//  window in the pane, and no calls to AddWindow() should happen after AddLastWindow() is called. 
	// PARAMS
	//	widget - bank widget of the added window
	//	className - the name of the Windows type control
	//	exStyle - the exStyle argument passed to the Windows function CreateWindowEx()
	//	style - the style argument passed to the Windows function CreateWindowEx()
	//	extraHeight - increment added to the window size 
	//
	void AddLastWindow(bkWidget *widget,const char *className,unsigned exStyle,unsigned style,int extraHeight = 0);

	//
	// PURPOSE
	//	Adds a Windows window to the pane with the given title and tooltip.  This window is the last 
	//  window in the pane, and no calls to AddWindow() should happen after AddLastWindow() is called. 
	// PARAMS
	//	widget - bank widget of the added window
	//	className - the name of the Windows type control
	//	exStyle - the exStyle argument passed to the Windows function CreateWindowEx()
	//	style - the style argument passed to the Windows function CreateWindowEx()
	//  title - the title to give the window
	//  tooltip - the tooltip to give the window
	//	extraHeight - increment added to the window size 
	//
	void AddLastWindow( bkWidget *widget, const char *className, unsigned exStyle, unsigned style,
		const char* title, const char* tooltip=0, int extraHeight=0 );
	
	// PURPOSE: accessor for the pointer to the inner Windows window of the pane
	// RETURNS: returns a pointer to the Windows inner window
	HWND__* GetInnerHandle() const;
	// PURPOSE: accessor for the pointer to the outer Windows window of the pane
	// RETURNS: returns a pointer to the Windows outer window
	HWND__* GetOuterHandle() const;

	// PURPOSE: accessor for the Windows font used in the Windows widgets
	// RETURNS: a pointer to the Windows font
	HFONT__* GetFont() const;
	// PURPOSE: accessor for the Windows bold font used in the Windows widgets
	// RETURNS: a pointer to the Windows bold font
	HFONT__* GetBoldFont() const;
	// PURPOSE: accessor for the bank widget that is linked to the pane
	// RETURNS: returns a pointer to the bank widget
	bkWidget *GetWidget() const;

	// PURPOSE: accessor for the width of the pane 
	// RETURNS: returns the width of the pane
	int GetWidth() const;
	// PURPOSE: call this to notify the pane that it may need to resize its border.
	void SetNeedsResize();
	// PURPOSE: accessor to see if the pane has been told that it needs a resize
	// RETURNS: returns true if it needs a resize, false if not
	bool GetNeedsResize() const;
	// PURPOSE: resizes the pane's visual window
	void Resize();
	
	//
	// PURPOSE
	//	gets the current state of the pane's window
	// PARAMS
	//	shown - is the pane's window currently visible?
	//	x - the horizontal position of the pane's window
	//	y - the vertical position of the pane's window
	//	width - the horizontal dimension of the pane's window
	//	height - the vertical dimension of the pane's window
	//
	void GetState(bool &shown,int &x,int &y,int &width,int &height) const;
	//
	// PURPOSE
	//	sets the current state of the pane's window
	// PARAMS
	//	shown - is the pane's window currently visible?
	//	x - the horizontal position of the pane's window
	//	y - the vertical position of the pane's window
	//	width - the horizontal dimension of the pane's window
	//	height - the vertical dimension of the pane's window
	//
	void SetState(bool shown,int x,int y,int width,int height);	

private:
#if __WIN32PC
	static rageLRESULT __stdcall WndProcInner(HWND__ *,rageUINT,rageWPARAM,rageLPARAM);
	static rageLRESULT __stdcall WndProcOuter(HWND__ *,rageUINT,rageWPARAM,rageLPARAM);

	void ClampHeight();

public:
	// HACK: This is the handle to the window that is calling bkWidget::WindowMessage(...)
	static struct HWND__* WindowMessageHwnd;

private:
#endif

	int m_Height, m_Width, m_ItemHeight;
	int m_OuterWidth, m_OuterHeight;		// Extents of non-client area (caption, size borders, etc)
	bkWidget *m_Widget;
	HWND__* m_Inner;
	HWND__* m_Outer;
	HFONT__* m_Font;
	HFONT__* m_BoldFont;
	HWND__* m_Tooltip;
	bool m_IsMaster;
	bool m_NeedsResize;
};

inline HWND__* bkPane::GetInnerHandle() const 
{ 
	return m_Inner; 
}

inline HWND__* bkPane::GetOuterHandle() const 
{ 
	return m_Outer; 
}

inline HFONT__* bkPane::GetFont() const 
{ 
	return m_Font; 
}

inline HFONT__* bkPane::GetBoldFont() const 
{ 
	return m_BoldFont; 
}

inline bkWidget* bkPane::GetWidget() const 
{
	return m_Widget; 
}

inline int bkPane::GetWidth() const 
{ 
	return m_Width; 
}

inline void bkPane::SetNeedsResize()		
{
	m_NeedsResize=true;
}

inline bool bkPane::GetNeedsResize() const 
{
	return m_NeedsResize;
}

// setting to force widget windows to always be visibly on top of all other windows
extern bool bkAlwaysOnTop;

}	// namespace rage

#endif // __BANK

#endif
