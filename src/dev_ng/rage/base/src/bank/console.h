// 
// bank/console.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BANK_CONSOLE_H
#define BANK_CONSOLE_H

#include "atl/atfunctor.h"
#include "atl/map.h"
#include "bank/packet.h"
#include "file/handle.h"
#include "string/string.h"
#include "system/ipc.h"
#include "atl/string.h"

#if !__FINAL

namespace rage {

// Macro to implement help text within the function
#define CONSOLE_HELP_TEXT(txt)	if (numArgs==1&&!strcmp(args[0],"-help")) { output(txt); return; }

#define CONSOLE_SCRIPT_WRAPPER(txt)	if (numArgs==1&&!strcmp(args[0],"-scriptwrapper")) { output(txt); return; }

// Macro to set the command text script

// Macro to get the arg list exactly correct in case we have to change in the future
#define CONSOLE_ARG_LIST		const char** args,int numArgs,bkConsole::OutputFunctor output
#define UNUSED_CONSOLE_ARG_LIST const char** /*args*/,int /*numArgs*/,bkConsole::OutputFunctor output

class bkConsole
{
public:
	// PURPOSE: constructor.
	bkConsole( bool usePowerConsole = false );

    // PURPOSE: Initializes the single instance of bkConsole
    // PARAMS:
    //    usePowerConsole - whether or not to use the powershell
    static void InitClass( bool usePowerConsole = false );

    // PURPOSE: Shutsdown the single instance of bkConsole
    static void ShutdownClass();

	// PURPOSE: Accessor for the singleton instance.
	// RETURNS: Returns a pointer singleton instance.
	static bkConsole* GetInstance();

	// PURPOSE: waits for a connection to the PC side.
	void WaitForConnection();

	// PURPOSE: Reads commands from the PC side console and executes the corresponding functors.
	void Update();

	// PURPOSE:
	//	Functor that the client application can use to send output to the console.
	typedef atFunctor1<void,const char*> OutputFunctor;

	// PURPOSE:
	//	This is the functor that is called back once a command has been found.
	//	It takes an array of arguments to the command as the first argument,
	//  and the second argument is the count of arguments in the first parameter.
	typedef atFunctor3<void,const char**,int,OutputFunctor> CommandFunctor;

	// PURPOSE:
	//	Adds a command to the console.  
	// PARAMETERS:
	//	commandName - the name of the command to add
	//	callback - the functor that gets executed if the command is found
	static void AddCommand(const char* commandName,CommandFunctor callback);

	// PURPOSE:
	//	Removes a command to the console.  
	// PARAMETERS:
	//	commandName - the name of the command to remove 
	static void RemoveCommand(const char* commandName);

	void ExecuteString(const char *msg);
private:
	void ReadAndExecuteCommand();
	void PSReadAndExecuteCommand();
	void SendOutput(const char* output);
	void Help(CONSOLE_ARG_LIST);
	void Alias(CONSOLE_ARG_LIST);
	void Widget(CONSOLE_ARG_LIST);

	enum {
#if __DEV | __BANK
		RAG_CONSOLE_PORT_OFFSET	= bkRemotePacket::SOCKET_OFFSET_USER5 + 1,
#endif // __DEV | __BANK,
		CONSOLE_PORT = 5050
	};

	atMap<ConstString,CommandFunctor>	m_Map;			// TODO: Consider a u32 hash here
	fiHandle							m_TcpHandle;
	sysIpcSema							m_SemaConnect;

	bool								m_usePowerConsole;
	static bkConsole*					sm_Instance;

	bool								m_outputSent;
};

inline void bkConsole::InitClass( bool usePowerConsole )
{
    bkAssertf( sm_Instance == NULL, "Only one bkConsole instance is allowed" );
    sm_Instance = rage_new bkConsole( usePowerConsole );
}

inline void bkConsole::ShutdownClass()
{
    bkAssertf( sm_Instance, "Can't shutdown bkConsole, it was never initted (or already shut down)" );
    delete sm_Instance;
    sm_Instance = NULL;
}

inline bkConsole* bkConsole::GetInstance()
{
	return sm_Instance;
}

} // namespace rage

#endif

#endif
