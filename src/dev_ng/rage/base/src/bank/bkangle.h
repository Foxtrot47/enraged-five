//
// bank/bkangle.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_BKANGLE_H
#define BANK_BKANGLE_H

#include "packet.h"
#include "widget.h"

#include "vector/vector2.h"

#if __BANK && !__SPU

namespace rage 
{

class Vector2;

class bkAngle : public bkWidget
{
public:
	typedef ::rage::bkAngleType::Enum	EAngleType;

protected:
	friend class bkGroup;
	friend class bkManager;
	bkAngle( datCallback &callback, const char *title, const char *memo, float *data, EAngleType angleType, float min=-FLT_MAX / 1000.0f, float max=FLT_MAX / 1000.0f, const char *fillColor=NULL, bool readOnly=false );
	bkAngle( datCallback &callback, const char *title, const char *memo, Vector2 *data, float min=-FLT_MAX / 1000.0f, float max=FLT_MAX / 1000.0f, const char *fillColor=NULL, bool readOnly=false );
	~bkAngle();

	static inline int GetStaticGuid();
	inline int GetGuid() const;
	void Update();

	static void RemoteHandler( const bkRemotePacket& packet );

public:
    EAngleType GetAngleType() const;

    float GetDegrees() const;
    void SetDegrees( float f );

    float GetFraction() const;
    void SetFraction( float f );

    float GetRadians() const;
    void SetRadians( float f );

    const Vector2& GetVector() const;
    void SetVector( const Vector2 &v );

    // PURPOSE: Retrieves a string representation of the current value of this widget.
    // PARAMS:
    //    buf - the buffer to write the string representation to.
    //    bufLen - the length of the buffer.
    virtual void GetStringRepr( char *buf, int bufLen );

    // PURPOSE: Sets the current value of this widget using the string representation of the new value.
    // PARAMS:
    //    buf - the new value.
    virtual void SetStringRepr( const char *buf );

protected:
	void RemoteCreate();
	void RemoteUpdate();

#if __WIN32PC
	void WindowCreate();
	rageLRESULT WindowMessage( rageUINT, rageWPARAM, rageLPARAM );
#endif

private:
	EAngleType m_angleType;

	union AngleValue
	{
		float* f;
		Vector2* v2;
	};	
	
	AngleValue m_value;
	AngleValue m_prevValue;
	float m_minValue;
	float m_maxValue;

    static Vector2 sm_tempReturnVector;
};

inline int bkAngle::GetStaticGuid()
{
	return BKGUID('a','n','g','l');
}

inline int bkAngle::GetGuid() const
{
	return GetStaticGuid();
}

inline bkAngle::EAngleType bkAngle::GetAngleType() const
{
    return m_angleType;
}

#if __WIN32PC
inline void bkAngle::WindowCreate()
{

}

inline rageLRESULT bkAngle::WindowMessage( rageUINT, rageWPARAM, rageLPARAM )
{
	return 0;
}
#endif
}	// namespace rage

#endif // __BANK

#endif
