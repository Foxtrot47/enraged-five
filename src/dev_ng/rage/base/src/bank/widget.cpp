//
// bank/widget.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "widget.h"

#include "bank.h"
#include "io.h"
#include "pane.h"

#include "string/string.h"
#include "system/xtl.h"

#include <stdarg.h>
#include <stdio.h>

/*
	What functions to call when:
	- Callbacks should only be called as a result of user input.  Therefore, if a remote
	  server calls RemoteUpdate, it will always be a result of user input.  As long as
	  callbacks are always called in the RemoteHandler, m_Command==CHANGED case, we will
	  have correct behavior.  Callbacks will get invoked on the server as a result of
	  changes made on the client, but there are no callbacks on the remote server anyway.
	- Callbacks should be called in response to local widget input or Windows control
	  manupulation via the Changed() function.  Changed() should *never* be called during
	  a normal Update just because we detected that the variable is different from our
	  cached value; however, RemoteUpdate should be called *whenever* we detect a value changing.

	Still to do:
	- Save and load bank layout?
*/


using namespace rage;

bkWidget::bkWidget(datCallback &callback,const char *title,const char *memo, const char* fillColor, bool readOnly) : 
	m_Next(0),
	m_Parent(0),
	m_Child(0),
	m_Title(ConstStringDuplicate(title)),
	m_ExtraData(NULL)
{
#if __WIN32PC
	m_Hwnd = 0;
#endif

	if (!(callback == NullCB) ||
		memo != NULL || 
		fillColor != NULL ||
		readOnly == true)
	{
		m_ExtraData = rage_new ExtraData;
		m_ExtraData->m_Callback = callback;
		m_ExtraData->m_Tooltip = ConstStringDuplicate(memo);
		m_ExtraData->m_FillColor = ConstStringDuplicate(fillColor);
		m_ExtraData->m_ReadOnly = readOnly;
	}

	// bkDisplayf("bkWidget::bkWidget(%s)",m_Title);

	m_RemoteToLocalNode.m_key = (u32)bkRemoteWidgetMap::INVALID_ID;
}



bkWidget::~bkWidget()
{
	bkRemotePacket::UnsetWidgetId(*this);

	if (sm_Focus == this)
		sm_Focus = 0;

	// bkDisplayf("delete %p [%s]",this,(const char*)m_Title);

	Detach();

	while (m_Child)
		delete m_Child;

	bkAssertf(!m_Child, "Widget %s still has children, wasn't removed from tree", m_Title);
	bkAssertf(!m_Parent, "Widget %s still has a parent, wasn't removed from tree", m_Title);
	bkAssertf(!m_Next, "Widget %s still has a sibling, wasn't removed from tree", m_Title);

	// bkDisplayf("bkWidget::~bkWidget(%s)",m_Title);
	ConstStringFree(m_Title);
	if (m_ExtraData)
	{
		ConstStringFree(m_ExtraData->m_Tooltip);
		ConstStringFree(m_ExtraData->m_FillColor);
		delete m_ExtraData;
	}
}

void bkWidget::Print(int indent) {
	for (int i=0; i<indent; i++)
		Printf(" ");
	Displayf("[%s]",m_Title);
	if (m_Child)
		m_Child->Print(indent + 2);
	if (m_Next)
		m_Next->Print(indent);
}

void bkWidget::Detach() {
	if (!m_Parent)
		return;

	bkWidget **c = &m_Parent->m_Child;
	while (*c != this && *c)
		c = &(*c)->m_Next;
	bkAssertf(*c, "Can't find widget %s from it's parent", m_Title);
	*c = m_Next;
	m_Parent = m_Next = 0;
}

bkWidget& bkWidget::AddChild(bkWidget& newChild) {
	bkAssertf(!newChild.m_Parent, "New child %s already has a parent (%s)", newChild.m_Title, newChild.m_Parent->m_Title);
	// newChild->Detach();
	bkWidget **c = &m_Child;
	while (*c)
		c = &(*c)->m_Next;
	*c = &newChild;
	newChild.m_Parent = this;

#if __WIN32PC
	if (sm_EnableWindows && !IsClosedGroup())
		newChild.WindowCreate();
#endif

	return newChild;
}

bkWidget* bkWidget::FindChild(const char *childTitle) const
{
	bkWidget *w = GetChild();
	while ( w ) 
	{
		if ( stricmp( w->GetTitle(), childTitle ) == 0 )
		{
			return w;
		}

		w = w->GetNext();
	}

	return NULL;
}

void bkWidget::SetFillColor( const char* fillColor )
{
	// Don't update anything if the color doesn't change.
	if (GetFillColor() && !strcmp(GetFillColor(), fillColor))
	{
		// Yup - no need to update.
		return;
	}

	if (!m_ExtraData)
	{
		m_ExtraData = rage_new ExtraData;
	}

	if ( m_ExtraData->m_FillColor != NULL )
	{
		ConstStringFree( m_ExtraData->m_FillColor );
	}

	m_ExtraData->m_FillColor = ConstStringDuplicate( fillColor );
	
	if ( !bkRemotePacket::IsConnected() )
	{
		return;
	}

	bkRemotePacket p;
	p.Begin(bkRemotePacket::FILL_COLOR,GetGuid(),this);
	p.Write_const_char( m_ExtraData->m_FillColor );
	p.Send();
}

void bkWidget::SetReadOnly( bool value )
{
	// No need to update anything if the value hasn't changed.
	if (IsReadOnly() && value)
	{
		return;
	}

	if (!m_ExtraData)
	{
		m_ExtraData = rage_new ExtraData;
	}

	m_ExtraData->m_ReadOnly = value;

	if ( !bkRemotePacket::IsConnected() )
	{
		return;
	}

	bkRemotePacket p;
	p.Begin(bkRemotePacket::READ_ONLY,GetGuid(),this);
	p.Write_bool( m_ExtraData->m_ReadOnly );
	p.Send();
}

int bkWidget::GetNumWidgets(bool countOpenOnly) const {
	int count = 1;
	bkWidget *i = m_Child;
	while (i && (!countOpenOnly || IsOpen())) {
		count += i->GetNumWidgets(countOpenOnly);
		i = i->GetNext();
	}
	return count;
}

void bkWidget::RemoteCreate()
{
	if ( sm_setWidgetId || !bkRemotePacket::WidgetHasId( *this ) )
	{
		bkRemotePacket::SetNewWidgetId( *this );
	}
}

int bkWidget::sm_Cursor;
int bkWidget::sm_MaxCursorDraw;
int bkWidget::sm_MaxCursorUpdate;
int bkWidget::sm_TopLine;
int bkWidget::sm_ShownLineCount = 10;
bkWidget* bkWidget::sm_Focus;
bool bkWidget::sm_setWidgetId = true;

#if __WIN32PC
#if __TOOL
bool bkWidget::sm_EnableWindows = false; //This would be best tied to a command line option
#else
bool bkWidget::sm_EnableWindows = true;
#endif
#endif

void bkWidget::Drawf(int x,int y,const char *fmt,...) const {
	if (y < 0 || y >= sm_ShownLineCount)
		return;

	char buf[512];
	va_list args;
	va_start(args,fmt);
	vsprintf(buf,fmt,args);
	if (bkIo::GetInstance())
		bkIo::GetInstance()->Draw(x,y,buf);
	va_end(args);
}



void bkWidget::Update() {
}



int bkWidget::DrawLocal(int x,int y) {
	if (y == sm_Cursor - sm_TopLine) {
		Drawf(x-2,y,">>");
		sm_Focus = this;
	}
	if (y >= sm_MaxCursorDraw - sm_TopLine)
		sm_MaxCursorDraw = y + sm_TopLine;

	return y+1;
}


void bkWidget::Changed() {
	if (m_ExtraData)
	{
		m_ExtraData->m_Callback.Call(this);
	}
}



void bkWidget::Message(Action /*action*/,float /*value*/) {
}


bool bkWidget::IsOpen() const {
	bkAssertf(m_Parent, "Couldn't find parent for widget %s", m_Title);
	return m_Parent->IsOpen();
}

void bkWidget::SetClosed() {
}

bool bkWidget::AreAnyWidgetsShown() const {
	return false;
}


bool bkWidget::IsClosedGroup() const {
	return false;
}

void bkWidget::Rebuild()
{
#if __WIN32PC
	SetClosed();

	if ( sm_EnableWindows )
	{
		WindowDestroy();
	}
#endif

	FinishCreate();

	bkWidget *i = m_Child;
	while ( i ) 
	{
		i->Rebuild();
		i = i->GetNext();
	}
}

void bkWidget::RemoteDestroy() {
	bkRemotePacket p;
	p.Begin(bkRemotePacket::DESTROY,GetGuid(),this);
	p.Send();
}


void bkWidget::RemoteUpdate() {
}


bool bkWidget::SetFocus() { 
	sm_Focus = this;
	return false;
}


void bkWidget::Destroy()
{
	// make sure we consume any stale messages that may have been intended for us

	if (bkRemotePacket::IsConnected())
	{
		bkRemotePacket::ReceivePackets();
		RemoteDestroy();
	}

	InternalDestroy();
	delete this;
}

void bkWidget::InternalDestroy() {
	// bkDisplayf("Destroy %p [%s]",this,(const char*)m_Title);

	SetClosed();

	Detach();

#if __WIN32PC
	if (sm_EnableWindows)
		WindowDestroy();
#endif

	if (!bkRemotePacket::IsServer()) {

		while (m_Child)
		{
			bkWidget* child = m_Child;
			child->InternalDestroy();
			delete child;
		}
	}

}


bkWidget& bkWidget::FinishCreate() {
	if (bkRemotePacket::IsConnected())
		RemoteCreate();
#if __WIN32PC
	if (GetPane())
		GetPane()->SetNeedsResize();
#endif
	return *this;
}


void bkWidget::FinishUpdate() {
	if (bkRemotePacket::IsConnected())
		RemoteUpdate();
#if __WIN32PC
	WindowUpdate();
#endif
}


#if __WIN32PC
void bkWidget::WindowUpdate() {
}


int bkWidget::WindowResize(int x,int y,int width,int height) {
	if (m_Hwnd)
		SetWindowPos(m_Hwnd,0,x,y,width,height,SWP_NOACTIVATE | SWP_NOZORDER);
	return y+height;
}


void bkWidget::WindowDestroy() {
	if (m_Hwnd)
		::DestroyWindow(m_Hwnd);
	m_Hwnd = 0;
}

bkPane* bkWidget::GetPane() {
	return m_Parent? m_Parent->GetPane() : 0;
}


void bkWidget::WindowCreateAllKids() {
	bkWidget *i = GetChild();
	while (i) {
		if (!i->IsClosedGroup())
			i->WindowCreateAllKids();
		i->WindowCreate();
		i = i->GetNext();
	}
}

void bkWidget::WindowDestroyAllKids() {
	bkWidget *i = GetChild();
	while (i) {
		i->WindowDestroyAllKids();
		i->WindowDestroy();
		i = i->GetNext();
	}
}

#endif

void bkWidget::AddStaticWidgets(bkBank& bank)
{
	bank.AddSlider("Lines Displayed", &bkWidget::sm_ShownLineCount, 5, 27, 1);
}

#endif
