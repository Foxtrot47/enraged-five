// 
// bank/channel.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BANK_CHANNEL_H 
#define BANK_CHANNEL_H 

#include "diag/channel.h"

// DOM-IGNORE-BEGIN
RAGE_DECLARE_CHANNEL(Bank)
#define bkAssertf(cond,fmt,...)			RAGE_ASSERTF(Bank,cond,fmt,##__VA_ARGS__)
#define bkVerifyf(cond,fmt,...)			RAGE_VERIFYF(Bank,cond,fmt,##__VA_ARGS__)
#define bkErrorf(fmt,...)					RAGE_ERRORF(Bank,fmt,##__VA_ARGS__)
#define bkWarningf(fmt,...)				RAGE_WARNINGF(Bank,fmt,##__VA_ARGS__)
#define bkDisplayf(fmt,...)				RAGE_DISPLAYF(Bank,fmt,##__VA_ARGS__)
#define bkDebugf1(fmt,...)					RAGE_DEBUGF1(Bank,fmt,##__VA_ARGS__)
#define bkDebugf2(fmt,...)					RAGE_DEBUGF2(Bank,fmt,##__VA_ARGS__)
#define bkDebugf3(fmt,...)					RAGE_DEBUGF3(Bank,fmt,##__VA_ARGS__)
#define bkLogf(severity,fmt,...)			RAGE_LOGF(Bank,severity,fmt,##__VA_ARGS__)
#define bkCondLogf(cond,severity,fmt,...)	RAGE_CONDLOGF(cond,Bank,severity,fmt,##__VA_ARGS__)
// DOM-IGNORE-END


#endif // BANK_CHANNEL_H 
