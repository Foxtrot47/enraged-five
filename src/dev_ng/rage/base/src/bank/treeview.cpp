// 
// bank/treeview.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#define USE_STOCK_ALLOCATOR
#if __WIN32PC
#include "tree.h"

#include "file/stream.h"
#include "string/string.h"
#include "system/main.h"
#include "system/param.h"
#include "system/xtl.h"

#include <stdio.h>

using namespace rage;


#pragma comment(lib,"comctl32.lib")

PARAM(sortbyname,"Sort by name instead of size");

struct Directory {
	Directory(const char *root,Directory *sibling);
	~Directory() { delete Next; delete Child; StringFree(Name); }
	void LogToFile(const char *name);
	void RecurseLogToFile(int indent);
	const char *Name;
	Directory *Next, *Child;
	__int64 Size, LocalSize;
	static bkTreeViewer *sm_Viewer;
	static fiStream *sm_OutLog;
};

bkTreeViewer* Directory::sm_Viewer;

fiStream* Directory::sm_OutLog;

static int cmpDir(const void *a,const void *b) {
	Directory *da = *(Directory**)a;
	Directory *db = *(Directory**)b;
	if (PARAM_sortbyname.Get()) {
		return strcasecmp(da->Name,db->Name);
	}
	else if (da->Size > db->Size)
		return -1;
	else if (da->Size < db->Size)
		return +1;
	else
		return 0;
}

void Directory::LogToFile(const char *name) {
	sm_OutLog = fiStream::Create(name);
	if (sm_OutLog) {
		RecurseLogToFile(0);
		sm_OutLog->Close();
		sm_OutLog = 0;
	}
}


void Directory::RecurseLogToFile(int indent) {
	for (int i=0; i<indent; i++)
		fputc(32, sm_OutLog);
	fprintf(sm_OutLog,"%s\r\n",Name);
	Directory *d = Child;
	while (d) {
		d->RecurseLogToFile(indent+2);
		d = d->Next;
	}
}

Directory::Directory(const char *root,Directory *sibling) {
	Next = sibling;
	Child = 0;
	Size = 0;
	LocalSize = 0;

	WIN32_FIND_DATA data;
	char filespec[1024];

	safecpy(filespec,root);
	safecat(filespec,"\\*");

	// collect all subdirectories
	HANDLE h = FindFirstFile(filespec,&data);
	if (h != INVALID_HANDLE_VALUE) {
		do {
			if (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				if (data.cFileName[0]=='.' && (data.cFileName[1]==0 || (data.cFileName[1]=='.' && data.cFileName[2]==0)))
					continue;
				char temp[1024];
				safecpy(temp,root);
				safecat(temp,"\\");
				safecat(temp,data.cFileName);
				Child = rage_new Directory(temp,Child);
				Size += Child->Size;
			}
			Size += data.nFileSizeLow + ((__int64)data.nFileSizeHigh << 32);
			LocalSize += data.nFileSizeLow + ((__int64)data.nFileSizeHigh << 32);
		} while (FindNextFile(h,&data));
		FindClose(h);
	}

	// Sort kids
	int count = 0;
	Directory *i = Child;
	while (i) {
		++count;
		i = i->Next;
	}
	if (count) {
		Directory **array = rage_new Directory*[count];
		count = 0;
		i = Child;
		while (i) {
			array[count++] = i;
			i = i->Next;
		}
		qsort(array,count,sizeof(Directory*),cmpDir);
		int j;
		Child = array[0];
		for (j=0; j<count; j++)
			array[j]->Next = j<count-1? array[j+1] : 0;
		delete[] array;
	}

	// Make label for self
	formatf(filespec,Child? "%s: %I64u MB [%I64u MB local + %I64u MB in subdirs]" : "%s: %I64u MB" ,root,(Size + 1023 * 1024) >> 20,(LocalSize + 1023 * 1024) >> 20,
		((Size + 1023 * 1024) >> 20) - ((LocalSize + 1023 * 1024) >> 20));
	Name = StringDuplicate(filespec);

	if (sm_Viewer)
		sm_Viewer->SetTitle(root);
}


static void* Callback(void *context,int cmd,void * /*user*/) {
	Directory *D = (Directory*) context;

	switch (cmd) {
	case BKTV_GETLABEL: return (void*) D->Name;
	case BKTV_GETCHILD: return (void*) D->Child;
	case BKTV_GETNEXT: return (void*) D->Next;
	}
	return 0;

}

#if __WIN32PC
#include <CommCtrl.h>

void sCopyTextToClipboard(const char *text)
{
	// the clipboard becomes the owner of this memory, do not free it
	if(OpenClipboard(0))
	{
		HGLOBAL clipbuffer;
		char * buffer;
		EmptyClipboard();
		int allocSize = strlen(text)+1;
		clipbuffer = GlobalAlloc(GMEM_DDESHARE, allocSize);
		buffer = (char*)GlobalLock(clipbuffer);
		safecpy(buffer, LPCSTR(text), allocSize);
		GlobalUnlock(clipbuffer);
		SetClipboardData(CF_TEXT,clipbuffer);
		CloseClipboard();
	}
}

void* TreeViewerCallback(void *userdata,int cmd,void *user)
{
	char selectionLabel[255];
	if(cmd==BKTV_KEYDOWN)
	{
		bkTreeViewer *TV = (bkTreeViewer *)userdata;
		static bool control_pressed = false;
		static bool c_pressed = false;
		UINT key = *(UINT*)user;
				  
		// technically, this is responding to control then c, not the two simultaneously
		// (sticky keys ftw)
		c_pressed = (key==1179715);
		if(control_pressed && c_pressed)
		{
			if(TV)
			{
				TV->GetCurSelectionLabel(selectionLabel, sizeof(selectionLabel));
				safecat(selectionLabel,"\r\n");
				sCopyTextToClipboard(selectionLabel);
			}
		}
		control_pressed = (key==1179665);
	}
	return NULL;
}

#endif

int Main()
{
	const char *root = sysParam::GetArgCount() > 1? sysParam::GetArg(1) : "c:";

	if (sysParam::GetArgCount() > 1 && (!strcmp(sysParam::GetArg(1),"/?") || !strcmp(sysParam::GetArg(1),"-h") || !strcmp(sysParam::GetArg(1),"-help"))) {
		MessageBox(NULL,
			"Usage: TreeView [ dirname [outfile] | infile ] [-sortbyname] \n\n"
			"If directory name is specified, disk usage is computed from that point downward.\n\n"
			"If outfile is specified the summary is written to that file (which can be saved\n"
			"for future reference or quickly reloaded into treeview again).\n\n"
			"If infile is specified, that file is displayed in a collapsable treeview.\n\n"
			"If run with no parameters, default is to check C:\\ root directory."
			,
			"TreeView", MB_OK);
		return 0;
	}

	fiStream *input = fiStream::Open(root);
	if (input) {
#if __WIN32PC
		char dateTimeString[256];
		FILETIME lastWriteTime;
		SYSTEMTIME stUTC, stLocal;
		::GetFileTime(input->GetLocalHandle(),NULL,NULL,&lastWriteTime);
		// Convert the last-write time to local time.
		FileTimeToSystemTime(&lastWriteTime, &stUTC);
		SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

		const char *months[13] = { "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
		// Build a string showing the date and time.
		wsprintf(dateTimeString, "%s - %02d %s %d  %02d:%02d",
			root, stLocal.wDay, months[stLocal.wMonth], stLocal.wYear,
			stLocal.wHour, stLocal.wMinute);
		root = dateTimeString;
#endif

		bkTreeViewer TV(root,100,100,400,600,true);
		TV.SetFont("Courier New",11);
		TV.MakeTreeFromStream(*input);
		input->Close();
		TV.SetTitle(root);
#if __WIN32
		TV.SetCallback(TreeViewerCallback);
		TV.SetUserData((void*)&TV);
		MSG msg;
		while (TV.IsAlive() && ::GetMessage(&msg,0,0,0)) {
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
#endif
	}
	else {
		Directory DIRECTORY(root,0);
		if (sysParam::GetArgCount() > 2 && sysParam::GetArg(2)[0] != '-')
			DIRECTORY.LogToFile(sysParam::GetArg(2));
		else {
			bkTreeViewer TV(root,100,100,400,600,true);
			TV.SetFont("Courier New",11);
			TV.MakeTree(Callback,&DIRECTORY);
			TV.SetTitle(root);
#if __WIN32
			MSG msg;
			while (TV.IsAlive() && ::GetMessage(&msg,0,0,0)) {
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
#endif
		}
	}


	return 0;
}
#else
#include "system/main.h" //RAGE_QA: INCLUDE_OK

int Main()
{
	return 0;
}
#endif
