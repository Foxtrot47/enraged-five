//
// bank/imageviewer.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_IMAGEVIEWER_H
#define BANK_IMAGEVIEWER_H

#if __BANK

#include "widget.h"

namespace rage {
class bkImageViewer : public bkWidget {
	friend class bkGroup;
	friend class bkManager;
public:
	//
	// PURPOSE
	//	Sets the current image that will be displayed by sending
	//  a remote message to RAG.
	// PARAMS
	//	imageFileName - the name of the image file.
	//
	void SetImage(const char* imageFileName);

    //
    // PURPOSE
    //	Gets the current image's filename.
    // RETURNS
    //	the name of the image file.
    //
    const char* GetImage() const;
	
    // PURPOSE: Retrieves a string representation of the current value of this widget.
    // PARAMS:
    //    buf - the buffer to write the string representation to.
    //    bufLen - the length of the buffer.
    virtual void GetStringRepr( char *buf, int bufLen );

    // PURPOSE: Sets the current value of this widget using the string representation of the new value.
    // PARAMS:
    //    buf - the new value.
    virtual void SetStringRepr( const char *buf );

protected:
	//
	// PURPOSE
	//	constructor.
	// PARAMS
	//  title - the name of this widget.
	//  memo - the help description for this widget.
	//
	bkImageViewer(const char *title,const char *memo, const char *fillColor=NULL);

	// PURPOSE
	//  destructor.
	~bkImageViewer();

	// PURPOSE: Accessor for the GUID of this widget type.
	// RETURNS: The integer representation of the 4 letter GUID string.
	static int GetStaticGuid() { return BKGUID('i','m','g','v'); }


	// PURPOSE: Member function that just calls GetStaticGuid().
	// RETURNS: Returns the results from GetStaticGuid().
	int GetGuid() const;

	//
	// PURPOSE
	//	This function handles the communication between the 
	//  application and the widget type.
	// PARAMS
	//	p - the current packet of information sent by RAG.
	//
	static void RemoteHandler(const bkRemotePacket& p);

	// PURPOSE: Sends create messages to RAG to tell it to create an instance of this widget type.
	void RemoteCreate();

#if __WIN32PC
	// These must be declared and defined - but we define them to nothing
	// since this widget isn't support in the old bank widgets.
	void WindowCreate() {}
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM) {return 0;}
#endif

private:
	// declare these but don't define these so that the compiler doesn't complain:
	bkImageViewer();
	const bkImageViewer& operator=(bkImageViewer&) {return *this;}

    char *m_pImageFilename;
};

inline const char* bkImageViewer::GetImage() const
{
    return m_pImageFilename;
}

inline void bkImageViewer::SetStringRepr( const char *buf )
{
    SetImage( buf );
}

}	// namespace rage

#endif

#endif
