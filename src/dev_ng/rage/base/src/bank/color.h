//
// bank/color.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_COLOR_H
#define BANK_COLOR_H

#if __BANK

#include "widget.h"

#include "math/float16.h"
#include "vector/color32.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

#if __WIN32
#pragma warning(push)
#pragma warning(disable:4324)
#endif

namespace rage {

	class Vector3;
	class Vector4;
	class Color32;

class bkColor: public bkWidget {
	friend class bkGroup;
	friend class bkManager;
public:
    enum EColorType
    {
        COLOR_TYPE_VECTOR3,
        COLOR_TYPE_VECTOR4,
        COLOR_TYPE_COLOR32,
        COLOR_TYPE_FLOAT3,
		COLOR_TYPE_FLOAT4,
		COLOR_TYPE_FLOAT163,
		COLOR_TYPE_FLOAT164,
    };

	int GetAlpha() const;
    void SetAlpha( int alpha );

	int GetRed() const;
    void SetRed( int red );

	int GetGreen() const;
    void SetGreen( int green );

	int GetBlue() const;
    void SetBlue( int blue );

	float GetAlphaf() const;
    void SetAlphaf( float alpha );

	float GetRedf() const;
    void SetRedf( float red );

	float GetGreenf() const;
    void SetGreenf( float green );

	float GetBluef() const;
    void SetBluef( float blue );

    void Get( int &red, int &green, int &blue, int &alpha ) const;
    void Set( int red, int green, int blue, int alpha );

    void Getf( float &red, float &green, float &blue, float &alpha ) const;
    void Setf( float red, float green, float blue, float alpha );

    // PURPOSE: Retrieves a string representation of the current value of this widget.
    // PARAMS:
    //    buf - the buffer to write the string representation to.
    //    bufLen - the length of the buffer.
    virtual void GetStringRepr( char *buf, int bufLen );

    // PURPOSE: Sets the current value of this widget using the string representation of the new value.
    // PARAMS:
    //    buf - the new value.
    virtual void SetStringRepr( const char *buf );

protected:
	bkColor(Vector3& vector,datCallback cb,const char *title,const char *memo, const char *fillColor=NULL, bool readOnly=false, bool inOutLinear = false);
	bkColor(Vector4& vector,datCallback cb,const char *title,const char *memo, const char *fillColor=NULL, bool readOnly=false, bool inOutLinear = false);
	bkColor(Color32& color,datCallback cb,const char *title,const char *memo, const char *fillColor=NULL, bool readOnly=false, bool inOutLinear = false);
	bkColor(float* pFloat,int arraySize,datCallback cb,const char *title,const char *memo, const char *fillColor=NULL, bool readOnly=false, bool inOutLinear = false);
	bkColor(Float16* pFloat16,int arraySize,datCallback cb,const char *title,const char *memo, const char *fillColor=NULL, bool readOnly=false, bool inOutLinear = false);
	~bkColor();

	void Message(Action action,float value);
	int DrawLocal(int x,int y);
	static void RemoteHandler(const bkRemotePacket& p);

	static int GetStaticGuid() { return BKGUID('v','c','l','r'); }
	int GetGuid() const;

	void Update();

    enum EColorComponent
    {
        ALPHA_COMPONENT,
        RED_COMPONENT,
        GREEN_COMPONENT,
        BLUE_COMPONENT
    };

	int GetComponent( EColorComponent component ) const;
	float GetComponentf( EColorComponent component ) const;

	bool SetComponent( EColorComponent component, int value );
	bool SetComponentf( EColorComponent component, float value );

protected:
#if __WIN32PC
	void WindowCreate();
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM);
#endif
	void RemoteCreate();
	void RemoteUpdate();

	int m_drawLocalExpanded;

	struct VECTOR_ALIGN PrevValue
	{
	public:
		// *sigh* Vector3s and Vector4s don't work in unions, so this is 
		// slightly hacky:
		union
		{
			float	m_Vector[4];
			u32		m_Color32;
		};
	}
#if __PPU && VECTORIZED
	
#endif
	;

	PrevValue	m_PrevValue;

	EColorType m_colorType;
	union
	{
		Vector3*	m_Vector3;
		Vector4*	m_Vector4;
		Color32*	m_Color32;
		float*	m_floats;
		Float16* m_float16s;
	};

	bool m_inOutLinear;


	CompileTimeAssert(sizeof(Color32)==sizeof(u32));

private:
	void RemoteReadColor(const bkRemotePacket& packet);
	void RemoteWriteColor(bkRemotePacket& packet);

};

#if __WIN32
#pragma warning(pop)
#endif

}	// namespace rage
#endif

#endif
