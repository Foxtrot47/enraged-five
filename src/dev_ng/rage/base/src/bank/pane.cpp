//
// bank/pane.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#if __BANK 

#include "pane.h"
#include "bank.h"
#include "system/param.h"
#include "system/bootmgr.h"

#if __WIN32PC
#pragma comment(lib,"comctl32.lib")
#include "system/xtl.h"
#pragma warning(disable: 4668)
#include <commctrl.h>
#pragma warning(error: 4668)

using namespace rage;

PARAM(hidewidgets,"[bank] Hides bank widgets");

static int s_Creates;
HWND bkPane::WindowMessageHwnd = NULL;


LRESULT WINAPI bkPane::WndProcInner(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) 
{
	if ( ((msg == WM_COMMAND) || (msg == WM_VSCROLL)) && lParam )
	{
		bkWidget *widget = (bkWidget*) GetWindowLongPtr( (HWND)lParam, GWLP_USERDATA );
		if ( widget ) 
		{
			// bkDisplayf("Sending (%x,%x,%x) to %s",msg,wParam,lParam,widget->GetTitle());
		
			bkPane::WindowMessageHwnd = hwnd;
			widget->WindowMessage( msg, wParam, lParam );
		}
	}

	return DefWindowProc( hwnd, msg, wParam, lParam );
}


LRESULT WINAPI bkPane::WndProcOuter( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam ) 
{
	// This isn't going to be valid during WM_CREATE...
	bkPane *pane = (bkPane*) GetWindowLongPtr( hwnd, GWLP_USERDATA );

	if ( msg == WM_CLOSE ) 
	{
		bkPane::WindowMessageHwnd = hwnd;
		pane->GetWidget()->WindowMessage( msg, wParam, lParam );
		return 0L;
	}
	else if (msg == WM_SIZING) {
		RECT *rect = (RECT*) lParam;
		if (rect->bottom - rect->top > pane->m_Height + pane->m_OuterHeight) {
			if (wParam == WMSZ_TOPLEFT || wParam == WMSZ_TOP || wParam == WMSZ_TOPRIGHT)
				rect->top = rect->bottom - pane->m_Height - pane->m_OuterHeight;
			else
				rect->bottom = rect->top + pane->m_Height + pane->m_OuterHeight;
		}
	}
	else if (msg == WM_SIZE && pane) {
		pane->m_Width = LOWORD(lParam);		// Client area, outer width already subtracted out
		pane->Resize();
		return 0L;
	}
	else if (msg == WM_VSCROLL) {
		if (LOWORD(wParam) == SB_THUMBPOSITION || LOWORD(wParam) == SB_THUMBTRACK) {
			SetScrollPos(hwnd,SB_VERT,HIWORD(wParam),TRUE);
			SetWindowPos(pane->m_Inner,0,0,-HIWORD(wParam),0,0,SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);
		}
	}
	else if (msg == 0x20A /*WM_MOUSEWHEEL*/) {
		int delta = ((s16)HIWORD(wParam));
		int scrollPos = GetScrollPos(hwnd,SB_VERT);
		scrollPos -= delta;
		SetScrollPos(hwnd,SB_VERT,scrollPos,TRUE);
		// Get the scrollpos again in case it was clamped
		scrollPos = GetScrollPos(hwnd,SB_VERT);
		SetWindowPos(pane->m_Inner,0,0,-scrollPos,0,0,SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);
	}
	return DefWindowProc(hwnd,msg,wParam,lParam);
}

void bkPane::ClampHeight()
{
	// limit window size to height of workable area:
	RECT rectWorkArea;
	RECT rectOuter;
	GetWindowRect(m_Outer,&rectOuter);
	SystemParametersInfo(SPI_GETWORKAREA,0x0,&rectWorkArea,0x0);

	// clamp height if we need to:
	if ((rectOuter.top+m_Height+m_OuterHeight)>rectWorkArea.bottom)
		m_Height=rectWorkArea.bottom-rectOuter.top-m_OuterHeight;
}

void bkPane::Resize() {
	bkWidget *i = GetWidget()->GetChild();
	m_Height = 0;
	while (i) {
		m_Height = i->WindowResize(0,m_Height,m_Width,m_ItemHeight);
		i = i->GetNext();
	}
	int realHeight=m_Height;

	SCROLLINFO old_si = { sizeof(SCROLLINFO), SIF_POS };
	GetScrollInfo(m_Outer,SB_VERT,&old_si);
	
	ClampHeight();

	// get window information:
	RECT rectOuter,rectInner;
	GetClientRect(m_Outer,&rectOuter);
	int outerHeight = rectOuter.bottom - rectOuter.top;
	GetClientRect(m_Inner,&rectInner);

	// Only resize outer window if we're now taller than the inner window (because they closed a group within the bank)
	if ((outerHeight) > m_Height)
	{
		SetWindowPos(m_Outer,0,0,0,m_Width + m_OuterWidth,(outerHeight = m_Height) + m_OuterHeight,SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOZORDER);
		::SetWindowPos(m_Inner,0,0,0,m_Width,realHeight,SWP_NOACTIVATE | SWP_NOZORDER);
		SCROLLINFO new_si = { sizeof(SCROLLINFO), SIF_DISABLENOSCROLL | SIF_PAGE | SIF_RANGE | SIF_POS, 0, realHeight-1, m_Height, 0 /*nPos*/, 0/*nTrackPos*/ };
		SetScrollInfo(m_Outer,SB_VERT,&new_si,TRUE);
	}
	// user just collapsed window so the actual bank area is smaller than the visible bank area, so 
	// we scroll up so the actual bank bottom draws at the bottom of the visible bank area:
	else if ((realHeight-old_si.nPos)<outerHeight)
	{
		int newScrollPos=-(old_si.nPos-(outerHeight-(realHeight-old_si.nPos)));
		SCROLLINFO new_si = { sizeof(SCROLLINFO), SIF_DISABLENOSCROLL | SIF_PAGE | SIF_RANGE /*| SIF_POS*/, 0, realHeight-1, outerHeight, newScrollPos /*nPos*/, 0/*nTrackPos*/ };
		::SetWindowPos(m_Inner,0,0,newScrollPos,m_Width,realHeight,SWP_NOACTIVATE | SWP_NOZORDER);
		SetScrollInfo(m_Outer,SB_VERT,&new_si,TRUE);
	}
	else 
	{
		SCROLLINFO new_si = { sizeof(SCROLLINFO), SIF_DISABLENOSCROLL | SIF_PAGE | SIF_RANGE, 0, realHeight-1, outerHeight, 0 /*nPos*/, 0/*nTrackPos*/ };
		::SetWindowPos(m_Inner,0,0,-old_si.nPos,m_Width,realHeight,SWP_NOACTIVATE | SWP_NOZORDER);
		SetScrollInfo(m_Outer,SB_VERT,&new_si,TRUE);
	}
	m_NeedsResize=false;
}


void bkPane::GetState(bool &shown,int &x,int &y,int &width,int &height) const {
	shown = IsWindowVisible(m_Outer) != 0;
	RECT rect;
	GetWindowRect(m_Outer,&rect);
	x = rect.left;
	y = rect.top;
	width = rect.right - rect.left;
	height = rect.bottom - rect.top;
}


void bkPane::SetState(bool shown,int x,int y,int width,int height) {
	SetWindowPos(m_Outer, NULL, x, y, width, height, (SWP_NOACTIVATE | SWP_NOZORDER) | (shown? SWP_SHOWWINDOW : SWP_HIDEWINDOW));
}

bool rage::bkAlwaysOnTop = false;


bkPane::bkPane(HWND owner,bkWidget *widget,const char *title,int x,int y,bool isMaster) {
	InitCommonControls();

	m_Widget = widget;
	static ATOM classAtomInner, classAtomOuter;
	if (!classAtomInner) {
		WNDCLASS wc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
		wc.hCursor = LoadCursor(GetModuleHandle(0), IDC_ARROW);
		wc.hIcon = LoadCursor(GetModuleHandle(0), IDI_APPLICATION);
		wc.hInstance = GetModuleHandle(0);
		wc.lpfnWndProc = bkPane::WndProcInner;
		wc.lpszClassName = "bkPaneInner";
		wc.lpszMenuName = 0;
		wc.style = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
#		ifdef UNICODE
			classAtomInner = RegisterClassW(&wc);
#		else
			classAtomInner = RegisterClassA(&wc);
#		endif
	}
	if (!classAtomOuter) {
		WNDCLASS wc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1);
		wc.hCursor = LoadCursor(GetModuleHandle(0), IDC_ARROW);
		wc.hIcon = LoadCursor(GetModuleHandle(0), IDI_APPLICATION);
		wc.hInstance = GetModuleHandle(0);
		wc.lpfnWndProc = bkPane::WndProcOuter;
		wc.lpszClassName = "bkPaneOuter";
		wc.lpszMenuName = 0;
		wc.style = CS_DBLCLKS | CS_HREDRAW | CS_VREDRAW;
#		ifdef UNICODE
			classAtomOuter = RegisterClassW(&wc);
#		else
			classAtomOuter = RegisterClassA(&wc);
#		endif
	}
	bool remoteMaster = bkRemotePacket::IsConnected() && isMaster;
	DWORD exStyle = remoteMaster? WS_EX_APPWINDOW :  WS_EX_TOOLWINDOW;
	DWORD style = remoteMaster
		? WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_VSCROLL
		: WS_POPUP | WS_CAPTION |              WS_THICKFRAME                  | WS_VSCROLL;

	m_IsMaster = isMaster;
	if (isMaster)
	{
		if(!PARAM_hidewidgets.Get())
			style |= WS_VISIBLE;
	}
	else
		style |= WS_SYSMENU;

	m_Height = 0;
	m_Width = 250;
	m_ItemHeight = 20;

	RECT rect;
	ZeroMemory(&rect,sizeof(rect));
	AdjustWindowRectEx(&rect,style,FALSE,exStyle);
	m_OuterWidth = rect.right - rect.left + GetSystemMetrics(SM_CXVSCROLL);
	m_OuterHeight = rect.bottom - rect.top;

	if (!sysBootManager::IsDebuggerPresent() && bkAlwaysOnTop)
		exStyle |= WS_EX_TOPMOST;

	m_Outer = CreateWindowEx(exStyle,MAKEINTRESOURCE(classAtomOuter),title,style,x,y,m_Width + m_OuterWidth,m_Height + m_OuterHeight,owner,NULL,GetModuleHandle(0),0);
	if (!m_Outer) {
		DWORD code = GetLastError();
		bkErrorf("Could not create outer window. Error code %d\n", code);
	}

	++s_Creates;
	SetWindowLongPtr(m_Outer,GWLP_USERDATA,(LONG_PTR) this);

	if (style & WS_SYSMENU) {
		HMENU menu = GetSystemMenu(m_Outer,FALSE);
		EnableMenuItem(menu, SC_MAXIMIZE, MF_BYCOMMAND | MF_GRAYED);
		EnableMenuItem(menu, SC_MINIMIZE, MF_BYCOMMAND | MF_GRAYED);
		EnableMenuItem(menu, SC_RESTORE, MF_BYCOMMAND | MF_GRAYED);
	}

	m_Inner = CreateWindowEx(0,MAKEINTRESOURCE(classAtomInner),NULL,WS_CHILD | WS_VISIBLE,0,0,m_Width,0,m_Outer,NULL,GetModuleHandle(0),0);
	if (!m_Inner) {
		DWORD code = GetLastError();
		bkErrorf("Could not create inner window. Error code %d\n", code);
	}
	++s_Creates;
	SetWindowLongPtr(m_Inner,GWLP_USERDATA,(LONG_PTR) this);

	LOGFONT lf;
	ZeroMemory(&lf,sizeof(lf));
	lf.lfHeight = 8;
	safecpy(lf.lfFaceName,"MS Sans Serif");
	m_Font = CreateFontIndirect(&lf);

	lf.lfWeight = 700;
	m_BoldFont = CreateFontIndirect(&lf);
	
	m_Tooltip = CreateWindow(TOOLTIPS_CLASS,NULL,TTS_ALWAYSTIP,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,NULL,0,GetModuleHandle(0),NULL); 
	if (!m_Tooltip) {
		DWORD code = GetLastError();
		bkErrorf("Could not create tooltip window. Error code %d\n", code);
	}
	++s_Creates;
}


bkPane::~bkPane() {
	static bool classUnregister = false;
	if (!classUnregister) {
		UnregisterClass("bkPaneOuter", GetModuleHandle(0));
		UnregisterClass("bkPaneInner", GetModuleHandle(0));
		classUnregister = true;
	}

	DestroyWindow(m_Tooltip);
	DestroyWindow(m_Outer);
	DestroyWindow(m_Inner);
	DeleteObject(m_Font);
	DeleteObject(m_BoldFont);
}


HWND bkPane::AddWindow( bkWidget *widget, const char *className, unsigned exStyle, unsigned style, int extraHeight )
{
	return AddWindow( widget, className, exStyle, style, widget->GetTitle(), widget->GetTooltip(), extraHeight );
}

HWND bkPane::AddWindow( bkWidget *widget, const char *className, unsigned exStyle, unsigned style,
					   const char* title, const char* tooltip, int extraHeight )
{
	style |= WS_CHILD | WS_VISIBLE;
	
	HWND hwndKid = CreateWindowEx( exStyle, className, title, style, 0, m_Height, 0, m_ItemHeight + extraHeight, m_Inner, 0, GetModuleHandle(0), 0 );
	if ( !hwndKid ) 
	{
		bkErrorf( "Cannot create window type %s for '%s' in '%s' (Window #%d)", className, widget->GetTitle(),
			widget->GetParent() ? widget->GetParent()->GetTitle() : "Master Bank", s_Creates );
		return NULL;
	}

	++s_Creates;
	SetWindowLongPtr( hwndKid, GWLP_USERDATA, (LONG_PTR)widget );
	SendMessage( hwndKid, WM_SETFONT, (WPARAM)m_Font, MAKELPARAM(TRUE, 0) );

	TOOLINFO ti;
	ti.cbSize	= sizeof(TOOLINFO);
	ti.uFlags	= TTF_IDISHWND | TTF_SUBCLASS ; 
	ti.hwnd		= hwndKid; 
	ti.uId		= (UINT_PTR)hwndKid; 
	ti.lpszText = (LPSTR)(tooltip ? tooltip : title); 

	SendMessage( m_Tooltip, TTM_SETMAXTIPWIDTH, 0, (LPARAM)(INT)400 );
	SendMessage( m_Tooltip, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti ); 

	return hwndKid;
}

void bkPane::AddLastWindow( bkWidget *widget, const char *className, unsigned exStyle, unsigned style, int extraHeight ) 
{
	AddLastWindow( widget, className, exStyle, style, widget->GetTitle(), widget->GetTooltip(), extraHeight );
}

void bkPane::AddLastWindow( bkWidget *widget, const char *className, unsigned exStyle, unsigned style,
				   const char* title, const char* tooltip, int extraHeight )
{
	if ( bkRemotePacket::IsConnectedToRag() )
	{
		return;
	}

	widget->m_Hwnd = AddWindow( widget, className, exStyle, style, title, tooltip, extraHeight );

	bkWidget *i = widget;
	while ( i->GetParent()->IsOpen() && (i->GetParent() != m_Widget) )
	{
		i = i->GetParent();
	}

	bool isVisible = i->GetParent() == m_Widget;

	if (isVisible || m_IsMaster) 
	{
		widget->WindowUpdate();
		m_Height = widget->WindowResize( 0, m_Height, m_Width, m_ItemHeight );

		ClampHeight();

		SetWindowPos( m_Inner, 0, 0, 0, m_Width, m_Height, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOZORDER );
		SetWindowPos( m_Outer, 0, 0, 0, m_Width + m_OuterWidth, m_Height + m_OuterHeight, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOZORDER );
	}
	else
	{
		widget->WindowUpdate();
	}
}

#endif

#endif
