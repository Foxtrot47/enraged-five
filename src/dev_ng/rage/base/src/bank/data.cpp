// 
// bank/data.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if __BANK

#include "data.h"

#include "packet.h"
#include "pane.h"

using namespace rage;

bkData::bkData( datCallback &callback, const char *title, const char *memo, u8* data, u16 length, bool showDataView, const char *fillColor, bool readOnly )
: bkWidget( callback, title, memo, fillColor, readOnly )
, m_pData(NULL)
, m_length(0)
, m_showDataView(showDataView)
, m_ownsData(false)
{
	m_pData = data;
	m_length = length;

	if ( (m_pData == NULL) && (m_length > 0) )
	{
		m_pData = rage_new u8[m_length];
		m_ownsData = true;
	}
}

bkData::~bkData()
{
	if ( m_ownsData && m_pData )
	{
		delete m_pData;
		m_pData = NULL;
		m_length = 0;
		m_ownsData = false;
	}
}

void bkData::RemoteCreate() 
{
	bkWidget::RemoteCreate();

    bkRemotePacket p;
    p.Begin( bkRemotePacket::CREATE, GetStaticGuid(), this );
    p.WriteWidget( m_Parent );
    p.Write_const_char( m_Title );
    p.Write_const_char( GetTooltip() );
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
    p.Write_u16( m_length );
    p.Write_bool( m_showDataView );
    p.Send();

    // this will send over the data
    RemoteUpdate();
}

int bkData::DrawLocal(int x,int y)
{
	Drawf( x, y, m_Title );

	return bkWidget::DrawLocal( x, y );
}

void bkData::RemoteUpdate()
{
    RemoteUpdate( 0, m_length );
}

void bkData::RemoteUpdate( u16 offset, u16 count )
{
	if (!bkRemotePacket::IsConnected()) return;
    bkRemotePacket p;
    if ( m_pData != NULL )
    {
        if ( count > m_length )
        {
            count = m_length;
        }

		u16 maxPayloadSize = sysPipePacket::DATA_SIZE - 8;			// 8 = size of length + count + offset + remaining packets
        s16 packetsRemaining = (s16)((count - offset) / maxPayloadSize);
        while ( packetsRemaining >= 0 )
        {
            p.Begin( bkRemotePacket::CHANGED, GetStaticGuid(), this );

            u16 pcount = (u16)(count - offset > maxPayloadSize ? maxPayloadSize : count - offset);
            p.Write_u16( m_length );
            p.Write_u16( pcount );
            p.Write_u16( offset );
            p.Write_u16( packetsRemaining );

            for ( int i = 0; i < pcount; ++i )
            {
                p.Write_u8( m_pData[offset + i] );
            }

            p.Send();
            offset = offset + pcount;
            --packetsRemaining;
        }
    }
    else
    {
        p.Begin( bkRemotePacket::CHANGED, GetStaticGuid(), this );
        p.Write_u16( 0 );
        p.Write_u16( 0 );
        p.Write_u16( 0 );
        p.Write_s16( 0 );
        p.Send();
    }
}
   
void bkData::RemoteHandler(const bkRemotePacket& packet) 
{
    if ( packet.GetCommand() == bkRemotePacket::CREATE )
    {
        packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;
        const char *title = packet.Read_const_char(), *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();
        u16 length = packet.Read_u16();
		int showDataView = packet.Read_u16();
        packet.End();
        bkData& widget = *(rage_new bkData(NullCB,title,memo,NULL,length,showDataView == 1,fillColor,readOnly));
		bkRemotePacket::SetWidgetId(widget, id);
		parent->AddChild(widget);
    }
    else if ( packet.GetCommand() == bkRemotePacket::CHANGED )
    {
        packet.Begin();
		bkData* widget = packet.ReadWidget<bkData>();

        u16 length = packet.Read_u16();
        u16 count = packet.Read_u16();
        u16 offset = packet.Read_u16();
        u16 packetsRemaining = packet.Read_s16();

        if ( length == 0 )
        {
            if ( widget->m_ownsData )
            {
                delete widget->m_pData;
            }

            widget->m_pData = NULL;
        }
        else if ( (widget->m_pData == NULL) || (widget->m_ownsData && (widget->m_length < length)) )
        {
            widget->SetData( NULL, length );
        }
        else if ( !widget->m_ownsData )
        {
            bkAssertf( (length <= widget->m_length) , "Tried to receive more than WidgetData could hold" );
        }

        if ( widget->m_pData != NULL )
        {
            for ( int i = offset; (i < offset + count) && (i < length); ++i )
            {
                widget->m_pData[i] = packet.Read_u8();
            }
            
            if ( packetsRemaining == 0 )
            {
                widget->Changed();
            }
        }

        packet.End();
    }
}

void bkData::SetData( u8* data, u16 length )
{
    if ( m_ownsData && m_pData )
    {
        delete m_pData;
        m_pData = NULL;
        m_ownsData = false;
    }

    m_pData = data;
    m_length = length;

    if ( (m_pData == NULL) && (m_length > 0) )
    {
        m_pData = rage_new u8[m_length];
        m_ownsData = true;
    }

	RemoteUpdate();
}

#if __WIN32PC
#include "system/xtl.h"

void bkData::WindowCreate() 
{
	if ( !bkRemotePacket::IsConnectedToRag() )
	{
		GetPane()->AddLastWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX );
	}
}
#endif // __WIN32PC

#endif // __BANK
