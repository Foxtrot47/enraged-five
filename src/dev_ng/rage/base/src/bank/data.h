// 
// bank/data.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BANK_DATA_H
#define BANK_DATA_H

#if __BANK

#include "widget.h"

namespace rage 
{

class bkData: public bkWidget 
{
    friend class bkGroup;
    friend class bkManager;

protected:
	bkData( datCallback &callback, const char *title, const char *memo, u8* data=NULL, u16 length=0, bool showDataView=false, const char *fillColor=NULL, bool readOnly=false );
    ~bkData();

    void RemoteCreate();

#if __WIN32PC
    void WindowCreate();
    rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM) { return 0; }
#endif

	int DrawLocal(int x,int y);

public:
    // PURPOSE: Sends the contents the data buffer to Rag
    void RemoteUpdate();
    
    // PURPOSE: 
    //  Sends the specified number of bytes starting at the offset to Rag
    // PARAMS:
    //  offset 
    //  count
    // NOTES:
    //  If count is greater than the length of the data buffer, count is truncated.
    void RemoteUpdate( u16 offset, u16 count );

    static void RemoteHandler(const bkRemotePacket& p);

    static int GetStaticGuid() { return BKGUID('d','a','t','a'); }
    int GetGuid() const;

    // PURPOSE: 
    //  Sets the send/receive buffer and its length
    // PARAMS:
    //  data
    //  length - number of bytes in data
    // NOTES:
    //  If data is NULL and length is greater than zero, an internal buffer is allocated and managed for you.  
    //  This buffer can then shrink/grow according to the size of the messages received.
    void SetData( u8* data, u16 length );

    u8* GetData() const { return m_pData; }
    u16 GetLength() const { return m_length; }

private:
    u8* m_pData;
    u16 m_length;
    bool m_showDataView;
    bool m_ownsData;
};

inline int bkData::GetGuid() const
{
    return bkData::GetStaticGuid();
}

} // namespace rage

#endif // __BANK

#endif // BANK_DATA_H
