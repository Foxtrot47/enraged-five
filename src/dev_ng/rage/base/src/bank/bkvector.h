// 
// bank/bkvector.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BANK_BKVECTOR_H 
#define BANK_BKVECTOR_H 

#include "packet.h"
#include "widget.h"

#include "file/remote.h"
#include "system/param.h"
#include "vector/vector2.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

#if __BANK
namespace rage 
{

class bkVector : public bkWidget
{
public:
	int GetNumComponents() const;

	float GetMinimum() const;
	void SetMinimum( float min );

	float GetMaximum() const;
	void SetMaxium( float max );

	float GetStep() const;
	void SetStep( float step );

	virtual int GetGuid() const = 0;

	bool SetFocus();

	virtual bool SetValue( int component, float f ) = 0;
	virtual float GetValue( int component ) = 0;

	virtual bool SetString( int component, const char* src ) = 0;
	virtual const char* GetString( int component, char* dest, int destSize ) = 0;

    // PURPOSE: Retrieves a string representation of the current value of this widget.
    // PARAMS:
    //    buf - the buffer to write the string representation to.
    //    bufLen - the length of the buffer.
    virtual void GetStringRepr( char *buf, int bufLen );

    // PURPOSE: Sets the current value of this widget using the string representation of the new value.
    // PARAMS:
    //    buf - the new value.
    virtual void SetStringRepr( const char *buf );

	// PURPOSE: Minimum value of floating point slider
	static const float FLOAT_MIN_VALUE;

	// PURPOSE: Maximum value of floating point slider
	static const float FLOAT_MAX_VALUE;

protected:	
	enum EnumType
	{
		EVector2 = 2,
		EVector3 = 3,
		EVector4 = 4
	};

	enum Operation
	{
		EScale,
		EInvScale,
		EExtend,
		EScale3,
		EAdd,
		ESubtract,
		EMultiply,
		EInvert,
		EInvertSafe,
		ENegate,
		EAbs,
		EAverage,
		ENormalize,
		ENormalizeSafe,
		ENormalizeSaveV,
		ENormalizeFast,
		ENormalize3,
		ENormalize3V,
		ENormalizeFast3,
		ERotate,
		ERotateX,
		ERotateY,
		ERotateZ,
		ERotateAboutAxis,
		ECross,
		EDotV,
		EDot3V,
		ELerp,
		ELog,
		ELog10,
		EReflectAbout,
		EReflectAboutFast,
		EAddNet
	};

	bkVector( datCallback &callback, const char *title, const char *memo, EnumType enumType, float min, float max, float step, const char *fillColor=NULL, bool readOnly=false );
	~bkVector() = 0;	

	virtual void ReadValues( const bkRemotePacket& packet ) = 0;
	virtual void ReadValues( const bkRemotePacket& packet, float vec[] ) = 0;
	virtual void WriteValues( bkRemotePacket& packet ) = 0;

	virtual void ExecuteOperation( Operation /*op*/ ) {};
	virtual void ExecuteOperation( Operation /*op*/, float /*f*/ ) {};
	virtual void ExecuteOperation( Operation /*op*/, float /*vec*/[] ) {};
	virtual void ExecuteOperation( Operation /*op*/, float /*f*/, float /*vec*/[] ) {};
	virtual void ExecuteOperation( Operation /*op*/, float /*f*/, int /*i*/ ) {};

	virtual void ClampValuesToAllowedRange() {};

	void Message( Action action, float value );
	int DrawLocal( int x, int y );

	static void RemoteHandler( const bkRemotePacket& packet, EnumType enumType );

	void CheckInitialValues();

	float m_minimum;
	float m_maximum;
	float m_step;
	EnumType m_enumType;

private:
	void RemoteCreate();
	void RemoteUpdate();

	int m_drawLocalExpanded;

#if __WIN32PC
	void WindowCreate();
	void WindowUpdate();
	void WindowDestroy();
	rageLRESULT WindowMessage( rageUINT msg, rageWPARAM wParam, rageLPARAM lParam );
	void WindowMessage( Action action, float value );
	int WindowResize( int x, int y, int width, int height );
	
	void AddComponentWindow( int component );
	void ResizeComponentWindow( int component, int x, int y, int width, int height );

public:
	bool ValidateString( const char* string, bool okOnly ) const;
	bool CheckKey( char key ) const;
	void SetFont( bool bold );

	void SetComponentInFocus( int component, bool setWindowFocus );
	int GetComponentInFocus() const;

	void SetWindowInFocus( struct HWND__* hwnd );
	struct HWND__* GetWindowInFocus() const;

protected:
	struct HWND__* m_titleWindowHandle;
	struct HWND__* m_windowHandles[13];
	int m_componentInFocus;
#endif	
};

inline int bkVector::GetNumComponents() const
{
	return m_enumType;
}

inline float bkVector::GetMinimum() const
{
	return m_minimum;
}

inline void bkVector::SetMinimum( float min )
{
	m_minimum = min;
}

inline float bkVector::GetMaximum() const
{
	return m_maximum;
}

inline void bkVector::SetMaxium( float max )
{
	m_maximum = max;
}

inline float bkVector::GetStep() const
{
	return m_step;
}

inline void bkVector::SetStep( float step )
{
	m_step = step;
}

#if __WIN32PC

inline int bkVector::GetComponentInFocus() const
{
	return m_componentInFocus;
}

#endif

//#############################################################################

class bkVector2 : public bkVector
{
public:
	static inline int GetStaticGuid();
	int GetGuid() const;

	bool SetValue( int component, float f );
	float GetValue( int component );

	bool SetString( int component, const char* src );
	const char* GetString( int component, char* dest, int destSize );

    const Vector2& GetVector() const;
    void SetVector( const Vector2 &v );

protected:
	friend class bkGroup;
	friend class bkManager;
	friend class bkVector;

	bkVector2( datCallback &callback, const char *title, const char *memo, Vector2 *data, float min, float max, float step, const char* fillColor=NULL, bool readOnly=false );
	~bkVector2();

	void ReadValues( const bkRemotePacket& packet );
	void ReadValues( const bkRemotePacket& packet, float vec[] );
	void WriteValues( bkRemotePacket& packet );

	void ExecuteOperation( Operation op );
	void ExecuteOperation( Operation op, float f );
	void ExecuteOperation( Operation op, float vec[] );
	void ExecuteOperation( Operation op, float f, float vec[] );

	void ClampValuesToAllowedRange();

	void Update();

private:
	static void RemoteHandler( const bkRemotePacket& packet );

	Vector2 *m_pValue;
	Vector2 m_prevValue;
};

inline int bkVector2::GetStaticGuid()
{
	return BKGUID( 'v', 'e', 'c', '2' );
}

inline int bkVector2::GetGuid() const
{
	return GetStaticGuid();
}

inline const Vector2& bkVector2::GetVector() const
{
    return *m_pValue;
}

inline void bkVector2::SetVector( const Vector2 &v )
{
    m_pValue->Set( v );
    Changed();
}

inline void bkVector2::Update()
{
	if ( *m_pValue != m_prevValue ) 
	{ 
		m_prevValue = *m_pValue; 
		FinishUpdate();
	}
}

//#############################################################################

class bkVector3 : public bkVector
{
public:
	static inline int GetStaticGuid();
	int GetGuid() const;

	bool SetValue( int component, float f );
	float GetValue( int component );

	bool SetString( int component, const char* src );
	const char* GetString( int component, char* dest, int destSize );

    const Vector3& GetVector() const;
    void SetVector( const Vector3 &v );

protected:
	friend class bkGroup;
	friend class bkManager;
	friend class bkVector;

	bkVector3( datCallback &callback, const char *title, const char *memo, Vector3 *data, float min, float max, float step, const char* fillColor=NULL, bool readOnly=false );
	~bkVector3();

	void ReadValues( const bkRemotePacket& packet );
	void ReadValues( const bkRemotePacket& packet, float vec[] );
	void WriteValues( bkRemotePacket& packet );

	void ExecuteOperation( Operation op );
	void ExecuteOperation( Operation op, float f );
	void ExecuteOperation( Operation op, float vec[] );
	void ExecuteOperation( Operation op, float f, float vec[] );
	void ExecuteOperation( Operation op, float f, int i );

	void ClampValuesToAllowedRange();

	void Update();

private:
	static void RemoteHandler( const bkRemotePacket& packet );

	Vector3 *m_pValue;
	Vector3 m_prevValue;
};

inline int bkVector3::GetStaticGuid()
{
	return BKGUID( 'v', 'e', 'c', '3' );
}

inline int bkVector3::GetGuid() const
{
	return GetStaticGuid();
}

inline const Vector3& bkVector3::GetVector() const
{
    return *m_pValue;
}

inline void bkVector3::SetVector( const Vector3 &v )
{
    m_pValue->Set( v );
    Changed();
}

inline void bkVector3::Update()
{
	if ( *m_pValue != m_prevValue ) 
	{ 
		m_prevValue = *m_pValue; 
		FinishUpdate();
	}
}

//#############################################################################

class bkVector4 : public bkVector
{
public:
	static inline int GetStaticGuid();
	int GetGuid() const;

	bool SetValue( int component, float f );
	float GetValue( int component );

	bool SetString( int component, const char* src );
	const char* GetString( int component, char* dest, int destSize );

    const Vector4& GetVector() const;
    void SetVector( const Vector4 &v );

protected:
	friend class bkGroup;
	friend class bkManager;
	friend class bkVector;

	bkVector4( datCallback &callback, const char *title, const char *memo, Vector4 *data, float min, float max, float step, const char* fillColor=NULL, bool readOnly=false );
	~bkVector4();

	void ReadValues( const bkRemotePacket& packet );
	void ReadValues( const bkRemotePacket& packet, float vec[] );
	void WriteValues( bkRemotePacket& packet );

	void ExecuteOperation( Operation op );
	void ExecuteOperation( Operation op, float f );
	void ExecuteOperation( Operation op, float vec[] );
	void ExecuteOperation( Operation op, float f, float vec[] );

	void ClampValuesToAllowedRange();

	void Update();

private:
	static void RemoteHandler( const bkRemotePacket& packet );

	Vector4 *m_pValue;
	Vector4 m_prevValue;
};

inline int bkVector4::GetStaticGuid()
{
	return BKGUID( 'v', 'e', 'c', '4' );
}

inline int bkVector4::GetGuid() const
{
	return GetStaticGuid();
}

inline const Vector4& bkVector4::GetVector() const
{
    return *m_pValue;
}

inline void bkVector4::SetVector( const Vector4 &v )
{
    m_pValue->Set( v );
    Changed();
}

inline void bkVector4::Update()
{
	if ( *m_pValue != m_prevValue )
	{ 
		m_prevValue = *m_pValue; 
		FinishUpdate();
	}
}

} // namespace rage

#endif // __BANK

#endif // BANK_BKVECTOR_H 
