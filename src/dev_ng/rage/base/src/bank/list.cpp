//
// bank/list.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "list.h"

#include "packet.h"
#include "pane.h"

using namespace rage;

bkList::bkList(const char *title,bool readOnly,const char *memo, const char *fillColor) :
bkWidget(NullCallback,title,memo,fillColor,readOnly)
{
}



bkList::~bkList() {
}


int bkList::GetGuid() const { return GetStaticGuid(); }


int bkList::DrawLocal(int x,int y) 
{
	Drawf( x, y, m_Title );
	return bkWidget::DrawLocal(x,y);
}

enum 
{
	L_EVENT_ADD_COLUMN_HEADER=bkRemotePacket::USER+0,
	L_EVENT_ADD_ITEM=bkRemotePacket::USER+1,
	L_EVENT_REMOVE_ITEM=bkRemotePacket::USER+2,
	L_EVENT_UPDATE_ITEM=bkRemotePacket::USER+3,
	L_EVENT_DOUBLECLICK_ITEM=bkRemotePacket::USER+4,
	L_EVENT_SINGLECLICK_ITEM=bkRemotePacket::USER+5
};

void bkList::AddColumnHeader(int column,const char* heading,ItemType type)
{
	if (!bkRemotePacket::IsConnected())
	{
		return;
	}
	bkRemotePacket p;
	p.Begin(L_EVENT_ADD_COLUMN_HEADER,GetStaticGuid(),this);
	p.Write_s32(column);
	p.Write_const_char(heading);
	p.Write_s32(type);
	p.Send();
}

namespace rage
{
	int sGetStaticGuid()
	{
		return bkList::GetStaticGuid();
	}
}

template<class __Type> void sAddItemCommon(bkList& list,s32 key,int column,__Type value,void (bkRemotePacket::*writeFunc)(__Type))
{
	if (!bkRemotePacket::IsConnected())
	{
		return;
	}
	bkRemotePacket p;
	p.Begin(L_EVENT_ADD_ITEM,sGetStaticGuid(),&list);
	p.Write_s32(key);
	p.Write_s32(column);
	(p.*writeFunc)(value);
	p.Send();
}

void bkList::AddItem(s32 key,int column,float value)
{
	sAddItemCommon<float>(*this,key,column,value,&bkRemotePacket::Write_float);
}

void bkList::AddItem(s32 key,int column,int value)
{
	sAddItemCommon<int>(*this,key,column,value,&bkRemotePacket::Write_s32);
}

void bkList::AddItem(s32 key,int column,const char* value)
{
	sAddItemCommon<const char*>(*this,key,column,value,&bkRemotePacket::Write_const_char);
}

void bkList::RemoveItem(s32 key)
{
	if (!bkRemotePacket::IsConnected())
	{
		return;
	}
	bkRemotePacket p;
	p.Begin(L_EVENT_REMOVE_ITEM,GetStaticGuid(),this);
	p.Write_s32(key);
	p.Send();
}


void bkList::RemoteCreate() {
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin(bkRemotePacket::CREATE,GetStaticGuid(),this);
	p.WriteWidget(m_Parent);
	p.Write_const_char(m_Title);
	p.Write_const_char(GetTooltip());
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
	p.Send();
}


void bkList::RemoteUpdate() {
	//bkRemotePacket p;
	//p.Begin(bkRemotePacket::CHANGED,GetStaticGuid());
	//p.Write_bkWidget(bkRemotePacket::LocalToRemote(*this));
	//p.Write_const_char(m_String);
	//p.Send();
}


void bkList::RemoteHandler(const bkRemotePacket& packet) {
	if (packet.GetCommand() == L_EVENT_UPDATE_ITEM) 
	{ 
		packet.Begin();		
		bkList* widget = packet.ReadWidget<bkList>();
		if (!widget) return;
		s32 key=packet.Read_s32();
		s32 column=packet.Read_s32();
		const char* newValue=packet.Read_const_char();
		packet.End();

		if (widget->m_UpdateItemFunc.IsValid())
			widget->m_UpdateItemFunc(key,column,newValue);
	}
	else if(packet.GetCommand() == L_EVENT_DOUBLECLICK_ITEM)
	{
		packet.Begin();
		bkList* widget = packet.ReadWidget<bkList>();
		if(!widget) return;
		s32 key=packet.Read_s32();
		packet.End();

		if (widget->m_DoubleClickItemFunc.IsValid())
			widget->m_DoubleClickItemFunc(key);
	}
	else if(packet.GetCommand() == L_EVENT_SINGLECLICK_ITEM)
	{
		packet.Begin();
		bkList* widget = packet.ReadWidget<bkList>();
		if(!widget) return;
		s32 key=packet.Read_s32();
		packet.End();

		if (widget->m_SingleClickItemFunc.IsValid())
			widget->m_SingleClickItemFunc(key);
	}
}

void bkList::Update() {
}

#if __WIN32PC
#include "system/xtl.h"

void bkList::WindowCreate() 
{
	if ( !bkRemotePacket::IsConnectedToRag() )
	{
		GetPane()->AddLastWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX );
	}
}
#endif // __WIN32PC

#endif
