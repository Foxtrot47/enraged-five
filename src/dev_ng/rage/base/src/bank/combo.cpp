//
// bank/combo.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#if __BANK

#include "combo.h"

#include "packet.h"
#include "pane.h"

#include "string/string.h"

extern __THREAD int RAGE_LOG_DISABLE;

using namespace rage;

bkCombo::bkCombo(datCallback &callback,const char *title,const char *memo,const char** items,int count,int offset, const char *fillColor, bool readOnly) :
	bkWidget(callback,title,memo,fillColor,readOnly), 
	m_Items(0), 
	m_Count(count), 
	m_Offset(offset) 
{
	m_Items = rage_new const char*[m_Count];
	for (int i=0; i<m_Count; i++)
		m_Items[i] = items? ConstStringDuplicate(items[i]) : 0;
#if __WIN32PC
	m_Label = 0;
#endif
}



bkCombo::~bkCombo() {
	for (int i=0; i<m_Count; i++)
		ConstStringFree(m_Items[i]);
	delete [] m_Items;
}


#if __WIN32PC

#include "system/xtl.h"

void bkCombo::WindowCreate() {
	if (!bkRemotePacket::IsConnectedToRag())
	{
		bkPane *pane = GetPane();
		m_Label = pane->AddWindow(this, "STATIC", 0, SS_LEFT | SS_NOPREFIX);
		pane->AddLastWindow(this, "COMBOBOX", 0, CBS_DROPDOWNLIST|WS_VSCROLL|WS_BORDER, 500);
		for (int i=0; i<m_Count; i++)
			if (m_Items[i])
				SendMessage(m_Hwnd,CB_ADDSTRING,0,(LPARAM)m_Items[i]);
		// Call it again now that strings are there.
		WindowUpdate();
	}
}


void bkCombo::WindowDestroy() {
	if (m_Label) {
		DestroyWindow(m_Label);
		m_Label = 0;
	}
	bkWidget::WindowDestroy();
}


void bkCombo::WindowUpdate() {
	if (m_Hwnd)
		SendMessage(m_Hwnd,CB_SETCURSEL,GetValue() - m_Offset,0);
}


rageLRESULT bkCombo::WindowMessage(rageUINT msg,rageWPARAM wParam,rageLPARAM /*lParam*/) {
	if (msg == WM_COMMAND && HIWORD(wParam) == CBN_SELCHANGE)
		SetValue((int) SendMessage(m_Hwnd,CB_GETCURSEL,0,0) + m_Offset);
	return 0;
}


int bkCombo::WindowResize(int x,int y,int width,int height) {
	int comboWidth = width * 2 / 3;
	int comboStart = width - comboWidth;
	if (m_Label)
		SetWindowPos(m_Label,0,x,y,comboStart,height,SWP_NOACTIVATE | SWP_NOZORDER);
	if (m_Hwnd)
		SetWindowPos(m_Hwnd,0,x+comboStart,y,comboWidth,height,SWP_NOACTIVATE | SWP_NOZORDER);
	return y+height;
}

#endif // __WIN32PC

void bkCombo::RemoteUpdateComboItem(int,const char*) {}

void bkCombo::RemoteUpdateCombo() {}

void bkCombo::SetString(int id,const char *str) {
	if (!m_Items || id < 0 || id >= m_Count)
		return;
	ConstStringFree(m_Items[id]);
	m_Items[id] = ConstStringDuplicate(str);
#if __WIN32PC
	if (m_Hwnd)
	{
		bkAssertf(m_Items[id], "NULL strings not allowed for combo names");
		SendMessage(m_Hwnd,CB_DELETESTRING,id,0);
		SendMessage(m_Hwnd,CB_INSERTSTRING,id,(LPARAM)m_Items[id]);

        // (re)set selection in case id was selected when we deleted it
        SendMessage( m_Hwnd, CB_SETCURSEL, GetValue() - m_Offset, 0 );
	}
	else
#endif
	{
		RemoteUpdateComboItem(id,str);
	}
}

const char* bkCombo::GetString(int id)
{
	if (!m_Items || id < 0 || id >= m_Count)
	{
		return NULL;
	}

	return m_Items[id];
}

int bkCombo::GetStringIndex(const char *str)
{
	for (int id = 0; id < m_Count; id++)
	{
		if (strcmp(m_Items[id], str) == 0)
		{
			return id;
		}
	}

	return -1;
}

void bkCombo::UpdateCombo( const char *title, void * /*value*/, int numitems, const char **list,
                         int valueOffset, datCallback callback, const char *memo )
{
    UpdateCombo( title, numitems, list, valueOffset, callback, memo );
}

void bkCombo::UpdateCombo( const char *title, void * /*value*/, int numitems, const char **list,
                         datCallback callback, const char *memo )
{
    UpdateCombo( title, numitems, list, m_Offset, callback, memo );
}

void bkCombo::UpdateCombo( const char *title, int numitems, const char **list,
                          int valueOffset, datCallback callback, const char *memo ) 
{
	++RAGE_LOG_DISABLE;
    // replace title if needed
    if ( m_Title && (m_Title != title) ) 
    {
        ConstStringFree( m_Title );
        m_Title = 0;
    }

    if ( title && (m_Title != title) )
    {
        m_Title = ConstStringDuplicate( title );
    }

    // delete items
	if ( m_Items )
	{
		for (int i = 0; i < m_Count; ++i)
		{
			ConstStringFree( m_Items[i] );
			m_Items[i] = 0;
		}
	}

    if ( m_Items )
    {
        delete [] m_Items;
    }	

    // set new count
#if __WIN32PC
    int oldCount = m_Count;
#endif
    m_Count = numitems;

    // allocate new items and copy them over
	m_Items = rage_new const char*[numitems];
	for (int i = 0; i < numitems; ++i)
	{
		m_Items[i] = list ? ConstStringDuplicate(list[i]) : 0;
	}

    // set new offset
    m_Offset = valueOffset;

    // set new callback
	if (m_ExtraData)
	{
		m_ExtraData->m_Callback = callback;
	}
	else if (!(callback == NullCB))
	{
		m_ExtraData = rage_new ExtraData;
		m_ExtraData->m_Callback = callback;
	}

    // replace tooltip if needed 
	if (m_ExtraData)
	{
	    if ( m_ExtraData->m_Tooltip && (m_ExtraData->m_Tooltip != memo) )
		{
			ConstStringFree( m_ExtraData->m_Tooltip );
			m_ExtraData->m_Tooltip = 0;
		}
	}

	if (memo)
	{
		if (!m_ExtraData)
		{
			m_ExtraData = rage_new ExtraData;
		}

		if (m_ExtraData->m_Tooltip != memo)
		{
			m_ExtraData->m_Tooltip = ConstStringDuplicate(memo);
		}
	}

    // dispatch events to win/rag
#if __WIN32PC
	if (m_Hwnd)
	{
        // clear selected item
        SendMessage( m_Hwnd, CB_SETCURSEL, (WPARAM)-1, 0 );

        // clear old list
        for (int i = 0; i < oldCount; ++i)
        {
            SendMessage( m_Hwnd, CB_DELETESTRING, 0, 0 );
        }

        // add new list
		for (int i = 0; i < m_Count; ++i)
		{
            SendMessage( m_Hwnd, CB_DELETESTRING, i, 0 );   // middle item didn't get cleared for some reason, so make sure we delete again

            if ( m_Items[i] )
            {
                SendMessage( m_Hwnd, CB_INSERTSTRING, i, (LPARAM)m_Items[i] );
            }
            else
            {
                SendMessage( m_Hwnd, CB_INSERTSTRING, i, (LPARAM)"" );
            }
		}

        // set selection
        SendMessage( m_Hwnd, CB_SETCURSEL, GetValue() - m_Offset, 0 );
	}
	else
#endif
	{
		RemoteUpdateCombo();
	}
	--RAGE_LOG_DISABLE;
}

void bkCombo::GetStringRepr( char *buf, int bufLen )
{
    const char *pStringValue = GetString( GetValue() );

    char cBuf[256];
    formatf( cBuf, "%s", pStringValue ? pStringValue : "" );
    safecpy( buf, cBuf, bufLen );
}

void bkCombo::SetStringRepr( const char *buf )
{
    int i = GetStringIndex( buf );
    SetValue( i );
}

#endif
