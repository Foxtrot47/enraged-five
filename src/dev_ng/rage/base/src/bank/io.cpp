//
// bank/io.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "io.h"

#include "bank.h"
#include "bkmgr.h"

using namespace rage;

bkIo* bkIo::sm_Instance;

#if __XENON
int bkIo::sm_BaseX = 40;
#else
int bkIo::sm_BaseX = 24;
#endif

int bkIo::sm_BaseY = 40;
int bkIo::sm_PadIndex = 0;

#if __BANK
bkBank *bkIo::sm_pBank = NULL;
#endif

bkIo::bkIo() 
{
	bkAssertf(!sm_Instance, "Only one bkIo instance is allowed");
	sm_Instance = this;
}

bkIo::~bkIo() 
{
	sm_Instance = 0;	
}

#if __BANK

void bkIo::AddWidgets()
{
#if !__WIN32PC
	if ( !bkRemotePacket::IsConnectedToRag() && bkManager::IsEnabled() )
	{
		sm_pBank = BANKMGR.FindBank( "rage - Bank" );
		if ( sm_pBank == NULL )
		{
			sm_pBank = &BANKMGR.CreateBank( "rage - Bank" );
		}

		sm_pBank->AddSlider("Bank X Offset", &sm_BaseX, 0, 640, 4);
		sm_pBank->AddSlider("Bank Y Offset", &sm_BaseY, 0, 448, 4);
		bkWidget::AddStaticWidgets( *sm_pBank );
	}
#endif
}

void bkIo::RemoveWidgets()
{
	if ( bkManager::IsEnabled() )
	{
		bkBank *pBank = BANKMGR.FindBank( "rage - Bank" );
		if ( pBank != NULL )
		{
			BANKMGR.DestroyBank( *pBank );
		}
		sm_pBank = NULL;
	}
}

#endif	// __BANK
