// 
// bank/bkvector.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#if __BANK

#include "bkvector.h"

#include "pane.h"

#include "atl/string.h"
#include "math/amath.h"
#include "system/stack.h"
#include "system/xtl.h"

using namespace rage;

#if __WIN32PC

static WNDPROC s_oldVectorVectorWindowEditMessageCallback;

static LRESULT CALLBACK VectorWindowEditMessageCallback( HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam )
{
	bkVector *vectorWidget = (bkVector *)GetWindowLongPtr( hwnd, GWLP_USERDATA );

	switch ( iMsg )
	{
	case WM_CREATE:
		return CallWindowProc( s_oldVectorVectorWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );

	case WM_KEYDOWN:
		if ( wParam == VK_RETURN )
		{
			::SetFocus( NULL );
			
			char buf[128];
			GetWindowText( hwnd, buf, sizeof(buf) );
			vectorWidget->SetFont( false );
			
			if ( !vectorWidget->ValidateString( buf, true ) || !vectorWidget->SetString( vectorWidget->GetComponentInFocus(), buf ) )
			{
				vectorWidget->GetString( vectorWidget->GetComponentInFocus(), buf, sizeof(buf) );
				SetWindowText( hwnd, buf );
				MessageBeep( MB_ICONHAND );
			}
			else
			{
				vectorWidget->Changed();
				return 0;
			}
			return 0;
		}
		else if ( (wParam == VK_LEFT) || (wParam == VK_RIGHT) || (wParam == VK_UP) || (wParam == VK_DOWN) 
			|| (wParam == VK_HOME) || (wParam == VK_END) )
		{
			return CallWindowProc( s_oldVectorVectorWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );
		}
		else if ( wParam == VK_TAB ) 
		{
			if ( vectorWidget->GetComponentInFocus() + 1 == vectorWidget->GetNumComponents() )
			{
				vectorWidget->SetComponentInFocus( 0, false );

				bkWidget *i = vectorWidget->GetNext();
				while ( i && !i->SetFocus() )
				{
					i = i->GetNext();
				}

				if ( !i )
				{
					i = vectorWidget->GetParent()->GetChild();
					while ( i && !i->SetFocus() )
					{
						i = i->GetNext();
					}
				}

				bkDisplayf( "Next control!" );
			}
			else
			{
				vectorWidget->SetComponentInFocus( vectorWidget->GetComponentInFocus() + 1, true );
			}

			return 0;
		}

		return 0;

	case WM_CHAR:
		if ( wParam == '\t' ) 
		{
			return 0;
		}
		else if ( wParam != '\r' )
		{
			if ( !vectorWidget->CheckKey( (char)wParam) )
			{
				return 0;
			}

			vectorWidget->SetFont( true );
			
			return CallWindowProc( s_oldVectorVectorWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );
		}
		else 
		{
			return 0;
		}

	case WM_SETFOCUS:
		vectorWidget->SetWindowInFocus( hwnd );
		::SendMessage( hwnd, EM_SETSEL, 0, ~0 );
		return CallWindowProc( s_oldVectorVectorWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );

	case WM_KILLFOCUS:
		char buf[128];
		GetWindowText( hwnd, buf, sizeof(buf) );
		vectorWidget->SetFont( false );
		
		if ( !vectorWidget->ValidateString( buf, true ) || !vectorWidget->SetString( vectorWidget->GetComponentInFocus(), buf ) )
		{
			vectorWidget->GetString( vectorWidget->GetComponentInFocus(), buf, sizeof(buf) );
			SetWindowText( hwnd, buf );
			MessageBeep( MB_ICONHAND );
		}
		else
		{
			vectorWidget->Changed();
		}

		return CallWindowProc( s_oldVectorVectorWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );

	default:
		return CallWindowProc( s_oldVectorVectorWindowEditMessageCallback, hwnd, iMsg, wParam, lParam );
		break;
	}
}

static WNDPROC s_oldWindowScrollMessageCallback;

static LRESULT CALLBACK WindowScrollMessageCallback( HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam )
{
	bkVector *vectorWidget = (bkVector *)GetWindowLongPtr( hwnd, GWLP_USERDATA );

	switch ( iMsg )
	{
	case WM_KEYDOWN:
		if ( wParam == VK_TAB ) 
		{
			if ( vectorWidget->GetComponentInFocus() + 1 == vectorWidget->GetNumComponents() )
			{
				vectorWidget->SetComponentInFocus( 0, false );

				bkWidget *i = vectorWidget->GetNext();
				while ( i && !i->SetFocus() )
				{
					i = i->GetNext();
				}

				if ( !i )
				{
					i = vectorWidget->GetParent()->GetChild();
					while ( i && !i->SetFocus() )
					{
						i = i->GetNext();
					}
				}

				bkDisplayf( "Next control!" );
			}
			else
			{
				vectorWidget->SetComponentInFocus( vectorWidget->GetComponentInFocus() + 1, true );
			}

			return 0;
		}
		return CallWindowProc( s_oldWindowScrollMessageCallback, hwnd, iMsg, wParam, lParam );

	case WM_SETFOCUS:
		vectorWidget->SetWindowInFocus( hwnd );
		return CallWindowProc( s_oldWindowScrollMessageCallback, hwnd, iMsg, wParam, lParam );

	default:
		return CallWindowProc( s_oldWindowScrollMessageCallback, hwnd, iMsg, wParam, lParam );
	}
}

#endif // __WIN32PC

//#############################################################################

const float bkVector::FLOAT_MIN_VALUE = -FLT_MAX / 1000.0f;
const float bkVector::FLOAT_MAX_VALUE = +FLT_MAX / 1000.0f;

bkVector::bkVector( datCallback &callback, const char *title, const char *memo, EnumType enumType, float min, float max, float step, const char *fillColor, bool readOnly )
: bkWidget(callback,title,memo,fillColor,readOnly)
, m_enumType(enumType)
, m_minimum(min)
, m_maximum(max)
, m_step(step)
, m_drawLocalExpanded(0)
{
#if __WIN32PC
	m_titleWindowHandle = NULL;

	for ( int i = 0; i < 12; ++i )
	{
		m_windowHandles[i] = NULL;
	}

	m_componentInFocus = 0;
#endif
}

bkVector::~bkVector()
{

}

bool bkVector::SetFocus() 
{
#if __WIN32PC
	if ( m_windowHandles[(m_componentInFocus * 3) + 1] != NULL )
	{
		::SetFocus( m_windowHandles[(m_componentInFocus * 3) + 1] );
	}
#endif

	sm_Focus = this;
	return m_step != 0.0f;
}

void bkVector::GetStringRepr( char *buf, int bufLen )
{
    char cBuf[512];
    
    formatf( cBuf, "%f", GetValue( 0 ) );
    for ( int i = 1; i < GetNumComponents(); ++i )    
    {
        safecatf( cBuf, ",%f", GetValue( i ) );
    }

    safecpy( cBuf, buf, bufLen );
}

void bkVector::SetStringRepr( const char *buf )
{
    atString strBuf( buf );
    atArray<atString> split;
    strBuf.Split( split, ',' );
    
    for ( int i = 0; (i < GetNumComponents()) && (i < split.GetCount()); ++i )
    {
        split[i].Trim();
        SetString( i, split[i].c_str() );
    }

    Changed();
}

void bkVector::RemoteCreate()
{
	bkWidget::RemoteCreate();

	bkRemotePacket p;
	p.Begin( bkRemotePacket::CREATE, GetGuid(), this );
	p.WriteWidget (m_Parent );
	p.Write_const_char( m_Title );
	p.Write_const_char( GetTooltip() );
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
	WriteValues( p );
	p.Write_float( m_minimum );
	p.Write_float( m_maximum );
	p.Write_float( m_step );
	p.Send();
}

void bkVector::RemoteUpdate()
{
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
	p.Begin( bkRemotePacket::CHANGED, GetGuid(), this );
	WriteValues( p );
	p.Send();
}

#if __WIN32PC

void bkVector::WindowCreate()
{
	if ( !bkRemotePacket::IsConnectedToRag() )
	{	
		m_titleWindowHandle= GetPane()->AddWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX | SS_NOTIFY );

		for ( int i = 0; i < (int)m_enumType; ++i )
		{
			AddComponentWindow( i );
		}		
	}
}

void bkVector::AddComponentWindow( int component )
{
	char* text[] = { "X", "Y", "Z", "W" };
	bkPane *pane = GetPane();

	int index = component * 3;

	m_windowHandles[index] = pane->AddWindow( this, "STATIC", 0, SS_CENTER | SS_NOPREFIX, text[component], GetTooltip() );

	m_windowHandles[index + 1] = pane->AddWindow( this, m_step == 0.0f ? "STATIC" : "EDIT", 
		0, m_step == 0.0f ? SS_CENTER | SS_NOPREFIX : ES_AUTOHSCROLL | ES_RIGHT | WS_BORDER, "", GetTooltip() );

	if ( m_windowHandles[index + 1] && (m_step > 0.0f) )
	{
		s_oldVectorVectorWindowEditMessageCallback = (WNDPROC)SetWindowLongPtr( m_windowHandles[index + 1], GWLP_WNDPROC,
			(LONG_PTR)VectorWindowEditMessageCallback );
	}

	if ( component < GetNumComponents() - 1 )
	{
		m_windowHandles[index + 2] = pane->AddWindow( this, "SCROLLBAR", 0, SBS_VERT );

		if ( m_windowHandles[index + 2] && (m_step > 0.0f) )
		{
			s_oldWindowScrollMessageCallback = (WNDPROC)SetWindowLongPtr( m_windowHandles[index + 2], GWLP_WNDPROC,
				(LONG_PTR)WindowScrollMessageCallback );
		}
	}
	else
	{
		pane->AddLastWindow( this, "SCROLLBAR", 0, SBS_VERT );

		if ( m_Hwnd && (m_step > 0.0f) )
		{
			s_oldWindowScrollMessageCallback = (WNDPROC)SetWindowLongPtr( m_Hwnd, GWLP_WNDPROC,
				(LONG_PTR)WindowScrollMessageCallback );
		}
	}
}

void bkVector::WindowUpdate()
{
	for ( int i = 0; i < (int)m_enumType; ++i )
	{
		char buffer[128];		
		SetWindowText( m_windowHandles[(i * 3) + 1], GetString( i, buffer, sizeof(buffer) ) );
	}		
}

void bkVector::WindowDestroy()
{
	if ( m_titleWindowHandle != NULL )
	{
		DestroyWindow( m_titleWindowHandle );
	}

	for ( int i = 0; i < 12; ++i )
	{
		if ( m_windowHandles[i] != NULL )
		{
			DestroyWindow( m_windowHandles[i] );
			m_windowHandles[i] = NULL;
		}
	}

	bkWidget::WindowDestroy();
}

rageLRESULT bkVector::WindowMessage( rageUINT msg, rageWPARAM wParam, rageLPARAM /*lParam*/ )
{
	if ( msg == WM_VSCROLL )
	{
		if ( LOWORD(wParam) == SB_LINEUP )
		{				
			WindowMessage( DIAL, +1 );
		}
		else if ( LOWORD(wParam) == SB_LINEDOWN )
		{
			WindowMessage( DIAL, -1 );
		}
	}

	return 0;
}

void bkVector::WindowMessage( Action action, float value )
{
	if ( (action == DIAL) && (m_step > 0.0f) )
	{ 
		SetWindowInFocus( bkPane::WindowMessageHwnd );

		if ( SetValue( m_componentInFocus, GetValue( m_componentInFocus ) + (value * m_step) ) )
		{
			WindowUpdate(); 
			Changed(); 
		} 
	} 
}

int bkVector::WindowResize( int x, int y, int width, int height )
{
	int itemWidth = (width / 5);
	
	if ( m_titleWindowHandle != NULL )
	{
		SetWindowPos( m_titleWindowHandle, 0, x, y, itemWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );
	}

	for ( int i = 0; i < (int)m_enumType; ++i )
	{
		ResizeComponentWindow( i, x + ((i + 1) * itemWidth), y, itemWidth, height );
	}

	return y + height;
}

void bkVector::ResizeComponentWindow( int component, int x, int y, int width, int height )
{
	const int scrollWidth = GetSystemMetrics( SM_CXVSCROLL );

	int index = component * 3;

	if ( m_windowHandles[index] != NULL )
	{
		SetWindowPos( m_windowHandles[index], 0, x, y, scrollWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );

		int itemWidth = width - (scrollWidth * 2);
		if ( itemWidth < 0 )
		{
			itemWidth = 0;
		}

		SetWindowPos( m_windowHandles[index + 1], 0, x + scrollWidth, y, itemWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );

		if ( component < GetNumComponents() - 1 )
		{
			SetWindowPos( m_windowHandles[index + 2], 0, x + scrollWidth + itemWidth, y, scrollWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );
		}
		else
		{
			if ( m_Hwnd != NULL )
			{
				SetWindowPos( m_Hwnd, 0, x + scrollWidth + itemWidth, y, scrollWidth, height, SWP_NOACTIVATE | SWP_NOZORDER );
			}
		}
	}
}

#define MAKE_CASE_CHECK(state,chr)	(state | (chr << 16))

bool bkVector::ValidateString( const char* string, bool okOnly ) const
{
	enum EnumState		{STATE_INITIAL,STATE_IPART,STATE_FPART,STATE_ESIGN,STATE_EPART};
	enum EnumValidation {VALID_START,VALID_ERROR,VALID_PARTIAL,VALID_OK};

	// culled from http://www.codeproject.com/editctrl/validator.asp
	EnumState		state			= STATE_INITIAL;
	EnumValidation	validateState	= VALID_START;
	int				StringLengthgth		= StringLength(string);
	for ( int i = 0; validateState != VALID_ERROR && i < StringLengthgth; )
	{ 
		/* scan string */
		char ch = string[i];
		switch(MAKE_CASE_CHECK(state, ch))
		{ /* states */
		case MAKE_CASE_CHECK(STATE_INITIAL, ' '):
		case MAKE_CASE_CHECK(STATE_INITIAL, '\t'):
			i++;
			continue;

		case MAKE_CASE_CHECK(STATE_INITIAL, '+'):
			i++;
			validateState = VALID_PARTIAL;
			state = STATE_IPART;
			continue;

		case MAKE_CASE_CHECK(STATE_INITIAL, '-'):
			if ( m_minimum < 0.0f )
			{
				i++;
				validateState = VALID_PARTIAL;
				state = STATE_IPART;
			}
			else 
			{
				validateState = VALID_ERROR;
			}
			continue;

		case MAKE_CASE_CHECK(STATE_INITIAL, '0'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '1'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '2'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '3'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '4'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '5'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '6'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '7'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '8'):
		case MAKE_CASE_CHECK(STATE_INITIAL, '9'):
			state = STATE_IPART;
			continue;

		case MAKE_CASE_CHECK(STATE_INITIAL, '.'):
			i++;
			validateState = VALID_PARTIAL;
			state = STATE_FPART;
			continue;

		case MAKE_CASE_CHECK(STATE_INITIAL, 'E'):
		case MAKE_CASE_CHECK(STATE_INITIAL, 'e'):
			i++;
			state = STATE_ESIGN;
			validateState = VALID_PARTIAL;
			continue;

		case MAKE_CASE_CHECK(STATE_IPART, '0'):
		case MAKE_CASE_CHECK(STATE_IPART, '1'):
		case MAKE_CASE_CHECK(STATE_IPART, '2'):
		case MAKE_CASE_CHECK(STATE_IPART, '3'):
		case MAKE_CASE_CHECK(STATE_IPART, '4'):
		case MAKE_CASE_CHECK(STATE_IPART, '5'):
		case MAKE_CASE_CHECK(STATE_IPART, '6'):
		case MAKE_CASE_CHECK(STATE_IPART, '7'):
		case MAKE_CASE_CHECK(STATE_IPART, '8'):
		case MAKE_CASE_CHECK(STATE_IPART, '9'):
			i++;
			validateState = VALID_OK;
			continue;

		case MAKE_CASE_CHECK(STATE_IPART, '.'):
			i++;
			validateState = VALID_OK;
			state = STATE_FPART;
			continue;

		case MAKE_CASE_CHECK(STATE_IPART, 'e'):
		case MAKE_CASE_CHECK(STATE_IPART, 'E'):
			i++;
			validateState = VALID_PARTIAL;
			state = STATE_ESIGN;
			continue;

		case MAKE_CASE_CHECK(STATE_FPART, '0'):
		case MAKE_CASE_CHECK(STATE_FPART, '1'):
		case MAKE_CASE_CHECK(STATE_FPART, '2'):
		case MAKE_CASE_CHECK(STATE_FPART, '3'):
		case MAKE_CASE_CHECK(STATE_FPART, '4'):
		case MAKE_CASE_CHECK(STATE_FPART, '5'):
		case MAKE_CASE_CHECK(STATE_FPART, '6'):
		case MAKE_CASE_CHECK(STATE_FPART, '7'):
		case MAKE_CASE_CHECK(STATE_FPART, '8'):
		case MAKE_CASE_CHECK(STATE_FPART, '9'):
			i++;
			validateState = VALID_OK;
			continue;

		case MAKE_CASE_CHECK(STATE_FPART, 'e'):
		case MAKE_CASE_CHECK(STATE_FPART, 'E'):
			i++;
			validateState = VALID_PARTIAL;
			state = STATE_ESIGN;
			continue;

		case MAKE_CASE_CHECK(STATE_ESIGN, '+'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '-'):
			i++;
			validateState = VALID_PARTIAL;
			state = STATE_EPART;
			continue;

		case MAKE_CASE_CHECK(STATE_ESIGN, '0'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '1'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '2'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '3'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '4'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '5'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '6'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '7'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '8'):
		case MAKE_CASE_CHECK(STATE_ESIGN, '9'):
			state = STATE_EPART;
			continue;

		case MAKE_CASE_CHECK(STATE_EPART, '0'):
		case MAKE_CASE_CHECK(STATE_EPART, '1'):
		case MAKE_CASE_CHECK(STATE_EPART, '2'):
		case MAKE_CASE_CHECK(STATE_EPART, '3'):
		case MAKE_CASE_CHECK(STATE_EPART, '4'):
		case MAKE_CASE_CHECK(STATE_EPART, '5'):
		case MAKE_CASE_CHECK(STATE_EPART, '6'):
		case MAKE_CASE_CHECK(STATE_EPART, '7'):
		case MAKE_CASE_CHECK(STATE_EPART, '8'):
		case MAKE_CASE_CHECK(STATE_EPART, '9'):
			i++;
			validateState = VALID_OK;
			continue;

		default:
			validateState = VALID_ERROR;
			continue;
		} /* states */
	} /* scan string */

	return (validateState == VALID_OK) || (!okOnly && (validateState == VALID_PARTIAL));
}

bool bkVector::CheckKey( char key ) const
{	
	struct HWND__* hwnd = m_windowHandles[ (m_componentInFocus * 3) + 1 ];
	
	char temp1[128];
	GetWindowText( hwnd, temp1, 128 );

	unsigned int start;
	unsigned int end;
	SendMessage( hwnd, EM_GETSEL, (WPARAM)&start, (LPARAM)&end ); 

	// backspace key:
	if ( key == 0x08 )
	{
		return true;
	}
	else if ( ((key >= '0') && (key <= '9')) || (key == '-') || (key == '.') || (key == '+') || (key == 'e') || (key == 'E') )
	{
		// check to see if the created string would be valid:
		char temp2[256];
		if ( start != 0 )
		{
			strncpy( temp2, temp1, start );
		}

		const char* postfix = temp1 + end;
		temp2[start] = key;
		temp2[start+1] = NULL;
		safecat( temp2, postfix );

		return ValidateString( temp2, false );
	}
	else 
	{
		return false;
	}
}

void bkVector::SetFont( bool bold ) 
{
	struct HWND__* hwnd = m_windowHandles[ (m_componentInFocus * 3) + 1 ];

	if ( GetPane() )
	{
		if ( bold )
		{
			SendMessage( hwnd, WM_SETFONT, (WPARAM)GetPane()->GetBoldFont(), MAKELPARAM(TRUE, 0) );	
		}
		else
		{
			SendMessage( hwnd, WM_SETFONT, (WPARAM)GetPane()->GetFont(), MAKELPARAM(TRUE, 0) );	
		}
	}
}

void bkVector::SetComponentInFocus( int component, bool setWindowFocus )
{
	if ( (component != m_componentInFocus) && (component >= 0) && (component < (int)m_enumType) )
	{
		int index = m_componentInFocus * 3;

		// deselect any highlighted text
		if ( m_windowHandles[index + 1] != NULL )
		{
			::SendMessage( m_windowHandles[index + 1], EM_SETSEL, 0, 0 );
		}

		m_componentInFocus = component;
		index = m_componentInFocus * 3;

		if ( setWindowFocus && (m_windowHandles[index + 1] != NULL) )
		{
			::SendMessage( m_windowHandles[index + 1], WM_SETFOCUS, 0, 0 );
		}

		bkDisplayf( "ComponentInFocus=%d", m_componentInFocus );
	}
}

void bkVector::SetWindowInFocus( struct HWND__* hwnd )
{
	int newComponent = m_componentInFocus;
	for ( int i = 0; i < 12; ++i )
	{
		if ( m_windowHandles[i] == hwnd )
		{
			switch ( i )
			{
			case 1:
			case 2:
				newComponent = 0;				
				break;
			case 4:
			case 5:
				newComponent = 1;
				break;
			case 7:
			case 8:
				newComponent = 2;
				break;
			case 10:
			case 11:
				newComponent = 3;
				break;
			}

			break;
		}
	}

	SetComponentInFocus( newComponent, false );
}

struct HWND__* bkVector::GetWindowInFocus() const
{
	return m_windowHandles[(m_componentInFocus * 3) + 1];
}

#endif // __WIN32PC

void bkVector::Message( Action action, float value )
{
	if ( action == DIAL )
	{
		if ( m_drawLocalExpanded )
		{
			if ( m_drawLocalExpanded > 0 )
			{
				int component = sm_Cursor - sm_TopLine - m_drawLocalExpanded;
				if ( component == 0 )
				{
					if ( value < 0.0f )
					{
						m_drawLocalExpanded = 0;
					}
				}
				else
				{
					if ( (m_step > 0.0f) && SetValue( component - 1, GetValue( component - 1 ) + (value * m_step) ) )
					{
						WindowUpdate();
						Changed();
					}
				}
			}
		}
		else
		{
			if ( value > 0.0f )
			{
				m_drawLocalExpanded = -1;
			}
		}
	}
}

int bkVector::DrawLocal( int x, int y )
{
	if ( m_drawLocalExpanded )
	{
		m_drawLocalExpanded = y;

		Drawf( x, y, "(-)%s", m_Title );
		y = bkWidget::DrawLocal( x, y );

		static const char componentNames[4][2] = { "X", "Y", "Z", "W" };
		for ( int i = 0; i < GetNumComponents(); ++i )
		{
			char value[32];
			GetString( i, value, sizeof(value) );

			Drawf( x, y, "  %s %s", componentNames[i], value );
			y = bkWidget::DrawLocal( x, y );
		}

		return y;
	}
	else
	{
		switch ( m_enumType )
		{
		case EVector2:
			{
				char value0[32];
				GetString( 0, value0, sizeof(value0) );

				char value1[32];
				GetString( 1, value1, sizeof(value1) );

				Drawf( x, y, "(+)%s X=%s, Y=%s", m_Title, value0, value1 );
			}
			break;
		case EVector3:
			{
				char value0[32];
				GetString( 0, value0, sizeof(value0) );

				char value1[32];
				GetString( 1, value1, sizeof(value1) );

				char value2[32];
				GetString( 2, value2, sizeof(value2) );

				Drawf( x, y, "(+)%s X=%s, Y=%s, Z=%s", m_Title, value0, value1, value2 );
			}
			break;
		case EVector4:
			{
				char value0[32];
				GetString( 0, value0, sizeof(value0) );

				char value1[32];
				GetString( 1, value1, sizeof(value1) );

				char value2[32];
				GetString( 2, value2, sizeof(value2) );

				char value3[32];
				GetString( 3, value3, sizeof(value3) );

				Drawf( x, y, "(+)%s X=%s, Y=%s, Z=%s, W=%s", m_Title, value0, value1, value2, value3 );
			}
			break;
		}
		
		return bkWidget::DrawLocal( x, y );
	}
}

void bkVector::RemoteHandler( const bkRemotePacket& packet, EnumType enumType )
{
	if ( packet.GetCommand() == bkRemotePacket::CREATE ) 
	{
		packet.Begin();
		u32 id = packet.GetId();
		bkWidget* parent = packet.ReadWidget<bkWidget>();
		if (!parent) return;

		const char *title = packet.Read_const_char(), *memo = packet.Read_const_char();
		const char *fillColor = packet.Read_const_char();
		bool readOnly = packet.Read_bool();
		
		float values[4];
		for ( int i = 0; i < enumType; ++i )
		{
			values[i] = packet.Read_float();
		}

		float min = packet.Read_float();
		float max = packet.Read_float();
		float step = packet.Read_float();
		packet.End();

		bkVector *widget = NULL;
		switch ( enumType )
		{
		case EVector2:
			widget = rage_new bkVector2( NullCB, title, memo, rage_new Vector2( values[0], values[1] ), min, max, step, fillColor, readOnly );
			break;
		case EVector3:
			widget = rage_new bkVector3( NullCB, title, memo, rage_new Vector3( values[0], values[1], values[2] ), min, max, step, fillColor, readOnly );
			break;
		case EVector4:
			widget = rage_new bkVector4( NullCB, title, memo, rage_new Vector4( values[0], values[1], values[2], values[3] ), min, max, step, fillColor, readOnly );
			break;
		}

		if ( widget != NULL )
		{
			bkRemotePacket::SetWidgetId( *widget, id );
			parent->AddChild( *widget );
		}
	}
	else if ( packet.GetCommand() == bkRemotePacket::CHANGED ) 
	{
		packet.Begin();

		bkVector *widget = NULL;
		switch ( enumType )
		{
		case EVector2:
			widget = packet.ReadWidget<bkVector2>();
			break;
		case EVector3:
			widget = packet.ReadWidget<bkVector3>();
			break;
		case EVector4:
			widget = packet.ReadWidget<bkVector4>();
			break;
		}

		if ( !widget )
		{
			return;
		}

		widget->ReadValues( packet );

		packet.End();

		widget->WindowUpdate();
		widget->Changed();
	}
	else if ( (packet.GetCommand() >= bkRemotePacket::USER)
		&& (packet.GetCommand() <= bkRemotePacket::USER + 4) )
	{
		packet.Begin();
		
		bkVector *widget = NULL;
		switch ( enumType )
		{
		case EVector2:
			widget = packet.ReadWidget<bkVector2>();
			break;
		case EVector3:
			widget = packet.ReadWidget<bkVector3>();
			break;
		case EVector4:
			widget = packet.ReadWidget<bkVector4>();
			break;
		}

		if ( !widget )
		{
			return;
		}

		Operation op = (Operation)packet.Read_s32();

		// vector operation, 1 float argument
		switch ( packet.GetCommand() )
		{
		case bkRemotePacket::USER:
			{
				widget->ExecuteOperation( op );
			}
			break;
		case bkRemotePacket::USER + 1:
			{
				float f = packet.Read_float();
				
				widget->ExecuteOperation( op, f );
			}
			break;
		case bkRemotePacket::USER + 2:
			{
				float vec[4];
				widget->ReadValues( packet, vec );

				widget->ExecuteOperation( op, vec );
			}
			break;
		case bkRemotePacket::USER + 3:
			{
				float f = packet.Read_float();

				float vec[4];
				widget->ReadValues( packet, vec );

				widget->ExecuteOperation( op, f, vec );
			}
			break;
		case bkRemotePacket::USER + 4:
			{
				float f = packet.Read_float();
				int i = packet.Read_s32();

				widget->ExecuteOperation( op, f, i );
			}
			break;
		default:
			break;
		}

		packet.End();

		widget->WindowUpdate();
		widget->Changed();
	}
}

void bkVector::CheckInitialValues()
{
	char buffer[256];

	if ( (m_minimum < FLOAT_MIN_VALUE) || (m_maximum > FLOAT_MAX_VALUE) )
	{
		formatf( buffer, sizeof(buffer), 
			"Widget '%s' min and max values (%.3f,%.3f) are out of allowed range.\n\nClick Ok to clamp it and continue, or Cancel to exit.",
			GetTitle(), m_minimum, m_maximum ); 
		bkErrorf( "%s", buffer ); 
		sysStack::PrintStackTrace();

		int result = fiRemoteShowMessageBox( buffer, sysParam::GetProgramName(), MB_ICONERROR | MB_OKCANCEL | MB_TOPMOST, IDOK );
		if ( result == IDOK ) 
		{ 
			m_minimum = Clamp( m_minimum, FLOAT_MIN_VALUE, FLOAT_MAX_VALUE ); 
			m_maximum = Clamp( m_maximum, FLOAT_MIN_VALUE, FLOAT_MAX_VALUE ); 
		} 
		else
		{
			Quitf("User-requested abort."); 
		}
	} 

	if ( m_step )
	{
		static const char componentNames[4][2] = { "X", "Y", "Z", "W" };
		for ( int i = 0; i < GetNumComponents(); ++i )
		{
			float value = GetValue( i );
			if ( !((value >= m_minimum) && (value <= m_maximum)) ) 
			{ 
				formatf( buffer, sizeof(buffer), 
					"Widget '%s' initial value %s=%.3f out of allowed range (%.3f,%.3f).\n\nClick Ok to clamp it and continue, or Cancel to exit.", 
					GetTitle(), componentNames[i], value, m_minimum, m_maximum ); 
				bkErrorf( "%s", buffer ); 
				sysStack::PrintStackTrace();

				int result = fiRemoteShowMessageBox( buffer, sysParam::GetProgramName(), MB_ICONERROR | MB_OKCANCEL | MB_TOPMOST, IDOK );
				if ( result == IDOK ) 
				{
					SetValue( i, Clamp( value, m_minimum, m_maximum ) );
				}
				else 
				{
					Quitf( "User-requested abort." ); 
				}
			} 
		}
	}
}

//#############################################################################

bkVector2::bkVector2( datCallback &callback, const char *title, const char *memo, Vector2 *data, float min, float max, float step, const char* fillColor, bool readOnly )
: bkVector( callback, title, memo, EVector2, min, max, step, fillColor, readOnly )
, m_pValue(data), m_prevValue(*data)
{
	CheckInitialValues();
}

bkVector2::~bkVector2()
{

}

void bkVector2::ReadValues( const bkRemotePacket& packet )
{
	(*m_pValue)[0] = packet.Read_float();
	(*m_pValue)[1] = packet.Read_float();

	m_prevValue = *m_pValue;
}

void bkVector2::ReadValues( const bkRemotePacket& packet, float vec[] )
{
	vec[0] = packet.Read_float();
	vec[1] = packet.Read_float();
}

void bkVector2::WriteValues( bkRemotePacket& packet )
{
	packet.Write_float( (*m_pValue)[0] );
	packet.Write_float( (*m_pValue)[1] );
}

void bkVector2::ExecuteOperation( Operation op )
{
	switch ( op )
	{
	case ENegate:
		m_pValue->Negate();
		break;
	case ENormalize:
		m_pValue->Normalize();
		break;
	case ENormalizeSafe:
		m_pValue->NormalizeSafe();
		break;
	default:
		break;
	}
}

void bkVector2::ExecuteOperation( Operation op, float f )
{
	switch ( op )
	{
	case EScale:
		m_pValue->Scale( f );
		break;
	case EInvScale:
		m_pValue->InvScale( f );
		break;
	case ERotate:
		m_pValue->Rotate( f );
		break;
	case ERotateY:
		m_pValue->RotateY( f );
		break;
	default:
		break;
	}
}

void bkVector2::ExecuteOperation( Operation op, float vec[] )
{
	switch ( op )
	{
	case EAdd:
		m_pValue->Add( Vector2( vec[0], vec[1] ) );
		break;
	case ESubtract:
		m_pValue->Subtract( Vector2( vec[0], vec[1] ) );
		break;
	case EMultiply:
		m_pValue->Multiply( Vector2( vec[0], vec[1] ) );
		break;
	case EAverage:
		m_pValue->Average( *m_pValue, Vector2( vec[0], vec[1] ) );
		break;
	default:
	    break;
	}
}

void bkVector2::ExecuteOperation( Operation op, float f, float vec[] )
{
	switch ( op )
	{
	case ELerp:
		m_pValue->Lerp( f, *m_pValue, Vector2( vec[0], vec[1] ) );
		break;
	default:
		break;
	}
}

void bkVector2::ClampValuesToAllowedRange()
{
	if ( (*m_pValue)[0] < m_minimum )
	{
		(*m_pValue)[0] = m_minimum;
	}
	else if ( (*m_pValue)[0] < m_maximum )
	{
		(*m_pValue)[0] = m_maximum;
	}

	if ( (*m_pValue)[1] < m_minimum )
	{
		(*m_pValue)[1] = m_minimum;
	}
	else if ( (*m_pValue)[1] < m_maximum )
	{
		(*m_pValue)[1] = m_maximum;
	}
}

bool bkVector2::SetValue( int component, float f )
{
	if ( (component >= 0) && (component < (int)m_enumType) ) 
	{ 
		float value = Clamp( f, m_minimum, m_maximum );
		float oldValue = (*m_pValue)[component];

		(*m_pValue)[component] = value;

		return value != oldValue; 
	} 
	else
	{
		return false;
	} 
}

float bkVector2::GetValue( int component )
{
	if ( (component >= 0) && (component < (int)m_enumType) )
	{
		return (*m_pValue)[component];
	}
	
	return 0.0f;
}

bool bkVector2::SetString( int component, const char* src )
{
	return SetValue( component, (float)atof( src ) );
}

const char* bkVector2::GetString( int component, char* dest, int destSize )
{
	float val = GetValue( component );
	if ( (val >= m_minimum) && (val <= m_maximum) )
	{
		formatf( dest, destSize, "%1.03f", val );
	}
	else
	{
		safecpy( dest, "**invalid**", destSize );
	}

	return dest;	
}

void bkVector2::RemoteHandler( const bkRemotePacket& packet )
{
	bkVector::RemoteHandler( packet, EVector2 );
}

//#############################################################################

bkVector3::bkVector3( datCallback &callback, const char *title, const char *memo, Vector3 *data, float min, float max, float step, const char* fillColor, bool readOnly )
: bkVector( callback, title, memo, EVector3, min, max, step, fillColor, readOnly )
, m_pValue(data), m_prevValue(*data)
{
	CheckInitialValues();
}

bkVector3::~bkVector3()
{

}

void bkVector3::ReadValues( const bkRemotePacket& packet )
{
	m_pValue->SetX( packet.Read_float() );
	m_pValue->SetY( packet.Read_float() );
	m_pValue->SetZ( packet.Read_float() );

	m_prevValue = *m_pValue;
}

void bkVector3::ReadValues( const bkRemotePacket& packet, float vec[] )
{
	vec[0] = packet.Read_float();
	vec[1] = packet.Read_float();
	vec[2] = packet.Read_float();
}

void bkVector3::WriteValues( bkRemotePacket& packet )
{
	packet.Write_float( m_pValue->GetX() );
	packet.Write_float( m_pValue->GetY() );
	packet.Write_float( m_pValue->GetZ() );
}

void bkVector3::ExecuteOperation( Operation op )
{
	switch ( op )
	{
	case ENegate:
		m_pValue->Negate();
		break;
	case EAbs:
		m_pValue->Abs();
		break;
	case ENormalize:
		m_pValue->Normalize();
		break;
	case ENormalizeSafe:
		m_pValue->NormalizeSafe();
		break;
	case ENormalizeSaveV:
		m_pValue->NormalizeSafeV();
		break;
	case ENormalizeFast:
		m_pValue->NormalizeFast();
		break;
	case ELog:
		m_pValue->Log();
		break;
	case ELog10:
		m_pValue->Log10();
		break;
	default:
	    break;
	}
}

void bkVector3::ExecuteOperation( Operation op, float f )
{
	switch ( op )
	{
	case EScale:
		m_pValue->Scale( f );
		break;
	case EInvScale:
		m_pValue->InvScale( f );
		break;
	case EExtend:
		m_pValue->Extend( f );
		break;
	case ERotateX:
		m_pValue->RotateX( f );
		break;
	case ERotateY:
		m_pValue->RotateY( f );
		break;
	case ERotateZ:
		m_pValue->RotateZ( f );
		break;
	default:
		break;
	}
}

void bkVector3::ExecuteOperation( Operation op, float vec[] )
{
	switch ( op )
	{
	case EAdd:
		m_pValue->Add( Vector3( vec[0], vec[1], vec[2] ) );
		break;
	case ESubtract:
		m_pValue->Subtract( Vector3( vec[0], vec[1], vec[2] ) );
		break;
	case EMultiply:
		m_pValue->Multiply( Vector3( vec[0], vec[1], vec[2] ) );
		break;
	case EAverage:
		m_pValue->Average( *m_pValue, Vector3( vec[0], vec[1], vec[2] ) );
		break;
	case ECross:
		m_pValue->Cross( *m_pValue, Vector3( vec[0], vec[1], vec[2] ) );
		break;
	case EDotV:
		m_pValue->DotV( *m_pValue, Vector3( vec[0], vec[1], vec[2] ) );
		break;
	case EReflectAbout:
		m_pValue->ReflectAbout( Vector3( vec[0], vec[1], vec[2] ) );
		break;
	case EReflectAboutFast:
		m_pValue->ReflectAboutFast( Vector3( vec[0], vec[1], vec[2] ) );
		break;
	case EAddNet:
		m_pValue->AddNet( Vector3( vec[0], vec[1], vec[2] ) );
		break;
	default:
		break;
	}
}

void bkVector3::ExecuteOperation( Operation op, float f, float vec[] )
{
	switch ( op )
	{
	case ELerp:
		m_pValue->Lerp( f, *m_pValue, Vector3( vec[0], vec[1], vec[2] ) );
		break;
	default:
		break;
	}
}

void bkVector3::ExecuteOperation( Operation op, float f, int i )
{
	switch ( op )
	{
	case ERotateAboutAxis:
		m_pValue->RotateAboutAxis( f, i );
		break;
	default:
		break;
	}
}

void bkVector3::ClampValuesToAllowedRange()
{
	if ( m_pValue->GetX() < m_minimum )
	{
		m_pValue->SetX( m_minimum );
	}
	else if ( m_pValue->GetX() > m_maximum )
	{
		m_pValue->SetX( m_maximum );
	}

	if ( m_pValue->GetY() < m_minimum )
	{
		m_pValue->SetY( m_minimum );
	}
	else if ( m_pValue->GetY() > m_maximum )
	{
		m_pValue->SetY( m_maximum );
	}

	if ( m_pValue->GetZ() < m_minimum )
	{
		m_pValue->SetZ( m_minimum );
	}
	else if ( m_pValue->GetZ() > m_maximum )
	{
		m_pValue->SetZ( m_maximum );
	}
}

bool bkVector3::SetValue( int component, float f )
{
	if ( (component >= 0) && (component < (int)m_enumType) ) 
	{ 
		float value = Clamp( f, m_minimum, m_maximum );
		float oldValue = value;

		switch ( component )
		{
		case 0:
			oldValue = m_pValue->GetX();
			m_pValue->SetX( value );
			break;
		case 1:
			oldValue = m_pValue->GetY();
			m_pValue->SetY( value );
			break;
		case 2:
			oldValue = m_pValue->GetZ();
			m_pValue->SetZ( value );
			break;
		default:
			break;
		}

		return value != oldValue; 
	} 
	else
	{
		return false;
	} 
}

float bkVector3::GetValue( int component )
{
	if ( (component >= 0) && (component < (int)m_enumType) )
	{
		switch ( component )
		{
		case 0:
			return m_pValue->GetX();
		case 1:
			return m_pValue->GetY();
		case 2:
			return m_pValue->GetZ();
		default:
			break;
		}
	}

	return 0.0f;
}

bool bkVector3::SetString( int component, const char* src )
{
	return SetValue( component, (float)atof( src ) );
}

const char* bkVector3::GetString( int component, char* dest, int destSize )
{
	float val = GetValue( component );
	if ( (val >= m_minimum) && (val <= m_maximum) )
	{
		formatf( dest, destSize, "%1.03f", val );
	}
	else
	{
		safecpy( dest, "**invalid**", destSize );
	}

	return dest;	
}

void bkVector3::RemoteHandler( const bkRemotePacket& packet )
{
	bkVector::RemoteHandler( packet, EVector3 );
}

//#############################################################################

bkVector4::bkVector4( datCallback &callback, const char *title, const char *memo, Vector4 *data, float min, float max, float step, const char* fillColor, bool readOnly )
: bkVector( callback, title, memo, EVector4, min, max, step, fillColor, readOnly )
, m_pValue(data), m_prevValue(*data)
{
	CheckInitialValues();
}

bkVector4::~bkVector4()
{

}

void bkVector4::ReadValues( const bkRemotePacket& packet )
{
	m_pValue->SetX( packet.Read_float() );
	m_pValue->SetY( packet.Read_float() );
	m_pValue->SetZ( packet.Read_float() );
	m_pValue->SetW( packet.Read_float() );

	m_prevValue = *m_pValue;
}

void bkVector4::ReadValues( const bkRemotePacket& packet, float vec[] )
{
	vec[0] = packet.Read_float();
	vec[1] = packet.Read_float();
	vec[2] = packet.Read_float();
	vec[3] = packet.Read_float();
}

void bkVector4::WriteValues( bkRemotePacket& packet )
{
	packet.Write_float( m_pValue->GetX() );
	packet.Write_float( m_pValue->GetY() );
	packet.Write_float( m_pValue->GetZ() );
	packet.Write_float( m_pValue->GetW() );
}

void bkVector4::ExecuteOperation( Operation op )
{
	switch ( op )
	{
	case EInvert:
		m_pValue->Invert();
		break;
	case EInvertSafe:
		m_pValue->InvertSafe();
		break;
	case ENegate:
		m_pValue->Negate();
		break;
	case EAbs:
		m_pValue->Abs();
		break;
	case ENormalize:
		m_pValue->Normalize();
		break;
	case ENormalizeFast:
		m_pValue->NormalizeFast();
		break;
	case ENormalize3:
		m_pValue->Normalize3();
		break;
	case ENormalizeFast3:
		m_pValue->NormalizeFast3();
		break;
	case ELog:
		m_pValue->Log();
		break;
	case ELog10:
		m_pValue->Log10();
		break;
	default:
		break;
	}
}

void bkVector4::ExecuteOperation( Operation op, float f )
{
	switch ( op )
	{
	case EScale:
		m_pValue->Scale( f );
		break;
	case EInvScale:
		m_pValue->InvScale( f );
		break;
	case EScale3:
		m_pValue->Scale3( f );
		break;
	default:
		break;
	}
}

void bkVector4::ExecuteOperation( Operation op, float vec[] )
{
	switch ( op )
	{
	case EAdd:
		m_pValue->Add( Vector4( vec[0], vec[1], vec[2], vec[3] ) );
		break;
	case ESubtract:
		m_pValue->Subtract( Vector4( vec[0], vec[1], vec[2], vec[3] ) );
		break;
	case EMultiply:
		m_pValue->Multiply( Vector4( vec[0], vec[1], vec[2], vec[3] ) );
		break;
	case EAverage:
		m_pValue->Average( *m_pValue, Vector4( vec[0], vec[1], vec[2], vec[3] ) );
		break;
	case ECross:
		m_pValue->Cross( *m_pValue, Vector4( vec[0], vec[1], vec[2], vec[3] ) );
		break;
	case EDotV:
		*m_pValue = m_pValue->DotV( Vector4( vec[0], vec[1], vec[2], vec[3] ) );
		break;
	case EDot3V:
		*m_pValue = m_pValue->Dot3V( Vector4( vec[0], vec[1], vec[2], vec[3] ) );
		break;
	default:
		break;
	}
}

void bkVector4::ExecuteOperation( Operation op, float f, float vec[] )
{
	switch ( op )
	{
	case ELerp:
		m_pValue->Lerp( f, *m_pValue, Vector4( vec[0], vec[1], vec[2], vec[3] ) );
		break;
	default:
		break;
	}
}

void bkVector4::ClampValuesToAllowedRange()
{
	if ( m_pValue->GetX() < m_minimum )
	{
		m_pValue->SetX( m_minimum );
	}
	else if ( m_pValue->GetX() > m_maximum )
	{
		m_pValue->SetX( m_maximum );
	}

	if ( m_pValue->GetY() < m_minimum )
	{
		m_pValue->SetY( m_minimum );
	}
	else if ( m_pValue->GetY() > m_maximum )
	{
		m_pValue->SetY( m_maximum );
	}

	if ( m_pValue->GetZ() < m_minimum )
	{
		m_pValue->SetZ( m_minimum );
	}
	else if ( m_pValue->GetZ() > m_maximum )
	{
		m_pValue->SetZ( m_maximum );
	}

	if ( m_pValue->GetW() < m_minimum )
	{
		m_pValue->SetW( m_minimum );
	}
	else if ( m_pValue->GetW() > m_maximum )
	{
		m_pValue->SetW( m_maximum );
	}
}

bool bkVector4::SetValue( int component, float f )
{
	if ( (component >= 0) && (component < (int)m_enumType) ) 
	{ 
		float value = Clamp( f, m_minimum, m_maximum );
		float oldValue = value;

		switch ( component )
		{
		case 0:
			oldValue = m_pValue->GetX();
			m_pValue->SetX( value );
			break;
		case 1:
			oldValue = m_pValue->GetY();
			m_pValue->SetY( value );
			break;
		case 2:
			oldValue = m_pValue->GetZ();
			m_pValue->SetZ( value );
			break;
		case 3:
			oldValue = m_pValue->GetW();
			m_pValue->SetW( value );
			break;
		default:
			break;
		}

		return value != oldValue; 
	} 
	else
	{
		return false;
	} 
}

float bkVector4::GetValue( int component )
{
	if ( (component >= 0) && (component < (int)m_enumType) )
	{
		switch ( component )
		{
		case 0:
			return m_pValue->GetX();
		case 1:
			return m_pValue->GetY();
		case 2:
			return m_pValue->GetZ();
		case 3:
			return m_pValue->GetW();
		default:
			break;
		}
	}

	return 0.0f;
}

bool bkVector4::SetString( int component, const char* src )
{
	return SetValue( component, (float)atof( src ) );
}

const char* bkVector4::GetString( int component, char* dest, int destSize )
{
	float val = GetValue( component );
	if ( (val >= m_minimum) && (val <= m_maximum) )
	{
		formatf( dest, destSize, "%1.03f", val );
	}
	else
	{
		safecpy( dest, "**invalid**", destSize );
	}

	return dest;	
}

void bkVector4::RemoteHandler( const bkRemotePacket& packet )
{
	bkVector::RemoteHandler( packet, EVector4 );
}

#endif // __BANK
