//
// bank/text.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_TEXT_H
#define BANK_TEXT_H

#if __BANK

#include "widget.h"

#include "atl/hashstring.h"

namespace rage {

class bkText: public bkWidget {
	friend class bkGroup;
	friend class bkManager;

protected:
	bkText( datCallback &callback, const char *title, const char *memo, bool *data, bool readOnly, const char *fillColor=NULL );
	bkText( datCallback &callback, const char *title, const char *memo, int *data, bool readOnly, const char *fillColor=NULL );
	bkText( datCallback &callback, const char *title, const char *memo, long *data, bool readOnly, const char *fillColor=NULL );
	bkText( datCallback &callback, const char *title, const char *memo, float *data, bool readOnly, const char *fillColor=NULL );
	bkText( datCallback &callback, const char *title, const char *memo, char *data, int dataSize, bool readOnly, const char *fillColor=NULL );
	bkText( datCallback &callback, const char *title, const char *memo, atHashString* data, bool readOnly, const char *fillColor=NULL );
	bkText( datCallback &callback, const char *title, const char *memo, atFinalHashString* data, bool readOnly, const char *fillColor=NULL );
	~bkText();

	int DrawLocal(int x,int y);

	static int GetStaticGuid() { return BKGUID('t','e','x','t'); }
	int GetGuid() const;

	void Update();
	static void RemoteHandler(const bkRemotePacket& p);
	
public:
    enum EDataType
    {
        BOOL,
        FLOAT,
		INT,
		LONG,
		STRING,
		ATHASHSTRING,
		ATFINALHASHSTRING,
    };

    EDataType GetDatType() const;

    bool GetBool() const;
    void SetBool( bool b );

    float GetFloat() const;
    void SetFloat( float f );
    
    int GetInt() const;
    void SetInt( int i );

	long GetLong() const;
	void SetLong( long l );

    const char* GetString() const;
    void SetString( const char *s );
	void SetStringPointer( char *s ); // changes internal string pointer 

	atHashString GetHashString() const;
	void SetHashString(atHashString h);

	atFinalHashString GetFinalHashString() const;
	void SetFinalHashString(atFinalHashString h);

    // PURPOSE: Retrieves a string representation of the current value of this widget.
    // PARAMS:
    //    buf - the buffer to write the string representation to.
    //    bufLen - the length of the buffer.
    virtual void GetStringRepr( char *buf, int bufLen );

    // PURPOSE: Sets the current value of this widget using the string representation of the new value.
    // PARAMS:
    //    buf - the new value.
    virtual void SetStringRepr( const char *buf );

protected:
	const char* GetHashStringCStr();
	const char* TryGetHashStringCStr();
	union Data
	{
		bool* b;
		float* f;
		int* i;
		long* l;
		char* s;
		atHashString* h;
		atFinalHashString* fh;
	};

#if __WIN32PC
	void WindowCreate();
	void WindowDestroy();
	int WindowResize(int x,int y,int width,int height);
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM);
	void WindowUpdate();
	struct HWND__* m_Label;
#endif
	void RemoteCreate();
	void RemoteUpdate();

	EDataType m_dataType;
	Data m_data;
	Data m_prevData;

	int m_StringSize;
};

inline bkText::EDataType bkText::GetDatType() const
{
    return m_dataType;
}

inline bool bkText::GetBool() const
{
    return *(m_data.b);
}

inline void bkText::SetBool( bool b )
{
    if ( m_dataType == BOOL )
    {
        *(m_data.b) = b;
        Changed();
    }
}

inline float bkText::GetFloat() const
{
    return *(m_data.f);
}

inline void bkText::SetFloat( float f )
{
    if ( m_dataType == FLOAT )
    {
        *(m_data.f) = f;
        Changed();
    }

}

inline int bkText::GetInt() const
{
    return *(m_data.i);
}

inline void bkText::SetInt( int i )
{
    if ( m_dataType == INT )
    {
        *(m_data.i) = i;
        Changed();
    }
}

inline long bkText::GetLong() const
{
	return *(m_data.l);
}

inline void bkText::SetLong( long l )
{
	if ( m_dataType == LONG )
	{
		*(m_data.l) = l;
		Changed();
	}
}

inline const char* bkText::GetString() const
{
    return m_data.s;
}

inline void bkText::SetString( const char *s )
{
    if ( m_dataType == STRING )
    {
        int len = (int)strlen( s );
        for ( int i = 0; (i <= len) && (i < m_StringSize); ++i )
        {
            m_data.s[i] = s[i];
        }

        Changed();
    }
}

inline void bkText::SetStringPointer( char *s )
{
	if (AssertVerify( m_dataType == STRING ))
	{
		m_data.s = s;
		Changed();
	}
}

inline atHashString bkText::GetHashString() const
{
	return *(m_data.h);
}

inline void bkText::SetHashString( atHashString h )
{
	if ( m_dataType == ATHASHSTRING )
	{
		*(m_data.h) = h;
		Changed();
	}
}

inline atFinalHashString bkText::GetFinalHashString() const
{
	return *(m_data.fh);
}

inline void bkText::SetFinalHashString( atFinalHashString fh )
{
	if ( m_dataType == ATFINALHASHSTRING )
	{
		*(m_data.fh) = fh;
		Changed();
	}
}

inline const char* bkText::GetHashStringCStr()
{
	if (m_dataType == ATHASHSTRING) {
		return m_data.h->GetCStr();
	}
	else if (m_dataType == ATFINALHASHSTRING) {
		return m_data.fh->GetCStr();
	}
	else
	{
		FastAssert(0);
	}
	return NULL;
}

inline const char* bkText::TryGetHashStringCStr()
{
	if (m_dataType == ATHASHSTRING) {
		return m_data.h->TryGetCStr();
	}
	else if (m_dataType == ATFINALHASHSTRING) {
		return m_data.fh->TryGetCStr();
	}
	else
	{
		FastAssert(0);
	}
	return NULL;
}

}	// namespace rage

#endif

#endif
