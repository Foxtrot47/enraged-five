// 
// bank/bkmatrix.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#ifndef BANK_BKMATRIX_H 
#define BANK_BKMATRIX_H 

#include "packet.h"
#include "widget.h"

#include "file/remote.h"
#include "system/param.h"
#include "vector/matrix33.h"
#include "vector/matrix34.h"
#include "vector/matrix44.h"

namespace rage {

class bkMatrix : public bkWidget
{
public:
	int GetNumRows() const;
	int GetNumColumns() const;

	float GetMinimum() const;
	void SetMinimum( float min );

	float GetMaximum() const;
	void SetMaxium( float max );

	float GetStep() const;
	void SetStep( float step );

	bool SetFocus();

	virtual int GetGuid() const = 0;

	virtual bool SetValue( int row, int col, float f ) = 0;
	virtual float GetValue( int row, int col ) = 0;

	virtual bool SetString( int row, int col, const char* src ) = 0;
	virtual const char* GetString( int row, int col, char* dest, int destSize ) = 0;

    // PURPOSE: Retrieves a string representation of the current value of this widget.
    // PARAMS:
    //    buf - the buffer to write the string representation to.
    //    bufLen - the length of the buffer.
    virtual void GetStringRepr( char *buf, int bufLen );

    // PURPOSE: Sets the current value of this widget using the string representation of the new value.
    // PARAMS:
    //    buf - the new value.
    virtual void SetStringRepr( const char *buf );

	// PURPOSE: Minimum value of floating point slider
	static const float FLOAT_MIN_VALUE;

	// PURPOSE: Maximum value of floating point slider
	static const float FLOAT_MAX_VALUE;

protected:	
	enum EnumType
	{
		EMatrix33,
		EMatrix34,
		EMatrix44
	};

	enum Operation
	{
		EIdentity,
		EIdentity3x3,
		EZero,
		EZero3x3,
		ESetDiagonal,
		EAdd,
		EAdd3x3,
		ESubtract,
		ESubtract3x3, 
		EAbs,
		ENegate,
		ENegate3x3,            
		EDot,
		EDot3x3,
		EDotFromLeft,
		EDot3x3FromLeft,
		EDotTranspose,
		EDot3x3Transpose,
		ECrossProduct,
		EOuterProduct,
		ERotate,
		ERotateX,
		ERotateY,
		ERotateZ,
		ERotateUnitAxis,
		ERotateLocalX,
		ERotateLocalY,
		ERotateLocalZ,
		ERotateLocalAxis,
		EMakeRotate,
		EMakeRotateX,
		EMakeRotateY,
		EMakeRotateZ,
		EMakeRotateUnitAxis,
		ERotateTo,
		EMakeRotateTo,
		EMakeUpright,
		EScale,
		EMakeScale,
		EInverse,
		EInverse3x3,
		EFastInverse,
		ETranspose,
		ETranspose3x4,
		ECoordinateInverseSafe,
		EDotCrossProdMtx,
		EDot3x3CrossProdMtx,
		EDotCrossProdTranspose,
		EDot3x3CrossProdTranspose,
		EMakeDoubleCrossMatrix,
		ENormalize,
		ENormalizeSafe,
		EMirrorOnPlane,                                    
		ERotateFull,
		ERotateFullX,
		ERotateFullY,
		ERotateFullZ,
		ERotateFullUnitAxis,
		EScaleFull,
		ETranslate,
		EMakeTranslate,                
		ELookDown,
		ELookAt,
		EPolarView,
		EInterpolate,
		EMakeScaleFull,
		EMakePos,
		EMakePosRotY,
		EMakeRotX,
		EMakeRotY,
		EMakeRotZ,
		EMakeReflect
	};

	bkMatrix( datCallback &callback, const char *title, const char *memo, EnumType enumType, float min, float max, float step, const char *fillColor=NULL, bool readOnly=false );
	~bkMatrix() = 0;	

	virtual void ReadValues( const bkRemotePacket& packet ) = 0;
	virtual void ReadValues( const bkRemotePacket& packet, float mtx[4][4] ) = 0;
	virtual void ReadValues( const bkRemotePacket& packet, float vec[4] ) = 0;
	virtual void WriteValues( bkRemotePacket& packet ) = 0;

	virtual void ExecuteOperation( Operation /*op*/ ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*f*/ ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*f1*/, float /*f2*/, float /*f3*/ ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*f1*/, float /*f2*/, float /*f3*/, float /*f4*/ ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*v*/[] ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*v1*/[], float /*v2*/[] ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*v1*/[], float /*v2*/[], float /*f*/ ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*v*/[], float /*f*/ ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*v*/[], float /*f1*/, float /*f2*/ ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*m*/[4][4] ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*m*/[4][4], float /*f*/ ) {}
	virtual void ExecuteOperation( Operation /*op*/, float /*f*/, int /*i*/ ) {}

	virtual void ClampValuesToAllowedRange() {}

	void Message( Action action, float value );
	int DrawLocal( int x, int y );

	static void RemoteHandler( const bkRemotePacket& packet, EnumType enumType );

	void CheckInitialValues();

	float m_minimum;
	float m_maximum;
	float m_step;
	EnumType m_enumType;

private:
	void RemoteCreate();
	void RemoteUpdate();

	int m_drawLocalTopY;
	int m_drawLocalExpanded[4];
#if __WIN32PC
	void WindowCreate();
	void WindowUpdate();
	void WindowDestroy();
	rageLRESULT WindowMessage( rageUINT msg, rageWPARAM wParam, rageLPARAM lParam );
	void WindowMessage( Action action, float value );
	int WindowResize( int x, int y, int width, int height );

	void AddComponentWindow( int row, int col );
	void ResizeComponentWindow( int row, int col, int x, int y, int width, int height );

public:
	bool ValidateString( const char* string, bool okOnly ) const;
	bool CheckKey( char key ) const;
	void SetFont( bool bold );

	void SetComponentInFocus( int row, int col, bool setWindowFocus );
	void GetComponentInFocus( int &row, int &col ) const;

	void SetWindowInFocus( struct HWND__* hwnd );
	struct HWND__* GetWindowInFocus() const;

protected:
	struct HWND__* m_titleWindowHandle;
	struct HWND__* m_rowTitleWindowHandles[4];
	struct HWND__* m_windowHandles[4][12];
	int m_rowInFocus;
	int m_colInFocus;
#endif
};

inline int bkMatrix::GetNumRows() const
{
	switch ( m_enumType )
	{
	case EMatrix33:
		return 3;
	case EMatrix34:
		return 4;
	case EMatrix44:
		return 4;
	}

	return 0;
}

inline int bkMatrix::GetNumColumns() const
{
	switch ( m_enumType )
	{
	case EMatrix33:
		return 3;
	case EMatrix34:
		return 3;
	case EMatrix44:
		return 4;
	}

	return 0;
}

inline float bkMatrix::GetMinimum() const
{
	return m_minimum;
}

inline void bkMatrix::SetMinimum( float min )
{
	m_minimum = min;
}

inline float bkMatrix::GetMaximum() const
{
	return m_maximum;
}

inline void bkMatrix::SetMaxium( float max )
{
	m_maximum = max;
}

inline float bkMatrix::GetStep() const
{
	return m_step;
}

inline void bkMatrix::SetStep( float step )
{
	m_step = step;
}

#if __WIN32PC

inline void bkMatrix::GetComponentInFocus( int &row, int &col ) const
{
	row = m_rowInFocus;
	col = m_colInFocus;
}

#endif // __WIN32PC

//#############################################################################

class bkMatrix33 : public bkMatrix
{
public:
	static inline int GetStaticGuid();
	int GetGuid() const;

protected:
	friend class bkGroup;
	friend class bkManager;
	friend class bkMatrix;

	bkMatrix33( datCallback &callback, const char *title, const char *memo, Matrix33 *data, float min, float max, float step, const char* fillColor=NULL, bool readOnly=false );
	~bkMatrix33();

	void ReadValues( const bkRemotePacket& packet );
	void ReadValues( const bkRemotePacket& packet, float mtx[4][4] );
	void ReadValues( const bkRemotePacket& packet, float vec[4] );
	void WriteValues( bkRemotePacket& packet );

	void ExecuteOperation( Operation op );
	void ExecuteOperation( Operation op, float f );
	void ExecuteOperation( Operation op, float v[] );
	void ExecuteOperation( Operation op, float v1[], float v2[] );
	void ExecuteOperation( Operation op, float v1[], float v2[], float f );
	void ExecuteOperation( Operation op, float v[], float f );
	void ExecuteOperation( Operation op, float m[4][4] );
	void ExecuteOperation( Operation op, float f, int i );

	void ClampValuesToAllowedRange();

	void Update();

public:
	bool SetValue( int row, int col, float f );
	float GetValue( int row, int col );

	bool SetString( int row, int col, const char* src );
	const char* GetString( int row, int col, char* dest, int destSize );

    const Matrix33& GetMatrix() const;
    void SetMatrix( const Matrix33 &m );

private:
	static void RemoteHandler( const bkRemotePacket& packet );

	Matrix33 *m_pValue;
	Matrix33 m_prevValue;
};

inline int bkMatrix33::GetStaticGuid()
{
	return BKGUID( 'm', 't', 'x', '9' );
}

inline int bkMatrix33::GetGuid() const
{
	return GetStaticGuid();
}

inline void bkMatrix33::Update()
{
	if ( m_pValue->IsNotEqual( m_prevValue ) ) 
	{ 
		m_prevValue.Set( *m_pValue );
		FinishUpdate();
	}
}

inline const Matrix33& bkMatrix33::GetMatrix() const
{
    return *m_pValue;
}

inline void bkMatrix33::SetMatrix( const Matrix33 &m )
{
    m_pValue->Set( m );
    Changed();
}

//#############################################################################

class bkMatrix34 : public bkMatrix
{
public:
	static inline int GetStaticGuid();
	int GetGuid() const;

protected:
	friend class bkGroup;
	friend class bkManager;
	friend class bkMatrix;

	bkMatrix34( datCallback &callback, const char *title, const char *memo, Matrix34 *data, float min, float max, float step, const char* fillColor=NULL, bool readOnly=false );
	~bkMatrix34();

	void ReadValues( const bkRemotePacket& packet );
	void ReadValues( const bkRemotePacket& packet, float mtx[4][4] );
	void ReadValues( const bkRemotePacket& packet, float vec[4] );
	void WriteValues( bkRemotePacket& packet );

	void ExecuteOperation( Operation op );
	void ExecuteOperation( Operation op, float f );
	void ExecuteOperation( Operation op, float f1, float f2, float f3, float f4 );
	void ExecuteOperation( Operation op, float v[] );
	void ExecuteOperation( Operation op, float v1[], float v2[] );
	void ExecuteOperation( Operation op, float v1[], float v2[], float f );
	void ExecuteOperation( Operation op, float v[], float f );
	void ExecuteOperation( Operation op, float m[4][4] );
	void ExecuteOperation( Operation op, float m[4][4], float f );
	void ExecuteOperation( Operation op, float f, int i );

	void ClampValuesToAllowedRange();

	void Update();

public:
	bool SetValue( int row, int col, float f );
	float GetValue( int row, int col );

	bool SetString( int row, int col, const char* src );
	const char* GetString( int row, int col, char* dest, int destSize );

    const Matrix34& GetMatrix() const;
    void SetMatrix( const Matrix34 &m );

private:
	static void RemoteHandler( const bkRemotePacket& packet );

	Matrix34 *m_pValue;
	Matrix34 m_prevValue;
};

inline int bkMatrix34::GetStaticGuid()
{
	return BKGUID( 'm', 't', 'x', '2' );
}

inline int bkMatrix34::GetGuid() const
{
	return GetStaticGuid();
}

inline void bkMatrix34::Update()
{
	if ( m_pValue->IsNotEqual( m_prevValue ) ) 
	{ 
		m_prevValue.Set( *m_pValue );
		FinishUpdate();
	}
}

inline const Matrix34& bkMatrix34::GetMatrix() const
{
    return *m_pValue;
}

inline void bkMatrix34::SetMatrix( const Matrix34 &m )
{
    m_pValue->Set( m );
    Changed();
}

//#############################################################################

class bkMatrix44 : public bkMatrix
{
public:
	static inline int GetStaticGuid();
	int GetGuid() const;

protected:
	friend class bkGroup;
	friend class bkManager;
	friend class bkMatrix;

	bkMatrix44( datCallback &callback, const char *title, const char *memo, Matrix44 *data, float min, float max, float step, const char* fillColor=NULL, bool readOnly=false );
	~bkMatrix44();

	void ReadValues( const bkRemotePacket& packet );
	void ReadValues( const bkRemotePacket& packet, float mtx[4][4] );
	void ReadValues( const bkRemotePacket& packet, float vec[4] );
	void WriteValues( bkRemotePacket& packet );

	void ExecuteOperation( Operation op );
	void ExecuteOperation( Operation op, float f );
	void ExecuteOperation( Operation op, float f1, float f2, float f3 );
	void ExecuteOperation( Operation op, float v[] );
	void ExecuteOperation( Operation op, float v[], float f1, float f2 );
	void ExecuteOperation( Operation op, float m[4][4] );

	void ClampValuesToAllowedRange();

	void Update();

public:
	bool SetValue( int row, int col, float f );
	float GetValue( int row, int col );

	bool SetString( int row, int col, const char* src );
	const char* GetString( int row, int col, char* dest, int destSize );

    const Matrix44& GetMatrix() const;
    void SetMatrix( const Matrix44 &m );

private:
	static void RemoteHandler( const bkRemotePacket& packet );

	Matrix44 *m_pValue;
	Matrix44 m_prevValue;
};

inline int bkMatrix44::GetStaticGuid()
{
	return BKGUID( 'm', 't', 'x', '6' );
}

inline int bkMatrix44::GetGuid() const
{
	return GetStaticGuid();
}

inline void bkMatrix44::Update()
{
	if ( m_pValue->GetVector( 0 ).IsNotEqual( m_prevValue.GetVector( 0 ) ) 
		|| m_pValue->GetVector( 1 ).IsNotEqual( m_prevValue.GetVector( 1 ) )
		|| m_pValue->GetVector( 2 ).IsNotEqual( m_prevValue.GetVector( 2 ) )
		|| m_pValue->GetVector( 3 ).IsNotEqual( m_prevValue.GetVector( 3 ) ) )
	{ 
		m_prevValue.Set( *m_pValue ); 
		FinishUpdate();
	}
}

inline const Matrix44& bkMatrix44::GetMatrix() const
{
    return *m_pValue;
}

inline void bkMatrix44::SetMatrix( const Matrix44 &m )
{
    m_pValue->Set( m );
    Changed();
}

} // namespace rage

#endif // BANK_BKMATRIX_H 
