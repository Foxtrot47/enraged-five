//
// bank/button.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef BANK_BUTTON_H
#define BANK_BUTTON_H

#if __BANK

#include "widget.h"

namespace rage {

class bkButton: public bkWidget {
	friend class bkGroup;
	friend class bkManager;
protected:
	bkButton(datCallback cb,const char *title,const char *memo, const char *fillColor=NULL, bool readOnly=false);
	~bkButton();

	void Message(Action action,float value);
	int DrawLocal(int x,int y);
	static void RemoteHandler(const bkRemotePacket& p);

	static int GetStaticGuid() { return BKGUID('b','u','t','n'); }
	int GetGuid() const;

public:
    void Activate();

protected:
#if __WIN32PC
	void WindowCreate();
	rageLRESULT WindowMessage(rageUINT,rageWPARAM,rageLPARAM);
#endif
	void RemoteCreate();
	void RemoteUpdate();
};

inline void bkButton::Activate()
{
    Changed();
}

}	// namespace rage
#endif

#endif
