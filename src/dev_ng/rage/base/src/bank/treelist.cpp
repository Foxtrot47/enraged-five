// 
// bank/treelist.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#if __BANK

#include "treelist.h"

#include "packet.h"
#include "pane.h"
#include "vector/vector3.h"

using namespace rage;

bkTreeList::bkTreeList(const char *title,const char *memo,bool allowDrag,bool allowDrop, bool allowDelete, bool showRootLines, const char *fillColor, bool readOnly)
: bkWidget(NullCallback,title,memo,fillColor,readOnly)
, m_allowDrag(allowDrag)
, m_allowDrop(allowDrop)
, m_allowDelete(allowDelete)
, m_showRootLines(showRootLines)
{
}



bkTreeList::~bkTreeList() 
{
}


int bkTreeList::GetGuid() const { return GetStaticGuid(); }


int bkTreeList::DrawLocal(int x,int y) 
{
	Drawf( x, y, m_Title );
    return bkWidget::DrawLocal( x, y );
}

enum 
{
    TL_EVENT_ADD_COLUMN=bkRemotePacket::USER+0,
    TL_EVENT_ADD_NODE=bkRemotePacket::USER+1,
    TL_EVENT_ADD_ITEM=bkRemotePacket::USER+2,
    TL_EVENT_REMOVE_NODE=bkRemotePacket::USER+3,
    TL_EVENT_REMOVE_ITEM=bkRemotePacket::USER+4,
    TL_EVENT_UPDATE_ITEM=bkRemotePacket::USER+5,
    TL_EVENT_ITEM_CHECKED=bkRemotePacket::USER+6,
    TL_EVENT_SET_ITEM=bkRemotePacket::USER+7,
    TL_EVENT_SET_CHECKBOX=bkRemotePacket::USER+8,
    TL_EVENT_SPECIAL=bkRemotePacket::USER+9
};

void bkTreeList::AddReadOnlyColumn( const char* name, 
                                   CheckBoxStyle checkStyle, CheckBoxAlign checkAlign )
{
    AddColumn( name, ColumnControlTypeNone, ItemControlTypeNone, EditConditionNone, checkStyle, checkAlign );
}

void bkTreeList::AddColumnTemplateColumn( const char* name, ItemControlType itemCtrlType, EditCondition editCond, 
                                         CheckBoxStyle checkStyle, CheckBoxAlign checkAlign )
{
    AddColumn( name, ColumnControlTypeColumnTemplate, itemCtrlType, editCond, checkStyle, checkAlign );
}

void bkTreeList::AddInPlaceTemplateColumn( const char* name, EditCondition editCond, 
                                          CheckBoxStyle checkStyle, CheckBoxAlign checkAlign )
{
    AddColumn( name, ColumnControlTypeInPlaceTemplate, ItemControlTypeNone, editCond, checkStyle, checkAlign );
}

void bkTreeList::AddTextBoxColumn( const char* name, EditCondition editCond, 
                                  CheckBoxStyle checkStyle, CheckBoxAlign checkAlign )
{
    AddColumn( name, ColumnControlTypeTextBox, ItemControlTypeNone, editCond, checkStyle, checkAlign );
}

void bkTreeList::AddComboBoxColumn( const char* name, EditCondition editCond, 
                                   CheckBoxStyle checkStyle, CheckBoxAlign checkAlign )
{
    AddColumn( name, ColumnControlTypeComboBox, ItemControlTypeNone, editCond, checkStyle, checkAlign );
}

void bkTreeList::AddDateTimePickerColumn( const char* name, EditCondition editCond, 
                                         CheckBoxStyle checkStyle, CheckBoxAlign checkAlign )
{
    AddColumn( name, ColumnControlTypeDateTimePicker, ItemControlTypeNone, editCond, checkStyle, checkAlign );
}

void bkTreeList::AddColumn( const char* name, ColumnControlType colCtrlType, ItemControlType itemCtrlType, 
               EditCondition editCond, CheckBoxStyle checkStyle, CheckBoxAlign checkAlign )
{
	if (!bkRemotePacket::IsConnected())
	{
		return;
	}
    bkRemotePacket p;
    p.Begin( TL_EVENT_ADD_COLUMN, bkTreeList::GetStaticGuid(), this );
    p.Write_const_char( name );
    p.Write_s32( (int)colCtrlType );
    p.Write_s32( (int)editCond );
    p.Write_s32( (int)checkStyle );
    p.Write_s32( (int)checkAlign );
    p.Write_s32( (int)itemCtrlType );
    p.Send();
}

void bkTreeList::AddNode( const char *name, int nodeKey, NodeDataType dataType, int parentKey, bool expand )
{
	if (!bkRemotePacket::IsConnected())
	{
		return;
	}
    bkRemotePacket p;
    p.Begin( TL_EVENT_ADD_NODE, bkTreeList::GetStaticGuid(), this );
    p.Write_const_char( name );
    p.Write_s32( nodeKey );
    p.Write_s32( (int)dataType );
    p.Write_s32( parentKey );
    p.Write_bool( expand );
    p.Send();
}

void bkTreeList::AddReadOnlyItem( int key, int val, int nodeKey,
                                 bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%d", val );

    AddItem( key, strVal, ItemDataTypeInt, nodeKey, ItemControlTypeNone, true, 
        updateParent, checkBoxKey, checkState, 0, NULL );
}

void bkTreeList::AddReadOnlyItem( int key, float val, int nodeKey,
                                 bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%f", val );

    AddItem( key, strVal, ItemDataTypeFloat, nodeKey, ItemControlTypeNone, true, 
        updateParent, checkBoxKey, checkState, 0, NULL );
}

void bkTreeList::AddReadOnlyItem( int key, const char* val, int nodeKey,
                                 bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    AddItem( key, val, ItemDataTypeString, nodeKey, ItemControlTypeNone, true, 
        updateParent, checkBoxKey, checkState, 0, NULL );
}

void bkTreeList::AddReadOnlyItem( int key, bool val, int nodeKey,
                                 bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%s", val ? "true" : "false" );

    AddItem( key, strVal, ItemDataTypeBool, nodeKey, ItemControlTypeNone, true, 
        updateParent, checkBoxKey, checkState, 0, NULL );
}

void bkTreeList::AddTextBoxItem( int key, int val, int nodeKey, bool readOnly, 
                                bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%d", val );

    AddItem( key, strVal, ItemDataTypeInt, nodeKey, ItemControlTypeTextBox, readOnly, 
        updateParent, checkBoxKey, checkState, 0, NULL );
}

void bkTreeList::AddTextBoxItem( int key, float val, int nodeKey, bool readOnly, 
                                bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%f", val );

    AddItem( key, strVal, ItemDataTypeFloat, nodeKey, ItemControlTypeTextBox, readOnly, 
        updateParent, checkBoxKey, checkState, 0, NULL );
}

void bkTreeList::AddTextBoxItem( int key, const char* val, int nodeKey, bool readOnly, 
                                bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    AddItem( key, val, ItemDataTypeString, nodeKey, ItemControlTypeTextBox, readOnly, 
        updateParent, checkBoxKey, checkState, 0, NULL );
}

void bkTreeList::AddTextBoxItem( int key, bool val, int nodeKey, bool readOnly, 
                                bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%s", val ? "true" : "false" );

    AddItem( key, strVal, ItemDataTypeBool, nodeKey, ItemControlTypeTextBox, readOnly, 
        updateParent, checkBoxKey, checkState, 0, NULL );
}

void bkTreeList::AddTextBoxItem( int key, const Vector3 &val, int nodeKey, bool readOnly,
                    bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[256];
    formatf( strVal, "<<%g,%g,%g>>", val.x, val.y, val.z );

    AddItem( key, strVal, ItemDataTypeVector, nodeKey, ItemControlTypeTextBox, readOnly, 
        updateParent, checkBoxKey, checkState, 0, NULL );
}

void bkTreeList::AddComboBoxItem( int key, int val, int nodeKey, int numComboBoxItems, const int* comboBoxItems,
                     bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%d", val );

    char **strComboBoxItems = NULL;
    if ( numComboBoxItems > 0 )
    {
        strComboBoxItems = rage_new char*[numComboBoxItems];
        for (int i = 0; i < numComboBoxItems; ++i)
        {
            strComboBoxItems[i] = rage_new char[ 64 ];
            formatf( strComboBoxItems[i], 64, "%d", comboBoxItems[i] );
        }
    }

    AddItem( key, strVal, ItemDataTypeInt, nodeKey, ItemControlTypeComboBox, false, 
        updateParent, checkBoxKey, checkState, numComboBoxItems, strComboBoxItems );
}

void bkTreeList::AddComboBoxItem( int key, float val, int nodeKey, int numComboBoxItems, const float* comboBoxItems,
                     bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%f", val );

    char **strComboBoxItems = NULL;
    if ( numComboBoxItems > 0 )
    {
        strComboBoxItems = rage_new char*[numComboBoxItems];
        for (int i = 0; i < numComboBoxItems; ++i)
        {
            strComboBoxItems[i] = rage_new char[ 64 ];
            formatf( strComboBoxItems[i], 64, "%f", comboBoxItems[i] );
        }
    }

    AddItem( key, strVal, ItemDataTypeFloat, nodeKey, ItemControlTypeComboBox, false, 
        updateParent, checkBoxKey, checkState, numComboBoxItems, strComboBoxItems );
}

void bkTreeList::AddComboBoxItem( int key, const char* val, int nodeKey, int numComboBoxItems, char** comboBoxItems,
                     bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    AddItem( key, val, ItemDataTypeString, nodeKey, ItemControlTypeComboBox, false, 
        updateParent, checkBoxKey, checkState, numComboBoxItems, comboBoxItems );
}

void bkTreeList::AddDateTimePickerItem( int key, const char* val, int nodeKey, DateTimeFormat format, 
                    const char* min, const char* max, bool readOnly, bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    int numComboBoxItems = 3;
    char **strComboBoxItems = rage_new char*[numComboBoxItems];

    strComboBoxItems[0] = rage_new char[ 64 ];
    formatf( strComboBoxItems[0], 64, "%d", (int)format );

    strComboBoxItems[1] = rage_new char[ 64 ];
    formatf( strComboBoxItems[1], 64, "%s", min );

    strComboBoxItems[2] = rage_new char[ 64 ];
    formatf( strComboBoxItems[2], 64, "%s", max );

    AddItem( key, val, ItemDataTypeString, nodeKey, ItemControlTypeDataTimePicker, readOnly, 
        updateParent, checkBoxKey, checkState, numComboBoxItems, strComboBoxItems );
}

void bkTreeList::AddNumericItem( int key, int val, int nodeKey, int min, int max, int step,
                    bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%d", val );

    int numComboBoxItems = 3;
    char **strComboBoxItems = rage_new char*[numComboBoxItems];

    strComboBoxItems[0] = rage_new char[ 64 ];
    formatf( strComboBoxItems[0], 64, "%d", min );

    strComboBoxItems[1] = rage_new char[ 64 ];
    formatf( strComboBoxItems[1], 64, "%d", max );

    strComboBoxItems[2] = rage_new char[ 64 ];
    formatf( strComboBoxItems[2], 64, "%d", step );

    AddItem( key, strVal, ItemDataTypeInt, nodeKey, ItemControlTypeNumericUpDown, false, 
        updateParent, checkBoxKey, checkState, numComboBoxItems, strComboBoxItems );
}

void bkTreeList::AddNumericItem( int key, float val, int nodeKey, float min, float max, float step,
                    bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%.2f", val );

    int numComboBoxItems = 3;
    char **strComboBoxItems = rage_new char*[numComboBoxItems];

    strComboBoxItems[0] = rage_new char[ 64 ];
    formatf( strComboBoxItems[0], 64, "%.2f", min );

    strComboBoxItems[1] = rage_new char[ 64 ];
    formatf( strComboBoxItems[1], 64, "%.2f", max );

    strComboBoxItems[2] = rage_new char[ 64 ];
    if ( step < 0.01f )
    {
        step = 0.01f;   // FUDGE, maybe we should allow them to specify the number of decimal places
    }
    formatf( strComboBoxItems[2], 64, "%.2f", step );

    AddItem( key, strVal, ItemDataTypeFloat, nodeKey, ItemControlTypeNumericUpDown, false, 
        updateParent, checkBoxKey, checkState, numComboBoxItems, strComboBoxItems );
}

void bkTreeList::AddButtonItem( int key, const char* name, int nodeKey )
{
    AddItem( key, name, ItemDataTypeString, nodeKey, ItemControlTypeButton, false, 
        false, -1, CheckBoxStateIndeterminate, 0, NULL );
}

void bkTreeList::AddProgressBarItem( int key, int val, int nodeKey, int min, int max,
                        bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%d", val );

    int numComboBoxItems = 2;
    char **strComboBoxItems = rage_new char*[numComboBoxItems];

    strComboBoxItems[0] = rage_new char[ 64 ];
    formatf( strComboBoxItems[0], 64, "%d", min );

    strComboBoxItems[1] = rage_new char[ 64 ];
    formatf( strComboBoxItems[1], 64, "%d", max );

    AddItem( key, strVal, ItemDataTypeInt, nodeKey, ItemControlTypeProgressBar, false, 
        updateParent, checkBoxKey, checkState, numComboBoxItems, strComboBoxItems );
}

void bkTreeList::AddSliderItem( int key, int val, int nodeKey, int min, int max,
                   bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%d", val );

    int numComboBoxItems = 2;
    char **strComboBoxItems = rage_new char*[numComboBoxItems];

    strComboBoxItems[0] = rage_new char[ 64 ];
    formatf( strComboBoxItems[0], 64, "%d", min );

    strComboBoxItems[1] = rage_new char[ 64 ];
    formatf( strComboBoxItems[1], 64, "%d", max );

    AddItem( key, strVal, ItemDataTypeInt, nodeKey, ItemControlTypeSlider, false, 
        updateParent, checkBoxKey, checkState, numComboBoxItems, strComboBoxItems );
}

void bkTreeList::AddSliderItem( int key, float val, int nodeKey, float min, float max,
                   bool updateParent, int checkBoxKey, CheckBoxState checkState )
{
    char strVal[64];
    formatf( strVal, "%f", val );

    int numComboBoxItems = 2;
    char **strComboBoxItems = rage_new char*[numComboBoxItems];

    strComboBoxItems[0] = rage_new char[ 64 ];
    formatf( strComboBoxItems[0], 64, "%f", min );

    strComboBoxItems[1] = rage_new char[ 64 ];
    formatf( strComboBoxItems[1], 64, "%f", max );

    AddItem( key, strVal, ItemDataTypeFloat, nodeKey, ItemControlTypeSlider, false, 
        updateParent, checkBoxKey, checkState, numComboBoxItems, strComboBoxItems );
}

void bkTreeList::AddItem( int key, const char *val, ItemDataType dataType, int nodeKey, ItemControlType itemCtrlType, 
                         bool readOnly, bool updateParent, int checkBoxKey, CheckBoxState checkState, int numComboBoxItems, char **comboBoxItems )
{
	if (!bkRemotePacket::IsConnected()) return;
    bkRemotePacket p;
    p.Begin( TL_EVENT_ADD_ITEM, bkTreeList::GetStaticGuid(), this );

    p.Write_s32( key );
    p.Write_const_char( val );
    p.Write_s32( nodeKey );
    p.Write_s32( (int)dataType );
    p.Write_bool( updateParent );
    p.Write_bool( readOnly );
    p.Write_s32( checkBoxKey );
    p.Write_s32( (int)checkState );
    p.Write_s32( (int)itemCtrlType );

    p.Write_s32( numComboBoxItems );
    for (int i = 0; i < numComboBoxItems; ++i)
    {
        p.Write_const_char( comboBoxItems[i] );
    }

    p.Send();
}

void bkTreeList::RemoveNode( int nodeKey )
{
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
    p.Begin( TL_EVENT_REMOVE_NODE, bkTreeList::GetStaticGuid(), this );
    p.Write_s32( nodeKey );
    p.Send();
}

void bkTreeList::RemoveItem( int key )
{
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
    p.Begin( TL_EVENT_REMOVE_ITEM, bkTreeList::GetStaticGuid(), this );
    p.Write_s32( key );
    p.Send();
}

void bkTreeList::SetItemValue( int key, int newVal )
{
    char str[64];
    formatf( str, "%d", newVal );
    SetItemValue( key, str );
}

void bkTreeList::SetItemValue( int key, float newVal )
{
    char str[64];
    formatf( str, "%.2f", newVal );
    SetItemValue( key, str );
}

void bkTreeList::SetItemValue( int key, bool newVal )
{
    char str[8];
    formatf( str, "%s", newVal ? "true" : "false" );
    SetItemValue( key, str );
}

void bkTreeList::SetItemValue( int key, const Vector3 &newVal )
{
    char str[128];
    formatf( str, "<<%g,%g,%g>>", newVal.x, newVal.y, newVal.z );
    SetItemValue( key, str );
}

void bkTreeList::SetItemValue( int key, const char* newVal )
{
	if (!bkRemotePacket::IsConnected()) return;
	bkRemotePacket p;
    p.Begin( TL_EVENT_SET_ITEM, bkTreeList::GetStaticGuid(), this );
    p.Write_s32( key );
    p.Write_const_char( newVal );
    p.Send();
}

void bkTreeList::SetItemCheckBox( int checkBoxKey, CheckBoxState state )
{
	if (!bkRemotePacket::IsConnected()) return;
    bkRemotePacket p;
    p.Begin( TL_EVENT_SET_CHECKBOX, bkTreeList::GetStaticGuid(), this );
    p.Write_s32( checkBoxKey );
    p.Write_s32( (int)state );
    p.Send();
}

void bkTreeList::RemoteCreate() 
{
	if (!bkRemotePacket::IsConnected()) return;
	bkWidget::RemoteCreate();

    bkRemotePacket p;
    p.Begin( bkRemotePacket::CREATE, bkTreeList::GetStaticGuid(), this );
    p.WriteWidget( m_Parent );
    p.Write_const_char( m_Title );
    p.Write_const_char( GetTooltip() );
	p.Write_const_char( GetFillColor() );
	p.Write_bool( IsReadOnly() );
    p.Write_bool( m_allowDrag );
    p.Write_bool( m_allowDrop );
    p.Write_bool( m_allowDelete );
    p.Write_bool( m_showRootLines );
    p.Send();
}

void bkTreeList::RemoteUpdate() 
{
}

void bkTreeList::RemoteHandler(const bkRemotePacket& packet) 
{
    if ( packet.GetCommand() == TL_EVENT_UPDATE_ITEM ) 
    { 
        packet.Begin();		
        
		bkTreeList* widget = packet.ReadWidget<rage::bkTreeList>();
		if (!widget) return;

        s32 key = packet.Read_s32();
        s32 dataType = packet.Read_s32();
        const char* newValue = packet.Read_const_char();

        if ( widget->m_UpdateItemFunc.IsValid() )
        {
            widget->m_UpdateItemFunc( key, dataType, newValue );
        }

        packet.End();
    }
    else if ( packet.GetCommand() == TL_EVENT_ITEM_CHECKED )
    {
        packet.Begin();		

		bkTreeList* widget = packet.ReadWidget<bkTreeList>();
		if (!widget) return;

        s32 key = packet.Read_s32();
        s32 checkedState = packet.Read_s32();

        if ( widget->m_ItemCheckedFunc.IsValid() )
        {
            widget->m_ItemCheckedFunc( key, checkedState );
        }

        packet.End();
    }
    else if ( packet.GetCommand() == TL_EVENT_SPECIAL )
    {
        packet.Begin();

		bkTreeList* widget = packet.ReadWidget<bkTreeList>();
		if (!widget) return;

        s32 key = packet.Read_s32();
        s32 type = packet.Read_s32();

        switch ( type )
        {
        case 0:
            if ( widget->m_ItemCheckedFunc.IsValid() )
            {
                widget->m_ItemButtonFunc( key );
            }
            break;
        case 1:
            if ( widget->m_allowDrop && widget->m_DragDropFunc.IsValid() )
            {
                widget->m_DragDropFunc( key );
            }            
            break;
        case 2:
            if ( widget->m_allowDelete && widget->m_DeleteNodeFunc.IsValid() )
            {
                widget->m_DeleteNodeFunc( key );
            }            
            break;
        default:
            break;
        }

        packet.End();
    }
}

void bkTreeList::Update() 
{
}

#if __WIN32PC
#include "system/xtl.h"

void bkTreeList::WindowCreate() 
{
	if ( !bkRemotePacket::IsConnectedToRag() )
	{
		GetPane()->AddLastWindow( this, "STATIC", 0, SS_LEFT | SS_NOPREFIX );
	}
}
#endif // __WIN32PC

#endif // __BANK
