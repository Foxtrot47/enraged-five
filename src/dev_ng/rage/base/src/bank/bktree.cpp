//
// bank/bktree.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#if __WIN32PC

#include "channel.h"
#include "tree.h"

#include "file/stream.h"
#include "string/string.h"

#pragma comment(lib,"comctl32.lib")

using namespace rage;

/* API for populating a viewer object from a simple tree. */
void bkTreeViewer::RecurseTree(bkTreeViewerItem *parent,void *node) {
	// Add self to parent
	parent = Insert(parent,BKTV_LAST,node,(char*)m_Callback(node,BKTV_GETLABEL,m_UserData));

	// And recurse into our children
	for (node = m_Callback(node,BKTV_GETCHILD,m_UserData); node; node = m_Callback(node,BKTV_GETNEXT,m_UserData))
		RecurseTree(parent,node);
}


void bkTreeViewer::MakeTree(bkTreeViewerCallback cb,void *root) {
	m_Callback = cb;
	Init();
	RecurseTree(0,root);
}


void bkTreeViewer::MakeTreeFromStream(fiStream& stream) {
	char linebuf[512];
	int level = 0;
	int lineNumber = 1;
	int spacing = 0;
	bkTreeViewerItem *stack[128];
	while (fgets(linebuf,sizeof(linebuf),&stream)) {
		int len = StringLength(linebuf);
		while (len && linebuf[len-1]<=32)
			linebuf[--len] = 0;
		int newIndent = 0;
		while (linebuf[newIndent] == 32)
			++newIndent;
		if (newIndent) {
			if (!spacing)
				spacing = newIndent;
			level = newIndent / spacing;
		}
		else
			level = 0;
		bkAssertf(level >= 0 && level < 128, "Level %d not in range [0, 128)", level);
		stack[level] = Insert(level?stack[level-1]:0,stack[level],(void*)lineNumber,(linebuf+newIndent));
		++lineNumber;
	}
}


#include "system/xtl.h"
#pragma warning(disable: 4668)
#include <commctrl.h>
#pragma warning(error: 4668)

LRESULT CALLBACK bkTreeViewer::WindowProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam) {
	// This isn't always valid (for example, during creation)
	bkTreeViewer *tv = (bkTreeViewer*) GetWindowLongPtr(hwnd, GWLP_USERDATA);

	if (msg == WM_NOTIFY) {
		tv->Notify((NMTREEVIEW*)lParam);
		return 0;
	}
	else if (msg == WM_SIZE) {
		if (tv && tv->m_HwndTree)
			MoveWindow(tv->m_HwndTree,0,0,LOWORD(lParam),HIWORD(lParam),true);
		return 0;
	}
	else if (msg == WM_DESTROY) {
		// Clear out handles so we can tell we're nuked.
		// Next time someone calls Init or MakeTree, we'll resurrect the window.
		tv->m_HwndParent = 0;
		tv->m_HwndTree = 0;
		return 0;
	}
	else
		return DefWindowProc(hwnd,msg,wParam,lParam);
}


static const char *treeParentClass = "bkTreeParentClass";

bkTreeViewer::bkTreeViewer(const char *title,int x,int y,int width,int height,bool standalone) : m_X(x), m_Y(y), m_Width(width), m_Height(height) {
	WNDCLASS wc;

	memset(&wc,0,sizeof(wc));
	wc.lpszClassName = treeParentClass;
	wc.style = CS_DBLCLKS;
	wc.lpfnWndProc = bkTreeViewer::WindowProc;
#	ifdef UNICODE
		RegisterClassW(&wc);
#	else
		RegisterClassA(&wc);
#	endif

	InitCommonControls();

	m_Title = ConstStringDuplicate(title);
	m_HwndParent = 0;
	m_HwndTree = 0;
	m_UserData = 0;
	m_Font = 0;
	m_Callback = 0;

	Init(standalone);

	m_AlwaysOnTop = false;
}


void bkTreeViewer::Init(bool standalone) {
	if (!m_HwndParent) {
		m_HwndParent = ::CreateWindowEx(
			standalone? 0 : WS_EX_TOOLWINDOW,
			treeParentClass,m_Title,
			WS_OVERLAPPED | WS_CAPTION | WS_VISIBLE | WS_SYSMENU | WS_SIZEBOX,
			m_X,m_Y,m_Width,m_Height,0,0,GetModuleHandle(0),0);
		SetWindowLongPtr(m_HwndParent, GWLP_USERDATA, (LONG_PTR)this);

		UpdateWindow(m_HwndParent);
		ShowWindow(m_HwndParent, SW_SHOW);
	}

	if (m_HwndTree)
		DestroyWindow(m_HwndTree);

	RECT rect;
	GetClientRect(m_HwndParent,&rect);
	m_HwndTree = ::CreateWindow(
		WC_TREEVIEW, "",
		WS_CHILD | WS_HSCROLL | WS_VSCROLL | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS,
		0,0,rect.right,rect.bottom,m_HwndParent,0,GetModuleHandle(0),0);
	if (m_Font)
		::SendMessage(m_HwndTree, WM_SETFONT, (WPARAM)m_Font,0);

	m_Selection = 0;
	SetWindowPos(m_HwndTree, m_AlwaysOnTop ? HWND_TOPMOST : HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE | ( m_AlwaysOnTop ? WS_EX_TOPMOST : 0 ));
}


void bkTreeViewer::SetTitle(const char *title) {
	ConstStringFree(m_Title);
	m_Title = ConstStringDuplicate(title);
	if (m_HwndParent)
		SetWindowText(m_HwndParent,title);
}


void bkTreeViewer::SetFont(const char *fontName,int height) {
	if (m_Font)
		DeleteObject(m_Font);
	m_Font = ::CreateFont(-height,0,0,0,0,FALSE,FALSE,FALSE,ANSI_CHARSET,0,0,ANTIALIASED_QUALITY,0,fontName);
	if (m_HwndTree)
		::SendMessage(m_HwndTree, WM_SETFONT, (WPARAM)m_Font,TRUE);
}


bkTreeViewer::~bkTreeViewer() {
	if (m_Font)
		DeleteObject(m_Font);
	DestroyWindow(m_HwndParent);
	ConstStringFree(m_Title);
}


bkTreeViewerItem* bkTreeViewer::Insert(bkTreeViewerItem *parent,bkTreeViewerItem *insertAfter,void *ctxt,const char *label) {
	TV_INSERTSTRUCT insert;
	insert.hParent = parent;
	insert.hInsertAfter = insertAfter;

	insert.item.mask = TVIF_TEXT | TVIF_PARAM;
	insert.item.pszText = (char*)label;
	insert.item.lParam = (LPARAM)ctxt;
	return (bkTreeViewerItem*) TreeView_InsertItem(m_HwndTree, &insert);
}


bool bkTreeViewer::Delete(bkTreeViewerItem *item) {
	return TreeView_DeleteItem(m_HwndTree, item) != 0;
}

const char* bkTreeViewer::GetCurSelectionLabel(char* textbuffer, int size)
{
	textbuffer[0]='\0';
	HTREEITEM itemhandle = TreeView_GetSelection(m_HwndTree);
	if(itemhandle)
	{
		TVITEM item;
		item.mask = TVIF_TEXT;
		item.pszText = textbuffer;
		item.cchTextMax =  size;
		item.hItem = itemhandle;
		TreeView_GetItem(m_HwndTree,&item);
	}
	return textbuffer;
}

bool bkTreeViewer::SetItemLabel(bkTreeViewerItem *item, const char *pszLabel) {
	TVITEM pitem;
	pitem.hItem = item;
	pitem.mask = TVIF_TEXT;
	pitem.pszText = (char*)pszLabel;
	return TreeView_SetItem(m_HwndTree,&pitem) !=0;
}

void bkTreeViewer::Reset() {
	TreeView_DeleteAllItems(m_HwndTree);
}


void bkTreeViewer::Update() {
	RedrawWindow(m_HwndTree, 0, 0, RDW_INVALIDATE);
}


void bkTreeViewer::Notify(NMTREEVIEW* notify) {
	/*if (notify->hdr.code == TVN_BEGINRDRAG)
		BeginDrag(notify);
	else */ 
	
	switch(notify->hdr.code)
	{
	
	case TVN_SELCHANGED:
		{
			m_Selection = (void*) notify->itemNew.lParam;
			if (m_Callback)
				m_Callback(m_Selection,BKTV_NEWSEL,m_UserData);
			break;
		}
	case TVN_KEYDOWN:
		{
			if (m_Callback)
				m_Callback(m_UserData,BKTV_KEYDOWN,&notify->action);
			break;
		}
	case NM_RCLICK:
		{
			// No, there really isn't an easier way to do this...
			TVHITTESTINFO hit;

			// GetCursorPos() not safe on vista. See notes for ioMouse::GetPlatformCursorPosition().
			//GetCursorPos(&hit.pt);
			int x=0, y=0;

			CURSORINFO ci;
			ci.cbSize = sizeof(CURSORINFO);

			// Safer than using GetCursorPos() as there is a bug in windows vista. See notes for this
			// function in the header file.
			bool result = GetCursorInfo(&ci) == TRUE;

			if(result)
			{
				x = static_cast<int>(ci.ptScreenPos.x);
				y = static_cast<int>(ci.ptScreenPos.y);
			}

			hit.pt.x = x;
			hit.pt.y = y;

			MapWindowPoints(NULL,m_HwndTree,&hit.pt,1);
			TreeView_HitTest(m_HwndTree,&hit);
			if (hit.hItem) {
				TVITEM item;
				item.hItem = hit.hItem;
				item.mask = TVIF_PARAM;
				if (TreeView_GetItem(m_HwndTree,&item)) { 
					m_Selection = (void*)  item.lParam;
					if (m_Callback)
						m_Callback(m_Selection,BKTV_POSTMENU,m_UserData);
					TreeView_SelectItem(m_HwndTree,hit.hItem);
				}
			}
			break;
		}
	}
	

}


void bkTreeViewer::Raise()
{
	if( !m_AlwaysOnTop )
	{
		::SetWindowPos(m_HwndParent,HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE);	
	}
	SetWindowPos(m_HwndParent, m_AlwaysOnTop ? HWND_TOPMOST : HWND_TOP,0,0,0,0,SWP_NOMOVE | SWP_NOSIZE );
}

#endif
