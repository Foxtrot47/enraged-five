/* SCE CONFIDENTIAL
 * PlayStation(R)Edge 1.1.1
 * Copyright (C) 2007 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */

#ifndef EDGEGEOM_CONFIG_H
#define EDGEGEOM_CONFIG_H

//
// Toggle C++ reference implementations of various Edge functions.
// - A value of 1 means "use the optimized, intrinsics implementation"
// - A value of 0 means "use the reference C++ implementation"
//
// This is mainly of use for debugging, if you suspect a bug in Edge,
// or wish to track your data through Edge.  Note that the C++
// versions are *much* slower than the intrinsic versions; they should
// never ever be used in release code!
//

#define EDGEGEOMCULLOCCLUDEDTRIANGLES_INTRINSICS   1 // edgeGeomCullOccludedTriangles
#define EDGEGEOMCULLTRIANGLES_INTRINSICS           1 // edgeGeomCullTriangles
#define EDGEGEOMNORMALIZEUNIFORMTABLE_INTRINSICS   1 // edgeGeomNormalizeUniformTable
#define EDGEGEOMSKINVERTEXES_INTRINSICS            1 // edgeGeomSkinVertexes
#define EDGEGEOMTRANSFORMVERTEXES_INTRINSICS       1 // edgeGeomTransformVertexes (only used within edgeGeomCullOccludedTriangles)
// Internal functions
#define	TRANSFORMVERTEXESFORCULL_INTRINSICS        1 // transformVertexesForCull
#define CULLOCCLUDEDTRIANGLES_INTRINSICS           1 // cullOccludedTriangles (only used within edgeGeomCullOccludedTriangles)
#define COMPRESSVERTEXESBYDESCRIPTION_INTRINSICS   1 // CompressVertexesByDescription
#define DECOMPRESSBLENDSHAPERUNTABLE_INTRINSICS    1 // DecompressBlendShapeRunTable
#define DECOMPRESSINDEXES_INTRINSICS               1 // DecompressIndexes
#define DECOMPRESSVERTEXESBYDESCRIPTION_INTRINSICS 1 // DecompressVertexesByDescription

//
// Disable unused features to reduce ELF size
//

// Safe to disable if all vertex formats are decompressed/compressed using built-in
// vertex formats (or custom callbacks).  This will not affect blend shapes, which
// always use the generic stream-description-based decompression code.
#define ENABLE_DECOMPRESS_BY_DESCRIPTION 1 // Code savings: ~6.5K
#define ENABLE_COMPRESS_BY_DESCRIPTION 1 // Code savings: ~5K

// Safe to disable if you're not using 4x RGMS multisampling on any render targets.
#define ENABLE_4XRGMS_CULLING 0  // Code savings: ~1K

// Safe to disable if you're not using Edge occlusion (edgeGeomCullOccludedTriangles)
#define ENABLE_EDGE_OCCLUSION 0

// Safe to disable if you're not using Edge's compressed index buffer format
#define ENABLE_INDEX_DECOMPRESSION 1 // Code savings: ~3K

#if HACK_GTA4
	#define ENABLE_EDGE_NOPIXEL_TEST                (1)
	#define ENABLE_EDGE_USE_QUANTISED_DIAGONAL_TEST (1 && ENABLE_EDGE_NOPIXEL_TEST)
//	#define ENABLE_EDGE_USE_QUANTISED_XY_IN_W       (1 && ENABLE_EDGE_NOPIXEL_TEST)
	#define ENABLE_EDGE_CULL_DEBUGGING              (1 && __DEV)
	#define ENABLE_SWITCHABLE_EDGE_INTRINSICS_CODE  (0 && __DEV && EDGEGEOMCULLTRIANGLES_INTRINSICS)
#else
	#define ENABLE_EDGE_NOPIXEL_TEST                (1)
	#define ENABLE_EDGE_USE_QUANTISED_DIAGONAL_TEST (0 && ENABLE_EDGE_NOPIXEL_TEST)
//	#define ENABLE_EDGE_USE_QUANTISED_XY_IN_W       (0 && ENABLE_EDGE_NOPIXEL_TEST)
	#define ENABLE_EDGE_CULL_DEBUGGING              (0 && __DEV)
	#define ENABLE_SWITCHABLE_EDGE_INTRINSICS_CODE  (0 && __DEV && EDGEGEOMCULLTRIANGLES_INTRINSICS)
#endif

#if ENABLE_EDGE_CULL_DEBUGGING
	#define EDGE_CULL_DEBUGGING_ONLY(x) x
#else
	#define EDGE_CULL_DEBUGGING_ONLY(x)
#endif

#endif // EDGEGEOM_CONFIG_H
