PATH %SCE_PS3_ROOT:/=\%\host-win32\bin;%PATH%

spa -o edgepost_argb8tofloats.o edgepost_argb8tofloats.spa
spa -o edgepost_avgluminance.o edgepost_avgluminance.spa
spa -o edgepost_bloomcapture.o edgepost_bloomcapture.spa
spa -o edgepost_copymemory.o edgepost_copymemory.spa
spa -o edgepost_dof.o edgepost_dof.spa --verboseswp
spa -o edgepost_dof_fuzziness.o edgepost_dof_fuzziness.spa
spa -o edgepost_dof_initialize.o edgepost_dof_initialize.spa
spa -o edgepost_dof_premultiply.o edgepost_dof_premultiply.spa
spa -o edgepost_dof_vsc.o edgepost_dof_vsc.spa
spa -o edgepost_downsample.o edgepost_downsample.spa
spa -o edgepost_downsample_minmax.o edgepost_downsample_minmax.spa
spa -o edgepost_extractdepth.o edgepost_extractdepth.spa
spa -o edgepost_floatstofp16luv.o edgepost_floatstofp16luv.spa
spa -o edgepost_floatstologluv.o edgepost_floatstologluv.spa
spa -o edgepost_floattograyscale.o edgepost_floattograyscale.spa
spa -o edgepost_fp16.o edgepost_fp16.spa
spa -o edgepost_fp16luvtofloats.o edgepost_fp16luvtofloats.spa
spa -o edgepost_fp16tofloats.o edgepost_fp16tofloats.spa
spa -o edgepost_gauss1x7f.o edgepost_gauss1x7f.spa
spa -o edgepost_gauss7x1f.o edgepost_gauss7x1f.spa
spa -o edgepost_horizontalgauss.o edgepost_horizontalgauss.spa
spa -o edgepost_ilr.o edgepost_ilr.spa
spa -o edgepost_logluvtofloats.o edgepost_logluvtofloats.spa
spa -o edgepost_luv.o edgepost_luv.spa
spa -o edgepost_mask.o edgepost_mask.spa
spa -o edgepost_motionblur.o edgepost_motionblur.spa
spa -o edgepost_ropaddsat.o edgepost_ropaddsat.spa
spa -o edgepost_ropblend.o edgepost_ropblend.spa
spa -o edgepost_ropcombine.o edgepost_ropcombine.spa
spa -o edgepost_ropmodulate.o edgepost_ropmodulate.spa
spa -o edgepost_tonemap.o edgepost_tonemap.spa
spa -o edgepost_upsample.o edgepost_upsample.spa
spa -o edgepost_verticalgauss.o edgepost_verticalgauss.spa
spa -o edgepost_vignette.o edgepost_vignette.spa

pause