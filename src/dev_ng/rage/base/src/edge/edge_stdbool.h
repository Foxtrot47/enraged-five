/* SCE CONFIDENTIAL
 * PlayStation(R)Edge 1.1.1
 * Copyright (C) 2007 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
#ifndef EDGE_STDBOOL_H
#define EDGE_STDBOOL_H

/* 
 * Workaround for PC tools. MSVC is still not C99 compliant.
 */

#ifndef _MSC_VER
#include <stdbool.h>
#endif

#endif // EDGE_STDBOOL_H
