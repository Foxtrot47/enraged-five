/* SCE CONFIDENTIAL
 * PlayStation(R)Edge 1.1.1
 * Copyright (C) 2007 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
#ifndef EDGE_VERSION_H
#define EDGE_VERSION_H

#define	__EDGE_SDK_MAJOR		0x1
#define	__EDGE_SDK_MINOR		0x1
#define	__EDGE_SDK_DOT			0x1
#define	__EDGE_SDK_BUILD		0x0

#define EDGE_SDK_VERSION        ((__EDGE_SDK_MAJOR << 20) | (__EDGE_SDK_MINOR << 16) | (__EDGE_SDK_DOT << 12) | (__EDGE_SDK_BUILD << 0))

#endif // EDGE_VERSION_H
