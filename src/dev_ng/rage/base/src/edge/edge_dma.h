/* SCE CONFIDENTIAL
 * PlayStation(R)Edge 1.1.1
 * Copyright (C) 2007 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */

#ifndef __EDGE_DMA_H__
#define __EDGE_DMA_H__

//////////////////////////////////////////////////////////////////////////

#if defined(__SPU__)
#if 1 // use RAGE DMA functions
	#include "system/dma.h"
	#define EDGE_DMA_GET( ls, ea, size, tag, tid, rid )					sysDmaGet( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_GETB( ls, ea, size, tag, tid, rid )				sysDmaDmaGetb( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_GETF( ls, ea, size, tag, tid, rid )				sysDmaGetf( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_LARGE_GET( ls, ea, size, tag, tid, rid )			sysDmaLargeGet( ((const void*)ls), (ea), (size), (tag) )
	#define EDGE_DMA_SMALL_GET( ls, ea, size, tag, tid, rid )			sysDmaSmallGet( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_SMALL_GETF( ls, ea, size, tag, tid, rid )			sysDmaSmallGetf( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_LIST_GET( ls, ea, list, listSize, tag, tid, rid )	sysDmaListGet( (const void*)(ls), (list), (listSize), (tag) )
	#define EDGE_DMA_LIST_GETB( ls, ea, list, listSize, tag, tid, rid )	sysDmaListGetb( (const void*)(ls), (list), (listSize), (tag) )
	#define EDGE_DMA_GETLLAR( ls, ea, tid, rid )                        cellDmaGetllar( (const void*)(ls), (ea), (tid), (rid) )

	#define EDGE_DMA_PUT( ls, ea, size, tag, tid, rid )					sysDmaPut( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_PUTB( ls, ea, size, tag, tid, rid )				sysDmaPutb( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_PUTF( ls, ea, size, tag, tid, rid )				sysDmaPutf( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_LARGE_PUT( ls, ea, size, tag, tid, rid )			sysDmaLargePut( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_SMALL_PUT( ls, ea, size, tag, tid, rid )			sysDmaSmallPut( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_SMALL_PUTF( ls, ea, size, tag, tid, rid )			sysDmaSmallPutf( (const void*)(ls), (ea), (size), (tag) )
	#define EDGE_DMA_LIST_PUT( ls, ea, list, listSize, tag, tid, rid )	sysDmaListPut( (const void*)(ls), (list), (listSize), (tag) )
	#define EDGE_DMA_PUTLLC( ls, ea, tid, rid )                         cellDmaPutllc( (const void*)(ls), (ea), (tid), (rid) )
	#define EDGE_DMA_PUTLLUC( ls, ea, tid, rid )                        cellDmaPutlluc( (const void*)(ls), (ea), (tid), (rid) )

	#define EDGE_DMA_WAIT_ATOMIC_STATUS( )                              cellDmaWaitAtomicStatus( )
	#define EDGE_DMA_WAIT_TAG_STATUS_ALL( mask )						sysDmaWaitTagStatusAll( (mask) )
#else
	#include <cell/dma.h>
	#define EDGE_DMA_GET( ls, ea, size, tag, tid, rid )					cellDmaGet( (ls), (ea), (size), (tag), (tid), (rid) )
	#define EDGE_DMA_GETB( ls, ea, size, tag, tid, rid )				cellDmaGetb( (ls), (ea), (size), (tag), (tid), (rid) )
	#define EDGE_DMA_GETF( ls, ea, size, tag, tid, rid )				cellDmaGetf( (ls), (ea), (size), (tag), (tid), (rid) )
	#define EDGE_DMA_LARGE_GET( ls, ea, size, tag, tid, rid )			cellDmaLargeGet( (ls), (ea), (size), (tag), (tid), (rid) )
	#define EDGE_DMA_SMALL_GET( ls, ea, size, tag, tid, rid )			cellDmaSmallGet( (ls), (ea), (size), (tag), (tid), (rid) )
	#define EDGE_DMA_SMALL_GETF( ls, ea, size, tag, tid, rid )			cellDmaSmallGetf( (ls), (ea), (size), (tag), (tid), (rid) )
    #define EDGE_DMA_LIST_GET( ls, ea, list, listSize, tag, tid, rid )	cellDmaListGet( (ls), (ea), (list), (listSize), (tag), (tid), (rid) )
    #define EDGE_DMA_LIST_GETB( ls, ea, list, listSize, tag, tid, rid )	cellDmaListGetb( (ls), (ea), (list), (listSize), (tag), (tid), (rid) )
    #define EDGE_DMA_GETLLAR( ls, ea, tid, rid )                        cellDmaGetllar( (ls), (ea), (tid), (rid) )

	#define EDGE_DMA_PUT( ls, ea, size, tag, tid, rid )					cellDmaPut( (ls), (ea), (size), (tag), (tid), (rid) )
	#define EDGE_DMA_PUTB( ls, ea, size, tag, tid, rid )				cellDmaPutb( (ls), (ea), (size), (tag), (tid), (rid) )
	#define EDGE_DMA_PUTF( ls, ea, size, tag, tid, rid )				cellDmaPutf( (ls), (ea), (size), (tag), (tid), (rid) )
	#define EDGE_DMA_LARGE_PUT( ls, ea, size, tag, tid, rid )			cellDmaLargePut( (ls), (ea), (size), (tag), (tid), (rid) )
	#define EDGE_DMA_SMALL_PUT( ls, ea, size, tag, tid, rid )			cellDmaSmallPut( (ls), (ea), (size), (tag), (tid), (rid) )
	#define EDGE_DMA_SMALL_PUTF( ls, ea, size, tag, tid, rid )			cellDmaSmallPutf( (ls), (ea), (size), (tag), (tid), (rid) )
    #define EDGE_DMA_LIST_PUT( ls, ea, list, listSize, tag, tid, rid )	cellDmaListPut( (ls), (ea), (list), (listSize), (tag), (tid), (rid) )
    #define EDGE_DMA_PUTLLC( ls, ea, tid, rid )                         cellDmaPutllc( (ls), (ea), (tid), (rid) )
    #define EDGE_DMA_PUTLLUC( ls, ea, tid, rid )                        cellDmaPutlluc( (ls), (ea), (tid), (rid) )

    #define EDGE_DMA_WAIT_ATOMIC_STATUS( )                              cellDmaWaitAtomicStatus( )
    #define EDGE_DMA_WAIT_TAG_STATUS_ALL( mask )						cellDmaWaitTagStatusAll( (mask) )
#endif // 0

	typedef CellDmaListElement EdgeDmaListElement;

#else

	#error EDGE_DMA functions not defined

#endif

//////////////////////////////////////////////////////////////////////////

#endif	//__EDGE_DMA_H__
