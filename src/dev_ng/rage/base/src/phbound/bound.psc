<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::phBound">
  <u8 name="m_Type" />
  <u8 name="m_Flags" />
  <u16 name="m_PartIndex" />
  <float name="m_RadiusAroundCentroid" />
  <Vec4V name="m_BoundingBoxMaxXYZMarginW" />
  <Vec4V name="m_BoundingBoxMinXYZRefCountW" />
  <Vec4V name="m_CentroidOffsetXYZMaterialId0W" />
  <Vec4V name="m_CGOffsetXYZMaterialId1W" />
  <Vec4V name="m_VolumeDistribution" />
</structdef>
  

</ParserSchema>