//
// cranimation/weightmodifier.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "weightmodifier.h"

#include "math/amath.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crWeightModifier::crWeightModifier()
: m_RefCount(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifier::~crWeightModifier()
{
}

////////////////////////////////////////////////////////////////////////////////

float crWeightModifier::ModifyWeight(float originalWeight) const
{
	return originalWeight;
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifierSlowInSlowOut::crWeightModifierSlowInSlowOut()
: m_SlowIn(false)
, m_SlowOut(false)
{
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifierSlowInSlowOut::crWeightModifierSlowInSlowOut(bool slowIn, bool slowOut)
: m_SlowIn(false)
, m_SlowOut(false)
{
	SetSlowIn(slowIn);
	SetSlowOut(slowOut);
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifierSlowInSlowOut::~crWeightModifierSlowInSlowOut()
{
}

////////////////////////////////////////////////////////////////////////////////

float crWeightModifierSlowInSlowOut::ModifyWeight(float originalWeight) const
{
	if(m_SlowIn && m_SlowOut)
	{
		return SlowInOut(originalWeight);
	}
	else if(m_SlowIn)
	{
		return SlowIn(originalWeight);
	}
	else if(m_SlowOut)
	{
		return SlowOut(originalWeight);
	}
	else
	{
		return originalWeight;
	}
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifierStep::crWeightModifierStep()
: m_Step(0.5f)
{
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifierStep::crWeightModifierStep(float step)
: m_Step(0.5f)
{
	SetStep(step);
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifierStep::~crWeightModifierStep()
{
}

////////////////////////////////////////////////////////////////////////////////

float crWeightModifierStep::ModifyWeight(float originalWeight) const
{
	if(originalWeight >= m_Step)
	{
		return 1.f;
	}
	
	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifierSkew::crWeightModifierSkew()
: m_Start(0.f)
, m_End(1.f)
{
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifierSkew::crWeightModifierSkew(float start, float end)
: m_Start(0.f)
, m_End(1.f)
{
	SetSkew(start, end);
}

////////////////////////////////////////////////////////////////////////////////

crWeightModifierSkew::~crWeightModifierSkew()
{
}

////////////////////////////////////////////////////////////////////////////////

float crWeightModifierSkew::ModifyWeight(float originalWeight) const
{
	if(originalWeight <= m_Start)
	{
		return 0.f;
	}
	else if(originalWeight >= m_End)
	{
		return 1.f;
	}

	return Clamp((originalWeight - m_Start) / (m_End - m_Start), 0.f, 1.f);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

