//
// cranimation/channelbasic.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_CHANNELBASIC_H
#define CRANIMATION_CHANNELBASIC_H

#include "animchannel.h"

#if CR_DEV
namespace rage
{

class crAnimChannelRawFloat : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelRawFloat() : crAnimChannel(AnimChannelTypeRawFloat) {}
	virtual ~crAnimChannelRawFloat() {}
	DECLARE_ANIM_CHANNEL(crAnimChannelRawFloat);
	virtual void EvaluateFloat(float fTime, float& outFloat) const;
	virtual bool CreateFloat(float* floats, int frames, int skip, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive&);
private:
	atArray<float> m_Floats;
};

class crAnimChannelRawVector3 : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelRawVector3() : crAnimChannel(AnimChannelTypeRawVector3) {}
	crAnimChannelRawVector3(const crAnimChannelRawVector3&);
	virtual ~crAnimChannelRawVector3() {}
	DECLARE_ANIM_CHANNEL(crAnimChannelRawVector3);
	virtual void EvaluateVector3(float fTime, Vec3V_InOut outVectors) const;
	virtual bool CreateVector3(Vec3V* vectors, int frames, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive&);
private:
	atArray<Vec3V> m_Vectors;
};

class crAnimChannelRawQuaternion : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelRawQuaternion() : crAnimChannel(AnimChannelTypeRawQuaternion) {}
	crAnimChannelRawQuaternion(const crAnimChannelRawQuaternion&);
	virtual ~crAnimChannelRawQuaternion() {}
	DECLARE_PLACE(crAnimChannelRawQuaternion);
	DECLARE_ANIM_CHANNEL(crAnimChannelRawQuaternion);
	virtual void EvaluateQuaternion(float fTime, QuatV_InOut outQuaternion) const;
	virtual bool CreateQuaternion(QuatV* quaternions, int frames, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive&);
private:
	atArray<QuatV> m_Quaternions;
};

class crAnimChannelRawInt : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelRawInt() : crAnimChannel(AnimChannelTypeRawInt) {}
	virtual ~crAnimChannelRawInt() {}
	DECLARE_ANIM_CHANNEL(crAnimChannelRawInt);
	virtual void EvaluateInt(float fTime, s32& outInt) const;
	virtual bool CreateInt(s32* ints, int frames);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive&);
private:
	atArray<s32> m_Ints;
};

class crAnimChannelRawBool : public crAnimChannel
{
public:
	crAnimChannelRawBool() : crAnimChannel(AnimChannelTypeRawBool) {}
	virtual ~crAnimChannelRawBool() {}
	DECLARE_ANIM_CHANNEL(crAnimChannelRawBool);
	virtual void EvaluateBool(float fTime, bool& outBool) const;
	virtual bool CreateBool(bool* bools, int frames);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive&);
private:
	atArray<u8> m_PackedBools;
};

class crAnimChannelStaticFloat : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelStaticFloat() : crAnimChannel(AnimChannelTypeStaticFloat), m_Float(0.0f) {}
	virtual ~crAnimChannelStaticFloat() {}
	DECLARE_ANIM_CHANNEL(crAnimChannelStaticFloat);
	virtual void EvaluateFloat(float fTime, float& outFloat) const;
	virtual bool CreateFloat(float* floats, int frames, int skip, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive& a);
private:	
	float m_Float;
};

class crAnimChannelStaticQuaternion : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelStaticQuaternion();
	crAnimChannelStaticQuaternion(const crAnimChannelStaticQuaternion&);
	virtual ~crAnimChannelStaticQuaternion();
	DECLARE_ANIM_CHANNEL(crAnimChannelStaticQuaternion);
	virtual void EvaluateQuaternion(float fTime, QuatV_InOut outQuaternion) const;
	virtual bool CreateQuaternion(QuatV* quaternions, int frames, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive& a);
private:	
	datOwner<QuatV> m_Quaternion;
};

class crAnimChannelStaticSmallestThreeQuaternion : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelStaticSmallestThreeQuaternion();
	DECLARE_ANIM_CHANNEL(crAnimChannelStaticSmallestThreeQuaternion);
	virtual void EvaluateQuaternion(float fTime, QuatV_InOut outQuaternion) const;
	virtual bool CreateQuaternion(QuatV* quaternions, int frames, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive& a);
private:	
	f32 m_X, m_Y, m_Z;
};

class crAnimChannelStaticInt : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelStaticInt() : crAnimChannel(AnimChannelTypeStaticInt), m_Int(0) {}
	virtual ~crAnimChannelStaticInt() {}
	DECLARE_ANIM_CHANNEL(crAnimChannelStaticInt);
	virtual void EvaluateInt(float fTime, s32& outInt) const;
	virtual bool CreateInt(s32* ints, int frames);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive& a);
private:	
	s32 m_Int;
};

class crAnimChannelStaticVector3 : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelStaticVector3();
	crAnimChannelStaticVector3(const crAnimChannelStaticVector3&);
	virtual ~crAnimChannelStaticVector3();
	DECLARE_ANIM_CHANNEL(crAnimChannelStaticVector3);
	virtual void EvaluateVector3(float fTime, Vec3V_InOut outVector) const;
	virtual bool CreateVector3(Vec3V* vectors, int frames, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive& a);
private:	
	datOwner<Vec3V> m_Vector;
};

} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_CHANNELBASIC_H
