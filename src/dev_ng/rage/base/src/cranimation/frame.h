//
// cranimation/frame.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_FRAME_H
#define CRANIMATION_FRAME_H

#include "animation_config.h"
#include "framedata.h"

#include "system/interlocked.h"
#include "system/memory.h"
#include "vectormath/transformv.h"

namespace rage
{

class crAnimation;
class crDumpOutput;
class crFrameAccelerator;
class crFrameFilter;
class crJointData;
class crSkeleton;
class crSkeletonData;
class crWeightSet;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE:
// Frames are a core class in the RAGE animation system.
// They are used to hold a snapshot of values from an animation at a particular moment in time.
// This snapshot can then be manipulated and combined with others.
// Frames contain a collection of degrees of freedom (DOFs).
// The frame data structure (which frames reference) describes the collection,
// while frames hold the actual values (and flags which mark them valid/invalid).
// When frames are combined, only degrees of freedom common to both frames are combined.
// This allows frames to only capture and combined what is required, not all degrees of freedom
// present within a character (this can be useful for partial composition or level of detail).
// Frame operations can be additionally controlled by frame filters, which allow
// per degree of freedom control over the operation.
class ALIGNAS(16) crFrame
{
public:

	// PURPOSE: Default constructor
	crFrame();

	// PURPOSE: Initializing constructor
	// PARAMS:
	// frameData - reference to the frame data structure that describes the contents of this frame
	// buffer - optional pointer to external buffer to use to store values, see notes for details
	//   (use NULL for automatically created buffer)
	// bufferSize - if external buffer is provided, specify size in bytes, see notes for details
	// bufferOwner - if external buffer is provided, is frame to take over responsibility for destruction
	// NOTES: Will increment reference count on frame data structure, and release on shutdown
	// If external buffer is supplied, it must be 16-byte aligned,
	// and must also be a multiple of 16-bytes in size.
	// To check the minimum size of buffer needed to support a given frame data structure
	// use crFrameData::GetBufferSize
	crFrame(const crFrameData& frameData, u8* buffer=NULL, u32 bufferSize=0, bool bufferOwner=false, bool invalidate=true);

	// PURPOSE: Copy constructor
	// NOTES:
	// Will share frame data used by source frame (incrementing reference counter)
	// Initializes, using automatically created internal buffer, and then copies values from source frame
	crFrame(const crFrame& src);

	// PURPOSE: Resource constructor
	crFrame(datResource& rsc);

	// PURPOSE: Destructor
	~crFrame();

	// PURPOSE: Initialization
	// PARAMS:
	// frameData - reference to the frame data structure that describes the contents of this frame
	// buffer - optional pointer to external buffer to use to store values, see notes for details
	//   (use NULL for automatically created buffer)
	// bufferSize - if external buffer is provided, specify size in bytes, see notes for details
	// bufferOwner - if external buffer is provided, is frame to take over responsibility for destruction
	// invalidate - if true will invalidate the frame
	// NOTES: Will increment reference count on frame data structure, and release on shutdown
	// If external buffer is supplied, it must be 16-byte aligned,
	// and must also be a multiple of 16-bytes in size.
	// To check the minimum size of buffer needed to support a given frame data structure
	// use crFrameData::GetBufferSize
	void Init(const crFrameData& frameData, u8* buffer=NULL, u32 bufferSize=0, bool bufferOwner=false, bool invalidate=true);

	// PURPOSE: Shutdown, free/release any dynamic resources
	// NOTES:
	// Decrements reference count on any frame data structure
	// Destroys any value buffer that was automatically created,
	// or externally passed in with responsibility for ownership.
	void Shutdown();

	// PURPOSE: Assignment operator
	// NOTES:
	// Will share frame data used by source frame (incrementing reference counter)
	// Initializes, using automatically created internal buffer, and then copies values from source frame
	const crFrame& operator=(const crFrame&);

	// PURPOSE: Placement
	DECLARE_PLACE(crFrame);

	// PURPOSE: Resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Resourcing version
	static const int RORC_VERSION = 3;


	// PURPOSE: Add reference
	void AddRef() const;

	// PURPOSE: Release reference
	u32 Release() const;

	// PURPOSE: Get current reference count
	u32 GetRef() const;


	// PURPOSE: Get frame data structure, which describes contents of this frame
	// RETURNS: const pointer to frame data structure (or NULL if frame not initialized)
	const crFrameData* GetFrameData() const;

	// PURPOSE: Exchange frame data structure, which describes contents of this frame
	// RETURNS: true - if exchange succeeded, false - failed to exchange frame data (frame is unchanged)
	// NOTES: Transfers all values common to both frame data structures, remainder marked invalid
	// Performance warning, this operation can be extremely expensive - in memory and processing
	// Depending on memory requirements, it can involve the dynamic allocation of a new buffer
	// Can fail if frame was constructed from external buffer that is of insufficient size
	// to support new frame data
	bool ExchangeFrameData(const crFrameData&);


	// PURPOSE:	Sets (or copies) degree of freedom values from source frame into destination frame.
	// PARAMS:
	// frame - source frame
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// NOTES
	// If the degree of freedom in the destination frame cannot be found in the source frame
	// (or the operation is prohibited by the frame filter) then the value in the destination
	// frame remains unchanged.
	// Degrees of freedom that exist in the source frame and not in the destination frame are ignored.
	void Set(const crFrame& frame, crFrameFilter* filter=NULL);

	// PURPOSE: Blends the degrees of freedom values from the source and destination frames,
	// using the weight provided.
	// PARAMS:
	// weight - blending weight 0.0 (0%) -> 1.0 (100%) of source blended with destination
	// frame - source frame
	// mergeBlend - if source degree of freedom is invalid, merge blend
	//   selects valid destination degrees of freedom (irrespective of weight)
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// NOTES:
	// The following examples illustrate how different weights affect the blending.
	// The actual math behind the blending is more complex than this -
	// this is just to illustrate conceptually what is happening:
	// If weight 0.0, result = destination frame
	// If weight 1.0, result = source frame (if mergeBlend and source invalid, result = destination)
	// If weight 0.5, result = destination frame * 0.5 + source frame * 0.5
	// (assuming the same degrees of freedom exist in both frames, and no frame filter is used).
	void Blend(float weight, const crFrame& frame, bool mergeBlend=true, crFrameFilter* filter=NULL);

	// PURPOSE: Adds the degree of freedom values from the source and destination frames.
	// PARAMS:
	// weight - adding weight 0.0 (0%), 1.0 (100%) of source added to destination
	//   (negative and > 1.0 weights can be used too)
	// frame - source frame
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// NOTES:
	// The following examples illustrate how different weights affect the adding.
	// The actual math behind the adding is more complex than this -
	// this is just to illustrate conceptually what is happening:
	// If weight 0.0, result = destination frame
	// If weight 1.0, result = destination frame + source frame
	// If weight 0.5, result = destination frame + source frame * 0.5
	// If weight -1.0, result = destination frame - source frame
	// If weight 2.0, result = destination frame + source frame * 2.0
	// (assuming the same degrees of freedom exist in both frames, and no frame filter is used).
	void Add(float weight, const crFrame& frame, crFrameFilter* filter=NULL);

	// PURPOSE: Merges the degrees of freedom values from the source and destination frames.
	// Degrees of freedom values will only be copied from source to destination
	// if destination degrees of freedom are non-valid.
	// PARAMS:
	// frame - source frame
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	void Merge(const crFrame& frame, crFrameFilter* filter=NULL);

	// PURPOSE: Subtracts the degree of freedom values in the source frame from the destination frame.
	// PARAMS
	// weight - subtraction weight 0.0 (0%), 1.0 (100%) of source subtracted from destination
	//   (negative and > 1.0 weights can be used too)
	// frame - source frame
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// NOTES:
	// The following examples illustrate how different weights affect the subtraction.
	// The actual math behind the subtraction is more complex than this -
	// this is just to illustrate conceptually what is happening:
	// If weight 0.0, result = destination frame
	// If weight 1.0, result = destination frame - source frame
	// If weight 0.5, result = destination frame - source frame * 0.5
	// If weight -1.0, result = destination frame + source frame
	// If weight 2.0, result = destination frame - source frame * 2.0
	// (assuming the same degrees of freedom exist in both frames, and no frame filter is used).
	void Subtract(float weight, const crFrame& frame, crFrameFilter* filter=NULL);

	// PURPOSE: Multiplies the destination frame by the weight.
	// PARAMS:
	// weight - multiplying weight
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// NOTES:
	// The following examples illustrate how different weights affect the multiplying.
	// The actual math behind the multiplying is more complex than this -
	// this is just to illustrate conceptually what is happening:
	// If weight 0.0, result = zero
	// If weight 1.0, result = destination frame
	// If weight -1.0, result = -destination frame
	// If weight 2.0, result = destination frame * 2.0
	// (assuming the exist in frame, and no frame filter is used).
	void Multiply(float weight, crFrameFilter* filter=NULL);

	// PURPOSE: Mirrors the degrees of freedom values in the frame.
	// PARAMS:
	// skelData - used to provide information about the symmetry of the skeleton
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// NOTES:
	// Both pairs of symmetrical degrees of freedom must be present and valid for this call to succeed.
	void Mirror(const crSkeletonData& skelData, crFrameFilter* filter=NULL);

	// PURPOSE: Zeros (resets/clears) all degree of freedom values in the frame.
	// PARAMS: filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	void Zero(crFrameFilter* filter=NULL);

	// PURPOSE: Sets the (skeleton based) degree of freedom values in a frame to their identity values.
	// PARAMS:
	// skelData - skeleton data to use to obtain the identity values from.
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// NOTE:
	// This call only sets bone translate and rotate degrees of freedom,
	// which are present in the skeleton data, to identity
	// (where as zero resets all degrees of freedom).
	// Also, unlike zero, bone translates and rotates with non-zero identities
	// will be set to their non-zero values.
	// For a version that works with all degrees of freedom within a creature, see crCreature::Identity.
	void IdentityFromSkel(const crSkeletonData& skelData, crFrameFilter* filter=NULL);

	// PURPOSE: Composite frame with data from an animation.
	// PARAMS:
	// anim - animation to composite
	// time - time in animation to composite (in seconds)
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	void Composite(const crAnimation& anim, float time, crFrameFilter* filter=NULL);

	// PURPOSE: Composite frame with data from an animation, and calculate delta values (for mover)
	// PARAMS:
	// anim - animation to composite
	// time - time in animation to composite (in seconds)
	// delta - delta time in animation to composite (in seconds)
	// rangeStart,rangeEnd - range of decompress delta from (in second), required to loop correctly in sub ranges
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	void CompositeWithDelta(const crAnimation& anim, float time, float delta, float rangeStart, float rangeEnd, crFrameFilter* filter=NULL);
	void CompositeWithDelta(const crAnimation& anim, float time, float delta, crFrameFilter* filter=NULL);

	// PURPOSE: Composite frame with data from an animation,
	// while blending with the existing contents of the frame.
	// PARAMS:
	// weight - weight to blend animation with existing values
	// anim - animation to composite
	// time - time in animation to composite (in seconds)
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// NOTES:
	// The following examples illustrate how different weights affect the blending.
	// The actual math behind the blending is more complex than this -
	// this is just to illustrate conceptually what is happening:
	// If weight 0.0, result = destination frame
	// If weight 1.0, result = animation
	// If weight 0.5, result = destination frame * 0.5 + animation * 0.5
	// (assuming the same degrees of freedom exist in both frames, and no frame filter is used).
	void CompositeBlend(float weight, const crAnimation& anim, float time, crFrameFilter* filter=NULL);

	// PURPOSE: Composite frame with data from an animation, and calculate delta values (for mover),
	// while blending with the existing contents of the frame.
	// PARAMS:
	// weight - weight to blend animation with existing values
	// anim - animation to composite
	// time - time in animation to composite (in seconds)
	// delta - delta time in animation to composite (in seconds)
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// NOTES:
	// The following examples illustrate how different weights affect the blending.
	// The actual math behind the blending is more complex than this -
	// this is just to illustrate conceptually what is happening:
	// If weight 0.0, result = destination frame
	// If weight 1.0, result = animation
	// If weight 0.5, result = destination frame * 0.5 + animation * 0.5
	// (assuming the same degrees of freedom exist in both frames, and no frame filter is used).
	void CompositeWithDeltaBlend(float weight, const crAnimation& anim, float time, float delta, crFrameFilter* filter=NULL);

	// PURPOSE: Invalidates all degrees of freedom within the frame
	// PARAMS:
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all)
	void Invalidate(crFrameFilter* filter=NULL);

	// PURPOSE: Dirties and invalidates all degrees of freedom values within the frame
	// PARAMS:
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all)
	// NOTES:
	// Dirty fills frame values with bad values (NaN's), before marking them as invalid.
	// Useful for debugging, to check code for unsafe access to degree of freedom values.
	void Dirty(crFrameFilter* filter=NULL);

	// PURPOSE: Normalizes all the valid degrees of freedom in the frame
	// PARAMS:
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all)
	// NOTES:
	// Only normalizes degrees of freedom of quaternion type, all other types remain unmodified
	void Normalize(crFrameFilter* filter=NULL);


	// PURPOSE: Checks all degrees of freedom are valid
	// PARAMS:
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// RETURNS:
	// true - if all degrees of freedom are valid
	// false - if any invalid degrees of freedom are encountered
	bool Valid(crFrameFilter* filter=NULL) const;

	// PURPOSE: Checks all valid degrees of freedom contain legal values
	// PARAMS:
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	// RETURNS:
	// true - if all valid degrees of freedom are contain legal values
	// false - if any valid degrees of freedom are contain illegal values (NaNs etc)
	bool Legal(crFrameFilter* filter=NULL) const;

	// PURPOSE: Apply joint limits to the degrees of freedom
	// PARAMS:
	// skelData - skeleton data used to supply the joint limits
	// useJointData - true, use the new joint data, or false, use the old Euler limits
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	void Limit(const crSkeletonData& skelData, const crJointData& jointData, crFrameFilter *filter=NULL);

	// PURPOSE: Blends the degree of freedom values from multiple frames, using the weights provided.
	// PARAMS:
	// num - number of frames to blend together (not including destination frame)
	// weights - pointer to blending weights [0..1]
	//   (must not be NULL, num entries, values depend on if fixWeights is specified, see notes)
	// frames - pointer to array of pointers to source frames
	//   (must not be NULL, num entries, no NULL pointers)
	// fixWeights - correct the weights for sequential application (see notes)
	// mergeBlend - if source degree of freedoms invalid,
	//   merge blend selects valid destination degrees of freedom (irrespective of weight)
	// filter - common frame filter, controls which degrees of freedom are affected (leave NULL for all, see notes).
	// filters - individual frame filters, provides per input degree of freedom control (leave NULL if not required, see notes).
	// NOTES:
	// *** Weights ***
	// Weights work in two different ways, depending on if fix weights is specified or not.
	// If fix weights is not specified, then weights work conceptually as follows:
	// Result =  (1-w2) * ( (1-w1) * ( (1-w0)*Fn + w0*F0 ) + w1*F1 ) + w2*F2
	// i.e. the dest frame and 0th frame are blended, using the 0th weight.  This result, is then blended
	// with the 1st frame, using the 1st weight, and so on...
	// If fix weights is specified, then the weights conceptually work in a different way:
	// Result = (1-w0-w1-w2)*Fn + w0*F0 + w1*F1+ w2*F2
	// i.e. all the weights must add up to <= 1.0 (any remainder is applied to the destination frame).
	// Although the frames are still blended sequentially, the result is as if the weights were
	// specifying a percentage of the final frame each source frame will contribute
	// (and any remainder is specifying how much the destination frame contributes).
	// In practice, internally just prior to actually using the weights (after all the filtering stages
	// have been completed) if fix weights is specified then an extra step takes place to modify the
	// weights to obtain this percentage behavior (even though in the blending is actually a sequential
	// series of pair operations).
	// If fix weights is true, care must be taken at all stages (i.e. both input and all filtering stages)
	// to ensure the sum of the weights never exceeds 1.0.
	// *** Filters ***
	// N-way operations support three different filters; individual, common and parallel.
	// Individual filters are specified by providing a pointer to an array of pointers to filters.
	// The entire array can either be left as NULL if no individual filtering is required at all.
	// If an array is provided, then the n'th entry in the filters array should point to a filter
	// that will be used to determine degree of freedom participation from the n'th source frame.
	// Alternatively, if only some frames require filtering, some filter pointers in the filter array can
	// be set to NULL to skip filtering on those frames.
	// The common filter is applied to the destination frame, rather than the source frames.
	// This means that if a degree of freedom is filtered in the common frame, it is skipped regardless of
	// what the individual filters may do.  The common filter filters degrees of freedom for _all_ frames,
	// whilst the individual filters just filter DOFs for a single input frame.
	// Finally, the parallel filter can be used to perform weight re-balancing.
	// After both common filtering and individual filtering of degree of freedom has been performed,
	// the final set of all the weights is passed into the parallel filter to allow for any
	// project specific correction of weights (to deal with special case behavior of missing/filtered
	// degree of freedoms). The FitlerDofs() call is made on the parallel filter, where it can see and
	// manipulate all the weights for all the frames (for a particular degree of freedom) in parallel,
	// so it can perform this final pass correction.
	// Warning: Parallel filtering has significant negative performance implications.
	void BlendN(u32 num, const float* weights, const crFrame*const* frames, bool fixWeights=true, bool mergeBlend=true, crFrameFilter* filter=NULL, crFrameFilter** filters=NULL);


	// PURPOSE: Add (or subtract) the degree of freedom values from multiple frames, using the weights provided.
	// PARAMS:
	// num - number of frames to blend together (not including destination frame)
	// weights - pointer to adding weights (must not be NULL, num entries)
	// frames - pointer to array of pointers to source frames
	//   (must not be NULL, num entries, no NULL pointers)
	// filter - common frame filter, controls which degrees of freedom are affected (leave NULL for all, see notes).
	// filters - individual frame filters, provides per input degree of freedom control (leave NULL if not required, see notes).
	// NOTES:
	// See BlendN notes for discussion of three different filter types.
	void AddN(u32 num, const float* weights, const crFrame*const* frames, crFrameFilter* filter=NULL, crFrameFilter** filters=NULL);

	// PURPOSE: Merge the degree of freedom values from multiple frames.
	// PARAMS:
	// num - number of frames to blend together (not including destination frame)
	// weights - pointer to adding weights (may be omitted, with NULL, assumes merge all frames)
	// frames - pointer to array of pointers to source frames
	//   (must not be NULL, num entries, no NULL pointers)
	// filter - common frame filter, controls which degrees of freedom are affected (leave NULL for all, see notes).
	// filters - individual frame filters, provides per input degree of freedom control (leave NULL if not required, see notes).
	// NOTES:
	// Weights are used to control which frames are merged.  As merge is a binary operation (unlike blend/add),
	// actual weight value is ignored, except to treat it as a binary state for the controlling which frames
	// and degrees of freedom participate in the merge (i.e. weight != 0.0).
	// See BlendN notes for discussion of three different filter types.
	void MergeN(u32 num, const float* weights, const crFrame*const* frames, crFrameFilter* filter=NULL, crFrameFilter** filters=NULL);

	// PURPOSE: Sets local matrices in skeleton using bone translate and rotate degree of freedom values.
	// PARAMS:
	// skel - skeleton instance to set values into.
	// normalize - normalizes skeleton matrix rotations
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	void Pose(crSkeleton& skel, bool normalize=false, crFrameFilter* filter=NULL) const;

	// PURPOSE: Sets the values for the bone position and rotation degree of freedom
	// from the local matrices in the skeleton
	// skel - source skeleton instance
	// normalize - normalizes quaternion rotation degrees of freedom
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	void InversePose(const crSkeleton& skel, bool normalize=false, crFrameFilter* filter=NULL);


	// PURPOSE: Get number of degrees of freedom contained within this frame
	// RETURNS: Number of degrees of freedom
	// NOTES: Convenience, this is really a function of the frame data structure
	int GetNumDofs() const;

	// PURPOSE: Invalidate an individual degree of freedom
	// track - degree of freedom track
	// id - degree of freedom id
	// RETURNS: true - invalidated degree of freedom, false - failed to find degree of freedom
	bool InvalidateDof(u8 track, u16 id);

	// PURPOSE: Check if individual degree of freedom present with the frame
	// track - degree of freedom track
	// id - degree of freedom id
	// RETURNS: true - degree of freedom present, false - degree of freedom absent
	// NOTES: Convenience, this is really a function of the frame data structure
	bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Check if individual degree of freedom present with the frame, and that it is valid
	// track - degree of freedom track
	// id - degree of freedom id
	// RETURNS: true - degree of freedom present and valid, false - degree of freedom absent or invalid
	bool HasDofValid(u8 track, u16 id) const;

	// PURPOSE: Swap the values of two degrees of freedom (and their valid/invalid statuses)
	// track0 - first degree of freedom track
	// id0 - first degree of freedom id
	// track1 - second degree of freedom track
	// id1 - second degree of freedom id
	// RETURNS: true - swap succeeded, false - swap failed
	// NOTES: Both degrees of freedom must be present and of same type for swap to be successful
	bool SwapDofValue(u8 track0, u16 id0, u8 track1, u16 id1);


	// PURPOSE: Gets bone degree of freedoms to a transform identified by a bone id provided
	// PARAMS:
	// boneId - bone id
	// outTransform - transform to store values into
	// tolerateMissingDof - can tolerate a single missing value (silently leaves uninitialized data, see notes)
	// RETURNS:	
	// true - if degrees of freedom (or degree of freedom, if tolerateMissingDof is true) successfully retrieved.
	// NOTES:
	// If false returned, transform passed in is unchanged.
	// If tolerateMissingDof is true, and one of the two degrees of freedom is missing
	// call will still return true but part of the transform will be uninitialized.
	// This is by design.  Make sure you initialize the transform before calling when using this option.
	bool GetBoneSituation(u16 boneId, TransformV_InOut outTransform, bool tolerateMissingDof=false) const;

	// PURPOSE: Sets bone degree of freedoms from a transform identified using bone id provided
	// PARAMS:
	// boneId - bone id
	// transform - transform to set
	// tolerateMissingDof - can tolerate a single missing value
	// RETURNS:	
	// true - if degrees of freedom (or degree of freedom, if tolerateMissingDof is true) successfully set.
	bool SetBoneSituation(u16 boneId, TransformV_In transform, bool tolerateMissingDof=false);

	// PURPOSE: Gets bone degree of freedoms to a matrix identified by a bone id provided
	// PARAMS:
	// boneId - bone id
	// outMatrix - matrix to store values into
	// tolerateMissingDof - can tolerate a single missing value (silently leaves uninitialized data, see notes)
	// RETURNS:	
	// true - if degrees of freedom (or degree of freedom, if tolerateMissingDof is true) successfully retrieved.
	// NOTES:
	// If false returned, transform passed in is unchanged.
	// If tolerateMissingDof is true, and one of the two degrees of freedom is missing
	// call will still return true but part of the matrix will be uninitialized.
	// This is by design.  Make sure you initialize the matrix before calling when using this option.
	bool GetBoneMatrix(u16 boneId, Mat34V_InOut outMtx, bool tolerateMissingDof=false) const;

	// PURPOSE: Sets bone degree of freedoms from a matrix identified using bone id provided
	// PARAMS:
	// boneId - bone id
	// matrix - matrix to set
	// tolerateMissingDof - can tolerate a single missing value
	// RETURNS:	
	// true - if degrees of freedom (or degree of freedom, if tolerateMissingDof is true) successfully set.
	bool SetBoneMatrix(u16 boneId, Mat34V_In mtx, bool tolerateMissingDof=false);

	// PURPOSE: Get bone translation degree of freedom to a vector3 identified by bone id provided
	// PARAMS:
	// boneId - bone id
	// outVector3 - vector3 to store value into
	// RETURNS:
	// true - if degrees of freedom successfully set
	bool GetBoneTranslation(u16 boneId, Vec3V_InOut outVector3) const;

	// PURPOSE: Set bone translation degree of freedom from a vector3 identified by bone id provided
	// PARAMS:
	// boneId - bone id
	// vector3 - vector3 containing rotation
	// RETURNS:
	// true - if degrees of freedom successfully set
	bool SetBoneTranslation(u16 boneId, Vec3V_In vector3);

	// PURPOSE: Get bone rotation degree of freedom to a quaternion identified by bone id provided
	// PARAMS:
	// boneId - bone id
	// outQuaternion - quaternion to store value into
	// RETURNS:
	// true - if degrees of freedom successfully set
	bool GetBoneRotation(u16 boneId, QuatV_InOut outQuaternion) const;

	// PURPOSE: Set bone rotation degree of freedom from a quaternion identified by bone id provided
	// PARAMS:
	// boneId - bone id
	// quaternion - quaternion containing rotation
	// RETURNS:
	// true - if degrees of freedom successfully set
	bool SetBoneRotation(u16 boneId, QuatV_In quaternion);

	// PURPOSE: Get bone rotation degree of freedom to a matrix identified by bone id provided
	// PARAMS:
	// boneId - bone id
	// outMtx - matrix to store value into
	// RETURNS:
	// true - if degrees of freedom successfully set
	// NOTES: Matrix positional component is unchanged
	bool GetBoneMatrix3x3(u16 boneId, Mat34V_InOut outMtx) const;

	// PURPOSE: Set bone rotation degree of freedom from a matrix identified by bone id provided
	// PARAMS:
	// boneId - bone id
	// mtx - matrix containing rotation
	// RETURNS:
	// true - if degrees of freedom successfully set
	// NOTES: Matrix positional component ignored
	bool SetBoneMatrix3x3(u16 boneId, Mat34V_In mtx);


	// PURPOSE: Gets mover degree of freedoms to a transform
	// PARAMS:
	// moverId - mover id
	// outTransform - transform to store values into
	// RETURNS:	
	// true - if degrees of freedom successfully retrieved.
	// false - degrees of freedom not retrieved, transform state undefined.
	bool GetMoverSituation(TransformV_InOut outTransform) const;

	// PURPOSE: Sets mover degree of freedoms from a transform
	// PARAMS:
	// transform - transform containing values
	// RETURNS:	
	// true - if degrees of freedom successfully set.
	bool SetMoverSituation(TransformV_In transform);

	// PURPOSE: Gets mover degree of freedoms to a matrix identified using mover id provided
	// PARAMS:
	// moverId - mover id
	// outTransform - transform to store values into
	// RETURNS:	
	// true - if degrees of freedom successfully retrieved.
	// false - degrees of freedom not retrieved, transform state undefined.
	bool GetMoverSituation(u16 moverId, TransformV_InOut outTransform) const;

	// PURPOSE: Sets mover degree of freedoms from a transform identified using mover id provided
	// PARAMS:
	// moverId - mover id
	// transform - transform containing values
	// RETURNS:	
	// true - if degrees of freedom successfully set.
	bool SetMoverSituation(u16 moverId, TransformV_In transform);

	// PURPOSE: Gets mover degree of freedoms to a matrix
	// PARAMS:
	// outMtx - matrix to store values into
	// RETURNS:	
	// true - if degrees of freedom successfully retrieved.
	// false - degrees of freedom not retrieved, matrix state undefined.
	bool GetMoverMatrix(Mat34V_InOut outMtx) const;

	// PURPOSE: Sets mover degree of freedoms from a matrix
	// PARAMS:
	// mtx - matrix containing values
	// RETURNS:	
	// true - if degrees of freedom successfully set.
	bool SetMoverMatrix(Mat34V_In mtx);

	// PURPOSE: Gets mover degree of freedoms to a matrix identified using mover id provided
	// PARAMS:
	// moverId - mover id
	// outMtx - matrix to store values into
	// RETURNS:	
	// true - if degrees of freedom successfully retrieved.
	// false - degrees of freedom not retrieved, matrix state undefined.
	bool GetMoverMatrix(u16 moverId, Mat34V_InOut outMtx) const;

	// PURPOSE: Sets mover degree of freedoms from a matrix identified using mover id provided
	// PARAMS:
	// moverId - mover id
	// mtx - matrix containing values
	// RETURNS:	
	// true - if degrees of freedom successfully set.
	bool SetMoverMatrix(u16 moverId, Mat34V_In mtx);


	// PURPOSE: Gets degree of freedoms to a transform identified using tracks and id provided
	// PARAMS:
	// posTrack - degree of freedom positional track index
	// rotTrack - degree of freedom rotational track index
	// id - degree of freedom id
	// outMtx - matrix to store values into
	// tolerateMissingDof - can tolerate a single missing value (silently leaves uninitialized data, see notes)
	// RETURNS:	
	// true - if degrees of freedom (or degree of freedom, if tolerateMissingDof is true) successfully retrieved.
	// NOTES:
	// If false returned, transform passed in is unchanged.
	// If tolerateMissingDof is true, and one of the two degrees of freedom is missing
	// call will still return true but part of the transform will be uninitialized.
	// This is by design.  Make sure you initialize the transform before calling when using this option.
	bool GetSituation(u8 posTrack, u8 rotTrack, u16 id, TransformV_InOut outTransform, bool tolerateMissingDof=false) const;

	// PURPOSE: Sets degree of freedoms from a transform identified using tracks and id provided
	// PARAMS:
	// posTrack - degree of freedom positional track index
	// rotTrack - degree of freedom rotational track index
	// id - degree of freedom id
	// transform - transform to set
	// tolerateMissingDof - can tolerate a single missing value
	// RETURNS:	
	// true - if degrees of freedom (or degree of freedom, if tolerateMissingDof is true) successfully set.
	bool SetSituation(u8 posTrack, u8 rotTrack, u16 id, TransformV_In transform, bool tolerateMissingDof=false);

	// PURPOSE: Gets degree of freedoms to a matrix identified using tracks and id provided
	// PARAMS:
	// posTrack - degree of freedom positional track index
	// rotTrack - degree of freedom rotational track index
	// id - degree of freedom id
	// outMtx - matrix to store values into
	// tolerateMissingDof - can tolerate a single missing value (silently leaves uninitialized data, see notes)
	// RETURNS:	
	// true - if degrees of freedom (or degree of freedom, if tolerateMissingDof is true) successfully retrieved.
	// NOTES:
	// If false returned, matrix passed in is unchanged.
	// If tolerateMissingDof is true, and one of the two degrees of freedom is missing
	// call will still return true but part of the matrix will be uninitialized.
	// This is by design.  Make sure you initialize the matrix before calling when using this option.
	bool GetMatrix(u8 posTrack, u8 rotTrack, u16 id, Mat34V_InOut outMtx, bool tolerateMissingDof=false) const;

	// PURPOSE: Sets degree of freedoms from a matrix identified using tracks and id provided
	// PARAMS:
	// posTrack - degree of freedom positional track index
	// rotTrack - degree of freedom rotational track index
	// id - degree of freedom id
	// mtx - matrix containing values
	// tolerateMissingDof - can tolerate a single missing value
	// RETURNS:	
	// true - if degrees of freedom (or degree of freedom, if tolerateMissingDof is true) successfully set.
	bool SetMatrix(u8 posTrack, u8 rotTrack, u16 id, Mat34V_In mtx, bool tolerateMissingDof=false);

	// PURPOSE: Gets vector3 degree of freedom identified using track and id provided
	// PARAMS:
	// track - degree of freedom track index
	// id - degree of freedom id
	// outVector3 - vector3 value found (undefined if failed to retrieve - see return value)
	// RETURNS: true - if vector3 value successfully retrieved, false - degree of freedom not found
	bool GetVector3(u8 track, u16 id, Vec3V_InOut outVector3) const;

	// PURPOSE: Sets vector3 degree of freedom identified using track and id provided
	// PARAMS:
	// track - degree of freedom track index
	// id - degree of freedom id
	// vector3 - vector3 value to set
	// RETURNS: true - if vector3 value successfully set
	bool SetVector3(u8 track, u16 id, Vec3V_In vector3);

	// PURPOSE: Gets quaternion degree of freedom identified using track and id provided
	// PARAMS:
	// track - degree of freedom track index
	// id - degree of freedom id
	// outQuaternion - quaternion value found (undefined if failed to retrieve - see return value)
	// RETURNS: true - if quaternion value successfully retrieved, false - degree of freedom not found
	bool GetQuaternion(u8 track, u16 id, QuatV_InOut outQuaternion) const;

	// PURPOSE: Sets quaternion degree of freedom identified using track and id provided
	// PARAMS:
	// track - degree of freedom track index
	// id - degree of freedom id
	// quaternion - quaternion value to set
	// RETURNS: true - if quaternion value successfully set
	bool SetQuaternion(u8 track, u16 id, QuatV_In quaternion);

	// PURPOSE: Gets quaternion degree of freedom identified using track and id provided as matrix rotation
	// PARAMS:
	// track - degree of freedom track index
	// id - degree of freedom id
	// outMtx - quaternion value found as matrix rotation (undefined if failed to retrieve - see return value)
	// RETURNS: true - if quaternion value successfully retrieved, false - degree of freedom not found
	// NOTES: translation component of matrix unchanged
	bool GetMatrix3x3(u8 track, u16 id, Mat34V_InOut outMtx) const;

	// PURPOSE: Sets quaternion degree of freedom identified using track and id provided from matrix rotation
	// PARAMS:
	// track - degree of freedom track index
	// id - degree of freedom id
	// outMtx - quaternion value to set from matrix rotation
	// RETURNS: true - if quaternion value successfully set
	// NOTES: translation component of matrix ignored
	bool SetMatrix3x3(u8 track, u16 id, Mat34V_In mtx);

	// PURPOSE: Gets float degree of freedom identified using track and id provided
	// PARAMS:
	// track - degree of freedom track index
	// id - degree of freedom id
	// outFloat - float value found (undefined if failed to retrieve - see return value)
	// RETURNS: true - if float value successfully retrieved, false - degree of freedom not found
	bool GetFloat(u8 track, u16 id, float& outFloat) const;

	// PURPOSE: Sets float degree of freedom identified using track and id provided
	// PARAMS:
	// track - degree of freedom track index
	// id - degree of freedom id
	// f - float value to set
	// RETURNS: true - if float value successfully set
	bool SetFloat(u8 track, u16 id, float f);


	// PURPOSE: Get degree of freedom value identified using track and id provided
	// PARAMS:
	// track - degree of freedom track index
	// id - degree of freedom id
	// outVal - template value found (undefined if failed to retrieve - see return value)
	// RETURNS: true - if value successfully retrieved, false - degree of freedom not found
	template<typename _T>
	bool GetValue(u8 track, u16 id, _T& outVal) const;

	// PURPOSE: Set degree of freedom identified using track and id provided
	// track - degree of freedom track index
	// id - degree of freedom id
	// val - template value to set
	// RETURNS: true - if value successfully set
	template<typename _T>
	bool SetValue(u8 track, u16 id, const _T& val);
	

	// PURPOSE: Return frame's signature
	// RETURNS: Frame signature (zero signifies invalid or yet to be calculated signature)
	// NOTES: The value of a signature is meaningless, except zero (which signifies an invalid
	// or yet to be calculated signature).  Signatures are just a hash of the frame structure.
	// If two frames have matching signatures, their structures are identical.  Signatures
	// can also be useful for detecting changes in a frame's structure.
	u32 GetSignature() const;

	// PURPOSE: Set an accelerator on this frame
	// PARAMS: accelerator - pointer to accelerator, can be NULL.
	// NOTES: Accelerators are optional structures used to accelerate frame operations.
	// The caller remains responsible for eventual destruction of the accelerator.
	void SetAccelerator(crFrameAccelerator& accelerator);

	// PURPOSE: Get accelerator on this frame
	// RETURNS: pointer to accelerator, can be NULL.
	crFrameAccelerator* GetAccelerator() const;

#if CR_DEV
	// PURPOSE: Debug contents of frame with dump output object
	// PARAMS: output - dump output object, used to collect, format and direct output
	// filter - frame filter, controls which degrees of freedom are affected (NULL for all).
	void Dump(crDumpOutput& output, crFrameFilter* filter=NULL) const;

	// PURPOSE: Debug contents of frame (dump all to standard out)
	void Dump() const;
#endif // CR_DEV

	// PURPOSE: LEGACY SUPPORT - WILL BE DEPRECATED SOON
	// CONVERT TO crFrameDataInitializer -> crFrameData -> crFrame
	void Init(const crFrame &src, bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: LEGACY SUPPORT - WILL BE DEPRECATED SOON
	// CONVERT TO crFrameDataInitializer -> crFrameData -> crFrame
	void InitCreateBoneAndMoverDofs(const crSkeletonData& skelData, bool createMoverDofs=true, u16 moverId=0, bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: LEGACY SUPPORT - WILL BE DEPRECATED SOON
	// CONVERT TO crFrameDataInitializer -> crFrameData -> crFrame
	void InitCreateRootBoneDofs(bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: LEGACY SUPPORT - WILL BE DEPRECATED SOON
	// CONVERT TO crFrameDataInitializer -> crFrameData -> crFrame
	void InitCreateMoverDofs(u16 moverId=0, bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: LEGACY SUPPORT - WILL BE DEPRECATED SOON
	// CONVERT TO crFrameDataInitializer -> crFrameData -> crFrame
	void InitCreateDofs(int numDofs, u8 tracks[], u16 ids[], u8 types[], bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: LEGACY SUPPORT - WILL BE DEPRECATED SOON
	// CONVERT TO crFrameDataInitializer -> crFrameData -> crFrame
	void InitCreateAnimationDofs(const crAnimation& anim, bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: DEPRECATED - CONVERT TO Pose(crSkeleton&, bool NORMALIZE, crFrameFilter*) with NORMALIZE=false
	DEPRECATED void Pose(crSkeleton& skel, crFrameFilter* filter) const;

	// PURPOSE: DEPRECATED - CONVERT TO Pose(crSkeleton&, bool NORMALIZE, crFrameFilter*) with NORMALIZE=false
	DEPRECATED void InversePose(const crSkeleton& skel, crFrameFilter* filter);

	// PURPOSE: LEGACY - calculate mover degrees of freedom as delta
	bool CalcMoverDelta(const crAnimation& anim, float time, float delta, float rangeStart, float rangeEnd);
	bool CalcMoverDelta(const crAnimation& anim, float time, float delta);

	// PURPOSE: Temporary structure to represent degree of freedom value
	// These structures are not actually stored, they are constructed on stack by certain calls
	// to provide convenient interface to values and invalid flags
	class Dof
	{
	public:

		// PURPOSE: Initializing constructor
		Dof(crFrame& frame, u16 index);

		// PURPOSE: Initializing constructor (const)
		Dof(const crFrame& frame, u16 index);

		// PURPOSE: Initializing constructor, with offset hint
		Dof(crFrame& frame, u16 index, u16 offset);

		// PURPOSE: Initializing constructor, with offset hint (const)
		Dof(const crFrame& frame, u16 index, u16 offset);

		// PURPOSE: Safe get operation (fails if value is invalid)
		// PARAMS: template value reference
		// RETURNS: true - get succeeded, false - value was invalid, get failed
		// NOTES: If value is invalid, get operation fails, reference unmodified
		template<typename _T>
		bool Get(_T&) const;

		// PURPOSE: Safe set operation (marks value valid on set)
		// PARAMS: const template value reference
		// NOTES: Marks value as valid after set
		template<typename _T>
		void Set(const _T&);

		// PURPOSE: Unsafe get operation (const)
		// RETURNS: const template value reference
		// NOTES: Warning, if value is invalid, call still returns (and may reference junk/old value).
		// Only use in situations where optimal performance required,
		// and validity of degree of freedom has already been established.
		template<typename _T>
		const _T& GetUnsafe() const;

		// PURPOSE: Unsafe get operation (non-const)
		// RETURNS: non-const template value reference
		// NOTES: Warning, if value is invalid, call still returns (and may reference junk/old value).
		// Only use in situations where optimal performance required,
		// and validity of degree of freedom has already been established.
		template<typename _T>
		_T& GetUnsafe();

		// PURPOSE: Check if degree of freedom is invalid
		// RETURNS: true - degree of freedom is invalid, false - degree of freedom is valid
		bool IsInvalid() const;

		// PURPOSE: Invalidate (or validate) degree of freedom
		// PARAMS: invalid - set or clear invalid status of degree of freedom
		void SetInvalid(bool invalid);

	private:

		// PURPOSE: Default constructor (restricted, only available to trusted users)
		Dof();

		// PURPOSE: Initializer (restricted, only available to trusted users)
		void Init(crFrame& frame, u16 index);		

		// PURPOSE: Initializer (restricted, only available to trusted users)
		void Init(const crFrame& frame, u16 index);		

	private:

		crFrame* m_Frame;
		u16 m_Offset;
		u16 m_Index;
	};


	// PURPOSE: Provides fast low level access to degrees of freedom with a frame
	// Use in high performance situations only, use normal interface on frame class
	// for regular access to degrees of freedom
	class Accessor
	{
	public:

		// PURPOSE: Default constructor
		Accessor(u8 track, u16 id);

		// PURPOSE: Read degree of freedom, return default value if dof invalid or missing
		Vec::Vector_4V_Out Read(const crFrame& frame, Vec::Vector_4V_In defaultVal);

		// PURPOSE: Write degree of freedom
		void Write(crFrame& frame, Vec::Vector_4V_In val);

	private:

		u32 m_Signature;
		u16 m_Offset;
		u16 m_Id;
		u8 m_Track;
	};

	// PURPOSE: Get internal buffer
	// RETURNS: buffer pointer
	u8* GetBuffer();
	const u8* GetBuffer() const;

	// PURPOSE: Get buffer size
	// RETURNS: buffer size, in bytes
	u32 GetBufferSize() const;

	// PURPOSE: Get buffer owner
	// RETURNS: true - frame owns buffer (is responsible for destruction), false - buffer externally owned
	bool IsBufferOwner() const;


protected:

	// PURPOSE: LEGACY - calculate mover degrees of freedom as delta, return as situation
	bool CalcMoverDeltaInternal(const crAnimation& anim, float time, float delta, float rangeStart, float rangeEnd, TransformV_InOut outSit);

	// PURPOSE: LEGACY - calculate mover degrees of freedom as delta (no loop checks etc), return as situation
	bool CalcMoverDeltaInternalUnsafe(const crAnimation& anim, float time, float delta, TransformV_InOut outSit);

	// PURPOSE: LEGACY - calculate mover degrees of freedom, non delta, return as situation
	bool CalcMoverInternalUnsafe(const crAnimation& anim, float time, TransformV_InOut outSit);

	// PURPOSE: Composite animation but only for the mover
	// RETURNS: true - if it succeed
	bool CompositeMoverOnly(const crAnimation& anim, float time);

	// PURPOSE: Set (or clear) frame data
	void SetFrameData(const crFrameData*);

	// PURPOSE: Set all degrees of freedom from another frame (quickly, identical, no filter support etc)
	void FastSet(const crFrame&);

	// PURPOSE: Invalidate all degrees of freedom (quickly, no filter support etc)
	void FastInvalidate();

	// PURPOSE: Is degree of freedom invalid (by index)
	bool FastIsDofInvalid(u32 idx) const;

	// PURPOSE: Set degree of freedom invalid (by index)
	void FastSetDofInvalid(u32 idx, bool invalid);

	u8* m_Buffer;
	const crFrameData* m_FrameData;
	crFrameAccelerator* m_Accelerator;

	mutable u32 m_RefCount;
	u32 m_Signature;
	u16 m_BufferSize;
	u16 m_BufferOwner;
	u8 m_Padding[8];

	template<typename T> friend class crFrameIterator;
	friend class crFrameBufferFrameData;
} ;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Creates a frame structure, with preallocated buffer
// for a fixed number of degrees of freedom
// NOTES: Useful for allocating frame temporarily on the stack,
// when the number of degrees of freedom is known at compile time
// Warning: Assumes the worst case, of all degrees of freedom being 16 bytes.
// Much more efficient to use a normal crFrame, and Alloca to passing a buffer
// of size returned from crFrameData::GetFrameBufferSize for most cases
template<u16 numDofs>
class crFrameFixedDofs : public crFrame
{
public:

	// PURPOSE: Initializing constructor
	crFrameFixedDofs(const crFrameData&);

private:
	Vec::Vector_4V m_FixedBuffer[numDofs+8];
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Creates a frame structure, with preallocated buffer
// for a single degree of freedom
// NOTES: Useful for allocating frame temporarily on the stack
// when it is known only one degree of freedom is needed
// Warning: Assumes the worst case, that the degree of freedom is 16 bytes
class crFrameSingleDof : public crFrameFixedDofs<1>
{
public:

	// PURPOSE: Initializing constructor
	crFrameSingleDof(const crFrameData&);

	// PURPOSE: Get degree of freedom value
	// RETURNS: true - if value successfully retrieved
	template<typename _T>
	bool GetValue(_T& outVal) const;

	// PURPOSE: Set degree of freedom value
	// RETURNS: true - if value successfully set
	template<typename _T>
	bool SetValue(const _T& val);

	// PURPOSE: Invalidate degree of freedom
	void Invalidate();

	// PURPOSE: Is the degree of freedom value
	bool Valid() const;
};

////////////////////////////////////////////////////////////////////////////////

inline void crFrame::AddRef() const
{
	sysInterlockedIncrement(&m_RefCount);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrame::Release() const
{
	u32 refCount = sysInterlockedDecrement(&m_RefCount);
	if(!refCount)
	{
		delete this;
		return 0;
	}
	else
	{
		AssertMsg(refCount < 10000,"Probable double-free of crFrame");	// if this fails, you probably double-freed a crFrame.
		return refCount;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrame::GetRef() const
{
	return m_RefCount;
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrameData* crFrame::GetFrameData() const
{
	return m_FrameData;
}

////////////////////////////////////////////////////////////////////////////////

inline int crFrame::GetNumDofs() const
{
	if(m_FrameData)
	{
		Assert(m_FrameData->GetSignature() == m_Signature);
		
		return m_FrameData->GetNumDofs();
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::InvalidateDof(u8 track, u16 id)
{
	if(m_FrameData)
	{
		Assert(m_FrameData->GetSignature() == m_Signature);

		u32 idx;
		if(m_FrameData->FindDofIndex(track, id, idx))
		{
			FastSetDofInvalid(idx, true);
			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::HasDof(u8 track, u16 id) const
{
	if(m_FrameData)
	{
		Assert(m_FrameData->GetSignature() == m_Signature);

		return m_FrameData->FindDof(track, id) != NULL;	
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::HasDofValid(u8 track, u16 id) const
{
	if(m_FrameData)
	{
		Assert(m_FrameData->GetSignature() == m_Signature);

		u32 idx;
		if(m_FrameData->FindDofIndex(track, id, idx))
		{
			return !FastIsDofInvalid(idx);
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::GetBoneSituation(u16 boneId, TransformV_InOut outTransform, bool tolerateMissingDof) const
{
	return GetSituation(kTrackBoneTranslation, kTrackBoneRotation, boneId, outTransform, tolerateMissingDof);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::SetBoneSituation(u16 boneId, TransformV_In transform, bool tolerateMissingDof)
{
	return SetSituation(kTrackBoneTranslation, kTrackBoneRotation, boneId, transform, tolerateMissingDof);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::GetBoneMatrix(u16 boneId, Mat34V_InOut outMtx, bool tolerateMissingDof) const
{
	return GetMatrix(kTrackBoneTranslation, kTrackBoneRotation, boneId, outMtx, tolerateMissingDof);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::SetBoneMatrix(u16 boneId, Mat34V_In mtx, bool tolerateMissingDof)
{
	return SetMatrix(kTrackBoneTranslation, kTrackBoneRotation, boneId, mtx, tolerateMissingDof);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::GetBoneTranslation(u16 boneId, Vec3V_InOut outVector3) const
{
	return GetVector3(kTrackBoneTranslation, boneId, outVector3);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::SetBoneTranslation(u16 boneId, Vec3V_In vector3)
{
	return SetVector3(kTrackBoneTranslation, boneId, vector3);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::GetBoneRotation(u16 boneId, QuatV_InOut outQuaternion) const
{
	return GetQuaternion(kTrackBoneRotation, boneId, outQuaternion);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::SetBoneRotation(u16 boneId, QuatV_In quaternion)
{
	return SetQuaternion(kTrackBoneRotation, boneId, quaternion);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::GetBoneMatrix3x3(u16 boneId, Mat34V_InOut outMtx) const
{
	return GetMatrix3x3(kTrackBoneRotation, boneId, outMtx);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::SetBoneMatrix3x3(u16 boneId, Mat34V_In mtx)
{
	return SetMatrix3x3(kTrackBoneRotation, boneId, mtx);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::GetMoverSituation(TransformV_InOut outTransform) const
{
	return GetMoverSituation(0, outTransform);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::SetMoverSituation(TransformV_In transform)
{
	return SetMoverSituation(0, transform);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::GetMoverSituation(u16 moverId, TransformV_InOut outTransform) const
{
	outTransform = TransformV(V_IDENTITY);
	return GetSituation(kTrackMoverTranslation, kTrackMoverRotation, moverId, outTransform, true);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::SetMoverSituation(u16 moverId, TransformV_In transform)
{
	return SetSituation(kTrackMoverTranslation, kTrackMoverRotation, moverId, transform, true);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::GetMoverMatrix(Mat34V_InOut outMtx) const
{
	return GetMoverMatrix(0, outMtx);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::SetMoverMatrix(Mat34V_In mtx)
{
	return SetMoverMatrix(0, mtx);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::GetMoverMatrix(u16 moverId, Mat34V_InOut outMtx) const
{
	outMtx = Mat34V(V_IDENTITY);
	return GetMatrix(kTrackMoverTranslation, kTrackMoverRotation, moverId, outMtx, true);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::SetMoverMatrix(u16 moverId, Mat34V_In mtx)
{
	return SetMatrix(kTrackMoverTranslation, kTrackMoverRotation, moverId, mtx, true);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline bool crFrame::GetValue<Vec3V>(u8 track, u16 id, Vec3V& outVal) const
{
	return GetVector3(track, id, outVal);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline bool crFrame::GetValue<QuatV>(u8 track, u16 id, QuatV& outVal) const
{
	return GetQuaternion(track, id, outVal);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline bool crFrame::GetValue<float>(u8 track, u16 id, float& outVal) const
{
	return GetFloat(track, id, outVal);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline bool crFrame::SetValue<Vec3V>(u8 track, u16 id, const Vec3V& val)
{
	return SetVector3(track, id, val);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline bool crFrame::SetValue<QuatV>(u8 track, u16 id, const QuatV& val)
{
	return SetQuaternion(track, id, val);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline bool crFrame::SetValue<float>(u8 track, u16 id, const float& val)
{
	return SetFloat(track, id, val);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrame::GetSignature() const
{
	return m_Signature;
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrame::SetAccelerator(crFrameAccelerator& accelerator)
{
	m_Accelerator = &accelerator;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameAccelerator* crFrame::GetAccelerator() const
{
	return m_Accelerator;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame::Dof::Dof(crFrame& frame, u16 index)
: m_Frame(&frame)
, m_Index(index)
{
	Assert(m_Frame->m_FrameData);
	Assert(m_Frame->m_FrameData->GetSignature() == m_Frame->m_Signature);

	m_Offset = m_Frame->m_FrameData->GetDof(m_Index).m_Offset;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame::Dof::Dof(const crFrame& frame, u16 index)
: m_Frame(const_cast<crFrame*>(&frame))
, m_Index(index)
{
	Assert(m_Frame->m_FrameData);
	Assert(m_Frame->m_FrameData->GetSignature() == m_Frame->m_Signature);

	m_Offset = m_Frame->m_FrameData->GetDof(m_Index).m_Offset;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame::Dof::Dof(crFrame& frame, u16 index, u16 offset)
: m_Frame(&frame)
, m_Offset(offset)
, m_Index(index)
{
	Assert(m_Frame->m_FrameData);
	Assert(m_Frame->m_FrameData->GetSignature() == m_Frame->m_Signature);
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame::Dof::Dof(const crFrame& frame, u16 index, u16 offset)
: m_Frame(const_cast<crFrame*>(&frame))
, m_Offset(offset)
, m_Index(index)
{
	Assert(m_Frame->m_FrameData);
	Assert(m_Frame->m_FrameData->GetSignature() == m_Frame->m_Signature);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline bool crFrame::Dof::Get(_T& val) const
{
	Assert(m_Frame);
	if(!IsInvalid())
	{
		val = *reinterpret_cast<const _T*>(m_Frame->m_Buffer + m_Offset);
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline void crFrame::Dof::Set(const _T& val)
{
	Assert(m_Frame);
	*reinterpret_cast<_T*>(m_Frame->m_Buffer + m_Offset) = val;
	SetInvalid(false);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline const _T& crFrame::Dof::GetUnsafe() const
{
	Assert(m_Frame);
	return *reinterpret_cast<const _T*>(m_Frame->m_Buffer + m_Offset);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline _T& crFrame::Dof::GetUnsafe()
{
	Assert(m_Frame);
	return *reinterpret_cast<_T*>(m_Frame->m_Buffer + m_Offset);
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame::Dof::Dof()
: m_Frame(NULL)
, m_Offset(0)
, m_Index(0)
{
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrame::Dof::Init(crFrame& frame, u16 index)
{
	Assert(!m_Frame);
	m_Frame = &frame;
	m_Index = index;

	Assert(m_Frame->m_FrameData);
	Assert(m_Frame->m_FrameData->GetSignature() == m_Frame->m_Signature);

	m_Offset = m_Frame->m_FrameData->GetDof(index).m_Offset;
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrame::Dof::Init(const crFrame& frame, u16 index)
{
	Assert(!m_Frame);
	m_Frame = const_cast<crFrame*>(&frame);
	m_Index = index;

	Assert(m_Frame->m_FrameData);
	Assert(m_Frame->m_FrameData->GetSignature() == m_Frame->m_Signature);

	m_Offset = m_Frame->m_FrameData->GetDof(index).m_Offset;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::Dof::IsInvalid() const
{
	return m_Frame->FastIsDofInvalid(m_Index);
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrame::Dof::SetInvalid(bool invalid)
{
	m_Frame->FastSetDofInvalid(m_Index, invalid);
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame::Accessor::Accessor(u8 track, u16 id)
: m_Signature(0)
, m_Offset(USHRT_MAX)
, m_Track(track)
, m_Id(id)
{
}

////////////////////////////////////////////////////////////////////////////////

inline Vec::Vector_4V_Out crFrame::Accessor::Read(const crFrame& frame, Vec::Vector_4V_In defaultVal)
{
	if(Unlikely(m_Signature != frame.m_Signature))
	{
		const crFrameData::Dof* dof = frame.m_FrameData->FindDof(m_Track, m_Id);
		m_Offset = dof ? dof->m_Offset : frame.m_FrameData->GetReadOnlyOffset();
		m_Signature = frame.GetSignature();
	}
	FastAssert(m_Offset != USHRT_MAX && m_Offset != frame.m_FrameData->GetWriteOnlyOffset());
	Vec::Vector_4V val = *reinterpret_cast<const Vec::Vector_4V*>(frame.m_Buffer + m_Offset);
	return Vec::V4SelectFT(Vec::V4IsEqualIntV(val, Vec::V4VConstant(V_MASKXYZW)), val, defaultVal);
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrame::Accessor::Write(crFrame& frame, Vec::Vector_4V_In val)
{
	if(Unlikely(m_Signature != frame.m_Signature))
	{
		const crFrameData::Dof* dof = frame.m_FrameData->FindDof(m_Track, m_Id);
		m_Offset = dof ? dof->m_Offset : frame.m_FrameData->GetWriteOnlyOffset();
		m_Signature = frame.GetSignature();
	}
	FastAssert(m_Offset != USHRT_MAX && m_Offset != frame.m_FrameData->GetReadOnlyOffset());
	*reinterpret_cast<Vec::Vector_4V*>(frame.m_Buffer + m_Offset) = val;
}

////////////////////////////////////////////////////////////////////////////////

inline u8* crFrame::GetBuffer()
{
	return m_Buffer;
}
////////////////////////////////////////////////////////////////////////////////

inline const u8* crFrame::GetBuffer() const
{
	return m_Buffer;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrame::GetBufferSize() const
{
	return m_BufferSize;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::IsBufferOwner() const
{
	return m_BufferOwner != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrame::SetFrameData(const crFrameData* frameData)
{
	if(frameData)
	{
		frameData->AddRef();
		m_Signature = frameData->GetSignature();
	}
	else
	{
		m_Signature = 0;
	}
	
	if(m_FrameData)
	{
		m_FrameData->Release();
	}

	m_FrameData = frameData;
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrame::FastSet(const crFrame& src)
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);
	Assert(m_FrameData->GetFrameBufferSize() <= m_BufferSize);

	Assert(src.m_FrameData);
	Assert(src.m_FrameData->GetSignature() == src.m_Signature);
	Assert(src.m_FrameData->GetFrameBufferSize() <= src.m_BufferSize);
	
	Assert(src.m_Signature == m_Signature);
	Assert(src.m_FrameData->GetFrameBufferSize() == m_FrameData->GetFrameBufferSize());
			
	sysMemCpy(m_Buffer, src.m_Buffer, m_FrameData->GetFrameBufferSize());
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrame::FastInvalidate()
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);
	sysMemSet(m_Buffer, 0xff, m_BufferSize);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrame::FastIsDofInvalid(u32 idx) const
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);
	Assert(idx < u32(GetNumDofs()));
	u32 offset = m_FrameData->m_Dofs[idx].m_Offset;
	return *reinterpret_cast<const u32*>(m_Buffer + offset) == UINT_MAX;
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrame::FastSetDofInvalid(u32 idx, bool invalid)
{
	if(invalid)
	{
		u32 type = m_FrameData->m_Dofs[idx].m_Type;
		u32 offset = m_FrameData->m_Dofs[idx].m_Offset;
		if(type <= kFormatTypeQuaternion)
		{
			*reinterpret_cast<Vec::Vector_4V*>(m_Buffer+offset) = Vec::V4VConstant(V_MASKXYZW);
		}
		else
		{
			*reinterpret_cast<u32*>(m_Buffer+offset) = UINT_MAX;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

template<u16 numDofs>
inline crFrameFixedDofs<numDofs>::crFrameFixedDofs(const crFrameData& frameData)
: crFrame(frameData, reinterpret_cast<u8*>(m_FixedBuffer), sizeof(m_FixedBuffer), false)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameSingleDof::crFrameSingleDof(const crFrameData& frameData)
: crFrameFixedDofs<1>(frameData)
{
	Assert(frameData.GetNumDofs() == 1);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline bool crFrameSingleDof::GetValue(_T& outVal) const
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);
	Assert(m_FrameData->GetNumDofs() == 1);
	
	const crFrameData::Dof& dof0 = m_FrameData->m_Dofs[0];
	return crFrame::GetValue<_T>(dof0.m_Track, dof0.m_Id, outVal);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline bool crFrameSingleDof::SetValue(const _T& val)
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);
	Assert(m_FrameData->GetNumDofs() == 1);

	const crFrameData::Dof& dof0 = m_FrameData->m_Dofs[0];
	return crFrame::SetValue<_T>(dof0.m_Track, dof0.m_Id, val);
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrameSingleDof::Invalidate()
{
	return FastInvalidate();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrameSingleDof::Valid() const
{
	return !FastIsDofInvalid(0);
}

////////////////////////////////////////////////////////////////////////////////

}	// namespace rage

#endif  // CRANIMATION_FRAME_H
