//
// cranimation/animtrack.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "animtrack.h"

#if CR_DEV
#include "animarchive.h"
#include "animation.h"
#include "animchannel.h"
#include "animchunk.h"
#include "channelbasic.h"
#include "channelquantize.h"
#include "framedata.h"

#include "crskeleton/skeletondata.h"
#include "data/resource.h"
#include "data/resourcehelpers.h"
#include "system/nelem.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crAnimTrack::crAnimTrack()
: m_Track(0xff)
, m_Type(0x0f)
, m_Id(0xffff)
, m_FramesPerChunk(crAnimation::sm_DefaultFramesPerChunk)
, m_Flags(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimTrack::crAnimTrack(u8 track, u16 id)
: m_Track(track)
, m_Type(0x0f)
, m_Id(id)
, m_FramesPerChunk(crAnimation::sm_DefaultFramesPerChunk)
, m_Flags(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimTrack::~crAnimTrack()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::Shutdown()
{
	Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::Serialize(crArchive& a)
{
	a<<=m_Track;
	a<<=m_Type;
	a<<=m_Id;
	a<<=m_Flags;

	u16 numChunks = u16(m_Chunks.GetCount());
	a<<=numChunks;

	if(a.IsLoading())
	{
		m_Chunks.Resize(numChunks);
		for(u16 i=0; i<numChunks; ++i)
		{
			m_Chunks[i] = rage_new crAnimChunk;
		}
	}

	for(u16 i=0; i<numChunks; ++i)
	{
		m_Chunks[i]->Serialize(a);
	}

	a<<=m_FramesPerChunk;
}

////////////////////////////////////////////////////////////////////////////////

crAnimTrack* crAnimTrack::Clone() const
{
	crAnimTrack* newTrack = rage_new crAnimTrack;
	newTrack->m_Track = m_Track;
	newTrack->m_Type = m_Type;
	newTrack->m_Id = m_Id;
	newTrack->m_FramesPerChunk = m_FramesPerChunk;
	newTrack->m_Flags = m_Flags;

	newTrack->m_Chunks.Resize(m_Chunks.GetCount());
	for(int i=0; i<m_Chunks.GetCount(); ++i)
	{
		newTrack->m_Chunks[i] = m_Chunks[i]->Clone();
	}

	return newTrack;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::SetId(u16 id)
{
	m_Id = id;

	for(int i=0; i<m_Chunks.GetCount(); ++i)
	{
		m_Chunks[i]->SetId(id);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::CreateVector3(atArray<Vec3V>& vectors, const crAnimTolerance& tolerance, u32 framesPerChunk)
{
	Reset();

	m_Type = kFormatTypeVector3;
	Assign(m_FramesPerChunk, framesPerChunk);

	u32 numFrames = vectors.GetCount();
	u32 numChunks = CalcNumChunks(numFrames);
	m_Chunks.Resize(numChunks);

	for(u32 i=0; i<numChunks; ++i)
	{
		u32 curFrame = i*framesPerChunk;
		u32 numFramesRemaining = numFrames-curFrame;
		u32 numFramesRemainingInChunk = Min(numFramesRemaining, framesPerChunk+1);

		m_Chunks[i] = rage_new crAnimChunk;

		crAnimChunk::ToleranceHelper toleranceHelper(tolerance, m_Track, m_Id, m_Type);
		m_Chunks[i]->CreateVector3(m_Track, m_Id, &(vectors[curFrame]), numFramesRemainingInChunk, toleranceHelper);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::CreateQuaternion(atArray<QuatV>& quaternions, const crAnimTolerance& tolerance, u32 framesPerChunk)
{
	Reset();

	m_Type = kFormatTypeQuaternion;
	Assign(m_FramesPerChunk, framesPerChunk);

	u32 numFrames = quaternions.GetCount();
	u32 numChunks = CalcNumChunks(numFrames);
	m_Chunks.Resize(numChunks);

	for(u32 i=0; i<numChunks; ++i)
	{
		u32 curFrame = i*framesPerChunk;
		u32 numFramesRemaining = numFrames-curFrame;
		u32 numFramesRemainingInChunk = Min(numFramesRemaining, framesPerChunk+1);

		m_Chunks[i] = rage_new crAnimChunk;
		crAnimChunk::ToleranceHelper toleranceHelper(tolerance, m_Track, m_Id, m_Type);
		m_Chunks[i]->CreateQuaternion(m_Track, m_Id, &(quaternions[curFrame]), numFramesRemainingInChunk, toleranceHelper);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::CreateFloat(atArray<float>& floats, const crAnimTolerance& tolerance, u32 framesPerChunk)
{
	Reset();

	m_Type = kFormatTypeFloat;
	Assign(m_FramesPerChunk, framesPerChunk);

	u32 numFrames = floats.GetCount();
	u32 numChunks = CalcNumChunks(numFrames);
	m_Chunks.Resize(numChunks);

	for(u32 i=0; i<numChunks; ++i)
	{
		u32 curFrame = i*framesPerChunk;
		u32 numFramesRemaining = numFrames-curFrame;
		u32 numFramesRemainingInChunk = Min(numFramesRemaining, framesPerChunk+1);

		m_Chunks[i] = rage_new crAnimChunk;
		crAnimChunk::ToleranceHelper toleranceHelper(tolerance, m_Track, m_Id, m_Type);
		m_Chunks[i]->CreateFloat(m_Track, m_Id, &(floats[curFrame]), numFramesRemainingInChunk, toleranceHelper);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::CopyTrack(const crAnimTrack& other)
{
	Reset();

	m_Type = other.m_Type;
	m_FramesPerChunk = other.m_FramesPerChunk;
	m_Flags = other.m_Flags;

	m_Chunks.Resize(other.m_Chunks.GetCount());
	for(int i=0; i<m_Chunks.GetCount(); ++i)
	{
		m_Chunks[i] = other.m_Chunks[i]->Clone();
	}
}

////////////////////////////////////////////////////////////////////////////////

int crAnimTrack::ComputeSize() const
{
	int size = sizeof(crAnimTrack);
	for(int i=0; i<m_Chunks.GetCount(); i++)
	{
		size += m_Chunks[i]->ComputeSize() + sizeof(crAnimChunk*);
	}

	return size;
}

////////////////////////////////////////////////////////////////////////////////

// search for other known track names
const char* s_KnownTrackNames[] =
{
	"boneTranslation",
	"boneRotation",
	"boneScale",
	"moverTranslation",
	"moverRotation",
	"moverScale",
	"visibility",
	"blendShape",
	"animatedNormalMaps",
	"focalLength",
	"fStop",
	"focusDistance",
	"cameraTranslation",
	"cameraRotation",
	"cameraFOV",
	"cameraDOFStrength",
	"shaderSlideU",
	"shaderSlideV",
	"color",
	"lightIntensity",
	"lightFallOff",
	"lightExpFallOff",
	"lightConeAngle",
	"facialControl",
	"facialTranslation",
	"facialRotation",
	"facialTinting",
	"genericControl",
	"genericTranslation",
	"genericRotation",
	"cameraDOF",
	"facialScale",
	"genericScale",
	"cameraShallowDOF",
	"cameraSimpleDOF",
	"cameraMotionBlur",
	"lightDirection",
	"cameraDepthOfFieldNearOutOfFocusPlane",
	"cameraDepthOfFieldNearInFocusPlane",
	"cameraDepthOfFieldFarOutOfFocusPlane",
	"cameraDepthOfFieldFarInFocusPlane",
	"particleData",
	"viseme",
	"cameraCoC",
	"cameraFocus",
	"cameraNightCoC",
	"cameraLimit",
	"projectData"
};

////////////////////////////////////////////////////////////////////////////////

u8 s_KnownTrackIds[] =
{
	kTrackBoneTranslation,
	kTrackBoneRotation,
	kTrackBoneScale,
	kTrackMoverTranslation,
	kTrackMoverRotation,
	kTrackMoverScale,
	kTrackVisibility,
	kTrackBlendShape,
	kTrackAnimatedNormalMaps,
	kTrackCameraFocalLength,
	kTrackCameraFStop,
	kTrackCameraFocusDistance,
	kTrackCameraTranslation,
	kTrackCameraRotation,
	kTrackCameraFieldOfView,
	kTrackCameraDepthOfFieldStrength,
	kTrackShaderSlideU,
	kTrackShaderSlideV,
	kTrackColor,
	kTrackLightIntensity,
	kTrackLightFallOff,
	kTrackLightExpFallOff,
	kTrackLightConeAngle,
	kTrackFacialControl,
	kTrackFacialTranslation,
	kTrackFacialRotation,
	kTrackFacialTinting,
	kTrackGenericControl,
	kTrackGenericTranslation,
	kTrackGenericRotation,
	kTrackCameraDepthOfField,
	kTrackFacialScale,
	kTrackGenericScale,
	kTrackCameraShallowDepthOfField,
	kTrackCameraSimpleDepthOfField,
	kTrackCameraMotionBlur,
	kTrackLightDirection,
	kTrackCameraDepthOfFieldNearOutOfFocusPlane,
	kTrackCameraDepthOfFieldNearInFocusPlane,
	kTrackCameraDepthOfFieldFarOutOfFocusPlane,
	kTrackCameraDepthOfFieldFarInFocusPlane,
	kTrackParticleData,
	kTrackVisemes,
	kTrackCameraCoC,
	kTrackCameraFocus,
	kTrackCameraNightCoC,
	kTrackCameraLimit,
	kTrackFirstReservedForProjectUse
};

////////////////////////////////////////////////////////////////////////////////

CompileTimeAssert((sizeof(s_KnownTrackNames) / sizeof(s_KnownTrackNames[0])) == (sizeof(s_KnownTrackIds) / sizeof(s_KnownTrackIds[0])));

////////////////////////////////////////////////////////////////////////////////

bool crAnimTrack::ConvertTrackNameToIndex(const char* trackIndexName, u8& outTrackIndex)
{
	// TODO --- white space/underscore stripping from trackIndexName?

	int numTracks = (sizeof(s_KnownTrackNames) / sizeof(s_KnownTrackNames[0]));
	for(int i=0; i<numTracks; i++)
	{
		if(!strnicmp(trackIndexName, s_KnownTrackNames[i], strlen(s_KnownTrackNames[i])))
		{
			u8 offset = 0;
			const char* offsetString = trackIndexName + strlen(s_KnownTrackNames[i]);

			if(*offsetString != '\0')
			{
				offset = (u8)atoi(offsetString);
			}

			outTrackIndex = s_KnownTrackIds[i] + offset;
			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

const char* crAnimTrack::ConvertTrackIndexToName(u8 trackIndex)
{
	int numTracks = (sizeof(s_KnownTrackNames) / sizeof(s_KnownTrackNames[0]));
	for(int i=0; i<numTracks; i++)
	{
		if(s_KnownTrackIds[i] == trackIndex)
		{
			return s_KnownTrackNames[i];
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

u16 crAnimTrack::ConvertBoneNameToId(const char* boneIdName)
{
	return crSkeletonData::ConvertBoneNameToId(boneIdName);
}

////////////////////////////////////////////////////////////////////////////////

u16 crAnimTrack::ConvertMoverNameToId(const char* moverIdName)
{
	return ConvertBoneNameToId(moverIdName);
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::Reset()
{
	const int numChunks = m_Chunks.GetCount();
	for(int i=0; i<numChunks; i++)
	{
		delete m_Chunks[i];
		m_Chunks[i] = NULL;
	}
	m_Chunks.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::EvaluateVector3(float t, Vec3V_InOut outVector3) const
{
	FastAssert(GetType() == kFormatTypeVector3);
	FastAssert(m_Chunks.GetCount() > 0);

	int chunkIdx = int(t / float(m_FramesPerChunk));
	chunkIdx = Min(chunkIdx, m_Chunks.GetCount()-1);

	FastAssert(m_Chunks[chunkIdx]);
	m_Chunks[chunkIdx]->EvaluateVector3(t-float(m_FramesPerChunk*chunkIdx), outVector3);
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::EvaluateQuaternion(float t, QuatV_InOut outQuaternion) const
{
	FastAssert(GetType() == kFormatTypeQuaternion);
	FastAssert(m_Chunks.GetCount() > 0);

	int chunkIdx = int(t / float(m_FramesPerChunk));
	chunkIdx = Min(chunkIdx, m_Chunks.GetCount()-1);

	FastAssert(m_Chunks[chunkIdx]);
	m_Chunks[chunkIdx]->EvaluateQuaternion(t-float(m_FramesPerChunk*chunkIdx), outQuaternion);
}

////////////////////////////////////////////////////////////////////////////////

void crAnimTrack::EvaluateFloat(float t, float& outFloat) const
{
	FastAssert(GetType() == kFormatTypeFloat);
	FastAssert(m_Chunks.GetCount() > 0);

	int chunkIdx = int(t / float(m_FramesPerChunk));
	chunkIdx = Min(chunkIdx, m_Chunks.GetCount()-1);

	FastAssert(m_Chunks[chunkIdx]);
	m_Chunks[chunkIdx]->EvaluateFloat(t-float(m_FramesPerChunk*chunkIdx), outFloat);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
