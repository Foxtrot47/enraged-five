//
// cranimation/channellinear.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "channellinear.h"
#include "channelquantize.h"

#if CR_DEV

#include "animarchive.h"
#include "animation.h"
#include "animstream.h"
#include "animtolerance.h"

namespace rage
{

IMPLEMENT_ANIM_CHANNEL(crAnimChannelLinearFloat, crAnimChannel::AnimChannelTypeLinearFloat, crAnimTolerance::kCompressionCostMinimum, crAnimTolerance::kDecompressionCostVeryHigh);

///////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelLinearFloat::EvaluateFloat(float fTime, float& outFloat) const
{
	int iTime = int(floor(fTime));
	float fract = fTime - float(iTime);

	if(fract > (1.f - INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE))
	{
		iTime++;
	}
	else if(fract > INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE)
	{
		outFloat = Lerp(fract, ReconstructFloat(iTime), ReconstructFloat(iTime+1));
		return;
	}

	outFloat = ReconstructFloat(iTime);
}

///////////////////////////////////////////////////////////////////////////////////////

static u32 GetNumBits(u32 values)
{
	u32 numBits = 0;
	while(values > 0)
	{
		values >>= 1;
		numBits++;
	}
	return numBits;
}
///////////////////////////////////////////////////////////////////////////////////////

bool crAnimChannelLinearFloat::CreateFloat(float* floats, int frames, int skip, float tolerance)
{
	if(frames < 3)
	{
		return false;
	}

	atPackedArray quantas;
	if(!crAnimChannelQuantizeFloat::QuantizeFloats(floats, frames, skip, tolerance, quantas, m_Scale, m_Offset))
	{
		return false;
	}

	static const u32 kDefaultSegmentSize = 64;
	u32 N = kDefaultSegmentSize;

	// Compute error from prediction
	atArray<int> values;
	values.Reserve(frames);
	for(int i=0; i < frames; ++i)
	{
		if(i%N)
		{
			int prediction = quantas.GetElement(i-1);
			if(i%N >= 2)
			{
				int v0 = quantas.GetElement(i-2);
				int v1 = quantas.GetElement(i-1);
				prediction = 2*v1 - v0;
			}

			int val = quantas.GetElement(i);
			values.Grow() = val - prediction;
		}
	}

	u32 lowestAddress;
	if(!m_Values.InitAndCreate(values, lowestAddress))
	{
		return false;
	}

	// Compute number of bits for initials and addresses
	u32 numSegments = 0;
	u32 maxAddr = 0, maxInitial = 0;
	for(u32 i=0, addr=0; i < u32(frames); i++)
	{
		if(i%N)
		{
			m_Values.GetValue(addr);
		}
		else
		{
			maxAddr = Max(maxAddr, addr);
			maxInitial = Max(maxInitial, quantas.GetElement(i));
			numSegments++;
		}
	}

	if(numSegments > UCHAR_MAX)
	{
		return false;
	}

	u32 numBitAddr = GetNumBits(maxAddr);
	u32 numBitInitial = GetNumBits(maxInitial);
	if(numBitAddr > UCHAR_MAX || numBitInitial > UCHAR_MAX)
	{
		return false;
	}

	// Fill initial and addresses array
	atArray<u32> addresses, initials;
	addresses.Reserve(numSegments);
	initials.Reserve(numSegments);
	for(u32 i=0, j=0, addr=0; i < u32(frames); i++)
	{
		if(i%N)
		{
			m_Values.GetValue(addr);
		}
		else
		{
			if(numBitAddr > 0)
			{
				addresses.Append() = addr;
			}

			if(numBitInitial > 0)
			{
				initials.Append() = quantas.GetElement(i);
			}
			j++;
		}
	}
	m_Addresses.Init(numBitAddr, addresses);
	m_Initials.Init(numBitInitial, initials);
	m_SegmentSize = N;

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////

int crAnimChannelLinearFloat::ComputeSize(void) const
{
	return 4*sizeof(u32) + m_Initials.ComputeSize() + m_Addresses.ComputeSize() + m_Values.ComputeSize();
}

///////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelLinearFloat::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	if(a.GetVersion() >= 16)
	{
		a<<=m_SegmentSize;
	}
	else
	{
		m_SegmentSize = 32;
	}

	a<<=m_Scale;
	a<<=m_Offset;

	m_Addresses.Serialize(a);
	m_Initials.Serialize(a);
	m_Values.Serialize(a);
}

///////////////////////////////////////////////////////////////////////////////////////

float crAnimChannelLinearFloat::ReconstructFloat(u32 tq) const
{
	u32 N = m_SegmentSize;
	u32 valueIdx = tq % N;
	u32 segmentIdx = tq / N;
	u32 address = m_Addresses.GetElementSize() != 0 ? m_Addresses.GetElement(segmentIdx) : 0;
	s32 val = m_Initials.GetElementSize() != 0 ? m_Initials.GetElement(segmentIdx) : 0;

	s32 vel = 0;
	for(u32 j=0; j<valueIdx; ++j)
	{
		vel += m_Values.GetValue(address);
		val += vel;
	}

	return m_Offset + float(val)*m_Scale;
}

///////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
