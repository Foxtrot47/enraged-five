//
// cranimation/channelrle.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_CHANNELRLE_H
#define CRANIMATION_CHANNELRLE_H

#include "animchannel.h"

#if CR_DEV

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Run length encoded int channel compression
class crAnimChannelRleInt : public crAnimChannel
{
	friend class crAnimPacker;
public:

	// PURPOSE: Default constructor
	crAnimChannelRleInt();

	// PURPOSE: Resource constructor
	crAnimChannelRleInt(datResource&);

	// PURPOSE: Destructor
	virtual ~crAnimChannelRleInt();

	// PURPOSE: Type registration
	DECLARE_ANIM_CHANNEL(crAnimChannelRleInt);

	// PURPOSE: Evaluation, evaluate int for given time
	// PARAMS: time - time in internal frames
	// outInt - output integer
	virtual void EvaluateInt(float time, int& outInt) const;

	// PURPOSE: Create compressed channel from array of data
	// PARAMS: ints - integers to compress
	// frames - number of integers to compress
	// RETURNS: true - if compression successful, false - failed to compress
	virtual bool CreateInt(int* ints, int frames);

	// PURPOSE: Compute storage use
	// RETURNS: Total storage used by this compression technique, in bytes
	virtual int ComputeSize() const;

	// PURPOSE: Serialize channel into archive
	virtual void Serialize(crArchive&);

private:
	atArray<s32> m_Values;
	atPackedSequence m_Runs;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_CHANNELRLE_H
