//
// cranimation/animtolerance.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#include "animtolerance.h"

#if CR_DEV
#include "animation.h"
#include "animtrack.h"
#include "frame.h"

#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "string/stringhash.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

const u32 crAnimTolerance::sm_HashMaxAbsoluteError = ATSTRINGHASH("MaxAbsoluteError", 0x957229CD);
const u32 crAnimTolerance::sm_HashMaxDecompressionCost = ATSTRINGHASH("MaxDecompressionCost", 0xEBDC1F78);
const u32 crAnimTolerance::sm_HashMaxCompressionCost = ATSTRINGHASH("MaxCompressionCost", 0x1973120E);
const u32 crAnimTolerance::sm_HashMinFrequency = ATSTRINGHASH("MinFrequency", 0x61E396D2);

crAnimTolerance crAnimTolerance::sm_DefaultInstance;

////////////////////////////////////////////////////////////////////////////////

bool crAnimTolerance::GetProperty(u8 track, u16 id, u8 type, u32 hash, float& outFloat) const
{
	if(hash == sm_HashMaxAbsoluteError)
	{
		outFloat = GetMaxAbsoluteError(track, id, type);
		return true;
	}
	else if(hash == sm_HashMinFrequency)
	{
		outFloat = GetMinFrequency(track, id, type);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimTolerance::GetProperty(u8 track, u16 id, u8 type, u32 hash, int& outInt) const
{
	if(hash == sm_HashMaxAbsoluteError)
	{
		outInt = GetMaxDecompressionCost(track, id, type);
		return true;
	}
	else if(hash == sm_HashMinFrequency)
	{
		outInt = GetMaxCompressionCost(track, id, type);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

const float crAnimToleranceSimple::sm_DefaultMaxAbsoluteTranslationError = 0.0005f;
const float crAnimToleranceSimple::sm_DefaultMaxAbsoluteRotationError = 0.0005f;
const float crAnimToleranceSimple::sm_DefaultMaxAbsoluteScaleError = 0.0005f;
const float crAnimToleranceSimple::sm_DefaultMaxAbsoluteDefaultError = 0.001f;
const float crAnimToleranceSimple::sm_DefaultMinFrequency = CR_DEFAULT_FREQUENCY;

crAnimToleranceSimple crAnimToleranceSimple::sm_DefaultInstance;

////////////////////////////////////////////////////////////////////////////////

crAnimToleranceSimple::crAnimToleranceSimple(
	float maxTranslationError,
	float maxRotationError,
	float maxScaleError,
	float maxDefaultError,
	eDecompressionCost maxDecompressionCost,
	eCompressionCost maxCompressionCost,
	float minFrequency
	)
: m_MaxAbsoluteTranslationError(0.f)
, m_MaxAbsoluteRotationError(0.f)
, m_MaxAbsoluteScaleError(0.f)
, m_MaxAbsoluteDefaultError(0.f)
, m_MaxDecompressionCost(kDecompressionCostDefault)
, m_MaxCompressionCost(kCompressionCostDefault)
, m_MinFrequency(0.f)
{
	Init(maxTranslationError, maxRotationError, maxScaleError, maxDefaultError, maxDecompressionCost, maxCompressionCost, minFrequency);
}

////////////////////////////////////////////////////////////////////////////////

void crAnimToleranceSimple::Init(
	float maxTranslationError,
	float maxRotationError,
	float maxScaleError,
	float maxDefaultError,
	eDecompressionCost maxDecompressionCost,
	eCompressionCost maxCompressionCost,
	float minFrequency
	)
{
	Assert(minFrequency > 0.f);

	m_MaxAbsoluteTranslationError = maxTranslationError;
	m_MaxAbsoluteRotationError = maxRotationError;
	m_MaxAbsoluteScaleError = maxScaleError;
	m_MaxAbsoluteDefaultError = maxDefaultError;
	m_MaxDecompressionCost = maxDecompressionCost;
	m_MaxCompressionCost = maxCompressionCost;
	m_MinFrequency = minFrequency;
}

////////////////////////////////////////////////////////////////////////////////

float crAnimToleranceSimple::GetMaxAbsoluteError(u8 track, u16 id, u8) const
{
	switch(track)
	{
	case kTrackBoneTranslation:
	case kTrackMoverTranslation:
		{
			// check for root bone
			if(id == 0)
			{
				// effectively no lossy compression on the root bone translate (except floating point noise)!
				return SMALL_FLOAT;
			}
			return m_MaxAbsoluteTranslationError;
		}

	case kTrackBoneRotation:
	case kTrackMoverRotation:
		{
			// check for root bone
			if(id == 0)
			{
				// effectively no lossy compression on the root bone rotate (except floating point noise)!
				return SMALL_FLOAT;
			}
			return m_MaxAbsoluteRotationError;
		}

	case kTrackBoneScale:
	case kTrackMoverScale:
		return m_MaxAbsoluteScaleError;
	}

	return m_MaxAbsoluteDefaultError;
}

////////////////////////////////////////////////////////////////////////////////

crAnimTolerance::eDecompressionCost crAnimToleranceSimple::GetMaxDecompressionCost(u8, u16, u8) const
{
	return m_MaxDecompressionCost;
}

////////////////////////////////////////////////////////////////////////////////

crAnimTolerance::eCompressionCost crAnimToleranceSimple::GetMaxCompressionCost(u8, u16, u8) const
{
	return m_MaxCompressionCost;
}

////////////////////////////////////////////////////////////////////////////////

float crAnimToleranceSimple::GetMinFrequency(u8, u16, u8) const
{
	return m_MinFrequency;
}

////////////////////////////////////////////////////////////////////////////////

float crAnimToleranceComplex::GetMaxAbsoluteError(u8 track, u16 id, u8 type) const
{
	const Rule::Property* rule = FindRule(track, id, sm_HashMaxAbsoluteError);
	if(rule)
	{
		return rule->m_Float;
	}

	return crAnimToleranceSimple::GetMaxAbsoluteError(track, id, type);
}

////////////////////////////////////////////////////////////////////////////////

crAnimTolerance::eDecompressionCost crAnimToleranceComplex::GetMaxDecompressionCost(u8 track, u16 id, u8 type) const
{
	const Rule::Property* rule = FindRule(track, id, sm_HashMaxDecompressionCost);
	if(rule)
	{
		return crAnimTolerance::eDecompressionCost(rule->m_Int);
	}

	return crAnimToleranceSimple::GetMaxDecompressionCost(track, id, type);
}

////////////////////////////////////////////////////////////////////////////////

crAnimTolerance::eCompressionCost crAnimToleranceComplex::GetMaxCompressionCost(u8 track, u16 id, u8 type) const
{
	const Rule::Property* rule = FindRule(track, id, sm_HashMaxCompressionCost);
	if(rule)
	{
		return crAnimTolerance::eCompressionCost(rule->m_Int);
	}

	return crAnimToleranceSimple::GetMaxCompressionCost(track, id, type);
}

////////////////////////////////////////////////////////////////////////////////

float crAnimToleranceComplex::GetMinFrequency(u8 track, u16 id, u8 type) const
{
	const Rule::Property* rule = FindRule(track, id, sm_HashMinFrequency);
	if(rule)
	{
		return rule->m_Float;
	}

	return crAnimToleranceSimple::GetMinFrequency(track, id, type);
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimToleranceComplex::GetProperty(u8 track, u16 id, u8 type, u32 hash, float& outFloat) const
{
	const Rule::Property* rule = FindRule(track, id, hash);
	if(rule)
	{
		outFloat = rule->m_Float;
		return true;
	}

	return crAnimToleranceSimple::GetProperty(track, id, type, hash, outFloat);
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimToleranceComplex::GetProperty(u8 track, u16 id, u8 type, u32 hash, int& outInt) const
{
	const Rule::Property* rule = FindRule(track, id, hash);
	if(rule)
	{
		outInt = rule->m_Int;
		return true;
	}

	return crAnimToleranceSimple::GetProperty(track, id, type, hash, outInt);
}

////////////////////////////////////////////////////////////////////////////////

u32 crAnimToleranceComplex::AddRule(u8 track, bool trackWildcard, u16 id, bool idWildcard)
{
	Rule& rule = m_Rules.Grow();

	if(!trackWildcard)
	{
		rule.m_Track = track;
		rule.m_ValidFlags |= Rule::kTrackValid;
	}

	if(!idWildcard)
	{
		rule.m_Id = id;
		rule.m_ValidFlags |= Rule::kIdValid;
	}

	return m_Rules.GetCount()-1;
}

////////////////////////////////////////////////////////////////////////////////

u32 crAnimToleranceComplex::AddRule(u8 track, bool trackWildcard, u16 id, bool idWildcard, float maxAbsoluteError)
{
	u32 ruleIdx = AddRule(track, trackWildcard, id, idWildcard);
	Rule& rule = m_Rules[ruleIdx];

	Rule::Property& prop = rule.m_Properties.Grow();
	prop.m_Hash = sm_HashMaxAbsoluteError;
	prop.m_Float = maxAbsoluteError;

	return m_Rules.GetCount()-1;
}

////////////////////////////////////////////////////////////////////////////////

u32 crAnimToleranceComplex::AddRule(u8 track, bool trackWildcard, u16 id, bool idWildcard, float maxAbsoluteError, float minFrequency)
{
	u32 ruleIdx = AddRule(track, trackWildcard, id, idWildcard, maxAbsoluteError);
	Rule& rule = m_Rules[ruleIdx];

	Assert(minFrequency > 0.f);
	if(minFrequency)
	{
		Rule::Property& prop = rule.m_Properties.Grow();
		prop.m_Hash = sm_HashMinFrequency;
		prop.m_Float = minFrequency;
	}

	return ruleIdx;
}

////////////////////////////////////////////////////////////////////////////////

u32 crAnimToleranceComplex::AddRule(u8 track, bool trackWildcard, u16 id, bool idWildcard, float maxAbsoluteError, float minFrequency, eDecompressionCost maxDecompressionCost, eCompressionCost maxCompressionCost)
{
	u32 ruleIdx = AddRule(track, trackWildcard, id, idWildcard, maxAbsoluteError, minFrequency);
	Rule& rule = m_Rules[ruleIdx];
	
	if(maxDecompressionCost != kDecompressionCostInvalid)
	{
		Rule::Property& prop = rule.m_Properties.Grow();
		prop.m_Hash = sm_HashMaxDecompressionCost;
		prop.m_Int = int(maxDecompressionCost);
	}

	if(maxCompressionCost != kCompressionCostInvalid)
	{
		Rule::Property& prop = rule.m_Properties.Grow();
		prop.m_Hash = sm_HashMaxCompressionCost;
		prop.m_Int = int(maxCompressionCost);
	}

	return ruleIdx;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimToleranceComplex::AddProperty(u32 ruleIdx, u32 hash, float f)
{
	Rule& rule = m_Rules[ruleIdx];

	Rule::Property& prop = rule.m_Properties.Grow();
	prop.m_Hash = hash;
	prop.m_Float = f;

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimToleranceComplex::AddProperty(u32 ruleIdx, u32 hash, int i)
{
	Rule& rule = m_Rules[ruleIdx];

	Rule::Property& prop = rule.m_Properties.Grow();
	prop.m_Hash = hash;
	prop.m_Int = i;

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimToleranceComplex::RemoveRule(u32 idx)
{
	m_Rules.Delete(idx);
}

////////////////////////////////////////////////////////////////////////////////

void crAnimToleranceComplex::RemoveAllRules()
{
	m_Rules.Reset();
}

////////////////////////////////////////////////////////////////////////////////

u32 crAnimToleranceComplex::GetNumRules() const
{
	return m_Rules.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

const crAnimToleranceComplex::Rule::Property* crAnimToleranceComplex::FindRule(u8 track, u16 id, u32 hash) const
{
	const int numRules = m_Rules.GetCount();
	for(int i=0; i<numRules; ++i)
	{
		const Rule& rule = m_Rules[i];

		if(((rule.m_ValidFlags & Rule::kTrackValid) == 0) || (rule.m_Track == track))
		{
			if(((rule.m_ValidFlags & Rule::kIdValid) == 0) || (rule.m_Id == id))
			{
				const int numProps = rule.m_Properties.GetCount();
				for(int n=0; n<numProps; ++n)
				{
					if(rule.m_Properties[n].m_Hash == hash)
					{
						return &rule.m_Properties[n];
					}
				}
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crAnimToleranceMovement::crAnimToleranceMovement(float speedConstriction, float speedRelaxation, float minSpeed, float maxSpeed, float maxTranslationError, float maxRotationError, float maxScaleError, float maxDefaultError, eDecompressionCost maxDecompressionCost, eCompressionCost maxCompressionCost, float minFrequency)
: crAnimToleranceComplex(maxTranslationError, maxRotationError, maxScaleError, maxDefaultError, maxDecompressionCost, maxCompressionCost, minFrequency)
, m_SpeedConstriction(0.f)
, m_SpeedRelaxation(0.f)
, m_MinSpeed(0.f)
, m_MaxSpeed(0.f)
, m_Duration(0.f)
, m_SampleRate(0.f)
{
	m_SpeedConstriction = speedConstriction;
	m_SpeedRelaxation = speedRelaxation;
	m_MinSpeed = minSpeed;
	m_MaxSpeed = maxSpeed;
}

////////////////////////////////////////////////////////////////////////////////

float crAnimToleranceMovement::GetMaxAbsoluteError(u8 track, u16 id, u8 type) const
{
	return GetMaxAbsoluteError(0.f, m_Duration, track, id, type);
}

////////////////////////////////////////////////////////////////////////////////

float crAnimToleranceMovement::GetMaxAbsoluteError(float timeStart, float timeEnd, u8 track, u16 id, u8 type) const
{
	float error = crAnimToleranceComplex::GetMaxAbsoluteError(track, id, type);

	switch(track)
	{
	case kTrackBoneTranslation:
	case kTrackBoneRotation:
		{
			const int numBones = m_BoneRelaxation.GetCount();

			for(int b=0; b<numBones; ++b)
			{
				if(m_BoneRelaxation[b].m_BoneId == id)
				{
					const int numSamples = m_BoneRelaxation[b].m_Relaxation.GetCount();

					const int sampleStart = Clamp(int(floorf(timeStart/m_SampleRate)), 0, numSamples-1);
					const int sampleEnd = Clamp(int(ceilf(timeEnd/m_SampleRate)), sampleStart, numSamples-1);

					float relaxation = FLT_MAX;
					for(int i=sampleStart; i<=sampleEnd; ++i)
					{
						relaxation = Min(relaxation, m_BoneRelaxation[b].m_Relaxation[i]);				
					}

					error *= relaxation;
					break;
				}
			}
		}
		break;

	default:
		break;
	}

	return error;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimToleranceMovement::CalcRelaxation(const crAnimation& anim, const crSkeletonData& skelData, float sampleRate)
{
	m_Duration = anim.GetDuration();
	m_SampleRate = sampleRate;

	const u16 moverId = 0;
	Mat34V moverMtx(V_IDENTITY);

	crSkeleton skeleton;
	skeleton.Init(skelData, &moverMtx);

	crFrame frame;
	frame.InitCreateBoneAndMoverDofs(skelData, true);

	const int numSamples = int(ceilf((m_Duration-SMALL_FLOAT) / sampleRate)) + 1;
	const int numBones = skelData.GetNumBones();

	// initialize bone relaxation array
	m_BoneRelaxation.Reset();
	m_BoneRelaxation.Resize(numBones);
	for(int b=0; b<numBones; ++b)
	{
		m_BoneRelaxation[b].m_BoneId = skelData.GetBoneData(b)->GetBoneId();
		m_BoneRelaxation[b].m_Relaxation.Resize(numSamples);
	}

	// allocate temporary data
	Vec3V* trans = rage_new Vec3V[numBones];
	Vec3V* vels = rage_new Vec3V[numBones];

	for(int i=0; i<numSamples; ++i)
	{
		float time = Min(float(i) * m_SampleRate, m_Duration);
		float delta = m_SampleRate;
		if(i > 0)
		{
			float lastTime = float(i-1) * m_SampleRate;
			delta = time - lastTime;
		}

		frame.CompositeWithDelta(anim, time, delta);
		frame.Pose(skeleton);

		Mat34V deltaMtx;
		if(frame.GetMoverMatrix(moverId, deltaMtx))
		{
			Transform(moverMtx, moverMtx, deltaMtx);
		}

		skeleton.Update();

		// TODO --- anything for the mover?

		for(int b=0; b<numBones; ++b)
		{
			m_BoneRelaxation[b].m_Relaxation[i] = 1.f;

			Mat34V mtx;
			skeleton.GetGlobalMtx(b, mtx);

			if(i > 0)
			{
				Vec3V d = trans[b]-mtx.GetCol3();
				Vec3V v = d / ScalarVFromF32(delta);

				float s = Mag(v).Getf();
				m_BoneRelaxation[b].m_Relaxation[i] = Lerp(Clamp((s-m_MinSpeed)/(m_MaxSpeed-m_MinSpeed), 0.f, 1.f), m_SpeedConstriction, m_SpeedRelaxation);

				// TODO --- acceleration?
//				if(i > 1)
//				{
//					float a = (vels[b]-v).Mag() / delta;
//				}

				vels[b] = v;
			}

			trans[b] = mtx.GetCol3();

		}
	}

	// transfer relaxation for last sample (or sample one for non-looped) to sample zero
	bool looped = anim.IsLooped();
	for(int b=0; b<numBones; ++b)
	{
		m_BoneRelaxation[b].m_Relaxation[0] = m_BoneRelaxation[b].m_Relaxation[looped?(numSamples-1):1];
	}

	// apply minimum chain anchor relaxation values to all bones in chain
	const int numBoneChains = m_BoneChains.GetCount();
	for(int bc=0; bc<numBoneChains; ++bc)
	{
		int boneIdx;
		if(skelData.ConvertBoneIdToIndex(m_BoneChains[bc], boneIdx))
		{
			const crBoneData* bd = skelData.GetBoneData(boneIdx);
			if(bd)
			{
				int b = bd->GetIndex();

				bd = bd->GetParent();
				while(bd)
				{
					int bi = bd->GetIndex();
					for(int i=0; i<numSamples; ++i)
					{
						m_BoneRelaxation[bi].m_Relaxation[i] = Min(m_BoneRelaxation[bi].m_Relaxation[i], m_BoneRelaxation[b].m_Relaxation[i]);
					}
					bd = bd->GetParent();
				}
			}
		}
	}

	// destroy temporary data
	delete [] trans;
	delete [] vels;

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimToleranceMovement::SetSpeedRelaxation(float speedConstriction, float speedRelaxation, float minSpeed, float maxSpeed)
{
	m_SpeedConstriction = speedConstriction;
	m_SpeedRelaxation = speedRelaxation;
	m_MinSpeed = minSpeed;
	m_MaxSpeed = maxSpeed;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimToleranceMovement::AddBoneChain(u16 boneId)
{
	const int numChains = m_BoneChains.GetCount();
	for(int i=0; i<numChains; ++i)
	{
		if(m_BoneChains[i] == boneId)
		{
			break;
		}
	}

	m_BoneChains.Grow() = boneId;
}

////////////////////////////////////////////////////////////////////////////////

void crAnimToleranceMovement::RemoveBoneChain(u16 boneId)
{
	const int numChains = m_BoneChains.GetCount();
	for(int i=0; i<numChains; ++i)
	{
		if(m_BoneChains[i] == boneId)
		{
			m_BoneChains.Delete(i);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAnimToleranceMovement::RemoveAllBoneChains()
{
	m_BoneChains.Reset();
}

////////////////////////////////////////////////////////////////////////////////

const float crAnimToleranceMovement::sm_DefaultSpeedConstriction = 1.f;

////////////////////////////////////////////////////////////////////////////////

const float crAnimToleranceMovement::sm_DefaultSpeedRelaxation = 2.f;

////////////////////////////////////////////////////////////////////////////////

const float crAnimToleranceMovement::sm_DefaultMinSpeed = 0.1f;

////////////////////////////////////////////////////////////////////////////////

const float crAnimToleranceMovement::sm_DefaultMaxSpeed = 0.5f;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
