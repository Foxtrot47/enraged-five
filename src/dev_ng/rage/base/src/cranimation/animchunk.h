//
// cranimation/animchunk.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMCHUNK_H
#define CRANIMATION_ANIMCHUNK_H

#include "animchannel.h"

#if CR_DEV
#include "animtolerance.h"
#include "framedata.h"

#include "atl/array.h"
#include "string/stringhash.h"
#include "vectormath/classes.h"


namespace rage
{

class crAnimChannel;
class crAnimTolerance;
class crAnimTrack;
class crArchive;


// PURPOSE:
// Animation chunks represent the change in a value over a short period of time.
// They may store compound types (ie vectors, quaternions) or basic types (ie floats, integers etc).
// The period of time may be that of the entire animation, or some short subsection of it.
// Internally they pack this changing value using one or more channels, which
// can use a variety of different compression techniques.
class crAnimChunk
{
	friend class crAnimation;
	friend class crAnimPacker;

public:

	// PURPOSE: Declarations
	struct ToleranceHelper;

	// PURPOSE: Default constructor
	crAnimChunk();

	// PURPOSE: Destructor
	~crAnimChunk();

	// PURPOSE: Shutdown, release/free dynamic resources
	void Shutdown();

	// PURPOSE: Serialization
	void Serialize(crArchive& a);

	// PURPOSE: Clone, allocate new copy of self
	crAnimChunk* Clone() const;

	// PURPOSE: Get track index
	// RETURNS: Track index (see crAnimTrack::TRACK_XXX for enumeration of known track indices)
	u8 GetTrack() const;

	// PURPOSE: Get track id
	// RETURNS: Track id (meaning entirely depends on track index)
	u16 GetId() const;

	// PURPOSE: Set track id when cloning tracks
	void SetId(u16 id);

	// PURPOSE: Get track type
	// RETURNS: Track type (see crAnimTrack::FORMAT_TYPE_XXX for enumeration of known track types)
	u8 GetType() const;

	// PURPOSE: Get channel index to reconstruct
	u8 GetReconstruct() const;

	// PURPOSE: Get number of channels
	u16 GetNumChannels() const;

	// PURPOSE: Get channel by index
	const crAnimChannel* GetChannel(u16 idx) const;

	// PURPOSE: Test track index and id
	// PARAMS: track - track index
	// id - track id
	// RETURNS: true - if track index and id match
	bool TestTrackAndId(u8 track, u16 bone) const;

	// PURPOSE: Test track index and id
	// PARAMS: track - track index
	// id - track id
	// outMissedInOrderedList - if not a match, returns true if missed index/id in ordered list
	// RETURNS: true - if track index and id match
	bool TestTrackAndId(u8 track, u16 bone, bool& outMissedInOrderedList) const;


	// PURPOSE: Evaluate vector3 chunk
	// PARAMS: t - time (in internal animation frames, chunk aligned)
	// outVector3 - vector3 at this time
	// NOTES: Will assert if called on a chunk that is not of vector3 type
	void EvaluateVector3(float t, Vec3V_InOut outVector3) const;

	// PURPOSE: Evaluate quaternion chunk
	// PARAMS: t - time (in internal animation frames, chunk aligned)
	// outQuaternion - quaternion at this time
	// NOTES: Will assert if called on a chunk that is not of quaternion type
	void EvaluateQuaternion(float t, QuatV_InOut outQuaternion) const;

	// PURPOSE: Evaluate float chunk
	// PARAMS: t - time (in internal animation frames, chunk aligned)
	// outFloat - float at this time
	// NOTES: Will assert if called on a chunk that is not of float type
	void EvaluateFloat(float t, float& outFloat) const;

	// PURPOSE: Create vector3 chunk
	// PARAMS: track - track index
	// id - track id
	// vectors - pointer to set of vectors
	// num - number of vectors in chunk
	// toleranceHelper - compression tolerance control
	// compressionHelper - optional compression helper
	void CreateVector3(u8 track, u16 id, Vec3V* vectors, int num, const ToleranceHelper& toleranceHelper);

	// PURPOSE: Create quaternion chunk
	// PARAMS: track - track index
	// id - track id
	// quaternions - pointer to set of quaternions
	// num - number of quaternions in chunk
	// toleranceHelper - compression tolerance control
	// compressionHelper - optional compression helper
	void CreateQuaternion(u8 track, u16 id, QuatV* quaternions, int num, const ToleranceHelper& toleranceHelper);

	// PURPOSE: Create float chunk
	// PARAMS: track - track index
	// id - track id
	// floats - pointer to set of floats
	// num - number of floats in chunk
	// toleranceHelper - compression tolerance control
	// compressionHelper - optional compression helper
	void CreateFloat(u8 track, u16 id, float* floats, int num, const ToleranceHelper& toleranceHelper);

	// PURPOSE: Compute total storage use
	// RETURNS: Storage used, in bytes
	u32 ComputeSize() const;

	// PURPOSE: Helps with tolerance ranges and time conversions
	struct ToleranceHelper
	{
	public:
		// PURPOSE: Construct with animation tolerance
		ToleranceHelper(const crAnimTolerance& tolerance, u8 track, u16 id, u8 type) : m_Tolerance(NULL), m_FrameRate(0.f) { m_Tolerance = &tolerance; m_Track = track; m_Id = id; m_Type = type; }

		// PURPOSE: Construct with animation tolerance and range
		ToleranceHelper(const crAnimTolerance& tolerance, u8 track, u16 id, u8 type, u16 startFrame, u16 endFrame, float frameRate) : m_Tolerance(NULL) { m_Tolerance = &tolerance; m_Track = track; m_Id = id; m_Type = type; m_StartFrame = startFrame; m_EndFrame = endFrame; m_FrameRate = frameRate; }

		// PURPOSE: Get max absolute error for current range
		float GetMaxAbsoluteError() const { return (m_FrameRate>0.f)?m_Tolerance->GetMaxAbsoluteError(float(m_StartFrame)*m_FrameRate, float(m_EndFrame)*m_FrameRate, m_Track, m_Id, m_Type):m_Tolerance->GetMaxAbsoluteError(m_Track, m_Id, m_Type); }

		// PURPOSE: Get max absolute error for an internal frame
		float GetMaxAbsoluteError(u32 internalFrame) const { return (m_FrameRate>0.f)?m_Tolerance->GetMaxAbsoluteError(float(internalFrame+m_StartFrame)*m_FrameRate, float(internalFrame+m_StartFrame+1)*m_FrameRate, m_Track, m_Id, m_Type):GetMaxAbsoluteError(); }

		// PURPOSE: Get max decompression cost
		crAnimTolerance::eDecompressionCost GetMaxDecompressionCost() const { return m_Tolerance->GetMaxDecompressionCost(m_Track, m_Id, m_Type); }

		// PURPOSE: Get max compression cost
		crAnimTolerance::eCompressionCost GetMaxCompressionCost() const { return m_Tolerance->GetMaxCompressionCost(m_Track, m_Id, m_Type); }

		// PURPOSE: Get linear compression bias
		float GetLinearBias() const { float bias = 2.f; m_Tolerance->GetProperty(m_Track, m_Id, m_Type, atHashString("ChannelLinearBias"), bias); return bias; }

	private:
		const crAnimTolerance* m_Tolerance;
		u8 m_Track;
		u16 m_Id;
		u8 m_Type;

		u16 m_StartFrame;
		u16 m_EndFrame;
		float m_FrameRate;
	};


	// PURPOSE: Internal function, returns information about storage packing used
	// See crAnimChunk::FORMAT_PACK_XXX for description of packing methods
	u8 GetPack() const;

protected:

	// PURPOSE: Internal function, creates a float channel (with best compression given data and control values)
	// PARAMS: floats - set of floats (can be in regular sparse pattern)
	// num - number of floats to compress in channel
	// skip - number of intervening floats to skip (used for regular sparse patterns, a skip zero is contiguous)
	// toleranceHelper - tolerance helper, controls compression costs/quality
	crAnimChannel* CreateFloatChannel(float* floats, int num, int skip, float maxErrorScalar, const ToleranceHelper& toleranceHelper) const;

	// PURPOSE: Internal function, creates the channel quaternion of the quaternion under a tolerance
	void OptimizeTolerance(u32& bestSize, u8 reconstruct, QuatV* quaternions, int num, float maxErrorScalar, const ToleranceHelper& toleranceHelper);

	// PURPOSE: Internal function, removes any existing channels from chunk
	void Reset();


	// PURPOSE: Internal storage formats (do not expose to end user)
	// These describe different ways in which the channels are used to pack values.
	// Do not change existing values.
	enum
	{
		FORMAT_PACK_VECTOR3_RAW = 0x00,
		FORMAT_PACK_VECTOR3_XYZ = 0x04,

		FORMAT_PACK_QUATERNION_RAW = 0x00,
		FORMAT_PACK_QUATERNION_XYZW = 0x04,
		FORMAT_PACK_QUATERNION_XYZ_RECONSTRUCT = 0x08,

		FORMAT_PACK_FLOAT_RAW = 0x00,
		FORMAT_PACK_MASK = 0x0c,
	};

	enum
	{
		FORMAT_RECONSTRUCT_X,
		FORMAT_RECONSTRUCT_Y,
		FORMAT_RECONSTRUCT_Z,
		FORMAT_RECONSTRUCT_W,
		FORMAT_RECONSTRUCT_NONE,
	};

	// PURPOSE: Known track types (do NOT reorder these)
	// This is a copy of the track types found in crAnimTrack (to avoid recursive includes)
	enum
	{
		FORMAT_TYPE_MASK = 0x03,
	};

private:
	u8 m_Track;
	u8 m_Format;
	u8 m_Reconstruct;
	u16 m_Id;

	// TODO --- change to range array?  count can be calculated from format...
	atFixedArray< crAnimChannel*, 4> m_Channels;
};


// inline functions

////////////////////////////////////////////////////////////////////////////////

inline u8 crAnimChunk::GetTrack() const
{
	return m_Track;
}

////////////////////////////////////////////////////////////////////////////////

inline u16 crAnimChunk::GetId() const
{
	return m_Id;
}

////////////////////////////////////////////////////////////////////////////////

inline void crAnimChunk::SetId(u16 id)
{
	m_Id = id;
}

////////////////////////////////////////////////////////////////////////////////

inline u8 crAnimChunk::GetType() const
{
	return u8(m_Format & u8(FORMAT_TYPE_MASK));
}

////////////////////////////////////////////////////////////////////////////////

inline u8 crAnimChunk::GetReconstruct() const
{
	return m_Reconstruct;
}

////////////////////////////////////////////////////////////////////////////////

inline u16 crAnimChunk::GetNumChannels() const
{
	return u16(m_Channels.GetCount());
}

////////////////////////////////////////////////////////////////////////////////

inline const crAnimChannel* crAnimChunk::GetChannel(u16 idx) const
{
	return m_Channels[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimChunk::TestTrackAndId(u8 track, u16 id) const
{
	return (track == m_Track) && (id == m_Id);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimChunk::TestTrackAndId(u8 track, u16 id, bool& outMissedInOrderedList) const
{
	u32 thisTrackId = (m_Track << 16) | m_Id;
	u32 otherTrackId = (track << 16) | id;

	outMissedInOrderedList = (thisTrackId > otherTrackId);
	return thisTrackId == otherTrackId;
}

////////////////////////////////////////////////////////////////////////////////

inline void crAnimChunk::EvaluateVector3(float t, Vec3V_InOut outVector3) const
{
	FastAssert(GetType() == kFormatTypeVector3);

	const u8 pack = GetPack();

	if(pack == FORMAT_PACK_VECTOR3_XYZ)
	{
		m_Channels[0]->EvaluateFloat(t, outVector3[0]);
		m_Channels[1]->EvaluateFloat(t, outVector3[1]);
		m_Channels[2]->EvaluateFloat(t, outVector3[2]);
	}
	else
	{
		FastAssert(pack == FORMAT_PACK_VECTOR3_RAW);

		m_Channels[0]->EvaluateVector3(t, outVector3);
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void crAnimChunk::EvaluateQuaternion(float t, QuatV_InOut outQuaternion) const
{
	FastAssert(GetType() == kFormatTypeQuaternion);

	const u8 pack = GetPack();

	if(pack == FORMAT_PACK_QUATERNION_RAW)
	{
		m_Channels[0]->EvaluateQuaternion(t, outQuaternion);
	}
	else if(m_Reconstruct == FORMAT_RECONSTRUCT_NONE)
	{
		m_Channels[0]->EvaluateFloat(t, outQuaternion[0]);
		m_Channels[1]->EvaluateFloat(t, outQuaternion[1]);
		m_Channels[2]->EvaluateFloat(t, outQuaternion[2]);
		m_Channels[3]->EvaluateFloat(t, outQuaternion[3]);
	}
	else
	{
		float f[4];
		m_Channels[0]->EvaluateFloat(t, f[0]);
		m_Channels[1]->EvaluateFloat(t, f[1]);
		m_Channels[2]->EvaluateFloat(t, f[2]);
		f[3] = sqrtf(Max(0.f, 1.f - (square(f[0])+square(f[1])+square(f[2]))));

		switch(m_Reconstruct)
		{
		case FORMAT_RECONSTRUCT_X:		outQuaternion = QuatV(f[3], f[0], f[1], f[2]); break;
		case FORMAT_RECONSTRUCT_Y:		outQuaternion = QuatV(f[0], f[3], f[1], f[2]); break;
		case FORMAT_RECONSTRUCT_Z:		outQuaternion = QuatV(f[0], f[1], f[3], f[2]); break;
		case FORMAT_RECONSTRUCT_W:		outQuaternion = QuatV(f[0], f[1], f[2], f[3]); break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void crAnimChunk::EvaluateFloat(float t, float& outFloat) const
{
	FastAssert(GetType() == kFormatTypeFloat);

	ASSERT_ONLY(const u8 pack = GetPack());
	FastAssert(pack == FORMAT_PACK_FLOAT_RAW);

	m_Channels[0]->EvaluateFloat(t, outFloat);
}

////////////////////////////////////////////////////////////////////////////////

inline u8 crAnimChunk::GetPack() const
{
	return u8(m_Format & u8(FORMAT_PACK_MASK));
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_ANIMCHUNK_H
