//
// cranimation/animpacker.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMPACKER_H
#define CRANIMATION_ANIMPACKER_H

#include "animation_config.h"

#if CR_DEV
#include "animchannel.h"
#include "animstream.h"
#include "streamer.h"

namespace rage
{

class crAnimation;
class crAnimChunk;
class crAnimTrack;

////////////////////////////////////////////////////////////////////////////////

class crAnimPacker
{
public:
	// PURPOSE: Pack an animation
	static u32 Pack(bool compact, bool byteSwap, crAnimation& anim);

	// PURPOSE: Compact an existing block
	static void Compact(Streamer<crBlockStream>& block, u32 dataSize);

	// PURPOSE: Swap an existing block
	static void Swap(Streamer<crBlockStream>& block);

private:
	// PURPOSE: Create a block from an animation and a chunkIdx
	static void Create(Streamer<crBlockStream>& block, const crAnimation& anim, u16 chunkIdx);

	// PURPOSE: Convert legacy channel for backward compatibility
	static void ConvertLegacyChannel(crAnimChunk* chunk, const crAnimChannel* input);

	// PURPOSE: Add new dof to the stream
	static bool AddDof(const crAnimChunk* input, u32 trackIdx, atArray<u16>* indices, atArray<u32>* constants);

	// PURPOSE: Add new channel to the stream
	static void AddChannel(const crAnimChannel* input, u32 trackIdx, u32 channelIdx, atArray<u16>* indices, atArray<u32,0,u32>* constants, u32& quantizeSize, u32& indirectSize);

	// PURPOSE: Create frame from a list of indices
	static void CreateFrame(u16 chunkIdx, u32 frameIdx, BaseStreamer& frame, const crAnimation& anim, atArray<u16>* channels);
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_ANIMPACKER_H
