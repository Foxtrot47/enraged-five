//
// cranimation/animdictionary.cpp
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#include "animdictionary.h"

#include "animtolerance.h"
#include "frame.h"

#include "atl/map_struct.h"
#include "atl/string.h"
#include "data/safestruct.h"
#include "file/stream.h"
#include "file/token.h"
#include "paging/rscbuilder.h"
#include "system/memory.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crAnimDictionary::crAnimDictionary()
: pgBaseRefCounted(0)
, m_BaseNameKeys(false)
{
}

////////////////////////////////////////////////////////////////////////////////

void FixupDataFunc(datResource &rsc, datOwner<crAnimation>& data)
{
	data.Place(&data, rsc);
}

crAnimDictionary::crAnimDictionary(datResource& rsc)
: pgBaseRefCounted(rsc)
, m_Animations(rsc, NULL, &FixupDataFunc)
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimDictionary::~crAnimDictionary()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

bool ReleaseAnimFunc(crAnimation* anim, crAnimDictionary::AnimKey, void*)
{
	if(anim)
	{
		anim->Release();
	}
	return true;
}

void crAnimDictionary::Shutdown()
{
	ForAll(&ReleaseAnimFunc, NULL);

	m_Animations.Kill();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crAnimDictionary);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crAnimDictionary::DeclareStruct(datTypeStruct& s)
{
	pgBaseRefCounted::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(crAnimDictionary, pgBaseRefCounted)
	SSTRUCT_FIELD(crAnimDictionary, m_Animations)
	SSTRUCT_FIELD(crAnimDictionary, m_BaseNameKeys)
	SSTRUCT_IGNORE(crAnimDictionary, m_Padding)
	SSTRUCT_END(crAnimDictionary)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crAnimDictionary* crAnimDictionary::LoadResource(const char* dictionaryFilename)
{
	crAnimDictionary *adt = NULL;

	pgRscBuilder::Load(adt, dictionaryFilename, "#adt", crAnimDictionary::RORC_VERSION);

	return adt;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
crAnimDictionary* crAnimDictionary::AllocateAndLoadAnimations(const char* listFilename, crAnimLoader* loader, bool baseNameKeys)
{
	crAnimDictionary* dictionary = rage_new crAnimDictionary;
	if(dictionary->LoadAnimations(listFilename, loader, baseNameKeys))
	{
		return dictionary;
	}
	delete dictionary;
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crAnimDictionary* crAnimDictionary::AllocateAndLoadAnimations(const atArray<atString>& animFilenames, crAnimLoader* loader, bool baseNameKeys)
{
	crAnimDictionary* dictionary = rage_new crAnimDictionary;
	if(dictionary->LoadAnimations(animFilenames, loader, baseNameKeys))
	{
		return dictionary;
	}
	delete dictionary;
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimDictionary::LoadAnimations(const char* listFilename, crAnimLoader* loader, bool baseNameKeys)
{
	fiSafeStream f(ASSET.Open(listFilename, "animlist"));
	if(f)
	{
		fiTokenizer T(listFilename, f);	

		sysMemStartTemp();
		atArray<atString> animFilenames;

		const int maxBufSize = RAGE_MAX_PATH;
		char buf[maxBufSize];
		while(T.GetLine(buf, maxBufSize) > 0)
		{
			animFilenames.Grow() = buf;
		}
		sysMemEndTemp();

		bool success = LoadAnimations(animFilenames, loader, baseNameKeys);

		sysMemStartTemp();
		animFilenames.Reset();
		sysMemEndTemp();

		return success;
	}
	else
	{
		Errorf("crAnimDictionary::LoadAnimations - failed to open list file '%s'", listFilename);
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool crAnimDictionary::LoadAnimations(const atArray<atString>& animFilenames, crAnimLoader* loader, bool baseNameKeys)
{
	crAnimLoaderBasic defaultLoader;
	if(!loader)
	{
		loader = &defaultLoader;
	}

	const int numAnims = animFilenames.GetCount();
	m_Animations.Create(u16(numAnims));
	m_BaseNameKeys = baseNameKeys;

	bool success = true;
	for(int i=0; i<numAnims; ++i)
	{
		const char* animFilename = (const char*)animFilenames[i];

		AnimKey key = ConvertNameToKey(animFilename);
		const crAnimation* existingAnim = GetAnimation(key);
		if(existingAnim)
		{
			if(!stricmp(existingAnim->GetName(), animFilename))
			{
				Warningf("crAnimDictionary::LoadAnimations - attempted multiple addition of same animation '%s'", animFilename);
				continue;
			}
			else
			{
				Errorf("crAnimDictionary::LoadAnimations - hash clash '%s' == '%s' (%d) base name keys %d", animFilename, existingAnim->GetName(), u32(key), baseNameKeys);
				success = false;
				continue;
			}
		}

		crAnimation* anim = loader->AllocateAndLoadAnimation(animFilename);
		if(!anim)
		{
			Errorf("crAnimDictionary::LoadAnimations - failed to load animation '%s'", animFilename);
			success = false;
			continue;
		}
		else if(anim->GetMaxBlockSize() > CR_DEFAULT_MAX_BLOCK_SIZE)
		{
			Errorf("crAnimDictionary::LoadAnimations - animation '%s' has invalid block size %d", animFilename, anim->GetMaxBlockSize());
			success = false;
			continue;
		}

		m_Animations.Insert(key, anim);
	}

	return success;
}

////////////////////////////////////////////////////////////////////////////////

crAnimLoaderBasic::crAnimLoaderBasic(bool compact, bool pack)
{
	Init(compact, pack);
}

////////////////////////////////////////////////////////////////////////////////

crAnimLoaderBasic::~crAnimLoaderBasic()
{
}

////////////////////////////////////////////////////////////////////////////////

void crAnimLoaderBasic::Init(bool compact, bool pack)
{
	m_Compact = compact;
	m_Pack = pack;
}

////////////////////////////////////////////////////////////////////////////////

crAnimation* crAnimLoaderBasic::AllocateAndLoadAnimation(const char* animFilename)
{
	u8 junkVersion;
	return crAnimation::AllocateAndLoad(animFilename, &junkVersion, m_Pack, m_Compact);
}

////////////////////////////////////////////////////////////////////////////////

crAnimLoaderCompress::crAnimLoaderCompress()
: crAnimLoaderBasic()
, m_Tolerance(&LOSSLESS_ANIM_TOLERANCE)
, m_MaxBlockSize(CR_DEFAULT_MAX_BLOCK_SIZE)
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimLoaderCompress::~crAnimLoaderCompress()
{
}

////////////////////////////////////////////////////////////////////////////////

void crAnimLoaderCompress::Init(crAnimTolerance& tolerance, const u32 maxBlockSize)
{
	m_Tolerance = &tolerance;
	m_MaxBlockSize = maxBlockSize;
}

////////////////////////////////////////////////////////////////////////////////

crAnimation* crAnimLoaderCompress::AllocateAndLoadAnimation(const char* animFilename)
{
	u8 junkVersion;
	crAnimation* srcAnim = crAnimation::AllocateAndLoad(animFilename, &junkVersion, false, false, false);
	if(srcAnim)
	{
		crAnimation* destAnim = rage_aligned_new(16) crAnimation;

		// TODO - COPIED FROM CRTOOLS/ANIMCOMPRESSCORE - MOVE TO MORE CENTRAL (NON-TOOLS LOCATION)
		// JUST HERE TEMPORARILY, TO SEE IF THIS APPROACH IS EVEN FEASIBLE...

		// JM TODO - BEGIN, TEMPORARY COPY/PASTED CODE

		// convert source animation into frames:
		const u32 numFrames = srcAnim->GetNumInternalFrames();
		atArray<const crFrame*> frames(numFrames, numFrames);

		for(u32 i=0; i<numFrames; i++)
		{
			crFrame* frame = rage_new crFrame;
			frame->InitCreateAnimationDofs(*srcAnim);
			frame->Composite(*srcAnim, srcAnim->ConvertInternalFrameToTime(float(i)));

			frames[i] = frame;
		}

		// create new animation:
		u32 numBlocks = 0;
		while(1)
		{
			// clear out previous attempt
			destAnim->Shutdown();

			// calculate the frames per chunk
			u32 framesPerChunk = u32((numFrames-1)/(++numBlocks));

			// round up to next 16 frame value minus 1
			const u32 minFramesPerChunk = 16;
			if((framesPerChunk > minFramesPerChunk) || (numBlocks == 1))
			{
				framesPerChunk = ((framesPerChunk+16)&(~15))-1;
			}
			else
			{
				Errorf("ERROR - Max block size driven frames per chunk search failed on animation '%s'", srcAnim->GetName());
				return NULL;
			}

			// create animation, compress frames with tolerance and then pack (so block size can be measured)
			destAnim->Create(numFrames, srcAnim->GetDuration(), srcAnim->IsLooped(), srcAnim->HasMoverTracks(), framesPerChunk);
			destAnim->CreateFromFramesFast(frames, *m_Tolerance);

			// skip packing if the block size is twice the maximum
			u32 averageSize = destAnim->ComputeSize() / numBlocks;
			if(averageSize > m_MaxBlockSize)
				continue;

			// increase the number of blocks until the block size is smaller than the maximum
			destAnim->Pack(m_Compact, false);
			if(destAnim->GetMaxBlockSize() <= m_MaxBlockSize)
				break;
		}

		destAnim->SetProjectFlags(srcAnim->GetProjectFlags());
		destAnim->SetName(srcAnim->GetName());

		// shutdown:
		for(u32 i=0; i<numFrames; i++)
		{
			delete frames[i];
		}

		// JM TODO - END, TEMPORARY COPY/PASTED CODE

		delete srcAnim;
		return destAnim;
	}

	return NULL;
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

crAnimLoader::crAnimLoader()
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimLoader::~crAnimLoader()
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimLoaderDictionary::crAnimLoaderDictionary()
: m_Dictionary(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimLoaderDictionary::crAnimLoaderDictionary(const crAnimDictionary* dictionary)
: m_Dictionary(NULL)
{
	Init(dictionary);
}

////////////////////////////////////////////////////////////////////////////////

crAnimLoaderDictionary::~crAnimLoaderDictionary()
{
}

////////////////////////////////////////////////////////////////////////////////

void crAnimLoaderDictionary::Init(const crAnimDictionary* dictionary)
{
	m_Dictionary = dictionary;
}

////////////////////////////////////////////////////////////////////////////////

crAnimation* crAnimLoaderDictionary::AllocateAndLoadAnimation(const char* animFilename)
{
	if(m_Dictionary)
	{
		crAnimation* anim = const_cast<crAnimation*>(m_Dictionary->GetAnimation(animFilename));
		if(anim)
		{
			anim->AddRef();
			return anim;
		}
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crAnimLoaderNull::crAnimLoaderNull()
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimLoaderNull::~crAnimLoaderNull()
{
}

////////////////////////////////////////////////////////////////////////////////

crAnimation* crAnimLoaderNull::AllocateAndLoadAnimation(const char*)
{
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
