//
// cranimation/channelquantize.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "channelquantize.h"

#if CR_DEV
#include "animarchive.h"
#include "animation.h"
#include "animstream.h"
#include "animtolerance.h"

namespace rage
{

IMPLEMENT_ANIM_CHANNEL(crAnimChannelQuantizeFloat, crAnimChannel::AnimChannelTypeQuantizeFloat, crAnimTolerance::kCompressionCostMed, crAnimTolerance::kDecompressionCostMed);

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelQuantizeFloat::EvaluateFloat(float fTime, float& outFloat) const
{
	int iTime = int(floor(fTime));
	float fract = fTime - float(iTime);

	if(fract > (1.f - INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE))
	{
		iTime++;
	}
	else if(fract > INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE)
	{
		outFloat = Lerp(fract, ReconstructFloat(iTime), ReconstructFloat(iTime+1));
		return;
	}

	outFloat = ReconstructFloat(iTime);
}

//////////////////////////////////////////////////////////////////////////////////////////////

bool crAnimChannelQuantizeFloat::CreateFloat(float* floats, int frames, int skip, float tolerance)
{
	return QuantizeFloats(floats, frames, skip, tolerance, m_QuantizedValues, m_Scale, m_Offset);
}

//////////////////////////////////////////////////////////////////////////////////////////////

int crAnimChannelQuantizeFloat::ComputeSize(void) const
{
	return 3*sizeof(u32) + m_QuantizedValues.ComputeSize();
}

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelQuantizeFloat::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	a<<=m_Scale;
	a<<=m_Offset;

	m_QuantizedValues.Serialize(a);
}

//////////////////////////////////////////////////////////////////////////////////////////////

bool crAnimChannelQuantizeFloat::QuantizeFloats(float* floats, int numFloats, int skip, float tolerance, atPackedArray& outQuantizedValues, float& outScale, float& outOffset)
{
	// quantizing is only worth it when trying to store more than 3 frames
	if(numFloats < 3)
	{
		return false;
	}

	// can only quantize if tolerance is non-zero
	if(tolerance == 0.f)
	{
		return false;
	}

	int stride = 1 + skip;

	// find range of floats
	float maxfloat = -FLT_MAX;
	float minfloat = FLT_MAX;

	float *fp = floats;
	for(int i=0; i<numFloats; i++)
	{
		maxfloat = Max(maxfloat, *fp);
		minfloat = Min(minfloat, *fp);
		fp += stride;
	}

	// normalize into 0->1 range
	float offset = minfloat;
	float scale = maxfloat - minfloat;

	float numValues = Max(ceil(scale / tolerance), 1.f);

	// can range of values be quantized within tolerance
	if(numValues >= float(0x80000000))
	{
		return false;
	}

	u32 numBits = 0;
	u32 values = u32(numValues);
	while(values > 0)
	{
		values >>= 1;
		numBits++;
	}

	Assert(numBits > 0);
	Assert(numBits < 32);

	// TODO --- might want to provide fast route, for factors powers of 2 (or multiples of 8)?

	scale *= 1.f / (0xffffffff>>(32-numBits));
	float invScale = 1.f / scale;

	atArray<u32> quantizedValues;
	quantizedValues.Resize(numFloats);

	// quantize floats
	fp = floats;
	for(int i=0; i<numFloats; i++)
	{
		quantizedValues[i] = u32(((*fp - offset) * invScale)+0.5f);
		fp += stride;
	}

	// clamp values
	u32 max = u32(0xffffffff>>(32-numBits));
	for(int i=0; i<numFloats; i++)
	{
		quantizedValues[i] = Clamp(quantizedValues[i], 0u, max);
	}

	outQuantizedValues.Init(numBits, quantizedValues);
	outOffset = offset;
	outScale = scale;
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////

float crAnimChannelQuantizeFloat::ReconstructFloat(int frame) const
{
	u32 quantizedValue = m_QuantizedValues.GetElement(Clamp(u32(frame), u32(0), m_QuantizedValues.GetElementMax()-1));

	return (float(quantizedValue) * m_Scale) + m_Offset;
}

//////////////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
