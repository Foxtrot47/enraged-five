//
// cranimation/animchannel.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMCHANNEL_H
#define CRANIMATION_ANIMCHANNEL_H

#include "animation_config.h"

#if CR_DEV
#include "atl/array.h"
#include "vectormath/vec3v.h"
#include "vectormath/quatv.h"

#define INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE (0.001f)

namespace rage
{

class crArchive;
class crAnimation;


// PURPOSE
// Animation channels represent the change in value over time of a single type.
// They may store compound types (ie vectors, quaternions) or basic types (ie floats, integers etc).
// There are many different types of channel, for storing all the different value types,
// and using different compression techniques.
class crAnimChannel
{
	// TODO --- cleanup, standardize and document this class

public:
	crAnimChannel() : m_Flags(0), m_Type(0), m_Padding(0) {}
	crAnimChannel(u8 type) : m_Flags(0), m_Type(type), m_Padding(0) {}
	virtual ~crAnimChannel() {}

	virtual crAnimChannel* Clone() = 0;

	static crAnimChannel* CreateChannel(u8 type);

	u8 GetType() const  { return m_Type; }

	static u8 GetCompressionCost(u8 type);

	static u8 GetDecompressionCost(u8 type);

	virtual void EvaluateVector3(float, Vec3V_InOut) const { }
	virtual void EvaluateQuaternion(float, QuatV_InOut) const { }
	virtual void EvaluateFloat(float, float&) const { }

	virtual bool CreateFloat(float*, int, int, float) { return false; }
	virtual bool CreateFloat(float* floats, int frames, int skip, float tolerance, float*) { return CreateFloat(floats, frames, skip, tolerance); }
	virtual bool CreateVector3(Vec3V*, int, float) { return false; }
	virtual bool CreateQuaternion(QuatV*, int, float) { return false; }

	virtual int ComputeSize() const  { return 0; }

	virtual void Serialize(crArchive&);

	enum
	{
		// do not change the order of these!
		AnimChannelTypeNone = 0,
		AnimChannelTypeRawFloat,
		AnimChannelTypeRawVector3,
		AnimChannelTypeRawQuaternion,
		AnimChannelTypeStaticFloat,
		AnimChannelTypeCurveFloat,
		AnimChannelTypeQuantizeFloat,
		AnimChannelTypeRawInt,
		AnimChannelTypeRawBool,
		AnimChannelTypeStaticQuaternion,
		AnimChannelTypeDeltaFloat,
		AnimChannelTypeStaticInt,
		AnimChannelTypeRleInt,
		AnimChannelTypeStaticVector3,
		AnimChannelTypeSmallestThreeQuaternion,
		AnimChannelTypeVariableQuantizeFloat,
		AnimChannelTypeIndirectQuantizeFloat,
		AnimChannelTypeLinearFloat,
		AnimChannelTypeQuadraticBSpline,
		AnimChannelTypeCubicBSpline,
		AnimChannelTypeStaticSmallestThreeQuaternion,
		AnimChannelTypeNum,  // must be last in list of rage channel types
		// reserved for future rage channel types only.
		// remember to initialize these classes in InitClass()

		AnimChannelTypeCustom = 0x80,  // custom channel types must be greater or equal to this value
	};

	typedef crAnimChannel* ChannelCreateFn();
	typedef void ChannelPlaceFn(crAnimChannel*, datResource&);

	struct ChannelTypeInfo
	{
		ChannelTypeInfo(u8 channelType, u8 compressCost, u8 decompressCost, ChannelCreateFn* createFn)
			: m_ChannelType(channelType)
			, m_CompressCost(compressCost)
			, m_DecompressCost(decompressCost)
			, m_CreateFn(createFn)
		{
		}
	
		void Register() const
		{
			crAnimChannel::RegisterChannelTypeInfo(*this);
		}

		u8 m_ChannelType;
		u8 m_CompressCost;
		u8 m_DecompressCost;
		ChannelCreateFn* m_CreateFn;
		ChannelPlaceFn* m_PlaceFn;
	};

	static void InitClass();

	static void ShutdownClass();

	static const ChannelTypeInfo* GetChannelTypeInfo(u8 type);

private:
	static void RegisterChannelTypeInfo(const ChannelTypeInfo& info);

private:
	u8 m_Flags;
	u8 m_Type;
	u16 m_Padding;

	static atArray<const ChannelTypeInfo*> sm_ChannelTypeInfos;

	static bool sm_InitClassCalled;

	friend struct ChannelTypeInfo;
};


#define DECLARE_ANIM_CHANNEL(__type) \
	virtual crAnimChannel* Clone() { return rage_new __type(*this); } \
	static crAnimChannel* Create() { return rage_new __type; } \
	static void InitClass() { sm_ChannelTypeInfo.Register(); } \
	static const ChannelTypeInfo sm_ChannelTypeInfo;
	
#define IMPLEMENT_ANIM_CHANNEL(__type, __index, __compressCost, __decompressCost) \
	const crAnimChannel::ChannelTypeInfo __type::sm_ChannelTypeInfo((__index), __compressCost, __decompressCost, __type::Create);


// TODO --- move this somewhere else!
// if references to crArchive can be removed, this could be moved to atl...
class atPackedArray
{
	friend class crAnimPacker;
public:
	atPackedArray();
	atPackedArray(const atPackedArray& r);
	~atPackedArray();
	void operator=(const atPackedArray& r);

	int ComputeSize() const;
	void Serialize(crArchive&);
	void Init(u32 elementSize, const atArray<u32>& values);
	u32 GetElement(u32 index) const;
	u32 GetElementMax() const;
	bool IsEmpty() const;
	u32 GetElementSize() const;

private:
	u32 GetElementCount() const;

	u32* m_Elements;
	u32 m_ElementSize;
	u32 m_ElementMax;
};

// Rice encoding, little-endian
// remainder, quotient and sign
class atPackedSequence
{
	friend class crAnimPacker;
public:
	atPackedSequence();
	~atPackedSequence();
	atPackedSequence(const atPackedSequence& r);
	int ComputeSize() const;
	void Serialize(crArchive& a);

	bool InitAndCreate(const atArray<s32>& values, u32& lowestAddressMax);
	s32 GetValue(u32& address) const;
	static u32 CalcAddressMax(const atArray<s32>& values, u32 divisor);

private:
	u32 GetSequenceMax() const;
	u32 GetSequenceCount() const;
	void SetValue(u32& address, s32 v);
	u32 GetBit(u32& address) const;
	void SetBit(u32& address, u32 v);

	// maximum number of bits for the quotient
	static const u32 kQuotientMax = 30;

	// maximum power of two for the divisor
	static const u32 kDivisorMax = 15;

	u32* m_Sequence;
	u32 m_SequenceMax;
	u32 m_Divisor;
};

}  // namespace rage

#endif // CR_DEV

#endif
