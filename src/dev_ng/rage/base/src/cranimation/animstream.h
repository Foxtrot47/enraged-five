//
// cranimation/animstream.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMSTREAM_H
#define CRANIMATION_ANIMSTREAM_H

#include "cranimation/animation_config.h"
#include "vectormath/quatv.h"
#include "vectormath/vec3v.h"

namespace rage
{

// PURPOSE: Global array of mask for 32 bits elements
extern const u32 g_MaskTable[];

// PURPOSE:
// The block stream is used at run-time to playback an animation.
// It contains the animation data for a range of frames.
// For the cache and the spu, the internal data is contiguous in memory.
// It contains two memory sections : constant and variable.
// The constant section contains all static values.
// The variable section is sorted per frame and contains the animated values.

struct crBlockStream
{
	struct DofCursor;

	// PURPOSE: Return true if the block is compact
	bool IsCompact() const;

	// PURPOSE: Return uncompressed block size
	u32 GetBlockSize() const;

	// PURPOSE: Evaluate a whole frame
	void EvaluateFrame(u8* output, u32 tq, f32 tr, const u16* offsets) const;

	// PURPOSE: Channel for fast single dof evaluation
	struct Channel
	{
		u8 m_Type;
		u8 m_Component;
		u16 m_Offset;
		u16 m_Address;
	};

	// PURPOSE: Find a list of channels inside the block
	void FindChannels(u32 dofIdx, u32& outCount, Channel* outChannels) const;

	// PURPOSE: Evaluate a list of channels
	void EvaluateChannels(u8* output, u32 tq, f32 tr, u32 count, const Channel* channels) const;

	// PURPOSE: Compression format used
	enum eFormatType { kConstantQuat, kConstantVec, kConstant, kRaw, kQuantize, kIndirect, kLinear, kReconstruct, kNormalize, kNumType };

	// PURPOSE: Data hash to check integrity
	u32 m_Hash;

	// PURPOSE: Size in byte of the uncompressed data excluding the block header
	u32 m_DataSize;

	// PURPOSE: Size in byte of the compressed data excluding the block header
	u32 m_CompactSize;

	u32 m_ConstantSize;
	u32 m_ChannelOffset;
	u16 m_SlopSize;
	u16 m_NumFrames;
	u16 m_FrameSize;
	u16 m_IndirectSize;
	u16 m_QuantizeSize;
	u8 m_SegmentSize;
	u8 m_NumMoverChannels;
	u8 m_Data[];
};

////////////////////////////////////////////////////////////////////////////////

inline bool crBlockStream::IsCompact() const
{
	return m_CompactSize != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crBlockStream::GetBlockSize() const
{
	return sizeof(crBlockStream) + m_DataSize;
}

////////////////////////////////////////////////////////////////////////////////

};  // namespace rage

#endif // CRANIMATION_ANIMSTREAM_H
