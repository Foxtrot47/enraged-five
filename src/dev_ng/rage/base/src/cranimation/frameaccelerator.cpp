//
// cranimation/frameaccelerator.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "frameaccelerator.h"

#include "animation.h"
#include "frame.h"

#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

#if CR_STATS
	PF_PAGE(crFrameAcceleratorPage, "cr FrameAccelerator");
	PF_GROUP(crFrameAcceleratorStats);
	PF_VALUE_FLOAT(crFrameAccel_FrameIdx, crFrameAcceleratorStats);
	PF_VALUE_FLOAT(crFrameAccel_FrameAnimIdx, crFrameAcceleratorStats);
	PF_VALUE_FLOAT(crFrameAccel_FrameSkelIdx, crFrameAcceleratorStats);
	PF_VALUE_FLOAT(crFrameAccel_FilterWeight, crFrameAcceleratorStats);
	PF_VALUE_FLOAT(crFrameAccel_ExprFrameIdx, crFrameAcceleratorStats);
	PF_VALUE_FLOAT(crFrameAccel_ExprSkelIdx, crFrameAcceleratorStats);
	PF_LINK(crFrameAcceleratorPage, crFrameAcceleratorStats);
#endif // CR_STATS
	
////////////////////////////////////////////////////////////////////////////////

crAccelerator::crAccelerator()
: m_Slots(NULL)
, m_NumSlots(0)
, m_Stamp(0)
, m_Buffer(NULL)
{
	Init(1, 0);
}

////////////////////////////////////////////////////////////////////////////////

crAccelerator::~crAccelerator()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crAccelerator::Init(u32 numSlots, u32 heapSize)
{
	delete [] m_Slots;
	m_Slots = rage_new Entry*[numSlots];
	sysMemSet(m_Slots, 0, numSlots*sizeof(Entry*));
	m_NumSlots = numSlots;

	if(heapSize)
	{
		u8* buffer = rage_aligned_new(16) u8[heapSize];
		m_Heap.Init(buffer, heapSize);
		delete [] m_Buffer;
		m_Buffer = buffer;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAccelerator::Shutdown()
{
	u32 numSlots = m_NumSlots;
	for(u32 i=0; i < numSlots; i++)
	{
		Entry* entry = m_Slots[i];
		while(entry)
		{
			Entry* nextEntry = entry->m_Next;
			FreeEntry(entry);
			entry = nextEntry;
		}
	}
	delete [] m_Slots;
	m_Slots = NULL;
	m_NumSlots = 0;

	delete [] m_Buffer;
	m_Buffer = NULL;
}

////////////////////////////////////////////////////////////////////////////////

u32 crAccelerator::GetSlotIdx(u64 signature) const
{
	u64 v = signature * 3935559000370003845ull + 2691343689449507681ull;
	v ^= v >> 21ull;
	v ^= v << 37ull;
	v ^= v >> 4ull;
	v *= 4768777513237032717ull;
	v ^= v << 20ull;
	v ^= v >> 41ull;
	v ^= v << 5ull;
	return u32(v % m_NumSlots);
}

////////////////////////////////////////////////////////////////////////////////

void crAccelerator::FreeEntry(Entry* entry)
{
	if(m_Heap.IsValidPointer(entry))
	{
		m_Heap.Free(entry);
	}
	else
	{
		delete entry;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAccelerator::RemoveEntry(u32 size)
{
	Entry* remove = NULL;
	Entry** removePrevious = NULL;

	Entry** previous = NULL;
	u32 stampLru = UINT_MAX;
	u32 numSlots = m_NumSlots;
	for(u32 i=0; i < numSlots; i++)
	{
		Entry* entry = m_Slots[i];
		previous = &m_Slots[i];
		while(entry)
		{
			if(!entry->m_Lock && entry->m_Size >= size && stampLru >= entry->m_Stamp)
			{
				remove = entry;
				removePrevious = previous;
				stampLru = entry->m_Stamp;
			}
			previous = &entry->m_Next;
			entry = entry->m_Next;
		}
	}

	if(remove)
	{
		*removePrevious = remove->m_Next;
		FreeEntry(remove);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAccelerator::Flush()
{
	Entry** previous = NULL;
	u32 numSlots = m_NumSlots;

	for(u32 i=0; i < numSlots; i++)
	{
		Entry* entry = m_Slots[i];
		previous = &m_Slots[i];
		while(entry)
		{
			if(!entry->m_Lock)
			{
				Entry* nextEntry = entry->m_Next;
				*previous = nextEntry;
				FreeEntry(entry);
				entry = nextEntry;
			}
			else
			{
				previous = &entry->m_Next;
				entry = entry->m_Next;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAccelerator::GetEntry(u64 signature, u32 size, CalcFn calcFn, void* calcData, Lock& outLock)
{
	Assert(signature);
	if(!FindInternal(signature, outLock))
	{
		InsertInternal(signature, size, calcFn, calcData, outLock);
	}
}

////////////////////////////////////////////////////////////////////////////////

#if CR_STATS
size_t crAccelerator::GetTotalHeapSize() const
{
	return m_Heap.GetHeapSize();
}

////////////////////////////////////////////////////////////////////////////////

u32 crAccelerator::CalcLockedHeapSize() const
{
	u32 lockedHeapSize = 0;
	
	u32 numSlots = m_NumSlots;

	for(u32 i=0; i < numSlots; i++)
	{
		Entry* entry = m_Slots[i];
		while(entry)
		{
			if(entry->m_Lock)
			{
				lockedHeapSize += u32(sizeof(Entry)) + entry->m_Size;
			}
			entry = entry->m_Next;
		}
	}
	
	return lockedHeapSize;
}
#endif // CR_STATS

////////////////////////////////////////////////////////////////////////////////

bool crAccelerator::FindInternal(u64 signature, Lock& outLock) const
{
	u32 slotIdx = GetSlotIdx(signature);
	Entry* entry = m_Slots[slotIdx];
	while(entry)
	{
		if(entry->m_Signature == signature)
		{
			entry->m_Stamp = ++m_Stamp;
			outLock.Set(entry);
			return true;
		}
		entry = entry->m_Next;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crAccelerator::InsertInternal(u64 signature, u32 size, CalcFn calcFn, void* calcData, Lock& outLock)
{
	u32 alignedSize = RAGE_ALIGN(size, 4);
	u32 totalSize = sizeof(Entry) + alignedSize;
	void* buffer = m_Heap.Allocate(totalSize);

	// if it failed, remove one entry of required size
	if(!buffer)
	{
		RemoveEntry(size);
		buffer = m_Heap.Allocate(totalSize);
	}

	// if it failed again, flush the cache
	if(!buffer)
	{
		Flush();
		buffer = m_Heap.Allocate(totalSize);
	}

	// fall back to regular allocator
	if(!buffer)
	{
		buffer = rage_aligned_new(16) u8[totalSize];
	}

	Entry* entry = static_cast<Entry*>(buffer);
	entry->m_Lock = 0;
	entry->m_Size = alignedSize;
	entry->m_Signature = signature;

	u8* data = reinterpret_cast<u8*>(entry + 1);
	ASSERT_ONLY(u32 checkSize =) calcFn(calcData, data);
	Assertf(checkSize <= alignedSize, "Used more memory than was allocated");

	u32 slotIdx = GetSlotIdx(signature);
	entry->m_Next = m_Slots[slotIdx];
	m_Slots[slotIdx] = entry;

	outLock.Set(entry);
}

////////////////////////////////////////////////////////////////////////////////

crAccelerator::Lock::Lock()
: m_Entry(NULL)
, m_Size(0)
{
}

////////////////////////////////////////////////////////////////////////////////

crAccelerator::Lock::Lock(const Lock& lock)
: m_Entry(NULL)
{
	Set(lock.m_Entry);
}

////////////////////////////////////////////////////////////////////////////////

void crAccelerator::Lock::operator=(const Lock& lock)
{
	Set(lock.m_Entry);
}

////////////////////////////////////////////////////////////////////////////////

crAccelerator::Lock::~Lock()
{
	Unlock();
}

////////////////////////////////////////////////////////////////////////////////

void crAccelerator::Lock::Set(crAccelerator::Entry* entry)
{
	if(entry != m_Entry)
	{
		if(entry)
		{
			m_Size = entry->m_Size;
			sysInterlockedIncrement(&entry->m_Lock);
		}
		else
		{
			m_Size = 0;
		}
		if(m_Entry)
		{
			sysInterlockedDecrement(&m_Entry->m_Lock);
		}
		m_Entry = entry;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crAccelerator::Lock::Unlock()
{
	Set(NULL);
}

////////////////////////////////////////////////////////////////////////////////

crFrameAccelerator::crFrameAccelerator()
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameAccelerator::~crFrameAccelerator()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameAccelerator::Shutdown()
{
	for(u32 i=0; i < kNumAccelerators; i++)
	{
		m_Accelerators[i].Shutdown();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameAccelerator::Init(const u32* numSlots, const u32* heapSizes)
{
	for(u32 i=0; i < kNumAccelerators; i++)
	{
		m_Accelerators[i].Init(numSlots[i], heapSizes[i]);
	}
}

////////////////////////////////////////////////////////////////////////////////

struct CalcFrameIndicesCallbackData
{
	const crFrameData* m_FrameData;
	const crFrameData* m_SrcFrameData;
};

////////////////////////////////////////////////////////////////////////////////

static u32 CalcFrameIndicesCallback(void* calcData, u8* buffer)
{
	CalcFrameIndicesCallbackData& cbData = *reinterpret_cast<CalcFrameIndicesCallbackData*>(calcData);
	return crFrameAccelerator::CalcFrameIndices(*cbData.m_FrameData, *cbData.m_SrcFrameData, reinterpret_cast<u16*>(buffer));
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameAccelerator::GetSizeFrameIndices(u32 numDofs)
{
	return (numDofs*2 + kFormatTypeNum*6 + 8)*sizeof(u16);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameAccelerator::FindFrameIndices(crLock& outLock, const crFrameData& frameData, const crFrameData& srcFrameData)
{
	u64 sigFrame = frameData.GetSignature();
	u64 sigSrcFrame = srcFrameData.GetSignature();
	Assert(sigFrame && sigSrcFrame);

	u64 sig = (sigFrame << 32) | sigSrcFrame;
	CalcFrameIndicesCallbackData data;
	data.m_FrameData = &frameData;
	data.m_SrcFrameData = &srcFrameData;

	m_Accelerators[kFrameIndices].GetEntry(sig, GetSizeFrameIndices(srcFrameData.GetNumDofs()), CalcFrameIndicesCallback, &data, outLock);

#if CR_STATS
	PF_SET(crFrameAccel_FrameIdx, 100.f * float(m_Accelerators[kFrameIndices].CalcLockedHeapSize()) / float(m_Accelerators[kFrameIndices].GetTotalHeapSize()));
#endif // CR_STATS
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameAccelerator::CalcFrameIndices(const crFrameData& frameData, const crFrameData& srcFrameData, u16* indices)
{
	const u32 numDofs = frameData.GetNumDofs();
	const u32 numSrcDofs = srcFrameData.GetNumDofs();
	u16* startIndices = indices;
	indices += 8;

	for(u32 i=0; i < kFormatTypeNum; i++)
	{
		u16* firstIndex = indices;

		// for each format size find matching frame offsets
		u32 srcDofIdx = 0;
		for(u32 dofIdx=0; dofIdx<numDofs; ++dofIdx)
		{
			const crFrameData::Dof& dof = frameData.m_Dofs[dofIdx];
			if(dof.m_Type==i && srcDofIdx<numSrcDofs)
			{
				do
				{
					const crFrameData::Dof& srcDof = srcFrameData.m_Dofs[srcDofIdx];
					if(srcDof == dof && dof.m_Type == srcDof.m_Type)
					{
						indices[0] = srcDof.m_Offset;
						indices[1] = dof.m_Offset;
						indices += 2;
						break;
					}
					else if(srcDof > dof)
					{
						break;
					}
					else
					{
						if((++srcDofIdx)<numSrcDofs)
						{
							continue;
						}
					}
					break;
				}
				while(1);
			}
		}

		// add padding on multiple of 4
		startIndices[i] = u16((indices - firstIndex) / 2);
		while(u32(indices - firstIndex) % 8)
		{
			indices[0] = indices[-2];
			indices[1] = indices[-1];
			indices += 2;
		}
	}

	return u32(indices - startIndices)*sizeof(u16);
}

////////////////////////////////////////////////////////////////////////////////

struct CalcFrameAnimIndicesCallbackData
{
	const crFrameData* m_FrameData;
	const crAnimation** m_Anims;
	u32 m_NumAnims;
};

////////////////////////////////////////////////////////////////////////////////

static u32 CalcFrameAnimIndicesCallback(void* calcData, u8* buffer)
{
	CalcFrameAnimIndicesCallbackData& cbData = *reinterpret_cast<CalcFrameAnimIndicesCallbackData*>(calcData);
	u8* startBuffer = buffer;
	for(u32 i=0; i < cbData.m_NumAnims; i++)
	{
		buffer += crFrameAccelerator::CalcFrameAnimIndices(*cbData.m_FrameData, *cbData.m_Anims[i], reinterpret_cast<u16*>(buffer));
	}
	return u32(buffer - startBuffer);
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameAccelerator::GetSizeFrameAnimIndices(u32 numDofs)
{
	return (numDofs + 1)*sizeof(u16);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameAccelerator::FindFrameAnimIndices(crLock& outLock, const crFrameData& frameData, u32 numAnims, const crAnimation** anims)
{
	u64 sigFrame = frameData.GetSignature();

	const u32 rot = 7;
	u64 sigAnim = 0;
	u32 size = 0;
	for(u32 i=0; i < numAnims; i++)
	{
		sigAnim = anims[i]->GetSignature() ^ ((sigAnim<<rot)|(sigAnim>>(32-rot)));
		size += GetSizeFrameAnimIndices(anims[i]->GetNumDofs());
	}
	Assert(sigFrame && sigAnim);

	u64 sig = (sigFrame << 32) | sigAnim;
	CalcFrameAnimIndicesCallbackData data;
	data.m_FrameData = &frameData;
	data.m_Anims = anims;
	data.m_NumAnims = numAnims;

	m_Accelerators[kFrameAnimIndices].GetEntry(sig, size, CalcFrameAnimIndicesCallback, &data, outLock);

#if CR_STATS
	PF_SET(crFrameAccel_FrameAnimIdx, 100.f * float(m_Accelerators[kFrameAnimIndices].CalcLockedHeapSize()) / float(m_Accelerators[kFrameAnimIndices].GetTotalHeapSize()));
#endif // CR_STATS
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameAccelerator::CalcFrameAnimIndices(const crFrameData& frameData, const crAnimation& anim, u16* indices)
{
	const u32 numFrameDofs = frameData.GetNumDofs();
	const crFrameData::Dof* frameDofs = frameData.GetBuffer();
	u16 writeOnlyOffset = u16(frameData.GetWriteOnlyOffset());

	u16* startIndices = indices;

	const u32 numDofs = anim.m_Dofs.GetCount();
	for(u32 i=0, j=0; i < numDofs; ++i)
	{
		const crAnimation::Dof& animDof = anim.m_Dofs[i];
		u32 animTrackId = (u32(animDof.m_Track) << 16) | u32(animDof.m_Id);
		u16 frameOffset = writeOnlyOffset;
		for(; j < numFrameDofs; ++j)
		{
			const crFrameData::Dof& frameDof = frameDofs[j];
			u32 frameTrackId = frameDof.GetTrackId();
			if(frameTrackId == animTrackId && animDof.m_Type == frameDof.m_Type)
			{
				frameOffset = frameDof.m_Offset;
				++j;
				break;
			}
			else if(frameTrackId > animTrackId)
			{
				break;
			}
		}

		*indices++ = frameOffset;
	}

	*indices++ = writeOnlyOffset;
	return u32(indices - startIndices)*sizeof(u16);
}

////////////////////////////////////////////////////////////////////////////////

struct CalcFrameSkelIndicesCallbackData
{
	const crFrameData* m_FrameData;
	const crSkeletonData* m_SkelData;
	crFrameFilter* m_Filter;
};

////////////////////////////////////////////////////////////////////////////////

static u32 CalcFrameSkelIndicesCallback(void* calcData, u8* buffer)
{
	CalcFrameSkelIndicesCallbackData& cbData = *reinterpret_cast<CalcFrameSkelIndicesCallbackData*>(calcData);
	return crFrameAccelerator::CalcFrameSkelIndices(*cbData.m_FrameData, *cbData.m_SkelData, cbData.m_Filter, reinterpret_cast<u16*>(buffer));
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameAccelerator::GetSizeFrameSkelIndices(u32 numDofs)
{
	return (numDofs*2 + 3*6 + 8)*sizeof(u16);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameAccelerator::FindFrameSkelIndices(crLock& outLock, const crFrameData& frameData, const crSkeletonData& skelData, crFrameFilter* filter)
{
	u64 sigFrame = frameData.GetSignature();
	u64 sigSkel = skelData.GetSignature();
	Assert(sigFrame && sigSkel);

	u64 sig = (sigFrame << 32) | sigSkel;
	CalcFrameSkelIndicesCallbackData data;
	data.m_FrameData = &frameData;
	data.m_SkelData = &skelData;
	data.m_Filter = filter;

	if(filter)
	{
		const u32 rot = 7;
		u64 sigFilter = filter->GetSignature();
		Assert(sigFilter);
		sig = sig ^ ((sigFilter<<rot)|(sigFilter>>(32-rot)));
	}

	m_Accelerators[kFrameSkelIndices].GetEntry(sig, GetSizeFrameSkelIndices(frameData.GetNumDofs()), CalcFrameSkelIndicesCallback, &data, outLock);

#if CR_STATS
	PF_SET(crFrameAccel_FrameSkelIdx, 100.f * float(m_Accelerators[kFrameSkelIndices].CalcLockedHeapSize()) / float(m_Accelerators[kFrameSkelIndices].GetTotalHeapSize()));
#endif // CR_STATS
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameAccelerator::CalcFrameSkelIndices(const crFrameData& frameData, const crSkeletonData& skelData, crFrameFilter* filter, u16* indices)
{
	u32 numDofs = frameData.GetNumDofs();
	static const u32 tracks[] = { kTrackBoneTranslation, kTrackBoneRotation, kTrackBoneScale };
	static const u32 formats[] = { kFormatTypeVector3, kFormatTypeQuaternion, kFormatTypeVector3 };
	static const u32 flags[] = { crBoneData::TRANSLATE_X|crBoneData::TRANSLATE_Y|crBoneData::TRANSLATE_Z ,
					crBoneData::ROTATE_X|crBoneData::ROTATE_Y|crBoneData::ROTATE_Z,
					crBoneData::SCALE_X|crBoneData::SCALE_Y|crBoneData::SCALE_Z};

	u16* startIndices = indices;
	indices += 8;

	for(u32 i=0; i < NELEM(tracks); ++i)
	{
		u32 track = tracks[i];
		u32 format = formats[i];
		u32 flag = flags[i];

		u16* firstIndex = indices;

		for(u32 j=0; j < numDofs; ++j)
		{
			const crFrameData::Dof& dof = frameData.m_Dofs[j];

			float weight;
			if(filter && !filter->FilterDof(dof.m_Track, dof.m_Id, weight))
				continue;

			if(dof.m_Track == track && dof.m_Type == format)
			{
				int boneIdx;
				if(skelData.ConvertBoneIdToIndex(dof.m_Id, boneIdx))
				{
					const crBoneData& bd = *skelData.GetBoneData(boneIdx);
					if(bd.GetDofs() & flag)
					{
						indices[0] = dof.m_Offset;
						indices[1] = u16(boneIdx);
						indices += 2;
					}
				}
			}
		}

		// add padding on multiple of 4
		startIndices[i] = u16((indices - firstIndex) / 2);
		while(u32(indices - firstIndex) % 8)
		{
			indices[0] = indices[-2];
			indices[1] = indices[-1];
			indices += 2;
		}
	}

	return u32(indices - startIndices)*sizeof(u16);
}

////////////////////////////////////////////////////////////////////////////////

struct CalcFilterWeightsCallbackData
{
	const crFrameData* m_FrameData;
	crFrameFilter* m_Filter;
};

////////////////////////////////////////////////////////////////////////////////

static u32 CalcFilterWeightsCallback(void* calcData, u8* buffer)
{
	CalcFilterWeightsCallbackData& cbData = *reinterpret_cast<CalcFilterWeightsCallbackData*>(calcData);
	return crFrameAccelerator::CalcFilterWeights(*cbData.m_FrameData, *cbData.m_Filter, reinterpret_cast<f32*>(buffer));
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameAccelerator::GetSizeFilterWeights(u32 numDofs)
{
	return (numDofs + kFormatTypeNum*3)*sizeof(f32);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameAccelerator::FindFilterWeights(crLock& outLock, const crFrameData& frameData, crFrameFilter& filter)
{
	u64 sigFrame = frameData.GetSignature();
	u64 sigFilter = filter.GetSignature();
	Assert(sigFrame && sigFilter);
	
	u64 sig = (sigFrame << 32) | sigFilter;
	CalcFilterWeightsCallbackData data;
	data.m_FrameData = &frameData;
	data.m_Filter = &filter;

	m_Accelerators[kFilterWeights].GetEntry(sig, GetSizeFilterWeights(frameData.GetNumDofs()), CalcFilterWeightsCallback, &data, outLock);

#if CR_STATS
	PF_SET(crFrameAccel_FilterWeight, 100.f * float(m_Accelerators[kFilterWeights].CalcLockedHeapSize()) / float(m_Accelerators[kFilterWeights].GetTotalHeapSize()));
#endif // CR_STATS
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameAccelerator::CalcFilterWeights(const crFrameData& frameData, crFrameFilter& filter, f32* weights)
{
	float* startWeight = weights;
	u32 numDofs = frameData.GetNumDofs();
	for(u32 i=0; i < kFormatTypeNum; i++)
	{
		float* firstWeight = weights;
		for(u32 j=0; j<numDofs; j++)
		{
			const crFrameData::Dof& dof = frameData.GetDof(j);
			if(dof.m_Type == i)
			{
				float weight = 1.f;
				if(!filter.FilterDof(dof.m_Track, dof.m_Id, weight))
				{
					weight = 0.f;
				}

				*weights++ = weight;
			}
		}

		// add padding on multiple of 4
		u32 count = u32(weights - firstWeight);
		while(count % 4)
		{
			*weights++ = 0.f;
			count++;
		}
	}

	return u32(weights - startWeight)*sizeof(f32);
}

////////////////////////////////////////////////////////////////////////////////

};  // namespace rage
