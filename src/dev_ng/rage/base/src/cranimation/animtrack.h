//
// cranimation/animtrack.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMTRACK_H
#define CRANIMATION_ANIMTRACK_H

#include "animation_config.h"

#if CR_DEV
#include "animchannel.h"
#include "animchunk.h"
#include "animtolerance.h"

#include "atl/array.h"
#include "atl/array_struct.h"
#include "data/struct.h"
#include "vectormath/quatv.h"
#include "vectormath/vec3v.h"

namespace rage
{

class crAnimChunk;
class crArchive;


// PURPOSE: Internal storage class of crAnimation.
// Tracks represent change in single value, which may be of type
// float/vector3/quaternion etc over the entire duration of an animation.
// Internally tracks hold their values in a series of one of more chunks,
// which then in turn compress their data within one or more channels.
// Chunking and compressing of data is deliberately hidden from the end user.
class crAnimTrack
{
	friend class crAnimation;

public:

	// PURPOSE: Default constructor
	crAnimTrack();

	// PURPOSE: Constructor
	crAnimTrack(u8 track, u16 id);

	// PURPOSE: Destructor
	~crAnimTrack();

	// PURPOSE: Shutdown, free/release dynamic resources
	void Shutdown();

	// PURPOSE: Serialization
	void Serialize(crArchive& a);

	// PURPOSE: Clone the track
	crAnimTrack* Clone() const;

	// PURPOSE: Get track index
	// RETURNS: Track index (see crAnimTrack::TRACK_XXX for enumeration of known track indices)
	u8 GetTrack() const;

	// PURPOSE: Get track id
	// RETURNS: Track id (meaning entirely depends on track index)
	u16 GetId() const;

	// PURPOSE: Set track id when cloning tracks
	void SetId(u16 id);

	// PURPOSE: Get track type
	// RETURNS: Track type (see crAnimTrack::FORMAT_TYPE_XXX for enumeration of known track types)
	u8 GetType() const;


	// PURPOSE: Test track index and id
	// PARAMS: track - track index
	// id - track id
	// RETURNS: true - if track index and id match
	bool TestTrackAndId(u8 track, u16 id) const;

	// PURPOSE: Test track index and id
	// PARAMS: track - track index
	// id - track id
	// outMissedInOrderedList - if not a match, returns true if missed index/id in ordered list
	// RETURNS: true - if track index and id match
	bool TestTrackAndId(u8 track, u16 id, bool& outMissedInOrderedList) const;


	// PURPOSE: Get the number of chunks contained by this track
	// RETURNS: Number of chunks
	u32 GetNumChunks() const;

	// PURPOSE: Get chunk by chunk index (const)
	// PARAMS: chunkIndex - chunk index [0..numChunks-1]
	// RETURNS: const pointer to chunk (will assert on bad index)
	const crAnimChunk* GetChunk(u32 chunkIdx) const;

	// PURPOSE: Get chunk by chunk index (non-const)
	// PARAMS: chunkIndex - chunk index [0..numChunks-1]
	// RETURNS: non-const pointer to chunk (will assert on bad index)
	crAnimChunk* GetChunk(u32 chunkIdx);

	
	// PURPOSE: Returns number of internal frames per chunk (for tools/debugging only, not for runtime use).
	// NOTES: The internal frames may not be 30Hz frames (or even constant frequency).
	// There is always an extra terminating frame (of zero duration) on the end of a chunk.
	// This is NOT included in this count.
	u32 GetNumInternalFramesPerChunk() const;


	// PURPOSE: Evaluate vector3 track
	// PARAMS: t - time (in internal animation frames)
	// outVector3 - vector3 at this time
	// NOTES: Will assert if called on a track that is not of vector3 type
	void EvaluateVector3(float t, Vec3V_InOut outVector3) const;

	// PURPOSE: Evaluate quaternion track
	// PARAMS: t - time (in internal animation frames)
	// outQuaternion - quaternion at this time
	// NOTES: Will assert if called on a track that is not of quaternion type
	void EvaluateQuaternion(float t, QuatV_InOut outQuaternion) const;

	// PURPOSE: Evaluate float track
	// PARAMS: t - time (in internal animation frames)
	// outFloat - float at this time
	// NOTES: Will assert if called on a track that is not of float type
	void EvaluateFloat(float t, float& outFloat) const;


	// PURPOSE: Create vector3 track
	// PARAMS: vectors - array of vector3 values
	// tolerance - compression tolerance control
	// anim - optional pointer to parent animation
	void CreateVector3(atArray<Vec3V>& vectors, const crAnimTolerance& tolerance=DEFAULT_ANIM_TOLERANCE, u32 framesPerChunk=sm_DefaultFramesPerChunk);

	// PURPOSE: Create quaternion track
	// PARAMS: quaternions - array of quaternion values
	// tolerance - compression tolerance control
	// anim - optional pointer to parent animation
	void CreateQuaternion(atArray<QuatV>& quaternions, const crAnimTolerance& tolerance=DEFAULT_ANIM_TOLERANCE, u32 framesPerChunk=sm_DefaultFramesPerChunk);

	// PURPOSE: Create float track
	// PARAMS: floats - array of float values
	// tolerance - compression tolerance control
	// anim - optional pointer to parent animation
	void CreateFloat(atArray<float>& floats, const crAnimTolerance& tolerance=DEFAULT_ANIM_TOLERANCE, u32 framesPerChunk=sm_DefaultFramesPerChunk);

	// PURPOSE: Create track
	// PARAMS: values - array of values
	// tolerance - compression tolerance control
	// anim - optional pointer to parent animation
	template<typename _T>
	void Create(atArray<_T>& values, const crAnimTolerance& tolerance=DEFAULT_ANIM_TOLERANCE, u32 framesPerChunk=sm_DefaultFramesPerChunk);


	// PURPOSE: Copy track
	// PARAMS: track - source track
	// NOTE: Values from source will be cloned, with existing compression algorithms.
	// Type and other internal settings are also copied from source, 
	// but track/id identifiers are *not* copied.
	void CopyTrack(const crAnimTrack& track);


	// PURPOSE: Compute total storage use
	// RETURNS: Storage used, in bytes
	int ComputeSize() const;


	// PURPOSE: Convert track index string to track index
	// PARAMS: trackIndexName - track index string
	// outTrackIndex - track index (if successful)
	// RETURNS: true - if conversion successful
	// NOTES: Matches first part of the track name with the [hard coded] known track
	// names.  If this matches, it converts any remaining portion of the track name
	// into an integer and adds this to the track index as an offset.
	// (ie knownTrack == TRACK_KNOWN, knownTrack02 == TRACK_KNOWN+2)
	static bool ConvertTrackNameToIndex(const char* trackIndexName, u8& outTrackIndex);

	// PURPOSE: Convert track index to name
	// PARAMS: trackIndex - track index (see crAnimTrack::TRACK_XXX)
	// RETURNS: trackName, NULL if unknown
	// NOTES: Doesn't handle project tracks, or undocumented tracks - all return NULL.
	static const char* ConvertTrackIndexToName(u8 trackIndex);

	// PURPOSE: Convert bone name string to id (via hashing)
	// PARAMS: boneIdName - bone name string
	// RETURNS: bone id hash value
	// NOTES: If the string starts with a '#' what follows is treated as a number and
	// returned.  Otherwise the string is hashed (the hash guarantees not to
	// return 0, or other low numeric values - preserving these for use by the
	// manual numbering system).
	static u16 ConvertBoneNameToId(const char* boneIdName);

	// PURPOSE: Convert mover name string to id (via hashing)
	// PARAMS: moverIdName - mover name string
	// RETURNS: mover id hash value
	// NOTES: See ConvertBoneNameToId for information on hashing system
	static u16 ConvertMoverNameToId(const char* moverIdName);

	// PURPOSE: Default internal frames per chunk (should match value found in crAnimation)
	static const u32 sm_DefaultFramesPerChunk = 127;


protected:

	// PURPOSE: Prevent use of the copy constructor
	crAnimTrack(const crAnimTrack&) {}

	// PURPOSE: Internal function, clear currently stored data
	void Reset();

	// PURPOSE: Calculate the number of chunks, given a number of frames
	u32 CalcNumChunks(u32 numFrames) const;


private:

	u8 m_Track;
	u8 m_Type;
	u16 m_Id;

	u16 m_FramesPerChunk;
	u16 m_Flags;

	atArray< crAnimChunk* > m_Chunks;
};


// inline functions

////////////////////////////////////////////////////////////////////////////////

inline u8 crAnimTrack::GetTrack() const
{
	return m_Track;
}

////////////////////////////////////////////////////////////////////////////////

inline u16 crAnimTrack::GetId() const
{
	return m_Id;
}

////////////////////////////////////////////////////////////////////////////////

inline u8 crAnimTrack::GetType() const
{
	return m_Type;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimTrack::TestTrackAndId(u8 track, u16 id) const
{
	return (track == m_Track) && (id == m_Id);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimTrack::TestTrackAndId(u8 track, u16 id, bool& outMissedInOrderedList) const
{
	u32 thisTrackId = (m_Track << 16) | m_Id;
	u32 otherTrackId = (track << 16) | id;

	outMissedInOrderedList = (thisTrackId > otherTrackId);
	return thisTrackId == otherTrackId;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimTrack::GetNumChunks() const
{
	return m_Chunks.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline const crAnimChunk* crAnimTrack::GetChunk(u32 chunkIdx) const
{
	return m_Chunks[chunkIdx];
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimChunk* crAnimTrack::GetChunk(u32 chunkIdx)
{
	return m_Chunks[chunkIdx];
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimTrack::GetNumInternalFramesPerChunk() const
{
	return m_FramesPerChunk;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimTrack::CalcNumChunks(u32 numFrames) const
{
	if(numFrames > 1)
	{
		return ((numFrames-2)/m_FramesPerChunk) + 1;
	}
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline void crAnimTrack::Create(atArray<Vec3V>& values, const crAnimTolerance& tolerance, u32 framesPerChunk)
{
	CreateVector3(values, tolerance, framesPerChunk);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline void crAnimTrack::Create(atArray<QuatV>& values, const crAnimTolerance& tolerance, u32 framesPerChunk)
{
	CreateQuaternion(values, tolerance, framesPerChunk);
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline void crAnimTrack::Create(atArray<float>& values, const crAnimTolerance& tolerance, u32 framesPerChunk)
{
	CreateFloat(values, tolerance, framesPerChunk);
}

////////////////////////////////////////////////////////////////////////////////


} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_ANIMTRACK_H


