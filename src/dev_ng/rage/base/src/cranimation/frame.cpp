//
// cranimation/frame.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "frame.h"

#include "animtrack.h"
#include "frameinitializers.h"
#include "frameiterators.h"

#include "crmetadata/dumpoutput.h"
#include "crskeleton/jointdata.h"
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "data/safestruct.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crFrame::crFrame()
: m_RefCount(1)
, m_FrameData(NULL)
, m_Accelerator(NULL)
, m_Buffer(NULL)
, m_BufferSize(0)
, m_BufferOwner(false)
, m_Signature(0)
{
	CompileTimeAssert(!OffsetOf(crFrame, m_Buffer));
}

////////////////////////////////////////////////////////////////////////////////

crFrame::crFrame(const crFrameData& frameData, u8* buffer, u32 bufferSize, bool bufferOwner, bool invalidate)
: m_RefCount(1)
, m_FrameData(NULL)
, m_Accelerator(NULL)
, m_Buffer(NULL)
, m_BufferSize(0)
, m_BufferOwner(false)
, m_Signature(0)
{
	Init(frameData, buffer, bufferSize, bufferOwner, invalidate);
}

////////////////////////////////////////////////////////////////////////////////

crFrame::crFrame(const crFrame& src)
: m_RefCount(1)
, m_FrameData(NULL)
, m_Accelerator(NULL)
, m_Buffer(NULL)
, m_BufferSize(0)
, m_BufferOwner(false)
, m_Signature(0)
{
	*this = src;
}

////////////////////////////////////////////////////////////////////////////////

crFrame::crFrame(datResource&)
{
	Assert(0);	// TODO - write me!
}

////////////////////////////////////////////////////////////////////////////////

crFrame::~crFrame()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::Init(const crFrameData& frameData, u8* buffer, u32 bufferSize, bool bufferOwner, bool invalidate)
{
	Assert(buffer?(bufferSize>0):(bufferSize==0));
	Assert16(buffer);
	Assert(!m_FrameData);

	SetFrameData(&frameData);

	if(buffer)
	{
		Assert(bufferSize >= m_FrameData->GetFrameBufferSize());
		Assert((bufferSize & 15) == 0);

		m_Buffer = buffer;
		m_BufferOwner = bufferOwner;
		Assign(m_BufferSize, bufferSize);
	}
	else
	{
		m_BufferOwner = true;
		Assign(m_BufferSize, m_FrameData->GetFrameBufferSize());

		m_Buffer = rage_aligned_new(16) u8[m_BufferSize];
		Assert(m_Buffer);
	}

	if(invalidate)
	{
		FastInvalidate();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::Shutdown()
{
	m_Accelerator = NULL;

	if(m_Buffer)
	{
		if(m_BufferOwner)
		{
			delete [] m_Buffer;
		}

		m_Buffer = NULL;
		m_BufferSize = 0;
		m_BufferOwner = false;
	}

	SetFrameData(NULL);
}

////////////////////////////////////////////////////////////////////////////////

const crFrame& crFrame::operator=(const crFrame& src)
{
	Shutdown();

	if(src.m_FrameData)
	{
		Assert(src.m_FrameData->GetSignature() == src.m_Signature);

		Init(*src.m_FrameData);

		FastSet(src);
	}
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crFrame);

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crFrame::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crFrame)
	SSTRUCT_FIELD_VP(crFrame, m_Buffer)
	SSTRUCT_FIELD_VP(crFrame, m_FrameData)
	SSTRUCT_FIELD_VP(crFrame, m_Accelerator)
	SSTRUCT_FIELD(crFrame, m_RefCount)
	SSTRUCT_FIELD(crFrame, m_Signature)
	SSTRUCT_FIELD(crFrame, m_BufferSize)
	SSTRUCT_FIELD(crFrame, m_BufferOwner)
	SSTRUCT_IGNORE(crFrame, m_Padding)
	SSTRUCT_END(crFrame)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

bool crFrame::ExchangeFrameData(const crFrameData& frameData)
{
	Assert(&frameData != m_FrameData);
	if(!m_FrameData)
	{
		Init(frameData);
	}
	else
	{
		const u32 newBufferSize = frameData.GetFrameBufferSize();
		if(!m_BufferOwner && m_BufferSize < newBufferSize)
		{
			// if not buffer owner, and buffer insufficient, can't exchange frame data
			return false;
		}

		u8* newBuffer = NULL;
		const bool newBufferAllocated = m_BufferOwner && m_BufferSize < newBufferSize;
		if(newBufferAllocated)
		{
			// if buffer owner, and current buffer insufficient - allocate new buffer
			newBuffer = rage_aligned_new(16) u8[newBufferSize];
			Assert(newBuffer);
		}
		else
		{
			// otherwise, create temporary buffer on stack
			newBuffer = Alloca(u8, newBufferSize);
		}

		// construct temporary frame, and copy values across
		crFrame tempFrame;
		tempFrame.Init(frameData, newBuffer, newBufferSize, false);
		tempFrame.Set(*this);

		if(newBufferAllocated)
		{
			// if new buffer was replacement, delete old buffer and swap over
			delete [] m_Buffer;
			m_Buffer = newBuffer;
			Assign(m_BufferSize, newBufferSize);
		}
		else
		{
			// or, if buffer was temporary, copy contents back to original
			sysMemCpy(m_Buffer, newBuffer, newBufferSize);
		}

		SetFrameData(&frameData);
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

class SetIterator : public crFrameIteratorByType<SetIterator>
{
public:
	SetIterator(crFrame& frame)
	: crFrameIteratorByType<SetIterator>(frame)
	{
	}

	template<typename _T>
	__forceinline void IterateDofsByType(const crFrameData::Dof&, crFrame::Dof& dest, const crFrame::Dof& src, float)
	{
		dest.Set<_T>(src.GetUnsafe<_T>());
	}
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::Set(const crFrame& frame, crFrameFilter* filter)
{
	if(filter || m_Signature != frame.m_Signature)
	{
		SetIterator it(*this);
		it.IteratePair(frame, filter, 1.f, true);
	}
	else if(m_FrameData)
	{
		FastSet(frame);
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void Interpolate(Vec3V_InOut dest, Vec3V_In src, float weight)
{
	dest = Lerp(ScalarVFromF32(weight), dest, src);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void Interpolate(QuatV_InOut dest, QuatV_In src, float weight)
{
	dest = PrepareSlerp(src, dest);
	dest = Nlerp(ScalarVFromF32(weight), dest, src);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void Interpolate(float& dest, const float& src, float weight)
{
	dest = Lerp(weight, dest, src);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void Interpolate(int& dest, const int& src, float weight)
{
	if(weight >= 0.5f)
	{
		dest = src;
	}
}

////////////////////////////////////////////////////////////////////////////////

template<bool mergeBlend, bool clampWeight>
class BlendIterator : public crFrameIteratorByType<BlendIterator<mergeBlend, clampWeight> >
{
public:
	BlendIterator(crFrame& frame)
		: crFrameIteratorByType<BlendIterator<mergeBlend, clampWeight> >(frame)
	{
	}

	template<typename _T>
	void IterateDofsByType(const crFrameData::Dof&, crFrame::Dof&, const crFrame::Dof&, float);
};

////////////////////////////////////////////////////////////////////////////////

template<bool mergeBlend, bool clampWeight>
template<typename _T>
__forceinline void BlendIterator<mergeBlend, clampWeight>::IterateDofsByType(const crFrameData::Dof&, crFrame::Dof& dest, const crFrame::Dof& src, float weight)
{
	if(clampWeight)
	{
		weight = Clamp(weight, 0.f, 1.f);
	}

	if(mergeBlend || !src.IsInvalid())
	{
		if(dest.IsInvalid())
		{
			dest.Set<_T>(src.GetUnsafe<_T>());
		}
		else
		{
			Interpolate(dest.GetUnsafe<_T>(), src.GetUnsafe<_T>(), weight);
		}
	}
	else if((weight > (1.f-WEIGHT_ROUNDING_ERROR_TOLERANCE)))
	{
		dest.SetInvalid(true);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::Blend(float weight, const crFrame& frame, bool mergeBlend, crFrameFilter* filter)
{
	weight = Clamp(weight, 0.f, 1.f);
	if(mergeBlend)
	{
		if(filter)
		{
			BlendIterator<true, true> it(*this);
			it.IteratePair(frame, filter, weight, mergeBlend);
		}
		else
		{
			BlendIterator<true, false> it(*this);
			it.IteratePair(frame, filter, weight, mergeBlend);
		}
	}
	else
	{
		if(filter)
		{
			BlendIterator<false, true> it(*this);
			it.IteratePair(frame, filter, weight, mergeBlend);
		}
		else
		{
			BlendIterator<false, false> it(*this);
			it.IteratePair(frame, filter, weight, mergeBlend);
		}
	}	
}

////////////////////////////////////////////////////////////////////////////////

template<bool addSet>
class AddIterator : public crFrameIteratorByType<AddIterator<addSet> >
{
public:
	AddIterator(crFrame& frame)
		: crFrameIteratorByType<AddIterator<addSet> >(frame)
	{
	}

	__forceinline void Add(Vec3V_InOut dest, Vec3V_In src, float weight)
	{
		dest = AddScaled(dest, src, ScalarVFromF32(weight));
	}

	__forceinline void Add(QuatV_InOut dest, QuatV_In src, float weight)
	{
		if(weight == 1.f)
		{
			dest = Multiply(dest, src);
		}
		else if(weight == -1.f)
		{
			dest = Multiply(dest, InvertNormInput(src));
		}
		else
		{
			ScalarV w = ScalarVFromF32(weight);
			QuatV q0 = QuatVScaleAngle(src, -w);
			QuatV q1 = QuatVScaleAngle(src, w);
			QuatV dest0 = Multiply(dest, InvertNormInput(q0));
			QuatV dest1 = Multiply(q1, dest);
			dest = SelectFT(IsLessThan(w, ScalarV(V_ZERO)), dest1, dest0);
		}
	}

	__forceinline void Add(float& dest, const float& src, float weight)
	{
		dest += src * weight;
	}

	__forceinline void Add(int& dest, const int& src, float weight)
	{
		if(fabs(weight) > WEIGHT_ROUNDING_ERROR_TOLERANCE)
		{
			if(fabs(weight-1.f) > WEIGHT_ROUNDING_ERROR_TOLERANCE)
			{
				dest += s32(float(src) * weight);
			}
			else
			{
				dest += src;
			}
		}
	}

	template<typename _T>
	__forceinline void IterateDofsByType(const crFrameData::Dof&, crFrame::Dof& dest, const crFrame::Dof& src, float weight)
	{
		if(addSet && dest.IsInvalid())
		{
			dest.Set<_T>(src.GetUnsafe<_T>());
		}
		else if(!src.IsInvalid())
		{
			Add(dest.GetUnsafe<_T>(), src.GetUnsafe<_T>(), weight);
		}
	}
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::Add(float weight, const crFrame& frame, crFrameFilter* filter)
{
	AddIterator<false> it(*this);
	it.IteratePair(frame, filter, weight, true);
}

////////////////////////////////////////////////////////////////////////////////

class MergeIterator : public crFrameIteratorByType<MergeIterator>
{
public:
	MergeIterator(crFrame& frame)
		: crFrameIteratorByType<MergeIterator>(frame)
	{
	}

	template<typename _T>
	__forceinline void IterateDofsByType(const crFrameData::Dof&, crFrame::Dof& dest, const crFrame::Dof& src, float)
	{
		if(dest.IsInvalid())
		{
			dest.Set<_T>(src.GetUnsafe<_T>());
		}
	}
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::Merge(const crFrame& frame, crFrameFilter* filter)
{
	MergeIterator it(*this);
	it.IteratePair(frame, filter, 1.f, true);
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::Subtract(float weight, const crFrame& frame, crFrameFilter* filter)
{
	Add(-weight, frame, filter);
}

////////////////////////////////////////////////////////////////////////////////

class MultiplyIterator : public crFrameIteratorByType<MultiplyIterator>
{
public:
	MultiplyIterator(crFrame& frame)
		: crFrameIteratorByType<MultiplyIterator>(frame)
	{
	}

	template<typename _T>
	void IterateDofByType(const crFrameData::Dof&, crFrame::Dof&, float);
};

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void MultiplyIterator::IterateDofByType<Vec3V>(const crFrameData::Dof&, crFrame::Dof& dest, float weight)
{
	dest.GetUnsafe<Vec3V>() *= ScalarVFromF32(weight);
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void MultiplyIterator::IterateDofByType<QuatV>(const crFrameData::Dof&, crFrame::Dof& dest, float weight)
{
	dest.GetUnsafe<QuatV>() = QuatVScaleAngle(dest.GetUnsafe<QuatV>(), ScalarVFromF32(weight));
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void MultiplyIterator::IterateDofByType<float>(const crFrameData::Dof&, crFrame::Dof& dest, float weight)
{
	dest.GetUnsafe<float>() *= weight;
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void MultiplyIterator::IterateDofByType<int>(const crFrameData::Dof&, crFrame::Dof& dest, float weight)
{
	dest.GetUnsafe<int>() = s32(float(dest.GetUnsafe<int>()) * weight);
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::Multiply(float weight, crFrameFilter* filter)
{
	MultiplyIterator it(*this);
	it.Iterate(filter, weight, true);
}

////////////////////////////////////////////////////////////////////////////////

class MirrorIterator : public crFrameIterator<MirrorIterator>
{
public:
	MirrorIterator(crFrame& frame, const crSkeletonData& skelData)
		: crFrameIterator<MirrorIterator>(frame)
		, m_SkelData(&skelData)
	{
	}
	
	enum eMirrorPlane
	{
		kMirrorPlaneYZ=0,
		kMirrorPlaneXZ,
		kMirrorPlaneXY,
		kMirrorPlaneCount,
	};


	__forceinline void Mirror(Vec3V_InOut v, eMirrorPlane plane=kMirrorPlaneXY)
	{
		const Vec3V scales[kMirrorPlaneCount] = {Vec3V(-1.f,1.f,1.f), Vec3V(1.f,-1.f,1.f), Vec3V(1.f,1.f,-1.f)};
		v = Scale(v, scales[plane]);
	}

	__forceinline void Mirror(QuatV_InOut q, eMirrorPlane plane=kMirrorPlaneXY)
	{
		const Vec4V scales[kMirrorPlaneCount] = {Vec4V(-1.f,1.f,1.f,-1.f), Vec4V(1.f,-1.f,1.f,-1.f), Vec4V(1.f,1.f,-1.f,-1.f)};
		q = Scale(q, scales[plane]);
	}

	void IterateDof(const crFrameData::Dof& dof, crFrame::Dof& dest, float)
	{
		switch(dof.m_Track)
		{
		case kTrackBoneRotation:
			{
				int boneIdx;
				if(m_SkelData->ConvertBoneIdToIndex(dof.m_Id, boneIdx))
				{
					const crBoneData* boneData = m_SkelData->GetBoneData(boneIdx);
					if(boneData->GetDofs() & (crBoneData::ROTATE_X|crBoneData::ROTATE_Y|crBoneData::ROTATE_Z))
					{
						QuatV mirror = Multiply(dest.GetUnsafe<QuatV>(), InvertNormInput(boneData->GetDefaultRotation()));

						eMirrorPlane plane = dof.m_Id?kMirrorPlaneXY:kMirrorPlaneYZ;
						Mirror(mirror, plane);

						int mirrorIdx = boneData->GetMirrorIndex();
						if(mirrorIdx != boneIdx)
						{
							const crBoneData* boneDataMirror = m_SkelData->GetBoneData(mirrorIdx);
							dest.GetUnsafe<QuatV>() = Multiply(mirror, boneDataMirror->GetDefaultRotation());

							u16 mirrorId;
							if(m_SkelData->ConvertBoneIndexToId(u16(mirrorIdx), mirrorId))
							{
								Assert(m_Frame->HasDof(dof.m_Track, u16(mirrorId)));
								if(mirrorId < dof.m_Id)
								{
									AssertMsg(boneData->GetDofs()==m_SkelData->GetBoneData(mirrorIdx)->GetDofs() , "cannot mirror asymmetrical joint dofs");
									m_Frame->SwapDofValue(dof.m_Track, dof.m_Id, dof.m_Track, u16(mirrorId));
								}
							}
						}
						else
						{
							dest.GetUnsafe<QuatV>() = Multiply(mirror, boneData->GetDefaultRotation());
						}
					}
				}
			}
			break;

		case kTrackMoverRotation:
			{
				Mirror(dest.GetUnsafe<QuatV>(), kMirrorPlaneYZ);
			}
			break;

		case kTrackBoneTranslation:
			{			
				int boneIdx;
				if(m_SkelData->ConvertBoneIdToIndex(dof.m_Id, boneIdx))
				{
					const crBoneData* boneData = m_SkelData->GetBoneData(boneIdx);
					if(boneData->GetDofs() & (crBoneData::TRANSLATE_X|crBoneData::TRANSLATE_Y|crBoneData::TRANSLATE_Z))
					{
//						Vec3V& mirror = dest.GetUnsafe<Vec3V>();
//						Vec3V mirror = UnTransform(boneData->GetDefaultRotation(), dest.GetUnsafe<Vec3V>());
						const crBoneData* boneDataParent = boneData->GetParent();
						Vec3V mirror = boneDataParent?UnTransform(boneDataParent->GetDefaultRotation(), dest.GetUnsafe<Vec3V>()):dest.GetUnsafe<Vec3V>();

						eMirrorPlane plane = dof.m_Id?kMirrorPlaneXY:kMirrorPlaneYZ;
						Mirror(mirror, plane);

						int mirrorIdx = boneData->GetMirrorIndex();
						if(mirrorIdx != boneIdx)
						{
							const crBoneData* boneDataMirror = m_SkelData->GetBoneData(mirrorIdx);
//							dest.GetUnsafe<Vec3V>() = Transform(boneDataMirror->GetDefaultRotation(), mirror);
							const crBoneData* boneDataMirrorParent = boneDataMirror->GetParent();
							dest.GetUnsafe<Vec3V>() = boneDataMirrorParent?Transform(boneDataMirrorParent->GetDefaultRotation(), mirror):mirror;

							u16 mirrorId;
							if(m_SkelData->ConvertBoneIndexToId(u16(mirrorIdx), mirrorId))
							{
								Assert(m_Frame->HasDof(dof.m_Track, u16(mirrorId)));
								if(mirrorId < dof.m_Id)
								{
									AssertMsg(boneData->GetDofs()==m_SkelData->GetBoneData(mirrorIdx)->GetDofs() , "cannot mirror asymmetrical joint dofs");
									m_Frame->SwapDofValue(dof.m_Track, dof.m_Id, dof.m_Track, u16(mirrorId));
								}
							}
						}
						else
						{
//							dest.GetUnsafe<Vec3V>() = Transform(boneData->GetDefaultRotation(), mirror);
							dest.GetUnsafe<Vec3V>() = boneDataParent?Transform(boneData->GetDefaultRotation(), mirror):mirror;
						}
					}
				}
			}
			break;

		case kTrackMoverTranslation:
			{
				Mirror(dest.GetUnsafe<Vec3V>(), kMirrorPlaneYZ);
			}
			break;
		}
	}

private:
	const crSkeletonData* m_SkelData;
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::Mirror(const crSkeletonData& skelData, crFrameFilter* filter)
{
	MirrorIterator it(*this, skelData);
	it.Iterate(filter, 1.f, true);
}

////////////////////////////////////////////////////////////////////////////////

class ZeroIterator : public crFrameIteratorByType<ZeroIterator>
{
public:
	ZeroIterator(crFrame& frame)
		: crFrameIteratorByType<ZeroIterator>(frame)
	{
	}

	template<typename _T>
	void IterateDofByType(const crFrameData::Dof&, crFrame::Dof&, float);
};

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void ZeroIterator::IterateDofByType<Vec3V>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	dest.GetUnsafe<Vec3V>() = Vec3V(V_ZERO);
	dest.SetInvalid(false);
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void ZeroIterator::IterateDofByType<QuatV>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	dest.GetUnsafe<QuatV>() = QuatV(V_IDENTITY);
	dest.SetInvalid(false);
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void ZeroIterator::IterateDofByType<float>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	dest.Set<float>(0.f);
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void ZeroIterator::IterateDofByType<int>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	dest.Set<int>(0);
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::Zero(crFrameFilter* filter)
{
	ZeroIterator it(*this);
	it.Iterate(filter, 1.f, false);
}

////////////////////////////////////////////////////////////////////////////////

class IdentityFromSkelIterator : public crFrameIterator<IdentityFromSkelIterator>
{
public:
	IdentityFromSkelIterator(crFrame& frame, const crSkeletonData& skelData)
		: crFrameIterator<IdentityFromSkelIterator>(frame)
		, m_SkelData(&skelData)
	{
	}

	__forceinline void IterateDofBone(const crFrameData::Dof& dof, crFrame::Dof& dest, int boneIdx, float)
	{
		const crBoneData* boneData = m_SkelData->GetBoneData(boneIdx);

		switch(dof.m_Track)
		{
		case kTrackBoneRotation:
			{
				dest.Set<QuatV>(boneData->GetDefaultRotation());
			}
			break;

		case kTrackBoneTranslation:
			{	
				dest.Set<Vec3V>(boneData->GetDefaultTranslation());
			}
			break;

		case kTrackBoneScale:
			{	
				dest.Set<Vec3V>(boneData->GetDefaultScale());
			}
			break;
		}
	}

private:
	const crSkeletonData* m_SkelData;
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::IdentityFromSkel(const crSkeletonData& skelData, crFrameFilter* filter)
{
	IdentityFromSkelIterator it(*this, skelData);
	it.IterateSkel(skelData, filter, 1.f, false);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
__forceinline void Evaluate(Vec3V_InOut dest, const crAnimTrack& track, float time)
{
	track.EvaluateVector3(time, dest);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void Evaluate(QuatV_InOut dest, const crAnimTrack& track, float time)
{
	track.EvaluateQuaternion(time, dest);
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void Evaluate(float& dest, const crAnimTrack& track, float time)
{
	track.EvaluateFloat(time, dest);
}

#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

class CompositeIterator : public crFrameIteratorByType<CompositeIterator>
{
public:
	CompositeIterator(crFrame& frame)
		: crFrameIteratorByType<CompositeIterator>(frame)
	{
	}

	template<typename _T>
	void IterateDofTrackByType(const crFrameData::Dof&, crFrame::Dof& dest, const crAnimTrack& track, float time, float)
	{
		Evaluate(dest.GetUnsafe<_T>(), track, time);
		dest.SetInvalid(false);
	}
};

////////////////////////////////////////////////////////////////////////////////

bool crFrame::CompositeMoverOnly(const crAnimation& anim, float time)
{
	// fast skip for large frames
	unsigned int numDofs = m_FrameData->GetNumDofs();
	if(numDofs > 2)
	{
		return false;
	}

	// get block and mover channels
	time = anim.ConvertTimeToInternalFrame(time);
	unsigned int blockIdx = anim.ConvertInternalFrameToBlockIndex(time);
	const crBlockStream* block = anim.GetBlock(blockIdx);

	time = anim.ConvertInternalFrameToBlockTime(time, blockIdx);
	unsigned int tq = Min((unsigned int)time, block->m_NumFrames-2u);
	float tr = Clamp(time - float(tq), 0.f, 1.f);	

	unsigned int numTransChannels = block->m_NumMoverChannels >> 4;
	unsigned int numRotChannels = block->m_NumMoverChannels & 15;
	const crBlockStream::Channel* channels = reinterpret_cast<const crBlockStream::Channel*>(reinterpret_cast<const u8*>(block) + block->m_ChannelOffset);
	
	// apply mover to frame
	for(unsigned int i = 0; i < numDofs; i++)
	{
		const crFrameData::Dof& dof = m_FrameData->m_Dofs[i];
		u8 track = dof.m_Track;
		u16 offset = dof.m_Offset;
		if(track == kTrackMoverTranslation)
		{
			block->EvaluateChannels(m_Buffer + offset, tq, tr, numTransChannels, channels);
		}
		else if(track == kTrackMoverRotation)
		{
			block->EvaluateChannels(m_Buffer + offset, tq, tr, numRotChannels, channels + numTransChannels);
		}
		else
		{
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::Composite(const crAnimation& anim, float time, crFrameFilter* filter)
{
	if(!filter && anim.IsPacked() && CompositeMoverOnly(anim, time))
	{
		return;
	}

	time = anim.ConvertTimeToInternalFrame(time);

	CompositeIterator it(*this);
	it.IterateAnim(anim, time, filter, 1.f);
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::CompositeWithDelta(const crAnimation& anim, float time, float delta, float rangeStart, float rangeEnd, crFrameFilter* filter)
{
	Composite(anim, time, filter);
	if(anim.HasMoverTracks())
	{
		CalcMoverDelta(anim, Clamp(time, rangeStart, rangeEnd), delta, rangeStart, rangeEnd);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::CompositeWithDelta(const crAnimation& anim, float time, float delta, crFrameFilter* filter)
{
	CompositeWithDelta(anim, time, delta, 0.f, anim.GetDuration(), filter);
}

////////////////////////////////////////////////////////////////////////////////

class CompositeBlendIterator : public crFrameIteratorByType<CompositeBlendIterator>
{
public:
	CompositeBlendIterator(crFrame& frame)
		: crFrameIteratorByType<CompositeBlendIterator>(frame)
	{
	}

	template<typename _T>
	void IterateDofTrackByType(const crFrameData::Dof&, crFrame::Dof& dest, const crAnimTrack& track, float time, float weight)
	{
		_T temp;
		Evaluate(temp, track, time);

		if(weight < (1.f - WEIGHT_ROUNDING_ERROR_TOLERANCE))
		{
			Interpolate(dest.GetUnsafe<_T>(), temp, weight);
			dest.SetInvalid(false);
		}
		else if(weight > WEIGHT_ROUNDING_ERROR_TOLERANCE)
		{
			dest.Set<_T>(temp);
		}
	}
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::CompositeBlend(float weight, const crAnimation& anim, float time, crFrameFilter* filter)
{
	time = anim.ConvertTimeToInternalFrame(time);
	if(weight > (1.f - WEIGHT_ROUNDING_ERROR_TOLERANCE))
	{
		CompositeIterator it(*this);
		it.IterateAnim(anim, time, filter, 1.f);
	}
	else
	{
		CompositeBlendIterator it(*this);
		it.IterateAnim(anim, time, filter, weight);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::CompositeWithDeltaBlend(float UNUSED_PARAM(weight), const crAnimation& UNUSED_PARAM(anim), float UNUSED_PARAM(time), float UNUSED_PARAM(delta), crFrameFilter* UNUSED_PARAM(filter))
{
	Assert(0);  // TODO --- write me!
}

////////////////////////////////////////////////////////////////////////////////

class InvalidateIterator : public crFrameIterator<InvalidateIterator>
{
public:
	InvalidateIterator(crFrame& frame)
		: crFrameIterator<InvalidateIterator>(frame)
	{
	}

	__forceinline void IterateDof(const crFrameData::Dof&, crFrame::Dof& dest, float)
	{
		dest.SetInvalid(true);
	}
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::Invalidate(crFrameFilter* filter)
{
	if(filter)
	{
		InvalidateIterator it(*this);
		it.Iterate(filter, 1.f, true);
	}
	else if(m_FrameData)
	{
		FastInvalidate();
	}
}

////////////////////////////////////////////////////////////////////////////////

class DirtyIterator : public crFrameIteratorByType<DirtyIterator>
{
public:
	DirtyIterator(crFrame& frame)
		: crFrameIteratorByType<DirtyIterator>(frame)
	{
	}

	template<typename _T>
	__forceinline void IterateDofByType(const crFrameData::Dof&, crFrame::Dof&, float);
};

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void DirtyIterator::IterateDofByType<Vec3V>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	dest.GetUnsafe<Vec3V>() = Vec3V(Vec::V4VConstant(V_NAN));
	dest.SetInvalid(true);
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void DirtyIterator::IterateDofByType<QuatV>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	dest.GetUnsafe<QuatV>() = QuatV(Vec::V4VConstant(V_NAN));
	dest.SetInvalid(true);
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void DirtyIterator::IterateDofByType<float>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	MakeNan(dest.GetUnsafe<float>());
	dest.SetInvalid(true);
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void DirtyIterator::IterateDofByType<int>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	dest.GetUnsafe<int>() = INT_MIN;
	dest.SetInvalid(true);
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::Dirty(crFrameFilter* filter)
{
	DirtyIterator it(*this);
	it.Iterate(filter, 1.f, false);
}

////////////////////////////////////////////////////////////////////////////////

class NormalizeIterator : public crFrameIteratorByType<NormalizeIterator>
{
public:
	NormalizeIterator(crFrame& frame)
		: crFrameIteratorByType<NormalizeIterator>(frame)
	{
	}

	template<typename _T>
	void IterateDofByType(const crFrameData::Dof&, crFrame::Dof&, float);
};

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void NormalizeIterator::IterateDofByType<QuatV>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	dest.GetUnsafe<QuatV>() = Normalize(dest.GetUnsafe<QuatV>());
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
__forceinline void NormalizeIterator::IterateDofByType(const crFrameData::Dof&, crFrame::Dof&, float)
{
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::Normalize(crFrameFilter* filter)
{
	NormalizeIterator it(*this);
	it.Iterate(filter, 1.f, true);
}

////////////////////////////////////////////////////////////////////////////////

class ValidIterator : public crFrameIterator<ValidIterator>
{
public:
	ValidIterator(const crFrame& frame)
		: crFrameIterator<ValidIterator>(frame)
		, m_Valid(true)
	{
	}

	__forceinline void IterateDof(const crFrameData::Dof&, crFrame::Dof& dest, float)
	{
		m_Valid = m_Valid && !dest.IsInvalid();
	}

	bool m_Valid;
};

////////////////////////////////////////////////////////////////////////////////

bool crFrame::Valid(crFrameFilter* filter) const
{
	// TODO --- provide a fast version of this when no filter present

	ValidIterator it(*this);
	it.Iterate(filter, 1.f, false);
	return it.m_Valid;
}

////////////////////////////////////////////////////////////////////////////////

class LegalIterator : public crFrameIteratorByType<LegalIterator>
{
public:
	LegalIterator(const crFrame& frame, bool unitQuaternions)
		: crFrameIteratorByType<LegalIterator>(frame)
		, m_UnitQuaternions(unitQuaternions)
		, m_Legal(true)
	{
	}

	template<typename _T>
	__forceinline void IterateDofByType(const crFrameData::Dof&, crFrame::Dof&, float);

public:
	bool m_UnitQuaternions;
	bool m_Legal;
};

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void LegalIterator::IterateDofByType<Vec3V>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	m_Legal = m_Legal && FPIsFinite(dest.GetUnsafe<Vec3V>().GetXf()) && FPIsFinite(dest.GetUnsafe<Vec3V>().GetYf()) && FPIsFinite(dest.GetUnsafe<Vec3V>().GetZf());
	m_Legal = m_Legal && (fabsf(dest.GetUnsafe<Vec3V>().GetXf()) < LARGE_FLOAT) && (fabsf(dest.GetUnsafe<Vec3V>().GetYf()) < LARGE_FLOAT) && (fabsf(dest.GetUnsafe<Vec3V>().GetZf()) < LARGE_FLOAT);
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void LegalIterator::IterateDofByType<QuatV>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	m_Legal = m_Legal && FPIsFinite(dest.GetUnsafe<QuatV>().GetXf()) && FPIsFinite(dest.GetUnsafe<QuatV>().GetYf()) && FPIsFinite(dest.GetUnsafe<QuatV>().GetZf()) && FPIsFinite(dest.GetUnsafe<QuatV>().GetWf());
	m_Legal = m_Legal && (fabsf(dest.GetUnsafe<QuatV>().GetXf()) < LARGE_FLOAT) && (fabsf(dest.GetUnsafe<QuatV>().GetYf()) < LARGE_FLOAT) && (fabsf(dest.GetUnsafe<QuatV>().GetZf()) < LARGE_FLOAT) && (fabsf(dest.GetUnsafe<QuatV>().GetWf()) < LARGE_FLOAT);
	m_Legal = m_Legal && (!m_UnitQuaternions || InRange(sqrt(square(dest.GetUnsafe<QuatV>().GetXf()) + square(dest.GetUnsafe<QuatV>().GetYf()) + square(dest.GetUnsafe<QuatV>().GetZf()) + square(dest.GetUnsafe<QuatV>().GetWf())), 0.999f, 1.001f));
}

////////////////////////////////////////////////////////////////////////////////

template<>
__forceinline void LegalIterator::IterateDofByType<float>(const crFrameData::Dof&, crFrame::Dof& dest, float)
{
	m_Legal = m_Legal && FPIsFinite(dest.GetUnsafe<float>());
	m_Legal = m_Legal && (fabsf(dest.GetUnsafe<float>()) < LARGE_FLOAT);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
__forceinline void LegalIterator::IterateDofByType(const crFrameData::Dof&, crFrame::Dof&, float)
{
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::Legal(crFrameFilter* filter) const
{
	LegalIterator it(*this, false);
	it.Iterate(filter, 1.f, true);
	return it.m_Legal;
}

////////////////////////////////////////////////////////////////////////////////

class JointLimitIterator : public crFrameIterator<JointLimitIterator>
{
public:
	JointLimitIterator(crFrame& frame, const crSkeletonData& skelData, const crJointData& jointData)
		: crFrameIterator<JointLimitIterator>(frame)
		, m_SkelData(&skelData)
		, m_JointData(&jointData)
	{
	}

	__forceinline void IterateDofBone(const crFrameData::Dof& dof, crFrame::Dof& dest, int boneIdx, float)
	{
		const crBoneData* boneData = m_SkelData->GetBoneData(boneIdx);
		const u32 dofs = boneData->GetDofs();
		switch (dof.m_Track)
		{
		case kTrackBoneRotation:
			{
				const crJointRotationLimit* limit = m_JointData->FindJointRotationLimit(boneData->GetBoneId());
				limit->ApplyEulerLimits(dest.GetUnsafe<QuatV>(), dofs);
			}
			break;
		case kTrackBoneTranslation:
			{
				const crJointTranslationLimit* limit = m_JointData->FindJointTranslationLimit(boneData->GetBoneId());
				limit->ApplyLimits(dest.GetUnsafe<Vec3V>(), dofs);
			}
			break;
		case kTrackBoneScale:
			{
				const crJointScaleLimit* limit = m_JointData->FindJointScaleLimit(boneData->GetBoneId());
				limit->ApplyLimits(dest.GetUnsafe<Vec3V>(), dofs);
			}
			break;
		}
	}
private:
	const crSkeletonData* m_SkelData;
	const crJointData* m_JointData;
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::Limit(const crSkeletonData& skelData, const crJointData& jointData, crFrameFilter* filter)
{
	JointLimitIterator it(*this, skelData, jointData);
	it.IterateSkel(skelData, filter, 1.f, true);
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::BlendN(u32 num, const float* weights, const crFrame*const* frames, bool fixWeights, bool mergeBlend, crFrameFilter* filter, crFrameFilter** filters)
{
	Assert(num > 0);
	Assert(weights);
	Assert(frames);

	if(mergeBlend)
	{
		BlendIterator<true, true> it(*this);
		it.IterateN(num, frames, filter, filters, weights, fixWeights, mergeBlend);
	}
	else
	{
		BlendIterator<false, true> it(*this);
		it.IterateN(num, frames, filter, filters, weights, fixWeights, mergeBlend);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::AddN(u32 num, const float* weights, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters)
{
	Assert(num > 0);
	Assert(weights);
	Assert(frames);

	AddIterator<true> it(*this);
	it.IterateN(num, frames, filter, filters, weights, false, true);
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::MergeN(u32 num, const float* weights, const crFrame*const* frames, crFrameFilter* filter, crFrameFilter** filters)
{
	Assert(num > 0);
	Assert(frames);

	if(!weights)
	{
		float* internalWeights = Alloca(float, num);
		for(u32 i=0; i<num; ++i)
		{
			internalWeights[i] = 1.f;
		}
		weights = internalWeights;
	}

	MergeIterator it(*this);
	it.IterateN(num, frames, filter, filters, weights, false, true);
}

////////////////////////////////////////////////////////////////////////////////

template<bool normalize>
class PoseIterator : public crFrameIterator<PoseIterator<normalize> >
{
public:
	PoseIterator(const crFrame& frame, crSkeleton& skel)
		: crFrameIterator<PoseIterator<normalize> >(frame)
		, m_Skel(&skel)
	{
	}

	__forceinline void IterateDofBone(const crFrameData::Dof& dof, crFrame::Dof& dest, int boneIdx, float)
	{
		Mat34V_Ref mtx = m_Skel->GetLocalMtx(boneIdx);
		Mat33V& mtx3 = mtx.GetMat33Ref();
		switch(dof.m_Track)
		{
		case kTrackBoneRotation:
			{
				Mat33VFromQuatV(mtx3, dest.GetUnsafe<QuatV>() );
				if(normalize)
				{
					ReOrthonormalize(mtx3, mtx3);
				}
			}
			break;

		case kTrackBoneTranslation:
			{
				mtx.SetCol3(dest.GetUnsafe<Vec3V>());
			}
			break;

		case kTrackBoneScale:
			{
				ScaleTranspose(mtx3, dest.GetUnsafe<Vec3V>(), mtx3);
			}
			break;
		}
	}

private:
	crSkeleton* m_Skel;
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::Pose(crSkeleton& skel, bool normalize, crFrameFilter* filter) const
{
	const crSkeletonData& skelData = skel.GetSkeletonData();

	if(normalize)
	{
		PoseIterator<true> it(*this, skel);
		it.IterateSkel(skelData, filter, 1.f, true);
	}
	else
	{
		PoseIterator<false> it(*this, skel);
		it.IterateSkel(skelData, filter, 1.f, true);
	}
}

////////////////////////////////////////////////////////////////////////////////

class InversePoseIterator : public crFrameIterator<InversePoseIterator>
{
public:
	InversePoseIterator(crFrame& frame, const crSkeleton& skeleton)
		: crFrameIterator<InversePoseIterator >(frame)
		, m_Skeleton(&skeleton)
	{
	}

	__forceinline void IterateDofBone(const crFrameData::Dof& dof, crFrame::Dof& dest, int boneIdx, float)
	{
		Mat34V_ConstRef mtx = m_Skeleton->GetLocalMtx(boneIdx);

		switch(dof.m_Track)
		{
		case kTrackBoneRotation:
			{
				QuatV q = QuatVFromMat33VSafe(mtx.GetMat33(), QuatV(Vec::V4VConstant(V_MASKXYZW)));
				dest.Set<QuatV>(q);
			}
			break;

		case kTrackBoneTranslation:
			{
				dest.Set<Vec3V>( mtx.GetCol3() );
			}
			break;

		case kTrackBoneScale:
			{
				dest.Set<Vec3V>( ScaleFromMat33VTranspose(mtx.GetMat33()) );
			}
			break;
		}
	}

private:
	const crSkeleton* m_Skeleton;
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::InversePose(const crSkeleton& skel, bool, crFrameFilter* filter)
{
	const crSkeletonData& skelData = skel.GetSkeletonData();

	InversePoseIterator it(*this, skel);
	it.IterateSkel(skelData, filter, 1.f, false);
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::SwapDofValue(u8 track0, u16 id0, u8 track1, u16 id1)
{
	Assert(m_FrameData);
	Assert(m_FrameData->m_Signature == m_Signature);

	u32 idx0, idx1;
	if(m_FrameData->FindDofIndex(track0, id0, idx0) && m_FrameData->FindDofIndex(track1, id1, idx1))
	{
		u8 type0 = m_FrameData->m_Dofs[idx0].m_Type;
		u8 type1 = m_FrameData->m_Dofs[idx1].m_Type;
		if(type0 == type1)
		{
			u16 offset0 = m_FrameData->m_Dofs[idx0].m_Offset;
			u16 offset1 = m_FrameData->m_Dofs[idx1].m_Offset;

			switch(type0)
			{
			case kFormatTypeVector3:
				SwapEm(*reinterpret_cast<Vec3V*>(m_Buffer+offset0), *reinterpret_cast<Vec3V*>(m_Buffer+offset1));
				break;

			case kFormatTypeQuaternion:
				SwapEm(*reinterpret_cast<QuatV*>(m_Buffer+offset0), *reinterpret_cast<QuatV*>(m_Buffer+offset1));
				break;

			case kFormatTypeFloat:
				SwapEm(*reinterpret_cast<float*>(m_Buffer+offset0), *reinterpret_cast<float*>(m_Buffer+offset1));
				break;

			default:
				break;
			}

			bool invalid0 = FastIsDofInvalid(idx0);
			bool invalid1 = FastIsDofInvalid(idx1);

			FastSetDofInvalid(idx0, invalid1);
			FastSetDofInvalid(idx1, invalid0);
			
			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::GetSituation(u8 posTrack, u8 rotTrack, u16 id, TransformV_InOut outTransform, bool tolerateMissingDof) const
{
	u32 posIdx;
	bool posSuccess = m_FrameData->FindDofIndex(posTrack, id, posIdx) && !FastIsDofInvalid(posIdx);
	if(posSuccess)
	{
		u16 posOffset = m_FrameData->m_Dofs[posIdx].m_Offset;
		outTransform.SetPosition(*reinterpret_cast<Vec3V*>(m_Buffer+posOffset));
	}
	else if(!tolerateMissingDof)
	{
		return false;
	}

	u32 rotIdx;
	if(m_FrameData->FindDofIndex(rotTrack, id, rotIdx) && !FastIsDofInvalid(rotIdx))
	{
		u16 rotOffset = m_FrameData->m_Dofs[rotIdx].m_Offset;
		outTransform.SetRotation(*reinterpret_cast<QuatV*>(m_Buffer+rotOffset));
	}

	return posSuccess;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::SetSituation(u8 posTrack, u8 rotTrack, u16 id, TransformV_In transform, bool tolerateMissingDof)
{
	u32 posIdx;
	bool posSuccess = m_FrameData->FindDofIndex(posTrack, id, posIdx) != NULL;
	if(posSuccess)
	{
		u16 posOffset = m_FrameData->m_Dofs[posIdx].m_Offset;
		*reinterpret_cast<Vec3V*>(m_Buffer+posOffset) = transform.GetPosition();

		FastSetDofInvalid(posIdx, false);
	}
	else if(!tolerateMissingDof)
	{
		return false;
	}

	u32 rotIdx;
	if(m_FrameData->FindDofIndex(rotTrack, id, rotIdx))
	{
		u16 rotOffset = m_FrameData->m_Dofs[rotIdx].m_Offset;
		*reinterpret_cast<QuatV*>(m_Buffer+rotOffset) = transform.GetRotation();

		FastSetDofInvalid(rotIdx, false);

		return true;
	}

	return posSuccess;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::GetMatrix(u8 posTrack, u8 rotTrack, u16 id, Mat34V_InOut outMtx, bool tolerateMissingDof) const
{
	u32 posIdx;
	bool posSuccess = m_FrameData->FindDofIndex(posTrack, id, posIdx) && !FastIsDofInvalid(posIdx);
	if(posSuccess)
	{
		u16 posOffset = m_FrameData->m_Dofs[posIdx].m_Offset;
		outMtx.SetCol3(*reinterpret_cast<Vec3V*>(m_Buffer+posOffset));
	}
	else if(!tolerateMissingDof)
	{
		return false;
	}

	u32 rotIdx;
	if(m_FrameData->FindDofIndex(rotTrack, id, rotIdx) && !FastIsDofInvalid(rotIdx))
	{
		u16 rotOffset = m_FrameData->m_Dofs[rotIdx].m_Offset;
		Mat34VFromQuatV(outMtx, *reinterpret_cast<QuatV*>(m_Buffer+rotOffset), outMtx.GetCol3());
		return true;
	}

	return posSuccess;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::SetMatrix(u8 posTrack, u8 rotTrack, u16 id, Mat34V_In mtx, bool tolerateMissingDof)
{
	u32 posIdx;
	bool posSuccess = m_FrameData->FindDofIndex(posTrack, id, posIdx) != NULL;
	if(posSuccess)
	{
		u16 posOffset = m_FrameData->m_Dofs[posIdx].m_Offset;
		*reinterpret_cast<Vec3V*>(m_Buffer+posOffset) = mtx.GetCol3();

		FastSetDofInvalid(posIdx, false);
	}
	else if(!tolerateMissingDof)
	{
		return false;
	}

	u32 rotIdx;
	if(m_FrameData->FindDofIndex(rotTrack, id, rotIdx))
	{
		u16 rotOffset = m_FrameData->m_Dofs[rotIdx].m_Offset;
		*reinterpret_cast<QuatV*>(m_Buffer+rotOffset) = QuatVFromMat33V(mtx.GetMat33());

		FastSetDofInvalid(rotIdx, false);

		return true;
	}

	return posSuccess;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::GetVector3(u8 track, u16 id, Vec3V_InOut outVector3) const
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);

	u32 idx;
	if(m_FrameData->FindDofIndex(track, id, idx) && !FastIsDofInvalid(idx))
	{
		u16 offset = m_FrameData->m_Dofs[idx].m_Offset;
		outVector3 = *reinterpret_cast<Vec3V*>(m_Buffer+offset);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::SetVector3(u8 track, u16 id, Vec3V_In vector3)
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);

	u32 idx;
	if(m_FrameData->FindDofIndex(track, id, idx))
	{
		u16 offset = m_FrameData->m_Dofs[idx].m_Offset;
		*reinterpret_cast<Vec3V*>(m_Buffer+offset) = vector3;

		FastSetDofInvalid(idx, false);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::GetQuaternion(u8 track, u16 id, QuatV_InOut outQuaternion) const
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);

	u32 idx;
	if(m_FrameData->FindDofIndex(track, id, idx) && !FastIsDofInvalid(idx))
	{
		u16 offset = m_FrameData->m_Dofs[idx].m_Offset;
		outQuaternion = *reinterpret_cast<QuatV*>(m_Buffer+offset);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::SetQuaternion(u8 track, u16 id, QuatV_In quaternion)
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);

	u32 idx;
	if(m_FrameData->FindDofIndex(track, id, idx))
	{
		u16 offset = m_FrameData->m_Dofs[idx].m_Offset;
		*reinterpret_cast<QuatV*>(m_Buffer+offset) = quaternion;

		FastSetDofInvalid(idx, false);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::GetMatrix3x3(u8 track, u16 id, Mat34V_InOut outMtx) const
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);

	u32 idx;
	if(m_FrameData->FindDofIndex(track, id, idx) && !FastIsDofInvalid(idx))
	{
		u16 offset = m_FrameData->m_Dofs[idx].m_Offset;
		Mat34VFromQuatV(outMtx, *reinterpret_cast<QuatV*>(m_Buffer+offset), outMtx.GetCol3());
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::SetMatrix3x3(u8 track, u16 id, Mat34V_In mtx)
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);

	u32 idx;
	if(m_FrameData->FindDofIndex(track, id, idx))
	{
		u16 offset = m_FrameData->m_Dofs[idx].m_Offset;
		*reinterpret_cast<QuatV*>(m_Buffer+offset) = QuatVFromMat33V(mtx.GetMat33());
		FastSetDofInvalid(idx, false);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::GetFloat(u8 track, u16 id, float& outFloat) const
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);

	u32 idx;
	if(m_FrameData->FindDofIndex(track, id, idx) && !FastIsDofInvalid(idx))
	{
		u16 offset = m_FrameData->m_Dofs[idx].m_Offset;
		outFloat = *reinterpret_cast<float*>(m_Buffer+offset);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::SetFloat(u8 track, u16 id, float f)
{
	Assert(m_FrameData);
	Assert(m_FrameData->GetSignature() == m_Signature);

	u32 idx;
	if(m_FrameData->FindDofIndex(track, id, idx))
	{
		u16 offset = m_FrameData->m_Dofs[idx].m_Offset;
		*reinterpret_cast<float*>(m_Buffer+offset) = f;

		FastSetDofInvalid(idx, false);

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
class DumpIterator : public crFrameIterator<DumpIterator>
{
public:
	DumpIterator(const crFrame& frame, crDumpOutput& output)
		: crFrameIterator<DumpIterator>(frame)
		, m_Output(&output)
	{
		m_Idx = 0;
	}

	__forceinline void IterateDof(const crFrameData::Dof& dof, const crFrame::Dof& dest, float)
	{
		m_Output->Outputf(0, "dof", m_Idx, "track %d id %d type %d invalid %d", dof.m_Track, dof.m_Id, dof.m_Type, dest.IsInvalid());

		const char* namePtr = crSkeletonData::DebugConvertBoneIdToName(dof.m_Id);
		if(namePtr)
		{
			m_Output->Outputf(1, "name", "'%s'", namePtr);
		}

		if(!dest.IsInvalid())
		{
			switch(dof.m_Type)
			{
			case kFormatTypeVector3:
				m_Output->Outputf(1, "value", "%f %f %f", dest.GetUnsafe<Vec3V>().GetXf(), dest.GetUnsafe<Vec3V>().GetYf(), dest.GetUnsafe<Vec3V>().GetZf());
				break;

			case kFormatTypeQuaternion:
				m_Output->Outputf(1, "value", "%f %f %f %f", dest.GetUnsafe<QuatV>().GetXf(), dest.GetUnsafe<QuatV>().GetYf(), dest.GetUnsafe<QuatV>().GetZf(), dest.GetUnsafe<QuatV>().GetWf());
				break;

			case kFormatTypeFloat:
				m_Output->Outputf(1, "value", "%f", dest.GetUnsafe<float>());
				break;

			default:
				m_Output->Outputf(1, "value", "unknown type");
				break;
			}
		}

		m_Idx++;
	}

	int m_Idx;

	crDumpOutput* m_Output;
};

////////////////////////////////////////////////////////////////////////////////

void crFrame::Dump(crDumpOutput& output, crFrameFilter* filter) const
{
	DumpIterator it(*this, output);
	it.Iterate(filter, 1.f, false);
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::Dump() const
{
	crDumpOutputStdIo output;
	Dump(output);
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

void crFrame::Init(const crFrame &src, bool destroyExistingDofs, crFrameFilter* filter)
{
	crFrameData* frameData = const_cast<crFrameData*>(m_FrameData);

	if(frameData)
	{
		Assert(frameData->GetRef() == 1);
		frameData->AddRef();
	}

	Shutdown();

	if(!frameData)
	{
		frameData = rage_new crFrameData;
	}

	crFrameDataInitializerFrameData init(*src.m_FrameData, destroyExistingDofs, filter);
	init.InitializeFrameData(*frameData);

	Init(*frameData);

	frameData->Release();
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::InitCreateBoneAndMoverDofs(const crSkeletonData& skelData, bool createMoverDofs, u16 moverId, bool destroyExistingDofs, crFrameFilter* filter)
{
	crFrameData* frameData = const_cast<crFrameData*>(m_FrameData);

	if(frameData)
	{
		Assert(frameData->GetRef() == 1);
		frameData->AddRef();
	}

	Shutdown();

	if(!frameData)
	{
		frameData = rage_new crFrameData;
	}

	crFrameDataInitializerBoneAndMover init(skelData, createMoverDofs, moverId, destroyExistingDofs, filter);
	init.InitializeFrameData(*frameData);
	
	Init(*frameData);

	frameData->Release();
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::InitCreateRootBoneDofs(bool destroyExistingDofs, crFrameFilter* filter)
{
	crFrameData* frameData = const_cast<crFrameData*>(m_FrameData);

	if(frameData)
	{
		Assert(frameData->GetRef() == 1);
		frameData->AddRef();
	}

	Shutdown();

	if(!frameData)
	{
		frameData = rage_new crFrameData;
	}

	crFrameDataInitializerRootBone init(destroyExistingDofs, filter);
	init.InitializeFrameData(*frameData);

	Init(*frameData);

	frameData->Release();
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::InitCreateMoverDofs(u16 moverId, bool destroyExistingDofs, crFrameFilter* filter)
{
	crFrameData* frameData = const_cast<crFrameData*>(m_FrameData);

	if(frameData)
	{
		Assert(frameData->GetRef() == 1);
		frameData->AddRef();
	}

	Shutdown();

	if(!frameData)
	{
		frameData = rage_new crFrameData;
	}

	crFrameDataInitializerMover init(moverId, destroyExistingDofs, filter);
	init.InitializeFrameData(*frameData);

	Init(*frameData);

	frameData->Release();
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::InitCreateDofs(int numDofs, u8 tracks[], u16 ids[], u8 types[], bool destroyExistingDofs, crFrameFilter* filter)
{
	crFrameData* frameData = const_cast<crFrameData*>(m_FrameData);

	if(frameData)
	{
		Assert(frameData->GetRef() == 1);
		frameData->AddRef();
	}

	Shutdown();

	if(!frameData)
	{
		frameData = rage_new crFrameData;
	}

	crFrameDataInitializerDofs init(numDofs, tracks, ids, types, destroyExistingDofs, filter);
	init.InitializeFrameData(*frameData);

	Init(*frameData);

	frameData->Release();
}

////////////////////////////////////////////////////////////////////////////////

void crFrame::InitCreateAnimationDofs(const crAnimation& anim, bool destroyExistingDofs, crFrameFilter* filter)
{
	crFrameData* frameData = const_cast<crFrameData*>(m_FrameData);

	if(frameData)
	{
		Assert(frameData->GetRef() == 1);
		frameData->AddRef();
	}

	Shutdown();

	if(!frameData)
	{
		frameData = rage_new crFrameData;
	}

	crFrameDataInitializerAnimation init(anim, destroyExistingDofs, filter);
	init.InitializeFrameData(*frameData);

	Init(*frameData);

	frameData->Release();
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::CalcMoverDelta(const crAnimation& anim, float time, float delta, float rangeStart, float rangeEnd)
{
	TransformV sit;
	if(CalcMoverDeltaInternal(anim, time, delta, rangeStart, rangeEnd, sit))
	{
		return SetMoverSituation(sit);
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::CalcMoverDelta(const crAnimation& anim, float time, float delta)
{
	return CalcMoverDelta(anim, time, delta, 0.f, anim.GetDuration());
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::CalcMoverDeltaInternal(const crAnimation& anim, float time, float delta, float rangeStart, float rangeEnd, TransformV_InOut moverDelta)
{
	Assert(rangeStart >= 0.f);
	Assert(rangeEnd <= anim.GetDuration());
	Assert(rangeEnd >= rangeStart);

	Assert(time >= rangeStart);
	Assert(time <= rangeEnd);

	float rangeDuration = rangeEnd-rangeStart;
	float start = time - delta;

	moverDelta = TransformV(V_IDENTITY);

	if(delta > 0.f)
	{
		// positive delta, start time is before current time
		if(start < rangeStart)
		{
			// delta caused looping
			float loopedDelta = fmodf(-(start-rangeStart), rangeDuration);
			if(!CalcMoverDeltaInternalUnsafe(anim, rangeEnd, loopedDelta, moverDelta))
			{
				return false;
			}

			if(-(start-rangeStart) > rangeDuration)
			{
				// loops multiple times
				TransformV moverLoop;
				if(!CalcMoverDeltaInternalUnsafe(anim, rangeEnd, rangeDuration, moverLoop))
				{
					return false;
				}

				int loops = int(floor(-(start-rangeStart) / rangeDuration));
				// WARNING: loops should be >= 1

				while(loops--)
				{
					Transform(moverDelta, moverDelta, moverLoop);
				}
			}
		}

		// calculate the delta in current loop
		TransformV moverCurrent;
		if(!CalcMoverDeltaInternalUnsafe(anim, time, Min(time-rangeStart, delta), moverCurrent))
		{
			return false;
		}

		Transform(moverDelta, moverDelta, moverCurrent);
	}
	else if(delta < 0.f)
	{
		// negative delta, start time is after current time
		if((start-rangeStart) > rangeDuration)
		{
			// delta caused looping
			float loopedDelta = -fmodf((start-rangeStart), rangeDuration);
			if(!CalcMoverDeltaInternalUnsafe(anim, rangeStart, loopedDelta, moverDelta))
			{
				return false;
			}

			if((start-rangeEnd) > rangeDuration)
			{
				// loops multiple times
				TransformV moverLoop;
				if(!CalcMoverDeltaInternalUnsafe(anim, rangeStart, -rangeDuration, moverLoop))
				{
					return false;
				}

				int loops = int(floor((start-rangeStart) / rangeDuration)-1.f);
				// WARNING: loops should be >= 1

				while(loops--)
				{
					Transform(moverDelta, moverDelta, moverLoop);
				}
			}
		}

		// calculate the delta in the current loop
		TransformV moverCurrent;
		if(!CalcMoverDeltaInternalUnsafe(anim, time, Max(time-rangeEnd, delta), moverCurrent))
		{
			return false;
		}

		Transform(moverDelta, moverDelta, moverCurrent);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::CalcMoverDeltaInternalUnsafe(const crAnimation& anim, float time, float delta, TransformV_InOut moverDelta)
{
	moverDelta = TransformV(V_IDENTITY);

	if(delta != 0.f)
	{
		float start;
		if(delta > 0.f)
		{
			delta = Min(time, delta);
			start = time - delta;
		}
		else
		{
			delta = Max(time-anim.m_Duration, delta);
			start = time - delta;
		}

		TransformV moverStart, moverEnd;
		if(CalcMoverInternalUnsafe(anim, start, moverStart) && CalcMoverInternalUnsafe(anim, time, moverEnd))
		{
			UnTransform(moverDelta, moverStart, moverEnd);

			return true;
		}

		return false;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrame::CalcMoverInternalUnsafe(const crAnimation& anim, float time, TransformV_InOut moverAbsolute)
{
	moverAbsolute = TransformV(V_IDENTITY);

	if(anim.IsPacked())
	{
		time = anim.ConvertTimeToInternalFrame(time);

		unsigned int blockIdx = anim.ConvertInternalFrameToBlockIndex(time);

		time = anim.ConvertInternalFrameToBlockTime(time, blockIdx);

		const crBlockStream* block = anim.GetBlock(blockIdx);

		unsigned int tq = Min((unsigned int)time, block->m_NumFrames-2u);
		float tr = Clamp(time - float(tq), 0.f, 1.f);	

		unsigned int numTransChannels = block->m_NumMoverChannels >> 4;
		unsigned int numRotChannels = block->m_NumMoverChannels & 15;
		const crBlockStream::Channel* channels = reinterpret_cast<const crBlockStream::Channel*>(reinterpret_cast<const u8*>(block) + block->m_ChannelOffset);
		block->EvaluateChannels(reinterpret_cast<u8*>(&moverAbsolute.GetPositionRef()), tq, tr, numTransChannels, channels);
		block->EvaluateChannels(reinterpret_cast<u8*>(&moverAbsolute.GetRotationRef()), tq, tr, numRotChannels, channels + numTransChannels);

		return numTransChannels!=0 || numRotChannels!=0;
	}

	crFrameDataFixedDofs<2> frameData;
	frameData.AddDof(kTrackMoverTranslation, 0, kFormatTypeVector3);
	frameData.AddDof(kTrackMoverRotation, 0, kFormatTypeQuaternion);

	crFrameFixedDofs<2> frame(frameData);
	frame.SetAccelerator(*m_Accelerator);
	frame.Composite(anim, time);

	return frame.GetMoverSituation(moverAbsolute);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
