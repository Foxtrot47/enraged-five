//
// cranimation/animdictionary.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMDICTIONARY_H
#define CRANIMATION_ANIMDICTIONARY_H

#include "animation.h"
#include "animtolerance.h"

#include "atl/map.h"
#include "data/resource.h"
#include "data/struct.h"
#include "file/asset.h"
#include "paging/base.h"
#include "string/stringhash.h"

namespace rage
{

class crAnimLoader;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Simple animation container class
class crAnimDictionary : public pgBaseRefCounted
{
public:

	// PURPOSE: Default constructor
	crAnimDictionary();

	// PURPOSE: Resource constructor
	crAnimDictionary(datResource&);

	// PURPOSE: Destructor
	virtual ~crAnimDictionary();

	// PURPOSE: Shutdown, free/release dynamic resources
	virtual void Shutdown();

	// PURPOSE: Placement
	DECLARE_PLACE(crAnimDictionary);

	// PURPOSE: Off-line resourcing
#if __DECLARESTRUCT
	virtual void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

	// PURPOSE: Off-line resourcing version
	static const int RORC_VERSION = 5 + crAnimation::RORC_VERSION;

#if CR_DEV
	// PURPOSE: Allocate and load, from animations contained in list file
	// PARAMS:
	// listFilename - filename of a text file, containing a list of animation filenames (one per line)
	// loader - optional derived animation loader class, to control loading of animations (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: Newly allocated animation dictionary, or NULL if error encountered
	static crAnimDictionary* AllocateAndLoadAnimations(const char* listFilename, crAnimLoader* loader=NULL, bool baseNameKeys=false);

	// PURPOSE: Allocate and load from list of animations supplied
	// PARAMS:
	// animFilenames - array of animation filenames
	// loader - optional derived animation loader class, to control loading of animations (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: Newly allocated animation dictionary, or NULL if error encountered
	static crAnimDictionary* AllocateAndLoadAnimations(const atArray<atString>& animFilenames, crAnimLoader* loader=NULL, bool baseNameKeys=false);

	// PURPOSE: Load from animations contained in list file
	// PARAMS:
	// listFilename - filename of a text file, containing a list of animation filenames (one per line)
	// loader - optional derived animation loader class, to control loading of animations (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: true - load successful, false - error encountered
	bool LoadAnimations(const char* listFilename, crAnimLoader* loader=NULL, bool baseNameKeys=false);

	// PURPOSE: Load from list of animations supplied
	// PARAMS:
	// animFilenames - array of animation filenames
	// loader - optional derived animation loader class, to control loading of animations (default NULL)
	// baseNameKeys - generate keys using base filename, rather than full filename (default false)
	// RETURNS: true - load successful, false - error encountered
	bool LoadAnimations(const atArray<atString>& animFilenames, crAnimLoader* loader=NULL, bool baseNameKeys=false);
#endif // CR_DEV

    // PURPOSE:	Loads an animation dictionary saved out as a resource.
	// The resource is created by rage/base/tools/rorc.
    // PARAMS:	
	// dictionaryFilename - Name of the dictionary; must not have an extension, and "_adt"
    // is automatically appended to it.
    // RETURNS:	Pointer to animation dictionary object, if load was successful.
    static crAnimDictionary* LoadResource(const char* dictionaryFilename);

	// PURPOSE: Dictionary key
	typedef u32 AnimKey;


	// PURPOSE: Retrieve animation using animation filename
	// PARAMS: filename - animation filename
	// RETURNS: Pointer to animation, or NULL if failed to find animation
	// NOTES: Fast, converts filename into hash key, then retrieves from map
	const crAnimation* GetAnimation(const char* filename) const;
	DEPRECATED const crAnimation* FindAnimation(const char* filename) const { return GetAnimation(filename); }

	// PURPOSE: Retrieve animation using animation filename (non-const)
	// PARAMS: filename - animation filename
	// RETURNS: Non-const pointer to animation, or NULL if failed to find animation
	// NOTES: Fast, converts filename into hash key, then retrieves from map
	crAnimation* GetAnimation(const char* filename);
	DEPRECATED crAnimation* FindAnimation(const char* filename) { return GetAnimation(filename); }

	// PURPOSE: Retrieve animation using hash key
	// PARAMS: key - animation hash key
	// RETURNS: Pointer to animation, or NULL if failed to find animation
	// NOTES: Very fast, uses hash key to retrieve from map
	const crAnimation* GetAnimation(AnimKey key) const;
	DEPRECATED const crAnimation* FindAnimation(AnimKey key) const { return GetAnimation(key); }

	// PURPOSE: Retrieve animation using hash key (non-const)
	// PARAMS: key - animation hash key
	// RETURNS: Non-const pointer to animation, or NULL if failed to find animation
	// NOTES: Very fast, uses hash key to retrieve from map
	crAnimation* GetAnimation(AnimKey key);
	DEPRECATED crAnimation* FindAnimation(AnimKey key) { return GetAnimation(key); }

	// PURPOSE: Convert animation filename into hash key
	// PARAMS: filename - animation filename
	// RETURNS: Hash key
	AnimKey ConvertNameToKey(const char* filename) const;


	// PURPOSE: Get number of animations in dictionary
	// RETURNS: Total number of animations stored in dictionary
	u32 GetNumAnimations() const;

	// PURPOSE: Get animation using index
	// PARAMS: idx - animation index [0..numAnimations-1]
	// RETURNS: Pointer to animation
	// NOTES: Very slow, iterates through animations to find index
	// Use ForAll instead if enumerating all the animations for better performance
	const crAnimation* FindAnimationByIndex(u32 idx) const;

	// PURPOSE: Get animation using index (non-const)
	// PARAMS: idx - animation index [0..numAnimations-1]
	// RETURNS: Non-const pointer to animation
	// NOTES: Very slow, iterates through animations to find index
	// Use ForAll instead if enumerating all the animations for better performance
	crAnimation* FindAnimationByIndex(u32 idx);

	// PURPOSE: Get animation hash key using index
	// PARAMS: idx - animation index [0..numAnimations-1]
	// RETURNS: Hash key
	AnimKey FindKeyByIndex(u32 idx) const;


	// PURPOSE: Const callback typedef
	typedef bool (*ConstAnimDictionaryCallback)(const crAnimation* anim, AnimKey key, void* data);

	// PURPOSE:	Execute a specified callback on every item in this dictionary
	// PARAMS:
	// cb - const callback function
	// data - user-specified callback data
	// RETURNS:	true - all callbacks succeeded (or dictionary was empty), false - callback failed
	bool ForAll(ConstAnimDictionaryCallback cb, void* data) const;


	// PURPOSE: Non-const callback typedef
	typedef bool (*AnimDictionaryCallback)(crAnimation* anim, AnimKey key, void* data);

	// PURPOSE:	Execute a specified callback on every item in this dictionary
	// PARAMS:
	// cb - non-const callback function
	// data - user-specified callback data
	// RETURNS:	true - all callbacks succeeded (or dictionary was empty), false - callback failed
	bool ForAll(AnimDictionaryCallback cb, void* data);


protected:

	// PURPOSE: Internal function for hashing filename to key
	// PARAMS: name - name to hash
	// RETURNS: Hash key
	// NOTES: Uses atStringHash by default, override to modify behavior
	virtual AnimKey CalcKey(const char* name) const;


	atMap<AnimKey, datOwner<crAnimation> >  m_Animations;

	bool m_BaseNameKeys;

	datPadding<3> m_Padding;
};


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base animation loader class
class crAnimLoader
{
public:

	// PURPOSE: Constructor
	crAnimLoader();

	// PURPOSE: Destructor
	virtual ~crAnimLoader();

	// PURPOSE: Allocate and load animation - override this to implement in derived class
	virtual crAnimation* AllocateAndLoadAnimation(const char* animFilename) = 0;
};

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
// PURPOSE: Basic animation loader - loads from file, with basic properties
class crAnimLoaderBasic : public crAnimLoader
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS:
	// compact - compact animation on load (optional, default false)
	// pack - pack animation on load (optional, default true)
	crAnimLoaderBasic(bool compact=false, bool pack=true);

	// PURPOSE: Destructor
	virtual ~crAnimLoaderBasic();

	// PURPOSE: Initializer
	// PARAMS:
	// compact - compact animation on load
	// pack - pack animation on load (optional, default true)
	void Init(bool compact, bool pack=true);

	// PURPOSE: Override allocate and load animation function
	virtual crAnimation* AllocateAndLoadAnimation(const char* animFilename);

protected:
	bool m_Compact;
	bool m_Pack;
};
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
// PURPOSE: Compressing animation loader - loads from file, with basic properties
// Then compresses the animation, using the tolerance provided
class crAnimLoaderCompress : public crAnimLoaderBasic
{
public:

	// PURPOSE: Constructor
	crAnimLoaderCompress();

	// PURPOSE: Destructor
	virtual ~crAnimLoaderCompress();

	// PURPOSE: Initializer
	// PARAMS:
	// tolerance - tolerance to use when compressing the animation
	// maxBlockSize - maximum animation block size (in bytes)
	void Init(crAnimTolerance& tolerance, const u32 maxBlockSize=CR_DEFAULT_MAX_BLOCK_SIZE);

	// PURPOSE: Override allocate and load animation function
	virtual crAnimation* AllocateAndLoadAnimation(const char* animFilename);

protected:
	crAnimTolerance* m_Tolerance;
	u32 m_MaxBlockSize;
};
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Dictionary animation loader - retrieves from dictionary
class crAnimLoaderDictionary : public crAnimLoader
{
public:

	// PURPOSE: Constructor
	crAnimLoaderDictionary();

	// PURPOSE: Initializing constructor
	// PARAMS: dictionary - pointer to dictionary
	crAnimLoaderDictionary(const crAnimDictionary* dictionary);

	// PURPOSE: Destructor
	virtual ~crAnimLoaderDictionary();

	// PURPOSE: Initializer
	// PARAMS: dictionary - pointer to dictionary
	void Init(const crAnimDictionary* dictionary);

	// PURPOSE: Override allocate and load animation function
	virtual crAnimation* AllocateAndLoadAnimation(const char* animFilename);

protected:
	const crAnimDictionary* m_Dictionary;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Null animation loader - doesn't attempt to load animation, just returns NULL
class crAnimLoaderNull : public crAnimLoader
{
public:

	// PURPOSE: Constructor
	crAnimLoaderNull();

	// PURPOSE: Destructor
	virtual ~crAnimLoaderNull();

	// PURPOSE: Override allocate and load animation function
	virtual crAnimation* AllocateAndLoadAnimation(const char* animFilename);
};

////////////////////////////////////////////////////////////////////////////////

inline const crAnimation* crAnimDictionary::GetAnimation(const char* animName) const
{
	return GetAnimation(ConvertNameToKey(animName));
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimation* crAnimDictionary::GetAnimation(const char* animName)
{
	return GetAnimation(ConvertNameToKey(animName));
}

////////////////////////////////////////////////////////////////////////////////

inline const crAnimation* crAnimDictionary::GetAnimation(AnimKey key) const
{
	const datOwner<crAnimation>* result = m_Animations.Access(key);
	return result?(*result):NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimation* crAnimDictionary::GetAnimation(AnimKey key)
{
	datOwner<crAnimation>* result = m_Animations.Access(key);
	return result?(*result):NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimDictionary::AnimKey crAnimDictionary::ConvertNameToKey(const char* animName) const
{
	if(m_BaseNameKeys)
	{
		char baseName[RAGE_MAX_PATH];
		ASSET.BaseName(baseName, RAGE_MAX_PATH, ASSET.FileName(animName));

		return CalcKey(baseName);
	}

	return CalcKey(animName);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimDictionary::GetNumAnimations() const
{
	return u32(m_Animations.GetNumUsed());
}

////////////////////////////////////////////////////////////////////////////////

inline const crAnimation* crAnimDictionary::FindAnimationByIndex(u32 idx) const
{
    atMap<AnimKey, datOwner<crAnimation> >::ConstIterator it = m_Animations.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
    return it.GetData();
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimation* crAnimDictionary::FindAnimationByIndex(u32 idx)
{
	atMap<AnimKey, datOwner<crAnimation> >::Iterator it = m_Animations.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
	return it.GetData();
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimDictionary::AnimKey crAnimDictionary::FindKeyByIndex(u32 idx) const
{
	atMap<AnimKey, datOwner<crAnimation> >::ConstIterator it = m_Animations.CreateIterator();
	for(u32 i=0; i<idx; ++i)
	{
		it.Next();
	}
	return it.GetKey();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimDictionary::ForAll(ConstAnimDictionaryCallback cb, void* data) const
{
	bool result = true;

	atMap<AnimKey, datOwner<crAnimation> >::ConstIterator it = m_Animations.CreateIterator();
	while(it)
	{
		if((result = cb(it.GetData(), it.GetKey(), data)) == false)
		{
			break;
		}
		it.Next();
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimDictionary::ForAll(AnimDictionaryCallback cb, void* data)
{
	bool result = true;

	atMap<AnimKey, datOwner<crAnimation> >::Iterator it = m_Animations.CreateIterator();
	while(it)
	{
		if((result = cb(it.GetData(), it.GetKey(), data)) == false)
		{
			break;
		}
		it.Next();
	}

	return result;
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimDictionary::AnimKey crAnimDictionary::CalcKey(const char* name) const
{
	return AnimKey(atStringHash(name));
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRANIMATION_ANIMDICTIONARY_H
