//
// cranimation/animplayer.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMPLAYER_H
#define CRANIMATION_ANIMPLAYER_H


// rage includes:
#include "animation.h"
#include "frameaccelerator.h"

#include "atl/functor.h"
#include "data/resource.h"
#include "data/struct.h"
#include "paging/ref.h"
#include "system/bit.h"

namespace rage
{

// rage pre-declarations:
class bkBank;
class crFrame;
class crFrameData;

/*
PURPOSE
 	A simple animation player.
	This class wraps a single animation, and manages time, looping,
	and basic frame compositing.
 */
class crAnimPlayer
{
public:
	crAnimPlayer();
	crAnimPlayer(const crAnimation* anim, float startTime = 0.f, float rate = 1.f, bool ignoreAnimsLooped = false, bool isLooped = false);
	crAnimPlayer(datResource& rsc);
	~crAnimPlayer();
	DECLARE_PLACE(crAnimPlayer);
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT
	void Shutdown();

	void Update(float deltaTime);
	void Update(float deltaTime, float& outUnusedTime, bool& outHasLoopedOrEnded);

	bool Composite(crFrame& frame, bool clear=true);
	
	void SetTime(float time);
	void SetDelta(float delta);
	void SetDeltaSupplement(float delta);
	void SetDeltaHiddenSupplement(float delta);
	void SetRate(float rate);
	void SetAnimation(const crAnimation* anim);
	void SetLooped(bool ignoreAnimsLooped, bool isLooped);

	float GetTime() const;
	float GetDelta() const;
	float GetDeltaSupplement() const;
	float GetDeltaHiddenSupplement() const;
	float GetRate() const;
	const crAnimation* GetAnimation() const;
	bool HasAnimation() const;
	bool IsLooped() const;
	void SetFunctor(const Functor0& functor);

#if __BANK
	void AddWidgets(bkBank &bank);
#endif // __BANK

private:
	enum
	{
		kLoopedAnimControlled = 0,
		kLoopedNever = BIT0,
		kLoopedAlways = BIT0|BIT1,
		kLoopedMask = BIT0|BIT1,

	};

	u32 m_Flags;
	float m_Time;
	float m_Delta;
	float m_DeltaSupplement;
	float m_DeltaHiddenSupplement;
	float m_Rate;
	pgRef<const crAnimation> m_Animation;
	Functor0 m_AnimCallback;
};


// inline functions:


/*
PURPOSE
 	Updates time (simple version).
PARAMS
	deltaTime - amount of time to advance (in seconds). Must be >= 0.0.
NOTES
	Advances current animation time, loops time if looped behavior indicated,
	otherwise clamps time to animation duration limits.
	Overwrites animation delta time with rage_new value.
 */
inline void crAnimPlayer::Update(float deltaTime)
{
	float unusedTime;
	bool hasLoopedOrEnded;
	
	Update(deltaTime, unusedTime, hasLoopedOrEnded);
}

/*
PURPOSE
 	Directly set the animation time.
PARAMS
	time - in seconds.
NOTES
	If animation is valid, time will be clamped to be within range [0..duration].
 */
inline void crAnimPlayer::SetTime(float time)
{
	m_Time = time;

	if(m_Animation)
	{
		m_Time = Clamp(m_Time, 0.f, m_Animation->GetDuration());
	}
}

// PURPOSE: Directly set the animation delta time.
// PARAMS: delta - new animation delta in seconds (can be negative).
inline void crAnimPlayer::SetDelta(float delta)
{
	m_Delta = delta;
}

// PURPOSE: Set animation delta supplement.
// PARAMS: delta - new animation delta supplement in seconds (can be negative).
// NOTES: Supplement is added into delta during update (and is then reset to 0).
inline void crAnimPlayer::SetDeltaSupplement(float delta)
{
	m_DeltaSupplement = delta;
}

// PURPOSE: Set animation delta hidden supplement.
// PARAMS: delta - new animation delta hidden supplement in seconds (can be negative).
// NOTES: Hidden supplement is added into delta after update (and is then reset to 0).
inline void crAnimPlayer::SetDeltaHiddenSupplement(float delta)
{
	m_DeltaHiddenSupplement = delta;
}


/*
PURPOSE
	Set the playback rate (default 1.0).
PARAMS
	rate - playback rate (multiplier).
NOTES
	Supports both positive and negative playback rates (and zero).
 */
inline void crAnimPlayer::SetRate(float rate)
{
	m_Rate = rate;
}

/*
PURPOSE
 	Set the looping behavior.
PARAMS
	ignoreAnimsLooped - true if you want to override the animation's looped flag, false to obey it.
	isLooped - (used only if ignoreAnimsLooped is true), true for always looped, false for never looped.
NOTES
	There are three possible states:
	A) use the animation's looped flag (which may or may not be set to looped)
	B) force looped behavior
	C) prevent looped behavior
 */
inline void crAnimPlayer::SetLooped(bool ignoreAnimsLooped, bool isLooped)
{
	m_Flags &= ~kLoopedMask;
	if(ignoreAnimsLooped)
	{
		m_Flags |= (isLooped ? kLoopedAlways : kLoopedNever);
	}
}

/*
PURPOSE
 	Return the current animation time.
RETURNS
	The current animation time in seconds.
 */
inline float crAnimPlayer::GetTime() const
{
	return m_Time;
}

// PURPOSE: Return the current animation delta time.
// RETURNS: The current animation delta time (can be positive, negative or zero).
inline float crAnimPlayer::GetDelta() const
{
	return m_Delta;
}


// PURPOSE: Return the current animation delta supplement.
// RETURNS: The current animation delta supplement (can be positive, negative or zero).
inline float crAnimPlayer::GetDeltaSupplement() const
{
	return m_DeltaSupplement;
}


// PURPOSE: Return the current animation delta supplement.
// RETURNS: The current animation delta hidden supplement (can be positive, negative or zero).
inline float crAnimPlayer::GetDeltaHiddenSupplement() const
{
	return m_DeltaHiddenSupplement;
}


/*
PURPOSE
 	Get the current playback rate.
RETURNS
	The current playback rate.
 */
inline float crAnimPlayer::GetRate() const
{
	return m_Rate;
}

/*
PURPOSE
 	Return the current animation being played.
RETURNS
	A pointer to the current animation being played, may return NULL if none.
 */
inline const crAnimation* crAnimPlayer::GetAnimation() const
{
	return m_Animation;
}

/*
PURPOSE
	Returns current animation state.
RETURNS
	true - if current animation not NULL.
*/
inline bool crAnimPlayer::HasAnimation() const
{
	return m_Animation != NULL;
}

/*
PURPOSE
 	Returns the current looping status.
RETURNS
	true - if forced looped is enabled, or animation is looped
	false - if never looped is enabled, or animation is not looped (or no animation present).
 */
inline bool crAnimPlayer::IsLooped() const
{
	if(m_Flags & kLoopedMask)
	{
		return (m_Flags & kLoopedMask) == kLoopedAlways;
	}
	else if(m_Animation != NULL)
	{
		return m_Animation->IsLooped();
	}

	return false;
}

} // namespace rage

#endif // CRANIMATION_ANIMPLAYER_H
