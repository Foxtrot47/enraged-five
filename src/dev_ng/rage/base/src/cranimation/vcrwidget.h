//
// cranimation/vcrwidget.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_VCRWIDGET_H
#define CRANIMATION_VCRWIDGET_H

#if __BANK

#include "atl/atfunctor.h"
#include "bank/widget.h"
#include "bank/packet.h"

namespace rage
{
class crVCRWidget : public bkWidget
{
public:
    enum ButtonStyle
    {
        VCR,
        Animation
    };

    enum Button
    {
        REWIND_OR_GO_TO_START,
        GO_TO_PREVIOUS_OR_STEP_BACKWARD,
        PLAY_BACKWARDS,
        PAUSE,
        PLAY_FORWARDS,
        GO_TO_NEXT_OR_STEP_FORWARD,
        FAST_FORWARD_OR_GO_TO_END,
        NUM_BUTTONS,
    };

    enum EnabledButton
    {
        NOTHING_ENABLED = 0x00,

        REWIND_OR_GO_TO_START_ENABLED = 0x01,
        GO_TO_PREVIOUS_OR_STEP_BACKWARD_ENABLED = 0x02,
        PLAY_BACKWARDS_ENABLED = 0x04,
        PAUSE_ENABLED = 0x08,
        PLAY_FORWARDS_ENABLED = 0x10,
        GO_TO_NEXT_OR_STEP_FORWARD_ENABLED = 0x20,
        FAST_FORWARD_OR_GO_TO_END_ENABLED = 0x40,

        FORWARD_ONLY_ENABLED = PAUSE_ENABLED | PLAY_FORWARDS_ENABLED | GO_TO_NEXT_OR_STEP_FORWARD_ENABLED | FAST_FORWARD_OR_GO_TO_END_ENABLED,
        ALL_ENABLED = 0x7f,
    };

	crVCRWidget( const char *title, const char *memo, ButtonStyle buttonStyle, u32 enabledButtons=ALL_ENABLED, const char *fillColor=NULL, bool readOnly=false );
    ~crVCRWidget();

    typedef atFunctor0<void> ActionFuncType;

    void SetRewindOrGoToStartCB( ActionFuncType func );
    void SetGoToPreviousOrStepBackwardCB( ActionFuncType func );
    void SetPlayBackwardsCB( ActionFuncType func );
    void SetPauseCB( ActionFuncType func );
    void SetPlayForwardsCB( ActionFuncType func );
    void SetGoToNextOrStepForwardCB( ActionFuncType func );
    void SetFastForwardOrGoToEndCB( ActionFuncType func );

    void SetEnabledButtons( u32 enabledButtons );

    bool SetFocus();

    void ButtonPressed( Button button );

protected:

    static int GetStaticGuid() { return BKGUID('a','v','c','r'); }
    int GetGuid() const;

    void Message( Action action, float value );
    int DrawLocal(int x,int y);

    void Update();
    static void RemoteHandler( const bkRemotePacket& p );

    ActionFuncType m_RewindOrGoToStartFunc;
    ActionFuncType m_GoToPreviousOrStepBackwardFunc;
    ActionFuncType m_PlayBackwardsFunc;
    ActionFuncType m_PauseFunc;
    ActionFuncType m_PlayForwardsFunc;
    ActionFuncType m_GoToNextOrStepForwardFunc;
    ActionFuncType m_FastForwardOrGoToEndFunc;

    ButtonStyle m_buttonStyle;
    u32 m_enabledButtons;

private:
    void RemoteCreate();
    void RemoteUpdate();

    int m_drawLocalExpanded;

#if __WIN32PC
    void WindowCreate();
    void WindowUpdate();
    void WindowDestroy();
    rageLRESULT WindowMessage( rageUINT msg, rageWPARAM wParam, rageLPARAM lParam );
    void WindowMessage( Action action, float value );
    int WindowResize( int x, int y, int width, int height );

public:
    void SetButtonInFocus( Button button, bool setWindowFocus );
    Button GetButtonInFocus() const;

    void SetWindowInFocus( struct HWND__* hwnd );
    struct HWND__* GetWindowInFocus() const;

protected:
    struct HWND__* m_titleWindowHandle;
    struct HWND__* m_windowHandles[8];
    Button m_buttonInFocus;
#endif

private:
    crVCRWidget();
    const crVCRWidget& operator=( crVCRWidget& );
};

inline void crVCRWidget::SetRewindOrGoToStartCB( ActionFuncType func )
{
    m_RewindOrGoToStartFunc = func;
}

inline void crVCRWidget::SetGoToPreviousOrStepBackwardCB( ActionFuncType func )
{
    m_GoToPreviousOrStepBackwardFunc = func;
}

inline void crVCRWidget::SetPlayBackwardsCB( ActionFuncType func )
{
    m_PlayBackwardsFunc = func;
}

inline void crVCRWidget::SetPauseCB( ActionFuncType func )
{
    m_PauseFunc = func;
}

inline void crVCRWidget::SetPlayForwardsCB( ActionFuncType func )
{
    m_PlayForwardsFunc = func;
}

inline void crVCRWidget::SetGoToNextOrStepForwardCB( ActionFuncType func )
{
    m_GoToNextOrStepForwardFunc = func;
}

inline void crVCRWidget::SetFastForwardOrGoToEndCB( ActionFuncType func )
{
    m_FastForwardOrGoToEndFunc = func;
}

#if __WIN32PC

inline crVCRWidget::Button crVCRWidget::GetButtonInFocus() const
{
    return m_buttonInFocus;
}

#endif

} // namespace rage

#endif // __BANK

#endif // CRANIMATION_VCRWIDGET_H
