//
// cranimation/animchannel.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "animchannel.h"

#if CR_DEV
#include "animation.h"
#include "animarchive.h"
#include "channelbasic.h"
#include "channelbspline.h"
#include "channellinear.h"
#include "channelindirectquantize.h"
#include "channelquantize.h"
#include "channelrle.h"
#include "channelsmallestthree.h"
#include "channelvariablequantize.h"
#include "animstream.h"

#include "system/memops.h"

namespace rage
{

atArray<const crAnimChannel::ChannelTypeInfo*> crAnimChannel::sm_ChannelTypeInfos;

bool crAnimChannel::sm_InitClassCalled = false;

//////////////////////////////////////////////////////////////////////////////////////////////

crAnimChannel* crAnimChannel::CreateChannel(u8 type)
{
	const ChannelTypeInfo* info = GetChannelTypeInfo(type);
	AssertMsg(info , "crAnimChannel::CreateChannel - attempting to create unknown channel type");

	return info->m_CreateFn();
}

u8 crAnimChannel::GetCompressionCost(u8 type)
{
	const ChannelTypeInfo* info = GetChannelTypeInfo(type);
	AssertMsg(info , "crAnimChannel::GetCompressionCost - unknown channel type");
	return info->m_CompressCost;
}

u8 crAnimChannel::GetDecompressionCost(u8 type)
{
	const ChannelTypeInfo* info = GetChannelTypeInfo(type);
	AssertMsg(info , "crAnimChannel::GetDecompressionCost - unknown channel type");
	return info->m_DecompressCost;
}

void crAnimChannel::Serialize(crArchive&)
{
}

void crAnimChannel::RegisterChannelTypeInfo(const ChannelTypeInfo& info)
{
	if(sm_ChannelTypeInfos.GetCount())
	{
		AssertMsg(!GetChannelTypeInfo(info.m_ChannelType) , "crAnimChannel::RegisterChannelTypeInfo - duplicate type info registration");
	}

	if(info.m_ChannelType < AnimChannelTypeCustom)
	{
		Assert(info.m_ChannelType < AnimChannelTypeNum);

		if(!sm_ChannelTypeInfos.GetCount())
		{
			sm_ChannelTypeInfos.Resize(AnimChannelTypeNum);
			for(int i=0; i<AnimChannelTypeNum; ++i)
			{
				sm_ChannelTypeInfos[i] = NULL;
			}
		}

		sm_ChannelTypeInfos[info.m_ChannelType] = &info;
	}
	else
	{
		sm_ChannelTypeInfos.Grow(1) = &info;
	}
}

const crAnimChannel::ChannelTypeInfo* crAnimChannel::GetChannelTypeInfo(u8 type)
{
	AssertMsg(sm_ChannelTypeInfos.GetCount() , "crAnimChannel::GetChannelTypeInfo - no channels registered, probably result of missing crAnimation::InitClass call");

	if(type < AnimChannelTypeCustom)
	{
		Assert(type < AnimChannelTypeNum);
		return sm_ChannelTypeInfos[type];
	}
	else
	{
		for(int i=AnimChannelTypeNum; i<sm_ChannelTypeInfos.GetCount(); ++i)
		{
			if(sm_ChannelTypeInfos[i] && sm_ChannelTypeInfos[i]->m_ChannelType == type)
			{
				return sm_ChannelTypeInfos[i];
			}

		}
	}

	return NULL;
}

void crAnimChannel::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		crAnimChannelRawFloat::InitClass();
		crAnimChannelRawVector3::InitClass();
		crAnimChannelRawQuaternion::InitClass();
		crAnimChannelStaticFloat::InitClass();
		crAnimChannelQuantizeFloat::InitClass();
		crAnimChannelRawInt::InitClass();
		crAnimChannelRawBool::InitClass();
		crAnimChannelStaticQuaternion::InitClass();
		crAnimChannelStaticInt::InitClass();
		crAnimChannelRleInt::InitClass();
		crAnimChannelStaticVector3::InitClass();
		crAnimChannelSmallestThreeQuaternion::InitClass();
		crAnimChannelVariableQuantizeFloat::InitClass();
		crAnimChannelIndirectQuantizeFloat::InitClass();
		crAnimChannelLinearFloat::InitClass();
		crAnimChannelQuadraticBSpline::InitClass();
		crAnimChannelCubicBSpline::InitClass();
		crAnimChannelStaticSmallestThreeQuaternion::InitClass();
	}
}

void crAnimChannel::ShutdownClass()
{
	sm_ChannelTypeInfos.Reset();

	sm_InitClassCalled = false;
}

//////////////////////////////////////////////////////////////////////////////////////////////

atPackedArray::atPackedArray()
	: m_Elements(NULL)
	, m_ElementSize(0)
	, m_ElementMax(0)
{
}

atPackedArray::atPackedArray(const atPackedArray& r)
{
	*this = r;
}

void atPackedArray::operator=(const atPackedArray& r)
{
	m_ElementSize = r.m_ElementSize;
	m_ElementMax = r.m_ElementMax;

	u32 elementCount = GetElementCount();
	m_Elements = rage_new u32[elementCount+1];
	sysMemCpy(m_Elements, r.m_Elements, elementCount*sizeof(u32));
}

atPackedArray::~atPackedArray()
{
	delete [] m_Elements;
}

int atPackedArray::ComputeSize() const
{
	return sizeof(u32) * GetElementCount();
}

void atPackedArray::Serialize(crArchive& a)
{
	a<<=m_ElementMax;
	a<<=m_ElementSize;
	u32 elementCount = GetElementCount();
	if(a.IsLoading())
	{
		m_Elements = rage_new u32[elementCount+1];
	}
	for(u32 i=0; i<elementCount; ++i)
	{
		a<<=m_Elements[i];
	}
}

void atPackedArray::Init(u32 elementSize, const atArray<u32>& values)
{
	FastAssert(m_ElementSize <= 32);

	delete[] m_Elements;

	u32 numValues = values.GetCount();
	m_ElementSize = elementSize;
	m_ElementMax = numValues;

	u32 numElements = GetElementCount();
	m_Elements = rage_new u32[numElements + 1]; // add an extra element as padding
	sysMemSet(m_Elements, 0, numElements*sizeof(u32));

	u32 addr = 0;
	u32* words = m_Elements;
	for(u32 i=0; i < numValues; i++)
	{
		Assert(values[i] < (1u<<elementSize));
		u32 val = values[i];
		u32 word = addr >> 5;
		u32 bit = addr & 31;

		words[word] |= val << bit;
		words[word+1] |= u64(val) >> (32-bit);

		addr += elementSize;
	}
}

u32 atPackedArray::GetElement(u32 index) const
{
	FastAssert(index < m_ElementMax);
	u32 address = index * m_ElementSize;
	u32 block = address >> 5, bit = address & 31;
	u64 word = m_Elements[block] | (u64(m_Elements[block+1])<<32);
	return u32((word>>bit) & g_MaskTable[m_ElementSize]);
}

u32 atPackedArray::GetElementMax() const
{
	return m_ElementMax;
}

bool atPackedArray::IsEmpty() const
{
	return (m_Elements == NULL);
}

u32 atPackedArray::GetElementSize() const
{
	return m_ElementSize;
}

u32 atPackedArray::GetElementCount() const
{
	return (m_ElementSize*m_ElementMax+31) >> 5;
}

//////////////////////////////////////////////////////////////////////////////////////////////

atPackedSequence::atPackedSequence()
	: m_Sequence(NULL)
	, m_SequenceMax(0)
	, m_Divisor(0)
{
}

atPackedSequence::~atPackedSequence()
{
	delete [] m_Sequence;
}

atPackedSequence::atPackedSequence(const atPackedSequence& r)
{
	m_SequenceMax = r.m_SequenceMax;
	m_Divisor = r.m_Divisor;

	u32 requiredCount = GetSequenceCount();
	m_Sequence = rage_new u32[requiredCount];
	for(u32 i=0; i<requiredCount; ++i)
	{
		m_Sequence[i] = r.m_Sequence[i];
	}
}

int atPackedSequence::ComputeSize() const
{
	return sizeof(u32) * GetSequenceCount();
}

void atPackedSequence::Serialize(crArchive& a)
{
	a<<=m_SequenceMax;
	a<<=m_Divisor;
	u32 requiredCount = GetSequenceCount();
	if(a.IsLoading())
	{
		m_Sequence = rage_new u32[requiredCount];
	}
	for(u32 i=0; i<requiredCount; ++i)
	{
		a<<=m_Sequence[i];
	}
}

bool atPackedSequence::InitAndCreate(const atArray<s32>& values, u32& lowestAddressMax)
{
	// find best rice encoding
	lowestAddressMax = UINT_MAX;
	for(u8 i=0; i<=kDivisorMax; i++)
	{
		u32 addressMax = atPackedSequence::CalcAddressMax(values, i);
		if(addressMax < lowestAddressMax)
		{
			lowestAddressMax = addressMax;
			m_Divisor = i;
		}
	}

	if(lowestAddressMax==UINT_MAX)
	{
		return false;
	}

	m_SequenceMax = lowestAddressMax;

	// create sequence
	u32 requiredCount = GetSequenceCount();
	delete [] m_Sequence;
	m_Sequence = rage_new u32[requiredCount];
	memset(m_Sequence, 0, requiredCount*sizeof(u32));

	u32 address = 0;
	for(int i=0; i<values.GetCount(); ++i)
	{
		SetValue(address, values[i]);
	}

	return true;
}

void atPackedSequence::SetValue(u32& address, s32 v)
{
	u32 abs = Abs(v);
	u32 quotient = abs >> m_Divisor;
	u32 remainder = abs & (0xffffffff>>(32-m_Divisor));

	for(u32 i=0; i < m_Divisor; i++)
	{
		SetBit(address, remainder&1);
		remainder >>= 1;
	}

	address += quotient;
	SetBit(address, 1);

	if(v != 0)
	{
		SetBit(address, v < 0);
	}
}

s32 atPackedSequence::GetValue(u32& address) const
{
	u32 divisor = m_Divisor;

	u32 remainder = 0;
	for(u32 i=0, bits=1; i < divisor; ++i)
	{
		if(GetBit(address))
		{
			remainder |= bits;
		}
		bits <<= 1;
	}

	s32 quotient = 0;
	while(!GetBit(address))
	{
		++quotient;
	}

	u32 pow = quotient << divisor;
	s32 v = pow | remainder;
	if(v && GetBit(address))
	{
		v = -v;
	}

	return v;
}

u32 atPackedSequence::CalcAddressMax(const atArray<s32>& values, u32 divisor)
{
	u32 count = values.GetCount();
	const s32* elements = values.GetElements();

	u32 addressMax = count*(divisor+1);
	u32 quotientMax = 0;

	for(u32 i=0; i < count; i++)
	{
		s32 elem = elements[i];
		addressMax += elem != 0;

		u32 quotient = abs(elem) >> divisor;
		addressMax += quotient;

		quotientMax = Max(quotientMax, quotient);
	}

	return quotientMax <= kQuotientMax ? addressMax : UINT_MAX;
}

u32 atPackedSequence::GetSequenceMax() const
{
	return m_SequenceMax;
}

u32 atPackedSequence::GetSequenceCount() const
{
	return (m_SequenceMax+31) >> 5;
}

u32 atPackedSequence::GetBit(u32& address) const
{
	FastAssert(address < m_SequenceMax);
	u32 word = address >> 5;
	u32 bit = address & 0x1f;
	address++;
	return (m_Sequence[word] >> bit) & 1;
}

void atPackedSequence::SetBit(u32& address, u32 v)
{
	FastAssert(address < m_SequenceMax);
	u32 word = address >> 5;
	u32 bit = address & 31;
	FastAssert(v <= 1);
	address++;
	m_Sequence[word] |= v << bit;
}

} // namespace rage

#endif // CR_DEV
