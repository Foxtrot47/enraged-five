//
// cranimation/animtolerance.h
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMTOLERANCE_H
#define CRANIMATION_ANIMTOLERANCE_H

#include "animation_config.h"

#define CR_DEFAULT_MAX_BLOCK_SIZE (96*1024)
#define CR_DEFAULT_FREQUENCY (30.f)

#if CR_DEV
#include "atl/array.h"
#include "math/simplemath.h"

namespace rage
{

class crAnimation;
class crSkeletonData;

// PURPOSE
// Base compression tolerance class.
// Used to control compression properties on tracks when compressing animation data.
class crAnimTolerance
{
public:
	crAnimTolerance() {}
	virtual ~crAnimTolerance() {}
	// TODO --- serialization support?

	// PURPOSE
	// Defines the maximum permissible error that the compression algorithms
	// can introduce when compressing a channel of data.
	// PARAMS
	// track - track index being considered for compression
	// id - track id being considered for compression
	// type - type of track being considered for compression
	// RETURNS
	// Maximum absolute error allowed in the data.
	// Return 0.f or SMALL_FLOAT for lossless compression only.
	virtual float GetMaxAbsoluteError(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Defines the maximum permissible error that the compression algorithms
	// can introduce when compressing a channel of data for a given time period.
	// PARAMS
	// timeStart - start of time range (in seconds)
	// timeEnd - end of time range (in seconds, must be >= timeStart)
	// track - track index being considered for compression
	// id - track id being considered for compression
	// type - type of track being considered for compression
	// RETURNS
	// Maximum absolute error allowed in the data.
	// Return 0.f or SMALL_FLOAT for lossless compression only.
	virtual float GetMaxAbsoluteError(float timeStart, float timeEnd, u8 track, u16 id, u8 type) const;

	enum eDecompressionCost
	{
		kDecompressionCostInvalid = -1,

		kDecompressionCostMinimum = 0,
		kDecompressionCostLow = 20,
		kDecompressionCostMed = 40,
		kDecompressionCostHigh = 60,
		kDecompressionCostVeryHigh = 80,
		kDecompressionCostMaximum = 100,

		kDecompressionCostDefault = kDecompressionCostHigh,
	};

	enum eCompressionCost
	{
		kCompressionCostInvalid = -1,

		kCompressionCostMinimum = 0,
		kCompressionCostLow = 20,
		kCompressionCostMed = 40,
		kCompressionCostHigh = 60,
		kCompressionCostVeryHigh = 80,
		kCompressionCostMaximum = 100,

		kCompressionCostDefault = kCompressionCostMaximum,
	};

	// PURPOSE
	// Defines the maximum computational cost the decompression algorithms can use
	// when decompressing the data.
	// Varying this enables you to trade off memory usage against runtime cost
	// of compositing your animations.
	// PARAMS
	// track - track index being considered for compression
	// id - track id being considered for compression
	// type - type of track being considered for compression
	// RETURNS
	// Maximum computational cost of decompression.
	virtual eDecompressionCost GetMaxDecompressionCost(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Defines the maximum computational cost the compression algorithms can use
	// when COMPRESSING the data.
	// If your compression takes place off line, you almost certainly want to set this
	// to maximum - let the exporter/pipeline take as long as it can to compress the data.
	// This is primarily intended for projects that capture and produce animations
	// at runtime (for replays etc).  They might want to trade off size of data produced,
	// against cost of performing the compression.
	// PARAMS
	// track - track index being considered for compression
	// id - track id being considered for compression
	// type - type of track being considered for compression
	// RETURNS
	// Maximum computational cost of compression.
	virtual eCompressionCost GetMaxCompressionCost(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Defines the minimum frequency the resampling algorithms can use when resampling the data
	// PARAMS
	// track - track index being considered for resampling
	// id - track id being considered for resampling
	// type - type of track being considered for resampling
	// RETURNS 
	// Minimum frequency (in Hz)
	virtual float GetMinFrequency(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Defines arbitrary property for use during compression
	// PARAMS
	// track - track index being considered
	// id - track id being considered
	// type - type of track being considered
	// hash - hash of property to retrieve
	// outFloat - value of property as float (if found)
	// outInt - value of property as integer (if found)
	// RETURNS 
	// true - property found, false - no such property (outFloat undefined)
	// NOTES
	// There are several "built-in" property names (see above for details):
	// MaxAbsoluteError, MaxDecompressionCost, MaxCompressionCost, MinFrequency
	virtual bool GetProperty(u8 track, u16 id, u8 type, u32 hash, float& outFloat) const;
	virtual bool GetProperty(u8 track, u16 id, u8 type, u32 hash, int& outInt) const;

	// PURPOSE
	// Return default static instance of this class, used for default parameters
	static crAnimTolerance& GetDefaultInstance() { return sm_DefaultInstance; }

	static const u32 sm_HashMaxAbsoluteError;
	static const u32 sm_HashMaxDecompressionCost;
	static const u32 sm_HashMaxCompressionCost;
	static const u32 sm_HashMinFrequency;

private:
	static crAnimTolerance sm_DefaultInstance;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE
// Simple compression tolerance class.
// Provides single separate compression tolerance values for translate and rotate channels.
class crAnimToleranceSimple : public crAnimTolerance
{
public:

	// PURPOSE
	// Default constructor.
	// SEE ALSO
	// Init
	crAnimToleranceSimple(
		float maxTranslationError=sm_DefaultMaxAbsoluteTranslationError,
		float maxRotationError=sm_DefaultMaxAbsoluteRotationError,
		float maxScaleError=sm_DefaultMaxAbsoluteScaleError,
		float maxDefaultError=sm_DefaultMaxAbsoluteDefaultError,
		eDecompressionCost maxDecompressionCost=kDecompressionCostDefault,
		eCompressionCost maxCompressionCost=kCompressionCostDefault,
		float minFrequency=sm_DefaultMinFrequency
		);

	// PURPOSE
	// Destructor
	virtual ~crAnimToleranceSimple() {}


	// PURPOSE
	// Initialize compression tolerances.
	// PARAMS
	// maxTranslationError - set the maximum permissible absolute error on translation tracks
	// maxRotationError - set the maximum permissible absolute error on rotation tracks
	// maxDefaultError - set the maximum permissible absolute error on all other tracks
	// maxDecompressionCost - set the maximum computational decompression cost
	// maxCompressionCost - set the maximum computational compression cost
	// minFrequency - minimum frequency (in Hz) for resampling
	// NOTES
	// The root is a special case in translation, and is not compressed regardless of the value of
	// maxTranslationError.
	void Init(
		float maxTranslationError=sm_DefaultMaxAbsoluteTranslationError,
		float maxRotationError=sm_DefaultMaxAbsoluteRotationError,
		float maxScaleError=sm_DefaultMaxAbsoluteScaleError,
		float maxDefaultError=sm_DefaultMaxAbsoluteDefaultError,
		eDecompressionCost maxDecompressionCost=kDecompressionCostDefault,
		eCompressionCost maxCompressionCost=kCompressionCostDefault,
		float minFrequency=sm_DefaultMinFrequency
		);

	// PURPOSE
 	// Return the max absolute error
 	virtual float GetMaxAbsoluteError(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Return the max decompression cost
	virtual eDecompressionCost GetMaxDecompressionCost(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Return the max compression cost
	virtual eCompressionCost GetMaxCompressionCost(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Return the min frequency
	virtual float GetMinFrequency(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Return default static instance of this class, used for default parameters
	static crAnimToleranceSimple& GetDefaultInstance() { return sm_DefaultInstance; }

	// PURPOSE
	// Default max absolute translation error
	static const float sm_DefaultMaxAbsoluteTranslationError;

	// PURPOSE
	// Default max absolute rotation error
	static const float sm_DefaultMaxAbsoluteRotationError;

	// PURPOSE
	// Default max absolute rotation error
	static const float sm_DefaultMaxAbsoluteScaleError;

	// PURPOSE
	// Default max absolute error (for all other uses)
	static const float sm_DefaultMaxAbsoluteDefaultError;

	// PURPOSE
	// Default min frequency for resampling
	static const float sm_DefaultMinFrequency;

private:
	static crAnimToleranceSimple sm_DefaultInstance;

	float m_MaxAbsoluteTranslationError;
	float m_MaxAbsoluteRotationError;
	float m_MaxAbsoluteScaleError;
	float m_MaxAbsoluteDefaultError;
	float m_MinFrequency;

	eDecompressionCost m_MaxDecompressionCost;
	eCompressionCost m_MaxCompressionCost;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE
// Complex compression tolerance class.
// Provides per track control of compression tolerance values.
class crAnimToleranceComplex : public crAnimToleranceSimple
{
public:

	// PURPOSE
	// Constructor
	crAnimToleranceComplex(
		float maxTranslationError=sm_DefaultMaxAbsoluteTranslationError,
		float maxRotationError=sm_DefaultMaxAbsoluteRotationError,
		float maxScaleError=sm_DefaultMaxAbsoluteScaleError,
		float maxDefaultError=sm_DefaultMaxAbsoluteDefaultError,
		eDecompressionCost maxDecompressionCost=kDecompressionCostDefault,
		eCompressionCost maxCompressionCost=kCompressionCostDefault,
		float minFrequency=sm_DefaultMinFrequency
		)
		: crAnimToleranceSimple(maxTranslationError, maxRotationError, maxScaleError, maxDefaultError, maxDecompressionCost, maxCompressionCost, minFrequency) {}

	// PURPOSE
	// Destructor
	virtual ~crAnimToleranceComplex() {}

	// PURPOSE
	// Return the max absolute error
	virtual float GetMaxAbsoluteError(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Return max decompression cost
	virtual eDecompressionCost GetMaxDecompressionCost(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Return max compression cost
	virtual eCompressionCost GetMaxCompressionCost(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Return the min frequency
	virtual float GetMinFrequency(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Return property
	virtual bool GetProperty(u8 track, u16 id, u8 type, u32 hash, float& outFloat) const;
	virtual bool GetProperty(u8 track, u16 id, u8 type, u32 hash, int& outInt) const;

	// PURPOSE
	// Append rule
	// PARAMS
	// track, id - track and id values
	// trackWildcard, idWildcard - if track and/or id not specified, set true to match all
	// maxAbsoluteError - (optional) max error tolerance to return if rule matches
	// minFrequency - (optional) min frequency to return if rule matches
	// maxDecompressionCost - (optional) max decompression cost to return if rule matches
	// maxCompressionCost - (optional) max compression cost to return if rule matches
	// RETURNS
	// new rule's index
	u32 AddRule(u8 track, bool trackWildcard, u16 id, bool idWildcard);
	u32 AddRule(u8 track, bool trackWildcard, u16 id, bool idWildcard, float maxAbsoluteError);
	u32 AddRule(u8 track, bool trackWildcard, u16 id, bool idWildcard, float maxAbsoluteError, float minFrequency);
	u32 AddRule(u8 track, bool trackWildcard, u16 id, bool idWildcard, float maxAbsoluteError, float minFrequency, eDecompressionCost maxDecompressionCost, eCompressionCost maxCompressionCost);

	// PURPOSE
	// Append property to existing rule
	// PARAMS
	// ruleIdx - index of existing rule (see return value of AddRule)
	// hash - hash value of property name
	// f/i - float or integer value of property
	// RETURNS
	// true - property added, false - failed to add property
	bool AddProperty(u32 ruleIdx, u32 hash, float f);
	bool AddProperty(u32 ruleIdx, u32 hash, int i);

	// PURPOSE
	// Remove rule
	void RemoveRule(u32 idx);

	// PURPOSE
	// Remove all rules
	void RemoveAllRules();

	// PURPOSE
	// Get the number of rules
	u32 GetNumRules() const;

private:

	// PURPOSE
	// Internal rule structure
	struct Rule
	{
		Rule()
			: m_ValidFlags(0)
			, m_Track(0)
			, m_Id(0)
			{
			}

		enum
		{
			kTrackValid = 0x01,
			kIdValid = 0x02,
		};

		u8 m_ValidFlags;

		u8 m_Track;
		u16 m_Id;

		struct Property
		{
			u32 m_Hash;
			union
			{
				float m_Float;
				int m_Int;
			};
		};

		atArray<Property> m_Properties;
	};

	// PURPOSE
	// Find first matching rule
	const Rule::Property* FindRule(u8 track, u16 id, u32 hash) const;

	atArray<Rule> m_Rules;
};


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Movement based animation compression
class crAnimToleranceMovement : public crAnimToleranceComplex
{
public:

	// PURPOSE: Constructor
	crAnimToleranceMovement(
		float speedConstriction=sm_DefaultSpeedConstriction,
		float speedRelaxation=sm_DefaultSpeedRelaxation,
		float minSpeed=sm_DefaultMinSpeed,
		float maxSpeed=sm_DefaultMaxSpeed,
		float maxTranslationError=sm_DefaultMaxAbsoluteTranslationError,
		float maxRotationError=sm_DefaultMaxAbsoluteRotationError,
		float maxScaleError=sm_DefaultMaxAbsoluteScaleError,
		float maxDefaultError=sm_DefaultMaxAbsoluteDefaultError,
		eDecompressionCost maxDecompressionCost=kDecompressionCostDefault,
		eCompressionCost maxCompressionCost=kCompressionCostDefault,
		float minFrequency=sm_DefaultMinFrequency
		);

	// PURPOSE
	// Destructor
	virtual ~crAnimToleranceMovement() {}

	// PURPOSE
	// Return the max absolute error.
	virtual float GetMaxAbsoluteError(u8 track, u16 id, u8 type) const;

	// PURPOSE
	// Return the max absolute error for given time range.
	virtual float GetMaxAbsoluteError(float timeStart, float timeEnd, u8 track, u16 id, u8 type) const;

	// TODO --- configure thresholds and error relaxation
	// bones to monitor - or monitor them all?
	bool CalcRelaxation(const crAnimation& anim, const crSkeletonData& skelData, float sampleRate);

	// PURPOSE
	void SetSpeedRelaxation(float speedConstriction, float speedRelaxation, float minSpeed, float maxSpeed);

	// PURPOSE
	void AddBoneChain(u16 boneId);

	// PURPOSE
	void RemoveBoneChain(u16 boneId);

	// PURPOSE
	void RemoveAllBoneChains();

	// PURPOSE
	// Default speed constriction
	static const float sm_DefaultSpeedConstriction;

	// PURPOSE
	// Default speed relaxation
	static const float sm_DefaultSpeedRelaxation;

	// PURPOSE
	// Default speed relaxation min speed threshold
	static const float sm_DefaultMinSpeed;

	// PURPOSE
	// Default speed relaxation max speed threshold
	static const float sm_DefaultMaxSpeed;

private:

	float m_SpeedConstriction;
	float m_SpeedRelaxation;
	float m_MinSpeed;
	float m_MaxSpeed;

	atArray<u16> m_BoneChains;

	struct BoneRelaxation
	{
		u16 m_BoneId;
		atArray<float> m_Relaxation;
	};

	atArray<BoneRelaxation> m_BoneRelaxation;

	float m_Duration;
	float m_SampleRate;
};


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Built in tolerance objects, used for default parameters
#define CR_DEFAULT_ANIM_TOLERANCE crAnimToleranceSimple::GetDefaultInstance()
#define CR_LOSSLESS_ANIM_TOLERANCE crAnimTolerance::GetDefaultInstance()

// LEGACY: Deprecated versions
#define DEFAULT_ANIM_TOLERANCE CR_DEFAULT_ANIM_TOLERANCE
#define LOSSLESS_ANIM_TOLERANCE CR_LOSSLESS_ANIM_TOLERANCE

////////////////////////////////////////////////////////////////////////////////

inline float crAnimTolerance::GetMaxAbsoluteError(u8, u16, u8) const
{
	return SMALL_FLOAT;
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimTolerance::GetMaxAbsoluteError(float, float, u8 track, u16 id, u8 type) const
{
	return GetMaxAbsoluteError(track, id, type);
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimTolerance::eCompressionCost crAnimTolerance::GetMaxCompressionCost(u8, u16, u8) const
{
	return kCompressionCostDefault;
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimTolerance::eDecompressionCost crAnimTolerance::GetMaxDecompressionCost(u8, u16, u8) const
{
	return kDecompressionCostDefault;
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimTolerance::GetMinFrequency(u8, u16, u8) const
{
	return CR_DEFAULT_FREQUENCY;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_ANIMTOLERANCE_H
