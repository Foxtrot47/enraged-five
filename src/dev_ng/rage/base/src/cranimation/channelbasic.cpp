//
// cranimation/channelbasic.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "channelbasic.h"

#if CR_DEV
#include "animation.h"
#include "animarchive.h"
#include "animtolerance.h"

#include "vectormath/classes.h"

namespace rage
{

//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelRawFloat, crAnimChannel::AnimChannelTypeRawFloat, crAnimTolerance::kCompressionCostMinimum, crAnimTolerance::kDecompressionCostMinimum);

void crAnimChannelRawFloat::EvaluateFloat(float fTime, float& outFloat) const
{
	int iTime = int(floor(fTime));
	float fract = fTime - float(iTime);

	if(fract > (1.f - INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE))
	{
		iTime++;
	}
	else if(fract > INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE)
	{
		outFloat = Lerp(fract, m_Floats[Clamp(iTime, 0, m_Floats.GetCount()-1)], m_Floats[Clamp(iTime+1, 0, m_Floats.GetCount()-1)]);
		return;
	}

	outFloat = m_Floats[Clamp(iTime, 0, m_Floats.GetCount()-1)];
}

bool crAnimChannelRawFloat::CreateFloat(float* floats, int frames, int skip, float /*tolerance*/)
{
	m_Floats.Reset();
	m_Floats.Resize(frames);
	int stride = 1 + skip;
	for(int i=0; i<frames; i++)
	{
		m_Floats[i] = *floats;
		floats += stride;
	}

	return true;
}

int crAnimChannelRawFloat::ComputeSize(void) const
{
	return sizeof(crAnimChannelRawFloat) + (m_Floats.GetCount() * sizeof(float));
}

void crAnimChannelRawFloat::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	u16 n = u16(m_Floats.GetCount());
	a<<=n;
	if(a.IsLoading())
	{
		m_Floats.Resize(n);
	}
	for(u16 i=0; i<n; i++)
	{
		a<<=m_Floats[i];
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelRawVector3, crAnimChannel::AnimChannelTypeRawVector3, crAnimTolerance::kCompressionCostMinimum, crAnimTolerance::kDecompressionCostMinimum);

crAnimChannelRawVector3::crAnimChannelRawVector3(const crAnimChannelRawVector3& r)
: crAnimChannel(r)
{
	m_Vectors = r.m_Vectors;
}

void crAnimChannelRawVector3::EvaluateVector3(float fTime, Vec3V_InOut outVector3) const
{
	int iTime = int(floor(fTime));
	float fract = fTime - float(iTime);

	if(fract > (1.f - INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE))
	{
		iTime++;
	}
	else if(fract > INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE)
	{
		const Vec3V& v0 = m_Vectors[Clamp(iTime, 0, m_Vectors.GetCount()-1)];
		const Vec3V& v1 = m_Vectors[Clamp(iTime+1, 0, m_Vectors.GetCount()-1)];

		outVector3 = Lerp(ScalarVFromF32(fract), v0, v1);

		return;
	}

	outVector3 = m_Vectors[Clamp(iTime, 0, m_Vectors.GetCount()-1)];
}

bool crAnimChannelRawVector3::CreateVector3(Vec3V* vectors, int frames, float /*tolerance*/)
{
	m_Vectors.Reset();
	m_Vectors.Resize(frames);
	for(int i=0; i<frames; i++)
	{
		m_Vectors[i] = *vectors;
		vectors++;
	}

	return true;
}

int crAnimChannelRawVector3::ComputeSize(void) const
{
	return sizeof(crAnimChannelRawVector3) + (m_Vectors.GetCount() * sizeof(Vec3V));
}

void crAnimChannelRawVector3::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	u16 n = u16(m_Vectors.GetCount());
	a<<=n;
	if(a.IsLoading())
	{
		m_Vectors.Resize(n);
	}
	for(u16 i=0; i<n; i++)
	{
		a<<=m_Vectors[i];
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelRawQuaternion, crAnimChannel::AnimChannelTypeRawQuaternion, crAnimTolerance::kCompressionCostMinimum, crAnimTolerance::kDecompressionCostMinimum);

crAnimChannelRawQuaternion::crAnimChannelRawQuaternion(const crAnimChannelRawQuaternion& r)
: crAnimChannel(r)
{
	m_Quaternions = r.m_Quaternions;
}

void crAnimChannelRawQuaternion::EvaluateQuaternion(float fTime, QuatV_InOut outQuaternion) const
{
	int iTime = int(floor(fTime));
	float fract = fTime - float(iTime);

	if(fract > (1.f - INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE))
	{
		iTime++;
	}
	else if(fract > INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE)
	{
		const QuatV& q0 = m_Quaternions[Clamp(iTime, 0, m_Quaternions.GetCount()-1)];
		const QuatV& q1 = m_Quaternions[Clamp(iTime+1, 0, m_Quaternions.GetCount()-1)];

		outQuaternion = Nlerp(ScalarVFromF32(fract), q0, q1);	
	
		return;
	}

	outQuaternion = m_Quaternions[Clamp(iTime, 0, m_Quaternions.GetCount()-1)];
}

bool crAnimChannelRawQuaternion::CreateQuaternion(QuatV* quaternions, int frames, float /*tolerance*/)
{
	m_Quaternions.Reset();
	m_Quaternions.Resize(frames);
	for(int i=0; i<frames; i++)
	{
		m_Quaternions[i] = *quaternions;
		if(i>0)
		{
			m_Quaternions[i] = PrepareSlerp(m_Quaternions[i-1], m_Quaternions[i]);
		}
		quaternions++;
	}

	return true;
}

int crAnimChannelRawQuaternion::ComputeSize(void) const
{
	return sizeof(crAnimChannelRawQuaternion) + (m_Quaternions.GetCount() * sizeof(QuatV));
}

void crAnimChannelRawQuaternion::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	u16 n = u16(m_Quaternions.GetCount());
	a<<=n;
	if(a.IsLoading())
	{
		m_Quaternions.Resize(n);
	}
	for(u16 i=0; i<n; i++)
	{
		a<<=m_Quaternions[i];
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelRawInt, crAnimChannel::AnimChannelTypeRawInt, crAnimTolerance::kCompressionCostMinimum, crAnimTolerance::kDecompressionCostMinimum);

void crAnimChannelRawInt::EvaluateInt(float fTime, s32& outInt) const
{
	// TODO --- no interpolation, should this be supported?
	int iTime = int(fTime+0.5f);
	outInt = m_Ints[Clamp(iTime, 0, m_Ints.GetCount()-1)];
}

bool crAnimChannelRawInt::CreateInt(s32* ints, int frames)
{
	m_Ints.Reset();
	m_Ints.Resize(frames);
	for(int i=0; i<frames; i++)
	{
		m_Ints[i] = *ints;
		ints++;
	}

	return true;
}

int crAnimChannelRawInt::ComputeSize(void) const
{
	return sizeof(crAnimChannelRawInt) + (m_Ints.GetCount() * sizeof(s32));
}

void crAnimChannelRawInt::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	u16 n = u16(m_Ints.GetCount());
	a<<=n;
	if(a.IsLoading())
	{
		m_Ints.Resize(n);
	}
	for(u16 i=0; i<n; i++)
	{
		a<<=m_Ints[i];
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelRawBool, crAnimChannel::AnimChannelTypeRawBool, crAnimTolerance::kCompressionCostMinimum, crAnimTolerance::kDecompressionCostMinimum);

void crAnimChannelRawBool::EvaluateBool(float fTime, bool& outBool) const
{
	int iTime = Max(0, int(fTime+0.5f));

	outBool = (m_PackedBools[Min(int(u32(iTime)>>3), m_PackedBools.GetCount())] & (u8(iTime)&0x7)) > 0;
}

bool crAnimChannelRawBool::CreateBool(bool* bools, int frames)
{
	m_PackedBools.Reset();

	int nPackedBools = (u32(frames)>>3) + (((u32(frames)&0x7)>0)?1:0);
	if(nPackedBools > 0)
	{
		m_PackedBools.Resize(nPackedBools);

		int n=0;
		for(int i=0; i<nPackedBools; i++)
		{
			for(int j=0; j<8; j++)
			{
				if(bools[n])
				{
					m_PackedBools[i] |= u8(1)<<j;
				}
				else
				{
					m_PackedBools[i] &= ~(u8(1)<<j);
				}

				n = Min(n+1, frames-1);
			}
		}
	}

	return true;
}

int crAnimChannelRawBool::ComputeSize(void) const
{
	return sizeof(crAnimChannelRawBool) + (m_PackedBools.GetCount() * sizeof(u8));
}

void crAnimChannelRawBool::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	u16 n = u16(m_PackedBools.GetCount());
	a<<=n;
	if(a.IsLoading())
	{
		m_PackedBools.Resize(n);
	}
	for(u16 i=0; i<n; i++)
	{
		a<<=m_PackedBools[i];
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelStaticFloat, crAnimChannel::AnimChannelTypeStaticFloat, crAnimTolerance::kCompressionCostLow, crAnimTolerance::kDecompressionCostMinimum);

void crAnimChannelStaticFloat::EvaluateFloat(float /*fTime*/, float& outFloat) const
{
	outFloat = m_Float;
}

bool crAnimChannelStaticFloat::CreateFloat(float* floats, int frames, int skip, float tolerance)
{
	int stride = 1 + skip;
	m_Float = *floats;
	floats+=stride;
	for(int i=1; i<frames; i++)
	{
		if(square(m_Float - *floats) > square(tolerance/2.f))
			return false;
		floats += stride;
	}

	return true;
}

int crAnimChannelStaticFloat::ComputeSize(void) const
{
	return sizeof(crAnimChannelStaticFloat);
}

void crAnimChannelStaticFloat::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	a<<=m_Float;
}

//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelStaticQuaternion, crAnimChannel::AnimChannelTypeStaticQuaternion, crAnimTolerance::kCompressionCostLow, crAnimTolerance::kDecompressionCostMinimum);

crAnimChannelStaticQuaternion::crAnimChannelStaticQuaternion()
: crAnimChannel(AnimChannelTypeStaticQuaternion)
, m_Quaternion(NULL)
{
	m_Quaternion = rage_new QuatV;
}

crAnimChannelStaticQuaternion::crAnimChannelStaticQuaternion(const crAnimChannelStaticQuaternion& r)
: crAnimChannel(r)
{
	m_Quaternion = rage_new QuatV;
	*m_Quaternion = *r.m_Quaternion;
}

crAnimChannelStaticQuaternion::~crAnimChannelStaticQuaternion()
{
	if(m_Quaternion)
	{
		delete m_Quaternion;
		m_Quaternion = NULL;
	}
}

void crAnimChannelStaticQuaternion::EvaluateQuaternion(float, QuatV_InOut outQuaternion) const
{
	outQuaternion = *m_Quaternion;
}

bool crAnimChannelStaticQuaternion::CreateQuaternion(QuatV* quaternions, int frames, float tolerance)
{
	*m_Quaternion = *quaternions++;

	ScalarV error = ScalarVFromF32(tolerance/2.f);
	for(int i=1; i<frames; i++)
	{
		if(!IsCloseAll(*m_Quaternion, *quaternions, error))
			return false;
		quaternions++;
	}

	return true;
}

int crAnimChannelStaticQuaternion::ComputeSize(void) const
{
	return sizeof(crAnimChannelStaticQuaternion) + sizeof(QuatV);
}

void crAnimChannelStaticQuaternion::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	a<<=(*m_Quaternion);
}

//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelStaticSmallestThreeQuaternion, crAnimChannel::AnimChannelTypeStaticSmallestThreeQuaternion, crAnimTolerance::kCompressionCostLow, crAnimTolerance::kDecompressionCostLow);

crAnimChannelStaticSmallestThreeQuaternion::crAnimChannelStaticSmallestThreeQuaternion()
: crAnimChannel(AnimChannelTypeStaticSmallestThreeQuaternion)
{
}

void crAnimChannelStaticSmallestThreeQuaternion::EvaluateQuaternion(float, QuatV_InOut outQuaternion) const
{
	outQuaternion[0] = m_X;
	outQuaternion[1] = m_Y;
	outQuaternion[2] = m_Z;
	outQuaternion[3] = sqrtf(Max(0.f, 1.f - (square(outQuaternion[0])+square(outQuaternion[1])+square(outQuaternion[2]))));
}

bool crAnimChannelStaticSmallestThreeQuaternion::CreateQuaternion(QuatV* quaternions, int frames, float tolerance)
{
	QuatV q = *quaternions++;
	if(q.GetWf() < 0.f)
	{
		q = Negate(q);
	}

	for(int i=1; i<frames; i++)
	{
		QuatV curr = *quaternions;
		if(Max(square(q[0] - curr[0]), square(q[1] - curr[1]), square(q[2] - curr[2]), square(q[3] - curr[3])) > square(tolerance))
			return false;
		quaternions++;
	}

	m_X = q[0];
	m_Y = q[1];
	m_Z = q[2];

	return true;
}

int crAnimChannelStaticSmallestThreeQuaternion::ComputeSize(void) const
{
	return 3*sizeof(f32);
}

void crAnimChannelStaticSmallestThreeQuaternion::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	a<<=m_X;
	a<<=m_Y;
	a<<=m_Z;
}

//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelStaticInt, crAnimChannel::AnimChannelTypeStaticInt, crAnimTolerance::kCompressionCostLow, crAnimTolerance::kDecompressionCostMinimum);

void crAnimChannelStaticInt::EvaluateInt(float, s32& outInt) const
{
	outInt = m_Int;
}

bool crAnimChannelStaticInt::CreateInt(s32* ints, int frames)
{
	for(int i=1; i<frames; ++i)
	{
		if(ints[i-1] != ints[i])
		{
			return false;
		}
	}

	m_Int = ints[0];

	return true;
}

int crAnimChannelStaticInt::ComputeSize(void) const
{
	return sizeof(crAnimChannelStaticInt);
}

void crAnimChannelStaticInt::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	a<<=m_Int;
}


//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelStaticVector3, crAnimChannel::AnimChannelTypeStaticVector3, crAnimTolerance::kCompressionCostLow, crAnimTolerance::kDecompressionCostMinimum);

crAnimChannelStaticVector3::crAnimChannelStaticVector3()
: crAnimChannel(AnimChannelTypeStaticVector3)
, m_Vector(NULL)
{
	m_Vector = rage_new Vec3V;
}

crAnimChannelStaticVector3::crAnimChannelStaticVector3(const crAnimChannelStaticVector3& r)
: crAnimChannel(r)
{
	m_Vector = rage_new Vec3V;
	*m_Vector = *r.m_Vector;
}

crAnimChannelStaticVector3::~crAnimChannelStaticVector3()
{
	if(m_Vector)
	{
		delete m_Vector;
		m_Vector = NULL;
	}
}

void crAnimChannelStaticVector3::EvaluateVector3(float, Vec3V_InOut outVector) const
{
	outVector = *m_Vector;
}

bool crAnimChannelStaticVector3::CreateVector3(Vec3V* vectors, int frames, float tolerance)
{
	*m_Vector = *vectors++;

	ScalarV error = ScalarVFromF32(tolerance/2.f);
	for(int i=1; i<frames; i++)
	{
		if(!IsCloseAll(*m_Vector, *vectors, error))
			return false;
		vectors++;
	}

	return true;
}

int crAnimChannelStaticVector3::ComputeSize(void) const
{
	return sizeof(crAnimChannelStaticVector3) + sizeof(Vec3V);
}

void crAnimChannelStaticVector3::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	a<<=(*m_Vector);
}

//////////////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
