//
// cranimation/frameinitializers.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_FRAMEINITIALIZERS_H
#define CRANIMATION_FRAMEINITIALIZERS_H

#include "animation_config.h"

#include "cranimation/framedata.h"

namespace rage
{

class crAnimation;
class crCreature;
class crFrameData;
class crFrameFilter;
class crSkeletonData;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Frame data initializer (abstract base)
class crFrameDataInitializer
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS:
	// destroyExistingDofs - destroy any existing dofs contained in frame data (default true)
	// filter - pointer to frame filter to use when adding new dofs (default null, all dofs added)
	crFrameDataInitializer(bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: Destructor
	virtual ~crFrameDataInitializer();

	// PURPOSE: Initialize a frame data
	// PARAMS: frameData - frame data to initialize (contents will be altered/destroyed)
	void InitializeFrameData(crFrameData& frameData) const;

protected:

	// PURPOSE: Preinitializing pass, override must be implemented
	// RETURNS: maximum number of new dofs that will be added
	virtual u32 PreInitialize() const = 0;

	// PURPOSE: Initializing pass, override must be implemented
	// RETURNS: true - if dofs added are in order (no sort required), false - unknown/out of order, sort required
	virtual bool Initialize() const = 0;

public:

	// PURPOSE: Add a dof to the frame data
	void FastAddDof(u8 track, u16 id, u8 type) const;

	// PURPOSE: Add another frame data's dofs to the frame data
	void FastAddFrameData(const crFrameData& frameData) const;

private:

	// PURPOSE: Add a dof to the frame data (bypassing any filter checks - assumes filtering already done)
	void FastAddDofUnfiltered(u8 track, u16 id, u8 type) const;

	// PURPOSE: Frame data add iterator
	class AddFrameDataIterator : public crFrameData::Iterator<AddFrameDataIterator>
	{
	public:

		// PURPOSE: Constructor
		AddFrameDataIterator(const crFrameData& frameData, const crFrameDataInitializer& initializer);

		// PURPOSE: Iterate callback
		void IterateDof(const crFrameData::Dof&);

	protected:

		const crFrameDataInitializer* m_Initializer;
	};

	friend class AddFrameDataIterator;

private:
	
	mutable crFrameData* m_FrameData;
	crFrameFilter* m_Filter;
	bool m_DestroyExistingDofs;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Frame data frame data initializer
// Populates frame data with dofs found in another frame data
class crFrameDataInitializerFrameData : public crFrameDataInitializer
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS:
	// sourceFrameData - source frame data to use as template
	// destroyExistingDofs - destroy any existing dofs contained in frame data (default true)
	// filter - pointer to frame filter to use when adding new dofs (default null, all dofs added)
	crFrameDataInitializerFrameData(const crFrameData& sourceFrameData, bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: Destructor
	virtual ~crFrameDataInitializerFrameData();

protected:

	// PURPOSE: Implementation of preinitializing pass
	virtual u32 PreInitialize() const;

	// PURPOSE: Implementation of initializing pass
	virtual bool Initialize() const;

private:

	const crFrameData* m_SourceFrameData;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Bone and mover dofs frame data initializer
// Populates frame data with skeleton dofs, and optionally a pair of mover dofs
class crFrameDataInitializerBoneAndMover : public crFrameDataInitializer
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS:
	// skelData - skeleton data to query for dofs
	// createMoverDofs - optionally create a pair of mover dofs
	// moverId - mover id to use if creating mover dofs
	// destroyExistingDofs - destroy any existing dofs contained in frame data (default true)
	// filter - pointer to frame filter to use when adding new dofs (default null, all dofs added)
	crFrameDataInitializerBoneAndMover(const crSkeletonData& skelData, bool createMoverDofs=true, u16 moverId=0, bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: Destructor
	virtual ~crFrameDataInitializerBoneAndMover();

protected:

	// PURPOSE: Implementation of preinitializing pass
	virtual u32 PreInitialize() const;

	// PURPOSE: Implementation of initializing pass
	virtual bool Initialize() const;

public:

	// PURPOSE: Implementation of preinitializing pass
	static u32 PreInitializeSkeleton(const crSkeletonData& skelData);

	// PURPOSE: Implementation of initializing pass
	static bool InitializeSkeleton(const crFrameDataInitializer& initializer, const crSkeletonData& skelData);

private:

	const crSkeletonData* m_SkeletonData;
	bool m_CreateMoverDofs;
	u16 m_MoverId;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Root bone dofs frame initializer
// Populates frame data with root bone dofs
class crFrameDataInitializerRootBone : public crFrameDataInitializer
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS:
	// destroyExistingDofs - destroy any existing dofs contained in frame data (default true)
	// filter - pointer to frame filter to use when adding new dofs (default null, all dofs added)
	crFrameDataInitializerRootBone(bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: Destructor
	virtual ~crFrameDataInitializerRootBone();

protected:

	// PURPOSE: Implementation of preinitializing pass
	virtual u32 PreInitialize() const;

	// PURPOSE: Implementation of initializing pass
	virtual bool Initialize() const;

private:

};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Root bone dofs frame data initializer
// Populates frame data with root bone dofs
class crFrameDataInitializerMover : public crFrameDataInitializer
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS:
	// moverId - mover id to use when creating mover dofs
	// destroyExistingDofs - destroy any existing dofs contained in frame data (default true)
	// filter - pointer to frame filter to use when adding new dofs (default null, all dofs added)
	crFrameDataInitializerMover(u16 moverId=0, bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: Destructor
	virtual ~crFrameDataInitializerMover();

protected:

	// PURPOSE: Implementation of preinitializing pass
	virtual u32 PreInitialize() const;

	// PURPOSE: Implementation of initializing pass
	virtual bool Initialize() const;

private:
	
	u16 m_MoverId;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: List of dofs frame data initializer
// Populates frame with list of dofs
class crFrameDataInitializerDofs : public crFrameDataInitializer
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS:
	// numDofs - number of dofs to create
	// tracks, id, types - track, id and type of dofs to create (must all have numDof elements)
	// destroyExistingDofs - destroy any existing dofs contained in frame data (default true)
	// filter - pointer to frame filter to use when adding new dofs (default null, all dofs added)
	crFrameDataInitializerDofs(int numDofs, u8 tracks[], u16 ids[], u8 types[], bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: Destructor
	virtual ~crFrameDataInitializerDofs();

protected:

	// PURPOSE: Implementation of preinitializing pass
	virtual u32 PreInitialize() const;

	// PURPOSE: Implementation of initializing pass
	virtual bool Initialize() const;

private:

	int m_NumDofs;
	u8* m_Tracks;
	u16* m_Ids;
	u8* m_Types;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Animation dofs frame data initializer
// Populates frame data with dofs from an animation
class crFrameDataInitializerAnimation : public crFrameDataInitializer
{
public:

	// PURPOSE: Initializing constructor
	// PARAMS:
	// anim - animation to query for dofs
	// destroyExistingDofs - destroy any existing dofs contained in frame data (default true)
	// filter - pointer to frame filter to use when adding new dofs (default null, all dofs added)
	crFrameDataInitializerAnimation(const crAnimation& anim, bool destroyExistingDofs=true, crFrameFilter* filter=NULL);

	// PURPOSE: Destructor
	virtual ~crFrameDataInitializerAnimation();

protected:

	// PURPOSE: Implementation of preinitializing pass
	virtual u32 PreInitialize() const;

	// PURPOSE: Implementation of initializing pass
	virtual bool Initialize() const;

private:

	const crAnimation* m_Animation;
};

////////////////////////////////////////////////////////////////////////////////

__forceinline void crFrameDataInitializer::FastAddDof(u8 track, u16 id, u8 type) const
{
	if(m_Filter)
	{
		m_FrameData->FastAddDof(track, id, type, m_Filter);
	}
	else
	{
		m_FrameData->FastAddDof(track, id, type);
	}
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void crFrameDataInitializer::FastAddDofUnfiltered(u8 track, u16 id, u8 type) const
{
	m_FrameData->FastAddDof(track, id, type);
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameDataInitializer::AddFrameDataIterator::AddFrameDataIterator(const crFrameData& frameData, const crFrameDataInitializer& initializer)
: crFrameData::Iterator<AddFrameDataIterator>(frameData)
, m_Initializer(&initializer)
{
}

////////////////////////////////////////////////////////////////////////////////

__forceinline void crFrameDataInitializer::AddFrameDataIterator::IterateDof(const crFrameData::Dof& dof)
{
	m_Initializer->FastAddDofUnfiltered(dof.m_Track, dof.m_Id, dof.m_Type);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRANIMATION_FRAMEINITIALIZERS_H
