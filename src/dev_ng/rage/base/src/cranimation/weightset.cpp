//
// cranimation/weightset.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "weightset.h"

#include "atl/array_struct.h"
#include "bank/bank.h"
#include "crskeleton/skeletondata.h"
#include "file/asset.h"
#include "file/token.h"

#if CR_DEV
#include "crmetadata/dumpoutput.h"
#endif // CR_DEV

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

crWeightSet::crWeightSet()
: m_SkelData(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crWeightSet::crWeightSet(const crSkeletonData& skelData)
: m_SkelData(NULL)
{
	Init(skelData);
}

////////////////////////////////////////////////////////////////////////////////

crWeightSet::crWeightSet(int maxBones)
: m_SkelData(NULL)
{
	m_Weights.Reserve(maxBones);
}

////////////////////////////////////////////////////////////////////////////////

crWeightSet::crWeightSet(datResource& rsc)
: m_Name(rsc)
, m_Weights(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crWeightSet::crWeightSet(const crWeightSet& other)
{
	CopyThis(other);
}

////////////////////////////////////////////////////////////////////////////////

crWeightSet::~crWeightSet()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_PLACE(crWeightSet);

////////////////////////////////////////////////////////////////////////////////
#if __DECLARESTRUCT
void crWeightSet::DeclareStruct(datTypeStruct& s)
{
	STRUCT_BEGIN(crWeightSet);
	STRUCT_FIELD(m_Name);
	STRUCT_FIELD(m_Weights);
	STRUCT_FIELD(m_SkelData);
	STRUCT_END();
}
#endif // !__FINAL

////////////////////////////////////////////////////////////////////////////////

const crWeightSet& crWeightSet::operator=(const crWeightSet& other)
{
	CopyThis(other);
	return *this;
}

////////////////////////////////////////////////////////////////////////////////

void crWeightSet::Init(const crSkeletonData& skelData)
{
	m_SkelData = &skelData;
	m_Weights.Resize(m_SkelData->GetNumBones());

	const int numWeights = m_Weights.GetCount();
	for(int i=0; i<numWeights; ++i)
	{
		m_Weights[i] = 0.f;
	}
}

////////////////////////////////////////////////////////////////////////////////

void crWeightSet::Shutdown()
{
	m_Name.Reset();
	m_Weights.Reset();

	m_SkelData = NULL;
}

////////////////////////////////////////////////////////////////////////////////

crWeightSet* crWeightSet::AllocateAndLoad(const char* name, const crSkeletonData& skelData)
{
	crWeightSet* weightSet = rage_new crWeightSet();
	
	if(weightSet->Load(name, skelData))
	{
		return weightSet;
	}

	delete weightSet;
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

bool crWeightSet::Load(const char* name, const crSkeletonData& skelData)
{
	Assert(!m_SkelData);
	m_SkelData = &skelData;

	fiStream* s = ASSET.Open(name, "weightset");
	if(!s)
	{
		char fullpath[RAGE_MAX_PATH];
		ASSET.FullPath(fullpath, sizeof(fullpath), name, "weightset");
		Errorf("crWeightSet::Load() - could not open file '%s'", fullpath);
		return false;
	}

	fiAsciiTokenizer tok;
	tok.Init(name, s);

	// TODO --- add magic, version

	m_Name = name;

	tok.MatchToken("NumAnimWeights");
	const int numWeights = tok.GetInt();
	
	Assert(skelData.GetNumBones() == numWeights);
	m_Weights.Resize(numWeights);
	
	for(int i=0; i<numWeights; ++i)
	{
		ASSERT_ONLY(int check =) tok.GetInt();
		Assert(check == i);

		char bone[128];
		tok.GetToken(bone, 128);
		if(strcmp(bone, skelData.GetBoneData(i)->GetName()))
		{
			char fullpath[RAGE_MAX_PATH];
			ASSET.FullPath(fullpath, sizeof(fullpath), name, "weightset");
			Errorf("crWeightSet::Load() - file '%s', bone %i '%s' in file doesn't match bone in skeleton '%s'",
				fullpath, i, bone, skelData.GetBoneData(i)->GetName());
			return false;
		}

		m_Weights[i] = tok.GetFloat();
	}

	s->Close();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crWeightSet::Save(const char* name) const
{
	Assert(m_SkelData);

	if(name == NULL)
	{
		name = m_Name;
	}
	Assert(name);

	fiStream* s = ASSET.Create(name, "weightset");
	if(!s)
	{
		char fullpath[RAGE_MAX_PATH];
		ASSET.FullPath(fullpath, sizeof(fullpath), m_Name, "weightset");
		Errorf("crWeightSet::Save() - Could not create file '%s'", fullpath);
		return false;
	}

	// TODO --- add magic, version

	const int numWeights = m_Weights.GetCount();

	fprintf(s, "NumAnimWeights %i\n", numWeights);
	for(int i=0; i<numWeights; ++i)
	{
		fprintf(s, "%i %s %f\n", i, m_SkelData->GetBoneData(i)->GetName(), m_Weights[i]);
	}

	s->Close();

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void crWeightSet::Serialize(datSerialize& s)
{
	int numWeights = m_Weights.GetCount();
	s << numWeights;
	if (s.IsRead())
	{
		m_Weights.Resize(numWeights);
	}

	for(int i=0; i<numWeights; ++i)
	{
		s << m_Weights[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

void crWeightSet::SetAnimWeight(int weightIdx, float newWeight, bool andChildren)
{
	m_Weights[weightIdx] = newWeight;

	if(andChildren)
	{
		Assert(m_SkelData);
		const crBoneData* bd = m_SkelData->GetBoneData(weightIdx);

		crBoneDataIterator it(bd);
		while(it.GetNext())
		{
			m_Weights[it.GetCurrent()->GetIndex()] = newWeight;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crWeightSet::Set(const crWeightSet& weightSet)
{
	const int numWeights = m_Weights.GetCount();
	for(int i=0; i<numWeights; ++i)
	{
		m_Weights[i] = weightSet.m_Weights[i];
	}
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void crWeightSet::AddWidgets(bkBank& bk)
{
	bk.PushGroup("Weight Set");

	const int numWeights = m_Weights.GetCount();
	for(int i=0; i<numWeights; ++i)
	{
		char buf[128];
		if(m_SkelData)
		{
			formatf(buf, "%s", m_SkelData->GetBoneData(i)->GetName());
		}
		else
		{
			formatf(buf, "weight[%d]", i);
		}

		bk.AddSlider(buf, &m_Weights[i], 0.f, 1.f, 0.001f);
	}

	bk.PopGroup();	
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
void crWeightSet::Dump(crDumpOutput& output) const
{
	output.Outputf(0, "name", "%s", m_Name.c_str()?m_Name.c_str():"");

	int weightCount = m_Weights.GetCount();
	for (int i=0; i<weightCount; ++i)
	{
		output.Outputf(0, "weight", "%f", m_Weights[i]);
	}
}
#endif // CR_DEV


////////////////////////////////////////////////////////////////////////////////

void crWeightSet::CopyThis(const crWeightSet& other)
{
	Shutdown();

	m_Name = other.m_Name;
	m_SkelData = other.m_SkelData;

	const int numWeights = other.m_Weights.GetCount();
	m_Weights.Resize(numWeights);

	Set(other);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
