//
// cranimation/framefilters.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "framefilters.h"

#include "frame.h"
#include "weightset.h"

#include "atl/array_struct.h"
#include "crskeleton/skeletondata.h"
#include "data/resourcehelpers.h"
#include "data/safestruct.h"
#include "file/serialize.h"
#include "zlib/zlib.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

datSerialize &operator <<(datSerialize &s, datOwner<crWeightSet>&)
{
	Assert(0);
	return s;
}

////////////////////////////////////////////////////////////////////////////////

int frameFilterVersions[] =
{
	5, // JM 19-OCT-11 - new .IFF version
};

////////////////////////////////////////////////////////////////////////////////

crFrameFilter::crFrameFilter(eFrameFilterType filterType)
: m_RefCount(1)
, m_Signature(0)
, m_Type(filterType)
{
	Assert(m_Type != kFrameFilterTypeNone);
	Assert(m_Type != kFrameFilterType);
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter::crFrameFilter(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter::~crFrameFilter()
{
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilter::Place(crFrameFilter *filter, datResource& rsc)
{
	const TypeInfo* info = FindTypeInfo(filter->GetType());
	Assert(info);

	PlaceFn* placeFn = info->GetPlaceFn();
	placeFn(filter, rsc);
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
datSwapper_ENUM(crFrameFilter::eFrameFilterType);
void crFrameFilter::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crFrameFilter)
	SSTRUCT_FIELD(crFrameFilter, m_RefCount)
	SSTRUCT_FIELD(crFrameFilter, m_Signature)
	SSTRUCT_FIELD(crFrameFilter, m_Type)
	SSTRUCT_END(crFrameFilter)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crFrameFilter::Allocate(eFrameFilterType filterType)
{
	const TypeInfo* info = FindTypeInfo(filterType);
	if(info)
	{
		AllocateFn* allocateFn = info->GetAllocateFn();
		return allocateFn();
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter* crFrameFilter::AllocateAndLoad(const char* fileName)
{
	Assert(fileName && fileName[0]);

	fiStream* f = ASSET.Open(fileName, s_FileExtension, false, true);
	if(!f)
	{
		Errorf("crFrameFilter::AllocateAndLoad - failed to open file '%s' for reading", fileName);
		return NULL;
	}

	int version;
	int filterType;
	int magic = 0;
	f->ReadInt(&magic, 1);

	if(magic != 'RTLF')
	{
		Errorf("crFrameFilter::AllocateAndLoad - file is not a frame filter file");
		f->Close();
		return NULL;
	}

	fiSerialize s(f, true, false);

	s << datLabel("FrameFilterVersion:") << version << datNewLine;
	s << datLabel("FrameFilterType:") << filterType << datNewLine;

	s_FrameFilterVersion = version;

	int i;
	for(i=NELEM(frameFilterVersions)-1; i>=0; --i)
	{
		if(frameFilterVersions[i] == version)
		{
			break;
		}
	}
	if(i<0)
	{
		Errorf("crFrameFilter::AllocateAndLoad - attempting to load unsupported frame filter '%s' version '%d' (only up to '%d' supported)", fileName, version, frameFilterVersions[0]);
		return NULL;
	}

	crFrameFilter* newFilter = Allocate(eFrameFilterType(filterType));
	if(newFilter)
	{
		if(fiSerializeFrom(f, *newFilter))
		{
			f->Close();
			newFilter->SetSignature(newFilter->CalcSignature());
			return newFilter;
		}
	}
	else
	{
		Errorf("crFrameFilter::AllocateAndLoad - unknown filter '%s' type (%d), no such filter type registered", fileName, filterType);
	}

	Errorf("crFrameFilter::AllocateAndLoad - failed to load file '%s'", fileName);

	f->Close();
	return NULL;

}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilter::Load(const char* fileName)
{
	Assert(fileName);

	fiStream *f = ASSET.Open(fileName, s_FileExtension, false, true);
	if(!f)
	{
		Errorf("crFrameFilter::Load - failed to open file '%s' for reading", fileName);
		return false;
	}

	int version;
	int filterType;
	int magic = 0;
	f->ReadInt(&magic, 1);
	if(magic != 'RTLF')
	{
		Errorf("crFrameFilter::Load - file is not a frame filter file");
		f->Close();
		return false;
	}

	fiSerialize s(f, true, true);

	s << datLabel("FrameFilterVersion:") << version << datNewLine;
	s << datLabel("FrameFilterType:") << filterType << datNewLine;

	s_FrameFilterVersion = version;

	int i;
	for(i=NELEM(frameFilterVersions)-1; i>=0; --i)
	{
		if(frameFilterVersions[i] == version)
		{
			break;
		}
	}
	if(i<0)
	{
		Errorf("crFrameFilter::Load - attempting to load unsupported frame filter '%s' version '%d' (only up to '%d' supported)", fileName, version, frameFilterVersions[0]);
		return false;
	}

	if(filterType == GetType())
	{
		if(fiSerializeFrom(f, *this))
		{
			f->Close();
			SetSignature(CalcSignature());
			return true;
		}
	}
	else
	{
#if CR_DEV
		Errorf("crFrameFilter::Load - type mismatch, structure is of type %s(%d) while file '%s' defines type %s(%d)", GetTypeName(), GetType(), fileName, FindTypeInfo(eFrameFilterType(filterType))?FindTypeInfo(eFrameFilterType(filterType))->GetName():"UNKNOWN", filterType);
#endif
	}

	Errorf("crFrameFilter::Load - failed to load file '%s'", fileName);

	f->Close();
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilter::Save(const char* fileName) const
{
	Assert(fileName);

	fiStream *f = ASSET.Create(fileName, s_FileExtension);
	if(!f)
	{
		Errorf("crFrameFilter - failed to open file '%s' for writing", fileName);
		return false;
	}

	int magic = 'RTLF';
	f->WriteInt(&magic, 1);

	fiSerialize s(f, false, true);

	int version = frameFilterVersions[0];
	int filterType = int(GetType());
	s << datLabel("FrameFilterVersion:") << version << datNewLine;
	s << datLabel("FrameFilterType:") << filterType << datNewLine;

	bool success = !s.HasError() && fiSerializeTo(f, *const_cast< crFrameFilter *>(this), true);
	if(!success)
	{
		Errorf("crFrameFilter - failed to save file '%s'", fileName);
	}

	f->Close();

	return success;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilter::Serialize(datSerialize& s)
{
	int filterType = m_Type;
	s << filterType;
	m_Type = eFrameFilterType(filterType);
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilter::IsCastableToType(eFrameFilterType filterType) const
{
	return (filterType == kFrameFilterType);
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
const char* crFrameFilter::GetTypeName() const
{
	const TypeInfo* typeInfo = FindTypeInfo(GetType());
	if(typeInfo)
	{
		return typeInfo->GetName();
	}
	return NULL;
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilter::FilterDof(u8, u16, float&)
{
	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilter::FilterDof(u8, u16, float&, crFrameFilter*)
{
	return true;
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void crFrameFilter::AddWidgets(bkBank&)
{
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

void crFrameFilter::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		sm_TypeInfos.Resize(kFrameFilterTypeRageFilterNum);
		for(int i=0; i<kFrameFilterTypeRageFilterNum; ++i)
		{
			sm_TypeInfos[i] = NULL;
		}

		// register all built in filter types
		crFrameFilterBone::InitClass();
		crFrameFilterBoneBasic::InitClass();
		crFrameFilterBoneMultiWeight::InitClass();
		crFrameFilterMultiWeight::InitClass();
		crFrameFilterTrackMultiWeight::InitClass();
		crFrameFilterMover::InitClass();
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilter::ShutdownClass()
{
	sm_TypeInfos.Reset();

	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter::TypeInfo::TypeInfo(eFrameFilterType filterType, const char* name, AllocateFn* allocateFn, PlaceFn* placeFn)
: m_Type(filterType)
, m_Name(name)
, m_AllocateFn(allocateFn)
, m_PlaceFn(placeFn)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter::TypeInfo::~TypeInfo()
{
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilter::TypeInfo::Register() const
{
	crFrameFilter::RegisterTypeInfo(*this);
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter::eFrameFilterType crFrameFilter::TypeInfo::GetType() const
{
	return m_Type;
}

////////////////////////////////////////////////////////////////////////////////

const char* crFrameFilter::TypeInfo::GetName() const
{
	return m_Name;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter::AllocateFn* crFrameFilter::TypeInfo::GetAllocateFn() const
{
	return m_AllocateFn;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilter::PlaceFn* crFrameFilter::TypeInfo::GetPlaceFn() const
{
	return m_PlaceFn;
}

////////////////////////////////////////////////////////////////////////////////

const crFrameFilter::TypeInfo* crFrameFilter::FindTypeInfo(eFrameFilterType filterType)
{
	Assert(filterType != kFrameFilterTypeNone);
	Assert(sm_TypeInfos.GetCount() >= kFrameFilterTypeRageFilterNum);
	if(filterType < kFrameFilterTypeRageFilterNum)
	{
		return sm_TypeInfos[filterType];
	}
	else
	{
		const int numTypeInfos = sm_TypeInfos.GetCount();
		for(int i=kFrameFilterTypeRageFilterNum; i<numTypeInfos; ++i)
		{
			Assert(sm_TypeInfos[i]);
			if(sm_TypeInfos[i]->GetType() == filterType)
			{
				return sm_TypeInfos[i];
			}
			else if(sm_TypeInfos[i]->GetType() > filterType)
			{
				break;
			}
		}
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilter::RegisterTypeInfo(const TypeInfo& info)
{
	Assert(info.GetType() != kFrameFilterTypeNone);
	Assert(sm_TypeInfos.GetCount() >= kFrameFilterTypeRageFilterNum);
	if(info.GetType() < kFrameFilterTypeRageFilterNum)
	{
		Assert(!sm_TypeInfos[info.GetType()]);

		sm_TypeInfos[info.GetType()] = &info;
	}
	else
	{
		sm_TypeInfos.Grow() = NULL;

		int i;
		for(i=sm_TypeInfos.GetCount()-1; i>kFrameFilterTypeRageFilterNum; --i)
		{
			Assert(!sm_TypeInfos[i]);
			Assert(sm_TypeInfos[i-1]);
			if(sm_TypeInfos[i-1]->GetType() < info.GetType())
			{
				break;
			}

			Assert(sm_TypeInfos[i-1]->GetType() != info.GetType());
			sm_TypeInfos[i] = sm_TypeInfos[i-1];
			sm_TypeInfos[i-1] = NULL;
		}
		
		Assert(!sm_TypeInfos[i]);
		sm_TypeInfos[i] = &info;
	}
}

////////////////////////////////////////////////////////////////////////////////

atArray<const crFrameFilter::TypeInfo*> crFrameFilter::sm_TypeInfos;
bool crFrameFilter::sm_InitClassCalled = false;
int crFrameFilter::s_FrameFilterVersion = -1;
const char* crFrameFilter::s_FileExtension = "iff";

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBone::crFrameFilterBone(eFrameFilterType filterType)
: crFrameFilter(filterType)
, m_NonBoneDofsAllowed(true)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBone::crFrameFilterBone()
: crFrameFilter(kFrameFilterTypeBone)
, m_NonBoneDofsAllowed(true)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBone::crFrameFilterBone(datResource &rsc)
: crFrameFilter(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBone::~crFrameFilterBone()
{
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_FRAME_FILTER_TYPE(crFrameFilterBone, kFrameFilterTypeBone, crFrameFilter)

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crFrameFilterBone::DeclareStruct(datTypeStruct& s)
{
	crFrameFilter::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(crFrameFilterBone, crFrameFilter)
	SSTRUCT_FIELD(crFrameFilterBone, m_NonBoneDofsAllowed)
	SSTRUCT_IGNORE(crFrameFilterBone, m_Padding)
	SSTRUCT_END(crFrameFilterBone)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBone::Serialize(datSerialize& s)
{
	crFrameFilter::Serialize(s);
	s << m_NonBoneDofsAllowed;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilterBone::GetNonBoneDofsAllowed() const
{
	return m_NonBoneDofsAllowed;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBone::SetNonBoneDofsAllowed(bool allow)
{
	m_NonBoneDofsAllowed = allow;
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void crFrameFilterBone::AddWidgets(bkBank& bank)
{
	crFrameFilter::AddWidgets(bank);

	bank.AddToggle("Non Bone Dofs Allowed", &m_NonBoneDofsAllowed);
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

u32 crFrameFilterBone::CalcSignature() const
{
	return crc32(0, (const u8*)&m_NonBoneDofsAllowed, sizeof(bool));
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBoneBasic::crFrameFilterBoneBasic()
: crFrameFilterBone(kFrameFilterTypeBoneBasic)
, m_SkelData(NULL)
, m_WeightSet(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBoneBasic::crFrameFilterBoneBasic(eFrameFilterType filterType)
: crFrameFilterBone(filterType)
, m_SkelData(NULL)
, m_WeightSet(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBoneBasic::crFrameFilterBoneBasic(const crSkeletonData& skelData)
: crFrameFilterBone(kFrameFilterTypeBoneBasic)
, m_SkelData(&skelData)
{
	m_WeightSet = rage_new crWeightSet(skelData);

	// initialize contents of weight set to zero
	for(int i=0; i<skelData.GetNumBones(); i++)
	{
		(*m_WeightSet)[i] = 0.f;
	}

	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBoneBasic::crFrameFilterBoneBasic(int maxBones)
: crFrameFilterBone(kFrameFilterTypeBoneBasic)
, m_SkelData(NULL)
{
	m_WeightSet = rage_new crWeightSet(maxBones);

	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBoneBasic::crFrameFilterBoneBasic(datResource &rsc)
: crFrameFilterBone(rsc)
, m_SkelData(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBoneBasic::~crFrameFilterBoneBasic()
{
	delete m_WeightSet;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::Init(const crSkeletonData& skelData)
{
	m_SkelData = &skelData;
	if(m_WeightSet)
	{
		m_WeightSet->Init(skelData);
	}
	else
	{
		m_WeightSet = rage_new crWeightSet(skelData);

		// initialize contents of weight set to zero
		for(int i=0; i<skelData.GetNumBones(); i++)
		{
			(*m_WeightSet)[i] = 0.f;
		}
	}
	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::Init(int maxBones)
{
	m_WeightSet = rage_new crWeightSet(maxBones);

	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_FRAME_FILTER_TYPE(crFrameFilterBoneBasic, kFrameFilterTypeBoneBasic, crFrameFilterBone)

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crFrameFilterBoneBasic::DeclareStruct(datTypeStruct& s)
{
	crFrameFilterBone::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(crFrameFilterBoneBasic, crFrameFilterBone)
	SSTRUCT_SKIP(crFrameFilterBoneBasic, m_SkelData, sizeof(datRef<crSkeletonData>))
	SSTRUCT_FIELD(crFrameFilterBoneBasic, m_WeightSet)
	SSTRUCT_END(crFrameFilterBoneBasic)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::Serialize(datSerialize& s)
{
	crFrameFilterBone::Serialize(s);
	s << m_WeightSet;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::AddWeightSet(const crWeightSet& weightSet)
{
	Assert(weightSet.GetNumAnimWeights() == m_WeightSet->GetNumAnimWeights());
	m_WeightSet->Set(weightSet);
	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::AddBone(const crBoneData* bd, bool andChildren, float weight)
{
	Assert(bd);
	m_WeightSet->SetAnimWeight(bd->GetIndex(), weight, andChildren);
	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::AddBoneIndex(int boneIdx, bool andChildren, float weight)
{
	const crBoneData* bd = m_SkelData->GetBoneData(boneIdx);
	Assert(bd);

	AddBone(bd, andChildren, weight);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::AddBoneId(u16 boneId, bool andChildren, float weight)
{
	int boneIdx;
	if (AssertVerify(m_SkelData->ConvertBoneIdToIndex(boneId, boneIdx)))
	{
		AddBoneIndex(boneIdx, andChildren, weight);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::RemoveBone(const crBoneData* bd, bool andChildren)
{
	AddBone(bd, andChildren, 0.f);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::RemoveBoneIndex(int boneIdx, bool andChildren)
{
	AddBoneIndex(boneIdx, andChildren, 0.f);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::RemoveBoneId(u16 boneId, bool andChildren)
{
	AddBoneId(boneId, andChildren, 0.f);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneBasic::InvertWeights()
{
	int nBones = m_SkelData->GetNumBones();
	for(int i=0; i<nBones; ++i)
	{
		(*m_WeightSet)[i] = Clamp(1.f - (*m_WeightSet)[i], 0.f, 1.f);
	}
	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilterBoneBasic::FilterDof(u8 track, u16 id, float& inoutWeight)
{
	if(track == kTrackBoneRotation || track == kTrackBoneTranslation || track == kTrackBoneScale)
	{
		int boneIdx;
		if(m_SkelData->ConvertBoneIdToIndex(id, boneIdx))
		{
			inoutWeight = m_WeightSet->GetAnimWeight(boneIdx);
			return (inoutWeight > 0.f);
		}
	}
	else
	{
		return GetNonBoneDofsAllowed();
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void crFrameFilterBoneBasic::AddWidgets(bkBank& bank)
{
	crFrameFilterBone::AddWidgets(bank);

	if(m_WeightSet)
	{
		m_WeightSet->AddWidgets(bank);
	}
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

u32 crFrameFilterBoneBasic::CalcSignature() const
{
	u32 signature = crFrameFilterBone::CalcSignature();
	signature = crc32(signature, (const u8*)m_WeightSet->GetAnimWeights(), m_WeightSet->GetNumAnimWeights()*sizeof(float));

	return signature;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBoneMultiWeight::crFrameFilterBoneMultiWeight()
: crFrameFilterBone(kFrameFilterTypeBoneMultiWeight)
, m_SkelData(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBoneMultiWeight::crFrameFilterBoneMultiWeight(const crSkeletonData& skelData, int numWeightGroups)
: crFrameFilterBone(kFrameFilterTypeBoneMultiWeight)
, m_SkelData(NULL)
{
	Init(skelData, numWeightGroups);
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBoneMultiWeight::crFrameFilterBoneMultiWeight(datResource &rsc)
: crFrameFilterBone(rsc)
, m_WeightIndices(rsc)
, m_Weights(rsc)
, m_SkelData(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterBoneMultiWeight::~crFrameFilterBoneMultiWeight()
{
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneMultiWeight::Init(const crSkeletonData& skelData, int numWeightGroups)
{
	const int nBones = skelData.GetNumBones();
	m_WeightIndices.Resize(nBones);
	for(int i=0; i<nBones; ++i)
	{
		m_WeightIndices[i] = -1;
	}

	Assert(numWeightGroups > 0);
	m_Weights.Resize(numWeightGroups);
	for(int i=0; i<numWeightGroups; ++i)
	{
		m_Weights[i] = 0.f;
	}
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_FRAME_FILTER_TYPE(crFrameFilterBoneMultiWeight, kFrameFilterTypeBoneMultiWeight, crFrameFilterBone)

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crFrameFilterBoneMultiWeight::DeclareStruct(datTypeStruct& s)
{
	crFrameFilterBone::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(crFrameFilterBoneMultiWeight, crFrameFilterBone)
	SSTRUCT_SKIP(crFrameFilterBoneMultiWeight, m_SkelData, sizeof(datRef< crSkeletonData >))
	SSTRUCT_FIELD(crFrameFilterBoneMultiWeight, m_WeightIndices)
	SSTRUCT_FIELD(crFrameFilterBoneMultiWeight, m_Weights)
	SSTRUCT_END(crFrameFilterBoneMultiWeight)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneMultiWeight::Serialize(datSerialize& s)
{
	crFrameFilterBone::Serialize(s);
	s << m_WeightIndices;
	s << m_Weights;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneMultiWeight::AddBone(const crBoneData* bd, int weightGroupIdx, bool andChildren)
{
	m_WeightIndices[bd->GetIndex()] = weightGroupIdx;

	if(andChildren)
	{
		crBoneDataIterator it(bd);
		while(it.GetNext())
		{
			m_WeightIndices[it.GetCurrent()->GetIndex()] = weightGroupIdx;
		}
	}
	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneMultiWeight::AddBoneIndex(int boneIdx, int weightGroupIdx, bool andChildren)
{
	const crBoneData* bd = m_SkelData->GetBoneData(boneIdx);
	Assert(bd);

	AddBone(bd, weightGroupIdx, andChildren);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneMultiWeight::AddBoneId(u16 boneId, int weightGroupIdx, bool andChildren)
{
	int boneIdx;
	if (AssertVerify(m_SkelData->ConvertBoneIdToIndex(boneId, boneIdx)))
	{
		AddBoneIndex(boneIdx, weightGroupIdx, andChildren);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneMultiWeight::RemoveBone(const crBoneData* bd, bool andChildren)
{
	AddBone(bd, -1, andChildren);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneMultiWeight::RemoveBoneIndex(int boneIdx, bool andChildren)
{
	AddBoneIndex(boneIdx, -1, andChildren);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneMultiWeight::RemoveBoneId(u16 boneId, bool andChildren)
{
	AddBoneId(boneId, -1, andChildren);
}

////////////////////////////////////////////////////////////////////////////////

int crFrameFilterBoneMultiWeight::GetNumWeightGroups() const
{
	return m_Weights.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

float crFrameFilterBoneMultiWeight::GetWeight(int weightGroupIdx) const
{
	return m_Weights[weightGroupIdx];
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterBoneMultiWeight::SetWeight(float weight, int weightGroupIdx)
{
	m_Weights[weightGroupIdx] = weight;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilterBoneMultiWeight::FilterDof(u8 track, u16 id, float& inoutWeight)
{
	if(track == kTrackBoneRotation || track == kTrackBoneTranslation || track == kTrackBoneScale)
	{
		int boneIdx;
		if(m_SkelData->ConvertBoneIdToIndex(id, boneIdx))
		{
			int weightIdx = m_WeightIndices[boneIdx];
			if(weightIdx >= 0)
			{
				inoutWeight = m_Weights[weightIdx];
				return (inoutWeight > 0.f);
			}
		}
	}
	else
	{
		return GetNonBoneDofsAllowed();
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameFilterBoneMultiWeight::CalcSignature() const
{
	u32 signature = crFrameFilterBone::CalcSignature();
	if(m_WeightIndices.GetCount())
	{
		signature = crc32(signature, (const u8*)&m_WeightIndices[0], m_WeightIndices.GetCount()*sizeof(int));
	}
	if(m_Weights.GetCount())
	{
		signature = crc32(signature, (const u8*)&m_Weights[0], m_Weights.GetCount()*sizeof(float));
	}

	return signature;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMultiWeight::crFrameFilterMultiWeight(eFrameFilterType filterType)
: crFrameFilter(filterType)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMultiWeight::crFrameFilterMultiWeight()
: crFrameFilter(kFrameFilterTypeMultiWeight)
, m_SkelData(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMultiWeight::crFrameFilterMultiWeight(int numWeightGroups, const crSkeletonData* skelData)
: crFrameFilter(kFrameFilterTypeMultiWeight)
, m_SkelData(NULL)
{
	Init(numWeightGroups, skelData);
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMultiWeight::crFrameFilterMultiWeight(datResource &rsc)
: crFrameFilter(rsc)
, m_TrackIdIndices(rsc)
, m_Weights(rsc)
, m_SkelData(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMultiWeight::~crFrameFilterMultiWeight()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::Init(int numWeightGroups, const crSkeletonData* skelData)
{
	Assert(numWeightGroups > 0);

	m_Weights.Resize(numWeightGroups);
	for(int i=0; i<numWeightGroups; ++i)
	{
		m_Weights[i] = 0.f;
	}

	m_SkelData = skelData;

	SetSignature(0);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::Shutdown()
{
	m_TrackIdIndices.Reset();
	m_Weights.Reset();

	m_SkelData = NULL;

	SetSignature(0);
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_FRAME_FILTER_TYPE(crFrameFilterMultiWeight, kFrameFilterTypeMultiWeight, crFrameFilter)

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crFrameFilterMultiWeight::DeclareStruct(datTypeStruct& s)
{
	crFrameFilter::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(crFrameFilterMultiWeight, crFrameFilter)
	SSTRUCT_FIELD(crFrameFilterMultiWeight, m_TrackIdIndices)
	SSTRUCT_FIELD(crFrameFilterMultiWeight, m_Weights)
	SSTRUCT_SKIP(crFrameFilterMultiWeight, m_SkelData, sizeof(datRef<crSkeletonData>))
	SSTRUCT_END(crFrameFilterMultiWeight)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::Serialize(datSerialize& s)
{
	crFrameFilter::Serialize(s);
	s << m_TrackIdIndices;
	s << m_Weights;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::Add(u8 track, u16 id, int weightGroupIdx)
{
	Assert(weightGroupIdx >= 0);
	Assert(weightGroupIdx < m_Weights.GetCount());

	int idx;
	if(!FindTrackIdIndex(track, id, idx))
	{
		m_TrackIdIndices.Grow();
		m_TrackIdIndices.DeleteFast(m_TrackIdIndices.GetCount()-1);
		m_TrackIdIndices.Insert(idx) = TrackIdIndex(track, id, weightGroupIdx);
	}
	else
	{
		m_TrackIdIndices[idx].m_Index = weightGroupIdx;
	}

	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::AddBoneIndex(int boneIdx, bool translation, bool rotation, bool scale, bool andChildren, int weightGroupIdx)
{
	Assert(m_SkelData);
	AddBoneData(*m_SkelData->GetBoneData(boneIdx), translation, rotation, scale, andChildren, weightGroupIdx);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::AddBoneId(u16 boneId, bool translation, bool rotation, bool scale, bool andChildren, int weightGroupIdx)
{
	Assert(m_SkelData);

	int idx;
	if(m_SkelData->ConvertBoneIdToIndex(boneId, idx))
	{
		AddBoneIndex(idx, translation, rotation, scale, andChildren, weightGroupIdx);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::AddBoneData(const crBoneData& boneData, bool translation, bool rotation, bool scale, bool andChildren, int weightGroupIdx)
{
	Assert(m_SkelData);

	if(translation)
	{
		Add(kTrackBoneTranslation, boneData.GetBoneId(), weightGroupIdx);
	}
	if(rotation)
	{
		Add(kTrackBoneRotation, boneData.GetBoneId(), weightGroupIdx);
	}
	if(scale)
	{
		Add(kTrackBoneScale, boneData.GetBoneId(), weightGroupIdx);
	}

	if(andChildren)
	{
		const crBoneData* bd = boneData.GetChild();
		while(bd)
		{
			AddBoneData(*bd, translation, rotation, scale, andChildren, weightGroupIdx);
			bd = bd->GetNext();
		}
	}	
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::Remove(u8 track, u16 id)
{
	int idx;
	if(FindTrackIdIndex(track, id, idx))
	{
		m_TrackIdIndices.Delete(idx);
	}

	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::RemoveBoneIndex(int boneIdx, bool translation, bool rotation, bool scale, bool andChildren)
{
	Assert(m_SkelData);
	RemoveBoneData(*m_SkelData->GetBoneData(boneIdx), translation, rotation, scale, andChildren);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::RemoveBoneId(u16 boneId, bool translation, bool rotation, bool scale, bool andChildren)
{
	Assert(m_SkelData);

	int idx;
	if(m_SkelData->ConvertBoneIdToIndex(boneId, idx))
	{
		RemoveBoneIndex(idx, translation, rotation, scale, andChildren);
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::RemoveBoneData(const crBoneData& boneData, bool translation, bool rotation, bool scale, bool andChildren)
{
	Assert(m_SkelData);

	if(translation)
	{
		Remove(kTrackBoneTranslation, boneData.GetBoneId());
	}
	if(rotation)
	{
		Remove(kTrackBoneRotation, boneData.GetBoneId());
	}
	if(scale)
	{
		Remove(kTrackBoneScale, boneData.GetBoneId());
	}
	
	if(andChildren)
	{
		const crBoneData* bd = boneData.GetChild();
		while(bd)
		{
			RemoveBoneData(*bd, translation, rotation, scale, andChildren);
			bd = bd->GetNext();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

int crFrameFilterMultiWeight::GetNumWeightGroups() const
{
	return m_Weights.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

float crFrameFilterMultiWeight::GetWeight(int weightGroupIdx) const
{
	return m_Weights[weightGroupIdx];
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilterMultiWeight::FilterDof(u8 track, u16 id, float& inoutWeight)
{
	int idx;
	if(FindTrackIdIndex(track, id, idx))
	{
		int weightIdx = m_TrackIdIndices[idx].m_Index;
		inoutWeight = m_Weights[weightIdx];
		return (inoutWeight > 0.f);
	}

	inoutWeight = 0.f;
	return false;
}

////////////////////////////////////////////////////////////////////////////////

int crFrameFilterMultiWeight::GetEntryCount() const
{
	return m_TrackIdIndices.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::GetEntry(int i, u8 &track, u16 &id, int &index, float &weight) const
{
	if(i < m_TrackIdIndices.GetCount())
	{
		const TrackIdIndex &trackIdIndex = m_TrackIdIndices[i];
		track = trackIdIndex.m_Track;
		id = trackIdIndex.m_Id;
		index = trackIdIndex.m_Index;
		if(index < m_Weights.GetCount())
		{
			weight = m_Weights[index];
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::SetWeight(int weightGroupIdx, float weight)
{
	m_Weights[weightGroupIdx] = weight;

	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameFilterMultiWeight::CalcSignature() const
{
	u32 signature = 0;

	if(m_TrackIdIndices.GetCount())
	{
		signature = crc32(signature, (const u8*)&m_TrackIdIndices[0], m_TrackIdIndices.GetCount()*sizeof(TrackIdIndex));
	}
	if(m_Weights.GetCount())
	{
		signature = crc32(signature, (const u8*)&m_Weights[0], m_Weights.GetCount()*sizeof(float));
	}

	return signature;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilterMultiWeight::FindTrackIdIndex(u8 track, u16 id, int& outIdx)
{
	u32 trackId = (u32(track)<<16)|u32(id);

	int low = 0;
	int high = m_TrackIdIndices.GetCount()-1;

	while(low <= high)
	{
		outIdx = (low+high) >> 1;

		u32 ti = (u32(m_TrackIdIndices[outIdx].m_Track)<<16)|u32(m_TrackIdIndices[outIdx].m_Id);

		if(trackId == ti)
		{
			return true;
		}
		else if(trackId < ti)
		{
			high = outIdx-1;
		}
		else
		{
			low = outIdx+1;
		}
	}

	outIdx = low;
	return false;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMultiWeight::TrackIdIndex::TrackIdIndex()
: m_Track(0xff)
, m_Id(0xffff)
, m_Index(-1)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMultiWeight::TrackIdIndex::TrackIdIndex(u8 track, u16 id, int index)
: m_Track(track)
, m_Id(id)
, m_Index(index)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMultiWeight::TrackIdIndex::TrackIdIndex(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMultiWeight::TrackIdIndex::~TrackIdIndex()
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crFrameFilterMultiWeight::TrackIdIndex::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crFrameFilterMultiWeight::TrackIdIndex)
	SSTRUCT_IGNORE(crFrameFilterMultiWeight::TrackIdIndex, m_Padding)
	SSTRUCT_FIELD(crFrameFilterMultiWeight::TrackIdIndex, m_Track)
	SSTRUCT_FIELD(crFrameFilterMultiWeight::TrackIdIndex, m_Id)
	SSTRUCT_FIELD(crFrameFilterMultiWeight::TrackIdIndex, m_Index)
	SSTRUCT_END(crFrameFilterMultiWeight::TrackIdIndex)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMultiWeight::TrackIdIndex::Serialize(datSerialize& s)
{
	s << m_Track;
	s << m_Id;
	s << m_Index;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterTrackMultiWeight::crFrameFilterTrackMultiWeight()
: crFrameFilter(kFrameFilterTypeTrackMultiWeight)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterTrackMultiWeight::crFrameFilterTrackMultiWeight(int numWeightGroups)
: crFrameFilter(kFrameFilterTypeTrackMultiWeight)
{
	Init(numWeightGroups);
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterTrackMultiWeight::crFrameFilterTrackMultiWeight(datResource &rsc)
: crFrameFilter(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterTrackMultiWeight::~crFrameFilterTrackMultiWeight()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterTrackMultiWeight::Init(int numWeightGroups)
{
	Assert(numWeightGroups > 0);

	m_Weights.Resize(numWeightGroups);
	for(int i=0; i<numWeightGroups; ++i)
	{
		m_Weights[i] = 0.f;
	}

	SetSignature(0);
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterTrackMultiWeight::Shutdown()
{
	m_TrackIndices.Reset();
	m_Weights.Reset();

	SetSignature(0);
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_FRAME_FILTER_TYPE(crFrameFilterTrackMultiWeight, kFrameFilterTypeTrackMultiWeight, crFrameFilter)

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crFrameFilterTrackMultiWeight::DeclareStruct(datTypeStruct& s)
{
	crFrameFilter::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(crFrameFilterTrackMultiWeight, crFrameFilter)
	SSTRUCT_FIELD(crFrameFilterTrackMultiWeight, m_TrackIndices)
	SSTRUCT_FIELD(crFrameFilterTrackMultiWeight, m_Weights)
	SSTRUCT_END(crFrameFilterTrackMultiWeight)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterTrackMultiWeight::Serialize(datSerialize& s)
{
	crFrameFilter::Serialize(s);
	s << m_TrackIndices;
	s << m_Weights;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterTrackMultiWeight::Add(u8 track, int weightGroupIdx)
{
	Assert(weightGroupIdx >= 0);
	Assert(weightGroupIdx < m_Weights.GetCount());

	int idx;
	if(!FindTrackIndex(track, idx))
	{
		m_TrackIndices.Grow();
		m_TrackIndices.DeleteFast(m_TrackIndices.GetCount()-1);
		m_TrackIndices.Insert(idx) = TrackIndex(track, weightGroupIdx);
	}
	else
	{
		m_TrackIndices[idx].m_Index = weightGroupIdx;
	}

	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterTrackMultiWeight::Remove(u8 track)
{
	int idx;
	if(FindTrackIndex(track, idx))
	{
		m_TrackIndices.Delete(idx);
	}

	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

int crFrameFilterTrackMultiWeight::GetNumWeightGroups() const
{
	return m_Weights.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

float crFrameFilterTrackMultiWeight::GetWeight(int weightGroupIdx) const
{
	return m_Weights[weightGroupIdx];
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilterTrackMultiWeight::FilterDof(u8 track, u16, float& inoutWeight)
{
	int idx;
	if(FindTrackIndex(track, idx))
	{
		int weightIdx = m_TrackIndices[idx].m_Index;
		inoutWeight = m_Weights[weightIdx];
		return (inoutWeight > 0.f);
	}

	inoutWeight = 0.f;
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterTrackMultiWeight::SetWeight(int weightGroupIdx, float weight)
{
	m_Weights[weightGroupIdx] = weight;

	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

u32 crFrameFilterTrackMultiWeight::CalcSignature() const
{
	u32 signature = 0;

	if(m_TrackIndices.GetCount())
	{
		signature = crc32(signature, (const u8*)&m_TrackIndices[0], m_TrackIndices.GetCount()*sizeof(TrackIndex));
	}
	if(m_Weights.GetCount())
	{
		signature = crc32(signature, (const u8*)&m_Weights[0], m_Weights.GetCount()*sizeof(float));
	}

	return signature;
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilterTrackMultiWeight::FindTrackIndex(u8 track,  int& outIdx)
{
	int low = 0;
	int high = m_TrackIndices.GetCount()-1;

	while(low <= high)
	{
		outIdx = (low+high) >> 1;

		u8 t = m_TrackIndices[outIdx].m_Track;

		if(track == t)
		{
			return true;
		}
		else if(track < t)
		{
			high = outIdx-1;
		}
		else
		{
			low = outIdx+1;
		}
	}

	outIdx = low;
	return false;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterTrackMultiWeight::TrackIndex::TrackIndex()
: m_Track(0xff)
, m_Index(-1)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterTrackMultiWeight::TrackIndex::TrackIndex(u8 track, int index)
: m_Track(track)
, m_Index(index)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterTrackMultiWeight::TrackIndex::TrackIndex(datResource&)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterTrackMultiWeight::TrackIndex::~TrackIndex()
{
}

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crFrameFilterTrackMultiWeight::TrackIndex::DeclareStruct(datTypeStruct& s)
{
	SSTRUCT_BEGIN(crFrameFilterTrackMultiWeight::TrackIndex)
	SSTRUCT_IGNORE(crFrameFilterTrackMultiWeight::TrackIndex, m_Padding)
	SSTRUCT_FIELD(crFrameFilterTrackMultiWeight::TrackIndex, m_Track)
	SSTRUCT_FIELD(crFrameFilterTrackMultiWeight::TrackIndex, m_Index)
	SSTRUCT_END(crFrameFilterTrackMultiWeight::TrackIndex)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterTrackMultiWeight::TrackIndex::Serialize(datSerialize& s)
{
	s << m_Track;
	s << m_Index;
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMover::crFrameFilterMover()
: crFrameFilter(kFrameFilterTypeMover)
, m_MoverTranslation(0.f)
, m_MoverRotation(0.f)
, m_MoverId(0)
, m_NonMoverDofsAllowed(true)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMover::crFrameFilterMover(datResource &rsc)
: crFrameFilter(rsc)
{
}

////////////////////////////////////////////////////////////////////////////////

crFrameFilterMover::~crFrameFilterMover()
{
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_FRAME_FILTER_TYPE(crFrameFilterMover, kFrameFilterTypeMover, crFrameFilter)

////////////////////////////////////////////////////////////////////////////////

#if __DECLARESTRUCT
void crFrameFilterMover::DeclareStruct(datTypeStruct& s)
{
	crFrameFilter::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(crFrameFilterMover, crFrameFilter)
	SSTRUCT_FIELD(crFrameFilterMover, m_MoverTranslation)
	SSTRUCT_FIELD(crFrameFilterMover, m_MoverRotation)
	SSTRUCT_FIELD(crFrameFilterMover, m_MoverId)
	SSTRUCT_FIELD(crFrameFilterMover, m_NonMoverDofsAllowed)
	SSTRUCT_IGNORE(crFrameFilterMover, m_Padding)
	SSTRUCT_END(crFrameFilterMover)
}
#endif // __DECLARESTRUCT

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMover::Serialize(datSerialize& s)
{
	crFrameFilter::Serialize(s);
	s << m_MoverTranslation;
	s << m_MoverRotation;
	s << m_MoverId;
	s << m_NonMoverDofsAllowed;
	//s << m_Padding;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMover::GetMoverDofWeights(float& outTranslateWeight, float& outRotateWeight) const
{
	outTranslateWeight = m_MoverTranslation;
	outRotateWeight = m_MoverRotation;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMover::SetMoverDofWeights(float translateWeight, float rotateWeight)
{
	m_MoverTranslation = translateWeight;
	m_MoverRotation = rotateWeight;
	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

u16 crFrameFilterMover::GetMoverId() const
{
	return m_MoverId;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMover::SetMoverId(u16 moverId)
{
	m_MoverId = moverId;
	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilterMover::GetNonMoverDofsAllowed() const
{
	return m_NonMoverDofsAllowed;
}

////////////////////////////////////////////////////////////////////////////////

void crFrameFilterMover::SetNonMoverDofsAllowed(bool allow)
{
	m_NonMoverDofsAllowed = allow;
	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

bool crFrameFilterMover::FilterDof(u8 track, u16 id, float& inoutWeight)
{
	switch(track)
	{
	case kTrackMoverTranslation:
		if(id == m_MoverId)
		{
			inoutWeight = m_MoverTranslation;
			return (inoutWeight > 0.f);
		}
		break;

	case kTrackMoverRotation:
		if(id == m_MoverId)
		{
			inoutWeight = m_MoverRotation;
			return (inoutWeight > 0.f);
		}
		break;
	}

	if(!m_NonMoverDofsAllowed)
	{
		inoutWeight = 0.f;
	}

	return (inoutWeight > 0.f);
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void crFrameFilterMover::AddWidgets(bkBank& bk)
{
	crFrameFilter::AddWidgets(bk);
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

u32 crFrameFilterMover::CalcSignature() const
{
	u32 signature = 0;
	signature = crc32(signature, (const u8*)&m_MoverTranslation, sizeof(float));
	signature = crc32(signature, (const u8*)&m_MoverRotation, sizeof(float));
	signature = crc32(signature, (const u8*)&m_MoverId, sizeof(u16));
	signature = crc32(signature, (const u8*)&m_NonMoverDofsAllowed, sizeof(bool));

	return signature;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
