//
// cranimation/channelsmallestthree.cpp
//
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved.
//

#include "channelsmallestthree.h"

#if CR_DEV
#include "animarchive.h"
#include "animation.h"
#include "animtolerance.h"

#include "vectormath/classes.h"

namespace rage
{

//////////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_ANIM_CHANNEL(crAnimChannelSmallestThreeQuaternion, crAnimChannel::AnimChannelTypeSmallestThreeQuaternion, crAnimTolerance::kCompressionCostMed, crAnimTolerance::kDecompressionCostHigh);

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelSmallestThreeQuaternion::EvaluateQuaternion(float time, QuatV_InOut outQuaternion) const
{
	int tq = int(floor(time));
	float tr = time - float(tq);

	if(tr > (1.f - INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE))
	{
		tq++;
	}
	else if(tr > INTERNAL_FRAME_ROUNDING_ERROR_TOLERANCE)
	{
		QuatV q0, q1;
		ReconstructQuaternion(tq, q0);
		ReconstructQuaternion(tq+1, q1);
		q0 = PrepareSlerp(q1, q0);

		outQuaternion = Nlerp(ScalarVFromF32(tr), q0, q1);
		return;
	}

	ReconstructQuaternion(tq, outQuaternion);
}

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelSmallestThreeQuaternion::ReconstructQuaternion(int t, QuatV_InOut q) const
{
	u32 qv0 = !m_QuantizedFloats[0].m_ScaleAndValues ? 0 : m_QuantizedFloats[0].m_ScaleAndValues->m_QuantizedValues.GetElement(t);
	u32 qv1 = !m_QuantizedFloats[1].m_ScaleAndValues ? 0 : m_QuantizedFloats[1].m_ScaleAndValues->m_QuantizedValues.GetElement(t);
	u32 qv2 = !m_QuantizedFloats[2].m_ScaleAndValues ? 0 : m_QuantizedFloats[2].m_ScaleAndValues->m_QuantizedValues.GetElement(t);

	float f0 = float(qv0)* (!m_QuantizedFloats[0].m_ScaleAndValues ? 1.f : m_QuantizedFloats[0].m_ScaleAndValues->m_Scale) + m_QuantizedFloats[0].m_Offset;
	float f1 = float(qv1)* (!m_QuantizedFloats[1].m_ScaleAndValues ? 1.f : m_QuantizedFloats[1].m_ScaleAndValues->m_Scale) + m_QuantizedFloats[1].m_Offset;
	float f2 = float(qv2)* (!m_QuantizedFloats[2].m_ScaleAndValues ? 1.f : m_QuantizedFloats[2].m_ScaleAndValues->m_Scale) + m_QuantizedFloats[2].m_Offset;

	float r = sqrtf(Max(0.f, 1.f - (square(f0)+square(f1)+square(f2))));

	switch(m_QuantizedOrder)
	{
	case 0: q = QuatV(r, f0, f1, f2); break;
	case 1: q = QuatV(f0, r, f1, f2);	break;
	case 2: q = QuatV(f0, f1, r, f2); break;
	case 3: q = QuatV(f0, f1, f2, r); break;
	default: q = QuatV(V_IDENTITY); break;
	}
	q = Normalize(q);
}

//////////////////////////////////////////////////////////////////////////////////////////////

bool crAnimChannelSmallestThreeQuaternion::CreateQuaternion(QuatV* quaternions, int num, float tolerance)
{
	if(num < 3)
	{
		return false;
	}

	float* buckets[3];
	for(int i=0; i<3; ++i)
	{
		buckets[i] = rage_new float[num];
	}

	for(int n=0; n<num; ++n)
	{
		QuatV q = quaternions[n];

		float f[4];
		f[0] = fabs(q[0]);
		f[1] = fabs(q[1]);
		f[2] = fabs(q[2]);
		f[3] = fabs(q[3]);

		int maxIdx = MaximumIndex(f[0], f[1], f[2], f[3]);
		if(reinterpret_cast<float*>(&q)[maxIdx] < 0.f)
		{
			q = Scale(q, ScalarV(V_NEGONE));
		}

		for(int i=0,j=0; i<4; ++i)
		{
			if(maxIdx != i)
			{
				buckets[j][n] = reinterpret_cast<float*>(&q)[i];
				++j;
			}
		}

		if(!n)
		{
			m_QuantizedOrder = u32(maxIdx);
		}
		else if(m_QuantizedOrder != u32(maxIdx))
		{
			for(int i=0; i<3; ++i)
			{
				delete [] buckets[i];
			}
			return false;
		}
	}

	const float tolerancePerChannel = tolerance / 3.f;

	bool success = true;
	for(int i=0; i<3; ++i)
	{
		m_QuantizedFloats[i].m_Offset = buckets[i][0];
		m_QuantizedFloats[i].m_ScaleAndValues = NULL;

		for(int n=1; n<num; ++n)
		{
			if(square(buckets[i][n] - buckets[i][0]) > square(tolerancePerChannel))
			{
				m_QuantizedFloats[i].m_ScaleAndValues = rage_new QuantizedFloats::ScaleAndValues;
				success = crAnimChannelQuantizeFloat::QuantizeFloats(buckets[i], num, 0, tolerancePerChannel, m_QuantizedFloats[i].m_ScaleAndValues->m_QuantizedValues, m_QuantizedFloats[i].m_ScaleAndValues->m_Scale, m_QuantizedFloats[i].m_Offset) & success;
				break;
			}
		}
	}

	for(int i=0; i<3; ++i)
	{
		delete [] buckets[i];
	}

	return success;
}

//////////////////////////////////////////////////////////////////////////////////////////////

int crAnimChannelSmallestThreeQuaternion::ComputeSize(void) const
{
	int size = sizeof(crAnimChannelSmallestThreeQuaternion);
	for(int i=0; i<3; ++i)
	{
		size += m_QuantizedFloats[i].ComputeSize();
	}

	return size;
}

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelSmallestThreeQuaternion::Serialize(crArchive& a)
{
	crAnimChannel::Serialize(a);

	for(int i=0; i<3; ++i)
	{
		m_QuantizedFloats[i].Serialize(a);
	}

	a <<= m_QuantizedOrder;
}

//////////////////////////////////////////////////////////////////////////////////////////////

int crAnimChannelSmallestThreeQuaternion::QuantizedFloats::ComputeSize() const
{
	int size = m_ScaleAndValues ? m_ScaleAndValues->ComputeSize() : 0;
	return size;
}

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelSmallestThreeQuaternion::QuantizedFloats::Serialize(crArchive& a)
{
	a <<= m_Offset;

	u8 hasScaleAndValues = (m_ScaleAndValues != NULL);
	a <<= hasScaleAndValues;
	if(hasScaleAndValues)
	{
		if(a.IsLoading())
		{
			m_ScaleAndValues = rage_new ScaleAndValues();
		}
		m_ScaleAndValues->Serialize(a);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

int crAnimChannelSmallestThreeQuaternion::QuantizedFloats::ScaleAndValues::ComputeSize() const
{
	int size = sizeof(ScaleAndValues) + m_QuantizedValues.ComputeSize();

	return size;
}

//////////////////////////////////////////////////////////////////////////////////////////////

void crAnimChannelSmallestThreeQuaternion::QuantizedFloats::ScaleAndValues::Serialize(crArchive& a)
{
	a <<= m_Scale;
	m_QuantizedValues.Serialize(a);
}

//////////////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV
