//
// cranimation/channellinear.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_CHANNELLINEAR_H
#define CRANIMATION_CHANNELLINEAR_H

#include "animchannel.h"

#if CR_DEV
namespace rage
{
	
class crAnimChannelLinearFloat : public crAnimChannel
{
	friend class crAnimPacker;
public:
	crAnimChannelLinearFloat() : crAnimChannel(AnimChannelTypeLinearFloat) {}
	virtual ~crAnimChannelLinearFloat() {}
	DECLARE_ANIM_CHANNEL(crAnimChannelLinearFloat);
	virtual void EvaluateFloat(float fTime, float& outFloat) const;
	virtual bool CreateFloat(float* floats, int frames, int skip, float tolerance);
	virtual int ComputeSize() const;
	virtual void Serialize(crArchive& a);

private:
	float ReconstructFloat(u32 frame) const;

	u32 m_SegmentSize;
	f32 m_Scale;
	f32 m_Offset;
	atPackedArray m_Addresses, m_Initials;
	atPackedSequence m_Values;
};

} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_CHANNELLINEAR_H
