//
// cranimation/animiterators.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMITERATORS_H
#define CRANIMATION_ANIMITERATORS_H

#include "animation.h"
#include "animation_config.h"
#include "animcache.h"
#include "animstream.h"
#include "animtrack.h"
#include "system/cache.h"

#define ANIMITERATOR_TEMPLATE_OPTIMIZATIONS (__OPTIMIZED)

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Animation iterator
template <typename _T>
class crAnimIterator
{
public:

	// PURPOSE: Initialization
	// PARAMS: animation - animation to iterate
	crAnimIterator(const crAnimation& anim);


	// PURPOSE: Simple animation iteration.  Iterates a single animation.
	// PARAMS:
	// time - time at which to iterate animation (in internal frames)
	// filter - optional filter to use during iteration
	// weight - optional weight for filter
	void Iterate(float time, crFrameFilter* filter=NULL, float weight=1.f);


protected:
	const crAnimation* m_Anim;

private:

#if ANIMITERATOR_TEMPLATE_OPTIMIZATIONS

	template<bool hasFilter>
	void IterateTemplate1(float time, crFrameFilter* filter, float weight);	

	template <bool hasFilter, bool hasFilterWeights>
	void IterateTemplate2(float time, crFrameFilter* filter, float weight, float* filterWeights);	

#endif // ANIMITERATOR_TEMPLATE_OPTIMIZATIONS
};

////////////////////////////////////////////////////////////////////////////////

#if ANIMITERATOR_TEMPLATE_OPTIMIZATIONS
#define BURN_UNUSED_TEMPLATE_PARAMETERS (!__PPU)
#endif // ANIMITERATOR_TEMPLATE_OPTIMIZATIONS

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crAnimIterator<_T>::crAnimIterator(const crAnimation& anim)
: m_Anim(&anim)
{
}

////////////////////////////////////////////////////////////////////////////////

#if ANIMITERATOR_TEMPLATE_OPTIMIZATIONS
template<typename _T>
inline void crAnimIterator<_T>::Iterate(float time, crFrameFilter* filter, float weight)
{
	if(filter != NULL)
	{
		IterateTemplate1<false>(time, filter, weight);
	}
	else
	{
		IterateTemplate1<true>(time, filter, weight);
	}
}
template<typename _T> template<bool noFilter>
__forceinline void crAnimIterator<_T>::IterateTemplate1(float time, crFrameFilter* filter, float weight)
{
	if(!noFilter)
	{
/*		crFrameAccelerator* accelerator = m_Frame->m_Accelerator;
		if(accelerator)
		{
			crFrameAccelerator::LockWeights filterWeightsLock;
			if(accelerator->FindFilterWeights(filterWeightsLock, *m_Frame->m_FrameData, *filter))
			{
				IterateTemplate2<noFilter, true>(time, filter, weight, filterWeightsLock);
			}
		}*/
	}
	IterateTemplate2<noFilter, false>(time, filter, weight, NULL);
}
template<typename _T> template<bool noFilter, bool hasFilterWeights>
inline void crAnimIterator<_T>::IterateTemplate2(float, crFrameFilter* filter, float weight, float* filterWeights)
{
#if BURN_UNUSED_TEMPLATE_PARAMETERS
	(void)filter;
	(void)weight;
	(void)filterWeights;
#endif // BURN_UNUSED_TEMPLATE_PARAMETERS

#else // ANIMITERATOR_TEMPLATE_OPTIMIZATIONS
template<typename _T>
void crAnimIterator<_T>::Iterate(float, crFrameFilter* NOTFINAL_ONLY(filter), float NOTFINAL_ONLY(weight))
{
	const bool noFilter = (filter == NULL);

	float* filterWeights = NULL;

/*	crFrameAccelerator::LockWeights filterWeightsLock;
	if(FRAME_ACCELERATOR_FILTER_WEIGHTS && !noFilter)
	{
		crFrameAccelerator* accelerator = m_Frame->m_Accelerator;
		if(accelerator)
		{
			accelerator->FindFilterWeights(filterWeightsLock, *m_Frame->m_FrameData, *filter);
			filterWeights = filterWeightsLock;
		}
	}*/
	const bool hasFilterWeights = (filterWeights != NULL);

#endif // FRAMEITERATOR_TEMPLATE_OPTIMIZATIONS

#if CR_DEV
	if(m_Anim->IsPacked())
	{
		const int numDofs = m_Anim->m_Dofs.GetCount();
		for(int dofIdx=0; dofIdx<numDofs; ++dofIdx)
		{
			const crAnimation::Dof& dof = m_Anim->m_Dofs[dofIdx];

			float opWeight = hasFilterWeights?*filterWeights++:weight;

			if(noFilter || (hasFilterWeights && opWeight>0.f) || (!hasFilterWeights && filter->FilterDof(dof.m_Track, dof.m_Id, opWeight)))
			{
				static_cast<_T*>(this)->IterateDof(dof.m_Track, dof.m_Id, dof.m_Type, opWeight);
			}
		}
	}
	else
	{
		const int numTracks = m_Anim->m_Tracks.GetCount();
		for(int trackIdx=0; trackIdx<numTracks; ++trackIdx)
		{
			const crAnimTrack& track = *m_Anim->m_Tracks[trackIdx];

			float opWeight = hasFilterWeights?*filterWeights++:weight;

			if(noFilter || (hasFilterWeights && opWeight>0.f) || (!hasFilterWeights && filter->FilterDof(track.GetTrack(), track.GetId(), opWeight)))
			{
				static_cast<_T*>(this)->IterateDof(track.GetTrack(), track.GetId(), track.GetType(), opWeight);
			}
		}
	}
#endif // CR_DEV
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRANIMATION_ANIMITERATORS_H
