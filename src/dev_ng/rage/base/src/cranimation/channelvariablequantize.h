//
// cranimation/channelvariablequantize.h
//
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_CHANNELVARIABLEQUANTIZE_H
#define CRANIMATION_CHANNELVARIABLEQUANTIZE_H

#include "animchannel.h"

#if CR_DEV
namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Variable bit quantization of a float channel
class crAnimChannelVariableQuantizeFloat : public crAnimChannel
{
public:

	// PURPOSE: Constructor
	crAnimChannelVariableQuantizeFloat();

	// PURPOSE: Destructor
	virtual ~crAnimChannelVariableQuantizeFloat();

	// PURPOSE: Animation channel declaration
	DECLARE_ANIM_CHANNEL(crAnimChannelVariableQuantizeFloat);

	// PURPOSE: Evaluation
	virtual void EvaluateFloat(float time, float& outFloat) const;

	// PURPOSE: Creation
	virtual bool CreateFloat(float* floats, int num, int skip, float tolerance, float* tolerances);

	// PURPOSE: Storage computation
	virtual int ComputeSize() const;

	// PURPOSE: Serialization
	virtual void Serialize(crArchive& a);

private:
	atPackedSequence m_QuantizedValues;
	float m_Scale;
	float m_Offset;
};

////////////////////////////////////////////////////////////////////////////////

inline crAnimChannelVariableQuantizeFloat::crAnimChannelVariableQuantizeFloat()
: crAnimChannel(AnimChannelTypeVariableQuantizeFloat)
, m_Scale(0.f)
, m_Offset(0.f)
{
}

////////////////////////////////////////////////////////////////////////////////

inline crAnimChannelVariableQuantizeFloat::~crAnimChannelVariableQuantizeFloat()
{
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CR_DEV

#endif // CRANIMATION_CHANNELVARIABLEQUANTIZE_H
