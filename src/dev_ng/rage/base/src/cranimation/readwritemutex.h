#ifndef CRANIMATION_READWRITEMUTEX_H
#define CRANIMATION_READWRITEMUTEX_H

#include "system/criticalsection.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Mutex that allows multiple simultaneous readers, or only one writer
template <u32 maxReaderThreads>
class ReadWriteMutex
{
public:

	// PURPOSE: Constructor
	ReadWriteMutex();

	// PURPOSE: Destructor
	~ReadWriteMutex();

	// PURPOSE: Lock a reading thread (up to maxReadThreads can be locked simultaneously)
	void LockRead();

	// PURPOSE: Unlock a reading thread
	// NOTES: Must pair exactly with LockRead calls
	void UnlockRead();

	// PURPOSE: Lock a writing thread (only one write thread allowed at once)
	void LockWrite();

	// PURPOSE: Unlock a writing thread
	// NOTES: Must pair exactly with LockWrite calls
	void UnlockWrite();

	// PURPOSE: Stack based read locking/unlocking
	class Read
	{
	public:

		// PURPOSE: Constructor auto locks for reading
		Read(ReadWriteMutex<maxReaderThreads>& mutex);

		// PURPOSE: Destructor auto unlocks
		~Read();

	private:
		ReadWriteMutex<maxReaderThreads>* m_Mutex;
	};

	// PURPOSE: Stack based write locking/unlocking
	class Write
	{
	public:

		// PURPOSE: Constructor auto locks for writing
		Write(ReadWriteMutex<maxReaderThreads>& mutex);

		// PURPOSE: Destructor auto unlocks 
		~Write();

	private:
		ReadWriteMutex<maxReaderThreads>* m_Mutex;
	};

private:
	sysIpcSema m_Semaphore;
	sysCriticalSectionToken m_Cs;
};

////////////////////////////////////////////////////////////////////////////////

template <u32 maxReaderThreads>
inline ReadWriteMutex<maxReaderThreads>::ReadWriteMutex()
{
	m_Semaphore = sysIpcCreateSema(maxReaderThreads);
}

////////////////////////////////////////////////////////////////////////////////

template <u32 maxReaderThreads>
inline ReadWriteMutex<maxReaderThreads>::~ReadWriteMutex()
{
	sysIpcDeleteSema(m_Semaphore);
}

////////////////////////////////////////////////////////////////////////////////

template <u32 maxReaderThreads>
inline void ReadWriteMutex<maxReaderThreads>::LockRead()
{
	sysIpcWaitSema(m_Semaphore);
}

////////////////////////////////////////////////////////////////////////////////

template <u32 maxReaderThreads>
inline void ReadWriteMutex<maxReaderThreads>::UnlockRead()
{
	sysIpcSignalSema(m_Semaphore);
}

////////////////////////////////////////////////////////////////////////////////

template <u32 maxReaderThreads>
inline void ReadWriteMutex<maxReaderThreads>::LockWrite()
{
	sysCriticalSection cs(m_Cs);
	for(u32 i=0; i<maxReaderThreads; ++i)
	{
		sysIpcWaitSema(m_Semaphore);
	}
}

////////////////////////////////////////////////////////////////////////////////

template <u32 maxReaderThreads>
inline void ReadWriteMutex<maxReaderThreads>::UnlockWrite()
{
	for(u32 i=0; i<maxReaderThreads; ++i)
	{
		sysIpcSignalSema(m_Semaphore);
	}
}

////////////////////////////////////////////////////////////////////////////////

template <u32 maxReaderThreads>
inline ReadWriteMutex<maxReaderThreads>::Read::Read(ReadWriteMutex<maxReaderThreads>& mutex)
: m_Mutex(&mutex)
{
	m_Mutex->LockRead();	
}

////////////////////////////////////////////////////////////////////////////////

template <u32 maxReaderThreads>
inline ReadWriteMutex<maxReaderThreads>::Read::~Read()
{
	m_Mutex->UnlockRead();
}

////////////////////////////////////////////////////////////////////////////////

template <u32 maxReaderThreads>
inline ReadWriteMutex<maxReaderThreads>::Write::Write(ReadWriteMutex<maxReaderThreads>& mutex)
: m_Mutex(&mutex)
{
	m_Mutex->LockWrite();
}

////////////////////////////////////////////////////////////////////////////////

template <u32 maxReaderThreads>
inline ReadWriteMutex<maxReaderThreads>::Write::~Write()
{
	m_Mutex->UnlockWrite();
}

////////////////////////////////////////////////////////////////////////////////

}; // namespace rage

#endif // CRANIMATION_READWRITEMUTEX_H