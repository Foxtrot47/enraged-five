//
// cranimation/framedata.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_FRAMEDATA_H
#define CRANIMATION_FRAMEDATA_H

#include "framefilters.h"

#define WEIGHT_ROUNDING_ERROR_TOLERANCE (0.001f)

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Known track indices (do NOT reorder these)
enum
{
	kTrackBoneTranslation = 0,
	kTrackBoneRotation,
	kTrackBoneScale,
	kTrackBoneConstraint,
	kTrackVisibility,
	kTrackMoverTranslation,
	kTrackMoverRotation,
	kTrackCameraTranslation,
	kTrackCameraRotation,
	kTrackCameraScale,
	kTrackCameraFocalLength,
	kTrackCameraHorizontalFilmAperture,
	kTrackCameraAperture,
	kTrackCameraFocalPoint,
	kTrackCameraFStop,
	kTrackCameraFocusDistance,
	kTrackShaderFrameIndex,
	kTrackShaderSlideU,
	kTrackShaderSlideV,
	kTrackShaderRotateUV,
	kTrackMoverScale,
	kTrackBlendShape,
	kTrackVisemes,
	kTrackAnimatedNormalMaps,
	kTrackFacialControl,
	kTrackFacialTranslation,
	kTrackFacialRotation,
	kTrackCameraFieldOfView,
	kTrackCameraDepthOfField,
	kTrackColor,
	kTrackLightIntensity,
	kTrackLightFallOff,
	kTrackLightConeAngle,
	kTrackGenericControl,
	kTrackGenericTranslation,
	kTrackGenericRotation,
	kTrackCameraDepthOfFieldStrength,
	kTrackFacialScale,
	kTrackGenericScale,
	kTrackCameraShallowDepthOfField,
	kTrackCameraMotionBlur,
	kTrackParticleData,
	kTrackLightDirection,
	kTrackCameraDepthOfFieldNearOutOfFocusPlane,
	kTrackCameraDepthOfFieldNearInFocusPlane,
	kTrackCameraDepthOfFieldFarOutOfFocusPlane,
	kTrackCameraDepthOfFieldFarInFocusPlane,
	kTrackLightExpFallOff,
	kTrackCameraSimpleDepthOfField,
	kTrackCameraCoC,
	kTrackFacialTinting,
	kTrackCameraFocus,
	kTrackCameraNightCoC,
	kTrackCameraLimit, 
	// animated cloth?

	// highest rage "built in" track type
	kTrackLastReservedForRageUseOnly = 0x7f,

	// project specific track types
	kTrackFirstReservedForProjectUse = 0x80,
};

const char *GetTrackName(u8 track);

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Known track types (do NOT reorder these)
enum
{
	// types, tracks can contain any of the following DOFs
	kFormatTypeVector3 = 0,
	kFormatTypeQuaternion,
	kFormatTypeFloat,

	kFormatTypeNum,  // must be last in list
};

const char *GetFormatTypeName(u8 type);

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Frame data
class ALIGNAS(16) crFrameData
{
public:

	// PURPOSE: Default constructor
	crFrameData();

	// PURPOSE: Copy constructor
	// NOTES:
	// Initializes, using automatically created internal buffer,
	// and then copies degrees of freedom from source frame data
	crFrameData(const crFrameData& src);

	// PURPOSE: Destructor
	~crFrameData();

	// PURPOSE: Initialization
	// PARAMS:
	// dofsBuffer - pointer to externally allocated buffer to use for storing internal frame data
	// dofsBufferSize - size of externally allocated buffer (in bytes)
	// dofsBufferOwner - transfer responsibility for destruction of external buffer
	// NOTES: If not provided, frame data will automatically allocate buffer internally
	// If external buffer is provided, and ownership is not transfered, then buffer
	// must be sufficiently large for degrees of freedom that are subsequently added
	// Use CalcDofsBufferSize to find the minimum buffer size required
	void Init(u8* dofsBuffer, u32 dofsBufferSize, bool dofsBufferOwner);

	// PURPOSE: Shutdown, free/release dynamic resources
	void Shutdown();

	// PURPOSE: Assignment operator
	// NOTES:
	// Initializes, using automatically created internal buffer,
	// and then copies degrees of freedom from source frame data
	const crFrameData& operator=(const crFrameData&);


	// PURPOSE: Add reference
	void AddRef() const;

	// PURPOSE: Release reference
	u32 Release() const;

	// PURPOSE: Get current reference count
	u32 GetRef() const;


	// PURPOSE: Get number of degrees of freedom
	u32 GetNumDofs() const;

	// PURPOSE: Get number of degrees of freedom
	u32 GetNumDofsType(u32 type) const;

	// PURPOSE: Get frame data signature
	u32 GetSignature() const;

	// PURPOSE: Get buffer size required for frame values
	u32 GetFrameBufferSize() const;


	// PURPOSE: Is degree of freedom present
	bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Add degree of freedom (safe)
	bool AddDof(u8 track, u16 id, u8 type);

	// PURPOSE: Remove degree of freedom (safe)
	bool RemoveDof(u8 track, u16 id);


	// PURPOSE: Represents a degree of freedom
	struct Dof
	{
		// PURPOSE: Get combined track id value (used for sorting)
		u32 GetTrackId() const;

		// PURPOSE: Equality operator
		bool operator==(const Dof&) const;

		// PURPOSE: Inequality operator
		bool operator!=(const Dof&) const;

		// PURPOSE: Less than operator
		bool operator<(const Dof&) const;

		// PURPOSE: Greater than operator
		bool operator>(const Dof&) const;

		u16 m_Offset;
		u16 m_Id;
		u8 m_Type;
		u8 m_Track;
	};


	// PURPOSE: Frame data degree of freedom iterator
	template <typename _T>
	class Iterator
	{
	public:

		// PURPOSE: Initialization
		Iterator(crFrameData& frameData);

		// PURPOSE: Initialization (const)
		Iterator(const crFrameData& frameData);


		// PURPOSE: Iterate frame data
		// PARAMS: filter - optional frame filter (default NULL iterates all degrees of freedom)
		void Iterate(crFrameFilter* filter=NULL);	

	protected:
		crFrameData* m_FrameData;
	};

	// PURPOSE: Get a specific dof
	const Dof& GetDof(u32 i) const;

	// PURPOSE: Get internal buffer
	// RETURNS: buffer pointer
	const Dof* GetBuffer() const;

	// PURPOSE: Get buffer size
	// RETURNS: buffer size, in bytes
	u32 GetBufferSize() const;

	// PURPOSE: Find degree of freedom
	const Dof* FindDof(u8 track, u16 id) const;
	const Dof* FindDofIndex(u8 track, u16 id, u32& outIdx) const;

	// PURPOSE: Get read-only offset, this dof must always be equal to invalid mask
	u16 GetReadOnlyOffset() const;

	// PURPOSE: Get write-only offset, this dof can be anything
	u16 GetWriteOnlyOffset() const;

protected:

	// PURPOSE: Calculate buffer size required for frame data degrees of freedom
	// NOTES: This is the buffer size required to initialize *crFrameData*
	// Not to be confused with the buffer size needed for the values stored in
	// a crFrame, which is returned by crFrameData::GetFrameBufferSize
	static u32 CalcDofsBufferSize(u32 numDofs);

	// PURPOSE: Add degree of freedom (unsafe)
	// NOTES: Fast, suitable for repeated calls, but SortDofs must be called afterwards
	void FastAddDof(u8 track, u16 id, u8 type);
	bool FastAddDof(u8 track, u16 id, u8 type, crFrameFilter* filter);

	// PURPOSE: Reserve degrees of freedom
	void ReserveDofs(u32 numDofs, bool destroyExistingDofs=true);

	// PURPOSE: Sort degrees of freedom
	void SortDofs(bool removeDuplicates=true);

	// PURPOSE: Calculate signature
	// NOTES: Signature is a hash representing all the degrees of freedom (their tracks/ids/types)
	// Frame signatures allow frames to be quickly compared to determine if their
	// structures (but not their values) are equal.  This allows a number of operations to
	// have optimized paths, when operating on identically structured frames.
	// The algorithm used here (fletcher's) MUST match exactly the one used in crAnimation::CalcSignature
	void CalcSignature();

	// PURPOSE: Calculate dof offsets
	void CalcOffsets();

	Dof* m_Dofs;
	mutable u32 m_RefCount;

	u32 m_Signature;
	u32 m_NumDofs;

	u16 m_NumTypes[kFormatTypeNum];
	u16 m_FrameBufferSize;
	u16 m_DofsBufferSize;
	u16 m_DofsBufferOwner;
	u8 m_Padding[2];

	friend class crFrame;
	template<u16> friend class crFrameFixedDofs;
	friend class crFrameSingleDof;
	friend class crFrameAccelerator;
	friend class crFrameDataInitializer;
	template<typename T> friend class crFrameIterator;
	friend class crFrameDataFactory;
	friend struct ExpressionCreatureInitializer;
} ;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Creates a frame data structure, with preallocated buffer
// for a fixed number of degrees of freedom
// NOTES: Useful for allocating frame data temporarily on the stack,
// when the number of degrees of freedom is known at compile time
template<u16 numDofs>
class crFrameDataFixedDofs : public crFrameData
{
public:

	// PURPOSE: Default constructor
	crFrameDataFixedDofs();

private:

	static const int fixedDofsBufferSize = RAGE_ALIGN(numDofs*sizeof(Dof), 4);
	u8 m_FixedDofsBuffer[fixedDofsBufferSize+15];
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Creates a frame data structure, with preallocated buffer
// for a single degree of freedom
// NOTES: Useful for allocating frame data temporarily on the stack
// when it is known only one degree of freedom is needed
class crFrameDataSingleDof : public crFrameDataFixedDofs<1>
{
public:

	// PURPOSE: Default constructor
	crFrameDataSingleDof();

	// PURPOSE: Initializing constructor
	crFrameDataSingleDof(u8 track, u16 id, u8 type);
};

////////////////////////////////////////////////////////////////////////////////

inline void crFrameData::AddRef() const
{
	sysInterlockedIncrement(&m_RefCount);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameData::Release() const
{
	u32 refCount = sysInterlockedDecrement(&m_RefCount);
	if(!refCount)
	{
		delete this;
		return 0;
	}
	else
	{
		return refCount;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameData::GetRef() const
{
	return m_RefCount;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameData::GetNumDofs() const
{
	return m_NumDofs;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameData::GetNumDofsType(u32 type) const
{
	FastAssert(type < kFormatTypeNum);
	return m_NumTypes[type];
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameData::GetSignature() const
{
	return m_Signature;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameData::GetFrameBufferSize() const
{
	return m_FrameBufferSize;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrameData::HasDof(u8 track, u16 id) const
{
	return FindDof(track, id) != NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrameData::AddDof(u8 track, u16 id, u8 type)
{
	if(!FindDof(track, id))
	{
		FastAddDof(track, id, type);

		SortDofs();

		CalcSignature();
		CalcOffsets();

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

__forceinline u32 crFrameData::Dof::GetTrackId() const
{
	return (u32(m_Track)<<16)|u32(m_Id);
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrameData::Dof::operator==(const Dof& other) const
{
	return GetTrackId() == other.GetTrackId();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrameData::Dof::operator!=(const Dof& other) const
{
	return GetTrackId() != other.GetTrackId();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrameData::Dof::operator<(const Dof& other) const
{
	return GetTrackId() < other.GetTrackId();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crFrameData::Dof::operator>(const Dof& other) const
{
	return GetTrackId() > other.GetTrackId();
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crFrameData::Iterator<_T>::Iterator(crFrameData& frameData)
{
	m_FrameData = &frameData;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline crFrameData::Iterator<_T>::Iterator(const crFrameData& frameData)
{
	m_FrameData = const_cast<crFrameData*>(&frameData);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _T>
inline void crFrameData::Iterator<_T>::Iterate(crFrameFilter* filter)
{
	const u32 numDofs = m_FrameData->m_NumDofs;
	for(u32 i=0; i<numDofs; ++i)
	{
		Dof& dof = m_FrameData->m_Dofs[i];

		// TODO --- use accelerator here to optimize filtering
		float weight = 1.f;
		if(!filter || filter->FilterDof(dof.m_Track, dof.m_Id, weight))
		{
			_T& t = *(_T*)(this);
			t.IterateDof(dof);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrameData::Dof& crFrameData::GetDof(u32 i) const
{
	FastAssert(i < m_NumDofs);
	return m_Dofs[i];
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrameData::Dof* crFrameData::GetBuffer() const
{
	return m_Dofs;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameData::GetBufferSize() const
{
	return m_DofsBufferSize;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crFrameData::CalcDofsBufferSize(u32 numDofs)
{
	return numDofs*sizeof(Dof);
}

////////////////////////////////////////////////////////////////////////////////

inline int FindDofIdx(u32 numDofs, const crFrameData::Dof* dofs, u8 track, u16 id)
{
	u32 trackid = (u32(track)<<16)|u32(id);
	int low = 0;
	int high = numDofs-1;

	while(low <= high)
	{
		int idx = (low+high) >> 1;
		u32 ti = dofs[idx].GetTrackId();

		if(trackid==ti)
		{
			return idx;
		}
		else if(trackid<ti)
		{
			high = idx-1;
		}
		else
		{
			low = idx+1;
		}
	}
	return -1;
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrameData::Dof* crFrameData::FindDof(u8 track, u16 id) const
{
	int idx = FindDofIdx(m_NumDofs, m_Dofs, track, id);
    return idx >= 0 ? &m_Dofs[idx] : NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrameData::Dof* crFrameData::FindDofIndex(u8 track, u16 id, u32& outIdx) const
{
	int idx = FindDofIdx(m_NumDofs, m_Dofs, track, id);
	outIdx = idx;
	return idx >= 0 ? &m_Dofs[idx] : NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline u16 crFrameData::GetReadOnlyOffset() const
{
	return m_FrameBufferSize - 2*sizeof(Vec4V);
}

////////////////////////////////////////////////////////////////////////////////

inline u16 crFrameData::GetWriteOnlyOffset() const
{
	return m_FrameBufferSize - sizeof(Vec4V);
}

////////////////////////////////////////////////////////////////////////////////

inline void crFrameData::FastAddDof(u8 track, u16 id, u8 type)
{
	u32 minDofsBufferSize = CalcDofsBufferSize(m_NumDofs+1);

	// reallocation required
	if(minDofsBufferSize > m_DofsBufferSize)
	{
		Assert(m_DofsBufferOwner);

		// add additional wasted elements to prevent repeated reallocation
		const u32 reallocationStep = 8;
		ReserveDofs(m_NumDofs + reallocationStep, false);
	}

	Dof& dof = m_Dofs[m_NumDofs++];
	dof.m_Track = track;
	dof.m_Id = id;
	dof.m_Type = type;

	m_Signature = 0;
	m_FrameBufferSize = 0;
}

////////////////////////////////////////////////////////////////////////////////

template<u16 numDofs>
inline crFrameDataFixedDofs<numDofs>::crFrameDataFixedDofs()
{
	Init((u8*)RAGE_ALIGN(uptr(m_FixedDofsBuffer), 4), fixedDofsBufferSize, false);
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameDataSingleDof::crFrameDataSingleDof()
{
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameDataSingleDof::crFrameDataSingleDof(u8 track, u16 id, u8 type)
{
	AddDof(track, id, type);
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // CRANIMATION_FRAMEDATA_H
