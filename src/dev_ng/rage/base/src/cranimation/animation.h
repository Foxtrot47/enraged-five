//
// cranimation/animation.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef CRANIMATION_ANIMATION_H
#define CRANIMATION_ANIMATION_H

#include "animation_config.h"

#include "atl/array.h"
#include "math/amath.h"
#include "paging/base.h"
#include "system/bit.h"
#include "system/interlocked.h"

namespace rage
{

class crAnimTolerance;
class crAnimTrack;
class crArchive;
class crFrame;
struct crBlockStream;

// PURPOSE:
// Animations represent the change in a series of values (tracks)
// over a period of time (duration).
// The crAnimation class hides the internal storage of these tracks,
// the channels and compression used.  The animation also constructs
// a parallel structure of blocks and chunks, to help organize the
// memory layout of the animation data in a more temporal fashion.
// The values on the tracks can be retrieved by constructing a frame
// (with dofs for values you are interested in) and then querying
// the animation at a particular point in time.
// Additionally there are some special case accessors that enable
// querying of certain values without presenting a frame
// (these special cases are maintained for largely legacy reasons).
class crAnimation : public pgBase
{
	friend class crAnimPacker;
	friend class crFrame;
	friend class crFrameAccelerator;
	template<typename T> friend class crFrameIterator;
	template<typename T> friend class crAnimIterator;
	template<bool> friend class CreateIterator;

	struct Dof;

public:

	// PURPOSE: Default constructor
	crAnimation();

	// PURPOSE: Resource constructor
	crAnimation(datResource&);

	// PURPOSE: Destructor
	~crAnimation();

	// PURPOSE: Placement
	DECLARE_PLACE(crAnimation);

	// PURPOSE: Offline resourcing
#if __DECLARESTRUCT
	void DeclareStruct(datTypeStruct&);
#endif // __DECLARESTRUCT

#if BASE_DEBUG_NAME
	virtual const char *GetDebugName(char * /*buffer*/, size_t /*bufferSize*/) const		{ return GetName(); }
#endif // BASE_DEBUG_NAME


	// PURPOSE: Offline resource version
	static const int RORC_VERSION = crAnimationResourceBaseVersion;

	// PURPOSE: Shutdown, Frees all dynamic memory used by the animation class.  Destroys all the tracks.
	void Shutdown();

	// PURPOSE: Get current reference counter value.
	int GetRef() const;

	// PURPOSE: Increment reference counter.
	void AddRef() const;

	// PURPOSE: Decrement reference counter.
	// RETURNS: New reference count.
	// NOTES: If reference counter reaches 0, animation will destruct.
	int Release() const;

	// PURPOSE: Composite a frame (a collection of dofs).
	// PARAMS
	//	time - time in seconds (0.0 -> duration of animation)
	//	inoutFrame - frame to composite, dofs should be correctly set up before call.
	//	closest - perform composite at nearest internal frame (maybe quicker, but results less smooth)
	//	clear - zero any dofs in frame that are not composited
	// NOTES
	//	If some dofs within the frame fail to match the tracks within the animation then the values
	//	within these dofs will be cleared (unless clear is set to false, in which case the values will
	//	be unchanged - so initialize before compositing or handle false return value properly in this case).
	void CompositeFrame(float time, crFrame& inoutFrame, bool closest=false, bool clear=true) const;
	
	// PURPOSE: Composite a frame (a collection of dofs) + convert the mover dofs to deltas.
	// PARAMS
	// time - time in seconds (0.0 -> duration of animation)
	// delta - time in seconds that has elapsed since last composite taken
	// inoutFrame - frame to composite, dofs should be correctly set up before call.
	// closest - perform composite at nearest internal frame (maybe quicker, but results less smooth)
	// clear - zero any dofs in frame that are not composited
	// NOTES : If some dofs within the frame fail to match the tracks within the animation then the values
	// within these dofs will be cleared (unless clear is set to false, in which case the values will
	// be unchanged - so initialize before compositing or handle false return value properly in this case).
	void CompositeFrameWithMover(float time, float delta, crFrame& inoutFrame, bool closest=false, bool clear=true) const;

	// PURPOSE: Get the name of animation.
	// NOTES: The name is extracted from the name of the file the animation was loaded from.
	const char* GetName() const;

	// PURPOSE: Get the duration of the animation in seconds.
	float GetDuration() const;
	
	// PURPOSE: Get duration in the number of 30th of a second frames.
	// RETURNS: The number of 30th of a second frames (may not be a whole number).
	float GetNum30Frames() const;

	// PURPOSE: Get number of tracks.
	u32 GetNumTracks() const;

	// PURPOSE: Returns project flags.
	u16 GetProjectFlags() const;

	// PURPOSE: Is the animation looped.
	bool IsLooped() const;

	// PURPOSE: Raw animations have only had lossless compression applied,
	// while cooked animations have had lossy techniques applied, and so are not fully editable.
	// RETURNS: true - animation is lossless compressed (raw), false - animation is lossy compressed (cooked)
	bool IsRaw() const;

	// PURPOSE: Does the animation have mover tracks.
	// RETURNS: true - if the animation has mover tracks.
	bool HasMoverTracks() const;

	// PURPOSE: Check if animation contains a particular track.
	// PARAMS: track - track index, id - id index
	// RETURNS: true - if track is found in animation
	bool HasTrack(u8 track, u16 id) const;

	// PURPOSE: Sets the project flags in an animation.
	// PARAMS: flags - new project flags.
	void SetProjectFlags(u16 flags);

	// PURPOSE: Converts phase (0.0 -> 1.0) to 30th of a second time representation.
	// RETURNS: Number of 30ths of a second equivalent to the phase supplied.
	float ConvertPhaseTo30Frame(float phase) const;

	// PURPOSE: Converts phase (0.0 -> 1.0) to time in seconds representation.
	// RETURNS: Time in seconds equivalent to the phase supplied.
	float ConvertPhaseToTime(float phase) const;

	// PURPOSE: Converts 30ths of a second to phase (0.0 -> 1.0) time representation.
	// RETURNS: Phase equivalent to the number of 30ths of a second supplied.
	float Convert30FrameToPhase(float frame) const;

	// PURPOSE: Converts 30ths of a second to time in seconds.
	// RETURNS: Time in seconds equivalent to the number of 30ths of a second supplied.
	float Convert30FrameToTime(float frame) const;

	// PURPOSE: Converts time in seconds to phase (0.0 -> 1.0) time representation.
	// RETURNS: Phase equivalent to the time in seconds supplied.
	float ConvertTimeToPhase(float time) const;

	// PURPOSE: Converts time in seconds into number of 30ths of a second.
	// RETURNS: Number of 30ths of a second equivalent to the time in seconds supplied.
	float ConvertTimeTo30Frame(float time) const;

	// PURPOSE: Set name.  For use in tools only.
	// PARAMS: name - new filename
	void SetName(const char* name);

	// PURPOSE: Returns the number of blocks contained within this animation
	// RETURNS: Number of blocks
	// NOTES: Blocks are a parallel way of accessing compressed animation data
	// which are organized by time, rather than by structure (as with tracks).
	// They are only of interest to very low level users of animations
	// (ie decompressing animations on PS3 SPUs) and should be avoided
	// by regular users of the animation system.
	u32 GetNumBlocks() const;

	// PURPOSE: Get an animation block by block index
	const crBlockStream* GetBlock(u32 blockIdx) const;

	// PURPOSE: Get array of blocks
	crBlockStream*const* GetBlocks() const;

	// PURPOSE: Get the size of the largest block in the animation (in bytes)
	// RETURNS: block size, in bytes (16 byte aligned)
	// Only meaningful if animation is packed, see crAnimation::IsPacked()
	// Returns block size after packing, but before any compacting.
	u32 GetMaxBlockSize() const;

	// PURPOSE: Returns internal frame count (for tools/debugging only, not for runtime use).
	// NOTES: The internal frames may not be 30Hz frames.  Internally an animation
	// can have any frequency (possibly in the future not even constant,
	// or consistent among different tracks).
	// There is always an extra (zero duration) terminating frame on the end of the animation,
	// this is included in the count returned here - exercise care when converting
	// to duration, remember to subtract one before multiplying by frame duration.
	u32 GetNumInternalFrames() const;

	// PURPOSE: Returns number of internal frames per chunk (for tools/debugging only, not for runtime use).
	// NOTES: The internal frames may not be 30Hz frames (or even constant frequency).
	// There is always an extra terminating frame (of zero duration) on the end of a chunk.
	// This is NOT included in this count.
	u32 GetNumInternalFramesPerChunk() const;

	// PURPOSE: Sets the duration of the animation.
	// PARAMS: duration - new duration in seconds.
	// NOTES: Changing the duration of an animation changes it's internal frame rate. Duration must be greater than 0.
	void SetDuration(float duration);

	// PURPOSE: Set looped status of the animation
	// PARAMS: looped - is animation looped?
	void SetLooped(bool looped);
	
	// PURPOSE: Is this animation packed
	// RETURNS: true - animation is packed, false - animation is unpacked
	// NOTES: In a packed animation all internal memory allocations in the
	// compression algorithms are packed into a contiguous buffer allocated,
	// stored and managed by the animation.  The allocations are also organized
	// in a block ordered manner, meaning that a small contiguous section of memory
	// contains all the data necessary to decompress an animation at a particular
	// time index.  This is useful for both improving the performance of animation
	// decompression, and allowing animations to be decompressed easily on PS3 SPUs.
	bool IsPacked() const;

	// PURPOSE: Is animation compacted
	// RETURNS: true - animation compacted, false - animation not compacted
	// NOTES: A compacted animation has been packed, and the packed data has then
	// been subjected to further secondary compression.
	// A compacted animation can not be composited by regular methods - its block
	// data must be decompressed first, before composite calls are possible.
	// An exception to this is that a compacted animation may contain some tracks
	// that were not packed and are still available for composition.
	bool IsCompact() const;

	// PURPOSE: Get animation signature (a hash of the animation structure)
	// NOTES: A signature of zero indicates an invalid signature.
	u32 GetSignature() const;

	// PURPOSE: Initialize the animation system
	static void InitClass();

	// PURPOSE: Shutdown the animation system
	static void ShutdownClass();	

	// PURPOSE: Default number of internal frames in a single chunk
	static const u32 sm_DefaultFramesPerChunk = 127;

	// PURPOSE: Calculate number of frames in a particular block (given frames and frames per chunk counts)
	u32 CalcNumFramesInBlock(u32 blockIdx) const;

	// PURPOSE: Internal call. Convert time in seconds to number of internal frames (not necessarily 30ths of a second).
	// RETURNS: Number of internal frames equivalent to time in seconds supplied.
	float ConvertTimeToInternalFrame(float time) const;

	// PURPOSE: Internal call. Convert number of internal frames (not necessarily 30ths of a second) into time in seconds.
	// RETURNS: Time in seconds equivalent to number of internal frames supplied.
	float ConvertInternalFrameToTime(float internalFrame) const;

	// PURPOSE: Get the number of dof inside the animation.
	u32 GetNumDofs() const;

	// PURPOSE: Does the animation contain this degree of freedom
	bool HasDof(u8 track, u16 id) const;

	// PURPOSE: Convert time to block index
	// PARAMS: time - animation time in seconds
	// RETURNS: block index [0..numBlocks-1]
	// NOTES: Only of interest to low level users of animations (ie PS3 decompression).
	u32 ConvertTimeToBlockIndex(float time) const;

	// PURPOSE: Convert time to block time
	// PARAMS: time - animation time in seconds
	// RETURNS: block time
	// NOTES: Only of interest to low level users of animations (ie PS3 decompression).
	float ConvertTimeToBlockTime(float time) const;

	// PURPOSE: Convert time to block time
	// PARAMS: time - animation time in seconds
	// blockIdx - block index to use [0..numBlocks-1]
	// RETURNS: block time for block index provided
	// NOTES: Only of interest to low level users of animations (ie PS3 decompression).
	float ConvertTimeToBlockTime(float time, u32 blockIdx) const;

	// PURPOSE: Convert internal time to block index
	// PARAMS: internalFrame - time, expressed in internal frames
	// RETURNS: block index [0..numBlocks-1]
	// NOTES: Only of interest to low level users of animations (ie PS3 decompression).
	u32 ConvertInternalFrameToBlockIndex(float internalFrame) const;

	// PURPOSE: Convert internal time to block time
	// PARAMS: internalFrame - time, expressed in internal frames
	// RETURNS: block time
	// NOTES: Only of interest to low level users of animations (ie PS3 decompression).
	float ConvertInternalFrameToBlockTime(float internalFrame, u32 blockIdx) const;

#if CR_DEV
	// PURPOSE: .anim file versions.
	// All versions found within array are legal versions that will load correctly.
	// The value in element[0] is the current version that is used during any save operations.
	static const u8 sm_SerializationVersions[];

	// PURPOSE: Allocate and load the animation from a .anim file
	// PARAMS: filename - name of .anim file to load
	// outVersion - optional pointer to byte to store version number of loaded file
	// (undefined if allocation or load fails)
	// pack - pack animation during load (only possible if loading newer anim formats,
	// silently fails with warning on old formats, pack is forced true if loading into
	// resource regardless of value of this parameter).
	// compact - if animation is packed, apply secondary compression to the packed data
	// (compacted animation data can not be decompressed at runtime using regular calls)
	// trackStrip - strip track structures from animation, leave only blocks
	// only respected if animation is packed, compact animations are always track stripped
	// RETURNS: pointer to newly allocated and loaded animation, or NULL if failed.
	static crAnimation* AllocateAndLoad(const char* filename, u8* outVersion=NULL, bool pack=false, bool compact=false, bool trackStrip=true);

	// PURPOSE: Load animation from a .anim file
	// PARAMS: filename - name of .anim file to load
	// outVersion - optional pointer to byte to store version number of loaded file
	// (undefined if load fails)
	// pack - pack animation during load (only possible if loading newer anim formats,
	// silently fails with warning on old formats, pack is forced true if loading into
	// resource regardless of value of this parameter).
	// compact - if animation is packed, apply secondary compression to the packed data
	// (compacted animation data can not be decompressed at runtime using regular calls)
	// trackStrip - strip track structures from animation, leave only blocks
	// only respected if animation is packed, compact animations are always track stripped
	// RETURNS: true - if load successful, false - load failed
	bool Load(const char* filename, u8* outVersion=NULL, bool pack=false, bool compact=false, bool trackStrip=true);

	// PURPOSE: Save animation file as a .anim file
	// PARAMS: filename - name of .anim file to save
	// RETURNS: true - if save successful, false - save failed
	bool Save(const char* filename);

	// PURPOSE: Pack an animation
	// PARAMS:
	// compact - also compact the data (apply secondary compression to packed data)
	// trackStrip - strip track structures from animation, leave only blocks
	// byteSwap - byte swap the blocks
	// only respected if animation is packed, compact animations are always track stripped
	// NOTES: This operation is not cheap, it performs a number of dynamic memory operations.
	// Packing should be performed at load time only.
	// See IsPacked for a description of what packing means internally for an animation.
	// Packing an already packed animation is a no-op.
	// Internal structures within an animation will be reallocated - do continue to use
	// pointers to tracks, blocks or chunks that were taken out prior to packing.
	// Compacted animation data can not be decompressed at runtime using regular calls.
	void Pack(bool compact=false, bool trackStrip=true, bool byteSwap=true);

	// PURPOSE: For use in exporter only.  Used to create an animation from raw data prior to saving at export.
	// PARAMS:
	// numFrames - number of internal frames in this animation
	// duration - duration in seconds of animation
	// looped - is this animation looped
	// mover - does this animation have mover tracks
	// framesPerChunk - number of internal frames per chunk (trades compression against block size)
	// raw - is animation compressed lossless
	// RETURNS: true - indicates animation created successfully
	// NOTES: Can only be called once, numFrames must be > 0.
	bool Create(u32 numFrames, float duration, bool looped=false, bool mover=false, u32 framesPerChunk=sm_DefaultFramesPerChunk, bool raw=false);

	// PURPOSE: For use in exporter only.  Used to create a track in an animation from raw data prior to saving at export.
	// PARAMS: track - track to add to the animation.
	// RETURNS
	//	true - indicates track successfully added to animation.  Ownership (responsibility for destruction) transfered to animation.
	//	false - indicates track not added.  Ownership not transfered, you must destroy track if no longer required or it will leak.
	// NOTES: track must have been created and track, id, type and values etc must have been filled out correctly.
	bool CreateTrack(crAnimTrack* track);

	// PURPOSE: For use in exporter only. Used to create an animation from an array of frames prior to saving at export.
	// PARAMS: frames - array of frames containing dofs to add to the animation.
	// RETURNS:
	//	true - indicates frames successfully added to animation. 
	//	false - indicates there was a problem adding frames to the animation.
	// NOTES:
	//	frames should all contain the same dofs.  The first frame will be used as a template to
	//	query the other frames, if subsequent frames contain additional dofs they will be ignored,
	//	whilst if they are missing dofs the value from frame 0 will be used.
	// SEE ALSO: CreateFromFrameFast
	bool CreateFromFrames(const atArray<const crFrame*>& frames, const crAnimTolerance& compressionTolerance);

	// PURPOSE: For use in exporter only. Used to create an animation from an array of frames prior to saving at export.
	//	All frames MUST be structurally identical in the fast version, if frames contain different dofs, use non-fast version.
	// PARAMS: frames - array of frames containing dofs to add to the animation.
	// RETURNS: true - indicates frames successfully added to animation, false - indicates there was a problem adding frames to the animation.
	// NOTES: frames MUST contain the same dofs.  Use non-fast version for non-identical frames.
	// SEE ALSO: CreateFromFrames
	bool CreateFromFramesFast(const atArray<const crFrame*>& frames, const crAnimTolerance& compressionTolerance);

	// PURPOSE: Internal call.  Finds track.
	// PARAMS: track - track index, id - id index.
	// RETURNS: pointer to track.  If NULL track not found.
	crAnimTrack* FindTrack(u8 track, u16 id) const;

	// PURPOSE:	Allows enumeration of the tracks contained within an animation (tools build only).
	// PARAMS:	idx - index of track [0..(GetNumTracks()-1)]
	// NOTES: No guarantee is made that the tracks will be returned in any particular order,
	// only that whilst no tracks are added or removed the tracks will enumerate consistently.
	crAnimTrack* GetTrack(int idx) const;

	// PURPOSE: Internal call.  Finds track and index in internal array.
	// PARAMS
	//	track - track index.
	//	id - id index.
	//	outIdx - if track found, this is the track's index in the internal array, if not found, this is the index of the insersion point.
	// RETURNS: pointer to track.  If NULL track not found (and outIdx will contain insertion index).
	crAnimTrack* FindTrackIndex(u8 track, u16 id, int& outIdx) const;

	// PURPOSE: Internal call.  Adds track data to the internal arrays.
	// PARAMS: track - pointer to track to be added.
	// RETURNS
	//	true - successfully added, ownership (responsibility for destruction) transfered to animation
	//	false - not added, matching track already exists, ownership not transfered.
	// NOTES: If not added, remember to destroy track if not required or it will leak.
	bool AddTrack(crAnimTrack* track);

	// PURPOSE: Internal call.  Deletes track data in the internal arrays.
	// PARAMS: idx - track index.
	// RETURNS
	//	true - successfully deleted 
	//	false - not deleted, matching track does not exist,
	// NOTES:
	bool DeleteTrack(int idx);

	// PURPOSE: Estimate total storage used, in bytes
	// RETURNS: Estimated total storage used, in bytes
	// NOTES: This is an estimate only, memory alignment/fragmentation can
	// cause the animation to use more than this estimated total.
	u32 ComputeSize() const;
#endif

private:
	// PURPOSE: Prevent use of the copy constructor
	crAnimation(const crAnimation&) {}

	// PURPOSE: Prevent use of the assignment operator
	crAnimation& operator=(const crAnimation&) { return *this; }

	// PURPOSE:	Internal call. Serialize ANI8 animation format, this does the actual loading and saving work.
	void SerializeANI8(crArchive& a);

	static void ResourcePageIn(class datResource&);

	// PURPOSE: Internal call.  Finds dof.
	// PARAMS: track - track index, id - id index.
	// RETURNS: pointer to dof.  If NULL dof not found.
	const Dof* FindDof(u8 track, u16 id) const;

	// PURPOSE: Internal function, calculate number of blocks (given frames and frames per chunk counts)
	u32 CalcNumBlocks() const;	

	// PURPOSE: Internal function that calculates a signature (a hash) representing all the tracks (their tracks/ids/types).
	// NOTES
	//	Anim signatures allow frames to be quickly compared to determine if their
	//	structures, but not their values, are equal.  This allows operations between
	//	animations and frames to have optimized paths, when they are structurally identical.
	//	The algorithm here MUST match the one used in crFrame::CalcSignature
	void CalcSignature();

private:

	// PURPOSE: Rage flags
	enum
	{
		kLooped = BIT0,
		kRaw = BIT3,
		kMoverTracks = BIT4,
		kPacked = BIT8,
		kCompact = BIT10,
		kNonSerializableMask = kPacked|kCompact,
	};

	// PURPOSE: Rage flags (see enumeration)
	u16 m_Flags;

	// PURPOSE: Project specific flags
	u16 m_ProjectFlags;

	// PURPOSE: Number of internal frames (including terminating zero duration frame)
	u16 m_NumFrames;

	// PURPOSE: Frames per chunk (excluding any terminating frames)
	u16 m_FramesPerChunk;

	// PURPOSE: Animation duration (in seconds)
	f32 m_Duration;

	// PURPOSE: Animation signature (hash that identifies structure)
	u32 m_Signature;

	// PURPOSE: Pointer to animation name string
	const char* m_Name;

	// PURPOSE: Animation tracks, structurally organized view of the compressed animation data
	atArray< crAnimTrack* > m_Tracks;

	// PURPOSE: Maximum block size, if animation data is packed
	u32 m_MaxBlockSize;

	// PURPOSE: Reference count
	mutable u32 m_RefCount;

	// PURPOSE: Class initialization
	static bool sm_InitClassCalled;

	// PURPOSE: Array of block stream
	atArray< datRef<crBlockStream> > m_Blocks;

	// purpose: Array with degrees of freedom
	struct Dof
	{
		DECLARE_DUMMY_PLACE(Dof);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct &s);
#endif
		u16 m_Id;
		u8 m_Type;
		u8 m_Track;
	};
	atArray<Dof> m_Dofs;
};

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimation::GetNumDofs() const
{
	return (!CR_DEV || m_Dofs.GetCount()!=0) ? m_Dofs.GetCount() : m_Tracks.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimation::HasDof(u8 track, u16 id) const
{
#if CR_DEV
	return m_Dofs.GetCount()!=0 ? (FindDof(track, id) != NULL) : (FindTrack(track, id) != NULL);
#else // CR_DEV
	return FindDof(track, id) != NULL;
#endif // CR_DEV
}

////////////////////////////////////////////////////////////////////////////////

inline int crAnimation::GetRef() const
{
	return m_RefCount;
}

////////////////////////////////////////////////////////////////////////////////

inline void crAnimation::AddRef() const
{
	sysInterlockedIncrement(&m_RefCount);
}

////////////////////////////////////////////////////////////////////////////////

inline int crAnimation::Release() const
{
	int refCount = int(sysInterlockedDecrement(&m_RefCount));
	if(!refCount)
	{
		delete this;
		return 0;
	}
	else
	{
		return refCount;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline const char* crAnimation::GetName() const
{
	return m_Name;
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::GetDuration() const
{
	return m_Duration;
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::GetNum30Frames() const
{
	return 1.f + m_Duration * 30.f;
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::ConvertPhaseTo30Frame(float phase) const
{
	return (phase * m_Duration) * 30.f;
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::ConvertPhaseToTime(float phase) const
{
	return phase * m_Duration;
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::Convert30FrameToPhase(float frame) const
{
	if(m_Duration > 0.f)
	{
		return (frame / 30.f) / m_Duration;
	}
	return 0.f;
}

///////////////////////////////////////////////////////////////////////////////

inline float crAnimation::Convert30FrameToTime(float frame) const
{
	return frame / 30.f;
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::ConvertTimeToPhase(float time) const
{
	if(m_Duration > 0.f)
	{
		return time / m_Duration;
	}
	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::ConvertTimeTo30Frame(float time) const
{
	return time * 30.f;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimation::IsLooped() const
{
	return (m_Flags&kLooped)!=0;	
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimation::IsRaw() const
{
	return (m_Flags&kRaw)!=0;	
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimation::HasMoverTracks() const
{
	return (m_Flags&kMoverTracks)!=0;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimation::ConvertTimeToBlockIndex(float time) const
{
	return ConvertInternalFrameToBlockIndex(ConvertTimeToInternalFrame(time));
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::ConvertTimeToBlockTime(float time) const
{
	f32 internalFrame = ConvertTimeToInternalFrame(time);
	u32 blockIdx = ConvertInternalFrameToBlockIndex(internalFrame);
	return ConvertInternalFrameToBlockTime(internalFrame, blockIdx);
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::ConvertTimeToBlockTime(float time, u32 blockIdx) const
{
	f32 internalFrame = ConvertTimeToInternalFrame(time);
	return ConvertInternalFrameToBlockTime(internalFrame, blockIdx);
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::ConvertInternalFrameToBlockTime(float internalFrame, u32 blockIdx) const
{
	return Clamp(internalFrame - float(blockIdx * m_FramesPerChunk), 0.f, float(m_FramesPerChunk));
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::ConvertTimeToInternalFrame(float time) const
{
	return time * float(m_NumFrames-1) / m_Duration;
}

////////////////////////////////////////////////////////////////////////////////

inline float crAnimation::ConvertInternalFrameToTime(float internalFrame) const
{
	return internalFrame * m_Duration / float(m_NumFrames-1);
}

////////////////////////////////////////////////////////////////////////////////

inline u16 crAnimation::GetProjectFlags() const
{
	return m_ProjectFlags;
}

////////////////////////////////////////////////////////////////////////////////

inline void crAnimation::SetProjectFlags(u16 flags)
{
	m_ProjectFlags = flags;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimation::GetMaxBlockSize() const
{
	return m_MaxBlockSize;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimation::GetNumInternalFrames() const
{
	return m_NumFrames;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimation::GetNumInternalFramesPerChunk() const
{
	return m_FramesPerChunk;
}

////////////////////////////////////////////////////////////////////////////////

inline void crAnimation::SetDuration(float duration)
{
	// TODO - duration == 0.f is probably valid, but need to double check before allowing it...
	FastAssert(duration > 0.f);

	m_Duration = duration;
}

////////////////////////////////////////////////////////////////////////////////

inline void crAnimation::SetLooped(bool looped)
{
	if(looped)
	{
		m_Flags |= kLooped;
	}
	else
	{
		m_Flags &= ~kLooped;
	}
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimation::IsPacked() const
{
	return (m_Flags & kPacked) != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline bool crAnimation::IsCompact() const
{
	return (m_Flags & kCompact) != 0;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimation::GetSignature() const
{
	return m_Signature;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimation::CalcNumBlocks() const
{
	if(m_NumFrames > 1)
	{
		return ((m_NumFrames-2)/m_FramesPerChunk) + 1;
	}
	return 1;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimation::CalcNumFramesInBlock(u32 blockIdx) const
{
	if(blockIdx < (CalcNumBlocks()-1))
	{
		return m_FramesPerChunk + 1;
	}
	else
	{
		u32 remainder = u32((m_NumFrames-1) % m_FramesPerChunk);
		if(remainder)
		{
			return remainder + 1;
		}
		else
		{
			return m_FramesPerChunk + 1;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimation::ConvertInternalFrameToBlockIndex(float internalFrame) const
{
	return Clamp(u32(internalFrame / float(m_FramesPerChunk)), u32(0), u32(GetNumBlocks()-1));
}

////////////////////////////////////////////////////////////////////////////////

inline crBlockStream*const* crAnimation::GetBlocks() const
{
	return &m_Blocks[0].ptr;
}

////////////////////////////////////////////////////////////////////////////////

inline const crBlockStream* crAnimation::GetBlock(u32 blockIdx) const
{
	return m_Blocks.GetCount()!=0 ? m_Blocks[blockIdx] : NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 crAnimation::GetNumBlocks() const
{
	return m_Blocks.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

}	// namespace rage

#endif // CRANIMATION_ANIMATION_H

